<?php

function decodeString($sStr) {
	$sStr = html_entity_decode($sStr, ENT_COMPAT);
	return $sStr;
}

	/**
	 * Funkcja pobiera z bazy danych konfiguracje serwisu i zapisuja ja do tablicy $aConfig
	 * 
	 * @param	integer	$iLangId	- Id wersji jez.
	 * @return	void
	 */
	function setWebsiteSettings($iLangId, $sWebsite='') {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT allow_comments, allow_versioning, name, email, users_email, orders_email, mail_footer_order, mail_footer
						 FROM ".$aConfig['tabls']['prefix']."website_settings
						 WHERE language_id = ".$iLangId;
		if ($sWebsite != '') {
			list ($aConfig['common']['allow_comments'],
						$aConfig['common']['allow_versioning'],
						$aConfig['default']['website_name'],
						$aConfig['default']['website_email'],
						$aConfig['default']['website_users_email'],
						$aConfig['default']['website_orders_email'],
            $aConfig['default']['website_mail_footer_order'],
						$aConfig['default']['website_mail_footer']) = $pDbMgr->GetRow($sWebsite, $sSql, array(), DB_FETCHMODE_ORDERED);
		} else {
			list ($aConfig['common']['allow_comments'],
						$aConfig['common']['allow_versioning'],
						$aConfig['default']['website_name'],
						$aConfig['default']['website_email'],
						$aConfig['default']['website_users_email'],
						$aConfig['default']['website_orders_email'],
            $aConfig['default']['website_mail_footer_order'],
						$aConfig['default']['website_mail_footer']) = Common::GetRow($sSql, array(), DB_FETCHMODE_ORDERED);
		}
	} // end of setWebsiteSettings() function
  
/**
 * Metoda przenosi przeslany plik do wskazanego katalogu zmieniajac
 * jego rozmiar do podanej szerokosci/wysokosci (w zaleznosci od orientacji
 * obrazka - pion, poziom)
 * 
 * @param	string  $sDestDir	- sciezka do katalogu docelowego, bez koncowego '/' lub '\'
 * @param	string	$sSrc	- sciezka do pliku zrodlowego
 * @param	array ref	$aImg	- dane pliku pobrane przez getimagesize
 * @param	string	$sNewWidthHeight	- nowa szerokosc/wysokosc obrazka	
 * @param	bool    $sNewName	- true: wygeneruj unikalna nazwe pliku, domyslnie false
 * @param	string	$sOutType	- typ wynikowego obrazka - 'jpg', 'png', 'gif'
 * @param	integer $iMode	- atrybuty dla nowo utworzonego pliku
 * @param	integer	$iQuality	- jakosc obrazka wynikowego jpg (0 - 100)
 * @return	integer	- -4: rozmiary docelowe wieksze niz pliku zrodlowego
 * 										-3: podana sciezka nie wskazuje na katalog
 *                  	-2: katalog nie jest zapisywalny
 *                  	-1: proba przeniesienia do katalogu docelowego nie powiodla sie
 *                   	 1: plik zostal przeniesiony do katalogu docelowego
 */
function moveResizedImage($sDestDir, $sSrc, &$aImg, $sNewWidthHeight, $sNewName, $sOutType='', $iMode=0644, $iQuality=90) {
	$aImageSize = array();
	$iNewWidth	= 0;
	$iNewHeight	= 0;
	
	if (is_dir($sDestDir)) {
		if (is_writable($sDestDir)) {
			// od tego miejsca zaczyna sie kod odpowiedzialny za zmiane rozmiaru obrazka
			$aNewSize = explode('x', $sNewWidthHeight);

			if ($aImg[0] > $aNewSize[0] || $aImg[1] > $aNewSize[1]) {
				// okreslenie nowych rozmiarow
				if ($aImg[0] > $aImg[1]) {
					// szerokosc jest wieksza od wysokosci
					$iNewWidth =& $aNewSize[0];
					$iNewHeight = ceil(($iNewWidth * $aImg[1]) / $aImg[0]);
				}
				else {
					// wysokosc jest wieksza od szerokosci
					$iNewHeight =& $aNewSize[1];
					$iNewWidth = ceil(($iNewHeight * $aImg[0]) / $aImg[1]);
				}
				if ($iNewWidth > $aNewSize[0]) {
					// szerokosc z wyliczynocyh nowych rozmiarow obrazka jest wieksza niz
					// szerokosc maxymalna, dostosowanie szerokosci do szerokosci max.
					// przeliczenie na nowo wysokosci
					$iNewWidth = $aNewSize[0];
					$iNewHeight = ceil(($iNewWidth * $aImg[1]) / $aImg[0]);
				}
				if ($iNewHeight > $aNewSize[1]) {
					// wysokosc z wyliczynocyh nowych rozmiarow obrazka jest wieksza niz
					// wysokosc maxymalna, dostosowanie wysokosci do wysokosci max.
					// przeliczenie na nowo szerokosci
					$iNewHeight = $aNewSize[1];
					$iNewWidth = ceil(($iNewHeight * $aImg[0]) / $aImg[1]);
				}
				// utworzenie obrazka o nowych rozmiarach
				if ($aImg['mime'] == "image/pjpeg" ||
						$aImg['mime'] == "image/jpeg" ||
						$aImg['mime'] == "image/jpg") {
					$oHandle = imagecreatefromjpeg($sSrc);
				}
				elseif ($aImg['mime'] == "image/png") {
					$oHandle = imagecreatefrompng($sSrc);
				}
				elseif ($aImg['mime'] == "image/gif") {
					$oHandle = imagecreatefromgif($sSrc);
				}
				else {
					return -1;
				}
				$oDstImg = imagecreatetruecolor($iNewWidth, $iNewHeight);
				if (!$oDstImg) {
					return -1;
				}
				if (!imagecopyresampled($oDstImg, $oHandle, 0, 0, 0, 0, $iNewWidth, $iNewHeight, $aImg[0], $aImg[1])) {
					return -1;
				}
				imagedestroy($oHandle);
				if ($sOutType == '') {
					$sOutType = $aImg['mime'];
				}
				if ($sOutType == "image/pjpeg" ||
						$sOutType == "image/jpeg" ||
						$sOutType == "image/jpg") {
					if (!imagejpeg($oDstImg, $sDestDir.'/'.$sNewName, $iQuality)) {
						return -1;
					}
				}
				elseif ($sOutType == "image/png") {
					if (!imagepng($oDstImg, $sDestDir.'/'.$sNewName)) {
						return -1;
					}
				}
				elseif ($sOutType == "image/gif") {
					if (!imagegif($oDstImg, $sDestDir.'/'.$sNewName)) {
						return -1;
					}
				}
				else {
					return -1;
				}
				@imagedestroy($oDstImg);
				// zmieniamy atrybuty
				@chmod($sDestDir.'/'.$sNewName, $iMode);
				return 1;
			}
			else {
				// nie ma potrzeby kopiowania zdjec - rzmiar duzego mniejszy niz docelowego
				return -4;
			}
		}
		return -2;
	}
	return -3;
} // end of moveResizedImage() method



/**
 * Funkcja koduje haslo
 * 
 * @param		string	$sString	- ciag do zakodowania
 * @return	string
 */
function encodePasswd($sString, $sSalt) {
	global $aConfig;
  if ($sString === '') return '';
  
  $iDCount = strlen($sSalt);
  $iDHalf = floor($iDCount/2);
  $sDLHalf = substr($sSalt, 0, $iDHalf);
  $sDRHalf = substr($sSalt, $iDHalf, $iDCount);
  
  $sString = sha1($sDLHalf.$sString.$sDRHalf);
  $sSaltPHP = $aConfig['common']['sha1_key'];
  $iCCount = strlen($sSaltPHP);
  $iCHalf = floor($iCCount/2);
  $sCLHalf = substr($sSaltPHP, 0, $iCHalf);
  $sCRHalf = substr($sSaltPHP, $iCHalf, $iCCount);
  return sha1($sCLHalf.$sString.$sCRHalf);
} // end of encodePasswd() function


/**
 * Funkcja dekoduje haslo
 * 
 * @param		string	$sString	- ciag do zdekodowania
 * @return	string
 */
function decodePasswd($sString) {
	global $aConfig;
	return rc4($aConfig['common']['rc4_key'], base64_decode($sString));
} // end of decodePasswd() function






/**
 * Crypt/decrypt strings with RC4 stream cypher algorithm.
 *
 * @param string $key Key
 * @param string $data Encripted/pure data
 * @see   http://pt.wikipedia.org/wiki/RC4
 * @return string
 */
function rc4($key, $data) {
    // Store the vectors "S" has calculated
    static $SC;
    // Function to swaps values of the vector "S"
    $swap = create_function('&$v1, &$v2', '
        $v1 = $v1 ^ $v2;
        $v2 = $v1 ^ $v2;
        $v1 = $v1 ^ $v2;
    ');
    $ikey = crc32($key);
    if (!isset($SC[$ikey])) {
        // Make the vector "S", basead in the key
        $S    = range(0, 255);
        $j    = 0;
        $n    = strlen($key);
        for ($i = 0; $i < 255; $i++) {
            $char  = ord($key{$i % $n});
            $j     = ($j + $S[$i] + $char) % 256;
            $swap($S[$i], $S[$j]);
        }
        $SC[$ikey] = $S;
    } else {
        $S = $SC[$ikey];
    }
    // Crypt/decrypt the data
    $n    = strlen($data);
    $data = str_split($data, 1);
    $i    = $j = 0;
    for ($m = 0; $m < $n; $m++) {
        $i        = ($i + 1) % 256;
        $j        = ($j + $S[$i]) % 256;
        $swap($S[$i], $S[$j]);
        $char     = ord($data[$m]);
        $char     = $S[($S[$i] + $S[$j]) % 256] ^ $char;
        $data[$m] = chr($char);
    }
    return implode('', $data);
} // end of rc4() function







/**
 * Funkcja okresla liczbe wersji jezykowych zdefiniowanych
 * dla serwisu
 * 
 * @return	integer	- liczba wersji jez.
 */
function &getLangVersions() {
	global $aConfig;
	
	$sSql = "SELECT id, symbol, default_lang
					 FROM ".$aConfig['tabls']['prefix']."languages".
					 (!isPreviewMode() ? " WHERE published = '1'" : '');
	return Common::GetAll($sSql);
} // end of getLangVersions()

/**
 * Funkcja pobiera ustawienia serwisu dla danej wersji jezykowej
 * oraz wersje CMSa
 * 
 * @return	array
 */
function &getSiteSettings() {
	global $aConfig;
	$aSettings = array();
	
	$sSql = "SELECT *
				 	 FROM ".$aConfig['tabls']['prefix']."website_settings
				 	 WHERE language_id = '".$_GET['lang_id']."'";
	$aSettings =& Common::GetRow($sSql);
	if (empty($aSettings['subtitle'])) {
		$aSettings['subtitle'] = $aSettings['title'];
	}
	
	$sSql = "SELECT name, version
				 	 FROM ".$aConfig['tabls']['prefix']."version";
	$aSettings['product'] =& Common::GetRow($sSql);
	
	return $aSettings;
} // end of getSiteSettings() function


/**
 * Funkcja pobiera i zwraca link do strony o podanym Id
 * 
 * @param	array	$aPage	- dane strony 
 */
function getPageLink(&$aPage) {
	global $aConfig;
	
	// sprawdzenie typu strony
	switch ($aPage['mtype']) {
		case '0':
			// strona modulu
			return $aConfig['_tmp']['lang'].'/'.$aPage['symbol'];
		break;
		
		case '1':
			// link wewnetrzny - pobranie linku do strony do ktorej linkuje
			$iLId = Common::GetLinkToId($aPage['link_to_id'], $_GET['lang_id']);
			if (Common::isExternalLink($iLId, $_GET['lang_id'])) {
				return Common::getExternalLink($iLId, $_GET['lang_id']);
			}
			else {
				return $aConfig['_tmp']['lang'].'/'.Common::getPageSymbol($iLId, $_GET['lang_id']);
			}
		break;
		
		case '2':
			// link zewnetrzny
			return $aPage['url'];
		break;
	}
} // end of getPageLink() function


function getPageData($symbol, $langId) {
    $sSql = "SELECT A.id, A.symbol, IFNULL(A.parent_id, 0) AS parent_id, A.template,
									A.menu_id, A.name, A.mtype, A.link_to_id, A.url, A.new_window, A.description, A.seo_title, A.seo_keywords,
									A.restricted, A.module_id, A.parent_id, A.moption, B.symbol AS module
					 FROM menus_items AS A
 	 				 LEFT JOIN modules AS B
 	 				 ON B.id = A.module_id
					 WHERE A.language_id = ".$langId." AND
					 			 A.symbol = '".$symbol."'";
    return Common::GetRow($sSql);
}

/**
 * Funkcja ma podstawie symbolu aktualnie przegladanej strony
 * pobiera dane jej wszystkich 'rodzicow' i jej samej
 * 
 * @return	array
 */
function setPages($path, $langId) {
	global $aConfig, $pSmarty;
	$aPath		= array();
	$sSymbol = array_pop(explode('/', preg_replace('/\/$/', '', $path)));

    $aItem = getPageData($sSymbol, $langId);

	if (empty($aItem['id'])) {
		// nie ma strony o takim symbolu - przekierowanie na index
        $aItem = set404Page();
	}
	if ($aItem['symbol'] == $aConfig['common']['404_page_symbol']) {
        header('HTTP/1.0 404 Not Found');
    }
	$aConfig['_tmp']['path'][] = array(
		'id' => $aItem['id'],
		'menu_id' => $aItem['menu_id'],
		'symbol' => $aItem['symbol'],
		'name' => $aItem['name'],
		'link' => getPageLink($aItem),
		'target' => ($aItem['new_window'] == '1' ? '_blank' : ''),
		'restricted' => $aItem['restricted'],
		'module_id' => $aItem['module_id'],
		'parent_id' => (int) $aItem['parent_id'],
		'module' => $aItem['module'],
		'mtype' => $aItem['mtype'],
		'link_to_id' => $aItem['link_to_id'],
		'moption' => $aItem['moption'],
		'description' => $aItem['description'],
		'keywords' => $aItem['seo_keywords'],
		'title' => $aItem['seo_title'],
		'template' => !empty($aItem['template']) ? $aItem['template'] : 'page.tpl'
	);
	$aConfig['_tmp']['selected'][] = $aItem['id'];
	while (intval($aItem['parent_id']) > 0) {
		$sSql = "SELECT A.id, A.symbol, IFNULL(A.parent_id, 0) AS parent_id, A.template,
  									A.menu_id, A.name, A.mtype, A.link_to_id, A.url, A.new_window,
  									A.restricted, A.module_id, A.moption, B.symbol AS module
  					 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
	 	 				 LEFT JOIN ".$aConfig['tabls']['prefix']."modules AS B
	 	 				 ON B.id = A.module_id
  					 WHERE A.language_id = ".$langId." AND
  					 			 A.id = ".$aItem['parent_id'];
  	$aItem =& Common::GetRow($sSql);
  	$aConfig['_tmp']['path'][] = array(
			'id' => $aItem['id'],
			'menu_id' => $aItem['menu_id'],
			'symbol' => $aItem['symbol'],
			'name' => $aItem['name'],
			'link' => getPageLink($aItem),
			'target' => ($aItem['new_window'] == '1' ? '_blank' : ''),
			'restricted' => $aItem['restricted'],
			'module_id' => $aItem['module_id'],
			'module' => $aItem['module'],
			'mtype' => $aItem['mtype'],
			'link_to_id' => $aItem['link_to_id'],
			'moption' => $aItem['moption'],
			'template' => !empty($aItem['template']) ? $aItem['template'] : 'page.tpl'
		);
		$aConfig['_tmp']['selected'][] = $aItem['id'];
	}
	// odwrocenie elementow sciezki
	$aConfig['_tmp']['path'] = array_reverse($aConfig['_tmp']['path']);
	// ustawienie aktualnej strony
	$aConfig['_tmp']['page'] =& $aConfig['_tmp']['path'][count($aConfig['_tmp']['path'])-1];
	// zdjecie aktualnie przegladanej strony
	$aConfig['_tmp']['page']['image'] = getItemImage($aConfig['tabls']['prefix']."pages_images", array('page_id' => $aConfig['_tmp']['page']['id']), '', true, false);
} // end of setPages() function

/**
 * @return mixed
 */
function set404Page () {
	global $aConfig;
    $pageSymbol404 = $aConfig['common']['404_page_symbol'];
    return getPageData($pageSymbol404, $_GET['lang_id']);
}

function setProductAllInfo($iId) {
	global $aConfig;
	
	// ustawienie $iId jako $_GET['id']
	$_GET['id'] = $iId;
	// pobranie informacji o stronie i module produktu
	$sSql = "SELECT A.id, A.symbol, A.template, A.menu_id, A.name, A.mtype, 
                  A.restricted, A.module_id, A.moption, B.symbol AS module, 
				  A.published AS prod_published, C.published AS cat_published,
				  C.id AS product_id, C.name AS product_name
					 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
 	 				 LEFT JOIN ".$aConfig['tabls']['prefix']."modules AS B
            ON B.id = A.module_id
 	 				 JOIN ".$aConfig['tabls']['prefix']."products AS C
            ON A.id = C.page_id
					 WHERE C.id = ".$iId;
	$aTmp =& Common::GetRow($sSql);
  // (!isPreviewMode() ? " AND A.published = '1' AND C.published = '1'" : '')
  
	if(!empty($aTmp) && (isPreviewMode() === TRUE || ($aTmp['prod_published'] == '1' && $aTmp['cat_published'] == '1') )){
		$aConfig['_tmp']['page'] = array(
			'product_id' => (double) $aTmp['product_id'],
			'product_name' => $aTmp['product_name'],
			'id' => (double) $aTmp['id'],
			'menu_id' => (double) $aTmp['menu_id'],
			'symbol' => $aTmp['symbol'],
			'name' => $aTmp['name'],
			'link' => getPageLink($aTmp),
			'restricted' => $aTmp['restricted'],
			'module_id' => (double) $aTmp['module_id'],
			'module' => $aTmp['module'],
			'mtype' => $aTmp['mtype'],
			'moption' => $aTmp['moption'],
			'template' => !empty($aTmp['template']) ? $aTmp['template'] : 'page.tpl'
		);
	} elseif ($aTmp['prod_published'] == '0' || $aTmp['cat_published'] == '0') {
        // pobierzmy kategorię najniższego poziomu dla produktu
        $sSql = "
          SELECT MI.symbol
          FROM products AS P
          JOIN products_extra_categories AS PEC
            ON P.id = PEC.product_id
          JOIN menus_items AS MI
            ON PEC.page_id = MI.id AND parent_id IS NOT NULL
          WHERE
            P.id = ".$iId;
        $sCatSymbol = Common::GetOne($sSql);
        if ($sCatSymbol != '') {
          // do kategorii
          doRedirect('/'.$sCatSymbol, '301');
        } else {
            setPages($aConfig['common']['404_page_symbol'], $_GET['lang_id']);
        }
	} else {
        setPages($aConfig['common']['404_page_symbol'], $_GET['lang_id']);
    }
} // end of setProductAllInfo() method


/**
 * Funkcja pobiera zdefiniowane dla serwisu obszary
 * 
 * @param	bool	$bStartPage	- true: dla strony startowej; false: dla podstron
 * @return	void
 */
function getAreas($bStartPage) {
	global $aConfig;
	
	$sSql = "SELECT DISTINCT id, number, subpage_area
					 FROM ".$aConfig['tabls']['prefix']."areas";
	$aConfig['_tmp']['areas'] =& Common::GetAll($sSql);
	if (!$bStartPage) {
		// wydzielenie glownego obszaru podstron
		foreach ($aConfig['_tmp']['areas'] as $iKey => $aArea) {
			if ($aArea['subpage_area'] == '1') {
				$aConfig['_tmp']['main_area']['id'] = $aArea['id'];
				$aConfig['_tmp']['main_area']['number'] = $aArea['number'];
				unset($aConfig['_tmp']['areas'][$iKey]);
			}
			else {
				unset($aConfig['_tmp']['areas'][$iKey]['subpage_area']);
			}
		}
	}
} // end of getAreas() function 


/**
 * Funkcja pobiera ustawienia modulow - cache'owania
 * 
 */
function getModulesSettings() {
	global $aConfig;
	$sSql = "SELECT id, symbol, cacheable, exclude_from_cache
									FROM ".$aConfig['tabls']['prefix']."modules";
	if (($aConfig['_tmp']['modules_settings'] =& Common::GetAssoc($sSql, true)) === false) {
		$aConfig['_tmp']['modules_settings'] = array();
	}
	foreach ($aConfig['_tmp']['modules_settings'] as $iKey => $aItem) {
		$aConfig['_tmp']['modules_settings'][$iKey]['id'] = $iKey;
		if (!empty($aItem['exclude_from_cache'])) {
			$aConfig['_tmp']['modules_settings'][$iKey]['exclude_from_cache'] = explode(',', $aItem['exclude_from_cache']);
		}
	}
} // end of getModulesSettings() function


/**
 * Funkcja zwraca galaz drzewa dla strony o Id $iPId
 * 
 * @param	array	$aItems	- tablica stron
 * @param	integer	$iPId	- Id strony dla ktorej zwracana jest galaz
 */
function &getTreeBranch($aItems, $iPId) {
	foreach ($aItems['item'] as $aItem) {
		if ((int) $aItem['id'] === (int) $iPId) {
			// uusniecie podstron podstron :)
			if (isset($aItem['children']['item'])) {
  			foreach ($aItem['children']['item'] as $iKey => $aChild) {
  				unset($aItem['children']['item'][$iKey]['children']);
  			}
			}
			return $aItem;
		}
		if (isset($aItem['children'])) {
			if (($aBranch =& getTreeBranch($aItem['children'],
																						 $iPId)) !== false) {
				return $aBranch; 
			}
		}
	}
	return false;
} // end of getTreeBranch() function
	

function TriggerError($sErr, $iErrType = E_USER_ERROR) {
	trigger_error($sErr, $iErrType);
}


function AddCookie($sVarName, $sValue='', $iExpDate=0, $sPath='/', $sDomain='', $iSafe=0) {
	return setcookie($sVarName, $sValue, $iExpDate, $sPath, $sDomain, $iSafe);
} // end of AddCookie() function


function DeleteCookie($sVarName, $sPath='/', $sDomain='', $iSafe=0){
	return setcookie($sVarName, '', time() - 3600, $sPath, $sDomain, $iSafe);
} // end of DeleteCookie() function


function formatDate($sDate, $iMode=1) {
	if ($iMode == 1) {
		if (preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $sDate, $aMatches)) {
			return $aMatches[3].'.'.$aMatches[2].'.'.$aMatches[1];
		}
	}
	else {
		if (preg_match('/^(\d{2})-(\d{2})-(\d{4})$/', $sDate, $aMatches)) {
			return $aMatches[3].'-'.$aMatches[2].'-'.$aMatches[1];
		}
	}
	return '';
}


function formatDateTime($sDate) {
	if (preg_match('/^(\d{4})-(\d{2})-(\d{2}) (\d{2}:\d{2}:\d{2})$/', $sDate, $aMatches)) {
		return $aMatches[3].'.'.$aMatches[2].'.'.$aMatches[1].' '.$aMatches[4];
	}
}

function formatDateMysql($sDate) {
	if (preg_match('/^(\d{2}).(\d{2}).(\d{4})$/', $sDate, $aMatches)) {
		return $aMatches[3].'-'.$aMatches[2].'-'.$aMatches[1];
	}
}


function doRedirect($sLocation='', $sOption='') {
	global $aConfig;
	
	if ($sOption == '301') {
		header("HTTP/1.1 301 Moved Permanently");
	} else if ($sOption == '410') {
        header("HTTP/1.0 410 Gone");
    }

	if (strpos($sLocation, $aConfig['common']['client_base_url_https']) !== false) {
		$sLocation = substr($sLocation, strlen($aConfig['common']['client_base_url_https']));
	}

	if (strpos($sLocation, $aConfig['common']['client_base_url_http']) !== false) {
	$sLocation = substr($sLocation, strlen($aConfig['common']['client_base_url_http']));
	}

    if ($sOption != '410') {
        header('Location: ' . $aConfig['common']['client_base_url_http'] . html_entity_decode(preg_replace('/^\//', '', $sLocation)));
        exit();
    }
}

function doRedirectHttps($sLocation='') {
	global $aConfig;
	if (strpos($sLocation, $aConfig['common']['client_base_url_https']) !== false) {
		$sLocation = substr($sLocation, strlen($aConfig['common']['client_base_url_https']));
	}

	if (strpos($sLocation, $aConfig['common']['client_base_url_http']) !== false) {
	$sLocation = substr($sLocation, strlen($aConfig['common']['client_base_url_http']));
	}
    header("HTTP/1.1 301 Moved Permanently");
    if (substr($_SERVER['HTTP_HOST'], 0, 3) !== 'www') {
        header('Location: http://www.'.
            substr($aConfig['common']['base_url'], 0, -1).'/'.html_entity_decode(preg_replace('/^\//', '', $sLocation)));
    } else {
        header('Location: '.$aConfig['common']['client_base_url_https'].html_entity_decode(preg_replace('/^\//', '', $sLocation)));
    }
	exit();
}

function doCartSafeRedirect($sLocation='') {
	global $aConfig;
	if (strpos($sLocation, $aConfig['_tmp']['cart_symbol']) !== false) {
		$sLocation = $aConfig['_tmp']['cart_symbol'];
	}
	if (strpos($sLocation, $aConfig['common']['client_base_url_https']) !== false) {
		$sLocation = substr($sLocation, strlen($aConfig['common']['client_base_url_https']));
	}
	if (strpos($sLocation, $aConfig['common']['client_base_url_http']) !== false) {
		$sLocation = substr($sLocation, strlen($aConfig['common']['client_base_url_http']));
	}
	
	header('Location: '.$aConfig['common']['client_base_url_http'].html_entity_decode(preg_replace('/^\//', '', $sLocation)));
	exit();
}


function strip($sText) {
	return stripslashes(strip_tags($sText));
}

function filterInput($sText) {
	return strip_tags($sText);
}

// first, an HTML attribute stripping function used by safe_html()
//   after stripping attributes, this function does a second pass
//   to ensure that the stripping operation didn't create an attack
//   vector.
function strip_attributes ($html, $attrs) {
  if (!is_array($attrs)) {
    $array= array( "$attrs" );
    unset($attrs);
    $attrs= $array;
  }
  
  foreach ($attrs AS $attribute) {
    // once for ", once for ', s makes the dot match linebreaks, too.
    $search[]= "/".$attribute.'\s*=\s*".+"/Uis';
    $search[]= "/".$attribute."\s*=\s*'.+'/Uis";
    // and once more for unquoted attributes
    $search[]= "/".$attribute."\s*=\s*\S+/i";
  }
  $html= preg_replace($search, "", $html);

  // do another pass and strip_tags() if matches are still found
  foreach ($search AS $pattern) {
    if (preg_match($pattern, $html)) {
      $html= strip_tags($html);
      break;
    }
  }

  return $html;
}

function js_and_entity_check( $html ) {
  // anything with ="javascript: is right out -- strip all tags if found
  $pattern= "/=[\S\s]*s\s*c\s*r\s*i\s*p\s*t\s*:\s*\S+/Ui";
  if (preg_match($pattern, $html)) {
  	//echo "<br /><br />JavaScript</br /><br />";
    return true;
  }
  
  // anything with encoded entites inside of tags is out, too
  $pattern= "/<[^>]*&#[x0-9]*[^>]*>/Ui";
  if (preg_match($pattern, $html)) {
  	//echo "<br /><br />Entities</br /><br />";
    return true;
  }  
  return false;
}

// the safe_html() function
//   note, there is a special format for $allowedtags, see ~line 90
function safe_html ($html, $allowedtags="") {
  
  // check for obvious oh-noes
  if ( js_and_entity_check( $html ) ) {
    $html= strip_tags($html);
    return $html;
  }
  
  // setup -- $allowedtags is an array of $tag=>$closeit pairs, 
  //   where $tag is an HTML tag to allow and $closeit is 1 if the tag 
  //   requires a matching, closing tag
  if ($allowedtags=="") {
    $allowedtags= array ( "p"=>1, "br"=>0, "a"=>1, "img"=>0, 
                        "li"=>1, "ol"=>1, "ul"=>1, 
                        "b"=>1, "i"=>1, "em"=>1, "strong"=>1, 
                        "del"=>1, "ins"=>1, "u"=>1, "code"=>1, "pre"=>1, 
                        "blockquote"=>1, "hr"=>0
                        );
  }
  elseif (!is_array($allowedtags)) {
    $array= array( "$allowedtags" );
  }

  // there's some debate about this.. is strip_tags() better than rolling your own regex?
  // note: a bug in PHP 4.3.1 caused improper handling of ! in tag attributes when using strip_tags()
  $stripallowed= "";
  foreach ($allowedtags AS $tag=>$closeit) {
    $stripallowed.= "<$tag>";
  }

  //print "Stripallowed: $stripallowed -- ".print_r($allowedtags,1);
  $html= strip_tags($html, $stripallowed);

  // also, lets get rid of some pesky attributes that may be set on the remaining tags...
  // this should be changed to keep_attributes($htmlm $goodattrs), or perhaps even better keep_attributes
  //  should be run first. then strip_attributes, if it finds any of those, should cause safe_html to strip all tags.
  $badattrs= array("on\w+", "style", "fs\w+", "seek\w+");
  $html= strip_attributes($html, $badattrs);

  // close html tags if necessary -- note that this WON'T be graceful formatting-wise, it just has to fix any maliciousness
  foreach ($allowedtags AS $tag=>$closeit) {
    if (!$closeit) continue;
    $patternopen= "/<$tag\b[^>]*>/Ui";
    $patternclose= "/<\/$tag\b[^>]*>/Ui";
    $totalopen= preg_match_all ( $patternopen, $html, $matches );
    $totalclose= preg_match_all ( $patternclose, $html, $matches2 );
    if ($totalopen>$totalclose) {
      $html.= str_repeat("</$tag>", ($totalopen - $totalclose));
    }
  }
  
  // check (again!) for obvious oh-noes that might have been caused by tag stipping
  if ( js_and_entity_check( $html ) ) {
    $html= strip_tags($html);
    return $html;
  }

  return $html;
}

function filterBlogContent($sText) {
	$aAllowedTags = array ("p"=>1, "br"=>0, "a"=>1, "img"=>0, 
                        "li"=>1, "ol"=>1, "ul"=>1, 
                        "b"=>1, "i"=>1, "em"=>1, "strong"=>1, 
                        "u"=>1
                        );
	return safe_html($sText, $aAllowedTags);
}


function utf8_strlen($str) {
	$ret = 0;
 	for($i = 0; $i < strlen($str); $i++){
  	if( (ord($str{$i}) >= 0 && ord($str{$i}) <= 127) || ord($str{$i}) >= 192 ) $ret++;
 	}
 	return $ret;
}


function utf8_substr($sStr, $iStart, $iLength=0) {
	$iCount = 0;
	$sRet = '';
	
 	for($i = 0; $i < strlen($sStr); $i++){
 		if ($iCount >= $iStart) {
 			// od tego momentu rozpocyzna sie zadany podciag
 			$sRet .= $sStr{$i};
 		}
  	if ((ord($sStr{$i}) >= 0 && ord($sStr{$i}) <= 127) || ord($sStr{$i}) >= 192) {
  		$iCount++;
  	}
  	if ($iLength > 0 && $iCount == ($iStart + $iLength)) {
  		if (ord($sStr{$i+1}) > 127 && ord($sStr{$i+1}) < 192) {
  			$sRet .= $sStr{$i+1};
  		} 
  		break;
  	}
 	}
 	return $sRet;
}


function trimString($sStr, $iLength) {
    $sStr = mb_substr(strip_tags($sStr), 0, $iLength, 'UTF-8');
    // obciecie do ostatniej spacji
    if (mb_strlen($sStr, 'UTF-8') == $iLength) {
        $sStr = mb_substr($sStr, 0, mb_strrpos($sStr, " ", 'UTF-8'), 'UTF-8');
        $sStr .= "...";
    }
    return $sStr;
}

/**
 * Funkcja zamienia HTMLowy tekst na zwykly tekst, usuwajac znaczniki HTML
 * i zamieniajac znak <br> na znak przejscia do nowej linii \n
 * 
 * @param	string	$sStr	- ciag w ktorym ma zostac dokonana zmiana
 */
function html2text($sStr, $sBRReplacement="\n") {
	return str_replace('"', '', html_entity_decode(strip_tags(preg_replace('/<br[^>]*\/?>/i', $sBRReplacement, $sStr))));
}

/**
 * Funkcja zamienia xHTMLowy <br /> na HTMLowy <br>
 * 
 * @param	string	$sStr	- ciag w ktorym ma zostac dokonana zmiana
 */
function newbr2br($sStr) {
	return preg_replace('/<br[^>]*\/?>/i', '<br>', $sStr);
}

/**
 * Funkcja zamienia znaki <br /> na znaki przejscia do nowej liniii \n
 * 
 * @param	string	$sStr	- ciag w ktorym ma nastapic zamiana
 * @return	string	- ciag po zamianie
 */
function br2nl($sStr, $sReplace="\n") { 
	return html_entity_decode(preg_replace('/<br[^>]*\/?>\n*/i', $sReplace, str_replace("\r", '', $sStr)));
} // end of br2nl() function

function clearEmailSender($sStr) {
	return preg_replace('/[\,\."<>:\[\]\\\\@\(\)]/', '', strip_tags($sStr));
}


/**
 * Funkcja sprawdza czy serwis jest przegladany w trybie podgladu
 * czy w normalnym (wyswietlane tylko opublikowane aktualnosci, artykuly
 * i wydania
 * 
 * @return	bool	- true:  tryb podgladu; false: zwykly tryb
 */
function isPreviewMode() {
	global $aConfig;
	
	return isset($_GET['preview']) && $_GET['preview'] == md5($_SERVER['REMOTE_ADDR'].
										 	 		  					 $aConfig['common']['client_base_url_http']);
}


/**
 * Funkcja sprawdza czy wybrano czyszczenie cache'u
 * 
 * @return	bool	- true: tak; false: nie
 */
function doClearCache() {
	global $aConfig;
	
	return isset($_GET['clear_cache']) && $_GET['clear_cache'] == md5($_SERVER['REMOTE_ADDR'].$aConfig['common']['client_base_url_http']);
}


/**
 * Wersja funkcji print_r() z dodaniem formatowania <pre></pre>
 *
 * @param	mixed	zmienna
 */
function dump() {
	global $aConfig;

		if (empty($aConfig['common']['programmer_ip']) || 
			 (!empty($aConfig['common']['programmer_ip']) && isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], $aConfig['common']['programmer_ip'])) || 
       (!isset($_SERVER['REMOTE_ADDR']))) {
	    $aTrace = debug_backtrace(false);
	    $aArgs = func_get_args();
	    echo "<pre style='white-space: pre'>";
	    echo "Line {$aTrace[0]['line']} \n";
	    echo "File {$aTrace[0]['file']} \n";
			foreach ($aArgs as $aArg) {
		  	print_r($aArg);
		  }
		  echo "</pre>";
		}
}// end of dump function 


/**
 * Funkcja zamienia pomiedzy stronami kodowymi polskich znakow
 */
function changeCharcodes($sStr, $sFrom, $sTo) {
	$aCodes = array(
		'iso88592' => array(
			"\xA1", "\xB1", // A, a
			"\xC6", "\xE6", // C, c
			"\xCA", "\xEA", // E, e
			"\xA3", "\xB3", // L, l
			"\xD1", "\xF1", // N, n
			"\xD3", "\xF3", // O, o
			"\xA6", "\xB6", // S, s
			"\xAC", "\xBC", // Z('), z(')
			"\xAF", "\xBF"  // Z(.), z(.)
		),
		'windows1250' => array(
			"\xA5", "\xB9", // A, a
			"\xC6", "\xE6", // C, c
			"\xCA", "\xEA", // E, e
			"\xA3", "\xB3", // L, l
			"\xD1", "\xF1", // N, n
			"\xD3", "\xF3", // O, o
			"\x8C", "\x9C", // S, s
			"\x8F", "\x9F", // Z('), z(')
			"\xAF", "\xBF"  // Z(.), z(.)
		),
		'utf8' => array(
			"\xC4\x84", "\xC4\x85", // A, a
			"\xC4\x86", "\xC4\x87", // C, c
			"\xC4\x98", "\xC4\x99", // E, e
			"\xC5\x81", "\xC5\x82", // L, l
			"\xC5\x83", "\xC5\x84", // N, n
			"\xC3\x93", "\xC3\xB3", // O, o
			"\xC5\x9A", "\xC5\x9B", // S, s
			"\xC5\xB9", "\xC5\xBA", // Z('), z(')
			"\xC5\xBB", "\xC5\xBC"  // Z(.), z(.)
		),
		'no' => array(
			"\x41", "\x61", // A, a
			"\x43", "\x63", // C, c
			"\x45", "\x65", // E, e
			"\x4C", "\x6C", // L, l
			"\x4E", "\x6E", // N, n
			"\x4F", "\x6F", // O, o
			"\x53", "\x73", // S, s
			"\x5A", "\x7A", // Z('), z(')
			"\x5A", "\x7A"  // Z(.), z(.)
		)
	);
	
	return str_replace($aCodes[$sFrom], $aCodes[$sTo], $sStr);
}


/**
 * Funkcja tworzy link do stony
 */
function phpSelf($aGet=array(), $aExtraVars=array(), $aOmit=array()) {
	$sPhpSelf = '';
	$aGetVars = array();
	$sPhpSelf = basename($_SERVER['PHP_SELF']);
	
	if (isset($_GET['preview'])) {
		$aExtraVars['preview'] = $_GET['preview']; 
	}
	
	if (!empty($_SERVER['QUERY_STRING'])) {
		$aVars = explode('&', $_SERVER['QUERY_STRING']);
		foreach ($aVars as $sVar) {
			$aTemp = explode('=', $sVar);
			$aGetVars[$aTemp[0]] = $_GET[$aTemp[0]];
		}
	}
	if (!empty($aGet)) {
		// tylko zmienne z $aGet beda dodane do Query String
		if (isset($aGet['lang']) && empty($aGet['lang'])) {
			unset($aGet['lang']);
		}
		$aGetVars = $aGet;
	}
	if (!empty($aExtraVars)) {
		$aGetVars = array_merge($aGetVars, $aExtraVars);
	}
	if (!empty($aGetVars)) {
		foreach ($aGetVars as $sName => $sVal) {
			if (!in_array($sName, $aOmit)) {
				$sGet .= $sName.'='.$sVal.'&amp;';
			}
		}
		$sPhpSelf .= '?'.substr($sGet, 0, strlen($sGet)-5);
	}
	return $sPhpSelf;
}

/**
 * Sprawdza czy użytkownik jest zalogowany
 * @return bool
 */
function isLoggedIn() {
	if (isset($_SESSION['wu_check']) && !empty($_SESSION['w_user']) &&
			isset($_COOKIE['wu_check'])) {
		return $_SESSION['wu_check'] === $_COOKIE['wu_check'];
	}
	return false;
} // end of isLoggedIn() funciton

	/**
	 * Logowanie automatyuczne
	 * @param $sUser
	 * @return unknown_type
	 */
	function doLogin($sUser) {
		// dolaczenie klasy UserLogin
		include_once('modules/m_konta/client/UserLogin.class.php');
		$oLogin = new UserLogin();
		// logowanie z ciasteczka
		if ($oLogin->doLoginFromCookie($sUser)) {
			$_COOKIE['wu_check'] = $_SESSION['wu_check'];
    }
	} // end of doLogin() method

  
  /**
   * Funkcja dodaje komunikat po stronie klienckiej
   * 
   * @global array $aConfig
   * @param string $sMsg
   * @param bool $bIsErr
   * @param bool $bRedirect
   * @param int $iCloseAfterSeconds - -1 lub '' lub 0 to długość wyś. zalezna od długości, jeśli >0 to wyświetla tyle sekund ile się poda
   */
  function setMessage($sMsg, $bIsErr=false,$bRedirect=false, $iCloseAfterSeconds = 0) {
    global $aConfig;

    $aConfig['_tmp']['message']['text'] = $sMsg;
    $aConfig['_tmp']['message']['error'] = $bIsErr;
    $aConfig['_tmp']['message']['close_afer_seconds'] = $iCloseAfterSeconds;
    if($bRedirect){
      $_SESSION['_tmp']['message']['text'] = $sMsg;
      $_SESSION['_tmp']['message']['error'] = $bIsErr;
      $_SESSION['_tmp']['message']['close_afer_seconds'] = $iCloseAfterSeconds;
    }
  } // end of setMessage() function


function setPageSubHeader($sStr) {
	global $aConfig;
	
	$aConfig['_tmp']['page_subheader'] = $sStr;
} // end of setPageSubHeader() function

function setPageHeader($sStr) {
	global $aConfig;
	
	$aConfig['_tmp']['page_header'] = $sStr;
} // end of setPageSubHeader() function


function setSubTitle($sStr) {
	global $aConfig;
	
	$aConfig['_tmp']['subtitle'] = $sStr;
} // end of setSubTitle() function

function setTitle($sStr) {
	global $aConfig;
	$aConfig['_tmp']['title'] = $sStr;
} // end of setTitle() function


function setKeywords($sStr) {
	global $aConfig;
	$aConfig['_tmp']['keywords'] = $sStr;
} // end of setKeywords() function


function setDescription($sStr) {
	global $aConfig;
	$aConfig['_tmp']['description'] = $sStr;
} // end of setDescription() function


/**
 * Funkcja ustawia w zmiennej $aConfig['_tmp']['fbog_tags'] znaczniki META Facebook OpenGraph
 *
 * @param array $aTags - lista tagow FB OpenGraph
 * @return	void
 */
function setFBOGTags($aTags) {
	global $aConfig;
	$aConfig['_tmp']['fbog_tags'] =& $aTags;
} // end of setFBOGTags() function


function addHeaderIncludes($sSrc) {
	global $aConfig;
	$aConfig['_tmp']['header_js_includes'][] = '<script type="text/javascript" src="'.$sSrc.'"></script>';
} // end of addHeaderIncludes() function


/**
 * Funkcja dodaje link do sciezki 'jestes tu'
 */
function addToPath($aItem) {
	global $aConfig;
	
	$aConfig['_tmp']['path'][] = array('link' => $aItem[0],
																		 'name' => $aItem[1],
																		 'target' => '');
} // end of addToPath() function

/**
 * Funkcja ustawia link w sciezce 'jestes tu' dla aktualnie przegladanej strony
 */
function setCurrentPagePathLink($sLink) {
	global $aConfig;
	
	$aConfig['_tmp']['path'][count($aConfig['_tmp']['path']) - 1]['link'] = $sLink;
} // end of setCurrentPagePathLink() function


/**
 * Funkcja tworzenia nowego linku po stronie klienckiej
 * 
 * @global array $aConfig
 * @param string $sPageLink
 * @param string $sAction
 * @param string $sItem
 * @param array $aParameters
 * @return string
 */
function createLink($sPageLink, $sAction='', $sItem='', $aParameters=array()) {
	
	$sPageLink .= '/';		
	$sPageLink .= $sAction != '' ? $sAction.',' : '';
	$sPageLink .= $sItem != '' ? link_encode($sItem).',' : '';
	foreach ($aParameters as $sKey => $mValue) {
		$sPageLink .= $sKey.':'.$mValue.',';
	}
	$sPageLink = substr($sPageLink, 0, -1).'.html';
	return $sPageLink; 
} // end of createLink() function


function createProductLink($iId, $sName='') {
	global $aConfig;
	
	$sPageLink .= '/';
	if($sName != ''){
		if(mb_strlen($sName, 'UTF-8') > 210) {
			$sPageLink .= mb_substr(link_encode($sName),0,210, 'UTF-8').',';
		} else {
			$sPageLink .= link_encode($sName).',';
		}
	}
	$sPageLink .= 'product'.$iId;
	$sPageLink .= '.html';
	return $sPageLink; 
} // end of createLink() function

function link_encode($sStr) {
	return preg_replace('/-{2,}/', '-', preg_replace('/[\s]+/', '-', preg_replace('/[^-_\. \w]/', '', changeCharcodes(strip_tags($sStr), 'utf8', 'no'))));
}

function encodeEmail($sEmail) {
	$returnValue = "";
	
	$length = strlen($sEmail);
	$l = $length -1;
	
	for ($i = 0; $i < $length; $i ++) {
		$char = $sEmail {$i};
		$asciivalue = ord($char);

		$returnValue .= $asciivalue;

		if ($i !== $l) {
			$returnValue .= "-";
		}
	}
	return $returnValue;
}

function obfuscate($sHrefB, $sEmail, $sHrefA, $sBody) {
	if ($sEmail == '') {
		return '';
	}
	return "<script type=\"text/javascript\">// <![CDATA[ \ndocument.write('<a".stripslashes($sHrefB)."href=\"' + deobfuscate('".encodeEmail('mailto:'.$sEmail)."') + '\"".stripslashes($sHrefA).">".(preg_match('/[-_a-z0-9]+(\.[-_a-z0-9]+)*@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]{2,6}/', $sBody) ? "' + deobfuscate('".encodeEmail($sBody)."') + '" : stripslashes($sBody))."</a>');\n // ]]></script>";
}

function obfuscateOutput($sStr) {
	return preg_replace('/<a([^>]*)href="mailto:([-_a-z0-9]+(\.[-_a-z0-9]+)*@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]{2,6})"([^>]*)>([^<]+)<\/a>/e', "obfuscate('\\1', '\\2', '\\5', '\\6')", $sStr);
}

/**
 * Funkcja pobiera i zwraca zdjecie dla podanych danych
 * 
 * @param	string	$sTable	- nazwa tabeli w bazie danych
 * @param	array	$aFields	- pola dla ktorych ma byc wybieranie
 * @param	string	$sImg	- rodzaj zdjecia ktore ma byc pobrane:
 * 												'__t_' - miniaturka
 * 												'' - male
 * 												'__b_' - duze
 * @param	bool	$bSmall	- true: jezeli nie ma zdjecia podanego w $sImg
 * 												i $sImg nie jest puste - pobranie malego zdjecia
 * @param	bool	$bOrderBy	- true: korzystaj z sortowania po kolejnosci
 * @return	array	- dane zdjecia aktualnosci
 */
function &getItemImage($sTable, $aFields, $sImg='', $bSmall=true, $bOrderBy=true) {
	global $aConfig;
	
	$sSql = "SELECT directory, photo, mime
					 FROM ".$sTable."
				 	 WHERE";
	foreach ($aFields as $sKey => $iValue) {
		$sSql .= " ".$sKey." = ".$iValue." AND";
	}
	$sSql = substr($sSql, 0, -4);
	$sSql .= $bOrderBy ? " ORDER BY order_by LIMIT 0, 1" : '';
	$aImg =& Common::GetRow($sSql);

	if (!empty($aImg)) {
		$sDir = '/'.$aConfig['common']['photo_dir'];
		if (!empty($aImg['directory'])) {
			$sDir .= '/'.$aImg['directory'];
		}
		if ($aSize = @getimagesize($aConfig['common']['base_path'].$sDir.'/'.$sImg.$aImg['photo'])) {
			$aImg = array('src' => $sDir.'/'.$sImg.$aImg['photo'],
										'width' => $aSize[0],
										'height' => $aSize[1],
										'mime' => $aImg['mime']);
		}
		elseif ($sImg != '' && $bSmall) {
			if ($aSize = @getimagesize($aConfig['common']['base_path'].$sDir.'/'.$aImg['photo'])) {
				$aImg = array('src' => $sDir.'/'.$aImg['photo'],
											'width' => $aSize[0],
											'height' => $aSize[1],
											'mime' => $aImg['mime']);
			}
		}
	}
	return $aImg;
} // end of getItemImage() function


/**
 * Funkcja pobiera i zwraca w tablicy wszystkie zdjecia dla podanych danych
 * 
 * @param	string	$sTable	- nazwa tabeli w bazie danych
 * @param	array	$aFields	- pola dla ktorych ma byc wybieranie
 * @param	integer	$iTrimTo	- do ilu znakow przycicac opis zdjec
 * @param	integer	$iStartFrom	- zacznij od zdjecia
 * @param	integer	$iQuantity	- ile zdjec wybrac
 * @param	bool	$bRandom	- true: losowa kolejnosc
 * @param	bool	$bBig	- true: pobieraj duze zdjecia - jezeli sa
 * @return	array ref	- tablica ze zdjeciami
 */
function &getItemImages($sTable, $aFields, $iTrimTo=0, $iStartFrom=0, $iQuantity=0, $bRandom=false, $bBig=true, $bThumb=false) {
	global $aConfig;
	$aLang =& $aConfig['lang']['mod_'.$aConfig['_tmp']['page']['module']];
	
	// pobranie zdjec
	$sSql = "SELECT directory, photo, description, author
					 FROM ".$sTable."
				 	 WHERE";
	foreach ($aFields as $sKey => $iValue) {
		$sSql .= " ".$sKey." = ".$iValue." AND";
	}
	$sSql = substr($sSql, 0, -4);
	$sSql .= " ORDER BY ".($bRandom ? "RAND()" : "order_by");
	if ($iQuantity > 0) {
		$sSql .= " LIMIT ".$iStartFrom.", ".$iQuantity;
	}
	$aImgs =& Common::GetAll($sSql);

	foreach ($aImgs as $iKey => $aImg) {
		$sDir = $aConfig['common']['photo_dir'];
		if (!empty($aImg['directory'])) {
			$sDir .= '/'.$aImg['directory'];
		}
		if ($bThumb && $aSize = @getimagesize($aConfig['common']['base_path'].
														$sDir.'/__t_'.$aImg['photo'])) {
			// miniaturka zdjecia zamiast sredniego
			$aImgs[$iKey]['src'] = '/'.$sDir.'/__t_'.$aImg['photo'];
			$aImgs[$iKey]['width'] = $aSize[0];
			$aImgs[$iKey]['height'] = $aSize[1];
		}
		elseif ($aSize = @getimagesize($aConfig['common']['base_path'].$sDir.'/'.
															 $aImg['photo'])) {
			$aImgs[$iKey]['src'] = '/'.$sDir.'/'.$aImg['photo'];
			$aImgs[$iKey]['width'] = $aSize[0];
			$aImgs[$iKey]['height'] = $aSize[1];
		}
		else {
			$aSize = array();
		}
		if (!empty($aSize)) {
			// duze zdjecie
			if ($bBig && $aSize = @getimagesize($aConfig['common']['base_path'].$sDir.'/__b_'.
																 $aImg['photo'])) {
				$aImgs[$iKey]['big']['src'] = '/'.$sDir.'/__b_'.$aImg['photo'];
				$aImgs[$iKey]['big']['width'] = $aSize[0];
				$aImgs[$iKey]['big']['height'] = $aSize[1];
			}
			if (!empty($aImgs[$iKey]['description'])) {
				if ($iTrimTo > 0) {
					// skrocenie opisow zdjec do podanej liczby znakow
					$aImgs[$iKey]['short_description'] = trimString($aImgs[$iKey]['description'], $iTrimTo);
				}
				$aImgs[$iKey]['decoded_description'] = htmlentities($aImgs[$iKey]['description'].($aImgs[$iKey]['author'] != '' ? ' | '.$aLang['fot'].' '.$aImgs[$iKey]['author'] : ''), ENT_QUOTES, 'UTF-8');					
				// JS
				$aImgs[$iKey]['js_description'] = base64_encode($aImgs[$iKey]['description']);
			}
		}
		else {
			unset($aImgs[$iKey]);
		}
	}
	return $aImgs;
} // end of getItemImages() function


/**
 * Metoda pobiera i zwraca w tablicy wszystkie pliki dla podanych danych
 * 
 * @param	string	$sTable	- nazwa tabeli w bazie danych
 * @param	array	$aFields	- pola dla ktorych ma byc wybieranie
 * @return	array ref	- tablica z plikami
 */
function &getItemFiles($sTable, $aFields) {
	global $aConfig;
	
	// pobranie plikow
	$sSql = "SELECT id, directory, filename, description, name
					 FROM ".$sTable."
				 	 WHERE";
	foreach ($aFields as $sKey => $iValue) {
		$sSql .= " ".$sKey." = ".$iValue." AND";
	}
	$sSql = substr($sSql, 0, -4);
	$sSql .= " ORDER BY order_by";
	$aFiles =& Common::GetAll($sSql);

	foreach ($aFiles as $iKey => $aFile) {
		$sDir = $aConfig['common']['file_dir'];
		if (!empty($aFile['directory'])) {
			$sDir .= '/'.$aFile['directory'];
		}
		if (($iSize = @filesize($aConfig['common']['base_path'].$sDir.'/'.
													 	$aFile['filename'])) !== false) {
			if ($iSize > 1024 * 1024) {
				$sSize = number_format($iSize / (1024 * 1024), 2, ',', '');
				$sSize = preg_replace('/,?0+$/', '', $sSize).' MB';
			}
			elseif ($iSize > 1024) {
				$sSize = number_format($iSize / 1024, 2, ',', '');
				$sSize = preg_replace('/,?0+$/', '', $sSize).' KB';
			}
			else {
				$sSize = $iSize.' B';
			}
			
			$aFiles[$iKey]['src'] = '/download,'.
			base64_encode($sTable.'#'.serialize($aFields).'#'.$aFile['id']).'.html';
			$aFiles[$iKey]['size'] = $sSize;
			// rozszerzenie pliku
			$aFiles[$iKey]['ext'] = strtolower(substr($aFile['filename'], strpos($aFile['filename'], '.') + 1));
		}
		else {
			unset($aFiles[$iKey]);
		}
	}
	return $aFiles;
} // end of getItemFiles() function


/**
 * Funkcja pobiera i zwraca w tablicy wszystkich autorow dla zadanych danych
 * 
 * @param	string	$sTable	- nazwa tabeli w bazie danych
 * @param	array	$aFields	- pola dla ktorych ma byc wybieranie
 * @param	bool	$bGetLinks	- true: tworz linki
 * @return	array ref	- tablica z autorami
 */
//function &getItemAuthors($sTable, $aFields, $bGetLinks=false) {
//	global $aConfig;
//	
//	// pobranie autorow
//	$sSql = "SELECT B.id AS aid, B.title, B.name, B.surname, B.description
//					 FROM ".$sTable." AS A
//					 JOIN ".$aConfig['tabls']['prefix']."authors AS B
//					 ON B.id = A.author_id
//				 	 WHERE";
//	foreach ($aFields as $sKey => $iValue) {
//		$sSql .= " ".$sKey." = ".$iValue." AND";
//	}
//	$sSql = substr($sSql, 0, -4);
//	$sSql .= " ORDER BY A.order_by";
//	$aItems =& Common::GetAll($sSql);
//	
//	if ($bGetLinks) {
//		$sPageLink = getModulePageLink('m_autorzy');
//		foreach ($aItems as $iKey => $aItem) {
//			$aItems[$iKey]['link'] = createLink($sPageLink,
//																					'id'.$aItem['aid'],
//																					$aItem['title'].' '.$aItem['name'].' '.$aItem['surname']);
//		}
//	}
//
//	return $aItems;
//} // end of getItemAuthors() function


/**
 * Funkcja pobiera link do strony autorow
 * 
 * @return	string
 */
function getModulePageLink($sModule, $sOption='') {
	global $aConfig;
	
	$sSql = "SELECT A.symbol
					 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
					 JOIN ".$aConfig['tabls']['prefix']."modules AS B
					 ON A.module_id = B.id
					 WHERE B.symbol = '".$sModule."' AND ".
					 			 ($sOption != '' ? " moption = '".$sOption."' AND" : '')."
					 			 A.language_id = ".$_GET['lang_id'];
	return '/'.Common::GetOne($sSql);
} // end of getAuthorsPageLink() function


/**
 * Funkcja pobiera id strony modulu o podanym symbolu lub opcji
 * 
 * @return	string
 */
function getModulePageId($sModule, $sOption='') {
	global $aConfig;
	
	$sSql = "SELECT A.id
					 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
					 JOIN ".$aConfig['tabls']['prefix']."modules AS B
					 ON A.module_id = B.id
					 WHERE B.symbol = '".$sModule."' AND ".
					 			 ($sOption != '' ? " moption = '".$sOption."' AND" : '')."
					 			 A.language_id = ".$_GET['lang_id'];
	return (int) Common::GetOne($sSql);
} // end of getModulePageId() function


/**
 * Funkja pobiera i zwraca tablice z danymi dla pola SELECT zawierajaca
 * liste ston modulu $iMId dla aktualnej wersji jez.
 * 
 * @param	mixed (integer, string)	$mModule	- Id lub symbol modulu
 * @return	array
 */
function &getModulePages($mModule) {
	global $aConfig;
	$aPages = array();
	
	$iMId = $mModule;
	if (!is_numeric($mModule)) {
		// podano symbol modulu - okreslenie Id modulu
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE symbol = '".$mModule."'";
		$iMId =& Common::GetOne($sSql);
	}
	
	// pobranie wszystkich stron modulu
	$sSql = "SELECT id AS value, name AS label
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".$iMId." AND
					 			 language_id = ".$_GET['lang_id'];
	return Common::GetAll($sSql);
} // end of getModulePages function


/**
 * Funkcja pobiera liste wojewodztw
 * 
 * @param	integer	$iLangId	- Id wersji jezykowej
 * @param	bool	$bId	- true: jako wartosc pobierz ID; false: pobierz symbol
 * @return	array ref	- lista wojewodztw w formacie dla pola SELECT
 */
function &getProvincesList($iLangId, $bId=true) {
	global $aConfig;
	$sSql = "SELECT ".($bId ? 'id' : 'symbol')." AS value, name AS label
					 FROM ".$aConfig['tabls']['prefix']."provinces
					 ORDER BY name";
	return Common::GetAll($sSql);
} // end of getProvincesList() function


function add_slashes($sStr) {
	return get_magic_quotes_gpc() ? $sStr : addslashes($sStr);
} // end of add_slashes() method


/**
 * Funkcja sprawdza czy uzytkownik wykupil dostep do artykulu
 * 
 * @param	integer	$iId	- Id artykulu
 * @return	bool	- true: wykupil; false: nie wykupil
 */
function hasBoughtArticle($iId) {
	global $aConfig;
	
	if (!isLoggedIn()) return false;
	
	$sSql = "SELECT article_id
					 FROM ".$aConfig['tabls']['prefix']."users_accounts_articles
					 WHERE user_id = ".$_SESSION['w_user']['id']." AND
					 			 article_id = ".$iId;
	return (int) Common::GetOne($sSql) > 0;
} // end of hasBoughtArticle() function


/**
 * Funkcja sprawdza czy uzytkownik wykupil dostep do porady
 * 
 * @param	integer	$iId	- Id porady
 * @param	integer	$iPaid	- -1: oplacona lub nie
 * 													 0: nieoplacona
 * 													 1: oplacona
 * @return	bool	- true: wykupil; false: nie wykupil
 */
function hasBoughtAdvice($iId, $iPaid=1) {
	global $aConfig;
	
	if (!isLoggedIn()) return false;
	
	$sSql = "SELECT advice_id
					 FROM ".$aConfig['tabls']['prefix']."users_accounts_advices
					 WHERE user_id = ".$_SESSION['w_user']['id']." AND
					 			 advice_id = ".$iId;
	switch ($iPaid) {
		case 0:
			$sSql .= " AND paid = '0'";
		break;
		
		case 1:
			$sSql .= " AND paid = '1'";
		break;
	}
	return (int) Common::GetOne($sSql) > 0;
} // end of hasBoughtAdvice() function


/**
 * Funkcja pobiera dane systemu DotPay
 *
 * @return	array ref	- dane dotpay
 */
function &getDotPayData() {
	global $aConfig;

	$sSql = "SELECT pdata
					 FROM ".$aConfig['tabls']['prefix']."orders_payment_types
					 WHERE ptype = 'allpay'";
	return unserialize(base64_decode(Common::GetOne($sSql)));
} // end of getDotPayData() function


/**
 * Funkcja sprawdza czy uzytkownik o podanym Id moze miec bloga
 * 
 * @param	integer	$iId	- Id profilu
 * @return	bool	- true: istnieje; false: nie istnieje
 */
function hasBlog($iId) {
	global $aConfig;
			
	$sSql = "SELECT has_blog
					 FROM ".$aConfig['tabls']['prefix']."users_accounts
					 WHERE id = ".$iId;
	return intval(Common::GetOne($sSql)) == 1;
} // end of hasBlog() function


/**
 * Funkcja pobiera i zwraca nazwe uzytkownika
 * 
 * @param	integer	$iId	- Id profilu
 * @return	string	- nazwa uzytkownika
 */
function getUserName($iUId) {
	global $aConfig;
	
	$sSql = "SELECT name, surname, login
					 FROM ".$aConfig['tabls']['prefix']."users_accounts
					 WHERE id = ".$iUId;
	$aUser =& Common::GetRow($sSql);
	return $aUser['name'].' '.$aUser['surname'];
} // end of getUserName() method


/**
 * Funkcja pobiera i zwraca nazwe blogu
 * 
 * @param	integer	$iId	- Id profilu
 * @return	string	- login uzytkownika
 */
function getBlogName($iUId) {
	global $aConfig;
	
	$sSql = "SELECT blog_name
					 FROM ".$aConfig['tabls']['prefix']."users_accounts
					 WHERE id = ".$iUId;
	return Common::GetOne($sSql);
} // end of getBlogName() method


/**
 * Funkcja pobiera Id stron modulu
 * 
 * @param	string	$sModule	- symbol modulu
 * @return	array ref
 */
function &getModulePagesIds($sModule) {
	global $aConfig;
	
	$sSql = "SELECT A.id
					 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
					 JOIN ".$aConfig['tabls']['prefix']."modules AS B
					 ON A.language_id = ".$_GET['lang_id']." AND
					 		A.module_id = B.id
					 WHERE B.symbol = '".$sModule."'";
	return Common::GetCol($sSql);
	
} // end of getModulePagesIds() method

/**
 * Funkcja pobiera tresc komunikatu strony 
 * 
 * @param	string $sSymbol		-	symbol komunikatu
 * @return 	string				-	tresc komunikatu
 */	
function getWebsiteMessage($sSymbol){
	global $aConfig;
	
	$sSql = "SELECT content FROM ".$aConfig['tabls']['prefix']."website_messages WHERE symbol = '".$sSymbol."'";
	return Common::GetOne($sSql);
}

function printWebsiteMessage($sMsg,$aSymbols=array(),$aValues=array()){
	$aToFind=array();
	foreach($aSymbols as $iKey => $sSmb)
		$aToFind[$iKey]='{'.$sSmb.'}';
	return str_replace($aToFind,$aValues,$sMsg);
}


/**
 * Funkcja pobiera domyslna konfiguracje modulu z pliku $sXMLFile
 * 
 * @param	string	$sXMLFile	- plik XML z konfiguracja
 * @return	array	- konfiguracja
 */
function getModuleConfig($sXMLFile) {
		// dolaczenie klasy XML/Unserializer
	require_once ("XML/Unserializer.php");
	$aData = array();
	$aOptions = array(
		'complexType'       => 'array'
	);
	$pUS = new XML_Unserializer($aOptions);
	
	if (file_exists($sXMLFile)) {
		$pResult = $pUS->unserialize($sXMLFile, true);
		if (!PEAR::IsError($pResult)) {
      $aData =& $pUS->getUnserializedData();
    }
	}
	return $aData;
}// end of setConfig() function

/**
 * Funkcja pobiera male okladki produktu
 */
function &getProductThumb($iId){
	global $aConfig;
	$sFileName = $aConfig['common']['base_path']."images/okladki/".$iId.".gif";

	if (file_exists($sFileName)) {
		$sImage = "/images/okladki/".$iId.".gif";
	} else {
		$sImage = "/images/gfx/blank.gif";
	}
	
	return $sImage;
}


/**
 * Funkcja sprawdza czy strona o podanym Id jest opublikowana - w zaleznosci od tego
 * czy jest podglad czy tez nie
 * 
 * @param	integer	$iPId	- Id strony
 * @return	bool
 */
function pageIsPublished($iPId) {
	global $aConfig;
	
	if (isPreviewMode()) return true;
	// sprawdzenie czy strona jest opublikowany
	$sSql = "SELECT id
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE id = ".$iPId." AND
					 			 published = '1'";
	return ((double) Common::GetOne($sSql)) == $iPId;
} // end of pageIsPublished() function

  /**
   * Rekurencyjna funkcja zwracajac Id wszystkich podstron
   * typu $iType strony o podanym Id $iStartFrom 
   *
   * @param	array	$aItems	- tablica z nieposegregowanymi stronami
   * @param	integer	$iStartFrom	- ID rodzica od ktorego rozpoczac
   * @param	string	$iType	- typ podstron
   * @return	array	- uporzadkowana tablica z Id podstron
   */
  function getSubpages(&$aItems, $iStartFrom=0, $sType) {
		global $aConfig;
		$aTree		= array();
		
  	if (!empty($aItems[$iStartFrom])) {
			foreach($aItems[$iStartFrom] as $iKey => $aItem) {
				if ($aItem['mtype'] == $sType) {
					$aTree[] = $aItem['id'];
				}
				if (isset($aItems[$aItem['id']])) {
					$aTree = array_merge($aTree,
															 getSubpages($aItems,
      																						 $aItem['id'],
      																						 $sType));
				}
			}
		}
		return $aTree;
  } // end getSubpages() function
  
  	
	/**
	 * Funkcja pobiera id modulu o podanym symbolu
	 * 
	 * @return	string
	 */
	function getModuleId($sSymbol) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE symbol = '".$sSymbol."'";
		return (int) Common::GetOne($sSql);
	} // end of getModuleId() function
	


function isbn2plain($sIsbn){
		return preg_replace('/[^0-9a-zA-Z]/', '', $sIsbn); 
		
	}
	
	  function getDotPayId() {
  	global $aConfig;
  	
  	$sSql = "SELECT pdata
  					 FROM ".$aConfig['tabls']['prefix']."orders_payment_types
  					 WHERE ptype = 'dotpay'";
  	$aData = unserialize(base64_decode(Common::GetOne($sSql)));
  	return $aData['id'];
  } // end of getDotPayId() method
  
  
  function getDotPayPin() {
  	global $aConfig;
  	
  	$sSql = "SELECT pdata
  					 FROM ".$aConfig['tabls']['prefix']."orders_payment_types
  					 WHERE ptype = 'dotpay'";
  	$aData = unserialize(base64_decode(Common::GetOne($sSql)));
  	return $aData['pin'];
  } // end of getDotPayPin() method
	
  function restoreCartFromDatabase($bSetMsg=false,$bLogin=false){
 	global $aConfig;
 		$sSql = "SELECT *
							FROM ".$aConfig['tabls']['prefix']."orders_cart
							WHERE user_id = ".$_SESSION['w_user']['id'];
		$aItems = Common::GetAll($sSql);
		if(!empty($aItems)) {
			if($bSetMsg) {
				$_SESSION['notification']=9;
			}
			include_once('modules/m_zamowienia/client/Common.class.php');
   	 $oOrders = new Common_m_zamowienia('','','','');
			foreach($aItems as $iKey=>$aProduct) {
				//for($i=1;$i<=intval($aProduct['quantity']);$i++){
					$oOrders->addProduct($aProduct['product_id'],false,$aProduct['quantity'],$bLogin);
				//}
			}
		}
  }
  
  function storeAddToDB($iId,$iQuantity) {
  	global $aConfig;
  	$sSql = "SELECT quantity
							FROM ".$aConfig['tabls']['prefix']."orders_cart
							WHERE user_id = ".$_SESSION['w_user']['id']." AND product_id =".$iId;
		if(Common::GetOne($sSql) > 0) {
			$aValues = array(
				'quantity' => $iQuantity
			);
			Common::Update($aConfig['tabls']['prefix']."orders_cart",$aValues,"user_id = ".$_SESSION['w_user']['id']." AND product_id =".$iId);
		} else {
			$aValues = array(
				'user_id' => $_SESSION['w_user']['id'],
				'product_id' => $iId,
				'quantity' => $iQuantity
			);
			Common::Insert($aConfig['tabls']['prefix']."orders_cart",$aValues,'',false);
		}
  }
  
  
  function restoreCartFromCookie(){
  	global $aConfig;
  	$sCart = base64_decode($_COOKIE['cart']);
		$aData = explode(";", $sCart);
		include_once('modules/m_zamowienia/client/Common.class.php');
    $oOrders = new Common_m_zamowienia('','','','');
    //remove cart items limit
    $aData = array_slice($aData, 0, $aConfig['common']['max_cart_items'], true);
		foreach($aData as $sItem) {
			$aProduct = explode('_',$sItem,2);
		//	for($i=1;$i<=intval($aProduct[1]);$i++){
				$oOrders->addProduct(intval($aProduct[0]),false,intval($aProduct[1]));
		//	}
		}
  }
  
  function serializeCartToCookie() {
		global $aConfig;
		$sCart='';
		if(!empty($_SESSION['wu_cart']['products'])) {
			foreach($_SESSION['wu_cart']['products'] as $iKey=>$aProduct) {
				$sCart.=$aProduct['id'].'_'.$aProduct['quantity'].';';
			}
		}
		if(!empty($sCart)) {
			$sCart=substr($sCart,0,-1);
		}
		$sEncCart = base64_encode($sCart);
		AddCookie("cart", $sEncCart, time() + 60*60*24*366);
	}
  
	
	function formatDateClient($sDate, $iMode=1) {
	if ($iMode == 1) {
		if (preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $sDate, $aMatches)) {
			return $aMatches[3].'.'.$aMatches[2].'.'.$aMatches[1];
		}
	}
	else {
		if (preg_match('/^(\d{2})-(\d{2})-(\d{4})$/', $sDate, $aMatches)) {
			return $aMatches[3].'-'.$aMatches[2].'-'.$aMatches[1];
		}
	}
	return '';
}

/**
 * Funkcja tworzy link do autora
 *
 * @param string $sName
 * @param integer $iPage
 * @return string 
 */
function getAuthorLink($sName, $iPage=0) {
	return '/autor/'.urlencode(trim(strip_tags($sName))).($iPage>0?','.$iPage:'');
} //end of getAuthorLink() function


/**
 * Funkcja tworzy link do autora
 *
 * @param string $sName
 * @param integer $iPage
 * @return string 
 */
function getTagLink($sName, $iPage=0) {
	return '/tag/'.urlencode(trim($sName)).($iPage>0?','.$iPage:'');
} //end of getAuthorLink() function


/**
 * Funkcja tworzy link do autora
 *
 * @param string $sName
 * @param integer $iPage
 * @return string 
 */
function getPublisherLink($sName, $iPage=0) {
	$sName = str_replace('&', ';amp;', $sName);
	$sName = urlencode(trim($sName));
	$sName = str_replace(array('%2F','%5C'), array('%252F','%255C'), $sName);
	return '/wydawnictwo/'.$sName.($iPage>0?','.$iPage:'');
} //end of getAuthorLink() function


/**
 * Funkcja tworzy link do autora
 *
 * @param string $sName
 * @param integer $iPage
 * @return string 
 */
function getSeriesLink($sPublisherName, $sSeriesName, $iPage=0) {
	$sPublisherName = str_replace('&', ';amp;', $sPublisherName);
	$sPublisherName = urlencode(trim($sPublisherName));
	$sPublisherName = str_replace(array('%2F','%5C'), array('%252F','%255C'), $sPublisherName);
	$sSeriesName = urlencode(trim($sSeriesName));
	$sSeriesName = str_replace(array('%2F','%5C'), array('%252F','%255C'), $sSeriesName);
	return '/wydawnictwo/'.$sPublisherName.'/'.$sSeriesName.($iPage>0?','.$iPage:'');
} //end of getAuthorLink() function
?>