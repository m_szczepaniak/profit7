<?php
use LIB\Assets\additionalHead;
use table\keyValue;

/**
 * Klasa actory do obslugi serwisu
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Factory {

  // Id aktualnie przegladanej strony
  var $iPId;

  // Id wersji jezykowej
  var $iLangId;

  // obszary
  var $aAreas;

  // glowny obszar podstron
  var $aMainArea;

  // obszary po renderowaniu
  var $aRenderedAreas;
  
  var $bRefUse = false;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Factory() {
		global $aConfig, $pSmarty, $oCache;

		if (isset($_GET['abpid']) && $_GET['abpid'] > 0) {
			$this->setAdditionalMarkupCookie();
		}
		


		if (isset($_GET['test_module'])) {
			$aConfig['_tmp']['page']['module'] = 'm_szukaj2';
		}


    $iCardBoxId = $this->getIdCardBox();
    if (intval($iCardBoxId) > 0) {
      $aConfig['header_js'][] = 'var iCardBoxId = '.intval($iCardBoxId).';';
    }
    
		// dowiazanie do Smartow podstawowych zmiennych serwisu
		$this->assignToSmarty();

		if (isset($aConfig['_tmp']['page'])) {
			$this->iPId =& $aConfig['_tmp']['page']['id'];
		}
		else {
			$this->iPId = 0;
		}
		$this->iLangId =& $_GET['lang_id'];
		$this->aAreas =& $aConfig['_tmp']['areas'];

		if (isset($aConfig['_tmp']['main_area'])) {
			$this->aMainArea =& $aConfig['_tmp']['main_area'];
		}
		// dowiazanie tablicy z wersja jezykowa do Smartow
		$pSmarty->assign_by_ref('aLang', $aConfig['lang']);
		
		if(isset($_GET['src']) && $_GET['src'] != '') {
			$sSource = base64_decode($_GET['src']);
			$aSource = explode(":", $sSource);
			$_SESSION['source']['source'] = $aSource[0];
			$_SESSION['source']['id'] = $aSource[1];
		}
		
		$aConfig['_tmp']['search_symbol'] = getModulePageLink('m_szukaj');
		$aConfig['_tmp']['accounts_symbol'] = getModulePageLink('m_konta');
		$aConfig['_tmp']['cart_symbol'] = getModulePageLink('m_zamowienia');
		$aConfig['_tmp']['repository_symbol'] = getModulePageLink('m_zamowienia', 'przechowalnia');
    
		// odtwarzanie koszyka
		if(empty($_SESSION['wu_cart']['products'])) {
			if(isLoggedIn()) {
				restoreCartFromDatabase();
			} else {
				if(isset($_COOKIE['cart'])) {
					restoreCartFromCookie();
				}
			}
		} else {
      //remove cart items limit
      $_SESSION['wu_cart']['products'] = array_slice($_SESSION['wu_cart']['products'], 0, $aConfig['common']['max_cart_items'], true);
    }
		// sprawdzenie czy wyswietlac kanal testowy dla Platnosci.pl
		$aConfig['common']['platosci_pl_show_test_mode'] = false;
		if (isset($_SESSION['w_user']['email']) && $_SESSION['w_user']['email'] == $aConfig['common']['platosci_pl_test_mode_email']) {
			$aConfig['common']['platosci_pl_show_test_mode'] = true;
		}
		
		if (isset($aConfig['_tmp']['page']) && !empty($this->aMainArea)) {
			// aktualnie przegladana strona jest podstrona
			$sLangFile = $aConfig['common']['cms_dir'].'modules/'.$aConfig['_tmp']['page']['module'].'/lang/'.$_GET['lang'].(!empty($aConfig['_tmp']['page']['moption']) ? '_'.$aConfig['_tmp']['page']['moption'] : '').'.php';
			if (file_exists($sLangFile)) {
				// dolaczenie pliku jezykowego modulu i klasy modulu strony
				include_once($sLangFile);
			}
			$sFile = $aConfig['common']['cms_dir'].'modules/'.$aConfig['_tmp']['page']['module'].'/client/Module'.(!empty($aConfig['_tmp']['page']['moption']) ? '_'.$aConfig['_tmp']['page']['moption'] : '').'.class.php';

			if (file_exists($sFile)) {
				include_once($sFile);
				$oModule = new Module($aConfig['_tmp']['page']['module'],
															$aConfig['_tmp']['page']['id'],
															$aConfig['_tmp']['page']['symbol']);
				$this->aMainArea['content'] =& $oModule->getContent();
			}	
		}
		// renderowanie obszarow i boksow serwisu
		$this->renderAreas();
		
		// strona glowna
		// znaczniki META Facebook Open Graph
		if ($aConfig['_settings']['site']['fbog_sitename'] != '' && $aConfig['_settings']['site']['fbog_admins'] != '' && $aConfig['_settings']['site']['fbog_image'] != '') {
			$aFBOGTags = array(
				'og:title' => $aConfig['_tmp']['title']==''?$aConfig['_settings']['site']['title']:$aConfig['_tmp']['title'],
				'og:type' => 'company',
				'og:url' => $aConfig['common']['base_url_http_no_slash'].$_SERVER['REQUEST_URI'],
				'og:description' => $aConfig['_tmp']['description']==''?$aConfig['_settings']['site']['description']:$aConfig['_tmp']['description']
			);
			if ($aConfig['_settings']['site']['fbog_image'] != '') {
				$aFBOGTags['og:image'] = $aConfig['_settings']['site']['fbog_image'];
			}
			if ($aConfig['_settings']['site']['fbog_sitename'] != '') {
				$aFBOGTags['og:site_name'] = $aConfig['_settings']['site']['fbog_sitename'];
			}
			if ($aConfig['_settings']['site']['fbog_admins'] != '') {
				$aFBOGTags['fb:admins'] = $aConfig['_settings']['site']['fbog_admins'];
			}
			if ($aConfig['_settings']['site']['fbog_page_id'] != '') {
				$aFBOGTags['fb:page_id'] = $aConfig['_settings']['site']['fbog_page_id'];
			}
			if ($aConfig['_settings']['site']['fbog_app_id'] != '') {
				$aFBOGTags['fb:app_id'] = $aConfig['_settings']['site']['fbog_app_id'];
			}
			if ($aConfig['_settings']['site']['fbog_latitude'] != '' && $aConfig['_settings']['site']['fbog_longitude'] != '') {
				$aFBOGTags['og:latitude'] = $aConfig['_settings']['site']['fbog_latitude'];
				$aFBOGTags['og:longitude'] = $aConfig['_settings']['site']['fbog_longitude'];
			}
			if ($aConfig['_settings']['site']['fbog_street_address'] != '') {
				$aFBOGTags['og:street-address'] = $aConfig['_settings']['site']['fbog_street_address'];
			}
			if ($aConfig['_settings']['site']['fbog_locality'] != '') {
				$aFBOGTags['og:locality'] = $aConfig['_settings']['site']['fbog_locality'];
			}
			if ($aConfig['_settings']['site']['fbog_region'] != '') {
				$aFBOGTags['og:region'] = $aConfig['_settings']['site']['fbog_region'];
			}
			if ($aConfig['_settings']['site']['fbog_latitude'] != '') {
				$aFBOGTags['og:latitude'] = $aConfig['_settings']['site']['fbog_latitude'];
			}
			if ($aConfig['_settings']['site']['fbog_postal_code'] != '') {
				$aFBOGTags['og:postal-code'] = $aConfig['_settings']['site']['fbog_postal_code'];
			}
			if ($aConfig['_settings']['site']['fbog_country_name'] != '') {
				$aFBOGTags['og:country-name'] = $aConfig['_settings']['site']['fbog_country_name'];
			}
			if ($aConfig['_settings']['site']['email'] != '') {
//				$aFBOGTags['og:email'] = $aConfig['_settings']['site']['email'];
			}
			if ($aConfig['_settings']['site']['fbog_phone_number'] != '') {
				$aFBOGTags['og:phone_number'] = $aConfig['_settings']['site']['fbog_phone_number'];
			}
			if ($aConfig['_settings']['site']['fbog_fax_number'] != '') {
				$aFBOGTags['og:fax_number'] = $aConfig['_settings']['site']['fbog_fax_number'];
			}
			setFBOGTags($aFBOGTags);
		}
		
		// dowiazywanie wszystkich zmiennych do Smartow
		$this->assignToSmarty2();
		
	} // end Factory() function

	/**
	 *  Cookie
	 */
	public function setAdditionalMarkupCookie() {
		$oKeyValue = new keyValue('mark_up_cookie');
		$cookieName = $oKeyValue->getByIdDestKey('1', 'cookie_name');
		$lifeTime = $oKeyValue->getByIdDestKey('1', 'lifetime');

		if (!isset($_COOKIE[$cookieName])) {
			$expire = time()+60*60*24*$lifeTime;
			AddCookie($cookieName, 'nn', $expire);
			$_COOKIE[$cookieName] = 'nn';
		}
	}


  /**
   * Metoda pobiera boks za pomocą ajaxa
   * 
   * @param int $iBoxId
   * @param int $iLangId
   * @return string
   */
   public function getBoxDivs($iBoxId) {
    
    $sHTML = $this->BoxIntoDiv($iBoxId, '');
    if ($this->bRefUse == false) {
      $this->bRefUse = true;
    }
    return $sHTML;
   }// end of getBoxDivs() method
   
   
   /**
    * Metoda oplata boks divem z id boksu
    * 
    * @param type $sContent
    * @param type $iBoxId
    * @return type
    */
   private function BoxIntoDiv($iBoxId, $sContent) {
    return '<div id="box_id_'.$iBoxId.'">'.$sContent.'</div>'; 
   }// end of BoxIntoDiv() method
   
   
	/**
	 * Metoda renderuje wszystkie obszary i boksy aktywne dla aktualnej strony
	 *
	 * @return	void
	 */
	function renderAreas() {
		global $aConfig, $pSmarty, $oCache;
		$sAreaContent = '';
		$aAreas = array();

		$aBoxes =& $this->getBoxes();
    
		// renderowanie boksow dla kazdego obszaru
        if ($this->aAreas) {
            foreach ($this->aAreas as $iKey => $aItem) {
                if (isset($aBoxes[$aItem['id']])) {
                    // pobieramy normalnie boks
                    $aAreaContent = array();
                    // jezeli obszar posiada bloki - renderowanie obszaru
                    foreach ($aBoxes[$aItem['id']] as $aBox) {
                        if ($aBox['omit_memcached'] == '1' && S_MEMCACHED === true) {
                            // boks pobierany przez ajax
                            $aAreaContent[$aBox['id']] = $this->getBoxDivs(intval($aBox['id']));
                        } else {
                            if (!$aConfig['common']['caching'] ||
                                !$this->boxIsCacheable(intval($aBox['cacheable'])) ||
                                !$aAreaContent[$aBox['id']] = $oCache->get($this->getBoxCacheName($aItem['id'], $aBox, intval($aBox['cache_per_page'])))
                            ) {
                                $pSmarty->assign('iBoxId', $aItem['id'] . '_' . $aBox['id']);
                                // boks nie jest cache'owalny,
                                // cacheowanie jest wylaczone, lub nie istnieje zcache'owana
                                // podstrona
                                switch ($aBox['btype']) {
                                    case '1':
                                        // boks menu
                                        $aBox['module'] = 'm_menu';

                                        $sFile = $aConfig['common']['cms_dir'] . 'boxes/' . $aBox['module'] . '/client/Box.class.php';
                                        $sLangFile = $aConfig['common']['cms_dir'] .
                                            'boxes/' . $aBox['module'] . '/lang/' . $_GET['lang'] . '.php';
                                        $sClass = 'Box_' . $aBox['module'];
                                        if (file_exists($sFile)) {
                                            if (!class_exists($sClass)) {
                                                include_once($sFile);
                                            }
                                            if (file_exists($sLangFile)) {
                                                include_once($sLangFile);
                                            }
                                            $oBox = new $sClass($aBox);
                                            $aAreaContent[$aBox['id']] = $this->BoxIntoDiv($aBox['id'], $oBox->getContent());
                                            unset($oBox);
                                        }
                                        break;

                                    case '4':
                                        // boks menu - pierwszy poziom
                                        $aBox['module'] = 'm_menu_pierwszy_poziom';

                                        $sFile = $aConfig['common']['cms_dir'] . 'boxes/' . $aBox['module'] . '/client/Box.class.php';
                                        $sLangFile = $aConfig['common']['cms_dir'] .
                                            'boxes/' . $aBox['module'] . '/lang/' . $_GET['lang'] . '.php';
                                        $sClass = 'Box_' . $aBox['module'];
                                        if (file_exists($sFile)) {
                                            if (!class_exists($sClass)) {
                                                include_once($sFile);
                                            }
                                            if (file_exists($sLangFile)) {
                                                include_once($sLangFile);
                                            }
                                            $oBox = new $sClass($aBox);
                                            $aAreaContent[$aBox['id']] = $this->BoxIntoDiv($aBox['id'], $oBox->getContent());
                                            unset($oBox);
                                        }
                                        break;

                                    case '2':
                                        // boks modulu
                                        $sFile = $aConfig['common']['cms_dir'] .
                                            'boxes/' . $aBox['module'] . '/client/Box' . (!empty($aBox['moption']) ? '_' . $aBox['moption'] : '') . '.class.php';
                                        $sLangFile = $aConfig['common']['cms_dir'] .
                                            'boxes/' . $aBox['module'] . '/lang/' . $_GET['lang'] . (!empty($aBox['moption']) ? '_' . $aBox['moption'] : '') . '.php';
                                        $sClass = 'Box_' . $aBox['module'] .
                                            (!empty($aBox['moption']) ? '_' . $aBox['moption'] : '');
                                        if (file_exists($sFile)) {
                                            if (!class_exists($sClass)) {
                                                include_once($sFile);
                                            }
                                            if (file_exists($sLangFile)) {
                                                include_once($sLangFile);
                                            }
                                            $oBox = new $sClass($aBox);
                                            $aAreaContent[$aBox['id']] = $this->BoxIntoDiv($aBox['id'], $oBox->getContent());
                                            unset($oBox);
                                        }
                                        break;

                                    case '3':
                                        // boks pusty
                                        $sFile = $aConfig['common']['cms_dir'] .
                                            'boxes/e_' . $aBox['symbol'] . '/client/Box.class.php';
                                        $sLangFile = $aConfig['common']['cms_dir'] .
                                            'boxes/e_' . $aBox['symbol'] . '/lang/' . $_GET['lang'] . '.php';
                                        $sClass = 'Box_e_' . $aBox['symbol'];

                                        if (file_exists($sFile)) {
                                            if (!class_exists($sClass)) {
                                                include_once($sFile);
                                            }
                                            if (file_exists($sLangFile)) {
                                                include_once($sLangFile);
                                            }
                                            $oBox = new $sClass($aBox);
                                            $aAreaContent[$aBox['id']] = $this->BoxIntoDiv($aBox['id'], $oBox->getContent());
                                            unset($oBox);
                                        }
                                        break;
                                }
                                // zapisanie do cache'a
                                if ($aConfig['common']['caching'] && $this->writeBoxCache(intval($aBox['cacheable']))) {
                                    // box jest cache'owalny - zapisanie go do cache'a
                                    $oCache->save($aAreaContent[$aBox['id']],
                                        $this->getBoxCacheName($aItem['id'], $aBox, intval($aBox['cache_per_page'])));
                                }
                            }
                        }
                        if (!empty($aAreaContent)) {
                            $pSmarty->assign_by_ref('sAreaContent', implode('', $aAreaContent));
                            $this->aRenderedAreas[$aItem['number']] = $pSmarty->fetch('areas/area_' . $aItem['number'] . '.tpl');
                            $pSmarty->clear_assign('sAreaContent');
                        }
                    }
                }
            }
        }
		// renderowanie glownego obszaru podstron
		if (!empty($this->aMainArea['content']) &&
				file_exists($aConfig['smarty']['template_dir'].
										'areas/area_'.$this->aMainArea['number'].'.tpl')) {
			$pSmarty->assign_by_ref('sAreaContent', $this->aMainArea['content']);
			$this->aRenderedAreas[$this->aMainArea['number']] = $pSmarty->fetch('areas/area_'.$this->aMainArea['number'].'.tpl');
		}
	} // end of renderAreas method

	
		/**
	 * Metoda pobiera boksy przegladanej aktualnie strony / podstrony
	 * 
	 * @return	array ref
	 */
	function &getBoxes() {
		global $aConfig, $oCache;

		//if (!$aConfig['common']['caching'] || !is_array($aBoxes = unserialize($oCache->get('boxes_'.$this->iPId, 'queries')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie boksow
			if ($this->iPId > 0) {
				// pobranie wszystkich boksow danej strony
				$sSql = "SELECT DISTINCT A.area_id AS aid, A.area_id, A.id, IFNULL(A.menu_id, 0) as menu_id, A.order_by,
												IFNULL(A.module_id, 0) AS module_id,
												IFNULL(A.page_id, 0) AS page_id, A.name,
												A.template, A.btype, A.symbol, A.cacheable, A.moption, A.subpage,
												A.cache_per_page,
                        A.omit_memcached,
												C.symbol AS module,
												D.name AS page_name, D.symbol AS page_symbol
								 FROM ".$aConfig['tabls']['prefix']."boxes AS A
								 			LEFT JOIN ".$aConfig['tabls']['prefix']."boxes_settings AS B
									 			ON B.page_id = ".$this->iPId." AND
									 				 B.language_id = A.language_id AND
									 				 B.box_id = A.id
									 		LEFT JOIN ".$aConfig['tabls']['prefix']."modules AS C
									 			ON C.id = A.module_id
									 		LEFT JOIN ".$aConfig['tabls']['prefix']."menus_items AS D
									 			ON D.id = A.page_id AND
									 				 D.language_id = A.language_id
								 WHERE A.language_id = ".$this->iLangId." AND A.subpage <> '0' AND
								 			 IF(B.show_mode IS NOT NULL, B.show_mode = '1' OR B.show_mode = '".(isset($_GET['id']) ? '3' : '2')."', A.subpage = '1' OR A.subpage = '".(isset($_GET['id']) ? '3' : '2')."')
								 ORDER BY A.area_id, A.order_by";
			}
			else {
				// pobranie wszystkich boksow strony sartowej
				$sSql = "SELECT A.area_id AS aid, A.area_id, A.id, IFNULL(A.menu_id, 0) as menu_id, A.order_by,
												IFNULL(A.module_id, 0) AS module_id,
												IFNULL(A.page_id, 0) AS page_id, A.name,
												A.template, A.btype, A.symbol, A.cacheable, A.moption,
												A.cache_per_page,
                        A.omit_memcached,
												B.symbol AS module,
												C.name AS page_name, C.symbol AS page_symbol
								 FROM ".$aConfig['tabls']['prefix']."boxes AS A
								 			LEFT JOIN ".$aConfig['tabls']['prefix']."modules AS B
									 			ON B.id = A.module_id
									 		LEFT JOIN ".$aConfig['tabls']['prefix']."menus_items AS C
									 			ON C.id = A.page_id AND
									 				 C.language_id = A.language_id
								 WHERE A.language_id = ".$this->iLangId." AND
								 			 A.start_page <> '0'
								 ORDER BY A.area_id, A.order_by";
			}
			$aBoxes =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);

      /*
			if ($aConfig['common']['caching'])
				// zapis do cache'a
				$oCache->save(serialize($aBoxes), 'boxes_'.$this->iPId, 'queries');
		}
    */
		return $aBoxes;
	} // end of getBoxes() method

	/**
	 * Metoda dowiazuje do Smartow podstawowe zmienne serwisu: aktualna sciezka,
	 * wersje jezykowe, realizacja, nazwa i wersja oprogramowania
	 *
	 * @return	void
	 */
	function assignToSmarty() {
		global $aConfig, $pSmarty;

		// WERSJE JEZYKOWE
		$pSmarty->assign_by_ref('aLangs',
														$aConfig['_settings']['lang']['versions']['symbols']);
		// AKTUALNA WERSJA JEZYKOWA
		$pSmarty->assign_by_ref('sLang', $_GET['lang']);

		// KODOWANIE ZNAKOW
		$pSmarty->assign_by_ref('sCharset', $aConfig['default']['charset']);

		// ADRES BAZOWY
		$pSmarty->assign_by_ref('sBaseHref', $aConfig['common']['base_href']);

		// AKTUALNA DATA
		// dzien, miesiac, rok
		$aDate['day'] = date('j');
		$aDate['month'] = date('n');
		$aDate['year'] = date('Y');
		// dzien i miesiac slownie
		$aDate['s_day'] = $aConfig['lang']['days'][date('w')];
		$aDate['s_month'] = $aConfig['lang']['months2'][$aDate['month']];
		$pSmarty->assign_by_ref('aDate', $aDate);
		
		//ksiazek w ksiegarni
		$pSmarty->assign('books_in_store', $aConfig['_settings']['site']['books_in_store']);

		// REALIZACJA
		$aRealization['text'] =& $aConfig['lang']['common']['realization'];
		$aRealization['text1'] =& $aConfig['lang']['common']['powered_by'];
		$aRealization['link'] = '<a rel="nofollow" href="http://www.omnia.pl" target="_blank" title="Tworzenie i projektowanie stron WWW, systemy CMS, sklepy internetowe, integracje baz danych"><strong>omnia<span class="orange">.</span>pl</strong></a>';
		$aRealization['link1'] = '<a rel="nofollow" href="http://www.omnia.pl" target="_blank">omnia.CMS</a>';
		$pSmarty->assign_by_ref('aRealization', $aRealization);

		// WERSJA CMSa
		$aPowered['text'] =& $aConfig['lang']['common']['powered_by'];
		$aPowered['name'] = 'omnia.CMS';
		$aPowered['version'] =& $aConfig['_settings']['site']['version'];
		$pSmarty->assign_by_ref('aPowered', $aPowered);

		// MAPA SERWISU
		if (isset($aConfig['lang']['common']['sitemap_page']) &&
				isset($aConfig['lang']['common']['sitemap'])) {
			$sSiteMap = '<a href="'.$aConfig['common']['base_href'].
									$aConfig['_tmp']['lang'].'/'.
									$aConfig['lang']['common']['sitemap_page'].'/">'.
						 			$aConfig['lang']['common']['sitemap'].'</a>';
			$pSmarty->assign_by_ref('sSiteMap', $sSiteMap);
		}

		// POWROT
		$pSmarty->assign('sGoBack', '<a href="javascript: history.go(-1);">'.
												$aConfig['lang']['common']['go_back'].'</a>');

		// rozmiar czcionki
		$sFontSize = '';
		if (isset($_GET['font']) && in_array($_GET['font'], array('inc', 'dec'))) {
			// wybrano zmiane rozmiaru czcionki
			// dodanie ciasteczka
			if ($_GET['font'] == 'inc') {
				// zwiekszenie czcioki
				AddCookie('fontSize', 'big');
				$sFontSize = 'big';
			}
			else {
				DeleteCookie('fontSize');
			}
		}
		elseif (isset($_COOKIE['fontSize'])) {
			$sFontSize = $_COOKIE['fontSize'];
		}
		$pSmarty->assign_by_ref('sFontSize', $sFontSize);

		// link pomniejszania / zwiekszania czcionki
		$sLink = array_shift(explode('?', $_SERVER['REQUEST_URI']));
		$pSmarty->assign('sIncreaseLink', $sLink.'?font=inc');
		$pSmarty->assign('sDecreaseLink', $sLink.'?font=dec');

		// link drukowania
		$pSmarty->assign('sPrintLink', $sLink.'?print=1');
	} // end of assignToSmarty() method


	/**
	 * Metoda dowiazuje do Smartow wszystkie zmienne po renderowaniu blokow
	 * i modulow
	 *
	 * @return	void
	 */
	function assignToSmarty2() {
		global $aConfig, $pSmarty;
		$sTitlePath = '';
		
		// META
		// keywords
		
		if (isset($aConfig['_tmp']['keywords'])) {
			$aMeta['keywords'] = $aConfig['_tmp']['keywords'];
		}
		elseif (isset($aConfig['_tmp']['page']['keywords'])) {
			$aMeta['keywords'] = $aConfig['_tmp']['page']['keywords'];
		}
		else {
			$aMeta['keywords'] =& $aConfig['_settings']['site']['keywords'];
		}
    
		if (isset($aConfig['_tmp']['robots']) && !empty($aConfig['_tmp']['robots'])) {
			$aMeta['robots'] = $aConfig['_tmp']['robots'];
		}

		$noindex = array(
			'/regulamin',
			'/schowek',
			'/reklamacje',
			'/moje-konto',
			'/moje-konto/register.html',
			'/pomoc',
			'/koszyk',
			'/szukaj',
			'/platnosci',
			'/zwrot',
			'/czas-i-koszt-dostawy',
			'/dane_osobowe',
			'/informacja-o-cookies',
			'/sprawdz-status-zamowienia'
		);

		if(in_array($_SERVER['REQUEST_URI'], $noindex))
			$aMeta['robots'] = 'noindex,nofollow';

		if(isset($aConfig['_tmp']['page']['product_id'])){
			$aMeta['canonical'] = createProductLink($aConfig['_tmp']['page']['product_id'], $aConfig['_tmp']['page']['product_name']);
		}

		// description
		if (isset($aConfig['_tmp']['description'])) {
			$aMeta['description'] = $aConfig['_tmp']['description'];
		}
		elseif (isset($aConfig['_tmp']['page']['description'])) {
			$aMeta['description'] = $aConfig['_tmp']['page']['description'];
		}
		else {
			$aMeta['description'] =& $aConfig['_settings']['site']['description'];
		}
		$pSmarty->assign_by_ref('aMeta', $aMeta);

		// tagi Facebook OpenGraph
		if (isset($aConfig['_tmp']['fbog_tags']) && !empty($aConfig['_tmp']['fbog_tags'])) {
			
			$pSmarty->assign_by_ref('aFBOGTags', $aConfig['_tmp']['fbog_tags']);
		}

		// OBSZARY
		$pSmarty->assign_by_ref('aAreas', $this->aRenderedAreas);

		// STRONA GLOWNA
		if (isset($aConfig['lang']['common']['main_page']) &&
				!empty($aConfig['lang']['common']['main_page'])) {
			$aConfig['_tmp']['main_page']['name'] = $aConfig['lang']['common']['main_page'];
		}
		else {
			$aConfig['_tmp']['main_page']['name'] = $aConfig['_settings']['site']['name'];
		}
		if (count($aConfig['_settings']['lang']['versions']['symbols']) > 1) {
			$aConfig['_tmp']['main_page']['link'] = $aConfig['_tmp']['lang'];
		}
		else {
			$aConfig['_tmp']['main_page']['link'] = '/';
		}
		$pSmarty->assign_by_ref('aMainPage', $aConfig['_tmp']['main_page']);

		// SCIEZKA 'JESTES TU'
		$sPath = ($aConfig['lang']['common']['breadcrumbs'] != '' ? $aConfig['lang']['common']['breadcrumbs'].' ' : '').'<a id="spathGlowna" href="'.$aConfig['_tmp']['main_page']['link'].'">'.
						 $aConfig['_tmp']['main_page']['name'].'</a>';
		for ($i = 0; $i < count($aConfig['_tmp']['path']); $i++) {
			if ($i == count($aConfig['_tmp']['path']) - 1) {
				// ostatni element sciezki
				if ($aConfig['_tmp']['path'][$i]['link'] != '') {
					$sPath .= $aConfig['lang']['common']['breadcrumbs_separator'].
										'<a href="'.$aConfig['_tmp']['path'][$i]['link'].'"'.
										($aConfig['_tmp']['path'][$i]['target'] != '' ? ' target="'.
										$aConfig['_tmp']['path'][$i]['target'].'"' : '').' class="selected">'.
										html2text($aConfig['_tmp']['path'][$i]['name'], ' ').'</a>';
				}
				else {
					$sPath .= $aConfig['lang']['common']['breadcrumbs_separator'].
										html2text($aConfig['_tmp']['path'][$i]['name'], ' ');
				}
			}
			else {
				if ($aConfig['_tmp']['path'][$i]['link'] != '') {
					$sPath .= $aConfig['lang']['common']['breadcrumbs_separator'].
										'<a href="'.$aConfig['_tmp']['path'][$i]['link'].'"'.
										($aConfig['_tmp']['path'][$i]['target'] != '' ? ' target="'.
										$aConfig['_tmp']['path'][$i]['target'].'"' : '').'>'.
										html2text($aConfig['_tmp']['path'][$i]['name'], ' ').'</a>';
				}
				else {
					$sPath .= $aConfig['lang']['common']['breadcrumbs_separator'].
										html2text($aConfig['_tmp']['path'][$i]['name'], ' ');
				}
			}
			// sciezka dla tytulu strony
			$sTitlePath .= html2text($aConfig['_tmp']['path'][$i]['name'], ' ').
										 $aConfig['lang']['common']['breadcrumbs_separator'];
		}
		$pSmarty->assign_by_ref('sPath', $sPath);
		// jezeli sciezka tytulu nie jest pusta ustawienie jej jako
		// podtytulu
		if ($sTitlePath != '' && $aConfig['_tmp']['page']['symbol'] != $aConfig['common']['main_page_symbol']) {
			$sTitlePath = substr($sTitlePath, 0,
													 -strlen($aConfig['lang']['common']['breadcrumbs_separator']));
			if (isset($aConfig['_tmp']['subtitle'])) {
				// jezeli istnieje juz ustawiony podtytul (np. aktualnosci)
				// dodanie go po sciezce
				$aConfig['_tmp']['subtitle'] = $sTitlePath.': '.strip_tags($aConfig['_tmp']['subtitle']);
			}
			else {
				// tylko przepisanie sciezki do podtytulu
				$aConfig['_tmp']['subtitle'] = $sTitlePath;
			}
		}

		// PODSTRONA
		if (isset($aConfig['_tmp']['page'])) {
			if(isset($aConfig['_tmp']['page_header']))
			{
				$pSmarty->assign_by_ref('sPageHeader', $aConfig['_tmp']['page_header']);
			}
			else {
				$pSmarty->assign_by_ref('sPageHeader', $aConfig['_tmp']['page']['name']);
			}
			// ZDJECIE PODSTRONY
			if (!empty($aConfig['_tmp']['page']['image'])) {
				$sPhoto = '<img src="'.$aConfig['_tmp']['page']['image']['src'].'" alt="'.$aConfig['_tmp']['page']['image']['name'].'" />';
			}
			else {
				$sPhoto = '';
			}
			$pSmarty->assign_by_ref('sPagePhoto', $sPhoto);
		}
		// PODTYTUL PODSTRONY
		if (isset($aConfig['_tmp']['page_subheader'])) {
			$pSmarty->assign_by_ref('sPageSubHeader', $aConfig['_tmp']['page_subheader']);
		}

		// NAZWA SERWISU
		$aPage['name'] =& $aConfig['_settings']['site']['name'];

		// STOPKA
		$aPage['footer'] = $this->getAjaxPageJS().$aConfig['_settings']['site']['footer'];
		$aPage['footer2'] =& $aConfig['_settings']['site']['footer2'];

		// TYTUL
		if (isset($aConfig['_tmp']['title'])) {
			$aPage['title'] = strip_tags($aConfig['_settings']['site']['subtitle'] != '' ? $aConfig['_tmp']['title'].' - '.$aConfig['_settings']['site']['subtitle'] : $aConfig['_tmp']['title']);
		} else
		if(isset($aConfig['_tmp']['page']['title'])){
			$aPage['title'] = $aConfig['_tmp']['page']['title'];
		} 
		else
		if (isset($aConfig['_tmp']['subtitle'])) {
			if ($aConfig['_settings']['site']['before_title'] == '1') {
				$aPage['title'] = strip_tags($aConfig['_settings']['site']['subtitle'].' '.$aConfig['_tmp']['subtitle']);
			}
			else {
				$aPage['title'] = strip_tags($aConfig['_tmp']['subtitle'].' '.
													$aConfig['_settings']['site']['subtitle']);
			}
		}
		else {
			$aPage['title'] =& $aConfig['_settings']['site']['title'];
		}
		$pSmarty->assign_by_ref('aPage', $aPage);
		

    
		// KOMUNIKAT
		if(isset($_SESSION['_tmp']['message'])){
      /*
    $sMsgJS = '
    <span id="global_msg" style="display: none;"></span>
    <script type="text/javascript">
      alert("test");
      $.get("/internals/getMessages.class.php", function(data) {
        $("#global_msg").html(data);
      });
    </script>
    ';
      $pSmarty->assign_by_ref('sMsg', $sMsgJS);
      */
		} else {
      if (isset($aConfig['_tmp']['message'])) {
        $pSmarty->assign_by_ref('sMsgText', $aConfig['_tmp']['message']['text']);
        $pSmarty->assign_by_ref('iCloseAfterSeconds', $aConfig['_tmp']['message']['close_afer_seconds']);
        if (isset($aConfig['_tmp']['message']['error']) &&
            $aConfig['_tmp']['message']['error']) {
          $pSmarty->assign_by_ref('sMsg', $pSmarty->fetch('error.tpl'));
        }
        else {
          $pSmarty->assign_by_ref('sMsg', $pSmarty->fetch('message.tpl'));
        }
        $pSmarty->clear_assign('sMsgText');
      }
    }

        $additionalHead = new additionalHead();
        $codeHead = $additionalHead->getAdditionalHeadsCode();
		
		// HEADER JS
		if (!empty($aConfig['header_js'])) {
			$sHeaderJS = '<script type="text/javascript">
	// <![CDATA[
		'.implode("\n\t\t", $aConfig['header_js']).'
	// ]]>
</script>
';
            $sHeaderJS .= $codeHead;
            $pSmarty->assign_by_ref('sHeaderJS', $sHeaderJS);
        }  else {
            $pSmarty->assign_by_ref('sHeaderJS', $codeHead);
        }


		// HEADER JS INCLUDES
		if (!empty($aConfig['_tmp']['header_js_includes'])) {
			$sHeaderJSIncludes = implode("\n", $aConfig['_tmp']['header_js_includes']);
			$pSmarty->assign_by_ref('sHeaderJSIncludes', $sHeaderJSIncludes);
		}

		// Google Analytics
		if (isset($aConfig['common']['google_analytics']) &&
				!empty($aConfig['common']['google_analytics'])) {
			$pSmarty->assign_by_ref('sGoogleAnalytics', $aConfig['common']['google_analytics']);
		}
	} // end of assignToSmarty2() method
  
  
  /**
   * Metoda pobiera js ajax dla strony
   * 
   * @return type
   */
  private function getAjaxPageJS() {
    global $aConfig;
    
    if ($this->bRefUse === TRUE) {
      $sJS = '<span id="_tmp" style="display: none"></span>
              <script type="text/javascript">
                $(function() {
                  $.ajax({
                    url: "/internals/getOneBox.class.php?lang_id='.intval($this->iLangId).'&page_id='.(intval($aConfig['_tmp']['page']['id']) > 0 ? intval($aConfig['_tmp']['page']['id']) : '-1').'",
                    async: true,
                    success: function(data) {
                      $("#_tmp").html(data);
                      $("#_tmp").html("");
                    }
                  });
                });
             </script>';
    }
    return $sJS;
  }// end of getAjaxPageJS() method
  
  
  /**
   * Metoda pobiera id boksu koszyka
   * 
   * @return int
   */
  private function getIdCardBox() {
    $sSql = "SELECT A.id 
             FROM boxes AS A
             JOIN modules AS B
             ON A.module_id=B.id
             WHERE B.symbol='m_zamowienia' AND
                   A.moption IS NULL";
    return Common::GetOne($sSql);
  }// end of getIdCardBox


	/**
	 * Metoda sprawdza czy modul ma byc cache'owany czy tez nie
	 *
	 * @return	bool	- true: cache'uj; false: nie cache'uj
	 */
	function isCacheable() {
		global $aConfig;
		$aModule =& $aConfig['_tmp']['modules_settings'][$aConfig['_tmp']['page']['module_id']];
		$sOption = $aConfig['_tmp']['page']['moption'];

		return (!isset($_GET['id']) &&
						($aModule['cacheable'] == '1' ||
						 ($aModule['cacheable'] == '2' && empty($_POST))) &&
						 (empty($sOption) ||
						  (!is_array($aModule['exclude_from_cache']) ||
						   !in_array($sOption, $aModule['exclude_from_cache']))));
	} // end of isCacheable() method


	/**
	 * Metoda sprawdza czy boks ma byc cache'owany czy tez nie
	 *
	 * @param	integer	$iCacheable - poziom cache'owania boksu:
	 * 															0 - nie cache'uj
	 * 															1 - cache'uj zawsze
	 * 															2 - nie cache'uj gdy przeslano POST
	 * @return	bool	- true: cache'uj; false: nie cache'uj
	 */
	function boxIsCacheable($iCacheable) {
		return ($iCacheable == 1 ||
					 ($iCacheable == 2 && empty($_POST)));
	} // end of boxIsCacheable() method
	
	
	/**
	 * Metoda sprawdza czy mozna zapisac do cache'a wygenerowany kod
	 * HTML boksu
	 *
	 * @param	integer	$iCacheable - poziom cache'owania boksu:
	 * 															0 - nie cache'uj
	 * 															1 - cache'uj zawsze
	 * 															2 - nie cache'uj gdy przeslano POST
	 * @return	bool	- true: cache'uj; false: nie cache'uj
	 */
	function writeBoxCache($iCacheable) {
		return $iCacheable == 1 || $iCacheable == 2;
	} // end of writeBoxCache() method


	/**
	 * Metoda tworzy i zwraca nazwe pliku zcache'owanego boksu
	 * Nazwa uzalezniona jest od zmienej $iCachePerPage: 0 - cache'uj raz dla
	 * wszystkich podstron; 1 - cache'uj dla kazdej podstrony z osobna (przy tworzeniu
	 * nazwy uwzagleniana jest wartosc $_SERVER['REQUEST_URI']
	 *
	 * @param	integer	$iAreaId	- Id obszaru
	 * @param	integer	$aBox	- tablica danych boksu
	 * @param	integer	$iCachePerPage	- 0 - cache'uj raz dla wszystkich podstron;
	 * 																	1 - cache'uj dla kazdej podstrony z osobna
	 * @return	string	- nazwa pliku zcache'owanego boksu
	 */
	function getBoxCacheName($iAreaId, $aBox, $iCachePerPage) {
    global $aConfig;
    
    $mBoxCategories = '';
    if ($aBox['module'] == 'm_menu' && $aBox['moption'] == 'glowna') {
      // dołączamy info o ciasteczku tego menu
      $mBoxCategories = intval($_COOKIE['box_categories']).'|';
      $mBoxCategories .= '|'.$aConfig['_tmp']['path'][0]['id'];
    }
    
		return $iAreaId.'|'.
               $mBoxCategories.'|'.
							 $aBox['id'].'|'.
							 ($iCachePerPage == 1 ? $_SERVER['REQUEST_URI'] : '');
	} // end of getBoxCacheName() method
  
  
  private function _doLoginByFacebook() {
    
    // sprawdza czy osoba jest zalogowana na facebooku
//    include_once('LIB/fb_connector/fbConnector.class.php');
//    $oFB = new fbConnector();
//    dump($oFB->checkLoggedIn());
  }
} // end of Singleton Class
?>