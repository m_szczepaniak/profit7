<?php
	// dolaczenie glownych klas serwisu
	include_once ('inc/classes/Factory.class.php');

  /****** wersja jezykowa ****/
  // okreslenie wersji jezykowych
 	$aConfig['_settings']['lang']['versions'] =& getLangVersions();
 	foreach ($aConfig['_settings']['lang']['versions'] as $iKey => $aLang) {
		if ($aLang['default_lang'] == '1') {
			$aConfig['_settings']['lang']['default'] = $aLang['symbol'];
		}
		$aConfig['_settings']['lang']['versions']['symbols'][$aLang['id']] = $aLang['symbol'];
		$aConfig['_settings']['lang']['versions']['ids'][$aLang['symbol']] = $aLang['id'];
		unset($aConfig['_settings']['lang']['versions'][$iKey]);
	}

  // sprawdzenie czy przekazano wersje jezykowa i czy ona istnieje
  if (!isset($_GET['lang']) ||
  		@!in_array(($_GET['lang'] = substr($_GET['lang'], 0, 2)),
  							$aConfig['_settings']['lang']['versions']['symbols'])) {
  	$_GET['lang'] = $aConfig['_settings']['lang']['default'];

  }
  if (count($aConfig['_settings']['lang']['versions']['ids']) > 1 &&
  		$_GET['lang'] !== $aConfig['_settings']['lang']['default']) {
  	// jezeli aktualna wersja nie jest wersja domyslna
  	// do linek w serwisie dolaczany bedzie symbol wersji jez.
  	$aConfig['_tmp']['lang'] = '/'.$_GET['lang'];
  }
  else {
  	$aConfig['_tmp']['lang'] = '';
  }
  $_GET['lang_id'] = $aConfig['_settings']['lang']['versions']['ids'][$_GET['lang']];

  // okreslenie adresu BaseHref
  $aConfig['common']['base_href'] = substr($aConfig['common']['client_base_url_http'], 0, -1);

  /****** dane serwisu - META, TITLE, stopka ****/
  $aConfig['_settings']['site'] =& getSiteSettings();
  if($aConfig['_settings']['site']['allow_cache'] != 1){
    $aConfig['common']['caching'] = false;
  }

  /****** aktualna sciezka i strona ****/
  $aConfig['_tmp']['selected'] = array();
  if (!empty($_GET['path'])) {
  	// zapobieganiu dublowania strony main_page_symbol
  	if($_GET['path'] === $aConfig['common']['main_page_symbol']){
        doRedirect('/', '301');
  	} else {
  		setPages($_GET['path'], $_GET['lang_id']);
  	}
  }
  elseif (isset($_GET['product']) && is_numeric($_GET['product'])) {
  	// produkt - ustawieni modulu jako m_oferta_produktow i odpowiednich zmiennych
  	setProductAllInfo((double) $_GET['product']);
  } else {
  //	doRedirect('/'.$aConfig['common']['main_page_symbol']);
  	$_GET['path'] = $aConfig['common']['main_page_symbol'];
  	setPages($_GET['path'], $_GET['lang_id']);
  }

  if (isset($aConfig['_tmp']['page'])) {
		if ($aConfig['_tmp']['page']['mtype'] == 1) {
			// strona jest Linkiem wewnetrznym przekierowanie do jej strony docelowej
			doRedirect(getPageLink($aConfig['_tmp']['page']), '301');
		}
		elseif ($aConfig['_tmp']['page']['mtype'] == 2) {
			// strona jest Linkiem zewnetrznym, przekierowanie na glowna strone
			doRedirect('/', '301');
		}
	}

    if($aConfig['_tmp']['page']['module'] == 'm_konta' ||
        ($aConfig['_tmp']['page']['module'] == 'm_zamowienia' && $_GET['action'] != 'platnosci')){
        if(!$aConfig['common']['is_https'] && $aConfig['common']['status'] != 'development') {
            doRedirectHttps($_SERVER['REQUEST_URI'], 301);
        }
    } else {
        if (($aConfig['_tmp']['page']['module'] == 'm_zamowienia' && $_GET['action'] == 'platnosci')) {
            if($aConfig['common']['is_https']) {
                //                doRedirect($_SERVER['REQUEST_URI']);
            }
        } else {
            if (!$aConfig['common']['is_https']) {
                doRedirectHttps($_SERVER['REQUEST_URI'], '301');
            }
        }
    }

	/****** obszary serwisu ****/
	getAreas(!isset($aConfig['_tmp']['page']));

	/****** ustawienia modulow - cache'owania dla strony ****/
	getModulesSettings();
	
	/****** akcja i jej parametry ****/
	if (isset($_GET['action'])) {
		if (strpos($_GET['action'], ',') === 0) $_GET['action'] = substr($_GET['action'], 1);
		if (strpos($_GET['action'], ',') !== false) {
			// akcja posiada parametry
			$_GET['parameters'] = explode(',', $_GET['action']);
			$_GET['action'] = $_GET['parameters'][0];
			if (strpos($_GET['action'], ':') === false) {
				// pierwszy element to rzeczywiscie akcja
				// usuniecie pierwszego parametru
				array_shift($_GET['parameters']);
			}
			else {
				// pierwszy element to rowniez parametr
				// wyczyszczenie zmiennej action
				unset($_GET['action']);
			}
			for ($i = 0; $i < count($_GET['parameters']); $i++) {
				if (empty($_GET['parameters'][$i])) {
					unset($_GET['parameters'][$i]);
				}
				else {
					if (strpos($_GET['parameters'][$i], ':') !== false) {
						$aParam = explode(':', $_GET['parameters'][$i]);
						$_GET[$aParam[0]] = $aParam[1];
					}
				}
			}
		}
		elseif (strpos($_GET['action'], ':') !== false) {
			// akcja jednak jest parametrem
			$aParam = explode(':', $_GET['action']);
			$_GET[$aParam[0]] = $aParam[1];
			unset($_GET['action']);
		}
	}
	if (isset($_GET['id'])) $_GET['id'] = (int) $_GET['id'];
	if (isset($_GET['it'])) $_GET['it'] = (int) $_GET['it'];
	if (isset($_GET['p'])) $_GET['p'] = (int) $_GET['p'];
	if (isset($_GET['del'])) $_GET['del'] = (int) $_GET['del'];
	
?>