<?php

require_once( 'class.magic-min.php' );

$vars = array(
    'echo' => false,
    'encode' => false,
    'timer' => true,
    'gzip' => false,
    'closure' => false,
    'remove_comments' => true,
    'force_rebuild' => false, //USE THIS SPARINGLY!
    'hashed_filenames' => false,
);

$minified = new Minifier( $vars );

$included_scripts = array(
    'js/jquery/jquery-1.9.1.min.js',
    'js/jquery.cycle.all.min.js',
    'js/jqueryui/jquery-ui-1.10.1.custom.min.js',
    'js/functions_04072013.js',
    'js/behaviours_04072013.js',
    'js/tooltip.js',
    'js/slick.js'
);

?>