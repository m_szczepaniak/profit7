(function ($) {
    $.fn.invisible = function () {
        return this.each(function () {
            $(this).css("visibility", "hidden");
        });
    };
    $.fn.visible = function () {
        return this.each(function () {
            $(this).css("visibility", "visible");
        });
    };
}(jQuery));


$(function () {
    console.log(aBooksToSend);
    console.log(aBooks);

    start = new Date().getTime();

    var allSuccessfullyScaned = false;

    if (typeof extraVerification !== 'undefined' && true == extraVerification) {

        var formLocked = true;
        var formTransfer = false;

        $('input[type="submit"]').on('click', function(){

            if (allSuccessfullyScaned == false) {

                var r = confirm("Wykryto braki - Czy chcesz zatwierdzić listę z brakami?");
                if (r == true) {
                    formLocked = false;
                }
            } else {
                formLocked = false;
            }
        });

        $('form').submit(function(){


            if (formLocked == true) {

                return false;
            }
        });
    }

    setInterval(
        function () {
            if (start != null) {
                var now = new Date().getTime();
                time = (now - start) / 1000;
                $("#timer").html(time);
            } else {
                $("#timer").html("STOP");
            }
        }, 1000
    );

    window.setInterval(function(){ saveListState(true); }, 6000);

    function saveListState(isAsync) {
        if (aBooks !== null && aBooksToSend !== null) {
            if (aBooks.length > 0 && aBooksToSend.length > 0) {
                var aaa = ({
                    'method': 'save',
                    'aBooks': aBooks,
                    'aBooksToSend': aBooksToSend,
                    'scannedBooks': scannedBooks,
                    'orders_items_lists_id': $("#orders_items_lists_id").val()
                });
                $.ajax({
                        data: aaa,
                        dataType: "json",
                        type: "POST",
                        cache: false,
                        async: isAsync,
                        url: "ajax/completationListStatus.php"
                    })
                    .done(function (data) {
                        //console.log(data);
                    });
            }
        }
    }

    function saveScannedItem(lastBook)
    {
            formTransfer = true;

            var scanDetails = ({
                'method': 'transfer',
                'orders_items_lists_id' : $("#orders_items_lists_id").val(),
                'orders_items_lists_items_id' : lastBook.OILI_ID,
                'stock_location_orders_items_lists_items_id': lastBook.SLOILI_ID,
                'user_id' : userId
            });

            $.ajax({
                data: scanDetails,
                dataType: "json",
                type: "POST",
                cache: false,
                async: true,
                url: "ajax/TransferList.php"
            })
            .complete(function(xhr, textStatus) {
                //new Messi('Historia skanowania została zapisana',{title: 'Powodzenia', modal: true});
                if ( $('.loc-wrapper').length === 0) {
                    if (lastBook.pack_number_area !== undefined && $("#content").contents().find("#pack_number_area").val() !== undefined) {
                        checkPackNumberArea(lastBook.pack_number_area, $("#content").contents().find("#pack_number_area").val());
                    }
                }
                else
                {
                    if (lastBook.pack_number_area !== undefined && $("#pack_number_area").val() !== undefined) {
                        checkPackNumberArea(lastBook.pack_number_area, $("#pack_number_area").val());
                    }
                }
            });
    }

    function checkPackNumberArea(previous,current)
    {
        if ($('#main_shelf_number').text() !== 'Lista zebrana')
        {
            console.log(previous);
            console.log(current);
            if (parseInt(previous) !== parseInt(current))
            {
                alertify.confirm("Przekaż listę do " + current + " strefy.",
                    function(){
                        alertify.success('Ok: ');
                        saveListState(false);
                        window.close();
                    },
                    function(){
                        alertify.error('Zbieraj dalej sam');
                    });
            }
            else
            {
                /*alertify
                    .alert("Te same lokacje zbieraj dalej.", function(){
                        alertify.message('OK');
                    });*/
            }
        }
    }

    $('button[name="transfer"]').click(function(){
        console.log('Przekazywanie listy');
        alertify.confirm("Przekaż listę do następnej strefy",
            function(){
                alertify.success('Ok: ');
                saveListState(false);
                window.close();
            },
            function(){
                //alertify.error('Zbieraj dalej sam');
            });
    });

    $('#go_back').click(function () {
        goProductToEnd();
    });

    function goProductToEnd() {
        if (aBooks !== undefined) {
            aBook = aBooks.shift();
            if (aBook !== undefined) {
                setBookTime(aBook);
                aBook.user_id = window.userId;
                aBooks.push(aBook);
                nextBook();
            }
        }
    }

    function setBookTime(aBook) {
        var end = new Date().getTime();
        time = end - start;
        if (aBook.time !== undefined && aBook.time > 0) {
            aBook.time += time;
        } else {
            aBook.time = time;
        }
        start = new Date().getTime();
    }

    function clearBookTime() {
        start = null;
    }

    function doCheckISBN(aBook, searchISBN) {
        if (searchISBN !== "" &&
            (aBook.isbn_plain === searchISBN ||
            aBook.isbn_10 === searchISBN ||
            aBook.isbn_13 === searchISBN ||
            aBook.ean_13 === searchISBN) &&
            aBook.checked === '0'
        )
        {
            setBookTime(aBook);
            return true;
        }

        new Messi('Błąd - nie potwierdzono pozycji : ' + searchISBN, {title: 'Błąd', modal: true});
        $(window.document).focus();
        return false;
    }

    function scanAllBook(aBook) {
        aBook.currquantity = parseInt(aBook.quantity);
        aBook.user_id = window.userId;
        if (parseInt(aBook.currquantity) >= parseInt(aBook.quantity)) {
            aBook.checked = '1';
            aBooksToSend.push(aBooks.shift());
            nextBook();
        }
        return aBook.quantity;
    }

    function incrementBook(aBook) {
        aBook.currquantity = parseInt(aBook.currquantity) + 1;
        aBook.user_id = window.userId;
        if (parseInt(aBook.currquantity) >= parseInt(aBook.quantity)) {
            aBook.checked = '1';
            aBooksToSend.push(aBooks.shift());
            nextBook();
        } else {
            var leftToScan = parseInt(aBook.quantity) - parseInt(aBook.currquantity);
            $("#main_quantity_current_item").html(leftToScan);
        }
    }

    function getISBNs(aBook) {
        var aISBNs = [aBook.isbn_plain, aBook.isbn_10, aBook.isbn_13, aBook.ean_13].filter(function (str) {
            return str;
        });

        var aUnique = $.unique(aISBNs);
        return aUnique.join(', ');
    }

    function updateBook(aBook) {
        // główny produkt
        if (aBook.photo !== undefined && aBook.photo !== null) {
            var aPhoto = aBook.photo.split("|");
            var sPhotoNorm = aPhoto[0] + '/' + aPhoto[1];
            var sPhotoBig = aPhoto[0] + '/__b_' + aPhoto[1];
            $("#main_image").removeAttr('src');
            $("#main_image").removeAttr('onError');
            $("#main_image")
                .attr("src", 'https://profit24.pl/images/photos/' + sPhotoBig)
                .attr("onError", "this.onerror=null; this.src='" + 'https://profit24.pl/images/photos/' + sPhotoNorm + "';");
        }

        $("#main_isbn").html(getISBNs(aBook));
        $("#main_name").html(aBook.name);
        if (aBook.desc !== undefined) {
            $("#main_desc").html(aBook.desc);
        } else {
            $("#main_desc").html(aBook.publisher + ' ' + aBook.publication_year + '<br />' + aBook.authors);
        }
        if (aBook.shelf_number !== null) {
            $("#main_shelf_number").html(aBook.shelf_number);
        } else {
            $("#main_shelf_number").html('Magazyn');
        }

        console.log(aBook);

        if (aBook.pack_number_area !== null) {
            $("#pack_number_area").val(aBook.pack_number_area);
        } else {
            $("#pack_number_area").val('');
        }

        $("#main_quantity_current_item").html(parseInt(aBook.quantity) - parseInt(aBook.currquantity));

        if (true === isHighLevelStock) {
            $("#main_profit_g_act_stock").html(aBook.profit_g_act_stock);
        }

        if (parseFloat(aBook.weight) <= 0) {
            $("#get_weight").show();
        } else {
            $("#get_weight").hide();
        }
    }

    function CoolEctorDone() {
        $("#main_image").attr("src", '').attr("onError", '');
        $("#main_isbn").html('');
        $("#main_name").html('');
        $("#main_desc").html('');
        $("#main_shelf_number").html('Lista zebrana');
        $("#main_quantity_current_item").html('');
        $("#main_profit_g_act_stock").html('');
        $("#get_weight").hide();

        allSuccessfullyScaned = true;
    }

    function showLastBookArray(aLastBookArray) {
        for (var i = 0; i <= 2; i++) {

            if (aLastBookArray[i] !== undefined && aLastBookArray[i] !== null) {
                aBook = aLastBookArray[i];
                if (aBook.photo !== undefined && aBook.photo !== null) {
                    var aPhoto = aBook.photo.split("|");
                    var sPhotoNorm = aPhoto[0] + '/' + aPhoto[1];
                    var sPhotoBig = aPhoto[0] + '/__b_' + aPhoto[1];
                    $("#image_" + i)
                        .attr("src", 'https://profit24.pl/images/photos/' + sPhotoBig)
                        .attr("onError", "this.onerror=null; this.src='" + 'https://profit24.pl/images/photos/' + sPhotoNorm + "';");
                }

                $("#isbn_" + i).html(getISBNs(aBook));
                $("#name_" + i).html(aBook.name);
                if (aBook.desc !== undefined) {
                    $("#main_desc").html(aBook.desc);
                } else {
                    $("#desc_" + i).html(aBook.publisher + ' ' + aBook.publication_year + ' / ' + aBook.authors);
                }

                if (aBook.shelf_number !== null) {
                    $("#shelf_number_" + i).html(aBook.shelf_number);
                } else {
                    $("#shelf_number_" + i).html("Magazyn");
                }
            } else {
                return null;
            }
        }
    }



    var iScanned = 0;
    var lastBook = ({});

    function getNextBook() {
        if (aBooks[0] !== undefined) {
            var aBook = aBooks[0];
            return aBook;
        } else {
            return false;
        }
    }

    function nextBook() {

        aBookToScan = getNextBook();
        setScanMode();
        if (aBookToScan !== false) {
            updateBook(aBookToScan);
        } else {
            clearBookTime();
            CoolEctorDone();
        }
    }
    $(function () {
        nextBook();
        if (scannedBooks != null) {
            showLastBookArray(scannedBooks);
        }
    });

    function setScanMode() {

        default_mode = 'CONTAINER';
        mode = default_mode;

        $('#number_cont').invisible();
        if (aBookToScan.pack_number == "" || lastBook.pack_number === aBookToScan.pack_number) {
            // jeśli brak lokalizacji FIX, lub ta sama lokalizacja
            $('#number_cont').visible();
            mode = 'ISBN';
            $('.tmp-msg').remove();
            if ($("#is_unpacking").length > 0 && $("#is_unpacking").val() == "1") {
                if (aBookToScan.pack_number == "") {
                    $('#number_cont').visible();
                    changeLocation();
                }
            }
        }
    }


    $(document).keydown(function (e) {
        if ($("#search_isbn").is(":focus") === true) {
            if (e.which === 13) {
                if (mode == 'CONTAINER') {
                    console.log(aBookToScan);
                    var searchISBN = $("#search_isbn").val();
                    var preg = searchISBN;
                    if (searchISBN.length > 9 && aBookToScan.pack_number.search(preg) >= 0) {
                        $('#number_cont').visible();
                        mode = 'ISBN';
                    } else {
                        if (aBookToScan.stock_pack_number !== undefined && aBookToScan.stock_pack_number == $("#search_isbn").val()) {
                            $('#number_cont').visible();
                            mode = 'ISBN';
                        } else {
                            new Messi('Nieprawidłowa lokalizacja', {title: 'Błąd lokalizacji', modal: true});
                        }
                    }
                } else if (mode == 'ISBN') {
                    if (doCheckISBN(aBookToScan, $("#search_isbn").val())) {
                        lastBook = aBookToScan;
                        if ($.isEmptyObject(lastBook) === false) {
                            scannedBooks[2] = scannedBooks[1];
                            scannedBooks[1] = scannedBooks[0];
                            scannedBooks[0] = lastBook;
                        }
                        if (aBookToScan !== null) {
                            if (scannedBooks !== null) {
                                showLastBookArray(scannedBooks);
                            }
                            if (isHighLevelStock === true) {
                                iScanned = parseInt(parseInt(aBookToScan.quantity) + iScanned);
                                scanAllBook(aBookToScan);
                            } else {
                                incrementBook(aBookToScan);
                                iScanned++;
                            }
                        }


                        if (typeof (Storage) !== "undefined") {
                            var iListId = $("#orders_items_lists_id").val();
                            localStorage.setItem("aBooksToSend[" + iListId + "]", JSON.stringify(aBooksToSend));
                            localStorage.setItem("aBooks[" + iListId + "]", JSON.stringify(aBooks));
                            localStorage.setItem("scannedBooks[" + iListId + "]", JSON.stringify(scannedBooks));
                        }
                        $("#left_order_items").html(quantityAllBooks - iScanned);

                        saveScannedItem(lastBook);
                    }
                } else if (mode == 'LOCATION') {
                    console.log('location');
                    console.log(aBookToScan);
                    var container = ({
                        'container_id': $("#search_isbn").val(),
                        'product_id': aBookToScan.product_id,
                        'random_pack_number': aBookToScan.random_pack_number
                    });
                    $.ajax({
                            data: container,
                            dataType: "json",
                            type: "POST",
                            cache: false,
                            async: true,
                            url: "ajax/changeProductLocation.php"
                        })
                        .done(function (data) {
                            aBookToScan.shelf_number = data.container_id;
                            $("#main_shelf_number").html(data.container_id);
                            recreateItems();
                            $('#number_cont').visible();
                            mode = 'ISBN';

                            if ($('.temp-message-g').length < 1) {
                                $('[name="ordered_itm"]').prepend('<p class="tmp-msg temp-message-g" style="font-size:40px;font-weight:bold; color: green">Zmieniono lokalizacje na: '+aBookToScan.shelf_number+'</p>');
                            }

                        })
                        .fail(function (data) {
                            new Messi(data.responseText, {title: 'Błąd', modal: true});
                        });
                }
                $("#search_isbn").val("");
                return false;
            }
        } else {
            $("#search_isbn").focus();
            return false;
        }
    });

});

function changeLocation() {
    if ($('.temp-message-b').length < 1) {
        $('[name="ordered_itm"]').prepend('<p class="tmp-msg temp-message-b" style="font-size:40px;font-weight:bold; color: blue">Wskaż nowa lokalizacje</p>');
    }

    mode = 'LOCATION';
}

/*
 // przedłużenie sesji na CoolEktor
 setInterval(function() {
 $.ajax({
 type: "GET",
 cache: false,
 async: true,
 url: "ajax/continueSession.php"
 });
 }, 60000);
 */