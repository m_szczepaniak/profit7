    $(document).keydown(function(e){
        var focused = $(":focus");
        if ($(focused).attr("class") !== "editable_input") {
          $("#search_isbn").focus();
        } else {
          if(e.keyCode == 13){
            var sWaga = parseFloat($(focused).val().replace(",", "."));
            $(focused).val(sWaga);
            if (sWaga > 0.00 && sWaga <= 15) {
              $(focused).parent().parent().children("td:first").next().next().children("span.isbn").attr('isbn', '');
              $("#search_isbn").val("");
              $(focused).parent().parent().children("td:first").children("input:checkbox").attr("checked", true).triggerHandler('click');
              $(focused).parent().parent().attr('style', 'background-color: rgb(215, 211, 188);');
              $(focused).blur();
            } else {
              $(this).focus();
              alert("Wprowadź prawidłową wagę");
              return false;
            }
            return false;
          }
        }
      });
      
      
      $(document).ready(function(){
        //$("#search_isbn").focus();
        $("#search_isbn").keydown(function(e){
          if(e.keyCode == 13){
            var sISBN = $(this).val();
            var oIsbn = $("span.isbn[isbn="+sISBN+"]").eq(0);
            var oSelRow = oIsbn.parent().parent().children("td:first").children("input:checkbox[checked!=\'checked\']");
            if(oSelRow.html() !== undefined && oSelRow.html() !== null){
              $('html, body').animate({
                scrollTop: $(oSelRow).offset().top
              }, 0);
              
              /** Sprawdzamy ilość **/
              var iLiczba = oSelRow.parent().parent().children("td:last").prev().html();
              var oWaga = oSelRow.parent().parent().children("td:last").children("input");
              var iWaga = oWaga.val();
              if (iLiczba > 1) {
                /** Zapytanie o ilość **/
                var reply = window.confirm("Czy na pewno jest "+iLiczba+" książek");
                if (reply == false) {
                  return false;
                }
              } 
              if (iWaga == "0,00") {
                alert("Uzupełnij wagę produktu");
                var iCellId = oSelRow.parent().parent().children("td:last").attr("id");
                $("#"+iCellId).click();
                return false;
              } 
              oSelRow.attr("checked", true).triggerHandler('click');
              oSelRow.parent().parent().attr('style', 'background-color: rgb(215, 211, 188);');
              
              $("#search_isbn").val("");
              oIsbn.attr('isbn', '');
              
            } else {
              alert("UWAGA !!! \nNie znaleziono ISBN: "+sISBN);
              $("#search_isbn").val("");
            }
            $("#search_isbn").focus();
            return false;
          }
        });
      });