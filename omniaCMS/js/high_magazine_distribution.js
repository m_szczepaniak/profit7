$(document).keydown(function (e) {
    var focused = $(":focus");

    if (false === $(focused).hasClass("editable_input")) {
        $("#search_isbn").focus();
        if (e.keyCode === 13) {
            if ("product_isbn" === $(focused).attr('id')) {
                $("#product_quantity").focus();
                return false;
            }

            if ("product_quantity" === $(focused).attr("id")) {
                $("#location").focus();
                return false;
            }

            if (false === $(focused).is('textarea')) {
                return false;
            }
        }
    } else {

    }

});


$(document).ready(function () {
    $("#location").keydown(function (e) {
        if (e.keyCode === 13) {
            if ($(this).hasClass('editable_input')) {
                $("#product_isbn").focus();
                return false;
            }
            var quantity = $('#product_quantity').val();
            var location = $('#location').val();
            // sprawdzamy produkt
            $.ajax({
                method: "POST",
                url: "ajax/inventory.php",
                data: {
                    ean_13: $("#product_isbn").val(),
                    sc_date: $("#sc_date").val(),
                    location: location,
                    quantity: quantity
                },
                dataType: "json"
            })
                .done(function (html_items) {
                    console.log(html_items);
                    var new_item = "<table width=\"100%\" class=\"scanTable\"><tr><th>#ID</th><th>Okładka</th><th>EAN_13</th><th>Ilość</th><th>Lokalizacja</th><th>Tytuł</th></tr>";
                    for (var sss in html_items) {
                        var key = html_items[sss].id+'_'+html_items[sss].location;


                        var hiddenInputs = '<input type="hidden" name="scanned_products['+key+'][quantity]" value="'+html_items[sss].quantity+'"/>';
                        hiddenInputs += '<input type="hidden" name="scanned_products['+key+'][location]" value="'+html_items[sss].location+'"/>';

                        new_item += "<tr class='row-id-" + (html_items[sss].last == true ? 'selected' : '' ) + "'><td>#" + sss + "</td><td><img src='" + html_items[sss].logo + "' style='max-weight: 90px; max-height: 90px' title='Okładka' /></td><td  style='font-size:36px;'>" + html_items[sss].ean_13 + "</td><td style='font-size:36px; color: red;'>" + html_items[sss].quantity + hiddenInputs+"</td>" +
                                '<td><span class="location-style"  style="font-size:36px;">' + html_items[sss].location+ '</span></td>' +
                                "<td>" + html_items[sss].name + "</td>" +
                            "</tr>";
                    }
                    new_item += "</table>";

                    $("#sc_products").html(new_item);
                    recreateItems();
                    $("#scanner_pool").remove();
                    $("#product_isbn").val('');
                    $("#product_quantity").val('');
                    $("#location").val('');
                    $('html, body').animate({
                        scrollTop: $(".row-id-selected").offset().top
                    }, 0);
                })
                .error(function () {
                    new Messi('Błąd - nie znaleziono pozycji : ' + $("#product_isbn").val(), {
                        title: 'Błąd',
                        modal: true
                    });
                })
        }
    });

    $("#ordered_itm").on('submit', function () {
        if (!confirm('Czy na pewno chcesz zapisać ?')) {
            return false;
        }
    });
});

