function doSearchISBN(searchISBN) {


  for(var i = 0; i < aBooks.length; i++)
  {
    if(searchISBN !== "" &&
      (aBooks[i].transport_number === searchISBN || 
       aBooks[i].order_number === searchISBN) &&
       aBooks[i].checked === '0'
       )
    {
      aBooksToSend.push(aBooks[i]);
      return aBooks[i];
    }
  }

  new Messi('Nie znaleziono : ' + searchISBN, {title: 'Brak', modal: true});
  $(window.document).focus();
  return null;
}

function incrementBook(aBook) {
  aBook.checked = '1';
}

function updateBook(aBook) {
  // główny produkt
  if (aBook.photo !== undefined) {
//    $("#main_image")
//            .attr("src", 'http://profit24.pl/images/photos/' + aBook.directory + '/__b_' + aBook.photo)
//            .attr("onError", "this.onerror=null; this.src='"+ 'http://profit24.pl/images/photos/' + aBook.directory + '/' + aBook.photo  +"';");
  }
  $("#main_isbn").html(aBook.order_number + ', ' + aBook.transport_number);
  $("#main_quantity").show();
  $("#main_quantity span").html(aBook.quantity);
  $("#main_weight").show();
  $("#main_weight span").html(aBook.weight);
  $("#main_shelf_number").html(aBook.shelf_number);
}
    
    
function showLastBookArray(aLastBookArray) {
  for (var i=0; i<=2; i++) {
    
    if (aLastBookArray[i] !== undefined && aLastBookArray[i] !== null) {
      aBook = aLastBookArray[i];
//      if (aBook.photo !== undefined) {
//        $("#image_"+ i)
//              .attr("src", 'http://profit24.pl/images/photos/' + aBook.directory + '/__b_' + aBook.photo)
//              .attr("onError", "this.onerror=null; this.src='"+ 'http://profit24.pl/images/photos/' + aBook.directory + '/' + aBook.photo  +"';");
//      }
      $("#isbn_"+ i).html(aBook.order_number + ', ' + aBook.transport_number);
      $("#quantity_"+ i).show();
      $("#quantity_"+ i + " span").html(aBook.quantity);
      $("#weight_"+ i).html(aBook.weight);
      $("#shelf_number_"+ i).html(aBook.shelf_number);
    } else {
      return null;
    }
  }
}
function showBooksImages(aOrderShelf) {
  $("#left_images_books").show();
  $("#books_images").html("");
  
  var aBookImage = aShelfImages[aOrderShelf['order_id']];
  for (var i = 0; i < aBookImage.length && i < 10; i++) {
    var aBookItem = aBookImage[i];
    console.log(aBookItem);
    for (var q = 0; q < aBookItem.quantity; q++) {
      $("#books_images").html($("#books_images").html() + 
              '<img style="width:70px;" ' +
                 ' src="http://profit24.pl/images/photos/' + aBookItem.directory + '/__b_' + aBookItem.photo + '" '+
                 ' onError="this.onerror=null; this.src=\'http://profit24.pl/images/photos/' + aBookItem.directory + '/' + aBookItem.photo  + '\';" /> <br />'
              );
    }
    
  }
}

iScanned = 0;
lastBook = ({});
scannedBooks = ({});
$(document).keydown(function(e){

  //var focused = $(":focus");
  if ($("#search_isbn").is(":focus") === true) {
    if(e.keyCode === 13){
      if ($.isEmptyObject(lastBook) === false) {
        scannedBooks[2] = scannedBooks[1];
        scannedBooks[1] = scannedBooks[0];
        scannedBooks[0] = lastBook;
      }
      var aBook = doSearchISBN($("#search_isbn").val());
      lastBook = aBook;
      if (aBook !== null) {
        if (scannedBooks !== null) {
          showLastBookArray(scannedBooks);
        }
        incrementBook(aBook);
        updateBook(aBook);

        showBooksImages(aBook);
        iScanned++;
        $("#left_order_items").html(--quantityAllBooks);
      }
      
      $("#search_isbn").val("");
    }
  } else {
    $("#search_isbn").focus();
  }
});


// przedłużenie sesji na sorterze
setInterval(function() {
    $.ajax({
      type: "GET",
      cache: false,
      async: false,
      url: "ajax/continueSession.php"
    });
 }, 60000);