function checkQuantity(oSelRow, oIsbn) {
  /** Sprawdzamy ilość **/
  var iLiczba = oSelRow.parent().parent().children("td:last").prev().prev().html();
  var oCurrQuantity = oSelRow.parent().parent().children("td:last").prev().children("input");
  var iCurrQuantity = oCurrQuantity.val();
  var bCond = oSelRow.parent().parent().children().eq(2).children('a').is('.order_deleted');
  
  iCurrQuantity = parseInt(iCurrQuantity) + 1;
  $(oCurrQuantity).val(iCurrQuantity);
  $(oCurrQuantity).parent().html(iCurrQuantity).append(oCurrQuantity);

  if (parseInt(iLiczba) === parseInt(iCurrQuantity)) {
    oSelRow.attr("checked", true).triggerHandler('click');
    if ('1' === oSelRow.parent().parent().attr("fast_track")) {
      var sCellId = oSelRow.parent().parent().children("td:last").attr("id");
      $.ajax({
        type: "GET",
        data: {
          cell_id: sCellId,
          fast_track_id: $("#fast_track_id").val()
        },
        cache: false,
        async: false,
        url: "ajax/lockOrderConfirmInSupply.php",
        success: function (responseJSON) {
          var ordersFastTrack = $("#fast_track_scanned_order_items").val();
          if (ordersFastTrack !== '') {
            var currentFastTrack = JSON.parse(ordersFastTrack);
          } else {
            var currentFastTrack = [];
          }

          currentFastTrack.push(sCellId);
          var json_value = JSON.stringify(currentFastTrack);
          $("#fast_track_scanned_order_items").val(json_value);
        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert("Błąd zapisywania składowej w fastTracku " + xhr.status);
        }
      });

      if (bCond) {
        oSelRow.parent().parent().attr('style', 'background-color: #7f7d1a;');
      } else {
        oSelRow.parent().parent().attr('style', 'background-color: #a37632;');
      }
    } else {
      if (bCond) {
        oSelRow.parent().parent().attr('style', 'background-color: #FC7C7C;');
      } else {
        oSelRow.parent().parent().attr('style', 'background-color: rgb(215, 211, 188);');
      }
    }
    oIsbn.attr('isbn', '-');
  }
}

$(document).keydown(function(e){
  var focused = $(":focus").eq(0);
  if ($(focused).attr("class") !== "editable_input" &&  $(focused).hasClass("important") === false) {
    $("#search_isbn").focus();
  } else if ($(focused).attr("id") === 'pack_number') {
    if(e.keyCode == 13){
      $("#fv_nr").focus();
      return false;
    } else {
      $(focused).focus();
    }
  } else if ($(focused).attr("id") === 'fv_nr') {
    if(e.keyCode == 13){
      $("#search_isbn").focus();
      return false;
    } else {
      $(focused).focus();
    }
  } else if ($(focused).attr("id") === 'additional_ean') {
    if(e.keyCode == 13){
      $("#additional_ean").focus();
    }
  } else if ($(focused).attr("id") === 'defect_ean') {
    if(e.keyCode == 13){
      $("#defect_ean").focus();
    }
  } else {
    if(e.keyCode == 13){
      var sWaga = parseFloat($(focused).val().replace(",", "."));
      $(focused).val(sWaga);
      if (sWaga > 0.00 && sWaga <= 15) {
        //$(focused).parent().parent().children("td:first").next().next().children("span.isbn").attr('isbn', '');
        $("#search_isbn").val("");
//              $(focused).parent().parent().children("td:first").children("input:checkbox").attr("checked", true).triggerHandler('click');
//              $(focused).parent().parent().attr('style', 'background-color: rgb(215, 211, 188);');
        //var oIsbn = $(focused).parent().parent().children("td:first").next().next().children("span.isbn").eq(0);
        var oIsbnNew = $(focused).parent().parent().find("span.isbn");
        checkQuantity($(focused).parent().parent().children("td:first").children("input:checkbox"), oIsbnNew);
        $(focused).blur();
      } else {
        $(this).focus();
        alert("Wprowadź prawidłową wagę");
        return false;
      }
      return false;
    }
  }
});
      
$(document).ready(function(){
  //$("#search_isbn").focus();
  $("#search_isbn").keydown(function(e){
    if(e.keyCode == 13){
      var sISBN = $(this).val();
      var oIsbn = $("span.isbn[isbn="+sISBN+"]").eq(0);
      var oSelRow = oIsbn.parent().parent().children("td:first").children("input:checkbox[checked!=\'checked\']");
      if ('1' === oIsbn.parent().parent().attr("fast_track")) {
        alert("Odłóż produkt na FastTracka");
      }
      if(oSelRow.html() !== undefined && oSelRow.html() !== null){
        $('html, body').animate({
          scrollTop: $(oSelRow).offset().top
        }, 0);

        var oWaga = oSelRow.parent().parent().children("td:last").children("input");
        var iWaga = oWaga.val();

        if (iWaga == "0,00") {
          alert("Uzupełnij wagę produktu");
          var iCellId = oSelRow.parent().parent().children("td:last").attr("id");
          $("#"+iCellId).click();
          return false;
        } 

        checkQuantity(oSelRow, oIsbn);


        $("#search_isbn").val("");
      } else {
        alert("UWAGA !!! \nNie znaleziono ISBN: "+sISBN);
        $("#search_isbn").val("");
      }
      $("#search_isbn").focus();
      return false;
    }
  });

  $(".viewDeleteCheckbox").change(function() {
    if ( false === this.checked) {
      if ('1' === $(this).parent().parent().attr("fast_track")) {
        var sCellId = $(this).parent().parent().children("td:last").attr("id");
        inputCurrquantity = $(this).parent().parent().find("[name*='currquantity']");
        textCurrquantity = $(this).parent().parent().find("[id*='currquantity']");


        $.ajax({
          type: "GET",
          data: {
            cell_id: sCellId,
            fast_track_id: $("#fast_track_id").val()
          },
          cache: false,
          async: false,
          url: "ajax/unlockOrderConfirmInSupply.php",
          success: function (responseJSON) {
            inputCurrquantity.val("0");
            var html = inputCurrquantity[0].outerHTML;
            textCurrquantity.html("0 "+html);
            var ordersFastTrack = $("#fast_track_scanned_order_items").val();
            if (ordersFastTrack !== '') {
              var currentFastTrack = JSON.parse(ordersFastTrack);
            } else {
              var currentFastTrack = [];
            }
            currentFastTrack.splice(sCellId, 1);
            var json_value = JSON.stringify(currentFastTrack);
            $("#fast_track_scanned_order_items").val(json_value);
            alert('Skladowa potwierdzona na fast tracka została pomyślnie cofnięta i nie może być już potwierdzona na liście !');
          },
          error: function (xhr, ajaxOptions, thrownError) {
            alert("Błąd zapisywania składowej w fastTracku " + xhr.status);
          }
        });
      }
    }
  });
});