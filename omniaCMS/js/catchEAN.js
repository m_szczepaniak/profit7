$(document).ready(function(){
    seconds__ = 0;
    start__ = true;
    $("input").keydown(function( e ) {
        if (e.which == 13) {
            stop__ = new Date() / 1000;
            if ((stop__ - seconds__) < 0.2) {
                var content = $(this).val();
                var matches = content.match(/^\w{12}$/);
                console.log(matches);
                if (matches != null) {
                    content = "0" + content;
                    $(this).val(content);
                    $(this).attr('value', content);
                }
            }
            start__ = true;
        } else {
            if (start__ == true && e.which != 17) {
                seconds__ = new Date() / 1000;
            }
            start__ = false;
        }
    });
});