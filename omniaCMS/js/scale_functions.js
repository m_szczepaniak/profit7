$(document).ready(function(){

    var locked = false;

    $(document).on('focusin', '[name*="[weight]"]', function(){
        console.log(scale.scaleIsActive);
        if (scale.scaleIsActive == true) {

            if (locked == false) {
                scale.startWeight();
            }

            locked = true;
        }
    });

    $(document).on('focusout', '[name*="[weight]"]', function(){

        if (scale.scaleIsActive == true) {

            $(this).trigger('stopScale', [$(this)]);
        }

        locked = false;
    });
});