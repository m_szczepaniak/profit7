var scale = {

    scaleLoaderAddress: '/omniaCMS/ajax/ScaleLoader.php',
    selectedScale: localStorage.getItem("selectedScale"),
    scaleIsHalted: false,
    scaleIsActive: false,

    _init: function()
    {
        this.scaleIsActive = true;
        this.resetScale();
        this.loadTemplate();
        this.actionsAfterLoadTemplate();
    },
    loadWeights: function()
    {
        var scales;

        $.ajax({
            url: this.scaleLoaderAddress,
            method: 'GET',
            async: false,
            data: {action: 'scaleList'}
        }).done(function(data) {

            scales = jQuery.parseJSON(data);
        });

        return scales;
    },
    actionsAfterLoadTemplate: function()
    {
        var scaleIsBlocked = localStorage.getItem('scaleIsBlocked');
        console.log(scaleIsBlocked);
        if (scaleIsBlocked == 'true') {
            console.log('asdasdsadsad');
            this.scaleIsActive = false;
            $(document).find('#blockScale').prop( "checked", true );
        }

        if (localStorage.getItem('selectedScale') == null) {
            localStorage.setItem("selectedScale", $('#scalesListBox').val());

            this.selectedScale = localStorage.getItem('selectedScale');
        }
    },
    loadTemplate: function()
    {
        var scaleList = this.loadWeights();

        var selectBox = '<select id="scalesListBox" name="scalesListBox">';

        for (var key in scaleList) {
            var value = scaleList[key];

            var selected = '';

            if (value.id == this.selectedScale) {
                selected = 'selected ';
            }

            selectBox += '<option '+selected+'value="'+value.id+'">'+value.name+'</option>';
        }

        selectBox += '</select>';

        var template = '<div id="scaleWrapper"><div id="scaleValue">0,00</div>';
        template += '<div id="scaleListWrapper">Wybierz wage: '+selectBox+'</div><label id="blockScaleLabel">Zablokuj wage<input id="blockScale" type="checkbox" /></label></div>';

        $('body').prepend(template);
    },
    resetScale: function(){

        this.scaleIsHalted = false;

        $('#scaleValue').html('0,00');
    },
    startWeight: function(){

        this.resetScale();

        var that = this;

        var refreshId = setInterval(function(){

            if (that.scaleIsActive == false) {
                clearInterval(refreshId);
            }

            if (that.scaleIsActive == true) {

                $.ajax({
                    url: that.scaleLoaderAddress,
                    method: 'GET',
                    async: true,
                    data: {action: 'scaleWeight', scaleId: that.selectedScale}
                }).done(function (data) {

                    var res = jQuery.parseJSON(data);

                    $('#scaleValue').html(res);
                    $('[name*="[weight]"]:focus').val(res);
                });

            }
        }, 1000);
    }
};

$(document).ready(function(){

    var scaleAccess = $('#scale_access').val();

    if (scaleAccess == 1) {
        scale._init();
    }

    $('#scalesListBox').on('change', function(){

        localStorage.setItem("selectedScale", $(this).val());

        scale.selectedScale = $(this).val();
    });

    $(document).on('stopScale', function(event, param){

        scale.scaleIsHalted = true;
    });

    $('#blockScale').on('change', function(){

        var value = $(this).is(':checked') ? false : true;
        var isBlocked = $(this).is(':checked') ? true : false;

        scale.scaleIsActive = value;
        localStorage.setItem("scaleIsBlocked", isBlocked);

        scale.resetScale();
    });
});
