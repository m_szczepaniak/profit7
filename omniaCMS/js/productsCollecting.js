$(function () {
    function submitData(product) {
        var productId = product.product_id;

        $.ajax({
            method: "POST",
            url: "ajax/highLevelCollecting.php",
            data: {item: product.id},
            cache: false,
            async: true
        })
        .success(function (data) {
            if (productId > 0) {
                $("#product_" + productId).hide();
                counter = parseInt($("#counter_quantity").html());
                $("#counter_quantity").html(counter-1);
            }
        })
        .error(function (jqXHR, data) {
            new Messi('Błąd potwierdzania ' + jqXHR.status + ' ' + jqXHR.statusText + ': ' + product.ean_13, {
                title: 'Błąd',
                modal: true
            });
        });

        return true;
    }

    function findProduct(barcode) {

        for (var key in products) {
            if (undefined != products[key]['idents'][barcode]) {
                var selectedProduct = products[key];
                return selectedProduct;
            }
        }
        return false;
    }


    $(document).keydown(function (e) {
        if ($("#search_isbn").is(":focus") === true) {
            if (e.which === 13) {
                var barcode = $("#search_isbn").val();
                var product = findProduct(barcode);
                if (false === product) {
                    new Messi('Nie znaleziono : ' + barcode, {title: 'Brak', modal: true});
                    $("#search_isbn").val("");
                    return false;
                }

                var result = submitData(product);
                if (result === true) {

                    $("#search_isbn").val("");
                    return false;
                }
            }
        } else {
            $("#search_isbn").focus();
        }
    });
});