function openSearchWindow(disc) {
  window.open("ajax/SearchProduct.php?ppid='.$iPId.'&pid='.$iId.'&site=profit24&mode=2&disc="+disc,"","menubar=0,location=0,scrollbars=1,resizable=1,toolbar=0,width=700,height=500");
};

function createCookie(name,value, days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
  }
  else var expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}

function eraseCookie(name) {
  createCookie(name,"",-1);
}

function showHideRow(id) {
  var status = 		document.getElementById("listContainer"+id).style.display;
  var newStatus=	"none";
  var label=			"'.$aConfig['lang'][$this->sModule]['show'].'";
  eraseCookie("displayedPromotionVal"+id);
  if(status=="none") {
    newStatus="";
    label=			"'.$aConfig['lang'][$this->sModule]['hide'].'";
    createCookie("displayedPromotionVal"+id,1);
  }

  document.getElementById("listContainer"+id).style.display=newStatus;
  document.getElementById("listContainer2_"+id).style.display=newStatus;
  document.getElementById("btnShowHide"+id).value=label;
}