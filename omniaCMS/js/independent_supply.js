$(document).keydown(function (e) {
    var focused = $(":focus");

    if (false === $(focused).hasClass("editable_input")) {
        $("#search_isbn").focus();
    } else {
        if (e.keyCode === 13) {
            if (false === $(focused).is('textarea')) {
                return false;
            }
        }
    }

});

function addProductQuantity(ean, quantity, scDate, requireWeight) {

    // sprawdzamy produkt
    $.ajax({
        method: "POST",
        url: "ajax/inventory.php",
        async: false,
        data: {
            ean_13: ean,
            sc_date: scDate,
            quantity: quantity
        },
        dataType: "json"
    })
        .done(function (html_items) {
            var new_item = "<table width=\"100%\" class=\"scanTable\"><tr><th>#ID</th><th>Okładka</th><th>EAN_13</th><th>Tytuł</th><th>Ilość</th><th>Waga</th></tr>";
            for (var key in html_items) {
                if (true === requireWeight) {
                    if (parseFloat(html_items[key].weight) === 0 || html_items[key].weight == null) {
                        var weightProd = "";
                        while ($.isNumeric(weightProd = prompt("Podaj wagę produktu ean " + html_items[key].ean_13 + " " + html_items[key].name + " !!!!").replace(',', ".")) === false) {
                        }
                        weightProd = weightProd.replace(',', '.')
                        html_items[key].weight = parseFloat(weightProd);

                        $.ajax({
                            method: "POST",
                            url: "ajax/setWeight.php",
                            data: {
                                id: html_items[key].id,
                                weight: weightProd,
                                sc_date: $("#sc_date").val()
                            },
                            dataType: "json"
                        }).done(function (data) {
                            if (undefined != data.err) {
                                alert(data.err);
                            }
                        });

                    }
                }
                new_item += "<tr class='row-id-" + (html_items[key].last == true ? 'selected' : '' ) + "'><td>#" + key + "</td><td><img src='" + html_items[key].logo + "' style='max-weight: 90px; max-height: 90px' title='Okładka' /></td><td>" + html_items[key].ean_13 + "</td><td>" + html_items[key].name + "</td><td style='font-size:26px; color: red;'>" + html_items[key].quantity + "</td><td style='font-size:26px; color: red;'>" + html_items[key].weight + "</td></tr>";
            }
            new_item += "</table>";
            $("#sc_products").html(new_item);
            $("#search_isbn").val('');
            $('html, body').animate({
                scrollTop: $(".row-id-selected").offset().top
            }, 0);
        })
        .error(function () {
            new Messi('Błąd - nie znaleziono pozycji : ' + ean, {
                title: 'Błąd',
                modal: true
            });
        })
}

$(document).ready(function () {
    $("#search_isbn").keydown(function (e) {
        if (e.keyCode === 13) {
            if ($(this).hasClass('editable_input')) {
                $("#search_isbn").focus();
                return false;
            }
            var quantity = 1;
            if ($('#ask_quantity:checked').length) {
                while ($.isNumeric(quantity = prompt("Podaj ilość")) === false) {
                }
            }

            var ean = $("#search_isbn").val();
            var scDate = $("#sc_date").val();
            addProductQuantity(ean, quantity, scDate, true);
        }
    });

    $("#ordered_itm").on('submit', function () {
        if (!confirm('Czy na pewno chcesz zapisać ?')) {
            return false;
        }
    });
});
