/* global aBooksToSend, aBooks, quantityAllBooks */

function doSearchISBN(searchISBN) {


  for(var i = 0; i < aBooks.length; i++)
  {
    if(searchISBN !== "" &&
      (aBooks[i].isbn_plain === searchISBN || 
       aBooks[i].isbn_10 === searchISBN ||
       aBooks[i].isbn_13 === searchISBN ||     
       aBooks[i].ean_13 === searchISBN) &&
       aBooks[i].checked === '0'
       )
    {
      aBooksToSend.push(aBooks[i]);
      return aBooks[i];
    }
  }

  new Messi('Nie znaleziono : ' + searchISBN, {title: 'Brak', modal: true});
  $(window.document).focus();
  return null;
}

function incrementBook(aBook) {
  aBook.currquantity = parseInt(aBook.currquantity) + 1;
  if (parseInt(aBook.currquantity) >= parseInt(aBook.quantity)) {
    aBook.checked = '1';
  }
}

function updateBook(aBook) {
  // główny produkt
  if (aBook.photo !== undefined && aBook.photo !== "") {
    $("#main_image")
            .attr("src", 'http://profit24.pl/images/photos/' + aBook.directory + '/__b_' + aBook.photo)
            .attr("onError", "this.onerror=null; this.src='"+ 'http://profit24.pl/images/photos/' + aBook.directory + '/' + aBook.photo  +"';");
  }
  $("#main_isbn").html(aBook.isbn_plain + ', ' + aBook.isbn_10 + (aBook.isbn_13 !== "" ? ', ' + aBook.isbn_13 : '') + (aBook.ean_13 !== "" ? ', ' + aBook.ean_13 : ''));
  $("#main_name").html(aBook.name);
  if (aBook.desc !== undefined) {
    $("#main_desc").html(aBook.desc);
  } else {
    $("#main_desc").html(aBook.publisher + ' ' + aBook.publication_year + '<br />' + aBook.authors);
  }
  $("#main_shelf_number").html(aBook.shelf_number);
}
    
    
function showLastBookArray(aLastBookArray) {
  for (var i=0; i<=2; i++) {
    
    if (aLastBookArray[i] !== undefined && aLastBookArray[i] !== null) {
      aBook = aLastBookArray[i];
      if (aBook.photo !== undefined) {
        $("#image_"+ i)
              .attr("src", 'http://profit24.pl/images/photos/' + aBook.directory + '/__b_' + aBook.photo)
              .attr("onError", "this.onerror=null; this.src='"+ 'http://profit24.pl/images/photos/' + aBook.directory + '/' + aBook.photo  +"';");
      }
      $("#isbn_"+ i).html(aBook.isbn_plain + ', ' + aBook.isbn_10 + ', ' + aBook.isbn_13 + ', ' + aBook.ean_13);
      $("#name_"+ i).html(aBook.name);
      if (aBook.desc !== undefined) {
        $("#main_desc").html(aBook.desc);
      } else {
        $("#desc_"+ i).html(aBook.publisher + ' ' + aBook.publication_year + ' / ' + aBook.authors);
      }
      $("#shelf_number_"+ i).html(aBook.shelf_number);
    } else {
      return null;
    }
  }
}

function veryfiList() {
  console.log(aBooks);
  aShowItems = Array();
  iCount = 0;
  for(var i = 0; i < aBooks.length; i++)
  {
    if(aBooks[i].checked === '0')
    {
      iCount++;
      aShowItems.push(aBooks[i]);
    }
  }
  if (iCount > 6) {
    new Messi('Za dużo pozycji pozostało do zeskanowania', {title: 'Brak', modal: true});
  } else {
    $("#not_scanned").show();
    var string = '';
    for(var i = 0; i < aShowItems.length; i++)
    {
      string = string + '<div class="list_scanned_item">' 
        + '<img class="sc_image" src="http://profit24.pl/images/photos/' + aShowItems[i].directory + '/' + aShowItems[i].photo + '" /><br />'
        + '<span class="sc_shelf">' + aShowItems[i].shelf_number + '</span><br />'
        + 'Egz. do zesk.: <span class="sc_quantity"> ' + (aShowItems[i].quantity - aShowItems[i].currquantity) + '</span><br />'
        + '<span class="sc_name">' + aShowItems[i].name + '</span><br />'
        + '<span class="sc_desc">' + aShowItems[i].publisher + ' ' + aShowItems[i].publication_year + ' / ' + aShowItems[i].authors + '</span><br />'
        + '<span class="sc_isbn">' + aShowItems[i].isbn_plain + ', ' + aShowItems[i].isbn_10 + ', ' + aShowItems[i].isbn_13 + ', ' + aShowItems[i].ean_13 + '</span>'
        + '</div>';
    }
    $("#not_scanned_sc").html(string);
  }
}


lastBook = ({});

$(document).keydown(function(e){

  //var focused = $(":focus");
  if ($("#search_isbn").is(":focus") === true) {
    if(e.keyCode === 13){
      if ($.isEmptyObject(lastBook) === false) {
        scannedBooks[2] = scannedBooks[1];
        scannedBooks[1] = scannedBooks[0];
        scannedBooks[0] = lastBook;
      }
      var aBook = doSearchISBN($("#search_isbn").val());
      lastBook = aBook;
      if (aBook !== null) {
        if (scannedBooks !== null) {
          showLastBookArray(scannedBooks);
        }
        incrementBook(aBook);
        updateBook(aBook);

        iScanned++;
      }
      var leftQuantity = (quantityAllBooks - iScanned);
      $("#left_order_items").html(leftQuantity);

      if(typeof(Storage) !== "undefined") {
        var iListId = $("#orders_items_lists_id").val();
        localStorage.setItem("leftQuantity[" + iListId +"]", leftQuantity);
        localStorage.setItem("aBooks[" + iListId +"]", JSON.stringify(aBooks));
        localStorage.setItem("aBooksToSend[" + iListId +"]", JSON.stringify(aBooksToSend));
        localStorage.setItem("scannedBooks[" + iListId +"]", JSON.stringify(scannedBooks));
      }
      $("#search_isbn").val("");
      return false;
    }
  } else {
    $("#search_isbn").focus();
  }
});
$(function() {
  $("#debug_auto_confirm").click ( function () {
    var quantityBooks = 0;
    for(var i = 0; i < aBooks.length; i++)
    {
      quantityBooks += parseInt(aBooks[i].quantity);
    }

    for(var f = 0; f < quantityBooks; f++)
    {
      for (var i = 0; i < aBooks.length; i++) {
        var aBook = aBooks[i];
        if ($.isEmptyObject(lastBook) === false) {
          scannedBooks[2] = scannedBooks[1];
          scannedBooks[1] = scannedBooks[0];
          scannedBooks[0] = lastBook;
        }
        lastBook = aBook;
        if (scannedBooks !== null) {
          showLastBookArray(scannedBooks);
        }
        incrementBook(aBooks[i]);
        updateBook(aBooks[i]);
        aBooksToSend.push(aBooks[i]);
      }
    }
  });
});
// przedłużenie sesji na sorterze
setInterval(function() {
    $.ajax({
      type: "GET",
      cache: false,
      async: false,
      url: "ajax/continueSession.php"
    });
 }, 60000);