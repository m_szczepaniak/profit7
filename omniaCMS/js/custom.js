$(window).load(function () {
    recreateItems();

    $('#left_order_items').on('DOMNodeInserted', function(e) {
        recreateItems();
    });

    $('#go_back').on('click', function(){
        recreateItems();
    });
});

function recreateItems()
{
    $(".location-style").each(function () {
        var html = $(this).html();
        var separator = '';

        var newHtml = html.replace(/(17|11|20|15)[0-9]{8}/g, function(element){

            var matches = [];

            matches[0] = element.substr(0,2);
            matches[1] = element.substr(2,2);
            matches[2] = element.substr(4,3);
            matches[3] = element.substr(7,2);
            matches[4] = element.substr(9,1);
            var counted = matches.length;

            var htmlp = '<span class="loc-wrapper">';
            var i = 1;
            $.each( matches, function( key, value ) {
                htmlp += '<span class="loc-element loc-style'+i+'">'+value+'</span>';

                if (i < counted) {
                    htmlp += '<span class="loc-element loc-separator">'+separator+'</span>';
                }

                i++;
            });

            htmlp += '</span>';
            return htmlp;
        })

        $(this).html(newHtml);
    });
}