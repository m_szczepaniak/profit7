$(function() {
    $('#shelf_id').on('change', function(){

        var val = $(this).val();
        var oilid_hidden = $("#oilid_hidden").val();

        $.ajax({
            url: "ajax/AjaxShelf.php?action=getShelfData&shelf_id="+val+"&oilid="+oilid_hidden,
            dataType: "json",
            success: function(data) {

                if (data.status != 1){
                    $("#shelf-container").html('');
                } else {
                    $("#shelf-container").html(data.content);
                }
            },
            type: 'GET'
        });
    });
});