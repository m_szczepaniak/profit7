$(document).keydown(function(e){
  var focused = $(":focus");
  if ($(focused).attr("class") !== "editable_input") {
    $("#search_isbn").focus();
  } else {
    if(e.keyCode === 13){
      return false;
    }
  }
});


$(document).ready(function(){
  //$("#search_isbn").focus();
  $("#search_isbn").keydown(function(e){
    if(e.keyCode === 13){
      var sISBN = $(this).val();
      var oIsbn = $("span.isbn[isbn="+sISBN+"]").eq(0);
      var oSelRow = oIsbn.parent().parent().children("td:first").children("input:checkbox[checked!=\'checked\']");
      if(oSelRow.html() !== undefined && oSelRow.html() !== null){
        $('html, body').animate({
          scrollTop: $(oSelRow).offset().top
        }, 0);

        /** Sprawdzamy ilość **/
        var iLiczba = oSelRow.parent().parent().children("td:last").prev().html();
        var oCurrQuantity = oSelRow.parent().parent().children("td:last").children("input");
        var iCurrQuantity = oCurrQuantity.val();
        iCurrQuantity = parseInt(iCurrQuantity)+1;
        $(oCurrQuantity).val(iCurrQuantity);
        $(oCurrQuantity).parent().html(iCurrQuantity).append(oCurrQuantity);
        if (parseInt(iLiczba) === parseInt(iCurrQuantity)) {
          oSelRow.attr("checked", true).triggerHandler('click');
          oSelRow.parent().parent().attr('style', 'background-color: rgb(215, 211, 188);');
        }

        $("#search_isbn").val("");

      } else {
        alert("UWAGA !!! \nNie znaleziono ISBN: "+sISBN);
        $("#search_isbn").val("");
      }
      $("#search_isbn").focus();
      return false;
    }
  });
});