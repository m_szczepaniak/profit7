/**
 * Biblioteka funkcji JavaScript dla okienek dialogowych
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2004 - 2006
 * @version   1.0
 */

  // Metoda odpowiedzialna za zmian? klasy stylu w wyniku zdarzenia onMouseOver
  function buttonOver(oButton) {
    if (!oButton.disabled) {
      oButton.className = "imageOver";
    }
  } // end buttonOver

  // Metoda odpowiedzialna za zmian? klasy stylu w wyniku zdarzenia onMouseOut
  function buttonOut(oButton) {
    var iSelected = oButton.getAttribute('iSelected');
    if (iSelected == false || iSelected == null) {
      oButton.className = "imageOut";
    }
  } // end buttonOut

  // Metoda odpowiedzialna za zmian? klasy stylu w wyniku zdarzenia onMouseDown
  function buttonDown(oButton) {
    if (!oButton.disabled) {
    	oButton.className = "imageDown";
		}
  } // end buttonDown

  // Metoda odpowiedzialna za zmian? klasy stylu w wyniku zdarzenia onMouseUp
  function buttonUp(oButton) {
    if (!oButton.disabled) {
      oButton.className = "imageOver";
    }
  } // end buttonUp


	// Usuwa wybrany plik
  function deleteFile(oButton, oForm, sType) {
    var sMsg = '';
    var sDel = '';
    var sFile = oForm.selectedFile.value;
    var sDir = oForm.currentDir.value;
    if (!oButton.disabled) {
    	if (sFile != '') {
      	sMsg = sDeleteFileMsg.replace('%s', sFile);
      	sDel = sFile;
    	}
    	if (confirm(sMsg)) {
      	// usuwamy wybrany plik
      	window.frames.dir_expl.location = "DirectoryExplorer.php" + oForm.action.substr(oForm.action.indexOf('?')) + "&currentDir=" + sDir + "&del=" + sDel + "&type=" + sType;
    	}
		}
  } // end delete()

  function preloadAllImages(aImages, sLook) {
    var oImage = null;
    for (var iI = 0; iI < aImages.length; iI++) {
      aPreloadedButtons[aImages[iI] + '_disabled'] = new Image();
      aPreloadedButtons[aImages[iI] + '_disabled'].src = '../../gfx/icons/explorer/' + aImages[iI] + '_disabled.gif';
      
      aPreloadedButtons[aImages[iI] + '_enabled'] = new Image();
      aPreloadedButtons[aImages[iI] + '_enabled'].src = '../../gfx/icons/explorer/' + aImages[iI] + '.gif';
    }
  } // end preloadAllImages


  // Metoda wylacza dzialanie wskazanego przycisku
  function disableButton(sButton, bDisable) {
    if ((bDisable && !document.getElementById(sButton).disabled) ||
      (!bDisable && document.getElementById(sButton).disabled)) {

      var oButton = document.getElementById(sButton);
      if (bDisable) {
        //alert(aPreloadedButtons[sButton + '_disabled'].src);
        oButton.src = aPreloadedButtons[sButton + '_disabled'].src;
      }
      else {
        oButton.src = aPreloadedButtons[sButton + '_enabled'].src;
      }
      oButton.disabled = bDisable;
      oButton.className = 'imageOut';
    }
  } // end disableButton()

  function checkUploadFileBoxIE(oForm) {
    var oCover    = document.getElementById('dialogCover');
    var oMessage  = document.getElementById('dialogMessage');
    var iDialogW  = parseInt(window.dialogWidth);
    var iDialogH  = parseInt(window.dialogHeight);
    var iMessgW   = 350;
    var iMessgH   = 50;
    
    if (oForm.uploadedFile.value == '') {
      return false;
    }
    
    // ukrywamy wszystkie listy rozwijane
    for (var i = 0; i < oForm.elements.length; i++) {
      if (oForm.elements[i].type == 'select-one') {
        oForm.elements[i].style.visibility = 'hidden';
      }
    }
    
    
    oCover.style.width          = iDialogW;
    oCover.style.height         = iDialogH;
    oCover.style.display        = 'block';
    oMessage.style.width        = iMessgW;
    oMessage.style.height       = iMessgH;
    oMessage.style.left         = parseInt((iDialogW - iMessgW) / 2);
    oMessage.style.top          = parseInt((iDialogH - iMessgH) / 4);
    oMessage.style.display      = 'block';
    return true;
  } // end checkUploadFileBoxIE()
  
  
  function checkUploadFileBoxGECKO(oForm) {
    var oCover    = document.getElementById('dialogCover');
    var oMessage  = document.getElementById('dialogMessage');
    var iDialogW  = parseInt(document.width);
    var iDialogH  = parseInt(document.height);
    var iMessgW   = 350;
    var iMessgH   = 50;

    if (oForm.uploadedFile.value == '') {
      return false;
    }
    
    oCover.style.width          = iDialogW+50;
    oCover.style.height         = iDialogH+50;
    oCover.style.display        = 'block';
    oMessage.style.width        = iMessgW;
    oMessage.style.height       = iMessgH;
    oMessage.style.left         = parseInt((iDialogW - iMessgW) / 2);
    oMessage.style.top          = parseInt((iDialogH - iMessgH) / 4);
    oMessage.style.display      = 'block';
    return true;
  } // end checkUploadFileBoxGECKO()