$(function() {
  $("#shelf_id").focus();
  $(document).keydown(function(e){
    if ($("#shelf_id").is(":focus") === true) {
      if (e.keyCode === 13) {
        $("#container_id").focus();
        return false;
      }
    }

    if ($("#container_id").is(":focus") === true) {
      if (e.keyCode === 13) {
        $("#shelf_id2").focus();
        return false;
      }
    }
  });
});