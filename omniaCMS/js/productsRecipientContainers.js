$( function () {
  //products
  
  
  // akcje
  $("#action_change_conainer").click ( function () {
    doScanContainer();
  });
  
  $("#action_mark_as_defect").click ( function () {
   markLastAsDefect(); 
  });
  
  
  $("input").keydown(function( event ) {
    var inputValue = $(this).val();
    
    if (event.keyCode === 13) {
      switch ($(this).attr("id")) {
        case 'container_id':
          doAjaxSetContainer(inputValue);
        break;
        case 'barcode':
          scanBarcodeBuffer(products_containers_quantity, products, inputValue);
        break;
      }
    }
  });
  
  
  // na start ustawiamy kuwetę
  doScanContainer();
  recountStats();
  
  /**
   * 
   * @returns {void}
   */
  function markLastAsDefect() {
    var item = products_containers_quantity.pop();
    item.defect = 1;
    products_containers_quantity.push(item);
    recountStats();
  }
  
  /**
   * 
   * @var pr product
   * @param {products.item} item
   * @returns {void}
   */
  function setProduct(item) {
    
    var pr = item.product;
    $("#main_product_cover").attr("src", item.cover);
    $("#main_product_name").html(pr.title);
    $("#main_product_ean_13").html(pr.ean13);
    $("#main_product_authors").html(pr.authors);
    $("#main_product_publisher").html(pr.publisher);
    $("#main_product_details").show();
  }
  
  /**
   * 
   * @param {products_containers_quantity} products_containers_quantity
   * @param {products} productsBuffer
   * @param {string} barcode
   * @returns {undefined}
   */
  function scanBarcodeBuffer(products_containers_quantity, productsBuffer, barcode) {
    var item = searchBarcodeInBuffor(productsBuffer, barcode);
    if (item !== false) {
      addToProductContainers(products_containers_quantity, item);
      setProduct(item);
      doScanBarcodes();
//      alert('Znaleziono w bufforze: '+item.product.title);
    }
    recountStats();
  }
    
    
  /**
   * Dodajemy do tablicy wynikowej nowy rekord
   * 
   * @param {products_containers_quantity} products_containers_quantity
   * @param {item} item
   * @returns {void}
   */
  function addToProductContainers(products_containers_quantity, item) {
    addHistoryItems(products_containers_quantity);
    
    var newItem = {};
    newItem = item;
    newItem.container_id = getSelectedContainer();
    products_containers_quantity.push(newItem);
  }
  
  /**
   * 
   * @param {products_containers_quantity} products_containers_quantity
   * @returns {void}
   */
  function addHistoryItems(products_containers_quantity) {
    var n=0;
    for (var i = products_containers_quantity.length; i > 0; i--) {
      var item = products_containers_quantity[i-1];
      updateItemHistory(n, item);
      showProductsHistory();
      n++;
    }
  }
  
  /**
   * 
   * @returns {void}
   */
  function recountStats() {
    
    $('#summary_left_scan').html((products.length - products_containers_quantity.length));
    $('#summary_scanned_items').html(products_containers_quantity.length);
    $('#summary_defect_items').html(getDefectQuantity(products_containers_quantity));
  }
  
  /**
   * Liczba defektów
   * 
   * @param {products_containers_quantity} products_containers_quantity
   * @returns {Number}
   */
  function getDefectQuantity(products_containers_quantity) {
    
    var quantity = 0;
    for(var i=0; i < products_containers_quantity.length; i++){
      if (products_containers_quantity[i].defect !== undefined && products_containers_quantity[i].defect === 1) {
        quantity++;
      }
    }
    return quantity;
  }
  
  /**
   * 
   * @returns {void}
   */
  function showProductsHistory() {
    $('#products_history').show();
  }
  
  /**
   * 
   * @param {int} i
   * @param {item} item
   * @returns {jQuery}
   */
  function updateItemHistory(i, item) {
    var oItemJQ = $("#history_item_" + i);
    changeItemHistoryData(oItemJQ, item);
  }
  
  /**
   * 
   * @param {jQuery} oItemJQ
   * @param {item} item
   * @returns {undefined}
   */
  function changeItemHistoryData(oItemJQ, item) {
    oItemJQ.find(".product_cover").attr('src', item.cover);
    oItemJQ.children(".product_name").html(item.product.title);
    oItemJQ.find(".product_ean_13").html(item.product.ean13);
    oItemJQ.find(".product_publisher").html(item.product.publisher);
    oItemJQ.find(".product_authors").html(item.product.authors);
    oItemJQ.find(".product_container_id").html(item.container_id);
    if (item.defect !== undefined && item.defect === 1) {
      oItemJQ.find(".product_defect").show();
    } else {
      oItemJQ.find(".product_defect").hide();
    }
  }
  
  /**
   * 
   * @returns {string}
   */
  function getSelectedContainer() {
    return $("#container_id").val();
  }
    
  /**
   * 
   * @param {products} productsBuffer
   * @param {string} barcode
   * @returns {product|false}
   */  
  function searchBarcodeInBuffor(productsBuffer, barcode) {
    
    for(var i=0;i < productsBuffer.length;i++){
      var item = jQuery.extend({}, productsBuffer[i]);
      item.numerek = parseInt(products_containers_quantity.length);
      if (item.product.isbnPlain == barcode) {
        return item;
      }
      
      if (item.product.ean13 == barcode) {
        return item;
      }
      
      if (item.product.isbn13 == barcode) {
        return item;
      }
      
      if (item.product.isbn10 == barcode) {
        return item;
      }
      
    }
    return false;
  }
  
  
  /**
   * 
   * @param {string} inputValue
   * @returns {void}
   */
  function doAjaxSetContainer(inputValue) {
    // ajax sprawdzamy kuwetę
    $.ajax({
      type: 'POST',
      url: 'ajax/checkContainer.php',
      data: {container_id: inputValue},
      cache: false,
      beforeSend: function( xhr ) {
        lockInputs();
      }
    }).done(function( msg ) {
      if (msg == '1') {
        // udało mi się zmienić kuwetę
        openLockedInputs();
        // przenosimy do skanowania EAN
        doScanBarcodes();
      } else {
        getOpacityMessage(msg);
      }
    }).fail(function() {
      getOpacityMessage('Wystąpił bład podczas sprawdzania kuwety, spróbuj ponownie za chwilę.');
    });
  }
  
  
  /**
   * 
   * @returns {void}
   */
  function doScanContainer() {
    selectInput("container_id", "barcode");
    $("#container_id").val('');
  }
  
  /**
   * 
   * @returns {void}
   */
  function doScanBarcodes() {
    selectInput("barcode", "container_id");
    $("#barcode").val('');
  }
    
  /**
   * 
   * @returns {void}
   */
  function openLockedInputs() {
    $("input, button, radio, select").removeAttr("DISABLED");
    $("input:not(.recipient_detail_unselected), button, radio, select").removeAttr("READONLY");
    $("#opacity_message").remove();
  }
  
  /**
   * 
   * @returns {void}
   */
  function lockInputs() {
    
    $("input, button, radio, select").attr("DISABLED", "DISABLED");
    $("input, button, radio, select").attr("READONLY", "READONLY");
    getOpacityMessage('Proszę czekać...');
  }
  
  
  /**
   * 
   * @param {string} message
   * @returns {void}
   */
  function getOpacityMessage(message) {
    
    if ($("#opacity_message").size() > 0) {
      $("#opacity_message_txt").html(message);
    } else {
      $("#products_recipient_containers").append('<div id="opacity_message">'
                                               +'<div style="position:absolute; top: 0; left: 0; width: 100%; height: 100%; margin: 0 auto; z-index: 9999; opacity: 0.8; display: block; background-color: #000;"></div>'
                                               +'<div style=" text-align: center; width: 100%; position: absolute; top: 50%;  margin: auto auto; z-index: 9999;color: #fff;" id="opacity_message_txt">'+message+'</div> '
                                               +'<button style="position: absolute; top: 40%; left: 70%; margin: 0 auto; z-index: 9999;color: #000;" id="action_close_opacity_message">Zamknij</button>'
                                               +'</div>');
      $("#action_close_opacity_message").click( function () {
        openLockedInputs();
      });  
    }
  }
  
  /**
   * 
   * @param {string} select
   * @param {string} unselect
   * @returns {void}
   */
  function selectInput(select, unselect) {
    $("#"+unselect).removeClass("recipient_detail_selected");
    $("#"+unselect).addClass("recipient_detail_unselected")
    $("#"+unselect).attr("READONLY", "READONLY");

    $("#"+select).removeClass("recipient_detail_unselected")
    $("#"+select).removeAttr("READONLY");
    $("#"+select).addClass("recipient_detail_selected");
    $("#"+select).focus();
  }
});

  