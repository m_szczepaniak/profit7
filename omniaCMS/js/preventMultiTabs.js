$(function () {
    var formLoadCount = parseInt(localStorage.getItem("FormLoadCount") || 0);
    if (formLoadCount == 0) {
        // OK
    }
    else {
        $("form[id='order_items'] :input[id*='status']").attr("onclick", "return false;");
        $("form[id='order_items'] :input[id*='source']").attr("onclick", "return false;");
        $("#order_additional_info").before("<h3 style='text-align: center; color: red;'>Zmiany odznaczania produktów zostały zablokowane.</h3>")
    }
    var formLoadCount = parseInt(localStorage.getItem("FormLoadCount") || 0);
    formLoadCount = parseInt(formLoadCount) + 1;
    localStorage.setItem("FormLoadCount", formLoadCount);

    $(window).unload(function () {
        var formLoadCount = parseInt(localStorage.getItem("FormLoadCount") || 0);
        formLoadCount = formLoadCount - 1;
        localStorage.setItem("FormLoadCount", formLoadCount);
    });
});