
$(document).keydown(function(e){
  var focused = $(":focus");
  if ($(focused).attr("class") !== "editable_input") {
    $("#search_isbn").focus();
  } else {
    if(e.keyCode === 13){
      $("#search_isbn").focus();
      return false;
    }
  }
  
});


$(document).ready(function(){
  //$("#search_isbn").focus();
  $("#search_isbn").keydown(function(e){
    if(e.keyCode === 13){
      var sISBN = $(this).val();
      var checked = false;
      $.each($("span.isbn[isbn="+sISBN+"]"), function () {
        if (checked === false) {
          var oIsbn = $(this);
          var oSelRow = oIsbn.parent().parent().children("td:first").children("input:checkbox[checked!=\'checked\']");
          if(oSelRow.html() !== undefined && oSelRow.html() !== null){
            checked = true;
            $('html, body').animate({
              scrollTop: $(oSelRow).offset().top
            }, 0);

            /** Sprawdzamy ilość **/
            var iLiczba = oSelRow.parent().parent().children("td:last").prev().html();
            var oCurrQuantity = oSelRow.parent().parent().children("td:last").children("input");
            var iCurrQuantity = oCurrQuantity.val();
            iCurrQuantity = parseInt(iCurrQuantity)+1;
            $(oCurrQuantity).val(iCurrQuantity);
            $(oCurrQuantity).parent().html(iCurrQuantity).append(oCurrQuantity);
            if (parseInt(iLiczba) === parseInt(iCurrQuantity)) {
              oSelRow.attr("checked", true).triggerHandler('click');
              oSelRow.parent().parent().attr('style', 'background-color: rgb(215, 211, 188);');
            }

            $("#search_isbn").val("");
          } else {

          }
          $("#search_isbn").focus();
          //return false;
        }
      });
      if (checked === false) {
        alert("UWAGA !!! \nNie znaleziono ISBN: "+sISBN);
        //$("#not_exists").val($("#not_exists").val() + sISBN + "\n");
        $("#search_isbn").val("");
        $("#search_isbn").focus();
        return false;
      } else {
        $("#search_isbn").val("");
        $("#search_isbn").focus();
        return false;
      }
    }
  });
});