<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
$aConfig['continue_session'] = true;
include_once('config/config.inc.php');
$aConfig['use_db_manager'] = true;
include_once('config/ini.inc.php');
include_once ('config/cache.inc.php');
include_once('Login.class.php');
include_once('lang/'.$aConfig['default']['language'].'.php');
// dolaczenie klasy admin obslugujacej CMS
include('Admin.class.php');

if (substr($_SERVER['HTTP_HOST'], 0, 3) !== 'www' &&
		strpos($_SERVER['DOCUMENT_ROOT'], 'projects') === false) {
	header("HTTP/1.1 301 Moved Permanently");
	header("location:https://www.".
				 substr($aConfig['common']['client_base_url'], 0, -1).
				 $_SERVER['REQUEST_URI']);
	header("Connection: close");
	exit;
}
if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""){
    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    header("Location: $redirect");
    exit;
}

// pobranie typu i wersji przegladarki
getBrowserInfo();

// utworzenie obiektu klasy Login
$pLogin           = new Login();

// pobranie nazwy i wersji CMS
setCMSVersion();

$pSmarty->assign_by_ref('sCharset', $aConfig['default']['charset']);
$pSmarty->assign('sTitle', $aConfig['common']['name'].' '.$aConfig['common']['version']);
$pSmarty->assign('sHeader', '<strong>'.$aConfig['common']['name'].'</strong> - '.$aConfig['lang']['login']['header']);
$pSmarty->assign('sBaseHref', $aConfig['common']['base_url_http']);


if (!$pLogin->IsLoggedIn()) {
	// wyswietlamy formularz logowania
 	// nastepnie jezeli zostal podany login i haslo
 	// wykonujemy logowanie
 	if (!empty($_POST)) {
 		if ($_POST['do'] == 'login') {
 			// wybrana akcja logowania
      $mLogin = $pLogin->DoLogin($pSmarty);
      if ($mLogin === false) {
 				$pSmarty->assign_by_ref('sMsg', $pLogin->GetErrMsg());
 				$pLogin->ShowLoginForm($pSmarty);
			}
			else if ($mLogin === true) {
				// logowanie pomyslne
				$pAdmin = new Admin($pSmarty);
			} else {
        // nic nie rób
      }
		}
    else if ($_POST['do'] == 'loginCode') {
      $mLogin = $pLogin->DoLoginCode();
      if ($mLogin === true ) {
				// logowanie pomyslne
				$pAdmin = new Admin($pSmarty);
      } else {
        $pSmarty->assign_by_ref('sMsg', $pLogin->GetErrMsg());
        $pLogin->ShowLoginForm($pSmarty);
      }
    } else {
			$pLogin->ShowLoginForm($pSmarty);
		}
	}
	else {
		$pLogin->ShowLoginForm($pSmarty);
	}
}
else {
 	// jestesmy zalogowani
 	if (isset($_GET['do']) && $_GET['do'] = 'logout') {
 	 	$pLogin->DoLogout();
 		header('Location: index.php');
 		exit();
  }
  $pAdmin = new Admin($pSmarty);
}
header('Content-Type: text/html; charset='.$aConfig['default']['charset']);
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if ($aConfig['common']['use_zlib']) {
	ob_start("ob_gzhandler");
}
$pSmarty->display('index.tpl');
?>