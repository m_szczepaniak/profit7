// JavaScript Document
/***********************************************************************************************
**
** Funkcje
** version 1.0
**
***********************************************************************************************/

var aSelOptions = new Array();



function ShowMessageBox(sPrefix, sMsg, sPostfix, bError) {
	alert(sPrefix + "\n" + sMsg + sPostfix);
	return !bError;
}

function showImg(oImg, oNewImg) {
	oImg.src = oNewImg.src;
}

function showPhoto(oA, sId, sImgSrc, sLoaderSrc, sHideTxt, sShowTxt) {
	if ((oDiv = document.getElementById(sId)) && (oImg = document.getElementById('img_' + sId))) {
		if (oDiv.style.display == 'none') {
			// pokazanie zdjecia
			oNewImg = new Image();
			oNewImg.onload = function() { showImg(oImg, oNewImg); }
			oNewImg.src = sImgSrc;
			oA.innerHTML = sHideTxt;
			oDiv.style.display = 'block';
		}
		else {
			// ukrycie zdjecia
			oA.innerHTML = sShowTxt;
			oDiv.style.display = 'none';
			oImg.src = sLoaderSrc;
		}
	}
}

function FocusFormField(oForm, sFieldName) {
  oForm[sFieldName].focus();
  if (parent.frames.length > 0) {
    parent.location.href = "index.php";
  }
} // end FocusFormField()

function RebuildCombo(oCombo, aOptions, mSelected, aFirstOption) {
  oCombo.options.length = 0;
  oCombo.options[0] = new Option(aFirstOption[1], aFirstOption[0]);
  if (aOptions != undefined) {
    for (var i = 1; i < aOptions.length; i++) {
      oCombo.options[i] = new Option(aOptions[i][1], aOptions[i][0]);
      if (aOptions[i][0] == mSelected) {
        oCombo.options[i].selected = true;
      }
    }
  }
} // end RebuildCombo function


function SetCookie(sName, mValue, iDays) {
  if (iDays) {
    var iDate = new Date();
    iDate.setTime(iDate.getTime()+(iDays*24*60*60*1000));
    var sExpires = "; expires="+iDate.toGMTString();
  }
  else var sExpires = "";
  document.cookie = sName+"="+mValue+sExpires+"; path=/";
}

function setPreviousSelected(oObj) {
	aSelOptions[oObj.name] = oObj.selectedIndex;
}

function preventSelectDisabled(oObj) {
  var bIsOptionDisabled = (oObj.options[oObj.selectedIndex].className == 'disabled');
  if(bIsOptionDisabled) {
    oObj.options[0].selected = true;
    return false;
  }
  return true;
}

function GetObject(sName) {
	return document.getElementById(sName);
}

function ChangeObjValue(sAttrib, sNewVal) {
	GetObject(sAttrib).value = sNewVal;
}

function SwapClass(oObj, sClassName, sMenu) {
	if (GetObject(sMenu).style.display != '') {
		oObj.className = sClassName;
	}
}

function SwapItemClass(oObj, sClassName) {
	if (sSelectedItem != oObj.id) {
		oObj.className = sClassName;
	}
}

function ExpandMenu(oObj, sMenu) {
	if (GetObject(sMenu).style.display == '') {
		GetObject(sMenu).style.display = 'none';
	}
	else {
		GetObject(sMenu).style.display = '';
	}
}

function LeftMenuNavigate(oObj, sHeader, sHref, sClassName) {
	if (sSelectedItem != '' && sSelectedItem != oObj.id) {
		GetObject(sSelectedItem).className = sClassName;
	}
	sSelectedItem = oObj.id;
	window.top.frames.moduleName.window.document.getElementById('moduleContent').innerHTML = '<strong>"' + sHeader + '"</strong>';
  window.top.frames.content.window.location.href = sHref;
}

function HeaderMenuNavigate(sHeader, sHref) {
	window.top.frames.moduleName.window.document.getElementById('moduleContent').innerHTML = '<strong>"' + sHeader + '"</strong>';
  window.top.frames.content.window.location.href = sHref;
  window.top.frames.leftMenu.window.location.href = "admin.php?frame=left";
}

function MenuNavigate(sHref) {
	window.top.frames.content.window.location.href = sHref;
}


function ToggleCheckboxes(oForm, sName, bSender) {
  if (!bSender) {
    // jezeli funkcja nie zostala wywolana poprzez klikniecie
    // checkboxa 'zaznacz wszystkie' nie zostaje zmieniony
    // jego stan
    oForm.check_all.checked = !oForm.check_all.checked;
  }
  
  for (var i=0; i < oForm.elements.length; i++) {
    if (oForm.elements[i].name.indexOf(sName) != -1 &&
    		!oForm.elements[i].disabled) {
      oForm.elements[i].checked = oForm.check_all.checked;
    }
  }
}

/**
 * Funkcja sprawdza czy zostal zaznaczony przynajmniej
 * jeden checkbox z zestawu o podanej nazwie
 */
function Checked(oForm, sName) {
  for (var i=0; i < oForm.elements.length; i++) {
    if (oForm.elements[i].name.indexOf(sName) != -1) {
      if (oForm.elements[i].checked) {
        return true;
      }
    }
  }
  return false;
}

function resizeWindowTo(iWidth, iHeight) {
	bNS = navigator.appName == 'Netscape';
	iWindowWidth = bNS ? window.innerWidth : document.body.clientWidth;
	iWindowHeight = bNS ? window.innerHeight : document.body.clientHeight;
	window.resizeBy(iWidth-iWindowWidth, iHeight-iWindowHeight);
}

/**
 * Funkcja synchronizuje nazwe symbol strony z jej nazwa usuwajac
 * znaki narodowe oraz zamieniajac duze litery na male i spacje na '_'
 */
function synchronizeSymbol(bDo) {
	var sName = document.getElementById('name').value.toLowerCase().replace(/^\s+|\s+$/g, '');
	var aFind = new Array(' ', "ą", "ż", "ś", "ź", "ę", "ć", "ń", "ó", "ł", "á", "ä", "č", "ď", "é", "í", "ľ", "ô", "š", "ť", "ú", "ý", "ž");
	var aReplace = new Array("-", "a",  "z",  "s",  "z",  "e",  "c",  "n",  "o", "l", "a", "a", "c", "d", "e", "i", "l", "o", "s", "t", "u", "y", "z");
	if (bDo) {
		// usuniecie znacznikow HTML
		sName = sName.replace(/<[^>]*>/g, '');
		// usuniecie znakow narodowych
		PCRE = /[^a-z0-9_]/g;
		for(i=0; i < aFind.length; i++) {
			PCRE.compile(aFind[i], "g");
			sName = sName.replace(PCRE, aReplace[i]);
		}
		// usuniecie wszystkich znakow spoza dozwolonego zestawu
		sOthers = /[^a-z0-9_-]/g;
		sMultiples = /-{2,}/g;
		sName = sName.replace(sOthers, '').replace(sMultiples, '-');
		
		// skrocenie do 255 znakow
		sName = sName.substr(0, 255);
		document.getElementById('symbol').value = sName;
	}
}

function moduleChangeInfo(oForm, sWarning) {
	var iOldModule = oForm.old_module_id.value;
	var iNewModule = oForm.module_id.options[oForm.module_id.options.selectedIndex].value;
	if (iOldModule != iNewModule && iNewModule != 0) {
		alert(sWarning);
	}
}

function toggleActivity(oCheckbox, fieldPrefix, disabledClass, enabledClass) {
	var oForm = oCheckbox.form;

	for (var i = 0; i < oForm.elements.length; i++) {
		if (oForm.elements[i].name.indexOf(fieldPrefix) == 0) {
			
			oForm.elements[i].disabled = !oCheckbox.checked;
			oForm.elements[i].className = (oCheckbox.checked ? enabledClass : disabledClass);
			document.getElementById(oForm.elements[i].name + '_label').style.display = (oCheckbox.checked ? 'inline' : 'none');
		}
	}
}

function toggleRecursive(sId) {
	var oCreatorCheck = GetObject('creator[' + sId + ']');
	var oPublisherCheck = GetObject('publisher[' + sId + ']');
	var oUsersCheck = GetObject('users_only[' + sId + ']');
	var oRecursiveCheck = GetObject('rec[' + sId + ']');
	var oForm = oRecursiveCheck.form;
	
	var sCreatorPCRE = new RegExp("^creator\\[" + sId + "(_[0-9]+)+\\]$", "g");
	var sPublisherPCRE = new RegExp("^publisher\\[" + sId + "(_[0-9]+)+\\]$", "g");
	var sUsersPCRE = new RegExp("^users_only\\[" + sId + "(_[0-9]+)+\\]$", "g");
		
	if (oRecursiveCheck.checked) {
		var bCreator = oCreatorCheck.checked;
		var bPublisher = oPublisherCheck.checked;
		var bUsers = oUsersCheck.checked;
		
		for (var iI = 0; iI < oForm.length; iI++) {
			if (sCreatorPCRE.test(oForm[iI].name)) {
				oForm[iI].checked = bCreator;
			}
			if (sPublisherPCRE.test(oForm[iI].name)) {
				oForm[iI].checked = bPublisher;
			}
			if (sUsersPCRE.test(oForm[iI].name)) {
				oForm[iI].checked = bUsers;
			}
		}
	}
}

function toggleModules() {
	var bCreatorPrivs = false;
	var oUMenusAccess = GetObject('umenus_access');
	var oUModules = GetObject('umodules');
	var oForm = GetObject('c_users');

	for (var i = 0; i < oForm.elements.length; i++) {
		if (oForm.elements[i].name.match(/creator\[[0-9]+\]/)) {
			if (oForm.elements[i].checked) {
				bCreatorPrivs = true;
				break;
			}
		}
	}
	oUModules.disabled = !bCreatorPrivs;
	oUMenusAccess.value = bCreatorPrivs ? '1' : '0';
}

function setMultiSelectOnSend(oObj){
	
 	if (oObj.options != undefined) {
		for (var i = 0; i < oObj.options.length; i++) {
			oObj.options[i].selected =true;
		}
	}
}

function sendMuliselects(){
	var elSelDest = document.getElementById('azymut_mapping');
	if(elSelDest){
		setMultiSelectOnSend(elSelDest);
	}
	var elSelDest2 = document.getElementById('abe_mapping');
	if(elSelDest2){
		setMultiSelectOnSend(elSelDest2);
	}
	var elSelDest3 = document.getElementById('publisher_mapping');
	if(elSelDest3){
		setMultiSelectOnSend(elSelDest3);
	}
	var elSelDest2 = document.getElementById('helion_mapping');
	if(elSelDest2){
		setMultiSelectOnSend(elSelDest2);
	}
}

function toggleRowVisibility(oA, sTHId, sTDId, sShowClass, sHideClass, sShowTxt, sHideTxt) {
	var oTH = document.getElementById(sTHId);
	var oTD = document.getElementById(sTDId);
	if (oA.className == 'showLink') {
		oTH.style.display = '';
		oTD.style.display = '';
		oA.innerHTML = sHideTxt;
		oA.className = sHideClass;
	}
	else {
		oTH.style.display = 'none';
		oTD.style.display = 'none';
		oA.innerHTML = sShowTxt;
		oA.className = sShowClass;
	}
}