<?php

use magazine\Containers;

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2016-03-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */

$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

function saveRandomLocation($iProductId, $sLocation) {
  global $pDbMgr;
  
  $type = [3,2];
  $oContainers = new Containers($pDbMgr, $type);
  $result = $oContainers->updateProductLocation($iProductId, $sLocation);
  if ($result === -2) {
    echo _('Wprowadzona kuweta jest nieprawidłowa.');
    die;
  } else if ($result === false) {
    echo _('Wystąpił błąd podczas zapisywania kuwety');
    die;
  }
}

if ($_SESSION['user']['id'] > 0) {
    if ($_POST['product_id'] > 0 && $_POST['container_id'] != '') {

        if ($_POST['container_id'] > 0 && $_POST['product_id']) {
            if (isset($_POST['random_pack_number']) && $_POST['random_pack_number'] == '1') {
                // czyścimy lokalizację G
//      $pDbMgr->Update('profit24', 'products_stock', ['profit_g_location' => ''], ' id = '.$_POST['product_id']);
            }
            saveRandomLocation($_POST['product_id'], $_POST['container_id']);
        }
        echo json_encode($_POST);
    } else {
        echo _('Wprowadzona kuweta jest nieprawidłowa.');
    }
}