<?php
/*
 * Sprawczamy, czy kontener jest zablokowany
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
//header('HTTP/1.1 500 No Record Found');die;
$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');
if ($_SESSION['user']['id'] > 0)  {
  if ($_POST['container_id'] != '') {
    $iContainerId = $_POST['container_id'];
    $oContainers = new \magazine\Containers($pDbMgr);
    $mContainerResult = $oContainers->lockContainer($iContainerId);
    if ($mContainerResult !== TRUE) {
      switch ($mContainerResult) {
        case -1:
          echo 'Wybrana kuweta jest zablokowana';
        break;
        case -2:
          echo 'Wybrana kuweta nie istnieje';
        break;
        default: 
          echo 'Wystąpił nieznany błąd podczas blokowania kuwety, spróbuj ponownie za chwilę';
      }
    } else {
      echo '1';
    }
  }
}