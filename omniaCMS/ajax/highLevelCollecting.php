<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 28.04.17
 * Time: 14:15
 */

$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = true;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

if (isset($_GET['item'])) {
    $itemId = intval($_GET['item']);
    $values['active'] = '0';
    if (false === $pDbMgr->Update('profit24', 'collecting_high_level', $values, ' id = '.$itemId)) {
        return 'ERROR';
    }
    return true;
}

