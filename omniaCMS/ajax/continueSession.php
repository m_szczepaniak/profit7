<?php
/**
 * Przedlużanie sesji
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-06-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */
$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = true;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');