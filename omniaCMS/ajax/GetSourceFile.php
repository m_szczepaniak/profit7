<?php
$iSource = intval($_GET['source']);
if ($iSource != '26' && $iSource != '31' && $iSource != '114') {
  die; 
}

$aConfig['common']['use_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

if(strlen($_GET['term'])>=2){
  
  if ($iSource == '26') {
    // super-7
    $sSourcePath = 'Siodemka';
  } else if ($iSource == '31') {
    // ateneum
    $sSourcePath = 'Ateneum';
  } else if ($iSource == '114') {
    // ateneum
    $sSourcePath = 'Panda/files';
  } else {
    die;
  }
  
  $sFilenamePath = $aConfig['common']['client_base_path'].
          'LIB/communicator/sources/' . $sSourcePath . 
          '/attachments/*' . $_GET['term'] . '*.[xX][mM][lL]';
  
  $aData = array();
  foreach (glob($sFilenamePath) as $filename) {
    $aMatched = array();
    preg_match('/(Fwz|FV|FS)/', $filename, $aMatched);
    if ($aMatched[1] != '') {
      $aData[] = $filename;
    }
  }
  echo json_encode($aData);
}