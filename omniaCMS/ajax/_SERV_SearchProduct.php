<?php
$aConfig['common']['use_session'] = true;
$aConfig['use_db_manager']=true;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');
include_once('../modules/m_oferta_produktowa/Module_Common.class.php');
include_once('../lang/pl.php');
include_once('../modules/m_oferta_produktowa/lang/admin_pl.php');
$_GET['module'] = 'm_oferta_produktowa';

if(empty($_GET['ppid']) && (!empty($_GET['mode']) || !empty($_GET['disc']))) {
	$_GET['ppid']=$_GET['mode'].','.$_GET['disc'];
}

if(!empty($_GET['ppid']) && strpos($_GET['ppid'], ',')>-1) {
	$aPpid=explode(',',$_GET['ppid']);
	$_GET['mode']=$aPpid[0];
	$_GET['disc']=$aPpid[1];
}

  /**
   * Metoda sprawdza czy podany string to ISBN
   * 
   * @param string $sStr
   * @return boolean
   */
  function SimpleCheckISBN($sStr) {
    
    $aMatched = array();
    preg_match("/^([0-9X]{8,13})$/", $sStr, $aMatched);
    if (isset($aMatched[1]) || (is_numeric($sStr) && $sStr > 0 && $sStr < 2018688)) {
      return true;
    }
    return false;
  }// end of SimpleCheckISBN() method
  
  
  /**
   * Metoda zmienia string na potrzeby wyszukiwania
   *  ciąg *mowa* zmieniany jest na %mowa%
   *  ciąg *mowa ciała* zamieniany jest na %mowa ciała%
   * 
   * @param string $sSearchStr
   * @return string
   */
  function parseSearchString($sSearchStr) {
    
    // preg_replace('/\s+/', '%', )
    if (mb_substr($sSearchStr, 0, 1) === '*') {
      $sSearchStr = '%'.mb_substr($sSearchStr, 1);
    }
    if (mb_substr($sSearchStr, -1, 1) === '*') {
      $sSearchStr = mb_substr($sSearchStr, 0, -1).'%';
    }
    return $sSearchStr;
  } // end of parseSearchString() method
  
  
	function SearchProduct($sSite) {
	global $aConfig, $pDbMgr;
	$oModule = new Module_Common();
	/*tryb pracy
	 * 0 - wyszukiwanie produktów do zamówienia
	 * 1 -           --//--          pakietow
	 * 2 -           --//--          promocji
	 * 3 -           --//--          
	 * 4 -           --//--       
	 * 5 -           --//--					kodow rabatowych w zamowieniach
	 */ 
	$iOMode=(isset($_GET['mode']) && $_GET['mode']>0)?intval($_GET['mode']):0;
	$sPacketAdditionalSql='';
	switch($iOMode) {
		case 0: {$iModuleId = getModuleIdM($sSite,'m_zamowienia_listy');
						$link=$aConfig['common']['base_url_http'].'admin.php?frame=main&module_id='.$iModuleId.'&module=m_zamowienia&ppid='.$_GET['ppid'].'&pid='.$_GET['pid'].'&do=add_product&id="+iId';
						} break;
		case 1: {$iModuleId = getModuleIdM($sSite,'m_oferta_produktowa');
						$link=$aConfig['common']['base_url_http'].'admin.php?frame=main&module_id=26&module=m_oferta_produktowa&do=edit_packet2&id='.$_GET['pid'].'&aid="+iId';
						$sPacketAdditionalSql=' AND A.packet=\'0\' AND A.prod_status <> \'0\' AND A.prod_status <> \'2\'';
						} break;
		case 2: {$iModuleId = getModuleIdM($sSite,'m_oferta_produktowa');
						$link=$aConfig['common']['base_url_http'].'admin.php?frame=main&module_id=26&module=m_oferta_produktowa&action=promotions&do=insertBooks&pid='.$_GET['pid'].'&aid="+iId+"&discount='.$_GET['disc'].'"';
						$sPacketAdditionalSql=' ';// AND A.packet=\'0\' // OR `shipment_date`>NOW() @mod 28.06.2013 r. M.Chudy wprowadza modyfikacje zeby wszystkie produkty mogly wejsc do tej promocji
						} break;
		case 3: {$iModuleId = getModuleIdM($sSite,'m_oferta_produktowa');
						$link=$aConfig['common']['base_url_http'].'admin.php?frame=main&module_id=26&module=m_oferta_produktowa&action=news&do=insert&dep='.$_GET['pid'].'&aid="+iId';
						$sPacketAdditionalSql='';
						} break;
		case 4: {$iModuleId = getModuleIdM($sSite,'m_oferta_produktowa');
						$link=$aConfig['common']['base_url_http'].'admin.php?frame=main&module_id=26&module=m_oferta_produktowa&action=bestsellers&do=insert&dep='.$_GET['pid'].'&aid="+iId';
						$sPacketAdditionalSql='';
						} break;
		case 5: {$iModuleId = getModuleIdM($sSite,'m_zamowienia_listy');
						$link=$aConfig['common']['base_url_http'].'admin.php?frame=main&module_id='.$iModuleId.'&module=m_zamowienia&action=codes&do=add_books&id='.$_GET['pid'].'&aid="+iId';
						$sPacketAdditionalSql=' AND A.packet=\'0\' ';
						} break;
		case 6: {$iModuleId = getModuleIdM($sSite,'m_oferta_produktowa');
						$link=$aConfig['common']['base_url_http'].'admin.php?frame=main&module_id=26&module=m_oferta_produktowa&action=free_delivery&do=insertBooks&pid='.$_GET['pid'].'&aid="+iId';
						$sPacketAdditionalSql='';
						} break;
		case 7: {$iModuleId = getModuleIdM($sSite,'m_oferta_produktowa');
						$link=$aConfig['common']['base_url_http'].'admin.php?frame=main&module_id=26&module=m_oferta_produktowa&action=previews&do=insert&dep='.$_GET['pid'].'&aid="+iId';
						$sPacketAdditionalSql='';
						} break;
	}
	
	
		$sHtml='
		<script>
		
			$(document).ready(function(){
				 $("#show_navitree").click(function() {
						window.open("NaviPopup.php",\'\',\'menubar=0,location=0,scrollbars=1,toolbar=0,width=500,height=300\');
					});
				$("#f_publisher").autocomplete("GetPublishers.php",{ minChars: 2, max: 100, matchContains: true, cacheLength: 1, matchSubset: false  });
							$("#f_publisher").bind("blur", function(e){
						   	 var elSelSeries = document.getElementById("f_series");
						   	 if(e.target.value != "") {  
				     	    $.get("GetSeries.php", { publisher: e.target.value },
									  function(data){
									    var brokenstring=data.split(";");
								    elSelSeries.length = 0;
									    elSelSeries.options[elSelSeries.length] =  new Option("'.$aConfig['lang']['common']['choose'].'","");
									    for (x in brokenstring){
												if(brokenstring[x]){
													var item=brokenstring[x].split("|");
						  						elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
					  						}
					  				}
									 });
						 			} else { elSelSeries.length = 0; elSelSeries.options[elSelSeries.length] =  new Option("'.$aConfig['lang']['common']['choose'].'",""); }
	    					});
			});
			function AddProductToOrder(iId){
			var loc = "'.$link.';
				window.opener.location.href = loc;
				self.close();
			}
		</script>';
		
		
	// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState('product_search');

		$aHeader = array(
			'header'	=> $aConfig['lang']['m_oferta_produktowa']['list'],
			'action'=>phpSelf(array('mode'=>$_GET['mode'], 'disc'=>$_GET['disc'])),
			'refresh_link'=>phpSelf(array('mode'=>$_GET['mode'], 'disc'=>$_GET['disc'],'reset'=>'1')),
			'search'	=> true,
			'refresh'	=> true,
			'checkboxes' => $iOMode?true:false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
		array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
		),
		array(
				'content'	=> 'Pak.',
				'sortable'	=> false,
				'width'	=> '50'
			),
		array(
				'db_field'	=> 'A.name',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_name'],
				'sortable'	=> true
		),
		array(
				'db_field'	=> 'A.vat',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_vat'],
				'sortable'	=> true,
				'width'	=> '30'
		),
		array(
				'db_field'	=> 'A.price_brutto',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_base'],
				'sortable'	=> true,
				'width'	=> '80'
		),
		array(
				'db_field'	=> 'price',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_price'],
				'sortable'	=> false,
				'width'	=> '80'
		),
		array(
				'db_field'	=> 'A.published',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_published'],
				'sortable'	=> true,
				'width'	=> '35'
		),
		array(
				'db_field'	=> 'source',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_source'],
				'sortable'	=> true,
				'width'	=> '120'
		),
		array(
				'db_field'	=> 'A.created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '140'
		),
		array(
				'db_field'	=> 'A.modified',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_modified'],
				'sortable'	=> false,
				'width'	=> '140'
		),
		array(
				'content' => $aConfig['lang']['common']['action'],
				'sortable' => false,
				'width' => '45'
		)
				);
				
		$aRecordsHeader2 = array(
		array(
				'content'	=> 'Pak.',
				'sortable'	=> false,
				'width'	=> '50'
			),
		array(
				'db_field'	=> 'A.name',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_name'],
				'sortable'	=> true
		),
		array(
				'db_field'	=> 'A.vat',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_vat'],
				'sortable'	=> true,
				'width'	=> '30'
		),
		array(
				'db_field'	=> 'A.price_brutto',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_base'],
				'sortable'	=> true,
				'width'	=> '80'
		),
		array(
				'db_field'	=> 'price',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_price'],
				'sortable'	=> false,
				'width'	=> '80'
		),
		array(
				'db_field'	=> 'A.published',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_published'],
				'sortable'	=> true,
				'width'	=> '35'
		),
		array(
				'db_field'	=> 'source',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_source'],
				'sortable'	=> true,
				'width'	=> '120'
		),
		array(
				'db_field'	=> 'A.created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '140'
		),
		array(
				'db_field'	=> 'A.modified',
				'content'	=> $aConfig['lang']['m_oferta_produktowa']['list_modified'],
				'sortable'	=> false,
				'width'	=> '140'
		),
		array(
				'content' => $aConfig['lang']['common']['action'],
				'sortable' => false,
				'width' => '45'
		)
				);

		if($_POST['f_price_from']<0 || $_POST['f_price_to']<0 || $_POST['f_price_from']>$_POST['f_price_to']) {
			$sMsg.=GetMessage($aConfig['lang']['m_oferta_produktowa']['invalid_price']);
				unset($_POST['f_price_from']);
				unset($_POST['f_price_to']);
			}
			
		$aRecordsFooter = array(
			array('check_all', 'remove_all')
		);
		$aRecordsFooterParams = array();		
        $sSearch = '';
        if (isset($_POST['search']) && !empty($_POST['search'])) {
          $_POST['search'] = trim($_POST['search']);
          if (SimpleCheckISBN($_POST['search'])) {
            $sSearch = ' AND ( 
                              A.id = ' . (int) $_POST['search'] . ' OR 
                              A.isbn_plain LIKE \'' . isbn2plain($_POST['search']) . '\' OR 
                              A.isbn_10 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                              A.isbn_13 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                              A.ean_13 LIKE \'' . isbn2plain($_POST['search']) . '\'
                              )';
          } else {
            $sSearchStr = parseSearchString($_POST['search']);
            $sSearch = ' AND (
                              A.name LIKE \'' . $sSearchStr . '\'
                              )';
          }
        }
        
        /*
				if(isset($_POST['search']) && !empty($_POST['search'])){
					$_POST['search']=trim($_POST['search']);
		//			$sSearch = preg_replace('/\s+/','%',$_POST['search'])		
					if(is_numeric($_POST['search']))	{
						$sSearch =  ' AND ( A.id = '.(int) $_POST['search'] .' OR A.isbn_plain LIKE \''.isbn2plain($_POST['search']).'\')';
					}
					elseif(preg_match('/[0-9]/',$_POST['search'])) {
						$sSearch = ' AND (A.name LIKE \'%'.preg_replace('/\s+/','%',$_POST['search']).'%\' OR A.isbn_plain LIKE \''.isbn2plain($_POST['search']).'\' OR A.name2 LIKE \'%'.preg_replace('/\s+/','%',$_POST['search']).'%\')';
					}
					else {
						$sSearch =  ' AND (A.name LIKE \'%'.preg_replace('/\s+/','%',$_POST['search']).'%\' OR A.name2 LIKE \'%'.preg_replace('/\s+/','%',$_POST['search']).'%\')';
					}
				}
         */
				$iNow=time();
        if (!empty($_POST)) {
          // pobranie liczby wszystkich rekordow
          $sSql = "SELECT COUNT(DISTINCT A.id)
               FROM ".$aConfig['tabls']['prefix']."products A ".
                 (isset($_POST['f_category']) && $_POST['f_category'] != '' ?
                   " LEFT JOIN ".$aConfig['tabls']['prefix']."products_extra_categories C
                   ON A.id = C.product_id" : "").
                 (isset($_POST['f_author']) && $_POST['f_author'] != '' ?
                  " LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_authors D
                   ON A.id = D.product_id
                   LEFT JOIN ".$aConfig['tabls']['prefix']."products_authors E
                   ON E.id = D.author_id" : "").
                   (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ?
                  " LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers F
                   ON A.publisher_id = F.id" : "").
                   (isset($_POST['f_image'])?" LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
                   ON I.product_id = A.id ":'').
                   (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ?
                 " LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series G
                   ON G.product_id = A.id" : '').
                  // ($_POST['f_price_from']>0 || $_POST['f_price_to']>0?' LEFT JOIN '.$aConfig['tabls']['prefix'].'products_tarrifs T ON T.product_id=A.id':'').
                   "
               WHERE 1=1".
                   ($sSearch != ''?$sSearch:'').
            ($_POST['f_price_from']>0?(' AND A.price_brutto>='.Common::FormatPrice2($_POST['f_price_from'])):'').			 	 
            ($_POST['f_price_to']>0?(' AND A.price_brutto<='.Common::FormatPrice2($_POST['f_price_to'])):'').		 	 
            (isset($_POST['f_category']) && $_POST['f_category'] != '' ? ' AND (A.page_id = '.$_POST['f_category'].' OR C.page_id = '.$_POST['f_category'].')' : '').
            (isset($_POST['f_published']) && $_POST['f_published'] != '' ? ' AND A.published = \''.$_POST['f_published'].'\'' : '').
            (isset($_POST['f_language']) && $_POST['f_language'] != '' ? ' AND A.language = '.$_POST['f_language'] : '').
            (isset($_POST['f_orig_language']) && $_POST['f_orig_language'] != '' ? ' AND A.original_language = '.$_POST['f_orig_language'] : '').
            (isset($_POST['f_binding']) && $_POST['f_binding'] != '' ? ' AND A.binding = '.$_POST['f_binding'] : '').
            (isset($_POST['f_year']) && $_POST['f_year'] != '' ? ' AND A.publication_year >= '.$_POST['f_year'] : '').
            (isset($_POST['f_yearto']) && $_POST['f_yearto'] != '' ? ' AND A.publication_year <= '.$_POST['f_yearto'] : '').
            (isset($_POST['f_created_to']) && $_POST['f_created_to'] != '' ? ' AND A.created <= "' . date('Y-m-d', strtotime($_POST['f_created_to'])) .' 23:59:59"' : '') .
            (isset($_POST['f_created_from']) && $_POST['f_created_from'] != '' ? ' AND A.created > "' . date('Y-m-d', strtotime($_POST['f_created_from'])) . ' 00:00:00"' : '') .
            //(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] != '' ? ' AND A.reviewed = \''.$_POST['f_reviewed'].'\'' : '').
            (isset($_POST['f_image']) && $_POST['f_image'] != '' ? ($_POST['f_image']=='0'?' AND I.id IS NULL':' AND I.id IS NOT NULL') : '').
            (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = \''.$_POST['f_source'].'\'' : '').
            (isset($_POST['f_shipment']) && $_POST['f_shipment'] != '' ? ' AND A.shipment_time = \''.$_POST['f_shipment'].'\'' : '').
            $sPacketAdditionalSql.//' AND A.prod_status = \'1\' '.
            (isset($_POST['f_type']) && $_POST['f_type'] != '' ?' AND A.packet = \''.$_POST['f_type'].'\'': '').
            (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ? ' AND F.name = \''.$_POST['f_publisher'].'\'' : '').
            (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != ''? ' AND G.series_id = '.$_POST['f_series'].'' : '').

              (isset($_POST['f_author']) && $_POST['f_author'] != '' ? ' AND CONCAT(E.name,\' \',E.surname,\' \',E.name) LIKE \'%'.$_POST['f_author'].'%\'' : '');
          $iRowCount = intval($pDbMgr->GetOne($sSite,$sSql));
        } else {
          $iRowCount = 500000;// tak na oko :)
        }
				if ($iRowCount == 0 && !isset($_GET['reset'])) {
					// resetowanie widoku
					resetViewState('product_search');
					// ponowne okreslenie liczny rekordow
					$sSql = "SELECT COUNT(id)
						 	 FROM ".$aConfig['tabls']['prefix']."products";
					$iRowCount = intval($pDbMgr->GetOne($sSite,$sSql));
				
				}

				$pView = new View('products', $aHeader, $aAttribs);
				if($iOMode)
					$pView->AddRecordsHeader($aRecordsHeader);
				else
					$pView->AddRecordsHeader($aRecordsHeader2);	
				//dump($_POST);
				// FILTRY
				//$pView->AddFilter('ab', '', '<form id="fileForm" name="" action="" method="post" enctype="multipart/form-data"><input type="file" name="file" />&nbsp;&nbsp;&nbsp;<input onclick="document.getElementById(\'fileForm\').submit(); return false;" type="submit" value="'.$aConfig['lang']['m_oferta_produktowa_promotions']['search_in'].'"></form>', '', 'html');
				//$pView->AddFilter('aa', '', '</td></tr><tr><td>', '', 'html');
				// kategoria Oferty produktowej
				
				$pView->AddFilter('f_category', $aConfig['lang']['m_oferta_produktowa']['f_category'], $oModule->getProductsCategoriesM($sSite,Common::getProductsMenuM($sSite,$_SESSION['lang']['id']), $aConfig['lang']['m_oferta_produktowa']['f_all']), $_POST['f_category']);
				$pView->AddFilter('f_category_btn', '', '<input type="button" name="nvtree" value="'.$aConfig['lang']['m_oferta_produktowa']['f_category_btn'].'" id="show_navitree" \>&nbsp;', '', 'html');
				
				
				// z okladka / bez okladki
				
				// typ produktu
				$aTypes = array(
					array('value' => '', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_all']),
					array('value' => '1', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_packet']),
					array('value' => '0', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_regular_product'])
				);
				$pView->AddFilter('f_type', $aConfig['lang']['m_oferta_produktowa']['f_type'], $aTypes, $_POST['f_type']);
				$pView->AddFilter('f_year', $aConfig['lang']['m_oferta_produktowa']['f_year'], $oModule->getYearsM($sSite), $_POST['f_year']);
				$pView->AddFilter('f_yearto', $aConfig['lang']['m_oferta_produktowa']['f_yearto'], $oModule->getYearsM($sSite), $_POST['f_yearto']);
				
				$pView->AddFilter('a', '', '</td></tr><tr><td id="FilterLine2">', '', 'html');
				$pView->AddFilter('f_language', $aConfig['lang']['m_oferta_produktowa']['f_language'], $oModule->getLanguagesM($sSite), $_POST['f_language']);
				$pView->AddFilter('f_orig_language', $aConfig['lang']['m_oferta_produktowa']['f_orig_language'], $oModule->getLanguagesM($sSite), $_POST['f_orig_language']);
				$pView->AddFilter('f_binding', $aConfig['lang']['m_oferta_produktowa']['f_binding'], $oModule->getBindingsM($sSite), $_POST['f_binding']);
				
				$pView->AddFilter('b', '', '</td></tr><tr><td id="FilterLine3">', '', 'html');
				
				// stan publikacji
				$aPublished = array(
				array('value' => '', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_all']),
				array('value' => '1', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_published']),
				array('value' => '0', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_not_published'])
				);
				$pView->AddFilter('f_published', $aConfig['lang']['m_oferta_produktowa']['f_publ_status'], $aPublished, $_POST['f_published']);
				
				$aSources = array(
				array('value' => '', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_all']),
				array('value' => '0', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_internalj']),
				array('value' => '1', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_internaln']),
				array('value' => '5', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_internalg']),
				array('value' => '6', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_internalx']),
				array('value' => '2', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_azymut']),
				array('value' => '3', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_abe']),
				array('value' => '4', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_helion']),
				array('value' => '7', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_dictum']),
				array('value' => '8', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_siodemka']),
				array('value' => '9', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_olesiejuk']),
				array('value' => '10', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_X']),
				);
				$pView->AddFilter('f_source', $aConfig['lang']['m_oferta_produktowa']['f_source'], $aSources, $_POST['f_source']);
				
				$aStatuses = array(
					array('value' => '', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_all']),
					array('value' => '1', 'label' => $aConfig['lang']['m_oferta_produktowa']['status_1']),
					array('value' => '0', 'label' => $aConfig['lang']['m_oferta_produktowa']['status_0']),
					array('value' => '2', 'label' => $aConfig['lang']['m_oferta_produktowa']['status_2']),
					array('value' => '3', 'label' => $aConfig['lang']['m_oferta_produktowa']['status_3'])
				);
				$pView->AddFilter('f_status', $aConfig['lang']['m_oferta_produktowa']['f_status'], $aStatuses, $_POST['f_status']);

                $pShipmentTime = new \orders\Shipment\ShipmentTime();

                $aShipments = array(
                    array('value' => '', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_all']),
                    array('value' => '0', 'label' => $pShipmentTime->getShipmentTime(null, 0)),
                    array('value' => '1', 'label' => $pShipmentTime->getShipmentTime(null, 1)),
                    array('value' => '2', 'label' => $pShipmentTime->getShipmentTime(null, 2)),
                    array('value' => '3', 'label' => $pShipmentTime->getShipmentTime(null, 3)),
                    array('value' => '4', 'label' => $pShipmentTime->getShipmentTime(null, 4)),
                    array('value' => '5', 'label' => $pShipmentTime->getShipmentTime(null, 5)),
                    array('value' => '6', 'label' => $pShipmentTime->getShipmentTime(null, 6)),
                    array('value' => '7', 'label' => $pShipmentTime->getShipmentTime(null, 7)),
                    array('value' => '8', 'label' => $pShipmentTime->getShipmentTime(null, 8)),
                    array('value' => '9', 'label' => $pShipmentTime->getShipmentTime(null, 9)),
                    array('value' => '10', 'label' => $pShipmentTime->getShipmentTime(null, 10)),
                );
				$pView->AddFilter('f_shipment', $aConfig['lang']['m_oferta_produktowa']['f_shipment'], $aShipments, $_POST['f_shipment']);
				
				$aImagesF = array(
				array('value' => '', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_all']),
				array('value' => '0', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_not_image'])
				);
				$pView->AddFilter('f_image', $aConfig['lang']['m_oferta_produktowa']['f_image'], $aImagesF, $_POST['f_image']);
				
				$pView->AddFilter('c', '', '</td></tr><tr><td id="FilterLine4">', '', 'html');
				
				//filtr ceny
				$pView->AddFilter('f_price_from', '', '<label>'.$aConfig['lang']['m_oferta_produktowa']['f_price'].'</label> <input type="text" name="f_price_from" value="'.$_POST['f_price_from'].'" style="width:50px;" maxlength="7" />', $_POST['f_price_from'], 'html');
				$pView->AddFilter('f_price_to', '', '<label>'.$aConfig['lang']['m_oferta_produktowa']['f_price_to'].'</label> <input type="text" name="f_price_to" value="'.$_POST['f_price_to'].'" style="width:50px;" maxlength="7" /> ', $_POST['f_price_to'], 'html');
				
						
				
				$pView->AddFilter('f_author', $aConfig['lang']['m_oferta_produktowa']['f_author'], array(), $_POST['f_author'], 'text');
				$pView->AddFilter('f_publisher', $aConfig['lang']['m_oferta_produktowa']['f_publisher'], array(), $_POST['f_publisher'], 'text');
				
				$pView->AddFilter('f_series', $aConfig['lang']['m_oferta_produktowa']['f_series'], $oModule->getPublisherSeriesListM($sSite,$_POST['f_publisher']), $_POST['f_series']);
				
				$pView->AddFilter('f_created_from', $aConfig['lang']['m_oferta_produktowa']['f_created_from'], array('style' => 'float: right;'),  (empty($_POST['f_created_from'])?'00-00-0000':$_POST['f_created_from']), 'date');
				$pView->AddFilter('f_created_to', $aConfig['lang']['m_oferta_produktowa']['f_created_to'], array('style' => 'float: right;'), (empty($_POST['f_created_to'])?'00-00-0000':$_POST['f_created_to']), 'date');

					$aIdList=array();
				if ($iRowCount > 0) {
					// dodanie Pagera do widoku
					$iCurrentPage = $pView->AddPager($iRowCount);
					$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
					$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
						
					// pobranie wszystkich rekordow
					$sSql = "SELECT A.id, A.page_id, A.packet, A.name, A.vat, A.price_brutto,
							0	AS price, F.name AS publisher, A.publication_year, 
							 A.published, A.source, A.created, A.created_by, A.modified, A.modified_by
						 	 FROM ".$aConfig['tabls']['prefix']."products A ".
						 	 (isset($_POST['f_category']) && $_POST['f_category'] != '' ?
							 	 " LEFT JOIN ".$aConfig['tabls']['prefix']."products_extra_categories C
							 	 ON A.id = C.product_id" : "").
							 (isset($_POST['f_author']) && $_POST['f_author'] != '' ?
								" LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_authors D
							 	 ON A.id = D.product_id
							 	 LEFT JOIN ".$aConfig['tabls']['prefix']."products_authors E
							 	 ON E.id = D.author_id" : "").
							 (isset($_POST['f_image'])?" LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id ":'').
							 (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ?
							 " LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series G
							 	 ON G.product_id = A.id" : '').
								" LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers F
							 	 ON A.publisher_id = F.id".
							 	 ($_POST['f_price_from']>0 || $_POST['f_price_to']>0?' LEFT JOIN '.$aConfig['tabls']['prefix'].'products_tarrifs T ON T.product_id=A.id':'').
							 	 "
						 	 WHERE 1=1".
							 ($sSearch != ''?$sSearch:'').
					//($_POST['f_price_from']>0?' AND T.price_brutto>='.Common::FormatPrice2($_POST['f_price_from']).' AND T.start_date<='.$iNow.' AND  T.end_date>='.$iNow.' AND T.price_brutto=(SELECT price_brutto FROM '.$aConfig['tabls']['prefix'].'products_tarrifs TT WHERE TT.product_id=A.id AND T.start_date<='.$iNow.' AND  T.end_date>='.$iNow.' ORDER BY price_brutto ASC LIMIT 1)':'').		 	 
					//($_POST['f_price_to']>0?' AND T.price_brutto<='.Common::FormatPrice2($_POST['f_price_to']).' AND T.start_date<='.$iNow.' AND  T.end_date>='.$iNow.' AND T.price_brutto=(SELECT price_brutto FROM '.$aConfig['tabls']['prefix'].'products_tarrifs TT WHERE TT.product_id=A.id AND T.start_date<='.$iNow.' AND  T.end_date>='.$iNow.' ORDER BY price_brutto ASC LIMIT 1)':'').
					($_POST['f_price_from']>0?(' AND A.price_brutto>='.Common::FormatPrice2($_POST['f_price_from'])):'').			 	 
					($_POST['f_price_to']>0?(' AND A.price_brutto<='.Common::FormatPrice2($_POST['f_price_to'])):'').			 	 
					(isset($_POST['f_category']) && $_POST['f_category'] != '' ? ' AND (A.page_id = '.$_POST['f_category'].' OR C.page_id = '.$_POST['f_category'].')' : '').
					(isset($_POST['f_published']) && $_POST['f_published'] != '' ? ' AND A.published = \''.$_POST['f_published'].'\'' : '').
					(isset($_POST['f_language']) && $_POST['f_language'] != '' ? ' AND A.language = '.$_POST['f_language'] : '').
					(isset($_POST['f_orig_language']) && $_POST['f_orig_language'] != '' ? ' AND A.original_language = '.$_POST['f_orig_language'] : '').
					(isset($_POST['f_binding']) && $_POST['f_binding'] != '' ? ' AND A.binding = '.$_POST['f_binding'] : '').
					(isset($_POST['f_year']) && $_POST['f_year'] != '' ? ' AND A.publication_year >= '.$_POST['f_year'] : '').
					(isset($_POST['f_yearto']) && $_POST['f_yearto'] != '' ? ' AND A.publication_year <= '.$_POST['f_yearto'] : '').
					(isset($_POST['f_created_to']) && $_POST['f_created_to'] != '' ? ' AND A.created <= "' . date('Y-m-d', strtotime($_POST['f_created_to'])) .' 23:59:59"' : '') .
					(isset($_POST['f_created_from']) && $_POST['f_created_from'] != '' ? ' AND A.created > "' . date('Y-m-d', strtotime($_POST['f_created_from'])) . ' 00:00:00"' : '') .
					//(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] != '' ? ' AND A.reviewed = \''.$_POST['f_reviewed'].'\'' : '').
					(isset($_POST['f_status']) && $_POST['f_status'] != '' ? ' AND A.prod_status = \'' . $_POST['f_status'] . '\'' : '') .
					(isset($_POST['f_image']) && $_POST['f_image'] != '' ? ($_POST['f_image']=='0'?' AND I.id IS NULL':' AND I.id IS NOT NULL') : '').
					(isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = \''.$_POST['f_source'].'\'' : '').
					(isset($_POST['f_shipment']) && $_POST['f_shipment'] != '' ? ' AND A.shipment_time = \''.$_POST['f_shipment'].'\'' : '').
					$sPacketAdditionalSql.// AND A.prod_status = \'1\' '.
					(isset($_POST['f_author']) && $_POST['f_author'] != '' ? ' AND CONCAT(E.name,\' \',E.surname,\' \',E.name) LIKE \'%'.$_POST['f_author'].'%\'' : '').
					(isset($_POST['f_type']) && $_POST['f_type'] != '' ?' AND A.packet = \''.$_POST['f_type'].'\'': '').
					(isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ? ' AND F.name = \''.$_POST['f_publisher'].'\'' : '').
					(isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != ''? ' AND G.series_id = '.$_POST['f_series'].'' : '').
							 ' GROUP BY A.id ORDER BY '.((isset($_GET['sort']) && !empty($_GET['sort']) && $_GET['sort'] != 'A.created') ? $_GET['sort'] : 'A.id').
					(isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : ' DESC').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
					$aRecords =& $pDbMgr->GetAll($sSite,$sSql);
//dump($sSql);
					foreach ($aRecords as $iKey => $aItem) {
						// stan publikacji
						$aRecords[$iKey]['published'] = $aConfig['lang']['common'][$aItem['published'] == '1' ? 'yes' : 'no'];
						$aIdList[]=$aItem['id'];
						// link podgladu
						//$aRecords[$iKey]['action']['links']['preview'] = (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'].'/' : '').(strlen($aItem['name'])>70?substr(link_encode($aItem['name']),0,70):link_encode($aItem['name'])).',product'.$aItem['id'].'.html?preview='.md5($_SERVER['REMOTE_ADDR'].$aConfig['common']['client_base_url_http']);

						$aRecords[$iKey]['created'] .= '<br />'.$aItem['created_by'];
						unset($aRecords[$iKey]['created_by']);
						
						$aRecords[$iKey]['modified'] .= '<br />'.$aItem['modified_by'];
						unset($aRecords[$iKey]['modified_by']);
						
						$aRecords[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($aItem['publisher'],0,40,'UTF-8').'</span>';
						unset($aRecords[$iKey]['publisher']);
						
						$aRecords[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($oModule->getAuthorsM($sSite,$aItem['id']),0,25,'UTF-8').' '.$aItem['publication_year'].'</span>';
						unset($aRecords[$iKey]['publication_year']);
						$aRecords[$iKey]['name']= '<a href="javascript:void(0);" onclick="AddProductToOrder('.$aItem['id'].');" target="_parent">'.$aRecords[$iKey]['name'].'</a>';
						
						$aRecords[$iKey]['source'] = $aConfig['lang']['m_oferta_produktowa']['source_'.$aItem['source']];
						
						$aRecords[$iKey]['action']['links']['preview'] = (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'] . '/' : '') . (mb_strlen($aItem['name'], 'UTF-8') > 210 ? mb_substr(link_encode($aItem['name']), 0, 210, 'UTF-8') : link_encode($aItem['name'])) . ',product' . $aItem['id'] . '.html?preview=' . md5($_SERVER['REMOTE_ADDR'] . $aConfig['common']['client_base_url_http']);

						// formatowanie ceny brutto
						$aTarrif = $oModule->getTarrifM($sSite,$aItem['id']);
						$aRecords[$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']);
						$aRecords[$iKey]['price'] = Common::formatPrice($aTarrif['price_brutto']);
//						if(!isset($_POST['f_reviewed']) || $_POST['f_reviewed'] != '0'){
//								$aRecords[$iKey]['disabled'][] = 'review';
//						}
						unset($aRecords[$iKey]['symbol']);
						
						if ($aItem['packet'] == '1') {
							$aRecords[$iKey]['packet'] = '[ '.$aConfig['lang']['common']['yes'].' ]';
						} else {
							$aRecords[$iKey]['packet'] = '&nbsp;';
						}
					}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
					),
				'page_id'	=> array(
					'show'	=> false
					),
					'action' => array(
							'actions' => array('preview'),
							'params' => array(
									'preview' => array('pid' => '{page_id}', 'id' => '{id}')
							),
							'show' => false
						 
						 )
					);
//					if(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] == '0'){
//						$aColSettings['action']['actions'][]= 'review';
//					}
					// dodanie rekordow do widoku
					$pView->AddRecords($aRecords, $aColSettings);
				}
				
		if(count($aid)==0) $aIdList[]=0;
				
		$sHtml.='
		<script>
		function serializeAndSend() {
			var aIds=new Array('.implode(',', $aIdList).');
			var sOut="";
			var iI=0;
			while(document.getElementById("delete["+aIds[iI]+"]")) {
				if(document.getElementById("delete["+aIds[iI]+"]").checked) {
					sOut=sOut+aIds[iI]+",";
					}
				++iI;
				}
			sOut=sOut.substr(0,sOut.length-1);
			if(sOut.length)	
				AddProductToOrder(sOut);
			else
				alert("Nie wybrano żadnej książki");		
			}
		</script>
		';		
		
		
		$sJs2='<script>
				function togleSpecialOpt(stat) {
					var option="none";
					var option2="";
					if(stat==true) { option=""; option2="none";}
					
					for(var i=1;i<6;++i) {
						if(document.getElementById("innerFilterLine"+i))
							 document.getElementById("innerFilterLine"+i).style.display=option;
							 
						if(document.getElementById("FilterLine"+i))
							 document.getElementById("FilterLine"+i).style.display=option2;
						}	 
					}
				</script>';	
		
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);	
		return $sHtml.$sMsg.$pView->Show().($iOMode?'<center><input onclick="serializeAndSend();" type="button" value="Dodaj zaznaczone" /></center><br />'.$sJs2:'');
	} // end of GetNaviTree() method
//	dump($_SESSION);
?>
	

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="Content-Language" content="{$sLanguage}">
  <meta name="Robots" content="NOFOLLOW, NOINDEX">
  <meta name="Pragma" content="no-cache">
  <meta name="Cache-Control" content="no-store, no-cache, must-revalidate">
  <title>Wybierz produkt z bazy <?php echo $pDbMgr->getDatabaseName($_GET['site']); ?></title>
  <link href="../css/styles.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" language="JavaScript" src="../js/functions.js"></script>
  <script src="../js/jquery-1.2.6.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="../js/jquery.autocomplete.min.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/theme.js" ></script>
  <script type="text/javascript" language="JavaScript" src="js/calendar/GCappearance.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/calendar/GurtCalendar.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/functions.js"></script>
	<link rel="stylesheet" href="css/calendar.css" type="text/css">


	
</head>
<body>
<?php 
if(isset($_SESSION['user'])){
	echo SearchProduct($_GET['site']);
} else {
	echo "<b>Zostałeś wylogowany - aby kontynuować zaloguj się ponownie!</b>";
}
?>

</body>
</html>
