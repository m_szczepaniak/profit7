<?php
use EntityManager\EntityManager;
use omniaCMS\lib\Products\ProductsStockView;

$tabType = empty($_GET['tab_type']) ? $_POST['tab_type'] : $_GET['tab_type'];

$aConfig['common']['use_session'] = true;
$aConfig['use_db_manager']=true;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');
include_once('../modules/m_oferta_produktowa/Module_Common.class.php');
include_once('../lang/pl.php');
include_once('../modules/m_oferta_produktowa/lang/admin_pl.php');
$_GET['module'] = 'm_zamowienia';

global $pDbMgr, $pSmarty;

$oProductsStockView = new ProductsStockView($pDbMgr, $pSmarty);
$oProductsStockView->setHideOnStart();
$aSourcesData = $pDbMgr->GetAll('profit24', 'SELECT * FROM sources AS S ORDER BY order_by');

$html = str_replace('display: none;', '', $oProductsStockView->getProductStockView($_GET['product_id'], $aSourcesData));

echo $html;