<?php
/**
 * Przedlużanie sesji
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-06-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */
$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = true;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

global $pDbMgr;

$aValues = array();

if (isset($_POST)&& !empty($_POST) ) {
  $_POST['orders_items_lists_id'] = intval($_POST['orders_items_lists_id']);
  
  if ($_POST['method'] == 'save') {
    $aValues = array(
        'books_serialized' => addslashes(json_encode((array)$_POST['aBooks'])),
        'books_to_send_serialized' => addslashes(json_encode((array)$_POST['aBooksToSend'])),
        'scanned_books_serialized' => addslashes(json_encode((array)$_POST['scannedBooks'])),
    );
    echo ($pDbMgr->Update('profit24', 'orders_items_lists', $aValues, ' id = '.$_POST['orders_items_lists_id'].' LIMIT 1') !== false ? 'OK' : 'ERROR');
  } 
  else {
    
    $_POST['orders_items_lists_id'] = intval($_POST['orders_items_lists_id']);
    
    // get
    $sSql = 'SELECT books_serialized, books_to_send_serialized, scanned_books_serialized
             FROM orders_items_lists
             WHERE id = '.$_POST['orders_items_lists_id'];
    
    header('Content-Type: application/json');
    $aData = $pDbMgr->GetRow('profit24', $sSql);
    if ($aData['books_serialized'] != '' && $aData['books_to_send_serialized'] != '') {
      $aNewObj['aBooks'] = (array)json_decode($aData['books_serialized']);
      $aNewObj['aBooksToSend'] = (array)json_decode($aData['books_to_send_serialized']);
      $aNewObj['scannedBooks'] = (array)json_decode($aData['scanned_books_serialized']);
      echo json_encode($aNewObj);
    }
  }
}
exit(0);