<?php
$aConfig['common']['use_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');
global $pDbMgr;

if (strlen($_GET['term']) >= 2) {
    if (empty($_GET['limit'])) {
        $_GET['limit'] = 50;
    }

    switch ($_GET['entity']) {
        case 'menus_items':
            $table = 'menus_items';
            $select = ' name ';

            $sSql = "SELECT A.id, CONCAT(IFNULL(B.name, ' -- '), ' > ', A." . $select . ") as name
					 FROM " . $aConfig['tabls']['prefix'] . $table . " AS A
					 LEFT JOIN " . $aConfig['tabls']['prefix'] . $table . " AS B
					 ON A.parent_id = B.id
					 WHERE A." . $select . " LIKE " . $pDB->quoteSmart('%' . stripslashes($_GET['term']) . '%') . "
					 AND A.link_to_id IS NULL
					  ORDER BY A.name LIMIT 0," . intval($_GET['limit']);

            break;
        case 'products_authors':
            $table = 'products_authors';
            $select = ' CONCAT(name, " ", surname) ';

            $sSql = "SELECT id, " . $select . " as name
					 FROM " . $aConfig['tabls']['prefix'] . $table . " 
					 WHERE " . $select . " LIKE " . $pDB->quoteSmart('%' . stripslashes($_GET['term']) . '%') . "
					  ORDER BY name LIMIT 0," . intval($_GET['limit']);

            break;
        case 'products_publishers':
            $table = 'products_publishers';
            $select = ' name ';

            $sSql = "SELECT id, " . $select . " as name
					 FROM " . $aConfig['tabls']['prefix'] . $table . " 
					 WHERE " . $select . " LIKE " . $pDB->quoteSmart('%' . stripslashes($_GET['term']) . '%') . "
					  ORDER BY name LIMIT 0," . intval($_GET['limit']);
            break;

    }
    if ($_GET['website'] > 0) {
        $sSqlWebsite = 'SELECT code FROM websites WHERE id = ' . $_GET['website'];
        $codeWebsite = Common::GetOne($sSqlWebsite);
    } else {
        $codeWebsite = 'profit24';
    }


//    $sSql = "SELECT id, " . $select . " as name
//					 FROM " . $aConfig['tabls']['prefix'] . $table . "
//					 WHERE " . $select . " LIKE " . $pDB->quoteSmart('%' . stripslashes($_GET['term']) . '%') . "
//					  ORDER BY name LIMIT 0," . intval($_GET['limit']);
    $aPublishers =& $pDbMgr->GetAll($codeWebsite, $sSql);
    $items = '';
    $aData = array();
    foreach ($aPublishers as $key => $value) {
        if ($_GET['opt'] == 'id') {
            $aData[] = $value['name'] . ' (' . $value['id'] . ')';
        } else {
            $aData[] = $value['name']. ' (' . $value['id'] . ')';
        }
    }
    echo json_encode($aData);
}
?>