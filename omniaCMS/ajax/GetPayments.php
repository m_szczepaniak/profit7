<?php
$aConfig['common']['use_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

$aPayments['ptype_postal_fee'] = "płatność przy odbiorze";
$aPayments['ptype_bank_transfer'] = "przelew - przedpłata na konto";
$aPayments['ptype_bank_14days'] = "przelew 14 dni";
$aPayments['ptype_dotpay'] = "Przelew elektroniczny (dotpay.pl)";
$aPayments['ptype_vat'] = "faktura VAT";
$aPayments['ptype_self_collect'] = "odbiór osobisty";
$aPayments['ptype_card'] = "Karta kredytowa";
$aPayments['ptype_platnoscipl'] = "Przelew elektroniczny (platnosci.pl)";

if(!empty($_GET['transport'])){
	$sSql = "SELECT id, ptype
				 		 FROM ".$aConfig['tabls']['prefix']."orders_payment_types
				 		 WHERE transport_mean = ".intval($_GET['transport'])." AND published = '1'
					 	 ORDER BY id"; 
	$aRecords=&Common::GetAll($sSql);
	$items='';
	foreach($aRecords as $iKey=>$aItem){
		$items .= $aPayments['ptype_'.$aItem['ptype']].'|'.$aItem['id'].";";
	}

	echo $items;
}
?>