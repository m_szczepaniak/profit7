<?php
/**
 * Przedlużanie sesji
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-06-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */
$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = true;
include_once('../config/config.inc.php');
$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = true;
include_once('../config/ini.inc.php');

function getCover($iId) {
  global $aConfig;
  
  // pobranie z bazy okladek
  $sSql = "SELECT *
						FROM " . $aConfig['tabls']['prefix'] . "products_images
						WHERE product_id = " . $iId . " LIMIT 0,1";

  $aResult = & Common::GetAll($sSql);
  if (!empty($aResult)) {
    foreach ($aResult as $iKey => $aValue) {
      if (file_exists($aConfig['common']['client_base_path'] . 'images/photos/' . $aValue['directory'] . '/__t_' . $aValue['photo'])) {
        return '/images/photos/' . $aValue['directory'] . '/__t_' . $aValue['photo'];
      }
      if (file_exists($aConfig['common']['client_base_path'] . 'images/photos/' . $aValue['directory'] . '/' . $aValue['photo'])) {
        return '/images/photos/' . $aValue['directory'] . '/' . $aValue['photo'];
      }
    }
  }
}


function sortByName($a, $b) {
  $sort = [$a['name'], $b['name']];
  sort($sort);

  if (array_pop($sort) == $a['name']) {
    return 1;
  } else {
    return -1;
  }
}

/**
 * @param $aProducts
 */
function clearLast($aProducts) {

  if (!empty($aProducts)) {
    foreach ($aProducts as $iKey => $aProduct) {
      $aProducts[$iKey]['last'] = false;
    }
    return $aProducts;
  }
}


if (isset($_GET['ean_13']) && isset($_GET['sc_date'])) {
  $aProducts = $_SESSION['inventory'][$_GET['sc_date']];
  include_once('../../import/CommonSynchro.class.php');
  $oCommonSynchro = new CommonSynchro();
  $aProduct = $oCommonSynchro->refMatchProductByISBNs(['ean_13' => $_GET['ean_13']], ['name', 'id', 'streamsoft_indeks', 'weight', 'standard_code', 'standard_quantity']);
  if (isset($aProduct['id'])) {
    $aProducts = clearLast($aProducts);
      $bIsMatchedByStandardCode = false;
      $incrementQuantity = (isset($_GET['quantity']) ? $_GET['quantity'] : 1);
      if ($aProduct['standard_code'] == $_GET['ean_13']) {
          $bIsMatchedByStandardCode = true;
          $incrementQuantity = intval($aProduct['standard_quantity']) * intval($_GET['quantity']);
      }

    $aProduct['name'] = trimString(str_replace(array('|', "\n"), '', $aProduct['name']), 60);
      if (isset($_GET['location'])) {
          $keyIdLocation = $aProduct['id'] . '_' . $_GET['location'];
      } else {
          $keyIdLocation = $aProduct['id'];
      }
    if (isset($aProducts[$keyIdLocation])) {
      $aProducts[$keyIdLocation]['quantity'] += $incrementQuantity;
      $aProducts[$keyIdLocation]['last'] = true;
    } else {
      $cover = getCover($aProduct['id']);
      $aProducts[$keyIdLocation] = $aProduct;
      $aProducts[$keyIdLocation]['logo'] = ($cover != ''? $aConfig['common']['client_base_url_http'].$cover : '');
      $aProducts[$keyIdLocation]['ean_13'] = $aProduct['streamsoft_indeks'];
      $aProducts[$keyIdLocation]['quantity'] = $incrementQuantity;
      $aProducts[$keyIdLocation]['last'] = true;
    }

      if (isset($_GET['location'])) {
          $aProducts[$keyIdLocation]['location'] = $_GET['location'];
      }

    $_SESSION['inventory'][$_GET['sc_date']] = $aProducts;



    usort($aProducts, 'sortByName');
    echo json_encode($aProducts);
  } else {
    echo 'BRAK PRODUKTU O PODANYM EAN';
  }
}