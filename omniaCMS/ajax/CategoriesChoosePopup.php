<?php
$aConfig['common']['use_session'] = true;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');
include_once('../modules/m_oferta_produktowa/Module_Common.class.php');
$sHTML = '';

function _getInternalSources() {
  global $aConfig;
  
  return array(
      array('label' => 'e', 'value' => 'e'), 
      array('label' => 'j', 'value' => 'j'), 
      array('label' => 'm', 'value' => 'm'), 
      array('label' => 'g', 'value' => 'g'), 
      array('label' => 'x', 'value' => 'x'), 
  );
}// end of _getInternalSources() function


function getInternalSourcePublishers($sInternalSource) {
  
  $sWhereSQL = '';
  $aAllInternalSources = _getInternalSources();
  foreach ($aAllInternalSources as $aItem) {
    
    if ($aItem['value'] == $sInternalSource) {
      $sWhereSQL .= ' AND PS.`profit_'.$aItem['value'].'_status` <> "0" ';
    } else {
      $sWhereSQL .= ' AND PS.`profit_'.$aItem['value'].'_status` = "0" ';
    }
  }
  
  $sSql = "SELECT DISTINCT PP.id AS value, PP.name AS label "
          . " FROM products_stock AS PS "
          . " JOIN products AS P "
          . " ON P.id = PS.id "
          . " JOIN products_publishers AS PP "
          . " ON PP.id = P.publisher_id "
          . " WHERE 1=1 "
          .$sWhereSQL.""
          . " ORDER BY PP.name ASC ";
  return Common::GetAll($sSql);
}// end of getInternalSourcePublishers() method


include_once('lib/Form/FormTable.class.php');
$oForm = new FormTable('Publishers', 'Wybierz źródło oraz wydawcę', array(), array('col_width' => 155));

$aData = array();
if (!empty($_POST)) {
  $aData = $_POST;
} elseif (isset($_GET['params']) && !empty($_GET['params'])) {
  $aParamsCategories = explode(';', $_GET['params']);
  $aData['categories'] = explode(',', $aParamsCategories[0]);
  $aData['internal_source'] = $aParamsCategories[1];
} else {
  $aData['internal_source'] = 'g';
}
$oForm->AddRow('Informacje', 'Filtr wybiera wydawnictwa produktów, które są dostępne wyłącznie w wybramym źródle wewnętrznym. '
        . '<br />Czyli jeśli wybierzemy filtr dla źródła G i książka ta występuje wyłącznie w tym źródle to jej wydawnictwo znajdzie się na liście wydawnictw poniżej.');
// najpierw dajemy do wyboru jakie źródło nas interesuje
$oForm->AddSelect('internal_source', _('Wybierz źródło'), array('onchange' => '$(\'#Publishers\').submit();'), _getInternalSources(), isset($aData['internal_source']) ? $aData['internal_source'] : 'g', '', false);
if (isset($aData['internal_source'])) {
  $aPublishers = getInternalSourcePublishers($aData['internal_source']);
  $aRadios = array();
  $oForm->AddMergedRow('<a href="javascript:void(0);" onclick="$(\'input:checkbox\').attr(\'checked\', \'checked\');">Zaznacz wszystkie</a> '
          . '/ <a href="javascript:void(0);" onclick="$(\'input:checkbox\').removeAttr(\'checked\');">Odznacz wszystkie</a>',
          array('class' => 'merged'));
  $oForm->AddInputButton('send', _('Wybierz'), array('class' => 'send_categories'));
  $oForm->AddCheckBoxSet('categories', _('Wybierz kategorię'), $aPublishers, isset($aData['categories']) ? $aData['categories'] : '', '', false, false);
  $oForm->AddInputButton('send2', _('Wybierz'), array('class' => 'send_categories'));
}

$sHTML .= $oForm->ShowForm();
?>
	

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="Content-Language" content="{$sLanguage}">
  <meta name="Robots" content="NOFOLLOW, NOINDEX">
  <meta name="Pragma" content="no-cache">
  <meta name="Cache-Control" content="no-store, no-cache, must-revalidate">
  <title>Wybierz kategorię</title>
  <link href="../css/styles.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" language="JavaScript" src="../js/functions.js"></script>
  <script src="../js/jquery-1.2.6.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/jquery.treeview.min.js"></script>
</head>
<body>
<script type="text/javascript">
  $(function() {
    $(".send_categories").click( function() {
      var fields = $( ":input:checkbox" ).serializeArray();
      $( "#f_hide_categories" ).empty();
      var sVal = "";
      jQuery.each( fields, function( i, field ) {
        sVal += "," + field.value;
      });
      sVal += ";" + $( "#internal_source" ).val();
      window.opener.$("#f_hide_categories").val(sVal);
      window.opener.$("#products").submit();
      self.close();
    })
  });
</script>
<?php 
if(isset($_SESSION['user'])){
	echo $sHTML;
} else {
	echo "<b>Zostałeś wylogowany - aby kontynuować zaloguj się ponownie!</b>";
}
?>

</body>
</html>
