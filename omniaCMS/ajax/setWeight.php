<?php
/**
 * Przedlużanie sesji
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-06-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */
$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = true;
include_once('../config/config.inc.php');
$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = true;
include_once('../config/ini.inc.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_oferta_produktowa/Module.class.php');

$oStdClass = new stdClass();
/** @var Module $oModule */
$oModule = new \Module($oStdClass, $oStdClass, TRUE);

if (isset($_GET['id']) && isset($_GET['weight']) && $_GET['weight'] > 0.00) {
    try {
        $_SESSION['inventory'][$_GET['sc_date']][$_GET['id']]['weight'] = $_GET['weight'];
        $tmp['weight'] = $_GET['weight'];

        $oModule->updateWeightOrders($_GET['id'], $tmp);

        $sSql = 'UPDATE products SET weight = "'.$_GET['weight'].'" WHERE  id = '.$_GET['id'].' LIMIT 1';
        Common::Query($sSql);

        $aProducts['ok'] = '1';
        echo json_encode($aProducts);
        die;
    } catch (Exception $ex) {
        $aProducts['err'] = 'Błąd ustawiania wagi produktu';
        echo json_encode($aProducts);
        die;
    }
}
$aProducts['err'] = 'Błąd ustawiania wagi produktu';
echo json_encode($aProducts);
die;