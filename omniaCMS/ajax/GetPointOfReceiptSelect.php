<?php
/**
 * Pobieranie listy wyboru punktów odbioru w zależności od wybranego rodzaju transportu
 * 
 * @author Paweł Bromka
 * @created 2014-12-05 
 * @copyrights Marcin Chudy - Profit24.pl
 */

$aConfig['common']['use_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');
include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';
header("Content-Type:text/html; charset=utf-8");


$sTransportId = intval($_POST['transport']);
preg_match('/^\w+$/', $_POST['selected'], $aMatches);
$sSelectedPointOfReceipt = $aMatches[0];

$oShipment = new \orders\Shipment($sTransportId, $pDbMgr);
$mList = $oShipment->getDestinationPointsDropdown();
if (is_array($mList)) {
  include_once('Form/FormTable.class.php');
  $pForm = new FormTable('point_of_receipt');  
  echo $pForm->GetSelectHTML('point_of_receipt', 'point_of_receipt', array('class' => 'point_of_receipt'), addDefaultValue($mList, _('-- wybierz --')), $sSelectedPointOfReceipt);
} else {
  echo $mList;
}