<?php
/**
 * Skrypt ajax do blokowania zamowien na których jest uzytkownik
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
if (!class_exists('memcached')) {
  echo '1';
  exit(0);
}

$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

$iOId = intval($_GET['order_id']);

if ($iOId > 0 && $_SESSION['user']['id'] > 0)  {
  $oMemcached = new Memcached();
  $oMemcached->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);

  include_once('../../LIB/orders_semaphore/ordersSemaphore.class.php');
  $oOrdersSemaphore = new ordersSemaphore($oMemcached);
  
  
  // sprawdźmy czy to zamówienie jest zalockowanie przez kogoś innego
  $mRet = $oOrdersSemaphore->isLocked($iOId, $_SESSION['user']['id']);
  if ($mRet === FALSE) {
    echo ($oOrdersSemaphore->lockOrder($iOId, $_SESSION['user']['id']) == true ? '1' : '-3');
    die;
  } else {
    echo '-1';// ktoś inny wcześniej zalockował to zamówienie
    die;
  }
}
echo '-2';
die;
?>