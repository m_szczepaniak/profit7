<?php
/**
 * Wybiera opcje dla metody transportu
 */
$aConfig['common']['use_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

$aPayments['ptype_postal_fee'] = "płatność przy odbiorze";
$aPayments['ptype_bank_transfer'] = "przelew - przedpłata na konto";
$aPayments['ptype_bank_14days'] = "przelew 14 dni";
$aPayments['ptype_dotpay'] = "Przelew elektroniczny (dotpay.pl)";
$aPayments['ptype_vat'] = "faktura VAT";
$aPayments['ptype_self_collect'] = "odbiór osobisty";
$aPayments['ptype_card'] = "Karta kredytowa";
$aPayments['ptype_platnoscipl'] = "Przelew elektroniczny (platnosci.pl)";

/**
	* Pobierz opcje metody transportu
	*
	* @global array $aConfig
	* @param type $iTransportMethod
	* @return type 
	*/
function GetTransportOptionsFN($iTransportMethod) {
	global $aConfig;

	$sSql = "SELECT CONCAT(symbol, ' - ', REPLACE(value, '.', ',')) AS label, id AS value 
						FROM ".$aConfig['tabls']['prefix']."orders_transport_options
						WHERE transport_id=".$iTransportMethod;
	return Common::GetAll($sSql);
}// end of GetTransportOptions() method

if(!empty($_GET['transport'])){
	$aRecords = GetTransportOptionsFN(intval($_GET['transport']));
	$items='';
	foreach($aRecords as $iKey=>$aItem){
		$items .= $aItem['label'].'|'.$aItem['value'].";";
	}

	echo $items;
}
?>