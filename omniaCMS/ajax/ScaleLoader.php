<?php

use Scale\ScaleLoader;

error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED);

$aConfig['common']['use_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

$scaleLoader = new ScaleLoader();

$action = $_GET['action'];

$response = null;

switch($action) {
    case 'scaleList':
        $response = $scaleLoader->loadScales();
        break;
    case 'scaleWeight':
        $response = $scaleLoader->scaleWeight($_GET['scaleId']);
        break;
}

echo json_encode($response);
die;