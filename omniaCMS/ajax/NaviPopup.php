<?php
$aConfig['common']['use_session'] = true;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');
include_once('../modules/m_oferta_produktowa/Module_Common.class.php');



function GetNaviTreeElem($sName, $aOptions=array()){
	global $aConfig;
	$sHtml = '';
		if (!empty($aOptions)) {
			foreach ($aOptions as $mKey => $aOAttribs) {
				$sHtml .= '<li><a href="javascript:void(0)" onclick="NaviSetFilterValue('.$aOAttribs['value'].', \''.$aOAttribs['label'].'\')">'.$aOAttribs['label'].'</a>';
				
				if(isset($aOAttribs['items'])){
					$sHtml .= '<ul>';
					$sHtml .= GetNaviTreeElem($sName, $aOAttribs['items']);
					$sHtml .= '</ul>';
				}
				$sHtml .= '</li>';
			}
		}
		return $sHtml;
	}
	
	function GetNaviTree() {
	global $aConfig;
	$oModule = new Module_Common();
		$sHtml='
		<script>
		
			$(document).ready(function(){
				 $("#categories_navi").treeview({
 				  collapsed: true
				});
			});
			function NaviSetFilterValue(iId, sLabel){
				window.opener.$("#f_category").html("<option value=\'"+iId+"\'>"+sLabel+"</option>");
				window.opener.$("#f_category").val(iId);
				self.close();
			}
		</script>';
		$aOptions = $oModule->getProductsCategoriesChTree(Common::getProductsMenu($_SESSION['lang']['id']));
		$sHtml .= '<ul id="categories_navi">';
		$sHtml .= GetNaviTreeElem($sName, $aOptions, $mSelected);

		$sHtml .= '</ul>';
	//	dump($sHtml);
		return $sHtml;
	} // end of GetNaviTree() method
//	dump($_SESSION);
?>
	

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="Content-Language" content="{$sLanguage}">
  <meta name="Robots" content="NOFOLLOW, NOINDEX">
  <meta name="Pragma" content="no-cache">
  <meta name="Cache-Control" content="no-store, no-cache, must-revalidate">
  <title>Wybierz kategorię</title>
  <link href="../css/styles.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" language="JavaScript" src="../js/functions.js"></script>
  <script src="../js/jquery-1.2.6.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="../js/jquery.treeview.min.js"></script>
</head>
<body>
<?php 
if(isset($_SESSION['user'])){
	echo GetNaviTree();
} else {
	echo "<b>Zostałeś wylogowany - aby kontynuować zaloguj się ponownie!</b>";
}
?>

</body>
</html>
