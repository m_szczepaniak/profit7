<?php
$aConfig['common']['use_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

/**
 * 
 * @param int $iOrderId
 * @return int
 */
function getOrderWebsiteId($iOrderId) {
  
  $sSql = 'SELECT website_id FROM orders WHERE id = '.$iOrderId;
  return Common::GetOne($sSql);
}


function getStatementsOrders($sTerm) {
  global $pDB, $aConfig;

        $regExp = $aConfig['regexp_order_number'];
        preg_match("/^\d{12}$/", $sTerm, $matches);

        if (isset($matches[0])) {
            $sAllSQL = "
                O.order_number = ".$pDB->quoteSmart(stripslashes($sTerm))."
            ";

        } else {

            $sAllSQL = "
                O.order_number LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." OR
                (O.to_pay-O.paid_amount) LIKE ".$pDB->quoteSmart('%'.str_replace(',', '.', stripslashes($sTerm)).'%')." OR
                O.email LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." OR
                OUA.company LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." OR
                OUA.name LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." OR
                OUA.surname LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." OR
                O.name LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." OR
                O.surname LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." OR
                O.company LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." OR
                O.check_status_key LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')."
        ";
        }


  		$sSql = "SELECT O.id, O.order_number, O.email, -(O.to_pay-(IF(O.order_status = '5', O.to_pay, O.paid_amount))) AS balance
						FROM orders AS O
						JOIN orders_users_addresses AS OUA
						  ON OUA.order_id = O.id
						WHERE 
							O.correction != '1'
              AND
              (
                ".$sAllSQL."
              )
        GROUP BY O.id
        ORDER BY O.balance ASC 
        
        LIMIT 0,".intval($_GET['limit']); 
      
	return Common::GetAll($sSql);
}

function getDefaultOrders($sTerm) {
  global $pDB;
  
  $iCurrentOrderID = intval($_GET['current_order_id']);
  $iWebsiteId = getOrderWebsiteId($iCurrentOrderID);
  
  $sSql = "SELECT id, order_number, email, -(to_pay-paid_amount) AS balance
					 FROM orders
					 WHERE (
            order_number LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." OR
            balance LIKE ".$pDB->quoteSmart('%'.str_replace(',', '.', stripslashes($sTerm)).'%')." OR
            email LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')."
              ) 
              AND id <> ".$iCurrentOrderID."
              AND website_id = ".$iWebsiteId."
					 ORDER BY balance ASC 
           LIMIT 0,".intval($_GET['limit']); 
	return Common::GetAll($sSql);
}

if(strlen($_GET['term'])>=4){
	if(empty($_GET['limit'])){
		$_GET['limit']=50;
	}
  
  switch ($_GET['mode']) {
    default:
      $aOrders = getDefaultOrders($_GET['term']);
      $aData = array();
      foreach($aOrders as $key => $value){
        $aData[] = $value['order_number'].' '.$value['email'].' '.$value['balance'].' ('.$value['id'].')';
      }
      echo json_encode($aData);
      
    break;
      
    case 'statements':
      $aOrders = getStatementsOrders($_GET['term']);
      foreach($aOrders as $key => $value){
        $aOrders[$key]['label'] = $value['order_number'].' '.$value['email'].' '.$value['balance'].' ('.$value['id'].')';
        $aOrders[$key]['value'] = $value['id'];
      }
      echo json_encode($aOrders);
    break;
  }

	

}