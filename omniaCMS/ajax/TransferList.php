<?php
$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = true;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

global $pDbMgr;

$aValues = array();

if (isset($_POST)&& !empty($_POST) ) {

    if ($_POST['method'] == 'transfer') {
        $lastBook = $_POST['lastBook'];

        $aValues = array(
            'orders_items_lists_id' => intval($_POST['orders_items_lists_id']),
            'orders_items_lists_items_id' => intval($_POST['orders_items_lists_items_id']),
            'stock_location_orders_items_lists_items_id' => (intval($_POST['stock_location_orders_items_lists_items_id']) > 0 ? intval($_POST['stock_location_orders_items_lists_items_id']) : 'NULL'),
            'user_id' => intval($_POST['user_id'])
        );

        echo ($pDbMgr->Insert('profit24', 'orders_items_lists_data', $aValues) !== false ? 'OK' : 'ERROR');
    }
}
exit(0);