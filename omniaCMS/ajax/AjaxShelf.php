<?php
use LIB\Helpers\ArrayHelper;

$aConfig['common']['use_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');

class AjaxShelf
{
    private $pDbMgr;

    public function __construct()
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;
    }

    public function runAction($action)
    {
        if (false === method_exists($this, $action)){
            throw new \Exception("Action does not exists");
        }

        $get = $_GET;

        return $this->$action($get);
    }

    private function getShelfData($get)
    {
        $shelfId = $get['shelf_id'];
        $oilid = $get['oilid'];

        $sql = "SELECT P.name, OILI.products_id, P.isbn_plain, P.isbn_10, P.isbn_13, P.isbn, sum(OILI.quantity) as quantity,
        (SELECT CONCAT(PI.directory, '/', PI.photo) FROM products_images AS PI WHERE PI.product_id = P.id LIMIT 1) as directory
                FROM orders_items_lists_items as OILI
                JOIN products AS P on P.id = OILI.products_id
                WHERE OILI.shelf_number = '%s'
                AND OILI.orders_items_lists_id = %d
                GROUP BY OILI.products_id
                ";

        $shelfProducts = $this->pDbMgr->GetAll('profit24', sprintf($sql, (int)$shelfId, $oilid));

        if(empty($shelfProducts)){
            return json_encode(
                [
                    'status' => 0,
                    'content' => ''
                ]
            );
        }

        return json_encode(
            [
                'status' => 1,
                'content' => $this->getContent($shelfProducts)
            ]
        );
    }

    private function getContent($shelfProducts)
    {
        $partialHtml = '';
        $sum = array_sum(ArrayHelper::arrayColumn($shelfProducts, 'quantity'));

        foreach($shelfProducts as $product){
            $isbns = [
                $product['isbn'],
                $product['isbn_plain'],
                $product['isbn_10'],
                $product['isbn_13']
            ];
            $isbn = implode(', ', $isbns);
            $partialHtml .=
                '<div class="list_scanned_item location-style" style="padding:10px">
                      <img class="sc_image" src="https://www.profit24.pl/images/photos/'.$product['directory'].'"/><br />
                      <span class="sc_shelf">'.$product['quantity'].'</span><br />
                      <span class="sc_name" ><b>'.$product['name'].'</b></span><br />
                      <span class="sc_isbn">'.$isbn.'</span>
                </div>
                <div class="clear"></div>';
        }

        $sumHtml = '<div><br>Ilość produktów na półce: <span class="sc_shelf">'.$sum.'</span><br><br></div>';
        $html ='<div style="position:absolute;width:232px;right:12px;background:#fff;" class="list_scanned_items">'.$sumHtml.$partialHtml.'</div>';

        return $html;
    }
}

$ajaxShelf = new AjaxShelf();
echo $ajaxShelf->runAction($_GET['action']);
exit;

?>