<?php
/**
 * Skrypt Ajax pobiera alerty użytkownika
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-04 
 * @copyrights Marcin Chudy - Profit24.pl
 */
$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');
if (!isset($_SESSION['user']['id'])) {
  echo  "-1";
  return;
}
$sSql = "SELECT count(id) AS c_all, count(user_id) AS c_users FROM orders_to_review WHERE 
  (user_id = ".$_SESSION['user']['id']." OR user_id IS NULL) AND
  group_id IS NULL AND
  disabled = '0' AND
  alert = '1' AND
  alert_datetime <= NOW()
  ";
$aCountsAlerts = Common::GetRow($sSql);

if ($aCountsAlerts['c_all'] > 0) {
  $sAddUser = '';
  if ($aCountsAlerts['c_users'] > 0) {
    $sAddUser = sprintf(', w tym %s bezpośrednio do Ciebie ', $aCountsAlerts['c_users']);
  }
  echo 'Masz '.$aCountsAlerts['c_all'].' zamówień do sprawdzenia'.$sAddUser.' ! <a href="admin.php?'.  http_build_query(array('frame' => 'main', 'module' => 'm_zamowienia', 'module_id' => '38', 'action' => 'to_review')).'" target="content"> Lista zamówień do sprawdzenia &raquo;</a>';
} else {
  echo  "-1";
}