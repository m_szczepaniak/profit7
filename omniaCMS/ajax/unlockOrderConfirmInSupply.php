<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @copyrights Marcin Chudy - Profit24.pl
 */
use magazine\FastTrack\containerFastTrack;

$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = true;
include_once('../config/config.inc.php');
$aConfig['common']['use_session'] = true;
$aConfig['continue_session'] = true;
include_once('../config/ini.inc.php');

global $pDbMgr;

$sCellId = $_GET['cell_id'];
$fastTrackId = intval($_GET['fast_track_id']);
preg_match('/^cell_(\d+)_weight$/', $sCellId, $matches);
if (isset($matches[1]) && intval($matches[1]) > 0 && $fastTrackId > 0) {
    $oshiItemId = intval($matches[1]);
    if ($oshiItemId > 0) {
        try {
            $containerFastTrack = new containerFastTrack($pDbMgr, $_SESSION['user']['id']);
            $ordersItems = $containerFastTrack->unlockOrderToFastTrack($oshiItemId, $fastTrackId);
            header("Content-Type: application/json", true);
            echo json_encode($ordersItems);
            exit(0);
        } catch (Exception $ex) {
            header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);
        }
    }
}
header($_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500);