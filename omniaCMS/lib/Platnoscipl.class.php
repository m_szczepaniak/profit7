<?php 
class Platnoscipl{
	public $pos_id=0;								//numer punktu nadany w platnosci.pl 
	public $pos_auth_key='';						//kod autoryzacyjny punktu nadany w platnosci.pl 
	public $key1='';								//kod autoryzacyjny1 otrzymany od platnosci.pl 
	public $key2='';								//kod autoryzacyjny2 otrzymany od platnosci.pl 
	public $sBaseUrl = 'https://www.platnosci.pl/paygw/';
	public $sEncoding = 'UTF';
	public $error_messages = array(
			"100" => "brak parametru pos id",
			"101" => "brak parametru session id",
			"102" => "brak parametru ts",
			"103" => "brak parametru sig",
			"104" => "brak parametru desc",
			"105" => "brak parametru client ip",
			"106" => "brak parametru first name",
			"107" => "brak parametru last name",
			"108" => "brak parametru street",
			"109" => "brak parametru city",
			"110" => "brak parametru post code",
			"111" => "brak parametru amount",
			"112" => "błędny numer konta bankowego",
			"200" => "inny chwilowy błąd",
			"201" => "inny chwilowy błąd bazy danych",
			"202" => "POS o podanym identyfikatorze jest zablokowany",
			"203" => "niedozwolona wartość pay_type dla danego pos_id",
			"204" => "podana metoda płatnosci (wartość pay_type) jest chwilowo zablokowana dla danego pos_id, np. przerwa konserwacyjna bramki płatniczej",
			"205" => "kwota transakcji mniejsza od wartości minimalnej",
			"206" => "kwota transakcji większa od wartości maksymalnej",
			"207" => "przekroczona wartość wszystkich transakcji dla jednego klienta w ostatnim przedziale czasowym",
			"208" => "POS działa w wariancie ExpressPayment lecz nie nastapiła aktywacja tego wariantu współpracy (czekamy na zgode działu obsługi klienta)",
			"209" => "błedny numer pos_id lub pos_auth_key",
			"500" => "transakcja nie istnieje",
			"501" => "brak autoryzacji dla danej transakcji",
			"502" => "transakcja rozpoczęta wcześniej",
			"503" => "autoryzacja do transakcji była już przeprowadzana",
			"504" => "transakcja anulowana wcześniej",
			"505" => "transakcja przekazana do odbioru wcześniej",
			"506" => "transakcja już odebrana",
			"507" => "błąd podczas zwrotu środków do klienta",
			"599" => "błędny stan transakcji, np. nie można uznać transakcji kilka razy lub inny, prosimy o kontakt",
			"999" => "inny błąd krytyczny - prosimy o kontakt	"
	);
	function errorMsg($id){
		return $this->error_messages[$id];
	}
	
	/**
	 * Pobiera wpisaną w CMS konfiguracje posa płatności
	 * @return unknown_type
	 */
	function getConfig() {
  	global $aConfig;
  	
  	$sSql = "SELECT pdata
  					 FROM ".$aConfig['tabls']['prefix']."orders_payment_types
  					 WHERE ptype = 'platnoscipl'";
  	$aData = unserialize(base64_decode(Common::GetOne($sSql)));
  	$this->pos_id = $aData['posid'];
  	$this->pos_auth_key = $aData['authkey'];
  	$this->key1 = $aData['key1'];
  	$this->key2 = $aData['key2'];
  } // end of getConfig() method
	
	function grosze($kwota) {
		//a tak se napisałem - zamienia zł na grosze, bo to strasznie skomplikowane jakieś ;)
		$kwota = str_replace(',','.',$kwota);
		if((!is_float($kwota) && !is_numeric($kwota)) || $kwota==0) return false;
		$kwota = $kwota * 100;
		return $kwota;
	}
	function zlotowki($kwota) {
		//j.w. tylko w przeciwną stronę
		$kwota = $kwota/100;
		$kwota = round($kwota, 2);
		return $kwota;
	}
		
	/**
	 * Tworzy dla zamówienia nową sesje płatności w bazie
	 * @param $iOrderId
	 * @param $iAmount
	 * @param $sPtype
	 * @return unknown_type
	 */
	function createPaymentSession($iOrderId,$iAmount,$sPtype){
	global $aConfig;
		$aValues = array(
			'order_id' => $iOrderId,
			'amount' => $iAmount,
			'ptype' => $sPtype,
			'status' => '1'
		);
		if(($iSessionId = Common::Insert($aConfig['tabls']['prefix'].'orders_platnoscipl_sessions',$aValues)) === false){
			return false;
		}
		return $iSessionId;
	} // end of createPaymentSession() method
	
	/**
	 * Aktualizuje stan sesji płatnbości w bazie
	 * @param $iSessionId
	 * @param $aData
	 * @return unknown_type
	 */
	function updatePaymentSession($iSessionId, &$aData){
	global $aConfig;
		$aValues = array(
			'status' => $aData['status'],
			'date_created' => $aData['create'],
			'date_init' => $aData['init'],
			'date_send' => $aData['sent'],
			'date_recv' => $aData['recv'],
			'date_cancel' => $aData['cancel'],
			'amount' => $aData['amount'],
			'transaction_id' => $aData['id']
		);
		if(Common::Update($aConfig['tabls']['prefix'].'orders_platnoscipl_sessions',$aValues,'id = '.$iSessionId.' AND order_id = '.$aData['order_id'].' AND `status` <> "99"') == false){
			return false;
		}
		return true;
	} // end of updatePaymentSession() method
	
	/**
	 * Pobiera status sesji płatności z bazy
	 * @param $iSessionId
	 * @return unknown_type
	 */
	function getPaymentSession($iSessionId){
		global $aConfig;
		$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_platnoscipl_sessions
						WHERE id = ".$iSessionId;
		return Common::GetRow($sSql);
	} // end of getPaymentSession() method

	/**
	 * Tworzy zapytanie typu get dla nowej płatności
	 * @param unknown_type $iOrderId
	 * @param unknown_type $sPType
	 * @param unknown_type $fValue
	 * @param unknown_type $sFirstName
	 * @param unknown_type $sLastName
	 * @param unknown_type $sEmail
	 * @param unknown_type $sDescr
	 * @param unknown_type $bJS
	 * @return unknown_type
	 */
	function newOrderLink($iOrderId,$sPType,$fValue,$sFirstName='',$sLastName='',$sEmail='',$sDescr='',$bJS=true){
		$iAmount = $this->grosze($fValue);
		$iSessionId = $this->createPaymentSession($iOrderId, $iAmount, $sPType);
		if($iSessionId === false){
			return false;
		} 
    $sDescr = mb_substr($sDescr, 0, 50, 'UTF-8');
		$aRequest = array(
			'first_name' => $sFirstName,
			'last_name' => $sLastName,
			'pay_type'	=> $sPType,
			'email' => $sEmail,
			'pos_id' => $this->pos_id,
			'pos_auth_key' => $this->pos_auth_key,
			'session_id' => $iSessionId,
			'order_id' => $iOrderId,
			'amount' => $iAmount,
			'desc' => $sDescr,
			'client_ip' => $_SERVER['REMOTE_ADDR'],
			'js' => ($bJS?'1':'0')
		);
		$sUrl .= $this->sBaseUrl.$this->sEncoding.'/NewPayment/?';
		$sUrl .= $this->makeParamsString($aRequest);
		return $sUrl;
	} // end of newOrderLink() metod
	
	function getStatus($id){
			//zwraca status w formie czytelnej ;)
			switch ($id) {
				case 1: return array('code' => $id, 'message' => 'nowa'); break;
				case 2: return array('code' => $id, 'message' => 'anulowana'); break;
				case 3: return array('code' => $id, 'message' => 'odrzucona'); break;
				case 4: return array('code' => $id, 'message' => 'rozpoczeta'); break;
				case 5: return array('code' => $id, 'message' => 'oczekuje na odbior'); break;
				case 6: return array('code' => $id, 'message' => 'autoryzacja odmowna'); break;
				case 7: return array('code' => $id, 'message' => 'platnosc odrzucona'); break;
				case 99: return array('code' => $id, 'message' => 'platnosc odebrana - zakonczona'); break;
				case 888: return array('code' => $id, 'message' => 'bledny status'); break;
				default: return array('code' => false, 'message' => 'brak statusu'); break;
		}
	}
	
	/**
	 * Zamienia tablicę parametrów na ciąg tekstowy
	 * @param $aData
	 * @return unknown_type
	 */
	function makeParamsString(&$aData){
		$pString = '';
		foreach($aData as $key => $val)
		  $pString .= trim($key) . '=' . urlencode(trim($val)) . "&";
		$pString = substr($pString,0,-1);
		return $pString;
	}
	
	function getCurlPost($aData, $sUrl, $bUseSSL = true){
		$ch = curl_init();
		
		$pString = $this->makeParamsString($aData);
		
		curl_setopt($ch, CURLOPT_URL, $sUrl);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_PORT, ($bUseSSL) ? 443 : 80);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $pString);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $bUseSSL);
	
		$aResult = curl_exec($ch);
		curl_close($ch);
	
		return $aResult;
	}
	
	function getCurlGet($sUrl, $bUseSSL = true){
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $sUrl);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_PORT, ($bUseSSL) ? 443 : 80);
		curl_setopt($ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $bUseSSL);
	
		$aResult = curl_exec($ch);
		curl_close($ch);
	
		return $aResult;
	}
	
	function &getPaymentMethods(){
		$sUrl = $this->sBaseUrl.$this->sEncoding.'/xml/'.$this->pos_id.'/'.substr($this->key1,0,2).'/paytype.xml';
		$sXMLData = $this->getCurlGet($sUrl);
		if(!empty($sXMLData)){
			$aData = array();
			$aOptions = array(
				'complexType'       => 'array'
			);
			include('XML/Unserializer.php');
			$pUS = new XML_Unserializer($aOptions);

			$pResult = $pUS->unserialize($sXMLData, false);
			if (!PEAR::IsError($pResult)) {
	      $aData =& $pUS->getUnserializedData();
	    }
			return $aData;
		}
		return false;
	}
	
	function updatePaymentTypes(){
	global $aConfig;
		Common::BeginTransaction();
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."orders_platnosci_payment_types
						WHERE 1=1";
		if(Common::Query($sSql) === false){
			Common::RollbackTransaction();
			return false;
		} 
		$aData = $this->getPaymentMethods();
		if(!empty($aData)){
			foreach($aData['paytype'] as $iKey=>$aPayment){
				if($aPayment['enable'] == 'true'){
					$aValues = array(
						'ptype' => $aPayment['type'],
						'image' => $aPayment['img'],
						'vmin' => $aPayment['min'],
						'vmax' => $aPayment['max'],
						'name' => $aPayment['name']
					);
					if(Common::Insert($aConfig['tabls']['prefix']."orders_platnosci_payment_types",	$aValues) === false) {
						Common::RollbackTransaction();
						return false;
					}
				}
			}
			Common::CommitTransaction();
			return true;
		} else {
			Common::RollbackTransaction();
			return false;
		}
	}
	
	function getPaymentSelection($fVal, $bIncludeCreditCard=true, $bIncludeTest=false, $sValChecked='') {
		global $aConfig;
		$aForm = array();
    if (isset($aConfig['common']['platosci_pl_test_mode']) && $aConfig['common']['platosci_pl_test_mode'] === TRUE) {
      $bIncludeTest = true;
    }
		$sSql = "SELECT ptype, name, image FROM ".$aConfig['tabls']['prefix']."orders_platnosci_payment_types
						WHERE ".$fVal." BETWEEN vmin AND vmax".
						($bIncludeCreditCard ? '' : " AND ptype <> 'c'").
						($bIncludeTest ? '' : " AND ptype <> 't'");	
		$aPayments = Common::GetAll($sSql);
		if(!empty($aPayments)) {
			include_once('Form/Form.class.php');
			foreach($aPayments as $iKey => $aPayment){
				$aForm[$iKey]['input'] = Form::GetRadio('payment_bank',array('id'=>'payment_bank_'.$aPayment['ptype'],'value'=>$aPayment['ptype']), '', ($aPayment['ptype'] === $sValChecked) ? TRUE : FALSE);
				$aForm[$iKey]['label'] = Form::GetLabel('payment_bank_'.$aPayment['ptype'], $aPayment['name'], false);
				$aForm[$iKey]['image'] = $aPayment['image'];
				$aForm[$iKey]['code'] = $aPayment['ptype'];
			}
		}
		return $aForm;
	}
	
	function &getPaymentStatus($iSessionId){
		$sUrl = $this->sBaseUrl.$this->sEncoding.'/Payment/get/xml';
		$ts = time();
		$sig = md5( $this->pos_id . $iSessionId . $ts . $this->key1);
		
		$aParams = array(
			'pos_id' => $this->pos_id,
		  'session_id' => $iSessionId,
		  'ts' => $ts,
		  'sig' => $sig
		);
		
		$sXMLData = $this->getCurlPost($aParams,$sUrl);
		if(!empty($sXMLData)){
			$aData = array();
			$aOptions = array(
				'complexType'       => 'array'
			);
			include('XML/Unserializer.php');
			$pUS = new XML_Unserializer($aOptions);

			$pResult = $pUS->unserialize($sXMLData, false);
			if (!PEAR::IsError($pResult)) {
	      $aData =& $pUS->getUnserializedData();
	    }
			return $aData;
		}
		return false;
	}

	
}

?>
