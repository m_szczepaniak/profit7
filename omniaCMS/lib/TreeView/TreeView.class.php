<?php
/**
 * Klasa View do obslugi widoków z bazy danych w postaci tabelki HTML
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie podstawowej klasy Form 
include_once('Form/Form.class.php');

class TreeView {

  // naglowek widoku
	var $sHeader;
	
	// informacja o braku elementow
	var $sNoItems;
	
	// tablica zawierajaca liste rekordow
  var $aRecords;
  
  // tablica zawierajaca identyfikatory rozwinietych galezi drzewa
  var $aExpanded;
  
  // tablica z ustawieniami kolumn rekordkow
  var $aColSettings;
  
  // tablica z ustawieniami kolumn rekordkow dla menu
  var $aMenuColSettings;
	
	// dodatkowe parametry dla linek stopki
	var $aFooterParams;
	
	// zmienna informujaca czy tablica $aRecords zawiera dane jednego
	// czy wiekszej ilosci menu
	var $bMultiple;
  
  
	/**
   * Konstruktor klsy TreeTreeView
   *
	 * @param	string		$sHeader				- naglowek
	 * @return void
   */
  function TreeView($sHeader='', $sNoItems='') {
  	global $aConfig;
  	
  	$this->sHeader = $sHeader;
  	$this->sNoItems = $sNoItems;
  	$this->aRecords = array();
    $this->aExpanded = array();
    $this->aColSettings = array();
    $this->aMenuColSettings = array();
		$this->aFooterParams = array();
		$this->bMultiple = false;
	} // end of Form() method

	
	/**
	* Metoda dodaje rekordy
	* 
	* @param	array	$aRecords	- rekordy
	* @param	array	$aColSettings	- ustawienia akcji
	* @param	array	$aExpanded	- Id rozwinietych menu
	* @param	bool	$bMultiple	- wiele menu
	* @return	void
	*/
	function AddRecords(&$aRecords, &$aColSettings, &$aExpanded, $bMultiple=false, $aMenuColSettings=array()) {
		if (!is_array($aExpanded)) {
			$aExpanded = array();
		}
		$this->aExpanded = $aExpanded;
		$this->aColSettings = $aColSettings;
		$this->aMenuColSettings = $aMenuColSettings;
		$this->bMultiple = $bMultiple;
		
		if (!$this->bMultiple && !empty($aRecords)) {
			$this->aRecords[0] =& $aRecords; 
		}
		else {
			$this->aRecords =& $aRecords;
		}
	} // end of AddRecords()
	
	
	/**
	* Metoda dodaje strony rekordow
	* 
	* @param	array		$aSubRecords					 - strony
	* @return	void
	*/
	function AddSubRecords(&$aRecords, &$aColSettings) {
		$this->aSubRecords = $aRecords;
		$this->aSubColSettings = $aColSettings;
	} // end of AddSubRecords()
	
	
	/**
	 * Metoda wyswietla widok
	 *
	 * @return	string									- widok
	 */
	function Show() {
		global $aConfig;
		
		if (isset($_GET['file']) && !empty($_GET['file'])) {
			$sModule = $_GET['file'];
		}
		else {
			$sModule = $_GET['module'];
			if (isset($_GET['action'])) {
				$sModule .= '_'.$_GET['action'];
			}
		}
		
		$sHtml = '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		$sHtml .=  '
		<tr>
			<td class="tableMargin">';
		if (!empty($this->sHeader)) {
			$sHtml .= '
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
						<tr>
							<td width="1" height="18" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="18" class="brownBg"><span class="tableHeaderBg"><strong>'.encodeHeaderString($this->sHeader).'</strong></span></td>
							</td>
							<td width="1" height="18" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
						<tr>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
					</table>';
		}
		
		$sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="recordsTable">';
		if (!empty($this->aRecords)) {
			foreach ($this->aRecords as $aRecords) {
				$sHtml .= '<tr class="row">'.
									'<td>'.
									'<table cellspacing="0" cellpadding="0" class="treeViewTable"><tr><td class="icoTop"><img src="gfx/tree/menu_top.gif" width="19" height="6" alt="" border=""></td><td><img src="gfx/pixel.gif" height="6" alt="" border=""></td></tr></table>'.
									$this->getTree($aRecords).'</td>'.
									'</tr>';
			}
		}
		else {
			$sHtml .= '<tr class="row">'.
								'<td class="message">'.$this->sNoItems.'</td>'.
								'</tr>';
		}
		$sHtml .= '</table>';
		$sHtml .= '</td></tr>';
		$sHtml .= '</table>';
		return $sHtml;
	} // end of Show() function
	
	
	/**
	 * Rekurencyjna metoda tworzaca drzewo stron
	 * 
	 * @param	array	$aItems	- strony menu
	 * @param	string	$sDots	- poczatkowe komorki z kropkami lub puste
	 * @return	string	- drzewo menu
	 */
	function getTree(&$aItems, $sDots='') {
		global $aConfig;

		$sTpl = '<table cellspacing="0" cellpadding="0" class="treeViewTable"><tr class="row"%s>%s<td class="ico%s">%s</td><td class="ico%s">%s</td><td class="%s">%s</td><td class="type">%s</td><td class="publish">%s</td><td class="actions">%s</td></tr></table>';
		$sDotsTpl = '<td class="ico%s"><img src="gfx/pixel.gif" width="19" height="20" alt="" border=""></td>';
		$sTree = '';

		foreach ($aItems as $iKey => $aItem) {
			$aItem['dots'] = $sDots;
			if (($iKey + 1) == count($aItems)) {
				// ostatni element w galezi
				$aItem['ico_class'] = 'End';
				$sNewDots = sprintf($sDotsTpl, 'Blank'); 
			}
			else {
				$aItem['ico_class'] = 'Middle';
				$sNewDots = sprintf($sDotsTpl, 'Normal');
			}
			if (isset($aItem['children']) && !empty($aItem['children'])) {
				if (in_array($aItem['id'], $this->aExpanded)) {
					$aItem['ico'] = '<a href="'.phpSelf(array('wrap'=>$aItem['id'])).'"><img src="gfx/tree/minus_middle.gif" width="19" height="20" alt="" border="0"></a>';
					$aItem['ico_img'] = '<img src="gfx/tree/open_dir_'.$aItem['menu'].'.gif" width="19" height="20" alt="" border="">';
					$aItem['ico_img_class'] = 'ExpDir';
				}
				else {
					$aItem['ico'] = '<a href="'.phpSelf(array('expand'=>$aItem['id'])).'"><img src="gfx/tree/plus_middle.gif" width="19" height="20" alt="" border="0"></a>';
					$aItem['ico_img'] = '<img src="gfx/tree/closed_dir_'.$aItem['menu'].'.gif" width="19" height="20" alt="" border="">';
					$aItem['ico_img_class'] = 'Dir';
				}
			}
			else {
				$aItem['ico'] = '<img src="gfx/tree/middle.gif" width="19" height="20" alt="" border="">';
				$aItem['ico_img'] = '<img src="gfx/tree/doc_'.$aItem['menu'].'.gif" width="19" height="20" alt="" border="">';
				$aItem['ico_img_class'] = 'Doc';
			}
			// kolumna z przyciskami akcji
			if ($sDots == '') {
				// menu
				if ((!isset($aItem['show_actions']) || $aItem['show_actions'] == 1) &&
						isset($this->aColSettings['actions']) &&
					  !empty($this->aColSettings['actions'])) {
					
					foreach ($this->aMenuColSettings['actions'] as $sActionDo) {
						$sTitle = '';
						if (isset($this->aMenuColSettings['lang'][$sActionDo])) {
							$sTitle = $this->aMenuColSettings['lang'][$sActionDo]; 
						}
						
						$aGet = array('do'=>$sActionDo);
						if (isset($this->aMenuColSettings['params'][$sActionDo])) {
							// zdefiniowano dodatkowe parametry dla przycisku
							foreach ($this->aMenuColSettings['params'][$sActionDo] as $sParam => $sValue) {
								// zamiana {zmienna} na $aRecord['zmienna'] w $sValue
								$sValue = preg_replace('/^\{(\w+)\}$/e', "\$aItem['\\1']", $sValue);
								if (isset($aGet[$sParam])) {
									unset($aGet[$sParam]);
								}
								if ($sValue != '') {
									$aGet[$sParam] = $sValue;
								}
							}
						}
						if (isset($aItem['disabled']) &&
								is_array($aItem['disabled']) &&
								in_array($sActionDo, $aItem['disabled'])) {
							// ten przycisk jest nieaktywny
							if (isset($this->aMenuColSettings['icons'][$sActionDo])) {
								$sIcon = $this->aMenuColSettings['icons'][$sActionDo]; 
							}
							$aItem['actions'] .= 	'<img src="gfx/icons/'.$sActionDo.'_disabled_ico.gif" alt="'.$sTitle.'" border="0">'.
													'<img src="gfx/pixel.gif" width="5" alt="" border="0">';
						}
						else {
							if ($sActionDo == 'delete') {
								$aItem['actions'] .= '<a href="javascript:void(0);" onclick="if (confirm(\''.$this->aMenuColSettings['lang']['delete_q'].'\')){MenuNavigate(\''.phpSelf($aGet).'\')}; return false;"';
							}
							elseif ($sActionDo == 'preview') {
								$sLink = $aConfig['common']['client_base_url_http'];
								$sLink .= (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'].'/' : '').$aItem['link'].'?'.'preview='.md5($_SERVER['REMOTE_ADDR'].$aConfig['common']['client_base_url_http']);
								$aItem['actions'] .= '<a href="'.$sLink.'" target="_blank"';
							}
							elseif ($sActionDo == 'sort') {
								$aGet['back'] = base64_encode(phpSelf($aGet, array('do', 'mid')));
								$aItem['actions'] .= '<a href="'.phpSelf($aGet).'"';
							}
							else {
								$aItem['actions'] .= '<a href="'.phpSelf($aGet).'"';
							}
							$aItem['actions'] .= 	' title="'.$sTitle.'">'.
													'<img src="gfx/icons/'.$sActionDo.'_ico.gif" alt="'.$sTitle.'" border="0">'.
													'</a>'.
													'<img src="gfx/pixel.gif" width="5" alt="" border="0">';
						}
					}
				}
			}
			else {
				// podstrony			
				if ((!isset($aItem['show_actions']) || $aItem['show_actions'] == 1) &&
						isset($this->aColSettings['actions']) &&
					  !empty($this->aColSettings['actions'])) {
					
					foreach ($this->aColSettings['actions'] as $sActionDo) {
						$sTitle = '';
						// ikonka
						$sIcon = $sActionDo;
						if (isset($this->aColSettings['icons'][$sActionDo])) {
							$sIcon = $this->aColSettings['icons'][$sActionDo]; 
						}
						if (isset($this->aColSettings['lang'][$sActionDo])) {
							$sTitle = $this->aColSettings['lang'][$sActionDo]; 
						}
						
						if (isset($this->aColSettings['links']) &&
								!empty($this->aColSettings['links'])) {
							foreach ($this->aColSettings['links'] as $sAttrName => $sAttrLink) {
								if (!is_array($aItem['disabled']) ||
										!in_array('link_'.$sAttrName, $aItem['disabled'])) {
									// zamiana w linku {zmienna} na $aPage['zmienna']
									$sAttrLink = preg_replace('/\{(\w+)\}/e', "\$aItem['\\1']", $sAttrLink);
									$aItem[$sAttrName] = '<a href="'.$sAttrLink.'">'.$aItem[$sAttrName].'</a>';
								}
							}
						}
											
						$aGet = array('do'=>$sActionDo);
						if (isset($this->aColSettings['params'][$sActionDo])) {
							// zdefiniowano dodatkowe parametry dla przycisku
							foreach ($this->aColSettings['params'][$sActionDo] as $sParam => $sValue) {
								// zamiana {zmienna} na $aRecord['zmienna'] w $sValue
								$sValue = preg_replace('/^\{(\w+)\}$/e', "\$aItem['\\1']", $sValue);
								if (isset($aGet[$sParam])) {
									unset($aGet[$sParam]);
								}
								if ($sValue != '') {
									$aGet[$sParam] = $sValue;
								}
							}
						}
						if (isset($aItem['disabled']) &&
								is_array($aItem['disabled']) &&
								in_array($sActionDo, $aItem['disabled'])) {
							 // ten przycisk jest nieaktywny
							 $aItem['actions'] .= 	'<img src="gfx/icons/'.$sIcon.'_disabled_ico.gif" alt="'.$sTitle.'" border="0">'.
													'<img src="gfx/pixel.gif" width="5" alt="" border="0">';
						}
						else {
							if ($sActionDo == 'delete') {
								$aItem['actions'] .= '<a href="javascript:void(0);" onclick="if (confirm(\''.$this->aColSettings['lang']['delete_q'].'\')){MenuNavigate(\''.phpSelf($aGet).'\')}; return false;"';
							}
							elseif ($sActionDo == 'preview') {
								$sLink = $aConfig['common']['client_base_url_http'];
								$sLink .= (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'].'/' : '').$aItem['link'].'?'.'preview='.md5($_SERVER['REMOTE_ADDR'].$aConfig['common']['client_base_url_http']);
								$aItem['actions'] .= '<a href="'.$sLink.'" target="_blank"';
							}
							elseif ($sActionDo == 'sort') {
								$aGet['back'] = base64_encode(phpSelf($aGet, array('do', 'mid', 'id')));
								$aItem['actions'] .= '<a href="'.phpSelf($aGet).'"';
							}
							else {
								$aItem['actions'] .= '<a href="'.phpSelf($aGet).'"';
							}
							$aItem['actions'] .= 	' title="'.$sTitle.'">'.
													'<img src="gfx/icons/'.$sIcon.'_ico.gif" alt="'.$sTitle.'" border="0">'.
													'</a>'.
													'<img src="gfx/pixel.gif" width="5" alt="" border="0">';
						}
					}
				}
				if (isset($this->aColSettings['links']['name']) &&
						!empty($this->aColSettings['links']['name'])) {
					if (!is_array($aItem['disabled']) ||
							!in_array('link_name', $aItem['disabled'])) {
						// zamiana w linku {zmienna} na $aPage['zmienna']
						$aItem['name'] = '<a href="'.preg_replace('/\{(\w+)\}/e', "\$aItem['\\1']", $this->aColSettings['links']['name']).'">'.$aItem['name'].'</a>';
					}
				}
			}
			
			
			if (!isset($aItem['hover']) || $aItem['hover'] == 1) {
				 $aItem['tr'] = ' onmouseover="this.className=\'rowHov\';" onmouseout="this.className=\'row\';"';
			}
			else {
				$aItem['tr'] = '';
			}
			
			if (!isset($aItem['class'])) {
				 $aItem['class'] = 'item';
			}
			
			$sTree .= sprintf($sTpl,
												$aItem['tr'],
												$sDots,
												$aItem['ico_class'],
												$aItem['ico'],
												$aItem['ico_img_class'],
												$aItem['ico_img'],
												$aItem['class'],
												$aItem['name'],
												$aItem['type'],
												$aItem['published'],
												$aItem['actions']);
			if (isset($aItem['children']) && !empty($aItem['children']) &&
					in_array($aItem['id'], $this->aExpanded)) {
				$sTree .= $this->getTree($aItem['children'], $sDots.$sNewDots);
			}
		}
		return $sTree;
	} // end of getTreeMethod
} // end of class TreeView
?>