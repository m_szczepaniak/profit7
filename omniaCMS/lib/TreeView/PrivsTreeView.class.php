<?php
/**
 * Klasa PrivsTreeView do obslugi widoków z drzewem stron menu sluzaca do nadawania
 * praw do poszczegolnych menu / stron
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie podstawowej klasy Form 
include_once('Form/Form.class.php');

class PrivsTreeView {

  // tablica zawierajaca liste rekordow
  var $aRecords;
  
  // tablica zawierajaca uprawnienia do poszczegolnych menu / stron
  var $aPrivs;
  
  
	/**
   * Konstruktor klsy PrivsTreeTreeView
   *
	 * @param	array	$aModPrivs - dostepne profile uzytkownika dla modulow
	 * @return void
   */
  function PrivsTreeView() {  	
  	$this->aRecords = array();
  	$this->aPrivs = array();
	} // end of PrivsTreeView() method

	
	/**
	* Metoda dodaje rekordy
	* 
	* @param	array	$aRecords	- rekordy
	* @param	array	$aPrivs
	* @return	void
	*/
	function AddRecords(&$aRecords, $aPrivs) {
		$this->aRecords = $aRecords;
		$this->aPrivs = $aPrivs;
	} // end of AddRecords()
	
	
	/**
	 * Metoda wyswietla widok
	 *
	 * @return	string									- widok
	 */
	function Show() {
		global $aConfig;
		
		if (isset($_GET['file']) && !empty($_GET['file'])) {
			$sModule = $_GET['file'];
		}
		else {
			$sModule = $_GET['module'];
			if (isset($_GET['action'])) {
				$sModule .= '_'.$_GET['action'];
			}
		}
		
		if (!empty($this->aRecords)) {
			$i = 1;
			foreach ($this->aRecords as $aRecord) {
				$sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		
				$sHtml .= '<tr>'.
								'<td'.($i == count($this->aRecords) ? ' style="border: none;"' : '').'>'.
								'<table cellspacing="0" cellpadding="0" class="treeViewTable"><tr class="row"><td class="header">'.$aConfig['lang'][$sModule]['item'].'</td><td class="checkbox"><a title="'.$aConfig['lang'][$sModule]['creator'].'"><img src="gfx/icons/add_ico.gif" alt="'.$aConfig['lang'][$sModule]['creator'].'"></a></td><td class="checkbox"><a title="'.$aConfig['lang'][$sModule]['publisher'].'"><img src="gfx/icons/publish_ico.gif" alt="'.$aConfig['lang'][$sModule]['publisher'].'"></a></td><td class="checkbox"><a title="'.$aConfig['lang'][$sModule]['users_only'].'"><img src="gfx/icons/user_ico.gif" alt="'.$aConfig['lang'][$sModule]['users_only'].'"></a></td><td class="checkbox"><a title="'.$aConfig['lang'][$sModule]['recursive'].'"><img src="gfx/icons/recursive_ico.gif" alt="'.$aConfig['lang'][$sModule]['recursive'].'"></a></td></tr></table>'.
								'<table cellspacing="0" cellpadding="0" class="treeViewTable"><tr><td class="icoTop"><img src="gfx/tree/menu_top.gif" width="19" height="6" alt="" border=""></td><td><img src="gfx/pixel.gif" height="6" alt="" border=""></td></tr></table>'.
								$this->getTree($aRecord).'</td>'.
								'</tr>';
				$sHtml .= '</table>';
				$i++;
			}
		}
		return $sHtml;
	} // end of Show() function
	
	
	/**
	 * Rekurencyjna metoda tworzaca drzewo stron
	 * 
	 * @param	array	$aItems	- strony menu
	 * @param	string	$sDots	- poczatkowe komorki z kropkami lub puste
	 * @param	string	$sPrevIds	- sciezka nadrzednych identyfikatorow
	 * @return	string	- drzewo menu
	 */
	function getTree(&$aItems, $sDots='', $sPrevIds='') {
		global $aConfig;

		$sTpl = '<table cellspacing="0" cellpadding="0" class="treeViewTable"><tr class="row" onmouseover="this.className=\'rowHov\';" onmouseout="this.className=\'row\';">%s<td class="ico%s">%s</td><td class="ico%s">%s</td><td class="item">%s</td>%s</tr></table>';
		$sDotsTpl = '<td class="ico%s"><img src="gfx/pixel.gif" width="19" height="20" alt="" border=""></td>';
		$sTree = '';
		
		foreach ($aItems as $iKey => $aItem) {
			$aItem['dots'] = $sDots;
			if (($iKey + 1) == count($aItems)) {
				// ostatni element w galezi
				$aItem['ico_class'] = 'End';
				$sNewDots = sprintf($sDotsTpl, 'Blank'); 
			}
			else {
				$aItem['ico_class'] = 'Middle';
				$sNewDots = sprintf($sDotsTpl, 'Normal');
			}
			if (isset($aItem['children']) && !empty($aItem['children'])) {
				$aItem['ico'] = '<img src="gfx/pixel.gif" width="19" height="20" alt="" border="">';
				$aItem['ico_img'] = '<img src="gfx/tree/open_dir_1.gif" width="19" height="20" alt="" border="">';
				$aItem['ico_img_class'] = 'Dir';
			}
			else {
				$aItem['ico'] = '<img src="gfx/pixel.gif" width="19" height="20" alt="" border="">';
				$aItem['ico_img'] = '<img src="gfx/tree/doc_1.gif" width="19" height="20" alt="" border="">';
				$aItem['ico_img_class'] = 'Doc';
			}
			
			if ($sPrevIds == '') {
				// element jest menu a nie strona
				$aItem['name'] = '<b>'.$aItem['name'].'</b>';
				// utworzenie checkboxow
				// creator checkbox
				$sCheckboxes = '<td class="checkbox">'.Form::GetCheckbox('creator['.$aItem['id'].']', array('onclick' => 'toggleRecursive(\''.$aItem['id'].'\'); toggleModules();'), '', (isset($this->aPrivs['menus'][$aItem['id']]['creator']))).'</td>';
				// publisher checkbox
				$sCheckboxes .= '<td class="checkbox">'.Form::GetCheckbox('publisher['.$aItem['id'].']', array('onclick' => 'toggleRecursive(\''.$aItem['id'].'\');'), '', (isset($this->aPrivs['menus'][$aItem['id']]['publisher']))).'</td>';
				// limited rights checkbox
				$sCheckboxes .= '<td class="checkbox">'.Form::GetCheckbox('users_only['.$aItem['id'].']', array('onclick' => 'toggleRecursive(\''.$aItem['id'].'\');'), '', (isset($this->aPrivs['menus'][$aItem['id']]['users_only']))).'</td>';
				// recursive checkbox
				$sCheckboxes .= '<td class="checkbox">'.Form::GetCheckbox('rec['.$aItem['id'].']', array('onclick' => 'toggleRecursive(\''.$aItem['id'].'\');'), '', (isset($this->aPrivs['menus'][$aItem['id']]['rec']))).'</td>';
			}
			else {
				if (isset($aItem['children']) && !empty($aItem['children'])) {
					// creator checkbox
					$sCheckboxes = '<td class="checkbox">'.Form::GetCheckbox('creator['.$sPrevIds.$aItem['id'].']', array('onclick' => 'toggleRecursive(\''.$sPrevIds.$aItem['id'].'\');'), '', (isset($this->aPrivs['pages'][$aItem['id']]['creator']))).'</td>';
					// publisher checkbox
					$sCheckboxes .= '<td class="checkbox">'.Form::GetCheckbox('publisher['.$sPrevIds.$aItem['id'].']', array('onclick' => 'toggleRecursive(\''.$sPrevIds.$aItem['id'].'\');'), '', (isset($this->aPrivs['pages'][$aItem['id']]['publisher']))).'</td>';
					// limited rights checkbox
					$sCheckboxes .= '<td class="checkbox">'.Form::GetCheckbox('users_only['.$sPrevIds.$aItem['id'].']', array('onclick' => 'toggleRecursive(\''.$sPrevIds.$aItem['id'].'\');'), '', (isset($this->aPrivs['pages'][$aItem['id']]['users_only']))).'</td>';
					// recursive checkbox
					$sCheckboxes .= '<td class="checkbox">'.Form::GetCheckbox('rec['.$sPrevIds.$aItem['id'].']', array('onclick' => 'toggleRecursive(\''.$sPrevIds.$aItem['id'].'\');'), '', (isset($this->aPrivs['pages'][$aItem['id']]['rec']))).'</td>';
				}
				else {
					// creator checkbox
					$sCheckboxes = '<td class="checkbox">'.Form::GetCheckbox('creator['.$sPrevIds.$aItem['id'].']', array(), '', (isset($this->aPrivs['pages'][$aItem['id']]['creator']))).'</td>';
					// publisher checkbox
					$sCheckboxes .= '<td class="checkbox">'.Form::GetCheckbox('publisher['.$sPrevIds.$aItem['id'].']', array(), '', (isset($this->aPrivs['pages'][$aItem['id']]['publisher']))).'</td>';
					// limited rights checkbox
					$sCheckboxes .= '<td class="checkbox">'.Form::GetCheckbox('users_only['.$sPrevIds.$aItem['id'].']', array(), '', (isset($this->aPrivs['pages'][$aItem['id']]['users_only']))).'</td>';
					// recursive checkbox
					$sCheckboxes .= '<td class="checkbox">&nbsp;</td>';
				}
			}
			$sTree .= sprintf($sTpl,
												$sDots,
												$aItem['ico_class'],
												$aItem['ico'],
												$aItem['ico_img_class'],
												$aItem['ico_img'],
												$aItem['name'],
												$sCheckboxes);
			if (isset($aItem['children']) && !empty($aItem['children'])) {
				$sTree .= $this->getTree($aItem['children'], $sDots.$sNewDots, $sPrevIds.$aItem['id'].'_');
			}
		}
		return $sTree;
	} // end of getTreeMethod
} // end of class PrivsTreeView
?>