<?php
/**
* Podstawowa klasa pomocnicza do wysyłania Maili
*
* @author Arkadiusz Golba <golba@omnia.pl>
* @copyright 2012
* @version 1.0
*/


class MailNewsletter {
	
	private $pMime;
	private $aHeaders;
	private $sBody;
	private $sBackend;
	private $mBackendParams;
	
	public function MailNewsletter ($sCRLF="\n") {
		
	
		if (!class_exists('Mail_mime')) {
			require_once('Mail/mime.php');
		}
		
		$this->pMime = new Mail_mime($sCRLF);
	}
	
	/**
	 * Metoda dołącza załączniki do maila
	 *
	 * @global type $aConfig
	 * @param array $aAttachments
	 * @return boolean 
	 */
	public function addAttachments (&$aAttachments) {
		global $aConfig;
		
		if (!empty($aAttachments)) {
			foreach ($aAttachments as &$aAttachment) {
				$mResult = $this->pMime->addAttachment($aAttachment[0],
																				 !empty($aAttachment[1]) ? $aAttachment[1] : 'application/octet-stream',
																				 !empty($aAttachment[2]) ? $aAttachment[2] : '',
																				 !empty($aAttachment[3]) ? $aAttachment[3] : true,
																				 !empty($aAttachment[4]) ? $aAttachment[4] : 'base64');
				
				if (PEAR::IsError($mResult) && $aConfig['common']['status'] == 'development') {
					TriggerError($mResult->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
					return false;
				}
				unset($aAttachment);
			}
		}
		return true;
	} // end of addAttachments() method
	
	
	/**
	 * Metoda dołącza zdjęcia do maila
	 *
	 * @global type $aConfig
	 * @param array $aHTMLImages 
	 * @return boolean 
	 */
	public function addHTMLImage (&$aHTMLImages) {
		global $aConfig;
		
		if (!empty($aHTMLImages)) {
			foreach ($aHTMLImages as &$aImage) {
				$mResult = $this->pMime->addHTMLImage($aImage[0],
														 isset($aImage[1]) ? $aImage[1] : 'application/octet-stream',
														 isset($aImage[2]) ? $aImage[2] : '',
														 isset($aImage[3]) ? $aImage[3] : true);
				
				if (PEAR::IsError($mResult) && $aConfig['common']['status'] == 'development') {
					TriggerError($mResult->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
					return false;
				}
				unset($aImage);
			}
		}
		return true;
	} // end of addHTMLImage() method
	
	
	/**
	 * Metoda ustawia podstawowe dane nagłowka
	 *
	 * @param type $sFrom
	 * @param type $sFromEmail
	 * @param type $sSubject
	 * @param type $sReplyToEmail
	 * @return boolean 
	 */
	public function setHeaderData($sFrom, $sFromEmail, $sSubject, $sReplyToEmail='') {
		
		$aSetHeaders = array();
		if ($sFrom != '') {
			$aSetHeaders['From'] = '"'.$sFrom.'" <'.$sFromEmail.'>';
		}
		else {
			$aSetHeaders['From'] = $sFromEmail;
		}
    
    // XXX  !Uwaga na sztywno ustawione pole w mailach 
    // - nie zawsze konfiguracja serwisu jest wczytana podczas wysyłania maila
		$aSetHeaders['Reply-To'] = $sReplyToEmail != '' ? $sReplyToEmail : 'kontakt@profit24.pl';
    
		$aSetHeaders['Subject'] = $sSubject;
    $aSetHeaders['Precedence'] = 'Bulk';
		return $this->setHeaders($aSetHeaders);
	}// end of setHeaderData() method
	
	
	/**
	 * Metoda ustawia nagłówki wiadomości email
	 *
	 * @global array $aConfig
	 * @param array $aHeaders
	 * @return boolean 
	 */
	private function setHeaders($aHeaders) {
		global $aConfig;
		
		if (isset($aConfig['_settings']['site']['product']['name']) && isset($aConfig['_settings']['site']['product']['version'])) {
			$aHeaders['X-Mailer'] = $aConfig['_settings']['site']['product']['name'].' '.$aConfig['_settings']['site']['product']['version'];
		}
		elseif (isset($aConfig['common']['name']) && isset($aConfig['common']['version'])) {
			$aHeaders['X-Mailer'] = $aConfig['common']['name'].' '.$aConfig['common']['version'];
		}
    $aHeaders['Return-Path'] = preg_replace('/([^<]+)>?$/', '$1', $aHeaders['From']);
		
		$this->aHeaders = $this->pMime->headers($aHeaders);
		return true;
	} // end of setHeaders() method
	
	
	/**
	 * Metoda ustawia treść maila w formie TXT
	 *
	 * @global array $aConfig
	 * @param array $aTXTBody
	 * @return boolean 
	 */
	public function setTXTBody (&$sTXTBody, &$bIsFile='') {
		global $aConfig;
		
		if (!empty($sTXTBody)) {
			$sTXTBody = str_replace("\r", '', $sTXTBody);
			$mResult = $this->pMime->setTXTBody($sTXTBody, $bIsFile === true ? true : false);
			if (PEAR::IsError($mResult) && $aConfig['common']['status'] == 'development') {
      	TriggerError($mResult->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
      	return false;
    	}
		}
		return true;
	}// end of setTXTBody() method
	
	
	/**
	 * Metoda ustawia treść maila w formie HTML
	 *
	 * @global array $aConfig
	 * @param array $aHTMLBody
	 * @return boolean 
	 */
	public function setHTMLBody (&$sHTMLBody, &$bIsFile='') {
		global $aConfig;
		
		if (!empty($sHTMLBody)) {
			$mResult = $this->pMime->setHTMLBody($sHTMLBody, $bIsFile === true ? true : false);
			if (PEAR::IsError($mResult) && $aConfig['common']['status'] == 'development') {
      	TriggerError($mResult->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
      	return false;
    	}
		}
		return true;
	}// end of setHTMLBody() method
	
	
	/**
	 * Metoda ustawia parametry get
	 *
	 * @global type $aConfig
	 * @param type $aGetParams 
	 */
	public function setGetParams ($aGetParams = array()) {
		global $aConfig;
		
		if (empty($aGetParams)) {
			$aGetParams = array('head_charset'=>$aConfig['default']['charset'],
													'text_charset'=>$aConfig['default']['charset'],
													'html_charset'=>$aConfig['default']['charset']);
		}
		$this->sBody = $this->pMime->get($aGetParams);
		if (PEAR::IsError($this->sBody) && $aConfig['common']['status'] == 'development') {
			TriggerError($this->sBody->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			return false;
		}
	}// end of setGetParams() method
	
	
	/**
	 * Metoda ustawia typ systemu maili
	 * 
	 * @param string $sBackend
	 */
	public function setBackend ($sBackend = 'mail') {
		$this->sBackend =& $sBackend;
	}// end of setBackend() method
	
	
	/**
	 * Metoda ustawia parametry z którymi mail zostaje wysyłany
	 * 
	 * @param type $mBackendParams
	 */
	public function setBackendParams ($mBackendParams) {
		$this->mBackendParams =& $mBackendParams;
	}// end of setBackendParams() method
	
	
	/**
	 * Metoda wysyła maila z newsletterem
	 *
	 * @global type $aConfig
	 * @param string $mRecipients
	 * @return boolean 
	 */
	public function sendNewsletterMail ($mRecipients) {
		global $aConfig;
		// sprawdzanie czy wymagane zmienne są zdefiniowane

		if (!class_exists('Mail')) {
			require_once('Mail.php');
		}
		
		if (empty($this->sBackend)) {
			$this->sBackend = 'mail';
		}
		$pMail =& Mail::factory($this->sBackend, $this->mBackendParams);
		$mResult = @$pMail->send($mRecipients, $this->aHeaders, $this->sBody);
		if (PEAR::IsError($mResult) && $aConfig['common']['status'] == 'development') {
    	TriggerError($mResult->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
    	return false;
    }
    return true;
	}// end of sendMailNewsletterMail() method
}// end of Class
?>
