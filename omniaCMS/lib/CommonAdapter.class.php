<?php
/**
 * Klasa adaptera do klasy DatabaseManager z automatycznym 
 * dołączeniem pierwszego parametru, nazwy bazy domyślnie jako profit24,
 * stworzona na potrzeby szybkiej zmiany z Common::Query(args) na CommonAdapter->Query(args);
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-10-18 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class CommonAdapter {
  private $_oDB;
  
  private $_sService;

  function __construct($oDb, $sService = 'profit24') {
    $this->_oDB = $oDb;
    $this->_sService = $sService;
  }
  
  /**
   * Metoda magiczna
   * 
   * @param string $sMethod
   * @param array $aArgs
   * @return mixed
   */
  public function __call($sMethod, $aArgs) {
    
    $aArgs = array_merge(array($this->_sService), $aArgs);
    return call_user_func_array(array($this->_oDB, $sMethod), $aArgs);
  }// end of __call() method
}

?>