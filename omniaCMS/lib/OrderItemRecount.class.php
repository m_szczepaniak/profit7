<?php
/**
 * Klasa przechowuje metody służące do przeliczania cen składowych zamówienia
 * 
 */
class OrderItemRecount {
	public $sTemplatePath = '';
	public $i = 1;
	private $aOrderIds = array();
  private $bToRaport;
	
	function __construct($bToRaport = FALSE) {
    $this->bToRaport = $bToRaport;
		$this->i = 1;
	}
	
	/**
	 * Metoda pobiera szczegółowe ceny zamówienia (potrzebne do generowania pdf i raportów)
	 *
	 * @param type $iId
	 * @param type $bSecondInvoice 
	 */
	function getOrderPricesDetail ($iId, $bSecondInvoice, $sPaymentType, $bOrderByName = false) { 
		
		// pobranie szczegółów zamówienia
		$aOrder = $this->getOrderData($iId);
		$aOrderIds[]=$iId;
		// określenie czy dostępna jest druga faktura
		$bEnabledSecondInvoice = ($aOrder['second_invoice'] == '1');
		// pobranie elementów zamówienia
		$aOrderItems = $this->GetOrderItemData($iId, $bEnabledSecondInvoice, $bSecondInvoice, $sPaymentType, $bOrderByName);
		// przelicz element zamówienia
		$aOrder = $this->recountItemOrder($aOrder, $aOrderItems);
		// dodanie metody transportu jako element zamówienia
		$aOrder = $this->addDeliverOrder($aOrder, $aOrderItems, $bSecondInvoice);
		
		$aResults['order'] = $aOrder;
		$aResults['order_items'] = $aOrderItems;
		return $aResults;
	} // end of getOrderPricesDetail() method
	
	
	/**
	 * Metoda pobiera dane o zamówieniu
	 *
	 * @global type $aConfig
	 * @return type 
	 */
	function getOrderData($iId) {
		global $aConfig;
		
		$sSql = "SELECT A.*, 
										DATE_FORMAT(order_date, '".$aConfig['common']['sql_date_format']."') as order_date,
										DATE_FORMAT(invoice_date, '".$aConfig['common']['sql_date_format']."') as invoice_date, 
										DATE_FORMAT(status_1_update, '".$aConfig['common']['sql_date_hour_format']."') as status_1_update, 
										DATE_FORMAT(status_2_update, '".$aConfig['common']['sql_date_hour_format']."') as status_2_update, 
										DATE_FORMAT(status_3_update, '".$aConfig['common']['sql_date_hour_format']."') as status_3_update,
		 								(A.transport_cost + A.value_brutto) as total_value_brutto, 
										B.personal_reception
						 FROM ".$aConfig['tabls']['prefix']."orders A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						 ON B.id=A.transport_id
						 WHERE A.id = ".$iId;
		return Common::GetRow($sSql);
	}// end of getOrderItem() method
	
	
	/**
	 * Metoda pobiera składowe zamowienia - książki, załączniki (za wyjątkiem pakietów)
	 *
	 * @global type $aConfig
	 * @param type $iId
	 * @param type $bEnabledSecondInvoice
	 * @param type $bSecondInvoice
	 * @return type 
	 */
	function getOrderItemData($iId, $bEnabledSecondInvoice, $bSecondInvoice, $sPaymentType='', $bOrderByName = false) {
		global $aConfig;
		
		$sPaymentSql = '';
		if ($sPaymentType != '') {
			if ($sPaymentType == 'platnoscipl') {
				$sPaymentSql = " AND IF(A.payment_type<>'', 
															 (A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay'), 
															 (O.payment_type = 'platnoscipl' OR O.payment_type = 'card' OR O.payment_type = 'dotpay')) ";
			}
			else {
				$sPaymentSql = " AND IF(A.payment_type<>'', (A.payment_type = '".$sPaymentType."'), (O.payment_type = '".$sPaymentType."')) ";
			}
		}
		
		$bAttachments = true;
		if ($bOrderByName == true) {
			// jesli sortowanie z uwzględnieniem załączników po nazwie w pierwszym przejściu nie 
			$bAttachments = false;
		}
		
		
		$sSql = "SELECT A.*, B.quantity AS parent_quantity, A.second_payment
						 FROM ".$aConfig['tabls']['prefix']."orders_items A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."orders_items B
							ON B.id=A.parent_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."orders O
							ON O.id=A.order_id
						 WHERE A.order_id = ".$iId."
						 AND A.packet='0'
						 AND A.deleted = '0'".
						 ($bAttachments === false ? " AND A.item_type <> 'A' " : '').
						($bEnabledSecondInvoice?($bSecondInvoice?" AND A.second_invoice='1'":" AND A.second_invoice='0'"):'').
						$sPaymentSql.
						" ORDER BY name ASC ";
						//" ORDER BY IFNULL( A.parent_id, A.id ) , A.id"; // sortowanie pod id czyli w kolejności złożonych zamówień z uwzględnieniem załączników
		$aOrderItems = Common::GetAll($sSql);
		
		// dodanie załączników zamówienia jeśli włączone sortowanie po nazwie z uwzględnieniem załączników
		if ( $bOrderByName === true ) {
			$aIOIds = array();
			$aNProducts = array();
			$aProductsTMP = array();
			foreach ($aOrderItems as $aPItem) {
				 $aIOIds[] = $aPItem['id'];

				 $sSql = "SELECT A.*, B.quantity AS parent_quantity, A.second_payment
								 FROM ".$aConfig['tabls']['prefix']."orders_items A
								 LEFT JOIN ".$aConfig['tabls']['prefix']."orders_items B
									ON B.id=A.parent_id
								 WHERE A.deleted ='0'
											AND A.packet = '0'
											AND A.item_type = 'A'
											AND A.parent_id = ".$aPItem['id']." 
								ORDER BY id";
					$aProductsTMP =& Common::GetAll($sSql);

					// dołączenie ojca
					$aNProducts[] = $aPItem;
					foreach ($aProductsTMP as $aPTMP) {
						// nazwy analogicznie jak były
						// $aPTMP['name'] = $aPTMP['name'].'-'.$aPItem['name'];
						$aNProducts[] = $aPTMP;
					}
			}
			// nadpisanie zamowień
			$aOrderItems = $aNProducts;
		}
		// koniec dodanie załączników zamówienia
		return $aOrderItems;
	}// end of getOrderItemData() method
	
	
	/**
	 * Metoda przelicza wartości vat dla wszystkich elementów zamówienia, 
	 *  oblicza vat całkowity oraz wartość brutto i netto całkowitą zamówienia
	 *
	 * @param type ref $aOrder
	 * @param type $aOrderItems
	 * @return type 
	 */
	function recountItemOrder(&$aOrder, &$aOrderItems) {
		
		$aOrder['value_netto'] = 0;
		$aOrder['value_brutto'] = 0;
		
		$aOrder['vat'][0] = array();
		$aOrder['vat'][7] = array();
		$aOrder['vat'][22] = array();
		foreach ($aOrderItems as $iKey => $aItem){
			if($aItem['promo_price_brutto'] == 0){
					$aItem['promo_price_brutto'] = $aItem['price_brutto'];
			}

			$aOrderItems[$iKey]['lp'] = ($this->i++).'.';
			if($aItem['vat']==0 || $aItem['vat']==5 )
				$aOrderItems[$iKey]['isbn'].='<br />58.11.1';
			elseif($aItem['vat']==7 || $aItem['vat']==8 )
				$aOrderItems[$iKey]['isbn'].='<br />58.14.1';
			
			$aOrderItems[$iKey]['jm'] = 'egz.';
			$aOrderItems[$iKey]['vat_currency'] = $aItem['total_vat_currency'];
			
			if($aItem['item_type'] == 'P'){
				$aItem['quantity'] = $aItem['parent_quantity'];
			}
			
			$aOrder['vat'][$aItem['vat']]['quantity'] += $aItem['quantity'];
			// produkt zwykly
			$aItem['vat_currency'] =  $aItem['total_vat_currency']; // Common::formatPrice2($aItem['value_brutto'] - $aItem['value_netto']);
			// dodawanie produktu do sumy vat dla wartości odpowiedniej
//			$aOrder['vat'][$aItem['vat']]['vat_currency'] += Common::formatPrice2($aItem['vat_currency']);
//			$aOrder['vat'][$aItem['vat']]['value_netto'] += Common::formatPrice2($aItem['value_netto']);
			$aOrder['vat'][$aItem['vat']]['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
			// vat calkowity
//			$aOrder['order_vat'] += Common::formatPrice2($aItem['vat_currency']);
			// suma netto zamowienia
//			$aOrder['value_netto'] += Common::formatPrice2($aItem['value_netto']);
			$aOrder['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
			
			$aOrder['vat'][$aItem['vat']]['quantity'] += $aItem['quantity'];
			$aOrder['quantity'] += $aItem['quantity'];
		}
    
    $aOrder['value_netto'] = 0;
    // po stawkach Vat
    foreach ($aOrder['vat'] as $iVat => $aOrderItem) {
      if ($aOrderItem['value_brutto'] > 0.00) {
        $aOrder['vat'][$iVat]['value_netto'] = Common::formatPrice2($aOrderItem['value_brutto'] / (1 + $iVat/100));
        $aOrder['value_netto'] += $aOrder['vat'][$iVat]['value_netto'];
        
        $aOrder['vat'][$iVat]['vat_currency'] = Common::formatPrice2($aOrderItem['value_brutto'] - $aOrder['vat'][$iVat]['value_netto']);
        $aOrder['order_vat'] += $aOrder['vat'][$iVat]['vat_currency'];
      }
    }
		
		return $aOrder;
	} // end of recountItemOrder() method
	
  
  /**
   * Metoda sprawdza, czy w zamówieniu znajduje sie produkt na stawce 23%
   * 
   * @ticket #43 Zmiana w zamówieniach, kosztów transportu z 23% na 5% VAT
   * @param type $aOrderItems
   * @return boolean
   */
  public function getTransportVat($iPaymentId, $aOrderItems) {
    foreach ($aOrderItems as $iItemId => $aItem) {
      if (!isset($aItem['deleted']) || $aItem['deleted'] == '0') {
        if ($aItem['vat'] == 23)  {
          return 23;
        }
        if ($aItem['packet'] == '1' ) {
          // pakiet, sprawdźmy czy w tym pakiecie znajduje się składowa na 23% VATU
          if ($this->_checkPacketItems23Vat($iItemId) === TRUE) {
            // jest jakiś na stawce 23%
            return 23;
          }
        }
      }
    }
    return $this->_getTransportVat($iPaymentId);
  }// end of checkOrderItem23Vat() method
  
  
  /**
   * Metoda sprawdza czy w podanym pakiecie dowolna składowa posiada produkt na stawce 23% vatu
   * 
   * @param int $iId
   * @return int
   */
  private function _checkPacketItems23Vat($iId) {
    
    $sSql = "SELECT packet_id
             FROM products_packets_items 
             WHERE packet_id = ".$iId.' AND vat = 23 ';
    return Common::GetOne($sSql) > 0;
  }// end of _checkPacketItems23Vat() method
  
  
  /**
   * Metoda pobiera stawkę Vat dla metody płatności
   * 
   * @param int $iPaymentId
   * @return array
   */
  private function _getTransportVat($iPaymentId) {
    
    return Common::GetOne('SELECT vat FROM orders_payment_types WHERE id='.$iPaymentId);
  }// end of _getTransportVat() method
  
	
	/**
	 * Dodawanie metody transportu do zamówienia
	 *
	 * @param array $aOrder - Zamówienie
	 */
	function addDeliverOrder(&$aOrder, &$aOrderItems, $bSecondInvoice) {
		
		// sprawdź czy w elementach występują elementy z 1 metodą płatności
		$bFirstMethod = false;
		foreach ($aOrderItems as $aOItem) {
			if ($aOItem['second_payment'] == '0') $bFirstMethod = true;
		}
    
    if ($this->bToRaport === TRUE) {
      $iTransportVat = $aOrder['transport_vat'];
    } else {
      $iTransportVat = $this->getTransportVat($aOrder['payment_id'], $aOrderItems);
    }
		
		if($aOrder['personal_reception'] == '0' && !$bSecondInvoice && $bFirstMethod == true){
			$aItem = array();
			$aItem['name'] = 'Koszt transportu';
			$aItem['discount'] = 0;
			$aItem['quantity'] = 1;
			$aItem['price_brutto'] = $aOrder['transport_cost'];
			$aItem['promo_price_brutto'] = $aItem['price_brutto'];
			$aItem['value_brutto'] = $aItem['price_brutto'];
			$aItem['vat'] = $iTransportVat;
			$aItem['value_netto'] =  Common::formatPrice2($aItem['price_brutto'] / (1 + $aItem['vat']/100));
			$aItem['promo_price_netto'] = $aItem['value_netto'];
			$aItem['vat_currency'] =  Common::formatPrice2($aItem['value_brutto'] - $aItem['value_netto']);
			// dodawanie transportu do sumy vat dla wartości odpowiedniej
			$aOrder['vat'][$aItem['vat']]['vat_currency'] += Common::formatPrice2($aItem['vat_currency']);
			$aOrder['vat'][$aItem['vat']]['value_netto'] += Common::formatPrice2($aItem['value_netto']);
			$aOrder['vat'][$aItem['vat']]['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
			// vat calkowity
			$aOrder['order_vat'] += Common::formatPrice2($aItem['vat_currency']);
			// dodanie kosztu transportu do calk wartosci zamowienia
			// suma netto zamowienia
			$aOrder['value_netto'] += Common::formatPrice2($aItem['value_netto']);
			$aOrder['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
			$aItem['lp'] = ($this->i++).'.';
			
			$aOrder['vat'][$aItem['vat']]['quantity'] += $aItem['quantity'];
			$aOrder['quantity'] += $aItem['quantity'];
			$aOrderItems[] = $aItem;
		}
		
		$aOrder['value_netto'] = Common::formatPrice($aOrder['value_netto']);
		$aOrder['value_brutto'] = Common::formatPrice($aOrder['value_brutto']);
		$aOrder['order_vat'] = Common::formatPrice($aOrder['order_vat']);
		$aOrder['total_value_brutto'] = Common::formatPrice($aOrder['total_value_brutto']);
		foreach($aOrder['vat'] as $iVat=>$aVat){
			if (isset($aOrder['vat'][$iVat]['vat_currency']))
				$aOrder['vat'][$iVat]['vat_currency'] = Common::formatPrice($aOrder['vat'][$iVat]['vat_currency']);
			if (isset($aOrder['vat'][$iVat]['value_netto']))
				$aOrder['vat'][$iVat]['value_netto'] = Common::formatPrice($aOrder['vat'][$iVat]['value_netto']);
			if (isset($aOrder['vat'][$iVat]['value_brutto']))
				$aOrder['vat'][$iVat]['value_brutto'] = Common::formatPrice($aOrder['vat'][$iVat]['value_brutto']);
		}
		foreach($aOrder['vat'] as $iVat=>$aVat){
			if (empty($aVat)) {
				unset($aOrder['vat'][$iVat]);
			}
		}

		return $aOrder;
	}// end of addDeliverOrder() method
	
	
	/**
	 * Metoda przelicza ceny książki oraz załącznika książki w przypadku 
	 *
	 * @param type $aAttachments
	 * @param type $fBundlePriceBrutto
	 * @param type $iDiscount 
	 */
	function getAttProductOrderPrices ($aAttachments, $fBundlePriceBrutto, $fBundlePromoPriceBrutto, $iProductVatt, $iDiscount) {
		
		if (empty($aAttachments) || !is_array($aAttachments) || $fBundlePriceBrutto <= 0.00) {
			return false;
		}
		
		$aPrices = array();
		/*
		$fAttNetto = 0.00;
		$fAttBrutto = 0.00;
		foreach ($aAttachments as $aAtt) {
			$fAttNetto += $aAtt['price_netto'];
			$fAttBrutto += $aAtt['price_brutto'];
		}
		 */
		/*** ZAŁĄCZNIKKI ***/
		// przeliczanie ceny promo netto i promo brutto załączników
		//$fPriceNettoSum = 0.00;
		$fPromoPriceBruttoSum = 0.00;
		$fPromoPriceNettoSum = 0.00;
		$fAttNettoSum = 0.00;
		$fAttBruttoSum = 0.00;
		foreach ($aAttachments as $iKey => $aAtt) {
			$fAttNettoSum += $aAtt['price_netto'];
			$fAttBruttoSum += $aAtt['price_brutto'];

			$aPrices['attachments'][$iKey]['price_brutto'] = $aAtt['price_brutto'];
			$aPrices['attachments'][$iKey]['price_netto'] = $aAtt['price_netto'];
			//$fPriceNettoSum += $aPrices['attachments'][$iKey]['price_netto']; // zmiena potrzebna do obliczenia netto bundle
			
			// obliczenie cen promocyjnych załącznika
			$aPrices['attachments'][$iKey]['promo_price_brutto'] = $aPrices['attachments'][$iKey]['price_brutto'] - Common::formatPrice2($aPrices['attachments'][$iKey]['price_brutto'] * ($iDiscount / 100));
			$aPrices['attachments'][$iKey]['promo_price_netto'] = Common::formatPrice2($aPrices['attachments'][$iKey]['promo_price_brutto'] / (1 + $aAtt['vat']/100));
			$fPromoPriceNettoSum += $aPrices['attachments'][$iKey]['promo_price_netto'];
			$fPromoPriceBruttoSum += $aPrices['attachments'][$iKey]['promo_price_brutto']; 
			
			$aPrices['attachments'][$iKey]['vat'] = $aAtt['vat'];
		}
		
		
		/*** SAMA KSIĄŻKA ***/
		// przeliczanie ceny netto,brutto, promo netto i promo brutto samej książki
		$aPrices['book']['price_brutto'] = ($fBundlePriceBrutto - $fAttBruttoSum);
		$aPrices['book']['price_netto'] = Common::formatPrice2($aPrices['book']['price_brutto'] / (1 + $iProductVatt/100));
		$aPrices['book']['promo_price_brutto'] = ($fBundlePromoPriceBrutto - $fPromoPriceBruttoSum); //$aPrices['book']['price_brutto'] - Common::formatPrice2($aPrices['book']['price_brutto'] * ($iDiscount / 100));
		$aPrices['book']['promo_price_netto'] = Common::formatPrice2($aPrices['book']['promo_price_brutto'] / (1 + $iProductVatt/100));
		
		$aPrices['book']['vat'] = $iProductVatt;
		
		

		/** BUNDLE **/
		// przeliczanie ceny netto bundle
		$aPrices['bundle']['price_brutto'] = $fBundlePriceBrutto;
		$aPrices['bundle']['price_netto'] = ($fAttNettoSum+$aPrices['book']['price_netto']);
		
		// przeliczanie ceny promo netto i promo brutto bundla
		$aPrices['bundle']['promo_price_brutto'] = $fBundlePromoPriceBrutto; //($fPromoPriceBruttoSum + $aPrices['book']['promo_price_brutto']); // $fBundlePriceBrutto - Common::formatPrice2($fBundlePriceBrutto * ($iDiscount / 100));
		$aPrices['bundle']['promo_price_netto'] = ($fPromoPriceNettoSum + $aPrices['book']['promo_price_netto']);
		
		/*
		dump(func_get_args());
		dump($aPrices);
		*/
		return $aPrices;
	}// end of getAttProductOrderPrices() method
	
	
	/**
	 * Metoda dokonuje przeliczeń danych załącznika
	 *
	 * @param type $aParentData
	 * @param type $aPAttVal 
	 */
	function getAttrValues($aPAttVal, $iParentId, $iOId) {
		

		$aValuesAttPacket = array();
		// wyliczenie cen dla nowego załącznika w orders_items
		// ceny po promocji
		$aValuesAttPacket['promo_price_brutto'] =  Common::formatPrice2($aPAttVal['price_brutto'] - $aPAttVal['price_brutto']*($aPAttVal['product_discount']/100));
		$aValuesAttPacket['promo_price_netto'] =  Common::formatPrice2(($aValuesAttPacket['promo_price_brutto'] / (1 + $aPAttVal['vat']/100)));
		// wartości zamówienia - cena przemnożona przez ilość
		$aValuesAttPacket['value_brutto'] =  Common::formatPrice2($aPAttVal['product_quantity'] * $aValuesAttPacket['promo_price_brutto']);
		$aValuesAttPacket['value_netto'] =  Common::formatPrice2($aPAttVal['product_quantity'] * ($aValuesAttPacket['promo_price_brutto'] / (1 + $aPAttVal['vat']/100)));

		// vat_currency nie ma uwzględniać ilości
		$aValuesAttPacket['vat_currency'] = Common::formatPrice2($aValuesAttPacket['promo_price_brutto'] - $aValuesAttPacket['promo_price_netto']); 
		// total_vat_currency uwzględnia ilość
		$aValuesAttPacket['total_vat_currency'] = Common::formatPrice2($aValuesAttPacket['value_brutto'] - $aValuesAttPacket['value_netto']); 
		$aValuesAttPacket['vat'] = $aPAttVal['vat'];

		// rabat, ilość
		$aValuesAttPacket['discount'] = $aPAttVal['product_discount'];
		// discount_currency Zdeprecjonowana Nie uwzglednia ilosci zamowionych pozycji
		$aValuesAttPacket['discount_currency'] = Common::formatPrice2($aPAttVal['price_brutto'] * (Common::formatPrice2($aPAttVal['product_discount']) / 100));
		// total_discount_currency uwzglednia ilosć zamowionych pozycji
		$aValuesAttPacket['total_discount_currency'] = Common::formatPrice2($aValuesAttPacket['discount_currency'] * $aPAttVal['product_quantity']);
		$aValuesAttPacket['quantity'] = $aPAttVal['product_quantity'];

		// cena netto i brutto pozostaje taka sama
		$aValuesAttPacket['price_brutto'] = Common::formatPrice2($aPAttVal['price_brutto']);
		$aValuesAttPacket['price_netto'] = Common::formatPrice2(($aPAttVal['price_brutto'] / (1 + $aPAttVal['vat']/100)));
		// inne
		$aValuesAttPacket['order_id'] = $iOId;
		$aValuesAttPacket['product_id'] = 'NULL';
		$aValuesAttPacket['attachment_id'] = $aPAttVal['id'];
		$aValuesAttPacket['name'] = $aPAttVal['name'];
		$aValuesAttPacket['item_type'] = 'A';
		$aValuesAttPacket['parent_id'] = $iParentId;
		$aValuesAttPacket['shipment_time'] = $aPAttVal['product_shipment_time'];
		$aValuesAttPacket['weight'] = '0.00';
		$aValuesAttPacket['bundle_price_netto'] = 'NULL';
		$aValuesAttPacket['bundle_price_brutto'] = 'NULL';
		$aValuesAttPacket['bundle_value_netto'] = 'NULL';
		$aValuesAttPacket['bundle_value_brutto'] = 'NULL';
		
		return $aValuesAttPacket;
	}// end of getAttrValues() method
	
	
	/**
	 * Metoda przelicza ceny elementu w przypadku kiedy element posiada załącznik
	 *
	 * @param type $aParentData - Tablica danych Rodzica załącznika
	 * @param type $aAData - Tablica danych załącznika
	 */
	function getParentAttData($aParentData,  $aAttachmentsPacket, $fBundlePromoPriceBrutto) {
		
		$fBundlePriceBrutto = $aParentData['price_brutto'];
		$iProductVatt = $aParentData['vat'];
		$iDiscount = $aParentData['discount'];
		
		//$aAData
		$aPrices = $this->getAttProductOrderPrices ($aAttachmentsPacket, $fBundlePriceBrutto, $fBundlePromoPriceBrutto, $iProductVatt, $iDiscount);
		
		$aValues = array(
			'attachments' => '1',
		);
		// do bundle lecą ceny w sumie pakietu
		$aValues['bundle_price_netto'] = $aPrices['bundle']['price_netto'];//$aParentData['price_netto'];
		$aValues['bundle_price_brutto'] = $aPrices['bundle']['price_brutto'];//$aParentData['price_brutto'];
		$aValues['bundle_value_netto'] = Common::formatPrice2($aPrices['bundle']['promo_price_netto'] * $aParentData['quantity']);
		$aValues['bundle_value_brutto'] = Common::formatPrice2($aPrices['bundle']['promo_price_brutto'] * $aParentData['quantity']);

		$aAData['promo_price_brutto'] =  Common::formatPrice2($aAData['price_brutto'] - $aAData['price_brutto']*($aParentData['discount']/100));
		$aValues['promo_price_brutto'] = $aPrices['book']['promo_price_brutto']; //Common::formatPrice2($aParentData['promo_price_brutto'] - $aAData['promo_price_brutto']);
		$aValues['promo_price_netto'] = $aPrices['book']['promo_price_netto'];// Common::formatPrice2(($aValues['promo_price_brutto'] / (1 + $aParentData['vat']/100)));
		$aValues['value_netto'] = Common::formatPrice2($aParentData['quantity'] * $aPrices['book']['promo_price_netto']);// = Common::formatPrice2($aParentData['quantity'] * ($aParentData['promo_price_brutto'] / (1 + $aParentData['vat']/100)));
		$aValues['value_brutto'] =  Common::formatPrice2($aParentData['quantity'] * $aValues['promo_price_brutto']);

		$aValues['price_netto'] = $aPrices['book']['price_netto']; //Common::formatPrice2($aValues['bundle_price_netto'] - $aAData['price_netto']);
		$aValues['price_brutto'] = $aPrices['book']['price_brutto']; //Common::formatPrice2($aValues['bundle_price_brutto'] - $aAData['price_brutto']);

		// nie uwzględnia ilości
		$aValues['discount_currency'] = Common::formatPrice2($aValues['price_brutto'] * (Common::formatPrice2($aParentData['discount']) / 100));

		// nie uwzględnia ilości
		$aValues['vat_currency'] = Common::formatPrice2($aValues['price_brutto'] - $aValues['price_netto']);
		// uwzględnia ilość
		$aValues['total_vat_currency'] = Common::formatPrice2($aValues['value_brutto'] - $aValues['value_netto']);

		// uwzględnia ilość
		$aValues['total_discount_currency'] = Common::formatPrice2($aValues['discount_currency'] * $aParentData['quantity']);		

		return $aValues;
	}// end of getParentAttData() method
	
	
	
}
