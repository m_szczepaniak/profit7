<?php
/**
* Podstawowa klasa biblioteki
*
* @author Marcin Korecki <korecki@pnet.pl>
* @copyright 2005 - 2006
* @version 1.0
*/

class Common {

  private static $_pDBMain;
  
	/**
	* Konstruktor klasy Common
	*
	*/
	function Common() {
  } // end Common()
  
  public function SetDatabaseObj($oDbObj) {
    self::$_pDBMain = $oDbObj;
  }
  
  
  public static function __callStatic($name, $arguments)
  {
      include_once('CommonAdapter.class.php');
      $oCommonAdapter = new CommonAdapter(self::$_pDBMain, 'profit24');
      return call_user_func_array(array($oCommonAdapter, $name), $arguments);
  }
  
  
  public function __call($name, $arguments)
  {
      include_once('CommonAdapter.class.php');
      $oCommonAdapter = new CommonAdapter(self::$_pDBMain, 'profit24');
      return call_user_func_array(array($oCommonAdapter, $name), $arguments);
  }

/**
	 * Metoda wysyla email do podanego uzytkownika
	 *
	 * @param		string	$sTO				- adresat maila: Imie Nazwisko <email>
	 * @param		string	$sFROM			- nadawca maila
	 * @param		string	$sSubject		- temat maila
	 * @param		string	$sContent		- tresc maila
	 * @return	bool		true: udalo się, false: nie udalo sie
	 */
	function SendMimeEmail($mRecipients, $aHeaders, $aTXTBody, $aHTMLBody=array(), $aGetParams=array(), $aAttachments=array(), $aHTMLImages=array(), $sBackend='mail', $mBackendParams=array(), $sCRLF="\n") {
    global $aConfig;
//    return true;// XXX ODBLOKOWANE
    
		$pMime		= null;
		$pMail		= null;
		$mResult	= false;
		$sBody		= '';
		$aHdrs		= array();

		if (!class_exists('Mail')) {
			require_once('Mail.php');
		}
		if (!class_exists('Mail_mime')) {
			require_once('Mail/mime.php');
		}

		$pMime = new Mail_mime($sCRLF);

		if (!empty($aTXTBody['body'])) {
			$aTXTBody['body'] = str_replace("\r", '', $aTXTBody['body']);
			$mResult = $pMime->setTXTBody($aTXTBody['body'], $aTXTBody['is_file'] === true ? true : false);
			if (PEAR::IsError($mResult) && $aConfig['common']['status'] == 'development') {
      	TriggerError($mResult->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
      	return false;
    	}
		}
		if (!empty($aHTMLBody['body'])) {
			$mResult = $pMime->setHTMLBody($aHTMLBody['body'], $aHTMLBody['is_file'] === true ? true : false);
			if (PEAR::IsError($mResult) && $aConfig['common']['status'] == 'development') {
      	TriggerError($mResult->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
      	return false;
    	}
      // tutaj sprobujemy sobie zmienić na textplain ;)
      if (empty($aTXTBody['body'])) {
        // ok zróbmy ten HTML
        $aTXTBody['body'] = trim(strip_tags(br2nl(preg_replace('/<style.*<\/style>/ims', '', $aHTMLBody['body']))));
        $aTXTBody['body'] = str_replace("\r", '', $aTXTBody['body']);
        $mResult = $pMime->setTXTBody($aTXTBody['body'], $aTXTBody['is_file'] === true ? true : false);
        if (PEAR::IsError($mResult) && $aConfig['common']['status'] == 'development') {
          TriggerError($mResult->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
          return false;
        }
      }
		}
		if (!empty($aAttachments)) {
			foreach ($aAttachments as $aAttachment) {
				$mResult = $pMime->addAttachment($aAttachment[0],
																				 !empty($aAttachment[1]) ? $aAttachment[1] : 'application/octet-stream',
																				 !empty($aAttachment[2]) ? $aAttachment[2] : '',
																				 !empty($aAttachment[3]) ? $aAttachment[3] : true,
																				 !empty($aAttachment[4]) ? $aAttachment[4] : 'base64');
			}
			if (PEAR::IsError($mResult) && $aConfig['common']['status'] == 'development') {
      	TriggerError($mResult->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
      	return false;
    	}
		}
	// obrazki HTML
		if (!empty($aHTMLImages)) {
			foreach ($aHTMLImages as $aImage) {
				$pMime->addHTMLImage($aImage[0],
														 isset($aImage[1]) ? $aImage[1] : 'application/octet-stream',
														 isset($aImage[2]) ? $aImage[2] : '',
														 isset($aImage[3]) ? $aImage[3] : true);
			}			
		}
		if (isset($aConfig['_settings']['site']['product']['name']) && isset($aConfig['_settings']['site']['product']['version'])) {
			$aHeaders['X-Mailer'] = $aConfig['_settings']['site']['product']['name'].' '.$aConfig['_settings']['site']['product']['version'];
		}
		elseif (isset($aConfig['common']['name']) && isset($aConfig['common']['version'])) {
			$aHeaders['X-Mailer'] = $aConfig['common']['name'].' '.$aConfig['common']['version'];
		}
    $aHeaders['Return-Path'] = preg_replace('/([^<]+)>?$/', '$1', $aHeaders['From']);

    // XXX  !Uwaga na sztywno ustawione pole w mailach 
    // - nie zawsze konfiguracja serwisu jest wczytana podczas wysyłania maila
    preg_match('/[_a-z0-9-]+(\.[_a-z0-9-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4}))/', $aHeaders['From'], $aMatches);
    $aHeaders['Reply-To'] = $aHeaders['Reply-To'] != '' ? $aHeaders['Reply-To'] : 'kontakt@' . $aMatches[2];
    
		$sBody = $pMime->get($aGetParams);
		$aHdrs = $pMime->headers($aHeaders);

		if (!empty($aHeaders['Reply-To'])) {
			$mBackendParams = '-f'.$aHeaders['Reply-To'];
		}

		$pMail =& Mail::factory($sBackend, $mBackendParams);
    
		$mResult = @$pMail->send($mRecipients, $aHdrs, $sBody);
    
		if (PEAR::IsError($mResult) && $aConfig['common']['status'] == 'development') {
    	TriggerError($mResult->getMessage()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
    	return false;
    }
    return true;
	} // end SendMimeEmail()
	
	
	/**
	 * Metoda wysyla wiadomosc pod adres $sTo, z tematem $sSubject o tresci
	 * $sBody
	 * 
	 * @return	bool
	 */
	function sendMail($sFrom, $sFromEmail, $sTo, $sSubject, $sBody, $bHtml=false, $sReplyToEmail='', $sReturnToEmail='') {
		global $aConfig;
//    return true;// XXX ODBLOKOWANE
    
		$aBody = array();
		$aHtmlBody = array();
		
		$aGetParams = array('head_charset'=>$aConfig['default']['charset'],
												'text_charset'=>$aConfig['default']['charset'],
												'html_charset'=>$aConfig['default']['charset']);
		if ($sFrom != '') {
			$aHeaders['From'] = '"'.$sFrom.'" <'.$sFromEmail.'>';
		}
		else {
			$aHeaders['From'] = $sFromEmail;
		}
    preg_match('/[_a-z0-9-]+(\.[_a-z0-9-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4}))/', $aHeaders['From'], $aMatches);
		$aHeaders['Reply-To'] = $sReplyToEmail != '' ? $sReplyToEmail : 'kontakt@' . $aMatches[2];
		$sReturnPath = $sReturnToEmail != '' ? $sReturnToEmail : $sFromEmail;
		$aHeaders['Subject'] = $sSubject;
		if ($bHtml) {
			$aHtmlBody['body'] = $sBody;
		}
		else {
			$aBody['body'] = $sBody;
		}
		return Common::SendMimeEmail($sTo,
																 $aHeaders,
																 $aBody,
																 $aHtmlBody,
																 $aGetParams);
	} // end of sendMail() method
	
	
	/**
	 * Metoda wysyla wiadomosc pod adres $sTo, z tematem $sSubject o tresci
	 * zarowno $sTxt jak i $sHtml
	 * 
	 * @return	bool
	 */
	function sendTxtHtmlMail($sFrom, $sFromEmail, $sTo, $sSubject, $sTxt, $sHtml, $sReplyToEmail='',$sAttachment='', $aHTMLImages=array()) {
		global $aConfig;
//    return true;// XXX ODBLOKOWANE
    
		$aTxtBody = array();
		$aHtmlBody = array();
		$aGetParams = array('head_charset'=>$aConfig['default']['charset'],
												'text_charset'=>$aConfig['default']['charset'],
												'html_charset'=>$aConfig['default']['charset']);
		if ($sFrom != '') {
			$aHeaders['From'] = '"'.$sFrom.'" <'.$sFromEmail.'>';
		}
		else {
			$aHeaders['From'] = $sFromEmail;
		}
    preg_match('/[_a-z0-9-]+(\.[_a-z0-9-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4}))/', $aHeaders['From'], $aMatches);
    
		$aHeaders['Reply-To'] = $sReplyToEmail != '' ? $sReplyToEmail : 'kontakt@' . $aMatches[2]; 
		$aHeaders['Subject'] = $sSubject;
		if ($sTxt != '') {
			$aTxtBody['body'] = $sTxt;
		}
		if ($sHtml != '') {
			$aHtmlBody['body'] = $sHtml;
		}

		if (!empty($sAttachment)) {

			if (!is_array($sAttachment)) {

				$sAttachment = array(array($sAttachment));
			}

		} else {

			$sAttachment = [];
		}

		return Common::SendMimeEmail($sTo,
																 $aHeaders,
																 $aTxtBody,
																 $aHtmlBody,
																 $aGetParams,
																 $sAttachment,
																 $aHTMLImages);
	} // end of sendTxtHtmlMail() method
	
  /**
   * Metoda wykonuje zapytanie na bazie danych, zwraca liczbe zmienionych
   * rekordow
   *
   * @param   string    $sSQL             - zapytanie SQL
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @return  integer                     - liczba zmienionych rekordow
  function Query($sSQL, $mParameters=array()) {
    global $pDB, $aConfig;

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult = $pDB->query($sSQL, $mParameters);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			} else {
				Common::logSqlError($_SERVER['SCRIPT_FILENAME']."\n".$sSQL."\n".$pResult->getMessage()."\n".$pResult->getDebugInfo());
			}
			return false;
    }
    return $pDB->AffectedRows();
  } // end Query()

  /**
   * Metoda wykonuje zapytanie na bazie danych ograniczajac wyswietlenie
   * wynikow zapytania do podanej ilosci zaczynajac od numeru podanego rekordu
   *
   * @param   string    $sSQL             - zapytanie SQL
   * @param		integer		$iFrom						- poczatkowy rekord zbioru wynikow
   * @param		integer		$iCount						- liczba zwroconych rekordow
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @param   integer   $iFetchMode       - metoda pobierania z bazy
   * @return  integer                     - liczba zmienionych rekordow
  function LimitQuery($sSQL, $iFrom, $iCount, $mParameters=array(), $iFetchMode=DB_FETCHMODE_ASSOC) {
  	global $pDB, $aConfig;
  	
  	$aTempArr		= array();
  	$aResult		= array();

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult = $pDB->limitQuery($sSQL, $iFrom, $iCount, $mParameters);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			} else {
				Common::logSqlError($_SERVER['SCRIPT_FILENAME']."\n".$sSQL."\n".$pResult->getMessage()."\n".$pResult->getDebugInfo());
			}
			return false;
    }
    while ($aTempArr =& $pResult->FetchRow($iFetchMode)) {
      $aResult[] =& $aTempArr;
    }
    return $aResult;
  } // end LimitQuery()

  
  /**
   * Metoda wykonuje zapytanie na bazie danych, dodajac nowy rekord
   * do tabeli $sTable, zawierajacy pola i wartosci z tablicy $aValues.
   * Jezeli w tej tablicy $aValues znajduje sie rekord o wartosci #id#
   * zostaje pobierana wartosc dla nowego klucza dla warunku $sWhere.
   * Jezeli czwarty parametr ma wartosc true metoda zwraca ID nowododanego
   * rekordu (tabel musi posiadac pole ID)
   *
   * @param	string	$sTable	- nazwa tabeli
   * @param	array	$aValuesList	- lista wartosci do wstawienia
   * @param	string	$sWhere	- czesc zapytania WHERE
   * @param	bool	$bReturnId	- true: zwroc Id dodanego rekordu; false: nie zwracaj
   * @return	integer	- ID nowo dodanego rekordu lub false w przypadku bledu
  function Insert($sTable, $aValuesList, $sWhere='', $bReturnId=true) {
    global $pDB, $aConfig;
    $iId = null;

    $sFields = implode(',', array_keys($aValuesList));
    $aValues = array();
    $sTable = $pDB->quoteIdentifier($sTable);

    foreach ($aValuesList as $sKey => $mValue) {
    	if (preg_match('/^#(\w+)#$/i', $mValue, $aMatches)) {
    		// okreslenie nowej wartosci dla kolumny
    		$sSql = "SELECT IFNULL(MAX(".$aMatches[1]."), 0) + 1 FROM ".$sTable.
    						($sWhere != '' ? " WHERE ".$sWhere : '');
    		if (($mValue = Common::GetOne($sSql)) === false) {
    			return false;
    		}
    		$mValue = (int) $mValue;
    		if ($aMatches[1] == 'id') {
    			$iId = $mValue;
    		}
    	}
    	switch ($mValue) {
    		case 'NULL':
    		case 'NOW()':
    			$aValues[] = $mValue;
    		break;

    		default:
    			$aValues[] = $pDB->quoteSmart(stripslashes($mValue));
    		break;
    	}
    }
    $sValues = implode(',', $aValues);

    // wstawianie wiersza
    $sSql = "INSERT INTO ".$sTable." (".$sFields.") VALUES(".$sValues.")";
    if (Common::Query($sSql) === false) {
    	return false;
    }
    

		//dump($sSql);
    if ($bReturnId) {
    	if (!$iId) {
	    	// pobranie ID wstawionego rekordu
		    $sSql = "SELECT MAX(id) FROM ".$sTable.
								($sWhere != '' ? " WHERE ".$sWhere : '');
				if (($iId = Common::GetOne($sSql)) === false) {
					return false;
				}
    	}
			return (int) $iId;
    }
    return true;
  } // end Insert() method


  /**
   * Metoda wykonuje zapytanie na bazie danych, wprowadzajac zmiany
   * w rekordzie odpowiadajacym zapytaniu $sWhere
   *
   * @param	string	$sTable	- nazwa tabeli
   * @param	array	$aValuesList	- lista wartosci do modyfikacji
   * @param	string	$sWhere	- czesc zapytania WHERE
   * @return	mixed - integer - liczba zmodyfikowanych rekordow; false: wystapil blad
  function Update($sTable, $aValuesList, $sWhere='') {
    global $pDB, $aConfig;

    $sValuesList = '';
    $sTable = $pDB->quoteIdentifier($sTable);

    foreach ($aValuesList as $sKey => $mValue) {
    	$sValuesList .= $sKey.' = ';
    	switch ($mValue) {
    		case 'NULL':
    		case 'NOW()':
    			$sValuesList .= $mValue;
    		break;

    		default:
    			$sValuesList .= $pDB->quoteSmart(stripslashes($mValue));
    		break;
    	}
    	$sValuesList .= ', ';
    }
    $sValuesList = substr($sValuesList, 0, -2);

    // aktualizacja
    $sSql = "UPDATE ".$sTable." SET ".$sValuesList.
						 ($sWhere != '' ? " WHERE ".$sWhere : '');
	
				 
		//dump($sSql);
		return Common::Query($sSql);
  } // end Update() method
   */

  function Quote($mValue) {
  	global $pDB;
  	return $pDB->quoteSmart(stripslashes($mValue));
  }

  
  /**
   * Metoda pobiera ostatnią klasę pomijając klasy dostępu do bazy danych
   * 
   * @param array $aData
   * @return int
   */
  static function _getDebugLastClass($aData) {
    foreach ($aData as $iKey => $mClass) {
      if (isset($mClass['file']) && 
          $mClass['file'] !== '' &&
          stristr($mClass['file'], 'lib/DatabaseManager') === false &&
          stristr($mClass['file'], 'lib/Common') === false
          ) {
        if (isset($aData[$iKey+1])) {
          return ($iKey + 1);
        } else {
          return $iKey;
        }
      }
    }
    return $iKey;
  }// end of _getDebugLastClass() method
  
  
  /**
   * Metoda pobiera komentarz zapytania SQL
   * 
   * @return string
   */
  static function _getDebugCommentSQL() {
    $sComment = '';
            
    $aData = debug_backtrace();
    $iKey = self::_getDebugLastClass($aData);
    if (isset($aData[$iKey])) {
      $aObjData = $aData[$iKey];
      if (!empty($aData) && !empty($aObjData)) {
        $sFile = '...'.substr($aObjData['file'], -30);
        $sLine = '['.$aObjData['line'].']';
        $sFunction = '  '.$aObjData['function'].'';
        if (isset($aObjData['object']) && is_object($aObjData['object'])) {
          $sObject = ''.get_class($aObjData['object']).'';
        }
        $sComment .= $sFile.$sLine.' - '.(isset($sObject) ? $sObject : '').'::'.$sFunction;
      }
      if (!empty($_SERVER['REMOTE_ADDR'])) {
        $sComment .= ' IP:'.$_SERVER['REMOTE_ADDR'];
      }
    }
    
    $sComment = '/*'.$sComment.'*/';
    return $sComment;
  }// end of _getDebugCommentSQL() method
  
  
  /**
   * Metoda wykonuje zapytanie na bazie danych, zwraca wartosc
   * pierwszej kolumny z pierwszego rekordu
   *
   * 
   * @global type $pDB
   * @global type $aConfig
   * @param string $sSQL - zapytanie SQL
   * @param mixed $mParameters - parametry dla zapytania
   * @return mixed|boolean - zawartosc pierwszej kolumny pierwszego rekordu lub false
  function &GetOne($sSQL, $mParameters=array()) {
    global $pDB, $aConfig;

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $pDB->getOne($sSQL, $mParameters);
    if (PEAR::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			} else {
				Common::logSqlError($_SERVER['SCRIPT_FILENAME']."\n".$sSQL."\n".$pResult->getMessage()."\n".$pResult->getDebugInfo());
			}
			return false;
    }
    return $pResult;
  } // end GetOne()
*/

  /**
   * Metoda zwraca pierwszy wiersz wyniku zapytania
   *
   * @param   string    $sSQL             - zapytanie SQL
   * @param   array     $aParameters      - parametry dla zapytania
   * @param   integer   $iFetchMode       - metoda pobierania z bazy
   * @return  array                       - tabela zawierajaca pierwszy rekord wyniku zapytania
  function &GetRow($sSQL, $aParameters=array(), $iFetchMode=DB_FETCHMODE_ASSOC) {
    global $pDB, $aConfig;

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $pDB->getRow($sSQL, $aParameters, $iFetchMode);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			} else {
				Common::logSqlError($_SERVER['SCRIPT_FILENAME']."\n".$sSQL."\n".$pResult->getMessage()."\n".$pResult->getDebugInfo());
			}
			return false;
    }
    return $pResult;
  } // GetRow()


  /**
   * Metoda zwraca wskazana kolumne wyniku zapytania
   *
   * @param   string    $sSQL             - zapytanie SQL
   * @param   mixed     $mColumn          - numer (zaczynajac od 0) lub nazwa kolumny
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @return  array                       - tabela zawierajaca wskayana kolumne wyniku zapytania
  function &GetCol($sSQL, $mColumn=0, $mParameters=array()) {
    global $pDB, $aConfig;

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $pDB->getCol($sSQL, $mColumn, $mParameters);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			} else {
				Common::logSqlError($_SERVER['SCRIPT_FILENAME']."\n".$sSQL."\n".$pResult->getMessage()."\n".$pResult->getDebugInfo());
			}
      return false;
    }
    return $pResult;
  } // GetCol()


  /**
   * Metoda zwraca wynik zapytania w postaci tablicy asocjacyjnej
   * Jezeli wynik zapytania zawiera wiecej niz 2 kolumny, wynikiem bedzie tablica
   * wartosci zaczynajac od kolumny drugiejdo n-tej. Jezeli wynik zawiera tylko 2 kolumny,
   * zwracana wartoscia bedzie skalar z wartoscia drugiej kolumny (z wyjatkiem sytuacji
   * gdy wymuszony jest zwrot tablicy poprzez parametr $bForceArray).
   *
   * @param   string    $sSQL             - zapytanie SQL
   * @param   boolean   $bForceArray      - wymuszaj wynik w postaci tablicy
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @param   integer   $iFetchMode       - metoda pobierania z bazy
   * @param   boolean   $bGroup           - grupuj wyniki dla tej samego klucza w tablice
   * @return  array                       - tabela zawierajaca wskayana kolumne wyniku zapytania
  function &GetAssoc($sSQL, $bForceArray=false, $mParameters=array(), $iFetchMode=DB_FETCHMODE_ASSOC, $bGroup=false) {
    global $pDB, $aConfig;

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $pDB->getAssoc($sSQL, $bForceArray, $mParameters, $iFetchMode, $bGroup);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			} else {
				Common::logSqlError($_SERVER['SCRIPT_FILENAME']."\n".$sSQL."\n".$pResult->getMessage()."\n".$pResult->getDebugInfo());
			}
			return false;
    }
    return $pResult;
  } // GetAssoc()


  /**
   * Metoda wykonuje zapytanie na bazie danych i zwraca rekordy wyniku
   * w postaci tablicy
   *
   * @param   string    $sSQL             - zapytanie SQL
   * @param   array     $aParameters      - parametry dla zapytania
   * @param   integer   $iFetchMode       - metoda pobierania z bazy
   * @return  array                       - tabela zawierajaca rekordy wyniku zapytania
  function &GetAll($sSQL, $aParameters=array(), $iFetchMode=DB_FETCHMODE_ASSOC) {
    global $pDB, $aConfig;

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $pDB->getAll($sSQL, $aParameters, $iFetchMode);
    if (PEAR::isError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			} else {
				Common::logSqlError($_SERVER['SCRIPT_FILENAME']."\n".$sSQL."\n".$pResult->getMessage()."\n".$pResult->getDebugInfo());
			}
			return false;
    }
    return $pResult;
  } // end GetAll()
  
  
   /**
   * Metoda wykonuje zapytanie na bazie danych, obiekt zapytania
   *
   * @param   string    $sSQL             - zapytanie SQL
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @return  integer                     - liczba zmienionych rekordow
  function PlainQuery($sSQL, $mParameters=array()) {
    global $pDB, $aConfig;

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $pDB->query($sSQL, $mParameters);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			} else {
				Common::logSqlError($_SERVER['SCRIPT_FILENAME']."\n".$sSQL."\n".$pResult->getMessage()."\n".$pResult->getDebugInfo());
			}
			return false;
    }
    return $pResult;
  } // end PlainQuery() 
  


  /**
   * Metoda zwraca ostatni dodane ID z kolumny o atrybucie AUTO_INCREMENT
   *
   * @return  integer                       - ostatnio dodane ID
  function &GetLastInsertedID() {
    global $pDB;

    return $pDB->GetOne("SELECT LAST_INSERT_ID()");
  } // end GetLastInsertedID()


  /**
   * Metoda rozpoczyna transakcje w bazie danych
   *
   * @return	void
  function BeginTransaction() {
  	global $pDB;

  	$pDB->query('BEGIN');
	} // end of BeginTransaction() function


	/**
   * Metoda zatwierdza transakcje w bazie danych
   *
   * @return	void
  function CommitTransaction() {
  	global $pDB;

  	$pDB->query('COMMIT');
	} // end of CommitTransaction() function


	/**
   * Metoda wycofuje transakcje w bazie danych
   *
   * @return	void
  function RollbackTransaction() {
  	global $pDB;

  	$pDB->query('ROLLBACK');
	} // end of RollbackTransaction() function
   */


	function encodeString($sStr) {
		$sStr = htmlentities(stripslashes($sStr), ENT_COMPAT, 'UTF-8');
		return $sStr;
	}


	/**
	 * Metoda pobiera i zwraca symbol strony
	 *
	 * @param	integer	$iId	- Id strony
	 * @param	integer	$iLangId	- Id wersji jezykowej
	 */
	function getPageSymbol($iId, $iLangId) {
		global $aConfig;

		$sSql = "SELECT symbol
					   FROM ".$aConfig['tabls']['prefix']."menus_items
					   WHERE language_id = ".$iLangId." AND
					   			 id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getPageSymbol() method


	/**
	 * Metoda zwraca Id najbardziej zaglebionej strony do ktorej linkuje strona
	 * o $iId. Jezeli strona o $iId nie linkuje do zadnej innej zwracane jest $iId
	 *
	 * @param	integer	$iId	- Id strony
	 * @param	integer	$iLangId	- Id wersji jezykowej
	 * @return	integer	- Id strony do ktorej linkuje strona :)
	 */
	function getLinkToId($iId, $iLangId) {
		global $aConfig;
		$iTemp = 0;

		$sSql = "SELECT link_to_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$iLangId." AND
						 			 id = !";
		while (($iTemp = intval(Common::GetOne($sSql, $iId))) > 0) {
			$iId = $iTemp;
		}
		return $iId;
	} // end of getLinkToId() method


	/**
	 * Metoda sprawdza czy strona o podanym id jest 'Linkiem wewnetrznym'
	 *
	 * @param		integer	$iId	- ID strony
	 * @param	integer	$iLangId	- Id wersji jezykowej
	 * @return	bool	- true: jest; false: nie jest
	 */
	function isInternalLink($iId, $iLangId) {
		global $aConfig;

		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE id = ".$iId." AND
						 			 language_id = ".$iLangId." AND
									 mtype = '1'";

		return intval(Common::GetOne($sSql)) == 1;
	} // end of isInternalLink() function


	/**
	 * Metoda sprawdza czy strona o podanym id jest 'Linkiem zewnetrznym'
	 *
	 * @param		integer	$iId	- ID strony
	 * @param	integer	$iLangId	- Id wersji jezykowej
	 * @return	bool	- true: jest; false: nie jest
	 */
	function isExternalLink($iId, $iLangId) {
		global $aConfig;

		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE id = ".$iId." AND
						 			 language_id = ".$iLangId." AND
									 mtype = '2'";

		return intval(Common::GetOne($sSql)) == 1;
	} // end of isExternalLink() function


	/**
	 * Metoda pobiera link zewnetrzny strony o $iId
	 *
	 * @param		integer	$iId	- ID strony
	 * @param	integer	$iLangId	- Id wersji jezykowej
	 * @return	string	- link zewnetrzny
	 */
	function getExternalLink($iId, $iLangId) {
		global $aConfig;

		$sSql = "SELECT url
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE id = ".$iId." AND
						 			 language_id = ".$iLangId;

		return Common::GetOne($sSql);
	} // end of getExternalLink() function


	/**
	 * Metoda sprawdza czy istnieje modul o podanej nazwie
	 *
	 * @param	string	$sModule	- nazwa modulu
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function moduleExists($sModule) {
		global $aConfig;

		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE symbol = '".$sModule."'";

		return (int) Common::GetOne($sSql) > 0;
	} // end of moduleExists() function


	/**
	 * Funkcja formatuje cene
	 *
	 * @param	float	$fPrice	- cena
	 * @return	string	- sformatowana cenat
	 */
	function formatPrice($fPrice, $sThousandsSep='') {
		return number_format((double)str_replace(',', '.', $fPrice), 2, ',', $sThousandsSep);
	} // end of formatPrice() method


	/**
	 * Funkcja formatuje cene do formatu MySQL
	 *
	 * @param	float	$fPrice	- cena
	 * @return	string	- sformatowana cena
	 */
	function formatPrice2($fPrice) {
		return number_format((double)str_replace(',', '.', $fPrice), 2, '.', '');
	} // end of formatPrice2() method
	
	
	/**
	 * Funkcja formatuje cene - bez koncowych zer
	 * 
	 * @param	float	$fPrice	- cena
	 * @return	string	- sformatowana cenat
	 */
	function formatPrice3($fPrice, $sThousandsSep='') {
		return preg_replace('/,?0+$/', '', number_format(str_replace(',', '.', $fPrice), 2, ',', $sThousandsSep));
	} // end of formatPrice3() method
	
	/**
	 * Funkcja formatuje wagę (3 po przecinku)
	 *
	 * @param	float	$fPrice	- cena
	 * @return	string	- sformatowana cenat
	 */
	function formatWeight($fPrice, $sThousandsSep='') {
		return number_format((double)str_replace(',', '.', $fPrice), 3, ',', $sThousandsSep);
	} // end of formatPrice() method


	/**
	 * Funkcja formatuje wagę do formatu MySQL (3 po przecinku)
	 *
	 * @param	float	$fPrice	- cena
	 * @return	string	- sformatowana cena
	 */
	function formatWeight2($fPrice) {
		return number_format(str_replace(',', '.', $fPrice), 3, '.', '');
	} // end of formatPrice2() method
	
	
	/**
	 * Metoda zapisuje informacje o wykupionych dostepach
	 * do poszczegolnych tabel (wykupione artykuly, pakiety wartosciowe,
	 * hisotria operacji na koncie, itp.)
	 * 
	 * @param	integer	$iUId	- Id uzytkownika
	 * @param	integer	$iOId	- Id zamowienia
	 * @return	bool	- true: sukces; false: wystapil blad
	 */
	function setOrderItems($iUId, $iOId) {
		global $aConfig;
		
		// okreslenie wartosci zamowionych pakietow wartosciowych
		$sSql = "SELECT IFNULL(SUM(value), 0) as amount
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id = ".$iOId." AND
						 			 item = 'packet'";
		$fAmount = (float) Common::GetOne($sSql);
		if ($fAmount > 0) {
			// okreslenie dotychczasowego stanu konta
			$sSql = "SELECT money
							 FROM ".$aConfig['tabls']['prefix']."users_accounts
							 WHERE id = ".$iUId;
			$fMoney = (float) Common::GetOne($sSql);
			// zapis do historii operacji na koncie
			$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."users_accounts_balance_history
							 (user_id, operation_date, operation, balance_before, balance_after, amount, name, otype)
							 VALUES(".$iUId.", NOW(), '1', ".$fMoney.", (".$fMoney." + ".$fAmount."), ".$fAmount.", CONCAT(".$iOId.", '|', (SELECT order_date FROM ".$aConfig['tabls']['prefix']."orders WHERE id = ".$iOId.")), 'packet')";
			if (Common::Query($sSql) === false) return false;
			// uznanie srodkow na koncie
			if (Common::changeUserBalance($iUId, $fAmount) === false) return false;
		}
		// jezeli byly jakies zakupione artykuly
		// - dodanie ich do tabeli 'users_accounts_articles'
		$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."users_accounts_articles
						 SELECT ".$iUId.", article_id FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id = ".$iOId." AND
						 			 item = 'article'
						 			 AND article_id IS NOT NULL";
		if (Common::Query($sSql) === false) return false;
		// jezeli byly jakies zakupione porady
		// - dodanie lub aktualizacja informacji o ich oplaceniu
		// do tabeli 'users_accounts_articles'
		$sSql = "SELECT advice_id FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id = ".$iOId." AND
						 			 item = 'advice'
						 			 AND advice_id IS NOT NULL";
		$aAdvices =& Common::GetCol($sSql);
		foreach ($aAdvices as $iAId) {
			if (Common::isAdviceCreator($iUId, $iAId)) {
				// to uzytkownik napisal zapytanie o te porade - aktualizacja
				// paid z '0' na '1'
				$aValues = array(
					'paid' => '1'
				);
				if (Common::Update($aConfig['tabls']['prefix']."users_accounts_advices",
													 $aValues,
													 "advice_id = ".$iAId." AND
													  user_id = ".$iUId) === false) {
					return false;
				}
			}
			else {
				// to nie uzytkownik napisal zapytanie o te porade - dodanie
				// wiersza do tabeli 'users_accounts_advices'
				$aValues = array(
					'advice_id' => $iAId,
					'user_id' => $iUId,
					'paid' => '1'
				);
				if (Common::Insert($aConfig['tabls']['prefix']."users_accounts_advices",
													 $aValues,
													 "",
													 false) === false) {
					return false;
				}
			}
		}
		return true;
	} // end of setOrderItems() method
	
	
	
	/**
	 * Metoda pobiera Id uzytkownika skladajacego zamowienie o id $iId
	 * 
	 * @return	integer	- Id uzytkownika
	 */
	function getOrderUserId($iId) {
		global $aConfig;
		
		$sSql = "SELECT user_id
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id = ".$iId;
		return (int) Common::GetOne($sSql);
	} // end of getOrderUserId() method
	

	/**
	 * Metoda sprawdza czy uzytkownik napisal zapytanie o porade o id $iId
	 * 
	 * @param	integer	$iUId	- Id uzytkownika
	 * @param	integer	$iId	- Id porady
	 * @return	bool	- true: napisal; false: nie napisal
	 */
	function isAdviceCreator($iUId, $iId) {
		global $aConfig;
		
		$sSql = "SELECT advice_id
						 FROM ".$aConfig['tabls']['prefix']."users_accounts_advices
						 WHERE user_id = ".$iUId." AND
						 			 advice_id = ".$iId;
		return (int) Common::GetOne($sSql) > 0;
	} // end of isAdviceCreator() method
	
	
	/**
	 * Metoda pobiera Id menu oferty produktowej
	 * 
	 * @param	integer	$iLId	- Id wersji jezykowej
	 * @return	integer
	 */
	function &getProductsMenu($iLId) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id = ".$iLId." AND
						 			 products = '1'";
		return Common::GetCol($sSql);
	} // end of getProductsMenu() method
	
	/**
	 * Metoda pobiera Id menu oferty produktowej dla bazy
	 * 
	 * @param	integer	$iLId	- Id wersji jezykowej
	 * @return	integer
	 */
	function &getProductsMenuM($sDb,$iLId) {
		global $aConfig,$pDbMgr;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id = ".$iLId." AND
						 			 products = '1'";
		return $pDbMgr->GetCol($sDb,$sSql);
	} // end of getProductsMenu() method
	
	function strip_slashes($sStr) {
		return get_magic_quotes_gpc() ? stripslashes($sStr) : $sStr;
	} // end of add_slashes() method
	
	/**
	 * Metoda sprawdza czy przekazany do niej tekst jest tekstem w kodowaniu utf8
	 * poprzez sprawdzenie czy wystepuja w nim polskie znaki w tym kodowaniu
	 * 
	 * DZIALA TYLKO Z POLSKIMI ZNAKAMI
	 * 
	 * @param	string	$sStr	- string do zbadania
	 * @return	bool	- true: utf8; false: nie utf8
	 */
	function isUTF8($sStr) {
		$aCodes = array(
			"\xC4\x84", "\xC4\x85", // A, a
			"\xC4\x86", "\xC4\x87", // C, c
			"\xC4\x98", "\xC4\x99", // E, e
			"\xC5\x81", "\xC5\x82", // L, l
			"\xC5\x83", "\xC5\x84", // N, n
			"\xC3\x93", "\xC3\xB3", // O, o
			"\xC5\x9A", "\xC5\x9B", // S, s
			"\xC5\xB9", "\xC5\xBA", // Z('), z(')
			"\xC5\xBB", "\xC5\xBC" // Z(.), z(.)
		);
		foreach ($aCodes as $sCode) {
			if (strpos($sStr, $sCode) !== false) {
				return true;
			}
		}
		return false;
	} // end of isUTF8() method
	
	
	/**
	 * Metoda konwertuje string z kodowania Win-1250 a UTF8
	 * sprawdzajac czy string nie jest juz w kodowaniu UTF8
	 * 
	 * @param	string	$sStr	- string do konwersji
	 * @return	string
	 */
	function convert($sStr) {
		if (!Common::isUTF8($sStr))
			return iconv('windows-1250', 'utf-8', $sStr);
		return $sStr;
	} // end of convert() method
	
	
	function addSourceLinkParam($bTxt, $sString, $sType, $iId=0) {
		global $aConfig;
		
		$sSource = base64_encode($sType.($iId > 0 ? ':'.$iId : ''));
		$sBaseUrl = str_replace('.', '\\.', substr($aConfig['common']['client_base_url'], 0, -1));
		if ($bTxt) {
			// newsletter tekstowy
			// dodanie parametru do adresow z istniejacym juz ciagiem QUERY_STRING
			$sString = preg_replace('/(http:\/\/)?(www\.)?'.$sBaseUrl.'[^\s]+\?[^\s]*/i', '\\0&src='.$sSource, $sString);
			// dodaie parametru do adresow bez ciagu QUERY_STRING
			$sString = preg_replace('/((http:\/\/)?(www\.)?'.$sBaseUrl.'[^\s\?]*)[\s]/i', '\\1?src='.$sSource, $sString);
		}
		else {
			// newsletter HTML
			// dodanie parametru do adresow z istniejacym juz ciagiem QUERY_STRING
			$sString = preg_replace('/href[\s]*=[\s]*"((http:\/\/)?(www\.)?'.$sBaseUrl.'[^\s]+\?[^\s]*)"/i', 'href="\\1&src='.$sSource.'"', $sString);
			// dodaie parametru do adresow bez ciagu QUERY_STRING
			$sString = preg_replace('/href[\s]*=[\s]*"((http:\/\/)?(www\.)?'.$sBaseUrl.'[^\s\?]*)"/i', 'href="\\1?src='.$sSource.'"', $sString);
		}
		return $sString;
	} // end of addSourceLinkParam() method
	
/**
 * Funkcja pobiera tresc maila strony 
 * 
 * @param	string $sSymbol		-	symbol maila
 * @return 	array				-	mail
 */	
function getWebsiteMail($sSymbol, $sWebsite=''){
	global $aConfig, $pDbMgr;
	
	if ($sWebsite != '') {
		$sSql = "SELECT template, content, subject FROM ".$aConfig['tabls']['prefix']."website_emails WHERE symbol = '".$sSymbol."'";
		return $pDbMgr->GetRow($sWebsite, $sSql);
	} else {
		$sSql = "SELECT template, content, subject FROM ".$aConfig['tabls']['prefix']."website_emails WHERE symbol = '".$sSymbol."'";
		return Common::GetRow($sSql);
	}
}


/**
 * Metoda pobiera stopkę maila z konfiguracji wybranego serwisu
 *
 * @global type $aConfig
 * @global type $pDbMgr
 * @param type $sWebsite 
 */
function getWebsiteMailFooter($sWebsite) {
	global $aConfig, $pDbMgr;
	
	$sSql = "SELECT mail_footer FROM ".$aConfig['tabls']['prefix']."website_settings";
	return $pDbMgr->GetOne($sWebsite, $sSql);
}// end of getWebsiteMailFooter() method


/**
 * Funkcja wysyła maila
 * @param unknown_type $aMail - tablica uzyskana z funkcji getWebsiteMail
 * @param unknown_type $sSender - nadawca
 * @param unknown_type $sSenderEmail - mail nadawcy
 * @param unknown_type $sTargetEmail - mail odiorcy
 * @param unknown_type $sContent - treść
 * @param unknown_type $aSymbols - symbole do podmiany ({symbol1}, {symbol2}, ....)
 * @param unknown_type $aValues - wartosći do podmiany za symbole
 * @param unknown_type $aEmailVars - tablica do podpiecia do smartów jako $aMail
 * @return bool
 */
function sendWebsiteMail($aMail, $sSender, $sSenderEmail, $sTargetEmail, $sContent, $aSymbols=array(),$aValues=array(),$aEmailVars=array(),$aAdditionalImages=array(), $sWebsite='', $attachments = []){
	global $aConfig,$pSmarty;
//  return true;// XXX ODBLOKOWANE
  
	$aHtmlImages = array();
//	dump($aMail);
	$sContent = str_replace($aSymbols,$aValues,$sContent);
	$pSmarty->assign_by_ref('sContent', $sContent);
	if ($sWebsite != '') {
		$sFooter = Common::getWebsiteMailFooter($sWebsite);
	} else {
		$sFooter = (!empty($aConfig['default']['website_mail_footer'])?$aConfig['default']['website_mail_footer']:$aConfig['_settings']['site']['mail_footer']);
	}
	$pSmarty->assign_by_ref('sFooter', $sFooter);
	$pSmarty->assign_by_ref('sBaseHref', $aConfig['common'][$sWebsite]['client_base_url_http_no_slash']);
	if(!empty($aEmailVars)){
		$pSmarty->assign_by_ref('aMail', $aEmailVars);
	}
	$sHtml = $pSmarty->fetch($aConfig['common']['client_base_path'].'smarty/templates/mails/'.$sWebsite.'/'.$aMail['template']);
	//	echo($sHtml); die();
	$pSmarty->clear_assign('sContent');
	$pSmarty->clear_assign('sFooter');
	$pSmarty->clear_assign('sBaseHref');
	if(!empty($aEmailVars)){
		$pSmarty->clear_assign('aMail');
	}
	
	if(!empty($aConfig['common'][$sWebsite]['email_logo_file'])){
		$aImgs[] = $aConfig['common'][$sWebsite]['email_logo_file'];
	}
	if(!empty($aAdditionalImages)){
		$aImgs = $aAdditionalImages;
	}
	if(!empty($aImgs)){
		foreach($aImgs as $sImg){
			$sImgF = $aConfig['common']['client_base_path'].$sImg;
			$aImage = getimagesize($sImgF);
			$aHtmlImages[] = array($sImgF, $aImage['mime']);
		}
	}
	$aMail['subject'] = '=?UTF-8?B?'.base64_encode($aMail['subject']).'?=';

	return Common::sendTxtHtmlMail($sSender, $sSenderEmail, $sTargetEmail, $aMail['subject'], '', $sHtml, '', $attachments, $aHtmlImages);
}

/**
 * Metoda zapisuje logi komunikatu
 *
 * @global array $aConfig
 * @param string $sMsg 
 */
function logSqlError($sMsg){
	global $aConfig; 
	$fp = fopen($aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].'logs/sql'.date('dmY').'.log', 'a');
	fwrite($fp, '['.date("d.m.Y H:m:s")."] \n".$sMsg."\n\n");
	fclose($fp);
}// end of logSqlError() method
	
/**
 * Konwertuje wartośc całkowitą na postać zapisaną słownie
 * @param int $iVal - wartośc całkowita
 * @return string - wartosc słownie
 */
function intValue2words($iVal){
	$sStr = '';
	$iRest = 0;
	$bMinus = false;
	$iRzad = 0;
	$j = 0;
	$aJednosci = array('', ' jeden', ' dwa', ' trzy', ' cztery', ' pięć', ' sześć', ' siedem', ' osiem', ' dziewięć');
	$aNascie = array('dziesięć', ' jedenaście', ' dwanaście', ' trzynaście', ' czternaście', ' piętnaście', ' szesnaście', ' siedemnaście', ' osiemnaście', ' dziewiętnaście');
	$aDziesiatki =array('', ' dziesięć', ' dwadzieścia', ' trzydzieści', ' czterdzieści', ' pięćdziesiąt', ' sześćdziesiąt', ' siedemdziesiąt', ' osiemdziesiąt', ' dziewięćdziesiąt');
	$aSetki = array('', ' sto', ' dwieście', ' trzysta', ' czterysta', ' pięćset', ' sześćset', ' siedemset', ' osiemset', ' dziewięćset');
	$aRzedy = array('', ' tys.', ' mln.', ' mld.', ' bln.', ' bld.');
    
	if ($iVal < 0) {
		$bMinus = true;
		$iVal=-$iVal;
	}

	if ($iVal == 0) $sStr = 'zero';

	while ($iVal > 0) {
	//	dump($iVal .' - '.$sStr);
		$iRest = (int)($iVal%10);
		$iVal /= 10;
		$iVal = (int)$iVal;
		if (($j == 0) && ($iVal%100 != 0 || $iVal < 100)) {
			$sStr = $aRzedy[$iRzad].$sStr;
		}
		if (($j == 0) && ($iVal%10 != 1)) {
			$sStr = $aJednosci[$iRest].$sStr;
		}
		if (($j == 0) && ($iVal%10 == 1)) {
			$sStr = $aNascie[$iRest].$sStr;
			$iVal /= 10;
			$iVal = (int)$iVal;
			$j+=2;
			continue;
		}
		if ($j==1) {
			$sStr = $aDziesiatki[$iRest].$sStr;
		}
		if ($j==2){
			$sStr = $aSetki[$iRest].$sStr;
			$j=-1;
			$iRzad++;
		}
		$j++;
	}

	if ($bMinus) {
		$sStr = "minus".$sStr;
	}
	return $sStr;
} // end of intValue2words() function
/**
 * Funkcja konwertuje cene na postać słowną
 * @param float $fVal - wartośc liczbowa
 * @return string - zapis słowny
 */
function price2Words($fVal){
global $aConfig;
	$sStr = '';
	$iPrice = (int)$fVal;
	$iFrac = (int)round((100*($fVal - $iPrice)));
	$sStr = Common::intValue2words($iPrice).' '.$aConfig['lang']['common']['currency'];
	$sStr .= ' '.trim(Common::intValue2words($iFrac)).' '.$aConfig['lang']['common']['currency_fraction'];
	return $sStr;
} // end of price2Words()


} // end class Common
?>