<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     modifier
 * Name:     intval
 * Purpose:  returns intval of float number/string
 * Added by: Marcin Korecki <korecki@pnet.pl> 09.11.2003 
 * -------------------------------------------------------------
 */
function smarty_modifier_intval($string)
{
	return intval($string);
}

?>
