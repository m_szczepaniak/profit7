<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 * @author Marcin Korecki <m.korecki@omnia.pl>
 */


/**
 * Smarty {menu_max_height} function plugin
 *
 * Type:     function<br>
 * Name:     menu_max_height<br>
 * Purpose:  returns max height by multiplying $params['item_height'] by
 * 					 maximum number of $params['items'] array items
 * @param   array   $params           - function parameters
 * @param   object  $smarty           - reference to smarty object
 * @return  string                    - maximum height
 */
  function smarty_function_menu_max_height($params, &$smarty) {
 		$iMaxCount = 0;
 		for ($i = 0; $i < count($params['items']); $i++) {
 			if (isset($params['items'][$i]['children']['item']) &&
 					($iCount = count($params['items'][$i]['children']['item'])) > $iMaxCount) {
 				$iMaxCount = $iCount; 
 			}
 		}
    return $iMaxCount * $params['item_height'];
  }
?>