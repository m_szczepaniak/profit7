<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 * @author Marcin Korecki <marcin.korecki@m2system.pl>
 */


/**
 * Smarty {loop_pattern} function plugin
 *
 * Type:     function<br>
 * Name:     loop_pattern<br>
 * Purpose:  prints out $pattern $counter times
 * @param   array   $params           - function parameters
 * @param   object  $smarty           - reference to smarty object
 * @return  string                    - multiplied pattern
 */
  function smarty_function_loop_pattern($params, &$smarty) {
    $sRetString = '';
    if (intval($params['from']) <= 0 || intval($params['to']) < 0
        || intval($params['from']) < intval($params['to'])) {
      return '';
    }
    for($iI = 0; $iI < intval($params['from']) - intval($params['to']); $iI++) {
      $sRetString .= $params['pattern'];
    }
    //echo htmlspecialchars($sRetString);
    return $sRetString;
  }
?>