<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty strip modifier plugin
 *
 * Type:     modifier<br>
 * Name:     strip<br>
 * Purpose:  Replace all repeated spaces, newlines, tabs
 *           with a single space or supplied replacement string.<br>
 * Example:  {$var|strip} {$var|strip:"&nbsp;"}
 * Date:     September 25th, 2002
 * @link http://smarty.php.net/manual/en/language.modifier.strip.php
 *          strip (Smarty online manual)
 * @author   Monte Ohrt <monte@ispi.net>
 * @version  1.0
 * @param string
 * @param string
 * @return string
 */
function smarty_modifier_strip_pl($text)
{
	$aUtf8 = array(
			"\xC4\x84", "\xC4\x85", // A, a
			"\xC4\x86", "\xC4\x87", // C, c
			"\xC4\x98", "\xC4\x99", // E, e
			"\xC5\x81", "\xC5\x82", // L, l
			"\xC5\x83", "\xC5\x84", // N, n
			"\xC3\x93", "\xC3\xB3", // O, o
			"\xC5\x9A", "\xC5\x9B", // S, s
			"\xC5\xB9", "\xC5\xBA", // Z('), z(')
			"\xC5\xBB", "\xC5\xBC"  // Z(.), z(.)
		);
		$aNo = array(
			"\x41", "\x61", // A, a
			"\x43", "\x63", // C, c
			"\x45", "\x65", // E, e
			"\x4C", "\x6C", // L, l
			"\x4E", "\x6E", // N, n
			"\x4F", "\x6F", // O, o
			"\x53", "\x73", // S, s
			"\x5A", "\x7A", // Z('), z(')
			"\x5A", "\x7A"  // Z(.), z(.)
		);
   	return str_replace($aUtf8, $aNo, $text);
}

/* vim: set expandtab: */

?>
