<?php

/*
 * Smarty function
 * -------------------------------------------------------------
 * Type:     modifier
 * Name:     dump
 * Purpose:  dumps variable into popup
 * Added by: Arkadiusz Golba 05.05.2010
 * -------------------------------------------------------------
 */
function smarty_function_serialize($aParams, &$oSmarty)
{

	return serialize($aParams['var']);
}
?>
