<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     modifier
 * Name:     multby
 * Purpose:  returns multiplied $number by $multby
 * Added by: Marcin Korecki <korecki@pnet.pl> 09.11.2003 
 * -------------------------------------------------------------
 */
function smarty_modifier_multby($number, $multby)
{
	return doubleval($number) * doubleval($multby);
}

?>
