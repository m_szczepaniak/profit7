<?php

/*
 * Smarty function
 * -------------------------------------------------------------
 * Type:     modifier
 * Name:     dump
 * Purpose:  dumps variable into popup
 * Added by: Arkadiusz Golba 05.05.2010
 * -------------------------------------------------------------
 */
function smarty_function_dump($aParams, &$oSmarty)
{

	$sHtml = "<script language=\"javascript\" type=\"text/javascript\">
	nw=window.open('','dump','height=400,width=800,scrollbars=1');
	var tmp = nw.document;
	tmp.write('<html><head><title>dump</title>');
	tmp.write('</head><body><pre>');
	tmp.write('".addslashes(str_replace("\n", '', str_replace("\r\n", '<br />', nl2br(print_r($aParams['var'],true)))))."');
	tmp.write('</pre></body></html>');
	tmp.close();
</script>";
	return $sHtml;
}
?>
