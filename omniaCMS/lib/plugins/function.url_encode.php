<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 * @author Marcin Korecki <m.korecki@omnia.pl>
 */


/**
 * Smarty {url_encode} function plugin
 *
 * Type:     function<br>
 * Name:     url_encode<br>
 * Purpose:  encodes given string for URL
 * @param   array   $params           - function parameters
 * @param   object  $smarty           - reference to smarty object
 * @return  string                    - url encoded string
 */
  function smarty_function_url_encode($params, &$smarty) {
 
    return urlencode($params['string']);
  }
?>