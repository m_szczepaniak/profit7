<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 * @author Arkadiusz Golba <m.korecki@omnia.pl>
 */


/**
 * Smarty {url_encode} function plugin
 *
 * Type:     function<br>
 * Name:     url_encode<br>
 * Purpose:  encodes given string for URL
 * @param   array   $params           - function parameters
 * @param   object  $smarty           - reference to smarty object
 * @return  string                    - url encoded string
 */
  function smarty_function_get_publisher_link($params, &$smarty) {
 
    return getPublisherLink($params['string']);
  }
?>