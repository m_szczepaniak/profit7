<?php
/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:        modifier
 *
 * Name:        format_number
 *
 * Purpose:     format numbers by inserting thousands seperators
 *              this is basically a wrapper for number_format
 *              with additional processing to retain the original
 *              digits after the decimal point (if any)
 *
 * Input:       string: number to be formatted
 *              decimals: [optional] number of decimal places
 *              dec_point: [optional] decimal point character
 *              thousands_sep: [optional] thousands seperator
 *
 * Examples:    {$number|commify}    12345.288 => 12,345.288
 *              {$number|commify:2}    12345.288 => 12,345.29
 *
 * Install:     Drop into the plugin directory as modifier.format_number.php.
 *
 * Author:      James Brown <james [at] hmpg [dot] net>
 * -------------------------------------------------------------
 */
 
function smarty_modifier_format_number($string, $decimals=-1, $dec_point=',', $thousands_sep='') {
    if ($decimals == -1) {
        if (preg_match('/\.\d+/', $string))
            return number_format((double)$string) . preg_replace('/.*(\.\d+).*/', '$1', $string);
        else
            return number_format((double)$string);
    }
    else
        return number_format((double)str_replace(',', '.', $string), $decimals, $dec_point, $thousands_sep);
}
?>