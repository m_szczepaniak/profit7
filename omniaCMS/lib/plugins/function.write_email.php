<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 * @author Marcin Korecki <marcin.korecki@m2system.pl>
 */


/**
 * Smarty {write_emails} function plugin
 *
 * Type:     function<br>
 * Name:     write_email<br>
 * Purpose:  separates user name and host from given email address
 * 					 and prints it out using JavaScript to concanate
 * 					 user name, @ and host
 * @param   array   $params           - function parameters
 * @param   object  $smarty           - reference to smarty object
 * @return  string                    - multiplied pattern
 */
  function smarty_function_write_email($params, &$smarty) {
    $sRetString 	= '';
    $aEmailParts	= array();
    if (empty($params['email'])) {
      return '';
    }
    $aEmailParts = explode('@', $params['email']);
    $sRetString='<script language="Javascript">user = "'.$aEmailParts[0].'";host = "'.$aEmailParts[1].'";document.write(\'<a href="mailto:\'+ user + \'@\' + host +\'"'.(!empty($params['class'])?'class="'.$params['class'].'"':'').'>'.(!empty($params['display'])?$params['display']:'\'+ user + \'@\' + host +\'').'</a>\');</script>';
    return $sRetString;
  }
?>