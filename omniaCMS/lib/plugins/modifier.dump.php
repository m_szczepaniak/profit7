<?php

/*
 * Smarty plugin
 * -------------------------------------------------------------
 * Type:     modifier
 * Name:     dump
 * Purpose:  dumps variable into popup
 * Added by: Grzegorz Świt 26.04.2010
 * -------------------------------------------------------------
 */
function smarty_modifier_dump($var)
{
	$sHtml = "<script language=\"javascript\" type=\"text/javascript\">
	nw=window.open('','dump','height=200,width=150');
	var tmp = nw.document;
	tmp.write('<html><head><title>dump</title>');
	tmp.write('</head><body><pre>');
	tmp.write('";
	$sHtml .= print_r($var,true);
	$sHtml .= "');
	tmp.write('</pre></body></html>');
	tmp.close();
</script>";
	return $sHtml;
}

?>
