<?php
class OrderRecount {
	function OrderRecount() {}

   /**
    * 
    * @global array $aConfig
    * @param int $iId
    * @return boolean
    */
	 public function sendOrderStatusMail($iId) {
		global $aConfig;
		$aMatches = array();
    
		$sWebsite = $this->getWebsiteSymbol($this->getWebsiteId($iId, false));
		
		setWebsiteSettings(1, $sWebsite);
    
    if (!isset($aConfig['lang']['m_zamowienia_mail'])) {
      include_once($aConfig['common']['base_path'].'/modules/m_zamowienia/lang/admin_pl.php');
    }
	 	$aModule['lang'] =& $aConfig['lang']['m_zamowienia_mail'];
	 	
	 	$_GET['lang_id']=1;
		$aSiteSettings = getSiteSettings($sWebsite);
	 	$sFrom = $aConfig['default']['website_name'];
		$sFromMail = $aSiteSettings['orders_email'];
		
		$sSql = "SELECT *, 
                    MD5(CONCAT(check_status_key, order_number)) as get_invoice_key,
                    (transport_cost + value_brutto) as total_value_brutto, 
                    DATE_FORMAT(order_date, '".$aConfig['common']['sql_date_hour_format']."') as order_date, 
                    DATE_FORMAT(status_1_update, '".$aConfig['common']['sql_date_hour_format']."') as status_1_update, 
                    DATE_FORMAT(status_2_update, '".$aConfig['common']['sql_date_hour_format']."') as status_2_update, 
                    DATE_FORMAT(status_3_update, '".$aConfig['common']['sql_date_hour_format']."') as status_3_update
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id = ".$iId;
		$aModule['order'] =& Common::GetRow($sSql);
		if($aModule['order']['order_status'] == '2') {
      return true;
    }
    
    $sMail = $aConfig['lang']['m_zamowienia']['mail_status_'.$aModule['order']['order_status']];
		if($aModule['order']['order_status'] == '4'){
			$sTMeansSymbol = $this->getTransportMeansSymbol($aModule['order']['transport_id']);
      switch ($sTMeansSymbol) {
          case 'odbior-osobisty':
            $sMail .= '_odb_osob';
          break;
          case 'paczkomaty_24_7':
            $sMail .= '_paczkomaty';
          break;
          case 'poczta_polska':
            $sMail .= '_poczta_pl';
          break;
          case 'opek-przesylka-kurierska':
            $sMail .= '_kurier';
          break;
		  case 'poczta-polska-doreczenie-pod-adres':
			  $sMail .= '_poczta_adres';
			  break;
          case 'ruch':
            $sMail .= '_ruch';
          break;
          case 'orlen':
            $sMail .= '_orlen';
            
            $sTransportNumber = $aModule['order']['transport_number'];
            preg_match("/^\d{4}(\d{13})/", $sTransportNumber, $aMatches);   
            $sTransportNumber = $aMatches[1];
            $aModule['order']['transport_number'] = $sTransportNumber;            
          break;
          case 'tba':
            $sMail .= '_tba';
          break;
      }
		} 
    
    $sFVLink = str_replace('http://', 'https://', $aConfig['common'][$sWebsite]['client_base_url_http_no_slash']).'/sprawdz-status-zamowienia/id'.$iId.',invoice.html?key='.$aModule['order']['get_invoice_key'];
    $aVars = array('{nr_zamowienia}','{data_statusu}','{list_przewozowy}', '{link_do_faktury}');
    $aValues = array($aModule['order']['order_number'], $aModule['order']['status_'.$aModule['order']['order_status'].'_update'], $aModule['order']['transport_number'], $sFVLink);
    
	 	if($aModule['order']['order_status'] == '2'){
			if(($aModule['order']['payment_status'] == '1' && $aModule['order']['paid_amount'] >= $aModule['order']['to_pay']) || $aModule['order']['payment_type'] == 'bank_14days' || $aModule['order']['payment_type'] == 'postal_fee'){
				$sMail .= '_oplacone';
			} else {
				$sMail .= '_nieoplacone';
			}
		}													 
		$aMail = Common::getWebsiteMail($sMail, $sWebsite);

         $attachments = [];
         if($aModule['order']['order_status'] == '3' && $sWebsite != 'np'){
             $attachments[] = [realpath($_SERVER['DOCUMENT_ROOT'].'/images/email_attachments/'.$sWebsite.'/Informacja.pdf')];
             $attachments[] = [realpath($_SERVER['DOCUMENT_ROOT'].'/images/email_attachments/'.$sWebsite.'/Zał.1 Odstąpienie od umowy.pdf')];
             $attachments[] = [realpath($_SERVER['DOCUMENT_ROOT'].'/images/email_attachments/'.$sWebsite.'/Zał.2 Reklamacja.pdf')];
         }

//    Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,'raporty@profit24.pl',  $aMail['content'],$aVars,$aValues,array(), array(), $sWebsite);
		return Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,$aModule['order']['email'],  $aMail['content'],$aVars,$aValues,array(), array(), $sWebsite, $attachments);
	} // end of sendUserPasswordEmail() method
  
  
	/**
	 * Metoda pobiera symbol dla metody transportu
	 *
	 * @global array $aConfig
	 * @param type $iTransportId
	 * @return type 
	 */
	function getTransportMeansSymbol($iTransportId) {
		global $aConfig;
		
		$sSql = "SELECT symbol FROM ".$aConfig['tabls']['prefix']."orders_transport_means
						 WHERE id=".$iTransportId;
		return Common::GetOne($sSql);
	}// end of () method
  
	 
	/**
	 * Metoda pobiera dane sprzedawcy
	 *
	 * @return	array ref	- tablica z danymi
	 */
	function getPaymentOrderDetailsA($iOId) {
		global $aConfig;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_seller_data";
		return  Common::GetRow($sSql);
	
	} // end of getSellerData() method
	
			/**
	 * Pobiera dane adresowe z zamówienai z bazy
	 * @param int $iId - id adresu
	 * @return array ref
	 */
	function &getOrderAddresses($iId){
		global $aConfig;

		$sSql = "SELECT address_type, is_company, company, name, surname, street, number, number2, postal, city, nip, phone
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
						 " ORDER BY address_type"; 
		return Common::GetAssoc($sSql);
	} // end of getOrderAddresses() method
	
		/**
	 * Metoda pobiera składowe zamówienia - nie pakiety
	 * @param int $iOrderId - id zamówienia
	 * @return array - składowe zamówienia
	 */
	function getOrderItems($iOrderId){
		global $aConfig;
			// pobranie zamowionych w ramach zamowienia towarow
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id = ".$iOrderId."
						 AND packet = '0'
						 ORDER BY id";
		return Common::GetAll($sSql);
	} // end of getOrderItems() method
	
		/**
	 * Pobiera dane zamówienia
	 * @param int $iOrderId - id zamowienia
	 * @return array
	 */
	function getOrderData($iOrderId){
		global $aConfig;
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id = ".$iOrderId;
		return Common::GetRow($sSql);
	} // end of getOrderData() method
	
	/**
	 * Sprawdza czy sposob transportu dla amówienia to odbiór osobisty
	 * @param int $iOrderId - id zamowienia
	 * @return bool
	 */
	function checkPersonalReception($iOrderId){
		global $aConfig;
		$sSql = "SELECT B.personal_reception
						FROM ".$aConfig['tabls']['prefix']."orders A
						LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						ON B.id=A.transport_id
						WHERE A.id = ".$iOrderId;
		return (Common::GetOne($sSql) == '1');
	}// end of checkPersonalReception() method
	
		/**
	 * Metoda pobiera pakiety zawarte w zamówieniu
	 * @param int $iOrderId - id zamówienia
	 * @return array - pakiety zawarte w zamówieniu
	 */
	function getOrderPackets($iOrderId){
		global $aConfig;
			// pobranie zamowionych w ramach zamowienia towarow
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id = ".$iOrderId."
						 AND packet = '1'
						 ORDER BY id";
		return Common::GetAll($sSql);
	} // end of getOrderPackets() method
	
		/**
	 * Pobiera wartość do zapłaty z zamówienia
	 * @param $iOrderId - id zamówienia
	 * @return float - wartośc do zapłaty
	 */
	function getOrderToPay($iOrderId){
		global $aConfig;
		$sSql = "SELECT to_pay
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id = ".$iOrderId;
		return Common::GetOne($sSql);
	} // end of getOrderToPay() method
	
	/**
	 * Metoda pobiera dane metody płatności
	 * @param $iPaymentId - id metody płatnosci
	 * @return array - dane metody platności
	 */
	function GetPaymentData($iPaymentId){
	global $aConfig;
	$aPtypes['ptype_postal_fee'] = "płatność przy odbiorze";
	$aPtypes['ptype_bank_transfer'] = "przelew - przedpłata na konto";
	$aPtypes['ptype_bank_14days'] = "przelew 14 dni";
	$aPtypes['ptype_dotpay'] = "karta płatnicza lub e-przelew";
	$aPtypes['ptype_vat'] = "faktura VAT";
	$aPtypes['ptype_self_collect'] = "odbiór osobisty";
	$aPtypes['ptype_platnoscipl'] = "przelew elektroniczny";
	$aPtypes['ptype_card'] = "karta kredytowa";
		$sSql = "SELECT A.id, A.ptype, A.cost, A.no_costs_from, B.id AS transport_id, B.name AS transport
				 		 FROM ".$aConfig['tabls']['prefix']."orders_payment_types A
				 		 JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
				 		 ON B.id=A.transport_mean
				 		 WHERE A.id = ".$iPaymentId;
		$aPayment=&Common::GetRow($sSql);
		$aPayment['payment_name']=$aPtypes['ptype_'.$aPayment['ptype']];
		return $aPayment;
	} // end of GetPaymentData() method

	
	/**
	 * Metoda zmienia statusy produktu na podstawie danych produktu
	 *
	 * @global array $aConfig
	 * @param int $iId
	 * @param bool $bSendMailAtt - czy może być wysłany email do klienta ze statusem zamówienia
   * @param bool $bChangeOrderStatus - czy status zam może zostać zmieniony
   * @param bool $bForceSendOrderStatusEmail - wymusza wysłanie emaila do klienta, niezależnie czy ten status na tym etapie został zmieniony: po sortowniku zawsze wysyłamy takiego emaila
	 * @return boolean 
	 */
function recountOrder($iId, $bSendMailAtt = true, $bChangeOrderStatus = true, $bForceSendOrderStatusEmail = false){
		global $aConfig;
		$bSendMail=false;
	$aOrderItems = $this->getOrderItems($iId);
	$aPackets = array();
	$fTotalValueNetto = 0;
	$fTotalValueBrutto = 0;
	$aOrder = $this->getOrderData($iId);
  if (!empty($aOrder['remarks'])) {
    if ($aOrder['remarks_read_1'] != '') {
      $bRemarksRead_1 = TRUE;
    } else {
      $bRemarksRead_1 = FALSE;
    }
  } else {
    $bRemarksRead_1 = TRUE;
  }
  
	$bAllNotNull = true; // czy czy wszystkie rozne od '---'
	$bExistNotNull = false; // czy jest pozycja rozna od '---'
	$bAllReady = true; // czy wszystkie "są"
	$bAllReadyNC = true; // czy wszystkie "są niepotwierdzone"
	$bAllWeight = true; // czy wszystkie produkty mają ustawioną wagę
	$bInvoice = false; // generuj fakturę, list przewozowy
	// modyfikacja pozycji zamówienia - zwykłych produktów i składowych pakietu
	foreach ( $aOrderItems as $iKey=>$aOItem ) {
		if($aOItem['deleted'] == '0') {
				if($aOItem['item_type'] == 'A'){
					$fTotalValueNetto += $aOItem['value_netto'];
					$fTotalValueBrutto += $aOItem['value_brutto'];
				} elseif($aOItem['item_type'] == 'I'){
					$fTotalValueNetto += $aOItem['value_netto'];
					$fTotalValueBrutto += $aOItem['value_brutto'];
					if($aOItem['status'] != '4'){
						$bAllReady = false;
					}
					if(intval($aOItem['status']) < 3){
						$bAllReadyNC = false;
					}
					if($aOItem['status'] == '0'){
						$bAllNotNull = false;
					} else {
						$bExistNotNull = true;
					}
					if($aOItem['weight'] == 0){
						$bAllWeight = false;
					}
				}
			
		}
	}
	// pakiety w zamówieniu
	$aOrderPackets = $this->getOrderPackets($iId);
	if(!empty($aOrderPackets)){
		foreach($aOrderPackets as $iKey=>$aPItem){
			if($aPItem['deleted'] == '0') {
					$fTotalValueNetto += $aPItem['value_netto'];
					$fTotalValueBrutto += $aPItem['value_brutto'];
					
					$sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aPItem['id'].' AND `status`<>\'4\'';
					$bPacketNotReady=Common::GetOne($sSql)>0?true:false;
					$sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aPItem['id'].' AND `status` IN (\'0\',\'1\',\'2\')';
					$bPacketNotReadyNC=Common::GetOne($sSql)>0?true:false;
					$sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aPItem['id'].' AND `status`=\'0\'';
					$bPacketExistNotNull=Common::GetOne($sSql)>0?true:false;
					$sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aPItem['id'].' AND `weight`=0.0';
					$bPacketNotAllWeight=Common::GetOne($sSql)>0?true:false;
					
					if($bPacketNotReady){
						$bAllReady = false;
					}
					if($bPacketNotReadyNC){
						$bAllReadyNC = false;
					}
					if($bPacketExistNotNull){
						$bAllNotNull = false;
					} else {
						$bExistNotNull = true;
					}
					if($bPacketNotAllWeight)
						$bAllWeight = false;
					
					/*if($aPItem['status'] != '4'){
						$bAllReady = false;
					}
					if(intval($aPItem['status']) < 3){
						$bAllReadyNC = false;
					}
					if($aPItem['status'] == '0'){
						$bAllNotNull = false;
					} else {
						$bExistNotNull = true;
					}
				if($aPItem['weight'] == 0){
					$bAllWeight = false;
				}*/
			}
		}
	}
	$aPayment = $this->GetPaymentData($aOrder['payment_id']);
	
		
		// blokowanie kosztów transportu - koszt z pola input
		if($aOrder['block_transport_cost'] == '1'){
			$fTransportCost = $aOrder['transport_cost'];
		} else {
		// przeliczanie kosztu na podstawie aktualnie wybranej formy płatności
			// zwolnienie z koztu transportu
			if($aPayment['no_costs_from'] > 0 && $fTotalValueBrutto > $aPayment['no_costs_from']){
				$fTransportCost = 0;
			} else {
			// normalny koszt transportu
				$fTransportCost = $aPayment['cost'];
			}
		}
		$aValues = array();
		/*
		// aktualizacja
		$aValues = array (
			'value_netto' => $fTotalValueNetto,
			'value_brutto' => $fTotalValueBrutto,
			'transport_cost' => $fTransportCost,
			'payment' => $aPayment['payment_name'],
			'payment_type' => $aPayment['ptype'],
			'payment_id' => $aPayment['id'],
			'transport' => $aPayment['transport'],
			'transport_id' => $aPayment['transport_id'],
			'is_modified' => '1',
			'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name']
		);
		*/
		$fBalanceDiff = ($aOrder['value_brutto'] + $aOrder['transport_cost']) - ($aValues['value_brutto'] + $aValues['transport_cost']);
		
		$aValues['order_status'] = $aOrder['order_status'];
		// zamówienie jest nowe i wszystkie produkty mają ustalony status różny od '---'
		// przestawiamy status zamowienia na 'w realizacji'
		//	dump($aOrder);
		//dump($bExistToOrder); die();
		if(intval($aOrder['order_status']) == 0 && $bAllNotNull){
			$aValues['order_status'] = '1';
			$aValues['status_1_update'] = 'NOW()';
			$aValues['status_1_update_by'] = $_SESSION['user']['name'];
			$bSendMail = true;
		}
		// zamówienie ma status niższy niż 'skompletowane' oraz wszystkie produkty sa ustawione na 'są'
		if(intval($aOrder['order_status']) < 2 && $bAllReady){
			// wszystkie produkty maja podana wagę
			if($bAllWeight){
				
					$aValues['order_status'] = '2';
					$aValues['status_2_update'] = 'NOW()';
					$aValues['status_2_update_by'] = $_SESSION['user']['name'];
					$bSendMail = true;
					// generowanie numeru faktury
					//$aValues['invoice_id'] = $this->getInvoiceNumber($aOrder['bookstore'],$aOrder['order_number']);
					$bInvoice = true; // generuj fakture, list
					
				//}
			} else {
				// brak wagi w conajmniej jednym produkcie
			//	$this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['no_weight_err']);
			}
		}
// zamówienie ma status 'skompletowane' oraz NIE wszystkie produkty sa ustawione na 'są'
		if(intval($aOrder['order_status']) == 2 && !$bAllReady){
			// cofamy status
			$aValues['order_status'] = '1';
			$aValues['status_2_update'] = '';
			$aValues['status_2_update_by'] = '';
		}
		
		//die;
		if ($bChangeOrderStatus == true) {
			if (Common::Update($aConfig['tabls']['prefix']."orders",
												$aValues,
												"id = ".$iId) === false) {
				return false;
			}
		}
		$bPersonalReception = $this->checkPersonalReception($iId);
		/*if($bInvoice){
			// jesli odbior osobisty, generujemy od razu fakturę
			if($bPersonalReception){
				if($this->setInvoiceReady($iId) === false){
					return false;
				}
			} else{
				// jeśli nie odbór odobisty, generowanie danych dla opek
				if($this->generateOpekData($iId)===false){
					return false;
				}
			}
		}*/

		
		/********** status wewnętrzny ***********************/
		$iInternalStatus = '0';
		if($aValues['order_status'] == '5'){
			$iInternalStatus = 'A'; // anulowane
		} 
		elseif($aValues['order_status'] == '4'){
			$iInternalStatus = '9'; // wysłane
		} 
		elseif($aValues['order_status'] == '3' && !$bPersonalReception && !empty($aOrder['invoice_date'])){
			$iInternalStatus = '8'; // paczki
		} 
    /**
     * Usunięcie  && !$bPersonalReception - zamówienia, 
     *  które nie posiadają faktury a są na odbiór osobisty także mają wpaść do zakładki faktury, 
     * aby została wygenerowana faktura
     * 
     *  @date 13.08.2013
     *  @mod arekgl0@op.pl
     */
    /**
     * Uwaga nic nie wpada do FV, link do FV wysyłany jest na maila
     * 
     * @TODO dopisać kod
     * @date 27.08.2014
     * @mod arekgl0@op.pl
     */
//		elseif($aValues['order_status'] == '3' || ($aOrder['order_status'] == '2' && $bPersonalReception === TRUE)){ // && !$bPersonalReception
//			//$iInternalStatus = '7'; // faktury
//		} 
		elseif($aValues['order_status'] == '3' && $bRemarksRead_1 && $bAllReady && $bAllWeight && $bPersonalReception && ($aOrder['payment_status'] == 1 || $aPayment['ptype'] == 'bank_14days' || $aPayment['ptype'] == 'postal_fee')) {
			$iInternalStatus = '6'; // odbór osobisty
		} 
		elseif($bRemarksRead_1 && $bAllReady  && $bAllWeight && ((($aOrder['payment_status'] == 0 || $aOrder['payment_status'] == 2 ) && $aPayment['ptype'] != 'bank_14days' && $aPayment['ptype'] != 'postal_fee') || ($aOrder['payment_status'] == 1 && $aOrder['paid_amount'] < $this->getOrderToPay($iId) && $aOrder['second_payment_type'] != 'postal_fee'))) {
			$iInternalStatus = '4'; // skompletowane
		}
		elseif($bRemarksRead_1 && $bAllReady && $bAllWeight && ($aOrder['payment_status'] == 1 || $aPayment['ptype'] == 'bank_14days' || $aPayment['ptype'] == 'postal_fee')) {
			$iInternalStatus = '5'; // wysyłka
		}
		elseif($bRemarksRead_1 && $bAllReadyNC) {
			$iInternalStatus = '3'; // do realizacji
		}
    /**
     * było dodane, i pozycje dwie normalnie odznaczone z pozostawionymi --- trafiły do częściowych
     */
		elseif($bRemarksRead_1 && $bExistNotNull && $bAllNotNull) {//elseif($bAllNotNull && $bAllNotNull) {// && $bExistNotNull) {//$bExistNotNull
			$iInternalStatus = '2'; // Czesciowe
		}
		elseif(($aPayment['ptype'] != 'platnoscipl' && $aPayment['ptype'] != 'card' && $aOrder['active'] != '0') || $aOrder['payment_status'] == 1 || !$bAllNotNull) {
			$iInternalStatus = '1'; // nowe
		}
    
		if($iInternalStatus != $aOrder['internal_status']){
			$aVal['internal_status'] = $iInternalStatus;
			if (Common::Update($aConfig['tabls']['prefix']."orders",
												 $aVal,
												 "id = ".$iId) === false) {
				return false;
			}
		}
		/********** /status wewnętrzny ***********************/
		if ($bSendMailAtt == true) {
			if($bSendMail || $bForceSendOrderStatusEmail === TRUE){
				$this->sendOrderStatusMail($iId);
      }
		}
		return true;
	} // end of recountOrder() method
	
	
	/**
	 * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type 
	 */
	function getWebsiteId($iOId) {
		global $aConfig;
		
		$sSql = "SELECT website_id FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id=".$iOId;
		return Common::GetOne($sSql);
	}// end of getWebsiteId() method
	
	
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type 
	 */
	function getWebsiteSymbol($iWebsiteId) {
		global $aConfig;
		
		$sSql = "SELECT code FROM ".$aConfig['tabls']['prefix']."websites
						 WHERE id=".$iWebsiteId;
		return Common::GetOne($sSql);
	}// end of getWebsiteSymbol() method
  
  
  
  
	/**
	 * Metoda przelicza wartośc zamówienia, koszty transportu po jakichkolwiek zamianach
   * 
	 * @param iId - id zamówienia
	 * @param iPaymentId - id metody płatności
	 * @return bool - success
	 */
	function recountOrderToRefactor($iId,$iPaymentId=0){
		global $aConfig;

    $aOrderItems = $this->getOrderItems($iId);
    $aPackets = array();
    $fTotalValueNetto = 0;
    $fTotalValueBrutto = 0;
    $aOrder = $this->getOrderData($iId);
    $bAllNotNull = true; // czy czy wszystkie rozne od '---'
    $bExistNotNull = false; // czy jest pozycja rozna od '---'
    $bAllReady = true; // czy wszystkie "są"
    $bAllReadyNC = true; // czy wszystkie "są niepotwierdzone"
    $bAllWeight = true; // czy wszystkie produkty mają ustawioną wagę
    $bSendMail = false; // wyslij mail o stanie zamowienia
    
    
    // modyfikacja pozycji zamówienia - zwykłych produktów i składowych pakietu
    $sWebsite = $this->getWebsiteSymbol($this->getWebsiteId($iId, false));
    include_once('FreeTransportPromo.class.php');
    $oFreeTransportPromo = new Lib_FreeTransportPromo($sWebsite);
    $aFreeTransport = array();

    /*
    $bMustChangePrice=false;//należy zmienic cene

    $aOldVals=$_POST['oldVals'];
    $aNewVals=$_POST['editable'];
    if (!empty($aOldVals) && is_array($aOldVals)) {
    foreach($aOldVals as $sIndex=>$aOld){
      if($sIndex!='transport') {
        //produkt
        if($aNewVals[$sIndex]['quantity']!=$aOld['quantity'] || $aNewVals[$sIndex]['discount']!=$aOld['discount'])
          $bMustChangePrice=true;
        }
      else														
        //transport
        if($aNewVals[$sIndex]['valuebrutto']!=$aOld['valuebrutto'])
          $bMustChangePrice=true;
      }
    }
    //jezeli dodajemy lub usowamy cos z zamowienia
    if($_GET['do']=='delete_item' || $_GET['do']=='add_product' || $_GET['do']=='undelete_item') 
      $bMustChangePrice=true;
    */



    /* LECIMY PO SKŁADOWYCH */
    //zawsze przeliczamy cenę
    $bMustChangePrice=true;	
    foreach ($aOrderItems as $iKey=>$aOItem){
      if ($aOItem['deleted'] == '0') {
        if (!empty($_POST['editable'][$aOItem['id']]) || 
            ($aOItem['item_type'] == 'A' && !empty($_POST)) || 
            (empty($_POST['editable']) && $_POST['do'] == 'connect_to' && $aOItem['item_type'] == 'P')){

          if ($_POST['editable'][$aOItem['id']]['status'] == 0 && $_POST['editable'][$aOItem['id']]['statusCur'] > 0) {
            $_POST['editable'][$aOItem['id']]['status'] = $_POST['editable'][$aOItem['id']]['statusCur'];
            $aOItem['status'] = $_POST['editable'][$aOItem['id']]['statusCur'];
          }

          if ($aOItem['item_type'] == 'A') {
            // jeśli jest to załącznik pobierz rabat z ojca
            $fDiscount = Common::formatPrice2($_POST['editable'][$aOItem['parent_id']]['discount']);
          } else {
            $fDiscount = Common::formatPrice2($_POST['editable'][$aOItem['id']]['discount']);
          }

          if (!isset($fDiscount)) {
            // jeśli nie zdefiniowane quantity należy pobrać wartość z bazy danych (w przypadku łączenia zamówień)
            $fDiscount = $aOItem['discount'];
          }

          $fDiscountCurrency = Common::formatPrice2($aOItem['price_brutto'] * ($fDiscount / 100));
          if ($aOItem['status'] == '4' && 
              intval($_POST['editable'][$aOItem['id']]['status']) < intval($aOItem['status']) && 
              isset($_POST['editable'][$aOItem['id']]['status'])){
            
            $sMsg = $aConfig['lang'][$this->sModule]['previous_book_status_err'];
            $this->sMsg .= GetMessage($sMsg);
            return false;
          }

          if ($aOItem['item_type'] == 'P'){
            // pakiet - ilość z rodzica
            $iQuantity = $_POST['editable'][$aOItem['parent_id']]['quantity'];
          } elseif ($aOItem['item_type'] == 'A') {
            // załącznik - ilość z rodzica
            $iQuantity = $_POST['editable'][$aOItem['parent_id']]['quantity'];
          } else {
            $iQuantity = intval($_POST['editable'][$aOItem['id']]['quantity']);
            if($_POST['editable'][$aOItem['id']]['status'] != '4'){
              $bAllReady = false;
            }
            if (intval($_POST['editable'][$aOItem['id']]['status']) < 3){
              $bAllReadyNC = false;
            }
            if ($_POST['editable'][$aOItem['id']]['status'] == 0){
              $bAllNotNull = false;
            } else {
              $bExistNotNull = true;
            }
          }

          if (!isset($iQuantity)) {
            // jeśli nie zdefiniowane quantity należy pobrać wartość z bazy danych (w przypadku łączenia zamówień)
            $iQuantity = $aOItem['quantity'];
          }

          if ($fDiscount > 0) {
            // jest ustawiony rabat

            include_once("OrderItemRecount.class.php");
            $oOrderItemRecount = new OrderItemRecount();

            $bHaveAttachments = false;
            // przeliczanie cen w przypadku kiedy książka ma załącznik
            $aAttachments = $this->getOrderItemAttachments($aOItem['id']);
            if (!empty($aAttachments) && is_array($aAttachments)) {
              // są załączniki
              $bHaveAttachments = true;

              $fBundlePromoPriceBrutto = $aOItem['bundle_price_brutto'] - Common::formatPrice2($aOItem['bundle_price_brutto'] * ($fDiscount / 100));//($aOItem['bundle_price_brutto']);
              $aPrices = $oOrderItemRecount->getAttProductOrderPrices($aAttachments, $aOItem['price_brutto'], $fBundlePromoPriceBrutto, $aOItem['vat'], $fDiscount);
              $fPromoBrutto = $aPrices['book']['promo_price_brutto'];//$aOItem['price_brutto'] - $fDiscountCurrency;
              $fPromoNetto = $aPrices['book']['promo_price_netto'];//($fPromoBrutto / (1 + $aOItem['vat']/100));//Common::formatPrice2
              $fValueNetto = $iQuantity * $fPromoNetto;
              $fValueBrutto = $iQuantity * $fPromoBrutto;
            } else {
              // brak załączników zwykłe przeliczanie
              $fPromoBrutto = $aOItem['price_brutto'] - $fDiscountCurrency;
              $fPromoNetto = ($fPromoBrutto / (1 + $aOItem['vat']/100));//Common::formatPrice2
              $fValueNetto = $iQuantity * $fPromoNetto;
              $fValueBrutto = $iQuantity * $fPromoBrutto;
            }
          } else {
            $fPromoBrutto = 0;
            $fPromoNetto = 0;
            $fValueNetto = $iQuantity * $aOItem['price_netto'];
            $fValueBrutto = $iQuantity * $aOItem['price_brutto'];
          }
          $aFreeTransportTMP = $oFreeTransportPromo->getBookFreeTransport($aOItem['product_id'], $fPromoBrutto);
          if (!empty($aFreeTransportTMP)) {
            $aFreeTransport = array_unique(array_merge($aFreeTransport, $aFreeTransportTMP));
          }

          $aValues = array(
            'discount' => $fDiscount,
            'discount_currency' => $fDiscountCurrency,
            'total_discount_currency' => Common::formatPrice2($fDiscountCurrency * $iQuantity),
            'quantity' => $iQuantity,
            'promo_price_netto' => Common::formatPrice2($fPromoNetto),
            'promo_price_brutto' => $fPromoBrutto,
            'value_netto' => Common::formatPrice2($fValueNetto),
            'value_brutto' => $fValueBrutto,
            'total_vat_currency' => Common::FormatPrice2($fValueBrutto - $fValueNetto),
            'status' => intval($_POST['editable'][$aOItem['id']]['status']),//tutaj zmiana na id parrenta
            'weight' => Common::formatPrice2($_POST['editable'][$aOItem['id']]['weight']),
            'source'=>($_POST['sourceSelect'.($aOItem['id'])])//tutaj zmiana na id parrenta
          );
          if ($bHaveAttachments === true && !empty($aPrices['bundle'])) {
            // jeśli posiada załączniki należy zmienić także value bundla
            $aValues['bundle_value_brutto'] = Common::formatPrice2($aPrices['bundle']['promo_price_brutto'] * $iQuantity);
            $aValues['bundle_value_netto'] = Common::formatPrice2($aPrices['bundle']['promo_price_netto'] * $iQuantity);
          }

          //jezeli nie msuimy przeliczac wartosci zamowienia
          if (!$bMustChangePrice) {
            $aValues = array(
              'status' => intval($_POST['editable'][$aOItem['id']]['status']),
              'weight' => Common::formatPrice2($_POST['editable'][$aOItem['id']]['weight']),
              'source' => ($_POST['sourceSelect'.$aOItem['id']])
              );
            }

          // usunięte z warunku A. Golba 18.08.2011
          // ($_POST['editable'][$aOItem['id']]['status']>2 && !in_array($aValues['source'],array(0,1,5,51,52))) || 

          if (($_POST['editable'][$aOItem['id']]['status'] < 3 && 
               $_POST['editable'][$aOItem['id']]['status'] > 0 && 
               in_array($aValues['source'],array(0,1,5,51,52)) && 
               $aValues['source'] !== null)
             ) {

            $aValues['source'] = null;
          }

          if ($aValues['source'] === null && $_POST['editable'][$aOItem['id']]['from_packet'] > 0) {
            $aValues['source'] = $_POST['sourceSelect'.$_POST['editable'][$aOItem['id']]['from_packet']];	
          }

          // usunięte A. Golba 27.09.2011 w związku z : Zgłoszenie #3, 01.09.2011
          if ( ($_POST['editable'][$aOItem['id']]['status'] == 1 || $_POST['editable'][$aOItem['id']]['status'] == 3) 
                && $aValues['source'] === null
            ) {

            $aValues['source'] = '0'; 
            $sMsg = $aConfig['lang'][$this->sModule]['no_selected_source'];
            $this->sMsg .= GetMessage($sMsg);
            return false;
          }
          if (Common::Update($aConfig['tabls']['prefix']."orders_items", $aValues, "id = ".$aOItem['id']) === false){
              return false;
          }

          // jeśli produkt wciaż istnieje w bazie
          // i nie jest to pakiet
          if ($this->existProduct($aOItem['product_id']) === true) {
            // aktualizacja wagi w produkcie
            if (floatval($aValues['weight']) > 0 && $aValues['weight'] != $this->getProductWeight($aOItem['product_id'])){
              $aPValues = array(
                'weight' => Common::formatPrice2($_POST['editable'][$aOItem['id']]['weight'])
              );
              if (Common::Update($aConfig['tabls']['prefix']."products", $aPValues, "id = ".$aOItem['product_id']) === false){
                  return false;					 
              }
            }
            // TODO: aktualizacja wagi w bazie poprzez DbMgr
          }
          if ($aValues['weight'] == 0 && $aOItem['item_type'] != 'A'){
            $bAllWeight = false;
          }
          // skłądowa pakietu - nie dodawaj wartości do zamówienia, dodaj do tablicy pakietu
          if ($aOItem['item_type'] == 'P'){
            if ($fDiscount == 0){
              $aPackets[$aOItem['parent_id']]['promo_price_netto'] += $aOItem['price_netto'];
              $aPackets[$aOItem['parent_id']]['promo_price_brutto'] += $aOItem['price_brutto'];
            } else {
              $aPackets[$aOItem['parent_id']]['promo_price_netto'] += $fPromoNetto;
              $aPackets[$aOItem['parent_id']]['promo_price_brutto'] += $fPromoBrutto;
            }
            $aPackets[$aOItem['parent_id']]['weight'] += Common::formatPrice2($_POST['editable'][$aOItem['id']]['weight']);
          } elseif ($aOItem['item_type'] == 'A') {
            $iPPOId = $this->getParentOrderItemId($aOItem['parent_id']);

            if ($fDiscount == 0){
              if ($iPPOId > 0) {
                // posiada ojca który jest pakietem
                $aPackets[$iPPOId]['promo_price_netto'] += $aOItem['price_netto'];
                $aPackets[$iPPOId]['promo_price_brutto'] += $aOItem['price_brutto'];
              } else {
                $fTotalValueNetto += $aOItem['value_netto'];
                $fTotalValueBrutto += $aOItem['value_brutto'];
              }
            } else {
              if ($iPPOId > 0) {
                // posiada ojca który jest pakietem
                $aPackets[$iPPOId]['promo_price_netto'] += $fPromoNetto;
                $aPackets[$iPPOId]['promo_price_brutto'] += $fPromoBrutto;
              } else {
                $fTotalValueNetto += $fValueNetto;
                $fTotalValueBrutto += $fValueBrutto;
              }
            }
          }
          // normalny produkt - dodanie do całkowitej wartości zamówienia 
          else {
            $fTotalValueNetto += $fValueNetto;
            $fTotalValueBrutto += $fValueBrutto;
          }

        } else {
          if ($aOItem['item_type'] == 'I'){
            if ($aOItem['bundle_value_netto'] > 0.00 && $aOItem['bundle_value_brutto'] > 0.00) {
              $fTotalValueNetto += $aOItem['bundle_value_netto'];
              $fTotalValueBrutto += $aOItem['bundle_value_brutto'];					
            } else {
              $fTotalValueNetto += $aOItem['value_netto'];
              $fTotalValueBrutto += $aOItem['value_brutto'];	
            }

            //sprawdzamy czy jest pakietem
          /*if($aOItem['packet']=='1') {
            $sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aOItem['id'].' AND `status`<>\'4\'';
            $bPacketNotReady=Common::GetOne($sSql)>0?false:true;
            $sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aOItem['id'].' AND `status` IN (\'0\',\'1\',\'2\')';
            $bPacketNotReadyNC=Common::GetOne($sSql)>0?false:true;
            $sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aOItem['id'].' AND `status`=\'0\'';
            $bPacketExistNotNull=Common::GetOne($sSql)>0?true:false;
            $sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aOItem['id'].' AND `weight`=0.0';
            $bPacketNotAllWeight=Common::GetOne($sSql)>0?true:false;
            }*/
            if ($aOItem['status'] != '4'){
              $bAllReady = false;
            }
            if (intval($aOItem['status']) < 3){
              $bAllReadyNC = false;
            }
            if ($aOItem['status'] == 0){
              $bAllNotNull = false;
            } else {
              $bExistNotNull = true;
            }
            if ($aOItem['weight'] == 0 && $aOItem['item_type'] != 'A'){
              $bAllWeight = false;
            }
          }
        }
      }
    }

    /* LECIMY PO SKŁADOWYCH */



    // pakiety w zamówieniu
    $aOrderPackets = $this->getOrderPackets($iId);
    if (!empty($aOrderPackets)){
      foreach ($aOrderPackets as $iKey => $aPItem){
        if ($aPItem['deleted'] == '0') {
          if (!empty($aPackets[$aPItem['id']])){
            $fDiscountCurrency = Common::formatPrice2($aPItem['price_brutto'] - $aPackets[$aPItem['id']]['promo_price_brutto']);
            if (isset($_POST['editable'][$aPItem['id']]['quantity'])) {
              $iQuantity = intval($_POST['editable'][$aPItem['id']]['quantity']);
            } else {
              // pobranie z bazy danych w przypadku kiedy łączymy zamówienia
              $iQuantity = $aPItem['quantity'];
            }
            if($aPItem['status'] == '4' && intval($_POST['editable'][$aPItem['id']]['status']) < intval($aPItem['status']) && isset($_POST['editable'][$aPItem['id']]['status'])){
              $sMsg = $aConfig['lang'][$this->sModule]['previous_book_status_err'];
              $this->sMsg .= GetMessage($sMsg);
              return false;
            }
            $sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aPItem['id'].' AND `status`<>\'4\'';
            $bPacketNotReady = Common::GetOne($sSql) > 0 ? true : false;
            $sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aPItem['id'].' AND `status` IN (\'0\',\'1\',\'2\')';
            $bPacketNotReadyNC = Common::GetOne($sSql) > 0 ? true : false;
            $sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aPItem['id'].' AND (`status`=\'0\' OR `status`=\'-1\')';
            $bPacketExistNotNull = Common::GetOne($sSql) > 0 ? true : false;
            $sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE item_type=\'P\' AND parent_id='.$aPItem['id'].' AND `weight`=0.0';
            $bPacketNotAllWeight = Common::GetOne($sSql) > 0 ? true : false;

            if ($bPacketNotReady){
              $bAllReady = false;
            }
            if ($bPacketNotReadyNC){
              $bAllReadyNC = false;
            }
            if ($bPacketExistNotNull){
              $bAllNotNull = false;
            } else {
              $bExistNotNull = true;
            }
            if ($bPacketNotAllWeight) {
              $bAllWeight = false;
            }

            /*
            if($_POST['editable'][$aPItem['id']]['status'] != '4'){
              $bAllReady = false;
            }
            if(intval($_POST['editable'][$aPItem['id']]['status']) < 3){
              $bAllReadyNC = false;
            }
            if($_POST['editable'][$aPItem['id']]['status'] == '0'){
              $bAllNotNull = false;
            } else {
              $bExistNotNull = true;
            }*/
            if ($fDiscountCurrency > 0){
              $fPromoBrutto = $aPackets[$aPItem['id']]['promo_price_brutto'];
              $fPromoNetto = $aPackets[$aPItem['id']]['promo_price_netto'];
              $fValueNetto = $iQuantity * $fPromoNetto;
              $fValueBrutto = $iQuantity * $fPromoBrutto;
            } else {
              $fPromoBrutto = 0;
              $fPromoNetto = 0;
              $fValueNetto = $iQuantity * $aPItem['price_netto'];
              $fValueBrutto = $iQuantity * $aPItem['price_brutto'];
            }
            $aValues = array(
              'discount_currency' => $fDiscountCurrency,
              'total_discount_currency' => Common::formatPrice2($fDiscountCurrency*$iQuantity),
              'quantity' => $iQuantity,
              'promo_price_netto' => Common::FormatPrice2($fPromoNetto),
              'promo_price_brutto' => Common::FormatPrice2($fPromoBrutto),
              'value_netto' => Common::FormatPrice2($fValueNetto),
              'value_brutto' => Common::FormatPrice2($fValueBrutto),
              'total_vat_currency' => Common::FormatPrice2($fValueBrutto - $fValueNetto),
              'status' => intval($_POST['editable'][$aPItem['id']]['status']),
              'weight' => $aPackets[$aPItem['id']]['weight'],
              'source' => ($_POST['sourceSelect'.($aPItem['id'])])//tutaj zmiana 
            );
            
            //jezeli nie msuimy przeliczac wartosci zamowienia
            if(!$bMustChangePrice) {
              $aValues = array(
                'status' => intval($_POST['editable'][$aPItem['id']]['status']),
                'weight' => $aPackets[$aPItem['id']]['weight']
                );
              }

            if (Common::Update($aConfig['tabls']['prefix']."orders_items", $aValues, "id = ".$aPItem['id']) === false){
                return false;		
            }
            if ($aValues['weight'] == 0){
              $bAllWeight = false;
            }
            $fTotalValueNetto += Common::FormatPrice2($fValueNetto);
            $fTotalValueBrutto += Common::FormatPrice2($fValueBrutto);
          } else {
            $fTotalValueNetto += Common::FormatPrice2($aPItem['value_netto']);
            $fTotalValueBrutto += Common::FormatPrice2($aPItem['value_brutto']);
            /*if($aPItem['status'] != '4'){
              $bAllReady = false;
            }
            if(intval($aPItem['status']) < 3){
              $bAllReadyNC = false;
            }
            if($aPItem['status'] == '0'){
              $bAllNotNull = false;
            } else {
              $bExistNotNull = true;
            }
            if($aPItem['weight'] == 0){
              $bAllWeight = false;
            }*/
          }
        }
      }
    }
	
		if ($fTotalValueBrutto > 0.00) {
			$aFreeTransportTMP = $oFreeTransportPromo->getBookFreeTransport('-1', $fTotalValueBrutto);
			if (!empty($aFreeTransportTMP)) {
				$aFreeTransport = array_unique(array_merge($aFreeTransport, $aFreeTransportTMP));
			}
		}
		
		if (!empty($_POST['transport_number'])) {
			$aPayment = $this->GetPaymentData($aOrder['payment_id'], $sWebsite);
		}
		elseif ($iPaymentId > 0) {
			$aPayment = $this->GetPaymentData($iPaymentId, $sWebsite);
		} else {
			$aPayment = $this->GetPaymentData(intval($_POST['payment_method']), $sWebsite);
		}
		
		if (!empty($_POST['transport_number'])){
			$fTransportCost = $aOrder['transport_cost'];
		} 
		elseif($iPaymentId > 0 && $aOrder['block_transport_cost'] == '1'){
			$fTransportCost = $aOrder['transport_cost'];
		}
		else {
			// blokowanie kosztów transportu - kozt z pola input
			if (isset($_POST['block_transport_cost'])){
				$fTransportCost = Common::formatPrice2($_POST['editable']['transport']['valuebrutto']);
			} else {
				// przeliczanie kosztu na podstawie aktualnie wybranej formy płatności
				// zwolnienie z koztu transportu
				if (!empty($aFreeTransport) && in_array($aPayment['symbol'], $aFreeTransport)) {
					$fTransportCost = 0;
				} else {
					// normalny koszt transportu
					$fTransportCost = $aPayment['cost'];
				}
			}
		}
		
		// aktualizacja
		$aValues = array (
			'value_netto' => Common::formatPrice2($fTotalValueNetto),
			'value_brutto' => Common::formatPrice2($fTotalValueBrutto),
			'transport_cost' => $fTransportCost,
			'payment' => $aPayment['payment_name'],
			'payment_type' => $aPayment['ptype'],
			'payment_id' => $aPayment['id'],
			'transport' => $aPayment['transport'],
			'transport_id' => $aPayment['transport_id'],
			'block_transport_cost' => isset($_POST['block_transport_cost'])?'1':'0',
			'is_modified' => '1',
			'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name']
		);
		//jezeli nie msuimy przeliczac wartosci zamowienia
		if (!$bMustChangePrice) {
			$aValues = array (
				'payment' => $aPayment['payment_name'],
				'payment_type' => $aPayment['ptype'],
				'payment_id' => $aPayment['id'],
				'transport' => $aPayment['transport'],
				'transport_id' => $aPayment['transport_id'],
				'block_transport_cost' => isset($_POST['block_transport_cost'])?'1':'0',
				'is_modified' => '1',
				'modified' => 'NOW()',
				'modified_by' => $_SESSION['user']['name']
			);
		}
		if ($_GET['do'] == 'undelete_item' || $_GET['do'] == 'delete_item') {
			unset($aValues['block_transport_cost']);
			unset($aValues['transport_cost']);
		}
		
		if ($aPayment['symbol'] == 'paczkomaty_24_7') {
			if (intval($_POST['transport_option_id']) > 0) {
				$aValues['transport_option_id'] = intval($_POST['transport_option_id']);
				$aValues['transport_option_symbol'] = $this->getTransportOptionSymbol(intval($_POST['transport_option_id']));
			} else {
				$aValues['transport_option_id'] = 'NULL';
				$aValues['transport_option_symbol'] = 'NULL';
			}
			// punkt odbioru zapisujemy tylko w przypadku kiedy opcja jest wybrana
			$aMatches = array();
			preg_match('/^\w{6}$/' , $_POST['point_of_receipt'], $aMatches);
			if ($_POST['point_of_receipt'] != '' && !empty($aMatches)) {
				$aValues['point_of_receipt'] = $_POST['point_of_receipt'];
			}
		}
		
		if (isset($_POST['second_payment_method'])) {
			$aSecondPayment = $this->GetPaymentData(intval($_POST['second_payment_method']), $sWebsite);
			$aValues['second_payment']=			$aSecondPayment['payment_name'];
			$aValues['second_payment_type']=$aSecondPayment['ptype'];
			$aValues['second_payment_id']=	$aSecondPayment['id'];
		}
				
    // $fBalanceDiff = ($aOrder['value_brutto'] + $aOrder['transport_cost']) - ($aValues['value_brutto'] + $aValues['transport_cost']);
		
		if (!empty($_POST['transport_number']) && intval($aOrder['order_status']) == 3){
			$aValues['transport_number'] = $_POST['transport_number'];
			//$aValues['invoice_ready'] = '1';
			// generowanie numeru faktury
			/*$aValues['invoice_id'] = $this->getInvoiceNumber($aOrder['bookstore'],$aOrder['order_number']);
			if($aOrder['second_invoice'] == '1'){
				$aValues['invoice2_id'] = $this->getInvoiceNumber($aOrder['bookstore'],$aOrder['order_number']);
			}*/
			if ($this->existOpekRequest($iId)){
				if ($this->deleteOpekRequest($iId)===false){
					return false;
				}
			}
		}
		// zamówienie jest nowe i wszystkie produkty mają ustalony status różny od '---'
		// przestawiamy status zamowienia na 'w realizacji'
		if (intval($aOrder['order_status']) == 0 && $bAllNotNull){
			$aValues['order_status'] = '1';
			$aOrder['order_status'] = '1';
			$aValues['status_1_update'] = 'NOW()';
			$aValues['status_1_update_by'] = $_SESSION['user']['name'];
			$bSendMail = true;
		}
		// zamówienie ma status niższy niż 'skompletowane' oraz wszystkie produkty sa ustawione na 'są'
		if (intval($aOrder['order_status']) < 2 && $bAllReady){
			// wszystkie produkty maja podana wagę
			if ($bAllWeight){
					$aValues['order_status'] = '2';
					$aOrder['order_status'] = '2';
					$aValues['status_2_update'] = 'NOW()';
					$aValues['status_2_update_by'] = $_SESSION['user']['name'];
					$bSendMail = true;
					
			} else {
				// brak wagi w conajmniej jednym produkcie
				$this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['no_weight_err']);
			}
		}
	// zamówienie ma status 'skompletowane' oraz NIE wszystkie produkty sa ustawione na 'są'
		if (intval($aOrder['order_status']) == 2 && !$bAllReady){
			// cofamy status
			$aValues['order_status'] = '1';
			$aOrder['order_status'] = '1';
			$aValues['status_2_update'] = '';
			$aValues['status_2_update_by'] = '';
		}
		if (Common::Update($aConfig['tabls']['prefix']."orders",
											 $aValues,
											 "id = ".$iId) === false) {
			return false;
		}
		
		// aktualizacja statusów także dla składowych zamówienia
		$aFirstPayment = array();
		$aFirstPayment['ptype'] = $aValues['second_payment_type'];
		$aFirstPayment['id'] = $aValues['second_payment_id'];
		if ($this->updatePaymentItems($aFirstPayment, $iId, true) === false) {
			return false;
		}
		$aSecondPayment = array();
		$aSecondPayment['ptype'] = $aValues['payment_type'];
		$aSecondPayment['id'] = $aValues['payment_id'];
		if ($this->updatePaymentItems($aSecondPayment, $iId, false) === false) {
			return false;
		}

		
    if (intval($aOrder['order_status']) >= 2) {
      $bAllWeight=1;
    }
		$bPersonalReception = $this->checkPersonalReception($iId);
		/********** status wewnętrzny ***********************/
		$iInternalStatus = '0';
		if ($aOrder['order_status'] == '5'){
			$iInternalStatus = 'A'; // anulowane
		} 
		elseif ($aOrder['order_status'] == '4'){
			$iInternalStatus = '9'; // wysłane
		} 
		elseif ($aOrder['order_status'] == '3' && !$bPersonalReception && !empty($aOrder['invoice_date'])){
			$iInternalStatus = '8'; // paczki
		} 
    /**
     * Usunięcie  && !$bPersonalReception - zamówienia, 
     *  które nie posiadają faktury a są na odbiór osobisty także mają wpaść do zakładki faktury, 
     * aby została wygenerowana faktura
     * 
     *  @date 13.08.2013
     *  @mod arekgl0@op.pl
     */
    /**
     * Uwaga nic nie wpada do FV, link do FV wysyłany jest na maila
     * 
     * @TODO dopisać kod
     * @date 27.08.2014
     * @mod arekgl0@op.pl
     */
		elseif ($aOrder['order_status'] == '3' || ($aOrder['order_status'] == '2' && $bPersonalReception === TRUE)){
			$iInternalStatus = '7'; // faktury
		} 
		elseif ($bAllReady && $bAllWeight && 
						(
							(($aOrder['payment_status'] == 0 || $aOrder['payment_status'] == 2) && $aValues['payment_type'] != 'bank_14days' && $aValues['payment_type'] != 'postal_fee') 
							|| 
							($aOrder['payment_status'] == 1 && ($aOrder['paid_amount'] < $this->getOrderToPay($iId) && $aValues['second_payment_type'] != 'postal_fee')))
					) {
			$iInternalStatus = '4'; // skompletowane
		}
		elseif ($bAllReady && $bAllWeight && $bPersonalReception && ($aOrder['payment_status'] == 1 || $aValues['payment_type'] == 'bank_14days' || $aValues['payment_type'] == 'postal_fee' || $aValues['second_payment_type'] == 'postal_fee')) {
			$iInternalStatus = '6'; // odbór osobisty
		} 	
		elseif ($bAllReady && $bAllWeight && ($aOrder['payment_status'] == 1 || $aValues['payment_type'] == 'bank_14days' || $aValues['payment_type'] == 'postal_fee' || $aValues['second_payment_type'] == 'postal_fee')) {//dodana opcja uzwgledniajaa mozlwiosc zmiany drugiej formy platnosci na pobranie
			$iInternalStatus = '5'; // wysyłka
		}
		elseif ($bAllReadyNC) {
			$iInternalStatus = '3'; // do realizacji
		}
		elseif ($bExistNotNull && $bAllNotNull) {
			$iInternalStatus = '2'; // Czesciowe
		}
		elseif (($aValues['payment_type'] != 'platnoscipl' && $aValues['payment_type'] != 'card' && $aOrder['active'] != '0') || $aOrder['payment_status'] == 1) {
			$iInternalStatus = '1'; // nowe
		}
		if ($iInternalStatus != $aOrder['internal_status']){
			$aVal['internal_status'] = $iInternalStatus;
			if (Common::Update($aConfig['tabls']['prefix']."orders",
												 $aVal,
												 "id = ".$iId) === false) {
												 dump($aVal);
				return false;
			}
		}
		/********** /status wewnętrzny ***********************/
		// wysylanie informacji o zmienie metody platnosci
		if (intval($_POST['payment_method']) != 0 && $_POST['payment_method'] != $aOrder['payment_id']){
			$this->sendOrderPaymentMail($iId);
		}
		if ($bSendMail){
			
			$this->sendOrderStatusMail($iId);
		}
		$fName = 'faktura_proforma'.str_replace("/","_",$aOrder['order_number']).'.pdf';
		$sFile = $aConfig['common']['client_base_path'].$aConfig['common']['pro_forma_invoice_dir'].$fName;
		if(file_exists($sFile)){
			@unlink($sFile);
		}
		///// TODO USUNAC
		//return false;
		return true;
	} // end of recountOrder() method
  
  
  /**
   * 
   * @global array $aConfig
   * @param int $iId
   * @return bool
   */
  private function sendOrderPaymentMail($iId) {
		global $aConfig;
		
		$sWebsite = $this->getWebsiteSymbol($this->getWebsiteId($iId, false));
		setWebsiteSettings(1, $sWebsite);
		
	 	$aModule['lang'] =& $aConfig['lang']['m_zamowienia_mail'];
	 	
	 	$_GET['lang_id']=1;
		$aSiteSettings = getSiteSettings($sWebsite);
	 	$sFrom = $aConfig['default']['website_name'];
		$sFromMail = $aSiteSettings['orders_email'];
		
		$sSql = "SELECT *, (transport_cost + value_brutto) as total_value_brutto, DATE_FORMAT(order_date, '".$aConfig['common']['sql_date_hour_format']."') as order_date, DATE_FORMAT(status_1_update, '".$aConfig['common']['sql_date_hour_format']."') as status_1_update, DATE_FORMAT(status_2_update, '".$aConfig['common']['sql_date_hour_format']."') as status_2_update, DATE_FORMAT(status_3_update, '".$aConfig['common']['sql_date_hour_format']."') as status_3_update
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id = ".$iId;
		$aModule['order'] =& Common::GetRow($sSql);
		$aVars = array('{nr_zamowienia}','{transport}','{platnosc}','{koszt_transportu}','{do_zaplaty}');
		$aValues = array($aModule['order']['order_number'],$aModule['order']['transport'],$aModule['order']['payment'],Common::formatPrice($aModule['order']['transport_cost']),Common::formatPrice($aModule['order']['to_pay']));

		$aMail = Common::getWebsiteMail('zmiana_platnosci', $sWebsite);
		return Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,$aModule['order']['email'],  $aMail['content'],$aVars,$aValues,array(), array(), $sWebsite);
	} // end of sendUserPasswordEmail() method
}
