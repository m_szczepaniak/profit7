<?php
class CodeDiscount implements InterfacePromotion {
  public function __construct() {}
  
  public function checkProductPromotion() {
  }
  
  
	/**
	 * Metoda sprawdza czy książka pasuje do kodu rabatowego w zamówieniu
	 *
	 * @param array $aDiscountCode
	 * @param integer $iProductId
	 * @return boolean 
	 */
	public function checkBookDiscount($aDiscountCode, $iProductId) {
    
    // jeśli brak 
    if (empty($aDiscountCode['books']) && empty($aDiscountCode['extra'])) {
      // brak ograniczeń
      return 1;
    } else {
      $aCodeExtraData = unserialize($aDiscountCode['extra']);
      if (empty($aCodeExtraData) && empty($aDiscountCode['books'])) {
        // brak ograniczeń na ksiażki oraz na dodatkowe elementy
        return 1;
      }
      
      if (!empty($aDiscountCode['books'])) {
        // ograniczenie ksiażki
        if (in_array($iProductId, $aDiscountCode['books'])) {
          return 3; // jest ograniczenie i jest książka
        }
      }
      
      if (!empty($aCodeExtraData)) {
        // ograniczenia dodatkowe
        if (!empty($aCodeExtraData['extra_categories'])) {
          // ograniczenie kategorii
          if ($this->_checkProductCategories($iProductId, $aCodeExtraData['extra_categories']) === true) {
            return 4;
          }
        }
        if (!empty($aCodeExtraData['series'])) {
          // ograniczenie serii wydawniczej
          if ($this->_checkProductSeries($iProductId, $aCodeExtraData['series']) === true) {
            return 5;
          }
        }
        if (!empty($aCodeExtraData['publishers'])) {
          // ograniczenie wydawcy
          if ($this->_checkProductPublishers($iProductId, $aCodeExtraData['publishers']) === true) {
            return 6;
          }
        }
      }
    }
    
		return -1; // brak książki jest ograniczenie
	}// end of checkBookDiscount() method
  
	
  /**
   * Metoda sprawdza czy produkt posiada przekazane dodatkowe kategorie
   * 
   * @param type $iProductId
   * @param type $aIds
   * @return type
   */
  private function _checkProductCategories($iProductId, $aIds) {
    
    $sSql = "SELECT product_id FROM products_extra_categories 
             WHERE product_id = ".$iProductId." AND page_id IN(".implode(',', $aIds).")
             LIMIT 1";
    return Common::GetOne($sSql) > 0;
  }// end of _checkProductCategories() method
  
  
  /**
   * Metoda sprawdza czy produkt posiada przekazane serie
   * 
   * @param type $iProductId
   * @param type $aIds
   * @return type
   */
  private function _checkProductSeries($iProductId, $aIds) {
    
    $sSql = "SELECT product_id FROM products_to_series
             WHERE product_id = ".$iProductId." AND series_id IN(".implode(',', $aIds).")
             LIMIT 1";
    return Common::GetOne($sSql) > 0;
  }// end of _checkProductCategories() method
  
  
  /**
   * Metoda sprawdza czy produkt posiada przekazane serie
   * 
   * @param type $iProductId
   * @param type $aIds
   * @return type
   */
  private function _checkProductPublishers($iProductId, $aIds) {
    
    $sSql = "SELECT id FROM products
             WHERE id = ".$iProductId." AND publisher_id IN(".implode(',', $aIds).")
             LIMIT 1";
    return Common::GetOne($sSql) > 0;
  }// end of _checkProductPublishers() method
  
  
  /**
   * Metoda dodaje promocję
   * 
   * @todo Dodać pełną obsługę rabatu
   * @param int $iId
   * @param float $fPromoDiscount
   * @param string $sPromoOption - przyjmuje: '', 'limited', 'additional_limited' 
   */
  public function addProductPromotion($iId, $fPromoDiscount, $sPromoOption) {
    global $aConfig;
    
    $aPromoData = array();
    $aPromoData['discount_message'] = $aConfig['lang']['discount']['code_discount'.($sPromoOption != '' ? '_'.$sPromoOption : $sPromoOption)];
    $aPromoData['discount'] = $fPromoDiscount;
    return $aPromoData;
  }// end of addProductPromotion() method
  
  public function deleteProductPromotion() {
    
  }
}
?>