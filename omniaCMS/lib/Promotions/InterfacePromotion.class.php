<?php
interface InterfacePromotion {
  public function checkProductPromotion();
  public function addProductPromotion($iId, $fPromoDiscount, $sPromoOption);
  public function deleteProductPromotion();
}
?>
