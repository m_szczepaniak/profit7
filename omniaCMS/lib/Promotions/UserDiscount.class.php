<?php
class UserDiscount implements InterfacePromotion {
  public function __construct() {}
  
  public function checkProductPromotion() {
  }
  public function addProductPromotion($iId, $fPromoDiscount, $sPromoOption) {
    global $aConfig;
    
    $aPromoData = array();
    $aPromoData['discount_message'] = $aConfig['lang']['discount']['code_discount'.($sPromoOption != '' ? '_'.$sPromoOption : $sPromoOption)];
    $aPromoData['discount'] = $fPromoDiscount;
    return $aPromoData;
  }
  public function deleteProductPromotion() {
    
  }
}
?>