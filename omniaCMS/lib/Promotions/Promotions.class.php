<?php
class Promotions {
  public $aItemPromotions;
  public $aPromotionsTypes = array(
      'user_discount', // rabat użytkownika
      'code_discount', // kod rabatowy (kod liczony od ceny katalogowej LUB jako suma rabatu produktu + kodu rabatowego od ceny katalogowej)
      'general_discount',// rabat ogólny/podstawowy produktu, najczęście ustawiony z rabatu ogólnego
      'promotion_discount', // oferta produktowa -> promocje
      'publishersseries_discount' //rabaty na wydawnictwa i serie wyd.
  );
  
  public $oPromotion;
  
  public function __construct() {
    $this->oPromotion = new stdClass();
    
    include_once('InterfacePromotion.class.php');
    // ładujemy klasy promocji
    
    foreach ($this->aPromotionsTypes as $sType) {
      $sClassName = $this->_classNameFormat($sType);
      include_once($sClassName.'.class.php');
      
      $aCImp = class_implements($sClassName);
      if (isset($aCImp['InterfacePromotion'])) {
        $this->oPromotion->$sClassName = new $sClassName();
      }
    }
  }
  
  public function checkProductPromotion($sPromotionType, $iProductId, $aProductData) {
    $sClassName = $this->_classNameFormat($sPromotionType);
    $this->oPromotion->$sClassName->checkProductPromotion($iProductId, $aProductData);
  }
  
  public function checkProductPromotions($iProductId, $aProductData) {
    
   foreach ($this->aPromotionsTypes as $sPromotionType) {
     $this->checkProductPromotion($sPromotionType, $iProductId, $aProductData);
   }
  }
  
  
  
  public function addProductPromotion($iId, $sPromotionType, $fPromoDiscount, $sPromoOption, $bPromoLimit = false, $sPromoName = '') {
    global $aConfig;
    
    $this->aItemPromotions[$iId][$sPromotionType] = array();
    $this->aItemPromotions[$iId][$sPromotionType]['discount_message'] = sprintf($aConfig['lang']['discount'][$sPromotionType.
                                                                            ($sPromoOption != '' ? '_'.$sPromoOption : $sPromoOption)][$bPromoLimit === true ? 'limited' : 'default'], $sPromoName);
    $this->aItemPromotions[$iId][$sPromotionType]['discount'] = $fPromoDiscount;
    
//    $sClassName = $this->_classNameFormat($sPromotionType);
//    $this->aItemPromotions[$sPromotionType] = $this->oPromotion->$sClassName->addProductPromotion($iId, $fPromoDiscount, $sPromoOption);
    // pełny komunikat
    $this->aItemPromotions[$iId][$sPromotionType]['message'] = sprintf($aConfig['lang']['discount']['info'], 
                                                                  str_replace('.', ',', round($fPromoDiscount, 2)),
                                                                  $this->aItemPromotions[$iId][$sPromotionType]['discount_message']);
  }
  
  
  
  public function setProductPromotion($iId, $sPromotionType, $fPromoDiscount, $sPromoOption, $bPromoLimit = false, $sPromoName = '') {
    global $aConfig;
    
    $this->aItemPromotions[$iId][$sPromotionType]['discount'] = $fPromoDiscount;
    $this->aItemPromotions[$iId][$sPromotionType]['discount_message'] = sprintf($aConfig['lang']['discount'][$sPromotionType.
                                                                              ($sPromoOption != '' ? '_'.$sPromoOption : $sPromoOption)][$bPromoLimit === true ? 'limited' : 'default'], $sPromoName);
    // pełny komunikat
    $this->aItemPromotions[$iId][$sPromotionType]['message'] = sprintf($aConfig['lang']['discount']['info'], 
                                                                  str_replace('.', ',', round($fPromoDiscount, 2)),
                                                                  $this->aItemPromotions[$iId][$sPromotionType]['discount_message']);
  }
  
  public function deleteProductPromotion($iId, $sPromotionType) {
    unset($this->aItemPromotions[$iId][$sPromotionType]);
  }
  
  
  public function clearProductPromotions($iId) {
    unset($this->aItemPromotions[$iId]);
  }
  
  public function clearAllProductsPromotions() {
    unset($this->aItemPromotions);
  }
  
  private function _classNameFormat($sStr) {
    $aArgs = explode('_', $sStr);
    foreach ($aArgs as $iKey => $sName) {
      $aArgs[$iKey] = ucwords($sName);
    }
    return implode('', $aArgs);
  }
  
}
?>
