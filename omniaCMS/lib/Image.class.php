<?php

/**
 * Klasa Image - przesylanie/resizowanie obrazkow
 *
 * @author Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version 1.0
 */

// ustawienie maxymalnego czasu wykonywania skryptu na 600 sekund
ini_set('max_execution_time', 600);

require_once('File.class.php');

class Image extends MyFile {
	
	// ustawienia - rozmiary zdjec
	var $aSettings;
	
	/**
	 * Konstruktor
	 * 
	 * @param		array	$sImgSrc				- tablica z danymi przesylanego pliku
	 * @param 	
	 * @return 	void
	 **/
	function Image()	{} // end of Image() method
	
	
	/**
	 * Metoda ustawia dane zdjecia
	 * 
	 * @param	array	$aImg	- ane zdjecia
	 * @return	void
	 */
	function setImage($sDir, &$aImg, &$aSettings)	{
		$this->SetFile($sDir, $aImg);
		$this->aSettings = $aSettings;
		
		if ($this->aSrc['ext'] == 'png') {
			$this->aSrc['type'] = 'image/png';
		}
		elseif($this->aSrc['ext'] == 'gif') {
			$this->aSrc['type'] = 'image/gif';
		}
		elseif($this->aSrc['ext'] == 'jpeg' || $this->aSrc['ext'] == 'jpg') {
			$this->aSrc['type'] = 'image/jpeg';
		}
		else {
			$this->aSrc = array();
		}
	} // end of setImage() method
	
	
	function proceedImage() {
		global $aConfig;
		
		if (in_array($this->aSrc['ext'], $aConfig['common']['img_extensions'])) {
			// operacje na obrazkach
		  	$iResult = $this->MoveResizedImageTo($this->sDir, $this->aSettings['small_size'], $this->aSettings['new_name'], '', (!empty($this->aSettings['new_name'])||$this->aSettings['not_unique']==true)?false:true);
			if ($iResult == 1) {
				// nazwa nowego pliku
				$sName = $this->aSrc['final_name'];
		  		// rozmiary oryginalnego pliku
		    	$aSrcSize = getimagesize($this->aSrc['tmp_name']);
		    	// rozmiary zmniejszonego obrazka
		    	$aInfo = getimagesize($this->sDir.'/'.$sName);
		    
			    if (isset($this->aSettings['thumb_size'])) {
			    	// docelowe rozmiary miniaturki
			    	$aThumbSize = explode('x', $this->aSettings['thumb_size']);
			    	// utworzenie miniaturki
			    	$iResult = $this->MoveResizedImageTo($this->sDir, $this->aSettings['thumb_size'], '__t_'.$sName, '');
				}
		    
			    if(isset($this->aSettings['big_size']) && $this->aSettings['big_size'] != $this->aSettings['small_size'] && ($aSrcSize[0] > intval(1.5 * $aInfo[0]) || $aSrcSize[1] > intval(1.5 * $aInfo[1]))) {
			      $iResult = $this->MoveResizedImageTo($this->sDir, $this->aSettings['big_size'], '__b_'.$sName);
			    }
			    // przywrcenie nazwy pliku
				$this->aSrc['final_name'] = $sName;
		  	}
		  	return $iResult;
		}
		return -5;
	}
  
  
  /**
   * Metoda przenosi przeslany plik do wskazanego katalogu zmieniajac
   * jego rozmiar do podanej szerokosci/wysokosci (w zaleznosci od orientacji
   * obrazka - pion, poziom)
   * 
   * Jezeli trzeci parametr '$bUniqueName' jest ustawiony na TRUE 
	 * zostanie wygenerowana unikalna nazwa pliku.
	 *
   * @param   string  $sDestDir     		- sciezka do katalogu docelowego,
   * 																			bez koncowego '/' lub '\'
   * @param		string	$sNewWidthHeight	- nowa szerokosc/wysokosc obrazka	
	 * @param		string	$sPrefix					- prefix dla nazwy pliku
   * @param 	bool    $bUniqueName  		- true: wygeneruj unikalna nazwe pliku, domyslnie false
   * @param   bool    $bOverwrite   		- true: nadpisz plik; false: nie nadpisuj zwroc blad
   * @param   integer $iMode        		- atrybuty dla nowo utworzonego pliku
   * @param		integer	$iQuality					- jakosc obrazka wynikowego jpg (0 - 100)
   * @return  integer               		- -4: podana sciezka nie wskazuje na katalog
   *                                  		-3: katalog nie jest zapisywalny
   *                                  		-2: plik o podanej nazwie juz istnieje
   *                                  		-1: proba przeniesienia do katalogu docelowego nie powiodla sie
   *                                   		 1: plik zostal przeniesiony do katalogu docelowego
   */
  function MoveResizedImageTo($sDestDir, $sNewWidthHeight, $sNewName='', $sPrefix='', $bUniqueName=false, $bOverwrite=false, $iMode=0644, $iQuality=90) {
  	$aImageSize = array();
  	$iNewWidth	= 0;
  	$iNewHeight	= 0;
		
		if (!empty($this->aSrc)) {
			if (is_dir($sDestDir)) {
				if (is_writable($sDestDir)) {
					if ($bUniqueName) {
						// generujemy unikalna nazwe dla pliku
						$sName = $this->_GenerateUniqueName().'.'.$this->aSrc['ext'];
					}
					elseif ($sNewName != '') {
						if (!strpos($sNewName, '.')) {
							// nowa nazwa zostala podana bez rozszerzenia, dodanie oryginalnego rozszerzenia
							$sNewName .= '.'.$this->aSrc['ext'];
						}
						$sName = $sNewName;
					}
					else {
						$sName = $sPrefix.$this->aSrc['name'];
					}
					if (!$bOverwrite && file_exists($sDestDir.'/'.$sName)) {
						return -2;
					}
					// od tego miejsca zaczyna sie kod odpowiedzialny za zmiane rozmiaru obrazka
					// pobranie rozmiaru obrazka
					$aImageSize = getimagesize($this->aSrc['tmp_name']);
					$aNewSize = explode('x', $sNewWidthHeight);

					if (!empty($sNewWidthHeight) && ($aImageSize[0] > $aNewSize[0] || $aImageSize[1] > $aNewSize[1])) {
						// okreslenie nowych rozmiarow
						if ($aImageSize[0] > $aImageSize[1]) {
							// szerokosc jest wieksza od wysokosci
							$iNewWidth =& $aNewSize[0];
							$iNewHeight = ceil(($iNewWidth * $aImageSize[1]) / $aImageSize[0]);
						}
						else {
							// wysokosc jest wieksza od szerokosci
							$iNewHeight =& $aNewSize[1];
							$iNewWidth = ceil(($iNewHeight * $aImageSize[0]) / $aImageSize[1]);
						}
						if ($iNewWidth > $aNewSize[0]) {
							// szerokosc z wyliczynocyh nowych rozmiarow obrazka jest wieksza niz
							// szerokosc maxymalna, dostosowanie szerokosci do szerokosci max.
							// przeliczenie na nowo wysokosci
							$iNewWidth = $aNewSize[0];
							$iNewHeight = ceil(($iNewWidth * $aImageSize[1]) / $aImageSize[0]);
						}
						if ($iNewHeight > $aNewSize[1]) {
							// wysokosc z wyliczynocyh nowych rozmiarow obrazka jest wieksza niz
							// wysokosc maxymalna, dostosowanie wysokosci do wysokosci max.
							// przeliczenie na nowo szerokosci
							$iNewHeight = $aNewSize[1];
							$iNewWidth = ceil(($iNewHeight * $aImageSize[0]) / $aImageSize[1]);
						}
						// utworzenie obrazka o nowych rozmiarach
						if ($this->aSrc['type'] == "image/pjpeg" ||
								$this->aSrc['type'] == "image/jpeg" ||
								$this->aSrc['type'] == "image/jpg") {
							$oHandle = imagecreatefromjpeg($this->aSrc['tmp_name']);
						}
						elseif ($this->aSrc['type'] == "image/png") {
							$oHandle = imagecreatefrompng($this->aSrc['tmp_name']);
						}
						elseif ($this->aSrc['type'] == "image/gif") {
							$oHandle = imagecreatefromgif($this->aSrc['tmp_name']);
						}
						else {
							return -1;
						}
						$oDstImg = imagecreatetruecolor($iNewWidth, $iNewHeight);
						if (!$oDstImg) {
							return -1;
						}
						
						// przezroczystosc
						if ($this->aSrc['type'] == "image/png") {
							// PNG
							imagealphablending($oDstImg, false);
				      $iColorTransparent = imagecolorallocatealpha($oDstImg, 0, 0, 0, 127);
				      imagefill($oDstImg, 0, 0, $iColorTransparent);
				      imagesavealpha($oDstImg, true);
						}
						elseif ($this->aSrc['type'] == "image/gif") {
							// GIF
							$iTranspInd = imagecolortransparent($oHandle);
							if ($iTranspInd >= 0) {
								$aTranspColor = imagecolorsforindex($oHandle, $iTranspInd);
				        $iTranspIndex = imagecolorallocate($oDstImg, $aTranspColor['red'], $aTranspColor['green'], $aTranspColor['blue']);
				        imagefill($oDstImg, 0, 0, $iTranspIndex);
				       	imagecolortransparent($oDstImg, $iTranspIndex);
							}
						}
						
						if (!imagecopyresampled($oDstImg, $oHandle, 0, 0, 0, 0, $iNewWidth, $iNewHeight, $aImageSize[0], $aImageSize[1])) {
							return -1;
						}
						imagedestroy($oHandle);
						if ($this->aSrc['type'] == "image/pjpeg" ||
								$this->aSrc['type'] == "image/jpeg" ||
								$this->aSrc['type'] == "image/jpg") {
							if (!imagejpeg($oDstImg, $sDestDir.'/'.$sName, $iQuality)) {
								return -1;
							}
						}
						elseif ($this->aSrc['type'] == "image/png") {
							if (!imagepng($oDstImg, $sDestDir.'/'.$sName)) {
								return -1;
							}
						}
						elseif ($this->aSrc['type'] == "image/gif") {
							if (!imagegif($oDstImg, $sDestDir.'/'.$sName)) {
								return -1;
							}
						}
						else {
							return -1;
						}
						@imagedestroy($oDstImg);
						@chmod($sDestDir.'/'.$sName, $iMode);
					}
					else {
						// zwykle przeniesienie pliku bez zmiany jego rozmiaru
						if (!@copy($this->aSrc['tmp_name'], $sDestDir.'/'.$sName)) {
							return -1;
						}
					}
					$this->aSrc['final_name'] = $sName;
					// zmieniamy atrybuty
					@chmod($sDestDir.'/'.$sName, $iMode);
					return 1;
				}
				else {
					return -3;
				}
			}
			else {
				return -4;
			}
		}
		return -1;
  } // end of MoveResizedImageTo() method
} // end of class Image
?>