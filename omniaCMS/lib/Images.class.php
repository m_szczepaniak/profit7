<?php

/**
 * Klasa Images - operacje na zdjeciach
 *
 * @author Marcin Korecki <korecki@omnia.pl>
 * @version 1.0
 */

// dolaczenie klasy Files
include_once('Files.class.php');

class Images extends Files {
	
	// konfiguracja - rozmiary zdjec
	var $aSettings;
	
	/**
	 * Konstruktor
	 * 
	 * @param		array	$sImgSrc				- tablica z danymi przesylanego pliku
	 * @param 	
	 * @return 	void
	 **/
	function Images($iPId, $iId, &$aSettings, $sDir, $sField, $sTable, $sItemTable='', $sItemField='', $iItemId=0)	{
		global $aConfig;
		
		parent::Files($iPId, $iId, $sDir, $sField, $sTable, $sItemTable, $sItemField, $iItemId);
		$this->aSettings =& $aSettings;
		$this->sBaseDir = $aConfig['common']['client_base_path'].
											$aConfig['common']['photo_dir'];
	} // end of Images() method
	
	
	function proceed($bDelete=true, $bUpdateAlign=true) {
		global $aConfig;
		
		if (!is_dir($this->sBaseDir.'/'.$this->sDir)) {
			// katalog docelowy nie istnieje - utworzenie go
			if (!$this->createDirectory()) return false;
		}
		if ($bUpdateAlign && $this->sItemTable != '') {
			// aktualizacja wyrowania zdjec
			if (!$this->updateImagesAlign()) {
				return false;
			}
		}
		if ($bDelete && !empty($_POST['delete_image'])) {
			foreach ($_POST['delete_image'] as $sKey => $mValue) {
				// rozdzielenie klucza
				$aKey = explode('_', $sKey);
				if (!$this->deleteImage($aKey[1])) return false;
				// wyczyszczenie tablicy z plikami dla usunietego pliku
				$_FILES['image']['name'][$sKey] = '';
			}
		}
		return $this->proceedImages();
	} // end of proceed() method
	
	
	/**
	 * Metoda usuwa zdjecie
	 * 
	 * @param	integer	$iId	- Id zdjecia
	 * @param	bool	$bDB	- true: rowniez z bazy danych; false: tylko z katalogu
	 * @return	bool	- true: usunieto; false: wystapil blad
	 */
	function deleteImage($iId, $bDB=true) {
		if (($aImgSrc =& $this->getImageSrc($iId)) !== false) {
			if (!$this->deleteFromHD($aImgSrc)) return false;
			if ($bDB) {
				return $this->deleteFromDB($iId);
			}
			return true;
		}
		return false;
	} // end of deleteImage() method
	
	
	/**
	 * Metoda pobiera i zwraca sciezke o zdjecia
	 * 
	 * @param	integer	$iId	- Id zdjecia
	 * @return	string	- sciezka do zdjecia
	 */
	function &getImageSrc($iId) {
		global $aConfig;
		
		if ($this->sItemField != '' || $this->sField != '') {
			$sWhere = " WHERE ".($this->sItemField != '' && $this->iItemId != 0 ? $this->sItemField." = ".$this->iItemId : $this->sField." = ".$this->iId)." AND
										id = ".$iId;
		}
		$sSql = "SELECT directory, photo
						 FROM ".$aConfig['tabls']['prefix'].$this->sTable." ".$sWhere;
						 
		return Common::GetRow($sSql);
	} // end of getImageSrc() method
	
	
	/**
	 * Metoda usuwa zdjecie z dysku plik o podanej sciezce
	 * 
	 * @param	array	$aImg	- dane zdjecia - katalog i nazwa
	 * @return	bool	- tru: usunieto; flase: nie udalo sie usunac
	 */
	function deleteFromHD(&$aImg) {
		$sDir = $this->sBaseDir;
		if (empty($aImg['photo'])) return false;
		
		if (!empty($aImg['directory'])) {
			$sDir .= '/'.$aImg['directory'];
		}
		if (file_exists($sDir.'/'.$aImg['photo'])) {
			if (!@unlink($sDir.'/'.$aImg['photo'])) return false;
		}
		if (file_exists($sDir.'/__t_'.$aImg['photo'])) {
			if (!@unlink($sDir.'/__t_'.$aImg['photo'])) return false;
		}
		if (file_exists($sDir.'/__b_'.$aImg['photo'])) {
			if (!@unlink($sDir.'/__b_'.$aImg['photo'])) return false;
		}
		return true;
	} // end of deleteFromHD() method
	

	/**
	 * Metoda aktualizuje informacje o wyrownaniu zdjec
	 * 
	 * @return	bool	true: zaktualizowano; false: wystapil blad
	 */
	function updateImagesAlign() {
		global $aConfig;
		
		$aValues = array(
			'img_align' => $_POST['img_align'],
			'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name'] 
		);
		
		if ($this->iItemId != 0) {
			$sWhere = $this->sField." = ".$this->iId." AND id = ".$this->iItemId;
		}
		else {
			$sWhere = "page_id = ".$this->iPId." AND id = ".$this->iId;
		}

		return Common::Update($aConfig['tabls']['prefix'].$this->sItemTable,
													$aValues,
													$sWhere) !== false;
	} // end of updateImagesAlign() method
	
	
	function proceedImages() {
		global $aConfig;
				
		// dolaczenie klasy obslugujacej operacje na zdjeciach
		include_once('Image.class.php');
		$oImage = new Image();

		foreach ($_POST['desc_image'] as $sKey => $sDesc) {
			$aValues = array();
			$bDelete = false;
			$sOldName = '';

			if (trim($_FILES['image']['name'][$sKey]) != '') {
				$aImg = array(
					'name' => $_FILES['image']['name'][$sKey],
					'type' => $_FILES['image']['type'][$sKey],
					'tmp_name' => $_FILES['image']['tmp_name'][$sKey],
					'size' => $_FILES['image']['size'][$sKey]
				);
				
				$oImage->setImage($this->sBaseDir.'/'.$this->sDir, $aImg, $this->aSettings);
				if (($iResult = $oImage->proceedImage()) == 1) {
					$aValues = array(
						'directory' => $this->sDir,
						'photo'	=> $oImage->GetFileProperty('final_name'),
						'mime' => $oImage->GetFileProperty('type'),
						'size' => $oImage->GetFileProperty('size'),
						'name' => $oImage->GetFileProperty('name')
					);
					$bDelete = true;
				}
				elseif ($iResult == -5) {
					continue;
				}
				else {
					return false;
				}
			}
			$aValues['description'] = trim($sDesc) != '' ? trim($sDesc) : 'NULL';
			$aValues['author'] = trim($_POST['author_image'][$sKey]) != '' ? trim($_POST['author_image'][$sKey]) : 'NULL';
						
			// rozdzielenie klucza
			$aKey = explode('_', $sKey);
			
			if (intval($aKey[1]) > 0) {
				if ($bDelete) {
					// pobranie nazwy starego pliku
					$sOldName = $this->getImageSrc($aKey[1]);
				}
				if ($this->sItemField != '' || $this->sField != '') {
					$sWhere = ($this->sItemField != '' && $this->iItemId != 0 ? $this->sItemField." = ".$this->iItemId : $this->sField." = ".$this->iId);
				} else {
					$sWhere = ' 1=1 ';
				}
				// aktualizacja zdjecia
				$aValues['order_by'] = $_POST['order_image'][$sKey];
				if (Common::Update($aConfig['tabls']['prefix'].$this->sTable,
												 	 $aValues,
												 	 $sWhere." AND id = ".$aKey[1]) === false) {
					return false;
				}
				// fizyczne usuniecie zdjecia z dysku
				if ($bDelete && $sOldName !== false) {
					if (!$this->deleteFromHD($sOldName)) return 0;
				}
			}
			elseif (isset($aValues['photo'])) {
				// dodawanie nowego zdjecia
				if ($this->sItemField != '' || $this->sField != '') {
					if ($this->sItemField != '' && $this->iItemId != 0) {
						$aValues = array_merge(
							array(
								$this->sItemField => $this->iItemId,
								'order_by' => $this->getMaxOrderBy() + 1
							),
							$aValues
						);
					}
					else {
						$aValues = array_merge(
							array(
								$this->sField => $this->iId,
								'order_by' => $this->getMaxOrderBy() + 1
							),
							$aValues
						);
					}
				}

				if (Common::Insert($aConfig['tabls']['prefix'].$this->sTable,
												 	 $aValues,
												 	 "",
												 	 false) === false) {
					return false;
				}
			}
		}
		return true;
	} // end of proceedImages() method
} // end of class Image
?>