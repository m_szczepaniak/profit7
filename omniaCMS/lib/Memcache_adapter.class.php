<?php
/**
 * Klasa adaptera ze statego Cache_Lite do nowego Memcached
 * 
 * @var memcached $oMemcachedObj
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @version 1.0
 * @since 12.12.2012
 */
class Memcache_adapter_Cache_Lite {
  
  private $oMemcachedObj;
  private $iBookstoreId;
  public function __construct($oMemcachedObj, $bUseMemcached) {
    global $aConfig;
    
    if ($bUseMemcached === true) {
      if (empty($oMemcachedObj) || !is_object($oMemcachedObj)) {
        $oMemcachedObj = new Memcached();
        $oMemcachedObj->addServer($aConfig['memcached']['host'], $aConfig['memcached']['port'], 1);
      }
      $this->oMemcachedObj =& $oMemcachedObj;
    } else {
      // gość jest zalogowany i ma rabat
      $this->oMemcachedObj = false;
    }
    $this->iBookstoreId = $aConfig[$aConfig['common']['bookstore_code'].'_website_id'];
  }
  
  public function save($sValue, $sKey, $sGroup = '') {
    
    $sNewKey = 'client_'.$this->iBookstoreId.'_'.$sGroup.$sKey;
    if (is_object($this->oMemcachedObj)) {
      return $this->oMemcachedObj->set($sNewKey, $sValue);
    } else {
      return false;
    }
  }
  
  public function get($sKey, $sGroup = '') {
    
    $sNewKey = 'client_'.$this->iBookstoreId.'_'.$sGroup.$sKey;
    if (is_object($this->oMemcachedObj)) {
      return $this->oMemcachedObj->get($sNewKey);
    } else {
      return false;
    }
  }
  
  public function clean() {
    $this->oMemcachedObj->flush();
    return true;
    /*
    if (is_object($this->oMemcachedObj)) {
      $aKeys = $this->oMemcachedObj->getAllKeys();
      // usuwamy tylko cache kliencki
      foreach ($aKeys as $iKey => $sKey) {
        if (!preg_match('/^(url_'.$this->iBookstoreId.'_|client_'.$this->iBookstoreId.'_)/', $sKey)) {
          unset($aKeys[$iKey]);
        }
      }
      $iCountPacket = 1000;
      $iIterations = ceil(count($aKeys) / $iCountPacket);

      $i = 0;
      while ($i <= $iIterations) {
        $aSliceKeys = array_slice($aKeys, ($i*$iCountPacket), $iCountPacket);

        $this->oMemcachedObj->deleteMulti($aSliceKeys);
        sleep(1);
        $i++;
      }
      return true;
    } else {
      return false;
    }
     */
  }
}
?>
