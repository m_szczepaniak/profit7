<?php
/**
 * Klasa CPager do stronicowania
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie klasy Pager
include_once('Pager/Pager.php');

class CPager {
  
  // obiekt pagera
  var $oPager;
  
  // linki pagera
  var $aLinks;
  
  // konfiguracja pagera
  var $aParams;
  
	/** Konstruktor klasy CPager
   *
	 * @return	void
   */
	function CPager($iRowCount, $aParams=array()) {
		global $aConfig;
		//$aParams['external_class'] = 'pagerLinksNoPages';
		// tablica konfiguracyjna dla Pagera
		$this->aParams = array(
			'totalItems'	=> $iRowCount,
			'perPage'	=> $aConfig['default']['per_page'],
			'delta'	=> 3,
			'mode'	=> 'Sliding',
			'urlVar'	=> 'p',
			'altPage'	=> $aConfig['lang']['pager']['page'],
			'altFirst'=> $aConfig['lang']['pager']['first_page'],
			'altPrev'	=> $aConfig['lang']['pager']['previous_page'],
			'altNext'	=> $aConfig['lang']['pager']['next_page'],
			'altLast'	=> $aConfig['lang']['pager']['last_page'],
			'firstPagePre'	=> '',
			'firstPageText'	=> '<span class="pierwsza">pierwsza</span>',
			'firstPagePost'	=> '',
			'lastPagePre'	=> '',
			'lastPageText'	=> '<span class="ostatnia">ostatnia</span>',
			'lastPagePost'	=> '',
			'prevImg'	=> '<span class="poprzednia">poprzednia</span>',
			'nextImg'	=> '<span class="nastepna">następna</span>',
			'separator'	=> '',
			'spacesBeforeSeparator'	=> 1,
			'spacesAfterSeparator'	=> 1,
			'linkClass'	=> 'pagerLink',
			'curPageLinkClassName'	=> 'selected'
		);
		$aParams['excludeVars'][] = 'lang_id';
		//$aParams['excludeVars'][] = 'external_class';
		foreach ($aParams as $sParam => $mValue) {
			$this->aParams[$sParam] = $mValue;
		}
		// aktualna strona jest okreslana na podstawie $_GET[$aPagerParams['urlVar']]
		$this->aParams['currentPage'] = isset($_GET[$this->aParams['urlVar']]) && !empty($_GET[$this->aParams['urlVar']]) ? $_GET[$this->aParams['urlVar']] : 1;

		$this->oPager = Pager::factory($this->aParams);
		$this->aLinks = $this->oPager->getLinks();
		$this->aLinks['total_pages'] = $this->oPager->numPages();
		$this->aLinks['total_items'] = $this->oPager->numItems();
		$this->aLinks['current_page'] = $this->oPager->getCurrentPageID(); 
//		if(!empty($aParams['external_class'])){
//			if(!empty($this->aLinks['back'])) {
//				$this->aLinks['back'] = '<span class="'.$aParams['external_class'].'">'.$this->aLinks['back'].'</span>';
//			}
//			if(!empty($this->aLinks['next'])) {
//				$this->aLinks['next'] = '<span class="'.$aParams['external_class'].'">'.$this->aLinks['next'].'</span>';
//			}
//			if(!empty($this->aLinks['first'])) {
//				$this->aLinks['first'] = '<span class="'.$aParams['external_class'].'">'.$this->aLinks['first'].'</span>';
//			}
//			if(!empty($this->aLinks['last'])) {
//				$this->aLinks['last'] = '<span class="'.$aParams['external_class'].'">'.$this->aLinks['last'].'</span>';
//			}
//		}
	} // end of AddPager() function
	
	
	/**
	 * Metoda zwraca numer rekordu od ktorego beda wybierane wyniki
	 */
	function getStartFrom() {
		return ($this->oPager->GetCurrentPageID() - 1) * $this->aParams['perPage'];
	} // end of getStartFrom() method
	
	
	/**
	 * Metoda zwraca linki pagera
	 */
	function &getLinks($sWhat='') {
		if ($sWhat != '') {
			return $this->aLinks[$sWhat];
		}
		return $this->aLinks;
	} // end of getStartFrom() method
} // end of Main Class
?>