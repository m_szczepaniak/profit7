<?php
/**
* Klasa Logowania
*
* @author Marcin Korecki <korecki@pnet.pl>
* @copyright 2005
* @version   1.0
*/

class Login extends Common {
	
	// zmienna na komunikat bledu
	var $sErrMsg;
	CONST LENGTH_CODE_PASSWD = 6;
  CONST LENGTH_VERYFICATION_CODE_PASSWD = 4;
  
	/**
  * Konstruktor klasy Login
	*/
	function Login() {
		$this->sErrMsg = '';
	} // end Login()
	
	
	/** 
	* Metoda sprawdza czy jest zalogowany uzytkownik
	* @return boolean
	*/
	function IsLoggedIn() {
		global $aConfig;

		if (isset($_COOKIE['check']) && isset($_COOKIE['usid'])) {
			// istnieje ciasteczko z kluczem
			// sprawzenie czy istnieja dane sesji
			if (isset($_SESSION['check']) &&
					isset($_SESSION['user']) && !empty($_SESSION['user'])) {
				// istnieja dane sesji - sprawdzenie zgodnosci klucza po stronie
				// sesji i ciasteczka
				return $_SESSION['check'] == $_COOKIE['check'];
			}
			// dane sesji nie istnieja - sesja wygasla
			// dodanie informacji o wygasnieciu sesji
      $sTimeLogout = NULL;
      if (isset($_SESSION['logout_timeout'])) {
        $today = getdate($_SESSION['logout_timeout']);
        $sTimeLogout = $today['year'].'-'.$today['mon'].'-'.$today['mday'].' '.$today['hours'].':'.$today['minutes'].':'.$today['seconds'];
      }
			$aValues = array(
				'logout_level' => '2',
				'session_expiry_date' => ($sTimeLogout !== NULL ? $sTimeLogout : 'NULL')
			);
      
			$this->Update($aConfig['tabls']['prefix']."login_history",
										$aValues,
										"MD5(CONCAT(user_id, ':', session_id)) = ".Common::Quote($_COOKIE['usid']));
			// usuniecie ciasteczek i sesji
	    DeleteSession();
      session_start();
      header('Location: index.php');
      die();
      
		}
		return false;
	} // end of IsLoggedIn()
	
	
	/**
	 * Metoda zwraca formularz logowania
	 * Wykorzystuje obiekt typu FormSmarty
	 *
	 * @return	void
	 */
	function ShowLoginForm(&$pSmarty) {
	 	global $aConfig;
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
	 	
	 	$aAttribs 		= array();
	 	$iLangId	= 0;
	 	
	 	if ($aConfig['browser']['compatible'] == '0') {
			$pSmarty->assign_by_ref('sMsg', $aConfig['lang']['login']['incompatible']);
			$aAttribs = array('disabled' => 1);
		}
		else {
			$bDisableForm = false;
		}

	 	$pForm = new FormTable('loginForm', '', array('action'=>'index.php'), array('col_width'=>100, 'class'=>'loginTable'), true, array('class'=>'otherRows'), array('class'=>'otherRows'));
		$pForm->AddHidden('do', 'login');		
		$pForm->AddText('login', $aConfig['lang']['login']['login'], $_POST['login'], array_merge($aAttribs, array('maxlength'=>16)), '', 'text', true, '', array(), array('class'=>'firstRow'), array('class'=>'firstRow'));
		$pForm->AddPassword('passwd', $aConfig['lang']['login']['passwd'], '', array_merge($aAttribs, array()), '', false);

		// wybranie z bazy wszystkich dostepnych dla tej kopii CMSa wersji jez.
	 	$sSQL = "SELECT id AS value, symbol AS label, default_lang
	 					 FROM ".$aConfig['tabls']['prefix']."languages
	 					 ORDER BY default_lang DESC, symbol";
	 	$aLangs =& $this->GetAll($sSQL);
	 	foreach ($aLangs as $iKey => $aLang) {
			if ($aLang['default_lang'] == '1') {
				$iLangId = $aLang['value']; 
			}
			unset($aLangs[$iKey]['default_lang']);
	 	}
		if (isset($_COOKIE['lang'])) {
	 		$iLangId = $_COOKIE['lang'];
	 	}
	 	if (count($aLangs) > 1) {
	 		array_unshift($aLangs, array('value'=>0, 'name'=>$aConfig['lang']['common']['choose']));
	 		$pForm->AddSelect('lang', $aConfig['lang']['login']['language'], array(), $aLangs, $iLangId);
	 	}
	 	else {
	 		$pForm->AddHidden('lang', $aLangs[0]['value']);
	 	}
	 		 	
	 	$pForm->AddInputButton('send', $aConfig['lang']['login']['do_login'], array_merge($aAttribs, array('class'=>'button', 'id'=>'do_login')), 'submit', array('class'=>'lastRow'), array('class'=>'lastRow'));
	 	
	 	$pSmarty->assign_by_ref('sForm', $pForm->ShowForm());
		$pSmarty->assign_by_ref('sContent', $pSmarty->fetch('login.tpl'));
		$pSmarty->assign('sBodyOnload', 'FocusFormField(document.forms.loginForm, \'login\');');
	} // end of GetLoginForm()
	
  /**
   * 
   * @return boolean
   */
  private function checkLoginCode() {
    
    if ($_POST['passwd'] != '') {
      return false;
    }
    
    // sprawdzamy czy o konto o podanym loginie istnieje
	 	$sSQL = "SELECT id, priv_order_5_status
             FROM users
             WHERE login=".Common::Quote($_POST['login']);
    $aData = Common::GetRow($sSQL);
    if (isset($aData['priv_order_5_status']) && $aData['priv_order_5_status'] == '1') {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   */
  private function showCodeLoginForm($pSmarty) {
    
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
	 	$pForm = new FormTable('loginForm', '', array('action'=>'index.php'), array('col_width'=>100, 'class'=>'loginTable'), false, array('class'=>'otherRows'), array('class'=>'otherRows'));
		$pForm->AddHidden('do', 'loginCode');		
    $pForm->AddHidden('login', $_POST['login']);		
    
    $aInputs = $this->getCodeName($pForm);
    $pForm->AddMergedRow(_('Wprowadź kod logowania'));
    $pForm->AddMergedRow(implode(' &nbsp; ', $aInputs));
//    $pForm->AddInputButton('save', 'save', array('style' => 'display:none;'));
    
    $sJS = $this->getJSNextElementOnEnter();
	 	$pSmarty->assign_by_ref('sForm', $pForm->ShowForm());
		$pSmarty->assign('sContent', $pSmarty->fetch('login.tpl').$sJS);
  }
  
  /**
   * 
   * @return string
   */
  private function getJSNextElementOnEnter() {
    
    $sJS = '
      <script type="text/javascript">
        $(".tabNext").on("keydown", function( event ) {
          if ( event.which == 13 ) {
            $(this).nextAll(".tabNext:not(.tabDisabled):first").focus();
            if ($(this).nextAll(".tabNext:not(.tabDisabled):first").val() === undefined) {
              $("#loginForm").submit();
            }
          }
        });
      </script>
      ';
    return $sJS;
  }
  
  /**
   * 
   * @param FormTable $pForm
   * @return array
   */
  private function getCodeName(FormTable &$pForm) {
    $aInputs = array();
    $aRandInt = $this->getRandomArray();
    $iTabIndex = 1;
    for ($i = 1; $i <= self::LENGTH_CODE_PASSWD; $i++) {
      $bRequired = false;
      $aAttributes = $this->getItemAttribsRef($iTabIndex, $bRequired, $i, $aRandInt);
      $aInputs[] = $pForm->GetTextHTML('code['.$i.']', $i, '', $aAttributes, '', 'unit', $bRequired, '/\d/');
    }
    $_SESSION['verification_index_code'] = $aRandInt;
    return $aInputs;
  }
  
  /**
   * 
   * @param int $iTabIndex
   * @param bool $bRequired
   * @param array $aRandInt
   * @return int
   */
  private function getItemAttribsRef(&$iTabIndex, &$bRequired, $i, $aRandInt) {
    
    if (in_array($i, $aRandInt)) {
      $bRequired = true;
      $aAttributes = array('maxlength'=> 1, 'class' => 'tabNext', 'style' => 'font-size: 1.5em; width:25px; height: 30px; ', 'tabindex' => $iTabIndex);
      if ($iTabIndex === 1) {
        $aAttributes['autofocus'] = 'autofocus';
      }
      $iTabIndex++;
    } else {
      $bRequired = false;
      $aAttributes = array('maxlength'=> 1, 'class' => 'tabDisabled', 'style' => 'font-size: 1.5em; width:25px; height: 30px; background: grey;', 'readonly' => 'readonly');
    }
    return $aAttributes;
  }
  
  /**
   * 
   * @return array
   */
  private function getRandomArray() {
    
    $aRandInt = [];
    
    while (count($aRandInt) < self::LENGTH_VERYFICATION_CODE_PASSWD) {
      $aRandInt[] = rand(1, self::LENGTH_CODE_PASSWD);
      $aRandInt = array_unique($aRandInt);
    }
    sort($aRandInt);
    return $aRandInt;
  }
  
  /**
   * 
   * @return boolean
   */
  public function DoLoginCode() {
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sErrMsg = $oValidator->GetErrorString();
			return false; 
		}
    $iUId = $this->validateCodeLogin($_POST['code'], $_POST['login']);
    if ($iUId > 0) {
      $_POST['lang'] = 1; // a jak inaczej ;p
      return $this->doLoginUser($_POST['login'], $_POST['lang'], $iUId);
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param array $aCodes
   * @param string $sLogin
   * @return mixed
   */
  private function validateCodeLogin($aCodes, $sLogin) {
    
    $sSql = 'SELECT id, code_passwd, ip_address
             FROM users 
             WHERE login = '.Common::Quote($sLogin) .' AND priv_order_5_status = "1" ';
    $aUserData = Common::GetRow($sSql);
    if ($aUserData['ip_address'] != '') {
      $aIPs = explode(',', $aUserData['ip_address']);
      if (!in_array($_SERVER['REMOTE_ADDR'], $aIPs)) {
        $this->sErrMsg = _('Nie można zalogować się z podanego adresu IP');
        return false;
      }
    }
    
    if ($this->validateCode($aCodes, $aUserData['code_passwd']) === true) {
      unset($_SESSION['verification_index_code']);
      return $aUserData['id'];
    } else {
      unset($_SESSION['verification_index_code']);
			$this->sErrMsg = _('Błąd autoryzacji');
      return false;
    }
  }
  
  /**
   * 
   * @param array $aCodes
   * @param string $sCodePasswd
   */
  private function validateCode($aCodes, $sCodePasswd) {
    
    if (empty($_SESSION['verification_index_code'])) {
      return false;
    }
    
    $iCorrectChars = 0;
    foreach ($_SESSION['verification_index_code'] as $iValidationIndex) {
      if ($iValidationIndex === '' || 
              !isset($aCodes[$iValidationIndex]) || 
              $aCodes[$iValidationIndex] === '' ||
              !isset($sCodePasswd[$iValidationIndex-1])
              ) {
        return false;
      }
      
      if (intval($sCodePasswd[$iValidationIndex-1]) != intval($aCodes[$iValidationIndex])) {
        return false;
      } else {
        $iCorrectChars++;
      }
    }
    if ($iCorrectChars == self::LENGTH_VERYFICATION_CODE_PASSWD) {
      return true;
    }
    return false;
  }
	
	/** 
	* Metoda wykonuje logowanie
	* Pobiera dane z tablicy $_POST
	* Sprawdza czy istnieje taki uzytkownik czy podano dobre haslo
	* 
	* @return	bool							- true: logowanie poprawne; false: logowanie niepoprawne
	*/
	function DoLogin($pSmarty) {
		global $aConfig;
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sErrMsg = $oValidator->GetErrorString();
			return false; 
		}
    
    if ($this->checkLoginCode() === true) {
      $this->showCodeLoginForm($pSmarty);
      return -2;
    }
		
  	if (($iID = $this->_UserExists()) > 0) {
      $sUserLogin = $_POST['login'];
      $iLangId = $_POST['lang'];
      $this->doLoginUser($sUserLogin, $iLangId, $iID);
		}
    if ($iID > 0) {
      return true;
    }
		switch ($iID) {
			case 0:
				$this->sErrMsg = $aConfig['lang']['login']['bad_login'];
			break;
			
			case -1:
				$this->sErrMsg = $aConfig['lang']['login']['inactive'];
			break;
    
			case -2:
				$this->sErrMsg = _('Nie można zalogować się z podanego adresu IP');
			break;
		}
		return false;
	} // end of DoLogin()
  
  /**
   * Logowanie użytkownika
   * 
   * 
   */
  private function doLoginUser($sUserLogin, $iLangId, $iID) {
    global $aConfig;
    
    $iType = $this->GetUserType($iID);
    $aUData = $this->_GetUserPriv($iID);

    $_SESSION['user']['id'] = $iID;
    $_SESSION['user']['name'] = $sUserLogin;
    $_SESSION['user']['author'] = $this->_GetAuthorName($iID);
    $_SESSION['user']['type'] = $iType;
    $_SESSION['user']['ip'] = $_SERVER['REMOTE_ADDR'];
    $_SESSION['user']['host'] = gethostbyaddr($_SERVER['REMOTE_ADDR']);
    $_SESSION['user']['type'] = $iType;
    $_SESSION['user']['continue_session'] = $aUData['continue_session'];
    $_SESSION['user']['priv_order_status'] = $aUData['priv_order_status'];
    $_SESSION['user']['priv_address_data'] = $aUData['priv_address_data'];
    $_SESSION['user']['scale_access'] = $aUData['scale_access'];
    $_SESSION['user']['priv_commercial_data'] = $aUData['priv_commercial_data'];
    $_SESSION['user']['priv_order_5_status'] = $aUData['priv_order_5_status'];
    $_SESSION['user']['priv_password'] = $aUData['priv_password'];
    $_SESSION['user']['priv_reviews'] = $aUData['priv_reviews'];
    $_SESSION['check'] = md5($sUserLogin);
    $_SESSION['lang']['id'] = $iLangId;
    $_SESSION['lang']['symbol'] = $this->_GetLangSign();
    $_SESSION['lang']['default'] = $this->_GetDefaultLang();

    if (!AddCookie('check', md5($sUserLogin)) ||
        !AddCookie('usid', md5($iID.':'.session_id()))) {
      $this->sErrMsg = $aConfig['lang']['login']['cookie_error'];
      return false;
    }
    if ($iType === 0) {
      // zwykly uzytkownik, pobranie jego uprawnien
      $_SESSION['user']['privileges'] = $this->_GetUserPrivileges($iID);
    }

    // ustawienie w COOKIE wybranej wersji jezykowej
    AddCookie('lang', $iLangId, time() + 3600 * 24 * 365);

    // uzytkownik zostal zalogowany, wprowadzenie do bazy danych informacji
    // dla historii logowania
    $sSQL = "INSERT INTO ".$aConfig['tabls']['prefix']."login_history
             (login,
              user_id,
              login_date,
              ip,
              host,
              session_id)
             VALUES (
              ".Common::Quote($sUserLogin).",
              ".$iID.",
              NOW(),
              ".Common::Quote($_SERVER['REMOTE_ADDR']).",
              '".gethostbyaddr($_SERVER['REMOTE_ADDR'])."',
              '".session_id()."'
             )";
    $this->Query($sSQL);

    // przywrocenie stanu sesji z ustawien uzytkownik
    $this->_setUserSettings($iID);
    return true;
  }
	
	
	/*
	 * Metoda zwraca komunikat bledu
	 *
	 * @return	string							- komunikat bledu
	 */
	function GetErrMsg() {
		return $this->sErrMsg;
	} // end GetErrString()
	
	
	/*
	 * Metoda pobiera typ uzytkownika
	 * 0 - zwykly uzytkownik
	 * 1 - superuzytkownik - glowny administrator
	 * 2 - zwykly administrator
	 *
	 * @param		integer	$iUserID		- ID uzytkownika
	 * @return	integer							- 
	 */
	function GetUserType($iUserID) {
		global $aConfig;
		
		$sSQL = "SELECT utype FROM ".$aConfig['tabls']['prefix']."users WHERE id=".$iUserID;
		return (int) $this->GetOne($sSQL);
	} // end GetUserType()
	
	/**
	 * Metoda pobiera ustawienia podstawowych uprawnień użytkownika
	 *
	 * @global array $aConfig
	 * @param integer $iUserID - id użytkownika
	 * @return array
	 */
	function _GetUserPriv($iUserID) {
		global $aConfig;
		
		$sSQL = "SELECT priv_address_data, priv_commercial_data, priv_order_5_status, priv_order_status, priv_password, priv_reviews, continue_session, scale_access FROM ".$aConfig['tabls']['prefix']."users WHERE id=".$iUserID;
		return $this->GetRow($sSQL);
	} // end _GetUserPriv()
	
	
	/** 
	* Metoda wykonuje wylogowanie
	* Usuwa cookie i zamyka sesje
	*
	* @return boolean
	*/
	function DoLogout() {
		global $aConfig;
		// aktualizacja informacji o wylogowaniu
		$aValues = array(
			'logout_level' => '1',
			'logout_date' => 'NOW()'
		);
		$this->Update($aConfig['tabls']['prefix']."login_history",
									$aValues,
									"user_id = ".$_SESSION['user']['id']." AND
									 session_id = '".session_id()."'");
		// usuniecie ciasteczek i sesji
		DeleteSession();
	} // end of DoLogout()
	
  
	/**
	 * Metoda sprawdza czy dane sa poprawne
	 * Jezeli tak to zwraca ID uzytkownika
	 * 
	 * @return	integer									- 	0: bledne dane;
	 * 																		> 0: poprawne dane;
	 * 																		 -1: konto nieaktywne
	 * 																		 -2: niedozwolony adres IP
	 */
	function _UserExists() {
		global $aConfig;
	 	$aRetArr	= array();

	 	// sprawdzamy czy o konto o podanym loginie istnieje
	 	$sSQL = "SELECT id, active, ip_address, priv_order_5_status
             FROM ".$aConfig['tabls']['prefix']."users
             WHERE login=".Common::Quote($_POST['login'])." AND
             			 passwd='".EncodePasswd($_POST['passwd'])."'";
      
	 	$aRetArr = &$this->GetRow($sSQL);
    if (empty($aRetArr)) {
    	// nie ma takiego uzytkownika
    	return 0;
		}
		elseif($aRetArr['active'] == '0') {
    	return -1;
    }
    elseif($aRetArr['ip_address'] != '') {
      $aIPs = explode(',', $aRetArr['ip_address']);
      if (!in_array($_SERVER['REMOTE_ADDR'], $aIPs)) {
        return -2;
      }
    }
    return $aRetArr['id'];
	} // end of _UserExists()
	
	
	/**
	 * Metoda pobiera nazwe serwisu dla serwisu o ID $iWebsiteID
	 * 
	 * @param		integer		$iWebsiteID					- ID serwisu
	 * @return	string												- nazwa serwisu
	 */
  function _GetWebsiteName() {
  	global $aConfig;
  	$sSQL		= '';
  	
  	$sSQL = "SELECT name
  					 FROM ".$aConfig['tabls']['prefix']."websites
  					 WHERE id=".$aConfig['common']['website'];
  	
  	return $this->GetOne($sSQL);
	} // end of _GetWebsiteName() function
	
	
	/** Metoda pobiera oznaczenie wersji jez. o Id $_POST['lang']
	 * 
	 * @return	string												- nazwa serwisu
	 */
  function _GetLangSign() {
  	global $aConfig;
  	$sSQL		= '';
  	
  	$sSQL = "SELECT symbol
  					 FROM ".$aConfig['tabls']['prefix']."languages
  					 WHERE id=".$_POST['lang'];
  	
  	return $this->GetOne($sSQL);
	} // end of _GetLangSign() function
	
	
	/** Metoda pobiera czy wersja jezykowa jest domyslna
	 * 
	 * @return	string												- domyslna wersja czy nie
	 */
  function _GetDefaultLang() {
  	global $aConfig;
  	$sSQL		= '';
  	
  	$sSQL = "SELECT default_lang
  					 FROM ".$aConfig['tabls']['prefix']."languages
  					 WHERE id=".$_POST['lang'];
  	
  	return $this->GetOne($sSQL);
	} // end of _GetDefaultLang() function
	
  
  /**
   * Metoda pobiera uprawnienia dla zalogowanego uzytkownika z bazy danych
   * 
   * */
  function &_GetUserPrivileges($iUserID) {
    global $aConfig;
    $aPrivileges = array();
    $aP = array();
    $bIsMenuRedactor = false;
    
    $sSql = "SELECT module_id
						 FROM ".$aConfig['tabls']['prefix']."umodules
						 WHERE user_id = ".$iUserID;
    $aMprivs = &$this->GetCol($sSql);
	
    foreach ($aMprivs as $iModule) {
      		$aP['modules'][$iModule] = 0;
    }
   
    return $aP;
  } // end of _GetUserPrivileges() function  
  

 
  /**
   * Metoda pobiera poprzedni stan ustawien uzytkownika
   * i zapisuje je do sesji
   * 
   * @param	integer	$iId	- Id uzytkownika
   * @return	void
   */
	function _setUserSettings($iId) {
		global $aConfig;
		
		$sSql = "SELECT settings
						 FROM ".$aConfig['tabls']['prefix']."users
						 WHERE id = ".$iId;
		$sSettings =& Common::GetOne($sSql);
		if (!empty($sSettings)) {
			$aSettings = unserialize($sSettings);
			$_SESSION['_viewState'] = $aSettings[0];
			$_SESSION['_treeViewState'] = $aSettings[1];
		}
	}
	
	
  /**
   * Metoda pobiera poprzedni stan ustawien uzytkownika
   * i zapisuje je do sesji
   * 
   * @param	integer	$iId	- Id uzytkownika
   * @return	void
   */
	function _GetAuthorName($iId) {
		global $aConfig;
		
		$sSql = "SELECT name, surname, description
						 FROM ".$aConfig['tabls']['prefix']."users
						 WHERE id = ".$iId;
		$aUser =& Common::GetRow($sSql);
		if ($aUser['name'] != $aUser['surname']) {
			$sAuthor = $aUser['name'].' '.$aUser['surname'];
		}
		else {
			$sAuthor = $aUser['name'];
		}
		if ($aUser['description'] != '') {
			$sAuthor .= '<br />'.$aUser['description'];
		}
		return $sAuthor;
	}
} // end of class Login
?>