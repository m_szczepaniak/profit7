<?php

/**
 * Klasa Files - operacje na plikach
 *
 * @author Marcin Korecki <korecki@omnia.pl>
 * @version 1.0
 */

class Files {
	
	// Id strony
	var $iPId;
	
	// Id elementu
	var $iId;
		
	// sciezka do glownego katalogu na pliki
	var $sBaseDir;
	
	// sciezka do katalogu ze plikammi
	var $sDir;
	
	// nazwa pola rodzica w tabeli z plikami
	var $sField;
	
	// nazwa tabeli plikow
	var $sTable;
	
	// nazwa tabeli elementu - paragrafu, aktualnosci,...
	var $sItemTable;
	
	// nazwa dodatkowego pola rodzica, np. articles_page_id 
	var $sItemField;
	
	// wartosc dodatkowego pola rodzica, np. articles_page_id
	var $iItemId;
	
	/**
	 * Konstruktor
	 * 
	 * @param		array	$sImgSrc				- tablica z danymi przesylanego pliku
	 * @param 	
	 * @return 	void
	 **/
	function Files($iPId, $iId, $sDir, $sField, $sTable, $sItemTable='', $sItemField='', $iItemId=0)	{
		global $aConfig;
		
		$this->iPId = $iPId;
		$this->iId = $iId;
		$this->sBaseDir = $aConfig['common']['client_base_path'].
											$aConfig['common']['file_dir'];
		$this->sDir = $sDir;
		$this->sField = $sField;
		$this->sTable = $sTable;
		$this->sItemTable = $sItemTable;
		$this->sItemField = $sItemField;
		$this->iItemId = $iItemId;
	} // end of Files() method
	
	
	function proceed($bDelete=true) {
		global $aConfig;
		
		if (!is_dir($this->sBaseDir.'/'.$this->sDir)) {
			// katalog docelowy nie istnieje - utworzenie go
			if (!$this->createDirectory()) return false;
		}
		if ($bDelete && !empty($_POST['delete_file'])) {
			foreach ($_POST['delete_file'] as $sKey => $mValue) {
				// rozdzielenie klucza
				$aKey = explode('_', $sKey);
				if (!$this->deleteFile($aKey[1])) return false;
				// wyczyszczenie tablicy z plikami dla usunietego pliku
				$_FILES['file']['name'][$sKey] = '';
			}
		}
		return $this->proceedFiles();
	} // end of proceed() method
	
	
	/**
	 * Metoda usuwa plik
	 * 
	 * @param	integer	$iId	- Id pliku
	 * @param	bool	$bDB	- true: rowniez z bazy danych; false: tylko z katalogu
	 * @return	bool	- true: usunieto; false: wystapil blad
	 */
	function deleteFile($iId, $bDB=true) {
		if (($aFileSrc =& $this->getFileSrc($iId)) !== false) {
			if (!$this->deleteFromHD($aFileSrc)) return false;
			if ($bDB) {
				return $this->deleteFromDB($iId);
			}
			return true;
		}
		return false;
	} // end of deleteFile() method
	
	
	/**
	 * Metoda pobiera i zwraca sciezke o pliku
	 * 
	 * @param	integer	$iId	- Id pliku
	 * @return	string	- sciezka do pliku
	 */
	function &getFileSrc($iId) {
		global $aConfig;
		
		$sSql = "SELECT directory, filename
						 FROM ".$aConfig['tabls']['prefix'].$this->sTable."
						 WHERE ".($this->sItemField != '' && $this->iItemId != 0 ? $this->sItemField." = ".$this->iItemId : $this->sField." = ".$this->iId)." AND
						 			 id = ".$iId;
		return Common::GetRow($sSql);
	} // end of getFileSrc() method
	
	
	/**
	 * Metoda usuwa z dysku plik o podanej sciezce
	 * 
	 * @param	array	$aFile	- dane pliku - katalog i nazwa
	 * @return	bool	- tru: usunieto; flase: nie udalo sie usunac
	 */
	function deleteFromHD(&$aFile) {
		$sDir = $this->sBaseDir;
		if (empty($aFile['filename'])) return false;
		
		if (!empty($aFile['directory'])) {
			$sDir .= '/'.$aFile['directory'];
		}
		if (file_exists($sDir.'/'.$aFile['filename'])) {
			if (!@unlink($sDir.'/'.$aFile['filename'])) return false;
		}
		return true;
	} // end of deleteFromHD() method
	
	
	/**
	 * Metoda usuwa plik z bazy danych plik o podanym Id
	 * 
	 * @param	integer	$iId	- Id pliku
	 * @return	bool	- tru: usunieto; flase: nie udalo sie usunac
	 */
	function deleteFromDB($iId) {
		global $aConfig;
		
		if ($this->sItemField != '' || $this->sField != '') {
			$sWhere = ($this->sItemField != '' && $this->iItemId != 0 ? $this->sItemField." = ".$this->iItemId : $this->sField." = ".$this->iId);
		} else {
			$sWhere = ' 1=1 ';
		}
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix'].$this->sTable."
						 WHERE ".$sWhere." AND
						 			 id = ".$iId;
		return	Common::Query($sSql) !== false;
	} // end of deleteFromDB() method
	

	function proceedFiles() {
		global $aConfig;
				
		// dolaczenie klasy obslugujacej operacje na plikach
		include_once('File.class.php');
		$oFile = new MyFile();
		foreach ($_POST['desc_file'] as $sKey => $sDesc) {
			$aValues = array();
			$bDelete = false;
			$sOldName = '';
			
			if (trim($_FILES['file']['name'][$sKey]) != '') {
				$aFile = array(
					'name' => $_FILES['file']['name'][$sKey],
					'type' => $_FILES['file']['type'][$sKey],
					'tmp_name' => $_FILES['file']['tmp_name'][$sKey],
					'size' => $_FILES['file']['size'][$sKey]
				);
				$oFile->setFile($this->sBaseDir.'/'.$this->sDir, $aFile);
				if (($iResult = $oFile->proceedFile()) == 1) {
					$aValues = array(
						'directory' => $this->sDir,
						'filename'	=> $oFile->GetFileProperty('final_name'),
						'mime' => $oFile->GetFileProperty('type'),
						'size' => $oFile->GetFileProperty('size'),
						'name' => $oFile->GetFileProperty('name')
					);
					$bDelete = true;
				}
				elseif ($iResult == -5) {
					continue;
				}
				else {
					return false;
				}
			}
			$aValues['description'] = trim($sDesc) != '' ? trim($sDesc) : 'NULL';
						
			// rozdzielenie klucza
			$aKey = explode('_', $sKey);
			
			if (intval($aKey[1]) > 0) {
				if ($bDelete) {
					// pobranie nazwy starego pliku
					$sOldName = $this->getFileSrc($aKey[1]);
				}
				
				// aktualizacja pliku
				$aValues['order_by'] = $_POST['order_file'][$sKey];
				if (Common::Update($aConfig['tabls']['prefix'].$this->sTable,
												 	 $aValues,
												 	 ($this->sItemField != '' && $this->iItemId != 0 ? $this->sItemField." = ".$this->iItemId : $this->sField." = ".$this->iId)." AND
								 		 			 id = ".$aKey[1]) === false) {
					return false;
				}
				// fizyczne usuniecie pliku z dysku
				if ($bDelete && $sOldName !== false) {
					if (!$this->deleteFromHD($sOldName)) return 0;
				}
			}
			elseif (isset($aValues['filename'])) {
				// dodawanie nowego pliku
				if ($this->sItemField != '' && $this->iItemId != 0) {
					$aValues = array_merge(
						array(
							$this->sItemField => $this->iItemId,
							'order_by' => $this->getMaxOrderBy() + 1
						),
						$aValues
					);
				}
				else {
					$aValues = array_merge(
						array(
							$this->sField => $this->iId,
							'order_by' => $this->getMaxOrderBy() + 1
						),
						$aValues
					);
				}
				if (Common::Insert($aConfig['tabls']['prefix'].$this->sTable,
												 	 $aValues,
												 	 "",
												 	 false) === false) {
					return false;
				}
			}
		}
		return true;
	} // end of proceedFiles() method
	
	
	/**
	 * Metoda zwraca tablice z elementami do pola select z wyborem
	 * kolejnosci wyswietlania zdjec
	 * 
	 * @param	integer	$iFiles	- liczbe zdjec
	 * @return	array
	 */
	function &getOrderBy($iFiles) {
		$aArr = array();
		$iMaxOrderBy = $this->getMaxOrderBy();
		if ($iMaxOrderBy < $iFiles) {
			$iMaxOrderBy = $iFiles;
		}
		for ($i = 0; $i < $iMaxOrderBy; $i++) {
			$aArr[] = array('value' => $i+1, 'label' => $i+1);
		}
		return $aArr;
	} // end of getOrderBy() method
	
	
	/**
	 * Metoda pobiera maksymalna wartosc order_by
	 * 
	 * @return	integer
	 */
	function getMaxOrderBy() {
		global $aConfig;
		
		$sWhere = '';
		if ($this->sField != '' || $this->sItemField != '') {
		$sWhere = "WHERE ".($this->sItemField != '' && $this->iItemId != 0 ? $this->sItemField." = ".$this->iItemId : 
							 $this->sField." = ".$this->iId
							);
		}
		
		$sSql = "SELECT MAX(order_by)
						 FROM ".$aConfig['tabls']['prefix'].$this->sTable." ".$sWhere;
						 		return intval(Common::GetOne($sSql));
	} // end of getMaxOrderBy() method
	
	
	/**
	 * Metoda tworzy katalog docelowy
	 * 
	 * @return	bool	- true: utworzoo; false: wystapil blad
	 */
	function createDirectory() {
		global $aConfig;
		
		$aDirs = explode('/', $this->sDir);
		foreach ($aDirs as $iKey => $sDir) {
			$sPath .= '/'.$sDir;
			if (!is_dir($this->sBaseDir.$sPath)) {
				if (!mkdir($this->sBaseDir.$sPath,
									 $aConfig['default']['new_dir_mode'])) {
					return false;
				};
			}
		}
		return true;
	} // end of createDirectory() method
} // end of class Image
?>