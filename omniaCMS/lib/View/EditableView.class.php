<?php
/**
 * Klasa View do obslugi widoków z bazy danych w postaci tabelki HTML
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie podstawowej klasy Form 
include_once('Form/Form.class.php');
include_once('Form/FormTable.class.php');

class EditableView {

  // nazwa tabeli i formularza
	var $sName;
	
	// tablica zawierajaca dane dla naglowka
	var $aHeader;
	
	// tabela zawierajaca ustawienia tabeli
	var $aViewAttribs;
	
	// tablica zawierajaca liste filtrow
	var $aFilters;
	
	// tablica zawierajaca naglowek rekordow
	var $aRecordsHeader;
	
	// tablica zawierajaca liste rekordow
  var $aRecords;
  
  // tablica z ustawieniami kolumn rekordkow
  var $aColSettings;
	
	// tablica zawierajaca liste stron rekordow
  var $aSubRecords;
  
  // tablica z ustawieniami kolumn stron rekordkow
  var $aSubColSettings;
  
  // prefix strony, akapitu, itp...
  var $sItemPrefix;
	
	// tablica zwierajaca ustawienia dla stopki (przyciski 'zaznacz wszystkie', itp.)
	var $aRecordsFooter;
	
	// dodatkowe parametry dla linek stopki
	var $aFooterParams;
	
	// tablica z akcjami grupowymi w select w stopce
	var $aFooterGroupActions;
	
	// tablica z ustawieniami pol edytowalnych
	var $aEditableRecords;
  
  
	/**
   * Konstruktor klsy View
   *
	 * @param	string	$sName					- nazwa tabeli
	 * @param	array		$aHeader				- dane dla naglowka
	 * @param array 	$aViewAttribs 	- tablica asocjacyjna zawierajaca atrybuty formularza.
	 * Dostepne atrybuty:
 	 *  - action			    		URI
	 *	- method			    		(GET|POST) domyslnie POST
	 * 	- enctype			    		domyslnie: "application/x-www-form-urlencoded"
	 * 	- accept			    		lista typow MIME uploadowanych plikow
	 * 	- onsubmit			  		zdarzenie: formularz zostal wyslany
	 * 	- onreset			    		zdarzenie: formularz zostal zresetowany
	 * 	- accept-charset  		lista akceptowanych stron kodowych
   *
	 * @return void
   */
  function EditableView($sName, $aHeader, $aViewAttribs=array()) {
  	global $aConfig;
  	
  	if (!isset($aHeader['checkboxes'])) {
  		$aHeader['checkboxes'] = true;
  	}
		$this->sName						= $sName;
  	$this->aHeader    			= $aHeader;
    $this->aViewAttribs  		= $aViewAttribs;
    $this->aFilters					= array();
    $this->aRecordsHeader		= array();
    $this->aRecords					= array();
    $this->aColSettings			= array();
		$this->aRecordsFooter		= array();
		$this->aEditableRecords = array();
		$this->aFooterParams		= array();
		$this->aFooterGroupActions		= array();
	} // end of Form() method

	
	/**
	* Metoda dodaje dane naglowka rekordow
	* 
	* @param	array		$aHeader					 - dane naglowka
	* @return	void
	*/
	function AddRecordsHeader(&$aHeader) {
		$this->aRecordsHeader = $aHeader;
	} // end of AddRecordsHeader()
	
	
	/**
	* Metoda dodaje rekordy
	* 
	* @param	array		$aRecords					 - rekordy
	* @return	void
	*/
	function AddRecords(&$aRecords, &$aColSettings, &$aEditableRecords=array()) {
		$aColSettings['disabled']['show']	= false;
		$this->aRecords = $aRecords;
		$this->aColSettings = $aColSettings;
		$this->aEditableRecords = $aEditableRecords;
	} // end of AddRecords()
	
	
	/**
	* Metoda dodaje strony rekordow
	* 
	* @param	array		$aSubRecords					 - strony
	* @return	void
	*/
	function AddSubRecords(&$aRecords, &$aColSettings, $sItemPrefix='') {
		$aColSettings['disabled']['show']	= false;
		$this->aSubRecords = $aRecords;
		$this->aSubColSettings = $aColSettings;
		$this->sItemPrefix = $sItemPrefix; 
	} // end of AddSubRecords()
	
	
	/**
	* Metoda dodaje filtr
	* 
	* @param		string  $sName						nazwa elementu
  * @param		array   $aAttribs					atrybuty elementu
  * @param		array		$aOptions					atrybuty opcji elementu
	* @param		mixed		$mSelected				list wartosci opcji ktore maja byc zaznaczone
	* @return		void
	*/
	function AddFilter($sName, $sLabel, $aOptions, $mSelected='',$sType='select') {
		$this->aFilters[$sName] = array($sName, $sLabel, $aOptions, $mSelected, $sType);
	} // end of AddFilter() function
	
	function AddFooterGroupActions($aActions){
		$this->aFooterGroupActions = $aActions;
	}

	function AddFoldableRow($aData) {
	    $sHtml = '<tr class="row">';
        foreach ($aData as $sField) {
            $sHtml .= '<td>' . $sField . '</td>';
	    }
	    $sHtml .= '</tr>';
        return $sHtml;
    }
	
	/**
	* Metoda dodaje Pager
	* 
	* @param	integer		$iRowCount				- liczba wierszy
	* @return	integer											- numer aktualnej strony
	*/
	function AddPager($iRowCount,$bSecond=false) {
		global $aConfig;
		
		// dolaczenie klasy Pager
		include_once('Pager/Pager.php');
		
		// tablica konfiguracyjna dla Pagera
		$aPagerParams = array(
			'totalItems'	=> $iRowCount,
			'perPage'	=> isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'],
			'delta'	=> 3,
			'mode'	=> 'Sliding',
			'currentPage'	=> isset($_GET['page']) && !empty($_GET['page']) ? $_GET['page'] : 1,
			'urlVar'	=> 'page',
			'altPage'	=> $aConfig['lang']['pager']['page'],
			'altFirst'	=> $aConfig['lang']['pager']['first_page'],
			'altPrev'	=> $aConfig['lang']['pager']['previous_page'],
			'altNext'	=> $aConfig['lang']['pager']['next_page'],
			'altLast'	=> $aConfig['lang']['pager']['last_page'],
			'firstPagePre'	=> '',
			'firstPageText'	=> '&laquo;&laquo;',
			'firstPagePost'	=> '',
			'lastPagePre'	=> '',
			'lastPageText'	=> '&raquo;&raquo;',
			'lastPagePost'	=> '',
			'prevImg'	=> '&laquo;',
			'nextImg'	=> '&raquo;',
			'separator'	=> '|',
			'spacesBeforeSeparator'	=> 1,
			'spacesAfterSeparator'	=> 1,
			'linkClass'	=> 'page',
			'curPageLinkClassName'	=> 'currentPage',
			//'onclickFunc'	=> 'MenuNavigate(\'%s\'); return false;',
			'excludeVars'	=> ($bSecond?array('reset', 'id'):array('reset', 'do', 'id'))
		);
		$pPager = & Pager::factory($aPagerParams);
		$this->aPagerLinks = $pPager->getLinks();
		return $pPager->GetCurrentPageID();
	} // end of AddPager() function
	
	
	/**
	* Metoda dodaje dane stopki rekordow
	* 
	* @param	array		$aFooter					 - dane naglowka
	* @return	void
	*/
	function AddRecordsFooter(&$aFooter, $aParams=array()) {
		$this->aRecordsFooter = $aFooter;
		$this->aFooterParams =  $aParams;
	} // end of AddRecordsFooter()


    function isFoldable($aRecords) {
	    $isFoldable = false;

	    foreach ( $aRecords as $aRecord ) {
	        if ( empty($aRecord['foldable']) === false ) {
	            $isFoldable = true;
            }
        }

        return $isFoldable;
    }
	
	/**
	 * Metoda wyswietla widok
	 *
	 * @return	string									- widok
	 */
	function Show() {
		global $aConfig;
		
		$iColspan = 0;
		$aCheckboxAttribs = array();
		
		if (isset($_GET['file']) && !empty($_GET['file'])) {
			$sModule = $_GET['file'];
		}
		else {
			$sModule = $_GET['module'];
			if (isset($_GET['action'])) {
				$sModule .= '_'.$_GET['action'];
			}
		}
		//dump(get_javascript_array($this->aRecordsHeader,"aEdVSett"));
		// utworzenie naglowka formularza
		$aFAttribs = array(
			'method'	=> 'post',
			'action'	=> !isset($this->aHeader['action']) ? phpSelf(array(), array('do', 'id', 'pno'), false) : $this->aHeader['action']
		);
		
		$aSortingChange = array(
			'ASC' => 'DESC',
			'DESC'	=> 'ASC'
		);
				
		if ($this->aHeader['refresh']) {
			// dodanie linku 'odswiez'
			$sRefresh = '<a href="'.(!isset($this->aHeader['refresh_link']) ? phpSelf(array('reset'=>1)) : $this->aHeader['refresh_link']).'" title="'.$aConfig['lang']['common']['refresh'].'"><img src="gfx/icons/refresh_ico.gif" border="0" alt="'.$aConfig['lang']['common']['refresh'].'" style="margin-right: 10px; margin-top: 1px;"></a>';
		}
		if ($this->aHeader['search']) {
			// dodanie wyszukiwarki
			if (isset($_GET['search'])) {
				$_POST['search'] = $_GET['search'];
			}
			$sSearch .= Form::GetText('search', $_POST['search']);
			$sSearchButton = '<a href="javascript:void(0);" onclick="document.forms.'.$this->sName.'.action += \'&reset=3\'; document.forms.'.$this->sName.'.submit(); return false;" title="'.$aConfig['lang']['common']['search'].'"><img src="gfx/icons/search_ico.gif" border="0" alt="'.$aConfig['lang']['common']['search'].'" style="margin-left: 2px; margin-top: 1px;"></a>';
		}
		if($this->aHeader['editable']){
			//funckje RadioClckd nalezy dokonczyc poniewaz nie działa poprawnie - powoduje błędy. Obecnie szkoda czasu są ważniejsze elementy
      $sCountHeader = '';
      if ($this->aHeader['count_header']) {
        $sCountHeader = '&nbsp;<div class="chceckboxesCountInfo" style="text-align: center">Zaznaczono: 0</div>';
      }
		$sHtml = $sCountHeader.'<script type="text/javascript">
	// <![CDATA[
	'.(!empty($this->aEditableRecords)?get_javascript_array($this->aEditableRecords,"aEdVSett_".$this->sName):'').'
		function bind_click_events_'.$this->sName.'(){
			$("#evt_'.$this->sName.' td.editable_cell").click(function(event) {
			  var oclick = event.target.id;
			 	var aData=oclick.split("_");
			 	/*var content = $(event.target).text();*/
			 	var content = $(this).children("input").val();
			 	var iw = $(this).width();
			 	/** select */
			 	if(aEdVSett_'.$this->sName.'[aData[2]]["type"] == "select"){
			 		var sHtml = "<td class=\"active_cell\" id=\"cell_"+aData[1]+"_"+aData[2]+"\"><select name=\"editable["+aData[1]+"]["+aData[2]+"]\" class=\"editable_select\" style=\"width: "+iw+"px;\">";
			 		var itm;
			 		for(itm in aEdVSett_'.$this->sName.'[aData[2]]["items"]){
			 			sHtml += "<option value=\""+itm+"\" "+((itm == content)?" selected=\"selected\"":"")+">"+aEdVSett_'.$this->sName.'[aData[2]]["items"][itm]+"</option>";
			 		}
			 		sHtml += "</select></td>";
			 		$(event.target).replaceWith(sHtml);
			 		
			 		 var ident = "#cell_"+aData[1]+"_"+aData[2]+" .editable_select";

				  $(ident).blur(function(evnt){
						var aData = $(this).parent(".active_cell").attr("id").split("_");
						var ival = $(this).val();
						$(this).parent(".active_cell").replaceWith("<td class=\"editable_cell\" id=\"cell_"+aData[1]+"_"+aData[2]+"\">"+aEdVSett_'.$this->sName.'[aData[2]]["items"][ival]+"<input type=\"hidden\" class=\"heditable_input\" name=\"editable["+aData[1]+"]["+aData[2]+"]\" value=\""+ival+"\"> </td>");
						bind_click_events_'.$this->sName.'();
					});
				  $("#cell_"+aData[1]+"_"+aData[2]+" .editable_select").focus();
			 	/** pole tesktowe */	
			 	} 
			 	else {
				  $(event.target).replaceWith("<td class=\"active_cell\" id=\"cell_"+aData[1]+"_"+aData[2]+"\"><input type=\"text\" name=\"editable["+aData[1]+"]["+aData[2]+"]\" class=\"editable_input\" value=\""+content+"\" style=\"width: "+iw+"px;\"/></td>");
				  var ident = "#cell_"+aData[1]+"_"+aData[2]+" .editable_input";

				  $(ident).blur(function(evnt){
				  	aDisId=ident.split(\'_\');
				  	
				  	if(document.getElementById(\'delete[\'+aDisId[1]+\']\'))
					  	if(this.value!="0,00" && this.value!="0" && this.value!="0,1") {
					  		document.getElementById(\'delete[\'+aDisId[1]+\']\').disabled=false;
								}
							else
								document.getElementById(\'delete[\'+aDisId[1]+\']\').disabled=true;
						var aData = $(this).parent(".active_cell").attr("id").split("_");
						var ival = $(this).val();
						$(this).parent(".active_cell").replaceWith("<td class=\"editable_cell\" id=\"cell_"+aData[1]+"_"+aData[2]+"\">"+ival+"<input type=\"hidden\" class=\"heditable_input\" name=\"editable["+aData[1]+"]["+aData[2]+"]\" value=\""+ival+"\"> </td>");
						bind_click_events_'.$this->sName.'();
					});
				  $("#cell_"+aData[1]+"_"+aData[2]+" .editable_input").focus();
			  }
			});
		}
	
		$(document).ready(function(){
			bind_click_events_'.$this->sName.'();
		});
	// ]]>
</script>
		';
		}	else {
			$sHtml = '';
		}
		$sHtml .= '<table id="evt_'.$this->sName.'"';
		foreach ($this->aViewAttribs as $sName => $sAttrib) {
			$sHtml .= ' '.$sName.'="'.$sAttrib.'"';
		}
		$sHtml .= '>';
		$sHtml .=  '
		<tr>
			<td class="tableMargin">';
		if (!isset($this->aHeader['form']) || $this->aHeader['form']) {
      
      $pForm = new FormTable($this->sName, '', $aFAttribs);
      $sHtml .= $pForm->GetFormHeader();
      $sHtml .= $pForm->GetJS();
      $sHtml .= $pForm->getTokenSessionHTML($_SESSION);
      $sHtml .= $pForm->GetHiddenHTML('do');
      $sHtml .= $pForm->GetHiddenHTML('__ErrPrefix', 
              $pForm->GetErrorPrefix(), 
              array('id' => 'uaF__ErrPrefix'));
      
      $sHtml .= $pForm->GetHiddenHTML('__ErrPostfix',
              $pForm->GetErrorPostfix(),
              array('id' => 'uaF__ErrPostfix'));
      $sHtml .= $pForm->GetHiddenHTML('__Validator',
              $pForm->GetValidator(),
              array('id' => 'uaF__Validator'));
//			$sHtml .= Form::GetHeader($this->sName, $aFAttribs);
		}
		if (isset($this->aHeader['header'])) {
			$sHtml .= '
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
						<tr>
							<td width="1" height="18" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="18" class="brownBg">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<col width="60%">
									<col>
									<tr>
										<td><span class="tableHeaderBg"><strong>'.encodeHeaderString($this->aHeader['header']).'</strong></span></td>
										<td style="text-align: right; padding-right: 10px;">
											<table border="0" cellspacing="0" cellpadding="0" align="right">
												<tr>
													<td>'.$sRefresh.'</td>
													<td align="right">'.$sSearch.'</td>
													<td align="right">'.$sSearchButton.'</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
							<td width="1" height="18" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
						<tr>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
					</table>';
		}
		if (!empty($this->aFilters)) {
			if (count($this->aFilters) > 1) {
				$sOnChange = 'void(0);';
				//$sAppendHtml = Form::GetInputButton('filters_submit', $aConfig['lang']['filters']['submit'], array('onclick'=>'this.form.action += \'&reset=3\'; this.form.submit();'), 'button').'&nbsp;&nbsp;';
				$sAppendHtml = Form::GetInputButton('submit_filters', $aConfig['lang']['filters']['submit'], array('onclick'=>'this.form.action += \'&reset=3\';')).'&nbsp;&nbsp;';
			}
			else {
				$sOnChange = 'this.form.action += \'&reset=3\'; this.form.submit();';
				$sAppendHtml = '';
			}
			$sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="filtersTable">';
			$sHtml .= '<tr>';
			$sHtml .= '<td>';
			foreach ($this->aFilters as $aFilter) {
				if($aFilter[4]!='html'){
					$sHtml .= '<label for="'.$aFilter[0].'" style="cursor: '.$aConfig['styles']['cursor'].';">'.$aFilter[1].'</label>';
				}
				$sHtml .= '&nbsp;';
				if($aFilter[4]=='text'){
					$sHtml .= Form::GetText($aFilter[0], $aFilter[3], array('id'=>$aFilter[0])).'&nbsp;';
				} 
				elseif($aFilter[4]=='date'){
					$sHtml .='<div style="float:left;"><div style="float:left;cursor: '.$aConfig['styles']['cursor'].'; font-size:11px; margin:2px; margin-top:4px; ">'.$aFilter[1].'</div>';
					$sHtml .= '<div style="float:left">'.Form::GetText($aFilter[0], $aFilter[3], array('id'=>$aFilter[0], 'style'=>'width:72px;'),'date').'</div></div>';
				} 
				elseif($aFilter[4]=='html'){
					$sHtml .= $aFilter[2];
				} 
				else {
					$sHtml .= Form::GetSelect($aFilter[0], array('id'=>$aFilter[0], 'onchange'=>$sOnChange, 'style'=>($aFilter[0]=='f_category'?'width:250px;':'')), $aFilter[2], $aFilter[3]).'&nbsp;';
				}
			}
			$sHtml .= $sAppendHtml;
			$sHtml .= '</td>';
			$sHtml .= '</tr>';
			$sHtml .= '</table>';
		}

        if ( $this->isFoldable($this->aRecords)  ) {
            $sHtml .= '<a href="javascript:void(0);" style="text-decoration: none; font-weight: bold; font-size: 12px;" onclick="toggleAllRows(this);">Rozwiń wszystkie <img src="gfx/collapsed.gif"></a>';
        }
		$sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="recordsTable">';
		if (!empty($this->aRecordsHeader) && !empty($this->aRecords)) {
			$sHtml .= '<tr>';
			if (isset($this->aSubRecords) && !empty($this->aSubRecords)) {
				$sHtml .= '<th width="20">&nbsp;</th>';
				$iColspan++;
			}
			foreach ($this->aRecordsHeader as $aColumn) {
				$sHtml .= '<th'.(isset($aColumn['width']) ? ' width="'.$aColumn['width'].'"' : '').'>';
				if ($aColumn['sortable']) {
					$aGet['sort'] = $aColumn['db_field'];
					if (isset($_GET['sort']) && $_GET['sort'] == $aColumn['db_field']) {
						$aGet['order'] = $aSortingChange[$_GET['order']];
					}
					else {
						$aGet['order'] = 'ASC';
					}
					// resetowanie strony
					$aGet['reset'] = 3;
					$sHtml .= '<a href="'.phpSelf($aGet).'">'.$aColumn['content'].'</a>';
				}
				else {
					$sHtml .= $aColumn['content'];
				}
				$sHtml .= '</th>';
				$iColspan++;
			}
			$sHtml .= '</tr>';
		
			// rekordy
			foreach ($this->aRecords as $aRecord) {
			    if ( empty($aRecord['foldable']) === false ) {
                    $sHtml .= $this->AddFoldableRow($aRecord['foldable']);
                }
				$sHtml .= '<tr style="display: ' . ($aRecord['foldable'] ? 'none' : 'table-row') .'" class="row" onmouseover="this.className=\'rowHov\';" onmouseout="this.className=\'row\';">';
    				if (isset($this->aSubRecords) && !empty($this->aSubRecords)) {
					$sHtml .= '<td class="pg">';
					if (isset($_SESSION['_viewState'][$sModule]['exp']) &&
							in_array($aRecord['id'], $_SESSION['_viewState'][$sModule]['exp']) &&
							!empty($this->aSubRecords[$aRecord['id']])) {
						$sHtml .= '<a href="'.phpSelf(array('nar' => $aRecord['id'])).'" title="'.$aConfig['lang'][$sModule]['hide_items'].'"><img src="gfx/icons/tree_minus.gif" width="18" alt="'.$aConfig['lang'][$sModule]['hide_items'].'" border="0"></a>';
					}
					elseif (isset($this->aSubRecords[$aRecord['id']])) {
						$sHtml .= '<a href="'.phpSelf(array('exp'=>$aRecord['id'])).'" return false;" title="'.$aConfig['lang'][$sModule]['show_items'].'"><img src="gfx/icons/tree_plus.gif" width="18"  alt="'.$aConfig['lang'][$sModule]['show_items'].'" border="0"></a>';
					}
					else {
						$sHtml .= '<img src="gfx/pixel.gif" width="18" alt="" border="0">';
					}
					$sHtml .= '</td>';
				}
				if (!isset($this->aHeader['checkboxes']) || $this->aHeader['checkboxes']) {
					if (isset($aRecord['disabled']) && in_array('delete', $aRecord['disabled'])) {
						// checkbox wylaczony - brak mozliwosci usuniecia rekordu
						$aCheckboxAttribs['disabled'] = 'disabled';
					}
					else {
						$aCheckboxAttribs = array('onclick'=>'
						{
                    var cond = $(this).parent().parent().children().eq(2).children(\'a\').is(\'.order_deleted\');
                                if(this.checked==true) {
                        if(cond) 
                          $(this).parent().parent().css(\'background\', \'#FC7C7C\');
                        
                        else 
                          this.parentNode.parentNode.style.backgroundColor=\'#d7d3bc\';
                    }
                                else {
                                    if(cond) {
                        $(this).parent().parent().css(\'background\', \'#FFA3A3\');
                      }
                      else 
                        this.parentNode.parentNode.style.backgroundColor=\'\';
                    }
            
						}
						');
					}
					$aCheckboxAttribs['class'] = 'viewDeleteCheckbox';
					$aCheckboxAttribs['title'] = $this->aHeader['count_checkboxes_by']!=''?$aRecord[$this->aHeader['count_checkboxes_by']]:1;
					$sHtml .= '<td class="check">'.Form::GetCheckBox('delete['.$aRecord['id'].']', $aCheckboxAttribs).'</td>';
				}
        $iRowsCount = 0;
				foreach ($aRecord as $sName => $sContent) {
					if ($sName != 'disabled' && $sName != 'invisible' && $sName != 'foldable') {
						if (trim($sContent) == '') {
							$sContent = '&nbsp';
						}
						if (!isset($this->aColSettings[$sName]['show']) ||
								(isset($this->aColSettings[$sName]['show']) && $this->aColSettings[$sName]['show'])) {
              
              $sStyleAdd = '';
              if (isset($this->aRecordsHeader[$iRowsCount]['style']) && !empty($this->aRecordsHeader[$iRowsCount]['style'])) {
                $sStyleAdd .= ' style="'.$this->aRecordsHeader[$iRowsCount]['style'].'" ';
              }
            if (isset($this->aRecordsHeader[$iRowsCount]['class']) && !empty($this->aRecordsHeader[$iRowsCount]['class'])) {
                $sStyleAdd .= ' class="'.$this->aRecordsHeader[$iRowsCount]['class'].'" ';
            }
							$sHtml .= '<td'.$sStyleAdd.(($this->aHeader['editable'] && isset($this->aColSettings[$sName]['editable']) && !empty($aRecord['id'])&&(!isset($aRecord['disabled']) || !in_array($sName, $aRecord['disabled'])))?' class="editable_cell" id="cell_'.$aRecord['id'].'_'.$sName.'"':'').'>';
							if (isset($this->aColSettings[$sName]['use_lang']) && isset($aConfig['lang'][$sModule][$sContent])) {
								$sContent = $aConfig['lang'][$sModule][$sContent];
							}
							
							if (isset($this->aColSettings[$sName]['filter'])) {
								$aFilter = explode(':', $this->aColSettings[$sName]['filter']);
								$aParams = array($sContent);
								if (count($aFilter) > 1) {
									for ($i = 1; $i < count($aFilter); $i++) {
										$aParams[] = $aFilter[$i];
									}
								}
								$sContent = call_user_func_array($aFilter[0], $aParams);
							}
							
							if (isset($this->aColSettings[$sName]['link'])) {
								if (!empty($this->aColSettings[$sName]['link']) &&
										(!isset($aRecord['disabled']) ||
										 !in_array($sName, $aRecord['disabled']))) {
									if (isset($aRecord['action']['links'][$sName])) {
										$sLink = $aRecord['action']['links'][$sName];
									}
									else {
										// zamiana w linku {zmienna} na $aRecord['zmienna']
										$sLink = preg_replace('/\{(\w+)\}/e', "\$aRecord['\\1']", $this->aColSettings[$sName]['link']);
									}
									$sHtml .= '<a href="'.$sLink.'" class="'.(($aRecord['id']==$_SESSION['_last_modified'][$this->sName])?'last_modified':'link').'">'.$sContent.'</a>';
								}
								else {
									if($this->aHeader['editable'] && isset($this->aColSettings[$sName]['editable']) && !empty($aRecord['id']) && ($this->aEditableRecords[$sName]['type']=='select'||$this->aEditableRecords[$sName]['type']=='radio') &&(!isset($aRecord['disabled']) || !in_array($sName, $aRecord['disabled']))){
										$sContentText = $this->aEditableRecords[$sName]['items'][$sContent];
									} else {
										$sContentText = $sContent;
									}
									$sHtml .= $sContentText.((isset($this->aColSettings[$sName]['editable']) && !empty($aRecord['id']))?' <input type="hidden" class="heditable_input" name="editable['.$aRecord['id'].']['.$sName.']" value="'.($sContent=='&nbsp;'?'':$sContent).'">':'');
								}
							}
							else {
								if($this->aHeader['editable'] && isset($this->aColSettings[$sName]['editable']) && !empty($aRecord['id']) && ($this->aEditableRecords[$sName]['type']=='select'||$this->aEditableRecords[$sName]['type']=='radio') &&(!isset($aRecord['disabled']) || !in_array($sName, $aRecord['disabled']))){
										$sContentText = $this->aEditableRecords[$sName]['items'][$sContent];
									} else {
										$sContentText = $sContent;
									}
								$sHtml .= $sContentText.(($this->aHeader['editable'] && isset($this->aColSettings[$sName]['editable']) && !empty($aRecord['id']))?' <input type="hidden" class="heditable_input" name="editable['.$aRecord['id'].']['.$sName.']" value="'.($sContent=='&nbsp;'?'':$sContent).'">':'');
							}
							$sHtml .= '</td>';
						}
					}
          $iRowsCount++;
				}
				// kolumna z przyciskami akcji
				if (isset($this->aColSettings['action']['actions']) && !empty($this->aColSettings['action']['actions'])) {
					$sHtml .= '<td'.(!empty($_SESSION['_modified'][$this->sName])&&in_array($aRecord['id'],$_SESSION['_modified'][$this->sName])?' class="modified"':'').'>';
					foreach ($this->aColSettings['action']['actions'] as $sActionDo) {
						if (isset($this->aColSettings['action']['icon'][$sActionDo])) {
							$sIcon = $this->aColSettings['action']['icon'][$sActionDo];
						}
						else {
							$sIcon = $sActionDo;
						}
						if (isset($this->aColSettings['action']['lang'][$sActionDo])) {
							$sTitle = $this->aColSettings['action']['lang'][$sActionDo]; 
						}
						else {
							$sTitle = $aConfig['lang'][$sModule][$sActionDo];
						}
						$aGet = array('do'=>$sActionDo);
						if (isset($this->aColSettings['action']['params'][$sActionDo])) {
							// zdefiniowano dodatkowe parametry dla przycisku
							foreach ($this->aColSettings['action']['params'][$sActionDo] as $sParam => $sValue) {
								// zamiana {zmienna} na $aRecord['zmienna'] w $sValue
								$sValue = preg_replace('/^\{(\w+)\}$/e', "\$aRecord['\\1']", $sValue);
								if (isset($aGet[$sParam])) {
									unset($aGet[$sParam]);
								}
								if ($sValue != '') {
									$aGet[$sParam] = $sValue;
								}
							}
						}
						if (isset($aRecord['disabled']) &&
								in_array($sActionDo, $aRecord['disabled'])) {
							 // ten przycisk jest nieaktywny
							 $sHtml .= 	'<img src="gfx/icons/'.$sIcon.'_disabled_ico.gif" alt="'.$sTitle.'" border="0">'.
													'<img src="gfx/pixel.gif" width="5" alt="" border="0">';
						}
						elseif (isset($aRecord['invisible']) &&
								in_array($sActionDo, $aRecord['invisible'])) {

								// ten przycisk jest nieaktywny
							 $sHtml .= 	'&nbsp;';
						}
						elseif ($sActionDo == 'br') {

								// nowa linia w kolumnie akcji
							 $sHtml .= 	'<br />';
						}
						else {
							if ($sActionDo == 'delete' || $sActionDo == 'delete_item') {
								$sHtml .= '<a href="javascript:void(0);" onclick="if (confirm(\''.$aConfig['lang'][$sModule][$sActionDo.'_q'].'\')){MenuNavigate(\''.phpSelf($aGet).'\')}; return false;"';
							}
							elseif ($sActionDo == 'preview') {
								$sLink = $aConfig['common']['client_base_url_http'];
								if (!isset($aRecord['action']['links'][$sActionDo])) {
									if (!empty($aGet)) {
										unset($aGet['do']);
										$sLink .= '?';
										foreach ($aGet as $sParam => $sValue) {
											$sLink .= $sParam.'='.$sValue.'&';
										}
										$sLink = substr($sLink, 0, strlen($sLink)-1);
									}
								}
								else {
									$sLink .= $aRecord['action']['links'][$sActionDo]; 
								}
								$sHtml .= '<a href="'.$sLink.'" target="_blank"';
							}
							else {
								if ($sActionDo == 'sort') {
									$aGet['back'] = base64_encode(phpSelf($aGet, array('do', 'id')));
								}
								if (isset($aRecord['action']['links'][$sActionDo])) {
									$sHtml .= '<a href="'.$aRecord['action']['links'][$sActionDo].'"';
								}
								else {
									$sHtml .= '<a href="'.phpSelf($aGet).'"';
								}
							}
                            $sAddConfirm = '';
                            if (isset($this->aColSettings['action']['confirms'][$sActionDo])) {
                                $sAddConfirm = 'onclick="return confirm(\''.$this->aColSettings['action']['confirms'][$sActionDo].'\')"';
                            }
                            $sHtml .= ' title="' . $sTitle . '" '.$sAddConfirm.'>' .
													'<img src="gfx/icons/'.$sIcon.'_ico.gif" alt="'.$sTitle.'" border="0">'.
													'</a>'.
													'<img src="gfx/pixel.gif" width="5" alt="" border="0">';
						}
					}
					$sHtml .= '</td>';
				}
				$sHtml .= '</tr>';
				
				// strony rekordu
				if (isset($this->aSubRecords[$aRecord['id']]) &&
						!empty($this->aSubRecords[$aRecord['id']]) &&
						isset($_SESSION['_viewState'][$sModule]['exp']) &&
						in_array($aRecord['id'], $_SESSION['_viewState'][$sModule]['exp'])) {
					foreach ($this->aSubRecords[$aRecord['id']] as $aPage) {
						$sHtml .= '<tr class="row" onmouseover="this.className=\'rowHov\';" onmouseout="this.className=\'row\';">';
						$sHtml .= '<td class="page">&nbsp;</td>';
						if (!isset($this->aHeader['checkboxes']) || $this->aHeader['checkboxes']) {
							$sHtml .= '<td class="page">&nbsp;</td>';
						}
						foreach ($aRecord as $sName => $sContent) {
							if (!isset($this->aColSettings[$sName]['show']) ||
									(isset($this->aColSettings[$sName]['show']) && $this->aColSettings[$sName]['show'])) {
									
								$sSubContent = $aPage[$sName];
								if (key_exists($sName, $aPage)) {
									if ($sName == 'name') {
										$sHtml .= '<td class="pageName">';
										if ($this->sItemPrefix != '') {
											$sPageNo = '<span class="internalLink">'.preg_replace('/\{(\w+)\}/e', "\$aPage['\\1']", $this->sItemPrefix).'</span>';
										
											$sSubContent = $sPageNo.(!empty($sSubContent) ? ': ' : '').$sSubContent;
										}
										if (isset($aPage['active']) && $aPage['active'] == '1') {
											$sSubContent = '<b>'.$sSubContent.'</b>';
										}
									}
									else {
										$sHtml .= '<td class="page">';
									}
						
									if (isset($this->aSubColSettings[$sName]['filter'])) {
										$aSubFilter = explode(':', $this->aSubColSettings[$sName]['filter']);
										$aSubParams = array(&$sSubContent);
										if (count($aSubFilter) > 1) {
											for ($i = 1; $i < count($aSubFilter); $i++) {
												$aSubParams[] = $aSubFilter[$i];
											}
										}
										call_user_func_array($aSubFilter[0], $aSubParams);
									}
							
									if (isset($this->aSubColSettings[$sName]['link'])) {
										if (!empty($this->aSubColSettings[$sName]['link'])) {
											// zamiana w linku {zmienna} na $aPage['zmienna']
											$sSubLink = preg_replace('/\{(\w+)\}/e', "\$aPage['\\1']", $this->aSubColSettings[$sName]['link']);
											$sHtml .= '<a class="sub link" href="'.$sSubLink.'">'.stripslashes($sSubContent).'</a>';
										}
										else {
											$sHtml .= '<strong>'.stripslashes($sSubContent).'</strong>';
										}
									}
									else {
										$sHtml .= stripslashes($sSubContent);
									}
									$sHtml .= '</td>';
								}
								else {
									$sHtml .= '<td class="page">&nbsp;</td>';
								}
							}
						}
						// kolumna z przyciskami akcji
						if (isset($this->aSubColSettings['action']['actions']) && !empty($this->aSubColSettings['action']['actions'])) {
							$sHtml .= '<td class="page">';
							foreach ($this->aSubColSettings['action']['actions'] as $sActionDo) {
								if ($sActionDo == 'empty') {
									$sHtml .= '<img src="gfx/pixel.gif" width="19" alt="" border="0">';
								}
								else {
									if (isset($this->aSubColSettings['action']['icon'][$sActionDo])) {
										$sIcon = $this->aSubColSettings['action']['icon'][$sActionDo];
									}
									else {
										$sIcon = $sActionDo;
									}
									if (isset($this->aSubColSettings['action']['lang'][$sActionDo])) {
										$sTitle = $this->aSubColSettings['action']['lang'][$sActionDo]; 
									}
									else {
										$sTitle = $aConfig['lang'][$sModule][$sActionDo];
									}
									$aGet = array('do'=>$sActionDo);
									if (isset($this->aSubColSettings['action']['params'][$sActionDo])) {
										// zdefiniowano dodatkowe parametry dla przycisku
										foreach ($this->aSubColSettings['action']['params'][$sActionDo] as $sParam => $sValue) {
											// zamiana {zmienna} na $aRecord['zmienna'] w $sValue
											$sValue = preg_replace('/^\{(\w+)\}$/e', "\$aPage['\\1']", $sValue);
											$aGet[$sParam] = $sValue;
										}
									}
									if (isset($aPage['disabled']) &&
											in_array($sActionDo, $aPage['disabled'])) {
											// ten przycisk jest nieaktywny
										 $sHtml .= 	'<img src="gfx/icons/'.$sIcon.'_disabled_ico.gif" alt="'.$sTitle.'" border="0">'.
																'<img src="gfx/pixel.gif" width="5" alt="" border="0">';
									}
									else {
										if ($sActionDo == 'delete_page') {
											$sHtml .= '<a href="javascript:void(0);" onclick="if (confirm(\''.$aConfig['lang'][$sModule][$sActionDo.'_q'].'\')){MenuNavigate(\''.phpSelf($aGet).'\')}; return false;"';
										}
										elseif ($sActionDo == 'preview_page') {
											$sLink = $aConfig['common']['client_base_url_http'];
											if (!isset($aPage['action']['links'][$sActionDo])) {
												if (!empty($aGet)) {
													unset($aGet['do']);
													$sLink .= '?';
													foreach ($aGet as $sParam => $sValue) {
														$sLink .= $sParam.'='.$sValue.'&';
													}
													$sLink = substr($sLink, 0, strlen($sLink)-1);
												}
											}
											else {
												$sLink .= $aPage['action']['links'][$sActionDo]; 
											}
											$sHtml .= '<a href="'.$sLink.'" target="_blank"';
										}
										else {
											$sHtml .= '<a href="'.phpSelf($aGet).'"';
										}
										$sHtml .= 	' title="'.$sTitle.'">'.
																'<img src="gfx/icons/'.$sIcon.'_ico.gif" alt="'.$sTitle.'" border="0">'.
																'</a>'.
																'<img src="gfx/pixel.gif" width="5" alt="" border="0">';
									}
								}
							}
							$sHtml .= '</td>';
						}
						$sHtml .= '</tr>';
					}
					$sHtml .= '<tr><td colspan="'.$iColspan.'" class="lastPage"><img src="gfx/pixel.gif" width="1" height="1" alt="" border="0"></td></tr>';
				}
			}
		}
		else {
			$sHtml .= '<tr class="row">'.
								'<td class="message">'.$aConfig['lang'][$sModule]['no_items'].'</td>'.
								'</tr>';
		}
		$sHtml .= '</table>';
		$bSendIsInside=false;
		if (!empty($this->aRecordsFooter)) {
			$sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="footerTable">'.
								'<tr>'.
								'<td class="first">';
			if (!empty($this->aRecordsFooter[0])) {
				foreach ($this->aRecordsFooter[0] as $sActionDo) {
					switch ($sActionDo) {
						case 'check_all':
							if (!empty($this->aRecords)) {
								// dodanie checkboxa zaznaczajacego wszystkie checkboxy
								$sHtml .= Form::GetCheckBox('check_all', array('onclick'=>'ToggleCheckboxes(document.forms.'.$this->sName.', \'delete\', true);')).
													'&nbsp;<a id="toggleCheckAll" href="javascript: void(0);" onclick="ToggleCheckboxes(document.forms.'.$this->sName.', \'delete\', false);"'.
													' title="'.$aConfig['lang'][$sModule][$sActionDo].'">'.
													$aConfig['lang'][$sModule][$sActionDo].
													'</a>&nbsp;&nbsp;'.
													($this->aHeader['count_checkboxes']?'|&nbsp;<span class="chceckboxesCountInfo">Zaznaczono: 0</span>&nbsp;|&nbsp;&nbsp;':'');
							}
						break;
					
						case 'delete_all':
							if (!empty($this->aRecords)) {
								// dodanie linku usuwajacego wszystkie zaznaczone rekordy
								$sHtml .= '<a href="javascript:void(0);"'.
													' onclick="if (Checked(document.forms.'.$this->sName.', \'delete\')){'.
													'if (confirm(\''.$aConfig['lang'][$sModule][$sActionDo.'_q'].'\')) { ChangeObjValue(\'do\', \'delete\'); document.forms.'.$this->sName.'.submit(); return false;} else { return false; }}'.
													'else {'.
													'alert(\''.$aConfig['lang'][$sModule][$sActionDo.'_err'].'\'); return false;}"'.
													' title="'.$aConfig['lang'][$sModule][$sActionDo].'">'.
													'<img src="gfx/icons/'.$sActionDo.'_ico.gif" border="0" align="absmiddle"'.
													' alt="'.$aConfig['lang'][$sModule][$sActionDo].'"> '.
													$aConfig['lang'][$sModule][$sActionDo].
													'</a>&nbsp;&nbsp;';
							}
						break;
						
						case 'remove_all':
							if (!empty($this->aRecords)) {
								// dodanie linku usuwajacego wszystkie zaznaczone rekordy
								$sHtml .= '<a href="javascript:void(0);"'.
													' onclick="if (Checked(document.forms.'.$this->sName.', \'delete\')){'.
													'if (confirm(\''.$aConfig['lang'][$sModule][$sActionDo.'_q'].'\')) { ChangeObjValue(\'do\', \'remove\'); document.forms.'.$this->sName.'.submit(); return false;} else { return false; }}'.
													'else {'.
													'alert(\''.$aConfig['lang'][$sModule][$sActionDo.'_err'].'\'); return false;}"'.
													' title="'.$aConfig['lang'][$sModule][$sActionDo].'">'.
													'<img src="gfx/icons/'.$sActionDo.'_ico.gif" border="0" align="absmiddle"'.
													' alt="'.$aConfig['lang'][$sModule][$sActionDo].'"> '.
													$aConfig['lang'][$sModule][$sActionDo].
													'</a>&nbsp;&nbsp;';
							}
						break;
						
						case 'publish_all':
							if (!empty($this->aRecords)) {
								// dodanie linku usuwajacego wszystkie zaznaczone rekordy
								$sHtml .= '<a href="javascript:void(0);"'.
													' onclick="if (Checked(document.forms.'.$this->sName.', \'delete\')){'.
													'if (confirm(\''.$aConfig['lang'][$sModule][$sActionDo.'_q'].'\')) { ChangeObjValue(\'do\', \'publish\'); document.forms.'.$this->sName.'.submit(); return false;} else { return false; }}'.
													'else {'.
													'alert(\''.$aConfig['lang'][$sModule][$sActionDo.'_err'].'\'); return false;}"'.
													' title="'.$aConfig['lang'][$sModule][$sActionDo].'">'.
													'<img src="gfx/icons/'.$sActionDo.'_ico.gif" border="0" align="absmiddle"'.
													' alt="'.$aConfig['lang'][$sModule][$sActionDo].'"> '.
													$aConfig['lang'][$sModule][$sActionDo].
													'</a>&nbsp;&nbsp;';
							}
						break;
						
						case 'review_all':
							if (!empty($this->aRecords)) {
								// dodanie linku usuwajacego wszystkie zaznaczone rekordy
								$sHtml .= '<a href="javascript:void(0);"'.
													' onclick="if (Checked(document.forms.'.$this->sName.', \'delete\')){'.
													'if (confirm(\''.$aConfig['lang'][$sModule][$sActionDo.'_q'].'\')) { ChangeObjValue(\'do\', \'review\'); document.forms.'.$this->sName.'.submit(); return false;} else { return false; }}'.
													'else {'.
													'alert(\''.$aConfig['lang'][$sModule][$sActionDo.'_err'].'\'); return false;}"'.
													' title="'.$aConfig['lang'][$sModule][$sActionDo].'">'.
													'<img src="gfx/icons/'.$sActionDo.'_ico.gif" border="0" align="absmiddle"'.
													' alt="'.$aConfig['lang'][$sModule][$sActionDo].'"> '.
													$aConfig['lang'][$sModule][$sActionDo].
													'</a>&nbsp;&nbsp;';
							}
						break;
						
						case 'go_back':
							$aGet = array();
							$aOmit = array();
							if (!empty($this->aFooterParams[$sActionDo][0])) {
								foreach ($this->aFooterParams[$sActionDo][0] as $sParam => $mValue) {
									$aGet[$sParam] = $mValue;
								}
							}
							if (!empty($this->aFooterParams[$sActionDo][1])) {
								foreach ($this->aFooterParams[$sActionDo][1] as $sParam) {
									$aOmit[] = $sParam;
								}
							}
							$sHtml .= '<a href="'.phpSelf($aGet, $aOmit).'"'.
										' title="'.$aConfig['lang'][$sModule][$sActionDo].'">'.
										'<img src="gfx/icons/'.$sActionDo.'_ico.gif" border="0" align="absmiddle"'.
										' alt="'.$aConfig['lang'][$sModule][$sActionDo].'"> '.
										$aConfig['lang'][$sModule][$sActionDo].
										'</a>&nbsp;&nbsp;';
						break;
					}
				}
			}
			else {
				$sHtml .= '&nbsp;';
			}
			if($this->aHeader['count_checkboxes']){
					$sHtml .='
					<script type="text/javascript">
		// <![CDATA[
				$(document).ready(function(){
					$(".viewDeleteCheckbox,#check_all,#toggleCheckAll").click(function(){
						var chcnt=0;
						
						var allHTMLTags=document.getElementsByTagName("*");

						for (i=0; i<allHTMLTags.length; i++) {
							if (allHTMLTags[i].className==\'viewDeleteCheckbox\') {
							if(allHTMLTags[i].checked)
								chcnt+=parseInt(allHTMLTags[i].title);
							}	
						}
						$(".chceckboxesCountInfo").html("Zaznaczono: "+chcnt);
					});
				});
				// ]]>
	</script>
					';
				}
			if(!empty($this->aFooterGroupActions)){
				$sHtml .="
				<script type=\"text/javascript\">
				";
				$sHtml .="var grchg_q = new Array(); 
					 var grchg_err = new Array(); 
					 ";
				$aFooterSelect=array(array('value'=>'','label'=>''));
				foreach($this->aFooterGroupActions as $sAction){
					if(!empty($sAction)){
						$sHtml .="grchg_q['".$sAction."']='".$aConfig['lang'][$sModule][$sAction.'_q']."';
											grchg_err['".$sAction."']='".$aConfig['lang'][$sModule][$sAction.'_err']."';
						";
					}
					$aFooterSelect[]=array('value'=>$sAction,'label'=>$aConfig['lang'][$sModule][$sAction]);
				}
				$sHtml .="
					function GroupActionChange(oObj){
						if(oObj.value != ''){
							if (Checked(document.forms.".$this->sName.", 'delete')) {
								if (confirm(grchg_q[oObj.value])) { 
									ChangeObjValue('do', oObj.value);
									document.forms.".$this->sName.".submit();
									oObj.selectedIndex = 0;
									return false;
								} else { 
									oObj.selectedIndex = 0;
									return false; 
								}
							} else {
								alert(grchg_err[oObj.value]);
								oObj.selectedIndex = 0; 
								return false;
							}
						}
					}
				</script>
				";
				$sHtml .= '<label for="v_group_change">Zaznaczone</label>';
				$sHtml .= '&nbsp;'.Form::GetSelect('v_group_change', array('id'=>'v_group_change_sel', 'onchange'=>'GroupActionChange(this);'), $aFooterSelect, '').'&nbsp;&nbsp;';
			}
			$bSendIsInside=true;
			if(isset($this->aHeader['send_button']) && $this->aHeader['send_button'] != false){
				$sHtml .= '&nbsp;'.Form::GetInputButton('send', $aConfig['lang'][$sModule]['send_button_'.$this->aHeader['send_button']],array('onclick'=>'ChangeObjValue(\'do\', \''.$this->aHeader['send_button'].'\');'));
			}
			if(isset($this->aHeader['send_button2']) && $this->aHeader['send_button2'] != false){
				$sHtml .= '&nbsp;'.Form::GetInputButton('sendB2', $aConfig['lang'][$sModule]['send_button_'.$this->aHeader['send_button2']],array('onclick'=>'ChangeObjValue(\'do\', \''.$this->aHeader['send_button2'].'\');'));
			}
      if(isset($this->aHeader['send_button3']) && $this->aHeader['send_button3'] != false){
				$sHtml .= '&nbsp;'.Form::GetInputButton('sendB3', $aConfig['lang'][$sModule]['send_button_'.$this->aHeader['send_button3']],array('onclick'=>'ChangeObjValue(\'do\', \''.$this->aHeader['send_button3'].'\');'));
			}
			$sHtml .= '</td>';
			if (isset($this->aRecordsFooter[1]) && !empty($this->aRecordsFooter[1])) {
				$sHtml .= '<td class="second">';
				foreach ($this->aRecordsFooter[1] as $sActionDo) {
					if (isset($this->aFooterParams[$sActionDo][2])) {
						$sIco = $this->aFooterParams[$sActionDo][2];
					}
					else {
						$sIco = $sActionDo;
					}
					// dodanie linku z akcja
					$aGet = array('do'=>$sActionDo);
					$aOmit = array();
					if (!empty($this->aFooterParams[$sActionDo][0])) {
						foreach ($this->aFooterParams[$sActionDo][0] as $sParam => $mValue) {
							$aGet[$sParam] = $mValue;
						}
					}
					if (!empty($this->aFooterParams[$sActionDo][1])) {
						foreach ($this->aFooterParams[$sActionDo][1] as $sParam) {
							$aOmit[] = $sParam;
						}
					}
					if ($sActionDo == 'sort') {
						if (!empty($this->aRecords) && count($this->aRecords) > 1) {
							$aGet['back'] = base64_encode(phpSelf($aGet, array('do', 'id')));
							$sHtml .= '<a href="'.phpSelf($aGet, $aOmit).'"'.
										' title="'.$aConfig['lang'][$sModule][$sActionDo].'">'.
										'<img src="gfx/icons/'.$sIco.'_ico.gif" border="0" align="absmiddle"'.
										' alt="'.$aConfig['lang'][$sModule][$sActionDo].'"> '.
										$aConfig['lang'][$sModule][$sActionDo].
										'</a>&nbsp;&nbsp;';
						}
					}
					else {
						$sHtml .= '<a href="'.phpSelf($aGet, $aOmit).'"'.
										' title="'.$aConfig['lang'][$sModule][$sActionDo].'">'.
										'<img src="gfx/icons/'.$sIco.'_ico.gif" border="0" align="absmiddle"'.
										' alt="'.$aConfig['lang'][$sModule][$sActionDo].'"> '.
										$aConfig['lang'][$sModule][$sActionDo].
										'</a>&nbsp;&nbsp;';
					}
				}
				$sHtml .= '</td>';
			}
			$sHtml .= '</tr></table>';
		}
    
		if (!isset($this->aHeader['per_page']) || $this->aHeader['per_page']) {
      if (!isset($this->aHeader['items_per_page'])) {
        $aPerPage = array (
          array('value'=>5, 'name'=>5),
          array('value'=>10, 'name'=>10),
          array('value'=>20, 'name'=>20),
          array('value'=>50, 'name'=>50),
          array('value'=>100, 'name'=>100),
          array('value'=>200, 'name'=>200)
        );

        $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
      } else {
        $iPerPage = $this->aHeader['items_per_page'];
        $aPerPage = array();
      }
      
			if (!empty($this->aRecords)) {
				$sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="pagerTable">'.
									'<tr>';
				if (!empty($this->aPagerLinks['all'])) {
					$sHtml .= '<td class="first">'.
										$aConfig['lang']['pager']['pages'].
										$this->aPagerLinks['all'].
										'</td>';
				}
				
				if (!empty($this->aRecords) && !empty($aPerPage)) {
					$sHtml .= 	'<td class="second">'.
											$aConfig['lang']['pager']['per_page'].' '.Form::GetSelect('per_page', array('onchange'=>'this.form.submit();'), $aPerPage, $iPerPage).
											'</td>';
				}
				$sHtml .= '</tr>'.
									'</table>';
			}
		}
		
		$sHtml .= '</td></tr>';
		if(!$bSendIsInside){
			if(isset($this->aHeader['send_button']) && $this->aHeader['send_button'] != false){
				$sHtml .= '<tr><td>'.Form::GetInputButton('send', $aConfig['lang'][$sModule]['send_button_'.$this->aHeader['send_button']]).'</td></tr>';
			}
		}
	if (!isset($this->aHeader['form']) || $this->aHeader['form']) {
			$sHtml .= Form::GetFooter();
		}
		$sHtml .= '</table>';
		return $sHtml;
	} // end of Show() function
} // end of class EditableView
?>