<?php
/**
 * Klasa View do obslugi widoków z bazy danych w postaci tabelki HTML
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie podstawowej klasy Form 
include_once('Form/Form.class.php');

class View
{

    // nazwa tabeli i formularza
    var $sName;

    // tablica zawierajaca dane dla naglowka
    var $aHeader;

    // tabela zawierajaca ustawienia tabeli
    var $aViewAttribs;

    // tablica zawierajaca liste filtrow
    var $aFilters;

    // tablica zawierajaca naglowek rekordow
    var $aRecordsHeader;

    // tablica zawierajaca liste rekordow
    var $aRecords;

    // tablica z ustawieniami kolumn rekordkow
    var $aColSettings;

    // tablica zawierajaca liste stron rekordow
    var $aSubRecords;

    // tablica z ustawieniami kolumn stron rekordkow
    var $aSubColSettings;

    // prefix strony, akapitu, itp...
    var $sItemPrefix;

    // tablica zwierajaca ustawienia dla stopki (przyciski 'zaznacz wszystkie', itp.)
    var $aRecordsFooter;

    // dodatkowe parametry dla linek stopki
    var $aFooterParams;

    // tablica z akcjami grupowymi w select w stopce
    var $aFooterGroupActions;

    /**
     *
     * @var string
     */
    private $sSql;

    /**
     *
     * @var array
     */
    private $aSelectCols;

    /**
     *
     * @var omniaCMS\lib\View\SingleView
     */
    private $oParseView;

    /**
     *
     * @var string
     */
    private $sFiltersSQL;

    /**
     *
     * @var bool
     */
    private $bUseViewFilters = true;


    /**
     * Konstruktor klsy View
     *
     * @param    string $sName - nazwa tabeli
     * @param    array $aHeader - dane dla naglowka
     * @param array $aViewAttribs - tablica asocjacyjna zawierajaca atrybuty formularza.
     * Dostepne atrybuty:
     *  - action                        URI
     *    - method                        (GET|POST) domyslnie POST
     *    - enctype                        domyslnie: "application/x-www-form-urlencoded"
     *    - accept                        lista typow MIME uploadowanych plikow
     *    - onsubmit                    zdarzenie: formularz zostal wyslany
     *    - onreset                        zdarzenie: formularz zostal zresetowany
     *    - accept-charset        lista akceptowanych stron kodowych
     *
     * @return void
     */
    function View($sName, $aHeader, $aViewAttribs = array())
    {
        global $aConfig;

        if (!isset($aHeader['checkboxes'])) {
            $aHeader['checkboxes'] = true;
        }
        $this->sName = $sName;
        $this->aHeader = $aHeader;
        $this->aViewAttribs = $aViewAttribs;
        $this->aFilters = array();
        $this->aRecordsHeader = array();
        $this->aRecords = array();
        $this->aColSettings = array();
        $this->aRecordsFooter = array();
        $this->aFooterParams = array();
        $this->aFooterGroupActions = array();
    } // end of Form() method


    /**
     * Metoda dodaje dane naglowka rekordow
     *
     * @param    array $aHeader - dane naglowka
     * @return    void
     */
    function AddRecordsHeader(&$aHeader)
    {
        $this->aRecordsHeader = $aHeader;
    } // end of AddRecordsHeader()


    /**
     * Metoda dodaje rekordy
     *
     * @param    array $aRecords - rekordy
     * @return    void
     */
    function AddRecords(&$aRecords, $aColSettings = array())
    {
        $aColSettings['disabled']['show'] = false;
        $this->aRecords = $aRecords;
        $this->aColSettings = $aColSettings;
    } // end of AddRecords()


    /**
     * Metoda dodaje strony rekordow
     *
     * @param    array $aSubRecords - strony
     * @return    void
     */
    function AddSubRecords(&$aRecords, &$aColSettings, $sItemPrefix = '')
    {
        $aColSettings['disabled']['show'] = false;
        $this->aSubRecords = $aRecords;
        $this->aSubColSettings = $aColSettings;
        $this->sItemPrefix = $sItemPrefix;
    } // end of AddSubRecords()

    /**
     *
     * @param array $aPost - kolumna filtra zamiast . powinna zawierać |,
     *  . to znak zarezerwowany w nazwie pola formularza
     * @return string
     */
    private function getFiltersSQLByPost($aPost)
    {
        $sSql = '';

        $sRegexp = '/^f_(.*)/';
        foreach ($aPost as $sName => $sValue) {
            $aMatches = array();
            if ($sValue !== '') {
                preg_match($sRegexp, $sName, $aMatches);
                if (isset($aMatches[1])) {
                    $sCol = str_replace('|', '.', $aMatches[1]);
                    $sSql .= ' AND ' . $sCol . ' = "' . $sValue . '"';
                }
            }
        }
        return $sSql;
    }

    /**
     *
     * @param array $aSearchCols
     * @param string $sWhereVal
     * @return string
     */
    private function getWhereSQLByPost($aSearchCols, $sWhereVal)
    {
        $sWhereSQL = array();
        $sWhereSQL = ' AND (';
        foreach ($aSearchCols as $sCol) {
            $sWhereSQL .= $sCol . ' = \'' . $sWhereVal . '\' OR ';
        }
        $iLen = mb_strlen($sWhereSQL, 'UTF-8');
        $sWhereSQL = mb_substr($sWhereSQL, 0, $iLen - 3, 'UTF-8');
        $sWhereSQL .= ' ) ';
        return $sWhereSQL;
    }


    /**
     *
     * @param omniaCMS\lib\View\SingleView $oParseView
     * @param string $sSql - zmienne %cols, %filters
     * @param array $aSelectCols
     * @param string $aSearchCols
     * @param string $sFiltersSQL
     * @param string $sGroupBySQL
     * @param string $sDefaultSort
     */
    public function setSqlListView(omniaCMS\lib\View\SingleView $oParseView, $sSql, $aSelectCols, $aSearchCols = '', $sFiltersSQL = '', $sGroupBySQL = '', $sDefaultSort = '')
    {
        global $aConfig;

        $this->sSql = $sSql;
        $this->aSelectCols = $aSelectCols;
        $this->oParseView = $oParseView;

        if ($sFiltersSQL != '') {
            $this->sFiltersSQL = $sFiltersSQL;
        } else {
            $this->sFiltersSQL = $this->getFiltersSQLByPost($_POST);
        }

        if (!empty($aSearchCols) && isset($_POST['search']) && $_POST['search'] != '') {
            $this->sFiltersSQL .= $this->getWhereSQLByPost($aSearchCols, $_POST['search']);
        }

        $iRowCount = $this->getCountSQL();
//    if ($this->bUseViewFilters === false) {
//      $this->sFiltersSQL = '';
//    }
        if ($iRowCount > 0) {
            // dodanie Pagera do widoku
            $iCurrentPage = $this->AddPager($iRowCount);
            $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
            $iStartFrom = ($iCurrentPage - 1) * $iPerPage;
            $sSortSQL = $this->getSQLOrderBy($_GET['sort'], $sDefaultSort);
            $sSql = $this->getSQL($aSelectCols, $this->sFiltersSQL, $sGroupBySQL, $sSortSQL, $iStartFrom, $iPerPage);
            $aRows = Common::GetAll($sSql);
            if (!empty($aRows)) {
                foreach ($aRows as $iKey => $aRow) {
                    $aRows[$iKey] = $oParseView->parseViewItem($aRow);
                }
                $this->AddRecords($aRows);
            }
        }
    }

    /**
     *
     * @param array $aColSettings
     */
    public function setActionColSettings(array $aColSettings)
    {
        $this->aColSettings = $aColSettings;
    }

    /**
     */
    public function getCountSQL()
    {

        $sFirsCol = $this->aSelectCols[0];
        if (!stristr($sFirsCol, 'DISTINCT ')) {
            $aCols = array('COUNT( DISTINCT ' . $sFirsCol . ' ) ');
        } else {
            $aCols = array('COUNT( ' . $sFirsCol . ' ) ');
        }
        $sSqlCount = $this->getSQL($aCols, $this->sFiltersSQL);
        $iCount = Common::GetOne($sSqlCount);
        if ($iCount == 0) {
//      $this->bUseViewFilters = false;
//      $this->oParseView->resetViewState();
//      $sSqlCount = $this->getSQL($aCols);
//      $iCount = Common::GetOne($sSqlCount);
        }
        return $iCount;
    }

    /**
     *
     * @param string $sGetSort
     * @param string $sDefaultSort
     * @return string
     */
    private function getSQLOrderBy($sGetSort, $sDefaultSort)
    {

        if ((isset($sGetSort) && !empty($sGetSort)) || $sDefaultSort != '') {
            $sOrderBySQL = ' ORDER BY ' . (isset($sGetSort) && !empty($sGetSort) ? $sGetSort : $sDefaultSort);
            return $sOrderBySQL;
        }
    }

    /**
     *
     * @param string $aCols
     * @param string $sFilters
     * @param string $sGroupBySQL
     * @param string $sOrderBy
     * @param int $iPStart
     * @param int $iPerPage
     * @return string
     */
    private function getSQL($aCols, $sFilters = '', $sGroupBySQL = '', $sOrderBy = '', $iPStart = 0, $iPerPage = 0)
    {

        $sColsSQL = implode(', ', $aCols);
        $sSql = str_replace('%cols', $sColsSQL, $this->sSql);
        $sSql = str_replace('%filters', $sFilters, $sSql);
        $sSql .= $sGroupBySQL;
        $sSql .= $sOrderBy;
        if ($iPStart > 0 || $iPerPage > 0) {
            $sSql .= ' LIMIT ' . $iPStart . ',' . $iPerPage;
        }
        return $sSql;
    }

    /**
     * Metoda dodaje filtr
     *
     * @param        string $sName nazwa elementu
     * @param        array $aAttribs atrybuty elementu
     * @param        array $aOptions atrybuty opcji elementu
     * @param        mixed $mSelected list wartosci opcji ktore maja byc zaznaczone
     * @return        void
     */
    function AddFilter($sName, $sLabel, $aOptions, $mSelected = '', $sType = 'select', $hidden = false)
    {
        $this->aFilters[$sName] = array($sName, $sLabel, $aOptions, $mSelected, $sType, $hidden);
    } // end of AddFilter() function

    function AddFooterGroupActions($aActions)
    {
        $this->aFooterGroupActions = $aActions;
    }

    /**
     * Metoda dodaje Pager
     *
     * @param    integer $iRowCount - liczba wierszy
     * @return    integer                                            - numer aktualnej strony
     */
    function AddPager($iRowCount, $bSecond = false)
    {
        global $aConfig;

        // dolaczenie klasy Pager
        include_once('Pager/Pager.php');

        // tablica konfiguracyjna dla Pagera
        $aPagerParams = array(
            'totalItems' => $iRowCount,
            'perPage' => isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'],
            'delta' => 3,
            'mode' => 'Sliding',
            'currentPage' => isset($_GET['page']) && !empty($_GET['page']) ? $_GET['page'] : 1,
            'urlVar' => 'page',
            'altPage' => $aConfig['lang']['pager']['page'],
            'altFirst' => $aConfig['lang']['pager']['first_page'],
            'altPrev' => $aConfig['lang']['pager']['previous_page'],
            'altNext' => $aConfig['lang']['pager']['next_page'],
            'altLast' => $aConfig['lang']['pager']['last_page'],
            'firstPagePre' => '',
            'firstPageText' => '&laquo;&laquo;',
            'firstPagePost' => '',
            'lastPagePre' => '',
            'lastPageText' => '&raquo;&raquo;',
            'lastPagePost' => '',
            'prevImg' => '&laquo;',
            'nextImg' => '&raquo;',
            'separator' => '|',
            'spacesBeforeSeparator' => 1,
            'spacesAfterSeparator' => 1,
            'linkClass' => 'page',
            'curPageLinkClassName' => 'currentPage',
            //'onclickFunc'	=> 'MenuNavigate(\'%s\'); return false;',
            'excludeVars' => ($bSecond ? array('reset', 'id') : array('reset', 'do', 'id'))
        );
        $pPager = &Pager::factory($aPagerParams);
        $this->aPagerLinks = $pPager->getLinks();
        return $pPager->GetCurrentPageID();
    } // end of AddPager() function


    /**
     * Metoda dodaje dane stopki rekordow
     *
     * @param    array $aFooter - dane naglowka
     * @return    void
     */
    function AddRecordsFooter(&$aFooter, $aParams = array())
    {
        $this->aRecordsFooter = $aFooter;
        $this->aFooterParams = $aParams;
    } // end of AddRecordsFooter()


    /**
     * Metoda wyswietla widok
     *
     * @return    string                                    - widok
     */
    function Show()
    {
        global $aConfig;

        $iColspan = 0;
        $aCheckboxAttribs = array();

        if (isset($_GET['file']) && !empty($_GET['file'])) {
            $sModule = $_GET['file'];
        } else {
            $sModule = $_GET['module'];
            if (isset($_GET['action'])) {
                $sModule .= '_' . $_GET['action'];
            }
        }
        if ($this->aHeader['second'] == true) {
            $sModule .= '_' . $_GET['do'];
        }

        // utworzenie naglowka formularza
        $aFAttribs = array(
            'method' => 'post',
            'action' => !isset($this->aHeader['action']) ? phpSelf(array(), array('do', 'id', 'pno'), false) : $this->aHeader['action']
        );

        $aSortingChange = array(
            'ASC' => 'DESC',
            'DESC' => 'ASC'
        );

        if ($this->aHeader['refresh']) {
            // dodanie linku 'odswiez'
            $sRefresh = '<a href="' . (!isset($this->aHeader['refresh_link']) ? phpSelf(array('reset' => 1)) : $this->aHeader['refresh_link']) . '" title="' . $aConfig['lang']['common']['refresh'] . '"><img src="gfx/icons/refresh_ico.gif" border="0" alt="' . $aConfig['lang']['common']['refresh'] . '" style="margin-right: 10px; margin-top: 1px;"></a>';
        }
        if ($this->aHeader['search']) {
            // dodanie wyszukiwarki
            if (isset($_GET['search'])) {
                $_POST['search'] = $_GET['search'];
            }
            $sSearch .= Form::GetText('search', $_POST['search'], $this->aHeader['bigSearch'] ? array('style' => 'font-size:16px; padding:2px; width:300px;') : array());
            $sSearchButton = '<a href="javascript:void(0);" onclick="' . ($this->aHeader['noreset'] == 1 ? '' : 'document.forms.' . $this->sName . '.action += \'&reset=3\'; ') . 'document.forms.' . $this->sName . '.submit(); return false;" title="' . $aConfig['lang']['common']['search'] . '"><img src="gfx/icons/search_ico.gif" border="0" alt="' . $aConfig['lang']['common']['search'] . '" style="margin-left: 2px; margin-top: 1px;"></a>';

            if ($this->aHeader['specialOption']) {//dodatkowy checkbox np przy wyszukiwaniu zamówień wyszukaj wewnątrz

                if (isset($_GET['f_specialSearchOption']))
                    $_POST['f_specialSearchOption'] = $_GET['specialSearchOption'];

                $options = [
                    [
                        'label' => '1',
                        'value' => '1'
                    ],
                    [
                        'label' => '2',
                        'value' => '2'
                    ]
                ];

                $selectedAnyCheckbox = (!isset($_POST['f_search_invoice_number']) && !isset($_POST['f_search_email']) && !isset($_POST['f_search_transport_number']));
                $sSpecial =
                    '<span id="filters_search_by" '.(isset($_POST['f_specialSearchOption']) && $_POST['f_specialSearchOption'] == '1' ? 'style="display: none;"': '').'><span style="color:#fff;">Szukaj wg.:</span>'.
                    Form::GetCheckbox('f_search_order_number', ['style' => 'color:#fff', 'class' => 'search_by'], '', isset($_POST['f_search_order_number']) || (true === $selectedAnyCheckbox) ? true : false) .
                    Form::GetLabel('f_search_order_number', _('Nr zamówienia'), false, 0, '', ['style' => 'color:#fff']).

                    Form::GetCheckbox('f_search_invoice_number', ['style' => 'color:#fff', 'class' => 'search_by'], '', isset($_POST['f_search_invoice_number']) ? true : false) .
                    Form::GetLabel('f_search_invoice_number', _('Nr FV'), false, 0, '', ['style' => 'color:#fff']).

                    Form::GetCheckbox('f_search_email', ['style' => 'color:#fff', 'class' => 'search_by'], '', isset($_POST['f_search_email']) ? true : false) .
                    Form::GetLabel('f_search_email', _('Email'), false, 0, '', ['style' => 'color:#fff']).

                    Form::GetCheckbox('f_search_transport_number', ['style' => 'color:#fff', 'class' => 'search_by'], '', isset($_POST['f_search_transport_number']) ? true : false) .
                    Form::GetLabel('f_search_transport_number', _('List przewozowy'), false, 0, '', ['style' => 'color:#fff']).

                    Form::GetCheckbox('f_seller_streamsoft_id', ['style' => 'color:#fff', 'class' => 'search_by'], '', isset($_POST['f_seller_streamsoft_id']) ? true : false) .
                    Form::GetLabel('f_seller_streamsoft_id', _('Handlowiec'), false, 0, '', ['style' => 'color:#fff']).
                    '</span> '.
                    Form::GetCheckbox('f_specialSearchOption', array('onchange' => 'togleSpecialOpt(this.checked)', 'style' => 'color:#fff'), '', $_POST['f_specialSearchOption'] ? true : false) .'<label for="f_specialSearchOption" style="color:#fff; font-size: 16px;">' . $this->aHeader['specialOptionName'] . '</label> ' .
                    '
				<script>
                $(\'input.search_by\').on(\'change\', function() {
                    $(\'input.search_by\').not(this).prop(\'checked\', false);  
                });
				function togleSpecialOpt(stat) {
					var option="none";
					if(stat==true) option="";
					for(var i=1;i<6;++i)
					if(document.getElementById("innerFilterLine"+i))
						 document.getElementById("innerFilterLine"+i).style.display=option;
						 if (option == "none") {
						    document.getElementById("filters_search_by").style.display="";
						 } else {
						    document.getElementById("filters_search_by").style.display="none";
						 }
					}
				</script>';

            }
        }

        $sHtml = '<table';
        foreach ($this->aViewAttribs as $sName => $sAttrib) {
            $sHtml .= ' ' . $sName . '="' . $sAttrib . '"';
        }
        $sHtml .= '>';
        $sHtml .= '
		<tr>
			<td class="tableMargin">';
        if (!isset($this->aHeader['form']) || $this->aHeader['form']) {
            $sHtml .= Form::GetHeader($this->sName, $aFAttribs);
            if ($this->aHeader['second'] != true) {
                $sHtml .= Form::GetHidden('do');
            }
        }
        if (isset($this->aHeader['header'])) {
            $sHtml .= '
        <script>
        $(document).ready(function(){
          $("a.ViewTooltip").cluetip({leftOffset: "0px", width: "350px", height: "60px", local:true, showTitle: false});
        });
        </script>
        
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
						<tr>
							<td width="1" height="18" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="18" class="brownBg">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><span class="tableHeaderBg"><strong>' . encodeHeaderString($this->aHeader['header']) . '</strong></span></td>
										<td style="text-align: right; padding-right: 10px;">
											<table border="0" cellspacing="0" cellpadding="0" align="right">
												<tr>
													<td style="padding-right: 10px;">' . $sSpecial . '</td>
													<td>' . $sRefresh . '</td>
													<td align="right">' . $sSearch . '</td>
													<td align="right">' . (!isset($this->aHeader['searchInfo']) ? '' : '<a href="#ViewTooltip_1" rel="#ViewTooltip_1" class="ViewTooltip">?</a>') . $sSearchButton . '</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
							<td width="1" height="18" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
						<tr>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
					</table>
          <span id="ViewTooltip_1" style="display: none;">
            ' . $this->aHeader['searchInfo'] . '
          </span>
';
        }
        if (!empty($this->aFilters)) {
            if (count($this->aFilters) > 1) {
                $sOnChange = 'void(0);';
                //$sAppendHtml = Form::GetInputButton('filters_submit', $aConfig['lang']['filters']['submit'], array('onclick'=>'this.form.action += \'&reset=3\'; this.form.submit();'), 'button').'&nbsp;&nbsp;';
                $sAppendHtml = Form::GetInputButton('submit_filters', $aConfig['lang']['filters']['submit'], array('onclick' => 'this.form.action += \'&reset=3\';')) . '&nbsp;&nbsp;';
            } else {
                $sOnChange = 'this.form.action += \'&reset=3\'; this.form.submit();';
                $sAppendHtml = '';
            }
            $sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="filtersTable">';
            $sHtml .= '<tr>';
            $sHtml .= '<td>';
            foreach ($this->aFilters as $aFilter) {
                if ($aFilter[4] != 'html' && $aFilter[4] != 'date') {
                    $sHtml .= '<label for="' . $aFilter[0] . '" style="cursor: ' . $aConfig['styles']['cursor'] . ';">' . $aFilter[1] . '</label>';
                }
                $sHtml .= '&nbsp;';
                if ($aFilter[4] == 'text') {
                    $input = Form::GetText($aFilter[0], $aFilter[3], array('id' => $aFilter[0])) . '&nbsp;';
                    if (isset($aFilter[5]) && $aFilter[5] == true) {
                        $input = '<span style="display:none">' . $input . '</span>';
                    }
                    $sHtml .= $input;
                } elseif ($aFilter[4] == 'date') {
                    $sHtml .= '<div style="float:left;"><div style="float:left;cursor: ' . $aConfig['styles']['cursor'] . '; font-size:11px; margin:2px; margin-top:4px; ">' . $aFilter[1] . '</div>';
                    $sHtml .= '<div style="float:left">' . Form::GetText($aFilter[0], $aFilter[3], array('id' => $aFilter[0], 'style' => 'width:72px;', 'clear' => (isset($aFilter[2]['clear']) ? true : false)), 'date') . '</div></div>';
                } elseif ($aFilter[4] == 'html') {
                    $sHtml .= $aFilter[2];
                } else/*if(!in_array($aFilter[0], array('f_author', 'f_publisher')))*/ {
                    $sHtml .= Form::GetSelect($aFilter[0], array('id' => $aFilter[0], 'onchange' => $sOnChange, 'style' => ($aFilter[0] == 'f_category' ? 'width:250px;' : '')), $aFilter[2], $aFilter[3]) . '&nbsp;';
                }
            }

            $sHtml .= $sAppendHtml;
            $sHtml .= '</td>';
            $sHtml .= '</tr>';
            $sHtml .= '</table>';
        }

        $sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="recordsTable">';
        if (!empty($this->aRecordsHeader) && !empty($this->aRecords)) {
            $sHtml .= '<tr>';
            if (isset($this->aSubRecords) && !empty($this->aSubRecords)) {
                $sHtml .= '<th width="20">&nbsp;</th>';
                $iColspan++;
            }
            foreach ($this->aRecordsHeader as $aColumn) {
                $sHtml .= '<th' . (isset($aColumn['width']) ? ' width="' . $aColumn['width'] . '"' : '') . '>';
                if ($aColumn['sortable']) {
                    if ($this->aHeader['second'] == true) {
                        $aGet['do'] = $_GET['do'];
                    }
                    $aGet['sort'] = $aColumn['db_field'];
                    if (isset($_GET['sort']) && $_GET['sort'] == $aColumn['db_field']) {
                        $aGet['order'] = $aSortingChange[$_GET['order']];
                    } else {
                        $aGet['order'] = 'ASC';
                    }
                    // resetowanie strony
                    $aGet['reset'] = 3;
                    $sHtml .= '<a href="' . ($this->aHeader['action'] != '' ? $this->aHeader['action'] . '&' . http_build_query($aGet) : phpSelf($aGet)) . '">' . $aColumn['content'] . '</a>';
                } else {
                    $sHtml .= $aColumn['content'];
                }
                $sHtml .= '</th>';
                $iColspan++;
            }
            $sHtml .= '</tr>';

            // rekordy
            foreach ($this->aRecords as $aRecord) {
                $sHtml .= '<tr class="row" onmouseover="this.className=\'rowHov\';" onmouseout="this.className=\'row\';">';
                if (isset($this->aSubRecords) && !empty($this->aSubRecords)) {
                    $sHtml .= '<td class="pg">';
                    if (isset($_SESSION['_viewState'][$sModule]['exp']) &&
                        in_array($aRecord['id'], $_SESSION['_viewState'][$sModule]['exp']) &&
                        !empty($this->aSubRecords[$aRecord['id']])
                    ) {
                        $sHtml .= '<a href="' . phpSelf(array('nar' => $aRecord['id'])) . '" title="' . $aConfig['lang'][$sModule]['hide_items'] . '"><img src="gfx/icons/tree_minus.gif" width="18" alt="' . $aConfig['lang'][$sModule]['hide_items'] . '" border="0"></a>';
                    } elseif (isset($this->aSubRecords[$aRecord['id']])) {
                        $sHtml .= '<a href="' . phpSelf(array('exp' => $aRecord['id'])) . '" return false;" title="' . $aConfig['lang'][$sModule]['show_items'] . '"><img src="gfx/icons/tree_plus.gif" width="18"  alt="' . $aConfig['lang'][$sModule]['show_items'] . '" border="0"></a>';
                    } else {
                        $sHtml .= '<img src="gfx/pixel.gif" width="18" alt="" border="0">';
                    }
                    $sHtml .= '</td>';
                }
                if (!isset($this->aHeader['checkboxes']) || $this->aHeader['checkboxes']) {
                    if (isset($aRecord['disabled']) && in_array('delete', $aRecord['disabled'])) {
                        // checkbox wylaczony - brak mozliwosci usuniecia rekordu
                        $aCheckboxAttribs['disabled'] = 'disabled';
                    } else {
                        $aCheckboxAttribs = array();
                    }
                    $aCheckboxAttribs['class'] = 'viewDeleteCheckbox';
                    $sHtml .= '<td class="check">' . Form::GetCheckBox('delete[' . $aRecord['id'] . ']', $aCheckboxAttribs) . '</td>';
                }
                foreach ($aRecord as $sName => $sContent) {
                    if ($sName != 'disabled' && $sName != 'hidden') {
                        if (is_array($sContent) || trim($sContent) == '') {
                            $sContent = '&nbsp';
                        }
                        if (!isset($this->aColSettings[$sName]['show']) ||
                            (isset($this->aColSettings[$sName]['show']) && $this->aColSettings[$sName]['show'])
                        ) {

                            $sHtml .= '<td' . (!empty($_SESSION['_modified'][$this->sName]) && in_array($aRecord['id'], $_SESSION['_modified'][$this->sName]) ? ' class="modified"' : '') . '>';
                            if (isset($this->aColSettings[$sName]['use_lang']) && isset($aConfig['lang'][$sModule][$sContent])) {
                                $sContent = $aConfig['lang'][$sModule][$sContent];
                            }

                            if (isset($this->aColSettings[$sName]['filter'])) {
                                $aFilter = explode(':', $this->aColSettings[$sName]['filter']);
                                $aParams = array($sContent);
                                if (count($aFilter) > 1) {
                                    for ($i = 1; $i < count($aFilter); $i++) {
                                        $aParams[] = $aFilter[$i];
                                    }
                                }
                                $sContent = call_user_func_array($aFilter[0], $aParams);
                            }

                            if (isset($this->aColSettings[$sName]['link'])) {
                                if (!empty($this->aColSettings[$sName]['link']) &&
                                    (!isset($aRecord['disabled']) ||
                                        !isset($aRecord['hidden']) ||
                                        !in_array($sName, $aRecord['disabled']) ||
                                        !in_array($sName, $aRecord['hidden'])
                                    )
                                ) {
                                    if (isset($aRecord['action']['links'][$sName])) {
                                        $sLink = $aRecord['action']['links'][$sName];
                                    } else {
                                        // zamiana w linku {zmienna} na $aRecord['zmienna']
                                        $sLink = preg_replace('/\{(\w+)\}/e', "\$aRecord['\\1']", $this->aColSettings[$sName]['link']);
                                    }
                                    $sHtml .= '<a href="' . $sLink . '" class="' . (($aRecord['id'] == $_SESSION['_last_modified'][$this->sName]) ? 'last_modified' : 'link') . '">' . $sContent . '</a>';
                                } else {
                                    $sHtml .= $sContent;
                                }
                            } else {
                                $sHtml .= $sContent;
                            }
                            $sHtml .= '</td>';
                        }
                    }
                }
                // kolumna z przyciskami akcji
                if (isset($this->aColSettings['action']['actions']) && !empty($this->aColSettings['action']['actions'])) {
                    $sHtml .= '<td' . (!empty($_SESSION['_modified'][$this->sName]) && in_array($aRecord['id'], $_SESSION['_modified'][$this->sName]) ? ' class="modified"' : '') . '>';
                    foreach ($this->aColSettings['action']['actions'] as $sActionDo) {
                        if (isset($this->aColSettings['action']['icon'][$sActionDo])) {
                            $sIcon = $this->aColSettings['action']['icon'][$sActionDo];
                        } else {
                            $sIcon = $sActionDo;
                        }
                        if (isset($this->aColSettings['action']['lang'][$sActionDo])) {
                            $sTitle = $this->aColSettings['action']['lang'][$sActionDo];
                        } elseif (isset($aConfig['lang'][$sModule][$sActionDo])) {
                            $sTitle = $aConfig['lang'][$sModule][$sActionDo];
                        } elseif (isset($aConfig['lang']['common'][$sActionDo])) {
                            $sTitle = $aConfig['lang']['common'][$sActionDo];
                        }
                        $aGet = array('do' => $sActionDo);
                        if (isset($this->aColSettings['action']['params'][$sActionDo])) {
                            // zdefiniowano dodatkowe parametry dla przycisku
                            foreach ($this->aColSettings['action']['params'][$sActionDo] as $sParam => $sValue) {
                                // zamiana {zmienna} na $aRecord['zmienna'] w $sValue
                                $sValue = preg_replace('/^\{(\w+)\}$/e', "\$aRecord['\\1']", $sValue);
                                if (isset($aGet[$sParam])) {
                                    unset($aGet[$sParam]);
                                }
                                if ($sValue != '') {
                                    $aGet[$sParam] = $sValue;
                                }
                            }
                        }
                        if (isset($aRecord['disabled']) &&
                            in_array($sActionDo, $aRecord['disabled'])
                        ) {
                            // ten przycisk jest nieaktywny
                            $sHtml .= '<img src="gfx/icons/' . $sIcon . '_disabled_ico.gif" alt="' . $sTitle . '" border="0">' .
                                '<img src="gfx/pixel.gif" width="5" alt="" border="0">';
                        } elseif (isset($aRecord['hidden']) &&
                            in_array($sActionDo, $aRecord['hidden'])
                        ) {
                            // ten przycisk jest nieaktywny
                            $sHtml .= '';
                        } else {
                            if ($sActionDo == 'delete') {

                                if (isset($aConfig['lang'][$sModule][$sActionDo . '_q'])) {
                                    $sDeleteQuestion = $aConfig['lang'][$sModule][$sActionDo . '_q'];
                                } elseif (isset($aConfig['lang']['common'][$sActionDo . '_q'])) {
                                    $sDeleteQuestion = $aConfig['lang']['common'][$sActionDo . '_q'];
                                } else {
                                    $sDeleteQuestion = '';
                                }
                                $sHtml .= '<a href="javascript:void(0);" onclick="if (confirm(\'' . $sDeleteQuestion . '\')){MenuNavigate(\'' . phpSelf($aGet) . '\')}; return false;"';
                            } elseif ($sActionDo == 'preview') {
                                $sLink = $aConfig['common']['client_base_url_http'];
                                if (!isset($aRecord['action']['links'][$sActionDo])) {
                                    if (!empty($aGet)) {
                                        unset($aGet['do']);
                                        $sLink .= '?';
                                        foreach ($aGet as $sParam => $sValue) {
                                            $sLink .= $sParam . '=' . $sValue . '&';
                                        }
                                        $sLink = substr($sLink, 0, strlen($sLink) - 1);
                                    }
                                } else {
                                    $sLink .= $aRecord['action']['links'][$sActionDo];
                                }
                                $sHtml .= '<a href="' . $sLink . '" target="_blank"';
                            } else {
                                if ($sActionDo == 'sort') {
                                    $aGet['back'] = base64_encode(phpSelf($aGet, array('do', 'id')));
                                }
                                if (isset($aRecord['action']['links'][$sActionDo])) {
                                    $sHtml .= '<a href="' . $aRecord['action']['links'][$sActionDo] . '"';
                                } else {
                                    $sHtml .= '<a href="' . phpSelf($aGet) . '"';
                                }
                            }
                            $sAddConfirm = '';
                            if (isset($this->aColSettings['action']['confirms'][$sActionDo])) {
                                $sAddConfirm = 'onclick="return confirm(\''.$this->aColSettings['action']['confirms'][$sActionDo].'\')"';
                            }
                            if (isset($this->aColSettings['action']['target'][$sActionDo])) {
                                $sTarget = 'target="'.$this->aColSettings['action']['target'][$sActionDo].'"';
                            }
                            $sHtml .= ' title="' . $sTitle . '" '.$sAddConfirm.' '.$sTarget.'>' .
                                '<img src="gfx/icons/' . $sIcon . '_ico.gif" alt="' . $sTitle . '" border="0">' .
                                '</a>' .
                                '<img src="gfx/pixel.gif" width="5" alt="" border="0">';
                        }
                    }
                    $sHtml .= '</td>';
                }
                $sHtml .= '</tr>';

                // strony rekordu
                if (isset($this->aSubRecords[$aRecord['id']]) &&
                    !empty($this->aSubRecords[$aRecord['id']]) &&
                    isset($_SESSION['_viewState'][$sModule]['exp']) &&
                    in_array($aRecord['id'], $_SESSION['_viewState'][$sModule]['exp'])
                ) {
                    foreach ($this->aSubRecords[$aRecord['id']] as $aPage) {
                        $sHtml .= '<tr class="row" onmouseover="this.className=\'rowHov\';" onmouseout="this.className=\'row\';">';
                        $sHtml .= '<td class="page">&nbsp;</td>';
                        if (!isset($this->aHeader['checkboxes']) || $this->aHeader['checkboxes']) {
                            $sHtml .= '<td class="page">&nbsp;</td>';
                        }
                        foreach ($aRecord as $sName => $sContent) {
                            if (!isset($this->aColSettings[$sName]['show']) ||
                                (isset($this->aColSettings[$sName]['show']) && $this->aColSettings[$sName]['show'])
                            ) {

                                $sSubContent = $aPage[$sName];
                                if (key_exists($sName, $aPage)) {
                                    if ($sName == 'name') {
                                        $sHtml .= '<td class="pageName">';
                                        if ($this->sItemPrefix != '') {
                                            $sPageNo = '<span class="internalLink">' . preg_replace('/\{(\w+)\}/e', "\$aPage['\\1']", $this->sItemPrefix) . '</span>';

                                            $sSubContent = $sPageNo . (!empty($sSubContent) ? ': ' : '') . $sSubContent;
                                        }
                                        if (isset($aPage['active']) && $aPage['active'] == '1') {
                                            $sSubContent = '<b>' . $sSubContent . '</b>';
                                        }
                                    } else {
                                        $sHtml .= '<td class="page">';
                                    }

                                    if (isset($this->aSubColSettings[$sName]['filter'])) {
                                        $aSubFilter = explode(':', $this->aSubColSettings[$sName]['filter']);
                                        $aSubParams = array(&$sSubContent);
                                        if (count($aSubFilter) > 1) {
                                            for ($i = 1; $i < count($aSubFilter); $i++) {
                                                $aSubParams[] = $aSubFilter[$i];
                                            }
                                        }
                                        call_user_func_array($aSubFilter[0], $aSubParams);
                                    }

                                    if (isset($this->aSubColSettings[$sName]['link'])) {
                                        if (!empty($this->aSubColSettings[$sName]['link'])) {
                                            // zamiana w linku {zmienna} na $aPage['zmienna']
                                            $sSubLink = preg_replace('/\{(\w+)\}/e', "\$aPage['\\1']", $this->aSubColSettings[$sName]['link']);
                                            $sHtml .= '<a class="sub link" href="' . $sSubLink . '">' . stripslashes($sSubContent) . '</a>';
                                        } else {
                                            $sHtml .= '<strong>' . stripslashes($sSubContent) . '</strong>';
                                        }
                                    } else {
                                        $sHtml .= stripslashes($sSubContent);
                                    }
                                    $sHtml .= '</td>';
                                } else {
                                    $sHtml .= '<td class="page">&nbsp;</td>';
                                }
                            }
                        }
                        // kolumna z przyciskami akcji
                        if (isset($this->aSubColSettings['action']['actions']) && !empty($this->aSubColSettings['action']['actions'])) {
                            $sHtml .= '<td class="page">';
                            foreach ($this->aSubColSettings['action']['actions'] as $sActionDo) {
                                if ($sActionDo == 'empty') {
                                    $sHtml .= '<img src="gfx/pixel.gif" width="19" alt="" border="0">';
                                } else {
                                    if (isset($this->aSubColSettings['action']['icon'][$sActionDo])) {
                                        $sIcon = $this->aSubColSettings['action']['icon'][$sActionDo];
                                    } else {
                                        $sIcon = $sActionDo;
                                    }
                                    if (isset($this->aSubColSettings['action']['lang'][$sActionDo])) {
                                        $sTitle = $this->aSubColSettings['action']['lang'][$sActionDo];
                                    } else {
                                        $sTitle = $aConfig['lang'][$sModule][$sActionDo];
                                    }
                                    $aGet = array('do' => $sActionDo);
                                    if (isset($this->aSubColSettings['action']['params'][$sActionDo])) {
                                        // zdefiniowano dodatkowe parametry dla przycisku
                                        foreach ($this->aSubColSettings['action']['params'][$sActionDo] as $sParam => $sValue) {
                                            // zamiana {zmienna} na $aRecord['zmienna'] w $sValue
                                            $sValue = preg_replace('/^\{(\w+)\}$/e', "\$aPage['\\1']", $sValue);
                                            $aGet[$sParam] = $sValue;
                                        }
                                    }
                                    if (isset($aPage['disabled']) &&
                                        in_array($sActionDo, $aPage['disabled'])
                                    ) {
                                        // ten przycisk jest nieaktywny
                                        $sHtml .= '<img src="gfx/icons/' . $sIcon . '_disabled_ico.gif" alt="' . $sTitle . '" border="0">' .
                                            '<img src="gfx/pixel.gif" width="5" alt="" border="0">';
                                    } else {
                                        if ($sActionDo == 'delete_page') {
                                            $sHtml .= '<a href="javascript:void(0);" onclick="if (confirm(\'' . $aConfig['lang'][$sModule][$sActionDo . '_q'] . '\')){MenuNavigate(\'' . phpSelf($aGet) . '\')}; return false;"';
                                        } elseif ($sActionDo == 'preview_page') {
                                            $sLink = $aConfig['common']['client_base_url_http'];
                                            if (!isset($aPage['action']['links'][$sActionDo])) {
                                                if (!empty($aGet)) {
                                                    unset($aGet['do']);
                                                    $sLink .= '?';
                                                    foreach ($aGet as $sParam => $sValue) {
                                                        $sLink .= $sParam . '=' . $sValue . '&';
                                                    }
                                                    $sLink = substr($sLink, 0, strlen($sLink) - 1);
                                                }
                                            } else {
                                                $sLink .= $aPage['action']['links'][$sActionDo];
                                            }
                                            $sHtml .= '<a href="' . $sLink . '" target="_blank"';
                                        } else {
                                            $sHtml .= '<a href="' . phpSelf($aGet) . '"';
                                        }
                                        $sHtml .= ' title="' . $sTitle . '">' .
                                            '<img src="gfx/icons/' . $sIcon . '_ico.gif" alt="' . $sTitle . '" border="0">' .
                                            '</a>' .
                                            '<img src="gfx/pixel.gif" width="5" alt="" border="0">';
                                    }
                                }
                            }
                            $sHtml .= '</td>';
                        }
                        $sHtml .= '</tr>';
                    }
                    $sHtml .= '<tr><td colspan="' . $iColspan . '" class="lastPage"><img src="gfx/pixel.gif" width="1" height="1" alt="" border="0"></td></tr>';
                }
            }
        } else {
            $sHtml .= '<tr class="row">' .
                '<td class="message">' . (!isset($aConfig['lang'][$sModule]['no_items']) ? $aConfig['lang']['common']['no_items'] : $aConfig['lang'][$sModule]['no_items']) . '</td>' .
                '</tr>';
        }
        $sHtml .= '</table>';

        if (!empty($this->aRecordsFooter)) {
            $sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="footerTable">' .
                '<tr>' .
                '<td class="first">';
            if (!empty($this->aRecordsFooter[0])) {
                foreach ($this->aRecordsFooter[0] as $sActionDo) {
                    switch ($sActionDo) {
                        case 'check_all':
                            if (!empty($this->aRecords)) {
                                // dodanie checkboxa zaznaczajacego wszystkie checkboxy
                                $sHtml .= Form::GetCheckBox('check_all', array('onclick' => 'ToggleCheckboxes(document.forms.' . $this->sName . ', \'delete\', true);')) .
                                    '&nbsp;<a id="toggleCheckAll" href="javascript: void(0);" onclick="ToggleCheckboxes(document.forms.' . $this->sName . ', \'delete\', false);"' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;' .
                                    ($this->aHeader['count_checkboxes'] ? '|&nbsp;<span id="chceckboxesCountInfo">Zaznaczono: 0</span>&nbsp;|&nbsp;&nbsp;' : '');
                            }
                            break;

                        case 'delete_all':
                            if (!empty($this->aRecords)) {
                                // dodanie linku usuwajacego wszystkie zaznaczone rekordy
                                $sHtml .= '<a href="javascript:void(0);"' .
                                    ' onclick="if (Checked(document.forms.' . $this->sName . ', \'delete\')){' .
                                    'if (confirm(\'' . $aConfig['lang'][$sModule][$sActionDo . '_q'] . '\')) { ChangeObjValue(\'do\', \'delete\'); document.forms.' . $this->sName . '.submit(); return false;} else { return false; }}' .
                                    'else {' .
                                    'alert(\'' . $aConfig['lang'][$sModule][$sActionDo . '_err'] . '\'); return false;}"' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    '<img src="gfx/icons/' . $sActionDo . '_ico.gif" border="0" align="absmiddle"' .
                                    ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;';
                            }
                            break;

                        case 'place_on_main':
                            if (!empty($this->aRecords)) {
                                // dodanie linku ustawiającego wyświetlanie produktów w boxie

                                $sHtml .= '<a href="javascript:void(0);"' .
                                    ' onclick="if (Checked(document.forms.' . $this->sName . ', \'delete\')){' .
                                    'if (confirm(\'' . $aConfig['lang'][$sModule][$sActionDo . '_q'] . '\')) { document.forms.' . $this->sName . '.action=\'' . phpSelf(array('do' => 'placeOnMain'), array(), false) . '\'; document.forms.' . $this->sName . '.submit(); return false;} else { return false; }}' .
                                    'else {' .
                                    'alert(\'' . $aConfig['lang'][$sModule][$sActionDo . '_err'] . '\'); return false;}"' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    '<img src="gfx/icons/' . $sActionDo . '_ico.gif" border="0" align="absmiddle"' .
                                    ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;';
                            }
                            break;

                        case 'unplace_on_main':
                            if (!empty($this->aRecords)) {
                                // dodanie linku ustawiającego wyświetlanie produktów w boxie

                                $sHtml .= '<a href="javascript:void(0);"' .
                                    ' onclick="if (Checked(document.forms.' . $this->sName . ', \'delete\')){' .
                                    'if (confirm(\'' . $aConfig['lang'][$sModule][$sActionDo . '_q'] . '\')) { document.forms.' . $this->sName . '.action=\'' . phpSelf(array('do' => 'unplaceOnMain'), array(), false) . '\'; document.forms.' . $this->sName . '.submit(); return false;} else { return false; }}' .
                                    'else {' .
                                    'alert(\'' . $aConfig['lang'][$sModule][$sActionDo . '_err'] . '\'); return false;}"' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    '<img src="gfx/icons/' . $sActionDo . '_ico.gif" border="0" align="absmiddle"' .
                                    ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;';
                            }
                            break;

                        case 'remove_all':
                            if (!empty($this->aRecords)) {
                                // dodanie linku usuwajacego wszystkie zaznaczone rekordy
                                $sHtml .= '<a href="javascript:void(0);"' .
                                    ' onclick="if (Checked(document.forms.' . $this->sName . ', \'delete\')){' .
                                    'if (confirm(\'' . $aConfig['lang'][$sModule][$sActionDo . '_q'] . '\')) { ChangeObjValue(\'do\', \'remove\'); document.forms.' . $this->sName . '.submit(); return false;} else { return false; }}' .
                                    'else {' .
                                    'alert(\'' . $aConfig['lang'][$sModule][$sActionDo . '_err'] . '\'); return false;}"' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    '<img src="gfx/icons/' . $sActionDo . '_ico.gif" border="0" align="absmiddle"' .
                                    ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;';
                            }
                            break;

                        case 'publish_all':
                            if (!empty($this->aRecords)) {
                                // dodanie linku usuwajacego wszystkie zaznaczone rekordy
                                $sHtml .= '<a href="javascript:void(0);"' .
                                    ' onclick="if (Checked(document.forms.' . $this->sName . ', \'delete\')){' .
                                    'if (confirm(\'' . $aConfig['lang'][$sModule][$sActionDo . '_q'] . '\')) { ChangeObjValue(\'do\', \'publish\'); document.forms.' . $this->sName . '.submit(); return false;} else { return false; }}' .
                                    'else {' .
                                    'alert(\'' . $aConfig['lang'][$sModule][$sActionDo . '_err'] . '\'); return false;}"' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    '<img src="gfx/icons/' . $sActionDo . '_ico.gif" border="0" align="absmiddle"' .
                                    ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;';
                            }
                            break;

                        case 'review_all':
                            if (!empty($this->aRecords)) {
                                // dodanie linku usuwajacego wszystkie zaznaczone rekordy
                                $sHtml .= '<a href="javascript:void(0);"' .
                                    ' onclick="if (Checked(document.forms.' . $this->sName . ', \'delete\')){' .
                                    'if (confirm(\'' . $aConfig['lang'][$sModule][$sActionDo . '_q'] . '\')) { ChangeObjValue(\'do\', \'review\'); document.forms.' . $this->sName . '.submit(); return false;} else { return false; }}' .
                                    'else {' .
                                    'alert(\'' . $aConfig['lang'][$sModule][$sActionDo . '_err'] . '\'); return false;}"' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    '<img src="gfx/icons/' . $sActionDo . '_ico.gif" border="0" align="absmiddle"' .
                                    ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;';
                            }
                            break;

                        case 'package_closed':
                            if (!empty($this->aRecords)) {
                                // dodanie linku usuwajacego wszystkie zaznaczone rekordy
                                $sHtml .= '<a href="javascript:void(0);"' .
                                    ' onclick="if (Checked(document.forms.' . $this->sName . ', \'delete\')){' .
                                    ' ChangeObjValue(\'do\', \'package_closed\'); document.forms.' . $this->sName . '.submit(); return false; }' .//if (confirm(\''.$aConfig['lang'][$sModule][$sActionDo.'_q'].'\')) { ChangeObjValue(\'do\', \'review\'); document.forms.'.$this->sName.'.submit(); return false;} else { return false; }
                                    'else {' .
                                    'alert(\'' . $aConfig['lang'][$sModule][$sActionDo . '_err'] . '\'); return false;}"' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    '<img src="gfx/icons/' . $sActionDo . '_ico.gif" border="0" align="absmiddle"' .
                                    ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;';
                            }
                            break;

                        case 'print_all':
                            if (!empty($this->aRecords)) {
                                // dodanie linku usuwajacego wszystkie zaznaczone rekordy
                                $sHtml .= '<a href="javascript:void(0);"' .
                                    ' onclick="if (Checked(document.forms.' . $this->sName . ', \'delete\')){' .
                                    'ChangeObjValue(\'do\', \'print\'); document.forms.' . $this->sName . '.submit(); return false;}' .
                                    'else {' .
                                    'alert(\'' . $aConfig['lang'][$sModule][$sActionDo . '_err'] . '\'); return false;}"' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    '<img src="gfx/icons/' . $sActionDo . '_ico.gif" border="0" align="absmiddle"' .
                                    ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;';
                            }
                            break;
                        case 'payment_sum':
                            if (!empty($this->aRecords)) {
                                // dodanie linku usuwajacego wszystkie zaznaczone rekordy

                                $sHtml .= '<a id="sum_button" href="javascript:void(0);"' .
                                    ' onclick=""' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    '<img src="gfx/icons/sum_ico.gif" border="0" style="margin-bottom:5px;" align="absmiddle"' .
                                    ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;<div id="tmp_sum" style="display: none"></div>';

                                $sHtml .= '<script type="text/javascript">
															var iShowOrder = 1;
															$(\'#sum_button\').click(function() {
															  
																if (iShowOrder == 1) {
																	$(\'#tmp_sum\').html($(\'.recordsTable tbody\').html());
																	$(\'.recordsTable tbody\').html($(\'#payment_sum_hidden\').html());
																	iShowOrder++;
																} else {
																	$(\'.recordsTable tbody\').html($(\'#tmp_sum\').html());
																	iShowOrder=1;
																}
															});

													 </script>
									';
                            }
                            break;

                        case 'go_back':
                            $aGet = array();
                            $aOmit = array();
                            if (!empty($this->aFooterParams[$sActionDo][0])) {
                                foreach ($this->aFooterParams[$sActionDo][0] as $sParam => $mValue) {
                                    $aGet[$sParam] = $mValue;
                                }
                            }
                            if (!empty($this->aFooterParams[$sActionDo][1])) {
                                foreach ($this->aFooterParams[$sActionDo][1] as $sParam) {
                                    $aOmit[] = $sParam;
                                }
                            }

                            if (isset($aConfig['lang'][$sModule][$sActionDo])) {
                                $sLangDo = $aConfig['lang'][$sModule][$sActionDo];
                            } elseif (isset($aConfig['lang']['common'][$sActionDo])) {
                                $sLangDo = $aConfig['lang']['common'][$sActionDo];
                            } else {
                                $sLangDo = 'Powrót';
                            }

                            $sHtml .= '<a href="' . phpSelf($aGet, $aOmit) . '"' .
                                ' title="' . $sLangDo . '">' .
                                '<img src="gfx/icons/' . $sActionDo . '_ico.gif" border="0" align="absmiddle"' .
                                ' alt="' . $sLangDo . '"> ' .
                                $sLangDo .
                                '</a>&nbsp;&nbsp;';
                            break;
                        case 'add_to_group':
                            $aGet = array();
                            $aOmit = array();
                            if (!empty($this->aFooterParams[$sActionDo][0])) {
                                foreach ($this->aFooterParams[$sActionDo][0] as $sParam => $mValue) {
                                    $aGet[$sParam] = $mValue;
                                }
                            }
                            if (!empty($this->aFooterParams[$sActionDo][1])) {
                                foreach ($this->aFooterParams[$sActionDo][1] as $sParam) {
                                    $aOmit[] = $sParam;
                                }
                            }

                            if (!empty($this->aRecords)) {
                                // dodanie linku ustawiającego wyświetlanie produktów w boxie

                                $sHtml .= '<a href="javascript:void(0);"' .
                                    ' onclick="if (Checked(document.forms.' . $this->sName . ', \'delete\')){' .
                                    'if (confirm(\'' . $aConfig['lang'][$sModule][$sActionDo . '_q'] . '\')) { document.forms.' . $this->sName . '.action=\'' . phpSelf($aGet, $aOmit) . '\'; document.forms.' . $this->sName . '.submit(); return false;} else { return false; }}' .
                                    'else {' .
                                    'alert(\'' . $aConfig['lang'][$sModule][$sActionDo . '_err'] . '\'); return false;}"' .
                                    ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                    '<img src="gfx/icons/' . $sActionDo . '_ico.gif" border="0" align="absmiddle"' .
                                    ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                    $aConfig['lang'][$sModule][$sActionDo] .
                                    '</a>&nbsp;&nbsp;';
                            }
                            break;
                    }
                }
            } else {
                $sHtml .= '&nbsp;';
            }
            if ($this->aHeader['count_checkboxes']) {
                $sHtml .= '
				<script type="text/javascript">
	// <![CDATA[
			$(document).ready(function(){
				$(".viewDeleteCheckbox,#check_all,#toggleCheckAll").click(function(){
					var chcnt = $(".viewDeleteCheckbox:checked").length;
					$("#chceckboxesCountInfo").html("Zaznaczono: "+chcnt);
				});
			});
			// ]]>
</script>
				';
            }
            if (!empty($this->aFooterGroupActions)) {
                $sHtml .= "
				<script type=\"text/javascript\">
				";
                $sHtml .= "var grchg_q = new Array(); 
					 var grchg_err = new Array(); 
					 ";
                $aFooterSelect = array(array('value' => '', 'label' => ''));
                foreach ($this->aFooterGroupActions as $sAction) {
                    if (!empty($sAction)) {
                        $sHtml .= "grchg_q['" . $sAction . "']='" . $aConfig['lang'][$sModule][$sAction . '_q'] . "';
											grchg_err['" . $sAction . "']='" . $aConfig['lang'][$sModule][$sAction . '_err'] . "';
						";
                    }
                    $aFooterSelect[] = array('value' => $sAction, 'label' => $aConfig['lang'][$sModule][$sAction]);
                }
                $sHtml .= "
					function GroupActionChange(oObj){
						if(oObj.value != ''){
							if (Checked(document.forms." . $this->sName . ", 'delete')) {
								if (confirm(grchg_q[oObj.value])) { 
									ChangeObjValue('do', oObj.value);
									document.forms." . $this->sName . ".submit();
									oObj.selectedIndex = 0;
									return false;
								} else { 
									oObj.selectedIndex = 0;
									return false; 
								}
							} else {
								alert(grchg_err[oObj.value]);
								oObj.selectedIndex = 0; 
								return false;
							}
						}
					}
				</script>
				";
                $sHtml .= '<label for="v_group_change">Zaznaczone</label>';
                $sHtml .= '&nbsp;' . Form::GetSelect('v_group_change', array('id' => 'v_group_change_sel', 'onchange' => 'GroupActionChange(this);'), $aFooterSelect, '') . '&nbsp;&nbsp;';
            }
            // send button
            if (isset($this->aHeader['send_button']) && $this->aHeader['send_button'] != false) {
                $sHtml .= Form::GetInputButton('send', $aConfig['lang'][$sModule]['send_button_' . $this->aHeader['send_button']], array('onclick' => 'ChangeObjValue(\'do\', \'' . $this->aHeader['send_button'] . '\');'));
            }
            $sHtml .= '</td>';
            if (isset($this->aRecordsFooter[1]) && !empty($this->aRecordsFooter[1])) {
                $sHtml .= '<td class="second">';
                foreach ($this->aRecordsFooter[1] as $sActionDo) {
                    if (isset($this->aFooterParams[$sActionDo][2])) {
                        $sIco = $this->aFooterParams[$sActionDo][2];
                    } else {
                        $sIco = $sActionDo;
                    }
                    // dodanie linku z akcja
                    $aGet = array('do' => $sActionDo);
                    $aOmit = array();
                    if (!empty($this->aFooterParams[$sActionDo][0])) {
                        foreach ($this->aFooterParams[$sActionDo][0] as $sParam => $mValue) {
                            $aGet[$sParam] = $mValue;
                        }
                    }
                    if (!empty($this->aFooterParams[$sActionDo][1])) {
                        foreach ($this->aFooterParams[$sActionDo][1] as $sParam) {
                            $aOmit[] = $sParam;
                        }
                    }
                    if ($sActionDo == 'sort') {
                        if (!empty($this->aRecords) && count($this->aRecords) > 1) {
                            $aGet['back'] = base64_encode(phpSelf($aGet, array('do', 'id')));
                            $sHtml .= '<a href="' . phpSelf($aGet, $aOmit) . '"' .
                                ' title="' . $aConfig['lang'][$sModule][$sActionDo] . '">' .
                                '<img src="gfx/icons/' . $sIco . '_ico.gif" border="0" align="absmiddle"' .
                                ' alt="' . $aConfig['lang'][$sModule][$sActionDo] . '"> ' .
                                $aConfig['lang'][$sModule][$sActionDo] .
                                '</a>&nbsp;&nbsp;';
                        }
                    } else {

                        if (isset($aConfig['lang'][$sModule][$sActionDo])) {
                            $sNameLang = $aConfig['lang'][$sModule][$sActionDo];
                        } else {
                            $sNameLang = $aConfig['lang']['common'][$sActionDo];
                        }
                        $sHtml .= '<a href="' . phpSelf($aGet, $aOmit) . '"' .
                            ' title="' . $sNameLang . '">' .
                            '<img src="gfx/icons/' . $sIco . '_ico.gif" border="0" align="absmiddle"' .
                            ' alt="' . $sNameLang . '"> ' .
                            $sNameLang .
                            '</a>&nbsp;&nbsp;';
                    }
                }
                $sHtml .= '</td>';
            }
            $sHtml .= '</tr></table>';
        }

        if (!isset($this->aHeader['per_page']) || $this->aHeader['per_page']) {
            $aPerPage = array(
                array('value' => 5, 'name' => 5),
                array('value' => 10, 'name' => 10),
                array('value' => 20, 'name' => 20),
                array('value' => 50, 'name' => 50),
                array('value' => 100, 'name' => 100),
                array('value' => 200, 'name' => 200),
                array('value' => 500, 'name' => 500)
            );
            $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];

            if (!empty($this->aRecords)) {
                $sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="pagerTable">' .
                    '<tr>';
                if (!empty($this->aPagerLinks['all'])) {
                    $sHtml .= '<td class="first">' .
                        $aConfig['lang']['pager']['pages'] .
                        $this->aPagerLinks['all'] .
                        '</td>';
                }

                if (!isset($this->aHeader['hide_per_page'])) {
                    if (!empty($this->aRecords)) {
                        if (!empty($_GET['do']) && ($this->aHeader['second'] == true)) {
                            $sHtml .= Form::GetHidden('do', $_GET['do']);
                        }
                        $sHtml .= '<td class="second">' .
                            $aConfig['lang']['pager']['per_page'] . ' ' . Form::GetSelect('per_page', array('onchange' => 'this.form.submit();'), $aPerPage, $iPerPage) .
                            '</td>';
                    }
                }
                $sHtml .= '</tr>' .
                    '</table>';
            }
        }

        if (!isset($this->aHeader['form']) || $this->aHeader['form']) {
            $sHtml .= Form::GetFooter();
        }
        $sHtml .= '</td></tr>';
        $sHtml .= '</table>';
        return $sHtml;
    } // end of Show() function
} // end of class View
?>