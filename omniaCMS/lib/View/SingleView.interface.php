<?php
namespace omniaCMS\lib\View;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
interface SingleView {

  public function parseViewItem(array $aItem);
  
  public function resetViewState();
}
