<?php

/**
 * Klasa MyFile - zarzadzanie plikami na serwerze
 *
 * @author Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version 1.0
 */
class MyFile {
	
	// dane zrodlowe pliku
	var $aSrc;
	
	// sciezka do katalogu docelowego
	var $sDir;
	

	/**
	 * Konstruktor
	 * 
	 * @param		array	$sSrc				- tablica z danymi przesylanego pliku
	 * @param 	
	 * @return 	void
	 **/
	function MyFile() {} // end of File() method
	
	
	/**
	 * Metoda ustawia dane pliku
	 * 
	 * @param	array	$aImg	- dane pliku
	 * @return	void
	 */
	function SetFile($sDir, &$aFile,$iFId=-1) {
		$this->sDir = $sDir;
		if($iFId>=0){
			$this->aSrc['name'] =& $aFile['name'][$iFId];
			$this->aSrc['type'] =& $aFile['type'][$iFId];
			$this->aSrc['tmp_name'] =& $aFile['tmp_name'][$iFId];
			$this->aSrc['error'] =& $aFile['error'][$iFId];
			$this->aSrc['size'] =& $aFile['size'][$iFId];
		} else {
			$this->aSrc =& $aFile;
		}
		$this->aSrc['ext'] = strtolower(array_pop(explode('.', $this->aSrc['name'])));
		
	} // end of SetFile() method
	
	
	function proceedFile($aAllowedExtensions=array(), $bUniqueName = false, $bOverwrite = false) {
		global $aConfig;
		if (in_array($this->aSrc['ext'], !empty($aAllowedExtensions) ? $aAllowedExtensions : $aConfig['common']['file_extensions'])) {
  		// operacje na obrazkach
		  return $this->MoveFileTo($this->sDir, '', $bUniqueName, $bOverwrite);
		}
		return -5;
	} // end of proceedFile() method
	
	
	/**
   * Metoda przenosi przeslany plik do wskazanego katalogu
   * Jezeli drugi parametr '$bUniqueName' jest ustawiony na TRUE
	 * zostanie wygenerowana unikalna nazwa pliku.
   *
	 * @param   string  $sDestDir     - sciezka do katalogu docelowego,
	 * 																	bez koncowego '/' lub '\'
	 * @param	string	$sNewName				- nowa nazwa pod ktora zostanie zapisany plik
   * @param   bool    $bUniqueName  - czy wygenerowac unikalna nazwe pliku, domyslnie FALSE
   * @param   bool    $bOverwrite   - czy nadpisywac pliki o istniejacej juz nazwie
   * @param   integer $iMode        - atrybuty dla nowo utworzonego pliku
   * @return  integer               - -4: podana sciezka nie wskazuje na katalog
   *                                  -3: katalog nie jest zapisywalny
   *                                  -2: plik o podanej nazwie juz istnieje
   *                                  -1: proba przeniesienia do katalogu docelowego nie powiodla sie
   *                                   1: plik zostal przeniesiony do katalogu docelowego
   */
  function MoveFileTo($sDestDir, $sNewName='', $bUniqueName = false, $bOverwrite = false, $iMode=0644) {
    if (!empty($this->aSrc)) {
			if (is_dir($sDestDir)) {
				if (is_writable($sDestDir)) {
					if ($bUniqueName) {
						// generujemy unikalna nazwe dla pliku
						$sName = $this->_GenerateUniqueName().'.'.$this->aSrc['ext'];
					}
					elseif ($sNewName != '') {
						if (!strpos($sNewName, '.')) {
							// nowa nazwa zostala podana bez rozszerzenia,
							// dodanie oryginalnego rozszerzenia
							$sNewName .= '.'.$this->aSrc['ext'];
						}
						$sName = $sNewName;
					}
					else {
						$sName = $this->aSrc['name'];
					}
					if (!$bOverwrite && file_exists($sDestDir.'/'.$sName)) {
						return -2;
					}
					if (!@copy($this->aSrc['tmp_name'], $sDestDir.'/'.$sName)) {
						return -1;
					}
					$this->aSrc['final_name'] = $sName;
					// zmieniamy atrybuty
					@chmod($sDestDir.'/'.$sName, $iMode);
					return 1;
				}
				else {
					return -3;
				}
			}
			else {
				return -4;
			}
		}
		return -1;
  } // end MoveFileTo()
	
	
	function GetFileProperty($sPropertyName) {
		return $this->aSrc[$sPropertyName];
	} // end of GetFileProperty()
	
	
	/**
   * Metoda generuje losowa nazwe pliku
   *
   * @return  string                    - wygenerowany identyfikator
   */
  function _GenerateUniqueName() {
    $sID = md5(uniqid(rand()));
    
    return $sID;
  } // end of _GenerateUniqueName() method
} // end of class MyFile
?>