<?php

/**
 * Podstawowa klasa CMSa
 *
 * @author Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version 1.0
 */
class Admin
{

    // wersja jezykowa
    public $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    public $aPrivileges;

    /**
     *
     * @var ordersSemaphore oOrdersSemaphore
     */
    public $oOrdersSemaphore;

    /**
     *
     * @var Doctrine\ORM\EntityManager
     */
    public $entityManager;

    /**
     *
     * @var array
     */
    public $aConfig;

    /**
     *
     * @var \Database
     */
    public $pDB;

    /**
     *
     * @var \DatabaseManager
     */
    public $pDbMgr;

    /**
     *
     * @var string
     */
    public $sModule;

    /**
     *
     * @var string
     */
    public $sAction;

    /**
     *
     * @var string
     */
    public $sDo;

    /**
     *
     * @var \Smarty
     */
    public $pSmarty;

    /**
     * Konstruktor klasy Admin
     *
     */
    function Admin(&$pSmarty)
    {
        global $aConfig, $pDbMgr, $pDB, $pSmarty;
        $sFrame = '';
        $sFile = '';
        $sModule = '';
        $sAction = '';


        $this->initMemcached();
        $this->initRefererURL();

        if (isset($_POST['change_lang']) && isset($_POST['lang']) &&
            !empty($_POST['lang'])
        ) {
            // wybrano zmiane wersji jezykowej
            $this->setLanguageVersion();
            // ustawienie w COOKIE wybranej wersji jezykowej
            AddCookie('lang', $_POST['lang']);
        }

        $this->iLangId =& $_SESSION['lang']['id'];
        $this->aPrivileges =& $_SESSION['user']['privileges'];
        $this->aConfig = $aConfig;
//    $this->entityManager = $this->getEntityManager($_GET['module']);
        $this->pDbMgr = $pDbMgr;
        $this->pDB = $pDB;
        $this->pSmarty = $pSmarty;

        if (isset($_GET['frame'])) {
            $sFrame = $_GET['frame'];
        }
        if (isset($_POST['do'])) {
            $this->sDo = $_POST['do'];
        } elseif (isset($_GET['do'])) {
            $this->sDo = $_GET['do'];
        }

        if (isset($_GET['action']) && !empty($_GET['action'])) {
            $this->sAction = '_' . $_GET['action'];
        }

        if ($this->sDo == 'main_preview') {
            // przekierowanei na glowna strone z podgladem
            header('Location: /?preview=' . md5($_SERVER['REMOTE_ADDR'] . $aConfig['common']['client_base_url_http']));
            exit(0);
        }
        if ($this->sDo == 'clear_cache') {
            // przekierowanie na glowna strone z czyszczeniem cache'a
            header('Location: /?clear_cache=' . md5($_SERVER['REMOTE_ADDR'] . $aConfig['common']['client_base_url_http']));
            exit(0);
        }

        $this->SessionMessage($pSmarty);

        // dodanie do Smartow tablicy z langami
        $pSmarty->assign_by_ref('aLang', $aConfig['lang']);

        switch ($sFrame) {
            case 'left':
                // wywolanie metody inicjujacej interfejs
                $this->ShowLeftMenu($pSmarty);
                break;

            case 'header':
                // wywolanie metody wyswietlajacej naglowek
                $this->ShowHeader($pSmarty);
                break;

            case 'main':
                // wywolanie metody inicjujacej interfejs
                if (isset($_GET['file'])) {
                    $sFile = $_GET['file'];
                }

                if (isset($_GET['module'])) {
                    $sModule = $_GET['module'];
                    $this->sModule = $sModule;
                }

                if (!empty($sFile)) {
                    // wybrana jedna z domyslnych akcji a nie modulow
                    include_once('core/' . $sFile . '/Core.class.php');
                    // dolaczenie pliku jezykowego czesci adminsitacyjnej
                    include_once('core/' . $sFile .
                        '/admin_' . $aConfig['default']['language'] . '.php');
                    $pCore = new Core($pSmarty);
                } elseif (!empty($sModule)) {
                    // wybrano modul
                    include_once('modules/' . $sModule . '/Module' . $this->sAction . '.class.php');
                    // dolaczenie pliku jezykowego czesci adminsitacyjnej
                    include_once('modules/' . $sModule .
                        '/lang/admin_' . $aConfig['default']['language'] . '.php');

                    $sClassName = 'Module';
                    $sClassName .= '__' . preg_replace('/^m_(.*)$/', '$1', $sModule);

                    if ($this->sAction != '') {
                        $sClassName .= '__' . preg_replace('/^_(.*)$/', '$1', $this->sAction);
                    }
                    $sNamespace = '\omniaCMS\modules\\' . $sModule . '\\';
                    $sClassNameWithNamespace = $sNamespace . $sClassName;

                    if (class_exists($sClassName)) {
                        $oModule = $this->initClass($sClassName);
                        if ($this->sDo != '') {
                            $this->callMethod($oModule, $this->sDo);
                        }
                    } else if (class_exists($sClassNameWithNamespace)) {
                        if ($this->checkDefaultPrivilages() === TRUE) {
                            $oModule = $this->initClass($sClassNameWithNamespace);
                            if ($this->sDo != '') {
                                $sModuleResults = $this->callMethod($oModule, $this->sDo);

                            } else {
                                $sModuleResults = $oModule->doDefault();
                            }
                            if (!isset($_GET['hideHeader']) && $_SESSION['user']['type'] == '1') {
                                $sAddContent = '' . $sModule . '/<b>Module' . $this->sAction.'</b>.class.php  - '.$this->sDo;
                            }

                            $this->parseSmarty($sModuleResults . $sAddContent, $oModule->getMsg());
                        }
                    } else {
                        $pCore = new Module($pSmarty, $this);
                        if (!isset($_GET['hideHeader']) && $_SESSION['user']['type'] == '1') {
                            $pSmarty->_tpl_vars['sContent'] .= '' . $sModule . '/<b>Module' . $this->sAction.'</b>.class.php  - '.$this->sDo;
                        }
                    }

                    // informacja do sesji o aktualnie wybranym module akcji
                    // na potrzeby eksploratora
                    if (isset($_GET['action']) && !empty($_GET['action'])) {
                        $sModule = $sModule . '_' . $_GET['action'];
                    }
                    $_SESSION['_currentModule'] = $sModule;
                } else {
                    // domyslna akcja wyswietlenie informacji o CMSie
                    include_once('core/c_main/Core.class.php');
                    // dolaczenie pliku jezykowego czesci adminsitacyjnej
                    include_once('core/c_main' .
                        '/admin_' . $aConfig['default']['language'] . '.php');
                    $pCore = new Core($pSmarty);

                    $pSmarty->assign_by_ref('sContent', $pCore->getContent(), true);
                }


                if (isset($_SESSION['url_referer']) && !empty($_SESSION['url_referer'])) {
                    $pSmarty->assign('aUrlReferer', $_SESSION['url_referer']);
                }

                include_once('../LIB/EntityManager/Entites/UsersGroups.php');

                $pUsersGroups = new \LIB\EntityManager\Entites\UsersGroups($pDbMgr);
                $aUserGroups = $pUsersGroups->getUserGroups($_SESSION['user']['id']);

                if (empty($aUserGroups) === false) {
                    $aAlert = $pUsersGroups->getAlertForGroup($aUserGroups, $_SESSION['user']['id']);

                    $sAlert = $this->ShowGroupAlert($aAlert);

                    $pSmarty->assign_by_ref('sGroupsAlert', $sAlert);
                }

                // zapisanie stanu sesji dla zalogowanego uzytkownika
                $sSql = "UPDATE " . $aConfig['tabls']['prefix'] . "users
								 SET settings = '" . serialize(array($_SESSION['_viewState'],
                        $_SESSION['_treeViewState'])) . "'
								 WHERE id = " . $_SESSION['user']['id'];
                Common::Query($sSql);

                break;

            default:
                // wywolanie metody inicjujacej interfejs
                $this->ShowInterface($pSmarty);
                break;
        }
    } // end of Admin() function


    /**
     *
     * @param string $sStr
     */
    private function parseSmarty($sStr, $sMsg)
    {

        $this->pSmarty
            ->assign('sContent', (!empty($sMsg) ? $sMsg : '')
                . $sStr);
    }

    /**
     *
     * @return null
     */
    private function checkDefaultPrivilages()
    {

        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($this->pSmarty);
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     *
     * @param object $oModule
     */
    private function callMethod($oModule)
    {

        $sMethodName = 'do' . ucfirst($this->sDo);
        if (method_exists($oModule, $sMethodName)) {
            return $oModule->$sMethodName();
        }
    }


    /**
     * Init Klasy
     *
     * @param string $sClassName
     * @return \sClassName
     */
    private function initClass($sClassName)
    {

        $aImplements = class_implements($sClassName);
        if (isset($aImplements['omniaCMS\lib\interfaces\ModuleEntity'])) {
            return new $sClassName($this);
        } else {
            return new $sClassName($this->pSmarty, $this);
        }
    }

    /**
     *
     * @param string $sModule
     * @return EntityManager
     */
    private function getEntityManager($sModule)
    {
        /* @var $entityManager EntityManager */
        if (stristr($sModule, 'zamowienia')) {
            require_once('../annotations_bootstrap.php');
            return $entityManager;
        } else {
            return null;
        }
    }


    /**
     * Zmiana w sesji referea
     */
    private function initRefererURL()
    {

        $aUrlParams = $this->getArrUrl($_SERVER["REQUEST_URI"]);
        if (isset($aUrlParams['save_referer_url']) && $aUrlParams['save_referer_url'] != '') {
            $aRefUrl = $this->getArrUrl($_SERVER['HTTP_REFERER']);
            $sURLKeyName = $aRefUrl['module'] . $aRefUrl['action'];
            $_SESSION['url_referer'][$sURLKeyName]['name'] = $aUrlParams['save_referer_url'];
            $_SESSION['url_referer'][$sURLKeyName]['url'] = preg_replace("/&(save_referer_url=[^&]*)/", "", $_SERVER['HTTP_REFERER']);
        } else {
            if (isset($_SESSION['url_referer']) && !empty($_SESSION['url_referer'])) {
                $sNowURLKeyName = $aUrlParams['module'] . $aUrlParams['action'];
                foreach ($_SESSION['url_referer'] as $sUrlKey => $aUrlVal) {
                    if ($sNowURLKeyName == $sUrlKey) {
                        unset($_SESSION['url_referer'][$sUrlKey]);
                    }
                }
            }
        }
    }


    /**
     *
     * @global array $aConfig
     */
    private function initMemcached()
    {
        global $aConfig;

        if (class_exists('memcached')) {
            $oMemcache = new Memcached();
            $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);

            include_once('../LIB/orders_semaphore/ordersSemaphore.class.php');
            $this->oOrdersSemaphore = new ordersSemaphore($oMemcache);

            if ($_GET['frame'] == 'main' && ($_GET['module'] != 'm_zamowienia' || $_GET['action'] != '' || $_GET['do'] != 'details')) {
                // jest gdzieś indziej wywalamy blokady zamowen
                $this->oOrdersSemaphore->unlockAllOrders($_SESSION['user']['id']);
            }
        }
    }


    /**
     * Metoda pobiera tablice na podstawie adresu url
     *  http://stackoverflow.com/questions/8067075/php-url-to-array
     *
     * @param string $sUrl
     * @return array
     */
    public function getArrUrl($sUrl)
    {

        $remove_http = str_replace('http://', '', $sUrl);
        $split_url = explode('?', $remove_http);
        $split_parameters = explode('&', $split_url[1]);

        for ($i = 0; $i < count($split_parameters); $i++) {
            $final_split = explode('=', $split_parameters[$i]);
            $split_complete[$final_split[0]] = $final_split[1];
        }
        return $split_complete;
    }// end of getArrUrl() method


    /**
     * Metoda wyswietla interfej CMSa
     *
     * @param    object $pSmarty
     * @return void
     */
    private function ShowInterface(&$pSmarty)
    {
        global $aConfig;

        // dolaczenie klasy FormTable
        include_once('Form/Form.class.php');

        // dowiazanie do Smartow zmiennych
        $pSmarty->assign_by_ref('sUserName', $_SESSION['user']['name']);
        $pSmarty->assign_by_ref('sIP', $_SESSION['user']['ip']);
        $pSmarty->assign_by_ref('sHost', $_SESSION['user']['host']);
        $pSmarty->assign('sApplName', '<strong>' . $aConfig['common']['name'] . '</strong> ' . $aConfig['common']['version']);

        // dodanie danych dla gornego menu
        $aHeaderMenu =& $this->GetMenu('config/menu.xml');
        $pSmarty->assign_by_ref('sHeaderMenu', $aHeaderMenu[0]);
        $pSmarty->assign_by_ref('sHeaderMenuDraw', $aHeaderMenu[1]);
        $pSmarty->assign_by_ref('sHeaderMenuTable', $aHeaderMenu[2]);

        // dodanie formularza z selectem wyboru wersji jezykowej
        $aLanguages =& $this->getLanguages();
        if (count($aLanguages) > 1) {
            $sLanguagesForm = Form::GetHeader('languages', array('method' => 'POST', 'action' => phpSelf()));
            $sLanguagesForm .= '<span class="text10">' . $aConfig['lang']['common']['language'] . ':</span>&nbsp;';
            $sLanguagesForm .= Form::GetHidden('change_lang', '1');
            $sLanguagesForm .= Form::GetSelect('lang', array('onchange' => 'this.form.submit()'), $this->getLanguages(), $_SESSION['lang']['id']);
            $sLanguagesForm .= Form::GetFooter();

            $pSmarty->assign_by_ref('sLang', $aConfig['lang']['common']['language']);
            $pSmarty->assign_by_ref('sLangForm', $sLanguagesForm);
        }

        $pSmarty->assign_by_ref('sContent', $pSmarty->fetch('main.tpl'));
    } // end of ShowInterface() function


    /**
     * Metoda wyswietla naglowek modulu
     *
     * @param    object $pSmarty
     * @return void
     */
    function ShowHeader(&$pSmarty)
    {
        global $aConfig;

        $pSmarty->assign('sHeader', '<strong>"' . $aConfig['lang']['lay_header']['start'] . '"</strong>');
        $pSmarty->assign_by_ref('sContent', $pSmarty->fetch('header.tpl'));
    } // end of ShowHeader() function


    function GetMenu($sXmlFile)
    {
        require_once("XML/Unserializer.php");
        $sMenu = '';
        $sDrawMenu = '';
        $sDrawMenuTable = '';

        $aOptions = array(
            'complexType' => 'array',
            'parseAttributes' => true,
            'attributesArray' => 'attribs'
        );
        $pUS = new XML_Unserializer($aOptions);
        $pResult = $pUS->unserialize($sXmlFile, true);
        if (!PEAR::IsError($pResult)) {
            $aData = $pUS->getUnserializedData();
            $this->GetMenuItems($aData, $sMenu, $sDrawMenu, $sDrawMenuTable);
            //dump($sMenu);
        }
        return array($sMenu, $sDrawMenu, $sDrawMenuTable);
    }


    /**
     * Metoda pobiera elementy gornego menu
     *
     * @return
     **/
    function GetMenuItems(&$aData, &$sMenu, &$sDrawMenu, &$sDrawMenuTable, $sID = '')
    {
        global $aConfig;

        foreach ($aData['item'] as $iI => $aItem) {
            if (in_array($_SESSION['user']['type'], explode(',', $aItem['attribs']['privs'])) ||
                in_array($_SESSION['user']['name'], explode(',', $aItem['attribs']['special_users']))
            ) {
                // tylko jezeli uzytkownik ma prawa do tej akcji
                if (empty($sID)) {
                    $sMenu .= "var menu" . $sID . $iI . " =
						[";
                }
                $sMenu .= "['<img src=\"gfx/" . $aItem['attribs']['ico'] . "\" style=\"margin-left: 2px; margin-right: 3px; margin-bottom: 1px;\">', '" . $aConfig['lang']['menu'][$aItem['attribs']['name']] . "', '" . sprintf($aItem['attribs']['url'], $aConfig['lang']['menu'][$aItem['attribs']['name']]) . "', '" . $aItem['attribs']['target'] . "', ''";

                if (isset($aItem['item'])) {
                    $sMenu .= ",";
                    $this->GetMenuItems($aItem, $sMenu, $sDrawMenu, $sDrawMenuTable, $sID . $iI);
                }
                if (empty($sID)) {
                    if (preg_match('/,$/', $sMenu)) {
                        $sMenu = substr($sMenu, 0, strlen($sMenu) - 1);
                    }
                    $sMenu .= "]];\n";
                    $sDrawMenu .= "cmDraw('menuID" . $sID . $iI . "', menu" . $sID . $iI . ", 'hbr', cmLayout, 'Layout');\n";
                    $sDrawMenuTable .= "<td id=\"menuID" . $sID . $iI . "\"></td><td width=\"2\"><img src=\"gfx/menu_separator.gif\" width=\"2\" height=\"25\" alt=\"\"></td>";
                } else {
                    $sMenu .= "],";
                }
            }
        }
    }


    function ShowLeftMenu(&$pSmarty)
    {
        global $aConfig;
        $sModules = '';
        $aModules = array();

        // pobranie listy dostepnych dla uzytkownika modulow
        if ($_SESSION['user']['type'] != 0) {
            // uzytkownik typu superadmin lub admin
            $sSql = "SELECT id, symbol, " . $_SESSION['user']['type'] . " AS profile
							 FROM " . $aConfig['tabls']['prefix'] . "modules
							 ORDER BY order_by";
            $aModules = Common::GetAll($sSql);
        } elseif (isset($this->aPrivileges['modules']) &&
            !empty($this->aPrivileges['modules'])
        ) {
            // zwykly uzytkownik
            // okreslenie ID modulow do ktorych ma dostep uzytkownik
            foreach ($this->aPrivileges['modules'] as $iMId => $iPriv) {
                $sModules .= $iMId . ',';
            }
            $sModules = substr($sModules, 0, -1);
            $sSql = "SELECT id, symbol
							 FROM " . $aConfig['tabls']['prefix'] . "modules
							 WHERE id IN (" . $sModules . ")
							 ORDER BY order_by";
            $aModules = Common::GetAll($sSql);
        } else {
            $sHtml = GetMessage($aConfig['lang']['common']['no_privileges2']);
            $pSmarty->assign_by_ref('sContent', $sHtml);
            return;
        }

        if (!empty($aModules)) {
            // dolaczenie klasy XML/Unserializer
            require_once("XML/Unserializer.php");
            $aOptions = array(
                'complexType' => 'array',
                'parseAttributes' => true,
                'attributesArray' => 'attribs',
                'forceEnum' => array('item')
            );
            $pUS = new XML_Unserializer($aOptions);

            $sHtml = '<script type="text/javascript" language="JavaScript">
																			var sSelectedItem = \'\';
																		</script>
																		<div style="vertical-align: top; margin-top: 20px;">';
            $i = 0;
            foreach ($aModules as $iModKey => $aModule) {
                if (file_exists('modules/' . $aModule['symbol'] . '/menu.xml')) {

                    if (!isset($aModule['profile'])) {
                        // ustawienie typu praw do modulu
                        $aModule['profile'] = $this->aPrivileges['modules'][$aModule['id']];
                    }

                    $pResult = $pUS->unserialize('modules/' . $aModule['symbol'] . '/menu.xml', true);
                    if (!PEAR::IsError($pResult)) {
                        $aData = $pUS->getUnserializedData();
                        // usuniecie elementow ktore maja byc niewidoczne w menu lub do ktorych
                        // uzytkownik nie posiada wystarczajacych praw
                        foreach ($aData['item'] as $iKey => $aItem) {
                            if ((isset($aItem['attribs']['visible']) &&
                                    $aItem['attribs']['visible'] == '0') ||
                                !in_array($aModule['profile'],
                                    explode(',', $aItem['attribs']['privs']))
                            ) {
                                unset($aData['item'][$iKey]);
                            }
                        }
                        if (empty($aData['item'])) {
                            unset($aModules[$iModKey]);
                        } else {
                            $aModules[$iModKey]['items'] = $aData['item'];
                        }
                    } else {
                        unset($aModules[$iModKey]);
                    }
                } else {
                    unset($aModules[$iModKey]);
                }
            }

            foreach ($aModules as $iModKey => $aModule) {
                $sHtml .= '<table border="0" cellspacing="0" cellpadding="0" align="center" class="menuTable">
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
														<tr class="menuTableHeader" onmouseover="SwapClass(this, \'menuTableHeaderHov\', \'menu_' . $i . '\');" onmouseout="SwapClass(this, \'menuTableHeader\', \'menu_' . $i . '\');" onclick="ExpandMenu(this, \'menu_' . $i . '\');">
															<td width="1"><img src="gfx/menu_header_pixels.gif" width="1" height="19"></td>
															<td><a href="javascript:void(0);" class="menuTableHeaderText"><div>' . (isset($aConfig['lang']['c_modules'][$aModule['symbol']]) ? $aConfig['lang']['c_modules'][$aModule['symbol']] : $aModule['symbol']) . '</div></a></td>
															<td width="1"><img src="gfx/menu_header_pixels.gif" width="1" height="19"></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td height="1"><img src="gfx/pixel.gif" width="1" height="1"></td>
											</tr>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0" id="menu_' . $i . '" style="display: none;">';
                $j = 0;
                foreach ($aModule['items'] as $aItem) {
                    $sHtml .= '<tr id="menu_' . $i . '_' . $j . '" class="menuRow" onmouseover="SwapItemClass(this, \'menuRowHov\');" onmouseout="SwapItemClass(this, \'menuRow\');" onclick="LeftMenuNavigate(this, \'' . (isset($aConfig['lang']['c_modules'][$aModule['symbol']]) ? $aConfig['lang']['c_modules'][$aModule['symbol']] : $aModule['symbol']) . '\', \'' . sprintf($aItem['attribs']['url'], $aModule['id']) . '\', \'menuRow\');">
													<td class="menuRowTD"><a href="javascript:void(0);" class="menuRowText"><div>' . (isset($aConfig['lang'][$aModule['symbol']][$aItem['attribs']['name']]) ? $aConfig['lang'][$aModule['symbol']][$aItem['attribs']['name']] : $aItem['attribs']['name']) . '</div></a></td>
												</tr>';
                    $j++;
                }
                $sHtml .= '<tr>
										<td height="20"></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>';
                $i++;
            }
            $sHtml .= '</div>';
        } else {
            $sHtml = GetMessage($aConfig['lang']['c_modules']['no_items']);
        }
        $pSmarty->assign_by_ref('sContent', $sHtml);
    }


    /**
     * Metoda pobiera liste zdefiniowanych dla serwisu wersji jezykowych
     *
     * @return    array    - tablica z lista wersji jezykowych
     */
    function getLanguages()
    {
        global $aConfig;

        $sSql = "SELECT id AS value, symbol AS label
						 FROM " . $aConfig['tabls']['prefix'] . "languages
						 ORDER BY default_lang DESC, id";
        return Common::GetAll($sSql);
    } // end of getLanguages() method


    /**
     * Metoda ustawia wersje jezykowa
     *
     * @return    void
     */
    function setLanguageVersion()
    {
        global $pLogin;

        $_SESSION['lang']['id'] = $_POST['lang'];
        $_SESSION['lang']['symbol'] = $pLogin->_GetLangSign();
        $_SESSION['lang']['default'] = $pLogin->_GetDefaultLang();
    } // end of setLanguageVersion() function


    /**
     * Metoda ustawia uprawnienia uzytkownika do wersji jezykowej
     *
     * @return    void
     */
    function setUserPrivileges()
    {
        global $pLogin;

        $_SESSION['user']['privileges'] = $pLogin->_GetUserPrivileges($_SESSION['user']['id']);
    } // end of setUserPrivileges() function

    /**
     * Metoda dowiązuje do szablonu smarty komunikaty sesji
     *
     * @param type $pSmarty
     */
    private function SessionMessage(&$pSmarty)
    {

        if (!empty($_SESSION['_tmp_cms']['message'])) {
            $sSessionMessage = GetMessage($_SESSION['_tmp_cms']['message']['text'], $_SESSION['_tmp_cms']['message']['error']);
            $pSmarty->assign('sSessionMessage', $sSessionMessage);
            unset($_SESSION['_tmp_cms']['message']);
        }
    }// end of SessionMessage() method

    function ShowGroupAlert($aAlert) {
        $sHtml = '';

        if(empty($aAlert) === false) {
            $sHtml .= '<div style="background: rgba(0,0,0,.3); width:100%; height:100%; position:fixed; top:0; left:0; z-index:999; text-align: center"><div style="margin: auto; height: 50%; position: absolute; top: 0; left: 0; bottom: 0; right: 0;">';

            $sHtml .= '<div style="background: url(\'gfx/warning_min.png\') no-repeat; background-position: 3px 0px; background-color: rgb(232, 221, 96); border: 1px solid #5b9f11; padding: 6px 9px 9px 36px; text-align: center; margin: 5px; color: #000; font-size: .9em">' .
                '<table style="margin-bottom: 12px">
                    <tr>
                        <td>Utworzono: ' . $aAlert['created'] . ' </td>
                        <td> przez ' . $aAlert['created_by'] . '  </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: 20px">' .
                            $aAlert['message'] . '
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"> 
                            <a href="#" onclick="SetAsReadAjax(\'admin.php?' . http_build_query(array('frame' => 'main', 'module' => 'm_konta', 'action' => 'groups', 'do' => 'set_as_read', 'id' => $aAlert['id'])) . '\', this)">Oznacz jako przeczytane</a>
                        </td>
                    </tr>
                </table>';

            $sHtml .= '</div></div></div>';
        }
        return $sHtml;
    }
} // end class Admin
?>