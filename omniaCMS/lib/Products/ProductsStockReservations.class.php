<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-17 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace omniaCMS\lib\Products;

use DatabaseManager;
use Exception;

/**
 * Obsługa rezerwacji produktów
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ProductsStockReservations {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  const TABLE_NAME = 'products_stock_reservations';
  const WEBSITE_CODE = 'profit24';
  static private $RESERVATION_PROVIDERS = array(
      'profit_g',
      'profit_j',
      'profit_e'
  );

  public function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @param int $iOldPrividerId
   * @param int $iNewProviderId
   * @param int $iProductsStockId
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @param int $iQuantity
   * @throws Exception
   */
  public function updateProvider($iOldPrividerId, $iNewProviderId, $iProductsStockId, $iOrderId, $iOrderItemId, $iQuantity) {

    try {
//      $this->pDbMgr->BeginTransaction(self::WEBSITE_CODE);
      $this->deleteReservation($iOldPrividerId, $iOrderId, $iOrderItemId);
      $this->createReservation($iNewProviderId, $iProductsStockId, $iOrderId, $iOrderItemId, $iQuantity);
//      $this->pDbMgr->CommitTransaction(self::WEBSITE_CODE);
      return true;
    } catch(Exception $ex) {
//      $this->pDbMgr->RollbackTransaction(self::WEBSITE_CODE);
      throw $ex;
    }
  }
  
  /**
   * 
   * @param int $iProviderId
   * @param int $iProductsStockId
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @param int $iNewReservationQuantity
   * @param int $iOldReservationQuantity
   * @return boolean
   */
  public function updateQuantity($iProviderId, $iProductsStockId, $iOrderId, $iOrderItemId, $iNewReservationQuantity, $iOldReservationQuantity = 0) {
    
    if ($iOldReservationQuantity === 0) {
      $aReservation = $this->getColReservationArr(array('quantity'), $iOrderItemId, $iOrderId);
      $iOldReservationQuantity = $aReservation['quantity'];
    }
    $aSourceColProvider = $this->getSourceColProvider($iProviderId);
    if ($this->checkIsColumnReservation($aSourceColProvider)) {
      try {

          // wylaczona rezerwacja
//        $this->pDbMgr->BeginTransaction(self::WEBSITE_CODE);
        $this->updateQuantityOnProductsStockReservation($iOrderId, $iOrderItemId, $iNewReservationQuantity);
        $iNewToReservationQuantity = ($iNewReservationQuantity - $iOldReservationQuantity);
        $this->updateReservationOnProductsStock($aSourceColProvider['column_prefix_products_stock'], $iProductsStockId, $iNewToReservationQuantity);
//        $this->pDbMgr->CommitTransaction(self::WEBSITE_CODE);
        return true;
      } catch (Exception $ex){
//        $this->pDbMgr->RollbackTransaction(self::WEBSITE_CODE);
        throw $ex;
      }
    } else {
      return true;
    }
  }
  
  /**
   * 
   * @param int $iProviderId
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @return int
   */
  public function deleteReservation($iProviderId, $iOrderId, $iOrderItemId) {
    
    $aSourceColProvider = $this->getSourceColProvider($iProviderId);
    $aCurrentReservation = $this->getColReservationArr(array('quantity', 'products_stock_id'), $iOrderItemId, $iOrderId);
    if ($this->checkIsColumnReservation($aSourceColProvider) && !empty($aCurrentReservation)) {
      try {
          //$this->pDbMgr->BeginTransaction(self::WEBSITE_CODE);
          $iNewToReservationQuantity = -$aCurrentReservation['quantity'];
          $this->updateReservationOnProductsStock($aSourceColProvider['column_prefix_products_stock'], $aCurrentReservation['products_stock_id'], $iNewToReservationQuantity);
          $this->deleteProductsStockReservaton($iOrderId, $iOrderItemId);
          //$this->pDbMgr->CommitTransaction(self::WEBSITE_CODE);
          return true;
      } catch (Exception $ex) {
        //$this->pDbMgr->RollbackTransaction(self::WEBSITE_CODE);
        throw $ex;
      }
    } else {
      return true;
    }
  }
  
  /**
   * 
   * @param int $iSourceId
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @return int
   */
  public function deleteReservationBySourceId($iSourceId, $iOrderId, $iOrderItemId) {
    
    $aSourceColProvider = $this->pDbMgr->getTableRow('sources', $iSourceId, array('column_prefix_products_stock', 'id as source_id'), self::WEBSITE_CODE);
    $aCurrentReservation = $this->getColReservationArr(array('quantity', 'products_stock_id'), $iOrderItemId, $iOrderId);
    if ($this->checkIsColumnReservation($aSourceColProvider) && !empty($aCurrentReservation)) {
      try {
          //$this->pDbMgr->BeginTransaction(self::WEBSITE_CODE);
          $iNewToReservationQuantity = -$aCurrentReservation['quantity'];
          $this->updateReservationOnProductsStock($aSourceColProvider['column_prefix_products_stock'], $aCurrentReservation['products_stock_id'], $iNewToReservationQuantity);
          $this->deleteProductsStockReservaton($iOrderId, $iOrderItemId);
          //$this->pDbMgr->CommitTransaction(self::WEBSITE_CODE);
          return true;
      } catch (Exception $ex) {
        //$this->pDbMgr->RollbackTransaction(self::WEBSITE_CODE);
        throw $ex;
      }
    } else {
      return true;
    }
  }

  /**
   * 
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @param char $cMode '1' - zdejmij po zrealizowaniu zam, '2' zdejmij od razu po aktualizacji z pliku
   * @return boolean
   * @throws Exception
   */
  public function moveToERPReservation($iOrderId, $iOrderItemId, $cMode = '1') {
    
    $aValues = array(
        'move_to_erp' => $cMode
    );
    if ($this->pDbMgr->Update(self::WEBSITE_CODE, self::TABLE_NAME, $aValues, ' orders_items_id = '.$iOrderItemId.' AND order_id = '.$iOrderId) === false) {
      throw new Exception('Wystąpił błąd podczas przenoszenia do ERP rezerwacji '.print_r($aValues, true));
    }
    return true;
  }
  
  /**
   * 
   * @param int $iProviderId
   * @param int $iProductsStockId
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @param int $iQuantity
   */
  public function createReservation($iProviderId, $iProductsStockId, $iOrderId, $iOrderItemId, $iQuantity) {
    
    $aSourceColProvider = $this->getSourceColProvider($iProviderId);
    if ($this->checkIsColumnReservation($aSourceColProvider)) {
      try {
        //$this->pDbMgr->BeginTransaction(self::WEBSITE_CODE);
        $this->createReservationOnProductsStockReservation($aSourceColProvider['source_id'], $iProductsStockId, $iOrderId, $iOrderItemId, $iQuantity);
        $this->updateReservationOnProductsStock($aSourceColProvider['column_prefix_products_stock'], $iProductsStockId, $iQuantity);
        //$this->pDbMgr->CommitTransaction(self::WEBSITE_CODE);
      } catch (Exception $ex) {
        //$this->pDbMgr->RollbackTransaction(self::WEBSITE_CODE);
        throw $ex;
      }
    }
    return true;
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @return boolean
   * @throws Exception
   */
  private function deleteProductsStockReservaton($iOrderId, $iOrderItemId) {
    
    $sSql = 'DELETE FROM '.self::TABLE_NAME.' WHERE orders_items_id = '.$iOrderItemId.' AND order_id = '.$iOrderId;
    try {
      if ($this->pDbMgr->Query(self::WEBSITE_CODE, $sSql) <= 0) {
        throw new Exception('Błąd usuwania rezerwacji z tabeli '.self::TABLE_NAME);
      }
      return true;
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  
  /**
   * 
   * @param int $sProductsStockSourceColPrefix
   * @param int $iProductsStockId
   * @param int $iReservationQuantity
   * @throws Exception
   */
  private function updateReservationOnProductsStock($sProductsStockSourceColPrefix, $iProductsStockId, $iReservationQuantity) {
    $aReservationSource = $this->getProductsStockSourceReservationsCols($sProductsStockSourceColPrefix, $iProductsStockId);
    if (empty($aReservationSource)) {
      throw new Exception('Brak danych rezerwacyjnych produktu w tabeli products_stock ');
    }

    if ($this->updateReservationProductsStock($aReservationSource, $sProductsStockSourceColPrefix, $iProductsStockId, $iReservationQuantity) === false) {
      throw new Exception('Wystąpił błąd podczas zakładania rezerwacji w tabeli products_stock'. $iProductsStockId);
    }
    return true;
  }
  
  /**
   * 
   * @param array $aSourceColProvider
   * @return boolean
   */
  private function checkIsColumnReservation($aSourceColProvider) {
    if (!empty($aSourceColProvider) && in_array($aSourceColProvider['column_prefix_products_stock'], self::$RESERVATION_PROVIDERS)) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param array $aReservationSource
   * @param string $sProductsStockSourceColPrefix
   * @param int $iProductsStockId
   * @param int $iReservationQuantity
   */
  private function updateReservationProductsStock($aReservationSource, $sProductsStockSourceColPrefix, $iProductsStockId, $iReservationQuantity) {
    $aValues = array();
    $iSumReservations = ($aReservationSource['reservations'] + $iReservationQuantity);
    $aValues[$sProductsStockSourceColPrefix.'_reservations'] = $iSumReservations;
    $aValues[$sProductsStockSourceColPrefix.'_status'] = ($aReservationSource['act_stock'] - $iSumReservations);
    return $this->pDbMgr->Update(self::WEBSITE_CODE, 'products_stock', $aValues, ' id = '.$iProductsStockId);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @param int $iQuantity
   * @return bool
   */
  private function updateQuantityOnProductsStockReservation($iOrderId, $iOrderItemId, $iQuantity) {
    
    $aValues = array(
        'quantity' => $iQuantity
    );
    if ($this->pDbMgr->Update(self::WEBSITE_CODE, self::TABLE_NAME, $aValues, ' orders_items_id = '.$iOrderItemId.' AND order_id = '.$iOrderId) === false) {
      throw new Exception('Wystąpił błąd podczas zmiany rezerwacji '.print_r($aValues, true));
    }
    return true;
  }
  
  
  /**
   * 
   * @param int $iSourceId
   * @param int $iProductsStockId
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @param int $iQuantity
   * @return boolean
   * @throws Exception
   */
  private function createReservationOnProductsStockReservation($iSourceId, $iProductsStockId, $iOrderId, $iOrderItemId, $iQuantity) {
    
    $aValues = array(
        'source_id' => $iSourceId,
        'products_stock_id' => $iProductsStockId,
        'order_id' => $iOrderId,
        'orders_items_id' => $iOrderItemId,
        'quantity' => $iQuantity
    );
    if ($this->pDbMgr->Insert(self::WEBSITE_CODE, self::TABLE_NAME, $aValues, '', false) === false) {
      throw new Exception('Wystąpił błąd podczas zakładania rezerwacji na towary '.print_r($aValues, true));
    }
    return true;
  }
  
  /**
   * 
   * @param int $iProviderId
   * @return type
   */
  private function getSourceColProvider($iProviderId) {
    $sSql = 'SELECT column_prefix_products_stock, id as source_id
      FROM sources 
      WHERE provider_id = '.$iProviderId;
    return $this->pDbMgr->GetRow(self::WEBSITE_CODE, $sSql);
  }
  
  /**
   * 
   * @param string $sProductsStockSourceColPrefix
   * @param int $iProductsStockId
   */
  private function getProductsStockSourceReservationsCols($sProductsStockSourceColPrefix, $iProductsStockId) {
    $aReservationsCols = array('%s_status AS status', 
                               '%s_act_stock AS act_stock', 
                               '%s_reservations AS reservations');
    
    foreach ($aReservationsCols as $iKey => $sReservationCol) {
      $aReservationsCols[$iKey] = sprintf($sReservationCol, $sProductsStockSourceColPrefix);
    }
    return $this->getProductsStockByCols($aReservationsCols, $iProductsStockId);
  }
  
  /**
   * 
   * @param array $aColumns
   * @param int $iProductsStock
   * @return array
   */
  private function getProductsStockByCols($aColumns, $iProductsStock) {
    $sSql = 'SELECT '.implode(',', $aColumns).'
             FROM products_stock
             WHERE id = '.$iProductsStock;
    return $this->pDbMgr->GetRow(self::WEBSITE_CODE, $sSql);
  }
  
  /**
   * 
   * @param array $aCols
   * @param int $iOrderItemId
   * @param int $iOrderId
   * @return int
   */
  private function getColReservationArr(array $aCols, $iOrderItemId, $iOrderId) {
    
    $sSql = 'SELECT '.implode(', ', $aCols).' FROM '.self::TABLE_NAME .' 
             WHERE 
              orders_items_id = '.$iOrderItemId. ' AND 
              order_id = '.$iOrderId;
    return $this->pDbMgr->GetRow(self::WEBSITE_CODE, $sSql);
  }
}
