<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-26 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace omniaCMS\lib\Products;

use Common;
use DatabaseManager;
/**
 * Description of ProductsStock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ProductsStock {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function __construct($pDbMgr) {
    
    $this->pDbMgr = $pDbMgr;
  }

  /**
   * 
   * @param int $iProductId
   * @return array
   */
  public function getProductsStockViewData($iProductId) {
    $sSql = "SELECT S.*,
                DATE_FORMAT(azymut_shipment_date, '%d.%m.%y') AS azymut_shipment_date,  
                DATE_FORMAT(ateneum_shipment_date, '%d.%m.%y') AS ateneum_shipment_date,  
                DATE_FORMAT(abe_shipment_date, '%d.%m.%y') AS abe_shipment_date,  
                DATE_FORMAT(helion_shipment_date, '%d.%m.%y') AS helion_shipment_date, 
                DATE_FORMAT(dictum_shipment_date, '%d.%m.%y') AS dictum_shipment_date, 
                DATE_FORMAT(siodemka_shipment_date, '%d.%m.%y') AS siodemka_shipment_date,  
                DATE_FORMAT(olesiejuk_shipment_date, '%d.%m.%y') AS olesiejuk_shipment_date,
                DATE_FORMAT(platon_shipment_date, '%d.%m.%y') AS platon_shipment_date
              FROM products_stock S
            WHERE id = ".$iProductId; 
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
  
  
  /**
   * 
   * @param array $aProductsStockData
   * @param array $aSources
   * @return array
   */
  protected function formateStockTemplateData(array $aProductsStockData, array $aSources) {
    $iMinWholsalePrice = 0;
    $aNewSourceArray = array();
    $sMinSource = '';
    
    foreach ($aSources as $aSource) {
      $sSourceColPrefix = $aSource['column_prefix_products_stock'];
      $sSourceName = $aSource['symbol'];
      if ($this->checkShowStockItem($aProductsStockData, $sSourceColPrefix) === true) {
        if (stristr($sSourceColPrefix, 'profit')) {
          $aNewSourceArray['profit'][$sSourceName] = $this->getSingleSourceCols($aProductsStockData, $sSourceColPrefix);
          $aNewSourceArray['profit'][$sSourceName]['supplies'] = $this->addOurDiscount($this->getLimitedDelivers($aSource['id'], $aProductsStockData['id']));
        } else {
          $aNewSourceArray['external'][$sSourceName] = $this->getSingleSourceCols($aProductsStockData, $sSourceColPrefix);
          if ($aProductsStockData[$sSourceColPrefix.'_wholesale_price'] > 0) {
            if ($iMinWholsalePrice == 0) {
              $iMinWholsalePrice = $aProductsStockData[$sSourceColPrefix.'_wholesale_price'];
              $sMinSource = $sSourceColPrefix;
            } elseif ($iMinWholsalePrice > $aProductsStockData[$sSourceColPrefix.'_wholesale_price']) {
              $iMinWholsalePrice = $aProductsStockData[$sSourceColPrefix.'_wholesale_price'];
              $sMinSource = $sSourceColPrefix;
            }
          }
        }
      }
    }
    if ($sMinSource != '') {
      $aNewSourceArray['external'][$sMinSource]['source_min_wh_price'] = $iMinWholsalePrice;
    }
    return $aNewSourceArray;
  }
  
  /**
   * 
   * @param array $aSupplies
   * @return array
   */
  private function addOurDiscount($aSupplies) {
    
    foreach ($aSupplies as $iKey => $aSupply) {
      if ($this->checkUserPrivCommercial() === false) {
        unset($aSupplies[$iKey]['wholesale_price']);
      } else {
        $aSupplies[$iKey]['our_discount'] = $this->getSourceOurDiscount($aSupply);
      }
    }
    return $aSupplies;
  }
  
  /**
   * 
   * @param int $iSourceId
   * @param int $iProductId
   */
  private function getLimitedDelivers($iSourceId, $iProductId) {
    
    $sSql = 'SELECT * 
             FROM products_stock_supplies AS PSS 
             WHERE product_id = ' . $iProductId . ' AND
                   source = "' . $iSourceId . '"
             ORDER BY wholesale_price ASC ';
                   
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param array $aProductsStockData
   * @param string $sSourceColPrefix
   * @return boolean
   */
  private function checkShowStockItem(array $aProductsStockData, $sSourceColPrefix) {
    
    if (($aProductsStockData[$sSourceColPrefix.'_status'] > 0 || ($sSourceColPrefix == 'olesiejuk' && $aProductsStockData['olesiejuk_stock'] != '')) || 
            (isset($aProductsStockData[$sSourceColPrefix.'_act_stock']) && $aProductsStockData[$sSourceColPrefix.'_act_stock'] > 0) || 
            (isset($aProductsStockData[$sSourceColPrefix.'_reservations']) && $aProductsStockData[$sSourceColPrefix.'_reservations'] > 0) || 
            (isset($aProductsStockData[$sSourceColPrefix.'_wholesale_price']) && $aProductsStockData[$sSourceColPrefix.'_wholesale_price'] > 0)
            ) {
      return true;
    } else {
      return false;
    }
  }
  
  
  /**
   * 
   * @param array $aProductStockData
   * @param string $sSourceColPrefix
   * @return array
   */
  private function getSingleSourceCols(array $aProductStockData, $sSourceColPrefix) {
    $aSourceStockData = array();
    $bUserPrivCommercial = $this->checkUserPrivCommercial();
    
    foreach ($aProductStockData as $sColName => $sColValue) {
      if (stristr($sColName, $sSourceColPrefix)) {
        $sExColName = str_replace($sSourceColPrefix.'_', '', $sColName);
        if ($bUserPrivCommercial === true) {
          $aSourceStockData[$sExColName] = $sColValue;
        } else {
          if ($sExColName != 'wholesale_price') {
            $aSourceStockData[$sExColName] = $sColValue;
          }
        }
      }
    }
    $aSourceStockData['our_discount'] = $this->getSourceOurDiscount($aSourceStockData);
    return $aSourceStockData;
  }
  
  private function checkUserPrivCommercial() {
    
    if (isset($_SESSION['user']['priv_commercial_data']) && $_SESSION['user']['priv_commercial_data'] == '1') {
      return true;
    }
    return false;
  }
  
  /**
   * 
   * @param array $aSourceStockData
   */
  private function getSourceOurDiscount(array $aSourceStockData) {
    
    if (isset($aSourceStockData['wholesale_price']) && 
        isset($aSourceStockData['price_brutto']) && 
        $aSourceStockData['wholesale_price'] > 0
            ) {
      $fWholesalePrice = $aSourceStockData['wholesale_price'];
      $fPriceBrutto = $aSourceStockData['price_brutto'];
      // mamy cenę zakupu, obliczamy nasz rabat 
      if ($fPriceBrutto > 0.00) {
        return Common::formatPrice2(100 - (($fWholesalePrice * 100) / $fPriceBrutto));
      } else {
        return 100;
      }
    }
    return null;
  }
}
