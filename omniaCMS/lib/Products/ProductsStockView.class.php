<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-26 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\lib\Products;

use DatabaseManager;
use Smarty;

/**
 * Description of ProductsStockView
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ProductsStockView extends ProductsStock {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var Smarty
   */
  private $pSmarty;
  
  /**
   *
   * @var bool
   */
  private $bHideOnStart = false;

  /**
   * 
   * @param DatabaseManager $pDbMgr
   * @param Smarty $pSmarty
   */
  public function __construct($pDbMgr, $pSmarty) {
    parent::__construct($pDbMgr);
    $this->pSmarty = $pSmarty;
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @param array $aProductsStockData
   * @param array $aSources
   * @return string - HTML
   */
  public function getMultipleStockView(array $aProductsStockData, array $aSources) {
    
    $sHTML = '';
    foreach ($aProductsStockData as $iProductId => $aProductsStockDataItem) {
      if (!empty($aProductsStockDataItem) && is_array($aProductsStockDataItem)) {
        $aProductsStocksTypes[$iProductId] = $this->formateStockTemplateData($aProductsStockDataItem, $aSources);
      }
      $sHTML .= $this->getParseStockView($aProductsStocksTypes);
    }
    return $sHTML;
  }

  /**
   *
   * @param int $iProductId
   * @param array $aSources
   * @return string
   */
  public function getProductStockView($iProductId, array $aSources) {
    
    $aProductsStockDataItem = $this->getProductsStockViewData($iProductId);
    if (!empty($aProductsStockDataItem) && is_array($aProductsStockDataItem)) {
      $aProductsStocksTypes[$iProductId] = $this->formateStockTemplateData($aProductsStockDataItem, $aSources);
      return $this->getParseStockView($aProductsStocksTypes);
    }
    return 'BRAK STANÓW';
  }
  
  /**
   * 
   */
  public function setHideOnStart() {
    $this->bHideOnStart = true;
  }
  
  /**
   * 
   * @return array
   */
  public function getParametrs() {
    $aParametrs = array(
        'bHideOnStart' => $this->bHideOnStart
    );
    return $aParametrs;
  }
  
  /**
   * 
   * @param array $aProductsStocksTypes
   * @return string HTML
   */
  private function getParseStockView(array $aProductsStocksTypes) {
    
    $this->pSmarty->assign('aParametrs', $this->getParametrs());
    $this->pSmarty->assign('aProductsStocksTypes', $aProductsStocksTypes);
    $sStockHtml = $this->pSmarty->fetch('stocks.tpl');
    $this->pSmarty->clear_assign('aProductsStocksTypes');
    $this->pSmarty->clear_assign('aParametrs');
    return $sStockHtml;
  }
  
}
