<?php
class Lib_FreeTransportPromo {
	private $aTransportOptions;
	private $aTimeTransportOptions;
	private $sWebsite;
	
	public function __construct($sWebsiteSymbol = 'profit24') {
		$this->sWebsite = $sWebsiteSymbol;
		// ustawienie tablic darmowej dostawy
		$this->getFreeDeliveryOptions();
	}


	/**
	 * Metoda pobiera wszystkie opcje transportu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @return type 
	 */
	function getTransportMethods() {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT symbol, no_costs_from 
						 FROM ".$aConfig['tabls']['prefix']."orders_transport_means";
		return $pDbMgr->GetAll($this->sWebsite, $sSql);
	}// end of getFreeTransportMethods() method
	
	
	/**
	 * Metoda pobiera opcje transportu których darmowa dostawa jest aktualnie aktywna 
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @return type 
	 */
	function getTimeTransportMethods() {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT symbol, date_no_costs_from
						 FROM ".$aConfig['tabls']['prefix']."orders_transport_means
						 WHERE date_no_costs_from > 0.00 AND 
									 UNIX_TIMESTAMP(date_start_date) <= UNIX_TIMESTAMP(NOW()) AND 
									 UNIX_TIMESTAMP(date_end_date) >= UNIX_TIMESTAMP(NOW())";
		return $pDbMgr->GetAll($this->sWebsite, $sSql);
	} // end of getTimeTransportMethods() method
	
	
	/**
	 * Metoda sprawdza czy pozycja posiada darmową przesyłkę
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $iProductId
	 * @param type $fPriceBrutto 
	 */
	function getFreeDeliveryOptions() {
		
		// darmowa przesyłka dla metody transportu
		$this->aTransportOptions = $this->getTransportMethods();
		$this->aTimeTransportOptions = $this->getTimeTransportMethods();
	}// end of getFreeDeliveryOptions() method
	
	
	/**
	 * Metoda pobiera darmowe formy transportu dla produktu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr 
	 */
	function getBookFreeTransport($iProductId, $fPriceBrutto) {
		
		// darmowa przesyłka - czasowa darmowa przesyłka
		$aTimeFreeTransportSymbol = $this->checkFreeTimeTransport($this->aTimeTransportOptions, $fPriceBrutto);
		// darmowa przesyłka - globalna na metodę transportu
		$aFreeTransportSymbol = $this->checkFreeTransport($this->aTransportOptions, $fPriceBrutto);
		// darmowa przesyłka - globalna na metodę transportu
		$aFreeProductTransportSymbol = $this->checkProductFreeTransport($iProductId);
		$aFreeTransportSymbol = array_merge($aFreeTransportSymbol, $aTimeFreeTransportSymbol);
		$aFreeTransportSymbol = array_merge($aFreeTransportSymbol, $aFreeProductTransportSymbol);
		$aFreeTransportSymbol = array_unique($aFreeTransportSymbol);
		return $aFreeTransportSymbol;
	}// end of getBookFreeTransport() method
	
	
	/**
	 * Metoda sprawdza czy dla podanego produktu wybrana jest darmowa przesyłka
	 *
	 * @global array $aConfig
	 * @global object $pDbMgr
	 * @param integer $iProductId
	 * @return array 
	 */
	function checkProductFreeTransport($iProductId) {
		global $aConfig, $pDbMgr;
		
		if (intval($iProductId) <= 0) return array();
		
		$sSql = "SELECT DISTINCT D.symbol FROM ".$aConfig['tabls']['prefix']."products_free_delivery AS A
						 JOIN ".$aConfig['tabls']['prefix']."products_free_delivery_items AS B
							 ON A.id = B.products_free_delivery_id
						 JOIN ".$aConfig['tabls']['prefix']."products_free_delivery_transport_method AS C
							 ON A.id = C.free_delivery_id
						 JOIN ".$aConfig['tabls']['prefix']."orders_transport_means AS D
							 ON C.transport_id = D.id
             JOIN ".$aConfig['tabls']['prefix']."orders_payment_types AS E
               ON D.id = E.transport_mean AND E.published = '1' AND E.deleted = '0'
						 WHERE B.product_id = ".$iProductId." AND 
									 UNIX_TIMESTAMP(A.start_date) <= UNIX_TIMESTAMP(NOW()) AND 
									 UNIX_TIMESTAMP(A.end_date) >= UNIX_TIMESTAMP(NOW())
									 ";
		return $pDbMgr->GetCol($this->sWebsite, $sSql);
	}// end of checkProductFreeTransport() method
	
	
	/**
	 * Metoda sprawdza czy podana cena książki jest niższa niż każda "cena od"
	 *	z pzekazanych metod płatności z tablicy
	 *
	 * @param array $aTransportOptions
	 * @param float $fPriceBrutto 
	 * @return array - tablica symboli darmowych metod transportu dla produktu
	 */
	function checkFreeTransport($aTimeTransportOptions, $fPriceBrutto) {
		$aSymbol = array();

		foreach ($aTimeTransportOptions as $aTransportOption) {
			if ($aTransportOption['no_costs_from'] > 0.00 && $aTransportOption['no_costs_from'] <= $fPriceBrutto) {
				$aSymbol[] = $aTransportOption['symbol'];
			}
		}
		return $aSymbol;
	}// end of checkFreeTransport() method
	
	
	/**
	 * Metoda sprawdza czy podana cena książki jest niższa niż każda "cena od" 
	 *	z przekazanych metod płatności z tablicy
	 *
	 * @param array $aTimeTransportOptions
	 * @param float $fPriceBrutto
	 * @return array - tablica symboli darmowych metod transportu dla produktu
	 */
	function checkFreeTimeTransport($aTimeTransportOptions, $fPriceBrutto) {
		$aSymbol = array();
		
		foreach ($aTimeTransportOptions as $aTransportOption) {
			if ($aTransportOption['date_no_costs_from'] > 0.00 && $aTransportOption['date_no_costs_from'] <= $fPriceBrutto) {
				$aSymbol[] = $aTransportOption['symbol'];
			}
		}
		return $aSymbol;
	}// end of checkFreeTimeTransport() method
	
	
	/**
	 * Metoda na podstawie tablicy przesłanych symboli dołącza logo i nazwę
	 *
	 * @param type $aFreeTransportSymbol 
	 */
	function getLogoNameBySymbol($aFreeTransportSymbol) {
		global $aConfig, $pDbMgr;
		
		
		if (isset($aFreeTransportSymbol) && !empty($aFreeTransportSymbol)) {
			$sSql = "SELECT DISTINCT D.symbol, D.logo
							FROM ".$aConfig['tabls']['prefix']."orders_transport_means AS D
              JOIN ".$aConfig['tabls']['prefix']."orders_payment_types AS E
               ON D.id = E.transport_mean AND E.published = '1' AND E.deleted = '0'
							WHERE D.symbol IN ('".implode($aFreeTransportSymbol, "', '")."')";
			$aData = $pDbMgr->GetAll($this->sWebsite, $sSql);
			foreach ($aData as $iKey => $aItem) {
				$aData[$iKey]['logo'] = '/'.$aConfig['common']['transport_logos_dir'].'/'.$aItem['logo'];
				$aData[$iKey]['name'] = $aConfig['lang']['delivery'][$aItem['symbol']];
			}
			return $aData;
		}
		return NULL;
	}// end of getLogoNameBySymbol() method
}
?>
