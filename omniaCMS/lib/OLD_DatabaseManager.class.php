<?php


class DatabaseManager {
	
	var $aDBConfig;
	
	var $aDBConn;

	function DatabaseManager(){
		global $aConfig;
		$this->aDBConn = array();
		
		$this->aDBConfig = $this->getConfiguration();
		
		if(!empty($this->aDBConfig)){
			foreach($this->aDBConfig as $sKey=>$aConf){
				// dsn
				$sDSN = $aConfig['db']['dbns'].'://'.$aConf['db_login'].':'
				.$aConf['db_password'].'@'.$aConf['db_host'].'/'.$aConf['db_name'];
				// połączenie
				$this->aDBConn[$sKey] =& DB::connect($sDSN);

				if (DB::isError($this->aDBConn[$sKey])) {
					if ($aConfig['common']['status'] == 'development') {
						TriggerError("Could not connect to database ".$aConf['name']."!");
					}
				}
				$this->aDBConn[$sKey]->query("SET CHARACTER SET utf8");
				$this->aDBConn[$sKey]->query("SET NAMES utf8");
			}
		}
		//dump($this->aDBConfig);
	}
	
	/**
	 * Metoda pobiera konfigurację baz danych
	 * @return array
	 */
	function getConfiguration(){
		global $aConfig;
		
		$sSql="SELECT code, name, db_host, db_name, db_login, db_password
					FROM ".$aConfig['tabls']['prefix']."websites";
		return Common::GetAssoc($sSql);
	}
	
	function getDatabaseName($sDB){
		if(empty($this->aDBConfig[$sDB]))
			return false;
		else
			return $this->aDBConfig[$sDB]['name'];
		
	}
	
  /**
   * Metoda pobiera ostatnią klasę pomijając klasy dostępu do bazy danych
   * 
   * @param array $aData
   * @return int
   */
  static function _getDebugLastClass($aData) {
    foreach ($aData as $iKey => $mClass) {
      if (isset($mClass['file']) && 
          $mClass['file'] !== '' &&
          stristr($mClass['file'], 'lib/DatabaseManager') === false &&
          stristr($mClass['file'], 'lib/Common') === false
          ) {
        if (isset($aData[$iKey+1])) {
          return ($iKey + 1);
        } else {
          return $iKey;
        }
      }
    }
    return $iKey;
  }// end of _getDebugLastClass() method
  
  
  /**
   * Metoda pobiera komentarz zapytania SQL
   * 
   * @return string
   */
  static function _getDebugCommentSQL() {
    $sComment = '';
            
    $aData = debug_backtrace();
    $iKey = self::_getDebugLastClass($aData);
    if (isset($aData[$iKey])) {
      $aObjData = $aData[$iKey];
      if (!empty($aData) && !empty($aObjData)) {
        $sFile = '...'.substr($aObjData['file'], -30);
        $sLine = '['.$aObjData['line'].']';
        $sFunction = '  '.$aObjData['function'].'';
        if (is_object($aObjData['object'])) {
          $sObject = ''.get_class($aObjData['object']).'';
        }
        $sComment .= $sFile.$sLine.' - '.$sObject.'::'.$sFunction;
      }
      if (!empty($_SERVER['REMOTE_ADDR'])) {
        $sComment .= ' IP:'.$_SERVER['REMOTE_ADDR'];
      }
    }
    
    $sComment = '/*'.$sComment.'*/';
    return $sComment;
  }// end of _getDebugCommentSQL() method
  
  
	/**
   * Metoda wykonuje zapytanie na bazie danych, zwraca liczbe zmienionych
   * rekordow
   *
   * @param		string		$sDB							- symbol bazy
   * @param   string    $sSQL             - zapytanie SQL
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @return  integer                     - liczba zmienionych rekordow
   */
  function Query($sDB, $sSQL, $mParameters=array()) {
    global $aConfig;

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    
    if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }
    $pResult = $this->aDBConn[$sDB]->query($sSQL, $mParameters);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
    }
    return $this->aDBConn[$sDB]->AffectedRows();
  } // end Query()

  /**
   * Metoda wykonuje zapytanie na bazie danych ograniczajac wyswietlenie
   * wynikow zapytania do podanej ilosci zaczynajac od numeru podanego rekordu
   *
   * @param		string		$sDB							- symbol bazy
   * @param   string    $sSQL             - zapytanie SQL
   * @param		integer		$iFrom						- poczatkowy rekord zbioru wynikow
   * @param		integer		$iCount						- liczba zwroconych rekordow
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @param   integer   $iFetchMode       - metoda pobierania z bazy
   * @return  integer                     - liczba zmienionych rekordow
   */
  function LimitQuery($sDB, $sSQL, $iFrom, $iCount, $mParameters=array(), $iFetchMode=DB_FETCHMODE_ASSOC) {
  	global $aConfig;
  	
  	$aTempArr		= array();
  	$aResult		= array();
  	
  	if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }
    
    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult = $this->aDBConn[$sDB]->limitQuery($sSQL, $iFrom, $iCount, $mParameters);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
    }
    while ($aTempArr =& $pResult->FetchRow($iFetchMode)) {
      $aResult[] =& $aTempArr;
    }
    return $aResult;
  } // end LimitQuery()

  
  /**
   * Metoda wykonuje zapytanie na bazie danych, dodajac nowy rekord
   * do tabeli $sTable, zawierajacy pola i wartosci z tablicy $aValues.
   * Jezeli w tej tablicy $aValues znajduje sie rekord o wartosci #id#
   * zostaje pobierana wartosc dla nowego klucza dla warunku $sWhere.
   * Jezeli czwarty parametr ma wartosc true metoda zwraca ID nowododanego
   * rekordu (tabel musi posiadac pole ID)
   *
   * @param		string		$sDB							- symbol bazy
   * @param	string	$sTable	- nazwa tabeli
   * @param	array	$aValuesList	- lista wartosci do wstawienia
   * @param	string	$sWhere	- czesc zapytania WHERE
   * @param	bool	$bReturnId	- true: zwroc Id dodanego rekordu; false: nie zwracaj
   * @return	integer	- ID nowo dodanego rekordu lub false w przypadku bledu
   */
  function Insert($sDB, $sTable, $aValuesList, $sWhere='', $bReturnId=true) {
    global $aConfig;
    $iId = null;
    
    $sFields = implode(',', array_keys($aValuesList));
    $aValues = array();
    $sTable = $this->aDBConn[$sDB]->quoteIdentifier($sTable);

    foreach ($aValuesList as $mValue) {
    	if (preg_match('/^#(\w+)#$/i', $mValue, $aMatches)) {
    		// okreslenie nowej wartosci dla kolumny
    		$sSql = "SELECT IFNULL(MAX(".$aMatches[1]."), 0) + 1 FROM ".$sTable.
    						($sWhere != '' ? " WHERE ".$sWhere : '');
    		if (($mValue = $this->GetOne($sDB,$sSql)) === false) {
    			return false;
    		}
    		$mValue = (int) $mValue;
    		if ($aMatches[1] == 'id') {
    			$iId = $mValue;
    		}
    	}
    	switch ($mValue) {
    		case 'NULL':
    		case 'NOW()':
    			$aValues[] = $mValue;
    		break;

    		default:
    			$aValues[] = $this->aDBConn[$sDB]->quoteSmart(stripslashes($mValue));
    		break;
    	}
    }
    $sValues = implode(',', $aValues);

    // wstawianie wiersza
    $sSql = "INSERT INTO ".$sTable." (".$sFields.") VALUES(".$sValues.")";
    if ($this->Query($sDB,$sSql) === false) {
    	return false;
    }
    

    if ($bReturnId) {
    	if (!$iId) {
	    	// pobranie ID wstawionego rekordu
		    $sSql = "SELECT MAX(id) FROM ".$sTable.
								($sWhere != '' ? " WHERE ".$sWhere : '');
				if (($iId = $this->GetOne($sDB,$sSql)) === false) {
					return false;
				}
    	}
			return (int) $iId;
    }
    return true;
  } // end Insert() method


  /**
   * Metoda wykonuje zapytanie na bazie danych, wprowadzajac zmiany
   * w rekordzie odpowiadajacym zapytaniu $sWhere
   *
   * @param		string		$sDB							- symbol bazy
   * @param	string	$sTable	- nazwa tabeli
   * @param	array	$aValuesList	- lista wartosci do modyfikacji
   * @param	string	$sWhere	- czesc zapytania WHERE
   * @return	mixed - integer - liczba zmodyfikowanych rekordow; false: wystapil blad
   */
  function Update($sDB, $sTable, $aValuesList, $sWhere='') {
    global $aConfig;
    
    $sValuesList = '';
    $sTable = $this->aDBConn[$sDB]->quoteIdentifier($sTable);

    foreach ($aValuesList as $sKey => $mValue) {
    	$sValuesList .= $sKey.' = ';
    	switch ($mValue) {
    		case 'NULL':
    		case 'NOW()':
    			$sValuesList .= $mValue;
    		break;

    		default:
    			$sValuesList .= $this->aDBConn[$sDB]->quoteSmart(stripslashes($mValue));
    		break;
    	}
    	$sValuesList .= ', ';
    }
    $sValuesList = substr($sValuesList, 0, -2);

    // aktualizacja
    $sSql = "UPDATE ".$sTable." SET ".$sValuesList.
						 ($sWhere != '' ? " WHERE ".$sWhere : '');
	
				 
		return $this->Query($sDB,$sSql);
  } // end Update() method

  /**
   * Metoda wykonuje zapytanie na bazie danych, zwraca wartosc
   * pierwszej kolumny z pierwszego rekordu
   *
   * @param		string		$sDB							- symbol bazy
   * @param   string    $sSQL             - zapytanie SQL
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @return  mixed                       - zawartosc pierwszej kolumny pierwszego rekordu lub false
   */
  function &GetOne($sDB, $sSQL, $mParameters=array()) {
    global $aConfig;
    
  	if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }
    
    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $this->aDBConn[$sDB]->getOne($sSQL, $mParameters);
    if (PEAR::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
    }
    return $pResult;
  } // end GetOne()


  /**
   * Metoda zwraca pierwszy wiersz wyniku zapytania
   *
   * @param		string		$sDB							- symbol bazy
   * @param   string    $sSQL             - zapytanie SQL
   * @param   array     $aParameters      - parametry dla zapytania
   * @param   integer   $iFetchMode       - metoda pobierania z bazy
   * @return  array                       - tabela zawierajaca pierwszy rekord wyniku zapytania
   */
  function &GetRow($sDB, $sSQL, $aParameters=array(), $iFetchMode=DB_FETCHMODE_ASSOC) {
    global $aConfig;

  	if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }
    
    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $this->aDBConn[$sDB]->getRow($sSQL, $aParameters, $iFetchMode);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
    }
    return $pResult;
  } // GetRow()


  /**
   * Metoda zwraca wskazana kolumne wyniku zapytania
   *
   * @param		string		$sDB							- symbol bazy
   * @param   string    $sSQL             - zapytanie SQL
   * @param   mixed     $mColumn          - numer (zaczynajac od 0) lub nazwa kolumny
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @return  array                       - tabela zawierajaca wskayana kolumne wyniku zapytania
   */
  function &GetCol($sDB, $sSQL, $mColumn=0, $mParameters=array()) {
    global $aConfig;
    
  	if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $this->aDBConn[$sDB]->getCol($sSQL, $mColumn, $mParameters);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
      return false;
    }
    return $pResult;
  } // GetCol()


  /**
   * Metoda zwraca wynik zapytania w postaci tablicy asocjacyjnej
   * Jezeli wynik zapytania zawiera wiecej niz 2 kolumny, wynikiem bedzie tablica
   * wartosci zaczynajac od kolumny drugiejdo n-tej. Jezeli wynik zawiera tylko 2 kolumny,
   * zwracana wartoscia bedzie skalar z wartoscia drugiej kolumny (z wyjatkiem sytuacji
   * gdy wymuszony jest zwrot tablicy poprzez parametr $bForceArray).
   *
   * @param		string		$sDB							- symbol bazy
   * @param   string    $sSQL             - zapytanie SQL
   * @param   boolean   $bForceArray      - wymuszaj wynik w postaci tablicy
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @param   integer   $iFetchMode       - metoda pobierania z bazy
   * @param   boolean   $bGroup           - grupuj wyniki dla tej samego klucza w tablice
   * @return  array                       - tabela zawierajaca wskayana kolumne wyniku zapytania
   */
  function &GetAssoc($sDB, $sSQL, $bForceArray=false, $mParameters=array(), $iFetchMode=DB_FETCHMODE_ASSOC, $bGroup=false) {
    global $aConfig;
    
  	if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }

    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $this->aDBConn[$sDB]->getAssoc($sSQL, $bForceArray, $mParameters, $iFetchMode, $bGroup);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
    }
    return $pResult;
  } // GetAssoc()


  /**
   * Metoda wykonuje zapytanie na bazie danych i zwraca rekordy wyniku
   * w postaci tablicy
   *
   * @param		string		$sDB							- symbol bazy
   * @param   string    $sSQL             - zapytanie SQL
   * @param   array     $aParameters      - parametry dla zapytania
   * @param   integer   $iFetchMode       - metoda pobierania z bazy
   * @return  array                       - tabela zawierajaca rekordy wyniku zapytania
   */
  function &GetAll($sDB, $sSQL, $aParameters=array(), $iFetchMode=DB_FETCHMODE_ASSOC) {
    global $aConfig;
    
  	if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }
    
    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $this->aDBConn[$sDB]->getAll($sSQL, $aParameters, $iFetchMode);
    if (PEAR::isError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
    }
    return $pResult;
  } // end GetAll()
  
  
   /**
   * Metoda wykonuje zapytanie na bazie danych, obiekt zapytania
   *
   * @param		string		$sDB							- symbol bazy
   * @param   string    $sSQL             - zapytanie SQL
   * @param   mixed     $mParameters      - parametry dla zapytania
   * @return  integer                     - liczba zmienionych rekordow
   */
  function PlainQuery($sDB, $sSQL, $mParameters=array()) {
    global $aConfig;
    
  	if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }
    
    if (!empty($sSQL)) {
      $sCommentSQL = self::_getDebugCommentSQL();
      $sSQL = $sSQL.$sCommentSQL;
    }
    $pResult =& $this->aDBConn[$sDB]->query($sSQL, $mParameters);
    if (DB::IsError($pResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($pResult->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
    }
    return $pResult;
  } // end PlainQuery() 
  


  /**
   * Metoda zwraca ostatni dodane ID z kolumny o atrybucie AUTO_INCREMENT
   *
   * @param		string		$sDB							- symbol bazy
   * @return  integer                       - ostatnio dodane ID
   */
  function &GetLastInsertedID($sDB) {
    global $aConfig;
		
  	if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }

    return $this->aDBConn[$sDB]->GetOne("SELECT LAST_INSERT_ID()");
  } // end GetLastInsertedID()


  /**
   * Metoda rozpoczyna transakcje w bazie danych
   *
   * @return	void
   */
  function BeginTransaction($sDB) {
  	global $aConfig;
		
  	if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }

  	$this->aDBConn[$sDB]->query('BEGIN');
	} // end of BeginTransaction() function


	/**
   * Metoda zatwierdza transakcje w bazie danych
   *
   * @return	void
   */
  function CommitTransaction($sDB) {
  	global $aConfig;
		
  	if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }

  	$this->aDBConn[$sDB]->query('COMMIT');
	} // end of CommitTransaction() function


	/**
   * Metoda wycofuje transakcje w bazie danych
   *
   * @return	void
   */
  function RollbackTransaction($sDB) {
  	global $aConfig;
		
 		if(!DB::isConnection($this->aDBConn[$sDB])){
    	if ($aConfig['common']['status'] == 'development') {
				TriggerError("Database ".$sDB." not connected!", E_USER_NOTICE);
			}
			return false;
    }

  	$this->aDBConn[$sDB]->query('ROLLBACK');
	} // end of RollbackTransaction() function
}

?>