<?php
/**
 * Klasa MySort - sortowanie rekordow
 *
 * @author Marcin Korecki <korecki@omnia.pl>
 * @copyright 2007
 * @version 1.0
 */
class MySort {

	/**
	 * Konstruktor
	 * 
	 * @param 	
	 * @return 	void
	 **/
	function MySort() { } // end of MySort() method
	
	/**
	 * Metoda wyswietla liste zdefiniowanych dla strony bannerow do sortowania
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Show($sName, &$aRecords, $sURL) {
		global $aConfig;
		
		// dolaczenie klasy SortView
		include_once('SortView/SortListView.class.php');
		// obiekt klasy SortListView
		$oView = new SortListView('items',
															$sName,
															$aRecords,
															$sURL);

		return $oView->Show();
	} // end of Sort() function
	
	
	/**
	 * Metoda ustawia nowa kolejnosc rekordow
	 *
	 * @return	void
	 */
	function Proceed($sTable, &$aRecords, $aFields, $sOrderField='order_by', $sIdField='id') {
		global $aConfig;
				
		// ustawienie zmiennej zapobiegajacej parsowaniu szablonu 'index.tpl'
		$_GET['hideHeader'] = 1;
		
		if (!isset($_POST['sortList']) || !is_array($_POST['sortList'])) {
			echo '0';
			return;
		}
		
		if (!empty($aRecords)) {
			// rozpoczecie transakcji
			Common::BeginTransaction();
			
			// aktualizacja numerow porzadkowych wszystkich rekordow
			$iOrder = 1;
			foreach ($_POST['sortList'] as $iId) {
        if (!array_key_exists($iId, $aRecords)) continue;
        
				$sSql = "UPDATE ".$sTable."
								 SET ".$sOrderField." = ".($iOrder++)." 
								 WHERE ".$sIdField." = ".$iId;
				foreach ($aFields as $sName => $mValue) {
					$sSql .= " AND ".$sName.(!is_null($mValue) ? " = ".$mValue : ' IS NULL');
				}

				if (Common::Query($sSql) === false) {
					// wystapil blad, wycofanie transakcji, zwrocenie wartosci '0'
					Common::RollbackTransaction();
					echo '0';
					return;
				}
      }		

			// zamiana sie powiodla, zatwierdzenie transakcji, zwrocenie '1'
			Common::CommitTransaction();
			echo '1';
			return;
		}
		else {
			echo '0';
			return;
		}
	} // end of ProceedSort() function
} // end of class MySort
?>