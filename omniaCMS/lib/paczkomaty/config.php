<?php

/*

Main config file.

Copyright (c) 2009 InPost

*/

global $inpost_data_dir, $inpost_api_url, $aConfig;

$inpost_data_dir = $aConfig['common']['client_base_path'].'/'.$aConfig['common']['cms_dir'].'lib/paczkomaty/data';
$inpost_api_url  = 'http://api.paczkomaty.pl';

?>
