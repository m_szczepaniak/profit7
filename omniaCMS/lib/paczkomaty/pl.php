<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */

$aLang['status']['ClaimProcessed'] = 'Rozpatrzono zgłoszenie reklamacyjne';
$aLang['status']['Claimed'] = 'Przyjęto zgłoszenie reklamacyjne';
$aLang['status']['Cancelled'] = 'Anulowana';
$aLang['status']['RetunedToAgency'] = 'Przekazana do oddziału';
$aLang['status']['Delivered'] = 'Dostarczona';
$aLang['status']['Expired'] = 'Nie odebrana';
$aLang['status']['Avizo'] = 'Ponowne avizo';
$aLang['status']['Stored'] = 'Oczekuje na odbiór';
$aLang['status']['InTransit'] = 'Przesyłka w drodze';
$aLang['status']['Sent'] = 'Przesyłka nadana';
$aLang['status']['Prepared'] = 'Gotowa do wysyłki';
$aLang['status']['Created'] = 'Oczekuje na wysyłkę';