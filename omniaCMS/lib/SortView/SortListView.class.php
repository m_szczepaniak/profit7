<?php
/**
 * Klasa SortistView do obslugi widoków z bazy danych w postaci listy UL
 * umozliwiajacej sortowanie rekordow z uzyciem biblioteki Script.culo.us
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2007
 * @version   1.0
 */

class SortListView {

  // nazwa widoku
	var $sName;
	
	// nazwa strony dla ktorej jest sortowanie
	var $sPageName;
		
	// tabela zawierajaca ustawienia tabeli
	var $aViewAttribs;
	
	// tablica zawierajaca liste rekordow
  var $aRecords;
  
  // adres URL skryptu PHP wywolywanego przez AJAX w momencie zmiany kolejnosci
  var $sURL;
  
    
	/**
   * Konstruktor klsy SortListView
   *
	 * @param	string	$sName					- nazwa strony
	 * @param	array		$aRecords				- rekordy
	 * @param array 	$sURL	- URL pod ktory zostana przeslane posortowane dane za pomoca AJAX
	 *
	 * @return void
   */
  function SortListView($sName, $sPageName, &$aRecords, $sURL) {
  	global $aConfig;
  	
		$this->sName						= $sName;
		$this->sPageName				= $sPageName;
  	$this->sURL							= $sURL;
    $this->aRecords					= $aRecords;
    $this->aViewAttribs  		= array(
    	'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
    );
	} // end of SortListView() method
	
	
	/**
	 * Metoda wyswietla widok sortowania
	 *
	 * @return	string									- widok
	 */
	function Show() {
		global $aConfig;
		
		if (isset($_GET['file']) && !empty($_GET['file'])) {
			$sLang = $_GET['file'];
		}
		else {
			$sLang = $_GET['module'];
			if (isset($_GET['action'])) {
				$sLang .= '_'.$_GET['action'];
			}
		}
		
		$sHtml .= '<script type="text/javascript" language="JavaScript" src="js/lib/prototype.js"></script>
		<script type="text/javascript" language="JavaScript" src="js/scriptaculous/scriptaculous.js"></script>
';
		
		$sHtml .= '<table';
		foreach ($this->aViewAttribs as $sName => $sAttrib) {
			$sHtml .= ' '.$sName.'="'.$sAttrib.'"';
		}
		$sHtml .= '>';
		$sHtml .=  '
		<tr>
			<td class="tableMargin">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
					</tr>
					<tr>
						<td width="1" height="18" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						<td height="18" class="brownBg"><span class="tableHeaderBg"><strong>'.sprintf($aConfig['lang'][$sLang]['sort_items'], $this->sPageName).'</strong></span></td>
						<td width="1" height="18" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
					</tr>
					<tr>
						<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
					</tr>
				</table>';
		
		$sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="recordsTable">';
		$sHtml .= '<tr>
								<th style="background-color: #E5E3CD; padding-left: 10px;"><div id="ajaxMsg" style="display: block;"><span class="msg0">'.$aConfig['lang']['sort']['sort_info'].'</span></div></th>
							 </tr>
							 </table>';
		// rekordy
		$sHtml .= '<ul class="sortList" id="sortList">';

		foreach ($this->aRecords as $iId => $sRecord) {
			$sHtml .= '<li id="sortItem_'.$iId.'">';
			if (empty($sRecord) && isset($aConfig['lang'][$sLang]['sort_item_number'])) {
				$sHtml .= sprintf($aConfig['lang'][$sLang]['sort_item_number'], $iId);
			}
			elseif (isset($aConfig['lang'][$sLang]['sort_item'])) {
				$sHtml .= sprintf($aConfig['lang'][$sLang]['sort_item'], $iId, stripslashes($sRecord));
			}
			else {
				$sHtml .= stripslashes($sRecord);
			}
			$sHtml .= '</li>';
		}
		$sHtml .= '</ul>';
		
		$sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="footerTable">'.
								'<tr>'.
								'<td class="first">';
		if (isset($_GET['back'])) {
			$sHtml .= '<a href="'.base64_decode($_GET['back']).'"';
		}
		else {
			$sHtml .= '<a href="javascript:void(0);" onclick="history.back(); return false;"';
		}
		$sHtml .= ' title="'.$aConfig['lang'][$sLang]['go_back'].'">'.
							'<img src="gfx/icons/go_back_ico.gif" border="0" align="absmiddle"'.
							' alt="'.$aConfig['lang'][$sLang]['go_back'].'"> '.
							$aConfig['lang'][$sLang]['go_back'].
							'</a>&nbsp;&nbsp;';
		$sHtml .= '</td>';
		$sHtml .= '</tr></table>
';
		
		// kod odpowiedzialny za skorzystanie z biblioteki Script.aculo.us
		$sHtml .= "<script type=\"text/javascript\">
            function updateOrder() {
    					GetObject('ajaxMsg').innerHTML = '<span class=\"msg0\">".$aConfig['lang']['sort']['sorting_items']."</span>';
    					var options = {
								method : 'post',
                parameters : Sortable.serialize('sortList'),
                onComplete : function(r) {
    								if (r.responseText == '1') {
    									GetObject('ajaxMsg').innerHTML = '<span class=\"msg1\">".$aConfig['lang']['sort']['sort_ok']."</span>';
    								}
    								else {
    									GetObject('ajaxMsg').innerHTML = '<span class=\"msg2\">".$aConfig['lang']['sort']['sort_err']."</span>';
    								}
                },
                onFailure : function(r) {
    								GetObject('ajaxMsg').innerHTML = '<span class=\"msg2\">".$aConfig['lang']['sort']['sort_err']."</span>';
                }
              };
 							new Ajax.Request('".$this->sURL."', options);
						}
						Sortable.create('sortList', {scroll: window, onUpdate: updateOrder});
</script>";
		
		return $sHtml;
	} // end of Show() function
} // end of class SortListView
?>