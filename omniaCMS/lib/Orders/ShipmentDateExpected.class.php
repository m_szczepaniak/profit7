<?php
/**
 * Klasa wyliczania oczekiwanej daty wysłania zamówienia
 * 
 * @XXX uwaga dostępność pozycji, 1-2 dni, od reki itp, jest określane 
 *  na podstawie dostępności z momentu zakupu, bo na przykład na ten moment pozycja już może być niedostepna w żadnym źródle
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-10-17 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class ShipmentDateExpected {

  private $pCommon;
  private $_aConfig;

  function __construct(&$pDbMgr, $aConfig) {
    include_once('CommonAdapter.class.php');
    $oCommonAdapter = new CommonAdapter($pDbMgr);
    $this->pCommon = $oCommonAdapter;
    $this->_aConfig = $aConfig;
  }

  /**
   * Metoda pobiera spodziewaną datę wysyłki paczki
   * 
   * @param int $iOId
   * @return mixed
   */
  public function getOrderExpectedShipmentDate($iOId) {
    $iTotalItemShipmentDays = 0;
    $iTotalPreviewShipment = 0;

    $aOrder = $this->_getOrder($iOId);
    if ($aOrder['user_id'] > 0 && $aOrder['payment_id'] > 0) {

      $aShipmentTimes = $this->_getShipmentTimes();
      $aOrderItems = $this->_getOrderItems($iOId);

      // pobieramy składowe zamówienia - (nie usunięte) i sprawdzamy
      foreach ($aOrderItems as $aOrderItem) {
        $iItemShipmentDays = $this->_getOrderItemExpectedShipmentDate($aOrderItem, $aShipmentTimes);
        
        // BARDZO WAŻNE XXX -- iterowanie po każdym elemencie
        if ($iItemShipmentDays > $iTotalItemShipmentDays) {
          $iTotalItemShipmentDays = $iItemShipmentDays;
        }

        // XXX WAŻNE
        if (!empty($aOrderItem['shipment_date'])) {
          $iPreviewTime = strtotime($aOrderItem['shipment_date']);
          if ($iPreviewTime > $iTotalPreviewShipment) {
            $iTotalPreviewShipment = $iPreviewTime;
          }
        }
        
        // przesunięta data wydania zapowiedzi
        if (!empty($aOrderItem['current_shipment_date'])) {
          $iPreviewTime = strtotime($aOrderItem['current_shipment_date']);
          if ($iPreviewTime > $iTotalPreviewShipment) {
            $iTotalPreviewShipment = $iPreviewTime;
          }
        }
      }

      $aUserPriv = $this->_getSpecialUserPrivileges($aOrder['user_id']);
      $aPayment = $this->_getPaymentData($aOrder['payment_id'], ($aUserPriv['allow_14days'] == '1'), ($aUserPriv['allow_personal_reception'] == '1'));

      $aFreeDays = $this->getFreeDays();
      $bIsFreeDay = $this->_todayIsFreeDay($aFreeDays);

      if (intval(date('G')) < $aPayment['time_border'] || $bIsFreeDay == true) {
        $iShipmentDays = $aPayment['shipment_days_before'];
      } else {
        $iShipmentDays = $aPayment['shipment_days_after'];
      }
      $iTotalItemShipmentDays = $iTotalItemShipmentDays + $iShipmentDays;

      return $this->getDateAfterWorkingDays($iTotalItemShipmentDays, $aFreeDays, $iTotalPreviewShipment, $iShipmentDays + $aShipmentTimes['3']['shipment_days']);
    } else {
      return false;
    }
  }

// end of getOrderExpectedShipmentDate() method

  /**
   * Metoda pobiera szczegóły danego zamówienia
   * 
   * @param int $iOId
   * @return array
   */
  private function _getOrder($iOId) {

    $sSql = '
      SELECT user_id, payment_id
      FROM orders 
      WHERE 
        id = ' . $iOId;
    return $this->pCommon->GetRow($sSql);
  }

// end of _getOrder() method

  /**
   * Metoda pobiera składowe zamówienia
   * 
   * @param int $iOId
   * @return array
   */
  private function _getOrderItems($iOId) {

    $sSql = '
      SELECT shipment_date, current_shipment_date, shipment_time, preview
      FROM orders_items 
      WHERE 
        order_id = ' . $iOId . ' AND
        (
          (item_type = "I" AND packet = "0") 
            OR 
           item_type = "P"
         )
        AND deleted = "0"';
    return $this->pCommon->GetAll($sSql);
  }

// end of _getOrderItems() method

  /**
   * Metoda pobiera Zakładaną datę wysyłki danej pozycji w obrębie zamówienia
   * 
   * @XXX uwaga dostępność pozycji, 1-2 dni, od reki itp, jest określane 
   *  na podstawie dostępności z momentu zakupu, bo na przykład na ten moment pozycja już może być niedostepna w żadnym źródle
   * 
   * @param array $aOrderItem
   * @param array $aShipmentTimes
   * @return int
   */
  private function _getOrderItemExpectedShipmentDate($aOrderItem, $aShipmentTimes) {

    if ($aOrderItem['preview'] == '1') {
      $iShipmentDays = $aShipmentTimes['3']['shipment_days'];
    } else {
      $iShipmentDays = $aShipmentTimes[$aOrderItem['shipment_time']]['shipment_days'];
    }
    return $iShipmentDays;
  }

  /**
   * pobiera informacje o specjanych uprawnieniach użytkownika - przelew 14 dni,
   * odbór osobisty, zwolnienie z kosztów transportu 
   * 
   * @return array
   */
  private function _getSpecialUserPrivileges($iUId) {

    if ($iUId > 0) {
      $sSql = "SELECT allow_14days, allow_personal_reception
		  					FROM " . $this->_aConfig['tabls']['prefix'] . "users_accounts 
		  					WHERE id = " . $iUId;
      return $this->pCommon->GetRow($sSql);
    } else {
      return array();
    }
  }

// end of getSpecialUserPrivileges() method

  /**
   * Metoda pobiera dane płatności
   * 
   * @param int $iPaymentId
   * @param bool $bAllow14Days
   * @param bool $bAllowPersonalReception
   * @return array
   */
  private function _getPaymentData($iPaymentId, $bAllow14Days, $bAllowPersonalReception) {


    $sSql = "SELECT A.ptype, A.pdata, A.cost, A.no_costs_from, 
              A.shipment_days_before, A.shipment_days_after, A.transport_days_before,
              A.transport_days_after, A.message_before, A.message_after, A.time_border,
              B.name AS transport, B.id AS transport_id, B.symbol
             FROM " . $this->_aConfig['tabls']['prefix'] . "orders_payment_types A
             JOIN " . $this->_aConfig['tabls']['prefix'] . "orders_transport_means B
             ON A.transport_mean=B.id
             WHERE A.id = " . $iPaymentId .
            ($bAllow14Days === TRUE ? '' : " AND A.ptype <> 'bank_14days'") .
            ($bAllowPersonalReception === TRUE ? '' : " AND B.personal_reception = '0'");

    return $this->pCommon->GetRow($sSql);
  }

// end of _getPaymentData() method

  /**
   * Metoda pobiera informacje konfiguracyjne, na temat czasów dostarczenia przesyłki
   * 
   * @return array
   */
  private function _getShipmentTimes() {


    // pobranie z bazy danych edytowanego rekordu
    $sSql = "SELECT shipment_time,shipment_days,transport_days
						 FROM " . $this->_aConfig['tabls']['prefix'] . "orders_shipment_times";
    return $this->pCommon->GetAssoc($sSql);
  }

// end of _getShipmentTimes() method

  /**
   * Metoda pobiera dni wolne od pracy
   * 
   * @return array
   */
  public function getFreeDays() {


    $sSql = "SELECT UNIX_TIMESTAMP(free_day)
							 FROM " . $this->_aConfig['tabls']['prefix'] . "orders_free_days";
    return $this->pCommon->GetCol($sSql);
  }

// end of getFreeDays() method

  /**
   * Metoda pobiera wylicza datę wysyłki zamówienia, uwzględniając, wolne dni, oraz zapowiedź
   * 
   * @param int $iDays - dni do wysyłki pozycji
   * @param array $aFreeDays - tablica dni wolnych
   * @param int $iPreviewTime - data wydania zapowiedzi
   * @param int $iPreviewAdd - ile dni powinno być dodanych do daty wyd. zapowiedzi
   * @return date
   */
  public function getDateAfterWorkingDays($iDays, &$aFreeDays, $iPreviewTime = 0, $iPreviewAdd = 0) {


    $iDate = time();
    $iDaysCount = 0;
    $aDate = getdate($iDate);
    if ($aDate['wday'] == 0 || $aDate['wday'] == 6 || in_array(mktime(0, 0, 0, $aDate['mon'], $aDate['mday'], $aDate['year']), $aFreeDays)) {
      ++$iDays;
    }
    // dopóki nie osiągniemy potrzebnej ilości dni
    while ($iDaysCount < $iDays) {
      // dodaj dzień
      $iDate += (24 * 60 * 60);
      $aDate = getdate($iDate);
      // jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
      if ($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0, 0, 0, $aDate['mon'], $aDate['mday'], $aDate['year']), $aFreeDays)) {
        $iDaysCount++;
      }
    }
    // jesli zapowiedz jest pozniej niz wyliczona data
    $iPreviewTime = $this->_getDatePreview($iPreviewTime, $iPreviewAdd, $aFreeDays);
    if ($iPreviewTime > $iDate) {
      $iDate = $iPreviewTime;
      /*
      // dodaje dodakowe dni robocze do daty zapowiedzi
      if ($iPreviewAdd > 0) {
        $iDaysCount = 0;
        // dopóki nie osiągniemy potrzebnej ilości dni
        while ($iDaysCount < $iPreviewAdd) {
          // dodaj dzień
          $iDate += (24 * 60 * 60);
          $aDate = getdate($iDate);
          // jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
          if ($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0, 0, 0, $aDate['mon'], $aDate['mday'], $aDate['year']), $aFreeDays)) {
            $iDaysCount++;
          }
        }
      }
       */
    }
    return date($this->_aConfig['common']['mysql_date_format'], $iDate);
  } // end of getDateAfterWorkingDays() method
  
  
  /**
   * Metoda pobiera przesuniętą datę wydania zapowiedzi
   * 
   * @param int(timestamp) $iPreviewTime 
   * @param int $iPreviewAdd
   * @param array $aFreeDays
   * @return int - timestamp
   */
  private function _getDatePreview($iPreviewTime, $iPreviewAdd, $aFreeDays) {
    // edodja dodakowe dni robocze do daty zapowiedzi
    $iDate = $iPreviewTime;
    if($iPreviewAdd > 0){
      $iDaysCount=0;
      // dopóki nie osiągniemy potrzebnej ilości dni
      while($iDaysCount < $iPreviewAdd){
        // dodaj dzień
        $iDate += (24 * 60 * 60);
        $aDate = getdate($iDate);
        // jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
        if($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
          $iDaysCount++;
        }
      }
    }
    return $iDate;
  }// end of _getDatePreview() method
  

  /**
   * Metoda sprawdza czy dzisiejszy dzień jest dniem wolnym
   *
   * @param array ref $aFreeDays tablica z dniami wolnymi
   * @return bool
   */
  private function _todayIsFreeDay(&$aFreeDays) {

    $aDate = getdate(time()); // dziś
    // jeśli to nie sobota, niedziela lub wolny, zwracamy false

    if ($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0, 0, 0, $aDate['mon'], $aDate['mday'], $aDate['year']), $aFreeDays)) {
      return false;
    }
    return true;
  }

// end of todayIsFreeDay() method
}

?>