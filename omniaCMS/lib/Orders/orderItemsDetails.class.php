<?php

use magazine\HighLevelStock;
use orders\ShipmentsCollection;
/**
 * Klasa podglądu szczegółów składowych zamówienia, refaktoryzacja z m_zamowienia/Module.class.php->Details i ->GetOrderDetails
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-21 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class orderItemsDetails {

  /**
   * @var DatabaseManager $pDbMgr
   */
  public $pDbMgr;
  

  protected $aConfig;
  
  protected $aLang;
  
  protected $pSmarty;
  
  /**
   * Tablica stanów na magazynach
   * 
   * @var array $aStocks
   */
  protected $aStocks;
  
  /**
   * CSS odpowiedzialny za formatowanie kolorów
   *
   * @var string $sCss
   */
  protected $sCss;
  
  protected $sModule;

  protected $highLevelStock;

    /**
   *
   * @var orders\magazine\providers\ordersToProviders $oOrdersToProviders
   */
//  protected $oOrdersToProviders;
  
  public function __construct($pDbMgr, $aConfig, $sModule, $pSmarty) {
    $this->pDbMgr = $pDbMgr;
    $this->aConfig = $aConfig;
    $this->aLang = $aConfig['lang'][$sModule];
    $this->pSmarty = $pSmarty;
    $this->sCurrency = $this->sCurrency;
    $this->sModule = $sModule;
    $this->highLevelStock = new HighLevelStock($this->pDbMgr, \LIB\EntityManager\Entites\Magazine::TYPE_HIGH_STOCK_SUPPLIES);
//    
//    include_once($_SERVER['DOCUMENT_ROOT'].'/LIB/orders/magazine/providers/ordersToProviders.class.php');
//    $this->oOrdersToProviders = new orders\magazine\providers\ordersToProviders($pDbMgr);
  }// end of __construct() method
  
  
  /**
   * Metoda generuje widok szczegółów składowych zamówienia
   * 
   * @param int $iId
   * @param int $iPId
   * @param array $aOrder
   * @return FormTable
   */
  public function getDetailsForm($iId, $iPId, $aOrder) {
      
    // status zatwierdzone i pusty nr listu przewozowego i nie jest to odbiór osobisty(wiec niepotrzebny nr listu)
    $bPersonalReception = $this->_checkPersonalReception($iId);
    $bEditTransportNumber = $this->_setOrderTransportInfo($iId, $aOrder['n_order_status'], $aOrder['transport_number'], $bPersonalReception);
    
    $pOrderItemsDetailsForm = new FormTable('order_items', '', array('action' => phpSelf(array('id' => $iId), array('pid'))), array('col_width'=>155), $this->aConfig['common']['js_validation'], array(), array('class' => 'bigger'));
    $pOrderItemsDetailsForm->AddHidden('do', $bEditTransportNumber ? 'update_transport_number':'update_items',array('id'=>'order_items_form_do'));

    // zawartosc zamowienia
    $pOrderItemsDetailsForm->AddMergedRow($this->GetOrderDetails($this->pSmarty, $pOrderItemsDetailsForm, $iPId, $iId, $aOrder, $bEditTransportNumber));
    if (intval($aOrder['order_status']) < 3) {
      $pOrderItemsDetailsForm->AddMergedRow($pOrderItemsDetailsForm->GetInputButtonHTML('add_to_order', $this->aLang['add_to_order'],array(),'button').'&nbsp;&nbsp;',
                           //$pOrderItemsDetailsForm->GetInputButtonHTML('toggle_2nd_invoice', $this->aLang['toggle_invoice_2_'.(($aOrder['second_invoice'] == '1')?'off':'on')], array('onclick'=>'document.location.href=\''.phpSelf(array('do'=>'toggle_2nd_invoice', 'id' => $iId)).'\';'), 'button'),
                            array('class'=>'','style'=>'text-align: left;'),array('class'=>'add_to_order_button'));

    }
    if ($aOrder['is_modified'] == '1') {
      $pOrderItemsDetailsForm->AddRow('&nbsp;', sprintf($this->aLang['modified_info'],$aOrder['modified'],$aOrder['modified_by']).'&nbsp;&nbsp;'.
                                      $pOrderItemsDetailsForm->GetInputButtonHTML('send_modified', $this->aLang['send_modified'], array('onclick'=>'document.location.href=\''.phpSelf(array('do'=>'send_modified', 'id' => $iId)).'\';'), 'button'));
    }
    if (!empty($aOrder['remarks'])) {
      if (empty($aOrder['remarks_read_1']) && $aOrder['internal_status'] <= 1) {
        $pOrderItemsDetailsForm->AddHidden('remarks_read_1', '');
      }
    }
    return $pOrderItemsDetailsForm;
  }// end of getDetailsForm() method
  
  
	/**
	 * Sprawdza czy sposob transportu dla amówienia to odbiór osobisty
   * 
	 * @param int $iOrderId - id zamowienia
	 * @return bool
	 */
	protected function _checkPersonalReception($iOrderId){
    
		$sSql = "SELECT B.personal_reception
						FROM orders A
						LEFT JOIN orders_transport_means B
              ON B.id=A.transport_id
						WHERE A.id = ".$iOrderId;
		return ($this->pDbMgr->GetOne('profit24', $sSql) == '1');
	}// end of checkPersonalReception() method
  
  
  /**
   * Metoda ustawia informacje odnośnie listu przewozowego jeśli zam:
   * w statusie Potwierdzono, ustawiony został list przewozowy i nie jest to odbiór osobisty
   * 
   * @param int $iId
   * @param char $cNOrderStatus
   * @param string $sTransportNumber
   * @param bool $bPersonalReception
   * @return bool czy edytowalny jest nr listu przewozowego
   */
  protected function _setOrderTransportInfo($iId, $cNOrderStatus, $sTransportNumber, $bPersonalReception) {
    
			if($cNOrderStatus == '50' && empty($sTransportNumber) && !$bPersonalReception){
				return true;
			} else {
				return false;
			}
  }// end of _setOrderTransportInfo() method
  
  
  /**
   * Metoda Tworzy widok edytowalny
   * 
   * @param bool $bEditable
   * @return EditableView
   */
  private function _getEditableView($bEditable) {
    
		// dolaczenie klasy View
		include_once('View/EditableView.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'form'	=>	false,
			'editable' => $bEditable,
			'checkboxes'	=>	false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> $this->aLang['items_list_lp'],
				'width' => '2%'
			),
			array(
				'content'	=> $this->aLang['items_list_name']
			),
      array(
				'content'	=> $this->aLang['items_list_quantity'],
				'width' => '20',
        'style' => ''//font-size: 18px!important; text-align: center;
			),
			array(
				'content'	=> $this->aLang['items_list_status'],
				'width' => '105',
        'style' => 'width: 112px;',
			),
			array(
				'content'	=> $this->aLang['items_list_source2'],
				'width' => '62'
			),
			array(
				'content'	=> $this->aLang['items_list_weight'],
				'width' => '75'
			),
			array(
				'content'	=> $this->aLang['source'],
				'width' => '10%',
			),
			array(
				'content'	=> $this->aLang['items_list_vat'],
				'width' => '50'
			),
			array(
				'content'	=> $this->aLang['items_list_price_brutto'],
				'width' => '75'
			),
			array(
				'content'	=> $this->aLang['items_list_discount2'],
				'width' => '55'
			),
			array(
				'content'	=> $this->aLang['items_list_value_brutto2'],
				'width' => '90'
			),
			array(
					'content'	=> $this->aConfig['lang']['common']['action'],
					'width'	=> '10'
			)
		);

		$pView = new EditableView('order_details', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
    return $pView;
  }// end of _getEditableView() method
  

  /**
   * Metoda pobiera składowe zamówienia
   * 
   * @param int $iId
   * @return array
   */
  private function _getOrderItems($iId) {

    // pobranie zamowionych w ramach zamowienia towarow
    $sSql = "SELECT OI.*, P.azymut_index
             FROM orders_items AS OI
             LEFT JOIN products AS P
              ON P.id = OI.product_id
             WHERE OI.order_id = ".$iId."
              AND OI.item_type = 'I'
             ORDER BY OI.name";
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of _getOrderItems
  
  
  /**
   * Metoda pobiera zewnętrznych dostawców, w kolejoności od naszych, 
   *  a nastepnie alfabetycznie
   * 
   * @return array
   */
  private function _getExternalProviders() {
    
    $sSql = "SELECT id as idk, id, icon, name
							 FROM external_providers
							ORDER BY IF(name LIKE  '%profit%', 0, 1), name ASC";
		return $this->pDbMgr->GetAssoc('profit24', $sSql);
  }// end of _getExternalProviders
  
  
  /**
   * Metoda pobiera zewnętrznych dostawców, w kolejoności od naszych, 
   *  a nastepnie alfabetycznie
   * 
   * @return array
   */
  private function _getInternalProviders() {
    
    return $this->aConfig['lang']['m_zamowienia_ordered_items']['sources'];
  }// end of _getExternalProviders
  
  
  /**
   * Metoda określa, które kolumny powinny być oznaczone jako zablokowane
   * 
   * @param array $aItem
   * @param bool $bEditable
   * @param bool $bSecondInvoice
   * @param bool $bSecondPayment
   * @return array
   */
  private function _getDisabledArray($aTemp, $aItem, $bEditable, $bSecondInvoice, $bSecondPayment) {
    
    if($aItem['deleted']=='1'){
      $aTemp['disabled'][] = 'discount';
      $aTemp['disabled'][] = 'weight';
      $aTemp['disabled'][] = 'delete_item';
      $aTemp['disabled'][] = 'status';
      $aTemp['disabled'][] = 'quantity';
      $aTemp['disabled'][] = 'change_invoice_1';
      $aTemp['disabled'][] = 'change_invoice_2';
      $aTemp['disabled'][] = 'change_payment_1';
      $aTemp['disabled'][] = 'change_payment_2';
      $aTemp['disabled'][] = 'divide';
      if($aItem['packet'] == '1'){
        $aTemp['discount'] = '&nbsp;';
      }
      if(!$bEditable){
        $aTemp['disabled'][] = 'undelete_item';
      }
      $aTemp['status'] = '&nbsp;';
    } else {
      $aTemp['disabled'][] = 'undelete_item';
      if($aItem['packet'] == '1'){
        $aTemp['disabled'][] = 'discount';
        $aTemp['disabled'][] = 'weight';
        $aTemp['discount'] = '&nbsp;';
        $aTemp['disabled'][] = 'divide';
      }
      if(!$bEditable){
        $aTemp['disabled'][] = 'divide';
        $aTemp['disabled'][] = 'delete_item';
        $aTemp['disabled'][] = 'change_invoice_1';
        $aTemp['disabled'][] = 'change_invoice_2';
        $aTemp['disabled'][] = 'change_payment_1';
        $aTemp['disabled'][] = 'change_payment_2';
      } else {
        
        if ($aItem['quantity'] <= 1) {
          $aTemp['disabled'][] = 'divide';
        }
        
        if($bSecondInvoice){
          if($aItem['second_invoice'] == '1'){
            $aTemp['disabled'][] = 'change_invoice_2';
          } else {
            $aTemp['disabled'][] = 'change_invoice_1';
          }
        } else {
          $aTemp['disabled'][] = 'change_invoice_1';
          $aTemp['disabled'][] = 'change_invoice_2';
        }

        if($bSecondPayment){
          if($aItem['second_payment'] == '1'){
            $aTemp['disabled'][] = 'change_payment_2';
          } else {
            $aTemp['disabled'][] = 'change_payment_1';
          }
        } else {
          $aTemp['disabled'][] = 'change_payment_1';
          $aTemp['disabled'][] = 'change_payment_2';
        }
      }
    }

    return $aTemp;
  }// end of _getDisabledArray() method
  
  
	/**
	 * Metoda pobiera id ksiegarnii źródłowej
	 *
	 * @global integer $aConfig
	 * @global integer $pDbMgr
	 * @param integer $iId
	 * @return id
	 */
	function getProductIdSource($iId, $sWebsite) {
		global $aConfig, $pDbMgr;
    
		if ($iId > 0) {
      $sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products 
               WHERE profit24_id = ".$iId;
      return $pDbMgr->GetOne($sWebsite, $sSql);
    }
    return false;
	}// end of getProductIdSource() method


    function _getProductLink($iOrderWebsiteId, $sProductId, $sName) {
        $sPLink = "";
        if ($iOrderWebsiteId != '1') {
            // inny serwis niz profit
            // XXX TODO trzeba to później inaczej rozwiązać
            // zwłaszcza inaczej nazwać kolumnę profit24_id - tak zostało ponieważ
            // info o multiprofit pojawiło się zbyt późno
            $aSourceData = $this->_getWebsiteUrl($iOrderWebsiteId);
            $iSourcePId = $this->getProductIdSource($sProductId, $aSourceData['code']);
            $sPLink = $aSourceData['url'].createProductLink($iSourcePId, $sName);
        } else {
            $sPLink = createProductLink($sProductId, $sName);
        }
        return $sPLink;
    }
  
  /**
   * Metoda zwraca kod HTML dla kolumny nazwa produktu
   * 
   * @param type $aItem
   * @return string
   */
  private function _getProductItemNameDescription($iOrderWebsiteId, $aItem, $bSecondInvoice, $bSecondPayment) {
    $sPLink = $this->_getProductLink($iOrderWebsiteId, $aItem['product_id'], $aItem['name']);

    $canProductBeCollecting = true;
    if ($aItem['product_id'] > 0 && $aItem['source'] == '51' && $aItem['status'] == '3') {
        $canProductBeCollecting = $this->highLevelStock->checkProductCanBeCollecting($aItem['product_id']);
        if (empty($canProductBeCollecting)) {
          $canProductBeCollecting = true;
        }
    }

    $sConfirmCSS = '<script type="text/javascript">
    $(document).ready(function() {
      $("#editable_'.$aItem['id'].'_status_1").parent().parent().parent().css("background-color", "#AFEEEE");
      $("#editable_'.$aItem['id'].'_status_1").parent().parent().parent().prev().css("background-color", "#AFEEEE");
    });
    </script>';
      
    $sHTMLName = ($aItem['status'] == '4' ? $sConfirmCSS : '').
    '
      <span'.($aItem['deleted']=='1'?' style="color: gray; "':($aItem['added_by_employee']==1?' style="color:brown;"':'')).'><strong>
        <a href="'.$sPLink.'" target="_blank"'.($aItem['deleted']=='1'?' style="color: gray; "':($aItem['added_by_employee']==1?' style="color:brown; "':'')).'>'.$aItem['name'].'</a>
      </strong>'.
                    ($aItem['second_invoice']=='1' && $bSecondInvoice?'<img src="gfx/icons/change_invoice_2_ico.gif" alt="Pozycja na 2 fakturze" border="0">':'').
                    ($aItem['second_payment']=='1' && $bSecondPayment?'<img src="gfx/icons/change_payment_2_ico.gif" alt="Druga metoda płatności" border="0">':'').
                    ($aItem['sent_hasnc'] == '1' && $aItem['status'] == '3' ?'<img src="gfx/icons/lock_ico.png" title="Składowa zablokowana, proszę już nie zmieniać ilości" alt="Składowa zablokowana" border="0" style="width: 20px; float: right; position: relative">':'').
                    ($aItem['isbn']?'<br />'.$aItem['isbn']:'').
                    ($aItem['publication_year']?'<br />'.$aItem['publication_year']:'').($aItem['edition']?' wyd. '.$aItem['edition']:'').
                    ($aItem['publisher']?'<br />'.$aItem['publisher']:'').
                    ($aItem['authors']?'<br />'.$aItem['authors']:'').'</span>'.
                    ($canProductBeCollecting === true ? '' : '<br /><img src="gfx/lock_collecting_'.$canProductBeCollecting.'.png" style="width: 80px;" title="Nie można pobrać na listę ponieważ czeka na zebranie z wysokiego składowania" alt="Nie można pobrać na listę ponieważ czeka na zebranie z wysokiego składowania" />').
                  '<input type="hidden" name="oldVals['.$aItem['id'].'][quantity]" value="'.$aItem['quantity'].'" />'.
                  '<input type="hidden" name="oldVals['.$aItem['id'].'][status]" value="'.$aItem['status'].'" />'.
                  $this->_getOrderItemReservationAzymutHTML($aItem['quantity'], $this->_getOrderItemReservationAzymut($aItem['id'])).
                  $this->getMagazineListsHTML($aItem['id'], $aItem['get_ready_list']).
                  ' <input type="hidden" name="oldVals['.$aItem['id'].'][discount]" value="'.Common::formatPrice3($aItem['discount']).'" />';
    return $sHTMLName;
  }// end of _getProductItemNameDescription() method
  
  /**
   * 
   * @param int $iId
   * @param char $cListsReady
   * @return string
   */
  private function getMagazineListsHTML($iId, $cListsReady) {
    
    $aMagazineLists = $this->getLastItemList($iId);
    if (!empty($aMagazineLists)) {
      $sHTML = '<span style="color: green">
              <br /><br />  Listy na magazynie: <br />';
      foreach ($aMagazineLists as $iKey => $aShelf) {
          $stockLocations = $this->getStockLocation($aShelf['id']);
          if (!empty($stockLocations)) {
              $sHTML .= 'zbierane z lokalizacji: ';
              foreach ($stockLocations as $stockLocation) {
                  $sHTML .= $stockLocation['container_id'].' - '.$stockLocation['reserved_quantity'].($stockLocation['reservation_subtracted'] == '1' ? ' zrzucona rezerwacja ' : '');
              }
          }

          if ($aShelf['type'] == '5') {
              $sHTML .= '<span style="color: darkorange"><br /> - lista stock, wysokie: ' . $aShelf['orders_items_lists_id'].'</span>';
          } else {
              $sHTML .= '<br /> - lista: ' . $aShelf['orders_items_lists_id'];
          }

        $aTimesInfo = $this->getTimesInfo($iId, $aShelf['orders_items_lists_id']);
        if (!empty($aTimesInfo)) {
          $aTimesInfo['created'] = formatDateTimeClient($aTimesInfo['created']);
          $sHTML .= ' <span style="color: #001BFF!important;">'. $aTimesInfo['name_surname'] .' (' . $aTimesInfo['sc_quantity'] . ') ' . $aTimesInfo['created'].' </span>';
        }
        if ($aShelf['shelf_number'] != '') {
          $sHTML .= ', polka na sortowniku: ' . $aShelf['shelf_number'];
        }
        if ($aShelf['container_id'] != '' && $aShelf['container_id'] != '0000000000') {
          $sHTML .= ', kuweta do pakujących: '.$aShelf['container_id'];
        } else if ($aShelf['package_number'] != '') {
          $sHTML .= ', lokalizacja: '.$aShelf['package_number'];
        }
      }
      if ($cListsReady == '1') {
          $sHTML .= '<br /><br /> Produkt juz nie zostanie pobrany na liste do zebrania na magazynie! ';
      }
      $sHTML .= '</span>';
    }
    return $sHTML;
  }
  
  /**
   * 
   * @param type $iOId
   * @param type $iOILId
   */
  private function getTimesInfo($iOId, $iOILId) {
    
    // #001BFF osoba (ilość) czas
    $sSql = 'SELECT OILIT.*,  SUM(OILIT.sc_quantity), CONCAT(U.name, " ", U.surname) AS name_surname
             FROM orders_items_lists_items_times AS OILIT
             JOIN orders_items_lists_items AS OILI
              ON OILI.id = OILIT.id
             JOIN users AS U
              ON U.id = OILIT.user_id
             WHERE OILI.orders_items_lists_id = '.$iOILId.' 
               AND OILI.orders_items_id = '.$iOId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
  
  /**
   * Metoda pobiera numery list na ktorej pozycja sie pojawila
   * 
   * @global object $pDbMgr
   * @param type $iId
   * @return type
   */
	private function getLastItemList($iId) {
    global $pDbMgr;
    
    $sSql = "SELECT OILI.id, OILI.orders_items_lists_id, OILI.shelf_number, OILI.container_id, OIL.package_number, OIL.type
             FROM orders_items_lists_items  AS OILI
             JOIN orders_items_lists AS OIL
              ON OIL.id = OILI.orders_items_lists_id
             WHERE orders_items_id = ".$iId.'
             ORDER BY OILI.id DESC';
    return $pDbMgr->GetAll('profit24', $sSql);
  }

    /**
     * @param $iLId
     * @return array
     */
  private function getStockLocation($iLId) {

      $sSql = 'SELECT SLO.*, SL.container_id
               FROM stock_location_orders_items_lists_items AS SLO
               JOIN stock_location AS SL
                ON SLO.stock_location_id = SL.id AND SLO.stock_location_magazine_type = SL.magazine_type
               WHERE SLO.orders_items_lists_items_id = '.$iLId;
      return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
	/**
	 * Metoda pobiera adres url serwisu
	 *
	 * @param type $iWebsiteId
	 * @return type 
	 */
	private function _getWebsiteUrl($iWebsiteId) {
		
		$sSql = "SELECT url, code 
             FROM websites
						 WHERE id=".$iWebsiteId;
		return $this->pDbMgr->GetRow('profit24', $sSql);
	}// end of getWebsiteUrl() method
  
  
  /**
   * Metoda pobiera informację na temat rezerwacji produktu w źródle
   * 
   * @param type $iOrderItemId
   */
  private function _getOrderItemReservationAzymut($iOrderItemId) {
    
    $sSql = "SELECT * , EP.name AS source_name, OSH.status AS send_history_status, OSHI.status AS status
             FROM orders_send_history_items AS OSHI
             JOIN orders_send_history AS OSH
              ON OSHI.send_history_id = OSH.id
             LEFT JOIN external_providers AS EP
              ON EP.id = OSH.source
             LEFT JOIN orders_send_history_attributes AS OSHA
              ON OSHA.orders_send_history_id = OSH.id
             WHERE item_id = ".$iOrderItemId.'
             GROUP BY send_history_id, item_id
             ORDER BY last_update DESC';
    return Common::GetAll($sSql);
  }// end of getOrderItemReservationAzymut() method
  
  
  /**
   * Metoda zwraca kod HTML dla zamówionych pozycji
   * 
   * @param int $iQuantity
   * @param array $aOrdersItemsReservation
   * @return string
   */
  private function _getOrderItemReservationAzymutHTML($iQuantity, $aOrdersItemsReservation) {
    global $aConfig;

    $sHTML = '';
    $bIsInfo = false;
    if (!empty($aOrdersItemsReservation)) {
      foreach ($aOrdersItemsReservation as $aOrderItemReservation) {
        if ($aOrderItemReservation['pack_number'] != '') {
          $sHTML .= ' polka: '. $aOrderItemReservation['pack_number'].' - ';
        }
        $bErr = false;
        if (empty($aOrderItemReservation['source_name'])) {
          $aOrderItemReservation['source_name'] = $aConfig['lang']['m_zamowienia_ordered_items']['source_'.$aOrderItemReservation['source']];
        }
        $sFV = '';
          if ($aOrderItemReservation['fv_nr'] != '') {
              $sFV .= ' FV: '.$aOrderItemReservation['fv_nr'];
          }
        if ($aOrderItemReservation['status'] != '0' && $aOrderItemReservation['current_quantity'] != '' && $iQuantity > $aOrderItemReservation['current_quantity']) {
          $bErr = true;
          $bIsInfo = true;
          $sHTML .= formatDateTimeClient($aOrderItemReservation['date_send']).' '.$aOrderItemReservation['number'].$sFV.' '.$aOrderItemReservation['source_name'].' <b style="text-weight: 900;font-size:13px;">'.$aOrderItemReservation['current_quantity'].' &#8595;</b> ';
        } elseif ($aOrderItemReservation['current_quantity'] == '') {
          $bIsInfo = true;
          $sHTML .= formatDateTimeClient($aOrderItemReservation['date_send']).' '.$aOrderItemReservation['number'].$sFV.' '.$aOrderItemReservation['source_name'].' ';
        } elseif ($aOrderItemReservation['status'] == '6') {
          $bIsInfo = true;
          $sHTML .= formatDateTimeClient($aOrderItemReservation['date_send']).' '.$aOrderItemReservation['number'].$sFV.' '.$aOrderItemReservation['source_name'].' <b style="text-weight: 900;font-size:13px;">'.$aOrderItemReservation['current_quantity'].' Pot </b> ';
        } elseif ($aOrderItemReservation['status'] == '3' && $aOrderItemReservation['fv_nr'] != '') {
          $bIsInfo = true;
          $sHTML .= formatDateTimeClient($aOrderItemReservation['date_send']).' '.$aOrderItemReservation['number'].$sFV.' '.$aOrderItemReservation['source_name'].' <b style="text-weight: 900;font-size:13px;">'.$aOrderItemReservation['current_quantity'].' &#8595; Pot </b> ';
        }
        else {
          $bIsInfo = true;
          $sHTML .= formatDateTimeClient($aOrderItemReservation['date_send']).' '.$aOrderItemReservation['number'].$sFV.' '.$aOrderItemReservation['source_name'].' <b style="text-weight: 900;font-size:13px;">'.$aOrderItemReservation['current_quantity'].' Rez </b> ';
        }
        
        $sHTML .= ' ('.$aOrderItemReservation['in_confirmed_quantity'].') <br />';
        
        if ($bErr === true) {
          $sHTML = '<span style="color: red">'.$sHTML.'</span>';
        }
      }
      if ($bIsInfo == true) {
        $sHTML = '<br />
          <div style="font-size: 11px; color: green">
          Zamówione w :<br />
          '.
          $sHTML
          .' przez: '.
          $aOrderItemReservation['send_by'].
          '<div>';
      }
    }
    
    return $sHTML;
  }// end of _getOrderItemReservationAzymutHTML() method
  
  
  /**
   * Metoda zwraca statusy składowej zamówienia
   * 
   * @return array
   */
  private function _getOrderItemStatuses() {
    return $this->aConfig['lang']['m_zamowienia']['items_status'];
  }// end of _getOrderItemStatuses() method
  
  
  /**
   * Metoda dodaje ukryte pole dla źródła
   * 
   * @param int $iItemId
   * @param string $sValue
   * @param string $sType
   * @return string
   */
  private function _getOrderItemHidden($iItemId, $sValue, $sType) {
    
    $sHTML = '<input type="hidden" name="editable['.$iItemId.']['.$sType.']"" value="'.$sValue.'" />';
    return $sHTML;
  }// end of _getOrderItemHidden() method

  
  /**
   * Metoda pobiera kod HTML przycisku input
   * 
   * @param int $iItemId
   * @param int $iQuantity
   * @return string
   */
  private function getInputAddQuantity($iItemId, $iParentId, $iQuantity) {
    
    $sHTML = ' &nbsp; <input name="editable'.($iParentId > 0 ? '['.$iParentId.'][orders_to_providers]' : '' ).'['.$iItemId.'][quantity]" value="'.intval($iQuantity).'" style="width:30px; text-align: center;" />';
    return $sHTML;
  }// end of getInputAddQuantity() method
  
  
  /**
   * Metoda przycina słowa po określonej długości w zadanym stringu
   * 
   * @param string $sString
   * @param int $iLength
   * @return string
   */
  private function _trimWords($sString, $iLength) {
    $sRetSTR = '';
    
    $aWords = explode(' ', $sString);
    foreach ($aWords as $sWord) {
      $sRetSTR .= mb_substr($sWord,0,$iLength, 'UTF8').' ';
    }
    $sRetSTR = substr($sRetSTR, 0, -1);
    return $sRetSTR;
  }// end of _trimWords() method
  
  
  /**
   * Metoda zwraca przyciski Radio składowej zamówienia
   * 
   * @param array $aItem
   * @param int $iParentId
   * @param bool $bAddInputQuantity
   * @param string $sType
   * @return string - HTML
   */
  private function _getOrderItemStatusRadio($aItem, $iParentId = NULL, $bAddInputQuantity = FALSE, $sType = 'status') {
    $sStatusHtml = '';//'#'.$aItem['id'];//.' - '.$sType.''
    $aOrderStatuses = $this->_getOrderItemStatuses();
    $sStatusHtml .= '<div style="height: 140px;">';
    
    $bFirst = TRUE;
    foreach ($aOrderStatuses as $iOrderStatusId => $sOrderStatusName) {
      if ($bFirst === FALSE) {
        $sStatusHtml .= '<br />';
      } else {
        $bFirst = FALSE;
      }
      $sStatusHtml .= ' <input type="radio" '.
                        ' name="editable'.($iParentId > 0 ? '['.$iParentId.'][orders_to_providers]' : '' ).'['.$aItem['id'].']['.$sType.']" '.
                        ' id="editable_'.$aItem['id'].'_'.$sType.'_'.$iOrderStatusId.'" '.
                        ' value="'.$iOrderStatusId.'" '.
                        ' onclick="showSourceRadio(this.value, '.$aItem['id'].');" '.
                        ' style="border:none;" '.
                        ($aItem[$sType] == $iOrderStatusId ? ' checked="checked"':'').
                        ($iOrderStatusId == -1 || $iOrderStatusId == 1 || $iOrderStatusId == 3 ? '' : ' disabled="disabled" ').
                        ' />';
      $sStatusHtml .= '<input type="hidden" name="editable['.$aItem['id'].'][statusCur]" value="'.$aItem['status'].'" />';
      $sStatusHtml .= ' <label for="editable_'.$aItem['id'].'_status_'.$iOrderStatusId.'">'.$this->_trimWords($sOrderStatusName, 4).'</label>';
      if ($bAddInputQuantity === TRUE && $aItem[$sType] == $iOrderStatusId) {
        $sStatusHtml .= $this->getInputAddQuantity($aItem['id'], $iParentId, $aItem['quantity']);
      }
    }
    $sStatusHtml .= '</div>';
    return $sStatusHtml;
  }// end of _getOrderItemStatusRadio() method
  
  /**
   * 
   * @param array $aProvidersOrdered
   * @param int $iSource
   * @return boolean
   */
  private function checkSelectedExternalSource(array $aProvidersOrdered, $iSource) {
    
    if ($iSource > 0 &&
            (
             isset($aProvidersOrdered[$iSource]) || 
             isset($aProvidersOrdered['not_integrated_sources'][$iSource])
            )
        ) {
      return true;
    } else {
      return false;
    }
  }

 /**
  * Metoda wyświetla nazwę źródła
  *  jeśli wybrane są zapo lub --- nic się nie wyświetla,
  *  jeśli w "do Rezerwacji" wszystkie źródła mają być widoczne,
  *  jeśli jeśli w późniejszym niż "do Rezerwacji" to wyświetla się tylko wybrane źródło
  *
  * @param array $aProvidersOrdered
  * @param array $aItem
  * @param int $iParentId
  * @param bool $bAddInputQuantity
  * @param string $sType
  * @return string
  */
  private function _getOrderItemSource(array $aProvidersOrdered, array $aItem, $iParentId = NULL, $bAddInputQuantity = FALSE, $sType = 'source') {
      $bExtSource = $this->checkSelectedExternalSource($aProvidersOrdered, $aItem['source']);

      $sName = '';

      foreach ($aProvidersOrdered as $mKey => $aProvider) {
          if ($mKey != 'not_integrated_sources') {
              if ($this->isProviderSelected($aItem['source'], $aProvider['id']) === true) {
                  $sName = mb_substr($aProvider['name'], 0, 4);
              }
          }
      }

      foreach ($aProvidersOrdered['not_integrated_sources'] as $mKey => $aMoreProvider) {
          if ($this->isProviderSelected($aItem['source'], $aMoreProvider['id']) === true) {
              $sName = mb_substr($aMoreProvider['name'], 0, 4);
          }
      }

      if(($aItem['status']==3 || $aItem['status']==4) && $bExtSource === false) {

          switch ($aItem['source']) {
              case 1:
                  $sName = 'E';
                  break;
              case 0:
                  $sName = 'J';
                  break;
              case 51:
                  $sName = 'S';
                  break;
          }
      }
      return $sName;
  }

  /**
   * Metoda wyświetla przyciski radio źródeł
   *  jeśli wybrane są zapo lub --- nic się nie wyświetla,
   *  jeśli w "do Rezerwacji" wszystkie źródła mają być widoczne,
   *  jeśli jeśli w późniejszym niż "do Rezerwacji" to wyświetla się tylko wybrane źródło
   * 
   * @param array $aProvidersOrdered
   * @param array $aItem
   * @param int $iParentId
   * @param bool $bAddInputQuantity
   * @param string $sType
   * @return string
   */
  private function _getOrderItemSourceRadio(array $aProvidersOrdered, array $aItem, $iParentId = NULL, $bAddInputQuantity = FALSE, $sType = 'source') {

    $sExRadioHTML = '';
    $sRadioName = 'editable'.($iParentId > 0 ? '['.$iParentId.']['.$sType.']' : '' ).'['.$aItem['id'].']['.$sType.']';
      
    $bExtSource = $this->checkSelectedExternalSource($aProvidersOrdered, $aItem['source']);
    
    $iItemId = $aItem['id'];
    $sExternalProvidersHtml='
    <div'.($aItem['status'] != -1 && $aItem['status'] != -0 && ($aItem['status'] == 1 || $aItem['status'] == 2 || $bExtSource === true) ?'':' style="display:none;"').' id="sourceRadioEx'.$iItemId.'">';
    
    $bSelectedIntegratedProvider = false;
    foreach ($aProvidersOrdered as $mKey => $aProvider) {
      if ($mKey != 'not_integrated_sources') {
        $sExternalProvidersHtml .= $this->getSourceProviderRadioHTML($sRadioName, $aProvider, $aItem);
        if ($this->isProviderSelected($aItem['source'], $aProvider['id']) === true) {
          $bSelectedIntegratedProvider = true;
        }
      }
    }
    
    foreach ($aProvidersOrdered['not_integrated_sources'] as $mKey => $aMoreProvider) {
      $sExRadioHTML .= $this->getSourceProviderRadioHTML($sRadioName, $aMoreProvider, $aItem);
    }
    $bShowNotIntegrated = $this->checkShowNotIntegrated($bExtSource, $bSelectedIntegratedProvider, $aItem['source']);
    $sExternalProvidersHtml .= $this->getNotIntegratedContainer($sExRadioHTML, $bShowNotIntegrated);
    
    
    $sExternalProvidersHtml .= '
    </div>
    <div'.(($aItem['status']==3 || $aItem['status']==4) && $bExtSource === false ?'':' style="display:none;"').' id="sourceRadioIn'.$iItemId.'">
      <input'.($aItem['source']==1?' checked="checked"':'').' type="radio" name="'.$sRadioName.'" id="'.$sRadioName.'_1" value="1" style="border:none;" /><label for="'.$sRadioName.'_1">E</label><br />
      <input'.($aItem['source']==0 && ($aItem['status'] != -1 && $aItem['status'] != -0) ?' checked="checked"':'').' type="radio" name="'.$sRadioName.'" id="'.$sRadioName.'_0" value="0" style="border:none;" /><label for="'.$sRadioName.'_0">J</label><br />
      <input'.($aItem['source']==51?' checked="checked"':'').' type="radio" name="'.$sRadioName.'" id="'.$sRadioName.'_5" value="51" style="border:none;" /><label for="'.$sRadioName.'_51">S</label>
    </div>';
    
    /*
    if ($aItem['status'] == '3') {
      $bAllVisable = TRUE;
    } else {
      $bAllVisable = FALSE;
    }
    if (intval($aItem['status']) >= '1') {
      $bSelectedVisable = TRUE;
    } else {
      $bSelectedVisable = FALSE;
    }
    
    $sExternalProvidersHtml = '<div id="sourceRadioEx'.$aItem['id'].'" '.
            ($bAllVisable === FALSE && $bSelectedVisable === FALSE ? ' style="display:none; height: 140px;" ': ' style="height: 140px;" ').
            '>';
    $sExternalProvidersHtml .= '<select name="editable'.($iParentId > 0 ? '['.$iParentId.']['.$sType.']' : '' ).'['.$aItem['id'].']['.$sType.']">';
    $aProviders = addDefaultValue($aProviders);
    foreach ($aProviders as $aProvider) {
      // czy cokolwiek ma być widoczne
      if ($bAllVisable === TRUE || $bSelectedVisable === TRUE) {
        if ($aItem[$sType] == $aProvider['id']) {
          // wybrane źródło
          if ($bSelectedVisable === TRUE) {
            $sExternalProvidersHtml .= $this->_getOrderItemSourceRadioHTML($aItem['id'], $aProvider['id'], $aProvider['name'], TRUE, TRUE, FALSE);
          } else {
            $sExternalProvidersHtml .= $this->_getOrderItemSourceRadioHTML($aItem['id'], $aProvider['id'], $aProvider['name'], TRUE, TRUE, FALSE);
          }
        } else {
          // inne źródła
          if ($bAllVisable === TRUE) {
            $sExternalProvidersHtml .= $this->_getOrderItemSourceRadioHTML($aItem['id'], $aProvider['id'], $aProvider['name'], FALSE, TRUE, FALSE);
          } else {
            $sExternalProvidersHtml .= $this->_getOrderItemSourceRadioHTML($aItem['id'], $aProvider['id'], $aProvider['name'], FALSE, FALSE, FALSE);
          }
        }
      } else {
        $sExternalProvidersHtml .= $this->_getOrderItemSourceRadioHTML($aItem['id'], $aProvider['id'], $aProvider['name'], FALSE, FALSE, FALSE);
      }
    }
    $sExternalProvidersHtml .= '</select>';
    $sExternalProvidersHtml .= '</div><br />';
     */
    return $sExternalProvidersHtml;
  }// end of _getOrderItemSourceRadio() method
  
  /**
   * 
   * @param bool $bExtSource
   * @param bool $bSelectedIntegratedProvider
   * @param int $iSource
   * @return boolean
   */
  private function checkShowNotIntegrated($bExtSource, $bSelectedIntegratedProvider, $iSource) {
    
    if ($bExtSource === true && $bSelectedIntegratedProvider === false && $iSource > 0) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param string $sExRadioHTML
   * @param bool $bShowNotIntegrated
   * @return string
   */
  private function getNotIntegratedContainer($sExRadioHTML, $bShowNotIntegrated) {
    $sExRadioHTMLContainer = '';
    if ($bShowNotIntegrated === false) {
      $iExMoreId = rand(0, 99999999);
      $sDivId = 'more_'.$iExMoreId;
      $sExRadioHTMLContainer .= '<a href="javascript:void(0);" onclick="showNotIntegratedProviders(this, \''.$sDivId.'\');">...więcej</a>';
      $sExRadioHTMLContainer .= '<div style="display: none;" id="'.$sDivId.'">';
    }
    $sExRadioHTMLContainer .= $sExRadioHTML;
    if ($bShowNotIntegrated === false) {
      $sExRadioHTMLContainer .= '</div>'; 
    }
    return $sExRadioHTMLContainer;
  }
  
  /**
   * 
   * @param string $sRadioName
   * @param array $aProvider
   * @param array $aItem
   * @return string
   */
  private function getSourceProviderRadioHTML($sRadioName, array $aProvider, array $aItem) {
      if ($this->checkShowOnlySingleSelected($aItem['status'], $aProvider['id'], $aItem['source'])) {
        $bSelectedProvider = $this->isProviderSelected($aItem['source'], $aProvider['id']);
        $bDisabledProvider = $this->isProviderDisabled($aItem['azymut_index'], $aProvider['id']);
        $sRadioId = $sRadioName.'_'.$aProvider['id'];
        return $this->_getOrderItemSourceRadioHTML($sRadioId, $sRadioName, $aProvider['id'], $aProvider['name'], $bSelectedProvider, $bDisabledProvider);
      }
  }
  
  /**
   * 
   * @param int $iItemStatus
   * @param int $iProviderId
   * @param int $iItemSoruceId
   * @return bool
   */
  private function checkShowOnlySingleSelected($iItemStatus, $iProviderId, $iItemSoruceId) {
    return ($iItemSoruceId == $iProviderId || $iItemStatus != 4);
  }
  
  /**
   * 
   * @param int $iItemSourceId
   * @param int $iProviderId
   * @return bool
   */
  private function isProviderSelected($iItemSourceId, $iProviderId) {
    return ($iItemSourceId == $iProviderId);
  }
  
  /**
   * 
   * @param string $sAzymutIndex
   * @param int $iProviderId
   * @return bool
   */
  private function isProviderDisabled($sAzymutIndex, $iProviderId) {
    return FALSE;($iProviderId == '7' && $sAzymutIndex == '');
  }
  
  /**
   * Metoda pobiera kod HTML przycisku radio źródła w formularzu
   * 
   * @param string $sRadioId
   * @param string $sRadioName
   * @param int $iProviderId
   * @param string $sProviderName
   * @param bool $bSelected
   * @param bool $bDisabled
   * @return string
   */
  private function _getOrderItemSourceRadioHTML($sRadioId, $sRadioName, $iProviderId, $sProviderName, $bSelected, $bDisabled) {

    $sExternalProvidersHtml .= '<input'.
                  ($bSelected?' checked="checked"':'')
                 .($bDisabled ? ' DISABLED ' : '')
                 . ' type="radio"  '
                 . ' name="'.$sRadioName.'" '
                 . ' id="'.$sRadioId.'" 
                     value="'.$iProviderId.'" 
                     style="border:none;" ?>
                     <label for="'.$sRadioId.'">'.mb_substr($sProviderName, 0, 4).'</label><br />';
    return $sExternalProvidersHtml;
  }// end of _getOrderItemSourceRadioHTML() method
  
  
  /**
   * Metoda generuje kod HTML dla edycji pojedynczego rekordu w szczegółach 
   *  edycji zamówienia i wstawia do tablicy kolumn rekordu
   * 
   * @param int $i
   * @param int $iId
   * @param array $aOrder
   * @param array $aItem
   * @param array $aProvidersOrdered
   * @param array $aSources
   * @param bool $bEditable
   * @param bool $bSecondInvoice
   * @param bool $bSecondPayment
   */
  private function _proceedOrderItem(&$i, $iId, array $aOrder, array $aItem, array $aProvidersOrdered, array $aSources, $bEditable, $bSecondInvoice, $bSecondPayment) {
      $aTemp['foldable'] = array();

      $aTemp['source2'] = '';

      $this->sCss .= ' #cell_'.$aItem['id'].'_quantity{font-weight:bold; font-size:14px;}';
      $aTemp = array();
      $aTemp['id'] = $aItem['id'];
      $aTemp['lp'] = ($i++).'. ';
      $aTemp['name'] = $this->_getProductItemNameDescription($aOrder['website_id'], $aItem, $bSecondInvoice, $bSecondPayment);
      $sPrefix = '';
      // liczba
      $aTemp['quantity'] = $aItem['quantity'];
      $aTemp['status'] = '';

    if ($aItem['packet'] != '1') {
      /*
      if (intval($aItem['n_order_item_status']) >= 10) {
        // znajduje się w składowych do zamówienia u dostawcy orders_to_providers_to_order
        // wyświetlamy RADIO i SELECT dla danych już z tej tabeli
//        $aOrdersToProvidersToOrder = $this->oOrdersToProviders->getOrdersToProvidersByOrderItemId($aItem['id'], array('id', 'status AS n_order_item_status', 'quantity', 'provider_id'));
//        foreach ($aOrdersToProvidersToOrder as $aOrderToProvider) {
//          $aTemp['status'] .= $this->_getOrderItemStatusRadio($aOrderToProvider, $aItem['id'], TRUE, 'status');
//          $aTemp['source2'] .= $this->_getOrderItemSourceRadio($aProviders, $aOrderToProvider, $aItem['id'], TRUE, 'provider_id');
//        }
        
        // dane z podstawowej tabeli orders_items, przekazywane są w hidden
        $aTemp['status'] .= $this->_getOrderItemHidden($aItem['id'], $aItem['status'], 'status');
        $aTemp['source2'] .= $this->_getOrderItemHidden($aItem['id'], $aItem['source'], 'source');
        
      } else {
        // nie ma w składowych do zamówienia(orders_to_providers_to_order)
        // wyświetlamy tylko podstawowe RADIO i SELCT
        $aTemp['status'] = $this->_getOrderItemStatusRadio($aItem, NULL, FALSE, 'status');
        $aTemp['source2'] = $this->_getOrderItemSourceRadio($aProviders, $aItem, NULL, FALSE, 'source');
      }
    */
      $aTemp['status'] = $this->_getOrderItemStatusRadio($aItem, NULL, FALSE, 'status');
      $aTemp['source2'] = $this->_getOrderItemSourceRadio($aProvidersOrdered, $aItem, NULL, FALSE, 'source');

      $aTemp['weight'] = Common::formatPrice($aItem['weight']);
    } else {
      // pakiet
      $aTemp['source2'] = '&nbsp;';
      $aTemp['status'] = '&nbsp;';
      $aTemp['weight'] = '&nbsp;';
    }

    $aTemp['shipment'] = '';
    if($aItem['preview']=='1'){
      $aTemp['shipment'] = $this->getShipmentHTML($iId, $aItem, $aOrder);
    }

    $oStockView = new omniaCMS\lib\Products\ProductsStockView($this->pDbMgr, $this->pSmarty);
    if ($aItem['product_id'] > 0 && !empty($aSources)) {
      $aTemp['shipment'] .= $oStockView->getProductStockView($aItem['product_id'], $aSources);
    }
    
    // formatowanie cen
    if ($aItem['promo_price_brutto'] > 0) {
      $sPrefix = '<br />';
      // vat
      $aTemp['vat'] = Common::formatPrice3($aItem['vat']).'<span style="color: gray">%</span>';
      // cena brutto
      $aTemp['price_brutto'] = ' <span style="text-decoration: line-through;">'.Common::formatPrice($aItem['price_brutto']).' <span style="color: gray">'.$this->sCurrency.'</span></span><br>'.Common::formatPrice($aItem['promo_price_brutto']).' <span style="color: gray">'.$this->sCurrency.'</span>';
    }
    else {
      // vat
      $aTemp['vat'] = Common::formatPrice3($aItem['vat']).'<span style="color: gray">%</span>';
      // cena brutto
      $aTemp['price_brutto'] = Common::formatPrice($aItem['price_brutto']).' <span style="color: gray">'.$this->sCurrency.'</span>';
    }

    if ($aItem['attachments'] == '1') {
      // produkt posiada zalaczniki - pobranie ich
      $sSql = "SELECT name, price_brutto, promo_price_brutto, vat
               FROM orders_items
               WHERE order_id = ".$iId." AND
                     parent_id = ".$aItem['id']." AND
                     item_type = 'A'
               ORDER BY id";
      $aAttach =& Common::GetAll($sSql);

      $aTemp['name'] .= '<br><br><strong>'.$this->aLang['attachments'].'</strong><ul>';

      $aTemp['price_brutto'] .= '<br><br>';

      foreach ($aAttach as $aAtt) {
          $aTemp['name'] .= '<li>'.$aAtt['name'].'</li>';

          if ($aAtt['promo_price_brutto'] > 0.00) {
            $aTemp['price_brutto'] .= '<span style="text-decoration: line-through;" >'.$aAtt['price_brutto'].' <span style="color: gray;">'.$this->sCurrency.'</span></span><br/>';
            $aTemp['price_brutto'] .= $aAtt['promo_price_brutto'].' <span style="color: gray;">'.$this->sCurrency.'</span><br/>';
          } else {
            $aTemp['price_brutto'] .= '<span>'.$aAtt['price_brutto'].' <span style="color: gray;">'.$this->sCurrency.'</span></span><br/>';
          }

          $aTemp['vat'] .= '<br /><br /><br />'.Common::formatPrice3($aAtt['vat']).'<span style="color: gray">%</span>';

      }
      $aTemp['name'] .= '</ul>';
    }

    // rabat
    if ($aItem['discount'] > 0) {
      $aTemp['discount'] = Common::formatPrice3($aItem['discount']);
    }
    else {
      $aTemp['discount'] = '&nbsp;';
    }


    $aTemp = $this->_getDisabledArray($aTemp, $aItem, $bEditable, $bSecondInvoice, $bSecondPayment);

    // formatowanie wartosci
    if ($aItem['bundle_value_brutto'] > 0.00) {
      // bundle ustawione to znaczy że należy tu go użyć
      $aTemp['value_brutto'] = $sPrefix.Common::formatPrice($aItem['bundle_value_brutto']).' <span style="color: gray">'.$this->sCurrency.'</span>'; 
    } else {
      $aTemp['value_brutto'] = $sPrefix.Common::formatPrice($aItem['value_brutto']).' <span style="color: gray">'.$this->sCurrency.'</span>';
    }

    //reset tych pol dla pakietu sa zbedne
    if ($aItem['packet'] == '1')	{
      $aTemp['source2'] = $aTemp['status'] = $aTemp['shipment'] = '&nbsp;';
    }

    if ( $aItem['status'] == 4 || $aItem['status'] == 3 || $aItem['status'] == 2 || $aItem['deleted'] == 1 ) {
        $aTemp['foldable']['lp'] = $aTemp['lp'] . '<a class="closedRow" href="javascript:void(0);" onclick="toggleRow(this);" style="font-size: 20px; text-shadow: 2px 2px 4px #fff;" title="Rozwiń">+</a>';
        $aTemp['lp'] .= '<a class="openRow" href="javascript:void(0);" onclick="toggleRow(this, true);" style="font-size: 28px; text-shadow: 2px 2px 4px #fff;" title="Zwiń">-</a>';

        $aTemp['foldable']['short_title'] = '<a href="' . $this->_getProductLink($aOrder['website_id'], $aItem['product_id'], $aItem['name']) . '" target="_blank" ' . ($aItem['deleted']=='1' ? ' style="color: gray; "' : ($aItem['added_by_employee']==1 ? ' style="color:brown;"' : '')) . '>' . $aItem['name'] . '</a>' . '<br />' . $aItem['isbn'];
        $aTemp['foldable']['quantity'] = $aTemp['quantity'];

        $aOrderStatuses = $this->_getOrderItemStatuses();
        $aTemp['foldable']['short_status'] = $aOrderStatuses[$aItem['status']];

        $aTemp['foldable']['short_source'] = $this->_getOrderItemSource($aProvidersOrdered, $aItem, NULL, FALSE, 'source');

        $aTemp['foldable']['weight'] = $aTemp['weight'];
        $aTemp['foldable']['source'] = '&nbsp;';
        $aTemp['foldable']['vat'] = $aTemp['vat'];
        $aTemp['foldable']['price_brutto'] = $aTemp['price_brutto'];
        $aTemp['foldable']['discount'] = $aTemp['discount'];
        $aTemp['foldable']['value_brutto'] = $aTemp['value_brutto'];

        $aTemp['foldable']['action'] = '&nbsp;';
    }

    return $aTemp;
  }// end of _proceedOrderItem() method
  
  
  /**
   * Metoda zwraca HTML odpowiedzialny za wyświetlanie informacji o zapowiedziach na liście zapowiedzi
   *  oraz za formularz zmiany wydania zapowiedzi
   * 
   * @param type $iId
   * @param type $aItem
   * @return string HTML
   */
  private function getShipmentHTML($iId, $aItem, $aOrder) {
    $sHTML = '
      Zakupiono z datą premiery: <span style="font-weight: bold">'.formatDateClient($aItem['shipment_date']).'r.</span>
      <div id="'.$aItem['id'].'_current_shipment_date">'.($aItem['current_shipment_date'] != '' ? 'Aktualna data premiery: <span style="font-weight: bold">'.formatDateClient($aItem['current_shipment_date']).'r.</span>' : '').'</div><br />
        ';

    if ($aOrder['order_status'] != '4' && $aOrder['order_status'] != '5') {
      $sHTML .= '<input type="button" id="'.$aItem['id'].'_change_shipment_date" value="Zmień datę premiery" /><br />
        <div id="'.$aItem['id'].'_hiden_shipment_date" style="display: none">
          <input type="text" value="DD.MM.RRRR" /><br />
          <input type="button" value="Zapisz" id="'.$aItem['id'].'_save_change_shipment_date" />
          <input type="button" value="Anuluj" id="'.$aItem['id'].'_cancel_change_shipment_date" />
        </div>

        <script type="text/javascript">
          $("#'.$aItem['id'].'_change_shipment_date").click( function() {
            $("#'.$aItem['id'].'_hiden_shipment_date").show();
            $("#'.$aItem['id'].'_change_shipment_date").hide();
          });
          $("#'.$aItem['id'].'_cancel_change_shipment_date").click( function() {
            $("#'.$aItem['id'].'_hiden_shipment_date").hide();
            $("#'.$aItem['id'].'_change_shipment_date").show();
          });
          $("#'.$aItem['id'].'_hiden_shipment_date input[type=text]").focus( function() {
            if ($(this).val() == "DD.MM.RRRR") {
              $(this).val("");
            }
            $(this).bind("keypress", function(e) {
              if(e.keyCode==13){
                $("#'.$aItem['id'].'_save_change_shipment_date").click();
                return false;
              }
            });
          });

          $("#'.$aItem['id'].'_save_change_shipment_date").click( function() {
            var curr_shipment_date = $("#'.$aItem['id'].'_hiden_shipment_date input[type=text]").val();
            $.ajax({
              url: "admin.php?frame=main&module_id=38&module=m_zamowienia&do=save_shipment_date&ppid=0&id='.$iId.'&pppid='.$aItem['id'].'",
              data: "new_shipment_date="+curr_shipment_date,
              type: "POST",
              async: false,
              success: function (data, response) {
                if (data == "1") {
                  $("#'.$aItem['id'].'_current_shipment_date").html("Aktualna data premiery: <b>"+curr_shipment_date+"r.</b>");
                  $("#'.$aItem['id'].'_hiden_shipment_date").hide();
                  $("#'.$aItem['id'].'_change_shipment_date").show();
                } else {
                  alert(data);
                }
              }
            });
          });
        </script>

        <br />';
      }
    return $sHTML;
  }// end of getShipmentHTML() method
  
  /**
   * 
   * @return array
   */
  private function _getSources() {
    $sSql = 'SELECT provider_id as idk, id, S.* 
             FROM sources AS S
             ORDER BY order_by';
    return $this->pDbMgr->GetAssoc('profit24', $sSql);
  }
  
  
  /**
   * Metoda przetważa wszystkie składowe zamówienia
   *  i zwraca tablicę do wyświetlenia w formularzu szczegółów zamówienia
   * 
   * @param int $iId
   * @param array $aRecords
   * @param array $aOrder
   * @param bool $bEditable
   * @return array
   */
  private function _proceedOrderItems($iId, $aRecords, $aOrder, $bEditable) {
		$i = 1;
		$iCount = 0;
    
    $aProviders = $this->_getExternalProviders();
    $aSources = $this->_getSources();
    $aProvidersOrdered = $this->reorderProvidersBySources($aProviders, $aSources);
    
		$bSecondInvoice = ($aOrder['second_invoice'] == '1');
		$bSecondPayment = ($aOrder['second_payment_enabled'] == '1');
		$this->sCss = '<style>';
		foreach($aRecords as $aItem) {
      $aTemp = $this->_proceedOrderItem($i, $iId, $aOrder, $aItem, $aProvidersOrdered, $aSources, $bEditable, $bSecondInvoice, $bSecondPayment);
			$aRecords[$iCount] = $aTemp;
      
      if ($aItem['packet'] == '1') {
        // pobranie danych skladowych pakietu
        $sSql = "SELECT *
                 FROM orders_items
                 WHERE order_id = ".$iId." AND
                       parent_id = ".$aItem['id']." AND
                       item_type = 'P'
                 ORDER BY id";
        $aPacketItems =& Common::GetAll($sSql);	

        foreach ($aPacketItems as $aPItem) {
          
          $aTemp = $this->_proceedOrderItem($i, $iId, $aOrder, $aPItem, $aProvidersOrdered, $aSources, $bEditable, $bSecondInvoice, $bSecondPayment);
          $iCount ++;
          $aRecords[$iCount] = $aTemp;
        }
        $iCount ++;
			} 
      else {
				$iCount++;
			}
		}
    
    return $aRecords;
  }// end of _proceedOrderItems() method
  
  
  /**
   * 
   * @param array $aProviders
   * @param array $aSources
   * @return array
   */
  private function reorderProvidersBySources(array $aProviders, array $aSources) {
    $aNewProviders = array();
    foreach ($aSources as $iProviderId => $aSource) {
      if (isset($aProviders[$iProviderId])) {
        $aNewProviders[$iProviderId] = $aProviders[$iProviderId];
        unset($aProviders[$iProviderId]);
      }
    }
    $aNewProviders['not_integrated_sources'] = $aProviders;
    return $aNewProviders;
  }
  
	/**
	 * Metoda wyswietla edytowalną liste zamowionych w ramach zamowienia towarow
	 *
	 * @param		object	$pSmarty
	 * @param	object	$pForm	- formularz
	 * @param	integer	$iId	- Id zamowienia
	 * @param	array ref	$aOrder	- dane zamowienia
	 * @param bool $bEditTransportNumber - edytuj nr listu przewozowego
	 * @return	void
	 */
	public function GetOrderDetails(&$pSmarty, &$pForm,$iPId, $iId, &$aOrder, $bEditTransportNumber=false) {
		
		
		// czy w trybie edycji - dla nowych i w realizacji
		$bEditable = (intval($aOrder['order_status']) < 3);
    
    $aRecords = $this->_getOrderItems($iId);

    $aRecords = $this->_proceedOrderItems($iId, $aRecords, $aOrder, $bEditable);
		
		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array(
			'id'	=> array(
					'show'	=> false
					),
			'weight'=> array(
					'editable'	=> true
					),
			'quantity'=> array(
					'editable'	=> true
					),
			'discount'=> array(
					'editable'	=> true
					),
				'action' => array (
					'actions'	=> array ('undelete_item', 'delete_item', 'br', 'change_invoice_1', 'change_invoice_2', 'br', 'change_payment_1', 'change_payment_2', 'br', 'br'),//'divide'
					'params' => array (
												'undelete_item'	=> array('ppid'=>$iPId,'pid' => $iId, 'id'=>'{id}'),
												'delete_item'	=> array('ppid'=>$iPId,'pid' => $iId, 'id'=>'{id}'),
												'br' => array(),
												'change_invoice_1'	=> array('do' => 'change_invoice','ppid'=>$iPId,'pid' => $iId, 'id'=>'{id}', 'it'=>'1'),
												'change_invoice_2'	=> array('do' => 'change_invoice','ppid'=>$iPId,'pid' => $iId, 'id'=>'{id}', 'it'=>'0'),
												'br' => array(),
												'change_payment_1'	=> array('do' => 'change_payment','ppid'=>$iPId,'pid' => $iId, 'id'=>'{id}', 'it'=>'1'),
												'change_payment_2'	=> array('do' => 'change_payment','ppid'=>$iPId,'pid' => $iId, 'id'=>'{id}', 'it'=>'0'),
                        'br' => array(),
//                        'divide'	=> array('do' => 'divide', 'ppid' => $iPId, 'pid' => $iId, 'id'=>'{id}'),
					),
					'icon' => array(
						'delete_item' => 'delete'
					),
					'show' => false
					)
		);
		// ustawienia dla kolumn edytowalnych
		$aEditableSettings = array(
			'quantity' => array(
				'type' => 'text'
			),
			'weight' => array(
				'type' => 'text'
			),
			'discount' => array(
				'type' => 'text'
			)
		);
		
		// dodanie rekordow do widoku
    $pView = $this->_getEditableView($bEditable);
		$pView->AddRecords($aRecords, $aColSettings,$aEditableSettings);
    /*
		if(!empty($this->aStocks)){
      $this->aStocks = $this->formateStockTemplateData($this->aStocks);
      $pSmarty->assign_by_ref('aStocks', $this->aStocks);
      $sStockHtml = $pSmarty->fetch('stocks2.tpl');
      dump(htmlspecialchars($sStockHtml));
      $pSmarty->clear_assign('aStocks', $this->aStocks);
    }
     */
		$aRecordsHeader2 = array(
			array(
				'width' => '20'
			),
			array(
			),
			array(
				'width' => '20'
			),
			array(
				'width' => '90'
			),
			array(
					'width'	=> '30'
			)
		);
		
		$aHeader2 = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'form'	=>	false,
			'editable' => $bEditable,
			'checkboxes'	=>	false,
			'send_button' => ($bEditable || $bEditTransportNumber)?'items':false
		);
    
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
    
		$pView2 = new EditableView('order_details2', $aHeader2, $aAttribs);
		$pView2->AddRecordsHeader($aRecordsHeader2);
		$aTemp = array();
		
		// metoda transportu
		$sName = '<div style="text-align: right; font-weight: bold;">'.
										 $this->aLang['transport_mean'].
										 ':</strong>';
																																									
		$sContent = $bEditable?$pForm->GetSelectHTML('transport_mean', $this->aLang['transport_means'], array('style'=>'width:200px;'), $this->_getTransportMeans(), $aOrder['transport_id']):$aOrder['transport'];
		$aRecords2[] = $this->_addInfoRecord($sName, $sContent);
		
    unset($aTemp);
    
		// wybor punktu odbioru
		if (intval($aOrder['internal_status']) != 0 && intval($aOrder['order_status']) != 0 && intval($aOrder['internal_status']) <= 5) {
			// punkt odbioru
			$sName = '<div style="text-align: right; font-weight: bold; ">'.
										$this->aLang['point_of_receipt'].
										':</strong>';

			$aOrder['addresses'] = $this->getOrderAddresses($iId);
			include_once('paczkomaty/inpost.php');
			// mail lokalnie ustawiamy na testowy !!!
			if ($this->aConfig['common']['status'] == 'development' && $this->aConfig['common']['paczkomaty']['email'] != '') {
				$sEmail = $this->aConfig['common']['paczkomaty']['email'];
			} else {
				$sEmail = $aOrder['email'];
			}
      
			$aParam = array('email' => '',//$sEmail,
											'postcode' => '',//$aOrder['addresses']['1']['postal'],
											'name' => 'point_of_receipt',
											'paymentavailable' => '0',
											'selected' => ($aOrder['point_of_receipt'] == '' ? 'NULL' : $aOrder['point_of_receipt']), // sami mozemy zapisywać ostatnio wybraną opcję
											'class' => 'point_of_receipt',
			);
      $oShipment = new \orders\Shipment($aOrder['transport_id'], $this->pDbMgr);
      if ($oShipment->isReceptionAtPoint() === true) {
        $mPointsOfReceipt = $oShipment->getDestinationPointsDropdown('PointsOfReceipt');
        $bHiddeErr = false;
        if (is_array($mPointsOfReceipt)) {
            foreach ($mPointsOfReceipt as $key => $item) {
                $details = unserialize(base64_decode($item['details']));
                if ('false' == $details['cashondelivery']) {
                    $mPointsOfReceipt[$key]['data-cashondelivery'] = 'false';
                } else {
                    $mPointsOfReceipt[$key]['data-cashondelivery'] = 'true';
                }
            }

            if ('postal_fee' == $aOrder['payment_type'] && $aOrder['transport_id'] == '5' && $aOrder['point_of_receipt'] != '') {
                foreach ($mPointsOfReceipt as $key => $item) {
                    $details = unserialize(base64_decode($item['details']));
                    if ('false' == $details['cashondelivery']) {
                        unset($mPointsOfReceipt[$key]);
                    } else {
                        unset($mPointsOfReceipt[$key]['details']);
                    }
                }
                // sprawdźmy czy wybrany pkt może być odebrany
                $av = $oShipment->checkDestinationPointAvailablePayCashOnDelivery('PointsOfReceipt', $aOrder['point_of_receipt']);
                if ($av === false) {
                    $bHiddeErr = true;
                }
            }
            foreach ($mPointsOfReceipt as $key => $item) {
                unset($mPointsOfReceipt[$key]['details']);
            }

            if ($bHiddeErr === false) {
                $sContent = $pForm->GetSelectHTML('point_of_receipt', 'point_of_receipt', $aParam, addDefaultValue($mPointsOfReceipt), ($aOrder['point_of_receipt'] == '' ? 'NULL' : $aOrder['point_of_receipt']), _('Punkt odbioru'), '', false);
            } else {
                $sContent = 'WYBRANY PKT ODBIORU NIE OBSŁUGUJE płatności przy odbiorze'.$pForm->GetSelectHTML('point_of_receipt', 'point_of_receipt', $aParam, addDefaultValue($mPointsOfReceipt), 'NULL', _('Punkt odbioru'), '', false);
            }

        } else{
          $sContent = $mPointsOfReceipt;
        }
      } else {
        $sContent = '<select name="point_of_receipt" id="point_of_receipt" class="point_of_receipt"></select>';
      }
      $aRecords2[] = $this->_addInfoRecord($sName, $sContent);
      /*
			$aParam = array('email' => '',//$sEmail,
											'postcode' => '',//$aOrder['addresses']['1']['postal'],
											'name' => 'point_of_receipt',
											'paymentavailable' => '0',
											'selected' => ($aOrder['point_of_receipt'] == '' ? 'NULL' : $aOrder['point_of_receipt']), // sami mozemy zapisywać ostatnio wybraną opcję
											'class' => 'point_of_receipt',
			);
			$sContent = inpost_machines_dropdown($aParam);
			$aRecords2[] = $this->_addInfoRecord($sName, $sContent);
       */
		}
		elseif (intval($aOrder['internal_status']) > 5 && $aOrder['point_of_receipt'] != '') {
			$sName = '<div style="text-align: right; font-weight: bold; ">'.
										$this->aLang['point_of_receipt'].
										':</strong>';


			$sContent = $aOrder['point_of_receipt'];
			$aRecords2[] = $this->_addInfoRecord($sName, $sContent);
		}

		
		if($bEditTransportNumber) {
			$sName = '<div style="text-align: right; font-weight: bold;">'.
											 $this->aLang['transport_number'].
											 ':</strong>';
			
			$sContent = $pForm->GetTextHTML('transport_number', $this->aLang['transport_number'], '', array('style'=>'width:200px;'));
			$aRecords2[] = $this->_addInfoRecord($sName, $sContent);
		}
		
		// metoda płatności
		$sName = '<div style="text-align: right; font-weight: bold;">'.
										 $this->aLang['payment_method'].
										 ':</strong>
										 <input type="hidden" name="oldVals[transport][valuebrutto]" value="'.Common::formatPrice($aOrder['transport_cost']).'" />';
		
		$sContent = $bEditable?$pForm->GetSelectHTML('payment_method', $this->aLang['payment_method'], array('style'=>'width:200px;'), $this->_getPaymentMethods($aOrder['transport_id'], $aOrder['payment_id'], true), $aOrder['payment_id']):$aOrder['payment'];
		$aRecords2[] = $this->_addInfoRecord($sName, $sContent);
		//druga metoda platnosci
		if(Common::GetOne('SELECT second_payment_enabled FROM orders WHERE id='.$iId)=='1') {
			$sName = '<div style="text-align: right; font-weight: bold;">'.
											 $this->aLang['second_payment_method'].
											 ':</strong>
											 <input type="hidden" name="oldVals[transport2][valuebrutto]" value="'.Common::formatPrice($aOrder['transport_cost']).'" />';
			
			$sContent = $bEditable?$pForm->GetSelectHTML('second_payment_method', $this->aLang['second_payment_method'], array('style'=>'width:200px;'), $this->_getPaymentMethods($aOrder['transport_id'], $aOrder['payment_id'], true), $aOrder['second_payment_id'],'',false):$aOrder['second_payment'];
			$aRecords2[] = $this->_addInfoRecord($sName, $sContent);
		}
      
		// KOSZT TRANSPORTU
		$sId = 'transport';
		$sName = '<div style="text-align: right; font-weight: bold;">'.
										 $this->aLang['items_list_transport_price'].
										 ':</strong>';
		// formatowanie cen
		$sContent = Common::formatPrice($aOrder['transport_cost']);
    
		$aRecords2[] = $this->_addInfoRecord($sName, $sContent, $sId);
		// blokuj KOSZT TRANSPORTU
		if($bEditable){
			$sName = '<div style="text-align: right; font-weight: bold;">'.
											 $this->aLang['items_list_block_transport_cost'].
											 ':</strong>';
			$sContent = $pForm->GetCheckBoxHTML('block_transport_cost', $this->aLang['block_transport_cost'], array(), '', !empty($aOrder['block_transport_cost']), false);
			$aRecords2[] = $this->_addInfoRecord($sName, $sContent);
		}
		// Wartość zamówienia
		$sName = '<div style="text-align: right; font-weight: bold;">'.
										 $this->aLang['items_list_items_total_value'].
										 ':</strong>';
		$sContent = '<span style="font-weight: bold;">'.
														Common::formatPrice($aOrder['value_brutto']).' <span style="color: gray">'.$this->sCurrency.'</span></span>';
		
		$aRecords2[] = $this->_addInfoRecord($sName, $sContent);

		// PODSUMOWANIE BRUTTO
		$sName = '<div style="text-align: right; font-weight: bold;">'.
										 $this->aLang['items_list_items_total_value_transport'].
										 ':</strong>';
		$sContent = '<span style="font-weight: bold;">'.
														 Common::formatPrice($aOrder['value_brutto'] + $aOrder['transport_cost']).' <span style="color: gray">'.$this->sCurrency.'</span></span>';
		$aRecords2[] = $this->_addInfoRecord($sName, $sContent);
		
		// do zapłaty
		$sName = '<div style="text-align: right; font-weight: bold;">'.
										 $this->aLang['items_list_items_to_pay'].
										 ':</strong>';
		$sContent = '<span style="font-weight: bold;">'.
														 Common::formatPrice($aOrder['to_pay']).' <span style="color: gray">'.$this->sCurrency.'</span></span>';
		$aRecords2[] = $this->_addInfoRecord($sName, $sContent);
		
		if(!empty($aOrder['paid_amount'])){
			
			// PODSUMOWANIE BRUTTO
			$sName = '<div style="text-align: right; font-weight: bold;">'.
											 $this->aLang['items_list_items_paid_amount'].
											 ':</strong>';
			$sContent = '<span style="font-weight: bold;">'.
															 Common::formatPrice($aOrder['paid_amount']).' <span style="color: gray">'.$this->sCurrency.'</span></span>';
			$aRecords2[] = $this->_addInfoRecord($sName, $sContent);
			
			//pozostało do zapłaty
			if($aOrder['paid_amount'] < $aOrder['to_pay'] && $aOrder['paid_amount'] > 0) {
				// PODSUMOWANIE BRUTTO
        $sName = '<div style="text-align: right; font-weight: bold; color:red;">'.
												 ucfirst($this->aLang['underpayment']).
												 ':</strong>';
        $sContent = '<span style="font-weight: bold; color:red;">'.
																 Common::formatPrice($aOrder['to_pay']-$aOrder['paid_amount']).' '.$this->sCurrency.'</span>&nbsp;'
																 .(Common::GetOne('SELECT second_payment_enabled FROM orders WHERE id='.$iId)=='0'?$pForm->GetInputButtonHTML('enable_second_payment', $this->aLang['enable_second_payment'], array('onclick'=>'document.location.href=\''.phpSelf(array('do'=>'enable_second_payment', 'id'=>$iId)).'\';'), 'button'):'');
        $aRecords2[] = $this->_addInfoRecord($sName, $sContent);
			}
		}


    $this->sCss.='</style>';
    
    $pView2 = $this->_addViewRecords($pView2, $aRecords2);
    
    
		// walidacja js kolumn edytowalnych
		//oraz pokazywanie odpowiendiiego radio dla zrodla
    $oShipmentCollection = new ShipmentsCollection($this->pDbMgr);
    $aShipmentAtPoint = $oShipmentCollection->getShipmentReceptionAtPoint();
    $aShipmentToAddress = $oShipmentCollection->getDeliveryToAddress();
    $aParametrs = array();
    $aParametrs['aShipmentAtPoint'] = json_encode($aShipmentAtPoint);
    $aParametrs['aShipmentToAddress'] = json_encode($aShipmentToAddress);
    $aParametrs['selected'] = $aParam['selected'];
    $aParametrs['error_prefix'] = $this->aConfig['lang']['form']['error_prefix'];
    $aParametrs['error_postfix'] = $this->aConfig['lang']['form']['error_postfix'];
		$pSmarty->assign_by_ref('aParametrs', $aParametrs);
		$sJS = $pSmarty->fetch('m_zamowienia/orderJS.tpl');
		$pSmarty->clear_assign('aParametrs');
    

		return $this->sCss.$sJS.$pView->Show().$pView2->Show();
	} // end of GetOrderDetails() function
  
  
	/**
	 * Metoda pobiera symbol dla metody transportu
	 *
	 * @param string $sSymbol
	 * @return int 
	 */
	public function getTransportMeansIdBySymbol($sSymbol) {
		
		$sSql = "SELECT id FROM orders_transport_means
						 WHERE symbol='".$sSymbol."'";
		return $this->pDbMgr->GetOne('profit24', $sSql);
	}// end of getTransportMeansIdBySymbol() method
  
	
	/**
	 * Metoda pobiera metody płatności dla comboboxa
   * 
	 * @param int $iTransportMean - id metody transportu
   * @param bool $bPublished
	 * @return array
	 */
	private function _getPaymentMethods($iTransportMean, $iPaymentId, $bPublished=false){

		$aRecords=array();
		if(!empty($iTransportMean)){
            $sPublishedSQL = ' AND published = "1" ';
            if ($iPaymentId > 0) {
                $sPublishedSQL = ' AND (published = "1" OR id = '.$iPaymentId.')';
            }
			$sSql = "SELECT id AS value, ptype AS label
					 		 FROM orders_payment_types
					 		 WHERE transport_mean = ".$iTransportMean."
								 ".($bPublished == true ? $sPublishedSQL : "")."
						 	 ORDER BY id";
			$aRecords = $this->pDbMgr->GetAll('profit24', $sSql);
			foreach($aRecords as $iKey=>$aItem){
				$aRecords[$iKey]['label'] = $this->aConfig['lang'][$this->sModule]['ptype_'.$aItem['label']];
			}
		}
		$aItems = array(
			array('value' => '', 'label' => $this->aConfig['lang']['common']['choose'])
		);
		return array_merge($aItems, $aRecords);
	} // end of _getPaymentMethods() method
  
  
	/**
	 * Pobiera dane adresowe z zamówienai z bazy
	 * 
	 * @param int $iId - id adresu
   * @param bool $bIgnoreTransportAdress
	 * @return array ref
	 */
	public function getOrderAddresses($iId, $bIgnoreTransportAdress = false){

		$sSql = "SELECT address_type, is_company, company, name, surname, street, number, number2, postal, city, nip, phone, is_postal_mismatch, streamsoft_id
						 FROM orders_users_addresses 
						 WHERE order_id = ".$iId.
            ($bIgnoreTransportAdress === TRUE ? ' AND address_type="0"' : '').
						 " ORDER BY address_type"; 
		return $this->pDbMgr->GetAssoc('profit24', $sSql);
	} // end of _getOrderAddresses() method
  
  
	/**
	 * Metoda pobiera listę metod transportu dla coboboxa
   * 
	 * @return array
	 */
	function _getTransportMeans($bTrim = false){

		$sSql = "SELECT id AS value, name AS label
				 		 FROM orders_transport_means
					 	 ORDER BY name";
		$aRecords = $this->pDbMgr->GetAll('profit24', $sSql);
		if ($bTrim == true) {
			foreach ($aRecords as $iKey => $aItem) {
				$aRecords[$iKey]['label'] = trimString($aItem['label'], 16);
			}
		}
		$aItems = array(
			array('value' => '', 'label' => $this->aConfig['lang']['common']['choose'])
		);
		return array_merge($aItems,$aRecords);
	} // end of getTransportMeans() method
  
  
  /**
   * Metoda dodaje uniwersalny rekord do tabelki z widokiem
   * 
   * @param string $sName
   * @param string $sContent
   * @param string $sId
   * @return array
   */
  private function _addInfoRecord($sName, $sContent, $sId = '') {
    $aRecord = array(
        'id' => $sId,
        'lp' => '&nbsp;',
        'name' => $sName,
        'spacer' => '&nbsp;',
        'valuebrutto' => $sContent
    );
    return $aRecord;
  }// end of _addInfoRecord() method
  
  
  /**
   * Metoda dodaje do Widoku rekordy
   * 
   * @param View $pView2
   * @param array $aRecords2
   * @return View
   */
  private function _addViewRecords($pView2, $aRecords2) {
    
		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings2 = array(
			'id'	=> array(
					'show'	=> false
				),
			'valuebrutto'=> array(
					'editable'	=> true
				)
		);
		$aEditableSettings2 = array(
			'valuebrutto' => array(
				'type' => 'text'
			)
		);
		// dodanie rekordow do widoku
		$pView2->AddRecords($aRecords2, $aColSettings2,$aEditableSettings2);
    
    return $pView2;
  }// end of _addViewRecords() method
}// end of Class
