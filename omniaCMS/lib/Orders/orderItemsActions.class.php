<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-13 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace omniaCMS\lib\Orders;

use DatabaseManager;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use LIB\EntityManager\Entites\Magazine;
use magazine\HighLevelStock;
use Module;
use orders\logger;
/**
 * Description of orderItemsActions
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class orderItemsActions extends OrderActions {

  /**
   *
   * @var int
   */
  protected $iOrderItemId;
  
  /**
   *
   * @var int
   */
  protected $iOrderId;
  
  
  /**
   *
   * @var Module
   */
  protected $oModule;
  
  
  /**
   *
   * @var DatabaseManager
   */
  protected $pDbMgr;
  
  /**
   *
   * @var integer
   */
  private $iIt;
  
  /**
   *
   * @var array
   */
  protected $aOrderItemData;

  public function __construct($pDbMgr, $oModule, $iOrderId, $iOrderItemId = 0) {
    parent::__construct($pDbMgr, $oModule, $iOrderId);
    
    $this->iOrderId = $iOrderId;
    $this->pDbMgr = $pDbMgr;
    $this->iOrderItemId = $iOrderItemId;
    $this->aOrderItemData = $this->getSelOrderItemData(array('*'));
  }
  
  /**
   * 
   * @param mixed $name
   * @throws Exception
   */
  public function __get($name) {
    throw new Exception('Pobierano nieznana właściwość : '.$name);
  }
  
  /**
   * 
   * @param mixed $name
   * @param mixed $value
   * @throws Exception
   */
  public function __set($name, $value) {
    throw new Exception('Zmieniano Nieznana właściwość : '.$name.' na wartość: '.nl2br(print_r($value, true)));
  }
  
  /**
   * Zmianna pomocnicza
   * 
   * @param int $iIt
   */
  protected function setIt($iIt) {
    $this->iIt = $iIt;
  }
  
	/**
	 * Przesuwa pomiedzy metodami płatności
	 * 
	 * @return bool
	 */
	public function changePayment(){
		$this->pDbMgr->BeginTransaction('profit24');
		
    $aValues = array();
		if($this->iIt == '1'){
			$aValues['second_payment'] = '0';
			$aValues['payment_id'] = $this->aOrderData['payment_id'];
			$aValues['payment_type'] = $this->aOrderData['payment_type'];
		} else {
			$aValues['second_payment'] = '1';
			$aValues['payment_id'] = $this->aOrderData['second_payment_id'];
			$aValues['payment_type'] = $this->aOrderData['second_payment_type'];
		}
    
		$bIsErr = false;
		if($this->Update("orders_items", $aValues, "order_id = ".$this->iOrderId." AND id = ".$this->iOrderItemId) === false) {
			$bIsErr = true;
		}
		if ($this->AffectIfHaveChildUpdate($aValues) === false) {
      $bIsErr = true;
    }
    
		if ($bIsErr) {
			$this->pDbMgr->RollbackTransaction('profit24');
			$sMsg = sprintf($this->aLang['change_payment_err'], $this->aOrderData['name']);
			$this->setMessage($sMsg, true);
		}
		else {
			$this->pDbMgr->CommitTransaction('profit24');
			$sMsg = sprintf($this->aLang['change_payment_ok'], $this->aOrderData['name']);
			$this->setMessage($sMsg, false);
		}
	} // end of changePayment() method
  
  
  /**
   * Metoda stosuje zmiany do dzieci elementu zamówienia, czyli np pakiet lub załącznik
   * 
   * @param array $aValues
   * @return boolean
   */
  private function AffectIfHaveChildUpdate($aValues) {

		// jeśli to pakiet przenieść także jego składowe
		if ($this->aOrderItemData['packet'] == '1' || $this->aOrderItemData['attachments'] == '1') {
			// pobierz składowe pakietu
			$aItemsPacketIds = $this->getChildsIDsOfItem();
			if (!empty($aItemsPacketIds)) {
				// przenies skladowe i dzieci składowych - czyli pakiet z książkami i z załącznikami
				if($this->pDbMgr->Update('profit24', "orders_items", $aValues, 
                                  "order_id = ".$this->iOrderId." AND (id IN (".implode(',', $aItemsPacketIds).") OR parent_id IN (".implode(',', $aItemsPacketIds)."))") === false){
					return false;
				}
			}
		}
    return true;
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @return bool
   */
  private function checkIsLastInInvoice($iOrderId, $iOrderItemId) {
    $aOrderItemData = $this->getSelOrderItemData(array('second_invoice'));
    
    $sSql = 'SELECT id 
             FROM orders_items 
             WHERE order_id = '.$iOrderId.' 
               AND id <> '.$iOrderItemId.'
               AND deleted = "0"
               AND item_type = "I"
               AND second_invoice = "'.$aOrderItemData['second_invoice'].'" ';
    return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? FALSE : TRUE);
  }
  
	/**
	 * Usuwa pozycję z zamówienia
   * 
   * @return bool
	 */
	public function deleteItem(){
		$bIsErr = false;
    
    if ($this->checkIsLastInInvoice($this->iOrderId, $this->iOrderItemId) === true) {
      $this->setMessage(_('Nie można usunąć ostatniej pozycji z FV, jeśli jest to druga FV - to musisz najpierw usunąć druga FV'), true);
			return false;
    }
    
		$iCountNotDeleted = $this->countNotDeletedItems($this->iOrderId);
		if($iCountNotDeleted < 2){
      $this->setMessage('delete_item_err_last', true);
			return false;
		}
    
		if ($this->ValidateTransportQuantityWeightDiscount($_POST['editable']) === true) {

      $this->pDbMgr->BeginTransaction('profit24');
      
      if(!empty($this->aOrderData)){

        $highLevelStock = new HighLevelStock($this->pDbMgr, Magazine::TYPE_HIGH_STOCK_SUPPLIES);
        $highLevelStock->cancelOrderItemReservations($this->iOrderItemId);

        $aValues = array(
          'deleted' => '1'
        );

          // przeliczanie rezerwacji
          global $pDbMgr;
          $reservationManager = new ReservationManager($pDbMgr);
          $beforeReservation = $reservationManager->getDataProvider()->getReservationsByOrderId($this->iOrderId);

        if($this->Update("orders_items", $aValues, "order_id = ".$this->iOrderId." AND (id = ".$this->iOrderItemId." OR parent_id = ".$this->iOrderItemId.")") === false){
          $bIsErr = true;
        }
        if(!$bIsErr){
            try{
                $reservationResult = $reservationManager->createStreamsoftReservation($beforeReservation, $this->iOrderId);

                // zamowienie czesciowe, wyswielamy info
                if(false === $reservationResult){
                    $this->sMsg .= GetMessage($reservationManager->getErrorMessage());
                }
            } catch(\Exception $e) {
                $bIsErr = true;
                $this->sMsg .= GetMessage($e->getMessage());
            }

        }
          if(!$bIsErr) {
              if ($this->recountOrder($this->iOrderId, $this->aOrderData['payment_id']) === false) {
                  $bIsErr = true;
              }
          }
      } else {
        $bIsErr = true;
      }
    } else {
      $bIsErr = true;
    }
    
    
		if ($bIsErr) {
			$this->pDbMgr->RollbackTransaction('profit24');
            $reservationManager->rollbackChanges($this->iOrderId);
			$sMsg = sprintf($this->aLang['delete_item_err'], $this->aOrderItemData['name']);
      $this->setMessage($sMsg, true);
      return false;
		}
		else {
			$this->pDbMgr->CommitTransaction('profit24');
			$sMsg = sprintf($this->aLang['delete_item_ok'], $this->aOrderItemData['name']);
      $this->setMessage($sMsg, false);
      return true;
		}
	} // end of deleteItem() method
  
  
  /**
   * 
   * @param string $sTable
   * @param array $aValues
   * @param string $sWhere
   * @return mixed
   * @throws Exception
   */
  protected function Update($sTable, $aValues, $sWhere) {
    
    if ($sTable == 'orders_items' ) {
      $oLogger = new logger($this->pDbMgr);
      $aValues = $this->pDbMgr->getChangedUpdateValues($aValues, $this->aOrderItemData);
      $this->affectReservations($aValues, $this->aOrderItemData);
      if (!empty($aValues)) {
        $oLogger->addLog($this->iOrderId, $_SESSION['user']['id'], $aValues, $this->iOrderItemId);
        return $this->pDbMgr->Update('profit24', $sTable, $aValues, $sWhere);
      } else {
        return true;
      }
    } else {
      throw new Exception(_('Nie obsługiwana tabela'));
    }
  }
  
  private function affectReservations($aValues, $aOrderItemData) {
    
    $oReservationNN = new \omniaCMS\lib\Products\ProductsStockReservations($this->pDbMgr);
    $oReservation = new \omniaCMS\lib\Orders\orderItemReservation($oReservationNN);
    $oReservation->updateValues($aValues, $aOrderItemData);
    
  }
  
  
	/**
	 * Przywaraca usuniety element z zamówienia
   * 
   * @return bool
	 */
	public function undeleteItem(){
		$bIsErr = false;
    
    // walidacja
		if ($this->ValidateTransportQuantityWeightDiscount($_POST['editable']) === true) {

      $this->pDbMgr->BeginTransaction('profit24');
      if(!empty($this->aOrderData) && !empty($this->aOrderItemData)){
        $aValues = array(
          'deleted' => '0'
        );

          // przeliczanie rezerwacji
          global $pDbMgr;
          $reservationManager = new ReservationManager($pDbMgr);
          $beforeReservation = $reservationManager->getDataProvider()->getReservationsByOrderId($this->iOrderId);

        if($this->Update("orders_items", $aValues, " order_id = ".$this->iOrderId." AND (id = ".$this->iOrderItemId." OR parent_id = ".$this->iOrderItemId.")") === false){
          $bIsErr = true;
        }
        if(!$bIsErr){
            try{
                $reservationResult = $reservationManager->createStreamsoftReservation($beforeReservation, $this->iOrderId);

                // zamowienie czesciowe, wyswielamy info
                if(false === $reservationResult){
                    $this->sMsg .= GetMessage($reservationManager->getErrorMessage());
                }
            } catch(\Exception $e) {
                $bIsErr = true;
                $this->sMsg .= GetMessage($e->getMessage());
            }
        }
        if(!$bIsErr){
            if($this->recountOrder($this->iOrderId, $this->aOrderData['payment_id']) === false){
                $bIsErr = true;
            }
        }
      } else {
        $bIsErr = true;
      }
    } else {
      $bIsErr = true;
    }
    
		if ($bIsErr) {
			$this->pDbMgr->RollbackTransaction('profit24');
            $reservationManager->rollbackChanges($this->iOrderId);
			$sMsg = sprintf($this->aLang['undelete_item_err'], $this->aOrderItemData['name']);
      $this->setMessage($sMsg, true);
		}
		else {
			$this->pDbMgr->CommitTransaction('profit24');
			$sMsg = sprintf($this->aLang['undelete_item_ok'], $this->aOrderItemData['name']);
      $this->setMessage($sMsg, false);
		}
	} // end of undeleteItem() method
  
  
	/**
	 * Przesuwa pomiedzy fakturami
   * 
	 * @return bool
	 */
	public function changeInvoice(){
    
    if ($this->checkIsLastInInvoice($this->iOrderId, $this->iOrderItemId) === true) {
      $this->setMessage(_('Nie można przenieść ostatniej pozycji z FV, jeśli jest to druga FV - to musisz najpierw usunąć druga FV'), true);
			return false;
    }
    
		if($this->iIt == '1'){
			$aValues['second_invoice'] = '0';
		} else {
			$aValues['second_invoice'] = '1';
		}
    $this->pDbMgr->BeginTransaction('profit24');
		$bIsErr = false;
		if($this->Update("orders_items", $aValues, " order_id = ".$this->iOrderId." AND id = ".$this->iOrderItemId) === false){
			$bIsErr = true;
		}
		
		$this->AffectIfHaveChildUpdate($aValues);
    
		if ($bIsErr) {
			$this->pDbMgr->RollbackTransaction('profit24');
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = sprintf($this->aLang['change_invoice_err'], $this->aOrderItemData['name']);
      $this->setMessage($sMsg, true);
		}
		else {
			$this->pDbMgr->CommitTransaction('profit24');
      $this->removeProforma();
      
			$sMsg = sprintf($this->aLang['change_invoice_ok'], $this->aOrderItemData['name']);
      $this->setMessage($sMsg, false);
		}
	} // end of deleteItem() method
  
  
  /**
   * 
   * @return array
   */
  private function getChildsIDsOfItem() {
    
    $sSql = "SELECT id
        FROM orders_items
        WHERE order_id = ".$this->iOrderId."
        AND parent_id = ".$this->iOrderItemId;
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }
  
  
  /**
   * 
   * @param array $aCols
   * @return array
   */
  protected function getSelOrderItemData($aCols) {
    
    $sSql = 'SELECT '.implode(', ', $aCols).'
             FROM orders_items
             WHERE id = '.$this->iOrderItemId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
  

  /**
   * 
   * @return type
   */
  private function countNotDeletedItems(){
		
		$sSql = "SELECT COUNT(1) 
						FROM orders_items
						WHERE order_id = ".$this->iOrderId."
						AND item_type = 'I'
						AND deleted = '0'"; 
		return intval($this->pDbMgr->GetOne('profit24', $sSql));
	}// end of countNotDeletedItems() method
}
