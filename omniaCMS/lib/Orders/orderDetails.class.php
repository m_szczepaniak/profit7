<?php

use LIB\EntityManager\Entites\OrdersPaymentReturns;
use orders\Shipment;
/**
 * Klasa podglądu szczegółów zamówienia, refaktoryzacja z m_zamowienia/Module.class.php->Details
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-21 
 * @copyrights Marcin Chudy - Profit24.pl
 */
include_once('orderItemsDetails.class.php');
class orderDetails {

  /**
   * @var DatabaseManager $pDbMgr
   */
  public $pDbMgr;
  

  protected $aConfig;
  
  /**
   * @var Admin oParent
   */
  public $oParent;
  
  /**
   * Id aktualnie zalogowanego uzytkownika
   * 
   * @var int $iUId
   */
  protected $iUId;
  public $sMsg;
  protected $sModule;
  protected $aLang;
  protected $pSmarty;
  
  /**
   * @var Module__zamowienia__alert $oZamAlert 
   */
  protected $oZamAlert;
  
  /**
   * @var Module__zamowienia__mail $oOrderEmails 
   */
  protected $oOrderEmails;
  
  
  protected $bIsOrderLocked;
  
  protected $bBlockComments;
  
  /**
   * @var \orderItemDetails $oOrderItemDetails
   */
  protected $oOrderItemDetails;
  
  /**
   * @var orderUserDetails $oOrderUseDetails
   */
  protected $oOrderUserDetails;
    protected $ordersPacking;

    /**
   * Konstruktor
   * 
   * @param DatabaseManager $pDbMgr
   * @param array $aConfig
   * @param Admin $oParent
   * @param int $iUId
   */
  public function __construct($pDbMgr, $aConfig, $pSmarty, Admin $oParent, $iUId, $sModule) {
    $this->pDbMgr = $pDbMgr;
    $this->aConfig = $aConfig;
    $this->oParent = $oParent;
    $this->sModule = $sModule;
    $this->iUId = $iUId;
    $this->aLang = $aConfig['lang'][$sModule];
    $this->pSmarty = $pSmarty;
    
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		include_once('View/View.class.php');
    include_once('orderUserDetails.class.php');
    include_once($_SERVER['DOCUMENT_ROOT'].'omniaCMS/modules/m_zamowienia/Module_alert.class.php');
    include_once($_SERVER['DOCUMENT_ROOT'].'omniaCMS/modules/m_zamowienia/Module_mail.class.php');

    $this->oZamAlert = new Module__zamowienia__alert($pSmarty, false);
    $this->oOrderEmails = new Module__zamowienia__mail($pSmarty, false);
    $this->oOrderItemDetails = new orderItemsDetails($pDbMgr, $aConfig, $sModule, $pSmarty);
    $this->oOrderUserDetails = new orderUserDetails($pDbMgr, $aConfig, $sModule);
  }// end of __construct() method
  
  
	/**
	 * Metoda wyswietla szczegolowe informacje o zamowieniu z mozliwoscia
	 * modyfikacji elementów, danych użytkownika i statusu zamówienia
	 *
	 * @param		object	$pSmarty
   * @param   integer $iPId - id zakładki
	 * @param		integer	$iId	Id wysiwetlanego zamowienia
	 * @return	void
	 */
	public function Details(&$pSmarty, $iPId, $iId, $bAssignSmarty = TRUE) {
    
    $aOrder = $this->_getOrderData($iId);
    // sprawdzamy i ustawiamy flagi blokowania zamówienia
    $this->_checkSetLocked($iId);
    
    
		if (!empty($aOrder)) {
      if ($aOrder['internal_status'] === '0' && $aOrder['active'] === '0') {
        $bIsOrderLocked = true;
        $bBlockComments = false;
      }
    
      $sPrintListReadyInfo = '';
      if ($aOrder['print_ready_list'] == '1' || $aOrder['order_on_list_locked']) {
        $sPrintListReadyInfo = getMessage(_("Zamówienie zostało wydrukowane do zebrania, proszę nie modyfikować książek oraz ich ilości w zamównieniu. <br />Dane takie jak dane do faktury mogą zostać jeszcze zmienione."));
      }
    
			// pobierz symbol metody transportu
			$sTransportSymbol = $this->_getTransportMeansSymbol($aOrder['transport_id']);
      

      /* ********************** Zwijak zamówienia    ***/
      $pRollOutForm = $this->_getRollOutForm();

      /* ********************** Szczegóły zamówienia    ***/
      $pOrderItemsDetailsForm = $this->oOrderItemDetails->getDetailsForm($iId, $iPId, $aOrder);
      
      /* ********************** Dane adresowe zamówienia    ***/
      $pOrderAddressesAndOrderDetails = $this->_getOrderAddressesAndOrderDetailsForm($iId, $aOrder, $sTransportSymbol);
      
			/* ********************** Dodatkowe informacje do zamówienia    ***/
      $pAdditionalInfoForm = $this->_getOrderAdditionalInfoForm($iId, $iPId, $aOrder);
			
      
			/* ******** Status zamówienia ****************************/			
      $pOrderStatusHistory = $this->_getOrderStatusChangesHistoryForm($iId, $iPId, $aOrder);
      
				
      /* ******** Płatnosci do zamowienia ****************************/			
      $pOrderPaymantFormView = $this->_getOrderPaymantFormView($pOrderItemsDetailsForm, $iId, $iPId, $aOrder, $sTransportSymbol);

      $iTPaczkomatyId = $this->oOrderItemDetails->getTransportMeansIdBySymbol("paczkomaty_24_7");
      
      $sJS = $this->_getJS($iId, $iPId, $aOrder['internal_status'], $iTPaczkomatyId, $aOrder['bookstore'], $bBlockComments, $bIsOrderLocked);

			/** komentarze uzytkownika **/
      if ($aOrder['user_id'] > 0) {
        $pUserComments = $this->_getUserCommentForm($aOrder['user_id'], $iId);
      }
      
      $pPaidForm = $this->getPaidAmountForm($aOrder, $iId, $iPId);
      $sAddressHTML = '';
      
      if ($this->checkUserPrivilagesAddressOrderDetails() === true) {
        $sAddressHTML = $pOrderAddressesAndOrderDetails->ShowForm();
      }
      
      if ($bAssignSmarty === TRUE) {
        $pSmarty->assign('sContent', 
                (!empty($this->sMsg) ? $this->sMsg : '')
                .$sJS
                .$sPrintListReadyInfo
                .ShowTable(
                        $pAdditionalInfoForm->ShowForm()
                        .($aOrder['user_id'] > 0 ? $pUserComments->ShowForm() : '')
                        .$pRollOutForm->ShowForm()
                        .$pOrderItemsDetailsForm->ShowForm()
                        .(isset($pPaidForm) ? $pPaidForm->ShowForm() : '')
                        .$sAddressHTML
                        .$pOrderStatusHistory->ShowForm()
                        .$pOrderPaymantFormView->ShowForm()));
      } else {
        return ($sJS.
                $sPrintListReadyInfo
                .ShowTable(
                        $pAdditionalInfoForm->ShowForm()
                        .($aOrder['user_id'] > 0 ? $pUserComments->ShowForm() : '')
                        .$pRollOutForm->ShowForm()
                        .$pOrderItemsDetailsForm->ShowForm()
                        .(isset($pPaidForm) ? $pPaidForm->ShowForm() : '')
                        .$sAddressHTML
                        .$pOrderStatusHistory->ShowForm()
                        .$pOrderPaymantFormView->ShowForm()));
      }
		}
	} // end of Details() function
  
  
  /**
   * 
   * @return boolean
   */
  private function checkUserPrivilagesAddressOrderDetails() {
    
    if (isset($_SESSION['user']['priv_address_data']) && $_SESSION['user']['priv_address_data'] == '1') {
      return true;
    }
    return false; 
  }
  
  /**
   * 
   * @param array $aOrder
   * @param int $iId
   * @param int $iPId
   * @return FormTable
   */
  private function getPaidAmountForm($aOrder, $iId, $iPId) {
    
    if ($this->isMegaSupervisor($_SESSION['user']['name'])) {
      include_once('Form/FormTable.class.php');
      $oForm = new FormTable('change_paid_amount', '', array('action' => phpSelf(array('do'=>'change_paid_amount', 'id' => $iId, 'ppid' => $iPId))), array(), true);
      $oForm->addHidden('do', 'change_paid_amount');
      $oForm->AddMergedRow(_('Zapłacono'), array('class' => 'merged'));
      
      $sPaidAmount = str_replace('.', ',', $aOrder['paid_amount']);
      $oForm->AddText('paid_amount', _('Zapłacono'), $sPaidAmount, array(), '', 'text', false, '');
      $oForm->AddInputButton('save', _('Zapisz'), array(), 'submit');
      return $oForm;
    }
    return null;
  }// end of getPaidAmountForm() method
  
  
  /**
   * 
   * @param string $sLogin
   * @return bool
   */
  private function isMegaSupervisor($sLogin) {
    
    return ($sLogin == 'agolba' || $sLogin == 'mchudy') ? TRUE : FALSE;
  }// end of isMegaSupervisor() method
  
  
  /**
   * Metoda sprawdza, czy zamówienie jest zablokowane przez inną osobę, i ustawia odpowiednio flagi
   * 
   * @param int $iId
   * @return boolean
   */
  protected function _checkSetLocked($iId) {
    
    if (class_exists('memcached')) {
      $mReturLocked = $this->oParent->oOrdersSemaphore->isLocked($iId, $this->iUId);
      if ($mReturLocked === FALSE) {
        $this->oParent->oOrdersSemaphore->lockOrder($iId, $this->iUId);
        $this->bIsOrderLocked = false;
        $this->bBlockComments = true;
      } else {
        $this->bIsOrderLocked = true;
        $this->bBlockComments = true;
        $this->sMsg = GetMessage(sprintf($this->aLang['order_locked'], $this->_getAdminUser(intval($mReturLocked))), true);
      }
    }
    return true;
  }// end of _checkSetLocked() method
  
  
  /**
   * Metoda pobiera dane administratora
   * 
   * @global array $aConfig
   * @param int $iId
   * @return string
   */
	protected function _getAdminUser($iId) {
    
		$sSql = "SELECT CONCAT(name, ' ', surname, ' (', login, ') ')
							FROM users
							 WHERE id = ".intval($iId);
		return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of _getAdminUser() method
  
  
	/**
	 * Pobiera dane zamówienia
   * 
	 * @param int $iOrderId - id zamowienia
	 * @return array
	 */
	protected function _getOrderData($iOrderId){
    
		$sSql = "SELECT *
						FROM orders
						WHERE id = ".$iOrderId;
		return $this->pDbMgr->GetRow('profit24', $sSql);
	} // end of getOrderData() method
  
  
	/**
	 * Metoda pobiera symbol dla metody transportu
	 *
	 * @param int $iTransportId
	 * @return string
	 */
	protected function _getTransportMeansSymbol($iTransportId) {
		
		$sSql = "SELECT symbol FROM orders_transport_means
						 WHERE id = ".$iTransportId;
		return $this->pDbMgr->GetOne('profit24', $sSql);
	}// end of _getTransportMeansSymbol() method
  
  
  /**
   * Metoda generuje formularz RollOut
   * 
   * @return FormTable
   */
  protected function _getRollOutForm() {
    
    $pRollOutForm = new FormTable('order_header');
    $pRollOutForm->AddMergedRow($this->aLang['order_data'].' - <a id="toggleOrdersDetails" href="javascript:void(0);" style="text-decoration: none; font-size: 12px;" onclick="toggleOrdersDetails();">Zwiń <img src=\'gfx/expanded.gif\' /></a>', array('class'=>'merged', 'style'=>'padding-left: 10px'));
    return $pRollOutForm;
  }// end of _getRollOutForm() method
  
  
  
  /**
   * Metoda zwraca informacje na temat danych adresowych użytkownika, 
   *  wraz z informacjami szczegółowymi na temat zamówienia
   * 
   * @param int $iId
   * @param array $aOrder
   * @param string $sTransportSymbol
   * @return FormTable
   */
  private function _getOrderAddressesAndOrderDetailsForm($iId, $aOrder, $sTransportSymbol) {

    /* ******** Dane zamawiającego ****************************/	
    $aAddresses = $this->oOrderItemDetails->getOrderAddresses($iId);		

    $pForm3 = new FormTable('order_user_data', '', array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>155), true, array(), array('class' => 'bigger'));
    $pForm3->AddHidden('do', 'update_user_data',array('id'=>'order_user_data_do'));

    $pForm3->AddMergedRow($this->aLang['user_data'], array('class'=>'merged', 'style'=>'padding-left: 10px'));

    $pForm3->AddMergedRow($this->oOrderUserDetails->getUserDetails($aOrder, $aAddresses, $sTransportSymbol));
    if (intval($aOrder['order_status']) < 3) {
      $pForm3->AddMergedRow($pForm3->GetInputButtonHTML('edit_user_data_show', $this->aLang['edit_user_data'],
                            array('onclick'=>'show_user_details();'),'button'),		array('class'=>'userDetailsFormth'),array('class'=>'userDetailsFormButtonShow'));
      $pForm3->AddMergedRow($this->oOrderUserDetails->getUserDetailsForms($aOrder, $aAddresses, $pForm3, $sTransportSymbol),array('class'=>'userDetailsFormth'),array('class'=>'userDetailsForm', 'style'=>'display:none;'));

      $pForm3->AddMergedRow($pForm3->GetInputButtonHTML('edit_user_data_hide', $this->aLang['edit_user_data_hide'],
                            array('onclick'=>'hide_user_details();'),'button'),	array('class'=>'userDetailsFormth'),array('class'=>'userDetailsFormButtonHide', 'style'=>'display:none;'));


      $pForm3->AddMergedRow($pForm3->GetInputButtonHTML('send', $this->aConfig['lang']['common']['button_1']).'&nbsp;'.
                            $pForm3->GetInputButtonHTML('edit_user_data_hide', $this->aLang['edit_user_data_hide'],
                            array('onclick'=>'hide_user_details();'),'button'),
                            array('class'=>'userDetailsFormth'),array('class'=>'userDetailsForm', 'style'=>'display:none;'));
    }
    //wykrzyknik jeśli są jakieś zamówienia
    $sSql='SELECT COUNT(id) FROM orders WHERE `email`=\''.$aOrder['email'].'\' AND `order_status` <> "4" AND `order_status` <> "5" ';
    $iSimilarCounter=Common::GetOne($sSql);

    // jeśli nie istnieje druga faktura, numer transportu, typ płatności i jeśli 
    // zapłacono za zamowienie więcej lub tyle samo co należy zapłacić 
    //		lub jeśli typ płatności to za pobraniem
    if ($aOrder['second_invoice'] == '0' && $aOrder['transport_number']=='' && $aOrder['second_payment_enabled']=='0' && 
            ($aOrder['paid_amount'] >= $aOrder['to_pay'] || $aOrder['payment_type']=='postal_fee'|| $aOrder['payment_type']=='bank_14days')) {

      //pobranie informacji o innych zmaówieniach jeśli możnaby je połączyć

      // Warunki połączenia:
      //	identyczny email
      //	status wcześniejszy niż skompletowane (4)
      //	zamówienia muszą być tego samego użytkownika
      //	nie może być nadany nr listu przewozowego
      //	forma transportu musi być identyczna
      //	modyfikacja 19.06.2012 nie można połączyć jeśli metoda transportu paczkomaty i paczkomaty są różne
      //	data wysyłki musi być zgodna (lub może być nie ustawiona)
      //	nie może istnieć druga faktura
      //	nie może istnieć druga forma płatności
      //	zapłacono za zamowienie więcej lub tyle samo co należy zapłacić lub jeśli typ płatności to za pobraniem
      //	
      $bIgnoreTransportAdress = false;
      $sPointOfReceiptSQL = '';
			// jeśli metoda transportu paczkomaty lub poczta polska odbiór w punkcie
      $oShipment = new Shipment($aOrder['transport_id'], $this->pDbMgr);
			if ($oShipment->isReceptionAtPoint()) { 
        // to punkt odbioru paczki ma być ten sam
        $sPointOfReceiptSQL = ' AND point_of_receipt="'.$aOrder['point_of_receipt'].'"';
        $bIgnoreTransportAdress = true;
      }
      $sSql = "SELECT id AS value, order_number AS label FROM ".$this->aConfig['tabls']['prefix']."orders 
        WHERE
          email = '".$aOrder['email']."' 
          AND order_status <> '4' 
          AND order_status <> '5'
          AND user_id = '".intval($aOrder['user_id'])."' 
          AND transport_number IS NULL 
          AND transport = '".$aOrder['transport']."'
          ".$sPointOfReceiptSQL."
          AND send_date = '".$aOrder['send_date']."'
          AND second_invoice = '0' 
          AND second_payment_enabled = '0'
          AND id != '".$iId."' 
          AND (paid_amount >= to_pay OR payment_type = 'postal_fee' OR payment_type = 'bank_14days')
      ";

      $aPossibleConnect = Common::GetAll($sSql);

      // dane adresowe
      $aCurAddresses = $this->_trim_r($this->oOrderItemDetails->getOrderAddresses($iId, $bIgnoreTransportAdress));
      foreach($aPossibleConnect as $iKey =>$aSingleConnect) {
        $aOneAddres = $this->_trim_r($this->oOrderItemDetails->getOrderAddresses($aSingleConnect['value'], $bIgnoreTransportAdress));

        // jeśli dane adresowe są  rózne nie można połączyć zamówień
        if(print_r($aOneAddres, true) != print_r($aCurAddresses, true))	{
            unset($aPossibleConnect[$iKey]);
        }
      }
      $aMrgPossibleConnect = array_merge(array(array('value'=>'0','label'=>$this->aLang['select_to_connect'])), $aPossibleConnect);
    }
    
    if ($aOrder['payment_type'] == 'bank_14days') {
      $pForm3->AddEditableText('invoice_to_pay_days', _('Liczba dni do opłacenia FV: '), 
                               'save_invoice_to_pay_days', 'order_user_data_do', 
                                (isset($aOrder['invoice_to_pay_days']) && intval($aOrder['invoice_to_pay_days']) > 0 ? intval($aOrder['invoice_to_pay_days']) : '14'), 
                              array('style' => 'width: 60px; height: 20px;'),
                              'order_user_data'
              );
    }

    if ($aOrder['discount_code'] != '' || $aOrder['discount_code_value'] > 0.00) {
      $pForm3->AddRow(_('Kod rabatowy'), ' <span style="font-size: 2.5em; color: red; font-weight:  bold;">'.$aOrder['discount_code'].'</span>');
    }

    $aGatisData = $this->getGratisInformation($iId);
    if (!empty($aGatisData)) {
      foreach ($aGatisData as $iKey => $aGratis) {
        $pForm3->AddRow('Dodano gratis: ', $aGratis['name'].' - '.formatDate($aGratis['created']), ' '. $aGratis['created_by']);
      }
    }

    $pForm3->AddRow($this->aLang['phone'], ' '.$aOrder['phone']);
    $pForm3->AddRow($this->aLang['email'], '<a href="mailto:'.$aOrder['email'].'">'.$aOrder['email']."</a>"
                      .' <div style="margin: 5px; float: left;"><a alt="Wyślij email do użytkownika" title="Wyślij email do użytkownika" href="'.  phpSelf(array('pid' => $iId, 'action' => 'mail', 'do' => 'add')).'"><img src="gfx/icons/send_ico.gif" border="0" alt="przejdź do konta użytkownika" /></a></div>'
                      .' <div style="margin: 5px; float: left;"><a alt="Przejdź do konta użytkownika" title="Przejdź do konta użytkownika" href="'.  phpSelf(array('module' => 'm_konta', 'module_id' => 28, 'search' => $aOrder['email'], 'save_referer_url' => urlencode('Zamówienie '.$aOrder['order_number']))).'"><img src="gfx/icons/authors_ico.gif" border="0" alt="przejdź do konta użytkownika" /></a></div>'
                      .' ('.$this->_getCountPrevOrders($aOrder['user_id'], $aOrder['website_id']).')'
                      .'&nbsp;&nbsp; <img style="vertical-align:middle;" src="gfx/b_'.$this->aConfig['invoice_website_symbol_'.$aOrder['website_id']].'.gif" /> '
                      .($iSimilarCounter>1?'<img style="vertical-align:middle;" src="gfx/msg_1.gif" alt="Istnieją inne niezrealizowane zamówienia tego użytkownika" title="Istnieją inne niezrealizowane zamówienia tego użytkownika" />':'')
                      .($this->_checkCorrectionOther($iId, $aOrder['user_id']) ? 
                               '<img style="vertical-align:middle;" src="gfx/msg_2.png" alt="Istnieją inne nieopłacone zamówienia tego użytkownika" title="Istnieją inne nieopłacone zamówienia tego użytkownika" /> &nbsp;&nbsp;' 
                               :'')
                      .(count($aMrgPossibleConnect)>1?($pForm3->GetSelectHTML('connect_to', $this->aLang['connect_to'], array('style'=>'width:250px;'), $aMrgPossibleConnect, 0, '', false)
                      .' '.$pForm3->GetInputButtonHTML('connect_to_connect', $this->aLang['connect_to_connect'], array('onclick'=>'send_connect_to();'),'button')
                      ):'')
                      .' &nbsp; <img style="vertical-align:middle;" src="gfx/ceneo'.($aOrder['ceneo_agreement'] != '1' ? '_disabled' : '').'.gif" alt="Zgoda na przekazanie danych do Ceneo" title="Zgoda na przekazanie danych do Ceneo" /> &nbsp;&nbsp;'
                   );

    $pForm3->AddEditableText('seller_streamsoft_id', _('Handlowiec'),
            'save_seller_streamsoft_id', 'order_user_data_do',
            $aOrder['seller_streamsoft_id'],
              array('style' => 'width: 60px; height: 20px;'),
              'order_user_data'
            );


    $pForm3->AddRow($this->aLang['transport_remarks'], '<div id="edit_transport_norm" style="font-size:14px; font-weight: bold; color:#0b0;">'.$aOrder['transport_remarks'].'&nbsp;'.
    //$pForm3->GetInputButtonHTML('edit_transport_show', $this->aLang['edit_transport_remarks'], array('onclick'=>'show_transport_remarks();'),'button').
    '</div><div id="transportForm" style="display:none;">'.$pForm3->GetTextHTML('transport_remarks', $this->aLang['transport_remarks'], $aOrder['transport_remarks'], array('maxlength' => 150,'style'=>'width:250px;'), '', 'text', false).
    $pForm3->GetInputButtonHTML('transport_remarks_send', $this->aLang['transport_remarks_send'], array('onclick'=>'send_transport_remarks();'),'button').
    $pForm3->GetInputButtonHTML('transport_remarks_hide', $this->aLang['transport_remarks_hide'], array('onclick'=>'hide_transport_remarks();'),'button').'</div>');

    //data wysylki
    $pForm3->AddRow($this->aLang['send_date'], '
    <div id="edit_send_date">'.($aOrder['send_date']!='0000-00-00'?$aOrder['send_date']:'').'&nbsp;'.
    //$pForm3->GetInputButtonHTML('edit_send_date_show', $this->aLang['edit_transport_remarks'], array('onclick'=>'show_send_date();'),'button').
    '</div>
    <div id="send_dateForm" style="display:none;">'.$pForm3->GetTextHTML('send_date_input', $this->aLang['send_date'], $aOrder['send_date'], array('maxlength' => 10,'style'=>'width:75px;'), '', 'date', false).
    $pForm3->GetInputButtonHTML('send_date_send', $this->aLang['transport_remarks_send'], array('onclick'=>'send_send_date();'),'button').
    $pForm3->GetInputButtonHTML('send_date_hide', $this->aLang['transport_remarks_hide'], array('onclick'=>'hide_send_date();'),'button').'</div>
    ');

    $iTotalOrderWeight = $this->_getTotalOrderWeight($iId);
    $pForm3->AddRow($this->aLang['total_order_weight'], $iTotalOrderWeight);
    if ($aOrder['shipment_date_expected'] != '') {
      $pForm3->AddRow($this->aLang['shipment_date_expected'], ' '.formatDateClient($aOrder['shipment_date_expected']).' - (uwzg. datę aktywacji)');
    }
    $pForm3->AddRow($this->aLang['shipment_date'], ' '.formatDateClient($aOrder['shipment_date']).' - (data wysyłki wyświetlona klientowi)');
    $pForm3->AddRow($this->aLang['transport_date'], ' '.formatDateClient($aOrder['transport_date']));

      // tutaj dodajemy info o zamówieniach które razem mają wyjechać
      $pForm3 = $this->addLinkedTransportInfo($pForm3, $iPId, $iId);
		
    if(!empty($aOrder['user_id'])) {
      $pForm3->AddRow($this->aLang['user_type'], $this->aLang['user_type_registred']);
    }
    else {
      $pForm3->AddRow($this->aLang['user_type'], $this->aLang['user_type_not_registred']);
    }
    if(!empty($aOrder['client_ip'])) {
      $pForm3->AddRow($this->aLang['client_ip'], $aOrder['client_ip']);
    }
    if(!empty($aOrder['client_host'])) {
      $pForm3->AddRow($this->aLang['client_host'], $aOrder['client_host']);
    }			
    if ($aOrder['erp_send_ouz'] != '') {
      $pForm3->AddRow(_('Wysyłka do Streamsoft OUZ'), formatDateTimeClient($aOrder['erp_send_ouz']));
    }
    if ($aOrder['erp_export'] != '') {
      $pForm3->AddRow(_('Faktura w Streamsoft '), formatDateTimeClient($aOrder['erp_export']));
    }
    if($aOrder['source'] != '') {
      // zrodlo zamowienia
      $pForm3->AddMergedRow($this->aLang['source'], array('class'=>'merged', 'style'=>'padding-left: 10px'));
      $pForm3->AddMergedRow($this->GetSourceDetails($pForm3, $aOrder));
    }
    return $pForm3;
  }// end of _getOrderAddressesAndOrderDetailsForm() method


  /**
   * @param $iOrderId
   * @return array
   */
  private function getGratisInformation($iOrderId) {

    $this->gratis = new \magazine\Gratis($this->pDbMgr);
    $aGratis = $this->gratis->getOrderAddedGratis($iOrderId);
    return $aGratis;
  }
  
  
	/**
	 * Metoda wyswietla informacje o zrodle zamowienia
	 *
	 * @param		object	$pForm	- obiekt formularza
	 * @param	array ref	$aOrder	- dane zamowienia
	 * @return	void
	 */
	private function GetSourceDetails(&$pForm, &$aOrder) {

		$pForm->AddRow($this->aLang['source'], $aOrder['source'].($aOrder['source'] == 'newsletter' ? ' ('.getNewsletter((double) $aOrder['source_id']).')' : ''));
	} // end of GetSourceDetails() function
  
  
  /**
   * Metoda pobiera formularz z widokiem historii płatności
   * 
   * @param int $iId
   * @param int $iPId
   * @param array $aOrder
   * @param string $sTransportSymbol
   * @return FormTable
   */
  private function _getOrderPaymantFormView(&$pRollOutForm, $iId, $iPId, $aOrder, $sTransportSymbol) {
    global $aConfig;
    
		$this->aConfig['lang']['m_zamowienia']['no_items'] = _('Brak płatności');	
    $pOrderPaymantFormView = new FormTable('order6', '', array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>155), $this->aConfig['common']['js_validation'], array(), array('class' => 'bigger'));
    $pOrderPaymantFormView->AddMergedRow($this->aLang['order_payments_header'], array('class'=>'merged', 'style'=>'padding-left: 10px'));

		$aHeader6 = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'form'	=>	false,
			'checkboxes'	=>	false
		);
		$aAttribs6 = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader6 = array(
			array(
				'db_field'	=> 'id',
				'content'	=> 'ID',
				'sortable'	=> true,
				'width'	=> '10'
			),
			array(
				'db_field'	=> 'statement_id',
				'content'	=> 'ID przelewu',
				'sortable'	=> true,
				'width'	=> '10'
			),
			array(
				'db_field'	=> 'date',
				'content'	=> $this->aLang['date'],
				'sortable'	=> true,
				'width'	=> '140'
			),
			array(
				'db_field'	=> 'ammount',
				'content'	=> $this->aLang['ammount'],
				'sortable'	=> true,
				'width'	=> '45'
			),
			array(
				'db_field'	=> 'type',
				'content'	=> $this->aLang['type'],
				'sortable'	=> true,
				'width'	=> '160'
			),
			array(
				'db_field'	=> 'iban',
				'content'	=> $this->aLang['iban'],
				'sortable'	=> false,
				'width'	=> '80'
			),
			array(
				'db_field'	=> 'reference',
				'content'	=> $this->aLang['reference'],
				'sortable'	=> false,
				'width'	=> '80'
			),
			array(
				'db_field'	=> 'title',
				'content'	=> $this->aLang['title'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'matched_by',
				'content'	=> $this->aLang['resetBalance_author'],
				'sortable'	=> true
			),
      array(
				'db_field'	=> 'actions',
				'content'	=> '',
				'sortable'	=> true
			)
		);
		$aColSettings = array(
				'action' => array (
					'actions'	=> array ('delete', 'renouncement'),
					'params' => array (
                          'renouncement' => [
                              'module' => 'm_finanse',
                              'action' => 'bank_renouncement',
                              'do' => 'addRenouncementByStatement',
                              'order_id' => $iId,
                              'payment_id' => '{id}',
                              'statement_id' => '{statement_id}'
                          ],
                          'delete' => array(
                            'do' => 'detele_order_payment',
                            'id' => $iId, 'ppid'=> $iPId,
                            'payment_id' => '{id}',
                            'statement_id' => '{statement_id}'
                          )
                    ),
					'show' => false,
                    'confirms' => [
                        'renouncement' => _('Czy chcesz zrobić odstąpienie ?')
                    ]
				)
			);
			$aRecordsHeader2 = array(
			array(
				'db_field'	=> 'id',
				'content'	=> 'ID',
				'sortable'	=> true,
				'width'	=> '10'
			),
			array(
				'db_field'	=> 'date',
				'content'	=> $this->aLang['date'],
				'sortable'	=> true,
				'width'	=> '50%'
			),
			array(
				'db_field'	=> 'amount',
				'content'	=> $this->aLang['ammount'],
				'sortable'	=> true,
				'width'	=> '45'
			),
			array(
				'db_field'	=> 'type',
				'content'	=> $this->aLang['type'],
				'sortable'	=> true,
				'width'	=> '140'
			),
			array(
				'db_field'	=> 'transaction_id',
				'content'	=> $this->aLang['transaction_id'],
				'sortable'	=> true,
				'width'	=> '120'
			),
			array(
				'db_field'	=> 'status',
				'content'	=> $this->aLang['status'],
				'sortable'	=> true,
				'width'	=> '100'
			),
            array(
                'db_field'	=> 'actions',
                'content'	=> 'Akcja',
                'sortable'	=> true,
                'width'	=> '10'
            )
		);
		$aColSettings2 = array(
            'action' => array (
                'actions'	=> array ('renouncement'),
                'params' => array (
                    'renouncement' => [
                        'module' => 'm_finanse',
                        'action' => 'bank_renouncement',
                        'do' => 'addRenouncementByPayU',
                        'order_id' => $iId,
                        'orders_platnoscipl_sessions_id' => '{id}',
                    ],
                ),
                'show' => false,
                'confirms' => [
                    'renouncement' => _('Czy chcesz zrobić odstąpienie ?')
                ]
            ),
            'order_id'	=> array(
                'show'	=> false
            )
    );	
    $pView = new EditableView('order_details', $aHeader6, $aAttribs6);
    $pView->AddRecordsHeader($aRecordsHeader6);
    
    
    
    
    $sSql = 'SELECT id, date_created AS date, amount,ptype as type, transaction_id, status FROM orders_platnoscipl_sessions WHERE `order_id`='.$iId.' ORDER BY `date_created`';
    $aRecords2 = Common::GetAll($sSql);
    
    foreach($aRecords2 as $iKey=>$aRecord) {
      $aRecords2[$iKey]['type']=Common::GetOne('SELECT name FROM orders_platnosci_payment_types WHERE ptype=\''.$aRecords2[$iKey]['type'].'\'');
      $aRecords2[$iKey]['amount']=Common::FormatPrice($aRecords2[$iKey]['amount']/100);	
      $aRecords2[$iKey]['status']=$this->aLang['platnosci_status_'.$aRecords2[$iKey]['status']];
      if (strlen($aRecords2[$iKey]['date']) < 5) {
        $aRecords2[$iKey]['date'] = 'b.d.';
      }
      if (strlen($aRecords2[$iKey]['transaction_id']) < 5) {
        $aRecords2[$iKey]['transaction_id'] = 'b.d.';
      }

      if (strlen($aRecords2[$iKey]['date']) < 5 && strlen($aRecords2[$iKey]['transaction_id']) < 5 && $aRecords2[$iKey]['type'] == '') {
        unset($aRecords2[$iKey]);
      }

      if ('99' != $aRecord['status']) {
          $aRecords2[$iKey]['disabled'] = ['renouncement'];
      }
    }

    if ($aOrder['order_status'] != '4' || false === $this->checkUserPrivilegesFinance()) {
      $aColSettings2 = array('');
      array_pop($aRecordsHeader2);
    }
    $pView2 = new View('payment_history2', $aHeader6, $aAttribs6);
    $pView2->AddRecordsHeader($aRecordsHeader2);

    $pView2->AddRecords($aRecords2, $aColSettings2);
    
    // dodanie rekordow do widoku
    $aRecords = $this->getPaymentList($iId);

    foreach ($aRecords as $key => $row) {
        if ($row['ammount'] <= 0) {
            $aRecords[$key]['disabled'] = array('renouncement');
        }
    }

    if ($aOrder['order_status'] != '4' || false === $this->checkUserPrivilegesFinance()) {
        $aColSettings['action']['actions'] = array ('delete');
    }
    $pView->AddRecords($aRecords, $aColSettings);


    $pOrderPaymantFormView->AddHidden('do', '', array('id' => 'order6_do'));
    $pOrderPaymantFormView->AddMergedRow($pView->Show());

    $pOrderPaymantFormView->AddMergedRow($this->aLang['order_payments_header2'], array('class'=>'merged', 'style'=>'padding-left: 10px'));

    $pOrderPaymantFormView->AddMergedRow($pView2->Show());

    $viewReturnHTML = $this->getReturnView($iId, $aHeader6, $aAttribs6);
    if ('' != $viewReturnHTML) {
      $pOrderPaymantFormView->AddMergedRow(_('Zwroty/Odstąpienia'), array('class' => 'merged', 'style' => 'padding-left: 10px'));
      $pOrderPaymantFormView->AddMergedRow($viewReturnHTML);
    }
    // koniec  platnosci

    // Uwagi do zamówienia
    /**if(!empty($aOrder['remarks'])) {
      $pOrderPaymantFormView->AddMergedRow($this->aLang['remarks'], array('class'=>'merged', 'style'=>'padding-left: 10px'));
      $pOrderPaymantFormView->AddMergedRow('<span style="float:left; font-size: 14px; font-weight: bold; color: red;">'.$aOrder['remarks'].'</span>');
      if ($aOrder['internal_status']>='5') {
        if ($aOrder['internal_status'] == '5') {
          $aAttrRemarks = array('id' => 'remarks_read');
        } else {
          $aAttrRemarks = array('id' => 'remarks_read', 'disabled' => 'disabled');
        }

        $pOrderPaymantFormView->AddCheckbox('remarks_read', $this->aLang['remarks_read'], $aAttrRemarks, '', ($aOrder['remarks_read']=='1'), ($aOrder['internal_status']=='5'));
      }
      if (empty($aOrder['remarks_read_1']) && $aOrder['internal_status'] <= 1) {
        $pRollOutForm->AddHidden('remarks_read_1', '');
        $pOrderPaymantFormView->AddCheckbox('remarks_read_1_ck', $this->aLang['remarks_read'], array('onclick' => '$(\'#remarks_read_1\').val($(this).attr(\'checked\'));'), '', ($aOrder['remarks_read_1'] != ''), $aOrder['internal_status'] == '1');
      }
    }*/
    
      /**if (!empty($aOrder['transport_remarks'])) {
          $pOrderPaymantFormView->AddMergedRow(_('Uwagi do kuriera'), array('class' => 'merged', 'style' => 'padding-left: 10px'));
          $pOrderPaymantFormView->AddMergedRow('<span style="float:left; font-size: 14px; font-weight: bold; color: red;">' . $aOrder['transport_remarks'] . '</span>');
//          if (empty($aOrder['remarks_read_1']) && $aOrder['internal_status'] <= 1) {
//              $pRollOutForm->AddHidden('remarks_read_1', '');
//              $pOrderPaymantFormView->AddCheckbox('remarks_read_1_ck', $aConfig['lang'][$this->sModule]['remarks_read'], array('onclick' => '$(\'#remarks_read_1\').val($(this).attr(\'checked\'));'), '', ($aOrder['remarks_read_1'] != ''), $aOrder['internal_status'] == '1');
//          }
      }*/


      //Faktura - checkbox drukuj fakturę
      if($aOrder['order_status']<3) {
          $pOrderPaymantFormView->AddMergedRow(_('Faktura'), array('class'=>'merged', 'style'=>'padding-left: 10px'));
          $pOrderPaymantFormView->AddCheckbox('print_fv', $this->aLang['print_fv'], array('onclick' => 'MenuNavigate(\''.phpSelf(array('do'=>'printFV', 'id' => $iId, 'ppid'=>$iPId)).'\');'), '', ($aOrder['print_fv'] ? 1 : 0), false);
      }

      // Uwagi do magazynu
      $pOrderPaymantFormView->AddMergedRow(_('Uwagi do magazynu'), array('class'=>'merged', 'style'=>'padding-left: 10px'));
      /*$pOrderPaymantFormView->AddEditableText('magazine_remarks',
                               _('Uwagi do magazynu'), 
                               'save_magazine_remarks', 
                               'order6_do', 
                               $aOrder['magazine_remarks'], 
                               array('style' => 'width: 90%; height: 80px;'),
                               'order6'
              );*/
      
    if ($aOrder['order_status'] == '4') {
      $pOrderPaymantFormView->AddMergedRow($this->aLang['correction'], array('class'=>'merged', 'style'=>'padding-left: 10px'));
      // dodanie checkboxa Korekta
      $pOrderPaymantFormView->AddCheckbox('correction', $this->aLang['correction'], array('onclick' => 'MenuNavigate(\''.phpSelf(array('do'=>'change_correction', 'id' => $iId, 'ppid'=>$iPId)).'\');'), '', ($aOrder['correction']=='1'), false);
    }

    // js odnośnie zabezpieczenia przed drukowaniem
			if ($aOrder['internal_status'] == '5') {
//        if ($sTransportSymbol == 'paczkomaty_24_7') {
////          $aTransportOptions = $this->GetTransportOptions($aOrder['transport_id']);
////          $pForm6->AddSelect('transport_option_id', $aConfig['lang'][$this->sModule]['transport_option'], array('style'=>'width:200px;'), $aTransportOptions, $aOrder['transport_option_id'], '', true, array(), array(), 'transport_mean');
//        } elseif ($sTransportSymbol == 'opek-przesylka-kurierska') {
//          // tutaj dzielimy wagę i wybieramy ilość paczek na które zostanie podzielone zamówienie
////          $aLinkedOrders = $this->getOrdersLinked($iId);
////          $iSuggestedPackegesFedEx = $this->getSuggestedCountPackages($iId, $aOrder['bookstore'], $aOrder['transport_id'], $aLinkedOrders);
////          $pForm6->AddText('transport_packages', _('Liczba paczek FedEx'), ($aOrder['transport_packages'] > 0 ? $aOrder['transport_packages'] : $iSuggestedPackegesFedEx), array('style' => 'width: 20px;', 'maxlength' => 2), '', 'uint', true);
//        }
			} else {
				if ($aOrder['transport_option_symbol']) {
					$pOrderPaymantFormView->AddRow($aConfig['lang'][$this->sModule]['transport_option'], $this->getTransportOptionLabel($aOrder['transport_option_id']));
				}
			}


    if ($aOrder['internal_status'] == '5') {
      $aTransportOptions = $this->GetTransportOptions($aOrder['transport_id']);
      $pOrderPaymantFormView->AddSelect('transport_option_id', $this->aLang['transport_option'], array('style'=>'width:200px;'), $aTransportOptions, $aOrder['transport_option_id'], '', false);
//      $pOrderPaymantFormView->AddHidden('transport_option_id', $aOrder['transport_option_id']);
    } else {
      if ($aOrder['transport_option_symbol']) {
        $pOrderPaymantFormView->AddRow($this->aLang['transport_option'], $this->getTransportOptionLabel($aOrder['transport_option_id']));
      }
    }


    $pOrderPaymantFormView->AddRow('&nbsp;',( (intval($aOrder['order_status']) < 3)?$pOrderPaymantFormView->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1'], array('onclick'=>'save_and_goback();'), 'button').'&nbsp;&nbsp;':'').
                             $pOrderPaymantFormView->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('pid')).'\');'), 'button').'&nbsp;&nbsp;'.
                             $pOrderPaymantFormView->GetInputButtonHTML('print', $aConfig['lang']['common']['print'], array('onclick'=>'window.open(\''.phpSelf(array('do'=>'print', 'id'=>$iId), array('pid')).'\',\'print\');'), 'button').
                             (
                                 ((intval($aOrder['order_status']) == 2 || intval($aOrder['order_status']) == 3 || intval($aOrder['order_status']) == 4) 
                                     &&$aOrder['transport_number'] == '' ) // tymczasowo wyłączone && intval($aOrder['transport_option_id']) > 0 && $aOrder['transport_option_symbol'] != ''
                                     ?'&nbsp;&nbsp;'.
                             $pOrderPaymantFormView->GetInputButtonHTML('print_invoice',
                                     $aConfig['lang'][$this->sModule]['print_transport'], array('onclick' => 'MenuNavigate(\''.phpSelf(array('do'=>'print_invoice', 'id'=>$iId)).'\');'), 'button')
                                     :'').
            (($aOrder['order_status'] == '4' || $aOrder['order_status'] == '3') ? ' &nbsp; '.$pOrderPaymantFormView->GetInputButtonHTML('print_invoice', $aConfig['lang'][$this->sModule]['print_invoice'], array('style' => 'color: red; font-weight: bold', 'onclick'=>'window.open(\''.phpSelf(array('do'=>'get_invoice', 'id'=>$iId)).'\',\'print\');')) : ''). //get_invoice
            (($aOrder['order_status'] == '4' || $aOrder['order_status'] == '3' || $aOrder['invoice_id'] != '') ? '' : (' &nbsp; '.$pOrderPaymantFormView->GetInputButtonHTML('print_invoice_proforma', $aConfig['lang'][$this->sModule]['print_invoice_proforma'], array('style' => 'color: red; font-weight: bold', 'onclick'=>'window.open(\''.phpSelf(array('do'=>'get_invoice_proforma', 'id'=>$iId)).'\',\'print\');'))))
    );
    return $pOrderPaymantFormView;
  }// end of _getOrderPaymantFormView() method


    /**
     * @param $iId
     * @param $aHeader6
     * @param $aAttribs6
     * @return bool|string
     */
    private function getReturnView($iId, $aHeader6, $aAttribs6) {

        $sSql = 'SELECT OPR.id, OPR.balance, OPR.type, OPR.active, OPR.submitted, OPR.bank_account, OPR.statement_title, OPR.created, OPR.created_by  
                 FROM orders_payment_returns AS OPR 
                 WHERE order_id = '.$iId;
        $paymentReturnRecords = $this->pDbMgr->GetAll('profit24', $sSql);
        foreach ($paymentReturnRecords as $key => $record) {
            if ($record['active'] == '1') {
                $paymentReturnRecords[$key]['active'] = 'W trakcie';
                if ($record['submitted'] == '1') {
                    $paymentReturnRecords[$key]['active'] .= ' - przew pobrany do pliku';
                }
            } else {
                $paymentReturnRecords[$key]['active'] = 'Zakończone';
            }
            unset($paymentReturnRecords[$key]['submitted']);
            if (OrdersPaymentReturns::TYPE_RENOUNCEMENT == $record['type']) {
                $paymentReturnRecords[$key]['type'] = 'Odstąpienie';
            } elseif (OrdersPaymentReturns::TYPE_RETURN == $record['type']) {
                $paymentReturnRecords[$key]['type'] = 'Zwrot';
            } else {
                $paymentReturnRecords[$key]['type'] = '???';
            }
        }

        $pViewReturn = new View('payment_history2', $aHeader6, $aAttribs6);

        $aRecordsHeaderReturn = array(
            array(
                'db_field'	=> 'id',
                'content'	=> 'ID',
                'sortable'	=> true,
                'width'	=> '10'
            ),
            array(
                'db_field'	=> 'balance',
                'content'	=> _('Wartość'),
                'sortable'	=> true,
                'width'	=> '50'
            ),
            array(
                'db_field'	=> 'type',
                'content'	=> _('Typ'),
                'sortable'	=> true,
                'width'	=> '45'
            ),
            array(
                'db_field'	=> 'active',
                'content'	=> _('Aktywne'),
                'sortable'	=> true,
                'width'	=> '45'
            ),
            array(
                'db_field'	=> 'bank_account',
                'content'	=> _('Numer rachunku'),
                'sortable'	=> true,
                'width'	=> '45'
            ),
            array(
                'db_field'	=> 'statement_title',
                'content'	=> _('Tytuł'),
                'sortable'	=> true,
                'width'	=> '45'
            ),
            array(
                'db_field'	=> 'created',
                'content'	=> _('Utworzono'),
                'sortable'	=> true,
                'width'	=> '45'
            ),
            array(
                'db_field'	=> 'created_by',
                'content'	=> _('przez'),
                'sortable'	=> true,
                'width'	=> '45'
            ),
        );
        $pViewReturn->AddRecordsHeader($aRecordsHeaderReturn);
        $aColSettingsReturn = [];
        $pViewReturn->AddRecords($paymentReturnRecords, $aColSettingsReturn);
        if (!empty($paymentReturnRecords)) {
            return $pViewReturn->Show();
        } else {
            return false;
        }
    }


    /**
     * @return bool
     */
    private function checkUserPrivilegesFinance() {

        $sSql = 'SELECT id FROM modules WHERE symbol = "m_finanse" ';
        $moduleId = $this->pDbMgr->GetOne('profit24', $sSql);
        return hasModulePrivileges($moduleId);
    }

  /**
   * 
   * @param int $iId
   * @return array
   */
  private function getPaymentList($iId) {
    
    $sSql = 'SELECT id, statement_id, date, ammount, 
                    type, iban, reference, title, matched_by, erp_export
             FROM orders_payments_list 
             WHERE `order_id`='.$iId.' AND `type`<>\'platnoscipl\' 
             ORDER BY `date`';
    $aRecords = Common::GetAll($sSql);
    
    foreach ($aRecords as $iKey => $aRecord) {
      if (isset($this->aConfig['lang']['m_zamowienia_payment_types']['ptype_'.$aRecord['type']])) {
        $aRecords[$iKey]['type']=$this->aConfig['lang']['m_zamowienia_payment_types']['ptype_'.$aRecord['type']];	
      }
      if ($this->checkPaymentDisableDelete($aRecord) === false) {
        $aRecords[$iKey]['disabled'] = array('delete');
      }
      unset($aRecords[$iKey]['erp_export']);
    }
    return $aRecords;
  }
  
  /**
   * 
   * @param array $aPayment
   * @return array
   */
  private function checkPaymentDisableDelete($aPayment) {
    if ($aPayment['ammount'] <= 0.00) {
      return false;
    }
    if ($aPayment['erp_export'] != '') {
      return false;
    }
    if ($aPayment['statement_id'] <= 0) {
      return false;
    }
  }
	
	/**
	 * Metoda pobiera opcje dla metody transportu
	 *
	 * @global array $aConfig
	 * @param integer $iTransportOptionId
	 * @return string
	 */
	function getTransportOptionLabel($iTransportOptionId) {
		global $aConfig;
		
		$sSql = "SELECT CONCAT(symbol, ' - ', value, ' cm') FROM ".$aConfig['tabls']['prefix']."orders_transport_options
						 WHERE id=".$iTransportOptionId;
		return Common::GetOne($sSql);
	}// end of getTransportOptionLabel() method
  

	/**
	 * Pobierz opcje metody transportu
	 *
	 * @global array $aConfig
	 * @param type $iTransportMethod
	 * @return type 
	 */
	private function GetTransportOptions($iTransportMethod) {
		global $aConfig;
		
		$sSql = "SELECT CONCAT(symbol, ' - ', value, ' cm') AS label, id AS value 
						 FROM ".$aConfig['tabls']['prefix']."orders_transport_options
						 WHERE transport_id=".$iTransportMethod;
		$aRecords=&Common::GetAll($sSql);
		if (!empty($aRecords)) { 
			$aItems = array(
				array('value' => '', 'label' => $aConfig['lang']['common']['choose'])
			);
			return array_merge($aItems,$aRecords);
		}
		return false;
	}// end of GetTransportOptions() method
  
  
  /**
   * Tworzy formularz historii zmian statusów i innych szczegółów na temat zamówienia
   * 
   * @param int $iId
   * @param int $iPId
   * @param array $aOrder
   * @return FormTable
   */
  private function _getOrderStatusChangesHistoryForm($iId, $iPId, $aOrder) {

    $pForm = new FormTable('order', '', array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>155), $this->aConfig['common']['js_validation'], array(), array('class' => 'bigger'));
    $pForm->AddHidden('do', 'NULL',array('id'=>'__order_do'));
    
    $pForm->AddMergedRow($this->aLang['order_status_history'], array('class'=>'merged', 'style'=>'padding-left: 10px'));

    if(!empty($aOrder['invoice_id'])){
      $pForm->AddRow($this->aLang['invoice_id'], $aOrder['invoice_id']);
    }
    if('' != $aOrder['transport_number'] || $_SESSION['user']['priv_order_status'] == '1' || $_SESSION['user']['type'] == '1') {
      //edycja transport number
      $pForm->AddRow($this->aLang['transport_number'], '
      <div id="edit_transport_number">'.$aOrder['transport_number'].'&nbsp;'.
      //$pForm->GetInputButtonHTML('edit_transport_number_show', $this->aLang['edit_transport_remarks'], array('onclick'=>'show_transport_number();'),'button').
      '</div>
      <div id="transport_numberForm" style="display:none;"><form name="" action="" method="post" id="form_update_transport_number"><input name="do" value="update_transport_number" type="hidden"/>'.$pForm->GetTextHTML('transport_number', $this->aLang['transport_number'], $aOrder['transport_number'], array('maxlength' => 128,'style'=>'width:375px;'), '', '', false).''.
      $pForm->GetInputButtonHTML('transport_number_send', $this->aLang['transport_remarks_send'], array('onclick'=>'submit();'),'button').'</form>'.
      $pForm->GetInputButtonHTML('transport_number_hide', $this->aLang['transport_remarks_hide'], array('onclick'=>'hide_transport_number();'),'button').'</div>
      ');
    }
    $this->ordersPacking = new \LIB\EntityManager\Entites\OrdersPacking($this->pDbMgr);
    $orderPacking = $this->ordersPacking->getOrderPacking($iId);
      if (!empty($orderPacking)) {
          $pForm->AddRow(_('List przewozowy został potwierdzony: '), formatDateTimeClient($orderPacking['created']).' przez '.$orderPacking['created_by']);
      }

    if(!empty($aOrder['transport_number'])){
      $pForm = $this->getShipmentStickers($pForm, $aOrder, $iId);
    }

    if(!empty($aOrder['activation_date'])){
      $pForm->AddRow($this->aLang['activation_date'], sprintf($this->aLang['status_update'], $aOrder['activation_date'], $aOrder['activation_by']));
    }
    if(!empty($aOrder['order_date'])){
      $pForm->AddRow(_('Złożone'), sprintf($this->aLang['status_update'],$aOrder['order_date'], ''));
    }
    if(!empty($aOrder['status_1_update'])){
      $pForm->AddRow($this->aLang['status_1_update'], sprintf($this->aLang['status_update'],$aOrder['status_1_update'],$aOrder['status_1_update_by']));
    }
    if(!empty($aOrder['status_2_update'])){ 
      $pForm->AddRow($this->aLang['status_2_update'], sprintf($this->aLang['status_update'],$aOrder['status_2_update'],$aOrder['status_2_update_by']));
    }
    if(!empty($aOrder['status_3_update'])){ 
      $pForm->AddRow($this->aLang['status_3_update'], sprintf($this->aLang['status_update'],$aOrder['status_3_update'],$aOrder['status_3_update_by']));
    }
    if(!empty($aOrder['status_4_update'])){ 
      $pForm->AddRow($this->aLang['status_4_update'], sprintf($this->aLang['status_update'],$aOrder['status_4_update'],$aOrder['status_4_update_by']));
    }
    if(!empty($aOrder['status_5_update'])){ 
      $pForm->AddRow($this->aLang['status_5_update'], sprintf($this->aLang['status_update'],$aOrder['status_5_update'],$aOrder['status_5_update_by']));
    }
    if($aOrder['internal_status'] > 5){ 
      $aUserStatus5 = $this->_getOrdersToUsers($iId, '5');
      if (!empty($aUserStatus5)) {
        $pForm->AddRow($this->aLang['status_int_5_update'], sprintf($this->aLang['status_update'],$aUserStatus5['created'], $aUserStatus5['name'].' '.$aUserStatus5['surname']));
      }
    }
    
    $pForm->AddMergedRow(_('Pakowanie'), array('class'=>'merged', 'style'=>'padding-left: 10px'));
    $aOrderPackingInfo = $this->getOrderPackingInfo($iId);
    if (!empty($aOrderPackingInfo)) {
      foreach ($aOrderPackingInfo as $iKey => $aPackingData) {
        
        if ($aPackingData['status'] == '0') {
          $sStatus = _(' Rozpoczęto');
        } elseif ($aPackingData['status'] == '9') {
          $sStatus = _(' Zakończono');
        }
        $pForm->AddRow(_(' '.$sStatus), sprintf(_('  %s przez %s '), $aPackingData['created'], $aPackingData['user']));
      }      
    }

    $pForm->AddMergedRow($this->aLang['order_status'], array('class'=>'merged', 'style'=>'padding-left: 10px'));
    //$pForm->AddMergedRow($this->GetOrderStatusForm($pForm, $aOrder));
    $pForm->AddRow($this->aLang['order_status'], $this->aLang['status_'.$aOrder['order_status']]);
    $pForm->AddRow($this->aLang['payment_status'], $this->aLang['payment_status_'.$aOrder['payment_status']]);

    if($aOrder['payment_type'] == 'platnoscipl' || $aOrder['payment_type'] == 'payu_bank_transfer' || $aOrder['payment_type'] == 'card'){
      if($aOrder['payment_status'] == '1'){
        $iTransactionNr = $this->getPlatnosciplTransactionNr($iId);
        $pForm->AddRow($this->aLang['payment_status'], $this->aLang['platnosci_booked'].' '.$aOrder['payment_date'].(!empty($iTransactionNr)?' '.$this->aLang['platnosci_transaction'].' '.$iTransactionNr:'').(($aOrder['paid_amount'] < $aOrder['to_pay'])?' <span style="color: red">'.$this->aLang['underpayment'].'</span>':''));
      } 
    } elseif($aOrder['payment_type'] == 'bank_transfer' && $aOrder['payment_status'] == '1') {
      $pForm->AddRow($this->aLang['payment_status'], $this->aLang['booked_payment'].' '.$aOrder['payment_date'].(($aOrder['paid_amount'] < $aOrder['to_pay'])?' <span style="color: red">'.$this->aLang['underpayment'].' '.( $aOrder['to_pay']-$aOrder['paid_amount']).' '.$this->aConfig['lang']['common']['currency'].'</span>':''));
    }
    $pForm->AddCheckBox('opineo_dont_send',$this->aLang['opineo_dont_send'],array('onclick' => 'MenuNavigate(\''.phpSelf(array('do'=>'change_opineo', 'id' => $iId, 'ppid'=>$iPId)).'\');'),'',$aOrder['opineo_dont_send'],false);
    
    $pForm->AddCheckBox('dont_send_info_not_send_on_time', _('Nie wysyłaj informacji o opóźnieniu wysyłki'), array('onclick' => 'MenuNavigate(\''.phpSelf(array('do'=>'dont_send_info_not_send_on_time', 'id' => $iId, 'ppid'=>$iPId)).'\');') , '', $aOrder['dont_send_info_not_send_on_time'], false);
    
    $pForm->AddCheckBox('dont_cancel', _('Wyłącz auto-anulowanie'),array('onclick' => 'MenuNavigate(\''.phpSelf(array('do'=>'dont_cancel', 'id' => $iId, 'ppid'=>$iPId)).'\');'), '', $aOrder['dont_cancel'] == '1' ? true : false, false);
    
    $pForm->AddCheckBox('magazine_get_ready_list_VIP', _('VIP - Priorytet przy zbieraniu na magazynie'),array('onclick' => 'MenuNavigate(\''.phpSelf(array('do'=>'magazine_get_ready_list_VIP', 'id' => $iId, 'ppid'=>$iPId)).'\');'), '', $aOrder['magazine_get_ready_list_VIP'] == '1' ? true : false, false);
    //$pForm->AddEditableText('invoice_remarks', _('Uwagi do faktury'), 'invoice_remarks', '__order_do', $aOrder['invoice_remarks'], array('maxlength' => '512', 'style' =>'width: 512px;'));
    return $pForm;
  }// end of _getOrderStatusChangesHistoryForm() method
  
  /**
   * 
   * @param int $iOrderId
   * @return array
   */
  private function getOrderPackingInfo($iOrderId) {

    $sSql = 'SELECT OPT.*, CONCAT(U.name, " ", U.surname, " (", login,")") AS user
             FROM orders_packing_times AS OPT
             JOIN users AS U
              ON U.id = OPT.user_id
             WHERE order_id = '.$iOrderId;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return id
   */
	private function getPlatnosciplTransactionNr($iOrderId){
    
		$sSql = "SELECT transaction_id
							FROM orders_platnoscipl_sessions
							WHERE order_id = ".$iOrderId.
							" LIMIT 1";
		return $this->pDbMgr->GetOne('profit24', $sSql);
	}
  
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param FormTable $pForm
   * @param array $aOrder
   * @param int $iId
   * @return FormTable
   */
  private function getShipmentStickers($pForm, $aOrder, $iId) {
    global $pDbMgr, $aConfig;
    
    $oShipment = new Shipment($aOrder['transport_id'], $pDbMgr);
    
    $pForm->AddRow(_('Sprawdź status paczki'), 
                      '<a href="'.phpSelf(array('do' => 'get_package_status', 'id' => $iId)).
                      '" title="'._('Sprawdź status paczki').'"><img src="gfx/icons/package_closed_ico.gif" alt="'._('Sprawdź status paczki').'" /></a>');

    $sFilename = $oShipment->getTransportListFilename($aConfig, $aOrder['transport_number']);

    if (file_exists($sFilename)) {
      $pForm->AddRow($aConfig['lang'][$this->sModule]['get_sticker'], 
                      '<a href="'.phpSelf(array('do' => 'get_sticker', 'id' => $iId)).
                      '" title="'.$aConfig['lang'][$this->sModule]['get_sticker'].'"><img src="gfx/download-barcode.gif" alt="'.$aConfig['lang'][$this->sModule]['get_sticker'].'" /></a>');
    }
    $sFilenameConfirm = $oShipment->getOrderDeliverFilePath($aOrder['id']);
    if (file_exists($sFilenameConfirm)) {
      $pForm->AddRow($aConfig['lang'][$this->sModule]['get_confirm_sticker'], 
                      '<a href="'.phpSelf(array('do' => 'get_sticker_confirm', 'id' => $iId)).
                      '" title="'.$aConfig['lang'][$this->sModule]['get_confirm_sticker'].'"><img src="gfx/download-barcode.gif" alt="'.$aConfig['lang'][$this->sModule]['get_sticker'].'" /></a>');
    }
        
    return $pForm;
  }
  
  
  /**
   * Metoda dodaje pola do formularza o zamówieniach które należy razem wysłać
   * 
   * @global integer $pDbMgr
   * @param FormTable $pForm
   * @param int $iPId
   * @param int $iOrderId
   */
  private function addLinkedTransportInfo(&$pForm, $iPId, $iOrderId) {
    global $pDbMgr;
    
    $sSql = "SELECT * 
             FROM orders_linked_transport
             WHERE main_order_id = ".$iOrderId;
    $aOLTtems = $pDbMgr->GetAll('profit24', $sSql);
    if (!empty($aOLTtems)) {
      $sStrHTML = '';
      foreach ($aOLTtems as $aItem) {
        $aOrderData = $this->_getOrderData($aItem['linked_order_id']);
        $sStrHTML .= $aOrderData['order_number'].' <a href="'.phpSelf(array('do' => 'del_relation', 'ppid' => $iPId, 'id' => $iOrderId, 'm_id' => $aItem['main_order_id'], 's_id' => $aItem['linked_order_id'])).'">Usuń</a> <br />';
      }
      $pForm->AddRow("Zamówienia które zostaną wysłane razem: ", $sStrHTML);
    }
    return $pForm;
  }
  
  /**
   * Metoda wyświetla formularz z dodatkowymi informacjami odnośnie zamówienia
   * 
   * @param int $iId
   * @param int $iPId
   * @param array $aOrder
   * @param array $aOrdersAlertsList
   * @param array $aOrdersMailsList
   * @return FormTable
   */
  private function _getOrderAdditionalInfoForm($iId, $iPId, $aOrder) {
    $aOrdersAlertsList = $this->oZamAlert->getAlertsUserOrderLists($iId, intval($this->iUId));
    $aOrdersMailsList = $this->oOrderEmails->getMailsData($iId, array('title', 'content', 'attachments', 'created', 'created_by'));
    
    $sHeader = sprintf($this->aLang['order_details_header'], $aOrder['order_number'], $aOrder['id']);
    $pForm5 = new FormTable('order_additional_info', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>155), $this->aConfig['common']['js_validation'], array(), array('class' => 'bigger'));

    $this->_getAlertsForm($pForm5, $aOrdersAlertsList);

    $this->_getMailsForm($pForm5, $this->oOrderEmails->sMailsPath, $aOrdersMailsList, $iId);


    $pForm5->sTableAddHeader = ' <a style="float:right" title="'.$this->aLang['alert'].'" href="'.phpSelf(array('action' => 'alert', 'pid' => $iId, 'ppid' => $iPId)).'"><img src="gfx/icons/alert_ico.gif" /></a>';
    $pForm5->sTableAddHeader .= ' &nbsp;  <a style="float:right" title="'._('Zmiany').'" href="'.phpSelf(array('do' => 'changes', 'id' => $iId)).'"><img src="gfx/icons/changes_ico.gif" /></a>';
    $pForm5->AddHidden('do', 'add_additional_info',array('id'=>'__order_do2'));

    $pForm5->AddMergedRow($this->aLang['additional_info'], array('class'=>'merged', 'style'=>'padding-left: 10px'));

    $aInfoMessages = $this->_getAdditionalInfoOrder($iId);

    foreach($aInfoMessages as $aMessage) {
      $aMessage['created']=explode(' ',$aMessage['created']);
      $pForm5->AddRow(formatDateClient($aMessage['created'][0]).' '.substr($aMessage['created'][1],0,5), '<strong>'.$aMessage['created_by'].':</strong> <span style="font-size:14px; color:#f20000; font-weight:bold">'.$aMessage['value'].'</span>');
    }

    $pForm5->AddRow('&nbsp;', '<div id="additional_info_norm">'.
    //$pForm5->GetInputButtonHTML('edit_additional_info_show', $this->aLang['edit_additional_info'], array('onclick'=>'show_additional_info();'),'button').
    '</div><div id="additional_info_Form" style="display:none;">'.$pForm5->GetTextHTML('additional_info_input', $this->aLang['edit_additional_info'], '', array('maxlength' => 250,'style'=>'width:250px;'), '', 'text', false).
    $pForm5->GetInputButtonHTML('additional_info_send', $this->aLang['edit_additional_info'], array('onclick'=>'send_additional_info();'),'button').
    $pForm5->GetInputButtonHTML('additional_info_hide', $this->aLang['close_additional_info'], array('onclick'=>'hide_additional_info();'),'button')."</div><br />".
    $pForm5->GetInputButtonHTML('send_merlin_order', 'Wyślij ponownie zamówienie do merlina razem z płatnością', array('onclick'=>'send_merlin_order_with_payment('.$iId.');'),'button').'</div>');
    return $pForm5;
  }// end of _getOrderAdditionalInfoForm() method
  
  
  /**
   * Metoda zwraca kod Javascript, pomocniczy w generowaniu formularza szczegółów zamówienia
   * 
   * @param int $iId
   * @param int $iPId
   * @param int $iInternalStatus
   * @param int $iTPaczkomatyId
   * @param string $sBookStore
   * @param bool $bBlockComments
   * @param bool $bIsOrderLocked
   * @return string
   */
  private function _getJS($iId, $iPId, $iInternalStatus, $iTPaczkomatyId, $sBookStore, $bBlockComments, $bIsOrderLocked) {
    
      if ($iInternalStatus >= 4) {
        $sJSHideOrderDetails = '
      $("#order_items").ready(function() {
        $("#order_items").hide();
        hideOrders = true;
        $("#toggleOrdersDetails").html("Rozwiń <img src=\'gfx/collapsed.gif\' />");
      });';
      }
      
			$sJS = '
        <script type="text/javascript" src="js/preventMultiTabs.js"></script>
		<script type="text/javascript">
	// <![CDATA[
      '.$sJSHideOrderDetails.'


  
      var hideOrders = false;
      function toggleOrdersDetails() {
        if (hideOrders == true) {
          $("#toggleOrdersDetails").html("Zwiń <img src=\'gfx/expanded.gif\' />");
          hideOrders = false;
        } else {
          $("#toggleOrdersDetails").html("Rozwiń <img src=\'gfx/collapsed.gif\' />");
          hideOrders = true;
        }
        $("#order_items").toggle();
      }
      
      function toggleAllRows(object) {
          var href = $(object);
          $(".openRow").parent().parent().toggle();
          $(".closedRow").parent().parent().toggle();
          
          if ($(".openRow").parent().parent().css("display") == "none") {
              href.html("Rozwiń wszystkie <img src=\"gfx/collapsed.gif\">");
          } else {
              href.html("Zwiń wszystkie <img src=\"gfx/expanded.gif\">");
          }
      }
      
      function toggleRow(object, isOpen) {
        var isHref = (typeof isHref !== \'undefined\') ? isHref : false;
        var button = $(object);
        if (isOpen) {           
            var minRow = $(object).parent().parent().prev();
        } else {
            var minRow = $(object).parent().parent();
        }
        var row = minRow.next();
        
        row.toggle();
        minRow.toggle();
      }

			function send_connect_to(){
				$("#order_user_data_do").val("connect_to");
				$("#order_user_data").submit();
			}
			function save_and_goback(){
				$("#order_items_form_do").val("save_items");
				$("#order_items").submit();
			}
			function show_user_details(){
				$(".userDetailsForm").show();
				$(".userDetailsFormButtonShow").hide();
				/*$(".userDetailsFormButtonHide").show();*/
			}
			function hide_user_details(){
				$(".userDetailsForm").hide();
				/*$(".userDetailsFormButtonHide").hide();*/
				$(".userDetailsFormButtonShow").show();
			}
			function send_merlin_order_with_payment(id){
                $.ajax({
                  type: "GET",
                  cache: false,
                  async: false,
                  url: "/LIB/communicator/sources/Merlin/testMerlin.php?order_id="+id
                }).done(function( data ) {
                    alert("Zamówienie zostało wysłane do Merlina");
                    console.log(data);
                });
			}
			function show_transport_remarks(){
				$("#transportForm").show();
				$("#edit_transport_norm").hide();
			}
			function show_additional_info(){
				$("#additional_info_Form").show();
				$("#additional_info_norm").hide();
			}
			function hide_additional_info(){
				$("#additional_info_norm").show();
				$("#additional_info_Form").hide();
			}
			function send_additional_info(){
				$("#order_user_data_do2").val("add_additional_info");
				$("#order_additional_info").submit();
			}
      
			function show_user_comments(){
				$("#user_comments_Form").show();
				$("#user_comments_norm").hide();
			}
			function hide_user_comments(){
				$("#user_comments_norm").show();
				$("#user_comments_Form").hide();
			}
			function send_user_comments(){
				$("#order_user_data_do3").val("add_user_comments");
				$("#users_comments").submit();
			}
      
			function show_send_date(){
				$("#send_dateForm").show();
				$("#edit_send_date").hide();
			}
			function send_send_date(){
				$("#order_user_data_do").val("update_send_date");
				$("#order_user_data").submit();
			}
			function hide_send_date(){
				$("#send_dateForm").hide();
				$("#edit_send_date").show();
			}
			function show_transport_number(){
				$("#transport_numberForm").show();
				$("#edit_transport_number").hide();
			}
			function send_transport_number(){
				//$("#order_user_data_do").val("update_transport_number");
				$("#form_update_transport_number").submit();
			}
			function hide_transport_number(){
				$("#transport_numberForm").hide();
				$("#edit_transport_number").show();
			}
			function send_transport_remarks(){
				$("#order_user_data_do").val("update_transport_remarks");
				$("#order_user_data").submit();
			}
			function hide_transport_remarks(){
				$("#transportForm").hide();
				$("#edit_transport_norm").show();
			}
			function toggleAddresType(iAddrNr,iType){
			if(iType == 1){
				/*  $("#addr"+iAddrNr+"_grp .corp").removeAttr("disabled"); */
				$("#company_"+iAddrNr+"_label.gwiazdka").css("display","inline");
			} else {
				/* $("#addr"+iAddrNr+"_grp .corp").attr("disabled","disabled"); */
				$("#company_"+iAddrNr+"_label.gwiazdka").css("display","none");
			}
		}

		    function changePrintFv(value)
		    {
		        var id = '.$iId.';
		        var ppid = '.$iPId.';

                var address = "admin.php?frame=main&module_id=38&module=m_zamowienia&do=printFV&id="+id+"&ppid="+ppid;
                address += "&force_print_fv="+value;

                $.get( address, function( data ) {
                      $("#evt_order_details2").prepend("<div style=float:right;color:red><b>UWAGA, FAKTURA ZOSTANIE WYDRUKOWANA<b/></div>");
//                    $( ".result" ).html( data );
                });
		    }
//

            function changePrintValue() {
			    
			        var transportValId = $("#transport_mean").val();
                    var paymentValId = $("#payment_method").val();
                    var cash = $("#point_of_receipt option:selected").attr("data-cashondelivery");//, "cashondelivery"
                    console.log(cash);
                    if (transportValId == 5 && paymentValId == 33 && cash == "false") {
                        alert("Wybrany pkt odbioru nie obsługuje pobrań");
                        $("#payment_method").val("");
                        $("#payment_method").change();
                    }
                    
            setTimeout(function (){
                    var transportVal = $("#transport_mean").val();
                    var paymentVal = $("#payment_method option:selected").html();
                    var checkbox = $("#print_fv");
                    console.log(paymentVal)
                    if (transportVal == 2 || paymentVal == "przelew 14 dni") {

                        changePrintFv(1);
                        checkbox.prop("checked", true);
                    }
            }, 1000);

            }

			$(document).ready(function(){
//
                $("#transport_mean").on("change", function(e){
                        return changePrintValue();
                });

                $("#payment_method").on("change", function(e){
                        return changePrintValue();
                });
                
                $("#point_of_receipt").on("change", function(e){
                        return changePrintValue();
                });

                $("#print_fv").on("click", function(){
                    var value = $(this).is(":checked");

                    var svalue = 0;

                    if (value == true) {
                        svalue = 1;
                    }

                    var url = "'.phpSelf(array("do"=>"printFV", "id" => $iId, "ppid"=>$iPId)).'";

                    return MenuNavigate(url+"&force_print_fv="+svalue);
                });

				$("#add_to_order").click(function() {
				window.open("ajax/SearchProduct.php?ppid='.$iPId.'&pid='.$iId.'&site='.$sBookStore.'","","menubar=0,location=0,scrollbars=1,resizable=1,toolbar=0,width=700,height=500");
				});
			});
			
			function checkCheckboxPrintTransport() {
				if ($("#remarks_read").length > 0) {
					// checkbox istnieje
					if ($("#remarks_read").attr("checked") == "checked") {
						//return true;
					} else {
						alert("'.$this->aLang['err_remarks_read'].'");
						return false;
					}
				}
				
				if ( $("#transport_option_id").size()) {
					if ($("#transport_option_id").html() != "" && $("#transport_option_id option:selected").val() == "") {
						alert("Wybierz typ przesyłki");
						return false;
					} else {
						if ($("#transport_option_id option:selected").val() != null && $("#transport_option_id option:selected").val() != undefined) {
							return "&transport_option_id="+$("#transport_option_id option:selected").val();
						} else {
							return true;
						}
					}
				} else if ( $("#transport_packages").size() ) {
          var transport_packages = parseInt($("#transport_packages").val());
          if (transport_packages > 0) {
            return "&transport_packages=" + transport_packages;
          } else {
            alert("Wybierz ilość paczek");
						return false;
          }
        
        } else {
					return true;
				}
				return true;
			}
      
      function doBlock(bWithComment) {
        if (bWithComment === true) {
          var oToBlock = $("input[name!=\'cancel\']");
          var oHidden = $("input:hidden");
        } else {
          var oToBlock = $("form[name!=\'users_comments\'][name!=\'order_additional_info\'] input[name!=\'cancel\']");
          var oHidden = $("form[name!=\'users_comments\'][name!=\'order_additional_info\'] input:hidden");
        }
        oToBlock.attr("disabled", "disabled");
        oToBlock.attr("style", "color: grey; font-weight: normal;");
        $("a[onclick!=\'\']").attr("onclick", "");
        oHidden.remove();
        $(".editable_cell").removeAttr("class");
      }
      ';
      
      if ($bIsOrderLocked === true) {
        $sJS .= '
          $(document).ready(function(){
            doBlock('.($bBlockComments == true ? 'true' : 'false').');
          });
          ';
      } else {
        $sJS .= '
        
      $(document).ready(function(){
       $("#order_items").submit(  function() {
        if ($("#remarks_read_1").val() !== undefined) {
          if ($("#remarks_read_1").val() == "") {
            hideOverlay = true;
            alert("Przeczytaj uwagi do zamówienia");
            return false;
          }
        }
          $("#order_items").css("visibility", "hidden");
          $(":button").remove();
       });
       
        
        setInterval(function() {
            $.ajax({
              type: "GET",
              cache: false,
              async: false,
              url: "ajax/lockOrder.php?order_id='.$iId.'&time="+parseInt(new Date().getTime().toString().substring(0, 10))
            }).done(function( data ) {
              if (data == "-1") {
                doBlock();
              } else if (data == "1") {
                // ok
              } else {
                alert("Zostałeś wylogowany, zaloguj się ponownie !");
              }
            });
         }, 90000);
         
        });';
      }      
      $sJS .= '

			// ]]>
</script>
			';
      return $sJS;
  }// end of _getJS() method
  
  
  /**
	 * Metoda sprawdza czy istnieje potwierdzenie paczkomaty dla tego zamowienia
	 *
	 * @param int $iId id zamówienia
	 */
	private function _getPaczkomatyConfirmFilename($iId) {
		
		$sSql = "SELECT filename 
             FROM orders_paczkomaty_confirm 
						 WHERE order_id=".$iId;
		return $this->pDbMgr->GetOne('profit24', $sSql);
	} // end of getPaczkomatyConfirmFilename() method
  
  
  /**
   * Metoda pobiera prostą listę komentarzy
   * 
   * @param type $iUId
   * @return type
   */
  private function _getSimpleCommentsList($iUId) {
    
    $sSql = 'SELECT comment, created, created_by
             FROM users_accounts_comments
             WHERE user_id = '.$iUId."
             ORDER BY id DESC";
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of _getSimpleCommentsList() method
  
  
  /**
   * Metoda tworzy komentarz użytkownika
   * 
   * @param int $iUId
   * @param int $iId
   * @return FormTable
   */
  private function _getUserCommentForm($iUId, $iId) {
    
    include_once('Form/FormTable.class.php');
    
    $aComments = $this->_getSimpleCommentsList($iUId);
    $pForm = new FormTable('users_comments', '', array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>155), $this->aConfig['common']['js_validation'], array('style' => 'font-weight: normal;'), array('style' => ' font-weight: normal;', 'class' => 'bigger'));
    $pForm->AddHidden('do', 'users_comments', array('id'=>'order_user_data_do3'));
    $pForm->AddMergedRow($this->aLang['user_comments'].' '.$this->_getUserEmail($iUId), array('class'=>'merged', 'style'=>'padding-left: 10px'));
    foreach ($aComments as $aComment) {
      $pForm->addRow($aComment['created'].'<br />'.$aComment['created_by'], nl2br($aComment['comment']), false);
    }
    
    
    $pForm->AddRow('&nbsp;', '<div id="user_comments_norm">'.
    $pForm->GetInputButtonHTML('add_user_comments', $this->aLang['add_user_comments'], array('onclick'=>'show_user_comments();'),'button').
    '</div><div id="user_comments_Form" style="display:none;">'.$pForm->GetTextAreaHTML('user_comments_input', $this->aLang['add_user_comments'], '', array('maxlength' => 250,'style'=>'width:450px; height: 40px;'), '', 'text', false).
    $pForm->GetInputButtonHTML('user_comments_send', $this->aLang['add_user_comments'], array('onclick'=>'send_user_comments();'),'button').
    $pForm->GetInputButtonHTML('user_comments_hide', $this->aLang['user_comments_hide'], array('onclick'=>'hide_user_comments();'),'button').'</div>');

    return $pForm;
  }// end of getUserCommentForm() method
  
  
	/**
	 * Metoda pobiera adres email uzytkownika o podanym Id
	 * 
	 * @param	integer	$iId	- Id uzytkownika
	 * @return	string
	 */
	private function _getUserEmail($iId) {

		$sSql = "SELECT email
						 FROM users_accounts
						 WHERE id = ".$iId;
		return $this->pDbMgr->GetOne('profit24', $sSql);
	} // end of getUserEmail() method
  
  
	/**
	 * Metoda pobiera informacje dodatkowe o zamówieniu
	 *
	 * @param type $iId
	 * @return type 
	 */
	private function _getAdditionalInfoOrder($iId) {
		
		$sSql = "SELECT *
             FROM orders_additional_info
						 WHERE order_id = ".$iId.' 
             ORDER BY created DESC;';
		return $this->pDbMgr->GetAll('profit24', $sSql);
	}// end of getAdditionalInfoOrder() method
  
  
	/**
	 * Metoda czyści ze spacji tablicę
	 *
	 * @author akarmenia at gmail dot com http://www.php.net/manual/en/function.trim.php#103278
	 * @param array $array
	 * @return array
	 */
	private function _trim_r($array) {
    if (is_string($array)) {
        return trim($array);
    } else if (!is_array($array)) {
        return '';
    }
    $keys = array_keys($array);
    for ($i=0; $i<count($keys); $i++) {
        $key = $keys[$i];
        if ( is_array($array[$key]) ) {
            $array[$key] = $this->_trim_r($array[$key]);
        } else if ( is_string($array[$key]) ) {
            $array[$key] = trim($array[$key]);
        }
    }
    return $array;
	} // end of _trim_r() method
  
  
	/**
	 * Metoda pobiera ilość do tej pory zrealizowanych zamówień przez użytkownika
	 *
	 * @param int $iOId
	 * @param int $iUId
	 * @param int $iWebsiteId
	 * @return int
	 */
	private function _getCountPrevOrders($iUId, $iWebsiteId) {

		$sSql = "SELECT count(id) 
             FROM orders
						 WHERE order_status = '4' AND 
									 user_id = ".intval($iUId)." AND 
									 website_id = ".intval($iWebsiteId);
		return $this->pDbMgr->GetOne('profit24', $sSql);
	}// end of _getCountPrevOrders() method
  
	/**
	 * Metoda sprawdza czy istnieje korekta dla innego zamówienia tego samego użytkownika
	 *
	 * @param integer $iOrderId
	 * @param integer $iUserId 
	 * @return bool
	 */
	private function _checkCorrectionOther($iOrderId, $iUserId) {
		
		$sSql = "SELECT id 
             FROM orders
						 WHERE 
                id <> ".intval($iOrderId)." AND 
                user_id = ".intval($iUserId)." AND correction='1' ";
		return ($this->pDbMgr->GetOne('profit24', $sSql) > 0);
	}// end of _checkCorrectionOther() method
  
  
  /**
   * Metoda przelicza sumę wag składowych zamówienia
   * 
   * @param int $iOId
   * @return int
   */
  private function _getTotalOrderWeight($iOId) {
    
    $sSql = 'SELECT SUM(OI.weight * OI.quantity)
             FROM orders_items AS OI
             WHERE 
              OI.deleted = "0" AND 
              (
                (OI.item_type = "I" AND OI.packet = "0") 
                  OR 
                 OI.item_type = "P"
               ) AND
              OI.order_id = '.$iOId;
    return str_replace('.', ',', $this->pDbMgr->GetOne('profit24', $sSql));
  }// end of _getTotalOrderWeight() method
  
  
  /**
   * Metoda generuje formularz alrtów
   * 
   * @param object $pForm
   * @param array $aOrdersAlertsList
   */
  private function _getAlertsForm(&$pForm, $aOrdersAlertsList) {
    
    if (!empty($aOrdersAlertsList)) {
      $pForm->AddMergedRow(_('Alerty zamówienia'), array('class'=>'merged', 'style'=>'padding-left: 10px'));
      foreach ($aOrdersAlertsList as $aAlertsList) {
        $aAlertsList['created'] = explode(' ',$aAlertsList['created']);
        $pForm->addRow(
                formatDateClient($aAlertsList['created'][0]).' '.substr($aAlertsList['created'][1],0,5), 
                '
                  <strong>'.$aAlertsList['created_by'].' -> '.($aAlertsList['user_id'] > 0 ? $this->_getAdminUser($aAlertsList['user_id']): _('Wszyscy użytkownicy')).
                ':</strong> <span style="font-size:14px; color:green; font-weight:bold">'.$aAlertsList['message'].'</span>'
                .($aAlertsList['disabled'] == '1' ? '' : ' &nbsp; <a title="Wyłącz Alert" href="'.phpSelf(array('action_goback' => '', 'action' => 'alert', 'do' => 'deactivate', 'pid' => $aAlertsList['id'])).'"><img src="gfx/icons/activate_ico.gif" alt="Wyłącz Alert" border="0" /></a>')
                );
      }
    }
  }// end of _getAlertsForm() method
  
  
  /**
   * Metoda generuje formularz alrtów
   * 
   * @param object $pForm
   * @param array $aOrdersMailsList
   */
  private function _getMailsForm(&$pForm, $sMailPath, $aOrdersMailsList, $iOId) {
    
    if (!empty($aOrdersMailsList)) {
      $pForm->AddMergedRow(_('Emaile zamówienia'), array('class'=>'merged', 'style'=>'padding-left: 10px'));
      foreach ($aOrdersMailsList as $aMailsList) {
        $aAttFilePaths = unserialize($aMailsList['attachments']);
        $sAttLinksHTML = '';
        if (!empty($aAttFilePaths) && is_array($aAttFilePaths)) {
          foreach ($aAttFilePaths as $sAttFilePath) {
            
            if (file_exists($sAttFilePath)) {
              $aMatched = array();
              preg_match('/([^\/]+)$/', $sAttFilePath, $aMatched);
              $sFileName = trim($aMatched[1]);
              $sAllFileUrl = $this->aConfig['common']['client_base_url_http'].$sMailPath.'/'.$sFileName;
              
              $sAttLinksHTML .= '<a target="_blank" href="'.$sAllFileUrl.'">'.$sFileName.'</a> <br />';
            }
          }
        }
        
        $aMailsList['created'] = explode(' ',$aMailsList['created']);
        $pForm->addRow(
                formatDateClient($aMailsList['created'][0]).' '.substr($aMailsList['created'][1],0,5).'<br />'.$aMailsList['created_by']
                .' '.$sAttLinksHTML
                , 
                '<hr />'.
                ''.$aMailsList['title'].'<hr />'.
                '<span>'.nl2br($aMailsList['content']).'</span>');
      }
      $pForm->AddInputButton('send_mail', $this->aLang['send_info_mail'], array('onclick'=>'document.location.href=\''.phpSelf(array('action' => 'mail', 'do'=>'add', 'pid'=>$iOId)).'\';'),'button');
    }
  }// end of _getMailsForm() method
  
  
  /**
   * Metoda pobiera imię oraz nazwisko dla osoby dla zamówienia w statusie
   * 
   * @param int $iOrderId
   * @param int $iStatus
   * @return array
   */
  private function _getOrdersToUsers($iOrderId, $iStatus) {

    $sSql = "SELECT U.name, U.surname, OTU.created
             FROM orders_to_users AS OTU
             JOIN users AS U
              ON OTU.user_id = U.id
             WHERE OTU.order_id = ".$iOrderId."
                   AND OTU.internal_status = '".$iStatus."'";
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of _getOrdersToUsers() method
}// end of Class()
