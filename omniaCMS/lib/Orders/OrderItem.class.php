<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\lib\Orders;

use DatabaseManager;

/**
 * Description of Order
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class OrderItem {

  /**
   *
   * @var int
   */
  private $iOrderItemId;
  
  /**
   *
   * @var int
   */
  private $iOrderId;
  
  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var Order
   */
  public $oOrder;

  public function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }
  
  
  public function setOrderItemId($iOrderItemId) {
    $this->iOrderItemId = $iOrderItemId;
  }
  
  
  /**
   * 
   * @param int $iOrderId
   */
  public function setOrder($iOrderId) {
    $this->iOrderId = $iOrderId;
    $this->oOrder = new Order($this->pDbMgr, $iOrderId);
  }
  
  /**
   * 
   * @param char $cType - Typy: 
   *  I - pojedyncze egz.
   *  A - załączniki
   *  P - Pakiety
   * 
   */
  public function getSelOrdersItemsByOrderId($cType) {
    
		$sSql = "SELECT *
						 FROM orders_items
						 WHERE order_id = ".$this->iOrderId."
						  AND item_type = '".$cType."'
						 ORDER BY name";
		return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  
  /**
   * 
   * @param char $cType - Typy: 
   *  I - pojedyncze egz.
   *  A - załączniki
   *  P - Pakiety
   * 
   */
  public function getSelOrdersItemsChildsData($cType) {
    
    $sSql = "SELECT  *
             FROM orders_items
             WHERE order_id = ".$this->iOrderId." AND
                   parent_id = ".$this->iOrderItemId." AND
                   item_type='".$cType."'
             ORDER BY id";
    return $this->pDbMgr->GetAll($sSql);
  } 
}