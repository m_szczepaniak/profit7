<?php
/**
 * Klasa do zmiany zamówienia na podstawie przekazanych danych
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-11
 * @copyrights Marcin Chudy - Profit24.pl
 */
use orders\logger;

include_once('OrderItemRecountPost.class.php');
class OrderRecountPost extends OrderItemRecountPost {
  
  /**
   * @var DatabaseManager $pDbMgr
   */
  public $pDbMgr;
  public $bTestMode = TRUE;

  protected $sMessage;
  
  /**
   * @var Lib_FreeTransportPromo $this->oFreeTransportPromo
   */
  protected $oFreeTransportPromo;
  
  /**
   * @var OrderItemRecount $oOrderItemRecount
   */
  protected $oOrderItemRecount;
  
  protected $sWebsite;
  
  protected $bAllNotNull = true; // czy czy wszystkie rozne od '---'
  protected  $bExistNotNull = false; // czy jest pozycja rozna od '---'
  protected  $bAllReady = true; // czy wszystkie "są"
  protected  $bAllReadyNC = true; // czy wszystkie "są niepotwierdzone"
  protected  $bAllWeight = true; // czy wszystkie produkty mają ustawioną wagę
  protected  $bSendMail = false; // wyslij mail o stanie zamowienia
  protected  $bRemarksRead_1;
  
  protected $aPackets = array();
  protected $fTotalValueNetto = 0;
  protected $fTotalValueBrutto = 0;
  protected $aFreeTransport = array();
  

  function __construct($pDbMgr, $bTestMode = TRUE) {
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    $this->sWebsite = 'profit24';// default
    
    include_once("OrderItemRecount.class.php");
    $this->oOrderItemRecount = new OrderItemRecount();
    parent::__construct();
  }
  
  
  /**
   * Metoda pobiera dane płatności
   * 
   * @param string $sTransportNumber
   * @param int $iPaymentId
   * @param int $iOrderPaymentId
   * @param string $sPaymentMethod
   * @return array
   */
  private function _getPayment($sTransportNumber, $iPaymentId, $iOrderPaymentId, $sPaymentMethod) {

    if (!empty($sTransportNumber)) {
      return $this->_getPaymentData($iOrderPaymentId, $this->sWebsite);
    } elseif ($iPaymentId > 0) {
      return $this->_getPaymentData($iPaymentId, $this->sWebsite);
    } else {
      return $this->_getPaymentData(intval($sPaymentMethod), $this->sWebsite);
    }
  }// end of _getPayment() method
  
  
	/**
	 * Metoda pobiera dane metody płatności
	 * @param $iPaymentId - id metody płatnosci
	 * @return array - dane metody platności
	 */
	private function _getPaymentData($iPaymentId, $sWebsite){
    global $aConfig;
    
		$sSql = "SELECT A.id, A.ptype, A.cost, A.no_costs_from, B.id AS transport_id, B.name AS transport, B.symbol
				 		 FROM orders_payment_types A
				 		 JOIN orders_transport_means B
				 		 ON B.id=A.transport_mean
				 		 WHERE A.id = ".$iPaymentId;
		$aPayment =& $this->pDbMgr->GetRow($sWebsite, $sSql);
    
    // jeśli nie załadowany lang
    if (!isset($aConfig['lang']['m_zamowienia']['ptype_'.$aPayment['ptype']])) {
      include_once($_SERVER['DOCUMENT_ROOT'].'omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
    }
		$aPayment['payment_name']=$aConfig['lang']['m_zamowienia']['ptype_'.$aPayment['ptype']];
		return $aPayment;
	} // end of GetPaymentData() method
  
  
  /**
   * Metoda zwraca koszty transportu
   * 
   * @param string $sPaymentSymbol
   * @param float $fPaymentCost
   * @param int $iPaymentId
   * @param string $sTransportNumber
   * @param float $fOrderTransportCost
   * @param bool $bBlockTranpsortCost
   * @param float $fTransportValueBrutto
   * @retun float
   */
  private function _getfTransportCost($sPaymentSymbol, $fPaymentCost, $iPaymentId, $sTransportNumber, $fOrderTransportCost, $bPOSTBlockTranpsortCost, $bBlockTranpsortCost, $fTransportValueBrutto) {
    
    if (!empty($sTransportNumber)) {
      $fTransportCost = $fOrderTransportCost;
    } elseif ($iPaymentId > 0 && $bBlockTranpsortCost == '1') {
      $fTransportCost = $fOrderTransportCost;
    } else {
      // blokowanie kosztów transportu - kozt z pola input
      if (isset($bPOSTBlockTranpsortCost)) {
        $fTransportCost = Common::formatPrice2($fTransportValueBrutto);
      } else {
        // przeliczanie kosztu na podstawie aktualnie wybranej formy płatności
        // zwolnienie z koztu transportu
        if (!empty($this->aFreeTransport) && in_array($sPaymentSymbol, $this->aFreeTransport)) {
          $fTransportCost = 0;
        } else {
          // normalny koszt transportu
          $fTransportCost = $fPaymentCost;
        }
      }
    }
    return $fTransportCost;
  }// end of _getfTransportCost() method
  
  
	/**
	 * Metoda pobiera opcje dla metody transportu
	 *
	 * @global array $aConfig
	 * @param integer $iTransportOptionId
	 * @return string
	 */
	private function _getTransportOptionSymbol($iTransportOptionId) {
		
		$sSql = "SELECT symbol 
             FROM orders_transport_options
						 WHERE id=".$iTransportOptionId;
		return $this->pDbMgr->GetOne('profit24', $sSql);
	}// end of getTransportOptionSymbol() method
  
  
  /**
   * Metoda dodaje specyficzne wartości zamówienia dla metody transportu paczkomaty
   * 
   * @param array $aValues
   * @param int $iTransportOptionId
   * @param string $sPointOfReceipt
   * @return array
   */
  private function _addSpecificSpeditorValues($aValues, $sTransportSymbol, $iTransportOptionId, $sPointOfReceipt) {
    
    if (intval($iTransportOptionId) > 0) {
      $aValues['transport_option_id'] = intval($iTransportOptionId);
      $aValues['transport_option_symbol'] = $this->_getTransportOptionSymbol(intval($iTransportOptionId));
    } else {
      $aValues['transport_option_id'] = 'NULL';
      $aValues['transport_option_symbol'] = 'NULL';
    }
    // punkt odbioru zapisujemy tylko w przypadku kiedy opcja jest wybrana
    $aMatches = array();
    
    if ($sTransportSymbol == 'orlen') {
      preg_match('/^\w{1,6}$/' , $_POST['point_of_receipt'], $aMatches);
    } else {
      preg_match('/^\w{1,10}$/' , $_POST['point_of_receipt'], $aMatches);
    }  
      
    if ($sPointOfReceipt != '' && !empty($aMatches)) {
      $aValues['point_of_receipt'] = $sPointOfReceipt;
    }
    return $aValues;
  }// end of _addSpecificPaczkomatyValues() method
  
	
	/**
	 * Metoda pobiera pakiety zawarte w zamówieniu
   * 
	 * @param int $iOrderId - id zamówienia
	 * @return array - pakiety zawarte w zamówieniu
	 */
	private function _getOrderPackets($iOrderId){
    
		global $aConfig;
			// pobranie zamowionych w ramach zamowienia towarow
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id = ".$iOrderId."
						 AND packet = '1'
						 ORDER BY id";
		return $this->pDbMgr->GetAll('profit24', $sSql);
	}// end of getOrderPackets() method
  

  private function setMessage($sMessage) {
    $this->sMessage .= $sMessage;
  }

  public function getMessage() {
    return $this->sMessage;
  }


  /**
   * Metoda przelicza wartośc zamówienia, koszty transportu po jakichkolwiek zamianach
   *
   * @param int $iId - id zamówienia
   * @param array $aPost - tablica post
   * @param array $aPostItems - elementy składowe zamóweni z tablicy post
   * @param int $iPaymentId - id metody płatności
   * @return bool - success
   */
  public function recountOrderPost($iId, $aPost, $aPostItems, $iPaymentId = 0) {
    
    $this->sWebsite = $this->getWebsiteSymbol($this->getWebsiteId($iId, false));
    include_once('FreeTransportPromo.class.php');
    $this->oFreeTransportPromo = new Lib_FreeTransportPromo($this->sWebsite);
    
    $this->aPackets = array();
    $this->fTotalValueNetto = 0;
    $this->fTotalValueBrutto = 0;
    $this->bAllNotNull = true; // czy czy wszystkie rozne od '---'
    $this->bExistNotNull = false; // czy jest pozycja rozna od '---'
    $this->bAllReady = true; // czy wszystkie "są"
    $this->bAllReadyNC = true; // czy wszystkie "są niepotwierdzone"
    $this->bAllWeight = true; // czy wszystkie produkty mają ustawioną wagę
    $this->bSendMail = false; // wyslij mail o stanie zamowienia
    $this->aFreeTransport = array();
    
    $aOrderItems = $this->_getOrderItems($iId);
    $aOrder = $this->_getOrderData($iId);
    $this->bRemarksRead_1 = $this->_getRemarksFlag($aOrder['remarks'], $aOrder['remarks_read_1'], $aPost['remarks_read_1']);

//      global $pDbMgr;
//      $reservationManager = new ReservationManager($pDbMgr);
//      $beforeReservation = $reservationManager->getDataProvider()->getReservationsByOrderId($iId);

      $sellPredict = new \LIB\EntityManager\Entites\SellPredict($this->pDbMgr);
    foreach ($aOrderItems as $iKey => $aOItem) {
      $this->_proceedItem($aOItem, $aPostItems[$aOItem['id']], $aPostItems[$aOItem['parent_id']], $aPost);

        // tutaj wbijamy się
        try {
            $sellPredict->autoSellPredictOrderItem($aOItem['id']);
        } catch (\Exception $e) {}
    }

    // pakiety w zamówieniu
    $aOrderPackets = $this->_getOrderPackets($iId);
    if (!empty($aOrderPackets)) {
      foreach ($aOrderPackets as $iKey => $aPItem) {
        $this->_proceedItemPacket($aPItem, $aPostItems[$aPItem['id']], $aPost);
      }
    }

    //TODO DOKONCZYC
//    if($aPost['do'] == 'update_items'){
//      $reservationManager->createStreamsoftReservation($beforeReservation, $iId);
//    }

    if ($this->fTotalValueBrutto > 0.00) {
      $aFreeTransportTMP = $this->oFreeTransportPromo->getBookFreeTransport('-1', $this->fTotalValueBrutto);
      if (!empty($aFreeTransportTMP)) {
        $this->aFreeTransport = array_unique(array_merge($this->aFreeTransport, $aFreeTransportTMP));
      }
    }
    
    $aPayment = $this->_getPayment($aOrder['transport_number'], $iPaymentId, $aOrder['payment_id'], $aPost['payment_method']);

    $fTransportCost = $this->_getfTransportCost($aPayment['symbol'], $aPayment['cost'], $iPaymentId, 
                             $aPost['transport_number'], $aOrder['transport_cost'], 
                             $aPost['block_transport_cost'], $aOrder['block_transport_cost'], $aPostItems['transport']['valuebrutto']);

    if (Common::formatPrice2($aOrder['transport_cost']) != Common::formatPrice2($fTransportCost)) {
      $this->setMessage(GetPopupMessage(_('Uwaga zmieniono koszty transportu na '.Common::formatPrice($fTransportCost))));
    }

    $iTransportVat = $this->oOrderItemRecount->getTransportVat($aPayment['id'], $aOrderItems);
    // aktualizacja
    $aValues = array(
        'value_netto' => Common::formatPrice2($this->fTotalValueNetto),
        'value_brutto' => Common::formatPrice2($this->fTotalValueBrutto),
        'transport_cost' => $fTransportCost,
        'transport_vat' => $iTransportVat,
        'payment' => $aPayment['payment_name'],
        'payment_type' => $aPayment['ptype'],
        'payment_id' => $aPayment['id'],
        'transport' => $aPayment['transport'],
        'transport_id' => $aPayment['transport_id'],
        'block_transport_cost' => isset($aPost['block_transport_cost']) ? '1' : '0',
        'is_modified' => '1',
        'modified' => 'NOW()',
        'modified_by' => $_SESSION['user']['name']
    );
    if ($_GET['do'] == 'undelete_item' || $_GET['do'] == 'delete_item') {
      unset($aValues['block_transport_cost']);
      unset($aValues['transport_cost']);
    }

    if ($aPayment['symbol'] == 'paczkomaty_24_7' || $aPayment['symbol'] == 'poczta_polska' || $aPayment['symbol'] == 'ruch' || $aPayment['symbol'] == 'orlen') {
      $aValues = $this->_addSpecificSpeditorValues($aValues, $aPayment['symbol'], $aPost['transport_option_id'], $aPost['point_of_receipt']);
    }

    if (isset($aPost['second_payment_method'])) {
      $aSecondPayment = $this->_getPaymentData(intval($aPost['second_payment_method']), $this->sWebsite);
      $aValues['second_payment'] = $aSecondPayment['payment_name'];
      $aValues['second_payment_type'] = $aSecondPayment['ptype'];
      $aValues['second_payment_id'] = $aSecondPayment['id'];
    }

    if (!empty($aPost['transport_number']) && intval($aOrder['order_status']) == 3) {
      $aValues['transport_number'] = $aPost['transport_number'];
    }
    
    $mStatus = $this->_getOrderValuesStatuses($aOrder['order_status']);
    if (isset($mStatus) && $mStatus > 0) {
      $aValues['order_status'] = $mStatus;
      $aOrder['order_status'] = $mStatus;
      $aValues['status_'.$mStatus.'_update'] = 'NOW()';
      $aValues['status_'.$mStatus.'_update_by'] = $_SESSION['user']['name'];
    }

    if (isset($aPost['remarks_read_1']) && $aPost['remarks_read_1'] != '') {
      $aValues['remarks_read_1'] = 'NOW()';
      $aValues['remarks_read_1_by'] = $_SESSION['user']['name'];
    }
    
    $this->_updateOrder($iId, $aValues, $aOrder);

    // aktualizacja statusów także dla składowych zamówienia
    $aFirstPayment = array();
    $aFirstPayment['ptype'] = $aValues['second_payment_type'];
    $aFirstPayment['id'] = $aValues['second_payment_id'];
    if ($this->_updatePaymentItems($aFirstPayment, $iId, true) === false) {
      return false;
    }
    $aSecondPayment = array();
    $aSecondPayment['ptype'] = $aValues['payment_type'];
    $aSecondPayment['id'] = $aValues['payment_id'];
    if ($this->_updatePaymentItems($aSecondPayment, $iId, false) === false) {
      return false;
    }


    if (intval($aOrder['order_status']) >= 2) {
      $this->bAllWeight = 1;
    }
    $bPersonalReception = $this->_checkPersonalReception($iId);
    
    
    $iInternalStatus = $this->_getInternalStatus($iId,
                                                 $aOrder['order_status'], 
                                                 $bPersonalReception, 
                                                 $aOrder['invoice_date'], 
                                                 $aOrder['payment_status'],
                                                 $aValues['payment_type'],
                                                 $aValues['second_payment_type'],
                                                 $aOrder['paid_amount'],
                                                 $aOrder['to_pay'],
                                                 $aOrder['active']);

    $this->_changeInternalStatus($iId, $iInternalStatus, $aOrder['internal_status']);
    
    
    // wysylanie informacji o zmienie metody platnosci
    if (intval($aPost['payment_method']) != 0 && $aPost['payment_method'] != $aOrder['payment_id']) {
      return 2;
    }
    if ($this->bSendMail) {
      return 2;
    }
    
    $this->_removeProforma($aOrder['order_number']);
    return true;
  }// end of recountOrderPost() method
  
  
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type 
	 */
	private function getWebsiteSymbol($iWebsiteId) {
		global $aConfig;
		
		$sSql = "SELECT code FROM ".$aConfig['tabls']['prefix']."websites
						 WHERE id=".$iWebsiteId;
		return Common::GetOne($sSql);
	}// end of getWebsiteSymbol() method
	
	
	/**
	 * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
	 *
	 * @global array $aConfig
	 * @param int $iOId
   * @param bool $bRemember
	 * @return type 
	 */
	private function getWebsiteId($iOId, $bRemember = true) {
		global $aConfig;
		
		if ($bRemember) {
			if ($this->iWebsiteId <= 0 ) {
				$sSql = "SELECT website_id FROM ".$aConfig['tabls']['prefix']."orders
								WHERE id=".$iOId;
				$this->iWebsiteId = Common::GetOne($sSql);
				return $this->iWebsiteId;
			}
		} else {
				$sSql = "SELECT website_id FROM ".$aConfig['tabls']['prefix']."orders
								WHERE id=".$iOId;
				return Common::GetOne($sSql);
		}
	}// end of getWebsiteId() method
  
  
  /**
   * Metoda zwraca nowy status zamówienia
   * 
   * @param int $iOrderStatus
   * @return int
   * @throws Exception
   */
  private function _getOrderValuesStatuses($iOrderStatus) {
    
    // zamówienie jest nowe i wszystkie produkty mają ustalony status różny od '---'
    // przestawiamy status zamowienia na 'w realizacji'
    if (intval($iOrderStatus) == 0 && $this->bAllNotNull) {
      $this->bSendMail = true;
      return 1;
    }
    // zamówienie ma status niższy niż 'skompletowane' oraz wszystkie produkty sa ustawione na 'są'
    if (intval($iOrderStatus) < 2 && $this->bAllReady) {
      // wszystkie produkty maja podana wagę
      if ($this->bAllWeight) {
        $this->bSendMail = true;
        return 2;
      } else {
        // brak wagi w conajmniej jednym produkcie
        throw new Exception(_('Brak wagi w conajmniej jednym produkcie'));
      }
    }
    // zamówienie ma status 'skompletowane' oraz NIE wszystkie produkty sa ustawione na 'są'
    if (intval($iOrderStatus) == 2 && !$this->bAllReady) {
      return 1;
    }
  }// end of _getOrderValuesStatuses() method
  
  
  /**
   * Metoda usuwa fakturę proformę
   * 
   * @global array $aConfig
   * @param string $sOrderNumber
   */
  private function _removeProforma($sOrderNumber) {
    global $aConfig;
    
    $fName = 'faktura_proforma' . str_replace("/", "_", $sOrderNumber) . '.pdf';
    $sFile = $aConfig['common']['client_base_path'] . $aConfig['common']['pro_forma_invoice_dir'] . $fName;
    if (file_exists($sFile)) {
      @unlink($sFile);
    }
  }// end of _removeProforma() method
  
  
  /**
   * Metoda zmienia dane zamówienia
   * 
   * @param int $iId
   * @param array $aValues
   * @param array $aOrder
   * @throws Exception
   */
  private function _updateOrder($iId, $aValues, $aOrder) {
    $oLogger = new logger($this->pDbMgr);
    $aValuesChanged = $this->pDbMgr->getChangedUpdateValues($aValues, $aOrder);
    if (isset($aValuesChanged['transport_id']) || isset($aValuesChanged['point_of_receipt'])) {
//      $sTitle = 'Zmiana transportu lub pkt. odbioru '.$aOrder['order_number'];
//      Common::sendMail('zamowienia@profit24.pl', 'zamowienia@profit24.pl', 'a.golba@profit24.pl', $sTitle, print_r($_GET, true).print_r($_POST, true).print_r($_SESSION, true).print_r(debug_backtrace(), true));
//      Common::sendMail('zamowienia@profit24.pl', 'zamowienia@profit24.pl', 'k.malz@profit24.pl', $sTitle, ' Zmiana metody transportu w zamówienu: '.$aOrder['order_number']);
    }
    $oLogger->addLog($iId, $_SESSION['user']['id'], $aValuesChanged);
    if ($this->pDbMgr->Update('profit24', "orders", $aValues, "id = " . $iId) === false) {
      throw new Exception(_('Wystąpił błąd podczas zmian w zamówieniu'));
    }
  }// end of _updateOrder() method
  
  
	/**
	 * Jeśli w zamówieniu zmieniła się metoda płatności aktualizacja także w składowych zamówienia
	 *
   * @param array $aPayment
	 * @param int $iOrderId
   * @param array $bSecondPayment
	 * @return bool
	 */
	private function _updatePaymentItems($aPayment, $iOrderId, $bSecondPayment=true) {
		
		$aValues = array();
		if ($aPayment['ptype'] != '' && $aPayment['id'] != '') {
			$aValues['payment_type'] = $aPayment['ptype'];
			$aValues['payment_id'] =	$aPayment['id'];
		} else {
			// nic nie robimy
			return true;
		}
    
		return $this->pDbMgr->Update('profit24', "orders_items", $aValues, ($bSecondPayment == true ? " second_payment='1'" : " second_payment='0'")." AND order_id=".$iOrderId);
	} // end of updatePaymentItems() method
  
  
	/**
	 * Sprawdza czy sposob transportu dla amówienia to odbiór osobisty
   * 
	 * @param int $iOrderId - id zamowienia
	 * @return bool
	 */
	private function _checkPersonalReception($iOrderId){
    
		$sSql = "SELECT B.personal_reception
						FROM orders A
						LEFT JOIN orders_transport_means B
						ON B.id=A.transport_id
						WHERE A.id = ".$iOrderId;
		return ($this->pDbMgr->GetOne('profit24', $sSql) == '1');
	}// end of checkPersonalReception() method
  
  
  /**
   * Metoda zmienia status wewnętrzny zamówienia
   * 
   * @param int $iId
   * @param int $iNewInternalStatus
   * @param int $iNowInternalStatus
   * @return boolean
   */
  protected function _changeInternalStatus($iId, $iNewInternalStatus, $iNowInternalStatus) {
    
    if ($iNewInternalStatus != $iNowInternalStatus) {
      $aVal = array('internal_status' => $iNewInternalStatus);
      $oLogger = new logger($this->pDbMgr);
      $oLogger->addLog($iId, $_SESSION['user']['id'], $aVal);
      if ($this->pDbMgr->Update('profit24', "orders", $aVal, "id = " . $iId) === false) {
        return false;
      }
    }
    return true;
  }// end of _changeInternalStatus() method
  
  
  /**
   * Metoda określa wewnętrzny status zamówienia
   * 
   * @param char $cOrderStatus
   * @param bool $bPersonalReception
   * @param string $sInvoiceDate
   * @param char $cPaymentStatus
   * @param string $sPaymentType
   * @param string $sSecondPaymentType
   * @param float $fPaidAmount
   * @param float $fToPaidAmount
   * @param char $bActive
   * @return string
   */
  protected function _getInternalStatus($cOrderStatus, $bPersonalReception, $sInvoiceDate, $cPaymentStatus, $sPaymentType, $sSecondPaymentType, $fPaidAmount, $fToPaidAmount, $bActive) {
    
    
    /*     * ******** status wewnętrzny ********************** */
    $iInternalStatus = '0';
    if ($cOrderStatus == '5') {
      $iInternalStatus = 'A'; // anulowane
    } elseif ($cOrderStatus == '4') {
      $iInternalStatus = '9'; // wysłane
    } elseif ($cOrderStatus == '3' && !$bPersonalReception && !empty($sInvoiceDate)) {
      $iInternalStatus = '8'; // paczki
    }
    /**
     * Usunięcie  && !$bPersonalReception - zamówienia,
     *  które nie posiadają faktury a są na odbiór osobisty także mają wpaść do zakładki faktury,
     * aby została wygenerowana faktura
     *
     *  @date 13.08.2013
     *  @mod arekgl0@op.pl
      elseif ($cOrderStatus == '3' || ($cOrderStatus == '2' && $bPersonalReception === TRUE)) {
      $iInternalStatus = '7'; // faktury
     */
      elseif ($this->bRemarksRead_1 && $this->bAllReady && $this->bAllWeight &&
            (
            (($cPaymentStatus == 0 || $cPaymentStatus == 2) && $sPaymentType != 'bank_14days' && $sPaymentType != 'postal_fee') ||
            ($cPaymentStatus == 1 && ($fPaidAmount < $fToPaidAmount && $sSecondPaymentType != 'postal_fee')))
    ) {
      $iInternalStatus = '4'; // skompletowane
    } elseif ($cOrderStatus == '3' && $this->bRemarksRead_1 && $this->bAllReady && $this->bAllWeight && $bPersonalReception && ($cPaymentStatus == 1 || $sPaymentType == 'bank_14days' || $sPaymentType == 'postal_fee' || $sSecondPaymentType == 'postal_fee')) {
      $iInternalStatus = '6'; // odbór osobisty
    } elseif ($this->bRemarksRead_1 && $this->bAllReady && $this->bAllWeight && ($cPaymentStatus == 1 || $sPaymentType == 'bank_14days' || $sPaymentType == 'postal_fee' || $sSecondPaymentType == 'postal_fee')) {//dodana opcja uzwgledniajaa mozlwiosc zmiany drugiej formy platnosci na pobranie
      $iInternalStatus = '5'; // wysyłka
    } elseif ($this->bRemarksRead_1 && $this->bAllReadyNC) {
      $iInternalStatus = '3'; // do realizacji
    } elseif ($this->bRemarksRead_1 && $this->bExistNotNull && $this->bAllNotNull) {
      $iInternalStatus = '2'; // Czesciowe
    } elseif (($sPaymentType != 'platnoscipl' && $sPaymentType != 'card' && $bActive != '0') || $cPaymentStatus == 1) {
      $iInternalStatus = '1'; // nowe
    }
    return $iInternalStatus;
  }// end of _getInternalStatus() method
}// end of Class
