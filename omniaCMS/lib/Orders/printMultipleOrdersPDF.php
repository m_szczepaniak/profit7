<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\lib\Orders;

use DatabaseManager;
use TCPDF;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class printMultipleOrdersPDF {

  private $pSmarty;

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var TCPDF
   */
  private $pdf;

  public function __construct($pDbMgr, $pSmarty) {
    
    $this->pDbMgr = $pDbMgr;
    $this->pSmarty = $pSmarty;
  }
  
  /**
   * 
   * @param array $aItemsIds
   */
  public function setOrders($aItemsIds) {
    

		if (!empty($aItemsIds)) {
			
			require_once('tcpdf/config/lang/pl.php');
      require_once('tcpdf/tcpdf.php');		

      // create new PDF document
      $this->pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $this->pdf->SetCreator(PDF_CREATOR);
      $this->pdf->SetAuthor('profit24');
      $this->pdf->SetTitle($aLang['title']);
      $this->pdf->SetSubject($aLang['title']);
      $this->pdf->SetKeywords('');
      $this->pdf->setPrintHeader(false);

      // set default header data
      //$this->pdf->SetHeaderData('logo.gif', PDF_HEADER_LOGO_WIDTH, $aLang['logo_title'], $aLang['logo_string']);

      // set header and footer fonts
      //	$this->pdf->setHeaderFont(Array('freesans', '', PDF_FONT_SIZE_MAIN));
      $this->pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

      // set default monospaced font
      $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

      //set margins
      $this->pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
      //$this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

      //set auto page breaks
      $this->pdf->SetAutoPageBreak(TRUE, 10);

      //set image scale factor
      $this->pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 

      //set some language-dependent strings
      $this->pdf->setLanguageArray($l); 

      // set font
      $this->pdf->SetFont('freesans', '', 10);

      // ---------------------------------------------------------

      $printOrder = new getOrderHTML($this->pDbMgr, $this->pSmarty);
      foreach($aItemsIds as $iOrderId){
        // add a page
        $this->pdf->AddPage();
        $sHtml = $printOrder->getOrderHTML($iOrderId);
        $this->pdf->writeHTML($sHtml, true, false, false, false, '');
      }
		}
  }
  
  /**
   * 
   * @return type
   */
  public function getPDF() {
    return $this->pdf->Output('zamowienia.pdf', 'I');
  }
}
