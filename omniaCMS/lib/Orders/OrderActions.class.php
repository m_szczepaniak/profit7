<?php
/*
 * Klasa obsługująca akcje na zamówieniu,
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-13 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace omniaCMS\lib\Orders;

use DatabaseManager;
use DateInterval;
use DateTime;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use LIB\orders\Payment\Payment;
use Module;
use orders\logger;
use orders\OrderStreamsoftId;
use orders\Shipment;
use ValidatePhone;

/**
 * @todo na końcu detekcja $this
 *  new  na new \
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class OrderActions extends Order {

  /**
   *
   * @var DatabaseManager
   */
  protected $pDbMgr;
  
  /**
   *
   * @var array
   */
  protected $aLang;
  
  /**
   *
   * @var Module
   */
  private $oModuleZamowienia;
  
  /**
   *
   * @var array
   */
  protected $_aConfig;
  
  /**
   *
   * @var array
   */
  protected $aOrderData;
  
  /**
   *
   * @var id
   */
  private $iOrderId;
  
  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function __construct($pDbMgr, $oModule, $iOrderId) {
    global $aConfig;
    parent::__construct($pDbMgr, $iOrderId);
    $this->_aConfig = $aConfig;
    
    $this->pDbMgr = $pDbMgr;
    $this->aLang = $this->_aConfig['lang'][$oModule->sModule];
    $this->oModuleZamowienia = $oModule;
    $this->iOrderId = $iOrderId;
    $this->aOrderData = $this->getSelOrderData(array('*'));
  }
  
  /**
   * 
   * @param mixed $name
   * @throws Exception
   */
  public function __get($name) {
    throw new Exception('Pobierano nieznana właściwość : '.$name);
  }
  
  /**
   * 
   * @param mixed $name
   * @param mixed $value
   * @throws Exception
   */
  public function __set($name, $value) {
    throw new Exception('Zmieniano Nieznana właściwość : '.$name.' na wartość: '.nl2br(print_r($value, true)));
  }

  
  /**
   * 
   * @return bool
   */
  public function updateOrderPaidAmount() {
    
    if ($this->isMegaSupervisor($_SESSION['user']['name'])) {
      $fPaidAmount = str_replace(',', '.', $_POST['paid_amount']);
      
      $aValues = array(
          'paid_amount' => $fPaidAmount
      );
      if ($this->Update('orders', $aValues, 'id = '.$this->iOrderId) === FALSE) {
        $bIsErr = true;
        $sMsg = _('Wystąpił błąd podczas ustawiania wartości opłaconej: '.$fPaidAmount.' w zamówieniu '.$this->aOrderData['order_number']);
      } else {
        $bIsErr = false;
        $sMsg = _('Ustawiono zapłaconą wartość "'.$fPaidAmount.'" w zamówieniu '.$this->aOrderData['order_number']);
      }
      $this->setMessage($sMsg, $bIsErr);
      return true;
    }
    return false;
  }

  
  /**
   * 
   * @param string $sLogin
   * @return bool
   */
  protected function isMegaSupervisor($sLogin) {
    
    return ($sLogin == 'agolba' || $sLogin == 'mchudy') ? TRUE : FALSE;
  }// end of isMegaSupervisor() method
  
  
  /**
   * 
   * @return bool
   */
  public function addLinkedOrdersTransport() {
    $bIsErr = false;
    
    $oLogger = new logger($this->pDbMgr);
    $this->pDbMgr->BeginTransaction('profit24');
    $aLinkedOrders = array_keys($_POST['delete']);
   
    if (count($aLinkedOrders) >= 2) {
      foreach ($aLinkedOrders as $iKey => $iMainItemId) {
        $aValues = array(
            'linked_order' => '1'
        );
        if ($this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iMainItemId) === false) {
          $bIsErr = true;
          break;
        }
        foreach ($aLinkedOrders as $iNdKey => $iSubItemId) {
          if ($iKey != $iNdKey) {
            $aValues = array(
                'main_order_id' => $iMainItemId,
                'linked_order_id' => $iSubItemId
            );
            $oLogger->addLog($iSubItemId, $_SESSION['user']['id'], array('orders_linked_transport' => $aValues));
            if ($this->pDbMgr->Insert('profit24', 'orders_linked_transport', $aValues) === FALSE) {
              $bIsErr = true;
              break;
            }
          }
        }
      }
      
      if ($bIsErr == FALSE) {
        $this->pDbMgr->CommitTransaction('profit24');
        $sMsg = _("Zamówienia o id ".implode(', ', $aLinkedOrders)." zostaną wysłane razem.");
        $this->setMessage($sMsg, false);
        return true;
      } else {
        $this->pDbMgr->RollbackTransaction('profit24');
        $sMsg = _("Wystąpił błąd podczas łączenia zamówień ".implode(', ', $aLinkedOrders));
        $this->setMessage($sMsg, true);
        return false;
      }
    } else {
      $sMsg = _("Wybierz przynajmniej dwa zamówienia, aby wysłać je razem");
      $this->setMessage($sMsg, false);
      return false;
    }
  }
  
  /**
   * 
   * @return boolean
   */
  public function deleteOrderPayment() {
    $iPaymentId = $_GET['payment_id'];
    $iStatementId = $_GET['statement_id'];
    if ($iPaymentId > 0 && $iStatementId > 0 && $this->iOrderId > 0) {
      $this->pDbMgr->BeginTransaction('profit24');
      // 1) przelew na -  i na + | przeksięgowanie
      $oPayment = new Payment($this->pDbMgr);
      if ($oPayment->deletePayment($iPaymentId, $iStatementId, $this->iOrderId) === false) {
        $sMsg = _('Wystąpił błąd podczas usuwnaia przelewu');
        $this->setMessage($sMsg, true);
        $this->pDbMgr->RollbackTransaction('profit24');
        return false;
      } else {
        $this->pDbMgr->CommitTransaction('profit24');
        $sMsg = _('Usunięto przelew '.$iStatementId.' z zamówienia / zamówień');
        $this->setMessage($sMsg, false);
        return true;
      }
    } else {
      $sMsg = _('Nieprawidłowy przelew do usunięcia');
      $this->setMessage($sMsg, true);
      return false;
    }
  }
  
  
  /**
   * Metoda usuwa powiązanie
   * 
   * @return bool
   */
  public function deleteLinkedOrdersTransport() {
    $bIsErr = false;
    
    $sSql = 'DELETE FROM orders_linked_transport WHERE main_order_id = '.$_GET['m_id'].' AND linked_order_id = '.$_GET['s_id'];
    if ($this->pDbMgr->Query('profit24', $sSql) === FALSE){
      $bIsErr = true;
    }
    $sSql = 'DELETE FROM orders_linked_transport WHERE main_order_id = '.$_GET['s_id'].' AND linked_order_id = '.$_GET['m_id'];
    if ($this->pDbMgr->Query('profit24', $sSql) === FALSE) {
      $bIsErr = true;
    }
    
    $aValues = array(
      'linked_order' => '0'
    );
    if ($this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$_GET['s_id']) === false) {
      $bIsErr = true;
    }
    $aValues = array(
      'linked_order' => '0'
    );
    if ($this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$_GET['m_id']) === false) {
      $bIsErr = true;
    }

    if ($bIsErr === TRUE) {
      $sMsg = _('Wystąpił błąd podczas usuwania powiązania zamówień które powinny być razem wysłane');
      $this->setMessage($sMsg, true);
      return false;
    } else {
      $sMsg = _('Usunięto powiązanie pomiędzy zamówieniami które powinny być razem wysłane');
      $this->setMessage($sMsg, false);
      return true;
    }
  }

  
  /**
   * 
   * @return boolean
   */
  public function AddUserComments() {
    
		$sSql = "SELECT name, surname
							FROM users
							 WHERE id = ".intval($_SESSION['user']['id']);
		$aGeneratorName = $this->pDbMgr->GetRow('profit24', $sSql);
		
		$aValues=array(
      'comment' => $_POST['user_comments_input'],
      'created' => 'NOW()',
      'created_by' => $aGeneratorName['name'].' '.$aGeneratorName['surname'],
      'user_id' => $this->aOrderData['user_id']
		);
		if ($this->pDbMgr->Insert('profit24', "users_accounts_comments", $aValues,'', false) === false) {
      $this->setMessage('user_comments_edit_err', true);
      return false;
		} else {
      $this->addAdditionalUpdateInfo("users_accounts_comments", $aValues);
      $this->setMessage('user_comments_edit_ok', false);
      return true;
    }
  }
  
  
  /**
   * 
   * @return boolean
   */
  public function AddAdditionalInfo() {
    
		$sSql = "SELECT name, surname
							FROM users
							 WHERE id = ".intval($_SESSION['user']['id']);
		$aGeneratorName = $this->pDbMgr->GetRow('profit24', $sSql);
		
		$aValues=array(
			'created' => 'NOW()',
      'created_by' => $aGeneratorName['name'].' '.$aGeneratorName['surname'],
      'order_id' => $this->iOrderId,
      'value' => $_POST['additional_info_input']
		);
    
		if ($this->pDbMgr->Insert('profit24', "orders_additional_info", $aValues,'', false) === false) {
      $this->setMessage('order_additional_info_edit_err', true);
      return false;
		} else {
      $this->addAdditionalUpdateInfo("orders_additional_info", $aValues);
      $this->setMessage('order_additional_info_edit_ok', false);
      return true;
    }
  }
  
  
  /**
  * Metoda wysyła maila z informacją o zmianie zawartości zamówinia
  * 
  * @return boolean
  */
  public function UpdateSendDateAndExpectedSendDate(){

    $aDate=explode('-', $_POST['send_date_input']);
    $dSendDate = $aDate[2].'-'.$aDate[1].'-'.$aDate[0];
		$aValues=array(
			'send_date' => $dSendDate,
      'shipment_date_expected' => $dSendDate
		);
    
    $mReturn = $this->Update("orders", $aValues, "id = ".$this->iOrderId);

    if($mReturn === false){
      $this->setMessage('order_send_date_edit_err', true);
      return false;
    } else {
      $this->setMessage('order_send_date_edit_ok', false);
      return true;
    }
  } // end of sendModified() method
  
  /**
   * Metoda dodaje uwagi do faktury
   * 
   * @return bool
   */
  public function saveInvoiceToPayDays() {
    
    $iAddDate = $_POST['invoice_to_pay_days'];
    if ($iAddDate > 0) {
      $aValues = array('invoice_to_pay_days' => $_POST['invoice_to_pay_days']);
      $dInvoiceDateTime = $this->aOrderData['invoice_date'];
      if ($dInvoiceDateTime != '') {
        // już ustawiona data płatności, więc zmieniamy ją
        $dInvoiceDate = explode(' ', $dInvoiceDateTime);
        $newDatePay = new DateTime($dInvoiceDate[0]);
        $newDatePay->add(new DateInterval('P'.$iAddDate.'D'));
        $aValues['invoice_date_pay'] = $newDatePay->format('Y-m-d');
      }
      
      if ($this->Update('orders', $aValues, ' id = '.$this->iOrderId) === FALSE) {
        // ERR
        $sMsg = _('Wystąpił błąd podczas zmiany ilości dni do zapłaty FV na "'.$_POST['invoice_to_pay_days'].'" dni w zamówieniu '.$this->aOrderData['order_number']);
        $this->setMessage($sMsg, true);
        return false;
      } else {
        // OK
        $sMsg = _('Zmieniono ilość dni do zapłacenia FV na "'.$_POST['invoice_to_pay_days'].'" dni w zamówieniu '.$this->aOrderData['order_number']);
        $this->setMessage($sMsg, false);
        return true;
      }
    } else {
      // ERR
      $sMsg = _('Wystąpił błąd podczas zmiany ilości dni do zapłaty FV w zamówieniu '.$this->aOrderData['order_number']);
      $this->setMessage($sMsg, true);
      return false;
    }
    return true;
  }// end of saveInvoiceToPayDays() method
  
  /**
   * Metoda wysyła maila z informacją o zmianie zawartości zamówinia
   * 
   * @return boolean
   */
  public function saveMagazineRemarks(){

    $aValues = array('magazine_remarks' => $_POST['magazine_remarks']);
    $mReturn = $this->Update("orders", $aValues, "id = ".$this->iOrderId);
    $this->removeInvoice();
    
    if($mReturn === false){
      $sMsg = _('Wystąpił błąd podczas ustawiania uwag do zamówienia '.$this->aOrderData['order_number']);
      $this->setMessage($sMsg, true);
      return false;
    } else {
      $sMsg = _('Ustawiono uwagi do zamówienia '.$this->aOrderData['order_number']);
      $this->setMessage($sMsg, false);
      return true;
    }
  } // end of sendModified() method
  
  
  /**
   * Metoda wysyła maila z informacją o zmianie zawartości zamówinia
   * 
   * @return boolean
   */
  public function invoiceRemarks(){

    $aValues = array('invoice_remarks' => $_POST['invoice_remarks']);
    $mReturn = $this->Update("orders", $aValues, "id = ".$this->iOrderId);
    $this->removeInvoice();

    if($mReturn === false){
      $sMsg = _('Wystąpił błąd podczas dodawania uwagi do faktury '.$this->aOrderData['order_number']);
      $this->setMessage($sMsg, true);
      return false;
    } else {
      $sMsg = _('Dodano uwagi do faktury do zamówienia '.$this->aOrderData['order_number']);
      $this->setMessage($sMsg, false);
      return true;
    }
  } // end of sendModified() method
  
  
  /**
   * Metoda wysyła maila z informacją o zmianie zawartości zamówinia
   * 
   * @return boolean
   */
  public function UpdateTransportNumberAndDeleteInvoice(){

    $aValues = array('transport_number' => $_POST['transport_number']);
    $mReturn = $this->Update("orders", $aValues, "id = ".$this->iOrderId);
    $this->removeInvoice();

    if($mReturn === false){
      $this->setMessage('order_transport_number_edit_err', true);
      return false;
    } else {
      $this->setMessage('order_transport_number_edit_ok', false);
      return true;
    }
  } // end of sendModified() method
  
  
	/**
	  * Metoda wysyła maila z informacją o zmianie zawartości zamówinia
    * 
    * @return boolean
	  */
	public function UpdateSellerStreamsoftId(){
  
    $aValues = array('seller_streamsoft_id' => $_POST['seller_streamsoft_id']);
    if($this->Update("orders", $aValues, "id = ".$this->iOrderId) === false){
      $this->setMessage(sprintf(_('Wystąpił błąd podczas próby ustawiania handlowca'), $_POST['seller_streamsoft_id']), true);
      return false;
    } else {
      $this->setMessage(sprintf(_('Ustawiono handlowca'), $_POST['seller_streamsoft_id']), false);
      return true;
    }
	} // end of sendModified() method
  
  
	 /**
	  * Metoda wysyła maila z informacją o zmianie zawartości zamówinia
    * 
    * @return boolean
	  */
	public function UpdateTransportRemarks(){
  
    $aValues = array('transport_remarks' => $_POST['transport_remarks']);
    if($this->Update("orders", $aValues, "id = ".$this->iOrderId) === false){
      $this->setMessage('order_transport_remarks_edit_err', true);
      return false;
    } else {
      $this->setMessage('order_transport_remarks_edit_ok', false);
      return true;
    }
	} // end of sendModified() method
  
  /**
	 * Metoda aktualizuje status pola "Drukuj fakturę"
	 *
	 * @return boolean
	 */
	public function printFV() {
    
    if ($this->aOrderData['print_fv'] == '1') {
      $aValues['print_fv'] = 0;
    } else {
      $aValues['print_fv'] = 1;
    }

      if (isset($_GET['force_print_fv'])) {
        $aValues['print_fv'] = $_GET['force_print_fv'];
      }

		if ($this->Update('orders', $aValues, ' id = '.$this->iOrderId) === false) {
      $sMsg = _('Wystąpił błąd podczas ustawiania "Drukuj fakturę" w zamówieniu o id '.$this->iOrderId);
      $this->setMessage($sMsg, true);
      return false;
    } else {
      $sMsg = _('Faktura zamówienia o id '.$this->iOrderId.' zostanie wydrukowana');
      $this->setMessage($sMsg, false);
      return true;
    }
	}// end of UpdateOpineo() function
  
	/**
	 * Metoda aktualizuje status pola "Nie wysyłaj prośby o opinię"
	 *
	 * @return boolean
	 */
	public function dontCancel() {
    
    if ($this->aOrderData['dont_cancel'] == '1') {
      $aValues['dont_cancel'] = 0;
    } else {
      $aValues['dont_cancel'] = 1;
    }
		if ($this->Update('orders', $aValues, ' id = '.$this->iOrderId) === false) {
      $sMsg = _('Wystąpił błąd podczas ustawiania "Wyłącz auto-anulowanie zamówienia" w zamówieniu o id '.$this->iOrderId);
      $this->setMessage($sMsg, true);
      return false;
    } else {
      $sMsg = _('Zamówienie o id '.$this->iOrderId.' nie zostanie automatycznie anulowane');
      $this->setMessage($sMsg, false);
      return true;
    }
	}// end of UpdateOpineo() function
  
  
	/**
	 * Metoda aktualizuje status pola "Nie wysyłaj prośby o opinię"
	 *
	 * @return boolean
	 */
	public function magazineGetReadyListVIP() {
    
    if ($this->aOrderData['magazine_get_ready_list_VIP'] == '1') {
      $aValues['magazine_get_ready_list_VIP'] = 0;
    } else {
      $aValues['magazine_get_ready_list_VIP'] = 1;
    }
		if ($this->Update('orders', $aValues, ' id = '.$this->iOrderId) === false) {
      $sMsg = _('Wystąpił błąd podczas ustawiania "zmiany statusu VIP" w zamówieniu o id '.$this->iOrderId);
      $this->setMessage($sMsg, true);
      return false;
    } else {
      $sMsg = _('Zamówienie o id '.$this->iOrderId.' ma status VIP na magazynie');
      $this->setMessage($sMsg, false);
      return true;
    }
	}// end of UpdateOpineo() function
  
  
	/**
	 * Metoda aktualizuje status pola "Nie wysyłaj prośby o opinię"
	 *
	 * @return boolean
	 */
	public function UpdateSendInfoDontSendOnTime() {
    
    if ($this->aOrderData['dont_send_info_not_send_on_time'] == '1') {
      $aValues['dont_send_info_not_send_on_time'] = 0;
    } else {
      $aValues['dont_send_info_not_send_on_time'] = 1;
    }
		if ($this->Update('orders', $aValues, ' id = '.$this->iOrderId) === false) {
      $this->setMessage(_('Błąd ustawiania flagi Nie wysyłaj informacji o opóźnieniu'), true);
      return false;
    } else {
      $this->setMessage(_('Klient nie otrzyma informacji o opóźnieniu'), false);
      return true;
    }
	}// end of UpdateSendInfoDontSendOnTime() function
  
  
	/**
	 * Metoda aktualizuje status pola "Nie wysyłaj prośby o opinię"
	 *
	 * @return boolean
	 */
	public function UpdateOpineo() {
    
    if ($this->aOrderData['opineo_dont_send'] == '1') {
      $aValues['opineo_dont_send'] = 0;
    } else {
      $aValues['opineo_dont_send'] = 1;
    }
		if ($this->Update('orders', $aValues, ' id = '.$this->iOrderId) === false) {
      $this->setMessage('order_opineo_edit_err', true);
      return false;
    } else {
      $this->setMessage('order_opineo_edit_ok', false);
      return true;
    }
	}// end of UpdateOpineo() function
  
	 /**
	  * Metoda wysyła maila z informacją o zmianie zawartości zamówinia
    * 
    * @return boolean
	  */
	public function sendModified(){
  
    if($this->oModuleZamowienia->sendOrderMail($this->iOrderId)){
      $aValues = array('is_modified' => '0');
      if($this->Update("orders", $aValues, "id = ".$this->iOrderId)===false){
        $this->setMessage('send_update_err', true);
        return false;
      }
    }
    return true;
	} // end of sendModified() method
  
  
	/**
	 * Metoda wprowadza zmiany w danych uzytkownika dla zamówienia
	 *
	 * @return	boolean
	 */
	public function UpdateUserData() {
		$bIsErr = false;

    $sTransportSymbol = $this->oModuleZamowienia->getTransportMeansSymbol($this->aOrderData['transport_id']);

    // Jeśli TBA to walidujemy kod pocztowy (musi być w rejonie doręczeń TBA Express)
    if ($sTransportSymbol === 'tba') {
      include_once($this->_aConfig['common']['client_base_path'].'LIB/autoloader.php');
      $oShipment = new Shipment($this->aOrderData['transport_id'], $this->pDbMgr);
      $bValidatePostcode = $oShipment->checkDestinationPointAvailable($_POST['postal_1']);  

      if (!$bValidatePostcode) {
        $this->setMessage(_('Wprowadzony kod pocztowy znajduje się poza obszarem doręczeń firmy TBA Express'), true);
        return false;
      }      
    }    

    // Walidacja telefonu przy aktualizacji danych. Jeśli Fedex lub TBA to pozwalamy wprowadzić nr stacjonarny
    if ($sTransportSymbol === 'opek-przesylka-kurierska' || $sTransportSymbol === 'poczta-polska-doreczenie-pod-adres' || $sTransportSymbol === 'tba') {
      $aMatches = array();
      if (!preg_match($this->_aConfig['class']['form']['phone']['pcre'], $_POST['phone_1'], $aMatches)) {
        $sMsg = _('Podany numer telefonu nie jest prawidłowy. Dane nie zostały uaktualnione');
        $this->setMessage($sMsg, true);
        return false;
      }
      // Jeśli nie Fedex i nie TBA to walidujemy nr tel. kom. zgodnie z UKE  
    } else {
      // Walidator numerów telefonów komórkowych zgodny z UKE
      include_once($this->_aConfig['common']['client_base_path'].'/omniaCMS/lib/Form/ValidateFields/ValidatePhone.class.php');
      $oValidatePhone = new ValidatePhone();

      if (isset($_POST['phone_1']) && !$oValidatePhone->validate($_POST['phone_1'])) {
        $sMsg = _('Podany numer telefonu nie jest prawidłowy. Dane nie zostały uaktualnione');
        $this->setMessage($sMsg, true);
        return false;
      }      
    }

    $this->pDbMgr->BeginTransaction('profit24');
    if (!empty($this->aOrderData)) {
      // pobierz symbol metody transportu
      $sTransportSymbol = $this->oModuleZamowienia->getTransportMeansSymbol($this->aOrderData['transport_id']);
    }

    if ($sTransportSymbol == 'paczkomaty_24_7') {
      // wyświetl info dla paczkomatów
      if ($this->updatePhonePaczkomaty($this->iOrderId, $_POST['phone_paczkomaty']) === false) {
        $bIsErr = true;

      // walidacja telefonu dla paczkomatów w edycji danych do dostawy  
      } elseif (!$oValidatePhone->validate($_POST['phone_paczkomaty'])) {
        $sMsg = _('Podany numer telefonu nie jest prawidłowy. Dane nie zostały uaktualnione');
        $this->setMessage($sMsg, true);
        return false;
      }

    } else {
       if ( $this->updateOrderAddress(1) === false){
        $bIsErr = true;
      }
    }


    if($this->updateOrderAddress(0) === false){
      $bIsErr = true;
    }

    if($this->checkSecondInvoice() == '1'){
      if($this->updateOrderAddress(2) === false){
        $bIsErr = true;
      }
    }

    if (isset($_POST['invoice_recipient'])) {
        $_POST['invoice_recipient'] = trim($_POST['invoice_recipient']);
        if ($_POST['invoice_recipient'] != $this->aOrderData['invoice_recipient']) {
            $values = ['invoice_recipient' => $_POST['invoice_recipient']];
            $this->Update('orders', $values, ' id = '.$this->aOrderData['id']);
        }
    }


    if ($bIsErr) {
      $this->pDbMgr->RollbackTransaction('profit24');
      $this->setMessage('order_user_data_edit_err', true);
      return false;
    }
    else {
      $this->pDbMgr->CommitTransaction('profit24');
      $this->setMessage('order_user_data_edit_ok', false);
      return true;
    }
	}// end of UpdateUserData() function
  
  
	/**
	 * Sprawdza czy zamówienie posiada drugą fakturę
   * 
	 * @return char
	 */
	private function checkSecondInvoice(){
    
		return $this->aOrderData['second_invoice'];
	}
	
	
	/**
	 * Aktualizuje dane adresowe w bazie
	 * 
	 * @param $iFormAddresId - typ adresu
	 * @return bool - success
	 */
	private function updateOrderAddress($iFormAddresId){

		$aValues = array(
		  'name' => trim($_POST['name_'.$iFormAddresId]),
		  'surname' => trim($_POST['surname_'.$iFormAddresId]),
		  'street' => trim($_POST['street_'.$iFormAddresId]),
		  'number' => trim($_POST['number_'.$iFormAddresId]),
		  'number2' => trim($_POST['number2_'.$iFormAddresId]),
		  'postal' => trim($_POST['postal_'.$iFormAddresId]),
		  'city' => trim($_POST['city_'.$iFormAddresId]),
		  'phone' => trim($_POST['phone_'.$iFormAddresId]),
		);
		if($_POST['is_company_'.$iFormAddresId] == "1"){
			$aValues['is_company'] = '1';
		} else {
			$aValues['is_company'] = '0';
		}
		
		$aValues['company'] = trim($_POST['company_'.$iFormAddresId]);
		if ($aValues['company'] == '') {
			$aValues['company'] = 'NULL';
		}
		
		$aValues['nip'] = trim($_POST['nip_'.$iFormAddresId]);
		if ($aValues['nip'] == '') {
			$aValues['nip'] = 'NULL';
		}

        if (isset($_POST['streamsoft_id_' . $iFormAddresId])) {
            $streamsoftId = trim($_POST['streamsoft_id_' . $iFormAddresId]);
            if ($streamsoftId > 0 && is_numeric($streamsoftId)) {
                $aValues['streamsoft_id'] = $streamsoftId;
            }
        }

      if (
          0 == $iFormAddresId &&
          ($this->aOrderData['erp_export'] == null || $this->aOrderData['erp_export'] == '') &&
          ($this->aOrderData['erp_send_ouz'] == null || $this->aOrderData['erp_send_ouz'] == '')
      ) {

        $orderId = $this->iOrderId;
        $currentInvoiceAddress = $this->pDbMgr->GetRow('profit24', "SELECT is_company, company, `name`, surname, street, number, number2, postal, city, nip, phone FROM orders_users_addresses WHERE order_id = $orderId AND address_type = '0'");

        if (!empty($currentInvoiceAddress)) {

          $OrderStreamsoftId = new OrderStreamsoftId();
          $addressesAreTheSame = $OrderStreamsoftId->compareOrderArrays($aValues, $currentInvoiceAddress, ['is_postal_mismatch', 'address_type', 'default_invoice', 'default_transport']);

          if ($addressesAreTheSame == false) {
            $aValues['streamsoft_id'] = 'NULL';
          }
        }
      }

		$aValues['is_postal_mismatch'] = ($this->oModuleZamowienia->checkCityPostal($aValues['city'],$aValues['postal'])?'0':'1');
		if(!$this->checkAddresExists($iFormAddresId)){
			$aValues['order_id'] = $this->iOrderId;
			$aValues['address_type'] = $iFormAddresId;
			if ($this->pDbMgr->Insert('profit24', "orders_users_addresses", $aValues) === false) {
				return false;
			}
      $this->addAdditionalUpdateInfo("orders_users_addresses", $aValues);
		} else {
			if ($this->pDbMgr->Update('profit24', "orders_users_addresses", $aValues," order_id = ".$this->iOrderId." AND address_type = '".$iFormAddresId."'") === false) {
				return false;
			}
      $this->addAdditionalUpdateInfo("orders_users_addresses", $aValues);
		}

		return true;
	} // end of updateOrderAddress() method
  
	
	/**
	 * Metoda sprawdza czy typ adresu zgadza się z id zamówienia
	 *
	 * @param integer $iFormAddresId
	 * @return bool
	 */
	private function checkAddresExists($iFormAddresId){
    
		$sSql = "SELECT COUNT(1)
							FROM orders_users_addresses
							WHERE order_id = ".$this->iOrderId." AND address_type = '".$iFormAddresId."'";
		return intval($this->pDbMgr->GetOne('profit24', $sSql)) > 0;
	}// end of checkAddresExists() method
  
  
  /**
   * Metoda aktualizuje telefon do paczkomaty inpost
   * 
   * @param int $iId
   * @param string $sPaczkomatyPhone
   * @return type
   */
  private function updatePhonePaczkomaty($iId, $sPaczkomatyPhone) {
    
    $aValues = array(
        'phone_paczkomaty' => $sPaczkomatyPhone
    );
    return $this->Update('orders', $aValues, ' id='.$iId);
  }// end of updatePhonePaczkomaty() method
  
  
  /**
   * 
   * @param string $sTable
   * @param array $aValues
   */
  private function addAdditionalUpdateInfo($sTable, $aValues) {
    
    $oLogger = new logger($this->pDbMgr);
    $oLogger->addLog($this->iOrderId, $_SESSION['user']['id'], array($sTable => $aValues));
  }
  
  
  /**
   * 
   * @param string $sTable
   * @param array $aValues
   * @param string $sWhere
   * @return mixed
   * @throws Exception
   */
  private function Update($sTable, $aValues, $sWhere) {
    
    if ($sTable == 'orders' ) {
      $oLogger = new logger($this->pDbMgr);
      $aValues = $this->pDbMgr->getChangedUpdateValues($aValues, $this->aOrderData);
      if (!empty($aValues)) {
        $oLogger->addLog($this->iOrderId, $_SESSION['user']['id'], $aValues);
        return $this->pDbMgr->Update('profit24', $sTable, $aValues, $sWhere);
      } else {
//        throw new \Exception(_('Dane nie zostały zmienione'));
        return true;
      }
    } elseif ($sTable == 'orders_items' ) {
      $oLogger = new logger($this->pDbMgr);
      $aValues = $this->pDbMgr->getChangedUpdateValues($aValues, $this->aOrderData);
      if (!empty($aValues)) {
        $aTMPValues = $aValues;
        $aTMPValues['where'] = $sWhere;
        $oLogger->addLog($this->iOrderId, $_SESSION['user']['id'], $aTMPValues);
        return $this->pDbMgr->Update('profit24', $sTable, $aValues, $sWhere);
      } else {
//        throw new \Exception(_('Dane nie zostały zmienione'));
        return true;
      }
    } else {
      throw new \Exception(_('Nie obsługiwana tabela'));
    }
  }
  
  /**
   * 
   * @param string $sMsgSymbol
   * @param bool $bIsErr
   */
  protected function setMessage($sMsgSymbol, $bIsErr = true) {
    
    if (isset($this->aLang[$sMsgSymbol])) {
      $sMsg = $this->aLang[$sMsgSymbol];
    } else {
      $sMsg = $sMsgSymbol;
    }
    $this->sMsg .= GetMessage($sMsg, $bIsErr);
    // dodanie informacji do logow
    AddLog($sMsg, $bIsErr);
  }
  
  /**
   * 
   * @param array $aPostOldOrder
   * @return array
   */
  private function validateOldVars($aPostOldOrder) {
    
    foreach($aPostOldOrder as $iOrderId => $aPostOldOrderItem) {
      if (is_numeric($iOrderId) && intval($iOrderId) > 0) {
        $aOrderItem = $this->getOrderItem($iOrderId);
        if ($aOrderItem['status'] != $aPostOldOrderItem['status'] ||
            $aOrderItem['quantity'] != $aPostOldOrderItem['quantity'] ||
            $aOrderItem['discount'] != \Common::formatPrice2($aPostOldOrderItem['discount'])
                ) {
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return array
   */
  private function getOrderItem($iOrderId) {
    
    $sSql = 'SELECT status, quantity, discount 
             FROM orders_items
             WHERE id = '.$iOrderId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
  
	/**
	 * Metoda wprowadza zmiany w pozycjach zamowienia
	 *
	 * @return	boolean
	 */
	public function UpdateItems() {
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			throw new Exception('Nie psuj, pusty formularz');
		}
    
    if ($this->validateOldVars($_POST['oldVals']) === false) {
        $this->setMessage(_('Uwaga przesłany formularz zawiera nieaktualne dane. <br />Zamówienie mogło zostać zmienione przez automat lub inną osobę. <br />Spróbuj ponownie.'), true);
        return true;
    }
    
    if ($this->ValidateTransportQuantityWeightDiscount($_POST['editable']) === true)  {
      $this->pDbMgr->BeginTransaction('profit24');

      $reservationManager = new ReservationManager($this->pDbMgr);
      $beforeReservation = $reservationManager->getDataProvider()->getReservationsByOrderId($this->iOrderId);

      if($this->oModuleZamowienia->recountOrder($this->iOrderId)===false){
        $bIsErr = true;
      }

      if ($this->setOrderNumberIfEmpty() === false) {
        $bIsErr = true;
      }
      
      if ($this->setLockedAutoOrder() === false) {
        $bIsErr = true;
      }

      if($this->oModuleZamowienia->recountOrder($this->iOrderId)===false){
          $bIsErr = true;
      }

        // Streamsoft rezerwacje
        if(false == $bIsErr){
            try{
                $reservationResult = $reservationManager->createStreamsoftReservation($beforeReservation, $this->iOrderId);

                // zamowienie czesciowe, wyswielamy info
                if(false === $reservationResult){
                    $this->sMsg .= GetMessage($reservationManager->getErrorMessage());
                }

            } catch(\Exception $e) {
                $bIsErr = true;
                $this->sMsg .= GetMessage($e->getMessage());
            }
        }
      
      if ($bIsErr) {
        echo mysql_error();
        $this->pDbMgr->RollbackTransaction('profit24');
        $reservationManager->rollbackChanges($this->iOrderId);
        $this->setMessage('order_items_edit_err', true);
        return false;
      }
      else {
        $this->pDbMgr->CommitTransaction('profit24');
        $this->setMessage('order_items_edit_ok', false);
        return true;
      }
    }
    return true;
	} // end of UpdateItems() function
  
  
  /**
   * 
   * @return boolean
   */
  private function setLockedAutoOrder() {
    
    if ($this->aOrderData['locked_auto_order'] == '0') {
      $aLockedValues = array('locked_auto_order' => '1');
      if($this->Update('orders', $aLockedValues, 'id = '.$this->iOrderId)===false){
        return false;
      }
    }
    return true;
  }
  
  
  /**
   * 
   * @param int $iOrderId
   * @return boolean
   */
  private function setOrderNumberIfEmpty() {

    if($this->aOrderData['order_number'] == '') {
      if(($sOrderNumber = $this->genOrderNumber()) === false){
        return false;
      }
      $aValues['order_number'] = $sOrderNumber;
      if($this->Update('orders', $aValues, 'id = '.$this->iOrderId)===false){
        return false;
      }
    }
    return true;
  }
  
  
	/**
	 * Pobiera i generuje kolejny nr zamówienia
   * 
	 * @return string - nr zamówienia
	 */
	private function genOrderNumber() {
		
		$sSql="SELECT * FROM orders_numbering FOR UPDATE";
		$aNumber = $this->pDbMgr->GetRow('profit24', $sSql);
    
		if (!empty($aNumber) && $aNumber['number_date'] == date('Ymd')) {
				$sSql = "UPDATE orders_numbering SET id=id+1";
				if ($this->pDbMgr->Query('profit24', $sSql)===false){
					 //Common::Query("UNLOCK TABLES");
					return false;
				} else{
					//Common::Query("UNLOCK TABLES");
          return sprintf("24%02d%02d%04d%02d", date('m'), date('d'), $aNumber['id'], substr(date('Y'), -2));
				}
		} else {
      // czyścimy wszystko z tabeli numeracji, pierwsze zamówienie w tym dniu
      $sSql = "DELETE FROM orders_numbering";
      @$this->pDbMgr->Query('profit24', $sSql);
      
			$aValues = array(
				'id' => 2,
        'number_date' => date('Ymd'),
        'number_year' => date('Y'),
			);
			if ($this->pDbMgr->Insert('profit24', "orders_numbering", $aValues,'',false) === false) {
	 				return false;
			} else {
        return sprintf("24%02d%02d%04d%02d", date('m'), date('d'), 1, substr(date('Y'), -2));
			}
		}
	} // end of getOrderNumber() method
  
  
  /**
   * 
   * @param array $aPostDataEditable
   * @return boolean
   */
  protected function ValidateTransportQuantityWeightDiscount($aPostDataEditable) {
		// walidacja
		$sErr = '';
		
		if(!empty($aPostDataEditable)){
			foreach($aPostDataEditable as $sKey=>&$aElement){
				if($sKey == 'transport'){
					$aElement['transport_value_brutto'] = trim($aElement['transport_value_brutto']);
					if ($aElement['transport_value_brutto'] != "" && (!preg_match('/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/',$aElement['transport_value_brutto']) || $aElement['transport_value_brutto'] < 0)) {
						$sErr .= "<li>Koszt transportu</li>";
					}
				} else {
					$aElement['quantity'] = trim($aElement['quantity']);
					$aElement['discount'] = trim($aElement['discount']);
					$aElement['weight'] = trim($aElement['weight']);
					if ($aElement['quantity'] != "" && (!preg_match('/^\d+$/',$aElement['quantity']) || $aElement['quantity'] <= 0)) {
							$sErr .= "<li>Sztuk</li>";
					}
					// jeśli pakiet to nie walidujemy tego pola
					if ($this->isPacket($sKey) != true && ($aElement['weight'] != "" && (!preg_match('/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/',$aElement['weight']) || $aElement['weight'] < 0 || $aElement['weight'] > 40.0))) {
							$sErr .= "<li>Waga</li>";
					}
					if ($aElement['discount'] != "" && (!preg_match('/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/',$aElement['discount']) || $aElement['discount'] < 0 || $aElement['discount'] >= 100)) {
							$sErr .= "<li>Rabat</li>";
					}
				}
			}
      
			if(!empty($sErr)){
				$sErr = $this->_aConfig['lang']['form']['error_prefix'].
					'<ul class="formError">'.$sErr.'</ul>'.
					$this->_aConfig['lang']['form']['error_postfix'];
        
				$this->setMessage($sErr);
        return false;
			}
		}
    return true;
  }
  
  /**
	 * Metoda sprawdza czy element jest pakietem
	 *
	 * @global array $aConfig
	 * @param type $iItemId 
	 */
	private function isPacket($iItemId) {
		global $aConfig;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE packet = '1' AND id=".$iItemId;
		return $this->pDbMgr->GetOne('profit24', $sSql)>0;
	}// end of isPacket() method
  
  
  /**
   * 
   * @return bool
   */
  private function removeInvoice() {
    
    $fName = 'faktura'.str_replace("/","_",$this->aOrderData['invoice_id']).'.pdf';
    $sFile = $this->_aConfig['common']['base_path'].$this->_aConfig['common']['invoice_dir'].$fName;
    if(file_exists ($sFile)) {
      return unlink($sFile);//usunięcie faktury z dysku
    }
  }
  
  
  /**
   * 
   * @return bool
   */
  protected function removeProforma() {
    
    $fName = 'faktura_proforma'.str_replace("/", "_", $this->aOrderData['order_number']).'.pdf';
    $sFile = $this->_aConfig['common']['client_base_path'].$this->_aConfig['common']['pro_forma_invoice_dir'].$fName;
    if(file_exists($sFile)){
      @unlink($sFile);
    }
  }
  
  
  /**
	 * Metoda zmienia stan Korekcja dla zamówienia
	 *
	 */
	public function EnableSecondPayment() {
		
    $aValues = array(
        'second_payment_enabled' => '1'
    );
		if ($this->Update('orders', $aValues, ' id = '.$this->iOrderId.' LIMIT 1') === false) {
      $sMsg = sprintf($this->aLang['enable_second_payment_err'], $this->aOrderData['order_number']);
      $this->setMessage($sMsg, true);
      return false;
		} else {
      $sMsg = sprintf($this->aLang['enable_second_payment_enabled'], $this->aOrderData['order_number']);
      $this->setMessage($sMsg, false);
      return true;
		}
	} // end of changeCorrection() method
  
  
	/**
	 * Metoda zmienia stan Korekcja dla zamówienia
	 *
	 */
	public function changeCorrection() {
		
    $aValues = array(
        'correction' => ($this->aOrderData['correction'] == '1' ? '0' : '1')
    );
		if ($this->Update('orders', $aValues, ' id = '.$this->iOrderId.' LIMIT 1') === false) {
      $sMsg = sprintf($this->aLang['correction_err'], $this->aOrderData['order_number']);
      $this->setMessage($sMsg, true);
      return false;
		} else {
      $sMsg = sprintf($this->aLang['correction_ok'], $this->aOrderData['order_number']);
      $this->setMessage($sMsg, false);
      return true;
		}
	} // end of changeCorrection() method
  
  
	/**
	 * Włącza / wyłącza 2 fakturę
   * 
	 * @return boolean
	 */
	public function toggleOrderInvoice(){
		
		if($this->aOrderData['second_invoice'] == '1'){
			$aValues['second_invoice'] = '0';
		} else {
			$aValues['second_invoice'] = '1';
		}
    
		$bIsErr = false;
		if($this->Update("orders", $aValues, "id =" . $this->iOrderId) === false){
			$bIsErr = true;
		}
    
    if ($aValues['second_invoice'] == '0') {
      // jeśli wywalamy drugą FV, to wszystko ma zostać przepisane na 1-szą FV
      if ($bIsErr === false) {
        if($this->Update("orders_items", $aValues, " order_id = " . $this->iOrderId) === false){
          $bIsErr = true;
        }
      }
    }
    
    $this->removeProforma();
		
		if ($bIsErr) {
      $this->setMessage('toggle_invoice_err_'.intval($this->aOrderData['second_invoice']), false);
      return false;
		}
		else {
      $this->setMessage('toggle_invoice_ok_'.intval($this->aOrderData['second_invoice']), false);
      return true;
		}
	} // end of toggleOrderInvoice() method
}
