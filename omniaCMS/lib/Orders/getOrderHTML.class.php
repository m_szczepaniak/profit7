<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace omniaCMS\lib\Orders;

use Common;
use DatabaseManager;
use orders\Shipment\ShipmentTime;
use Smarty;

/**
 * Description of printOrder
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class getOrderHTML {
  
  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;
  
  /**
   * @param DatabaseManager $pDbMgr
   * @param Smarty $pSmarty
   */
  public function __construct($pDbMgr, $pSmarty) {
    $this->pSmarty = $pSmarty;
    $this->pDbMgr = $pDbMgr;
  }
  
  

	/**
	 * Metoda generuje html dla wydruku pojedynczego zamówienia
   * 
	 * @param int $iId - id zamówienia
	 * @return string - html
	 */
	public function getOrderHtml($iId){
	
		$aModule = array();
		$oOrderItem = new OrderItem($this->pDbMgr);
    $oOrderItem->setOrder($iId);
    
    $aModule['order'] = $oOrderItem->oOrder->getSelOrderData(array(' * '));
    $aRecords = $oOrderItem->getSelOrdersItemsByOrderId('I');
    
		$i = 1;
		$iCount=0;
		foreach($aRecords as $aItem) {
			$aTemp = array();
			$aTemp['id'] = $aItem['id'];
			$aTemp['lp'] = ($i++).'.';
			$aTemp['name'] = '<span'.($aItem['deleted']=='1'?' style="color: gray; "':($aItem['added_by_employee']==1?' style="color:brown;"':'')).'><strong>'.$aItem['name'].'</strong>'.
											($aItem['isbn']?'<br/>'.$aItem['isbn']:'').
											($aItem['publication_year']?'<br/>'.$aItem['publication_year']:'').($aItem['edition']?' wyd. '.$aItem['edition']:'').
											($aItem['publisher']?'<br/>'.$aItem['publisher']:'').
											($aItem['authors']?'<br/>'.$aItem['authors']:'').'</span>';
			$sPrefix = '';
			$aTemp['status'] = $this->aLang['items_status_'.$aItem['status']];
			$aTemp['weight'] = Common::formatPrice($aItem['weight']);
			// liczba
			$aTemp['quantity'] = $aItem['quantity'];
			if($aItem['preview']=='1'){
				$aTemp['shipment'] = formatDateClient($aItem['shipment_date']);
			} else {
			    $pShipmentTime = new ShipmentTime();
				$aTemp['shipment'] = $pShipmentTime->getShipmentTime($aItem['id'], $aItem['shipment_time']);
			}
			
			// formatowanie cen
			if ($aItem['promo_price_brutto'] > 0) {
				$sPrefix = '<br/>';
				// vat
				$aTemp['vat'] = Common::formatPrice3($aItem['vat']).'<span style="color: gray">%</span>';
				// cena brutto
				$aTemp['price_brutto'] = '<span style="text-decoration: line-through;">'.Common::formatPrice($aItem['price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span></span><br/>'.Common::formatPrice($aItem['promo_price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
			}
			else {
				// vat
				$aTemp['vat'] = Common::formatPrice3($aItem['vat']).'<span style="color: gray">%</span>';
				// cena brutto
				$aTemp['price_brutto'] = Common::formatPrice($aItem['price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
			}
			
			if ($aItem['attachments'] == '1') {
				$aTempPacket = array();
				// produkt posiada zalaczniki - pobranie ich
				$sSql = "SELECT  *
								 FROM ".$aConfig['tabls']['prefix']."orders_items
								 WHERE order_id = ".$iId." AND
								 			 parent_id = ".$aItem['id']." AND
											 item_type='A'
								 ORDER BY id";
				$aAttach =& Common::GetAll($sSql);
				
				$aTempPacket['name'] .= '<span style="color: gray;"><strong>'.$this->aLang['attachments'].'</strong><ul>';

				
				$aTemp['price_brutto'] .= '<br/><br/>';
				
				foreach ($aAttach as $iAtId => $aAtt) {
						$aTempPacket['name'] .= '<li>'.$aAtt['name'].($aAtt['exact_code'] != '' ? ' - '.$this->aLang['exact_code'].'<strong>'.$aAtt['exact_code'].'</strong>' : '').'</li>';
						$aTempPacket['vat'] = '<span style="color: gray">'.Common::formatPrice3($aAtt['vat']).'%</span>';
						
						if ($aAtt['promo_price_brutto'] > 0) {
							$aTempPacket['price_brutto'] = '<span style="text-decoration: line-through; color: gray;">'.Common::formatPrice($aAtt['price_brutto']).$aConfig['lang']['common']['currency'].'</span><br/>
																				<span style="color: gray;">'.Common::formatPrice($aAtt['promo_price_brutto']).$aConfig['lang']['common']['currency'].'</span>';
						} else {
							$aTempPacket['price_brutto'] = '<span style="color: gray">'.Common::formatPrice($aAtt['price_brutto']).$aConfig['lang']['common']['currency'].'</span>';
						}
						
						$aTempPacket['discount'] = Common::formatPrice3($aAtt['discount']);
						$aTempPacket['quantity'] = $aAtt['quntity'];
						$aTempPacket['value_brutto'] = ' <span style="color: gray">'.$sPrefix.Common::formatPrice($aAtt['value_brutto']).$aConfig['lang']['common']['currency'].'</span>';
				}
				$aTempPacket['name'] .= '</ul></span>';
			}
			
			// rabat
			if ($aItem['discount'] > 0) {
				$aTemp['discount'] = Common::formatPrice3($aItem['discount']);
			}
			else {
				$aTemp['discount'] = '0';
			}

			// formatowanie wartosci
			if ($aItem['attachments'] == '1') {
				$aTemp['value_brutto'] = $sPrefix.Common::formatPrice($aItem['bundle_value_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
			} else {
				$aTemp['value_brutto'] = $sPrefix.Common::formatPrice($aItem['value_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
			}
			
			$aModule['items'][$iCount] = $aTemp;
			if (!empty($aTempPacket)) {
				$iCount ++;
				$aModule['items'][$iCount] = $aTempPacket;
				unset($aTempPacket);
			}
			
			if ($aItem['packet'] == '1') {
				
			// pobranie danych skladowych pakietu
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."orders_items
							 WHERE order_id = ".$iId." AND
							 			 parent_id = ".$aItem['id']." AND
							 			 item_type = 'P'
							 ORDER BY id";
			$aPacketItems =& Common::GetAll($sSql);	

			foreach ($aPacketItems as $aPItem) {

				$aTemp = array();
				$aTemp['id'] = $aPItem['id'];
				$aTemp['lp'] = '';
				$aTemp['name'] = '<span style="color: gray;"><strong>'.$aPItem['name'].'</strong></span>';
				$sPrefix = '';


				if ($aPItem['attachments'] == '1') {
					$aTempPacket = array();
					// produkt posiada zalaczniki - pobranie ich
					$sSql = "SELECT *
									 FROM ".$aConfig['tabls']['prefix']."orders_items
									 WHERE order_id = ".$iId." AND
												 parent_id = ".$aPItem['id']." AND
												 item_type = 'A'
									 ORDER BY id";
					$aAttach =& Common::GetAll($sSql);
					$aTempPacket['name'] .= '<span style="color: gray;"><strong>'.$this->aLang['attachments'].'</strong><ul>';

					foreach ($aAttach as $iAtId => $aAtt) {
						$aTempPacket['name'] .= '<li>'.$aAtt['name'].($aAtt['exact_code'] != '' ? ' - '.$this->aLang['exact_code'].'<strong>'.$aAtt['exact_code'].'</strong>' : '').'</li>';
						
						$aTempPacket['vat'] = '<span style="color: gray">'.Common::formatPrice3($aAtt['vat']).'%</span>';
						
						if ($aAtt['promo_price_brutto'] > 0) {
							$aTempPacket['price_brutto'] = '<span style="text-decoration: line-through; color: gray;">'.Common::formatPrice($aAtt['price_brutto']).$aConfig['lang']['common']['currency'].'</span><br/>
																				<span style="color: gray;">'.Common::formatPrice($aAtt['promo_price_brutto']).$aConfig['lang']['common']['currency'].'</span>';
						} else {
							$aTempPacket['price_brutto'] = '<span style="color: gray">'.Common::formatPrice($aAtt['price_brutto']).$aConfig['lang']['common']['currency'].'</span>';
						}
						
						$aTempPacket['discount'] = Common::formatPrice3($aAtt['discount']);
						$aTempPacket['quantity'] = $aAtt['quntity'];
						$aTempPacket['value_brutto'] = ' <span style="color: gray">'.$sPrefix.Common::formatPrice($aAtt['value_brutto']).$aConfig['lang']['common']['currency'].'</span>';
					}

					$aTempPacket['name'] .= '</ul></span>';
				}
				$aTemp['status'] = '&nbsp;';

				$aTemp['weight'] = Common::formatPrice($aPItem['weight']);
				// liczba
				$aTemp['quantity'] = $aPItem['quntity'];
				$aTemp['shipment'] = '&nbsp;';

				// formatowanie cen
				if ($aPItem['promo_price_brutto'] > 0) {
					$sPrefix = '<br/>';
					// vat
					$aTemp['vat'] = '<span style="color: gray">'.Common::formatPrice3($aPItem['vat']).'%</span>';
					// cena brutto
					$aTemp['price_brutto'] = '<span style="text-decoration: line-through; color: gray;">'.Common::formatPrice($aPItem['price_brutto']).$aConfig['lang']['common']['currency'].'</span><br/><span style="color: gray;">'.Common::formatPrice($aPItem['promo_price_brutto']).$aConfig['lang']['common']['currency'].'</span>';
				}
				else {
					// vat
					$aTemp['vat'] = '<span style="color: gray">'.Common::formatPrice3($aPItem['vat']).'%</span>';
					// cena brutto
					$aTemp['price_brutto'] = '<span style="color: gray">'.Common::formatPrice($aPItem['price_brutto']).$aConfig['lang']['common']['currency'].'</span>';
				}
				// rabat
				if ($aPItem['discount'] > 0) {
					$aTemp['discount'] = Common::formatPrice3($aPItem['discount']);
					//		$aTemp['discount_currency'] = Common::formatPrice($aPItem['discount_currency']).$aConfig['lang']['common']['currency'].'</span>';
				}
				else {
					$aTemp['discount'] = '&nbsp;';
					//	$aTemp['discount_currency'] = '&nbsp;';
				}

				// formatowanie wartosci
				$aTemp['value_brutto'] = ' <span style="color: gray">'.$sPrefix.Common::formatPrice($aPItem['value_brutto']).$aConfig['lang']['common']['currency'].'</span>';

				$iCount ++;
				$aModule['items'][$iCount] = $aTemp;
				if (!empty($aTempPacket)) {
					$iCount ++;
					$aModule['items'][$iCount] = $aTempPacket;
					unset($aTempPacket);
				}
			}
			
			$iCount ++;
			
			} else {
				$iCount++;
			}
		}
		
		$aModule['order']['total_value_transport'] = $aModule['order']['value_brutto'] + $aModule['order']['transport_cost'];
		
		// dane zamawiającego
		$aAddresses = $this->getOrderAddresses($iId);	
		$aModule['invoice_data'] = $this->_formatUserData($aAddresses[0]);
		$aModule['transport_data'] = $this->_formatUserData($aAddresses[1]);
		$aModule['print_date'] = date('d.m.Y');
		
		if($aModule['order']['second_invoice'] == '1'){
			$aModule['invoice2_data'] = $this->_formatUserData($aAddresses[2]);
		} else {
			$aModule['invoice2_data'] = '&nbsp;';
		}

		// źródło zamówienia
		if($aModule['order']['source'] != ''){
			$aModule['order']['source'] .= ($aModule['order']['source'] == 'newsletter' ? ' ('.getNewsletter((double) $aModule['order']['source_id']).')' : '');
		}
		
		$aModule['order']['order_status_txt'] = $this->aLang['status_'.$aModule['order']['order_status']];
		$aModule['order']['payment_status_txt'] = $this->aLang['payment_status_'.$aModule['order']['payment_status']];
		
		$this->pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $this->pSmarty->fetch('order_print.tpl');
		$this->pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	} // end of getOrderHtml() method
  
  
	/**
	 * Metoda formatuje dane zamawiajacego
	 *
	 * @param	array	ref	$aOrder	- dane zamowienia
	 * @return	string	- sformatowana dane uzytkownika
	 */
	private function _formatUserData(&$aAddress) {
    
		$sData = '';
		if($aAddress['is_company'] == '1'){
			$sData = !empty($aAddress['company']) ? $aAddress['company']."<br/>" : '';
			$sData .= !empty($aAddress['nip']) ? $this->aLang['order_nip'].' '.$aAddress['nip']."<br/>" : '';
			$sData .= $aAddress['street'].' '.$aAddress['number'].
						 (!empty($aAddress['number2']) ? '/'.$aAddress['number2'] : '' )."<br/>";
			$sData .= ($aAddress['is_postal_mismatch']=='1'?'<span style="color: red;">':'').$aAddress['postal'].' '.$aAddress['city'].($aAddress['is_postal_mismatch']=='1'?'</span>':'')."<br/>";
			$sData .= $aAddress['name'].' '.$aAddress['surname']."<br/>";
			$sData .= $aAddress['phone'] != '' ? $this->aLang['phone'].' '.$aAddress['phone']."<br/>" : '';
		} else {
			$sData = !empty($aAddress['company']) ? $aAddress['company']."<br/>" : '';
			$sData .= !empty($aAddress['nip']) ? 'Numer ewidencyjny '.$aAddress['nip']."<br/>" : '';
			$sData .= $aAddress['name'].' '.$aAddress['surname']."<br/>";
			$sData .= $aAddress['street'].' '.$aAddress['number'].
						 (!empty($aAddress['number2']) ? '/'.$aAddress['number2'] : '' )."<br/>";
			$sData .= ($aAddress['is_postal_mismatch']=='1'?'<span style="color: red;">':'').$aAddress['postal'].' '.$aAddress['city'].($aAddress['is_postal_mismatch']=='1'?'</span>':'')."<br/>";
			$sData .= $aAddress['phone'] != '' ? $this->aLang['phone'].' '.$aAddress['phone']."<br/>" : '';
		}		
		return $sData;
	} // end of __formatUserData() method
  
  
	/**
	 * Pobiera dane adresowe z zamówienai z bazy
	 * 
	 * @param int $iId - id adresu
	 * @return array ref
	 */
	private function getOrderAddresses($iId, $bIgnoreTransportAdress = false){
		global $aConfig;

		$sSql = "SELECT address_type, is_company, company, name, surname, street, number, number2, postal, city, nip, phone, is_postal_mismatch
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
            ($bIgnoreTransportAdress === TRUE ? ' AND address_type="0"' : '').
						 " ORDER BY address_type"; 
		return Common::GetAssoc($sSql);
	} // end of getOrderAddresses() method
}
