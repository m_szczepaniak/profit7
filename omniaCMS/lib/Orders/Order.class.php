<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\lib\Orders;

/**
 * Description of Order
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Order {

  private $iOrderId;
  private $pDbMgr;

  public function __construct($pDbMgr, $iOrderId) {
    $this->pDbMgr = $pDbMgr;
    $this->iOrderId = $iOrderId;
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param array $aCols
   * @return array
   */
  public function getSelOrderData($aCols) {
    
    $sSql = 'SELECT '.implode(', ', $aCols).'
             FROM orders
             WHERE id = '.$this->iOrderId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
}