<?php

use orders\Shipment;
/**
 * Klasa szczegółów zamawiającego
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-24 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class orderUserDetails {

  /**
   * @var DatabaseManager $pDbMgr
   */
  public $pDbMgr;
  

  protected $aConfig;
  
  protected $aLang;
  
  protected $sModule;
  
  function __construct($pDbMgr, $aConfig, $sModule) {
    $this->pDbMgr = $pDbMgr;
    $this->aConfig = $aConfig;
    $this->sModule = $sModule;
    $this->aLang = $aConfig['lang'][$sModule];
  }


	/**
	 * Metoda wyswietla dane zamwiajacego
	 *
	 * @param $aOrder
	 * @param $aAddresses
	 * @param $sTransportSymbol
	 * @return string
	 */
	public function getUserDetails(&$aOrder, &$aAddresses, $sTransportSymbol) {

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'form'	=>	false,
			'checkboxes'	=>	false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
		array(
				'content'	=> $this->aLang['user_data_invoice'],
				'width' => '33%'
			),
			array(
				'content'	=> $this->aLang['user_data_delivery'],
				'width' => '33%'
			),
			array(
				'content'	=> $this->aLang['user_data_invoice2'],
				'width' => '33%'
			),
            array(
                'content'	=> _('Dane odbiorcy'),
                'width' => '33%'
            )
		);
    
    // wszystkie zamówienia dane płatnika
		$aTemp['invoice'] = $this->_formatUserData($aAddresses[0]);
    if ($sTransportSymbol == 'opek-przesylka-kurierska' || $sTransportSymbol == 'poczta-polska-doreczenie-pod-adres') {
      // jeśli opek to wyświetl dane dostawy
      $aTemp['transport'] = $this->_formatUserData($aAddresses[1]);
    } elseif ($sTransportSymbol == 'tba') {
      // jeśli TBA, sprawdzamy, czy kod pocztowy znajduje się w obszarze doręczeń
      // jeśli jest poza obszarem, przekazujemy do FormatUserData adres z parametrem is_postal_mismatch = 1
      $bTestMode = $this->aConfig['common']['status'] == 'development' ? true : false;
      $oShipment = new Shipment($aOrder['transport_id'], $this->pDbMgr, $bTestMode);
      $bValidatePostcode = $oShipment->checkDestinationPointAvailable($aAddresses[1]['postal']);      

      if (!$bValidatePostcode) {
        $aAddresses[1]['is_postal_mismatch'] = 1;
      }
      
      // wyświetl dane dostawy
      $aTemp['transport'] = $this->_formatUserData($aAddresses[1]);
    } elseif ($sTransportSymbol == 'paczkomaty_24_7') {
      
      // dane paczkomatu
			$aTemp['transport'] = $this->_formatUserDataPaczkomaty($aOrder['point_of_receipt'], $aOrder['phone_paczkomaty']);
		} elseif ($sTransportSymbol == 'poczta_polska') {
      
      // dane punktu odbioru Poczty Polskiej
			$aTemp['transport'] = $this->_formatUserDataOdbiorWPunkcie($aOrder['point_of_receipt'], $aAddresses[1]);
		} elseif ($sTransportSymbol == 'ruch') {
      
       // dane punktu odbioru Ruch 
			$aTemp['transport'] = $this->_formatUserDataPaczkaWRuchu($aOrder['point_of_receipt'], $aAddresses[1]);
		} elseif ($sTransportSymbol == 'orlen') {
      
       // dane punktu odbioru Orlen 
			$aTemp['transport'] = $this->_formatUserDataOrlen($aOrder['point_of_receipt'], $aAddresses[1]);
		} elseif($sTransportSymbol === 'odbior-osobisty') {
      // odbiór osobisty, brak kol z dostawą
      unset($aTemp['transport']);
			unset($aRecordsHeader[1]);
		}    
    
    
		
		if($aOrder['second_invoice'] == '1'){
			$aTemp['invoice2'] = $this->_formatUserData($aAddresses[2]);
		}else {
      unset($aRecordsHeader[2]);
		}

        if($aOrder['invoice_recipient'] != ''){
            $aTemp['invoice_recipient'] = nl2br($aOrder['invoice_recipient']);
        }

		$pView = new View('user_details', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		$aRecords[] = $aTemp;

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array();
		// dodanie rekordow do widoku
		$pView->AddRecords($aRecords, $aColSettings);

		return $pView->Show();
	} // end of getUserDetails() function
  
  
	/**
   * Formularz adresów użytkownika
   * 
   * @deprecated since version 0.01
   * @global array $aConfig
   * @param array $aOrder
   * @param array $aAddresses
   * @param object $pForm
   * @param string $sTransportSymbol
   * @return string HTML
   */
	public function getUserDetailsForms(&$aOrder, &$aAddresses, $pForm, $sTransportSymbol) {

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'form'	=>	false,
			'checkboxes'	=>	false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
		array(
				'content'	=> $this->aLang['user_data_invoice'],
				'width' => '33%'
			),
			array(
				'content'	=> $this->aLang['user_data_delivery'],
				'width' => '33%'
			),
			array(
				'content'	=> $this->aLang['user_data_invoice2'],
				'width' => '33%'
			),
            array(
                'content'	=> _('Dane odbiorcy'),
                'width' => '33%'
            )
		);

		$aTemp['invoice'] = $this->_getUserDetailsForm(0,$aAddresses[0],$pForm, 'invoice');
    if ($sTransportSymbol == 'opek-przesylka-kurierska' || $sTransportSymbol == 'poczta-polska-doreczenie-pod-adres' || $sTransportSymbol == 'poczta_polska' || $sTransportSymbol == 'ruch' ||
      $sTransportSymbol == 'orlen' || $sTransportSymbol == 'tba') {
      $aTemp['transport'] = $this->_getUserDetailsForm(1,$aAddresses[1],$pForm, 'transport');
    } elseif($sTransportSymbol === 'paczkomaty_24_7') {
      $aTemp['transport'] = $this->_getPaczkomatyPhoneForm($pForm, 1, $aOrder['phone_paczkomaty']);
    } elseif($sTransportSymbol === 'odbior-osobisty') {
      unset($aRecordsHeader[1]);
    }
		
		if($aOrder['second_invoice'] == '1'){
			$aTemp['invoice2'] = $this->_getUserDetailsForm(2,$aAddresses[2],$pForm, 'invoice2');
		}else {
            unset($aRecordsHeader[2]);
		}

        $aTemp['user_data_invoice_recipient'] = $this->_getInvoiceRecipient($pForm, $aOrder['invoice_recipient']);
    
		$pView = new View('user_details', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		

		
		$aRecords[] = $aTemp;

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array();
		// dodanie rekordow do widoku
		$pView->AddRecords($aRecords, $aColSettings);

		return $pView->Show();
	} // end of getUserDetailsForms() function


    /**
     * @param FormTable $oForm
     * @param $invoiceReciptint
     * @return string
     */
    private function _getInvoiceRecipient(\FormTable $oForm, $invoiceReciptint) {
        $sFormHtml = $oForm->GetLabelHTML('invoice_recipient', _('Dane odbiorcy'), false).
            $oForm->GetTextAreaHTML('invoice_recipient', _('Dane odbiorcy'), $invoiceReciptint, array('rows' => '10', 'style'=>'width:100%;height:100%'), '', 'text', '', false);
        return $sFormHtml;
    }
  
  
	/**
	 * Metoda wyswietla formularz edycji danych zamwiajacego
	 *
	 * @param		object	$pSmarty
	 * @param	array ref	$aOrder	- dane zamowienia
	 * @param FormTable $oForm - formularz
	 * @return	string - html
	 */
	private function _getUserDetailsForm($iNr, &$aAddress, &$oForm, $sType) {

		$sFormHtml = '<div id="addr'.$iNr.'_grp">';
		$aType = array(
			array('value' => '0','label' => $this->aLang['is_company_0'],'id'=>'addr_'.$iNr.'_is_company_0','onclick'=>'toggleAddresType('.$iNr.',0)'),
			array('value' => '1','label' => $this->aLang['is_company_1'],'id'=>'addr_'.$iNr.'_is_company_1','onclick'=>'toggleAddresType('.$iNr.',1)')
		);
		
		$aCorporateAttribs = array(
			'class' => 'text corp addr_'.$iNr
		);
		if(!isset($aAddress['is_company'])){
			$aAddress['is_company'] = '0';
		}
		
		if(isset($_POST['is_company_'.$iNr])){
			$aAddress['is_company'] = $_POST['is_company_'.$iNr];
		}
		
		if($aAddress['is_company'] != '1'){
			//$aCorporateAttribs['disabled'] = 'disabled';
		} 
			
		$sFormHtml .= $oForm->GetLabelHTML('is_company_'.$iNr, $this->aLang['is_company'], true);
		$sFormHtml .= $oForm->GetRadioSetHTML('is_company_'.$iNr, $this->aLang['is_company'], $aType, (isset($_POST['is_company_'.$iNr])?$_POST['is_company_'.$iNr]:$aAddress['is_company']), '', true, false);
		$sFormHtml .= '<br /><div style="float: left;width: 90px;">';
		$sFormHtml .= $oForm->GetLabelHTML('company_'.$iNr, $this->aLang['invoice_name'], ($aAddress['is_company'] == '1')).
										'</div>'.
										$oForm->GetTextHTML('company_'.$iNr, $this->aLang['invoice_name'], (isset($_POST['company_'.$iNr])?$_POST['company_'.$iNr]:$aAddress['company']), array_merge($aCorporateAttribs,array('maxlength' => 255,'style'=>'width:250px;')), '', 'text', $aAddress['is_company'], '', array(), 'is_company_'.$iNr,'1','addr_'.$iNr.'_is_company_1');
		$sFormHtml .= '<br /><div style="float: left;width: 90px;">';
		$sFormHtml .=  $oForm->GetLabelHTML('nip_'.$iNr, $this->aLang['nip'],false).
												"</div>".
												$oForm->GetTextHTML('nip_'.$iNr, $this->aLang['nip'], (isset($_POST['nip_'.$iNr])?$_POST['nip_'.$iNr]:$aAddress['nip']), array_merge($aCorporateAttribs,array('maxlength' => 13,'style'=>'width:250px;')), '', '',false, '', array());
		$sFormHtml .= '<br /><div style="float: left;width: 90px;">';
		
		
		$sFormHtml .=  $oForm->GetLabelHTML('name_'.$iNr, $this->aLang['name'],false).
											"</div>".
											$oForm->GetTextHTML('name_'.$iNr, $this->aLang['name'], (isset($_POST['name_'.$iNr])?$_POST['name_'.$iNr]:$aAddress['name']), array('maxlength' => 32,'style'=>'width:250px;'), '', 'text',false);
		$sFormHtml .= '<br /><div style="float: left;width: 90px;">';
		$sFormHtml .= $oForm->GetLabelHTML('surname_'.$iNr, $this->aLang['surname'],false).
											"</div>".
											$oForm->GetTextHTML('surname_'.$iNr, $this->aLang['surname'], (isset($_POST['surname_'.$iNr])?$_POST['surname_'.$iNr]:$aAddress['surname']), array('maxlength' => 32,'style'=>'width:250px;'), '', 'text',false);
		$sFormHtml .= '<br /><div style="float: left;width: 90px;">';
		$sFormHtml .=  $oForm->GetLabelHTML('street_'.$iNr, $this->aLang['street']).
												"</div>".
												$oForm->GetTextHTML('street_'.$iNr, $this->aLang['street'], (isset($_POST['street_'.$iNr])?$_POST['street_'.$iNr]:$aAddress['street']), array('maxlength' => 64,'style'=>'width:250px;'), '', 'text');
		$sFormHtml .= '<br /><div style="float: left;width: 90px;">';
		$sFormHtml .= $oForm->GetLabelHTML('number_'.$iNr, $this->aLang['number'], true).
												"</div>".
												$oForm->GetTextHTML('number_'.$iNr, $this->aLang['number'], (isset($_POST['number_'.$iNr])?$_POST['number_'.$iNr]:$aAddress['number']), array('maxlength' => 8,'style'=>'width:93px;'), '', 'text', true).
												"&nbsp;".
												$oForm->GetLabelHTML('number2_'.$iNr, $this->aLang['number2'], false).
												"&nbsp;".
												$oForm->GetTextHTML('number2_'.$iNr, $this->aLang['number2'], (isset($_POST['number2_'.$iNr])?$_POST['number2_'.$iNr]:$aAddress['number2']), array('maxlength' => 8,'style'=>'width:94px;'), '', 'text', false);
		$sFormHtml .= '<br /><div style="float: left;width: 90px;">';
    $mRequiredPostalType = 'text';
    if ($sType == 'transport') {
      $mRequiredPostalType = 'postal';
    }
		$sFormHtml .= $oForm->GetLabelHTML('postal_'.$iNr, $this->aLang['postal']).
												"</div>".
												$oForm->GetTextHTML('postal_'.$iNr, $this->aLang['postal'], (isset($_POST['postal_'.$iNr])?$_POST['postal_'.$iNr]:$aAddress['postal']), array('maxlength' => 10,'style'=>'width:250px;'), '', $mRequiredPostalType);
		$sFormHtml .= '<br /><div style="float: left;width: 90px;">';
		$sFormHtml .= $oForm->GetLabelHTML('city_'.$iNr, $this->aLang['city']).
												"</div>".
												$oForm->GetTextHTML('city_'.$iNr, $this->aLang['city'], (isset($_POST['city_'.$iNr])?$_POST['city_'.$iNr]:$aAddress['city']), array('maxlength' => 32,'style'=>'width:250px;'), '', 'text');
		$sFormHtml .= '<br /><div style="float: left;width: 90px;">';
		
		$bTransportRequired = false;
		if ($sType == 'transport') {
			$bTransportRequired = true;
		}
			$sFormHtml .= $oForm->GetLabelHTML('phone_'.$iNr, $this->aLang['phone'], $bTransportRequired).
													"</div>".
													$oForm->GetTextHTML('phone_'.$iNr, $this->aLang['phone'], (isset($_POST['phone_'.$iNr])?$_POST['phone_'.$iNr]:$aAddress['phone']), array('maxlength' => 32,'style'=>'width:250px;'), '', '', $bTransportRequired);

        $aAllowedToEdit = [
            'agolba',
            'kmalz',
            'mchudy'
        ];
        if (($sType == 'invoice' || $sType == 'invoice2') && (in_array($_SESSION['user']['name'], $aAllowedToEdit))) {
            $sFormHtml .= '<br /><div style="float: left;width: 90px;">';
            $sFormHtml .= $oForm->GetLabelHTML('streamsoft_id_' . $iNr, _('Streamsoft id'), false) .
                "</div>" .
                $oForm->GetTextHTML('streamsoft_id_' . $iNr, _('Streamsoft id'), (isset($_POST['streamsoft_id_' . $iNr]) ? $_POST['streamsoft_id_' . $iNr] : $aAddress['streamsoft_id']), array('maxlength' => 32, 'style' => 'width:250px;'), '', 'uint', false);
        }

		$sFormHtml .= '</div>';

		return $sFormHtml;
	} // end of _getUserDetailsForms() function
  
  
  /**
   * Formularz pobiera szczegóły danych telefonu paczkomatu
   * 
   * @global type $aConfig
   * @param object $oForm
   * @param int $iNr
   * @param string $sPaczkomatyPhone
   * @return string
   */
  private function _getPaczkomatyPhoneForm(&$oForm, $iNr, $sPaczkomatyPhone) {
    
    $bPaczkomatyPhoneRequired = true;
    
    $sFormHtml = '<div id="addr'.$iNr.'_grp">';
    $sFormHtml .= '<br /><div style="float: left;width: 130px;">';
    $sFormHtml .= $oForm->GetLabelHTML('phone_paczkomaty', $this->aLang['phone_paczkomaty'], $bPaczkomatyPhoneRequired).
                  "</div>".
                  $oForm->GetTextHTML('phone_paczkomaty', $this->aLang['phone_paczkomaty'], 
                                      (isset($_POST['phone_paczkomaty'])?$_POST['phone_paczkomaty'] : $sPaczkomatyPhone), 
                                      array('maxlength' => 32,'style'=>'width:80px;'), '', '', $bPaczkomatyPhoneRequired);
    $sFormHtml .= '</div>';
    return $sFormHtml;
  }// end of GetPaczkomatyPhoneForm() method
  
  
	/**
	 * Metoda formatuje dane zamawiajacego
	 *
	 * @param	array	ref	$aOrder	- dane zamowienia
	 * @return	string	- sformatowana dane uzytkownika
	 */
	private function _formatUserData(&$aAddress) {
    
		$sData = '';
		if($aAddress['is_company'] == '1'){
			$sData = !empty($aAddress['company']) ? $aAddress['company']."<br/>" : '';
			$sData .= !empty($aAddress['nip']) ? $this->aLang['order_nip'].' '.$aAddress['nip']."<br/>" : '';
			$sData .= $aAddress['street'].' '.$aAddress['number'].
						 (!empty($aAddress['number2']) ? '/'.$aAddress['number2'] : '' )."<br/>";
			$sData .= ($aAddress['is_postal_mismatch']=='1'?'<span style="color: red;">':'').$aAddress['postal'].' '.$aAddress['city'].($aAddress['is_postal_mismatch']=='1'?'</span>':'')."<br/>";
			$sData .= $aAddress['name'].' '.$aAddress['surname']."<br/>";
			$sData .= $aAddress['phone'] != '' ? $this->aLang['phone'].' '.$aAddress['phone']."<br/>" : '';
		} else {
			$sData = !empty($aAddress['company']) ? $aAddress['company']."<br/>" : '';
			$sData .= !empty($aAddress['nip']) ? 'Numer ewidencyjny '.$aAddress['nip']."<br/>" : '';
			$sData .= $aAddress['name'].' '.$aAddress['surname']."<br/>";
			$sData .= $aAddress['street'].' '.$aAddress['number'].
						 (!empty($aAddress['number2']) ? '/'.$aAddress['number2'] : '' )."<br/>";
			$sData .= ($aAddress['is_postal_mismatch']=='1'?'<span style="color: red;">':'').$aAddress['postal'].' '.$aAddress['city'].($aAddress['is_postal_mismatch']=='1'?'</span>':'')."<br/>";
			$sData .= $aAddress['phone'] != '' ? $this->aLang['phone'].' '.$aAddress['phone']."<br/>" : '';
		}
		if ($aAddress['streamsoft_id'] != '') {
            $sData .= $aAddress['streamsoft_id'] != '' ? _('Streamsoft Id: ').' '.$aAddress['streamsoft_id']."<br/>" : '';
        }
		return $sData;
	} // end of __formatUserData() method
  
	
	/**
	 * Metoda formatuje dane dostawy dla paczkomaty
	 *
	 * @global type $aConfig
	 * @param string $sPaczkomatCode
	 * @param string $sPaczkomatyPhone
	 * @return string 
	 */
	private function _formatUserDataPaczkomaty($sPaczkomatCode, $sPaczkomatyPhone) {
		
    $oShipment = new Shipment('paczkomaty_24_7', $this->pDbMgr);
		$machines_all = $oShipment->getDestinationPointsList();
		$machines_with_names_as_keys = array();
		$aPointOfRecipt = array();
		$sHTML = '';

		if (count($machines_all)) {
			foreach ($machines_all as $machine) {
				$machines_with_names_as_keys[$machine['name']] = $machine;
			}
		}

		if (!empty($machines_with_names_as_keys)) {
			foreach ($machines_with_names_as_keys as $machine) {
				if ($sPaczkomatCode == $machine['name']) {
					$aPointOfRecipt = $machine;
				}
			}
		}

		if (!empty($aPointOfRecipt) && is_array($aPointOfRecipt)) {
			$sHTML = $this->aLang['paczkomaty_point_of_recipt'].'<br />';
			$sHTML .= $aPointOfRecipt['town'].' <br />';
			$sHTML .= $aPointOfRecipt['street'].' '.$aPointOfRecipt['buildingnumber'].' <br />';
			if ($aPointOfRecipt['locationdescription'] != '') {
				$sHTML .= 'Opis punktu: '.$aPointOfRecipt['locationdescription'].' <br />';
			}
		} else {
			// brak zdefiniowanego paczkomatu
			$sHTML = '<strong><span style="color: red">Wybrany przez klienta paczkomat nie jest aktualnie dostępny, kod paczkomatu: '.$sPaczkomatCode.'</span></strong> <br />';
		}
		if ($sPaczkomatyPhone != '') {
			$sHTML .= 'Telefon dla InPost: '.$sPaczkomatyPhone;
		}

		return $sHTML;
	} // end of _formatUserDataPaczkomaty() method
  
  
  /**
	 * Metoda formatuje dane dostawy dla Poczta Polska Odbiór w punkcie
	 *
	 * @param string $sPunktOdbioruCode
	 * @param array $aAddress
	 * @return string
	 */
	function _formatUserDataOdbiorWPunkcie($sPunktOdbioruCode, $aAddress) {
    $sHTML = '';
    
    $oShipment = new Shipment('poczta_polska', $this->pDbMgr);
    $aPocztaDestinationPointAvailable = $oShipment->checkDestinationPointAvailable($sPunktOdbioruCode);
  
    if ($aPocztaDestinationPointAvailable) {
      
      $aPunktOdbioru = $oShipment->getSinglePointDetails($sPunktOdbioruCode);

      $city = $aPunktOdbioru['city'];
      $street = $aPunktOdbioru['street'];
      $hours = $aPunktOdbioru['hours'];

      if (!$aAddress['city'] || !$aAddress['postal']) {
        $sHTML .= _('<strong><span style="color: red">Zamówienie nie zawiera wymaganych przez Pocztę Polską danych osoby odbierającej przesyłkę (tj, adres, telefon)</span></strong> <br /><br />');
      } else { 
        if (!$this->checkCityPostal($aAddress['city'],$aAddress['postal'])) {
          $sHTML .= _('<strong><span style="color: red">Podany przez klienta kod pocztowy: ').$aAddress['postal']._(' nie jest zgodny z podaną miejscowością: ').$aAddress['city'].'</span></strong> <br /><br />';
        }
        
        $sHTML .= $this->aLang['paczkomaty_point_of_recipt'].'<br />';
        $sHTML .= $city.'<br />'.$street._('<br /><br />Opis placówki:<br />').$hours.'<br />';
        $sHTML .= _('Osoba odbierająca przesyłkę: ').$aAddress['name'].' '.$aAddress['surname']._(', telefon: ').$aAddress['phone'];    
      }    
      
    }  else {
			// brak zdefiniowanego punktu odbioru
			$sHTML = _('<strong><span style="color: red">Wybrany numer Punktu Odbioru: ').$sPunktOdbioruCode._(' nie jest poprawnym/aktualnym numerem placówki pocztowej</span></strong> <br /><br />');
    }
		
    return $sHTML;
	} // end of FormatUserDataOdbiorWPunkcie() method
  
  
	/**
	 * Metoda sprawdza czy kod pocztowy pasuje do miasta
	 *
	 * @global array $aConfig
	 * @param string $sCity
	 * @param string $sPostal
	 * @return bool 
	 */
	function checkCityPostal($sCity, $sPostal) {
		global $aConfig;
		
		$sSql = "SELECT count(1)
						FROM ".$aConfig['tabls']['prefix']."users_accounts_postals
						WHERE city = ".Common::Quote(stripslashes($sCity)).
						 " AND postal = ".Common::Quote(stripslashes($sPostal));
		return (intval(Common::GetOne($sSql)) > 0);
	}	// end of checkCityPostal() method
  

  /**
	 * Metoda formatuje dane dostawy dla Ruch
	 *
	 * @param string $sPunktOdbioruCode
   * @param array $aAddress
	 * @return string
	 */
	function _formatUserDataPaczkaWRuchu($sPunktOdbioruCode, $aAddress) {
    
    $oShipment = new Shipment('ruch', $this->pDbMgr);
    $aRuchDestinationPointAvailable = $oShipment->checkDestinationPointAvailable($sPunktOdbioruCode);

    if ($aRuchDestinationPointAvailable) {
      
      $sSql = "SELECT `desc`, details FROM shipping_points_ruch WHERE id = '".$sPunktOdbioruCode."'";
      $aPunktOdbioru = Common::GetRow($sSql);    

      $desc = $aPunktOdbioru['desc'];
      $details = unserialize(base64_decode($aPunktOdbioru['details']));
      $locale = $details['locale'];
      $hours = $details['hours'];
 
      if (!$aAddress['city'] || !$aAddress['postal']) {
        $sHTML .= _('<strong><span style="color: red">Zamówienie nie zawiera wymaganych przez RUCH danych osoby odbierającej przesyłkę (tj, adres, telefon)</span></strong> <br /><br />');
      } else { 
        if (!$this->checkCityPostal($aAddress['city'],$aAddress['postal'])) {
          $sHTML .= _('<strong><span style="color: red">Podany przez klienta kod pocztowy: ').$aAddress['postal']._(' nie jest zgodny z podaną miejscowością: ').$aAddress['city'].'</span></strong> <br /><br />';
        }
        
        $sHTML .= $this->aLang['paczkomaty_point_of_recipt'].'<br />';
        $sHTML .= $desc._('<br /><br />Opis placówki:<br />').$locale.'<br />'.$hours.'<br /><br />';
        $sHTML .= _('Osoba odbierająca przesyłkę: ').$aAddress['name'].' '.$aAddress['surname']._(', telefon: ').$aAddress['phone'];    
      }   
      
    }  else {
			// brak zdefiniowanego punktu odbioru
			$sHTML = _('<strong><span style="color: red">Wybrany numer Punktu Odbioru: ').$sPunktOdbioruCode._(' nie jest poprawnym/aktualnym numerem Punktu Odbioru RUCH</span></strong> <br /><br />');
    }
    
    return $sHTML;
	} // end of FormatUserDataPaczkaWRuchu() method

  
  /**
	 * Metoda formatuje dane dostawy dla Orlen
	 *
	 * @param string $sPunktOdbioruCode
	 * @param array $aAddress
	 * @return string
	 */
	function _formatUserDataOrlen($sPunktOdbioruCode, $aAddress) {
    
    $oOrlen = new Shipment('orlen', $this->pDbMgr);
    $bOrlenDestinationPointAvailable = $oOrlen->checkDestinationPointAvailable($sPunktOdbioruCode);
    
    if ($bOrlenDestinationPointAvailable) {

      $aFormatedPointOfReciptDetails = $oOrlen->getSinglePointDetails($sPunktOdbioruCode);     
      
      if (!$aAddress['city'] || !$aAddress['postal']) {
        $sHTML .= _('<strong><span style="color: red">Zamówienie nie zawiera wymaganych przez Orlen danych osoby odbierającej przesyłkę (tj, adres, telefon)</span></strong> <br /><br />');
      } else { 
        if (!$this->checkCityPostal($aAddress['city'],$aAddress['postal'])) {
          $sHTML .= _('<strong><span style="color: red">Podany przez klienta kod pocztowy: ').$aAddress['postal']._(' nie jest zgodny z podaną miejscowością: ').$aAddress['city'].'</span></strong> <br /><br />';
        }
        
        $sHTML = '';
        $sHTML = $this->aLang['paczkomaty_point_of_recipt'].'<br />';
        $sHTML .= $aFormatedPointOfReciptDetails['desc'].'<br /><br />'.$aFormatedPointOfReciptDetails['Name'].'<br />'.$aFormatedPointOfReciptDetails['openHours'].'<br /><br />';
        $sHTML .= _('Osoba odbierająca przesyłkę').': '.$aAddress['name'].' '.$aAddress['surname'].', '._('telefon').': '.$aAddress['phone'];
      }   
      
    }  else {
			// brak zdefiniowanego punktu odbioru
			$sHTML = _('<strong><span style="color: red">Wybrany numer Punktu Odbioru: ').$sPunktOdbioruCode._(' nie jest poprawnym/aktualnym numerem Stacji Orlen</span></strong> <br /><br />');
    }    
    
    return $sHTML;
	} // end of FormatUserDataOrlen() method  
}// end of Class
