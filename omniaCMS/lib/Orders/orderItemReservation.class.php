<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\lib\Orders;

use Exception;
use omniaCMS\lib\Products\ProductsStockReservations;

/**
 * Description of orderItemReservation
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class orderItemReservation {

  /**
   * @var ProductsStockReservations
   */
  private $oReservations;
  
  /**
   *
   * @var array
   */
  private $aValues;
  
  /**
   *
   * @var array
   */
  private $aOrderItem;
  
  /**
   *
   * @var int
   */
  private $iOrderId;
  
  /**
   *
   * @var int
   */
  private $iOrderItemId;
  
  /**
   *
   * @var int
   */
  private $iProductId;

  /**
   * 
   * @param ProductsStockReservations $oReservations
   */
  public function __construct(ProductsStockReservations $oReservations) {
    
    $this->oReservations = $oReservations;
  }
  
  /**
   * 
   * @param array $aValues
   * @param array $aOrderItem
   * @return bool
   */
  public function updateValues(array $aValues, array $aOrderItem) {
    $this->aValues = $aValues;
    $this->aOrderItem = $aOrderItem;
    $this->iOrderId = $aOrderItem['order_id'];
    $this->iOrderItemId = $aOrderItem['id'];
    $this->iProductId = $aOrderItem['product_id'];

    if ($this->checkDeleted()) {
      return $this->deleteReservation();
    }
    
    if ($this->checkCreated()) {
      return $this->createReservation();
    }
    
    if ($this->checkSetProvider()) {
      return $this->setProviderQuantity();
    }
    
    if ($this->checkSetQuantity()) {
      return $this->setQuantity();
    }
    
    return true;
  }

    public function onlyCreateReservation(array $aValues, array $aOrderItem)
    {
        $this->aValues = $aValues;
        $this->aOrderItem = $aOrderItem;
        $this->iOrderId = $aOrderItem['order_id'];
        $this->iOrderItemId = $aOrderItem['id'];
        $this->iProductId = $aOrderItem['product_id'];

        $this->createReservation();
    }
  
  /**
   * 
   * @return bool
   */
  private function setQuantity() {
    $iProviderId = $this->getItemOrderValue('source');
    return $this->oReservations->updateQuantity($iProviderId, $this->iProductId, $this->iOrderId, $this->iOrderItemId, $this->aValues['quantity'], $this->aOrderItem['quantity']);
  }
  
  /**
   * 
   * @return boolean
   */
  private function checkSetQuantity() {
    if ($this->checkIsItemReservation() === false) {
      return false;
    }
    
    if (isset($this->aValues['quantity']) && isset($this->aOrderItem['quantity']) && $this->aValues['quantity'] != $this->aOrderItem['quantity']) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @return boolean
   */
  private function checkIsItemReservation() {
    if (isset($this->aValues['status'])) {
      if (($this->aValues['status'] == '1' || $this->aValues['status'] == '3')) {
        return true;
      } else {
        return false;
      }
    } elseif (isset($this->aOrderItem['status'])) {
      if (($this->aOrderItem['status'] == '1' || $this->aOrderItem['status'] == '3' || $this->aOrderItem['status'] == '4')) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @return bool
   */
  private function setProviderQuantity() {
    $iQuantity = $this->getItemOrderValue('quantity');
    return $this->oReservations->updateProvider($this->aOrderItem['source'], $this->aValues['source'], $this->iProductId, $this->iOrderId, $this->iOrderItemId, $iQuantity);
  }
  
  /**
   * 
   * @return boolean
   */
  private function checkSetProvider() {
    if ($this->checkIsItemReservation() === false) {
      return false;
    }
    
    if (isset($this->aValues['source']) && isset($this->aOrderItem['source']) && $this->aValues['source'] != $this->aOrderItem['source']) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @return bool
   */
  private function deleteReservation() {
    $iSource = $this->getItemOrderValue('source', false);
    return $this->oReservations->deleteReservation($iSource, $this->iOrderId, $this->iOrderItemId);    
  }
  
  /**
   * 
   * @return bool
   */
  private function createReservation() {
    
    $iQuantity = $this->getItemOrderValue('quantity');
    $iSource = $this->getItemOrderValue('source');
    return $this->oReservations->createReservation($iSource, $this->iProductId, $this->iOrderId, $this->iOrderItemId, $iQuantity);
  }
  
  /**
   * 
   * @param string $sCols
   * @param bool $bOrderByValue
   * @return mixed
   * @throws Exception
   */
  private function getItemOrderValue($sCols, $bOrderByValue = true) {
    if ($bOrderByValue === true) {
      if (isset($this->aValues[$sCols])) {
        return $this->aValues[$sCols];
      } elseif (isset($this->aOrderItem[$sCols])) {
        return $this->aOrderItem[$sCols];
      } else {
        throw new Exception('Brak wartości kolumny '.$sCols);
      }
    } else {
      if (isset($this->aOrderItem[$sCols])) {
        return $this->aOrderItem[$sCols];
      } elseif (isset($this->aValues[$sCols])) {
        return $this->aValues[$sCols];
      } else {
        throw new Exception('Brak wartości kolumny '.$sCols);
      }
    }
  }
  
  /**
   * 
   * @return boolean
   */
  private function checkSetToOrder() {
    
    if (isset($this->aValues['status']) && 
        ($this->aValues['status'] == '1' || $this->aValues['status'] == '3') && 
        $this->aOrderItem['status'] == '0' //&& (!isset($this->aValues['deleted']) || $this->aValues['deleted'] == '0')
            ) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @return boolean
   */
  private function undeleteOrderItem() {
    if (isset($this->aOrderItem['deleted']) && $this->aOrderItem['deleted'] == '1' && 
        isset($this->aValues['deleted']) && $this->aValues['deleted'] == '0') {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @return boolean
   */
  private function checkCreated() {
    if ($this->checkIsItemReservation() === false) {
      return false;
    }
    
    if ($this->checkSetToOrder() ||
        $this->undeleteOrderItem()) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @return boolean
   */
  private function checkDeleted() {
    if ($this->checkIsItemReservation() === false) {
      return false;
    }
    
    if (isset($this->aValues['deleted']) && $this->aValues['deleted'] == '1' &&
        isset($this->aOrderItem['deleted']) && $this->aOrderItem['deleted'] == '0'
            ) {
      return true;
    } else {
      return false;
    }
  }
}
