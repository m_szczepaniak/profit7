<?php

use orders\logger;

/**
 * Klasa zamuje się przeliczaniem pojedynczej pozycji w zamówieniu
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class OrderItemRecountPost {
  /**
   * @var DatabaseManager $pDbMgr
   */
  public $pDbMgr;
  
  /**
   * @var Lib_FreeTransportPromo $this->oFreeTransportPromo
   */
  protected $oFreeTransportPromo;
  
  /**
   * @var OrderItemRecount $oOrderItemRecount
   */
  protected $oOrderItemRecount;
  
  //private $oOrdersToProviders;
  
  function __construct() {
    $this->sWebsite = 'profit24';
    include_once('FreeTransportPromo.class.php');
    $this->oFreeTransportPromo = new Lib_FreeTransportPromo('profit24');
    
    include_once("OrderItemRecount.class.php");
    $this->oOrderItemRecount = new OrderItemRecount();
    
    //include_once($_SERVER['DOCUMENT_ROOT'].'/LIB/orders/magazine/providers/ordersToProviders.class.php');
    //$this->oOrdersToProviders = new orders\magazine\providers\ordersToProviders($this->pDbMgr);
  }
  
  /**
   * Metoda zwraca flagę, czy należy zwrócić uwagę na przeczytanie uwag do zamówienia
   * 
   * @param string $sOrderRemarks
   * @param bool $bOrderRemarksRead
   * @param bool $bPostRemarksRead
   * @return boolean
   */
  protected function _getRemarksFlag($sOrderRemarks, $bOrderRemarksRead, $bPostRemarksRead) {

    // czy ustawione uwagi
    if (!empty($sOrderRemarks)) {
      if ($bOrderRemarksRead != '') {
        // przeczytane uwagi
        return TRUE;
      } elseif (isset($bPostRemarksRead) && $bPostRemarksRead != '') {
        // odznaczono, że przeczytano uwagi
        return TRUE;
      } else {
        // nie przeczytano uwag
        return FALSE;
      }
    } else {
      // nie trzeba czytać wogole uwag
      return TRUE;
    }
  }// end of _getRemarksFlag() method
  
	/**
	 * Metoda pobiera składowe zamówienia - nie pakiety
   * 
	 * @param int $iOrderId - id zamówienia
	 * @return array - składowe zamówienia
	 */
	protected function _getOrderItems($iOrderId){
    
		// pobranie zamowionych w ramach zamowienia towarow
		$sSql = "SELECT *
						 FROM orders_items
						 WHERE order_id = ".$iOrderId."
						 AND packet = '0'
						 ORDER BY id";
		return $this->pDbMgr->GetAll('profit24', $sSql);
	} // end of getOrderItems() method
	
	/**
	 * Pobiera dane zamówienia
	 * @param int $iOrderId - id zamowienia
	 * @return array
	 */
	protected function _getOrderData($iOrderId){
    
		$sSql = "SELECT *
						FROM orders
						WHERE id = ".$iOrderId;
		return $this->pDbMgr->GetRow('profit24', $sSql);
	} // end of getOrderData() method
  
  /**
   * Metoda pobiera rabat dla podanej pozycji w zamówieniu
   * 
   * @param char $cItemType
   * @param float $fItemDiscount
   * @param float $fParentDiscount
   * @param float $fOrderItemDiscount
   * @param string $sDo
   * @return float
   */
  protected function _getItemDiscount($cItemType, $fItemDiscount, $fParentDiscount, $fOrderItemDiscount, $sDo) {
    
    if ($cItemType == 'A') {
      // jeśli jest to załącznik pobierz rabat z ojca
      $fDiscount = Common::formatPrice2($fParentDiscount);
    } else {
      $fDiscount = Common::formatPrice2($fItemDiscount);
    }
    if ($fDiscount == 0.00 && ($sDo == 'connect_to' && $cItemType == 'P')) {
      // jeśli nie zdefiniowane quantity należy pobrać wartość z bazy danych (w przypadku łączenia zamówień)
      $fDiscount = $fOrderItemDiscount;
    }
    return $fDiscount;
  }// end of _getItemDiscount() method
  
  /**
   * Metoda sprawdza czy rekord jest rekordem edytowalnym, nie jest to załacznik lub pakiet
   * 
   * @param array $aPostItem
   * @param char $cOrderItemType
   * @param array $aPost
   * @return boolean
   */
  protected function _checkEditable($aPostItem, $cOrderItemType, $aPost) {
    
    if (!empty($aPostItem) || ($cOrderItemType == 'A' && !empty($aPost)) || (empty($aPost['editable']) && $aPost['do'] == 'connect_to' && $cOrderItemType == 'P')) {
      return true;
    }
    return false;
  }// end of _checkEditable() method
  
  /**
   * Metoda pobiera liczbę egzemplarzy, jeśli jest to Pakiet lub załącznik pobieramy z rodzica
   * 
   * @param char $cGetItemType
   * @param int $iPostItemQuantity
   * @param int $iParentPostItemQuantity
   * @param int $iOrderItemQuantity
   * @return int
   */
  protected function _getItemQuantity($cGetItemType, $iPostItemQuantity, $iParentPostItemQuantity, $iOrderItemQuantity) {
    
    $iQuantity = 0;
    if ($cGetItemType == 'P' || $cGetItemType == 'A') {
      $iQuantity = intval($iParentPostItemQuantity);
    } else {
      $iQuantity = intval($iPostItemQuantity);
    } 
    if ($iQuantity > 0) {
      return $iQuantity;
    } else {
      return $iOrderItemQuantity;
    }
  }// end of _getItemQuantity() method
  
  /**
   * Metoda ustawia flagi dla składowej zamówienia
   * 
   * @param char $cGetItemType
   * @param string $sStatus
   */
  protected function _proceedEditableItemFlags($cGetItemType, $sStatus, $fWeight) {
    if ($cGetItemType != 'P' && $cGetItemType != 'A') {
      if ($sStatus != '4') {
        $this->bAllReady = false;
      }
      if (intval($sStatus) < 3) {
        $this->bAllReadyNC = false;
      }
      if ($sStatus == 0) {
        $this->bAllNotNull = false;
      } else {
        $this->bExistNotNull = true;
      }
    }
    
    if ($fWeight == 0 && $cGetItemType != 'P' && $cGetItemType != 'A') { // zmiana z samego != A
      $this->bAllWeight = false;
    }
  }// end of _proceedEditableItemFlags() method
  
  /**
   * Metdoda dokonuje zmian w związku ze zmianą dostawcy
   * 
   * @global object $pDbMgr
   * @param int $iOrderItemId
   * @param array $aPostOTP - dane post z orders_to_providres
   * @param array $aNewData
   * @param array $aOldData
   * @return bool
  private function _affectOrdersToProviders($iOrderItemId, $aPostOTP, $aPostMainData, $aOldData) {
    
    if (!empty($aPostOTP)) {
      // JEST W OTP
      // lecimy po elementach
      foreach ($aPostOTP as $iOTPItemId => $aItemPostOTP) {
        // czy źródło zostało zmienione
        $aOldOTPData = $this->oOrdersToProviders->getOrdersToProvidersToOrder($iOTPItemId, array('provider_id', 'status', 'orders_to_providers_items_id', 'quantity'));
        if ($this->oOrdersToProviders->checkChangeOrdersToProviders($aItemPostOTP, $aOldOTPData)) {
          if ($aOldOTPData['orders_to_providers_items_id'] > 0) {
            // anulujemy strą pozycję na liście, zamówienia do dostawcy
            $this->oOrdersToProviders->changeOrderToProvidersItemsStatusByOrderItemIdProvider($aOldOTPData['orders_to_providers_items_id'], $aOldData['source'], 'E');
          }
          
          // anulujemy także pozycję w zamówieniach do dostawcy
          $this->oOrdersToProviders->changeStatusOrderToProviderToOrder($iOTPItemId, 99);
          
          // nowy dodajemy z nowym źródłem i statusem
          $this->oOrdersToProviders->InsertOrderToProviderToOrder($aItemPostOTP['quantity'], $iOrderItemId, $aOldData['order_id'], $aItemPostOTP['status'], $aItemPostOTP['provider_id']);
        }
        
        // zmiana ilości
        if ($this->oOrdersToProviders->checkChangeQuantity($aItemPostOTP['status'], $aItemPostOTP['quantity'], $aOldOTPData['quantity'])) {
          $this->oOrdersToProviders->changeOrderToProviderQuantity($iOTPItemId, $aItemPostOTP['quantity']);
        }
        
        // zmiana dostawcy
        if ($this->oOrdersToProviders->checkSetProvider($aItemPostOTP['status'], $aItemPostOTP['provider_id'], $aOldOTPData['provider_id'])) {
          $this->oOrdersToProviders->changeOrderToProviderToOrderProvider($iOTPItemId, $aItemPostOTP['provider_id']);
        }
        
        // anulowanie
        if ($this->oOrdersToProviders->checkCancelOrderToProviderToOrder($aItemPostOTP['status'], $aOldOTPData['status'], $aItemPostOTP['quantity'], $aOldOTPData['quantity'])) {
          // anulujemy także pozycję w zamówieniach do dostawcy
          $this->oOrdersToProviders->changeStatusOrderToProviderToOrder($iOTPItemId, 99);
          
          if ($aOldOTPData['orders_to_providers_items_id'] > 0) {
            // anulujemy strą pozycję na liście, zamówienia do dostawcy
            $this->oOrdersToProviders->changeOrderToProvidersItemsStatusByOrderItemIdProvider($aOldOTPData['orders_to_providers_items_id'], $aOldData['source'], 'E');
          }
        }
        
        // cofnięcie statusu do '00'
        if ($this->oOrdersToProviders->checkStatusGoBack($aItemPostOTP['status'], $aOldOTPData['status'])) {
          $this->oOrdersToProviders->changeStatusOrderToProviderToOrder($iOTPItemId, 0);
        }
        
      }
    } else {
      // BRAK AKTYWNEGO ODPOWIEDNIKA W OTP
      // czy dodajemy do OTP
      if ($this->oOrdersToProviders->checkAddToOrdersToProvidersToOrder($aOldData, $aPostMainData) === TRUE) {
        $this->oOrdersToProviders->InsertOrderToProviderToOrder($aPostMainData['quantity'], $iOrderItemId, $aOldData['order_id'], $aPostMainData['status'], $aPostMainData['source']);
      }
    }
    return true;
    */
    /*
    
    // sprawdzamy czy zmienił się dostawca, przebita pozycja
    if ($this->oOrdersToProviders->checkChangeOrdersToProviders($aPostOTP, $aOldData) === TRUE) {
      
      // TODO dodanie na nową listę ???
      
    }
    
    // sprawdzamy czy pozycja nie została dodana, na podstawie danych z pozycji podstawowej
    if ($this->oOrdersToProviders->checkInsertOrdersToProvidersToOrder($aPostMainData, $aOldData) === TRUE) {
      // dodajemy na listę do zamówienia
      $this->oOrdersToProviders->InsertOrderToProviderToOrder($aPostMainData['quantity'], $iOrderItemId, $aPostMainData['status'], $aPostMainData['source']);
    }
    
    // sprawdzamy czy pozycja nie została anulowana
    if ($this->oOrdersToProviders->checkCancelOrdersToProvidersToOrder($aPostOTP, $aOldData) === TRUE) {
      $this->oOrdersToProviders->CancelOrderToProviderToOrderByOrderItemId($iOrderItemId);
    }
  }// end of _affectOrdersToProviders() method
  */
  
	/**
	 * Metoda pobiera dane o załącznikach produktu
	 *
	 * @global type $aConfig
	 * @param type $iId
	 * @return type 
	 */
	protected function _getOrderItemAttachments($iId) {
		global $aConfig;
		
		$sSql = "SELECT price_netto, price_brutto, vat FROM ".$aConfig['tabls']['prefix']."orders_items A
						 WHERE parent_id = ".$iId." AND item_type='A'";
		return Common::GetAll($sSql); 
	}// end of _getOrderItemAttachments() method
  
  /**
   * Metoda dodaje wartości cen przy zmienie składowej zamówienia
   * 
   * @param array $aValues
   * @param int $iQuantity
   * @param float $fDiscount
   * @param float $fDiscountCurrency
   * @param array $aOItem
   * @return array
   */
  protected function _addPricesValues($aValues, $iQuantity, $fDiscount, $fDiscountCurrency, $aOItem) {
    $bHaveAttachments = FALSE;
    if ($fDiscount > 0) {

      // przeliczanie cen w przypadku kiedy książka ma załącznik
      $aAttachments = $this->_getOrderItemAttachments($aOItem['id']);
      if (!empty($aAttachments) && is_array($aAttachments)) {
        $bHaveAttachments = TRUE;

        $fBundlePromoPriceBrutto = $aOItem['bundle_price_brutto'] - Common::formatPrice2($aOItem['bundle_price_brutto'] * ($fDiscount / 100)); //($aOItem['bundle_price_brutto']);
        $aPrices = $this->oOrderItemRecount->getAttProductOrderPrices($aAttachments, $aOItem['price_brutto'], $fBundlePromoPriceBrutto, $aOItem['vat'], $fDiscount);
        $fPromoBrutto = $aPrices['book']['promo_price_brutto']; //$aOItem['price_brutto'] - $fDiscountCurrency;
        $fPromoNetto = $aPrices['book']['promo_price_netto']; //($fPromoBrutto / (1 + $aOItem['vat']/100));//Common::formatPrice2
        $fValueNetto = $iQuantity * $fPromoNetto;
        $fValueBrutto = $iQuantity * $fPromoBrutto;
      } else {
        // brak załączników zwykłe przeliczanie
        $fPromoBrutto = $aOItem['price_brutto'] - $fDiscountCurrency;
        $fPromoNetto = ($fPromoBrutto / (1 + $aOItem['vat'] / 100)); //Common::formatPrice2
        $fValueNetto = $iQuantity * $fPromoNetto;
        $fValueBrutto = $iQuantity * $fPromoBrutto;
      }
    } else {
      $fPromoBrutto = $aOItem['price_brutto'];
      $fPromoNetto = $aOItem['price_netto'];
      $fValueNetto = $iQuantity * $aOItem['price_netto'];
      $fValueBrutto = $iQuantity * $aOItem['price_brutto'];
    }
    $aFreeTransportTMP = $this->oFreeTransportPromo->getBookFreeTransport($aOItem['product_id'], $fPromoBrutto);
    if (!empty($aFreeTransportTMP)) {
      $this->aFreeTransport = array_unique(array_merge($this->aFreeTransport, $aFreeTransportTMP));
    }
    
    $aValues['promo_price_netto'] = Common::formatPrice2($fPromoNetto);
    $aValues['promo_price_brutto'] = $fPromoBrutto;
    $aValues['value_netto'] = Common::formatPrice2($fValueNetto);
    $aValues['value_brutto'] = $fValueBrutto;
    $aValues['total_vat_currency'] = Common::FormatPrice2($fValueBrutto - $fValueNetto);
    
    if ($bHaveAttachments === true && !empty($aPrices['bundle'])) {
      // jeśli posiada załączniki należy zmienić także value bundla
      $aValues['bundle_value_brutto'] = Common::formatPrice2($aPrices['bundle']['promo_price_brutto'] * $iQuantity);
      $aValues['bundle_value_netto'] = Common::formatPrice2($aPrices['bundle']['promo_price_netto'] * $iQuantity);
    }
    
    // skłądowa pakietu - nie dodawaj wartości do zamówienia, dodaj do tablicy pakietu
    if ($aOItem['item_type'] == 'P') {
      if ($fDiscount == 0) {
        $this->aPackets[$aOItem['parent_id']]['promo_price_netto'] += $aOItem['price_netto'];
        $this->aPackets[$aOItem['parent_id']]['promo_price_brutto'] += $aOItem['price_brutto'];
      } else {
        $this->aPackets[$aOItem['parent_id']]['promo_price_netto'] += $fPromoNetto;
        $this->aPackets[$aOItem['parent_id']]['promo_price_brutto'] += $fPromoBrutto;
      }
      $this->aPackets[$aOItem['parent_id']]['weight'] += Common::formatPrice2($aValues['weight']);
    } elseif ($aOItem['item_type'] == 'A') {
      $iPPOId = $this->_getParentOrderItemId($aOItem['parent_id']);

      if ($fDiscount == 0) {
        if ($iPPOId > 0) {
          // posiada ojca który jest pakietem
          $this->aPackets[$iPPOId]['promo_price_netto'] += $aOItem['price_netto'];
          $this->aPackets[$iPPOId]['promo_price_brutto'] += $aOItem['price_brutto'];
        } else {
          $this->fTotalValueNetto += $aOItem['value_netto'];
          $this->fTotalValueBrutto += $aOItem['value_brutto'];
        }
      } else {
        if ($iPPOId > 0) {
          // posiada ojca który jest pakietem
          $this->aPackets[$iPPOId]['promo_price_netto'] += $fPromoNetto;
          $this->aPackets[$iPPOId]['promo_price_brutto'] += $fPromoBrutto;
        } else {
          $this->fTotalValueNetto += $fValueNetto;
          $this->fTotalValueBrutto += $fValueBrutto;
        }
      }
    }
    // normalny produkt - dodanie do całkowitej wartości zamówienia
    else {
      $this->fTotalValueNetto += $fValueNetto;
      $this->fTotalValueBrutto += $fValueBrutto;
    }
    
    return $aValues;
  }// end of _addPricesValues() method
  
	/**
	 * Metoda pobiera ojca dla id w orders_items
	 * 
	 * @param $iId - id składowej zamówienia
	 */
	protected function _getParentOrderItemId($iId) {
		
		$sSql = "SELECT parent_id 
             FROM orders_items
						 WHERE id = '".$iId."'";
		return $this->pDbMgr->GetOne('profit24', $sSql);
	}// end of getParentOrderItemId() method
  
  /**
   * Metoda aktualizuje składową zamówienia
   * 
   * @param int $iId
   * @param array $aValues
   * @throws Exception
   */
  protected function _doUpdateOrderItem($iId, $aValues) {
    if ($this->pDbMgr->Update('profit24', "orders_items", $aValues, "id = " . $iId) === false) {
      throw new Exception(_('Wystąpił błąd zmiany składowej'));
    }
  }// end of _doUpdateOrderItem() method
  
  /**
   * Metoda ustawia flagi zamówienia w przypadku pakietu
   * 
   * @param int $iItemId
   */
  private function _proceedPacketFlags($iItemId, $fWeight) {
    
    $sSql = 'SELECT COUNT(id) FROM orders_items WHERE item_type=\'P\' AND parent_id=' . $iItemId . ' AND `status`<>\'4\'';
    $bPacketNotReady = $this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false;
    $sSql = 'SELECT COUNT(id) FROM orders_items WHERE item_type=\'P\' AND parent_id=' . $iItemId . ' AND `status` IN (\'0\',\'1\',\'2\')';
    $bPacketNotReadyNC = $this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false;
    $sSql = 'SELECT COUNT(id) FROM orders_items WHERE item_type=\'P\' AND parent_id=' . $iItemId . ' AND (`status`=\'0\' OR `status`=\'-1\')';
    $bPacketExistNotNull = $this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false;
    $sSql = 'SELECT COUNT(id) FROM orders_items WHERE item_type=\'P\' AND parent_id=' . $iItemId . ' AND `weight`=0.0';
    $bPacketNotAllWeight = $this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false;

    if ($bPacketNotReady) {
      $this->bAllReady = false;
    }
    if ($bPacketNotReadyNC) {
      $this->bAllReadyNC = false;
    }
    if ($bPacketExistNotNull) {
      $this->bAllNotNull = false;
    } else {
      $this->bExistNotNull = true;
    }
    if ($bPacketNotAllWeight) {
      $this->bAllWeight = false;
    }
    if ($fWeight == 0) {
      $this->bAllWeight = false;
    }
  }// end of _proceedPacketFlags() method
  
  /**
   * Metoda zmienia pakiet
   * 
   * @param array $aOItem
   * @param array $aPostItem
   * @param array $aPost
   * @throws Exception
   */
  protected function _proceedItemPacket($aOItem, $aPostItem, $aPost) {
    
    if ($aOItem['deleted'] == '0') {
      if (!empty($this->aPackets[$aOItem['id']])) {
        $fDiscountCurrency = Common::formatPrice2($aOItem['price_brutto'] - $this->aPackets[$aOItem['id']]['promo_price_brutto']);
        if (isset($aPostItem['quantity'])) {
          $iQuantity = intval($aPostItem['quantity']);
        } else {
          // pobranie z bazy danych w przypadku kiedy łączymy zamówienia
          $iQuantity = $aOItem['quantity'];
        }
        if ($aOItem['status'] == '4' && intval($aPostItem['status']) < intval($aOItem['status']) && isset($aPostItem['status'])) {
          throw new Exception(_('Nie można cofać statusu produktu zamówionego z potwierdzone.'));
        }

        if ($fDiscountCurrency > 0) {
          $fPromoBrutto = $this->aPackets[$aOItem['id']]['promo_price_brutto'];
          $fPromoNetto = $this->aPackets[$aOItem['id']]['promo_price_netto'];
          $fValueNetto = $iQuantity * $fPromoNetto;
          $fValueBrutto = $iQuantity * $fPromoBrutto;
        } else {
          $fPromoBrutto = 0;
          $fPromoNetto = 0;
          $fValueNetto = $iQuantity * $aOItem['price_netto'];
          $fValueBrutto = $iQuantity * $aOItem['price_brutto'];
        }
        $aValues = array(
            'discount_currency' => $fDiscountCurrency,
            'total_discount_currency' => Common::formatPrice2($fDiscountCurrency * $iQuantity),
            'quantity' => $iQuantity,
            'promo_price_netto' => Common::FormatPrice2($fPromoNetto),
            'promo_price_brutto' => Common::FormatPrice2($fPromoBrutto),
            'value_netto' => Common::FormatPrice2($fValueNetto),
            'value_brutto' => Common::FormatPrice2($fValueBrutto),
            'total_vat_currency' => Common::FormatPrice2($fValueBrutto - $fValueNetto),
            'status' => intval($aPostItem['status']),
//            'n_order_item_status' => intval($aPostItem['status']),
            'weight' => $this->aPackets[$aOItem['id']]['weight'],
            'source' => $aPostItem['source']//tutaj zmiana
        );

        $this->_proceedPacketFlags($aOItem['id'], $aValues['weight']);
        
        // zmiana źródła produktu, skutkuje usunięciem z listy wysłanych
        // @ticket M.Chudy - w przypadku zmiany źródła zamówienia z G do X
        /*
         * XXX w nowym nie powinno mieć to zastosowania
         */
        if ($aValues['status'] == '3' && $aValues['source'] != $aOItem['source']) {
          $aValues['sent_hasnc'] = '0';
        }

        // UWAGA !!!! 
        // zmieniamy wyłącznie te dane, które rzeczywiście się zmieniły
        $aChangedValues = $this->pDbMgr->getChangedUpdateValues($aValues, $aOItem);
        if ($this->checkSendToAzymut($aPostItem)) {
          if (false === $this->checkProductHaveAzymutbookindeks($aOItem)) {
            throw new Exception(_('Nie można odznaczyć produktu do Azymut jeśli nie posiada azymut bookindeksu.'));
          }
        }
        if (!empty($aChangedValues)) {
          //$this->_affectOrdersToProviders($aOItem['id'], $aPostItem['orders_to_providers'], $aValues, $aOItem);
          $this->affectReservations($aChangedValues, $aOItem);
          $this->_doUpdateOrderItem($aOItem['id'], $aChangedValues);
          $this->_addOrderUpdateLog($aOItem['order_id'], $aOItem['id'], $aChangedValues);
        }

        $this->fTotalValueNetto += Common::FormatPrice2($fValueNetto);
        $this->fTotalValueBrutto += Common::FormatPrice2($fValueBrutto);
      } else {
        $this->fTotalValueNetto += Common::FormatPrice2($aOItem['value_netto']);
        $this->fTotalValueBrutto += Common::FormatPrice2($aOItem['value_brutto']);
      }
    }
  }// end of _proceedItemPacket() method
  
  /**
   * 
   * @param type $aOItem
   */
  private function checkProductHaveAzymutbookindeks($aOItem) {
    
    if ($aOItem['product_id'] > 0) {
      $sSql = 'SELECT azymut_index FROM products WHERE id = '.$aOItem['product_id'];
      return $this->pDbMgr->GetOne('profit24', $sSql) != '' ? true : false;
    } else {
      return true;
    }
  }
  
  /**
   * 
   * @param array $aPostItem
   * @param array $aOItem
   * @return boolean
   */
  private function checkSendToAzymut($aPostItem) {
    
    if ($aPostItem['status'] == '1' && $aPostItem['source'] == '7') {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param array $aChanges
   */
  private function _addOrderUpdateLog($iOrderId, $iOrderItemId, $aChanges) {
    $oShipment = new logger($this->pDbMgr);
    $oShipment->addLog($iOrderId, $_SESSION['user']['id'], $aChanges, $iOrderItemId);
  }
  
  private function affectReservations($aValues, $aOrderItemData) {
    
    $oReservationNN = new \omniaCMS\lib\Products\ProductsStockReservations($this->pDbMgr);
    $oReservation = new \omniaCMS\lib\Orders\orderItemReservation($oReservationNN);
    $oReservation->updateValues($aValues, $aOrderItemData);
    
  }
  
  /**
   * Metoda przetważa zwykłe składowe zamówienia
   * 
   * @todo dodać obsługę n_order_item_status i provider_id w tabeli orders_to_providers_to_order - aktualizacja itp
   * @param array $aOItem
   * @param array $aPostItem
   * @param array $aParentPostItem
   * @param array $aPost
   * @throws Exception
   */
  protected function _proceedItem($aOItem, $aPostItem, $aParentPostItem, $aPost) {
    
    if ($aOItem['deleted'] == '0') {
      if ($this->_checkEditable($aPostItem, $aOItem['item_type'], $aPost)) {
        
        if ($aPostItem['status'] == 0 && $aPostItem['statusCur'] > 0) {
          $aPostItem['status'] = $aPostItem['statusCur'];
          $aOItem['status'] = $aPostItem['statusCur'];
        }
        if ($aOItem['status'] == '4' && intval($aPostItem['status']) < intval($aOItem['status']) && isset($aPostItem['status'])) {
          throw new Exception(_('Nie można cofać statusu produktu zamówionego z potwierdzone'));
        }
        
        $iQuantity = $this->_getItemQuantity($aOItem['item_type'], $aPostItem['quantity'], $aParentPostItem['quantity'], $aOItem['quantity']);
        
        $fDiscount = $this->_getItemDiscount($aOItem['item_type'], $aPostItem['discount'], $aParentPostItem['discount'], $aOItem['discount'], $aPost['do']);
        $fDiscountCurrency = Common::formatPrice2($aOItem['price_brutto'] * ($fDiscount / 100));
        
        $aValues = array(
            'discount' => $fDiscount,
            'discount_currency' => $fDiscountCurrency,
            'total_discount_currency' => Common::formatPrice2($fDiscountCurrency * $iQuantity),
            'quantity' => $iQuantity,
            'status' => intval($aPostItem['status']), //tutaj zmiana na id parrenta
//            'n_order_item_status' => intval($aPostItem['status']),
            'weight' => Common::formatPrice2($aPostItem['weight']),
            'source' => $aPostItem['source']//tutaj zmiana na id parrenta
        );
        
        $aValues = $this->_addPricesValues($aValues, $iQuantity, $fDiscount, $fDiscountCurrency, $aOItem);

        /*
        if (($aPostItem['status'] < 3 &&
                $aPostItem['status'] > 0 &&
                in_array($aValues['source'], array(0, 1, 5, 51, 52)) &&
                $aValues['source'] !== null)
        ) {
          $aValues['source'] = null;
        }
        */

        // XXX TODO mi tu coś się nie podoba, do zbadania nie wybranie źródła
        if (($aPostItem['status'] == 1 || $aPostItem['status'] == 3) && $aValues['source'] === null
        ) {
          throw new Exception(_('Źródło nie zostało wybrane '.print_r($aPostItem, true).print_r($aValues, true)));
        }
        if ($this->checkSendToAzymut($aPostItem)) {
          if (false === $this->checkProductHaveAzymutbookindeks($aOItem)) {
            throw new Exception(_('Nie można odznaczyć produktu do Azymut jeśli nie posiada azymut bookindeksu.'));
          }
        }
        
        $this->_proceedEditableItemFlags($aOItem['item_type'], $aPostItem['status'], $aValues['weight']);
        
        // zmiana źródła produktu, skutkuje usunięciem z listy wysłanych
        // @ticket M.Chudy - w przypadku zmiany źródła zamówienia z G do X
        /*
         * XXX w nowym nie powinno mieć to zastosowania
         */
        if ($aValues['status'] == '3' && $aValues['source'] != $aOItem['source']) {
          $aValues['sent_hasnc'] = '0';
        }
        
        // UWAGA !!!! 
        // zmieniamy wyłącznie te dane, które rzeczywiście się zmieniły
        $aChangedValues = $this->pDbMgr->getChangedUpdateValues($aValues, $aOItem);
        if (!empty($aChangedValues)) {
          //$this->_affectOrdersToProviders($aOItem['id'], $aPostItem['orders_to_providers'], $aValues, $aOItem);
          if ($aOItem['item_type'] != 'A') {
            $this->affectReservations($aChangedValues, $aOItem);
          }
          $this->_doUpdateOrderItem($aOItem['id'], $aChangedValues);
          $this->_addOrderUpdateLog($aOItem['order_id'], $aOItem['id'], $aChangedValues);
        }
        $this->_doUpdateProductWeight($aOItem['product_id'], $aValues['weight']);

        
      } else {
        if ($aOItem['item_type'] == 'I') {
          if ($aOItem['bundle_value_netto'] > 0.00 && $aOItem['bundle_value_brutto'] > 0.00) {
            $this->fTotalValueNetto += $aOItem['bundle_value_netto'];
            $this->fTotalValueBrutto += $aOItem['bundle_value_brutto'];
          } else {
            $this->fTotalValueNetto += $aOItem['value_netto'];
            $this->fTotalValueBrutto += $aOItem['value_brutto'];
          }

          if ($aOItem['status'] != '4') {
            $this->bAllReady = false;
          }
          if (intval($aOItem['status']) < 3) {
            $this->bAllReadyNC = false;
          }
          if ($aOItem['status'] == 0) {
            $this->bAllNotNull = false;
          } else {
            $this->bExistNotNull = true;
          }
          if ($aOItem['weight'] == 0 && $aOItem['item_type'] != 'A') {
            $this->bAllWeight = false;
          }
        }
      }
    }
  }// end of method
  
  /**
   * Metoda aktualiuje wagę produktu w tabeli z produktami
   * 
   * @param int $iProductId
   * @param float $fWeight
   * @return boolean
   * @throws Exception
   */
  protected function _doUpdateProductWeight($iProductId, $fWeight) {
    
    // jeśli produkt wciaż istnieje w bazie
    // i nie jest to pakiet
    $mWeight = $this->_checkExistsAndGetWeightProduct($iProductId);
    if ($mWeight !== FALSE) {
      // aktualizacja wagi w produkcie
      if (floatval($fWeight) > 0 && $fWeight != $mWeight) {
        $aPValues = array(
            'weight' => Common::formatPrice2($fWeight)
        );
        if ($this->pDbMgr->Update('profit24', "products", $aPValues, "id = " . $iProductId) === false) {
          throw new Exception(_('Wystąpił błąd zmiany składowej'));
        }
      }
    }
    return TRUE;
  }// end of _doUpdateProductWeight() method
  
	/**
	 * Metoda sprawdza, czy produkt istnieje, jeśli tak to pobiera wagę
	 *
	 * @global array $aConfig
	 * @param integer $iId
	 * @return bool
	 */
	protected function _checkExistsAndGetWeightProduct($iId) {
		global $aConfig;
		
		$sSql = "SELECT id, weight FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id = '".$iId."'";
		$aProduct = $this->pDbMgr->GetRow('profit24', $sSql);
    if (!empty($aProduct)) {
      return $aProduct['weight'];
    } else {
      return FALSE;
    }
	}// end of existProduct() method
}