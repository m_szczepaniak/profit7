<?php

$aConfig['config']['project_dir'] = 'omniaCMS/lib/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

class testPlatnoscipl{

    public $endPoint = 'koszyk/platnosci.html';

    public $testData;

    function __construct()
    {
        $this->testData = array(
            //'pos_id' => 166495,
            'pos_id' => 166495,
            'session_id' => 999563,
            'ts' => 1550617841328,
            'sig' => 'dce9ea32c1a70a5d1767f2d5e522588d',
        );

        $this->testPayment = array(
            'status' => 'OK',
            'trans' => array(
                'id' => 1289124460,
                'pos_id' => 166495,
                'session_id' => 999563,
                'order_id' => 1754330,
                'amount' => 13260,
                'status' => 1,
                'pay_type' => 'b',
                'pay_gw_name' => 'bp',
                'desc' => 'Zamówienie nr: 060220136719 - julsqlx@gmail.com',
                'desc2' => '',
                'create' => '2019-02-20 00:10:40',
                'init' => '',
                'sent' => '',
                'recv' => '',
                'cancel' => '',
                'auth_fraud' => 0,
                'ts' => 1550617841328,
                'sig' => 'dce9ea32c1a70a5d1767f2d5e522588d',
                'add_bank_name' => 'mBank S.A.',
                'add_owner_address' => 'ul. Grunwaldzka 182, 60-166 Poznań',
                'add_owner_name' => 'PayU S.A.',
                'add_clients_count' => '0',
                'add_trans_prev' => 'https://www.platnosci.pl/np/show.action?transId=f900c19ec0br08ak104',
                'add_cc_number' => '80114011243630011289124460',
                'add_trans_title' => 'XX1289124460XX'
            )
        );
    }

    function sendCurl()
    {
        global $aConfig;
        $host = $aConfig['common']['base_url_http'].$this->endPoint;
        $ch = curl_init($host);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->testData);

// execute!
        $response = curl_exec($ch);

        if ($response === false)
        {
            echo 'Curl error: ' . curl_error($ch);
        }
        else
        {
            echo $response;
        }

// close the connection, release resources used
        curl_close($ch);
    }
}

$testPlatnosci = new testPlatnoscipl();
$testPlatnosci->sendCurl();