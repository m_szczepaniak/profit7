<?php 
class Invoice{
	public $sTemplatePath = '';
	public $iWebsiteId;
	public $sInvoicePostfix;
	
	
	function __construct() {
		global $pDbMgr;
		
		if (empty($pDbMgr) && !is_object($pDbMgr)) {
			include_once('DatabaseManager.class.php');
			$pDbMgr = new DatabaseManager();
		}
	}
	/**
	 * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type 
	 */
	function getWebsiteId($iOId) {
		global $aConfig, $pDbMgr;
		
		if ($this->iWebsiteId <= 0 ) {
			$sSql = "SELECT website_id FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE id=".$iOId;
			$this->iWebsiteId = $pDbMgr->GetOne('profit24', $sSql);
		}
		return $this->iWebsiteId;
	}// end of getWebsiteId() method
	
	
	/**
	 * Metoda zwaca posf
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type 
	 */
	function getInvoiceWebsiteSymbol($iOId) {
		global $aConfig;
		
		return $aConfig['invoice_website_symbol_'.$this->getWebsiteId($iOId)];
	}// end of getInvoiceWebsiteSymbol() method
	
	
	/**
	 * Generuje html faktury
	 * @param int $iId - id zamówienia
	 * @return string - html
	 */
	function getInvoiceHtml($iId,$bSecondInvoice=false,$bProForma=false){
	global $aConfig,$pSmarty, $pDbMgr;
	
		$aModule = array();
		$aModule['lang'] =& $aConfig['lang']['m_zamowienia_invoice'];

		include_once('OrderItemRecount.class.php');
		$oOrderItemRecount = new OrderItemRecount();
		$aOrderData = $oOrderItemRecount->getOrderPricesDetail($iId, $bSecondInvoice, '', true);
    $aModule['orders_user_workflow'] = $this->_getOrdersToUsers($iId, 5);
		$aModule['order'] = $aOrderData['order'];
		$aModule['invoice_items'] = $aOrderData['order_items'];
		$aModule['vat'] = $aOrderData['order']['vat'];
		$aModule['order']['vat'] = $aModule['order']['order_vat'];
		
		
		if($bProForma){
			$aModule['pro_forma'] = '1'; 
			if($bSecondInvoice){
				$aModule['invoice_number'] = $aModule['order']['order_number'].'A';
			} else {
				$aModule['invoice_number'] = $aModule['order']['order_number'];
			}
		} else {
			if($bSecondInvoice){
				$aModule['invoice_number'] = $aModule['order']['invoice2_id'];
			} else {
				$aModule['invoice_number'] = $aModule['order']['invoice_id'];
			}
		}
		
		$aModule['order']['value_brutto'] = Common::formatPrice2($aModule['order']['value_brutto']);
		$aModule['order']['total_value_brutto_words'] = Common::price2Words($aModule['order']['value_brutto']);
		
		$aModule['order']['value_netto'] = Common::formatPrice($aModule['order']['value_netto']);
		$aModule['order']['value_brutto'] = Common::formatPrice($aModule['order']['value_brutto']);
		$aModule['order']['vat'] = Common::formatPrice($aModule['order']['vat']);
		
		
		$aModule['order']['transport_cost'] = Common::formatPrice($aModule['order']['transport_cost']);
		//$aModule['order']['total_value_brutto_words'] = Common::price2Words($aModule['order']['value_brutto']);
		//$aModule['order']['total_value_brutto'] = Common::formatPrice($aModule['order']['total_value_brutto']);
		
		//$aModule['order']['order_date'] = formatDateClient($aModule['order']['order_date']);
		$aModule['invoice_date'] = date('d.m.Y');
				
		if ($aModule['order']['invoice_date_pay'] != '') {
			$aModule['invoice_date_14'] = formatDateClient($aModule['order']['invoice_date_pay']);
//      AREK Do modyfikacji
//      $iDays14  = mktime(0, 0, 0, date("m")  , date("d")+20, date("Y"));
//      $aModule['invoice_date_14'] = date('d.m.Y',$iDays14);
		} else {
			if($aModule['order']['payment_type'] == 'bank_14days'){
				$aDataArr = explode('.', $aModule['order']['invoice_date']);
//				$iDays14  = mktime(0, 0, 0, date("m")  , date("d")+21, date("Y"));
				$iDays14  = mktime(0, 0, 0, $aDataArr[1]  , $aDataArr[0]+14, $aDataArr[2]); // AREK Do modyfikacji
				$aModule['invoice_date_14'] = date('d.m.Y',$iDays14);
				if ($bProForma == false) {
					$sMysqlDateOfPay = date('Y-m-d',$iDays14);
					$this->updateInvoiceDatePay($iId, $sMysqlDateOfPay);
				}
			}
		}
		if ($bProForma != false) {
			// brak terminu do zapłaty na fakturze proforma
			unset($aModule['invoice_date_14']);
		}
		// arek mod
//		if (stristr($aModule['order']['payment'], 'przelew 14 dni')) {
//			$aModule['order']['payment'] = 'przelew 21 dni';
//		}
		if($aModule['order']['second_payment_type']!='' && $aModule['order']['second_payment_enabled'])
			$aModule['order']['payment'].=', '.$aModule['order']['second_payment']; // Arek Do modyfikacji przelew 30 dni na sztywno
		
		// dane zamawiającego
		$aAddresses = $this->getOrderAddresses($iId);	
		if($bSecondInvoice){
			$aModule['address'] =& $aAddresses[2];
		} else {
			$aModule['address'] =& $aAddresses[0];
		}
	
		$aModule['website_symbol'] = $this->getWebsiteSymbol($this->getWebsiteId($iId, false));
		//$aModule['order']['transport'] = $aConfig['lang'][$this->sModule]['transport_1'];
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_seller_data";
		$aModule['seller_data'] =& $pDbMgr->GetRow($aModule['website_symbol'], $sSql);
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'invoice.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	} // end of getInvoiceHtml() method

  
  /**
   * Metoda pobiera imię oraz nazwisko dla osoby dla zamówienia w statusie
   * 
   * @param int $iOrderId
   * @param int $iStatus
   * @return array
   */
  private function _getOrdersToUsers($iOrderId, $iStatus) {

    $sSql = "SELECT U.name, U.surname, OTU.created
             FROM orders_to_users AS OTU
             JOIN users AS U
              ON OTU.user_id = U.id
             WHERE OTU.order_id = ".$iOrderId."
                   AND OTU.internal_status = '".$iStatus."'";
    return Common::GetRow($sSql);
  }// end of _getOrdersToUsers() method
  
  
	/**
	 * Metoda pobiera dane faktury z bazy danych
	 *
	 * @global type $aConfig
	 * @param integer $iOrderId
	 * @param bool $bProforma
	 * @return string 
	 */
	function invoiceExists($iOrderId, $bProforma) {
		global $aConfig;
		$iWebsiteId = $this->getWebsiteId($iOrderId);
		$sInvoiceWebsiteSymbol = $this->getInvoiceWebsiteSymbol($iOrderId);
		/*
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."invoices
						 WHERE order_id=".$iOrderId." AND proforma='".($bProforma == true? '1' : '0' )."'";
		return (Common::GetOne($sSql) > 0? true : false);
		 */
		
		if ($bProforma == false) {
			$aMatches = array();
			$aOData = $this->getOrderDefData($iOrderId, array('invoice_date', 'invoice_id'));
			preg_match("/(\d+)-(\d+)-\d+/", $aOData['invoice_date'], $aMatches);
			if (empty($aMatches)) return false;
			// faktura zwykła
			
			$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].$aConfig['common']['invoice_dir'];
			$sFilename = 'faktura'.str_replace("/","_",$aOData['invoice_id']).'.pdf';
		} else {
			$aMatches = array();
			$sFilename = 'faktura_proforma'.str_replace("/","_",$this->getOrderNumber($iOrderId)).'.pdf';
			preg_match("/faktura_proforma\d+_(\d+)_(\d+)_".$sInvoiceWebsiteSymbol."\.pdf/", $sFilename, $aMatches);
			if (!empty($aMatches)) {
				$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			} else {
				return false;
			}
			
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['pro_forma_invoice_dir'];
		}
		
		if (file_exists($sBaseFile.$sPath.'/'.$sFilename)) {
			return true;
		} else{
			return false;
		}
	}// end of getInvoiceData() method
	
	
		/**
	 * Metoda pobiera dane faktury z bazy danych
	 *
	 * @global type $aConfig
	 * @param integer $iOrderId
	 * @param bool $bProforma
	 * @return string 
	 */
	function getInvoiceData($iOrderId, $bProforma) {
		global $aConfig;
		$iWebsiteId = $this->getWebsiteId($iOrderId);
		$sInvoiceWebsiteSymbol = $this->getInvoiceWebsiteSymbol($iOrderId);
		
		/*
		$sSql = "SELECT content FROM ".$aConfig['tabls']['prefix']."invoices
						 WHERE order_id=".$iOrderId." AND proforma='".($bProforma == true? '1' : '0' )."'";
		return Common::GetOne($sSql);
		 */
		
		if ($bProforma == false) {
			$aMatches = array();
			$aOData = $this->getOrderDefData($iOrderId, array('invoice_date', 'invoice_id'));
			preg_match("/(\d+)-(\d+)-\d+/", $aOData['invoice_date'], $aMatches);
			if (empty($aMatches)) return false;
			// faktura zwykła
			
			$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].$aConfig['common']['invoice_dir'];
			$sFilename = 'faktura'.str_replace("/","_",$aOData['invoice_id']).'.pdf';
		} else {
			$aMatches = array();
			$sFilename = 'faktura_proforma'.str_replace("/","_",$this->getOrderNumber($iOrderId)).'.pdf';
			preg_match("/faktura_proforma\d+_(\d+)_(\d+)_".$sInvoiceWebsiteSymbol."\.pdf/", $sFilename, $aMatches);
			if (!empty($aMatches)) {
				$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			} else {
				return false;
			}
			
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['pro_forma_invoice_dir'];
		}
		
		if (file_exists($sBaseFile.$sPath.'/'.$sFilename)) {
			return file_get_contents($sBaseFile.$sPath.'/'.$sFilename);
		} else{
			return false;
		}
	}// end of getInvoiceData() method
	
	/**
	 * Metoda usuwa fakture
	 *
	 * @global type $aConfig
	 * @param type $iOrderId
	 * @param type $bProforma 
	 */
	function deleteInvoice($iOrderId, $bProforma) {
		global $aConfig;
		$iWebsiteId = $this->getWebsiteId($iOrderId);
		$sInvoiceWebsiteSymbol = $this->getInvoiceWebsiteSymbol($iOrderId);

		if ($bProforma == false) {
			$aMatches = array();
			$aOData = $this->getOrderDefData($iOrderId, array('invoice_date', 'invoice_id'));
			preg_match("/(\d+)-(\d+)-\d+/", $aOData['invoice_date'], $aMatches);
			if (empty($aMatches)) return false;
			// faktura zwykła
			
			$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].$aConfig['common']['invoice_dir'];
			$sFilename = 'faktura'.str_replace("/","_",$aOData['invoice_id']).'.pdf';
		} else {
			$aMatches = array();
			$sFilename = 'faktura_proforma'.str_replace("/","_",$this->getOrderNumber($iOrderId)).'.pdf';
			preg_match("/faktura_proforma\d+_(\d+)_(\d+)_".$sInvoiceWebsiteSymbol."\.pdf/", $sFilename, $aMatches);
			if (!empty($aMatches)) {
				$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			} else {
				return false;
			}
			
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['pro_forma_invoice_dir'];
		}
		
		if (file_exists($sBaseFile.$sPath.'/'.$sFilename)) {
			return unlink($sBaseFile.$sPath.'/'.$sFilename);
		} else{
			return false;
		}
	}// end of deleteInvoice() method
	

	/**
		* Funkcja tworzy katalog docelowy
		* 
		* @return	bool	- true: utworzoo; false: wystapil blad
		*/
	function createDirectory($sBaseDir, $sAPath) {
		global $aConfig;

		$aDirs = explode('/', $sAPath);
		foreach ($aDirs as $sDir) {
			$sPath .= '/'.$sDir;
			if (!is_dir($sBaseDir.$sPath)) {
				if (!mkdir($sBaseDir.$sPath,
										$aConfig['default']['new_dir_mode'])) {
					return false;
				};
			}
		}
		return true;
	} // end of createDirectory() function

	
	/**
	 * Metoda zapisuje informacje o fakturze lub fakturze proforma
	 *
	 * @global type $aConfig
	 * @param type $iOrderId - id zamówienia
	 * @param type $sFilename - nazwa pliku
	 * @param type $sContent - zawartosc pliku
	 * @param type $bProforma - czy proforma
	 * @param type $sInvoiceData - data wygenerowania faktury uzupełniane 
	 *															tylko w przypadku kiedy nie jest to proforma
	 */
	function saveInvoiceData($iOrderId, $sFilename, $sContent, $bProforma, $sInvoiceData = 'NULL') {
		global $aConfig;
		$iWebsiteId = $this->getWebsiteId($iOrderId);
		$sInvoiceWebsiteSymbol = $this->getInvoiceWebsiteSymbol($iOrderId);
		
		/*
		$aValues = array(
				'order_id' => $iOrderId,
				'invoice_name' => $sFilename,
				'data' => ($bProforma == true ? 'NULL' : $sInvoiceData),
				'invoice2' => '0', //pole jeśli faktury miały by zostać rozdzielone
				'proforma' => ($bProforma == true ? '1' : '0'),
				'content' => addslashes($sContent)
		);
		if ( Common::Insert($aConfig['tabls']['prefix']."invoices", $aValues) === false) {
			return false;
		} else {
			if ($bProforma != true) {
			// jeśli powiodło się i nowa faktura nie jest proformą usunięcie faktury proforma zamówienia
			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."invoices WHERE order_id=".$iOrderId." AND proforma='1' LIMIT 1";
			return Common::Query($sSql);
			} else {
				return true;
			}
		}
		// nie powinno tu nigdy wpasc
		return false;*/
		
		if ($bProforma == false) {
			$aMatches = array();
			$sDate = $this->getOrderInvoiceData($iOrderId);
			preg_match("/(\d+)-(\d+)-\d+/", $sDate, $aMatches);
			if (empty($aMatches)) return false;
			// faktura zwykła
			
			$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].$aConfig['common']['invoice_dir'];
			$this->createDirectory($sBaseFile, $sPath);
		} else {
			preg_match("/faktura_proforma\d+_(\d+)_(\d+)_".$sInvoiceWebsiteSymbol."\.pdf/", $sFilename, $aMatches);
			if (!empty($aMatches)) {
				$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			} else {
				return false;
			}
			
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['pro_forma_invoice_dir'];
			$this->createDirectory($sBaseFile, $sPath);
		}
    
    // XXX TODO do usunięcia - dodane aby zablokować zapisywanie
    // return true; //
		//dump($sBaseFile.$sPath.'/'.$sFilename);
		if (!file_exists($sBaseFile.$sPath.'/'.$sFilename)) {
			$iCount = file_put_contents($sBaseFile.$sPath.'/'.$sFilename, $sContent);
		}
		if ($iCount <=0 ) {
			return false;
		}
		return true;
	}// end of saveInvoiceData() method
	
	
	/**
	 * Metoda pobiera date wygenerowania faktury
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type 
	 */
	function getOrderInvoiceData($iOId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT invoice_date FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id=".$iOId."
						 LIMIT 1";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getOrderInvoiceData() method
	
	
	/**
	 * Metoda pobiera 
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @param type $aPools
	 * @return type 
	 */
	function getOrderDefData($iOId, $aPools) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT ".implode(',', $aPools)." 
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id=".$iOId;
		return $pDbMgr->GetRow('profit24', $sSql);
	}// end of getOrderDefData() method
	
	
	/**
	 * Metoda pobiera date wygenerowania faktury
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type 
	 */
	function getOrderNumber($iOId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT order_number FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id=".$iOId."
						 LIMIT 1";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getOrderInvoiceData() method
	

	/**
	 * Sprawdza czy sposob transportu dla amówienia to odbiór osobisty
	 * @param int $iOrderId - id zamowienia
	 * @return bool
	 */
	function checkPersonalReception($iOrderId){
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT B.personal_reception
						FROM ".$aConfig['tabls']['prefix']."orders A
						LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						ON B.id=A.transport_id
						WHERE A.id = ".$iOrderId;
		return ($pDbMgr->GetOne('profit24', $sSql) == '1');
	}// end of checkPersonalReception() method
	
		/**
	 * Pobiera wysokosc stawki VAT dla metody płatności/transportu 
	 * @param int $iId - id zamówienia
	 * @return float - stawka vat
	 */
	function getPaymentVat($iId){
		global $aConfig, $pDbMgr;
		$sSql = "SELECT vat
						FROM ".$aConfig['tabls']['prefix']."orders_payment_types
						WHERE id = ".$iId;
		return intval($pDbMgr->GetOne('profit24', $sSql));
	}
			/**
	 * Pobiera dane adresowe z zamówienai z bazy
	 * @param int $iId - id adresu
	 * @return array ref
	 */
	function &getOrderAddresses($iId){
		global $aConfig, $pDbMgr;

		$sSql = "SELECT address_type, is_company, company, name, surname, street, number, number2, postal, city, nip, phone
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
						 " ORDER BY address_type"; 
		return $pDbMgr->GetAssoc('profit24', $sSql);
	} // end of getOrderAddresses() method
	
	
	/**
	 * Metoda aktualizuje datę płatności faktury w przypadku wybrania metody płatności vat 14 oraz vat ...(30?)
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @param type $sDateOfPay
	 * @return bool
	 */
	function updateInvoiceDatePay($iOId, $sDateOfPay) {
		global $aConfig, $pDbMgr;
		
		$aValues = array();
		$aValues['invoice_date_pay'] = $sDateOfPay;
		if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."orders", $aValues, " id=".$iOId) !== false) {
			return true;
		}
		return false;
	}// end of updateInvoiceDatePay() method
	
	
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type 
	 */
	function getWebsiteSymbol($iWebsiteId) {
		global $aConfig;
		
		$sSql = "SELECT code FROM ".$aConfig['tabls']['prefix']."websites
						 WHERE id=".$iWebsiteId;
		return Common::GetOne($sSql);
	}// end of getWebsiteSymbol() method
}	

?>
