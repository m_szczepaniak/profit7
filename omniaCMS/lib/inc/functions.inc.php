<?php

/**
 * Wersja funkcji print_r() z dodaniem formatowania <pre></pre>
 *
 * @param	mixed	zmienna
 */
function dump() {
	global $aConfig;
	
		if (empty($aConfig['common']['programmer_ip']) || 
			 (!empty($aConfig['common']['programmer_ip']) && in_array($_SERVER['REMOTE_ADDR'], $aConfig['common']['programmer_ip'])) || 
       empty($_SERVER['REMOTE_ADDR'])) {
	    $aTrace = debug_backtrace(false);
	    $aArgs = func_get_args();
	    echo "<pre style='white-space: pre'>";
	    echo "Line {$aTrace[0]['line']} \n";
	    echo "File {$aTrace[0]['file']} \n";
			foreach ($aArgs as $aArg) {
		  	print_r($aArg);
		  }
		  echo "</pre>";
		}
}// end of dump function 

function ldump($var, $die = false)
{
	if ($_SESSION['user']['name'] == 'ljaskiewicz' || $_SESSION['user']['name'] == 'agolba') {
		dump($var);

		if (true === $die) {
			die;
		}
	}

	return null;
}

function enableScale(Smarty &$pSmarty)
{
	if ($_SESSION['user']['scale_access'] != 1) {
		return false;
	}

	$pSmarty->assign('scale', '1');
}
	
	
/**
 * Funkcja zamienia pomiedzy stronami kodowymi polskich znakow
 */
function changeCharcodes($sStr, $sFrom, $sTo) {
	$aCodes = array(
		'strange' => array (
			"\x96", "\x93",	// -, “
			"\x94", "\x84",	// ”, „
			"\xA7"
		),
		'strange2' => array(
			"-", "“", 
			"”", "„",
			"§"
		),
		'iso88592' => array(
			"\xA1", "\xB1", // A, a
			"\xC6", "\xE6", // C, c
			"\xCA", "\xEA", // E, e
			"\xA3", "\xB3", // L, l
			"\xD1", "\xF1", // N, n
			"\xD3", "\xF3", // O, o
			"\xA6", "\xB6", // S, s
			"\xAC", "\xBC", // Z('), z(')
			"\xAF", "\xBF"  // Z(.), z(.)
		),
		'windows1250' => array(
			"\xA5", "\xB9", // A, a
			"\xC6", "\xE6", // C, c
			"\xCA", "\xEA", // E, e
			"\xA3", "\xB3", // L, l
			"\xD1", "\xF1", // N, n
			"\xD3", "\xF3", // O, o
			"\x8C", "\x9C", // S, s
			"\x8F", "\x9F", // Z('), z(')
			"\xAF", "\xBF"  // Z(.), z(.)
		),
		'cp852' => array(
			"\xA4", "\xA5", // A, a
			"\x8F", "\x86", // C, c
			"\xA8", "\xA9", // E, e
			"\x9D", "\x88", // L, l
			"\xE3", "\xE4", // N, n
			"\xE0", "\xA2", // O, o
			"\x97", "\x98", // S, s
			"\x8D", "\xAB", // Z('), z(')
			"\xBD", "\xBE"  // Z(.), z(.)
		),
		'utf8' => array(
			"\xC4\x84", "\xC4\x85", // A, a
			"\xC4\x86", "\xC4\x87", // C, c
			"\xC4\x98", "\xC4\x99", // E, e
			"\xC5\x81", "\xC5\x82", // L, l
			"\xC5\x83", "\xC5\x84", // N, n
			"\xC3\x93", "\xC3\xB3", // O, o
			"\xC5\x9A", "\xC5\x9B", // S, s
			"\xC5\xB9", "\xC5\xBA", // Z('), z(')
			"\xC5\xBB", "\xC5\xBC"  // Z(.), z(.)
		),
		'no' => array(
			"\x41", "\x61", // A, a
			"\x43", "\x63", // C, c
			"\x45", "\x65", // E, e
			"\x4C", "\x6C", // L, l
			"\x4E", "\x6E", // N, n
			"\x4F", "\x6F", // O, o
			"\x53", "\x73", // S, s
			"\x5A", "\x7A", // Z('), z(')
			"\x5A", "\x7A"  // Z(.), z(.)
		)
	);
	
	return str_replace($aCodes[$sFrom], $aCodes[$sTo], $sStr);
}
	
	
/* Funkcja zwraca sciezke do pliku wraz ze zmiennymi $_POST
 *
 * @param		array		$aGet				- tablica z wartosciami do QueryString
 * @param		array		$aOmit			- nazwy zmiennych, ktore maja byc pominiete
 * @param		bool		$bAddFilters- dodawaj do URLa zmienne filtrow
 * @return	string	sciezka PHP_SELF wraz ze zmiennymi $_GET
 */
	function phpSelf($aGet=array(), $aOmit=array(), $bAddFilters=false) {
		$sGet = '';
		$aGetVars = array();
		$sPhpSelf = basename($_SERVER['PHP_SELF']);
		$aAllowed = array('frame', 'module_id', 'module', 'action', 'file', 'mid', 'pid', 'tid', 'thid', 'ppid', 'site');
		
		if (!empty($_SERVER['QUERY_STRING'])) {
			$aVars = explode('&', $_SERVER['QUERY_STRING']);
			foreach ($aVars as $sVar) {
				$aTemp = explode('=', $sVar);
				if (in_array($aTemp[0], $aAllowed)) {
					$aGetVars[$aTemp[0]] = $_GET[$aTemp[0]];
				}
			}
		}
		// dolaczenie do zmiennych $aGetVars zmiennych z tablicy $aGet
		foreach ($aGet as $sName => $sVal) {
			$aGetVars[$sName] = $sVal;
		}
		
		if ($bAddFilters) {
			if (isset($_POST['search']) && !empty($_POST['search'])) {
				$aGetVars['search'] = $_POST['search'];
			}
			foreach ($_POST as $sName => $mVal) {
				if (preg_match('/^f_/', $sName)) {
					$aGetVars[$sName] = $mVal;
				}
			}
		}

		if (!empty($aGetVars)) {
			foreach ($aGetVars as $sName => $sVal) {
				if (!in_array($sName, $aOmit)) {
					$sGet .= $sName.'='.$sVal.'&';
				}
			}
			$sPhpSelf .= '?'.substr($sGet, 0, strlen($sGet)-1);
		}
		return $sPhpSelf;
	}
	
	
/* Funkcja sprawdza rodzaj i wersję przeglądarki
 *
 */	
	function getBrowserInfo() {
		global $HTTP_USER_AGENT, $aConfig;
		
		if (isset($HTTP_USER_AGENT)) {
			$sAgent = $HTTP_USER_AGENT;
		}
		else {
			$sAgent = $_SERVER['HTTP_USER_AGENT'];
		}
		if (strpos($sAgent, 'MSIE') !== false && strpos($sAgent, 'mac') === false && strpos($sAgent, 'Opera') === false) {
			$aConfig['browser']['version'] = (float)substr($sAgent, strpos($sAgent, 'MSIE') + 5, 3);
			$aConfig['browser']['name'] = 'ie';
			$aConfig['browser']['compatible'] = $aConfig['browser']['version'] >= 5.5;
			$aConfig['styles']['cursor'] = 'hand';
		}
		elseif (strpos($sAgent, 'Gecko/') !== false) {
			$aConfig['browser']['version'] = (int)substr($sAgent, strpos($sAgent, 'Gecko/') + 6, 8);
			$aConfig['browser']['name'] = 'gecko';
			$aConfig['browser']['compatible'] = $aConfig['browser']['version'] >= 20030210;
			//$aConfig['browser']['compatible'] = false;
			$aConfig['styles']['cursor'] = 'pointer';
		}
		elseif (strpos($sAgent, 'AppleWebKit/') !== false) {
			$aConfig['browser']['version'] = substr($sAgent, strpos($sAgent, 'AppleWebKit/') + 47, 5);
			$aConfig['browser']['name'] = 'apple';
			$aConfig['browser']['compatible'] = true;
			//$aConfig['browser']['compatible'] = false;
			$aConfig['styles']['cursor'] = 'pointer';
		}
		elseif (strpos($sAgent, 'Opera/') !== false) {
			$aConfig['browser']['version'] = (float)substr($sAgent, strpos($sAgent, 'Opera/') + 6, 4);
			$aConfig['browser']['name'] = 'opera';
			$aConfig['browser']['compatible'] = true;
			//$aConfig['browser']['compatible'] = false;
			$aConfig['styles']['cursor'] = 'pointer';
		}
		else {		
			$aConfig['browser']['version'] = 0;
			$aConfig['browser']['name'] = '';
			$aConfig['browser']['compatible'] = false;
			$aConfig['styles']['cursor'] = 'hand';
		}
	}
	
	function TriggerError($sErr) {
		trigger_error($sErr);
	}
	
	
	function AddCookie($sVarName, $sValue='', $iExpDate=0, $sPath='', $sDomain='', $iSafe=0) {
		return setcookie($sVarName, $sValue, $iExpDate, $sPath, $sDomain, $iSafe);
	} // end of AddCookie() function


	function DeleteCookie($sVarName, $sPath='', $sDomain='', $iSafe=0){
		return setcookie($sVarName, '', time() - 3600, $sPath, $sDomain, $iSafe);
	} // end of DeleteCookie() function
	
	
	function DeleteSession() {
		DeleteCookie('check');
		DeleteCookie('usid');
		DeleteCookie(session_name());
		
		$sOriginalPath = getcwd();
//	 	chdir(session_save_path());
// 		$sPath = realpath(getcwd()).'/';
// 		
//		$sSessId = session_id();
//		if(file_exists($sPath.'sess_'.$sSessId)) {
//   		unlink($sPath.'sess_'.$sSessId);
// 		}
// 		
 		session_regenerate_id();
 		$sSessId = session_id();
		@session_destroy();
		
//    if(file_exists($sPath.'sess_'.$sSessId)) {
//   		unlink($sPath.'sess_'.$sSessId);
// 		}
 		chdir($sOriginalPath);
	} // end of DeleteSession() funciton
	
	
	function FormatDate($sDate='', $bAddTime=false) {
		if (preg_match('/^(\d{2})-(\d{2})-(\d{4})$/', $sDate, $aMatches)) {
			return date('Y-m-d', strtotime($aMatches[3].'-'.$aMatches[2].'-'.$aMatches[1])).
						 ($bAddTime ? date(' H:i:s') : '');
		}
		return date('Y-m-d H:i:s');
	}
	
	function formatDateClient($sDate, $iMode=1) {
	if ($iMode == 1) {
		if (preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $sDate, $aMatches)) {
			return $aMatches[3].'.'.$aMatches[2].'.'.$aMatches[1];
		}
	}
	else {
		if (preg_match('/^(\d{2})-(\d{2})-(\d{4})$/', $sDate, $aMatches)) {
			return $aMatches[3].'-'.$aMatches[2].'-'.$aMatches[1];
		}
	}
	return '';
}
	
	function formatDateTimeClient($sDate) {
		if (preg_match('/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/', $sDate, $aMatches)) {
			return $aMatches[3].'.'.$aMatches[2].'.'.$aMatches[1].' '.$aMatches[4].':'.$aMatches[5].':'.$aMatches[6];
		}
  }
	
	function CalDateToTimestamp($sDate='',$bEndOfDay=false) {
		if (preg_match('/^(\d{2})-(\d{2})-(\d{4})$/', $sDate, $aMatches)) {
			if($bEndOfDay) {
				return mktime(23,59,59,$aMatches[2],$aMatches[1],$aMatches[3]);
			} else {
				return mktime(0,0,0,$aMatches[2],$aMatches[1],$aMatches[3]);
			}
		}
		return time();
	}
	
	function CalDateTimeToTimestamp($sDate='') {
		if (preg_match('/^(\d{2})-(\d{2})-(\d{4}) (\d{2}):(\d{2})$/', $sDate, $aMatches)) {
			 mktime($aMatches[4],$aMatches[5],0,$aMatches[2],$aMatches[1],$aMatches[3]).' ';
				return mktime($aMatches[4],$aMatches[5],0,$aMatches[2],$aMatches[1],$aMatches[3]);
			
		}
		return time();
	}
	function FormatDateHour($sDate, $sHour) {
		if (preg_match('/^(\d{2})-(\d{2})-(\d{4})$/', $sDate, $aMatches)) {
			return date('Y-m-d', 									strtotime($aMatches[3].'-'.$aMatches[2].'-'.$aMatches[1])).
						 			' '.$sHour.':00';
		}
		return date('Y-m-d H:i:s');
	}
	

	/**
	 * Funkcja "opakowuje" przekazany do niej kod HTML tabelki
	 * w inna tabelke :)
	 *
	 * @param		string	$sTable
	 * @return	string
	 */
	function ShowTable($sTable) {
		return '<table border="0" width="100%" cellspacing="0" cellpadding="0">'.
					 '<tr><td class="formTableContainer">'.$sTable.'</td></tr></table>';
	} // end of ShowTable() function
	
	
   /**
	 * Funkcja koduje haslo
	 * 
	 * @param		string	$sString				- ciag do zakodowania
	 * @return	string									- zakodowany ciag
	 */
	function EncodePasswd($sString) {
		return md5($sString);
	} // end of EncodePasswd() function
	
	function ShowText(&$sText) {
		$sText = strip_tags($sText);
	}
	
	function rememberViewState($sModule) {
		$aGetVars = array('sort', 'order', 'page', 'pid');
		$aPostVars = array('per_page', 'search');
		
		if (isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($sModule, $_GET['reset']);
		}
		
		
		// GET
		foreach ($aGetVars as $sVar) {
			if (isset($_GET[$sVar])) {
				$_SESSION['_viewState'][$sModule]['get'][$sVar] = $_GET[$sVar];
			}
			elseif (isset($_SESSION['_viewState'][$sModule]['get'][$sVar])) {
				$_GET[$sVar] = $_SESSION['_viewState'][$sModule]['get'][$sVar];
			}
		}
		// zmienna 'exp' - id rozwinietych stron artykulu, aktualnosci,...
		// w danym momencie moze byc rozwinietych kilka stron z tego wzgleu
		// do sesji dodawana jest tablica z ID rozwinietych elementow
		if (isset($_GET['exp']) && !empty($_GET['exp'])) {
			$_SESSION['_viewState'][$sModule]['exp'][] = $_GET['exp'];
		}
		// zmienna 'nar' - id rozwinietych stron artykulu, aktualnosci,... do zwiniecia
		if (isset($_GET['nar']) && !empty($_GET['nar']) &&
				in_array($_GET['nar'], $_SESSION['_viewState'][$sModule]['exp'])) {
			// usuniecie id strony z tablicy $_SESSION['_viewState'][$sModule]['exp']
			foreach ($_SESSION['_viewState'][$sModule]['exp'] as $iKey => $iExp) {
				if ($iExp == $_GET['nar']) {
					unset($_SESSION['_viewState'][$sModule]['exp'][$iKey]);
				}
			}
		}
		
		// POST
		// zapamietanie stanu filtrow - zmiennych rozpoczynajacych sie od f_*
		foreach ($_POST as $sVarName => $sValue) {
			if (preg_match('/^f_[\w|]+$/', $sVarName)) {
				$_SESSION['_viewState'][$sModule]['filters'][$sVarName] = $_POST[$sVarName];
			}
		}
		foreach ($_GET as $sVarName => $sValue) {
			if (preg_match('/^f_[\w|]+$/', $sVarName)) {
				$_SESSION['_viewState'][$sModule]['filters'][$sVarName] = $_GET[$sVarName];
			}
		}
		if (isset($_SESSION['_viewState'][$sModule]['filters'])) {
			foreach ($_SESSION['_viewState'][$sModule]['filters'] as $sVarName => $sValue) {
				if (preg_match('/^f_[\w|]+$/', $sVarName)) {
					$_POST[$sVarName] = $_SESSION['_viewState'][$sModule]['filters'][$sVarName];
				}
			}
		}
		
		// pozostale zmienne POST
		foreach ($aPostVars as $sVar) {
			if (isset($_POST[$sVar])) {
				$_SESSION['_viewState'][$sModule]['post'][$sVar] = $_POST[$sVar];
			}
			elseif (isset($_SESSION['_viewState'][$sModule]['post'][$sVar])) {
				$_POST[$sVar] = $_SESSION['_viewState'][$sModule]['post'][$sVar];
			}
		}
	}
	
	
	/**
	 * Funkcja resetuje widok
	 * 
	 * @param	string	$sModule	- nazwa modulu dla ktorego bedzie resetowany widok
	 * @param	integer	$iMode	- tryb resetowania: 1 - wszystkie zmienne
	 * 																						2 - wszystkie bez liczby rekordow na stronie
	 * 																						3 - tylko numer aktualnej strony
	 */
	function resetViewState($sModule, $iMode = 1) {
		$aResetGetVars = array('sort', 'order', 'page');
		$aResetPostVars = array('search');
		$bResetFilters = true;
		
		if ($iMode == 1) {
			// resetowanie rowniez liczby rekordow na stronie
			$aResetPostVars[] = 'per_page';
		}
		elseif ($iMode == 3) {
			// resetowanie tylko numeru aktualnej strony
			$aResetGetVars = array('page');
			$aResetPostVars = array();
			$bResetFilters = false;
		}
		elseif ($iMode == 4) {
			// resetowane wszystkie ustawienia z wyjatkiem $_GET['pid']
			$aResetGetVars = array('sort', 'order', 'page');
		}
		// resetowanie zapamietanych ustawien widoku
		// GET
		foreach ($aResetGetVars as $sVar) {
			if (isset($_GET[$sVar])) {
				unset($_GET[$sVar]);
			}
			if (isset($_SESSION['_viewState'][$sModule]['get'][$sVar])) {
				unset($_SESSION['_viewState'][$sModule]['get'][$sVar]);
			}
		}
		if (isset($_SESSION['_viewState'][$sModule]['exp'])) {
			unset($_SESSION['_viewState'][$sModule]['exp']);
		}
		
		// filtry
		if ($bResetFilters && !empty($_SESSION['_viewState'][$sModule]['filters'])) {
			unset ($_SESSION['_viewState'][$sModule]['filters']);
			foreach ($_POST as $sVarName => $sValue) {
				if (preg_match('/^f_\w+$/', $sVarName)) {
					unset($_POST[$sVarName]);
				}
			}
		}
		// POST
		foreach ($aResetPostVars as $sVar) {
			if (isset($_POST[$sVar])) {
				unset($_POST[$sVar]);
			}
			if (isset($_SESSION['_viewState'][$sModule]['post'][$sVar])) {
				unset($_SESSION['_viewState'][$sModule]['post'][$sVar]);
			}
		}
	}
	
	function rememberTreeViewState($sSign) {
		if (isset($_GET['expand']) &&
				!in_array($_GET['expand'], $_SESSION['_treeViewState'][$sSign]['expanded'])) {
			// dodanie nowej rozwinietej galezi
			$_SESSION['_treeViewState'][$sSign]['expanded'][] = $_GET['expand']; 
		}
		elseif (isset($_GET['wrap']) &&
				in_array($_GET['wrap'], $_SESSION['_treeViewState'][$sSign]['expanded'])) {
			foreach ($_SESSION['_treeViewState'][$sSign]['expanded'] as $iKey => $iVal) {
				if (intval($iVal) == intval($_GET['wrap'])) {
					unset($_SESSION['_treeViewState'][$sSign]['expanded'][$iKey]);
				}
			}
		}
		elseif (isset($_GET['wrap_all'])) {
			// zwiniecie wszystkich
			$_SESSION['_treeViewState'][$sSign]['expanded'] = array();
		}
	}
	
	function utf8_strlen($str) {
		$ret = 0;
   	for($i = 0; $i < strlen($str); $i++){
    	if( (ord($str{$i}) >= 0 && ord($str{$i}) <= 127) || ord($str{$i}) >= 192 ) $ret++;
   	}
	 	return $ret;
	}
	
	
	/**
	 * Funkcja dowiazuje do Smartow komunikat o niewystarczajacych uprawnieniach
	 * 
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function showPrivsAlert(&$pSmarty) {
		global $aConfig;
		$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
	} // end of privsAlert()
	
	
	/**
	 * Funkcja pobiera z bazy danych nazwe i wersje CMSa i ustawia je w tablicy $aConfig
	 * 
	 * @return	void
	 */
	function setCMSVersion() {
		global $aConfig;
		
		$sSql = "SELECT name, version
						 FROM ".$aConfig['tabls']['prefix']."version";
		list ($aConfig['common']['name'], $aConfig['common']['version']) = Common::GetRow($sSql, array(), DB_FETCHMODE_ORDERED);
	} // end of setCMSVersion() function
	
	
	/**
	 * Funkcja pobiera z bazy danych konfiguracje serwisu i zapisuja ja do tablicy $aConfig
	 * 
	 * @param	integer	$iLangId	- Id wersji jez.
	 * @return	void
	 */
	function setWebsiteSettings($iLangId, $sWebsite='') {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT allow_comments, allow_versioning, name, email, users_email, orders_email, mail_footer_order, mail_footer
						 FROM ".$aConfig['tabls']['prefix']."website_settings
						 WHERE language_id = ".$iLangId;
		if ($sWebsite != '') {
			list ($aConfig['common']['allow_comments'],
						$aConfig['common']['allow_versioning'],
						$aConfig['default']['website_name'],
						$aConfig['default']['website_email'],
						$aConfig['default']['website_users_email'],
						$aConfig['default']['website_orders_email'],
            $aConfig['default']['website_mail_footer_order'],
						$aConfig['default']['website_mail_footer']) = $pDbMgr->GetRow($sWebsite, $sSql, array(), DB_FETCHMODE_ORDERED);
		} else {
			list ($aConfig['common']['allow_comments'],
						$aConfig['common']['allow_versioning'],
						$aConfig['default']['website_name'],
						$aConfig['default']['website_email'],
						$aConfig['default']['website_users_email'],
						$aConfig['default']['website_orders_email'],
            $aConfig['default']['website_mail_footer_order'],
						$aConfig['default']['website_mail_footer']) = Common::GetRow($sSql, array(), DB_FETCHMODE_ORDERED);
		}
	} // end of setWebsiteSettings() function
	
	
	/**
	 * Funkcja zamienia znaki <br /> na znaki przejscia do nowej liniii \n
	 * lub dowolny inny podany znak
	 * 
	 * @param	string	$sStr	- ciag w ktorym ma nastapic zamiana
	 * @return	string	- ciag po zamianie
	 */
	function br2nl($sStr, $sReplace="\n") { 
		return html_entity_decode(preg_replace('/<br[^>]*\/?>\n*/i', $sReplace, str_replace("\r", '', $sStr)));
	} // end of br2nl() function 
	
	
		
	/**
	 * funkcja pobiera i zwraca nazwe uzytkownika tworzacego strone
	 * 
	 * @param	integer	$iLangId	- Id wersji jezykowej
	 * @param	integer	$iPId	- Id strony
	 * @return	string	- nazwa uzytkwonika tworzacego strone
	 */
	function getPageCreatorName($iLangId, $iPId) {
		global $aConfig;
		
		$sSql = "SELECT created_by
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$iLangId." AND
						 			 id = ".$iPId;
		return Common::GetOne($sSql);
	} // end of getPageCreatorName() method
	
	function getMaxUploadSize() {
		$sUploadMaxFileSize = ini_get('upload_max_filesize');
		
		return $sUploadMaxFileSize;
	} // end of getMaxUploadSize() function
	
	
	function deleteDir($sDir) {
		global $aConfig;
		$sImgDir = $aConfig['common']['client_base_path'].
							 $aConfig['common']['photo_dir'].'/'.
							 $sDir;
		$sFileDir = $aConfig['common']['client_base_path'].
							 $aConfig['common']['file_dir'].'/'.
							 $sDir;
		if (is_dir($sImgDir)) {
			deleteDirTree($sImgDir); 
		}
		
		if (is_dir($sFileDir)) {
			deleteDirTree($sFileDir); 
		}
	}
	
	function deleteDirTree($sDir) {
	  $pDir = @opendir($sDir);
	  while (false!==($sFile = readdir($pDir))) {
	    if($sFile != '.' && $sFile != '..') {
	      if(is_dir($sDir.'/'.$sFile)) {
	        if (!deleteDirTree($sDir.'/'.$sFile)) {return false;}
	      }
	      else {
	        if (!@unlink($sDir.'/'.$sFile)) {
						return false;
					}
	      }
	    }
	  }
	  @closedir($pDir);
	  if (@rmdir($sDir)) {return true;}
	  return false;
	}
	
	/**
	 * Funkcja pobiera i zwraca domyslna konfiguracje modulu
	 * 
	 * @param	string	$sXMLFile	- sciezka do pliku XML
	 * @return	array ref	- domyslna konfiguracja modulu
	 */
	function &getDefaultSettings($sXMLFile) {
		$aData = array();
		$aOptions = array(
			'complexType'       => 'array'
		);
		$pUS = new XML_Unserializer($aOptions);
		
		if (file_exists($sXMLFile)) {
			$pResult = $pUS->unserialize($sXMLFile, true);
			if (!PEAR::IsError($pResult)) {
	      $aData =& $pUS->getUnserializedData();
	    }
		}
		return $aData;
	} // end of getDefaultSettings() function
	
	/**
	 * Funkcja zapisuje domyslna konfiguracje dla modulu do pliku XML
	 * 
	 * @param	string	$sModule	- symbol modulu
	 * @param	string	$sXMLFile	- plik XML
	 * @return	bool	- true: udalo sie; false: nie udalo sie
	 */
	function writeDefaultSettings($sModule, $sXMLFile) {
		global $aConfig;
		
		// dolaczenie klasy XML/Serializer
		require_once ("XML/Serializer.php");
		// Serializer options
		$aOptions = array(
			'addDecl'	=> true,
			'encoding'	=> 'utf-8',
			'indent'	=>	"\t",
			'rootName'	=>	'config'
		);
		
		$oSerializer = new XML_Serializer($aOptions);
		// usuniecie zbednych danych
		$aData = $_POST;
		unset($aData['do']);
		unset($aData['__ErrPrefix']); 
		unset($aData['__ErrPostfix']);
		unset($aData['__Validator']);
		unset($aData['send']);
		$oResult = $oSerializer->serialize($aData);
		
		if (PEAR::isError($oResult)) {
			// blad serializacji
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($status->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
		}
		// sprawdzenie czy instnieje katalog konfiguracji
		if (!is_dir('config/'.$sModule)) {
			if (!@mkdir('config/'.$sModule)) {
				return false;
			}
		}

		// zapis do pliku XML
		if ($oFile = fopen($sXMLFile, 'w')) {
			flock($oFile, 2);
			if (fwrite($oFile, $oSerializer->getSerializedData())) {
				flock($oFile, 3);
				fclose($oFile);
			}
			else {
				// blad zapisu
				return false;
			}
		}
		else {
			// blad otwarcia pliku do zapisu
			return false;
		}
		return true;
	} // end of writeDefaultSettings() function
	
	
	/**
	 * Metoda pobiera i zwraca tablice z symbolami dla ston modulu $iMId
	 * i wersji jez. $iLangId
	 * 
	 * @param	integer	$iMId	- Id modulu
	 * @param	integer	$iLangId	- Id wersji jez.
	 * @return	array
	 */
	function getModulePagesSymbols($iMId, $iLangId) {
		global $aConfig;
		$aPages = array();
		
		$sSql = "SELECT id, symbol
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$iLangId." AND
						 			 module_id IS NOT NULL AND
						 			 module_id = ".$iMId;
		return Common::GetAssoc($sSql);
	} // end of getModulePagesSymbols() method
	
	
	/**
	 * Funkcja pobiera liste plikow z katalogu $aConfig['common']['file_upload_dir']
	 * w formacie dla pola SELECT
	 * 
	 * @return	array	- tablica z lista plikow
	 */
	function &getFilesList() {
		global $aConfig;
		$aFiles	= array();
		
		$pDir = opendir($aConfig['common']['client_base_path'].$aConfig['common']['file_upload_dir']);
  	while (false !== ($sFile = readdir($pDir))) {
    	if ($sFile != '.' && $sFile != '..') {
    		$aFile[] = array('value' => $sFile,
	      								 'name' => $sFile
	      								);
    	}
    }
    closedir($pDir);
    sort($aFiles);
    
    return $aFiles;
	} // end of getFilesList() function
	
	
	/**
   * Funkcja tworzaca na podstawie przekazanego do niej Id strony
   * jej pelna sciezke od korzenia do niej
   *
   * @param	integer	$iId	- ID strony do ktorej ma byc tworzona sciezka
   * @param	integer	$iLangId	- Id wersji jezykowej
   * @param	bool	$bSymbols	- true: sciezka symboli; false: sciezka nazw 
   * @return	array	- uporzadkowana tablica ze sciezka do strony
   */
  function getPathItems($iId, $iLangId, $bSymbols=false) {
		global $aConfig;
		$aPath		= array();
		$sColumn = ($bSymbols ? 'symbol' : 'name');
		
  	$sSql = "SELECT A.".$sColumn.", IFNULL(A.parent_id, 0) AS parent_id,
  									B.name AS menu_name
  					 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
  					 			JOIN ".$aConfig['tabls']['prefix']."menus AS B
  					 			ON B.id = A.menu_id AND
  					 				 B.language_id = A.language_id
  					 WHERE A.id = ".$iId." AND
  					 			 A.language_id = ".$iLangId;
  	$aItem =& Common::GetRow($sSql);
  	$sMenuName = $aItem['menu_name'];
  	$aPath[] = $aItem[$sColumn];
  	while (intval($aItem['parent_id']) > 0) {
  		$sSql = "SELECT ".$sColumn.", IFNULL(parent_id, 0) AS parent_id
  					 FROM ".$aConfig['tabls']['prefix']."menus_items
  					 WHERE id = ".$aItem['parent_id']." AND
  					 			 language_id = ".$iLangId;
	  	$aItem =& Common::GetRow($sSql);
	  	$aPath[] = $aItem[$sColumn];
  	}
  	$aPath[] = $sMenuName;
		
		return array_reverse($aPath);
  } // end getPathItems() function
  
  
 	/**
	 * Funkja pobiera i zwraca tablice z danymi dla pola SELECT zawierajaca
	 * liste ston modulu $iMId i wersji jez. $iLangId
	 * 
	 * @param	mixed (integer, string)	$mModule	- Id lub symbol modulu
	 * @param	integer	$iLangId	- Id wersji jez.
	 * @return	array
	 */
	function getModulePages($mModule, $iLangId, $bForCombo=true, $sSearch='', $sOption='') {
		global $aConfig;
		$aPages = array();
		
		$iMId = $mModule;
		if (!is_numeric($mModule)) {
			// podano symbol modulu - okreslenie Id modulu
			$sSql = "SELECT id
							 FROM ".$aConfig['tabls']['prefix']."modules
							 WHERE symbol = '".$mModule."'";
			$iMId =& Common::GetOne($sSql);
		}
		
		// pobranie wszystkich menu
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id = ".$iLangId;
		$aMenus =& Common::GetAll($sSql);
		foreach ($aMenus as $aMenu) {
			$aItems = array();
			// pobranie wszystkich stron danego menu
			$sSql = "SELECT IFNULL(parent_id, 0) AS parent_id,
											IFNULL(module_id, 0) AS module_id, id, name, moption
							 FROM ".$aConfig['tabls']['prefix']."menus_items
							 WHERE language_id = ".$iLangId." AND
							 			 menu_id = ".$aMenu['id']; 
			$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
			if (!empty($aItems)) {
				if ($bForCombo) {
					$aModuleItems = getModuleTree($aItems, $iMId, 0, '', ' / ', '', $sSearch, $sOption);
					if (!empty($aModuleItems)) {
						$aPages[] = array('value'=>'OPTGROUP', 'label'=>$aMenu['name']);
						$aPages = array_merge($aPages, $aModuleItems);
						$aPages[] = array('value'=>'/OPTGROUP');
					}
				}
				else {
					$aModuleItems = getModuleTree($aItems, $iMId, 0, '', ' / ', $aMenu['name'], $sSearch, $sOption);
					if (!empty($aModuleItems)) {
						$aPages = array_merge($aPages, $aModuleItems);
					}
				}
			}
		}
		return $aPages;
	} // end of getModulePages function
	
	
	/**
	 * Funkcja pobiera i zwraca tablice z linkami dla ston modulu $iMId
	 * i wersji jez. $iLangId
	 * 
	 * @param	integer	$iMId	- Id modulu
	 * @param	integer	$iLangId	- Id wersji jez.
	 * @return	array
	 */
	function getModulePagesLinks($iMId, $iLangId) {
		global $aConfig;
		$aPages = array();
		
		// pobranie wszystkich menu
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id = ".$iLangId;
		$aMenus =& Common::GetCol($sSql);
		foreach ($aMenus as $iMenuId) {
			$aItems = array();
			// pobranie wszystkich stron danego menu
			$sSql = "SELECT IFNULL(parent_id, 0) AS parent_id,
											IFNULL(module_id, 0) AS module_id, id, symbol AS name
							 FROM ".$aConfig['tabls']['prefix']."menus_items
							 WHERE language_id = ".$iLangId." AND
							 			 menu_id = ".$iMenuId; 
			$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
			if (!empty($aItems)) {
				$aModuleItems = getModuleTree($aItems, $iMId, 0, '', '/');
				if (!empty($aModuleItems)) {
					$aPages += $aModuleItems;
				}
			}
		}
		return $aPages;
	} // end of getModulePagesLinks() function
	
	
 	/**
   * Rekurencyjna funkcja tworzaca na podstawie danych z tablicy $aItems drzewo
   * elementow menu dla pola typu COMBO
   *
   * @param	array	$aItems	- tablica z nieposegregowanymi stronami
   * @param	integer	$iMaxLevel	- maksymalny poziom zaglebienia
   * @param	integer	$iLevel	- numer aktualnego poziomu zaglebienia
   * @param	integer	$iModId	- jezeli wieksze od 0, wszystkie elementy nieklikalne
   * 													(niemozliwe do wybrania)
   * @return	array	- uporzadkowana tablica z drzewem menu
   */
  function getComboMenuTree(&$aItems, $iMaxLevel=0, $iLevel=1, $iOmit=0, $iModId=0) {
		$aTree		= array();

		if ($iMaxLevel == 0 || $iMaxLevel > 1) {
	  	foreach($aItems as $iKey => $aItem) {
	  		if ($aItem['id'] != $iOmit) {
	  			//dump($iModId);
	  			if ($iModId > 0 && $iModId != intval($aItem['module_id'])) {
	  				// element nieaktywny
	  				//
						$aTree[] = array('value' => $aItem['id'],
														 'label' => str_repeat('&nbsp;', $iLevel*5).$aItem['name'],
														 'class' => 'disabled');
	  			}
	  			else {
	  				$aTree[] = array('value' => $aItem['id'],
														 'label' => str_repeat('&nbsp;', $iLevel*5).$aItem['name'],
	  													'title' => getPagePath($aItem['id'],$_SESSION['lang']['id'],true,false,false)
															);
	  			}
					if (isset($aItem['children']) && !empty($aItem['children'])) {
						if ($iMaxLevel == 0 || ($iLevel + 1) < $iMaxLevel) {
							$aTree = array_merge($aTree,
																	 GetComboMenuTree($aItem['children'], $iMaxLevel, $iLevel+1, $iOmit,$iModId));
						}
					}
	  		}
			}
		}
		return $aTree;
  } // end getComboMenuTree() function
  
  /**
   * Rekurencyjna funkcja tworzaca na podstawie danych z tablicy $aItems drzewo
   * elementow menu dla pola typu COMBO
   *
   * @param	array	$aItems	- tablica z nieposegregowanymi stronami
   * @param	integer	$iMaxLevel	- maksymalny poziom zaglebienia
   * @param	integer	$iLevel	- numer aktualnego poziomu zaglebienia
   * @param	integer	$iModId	- jezeli wieksze od 0, wszystkie elementy nieklikalne
   * 													(niemozliwe do wybrania)
   * @return	array	- uporzadkowana tablica z drzewem menu
   */
  function getCheckboxMenuTree(&$aItems, $iMaxLevel=0, $iLevel=1, $iOmit=0, $iModId=0) {
		$aTree		= array();
		
		if ($iMaxLevel == 0 || $iMaxLevel > 1) {
	  	foreach($aItems as $iKey => $aItem) {
	  		if ($aItem['id'] != $iOmit) {
	  				$aTreeElem = array('value' => $aItem['id'],
														 'label' => $aItem['name']);
					if (isset($aItem['children']) && !empty($aItem['children'])) {
						if ($iMaxLevel == 0 || ($iLevel + 1) < $iMaxLevel) {
							$aTreeElem['items'] = getCheckboxMenuTree($aItem['children'], $iMaxLevel, $iLevel+1, $iOmit,$iModId);
						}
					}
					$aTree[] = $aTreeElem;
	  		}
			}
		}
		return $aTree;
  } // end getComboMenuTree() function
  
  
	/**
   * Rekurencyjna funkcja tworzaca na podstawie danych z tablicy $aItems drzewo
   * elementow menu zwracajaca tylko elementy danego modulu.
   * Elementy innych modulow sa dolaczane tylko wtedy gdy wsrod ich 'potomkow'
   * znajduja sie elementy modulu $iModuleId
   *
   * @param	array	$aItems	- tablica z nieposegregowanymi stronami
   * @param	integer	$iStartFrom	- ID rodzica od ktorego rozpoczac tworzenie drzewa
   * @param	string	$sSymbol	- sciezka symboli galezi strony
   * @param	integer	$iModuleId	- Id modulu
   * @param	array	$aPrivileges	- tablica z uprawnieniami uzytkownika do stron
   * @return	array	- uporzadkowana tablica z drzewem menu
   */
  function getModuleMenuTree(&$aItems, $iStartFrom, $iModuleId, $aPrivileges) {
		global $aConfig;
		$aTree		= array();
		
  	if (!empty($aItems[$iStartFrom])) {
			foreach($aItems[$iStartFrom] as $iKey => $aItem) {
				if (($aItem['module_id'] == $iModuleId && empty($aItem['moption']) &&
						 (empty($aPrivileges) || isset($aPrivileges[$aItem['id']]))) ||
						(isset($aItems[$aItem['id']]) && hasModuleChildren($aItems, $aItem['id'],
																																			 $iModuleId,
																																			 $aPrivileges))) {
					// tworzenie struktury menu
					if (isset($aItem['mtype'])) {
						switch ($aItem['mtype']) {
							case '0':
								$aItem['link'] = $aItem['symbol'];
							break;
							
							case '1':
								$aItem['link'] = getPagePath($aItem['link_to_id'],
																										 $_SESSION['lang']['id'],
																										 true,
																										 true);
							break;
							
							case '2':
								$aItem['link'] = $aItem['url'];
							break;
						}
					}
	
					if (isset($aItems[$aItem['id']])) {
						$aItem['children'] = getModuleMenuTree($aItems,
      																						 $aItem['id'],
      																						 $iModuleId,
      																						 $aPrivileges);
	      		if (empty($aItem['children'])) {
	      			unset($aItem['children']);
	      		}
					}
					$aTree[] = $aItem;
				}
			}
		}
		return $aTree;
  } // end getModuleMenuTree() function



	/**
	 * @param $sMsg
	 * @param bool|true $bIsError
	 * @return string
	 */
	function GetPopupMessage($sMsg, $bIsError=true, $sTitle = '') {
		$sCSS = '';
		if ($sTitle === '') {
			if ($bIsError === true) {
				$sTitle = 'Błąd';
				$sCSS = 'error';
			} else {
				$sTitle = 'Powodzenie';
				$sCSS = 'success';
			}
		}
		$sHTML = '<script>
window.dialog = new Messi("'.$sMsg.'",
	{
		title: "'.$sTitle.'",
		titleClass: "'.$sCSS.'"
	}
);
$("body").prepend("<div id=\'opacity_messi\' style=\'background: black;width: 100%; height: 100%;display: block;position: fixed; opacity: .3;\'></div>");

$(document).ready(function(){
	$(document).on(\'click\', ".messi-closebtn", function(){
		$("#opacity_messi").remove();
	});
});


</script>';
		return $sHTML;
	}


  /**
   * Funkcja zwraca komunikat CMSa
   *
   * @param string $sMsg
   * @param bool $bIsError
   * @return	string	- kod HTML z komunikatem
   */
  function GetMessage($sMsg, $bIsError=true) {
    global $pSmarty;
    
    $sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="tableMargin">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 1px;">
						<tr>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
						<tr>
							<td height="1" colspan="3" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
						<tr>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
					</table>
					<table width="100%" height="50" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 1px;">
						<tr>
							<td width="50" align="center" valign="middle" style="background-color: #ffffff;"><img src="gfx/msg_'.intval($bIsError).'.gif" width="31" height="30" alt=""></td>
							<td style="background-color: #ffffff; padding: 10px 10px 10px 0px; vertical-align: bottom;" class="message'.intval($bIsError).'">'.stripslashes($sMsg).'</td>
						</tr>
					</table>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 1px;">
						<tr>
							<td height="2" colspan="3" class="brownLightBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
						<tr>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td height="1" class="brownLightBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
							<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>';
    return $sHtml;
  } // end GetMessage() function
  
  
  /**
   * Rekurencyjna funkcja zwracajac Id wszystkich podstron
   * typu $iType strony o podanym Id $iStartFrom 
   *
   * @param	array	$aItems	- tablica z nieposegregowanymi stronami
   * @param	integer	$iStartFrom	- ID rodzica od ktorego rozpoczac
   * @param	string	$iType	- typ podstron
   * @return	array	- uporzadkowana tablica z Id podstron
   */
  function getSubpages(&$aItems, $iStartFrom=0, $sType) {
		global $aConfig;
		$aTree		= array();
		
  	if (!empty($aItems[$iStartFrom])) {
			foreach($aItems[$iStartFrom] as $iKey => $aItem) {
				if ($aItem['mtype'] == $sType) {
					$aTree[] = $aItem['id'];
				}
				if (isset($aItems[$aItem['id']])) {
					$aTree = array_merge($aTree,
															 getSubpages($aItems,
      																						 $aItem['id'],
      																						 $sType));
				}
			}
		}
		return $aTree;
  } // end getSubpages() function
  
  
	/**
   * Rekurencyjna funkcja tworzaca na podstawie danych z tablicy $aItems drzewo
   * elementow menu
   *
   * @param	array	$aItems	- tablica z nieposegregowanymi stronami
   * @param	integer	$iStartFrom	- ID rodzica od ktorego rozpoczac tworzenie drzewa
   * @param	string	$sSymbol	- sciezka symboli galezi strony
   * @param	bool	$bForXML	- true: dla XMLa (usuwane sa wszystkie atrybuty oprocz
   * 																name i link)
   * 													false: nie usuwane sa zadne atrybuty
   * @return	array	- uporzadkowana tablica z drzewem menu
   */
  function getMenuTree(&$aItems, $iStartFrom=0) {
		global $aConfig;
		$aTree		= array();
		
  	if (!empty($aItems[$iStartFrom])) {
			foreach($aItems[$iStartFrom] as $iKey => $aItem) {
				// tworzenie linek stron menu
				if (isset($aItem['mtype'])) {
					switch ($aItem['mtype']) {
						case '0':
							$aItem['link'] = $aItem['symbol'];
						break;
						
						case '1':
							$aItem['link'] = getPagePath($aItem['link_to_id'],
																					 $_SESSION['lang']['id'],
																					 true,
																					 true);
						break;
						
						case '2':
							$aItem['link'] = $aItem['url'];
						break;
					}
					if ($aItem['new_window'] == '1') {
						$aItem['target'] = '_blank';
					}
				}

				if (isset($aItems[$aItem['id']])) {
					$aItem['children'] = getMenuTree($aItems, $aItem['id']);
				}
				$aTree[$iKey] = $aItem;
			}
		}
		return $aTree;
  } // end getMenuTree() function
  
  
	/**
   * Rekurencyjna funkcja tworzaca na podstawie danych z tablicy $aItems drzewo
   * elementow menu dla pliku XML
   *
   * @param	array	$aItems	- tablica z nieposegregowanymi stronami
   * @param	integer	$iStartFrom	- ID rodzica od ktorego rozpoczac tworzenie drzewa
   * @param	string	$sLang	- symbol wersji jez. dodawany do linek
   * @return	array	- uporzadkowana tablica z drzewem menu
   */
  function getMenuTreeForXML(&$aItems, $iStartFrom=0, $sLang='') {
		global $aConfig;
		$aTree		= array();
		
  	if (!empty($aItems[$iStartFrom])) {
			foreach($aItems[$iStartFrom] as $iKey => $aItem) {
				// tworzenie linek stron menu
				if (isset($aItem['mtype'])) {
					switch ($aItem['mtype']) {
						case '0':
							$aItem['link'] = '/'.(!empty($sLang) ? $sLang.'/' : '').
															 $aItem['symbol'];
						break;
						
						case '1':
							$aItem['link_to_id'] = Common::getLinkToId($aItem['link_to_id'],
																												 $_SESSION['lang']['id']);
																	 
							if (Common::isExternalLink($aItem['link_to_id'], $_SESSION['lang']['id'])) {
								$aItem['link'] = Common::getExternalLink($aItem['link_to_id'],
																												 $_SESSION['lang']['id']);
							}
							else {
								$aItem['link'] = '/'.(!empty($sLang) ? $sLang.'/' : '').
																 Common::getPageSymbol($aItem['link_to_id'],
																											 $_SESSION['lang']['id']);
							}
						break;
						
						case '2':
							$aItem['link'] = $aItem['url'];
						break;
					}
					if ($aItem['new_window'] == '1') {
						$aItem['target'] = '_blank';
					}
					if ($aItem['description'] == '') {
						$aItem['description'] = $aItem['description'];
					}
				}

				if (isset($aItems[$aItem['id']])) {
					$aItem['children'] = getMenuTreeForXML($aItems, $aItem['id'], $sLang);
				}

				$aTree[$iKey] = array(
					'id'	=> $aItem['id'],
					'name'	=> $aItem['name'],
					'symbol'	=> $aItem['symbol'],
					'link'	=> $aItem['link']
				);
				if (count($aItems[$iStartFrom]) === ($iKey + 1)) {
					// ostatni element na tym poziomie
					$aTree[$iKey]['last'] = '1';
				}
				if (isset($aItem['children'])) {
					$aTree[$iKey]['children'] = $aItem['children'];
				}
				if (isset($aItem['target'])) {
					$aTree[$iKey]['target'] = $aItem['target'];
				}
				if (isset($aItem['description'])) {
					$aTree[$iKey]['description'] = $aItem['description'];
				}
			}
		}
		return $aTree;
  } // end getMenuTreeForXML() function
  
  
	/**
	 * Rekurencyjna funkcja tworzaca na podstawie danych z tablicy $aItems drzewo
   * stron podanego modulu $iMId
   *
   * @param	array	$aItems	- tablica z nieposegregowanymi stronami
   * @param	integer	$iMId	- ID modulu
   * @param	integer	$iStartFrom	- ID rodzica od ktorego rozpoczac tworzenie drzewa
   * @param	string	$sPrevName	- sciezka nazw galezi strony
   * @param	string	$sSep	- separator stron
   * @param	string	$sMenuName	- nazwa menu
   * @param	string	$sSearch	- fraza wyszukiwania - zwracane strony zawierajace w nazwie
   * 														wyszukiwana fraze
   * @param	string	$sOptin	- opcja modulu
   * 
   * @return	array	- uporzadkowana tablica z drzewem stron modulu
   */
  function getModuleTree(&$aItems, $iMId, $iStartFrom=0, $sPrevName='', $sSep=' / ', $sMenuName='', $sSearch='', $sOption='') {
		global $aConfig;
		$aTree		= array();
		
  	if (!empty($aItems[$iStartFrom])) {
			foreach($aItems[$iStartFrom] as $iKey => $aItem) {
				$aItem['name'] = $aItem['name'];
				if (empty($aItem['moption'])) $aItem['moption'] = '';
				if ($aItem['module_id'] == $iMId && $aItem['moption'] == $sOption) {
					if ($sSearch == '' || stripos($aItem['name'], $sSearch) !== false) {
						$aTree[$aItem['id']] = array('value' => $aItem['id'],
																			 'label' => ($sMenuName != '' ? $sMenuName.': ' : '').$sPrevName.$aItem['name']);
					}
				}
				if (isset($aItems[$aItem['id']])) {
      		$aTree += getModuleTree($aItems,
																	$iMId,
	      													$aItem['id'],
	      													$sPrevName.$aItem['name'].$sSep,
	      													$sSep,
	      													$sMenuName,
	      													$sSearch,
	      													$sOption);
				}
			}
		}
		return $aTree;
  } // end getModuleTree() function
  
  
  /**
   * Funkcja sprawdza czy wsrod stron oraz ich podstron znajduje sie przynajmniej
   * jedna strona bedaca strona modulu o $iModuleId i uzytkownik posiada
   * do niej prawa jezeli tablica praw nie jest pusta
   * 
   * @param	array	$aItems	- tablica stron
   * @param	integer	$iStartFrom	- Id strony od ktorej dzieci
   * 															zacznie sie przeszukiwanie
   * @param	integer	$iModuleId - Id modulu
   * @param	array	$aPrivileges	- uprawnienia
   * @return	bool	true: znajduja; false: nie znajduja sie strony modulu	
   */
  function hasModuleChildren($aItems, $iStartFrom, $iModuleId, $aPrivileges) {
  	foreach ($aItems[$iStartFrom] as $iKey => $aItem) {
  		if (($aItem['module_id'] == $iModuleId && empty($aItem['moption']) &&
  				 (empty($aPrivileges) || isset($aPrivileges[$aItem['id']]))) ||
						(isset($aItems[$aItem['id']]) && hasModuleChildren($aItems, $aItem['id'], $iModuleId, $aPrivileges))) {
				return true;
			}
  	}
  	return false;
  } // end of hasModuleChildren() function
	

	/**
	 * Funkcja aktualizuje uzytkownika i czas modyfikacji rekordu
	 * 
	 * @param	string	$sTable	- nazwa tabeli w bazie danych
	 * @param	array	$aFields	- pola do zapytania WHERE
	 * @return	bool	- true: zmodyfikowano; false: blad
	 */
	function updateModificationTime($sTable, $aFields) {
		// aktualizacja czasu modyfikacji artykulu
		$aValues = array(
			'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name']
		);
		foreach ($aFields as $sKey => $iValue) {
			$sWhere .= " ".$sKey." = ".$iValue." AND";
		}
		$sWhere = substr($sWhere, 0, -4);
		return Common::Update($sTable,
											 		$aValues,
													$sWhere) !== false;
	} // end of updateModificationTime() function
	
	
	/**
	 * Funkcja pobiera domyslna konfiguracje modulu z pliku $sXMLFile
	 * 
	 * @param	string	$sXMLFile	- plik XML z konfiguracja
	 * @return	array	- konfiguracja
	 */
	function getModuleConfig($sXMLFile) {
		$aData = array();
		$aOptions = array(
			'complexType'       => 'array'
		);
		$pUS = new XML_Unserializer($aOptions);
		
		if (file_exists($sXMLFile)) {
			$pResult = $pUS->unserialize($sXMLFile, true);
			if (!PEAR::IsError($pResult)) {
	      $aData =& $pUS->getUnserializedData();
	    }
		}
		return $aData;
	} // end of setConfig() function
	
	
	/**
	 * Funkcja zapisuje domyslna konfiguracje modulu z $_POST do pliku $sXMLFile
	 * 
	 * @param	string	$sXMLFile	- plik XML z konfiguracja
	 * @return	bool	- true: zapisano; false: wystapil blad
	 */
	function writeModuleConfig($sXMLFile) {
		global $aConfig;
		
		// Serializer options
		$aOptions = array(
			'addDecl'	=> true,
			'encoding'	=> 'utf-8',
			'indent'	=>	"\t",
			'rootName'	=>	'config'
		);
		
		$oSerializer = new XML_Serializer($aOptions);
		// usuniecie zbednych danych
		$aData = $_POST;
		unset($aData['do']);
		unset($aData['__ErrPrefix']); 
		unset($aData['__ErrPostfix']);
		unset($aData['__Validator']);
		unset($aData['send']);
		$oResult = $oSerializer->serialize($aData);
		
		if (PEAR::isError($oResult)) {
			// blad serializacji
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($status->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
		}
		// sprawdzenie czy instnieje katalog konfiguracji
		if (!is_dir('config/'.$_GET['module'])) {
			if (!@mkdir('config/'.$_GET['module'])) {
				return false;
			}
		}

		// zapis do pliku XML
		if ($oFile = fopen($sXMLFile, 'w')) {
			flock($oFile, 2);
			if (fwrite($oFile, $oSerializer->getSerializedData())) {
				flock($oFile, 3);
				fclose($oFile);
			}
			else {
				// blad zapisu
				return false;
			}
		}
		return true;
	} // end of writeModuleConfig() function
	
	
  /**
   * Funkcja dodaje do bazy danych nowy log
   *
   * @param	string	$sLog	- tresc logu
   * @param	bool	$bIsError	- true: blad; false: komunikat
   * @param	integer   
   */
  function AddLog($sLog, $bIsError=true) {
    global $pDB, $aConfig;
		
		if (isset($_GET['do'])) {
			$sDo = "'".$_GET['do']."'";
		}
		elseif (isset($_POST['do'])) {
			$sDo = "'".$_POST['do']."'";
		}
		else {
			$sDo = 'NULL';
		}
		$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."events_log
						 (
							user_id,
							login,
							language_id,
							lang,
							file_name,
							module_symbol,
							action,
							do,
							error,
							message,
							added,
							session_id
						 )
						 VALUES (
							".$_SESSION['user']['id'].",
							'".$_SESSION['user']['name']."',
							".$_SESSION['lang']['id'].",
							'".$_SESSION['lang']['symbol']."',
							".(isset($_GET['file']) ? "'".$_GET['file']."'" : 'NULL').",
							".(isset($_GET['module']) ? "'".$_GET['module']."'" : 'NULL').",
							".(isset($_GET['action']) ? "'".$_GET['action']."'" : 'NULL').",
							".$sDo.",
							'".($bIsError ? '1' : '0')."',
							'".addslashes($sLog)."',
							NOW(),
							'".session_id()."'
						 )";
		Common::Query($sSql);
  } // end AddLog() function
  
  
 	/**
	 * Funkcja pobiera liste szablonow ze wskazanego katalogu
	 * i zwraca ja w postaci tablicy
	 * 
	 * @param	string	$sDirSrc	- sciezka do katalogu z szablonami
	 * @return	array	- tablica z lista szablonow
	 */
	function &GetTemplatesList($sDirSrc) {
		$aTemplates	= array();
		
		$pDir = opendir($sDirSrc);
  	while (false !== ($sFile = readdir($pDir))) {
    	if (preg_match('/^[a-z0-9]\w+\.tpl$/i', $sFile)) {
      	$aTemplates[] = array('value' => $sFile,
      												'name' => $sFile
      											 );
      }
    }
    closedir($pDir);
    sort($aTemplates);
    
    return $aTemplates;
	} // end of GetTemplatesList() function
	
	
	/**
	 * Funkcja przycina string do podanej dlugosci z obcieciem do ostatniej spacji
	 * 
	 * @param	string	$sStr	- string do obciecia
	 * @param	integer	$iLength	- liczba znakow do jakiej obciety bedzie string
	 * @return	string	- obciety string
	 */
	function trimString($sStr, $iLength) {
	  $sStr = mb_substr(strip_tags($sStr), 0, $iLength, 'utf8');
	  // obciecie do ostatniej spacji
	  if (mb_strlen($sStr, 'utf8') == $iLength) {
		  $sStr = mb_substr($sStr, 0, mb_strrpos($sStr, " ", 'utf8'), 'utf8');
		  $sStr .= "...";
		}
	  return $sStr;
	}
	
	
	function encodeHeaderString($sStr) {
		$sStr = stripslashes(strip_tags($sStr));
		return $sStr;
	}
	
	function decodeString($sStr) {
		$sStr = html_entity_decode($sStr, ENT_COMPAT);
		return $sStr;
	}
	
	
	/**
	 * Funkcja pobiera i zwraca pelna sciezke do strony o podanym Id
	 * 
	 * @param	integer	$iId	- Id strony dla ktorej tworzona jest sciezka
	 * @param	integer	$iLangId	- Id wersji jezykowej
	 * @param	bool	$bFullPath	- true: pelna sciezka; false: pierwszy i ostatni element
	 * @param	bool	$bSymbols	- true: sciezka symboli; false: sciezka nazw 
	 * @param	bool	$bMenu	- true: dodawaj nazwe menu na poczatku
	 */
	function getPagePath($iId, $iLangId, $bFullPath=false, $bSymbols=false, $bMenu=true) {
		$sPath = '';
		$aPathItems = getPathItems($iId, $iLangId, $bSymbols);
		if (!$bFullPath) {
			foreach ($aPathItems as $iKey => $sItem) {
				if (!$bMenu && $iKey == 0) {
					continue;
				}
				if ($iKey == 0) {
					$sPath .= '<span class="internalLink">'.$sItem.':</span> ';
				}
				elseif ($iKey == 1) {
					$sPath .= $sItem;
					if (($iKey + 1) < count($aPathItems)) {
						$sPath .= ' / ';
					}
				}
				elseif (($iKey + 1) < count($aPathItems)) {
					$sPath .= '... / ';
				}
				else {
					$sPath .= $sItem;
				}
			}
		}
		else {
			if ($bSymbols) {
				foreach ($aPathItems as $iKey => $sItem) {
					if ($iKey > 0) {
						$sPath .= $sItem.'/';
					}
				}
			}
			else {
				foreach ($aPathItems as $iKey => $sItem) {
					if (!$bMenu && $iKey == 0) {
						continue;
					}
					if ($iKey == 0) {
						$sPath .= $sItem.': ';
					}
					else {
						$sPath .= $sItem.' / ';
					}
				}
				$sPath = substr($sPath, 0, -3);
			}
		}
		return $sPath;
	} // end of getPagePath() method
	
	
	/**
	 * Funkcja pobiera id do strony modulu
	 * 
	 * @return	string
	 */
	function getModulePageId($sModule, $sOption='') {
		global $aConfig;
		
		$sSql = "SELECT A.id
						 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
						 JOIN ".$aConfig['tabls']['prefix']."modules AS B
						 ON A.module_id = B.id
						 WHERE B.symbol = '".$sModule."'".
									 ($sOption != '' ? " AND moption = '".$sOption."'" : '');
		return (int) Common::GetOne($sSql);
	} // end of getModulePageId() function
	
	
	/**
	 * Funkcja pobiera id modulu o podanym symbolu
	 * 
	 * @return	string
	 */
	function getModuleId($sSymbol) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE symbol = '".$sSymbol."'";
		return (int) Common::GetOne($sSql);
	} // end of getModuleId() function
	
	/**
	 * Funkcja pobiera id modulu o podanym symbolu
	 * 
	 * @return	string
	 */
	function getModuleIdM($sDb,$sSymbol) {
		global $aConfig,$pDbMgr;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE symbol = '".$sSymbol."'";
		return (int) $pDbMgr->GetOne($sDb,$sSql);
	} // end of getModuleId() function
	
	
	/**
	 * Funkcja pobiera id modulu strony o podanym Id
	 * 
	 * @return	string
	 */
	function getPageModuleId($iPId) {
		global $aConfig;
		
		$sSql = "SELECT module_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE id = ".$iPId;
		return (int) Common::GetOne($sSql);
	} // end of getPageModuleId() function
	
		
	/**
	 * 
	 */
	function &GetAllModulesList() {
		global $aConfig;
		
		$sSql = "SELECT id AS value, symbol AS name
						 FROM ".$aConfig['tabls']['prefix']."modules
						 ORDER BY symbol";
		$aModules =& Common::GetAll($sSql);
		foreach ($aModules as $iKey => $aItem) {
			if (isset($aConfig['lang']['c_modules'][$aItem['name']])) {
				$aModules[$iKey]['name'] =& $aConfig['lang']['c_modules'][$aItem['name']];
			}
		}
		return $aModules;
	} // end of GetAllModulesList() function
	
	/**
	 * 
	 */
	function &GetModulesList($sField='id') {
		global $aConfig;
		
		$sSql = "SELECT ".$sField." AS value, symbol AS name
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE menu = '1'
						 ORDER BY symbol";
		$aModules =& Common::GetAll($sSql);
		foreach ($aModules as $iKey => $aItem) {
			if (isset($aConfig['lang']['c_modules'][$aItem['name']])) {
				$aModules[$iKey]['name'] =& $aConfig['lang']['c_modules'][$aItem['name']];
			}
		}
		return $aModules;
	} // end of GetModulesList() function
	
	
	/**
	 * 
	 */
	function &GetShopOfferModulesIds() {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE symbol = 'm_oferta_produktowa' OR
						 			 symbol = 'm_lista_podkategorii'";
		return Common::GetCol($sSql);
	} // end of GetShopOfferModules() function

	/**
	 * sprawdz czy user ma uprawnienia do modulu
	 * @param $iModuleId
	 * @return unknown_type
	 */
	function hasModulePrivileges($iModuleId)
	{
		if ($_SESSION['user']['type'] === 0) {
						
			if($_SESSION['user']['privileges']['modules'][$iModuleId] === 0){ 
				return true;
			}	else{
				return false;
			}
		}
		else {
			return true;
		}

	}
	
function isbn2plain($sIsbn){
		return preg_replace('/[^0-9a-zA-Z]/', '', $sIsbn); 		
	}
	
	function link_encode($sStr) {
	return preg_replace('/-{2,}/', '-', preg_replace('/[\s]+/', '-', preg_replace('/[^-_\. \w]/', '', changeCharcodes(strip_tags($sStr), 'utf8', 'no'))));
}

/**
	 * Funkcja pobiera newsletter (kategoria / temat) o podanym Id
	 * 
	 * @param	integer	$iNId	- Id newslettera
	 * @return 	string
	 */
	function getNewsletter($iNId) {
		global $aConfig;
		
		$sSql = "SELECT CONCAT( IF( A.sent IS NULL , '', CONCAT( B.name, ' / ' ) ) , A.subject, IF( A.sent IS NULL , '', CONCAT( ' - ', A.sent ) ) )
						 FROM ".$aConfig['tabls']['prefix']."newsletters A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."newsletters_categories B
						 ON B.id = A.category_id
						 WHERE A.id = ".$iNId;
		return Common::GetOne($sSql);
	} // end of getNewsletter() function
	
	
	/**
	 * Funkcja aktualizuje uzytkownika i czas modyfikacji produktu
	 * 
	 * @param	string	$sTable	- nazwa tabeli w bazie danych
	 * @param	array	$aFields	- pola do zapytania WHERE
	 * @return	bool	- true: zmodyfikowano; false: blad
	 */
	function SetModification($sTable,$iPId) {
		global $aConfig;
		
		// aktualizacja czasu modyfikacji artykulu
		$aValues = array(
			'modified_by' => $_SESSION['user']['name'],
			'modified' => 'NOW()',
			'modiefied_by_service' => '1'
		);
		if(empty($_SESSION['_modified'][$sTable]) || !in_array($iPId,$_SESSION['_modified'][$sTable])){
			$_SESSION['_modified'][$sTable][]=$iPId;
		}
			$_SESSION['_last_modified'][$sTable]=$iPId;
		return Common::Update($aConfig['tabls']['prefix'].$sTable,
											 		$aValues,
													'id = '.$iPId) !== false;
	} // end of updateModificationTime() function
	
	
/**
 * Funkcja pobiera i zwraca zdjecie dla podanych danych
 * 
 * @param	string	$sTable	- nazwa tabeli w bazie danych
 * @param	array	$aFields	- pola dla ktorych ma byc wybieranie
 * @param	string	$sImg	- rodzaj zdjecia ktore ma byc pobrane:
 * 												'__t_' - miniaturka
 * 												'' - male
 * 												'__b_' - duze
 * @param	bool	$bSmall	- true: jezeli nie ma zdjecia podanego w $sImg
 * 												i $sImg nie jest puste - pobranie malego zdjecia
 * @param	bool	$bOrderBy	- true: korzystaj z sortowania po kolejnosci
 * @return	array	- dane zdjecia aktualnosci
 */
function &getItemImage($sTable, $aFields, $sImg='', $bSmall=true, $bOrderBy=true) {
	global $aConfig;
	
	$sSql = "SELECT directory, photo, mime
					 FROM ".$sTable."
				 	 WHERE";
	foreach ($aFields as $sKey => $iValue) {
		$sSql .= " ".$sKey." = ".$iValue." AND";
	}
	$sSql = substr($sSql, 0, -4);
	$sSql .= $bOrderBy ? " ORDER BY order_by LIMIT 0, 1" : '';
	$aImg =& Common::GetRow($sSql);

	if (!empty($aImg)) {
		$sDir = '/'.$aConfig['common']['photo_dir'];
		if (!empty($aImg['directory'])) {
			$sDir .= '/'.$aImg['directory'];
		}
		if ($aSize = @getimagesize($aConfig['common']['base_path'].$sDir.'/'.$sImg.$aImg['photo'])) {
			$aImg = array('src' => $sDir.'/'.$sImg.$aImg['photo'],
										'width' => $aSize[0],
										'height' => $aSize[1],
										'mime' => $aImg['mime']);
		}
		elseif ($sImg != '' && $bSmall) {
			if ($aSize = @getimagesize($aConfig['common']['base_path'].$sDir.'/'.$aImg['photo'])) {
				$aImg = array('src' => $sDir.'/'.$aImg['photo'],
											'width' => $aSize[0],
											'height' => $aSize[1],
											'mime' => $aImg['mime']);
			}
		}
	}
	return $aImg;
} // end of getItemImage() function

function createProductLink($iId, $sName='') {
	global $aConfig;
	
	$sPageLink .= '/';
	if($sName != ''){
		if(mb_strlen($sName, 'UTF-8') > 210) {
			$sPageLink .= mb_substr(link_encode($sName),0,210, 'UTF-8').',';
		} else {
			$sPageLink .= link_encode($sName).',';
		}
	}
	$sPageLink .= 'product'.$iId;
	$sPageLink .= '.html';
	return $sPageLink; 
} // end of createLink() function

/**
 * Funkcja pobiera ustawienia serwisu dla danej wersji jezykowej
 * oraz wersje CMSa
 * 
 * @return	array
 */
function &getSiteSettings($sWebsite = '') {
	global $aConfig, $pDbMgr;
	$aSettings = array();
	
	if ($sWebsite != '') {
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."website_settings
						WHERE language_id = '".$_GET['lang_id']."'";
		$aSettings =& $pDbMgr->GetRow($sWebsite, $sSql);
		if (empty($aSettings['subtitle'])) {
			$aSettings['subtitle'] = $aSettings['title'];
		}

		$sSql = "SELECT name, version
						FROM ".$aConfig['tabls']['prefix']."version";
		$aSettings['product'] =& $pDbMgr->GetRow($sWebsite, $sSql);

		return $aSettings;
	} else {
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."website_settings
						WHERE language_id = '".$_GET['lang_id']."'";
		$aSettings =& Common::GetRow($sSql);
		if (empty($aSettings['subtitle'])) {
			$aSettings['subtitle'] = $aSettings['title'];
		}

		$sSql = "SELECT name, version
						FROM ".$aConfig['tabls']['prefix']."version";
		$aSettings['product'] =& Common::GetRow($sSql);

		return $aSettings;
	}
} // end of getSiteSettings() function
	// dla funkcji czesci klienckiej wywolywanych z cms
	function isLoggedIn() {
		return false;
	}
	
	/**
 * Funkcja dekoduje haslo
 * 
 * @param		string	$sString	- ciag do zdekodowania
 * @return	string
 */
function decodePasswd($sString) {
	global $aConfig;
	return rc4($aConfig['common']['rc4_key'], base64_decode($sString));
} // end of decodePasswd() function
	


/**
 * Crypt/decrypt strings with RC4 stream cypher algorithm.
 *
 * @param string $key Key
 * @param string $data Encripted/pure data
 * @see   http://pt.wikipedia.org/wiki/RC4
 * @return string
 */
function rc4($key, $data) {
    // Store the vectors "S" has calculated
    static $SC;
    // Function to swaps values of the vector "S"
    $swap = create_function('&$v1, &$v2', '
        $v1 = $v1 ^ $v2;
        $v2 = $v1 ^ $v2;
        $v1 = $v1 ^ $v2;
    ');
    $ikey = crc32($key);
    if (!isset($SC[$ikey])) {
        // Make the vector "S", basead in the key
        $S    = range(0, 255);
        $j    = 0;
        $n    = strlen($key);
        for ($i = 0; $i < 255; $i++) {
            $char  = ord($key{$i % $n});
            $j     = ($j + $S[$i] + $char) % 256;
            $swap($S[$i], $S[$j]);
        }
        $SC[$ikey] = $S;
    } else {
        $S = $SC[$ikey];
    }
    // Crypt/decrypt the data
    $n    = strlen($data);
    $data = str_split($data, 1);
    $i    = $j = 0;
    for ($m = 0; $m < $n; $m++) {
        $i        = ($i + 1) % 256;
        $j        = ($j + $S[$i]) % 256;
        $swap($S[$i], $S[$j]);
        $char     = ord($data[$m]);
        $char     = $S[($S[$i] + $S[$j]) % 256] ^ $char;
        $data[$m] = chr($char);
    }
    return implode('', $data);
} // end of rc4() function

function createLink($sPageLink, $sAction='', $sItem='', $aParameters=array()) {
	global $aConfig;
	
	$sPageLink .= '/';		
	$sPageLink .= $sAction != '' ? $sAction.',' : '';
	$sPageLink .= $sItem != '' ? link_encode($sItem).',' : '';
	foreach ($aParameters as $sKey => $mValue) {
		$sPageLink .= $sKey.':'.$mValue.',';
	}
	$sPageLink = substr($sPageLink, 0, -1).'.html';
	return $sPageLink; 
} // end of createLink() function


/**
* Converts a PHP array to a JavaScript array
*
* Takes a PHP array, and returns a string formated as a JavaScript array
* that exactly matches the PHP array.
*
* @param       array  $phpArray  The PHP array
* @param       string $jsArrayName          The name for the JavaScript array
* @return      string
*/
function get_javascript_array($phpArray, $jsArrayName, &$html = '') {
	$html .= $jsArrayName . " = new Array(); \r\n ";
	foreach ($phpArray as $key => $value) {
		$outKey = (is_int($key)) ? '[' . $key . ']' : "['" . $key . "']";
		
		if (is_array($value)) {
			get_javascript_array($value, $jsArrayName . $outKey, $html);
			continue;
		}
		$html .= $jsArrayName . $outKey . " = ";
		if (is_string($value)) {
			$html .= "'" . $value . "'; \r\n ";
		} else if ($value === false) {
			$html .= "false; \r\n";
		} else if ($value === NULL) {
			$html .= "null; \r\n";
		} else if ($value === true) {
			$html .= "true; \r\n";
		} else {
			$html .= $value . "; \r\n";
		}
	}
	
	return $html;
}


function doRedirect($sLocation='') {
	global $aConfig;
	if (strpos($sLocation, $aConfig['common']['client_base_url_https']) !== false) {
		$sLocation = substr($sLocation, strlen($aConfig['common']['client_base_url_https']));
	}

	if (strpos($sLocation, $aConfig['common']['client_base_url_http']) !== false) {
	$sLocation = substr($sLocation, strlen($aConfig['common']['client_base_url_http']));
	}
  
	header('Location: '.$aConfig['common']['client_base_url_https'].html_entity_decode(preg_replace('/^\//', '', $sLocation)));
	exit();
}

function addDefaultValue(&$aList, $sLabel = NULL, $sValue = NULL) {
  global $aConfig;
  
  $aFirstItem = array(array('label' => (isset($sLabel) ? $sLabel : $aConfig['lang']['common']['choose']), 'value' => (isset($sValue) ? $sValue : '')));
  if (!empty($aList) && is_array($aList)) {
    $aList = array_merge($aFirstItem, $aList);
  } else {
    $aList = $aFirstItem;
  }
  return $aList;
}

if (!function_exists('setSessionMessage')) {
  function setSessionMessage($sMsg, $bIsErr=false) {

    $_SESSION['_tmp_cms']['message']['text'] = $sMsg;
    $_SESSION['_tmp_cms']['message']['error'] = $bIsErr;
  } // end of setMessage() function
}

?>