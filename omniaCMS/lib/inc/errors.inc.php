<?php

function show_error($msg)
{
	echo $msg;
	exit;
}


/**
 * error_handler()
 * 
 * Funkcja obsluguje bledy aplikacji.
 * W zaleznosci od konfiguracji wyswietla bledy na ekran lub loguje do wskazanego pliku
 * 
 * @param type $nr
 * @param type $msg
 * @param type $file
 * @param type $line 
 */
function error_handler($nr, $msg, $file, $line)
{
	global $aConfig;
	
	if (error_reporting() == 0) {
			return;
	}

	// tresc komunikatu do logu
	$log_record = "****************************** %s %s ******************************
* URL						: %s
* MESSAGE				: (%s) %s
* LINE AND FILE	: (%s) %s
* IP & HOST			: (%s) %s
* BACKTRACE:
%s*
* SUPERGLOBALS:
******************************************************************************************
%s
******************************************************************************************
******************************************************************************************\n\n";
	
	// tresc komuikatu do maila
	$mail_record = '
	<div class="error_report" style="font-size: 10px; border: 1px solid #666; background: #ccc; padding-bottom: 5px; margin-bottom: 5px;">
		<h1 style="padding: 5px 0 0 5px; margin: 0; font-size: 14px; font-weight: normal; cursor: pointert;"><b><u>%s!</u></b>&nbsp;&nbsp;&nbsp;&nbsp;<b>LINE:</b> %s <b>MESSAGE:</b> (%s) %s</h1>
		<div style="padding: 5px 10px; border-top: 1px solid #666 ; background: #eee;">
			<table>
				<tr><td>URL: &nbsp;&nbsp;</td><td>%s</td></tr>
				<tr><td>FILE: &nbsp;&nbsp;</td><td>%s</td></tr>
				<tr><td>IP & HOST: &nbsp;&nbsp;</td><td>(%s) %s</td></tr>
			</table>
			<strong style="font-size: 14px;">BACKTRACE:</strong>
			<table style="font-size: 12px; margin-left: 15px;">
			%s
			</table>
			<strong style="font-size: 14px;">SUPERGLOBALS:</strong>
			<h2 style="padding: 5px 0 0 5px; margin-top: 5px; font-size: 12px; font-weight: normal; background: #ccc; border: 1px solid #666; cursor: pointer;">SESSION:</h2>
			<div style="padding-left: 10px; font-size: 12px; margin: 0 0 4px 10px; border-left: 1px solid #666;">
				<pre>%s</pre>
			</div>
			<h2 style="padding: 5px 0 0 5px; margin-top: 5px; font-size: 12px; font-weight: normal; background: #ccc; border: 1px solid #666; cursor: pointer;">GET:</h2>
			<div style="padding-left: 10px; font-size: 12px; margin: 0 0 4px 10px; border-left: 1px solid #666;">
				<pre>%s</pre>
			</div>
			<h2 style="height: 20px; padding: 5px 0 0 5px; margin-top: 5px; font-size: 12px; font-weight: normal; background: #ccc; border: 1px solid #666; cursor: pointer;">POST:</h2>
			<div style="padding-left: 10px; font-size: 12px; margin: 0 0 4px 10px; border-left: 1px solid #666;">
				<pre>%s</pre>
			</div>
			<h2 style="height: 20px; padding: 5px 0 0 5px; margin-top: 5px; font-size: 12px; font-weight: normal; background: #ccc; border: 1px solid #666; cursor: pointer;">FILES:</h2>
			<div style="padding-left: 10px; font-size: 12px; margin: 0 0 4px 10px; border-left: 1px solid #666;">
				<pre>%s</pre>
			</div>
		</div>
	</div>';

	// tresc komuikatu do wyswietlenia na ekranie
	$screen_record = '
	<div class="error_report" style="font-size: 10px; border: 1px solid #666; background: #ccc; padding-bottom: 5px; margin-bottom: 5px;">
		<h1 onclick="var errorhandlerel = this.nextSibling.nextSibling; if( errorhandlerel.style.display == \'none\') { errorhandlerel.style.display = \'block\'; } else { errorhandlerel.style.display = \'none\'; }" style="padding: 5px 0 0 5px; margin: 0; font-size: 14px; font-weight: normal; cursor: pointer;"><b><u>%s!</u></b>&nbsp;&nbsp;&nbsp;&nbsp;<b>LINE:</b> %s <b>MESSAGE:</b> (%s) %s</h1>
		<div style="padding: 5px 10px; display: none; border-top: 1px solid #666; background: #eee;">
			<table>
				<tr><td>URL: &nbsp;&nbsp;</td><td>%s</td></tr>
				<tr><td>FILE: &nbsp;&nbsp;</td><td>%s</td></tr>
				<tr><td>IP & HOST: &nbsp;&nbsp;</td><td>(%s) %s</td></tr>
			</table>
			<strong style="font-size: 14px;">BACKTRACE:</strong>
			<table style="font-size: 12px; margin-left: 15px;">
			%s
			</table>
			<strong style="font-size: 14px;">SUPERGLOBALS:</strong>
			<h2 onclick="var errorhandler2el = this.nextSibling.nextSibling; if( errorhandler2el.style.display == \'none\') { errorhandler2el.style.display = \'block\'; } else { errorhandler2el.style.display = \'none\'; }" style="padding: 5px 0 0 5px; margin-top: 5px; font-size: 12px; font-weight: normal; background: #ccc; border: 1px solid #666; cursor: pointer;">SESSION:</h2>
			<div style="padding-left: 10px; font-size: 12px; display: none; margin: 0 0 4px 10px; border-left: 1px solid #666;">
				<pre>%s</pre>
			</div>
			<h2 onclick="var errorhandler2el = this.nextSibling.nextSibling; if( errorhandler2el.style.display == \'none\') { errorhandler2el.style.display = \'block\'; } else { errorhandler2el.style.display = \'none\'; }" style="padding: 5px 0 0 5px; margin-top: 5px; font-size: 12px; font-weight: normal; background: #ccc; border: 1px solid #666; cursor: pointer;">GET:</h2>
			<div style="padding-left: 10px; font-size: 12px; display: none; margin: 0 0 4px 10px; border-left: 1px solid #666;">
				<pre>%s</pre>
			</div>
			<h2 onclick="var errorhandler2el = this.nextSibling.nextSibling; if( errorhandler2el.style.display == \'none\') { errorhandler2el.style.display = \'block\'; } else { errorhandler2el.style.display = \'none\'; }" style="padding: 5px 0 0 5px; margin-top: 5px; font-size: 12px; font-weight: normal; background: #ccc; border: 1px solid #666; cursor: pointer;">POST:</h2>
			<div style="padding-left: 10px; font-size: 12px; display: none; margin: 0 0 4px 10px; border-left: 1px solid #666;">
				<pre>%s</pre>
			</div>
			<h2 onclick="var errorhandler2el = this.nextSibling.nextSibling; if( errorhandler2el.style.display == \'none\') { errorhandler2el.style.display = \'block\'; } else { errorhandler2el.style.display = \'none\'; }" style="padding: 5px 0 0 5px; margin-top: 5px; font-size: 12px; font-weight: normal; background: #ccc; border: 1px solid #666; cursor: pointer;">FILES:</h2>
			<div style="padding-left: 10px; font-size: 12px; display: none; margin: 0 0 4px 10px; border-left: 1px solid #666;">
				<pre>%s</pre>
			</div>
		</div>
	</div>';
	
	switch ($nr)
	{
		case E_ERROR:
			$error_lvl = 'ERROR';
		break;

		case E_WARNING:
			$error_lvl = 'WARNING';
		break;

		case E_PARSE:
			$error_lvl = 'PARSE';
		break;

		case E_NOTICE:
			$error_lvl = 'NOTICE';
		break;

		case E_USER_ERROR:
			$error_lvl = 'USER ERROR';
		break;

		case E_USER_WARNING:
			$error_lvl = 'USER WARNING';
		break;

		case E_USER_NOTICE:
			$error_lvl = 'USER NOTICE';
		break;

		case E_USER_DEPRECATED:
			$error_lvl = 'USER DEPRECATED';
		break;

		case E_STRICT:
			$error_lvl = 'STRICT';
		break;

		case E_RECOVERABLE_ERROR:
			$error_lvl = 'RECOVERABLE ERROR';
		break;

		case E_DEPRECATED:
			$error_lvl = 'DEPRECATED';
		break;	

		case E_CORE_WARNING:
			$error_lvl = 'CORE WARNING';
		break;	

		case E_COMPILE_WARNING:
			$error_lvl = 'COMPILE WARNING';
		break;

		default:
			$error_lvl = 'CUSTOM';
	}
		
	// zrzut zawartosci zmiennych $_SESSION, $_POST, $_GET, $_FILES
	$_SESSION = (isset($_SESSION)) ? $_SESSION : array();

	$superglobals = "SESSION: ".var_export($_SESSION, true)."\n".
			  "GET: ".var_export($_GET, true)."\n".
			  "POST: ".var_export($_POST, true)."\n".
			  "FILES: ".var_export($_FILES, true);
	

	// zrzut zawartosci backtrace
	$date = date('d.m.Y G:i:s');
	$backtrace = debug_backtrace();	
	$backtrace = (is_array($backtrace)) ? $backtrace : array();
	$backtrace = array_reverse($backtrace);
	
	$backtrace_to_string = '';
	$calls_array = array();
	$calls_output = '';
	
	foreach($backtrace as $k => $v) {
		$call = '';
		$call .= (array_key_exists('file', $v)) ? $v['file'] : '';
		$call .= (array_key_exists('line', $v)) ? ' ('.$v['line'].') ' : '';
		$call .= (array_key_exists('class', $v)) ? $v['class'] : '';
		$call .= (array_key_exists('type', $v)) ? $v['type'] : '';
		$call .= (array_key_exists('function', $v)) ? $v['function'] : '';
		$backtrace_to_string .= sprintf("*\t\t%s\n", $call);
		$calls_output .= "<tr><td>$call</td></tr>";
	}
	
	// zapis do logu
	if ($aConfig['err_handler']['log_errors'] === TRUE &&
			in_array($nr, $aConfig['err_handler']['log_errors_type']) && 
			is_dir(dirname($aConfig['err_handler']['log_dir'])) &&
			is_writable(dirname($aConfig['err_handler']['log_dir']))) {

		$error = sprintf($log_record, $error_lvl, $date, $_SERVER['REQUEST_URI'], $nr, $msg, $line, $file, $_SERVER['REMOTE_ADDR'].':'.$_SERVER['REMOTE_PORT'], $_SERVER['HTTP_HOST'], $backtrace_to_string, $superglobals);
		
		// zapis logu do pliku
		file_put_contents($aConfig['err_handler']['log_dir'].'/errors_'.date('dmY').'.log', $error, FILE_APPEND);
	} // zapis do pliku
	
	// powiadomienie mailowe
	if ($aConfig['err_handler']['mail_errors'] === TRUE &&
			in_array($nr, $aConfig['err_handler']['mail_errors_type']) && 
			$aConfig['err_handler']['mail_errors_to'] != '') {
		
		$html_output = sprintf($mail_record, $error_lvl, $line, $nr, $msg,
										$_SERVER['REQUEST_URI'],
										$file,
										$_SERVER['REMOTE_ADDR'].':'.$_SERVER['REMOTE_PORT'], $_SERVER['HTTP_HOST'],
										$calls_output,
										var_export($_SESSION, true),
										var_export($_GET, true),
										var_export($_POST, true),
										var_export($_FILES, true)
		);
		/*
		// wysylanie maila
		mail($aConfig['err_handler']['mail_errors_to'],
				sprintf($aConfig['err_handler']['mail_errors_subject'], $error_lvl),
				$html_output,
				'MIME-Version: 1.0'."\r\n".
				'Content-type: text/html; charset=utf-8'."\r\n".
				'From: '.$aConfig['err_handler']['mail_errors_from']."\r\n".
				'Reply-To: '.$aConfig['err_handler']['mail_errors_from']."\r\n".
				'X-Mailer: PHP/' . phpversion());
     * 
     */
	} // powiadomienie mailowe
	
	if ($aConfig['err_handler']['show_errors'] === TRUE &&
			in_array($nr, $aConfig['err_handler']['display_errors']))
	{
		$html_output = sprintf($screen_record, $error_lvl, $line, $nr, $msg,
										$_SERVER['REQUEST_URI'],
										$file,
										$_SERVER['REMOTE_ADDR'].':'.$_SERVER['REMOTE_PORT'], $_SERVER['HTTP_HOST'],
										$calls_output,
										var_export($_SESSION, true),
										var_export($_GET, true),
										var_export($_POST, true),
										var_export($_FILES, true)
		);

		echo $html_output;
	}
} // end of error_handler() function

if ($aConfig['err_handler']['use_error_handler'] === TRUE)
{	
	// usrawienie funkcji error_handler jako obslugujacej bledy
	set_error_handler('error_handler');
}
?>