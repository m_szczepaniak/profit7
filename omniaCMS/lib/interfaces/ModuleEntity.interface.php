<?php
/**
 * Interfejs modułu, obsługującego Encje
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
namespace omniaCMS\lib\interfaces;
interface ModuleEntity {
  
  public function getMsg();
  
  public function doDefault();
  
  function __construct(\Admin $oClassRepository);
}