<?php

/**
 * Klasa Validator do walidacji formularza
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
class Validator {
  // kod JavaScript do walidacji formularza
  var $sError;
	
	/**
   * Konstruktor klsy Validator
	 * @return void
   */
  function Validator() {
		$this->sError	= '';
	} // end of Validator() method


	/**
	 * Metoda waliduje poprawnosc wypelnienia formularza
	 * 
   * @return	bool			true: poprawnie wypelniony; false: niepoprawnie wypelniony
	 */
	function Validate($aInput) {
		if (!empty($aInput['__Validator'])) {
			$aInput['__Validator'] = unserialize(base64_decode($aInput['__Validator']));
			//dump($aInput['__Validator']);
			//dump($_POST);
			//dump($Input['__Validator']);
			foreach ($aInput['__Validator'] as $sName => $aData) {
        $sVarName = '';
        $mValue = $this->getPoolData($sName, $sVarName);

        // czy pole jest wymagane
				if (!isset($aData['depends_on']) || 
            ($this->checkIsRequired($aData) === TRUE)) {

					switch ($aData['type']) {
						case 'confirm_password':
							// filtrowanie
							preg_match('/^([\w-]+)/', $aData['passwd_field'], $aMatches);
							$aData['passwd_field'] = $aMatches[1];
							if (strpos($aData['passwd_field'], '[') != false) {
								$sPasswdName = substr($aData['passwd_field'], 0, strpos($aData['passwd_field'], '['));
								$sPasswdTableKeys = str_replace('[]', ' ', substr($aData['passwd_field'],
																									strpos($aData['passwd_field'], '[')));
							}
							else {
								$sPasswdName = $aData['passwd_field'];
								$sPasswdTableKeys = '';
							}
							eval('$sPasswdValue =& $_POST["'.$sPasswdName.'"]'.$sPasswdTableKeys.';');
							if (!empty($sPasswdValue)) {
								if ($mValue != $sPasswdValue) {
									$this->sError .= '<li>'.$aData['err_msg'].'</li>';
								}
							}
						break;
						
						case 'file':
							if (!isset($_FILES[$sVarName]['name']) || empty($_FILES[$sVarName]['name'])) {
								$this->sError .= '<li>'.$aData['err_msg'].'</li>';
							}
						break;
						
						case 'radio_set':
							if ($mValue == '') {
								$this->sError .= '<li>'.$aData['err_msg'].'</li>';
							}
						break;
						/*
						case 'isbn': {
							$sPureIsbn=str_replace('-', '', $mValue);
							if(strlen($sPureIsbn)==13) {
								//walidacja nowego isbn
								$remainder = $sPureIsbn[12]=='X'?10:$sPureIsbn[12];
				        $checksum  = 0;
				 
				        for ($i=0; $i<12; $i++) {
				            $multi     = (($i % 2) == 1) ? 3 : 1;
				            $digit     = (int) $sPureIsbn[$i];
				            $checksum += $digit * $multi;
				        }
				 
				        $bValide = ((10 - ($checksum % 10)) == $remainder);
				 
				        if (!$bValide) 
									$this->sError .= '<li>'.$aData['err_msg'].'</li>';
							}
							elseif(strlen($sPureIsbn)==10) {
								//walidacja starego isbn
								$remainder = $sPureIsbn[9]=='X'?10:$sPureIsbn[9];
								$remainder = $remainder==0?11:$remainder;
				        $checksum  = 0;
				 	
				        for ($i=10, $a=0; $i>1; $i--, $a++) {
			            $digit     = (int) $sPureIsbn[$a];
			            $checksum += $digit * $i;
				        	}
				 
				        $bValide = ((11 - ($checksum % 11)) == $remainder);
				 				if(!$bValide)
									$this->sError .= '<li>'.$aData['err_msg'].'</li>';
							}	
							else
									$this->sError .= '<li>'.$aData['err_msg'].'</li>';
						}break;*/
            
            case 'token':
              if ($mValue != '') {// czy token jest wyciagniety do widoku i przesłany w innym przypadku byłby wypełniony, albo wcześniej zwalidowane
                if (isset($_SESSION[$sName])) {// no to tutaj w sesji na pewno ten element powinien siedzieć, skoro przeszedł postem
                  if (isset($aData['pcre']) && !empty($aData['pcre'])) {
                    if (isset($aData['required']) && $aData['required'] == true) {
                      // jezeli pole jest nieobowiazkowe, ale zostalo wypelnione i istnieje pcre
                      // sprawdzenie zgodnosci z pcre
                      if ($mValue != '' && !preg_match($aData['pcre'], $mValue)) {
                        $this->sError .= '<li>'.$aData['err_msg'].'</li>';
                      } else {
                        if ($mValue !== $_SESSION[$sName]) {
                          $this->sError .= '<li>'.$aData['err_msg'].'</li>';
                        }
                      }
                    }
                  }
                  unset($_POST[$sName]);// nie potrzebujemy w dalszej części tego w POST
                  unset($_SESSION[$sName]);
                } else {
                  $this->sError .= '<li>'.$aData['err_msg'].'</li>';
                }
              }// jesli token pusty to znaczy ze nikt go nie wyciaga do widoku
            break;
						
						default:
							if (isset($aData['pcre']) && !empty($aData['pcre'])) {
								if (isset($aData['required']) && !$aData['required']) {
									// jezeli pole jest nieobowiazkowe, ale zostalo wypelnione i istnieje pcre
									// sprawdzenie zgodnosci z pcre
									if ($mValue != '' && !preg_match($aData['pcre'], $mValue)) {
										$this->sError .= '<li>'.$aData['err_msg'].'</li>';
									}
								}
								elseif (!preg_match($aData['pcre'], $mValue)) {
									$this->sError .= '<li>'.$aData['err_msg'].'</li>';
								}
							}
							elseif (isset($aData['range']) && !empty($aData['range'])) {
								$mValue = floatval(str_replace(',', '.', $mValue));
								if ($mValue < $aData['range'][0] || $mValue > $aData['range'][1]) {
									$this->sError .= '<li>'.$aData['err_msg'].'</li>';
								}
							}
							elseif ($mValue == '') {
								$this->sError .= '<li>'.$aData['err_msg'].'</li>';
							}
						break;
					}
				}
			}
			if (!empty($this->sError)) {
				$this->sError = $aInput['__ErrPrefix'].
					'<ul class="formError">'.
					$this->sError.
					'</ul>'.
					$aInput['__ErrPostfix'];
				return false;
			}
		}
		return true;
	} // end Validate()
  
  
  /**
   * Metoda sprawdza czy pole jest wymagane
   * 
   * @param array $aData
   */
  public function checkIsRequired($aData) {
    
    // czy pole depends_on zostało serializowane i jest wymagane
    $bIsRequired = false;
    if (isset($aData['depends_on']) && $aData['depends_on'] !== '') {
      $aSerializedOnDependsOn = $this->_checkIsDependsOnSerialized($aData['depends_on']);
      if (!empty($aSerializedOnDependsOn)) {
        // poole serializowane
        $bIsRequired = $this->_checkSerializedOnDependsOnRequire($aSerializedOnDependsOn);
      } else {
        // w takim razie zwykłe pole
        $this->getPoolData($aData['depends_on']);
        $mValue = $this->getPoolData($aData['depends_on']);
        $bIsRequired = (isset($aData['depends_on']) && isset($mValue) && (!isset($aData['depends_val']) || ($mValue == $aData['depends_val'])));
      }
    }
    return $bIsRequired;
  }// end of checkIsRequired() method
  
  
  /**
   * Metoda sprawdza czy dane w post zgadzają sie z serializowana tablica danych, 
   *  jeśli wszystkie dane się zgadzają pole staje się wymagane
   * 
   * Metoda rekurencyjna jeśli przekazano tablice wielowymiarową
   * 
   * @version 1.1
   * @param array $aSerializedOnDependsOn przyjmuje array('nazwa_pola' => 'wartość') LUB 
   *    array(
   *      array('nazwa_pola' => 'wartość', (&&) 'nazwa_pola2' => 'wartość2')
   *      (||)
   *      array('nazwa_pola3' => 'wartość3',(&&) 'nazwa_pola4' => 'wartość4')
   *    )
   * @param array $aPost
   */
  private function _checkSerializedOnDependsOnRequire($aSerializedOnDependsOn) {
    
    $bIsRequired = false; // domyślnie nie jest wymagane
    foreach ($aSerializedOnDependsOn as $sPool => $mVal) {
      if (!empty($mVal) && is_array($mVal)) {
        if ($this->_checkSerializedOnDependsOnRequire($mVal) === true) {
          return true; // dowolna tablica zawarta wskazuje, ze pole powinno byc wymagane
        }
      } else {
        if ($this->getPoolData($sPool) != $mVal) {
          // jest dowolne inna wartość pola, nie jest wymagane
          return false;
        } else {
          // jest wymagane
          $bIsRequired = true;
        }
      }
    }
    
    return $bIsRequired; // wszystkie wartości są wymagane, lub nie wystąpiła żadna i pole nie jest wymagane
  }// end of _checkSerializedOnDependsOnRequire() method
  
  
  /**
   * Metoda sprawdza czy przekazane dane do onDependsOn są serializowaną tablicą
   * 
   * @param type $mData
   * @return mixed array|bool
   */
  private function _checkIsDependsOnSerialized($mData) {
    if (isset($mData) && $mData !== '') {
      $aData = @unserialize($mData);
      if (!empty($aData) && is_array($aData)) {
        return $aData;
      }
      return false;
    }
    return false;
  }// end of checkIsDependsOnSerialized() method
	
	
  /**
   * Metoda wyłuskuje pole z POSTA, na podstawie nazwy elementu,
   *  obsługuje tablice wielowymiarowe
   * 
   * @param string $sName
   * @return mixed
   */
  public function getPoolData($sName, &$sVarName = '') {
    $mValue = NULL;

    // filtrowanie
    $aMatches = array();
    preg_match('/^([\w-\[\]]+)/', $sName, $aMatches);
    $sName = $aMatches[1];
    if (strpos($sName, '[') != false) {
      $sVarName = substr($sName, 0, strpos($sName, '['));
      $sTableKeys = str_replace('[]', ' ', substr($sName, strpos($sName, '[')));
    }
    else {
      $sVarName = $sName;
      $sTableKeys = '';
    }
    eval('$mValue = $_POST["'.$sVarName.'"]'.$sTableKeys.';');
    if (!is_array($mValue)) {
      $mValue = trim($mValue);
    }
    return $mValue;
  }// end of getPoolData() method
  
  
	/**
	 * Metoda zwraca komunikat bledu
	 * 
	 * @return	string	$sError
	 */
	function GetErrorString() {
		return $this->sError;
	} // end GetErrorString()
} // end of class Validator
?>