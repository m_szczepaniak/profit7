<?php
/**
 * Klasa FormTable do obslugi formularzy zwracanych w postaci tabelki HTML
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2004 - 2006
 * @version   1.0
 */

// dolaczenie podstawowej klasy Form 
include_once('Form/Form.class.php');

class FormTable {
  // tablica zawierajaca atrybutu calego formularza
	var $aFormAttribs;
	
	// tablica zawierajaca elementy formularza
  var $aElements;
	
	// tablica zawierajaca atrybuty tabelki HTML
  var $aTableAttribs;
  
  // tablica zawierajaca elementy formularza hidden
  var $aHiddenElements;
  
  // dodatkowy kod JavaScript
  var $aJavaScript;
	
	// tablica zawierajaca dane dla walidatora po stronie serwera
	var $aValidator;
  
  // liczba elementow typu date - z kalendarzem JS
  var $iCalendars;
  
  // kod JavaScript do walidacji formularza
  var $sJS;
  var $sAdditionalJS;
	
	// nazwa formularza
	var $sName;
	
	// naglowek tabeli
	var $sTableHeader;
	
	// szerokosc pierwszej kolumny tabeli HTML
	var $iColWidth;
	
	// uzywac/nieuzywac walidacji JS
	var $bUseJS;
	
	// tablica z atrybutami dla komorek pierwszej kolumny tabeli
	var $aTHAttribs;
	
	// tablica z atrybutami dla komorek drugiej kolumny tabeli
	var $aTDAttribs;
	
	// prefix komunikatu o bledzie wypelnienia formularza
	var $sErrPrefix;
	
	// postfix komunikatu o bledzie wypelnienia formularza
	var $sErrPostfix;
  
  public $sTableAddHeader;
  
  private $bIsTokenProtected;
  
  private $sFormToken;

	/**
   * Konstruktor klsy Form
   *
	 * @param	string	$sName	- nazwa formularza
   * @param string  $sTableHeader
	 * @param array $aFormAttribs - tablica asocjacyjna zawierajaca atrybuty formularza.
	 * Dostepne atrybuty:
 	 *  - action			    		URI
	 *	- method			    		(GET|POST) domyslnie POST
	 * 	- enctype			    		domyslnie: "application/x-www-form-urlencoded"
	 * 	- accept			    		lista typow MIME uploadowanych plikow
	 * 	- onsubmit			  		zdarzenie: formularz zostal wyslany
	 * 	- onreset			    		zdarzenie: formularz zostal zresetowany
	 * 	- accept-charset  		lista akceptowanych stron kodowych
	 *  - col_width						szerokosc pierwszej kolumny z etykietami
   * @param array   $aTableAttribs
   * @param	bool	$bUseJS	- false: brak walidacji JS; true: walidacja JS;
   * @param array $aTHAttribs
   * @param array $aTDAttribs
	 * @param	string	$sErrPrefix	- postfix alertu o bledzie
   * @param	string $sErrPostfix	- prefix alertu o bledzie
   *
	 * @return void
   */
  function FormTable($sName, $sTableHeader='', $aFormAttribs=array(), $aTableAttribs=array(), $bUseJS=true, $aTHAttribs=array(), $aTDAttribs=array(), $sErrPrefix='', $sErrPostfix='') {
  	global $aConfig;
  	
  	$this->aElements    		= array();
    $this->aHiddenElements  = array();
    $this->aFormAttribs 		= array();
    $this->aJavaScript 			= array();
		$this->aValidator				= array();
    $this->iCalendars				= 0;
    $this->sFormErr     		= "";
    $this->bIsTokenProtected = false;
    
    if (isset($_SESSION)) {
      $this->setTokenSession($_SESSION);
    }
    
    if (!isset($aFormAttribs['action']) || empty($aFormAttribs['action'])) {
      $aFormAttribs['action'] = phpSelf();
    }
    if (!isset($aFormAttribs['method']) || 
    	(strtolower($aFormAttribs['method']) != 'post' && strtolower($aFormAttribs['method']) != 'get')) {
			$aFormAttribs['method'] = 'post';
		}
    if (isset($aFormAttribs['enctype']) &&
    	strtolower($aFormAttribs['enctype']) != 'application/x-www-form-urlencoded' &&
    	strtolower($aFormAttribs['enctype']) != 'multipart/form-data') {
			unset($aFormAttribs['enctype']);
		}
		if (!isset($aFormAttribs['id']) || empty($aFormAttribs['id'])) {
			$aFormAttribs['id'] = $sName;
		}
		if (empty($sErrPrefix)) {
			$sErrPrefix = $aConfig['lang']['form']['error_prefix'];
		}
		if (empty($sErrPostfix)) {
			$sErrPostfix = $aConfig['lang']['form']['error_postfix'];
		}
		
		if (!isset($aTableAttribs['col_width'])) {
			$aTableAttribs['col_width'] = 200;
		}
		if (!isset($aTableAttribs['class'])) {
			$aTableAttribs['class'] = "formTable";
		}
		if (!isset($aTableAttribs['width'])) {
			$aTableAttribs['width'] = "100%";
		}
		if (!isset($aTableAttribs['border'])) {
			$aTableAttribs['border'] = "0";
		}
		if (!isset($aTableAttribs['cellspacing'])) {
			$aTableAttribs['cellspacing'] = "0";
		}
		if (!isset($aTableAttribs['cellpadding'])) {
			$aTableAttribs['cellpadding'] = "0";
		}
		
		$this->sName = $sName;
		$this->sTableHeader = $sTableHeader;
		$this->aFormAttribs =& $aFormAttribs;
		$this->iColWidth = $aTableAttribs['col_width'];
		unset($aTableAttribs['col_width']);
		
		$this->bUseJS = $bUseJS;
		$this->aTableAttribs = $aTableAttribs;
		$this->aTHAttribs = $aTHAttribs;
		$this->aTDAttribs = $aTDAttribs;
		$this->sErrPrefix = $sErrPrefix;
		$this->sErrPostfix = $sErrPostfix;
	} // end of Form() method
	
  /**
   * 
   * @param array $aSaveTo
   */
  public function setTokenSession(&$aSaveTo) {
    
    if ($this->bIsTokenProtected === false) {
      $this->bIsTokenProtected = true;
      $this->sFormToken = md5(session_id(). time());
      $sName = 'token_'.substr($this->sFormToken, 0, 5);
      $aSaveTo[$sName] = $this->sFormToken;
      $this->AddHidden($sName, $this->sFormToken, array('id' => $sName));

      $sPcre = '/^'.$this->sFormToken.'$/';
      $sErrMsg = _('Zabezpieczenie przed podwójnym przesłaniem formularza');
      $this->aValidator[$sName] = array(
        'type'			=> 'token',
        'pcre'			=> $sPcre,
        'err_msg'		=> $sErrMsg,
        'required' => true
      );

      $this->sJS .= $this->GetTextJS($sName, $sName, $sErrMsg, $sPcre, array(), '', true, '');
    }
  }
  
  /**
   * 
   * @param array $aSaveTo
   */
  public function getTokenSessionHTML(&$aSaveTo) {
    
    if ($this->bIsTokenProtected === false) {
      $this->bIsTokenProtected = true;
      $this->sFormToken = md5(session_id(). time());
      $sName = 'token_'.substr($this->sFormToken, 0, 5);
      $aSaveTo[$sName] = $this->sFormToken;

      $sPcre = '/^'.$this->sFormToken.'$/';
      $sErrMsg = _('Zabezpieczenie przed podwójnym przesłaniem formularza');
      $this->aValidator[$sName] = array(
        'type'			=> 'token',
        'pcre'			=> $sPcre,
        'err_msg'		=> $sErrMsg,
        'required' => true
      );

      $this->sJS .= $this->GetTextJS($sName, $sName, $sErrMsg, $sPcre, array(), '', true, '');
      return Form::GetHidden($sName, $this->sFormToken, array('id' => $sName));
    } else {
      $sName = 'token_'.substr($this->sFormToken, 0, 5);
      return Form::GetHidden($sName, $this->sFormToken, array('id' => $sName));
    }
  }
	
	/**
	 * Metoda zwraca kod HTML etykiety elementu
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sHtml	kod HTML etykiety
	 */
	function GetLabelHTML($sName, $sLabel, $bRequired=true, $iMaxlength=0, $sData='', $aAttribs = []) {
		return Form::GetLabel($sName, $sLabel, $bRequired, $iMaxlength, $sData, $aAttribs);
	}	// end of  GetLabelHTML() function


	/**
	 * Metoda zwraca kod HTML elementu
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param   string  $sValue           domyślna wartość elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		string	$sType						typ elementu
   * @param		bool		$bRequired				czy pole wymagane
   * @param		string	$sPcre						wyrazenie regularne do walidacji
   * @param		array		$aRange						zakres dostepnych wartosci array(min, max)
   *
   * @param		array		$sDependsOn				nazwa pola od ktorego stanu zalezy obowiazkowosc wypelnienia pola
	 * @return	string	$sHtml	kod HTML
	 */
	function GetTextHTML($sName, $sLabel, $sValue='', $aAttribs=array(), $sErrMsg='', $sType='text', $bRequired=true, $sPcre='', $aRange=array(), $sDependsOn='', $mDependsVal='',$sDependsId='') {
		global $aConfig;
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		if (!isset($aAttribs['maxlength']) && isset($aConfig['class']['form'][$sType]['maxlength'])
	    	&& !empty($aConfig['class']['form'][$sType]['maxlength'])) {
    	$aAttribs['maxlength'] =& $aConfig['class']['form'][$sType]['maxlength'];
		}
		
		if (empty($sPcre) && isset($aConfig['class']['form'][$sType]['pcre'])
				&& !empty($aConfig['class']['form'][$sType]['pcre'])) {
    	$sPcre =& $aConfig['class']['form'][$sType]['pcre'];
		}
				
		if ($bRequired || $sDependsOn != '' || $sPcre != '') {
			$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
			$this->aValidator[$sName] = array(
				'type'			=> $sType,
				'pcre'			=> $sPcre,
				'range'			=> $aRange,
				'err_msg'		=> $sErrMsg,
				'required' => $bRequired
			);
			if ($sDependsOn != '') {
				$this->aValidator[$sName]['depends_on'] = $sDependsOn;
			}
			if ($mDependsVal != '') {
				$this->aValidator[$sName]['depends_val'] = $mDependsVal;
			}
		
			$this->sJS .= $this->GetTextJS($sName, $aAttribs['id'], $sErrMsg, $sPcre, $aRange, $sDependsOn, $bRequired, $sDependsId);
		}
		return Form::GetText($sName, $sValue, $aAttribs, $sType);
	} // end GetTextHTML()
	
	
	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
	 * 
	 * @return	string	$sJS - kod JS
	 */
	function GetTextJS($sName, $sID, $sErrMsg='', $sPcre='', $aRange=array(), $sDependsOn='', $bRequired=false, $sDependsId='') {
		$sJS = '';
		
		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		if ($sDependsOn != '') {
			if($sDependsId != ''){
				$sDependsOnJSName = $sDependsId;
			} else {
				$sDependsOnJSName = preg_replace('/\[|\]/', '_', $sDependsOn);				
			}
			$sJS .= "\tif (document.getElementById('".$sDependsOnJSName."').checked) {\n\t";
		}
		$sJS .= "\to".$sJSName."=document.getElementById('".$sID."');\n\t";
		$sJS .= "\n\to".$sJSName.".value=o".$sJSName.".value.replace(/^\s+/g, '').replace(/\s+\$/g, '');"."\n";
		if (!empty($sPcre)) {
			if ($bRequired || $sDependsOn != '') {
				$sJS .= "if (!o".$sJSName.".value.match(".$sPcre.")) {\n\t\t";
			}
			else {
				// pole jest nieobowiazkowe, walidacja tylko wtedy gdy zostalo
				// cokowliek do pola wpisane
				$sJS .= "if (o".$sJSName.".value != '' && !o".$sJSName.".value.match(".$sPcre.")) {\n\t\t";
			}
		}
		elseif (!empty($aRange) && count($aRange) == 2) {
			$sJS .= "o".$sJSName.".value = o".$sJSName.".value.replace(',', '.');";
			$sJS .= "if (isNaN(parseFloat(o".$sJSName.".value)) ||
									 parseFloat(o".$sJSName.".value) < ".$aRange[0]." ||
									 parseFloat(o".$sJSName.".value) > ".$aRange[1].") {\n\t\t";
		}
		else {
			$sJS .= "if (o".$sJSName.".value == \"\") {\n\t\t";
		}
		$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
		$sJS .= "\t}\n";
		if ($sDependsOn != '') {
			$sJS .= "\t}\n";
		}
		
		return $sJS;
	} // end GetTextJS()
	
	function GetBlurFieldValidatorJS($sFormName, $aOmmit=array()){
		$sJS = '';
		$sJSselector = '$(this)';
		$sJS .= "$(\"#\"+this.name+\"_validMsg\").remove();\n\t";	
		$sJS .= "$(\"#".$sFormName." input:enabled[name='\"+this.name+\"']\").removeClass(\"validOK validErr validWarn\");\n\t";	
		$sJS .= "$(\"#".$sFormName." .fRow:has(input[name='\"+this.name+\"']) .hide_message\").hide();";
					
		foreach($this->aValidator as $sKey =>$aVal){
			if(empty($aOmmit) || !in_array($sKey,$aOmmit)){
				$sJSName = preg_replace('/\[|\]/', '_', $sKey);
				$sJS .= "if (this.name == \"".$sKey."\" && ".$sJSselector.".val() != ''){\n\t";	
					
					$sJS .= "\n\t".$sJSselector.".val(".$sJSselector.".val().replace(/^\s+/g, '').replace(/\s+\$/g, ''));"."\n";
					if (!empty($aVal['pcre'])) {
						if ($aVal['required']) {
							$sJS .= "if (!".$sJSselector.".val().match(".$aVal['pcre'].")) {\n\t\t";
						}
						else {
							// pole jest nieobowiazkowe, walidacja tylko wtedy gdy zostalo
							// cokowliek do pola wpisane
							$sJS .= "if (".$sJSselector.".val() != '' && !".$sJSselector.".val().match(".$aVal['pcre'].")) {\n\t\t";
						}
					}
					elseif (!empty($aVal['range']) && count($aVal['range']) == 2) {
						$sJS .= "m".$sJSName." = ".$sJSselector.".val().replace(',', '.');";
						$sJS .= "if (isNaN(parseFloat(m".$sJSName.")) ||
												 parseFloat(m".$sJSName.") < ".$aVal['range'][0]." ||
												 parseFloat(m".$sJSName.") > ".$aVal['range'][1].") {\n\t\t";
					}
					else {
						$sJS .= "if (".$sJSselector.".val() == \"\") {\n\t\t";
					}
					//$sJS .= "alert('".$aVal['err_msg']."');\n";
					$sJS .= "\t\t".$sJSselector.".addClass(\"validErr\");\n";
					$sJS .= "\t\t$(\"#".$sFormName." .fRow:has(input[name='".$sJSName."'])\").append('<div id=\"".$sJSName."_validMsg\" class=\"validMsg\">".$aVal['err_msg']."</div>');";
					$sJS .= "\t\t$(\"#".$sFormName." .fRow:has(input[name='".$sJSName."']) .hide_message\").show();";
					$sJS .= "\t} else {\n";
					$sJS .= "\t\t".$sJSselector.".addClass(\"validOK\");";
					$sJS .= "\n}\n";
				$sJS .= "\t}\n";
			}
		}
		return $sJS;
	}

  /**
   * Metoda dodaje element tekstowy do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		string	$sType						typ elementu
   * @param		bool		$bRequired				czy pole wymagane
   * @param		string	$sPcre						wyrazenie regularne do walidacji
   * @param		array		$aRange						zakres dostepnych wartosci array(min, max)
   * 
   * @param		array		$sDependsOn				nazwa pola od ktorego stanu zalezy obowiazkowosc wypelnienia pola
   * @return void
   */
  function AddText($sName, $sLabel, $sValue='', $aAttribs=array(), $sErrMsg='', $sType='text', $bRequired=true, $sPcre='', $aRange=array(), $aTHAttribs=array(), $aTDAttribs=array(), $sDependsOn='') {
    global $aConfig;
    
    if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
    $this->aElements[$sName] = array(
    	'th' => '<label for="'.$aAttribs['id'].'">'.$sLabel.'&nbsp;<span id="'.$aAttribs['id'].'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span>'.'</label>',
      'td' => $this->GetTextHTML($sName, $sLabel, $sValue, $aAttribs, $sErrMsg, $sType, $bRequired, $sPcre, $aRange, $sDependsOn),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddText() method
  
  /**
   * Metoda wyświetla dynamiczne pole wprowadzania teksu
   * 
   * @param string $sName
   * @param string $sLabel
   * @param string $sDo - do której akcji ma przenosić po przesłaniu form
   * @param string $sDoId - id akcji do
   * @param string $sValue
   * @param array $aAttribs
   * @param string $sSendFormId
   */
  function AddEditableText($sName, $sLabel, $sDo, $sDoId, $sValue = '', $aAttribs = array(), $sSendFormId = '') {

    $sRand = rand(0, 100000);
    $sRemoveFirst = ' $(\'input[name=do][id!='.$sDoId.']\').remove(); ';
    if ($sSendFormId != '') {
      $sJs = '$(\'#'.$sDoId.'\').val(\''.$sDo.'\'); '.$sRemoveFirst.' $(\'#'.$sSendFormId.'\').submit();';
    } else {
      $sJs = '$(\'#'.$sDoId.'\').val(\''.$sDo.'\'); '.$sRemoveFirst.' submit();';
    }
    
    //edycja transport number
    $this->AddRow($sLabel, '
    <div id="edit_row_'.$sRand.'" style="float: left;" >'.nl2br($sValue).'&nbsp;'.
    '</div> '.
        $this->GetInputButtonHTML('edit_row_show', _('Edytuj'), array('onclick'=>'show_row('.$sRand.');'),'button')
        .'
    <div class="clear"></div>
    <div id="edit_row_buttons_'.$sRand.'" style="display:none;"> '.
        $this->GetTextAreaHTML($sName, $sLabel, $sValue, $aAttribs, 0, '', '', false).'<div class="clear"></div>'.
        $this->GetInputButtonHTML('row_send_'.$sRand, _('Zapisz'), array('onclick' => $sJs),'button').
      $this->GetInputButtonHTML('row_hide_'.$sRand, _('Zamknij'), array('onclick' => 'hide_row('.$sRand.');'),'button')
   .'</div>'
            );
  }// end of AddEditableText() method
	
	
	/**
	 * Metoda zwraca kod HTML elementu typu File
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
   * @return	string	$sHtml	kod HTML
	 */
	function GetFileHTML($sName, $sLabel, $aAttribs=array(), $sErrMsg='', $bRequired=true, $sDependsOn='') {
		global $aConfig;
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		if ($bRequired || $sDependsOn != '') {
			$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
			$this->aValidator[$sName] = array(
				'type'			=> 'file',
				'err_msg'		=> $sErrMsg
			);
			if ($sDependsOn != '') {
				$this->aValidator[$sName]['depends_on'] = $sDependsOn;
			}
			$this->sJS .= $this->GetFileJS($sName, $aAttribs['id'], $sErrMsg, $sDependsOn);
		}
		return Form::GetFile($sName, $aAttribs);
	} // end GetFileHTML()
	
	
	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
	 * 
	 * @return	string	$sJS - kod JS
	 */
	function GetFileJS($sName, $sID, $sErrMsg='', $sDependsOn='') {
		$sJS = '';
		
		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		if ($sDependsOn != '') {
			$sDependsOnJSName = preg_replace('/\[|\]/', '_', $sDependsOn);
			$sJS .= "\tif (document.getElementById('".$sDependsOnJSName."').checked) {\n\t";
		}
		$sJS .= "\to".$sJSName."=document.getElementById('".$sID."');\n\t";
		$sJS .= "\n\to".$sJSName.".value=o".$sJSName.".value.replace(/^\s+/g, '');"."\n";
		$sJS .= "if (o".$sJSName.".value == \"\") {\n\t\t";
		$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
		$sJS .= "\t}\n";
		if ($sDependsOn != '') {
			$sJS .= "\t}\n";
		}
		
		return $sJS;
	} // end GetFileJS()


  /**
   * Metoda dodaje element typu File do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @param		array		$aTHAttribs				tablica z atrybutami dla komorki TH
	 * @param		array		$aTDAttribs				tablica z atrybutami dla komorki TD
   * @return void
   */
  function AddFile($sName, $sLabel, $aAttribs=array(), $sErrMsg='', $bRequired=true, $aTHAttribs=array(), $aTDAttribs=array(), $sDependsOn='') {
    global $aConfig;
    
    if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
    $this->aElements[$sName] = array(
    	'th' => '<label for="'.$aAttribs['id'].'">'.$sLabel.($bRequired?'&nbsp;<span id="'.$aAttribs['id'].'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span>': '').'</label>',
      'td' => $this->GetFileHTML($sName, $sLabel, $aAttribs, $sErrMsg, $bRequired, $sDependsOn),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddFile() method
  
  
	/**
	 * Metoda zwraca kod HTML elementu
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		array		$aImg							dane zdjecia
	 * @param		array		$aOrderBy					dane do comba wyboru kolejnosci
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sHtml	kod HTML
	 */
	function GetImageHTML($sName, $sLabel, $aImg=array(), $aOrderBy=array(), $aAttribs=array(), $sErrMsg='', $bRequired=false, $bShowDesc=true, $bShowAuthor=true) {
		global $aConfig;
		$sHtml = '';
		$sHtml2 = '';
		$sHtml3 = '';

		if (!isset($aAttribs['image']['id'])) {
			$aAttribs['image']['id'] = $sName;
		}
		if (!isset($aAttribs['desc']['id'])) {
			$aAttribs['desc']['id'] = 'desc_'.$sName;
		}
		if (!isset($aAttribs['image']['class'])) {
			$aAttribs['image']['class'] = 'fileUpload';
		}
		if (!isset($aAttribs['desc']['class'])) {
			$aAttribs['desc']['class'] = 'fileUploadDesc';
		}
		if (!isset($aAttribs['author']['id'])) {
			$aAttribs['author']['id'] = 'author_'.$sName;
			$aAttribs['author']['class'] = 'fileUploadAuthor';
		}
		
		// okreslenie id z nazwy
		$sId = substr($sName, strpos($sName, '[') + 1, -1);
		if (isset($_POST['desc_image'][$sId])) {
			$aImg['description'] =& $_POST['desc_image'][$sId];
		};
		if (isset($_POST['order_image'][$sId])) {
			$aImg['order_by'] =& $_POST['order_image'][$sId];
		};
		
		if (isset($aImg['photo'])) {
			$aImg['photo'] = (!empty($aImg['directory']) ? $aImg['directory'].'/' : '').$aImg['photo'];
			if (file_exists($aConfig['common']['client_base_path'].$aConfig['common']['photo_dir'].'/'.$aImg['photo'])) {
				$sHtml = '<div class="photoFormCnt"><div class="existingPhotoName">'.$aImg['name'].'</div>';
				
				$aImgSize = @getimagesize($aConfig['common']['client_base_path'].$aConfig['common']['photo_dir'].'/'.$aImg['photo']);
				$sHtml2 = '<div class="existingPhoto"><a href="javascript: void(0);" onclick="showPhoto(this, \''.$sId.'\', \''.$aConfig['common']['client_base_url_http'].$aConfig['common']['photo_dir'].'/'.$aImg['photo'].'\', \''.$aConfig['common']['base_url_http'].'gfx/loading.gif\', \''.$aConfig['lang']['common']['hide_image'].'\', \''.$aConfig['lang']['common']['show_image'].'\'); return false;" title="">'.$aConfig['lang']['common']['show_image'].'</a>&nbsp;&nbsp;|&nbsp;&nbsp;'.$this->GetCheckboxHTML('delete_'.$sName, '', array(), '', false, false).$this->GetLabelHTML('delete_'.$sName, $aConfig['lang']['common']['delete_image'], false).'</div>';
				if (!empty($aOrderBy)) {
					$sHtml2 .= '<div class="existingPhoto">'.$this->GetLabelHTML('order_'.$sName, $aConfig['lang']['common']['order_by'], false).' '.$this->GetSelectHTML('order_'.$sName, '', array(), $aOrderBy, $aImg['order_by'], '', false).'</div>';
				}
				
				$sHtml3 = '<div id="'.$sId.'" class="photoCnt" style="display: none;"><div class="photo"><img src="gfx/loading.gif" id="img_'.$sId.'" alt=""></div></div>';
			}
		}
		$sHtml .= $this->GetFileHTML($sName, $sLabel, $aAttribs['image'], $sErrMsg, $bRequired);
		if ($bShowDesc) {
			$sHtml .= '<br /><label for="desc_'.$sName.'">'.$aConfig['lang']['image']['description'].'</label>&nbsp;'.$this->GetTextAreaHTML('desc_'.$sName, '', isset($aImg['description']) ? br2nl($aImg['description']) : '', array_merge($aAttribs['desc'], array('rows'=>3)), 0, '', '', false);
		}
		else {
			$sHtml .= $this->GetHiddenHTML('desc_'.$sName, '');
		}
		// autor zdjecia
		if ($bShowAuthor) {
			$sHtml .= '<br /><label for="author_'.$sName.'">'.$aConfig['lang']['image']['author'].'</label>&nbsp;'.$this->GetTextHTML('author_'.$sName, '', $aImg['author'], $aAttribs['author'], '', 'text', false);
		}
		$sHtml .= $sHtml2;
		$sHtml .= '</div>';
		$sHtml .= $sHtml3;
		return $sHtml;
	} // end GetImageHTML()
	
	
  /**
   * Metoda dodaje element tekstowy do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		array		$aImg							dane zdjecia
	 * @param		array		$aOrderBy					dane do comba wyboru kolejnosci
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @param		array		$aTHAttribs				tablica z atrybutami dla komorki TH
	 * @param		array		$aTDAttribs				tablica z atrybutami dla komorki TD
   * @return void
   */
  function AddImage($sName, $sLabel, $aImg=array(), $aOrderBy=array(), $aAttribs=array(), $sErrMsg='', $bRequired=false, $bShowDesc=true, $bShowAuthor=true, $aTHAttribs=array(), $aTDAttribs=array()) {
		global $aConfig;
		
		if (!isset($aAttribs['image']['id'])) {
			$aAttribs['image']['id'] = $sName;
		}
		if (!isset($aAttribs['desc']['id'])) {
			$aAttribs['desc']['id'] = 'desc_'.$sName;
		}
		if (!isset($aAttribs['image']['class'])) {
			$aAttribs['image']['class'] = 'fileUpload';
		}
		if (!isset($aAttribs['desc']['class'])) {
			$aAttribs['desc']['class'] = 'fileUploadDesc';
		}
    $this->aElements[] = array(
    	'th' => '<label for="'.$aAttribs['image']['id'].'">'.$sLabel.($bRequired?'&nbsp;<span class="gwiazdka">*</span>': '').'</label>',
      'td' => $this->GetImageHTML($sName, $sLabel, $aImg, $aOrderBy, $aAttribs, $sErrMsg, $bRequired, $bShowDesc, $bShowAuthor),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddImage() method
  
  
	/**
	 * Metoda zwraca kod HTML elementu
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sImg							nazwa zdjecia
	 * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sHtml	kod HTML
	 */
	function GetProductImageHTML($sName, $sLabel, $sDir='', $sImg='', $aAttribs=array(), $sErrMsg='', $bRequired=false) {
		global $aConfig;
		$sHtml = '';
		$sHtml2 = '';
		$sHtml3 = '';

		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		if (!isset($aAttribs['class'])) {
			$aAttribs['image'] = 'fileUpload';
		}
		
		if ($sImg != '') {
			if (file_exists($aConfig['common']['client_base_path'].$sDir.'/'.$sImg)) {
				$sHtml = '<div class="photoFormCnt"><div class="existingPhotoName">'.$sImg.'</div>';
				
				$sHtml2 = '<div class="existingPhoto"><a href="javascript: void(0);" onclick="showPhoto(this, \'prodImg\', \''.$aConfig['common']['client_base_url_http'].$sDir.'/'.$sImg.'\', \''.$aConfig['common']['base_url_http'].'gfx/loading.gif\', \''.$aConfig['lang']['common']['hide_image'].'\', \''.$aConfig['lang']['common']['show_image'].'\'); return false;" title="">'.$aConfig['lang']['common']['show_image'].'</a></div>';
				
				$sHtml3 = '<div id="prodImg" class="photoCnt" style="display: none;"><div class="photo"><img src="gfx/loading.gif" id="img_prodImg" alt=""></div></div>';
			}
		}
		$sHtml .= $this->GetFileHTML($sName, $sLabel, $aAttribs, $sErrMsg, $bRequired);
		$sHtml .= $sHtml2;
		$sHtml .= '</div>';
		$sHtml .= $sHtml3;
		return $sHtml;
	} // end GetProductImageHTML()
	
	
  /**
   * Metoda dodaje element tekstowy do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sImg							nazwa zdjecia
	 * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @param		array		$aTHAttribs				tablica z atrybutami dla komorki TH
	 * @param		array		$aTDAttribs				tablica z atrybutami dla komorki TD
   * @return void
   */
  function AddProductImage($sName, $sLabel, $sDir, $sImg='', $aAttribs=array(), $sErrMsg='', $bRequired=false, $aTHAttribs=array(), $aTDAttribs=array()) {
		global $aConfig;
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		if (!isset($aAttribs['class'])) {
			$aAttribs['class'] = 'fileUpload';
		}
		$this->aElements[] = array(
    	'th' => '<label for="'.$aAttribs['id'].'">'.$sLabel.($bRequired?'&nbsp;<span class="gwiazdka">*</span>': '').'</label>',
      'td' => $this->GetProductImageHTML($sName, $sLabel, $sDir, $sImg, $aAttribs, $sErrMsg, $bRequired),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddProductImage() method
  
  
	/**
	 * Metoda zwraca kod HTML elementu
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		array		$aFile							dane pliku
	 * @param		array		$aOrderBy					dane do comba wyboru kolejnosci
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sHtml	kod HTML
	 */
	function GetAttachmentHTML($sName, $sLabel, $aFile=array(), $aOrderBy=array(), $aAttribs=array(), $sErrMsg='', $bRequired=false) {
		global $aConfig;
		$sHtml = '';
		$sHtml2 = '';
		$sHtml3 = '';
		
		if (!isset($aAttribs['file']['id'])) {
			$aAttribs['file']['id'] = $sName;
		}
		if (!isset($aAttribs['desc']['id'])) {
			$aAttribs['desc']['id'] = 'desc_'.$sName;
		}
		if (!isset($aAttribs['file']['class'])) {
			$aAttribs['file']['class'] = 'fileUpload';
		}
		if (!isset($aAttribs['desc']['class'])) {
			$aAttribs['desc']['class'] = 'fileUploadDesc';
		}
		
		// okreslenie id z nazwy
		$sId = substr($sName, strpos($sName, '[') + 1, -1);
		if (isset($_POST['desc_file'][$sId])) {
			$aFile['description'] =& $_POST['desc_file'][$sId];
		};
		if (isset($_POST['order_file'][$sId])) {
			$aFile['order_by'] =& $_POST['order_file'][$sId];
		};
		
		if (isset($aFile['filename'])) {
			$aFile['filename'] = (!empty($aFile['directory']) ? $aFile['directory'].'/' : '').$aFile['filename'];
			if (file_exists($aConfig['common']['client_base_path'].$aConfig['common']['file_dir'].'/'.$aFile['filename'])) {
				
				$sHtml = '<div class="photoFormCnt"><div class="existingPhotoName">'.$aFile['name'].'</div>';
				
				$sHtml2 = '<div class="existingFile"><a href="'.$aConfig['common']['client_base_url_http'].$aConfig['common']['file_dir'].'/'.$aFile['filename'].'" title="">'.$aConfig['lang']['common']['show_file'].'</a>&nbsp;&nbsp;|&nbsp;&nbsp;'.$this->GetCheckboxHTML('delete_'.$sName, '', array(), '', false, false).$this->GetLabelHTML('delete_'.$sName, $aConfig['lang']['common']['delete_file'], false).'</div>';
				$sHtml2 .= '<div class="existingPhoto">'.$this->GetLabelHTML('order_'.$sName, $aConfig['lang']['common']['order_by'], false).' '.$this->GetSelectHTML('order_'.$sName, '', array(), $aOrderBy, $aFile['order_by'], '', false).'</div>';
			}
		}
		$sHtml .= $this->GetFileHTML($sName, $sLabel, $aAttribs['file'], $sErrMsg, $bRequired);
		$sHtml .= '<br /><label for="desc_'.$sName.'" style="cursor: '.$aConfig['styles']['cursor'].';">'.$aConfig['lang']['file']['description'].'</label>&nbsp;'.$this->GetTextAreaHTML('desc_'.$sName, '', isset($aFile['description']) ? br2nl($aFile['description']) : '', array_merge($aAttribs['desc'], array('rows'=>3)), 0, '', '', false);
		
		$sHtml .= $sHtml2;
		$sHtml .= '</div>';
		return $sHtml;
	} // end GetAttachmentHTML()
  
  
  /**
   * Metoda dodaje element typu pole dodawania pliku
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		array		$aFile						dane pliku
	 * @param		array		$aOrderBy					dane do comba wyboru kolejnosci
	 * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @param		array		$aTHAttribs				tablica z atrybutami dla komorki TH
	 * @param		array		$aTDAttribs				tablica z atrybutami dla komorki TD
   * @return void
   */
  function AddAttachment($sName, $sLabel, $aFile=array(), $aOrderBy=array(), $aAttribs=array(), $sErrMsg='', $bRequired=false, $aTHAttribs=array(), $aTDAttribs=array()) {
		global $aConfig;

		if (!isset($aAttribs['file']['id'])) {
			$aAttribs['file']['id'] = $sName;
		}
		if (!isset($aAttribs['desc']['id'])) {
			$aAttribs['desc']['id'] = 'desc_'.$sName;
		}
		if (!isset($aAttribs['file']['class'])) {
			$aAttribs['file']['class'] = 'fileUpload';
		}
		if (!isset($aAttribs['desc']['class'])) {
			$aAttribs['desc']['class'] = 'fileUploadDesc';
		}
		
    $this->aElements[] = array(
    	'th' => '<label for="'.$aAttribs['file']['id'].'" style="cursor: '.$aConfig['styles']['cursor'].';">'.$sLabel.($bRequired?'&nbsp;<span class="gwiazdka">*</span>': '').'</label>',
      'td' => $this->GetAttachmentHTML($sName, $sLabel, $aFile, $aOrderBy, $aAttribs, $sErrMsg, $bRequired),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddAttachment() method
  
  
 	/**
	 * Metoda zwraca kod HTML elementu
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sFile						nazwa pliku
	 * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sHtml	kod HTML
	 */
	function GetProductAttachmentHTML($sName, $sLabel, $sFile=array(), $aAttribs=array(), $sErrMsg='', $bRequired=false) {
		global $aConfig;
		$sHtml = '';
		$sHtml2 = '';
		$sHtml3 = '';
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		if (!isset($aAttribs['class'])) {
			$aAttribs['class'] = 'fileUpload';
		}
		
		if ($sFile != '') {
			if (file_exists($aConfig['common']['client_base_path'].$aConfig['common']['product_file_dir'].'/'.$sFile)) {
				
				$sHtml = '<div class="photoFormCnt"><div class="existingPhotoName">'.$sFile.'</div>'.
				$this->GetHiddenHTML('old_'.$sName, $sFile);
				
				$sHtml2 = '<div class="existingFile"><a href="'.$aConfig['common']['client_base_url_http'].$aConfig['common']['product_file_dir'].'/'.$sFile.'" title="" target="_blank">'.$aConfig['lang']['common']['show_file'].'</a>&nbsp;&nbsp;|&nbsp;&nbsp;'.$this->GetCheckboxHTML('delete_'.$sName, '', array(), '', false, false).$this->GetLabelHTML('delete_'.$sName, $aConfig['lang']['common']['delete_file'], false).'</div>';
			}
		}
		$sHtml .= $this->GetFileHTML($sName, $sLabel, $aAttribs, $sErrMsg, $bRequired);
				
		$sHtml .= $sHtml2;
		$sHtml .= '</div>';
		return $sHtml;
	} // end GetProductAttachmentHTML()
  
  
  /**
   * Metoda dodaje element typu pole dodawania pliku
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sFile						nazwa pliku
	 * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @param		array		$aTHAttribs				tablica z atrybutami dla komorki TH
	 * @param		array		$aTDAttribs				tablica z atrybutami dla komorki TD
   * @return void
   */
  function AddProductAttachment($sName, $sLabel, $sFile='', $aAttribs=array(), $sErrMsg='', $bRequired=false, $aTHAttribs=array(), $aTDAttribs=array()) {
		global $aConfig;

		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		if (!isset($aAttribs['class'])) {
			$aAttribs['class'] = 'fileUpload';
		}
		
    $this->aElements[] = array(
    	'th' => '<label for="'.$aAttribs['id'].'" style="cursor: '.$aConfig['styles']['cursor'].';">'.$sLabel.($bRequired?'&nbsp;<span class="gwiazdka">*</span>': '').'</label>',
      'td' => $this->GetProductAttachmentHTML($sName, $sLabel, $sFile, $aAttribs, $sErrMsg, $bRequired),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddProductAttachment() method
	
	
	/**
	 * Metoda zwraca kod HTML elementu
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sHtml	kod HTML
	 */
	function GetWYSIWYGHTML($sName, $sLabel, $sValue='', $aAttribs=array(), $sErrMsg='', $bRequired=true, $sDependsOn='') {
		global $aConfig;
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
						
		if ($bRequired || $sDependsOn != '') {
			$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
			$this->aValidator[$sName] = array(
				'type'			=> 'wysiwyg',
				'err_msg'		=> $sErrMsg
			);
			if ($sDependsOn != '') {
				$this->aValidator[$sName]['depends_on'] = $sDependsOn;
			}
			$this->sJS .= $this->GetWYSIWYGJS($sName, $aAttribs['id'], $sErrMsg, $sDependsOn);
		}
		return Form::GetWYSIWYG($sName, $sValue, $aAttribs);
	} // end GetWYSIWYGHTML()
	
	
	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
	 * 
	 * @return	string	$sJS - kod JS
	 */
	function GetWYSIWYGJS($sName, $sID, $sErrMsg, $sDependsOn='') {
		$sJS = 'tinyMCE.triggerSave();';
		
		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		if ($sDependsOn != '') {
			$sDependsOnJSName = preg_replace('/\[|\]/', '_', $sDependsOn);
			$sJS .= "\tif (document.getElementById('".$sDependsOnJSName."').checked) {\n\t";
		}
		$sJS .= "\to".$sJSName."=document.getElementById('".$sID."');\n\t";
		$sJS .= "\n\to".$sJSName.".value=o".$sJSName.".value.replace(/^\s+/g, '');"."\n";
		$sJS .= "if (o".$sJSName.".value == \"\") {\n\t\t";
		$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
		$sJS .= "\t}\n";
		if ($sDependsOn != '') {
			$sJS .= "\t}\n";
		}
		
		return $sJS;
	} // end GetWYSIWYGJS()

  /**
   * Metoda dodaje element tekstowy do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
   * @return void
   */
  function AddWYSIWYG($sName, $sLabel, $sValue='', $aAttribs=array(), $sErrMsg='', $bRequired=true, $aTHAttribs=array(), $aTDAttribs=array(), $sDependsOn='') {
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		$this->aElements[] = array(
    	'th' => '<label>'.$sLabel.($bRequired?'&nbsp;<span id="'.$aAttribs['id'].'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span>': '').'</label>',
      'td' => $this->GetWYSIWYGHTML($sName, $sLabel, $sValue, $aAttribs, $sErrMsg, $bRequired, $sDependsOn),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddWYSIWYG() method
  
  
	/**
	 * Metoda zwraca kod HTML checkboxa
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		integer	$iMaxLength				maksymalna dlugosc tekstu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		string	$sMaxLengthErr		komunikat bledu przekroczenia dopuszczalnej liczby znakow
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sHtml	kod HTML
	 */
	function GetTextAreaHTML($sName, $sLabel, $sValue='', $aAttribs=array(), $iMaxlength=0, $sErrMsg='', $sMaxLengthErr='', $bRequired=true, $sDependsOn='') {
    if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		if ($iMaxlength > 0) {
			if (isset($aAttribs['onkeyup'])) {
				$aAttribs['onkeyup'] .= ' CheckTextLength(this, '.$iMaxlength.', \''.$sMaxLengthErr.'\');';
			}
			else {
				$aAttribs['onkeyup'] = 'CheckTextLength(this, '.$iMaxlength.', \''.$sMaxLengthErr.'\');';
			}
			if ($iMaxlength > 0 && !isset($this->aJavaScript['CheckTextLength'])) {
				$this->aJavaScript['CheckTextLength'] = '	function CheckTextLength(oField, iMaxLength, sAlertText) {
	sName = \'textarea_\' + oField.name.replace(\'[\', \'_\');
	sName = sName.replace(\']\', \'_\');
	oSpan = document.getElementById(sName);
	iLeft = iMaxLength - oField.value.length;
	oSpan.innerHTML = iLeft;

	if (oField.value.length > iMaxLength) {
  	oField.value = oField.value.substring(0, iMaxLength);
  	oSpan.innerHTML = 0;
    alert(sAlertText.replace(\'%d\', iMaxLength));
    }
  }';
			}
		}
		
		if ($bRequired || $sDependsOn != '') {
			$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
			$this->aValidator[$sName] = array(
				'type'			=> 'textarea',
				'err_msg'		=> $sErrMsg
			);
			if ($sDependsOn != '') {
				$this->aValidator[$sName]['depends_on'] = $sDependsOn;
			}
			$this->sJS .= $this->GetTextAreaJS($sName, $aAttribs['id'], $sErrMsg, $sDependsOn);
		}
		return Form::GetTextArea($sName, $sValue, $aAttribs, $iMaxlength);
	} // end GetTextAreaHTML()
	
	
	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sJS - kod JS
	 */
	function GetTextAreaJS($sName, $sID, $sErrMsg='', $sDependsOn='') {
		$sJS = '';

		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		if ($sDependsOn != '') {
			$sDependsOnJSName = preg_replace('/\[|\]/', '_', $sDependsOn);
			$sJS .= "\tif (document.getElementById('".$sDependsOnJSName."').checked) {\n\t";
		}
		$sJS .= "\to".$sJSName."=document.getElementById('".$sID."');\n\t";
		$sJS .= "\n\to".$sJSName.".value=o".$sJSName.".value.replace(/^\s+/g, '');"."\n";
		$sJS .= "if (o".$sJSName.".value == \"\") {\n\t\t";
		$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
		$sJS .= "\t}\n";
		if ($sDependsOn != '') {
			$sJS .= "\t}\n";
		}
		
		return $sJS;
	} // end GetTextAreaJS()
	
	
 /**
   * Metoda dodaje element textarea do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		integer	$iMaxLength				maksymalna dlugosc tekstu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
   * @return void
   */
  function AddTextArea($sName, $sLabel, $sValue='', $aAttribs=array(), $iMaxlength=0, $sErrMsg='', $sMaxLengthErr='', $bRequired=true, $aTHAttribs=array(), $aTDAttribs=array(), $sDependsOn='') {
		global $aConfig;
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		$this->aElements[$sName] = array(
    	'th' => '<label for="'.$aAttribs['id'].'">'.
    					$sLabel.
    					($iMaxlength > 0 ? ' (<span id="textarea_'.str_replace(array('[', ']'), array('_','_'), $aAttribs['id']).'">'.
    					($iMaxlength - utf8_strlen($sValue)).'</span>)' : '').
    					($bRequired?'&nbsp;<span id="'.$aAttribs['id'].'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span>': '').'</label>',
      'td' => $this->GetTextAreaHTML($sName, $sLabel, $sValue, $aAttribs, $iMaxlength, $sErrMsg, $sMaxLengthErr, $bRequired, $sDependsOn),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end AddTextArea
	
	
	/**
	 * Metoda zwraca kod HTML elementu select
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		array		$aOptions					atrybuty opcji elementu
	 * @param		mixed		$mSelected				list wartosci opcji ktore maja byc zaznaczone
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sHtml	kod HTML
	 */
	function GetSelectHTML($sName, $sLabel, $aAttribs=array(), $aOptions=array(), $mSelected='', $sErrMsg='', $bRequired=true, $sDependsOn='', $mDependsVal='') {
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		if ($bRequired || $sDependsOn != '') {
			$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
			$this->aValidator[$sName] = array(
				'type'			=> 'select',
				'err_msg'		=> $sErrMsg
			);
			if ($sDependsOn != '') {
				$this->aValidator[$sName]['depends_on'] = $sDependsOn;
			}
			if ($mDependsVal != '') {
				$this->aValidator[$sName]['depends_val'] = $mDependsVal;
			}
			$this->sJS .= $this->GetSelectJS($sName, $aAttribs['id'], $aAttribs['multiple'], $sErrMsg, $sDependsOn);
		}
		return Form::GetSelect($sName, $aAttribs, $aOptions, $mSelected);
	} // end of GetSelectHTML() method
	
	
	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
	 * 
	 * @param	string	$sName	nazwa elementu
	 * @param	string	$sID	ID elementu
   * @param	array   $iMultiple	1: element typu multiple; 0: element nie jest typu multiple
	 * @param	string	$sErrMsg	wiadomosc bledu
	 *
	 * @return	string	$sJS - kod JS
	 */
	function GetSelectJS($sName, $sID, $iMultiple, $sErrMsg='', $sDependsOn='') {
		$sJS = '';
		
		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		if ($sDependsOn != '') {
			$sDependsOnJSName = preg_replace('/\[|\]/', '_', $sDependsOn);
			$sJS .= "\tif (document.getElementById('".$sDependsOnJSName."').checked) {\n\t";
		}
		$sJS .= "\to".$sJSName."=document.getElementById('".$sID."');\n\t";
		$sJS .= "\n\tvar b".$sJSName."Val=false;\n";
		if ($iMultiple == '1') {
			$sJS .= "\tfor (var i=0; i < oForm.elements.length; i++) {\n".
			"\t\tif (oForm.elements[i].name == '".$sName."[]') {\n";
			$sJS .= "\t\t\tb".$sJSName."Val = (oForm.elements[i].selectedIndex >= 0);\n";
			$sJS .= "\t\t}\n".
			"\t}\n\n";
		}
		else {
			$sJS .= "\tb".$sJSName."Val = o".$sJSName.".selectedIndex > 0;\n";
		}
		$sJS .= "\tif (!b".$sJSName."Val) {\n";
		$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
		$sJS .= "\t}\n";
		if ($sDependsOn != '') {
			$sJS .= "\t}\n";
		}
		
		return $sJS;
	} // end of GetSelectJS() method

  /**
   * Metoda dodaje element tekstowy do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		array		$aOptions					atrybuty opcji elementu
	 * @param		mixed		$mSelected				list wartosci opcji ktore maja byc zaznaczone
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
   * @return void
   */
  function AddSelect($sName, $sLabel, $aAttribs=array(), $aOptions=array(), $mSelected='', $sErrMsg='', $bRequired=true, $aTHAttribs=array(), $aTDAttribs=array(), $sDependsOn='') {
		global $aConfig;
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		$this->aElements[$sName] = array(
    	'th' => '<label for="'.$aAttribs['id'].'">'.$sLabel.($bRequired?'&nbsp;<span id="'.$aAttribs['id'].'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span>': '').'</label>',
      'td' => $this->GetSelectHTML($sName, $sLabel, $aAttribs, $aOptions, $mSelected, $sErrMsg, $bRequired, $sDependsOn),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddSelect() method
  

	/**
	 * Metoda zwraca kod HTML elementu password
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
   * @param		string	$sPcre						wyrazenie regularne do walidacji
   * @param   string  $sDependsOn       pole wymagane jeśli wprowadzon
	 * @return	string	$sHtml	kod HTML
	 */
	function GetPasswordHTML($sName, $sLabel, $sValue='', $aAttribs=array(), $sErrMsg='', $bRequired=true, $sPcre='', $sDependsOn='') {
		global $aConfig;
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		if (!isset($aAttribs['maxlength']) && isset($aConfig['class']['form']['password']['maxlength'])
	    	&& !empty($aConfig['class']['form']['password']['maxlength'])) {
    	$aAttribs['maxlength'] =& $aConfig['class']['form']['password']['maxlength'];
		}
		
		if (empty($sPcre) && isset($aConfig['class']['form']['password']['pcre'])
				&& !empty($aConfig['class']['form']['password']['pcre'])) {
    	$sPcre =& $aConfig['class']['form']['password']['pcre'];
		}
				
		$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
		$this->aValidator[$sName] = array(
			'type'			=> 'password',
			'pcre'			=> $sPcre,
			'err_msg'		=> $sErrMsg,
			'required'	=> $bRequired
		);
		if ($sDependsOn != '') {
			$this->aValidator[$sName]['depends_on'] = $sDependsOn;
		}
		// kod JavaScript dla elementu typu password bedzie generowany niezaleznie
		// od tego czy element jest wymagany czy tez nie, w przypadku gdy element
		// nie jest wymagany ale istnieje dla niego pcre i tak jest sprawdzana poprawnosc
		// jego wypelnienia
		$this->sJS .= $this->GetPasswordJS($sName, $aAttribs['id'], $sErrMsg, $bRequired, $sPcre, $sDependsOn);
		return Form::GetPassword($sName, $sValue, $aAttribs);
	} // end GetPasswordHTML()
	
	
	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
	 * @return	string	$sJS - kod JS
	 */
	function GetPasswordJS($sName, $sID, $sErrMsg, $bRequired, $sPcre, $sDependsOn='') {
		$sJS = '';

		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		if ($sDependsOn != '') {
			$sDependsOnJSName = preg_replace('/\[|\]/', '_', $sDependsOn);
			$sJS .= "\tif (document.getElementById('".$sDependsOnJSName."').checked) {\n\t";
		}
		$sJS .= "\to".$sJSName."=document.getElementById('".$sID."');\n\t";
		$sJS .= "\n\to".$sJSName.".value=o".$sJSName.".value.replace(/^\s+/g, '');"."\n";
		if ($bRequired) {
			// element jest wymagan, normalny kod sprawdzenia elementu
			if (!empty($sPcre)) {
				$sJS .= "if (!o".$sJSName.".value.match(".$sPcre.")) {\n\t\t";
			}
			else {
				$sJS .= "if (o".$sJSName.".value == \"\") {\n\t\t";
			}
			$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
			$sJS .= "\t}\n";
		}
		else {
			if (!empty($sPcre)) {
				// element bedzie sprawdzany tylko wtedy gdy zostanie do niego cos wpisane
				$sJS .= "if (o".$sJSName.".value != \"\" && !o".$sJSName.".value.match(".$sPcre.")) {\n\t\t";
				$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
				$sJS .= "\t}\n";
			}
		}
		if ($sDependsOn != '') {
			$sJS .= "\t}\n";
		}
		return $sJS;
	} // end GetPasswordJS()

  /**
   * Metoda dodaje element password do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
   * @param		string	$sPcre						wyrazenie regularne do walidacji
   * @return void
   */
  function AddPassword($sName, $sLabel, $sValue='', $aAttribs=array(), $sErrMsg='', $bRequired=true, $sPcre='', $aTHAttribs=array(), $aTDAttribs=array(), $sDependsOn='') {
    global $aConfig;
    
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
    $this->aElements[$sName] = array(
    	'th' => '<label for="'.$aAttribs['id'].'">'.$sLabel.($bRequired?'&nbsp;<span id="'.$aAttribs['id'].'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span>': '').'</label>',
      'td' => $this->GetPasswordHTML($sName, $sLabel, $sValue, $aAttribs, $sErrMsg, $bRequired, $sPcre, $sDependsOn),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end AddPassword
  
  
	/**
	 * Metoda zwraca kod HTML elementu password do potwierdzenia hasla
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param   string  $sDependsOn       pole wymagane jeśli wprowadzon
	 * @return	string	$sHtml	kod HTML
	 */
	function GetConfirmPasswordHTML($sName, $sLabel, $sValue='', $sPasswdField, $aAttribs=array(), $sErrMsg='', $sDependsOn='') {
		global $aConfig;
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		if (!isset($aAttribs['maxlength']) && isset($aConfig['class']['form']['password']['maxlength'])
	    	&& !empty($aConfig['class']['form']['password']['maxlength'])) {
    	$aAttribs['maxlength'] =& $aConfig['class']['form']['password']['maxlength'];
		}
		
		$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
		$this->aValidator[$sName] = array(
			'type'					=> 'confirm_password',
			'passwd_field'	=> $sPasswdField,
			'err_msg'				=> $sErrMsg,
			'required'	=> true
		);
		if ($sDependsOn != '') {
			$this->aValidator[$sName]['depends_on'] = $sDependsOn;
		}
		$this->sJS .= $this->GetConfirmPasswordJS($sName, $aAttribs['id'], $sPasswdField, $sErrMsg, $sDependsOn);
		return Form::GetPassword($sName, $sValue, $aAttribs);
	} // end GetPasswordHTML()
	  

	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
	 * @return	string	$sJS - kod JS
	 */
	function GetConfirmPasswordJS($sName, $sID, $sPasswdField, $sErrMsg='', $sDependsOn='') {
		$sJS = '';
		
		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		$sJSName2 = preg_replace('/\[|\]/', '_', $sPasswdField);
		if ($sDependsOn != '') {
			$sDependsOnJSName = preg_replace('/\[|\]/', '_', $sDependsOn);
			$sJS .= "\tif (document.getElementById('".$sDependsOnJSName."').checked) {\n\t";
		}
		$sJS .= "\to".$sJSName."=document.getElementById('".$sID."');\n\t";
		$sJS .= "\to".$sJSName2."=document.getElementById('".$sPasswdField."');\n\t";
		$sJS .= "\t".'if (o'.$sJSName2.'.value != "" && o'.$sJSName.'.value != o'.$sJSName2.'.value) {'."\n";
		$sJS .= "\t".'sErr += "\t- '.$sErrMsg.'\n";'."\n";
		$sJS .= "\t}\n";
		if ($sDependsOn != '') {
			$sJS .= "\t}\n";
		}
		return $sJS;
	} // end GetConfirmPasswordJS()
	

  /**
   * Metoda dodaje element password do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		string	$sPasswdField			nazwa pola hasla do ktorego jest potwierdzenie
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @return void
   */
  function AddConfirmPassword($sName, $sLabel, $sValue='', $sPasswdField, $aAttribs=array(), $sErrMsg='', $bRequired=true, $aTHAttribs=array(), $aTDAttribs=array(), $sDependsOn='') {
    global $aConfig;
    
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
    $this->aElements[$sName] = array(
    	'th' => '<label for="'.$aAttribs['id'].'">'.$sLabel.($bRequired?'&nbsp;<span id="'.$aAttribs['id'].'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span>': '').'</label>',
      'td' => $this->GetConfirmPasswordHTML($sName, $sLabel, $sValue, $sPasswdField, $aAttribs, $sErrMsg, $sDependsOn),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end AddConfirmPassword
  
  
  /**
	 * Metoda zwraca kod HTML przycisku typu input przycisk
	 * @param		string	$sValue						wartosc elementu
	 * @return	string	$sHtml	kod HTML
	 */
	function GetInputButtonHTML($sName, $sValue='', $aAttribs=array(), $sType='submit') {
		return Form::GetInputButton($sName, $sValue, $aAttribs, $sType);
	} // end GetInputButtonHTML()
  
  
  /**
   * Metoda dodaje element typu input przycisk do tabeli/formularza
   *
   * @param		string  $sName						nazwa przycisku
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty przycisku
   * @param		string	$sType						typ przycisku	
   * @return void
   */
  function AddInputButton($sName, $sValue='', $aAttribs=array(), $sType='submit', $aTHAttribs=array(), $aTDAttribs=array()) {
        
		$this->aElements[$sName] = array(
    	'th' => '',
      'td' => $this->GetInputButtonHTML($sName, $sValue, $aAttribs, $sType),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
	} // end AddInputButton
	
	
	/**
	 * Metoda zwraca kod HTML checkboxa
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bChecked					czy domyslnie ma byc zaznaczony
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sHtml	kod HTML
	 */
	function GetCheckBoxHTML($sName, $sLabel, $aAttribs=array(), $sErrMsg='', $bChecked=false, $bRequired=true, $sDependsOn='', $mDependsVal='') {
    if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		if (!isset($aAttribs['value'])) {
    	$aAttribs['value'] = 1;
		}
    
    
		if ($bRequired || $sDependsOn != '') {
			$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
			$this->aValidator[$sName] = array(
				'type'					=> 'checkbox',
				'err_msg'				=> $sErrMsg
			);
			if ($sDependsOn != '') {
				$this->aValidator[$sName]['depends_on'] = $sDependsOn;
			}
			if ($mDependsVal != '') {
				$this->aValidator[$sName]['depends_val'] = $mDependsVal;
			}
			$this->sJS .= $this->GetCheckBoxJS($sName, $aAttribs['id'], $sErrMsg, $sDependsOn);
		}
		return Form::GetCheckBox($sName, $aAttribs, $sErrMsg, $bChecked);
	} // end GetCheckBoxHTML()
	
	
	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
   * @param		string  $sName						nazwa elementu
   * @param		string	$sErrMsg					komunikat bledu
	 * @return	string	$sJS - kod JS
	 */
	function GetCheckBoxJS($sName, $sID, $sErrMsg='', $sDependsOn='') {
		$sJS = '';
		
		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		if ($sDependsOn != '') {
			$sDependsOnJSName = preg_replace('/\[|\]/', '_', $sDependsOn);
			$sJS .= "\tif (document.getElementById('".$sDependsOnJSName."').checked) {\n\t";
		}
		$sJS .= "\to".$sJSName."=document.getElementById('".$sID."');\n\t";
		$sJS .= "\n\tb".$sJSName."Val=o".$sJSName.".checked;"."\n";
		$sJS .= "if (!b".$sJSName."Val) {\n\t\t";
		$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
		$sJS .= "\to".$sJSName."_err=true;\n\t";
		$sJS .= "\n\t}\n";
		if ($sDependsOn != '') {
			$sJS .= "\t}\n";
		}
		
		return $sJS;
	} // end GetCheckBoxJS()
	
	
 /**
   * Metoda dodaje element checkbox do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bChecked					czy domyslnie ma byc zaznaczony
   * @param		bool		$bRequired				czy pole wymagane
   * @return void
   */
  function AddCheckBox($sName, $sLabel, $aAttribs=array(), $sErrMsg='', $bChecked=false, $bRequired=true, $aTHAttribs=array(), $aTDAttribs=array(), $sDependsOn='') {
    global $aConfig;
    
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
    $this->aElements[$sName] = array(
    	'th' => '<label for="'.$aAttribs['id'].'">'.$sLabel.($bRequired?'&nbsp;<span id="'.$aAttribs['id'].'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span>': '').'</label>',
      'td' => $this->GetCheckBoxHTML($sName, $sLabel, $aAttribs, $sErrMsg, $bChecked, $bRequired, $sDependsOn),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end AddCheckBox
	
	
	/**
	 * Metoda zwraca kod HTML zestawu elementow typu checkbox
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		array		$aBoxes						atrybuty poszczegolnych checkboxow elementu
	 * @param		mixed		$aChecked					list checkboxow ktore maja byc zaznaczone
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @param		bool		$bReturnAsTable		true: zwroc jako tabele; false: zwroc jako liste <ul>
	 * @return	string	$sHtml	kod HTML
	 */
	function GetCheckBoxSetHTML($sName, $sLabel, $aBoxes=array(), $aChecked=array(), $sErrMsg='', $bRequired=true, $bReturnAsTable=true, $sDependsOn='') {
		global $aConfig;

		if ($bRequired || $sDependsOn != '') {
			$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
			$this->aValidator[$sName] = array(
				'type'					=> 'checkbox_set',
				'err_msg'				=> $sErrMsg
			);
			if ($sDependsOn != '') {
				$this->aValidator[$sName]['depends_on'] = $sDependsOn;
			}
			$this->sJS .= $this->GetCheckBoxSetJS($sName, $sErrMsg, $sDependsOn);
		}
		return Form::GetCheckBoxSet($sName, $aBoxes, $aChecked, $bReturnAsTable);
	} // end of GetCheckBoxSetHTML() method
	
	
	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
	 * 
	 * @param		string	$sName			nazwa elementu
	 * @param		string	$sLabel			etykieta elementu
	 * @param		string	$sErrMsg		wiadomosc bledu
	 * @param	bool	$bRequired	true: wymagane; false: niewymagane
	 *
	 * @return	string	$sJS - kod JS
	 */
	function GetCheckBoxSetJS($sName, $sErrMsg='', $sDependsOn='') {
		$sJS = '';
		
		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		if ($sDependsOn != '') {
			$sDependsOnJSName = preg_replace('/\[|\]/', '_', $sDependsOn);
			$sJS .= "\tif (document.getElementById('".$sDependsOnJSName."').checked) {\n\t";
		}
		$sJS .= "\n\tvar bChecked_".$sJSName." = false;\n";
		$sJS .= "\tfor (var i = 0; i< oForm.elements.length ; i++) {\n";
		$sJS .= "\t\tif (oForm.elements[i].name.indexOf('".$sName."[') == 0) {\n";
		$sJS .= "\t\t\tif (oForm.elements[i].checked) {\n";
		$sJS .= "\t\t\t\tbChecked_".$sJSName." = true;\n";
		$sJS .= "\t\t\t\tbreak;\n";
		$sJS .= "\t\t\t}\n";
		$sJS .= "\t\t}\n";
		$sJS .= "\t}\n\n";
		$sJS .= "\tif (!bChecked_".$sJSName.") {\n";
		$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
		$sJS .= "\n\t}\n";
		if ($sDependsOn != '') {
			$sJS .= "\t}\n";
		}
		
		return $sJS;
	} // end of GetCheckBoxSetJS() method
	
	
	/**
	 * Metoda zwraca kod HTML zestawu elementow typu checkbox
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		array		$aBoxes						atrybuty poszczegolnych checkboxow elementu
	 * @param		mixed		$aChecked					list checkboxow ktore maja byc zaznaczone
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @param		bool		$bReturnAsTable		true: zwroc jako tabele; false: zwroc jako liste <ul>
	 * @return	string	$sHtml	kod HTML
	 */
	function GetCheckBoxModulesSetHTML($sName, $sLabel, $aBoxes=array(), $aData=array(), $sErrMsg='', $bRequired=true, $aAttribs2=array(), $aAttribs3=array()) {
		global $aConfig;
		
		if ($bRequired) {
			$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
			foreach ($aBoxes as $aBox) {
				$aOptions[$aBox['value']] = $aBox['label'];
			}
			$this->aValidator[$sName] = array(
				'type'	=> 'checkbox_set_module',
				'err_msg'	=> $sErrMsg,
				'options'	=> $aOptions
			);
			$this->sJS .= $this->GetCheckBoxModulesSetJS($sName, $sErrMsg);
		}
		return Form::GetCheckBoxModulesSet($sName, $aBoxes, $aData, $aAttribs2, $aAttribs3);
	} // end of GetCheckBoxModulesSetHTML() method
	
	
	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
	 * 
	 * @param		string	$sName			nazwa elementu
	 * @param		string	$sErrMsg		wiadomosc bledu
	 * @param	bool	$bRequired	true: wymagane; false: niewymagane
	 *
	 * @return	string	$sJS - kod JS
	 */
	function GetCheckBoxModulesSetJS($sName, $sErrMsg='') {
		$sJS = '';
		
		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		$sJS .= "\n\tvar bChecked_".$sJSName." = false;\n";
		$sJS .= "\tfor (var i = 0; i< oForm.elements.length ; i++) {\n";
		$sJS .= "\t\tif (oForm.elements[i].name.indexOf('".$sName."[') == 0) {\n";
		$sJS .= "\t\t\tif (oForm.elements[i].checked) {\n";
		$sJS .= "\t\t\t\tbChecked_".$sJSName." = true;\n";
		$sJS .= "\t\t\t\taName = oForm.elements[i].name.split('[');\n";
		$sJS .= "\t\t\t\tif((oTxt2 = getObject(aName[0] + '_2[' + aName[1])) && (oTxt3 = getObject(aName[0] + '_3[' + aName[1])) && (oTxt4 = getObject(aName[0] + '_4[' + aName[1])) && (oTxt5 = getObject(aName[0] + '_5[' + aName[1])) && (oTxt6 = getObject(aName[0] + '_6[' + aName[1])) && (oLab = getObject('l_' + oForm.elements[i].name))) {\n";
		$sJS .= "\t\t\t\t\tif (oTxt2.value.length != 5 || oTxt3.value.length != 5 || oTxt4.value.length != 5 || oTxt5.value.length != 5 || oTxt6.value.length != 3) {\n";
		$sJS .= "\t\t\t\t\t\t".'sErr += "\t- '.$sErrMsg.': " + oLab.innerHTML + "\n";'."\n";
		$sJS .= "\t\t\t\t\t\tbreak;\n";
		$sJS .= "\t\t\t\t\t}\n";
		$sJS .= "\t\t\t\t}\n";
		$sJS .= "\t\t\t\telse if((oTxt2 = getObject(aName[0] + '_2[' + aName[1])) && (oTxt3 = getObject(aName[0] + '_3[' + aName[1])) && (oLab = getObject('l_' + oForm.elements[i].name))) {\n";
		$sJS .= "\t\t\t\t\tif (!oTxt2.value.match(/^\d{1,3}$/) || !oTxt3.value.match(/^\d{1,3}$/) || parseInt(oTxt2.value) == 0 || parseInt(oTxt3.value) == 0) {\n";
		$sJS .= "\t\t\t\t\t\t".'sErr += "\t- '.$sErrMsg.': " + oLab.innerHTML + "\n";'."\n";
		$sJS .= "\t\t\t\t\t\tbreak;\n";
		$sJS .= "\t\t\t\t\t}\n";
		$sJS .= "\t\t\t\t}\n";
		$sJS .= "\t\t\t}\n";
		$sJS .= "\t\t}\n";
		$sJS .= "\t}\n\n";
		$sJS .= "\tif (!bChecked_".$sJSName.") {\n";
		$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
		$sJS .= "\n\t}\n";
		
		return $sJS;
	} // end of GetCheckBoxModulesSetJS() method


  /**
   * Metoda dodaje element tekstowy do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		array		$aBoxes						atrybuty poszczegolnych checkboxow elementu
	 * @param		mixed		$aChecked					list checkboxow ktore maja byc zaznaczone
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @param		bool		$bReturnAsTable		true: zwroc jako tabele; false: oddzielone przez <br />
   * @return void
   */
  function AddCheckBoxSet($sName, $sLabel, $aBoxes=array(), $aChecked=array(), $sErrMsg='', $bRequired=true, $bReturnAsTable=true, $aTHAttribs=array(), $aTDAttribs=array(), $sDependsOn='') {
  	
    $this->aElements[$sName] = array(
    	'th' => '<label>'.$sLabel.($bRequired?'&nbsp;<span id="'.$sName.'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span>': '').'</label>',
      'td' => $this->GetCheckBoxSetHTML($sName, $sLabel, $aBoxes, $aChecked, $sErrMsg, $bRequired, $bReturnAsTable, $sDependsOn),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddCheckBoxSet() method
	
	
	/**
	 * Metoda zwraca kod HTML zestawu elementow typu radio
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		array		$aRadios					atrybuty poszczegolnych elementow radio elementu
	 * @param		mixed		$mChecked					list checkboxow ktore maja byc zaznaczone
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @param		bool		$bReturnAsTable		true: zwroc jako tabele; false: zwroc jako liste <ul>
	 * @return	string	$sHtml	kod HTML
	 */
	function GetRadioSetHTML($sName, $sLabel, $aRadios=array(), $mChecked='', $sErrMsg='', $bRequired=true, $bReturnAsTable=true, $sDependsOn='',$mDependsVal='') {
		global $aConfig;
		
		if ($bRequired || $sDependsOn != '') {
			$sErrMsg = !empty($sErrMsg) ? $sErrMsg : (!empty($sLabel) ? $sLabel : $sName);
			$this->aValidator[$sName] = array(
				'type'					=> 'radio_set',
				'err_msg'				=> $sErrMsg
			);
			if ($sDependsOn != '') {
				$this->aValidator[$sName]['depends_on'] = $sDependsOn;
			}
			if ($mDependsVal != '') {
				$this->aValidator[$sName]['depends_val'] = $mDependsVal;
			}
			$this->sJS .= $this->GetRadioSetJS($sName, $sErrMsg, $sDependsOn);
		}
		return Form::GetRadioSet($sName, $aRadios, $mChecked, $bReturnAsTable);
	} // end of GetRadioSetHTML() method
	
	
	/**
	 * Metoda zwraca kod JavaScript do walidacji elementu
	 * 
	 * @param		string	$sName			nazwa elementu
	 * @param		string	$sErrMsg		wiadomosc bledu
	 * @param	bool	$bRequired	true: wymagane; false: niewymagane
	 *
	 * @return	string	$sJS - kod JS
	 */
	function GetRadioSetJS($sName, $sErrMsg='', $sDependsOn='') {
		$sJS = '';
		
		$sJSName = preg_replace('/\[|\]/', '_', $sName);
		if ($sDependsOn != '') {
			$sDependsOnJSName = preg_replace('/\[|\]/', '_', $sDependsOn);
			$sJS .= "\tif (document.getElementById('".$sDependsOnJSName."').checked) {\n\t";
		}
		$sJS .= "\n\tvar bChecked_".$sJSName." = false;\n";
		$sJS .= "\tfor (var i = 0; i< oForm.elements.length ; i++) {\n";
		$sJS .= "\t\tif (oForm.elements[i].name == '".$sName."' && oForm.elements[i].checked) {\n";
		$sJS .= "\t\t\tbChecked_".$sJSName." = true;\n";
		$sJS .= "\t\t\tbreak;\n";
		$sJS .= "\t\t}\n";
		$sJS .= "\t}\n\n";
		$sJS .= "\tif (!bChecked_".$sJSName.") {\n";
		$sJS .= 'sErr += "\t- '.$sErrMsg.'\n";'."\n";
		$sJS .= "\n\t}\n";
		if ($sDependsOn != '') {
			$sJS .= "\t}\n";
		}
		
		return $sJS;
	} // end of GetRadioSetJS() method

  /**
   * Metoda dodaje zestaw elementow radio do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		array		$aBoxes						atrybuty poszczegolnych elementow radio elementu
	 * @param		mixed		$aChecked					radio ktore ma byc zaznaczone
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bRequired				czy pole wymagane
	 * @param		bool		$bReturnAsTable		true: zwroc jako tabele; false: oddzielone przez <br />
   * @return void
   */
  function AddRadioSet($sName, $sLabel, $aRadios=array(), $mChecked='', $sErrMsg='', $bRequired=true, $bReturnAsTable=true, $aTHAttribs=array(), $aTDAttribs=array(), $sDependsOn='', $sDependsVal='') {
    $this->aElements[$sName] = array(
    	'th' => '<label>'.$sLabel.($bRequired?'&nbsp;<span id="'.$sName.'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span></label>': ''),
      'td' => $this->GetRadioSetHTML($sName, $sLabel, $aRadios, $mChecked, $sErrMsg, $bRequired, $bReturnAsTable, $sDependsOn, $sDependsVal),
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddRadioSet() method
  
  
	/**
	 * Metoda zwraca kod HTML checkboxa
   * @param		string  $sName						nazwa elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
	 * @return	string	$sHtml	kod HTML
	 */
	function GetHiddenHTML($sName, $sValue='', $aAttribs=array()) {
    if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		return Form::GetHidden($sName, $sValue, $aAttribs);
	} // end GetHiddenHTML()
	
	
 /**
   * Metoda dodaje element hidden do tabeli/formularza
   *
   * @param		string  $sName						nazwa elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @return void
   */
  function AddHidden($sName, $sValue='', $aAttribs=array()) {
    
    $this->aHiddenElements[$sName] = $this->GetHiddenHTML($sName, $sValue, $aAttribs);
  } // end AddHidden
  
  function AddInfo($sValue) {
    $this->aElements[array_pop(array_keys($this->aElements))]['td'] .= Form::GetInfo($sValue);
  }

    /**
     * Metoda dodaje widok kart
     *
     * @param string $sName nazwa elementu
     * @param array $aTabs nazwy kart
     * @param array $aTHAttribs
     * @param array $aTDAttribs
     */
    function AddTabs($sName, $aTabs, $aTabsLabels, $aTHAttribs=array(), $aTDAttribs=array()) {
        $this->aElements[$sName] = array(
            'tabs' => $aTabs,
            'labels' => $aTabsLabels,
            'th' => '',
            'td' => '',
            'th_attribs' => $aTHAttribs,
            'td_attribs' => $aTDAttribs
        );
    }

    function AddTabFields($sName, $sTabName, $aContent, $aLabels) {
        $sFullContent = '';
        foreach ($aContent as $iKey => $sContent) {
            $sFullContent .= '<div class="tab-row"><div class="tab-row-header">' . $aLabels[$iKey] . '</div>' . $sContent . "</div>";
        }

        $this->aElements[$sName]['tabs'][$sTabName] = $sFullContent;
        $this->aElements[$sName]['td'] = $this->getTabsHTML($sName);

        $this->sAdditionalJS .= $this->GetTabsJS($sName, $sTabName);
    }

    function GetTabsHTML($sName) {
        $aTabs = $this->aElements[$sName]['tabs'];
        $aTabsLabels = $this->aElements[$sName]['labels'];
        $aTabsHTML = '<div class="tabs" name="' . $sName . '">';
        $aTabsHeaderHTML = '<div class="tabs-header">';
        $aTabsContentHTML = '<div class="tab-content">';

        foreach($aTabs as $sTabName => $sTab) {
            $aTabsHeaderHTML .= '<div class="tab-title ' .
                            (array_search($sTabName, array_keys($aTabs)) === 0 ? 'active' : '') .
                            '" name="' . $sTabName . '">' . $aTabsLabels[$sTabName] . '</div>';

            $aTabsContentHTML .= '<div class="tab ' .
                            (array_search($sTabName, array_keys($aTabs)) === 0 ? 'active' : '') .
                            '" name="' . $sTabName . '">' . $sTab . '</div>';
        }

        $aTabsHeaderHTML .= '</div>';
        $aTabsContentHTML .= '</div>';

        $sTabFillAllButton = '<input type="button" name="fillall" value="Wypełnij we wszyskich serwisach" id="fillall">';

        $aTabsHTML .= $aTabsHeaderHTML . $sTabFillAllButton . $aTabsContentHTML;

        return $aTabsHTML;
    }

    function GetTabsJS($sName, $sTabName) {
        $sJS = '';

        $sJSName = preg_replace('/\[|\]/', '_', $sName);
        $sJSTabName = preg_replace('/\[|\]/', '_', $sTabName);
        $sJS .= "$(function() {\n";
        $sJS .= "$('[name=\"" . $sJSName . "\"] .tab-title[name=\"" . $sJSTabName . "\"]').click(function() {\n";
        $sJS .= "if($(this).hasClass('active') === false) {";
        $sJS .= "$(this).parent().find('.active').toggleClass('active');\n";
        $sJS .= "$(this).toggleClass('active');\n";
        $sJS .= "$('.tab-content .active').toggleClass('active');\n";
        $sJS .= "$('.tab[name=\"" . $sJSTabName . "\"]').toggleClass('active');\n";
        $sJS .= "}\n";
        $sJS .= "});\n";
        $sJS .= "$('#fillall').click(function() {\n";
        $sJS .= "$('#" . $sJSTabName . "_name').val($('#profit24_name').val());\n";
        $sJS .= "$('#" . $sJSTabName . "_name2').val($('#profit24_name2').val());\n";
        $sJS .= "$('#" . $sJSTabName . "_description').parent().find('iframe').contents().find('.mceContentBody').html($('#profit24_description').parent().find('iframe').contents().find('.mceContentBody').get(0).innerHTML);\n";
        $sJS .= "$('#" . $sJSTabName . "_short_description').val($('#profit24_short_description').val());\n";
        $sJS .= "});\n";
        $sJS .= "});\n";


        return $sJS;
    }

  /**
   * Metoda dodaje wiersz do tabeli/formularza
   *
   * @param		string	$sLabel						etykieta elementu
   * @param		string	$sContent					tresc wiersza
	 * @param		bool		$bRequired				czy pole wymagane
	 * @return void
   */
  function AddRow($sLabel, $sContent, $sFor='', $aTHAttribs=array(), $aTDAttribs=array(), $bRequired=false) {
    $this->aElements[] = array(
    	'th' => (!empty($sLabel) ? '<label'.($sFor != '' ? ' for="'.$sFor.'"' : '').'>'.$sLabel.'&nbsp;<span class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span></label>' : ''),
      'td' => $sContent,
			'th_attribs' => $aTHAttribs,
			'td_attribs' => $aTDAttribs
    );
  } // end of AddRow() method
	
	
	/**
   * Metoda dodaje wiersz do tabeli/formularza
   *
   * @param		string	$sLabel						etykieta elementu
   * @param		string	$sContent					tresc wiersza
	 * @param		bool		$bRequired				czy pole wymagane
	 * @return void
   */
  function AddMergedRow($sLabel, $aTHAttribs=array(), $aTRAttribs=array()) {
    $this->aElements[] = array(
    	'th' => $sLabel,
      'td' => '',
			'th_attribs' => $aTHAttribs,
   		'tr_attribs' => $aTRAttribs,
			'td_attribs' => ''
    );
  } // end of AddMergedRow() method
  

  /**
   * Metoda zwraca naglowek formularza
   *
   * @return	string	$sHtml - naglowek formularza
   */
  function GetFormHeader() {
	  if ($this->bUseJS) {
			$this->aFormAttribs['onsubmit'] = 'return Validate_'.$this->sName.'_Form(this);';
		}

		return Form::GetHeader($this->sName, $this->aFormAttribs);
  } // end of GetFormHeader() metod

  /**
   * Metoda zwraca stopke formularza
   *
   * @return	string	$sHtml - stopka formularza
   */
  function GetFormFooter() {
    return Form::GetFooter();
  } // end of GetFormFooter() method
  
  
  function GetFieldJSBeginning($sName) {
    $sJSName = preg_replace('/\[|\]/', '_', $sName);
		return "\to".$sJSName."_err=false;\n\t";
	}
  
  /**
   * Metoda ustawia JavaScript do walidacji formularza
   *
   * @return	string	$sJS - kod JavaScript
   */
  function GetJS($sExtraJS='') {
    $sJS = '';

    if ($this->bUseJS) {
    	$sJS .= "<script type=\"text/javascript\">\n";
    	$sJS .= "// <![CDATA[\n";
    	$sJS .= $this->sAdditionalJS;
    	$sJS .= "function Validate_".$this->sName."_Form(oForm) {\n";
    	$sJS .= "\tvar sErr = \"\";\n";
    	$sJS .= $this->sJS.$sExtraJS;
    	$sJS .= "\n\tif (sErr != \"\") {\n";
        $sJS .= '
        hideOverlay = true;
        ';
      $sJS .= "\t\treturn ShowMessageBox('".$this->sErrPrefix."', sErr, '".$this->sErrPostfix."', true);";
      
      $sJS .= "\n\t}\n}\n";
      $sJS .= "// ]]>\n";
      $sJS .= "</script>";
		}
		if (!empty($this->aJavaScript)) {
			$sJS .= "<script type=\"text/javascript\">\n";
    	$sJS .= "// <![CDATA[\n";
    	foreach ($this->aJavaScript as $sScript) {
    		$sJS .= $sScript."\n\n";
			}
			$sJS .= "// ]]>\n";
      $sJS .= "</script>";
		}
    return $sJS;
  } // end of GetJS() method
  
  
	/**
	 * Metoda zwraca prefix bledu
	 */
  function GetErrorPrefix() {
  	return $this->sErrPrefix;
	} // end of GetErrorPrefix() method
	
	
	/**
	 * Metoda zwraca postfix bledu
	 */
	function GetErrorPostfix() {
  	return $this->sErrPostfix;
	} // end of GetErrorPostfix() method
	
  /**
   * Metoda zwraca kod walidatora
   * 
   * @return	string
   */
	function GetValidator() {
		if (!empty($this->aValidator)) {
			return base64_encode(serialize($this->aValidator));
		}
		return '';
	} // end of GetValidator() method
	
  function setAdditionalJS($sJs){
  	$this->sAdditionalJS.=$sJs;
  }
	function ShowForm() {
		$sHtml = $this->GetJS()."\n";
		$sHtml .= $this->GetFormHeader()."\n";
		
		// dodanie ukrytych pol z prefixem i postfixem komunikatu o bledzie
		$this->AddHidden('__ErrPrefix', $this->GetErrorPrefix(), array('id' => $this->sName.'__ErrPrefix'));
		$this->AddHidden('__ErrPostfix', $this->GetErrorPostfix(), array('id' => $this->sName.'__ErrPostfix'));
		
		// jezeli tablica walidatora nie jest pusta
		// dodanie ukrytego pola z zakodowana zawartoscia walidatora
		$this->AddHidden('__Validator', $this->GetValidator(), array('id' => $this->sName.'__Validator'));

		// dodanie elementow typu hidden
		foreach ($this->aHiddenElements as $sName => $sElement) {
			$sHtml .= $sElement."\n";
		}
		
		// naglowek tabeli
		if (!empty($this->sTableHeader)) {
			$sHtml .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
					<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
					<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
				</tr>
				<tr>
					<td width="1" height="18" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
					<td height="18" class="brownBg"><span class="tableHeaderBg"><strong>'.encodeHeaderString($this->sTableHeader).$this->sTableAddHeader.'</strong></span></td>
					<td width="1" height="18" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
				</tr>
				<tr>
					<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
					<td height="1" class="brownBg"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
					<td width="1" height="1"><img src="gfx/pixel.gif" width="1" height="1" alt=""></td>
				</tr>
			</table>';
		}
		$sHtml .= '<table';
		foreach ($this->aTableAttribs as $sAttrib => $sValue) {
			$sHtml .= ' '.$sAttrib.'="'.$sValue.'"';
		}
		$sHtml .= '>
			<colgroup>
				<col width="'.$this->iColWidth.'" />
				<col />
			</colgroup>';
		foreach ($this->aElements as $sName => $aElement) {
      if (!empty($aElement['th_attribs'])) {
        $aElement['th_attribs'] = array_merge($this->aTHAttribs, $aElement['th_attribs']);
      } else {
        $aElement['th_attribs'] = $this->aTHAttribs;
      }
			$sHtml .= "<tr";
			if(!empty($aElement['tr_attribs'])){
				foreach($aElement['tr_attribs'] as $key => $value) {
					$sHtml .= ' '.$key.'="'.$value.'"';
				}
			}
			$sHtml .= ">\n";
			$sHtml .= '<th';
			foreach($aElement['th_attribs'] as $key => $value) {
				$sHtml .= ' '.$key.'="'.$value.'"';
			}
			$sHtml .= empty($aElement['td']) ? ' colspan="2"' : '';
			$sHtml .= '>';
			$sHtml .= (!empty($aElement['th']) ? $aElement['th'] : '&nbsp;');
			$sHtml .= "</th>\n";
			if (!empty($aElement['td'])) {
				$aElement['td_attribs'] = array_merge($this->aTDAttribs, $aElement['td_attribs']);
				$sHtml .= '<td';
				foreach($aElement['td_attribs'] as $key => $value) {
					$sHtml .= ' '.$key.'="'.$value.'"';
				}
				$sHtml .= '>';
				$sHtml .= $aElement['td'];
				$sHtml .= "</td>\n";
			}
			$sHtml .= "</tr>\n";
		}
		$sHtml .= "</table>\n";
		$sHtml .= $this->GetFormFooter()."\n";
		return $sHtml;
	} // end of ShowForm() method
} // end of class FormTable
?>