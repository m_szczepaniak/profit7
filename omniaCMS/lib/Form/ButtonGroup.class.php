<?php
namespace omniaCMS\lib\Form;
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-05 
 * @copyrights Marcin Chudy - Profit24.pl
 */

/**
 * Lista przycisków
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ButtonGroup {
  private $aHTMLElements;
  private $sActionURL;
  
  public function __construct($sActionURL) {
    $this->sActionURL = $sActionURL;
  }
  
  /**
   * 
   * @param array $aButtons
   * @param array $aAttribs
   * @return \omniaCMS\lib\Form\ButtonGroup
   */
  public function AddButtonsArray($aButtons, $aAttribs) {
    
    foreach ($aButtons as $aButton) {
      $this->AddButton($aButton['label'], $aButton['value'], $aAttribs);
    }
    return $this;
  }
  
  
  /**
   * 
   * @param string $sHeader
   * @param int $iHNumber
   * @return \omniaCMS\lib\Form\ButtonGroup
   */
  public function AddHeader ($sHeader, $iHNumber) {
    
    $this->aHTMLElements[] = '<h'.$iHNumber.'>'.$sHeader.'<h'.$iHNumber.'/>';
    return $this;
  }
  
  
  /**
   * Dodajemy pojedynczy przycisk
   * 
   * @param string $sName
   * @param string $sValue
   * @param array $aAttribs
   * @return \omniaCMS\lib\Form\ButtonGroup
   */
  public function AddButton($sName, $sValue, $aAttribs) {
    
    $sHtml = '<div style="display: inline;"><form action="'.$this->sActionURL.'" method="POST">';
		$sHtml .= '<button type="submit" name="magazine" value="'.$sValue.'"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		$sHtml .= '>'.$sName.'</button>';
    $sHtml .= '</form></div>';
    $this->aHTMLElements[] = $sHtml;
    return $this;
  }
  
  /**
   * 
   * @return string HTML
   */
  public function GetHTML() {
    
    return implode($this->aHTMLElements);
  }
}
