<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */

/**
 * Validacja telefonu, wygenerowany regexp + sprawdzenie długości w php
 * Uwaga tablica 
 * 
 * @link http://www.uke.gov.pl/tablice/home.do?execution=e1s1 Tablica pobra z , 
 * następnie preg_replace '(\d)-0', 0,($1)-9 poniważ dziwnie traktują zakres liczbowy, 
 * jakby 0 było po 9
 * 
 * @todo do RegExp dodadać validację długości, validacja aktualnie odbywa się w PHP, a jeśli znajdzie się w regexp można przenieść do common.inc.php
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ValidatePhone {
  private $phone_prefix = array(
  '500',
  '501',
  '502',
  '503',
  '504',
  '505',
  '506',
  '507',
  '508',
  '509',
  '510',
  '511',
  '512',
  '513',
  '514',
  '515',
  '516',
  '517',
  '518',
  '519',
  '530',
  '531',
  '532',
  '533',
  '534',
  '535',
  '536(0-5,7-9)',
  '5366',
  '537',
  '538',
  '539',
  '570',
  '571(1-4)',
  '571(0,5-9)',
  '57(2,3)',
  '574',
  '575',
  '57(6,7,9)',
  '578',
  '600',
  '601',
  '602',
  '603',
  '604',
  '605',
  '606',
  '607',
  '608',
  '609',
  '660',
  '661',
  '662',
  '663',
  '664',
  '665',
  '666(0-5,7-9)',
  '6666',
  '667',
  '668',
  '669',
  '690(1-6)',
  '6907',
  '690(0,8-9)',
  '691',
  '692',
  '693',
  '694',
  '695',
  '696',
  '697',
  '698',
  '69900',
  '69901',
  '69902',
  '69903',
  '69904',
  '69905',
  '69906',
  '69907',
  '69908',
  '69909',
  '69910',
  '69911',
  '69912',
  '69913',
  '69914',
  '69915',
  '69916',
  '69917',
  '69918',
  '69919',
  '69920',
  '69921',
  '69922',
  '69923',
  '69924',
  '69925',
  '69926',
  '69927',
  '69928',
  '69929',
  '6993',
  '6994',
  '69950',
  '6995(1-9)',
  '69960',
  '69961',
  '69962',
  '69963',
  '69964',
  '69965',
  '69966',
  '69967',
  '69968',
  '69969',
  '69970',
  '69971',
  '6997(2,3,5-7)',
  '69974',
  '69978',
  '69979',
  '6998',
  '6999',
  '720(1-7,9,0)',
  '7208',
  '721',
  '722',
  '723',
  '724',
  '725',
  '726',
  '7271',
  '727(2,3)',
  '727(0,4-9)',
  '7280',
  '728(1-9)',
  '7290',
  '7291',
  '729(2-6)',
  '72970',
  '72971',
  '72972',
  '7297(3,4)',
  '72975',
  '72976',
  '7297(7-9)',
  '729(8,9)',
  '730',
  '731',
  '732',
  '733',
  '734(1-4)',
  '734(0,5-9)',
  '73(5,6)',
  '7370',
  '737(1,2)',
  '737(3-7)',
  '737(8,9)',
  '738',
  '7391',
  '739(2,0)',
  '73930',
  '7393(1-9)',
  '739(4-6)',
  '7397',
  '739(8,9)',
  '780(1,5,6,0)',
  '7802(1-4,7)',
  '7802(5,6)',
  '7802(0,8-9)',
  '780(3,4,7-9)',
  '781',
  '782',
  '783',
  '784',
  '785',
  '7860',
  '786(1,2)',
  '7863',
  '7864',
  '786(5-9)',
  '787',
  '788',
  '7890',
  '7891',
  '7892',
  '7893',
  '7894',
  '789(5-9)',
  '790',
  '791',
  '792',
  '793',
  '794',
  '795(1-5)',
  '795(0,6-9)',
  '796',
  '797',
  '798',
  '7990',
  '799(1-5)',
  '7996',
  '799(7-9)',
  '880',
  '8811',
  '881(2-7)',
  '881(0,8-9)',
  '882',
  '883(1,2,4-7,9,0)',
  '883(3,8)',
  '884(1,2)',
  '884(3,0,6-9)',
  '8844',
  '8845',
  '885',
  '886',
  '887',
  '888',
  '889',
  );
  
  CONST LENGTH_OF_PHONE_NUMBER = 9;
  
  public function __construct() {}
  
  /**
   * 
   * @return bool
   */
  public function validate($sPhone) {

    if ($this->_validateByLength(preg_replace('/[\s\-]/', '', $sPhone)) === FALSE) {
      return false;
    }
    
    if ($this->_validateByPrefix($sPhone) === FALSE) {
      //return false;
    }
    
    return true;
  }
  
  /**
   * 
   * @param string $sPhone
   * @return bool
   */
  private function _validateByLength($sPhone) {
    
    $aMatches = array();
    preg_match('/^\d{'.self::LENGTH_OF_PHONE_NUMBER.'}$/', $sPhone, $aMatches);
    return (!empty($aMatches) ? TRUE : FALSE );
  }
  
  
  /**
   * @param string $sPhone
   * @return bool
   */
  private function _validateByPrefix($sPhone) {
    $aMatches = array();
    
    $sRegExp = $this->getPrefixRegExp();
    preg_match($sRegExp, $sPhone, $aMatches);
    return (!empty($aMatches) && isset($aMatches[0]));
  }
  
  
  /**
   * 
   * @param string $sPhonePrefix
   * @return string RegExp
   */
  private function _formatRegExp($sPhonePrefix) {
    
    $sPhonePrefix = str_replace(',', '|', $sPhonePrefix);
    $sPhoneRegexp = preg_replace('/(\d+-\d)/', '[$1]', $sPhonePrefix);
    return $sPhoneRegexp;
  }

  /**
   * 
   * @return string RegExp
   */
  public function getPrefixRegExp() {
    $sRegExp = '/^(';
    foreach ($this->phone_prefix as $sPhonePrefix) {
      $sRegExp .= '(' . $this->_formatRegExp($sPhonePrefix) . ')|';
    }
    $sRegExp = substr($sRegExp, 0, strlen($sRegExp)-1);
    $sRegExp .= ')/';
    return $sRegExp;
  }
}
