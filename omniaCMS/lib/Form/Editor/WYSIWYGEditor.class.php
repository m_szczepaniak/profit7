<?php
/**
 * Klasa edytora WYSIWYG
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2004 - 2006
 * @version   1.0
 * @todo
 */

// tablica licznikowa edytorow, zawiera wszystkie nazwy edytorow dostepnych
// w aktualnym dokumencie.
static $aEditors;

class WYSIWYGEditor {
	
	// ustawienia edytora
	var $aAttributes;
	
	// konfiguracja edytora tinyMCE
	var $aParameters;
	
	// nazwa edytor
	var $sName;
	
	// prosty tryb
	var $bSimple;
	
	// usuwac adres bazowy z URLi czy tez nie
	var $bStripURLs;
		
	// constructor
  function WYSIWYGEditor($sName, $aAttributes) {
		global $aEditors, $aConfig;
		$this->bSimple = false;
		$this->bStripURLs = true;
		$this->sName = $sName;
		
    if (!is_array($aEditors)) {
      $aEditors = array();
    }
    array_push($aEditors, $sName);
		
    if (empty($aAttributes['mode'])) {
      $this->aParameters['mode'] = 'exact';
    }
    else {
    	$this->aParameters['mode'] = $aAttributes['mode'];
    }
    if (isset($aAttributes['theme']) && !empty($aAttributes['theme'])) {
      $this->bSimple = true;
    }
    if (isset($aAttributes['strip_urls']) && $aAttributes['strip_urls'] == '0') {
      $this->bStripURLs = false;
    }
    if (empty($aAttributes['lang'])) {
      $this->aParameters['language'] = 'pl';
      //$this->aParameters['language'] = 'en';
    }
    else {
    	$this->aParameters['lang'] = $aAttributes['lang'];
    }
    if (empty($aAttributes['width'])) {
      $this->aParameters['width'] = '100%';
    }
    else {
    	$this->aParameters['width'] = $aAttributes['width'];
    }
    if (empty($aAttributes['height'])) {
      $this->aParameters['height'] = '350px';
    }
    else {
    	$this->aParameters['height'] = $aAttributes['height'];
    }
    if (empty($aAttributes['css_style'])) {
      $this->aParameters['content_css'] = $aConfig['common']['client_base_url_http'].$aConfig['wysiwyg']['css_style'];
    }
    else {
    	$this->aParameters['content_css'] = $aAttributes['css_style'];
    }
    
    $this->aParameters['elements'] = $sName;
    $this->aParameters['browsers'] = 'msie,gecko,opera,safari';
    $this->aParameters['dialog_type'] = 'modal';
    $this->aParameters['inline_styles'] = 'true';
    $this->aParameters['remove_linebreaks'] = 'false';
    $this->aParameters['fix_list_elements'] = 'true';
    $this->aParameters['fix_table_elements'] = 'true';
    $this->aParameters['entity_encoding'] = 'raw';
    $this->aParameters['theme_advanced_toolbar_location'] = 'top';
    $this->aParameters['theme_advanced_toolbar_align'] = 'left';
    $this->aParameters['debug'] = 'false';
    $this->aParameters['extended_valid_elements'] = 'iframe[src|scrolling|frameborder|width|height],object[classid|codebase|width|height],param[name|value],embed[class|src|type|wmode|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|bgcolor=#ffffff|flashVars|base|allowFullScreen|allowScriptAccess|seamlesstabbing|swLiveConnect|pluginspage],script[type|language|src]';
    
    if (!$this->bSimple) {
    	$this->aParameters['urlconverter_callback'] = 'myURLConverter';
    	// modalna wersja
    	$this->aParameters['plugins'] = 'safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups';
    	// okienkowa wersja
    	//$this->aParameters['plugins'] = 'safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell';
    	$this->aParameters['file_browser_callback'] = 'fileBrowserCallBack';
    	$this->aParameters['apply_source_formatting'] = 'true';

    	$this->aParameters['theme_advanced_buttons1'] = 'cut,copy,paste,pastetext,pasteword,separator,undo,redo,separator,styleselect,formatselect,fontsizeselect';
    	$this->aParameters['theme_advanced_buttons2'] = 'bold,italic,underline,strikethrough,separator,bullist,numlist,separator,indent,outdent,separator,sub,sup,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,forecolor,backcolor,separator,search,replace,separator,link,unlink,separator,image';
			$this->aParameters['theme_advanced_buttons3'] = 'tablecontrols,visualaid,separator,hr,separator,charmap,separator,removeformat,cleanup,separator,code,media,advimage';
 		}
    else {
    	$this->aParameters['plugins'] = 'paste';
	    
	    $this->aParameters['valid_elements'] = 'p,strong/b,em/i,u,ul,ol,li,a[href|title|target],img[src|alt|width|height|vspace|hspace|border|align],br';
	    
	    $this->aParameters['theme_advanced_buttons1'] = 'cut,copy,paste,pastetext,pasteword,separator,undo,redo,separator,bold,italic,underline,strikethrough,separator,bullist,numlist,separator,indent,outdent,separator,sub,sup,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,forecolor,backcolor,separator,link,unlink';
	    $this->aParameters['theme_advanced_buttons2'] = '';
			$this->aParameters['theme_advanced_buttons3'] = '';
    }
		$this->aParameters['paste_use_dialog'] = 'true';
		$this->aParameters['paste_convert_middot_lists'] = 'true';
		$this->aParameters['paste_unindented_list_class'] = '';
		$this->aParameters['paste_convert_headers_to_strong'] = 'flase';
  } // end constructor


	/**
	 * Metoda zwraca kod HTML elementu
	 * @return	string	$sHTML	kod HTML
	 */
	function GetHTML() {
		global $aEditors, $aConfig, $sLibDir;
		$sHTML = '';
    $sInitValue = '';
    $sScript = '';
    
    if ($aEditors[0] == $this->sName) {
      // dolaczamy plik JavaScript
      $sHTML = '<script language="JavaScript" type="text/javascript" src="lib/Form/Editor/tiny_mce/tiny_mce.js"></script>';
      if (!$this->bSimple) {
				$sHTML .= '<script language="javascript" type="text/javascript">
		function myURLConverter(url, node, on_save) {
			'.($this->bStripURLs ? 'url = url.replace(/^'.str_replace('/', '\/', $aConfig['common']['client_base_url_http']).'/, \'/\');
			if (!url.match(/^(http|mailto:|news|javascript:|gg:|skype:)/) && !url.match(/^\//))
				url = \'/\' + url;' : '').'
			return url;
		}
</script>';
      $sHTML .= '
<script type="text/javascript" src="lib/Form/Editor/tiny_mce/plugins/advimage/assets/dialog.js"></script>
<script type="text/javascript" src="lib/Form/Editor/tiny_mce/plugins/advimage/IMEStandalone.js"></script>
<script language="javascript" type="text/javascript">
		var connector = "manager.php";
		base = "'.$aConfig['common']['client_base_path'].'images/editor";	// dynamic path to images dir
		path = "/images/editor";		// relative url to images dir
		//Create a new Imanager Manager, needs the directory and which language translation to use.
		var manager = new ImageManager(\'\', \''.$this->aParameters['language'].'\');
</script>
<script language="javascript" type="text/javascript" src="lib/Form/Editor/tiny_mce/plugins/advimage/tinycall.js"></script>';
			}
		}
		
		foreach ($this->aParameters as $sName => $mValue) {
			$sScript .= "".$sName.' : '.
			($mValue != 'false' && $mValue != 'true' ? '"'.$mValue.'"' : $mValue).', ';
		}
		$sScript = substr($sScript, 0, -2);
		
		$sHTML .= '<script language="javascript" type="text/javascript">
	tinyMCE.init({';
		$sHTML .= $sScript;
		$sHTML .= '});
				</script>';
		
		return $sHTML;
	} // end GetHTML()
} // end class WYSIWYGEditor
?>