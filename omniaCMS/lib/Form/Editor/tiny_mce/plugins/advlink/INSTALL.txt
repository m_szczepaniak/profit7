INSTALL Instructions as Image Uploader & Editor for TinyMCE



I) Adapte following scripts' calls and place them in the page after the usual tinyMCE.init call.

a) Replace '../ImageManager/' for the relative path from the page to the ImageManager folder.

b) Replace '../../../ImageManager' for the relative path from 'tiny_mce/plugins/advimage' 
to the ImageManager folder.

c) Replace "/var/timg" for the full path where you want to upload images

d) Replace "/timg" for the relative path from your page to the previous path (image upload folder).



II) Place the following (Adapted) calls after the javascript that loads tinyMCE.init call in your page.

<!-- start of calls -->
<script type="text/javascript" src="../ImageManager/assets/dialog.js"></script>
<script type="text/javascript" src="../ImageManager/IMEStandalone.js"></script>
<script language="javascript" type="text/javascript">
		var connector = "../../../ImageManager/manager.php";
		base = "/var/timg";	// dynamic path to images dir
		path = "/timg";		// relative url to images dir
		//Create a new Imanager Manager, needs the directory and which language translation to use.
		var manager = new ImageManager('../ImageManager','en');	
</script>
<script language="javascript" type="text/javascript" src="../ImageManager/tinycall.js"></script>
<!-- end of calls -->



III) See test.html for standalone use and test_tinymce.html for using it with TinyMCE



Jaume Teixi 02/09/2007 <jaume@NOSPAM@teixi.net>


