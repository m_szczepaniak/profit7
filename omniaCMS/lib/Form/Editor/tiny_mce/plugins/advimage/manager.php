<?php
/**
 * The main GUI for the ImageManager.
 * @author $Author: Wei Zhuo $
 * @version $Id: manager.php 26 2004-03-31 02:35:21Z Wei Zhuo $
 * @package ImageManager
 */
 
// Teixi 18/08/2007 dynamic vars from GET & custom_image_browser_callback
/*	$file = 'phpconfig.inc.php';
	$continguts='<?php
$IMConfig[\'base_dir\'] = \'' .$_GET['b']. '\';
$IMConfig[\'base_url\'] = \'http://' .$_SERVER['SERVER_NAME'].$_GET['p'].'\';
?>';
	if (!file_exists($file)) touch($file);
	$towrite=$continguts;
	$fh2 = fopen($file, 'w+');
	$ab=fwrite($fh2, $towrite);
	fclose($fh2);
// end */
require_once('config.inc.php');
$IMConfig['base_dir'] = $_GET['b'];
$IMConfig['base_url'] = 'http://' .$_SERVER['SERVER_NAME'].$_GET['p'];	
require_once('Classes/ImageManager.php');	
	
$manager = new ImageManager($IMConfig);
$dirs = $manager->getDirs();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
	<title>Zarządzanie zdjęciami</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <link href="assets/manager.css" rel="stylesheet" type="text/css" />	
<script type="text/javascript" src="assets/popup.js"></script>
<script type="text/javascript" src="assets/dialog.js"></script>
<script type="text/javascript">
/*<![CDATA[*/
	window.resizeTo(600, 480);

	if(window.opener)
		I18N = window.opener.ImageManager.I18N;

	var thumbdir = "<?php echo $IMConfig['thumbnail_dir']; ?>";
	var base_url = "<?php echo $manager->getBaseURL(); ?>";
	var baseurl = "<?=$_GET[b]?>";
	var basepath = "<?=$_GET[p]?>";
/*]]>*/
</script>
<script type="text/javascript" src="assets/manager.js"></script>
</head>
<body>
<div class="title">Wstawianie zdjęcia</div>
<form action="images.php?b=<?=$_GET[b]?>&p=<?=$_GET[p]?>" id="uploadForm" method="post" enctype="multipart/form-data">
<fieldset><legend>Menedżer zdjęć</legend>
<div class="dirs">
	<label for="dirPath">Katalog</label>
	<select name="dir" class="dirWidth" id="dirPath" onchange="updateDir(this)">
	<option value="/">/</option>
<?php foreach($dirs as $relative=>$fullpath) { ?>
		<option value="<?php echo rawurlencode($relative); ?>"><?php echo $relative; ?></option>
<?php } ?>
	</select>
	<a href="#" onclick="javascript: goUpDir();" title="Katalog wyżej"><img src="img/btnFolderUp.gif" height="15" width="15" alt="Katalog wyżej" /></a>
<?php if($IMConfig['safe_mode'] == false && $IMConfig['allow_new_dir']) { ?>
	<a href="#" onclick="newFolder();" title="Nowy katalog"><img src="img/btnFolderNew.gif" height="15" width="15" alt="Nowy katalog" /></a>
<?php } ?>
	<div id="messages" style="display: none;"><span id="message"></span><img SRC="img/dots.gif" width="22" height="12" alt="..." /></div>
	<iframe src="images.php?b=<?=$_GET[b]?>&p=<?=$_GET[p]?>" name="imgManager" id="imgManager" class="imageFrame" scrolling="auto" title="Wybór zdjęcia" frameborder="0"></iframe>
</div>
</fieldset>
<!-- image properties -->
	<table class="inputTable" border="0">
		<tr>
			<td align="right"><label for="f_url">Zdjęcie</label></td>
			<td><input type="text" id="f_url" class="largelWidth" value="" /></td>
			<td rowspan="3" align="right">&nbsp;</td>
			<td align="right"><!--<label for="f_width">Width</label>--></td>
			<td><input type="hidden" id="f_width" value="" /></td>
			<td rowspan="2" align="right"><!--<img src="img/locked.gif" id="imgLock" width="25" height="32" alt="Constrained Proportions" />--></td>
			<td rowspan="3" align="right">&nbsp;</td>
			<td align="right"><!--<label for="f_vert">V Space</label>--></td>
			<td><input type="hidden" id="f_vert" value="" /></td>
		</tr>		
		<tr>
			<td align="right"><label for="f_alt">Opis zdjęcia</label></td>
			<td><input type="text" id="f_alt" class="largelWidth" value="" /></td>
			<td align="right"><!--<label for="f_height">Height</label>--></td>
			<td><input type="hidden" id="f_height" value="" /></td>
			<td align="right"><!--<label for="f_horiz">H Space</label>--></td>
			<td><input type="hidden" id="f_horiz" value="" /></td>
		</tr>
		<tr>
<?php if($IMConfig['allow_upload'] == true) { ?>
			<td align="right"><label for="upload">Prześlij <br />
			<strong><?php echo ($IMConfig['max_size'] / (1024 * 1024)); ?> MB max.</strong></label></td>
			<td>
				<table cellpadding="0" cellspacing="0" border="0">
                  <tr>
                    <td><input type="file" name="upload" id="upload"/></td>
                    <td>&nbsp;<button type="submit" name="submit" onclick="doUpload();"/>Prześlij</button></td>
                  </tr>
                </table>
			</td>
<?php } else { ?>
			<td colspan="2"></td>
<?php } ?>
			<td align="right"><!--<label for="f_align">Align</label>--></td>
			<td colspan="2">
				<input type="hidden" id="f_align" value="" />
				<!--<select size="1" id="f_align"  title="Positioning of this image">
				  <option value=""                             >Not Set</option>
				  <option value="left"                         >Left</option>
				  <option value="right"                        >Right</option>
				  <option value="texttop"                      >Texttop</option>
				  <option value="absmiddle"                    >Absmiddle</option>
				  <option value="baseline" selected="selected" >Baseline</option>
				  <option value="absbottom"                    >Absbottom</option>
				  <option value="bottom"                       >Bottom</option>
				  <option value="middle"                       >Middle</option>
				  <option value="top"                          >Top</option>
				</select>-->
			</td>
			<td align="right"><!--<label for="f_border">Border</label>--></td>
			<td><input type="hidden" id="f_border" value="" /></td>
		</tr>
		<tr> 
         <td colspan="4" align="right">
				<input type="hidden" id="orginal_width" />
				<input type="hidden" id="orginal_height" />
				<input type="hidden" id="constrain_prop" />
        <!--<input type="checkbox" id="constrain_prop" checked="checked" onclick="javascript:toggleConstrains(this);" />-->
          </td>
          <td colspan="5"><!--<label for="constrain_prop">Constrain Proportions</label>--></td>
      </tr>
	</table>
<!--// image properties -->	
	<div style="text-align:right;"> 
		  <button type="button" class="buttons" onclick="return refresh();">Odśwież</button>
          <button type="button" class="buttons" onclick="return onOK();">OK</button>
          <button type="button" class="buttons" onclick="return onCancel();">Anuluj</button>
    </div>
	<input type="hidden" id="f_file" name="f_file" />
</form>
</body>
</html>