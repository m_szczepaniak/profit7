<?php
/**
 * Klasa Form do obslugi formularzy
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2004 - 2006
 * @version   1.0
 */
class Form {
  
	/**
   * Konstruktor klsy Form
   *
	 * @return void
   */
  function Form() {
  	
	} // end of Form() method

	
	/**
	 * Metoda zwraca kod HTML etykiety elementu
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		string	$sLabel						etykieta elementu
   * @param		bool		$bRequired				czy pole wymagane
	 * @return	string	$sHtml	kod HTML etykiety
	 */
	function GetLabel($sName, $sLabel, $bRequired=true, $iMaxlength=0, $sData='', $aAttribs = []) {

        $html = '';
        foreach ($aAttribs as $key => $value) {
            $html .= ' '.$key.'="'.$value.'" ';
        }


        $sHtml = '<label for="'.$sName.'" '.$html.' >'.stripslashes($sLabel).
					 ($iMaxlength > 0 ? ' (<span id="textarea_'.str_replace(array('[', ']'), array('_','_'), $sName).'">'.
    				($iMaxlength - utf8_strlen($sData)).'</span>)' : '').
						'&nbsp;<span id="'.$sName.'_label" class="gwiazdka" style="display: '.($bRequired ? 'inline' : 'none').';">*</span></label>';


        return $sHtml;
	} // end of GetLabel() function
	

	/**
	 * Metoda zwraca kod HTML elementu
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sType						typ elementu
	 * @return	string	$sHtml	kod HTML
	 */
	function GetText($sName, $sValue='', $aAttribs=array(), $sType='text') {
		global $aConfig;
		$bShowClear = true;
		$sValue = Common::encodeString($sValue);
		
		if ($sType == 'date') {
			static $iCalendars = 0;
			$aAttribs['readonly'] = 1;
			$aAttribs['style'] .= 'cursor: pointer;';
			$aAttribs['onclick'] = 'javascript:_c1Z(agC['.$iCalendars.']);';
		}
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		if (isset($aAttribs['no_clear'])) {
			$bShowClear = false;
			unset($aAttribs['no_clear']);
		}
		
		if ($sType == 'date' && 
				!preg_match($aConfig['class']['form']['date']['pcre'], $sValue) && !$aAttribs['clear']) {
			$sValue = date('d-m-Y');
		}
		if ($sValue == '00-00-0000') {
			$sValue = '';
		}
		
		if($sType=='isbn') {
			//$aAttribs['onblur']='validateISBNFromField(this.value);';	
			}
		
		$sHtml = '<input type="text" name="'.$sName.'" value="'.$sValue.'"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		$sHtml .= ' />';
		
		if ($sType == 'date') {						
			$sHtml = '<table border="0" cellspacing="0" cellpadding="0"><tr><td style="padding: 0px; border: none;">'.$sHtml.'</td><td style="padding: 2px 0px 0px 0px; border: none;">&nbsp;';
    	$sHtml .= '<script type="text/javascript" language="JavaScript">
				<!--
					// dni tygodnia
					GC_APPEARANCE.weekdays = ["'.$aConfig['lang']['calendar']['weekdays'][1].'",'.
																		'"'.$aConfig['lang']['calendar']['weekdays'][2].'",'.
																		'"'.$aConfig['lang']['calendar']['weekdays'][3].'",'.
																		'"'.$aConfig['lang']['calendar']['weekdays'][4].'",'.
																		'"'.$aConfig['lang']['calendar']['weekdays'][5].'",'.
																		'"'.$aConfig['lang']['calendar']['weekdays'][6].'",'.
																		'"'.$aConfig['lang']['calendar']['weekdays'][7].'"];
					// miesiace
					GC_APPEARANCE.longmonth = ["'.$aConfig['lang']['months'][1].'",'.
																		'"'.$aConfig['lang']['months'][2].'",'.
																		'"'.$aConfig['lang']['months'][3].'",'.
																		'"'.$aConfig['lang']['months'][4].'",'.
																		'"'.$aConfig['lang']['months'][5].'",'.
																		'"'.$aConfig['lang']['months'][6].'",'.
																		'"'.$aConfig['lang']['months'][7].'",'.
																		'"'.$aConfig['lang']['months'][8].'",'.
																		'"'.$aConfig['lang']['months'][9].'",'.
																		'"'.$aConfig['lang']['months'][10].'",'.
																		'"'.$aConfig['lang']['months'][11].'",'.
																		'"'.$aConfig['lang']['months'][12].'"];
					GC_APPEARANCE.messages.Warning = "'.$aConfig['lang']['calendar']['Warning'].'";
					GC_APPEARANCE.messages.AltPrevYear = "'.$aConfig['lang']['calendar']['AltPrevYear'].'";
					GC_APPEARANCE.messages.AltNextYear = "'.$aConfig['lang']['calendar']['AltNextYear'].'";
					GC_APPEARANCE.messages.AltPrevMonth = "'.$aConfig['lang']['calendar']['AltPrevMonth'].'";
					GC_APPEARANCE.messages.AltNextMonth = "'.$aConfig['lang']['calendar']['AltNextMonth'].'";
					var GC_SET_'.$iCalendars.' = {
						\'dataArea\': \''.$aAttribs['id'].'\',
						\'appearance\': GC_APPEARANCE,
						\'dateFormat\' : \''.$aConfig['common']['cal_date_format'].'\'
					}
					new gCalendar(GC_SET_'.$iCalendars.');
					'.($aAttribs['clear'] && $sValue==''?'
					document.getElementById("'.$aAttribs['id'].'").value="";
					':'').'
					
				//-->
			</script>';
			if ($bShowClear) {
				$sHtml .= '&nbsp;<a href="javascript: void(0);" onclick="GetObject(\''.$aAttribs['id'].'\').value=\'\';" title="'.$aConfig['lang']['clendar']['clear_date'].'"><img src="gfx/icons/clear_ico.gif" width="16" height="17" alt="'.$aConfig['lang']['calendar']['clear_date'].'" border="0" align="absmiddle"></a>';
			}
			$sHtml .= '</td></tr></table>';
			$iCalendars++;
		}
		elseif($sType=='isbn') {
			$sHtml .= '
			<span id="'.$sName.'_spanOK" style="color:#0c0; display:none;">ISBN poprawny</span>
			<script type="text/javascript" language="JavaScript">
			function validateISBNFromField(val){
				val=val.replace(/-/g, "");
				if(val.length==13) {
					//walidacja nowego isbn
					var sum=0;
					for(var i=0;i<12;++i) {
						var mult=	i%2==1?3:1;
						sum+=mult*parseInt( val[i]);
						
						}
					var dif=10-sum%10;
					if((dif==val[12] && dif<10) || (dif==10 && val[12]==\'X\'))	{
						//czynnosci jeżeli jest poprawny
						document.getElementById("'.$sName.'_spanOK").style.display="";
						} 
					else {
						alert("Numer ISBN nie jest poprawny! / КОД НЕВЕРЕН. ПОПРОБУЙТЕ ЕЩЁ РАЗ");
						document.getElementById("'.$sName.'_spanOK").style.display="none";
						} 
							
					}
				else if(val.length==10) {
					//walidacja starego isbn
					var vag=10;
					var sum=0;
					for(var i=0;i<9;++i) {
						sum+=vag*parseInt( val[i]);
						--vag;
						} 
					var dif=11-(sum%11);
					dif=dif==11?0:dif;
					if((dif==val[9] && dif<10) || (dif==10 && val[9]==\'X\'))	{
						//czynnosci jeżeli jest poprawny
						document.getElementById("'.$sName.'_spanOK").style.display="";
						;
						} 
					else {
						alert("Numer ISBN nie jest poprawny! / КОД НЕВЕРЕН. ПОПРОБУЙТЕ ЕЩЁ РАЗ");
						document.getElementById("'.$sName.'_spanOK").style.display="none";
						}	
					}
				else {
						alert("Numer ISBN nie jest poprawny! / КОД НЕВЕРЕН. ПОПРОБУЙТЕ ЕЩЁ РАЗ");
						document.getElementById("'.$sName.'_spanOK").style.display="none";
						}	
						
				}
			</script>';
			}
		
		return $sHtml;
	} // end GetText()
	
	
	/**
	 * Metoda zwraca kod HTML elementu typu File
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		array   $aAttribs					atrybuty elementu
	 * @return	string	$sHtml	kod HTML
	 */
	function GetFile($sName, $aAttribs=array()) {
		global $aConfig;
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		$sHtml = '<input type="file" name="'.$sName.'"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		$sHtml .= ' />';
		
		return $sHtml;
	} // end GetFile()
	
	
	/**
	 * Metoda zwraca kod HTML elementu
	 * 
   * @param		string  $sName						nazwa elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
	 * @return	string	$sHtml	kod HTML
	 */
	function GetWYSIWYG($sName, $sValue='', $aAttribs=array()) {
		global $aConfig;
		
		include_once("Form/Editor/WYSIWYGEditor.class.php");
		$oEditor = new WYSIWYGEditor($sName, $aAttribs);
		return Form::GetTextArea($sName, $sValue).$oEditor->GetHTML();
	} // end GetWYSIWYG()
	
	
	/**
	 * Metoda zwraca kod HTML checkboxa
	 * 
   * @param		string  $sName						nazwa elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
	 * @return	string	$sHtml	kod HTML
	 */
	function GetTextArea($sName, $sValue='', $aAttribs=array()) {
    
    $sValue = Common::encodeString($sValue);
    
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		$sHtml .= '<textarea name="'.$sName.'"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		$sHtml .= '>'.(!empty($sValue)?$sValue:'');
		$sHtml .= '</textarea>';
		
		return $sHtml;
	} // end GetTextArea()
	

	/**
	 * Metoda zwraca kod HTML elementu select
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		array		$aOptions					atrybuty opcji elementu
	 * @param		mixed		$mSelected				list wartosci opcji ktore maja byc zaznaczone
	 * @return	string	$sHtml	kod HTML
	 */
	function GetSelect($sName, $aAttribs=array(), $aOptions=array(), $mSelected='') {
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		if (is_array($mSelected)) {
			$mSelected = array_flip($mSelected);
		}
		else {
			$mSelected = ''.$mSelected;
		}

		$sHtml .= '<select name="'.$sName.(isset($aAttribs['multiple'])?'[]':'').'"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		
		$sHtml .= '>';
		if (!empty($aOptions)) {
			foreach ($aOptions as $mKey => $aOAttribs) {
				if ($aOAttribs['value'] === 'OPTGROUP') {
					$sHtml .= '<optgroup label="';
					$sHtml .= ($aOAttribs['name'] != '' ? $aOAttribs['name'] : $aOAttribs['label']).'">';
				}
				elseif ($aOAttribs['value'] === '/OPTGROUP') {
					$sHtml .= '</optgroup>';
				}
				else {
					$sHtml .= '<option';
					foreach ($aOAttribs as $key => $value) {
						if ($key != 'name' && $key != 'label') {
							$sHtml .= ' '.$key.'="'.$value.'"';
							if ($key == 'value') {									
								if ($mSelected != '') {
									if (is_array($mSelected)) {
										if (isset($mSelected[$value])) {
											$sHtml .= ' selected="selected"';
										}
									}
									else {
										if ($value == $mSelected) {
											$sHtml .= ' selected="selected"';
										}
									}
								}
							}
	        	}
					}
					$sHtml .= '>'.stripslashes($aOAttribs['name'] != '' ? $aOAttribs['name'] : $aOAttribs['label']).'</option>';
				}
			}
		}
		$sHtml .= '</select>';
		
		return $sHtml;
	} // end of GetSelect() method
	
	
	/**
	 * Metoda zwraca kod HTML elementu password
	 * 
   * @param		string  $sName						nazwa elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
	 * @return	string	$sHtml	kod HTML
	 */
	function GetPassword($sName, $sValue='', $aAttribs=array()) {
		global $aConfig;
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		$sHtml = '<input type="password" name="'.$sName.'" value="'.$sValue.'"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		$sHtml .= ' />';
		
		return $sHtml;
	} // end GetPassword()
	
	
	/**
	 * Metoda zwraca kod HTML przycisku typu input przycisk
	 * @param		string	$sValue						wartosc elementu
	 * @return	string	$sHtml	kod HTML
	 */
	function GetInputButton($sName, $sValue='', $aAttribs=array(), $sType='submit') {
		
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		$sHtml = '<input type="'.$sType.'" name="'.$sName.'" value="'.$sValue.'"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		$sHtml .= ' />';
		return $sHtml;
	} // end GetInputButton()
  
  
	/**
	 * Metoda zwraca kod HTML checkboxa
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bChecked					czy domyslnie ma byc zaznaczony
	 * @return	string	$sHtml	kod HTML
	 */
	function GetCheckBox($sName, $aAttribs=array(), $sErrMsg='', $bChecked=false) {
    
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		if (!isset($aAttribs['value'])) {
			$aAttribs['value'] = '1';
		}
		
    $sHtml = '<input type="checkbox" name="'.$sName.'" style="border:none;"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		$sHtml .= ($bChecked ? ' checked="checked"' : '').' />';
		
		return $sHtml;
	} // end GetCheckBox()
	
	
	/**
	 * Metoda zwraca kod HTML zestawu elementow typu checkbox
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		array		$aBoxes						atrybuty poszczegolnych checkboxow elementu
	 * @param		mixed		$aChecked					list checkboxow ktore maja byc zaznaczone
	 * @param		bool		$bReturnAsTable		true: zwroc jako tabele; false: zwroc jako liste <ul>
	 * @return	string	$sHtml	kod HTML
	 */
	function GetCheckBoxSet($sName, $aBoxes=array(), $aChecked=array(), $bReturnAsTable=true) {
		global $aConfig;
		
		if (is_array($aChecked) && !empty($aChecked)) {
			$aChecked = array_flip($aChecked);
		}
		
		if ($bReturnAsTable) {
			$sPrefix = '<table cellspacing="0" cellpadding="0" border="0" class="checkBoxList">
			<colgroup>
				<col width="28" />
				<col />
			</colgroup>
			<tr>
				<th>';
			$sPreSpacer = '</th>
			<td>';
			$sPostSpacer = '</td>
			</tr>
			<tr>
				<th>';
			$sPostfix = '</td>
			</tr>
			</table>';
		}
		else {
			$sPrefix = '<ul class="checkBoxList">
			<li>';
			$sPreSpacer = '&nbsp;';
			$sPostSpacer = '</li>
			<li>';
			$sPostfix = '</li>
			</ul>';
		}
		
		if (!empty($aBoxes)) {
			$sHtml = $sPrefix;
			$iI = 1;
			$iBoxes = count($aBoxes);
			foreach ($aBoxes as $mKey => $aAttribs) {
				$sChLabel = stripslashes($aAttribs['label']);
				unset($aAttribs['label']);
				if($aAttribs['value'] == 'GROUPHEADER') {
					$sHtml .= '<span>'.$sChLabel.'</span>';
				} else {	
					if (!isset($aAttribs['id'])) {
						$aAttribs['id'] = $sName.'['.$aAttribs['value'].']';
					}
					$sHtml .= Form::GetCheckBox($sName.'['.$aAttribs['value'].']', $aAttribs, '', isset($aChecked[$aAttribs['value']]));
					$sHtml .= $sPreSpacer;
					$sHtml .= '<label style="cursor: pointer;" for="'.$aAttribs['id'].'">'.$sChLabel.'</label>';
				}
				if ($iI++ < $iBoxes) {
					$sHtml .= $sPostSpacer;
				}
			}
			$sHtml .= $sPostfix;
		}
		
		return $sHtml;
	} // end of GetCheckBoxSet() method
	
	
	function getCheckTreeSubItems($sName, $aOptions=array(), $mSelected=''){
	global $aConfig;
	$sHtml = '';
		if (!empty($aOptions)) {
			foreach ($aOptions as $mKey => $aOAttribs) {
				$sHtml .= '<li><input type="checkbox" name="'.$sName.'" value="'.$aOAttribs['value'].'" ';
				if ($mSelected != '') {
					if (is_array($mSelected)) {
						if (isset($mSelected[$aOAttribs['value']])) {
							$sHtml .= ' checked="checked"';
						}
					}
					else {
						if ($aOAttribs['value'] == $mSelected) {
							$sHtml .= ' checked="checked"';
						}
					}
				}
				$sHtml .= '/>';
				$sHtml .= '<label>'.$aOAttribs['label'].'</label>';
				
				if(isset($aOAttribs['items'])){
					$sHtml .= '<ul>';
					$sHtml .= Form::getCheckTreeSubItems($sName, $aOAttribs['items'], $mSelected);
					$sHtml .= '</ul>';
				}
				$sHtml .= '</li>';
			}
		}
		return $sHtml;
	}
	
	/**
   * Metoda generuje drzewo z checkboksami do zaznaczenia
   * 
   * @global type $aConfig
   * @param type $sName
   * @param type $aAttribs - może zawierać klucz 'free_choice', oznaczający dowolne wybieranie elementów, bez przodków
   * @param type $aOptions
   * @param type $mSelected
   * @return string
   */
	function GetCheckTree($sName, $aAttribs=array(), $aOptions=array(), $mSelected='') {
	global $aConfig;
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		if (is_array($mSelected)) {
			$mSelected = array_flip($mSelected);
		}
		else {
			$mSelected = ''.$mSelected;
		}
    if ($aAttribs['free_choice'] == true) {
      $sHtml='
      <script>
        $(document).ready(function(){
          $("ul.tree").checkboxTree({
              initializeChecked: "expanded",
              initializeUnchecked: "collapsed",
              onCheck: {
                  ancestors: "none",
                  descendants: "none"
              },
              onUncheck: {
                ancestors: "none",
                descendants: "none"
            }
          });
        });
      </script>';
    } else {
      $sHtml='
      <script>
        $(document).ready(function(){
          $("ul.tree").checkboxTree({
              initializeChecked: "expanded",
              initializeUnchecked: "collapsed",
              onCheck: {
                  descendants: "uncheck"
              },
              onUncheck: {
                  descendants: "uncheck"
              }
          });
        });
      </script>';
    }


		$sHtml .= '<ul class="tree" '.(isset($aAttribs['style'])?$aAttribs['style']:'style="margin-left: 15px;"').'>';
		$sHtml .= Form::getCheckTreeSubItems($sName, $aOptions, $mSelected);

		$sHtml .= '</ul>';
	//	dump($sHtml);
		return $sHtml;
	} // end of GetCheckBoxSet() method
	
	/**
	 * Metoda zwraca kod HTML zestawu elementow typu checkbox
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		array		$aBoxes						atrybuty poszczegolnych checkboxow elementu
	 * @param		mixed		$aChecked					list checkboxow ktore maja byc zaznaczone
	 * @param		bool		$bReturnAsTable		true: zwroc jako tabele; false: zwroc jako liste <ul>
	 * @return	string	$sHtml	kod HTML
	 */
	function GetCheckBoxModulesSet($sName, $aBoxes=array(), $aData=array(), $aAttribs2=array(), $aAttribs3=array()) {
		global $aConfig;
		$aSet = array();
		
		if (is_array($aData[0]) && !empty($aData[0])) {
			$aData[0] = array_flip($aData[0]);
		}
		
		if (!empty($aBoxes)) {
			$iI = 1;
			$iBoxes = count($aBoxes);
			foreach ($aBoxes as $mKey => $aAttribs) {
				$sChLabel = stripslashes($aAttribs['label']);
				unset($aAttribs['label']);
				
				if (!isset($aAttribs['id'])) {
					$aAttribs['id'] = $sName.'['.$aAttribs['value'].']';
				}
				if (isset($aData[0][$aAttribs['value']])) {
					$aAttribs2['class'] = 'text';
					$aAttribs3['class'] = 'text';
					unset($aAttribs2['disabled']);
					unset($aAttribs3['disabled']);
				}
				else {
					$aAttribs2['class'] = 'textDisabled';
					$aAttribs2['disabled'] = 'disabled';
					$aAttribs3['class'] = 'textDisabled';
					$aAttribs3['disabled'] = 'disabled';
				}
				if ($aAttribs['extra'] == '1') {
					// modul kacelaris - 5 pol tekstowych - modul Kancelaris z kodem
					$aAttribs['onclick'] = 'toggleLM2Inputs(\''.$sName.'\', '.$aAttribs['value'].');';
					$aSet[] = array(
						'<label id="l_'.$sName.'['.$aAttribs['value'].']" for="'.$aAttribs['id'].'">'.$sChLabel.'</label>',
						Form::GetCheckBox($sName.'['.$aAttribs['value'].']', $aAttribs, '', isset($aData[0][$aAttribs['value']])),
						Form::GetText($sName.'_2['.$aAttribs['value'].']', isset($aData[1][$aAttribs['value']]) ? $aData[1][$aAttribs['value']] : '', $aAttribs3),
						Form::GetText($sName.'_3['.$aAttribs['value'].']', isset($aData[2][$aAttribs['value']]) ? $aData[2][$aAttribs['value']] : '', $aAttribs3),
						Form::GetText($sName.'_4['.$aAttribs['value'].']', isset($aData[3][$aAttribs['value']]) ? $aData[3][$aAttribs['value']] : '', $aAttribs3),
						Form::GetText($sName.'_5['.$aAttribs['value'].']', isset($aData[4][$aAttribs['value']]) ? $aData[4][$aAttribs['value']] : '', $aAttribs3),
						Form::GetText($sName.'_6['.$aAttribs['value'].']', isset($aData[5][$aAttribs['value']]) ? $aData[5][$aAttribs['value']] : '', $aAttribs2)
					);
				}
				else {
					$aAttribs['onclick'] = 'toggleLMInputs(\''.$sName.'\', '.$aAttribs['value'].');';
					$aSet[] = array(
						'<label id="l_'.$sName.'['.$aAttribs['value'].']" for="'.$aAttribs['id'].'">'.$sChLabel.'</label>',
						Form::GetCheckBox($sName.'['.$aAttribs['value'].']', $aAttribs, '', isset($aData[0][$aAttribs['value']])),
						Form::GetText($sName.'_2['.$aAttribs['value'].']', isset($aData[1][$aAttribs['value']]) ? $aData[1][$aAttribs['value']] : 1, $aAttribs2),
						Form::GetText($sName.'_3['.$aAttribs['value'].']', isset($aData[2][$aAttribs['value']]) ? $aData[2][$aAttribs['value']] : 12, $aAttribs2)
					);
				}
			}
		}
		
		return $aSet;
	} // end of GetCheckBoxModulesSet() method
	
	
	/**
	 * Metoda zwraca kod HTML zestawu elementow typu radio
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		array		$aRadios					atrybuty poszczegolnych elementow radio elementu
	 * @param		mixed		$mChecked					list checkboxow ktore maja byc zaznaczone
	 * @param		bool		$bReturnAsTable		true: zwroc jako tabele; false: zwroc jako liste <ul>
	 * @return	string	$sHtml	kod HTML
	 */
	function GetRadioSet($sName, $aRadios=array(), $mChecked='', $bReturnAsTable=true) {
		global $aConfig;
		
		if ($bReturnAsTable) {
			$sPrefix = '<table cellspacing="0" cellpadding="0" border="0" class="radiosList">
			<colgroup>
				<col width="28" />
				<col />
			</colgroup>
			<tr>
				<th>';
			$sPreSpacer = '</th>
			<td>';
			$sPostSpacer = '</td>
			</tr>
			<tr>
				<th>';
			$sPostfix = '</td>
			</tr>
			</table>';
		}
		else {
			$sPrefix = '<ul class="radiosList">
			<li>';
			$sPreSpacer = '&nbsp;';
			$sPostSpacer = '</li>
			<li>';
			$sPostfix = '</li>
			</ul>';
		}
		
		if (!empty($aRadios)) {
			$sHtml = $sPrefix;
			$iI = 1;
			$iRadios = count($aRadios);
			foreach ($aRadios as $mKey => $aAttribs) {
				$sRLabel = stripslashes($aAttribs['label']);
				unset($aAttribs['label']);
				if (!isset($aAttribs['id'])) {
					$aAttribs['id'] = $sName.'['.$aAttribs['value'].']';
				}
				$sHtml .= '<input type="radio" name="'.$sName.'" style="border:none;"';
				foreach ($aAttribs as $key => $value) {
					if ($key != 'additional_row_html') {
						if ($key == 'value' && $value == $mChecked) {
							$sHtml .= ' checked="checked"';
						}
						$sHtml .= ' '.$key.'="'.$value.'"';
					}

				}
				$sHtml .= ' />';
				$sHtml .= $sPreSpacer;
				$sHtml .= '<label style="cursor: pointer;" for="'.$aAttribs['id'].'">'.$sRLabel.'</label>';
				$sHtml .= $aAttribs['additional_row_html'];
				if ($iI++ < $iRadios) {
					$sHtml .= $sPostSpacer;
				}
			}
			$sHtml .= $sPostfix;
		}
 	
		return $sHtml;
	} // end of GetRadioSet() method
	
		/**
	 * Metoda zwraca kod HTML pojedynczego radio
	 * 
   * @param		string  $sName						nazwa elementu
   * @param		array   $aAttribs					atrybuty elementu
   * @param		string	$sErrMsg					komunikat bledu
   * @param		bool		$bChecked					czy domyslnie ma byc zaznaczony
	 * @return	string	$sHtml	kod HTML
	 */
	function GetRadio($sName, $aAttribs=array(), $sErrMsg='', $bChecked=false) {
    
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
		if (!isset($aAttribs['value'])) {
			$aAttribs['value'] = '1';
		}
		
    $sHtml = '<input type="radio" name="'.$sName.'" style="border:none;"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		$sHtml .= ($bChecked ? ' checked="checked"' : '').' />';
		
		return $sHtml;
	} // end GetRadio()
	
	/**
	 * Metoda zwraca kod HTML checkboxa
   * @param		string  $sName						nazwa elementu
	 * @param		string	$sValue						wartosc elementu
   * @param		array   $aAttribs					atrybuty elementu
	 * @return	string	$sHtml	kod HTML
	 */
	function GetHidden($sName, $sValue='', $aAttribs=array()) {
    
    $sValue = Common::encodeString($sValue);
    
		if (!isset($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
    $sHtml = '<input type="hidden" name="'.$sName.'" value="'.$sValue.'"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		$sHtml .= ' />';
                
		return $sHtml;
	} // end GetHidden()
	
  
  function GetInfo($sValue) {
    $iId = intval(microtime(true))+'_'.rand(0,99999);
    
    $sHtml .= ' <a href="javascript:void(0);" style="color: black; text-decoration: none; font-size: 14px; font-weight: bold;" id="'.$iId.'" title="'.addslashes($sValue).'">?</a>';
    $sHtml .= '
     <script type="text/javascript">
      $("#'.$iId.'").tooltip();
     </script>
    ';
    
    return $sHtml;
  }
	
  /**
   * Metoda zwraca naglowek formularza
   *
   * @return	string	$sHtml - naglowek formularza
   */
  function GetHeader($sName, $aAttribs=array()) {
	  if (!isset($aAttribs['action']) || empty($aAttribs['action'])) {
      $aAttribs['action'] = phpSelf();
    }
    if (!isset($aAttribs['method']) || 
    	(strtolower($aAttribs['method']) != 'post' && strtolower($aAttribs['method']) != 'get')) {
			$aAttribs['method'] = 'post';
		}
    if (isset($aAttribs['enctype']) &&
    	strtolower($aAttribs['enctype']) != 'application/x-www-form-urlencoded' &&
    	strtolower($aAttribs['enctype']) != 'multipart/form-data') {
			unset($aAttribs['enctype']);
		}
		if (!isset($aAttribs['id']) || empty($aAttribs['id'])) {
			$aAttribs['id'] = $sName;
		}
		
	  $sHtml = '<form name="'.$sName.'"';
		foreach ($aAttribs as $key => $value) {
			$sHtml .= ' '.$key.'="'.$value.'"';
		}
		$sHtml .= '>';
		
    return $sHtml;
  } // end of GetHeader() metod

  /**
   * Metoda zwraca stopke formularza
   *
   * @return	string	$sHtml - stopka formularza
   */
  function GetFooter() {
    return '</form>';
  } // end of GetFooter() method
} // end of class Form
?>