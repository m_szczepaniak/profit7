<?php
class Invoice{
	public $sTemplatePath = '';
	public $iWebsiteId;
	public $sInvoicePostfix;


	function __construct() {
		global $pDbMgr;

		if (empty($pDbMgr) && !is_object($pDbMgr)) {
			include_once('DatabaseManager.class.php');
			$pDbMgr = new DatabaseManager();
		}
	}
	/**
	 * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type
	 */
	function getWebsiteId($iOId) {
		global $aConfig, $pDbMgr;

		if ($this->iWebsiteId <= 0 ) {
			$sSql = "SELECT website_id FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE id=".$iOId;
			$this->iWebsiteId = $pDbMgr->GetOne('profit24', $sSql);
		}
		return $this->iWebsiteId;
	}// end of getWebsiteId() method


	/**
	 * Metoda zwaca posf
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type
	 */
	function getInvoiceWebsiteSymbol($iOId) {
		global $aConfig;

		return $aConfig['invoice_website_symbol_'.$this->getWebsiteId($iOId)];
	}// end of getInvoiceWebsiteSymbol() method


	/**
	 * Ustawia w zamówieniu flagę "faktura gotowa"
	 * @param int $iId - id zamówienia
	 * @return bool - success
	 */
	function setInvoiceReady($iId){
		global $aConfig, $pDbMgr;

		$aUpd = array(
			'invoice_ready' => '1'
		);
		if ($pDbMgr->Update('profit24',
                       $aConfig['tabls']['prefix']."orders",
											 $aUpd,
											 "id = ".$iId) === false) {
			return false;
		}
		return true;
	} // end of setInvoiceReady() method


    /**
     * Metoda pobiera plik PDF, jeśli nie istnieje to generuje i zapisuje
     *
     * @global array $aConfig
     * @param int $iOrderId
     * @param array $aOrder
     */
    public function getInvoicePDF($iOrderId, $aOrder, $cReturnType = 1) {
        global $aConfig;

        if($aOrder['invoice_date'] != '' && intval($aOrder['invoice_id']) > 0){
            if ($aOrder['invoice_ready'] == '0') {
                $this->setInvoiceReady($iOrderId);
            }
            if ($cReturnType == 1) {
                $_GET['hideHeader'] = '1';
            }
            $iWebsiteId = $this->getWebsiteId($iOrderId);

            $fName = 'faktura'.str_replace("/","_",$aOrder['invoice_id']).'.pdf';
            $sPath = $this->getInvoiceNumberPath($iWebsiteId, $iOrderId);
            $sFile = $aConfig['common']['base_path'].$aConfig['common']['invoice_dir'].$sPath.'/'.$fName;
            // mamy pdf na dysku serwera, wysyłamy
            if($this->invoiceExists($iOrderId, false) && $cReturnType != 3){
                if ($cReturnType == 2) {
                    return $sFile;
                } else {
                    $sContent =& $this->getInvoiceData($iOrderId, false);
                    if ($sContent != '') {
                        header('Content-Description: File Transfer');
                        //header('Content-Type: application/force-download');
                        header('Content-Type: application/octet-stream', false);
                        //header('Content-Type: application/download', false);
                        header('Content-Type: application/pdf', false);
                        header('Content-Disposition: inline; filename="'.$fName.'"');
                        header('Content-Transfer-Encoding: binary');
                        //header('Content-Length: '.count($sContent));
                        echo $sContent;
                    }
                }
            }
            // ,musimy wygenerować pdf z fakturą
            else {
                include_once($aConfig['common']['client_base_path'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
                include_once($aConfig['common']['client_base_path'].'/omniaCMS/modules/m_zamowienia/lang/invoice_'.$aConfig['default']['language'].'.php');
                $aLang =& $aConfig['lang']['m_zamowienia'];

                require_once('tcpdf/config/lang/pl.php');
                require_once('tcpdf/tcpdf.php');

                // create new PDF document
                $pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

//        $info = array(
//            'Name' => 'profit24.pl',
//            'Location' => 'Office',
//            'Reason' => 'Testing TCPDF',
//            'ContactInfo' => 'http://www.tcpdf.org',
//        );
//
//        $sCertificate = $aConfig['common']['client_base_path'].'../';
//        //$pdf->setSignature($sCertificate, $sCertificate, 'profit24.pl', '', 2);

                // set document information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('profit24');
                $pdf->SetTitle($aLang['title']);
                $pdf->SetSubject($aLang['title']);
                $pdf->SetKeywords('');
                $pdf->setPrintHeader(false);

                // set default header data
                //$pdf->SetHeaderData('logo.gif', PDF_HEADER_LOGO_WIDTH, $aLang['logo_title'], $aLang['logo_string']);

                // set header and footer fonts
                //$pdf->setHeaderFont(Array('freesans', '', PDF_FONT_SIZE_MAIN));
                $pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

                // set default monospaced font
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                //set margins
                $pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
                //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

                //set auto page breaks
                $pdf->SetAutoPageBreak(TRUE, 10);

                //set image scale factor
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                //set some language-dependent strings
                $l = array();
                $pdf->setLanguageArray($l);

                $sHtml = $this->getInvoiceHtml($iOrderId);


                // set font
                $pdf->SetFont('freesans', '', 10);

                // add a page
                $pdf->AddPage();
                $pdf->writeHTML($sHtml, true, false, false, false, '');
                // druga faktura
                if($aOrder['second_invoice'] == '1'){
                    $sHtml2 =$this->getInvoiceHtml($iOrderId,true);
                    if(!empty($sHtml2)){
                        // add a page
                        $pdf->AddPage();
                        $pdf->writeHTML($sHtml2, true, false, false, false, '');
                    }
                }
                //Close and output PDF document
                // tylko jeśli profit24.pl
                $private_key = 'file://'.realpath($aConfig['common']['client_base_path'].'../cert/professional_id/'.$aOrder['website_id'].'/private.crt');
                $signing_cert = 'file://'.realpath($aConfig['common']['client_base_path'].'../cert/professional_id/'.$aOrder['website_id'].'/certyficate.pem');
                $l3_cert = realpath($aConfig['common']['client_base_path'].'../cert/'.$aOrder['website_id'].'/professional_id/L3-CA.pem');
                if (file_exists($private_key) && file_exists($signing_cert)) {
//          $pdf->setSignature($signing_cert, $private_key, '', $l3_cert, 2, array(), 'A');
                }

                //$pdf->Output($fName, 'D');
                //$pdf->Output($sFile, 'F');
                $sBuffStr = $pdf->Output($sFile, 'S');

                if ($cReturnType == 3) {

                    return $sBuffStr;
                }

                $sBasePathName = $this->saveInvoiceData($iOrderId, $fName, $sBuffStr, false, 'NOW()');
                if ($cReturnType == 2) {
                    return $sBasePathName;
                } else {
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/force-download');
                    header('Content-Type: application/octet-stream', false);
                    header('Content-Type: application/download', false);
                    header('Content-Type: application/pdf', false);
                    header('Content-Disposition: attachment; filename="'.$fName.'"');
                    header('Content-Transfer-Encoding: binary');
                    echo $sBuffStr;
                }
            }
        }
    }// end of getInvoicePDF() method

  /**
   * Metoda pobiera plik PDF, jeśli nie istnieje to generuje i zapisuje
   *
   * @global array $aConfig
   * @param int $iOrderId
   * @param array $aOrder
   */
  public function getInvoiceProforma($iOrderId, $aOrder, $cReturnType = 1) {
    global $aConfig;

      $sFile = 'faktura_proforma'.str_replace("/","_",$aOrder['order_number']).'.pdf';

	  include_once($aConfig['common']['client_base_path'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
	  include_once($aConfig['common']['client_base_path'].'/omniaCMS/modules/m_zamowienia/lang/invoice_'.$aConfig['default']['language'].'.php');
	  $aLang =& $aConfig['lang']['m_zamowienia'];

      require_once('OLD_tcpdf/config/lang/pl.php');
      require_once('OLD_tcpdf/tcpdf.php');

      // create new PDF document
      $pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

      // set document information
      $pdf->SetCreator(PDF_CREATOR);
      $pdf->SetAuthor('profit24');
      $pdf->SetTitle($aLang['title']);
      $pdf->SetSubject($aLang['title']);
      $pdf->SetKeywords('');
      $pdf->setPrintHeader(false);

      // set default header data
      //$pdf->SetHeaderData('logo.gif', PDF_HEADER_LOGO_WIDTH, $aLang['logo_title'], $aLang['logo_string']);

      // set header and footer fonts
      //$pdf->setHeaderFont(Array('freesans', '', PDF_FONT_SIZE_MAIN));
      $pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

      // set default monospaced font
      $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

      //set margins
      $pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
      //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
      $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

      //set auto page breaks
      $pdf->SetAutoPageBreak(TRUE, 10);

      //set image scale factor
      $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

      //set some language-dependent strings
      $pdf->setLanguageArray($l);

      $sHtml =$this->getInvoiceHtml($iOrderId,false,true);

      // set font
      $pdf->SetFont('freesans', '', 10);

      // add a page
      $pdf->AddPage();
      $pdf->writeHTML($sHtml, true, false, false, false, '');
      // druga faktura
      if($aOrder['second_invoice'] == '1'){
          $sHtml2 =$this->getInvoiceHtml($iOrderId,true,true);
          if(!empty($sHtml2)){
              // add a page
              $pdf->AddPage();
              $pdf->writeHTML($sHtml2, true, false, false, false, '');
          }
      }

    $sBuffStr = $pdf->Output($sFile);
  }// end of getInvoicePDF() method

	/**
   * Metoda pobiera użytkownika na podstwie loginu
   *
   * @global \DatabaseManager $pDbMgr
   * @param string $sLogin
   * @return array
   */
  private function getUserByLogin($sLogin) {
    global $pDbMgr;

    $sSql = 'SELECT name, surname FROM users WHERE login = "'.$sLogin.'"';
    return $pDbMgr->GetRow('profit24', $sSql);
  }


	/**
	 * Generuje html faktury
	 * @param int $iId - id zamówienia
	 * @return string - html
	 */
	function getInvoiceHtml($iId,$bSecondInvoice=false,$bProForma=false){
	global $aConfig,$pSmarty, $pDbMgr;

		$aModule = array();
		$aModule['lang'] =& $aConfig['lang']['m_zamowienia_invoice'];

		include_once('OrderItemRecount.class.php');
		$oOrderItemRecount = new OrderItemRecount();
		$aOrderData = $oOrderItemRecount->getOrderPricesDetail($iId, $bSecondInvoice, '', true);
    $aModule['orders_user_workflow'] = $this->getUserByLogin($aOrderData['order']['status_3_update_by']);//$this->_getOrdersToUsers($iId, 5);
		$aModule['order'] = $aOrderData['order'];
		$aModule['invoice_items'] = $aOrderData['order_items'];
		$aModule['vat'] = $aOrderData['order']['vat'];
		$aModule['order']['vat'] = $aModule['order']['order_vat'];


		if($bProForma){
			$aModule['pro_forma'] = '1';
			if($bSecondInvoice){
				$aModule['invoice_number'] = $aModule['order']['order_number'].'A';
			} else {
				$aModule['invoice_number'] = $aModule['order']['order_number'];
			}
		} else {
			if($bSecondInvoice){
				$aModule['invoice_number'] = $aModule['order']['invoice2_id'];
			} else {
				$aModule['invoice_number'] = $aModule['order']['invoice_id'];
			}
		}

    //kod kreskowy
    include_once 'barcode/barcode.php';

    $sCode = str_pad($aModule['invoice_number'], 13, '0', STR_PAD_LEFT);
    $im     = imagecreatetruecolor(300, 100);
    $black  = ImageColorAllocate($im,0x00,0x00,0x00);
    $white  = ImageColorAllocate($im,0xff,0xff,0xff);
    imagefilledrectangle($im, 0, 0, 300, 100, $white);
    $data = Barcode::gd($im, $black, 150, 50, 0, "code128", $sCode, 2, 50);

    ob_start();
    imagepng($im);
    imagedestroy( $im );
    $imagedata = ob_get_clean();

    $sBarCodePath = $_SERVER['DOCUMENT_ROOT'].'/tmp/barcode_'.$aModule['invoice_number'].'.png';
    file_put_contents($sBarCodePath,$imagedata);

		if (file_exists($sBarCodePath)) {
			$aModule['invoice_barcode'] = $sBarCodePath;
		} else {
			$aModule['invoice_barcode'] = '';
		}

		$aModule['order']['value_brutto'] = Common::formatPrice2($aModule['order']['value_brutto']);
		$aModule['order']['total_value_brutto_words'] = Common::price2Words($aModule['order']['value_brutto']);

		$aModule['order']['value_netto'] = Common::formatPrice($aModule['order']['value_netto']);
		$aModule['order']['value_brutto'] = Common::formatPrice($aModule['order']['value_brutto']);
		$aModule['order']['vat'] = Common::formatPrice($aModule['order']['vat']);


		$aModule['order']['transport_cost'] = Common::formatPrice($aModule['order']['transport_cost']);
		//$aModule['order']['total_value_brutto_words'] = Common::price2Words($aModule['order']['value_brutto']);
		//$aModule['order']['total_value_brutto'] = Common::formatPrice($aModule['order']['total_value_brutto']);

		//$aModule['order']['order_date'] = formatDateClient($aModule['order']['order_date']);
		$aModule['invoice_date'] = date('d.m.Y');

    if ($aModule['order']['invoice_to_pay_days'] == 0 || $aModule['order']['invoice_to_pay_days'] == '' || empty($aModule['order']['invoice_to_pay_days'])) {
      $aModule['order']['invoice_to_pay_days'] = 14;// domyślna wartość
    }

		if ($aModule['order']['invoice_date_pay'] != '') {
			$aModule['invoice_date_14'] = formatDateClient($aModule['order']['invoice_date_pay']);
//      AREK Do modyfikacji
//      $iDays14  = mktime(0, 0, 0, date("m")  , date("d")+20, date("Y"));
//      $aModule['invoice_date_14'] = date('d.m.Y',$iDays14);
		} else {
			if($aModule['order']['payment_type'] == 'bank_14days'){
        // pusta data płatności więc zmieniamy na n DNI
				$aDataArr = explode('.', $aModule['order']['invoice_date']);
//				$iDays14  = mktime(0, 0, 0, date("m")  , date("d")+21, date("Y"));
				$iDays14  = mktime(0, 0, 0, $aDataArr[1]  , $aDataArr[0]+intval($aModule['order']['invoice_to_pay_days']), $aDataArr[2]); // AREK Do modyfikacji
				$aModule['invoice_date_14'] = date('d.m.Y',$iDays14);
				if ($bProForma == false) {
					$sMysqlDateOfPay = date('Y-m-d',$iDays14);
					$this->updateInvoiceDatePay($iId, $sMysqlDateOfPay);
				}
			}
		}
		if ($bProForma != false) {
			// brak terminu do zapłaty na fakturze proforma
			unset($aModule['invoice_date_14']);
		}
		// arek mod
		if (stristr($aModule['order']['payment'], 'przelew 14 dni')) {
			$aModule['order']['payment'] = 'przelew '.$aModule['order']['invoice_to_pay_days'].' dni';
		}
		if($aModule['order']['second_payment_type']!='' && $aModule['order']['second_payment_enabled'])
			$aModule['order']['payment'].=', '.$aModule['order']['second_payment']; // Arek Do modyfikacji przelew 30 dni na sztywno

		// dane zamawiającego
		$aAddresses = $this->getOrderAddresses($iId);
		if($bSecondInvoice){
			$aModule['address'] =& $aAddresses[2];
		} else {
			$aModule['address'] =& $aAddresses[0];
		}

		$aModule['website_symbol'] = $this->getWebsiteSymbol($this->getWebsiteId($iId, false));
		//$aModule['order']['transport'] = $aConfig['lang'][$this->sModule]['transport_1'];
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_seller_data";
		$aModule['seller_data'] =& $pDbMgr->GetRow($aModule['website_symbol'], $sSql);
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.($bProForma ? 'invoice_proforma.tpl' : 'invoice.tpl'));
		$pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	} // end of getInvoiceHtml() method


  /**
   * Metoda pobiera imię oraz nazwisko dla osoby dla zamówienia w statusie
   *
   * @param int $iOrderId
   * @param int $iStatus
   * @return array
   */
  private function _getOrdersToUsers($iOrderId, $iStatus) {

    $sSql = "SELECT U.name, U.surname, OTU.created
             FROM orders_to_users AS OTU
             JOIN users AS U
              ON OTU.user_id = U.id
             WHERE OTU.order_id = ".$iOrderId."
                   AND OTU.internal_status = '".$iStatus."'";
    return Common::GetRow($sSql);
  }// end of _getOrdersToUsers() method


	/**
	 * Metoda pobiera dane faktury z bazy danych
	 *
	 * @global type $aConfig
	 * @param integer $iOrderId
	 * @param bool $bProforma
	 * @return string
	 */
	function invoiceExists($iOrderId, $bProforma) {
		global $aConfig;
		$iWebsiteId = $this->getWebsiteId($iOrderId);
		$sInvoiceWebsiteSymbol = $this->getInvoiceWebsiteSymbol($iOrderId);
		/*
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."invoices
						 WHERE order_id=".$iOrderId." AND proforma='".($bProforma == true? '1' : '0' )."'";
		return (Common::GetOne($sSql) > 0? true : false);
		 */

		if ($bProforma == false) {
			$aMatches = array();
			$aOData = $this->getOrderDefData($iOrderId, array('invoice_date', 'invoice_id'));
			preg_match("/(\d+)-(\d+)-\d+/", $aOData['invoice_date'], $aMatches);
			if (empty($aMatches)) return false;
			// faktura zwykła

			$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].$aConfig['common']['invoice_dir'];
			$sFilename = 'faktura'.str_replace("/","_",$aOData['invoice_id']).'.pdf';
		} else {
			$aMatches = array();
			$sFilename = 'faktura_proforma'.str_replace("/","_",$this->getOrderNumber($iOrderId)).'.pdf';
			preg_match("/faktura_proforma\d+_(\d+)_(\d+)_".$sInvoiceWebsiteSymbol."\.pdf/", $sFilename, $aMatches);
			if (!empty($aMatches)) {
				$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			} else {
				return false;
			}

			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['pro_forma_invoice_dir'];
		}

		if (file_exists($sBaseFile.$sPath.'/'.$sFilename)) {
			return true;
		} else{
			return false;
		}
	}// end of getInvoiceData() method


	/**
	 * Metoda pobiera dane faktury z bazy danych
	 *
	 * @global type $aConfig
	 * @param integer $iOrderId
	 * @param bool $bProforma
	 * @return string
	 */
	function getInvoiceData($iOrderId, $bProforma) {
		global $aConfig;
		$iWebsiteId = $this->getWebsiteId($iOrderId);
		$sInvoiceWebsiteSymbol = $this->getInvoiceWebsiteSymbol($iOrderId);

		/*
		$sSql = "SELECT content FROM ".$aConfig['tabls']['prefix']."invoices
						 WHERE order_id=".$iOrderId." AND proforma='".($bProforma == true? '1' : '0' )."'";
		return Common::GetOne($sSql);
		 */

		if ($bProforma == false) {
			$aMatches = array();
			$aOData = $this->getOrderDefData($iOrderId, array('invoice_date', 'invoice_id'));
			preg_match("/(\d+)-(\d+)-\d+/", $aOData['invoice_date'], $aMatches);
			if (empty($aMatches)) return false;
			// faktura zwykła

			$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].$aConfig['common']['invoice_dir'];
			$sFilename = 'faktura'.str_replace("/","_",$aOData['invoice_id']).'.pdf';
		} else {
			$aMatches = array();
			$sFilename = 'faktura_proforma'.str_replace("/","_",$this->getOrderNumber($iOrderId)).'.pdf';
			preg_match("/faktura_proforma\d+_(\d+)_(\d+)_".$sInvoiceWebsiteSymbol."\.pdf/", $sFilename, $aMatches);
			if (!empty($aMatches)) {
				$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			} else {
				return false;
			}

			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['pro_forma_invoice_dir'];
		}

		if (file_exists($sBaseFile.$sPath.'/'.$sFilename)) {
			return file_get_contents($sBaseFile.$sPath.'/'.$sFilename);
		} else{
			return false;
		}
	}// end of getInvoiceData() method

	/**
	 * Metoda usuwa fakture
	 *
	 * @global type $aConfig
	 * @param type $iOrderId
	 * @param type $bProforma
	 */
	function deleteInvoice($iOrderId, $bProforma) {
		global $aConfig;
		$iWebsiteId = $this->getWebsiteId($iOrderId);
		$sInvoiceWebsiteSymbol = $this->getInvoiceWebsiteSymbol($iOrderId);

		if ($bProforma == false) {
			$aMatches = array();
			$aOData = $this->getOrderDefData($iOrderId, array('invoice_date', 'invoice_id'));
			preg_match("/(\d+)-(\d+)-\d+/", $aOData['invoice_date'], $aMatches);
			if (empty($aMatches)) return false;
			// faktura zwykła

			$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].$aConfig['common']['invoice_dir'];
			$sFilename = 'faktura'.str_replace("/","_",$aOData['invoice_id']).'.pdf';
		} else {
			$aMatches = array();
			$sFilename = 'faktura_proforma'.str_replace("/","_",$this->getOrderNumber($iOrderId)).'.pdf';
			preg_match("/faktura_proforma\d+_(\d+)_(\d+)_".$sInvoiceWebsiteSymbol."\.pdf/", $sFilename, $aMatches);
			if (!empty($aMatches)) {
				$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			} else {
				return false;
			}

			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['pro_forma_invoice_dir'];
		}

		if (file_exists($sBaseFile.$sPath.'/'.$sFilename)) {
			return unlink($sBaseFile.$sPath.'/'.$sFilename);
		} else{
			return false;
		}
	}// end of deleteInvoice() method


	/**
		* Funkcja tworzy katalog docelowy
		*
		* @return	bool	- true: utworzoo; false: wystapil blad
		*/
	function createDirectory($sBaseDir, $sAPath) {
		global $aConfig;

		$aDirs = explode('/', $sAPath);
		foreach ($aDirs as $sDir) {
			$sPath .= '/'.$sDir;
			if (!is_dir($sBaseDir.$sPath)) {
				if (!mkdir($sBaseDir.$sPath,
										$aConfig['default']['new_dir_mode'])) {
					return false;
				}
        @chmod($sBaseDir.$sPath, $aConfig['default']['new_dir_mode']);
			}
		}
		return true;
	} // end of createDirectory() function

  /**
   *
   * @param int $iWebsiteId
   * @param int $iOrderId
   * @return boolean
   */
  private function getInvoiceNumberPath($iWebsiteId, $iOrderId) {

			$sDate = $this->getOrderInvoiceData($iOrderId);
			preg_match("/(\d+)-(\d+)-\d+/", $sDate, $aMatches);
      if (empty($aMatches)) return false;

      return '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
  }


	/**
	 * Metoda zapisuje informacje o fakturze lub fakturze proforma
	 *
	 * @global type $aConfig
	 * @param type $iOrderId - id zamówienia
	 * @param type $sFilename - nazwa pliku
	 * @param type $sContent - zawartosc pliku
	 * @param type $bProforma - czy proforma
	 * @param type $sInvoiceData - data wygenerowania faktury uzupełniane
	 *															tylko w przypadku kiedy nie jest to proforma
	 */
	function saveInvoiceData($iOrderId, $sFilename, $sContent, $bProforma, $sInvoiceData = 'NULL') {
		global $aConfig;
		$iWebsiteId = $this->getWebsiteId($iOrderId);
		$sInvoiceWebsiteSymbol = $this->getInvoiceWebsiteSymbol($iOrderId);

		/*
		$aValues = array(
				'order_id' => $iOrderId,
				'invoice_name' => $sFilename,
				'data' => ($bProforma == true ? 'NULL' : $sInvoiceData),
				'invoice2' => '0', //pole jeśli faktury miały by zostać rozdzielone
				'proforma' => ($bProforma == true ? '1' : '0'),
				'content' => addslashes($sContent)
		);
		if ( Common::Insert($aConfig['tabls']['prefix']."invoices", $aValues) === false) {
			return false;
		} else {
			if ($bProforma != true) {
			// jeśli powiodło się i nowa faktura nie jest proformą usunięcie faktury proforma zamówienia
			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."invoices WHERE order_id=".$iOrderId." AND proforma='1' LIMIT 1";
			return Common::Query($sSql);
			} else {
				return true;
			}
		}
		// nie powinno tu nigdy wpasc
		return false;*/

		if ($bProforma == false) {
			$sPath = $this->getInvoiceNumberPath($iWebsiteId, $iOrderId);
			if (false == $sPath) return false;
			// faktura zwykła

			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].$aConfig['common']['invoice_dir'];
			$this->createDirectory($sBaseFile, $sPath);
		} else {
			preg_match("/faktura_proforma\d+_(\d+)_(\d+)_".$sInvoiceWebsiteSymbol."\.pdf/", $sFilename, $aMatches);
			if (!empty($aMatches)) {
				$sPath = '/'.$iWebsiteId.'/'.$aMatches['1'].'/'.$aMatches['2'];
			} else {
				return false;
			}

			$sBaseFile = $aConfig['common']['client_base_path'].$aConfig['common']['pro_forma_invoice_dir'];
			$this->createDirectory($sBaseFile, $sPath);
		}

    // XXX TODO do usunięcia - dodane aby zablokować zapisywanie
    // return true; //
		//dump($sBaseFile.$sPath.'/'.$sFilename);
    $sBasePathFilename = $sBaseFile.$sPath.'/'.$sFilename;
		if (!file_exists($sBasePathFilename)) {
			$iCount = file_put_contents($sBasePathFilename, $sContent);
		}
		if ($iCount === false) {
			return false;
		}
		return $sBasePathFilename;
	}// end of saveInvoiceData() method


	/**
	 * Metoda pobiera date wygenerowania faktury
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type
	 */
	function getOrderInvoiceData($iOId) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT invoice_date FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id=".$iOId."
						 LIMIT 1";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getOrderInvoiceData() method


	/**
	 * Metoda pobiera
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @param type $aPools
	 * @return type
	 */
	function getOrderDefData($iOId, $aPools) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT ".implode(',', $aPools)." 
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id=".$iOId;
		return $pDbMgr->GetRow('profit24', $sSql);
	}// end of getOrderDefData() method


	/**
	 * Metoda pobiera date wygenerowania faktury
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type
	 */
	function getOrderNumber($iOId) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT order_number FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id=".$iOId."
						 LIMIT 1";
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getOrderInvoiceData() method


	/**
	 * Sprawdza czy sposob transportu dla amówienia to odbiór osobisty
	 * @param int $iOrderId - id zamowienia
	 * @return bool
	 */
	function checkPersonalReception($iOrderId){
		global $aConfig, $pDbMgr;

		$sSql = "SELECT B.personal_reception
						FROM ".$aConfig['tabls']['prefix']."orders A
						LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						ON B.id=A.transport_id
						WHERE A.id = ".$iOrderId;
		return ($pDbMgr->GetOne('profit24', $sSql) == '1');
	}// end of checkPersonalReception() method

		/**
	 * Pobiera wysokosc stawki VAT dla metody płatności/transportu
	 * @param int $iId - id zamówienia
	 * @return float - stawka vat
	 */
	function getPaymentVat($iId){
		global $aConfig, $pDbMgr;
		$sSql = "SELECT vat
						FROM ".$aConfig['tabls']['prefix']."orders_payment_types
						WHERE id = ".$iId;
		return intval($pDbMgr->GetOne('profit24', $sSql));
	}
			/**
	 * Pobiera dane adresowe z zamówienai z bazy
	 * @param int $iId - id adresu
	 * @return array ref
	 */
	function &getOrderAddresses($iId){
		global $aConfig, $pDbMgr;

		$sSql = "SELECT address_type, is_company, company, name, surname, street, number, number2, postal, city, nip, phone
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
						 " ORDER BY address_type";
		return $pDbMgr->GetAssoc('profit24', $sSql);
	} // end of getOrderAddresses() method


	/**
	 * Metoda aktualizuje datę płatności faktury w przypadku wybrania metody płatności vat 14 oraz vat ...(30?)
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @param type $sDateOfPay
	 * @return bool
	 */
	function updateInvoiceDatePay($iOId, $sDateOfPay) {
		global $aConfig, $pDbMgr;

		$aValues = array();
		$aValues['invoice_date_pay'] = $sDateOfPay;
		if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."orders", $aValues, " id=".$iOId) !== false) {
			return true;
		}
		return false;
	}// end of updateInvoiceDatePay() method


	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type
	 */
	function getWebsiteSymbol($iWebsiteId) {
		global $aConfig;

		$sSql = "SELECT code FROM ".$aConfig['tabls']['prefix']."websites
						 WHERE id=".$iWebsiteId;
		return Common::GetOne($sSql);
	}// end of getWebsiteSymbol() method
}

?>
