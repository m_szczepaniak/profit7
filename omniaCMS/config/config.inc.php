<?php
// dolaczenie pliku konfiguracyjnymi ze wspolnymi ustawieniami dla
// czesci klienckiej i administracyjnej
include_once ('common.inc.php');

/********************************************************************
 * Plik konfiguracyjny
 *******************************************************************/
 
$aConfig['common']['base_path']	.= $aConfig['common']['cms_dir'];
$aConfig['common']['base_url']	.= $aConfig['common']['cms_dir'];
$aConfig['common']['base_url_http']	= 'https://'.$aConfig['common']['base_url'];
$aConfig['common']['base_url_https'] = 'https://'.$aConfig['common']['base_url'];

$aConfig['opek']['acces_key'] = 'o0p1T42jdInV64';
$aConfig['connector']['connector_acces_key'] = 'Ho039l1i1sd';

$aConfig['wysiwyg']['css_style'] = 'css/editor.css?v=25-04-2017';

// katalog zapisywania sesji
if (!class_exists('memcached')) {
  session_save_path($aConfig['common']['client_base_path'].'tmp');
} else {
  session_save_path($aConfig['memcached_storage']['host'].':'.$aConfig['memcached_storage']['port']);
  ini_set('session.save_handler', 'memcached');
}
session_save_path($aConfig['memcached_storage']['host'].':'.$aConfig['memcached_storage']['port']);

session_name('sid');
ini_set('session.use_only_cookies', 1);
ini_set('session.use_trans_sid', 0);
ini_set('url_rewriter.tags', '');


// rozpoczecie sesji
session_start();

if ($_SESSION['user']['name'] == 'agolba' || $_SESSION['user']['continue_session'] == '1') {
  $iTimeout = (3600*60);// czyli po ponad 2 godzinach od ostatnich odwiedzin niszczymy sesję
} elseif ($_SESSION['user']['priv_order_5_status'] == '1') {
  $iTimeout = (10*60); // Number of seconds until it times out.
} else {
  $iTimeout = (20*60); // Number of seconds until it times out.
}
// Check if the timeout field exists.
if(isset($_SESSION['timeout'])) {
  // See if the number of seconds since the last
  // visit is larger than the timeout period.
  $iStartTime = (int)$_SESSION['timeout'];
  $iDuration = time() - $iStartTime;
  if($iDuration > $iTimeout) {
    // Destroy the session and restart it.
    session_destroy();
    session_start();
    $_SESSION['logout_timeout'] = $iStartTime+$iTimeout;
  }
}
if ($aConfig['continue_session'] == TRUE || $_SESSION['user']['name'] == 'agolba' || $_SESSION['user']['continue_session'] == '1') {
  // Update the timout field with the current time.
  $_SESSION['timeout'] = time();
}