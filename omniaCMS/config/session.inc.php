<?php
global $aConfig;

if ($aConfig['common']['use_session'] && !isset($_SESSION)) {
  // TEN PROJEKT UZYWA MODULOW WYMAGAJACYCH SESJI
  // katalog zapisywania sesji

  if (class_exists('memcached')) {
    //session_save_path($aConfig['memcached_storage']['host'].':'.$aConfig['memcached_storage']['port']);
	ini_set('session.save_path', $aConfig['memcached_storage']['host'].':'.$aConfig['memcached_storage']['port']);
    ini_set('session.save_handler', 'memcached');
  } else {
    session_save_path($aConfig['common']['client_base_path'].'tmp');
  }
  
  session_name('sid');
  ini_set('session.use_only_cookies', 1);
  ini_set('session.use_trans_sid', 0);
  ini_set('url_rewriter.tags', '');
  // rozpoczecie sesji
  session_start();
}
?>