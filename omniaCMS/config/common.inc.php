<?php
/************************************************************************
 * Wspolny plik konfiguracyjny dla czesci klienckiej i administracyjnej
 ***********************************************************************/

if (!preg_match('/.*\/$/', $_SERVER['DOCUMENT_ROOT'])) {
    $_SERVER['DOCUMENT_ROOT'] .= '/';
}
if (!preg_match('/.*\/$/', $_SERVER['SERVER_NAME'])) {
    $_SERVER['SERVER_NAME'] .= '/';
}
if (substr($_SERVER['HTTP_HOST'], 0, 3) == 'www' &&
    substr($_SERVER['SERVER_NAME'], 0, 3) != 'www'
) {
    $_SERVER['SERVER_NAME'] = 'www.' . $_SERVER['SERVER_NAME'];
}

$aConfig['common']['bookstore_code'] = 'profit24';

$aConfig['common']['404_page_symbol'] = '404';

// domayslna wersja jezykowa interfejsu i kodowanie znakow
$aConfig['default']['language'] = 'pl';
$aConfig['default']['charset'] = 'utf-8';
$aConfig['default']['per_page'] = 20;
$aConfig['common']['allowed_product_type'] = ['K', 'A'];// tylko książki sprzedajemy

$aConfig['common']['max_cart_items'] = 100;

// domyslne rozmiary obrazkow
$aConfig['default']['thumb_size'] = '82x96';
$aConfig['default']['small_size'] = '160x160';
$aConfig['default']['big_size'] = '800x800';

// rozmiar obrazka z importu
$aConfig['import']['thumb_size'] = '82x96';
$aConfig['import']['small_size'] = '160x160';
$aConfig['import']['big_size'] = '800x800';

$aConfig['import']['unsorted_category'] = 422;//422
$aConfig['import']['unsorted_category_editio'] = 521; //  UWAGA TAKA KATEGORIA MA BYĆ NIEZMAPOWANĄ HELION

$aConfig['import']['abe_login'] = 'profit';
$aConfig['import']['abe_password'] = 'ejij824';
$aConfig['import']['azymut_login'] = '10831';
$aConfig['import']['azymut_password'] = 'ncYX8PwJ';

$aConfig['import']['ftp_azymut_login'] = 'wbd_10831';
$aConfig['import']['ftp_azymut_password'] = 'VCcXT96CZK\pBtH@';

$aConfig['import']['AUTO_azymut_login'] = '03_03831';
$aConfig['import']['AUTO_azymut_password'] = '431$ProfitMJer';

// długości tekstu w generatorze ofert
$aConfig['offer_generator']['max_records'] = 1000;
$aConfig['offer_generator']['name_length'] = 100;
$aConfig['offer_generator']['publisher_length'] = 100;
$aConfig['offer_generator']['authors_length'] = 100;

// domyslny tryb praw zapisu ustawiany dla tworzonych katalogow
$aConfig['default']['new_dir_mode'] = 0777;

// domylsna liczba pol na zdjecia
$aConfig['default']['photos_no'] = 5;

// domylsna liczba pol na zalaczniki
$aConfig['default']['files_no'] = 5;

// liczba dni na potwierdzenie
$aConfig['default']['days_to_confirm'] = 7;

// szerokosc pol formularza
$aConfig['default']['form_field_width'] = '200px;';
// szerokość pola textowego
$aConfig['default']['form_textarea_width'] = '360px;';
// szerokosc pol krotkich np. numer domu, mieszkania formularza
$aConfig['default']['short_form_field_width'] = '38px;';
// szerokosc pol srednich, np. logowania formularza
$aConfig['default']['middle_form_field_width'] = '125px;';
// szerokosc pola daty formularza
$aConfig['default']['date_form_field_width'] = '75px;';

// status - development lub productive
$aConfig['common']['status'] = 'development';
$aConfig['common']['js_validation'] = true;
$aConfig['common']['mod_rewrite'] = true;
$aConfig['common']['use_session'] = true;
// kompresja zlib
$aConfig['common']['use_zlib'] = false;

// rc4 key
$aConfig['common']['rc4_key'] = 'Ad5$56jhUGcd#lk';

$aConfig['common']['login_safekey'] = '9f6xdh9';

$aConfig['common']['base_path'] = $_SERVER['DOCUMENT_ROOT'];
$aConfig['common']['cms_dir'] = 'omniaCMS/';
$aConfig['common']['base_url'] = $_SERVER['SERVER_NAME'];
$aConfig['common']['base_url_http'] = 'https://' . $aConfig['common']['base_url'];
$aConfig['common']['base_url_http_no_slash'] = substr($aConfig['common']['base_url_http'], 0, -1);
$aConfig['common']['base_url_https'] = 'https://' . $aConfig['common']['base_url'];
$aConfig['common']['base_url_https_no_slash'] = substr($aConfig['common']['base_url_https'], 0, -1);
$aConfig['common']['is_https'] = ($_SERVER['HTTPS'] == 'on');
$aConfig['common']['client_base_path'] = $aConfig['common']['base_path'];
$aConfig['common']['client_base_url'] = $aConfig['common']['base_url'];
$aConfig['common']['client_base_url_http'] = $aConfig['common']['base_url_http'];
$aConfig['common']['client_base_url_http_no_slash'] = substr($aConfig['common']['client_base_url_http'], 0, -1);
$aConfig['common']['client_base_url_https'] = $aConfig['common']['base_url_https'];
$aConfig['common']['client_base_url_https_no_slash'] = substr($aConfig['common']['client_base_url_https'], 0, -1);
$aConfig['common']['sql_date_format'] = '%d.%m.%Y';
$aConfig['common']['sql_hour_format'] = '%H:%i';
$aConfig['common']['sql_date_hour_format'] = '%d.%m.%Y %H:%i';
$aConfig['common']['sql_plain_hour_format'] = '%H';
$aConfig['common']['cal_sql_date_format'] = '%d-%m-%Y';
$aConfig['common']['date_format'] = 'd.m.Y';
$aConfig['common']['cal_date_format'] = 'd-m-Y';
$aConfig['common']['mysql_date_format'] = 'Y-m-d';
$aConfig['common']['img_extensions'] = array('gif', 'jpg', 'jpeg', 'png');
$aConfig['common']['photo_dir'] = 'images/photos';
$aConfig['common']['product_photo_dir'] = 'images/okladki';
$aConfig['common']['publishers_logos_dir'] = 'images/publishers';
$aConfig['common']['language_flags_dir'] = 'images/languages';
$aConfig['common']['file_extensions'] = array('zip', 'rar', 'doc', 'odt', 'rtf', 'pdf', 'xls', 'ppt', 'pps', 'exe', 'txt');
$aConfig['common']['product_file_extensions'] = array('doc', 'odt', 'rtf', 'pdf');
$aConfig['common']['file_dir'] = 'images/files';
$aConfig['common']['product_file_dir'] = 'images/pdf';
$aConfig['common']['upload_max_filesize'] = ini_get('upload_max_filesize');
$aConfig['common']['import_dir'] = 'import';

$aConfig['common']['urchin_tracker'] = true;

$aConfig['common']['email_logo_file'] = '/images/gfx/logo_mail.png';

$aConfig['common']['import_send_to'] = 'raporty@profit24.pl';
$aConfig['common']['import_sender_email'] = 'a.golba@profit24.pl';

$aConfig['common']['transport_logos_dir'] = 'images/transports';
$aConfig['common']['transport_logos_size'] = '84x84';

$aConfig['common']['main_page_symbol'] = 'dzial-naukowy';

// ustawienia Smartow
$aConfig['smarty']['template_dir'] = 'smarty/templates/';
$aConfig['smarty']['compile_dir'] = 'smarty/templates_c/';
$aConfig['smarty']['config_dir'] = 'smarty/configs/';
$aConfig['smarty']['cache_dir'] = 'smarty/cache/';
$aConfig['common']['smarty_caching'] = TRUE;

// dane do logowania do bazy danych
if (strpos($_SERVER['DOCUMENT_ROOT'], 'xampp') !== false || strpos($_SERVER['DOCUMENT_ROOT'], '/httpdocs/') === false) {
    $aConfig['memcached']['host'] = 'localhost';
    $aConfig['memcached']['port'] = 11211;

    $aConfig['memcached_storage']['host'] = 'localhost';
    $aConfig['memcached_storage']['port'] = 11211;

    include_once('config_files/dev_db_config.php');

    $aConfig['rabbitmq']['host'] = '172.16.5.213';
    $aConfig['rabbitmq']['port'] = 5672;
    $aConfig['rabbitmq']['user'] = 'test';
    $aConfig['rabbitmq']['pass'] = '7SztdEwz8RMTQbvC';
    $aConfig['rabbitmq']['vhost'] = '/';

    $aConfig['merlin']['user'] = 'profitm';
    $aConfig['merlin']['pass'] = 'aOlSkPqxOK7EZE0b';

    $aConfig['common']['newsletter_test'] = true;
    $aConfig['common']['newsletter_dir'] = 'newsletter/';
    $aConfig['common']['opek_dir'] = 'opek/';
    $aConfig['common']['invoice_dir'] = 'invoices/';
    $aConfig['common']['pro_forma_invoice_dir'] = 'proforma/';
    $aConfig['common']['platosci_pl_test_mode_email'] = 'a.golba@profit24.pl';
    $aConfig['common']['platosci_pl_test_mode'] = true;
    //$aConfig['common']['programmer_ip'] = array('192.168.1.188', '192.168.1.145', '192.168.1.164', '10.0.2.2', '127.0.0.1', '192.168.1.188', '192.168.1.6', '192.168.1.5','192.168.1.8', '192.168.1.102', '192.168.1.11', '192.168.1.7');

    // ustawienie error_handlera
    $aConfig['err_handler']['use_error_handler'] = FALSE;
    $aConfig['err_handler']['show_errors'] = TRUE;
    $aConfig['err_handler']['display_errors'] = array(E_ERROR, E_WARNING, E_PARSE, E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE, E_RECOVERABLE_ERROR, E_USER_DEPRECATED, E_CORE_WARNING, E_COMPILE_WARNING);
    $aConfig['err_handler']['log_errors'] = FALSE;
    $aConfig['err_handler']['log_errors_type'] = array(E_ERROR, E_WARNING, E_PARSE, E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE, E_RECOVERABLE_ERROR, E_USER_DEPRECATED, E_CORE_WARNING, E_COMPILE_WARNING);
    $aConfig['err_handler']['log_dir'] = 'logs';
    $aConfig['err_handler']['mail_errors'] = FALSE;
    $aConfig['err_handler']['mail_errors_type'] = array(E_ERROR, E_WARNING, E_PARSE, E_USER_ERROR, E_USER_WARNING, E_STRICT, E_RECOVERABLE_ERROR, E_USER_NOTICE, E_DEPRECATED, E_USER_DEPRECATED, E_CORE_WARNING, E_COMPILE_WARNING);
    $aConfig['err_handler']['mail_errors_subject'] = 'PHP Error: %s';
    $aConfig['err_handler']['mail_errors_from'] = 'arekgolba@gmail.com';
    $aConfig['err_handler']['mail_errors_to'] = 'arekgolba@gmail.com';

    // Paczkomaty
    $aConfig['common']['paczkomaty']['api'] = 'https://sandbox-api.paczkomaty.pl';
    $aConfig['common']['paczkomaty']['login'] = 'test@testowy.pl';
    $aConfig['common']['paczkomaty']['password'] = 'WqJevQy*X7';
    // mail testowy - tworzenia nowego konta - do uzycia tylko lokalnie !!
    $aConfig['common']['paczkomaty']['email'] = 'test04@paczkomaty.pl';
    $aConfig['common']['paczkomaty_dir'] = 'paczkomaty/';
    $aConfig['common']['fedex_dir'] = 'fedex/';
    $aConfig['common']['ruch_dir'] = 'ruch/';
    $aConfig['common']['orlen_dir'] = 'orlen/';
    $aConfig['common']['tba_dir'] = 'tba/';
    $aConfig['common']['poczta_polska_dir'] = 'pocztapolska/';
    $aConfig['common']['confirm_dir'] = 'paczkomaty_potwierdzenia/';
    $aConfig['common']['smarty_caching'] = FALSE;
    $aConfig['common']['internal_label_dir'] = 'internal_label/';

    $aConfig['facebook']['appId'] = '449900555102390';
    $aConfig['facebook']['secret'] = 'e3a636f49daf1abdc44dcaf934ae9e65';

    $aConfig['common']['google_survey'] = '
    
    <script src="https://apis.google.com/js/platform.js?onload=renderOptIn" async defer></script>

<script>
  window.renderOptIn = function() {
    window.gapi.load("surveyoptin", function() {
      window.gapi.surveyoptin.render(
        {
          "merchant_id": 113291023,
          "order_id": "%s",
          "email": "%s",
          "delivery_country": "PL",
          "estimated_delivery_date": "%s"
        });
    });
  }
</script>

    ';

    $aConfig['common']['facebook_pixel_purchase'] = '
    <script type="text/javascript">
        fbq("track", "Purchase", {currency: "PLN", value: %s});
    </script>
    ';

} else {
    if ($_SERVER['REMOTE_ADDR'] == '89.68.221.12') {
        $aConfig['common']['platosci_pl_test_mode'] = true;
    } else {
        $aConfig['common']['platosci_pl_test_mode'] = false;
    }
    $aConfig['memcached']['host'] = '172.16.6.12';
    $aConfig['memcached']['port'] = '11211';

    $aConfig['memcached_storage']['host'] = '172.16.6.12';
    $aConfig['memcached_storage']['port'] = 11216;

    $aConfig['rabbitmq']['host'] = '172.16.4.40';
    $aConfig['rabbitmq']['port'] = 5672;
    $aConfig['rabbitmq']['user'] = 'krolIk';
    $aConfig['rabbitmq']['pass'] = 'ueM38YvRDwYQ6rNE0gMYq7xKJ';
    $aConfig['rabbitmq']['vhost'] = '/';

    $aConfig['merlin']['user'] = 'profitm';
    $aConfig['merlin']['pass'] = 'aOlSkPqxOK7EZE0b';

    $aConfig['common']['smarty_caching'] = TRUE;

    $aConfig['facebook']['appId'] = '593936837295662';
    $aConfig['facebook']['secret'] = '25e5100f91c64da2fdbda41a628b2435';

    include_once('config_files/prod_db_config.php');

    // Paczkomaty
    //$aConfig['common']['paczkomaty']['login'] = 'test@testowy.pl';
    //$aConfig['common']['paczkomaty']['password'] = 'WqJevQy*X7';
    // mail testowy - tworzenia nowego konta - do uzycia tylko lokalnie !!
    //$aConfig['common']['paczkomaty']['email'] = 'test04@paczkomaty.pl';
    /*
  $aConfig['common']['paczkomaty_dir']	= '../../paczkomaty/';
  $aConfig['common']['confirm_dir']	= 'paczkomaty_potwierdzenia/';
  $aConfig['common']['opek_dir']	= '../../opek/';
  $aConfig['common']['invoice_dir']	= '../../invoices/';
  $aConfig['common']['pro_forma_invoice_dir']	= '../proforma/';
  $aConfig['common']['platosci_pl_test_mode_email']	= 'arekgolba@gmail.com';

  $aConfig['db']['dbns'] = 'mysql';
  $aConfig['db']['host'] = '58727.m.tld.pl';
  $aConfig['db']['name'] = 'baza58727_prof2';
  $aConfig['db']['user'] = 'admin58727_prof2';
  $aConfig['db']['passwd'] = 'D5vb4P02';
*/
    $aConfig['default']['new_dir_mode'] = 0775;
    $aConfig['common']['status'] = 'productive';
    $aConfig['common']['newsletter_test'] = false;
    $aConfig['common']['newsletter_dir'] = 'newsletter/';

    // Paczkomaty
    $aConfig['common']['paczkomaty']['login'] = 'kontakt@profit24.pl';
    $aConfig['common']['paczkomaty']['password'] = 'hadenprofit2002D';
    $aConfig['common']['paczkomaty_dir'] = '../../paczkomaty/';
    $aConfig['common']['internal_label_dir'] = '../../internal_label/';
    $aConfig['common']['fedex_dir'] = '../../fedex/';
    $aConfig['common']['poczta_polska_dir'] = '../../pocztapolska/';
    $aConfig['common']['ruch_dir'] = '../../ruch/';
    $aConfig['common']['orlen_dir'] = '../../orlen/';
    $aConfig['common']['tba_dir'] = '../../tba/';
    $aConfig['common']['confirm_dir'] = 'paczkomaty_potwierdzenia/';
    $aConfig['common']['opek_dir'] = '../../opek/';
    $aConfig['common']['invoice_dir'] = '../../invoices/';
    $aConfig['common']['pro_forma_invoice_dir'] = '../proforma/';
    $aConfig['common']['platosci_pl_test_mode_email'] = 'arekgolba@gmail.com';

    $aConfig['common']['google_analytics'] = "
<script type=\"text/javascript\">

  var _gaq = _gaq || [];

  _gaq.push(
    ['_setAccount', 'UA-11882157-2'],
    ['_trackPageview']
  );


  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>   
";


    $aConfig['common']['google_analytics_conversion_order'] = '<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src=\'" + gaJsHost + "google-analytics.com/ga.js\' type=\'text/javascript\'%3E%3C/script%3E"));
</script>

<script type="text/javascript">
try {
  var pageTracker = _gat._getTracker("UA-11882157-2");
  pageTracker._initData();
  pageTracker._trackPageview();

  {CONVERSION_CODE}
  
  pageTracker._trackTrans();
} catch(err) {}</script>';

    // XXX TU jeszcze włączony
    $aConfig['common']['ceneo_zaufane_opinie'] = '
<script type="text/javascript">
  <!--
  ceneo_client_email = "%s";
  ceneo_order_id = "%s";
  ceneo_shop_product_ids ="%s";
  ceneo_work_days_to_send_questionnaire = 5;
  //-->
</script>
<script type="text/javascript" src="https://ssl.ceneo.pl/transactions/track/v2/script.js?accountGuid=dd6cd223-6c35-49c7-a38d-195a316dd1af">
</script>
';
    $aConfig['common']['google_survey'] = '
    
    <script src="https://apis.google.com/js/platform.js?onload=renderOptIn" async defer></script>

<script>
  window.renderOptIn = function() {
    window.gapi.load("surveyoptin", function() {
      window.gapi.surveyoptin.render(
        {
          "merchant_id": 113291023,
          "order_id": "%s",
          "email": "%s",
          "delivery_country": "PL",
          "estimated_delivery_date": "%s"
        });
    });
  }
</script>

    ';

    $aConfig['common']['facebook_pixel_purchase'] = '
    <script type="text/javascript">
        fbq("track", "Purchase", {currency: "PLN", value: %s});
    </script>
    ';


    // ustawienie error_handlera
    $aConfig['err_handler']['use_error_handler'] = FALSE;
    $aConfig['err_handler']['show_errors'] = FALSE;
    $aConfig['err_handler']['mail_errors'] = FALSE;
    $aConfig['err_handler']['log_errors'] = FALSE;
    $aConfig['common']['buybox_commision_id'] = 164;
}

// tabele bazy danych
$aConfig['tabls']['prefix'] = '';

// dane do dotpay.pl
$aConfig['dotpay']['id'] = '';

$aConfig['regexp_order_number'] = "\s{12}";

// konfiguracja dla skladowych klas biblioteki
$aConfig['class']['form']['order_number'] = array('pcre' => '/^' . $aConfig['regexp_order_number'] . '$/', 'maxlength' => 26);
$aConfig['class']['form']['password'] = array('pcre' => '/^[^\\\\"\'\/]{6,16}$/', 'maxlength' => 16);
$aConfig['class']['form']['login'] = array('pcre' => '/^[\w@.-]{3,32}$/', 'maxlength' => 32);
$aConfig['class']['form']['text'] = array('pcre' => "", 'maxlength' => 0);
$aConfig['class']['form']['integer'] = array('pcre' => "/^[+-]?\d+$/", 'maxlength' => 15);
$aConfig['class']['form']['uinteger'] = array('pcre' => "/^\d+$/", 'maxlength' => 15);
$aConfig['class']['form']['float'] = array('pcre' => "/^([+-]?)(?=\d|[\.,]\d)\d*([\.,]\d*)?([Ee]([+-]?\d+))?$/", 'maxlength' => 15);
$aConfig['class']['form']['price'] = array('pcre' => "/^\d{1,}([\.,]\d{1,2})?$/", 'maxlength' => 10);
//$aConfig['class']['form']['email'] = array('pcre' => "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,6})$/", 'maxlength' => 100);
$aConfig['class']['form']['email']		= array('pcre'=>"/^[-0-9a-zA-Z.+_]+@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]{2,6}$/i", 'maxlength'=>100);
$aConfig['class']['form']['emails'] = array('pcre' => "/^[-_a-z0-9]+(\.[-_a-z0-9]+)*@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]{2,6}(;[-_a-z0-9]+(\.[-_a-z0-9]+)*@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]{2,6})*$/i", 'maxlength' => 255);
$aConfig['class']['form']['pesel'] = array('pcre' => "", 'maxlength' => 11);
$aConfig['class']['form']['nip'] = array('pcre' => "/^((\d{3}-\d{3}-\d{2}-\d{2})|(\d{3}-\d{2}-\d{2}-\d{3})|(\d{10}))$/", 'maxlength' => 13);
/*
 * 
xx xxx xx xx (np. 12 345 67 89)
xxxxxxxxx (np. 123456789)
xxx xxx xxx (np. 123 456 789)
xx xx xx xxx (np. 12 34 56 789)
xxx xx xx xx (np. 123 45 67 89)
xx xxxxxxx (np. 12 3456789)

 */
$aConfig['class']['form']['phone'] = array('pcre' => "/^((\d{2}[ -]{1}\d{3}[ -]{1}\d{2}[ -]{1}\d{2})|(\d{9})|(\d{3}[ -]{1}\d{3}[ -]{1}\d{3})|(\d{2}[ -]{1}\d{2}[ -]{1}\d{2}[ -]{1}\d{3})|(\d{3}[ -]{1}\d{2}[ -]{1}\d{2}[ -]{1}\d{2})|(\d{2}[ -]{1}\d{7}))$/", 'maxlength' => 16);
$aConfig['class']['form']['regon'] = array('pcre' => "", 'maxlength' => 14);
$aConfig['class']['form']['postal'] = array('pcre' => "/^[0-9]{2}-[0-9]{3}$/", 'maxlength' => 6);
$aConfig['class']['form']['date'] = array('pcre' => "/^\d{2}-\d{2}-\d{4}$/", 'format' => $aConfig['common']['date_format'], 'maxlength' => 10);

// wyrazenia PCRE
$aConfig['common']['login']['pcre'] = '/^[\w]{3,16}$/';
$aConfig['common']['passwd']['pcre'] = '/^[\w!@#$%\^&*\(\)\-\+=\[\]\{\}:;\|\/,\.\?><~`]{6,16}$/';
$aConfig['common']['img_size']['pcre'] = '/^[0-9]{1,4}x[0-9]{1,4}$/';
$aConfig['common']['symbol']['pcre'] = '/^[\w]{1,32}$/';


// id serwisów
$aConfig['smarkacz_website_id'] = 5;
$aConfig['mestro_website_id'] = 4;
$aConfig['np_website_id'] = 3;
$aConfig['it_website_id'] = 2;
$aConfig['profit24_website_id'] = 1;
$aConfig['nb_website_id'] = 8;
$aConfig['invoice_website_symbol_' . $aConfig['profit24_website_id']] = 'PR24';
$aConfig['invoice_website_symbol_' . $aConfig['it_website_id']] = 'IT';
$aConfig['invoice_website_symbol_' . $aConfig['np_website_id']] = 'NP';
$aConfig['invoice_website_symbol_' . $aConfig['mestro_website_id']] = 'ME';
$aConfig['invoice_website_symbol_' . $aConfig['smarkacz_website_id']] = 'SM';
$aConfig['invoice_website_symbol_' . $aConfig['nb_website_id']] = 'NB';

// numery faktur
//$aConfig['invoice_number_'.$aConfig['profit24_website_id']] = "%2$05d/%1$05d/%3$02dPR24";
//$aConfig['invoice_number_'.$aConfig['it_website_id']] = "%2$05d/%1$05d/%3$02dIT";
$aConfig['invoice_number_' . $aConfig['profit24_website_id']] = "%2$12s%1$06s";
$aConfig['invoice_number_' . $aConfig['it_website_id']] = "%2$12s%1$06s";
$aConfig['invoice_number_' . $aConfig['np_website_id']] = "%2$12s%1$06s";
$aConfig['invoice_number_' . $aConfig['mestro_website_id']] = "%2$12s%1$06s";
$aConfig['invoice_number_' . $aConfig['smarkacz_website_id']] = "%2$12s%1$06s";
$aConfig['invoice_number_' . $aConfig['nb_website_id']] = "%2$12s%1$06s";

//$aConfig['order_number_'.$aConfig['profit24_website_id']] = "%05d/%s/%s/PR24";
//$aConfig['order_number_'.$aConfig['it_website_id']] = "%05d/%s/%s/IT";

$aConfig['common']['profit24']['client_base_url_http_no_slash'] = 'https://profit24.pl';
$aConfig['common']['it']['client_base_url_http_no_slash'] = 'https://ksiegarnia.it';
$aConfig['common']['np']['client_base_url_http_no_slash'] = 'https://nieprzeczytane.pl';
$aConfig['common']['mestro']['client_base_url_http_no_slash'] = 'https://mestro.pl';
$aConfig['common']['smarkacz']['client_base_url_http_no_slash'] = 'https://smarkacz.pl';
$aConfig['common']['nb']['client_base_url_http_no_slash'] = 'https://www.naszabiblioteka.pl';

$aConfig['common']['profit24']['email_logo_file'] = '/images/gfx/logo_mail.png';
$aConfig['common']['it']['email_logo_file'] = '/images/gfx/logoIT_mail.png';
$aConfig['common']['np']['email_logo_file'] = '/images/gfx/logoNP_mail.png';
$aConfig['common']['mestro']['email_logo_file'] = '/images/gfx/logoMESTRO_mail.png';
$aConfig['common']['smarkacz']['email_logo_file'] = '/images/gfx/logoSMARKACZ_mail.png';
$aConfig['common']['nb']['email_logo_file'] = '/images/gfx/logoNB_mail.png';

$aConfig['ceneo']['api_domain'] = "https://developers.ceneo.pl/api/v2/function/";
$aConfig['ceneo']['api_key'] = "eab8423f-71b6-452a-a816-09674f42186d";
$aConfig['ceneo']['api_time_to_live_minutes'] = 60;
$aConfig['ceneo']['mail_recivers'] = ['raporty@profit24.pl', 'l.jaskiewicz@profit24.pl'];
//$aConfig['ceneo']['near_products_mail_recivers'] = ['k.malz@profit24.pl', 'm.chudy@profitm.pl', 'l.jaskiewicz@profit24.pl'];
$aConfig['ceneo']['near_products_mail_recivers'] = [];

$aConfig['price_updater']['our_book_stores']['current_book_store'] = "profit24.pl";

$aConfig['common']['ceneo_xml_path'] = $_SERVER['DOCUMENT_ROOT'] . 'ceneo_xml_report/';

$aConfig['common']['base_url_http'] = 'https://www.profit24.pl/';
$aConfig['common']['base_url_http_no_slash'] = 'https://www.profit24.pl';
$aConfig['website_id'] = $aConfig[$aConfig['common']['bookstore_code'] . '_website_id'];
if ($aConfig['common']['status'] == 'development') {
    $aConfig['common']['base_url_http'] = 'https://www.profit.local/';
    $aConfig['common']['base_url_http_no_slash'] = 'https://www.profit.local';
}
