<?php
/**
 * Klasa Core do obslugi modulow.
 * Zarzadzanie modulami jest dozwolone tylko dla
 * superuzytkownika
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Core {

	// komunikat
	var $sMsg;

	// Id wersji jezykowej
	var $iLangId;

	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];

		$sAction = '';
		$iId = 0;

		if (isset($_GET['do'])) {
			$sAction = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sAction = $_POST['do'];
		}
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}

		if ($_SESSION['user']['type'] == 1) {
			switch ($sAction) {
				case 'delete': $this->DeleteModules($pSmarty, $iId); break;
				case 'show': $this->ShowModules($pSmarty); break;
				case 'insert': $this->InsertModule($pSmarty); break;
				case 'update': $this->UpdateModule($pSmarty); break;
				case 'edit': $this->AddEditModule($pSmarty, $iId); break;
				case 'add': $this->AddEditModule($pSmarty); break;

              case 'sort':
                // dolaczenie pliku klasy MySort
                include_once('Sort.class.php');
                // obiekt klasy MySort
                $oSort = new MySort();
                // utwozenie widoku do sortowania
                $pSmarty->assign('sContent',
                    $oSort->Show('',
                        $this->getRecords(),
                        phpSelf(array('do'=>'proceed_sort'),
                            array(), false)));
                break;
              case 'proceed_sort':
                // dolaczenie pliku klasy MySort
                include_once('Sort.class.php');
                // obiekt klasy MySort
                $oSort = new MySort();
                // sortowanie rekordow
                $oSort->Proceed($aConfig['tabls']['prefix']."modules",
                    $this->getRecords(),
                    array(),
                    'order_by');
                break;

				default: $this->ShowModules($pSmarty); break;
			}
		}
		else {
			$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
		}
	} // end Core() function


    /**
     * Metoda pobiera liste rekordow
     *
     * @return	array ref	- lista rekordow
     */
    function &getRecords() {
      global $aConfig;

      // pobranie wszystkich polecanych produktu o $iId
      $sSql = "SELECT A.id, A.symbol
                           FROM modules A
                           ORDER BY A.order_by";
      $rows =  Common::GetAssoc($sSql);
      $records = [];
      foreach ($rows as $key => $row) {
          if (isset($aConfig['lang']['c_modules'][$row])) {
            $records[$key] = $aConfig['lang']['c_modules'][$row];
          }
      }
      return $records;
    } // end of getRecords() method


	/**
	 * Metoda usuwa wybrany(e) modul(y)
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID usuwanego modulu
	 * @return 	void
	 */
	function DeleteModules(&$pSmarty, $iId=0) {
		global $aConfig;
		$bIsErr = false;

		if ($iId > 0) {
			$_POST['delete'][$iId] = 'on';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();

			// pobranie symboli usuwanych modulow
			$sSql = "SELECT symbol
					 		 FROM ".$aConfig['tabls']['prefix']."modules
						 	 WHERE id IN (".implode(',', $_POST['delete']).")";
			$aModules =& Common::GetCol($sSql);
			if ($aModules === false) {
				$bIsErr = true;
			}
			else {
				foreach ($aModules as $sSymbol) {
					if (isset($aConfig['lang'][$_GET['file']][$sSymbol])) {
						$sSymbol = $aConfig['lang'][$_GET['file']][$sSymbol];
					}
					$sDel .= '"'.$sSymbol.'", ';
				}
				$sDel = substr($sDel, 0, -2);
			}

			if (!$bIsErr) {
				// usuwanie modulu(ow)
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."modules
								 WHERE id IN (".implode(',', $_POST['delete']).")";
				if (count($_POST['delete']) == 1) {
					$sErrPostfix = '0';
				}
				else {
					$sErrPostfix = '1';
				}
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_ok_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_err_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg);
			}
		}
		$this->ShowModules($pSmarty);
	} // end of DeleteModules() funciton

	/**
	 * Metoda dodaje do bazy danych nowy modul
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function InsertModule(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditModule($pSmarty);
			return;
		}
		// sprawdzenie czy modul o podanej nazwie nie istnieje w bazie danych
		if (!$this->ModuleExists($_POST['symbol'])) {
			$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."modules (
							 symbol,
							 menu,
							 box,
							 default_module,
							 one_per_lang,
							 cacheable,
							 exclude_from_cache
						  )
						  VALUES (
							 '".$_POST['symbol']."',
							 '".(isset($_POST['menu']) ? '1' : '0')."',
							 '".(isset($_POST['box']) ? '1' : '0')."',
							 '".(isset($_POST['default_module']) ? '1' : '0')."',
							 '".(isset($_POST['one_per_lang']) ? '1' : '0')."',
							 '".$_POST['cacheable']."',
							 ".(trim($_POST['exclude_from_cache']) != '' ? "'".trim($_POST['exclude_from_cache'])."'" : 'NULL')."
						  )";
			if (isset($aConfig['lang'][$_GET['file']][$_POST['symbol']])) {
				$_POST['gen_symbol'] = $_POST['symbol'];
				$_POST['symbol'] = $aConfig['lang'][$_GET['file']][$_POST['symbol']];
			}
			if (Common::Query($sSql) !== false) {
				// modul zostal dodany, wyswietlenie komunikatu o powodzeniu
				// oraz wyswietlenie listy modulow
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['module_add_ok'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg, false);
				AddLog($sMsg, false);
				// resetowanie stanu widoku
				$_GET['reset'] = 2;
				$this->ShowModules($pSmarty);
			}
			else {
				// modul nie zostal dodany, wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['module_add_err'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEditModule($pSmarty);
			}
		}
		else {
			// modul o podanym symbolu juz istnieje
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['module_add_err2'], $_POST['symbol']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEditModule($pSmarty);
		}
	} // end of InsertModule() funciton


	/**
	 * Metoda wprowadza zmiany w module do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateModule(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditModule($pSmarty);
			return;
		}

		// sprawdzenie czy modul o podanej nazwie nie istnieje w bazie danych
		if (!$this->ModuleExists($_POST['symbol'], $_GET['id'])) {
			Common::BeginTransaction();

			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."modules SET
								symbol = '".$_POST['symbol']."',
								menu = '".(isset($_POST['menu']) ? '1' : '0')."',
								box = '".(isset($_POST['box']) ? '1' : '0')."',
								default_module = '".(isset($_POST['default_module']) ? '1' : '0')."',
								one_per_lang = '".(isset($_POST['one_per_lang']) ? '1' : '0')."',
								cacheable = '".$_POST['cacheable']."',
								exclude_from_cache = ".(trim($_POST['exclude_from_cache']) != '' ? "'".trim($_POST['exclude_from_cache'])."'" : 'NULL')."
						   WHERE id=".$_GET['id'];

			if (isset($aConfig['lang'][$_GET['file']][$_POST['symbol']])) {
				$_POST['gen_symbol'] = $_POST['symbol'];
				$_POST['symbol'] = $aConfig['lang'][$_GET['file']][$_POST['symbol']];
			}
			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
			if (!$bIsErr) {
				if (trim($_POST['old_privs']) != '') {
					$aOldPrivs = explode(',', $_POST['old_privs']);
					if (!empty($_POST['privs'])) {
						$aDeletedPrivs = array_diff($aOldPrivs, $_POST['privs']);
					}
					else {
						$aDeletedPrivs = $aOldPrivs;
					}
					if (!empty($aDeletedPrivs)) {
						// usuniecie tych profili z uprawnien do modulu z tablicy uprawnien uzytkownikow
						// ktore zostaly wlasnie usuniete z modulu
						$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."uprivileges
										 WHERE module_id = ".$_GET['id']." AND
													 profile_id IN (".implode(',', $aDeletedPrivs).") AND
													 language_id = ".$this->iLangId;
						if (Common::Query($sSql) === false) {
							$bIsErr = true;
						}
					}
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				// modul zostal zmodyfikowany, wyswietlenie komunikatu o powodzeniu
				// oraz wyswietlenie listy modulow
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['module_edit_ok'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg, false);
				AddLog($sMsg, false);
				$this->ShowModules($pSmarty);
			}
			else {
				Common::RollbackTransaction();
				// nie wprowadzono zadnych zmian, wyswietlenie komunikatu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['module_edit_err'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEditModule($pSmarty, $_GET['id']);
			}
		}
		else {
			// modul o podanym symbolu juz istnieje
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['module_add_err2'], $_POST['symbol']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEditModule($pSmarty);
		}
	} // end of UpdateModule() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji modulu
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	ID edytowanego modulu
	 */
	function AddEditModule(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego modulu
			$sSql = "SELECT symbol, menu, box, default_module, cacheable,
							 exclude_from_cache, one_per_lang
							 FROM ".$aConfig['tabls']['prefix']."modules
							 WHERE id=".$iId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			if (!isset($aData['menu'])) {
				$aData['menu'] = '1';
			}
			if (!isset($aData['cacheable'])) {
				$aData['cacheable'] = '1';
			}
		}

		$sHeader = $aConfig['lang'][$_GET['file']]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['symbol']) && !empty($aData['symbol'])) {
			if (isset($aConfig['lang'][$_GET['file']][$aData['symbol']]) &&
					!empty($aConfig['lang'][$_GET['file']][$aData['symbol']])) {
				$sHeader .= ' "'.$aConfig['lang'][$_GET['file']][$aData['symbol']].'"';
			}
			else {
				$sHeader .= ' "'.$aData['symbol'].'"';
			}
		}

		// tworzenie formularza
		$pForm = new FormTable('c_modules', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>160), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));
		if ($iId > 0) {
			$pForm->AddHidden('old_privs', $aData['old_privs']);
		}
		$pForm->AddText('symbol', $aConfig['lang'][$_GET['file']]['symbol'], $aData['symbol'], array('maxlength'=>128, 'style'=>'width: 200px;'));

		// moze wystepowac w menu serwisu
		$pForm->AddCheckBox('menu', $aConfig['lang'][$_GET['file']]['menu'], array(), '', !empty($aData['menu']), false);

		// modul bloku
		$pForm->AddCheckBox('box', $aConfig['lang'][$_GET['file']]['box'], array(), '', !empty($aData['box']), false);

		// domyslny modul przy dodawaniu strony modulu
		$pForm->AddCheckBox('default_module', $aConfig['lang'][$_GET['file']]['default_module'], array(), '', !empty($aData['default_module']), false);

		// moze istniec tylko jedna strona modulu na wersje jezykowa
		$pForm->AddCheckBox('one_per_lang', $aConfig['lang'][$_GET['file']]['one_per_lang'], array(), '', !empty($aData['one_per_lang']), false);

		// cache'owanie stron modulu
		$aCache = array(
			array('value' => '0', 'label' => $aConfig['lang'][$_GET['file']]['cache_0']),
			array('value' => '1', 'label' => $aConfig['lang'][$_GET['file']]['cache_1']),
			array('value' => '2', 'label' => $aConfig['lang'][$_GET['file']]['cache_2']),
		);
		$pForm->AddSelect('cacheable', $aConfig['lang'][$_GET['file']]['cacheable'], array(), $aCache, $aData['cacheable'], '', false);

		$pForm->AddText('exclude_from_cache', $aConfig['lang'][$_GET['file']]['exclude_from_cache'], $aData['exclude_from_cache'], array('maxlength'=>255, 'style'=>'width: 200px;'), '', 'text', false);

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).
														 '&nbsp;&nbsp;'.
														 $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEditModule() function


	/**
	 * Metoda wyswietla liste zdefiniowanych dla serwisu modulow
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function ShowModules(&$pSmarty) {
		global $aConfig;
		$aModules = array();

		// zapamietanie opcji
		rememberViewState($_GET['file']);

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'header'	=> $aConfig['lang'][$_GET['file']]['modules_list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '20'
			),
			array(
				'db_field'	=> 'symbol',
				'content'	=> $aConfig['lang'][$_GET['file']]['module_name'],
				'sortable'	=> true
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		if (isset($_GET['search'])) {
			$_POST['search'] = $_GET['search'];
		}

		// pobranie liczby wszystkich modulow
		$sSql = "SELECT COUNT(*)
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND symbol LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($_GET['file']);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(*)
							 FROM ".$aConfig['tabls']['prefix']."modules
							 WHERE 1 = 1".
									 	 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND symbol LIKE \'%'.$_POST['search'].'%\'' : '');
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('modules', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich modulow dla danego serwisu
			$sSql = "SELECT id, symbol
							 FROM ".$aConfig['tabls']['prefix']."modules
							 WHERE TRUE".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND symbol LIKE \'%'.$_POST['search'].'%\'' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aModules =& Common::GetAll($sSql);

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'symbol' => array (
					'link'	=> phpSelf(array('do'=>'edit', 'id'=>'{id}')),
					'use_lang'	=> true
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params'	=> array (
												'edit'	=>	array('id' => '{id}'),
												'delete'	=>	array('id' => '{id}')
					)
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aModules, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('sort','add')
		);
		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of ShowModules() function


	/**
	 * Metoda sprawdza czy modul o podanym symbolu istnieje
	 *
	 * @param		string	$sSymbol	- symbol modulu
	 * @param		integer	$iId	- ID modulu dla ktorego pomijane jest sprawdzanie
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function ModuleExists($sSymbol, $iId=0) {
		global $aConfig;

		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE symbol='".$sSymbol."'".
									($iId > 0 ? " AND id!=".$iId : '');

		return intval(Common::GetOne($sSql)) == 1;
	} // end of ModuleExists() function
} //end of Core class
?>