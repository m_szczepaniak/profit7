<?php
/**
* Plik jezykowy dla interfejsu Core Modules Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'MODULY' --------------*/
$aConfig['lang']['c_modules']['modules_list']		= "Lista modułów";
$aConfig['lang']['c_modules']['module_name']		= "Symbol / nazwa modułu";
$aConfig['lang']['c_modules']['no_items']				= "Brak zdefiniowanych modułów";

$aConfig['lang']['c_modules']['edit']						= "Edytuj moduł";
$aConfig['lang']['c_modules']['delete']					= "Usuń moduł";
$aConfig['lang']['c_modules']['delete_q']				= "Czy na pewno usunąć wybrany moduł?";
$aConfig['lang']['c_modules']['check_all']			= "Zaznacz / odznacz wszystkie";
$aConfig['lang']['c_modules']['delete_all']			= "Usuń zaznaczone";
$aConfig['lang']['c_modules']['delete_all_q']		= "Czy na pewno usunąć wybrane moduły?";
$aConfig['lang']['c_modules']['delete_all_err']	= "Nie wybrano żadnego modułu do usunięcia!";
$aConfig['lang']['c_modules']['add']						= "Dodaj moduł";

$aConfig['lang']['c_modules']['header_0']				= "Dodawanie modułu";
$aConfig['lang']['c_modules']['header_1']				= "Edycja modułu";
$aConfig['lang']['c_modules']['symbol']					= "Symbol modułu";
$aConfig['lang']['c_modules']['prefix']					= "Prefix tabeli";
$aConfig['lang']['c_modules']['privs']					= "Możliwość ograniczenia dostępu do profili";
$aConfig['lang']['c_modules']['menu']						= "Może występować w menu serwisu";
$aConfig['lang']['c_modules']['box']						= "Może być modułem boksu";
$aConfig['lang']['c_modules']['default_module']	= "Moduł domyślny";
$aConfig['lang']['c_modules']['one_per_lang']		= "Tylko jedna strona modułu na wersję językową";

$aConfig['lang']['c_modules']['cacheable']	= "Moduł cache'owany";
$aConfig['lang']['c_modules']['exclude_from_cache']	= "Nie cache'uj opcji";
$aConfig['lang']['c_modules']['cache_0']	= "Nigdy";
$aConfig['lang']['c_modules']['cache_1']	= "Zawsze";
$aConfig['lang']['c_modules']['cache_2']	= "Nie gdy przesłano POST";

$aConfig['lang']['c_modules']['module_add_ok']	= "Moduł \"%s\" został dodany do bazy danych";
$aConfig['lang']['c_modules']['module_add_err']	= "Nie udało się dodać modułu \"%s\" do bazy danych!<br>Spróbuj ponownie";
$aConfig['lang']['c_modules']['module_add_err2']= "Moduł o podanym symbolu \"%s\" już istnieje!<br>Wybierz inny symbol";
$aConfig['lang']['c_modules']['del_ok_0']	= "Moduł %s został usunięty";
$aConfig['lang']['c_modules']['del_ok_1']	= "Moduły %s zostały usunięte";
$aConfig['lang']['c_modules']['del_err_0']	= "Nie udało się usunąć modułu %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_modules']['del_err_1']	= "Nie udało się usunąć modułów %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_modules']['module_edit_ok']	= "Zmiany w module \"%s\" zostały wprowadzone do bazy danych";
$aConfig['lang']['c_modules']['module_edit_err']= "Nie wprowadzono żadnych zmian w module \"%s\"";

$aConfig['lang']['c_modules']['sort'] = "Sortuj";
$aConfig['lang']['c_modules']['sort_items'] = "Sortowanie \"%s\"";

?>