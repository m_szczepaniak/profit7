<?php
/**
 * Klasa Core do obslugi kont uzytkownikow.
 * Zarzadzanie uzytkownikami jest dostepne tylko dla
 * superuzytkownika lub administratorow serwisow
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Core {
	
	// komunikat
	var $sMsg;
	
	// ID wersji jez
	var $iLangId;
	
	// symbol wersji jez
	var $sLang;
  
  // id aktualnego uzytkownika, jeśli jest na poziomie zwykłego użytkownika
  var $iNUId;

    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    private $pDbMgr;

	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;
        global $pDbMgr;


        $this->pDbMgr = $pDbMgr;
		
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sLang = $_SESSION['site']['sign'];
		
		$sDo = '';
		$iId = 0;
    $this->iNUId = 0;
		
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do']) && $_POST['do'] !== "") {
			$sDo = $_POST['do'];
		}
    
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia
		if ($_SESSION['user']['type'] == 0 || $_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {
			if ($_SESSION['user']['type'] == 0) {
        $this->iNUId = $_SESSION['user']['id'];
      }

      
			switch ($sDo) {
				case 'delete':
					if (isset($_GET['id'])) {
						$iId = $_GET['id'];
					}
					else {
						$iId = 0;
					}
					$this->DeleteUsers($pSmarty, $iId);
				break;
			
				case 'show':
					$this->ShowUsers($pSmarty);
				break;
			
				case 'insert':
					$this->InsertUser($pSmarty);
				break;
			
				case 'update':
					$this->UpdateUser($pSmarty);
				break;
			
				case 'edit':
					$this->AddEditUser($pSmarty, $_GET['id']);
				break;
			
				case 'add':
					$this->AddEditUser($pSmarty);
				break;
				
				case 'checkLogin':
					$this->CheckLogin();
				break;
						
				default:
					$this->ShowUsers($pSmarty);
				break;
			}
		}
		else {
			$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
		}
	} // end Core() function

	
	/**
	 * Metoda usuwa wybranych(ego) usytkownikow(a)
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID usuwanego uzytkownika
	 * @return 	void
	 */
	function DeleteUsers(&$pSmarty, $iId=0) {
		global $aConfig;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = 'on';
		}
		$iI = 0;
		if (isset($_POST['delete'])) {
			foreach($_POST['delete'] as $sKey => $sVal) {
				unset($_POST['delete'][$sKey]);
				// zabezpieczenie przed usunieciem zalogowanego uzytkownika
				if ($sKey != $_SESSION['user']['id']) {
					$_POST['delete'][$iI] = $sKey;
					$iI++;
				}
			}
		}
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			// pobranie loginow usuwanych uzytkownikow
			$sSql = "SELECT login
					 		 FROM ".$aConfig['tabls']['prefix']."users
						 	 WHERE id IN (".implode(',', $_POST['delete']).")";
			$aRecords =& Common::GetCol($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				foreach ($aRecords as $sItem) {
					$sDel .= '"'.$sItem.'", ';
				}
				$sDel = substr($sDel, 0, -2);
			}
			if (!$bIsErr) {
				// usuwanie uzytkownika(ow)
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."users
								 WHERE id IN (".implode(',', $_POST['delete']).")";
				if (count($_POST['delete']) == 1) {
					$sErrPostfix = '0';
				}
				else {
					$sErrPostfix = '1';
				}
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_ok_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_err_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie logu
				AddLog($sMsg);
			}
		}
		
		$this->ShowUsers($pSmarty);
	} // end of DeleteUsers() funciton
	
	/**
	 * Metoda dodaje do bazy danych nowy modul
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function InsertUser(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		$aMenusPrivs = array();
		$aPagesPrivs = array();
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditUser($pSmarty);
			return;
		}
		
		// jezeli wybranym typem uzytkownika jest zwykly uzytkownik
		// sprawdzenie czy zostaly wybrane prawa dostepu do przynajmniej jednego
		// menu lub strony
		if ((!isset($_POST['utype']) || $_POST['utype'] == '0') && ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2)) {
			
			if ($bIsErr) {
				$this->sMsg = GetMessage($aConfig['lang'][$_GET['file']]['no_privs_err']);
				$this->AddEditUser($pSmarty, $_GET['id']);
				return;
			}
			// sprawdzenie czy uzytkownik ma prawa do zarzadzania struktura menu
			// i jezeli tak to czy wybrano moduly do ktorych posiada dostep
			if (empty($_POST['umodules'])) {
				$this->sMsg = GetMessage($aConfig['lang'][$_GET['file']]['no_mods_err']);
				$this->AddEditUser($pSmarty, $_GET['id']);
				return;
			}
		}
		
		// sprawdzenie czy uzytkownik o podanym loginie nie istnieje w bazie danych
		if (!$this->UserExists($_POST['login'])) {
			Common::BeginTransaction();
			$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."users (
							 scale_access,
							 login,
							 passwd,
               code_passwd,
							 name,
							 surname,
							 email,
							 description,
							 active,
               priv_commercial_data,
               priv_address_data,
							 priv_order_status,
							 priv_password,
               priv_reviews,
               priv_order_5_status,
               manage_get_ready_send,
               continue_session,
               ip_address,
							 utype,
							 created,
							 created_by
						  )
						  VALUES (
						     '".($_POST['scale_access'] != '1' ? '0' : '1')."',
							 '".$_POST['login']."',
							 '".EncodePasswd($_POST['n_passwd'])."',
               '".$_POST['code_passwd']."',
							 '".$_POST['name']."',
							 '".$_POST['surname']."',
							 '".$_POST['email']."',
							 ".(!empty($_POST['description']) ? "'".$_POST['description']."'" : 'NULL').",
							 '".$_POST['active']."',
               '".($_POST['priv_commercial_data'] != '1' ? '0' : '1')."',
							 '".($_POST['priv_address_data'] != '1' ? '0' : '1')."',
               '".($_POST['priv_order_status'] != '1' ? '0' : '1')."',
							 '".($_POST['priv_password'] != '1' ? '0' : '1')."',
               '".($_POST['priv_reviews'] != '1' ? '0' : '1')."',  
               '".($_POST['priv_order_5_status'] != '1' ? '0' : '1')."',  
               '".($_POST['manage_get_ready_send'] != '1' ? '0' : '1')."',  
               '".($_POST['continue_session'] != '1' ? '0' : '1')."',  
               '".$_POST['ip_address']."',
							 '".(isset($_POST['utype']) ? $_POST['utype'] : 0)."',
							 NOW(),
							 '".$_SESSION['user']['name']."'
						  )";
			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
			if (!$bIsErr) {
				// uzytkownik zostal dodany, dodanie jego uprawnien
				// pobranie ID dodanego uzytkownika
				$iUserId = Common::GetLastInsertedID();
				
				if (!isset($_POST['utype']) || $_POST['utype'] == '0') {
					// dodawany uzytkownik jest zwyklym uzytkownikiem,
					// dodanie uprawnien do poszczegolnych menu i stron serwisu
					// uprawnienia tworzenia elementow
					
				
					if (!$bIsErr) {
						if (!empty($_POST['umodules'])) {
							foreach ($_POST['umodules'] as $iUModId) {
								$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."umodules
												 (
												 	user_id,
												 	module_id
												 )
												 VALUES (
												 	".$iUserId.",
												 	".$iUModId."
												 )";
								if (Common::Query($sSql) === false) {
									$bIsErr = true;
								}
							}
						}
					}
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['user_add_ok'], $_POST['login']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg, false);
				// resetowanie ustawien widoku
				$_GET['reset'] = 2;
				$this->ShowUsers($pSmarty);
			}
			else {
				// uzytkownik nie zostal dodany, zakonczenie transakcji,
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['user_add_err'], $_POST['login']);
				$this->sMsg = GetMessage($sMsg);
				// dodanie logu
				AddLog($sMsg);
				$this->AddEditUser($pSmarty);
				return;
			}
		}
		else {
			// uzytkownik o podanym loginie juz istnieje
			// zakonczenie trasakcji, wyswietlenie komunikatu oraz
			// ponowne wyswietlenie formularza
			Common::RollbackTransaction();
			$this->sMsg = GetMessage(sprintf($aConfig['lang'][$_GET['file']]['user_add_err2'], $_POST['login']));
			$this->AddEditUser($pSmarty);
		}
	} // end of InsertUser() funciton
	
	
	/**
	 * Metoda wprowadza zmiany w uzytkowniku do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateUser(&$pSmarty) {
		global $aConfig;
		$aMenusPrivs = array();
		$aPagesPrivs = array();
		$bIsErr = false;
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditUser($pSmarty, $_GET['id']);
			return;
		}
		// jezeli wybranym typem uzytkownika jest zwykly uzytkownik
		// sprawdzenie czy zostaly wybrane prawa dostepu do przynajmniej jednego
		// menu lub strony
		if ((!isset($_POST['utype']) || $_POST['utype'] == '0') && ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2)) {
			
			if ($bIsErr) {
				$this->sMsg = GetMessage($aConfig['lang'][$_GET['file']]['no_privs_err']);
				$this->AddEditUser($pSmarty, $_GET['id']);
				return;
			}
			// sprawdzenie czy uzytkownik ma prawa do zarzadzania struktura menu
			// i jezeli tak to czy wybrano moduly do ktorych posiada dostep
			if (empty($_POST['umodules'])) {
				$this->sMsg = GetMessage($aConfig['lang'][$_GET['file']]['no_mods_err']);
				$this->AddEditUser($pSmarty, $_GET['id']);
				return;
			}
		}
		
		Common::BeginTransaction();
    if($_SESSION['user']['type'] != 0) {
        $sSql = "UPDATE ".$aConfig['tabls']['prefix']."users SET
                        ".(!empty($_POST['n_passwd']) ? "passwd='".EncodePasswd($_POST['n_passwd'])."'," : '')."
                        name='".$_POST['name']."',
                        surname='".$_POST['surname']."',
                        email='".$_POST['email']."',";
        if ($_GET['id'] != $_SESSION['user']['id']) {
          $sSql .= "
                        description=".(!empty($_POST['description']) ? "'".$_POST['description']."'" : 'NULL').",
                        active='".$_POST['active']."',";
        }
          $sSql .= "
                        ".(!empty($_POST['code_passwd']) ? " code_passwd='".$_POST['code_passwd']."'," : '')."
                        priv_address_data = '".($_POST['priv_address_data'] != '1' ? '0' : '1')."',
                        priv_commercial_data = '".($_POST['priv_commercial_data'] != '1' ? '0' : '1')."',
                        priv_order_status = '".($_POST['priv_order_status'] != '1' ? '0' : '1')."',
                        priv_password = '".($_POST['priv_password'] != '1' ? '0' : '1')."',
                        priv_reviews = '".($_POST['priv_reviews'] != '1' ? '0' : '1')."',
                        scale_access = '".($_POST['scale_access'] != '1' ? '0' : '1')."',
                        priv_order_5_status = '".($_POST['priv_order_5_status'] != '1' ? '0' : '1')."',
                        manage_get_ready_send = '".($_POST['manage_get_ready_send'] != '1' ? '0' : '1')."',
                        continue_session = '".($_POST['continue_session'] != '1' ? '0' : '1')."',
                        ip_address = '".$_POST['ip_address']."',
                        utype='".(isset($_POST['utype']) ? $_POST['utype'] : 0)."',
                        modified=NOW(),
                        modified_by='".$_SESSION['user']['name']."'
                 WHERE id=".$_GET['id'];
    } else {
        $sSql = "UPDATE ".$aConfig['tabls']['prefix']."users SET
                  ".(!empty($_POST['n_passwd']) ? "passwd='".EncodePasswd($_POST['n_passwd'])."'," : '')."
                 modified=NOW(),
                 scale_access = '".($_POST['scale_access'] != '1' ? '0' : '1')."',
                 ".(!empty($_POST['code_passwd']) ? " code_passwd='".$_POST['code_passwd']."'," : '')."
                 modified_by='".$_SESSION['user']['name']."'
                 WHERE id=".$_GET['id'];   
    }
		if (Common::Query($sSql) === false) {
			$bIsErr = true;
		}
		if (!$bIsErr && (!isset($_POST['utype']) || $_POST['utype'] == '0') && $_SESSION['user']['type'] != 0) {
			// edytowany uzytkownik jest zwyklym uzytkownikiem, edycja uprawnien
			
			
			if (!$bIsErr) {
				// usuniecie starych ustawien modulow
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."umodules
								 WHERE user_id=".$_GET['id'];
				Common::Query($sSql);
				if (!empty($_POST['umodules'])) {
					foreach ($_POST['umodules'] as $iUModId) {
						$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."umodules
										 (
										 	user_id,
										 	module_id
										 )
										 VALUES (
										 	".$_GET['id'].",
										 	".$iUModId."
										 )";
						if (Common::Query($sSql) === false) {
							$bIsErr = true;
						}
					}
				}
			}
		}
		
		if (!$bIsErr) {
			// ustawienia uzytkownika zostaly zmienione, zstwierdzenie transakcji
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['user_edit_ok'], $_POST['login']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie logu
			AddLog($sMsg, false);
			$this->ShowUsers($pSmarty);
		}
		else {
			// wystapil blad podczas edycji uzytkownika, zakonczenie transakcji,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['user_edit_err'], $_POST['login']);
			$this->sMsg = GetMessage($sMsg);
			// dodanie logu
			AddLog($sMsg);
			$this->AddEditUser($pSmarty, $_GET['id']);
		}
	} // end of UpdateUser() funciton
	
	
	/**
	 * Metoda tworzy formularz dodawania / edycji uzytkownika
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	ID edytowanego uzytkownika
	 */
	function AddEditUser(&$pSmarty, $iId=0) {
		global $aConfig;
		
		$aData = array();
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if ($iId > 0) {
			// pobranie z bazy danych edytowanego uzytkownika
			$sSql = "SELECT login, name, surname, email, description, active, utype, priv_address_data, priv_commercial_data, priv_order_status, priv_password, priv_reviews, priv_order_5_status, manage_get_ready_send, continue_session, ip_address, scale_access
							 FROM ".$aConfig['tabls']['prefix']."users
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
			if ($aData['utype'] == '0') {
				// usytkownik jest zwyklym uzytkownikiem, sformowanie tablicy z uprawnieniami
				// do serwisow i ich modulow
				
				// pobranie modulow ktorych strony moze tworzyc uzytkownik
				$sSql = "SELECT module_id
								 FROM ".$aConfig['tabls']['prefix']."umodules
								 WHERE user_id = ".$iId;
				$aData['umodules'] =& Common::GetCol($sSql);
			}
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		
		}

		$sHeader = $aConfig['lang'][$_GET['file']]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['login']) && !empty($aData['login'])) {
			$sHeader .= ' "'.$aData['login'].'"';
		}
		
		// tworzenie formularza
		$aLoginParams = array (
			'maxlength'=>16,
			'style'=>'width: 100px;'
		);
		
		$pForm = new FormTable('c_users', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>160), false);
		
		if ($iId > 0) {
			// edycja uzytkownika - nie mozna zmienic loginu
			$aLoginParams['readonly'] = 1;
		}
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['login_passwd'], array('class'=>'merged'));
		$pForm->AddText('login', $aConfig['lang'][$_GET['file']]['user_name'], $aData['login'], $aLoginParams, $aConfig['lang'][$_GET['file']]['login_err'], 'text', true, $aConfig['common']['login']['pcre']);
		$pForm->AddPassword('n_passwd', $aConfig['lang']['common']['passwd'], $_POST['n_passwd'], array('style'=>'width: 100px;'), '', ($iId > 0 ? false : true));
		$pForm->AddConfirmPassword('n_passwd2', $aConfig['lang']['common']['passwd2'], $_POST['n_passwd'], 'n_passwd', array('style'=>'width: 100px;'), $aConfig['lang']['common']['passwd2_err'], ($iId > 0 ? false : true));
		$pForm->AddCheckBox('scale_access', _('Waga'),array(),'', $aData['scale_access'],false);
    
    // tylko superuser lub administrator moze modyfikować te parametry
		if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {
      // nadanie statusu, osoby pakującej
      $pForm->AddCheckBox('priv_order_5_status', $aConfig['lang'][$_GET['file']]['allow_priv_order_5_status'],array('onchange'=>'ChangeObjValue(\'do\', \'edit\'); this.form.submit();'),'', $aData['priv_order_5_status'],false);
    }
    
    if ($aData['priv_order_5_status'] == '1') {
      // nadanie statusu, osoby pakującej
      $pForm->AddText('code_passwd', $aConfig['lang'][$_GET['file']]['priv_code_passwd'], $aData['code_passwd'], array('maxlength'=>6, 'style'=>'width: 60px;'), '', 'uint', false, '/\d{6}/');
    }
    
    if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {
      $pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['user_data'], array('class'=>'merged'));
      $pForm->AddText('name', $aConfig['lang'][$_GET['file']]['name'], $aData['name'], array('maxlength'=>20, 'style'=>'width: 150px;'));
      $pForm->AddText('surname', $aConfig['lang'][$_GET['file']]['surname'], $aData['surname'], array('maxlength'=>30, 'style'=>'width: 200px;'));
      $pForm->AddText('email', $aConfig['lang']['common']['email'], $aData['email'], array('maxlength'=>75, 'style'=>'width: 250px;'), '', 'email');
    }
    
		if ($iId != $_SESSION['user']['id']) {
			// jezeli aktualizacja zalogowanego uzytkownika, wylaczenie
			// mozliwosci zmiany opisu oraz stanu aktywnosci konta
			$pForm->AddText('description', $aConfig['lang'][$_GET['file']]['user_desc'], $aData['description'], array('maxlength'=>200, 'style'=>'width: 350px;'), '', 'text', false);
			$aYesNo = array (
				array ('value'=>'1', 'name'=>$aConfig['lang']['common']['yes']),
				array ('value'=>'0', 'name'=>$aConfig['lang']['common']['no'])
			);
			$pForm->AddSelect('active', $aConfig['lang'][$_GET['file']]['user_active'], array(), $aYesNo, $aData['active'], '', false);
		}
		
		// tylko superuser lub administrator moze modyfikować te parametry
		if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {
      
      $pForm->AddMergedRow(_('Zaawansowane'), array('class'=>'merged'));
      
      // dane adresowe
      $pForm->AddCheckBox('priv_address_data', $aConfig['lang'][$_GET['file']]['priv_address_data'],array(),'', $aData['priv_address_data'],false);
      
      // dane adresowe
      $pForm->AddCheckBox('priv_commercial_data', $aConfig['lang'][$_GET['file']]['priv_commercial_data'],array(),'', $aData['priv_commercial_data'],false);
      
			// modyfikacja dostępu do zmiany statusu zamówienia
			$pForm->AddCheckBox('priv_order_status', $aConfig['lang'][$_GET['file']]['allow_priv_status'],array(),'', $aData['priv_order_status'],false);

			// modyfikacja dostępu do wyświetlania haseł użytkowników w CMS
			$pForm->AddCheckBox('priv_password', $aConfig['lang'][$_GET['file']]['allow_priv_password'],array(),'', $aData['priv_password'],false);
      
      // modyfikacja dostępu do wyświetlania haseł użytkowników w CMS
			$pForm->AddCheckBox('priv_reviews', $aConfig['lang'][$_GET['file']]['allow_priv_reviews'],array(),'', $aData['priv_reviews'],false);
      
      // nie wylogowuj usera
      $pForm->AddCheckBox('continue_session', _('Nie wylogowuj konta'), array(), '', $aData['continue_session'],false);
      
      // nadanie statusu, osoby pakującej
      $pForm->AddCheckBox('manage_get_ready_send', _('Zarządzanie parametrami pobieranej listy do zebrania') ,array(),'', $aData['manage_get_ready_send'],false);
      
      // ograniczenie adresu ip - ip_address
      $pForm->AddText('ip_address', _('Ogranicz możliwość logowania się do wymienionych adresów IP'), $aData['ip_address'], array('style' => 'width: 400px;', 'maxlength' => '140'), '', 'text', false);
      $pForm->AddRow('', _(' Uwaga jeśli chcesz podać wiecej adresów rozdziel je wylącznie ","'));
		}
    
			
		if ($_SESSION['user']['type'] == 1) {
			$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['user_type'], array('class'=>'merged'));
			$aUserTypes = array (
				array ('value'=>'0', 'name'=>$aConfig['lang'][$_GET['file']]['normal_user']),
				array ('value'=>'2', 'name'=>$aConfig['lang'][$_GET['file']]['admin']),
        array ('value'=>'1', 'name'=> _('Superadmin'))
			);
			$pForm->AddSelect('utype', $aConfig['lang'][$_GET['file']]['user_type'], array('onchange'=>'ChangeObjValue(\'do\', \''.($iId>0?'edit':'add').'\'); this.form.submit();'), $aUserTypes, $aData['utype'], '', false);
		}
		elseif ($iId > 0) {
			$pForm->AddHidden('utype', $aData['utype']);
		}
				
    if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {
      if (!isset($aData['utype']) || (isset($aData['utype']) && $aData['utype'] == '0')) {
        $sJS = '';
        // dodawanym typem uzytkownika jest uzytkownik 'zwykly'
        // wyswietlenie listy stron i profili dostepu
        // do ktorych moga zostac nadane uzytkownikowi prawa


        $pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['modules_access'], array('class'=>'merged'));
        $pForm->AddSelect('umodules', $aConfig['lang'][$_GET['file']]['user_modules'], array('size' => 5, 'multiple' => 'multiple'), GetAllModulesList(), $aData['umodules'], '', false);
      }
    }
    
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).
														 '&nbsp;&nbsp;'.
														 $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	} // end of AddEditUser() function
	
	
	/**
	 * Metoda wyswietla liste zdefiniowanych dla serwisu usytkownikow
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function  ShowUsers(&$pSmarty) {
		global $aConfig;
		$aUsers = array();
		
		// zapamietanie opcji
		rememberViewState($_GET['file']);
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		
		$aHeader = array(
			'header'	=> $aConfig['lang'][$_GET['file']]['users_list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '30'
			),
			array(
				'db_field'	=> 'login',
				'content'	=> $aConfig['lang'][$_GET['file']]['user_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'surname',
				'content'	=> $aConfig['lang'][$_GET['file']]['surname'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$_GET['file']]['name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'utype',
				'content'	=> $aConfig['lang'][$_GET['file']]['administrator'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'active',
				'content'	=> $aConfig['lang'][$_GET['file']]['active'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang'][$_GET['file']]['created'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang'][$_GET['file']]['created_by'],
				'sortable'	=> true
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);
		
		if (isset($_GET['f_type'])) {
			$_POST['f_type'] = $_GET['f_type'];
		}
		if (isset($_GET['search'])) {
			$_POST['search'] = $_GET['search'];
		}
    
    $sSqlAdd = '';
    if ($this->iNUId > 0) {
      $sSqlAdd = ' AND id = '.$this->iNUId.' ';
    }
		
		$sSql = "SELECT DISTINCT id 
						 FROM ".$aConfig['tabls']['prefix']."users
						 WHERE 1=1 ".$sSqlAdd.
						 			 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND (login LIKE \'%'.$_POST['search'].'%\' OR surname LIKE \'%'.$_POST['search'].'%\' OR name LIKE \'%'.$_POST['search'].'%\')' : '').
									 (isset($_POST['f_type']) && $_POST['f_type'] != '-1' ? ' AND utype= \''.$_POST['f_type'].'\'' : '').
                   (isset($_POST['f_active']) && $_POST['f_active'] != '' ? ' AND active= \''.$_POST['f_active'].'\'' : '');
		$iRowCount = count(Common::GetAll($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($_GET['file']);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT DISTINCT id 
							 FROM ".$aConfig['tabls']['prefix']."users
							 WHERE 1=1 ".$sSqlAdd.
							 			 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND (login LIKE \'%'.$_POST['search'].'%\' OR surname LIKE \'%'.$_POST['search'].'%\' OR name LIKE \'%'.$_POST['search'].'%\')' : '').
										 (isset($_POST['f_type']) && $_POST['f_type'] != '-1' ? ' AND utype= \''.$_POST['f_type'].'\'' : '').
                     (isset($_POST['f_active']) && $_POST['f_active'] != '' ? ' AND active= \''.$_POST['f_active'].'\'' : '');
			$iRowCount = intval(Common::GetOne($sSql));
		}
		
		$pView = new View('users', $aHeader, $aAttribs);
		
//		if ($_SESSION['user']['type'] == '1') {
			// typy uzytkownikow - na potrzeby filtrow - tylko gdy zalogowany jest superuser
    $aUserTypes = array (
      array('value'=>'-1', 'name'=>$aConfig['lang'][$_GET['file']]['all_users']),
      array('value'=>'0', 'name'=>$aConfig['lang'][$_GET['file']]['normal_users']),
      array('value'=>'2', 'name'=>$aConfig['lang'][$_GET['file']]['admins'])
    );
    $pView->AddFilter('f_type', $aConfig['lang'][$_GET['file']]['user_types'], $aUserTypes, $_POST['f_type']);
//		}
    
    $aActiveList = array (
      array('value'=>'1', 'name'=> _('Aktywny')),
      array('value'=>'0', 'name'=> _('Nieaktywny'))
    );
    $pView->AddFilter('f_active', _('Konto aktywne'), addDefaultValue($aActiveList), $_POST['f_active']);
    
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
			// pobranie wszystkich uzytkownikow dla danego serwisu
			$sSql = "SELECT DISTINCT id, login, surname, name, utype, active, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."users
							 WHERE 1=1 ".$sSqlAdd.
							 			 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND (login LIKE \'%'.$_POST['search'].'%\' OR surname LIKE \'%'.$_POST['search'].'%\' OR name LIKE \'%'.$_POST['search'].'%\')' : '').
							 			 (isset($_POST['f_type']) && $_POST['f_type'] != '-1' ? ' AND utype= \''.$_POST['f_type'].'\'' : '').
                     (isset($_POST['f_active']) && $_POST['f_active'] != ''? ' AND active= \''.$_POST['f_active'].'\'' : '').
							 ' ORDER BY '.
							 (isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : ' DESC')."
							 LIMIT ".$iStartFrom.", ".$iPerPage;
			$aUsers =& Common::GetAll($sSql);
			foreach ($aUsers as $iKey => $aUser) {
				if (intval($aUsers[$iKey]['utype']) == 0) {
					$aUsers[$iKey]['utype'] = $aConfig['lang']['common']['no'];
				}
				else {
					$aUsers[$iKey]['utype'] = $aConfig['lang']['common']['yes'];
				}
				if (intval($aUsers[$iKey]['active']) == 0) {
					$aUsers[$iKey]['active'] = $aConfig['lang']['common']['no'];
				}
				else {
					$aUsers[$iKey]['active'] = $aConfig['lang']['common']['yes'];
				}
			}
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'disabled'	=> array(
					'show'	=> false
				),
				'login' => array (
					'link'	=> phpSelf(array('do'=>'edit', 'id'=>'{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params'	=> array (
												'edit'	=>	array('id' => '{id}'),
												'delete'	=>	array('id' => '{id}')
					)
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aUsers, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'add_to_group'),
			array('users_groups', 'add')
		);

		$aFooterParams = array(
		    'users_groups' => array(array('file' => null, 'module' => 'm_konta', 'action' => 'groups')),
		    'add_to_group' => array(array('do' => 'addToGroup', 'file' => null, 'module' => 'm_konta', 'action' => 'groups'))
        );

		$pView->AddRecordsFooter($aRecordsFooter, $aFooterParams);
				
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of ShowUsers() function
	
	
	/**
	 * Metoda sprawdza na potrzeby wywolania AJAX czy uzytkownik o podanym loginie istnieje
	 * Wypisuje '1' jezeli istnieje; '0' jezeli nie istnieje
	 *
	 * @return	void
	 */
	function CheckLogin() {
		// ustawienie zmiennej $_GET['hideHeader'] aby nie byly wysylane zadne naglowki
		// i zeby nie byl parsowany szablon index.tpl
		$_GET['hideHeader'] = 1;
		echo intval($this->UserExists($_GET['login'], $_GET['id']));
	} // end of UserExists() function
	
	
	/**
	 * Metoda sprawdza czy uzytkownik o podanym loginie istnieje
	 *
	 * @param		string	$sLogin	- login uzytkownika
	 * @param		integer	$iId	- ID uzytkownika dla ktorego pomijane jest sprawdzanie
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function UserExists($sLogin, $iId=0) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."users
						 WHERE login = '".$sLogin."'".
						 			 ($iId > 0 ? " AND id!=".$iId : '');
		
		return Common::GetOne($sSql) > 0;
	} // end of UserExists() function

} //end of Core class
?>