<?php
/**
* Plik jezykowy dla interfejsu Core Users Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'UZYTKOWNICY' --------------*/
$aConfig['lang']['c_users']['users_list']			= "Lista użytkowników";
$aConfig['lang']['c_users']['user_name']			= "Nazwa użytkownika";
$aConfig['lang']['c_users']['check_name']			= "Sprawdź nazwę";
$aConfig['lang']['c_users']['checking_login']	= "Trwa sprawdzanie...";
$aConfig['lang']['c_users']['login_check_err']= "Wystąpił błąd podczas sprawdzania";
$aConfig['lang']['c_users']['login_exists_0']	= "Użytkownik o podanym loginie nie istnieje";
$aConfig['lang']['c_users']['login_exists_1']	= "Użytkownik o podanym loginie już istnieje!";
$aConfig['lang']['c_users']['surname']				= "Nazwisko";
$aConfig['lang']['c_users']['name']						= "Imię";
$aConfig['lang']['c_users']['created']				= "Utworzono";
$aConfig['lang']['c_users']['created_by']			= "Utworzył";
$aConfig['lang']['c_users']['administrator']	= "Administrator";
$aConfig['lang']['c_users']['active']					= "Aktywne";
$aConfig['lang']['c_users']['no_items']				= "Brak zdefiniowanych użytkowników";

$aConfig['lang']['c_users']['user_types']			= "Typ użytkowników";
$aConfig['lang']['c_users']['all_users']			= "Wszyscy użytkownicy";

$aConfig['lang']['c_users']['all_websites']		= "Wszystkie serwisy";
$aConfig['lang']['c_users']['f_website_id']		= "Użytkownicy serwisu";
$aConfig['lang']['c_users']['f_user_type']		= "Typ użytkowników:";

$aConfig['lang']['c_users']['edit']						= "Edytuj użytkownika";
$aConfig['lang']['c_users']['delete']					= "Usuń użytkownika";
$aConfig['lang']['c_users']['delete_q']				= "Czy na pewno usunąć wybranego użytkownika?";
$aConfig['lang']['c_users']['check_all']			= "Zaznacz / odznacz wszystkich";
$aConfig['lang']['c_users']['delete_all']			= "Usuń zaznaczonych";
$aConfig['lang']['c_users']['delete_all_q']		= "Czy na pewno usunąć wybranych użytkowników?";
$aConfig['lang']['c_users']['delete_all_err']	= "Nie wybrano żadnego użytkownika do usunięcia!";
$aConfig['lang']['c_users']['add']						= "Dodaj użytkownika";
$aConfig['lang']['c_users']['users_groups']						= "Edytuj grupy użytkowników";
$aConfig['lang']['c_users']['add_to_group']						= "Dodaj do grupy użytkowników";
$aConfig['lang']['c_users']['add_to_group_q']						= "Dodać do grupy użytkowników";

$aConfig['lang']['c_users']['header_0']				= "Dodawanie użytkownika";
$aConfig['lang']['c_users']['header_1']				= "Edycja użytkownika";

$aConfig['lang']['c_users']['login_passwd']		= "Login i hasło";
$aConfig['lang']['c_users']['user_type']			= "Typ użytkownika";
$aConfig['lang']['c_users']['normal_user']		= "Zwykły użytkownik";
$aConfig['lang']['c_users']['admin']					= "Administrator";
$aConfig['lang']['c_users']['normal_users']		= "Zwykli użytkownicy";
$aConfig['lang']['c_users']['admins']					= "Administratorzy";

$aConfig['lang']['c_users']['user_data']			= "Dane użytkownika i parametry konta";
$aConfig['lang']['c_users']['user_desc']			= "Opis użytkownika";
$aConfig['lang']['c_users']['user_active']		= "Konto aktywne";
$aConfig['lang']['c_users']['priv_address_data'] = "Zezwól na wyświetlanie danych adresowych";
$aConfig['lang']['c_users']['priv_commercial_data'] = "Zezwól na wyświetlanie danych handlowych";
$aConfig['lang']['c_users']['allow_priv_status'] = "Zezwól na zmianę statusów zamówienia";
$aConfig['lang']['c_users']['allow_priv_password'] = "Zezwól na wyświetlanie haseł użytkowników";
$aConfig['lang']['c_users']['allow_priv_reviews'] = "Zezwól na wyświetlanie wszystkich zamówień do sprawdzenia";
$aConfig['lang']['c_users']['allow_priv_order_5_status'] = "Osoba pakująca przesyłki";
$aConfig['lang']['c_users']['priv_code_passwd'] = "Kod autoryzacyjny";

$aConfig['lang']['c_users']['websites_access'] = "Dostęp do serwisów";
$aConfig['lang']['c_users']['pages_access'] = "Dostęp do menu / stron";
$aConfig['lang']['c_users']['modules_access'] = "Dostęp do modułów";
$aConfig['lang']['c_users']['user_modules'] = "Moduły";

$aConfig['lang']['c_users']['login_err']			= "Login powinien się składać z małych lub dużych liter oraz znaku podkreślenia bez znaków narodowych i mieć długość od 3 do 16 znaków!";
$aConfig['lang']['c_users']['user_add_ok']		= "Użytkownik \"%s\" został dodany do bazy danych";
$aConfig['lang']['c_users']['user_add_err']		= "Nie udało się dodać użytkownika \"%s\" do bazy danych!<br>Spróbuj ponownie";
$aConfig['lang']['c_users']['user_add_err2']	= "Użytkownik o loginie \"%s\" już istnieje!<br>Wybierz inny login";
$aConfig['lang']['c_users']['no_privs_err']	= "Nie wybrano żadnych praw dostępu do menu lub stron!";
$aConfig['lang']['c_users']['no_mods_err']	= "Nie wybrano żadnego modułu, którego strony może tworzyć użytkownik!";
$aConfig['lang']['c_users']['del_ok_0']		= "Użytkownik %s został usunięty";
$aConfig['lang']['c_users']['del_ok_1']		= "Użytkownicy %s zostali usunięci";
$aConfig['lang']['c_users']['del_err_0']		= "Nie udało się usunąć użytkownika %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_users']['del_err_1']		= "Nie udało się usunąć użytkowników %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_users']['user_edit_ok']		= "Zmiany w ustawieniach użytkownika \"%s\" zostały wprowadzone do bazy danych";
$aConfig['lang']['c_users']['user_edit_err']	= "Nie udało się wprowadzić zmian w ustawieniach użytkownika \"%s\"!<br>Spróbuj ponownie";

$aConfig['lang']['c_users']['item']	= "Menu / strona";
$aConfig['lang']['c_users']['creator']	= "Prawo tworzenia / edycji / usuwania elementów";
$aConfig['lang']['c_users']['publisher']	= "Prawo publikowania elementów";
$aConfig['lang']['c_users']['users_only']	= "Prawo do elementów tylko tworzonych przez Użytkownika";
$aConfig['lang']['c_users']['recursive']	= "Zastosuj do podstron";
?>
