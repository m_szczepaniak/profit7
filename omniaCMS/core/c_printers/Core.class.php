<?php
/**
 * Klasa zarządzania drukarkami na aktualnym stanowisku,
 *  drukarki oparte są na cookie + config na stanowisku do którego podłączona jest drukarka, 
 *  może to być także drukarka sieciowa.
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-24 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Core {

	// komunikat
	var $sMsg;

	// ID wersji jezykowej serwisu
	var $iLangId;

	// nazwa modulu
	var $sModule;
  
  function __construct($pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];

		$sDo = '';
		$iId = 0;

		$this->sModule = $_GET['file'];

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}


		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia
		if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {

			switch ($sDo) {
				case 'update':
					$this->UpdateSettings($pSmarty);
				break;

				default:
					$this->Settings($pSmarty);
				break;
			}
		}
		else {
			$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
		}
  }
  
  
  /**
   * Metoda ustawia nazwę drukarki
   * 
   * @param object $pSmarty
   * @return void
   */
  private function UpdateSettings(&$pSmarty) {

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Settings($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Settings($pSmarty);
			return;
		}
    
    AddCookie("printer_labels", $_POST['printer_labels'], time() + 60*60*24*366);
    AddCookie("printer_invoices", $_POST['printer_invoices'], time() + 60*60*24*366);
    
    // dodano adresy
    $sMsg = sprintf(_('Ustawiono nazwy drukarek na tym komputerze'));
    $this->sMsg = GetMessage($sMsg, false);
    AddLog($sMsg, false);
    $this->Settings($pSmarty);
  }// end of UpdateSettings() method


	/**
	 * Metoda tworzy formularz edycji konfiguracji aktualnosci
	 *
	 * @param		object	$pSmarty
	 */
	function Settings(&$pSmarty) {
		global $aConfig;

    $aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

    if (!empty($_POST)) {
      $aData =& $_POST;
    }
		elseif (!empty($_COOKIE)) {
			$aData =& $_COOKIE;
		}

		$pForm = new FormTable('import_statements', _('Zarządzaj drukarkami na tym komputerze'), array('action'=>phpSelf(), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');
    $pForm->AddText('printer_labels', _('Nazwa drukarki etykiet'), $aData['printer_labels'], array('maxlength' => 255), '', 'text', true);
    $pForm->AddText('printer_invoices', _('Nazwa drukarki faktur'), $aData['printer_invoices'], array('maxlength' => 255), '', 'text', true);
    
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Settings() function
}