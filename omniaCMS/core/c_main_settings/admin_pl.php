<?php
/**
* Plik jezykowy dla interfejsu Core Main Settings Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'KONFIGURACJA SERWISU' --------------*/
$aConfig['lang']['c_main_settings']['header']				= "Edycja ustawień serwisu";

$aConfig['lang']['c_main_settings']['name']	= "Nazwa seriwsu";
$aConfig['lang']['c_main_settings']['email']	= "Adres e-mail seriwsu <br />(w tym dla paczkomaty)";
$aConfig['lang']['c_main_settings']['users_email']	= "Adres e-mail dla wiadomości do użytkowników";
$aConfig['lang']['c_main_settings']['orders_email']	= "Adres e-mail dla wiadomości odnośnie zamówień";
$aConfig['lang']['c_main_settings']['recommendations_email']	= "Adres e-mail do wysyłania zapytań";
$aConfig['lang']['c_main_settings']['cancel_email']	= "Adres e-mail do informacji o anulowaniu zamówień";
$aConfig['lang']['c_main_settings']['title']	= "Tytuł głównej strony";
$aConfig['lang']['c_main_settings']['subtitle']	= "Tytuł podstrony";
$aConfig['lang']['c_main_settings']['before_title']	= "Tytuł podstrony przed tytułem aktualności, artykułu, itp.";
$aConfig['lang']['c_main_settings']['description']	= "Opis";
$aConfig['lang']['c_main_settings']['keywords']	= "Słowa kluczowe";
$aConfig['lang']['c_main_settings']['footer']	= "Stopka serwisu";
$aConfig['lang']['c_main_settings']['footer2']	= "Stopka serwisu 2";
$aConfig['lang']['c_main_settings']['mail_footer']	= "Stopka maila";
$aConfig['lang']['c_main_settings']['mail_footer_order']	= "Stopka maila wysyłana z zamówień <br /><br />użyj: {imie_nazwisko}";
$aConfig['lang']['c_main_settings']['mail_footer_unsubscribe']	= "Komunikat wypisania się z rozsyłki kodów rabatowych w stopce maili wysyłanych z newslettera<br /><br />użyj: {link_zrezygnuj}";
$aConfig['lang']['c_main_settings']['comments']	= "Komentowanie w serwisie";
$aConfig['lang']['c_main_settings']['allow_comments']	= "Włącz komentowanie w serwisie";
$aConfig['lang']['c_main_settings']['comments_in_modules']	= "Komentowanie w modułach";
$aConfig['lang']['c_main_settings']['comments_per_page']	= "Komentarzy na stronie";
$aConfig['lang']['c_main_settings']['banned_ips'] = "Zbanowane adresy IP";
$aConfig['lang']['c_main_settings']['swears'] = "Filtrowane wulgaryzmy";
$aConfig['lang']['c_main_settings']['versioning']	= "Wersjonowanie w serwisie";
$aConfig['lang']['c_main_settings']['allow_cache']	= "Włącz cache'owanie";
$aConfig['lang']['c_main_settings']['allow_versioning']	= "Włącz wersjonowanie w serwisie";
$aConfig['lang']['c_main_settings']['delete_inactive_accounts_after']	= "Usuń niepotwierdzone konta po dniach";
$aConfig['lang']['c_main_settings']['clear_news_after']	= "Usuń nowości po";
$aConfig['lang']['c_main_settings']['send_opineo_after']	= "Wyślij opineo po";
$aConfig['lang']['c_main_settings']['fbog_config']	= "Konfiguracja Facebook OpenGraph";
$aConfig['lang']['c_main_settings']['fbog_sitename']	= "og:site_name - nazwa serwisu";
$aConfig['lang']['c_main_settings']['fbog_admins']	= "fb:admins - ID administratorów (oddzielone przecinkiem)";
$aConfig['lang']['c_main_settings']['fbog_page_id']	= "fb:page_id - ID strony administratora (oddzielone przecinkiem)";
$aConfig['lang']['c_main_settings']['fbog_app_id']	= "fb:app_id - ID aplikacji administratora (oddzielone przecinkiem)";
$aConfig['lang']['c_main_settings']['fbog_image']	= "fb:image - URL do zdjęcia dla strony głównej";
$aConfig['lang']['c_main_settings']['fbog_latitude']	= "fb:latitude - szerokość geograficzna";
$aConfig['lang']['c_main_settings']['fbog_longitude']	= "fb:longitude - długość geograficzna";
$aConfig['lang']['c_main_settings']['fbog_street_address']	= "fb:street-address - adres siedziby";
$aConfig['lang']['c_main_settings']['fbog_locality']	= "fb:locality - miejscowość siedziby";
$aConfig['lang']['c_main_settings']['fbog_region']	= "fb:region - województwo (region) siedziby";
$aConfig['lang']['c_main_settings']['fbog_postal_code']	= "fb:postal-code - kod pocztowy siedziby";
$aConfig['lang']['c_main_settings']['fbog_country_name']	= "fb:country-name - kraj siedziby";
$aConfig['lang']['c_main_settings']['fbog_phone_number']	= "fb:phone_number - numer telefonu siedziby";
$aConfig['lang']['c_main_settings']['fbog_fax_number']	= "fb:fax_number - numer faxu siedziby";

$aConfig['lang']['c_main_settings']['edit_ok']	= "Zmiany w ustawieniach serwisu zostały wprowadzone do bazy danych";
$aConfig['lang']['c_main_settings']['edit_err']	= "Nie udało się wprowadzić zmian w ustawieniach serwisu!<br>Spróbuj ponownie";

$aConfig['lang']['c_main_settings']['mbreak_section']	= "Przerwa techniczna";
$aConfig['lang']['c_main_settings']['mbreak']	= "Włącz przerwę techniczną";
$aConfig['lang']['c_main_settings']['mbreak_key']	= "Klucz przerwy technicznej";
$aConfig['lang']['c_main_settings']['mbreak_msg']	= "Wiadomosc przerwy technicznej";
?>
