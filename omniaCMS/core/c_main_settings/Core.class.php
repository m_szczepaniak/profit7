<?php
/**
 * Klasa Core do obslugi ustawien serwisu
 * Zarzadzanie uzytkownikami jest dostepne tylko dla
 * superuzytkownika lub administratorow serwisow
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Core {

	// komunikat
	var $sMsg;

	// ID wersji jezykowej serwisu
	var $iLangId;

	// nazwa modulu
	var $sModule;

	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];

		$sDo = '';
		$iId = 0;

		$this->sModule = $_GET['file'];

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}

		// pobranie ustawien serwisu
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."website_settings
						 WHERE language_id=".$this->iLangId;
		$aCfg = Common::GetRow($sSql);
		if (!empty($aCfg)) {
			$aCfg['footer'] = br2nl($aCfg['footer']);
			$aCfg['footer2'] = br2nl($aCfg['footer2']);
      $aCfg['mail_footer_unsubscribe'] = br2nl($aCfg['mail_footer_unsubscribe']);
			$aCfg['comments_in_modules'] = explode(',', $aCfg['comments_in_modules']);
		}
		
		
		if(file_exists($aConfig['common']['client_base_path'].'/przerwa/_przerwa_konfiguracja.php')){
			include_once($aConfig['common']['client_base_path'].'/przerwa/_przerwa_konfiguracja.php');
			$aCfg['mbreak_key'] = $przerwa_klucz;
			$aCfg['mbreak_msg'] = $przerwa_wiadomosc;
		}
		
		if(file_exists($aConfig['common']['client_base_path'].'/przerwa/przerwa_konfiguracja.php')){
			include_once($aConfig['common']['client_base_path'].'/przerwa/przerwa_konfiguracja.php');
			$aCfg['mbreak'] = '1';
			$aCfg['mbreak_key'] = $przerwa_klucz;
			$aCfg['mbreak_msg'] = $przerwa_wiadomosc;
		}
		
		// dolaczenie ustawien do glownej tablicy ustawien $aConfig
		$aConfig['settings'][$_GET['file']] = $aCfg;

		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia
		if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {

			switch ($sDo) {
				case 'update':
					$this->UpdateSettings($pSmarty);
				break;

				default:
					$this->Settings($pSmarty);
				break;
			}
		}
		else {
			$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
		}
	} // end Core() function


	/**
	 * Metoda wprowadza zmiany w konfiguracji serwisu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings(&$pSmarty) {
		global $aConfig;
		$bError = false;
		$sCommentsSQL = '';


		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Settings($pSmarty);
			return;
		}

		$aValues = array(
			'name' => $_POST['name'],
 			'email' => $_POST['email'],
 			'users_email' => $_POST['users_email'],
 			'orders_email' => $_POST['orders_email'],
 			'cancel_email' => $_POST['cancel_email'],
 			'recommendations_email' => $_POST['recommendations_email'],
 			'title' => $_POST['title'],
 			'subtitle' => trim($_POST['subtitle']) != '' ? $_POST['subtitle'] : 'NULL',
 			'before_title' => isset($_POST['before_title']) ? '1' : '0',
 			'keywords' => $_POST['keywords'],
 			'description' => $_POST['description'],
 			'footer' => trim($_POST['footer']) != '' ? nl2br(trim($_POST['footer'])) : 'NULL',
 			'footer2' => trim($_POST['footer2']) != '' ? nl2br(trim($_POST['footer2'])) : 'NULL',
 			'mail_footer' => trim($_POST['mail_footer']) != '' ? trim($_POST['mail_footer']) : 'NULL',
      'mail_footer_order' => trim($_POST['mail_footer_order']) != '' ? trim($_POST['mail_footer_order']) : 'NULL',
      'mail_footer_unsubscribe' => trim($_POST['mail_footer_unsubscribe']) != '' ? nl2br(trim($_POST['mail_footer_unsubscribe'])) : 'NULL',
			'delete_inactive_accounts_after' => (int) $_POST['delete_inactive_accounts_after'],
			'clear_news_after' => (int) $_POST['clear_news_after'],
			'send_opineo_after' => (int) $_POST['send_opineo_after'],
			'fbog_sitename' => trim($_POST['fbog_sitename']) != '' ? trim($_POST['fbog_sitename']) : 'NULL',
			'fbog_admins' => trim($_POST['fbog_admins']) != '' ? trim($_POST['fbog_admins']) : 'NULL',
			'fbog_page_id' => trim($_POST['fbog_page_id']) != '' ? trim($_POST['fbog_page_id']) : 'NULL',
			'fbog_app_id' => trim($_POST['fbog_app_id']) != '' ? trim($_POST['fbog_app_id']) : 'NULL',
			'fbog_image' => trim($_POST['fbog_image']) != '' ? trim($_POST['fbog_image']) : 'NULL',
			'fbog_latitude' => trim($_POST['fbog_latitude']) != '' ? trim($_POST['fbog_latitude']) : 'NULL',
			'fbog_longitude' => trim($_POST['fbog_longitude']) != '' ? trim($_POST['fbog_longitude']) : 'NULL',
			'fbog_street_address' => trim($_POST['fbog_street_address']) != '' ? trim($_POST['fbog_street_address']) : 'NULL',
			'fbog_locality' => trim($_POST['fbog_locality']) != '' ? trim($_POST['fbog_locality']) : 'NULL',
			'fbog_region' => trim($_POST['fbog_region']) != '' ? trim($_POST['fbog_region']) : 'NULL',
			'fbog_postal_code' => trim($_POST['fbog_postal_code']) != '' ? trim($_POST['fbog_postal_code']) : 'NULL',
			'fbog_country_name' => trim($_POST['fbog_country_name']) != '' ? trim($_POST['fbog_country_name']) : 'NULL',
			'fbog_phone_number' => trim($_POST['fbog_phone_number']) != '' ? trim($_POST['fbog_phone_number']) : 'NULL',
			'fbog_fax_number' => trim($_POST['fbog_fax_number']) != '' ? trim($_POST['fbog_fax_number']) : 'NULL'
		);
		if ($_SESSION['user']['type'] === 1) {
			$aValues['allow_cache'] = (int) isset($_POST['allow_cache']);
		}
		/*
		if ($_SESSION['user']['type'] === 1) {
			if (isset($_POST['allow_comments'])) {
				$aValues['allow_comments'] = '1';
				$aValues['comments_in_modules'] = implode(',', $_POST['comments_in_modules']);
				$aValues['comments_per_page'] = (int) $_POST['comments_per_page'];
				$aValues['swears'] = trim($_POST['swears']) != '' ? trim($_POST['swears']) : 'NULL';
			}
			else {
				$aValues['allow_comments'] = '0';
				$aValues['comments_in_modules'] = 'NULL';
				$aValues['comments_per_page'] = 0;
				$aValues['swears'] = 'NULL';
			}
			$aValues['allow_versioning'] = (int) isset($_POST['allow_versioning']);
		}
		*/
		
		if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2){
			$sBreakFile='<?php
$przerwa_klucz = "'.$_POST['mbreak_key'].'";
$przerwa_wiadomosc = "'.$_POST['mbreak_msg'].'";
?>';
			file_put_contents($aConfig['common']['client_base_path'].'/przerwa/_przerwa_konfiguracja.php', $sBreakFile);
			
			if(isset($_POST['mbreak']) && file_exists($aConfig['common']['client_base_path'].'/przerwa/_przerwa_konfiguracja.php')){
				if(!rename($aConfig['common']['client_base_path'].'/przerwa/_przerwa_konfiguracja.php',
									 $aConfig['common']['client_base_path'].'/przerwa/przerwa_konfiguracja.php')){
									 		$bError = true;
									 }
			}
			if(!isset($_POST['mbreak']) && file_exists($aConfig['common']['client_base_path'].'/przerwa/przerwa_konfiguracja.php')){
				if(!rename($aConfig['common']['client_base_path'].'/przerwa/przerwa_konfiguracja.php',
									 $aConfig['common']['client_base_path'].'/przerwa/_przerwa_konfiguracja.php')){
									 		$bError = true;
									 }
			}
		}
		
		if ($aConfig['settings'][$_GET['file']]['name'] != '') {
			// Update ustawien serwisu w bazie danych
			if (Common::Update($aConfig['tabls']['prefix']."website_settings",
												 $aValues,
												 "language_id = ".$this->iLangId) === false) {
				$bError = true;
			}
		}
		else {
			// Insert ustawien dla tej wersji jezykowej
			$aValues['language_id'] = $this->iLangId;
			if (Common::Insert($aConfig['tabls']['prefix']."website_settings",
												 $aValues,
												 '',
												 false) === false) {
				$bError = true;
			}
		}
		if (!$bError) {
			// ustawienia zostaly zaktualizowane
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_ok'], false);
			// dodanie informacji do logow
			AddLog($aConfig['lang'][$this->sModule]['edit_ok'], false);
			$this->Settings($pSmarty);
		}
		else {
			// nie udalo sie wprowadzic zmian w ustawieniach,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_err']);
			AddLog($aConfig['lang'][$this->sModule]['edit_err']);
			$this->Settings($pSmarty);
		}
	} // end of UpdateSettings() funciton


	/**
	 * Metoda tworzy formularz edycji konfiguracji aktualnosci
	 *
	 * @param		object	$pSmarty
	 */
	function Settings(&$pSmarty) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		$aData =& $aConfig['settings'][$_GET['file']];
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		elseif (!isset($aData['before_title'])) {
			$aData['before_title'] = '1';
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header'];

		$pForm = new FormTable('site_settings', $sHeader, array('action'=>phpSelf(array('do'=>'update'))), array('col_width'=>200), $aConfig['common']['js_validation']);


		// nazwa
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>64, 'style'=>'width: 250px;'));

		// email
		$pForm->AddText('email', $aConfig['lang'][$this->sModule]['email'], $aData['email'], array('maxlength'=>64, 'style'=>'width: 250px;'), '', 'email');

		// email do wysylania wiadomosci do uzytkownikow
		$pForm->AddText('users_email', $aConfig['lang'][$this->sModule]['users_email'], $aData['users_email'], array('maxlength'=>64, 'style'=>'width: 250px;'), '', 'email');
		
		// email do wysylania wiadomosci dotyczacych zamowien
		$pForm->AddText('orders_email', $aConfig['lang'][$this->sModule]['orders_email'], $aData['orders_email'], array('maxlength'=>64, 'style'=>'width: 250px;'), '', 'email');
		
		// email do wysylania polecen
		$pForm->AddText('recommendations_email', $aConfig['lang'][$this->sModule]['recommendations_email'], $aData['recommendations_email'], array('maxlength'=>64, 'style'=>'width: 250px;'), '', 'email');
		
		// email do wysylania info o anulowaniu
		$pForm->AddText('cancel_email', $aConfig['lang'][$this->sModule]['cancel_email'], $aData['cancel_email'], array('maxlength'=>64, 'style'=>'width: 250px;'), '', 'email');
		
		// tytul strony glownej
		$pForm->AddText('title', $aConfig['lang'][$this->sModule]['title'], $aData['title'], array('maxlength'=>255, 'style'=>'width: 350px;'));

		// tytul podstrony
		$pForm->AddText('subtitle', $aConfig['lang'][$this->sModule]['subtitle'], $aData['subtitle'], array('maxlength'=>64, 'style'=>'width: 250px;'), '', 'text', false);

		// dolaczaj tytul podstrony przed tytulem aktualnosci, artykulu, itp.
		$pForm->AddCheckBox('before_title', $aConfig['lang'][$this->sModule]['before_title'], array(), '', !empty($aData['before_title']), false);

		// slowa kluczowe
		$pForm->AddText('keywords', $aConfig['lang'][$this->sModule]['keywords'], $aData['keywords'], array('maxlength'=>255, 'style'=>'width: 350px;'));

		// opis serwisu
		$pForm->AddTextarea('description', $aConfig['lang'][$this->sModule]['description'], $aData['description'], array('style'=>'width: 350px;', 'rows'=>8));

		// stopka serwisu
		$pForm->AddTextarea('footer', $aConfig['lang'][$this->sModule]['footer'], $aData['footer'], array('style'=>'width: 350px;', 'rows'=>8), 0, '', '', false);

		// stopka serwisu 2
		$pForm->AddTextarea('footer2', $aConfig['lang'][$this->sModule]['footer2'], $aData['footer2'], array('style'=>'width: 350px;', 'rows'=>8), 0, '', '', false);

		// stopka maila
		$pForm->AddTextarea('mail_footer', $aConfig['lang'][$this->sModule]['mail_footer'], $aData['mail_footer'], array('style'=>'width: 350px;', 'rows'=>8), 0, '', '', false);
		
		// stopka maila
		$pForm->AddTextarea('mail_footer_order', $aConfig['lang'][$this->sModule]['mail_footer_order'], $aData['mail_footer_order'], array('style'=>'width: 350px;', 'rows'=>8), 0, '', '', false);
		
    // stopka maila
		$pForm->AddTextarea('mail_footer_unsubscribe', $aConfig['lang'][$this->sModule]['mail_footer_unsubscribe'], $aData['mail_footer_unsubscribe'], array('style'=>'width: 350px;', 'rows'=>8), 0, '', '', false);
    
		// usuwanie nieaktywnych kont po dniach
		$pForm->AddText('delete_inactive_accounts_after', $aConfig['lang'][$this->sModule]['delete_inactive_accounts_after'], $aData['delete_inactive_accounts_after'], array('maxlength'=>3, 'style'=>'width: 50px;'), '', 'uinteger');
		
		// ilość książek w księgarnii
		$pForm->AddText('clear_news_after', $aConfig['lang'][$this->sModule]['clear_news_after'], $aData['clear_news_after'], array('style'=>'width: 50px;'), '', 'uinteger');
		
		
		// ilość dni po ktorych wysylane jest info opineo
		$pForm->AddText('send_opineo_after', $aConfig['lang'][$this->sModule]['send_opineo_after'], $aData['send_opineo_after'], array('style'=>'width: 50px;'), '', 'uinteger');
		
		if ($_SESSION['user']['type'] == 1){
			// wlacz / wylacz cache'owanie
			$pForm->AddCheckBox('allow_cache', $aConfig['lang'][$this->sModule]['allow_cache'], array(), '', !empty($aData['allow_cache']), false);
		}
		
		// sekcja Facebook OpenGraph
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['fbog_config'], array('class'=>'merged', 'style'=>'padding-left: 205px'));
		// OG SiteName
		$pForm->AddText('fbog_sitename', $aConfig['lang'][$this->sModule]['fbog_sitename'], $aData['fbog_sitename'], array('style'=>'width: 350px;', 'maxlength'=>256), '', 'text', false);

		// OG admins
		$pForm->AddText('fbog_admins', $aConfig['lang'][$this->sModule]['fbog_admins'], $aData['fbog_admins'], array('style'=>'width: 350px;', 'maxlength'=>256), '', 'text', false);

		// OG page_id
		$pForm->AddText('fbog_page_id', $aConfig['lang'][$this->sModule]['fbog_page_id'], $aData['fbog_page_id'], array('style'=>'width: 350px;', 'maxlength'=>256), '', 'text', false);

		// OG app_id
		$pForm->AddText('fbog_app_id', $aConfig['lang'][$this->sModule]['fbog_app_id'], $aData['fbog_app_id'], array('style'=>'width: 350px;', 'maxlength'=>256), '', 'text', false);

		// OG image - strona glowna
		$pForm->AddText('fbog_image', $aConfig['lang'][$this->sModule]['fbog_image'], $aData['fbog_image'], array('style'=>'width: 350px;', 'maxlength'=>128), '', 'text', false);

		// OG szerokosc geograficzna siedziby
		$pForm->AddText('fbog_latitude', $aConfig['lang'][$this->sModule]['fbog_latitude'], $aData['fbog_latitude'], array('style'=>'width: 100px;', 'maxlength'=>16), '', 'text', false);

		// OG dlugosc geograficzna siedziby
		$pForm->AddText('fbog_longitude', $aConfig['lang'][$this->sModule]['fbog_longitude'], $aData['fbog_longitude'], array('style'=>'width: 100px;', 'maxlength'=>16), '', 'text', false);

		// OG adres siedziby
		$pForm->AddText('fbog_street_address', $aConfig['lang'][$this->sModule]['fbog_street_address'], $aData['fbog_street_address'], array('style'=>'width: 200px;', 'maxlength'=>64), '', 'text', false);

		// OG miejscowosc siedziby
		$pForm->AddText('fbog_locality', $aConfig['lang'][$this->sModule]['fbog_locality'], $aData['fbog_locality'], array('style'=>'width: 200px;', 'maxlength'=>64), '', 'text', false);

		// OG wojewodztwo siedziby
		$pForm->AddText('fbog_region', $aConfig['lang'][$this->sModule]['fbog_region'], $aData['fbog_region'], array('style'=>'width: 200px;', 'maxlength'=>64), '', 'text', false);

		// OG kod pocztowy siedziby
		$pForm->AddText('fbog_postal_code', $aConfig['lang'][$this->sModule]['fbog_postal_code'], $aData['fbog_postal_code'], array('style'=>'width: 75px;', 'maxlength'=>8), '', 'text', false);

		// OG kraj siedziby
		$pForm->AddText('fbog_country_name', $aConfig['lang'][$this->sModule]['fbog_country_name'], $aData['fbog_country_name'], array('style'=>'width: 150px;', 'maxlength'=>32), '', 'text', false);

		// OG numer telefonu siedziby
		$pForm->AddText('fbog_phone_number', $aConfig['lang'][$this->sModule]['fbog_phone_number'], $aData['fbog_phone_number'], array('style'=>'width: 150px;', 'maxlength'=>20), '', 'text', false);

		// OG numer telefonu siedziby
		$pForm->AddText('fbog_fax_number', $aConfig['lang'][$this->sModule]['fbog_fax_number'], $aData['fbog_fax_number'], array('style'=>'width: 150px;', 'maxlength'=>20), '', 'text', false);

		
		/*
		if ($_SESSION['user']['type'] == 1 ||
				$_SESSION['user']['type'] == 2 && $aData['allow_comments'] == '1') {
			// wlacz / wylacz komentowanie w serwisie - dostepne tylko dla superadmina
			// komentarze

			$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['comments'], array('class'=>'mergedLight', 'style'=>'padding-left: 205px'));
			
			if ($_SESSION['user']['type'] == 1) {
				// wlacz komentowanie w serwisie
				$pForm->AddCheckbox('allow_comments', $aConfig['lang'][$this->sModule]['allow_comments'], array(), '', $aData['allow_comments'], false);
			
				// moduly dla ktorych ma byc wlaczone komentowanie
				$pForm->AddSelect('comments_in_modules', $aConfig['lang'][$this->sModule]['comments_in_modules'], array('size' => 5, 'multiple' => 'multiple'), GetModulesList('symbol'), $aData['comments_in_modules'], '', false);
	
				// liczba komentarzy na stronie
				$pForm->AddText('comments_per_page', $aConfig['lang'][$this->sModule]['comments_per_page'], $aData['comments_per_page'], array('maxlength'=>3, 'style'=>'width: 50px;'), '', 'uinteger', false);
			}

			// lista wulgaryzmow do filtrowania
			$pForm->AddTextArea('swears', $aConfig['lang'][$this->sModule]['swears'], $aData['swears'], array('rows'=>8, 'style'=>'width: 350px;'), '', 0, '', false);
		}
		if ($_SESSION['user']['type'] == 1) {
			// wersjonowanie
			$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['versioning'], array('class'=>'mergedLight', 'style'=>'padding-left: 205px'));

			// wlacz / wylacz wersjonowanie
			$pForm->AddCheckBox('allow_versioning', $aConfig['lang'][$this->sModule]['allow_versioning'], array(), '', !empty($aData['allow_versioning']), false);
		}
		*/
		

		if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2){
			$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['mbreak_section'], array('class'=>'merged', 'style'=>'padding-left: 165px;'));
			$pForm->AddCheckBox('mbreak', $aConfig['lang'][$this->sModule]['mbreak'], array(), '', !empty($aData['mbreak']), false);
			// usuwanie nieaktywnych kont po dniach
			$pForm->AddText('mbreak_key', $aConfig['lang'][$this->sModule]['mbreak_key'], $aData['mbreak_key'], array('maxlength'=>32, 'style'=>'width: 250px;'), '', 'text', false);
			// opis serwisu
			$pForm->AddTextarea('mbreak_msg', $aConfig['lang'][$this->sModule]['mbreak_msg'], $aData['mbreak_msg'], array('style'=>'width: 350px;', 'rows'=>8),0,'','',false);
		}
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Settings() function
} //end of Core class
?>