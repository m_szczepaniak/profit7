<?php
/**
 * Klasa Core do obslugi wersji jezykowych serwisu.
 * Zarzadzanie wersjami jezykowymi jest dozwolone tylko dla
 * superuzytkownika
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Core {

	// komunikat
	var $sMsg;

	// sciezka do katalogu zawierajacego pliki XML
	var $sXMLDir;

	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->sXMLDir = $aConfig['common']['client_base_path'].'images/xml';

		$sAction = '';
		$iId = 0;

		if (isset($_GET['do'])) {
			$sAction = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sAction = $_POST['do'];
		}
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}

		if ($_SESSION['user']['type'] == 1) {
			switch ($sAction) {
				case 'delete': $this->Delete($pSmarty, $iId); break;
				case 'show': $this->Show($pSmarty); break;
				case 'insert': $this->Insert($pSmarty); break;
				case 'update': $this->Update($pSmarty); break;
				case 'edit': $this->AddEdit($pSmarty, $iId); break;
				case 'add': $this->AddEdit($pSmarty); break;
				default: $this->Show($pSmarty); break;
			}
		}
		else {
			$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
		}
	} // end Core() function


	/**
	 * Metoda usuwa wybrana(e) wersje jez.
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID usuwanej wersji jez.
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId=0) {
		global $aConfig;
		$bIsErr = false;

		if ($iId > 0) {
			$_POST['delete'][$iId] = 'on';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();

			// pobranie symboli usuwanych wersji jez.
			$sSql = "SELECT symbol
					 		 FROM ".$aConfig['tabls']['prefix']."languages
						 	 WHERE id IN (".implode(',', $_POST['delete']).")";
			$aRecords =& Common::GetCol($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				foreach ($aRecords as $sSymbol) {
					$sDel .= '"'.$sSymbol.'", ';
				}
				$sDel = substr($sDel, 0, -2);
			}

			if (!$bIsErr) {
				// usuwanie
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."languages
								 WHERE id IN (".implode(',', $_POST['delete']).")";
				if (count($_POST['delete']) == 1) {
					$sErrPostfix = '0';
				}
				else {
					$sErrPostfix = '1';
				}
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}
			if (!$bIsErr && (!$this->writeLanguagesToXML() || !$this->writeMenusToXML())) {
				$bIsErr = true;
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_ok_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_err_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton

	/**
	 * Metoda dodaje do bazy danych nowa wersje jez.
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}
		// sprawdzenie czy wersja jez. o podanym symbolu nie istnieje w bazie danych
		if (!$this->LanguageVersionExists($_POST['symbol'])) {
			// rozpoczecie transakcji
			Common::BeginTransaction();

			$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."languages (
							 symbol,
							 default_lang,
							 published
						  )
						  VALUES (
							 '".$_POST['symbol']."',
							 '".(isset($_POST['default_lang']) ? '1' : '0')."',
							 '".(isset($_POST['published']) ? '1' : '0')."'
						  )";

			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
			if (!$bIsErr && (!$this->writeLanguagesToXML() || !$this->writeMenusToXML())) {
				$bIsErr = true;
			}
			if (!$bIsErr) {
				// wersja jez. zostala zmodyfikowana,
				// zatwierdzenie transakcji
				Common::CommitTransaction();
				// wersja jez. zostala dodana, wyswietlenie komunikatu o powodzeniu
				// oraz wyswietlenie listy obszarow
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_ok'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg, false);
				AddLog($sMsg, false);
				// resetowanie stanu widoku
				$_GET['reset'] = 2;
				$this->Show($pSmarty);
			}
			else {
				// wersja jez. nie zostala dodana,
				// wycofanie transakcji
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_err'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty);
			}
		}
		else {
			// wersja jez. o podanym symbolu juz istnieje
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_err2'],
											$_POST['symbol']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEdit($pSmarty);
		}
	} // end of Insert() funciton


	/**
	 * Metoda wprowadza zmiany w wersji jezykowej do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $_GET['id']);
			return;
		}

		// sprawdzenie czy wersja jez. o podanym symbolu nie istnieje w bazie danych
		if (!$this->LanguageVersionExists($_POST['symbol'], $_GET['id'])) {
			// rozpoczecie transakcji
			Common::BeginTransaction();

			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."languages SET
								symbol = '".$_POST['symbol']."',
								default_lang = '".(isset($_POST['default_lang']) ? '1' : '0')."',
								published = '".(isset($_POST['published']) ? '1' : '0')."'
						   WHERE id=".$_GET['id'];

			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
			if (!$bIsErr && (!$this->writeLanguagesToXML() || !$this->writeMenusToXML())) {
				$bIsErr = true;
			}
			if (!$bIsErr) {
				// wersja jez. zostala zmodyfikowana,
				// zatwierdzenie transakcji
				Common::CommitTransaction();
		 		// wyswietlenie komunikatu o powodzeniu
				// oraz wyswietlenie listy wersji jezykowych
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['edit_ok'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg, false);
				AddLog($sMsg, false);
				$this->Show($pSmarty);
			}
			else {
				// nie wprowadzono zadnych zmian,
				// wycofanie transakcji
				Common::RollbackTransaction();
				// wyswietlenie komunikatu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['edit_err'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $_GET['id']);
			}
		}
		else {
			// wersja jez. o podanym symbolu lub numerze juz istnieje
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_err2'],
											$_POST['symbol']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEdit($pSmarty, $_GET['id']);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji wersji jezykowej
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	ID edytowanej wersji jezykowej
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanej wersji jezykowej
			$sSql = "SELECT symbol, default_lang, published
							 FROM ".$aConfig['tabls']['prefix']."languages
							 WHERE id=".$iId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$_GET['file']]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['symbol']) && !empty($aData['symbol'])) {
			$sHeader .= ' "'.$aData['symbol'].'"';
		}

		// tworzenie formularza
		$pForm = new FormTable('languages', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>160), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));

		// symbol wersji jez.
		$pForm->AddText('symbol', $aConfig['lang'][$_GET['file']]['symbol'], $aData['symbol'], array('maxlength'=>2, 'style'=>'width: 50px;'));

		// jest domyslna wersja jezykowa serwisu
		$pForm->AddCheckBox('default_lang', $aConfig['lang'][$_GET['file']]['default_lang'], array(), '', !empty($aData['default_lang']), false);

		// wersja jezykowa jest opublikowana
		$pForm->AddCheckBox('published', $aConfig['lang'][$_GET['file']]['published'], array(), '', !empty($aData['published']), false);

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).
														 '&nbsp;&nbsp;'.
														 $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function


	/**
	 * Metoda wyswietla liste zdefiniowanych dla serwisu wersji jezykowych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// zapamietanie opcji
		rememberViewState($_GET['file']);

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'header'	=> $aConfig['lang'][$_GET['file']]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '30'
			),
			array(
				'db_field'	=> 'symbol',
				'content'	=> $aConfig['lang'][$_GET['file']]['symbol'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'default_lang',
				'content'	=> $aConfig['lang'][$_GET['file']]['default_lang'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'published',
				'content'	=> $aConfig['lang'][$_GET['file']]['published'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich wersji jezykowych
		$sSql = "SELECT COUNT(*)
						 FROM ".$aConfig['tabls']['prefix']."languages
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND symbol LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($_GET['file']);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(*)
							 FROM ".$aConfig['tabls']['prefix']."languages
							 WHERE 1 = 1";
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('languages', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich wersji jezykowych dla danego serwisu
			$sSql = "SELECT id, symbol, IF (default_lang = '0', '".$aConfig['lang']['common']['no']."', '".$aConfig['lang']['common']['yes']."') as default_lang, IF (published = '0', '".$aConfig['lang']['common']['no']."', '".$aConfig['lang']['common']['yes']."') as published
							 FROM ".$aConfig['tabls']['prefix']."languages
							 WHERE TRUE".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND symbol LIKE \'%'.$_POST['search'].'%\'' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'default_lang DESC, id').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'symbol' => array (
					'link'	=> phpSelf(array('do'=>'edit', 'id'=>'{id}')),
					'use_lang'	=> true
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params'	=> array (
												'edit'	=>	array('id' => '{id}'),
												'delete'	=>	array('id' => '{id}')
					)
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('add')
		);
		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda zapisuje liste wersji jezykowych do pliku XML
	 *
	 * @return	bool	- true: zapisano; false: wystapil blad
	 */
	function writeLanguagesToXML() {
		global $aConfig;

		// pobranie z bazy danych listy wszystkich wersji jezykowych
		$sSql = "SELECT id, symbol, default_lang, published
						 FROM ".$aConfig['tabls']['prefix']."languages
						 ORDER BY default_lang DESC, symbol";
		$aItems =& Common::GetAll($sSql);

		// dolaczenie klasy XML/Serializer
		require_once ("XML/Serializer.php");
		// Serializer options
		$aOptions = array(
			'addDecl'	=> true,
			'encoding'	=> 'utf-8',
			'indent'	=>	"\t",
			'rootName'	=>	'languages',
			'defaultTagName'	=>	'item'
		);

		$oSerializer = new XML_Serializer($aOptions);
		$oResult = $oSerializer->serialize($aItems);

		if (PEAR::isError($oResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($status->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
		}

		if (!file_exists($this->sXMLDir)) {
			// utworzenie katalogu
			if (!mkdir($this->sXMLDir, 0755)) {
				return false;
			}
		}

		// zapis do pliku XML
		if ($oFile = fopen($this->sXMLDir.'/lang.xml', 'w')) {
			flock($oFile, 2);
			if (fwrite($oFile, $oSerializer->getSerializedData())) {
				flock($oFile, 3);
				fclose($oFile);
				return true;
			}
		}
		return false;
	} // end of writeLanguagesToXML() method


	/**
	 * Metoda zapisuje strukture drzewa stron wszystkich menu
	 * do pliku XML
	 *
	 * @return	bool	- true: zapisano; false: wystapil blad
	 */
	function writeMenusToXML() {
		global $aConfig;

		// dolaczenie klasy XML/Serializer
		require_once ("XML/Serializer.php");
		// Serializer options
		$aOptions = array(
			'addDecl'	=> true,
			'encoding'	=> 'utf-8',
			'indent'	=>	"\t",
			'rootName'	=>	'menu',
			'defaultTagName'	=>	'item'
		);

		$oSerializer = new XML_Serializer($aOptions);

		// jezeli nie istnieje katalog
		if (!file_exists($this->sXMLDir)) {
			// utworzenie katalogu
			if (!mkdir($this->sXMLDir, 0755)) {
				return false;
			}
		}

		// pobranie z bazy danych wszystkich wersji jezykowych
		$sSql = "SELECT id, symbol, default_lang
						 FROM ".$aConfig['tabls']['prefix']."languages";
		$aLangs =& Common::GetAll($sSql);

		foreach ($aLangs as $aLang) {
			// katalog wersji jezykowej
			$sDir = $this->sXMLDir.'/'.$aLang['symbol'];
			if (!file_exists($sDir)) {
				// utworzenie katalogu
				if (!mkdir($sDir, 0755)) {
					return false;
				}
			}

			// pobranie z bazy danych listy wszystkich menu
			$sSql = "SELECT id
							 FROM ".$aConfig['tabls']['prefix']."menus
							 WHERE language_id = ".$aLang['id'];
			$aMenus =& Common::GetCol($sSql);
			foreach ($aMenus as $iMId) {
				$sSql = "SELECT IFNULL(parent_id, 0), id, name, symbol, mtype, description,
												url, new_window, link_to_id
								 FROM ".$aConfig['tabls']['prefix']."menus_items
								 WHERE menu = '1' AND
								 			 menu_id = ".$iMId." AND
								 			 language_id = ".$aLang['id']."
								 			 ORDER BY parent_id, order_by";
				$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);

				// utworzenie drzewa menu
				if ($aLang['default_lang'] === '0') {
					$aMenuItems =& getMenuTreeForXML($aItems, 0, '',
																									 $aLang['symbol']);
				}
				else {
					$aMenuItems =& getMenuTreeForXML($aItems);
				}

				$oResult = $oSerializer->serialize($aMenuItems);

				if (PEAR::isError($oResult)) {
					if ($aConfig['common']['status'] == 'development') {
						TriggerError($status->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
					}
					return false;
				}

				// zapis do pliku XML
				if ($oFile = fopen($sDir.'/menu_'.$iMId.'.xml', 'w')) {
					flock($oFile, 2);
					if (fwrite($oFile, $oSerializer->getSerializedData())) {
						flock($oFile, 3);
						fclose($oFile);
					}
					else {
						return false;
					}
				}
			}
		}
		return true;
	} // end of writeMenusToXML() method


	/**
	 * Metoda sprawdza czy wersja jezykowa o podanym symbolu istnieje
	 *
	 * @param		string	$sSymbol	- symbol wersji jez.
	 * @param		integer	$iId	- ID wersji jez. dla ktorej pomijane jest sprawdzanie
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function LanguageVersionExists($sSymbol, $iId=0) {
		global $aConfig;

		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."languages
						 WHERE symbol = '".$sSymbol."'".
									 ($iId > 0 ? " AND id != ".$iId : '');

		return intval(Common::GetOne($sSql)) == 1;
	} // end of LanguageVersionExists() function
} //end of Core class
?>