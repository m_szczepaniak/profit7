<?php
/**
* Plik jezykowy dla interfejsu Core Languages Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'WERSJE JEZYKOWE' --------------*/
$aConfig['lang']['c_languages']['list']		= "Lista wersji językowych";
$aConfig['lang']['c_languages']['symbol']		= "Symbol";
$aConfig['lang']['c_languages']['default_lang']		= "Wersja domyślna";
$aConfig['lang']['c_languages']['no_items']	= "Brak zdefiniowanych wersji językowych";

$aConfig['lang']['c_languages']['edit']	= "Edytuj wersję językową";
$aConfig['lang']['c_languages']['delete']	= "Usuń wersję językową";
$aConfig['lang']['c_languages']['delete_q']	= "Czy na pewno usunąć wybraną wersję językową?";
$aConfig['lang']['c_languages']['check_all']	= "Zaznacz / odznacz wszystkie";
$aConfig['lang']['c_languages']['delete_all']	= "Usuń zaznaczone";
$aConfig['lang']['c_languages']['delete_all_q']	= "Czy na pewno usunąć wybrane wersje językowe?";
$aConfig['lang']['c_languages']['delete_all_err']	= "Nie wybrano żadnej wersji językowej do usunięcia!";
$aConfig['lang']['c_languages']['add']	= "Dodaj wersję językową";

$aConfig['lang']['c_languages']['header_0']	= "Dodawanie wersji językowej";
$aConfig['lang']['c_languages']['header_1']	= "Edycja wersji językowej";
$aConfig['lang']['c_languages']['symbol']	= "Symbol";
$aConfig['lang']['c_languages']['default_lang']	= "Wersja domyślna";
$aConfig['lang']['c_languages']['published']	= "Opublikowana";

$aConfig['lang']['c_languages']['add_ok']	= "Wersja językowa \"%s\" została dodana do bazy danych";
$aConfig['lang']['c_languages']['add_err']	= "Nie udało się dodać wersji językowej \"%s\" do bazy danych!<br>Spróbuj ponownie";
$aConfig['lang']['c_languages']['add_err2']= "Wersja językowa o podanym symbolu \"%s\" już istnieje!<br>Wybierz inny symbol";
$aConfig['lang']['c_languages']['del_ok_0']	= "Wersja językowa %s została usunięta";
$aConfig['lang']['c_languages']['del_ok_1']	= "Wersje językowe %s zostały usunięte";
$aConfig['lang']['c_languages']['del_err_0']	= "Nie udało się usunąć wersji językowej %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_languages']['del_err_1']	= "Nie udało się usunąć wersji językowych %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_languages']['edit_ok']	= "Zmiany w wersji językowej \"%s\" zostały wprowadzone do bazy danych";
$aConfig['lang']['c_languages']['edit_err']= "Nie udało się wprowadzić zmian w wersji językowej \"%s\"<br>Spróbuj ponownie";
?>
