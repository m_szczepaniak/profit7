<?php
/**
* Plik jezykowy dla interfejsu Core Boxes Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'BOKSY' --------------*/
$aConfig['lang']['c_boxes']['list']	= "Lista Boksów";
$aConfig['lang']['c_boxes']['area_boxes_list']	= "Lista boksów obszaru: \"%s\"";
$aConfig['lang']['c_boxes']['list_name']	= "Nazwa boksu";
$aConfig['lang']['c_boxes']['list_cache_info']	= "Cachowanie";
$aConfig['lang']['c_boxes']['list_type']	= "Typ boksu";
$aConfig['lang']['c_boxes']['list_area']	= "Obszar boksu";
$aConfig['lang']['c_boxes']['no_items']	= "Brak zdefiniowanych boksów";

/*-------------- opcje boksow */
$aConfig['lang']['c_boxes']['default']	= "domyślna";
$aConfig['lang']['c_boxes']['m_oferta_produktowa_promocje']	= "Promocje";
$aConfig['lang']['c_boxes']['m_oferta_produktowa_aukcje']	= "Aukcje";

$aConfig['lang']['c_boxes']['edit']	= "Edytuj boks";
$aConfig['lang']['c_boxes']['delete']	= "Usuń boks";
$aConfig['lang']['c_boxes']['delete_q']	= "Czy na pewno usunąć wybrany boks?";
$aConfig['lang']['c_boxes']['check_all']	= "Zaznacz / odznacz wszystkie";
$aConfig['lang']['c_boxes']['delete_all']	= "Usuń zaznaczone";
$aConfig['lang']['c_boxes']['delete_all_q']	= "Czy na pewno usunąć wybrane boksy?";
$aConfig['lang']['c_boxes']['delete_all_err']	= "Nie wybrano żadnego boksu do usunięcia!";
$aConfig['lang']['c_boxes']['sort']	= "Sortuj boksy";
$aConfig['lang']['c_boxes']['add']	= "Dodaj boks";

$aConfig['lang']['c_boxes']['header_0']	= "Dodawanie boksu";
$aConfig['lang']['c_boxes']['header_1']	= "Edycja boksu";
$aConfig['lang']['c_boxes']['name']	= "Nazwa";
$aConfig['lang']['c_boxes']['area']	= "Obszar boksu";
$aConfig['lang']['c_boxes']['btype']	= "Typ";
$aConfig['lang']['c_boxes']['btype_1']	= "Boks menu";
$aConfig['lang']['c_boxes']['btype_4']	= "Boks menu - pierwszy poziom";
$aConfig['lang']['c_boxes']['btype_2']	= "Boks modułu";
$aConfig['lang']['c_boxes']['btype_3']	= "Boks pusty";
$aConfig['lang']['c_boxes']['menu']	= "Menu";
$aConfig['lang']['c_boxes']['module']	= "Moduł";
$aConfig['lang']['c_boxes']['moption']	= "Opcja modułu";
$aConfig['lang']['c_boxes']['symbol']	= "Symbol";
$aConfig['lang']['c_boxes']['settings']	= "Konfiguracja boksu";
$aConfig['lang']['c_boxes']['template']	= "Szablon";

$aConfig['lang']['c_boxes']['default_section']	= "Domyślne ustawienia";
$aConfig['lang']['c_boxes']['start_page']	= "Wyświetlaj na stronie głównej";
$aConfig['lang']['c_boxes']['subpage']	= "Wyświetlaj na podstronach";
$aConfig['lang']['c_boxes']['show_mode_0'] = "Nie wyświetlaj";
$aConfig['lang']['c_boxes']['show_mode_4'] = "Nie wyświetlaj - listuj w podstronach";
$aConfig['lang']['c_boxes']['show_mode_1'] = "Wyświetlaj";
$aConfig['lang']['c_boxes']['show_mode_2'] = "Nie wyświetlaj gdy ID";
$aConfig['lang']['c_boxes']['show_mode_3'] = "Wyświetlaj gdy ID";
$aConfig['lang']['c_boxes']['cacheable']	= "Cache'uj box";
$aConfig['lang']['c_boxes']['cacheable_0']	= "Nigdy";
$aConfig['lang']['c_boxes']['cacheable_1']	= "Zawsze";
$aConfig['lang']['c_boxes']['cacheable_2']	= "Nie gdy przesłano POST";
$aConfig['lang']['c_boxes']['cache_per_page']	= "Cache'uj na stronę";
$aConfig['lang']['c_boxes']['omit_memcached']	= "Nie cachuj w memcached";

$aConfig['lang']['c_boxes']['add_ok']	= "Boks \"%s\" został dodany do bazy danych";
$aConfig['lang']['c_boxes']['add_err']	= "Nie udało się dodać boksu \"%s\" do bazy danych!<br>Spróbuj ponownie";
$aConfig['lang']['c_boxes']['del_ok_0']	= "Boks %s został usunięty";
$aConfig['lang']['c_boxes']['del_ok_1']	= "Boksy %s zostały usunięte";
$aConfig['lang']['c_boxes']['del_err_0']	= "Nie udało się usunąć boksu %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_boxes']['del_err_1']	= "Nie udało się usunąć boksów %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_boxes']['edit_ok']	= "Zmiany w boksie \"%s\" zostały wprowadzone do bazy danych";
$aConfig['lang']['c_boxes']['edit_err']= "Nie wprowadzono żadnych zmian w boksie \"%s\"";

$aConfig['lang']['c_boxes']['sort_items'] = "Sortowanie boksów obszaru: \"%s\"";
$aConfig['lang']['c_boxes']['go_back'] = "Powrót do listy boksów";
?>
