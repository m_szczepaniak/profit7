<?php
/**
 * Klasa Core do obslugi boksow.
 * Zarzadzanie modulami jest dozwolone tylko dla
 * superuzytkownika
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Core {

	// komunikat
	var $sMsg;

	// Id wersji jezykowej
	var $iLangId;

	// sciezka do katalogu zawierajacego szablony czesci klienckiej boksow
	var $sTemplatesDir;

	// lista szablonow czesci klienckiej boksow
	var $sTemplatesList;

	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/boxes';

		$sAction = '';
		$iId = 0;
		$iAId = 0;

		if (isset($_GET['do'])) {
			$sAction = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sAction = $_POST['do'];
		}
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['aid'])) {
			$iAId = $_GET['aid'];
		}

		if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {
			switch ($sAction) {
				case 'delete': $this->Delete($pSmarty, $iId); break;
				case 'insert': $this->Insert($pSmarty); break;
				case 'update': $this->Update($pSmarty, $iId); break;
				case 'edit':
				case 'add':
				case 'change_type': $this->AddEdit($pSmarty, $iAId, $iId); break;
				case 'sort':
					// dolaczenie pliku klasy MySort
					include_once('Sort.class.php');
					// obiekt klasy MySort
					$oSort = new MySort();
					// utwozenie widoku do sortowania
					$pSmarty->assign('sContent',
														$oSort->Show($this->getAreaName($iAId),
																				 $this->getRecords($iAId),
																				 phpSelf(array('do'=>'proceed_sort',
																				 							 'aid' => $iAId),
																				 				 array(), false)));
				break;
				case 'proceed_sort':
					// dolaczenie pliku klasy MySort
					include_once('Sort.class.php');
					// obiekt klasy MySort
					$oSort = new MySort();
					// sortowanie rekordow
					$oSort->Proceed($aConfig['tabls']['prefix']."boxes",
													$this->getRecords($iAId),
													array(
														'language_id' => $this->iLangId,
														'area_id' => $iAId
												 	));
				break;
				default: $this->Show($pSmarty); break;
			}
		}
		else {
			$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
		}
	} // end Core() function


	/**
	 * Metoda usuwa wybrany(e) boks(y)
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID usuwanego boksu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId=0) {
		global $aConfig;
		$bIsErr = false;

		if ($iId > 0) {
			$_POST['delete'][$iId] = 'on';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();

			// pobranie nazw usuwanych boksow
			$sSql = "SELECT name
					 		 FROM ".$aConfig['tabls']['prefix']."boxes
						 	 WHERE id IN (".implode(',', $_POST['delete']).") AND
						 	 			 language_id = ".$this->iLangId;
			$aRecords =& Common::GetCol($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				foreach ($aRecords as $sItem) {
					$sDel .= '"'.$sItem.'", ';
				}
				$sDel = substr($sDel, 0, -2);
			}

			if (!$bIsErr) {
				// usuwanie
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."boxes
								 WHERE id IN (".implode(',', $_POST['delete']).") AND
						 	 			 	 language_id = ".$this->iLangId;
				if (count($_POST['delete']) == 1) {
					$sErrPostfix = '0';
				}
				else {
					$sErrPostfix = '1';
				}
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_ok_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_err_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton

	/**
	 * Metoda dodaje do bazy danych nowy boks
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $_POST['area_id']);
			return;
		}

		Common::BeginTransaction();

		// okreslenie kolejnego numeru porzadkowego boksu
		$sSql = "SELECT IFNULL(MAX(order_by), 0) + 1
						 FROM ".$aConfig['tabls']['prefix']."boxes
						 WHERE area_id = ".$_POST['area_id'];
		$iOrderBy = Common::GetOne($sSql);
		if ($iOrderBy === false) {
			$bIsErr = true;
		}
		elseif ($iOrderBy > 65535) {
			$iOrderBy = 65535;
		}

		if (!$bIsErr) {
			// dodawanie boksu
			$aValues = array(
				'language_id' => $this->iLangId,
				'area_id' => $_POST['area_id'],
				'menu_id' => isset($_POST['menu_id']) ? $_POST['menu_id'] : 'NULL',
				'module_id' => isset($_POST['module_id']) ? $_POST['module_id'] : 'NULL',
				'page_id' => isset($_POST['page_id']) && $_POST['page_id'] != '0' ? $_POST['page_id'] : 'NULL',
				'template' => $_POST['template'],
				'name' => $_POST['name'],
				'btype' => $_POST['btype'],
				'start_page' => $_POST['start_page'],
				'subpage' => $_POST['subpage'],
				'symbol' => isset($_POST['symbol']) ? $_POST['symbol'] : 'NULL',
				'order_by' => $iOrderBy,
				'cacheable' => isset($_POST['cacheable']) ? $_POST['cacheable'] : 0,
        'omit_memcached' => isset($_POST['omit_memcached']) ? $_POST['omit_memcached'] : 0,
				'moption' => isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'] : 'NULL',
				'cache_per_page' => isset($_POST['cache_per_page']) ? '1' : '0'
			);
			if (($iId = Common::Insert($aConfig['tabls']['prefix']."boxes",
												 				 $aValues)) === false) {
				$bIsErr = true;
			}
		}

		if (!$bIsErr) {
			// zapisanie konfiguracji
			switch ($_POST['btype']) {
				case '1':
					if (file_exists('boxes/m_menu/Box_settings.class.php')) {
						// dolaczenie pliku zarzadzajacego konfiguracja kategorii modulu
						include_once('boxes/m_menu/Box_settings.class.php');
						$pSettings = new Settings('m_menu', $this->iLangId, $iId);
						if (!$pSettings->UpdateSettings()) {
							$bIsErr = true;
						}
					}
				break;

				case '2':
					// okreslenie symbolu modulu
					$sSql = "SELECT symbol
									 FROM ".$aConfig['tabls']['prefix']."modules
									 WHERE id = ".$_POST['module_id'];
					$sModule =& Common::GetOne($sSql);
					if ($sModule === false) {
						$bIsErr = true;
					}
					if (!$bIsErr) {
						if (file_exists('boxes/'.$sModule.'/Box_'.(isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'].'_' : '').'settings.class.php')) {
							// dolaczenie pliku zarzadzajacego konfiguracja kategorii modulu
							include_once('boxes/'.$sModule.'/Box_'.(isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'].'_' : '').'settings.class.php');
							$pSettings = new Settings($_POST['module_id'], $sModule, $this->iLangId, $iId, isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'] : '');
							if (!$pSettings->UpdateSettings()) {
								$bIsErr = true;
							}
						}
					}
				break;
			}
		}
		if (!$bIsErr) {
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_ok'], $_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			AddLog($sMsg, false);
			// resetowanie stanu widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_err'], $_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $_POST['area_id']);
		}
	} // end of Insert() funciton


	/**
	 * Metoda wprowadza zmiany w boksie
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id edytowanego boksu
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $_POST['area_id'], $iId);
			return;
		}

		Common::BeginTransaction();

		if ($_POST['area_id'] != $_POST['old_area_id']) {
			// nastapila zmiana obszaru boksu
			// okreslenie kolejnego numeru porzadkowego boksu
			$sSql = "SELECT IFNULL(MAX(order_by), 0) + 1
							 FROM ".$aConfig['tabls']['prefix']."boxes
							 WHERE area_id = ".$_POST['area_id'];
			$iOrderBy = Common::GetOne($sSql);
			if ($iOrderBy === false) {
				$bIsErr = true;
			}
			elseif ($iOrderBy > 65535) {
				$iOrderBy = 65535;
			}
		}
		
		if (!$bIsErr) {
			// aktualizacja boksu
			$aValues = array(
				'area_id' => $_POST['area_id'],
				'menu_id' => isset($_POST['menu_id']) ? $_POST['menu_id'] : 'NULL',
				'module_id' => isset($_POST['module_id']) ? $_POST['module_id'] : 'NULL',
				'page_id' => isset($_POST['page_id']) && $_POST['page_id'] != '0' ? $_POST['page_id'] : 'NULL',
				'template' => $_POST['template'],
				'name' => $_POST['name'],
				'btype' => $_POST['btype'],
				'start_page' => $_POST['start_page'],
				'subpage' => $_POST['subpage'],
				'symbol' => isset($_POST['symbol']) ? $_POST['symbol'] : 'NULL',
				'cacheable' => isset($_POST['cacheable']) ? $_POST['cacheable'] : '0',
        'omit_memcached' => isset($_POST['omit_memcached']) ? $_POST['omit_memcached'] : 0,
				'moption' => isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'] : 'NULL',
				'cache_per_page' => isset($_POST['cache_per_page']) ? '1' : '0'
			);
			if (isset($iOrderBy)) {
				$aValues['order_by'] = $iOrderBy;
			}
			if (Common::Update($aConfig['tabls']['prefix']."boxes",
												 $aValues,
												 "id = ".$iId) === false) {
				$bIsErr = true;
			}
		}

		if (!$bIsErr) {
			// zapisanie konfiguracji
			switch ($_POST['btype']) {
				case '1':
					if (file_exists('boxes/m_menu/Box_settings.class.php')) {
						// dolaczenie pliku zarzadzajacego konfiguracja kategorii modulu
						include_once('boxes/m_menu/Box_settings.class.php');
						$pSettings = new Settings('m_menu', $this->iLangId, $iId);
						if (!$pSettings->UpdateSettings()) {
							$bIsErr = true;
						}
					}
				break;

				case '2':
					// okreslenie symbolu modulu
					$sSql = "SELECT symbol
									 FROM ".$aConfig['tabls']['prefix']."modules
									 WHERE id = ".$_POST['module_id'];
					$sModule =& Common::GetOne($sSql);
					if ($sModule === false) {
						$bIsErr = true;
					}
					if (!$bIsErr) {
						if (file_exists('boxes/'.$sModule.'/Box_'.(isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'].'_' : '').'settings.class.php')) {
							// dolaczenie pliku zarzadzajacego konfiguracja kategorii modulu
							include_once('boxes/'.$sModule.'/Box_'.(isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'].'_' : '').'settings.class.php');
							$pSettings = new Settings($_POST['module_id'], $sModule, $this->iLangId, $iId, isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'] : '');
							if (!$pSettings->UpdateSettings()) {
								$bIsErr = true;
							}
						}
					}
				break;
			}
		}
		if (!$bIsErr) {
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['edit_ok'], $_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			AddLog($sMsg, false);
			// resetowanie stanu widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['edit_err'], $_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $_POST['area_id'], $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji boksu
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iAId	ID obszaru
	 * @param		integer	$iId	ID edytowanego modulu
	 */
	function AddEdit(&$pSmarty, $iAId, $iId=0) {
		global $aConfig;

		$aData = array();
		$aTemplatesList	= array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego boksu
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."boxes
							 WHERE id=".$iId." AND
							 			 language_id = ".$this->iLangId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			if (!isset($aData['btype'])) {
				$aData['btype'] = '2';
			}
			if (!isset($aData['start_page'])) {
				$aData['start_page'] = '1';
			}
			if (!isset($aData['subpage'])) {
				$aData['subpage'] = '1';
			}
			if (!isset($aData['area_id']) && $iAId > 0) {
				$aData['area_id'] = $iAId;
			}
		}

		$sHeader = $aConfig['lang'][$_GET['file']]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0) {
			$sHeader .= ' "'.$aData['name'].'"';
		}

		// pobranie obszarow zdefiniowanych dla serwisu
		$sSql = "SELECT id AS value, symbol AS label
						 FROM ".$aConfig['tabls']['prefix']."areas
						 WHERE subpage_area = '0'
						 ORDER BY number";
		$aAreas =& Common::GetAll($sSql);
		foreach ($aAreas as $iKey => $aArea) {
			if (isset($aConfig['lang']['c_areas'][$aArea['label']])) {
				$aAreas[$iKey]['label'] = $aConfig['lang']['c_areas'][$aArea['label']];
			}
		}

		// tworzenie formularza
		$pForm = new FormTable('c_boxes', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>160), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));

		// typ boksu
		$aTypes = array(
			array('value' => '1', 'label' => $aConfig['lang'][$_GET['file']]['btype_1']),
			array('value' => '4', 'label' => $aConfig['lang'][$_GET['file']]['btype_4']),
			array('value' => '2', 'label' => $aConfig['lang'][$_GET['file']]['btype_2']),
			array('value' => '3', 'label' => $aConfig['lang'][$_GET['file']]['btype_3']),
		);
		// typ boksu
		$pForm->AddSelect('btype', $aConfig['lang'][$_GET['file']]['btype'], array('onchange'=>'ChangeObjValue(\'do\', \'change_type\'); this.form.submit();'), $aTypes, $aData['btype'], '', false);

		if ($iId > 0) {
			// id starego obszaru
			$pForm->AddHidden('old_area_id', $aData['area_id']);
		}

		// obszar boksu
		$pForm->AddSelect('area_id', $aConfig['lang'][$_GET['file']]['area'], array(), $aAreas, $aData['area_id'], '', false);

		// nazwa
		$pForm->AddText('name', $aConfig['lang'][$_GET['file']]['name'], $aData['name'], array('maxlength'=>128, 'style'=>'width: 200px;'));

		switch ($aData['btype']) {
			case '1':
				// box menu
				// pobranie listy menu
				$sSql = "SELECT id AS value, name AS label
								 FROM ".$aConfig['tabls']['prefix']."menus
								 WHERE language_id = ".$this->iLangId."
								 ORDER BY id";
				$aMenus = array_merge(array(array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])), Common::GetAll($sSql));
				$pForm->AddSelect('menu_id', $aConfig['lang'][$_GET['file']]['menu'], array(), $aMenus, $aData['menu_id']);

				// pobranie listy szablonow
				$aTemplatesList =& GetTemplatesList($this->sTemplatesDir.'/m_menu');

				// konfiguracja boksu
				if (file_exists('boxes/m_menu/Box_settings.class.php')) {

					// dolaczenie pliku zarzadzajacego konfiguracja boksu
					include_once('boxes/m_menu/Box_settings.class.php');
					// dolaczenie pliku jezykowego modulu 'Menu'
					include_once('modules/m_menu/lang/admin_'.
											 $aConfig['default']['language'].'.php');
					$pSettings = new Settings('m_menu', $this->iLangId, $iId);
					$pSettings->SettingsForm($pForm);
				}
			break;

			case '4':
				// box menu - pierwszy poziom
				// pobranie listy menu
				$sSql = "SELECT id AS value, name AS label
								 FROM ".$aConfig['tabls']['prefix']."menus
								 WHERE language_id = ".$this->iLangId."
								 ORDER BY id DESC";
				$aMenus = array_merge(array(array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])), Common::GetAll($sSql));
				$pForm->AddSelect('menu_id', $aConfig['lang'][$_GET['file']]['menu'], array(), $aMenus, $aData['menu_id']);

				// pobranie listy szablonow
				$aTemplatesList =& GetTemplatesList($this->sTemplatesDir.'/m_menu_pierwszy_poziom');
			break;

			case '2':
				// box modulu
				$sModule = '';
				// pobranie listy modulow
				$sSql = "SELECT id AS value, symbol AS label
								 FROM ".$aConfig['tabls']['prefix']."modules
								 WHERE box = '1'
								 ORDER BY symbol";
				$aModules =& Common::GetAll($sSql);

				foreach ($aModules as $iKey => $aItem) {
					if (isset($aData['module_id']) && !empty($aData['module_id']) &&
							$aData['module_id'] == $aItem['value']) {
						$sModule = $aItem['label'];
					}
					if (isset($aConfig['lang']['c_modules'][$aItem['label']])) {
						$aModules[$iKey]['label'] = $aConfig['lang']['c_modules'][$aItem['label']];
					}
				}
				$aModules = array_merge(array(array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])), $aModules);
				$pForm->AddSelect('module_id', $aConfig['lang'][$_GET['file']]['module'], array('onchange'=>'ChangeObjValue(\'do\', \'change_type\'); this.form.submit();'), $aModules, $aData['module_id']);

				// pobranie listy szablonow
				if ($sModule != '') {
					$sTemplatesDir = $this->sTemplatesDir.'/'.$sModule;
					if (isset($aData['moption'])) {
						$sTemplatesDir .= '/'.$aData['moption'];
					}
					$aTemplatesList =& GetTemplatesList($sTemplatesDir);

					// pobranie mozliwych opcji modulu
					$aModuleOptions =& $this->getModuleOptions($sModule);
					if (!empty($aModuleOptions)) {
						$pForm->AddSelect('moption', $aConfig['lang'][$_GET['file']]['moption'], array('onchange'=>'ChangeObjValue(\'do\', \'change_type\'); this.form.submit();'), $aModuleOptions, $aData['moption'], '', false);
					}

					// konfiguracja boksu
					if (file_exists('boxes/'.$sModule.'/Box_'.(isset($aData['moption']) && !empty($aData['moption']) ? $aData['moption'].'_' : '').'settings.class.php')) {
						// dolaczenie pliku zarzadzajacego konfiguracja boksu
						include_once('boxes/'.$sModule.'/Box_'.(isset($aData['moption']) && !empty($aData['moption']) ? $aData['moption'].'_' : '').'settings.class.php');
						// dolaczenie pliku jezykowego modulu
						if (file_exists('modules/'.$sModule.'/lang/admin_'.
											 $aConfig['default']['language'].'.php')) {
							include_once('modules/'.$sModule.'/lang/admin_'.
												 $aConfig['default']['language'].'.php');
						}
						$pSettings = new Settings($aData['module_id'], $sModule, $this->iLangId, $iId, isset($aData['moption']) && !empty($aData['moption']) ? $aData['moption'] : '');
						$pSettings->SettingsForm($pForm);
					}
				}
			break;

			case '3':
				// box pusty
				// nazwa
				$pForm->AddText('symbol', $aConfig['lang'][$_GET['file']]['symbol'], $aData['symbol'], array('maxlength'=>32, 'style'=>'width: 200px;'));

				// pobranie listy szablonow
				$aTemplatesList =& GetTemplatesList($this->sTemplatesDir.'/empty');
			break;
		}
		
		// modul cache'owany
		// wyswietlanie miniaturki na liscie akatualnosci
		// szablon boksu
		$aCacheables = array(
			array('value' => '0', 'label' => $aConfig['lang'][$_GET['file']]['cacheable_0']),
			array('value' => '1', 'label' => $aConfig['lang'][$_GET['file']]['cacheable_1']),
			array('value' => '2', 'label' => $aConfig['lang'][$_GET['file']]['cacheable_2'])
		);
		$pForm->AddSelect('cacheable', $aConfig['lang'][$_GET['file']]['cacheable'], array(), $aCacheables, $aData['cacheable'], '', false);

		// cache'owanie dla calego serwisu czy dla kazdej strony
		$pForm->AddCheckbox('cache_per_page', $aConfig['lang'][$_GET['file']]['cache_per_page'], array(), '', !empty($aData['cache_per_page']), false);

		// cache'owanie dla calego serwisu czy dla kazdej strony
		$pForm->AddCheckbox('omit_memcached', $aConfig['lang'][$_GET['file']]['omit_memcached'], array(), '', !empty($aData['omit_memcached']), false);

		// szablon boksu
		$aTemplatesList = array_merge(
			array(array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])),
			$aTemplatesList
		);
		$pForm->AddSelect('template', $aConfig['lang'][$_GET['file']]['template'], array(), $aTemplatesList, $aData['template']);

		// sekcja ustawien domyslnych
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['default_section'], array('class'=>'merged', 'style'=>'padding-left: 165px;'));

		// wyswietlanie boksow
		$aStartPageModes = array(
			array('value' => '0', 'label'	=> $aConfig['lang'][$_GET['file']]['show_mode_0']),
			array('value' => '1', 'label'	=> $aConfig['lang'][$_GET['file']]['show_mode_1'])
		);
		$aSubpageModes = array(
			array('value' => '0', 'label'	=> $aConfig['lang'][$_GET['file']]['show_mode_0']),
			array('value' => '4', 'label'	=> $aConfig['lang'][$_GET['file']]['show_mode_4']),
			array('value' => '1', 'label'	=> $aConfig['lang'][$_GET['file']]['show_mode_1']),
			array('value' => '2', 'label'	=> $aConfig['lang'][$_GET['file']]['show_mode_2']),
			array('value' => '3', 'label'	=> $aConfig['lang'][$_GET['file']]['show_mode_3'])
		);
		// wyswietlany na stronie glownej
		$pForm->AddSelect('start_page', $aConfig['lang'][$_GET['file']]['start_page'], array(), $aStartPageModes, $aData['start_page'], '', false);

		// wyswietlany na podstronach
		$pForm->AddSelect('subpage', $aConfig['lang'][$_GET['file']]['subpage'], array(), $aSubpageModes, $aData['subpage'], '', false);

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).
														 '&nbsp;&nbsp;'.
														 $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEditModule() function


	/**
	 * Metoda wyswietla liste zdefiniowanych dla serwisu boksow
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		$sContent = '';
		$aRecords = array();

		// zapamietanie opcji
		rememberViewState($_GET['file']);

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'header'	=> $aConfig['lang'][$_GET['file']]['list'],
			'refresh'	=> true,
			'search'	=> false,
			'per_page'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '20'
			),
			array(
				'db_field'	=> 'symbol',
				'content'	=> $aConfig['lang'][$_GET['file']]['list_name'],
				'sortable'	=> false,
        'width'	=> '40%'
			),
			array(
				'db_field'	=> 'btype',
				'content'	=> $aConfig['lang'][$_GET['file']]['list_type'],
				'sortable'	=> false,
         'width'	=> '200'
			),
			array(
				'db_field'	=> 'area',
				'content'	=> $aConfig['lang'][$_GET['file']]['list_area'],
				'sortable'	=> false,
        'width'	=> '300'
			),
			array(
				'db_field'	=> 'cache_info',
				'content'	=> $aConfig['lang'][$_GET['file']]['list_cache_info'],
				'sortable'	=> false,
        'width'	=> '500'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array(
			'id'	=> array(
				'show'	=> false
			),
			'name' => array (
				'link'	=> phpSelf(array('do'=>'edit', 'id'=>'{id}'))
			),
			'action' => array (
				'actions'	=> array ('preview', 'edit', 'delete'),
				'params'	=> array (
											'edit'	=>	array('id' => '{id}'),
											'delete'	=>	array('id' => '{id}')
				),
				'show' => false
			)
		);

		// pobranie listy wszystkich obszarow
		$sSql = "SELECT id, symbol
						 FROM ".$aConfig['tabls']['prefix']."areas
						 ORDER BY number";
		$aAreas =& Common::GetAll($sSql);

		if (!empty($aAreas)) {
			$i = 0;
			foreach ($aAreas as $iKey => $aArea) {
				// pobranie wszystkich boksow dla obszaru
				$sSql = "SELECT id, name, btype, '' AS area, cacheable, omit_memcached, cache_per_page
								 FROM ".$aConfig['tabls']['prefix']."boxes
								 WHERE language_id = ".$this->iLangId." AND
								 			 area_id = ".$aArea['id']."
								 ORDER BY order_by";
				$aRecords =& Common::GetAll($sSql);
				if (!empty($aRecords)) {
					foreach ($aRecords as $iKey => $aItem) {
            $aRecords[$iKey]['area'] = $aConfig['lang']['c_areas'][$aArea['symbol']];
						$aRecords[$iKey]['btype'] = $aConfig['lang'][$_GET['file']]['btype_'.$aItem['btype']];
            $aRecords[$iKey]['cache_info'] = 
                    $aConfig['lang'][$_GET['file']]['cacheable_'.$aItem['cacheable']].' '.
                    ($aItem['cache_per_page'] == '1' ? $aConfig['lang'][$_GET['file']]['cache_per_page'] : '').' '.
                    ($aItem['omit_memcached'] == '1' ? 'Boks nie cachowany w memcached' : '');
            
            unset($aRecords[$iKey]['cacheable'], 
                  $aRecords[$iKey]['omit_memcached'],
                  $aRecords[$iKey]['cache_per_page']);
            $aRecords[$iKey]['action']['links']['preview'] = 'internals/getOneBox.class.php?box_id='.$aItem['id'].'&lang_id='.$this->iLangId;
					}
					$aHeader['header'] = sprintf($aConfig['lang'][$_GET['file']]['area_boxes_list'],
																			 isset($aConfig['lang']['c_areas'][$aArea['symbol']]) ? $aConfig['lang']['c_areas'][$aArea['symbol']] : $aArea['symbol']);
					$pView = new View($aArea['symbol'].'_boxes', $aHeader, $aAttribs);
					$pView->AddRecordsHeader($aRecordsHeader);

					// dodanie rekordow do widoku
					$pView->AddRecords($aRecords, $aColSettings);

					// dodanie stopki do widoku
					$aRecordsFooter = array(
						array('check_all', 'delete_all'),
						array('sort', 'add')
					);
					$aRecordsFooterParams = array(
						'sort' => array(array('aid' => $aArea['id'])),
						'add' => array(array('aid' => $aArea['id']))
					);
					$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

					$sContent .= ($i == 0 && !empty($this->sMsg) ? $this->sMsg : '').$pView->Show();
					$i++;
				}
			}
		}
		if ($sContent === '') {
			$pView = new View('boxes', $aHeader, $aAttribs);
			$pView->AddRecordsHeader($aRecordsHeader);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);

			// dodanie stopki do widoku
			$aRecordsFooter = array(
				array(),
				array('add')
			);
			$pView->AddRecordsFooter($aRecordsFooter);

			$sContent .= (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show();
		}
		$pSmarty->assign('sContent', $sContent);
	} // end of Show() function


	/**
	 * Metoda pobiera liste opcji modulu na podstawie plikow php z klasami
	 * z katalogu client boksu o przekazanym do metody symbolu
	 *
	 * @param	string	$sModule	- symbol modulu
	 */
	function &getModuleOptions($sModule) {
		global $aConfig;
		$aOptions = array();

		$sDir = $aConfig['common']['base_path'].'boxes/'.$sModule.'/client';
		$pDir = opendir($sDir);
		$i = 0;
  	while (false !== ($sFile = readdir($pDir))) {
    	if (preg_match('/^Box_\w+\.class.php$/i', $sFile)) {
    		$sFile = substr($sFile,
    										strpos($sFile, '_') + 1,
    										strpos($sFile, '.') - strpos($sFile, '_') - 1);

    		$aOptions[$i] = array('value' => $sFile,
    													'label' => $sFile
    												 );
      	if (isset($aConfig['lang']['c_boxes'][$sModule.'_'.$sFile])) {
					$aOptions[$i]['label'] = $aConfig['lang']['c_boxes'][$sModule.'_'.$sFile];
				}
				$i++;
      }
    }
    closedir($pDir);

    if (!empty($aOptions)) {
    	$aOptions = array_merge(
    		array(array('value' => '', 'label' => $aConfig['lang']['c_boxes']['default'])),
    		$aOptions
    	);
    }
    return $aOptions;
	} // end of getModuleOptions() method


	/**
	 * Metoda pobiera nazwe obszaru na podstawie podanego Id
	 *
	 * @param	integer	$iId	- Id obszaru
	 * @return	string	- nazwa obszaru
	 */
	function getAreaName($iId) {
		global $aConfig;

		// nazwa obszaru
		$sSql = "SELECT symbol
						 FROM ".$aConfig['tabls']['prefix']."areas
						 WHERE id = ".$iId;
		$sSymbol = Common::GetOne($sSql);
		return isset($aConfig['lang']['c_areas'][$sSymbol]) ? $aConfig['lang']['c_areas'][$sSymbol] : $sSymbol;
	} // end of getAreaName() method


	/**
	 * Metoda pobiera liste rekordow
	 *
	 * @param	integer	$iId	- Id obszaru
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($iId) {
		global $aConfig;

		// pobranie wszystkich boksow dla danego obszaru
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."boxes
						 WHERE language_id = ".$this->iLangId." AND
						 			 area_id = ".$iId."
						 ORDER BY order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
} //end of Core class
?>