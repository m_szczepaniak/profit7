<?php
/**
 * Klasa Core do obslugi obszarow serwisu.
 * Zarzadzanie obszarami jest dozwolone tylko dla
 * superuzytkownika
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Core {

	// komunikat
	var $sMsg;

	// ID serwisu
	var $iWebsiteId;

	// Id wersji jezykowej
	var $iLangId;

	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iWebsiteId = $_SESSION['site']['id'];
		$this->iLangId = $_SESSION['lang']['id'];

		$sAction = '';
		$iId = 0;

		if (isset($_GET['do'])) {
			$sAction = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sAction = $_POST['do'];
		}
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}

		if ($_SESSION['user']['type'] == 1) {
			switch ($sAction) {
				case 'delete': $this->DeleteAreas($pSmarty, $iId); break;
				case 'show': $this->ShowAreas($pSmarty); break;
				case 'insert': $this->InsertArea($pSmarty); break;
				case 'update': $this->UpdateArea($pSmarty); break;
				case 'edit': $this->AddEditArea($pSmarty, $iId); break;
				case 'add': $this->AddEditArea($pSmarty); break;
				default: $this->ShowAreas($pSmarty); break;
			}
		}
		else {
			$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
		}
	} // end Core() function


	/**
	 * Metoda usuwa wybrany(e) obszar(y)
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID usuwanego obszaru
	 * @return 	void
	 */
	function DeleteAreas(&$pSmarty, $iId=0) {
		global $aConfig;
		$bIsErr = false;

		if ($iId > 0) {
			$_POST['delete'][$iId] = 'on';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();

			// pobranie symboli usuwanych obszarow
			$sSql = "SELECT symbol
					 		 FROM ".$aConfig['tabls']['prefix']."areas
						 	 WHERE id IN (".implode(',', $_POST['delete']).")";
			$aRecords =& Common::GetCol($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				foreach ($aRecords as $sSymbol) {
					if (isset($aConfig['lang'][$_GET['file']][$sSymbol])) {
						$sSymbol = $aConfig['lang'][$_GET['file']][$sSymbol];
					}
					$sDel .= '"'.$sSymbol.'", ';
				}
				$sDel = substr($sDel, 0, -2);
			}

			if (!$bIsErr) {
				// usuwanie obszaru(ow)
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."areas
								 WHERE id IN (".implode(',', $_POST['delete']).")";
				if (count($_POST['delete']) == 1) {
					$sErrPostfix = '0';
				}
				else {
					$sErrPostfix = '1';
				}
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_ok_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_err_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg);
			}
		}
		$this->ShowAreas($pSmarty);
	} // end of DeleteAreas() funciton

	/**
	 * Metoda dodaje do bazy danych nowy obszar
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function InsertArea(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditArea($pSmarty);
			return;
		}
		// sprawdzenie czy obszar o podanym symbolu nie istnieje w bazie danych
		if (!$this->AreaExists($_POST['symbol'], $_POST['number'])) {
			$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."areas (
							 symbol,
							 number,
							 subpage_area
						  )
						  VALUES (
							 '".$_POST['symbol']."',
							 ".$_POST['number'].",
							 '".(isset($_POST['subpage_area']) ? '1' : '0')."'
						  )";
			if (isset($aConfig['lang'][$_GET['file']][$_POST['symbol']])) {
				$_POST['gen_symbol'] = $_POST['symbol'];
				$_POST['symbol'] = $aConfig['lang'][$_GET['file']][$_POST['symbol']];
			}
			if (Common::Query($sSql) !== false) {
				// obszar zostal dodany, wyswietlenie komunikatu o powodzeniu
				// oraz wyswietlenie listy obszarow
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_ok'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg, false);
				AddLog($sMsg, false);
				// resetowanie stanu widoku
				$_GET['reset'] = 2;
				$this->ShowAreas($pSmarty);
			}
			else {
				// obszar nie zostal dodany, wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_err'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEditArea($pSmarty);
			}
		}
		else {
			// obszar o podanym symbolu juz istnieje
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_err2'],
											$_POST['symbol'],
											$_POST['number']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEditArea($pSmarty);
		}
	} // end of InsertArea() funciton


	/**
	 * Metoda wprowadza zmiany w obszarze do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateArea(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditArea($pSmarty);
			return;
		}

		// sprawdzenie czy modul o podanej nazwie nie istnieje w bazie danych
		if (!$this->AreaExists($_POST['symbol'], $_POST['number'], $_GET['id'])) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."areas SET
								symbol = '".$_POST['symbol']."',
								number = ".$_POST['number'].",
								subpage_area = '".(isset($_POST['subpage_area']) ? '1' : '0')."'
						   WHERE id=".$_GET['id'];

			if (isset($aConfig['lang'][$_GET['file']][$_POST['symbol']])) {
				$_POST['gen_symbol'] = $_POST['symbol'];
				$_POST['symbol'] = $aConfig['lang'][$_GET['file']][$_POST['symbol']];
			}
			if (Common::Query($sSql) !== false) {
				// obszar zostal zmodyfikowany, wyswietlenie komunikatu o powodzeniu
				// oraz wyswietlenie listy obszarow
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['edit_ok'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg, false);
				AddLog($sMsg, false);
				$this->ShowAreas($pSmarty);
			}
			else {
				// nie wprowadzono zadnych zmian, wyswietlenie komunikatu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['edit_err'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEditArea($pSmarty, $_GET['id']);
			}
		}
		else {
			// obszar o podanym symbolu lub numerze juz istnieje
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_err2'],
											$_POST['symbol'],
											$_POST['number']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEditArea($pSmarty);
		}
	} // end of UpdateArea() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji obszaru
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	ID edytowanego obszaru
	 */
	function AddEditArea(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego obszaru
			$sSql = "SELECT symbol, number, subpage_area
							 FROM ".$aConfig['tabls']['prefix']."areas
							 WHERE id=".$iId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$_GET['file']]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['symbol']) && !empty($aData['symbol'])) {
			if (isset($aConfig['lang'][$_GET['file']][$aData['symbol']]) &&
					!empty($aConfig['lang'][$_GET['file']][$aData['symbol']])) {
				$sHeader .= ' "'.$aConfig['lang'][$_GET['file']][$aData['symbol']].'"';
			}
			else {
				$sHeader .= ' "'.$aData['symbol'].'"';
			}
		}

		// tworzenie formularza
		$pForm = new FormTable('areas', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>160), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));

		// symbol obszaru
		$pForm->AddText('symbol', $aConfig['lang'][$_GET['file']]['symbol'], $aData['symbol'], array('maxlength'=>32, 'style'=>'width: 200px;'));

		// numer obszaru
		$pForm->AddText('number', $aConfig['lang'][$_GET['file']]['number'], $aData['number'], array('maxlength'=>2, 'style'=>'width: 50px;'), '', 'uinteger');

		// jest glownym obszarem podstron
		$pForm->AddCheckBox('subpage_area', $aConfig['lang'][$_GET['file']]['main_area'], array(), '', !empty($aData['subpage_area']), false);

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).
														 '&nbsp;&nbsp;'.
														 $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEditArea() function


	/**
	 * Metoda wyswietla liste zdefiniowanych dla serwisu obszarow
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function ShowAreas(&$pSmarty) {
		global $aConfig;

		// zapamietanie opcji
		rememberViewState($_GET['file']);

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'header'	=> $aConfig['lang'][$_GET['file']]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '30'
			),
			array(
				'db_field'	=> 'symbol',
				'content'	=> $aConfig['lang'][$_GET['file']]['name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'number',
				'content'	=> $aConfig['lang'][$_GET['file']]['number'],
				'sortable'	=> true,
				'width'	=> '80'
			),
			array(
				'content'	=> $aConfig['lang'][$_GET['file']]['main_area'],
				'sortable'	=> false,
				'width'	=> '125'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich obszarow
		$sSql = "SELECT COUNT(*)
						 FROM ".$aConfig['tabls']['prefix']."areas
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND symbol LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($_GET['file']);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(*)
							 FROM ".$aConfig['tabls']['prefix']."areas";
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('areas', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich modulow dla danego serwisu
			$sSql = "SELECT id, symbol, number, IF (subpage_area = '0', '".$aConfig['lang']['common']['no']."', '".$aConfig['lang']['common']['yes']."') as subpage_area
							 FROM ".$aConfig['tabls']['prefix']."areas
							 WHERE TRUE".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND symbol LIKE \'%'.$_POST['search'].'%\'' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'number').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'symbol' => array (
					'link'	=> phpSelf(array('do'=>'edit', 'id'=>'{id}')),
					'use_lang'	=> true
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params'	=> array (
												'edit'	=>	array('id' => '{id}'),
												'delete'	=>	array('id' => '{id}')
					)
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('add')
		);
		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of ShowAreas() function


	/**
	 * Metoda sprawdza czy obszar o podanym symbolu istnieje
	 *
	 * @param		string	$sSymbol	- symbol obszaru
	 * * @param		string	$iNumber	- numer obszaru
	 * @param		integer	$iId	- ID obszaru dla ktorego pomijane jest sprawdzanie
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function AreaExists($sSymbol, $iNumber, $iId=0) {
		global $aConfig;

		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."areas
						 WHERE (symbol = '".$sSymbol."' OR
						 			 number = '".$iNumber."')".
									 ($iId > 0 ? " AND id != ".$iId : '');

		return intval(Common::GetOne($sSql)) == 1;
	} // end of AreaExists() function
} //end of Core class
?>