<?php
/**
* Plik jezykowy dla interfejsu Core Areas Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'OBSZARY' --------------*/
$aConfig['lang']['c_areas']['list']		= "Lista obszarów";
$aConfig['lang']['c_areas']['name']		= "Symbol / nazwa obszaru";
$aConfig['lang']['c_areas']['number']		= "Numer";
$aConfig['lang']['c_areas']['no_items']	= "Brak zdefiniowanych obszarów";

$aConfig['lang']['c_areas']['edit']	= "Edytuj obszar";
$aConfig['lang']['c_areas']['delete']	= "Usuń obszar";
$aConfig['lang']['c_areas']['delete_q']	= "Czy na pewno usunąć wybrany obszar?";
$aConfig['lang']['c_areas']['check_all']	= "Zaznacz / odznacz wszystkie";
$aConfig['lang']['c_areas']['delete_all']	= "Usuń zaznaczone";
$aConfig['lang']['c_areas']['delete_all_q']	= "Czy na pewno usunąć wybrane obszary?";
$aConfig['lang']['c_areas']['delete_all_err']	= "Nie wybrano żadnego obszaru do usunięcia!";
$aConfig['lang']['c_areas']['add']	= "Dodaj obszar";

$aConfig['lang']['c_areas']['header_0']	= "Dodawanie obszaru";
$aConfig['lang']['c_areas']['header_1']	= "Edycja obszaru";
$aConfig['lang']['c_areas']['symbol']	= "Symbol obszaru";
$aConfig['lang']['c_areas']['main_area']	= "Główny podstrony";

$aConfig['lang']['c_areas']['add_ok']	= "Obszar \"%s\" został dodany do bazy danych";
$aConfig['lang']['c_areas']['add_err']	= "Nie udało się dodać obszaru \"%s\" do bazy danych!<br>Spróbuj ponownie";
$aConfig['lang']['c_areas']['add_err2']= "Obszar o podanym symbolu \"%s\" lub numerze \"%d\" już istnieje!<br>Wybierz inny symbol";
$aConfig['lang']['c_areas']['del_ok_0']	= "Obszar %s został usunięty";
$aConfig['lang']['c_areas']['del_ok_1']	= "Obszary %s zostały usunięte";
$aConfig['lang']['c_areas']['del_err_0']	= "Nie udało się usunąć obszaru %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_areas']['del_err_1']	= "Nie udało się usunąć obaszrów %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_areas']['edit_ok']	= "Zmiany w obszarze \"%s\" zostały wprowadzone do bazy danych";
$aConfig['lang']['c_areas']['edit_err']= "Nie wprowadzono żadnych zmian w obszarze \"%s\"";
?>
