<?php
/**
 * Klasa do obsługi cronów systemu
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');
class Core {

    // komunikat
    var $sMsg;

    // ID wersji jezykowej serwisu
    var $iLangId;

    // nazwa modulu
    var $sModule;

    /**
     * Konstruktor klasy
     *
     * @return	void
     */
    function Core(&$pSmarty, $bTest = false) {
        global $aConfig;
        if ($bTest === true) {
            return true;
        }

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];

        $sDo = '';
        $iId = 0;

        $this->sModule = $_GET['file'];

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['id'])) {
            $iId = $_GET['id'];
        }

        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia
        if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {

            switch ($sDo) {
                case 'start':
                    $this->startProcess($iId);
                    $this->Show($pSmarty);
                    break;
                default:
                    $this->Show($pSmarty);
                    break;
            }
        }
        else {
            $pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
        }
    } // end Core() function


    /**
     * Metoda uruchamia nowy proces crona
     *
     * @param type $iProcessId
     */
    public function startProcess($iProcessId) {
        $sRefReturn = null;
        $aCrons = $this->_getCronsData();
        $sPHPFile = $this->_getCommandFromCrontab($aCrons[$iProcessId]);
        exec(' cd ~; ' . $sPHPFile . ' & echo $!');
        $this->sMsg = GetMessage(_("Polecenie \"".$sPHPFile."\" zostało uruchomionen ręcznie"), false);
        AddLog($this->sMsg, false);
    }// end of startProcess() method


    /**
     * Metoda pobiera polecenie z crontab'a
     *
     * @test
     * @param type $sCronCommand
     * @return array
     */
    public function _getCommandFromCrontab($sCronCommand) {

        $aMatches = array();
        preg_match('/^.*(\s+)?(php[\d]* -f.*)/', $sCronCommand, $aMatches);
        return $aMatches[2];
    }// end of _getCommandFromCrontab() method


    /**
     * Metoda pobiera dane crona
     *
     * @return array Tablica poleceń cron
     */
    public function _getCronsData() {

        $aCrontab = null;
        exec('crontab -l', $aCrontab);
        return $this->_getCommandsArr($aCrontab);
    }// end of getCronsData() method


    /**
     * Metoda pobiera listę procesów uruchomionych na serwerze
     *
     * @param array $aCrontabList
     * @return array $aWorkingProcess
     */
    public function _getWorkingProcesslist($aCrontabList) {
        $aCommandList = null;
        $user = get_current_user();
        $command = ' ps -Ao command:200,user,pid|grep "'.$user.'"|grep "[\.]php"';
        exec($command, $aCommandList);
        foreach ($aCommandList as &$mCommand) {
            $aPIdMatches = array();
            preg_match('/(\s+)(\d+)$/', $mCommand, $aPIdMatches);
            $iPId = 0;
            if (isset($aPIdMatches[2]) && intval($aPIdMatches[2]) > 0) {
                $iPId = intval($aPIdMatches[2]);
            }
            $sPHPFile = $this->_getFilePhpFromProcess($mCommand);

            // znajdźmy crona
            $iCronId = 0;
            foreach ($aCrontabList as $iKey => $sCron) {
                $sMat = '';
                if ($sPHPFile != '') {
                    $sMat = stristr($sCron, $sPHPFile);
                }
                if (!empty($sMat)) {
                    $iCronId = $iKey;
                }
            }

            $mCommand = array(
                'command' => $sPHPFile,
                'pid' => $iPId,
                'cron_id' => $iCronId
            );
        }
        return $aCommandList;
    }// end of _getWorkingProcesslist() method

    /**
     * Metoda pobiera nazwę pliku PHP z procesu
     *
     * @param type $mCommand
     * @return type
     */
    private function _getFilePhpFromProcess($mCommand) {

        $aCommMatches = array();
        preg_match('/[\/\s]([\w_]+(\.class)?\.php)/', $mCommand, $aCommMatches);
        $sPHPFile = trim($aCommMatches[1]);
        return $sPHPFile;
    }// end of _getFilePhpFromProcess method

    /**
     * Metoda pobiera polecenia
     *
     * @param type $sCrontab
     * @return type
     */
    private function _getCommandsArr($aCrontab) {
        foreach ($aCrontab as $iKey => $sCommand) {
            $aCrontab[$iKey] = trim($sCommand);
        }
        return $aCrontab;
    }// end of _getCommandsArr() method


    /**
     * Metoda sprawdza czy cron znajduje się na liście aktywnych procesów
     *
     * @param int $iPId
     * @param array $aProcesslist
     * @return int
     */
    private function _findProcessOnActiveProcesslist($iPId, $aProcesslist) {

        foreach ($aProcesslist as $iKey => $aProcess) {
            if ($aProcess['cron_id'] >= 0 && $aProcess['cron_id'] === $iPId) {
                $iProcPId = $aProcess['pid'];
                return $iProcPId;
            }
        }
        return false;
    }// end of _findProcessOnActiveProcesslist() method


    /**
     * Metoda tworzy formularz edycji konfiguracji aktualnosci
     *
     * @param		object	$pSmarty
     */
    function Show(&$pSmarty) {
        global $aConfig;

        // zapamietanie opcji
        rememberViewState($_GET['file']);

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header'	=> $aConfig['lang'][$_GET['file']]['list'],
            'refresh'	=> true,
            'search'	=> false,
            'checkboxes' => false,
            'per_page' => false
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 0,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field'	=> 'name',
                'content'	=> $aConfig['lang'][$_GET['file']]['name'],
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'minutes',
                'content'	=> $aConfig['lang'][$_GET['file']]['minutes'],
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'hours',
                'content'	=> $aConfig['lang'][$_GET['file']]['hours'],
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'days',
                'content'	=> $aConfig['lang'][$_GET['file']]['days'],
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'full-command',
                'content'	=> $aConfig['lang'][$_GET['file']]['full-command'],
                'sortable'	=> false
            ),
            array(
                'content'	=> $aConfig['lang']['common']['action'],
                'sortable'	=> false,
                'width'	=> '45'
            )
        );



        $pView = new View('languages', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);

        $aCommands = $this->_getCronsData();
        $aProcesslist = $this->_getWorkingProcesslist($aCommands);
        $iRowCount = count($aCommands);

        if ($iRowCount > 0) {
            foreach ($aCommands as $iKey => &$sCommand) {
                $aData = $this->_parseCommand($sCommand);
                $iCronProcessId = $this->_findProcessOnActiveProcesslist($iKey, $aProcesslist);
                if (!empty($aData)) {
                    $aData['id'] = $iKey;
                    $aCommands[$iKey] = $aData;
                    if ($iCronProcessId !== false && $iCronProcessId > 0) {
                        $aCommands[$iKey]['disabled'] = array('0' => 'start');
                    }
                } else {
                    unset($aCommands[$iKey]);
                }
                $aData['full-command'] = trim($aData['full-command']);
                if (mb_substr($aData['full-command'], 0, 1) == '#') {
                    unset($aCommands[$iKey]);
                }
            }


            // ustawienia dla poszczegolnych kolumn rekordu
            $aColSettings = array(
                'id'	=> array(
                    'show'	=> false
                ),
                'action' => array (
                    'actions'	=> array ('start'),
                    'params'	=> array (
                        'start'	=> array('id' => '{id}')
                    )
                )
            );
            // dodanie rekordow do widoku
            $pView->AddRecords($aCommands, $aColSettings);
        }
        // dodanie stopki do widoku
        $aRecordsFooter = array();
        $pView->AddRecordsFooter($aRecordsFooter);

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
            $pView->Show());
    } // end of Show() function


    /**
     * Metoda zamienia polecenie w tablicę pasującą do wyświetlenia listy poleceń
     *
     * @param string $sRefCommand
     */
    private function _parseCommand(&$sRefCommand) {
        $aMatches = array();

        $sRefCommand = trim($sRefCommand);
        $aCommand = preg_split('/[ \\t]+/', $sRefCommand);
        preg_match('/\/([^\/\.]*)\.\w+(\.\w*)?$/', $aCommand[7], $aMatches);
        if (count($aCommand) < 3 || empty($aMatches[1])) {
            unset($sRefCommand);
            return null;
        }


        $aListCommand = array();
        $aListCommand['name'] = $aMatches[1];
        $aListCommand['minutes'] = $aCommand[0];
        $aListCommand['hours'] = $aCommand[1];
        $aListCommand['days'] = $aCommand[2];
        $aListCommand['full-command'] = $sRefCommand;
        return $aListCommand;
    }// end of _parseCommand method

} //end of Core class
?>