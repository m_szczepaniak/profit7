<?php
/**
* Plik jezykowy dla interfejsu Core Main Settings Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'KONFIGURACJA SERWISU' --------------*/
$aConfig['lang']['c_crons']['list']				= "Lista zadań";

$aConfig['lang']['c_crons']['name']	= "Nazwa";
$aConfig['lang']['c_crons']['hours']	= "Godzina";
$aConfig['lang']['c_crons']['minutes']	= "Minuta";
$aConfig['lang']['c_crons']['days']	= "Dzień";
$aConfig['lang']['c_crons']['full-command']	= "Pełne polecenie";
$aConfig['lang']['c_crons']['start']	= "Uruchom";
?>
