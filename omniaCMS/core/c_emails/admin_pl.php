<?php
/**
* Plik jezykowy dla interfejsu Core Messages Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'KOMUNIKATY' --------------*/
$aConfig['lang']['c_emails']['list']		= "Lista emaili";
$aConfig['lang']['c_emails']['add']	= "Dodaj email";
$aConfig['lang']['c_emails']['no_items']	= "Brak zdefiniowanych emaili";
$aConfig['lang']['c_emails']['symbol']= "Symbol emaila";
$aConfig['lang']['c_emails']['subject']= "Temat emaila";
$aConfig['lang']['c_emails']['template']= "Szablon emaila";
$aConfig['lang']['c_emails']['content']= "Treść emaila";
$aConfig['lang']['c_emails']['use_header']= "Używaj nagłówka";
$aConfig['lang']['c_emails']['use_footer']= "Używaj stopki";
$aConfig['lang']['c_emails']['header_0']	= "Dodawanie emaila";
$aConfig['lang']['c_emails']['header_1']	= "Edycja emaila";
$aConfig['lang']['c_emails']['merlin_statuses']= "Statusy  merlina";

$aConfig['lang']['c_emails']['add_ok']	= "Email \"%s\" został dodany do bazy danych";
$aConfig['lang']['c_emails']['delete']	= "Usuń email";
$aConfig['lang']['c_emails']['delete_q']	= "Czy na pewno usunąć wybrany email?";
$aConfig['lang']['c_emails']['check_all']	= "Zaznacz / odznacz wszystkie";
$aConfig['lang']['c_emails']['delete_all']	= "Usuń zaznaczone";
$aConfig['lang']['c_emails']['delete_all_q']	= "Czy na pewno usunąć wybrane emaile?";
$aConfig['lang']['c_emails']['delete_all_err']	= "Nie wybrano żadnego emaila do usunięcia!";
$aConfig['lang']['c_emails']['del_ok_0']	= "Email %s został usunięty";
$aConfig['lang']['c_emails']['del_ok_1']	= "Email %s zostały usunięte";
$aConfig['lang']['c_emails']['del_err_0']	= "Nie udało się usunąć emaila %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_emails']['del_err_1']	= "Nie udało się usunąć emaila %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_emails']['edit_ok']	= "Zmiany w emailu \"%s\" zostały wprowadzone do bazy danych";
$aConfig['lang']['c_emails']['edit_err']= "Nie wprowadzono żadnych zmian w emailu \"%s\"";

$aConfig['lang']['c_emails']['add_err']	= "Nie udało się dodać emaila \"%s\" do bazy danych!<br>Spróbuj ponownie";
$aConfig['lang']['c_emails']['add_err2']= "Emaila o podanym symbolu \"%s\" już istnieje!<br>Wybierz inny symbol";


?>
