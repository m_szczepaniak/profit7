<?php
/**
 * Klasa Core do obslugi ustawien serwisu
 * Zarzadzanie uzytkownikami jest dostepne tylko dla
 * superuzytkownika lub administratorow serwisow
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Core {

	// komunikat
	var $sMsg;

	// ID wersji jezykowej serwisu
	var $iLangId;

	// nazwa modulu
	var $sModule;

	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];

		$sDo = '';
		$iId = 0;

		$this->sModule = $_GET['file'];

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}

	

		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia
		if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {

			switch ($sDo) {
				case 'update':
					$this->UpdateSettings($pSmarty);
				break;

				default:
					$this->Settings($pSmarty);
				break;
			}
		}
		else {
			$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
		}
	} // end Core() function


	/**
	 * Metoda wprowadza zmiany w konfiguracji serwisu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Settings($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Settings($pSmarty);
			return;
		}
		// rozpoczecie transkacji
		Common::BeginTransaction();
		$sSql = "TRUNCATE TABLE users_accounts_postals";
		if(Common::Query($sSql) === false){
	       		$bIsErr = true;
	       		dump($sSql);
	  }
	  
		// otwarcie pliku i wyszukiwanie adresow e-mail w jego zawartosci
		if (!$bIsErr && @$rFile = fopen($_FILES['postals']['tmp_name'], 'r')) {
			while(!feof($rFile)) {
				$sBuffer = fgets($rFile);
				// rozdziel sekcje csv
				$aLine=preg_split("/,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/",$sBuffer);
				if(!empty($aLine[0]) && !empty($aLine[1])){
					$sPostal = preg_replace("/[\"\\'\.]/","",$aLine[1]);
	       	
	       	$aValues = array(
	       		'city' => mb_convert_case(strtolower(preg_replace("/[\"\\'\.]/","",$aLine[0])), MB_CASE_TITLE, "UTF-8"),
	       		'postal' => substr($sPostal,0,2).'-'.substr($sPostal,2,3)
	       	);
	       	if(Common::Insert($aConfig['tabls']['prefix']."users_accounts_postals",$aValues,'',false) === false){
	       		$bIsErr = true;
	       		dump($aValues);
	       	}
	       }
			}
			fclose($rFile);
		}
		else {
			$bIsErr = true;
		}
		//echo memory_get_peak_usage()."<br />";
		//echo memory_get_peak_usage(true);
		if (!$bIsErr) {
			// dodano adresy
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();

			AddLog($sMsg, false);
			$this->Settings($pSmarty);
		}
		else {
			// nie dodano adresow
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err']);
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->Settings($pSmarty);
		}
	} // end of UpdateSettings() funciton


	/**
	 * Metoda tworzy formularz edycji konfiguracji aktualnosci
	 *
	 * @param		object	$pSmarty
	 */
	function Settings(&$pSmarty) {
		global $aConfig;

			$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header'];

		$pForm = new FormTable('import_statements', $sHeader, array('action'=>phpSelf(), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');

		// nazwa
		$pForm->AddFile('postals', $aConfig['lang'][$this->sModule]['file'],  array('style'=>'width: 350px;'));

		$sFormat = '
Żarówka;39-312
Dąbrówka Wisłocka;39-315
Łączki Brzeskie;39-315
Ruda;39-315
Warszawa;00-001
Warszawa;00-002
Warszawa;00-003
			';
		$pForm->AddMergedRow('<div style="text-align: left; font-size: 10px;"> format CSV: <br />'.nl2br($sFormat).'</div>');
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_0']));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Settings() function
} //end of Core class
?>