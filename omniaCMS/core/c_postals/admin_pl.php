<?php
/**
* Plik jezykowy dla interfejsu Core Main Settings Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'KONFIGURACJA SERWISU' --------------*/
$aConfig['lang']['c_postals']['header']				= "Kody pocztowe";

$aConfig['lang']['c_postals']['file']	= "Plik csv z miejscowościami i kodami";

$aConfig['lang']['c_postals']['add_ok']	= "Nowa lista kodów pocztowych została wprowadzona do bazy danych";
$aConfig['lang']['c_postals']['add_err']	= "Nie udało się zapisać listy kodów pocztowych w bazie danych!<br>Spróbuj ponownie";

?>
