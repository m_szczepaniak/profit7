<?php
/**
 * Klasa Core do obslugi historii zdarzen
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Core {
	
	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;
		
		$this->Show($pSmarty);
	} // end Core() function

	
	/**
	 * Metoda wyswietla historie zdarzen
	 * Dla administratora - dostep do historii zdarzen wszystkich uzytkownikow
	 * z wyjatkiem sadmin
	 * Dla zwyklego uzytkownika dostep tylko do historii zdarzen uzytkownika
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		$sWhereSql = '';
		$sUsersWhereSql = '';
		$sDateSql = '';
		
		// zapamietanie opcji
		rememberViewState($_GET['file']);
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		
		$aHeader = array(
			'header'	=> $aConfig['lang'][$_GET['file']]['list'],
			'refresh'	=> true,
			'search'	=> false,
			'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'login',
				'content'	=> $aConfig['lang'][$_GET['file']]['login'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'added',
				'content'	=> $aConfig['lang'][$_GET['file']]['added'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'event',
				'content'	=> $aConfig['lang'][$_GET['file']]['event'],
				'sortable'	=> true
			)
		);
		
		if ($_SESSION['user']['type'] === 0) {
			// zwykly uzytkownik - brak informacji o historii zdarzen
			// innych uzytkownikow - usuniecie kolumny login
			unset($aRecordsHeader[0]);
			
			$sWhereSql = " AND user_id = ".$_SESSION['user']['id'];
		}
		elseif ($_SESSION['user']['type'] === 2) {
			// zwykly administrator - brak informacji o uzytkowniku sadmin
			$sWhereSql = " AND user_id <> 1";
			// wylaczenie uzytkownika sadmin z historii zdarzen
			$sUsersWhereSql = " WHERE user_id <> 1";
		}
		
		// czesc SQL odpowiedzialna za filtr daty
		if ((isset($_POST['f_year']) && $_POST['f_year'] != '%') ||
				(isset($_POST['f_month']) && $_POST['f_month'] != '%') ||
				(isset($_POST['f_day']) && $_POST['f_day'] != '%')) {
			$sDateSql = " AND added LIKE '".$_POST['f_year'].
																		"-".$_POST['f_month'].
																		"-".$_POST['f_day']." %'";
		}
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(*)
						 FROM ".$aConfig['tabls']['prefix']."events_log
						 WHERE 1 = 1".
									 (isset($_POST['f_user_id']) && !empty($_POST['f_user_id']) ? ' AND user_id = '.$_POST['f_user_id'] : '').
									 (isset($_POST['f_type']) && $_POST['f_type'] != '' ? ' AND error = '.$_POST['f_type'] : '').
									 $sDateSql.
						 $sWhereSql;
		$iRowCount = intval(Common::GetOne($sSql));
				
		$pView = new View('events_history', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// dodanie filtru daty - dzien, miesiac, rok
		// dni
		$aDays = array(
			array('value' => '%', 'label' => '')
		);
		for ($i = 1; $i <= 31; $i++) {
			$sDay = $i < 10 ? '0'.$i : ''.$i;
			$aDays[] = array('value' => $sDay, 'label' => $sDay);
		}
		$pView->AddFilter('f_day', $aConfig['lang'][$this->sModule]['f_date'], $aDays, $_POST['f_day']);
		// miesiace
		$aMonths = array(
			array('value' => '%', 'label' => '')
		);
		for ($i = 1; $i <= 12; $i++) {
			$sMonth = $i < 10 ? '0'.$i : ''.$i;
			$aMonths[] = array('value' => $sMonth, 'label' => $sMonth);
		}
		$pView->AddFilter('f_month', '', $aMonths, $_POST['f_month']);
		// lata
		$pView->AddFilter('f_year', '', $this->getFilterYears(), $_POST['f_year']);
		
		// dodanie filtru typu zdarzenia - blad, powodzenie
		$aEventTypes = array(
			array('value' => '', 'label' => $aConfig['lang'][$_GET['file']]['f_all_types']),
			array('value' => '0', 'label' => $aConfig['lang'][$_GET['file']]['f_type_0']),
			array('value' => '1', 'label' => $aConfig['lang'][$_GET['file']]['f_type_1'])
		);
		$pView->AddFilter('f_type', $aConfig['lang'][$_GET['file']]['f_type'], $aEventTypes, $_POST['f_type']);
		
		if ($_SESSION['user']['type'] !== 0) {
			// dodanie filtru uzytkownika
			// zapytanie wybierajace liste uzytkownikow
			$sSql = "SELECT DISTINCT user_id AS value, login AS label
							 FROM ".$aConfig['tabls']['prefix']."events_log".
							 $sUsersWhereSql.
							 " ORDER BY login";
			$aUsers =& Common::GetAll($sSql);
			if (count($aUsers) > 1) {
				// filtr dodawany tylko wtedy gdy jest wiecej niz 1 uzytkownik
				$pView->AddFilter('f_user_id', $aConfig['lang'][$_GET['file']]['f_user_id'], array_merge(array(array('value' => '', 'label' => $aConfig['lang'][$_GET['file']]['f_all_users'])), $aUsers), $_POST['f_user_id']);
			}
		}
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
			// pobranie historii zdarzen
			$sSql = "SELECT login, added, message, error
							 FROM ".$aConfig['tabls']['prefix']."events_log
							 WHERE 1 = 1".
										 (isset($_POST['f_user_id']) && !empty($_POST['f_user_id']) ? ' AND user_id = '.$_POST['f_user_id'] : '').
										 (isset($_POST['f_type']) && $_POST['f_type'] != '' ? ' AND error = '.$_POST['f_type'] : '').
									 	 $sDateSql.
							 $sWhereSql.
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'added DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			foreach ($aRecords as $iKey => $aRecord) {
				if ($aRecord['error'] == '1') {
					$sClass = 'logoutLevel0';
				}
				else {
					$sClass = 'logoutLevel1';
				}
				$aRecords[$iKey] = array(
					'login' => $aRecord['login'],
					'added' => $aRecord['added'],
					'message' => '<span class="'.$sClass.'">'.$aRecord['message'].'</span>'
				);
			}
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array();
			if ($_SESSION['user']['type'] === 0) {
				$aColSettings['login']['show'] = false;
			}
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
				
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function
	
	
	/**
	 * Metoda zwraca tablice z lista lat do filtru daty - minimalny rok
	 * okresla na podstawie minimalnego roku logu
	 * 
	 * @return	array	- lista lat do filtrow
	 */
	function getFilterYears() {
		global $aConfig;
		
		// okreslenie minimalnego roku
		$sSql = "SELECT MIN(DATE_FORMAT(added, '%Y'))
						 FROM ".$aConfig['tabls']['prefix']."events_log";
		if (($iYear = (int) Common::GetOne($sSql)) == 0) {
			// brak danych o roku w logach zwrocenie roku aktualnego
			$iYear = date('Y');
		}
		$aYears = array(
			array('value' => '%', 'label' => '')
		);
		for ($i = date('Y'); $i >= $iYear; $i--) {
			$aYears[] = array('value' => $i, 'label' => $i);
		}
		return $aYears;
	} // end of getFilterYears() method
} //end of Core class
?>