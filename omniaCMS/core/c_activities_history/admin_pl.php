<?php
/**
* Plik jezykowy dla interfejsu Core Activities History Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'HISTORIA ZDARZEŃ' --------------*/
$aConfig['lang']['c_activities_history']['list']		= "Historia zdarzeń";
$aConfig['lang']['c_activities_history']['f_date']		= "Data";
$aConfig['lang']['c_activities_history']['f_user_id']		= "Użytkownik";
$aConfig['lang']['c_activities_history']['f_all_users']		= "Wszyscy";
$aConfig['lang']['c_activities_history']['f_type']		= "Typ";
$aConfig['lang']['c_activities_history']['f_all_types']		= "Wszystkie";
$aConfig['lang']['c_activities_history']['f_type_0']		= "Powodzenia";
$aConfig['lang']['c_activities_history']['f_type_1']		= "Błędy";
$aConfig['lang']['c_activities_history']['login']		= "Użytkownik";
$aConfig['lang']['c_activities_history']['added']		= "Czas zdarzenia";
$aConfig['lang']['c_activities_history']['event']		= "Zdarzenie";
$aConfig['lang']['c_activities_history']['no_items']	= "Brak historii zdarzeń";
?>
