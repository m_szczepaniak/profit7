<?php
/**
 * Klasa Core do obslugi historii logowania
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Core {
	
	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;
		
		$this->Show($pSmarty);
	} // end Core() function

	
	/**
	 * Metoda wyswietla historie logowania
	 * Dla administratora - dostep do historii logowania wszystkich uzytkownikow
	 * z wyjatkiem sadmin
	 * Dla zwyklego uzytkownika dostep tylko do historii logowania uzytkownika
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		$sWhereSql = '';
		$sUsersWhereSql = '';
		
		// zapamietanie opcji
		rememberViewState($_GET['file']);
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		
		$aHeader = array(
			'header'	=> $aConfig['lang'][$_GET['file']]['list'],
			'refresh'	=> true,
			'search'	=> false,
			'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'login',
				'content'	=> $aConfig['lang'][$_GET['file']]['login'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'login_date',
				'content'	=> $aConfig['lang'][$_GET['file']]['login_date'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'content'	=> $aConfig['lang'][$_GET['file']]['ip_host'],
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'logout_level',
				'content'	=> $aConfig['lang'][$_GET['file']]['logout_level'],
				'sortable'	=> true
			)
		);
		
		if ($_SESSION['user']['type'] === 0) {
			// zwykly uzytkownik - brak informacji o historii logowania
			// innych uzytkownikow - usuniecie kolumny login
			unset($aRecordsHeader[0]);
			
			$sWhereSql = " AND user_id = ".$_SESSION['user']['id'];
		}
		elseif ($_SESSION['user']['type'] === 2) {
			// zwykly administrator - brak informacji o uzytkowniku sadmin
			$sWhereSql = " AND user_id <> 1";
			// wylaczenie uzytkownika sadmin z historii logowania
			$sUsersWhereSql = " WHERE user_id <> 1";
		}
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(*)
						 FROM ".$aConfig['tabls']['prefix']."login_history
						 WHERE 1 = 1".
									 (isset($_POST['f_user_id']) && !empty($_POST['f_user_id']) ? ' AND user_id = '.$_POST['f_user_id'] : '').
						 $sWhereSql;
		$iRowCount = intval(Common::GetOne($sSql));

		$pView = new View('login_history', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($_SESSION['user']['type'] !== 0) {
			// dodanie filtru uzytkownika
			// zapytanie wybierajace liste uzytkownikow
			$sSql = "SELECT DISTINCT user_id AS value, login AS label
							 FROM ".$aConfig['tabls']['prefix']."login_history".
							 $sUsersWhereSql.
							 " ORDER BY login";
			$aUsers =& Common::GetAll($sSql);
			
			if (count($aUsers) > 1) {
				// filtr dodawany tylko wtedy gdy jest wiecej niz 1 uzytkownik
				$pView->AddFilter('f_user_id', $aConfig['lang'][$_GET['file']]['f_user_id'], array_merge(array(array('value' => '', 'label' => $aConfig['lang'][$_GET['file']]['f_all_users'])), $aUsers), $_POST['f_user_id']);
			}
		}
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
			// pobranie historii logowania
			$sSql = "SELECT user_id, login, login_date, ip, host, logout_level, logout_date,
											session_expiry_date, session_id
							 FROM ".$aConfig['tabls']['prefix']."login_history
							 WHERE 1 = 1".
										 (isset($_POST['f_user_id']) && !empty($_POST['f_user_id']) ? ' AND user_id = '.$_POST['f_user_id'] : '').
							 $sWhereSql.
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'login_date DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			foreach ($aRecords as $iKey => $aRecord) {
				$aRecord['ip_host'] = $aRecord['ip'].'<br>'.$aRecord['host'];
				
				switch ($aRecord['logout_level']) {
					case '0':
						$aRecord['logout_level'] = 
							'<span class="logoutLevel0">';
						if ($aRecord['session_id'] == session_id()) {
							// aktualnie trwajaca sesja
							$aRecord['logout_level'] .= $aConfig['lang'][$_GET['file']]['active_session'];
						}
						else {
							// brak danych o zakonczeniu sesji
							$aRecord['logout_level'] .= $aConfig['lang'][$_GET['file']]['logout_level_0'];
						}
						$aRecord['logout_level'] .= '</span>';
					break;
					
					case '1':
						$aRecord['logout_level'] = 
							'<span class="logoutLevel1">'.
							$aConfig['lang'][$_GET['file']]['logout_level_1'].'<br>'.
							$aRecord['logout_date'].
							'</span>';
					break;
					
					case '2':
						$aRecord['logout_level'] = 
							'<span class="logoutLevel2">'.
							$aConfig['lang'][$_GET['file']]['logout_level_2'].'<br>'.
							$aRecord['session_expiry_date'].
							'</span>';
					break;
				}
				$aRecords[$iKey] = array(
					'user_id' => $aRecord['user_id'],
					'login' => $aRecord['login'],
					'login_date' => $aRecord['login_date'],
					'ip_host' => $aRecord['ip_host'],
					'logout_level' => $aRecord['logout_level']
				);
			}
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'user_id' => array(
					'show' => false
				)
			);
			if ($_SESSION['user']['type'] === 0) {
				$aColSettings['login']['show'] = false;
			}
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
				
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function
} //end of Core class
?>