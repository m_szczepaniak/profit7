<?php
/**
* Plik jezykowy dla interfejsu Core Login History Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'HISTORIA LOGOWANIA' --------------*/
$aConfig['lang']['c_login_history']['list']		= "Historia logowania";
$aConfig['lang']['c_login_history']['f_user_id']		= "Użytkownik";
$aConfig['lang']['c_login_history']['f_all_users']		= "Wszyscy";
$aConfig['lang']['c_login_history']['login']		= "Użytkownik";
$aConfig['lang']['c_login_history']['login_date']		= "Zalogowano";
$aConfig['lang']['c_login_history']['ip_host']		= "IP / host";
$aConfig['lang']['c_login_history']['logout_level']		= "Zakończenie pracy";
$aConfig['lang']['c_login_history']['no_items']	= "Brak historii logowania";
$aConfig['lang']['c_login_history']['active_session']		= "Aktualna sesja";
$aConfig['lang']['c_login_history']['logout_level_0']		= "Brak danych";
$aConfig['lang']['c_login_history']['logout_level_1']		= "Wylogowano:";
$aConfig['lang']['c_login_history']['logout_level_2']		= "Sesja wygasła:";
?>
