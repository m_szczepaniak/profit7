<?php
use Menu\TileMenu;

/**
 * Klasa Core do obslugi glownej strony
 * Wyswietla glowne informacje o CMSie, obslugiwanych
 * serwisach, poszczegolnych modulach
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Core {

  /**
   * @var
   */
  private $smarty;

  /**
   * Metoda ustawia cookie
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
    global $aConfig;

    $this->smarty = $pSmarty;

	} // end Core() function

  public function getContent() {

    $tileMenu = new TileMenu();

    $this->smarty->assign('menu', $tileMenu->renderMenu($_GET['menu-symbol']));
    return $sHtml = $this->smarty->fetch($this->smarty->template_dir.'tile-menu.tpl');
  }
  
  
  private function getModules() {
    
  }
  
  private function getModuleItem() {
    
  }

} //end of Core class