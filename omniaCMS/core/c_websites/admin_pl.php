<?php
/**
* Plik jezykowy dla interfejsu Core Languages Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'SERWISY' --------------*/
$aConfig['lang']['c_websites']['list']		= "Lista serwisów";
$aConfig['lang']['c_websites']['code']		= "Symbol";
$aConfig['lang']['c_websites']['no_items']	= "Brak zdefiniowanych serwisów";

$aConfig['lang']['c_websites']['edit']	= "Edytuj serwis";
$aConfig['lang']['c_websites']['delete']	= "Usuń serwis";
$aConfig['lang']['c_websites']['delete_q']	= "Czy na pewno usunąć wybrany serwis?";
$aConfig['lang']['c_websites']['check_all']	= "Zaznacz / odznacz wszystkie";
$aConfig['lang']['c_websites']['delete_all']	= "Usuń zaznaczone";
$aConfig['lang']['c_websites']['delete_all_q']	= "Czy na pewno usunąć wybrane serwisy?";
$aConfig['lang']['c_websites']['delete_all_err']	= "Nie wybrano żadnego serwisu do usunięcia!";
$aConfig['lang']['c_websites']['add']	= "Dodaj serwis";

$aConfig['lang']['c_websites']['header_0']	= "Dodawanie serwisu";
$aConfig['lang']['c_websites']['header_1']	= "Edycja serwisu";
$aConfig['lang']['c_websites']['code']	= "Symbol";
$aConfig['lang']['c_websites']['name']	= "Nazwa serwisu";
$aConfig['lang']['c_websites']['url']	= "Adres";
$aConfig['lang']['c_websites']['db_section']	= "Baza danych";
$aConfig['lang']['c_websites']['db_host']	= "Host";
$aConfig['lang']['c_websites']['db_name']	= "Nazwa";
$aConfig['lang']['c_websites']['db_login']	= "Użytkownik";
$aConfig['lang']['c_websites']['db_password']	= "Hasło";


$aConfig['lang']['c_websites']['add_ok']	= "Serwis \"%s\" został dodany do bazy danych";
$aConfig['lang']['c_websites']['add_err']	= "Nie udało się dodać serwisu \"%s\" do bazy danych!<br>Spróbuj ponownie";
$aConfig['lang']['c_websites']['del_ok_0']	= "Serwis %s został usunięty";
$aConfig['lang']['c_websites']['del_ok_1']	= "Serwisy %s zostały usunięte";
$aConfig['lang']['c_websites']['del_err_0']	= "Nie udało się usunąć serwisu %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_websites']['del_err_1']	= "Nie udało się usunąć serwisów %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_websites']['edit_ok']	= "Zmiany w serwisie \"%s\" zostały wprowadzone do bazy danych";
$aConfig['lang']['c_websites']['edit_err']= "Nie udało się wprowadzić zmian w serwisie \"%s\"<br>Spróbuj ponownie";
?>
