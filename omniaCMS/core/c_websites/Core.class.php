<?php
/**
 * Klasa Core do obslugi wersji jezykowych serwisu.
 * Zarzadzanie wersjami jezykowymi jest dozwolone tylko dla
 * superuzytkownika
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Core {

	// komunikat
	var $sMsg;

	// sciezka do katalogu zawierajacego pliki XML
	var $sXMLDir;

	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->sXMLDir = $aConfig['common']['client_base_path'].'images/xml';

		$sAction = '';
		$iId = 0;

		if (isset($_GET['do'])) {
			$sAction = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sAction = $_POST['do'];
		}
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}

		if ($_SESSION['user']['type'] == 1) {
			switch ($sAction) {
				case 'delete': $this->Delete($pSmarty, $iId); break;
				case 'show': $this->Show($pSmarty); break;
				case 'insert': $this->Insert($pSmarty); break;
				case 'update': $this->Update($pSmarty, $iId); break;
				case 'edit': $this->AddEdit($pSmarty, $iId); break;
				case 'add': $this->AddEdit($pSmarty); break;
				default: $this->Show($pSmarty); break;
			}
		}
		else {
			$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
		}
	} // end Core() function


	/**
	 * Metoda usuwa wybrana(e) serwisy
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID usuwanego serwisu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId=0) {
		global $aConfig;
		$bIsErr = false;

		if ($iId > 0) {
			$_POST['delete'][$iId] = 'on';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();

			// pobranie symboli usuwanych wersji jez.
			$sSql = "SELECT name
					 		 FROM ".$aConfig['tabls']['prefix']."websites
						 	 WHERE id IN (".implode(',', $_POST['delete']).")";
			$aRecords =& Common::GetCol($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				foreach ($aRecords as $sSymbol) {
					$sDel .= '"'.$sSymbol.'", ';
				}
				$sDel = substr($sDel, 0, -2);
			}

			if (!$bIsErr) {
				// usuwanie
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."websites
								 WHERE id IN (".implode(',', $_POST['delete']).")";
				if (count($_POST['delete']) == 1) {
					$sErrPostfix = '0';
				}
				else {
					$sErrPostfix = '1';
				}
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}

			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_ok_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_err_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton

	/**
	 * Metoda dodaje do bazy danych nowy serwis
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}

			// rozpoczecie transakcji
			Common::BeginTransaction();
			$aValues = array(
				'code' => $_POST['code'],
				'name' => $_POST['name'],
				'url' => $_POST['url'],
				'db_host' => $_POST['db_host'],
				'db_name' => $_POST['db_name'],
				'db_login' => $_POST['db_login'],
				'db_password' => $_POST['db_password']
			);
			if (($iId = Common::Insert($aConfig['tabls']['prefix']."websites",$aValues)) === false) {
				$bIsErr = true;
			}
			

			if (!$bIsErr) {
				// serwiszostal zmodyfikowana,
				// zatwierdzenie transakcji
				Common::CommitTransaction();
				// wersja jez. zostala dodana, wyswietlenie komunikatu o powodzeniu
				// oraz wyswietlenie listy obszarow
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_ok'], $_POST['name']);
				$this->sMsg = GetMessage($sMsg, false);
				AddLog($sMsg, false);
				// resetowanie stanu widoku
				$_GET['reset'] = 2;
				$this->Show($pSmarty);
			}
			else {
				// wersja jez. nie zostala dodana,
				// wycofanie transakcji
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_err'], $_POST['name']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty);
			}

	} // end of Insert() funciton


	/**
	 * Metoda wprowadza zmiany w serwisie do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}


			// rozpoczecie transakcji
			Common::BeginTransaction();

			$aValues = array(
				'code' => $_POST['code'],
				'name' => $_POST['name'],
				'url' => $_POST['url'],
				'db_host' => $_POST['db_host'],
				'db_name' => $_POST['db_name'],
				'db_login' => $_POST['db_login'],
				'db_password' => $_POST['db_password']
			);
			if (Common::Update($aConfig['tabls']['prefix']."websites",$aValues,"id = ".$iId) === false) {
				$bIsErr = true;
			}
			if (!$bIsErr) {
				// wersja jez. zostala zmodyfikowana,
				// zatwierdzenie transakcji
				Common::CommitTransaction();
		 		// wyswietlenie komunikatu o powodzeniu
				// oraz wyswietlenie listy wersji jezykowych
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['edit_ok'], $_POST['name']);
				$this->sMsg = GetMessage($sMsg, false);
				AddLog($sMsg, false);
				$this->Show($pSmarty);
			}
			else {
				// nie wprowadzono zadnych zmian,
				// wycofanie transakcji
				Common::RollbackTransaction();
				// wyswietlenie komunikatu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['edit_err'], $_POST['name']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iId);
			}
		
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji serwisu
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	ID edytowanej wersji jezykowej
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych serwisu
			$sSql = "SELECT code, name, url, db_host, db_name, db_login, db_password
							 FROM ".$aConfig['tabls']['prefix']."websites
							 WHERE id=".$iId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$_GET['file']]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['symbol']) && !empty($aData['symbol'])) {
			$sHeader .= ' "'.$aData['symbol'].'"';
		}

		// tworzenie formularza
		$pForm = new FormTable('websites', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>160), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));


		$pForm->AddText('code', $aConfig['lang'][$_GET['file']]['code'], $aData['code'], array('maxlength'=>16, 'style'=>'width: 300px;'));
		$pForm->AddText('name', $aConfig['lang'][$_GET['file']]['name'], $aData['name'], array('maxlength'=>150, 'style'=>'width: 300px;'));
		$pForm->AddText('url', $aConfig['lang'][$_GET['file']]['url'], $aData['url'], array('maxlength'=>255, 'style'=>'width: 300px;'));
		
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['db_section'], array('class'=>'merged', 'style'=>'padding-left: 10px'));
		$pForm->AddText('db_host', $aConfig['lang'][$_GET['file']]['db_host'], $aData['db_host'], array('maxlength'=>255, 'style'=>'width: 300px;'));
		$pForm->AddText('db_name', $aConfig['lang'][$_GET['file']]['db_name'], $aData['db_name'], array('maxlength'=>64, 'style'=>'width: 300px;'));
		$pForm->AddText('db_login', $aConfig['lang'][$_GET['file']]['db_login'], $aData['db_login'], array('maxlength'=>64, 'style'=>'width: 300px;'));
		$pForm->AddText('db_password', $aConfig['lang'][$_GET['file']]['db_password'], $aData['db_password'], array('maxlength'=>64, 'style'=>'width: 300px;'));

		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).
														 '&nbsp;&nbsp;'.
														 $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function


	/**
	 * Metoda wyswietla liste zdefiniowanych dla serwisów
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// zapamietanie opcji
		rememberViewState($_GET['file']);

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'header'	=> $aConfig['lang'][$_GET['file']]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '30'
			),
			array(
				'db_field'	=> 'code',
				'content'	=> $aConfig['lang'][$_GET['file']]['website_code'],
				'sortable'	=> true,
			'width'	=> '150'
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$_GET['file']]['website_name'],
				'sortable'	=> true
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich serwisów
		$sSql = "SELECT COUNT(*)
						 FROM ".$aConfig['tabls']['prefix']."websites
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($_GET['file']);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(*)
							 FROM ".$aConfig['tabls']['prefix']."websites
							 WHERE 1 = 1";
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('websites', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich serwisów
			$sSql = "SELECT id, code, name
							 FROM ".$aConfig['tabls']['prefix']."websites
							 WHERE TRUE".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' id').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do'=>'edit', 'id'=>'{id}')),
					'use_lang'	=> true
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params'	=> array (
												'edit'	=>	array('id' => '{id}'),
												'delete'	=>	array('id' => '{id}')
					)
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('add')
		);
		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function

} //end of Core class
?>