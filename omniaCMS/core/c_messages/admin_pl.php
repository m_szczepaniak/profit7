<?php
/**
* Plik jezykowy dla interfejsu Core Messages Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*-------------- MODUL 'KOMUNIKATY' --------------*/
$aConfig['lang']['c_messages']['list']		= "Lista komunikatów";
$aConfig['lang']['c_messages']['add']	= "Dodaj komunikat";
$aConfig['lang']['c_messages']['no_items']	= "Brak zdefiniowanych komunikatów";
$aConfig['lang']['c_messages']['symbol']= "Symbol komunikatu";
$aConfig['lang']['c_messages']['content']= "Treść komunikatu";
$aConfig['lang']['c_messages']['header_0']	= "Dodawanie komunikatu";
$aConfig['lang']['c_messages']['header_1']	= "Edycja komunikatu";

$aConfig['lang']['c_messages']['add_ok']	= "Komunikat \"%s\" został dodany do bazy danych";
$aConfig['lang']['c_messages']['delete']	= "Usuń komunikat";
$aConfig['lang']['c_messages']['delete_q']	= "Czy na pewno usunąć wybrany komunikat?";
$aConfig['lang']['c_messages']['check_all']	= "Zaznacz / odznacz wszystkie";
$aConfig['lang']['c_messages']['delete_all']	= "Usuń zaznaczone";
$aConfig['lang']['c_messages']['delete_all_q']	= "Czy na pewno usunąć wybrane komunikaty?";
$aConfig['lang']['c_messages']['delete_all_err']	= "Nie wybrano żadnego komunikatu do usunięcia!";
$aConfig['lang']['c_messages']['del_ok_0']	= "Komunikat %s został usunięty";
$aConfig['lang']['c_messages']['del_ok_1']	= "Komunikat %s zostały usunięte";
$aConfig['lang']['c_messages']['del_err_0']	= "Nie udało się usunąć komunikatu %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_messages']['del_err_1']	= "Nie udało się usunąć komunikatu %s!<br>Spróbuj ponownie";
$aConfig['lang']['c_messages']['edit_ok']	= "Zmiany w komunikacie \"%s\" zostały wprowadzone do bazy danych";
$aConfig['lang']['c_messages']['edit_err']= "Nie wprowadzono żadnych zmian w komunikacie \"%s\"";

$aConfig['lang']['c_messages']['add_err']	= "Nie udało się dodać komunikatu \"%s\" do bazy danych!<br>Spróbuj ponownie";
$aConfig['lang']['c_messages']['add_err2']= "Komunikat o podanym symbolu \"%s\" już istnieje!<br>Wybierz inny symbol";


?>
