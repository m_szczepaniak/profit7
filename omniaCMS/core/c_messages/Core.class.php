<?php
/**
 * Klasa Core do obslugi obszarow serwisu.
 * Zarzadzanie obszarami jest dozwolone tylko dla
 * superuzytkownika
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Core {

	// komunikat
	var $sMsg;

	// ID serwisu
	var $iWebsiteId;

	// Id wersji jezykowej
	var $iLangId;

	/**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Core(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iWebsiteId = $_SESSION['site']['id'];
		$this->iLangId = $_SESSION['lang']['id'];

		$sAction = '';
		$iId = 0;

		if (isset($_GET['do'])) {
			$sAction = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sAction = $_POST['do'];
		}
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['symbol'])) {
			$sSymbol = $_GET['symbol'];
		}

//		if ($_SESSION['user']['type'] == 1 || $_SESSION['user']['type'] == 2) {
			switch ($sAction) {
				case 'delete': $this->DeleteMessages($pSmarty, $sSymbol); break;
				case 'show': $this->ShowMessages($pSmarty); break;
				case 'insert': $this->InsertMessage($pSmarty); break;
				case 'update': $this->UpdateMessage($pSmarty); break;
				case 'edit': $this->AddEditMessage($pSmarty, $sSymbol); break;
				case 'add': $this->AddEditMessage($pSmarty); break;
				default: $this->ShowMessages($pSmarty); break;
			}
//		}
//		else {
//			$pSmarty->assign_by_ref('sContent', GetMessage($aConfig['lang']['common']['no_privileges'], true));
//		}
	} // end Core() function


	/**
	 * Metoda usuwa wybrany(e) komunikat(y)
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$sSymbol	- symbol usuwanego obszaru
	 * @return 	void
	 */
	function DeleteMessages(&$pSmarty, $sSymbol = '') {
		global $aConfig;
		$bIsErr = false;

		if ($sSymbol != '') {
			$_POST['delete'][$sSymbol] = 'on';
		}

		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$sVal] = $sKey;
		}
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();

			// pobranie symboli usuwanych komunikatow
			$sSql = "SELECT symbol
					 		 FROM ".$aConfig['tabls']['prefix']."website_messages
						 	 WHERE symbol IN ('".implode(',', $_POST['delete'])."')";
			$aRecords =& Common::GetCol($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				foreach ($aRecords as $sSymbol) {
					if (isset($aConfig['lang'][$_GET['file']][$sSymbol])) {
						$sSymbol = $aConfig['lang'][$_GET['file']][$sSymbol];
					}
					$sDel .= '"'.$sSymbol.'", ';
				}
				$sDel = substr($sDel, 0, -2);
			}

			if (!$bIsErr) {
				// usuwanie komunikatu
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."website_messages
								 WHERE symbol IN ('".implode(',', $_POST['delete'])."')";
				if (count($_POST['delete']) == 1) {
					$sErrPostfix = '0';
				}
				else {
					$sErrPostfix = '1';
				}
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_ok_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['del_err_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie logu
				AddLog($sMsg);
			}
		}
		$this->ShowMessages($pSmarty);
	} // end of DeleteMessages() funciton

	/**
	 * Metoda dodaje do bazy danych nowy obszar
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function InsertMessage(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditMessage($pSmarty);
			return;
		}
		// sprawdzenie czy obszar o podanym symbolu nie istnieje w bazie danych
		if (!$this->MessageExists($_POST['symbol'])) {
						  
				$aValues = array (
					'symbol' => $_POST['symbol'],
					'content' => nl2br($_POST['content'])
				);
			if (Common::Insert($aConfig['tabls']['prefix']."website_messages", $aValues, '', false) !== false) {
				// obszar zostal dodany, wyswietlenie komunikatu o powodzeniu
				// oraz wyswietlenie listy obszarow
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_ok'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg, false);
				AddLog($sMsg, false);
				// resetowanie stanu widoku
				$_GET['reset'] = 2;
				$this->ShowMessages($pSmarty);
			}
			else {
				// obszar nie zostal dodany, wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_err'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEditMessage($pSmarty);
			}
		}
		else {
			// obszar o podanym symbolu juz istnieje
			$sMsg = sprintf($aConfig['lang'][$_GET['file']]['add_err2'],
											$_POST['symbol']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEditMessage($pSmarty);
		}
	} // end of InsertMessage() funciton


	/**
	 * Metoda wprowadza zmiany w obszarze do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateMessage(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');

		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditMessage($pSmarty);
			return;
		}


				$aValues = array (
					'symbol' => $_POST['symbol'],
					'content' => nl2br($_POST['content'])
				);
			
			if (Common::Update($aConfig['tabls']['prefix']."website_messages", $aValues, "symbol = '".$_POST['symbol']."'") !== false) {
				// obszar zostal zmodyfikowany, wyswietlenie komunikatu o powodzeniu
				// oraz wyswietlenie listy obszarow
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['edit_ok'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg, false);
				AddLog($sMsg, false);
				$this->ShowMessages($pSmarty);
			}
			else {
				// nie wprowadzono zadnych zmian, wyswietlenie komunikatu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$_GET['file']]['edit_err'], $_POST['symbol']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEditMessage($pSmarty, $_GET['symbol']);
			}

	} // end of UpdateMessage() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji obszaru
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$sSymbol	-	symbol komunikatu
	 */
	function AddEditMessage(&$pSmarty, $sSymbol = '') {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($sSymbol != '') {
			// pobranie z bazy danych edytowanego obszaru
			$sSql = "SELECT symbol, content
							 FROM ".$aConfig['tabls']['prefix']."website_messages
							 WHERE symbol='".$sSymbol."'";
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$_GET['file']]['header_'.($sSymbol != '' ? 1 : 0)];
		if ($sSymbol != '' && isset($aData['symbol']) && !empty($aData['symbol'])) {
			if (isset($aConfig['lang'][$_GET['file']][$aData['symbol']]) &&
					!empty($aConfig['lang'][$_GET['file']][$aData['symbol']])) {
				$sHeader .= ' "'.$aConfig['lang'][$_GET['file']][$aData['symbol']].'"';
			}
			else {
				$sHeader .= ' "'.$aData['symbol'].'"';
			}
		}

		// tworzenie formularza
		$pForm = new FormTable('messages', $sHeader, array('action'=>phpSelf(array('symbol' => $sSymbol))), array('col_width'=>160), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', ($sSymbol != '' ? 'update' : 'insert'));

		// symbol obszaru
		$pForm->AddText('symbol', $aConfig['lang'][$_GET['file']]['symbol'], $aData['symbol'], array_merge(array('maxlength'=>32, 'style'=>'width: 200px;'), $sSymbol != '' ? array('readonly'=>'readonly') : array()));

		// tresc
		$pForm->AddTextArea('content', $aConfig['lang'][$_GET['file']]['content'], br2nl($aData['content']), array('rows' => '7', 'style' => 'width: 400px;'));


		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($sSymbol != '' ? 1 : 0)]).
														 '&nbsp;&nbsp;'.
														 $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEditMessage() function


	/**
	 * Metoda wyswietla liste zdefiniowanych dla serwisu obszarow
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function ShowMessages(&$pSmarty) {
		global $aConfig;

		// zapamietanie opcji
		rememberViewState($_GET['file']);

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'header'	=> $aConfig['lang'][$_GET['file']]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '30'
			),
			array(
				'db_field'	=> 'symbol',
				'content'	=> $aConfig['lang'][$_GET['file']]['symbol'],
				'sortable'	=> true,
				'width'		=> '300'
			),
			array(
				'db_field'	=> 'content',
				'content'	=> $aConfig['lang'][$_GET['file']]['content'],
				'sortable'	=> true
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich obszarow
		$sSql = "SELECT COUNT(*)
						 FROM ".$aConfig['tabls']['prefix']."website_messages
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND symbol LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($_GET['file']);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(*)
							 FROM ".$aConfig['tabls']['prefix']."website_messages";
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('messages', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich modulow dla danego serwisu
			$sSql = "SELECT symbol, content
							 FROM ".$aConfig['tabls']['prefix']."website_messages
							 WHERE TRUE".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND symbol LIKE \'%'.$_POST['search'].'%\'' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'symbol').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'symbol' => array (
					'link'	=> phpSelf(array('do'=>'edit', 'symbol'=>'{symbol}')),
					'use_lang'	=> true
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params'	=> array (
												'edit'	=>	array('symbol' => '{symbol}'),
												'delete'	=>	array('symbol' => '{symbol}')
					)
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array(),
			array('add')
		);
		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of ShowMessages() function


	/**
	 * Metoda sprawdza czy komunikat o podanym symbolu istnieje
	 *
	 * @param		string	$sSymbol	- symbol komunikatu
	 * * @param		string	$sContent	- tresc komunikatu
	 * @param		integer	$iId	- ID obszaru dla ktorego pomijane jest sprawdzanie
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function MessageExists($sSymbol) {
		global $aConfig;

		$sSql = "SELECT COUNT(*)
						 FROM ".$aConfig['tabls']['prefix']."website_messages
						 WHERE symbol = '".$sSymbol."'";

		return intval(Common::GetOne($sSql)) == 1;
	} // end of MessageExists() function
} //end of Core class
?>