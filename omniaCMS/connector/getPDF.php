<?php
/**
 * Skrypt umożliwia wysłanie PDF'a faktury lub etykiety paczkomaty
 * 
 */
 
ini_set('display_errors', '0');
ini_set('display_errors', 'off');

global $aConfig;
$aConfig['common']['use_session'] = false;
include_once('../config/config.inc.php');
include_once('../config/ini.inc.php');
include_once('../lib/Invoice.class.php');
$oInvoices = new Invoice();
/*
INSERT INTO `print_queue` (`id`, `filetype`, `filepath`, `dest_printer`, `created`) VALUES
(33, 'FEDE', '132011-6014136041923', 'drukarka_test1', '2014-10-07 12:03:45');
*/

/**
 * 
 * @param string $sFile
 */
function returnFile($sFile) {
  for ($i = 0; $i <=5; $i++) {
    $sPDFContent = file_get_contents($sFile);
    if (stristr($sPDFContent, '%%EOF')) {
      break;
    }
    sleep(5);
  }
  
  if (!stristr($sPDFContent, '%%EOF')) {
    Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', 'raporty@profit24.pl', 'błędny plik pdf, z drukarki ', $sFile." \n\n <br />".$sPDFContent);
    return false;
  } else {
    echo $sPDFContent;
  }
}

if ($_GET['key'] != $aConfig['connector']['connector_acces_key']) { exit(0);}
if (!isset($_GET['printer']) || empty($_GET['printer'])) { exit(0);}

if ($_GET['type'] == 'invoices') {
	// faktury
	
	$sSql = "SELECT id FROM orders 
					WHERE print_invoice='1'
					LIMIT 1";
	$iId = Common::GetOne($sSql);
	// pobierz id zamówienia do druku
    if ($iId > 0) {
		$aVal = array('print_invoice' => '0');
		Common::Update($aConfig['tabls']['prefix']."orders", $aVal, " id = ".$iId." LIMIT 1");
		echo $oInvoices->getInvoiceData($iId, false);
	}
} elseif ($_GET['type'] == 'paczkomaty') {
	// paczkomaty
	$sSql = "SELECT id, filepath 
          FROM ".$aConfig['tabls']['prefix']."print_queue 
					WHERE 
            dest_printer = ".Common::Quote($_GET['printer'])."
            AND filetype = 'PDF'
					LIMIT 1";
  $aPrintItem = Common::GetRow($sSql);
  
  if (!empty($aPrintItem)) {
    
		if (file_exists($aPrintItem['filepath'])) {
			// pobianie PDF'a
			returnFile($aPrintItem['filepath']);
		} else {
      Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', 'raporty@profit24.pl', 'BRAK pliku PDF, z drukarki ', print_r($aPrintItem, true).'<br />'.print_r($_GET, true));
    }
    $sSql = 'DELETE FROM print_queue WHERE id = '.$aPrintItem['id'];
    Common::Query($sSql);
	}
} elseif ($_GET['type'] == 'fedex') {
  include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
  $oFedex = new \communicator\sources\FedEx\FedEx();
  
  // fedex
	$sSql = "SELECT id, filepath 
          FROM ".$aConfig['tabls']['prefix']."print_queue 
					WHERE 
            dest_printer = ".Common::Quote($_GET['printer'])."
            AND filetype = 'FEDE'
					LIMIT 1";
	$aPrintItem = Common::GetRow($sSql);
  if (!empty($aPrintItem)) {
		if (file_exists($aPrintItem['filepath'])) {
			// pobianie PDF'a
			returnFile($aPrintItem['filepath']);
		}
    $sSql = 'DELETE FROM print_queue WHERE id = '.$aPrintItem['id'];
    Common::Query($sSql);
  }
}
exit(0);