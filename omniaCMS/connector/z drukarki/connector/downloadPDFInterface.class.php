<?php
/**
 * Interfejs pobierania plików PDF
 * 
 */

interface downloadPDFInterface {
	public function getPDF ();
	public function savePDF ();
	public function setConfig ();
}

?>
