<?php
/**
 * Klasa pobiera faktury
 * klasa implementuje pobieranie PDF'ow
 *  
 */
include('downloadPDFInterface.class.php');
class downloadPDF implements downloadPDFInterface {
	public $sType = NULL;
	private $sKey = NULL;
	private $sURL = NULL;
	private $sPDFcontent = NULL;
	private $sBasePath = NULL;
	private $sFilePath = NULL;
  private $sPrinter;
	
	public function __construct ($sType) {
		$this->sType = $sType;
	}
	
	/**
	 * Metoda ustawia podstawowe zmienne
	 *
	 * @return boolean 
	 */
	public function setConfig($aConfig) {
		
		$this->sKey = $aConfig['connector']['connector_acces_key'];
		$this->sURL = $aConfig['connector']['profit24_url_script'];
		$this->sBasePath = $aConfig['connector']['base_dir'];
		$this->sFilePath = $aConfig['connector'][$this->sType.'_dir'];
		$this->sPrinter = $aConfig['connector']['printer'];
    
		return true;
	}// end of setConfig() method
	
	/**
	 * Metoda pobiera PDF'a
	 *
	 * @return boolean 
	 */
	public function getPDF() {
		
		if ($this->sKey != '' && $this->sURL != '' && $this->sType != '') {
			$sURLAll = $this->sURL.'?key='.$this->sKey.'&type='.$this->sType.'&printer='.$this->sPrinter;
			$this->sPDFcontent = file_get_contents($sURLAll);
			//echo $sURLAll ."\n";
			//echo $this->sPDFcontent;
			if (!empty($this->sPDFcontent)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}// end of getPDF() method
	
	/**
	 * Metoda zapisuje PDF'a
	 *
	 * @return boolean
	 */
	public function savePDF() {
		
		return file_put_contents($this->sFilePath.'\\'.rand(5, 4000000000).'.pdf', $this->sPDFcontent, LOCK_EX);
	}// end savePDF() method
}
?>
