
<script type="text/javascript">
  aBooks = {$sDataJSON};
  aShelfImages = {$sShelfImagesJSON};
  aBooksToSend = [];
  {literal}
// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"'+obj+'"';
        return String(obj);
    }
    else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n]; t = typeof(v);
            if (t == "string") v = '"'+v+'"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

    $(function(){
      quantityAllBooks = 0;
      for(var i = 0; i < aBooks.length; i++)
      {
        quantityAllBooks ++;
      }
      console.log(aBooks);
      $("#left_order_items").html(quantityAllBooks);
    });
    
  {/literal}
</script>
<div class="clear"></div>
<div>
  Pozostało do zeskanowania / ОСТАЛОСЬ КНИГ ДО РЕАЛИЗАЦИИ: <span id="left_order_items"></span><br /><br />
</div>
<hr />
<div class="clear"></div>
<div id="number_cont">
  <div id="left_images_books">
    <div id="books_images"></div>
  </div>
  <div>
    <p class="number_cont_transport">
      <img id="main_image" /><br /><br />

      <span id="main_quantity">Ilość: <span></span></span><br />
      <span id="main_weight"> Waga: <span></span></span><br />
      <span id="main_isbn"></span><br />
      <span id="main_desc"></span>
    </p>
    <p class="cont_main_shelf_number location-style">
      <span id="main_shelf_number" style="font-size: 300px; color: #51a351;"></span>  
    </p>
  </div>
  <div class="clear"></div>
</div>
<div class="list_scanned_items location-style">
  {section name=foo loop=3 step=+1}
    <div class="list_scanned_item">
      <img id="image_{$smarty.section.foo.index}" class="sc_image"/><br />
      <span id="shelf_number_{$smarty.section.foo.index}" class="sc_shelf"></span><br />
      <span id="quantity_{$smarty.section.foo.index}" class="sc_sp_quantity"> Ilość: <span class="sc_quantity"></span></span><br />
      <span id="isbn_{$smarty.section.foo.index}" style="font-size: 14px; color: grey" ></span>
    </div>
  {/section}
  <div class="clear"></div>
<div id="not_scanned" style="margin-top: 15px; display: none; text-align: center;">
  <hr />
  <div class="clear"></div>
  <div style="margin: 15px 0; font-size: 22px;">Braki:</div>
  <div class="clear"></div>
  <div id="not_scanned_sc">
    
  </div>
</div>
</div>

<br />
<div class="clear"></div>
<div class="clear"></div>
