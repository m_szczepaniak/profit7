{foreach from=$aProductsStocksTypes item=aStocks key=id}
	<div class="stockInfo" id="stockTooltip_{$id}" {if $aParametrs.bHideOnStart == true}style="display: none;"{/if}>
  {foreach from=$aStocks item=aStockType key=type}
    {if $type == 'profit'}
      {include file="stock_single_type_profit.tpl"}
    {else}
      {include file="stock_single_type_external.tpl"}
    {/if}
  {/foreach}
  </div>
{/foreach}
