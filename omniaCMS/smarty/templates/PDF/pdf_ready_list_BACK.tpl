<table style="text-align: left; width: 100%;" border="1" cellpadding="2">
  <tbody>
    <tr>
      <td width="20px">Lp</td>
      <td width="80px">Półka</td>
      
      <td width="300px">Tytuł</td>
      <td width="100px">Autor</td>
      <td width="90px">Wydawca</td>
      <td width="80px">ISBN</td>
      <td width="30px">CD</td>
      <td width="25px">Ilość</td>
    </tr>
    {foreach from=$aBooks.items key=iLp item=aItem}
    <tr>
      <td width="20px">{$iLp+1}</td>
      <td width="80px">{$aItem.pack_number}</td>
      <td width="300px">{$aItem.name}</td>
      <td width="100px">{$aItem.authors}</td>
      <td width="90px">{$aItem.publisher}</td>
      <td width="80px">{$aItem.isbn_plain}</td>
      <td width="30px">{if $aItem.is_cd > 0}TAK{else}-{/if}</td>
      <td width="25px" align="center">{$aItem.quantity}</td>
    </tr>
    {/foreach}
    {*
    <tr>
      <td width="20px">&nbsp;</td>
      <td width="80px">&nbsp;</td>
      <td width="180px">&nbsp;</td>
      <td width="100px">&nbsp;</td>
      <td width="80px">SUMA:</td>
      <td width="25px" align="center">{$aBooks.Qsum}</td>
    </tr>
    *}
  </tbody>
</table>
<br /><br />
Produktow na magazynie: {$aBooks.count_magazine}<br />
Produktow na tramwaju: {$aBooks.count_tramwaj}