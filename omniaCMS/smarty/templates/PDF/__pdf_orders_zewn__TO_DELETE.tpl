<h2>{$aBooks.orders_header}</h2>
{$aBooks.orders_header2}
<br />
<table style="text-align: left; width: 100%;" border="1" cellpadding="2">
  <tbody>
    <tr>
      <td width="20px">Lp</td>
      <td width="80px">ISBN</td>
      <td width="45px">Regał</td>
      <td width="180px">Tytuł</td>
      <td width="100px">Autor</td>
      <td width="80px">Wydawca</td>
      <td width="25px">Ilość</td>
    </tr>
    {foreach from=$aBooks.items item=aItem}
    <tr>
      <td width="20px">{$aItem.lp}</td>
      <td width="80px">{$aItem.isbn}</td>
      <td width="45px">{$aItem.location}</td>
      <td width="180px">{$aItem.name}</td>
      <td width="100px">{$aItem.authors}</td>
      <td width="80px">{$aItem.publisher}</td>
      <td width="25px" align="center">{$aItem.quantity}</td>
    </tr>
    {/foreach}
    <tr>
      <td width="20px">&nbsp;</td>
      <td width="80px">&nbsp;</td>
      <td width="45px">&nbsp;</td>
      <td width="180px">&nbsp;</td>
      <td width="100px">&nbsp;</td>
      <td width="80px">SUMA:</td>
      <td width="25px" align="center">{$aBooks.Qsum}</td>
    </tr>
  </tbody>
</table>
<br />