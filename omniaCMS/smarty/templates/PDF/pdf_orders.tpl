<h2>{$aBooks.orders_header}</h2>
{$aBooks.orders_header2}
<br />
<table style="text-align: left; width: 100%;" border="1" cellpadding="2">
  <tbody>
    <tr>
      <td width="20px">Lp</td>
      <td width="45px">Regał</td>
      <td width="300px">Tytuł</td>
      <td width="100px">Autor</td>
      <td width="120px">Wydawca</td>
      <td width="80px">ISBN</td>
      <td width="25px">Ilość</td>
      <td width="25px">Stan</td>
    </tr>
    {foreach from=$aBooks.items item=aItem}
    <tr {if $aItem.quantity > $aItem.profit_source_stock}style="background-color: grey;"{/if}>
      <td width="20px">{$aItem.lp}</td>
      <td width="45px">{$aItem.location}</td>
      <td width="300px">{$aItem.name}</td>
      <td width="100px">{$aItem.authors}</td>
      <td width="120px">{$aItem.publisher}</td>
      <td width="80px">{$aItem.isbn}</td>
      <td width="25px" align="center">{$aItem.quantity}</td>
      <td width="25px">{$aItem.profit_source_stock|string_format:"%d"}</td>
    </tr>
    {/foreach}
    <tr>
      <td width="20px">&nbsp;</td>
      <td width="45px">&nbsp;</td>
      <td width="300px">&nbsp;</td>
      <td width="100px">&nbsp;</td>
      <td width="120px">&nbsp;</td>
      <td width="80px">SUMA:</td>
      <td width="25px" align="center">{$aBooks.Qsum}</td>
      <td width="25px">&nbsp;</td>
    </tr>
  </tbody>
</table>
<br />
<table cellspacing="0" cellpadding="1" border="1" width="555px" align="center">
<tr>
<td>{$aBooks.orders_foot1}</td>
<td>{$aBooks.orders_foot2}</td>
<td>{$aBooks.orders_foot3}</td>
</tr>
<tr>
<td>&nbsp;<br /><br /><br /></td>
<td>&nbsp;<br /><br /><br /></td>
<td>&nbsp;<br /><br /><br /></td>
</tr>
</table>