<script type="text/javascript">
var products = {$products};
console.log(products);
{literal}
var products_containers_quantity = [];
{/literal}
</script>
<script src="js/productsRecipientContainers.js"></script>

<div id="products_recipient_containers">
  <div style="background: #BBBBBB;">
    <button id="action_change_conainer">Zmień kuwetę</button>
    <button id="action_close_container">Zamknij kuwetę</button>
    <button id="action_close_recipient">Zamknij dostawę</button>
    <div id="recipient_detail">
      <div>
        <div>KUWETA:</div>
        <input id="container_id" value="" maxlength="10" />
      </div>
      <div>
        <div>EAN:</div>
        <input id="barcode" value="" maxlength="15" />
      </div>
      <div>
        <div>POZOSTAŁO: <span id="summary_left_scan"></span></div>
      </div>
      <div>
        <div>ZESKANOWANO: <span id="summary_scanned_items"></span></div>
      </div>
      <div>
        <div>DEFEKTY: <span id="summary_defect_items"></span></div>
      </div>
    </div>
    <div clas="clear"></div>
  </div>
  <div clas="clear"></div>
  <div id="main_product_details">
    <div id="main_product_cover_container"><img id="main_product_cover" /></div>
    <div id="main_product_name"></div>
    <div id="main_product_ean_13"></div>
    <div><span id="main_product_publisher"></span> / <span id="main_product_authors"></span></div>
    <div><button id="action_mark_as_defect">Defekt</button></div>
  </div>
  <div id="products_history">
    {section name=history_item start=0 loop=3 step=1}
      <div id="history_item_{$smarty.section.history_item.index}">
        <div class="product_defect">DEFEKT</div>
        <div class="product_cover_container"><img class="product_cover" /></div>
        <div class="product_name"></div>
        <div><span class="product_ean_13"></span> -> <span class="product_container_id"></span></div>
        <div><span class="product_publisher"></span> / <span class="product_authors"></span></div>
      </div>
    {/section}
  </div>
</div>