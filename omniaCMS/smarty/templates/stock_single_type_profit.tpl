<table cellpadding="1" cellspacing="0" border="1" style="font-size: 11px;">
  <tbody style="background-color: #fff;">
    <tr>
      <th class="it_stock" style="background-color: #cc0000; color: #fff;">Wewnętrzne</th>
      <th class="it_stock" style="background-color: #cc0000; color: #fff;">S</th>
      {*<th class="it_stock" style="background-color: #cc0000; color: #fff;">R</th>*}
      {*<th class="it_stock" style="background-color: #cc0000; color: #fff;">ERP</th>*}
      <th class="it_stock" style="background-color: #cc0000; color: #fff;">C.Zakup brutto</th>
      <th class="it_stock" style="background-color: #cc0000; color: #fff;">C.SRP Brutto</th>
      <th class="it_stock" style="background-color: #cc0000; color: #fff;">Vat</th>
      <th class="it_stock" style="background-color: #cc0000; color: #fff;">%</th>
    </tr>
    {foreach from=$aStockType item=aStock key=name}
      <tr class="bold">
        <td class="it_stock" style="font-weight: bold">{$aStock.name}</td>
        <td class="it_stock center_text">{$aStock.status}</td>
        {*<td class="it_stock center_text red">{$aStock.reservations}</td>*}
        {*<td class="it_stock center_text">{$aStock.act_stock}</td>*}
        <td class="it_stock center_text">{$aStock.wholesale_price}</td>
        <td class="it_stock center_text">{$aStock.price_brutto}</td>
        <td class="it_stock center_text">{$aStock.vat}</td>
        <td class="it_stock center_text"><span style="display: none;">{if $aStock.our_discount != ''}{$aStock.our_discount}{/if}</span></td>
      </tr>
      {foreach from=$aStock.supplies item=aStock2 key=name2}
        <tr class="bold">
          <td class="it_stock" style="font-weight: bold"> </td>
          <td class="it_stock center_text">{math equation="x - y" x=$aStock2.quantity y=$aStock2.reservation}</td>
          {*<td class="it_stock center_text red">{$aStock2.reservation}</td>*}
          {*<td class="it_stock center_text">{$aStock2.quantity}</td>*}
          <td class="it_stock center_text">{$aStock2.wholesale_price}</td>
          <td class="it_stock center_text">{$aStock2.price_brutto}</td>
          <td class="it_stock center_text">{$aStock2.vat}</td>
          <td class="it_stock center_text">{if $aStock2.our_discount != ''}{$aStock2.our_discount}{/if}</td>
        </tr>
      {/foreach}

      {if !empty($aStock.magazine_locations) }
        <tr>
          <th class="it_stock" style="background-color: #1c94c4; color: #fff;">STOCK </th>
          <th class="it_stock" style="background-color: #1c94c4; color: #fff;">S</th>
          {*<th class="it_stock" style="background-color: #1c94c4; color: #fff;">R</th>*}
          {*<th class="it_stock" style="background-color: #1c94c4; color: #fff;">P</th>*}
          <th class="it_stock" style="background-color: #1c94c4; color: #fff;">Lokalizacja</th>
          <th class="it_stock" style="background-color: #1c94c4; color: #fff;">Magazyn</th>
        </tr>
      {/if}
      {foreach from=$aStock.magazine_locations item=magazineLocation key=keyLoc}
        {if $magazineLocation.available > 0 ||
            $magazineLocation.reservation > 0 ||
            $magazineLocation.quantity > 0}
          <tr class="bold">
            <td class="it_stock" style="font-weight: bold"> </td>
            <td class="it_stock center_text">{$magazineLocation.available}</td>
            {*<td class="it_stock center_text red">{$magazineLocation.reservation}</td>*}
            {*<td class="it_stock center_text">{$magazineLocation.quantity}</td>*}
            <td class="it_stock center_text">{$magazineLocation.container_id}</td>
            <td class="it_stock center_text">{$magazineLocation.magazine_type}</td>
          </tr>
        {/if}
      {/foreach}
    {/foreach}
  </tbody>
</table>