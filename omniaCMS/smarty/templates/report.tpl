Raport magazynowy wygenerowany przez: <strong>{$aRaport.generator_name}</strong> <br/>
za okres {$aRaport.date_start} - {$aRaport.date_end}<br/>


<h3>Sprzedaż pobranie - Profit24.pl</h3>
{if !empty($aRaport.postal_fee.1)}
	{$aRaport.postal_fee.1}
	<br/>
	Łączna wartość pobrania {$aRaport.postal_fee_total.1}
{else}
	{$aRaport.lang.no_orders}
{/if}
<br/>

<h3>Sprzedaż pobranie - ksiegaria.IT</h3>
{if !empty($aRaport.postal_fee.2)}
	{$aRaport.postal_fee.2}
	<br/>
	Łączna wartość pobrania {$aRaport.postal_fee_total.2}
{else}
	{$aRaport.lang.no_orders}
{/if}
<br/>

<h3>Sprzedaż pobranie - nieprzeczytane.pl</h3>
{if !empty($aRaport.postal_fee.3)}
	{$aRaport.postal_fee.3}
	<br/>
	Łączna wartość pobrania {$aRaport.postal_fee_total.3}
{else}
	{$aRaport.lang.no_orders}
{/if}
<br/>

<h3>Sprzedaż Platnoscipl - Profit24.pl</h3>
{if !empty($aRaport.platnoscipl.1)}
{$aRaport.platnoscipl.1}
<br/>
Łączna wartość wpłat {$aRaport.platnoscipl_total.1}<br/>
zapłacono <br/>
{foreach from=$aRaport.platnoscipl_payments.1 item=aPayment}
	{$aPayment.payment_date} - {$aPayment.paid_amount}<br/>
{/foreach}
{else}
	{$aRaport.lang.no_orders}
{/if}
<br/>

<h3>Sprzedaż Platnoscipl - ksiegaria.IT</h3>
{if !empty($aRaport.platnoscipl.2)}
{$aRaport.platnoscipl.2}
<br/>
Łączna wartość wpłat {$aRaport.platnoscipl_total.2}<br/>
zapłacono <br/>
{foreach from=$aRaport.platnoscipl_payments.2 item=aPayment}
	{$aPayment.payment_date} - {$aPayment.paid_amount}<br/>
{/foreach}
{else}
	{$aRaport.lang.no_orders}
{/if}
<br/>

<h3>Sprzedaż Platnoscipl - nieprzeczytane.pl</h3>
{if !empty($aRaport.platnoscipl.3)}
{$aRaport.platnoscipl.3}
<br/>
Łączna wartość wpłat {$aRaport.platnoscipl_total.3}<br/>
zapłacono <br/>
{foreach from=$aRaport.platnoscipl_payments.3 item=aPayment}
	{$aPayment.payment_date} - {$aPayment.paid_amount}<br/>
{/foreach}
{else}
	{$aRaport.lang.no_orders}
{/if}
<br/>

<h3>Sprzedaż przelew przedpłata - Profit24.pl</h3>
{if !empty($aRaport.bank_transfer.1)}
{$aRaport.bank_transfer.1}
<br/>
Łączna wartość wpłat {$aRaport.bank_transfer_total.1}<br/>
zapłacono <br/>
{foreach from=$aRaport.bank_transfer_payments.1 item=aPayment}
	{$aPayment.payment_date} - {$aPayment.paid_amount}<br/>
{/foreach}
{else}
	{$aRaport.lang.no_orders}
{/if}
<br/>

<h3>Sprzedaż przelew przedpłata - ksiegaria.IT</h3>
{if !empty($aRaport.bank_transfer.2)}
{$aRaport.bank_transfer.2}
<br/>
Łączna wartość wpłat {$aRaport.bank_transfer_total.2}<br/>
zapłacono <br/>
{foreach from=$aRaport.bank_transfer_payments.2 item=aPayment}
	{$aPayment.payment_date} - {$aPayment.paid_amount}<br/>
{/foreach}
{else}
	{$aRaport.lang.no_orders}
{/if}
<br/>

<h3>Sprzedaż przelew przedpłata - nieprzeczytane.pl</h3>
{if !empty($aRaport.bank_transfer.3)}
{$aRaport.bank_transfer.3}
<br/>
Łączna wartość wpłat {$aRaport.bank_transfer_total.3}<br/>
zapłacono <br/>
{foreach from=$aRaport.bank_transfer_payments.3 item=aPayment}
	{$aPayment.payment_date} - {$aPayment.paid_amount}<br/>
{/foreach}
{else}
	{$aRaport.lang.no_orders}
{/if}
<br/>

<h3>Sprzedaż przelew 14 dni - Profit24.pl</h3>
{if !empty($aRaport.bank_14days.1)}
{foreach from=$aRaport.bank_14days.1 key=iId item=aOrder name=vatorders}
		{if $aOrder.invoice_address.is_company == '1'}
				<strong>{$aOrder.invoice_address.company}</strong><br/>
				{$aOrder.invoice_address.street} {$aOrder.invoice_address.number}{if !empty($aOrder.invoice_address.number2)}/{$aOrder.invoice_address.number2}{/if}<br/>
				{$aOrder.invoice_address.postal} {$aOrder.invoice_address.city}<br/>
				{$aOrder.invoice_address.nip}<br/>
				
			{else}
				<strong>{$aOrder.invoice_address.name} {$aOrder.invoice_address.surname}</strong><br/>
				{$aOrder.invoice_address.street} {$aOrder.invoice_address.number}{if !empty($aOrder.invoice_address.number2)}/{$aOrder.invoice_address.number2}{/if}<br/>
				{$aOrder.invoice_address.postal} {$aOrder.invoice_address.city}<br/>
			{/if}
			
	{$aOrder.items}
	<br/>
	Łączna wartość zamówienia {$aOrder.total}<br/>
{/foreach}
{else}
	{$aRaport.lang.no_orders}
{/if}


<h3>Sprzedaż przelew 14 dni - ksiegaria.IT</h3>
{if !empty($aRaport.bank_14days.2)}
{foreach from=$aRaport.bank_14days.2 key=iId item=aOrder name=vatorders}
		{if $aOrder.invoice_address.is_company == '1'}
				<strong>{$aOrder.invoice_address.company}</strong><br/>
				{$aOrder.invoice_address.street} {$aOrder.invoice_address.number}{if !empty($aOrder.invoice_address.number2)}/{$aOrder.invoice_address.number2}{/if}<br/>
				{$aOrder.invoice_address.postal} {$aOrder.invoice_address.city}<br/>
				{$aOrder.invoice_address.nip}<br/>
				
			{else}
				<strong>{$aOrder.invoice_address.name} {$aOrder.invoice_address.surname}</strong><br/>
				{$aOrder.invoice_address.street} {$aOrder.invoice_address.number}{if !empty($aOrder.invoice_address.number2)}/{$aOrder.invoice_address.number2}{/if}<br/>
				{$aOrder.invoice_address.postal} {$aOrder.invoice_address.city}<br/>
			{/if}
			
	{$aOrder.items}
	<br/>
	Łączna wartość zamówienia {$aOrder.total}<br/>
{/foreach}
{else}
	{$aRaport.lang.no_orders}
{/if}


<h3>Sprzedaż przelew 14 dni - nieprzeczytane.pl</h3>
{if !empty($aRaport.bank_14days.3)}
{foreach from=$aRaport.bank_14days.3 key=iId item=aOrder name=vatorders}
		{if $aOrder.invoice_address.is_company == '1'}
				<strong>{$aOrder.invoice_address.company}</strong><br/>
				{$aOrder.invoice_address.street} {$aOrder.invoice_address.number}{if !empty($aOrder.invoice_address.number2)}/{$aOrder.invoice_address.number2}{/if}<br/>
				{$aOrder.invoice_address.postal} {$aOrder.invoice_address.city}<br/>
				{$aOrder.invoice_address.nip}<br/>
				
			{else}
				<strong>{$aOrder.invoice_address.name} {$aOrder.invoice_address.surname}</strong><br/>
				{$aOrder.invoice_address.street} {$aOrder.invoice_address.number}{if !empty($aOrder.invoice_address.number2)}/{$aOrder.invoice_address.number2}{/if}<br/>
				{$aOrder.invoice_address.postal} {$aOrder.invoice_address.city}<br/>
			{/if}
			
	{$aOrder.items}
	<br/>
	Łączna wartość zamówienia {$aOrder.total}<br/>
{/foreach}
{else}
	{$aRaport.lang.no_orders}
{/if}
