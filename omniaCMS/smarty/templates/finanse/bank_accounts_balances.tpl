<h1>Salda bankowe</h1>

<table class="finanse_bank_accounts_balances">
  <tr>
    <th>Rachunek</th>
    <th>Aktualne saldo</th>
    <th>Saldo na dzień</th>
  </tr>
{foreach from=$bankBalancesDays item=aBalance}
  <tr>
    <td>
      {$aBalance.number}
    </td>
    <td style="text-align: right;">
      {$aBalance.balance|format_number:2:','}
    </td>
    {foreach from=$aBalance.days_transfers item=aDayBalance}
      <td>
        <b>{$aDayBalance.date|date_format:"%d.%m.%Y"}</b><br />
        Przelewy: {$aDayBalance.paid_amount|format_number:2:','}<br />
{*        Saldo początkowe na dzień: {$aDayBalance.start_balance|format_number:2:','}<br />*}
        Saldo: {$aDayBalance.end_balance|format_number:2:','}
      </td>
    {/foreach}
  </tr>
{/foreach}
</table>