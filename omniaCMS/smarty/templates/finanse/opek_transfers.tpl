<script type="text/javascript">
  var aOrdersToPayedBalance = {$sOrdersToPayedBalanceJS};
</script>
<form method="POST">
  <input type="hidden" name="do" value="save" />
  <input type="hidden" name="transport_id" value="{$iTransportId}" />
  
  <table class="formTable" cellpadding="0" border="0" cellspacing="0">
    <tbody>
      <tr>
        <th class="brownBg" style="width: 140px; text-align: center; color: #e7e5d5;">
          <b>Numer listu</b>
        </th>
        <th class="brownBg" style="width: 220px; text-align: center; color: #e7e5d5;">
          <b>Zamówienia</b>
        </th>
        <th class="brownBg" style="width: 100px; text-align: center; color: #e7e5d5;">
          <b>Wartość zamówienia z CSV</b>
        </th>
        <th class="brownBg" style="width: 100px; text-align: center; color: #e7e5d5;">
          <b>Wartość zamówienia z DB</b>
        </th>
        <th class="brownBg" style="width: 150px; text-align: center; color: #e7e5d5;">
          <b>Opłacono</b>
        </th>
        <th class="brownBg"></th>
      </tr>
      <input type="hidden" name="sTransferSerialized" value='{$sTransferSerialized}' />

      {foreach from=$aTransfers key=iTransfer item=aTransfer}
        <tr class="row">
          <td style="vertical-align: top; font-size:14px">
            <input type="hidden" name="transport_number[{$iTransfer}]" value="{$aTransfer.1}" />
            <strong>{$aTransfer.1}</strong>
            <span style="font-size:10px">
              {foreach from=$aTransfer.orders key=iOrderNumber item=sOrderNumber}
                <br />{$sOrderNumber}
              {/foreach}
            </span>
          </td>
          <td style="vertical-align: top" id="order_transfer_{$iTransfer}">
            {foreach from=$aTransfer.transfers_orders item=sCompareHTML key=i}
              {$sCompareHTML}<br />
            {/foreach}
          </td>
          <td style="vertical-align: top;">
            <div class="order_value_CSV" style="border-bottom: 1px solid #000; text-align: right; font-size: 12px; height: 17px; padding: 0; margin: 0; margin-top: 3px;">{$aTransfer.5}</div>
          </td>
          <td style="vertical-align: top;" id="order_value_{$iTransfer}">
            {foreach from=$aTransfer.transfers_orders item=sCompareHTML key=iTransferOrders}
              <div name="order_value[{$iTransfer}][{$iTransferOrders}]" id="order_value[{$iTransfer}][{$iTransferOrders}]" style="border-bottom: 1px solid #000; text-align: right; font-size: 12px; height: 17px; padding: 0; margin: 0; margin-top: 3px;"></div>
            {/foreach}
          </td>
          <td style="vertical-align: top" id="order_payed_{$iTransfer}">
            {foreach from=$aTransfer.transfers_orders item=sCompareHTML key=iTransferOrders}
              <input type="text" name="order_payed[{$iTransfer}][{$iTransferOrders}]" id="order_payed[{$iTransfer}][{$iTransferOrders}]" /><br />
            {/foreach}
          </td>
          <td style="vertical-align: top">
            <a onclick="cloneSelect({$iTransfer})" href="javascript:void(0);" style="font-size: 10px; text-decoration: none;" title="dodaj zamówienie">
              <img src="gfx/icons/add.gif" alt="dodaj zamówienie" />
              dodaj zamówienie
            </a>
          </td>
        </tr>
      {/foreach}

      <tr>
        <td>
        </td>
        <td>
        </td>
        <td>
        </td>
        <td id="summary_order_value">
          <b></b>
        </td>
        <td id="summary_order_payed">
          <b></b>
        </td>
        <td></td>
      </tr>


    <tbody>
  </table>



  <table class="formTable" cellpadding="0" border="0" cellspacing="0">
    <tr>
      <th class="brownBg" style="width: 140px; text-align: center; color: #e7e5d5;">
        Wybierz przelew/przelewy
      </th>
      <th class="brownBg" style="width: 140px; text-align: center; color: #e7e5d5;">&nbsp;</th>
    </tr>
    <tr>
      <td>
        {foreach from=$aBankTransfer item=sTransfer}
          {$sTransfer}<br />
        {/foreach}
      </td>
      <td style="vertical-align: top">
        <a onclick="cloneSelectBankTransfer()" href="javascript:void(0);" style="font-size: 10px; text-decoration: none; " title="dodaj zamówienie">
          <img src="gfx/icons/add.gif" alt="dodaj zamówienie" />
          dodaj przelew
        </a>
      </td>
    </tr>
    <tr>
      <td>
        Suma przelewów <b><span id="summary_transfers"></span></b><br />
        Suma zamówień z DB <b><span id="summary_orders"></span></b><br />
        Suma zamówień z CSV <b><span id="summary_orders_CSV"></span></b><br />
        Różnica <b><span style="color: red" id="summary_diff"><b/></span>
      </td>
      <td> </td>
    </tr>
  </table>
  
      
      
      <input type="submit" value="zapisz" />
</form>
    
    
{literal}
  <script type="text/javascript">
    
    /**
     * Funkcja ustawia wartość zamowienia i ile opłacono 
     *  na podstawie przekazanego obiektu selecta
     * 
     * @param object oThis - obiekt samego siebie
     * @returns void
     */
    function setOrderBalance(oThis) {
      var sValName = $(oThis).val();
      var sNameElement = $(oThis).attr('name');
      var sValElement = sNameElement.replace('transfers_orders', '');

      if (sValName !== '') {
        $("[name='order_value"+sValElement+"']").html(aOrdersToPayedBalance[sValName]);
        $("[name='order_payed"+sValElement+"']").val(aOrdersToPayedBalance[sValName]);
      } else {
        $("[name='order_value"+sValElement+"']").html('');
        $("[name='order_payed"+sValElement+"']").val('');
      }
    }
    
    
    /**
     * Funkcja sumuje wartosć zamówienia oraz ile opłacono
     * 
     * @returns void
     */
    function recountSummary() {
      
      var iOrdersValue = $("[name^='order_value']").sum()+"";
      $("#summary_order_value").html(iOrdersValue.replace('.', ','));
      
      var iOrdersValue = $("[name^='order_payed']").sum()+"";
      $("#summary_order_payed").html(iOrdersValue.replace('.', ','));
    }
    
    /**
     * Funkcja sumuje wartosć zamówienia oraz ile opłacono
     * 
     * @returns void
     */
    function recountSummaryDiff() {
      
      var st = parseFloat($("#summary_transfers").html());
      var sop = parseFloat($("#summary_order_payed").html().replace(',', '.'));
      $("#summary_orders").html(sop.toFixed(2));
      var diff = st - sop;
      $("#summary_diff").html(diff.toFixed(2));
    }
    
    
    /**
     * Metoda powiela select zamowienia
     * 
     * @param integer iTransfer  id listu do przelewu
     * @returns void
     */
    function cloneSelect(iTransfer) {
      var sOrderSelectTransfer = "order_transfer_"+iTransfer;
      var iTransferOrder = $("#"+sOrderSelectTransfer+" select").size();
      $("#"+sOrderSelectTransfer+" select:first")
              .clone()
              .attr("name", 'transfers_orders['+iTransfer+']['+iTransferOrder+']')
              .attr("id", 'transfers_orders['+iTransfer+']['+iTransferOrder+']')
              .attr("value", '')
              .appendTo("#"+sOrderSelectTransfer)
              .after("<br />");
      
      
      var sOrderSelectTransfer = "order_value_"+iTransfer;
      var iTransferOrder = $("#"+sOrderSelectTransfer+" div").size();
      $("#"+sOrderSelectTransfer+" div:first")
              .clone()
              .attr("name", 'order_value['+iTransfer+']['+iTransferOrder+']')
              .attr("id", 'order_value['+iTransfer+']['+iTransferOrder+']')
              .html('')
              .appendTo("#"+sOrderSelectTransfer);
      
      
      var sOrderSelectTransfer = "order_payed_"+iTransfer;
      var iTransferOrder = $("#"+sOrderSelectTransfer+" input").size();
      $("#"+sOrderSelectTransfer+" input:first")
              .clone()
              .attr("name", 'order_payed['+iTransfer+']['+iTransferOrder+']')
              .attr("id", 'order_payed['+iTransfer+']['+iTransferOrder+']')
              .attr("value", '')
              .appendTo("#"+sOrderSelectTransfer)
              .after("<br />");
    }
    
    /**
     * Metoda powiela element przelewu bankowego
     * 
     * @returns void
     */
    function cloneSelectBankTransfer() {
      $("select[name^=bank_transfers]:first")
        .clone()
        .attr("value", '')
        .appendTo($("select[name^=bank_transfers]:first").parent())
        .after("<br />");
    }
    
    
    function recountBankTransfersSummary() {
      var fSum = 0;
      $("select[id^=bank_transfers] option:selected").each( function () {
        if ($(this).val() !== '') {
          var aTMP = $(this).text().split(" ");
          fSum += parseFloat(aTMP[0]);
        }
      })
      $("#summary_transfers").html(fSum.toFixed(2));
    }
    
    
    $(function() {
      $('body').on('change', '.transfers_orders', function() {
        setOrderBalance(this);
        recountSummary();
        recountSummaryDiff();
      });
      
      $('body').on('change', 'input[name^=order_payed]', function() {
        recountSummary();
        recountSummaryDiff();
      });
      
      $('body').on('change', 'select[name^=bank_transfers]', function() {
        recountBankTransfersSummary();
        recountSummaryDiff();
      });
      
      $("#summary_orders_CSV").html($(".order_value_CSV").sum());
       
      $('.transfers_orders').change();
      $('.bank_transfers').change();
    });
    
  </script>
{/literal}


