<table cellspacing="0" cellpadding="1" border="1" width="510px" align="center">
	<tr>
		<th rowspan="2" width="18px">{$aModule.lang.lp}</th>
		<th rowspan="2" width="58px">{$aModule.lang.payment_method}</th>
		<th rowspan="2" width="70px">{$aModule.lang.value_brutto}</th>
		<th rowspan="2" width="70px">{$aModule.lang.value_netto}</th>
		{if $aModule.table_cols.show_vat_5}
			<th colspan="2" width="100px">{$aModule.lang.vat_5}</th>
		{/if}
		{if $aModule.table_cols.show_vat_8}
			<th colspan="2" width="100px">{$aModule.lang.vat_8}</th>
		{/if}
		{if $aModule.table_cols.show_vat_22}	
			<th colspan="2" width="110px">{$aModule.lang.vat_22}</th>
		{/if}
		{if $aModule.table_cols.show_vat_23}	
			<th colspan="2" width="110px">{$aModule.lang.vat_23}</th>
		{/if}	
		{if $aModule.table_cols.show_vat_0}
			<th width="55px">{$aModule.lang.vat_0}</th>
		{/if}	
			<th rowspan="2" width="60px">{$aModule.lang.order_vat}</th>
	</tr>
    <tr>
      {if $aModule.table_cols.show_vat_5}
        <td width="65px">{$aModule.lang.vat_22_value_netto}</td>
        <td width="35px">{$aModule.lang.vat_22_currency}</td>
      {/if}	
      {if $aModule.table_cols.show_vat_8}
        <td width="65px">{$aModule.lang.vat_22_value_netto}</td>
        <td width="35px">{$aModule.lang.vat_22_currency}</td>
      {/if}	
      {if $aModule.table_cols.show_vat_22}
        <td width="65px">{$aModule.lang.vat_22_value_netto}</td>
        <td width="45px">{$aModule.lang.vat_22_currency}</td>
      {/if}	
      {if $aModule.table_cols.show_vat_23}
        <td width="65px">{$aModule.lang.vat_22_value_netto}</td>
        <td width="45px">{$aModule.lang.vat_22_currency}</td>
      {/if}	
      {if $aModule.table_cols.show_vat_0}
        <td width="55px">{$aModule.lang.vat_0_value_netto}</td>
      {/if}	
    </tr>
    {*foreach from=$aTable.items key=iId item=aItem name=types}
      <tr>
        <td width="18px" align="left">{$aItem.lp}</td>
        <td width="58px" align="left">{$aItem.order_date}</td>
        <td width="70px" align="right">{$aItem.value_brutto}</td>
        <td width="70px" align="right">{$aItem.value_netto}</td>
        {if $aModule.table_cols.show_vat_5}
          <td width="65px" align="right">{if $aItem.vat[5].value_netto!=''} {$aItem.vat[5].value_netto} {else} 0,00 {/if}</td>
          <td width="35px" align="right">{if $aItem.vat[5].vat_currency!=''} {$aItem.vat[5].vat_currency} {else} 0,00 {/if}</td>
        {/if}
        {if $aModule.table_cols.show_vat_8}
          <td width="65px" align="right">{if $aItem.vat[8].value_netto!=''} {$aItem.vat[8].value_netto} {else} 0,00 {/if}</td>
          <td width="35px" align="right">{if $aItem.vat[8].vat_currency!=''} {$aItem.vat[8].vat_currency} {else} 0,00 {/if}</td>
        {/if}
        {if $aModule.table_cols.show_vat_22}
          <td width="65px" align="right">{if $aItem.vat[22].value_netto!=''} {$aItem.vat[22].value_netto} {else} 0,00 {/if}</td>
          <td width="45px" align="right">{if $aItem.vat[22].vat_currency!=''} {$aItem.vat[22].vat_currency} {else} 0,00 {/if}</td>
        {/if}	
        {if $aModule.table_cols.show_vat_23}
          <td width="65px" align="right">{if $aItem.vat[23].value_netto!=''} {$aItem.vat[23].value_netto} {else} 0,00 {/if}</td>
          <td width="45px" align="right">{if $aItem.vat[23].vat_currency!=''} {$aItem.vat[23].vat_currency} {else} 0,00 {/if}</td>
        {/if}
        {if $aModule.table_cols.show_vat_0}	
          <td width="55px" align="right">{if $aItem.vat[0].value_netto!=''} {$aItem.vat[0].value_netto} {else} 0,00 {/if}</td>
        {/if}	
          <td width="60px" align="right">{$aItem.order_vat}</td>
      </tr>
    {/foreach*}
    {foreach from=$aModule.table key=iId item=aTable name=types}
    <tr>
        <td width="76px" colspan="2">{$aModule.lang.summary} {$aModule.lang.payment_methods.$iId}</td>
        <td width="70px" align="right">{$aTable.total_brutto}</td>
        <td width="70px" align="right">{$aTable.total_netto}</td>
        {if $aModule.table_cols.show_vat_5}
          <td width="65px" align="right">{if $aTable.vat_5_value!=''} {$aTable.vat_5_value} {else} 0,00 {/if}</td>
          <td width="35px" align="right">{if $aTable.vat_5_currency!=''} {$aTable.vat_5_currency} {else} 0,00 {/if}</td>
        {/if}
        {if $aModule.table_cols.show_vat_8}
          <td width="65px" align="right">{if $aTable.vat_8_value!=''} {$aTable.vat_8_value} {else} 0,00 {/if}</td>
          <td width="35px" align="right">{if $aTable.vat_8_currency!=''} {$aTable.vat_8_currency} {else} 0,00 {/if}</td>
        {/if}
        {if $aModule.table_cols.show_vat_22}
          <td width="65px" align="right">{if $aTable.vat_22_value!=''} {$aTable.vat_22_value} {else} 0,00 {/if}</td>
          <td width="45px" align="right">{if $aTable.vat_22_currency!=''} {$aTable.vat_22_currency} {else} 0,00 {/if}</td>
        {/if}	
        {if $aModule.table_cols.show_vat_23}
          <td width="65px" align="right">{if $aTable.vat_23_value!=''} {$aTable.vat_23_value} {else} 0,00 {/if}</td>
          <td width="45px" align="right">{if $aTable.vat_23_currency!=''} {$aTable.vat_23_currency} {else} 0,00 {/if}</td>
        {/if}	
        {if $aModule.table_cols.show_vat_0}
          <td width="55px" align="right">{$aTable.vat_0_value}</td>
        {/if}	
          <td width="60px" align="right">{$aTable.total_vat}</td>
      </tr>
  {/foreach}
  
  
  
    <tr>
        <td width="76px" colspan="2">{$aModule.lang.summary} {$aModule.lang.payment_methods.$iId}</td>
        <td width="70px" align="right">{$aModule.summary.total_brutto}</td>
        <td width="70px" align="right">{$aModule.summary.total_netto}</td>
        {if $aModule.table_cols.show_vat_5}
          <td width="65px" align="right">{if $aModule.summary.vat_5_value!=''} {$aModule.summary.vat_5_value} {else} 0,00 {/if}</td>
          <td width="35px" align="right">{if $aModule.summary.vat_5_currency!=''} {$aModule.summary.vat_5_currency} {else} 0,00 {/if}</td>
        {/if}
        {if $aModule.table_cols.show_vat_8}
          <td width="65px" align="right">{if $aModule.summary.vat_8_value!=''} {$aModule.summary.vat_8_value} {else} 0,00 {/if}</td>
          <td width="35px" align="right">{if $aModule.summary.vat_8_currency!=''} {$aModule.summary.vat_8_currency} {else} 0,00 {/if}</td>
        {/if}
        {if $aModule.table_cols.show_vat_22}
          <td width="65px" align="right">{if $aModule.summary.vat_22_value!=''} {$aModule.summary.vat_22_value} {else} 0,00 {/if}</td>
          <td width="45px" align="right">{if $aModule.summary.vat_22_currency!=''} {$aModule.summary.vat_22_currency} {else} 0,00 {/if}</td>
        {/if}	
        {if $aModule.table_cols.show_vat_23}
          <td width="65px" align="right">{if $aModule.summary.vat_23_value!=''} {$aModule.summary.vat_23_value} {else} 0,00 {/if}</td>
          <td width="45px" align="right">{if $aModule.summary.vat_23_currency!=''} {$aModule.summary.vat_23_currency} {else} 0,00 {/if}</td>
        {/if}	
        {if $aModule.table_cols.show_vat_0}
          <td width="55px" align="right">{$aModule.summary.vat_0_value}</td>
        {/if}	
          <td width="60px" align="right">{$aModule.summary.total_vat}</td>
      </tr>
</table>

