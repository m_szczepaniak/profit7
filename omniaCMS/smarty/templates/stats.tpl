<table class="statsTable" style="width: 400px; float: left; border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
<tr>
  <th>DATA</td>
  <th>PLAN</td>
  <th>ZATW/ZREA</td>
</tr>
{foreach from=$aData.table_dates key=date item=item}
  <tr>
    <td>{$date}</td>
    <td><a style="font-size: 16px; text-decoration: underline;" target="_blank" href="{$item.link}">{$item.count_planned}</a></td>
    <td>{$item.count_confirmed}</td>
  </tr>
{/foreach}
</table>

<DIV style="float: left; ">
  {$sListTypeHTML}
  {$sCompletationHTML}
  <br />
  {*
  <table class="statsTable" style="border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th colspan="2">WYSYŁKA</td>
    <th>SIN</td>
  </tr>
  {foreach from=$aData.ready_to_send key=date item=item}
    <tr>
      <td>{$item.transport}</td>
      <td>{$item.count}</td>
      <td>{$item.single_count}</td>
    </tr>
  {/foreach}
  </table>

  <table class="statsTable" style="border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th colspan="2">ZBIERANE ZAMÓWIENIA</td>
    <th>SIN</td>
  </tr>
  {foreach from=$aData.completatnion key=date item=item}
    <tr>
      <td>{$item.transport}</td>
      <td>{$item.count}</td>
      <td>{$item.single_count}</td>
    </tr>
  {/foreach}
  </table>
  
  *}
  <table class="statsTable" style="border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th colspan="2">SORTOWANE</td>
    <th>SIN</td>
  </tr>
  {foreach from=$aData.sorting key=date item=item}
    <tr>
      <td>{$item.transport|substr:0:10}</td>
      <td>{$item.count}</td>
      <td>{$item.single_count}</td>
    </tr>
  {/foreach}
  </table>
  
  
  <table class="statsTable" style="border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th colspan="2">POSORTOWANE</td>
    <th>SIN</td>
  </tr>
  {foreach from=$aData.out_sorting key=date item=item}
    <tr>
      <td>{$item.transport|substr:0:10}</td>
      <td>{$item.count}</td>
      <td>{$item.single_count}</td>
    </tr>
  {/foreach}
  </table>
  
  <table class="statsTable" style="border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th colspan="2">ZREAL LUB ZATW - DZIŚ</td>
    <th>SIN</td>
  </tr>
  {foreach from=$aData.confirm_or_realized key=date item=item}
    <tr>
      <td>{$item.transport|substr:0:10}</td>
      <td>{$item.count}</td>
      <td>{$item.single_count}</td>
    </tr>
  {/foreach}
  </table>
  
</DIV>
  <div class="clear"></div>
  <br />
<div style="text-align: left;">
    1) PLAN - zamówienia otwarte z ustawioną planowaną datą wysyłki na dzień. <br />
    2) ZATW/ZREA - zamówienia zrealizowane lub zatwierdzone, których data zatwierdzenia jest dziś.<br /> 
    3) WYSYŁKA - zamówienia gotowe do pobrania na listę do zebrania - czyli zamówienia spełniające wszystkie warunki wysyłki dziś i nie zostały jeszcze pobrane do zebrania. <br />
    4) ZBIERANE ZAMÓWIENIA - zamówienia, które zostały pobrane na listę do zebrania. <br />
    5) SORTOWANE - zamówienia wczytane na sortownik. <br />
    6) POSORTOWANE - zamówienia przesortowane. <br />
    7) ZREAL LUB ZATW - DZIŚ - zamówienia zrealizowane lub zatwierdzone, których data zatwierdzenia jest dziś.
</div>

  
  