<script type="text/javascript">
    var products = {$products_encoded};
    console.log(products);
    {literal}
    var products_containers_quantity = [];
    {/literal}
</script>
<script src="js/productsCollecting.js?date=29042017"></script>


<div class="fixed-couter">
    <div>
        EAN: <input id="search_isbn" type="text" autofocus/>
    </div>
    Pozostało do zebrania: <span id="counter_quantity">{$products|@count}</span>
</div>

{foreach item=row from=$products}
    <div id="product_{$row.product_id}">
        <div id="collecting_high">
            <span class="location-style">{$row.location}</span>
            {if !empty($row.image)}
                {if !empty($row.image.big)}
                    <div class="image">
                        <img class="dest_image" src="{$row.image.big}"/>
                    </div>
                {else}
                    <div class="image">
                        <img src="{$row.image.small}"/>
                    </div>
                {/if}
            {/if}
            <span class="name">{$row.name}</span>
            <div class="d_quantity">Ilość: <span class="quantity">{$row.quantity}</span></div>
            <div class="d_profit_g_act_stock">Stock: <span class="profit_g_act_stock">{$row.profit_g_act_stock}</span>
            </div>
            <span class="ean_13">{$row.ean_13}</span>
        </div>
    </div>
{/foreach}
