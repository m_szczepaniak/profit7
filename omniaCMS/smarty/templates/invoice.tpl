<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td width="40%" align="left">
			<img src="/images/gfx/faktura-logo{if $aModule.website_symbol != 'profit24'}-{$aModule.website_symbol}{/if}.jpg" width="200px" height="76px"/><br/>
			
		</td>
		<td width="60%" align="right" style="font-size: 9pt;">
			<br/>
			{$aModule.seller_data.name}<br/>
			{$aModule.seller_data.street} {$aModule.seller_data.number}{if !empty($aModule.seller_data.number2)}/{$aModule.seller_data.number2}{/if}<br/>
			{$aModule.seller_data.postal} {$aModule.seller_data.city}<br/>
			{$aModule.seller_data.bank_name} {$aModule.lang.seller_bank_account} {$aModule.seller_data.bank_account}<br/>
			{if !empty($aModule.invoice_barcode)}
				<img src="{$aModule.invoice_barcode}" alt="Kod kreskowy" />
			{/if}
		</td>
	</tr>
	<tr>
		<td width="70%" align="left" style="font-size: 15pt;">
			{if $aModule.pro_forma == '1'}{$aModule.lang.list_proforma_nr}{else}{$aModule.lang.list_vat_nr}{/if} <strong>{$aModule.invoice_number}</strong> / <span style="font-size: 13pt;">{$aModule.lang.oryginal_copy}</span>
		</td>
		<td width="30%" align="right">
			{$aModule.lang.list_city_day} {$aModule.order.invoice_date}<br/>
			{$aModule.lang.list_data_order} {$aModule.order.invoice_date}
		</td>
	</tr>
</table>
<br />
<hr>
<div style="margin-bottom:20px;">&nbsp;</div>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 8pt;">
	<tr>
		<th width="60%">
			<strong>{$aModule.lang.user_data_header}</strong><br/>
		</th>
		<th width="40%">
			<strong>{$aModule.lang.invoice_data_header}</strong><br/>
		</th>
	</tr>
	<tr>
		<td width="60%">
			<strong>{$aModule.seller_data.invoice_name}</strong><br/>
			{$aModule.seller_data.postal} {$aModule.seller_data.city} {$aModule.seller_data.street} {$aModule.seller_data.number}{if !empty($aModule.seller_data.number2)}/{$aModule.seller_data.number2}{/if}<br/>
			{*{$aModule.seller_data.bank_name} {$aModule.lang.seller_bank_account} {$aModule.seller_data.bank_account}<br/>*}
			{$aModule.lang.list_nip} {$aModule.seller_data.nip}, {$aModule.lang.list_regon} {$aModule.seller_data.regon}<br/><br/>
		</td>
		<td width="40%">
			{if $aModule.address.is_company == '1'}
				<strong>{$aModule.address.company}</strong><br/>
				{$aModule.lang.list_street} {$aModule.address.street} {$aModule.address.number}{if !empty($aModule.address.number2)}/{$aModule.address.number2}{/if},{$aModule.lang.postal} {$aModule.address.postal} {$aModule.address.city}<br/>
				{if !empty($aModule.address.nip)}{$aModule.address.nip}<br/>{/if}
				
			{else}
				<strong>{$aModule.address.name} {$aModule.address.surname}</strong><br/>
				{$aModule.lang.list_street} {$aModule.address.street} {$aModule.address.number}{if !empty($aModule.address.number2)}/{$aModule.address.number2}{/if},{$aModule.lang.postal} {$aModule.address.postal} {$aModule.address.city}<br/>
			{/if}

			{if $aModule.order.invoice_recipient != ""}
				<br />
				<strong>Dane Odbiorcy:</strong><br/><br />
				{$aModule.order.invoice_recipient|nl2br}
				<br />
			{/if}
		</td>
	</tr>
</table>
<hr>
<div style="margin-bottom:20px;">&nbsp;</div>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 8pt;">
	<tr>
		<td width="25%">
			<strong>{$aModule.lang.selected_payment}</strong> {$aModule.order.payment}<br/><br/>
		</td>
		<td width="40%" align="center">
			<strong>{$aModule.lang.selected_transport}</strong> {$aModule.order.transport}<br/><br/>
		</td>
		<td width="35%" align="right">
			{if !empty($aModule.order.transport_number)}<strong>{$aModule.lang.list_transport_nubmer}</strong> {$aModule.order.transport_number}{/if}<br/><br/>
		</td>	
	</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 8pt;" border="1">
	<tr>
		<th width="17px" align="center" rowspan="2">{$aModule.lang.list_lp}</th>
		<th width="140px" align="left" rowspan="2">{$aModule.lang.list_name}</th>
		<th width="92px" align="center" rowspan="2">{$aModule.lang.list_symbol}</th>
		<th width="32px" align="center" rowspan="2">{$aModule.lang.list_quantity}</th>
		<th width="32px" align="center" rowspan="2">{$aModule.lang.list_jm}</th>
		<th width="55px" align="center" rowspan="2">{$aModule.lang.list_price_brutto}</th>
		<th width="37px" align="center" rowspan="2">{$aModule.lang.list_discount}</th>
		<th width="55px" align="center" rowspan="2">{$aModule.lang.list_promo_price_netto}</th>
		<th width="55px" align="center" rowspan="2">{$aModule.lang.list_value_netto}</th>
		<th width="70px" align="center" colspan="2">{$aModule.lang.list_vat}</th>
		<th width="55px" align="center" rowspan="2">{$aModule.lang.list_value_brutto}</th>
	</tr>
	<tr>
		<th width="24px" align="center">{$aModule.lang.list_st}</th>
		<th width="46px" align="center">{$aModule.lang.list_price}</th>
	</tr>
	{foreach from=$aModule.invoice_items key=iId item=aItems name=types}
		<tr>
			<td width="17px" align="center">{$aItems.lp}</td>
			<td width="140px" align="left">
				{$aItems.name}<br />
				{$aItems.publisher}
			</td>
			<td width="92px" align="center" style="font-size: 34px;">{$aItems.isbn}</td>
			<td width="32px" align="center" style="font-size:32px;"><b>{$aItems.quantity}</b></td>
			<td width="32px" align="center">{$aItems.jm}</td>
			<td width="55px" align="center">{$aItems.price_brutto|format_number:2:',':' '}</td>
			<td width="37px" align="center">{$aItems.discount|format_number:2:',':' '}</td>
			<td width="55px" align="center">{$aItems.promo_price_netto|format_number:2:',':' '}</td>
			<td width="55px" align="center">{$aItems.value_netto|format_number:2:',':' '}</td>
			<td width="24px" align="center">{$aItems.vat}</td>
			<td width="46px" align="center">{$aItems.vat_currency|format_number:2:',':' '}</td>
			<td width="55px" align="center">{$aItems.value_brutto|format_number:2:',':' '}</td>
		</tr>
	{/foreach}
</table>
<br/><br />
<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 8pt;" border="0">
	<tr>
		<td width="232px" align="center"></td>
		<td width="66px" align="center" style="font-size:32px;"><b>{$aModule.order.quantity}</b></td>
		<td width="108px" align="center"></td>
		<td width="55px" align="center">{*$aModule.lang.list_together*}</td>
		<td width="55px" align="center">{*$aModule.order.value_netto*}</td>
		<td width="24px" align="center"></td>
		<td width="46px" align="center">{$aModule.lang.list_together}{*$aModule.order.vat*}</td>
		<td width="55px" align="center">{$aModule.order.value_brutto}</td>
	</tr>
</table>
<br/><br />
<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 8pt;" border="0">
	<tr>
		<td width="335px"></td>
		<td width="275px">
			<table width="275px" cellspacing="0" style="font-size: 8pt;" border="1" align="center">
				<tr>
					<td width="40px"></td>
					<td width="24px"></td>
					<td width="91px">{$aModule.lang.list_value_netto}</td>
					<td width="75px">{$aModule.lang.list_price_vat}</td>
					<td width="75px">{$aModule.lang.list_value_brutto}</td>
				</tr>
				{foreach from=$aModule.vat item=aVat key=iVat name=vats}
					<tr>
						<td width="40px">{if $smarty.foreach.vats.first}W tym{/if}</td>
						<td width="24px">{$iVat}%</td>
						<td width="91px">{$aVat.value_netto}</td>
						<td width="75px">{$aVat.vat_currency}</td>
						<td width="75px">{$aVat.value_brutto}</td>
					</tr>
				{/foreach}
					<tr>
						<td width="40px">Ogółem</td>
						<td width="24px">XXX</td>
						<td width="91px">{$aModule.order.value_netto}</td>
						<td width="75px">{$aModule.order.vat}</td>
						<td width="75px">{$aModule.order.value_brutto}</td>
					</tr>
			</table>
		</td>
	</tr>
</table>
<br/>
<hr>
<div style="margin-bottom:20px;">&nbsp;</div>
<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 9pt;" border="0">
	<tr>
		<td width="60%">
			<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 9pt;" border="0">
				<tr>
					<td>
						{if $aModule.order.payment_type == 'bank_14days'}
							{if $aModule.invoice_date_14 != ''}
								<strong>termin do zaplaty {$aModule.invoice_date_14}</strong><br/><br/>
							{/if}
							<strong>Dane sprzedawcy do przelewu:</strong> {$aModule.seller_data.name}<br/>
							{$aModule.seller_data.postal} {$aModule.seller_data.city} {$aModule.seller_data.street} {$aModule.seller_data.number}{if !empty($aModule.seller_data.number2)}/{$aModule.seller_data.number2}{/if}<br/>
							<strong>{$aModule.lang.list_bank}</strong> {$aModule.seller_data.bank_name}<br/>
							<strong>{$aModule.lang.list_number_account}</strong> {$aModule.seller_data.bank_account}<br/>
							<strong>{$aModule.lang.list_transfer}</strong> {if $aModule.order.payment_type=='bank_14days'}{$aModule.order.invoice_id}{else}{$aModule.order.order_number}{/if}
						{/if}
					</td>				
				</tr>
			</table>
		</td>
		<td width="40%">
			<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 9pt;" border="1" align="center">
				<tr>
					<td>	
						<table width="100%" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<td width="10%"></td>
								<td width="80%" style="font-size: 16pt">
									{$aModule.lang.list_pay} <strong>{$aModule.order.value_brutto} zł</strong><br/>
									<span style="font-size: 9pt;">{$aModule.lang.list_dictionary}<strong>{$aModule.order.total_value_brutto_words}</strong></span><br/>
									{if $aModule.order.payment_type == 'postal_fee'}
									<span style="font-size: 9pt;">{$aModule.lang.list_paid} <strong>{$aModule.order.value_brutto} zł</strong></span>
									{elseif $aModule.order.payment_type != 'bank_14days' && $aModule.order.payment_status == '1'}
									<span style="font-size: 9pt;">{$aModule.lang.list_paid} <strong>{$aModule.order.value_brutto} zł</strong></span>
									{/if}
								</td>	
								<td width="10%"></td>
							</tr>
						</table>
					</td>			
				</tr>
			</table>
		</td>
	</tr>
</table>
{if $aModule.order.invoice_remarks != ""}
	<br /><br /><br />
	{$aModule.order.invoice_remarks|nl2br}
{/if}
<br /><br /><br />
<table width="100%" cellspacing="0" cellpadding="0" style="font-size: 9pt;" border="0">
	<tr>
		<td width="25%" align="center">
			<br /><br />. . . . . . . . . . . . .  . . . . . . . . . . .<br />
			<span style="text-align: center; font-size: 7pt;">{$aModule.lang.list_vat_reception}</span>
		</td>
		<td width="25%" align="center">
			<br /><br />. . . . . . . . . . . . . . . . . . . . . . <br />
			<span style="text-align: center; font-size: 7pt;">{$aModule.lang.list_vat_data}</span>
		</td>
		<td width="25%" align="center">
      {if $aModule.orders_user_workflow.name != '' OR $aModule.orders_user_workflow.surname != ''}
        {$aModule.orders_user_workflow.name} {$aModule.orders_user_workflow.surname} <br />
      {/if}
{*			<span style="text-align: center; font-size: 8pt;">{$smarty.session.user.author}</span><br />*}
			<span style="text-align: center;">. . . . . . . . . . . . . . . . . . . . . . </span><br />
			<span style="text-align: center; font-size: 7pt;">{$aModule.lang.list_vat_acting}</span>
		</td>
		<td width="25%" align="center">
      <br /><br />
			. . . . . . . . . . . . . . . . . . . . . . . . .<br />
			<span style="text-align: center; font-size: 7pt;">{$aModule.lang.person_pack}</span>
		</td>
	</tr>
</table>