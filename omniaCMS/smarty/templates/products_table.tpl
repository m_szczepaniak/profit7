<table cellspacing="0" cellpadding="0" border="1" width="510px" align="center">
	<tr>
		<th width="15px" align="left">{$aModule.lang.lp}</th>
		<th width="166px" align="left">{$aModule.lang.name}</th>
		<th width="63px">{$aModule.lang.author}</th>
		<th width="75px">{$aModule.lang.isbn}</th>
		<th width="21px">{$aModule.lang.quantity}</th>
		<th width="45px">{$aModule.lang.price_brutto}</th>
		<th width="35px">{$aModule.lang.discount}</th>
		<th width="45px">{$aModule.lang.price_netto}</th>
		<th width="30px">{$aModule.lang.vat}</th>
		<th width="45px">{$aModule.lang.val_brutto}</th>
	</tr>
	{foreach from=$aModule.products.items key=iId item=aItem name=types}
		<tr {if strlen($aItem.isbn) != 13}style="background-color: grey"{/if}>
		<td width="15px" align="left">{$aItem.lp}</td>
			<td width="166px" align="left">{$aItem.name}</td>
			<td width="63px" align="left">&nbsp;{$aItem.authors}</td>
			<td width="75px">{$aItem.isbn}</td>
			<td width="21px">{$aItem.quantity}</td>
			<td width="45px">{$aItem.price_brutto}</td>
			<td width="35px">{$aItem.discount}</td>
			<td width="45px">{$aItem.price_netto}</td>
			<td width="30px">{$aItem.vat}%</td>
			<td width="45px">{$aItem.val_brutto}</td>
		</tr>
	{/foreach}
	<tr>
		<th width="15px" align="left">&nbsp;</th>
		<th width="166px" align="left">&nbsp;</th>
		<th width="63px">&nbsp;</th>
		<th width="75px">&nbsp;PODSUMOWANIE:</th>
		<th width="21px">{$aModule.products.total_quantity}</th>
		<th width="45px">&nbsp;</th>
		<th width="35px">&nbsp;</th>
		<td width="45px">{$aModule.products.total_price_netto}</td>
		<th width="30px">&nbsp;</th>
		<th width="45px">{$aModule.products.total_val_brutto}</th>
	</tr>
</table>

