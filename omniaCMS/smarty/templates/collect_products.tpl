<script type="text/javascript">
  aBooks = {$sProductsEncoded};
  aBooksToSend = [];
  {literal}
// implement JSON.stringify serialization
    JSON.stringify = JSON.stringify || function (obj) {
      var t = typeof (obj);
      if (t != "object" || obj === null) {
        // simple data type
        if (t == "string")
          obj = '"' + obj + '"';
        return String(obj);
      }
      else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
          v = obj[n];
          t = typeof (v);
          if (t == "string")
            v = '"' + v + '"';
          else if (t == "object" && v !== null)
            v = JSON.stringify(v);
          json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
      }
    };
    

// pozostało do zeskanowania
    $(function () {
      aBookToScan = ({});
      scannedBooks = ({});
    
      if(typeof(Storage) !== "undefined") {
        var iListId = $("#orders_items_lists_id").val();
        
        tmpData = localStorage.getItem("aBooksToSend[" + iListId + "]");
        var is_local_storage = false;
        
        if (tmpData !== null) {
          aBooksToSend = JSON.parse(tmpData);
          is_local_storage = true;
        }
        
        tmpDataa = localStorage.getItem("aBooks[" + iListId + "]");
        if (tmpDataa !== null) {
          aBooks = JSON.parse(tmpDataa);
          is_local_storage = true;
        }
        
        tmpDataa = localStorage.getItem("scannedBooks[" + iListId + "]");
        if (tmpDataa !== null) {
          scannedBooks = JSON.parse(tmpDataa);
          is_local_storage = true;
        }
        
  
        if (is_local_storage === false) {
          // pobieramy ajax
          var aaa = ({
            'method': 'get',
            'orders_items_lists_id': $("#orders_items_lists_id").val()
          });
          $.ajax({
              data: aaa,
              dataType: "json",
              type: "POST",
              cache: false,
              async: false,
              url: "ajax/completationListStatus.php"
          })
          .done(function(data) {
            if (data !== null) {
              if (data['aBooks'] !== undefined && data['aBooksToSend'] !== undefined && data['scannedBooks'] !== undefined) {
                aBooks = data['aBooks'];
                aBooksToSend = data['aBooksToSend'];
                scannedBooks = data['scannedBooks'];
              }
            }
          });
        }
      }
      
      quantityAllBooks = 0;
      for (var i = 0; i < aBooks.length; i++)
      {
        if (aBooks[i] !== null && parseInt(aBooks[i].quantity) > 0) {
          quantityAllBooks += parseInt(aBooks[i].quantity);
          quantityAllBooks -= parseInt(aBooks[i].currquantity);
        }
      }
      $("#left_order_items").html(quantityAllBooks);
    });


// przekazanie danych
    $("#ordered_itm").submit( function(){
      var serializedBooksScanned = JSON.stringify(aBooksToSend);
      var serializedBooksNotScanned = JSON.stringify(aBooks);

      if (serializedBooksScanned !== undefined) {
        $("#books").val(serializedBooksScanned);
      }
      if (serializedBooksNotScanned !== undefined) {
        $("#not_scanned_books").val(serializedBooksNotScanned);
      }
      
      if(typeof(Storage) !== "undefined") {
        localStorage.clear();
        /*
        var iListId = $("#orders_items_lists_id").val();
        localStorage.setItem("aBooksToSend[" + iListId + "]", "");
        localStorage.setItem("aBooks[" + iListId + "]", "");
        */
      }
    });
    
function addChar(char) {
  if (char == '↵') {
    $("#search_isbn").focus();
    var e = $.Event('keydown');
    e.which = 13;
    $("#search_isbn").trigger(e);
  } else {
    $("#search_isbn").val($("#search_isbn").val() + char);
  }
}
  {/literal}
</script>

      <a href="javascript:void(0);" onclick="{literal}$('#keyboard').toggle();{/literal}" style="float: left;">Pokaż/Ukryj klawiaturę / ПОКАЗАТЬ/СКРЫТЬ КЛАВИАТУРУ</a>
      <div class="clear"></div>
      <table id="keyboard" style="display: none;">
        <tr>
        {foreach from=$aChars key=sName item=aType}
          <td>
          {if $sName == "chars"}
            
                <table>
                {foreach from=$aType item=aChars}
                  <tr>
                    {foreach from=$aChars item=sChar}
                      <td><a href="javascript:void(0);" onclick="addChar('{$sChar}');">{$sChar}</a></td>
                    {/foreach}
                  </tr>
                {/foreach}
                </table>
          {elseif $sName == "int"}
                <table>
                {foreach from=$aType item=aInts}
                  <tr>
                    {foreach from=$aInts item=iInt}
                      <td><a href="javascript:void(0);" onclick="addChar('{$iInt}');">{$iInt}</a></td>
                    {/foreach}
                  </tr>
                {/foreach}
                </table>
          {/if}
          </td>
        {/foreach}
        </tr>
      </table>
<input type="hidden" name="books" id="books" />
<input type="hidden" name="not_scanned_books" id="not_scanned_books" />
<div id="collect_products">
  <div class="clear"></div>
  <div class="cont_main_shelf_number location-style">
    <span id="main_shelf_number" style="font-size: 100px; color: #51a351;"></span>
    <input type="hidden" name="pack_number_area" id="pack_number_area"/>
  </div>
  <div style="float: right;">
    Pozostało do zeskanowania / ОСТАЛОСЬ КНИГ ДО РЕАЛИЗАЦИИ: <span id="left_order_items"></span><br /><br />
  </div>
  {if $iChangeStatus == '1'}
    <input type="hidden" name="is_unpacking" id="is_unpacking" value="1" />
    <input type="button" name="save" value="Zmień loka." style="margin-top: 40px; background-color: #51a351; font-size: 32px; text-align: center; color: #000;" onclick="changeLocation();" id="save">
  {/if}
  <div class="clear"></div>
  <div id="number_cont">
    <div class="number_cont" style="width: 100%">
      <img id="main_image" />
        <div style="float: right;margin-right: 25%;">
          <div id="main_quantity_current_item"></div>
          <div class="main_profit_g_act_stock" style="display: none;"> (<span id="main_profit_g_act_stock"></span>)</div>
          <div id="get_weight">Zważ produkt !</div>
        </div>
      <br /><br />

      <span id="main_name" style="font-size: 44px; "></span><br />
      <span id="main_isbn" style="font-size: 34px; "></span><br />
      <span id="main_desc"></span>
    </div>
    <div class="clear"></div>
  </div>
  <div class="list_scanned_items location-style">
    {section name=foo loop=3 step=+1}
      <div class="list_scanned_item">
        <img id="image_{$smarty.section.foo.index}" class="sc_image"/><br />
        <span id="shelf_number_{$smarty.section.foo.index}" class="sc_shelf"></span><br />
        <span id="name_{$smarty.section.foo.index}" class="sc_name" ></span><br />
        <span id="isbn_{$smarty.section.foo.index}" style="font-size: 14px; color: grey" ></span><br />
        <span id="desc_{$smarty.section.foo.index}" class="sc_desc" ></span>
      </div>
    {/section}
    <div class="clear"></div>
    <div id="scanned" style="margin-top: 15px; display: none; text-align: center;">
      <hr />
      <div class="clear"></div>
      <div style="margin: 15px 0; font-size: 22px;">Braki:</div>
      <div class="clear"></div>
      <div id="not_scanned_sc">

        {*
        <div class="list_scanned_item">
        <img class="sc_image" /><br />
        <span class="sc_shelf"></span><br />
        <span class="sc_quantity"></span><br />
        <span class="sc_name"></span><br />
        <span class="sc_desc"></span>
        </div>
        *}
      </div>
    </div>
  </div>

  <br />
  <div class="clear"></div>
  <div class="clear"></div>
</div>