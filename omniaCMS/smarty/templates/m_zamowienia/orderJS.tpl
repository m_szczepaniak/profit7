{literal}
  <script type="text/javascript" src="js/order_detail_items.js"></script>
  <script type="text/javascript">
    //<![CDATA[
    function showSourceRadio(status, id) {
      //schowanie radio dla zewnetrznej/wewnetrznej
      document.getElementById("sourceRadioIn" + id).style.display = "none";
      document.getElementById("sourceRadioEx" + id).style.display = "none";
      //zaleznie od wybranje opcji pokazujemy radio
      switch (status) {
        case "1":
          document.getElementById("sourceRadioEx" + id).style.display = "block";
          break;
        case "2":
          document.getElementById("sourceRadioEx" + id).style.display = "block";
          break;
        case "3":
          document.getElementById("sourceRadioIn" + id).style.display = "block";
          break;
        case "4":
          document.getElementById("sourceRadioIn" + id).style.display = "block";
          break;
      }
    }

    $(document).ready(function () {
  {/literal}
      var shipment_at_point = {$aParametrs.aShipmentAtPoint};
      var shipment_to_address = {$aParametrs.aShipmentToAddress};

  {literal}
      var isReciptAtPoint;

      if ($.inArray($("#transport_mean option:selected").val(), shipment_at_point) !== -1) {
        isReciptAtPoint = true;
      } else {
        isReciptAtPoint = false;
      }

      $('#transport_option_id, select[name="point_of_receipt"]').parent().parent().ready(function () {
        if ($("#transport_option_id").html() == "") {
          $("#transport_option_id").parent().parent().hide();
          $('select[name="point_of_receipt"]').parent().parent().hide();
        }
      });
      if (isReciptAtPoint === false) {
        $('select[name="point_of_receipt"]').parent().parent().ready(function () {
          $('select[name="point_of_receipt"]').parent().parent().hide();
        });
      }

      // jeśli zmiana metody transportu i wybrano paczkomaty, pocztę, ruch lub orlen, pobieramy ajaxem odpowiednią listę punktów odbioru 
      $("#transport_mean").change(function () {
        if ($.inArray($("#transport_mean option:selected").val(), shipment_at_point) !== -1) {
          isReciptAtPoint = true;
        } else {
          isReciptAtPoint = false;
        }

        var isReciptToAddress;
        if ($.inArray($("#transport_mean option:selected").val(), shipment_to_address) !== -1) {
          isReciptToAddress = true;
        } else {
          isReciptToAddress = false;
        }
        
        var transport_id = ($("#transport_mean").val());
        if (isReciptAtPoint === true) {

          $.ajax({
            type: "POST",
            url: "ajax/GetPointOfReceiptSelect.php",
            cache: false,
            data: {
              "transport": transport_id,
              "selected": "{$aParametrs.aParam.selected}"
            },
            success: function (resp) {
              $(".point_of_receipt").html(resp);
            }
          });
        }


        var elSelPay = document.getElementById("payment_method");
        $.get("ajax/GetPayments.php", {transport: transport_id},
        function (data) {
          var brokenstring = data.split(";");
          elSelPay.length = 0;
          elSelPay.options[elSelPay.length] = new Option("-- wybierz --", "");
          for (x in brokenstring) {
            if (brokenstring[x]) {
              var item = brokenstring[x].split("|");
              elSelPay.options[elSelPay.length] = new Option(item[0], item[1]);
            }
          }
        });
        
        if (isReciptAtPoint === false) {
          if (isReciptToAddress === true) {
            alert("Pamiętaj o uzupełnieniu danych do dostawy po zmianie metody transportu. Upewnij się, czy podano numer telefonu komórkowego");
          }
          $('select[name="point_of_receipt"]').parent().parent().ready(function () {
            $('select[name="point_of_receipt"]').parent().parent().hide();
          });
        } else {
          $('select[name="point_of_receipt"]').parent().parent().ready(function () {
            $('select[name="point_of_receipt"]').parent().parent().show();
          });
        }

        if ($("#transport_option_id").size()) {
          var elSelTransOpt = document.getElementById("transport_option_id");
          $.get("ajax/GetTransportOptions.php", {transport: transport_id},
          function (data) {
            if (data == "") {
              $(elSelTransOpt).parent().parent().hide();
              $(elSelTransOpt).removeAttr("selected");
              $(elSelTransOpt).html("");

              $('select[name="point_of_receipt"]').parent().parent().hide();
            } else {
              $('select[name="point_of_receipt"]').parent().parent().show();
              $(elSelTransOpt).parent().parent().show();
              var brokenstring = data.split(";");
              elSelTransOpt.length = 0;
              elSelTransOpt.options[elSelTransOpt.length] = new Option("-- wybierz --", "");
              for (x in brokenstring) {
                if (brokenstring[x]) {
                  var item = brokenstring[x].split("|");
                  elSelTransOpt.options[elSelTransOpt.length] = new Option(item[0], item[1]);
                }
              }
            }
          });
        }

      });

      $("#order_items").submit(function () {
        var sErr = "";
        $(".heditable_input, .editable_input").each(function () {
          regex = /^editable\[([A-Za-z0-9_]+)\]\[([A-Za-z0-9_]+)\]$/;
          matches = regex.exec(this.name);
          if (matches[2] == "quantity") {
            $(this).val($(this).val().replace(/&nbsp;/g, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
            if ($(this).val() != "" && (!$(this).val().match(/^\d+$/) || $(this).val() <= 0)) {
              sErr += "\t- Sztuk\n";
            }
          }
          else if (matches[2] == "discount") {
            $(this).val($(this).val().replace(/&nbsp;/, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
            if ($(this).val() != "" && (!$(this).val().match(/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) || $(this).val() < 0 || $(this).val() >= 100)) {
              sErr += "\t- Rabat\n";
            }
          }
          else if (matches[2] == "transport_value_brutto") {
            $(this).val($(this).val().replace(/&nbsp;/g, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
            if ($(this).val() != "" && (!$(this).val().match(/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) || $(this).val() < 0)) {
              sErr += "\t- Koszt transportu\n";
            }
          }
          else if (matches[2] == "weight") {
            $(this).val($(this).val().replace(/&nbsp;/g, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
            if ($(this).val() != "" && (!$(this).val().match(/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) || $(this).val() < 0)) {
              sErr += "\t- Waga\n";
            }
          }

        });
        /*
         if ( $("#transport_option_id").size()) {
         if ($("#transport_option_id").html() != "" && $("#transport_option_id option:selected").val() == "") {
         sErr += "\t- Typ przesyłki\n";
         }
         }
         */

        if (sErr != "") {
  {/literal}
          alert("{$aParametrs.error_prefix}\n" + sErr + "{$aParametrs.error_postfix}");
  {literal}
          return false;
        }
      });
    });

    $(document).ready(function () {
      $("#show_navitree").click(function () {
        window.open("ajax/NaviPopup.php", '', 'menubar=0,location=0,scrollbars=1,toolbar=0,width=500,height=300');
      });
      $("a.stockTooltip").cluetip({width: "160px", local: true, showTitle: false});
    });
    //]]>
  </script>
{/literal}