{strip}

<table width="100%" cellspacing="0" cellpadding="0" style='padding:10px 4px 10px 4px'>
    {foreach from=$aOrdersWaiting item=rows key=date name=waiting_loop }
        {if $smarty.foreach.waiting_loop.first}
            <tr>
                {foreach from=$aSourcesWaiting key=source_id item=source name=sources_list_header}
                    {if $smarty.foreach.sources_list_header.first}
                        <th style="border: 1px solid #000;"> &nbsp; Data</th>
                    {/if}
                    <th style="border: 1px solid #000;">
                        {$source}
                    </th>
                {/foreach}
            </tr>
        {/if}
        <tr>
            <td style="border: 1px solid #000;">{$date}</td>
            {foreach from=$aSourcesWaiting key=source_id item=source}
                {assign var=rows_cur_source value=`$rows[$source_id]`}
                    {if !empty($rows_cur_source) }
                        <td style="border: 1px solid #000; padding: 5px;">
                        {foreach from=$rows_cur_source item=row name=sources_waiting_list}
                            {$row.number}&nbsp;-&nbsp;{$row.fv_nr} 
                            {if !$smarty.foreach.sources_waiting_list.last}<br />{/if}
                        {/foreach}
                        </td>
                    {else}
                        <td style="border: 1px solid #000; padding: 5px;"></td>
                    {/if}
            {/foreach}
        </tr>
    {/foreach}
</table>
{/strip}