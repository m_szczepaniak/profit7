<table cellspacing="0" cellpadding="1" border="1" width="510px" align="center">
	<tr>
		<th rowspan="2" width="22px">{$aModule.lang.lp}</th>
		<th rowspan="2" width="80px">{$aModule.lang.invoice_nr}</th>
		<th rowspan="2" width="54px">{$aModule.lang.order_date}</th>
		<th colspan="2" width="266px" align="left">{$aModule.lang.recipient}</th>
		<th rowspan="2" width="54px">{$aModule.lang.value_brutto}</th>
		<th rowspan="2" width="54px">{$aModule.lang.value_netto}</th>
		<th colspan="2" width="108px">{$aModule.lang.vat_22}</th>
		<th width="54px">{$aModule.lang.vat_0}</th>
		<th rowspan="2" width="54px">{$aModule.lang.order_vat}</th>
	</tr>
	<tr>
		<td width="196px" align="left">{$aModule.lang.name}</td>
		<td width="70px">{$aModule.lang.nip}</td>
		<td width="54px">{$aModule.lang.vat_22_value_netto}</td>
		<td width="54px">{$aModule.lang.vat_22_currency}</td>
		<td width="54px">{$aModule.lang.vat_0_value_netto}</td>
	</tr>
	{foreach from=$aModule.table.items key=iId item=aItem name=types}
		<tr>
			<td width="22px" align="left">{$aItem.lp}</td>
			<td width="80px" align="left">{$aItem.invoice_number}</td>
			<td width="54px" align="left">{$aItem.invoice_date}</td>
			<td width="196px" align="left">{$aItem.name}</td>
			<td width="70px" align="left">{$aItem.nip}</td>
			<td width="54px" align="right">{$aItem.value_brutto}</td>
			<td width="54px" align="right">{$aItem.value_netto}</td>
			<td width="54px" align="right">{$aItem.vat[22].value_netto}</td>
			<td width="54px" align="right">{$aItem.vat[22].vat_currency}</td>
			<td width="54px" align="right">{$aItem.vat[0].value_netto}</td>
			<td width="54px" align="right">{$aItem.order_vat}</td>
		</tr>
	{/foreach}
	<tr>
			<td width="422px" colspan="5">{$aModule.lang.summary}</td>
			<td width="54px" align="right">{$aModule.table.total_brutto}</td>
			<td width="54px" align="right">{$aModule.table.total_netto}</td>
			<td width="54px" align="right">{$aModule.table.vat_22_value}</td>
			<td width="54px" align="right">{$aModule.table.vat_22_currency}</td>
			<td width="54px" align="right">{$aModule.table.vat_0_value}</td>
			<td width="54px" align="right">{$aModule.table.total_vat}</td>
		</tr>
</table>

