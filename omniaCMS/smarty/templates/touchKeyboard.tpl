<script type="text/javascript">
  {literal}
function addChar(char) {
  if (char == '↵') {
    var e = $.Event('keydown');
    e.which = 13;
    $('form').submit();
  } else {
    $('input:focus').val($('input:focus').val() + char);
  }
}
$(function() {
    $('input').bind('focusout', function(e) {
        $(this).focus();
    });
});
  {/literal}
</script>
{*<a href="javascript:void(0);" onclick="{literal}$('#keyboard').toggle();{/literal}" style="float: left;">Pokaż/Ukryj klawiaturę</a>*}
<div class="clear"></div>
<table id="keyboard">
  <tr>
  {foreach from=$aChars key=sName item=aType}
    <td>
    {if $sName == "chars"}

          <table>
          {foreach from=$aType item=aChars}
            <tr>
              {foreach from=$aChars item=sChar}
                <td><a href="javascript:void(0);" onclick="addChar('{$sChar}');">{$sChar}</a></td>
              {/foreach}
            </tr>
          {/foreach}
          </table>
    {elseif $sName == "int"}
          <table>
          {foreach from=$aType item=aInts}
            <tr>
              {foreach from=$aInts item=iInt}
                <td><a href="javascript:void(0);" onclick="addChar('{$iInt}');">{$iInt}</a></td>
              {/foreach}
            </tr>
          {/foreach}
          </table>
    {/if}
    </td>
  {/foreach}
  </tr>
</table>