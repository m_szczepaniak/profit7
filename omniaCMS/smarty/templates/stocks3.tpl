{foreach from=$aStocks item=aStock key=id}
    <div class="stockInfo" id="stockTooltip_{$id}">
        <table cellpadding="1" cellspacing="0" border="1" style="min-width: 400px;">
            <tbody style="background-color: #fff;">
                <tr>
                    <td class="it_stock {if !empty($aStock.profit_e_status)}stock_available{/if}" style="font-weight: bold; background-color: #cc0000; color: #fff;">Eko</td>
                    <td class="it_stock center_text {if !empty($aStock.profit_e_status)}stock_available{/if}">{$aStock.profit_e_status} {*if empty($aStock.profit_e_location)}&nbsp;{else}{$aStock.profit_e_location}{/if*}</td>
                    <td class="it_stock center_text {if $aStock.profit_e_wholesale_price > 0}stock_available{/if}">{if $aStock.profit_e_wholesale_price === NULL}---{else}{$aStock.profit_e_wholesale_price|number_format:2:',':'.'}{/if}</td>
                    <td class="it_stock center_text {if $aStock.profit_e_wholesale_price > 0}stock_available{/if}">{if $aStock.profit_e_our_discount != ''}{$aStock.profit_e_our_discount}%{/if}</td>


                    <td class="it_stock it_out {if $aStock.abe_status == '1'}stock_available{/if}" style="font-weight: bold; border-left: 1px solid #cfcfcf; background-color: #000; color: #fff;">ABE</td>

                    <td class="it_stock it_out center_text {if $aStock.abe_status == '1'}stock_available{/if}">{if $aStock.abe_status == 0}0{else}JEST{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.abe_status == '1'}stock_available{/if}">{if $aStock.abe_wholesale_price === NULL}---{else}{$aStock.abe_wholesale_price|number_format:2:',':'.'}{/if}</td> 
                    <td class="it_stock it_out center_text {if $aStock.abe_status == '1'}stock_available{/if}">{if $aStock.abe_our_discount != ''}{$aStock.abe_our_discount}%{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.abe_status == '1'}stock_available{/if}">{if $aStock.abe_shipment_date != '' && $aStock.abe_shipment_date != '0000-00-00'}{$aStock.abe_shipment_date}{else}-{/if}</td>
                </tr>
                <tr>
                    <td class="it_stock {if !empty($aStock.profit_j_status)}stock_available{/if}" style="font-weight: bold; background-color: #cc0000; color: #fff;">Jęz</td>
                    <td class="it_stock center_text {if !empty($aStock.profit_j_status)}stock_available{/if}">{$aStock.profit_j_status} {*if empty($aStock.profit_j_location)}&nbsp;{else}{$aStock.profit_j_location}{/if*}</td>
                    <td class="it_stock center_text {if $aStock.profit_j_wholesale_price > 0}stock_available{/if}">{if $aStock.profit_j_wholesale_price === NULL}---{else}{$aStock.profit_j_wholesale_price|number_format:2:',':'.'}{/if}</td>
                    <td class="it_stock center_text {if $aStock.profit_j_wholesale_price > 0}stock_available{/if}">{if $aStock.profit_j_our_discount != ''}{$aStock.profit_j_our_discount}%{/if}</td>


                    <td class="it_stock it_out {if $aStock.azymut_status == '1'}stock_available{/if}" style="font-weight: bold; border-left: 1px solid #cfcfcf; background-color: #000; color: #fff;">Azy</td>
                    <td class="it_stock it_out center_text {if $aStock.azymut_status == '1'}stock_available{/if}">{if $aStock.azymut_status == '0'}0{else}{$aStock.azymut_stock}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.azymut_status == '1'}stock_available{/if}">{if $aStock.azymut_wholesale_price === NULL}---{else}{$aStock.azymut_wholesale_price|number_format:2:',':'.'}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.azymut_status == '1'}stock_available{/if}">{if $aStock.azymut_our_discount != ''}{$aStock.azymut_our_discount}%{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.azymut_status == '1'}stock_available{/if}">{if $aStock.azymut_shipment_date != '' && $aStock.azymut_shipment_date != '0000-00-00'}{$aStock.azymut_shipment_date}{else}-{/if}</td>
                </tr>
                <tr>
                    <td class="it_stock {if !empty($aStock.profit_g_status)}stock_available{/if}" style="font-weight: bold; background-color: #cc0000; color: #fff;">Gda</td>
                    <td class="it_stock center_text {if !empty($aStock.profit_g_status)}stock_available{/if}">S&nbsp;{$aStock.profit_g_status}  <span style="font-weight: normal; color: red;"><br />R&nbsp;{$aStock.profit_g_reservations}</span> <span style="font-weight: normal;"><br />W&nbsp;{$aStock.profit_g_act_stock}</span> {*if empty($aStock.profit_g_location)}&nbsp;{else}{$aStock.profit_g_location}{/if*}</td>
                    <td class="it_stock center_text {if $aStock.profit_g_wholesale_price > 0}stock_available{/if}">{if $aStock.profit_g_wholesale_price === NULL}---{else}{$aStock.profit_g_wholesale_price|number_format:2:',':'.'}{/if}</td>
                    <td class="it_stock center_text {if $aStock.profit_g_wholesale_price > 0}stock_available{/if}">{if $aStock.profit_g_our_discount != ''}{$aStock.profit_g_our_discount}%{/if}</td>


                    {*<td class="it_stock {if !empty($aStock.profit_m_status)}stock_available{/if}" style="font-weight: bold; background-color: #cc0000; color: #fff;">Men</td>
                    <td class="it_stock center_text {if !empty($aStock.profit_m_status)}stock_available{/if}">{$aStock.profit_m_status} {if empty($aStock.profit_m_location)}&nbsp;{else}{$aStock.profit_m_location}{/if}</td>*}
                    <td class="it_stock it_out {if $aStock.ateneum_status == '1'}stock_available{/if}" style="font-weight: bold; border-left: 1px solid #cfcfcf; background-color: #000; color: #fff;">Ate</td>
                    <td class="it_stock it_out center_text {if $aStock.ateneum_status == '1'}stock_available{/if}">{if $aStock.ateneum_status == '0'}0{else}{$aStock.ateneum_stock}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.ateneum_status == '1'}stock_available{/if}">{if $aStock.ateneum_wholesale_price === NULL}---{else}{$aStock.ateneum_wholesale_price|number_format:2:',':'.'}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.ateneum_status == '1'}stock_available{/if}">{if $aStock.ateneum_our_discount != ''}{$aStock.ateneum_our_discount}%{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.ateneum_status == '1'}stock_available{/if}">{if $aStock.ateneum_shipment_date != '' && $aStock.ateneum_shipment_date != '0000-00-00'}{$aStock.ateneum_shipment_date}{else}-{/if}</td>
                </tr>
                <tr>
                    <td class="it_stock"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock it_out {if $aStock.helion_status == '1'}stock_available{/if}" style="font-weight: bold; border-left: 1px solid #cfcfcf; background-color: #000; color: #fff;">Hel</td>
                    <td class="it_stock it_out center_text {if $aStock.helion_status == '1'}stock_available{/if}">{if $aStock.helion_status == '0'}0{else}{$aStock.helion_stock}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.helion_status == '1'}stock_available{/if}">{if $aStock.helion_wholesale_price === NULL}---{else}{$aStock.helion_wholesale_price|number_format:2:',':'.'}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.helion_status == '1'}stock_available{/if}">{if $aStock.helion_our_discount != ''}{$aStock.helion_our_discount}%{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.helion_status == '1'}stock_available{/if}">{if $aStock.helion_shipment_date != '' && $aStock.helion_shipment_date != '0000-00-00'}{$aStock.helion_shipment_date}{else}-{/if}</td>
                </tr>
                <tr>
                    {*
                    <td class="it_stock {if !empty($aStock.profit_x_status)}stock_available{/if}" style="font-weight: bold; background-color: #cc0000; color: #fff;">Rez</td>
                    <td class="it_stock center_text {if !empty($aStock.profit_x_status)}stock_available{/if}">{$aStock.profit_x_status} {if empty($aStock.profit_x_location)}&nbsp;{else}{$aStock.profit_x_location}{/if}</td>*}
                    <td class="it_stock"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock it_out {if $aStock.dictum_status == '1'}stock_available{/if}" style="font-weight: bold; border-left: 1px solid #cfcfcf; background-color: #000; color: #fff;">Dic</td>
                    <td class="it_stock it_out center_text {if $aStock.dictum_status == '1'}stock_available{/if}">{if $aStock.dictum_status == '0'}0{else}{$aStock.dictum_stock}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.dictum_status == '1'}stock_available{/if}">{if $aStock.dictum_wholesale_price === NULL}---{else}{$aStock.dictum_wholesale_price|number_format:2:',':'.'}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.dictum_status == '1'}stock_available{/if}">{if $aStock.dictum_our_discount != ''}{$aStock.dictum_our_discount}%{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.dictum_status == '1'}stock_available{/if}">{if $aStock.dictum_shipment_date != '' && $aStock.dictum_shipment_date != '0000-00-00'}{$aStock.dictum_shipment_date}{else}-{/if}</td>
                </tr>
                <tr>
                    <td class="it_stock"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock it_out {if $aStock.siodemka_status == '1'}stock_available{/if}" style="font-weight: bold; border-left: 1px solid #cfcfcf; background-color: #000; color: #fff;">Sió</td>
                    <td class="it_stock it_out center_text {if $aStock.siodemka_status == '1'}stock_available{/if}">{if $aStock.siodemka_status == '0'}0{else}{$aStock.siodemka_stock}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.siodemka_status == '1'}stock_available{/if}">{if $aStock.siodemka_wholesale_price === NULL}---{else}{$aStock.siodemka_wholesale_price|number_format:2:',':'.'}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.siodemka_status == '1'}stock_available{/if}">{if $aStock.siodemka_our_discount != ''}{$aStock.siodemka_our_discount}%{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.siodemka_status == '1'}stock_available{/if}">{if $aStock.siodemka_shipment_date != '' && $aStock.siodemka_shipment_date != '0000-00-00'}{$aStock.siodemka_shipment_date}{else}-{/if}</td>
                </tr>
                <tr>
                    <td class="it_stock"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock center_text"></td>
                    <td class="it_stock it_out {if $aStock.olesiejuk_status == '1'}stock_available{/if}" style="font-weight: bold; border-left: 1px solid #cfcfcf; background-color: #000; color: #fff;">Ole</td>
                    <td class="it_stock it_out center_text {if $aStock.olesiejuk_status == '1'}stock_available{/if}">{if $aStock.olesiejuk_status == '0'}0{else}{$aStock.olesiejuk_stock}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.olesiejuk_status == '1'}stock_available{/if}">{if $aStock.olesiejuk_wholesale_price === NULL}---{else}{$aStock.olesiejuk_wholesale_price|number_format:2:',':'.'}{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.olesiejuk_status == '1'}stock_available{/if}">{if $aStock.olesiejuk_our_discount != ''}{$aStock.olesiejuk_our_discount}%{/if}</td>
                    <td class="it_stock it_out center_text {if $aStock.olesiejuk_status == '1'}stock_available{/if}">{if $aStock.olesiejuk_shipment_date != '' && $aStock.olesiejuk_shipment_date != '0000-00-00'}{$aStock.olesiejuk_shipment_date}{else}-{/if}</td>
                </tr>
            </tbody>
        </table>
    </div>
{/foreach}