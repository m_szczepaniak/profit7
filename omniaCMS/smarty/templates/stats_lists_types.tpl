<div style="float: left;">
  <h4>Stock wysyłka</h4>
  <table class="statsTable" style="float: left; border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th>Single</td>
    <th>Ilość</td>
  </tr>
  {foreach from=$aData.to_collecting_stock_single key=date item=item}
    <tr>
      <td>{$item.transport|substr:0:6}</td>
      <td>{$item.count}</td>
    </tr>
  {/foreach}
  </table>

  <table class="statsTable" style=" float: left; border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th>Pełne</td>
    <th>Ilość</td>
  </tr>
  {foreach from=$aData.to_collecting_stock_full key=date item=item}
    <tr>
      <td>{$item.transport|substr:0:6}</td>
      <td>{$item.count}</td>
    </tr>
  {/foreach}
  </table>

  <table class="statsTable" style=" float: left; border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th>Część</td>
    <th>Ilość</td>
  </tr>
  {foreach from=$aData.to_collecting_stock_part_linked key=date item=item}
    <tr>
      <td>{$item.transport|substr:0:6}</td>
      <td>{$item.count}</td>
    </tr>
  {/foreach}
  </table>
</div>

<div style="float: left;">
  <h4>Tramwaj wysyłka</h4>
  <table class="statsTable" style="float: left; border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th>Single</td>
    <th>Ilość</td>
  </tr>
  {foreach from=$aData.to_collecting_train_single key=date item=item}
    <tr>
      <td>{$item.transport|substr:0:6}</td>
      <td>{$item.count}</td>
    </tr>
  {/foreach}
  </table>


  <table class="statsTable" style="float: left; border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th>Pełne</td>
    <th>Ilość</td>
  </tr>
  {foreach from=$aData.to_collecting_train_default key=date item=item}
    <tr>
      <td>{$item.transport|substr:0:6}</td>
      <td>{$item.count}</td>
    </tr>
  {/foreach}
  </table>


  <table class="statsTable" style="float: left; border-top: 1px solid #D8D6B7; border-right: 1px solid #D8D6B7;">
  <tr>
    <th>Łączone</td>
    <th>Ilość</td>
  </tr>
  {foreach from=$aData.to_collecting_train_part_linked key=date item=item}
    <tr>
      <td>{$item.transport|substr:0:6}</td>
      <td>{$item.count}</td>
    </tr>
  {/foreach}
  </table>
</div>

<div class="clear"></div>