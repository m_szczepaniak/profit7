Raport zbiorczy VAT<br/>
za okres {$aRaport.date_start} - {$aRaport.date_end}<br/>


{$aRaport.seller_data.name}<br/>
{$aRaport.seller_data.street} {$aRaport.seller_data.number}{if !empty($aRaport.seller_data.number2)}/{$aRaport.seller_data.number2}{/if}<br/>
{$aRaport.seller_data.postal} {$aRaport.seller_data.city}<br/>
{$aRaport.seller_data.bank_name}  {$aRaport.seller_data.bank_account}<br/>


<h3>Sprzedaż pobranie - Profit24.pl</h3>
{if !empty($aRaport.postal_fee.1)}
	{$aRaport.postal_fee.1}
{/if}
<br/>

<h3>Sprzedaż pobranie - ksiegarnia.IT</h3>
{if !empty($aRaport.postal_fee.2)}
	{$aRaport.postal_fee.2}
{/if}
<br/>

<h3>Sprzedaż pobranie - nieprzeczytane.pl</h3>
{if !empty($aRaport.postal_fee.3)}
	{$aRaport.postal_fee.3}
{/if}
<br/>

<h3>Sprzedaż pobranie - Mestro.pl</h3>
{if !empty($aRaport.postal_fee.4)}
	{$aRaport.postal_fee.4}
{/if}
<br/>

<h3>Sprzedaż pobranie - smarkacz.pl</h3>
{if !empty($aRaport.postal_fee.5)}
	{$aRaport.postal_fee.5}
{/if}
<br/>

<h3>Sprzedaż pobranie - naszabiblioteka.pl</h3>
{if !empty($aRaport.postal_fee.8)}
	{$aRaport.postal_fee.8}
{/if}
<br/>

<h3>Sprzedaż Platnoscipl - Profit24.pl</h3>
{if !empty($aRaport.platnoscipl.1)}
{$aRaport.platnoscipl.1}
{/if}
<br/>

<h3>Sprzedaż Platnoscipl - ksiegarnia.IT</h3>
{if !empty($aRaport.platnoscipl.2)}
{$aRaport.platnoscipl.2}
{/if}
<br/>

<h3>Sprzedaż Platnoscipl - nieprzeczytane.pl</h3>
{if !empty($aRaport.platnoscipl.3)}
{$aRaport.platnoscipl.3}
{/if}
<br/>

<h3>Sprzedaż Platnoscipl - Mestro.pl</h3>
{if !empty($aRaport.platnoscipl.4)}
{$aRaport.platnoscipl.4}
{/if}
<br/>

<h3>Sprzedaż Platnoscipl - smarkacz.pl</h3>
{if !empty($aRaport.platnoscipl.5)}
{$aRaport.platnoscipl.5}
{/if}
<br/>

<h3>Sprzedaż Platnoscipl - naszabiblioteka.pl</h3>
{if !empty($aRaport.platnoscipl.8)}
	{$aRaport.platnoscipl.8}
{/if}
<br/>

<h3>Sprzedaż przelew przedpłata - Profit24.pl</h3>
{if !empty($aRaport.bank_transfer.1)}
{$aRaport.bank_transfer.1}
{/if}
<br/>

<h3>Sprzedaż przelew przedpłata - ksiegarnia.IT</h3>
{if !empty($aRaport.bank_transfer.2)}
{$aRaport.bank_transfer.2}
{/if}
<br/>

<h3>Sprzedaż przelew przedpłata - nieprzeczytane.pl</h3>
{if !empty($aRaport.bank_transfer.3)}
{$aRaport.bank_transfer.3}
{/if}
<br/>

<h3>Sprzedaż przelew przedpłata - Mestro.pl</h3>
{if !empty($aRaport.bank_transfer.4)}
{$aRaport.bank_transfer.4}
{/if}
<br/>

<h3>Sprzedaż przelew przedpłata - smarkacz.pl</h3>
{if !empty($aRaport.bank_transfer.5)}
{$aRaport.bank_transfer.5}
{/if}
<br/>

<h3>Sprzedaż przelew przedpłata - naszabiblioteka.pl</h3>
{if !empty($aRaport.bank_transfer.8)}
	{$aRaport.bank_transfer.8}
{/if}
<br/>

<h3>Sprzedaż przelew 14 dni - Profit24.pl</h3>
{if !empty($aRaport.bank_14days.1)}
{$aRaport.bank_14days.1}
{/if}
<br />

<h3>Sprzedaż przelew 14 dni - ksiegarnia.IT</h3>
{if !empty($aRaport.bank_14days.2)}
{$aRaport.bank_14days.2}
{/if}
<br />

<h3>Sprzedaż przelew 14 dni - nieprzeczytane.pl</h3>
{if !empty($aRaport.bank_14days.3)}
{$aRaport.bank_14days.3}
{/if}
<br/>

<h3>Sprzedaż przelew 14 dni - Mestro.pl</h3>
{if !empty($aRaport.bank_14days.4)}
{$aRaport.bank_14days.4}
{/if}
<br/>

<h3>Sprzedaż przelew 14 dni - smarkacz.pl</h3>
{if !empty($aRaport.bank_14days.5)}
{$aRaport.bank_14days.5}
{/if}
<br/>

<h3>Sprzedaż przelew 14 dni - naszabiblioteka.pl</h3>
{if !empty($aRaport.bank_14days.8)}
	{$aRaport.bank_14days.8}
{/if}
<br/>

<h3>Podsumowanie - Profit24.pl</h3>
{if !empty($aRaport.summary.1)}
{$aRaport.summary.1}
{/if}
<br />

<h3>Podsumowanie - ksiegarnia.IT</h3>
{if !empty($aRaport.summary.2)}
{$aRaport.summary.2}
{/if}
<br />

<h3>Podsumowanie - nieprzeczytane.pl</h3>
{if !empty($aRaport.summary.3)}
  {$aRaport.summary.3}
{/if}
<br />

<h3>Podsumowanie - Mestro.pl</h3>
{if !empty($aRaport.summary.4)}
  {$aRaport.summary.4}
{/if}
<br />

<h3>Podsumowanie - smarkacz.pl</h3>
{if !empty($aRaport.summary.5)}
  {$aRaport.summary.5}
{/if}
<br />

<h3>Podsumowanie - naszabiblioteka.pl</h3>
{if !empty($aRaport.summary.8)}
	{$aRaport.summary.8}
{/if}
<br />

<table cellspacing="0" cellpadding="1" border="1" width="510px" align="center">
<tr>
<td colspan="2">Wygenerował w CMS</td>
</tr>
<tr>
<td colspan="2">&nbsp;<br /><br /><br /></td>
</tr>
<tr>
<td>Wprowadził</td>
<td>Sprawdził</td>
</tr>
<tr>
<td>&nbsp;<br /><br /><br /></td>
<td>&nbsp;<br /><br /><br /></td>
</tr>
</table>
