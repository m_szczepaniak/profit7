<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset={$sCharset}">
  <meta http-equiv="Content-Language" content="{$sLanguage}">
  <meta name="Robots" content="NOFOLLOW, NOINDEX">
  <meta name="Pragma" content="no-cache">
  <meta name="Cache-Control" content="no-store, no-cache, must-revalidate">
  <title>{$sTitle}</title>
  <link href="css/styles.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" language="JavaScript" src="js/JSCookMenu.js"></script>
  <link rel="stylesheet" href="css/theme.css" type="text/css">
  <link rel="stylesheet" href="js/css/ui-lightness/jquery-ui-1.9.2.custom.min.css">
  <script type="text/javascript" language="JavaScript" src="js/theme.js" ></script>
  <script type="text/javascript" language="JavaScript" src="js/calendar/GCappearance.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/calendar/GurtCalendar.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/functions.js"></script>
  <script src="js/jquery-1.8.3.min.js" type="text/javascript"></script>
	<!--<script src="js/jquery.checktree.js" type="text/javascript"></script>-->
	<script type="text/javascript" src="js/jquery.treeview.min.js"></script>
	<script type="text/javascript" src="js/jquery.cluetip.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
  <script type="text/javascript" src="js/jquery.calculation.min.js"></script>
  <script src="js/checkboxtree/NEW_jquery.checkboxtree.js" type="text/javascript"></script>
  <script src="js/jquery-scripts.js" type="text/javascript"></script>
  <link href="js/checkboxtree/NEW_jquery.checkboxtree.css" rel="stylesheet" type="text/css">

  <link rel="stylesheet" href="css/calendar.css" type="text/css">
  {if isset($sHeaderJS)}{$sHeaderJS}{/if}
</head>
<body{if isset($sBodyOnload) AND !empty($sBodyOnload)} onload="{$sBodyOnload}"{/if}>
<table style="width:100%; height:100%;" border="0" cellspacing="0" cellpadding="0">
  {if !empty($aUrlReferer)}
  <tr>
    <td style="font-size: 12px; height: 19px; vertical-align: center;">
      Wróć do 
      {foreach from=$aUrlReferer key=sUrlKey item=aUrl}
        
        &laquo; <a href="{$aUrl.url}">{$aUrl.name|urldecode}</a>
       {/foreach}
    </td>
  </tr>
  {/if}
  <tr>
    <td style="text-align: center; vertical-align: top;">
    {$sSessionMessage}
		{$sContent}
    </td>
  </tr>
</table>
</body>
</html>