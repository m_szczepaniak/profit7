
<script type="text/javascript">
  aBooks = {$aData.js};
  aBooksToSend = [];
  {literal}
// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {
    var t = typeof (obj);
    if (t != "object" || obj === null) {
        // simple data type
        if (t == "string") obj = '"'+obj+'"';
        return String(obj);
    }
    else {
        // recurse array or object
        var n, v, json = [], arr = (obj && obj.constructor == Array);
        for (n in obj) {
            v = obj[n]; t = typeof(v);
            if (t == "string") v = '"'+v+'"';
            else if (t == "object" && v !== null) v = JSON.stringify(v);
            json.push((arr ? "" : '"' + n + '":') + String(v));
        }
        return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
    }
};

    $(function(){
      
      
      
      
      var iListId = $("#orders_items_lists_id").val();
      leftQuantity = localStorage.getItem("leftQuantity[" + iListId +"]");
      if (leftQuantity != null) {
        aBooks = JSON.parse(localStorage.getItem("aBooks[" + iListId +"]"));
        aBooksToSend = JSON.parse(localStorage.getItem("aBooksToSend[" + iListId +"]"));
        scannedBooks = JSON.parse(localStorage.getItem("scannedBooks[" + iListId +"]"));
        showLastBookArray(scannedBooks);
      } else {
        scannedBooks = ({});
        iScanned = 0;
      }
      
      console.log(scannedBooks);
     
      quantityAllBooks = 0;
      for(var i = 0; i < aBooks.length; i++)
      {
        if (aBooks[i] !== null && parseInt(aBooks[i].quantity) > 0) {
          quantityAllBooks += parseInt(aBooks[i].quantity);
        }
      }
      
      for(var i = 0; i < scannedBooks.length; i++)
      {
        if (scannedBooks[i] !== null && parseInt(scannedBooks[i].currquantity) > 0) {
          quantityAllBooks -= parseInt(scannedBooks[i].currquantity);
        }
      }      
      
      if (leftQuantity != null) {
        iScanned = quantityAllBooks - leftQuantity;
        $("#left_order_items").html(leftQuantity);
      } else {
        console.log(aBooks);
        $("#left_order_items").html(quantityAllBooks);
      }
    });
    
    $("#ordered_itm").submit( function(){
      
      if(typeof(Storage) !== "undefined") {
        localStorage.clear();
      }
      
      var serialized = JSON.stringify(aBooksToSend);
      
      if (serialized !== undefined) {
        $("#books").val(serialized);
      }
    });
  {/literal}
</script>
<div class="clear"></div>
<div>
  Pozostało do zeskanowania / ОСТАЛОСЬ КНИГ ДО РЕАЛИЗАЦИИ: <span id="left_order_items"></span><br /><br />
</div>
<div class="clear"></div>
<div id="number_cont">
  <p class="number_cont">
    <img id="main_image" /><br /><br />

    <span id="main_name"></span><br />
    <span id="main_isbn"></span><br />
    <span id="main_desc"></span>
  </p>
  <p class="cont_main_shelf_number location-style">
    <span id="main_shelf_number" style="font-size: 300px; color: #51a351;"></span>  
  </p>
  <div class="clear"></div>
</div>
<div class="list_scanned_items location-style">
  {section name=foo loop=3 step=+1}
    <div class="list_scanned_item">
      <img id="image_{$smarty.section.foo.index}" class="sc_image"/><br />
      <span id="shelf_number_{$smarty.section.foo.index}" class="sc_shelf"></span><br />
      <span id="name_{$smarty.section.foo.index}" class="sc_name" ></span><br />
      <span id="isbn_{$smarty.section.foo.index}" style="font-size: 14px; color: grey" ></span><br />
      <span id="desc_{$smarty.section.foo.index}" class="sc_desc" ></span>
    </div>
  {/section}
  <div class="clear"></div>
<div id="not_scanned" style="margin-top: 15px; display: none; text-align: center;">
  <hr />
  <div class="clear"></div>
  <div style="margin: 15px 0; font-size: 22px;">Braki:</div>
  <div class="clear"></div>
  <div id="not_scanned_sc">
    
    {*
      <div class="list_scanned_item">
        <img class="sc_image" /><br />
        <span class="sc_shelf"></span><br />
        <span class="sc_quantity"></span><br />
        <span class="sc_name"></span><br />
        <span class="sc_desc"></span>
      </div>
    *}
  </div>
</div>
</div>

<br />
<div class="clear"></div>
<div class="clear"></div>
{if $smarty.session.user.type == "1" || $smarty.session.user.type == "2"}
  <input type="button" id="debug_auto_confirm" value="Przesortuj wszystkie / АВТОМАТИЧЕСКАЯ СОРТИРОВКА" style="font-size: 25px" />
{/if}
{if $aData.is_big == '1'}
  <input type="button" id="debug_auto_confirm" value="Przesortuj wszystkie / АВТОМАТИЧЕСКАЯ СОРТИРОВКА" style="font-size: 25px" />
{/if}

{if $aData.is_big == '1'}
  <input type="button" id="save_and_sortout" value="Przesortuj wszystkie automatycznie i zapisz / АВТОМАТИЧЕСКИ ОТСОРТИРОВАТЬ И ЗАПИСАТЬ" style=" color: red; font-size: 25px" />
{/if}
<script>
  {literal}
  $('#save_and_sortout').on('click', function() {
    $("#do").val("save_big_list");
    $("#ordered_itm").trigger('submit');
//    $("#ordered_itm").submit();
  });
  {/literal}
</script>