<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset={$sCharset}">
  <meta http-equiv="Content-Language" content="{$sLanguage}">
  <meta name="Robots" content="NOFOLLOW, NOINDEX">
  <meta name="Pragma" content="no-cache">
  <meta name="Cache-Control" content="no-store, no-cache, must-revalidate">
  <title>{$sTitle}</title>
  <link href="css/styles.css?date=20180925" rel="stylesheet" type="text/css">
  <script type="text/javascript" language="JavaScript" src="js/JSCookMenu.js"></script>
  <link rel="stylesheet" href="css/theme.css" type="text/css">
  <link rel="stylesheet" href="js/css/ui-lightness/jquery-ui-1.9.2.custom.min.css">
  <script type="text/javascript" language="JavaScript" src="js/theme.js" ></script>
  <script type="text/javascript" language="JavaScript" src="js/calendar/GCappearance.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/calendar/GurtCalendar.js"></script>
  <script type="text/javascript" language="JavaScript" src="js/functions.js?date=11102018"></script>
  <link rel="stylesheet" href="js/css/alertifyjs/alertify.min.css">

  <script src="js/jquery-1.8.3.min.js" type="text/javascript"></script>
  <script src="js/catchEAN.js?date=06042018" type="text/javascript"></script>
	<!--<script src="js/jquery.checktree.js" type="text/javascript"></script>-->
	<script type="text/javascript" src="js/jquery.treeview.min.js"></script>
	<script type="text/javascript" src="js/jquery.cluetip.js"></script>
  <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
  <script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
  <script type="text/javascript" src="js/jquery.calculation.min.js"></script>
  <script src="js/checkboxtree/jquery.checkboxtree.js" type="text/javascript"></script>
  <script src="js/jquery-scripts.js" type="text/javascript"></script>
  <link href="js/checkboxtree/jquery.checkboxtree.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="js/simplemodal/messi.min.css" />
  <script src="js/simplemodal/messi.min.js"></script>
  <script src="js/custom.js?v9" type="text/javascript"></script>
  <script type="text/javascript" src="js/loadingoverlay.min.js"></script>

  <script type="text/javascript" src="js/alertify.min.js"></script>

  <link rel="stylesheet" href="css/calendar.css" type="text/css">
  {if isset($sHeaderJS)}{$sHeaderJS}{/if}
</head>

{if $scale == 1}
  <input type="hidden" id="scale_access" value="1"/>
{else}
  <input type="hidden" id="scale_access" value="0"/>
{/if}

<body{if isset($sBodyOnload) AND !empty($sBodyOnload)} onload="{$sBodyOnload}"{/if}>
<script type="text/javascript">
  hideOverlay = false;
</script>
<table style="width:100%; height:100%;" border="0" cellspacing="0" cellpadding="0">
  {if !empty($aUrlReferer)}
  <tr>
    <td style="font-size: 12px; height: 19px; vertical-align: center;">
      Wróć do 
      {foreach from=$aUrlReferer key=sUrlKey item=aUrl}
        
        &laquo; <a href="{$aUrl.url}">{$aUrl.name|urldecode}</a>
       {/foreach}
    </td>
  </tr>
  {/if}
  <tr>
    <td style="text-align: center; vertical-align: top;">
    {$sSessionMessage}
      {$sGroupsAlert}
		{$sContent}
    </td>
  </tr>
</table>
{if $smarty.get.frame == 'main'}
<link rel="stylesheet" href="css/scale.css" type="text/css">
<script type="text/javascript" language="JavaScript" src="js/cookie.js" ></script>
<script type="text/javascript" language="JavaScript" src="js/scale.js" ></script>
<script type="text/javascript" language="JavaScript" src="js/scale_functions.js" ></script>
{/if}

{literal}
  <script>
    $(function() {
//      $("form").submit( function () {
//        if (hideOverlay === false) {
//          $.LoadingOverlay("show", {
//            color: "rgba(255, 255, 255, 0.4)"
//          });
//        }
//      });
        alertify.defaults = {
            // dialogs defaults
            autoReset:true,
            basic:false,
            closable:true,
            closableByDimmer:true,
            frameless:false,
            maintainFocus:true, // <== global default not per instance, applies to all dialogs
            maximizable:true,
            modal:true,
            movable:true,
            moveBounded:false,
            overflow:true,
            padding: true,
            pinnable:true,
            pinned:true,
            preventBodyShift:false, // <== global default not per instance, applies to all dialogs
            resizable:true,
            startMaximized:false,
            transition:'pulse',

            // notifier defaults
            notifier:{
                // auto-dismiss wait time (in seconds)
                delay:5,
                // default position
                position:'bottom-right',
                // adds a close button to notifier messages
                closeButton: false
            },

            // language resources
            glossary:{
                // dialogs default title
                title:'Profit',
                // ok button text
                ok: 'OK',
                // cancel button text
                cancel: 'Cancel'
            },

            // theme settings
            theme:{
                // class name attached to prompt dialog input textbox.
                input:'ajs-input',
                // class name attached to ok button
                ok:'ajs-ok',
                // class name attached to cancel button
                cancel:'ajs-cancel'
            }
        };

        var komunikat = $(".message1").html();
      if ($('.messi:visible').length > 0 && komunikat !== undefined && komunikat !== "") {
        $(".message1").append('<audio autoplay hidden><source src="gfx/broken_siren.mp3" type="audio/mpeg"></audio>');
      }

      $( window ).on("unload", function() {
        $.LoadingOverlay("show", {
          color: "rgba(255, 255, 255, 0.4)"
        });
      });


      $(window).on("beforeunload", function() {
          $.LoadingOverlay("show", {
            color: "rgba(255, 255, 255, 0.4)"
          });
      });
    });
  </script>
{/literal}

</body>
</html>