<div id="collect_products">
  <div class="clear"></div>
  <div class="cont_main_shelf_number location-style">
    <span id="main_shelf_number" style="font-size: 100px; color: #51a351;"></span>
  </div>
{*    Pozostało do zeskanowania: <span id="left_order_items">{$aData.left_to_scan}</span><br /><br />*}
  <div id="number_cont">
    <div class="number_cont">
      <img id="main_image" src="{$aProduct.photo_path}/__b_{$aProduct.photo_name}" onError="this.src='{$aProduct.photo_path}/{$aProduct.photo_name}';" /><br /><br />

      <span id="main_name">{$aProduct.name}</span><br />
      <span id="main_isbn">{$aProduct.isbn_plain}, {$aProduct.ean_13}, {$aProduct.isbn_13},{$aProduct.isbn_10}</span><br />
      <span id="main_desc">{$aProduct.authors} {$aProduct.publishers}</span>
    </div>
    <div style="float: left; width: 340px;">
      <div id="main_quantity_current_item"></div>
      <div id="serwis_{$aData.bookstore}"><img src="/omniaCMS/gfx/websites_logos/logo_{$aData.website_id}.png" alt="{$aData.bookstore}" /></div>
      <div id="magazine_remarks_sorter">{$aData.magazine_remarks}</div>
      {if $aData.transport_symbol == "odbior-osobisty"}<div id="personal_reciption">UWAGA !! <br />odbiór osobisty</div>{/if}
    </div>
    <div class="clear"></div>
  </div>
  <br />
  <div class="clear"></div>
  <div class="clear"></div>
</div>