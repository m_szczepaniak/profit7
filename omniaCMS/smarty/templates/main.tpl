<table width="100%" height="100%"  border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="165" height="82" class="headerBg"><div class="logo"><img src="gfx/omnia_cms_logo.gif" width="137" height="38"></div></td>
    <td width="4" class="layoutSeparator"><img src="gfx/pixel.gif" width="1" height="1"></td>
    <td class="headerBg"><table width="100%" height="82"  border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="25" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					{$sHeaderMenuTable}
				</tr>
			</table>
			<script language="javascript">
				<!--
					{$sHeaderMenu}
				-->
				</script>
				<script language="javascript">
				<!--
					{$sHeaderMenuDraw}
				-->
			</script>
		</td>
      </tr>
      <tr>
        <td height="2"><img src="gfx/pixel.gif" width="1" height="1"></td>
      </tr>
      <tr>
        <td height="28" class="headerRow"><span class="headerInfo">{$aLang.lay_top.logged}: <strong>{$sUserName}</strong>&nbsp;&nbsp;&nbsp;&nbsp;{$aLang.lay_top.from_addr}: <strong>{$sIP}</strong></span></td>
      </tr>
      <tr>
        <td height="27" class="headerRow">{$sLangForm}</td>
      </tr>
    </table>
  	</td>
  </tr>
  <tr>
    <td class="content"><iframe src="admin.php?frame=left" id="leftMenu" name="leftMenu" frameborder="no" marginheight="0" marginwidth="0" scrolling="auto"></iframe></td>
    <td class="layoutSeparator"><img src="gfx/pixel.gif" width="1" height="1"></td>
    <td class="content">
      <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td height="18" valign="top">
              <iframe src="admin.php?frame=header" id="moduleName" name="moduleName" frameborder="no" marginheight="0" marginwidth="0" scrolling="no"></iframe>

              <table>
                <tr>
                  <td>
                    <span id="containter_alert_ajax" style="display: none">
                      <div id="alert_ajax" style="background: url('gfx/warning_min.png') no-repeat; background-position: 3px 0px; background-color: rgb(175, 169, 104); border: 1px solid #5b9f11; padding: 6px 9px 9px 36px; text-align: center; margin: 5px; color: #000; font-size: .9em"></div>
                    </span>
                    {literal}
                    <script type="text/javascript">
                      function doAjax() {
                        $.ajax({
                          url: "ajax/GetAlerts.php"
                        }).done(function(data) {
                          if (data === "-1") {
                            $("#containter_alert_ajax").hide();
                          } else {
                            $("#containter_alert_ajax").show();
                            $("#alert_ajax").html(data);
                          }
                        });
                      }

                      doAjax();
                      setInterval(function() {
                        doAjax();
                      }, 120000);
                    </script>
                    {/literal}
                  </td>
                </tr>
              </table>

            </td>
        </tr>
            <td>
              <iframe src="admin.php?frame=main" id="content" name="content" frameborder="no" marginheight="0" marginwidth="0" scrolling="auto"></iframe>
            </td>
          </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td height="21" class="footerBg">&nbsp;</td>
    <td class="layoutSeparator"><img src="gfx/pixel.gif" width="1" height="1"></td>
    <td class="footerBg"><div class="version"><span>{$sApplName}</span></div></td>
  </tr>
</table>