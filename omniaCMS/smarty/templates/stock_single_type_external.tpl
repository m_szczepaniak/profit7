<table cellpadding="1" cellspacing="0" border="1" style="font-size: 11px;">
  <tbody style="background-color: #fff;">
    <tr>
      <th class="it_stock" style="background-color: #141414; color: #fff;">Zewnętrzne</th>
      <th class="it_stock" style="background-color: #141414; color: #fff;">Ilość</th>
      <th class="it_stock" style="background-color: #141414; color: #fff;">C.Zakup brutto</th>
      <th class="it_stock" style="background-color: #141414; color: #fff;">C.SRP brutto</th>
      <th class="it_stock" style="background-color: #141414; color: #fff;">vat</th>
      <th class="it_stock" style="background-color: #141414; color: #fff;">%</th>
      <th class="it_stock" style="background-color: #141414; color: #fff;">Premiera</th>
   </tr>
    {foreach from=$aStockType item=aStock key=name}
        {if $name != "olesiejuk"}
          <tr class="bold">
             <td class="it_stock it_out" style="font-weight: bold; border-left: 1px solid #cfcfcf;">{$aStock.name}</td>
             {*<td class="it_stock it_out center_text">{if $aStock.status == '0' && ($name != "olesiejuk" && $aStock.stock != "")}0{else}{if $name == 'abe'}JEST{else}{$aStock.stock}{/if}{/if}</td>*}
             <td class="it_stock it_out center_text">{if $aStock.status == '3'}ZAP{else}{$aStock.status}{/if}</td>
             <td class="it_stock it_out center_text"><span style="{if $aStock.source_min_wh_price}color: red{/if}">{if $aStock.wholesale_price === NULL}---{else}{$aStock.wholesale_price|number_format:2:',':'.'}{/if}</span></td>
             <td class="it_stock it_out center_text">{$aStock.price_brutto}</td>
             <td class="it_stock it_out center_text">{$aStock.vat}</td>
             <td class="it_stock it_out center_text">{if $aStock.our_discount != ''}{$aStock.our_discount}{/if}</td>
             <td class="it_stock it_out center_text">{if $aStock.shipment_date != '' && $aStock.shipment_date != '0000-00-00'}{$aStock.shipment_date}{else}-{/if}</td>
          </tr>
        {/if}
    {/foreach}
  </tbody>
</table>