{$aBooks.orders_header}
<br />
<br />
<table cellspacing="0" border="1">
		<tr>
			<td style="font-weight: bold;">Lp</td>
			<td style="font-weight: bold;">{$aLang.books_list_id}</td>
			<td style="font-weight: bold;">{$aLang.books_list_name}</td>
			<td style="font-weight: bold;">{$aLang.books_list_isbn}</td>
			<td style="font-weight: bold;">{$aLang.books_list_publisher}</td>
			<td style="font-weight: bold;">{$aLang.books_list_year}</td>
			<td style="font-weight: bold;">{$aLang.books_list_ile}</td>
		</tr>
	{foreach from=$aBooks.items item=aBook}
		<tr>
			<td>{$aBook.lp}</td>
			<td>{$aBook.id}</td>
			<td><a href="{$aBook.link}" target="_blank">{$aBook.name}</a></td>
			<td>{$aBook.isbn}</td>
			<td>{$aBook.publisher}</td>
			<td>{$aBook.publication_year}</td>
			<td>{$aBook.quantity}</td>
		</tr>
	{/foreach}
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td style="font-weight: bold;">SUMA:</td>
			<td style="font-weight: bold;">{$aBooks.Qsum}</td>
		</tr>
</table>