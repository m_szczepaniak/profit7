{foreach from=$aStocks item=aStock key=id}
	<div class="stockInfo" id="stockTooltip_{$id}" style="display:none;">
					<table cellpadding="1" cellspacing="0" border="1" width="160px" style="margin:-10px;">
					  <tbody>
					    <tr>
					      <td class="it_1">Wewnętrzne</td>
					      <td class="it_2">Ilość/Lokalizacja</td>
					      <td class="it_3">{if empty($aStock.profit_e_location)}&nbsp;{else}{$aStock.profit_e_location}{/if}</td>
					      <td class="it_4">ABE</td>
					      <td class="it_5">{$aStock.abe_shipment}</td>
					    </tr>
					    <tr>
					      <td class="it_1">N</td>
					      <td class="it_2">{$aStock.profit_e_status} {if empty($aStock.profit_e_location)}&nbsp;{else}{$aStock.profit_e_location}{/if}</td>
					      <td class="it_4">ABE</td>
					      <td class="it_5">{$aStock.abe_shipment}</td>
					    </tr>
					    <tr>
					      <td class="it_1">J</td>
					      <td class="it_2">{$aStock.profit_j_status} {if empty($aStock.profit_j_location)}&nbsp;{else}{$aStock.profit_j_location}{/if}</td>
					      <td class="it_4">Azy</td>
					      <td class="it_5">{$aStock.azymut_shipment}</td>
					    </tr>
					    <tr>
					      <td class="it_1">M</td>
					      <td class="it_2">{$aStock.profit_m_status} {if empty($aStock.profit_m_location)}&nbsp;{else}{$aStock.profit_m_location}{/if}</td>
					      <td class="it_4">Hel</td>
					      <td class="it_5">{$aStock.helion_shipment}</td>
					    </tr>
					    <tr>
					      <td class="it_1">G</td>
					      <td class="it_2">{$aStock.profit_g_status} {if empty($aStock.profit_g_location)}&nbsp;{else}{$aStock.profit_g_location}{/if}</td>
					      <td class="it_3"></td>
					      <td class="it_4">&nbsp;</td>
					      <td class="it_5">&nbsp;</td>
					    </tr>
					    <tr>
					      <td class="it_1">X</td>
					      <td class="it_2">{$aStock.profit_x_status} {if empty($aStock.profit_x_location)}&nbsp;{else}{$aStock.profit_x_location}{/if}</td>
					      <td class="it_3"></td>
					      <td class="it_4">&nbsp;</td>
					      <td class="it_5">&nbsp;</td>
					    </tr>
					  </tbody>
					</table>
				</div>
{/foreach}