<table style="width: 100%">
  <tr>
    <th>ISBN</th>
    <th>Tytuł</th>
    <th>Ilość</th>
  </tr>
{assign var=iSumQuantity value=0}
{assign var=iSumAttachment value=0}
{foreach from=$aRecords item=aItem key=id}
  {assign var=iSumQuantity value=$iSumQuantity+$aItem.quantity}
  {if $aItem.is_attachment == 1} 
    {assign var=iSumAttachment value=$iSumAttachment+$aItem.quantity}
  {/if}
  <tr {if $aItem.is_attachment == 1}style="background-color: grey;"{/if}>
    <td>{$aItem.isbn_plain}</td>
    <td>{$aItem.name}</td>
    <td>{$aItem.quantity}</td>
  </tr>
{/foreach}
</table>
<br /><br />
Ilość produktów z załącznikami: {$iSumAttachment}<br />
Całkowita ilość produktów: {$iSumQuantity}