{*<h2>{$aModule.lang.order_print_header}&nbsp;{$aModule.order.order_number}</h2>*}
<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td width="40%" align="left"><img src="/images/gfx/faktura-logo.jpg" width="200px" height="76px"/><br/></td>
		<td width="60%" align="right" style="font-size: 12pt;">
			<br/>
			{$aModule.lang.data_to_pay} {$aModule.order.order_date}<br/>
			{$aModule.lang.data_to_realize} {if !empty($aModule.order.status_3_update)}{$aModule.order.status_3_update}{else}-{/if}<br/>
			<span style="font-size: 10pt;">{$aModule.lang.printout} {$aModule.print_date}</span><br/>
		</td>
	</tr>
	<tr>
		<td width="70%" align="left" style="font-size: 15pt;">
			{$aModule.lang.nr_order} <strong>{$aModule.order.order_number}</strong>
		</td>
	</tr>
	<tr>
		<td width="70%" align="left" style="font-size: 12pt;">
			{$aModule.lang.invoice_id} <strong>{$aModule.order.invoice_id}</strong>
		</td>
	</tr>
</table>
<br />
<hr>
<div style="margin-bottom:20px;">&nbsp;</div>
<table width="100%" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td width="220px">
			<strong>{$aModule.lang.data_buyer}</strong><br/>
		</td>
		<td width="220px">
			<strong>{$aModule.lang.data_invoice}</strong><br/>
		</td>
		<td width="220px">
			{if $aModule.order.second_invoice == '1'}
				<strong>{$aModule.lang.data_invoice2}</strong><br/>
			{else}
			<strong>{$aModule.lang.data_user}</strong><br/>
			{/if}
		</td>
	</tr>
	<tr>
		<td width="220px">
			{$aModule.transport_data}
		</td>
		<td width="220px">
			{$aModule.invoice_data}
		</td>
		<td width="220px">
			{if $aModule.order.second_invoice == '1'}
				{$aModule.invoice2_data}
			{else}
				{if $aModule.order.registred_user == '1'}
					{$aModule.lang.user} {$aModule.lang.user_register}<br/>
				{else}
					{$aModule.lang.user} {$aModule.lang.user_not_register}
				{/if}
				{if !empty($aModule.order.client_ip)}
					{$aModule.lang.client_ip} {$aModule.order.client_ip}<br/>
				{/if}
				{if !empty($aModule.order.client_host)}
					{$aModule.lang.client_host} {$aModule.order.client_host}<br/>
				{/if}
			{/if}
		</td>
	</tr>
</table>
<br /><br />
<table cellspacing="0" cellpadding="0" border="1" width="100%" align="center">
	<tr>
		<th width="27px">{$aModule.lang.lp}</th>
		<th width="190px" align="left">{$aModule.lang.name}</th>
		<th width="87px">{$aModule.lang.status}</th>
		<th width="40px">{$aModule.lang.weight}</th>
		<th width="35px">{$aModule.lang.quantity}</th>
		<th width="64px">{$aModule.lang.shipment}</th>
		<th width="35px">{$aModule.lang.vat}</th>
		<th width="65px">{$aModule.lang.price_brutto}</th>
		<th width="45px">{$aModule.lang.discount}</th>
		<th width="50px">{$aModule.lang.value_brutto}</th>
	</tr>
	{foreach from=$aModule.items key=iId item=aItem name=types}
		<tr>
			<td width="27px">{$aItem.lp}</td>
			<td width="190px" align="left">{$aItem.name}</td>
			<td width="87px">{$aItem.status}</td>
			<td width="40px">{$aItem.weight}</td>		
			<td width="35px">{$aItem.quantity}</td>
			<td width="64px">{$aItem.shipment}</td>
			<td width="35px">{$aItem.vat}</td>
			<td width="65px">{$aItem.price_brutto}</td>
			<td width="45px">{$aItem.discount}</td>
			<td width="50px">{$aItem.value_brutto}</td>
		</tr>
	{/foreach}
</table>
<br/><br/>
<table width="100%" cellspacing="0" cellpadding="0"  border="0" align="center">
	<tr>
		<td width="361px"></td>
		<td width="30px">{*<strong>455!!!</strong>*}</td>
		<td width="25px">{*<strong>15!!!</strong>*}</td>
		<td width="29px"></td>
		<td width="50px"><strong>{$aModule.lang.summary}</strong></td>
		<td width="45px">{*<strong>322.44!!!</strong>*}</td>
		<td width="45px">{*strong>322.44!!!</strong>*}</td>
		<td width="45px"><strong>{$aModule.order.value_brutto|format_number:2:',':' '}</strong></td>
	</tr>
</table>
<br/>
<hr>
<div style="margin-bottom:20px;">&nbsp;</div><br />
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="255px;">
						
			{*			
						<td>{if !empty($aModule.order.status_1_update)}{$aModule.order.status_1_update}{else}-{/if}</td>
				<td>{if !empty($aModule.order.status_2_update)}{$aModule.order.status_2_update}{else}-{/if}</td>
				<td>{if !empty($aModule.order.status_3_update)}{$aModule.order.status_3_update}{else}-{/if}</td>
				<td>{if !empty($aModule.order.status_4_update)}{$aModule.order.status_4_update}{else}-{/if}</td>

				*}
				
				
				
				<tr>
					<td>
						<strong>{$aModule.lang.order_status_header}</strong><br/>
					</td>
				</tr>
				<tr>
					<td>
						<strong>{$aModule.lang.order_status}</strong> {$aModule.order.order_status_txt}{* data!!!*}
					</td>
				</tr>
				<tr>
					<td>
						<strong>{$aModule.lang.payment_status}</strong> {$aModule.order.payment_status_txt}
					</td>
				</tr>
				<tr>
					<td>
						<strong>{$aModule.lang.compled_status}</strong> {if !empty($aModule.order.status_2_update)}{$aModule.order.status_2_update}{else}-{/if}
					</td>
				</tr>
				<tr>
					<td>
						<strong>{$aModule.lang.cancel_status}</strong> {if !empty($aModule.order.status_4_update)}{$aModule.order.status_4_update}{else}-{/if}
					</td>
				</tr>
			</table>
		</td>
		<td>
			<table width="305px" border="1" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<br/>
						<table width="305px" border="0" cellspacing="0" cellpadding="0" style="font-size: 13pt;">
							<tr>
								<td width="290px" align="right" style="padding-right: 15px;">
                  <br /><br />
									<strong>{$aModule.lang.value_order} {$aModule.order.value_brutto|format_number:2:',':' '} {$aLang.common.currency}</strong><br/>
									<strong>{$aModule.lang.value_order_transport} {$aModule.order.to_pay|format_number:2:',':' '} {$aLang.common.currency}</strong><br/>
									<strong>
										{$aModule.lang.value_pay} {if $aModule.order.paid_amount > 0} {$aModule.order.paid_amount|format_number:2:',':' '} {$aLang.common.currency} {else}-{/if}
									</strong><br /><br />
								</td>
							</tr>
						</table>
					</td>		
				</tr>
			</table>
		</td>
	</tr>
</table>
<br/><br/>
<table width="500px;" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<strong>{$aModule.lang.transport_and_pay}</strong><br/>
		</td>
	</tr>
	<tr>
		<td>
			<strong>{$aModule.lang.transport_metod}</strong> {$aModule.order.transport}
		</td>
	</tr>
	<tr>
		<td>
			<strong>{$aModule.lang.number_list}</strong> {$aModule.order.transport_number}
		</td>
	</tr>
	<tr>
		<td>
			<strong>{$aModule.lang.pay_way}</strong> {$aModule.order.payment}
		</td>
	</tr>
	<tr>
		<td>
			<strong>{$aModule.lang.cost_transport}</strong> {$aModule.order.transport_cost|format_number:2:',':' '} {$aLang.common.currency}
		</td>
	</tr>
	<tr>
		<td>
			<strong>{$aModule.lang.info}</strong> {if !empty($aModule.order.block_transport_cost)}TAK{else}NIE{/if}
		</td>
	</tr>
</table>


{*
<br/><br/><br/><br/>

<table border="1">
	<tr>
		<td colspan="8">&nbsp;</td>
		<td>{$aModule.lang.transport_mean}</td>
		<td>{$aModule.order.transport}</td>
	</tr>
	<tr>
		<td colspan="8">&nbsp;</td>
		<td>{$aModule.lang.payment_method}</td>
		<td>{$aModule.order.payment}</td>
	</tr>
	<tr>
		<td colspan="8">&nbsp;</td>
		<td>{$aModule.lang.items_list_transport_price}</td>
		<td>{$aModule.order.transport_cost|format_number:2:',':' '} {$aLang.common.currency}</td>
	</tr>
	<tr>
		<td colspan="8">&nbsp;</td>
		<td>{$aModule.lang.items_list_block_transport_cost}</td>
		<td>{if !empty($aModule.order.block_transport_cost)}TAK{else}NIE{/if}</td>
	</tr>
	<tr>
		<td colspan="8">&nbsp;</td>
		<td>{$aModule.lang.items_list_items_total_value}</td>
		<td>{$aModule.order.value_brutto|format_number:2:',':' '} {$aLang.common.currency}</td>
	</tr>
	<tr>
		<td colspan="8">&nbsp;</td>
		<td>{$aModule.lang.items_list_items_total_value_transport}</td>
		<td>{$aModule.order.total_value_transport|format_number:2:',':' '} {$aLang.common.currency}</td>
	</tr>
	{if !empty($aModule.order.paid_amount)}
	<tr>
		<td colspan="8">&nbsp;</td>
		<td>{$aModule.lang.items_list_items_paid_amount}</td>
		<td>{$aModule.order.paid_amount|format_number:2:',':' '} {$aLang.common.currency}</td>
	</tr>
	{/if}
</table>
<table  border="1">
	<tr>
		<th>aaa{$aModule.lang.user_data_details}</th>
		<th>bbb{$aModule.lang.user_data_invoice}</th>
	</tr>
	<tr>
		<td>ccc{$aModule.user_data}</td>
		<td>ddd{$aModule.invoice_data}</td>
	</tr>
</table>
<table border="1">
	{if !empty($aModule.order.remarks)}
		<tr>
			<td>{$aModule.lang.remarks}</td>
			<td>{$aModule.order.remarks}</td>
		</tr>
	{/if}
	<tr>
		<td>{$aModule.lang.user_type}</td>
		<td>
		{if !empty($aModule.order.user_id)}
			{$aModule.lang.user_type_registred}
		{else}
			{$aModule.lang.user_type_not_registred}
		{/if}
		</td>
	</tr>
	{if !empty($aModule.order.client_ip)}
	<tr>
		<td>{$aModule.lang.client_ip}</td>
		<td>{$aModule.order.client_ip}</td>
	</tr>
	{/if}
	{if !empty($aModule.order.client_host)}
	<tr>
		<td>{$aModule.lang.client_host}</td>
		<td>{$aModule.order.client_host}</td>
	</tr>
	{/if}
	{if $aModule.order.source != ''}
	<tr>
		<td>{$aModule.lang.source}</td>
		<td>{$aModule.order.source}</td>
	</tr>
	{/if}
	{if !empty($aModule.order.invoice_id)}
	<tr>
		<td>{$aModule.lang.invoice_id}</td>
		<td>{$aModule.order.invoice_id}</td>
	</tr>
	{/if}
	{if !empty($aModule.order.transport_number)}
	<tr>
		<td>{$aModule.lang.transport_number}</td>
		<td>{$aModule.order.transport_number}</td>
	</tr>
	{/if}
	<tr>
		<td>{$aModule.lang.order_status}</td>
		<td>{$aModule.order.order_status_txt}</td>
	</tr>
	<tr>
		<td>{$aModule.lang.payment_status}</td>
		<td>{$aModule.order.payment_status_txt}</td>
	</tr>
	
</table>
<table border="1">
	<tr>
		<th>{$aModule.lang.status_1_update}</th>
		<th>{$aModule.lang.status_2_update}</th>
		<th>{$aModule.lang.status_3_update}</th>
		<th>{$aModule.lang.status_4_update}</th>
	</tr>
	<tr>
		<td>{if !empty($aModule.order.status_1_update)}{$aModule.order.status_1_update}{else}-{/if}</td>
		<td>{if !empty($aModule.order.status_2_update)}{$aModule.order.status_2_update}{else}-{/if}</td>
		<td>{if !empty($aModule.order.status_3_update)}{$aModule.order.status_3_update}{else}-{/if}</td>
		<td>{if !empty($aModule.order.status_4_update)}{$aModule.order.status_4_update}{else}-{/if}</td>
	</tr>
</table>*}