<?php
$aConfig['continue_session'] = true;
include_once('config/config.inc.php');
$aConfig['use_db_manager'] = true;
$aConfig['common']['use_session'] = '1';
include_once('config/ini.inc.php');
include_once('Login.class.php');
include_once('lang/'.$aConfig['default']['language'].'.php');

if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == ""){
    $redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    header("Location: $redirect");
    exit;
}

header('Content-Type: text/html; charset=UTF-8');
// pobranie typu i wersji przegladarki
getBrowserInfo();

// utworzenie obiektu klasy Login
$pLogin = new Login();

// pobranie nazwy i wersji CMS
setCMSVersion();

$pSmarty->assign_by_ref('sCharset', $aConfig['default']['charset']);
$pSmarty->assign('sTitle', $aConfig['common']['name'].' '.$aConfig['common']['version']);
$pSmarty->assign('sBaseHref', $aConfig['common']['base_url_http']);

if (!$pLogin->IsLoggedIn()) {
	// uzytkownik nie jest zalogowany, przkierowanie na strone logowania
 	header('Location: index.php');
 	exit();
}
else {
 	// uzytkownik jest zalogowamy
 	// pobranie ustawien serwisu
	setWebsiteSettings($_SESSION['lang']['id']);

 	// dolaczenie klasy admin obslugujacej CMS
  include('Admin.class.php');
  $pAdmin = new Admin($pSmarty);
  
 	if (!isset($_GET['hideHeader'])) {
  	header('Content-Type: text/html; charset='.$aConfig['default']['charset']);
  	header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		if ($aConfig['common']['use_zlib']) {
			ob_start("ob_gzhandler");
		}
    if ($_GET['action'] == 'opek_transfers' && $_GET['module'] == 'm_finanse') {
      $pSmarty->display('new_index.tpl');
    } else {
      $pSmarty->display('index.tpl');
    }
 	}
	if (isset($_GET['benchmark']) && $_GET['benchmark'] == '1') {
		// Timer
		$oTimer->stop();
 		$oTimer->display();
	}
}
?>