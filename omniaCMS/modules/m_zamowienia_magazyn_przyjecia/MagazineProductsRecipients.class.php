<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-05 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace omniaCMS\modules\m_zamowienia_magazyn_przyjecia;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

/**
 * Klasa obsługi przyjęć dostaw
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class MagazineProductsRecipients  {
  
  
  /**
   *
   * @var \Doctrine\ORM\EntityManager
   */
  private $em;

  /**
   *
   * @var \Smarty
   */
  private $pSmarty;
  
  /**
   *
   * @var string
   */
  private $sModule;
  
  public function __construct(\omniaCMS\lib\interfaces\ModuleEntity $oModuleRecipent) {
    
    $this->em = $oModuleRecipent->em;
    $this->pSmarty = $oModuleRecipent->pSmarty;
    $this->sModule = $oModuleRecipent->sModule;
  }
  
  /**
   * 
   * @param int $iMagazine
   * @param int $iOrdersItemsListId
   * @return string
   */
  public function getProductsView($iMagazine, $iOrdersItemsListId = 0) {
    // ładujemy produkty z tego zamówienia do dostawcy, do buffora skanowania
    
    // dodajemy AJAX, jeśli dany produkt nie występuje w bazie danych
    
    // zmieniamy kuwetę
    // zmieniamy wagę
    // oznaczamy defekt
    
    // dopasowywanie produktu z dostway do produktu w ofercie produktowej.
    
    // zamykamy kuwetę
    // 
    // dodajemy uwagi do dostawy--- wspólne pole
    // zamykamy dostawę

    /* @var $OrdersItemsListsItems \Entities\OrdersItemsListsItems  */
    $OrdersItemsListsItems = $this->getOrdersItemsListsItem($iOrdersItemsListId);
    /* @var $products \Entities\ExtProducts  */
    $products = array();
    foreach ($OrdersItemsListsItems as $iKey => $Item) {
      $products[$iKey] = new \Entities\ExtProducts();
      $products[$iKey]->weight = $Item->getOrdersItems()->getWeight();
      $products[$iKey]->quantity = $Item->getQuantity();
      $products[$iKey]->product = $Item->getProducts();
      $products[$iKey]->cover = $this->simpleGetCover($products[$iKey]->product->getId(), $Item->getProducts()->getIsbnPlain());
    }
    $encoders = array(new XmlEncoder(), new JsonEncoder());
    $normalizers = array(new GetSetMethodNormalizer());

    $serializer = new Serializer($normalizers, $encoders);
    $sExtProductsJSON = $serializer->serialize($products, 'json');
    $this->pSmarty->assign('magazine_name', $this->getMagazineName($iMagazine));
    $this->pSmarty->assign('products', $sExtProductsJSON);
    $sHTML = $this->pSmarty->fetch($this->sModule.'/products_recipient_containers.tpl');
    $this->pSmarty->clear_assign('products');
    return $sHTML;
  }
  
  /**
   * 
   * @param int $iProductId
   * @param string $sIsbnPlain
   * @return string
   */
  private function simpleGetCover($iProductId, $sIsbnPlain) {
    
		$iRange=intval(floor($iProductId/1000))+1;
		$sDestDir = 'images/photos/okladki/'.$iRange.'/'.$iProductId;
		return 'https://www.profit24.pl/'.$sDestDir.'/'.$sIsbnPlain.".jpg";
  }
  
  
  /**
   * 
   * @param int $iOrdersItemsLists
   * return \Entities\OrdersItemsListsItems
   */
  private function getOrdersItemsListsItem($iOrdersItemsLists) {
    
    
    $OrdersItemsLitstsItemsRepo = $this->em->getRepository('Entities\OrdersItemsListsItems');
    $qb = $OrdersItemsLitstsItemsRepo->createQueryBuilder('oili');
    $ss = $qb->select()
            ->where('IDENTITY(oili.ordersItemsLists) = :oil')
            ->setParameter('oil', $iOrdersItemsLists);
    /* @var $OrdersItemsListsItems \Entities\OrdersItemsListsItems  */
    $OrdersItemsListsItems = $ss->getQuery()->execute();
    return $OrdersItemsListsItems;
  }

  /**
   * 
   * @param int $iMagazine
   * @return string
   */
  private function getMagazineName($iMagazine) {
    
    /* @var $magazine \Entities\Magazines  */
    $magazine = $this->em->getReference('Entities\Magazines', $iMagazine);
    return $magazine->getName();
  }
}
