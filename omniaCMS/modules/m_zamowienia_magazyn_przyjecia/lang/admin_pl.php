<?php

$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['user_id'] = "Użytkownik";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['ean_13'] = "EAN13";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['container_id'] = "ID Półki";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['quantity'] = "Ilość";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['document_number'] = "Numer listy";