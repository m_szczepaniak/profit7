<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-05 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace omniaCMS\modules\m_zamowienia_magazyn_przyjecia;
/**
 * Klasa przyjęć
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_przyjecia__recipients implements \omniaCMS\lib\interfaces\ModuleEntity {

  /**
   * @var \Admin
   */
  public $oClassRepository;

  /**
   *
   * @var \Doctrine\ORM\EntityManager
   */
  public $em;
  
  /**
   *
   * @var string - komunikat
   */
  private $sMsg;
  
  /**
   *
   * @var \Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  public function __construct(\Admin $oClassRepository) {
    
    $this->oClassRepository = $oClassRepository;
    $this->em = $this->oClassRepository->entityManager;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
  }
  
  /**
   * 
   * @return string
   */
  public function doDefault() {
    // pobieramy magazyn
    $aMagazines = (array)$this->getMagazines();
    $oButtons = new \omniaCMS\lib\Form\ButtonGroup(phpSelf(array('do' => 'getProducts')));
    $oButtons->AddHeader(_('Wybierz Magazyn'), 1)
            ->AddButtonsArray($aMagazines, array('style' => 'font-size: 30px; padding: 30px; margin: 40px;'));
            
    return $oButtons->GetHTML();
  }
  
  /**
   * 
   * @return string
   * @throws \omniaCMS\modules\m_zamowienia_magazyn_przyjecia\Exception
   */
  public function doGetProducts() {
    try {
      $iMagazine = $this->setMagazine();
    } catch (Exception $ex) {
      throw $ex;
    }
    $oMagazineProductsRecipients = new MagazineProductsRecipients($this);
    return $oMagazineProductsRecipients->getProductsView($iMagazine, 1116);
  }

  
  /**
   * Ustawiamy magazyn
   * 
   * @return int
   * @throws Exception
   */
  private function setMagazine() {
    if (isset($_POST) && $_POST['magazine'] > 0) {
      return  $_POST['magazine'];
    } else {
      throw new \Exception('Nie wybrano magazynu');
    }
  }
  
  /**
   * 
   * @return array
   */
  private function getMagazines() {
    $Magazines = $this->em->getRepository('Entities\Magazines');
    $qb = $Magazines->createQueryBuilder('M');
    $aMagazines = $qb->select('M.id AS value, M.name as label')
            ->addOrderBy('M.name', 'ASC')
            ->getQuery()
            ->execute();
    return $aMagazines;
  }
  
  /**
   * 
   * @return string
   */
  public function getMsg() {
    return $this->sMsg;
  }
}



//    
//    $qb = $this->em->createQueryBuilder('Entities\MagazineProductsReceipts');
//    
//    $qb->select('u')
//       ->from('\Entities\OrdersItems', 'u')
//       ->where('u.order > :oi_id')
//       ->orderBy('u.order', 'ASC')
//            ->setParameter('oi_id', 133171)
//       ->getMaxResults(10);
//    dump($qb->getDQL());
//    /* @var $oe \Entities\OrdersItems */
//    $oe = $qb->getQuery()->execute();
//    foreach ($oe as $oes) {
//      var_dump($oes->getOrder()->getId());
//      
//    }
//    die;
    /* @var \Entities\MagazineProductsReceipts */
//    $qb->select('u')
//       ->from('User', 'u')
//       ->where('u.id = ?1')
//       ->orderBy('u.name', 'ASC');
//    
//    
//    
//            ->eagerCursor(true);
//    $query = $qb->getQuery();
//    $users = $query->execute();
//    dump($users);die;
//    
//    $websites = $this->em->getRepository('Entities\MagazineProductsReceipts');
//    $qb = $websites->createQueryBuilder('d');
//    $bilder = $qb->select('d.id')->where('d.id = 1');
//    dump($bilder->getQuery()->execute(1));
//   