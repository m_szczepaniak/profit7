<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_przyjecia;

use Admin;
use DatabaseManager;
use Exception;
use FormTable;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\EntityManager\Entites\StockLocation;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use magazine\Containers;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;
use Validator;
use View\touchKeyboard;

/**
 * Description of Module
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_przyjecia__products_stock_locations_log implements ModuleEntity
{

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;

  /**
   *
   * @var string - komunikat
   */
  private $sMsg;

  /**
   * @var Admin
   */
  public $oClassRepository;
  private $sModule;

  public function __construct(Admin $oClassRepository)
  {

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }

  public function doDefault()
  {
    return $this->getLogView();
  }

  public function getLogView() {
      global $aConfig;

      // dolaczenie klasy View
      include_once('View/View.class.php');

      // zapamietanie opcji
      rememberViewState($this->sModule);

      $aHeader = array(
          'header' => sprintf(_('Produkty przyjęte przez użytkowników')),
          'refresh' => true,
          'search' => true,
          'checkboxes' => false
      );
      $aAttribs = array(
          'width' => '100%',
          'border' => 0,
          'cellspacing' => 0,
          'cellpadding' => 0,
          'class' => 'viewHeaderTable'
      );
      $aRecordsHeader = array(
          array(
              'db_field'	=> 'login',
              'content'	=> $aConfig['lang'][$_GET['module']]['user_id'],
              'sortable'	=> true
          ),
          array(
              'db_field'	=> 'ean_13',
              'content'	=> $aConfig['lang'][$_GET['module']]['ean_13'],
              'sortable'	=> true
          ),
          array(
              'db_field'	=> 'container_id',
              'content'	=> $aConfig['lang'][$_GET['module']]['container_id'],
              'sortable'	=> true
          ),
          array(
              'db_field'	=> 'quantity',
              'content'	=> $aConfig['lang'][$_GET['module']]['quantity'],
              'sortable'	=> true
          ),
          array(
              'db_field'	=> 'document_number',
              'content'	=> $aConfig['lang'][$_GET['module']]['document_number'],
              'sortable'	=> true
          )
      );

      $pView = new \View('users_groups', $aHeader, $aAttribs);

      $sSql = 'SELECT A.id, C.login, B.ean_13, A.container_id, A.quantity, E.document_number 
                FROM products_stock_locations_log A 
                LEFT JOIN products B ON B.id = A.product_id 
                LEFT JOIN users C ON C.id = A.user_id 
                LEFT JOIN orders_items_lists_items D ON D.id = A.orders_items_lists_item_id 
                LEFT JOIN orders_items_lists E ON D.orders_items_lists_id = E.id' .
                (isset($_POST['search']) && !empty($_POST['search']) ? ' WHERE B.ean_13 LIKE \'%'.$_POST['search'].'%\'' : '') .
                (isset($_POST['search']) && !empty($_POST['search']) ? ' OR E.document_number LIKE \'%'.$_POST['search'].'%\'' : '');

      // pobranie liczby wszystkich uzytkownikow
      $aRecords = $this->pDbMgr->GetAssoc('profit24', $sSql);
      $iRowCount = count($aRecords);
      if ($iRowCount > 0) {
          $iCurrentPage = $pView->AddPager($iRowCount);
          $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
          $iStartFrom = ($iCurrentPage - 1) * $iPerPage;

          $sSql .= " LIMIT ".$iStartFrom.", ".$iPerPage;
          $aRecords = $this->pDbMgr->GetAssoc('profit24', $sSql);

          $aColSettings = array(
              'action' => array(
                  'actions' => array(),
                  'params' => array(
                  ),
                  'show' => false,
              )
          );

          $pView->AddRecords($aRecords, $aColSettings);
      }
      $pView->AddRecordsHeader($aRecordsHeader);
      $aRecordsFooter = array(
          array('go_back')
      );
      $aRecordsFooterParams = array(
          'go_back' => array(1 => array('action', 'id', 'pid'))
      );
      $pView->AddRecordsFooter($aRecordsFooter);
      return $pView->Show();
  }

  public function getMsg()
  {
    $sJS = '<script>
    $(function() {
      var komunikat = $(".message1").html();
      if (komunikat !== undefined && komunikat !== "") {
        new Messi(komunikat, {title: "Błąd", modal: true});
      }
    });
    </script>';
    return $this->sMsg . $sJS;
  }
}
