<?php

namespace omniaCMS\modules\m_zamowienia_magazyn_przyjecia;

use Admin;
use Common_get_ready_orders_books;
use DatabaseManager;
use FormTable;
use LIB\EntityManager\Entites\Magazine;
use magazine\HighLevelStock;
use omniaCMS\lib\interfaces\ModuleEntity;

require_once('modules/m_zamowienia_magazyn_zbieranie_tramwaj/Common_get_ready_orders_books.class.php');

/**
 * @author Paweł Bromka
 * @created 2014-11-12
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__zamowienia_magazyn_przyjecia__unpacking extends Common_get_ready_orders_books implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;
  protected $destinationMagazineDeficiencies;

  /**
   *
   * @var string - komunikat
   */
  private $sMsg;

  /**
   *
   * @var \Smarty
   */
  public $pSmarty;

  /**
   *
   * @var string
   */
  public $sModule;

  /**
   *
   * @var DatabaseManager
   */
  public $pDbMgr;

  protected $highLevelStock;

  public function __construct(Admin $oClassRepository) {

    $this->oClassRepository = $oClassRepository;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    if (isset($_GET['magazine']) && $_GET['magazine'] != '') {
      $this->destinationMagazineDeficiencies = $_GET['magazine'];
    }
    if (isset($_POST['magazine']) && $_POST['magazine'] != '') {
      $this->destinationMagazineDeficiencies = $_POST['magazine'];
    }
    $this->highLevelStock = new HighLevelStock($this->pDbMgr,$this->destinationMagazineDeficiencies);

    parent::__construct($this);
  }

  /**
   * 
   * @return HTML
   */
  public function doDefault() {
    // dolaczenie klasy FormTable
    include_once('Form/FormTable.class.php');

    $pForm = new FormTable('general_discounts', _('Wczytaj EANy do rozłożenia produktów'), '', ['col_width' => 100]);
    $pForm->AddHidden('magazine', $this->destinationMagazineDeficiencies);
    $pForm->AddTextArea('products_ean', _('EANy produktów'), $_POST['products_ean'], ['autofocus' => 'autofocus', 'style' => 'font-size: 45px; width:450px; height:600px']);
    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz'), array('style'=>'background-color: #51a351; font-size: 40px; text-align: center; color: #000;','onclick'=>'submit();'), 'button'));
    $pForm->AddHidden('do', 'showList');

    return $pForm->ShowForm();
  }

  /**
   * 
   * @return HTML
   */
  public function doShowList() {
    $sEANs = $_POST['products_ean'];
    $aList = $this->prepareProductsListItems($sEANs);
    if (!empty($aList)) {
      return $this->getProductsScanner($this->pSmarty, $aList, rand(0,10000));
    } else {
      return $this->doDefault();
    }
  }

    /**
     *
     * @return HTML
     */
    public function doGreets()
    {
        include_once($_SERVER['DOCUMENT_ROOT'].'LIB/magazine/HighLevelStock.class.php');
        if (!$this->highLevelStock instanceof HighLevelStock) {
            $this->highLevelStock = new HighLevelStock($this->pDbMgr, $this->destinationMagazineDeficiencies);
        }

        $bIsErr = false;

        // ok mamy sobie dwie tablice
        $aBooksScanned = json_decode($_POST['books']);
        $aDeficienciesBooks = json_decode($_POST['not_scanned_books']);

        if (empty($aBooksScanned)) {
            $_POST['books'] = preg_replace("/([{,])([a-zA-Z][^: ]+):/", "$1\"$2\":", $_POST['books']);
            $aBooksScanned = json_decode($_POST['books']);
        }

        if (empty($aDeficienciesBooks)) {
            $_POST['not_scanned_books'] = preg_replace("/([{,])([a-zA-Z][^: ]+):/", "$1\"$2\":", $_POST['not_scanned_books']);
            $aDeficienciesBooks = json_decode($_POST['not_scanned_books']);
        }
        $this->markUnpackingProducts($aBooksScanned);
        $this->markUnpackingProducts($aDeficienciesBooks);


        $this->sMsg .= GetMessage(_('Zakończono rozkładanie produktów na magazynie'), false);
        $fileContent = print_r($_SESSION, true) . " \n\n " . print_r($_POST, true) . " \n\n " . var_export($_POST, true);
        return $this->doDefault();
    }


    public function markUnpackingProducts($books) {

        foreach ($books as $book) {
            if (intval($book->currquantity) > 0) {
                if (intval($book->product_id) > 0) {
                    // sprawdźmy czy jest to na highLevelStock
                    $collectedProductId = $this->highLevelStock->getProductCollectedId($book->product_id);
                  if ($_SESSION['user']['name'] == 'agolba') {
                    dump($collectedProductId);
                  }
                    if ($collectedProductId > 0) {
                        $this->highLevelStock->markProductCompleted($collectedProductId);
                    }
                }
            }
        }
    }

  /**
   * 
   * @param array $sEANs
   * @return array
   */
  private function prepareProductsListItems($sEANs) {

    $aEANs = explode("\r\n", $sEANs);
    $aListItems = [];
    foreach ($aEANs as $sEAN) {
      if ($sEAN != '') {
        $aLineItem = $this->getItemDataForList($sEAN);
        if (empty($aLineItem)) {
          $this->sMsg .= GetMessage(_('Brak produktu w bazie o EAN: ' . $sEAN));
          return false;
        }
        $aListItems[] = $aLineItem;
      }
    }
    
//    $aListItems = $this->randomizeEmptyLocation($aListItems);
    $aListItems = $this->addStockPackNumberFormeted($aListItems);
    
    usort($aListItems, 'sortItems');
    return $aListItems;
  }
  
  /**
   * 
   * @param array $aListItems
   * @return string
   */
  private function addStockPackNumberFormeted($aListItems) {
    
    foreach ($aListItems as $iKey => $aItem) {
      if ($aItem['stock_pack_number'] != '') {
        $aListItems[$iKey]['pack_number'] = $aItem['pack_number'].'<br /><span style="font-size: 70%;color: #99E499;">('.$aItem['stock_pack_number'].')</span>';
      }
    }
    return $aListItems;
  }

  /**
   * 
   * @param string $sEAN
   * @return array
   */
  private function getItemDataForList($sEAN) {

    if ($this->destinationMagazineDeficiencies == Magazine::TYPE_HIGH_STOCK_SUPPLIES) {
      $sSqlectLocations = '
         (SELECT profit_g_location FROM products_stock AS PS WHERE PS.id = P.id) AS pack_number,
         "" AS stock_pack_number,
      ';
    } else {
      $sSqlectLocations = '
         (SELECT profit_x_location FROM products_stock AS PS WHERE PS.id = P.id) AS pack_number,
         "" AS stock_pack_number,
      ';
    }

    $sSqlParam = 'SELECT 
      "1" AS quantity, 
      P.name, PP.name AS publisher, 
      ( SELECT GROUP_CONCAT(CONCAT(PA.name, " ", PA.surname), ",") 
        FROM products_to_authors AS PTA
        JOIN products_authors AS PA
         ON PTA.author_id = PA.id
        WHERE PTA.product_id = P.id
        ) AS authors, 
      P.publication_year, P.edition, P.id AS product_id, 
      P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13, 
      '.$sSqlectLocations.'
      (SELECT profit_g_act_stock FROM products_stock AS PS WHERE PS.id = P.id) as profit_g_act_stock,
      (SELECT CONCAT(directory, "|", photo) FROM products_images AS PI WHERE PI.product_id = P.id LIMIT 1) AS photo
      FROM products AS P
      LEFT JOIN products_publishers AS PP
       ON P.publisher_id = PP.id
      WHERE ean_13 = "%1$s" OR
            streamsoft_indeks = "%1$s" OR
            isbn_plain = "%1$s" OR
            isbn_13 = "%1$s" OR
            isbn_10 = "%1$s" OR
            isbn_10 = "%1$s"
    ';
    $sSql = sprintf($sSqlParam, $sEAN);
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
  
  
  private function getRandomLocationDB($sType) {
    
    $sSql = 'SELECT id
             FROM containers
             WHERE type = "'.$sType.'"
             ORDER BY RAND()
             LIMIT 1';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
  
  /**
   * 
   * @param array $aBooks
   * @return array
   */
  private function randomizeEmptyLocation($aBooks) {

    $aProductsAssigned = [];
    foreach ($aBooks as $iKey => $aBook) {
      if ($aBook['pack_number'] === '' && $aBook['stock_pack_number'] === '') {
        if (isset($aProductsAssigned[$aBook['product_id']])) {
          $aBooks[$iKey]['pack_number'] = $aProductsAssigned[$aBook['product_id']];
          $aBooks[$iKey]['random_pack_number'] = '1';
        } else {
          $aBooks[$iKey]['pack_number'] = $this->getRandomLocationDB('2');
          $aBooks[$iKey]['random_pack_number'] = '1';
          $aProductsAssigned[$aBook['product_id']] = $aBooks[$iKey]['pack_number'];
          $this->saveRandomLocation($aBook['product_id'], $aBooks[$iKey]['pack_number']);
        }
      }
    }
    return $aBooks;
  }
  
  /**
   * 
   * @param int $iProductId
   * @param string $sLocation
   */
  private function saveRandomLocation($iProductId, $sLocation) {
    include_once('Module.class.php');
    $magazynPrzyjecia = new Module__zamowienia_magazyn_przyjecia($this->oClassRepository);
    $result = $magazynPrzyjecia->updateProductLocation($iProductId, $sLocation);
    if ($result === -2) {
      $this->sMsg .= _('Wylosowana kuweta nie istnieje.');
    }
    else if ($result === false) {
      $this->sMsg .= _('Wystąpił błąd podczas zapisywania kuwety');
    }
  }

  /**
   * Metoda tworzy i zwraca PDF'a
   * 
   * @param \Smarty $pSmarty
   * @param array $aList
   * @pram string $sPackageNumber
   */
  protected function getProductsScanner(&$pSmarty, $aList, $sPackageNumber) {

    $iRefCountMagazine = 0;
    $iRefCountTramwaj = 0;
    $this->countQuantityItems($aList, $iRefCountMagazine, $iRefCountTramwaj);
    $aViewList = $this->sumByArray($aList, 'quantity', 'pack_number');

    $aBooks = array();
    $aBooks['number'] = $sCode;
    $aBooks['items'] = $this->parseBooksToList($aViewList);


    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('ordered_itm', _('Rozkładanie towaru'), array('action' => phpSelf()), array('col_width' => 155), false);//, 'onsubmit' => 'return false;'
    $pForm->AddHidden('do', 'greets');
    $pForm->AddHidden('magazine', $this->destinationMagazineDeficiencies);
    $pForm->AddHidden('orders_items_lists_id', rand(999999,999999999999));
    $pForm->AddMergedRow(
            '<div style="float:left">' .
            $pForm->GetLabelHTML('search_isbn', _(' '), false) .
            $pForm->GetTextHTML('search_isbn', _('ISBN'), '', array('style' => 'font-size: 40px; margin-right: 50px;'), '', 'uint', true) .
            '</div>' .
            'Czas: <span id="timer"></span> &nbsp; &nbsp; ' .
            $pForm->GetLabelHTML('package_number', _('Numer kuwety: '), false) . ' ' . $sPackageNumber
    );

    $sHTML = $this->getViewProductsScanner($pSmarty, $aBooks, true);
    $pForm->AddMergedRow($sHTML);
    $pForm->AddMergedRow(
            '<div style="text-align: center;">'
            . $pForm->GetInputButtonHTML('save', _('Zakończ'), array('style' => 'background-color: #51a351; font-size: 40px; text-align: center; color: #000;'))
            . '</div>'
    );
    $pForm->AddMergedRow(
//            $pForm->GetInputButtonHTML('cancel', _('Anuluj'), array('style' => 'font-size: 40px; float:left;', 'onclick' => 'window.close();'), 'button') .
            $pForm->GetInputButtonHTML('go_back', _('Pomiń / ОТЛОЖИТЬ'), array('style' => 'font-size: 40px; float:right;'), 'button')
    );

    $sInput = '
    <script type="text/javascript" src="js/CoolEctor.js?date=20181109"></script>';
    $sInput .= $this->getOverrideJS();

    return (!empty($this->sMsg) ? $this->sMsg : '') . $sInput . ShowTable($pForm->ShowForm());
  }

// end of getProductsScanner() method

  /**
   * 
   * @return string
   */
  public function getMsg() {
    return $this->sMsg;
  }

}
