<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_przyjecia;

use Admin;
use Common;
use Doctrine\ORM\EntityManager;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;
use View;

/**
 * Description of Module_empty_localization
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_przyjecia__empty_location implements ModuleEntity {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;

  /**
   *
   * @var string - komunikat
   */
  private $sMsg;

  /**
   * @var Admin
   */
  public $oClassRepository;
  private $sModule;

  public function __construct(Admin $oClassRepository) {

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }

  /**
   * 
   * @return html
   */
  public function doDefault() {
    if (!empty($_POST) && isset($_POST['location'])) {
      $this->saveLocation($_POST['location']);
    }
    return $this->Show(); 
  }

  /**
   * 
   * @param string $sLocation
   * @return int
   */
  private function checkContainer($sLocation) {
    $oContainers = new \magazine\Containers($this->pDbMgr, 2);
    return $oContainers->checkContainer($sLocation);
  }
  
  /**
   * 
   * @param array $aLocation
   * @return bool
   */
  private function saveLocation($aLocation) {
    $bIsErr = false;
    $iCount = 0;
    foreach ($aLocation as $iProductId => $sLocation) {
      if ($sLocation != '') {
        $iCount = 1;
        if ($this->checkContainer($sLocation) === -2) {
          $this->sMsg .= GetMessage(_('Podana lokalizacja nie jest prawidłowa'));
          $bIsErr = true;
          break;
        }
        if ($this->updateLocationProductOnG($iProductId, $sLocation) === false) {
          $bIsErr = true;
          break;
        }
      }
    }
    if ($iCount > 0) {
      if ($bIsErr === true) {
        $this->sMsg .= GetMessage(_('Błąd przypisywania lokalizacji'));
        return false;
      } else {
        $this->sMsg .= GetMessage(_('Lokalizacja zostala przypisana'), false);
        return true;
      }
    } else {
      return true;
    }
  }
  
  
  /**
   * 
   * @param int $iProductId
   * @param string $sLocation
   * @return bool
   */
  private function updateLocationProductOnG($iProductId, $sLocation) {
    $aValues = ['profit_g_location' => $sLocation];
    return $this->pDbMgr->Update('profit24', 'products_stock', $aValues, ' id = '.$iProductId);
  }
  
  public function Show() {
    global $aConfig;
		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);
    
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM products_stock
						 WHERE profit_g_act_stock > 0 AND profit_g_location = '' AND (profit_x_location = '' OR  profit_x_location IS NULL) ";
		$iRowCount = intval($this->pDbMgr->GetOne('profit24', $sSql));

		$aHeader = array(
			'header'	=> sprintf(_('Lista nie przypisanych produktów do lokalizacji (%s)'), $iRowCount),
			'refresh'	=> false,
			'search'	=> false,
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'photo',
				'content'	=> _('Okładka'),
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'profit_g_act_stock',
				'content'	=> _('Stan na Stock'),
				'sortable'	=> false,
				'width'	=> '85'
			),
			array(
				'db_field'	=> 'name',
				'content'	=> _('Tytuł'),
				'sortable'	=> false,
				'width'	=> '145'
			),
      array(
				'db_field'	=> 'publisher',
				'content'	=> _('Wydawnictwo'),
				'sortable'	=> false,
				'width'	=> '85'
			),
      array(
				'db_field'	=> 'publication_year',
				'content'	=> _('Rok wydania'),
				'sortable'	=> false,
				'width'	=> '85'
			),
			array(
				'db_field'	=> 'ean_13',
				'content'	=> _('Indeksy'),
				'sortable'	=> false,
				'width'	=> '85'
			),
      array(
				'db_field'	=> 'shelf',
				'content'	=> _('Półka'),
				'sortable'	=> false,
				'width'	=> '85'
			),
		);

		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(id)
							 FROM products_stock
               WHERE profit_g_act_stock > 0 AND profit_g_location = '' AND (profit_x_location = '' OR  profit_x_location IS NULL) ";
			$iRowCount = intval($this->pDbMgr->GetOne('profit24', $sSql));
		}
		$pView = new View('products_statuses', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
    
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT PS.id, '' AS photo, PS.profit_g_act_stock
							 FROM products_stock AS PS
							 WHERE 
                profit_g_act_stock > 0 AND profit_g_location = '' AND (profit_x_location = '' OR  profit_x_location IS NULL)  ".
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' PS.profit_g_act_stock DESC ' ).
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& $this->pDbMgr->GetAll('profit24', $sSql);
      foreach ($aRecords as $iKey => $aRecord) {
        $aRow = $this->getProductData($aRecord['id']);
        $aRecords[$iKey] = array_merge($aRecord, $aRow);
        $aRecords[$iKey]['shelf'] = $this->getShelfInput($aRecord['id']);
        $aRecords[$iKey]['profit_g_act_stock'] = '<span style="font-size: 20px; font-weight: bold;">'.$aRecord['profit_g_act_stock'].'</span>';
        $aRecords[$iKey]['name'] = '<span style="font-size: 15px; font-weight: bold;">'.$aRow['name'].'</span>';
      }
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
			);
      $pView->AddRecords($aRecords, $aColSettings);
    }
    $aArr = array();
    $pView->AddRecordsFooter($aArr);
    
    $sAddHTML = $this->getAddHTML();
    return $pView->Show().$sAddHTML;
  }
  
  /**
   * 
   * @return string
   */
  private function getAddHTML() {
    $sSubmit = '<input onclick="$(\'#products_statuses\').submit()" name="Zapissz" type="submit" style="font-size: 20px; padding:10px;" > ';
    $sJS = '
<script type="text/javascript">
  $(".location").keypress(function (e) {
      var key = e.which;
      if(key == 13) {
        $(this).parent().parent().next().find(".location").focus();
      }
  });
</script>
      ';
    return $sSubmit.$sJS; 
  }
  
  /**
   * 
   * @param int $iId
   * @return string
   */
  private function getShelfInput($iId) {
    return '<input class="location" style="font-size: 25px; width: 180px;" type="text" name="location['.$iId.']" />';
  }
  
  
  /**
   * 
   * @param int $iPId
   * @return array
   */
  private function getProductData($iPId) {
    global $aConfig;
    
    $sSql = 'SELECT (SELECT CONCAT(directory, "/", photo) FROM products_images AS PI WHERE PI.product_id = P.id LIMIT 1) AS photo,
                      P.name, 
                      PP.name AS publisher,
                      P.publication_year,
                      P.isbn_plain, P.ean_13, P.isbn_13, P.isbn_10
             FROM products AS P
             JOIN products_publishers AS PP
                ON PP.id = P.publisher_id
             WHERE P.id = '.$iPId;
    $aRow = $this->pDbMgr->GetRow('profit24', $sSql);
    $aRow['photo'] = '<img src="https://www.profit24.pl/'.$aConfig['common']['photo_dir'].'/'.$aRow['photo'].'" />';
    $aRow['ean_13'] = $aRow['isbn_plain'].', '.$aRow['ean_13'].', '.$aRow['isbn_13'].', '.$aRow['isbn_10'];
    unset($aRow['isbn_plain'], $aRow['isbn_13'], $aRow['isbn_10']);
    return $aRow;
  }

  public function getMsg() {
    return $this->sMsg;
  }
}
