<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_przyjecia;

use Admin;
use DatabaseManager;
use FormTable;
use magazine\Containers;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;
use Validator;

/**
 * Description of Module
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_przyjecia implements ModuleEntity {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;

  /**
   *
   * @var string - komunikat
   */
  private $sMsg;

  /**
   * @var Admin
   */
  public $oClassRepository;
  private $sModule;

  public function __construct(Admin $oClassRepository) {

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }

  public function doDefault() {
    // wczytaj identyfikator książki
    return $this->getProductIdentView();

    // wyświetl info o lokalizacji, wraz z mośliwością zmiany lokalizacji
  }

  /**
   * 
   * @return string
   */
  public function doGetProductLocation() {
    if (empty($_POST)) {
      $this->sMsg = _('Pusty zestaw danych');
      return $this->doDefault();
    }
    
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			return $this->doDefault();
		}
    
    $sProdIdent = $_POST['product_ident'];
    $aProduct = $this->findProductByIdent($sProdIdent);
    if (!empty($aProduct)) {
      // ok mamy produkt
      if (trim($aProduct['location']) != '') {
        return $this->ShowEditProductlocationView($aProduct['id'], $aProduct['profit_g_act_stock'], $aProduct['location']);
      } else {
        return $this->GetNewProductlocationView($aProduct['id'], $aProduct['profit_g_act_stock'], $aProduct['name']);
      }
    } else {
      $this->sMsg .= GetMessage(sprintf(_('Wybrany produkt "%s" nie znajduje się w naszej bazie danych / ДАННОГО КОДА НЕТ В НАШЕЙ БАЗЕ ДАННЫХ'), $sProdIdent));
      return $this->getProductIdentView();
    }
  }
  
  /**
   * 
   * @param int $iProductId
   * @param string $sProductName
   * @return string html
   */
  private function GetNewProductlocationView($iProductId, $iActStock, $sProductName) {
    
    $sHeader = sprintf(_('Wskaż lokalizację książki %s (%d)'), $sProductName, $iActStock);
    
    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('ShowEditProductlocationView', $sHeader, array('action' => phpSelf(array('id' => $iProductId, 'do' => 'saveProductLocalization'))), array('col_width' => 155), false);
    $oForm->AddMergedRow($sHeader, array('class' => 'bigMerged'));
    $oForm->AddText('location', _('Półka'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $oForm->AddInputButton('save', _('Zapisz'), array('style' => 'font-size: 25'));
    return $oForm->ShowForm();
  }

  private function getProductsStockData($iProductId)
  {
      return $this->pDbMgr->GetRow('profit24', "SELECT profit_g_location, profit_x_location FROM products_stock WHERE id = $iProductId");
  }

  /**
   * 
   * @param int $iProductId
   * @param string $sProductlocation
   * @return string - html
   */
  private function ShowEditProductlocationView($iProductId, $iActStock, $sProductlocation) {

    $locations = '<span class="location-style">'.$sProductlocation.'</span>';

    $sHeader = sprintf(_('Połóż produkt <br /> na półce %s (%s)'), $locations, $iActStock);
    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('ShowEditProductlocationView', $sHeader, array('action' => phpSelf(array('id' => $iProductId))), array('col_width' => 155), false);
    $oForm->AddMergedRow($sHeader, array('class' => 'bigMerged'));
    $oForm->AddHidden('do', 'confirmProductLocalization');
    $oForm->AddHidden('veryfication_location', $sProductlocation);
    $oForm->AddText('location', _('Półka'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $sSave = $oForm->GetInputButtonHTML('save', _('Zapisz'), array('style' => 'font-size: 25;'));
    $sChange = $oForm->GetInputButtonHTML('change_location', _('Wskaż lokalizację'),
            array('style' => 'font-size: 25; padding: 20px; margin: 20px;', 'onclick' => '$(\'#do\').val(\'changeLocalization\'); $(\'#ShowEditProductlocationView\').submit();'), 'button');
//    $sClearStock = $oForm->GetInputButtonHTML('clear_stock_location', _('Wyczyść lokalizację'),
//            array('style' => 'font-size: 25; padding: 20px; margin: 20px;', 'onclick' => '$(\'#do\').val(\'clearStockLocation\'); $(\'#ShowEditProductlocationView\').submit();'), 'button');
    
    if (preg_match('/ \d+/', $sProductlocation)) {
      $sClearHighStock = $oForm->GetInputButtonHTML('clear_high_stock_location', _('Wyczyść wysokiego składowania'),
              array('style' => 'font-size: 25; padding: 20px; margin: 20px;', 'onclick' => '$(\'#do\').val(\'clearHighStockLocation\'); $(\'#ShowEditProductlocationView\').submit();'), 'button');    
    }
    
    if (preg_match('/^\d+/', $sProductlocation)) {
      $sClearStock = $oForm->GetInputButtonHTML('clear_stock_location', _('Wyczyść lokalizację'),
            array('style' => 'font-size: 25; padding: 20px; margin: 20px;', 'onclick' => '$(\'#do\').val(\'clearStockLocation\'); $(\'#ShowEditProductlocationView\').submit();'), 'button');
    }

    $oForm->AddMergedRow('<div style="float: left; margin-left: 155px;"> &nbsp; '.$sChange.'</div>'.$sClearStock.' &nbsp;  &nbsp; &nbsp; '.$sClearHighStock);
    return $oForm->ShowForm();
  }
  
  /**
   * 
   * @return type
   */
  public function doClearStockLocation() {
    $iProductId = $_GET['id'];
    
    if (false === $this->clearStockLocation($iProductId, false)) {
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas czyszczenia lokalizacji'));
    } else {
      $this->sMsg .= GetMessage(_('Lokalizacja stockowa została wyczyszczona.'), false);
    }
    return $this->doDefault();
  }
  
  /**
   * 
   */
  public function doClearHighStockLocation() {
    $iProductId = $_GET['id'];
    
    if (false === $this->clearStockLocation($iProductId, true)) {
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas czyszczenia lokalizacji'));
    } else {
      $this->sMsg .= GetMessage(_('Lokalizacja stockowa została wyczyszczona.'), false);
    }
    return $this->doDefault();
  }
  
  /**
   * 
   * @param int $iProductId
   * @param bool $bIsHighStock
   * @return bool
   */
  private function clearStockLocation($iProductId, $bIsHighStock) {
    
    if ($bIsHighStock === true) {
      $sCol = 'profit_x_location';
    } else {
      $sCol = 'profit_g_location';
    }
    return $this->pDbMgr->Update('profit24', 'products_stock', [$sCol => ''], ' id = '.$iProductId. ' LIMIT 1');
  }
  
  
  /**
   * @retun string HTML
   */
  public function doChangeLocalization() {
    $iProductId = $_GET['id'];
    $sLocation = $_POST['veryfication_location'];

    $sHeader = sprintf(_('Wczytaj nową półkę książki'));
    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('ShowEditProductlocationView', $sHeader, array('action' => phpSelf(array('do' => 'SaveProductLocalization', 'id' => $iProductId))), array('col_width' => 155), false);
    $oForm->AddMergedRow($sHeader, array('class' => 'bigMerged'));
    $oForm->AddText('location', _('Półka'), '', array('placeholder' => $sLocation, 'style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $oForm->AddInputButton('save', _('Zapisz'), array('style' => 'font-size: 25'));
    return $oForm->ShowForm();
  }

  private function getNonEmptyLocations($productsStockData)
  {
      $newLocations = [];

      foreach($productsStockData as $location) {
          if(!empty($location)) {
            $newLocations[] = $location;
          }
      }

      return $newLocations;
  }

  /**
   * 
   * @return string - HTML
   */
  public function doConfirmProductLocalization() {
    $iProductId = $_GET['id'];
    $sLocation = $_POST['location'];
    $sVeryfiLocation = $_POST['veryfication_location'];

    $productsStockData = $this->getProductsStockData($iProductId);
    $locations = $this->getNonEmptyLocations($productsStockData);

    if (true === in_array($sLocation, $locations)) {
      $this->sMsg .= GetMessage(_('Produkt został potwiedzony na lokalizacji'), false);
    } else {
      $this->sMsg .= GetMessage(sprintf(_('Wystąpił błąd podczas potwierdzania lokalizacji, 
                                wskanowana lokalizacja jest różna od wskazanej %s <> %s'),$sLocation , $sVeryfiLocation));
    }
    return $this->doDefault();
  }
  
  /**
   * @return string HTML
   */
  public function doSaveProductLocalization() {
    $iProductId = $_GET['id'];
    $sLocation = $_POST['location'];
    
    $result = $this->updateProductLocation($iProductId, $sLocation);
    
    if ($result === -2) {
       $this->getProductIdentView();
       $this->sMsg .= GetMessage(_('Wybrano nieprawidłową lokalizację'));
       return $this->doDefault();
     }  
    
    if ($result === false) {
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas aktualizacji lokalizacji produktu'));
    } else {
      $this->sMsg .= GetMessage(_('Ustawiono lokalizację produktu '. $sLocation), false);
    }
    return $this->doDefault();
  }
  
  /**
   * 
   * @param type $iProductId
   * @param type $sLocation
   * @return boolean|-2
   */
  public function updateProductLocation($iProductId, $sLocation) {
    
    $type = [3,2];
    $oContainers = new Containers($this->pDbMgr, $type);
    return $oContainers->updateProductLocation($iProductId, $sLocation);
  }

  /**
   * 
   * @param string $sProdIdent
   * @return array
   */
  private function findProductByIdent($sProdIdent) {

    $sSql = 'SELECT PS.id, CONCAT(PS.profit_g_location, " ", PS.profit_x_location) as location, PS.profit_g_act_stock, P.name
             FROM products AS P
             JOIN products_stock AS PS
              ON P.id = PS.id
             WHERE 
                (
                  P.isbn_plain = "%1$s" OR 
                  P.isbn_10 = "%1$s" OR 
                  P.isbn_13 = "%1$s" OR 
                  P.ean_13 = "%1$s"
                )'; // OR streamsoft_indeks = "%1$s"
    $sSql = sprintf($sSql, $sProdIdent);
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }

  /**
   * 
   * @return string HTML
   */
  private function getProductIdentView() {

    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('scan_product_ident', _('Wczytaj produkt na magazyn Stock / ПРИПИШИТЕ ПРОДУКТ НА РЕЗЕРВНОМ СКЛАДЕ'), array('action' => phpSelf(array('do' => 'getProductLocation'))), array('col_width' => 155), false);
    $oForm->AddText('product_ident', _('EAN / КОД КНИГИ'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
    return $oForm->ShowForm();
  }
  

  /**
   * 
   * @param string $sLocation
   * @return int
   */
  private function getContainerType($sLocation) {

    $type = [3,2];
    $oContainers = new Containers($this->pDbMgr, $type);
    return $oContainers->checkContainer($sLocation);
  }
  
  /**
   * 
   * @param int $type
   * @return string
   */
  private function getProductsStockColByContainerType($type) {
    
    switch ($type) {
      case '2':
        return 'profit_g_location';
      case '3':
        return 'profit_x_location';
    }
  }

  public function getMsg() {
    
    $sJS = '<script>
      $(function() {
        var komunikat = $(".message1").html();
        if (komunikat !== undefined && komunikat !== "") {
          new Messi(komunikat, {title: "Błąd", modal: true});
        }
      });
      </script>';
    return $this->sMsg.$sJS;
  }
}
