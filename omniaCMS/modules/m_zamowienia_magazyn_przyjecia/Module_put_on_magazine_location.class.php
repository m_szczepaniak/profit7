<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_przyjecia;

use Admin;
use DatabaseManager;
use Exception;
use FormTable;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\EntityManager\Entites\StockLocation;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use magazine\Containers;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;
use Validator;
use View\touchKeyboard;

/**
 * Description of Module
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_przyjecia__put_on_magazine_location implements ModuleEntity
{

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;

  /**
   *
   * @var string - komunikat
   */
  private $sMsg;

  /**
   * @var Admin
   */
  public $oClassRepository;
  private $sModule;

  public function __construct(Admin $oClassRepository)
  {

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }

  public function doDefault()
  {
    // wczytaj identyfikator książki
    return $this->getProductIdentView();

    // wyświetl info o lokalizacji, wraz z mośliwością zmiany lokalizacji
  }

  /**
   *
   * @return string
   */
  public function doGetProductLocation($productId, $quantity)
  {
    if (empty($_POST)) {
      $this->sMsg = _('Pusty zestaw danych');
      return $this->doDefault();
    }

    // dolaczenie klasy walidatora
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    if (!$oValidator->Validate($_POST)) {
      $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
      // wystapil blad wypelnienia formularza
      // wyswietlenie formularza wraz z komunikatem bledu
      return $this->doDefault();
    }

    return $this->ShowEditProductlocationView($productId, $quantity);
    /*
    // szukamy z dostawy 10 egz. do przyjęcia
    $supplies = $this->findOrdersItemsListsDocuments($productId, $quantity);
    if (false === $supplies) {
      $this->sMsg .= GetMessage(_('Brak wystarczającej ilości produktu w dostawach.'));
    } else {
    }
    */
  }

  /**
   *
   * @param int $iProductId
   * @param string $sProductName
   * @return string html
   */
  private function GetNewProductlocationView($iProductId, $iActStock, $sProductName)
  {

    $sHeader = sprintf(_('Wskaż lokalizację książki %s (%d)'), $sProductName, $iActStock);

    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('ShowEditProductlocationView', $sHeader, array('action' => phpSelf(array('id' => $iProductId, 'do' => 'saveProductLocalization'))), array('col_width' => 155), false);
    $oForm->AddMergedRow($sHeader, array('class' => 'bigMerged'));
    $oForm->AddText('location', _('Półka'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $oForm->AddInputButton('save', _('Zapisz'), array('style' => 'font-size: 25'));
    return $oForm->ShowForm();
  }

  private function getProductsStockData($iProductId)
  {
    return $this->pDbMgr->GetRow('profit24', "SELECT profit_g_location, profit_x_location FROM products_stock WHERE id = $iProductId");
  }

  /**
   *
   * @param int $iProductId
   * @param string $sProductlocation
   * @return string - html
   */
  private function ShowEditProductlocationView($productId, $quantity)
  {
    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('ShowEditProductlocationView', _('Podaj lokalizację'), array('action' => phpSelf(array('id' => $productId, 'quantity' => $quantity))), array('col_width' => 155), false);
    $oForm->AddText('location', _('Półka'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $oForm->AddHidden('do', 'confirmProductLocalization');
    $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
    return $oForm->ShowForm();
  }

  /**
   *
   * @return type
   */
  public function doClearStockLocation()
  {
    $iProductId = $_GET['id'];

    if (false === $this->clearStockLocation($iProductId, false)) {
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas czyszczenia lokalizacji'));
    } else {
      $this->sMsg .= GetMessage(_('Lokalizacja stockowa została wyczyszczona.'), false);
    }
    return $this->doDefault();
  }

  /**
   *
   */
  public function doClearHighStockLocation()
  {
    $iProductId = $_GET['id'];

    if (false === $this->clearStockLocation($iProductId, true)) {
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas czyszczenia lokalizacji'));
    } else {
      $this->sMsg .= GetMessage(_('Lokalizacja stockowa została wyczyszczona.'), false);
    }
    return $this->doDefault();
  }

  /**
   *
   * @param int $iProductId
   * @param bool $bIsHighStock
   * @return bool
   */
  private function clearStockLocation($iProductId, $bIsHighStock)
  {

    if ($bIsHighStock === true) {
      $sCol = 'profit_x_location';
    } else {
      $sCol = 'profit_g_location';
    }
    return $this->pDbMgr->Update('profit24', 'products_stock', [$sCol => ''], ' id = ' . $iProductId . ' LIMIT 1');
  }


  /**
   * @retun string HTML
   */
  public function doChangeLocalization()
  {
    $iProductId = $_GET['id'];
    $sLocation = $_POST['veryfication_location'];

    $sHeader = sprintf(_('Wczytaj nową półkę książki'));
    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('ShowEditProductlocationView', $sHeader, array('action' => phpSelf(array('do' => 'SaveProductLocalization', 'id' => $iProductId))), array('col_width' => 155), false);
    $oForm->AddMergedRow($sHeader, array('class' => 'bigMerged'));
    $oForm->AddText('location', _('Półka'), '', array('placeholder' => $sLocation, 'style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $oForm->AddInputButton('save', _('Zapisz'), array('style' => 'font-size: 25'));
    return $oForm->ShowForm();
  }

  private function getNonEmptyLocations($productsStockData)
  {
    $newLocations = [];

    foreach ($productsStockData as $location) {
      if (!empty($location)) {
        $newLocations[] = $location;
      }
    }

    return $newLocations;
  }

  /**
   *
   * @return string - HTML
   */
  public function doConfirmProductLocalization()
  {
    $productId = $_GET['id'];
    $quantity = $_GET['quantity'];
    $location = $_POST['location'];

      $this->pDbMgr->BeginTransaction('profit24');

    $ordersItemsLists = new OrdersItemsLists($this->pDbMgr);
    $ordersItemsListsItems = $ordersItemsLists->findOrdersItemsListsDocuments($productId, $quantity);
    if (false === $ordersItemsListsItems) {
        // sprawdzamy ilość na stocku i odejmujemy od ilości na naszych lokalizacjach

        try {
            if (true === $this->tryPutOnLocationsByDiff($productId, $quantity, $location)) {
                $this->pDbMgr->CommitTransaction('profit24');
                $msg = _('Produkt o id ' . $productId . ' został przyjęty na lokalizacje ' . $location . ' w ilości ' . $quantity.' na podstawie różnicy w ilościach na stanach magazynowych');
                AddLog($msg, false);
                $this->sMsg .= GetMessage($msg, false);
            } else {
                $msg = _('Brak wystarczającej ilości produktu w dostawach. / КОЛИЧЕСТВО ПРИПИСАННЫХ ЭКЗЕМПЛЯРОВ НЕ СОВПАДАЕТ С СИСТЕМОЙ');
                AddLog($msg);
                $this->sMsg .= GetMessage($msg);
                $this->pDbMgr->RollbackTransaction('profit24');
            }
        } catch (Exception $ex) {
            $msg = _('Wystąpił błąd - '.$ex->getMessage());
            AddLog($msg);
            $this->sMsg .= GetMessage($msg);
            $this->pDbMgr->RollbackTransaction('profit24');
        }

        $this->stockLocationLog($productId, $location, $quantity);
    } else {
      try {
        $stockLocationOrdersItemsListsItems = new StockLocationOrdersItemsListsItems();
        $stockLocationOrdersItemsListsItems->addByOrdersItemsLists($ordersItemsListsItems, $productId, $location, StockLocationOrdersItemsListsItems::TYPE_PZ);
        $this->pDbMgr->CommitTransaction('profit24');
        $msg = _('Produkt o id ' . $productId . ' został przyjęty na lokalizacje ' . $location . ' w ilości ' . $quantity);
        AddLog($msg, false);
        $this->sMsg .= GetMessage($msg, false);
      } catch (Exception $exception) {
        $msg = $exception->getMessage();
        $this->sMsg .= GetMessage($msg);
        AddLog($msg);
        $this->pDbMgr->RollbackTransaction('profit24');
      }

      foreach ($ordersItemsListsItems as $item) {
          if ($item['OILI_ID'] > 0) {
              $this->stockLocationLog($productId, $location, $item['used_quantity'], $item['OILI_ID']);
          }
      }
    }

    return $this->doDefault();
  }

  private function stockLocationLog($productId, $containerId, $quantity, $OILI_ID = null) {
      $aValues = [
          'user_id' => $_SESSION['user']['id'],
          'product_id' => $productId,
          'container_id' => $containerId,
          'quantity' => $quantity,
          'orders_items_lists_item_id' => $OILI_ID
      ];

    $this->pDbMgr->Insert('profit24', 'products_stock_locations_log', $aValues);
  }

    /**
     * @param $productId
     * @param $quantity
     * @return bool
     */
  private function tryPutOnLocationsByDiff($productId, $quantity, $location) {
      $sSql = 'SELECT profit_g_act_stock
               FROM products_stock
               WHERE id = '.$productId;
      $erpQuantity = $this->pDbMgr->GetOne('profit24', $sSql);

      $stockLocationQuantity = $this->getProductMagazineQuantity($productId);
      if (($erpQuantity - intval($stockLocationQuantity)) == $quantity) {
          // wrzucamy na lokalizacje
          $stockLocation = new StockLocation($this->pDbMgr);
          return $stockLocation->incAddStockLocationQuantity($productId, $quantity, $location);
      } else {
          return false;
      }
  }

    /**
     * @param $productId
     * @param $magazine
     * @return mixed
     */
    public function getProductMagazineQuantity($productId) {

        $sSql = '
    SELECT SUM(quantity) 
    FROM stock_location AS SL
    WHERE SL.products_stock_id = '.$productId.'
    ';
        return intval($this->pDbMgr->GetOne('profit24', $sSql));
    }


  /**
   * @return string HTML
   */
  public function doSaveProductLocalization()
  {
    $iProductId = $_GET['id'];
    $sLocation = $_POST['location'];

    $result = $this->updateProductLocation($iProductId, $sLocation);

    if ($result === -2) {
      $this->getProductIdentView();
      $this->sMsg .= GetMessage(_('Wybrano nieprawidłową lokalizację'));
      return $this->doDefault();
    }

    if ($result === false) {
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas aktualizacji lokalizacji produktu'));
    } else {
      $this->sMsg .= GetMessage(_('Ustawiono lokalizację produktu ' . $sLocation), false);
    }
    return $this->doDefault();
  }

  /**
   *
   * @param type $iProductId
   * @param type $sLocation
   * @return boolean|-2
   */
  public function updateProductLocation($iProductId, $sLocation)
  {

    $type = [3, 2];
    $oContainers = new Containers($this->pDbMgr, $type);
    return $oContainers->updateProductLocation($iProductId, $sLocation);
  }

  /**
   *
   * @param string $sProdIdent
   * @return array
   */
  private function findProductByIdent($sProdIdent)
  {

    $sSql = 'SELECT PS.id, CONCAT(PS.profit_g_location, " ", PS.profit_x_location) AS location, PS.profit_g_act_stock, P.name, P.standard_code, P.standard_quantity
             FROM products AS P
             JOIN products_stock AS PS
              ON P.id = PS.id
             WHERE 
                (
                  P.isbn_plain = "%1$s" OR 
                  P.isbn_10 = "%1$s" OR 
                  P.isbn_13 = "%1$s" OR 
                  P.ean_13 = "%1$s" OR 
                  P.standard_code = "%1$s" 
                )'; // OR streamsoft_indeks = "%1$s"
    $sSql = sprintf($sSql, $sProdIdent);
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }

  /**
   *
   * @return string HTML
   */
  private function getProductIdentView()
  {

    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('scan_product_ident', _('Wczytaj produkt na magazyn Stock / ПРИПИШИТЕ ПРОДУКТ НА РЕЗЕРВНОМ СКЛАДЕ'), array('action' => phpSelf(array('do' => 'getProductQuantity'))), array('col_width' => 155), false);
    $oForm->AddText('product_ident', _('EAN / КОД КНИГИ'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
    return $oForm->ShowForm();
  }

  private function validateGetProduct()
  {
    if (empty($_POST)) {
      $this->sMsg = _('Pusty zestaw danych');
      return false;
    }

    // dolaczenie klasy walidatora
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    if (!$oValidator->Validate($_POST)) {
      $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
      // wystapil blad wypelnienia formularza
      // wyswietlenie formularza wraz z komunikatem bledu
      return false;
    }

    $sProdIdent = $_POST['product_ident'];
    $aProduct = $this->findProductByIdent($sProdIdent);
    if (!empty($aProduct)) {
      // ok mamy produkt
      return $aProduct;
//      if (trim($aProduct['location']) != '') {
//        return $this->ShowEditProductlocationView($aProduct['id'], $aProduct['profit_g_act_stock'], $aProduct['location']);
//      } else {
//        return $this->GetNewProductlocationView($aProduct['id'], $aProduct['profit_g_act_stock'], $aProduct['name']);
//      }
    } else {
      $this->sMsg .= GetMessage(sprintf(_('Wybrany produkt "%s" nie znajduje się w naszej bazie danych / ДАННОГО КОДА НЕТ В НАШЕЙ БАЗЕ ДАННЫХ'), $sProdIdent));
      return false;
    }
  }

  public function doGetProductQuantity()
  {

    $aProduct = $this->validateGetProduct();
    if (false === $aProduct) {
      return $this->doDefault();
    }

    $bStandard = false;

    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('scan_product_ident_get_quantity', _('Podaj ilość produktu / ВВЕДИТЕ КОЛ-ВО'), array('action' => phpSelf(array('do' => 'saveProductQuantity'))), array('col_width' => 155), false);
    if ($aProduct['standard_code'] == $_POST['product_ident'] && intval($aProduct['standard_quantity']) > 0) {
      // jeśli skanowane standardem
      $oForm->AddHidden('standard_quantity', $aProduct['standard_quantity']);
      $bStandard = true;
    }
    $oForm->AddText('quantity', $bStandard == true ? _('Liczba standardów') : _('Ilość'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'uint');
    $oForm->AddHidden('product_id', $aProduct['id']);

    $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));

    $touchKeyboard = new touchKeyboard($this->pSmarty);
    $touchKeyboardHTML = $touchKeyboard->getKeyboardNumeric();

    return $oForm->ShowForm().$touchKeyboardHTML;
  }

  public function doSaveProductQuantity()
  {
    if (empty($_POST)) {
      $this->sMsg = _('Pusty zestaw danych');
      return $this->doDefault();
    }

    // dolaczenie klasy walidatora
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    if (!$oValidator->Validate($_POST)) {
      $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
      // wystapil blad wypelnienia formularza
      // wyswietlenie formularza wraz z komunikatem bledu
      return $this->doDefault();
    } else {
        $multiplyStandard = 1;
        if (isset($_POST['standard_quantity']) && intval($_POST['standard_quantity']) > 0) {
            $multiplyStandard = $_POST['standard_quantity'];
        }
      $quantity = $_POST['quantity'] * $multiplyStandard;
      $productId = $_POST['product_id'];
      return $this->doGetProductLocation($productId, $quantity);
    }
  }


  /**
   *
   * @param string $sLocation
   * @return int
   */
  private function getContainerType($sLocation)
  {

    $type = [3, 2];
    $oContainers = new Containers($this->pDbMgr, $type);
    return $oContainers->checkContainer($sLocation);
  }

  /**
   *
   * @param int $type
   * @return string
   */
  private function getProductsStockColByContainerType($type)
  {

    switch ($type) {
      case '2':
        return 'profit_g_location';
      case '3':
        return 'profit_x_location';
    }
  }

  public function getMsg()
  {

    $sJS = '<script>
      $(function() {
        var komunikat = $(".message1").html();
        if (komunikat !== undefined && komunikat !== "") {
          new Messi(komunikat, {title: "Błąd", modal: true});
        }
      });
      </script>';
    return $this->sMsg . $sJS;
  }
}
