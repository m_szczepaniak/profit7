<?php

class Module
{
    private $pSmarty;

    public function __construct(&$pSmarty)
    {
        $this->pSmarty = $pSmarty;

        switch (isset($_GET['do'])) {

            default:
                $this->showListAction();
                break;
        }
    }

    public function showListAction()
    {
        global $aConfig, $pDbMgr;

        rememberViewState($this->sModule);

        $aHeader = array(
            'header' => 'Lista produktów bez wagi',
            //            'header' => $aConfig['lang'][$this->sModule]['list'],
            'refresh' => true,
            'bigSearch' => true,
            'search' => true
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 0,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'content'	=> '&nbsp;',
                'width'	=> '20'
            ),
            array(
                'content'	=> _('Id'),
                'width' => '30'
            ),
            array(
                'content'	=> _('isbn'),
                'width' => '150'
            ),
            array(
                'content'	=> _('Nazwa')
            ),
            array(
                'content'	=> _('Lokalizacja 1')
            ),
            array(
                'content'	=> _('Lokalizacja 2')
            ),
            array(
                'edit_product'	=> _('Edytuj produkt')
            )
        );

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $iRowCount = $pDbMgr->GetOne('profit24', "
        SELECT
        count(DISTINCT P.id)
        FROM products AS P
        JOIN products_stock AS PS ON P.id = PS.id
        JOIN orders_items AS OI ON P.id = OI.product_id
        JOIN orders AS O ON O.id = OI.order_id AND O.order_status NOT IN('4','5')
        where
        (
          OI.weight = 0 OR OI.weight is null
        )
        AND OI.source = '51'
        ORDER BY O.order_date
        ");

        $pView = new View('group_discount', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);
        $iCurrentPage = $pView->AddPager($iRowCount);

        $aConfig['lang'][$this->sModule]['no_items'] = _('Brak elementów');

        $perPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];

        $iStartFrom = ($iCurrentPage - 1) * $perPage;
        if (null == $iCurrentPage){
            $iStartFrom = 0;
        }

        $aUsers = $pDbMgr->GetAll('profit24', "
        SELECT
        P.id, P.isbn_plain, P.name, PS.profit_g_location, PS.profit_x_location
        FROM products AS P
        JOIN products_stock AS PS ON P.id = PS.id
        JOIN orders_items AS OI ON P.id = OI.product_id
        JOIN orders AS O ON O.id = OI.order_id AND O.order_status NOT IN('4','5')
        where
        (
          OI.weight = 0 OR OI.weight is null
        )
        AND OI.source = '51'
        GROUP BY P.id
        ORDER BY O.order_date DESC

        limit $iStartFrom, $perPage
        ");

        $backUri = urlencode(str_replace(["/", 'omniaCMS'], '', $_SERVER['REQUEST_URI']));

        foreach($aUsers as &$element) {

            $input = '<a href="%s" title="Edytuj"><img src="gfx/icons/edit_ico.gif" alt="Edytuj" border="0"></a>';
            $link = "admin.php?frame=main&module_id=26&module=m_oferta_produktowa&do=edit&id=".$element['id'].'&back_uri='.$backUri;
//            $link = str_replace('&action=', '', phpSelf(array('action' => false, 'do' => 'edit', 'id' => $element['id'], 'back_uri' => $backUri)));

            $element['edit_product'] = sprintf($input, $link);

        }

        // ustawienia dla poszczegolnych kolumn rekordu
        $aColSettings = array();
        // dodanie rekordow do widoku
        $pView->AddRecords($aUsers, $aColSettings);
        // dodanie stopki do widoku

        $aRecordsFooter = array(

        );

        $pView->AddRecordsFooter($aRecordsFooter);
        // przepisanie langa - info o braku rekordow


        $this->pSmarty->assign('sContent', $pView->Show());
    }
}