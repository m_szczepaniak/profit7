<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2016-02-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_raporty;

use Admin;
use DatabaseManager;
use Exception;
use FormTable;
use omniaCMS\lib\interfaces\ModuleEntity;

/**
 * Description of Module_inventory
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__raporty__inventory implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected $sMsg;

  /**
   *
   * @var Smarty
   */
  public $pSmarty;

  /**
   *
   * @var string
   */
  public $sModule;

  protected $maxScannedShelfQuantity = 2;

  /**
   *
   * @var DatabaseManager
   */
  protected $pDbMgr;

  public function __construct(Admin $oClassRepository) {
    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }

  public function doDefault() {
    // wczytaj półkę 
    return $this->getShelf();
  }

  /**
   * 
   * @return string
   */
  private function getShelf() {
    if (isset($_POST) && !empty($_POST)) {
      $aData = $_POST;
      try {
        $this->validateCanScanShelf($aData['shelf']);
        return $this->doScanProducts($aData['shelf']);
      } catch (Exception $ex) {
        $this->sMsg .= GetMessage($ex->getMessage());
      }
    }
    include_once('Form/FormTable.class.php');
    $form = new FormTable('get_shelf', _('Wprowadź nr Półki'));
    $form->AddHidden('do', 'default');
    $form->AddText('shelf', _('Półka'), $aData['shelf'], array('autofocus' => 'autofocus', 'style' => 'font-size: 40px;'), '', 'uint', true);
    $form->AddInputButton("submit", _('Dalej'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');

    return $form->ShowForm();
  }

  /**
   * 
   * @param string $shelf
   * @return boolean
   * @throws Exception
   */
  private function validateCanScanShelf($shelf) {

    $this->validateShelf($shelf);


    $sSql = 'SELECT COUNT(id) FROM inventory WHERE container_id = "' . $shelf . '" ';
    $iScannedShelfQuantity = $this->pDbMgr->GetOne('profit24', $sSql);
    if ($iScannedShelfQuantity >= $this->maxScannedShelfQuantity) {
      throw new Exception(sprintf('Półka nr "' . $shelf . '" została zeskanowana już %s razy', $iScannedShelfQuantity));
    }
	
    $sSql = 'SELECT COUNT(id) FROM inventory WHERE container_id = "' . $shelf . '" AND created_by = "'.$_SESSION['user']['name'].'"';
    $iScannedShelfQuantity = $this->pDbMgr->GetOne('profit24', $sSql);
    if ($iScannedShelfQuantity >= 1) {
      throw new Exception(sprintf('Półka nr "' . $shelf . '" została zeskanowana już %s razy przez użytkownika %s', $iScannedShelfQuantity, $_SESSION['user']['name']));
    }
    return true;
  }

    /**
     * @param $shelf
     * @throws Exception
     */
  protected function validateShelf($shelf) {

      $sSql = 'SELECT id FROM containers WHERE id = "' . $shelf . '" AND id LIKE "8%" ';
      $isContainer = $this->pDbMgr->GetOne('profit24', $sSql);
      if ($isContainer <= 0) {
          throw new Exception('Brak półki o nr "' . $shelf . '" lub nieprawidłowa półka');
      }
  }

  /**
   * 
   * @return string
   */
  public function doScanProducts($iShelf) {

    include_once('Form/FormTable.class.php');
    $form = new FormTable('scan_products', _('Skanuj produkty'));
    $form->AddHidden('do', 'saveProducts');
    $form->AddHidden('shelf', $iShelf);
    $form->AddHidden('sc_date', date('YmdHis'));
    
    $form->AddMergedRow('', array('id' => 'sc_products', 'readonly' => 'readonly', 'style' => 'font-size:15px; width: 90%; height: 300px', 'class' => 'editable_input'));
    $sCheckbox = $form->GetCheckBoxHTML('ask_quantity', _('Zapytaj o ilość'), [], '', false, false);
    $sButton = $form->GetInputButtonHTML("submit", _('Dalej'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
    $form->AddMergedRow(_('Zapytaj o ilość:'). $sCheckbox.' &nbsp; &nbsp; &nbsp;'.$sButton);

    $input = '
    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" /></div>
    <script type="text/javascript" src="js/alertify.min.js?date=20160302"></script>
    <script type="text/javascript" src="js/inventory.js?date=20160302"></script>';
    return $input . ShowTable($form->ShowForm());
  }

  /**
   * 
   */
  public function doSaveProducts() {

    $iShelf = $_POST['shelf'];
    $aProducts = $_SESSION['inventory'][$_POST['sc_date']];


    $this->pDbMgr->BeginTransaction('profit24');

    $iInventoryId = $this->addInventory($iShelf);
    try {
		if (empty($aProducts)) {
			throw new Exception('Nie można zapisać pustej półki');
		}
      foreach ($aProducts as $aProduct) {
        $this->addInventoryItems($iInventoryId, $aProduct);
      }
      $this->defineInventoryStatus($iShelf);
    } catch (Exception $ex) {
      $this->sMsg .= GetMessage('Wystąpił błąd podczas zapistwania listy: ' . $ex->getMessage());
      $this->pDbMgr->RollbackTransaction('profit24');
      return $this->doScanProducts($iShelf);
    }
    $this->sMsg .= GetMessage('Zapisno półkę "' . $iShelf . '" ze składowymi ', false);
    $this->pDbMgr->CommitTransaction('profit24');
    unset($_POST);
    return $this->doDefault();
  }

  /**
   * 
   * @param int $iShelf
   * @return boolean
   */
  public function defineInventoryStatus($iShelf) {

    $sSql = 'SELECT id FROM inventory WHERE container_id = "' . $iShelf . '"';
    $aInventories = $this->pDbMgr->GetCol('profit24', $sSql);
    if (count($aInventories) > 0) {
      $cStatus = $this->getInventoryStatus($aInventories, $iShelf);
      $aValues = ['status' => $cStatus];
      $this->pDbMgr->Update('profit24', 'inventory', $aValues, ' container_id = "' . $iShelf . '" ');
    } else {
      return true;
    }
  }

  /**
   * 
   * @return string
   */
  protected function getInventoryStatus($aInventories, $iShelf) {
    if (count($aInventories) == 1) {
      return 'S'; // single
    }

    $sSql = 'SELECT product_id, quantity FROM inventory_items WHERE inventory_id = ' . $aInventories[0] . ' ORDER BY product_id DESC';
    $firstElements = md5(json_encode($this->pDbMgr->GetAll('profit24', $sSql)));

    $sSql = 'SELECT product_id, quantity FROM inventory_items WHERE inventory_id = ' . $aInventories[1] . ' ORDER BY product_id DESC';
    $secondElements = md5(json_encode($this->pDbMgr->GetAll('profit24', $sSql)));
    if ($firstElements === $secondElements) {
      return 'E'; // equal
    } else {
      return 'N'; // not equal
    }
  }

  /**
   * 
   * @param int $iShelf
   */
  private function addInventory($iShelf) {

    $aValues = [
        'container_id' => $iShelf,
        'created' => 'NOW()',
        'created_by' => $_SESSION['user']['name']
    ];
    return $this->pDbMgr->Insert('profit24', 'inventory', $aValues);
  }

  /**
   * 
   * @param array $aProduct
   * @return array
   */
  private function addInventoryItems($iInventoryId, $aProduct) {

    $aValues = [
        'product_id' => $aProduct['id'],
        'inventory_id' => $iInventoryId,
        'quantity' => $aProduct['quantity'],
        'orginally_scanned' => $aProduct['quantity'],
        'scanned_by' => $_SESSION['user']['name'],
        'scanned' => 'NOW()'
    ];
    return $this->pDbMgr->Insert('profit24', 'inventory_items', $aValues);
  }

  /**
   * 
   * @return string
   */
  public function getMsg() {
    return $this->sMsg;
  }

}
