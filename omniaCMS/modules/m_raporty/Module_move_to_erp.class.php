<?php
/**
 * Klasa generowania dokumentu DBF z przesunięciem magazynowym z G 
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-12 
 * @copyrights Marcin Chudy - Profit24.pl
 */
set_time_limit('3600');
ini_set('memory_limit', '1G');
class Module__raporty__move_to_erp {
  
  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  private $sDBFFilePath = 'import/XML/';
  
  private $sFileSavePath = 'import/DBF/move_to_erp';
  
  private $aAllDBFData;
  
  private $dmHi;
  
  private $aCols = array (
      0 => 
      array ('TOW_USL', 'N', 1, 0
      ),      1 => 
      array ('NAZWA_ART', 'C', 35, 0
      ),      2 => 
      array (
        'OPIS_ART','C',35,0,      
      ),      3 => 
      array (
        'NR_HANDL','C',20,0,      
      ),      4 => 
      array (
        'NR_KATAL','C',20,0,      
      ),  5 => 
      array (
        'PLU','N',6,0,
      ),      6 => 
      array (
        'BARKOD','C',20,0,
      ),      7 => 
      array (
        'IDENT_KAT','N',8,0,
      ),      8 => 
      array (
        'STAN','N',11,3,
      ),      9 => 
      array (
        'STAN_FAKT','N',11,3,
      ),      10 => 
      array (
        'STAN_MIN','N',11,3,
      ),      11 => 
      array (
        'STAN_MAX','N',11,3,
      ),      12 => 
      array (
        'CENA','N',11,2,
      ),      13 => 
      array (
        'CENAX','N',11,2,
      ),      14 => 
      array (
        'NARZUT_A','N',8,4,
      ),      15 => 
      array (
        'CENA_A','N',11,2,
      ),      16 => 
      array (
        'CENA_AX','N',11,2,
      ),      17 => 
      array (
        'UPUST_B','N',6,2,
      ),      18 => 
      array (
        'CENA_B','N',11,2,
      ),      19 => 
      array (
        'CENA_BX','N',11,2,
      ),      20 => 
      array (
        'UPUST_C','N',6,2,
      ),      21 => 
      array (
        'CENA_C','N',11,2,
      ),      22 => 
      array (
        'CENA_CX','N',11,2,
      ),      23 => 
      array (
        'CENAEWID','N',11,2,
      ),      24 => 
      array (
        'ZAMOWIENIA','N',11,3,
      ),      25 => 
      array (
        'ZAMREZER','N',11,3,
      ),      26 => 
      array (
        'ZAMOWDOST','N',11,3,
      ),      27 => 
      array (
        'CENAWALWE','N',15,4,
      ),      28 => 
      array (
        'NARZUTWAL','N',8,4,
      ),      29 => 
      array (
        'CENAWALWY','N',15,4,
      ),      30 => 
      array (
        'JEDN_M','C',7,0,
      ),      31 => 
      array (
        'JM_PODZIEL','L',1,0,
      ),      32 => 
      array (
        'STAWKA_VAT','N',5,2,
      ),      33 => 
      array (
        'ST_VAT_ZAK','N',5,2,
      ),      34 => 
      array (
        'SWW_KU','C',10,0,
      ),      35 => 
      array (
        'PKWIU','C',14,0,
      ),      36 => 
      array (
        'PCN','C',9,0,
      ),      37 => 
      array (
        'WAL_KOD','C',3,0,
      ),      38 => 
      array (
        'WAL_KURS','N',12,6,
      ),      39 => 
      array (
        'WAL_KURSS','N',12,6,
      ),      40 => 
      array (
        'WAL_DATA','D',8,0,
      ),      41 => 
      array (
        'CLO','N',6,2,      
      ),      42 => 
      array (
        'TRANSPORT','N',6,2,      
      ),      43 => 
      array (
        'AKCYZA_PR','N',6,2,
      ),      44 => 
      array (
        'AKCYZA','N',9,2,
      ),      45 => 
      array (
        'PODATEK','N',6,2,
      ),      46 => 
      array (
        'KOSZT_PROC','N',6,2,
      ),      47 => 
      array (
        'CERTYFIKAT','L',1,0,
      ),      48 => 
      array (
        'CERTYFOPIS','C',35,0,
      ),      49 => 
      array (
        'CERTYFDATA','D',8,0,
      ),      50 => 
      array (
        'PROD_MNOZ','N',11,3,
      ),      51 => 
      array (
        'LOKACJA','C',15,0,
      ),      52 => 
      array (
        'WAGA_JM_KG','N',10,4,
      ),      53 => 
      array (
        'ID_KAT','N',4,0,
      ),      54 => 
      array (
        'NAZWA_KAT','C',25,0,
      ),      55 => 
      array (
        'KSTAWKAVAT','N',6,2,
      ),      56 => 
      array (
        'CENAWE','N',13,2,
      ),      57 => 
      array (
        'CENAWEX','N',13,2,
      ),      58 => 
      array (
        'DATA_WAZN','D',8,0,
      ),      59 => 
      array (
        'NR_SERII','C',10,0,
      ),      60 => 
      array (
        'STAWKAVATP','N',5,2,
      ),      61 => 
      array (
        'DATA','D',8,0,
      ),      62 => 
      array (
        'ILOSC','N',11,3,      
      ),      63 => 
      array (
        'deleted','L',1,0
      )
    );
  
  
  function __construct(&$pSmarty) {
    
    $this->dmHi = date('dmHi');

		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];
		$this->iPageId = getModulePageId('m_konta');
    
		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
    
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    switch ($sDo) {
      default:
      case 'show':
        $this->Show($pSmarty);
      break;
      case 'get_dbf':
        $this->getDBF($pSmarty);
      break;
    }
  }
  
  /**
   * Wyświetlamy formularz
   * 
   * @param \Smarty $pSmarty
   */
  public function Show(&$pSmarty) {
    global $aConfig;
    
    include_once('Form/FormTable.class.php');
    $oForm = new \FormTable('get_dbf_form', _('Pobierz plik przesunięcia DBF z Stock'));
    $oForm->AddHidden('do', 'get_dbf');
    $oForm->AddInputButton('get_dbf', _('Pobierz plik DBF'));
    
    $oForm->AddMergedRow(_('Przesunięcie z G na BAZA'), array('class'=>'merged'));

    $aFiles = glob($aConfig['common']['client_base_path'].'/'.$this->sFileSavePath.'/*.*');
    
    foreach ($aFiles as $sFilenamePath) {
      $sFilename = str_replace($aConfig['common']['client_base_path'], '', $sFilenamePath);
      $sURLFile = $aConfig['common']['client_base_url_https'].$sFilename;

      $oForm->AddRow(sprintf(_('  %s '), str_replace('/'.$this->sFileSavePath.'/', '', $sFilename)), '<a href="'.$sURLFile.'">'._('Pobierz').'</a>');
    }
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') .ShowTable($oForm->ShowForm()));
  }// end of Show() method
  
  
  /**
   * Metoda pobiera produkty do przesunięcia z magazynu Stock
   * 
   * @global \DatabaseManager $pDbMgr
   * @return array
   */
  private function getProductsToMove() {
    global $pDbMgr;
    
    $sSql = 'SELECT P.*, GROUP_CONCAT(PSR.id SEPARATOR ",") AS ids, SUM(PSR.quantity) as quantity,
              IF ((
              SELECT id FROM orders_items AS OI WHERE PSR.orders_items_id = OI.parent_id AND OI.item_type = "A" LIMIT 1
              ) > 0, 1, 0) is_attachment
             FROM products_stock_reservations AS PSR
             JOIN products AS P
              ON PSR.products_stock_id = P.id
             WHERE move_to_erp = "1"
             GROUP BY P.id
             ';
    return $pDbMgr->GetAll('profit24', $sSql);
  }// end of getProductsToMove() method

  
  /**
   * Metoda pobiera z DBF isbn i zwraca, dopasowane rekordy na podstawie przesłanego pliku
   * 
   * @return array
   * @global \DatabaseManager $pDbMgr
   */
  private function getDBF(&$pSmarty) {
    global $pDbMgr, $aConfig;
    
    $bIsErr = false;
    $sPath = $aConfig['common']['client_base_path'].'/'.$this->sFileSavePath.'/'.$this->dmHi.'/';
    
    if (!file_exists($sPath)) {
      mkdir($sPath);
      
      $sFilePathDBF = $sPath.$this->dmHi.'.dbf';
      $sFilePathPDF = $sPath.$this->dmHi.'.PDF';

      $this->aAllDBFData =& $this->getDBFArray();
      $iDB = dbase_create($sFilePathDBF, $this->aCols);

      $aRecords = $this->getProductsToMove();
      foreach ($aRecords as $aRecord) {
        $aRow = array();
        // sprawdzamy
        // wg mnie jeśli brak pozycji to powinniśmy przerwać, 
        // nie powinno tu być dodeawania bo to błąd krytyczny
        $aRows = $this->checkGetItemRow($aRecord['isbn_plain'], $aRecord['quantity']);
        if (empty($aRows) || $aRows === false) {
          $aRows = $this->checkGetItemRow($aRecord['isbn_10'], $aRecord['quantity']);
        }
        if (empty($aRows) || $aRows === false) {
          $aRows = $this->checkGetItemRow($aRecord['isbn_13'], $aRecord['quantity']);
        }
        if (empty($aRows) || $aRows === false) {
          $aRows = $this->checkGetItemRow($aRecord['ean_13'], $aRecord['quantity']);
        }
        if (!empty($aRows)) {
          foreach ($aRows as $aRow) {
            dbase_add_record($iDB, array_values($aRow));
          }
        } else {
          // błąd krytyczny
          $sMsg .= sprintf(_(' W ERP brak pozycji o ISBN: %s EAN: %s'), $aRecord['isbn_plain'], $aRecord['ean_13']).' <br />';
          $bIsErr = true;
        }
      }

      if ($bIsErr === false) {
        // ok lecimy ze zmianą w DB
        $pDbMgr->BeginTransaction('profit24');
        try {
          foreach ($aRecords as $aRecord) {
            if ($this->removeFromReservation($aRecord['ids']) === FALSE) {
              throw new Exception('Wystąpił błąd podczas usuwania rezerwacji z produktu: '.print_r($aRecord, true));
            }
          }
          dbase_close($iDB);
          $this->createPDF($pSmarty, $aRecords, $sFilePathPDF);
          $this->getZipFile($sPath, $sFilePathDBF, $sFilePathPDF);
          $pDbMgr->CommitTransaction('profit24');
          exit(0);
        } catch (\Exception $ex) {
          $sMsg .= $ex->getMessage();
          $pDbMgr->RollbackTransaction('profit24');
          $bIsErr = true;
        }
      }

      if ($bIsErr === true) {
        $this->sMsg = GetMessage($sMsg);
        AddLog($sMsg);
        $this->Show($pSmarty);
      }
    } else {
      $sMsg .= _('Plik został wygenerowany już wcześniej');
      $this->sMsg = GetMessage($sMsg);
      AddLog($sMsg);
      $this->Show($pSmarty);
    }
  }
  
  /**
   * Pobieramy DBFa
   * 
   * @param string $sPath
   * @throws Exception
   */
  private function getZipFile($sPath, $sFilePathDBF, $sFilePathPDF) {
    
    $sFilePathZIP = $this->compresDir($sPath, $sFilePathDBF, $sFilePathPDF);
    if (file_exists($sFilePathZIP)) {
      $_GET['hideHeader'] = '1';
      header('Content-Encoding: UTF-8');
      header("Content-type: application/octet-stream; charset=UTF-8");
      header("Content-Disposition:attachment;filename='".$this->dmHi.".zip");
      echo file_get_contents($sFilePathZIP);

    } else {
      throw new Exception(_('Wystąpił błąd podczas pobierania DBFa'));
    }
  }// end of getZipFile() method
  
  
  /**
   * Kompresujemy pliki
   * 
   * @global array $aConfig
   * @param type $sPath
   * @param type $sFilePathDBF
   * @param type $sFilePathPDF
   * @return string
   */
  private function compresDir($sPath, $sFilePathDBF, $sFilePathPDF) {
    global $aConfig;
    
    $sZipCurrDate = $this->dmHi;
    $sFileZipName = $sZipCurrDate.'.zip';
    $sDBFZip = $aConfig['common']['client_base_path'].'/'.$this->sFileSavePath;
    
    exec('cd '.$sDBFZip.' && zip -r "'.$sFileZipName.'" '.$sZipCurrDate.'  & echo $!');

    // usuwamy stare pliki
    if (file_exists($sFilePathPDF)) {
      unlink($sFilePathPDF);
    }
    if (file_exists($sFilePathDBF)) {
      unlink($sFilePathDBF);
    }
    rmdir($sPath);
    return $sDBFZip.'/'.$sFileZipName;
  }// end of compressAndDelOld() method
  
  
  /**
   * Tworzymy plik PDF
   * 
   * @param \Smarty $pSmarty
   * @param array $aRecords
   * @param string $sFilePathPDF
   * @return bool
   */
  private function createPDF(&$pSmarty, $aRecords, $sFilePathPDF) {
    
    require_once('tcpdf/config/lang/pl.php');
    require_once('tcpdf/tcpdf.php');		

    // create new PDF document
    $pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('profit24');
    $pdf->SetTitle(_('Przesunięcie z G na BAZA z dnia '.date('d-m-Y')));
    $pdf->SetSubject(_('Przesunięcie z G na BAZA z dnia '.date('d-m-Y')));
    $pdf->SetKeywords('');
    $pdf->setPrintHeader(false);

    // set header and footer fonts
    //$pdf->setHeaderFont(Array('freesans', '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    //set margins
    $pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    //set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, 10);

    //set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 

    //set some language-dependent strings
    $l = array();
    $pdf->setLanguageArray($l); 

    $sHtml = $this->getMoveToERP($pSmarty, $aRecords);


    // set font
    $pdf->SetFont('freesans', '', 10);

    // add a page
    $pdf->AddPage();
    $pdf->writeHTML($sHtml, true, false, false, false, '');
    return $pdf->Output($sFilePathPDF, 'F');
  }
  
  
  /**
   * Metoda przesuwa towar na ERP
   * 
   * @param \Smarty $pSmarty
   * @param array $aRecords
   * @return html
   */
  private function getMoveToERP(&$pSmarty, $aRecords) {
    
    $pSmarty->assign_by_ref('aRecords', $aRecords);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'move_to_erp.tpl');
		$pSmarty->clear_assign('aRecords', $aRecords);
    return $sHtml;
  }// end of getMoveToERP() method
  
  
  /**
   * Metoda sprawdza, czy istnieje ten isbn w WF-Mag'u
   * 
   * @param type $sIdent
   * @param type $iQuantity
   */
  private function checkGetItemRow( $sIdent, $iQuantity) {
    
    if ($sIdent != '' && isset($this->aAllDBFData[$sIdent])) {
      return $this->getItemRow($this->aAllDBFData[$sIdent], $iQuantity);
    }
  }// end of checkGetItemRow() method
  
  
  /**
   * Metoda pobiera rekord, jeśli jest wystarczająca ilość
   * 
   * @param array $aRowsQuantity
   * @param int $iQuantity
   * @return array
   */
  private function getItemRow(&$aRowsQuantity, $iQuantity) {
    $aSourceRows = $aRowsQuantity;
    $aTMPITems = array();
    
    $iTMPSumQuantity = 0;
    krsort($aRowsQuantity);
    $iNowItemQuantity = 0;
    
    foreach($aRowsQuantity as $iRQuantity => $aItems) {
      foreach ($aItems as $iKey => $aItem) {
        $iNowItemQuantity += $iRQuantity;
        if ($iQuantity <= $iNowItemQuantity) {
          // ilość nam się zgadza na tym rekordzie
          $iLeftQuantity = $iNowItemQuantity - $iQuantity;
          if ($iLeftQuantity > 0) {
            $aRowsQuantity[$iNowItemQuantity - $iQuantity] = $aItems;
          }
          unset($aRowsQuantity[$iNowItemQuantity][$iKey]);
          if (!empty($aRowsQuantity[$iNowItemQuantity])) {
            $array = array();
            $aRowsQuantity[$iNowItemQuantity] = array_merge($array, $aRowsQuantity[$iNowItemQuantity]);
          }
          $iCurrentItemToConfirm = ($iRQuantity - $iLeftQuantity);
          $aTMPITems[] = $this->prepareItemDataQuantity($aItems[$iKey], $iCurrentItemToConfirm);
          return $aTMPITems;
        } else {
          // ok bierzemy taką ilośc jaka jest
          $iTMPSumQuantity += $iNowItemQuantity;
          $aTMPITems[] = $this->prepareItemDataQuantity($aItems[$iKey], $iRQuantity);
          unset($aItems[$iKey]);
        }
      }
    }
    // nie udało się nic nie usuwamy
    $aRowsQuantity = $aSourceRows;
    return false;
  }// end of getItemRow() method
  
  
  /**
   * Metoda przygotowuje dane składowej
   * 
   * @param array $aRow
   * @param int $iRowQuantity
   * @return array
   */
  private function prepareItemDataQuantity($aRow, $iRowQuantity) {
    
    $aRow['ILOSC'] = $iRowQuantity;
    $aRow['STAN'] = '';
    $aRow['STAN_FAKT'] = '';
    $aRow['DATA'] = date('Ymd'); 
    return $aRow;
  }// end of prepareItemDataQuantity() method
  
  
  /**
   * Metoda usuwa rezerwacje
   * 
   * @global \DatabaseManager $pDbMgr
   * @param int $iReservationId
   * @return bool
   */
  private function removeFromReservation($sReservationIds) {
    global $pDbMgr;
    
    $sSql = 'DELETE FROM products_stock_reservations WHERE id IN ('.$sReservationIds.')';
    return $pDbMgr->Query('profit24', $sSql);
  }// end of removeFromReservation() method
  
  
  /**
   * Metoda zrzuca cały plik DBF do tablicy
   * 
   * @global array $aConfig
   * @return array
   */
  private function getDBFArray() {
    global $aConfig;
    $aReturnArray = array();
    
    $aAllowedCols = array();
    foreach ($this->aCols as $iKey => $aCol) {
      $aAllowedCols[] = $aCol[0];
    }
    
//    $iDBF = dbase_open($aConfig['common']['client_base_path'].'import/XML/artykuly_n.dbf','0');
    $iDBF = dbase_open($aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath.'artykuly_g.dbf','0');
    
    $iCountRecords = dbase_numrecords($iDBF);
    for ($i = 1; $i <= $iCountRecords; $i++) {
      $aRecord = array();
      $aRecordTMP = dbase_get_record_with_names($iDBF, $i);
      foreach ($aRecordTMP as &$sCol) {
        $sCol = trim($sCol);
      }

      foreach ($aAllowedCols as $aACol) {
        if (!isset($aRecordTMP[$aACol])) {
          $aRecord[$aACol] = '';
        } else {
          $aRecord[$aACol] = $aRecordTMP[$aACol];
        }
      }
      if ($aRecord['STAN_FAKT'] > 0) {
        $iQuantity = $aRecord['STAN_FAKT'];
        $aReturnArray[$aRecordTMP['CERTYFOPIS']][$iQuantity][] = $aRecord;
      }
    }
    /*
     * Debug
     * 
    foreach ($aReturnArray as $aItems) {
      if (count($aItems) > 1) {
        dump($aItems);
      }
    }
    die;
    */
    return $aReturnArray;
  }//  end of getDBFArray() method
}// end of class
