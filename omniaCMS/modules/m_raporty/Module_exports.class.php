<?php
/**
 * Klasa raportów do eksportu gotowych zapytań do XLS
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
set_time_limit('3600');
ini_set('memory_limit', '1G');
class Module {
  
  const FIRST_AVAILABLE = TRUE;
  
  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  private $sDBFFilePath = 'import/DBF/';
  private $aCols = array (
      0 => 
      array ('TOW_USL', 'N', 1, 0
      ),      1 => 
      array ('NAZWA_ART', 'C', 35, 0
      ),      2 => 
      array (
        'OPIS_ART','C',35,0,      
      ),      3 => 
      array (
        'NR_HANDL','C',20,0,      
      ),      4 => 
      array (
        'NR_KATAL','C',20,0,      
      ),  5 => 
      array (
        'PLU','N',6,0,
      ),      6 => 
      array (
        'BARKOD','C',20,0,
      ),      7 => 
      array (
        'IDENT_KAT','N',8,0,
      ),      8 => 
      array (
        'STAN','N',11,3,
      ),      9 => 
      array (
        'STAN_FAKT','N',11,3,
      ),      10 => 
      array (
        'STAN_MIN','N',11,3,
      ),      11 => 
      array (
        'STAN_MAX','N',11,3,
      ),      12 => 
      array (
        'CENA','N',11,2, // zakup netto
      ),      13 => 
      array (
        'CENAX','N',11,2, // zakup brutto
      ),      14 => 
      array (
        'NARZUT_A','N',8,4,
      ),      15 => 
      array (
        'CENA_A','N',11,2,
      ),      16 => 
      array (
        'CENA_AX','N',11,2,
      ),      17 => 
      array (
        'UPUST_B','N',6,2,
      ),      18 => 
      array (
        'CENA_B','N',11,2,
      ),      19 => 
      array (
        'CENA_BX','N',11,2,
      ),      20 => 
      array (
        'UPUST_C','N',6,2,
      ),      21 => 
      array (
        'CENA_C','N',11,2,
      ),      22 => 
      array (
        'CENA_CX','N',11,2,
      ),      23 => 
      array (
        'CENAEWID','N',11,2,
      ),      24 => 
      array (
        'ZAMOWIENIA','N',11,3,
      ),      25 => 
      array (
        'ZAMREZER','N',11,3,
      ),      26 => 
      array (
        'ZAMOWDOST','N',11,3,
      ),      27 => 
      array (
        'CENAWALWE','N',15,4,
      ),      28 => 
      array (
        'NARZUTWAL','N',8,4,
      ),      29 => 
      array (
        'CENAWALWY','N',15,4,
      ),      30 => 
      array (
        'JEDN_M','C',7,0,
      ),      31 => 
      array (
        'JM_PODZIEL','L',1,0,
      ),      32 => 
      array (
        'STAWKA_VAT','N',5,2,
      ),      33 => 
      array (
        'ST_VAT_ZAK','N',5,2,
      ),      34 => 
      array (
        'SWW_KU','C',10,0,
      ),      35 => 
      array (
        'PKWIU','C',14,0,
      ),      36 => 
      array (
        'PCN','C',9,0,
      ),      37 => 
      array (
        'WAL_KOD','C',3,0,
      ),      38 => 
      array (
        'WAL_KURS','N',12,6,
      ),      39 => 
      array (
        'WAL_KURSS','N',12,6,
      ),      40 => 
      array (
        'WAL_DATA','D',8,0,
      ),      41 => 
      array (
        'CLO','N',6,2,      
      ),      42 => 
      array (
        'TRANSPORT','N',6,2,      
      ),      43 => 
      array (
        'AKCYZA_PR','N',6,2,
      ),      44 => 
      array (
        'AKCYZA','N',9,2,
      ),      45 => 
      array (
        'PODATEK','N',6,2,
      ),      46 => 
      array (
        'KOSZT_PROC','N',6,2,
      ),      47 => 
      array (
        'CERTYFIKAT','L',1,0,
      ),      48 => 
      array (
        'CERTYFOPIS','C',35,0,
      ),      49 => 
      array (
        'CERTYFDATA','D',8,0,
      ),      50 => 
      array (
        'PROD_MNOZ','N',11,3,
      ),      51 => 
      array (
        'LOKACJA','C',15,0,
      ),      52 => 
      array (
        'WAGA_JM_KG','N',10,4,
      ),      53 => 
      array (
        'ID_KAT','N',4,0,
      ),      54 => 
      array (
        'NAZWA_KAT','C',25,0,
      ),      55 => 
      array (
        'KSTAWKAVAT','N',6,2,
      ),      56 => 
      array (
        'CENAWE','N',13,2,
      ),      57 => 
      array (
        'CENAWEX','N',13,2,
      ),      58 => 
      array (
        'DATA_WAZN','D',8,0,
      ),      59 => 
      array (
        'NR_SERII','C',10,0,
      ),      60 => 
      array (
        'STAWKAVATP','N',5,2,
      ),      61 => 
      array (
        'DATA','D',8,0,
      ),      62 => 
      array (
        'ILOSC','N',11,3,      
      ),      63 => 
      array (
        'deleted','L',1,0
      )
    );
  
  function __construct(&$pSmarty) {
    
		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];
		$this->iPageId = getModulePageId('m_konta');
    
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
    
		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
    
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    switch ($sDo) {
      case 'getCSV':
        $this->getCSV($pSmarty);
      break;
    
      case 'change_option':
      default: $this->Show($pSmarty); break;
    }
  }// end of __construct() method
  
  /**
   * Metoda wyświetla dostępne eksporty do wygenerowania
   * 
   * @global type $aConfig
   * @param type $pSmarty
   */
  public function Show(&$pSmarty) {
    global $aConfig;
    
    $aData = array();
		if (!empty($_POST)) {
			$aData = & $_POST;
		}
    
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('exports', $aConfig['lang'][$this->sModule]['exports'], array('action' => phpSelf(), 'enctype' => 'multipart/form-data'), array('col_width' => 205), $aConfig['common']['js_validation']);
    $pForm->AddHidden('do', 'getCSV');
    $pForm->AddSelect('export_option', $aConfig['lang'][$this->sModule]['export_option'], array('onchange'=>'document.getElementById(\'do\').value =\'change_option\'; this.form.submit();'), addDefaultValue($this->_getExportOptions()), $aData['export_option'], '', true);
    if (isset($aData['export_option'])) {
      $this->getFormOptions($pForm, $aData, $aData['export_option']);
    }
    
    // przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['download'], array()));
    
    $pForm->AddRow($aConfig['lang'][$this->sModule]['eksport_description_label'], nl2br($aConfig['lang'][$this->sModule]['eksport_description']));
    
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') .ShowTable($pForm->ShowForm()));
  }// end of Show() method
  
  

  /**
   * Metoda pobiera dostępne opcje eksportu
   * 
   * @return array
   */
  private function _getExportOptions() {
    global $aConfig;
    
    $aOptions = array(
        array('label' => $aConfig['lang'][$this->sModule]['eksport_1'], 'value' => '1'),
        array('label' => $aConfig['lang'][$this->sModule]['eksport_2'], 'value' => '2'),
        array('label' => $aConfig['lang'][$this->sModule]['eksport_3'], 'value' => '3'),
        array('label' => $aConfig['lang'][$this->sModule]['eksport_4'], 'value' => '4'),
        array('label' => $aConfig['lang'][$this->sModule]['eksport_5'], 'value' => '5'),
        array('label' => $aConfig['lang'][$this->sModule]['eksport_6'], 'value' => '6'),
        array('label' => $aConfig['lang'][$this->sModule]['eksport_7'], 'value' => '7'),
        array('label' => $aConfig['lang'][$this->sModule]['eksport_8'], 'value' => '8'),
        array('label' => $aConfig['lang'][$this->sModule]['eksport_9'], 'value' => '9'),
        array('label' => $aConfig['lang'][$this->sModule]['eksport_10'], 'value' => '10'),
    );
    return $aOptions;
  }// end of _getExportOptions() method
  
  
  /**
   * Metoda generuje plik CSV
   * 
   * @global type $aConfig
   * @param type $pSmarty
   * @return type
   */
  public function getCSV(&$pSmarty) {
    global $aConfig;
    
    if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Show($pSmarty);
			return;
		}
    
    if ($_POST['export_option'] > 0) {
      $aRetRows = $this->getRecords($_POST['export_option']);
      if (!empty($aRetRows)) {
        
        $_GET['hideHeader'] = '1';
        header('Content-Encoding: UTF-8');
        header("Content-type: text/html; charset=UTF-8");
        header("Content-Disposition:attachment;filename='export_".date('YmdHis').".xls'");
        echo "\xEF\xBB\xBF";
        $bFirst = true;
        foreach ($aRetRows AS $aRow) {
          if ($bFirst === TRUE) {
            // pierwsze przejście nazwy kolumn
            echo '"'.implode('"'.";".'"', array_keys($aRow)).'"'."\r\n";
          }
            
          $bFirst = false;
          echo '"'.implode('"'.";".'"', $aRow).'"'."\r\n";
        }
        die;
      } else {
        $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_items'], true);
        $this->Show($pSmarty);
      }
    } else {
      $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['err_no_option'], true);
      $this->Show($pSmarty);
    }
  }// end of getCSV() method
  
  
  /**
   * Metoda dodaje do formularze dodatkowe pola uzależnione od wybranej opcji eksportu
   * 
   * @param FormTable $pForm
   * @param type $iSelectOption
   */
  private function getFormOptions(&$pForm, $aData, $iSelectOption) {
    
    switch($iSelectOption) {
      default:
      break;
      case '4':
      case '3':
        $pForm->AddRow($pForm->GetLabelHTML('', _('Zakres dat')), '<div style="float: left; margin-top: 8px;">'.$pForm->GetLabelHTML('start_date', _('od'), false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('start_date', _('od'), $aData['start_date'], array('style'=>'width: 75px;'), '', 'date').'</div><div style="float: left; margin-top: 8px; padding-left: 25px;">'.$pForm->GetLabelHTML('end_date', _(' do:'), false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('end_date', _(' do:'), $aData['end_date'], array('style'=>'width: 75px;'), '', 'date').'</div>');
      break;
      case '5':
        $pForm->AddFile('file', _('Wybierz plik DBF'));
      break;
      case '6':
        $pForm->AddFile('file', _('Wybierz plik CSV rozdzielony średnikami ; '));
      break;
      case '7':
        $pForm->AddText('date', _('Data wysyłki, lub aktualna planowana data wysyłki:'), $aData['date'], array('style'=>'width: 75px;'), '', 'date', true);
      break;
      case '8':
        $pForm->AddText('date', _('Data:'), $aData['date'], array('style'=>'width: 75px;'), '', 'date', true);
      break;
    }
  }// end of getFormOptions() method
  
  
  /**
   * Metoda pobiera rekordy
   * 
   * @param int $iExportOption
   * @return array tablica danych do zapisania CSV
   */
  private function  getRecords($iExportOption) {
    switch ($iExportOption) {
      default:
      case '1':
      case '3':
        $sSql = $this->_getSQLExport($iExportOption);
        return Common::GetAll($sSql);
      break;
      case '4':
        $sSql = $this->_getSQLExport($iExportOption);
        $aExportData = $this->_parseExport_4(Common::GetAll($sSql));
        return $aExportData;
      case '5':
        // DBF
        return $this->_getDBFRecords($_FILES['file']['tmp_name']);
      break;
      case '6':
        // CSV
        return $this->_getCSVToDBFRecords($_FILES['file']['tmp_name']);
      break;
      case '2':
        // DBF
        return $this->_getEmptyCertfOpis();
      break;
      case '7':
        $sSql = $this->_getSQLExport($iExportOption);
        return Common::GetAll($sSql);
      break;
      case '8':
        return $this->_getLoginUsers();
      break;
      case '9':
        $sSql = $this->_getSQLExport($iExportOption);
        return Common::GetAll($sSql);
      break;
      case '10':
        $sSql = $this->_getSQLExport($iExportOption);
        return Common::GetAll($sSql);
      break;
    }
  }//end of getRecords() method
  
  
  /**
   * Metoda generuje tablicę na postawie schematu kolumn tabeli
   * 
   * @param array $aColsArra
   * @return string
   */
  private function _getEmptyArrColsDBF($aColsArra) {
    
    $aRow = array();
    foreach ($aColsArra as $aCol) {
      $aRow[$aCol[0]] = '';
    }
    return $aRow;
  }// end of _getEmptyArrColsDBF() method
  
  
  /**
   * Metoda pobiera z DBF isbn i zwraca, dopasowane rekordy na podstawie przesłanego pliku
   * 
   * @global array $aConfig
   * @return array
   */
  private function _getCSVToDBFRecords($sFilePath) {
    global $aConfig;
    $sTmpFilename = '/tmp/'.microtime().'.dbf';
    $aEmptyISBN = array();
    $aAllProducts = array();
    include_once ($_SERVER['DOCUMENT_ROOT'].'/import/CommonSynchro.class.php');
    $oCommonSynchro = new CommonSynchro();
    
    // iterujemy po pliku CSV
    $handle = fopen($sFilePath, "r");
    $contents = '';
    $aEmptyRecord = $this->_getEmptyArrColsDBF($this->aCols);
    
    $aAllDBF =& $this->getDBFArray();
    $iDB = dbase_create($sTmpFilename, $this->aCols);
    while (($buffer = fgets($handle, 4096)) !== false) {
      $contents = $buffer;
      $aItem = explode(';',$contents);
      $sCerfOpis = trim($aItem[0]);
      if ($sCerfOpis != '') {
        if (isset($aAllDBF[$sCerfOpis])) {
          $aRecord = $aAllDBF[$sCerfOpis];
          $aRecord['ILOSC'] = intval(trim($aItem[1]));
          $aRecord['STAN'] = '';
          $aRecord['STAN_FAKT'] = '';
          $aRecord['DATA'] = date('Ymd');
          dbase_add_record($iDB, array_values($aRecord));
        } else {
          //exit('Brak pozycji w DBF o CERTFOPIS = '.$sCerfOpis.' przerywamy');
          //die;
        }
      }
    }

    fclose($handle);
    
    $_GET['hideHeader'] = '1';
    header('Content-Encoding: UTF-8');
    header("Content-type: application/octet-stream; charset=UTF-8");
    header("Content-Disposition:attachment;filename='export_".date('YmdHis').".dbf");
    echo file_get_contents($sTmpFilename);
    unlink($sTmpFilename);
    dbase_close($iDB);
    exit(0);
  }
  
  /**
   * Metoda zrzuca cały plik DBF do tablicy
   * 
   * @global array $aConfig
   * @return array
   */
  private function getDBFArray() {
    global $aConfig;
    $aReturnArray = array();
    
    $aAllowedCols = array();
    foreach ($this->aCols as $iKey => $aCol) {
      $aAllowedCols[] = $aCol[0];
    }
    
//    $iDBF = dbase_open($aConfig['common']['client_base_path'].'import/XML/artykuly_n.dbf','0');
    $iDBF = dbase_open($aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath.'ARTYKULY_BAZA_DANYCH.DBF','0');
    
    $iCountRecords = dbase_numrecords($iDBF);
    for ($i = 1; $i <= $iCountRecords; $i++) {
      $aRecord = array();
      $aRecordTMP = dbase_get_record_with_names($iDBF, $i);
      foreach ($aRecordTMP as &$sCol) {
        $sCol = trim($sCol);
      }

      foreach ($aAllowedCols as $aACol) {
        if (!isset($aRecordTMP[$aACol])) {
          $aRecord[$aACol] = '';
        } else {
          $aRecord[$aACol] = $aRecordTMP[$aACol];
        }
      }
//      if (self::FIRST_AVAILABLE === TRUE) {
//        if (!isset($aReturnArray[$aRecordTMP['CERTYFOPIS']]) ||
//            (isset($aReturnArray[$aRecordTMP['CERTYFOPIS']]) && $aReturnArray[$aRecordTMP['CERTYFOPIS']]['STAN_FAKT'] < $aRecord['STAN_FAKT'])    || 
//            $aRecord['STAN_FAKT'] > 0
//           ) {
          $aReturnArray[$aRecordTMP['CERTYFOPIS']] = $aRecord;
//        }
//        if (!isset($aReturnArray[$aRecordTMP['BARKOD']]) ||
//            (isset($aReturnArray[$aRecordTMP['BARKOD']]) && $aReturnArray[$aRecordTMP['BARKOD']]['STAN_FAKT'] < $aRecord['STAN_FAKT'])    || 
//            $aRecord['STAN_FAKT'] > 0
//           ) {
//          $aReturnArray[$aRecordTMP['BARKOD']] = $aRecord;
//        }
//      } else {
//        $aReturnArray[$aRecordTMP['CERTYFOPIS']] = $aRecord;
//      }
    }
    return $aReturnArray;
  }//  end of getDBFArray() method
  
  
  /**
   * Metoda pobiera z DBF isbn i zwraca, dopasowane rekordy na podstawie przesłanego pliku
   * 
   * @global array $aConfig
   * @return array
   */
  private function _getDBFRecords($sFilePath) {
    global $aConfig;
    $aEmptyISBN = array();
    $aAllProducts = array();
    include_once ($_SERVER['DOCUMENT_ROOT'].'/import/CommonSynchro.class.php');
    $oCommonSynchro = new CommonSynchro();
    
    $iDBF = dbase_open($sFilePath,'0');
    $iCountRecords = dbase_numrecords($iDBF);
    for ($i = 1; $i <= $iCountRecords; $i++) {
      $aRecordTMP = dbase_get_record_with_names($iDBF, $i);
      foreach ($aRecordTMP AS &$sCol) {
        $sCol = trim($sCol);
      }
      
      if (!empty($aRecordTMP['CERTYFOPIS'])) {
        // pusty isbn i stan większy od zera
        $this->_parseDBFItem($oCommonSynchro, $aAllProducts, $aRecordTMP['CERTYFOPIS'], $aRecordTMP['ILOSC'], $aRecordTMP['CENA'], $aRecordTMP['CENAX']);
      }
    }
    
    if (!empty($aAllProducts)) {
      $_GET['hideHeader'] = '1';
      header('Content-Encoding: UTF-8');
      header("Content-type: text/html; charset=UTF-8");
      header("Content-Disposition:attachment;filename='export_".date('YmdHis').".xls'");
      
      echo "\xEF\xBB\xBF";
      $bFirst = TRUE;
      foreach ($aAllProducts as $aAProduct) {
        if ($bFirst === TRUE) {
          echo implode(';', array_keys($aAProduct))."\n";
          $bFirst = FALSE;
        }
        echo implode(';', $aAProduct)."\n";
      }
      die;
      
    }
    return false;
  }

  /**
   * Metoda parsuje
   * 
   * @param object ref $oCommonSynchro
   * @param object ref $aAllProducts
   * @param string $sPIsbn
   * @param float $fStan
   * @param float $fCena
   * @param float $fCenaX
   */
  private function _parseDBFItem(&$oCommonSynchro, &$aAllProducts, $sPIsbn, $fStan, $fCena, $fCenaX) {
    $sPIsbn = isbn2plain(trim($sPIsbn));
    $aProduct = array(
        'CERTYFOPIS' => $sPIsbn,
        'TYTUŁ' => '',
        'WYDAWNICTWO' => '',
        'ILOSC' => $fStan,
        'CENA' => Common::formatPrice($fCena),
        'CENAX' => Common::formatPrice($fCenaX),
        'AZYMUT BOOKINDEX' => ''
    );
    
    if (!isset($aAllProducts[$sPIsbn])) {
      // dodawanie nowego rekordu
      $fStan = intval(str_replace(',', '.', $fStan));

      $aProductPattern = array(
          'isbn_plain' => $sPIsbn
      );
      $aProductAdd = $oCommonSynchro->refMatchProductByISBNs($aProductPattern, array('id', 'name, publisher_id, azymut_index'));
      if (!empty($aProductAdd)) {
        if ($aProductAdd['publisher_id'] > 0) {
          $aProduct['WYDAWNICTWO'] = $oCommonSynchro->getProductPublisherName($aProductAdd['publisher_id']);
        }
        $aProduct['TYTUŁ'] = $aProductAdd['name'];
        $aProduct['AZYMUT BOOKINDEX'] = $aProductAdd['azymut_index'];
      }

      $aAllProducts[$sPIsbn] = $aProduct;
    } else {
      // sumowanie ilości
      $fStan = intval(str_replace(',', '.', $fStan));
      $aAllProducts[$sPIsbn]['ILOSC'] += $fStan;
    }
  }// end of _parseDBFItem() method
  
  
  /**
   * Metoda przetważa dane dla eksportu
   * 
   * @param array $aItems
   * @return string
   */
  private function _parseExport_4($aItems) {
    $aGroupBy = array();
    foreach ($aItems AS $iKey => $aItem) {
      if (empty($aItem['user_id'])) {
        $aItems[$iKey]['Imię'] = 'Ogółem';
        $aItems[$iKey]['Nazwisko'] = '-';
      }
      if (empty($aItem['Data nadania listu'])) {
        $aItems[$iKey]['Data nadania listu'] = 'Ogółem';
      }
      if (empty($aItem['user_id'])) {
        $aItems[$iKey]['Godzina'] = 'Ogółem';
      }
      $sTMPGroupBy =& $aGroupBy[$aItem['user_id'].$aItem['Data nadania listu']];
      $sTMPGroupBy['Suma Ilość zamówień'] += $aItem['Ilość zamówień'];
      $sTMPGroupBy['Suma Wysłanych pozycji'] += $aItem['Wysłanych pozycji'];
      $sTMPGroupBy['Suma Wysłanych egzemplarzy'] += $aItem['Wysłanych egzemplarzy'];
      $aItems[$iKey]['Suma Ilość zamówień'] = $sTMPGroupBy['Suma Ilość zamówień'];
      $aItems[$iKey]['Suma Wysłanych pozycji'] = $sTMPGroupBy['Suma Wysłanych pozycji'];
      $aItems[$iKey]['Suma Wysłanych egzemplarzy'] = $sTMPGroupBy['Suma Wysłanych egzemplarzy'];
      

      unset($aItems[$iKey]['zamówienia']);
      unset($aItems[$iKey]['user_id']);
    }
    return $aItems;
  }// end of _parseExport_4() method
  
  
  /**
   * Metoda pobiera rekordy z bazy danych poroduktów które nie posiadają isbn i stan jest większy od zera
   * 
   * @global array $aConfig
   * @return array
   */
  private function _getEmptyCertfOpis() {
    global $aConfig;
    $aEmptyISBN = array();
    
    $iDBF = dbase_open($aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath.'ARTYKULY_BAZA_DANYCH.DBF','0');
    $iCountRecords = dbase_numrecords($iDBF);
    for ($i = 1; $i <= $iCountRecords; $i++) {
      $aRecordTMP = dbase_get_record_with_names($iDBF, $i);
      foreach ($aRecordTMP AS &$sCol) {
        $sCol = trim($sCol);
      }
      if ($i == 1 || (empty($aRecordTMP['CERTYFOPIS']) && intval($aRecordTMP['STAN']) > 0)) {
        // pusty isbn i stan większy od zera
        $aEmptyISBN[] = $aRecordTMP;
      }
    }
    return $aEmptyISBN;
  }// end of _getEmptyCertfOpis() method
  
  
  /**
   * Metoda na podstawie przekazanego id eksportu zwraca zapytanie 
   *  odpowiadające temu eksportowi
   * 
   * @param int $iExport
   * @return boolean|string
   */
  private function _getSQLExport($iExport) {
    switch ($iExport) {
      default :
        return false;
      break;
      case '1':
        $sSql = "
        SELECT 
          OI.name AS Tytuł, 
          OI.isbn, 
          SUM(OI.quantity) AS Ilość, 
          OI.publisher AS Wydawca, 
          GROUP_CONCAT(O.order_number SEPARATOR ',') AS 'Numery zamówień'
        FROM orders_items AS OI
        JOIN  `orders` AS O ON OI.order_id = O.id
        WHERE O.order_status <> '3'
          AND O.order_status <> '4'
          AND O.order_status <> '5'
          AND OI.status = '4'
          AND OI.item_type <> 'A'
          AND OI.deleted <> '1'
        GROUP BY OI.isbn
        ORDER BY SUM(OI.quantity) DESC
        ";
      break;
      case '3':
      
        $sSql = "
        SELECT 
          DATE( OBS.created ) AS DATA_GL_PRZEL , 
          REPLACE(OBS.paid_amount, '.', ',') AS WART_GL_PRZEL, 
          DATE( O.`invoice_date` ) AS DATA_ZAFAKT, 
          REPLACE(SUM( OPL.ammount ), '.', ',') AS WART_PRZEL, 
          IF( website_id =  '1',  'Profit24.pl', IF( website_id =  '2',  'Ksiegarnia.IT',  IF( website_id =  '3',  'niePrzeczytane.pl',  '---' ) ) )  AS SERWIS,
          UCASE(OPL.`type`) as POBRANIE
        FROM  `orders_payments_list` AS OPL
        JOIN orders AS O ON O.id = OPL.order_id
        JOIN orders_linking_transport_numbers AS OLTN ON OLTN.order_id = OPL.order_id
        JOIN orders_bank_statements AS OBS ON OBS.id = OLTN.orders_bank_statement_id
        WHERE OBS.is_opek =  '1'
            AND (OPL.`type` =  'opek' OR OPL.`type` =  'tba')
            AND DATE( OBS.created )
            AND OBS.created >= '".formatDate($_POST['start_date'])." 00:00:00'
            AND OBS.created <= '".formatDate($_POST['end_date'])." 23:59:59'
        GROUP BY OBS.paid_amount, DATE( OBS.created ) , DATE( O.`invoice_date` ) , O.website_id, OPL.`type`
        ORDER BY DATE( OBS.created ) DESC 
        ";
      break;
      case '4':
        $sSql = "
          SELECT 
            OTU.user_id, 
            DATE(OTU.created) AS 'Data nadania listu', 
            HOUR(OTU.created) AS 'Godzina', 
            U.name AS Imię,
            U.surname AS Nazwisko,
            COUNT( DISTINCT OTU.order_id ) AS 'Ilość zamówień', 
            COUNT(OI.id) AS 'Wysłanych pozycji',
            SUM(OI.quantity) AS 'Wysłanych egzemplarzy'
          FROM orders_to_users AS OTU
          JOIN orders_items AS OI
            ON OTU.order_id = OI.order_id
          JOIN users AS U 
            ON U.id = OTU.user_id
          WHERE 
            OTU.created >= '".formatDate($_POST['start_date'])." 00:00:00'
            AND OTU.created <= '".formatDate($_POST['end_date'])." 23:59:59'
          GROUP BY 
            CONCAT(DATE(OTU.created), hour(OTU.created)),
            OTU.user_id
          WITH ROLLUP
          ";
      break;
      case '7':
        $sSql = '
          SELECT O.order_number
          FROM orders AS O
          WHERE 
          IF (O.send_date != "0000-00-00", DATE(O.send_date), DATE(O.shipment_date_expected)) = "' . formatDate($_POST['date']) . '"
          AND
          (
            (
              (
                O.payment_type = "bank_transfer" OR
                O.payment_type = "platnoscipl" OR
                O.payment_type = "card"
              )
              AND O.to_pay <= O.paid_amount
            )
            OR
            (
              O.payment_type = "postal_fee" OR
              O.payment_type = "bank_14days"
            )
            OR
            (
              O.second_payment_type = "postal_fee" OR
              O.second_payment_type = "bank_14days"
            )
          )
          AND 
          (
            O.order_status = "1" OR
            O.order_status = "2" 
          )
          ';
      break;
      case '9':
        $sSql = "
          SELECT 
          DISTINCT OI.id, 
          P.streamsoft_indeks,
          P.ean_13,
          SUM(OI.quantity) as quantity
        FROM orders_items AS OI
        JOIN  `orders` AS O ON OI.order_id = O.id
        JOIN products AS P
          ON P.id = OI.product_id
        WHERE O.order_status <> '3'
          AND O.order_status <> '4'
          AND O.order_status <> '5'
          AND OI.status = '4'
          AND OI.item_type <> 'A'
          AND OI.deleted <> '1'
          AND OI.source <> '51'
          AND 
          ( SELECT OSH.pack_number
            FROM orders_send_history AS OSH
            JOIN orders_send_history_items AS OSHI
              ON OSH.id = OSHI.send_history_id
            WHERE OSHI.item_id = OI.id
            ORDER BY OSH.id DESC
            LIMIT 1 ) > 0
        GROUP BY OI.product_id
        ORDER BY OI.quantity DESC
          ";
      break;

      case '10':
        $sSql = "
          SELECT P.name, P.isbn_plain, P.ean_13
          FROM products_stock AS PS
          JOIN products AS P
            ON PS.id = P.id AND P.prod_status = '2'
          WHERE 
            PS.azymut_status = '1' OR
            PS.helion_status = '1' OR
            PS.dictum_status = '1' OR
            PS.siodemka_status = '1' OR
            PS.olesiejuk_status = '1' OR
            PS.olesiejuk_status = '1' OR
            PS.ateneum_status = '1' OR
            PS.platon_status = '1' OR
            PS.profit_e_status > 0 OR
            PS.profit_j_status > 0 OR
            PS.profit_g_status > 0 OR
            PS.profit_m_status > 0 OR
            PS.profit_x_status > 0
          ";
      break;
    }
    return $sSql;
  }// end of _getSQLExport() method
  
  /**
   * 
   * @param string $sMsqlDate
   * @global DatabaseManager $pDbMgr
   * @return array
   */
  private function getUsersLoginASC($sMsqlDate) {
    global $pDbMgr;
    
    $sSql = 'SELECT (SELECT U.login FROM users AS U WHERE U.id = user_id) as LOGIN, login_date as DATA_LOGOWANIA, user_id
             FROM  login_history 
             WHERE 
              DATE(login_date) = "' . $sMsqlDate . '"
              AND (
                ip = "83.144.105.248"
                  OR
                ip = "95.48.111.146"
                  OR
                ip = "78.11.78.195"
              )
             GROUP BY user_id
             ORDER BY  login_history.login_date ASC
             ';
    return $pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   */
  private function getUserLastLoginHistory($iUserId, $sMsqlDate) {
    global $pDbMgr;
    
    $sSql = 'SELECT IF(logout_date > login_date, logout_date, login_date) AS last_date
             FROM login_history
             WHERE 
              (
              DATE(login_date) = "' . $sMsqlDate . '" 
               OR 
              DATE(logout_date) = "' . $sMsqlDate . '"
                )
              AND user_id = '.$iUserId.'
              AND (
                ip = "83.144.105.248"
                  OR
                ip = "95.48.111.146"
                  OR
                ip = "78.11.78.195"
              )
             ORDER BY last_date DESC
             LIMIT 1 ';
    return $pDbMgr->GetOne('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iUserId
   * @param string $sMsqlDate
   * @return date
   */
  private function getUsersLastActivity($iUserId, $sMsqlDate) {
    return $this->getUserLastLoginHistory($iUserId, $sMsqlDate);
  }
  
  /**
   * 
   * @param array $aUsersLogin
   * @return array
   */
  private function getLogoutUsersDate($aUsersLogin, $sMsqlDate) {
    foreach ($aUsersLogin as $iKey => $aUser) {
      $aUsersLogin[$iKey]['DATA_WYLOGOWANIA'] = $this->getUsersLastActivity($aUser['user_id'], $sMsqlDate);
      unset($aUsersLogin[$iKey]['user_id']);
    }
    return $aUsersLogin;
  }
  
  /**
   * 
   */
  private function _getLoginUsers() {
    
    $sMsqlDate = formatDate($_POST['date']);
    $aUsersLogin = $this->getUsersLoginASC($sMsqlDate);
    $aUserStatistics = $this->getLogoutUsersDate($aUsersLogin, $sMsqlDate);
    return $aUserStatistics;
  }
}
