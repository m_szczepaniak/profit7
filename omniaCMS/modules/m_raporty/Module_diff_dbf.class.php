<?php
/**
 * Klasa generowania dokumentu DBF z przesunięciem magazynowym z G 
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-12 
 * @copyrights Marcin Chudy - Profit24.pl
 */
set_time_limit('3600');
ini_set('memory_limit', '1G');
class Module__raporty__diff_dbf {
  
  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  function __construct(&$pSmarty) {
    
    
    include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

    if ($_FILES['file1']['tmp_name'] != '' && $_FILES['file2']['tmp_name'] != '') {

      $sFilename1 = $_FILES['file1']['tmp_name'];
      $sFilename2 = $_FILES['file2']['tmp_name'];
      $sRealFilename1 = $_FILES['file1']['name'];
      $sRealFilename2 = $_FILES['file2']['name'];
      try {

        $oDiffDBF = new \reports\diffDBF($sFilename1, $sFilename2);  
        $oDiffDBF->GetRecords($oDiffDBF->pDBF1, $sRealFilename1);
        $oDiffDBF->GetRecords($oDiffDBF->pDBF2, $sRealFilename2);
        $sReturn = $oDiffDBF->compare($sRealFilename1, $sRealFilename2);
        if ($sReturn != '') {
          header('Content-Type: text/html; charset=UTF-8');
          header('Content-Disposition: attachment; filename="dbf_compare.xls"');
          echo $sReturn;
          exit;
        }
      } catch (Exception $exc) {
        dump('BŁĄD');
        echo $exc;
      }
    } else {
      echo 'Wygenerowane pliki:<br /><br />';
      $aFiles = glob($_SERVER['DOCUMENT_ROOT']."/LIB/reports/files_diff_dbf/*");
      foreach ($aFiles as $sPathFile) {
        $aFilePath = explode('/', $sPathFile);
        $sName = array_pop($aFilePath);
        echo '<a href="/LIB/reports/files_diff_dbf/'.$sName.'">'.$sName.'</a><br />';
      }
      ?>
        <br /><br />
        <form method="post" enctype="multipart/form-data">
          <input type="file" name="file1" />
          <input type="file" name="file2" />
          <input type="submit" name="Porównaj" />
        </form>
      <?php
    }
  }
}// end of class
