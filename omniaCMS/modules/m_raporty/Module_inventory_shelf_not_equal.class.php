<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2016-03-01 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_raporty;

use Admin;
use CommonSynchro;
use DatabaseManager;
use Exception;
use FormTable;
use LIB\EntityManager\Entites\Containers;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\EntityManager\Entites\StockLocation;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use Supply\IndependentSupply;
use Validator;

/**
 * Description of Moule_inventory_shelf_not_equal
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__raporty__inventory_shelf_not_equal implements ModuleEntity, SingleView
{

    /**
     * @var Admin
     */
    public $oClassRepository;

    /**
     *
     * @var string - komunikat
     */
    protected $sMsg;

    /**
     *
     * @var Smarty
     */
    public $pSmarty;

    /**
     *
     * @var string
     */
    public $sModule;

    /**
     *
     * @var DatabaseManager
     */
    protected $pDbMgr;

    public function __construct(Admin $oClassRepository)
    {
        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function doDefault()
    {
        return $this->getListOfNotEqualShelf();
    }

    private function getListOfNotEqualShelf()
    {
        global $aConfig;
        $aData = array();

        if (!empty($_POST)) {
            $aData = &$_POST;
        }

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header' => _('Niezgodne półki na remanencie'),
            'refresh' => true,
            'search' => true,
            'items_per_page' => 20,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 2,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'container_id',
                'content' => _('Półka'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'status',
                'content' => _('Status'),
                'sortable' => true,
                'width' => '150'
            ),
            array(
                'db_field' => 'created_by',
                'content' => _('Skanował'),
                'sortable' => true,
                'width' => '250'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Data skanowania'),
                'sortable' => true,
                'width' => '350'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '40'
            )
        );

        $aColSettings = array(
            'id' => array(
                'show' => false
            ),
            'action' => array(
                'actions' => array('view', 'edit'),
                'params' => array(
                    'view' => array('container_id' => '{container_id}'),
                    'edit' => array('container_id' => '{container_id}'),
                ),
                'icon' => ['view' => 'preview']
            ),
        );


        include_once('View/View.class.php');
        $oView = new \View(_('bank_accounts'), $aHeader, $aAttribs);

        $oView->AddFilter('f_created_from', _('Data od'), array('style' => 'float: right;'), (empty($_POST['f_created_from']) ? '00-00-0000' : $_POST['f_created_from']), 'date');
        $oView->AddFilter('f_created_to', _(' do '), array('style' => 'float: right;'), (empty($_POST['f_created_to']) ? '00-00-0000' : $_POST['f_created_to']), 'date');

        $aCols = array('I.id', 'I.container_id', 'I.status', 'GROUP_CONCAT(I.created_by SEPARATOR ", ") as created_by', 'GROUP_CONCAT(I.created SEPARATOR ", ") as created'
        );


        $sSql = 'SELECT %cols
             FROM inventory AS I
             WHERE 
              1=1
              %filters
             ';
        $sFilters = (isset($_POST['f_created_to']) && $_POST['f_created_to'] != '' ? ' AND I.created <= "' . date('Y-m-d', strtotime($_POST['f_created_to'])) . ' 23:59:59"' : '') .
            (isset($_POST['f_created_from']) && $_POST['f_created_from'] != '' ? ' AND I.created > "' . date('Y-m-d', strtotime($_POST['f_created_from'])) . ' 00:00:00"' : '');

        $sGroupBySQL = ' GROUP BY I.container_id  ';
        $aSearchCols = array('I.container_id');
        $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, $sFilters, $sGroupBySQL, ' IF(I.status = "N", "Z", I.status) DESC, I.id ASC ');
        $oView->AddRecordsHeader($aRecordsHeader);
        $oView->setActionColSettings($aColSettings);

        $aRecordsFooter = array(
            array(),
            array('add', 'add_packet')
        );
        $oView->AddRecordsFooter($aRecordsFooter, [
            'move_products_to_locations' => [
                2 => 'add'
            ]
        ]);
        return ShowTable($oView->Show());
    }


    /**
     * @param $shelf
     * @return bool
     */
    private function detectTypeStock($shelf)
    {
        $sSql = 'SELECT id FROM containers WHERE id = "' . $shelf . '" AND magazine_type IN("SH", "SL") ';
        return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
    }

    /**
     *
     * @return string
     */
    public function doEdit($bView = false)
    {
        $shelf = $_GET['container_id'];

        if (true === $this->detectTypeStock($shelf)) {

            return $this->getEditStock($bView, $shelf);
        } else {
            return $this->getEditShop($bView, $shelf);
        }
    }


    /**
     * @param $bView
     * @param $shelf
     * @return string
     */
    private function getEditStock($bView, $shelf)
    {
        $sSql = 'SELECT II.product_id, P.*, II.*
                 FROM inventory_items AS II
                 JOIN products AS P
                  ON P.id = II.product_id
                 JOIN inventory AS I
                  ON I.id = II.inventory_id
                 WHERE I.container_id = "' . $shelf . '"
                 ORDER BY II.inventory_id ASC';
        $aInventoriesItems = $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC);

        $sSql = 'SELECT SL.products_stock_id, P.*, SUM(quantity) as quantity 
                 FROM stock_location AS SL
                 JOIN products AS P
                  ON P.id = SL.products_stock_id
                 WHERE SL.container_id = "' . $shelf . '" 
                   AND SL.quantity > 0
                 GROUP BY SL.products_stock_id
                 ';
        $aStockLocationItems = $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC);

        include_once('Form/FormTable.class.php');
        $form = new FormTable('edit_shelf', sprintf(_('Niezgodności półki %s'), $shelf), [], [], true);
        $form->AddHidden('do', 'saveStock');
        $form->AddHidden('sc_shelf', $shelf);
        foreach ($aInventoriesItems as $iProductId => $aInventoryItems) {
            $aInventoryItem = $aInventoryItems;
            $aStockLocationItem = $aStockLocationItems[$iProductId];
            unset($aStockLocationItems[$iProductId]);


            $sPreText = '<div style="width: 500px; float:left;">' .
                trimString($aInventoryItem['name'], 80) . '</div> - ' .
                $aInventoryItem['streamsoft_indeks'] . ' - ' . $aInventoryItems['scanned_by'] . ' - <strong>' . $aInventoryItems['quantity'] . '</strong> ';
            $sPostText = '  <strong>' . (isset($aStockLocationItem['quantity']) ? $aStockLocationItem['quantity'] . '</strong> -  NA LOKALIZACJACH W BAZIE ' : ' BRAK NA STOCKU </strong>');
            $input = ' - ';

            if ($aStockLocationItem['quantity'] <> $aInventoryItems['quantity']) {
                if (true !== $bView) {
                    $input = $form->GetTextHTML('items[' . $iProductId . ']', $sPreText, '', ['style' => 'width:50px;'], '', 'uint', true);
                }
            }
            $form->addMergedRow($sPreText . $input . $sPostText, array('style' => 'text-align: left; font-weight: normal;'));
        }

        if (!empty($aStockLocationItems)) {
            foreach ($aStockLocationItems as $iProductId => $aStockLocationItem) {
                $aInventoryItems = $aInventoriesItems[$iProductId];
                $aStockLocationItem = $aStockLocationItems[$iProductId];
                unset($aStockLocationItems[$iProductId]);
                $sPreText = '<div style="width: 500px; float:left;">' .
                    trimString($aStockLocationItem['name'], 80) . '</div> - ' .
                    $aStockLocationItem['streamsoft_indeks'] . ' - ' . ($aInventoryItems['scanned_by'] != '' ? $aInventoryItems['scanned_by'] : ' BRAK ') . ' - <strong>' . $aInventoryItems['quantity'] . '</strong> ';
                $sPostText = '  <strong>' . (isset($aStockLocationItem['quantity']) ? $aStockLocationItem['quantity'] . '</strong> -  NA LOKALIZACJACH W BAZIE '  : ' BRAK </strong>');
                $input = ' - ';
                if (true !== $bView) {
                    $input = $form->GetTextHTML('items[' . $iProductId . ']', $sPreText, '', ['style' => 'width:50px;'], '', 'uint', true);
                }
                $form->addMergedRow($sPreText . $input . $sPostText, array('style' => 'text-align: left; font-weight: normal;'));
            }
        }
        if (true !== $bView) {
            $form->addMergedRow(_('<h3>UWAGA ZAPIS SPOWODUJE ZMIANĘ ILOŚCI produktów na lokalizacji, (jeśli ilość jest inna niż w bazie) !!!! </h3>'), array('style' => 'text-align: left; font-weight: normal;'));
            $form->AddInputButton("submit", _('Zapisz'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
        }
        return $form->ShowForm();
    }


    /**
     * @param $bView
     * @param $shelf
     * @return string
     */
    private function getEditShop($bView, $shelf)
    {
        $sSql = 'SELECT II.product_id, P.*, II.*
                 FROM inventory_items AS II
                 JOIN products AS P
                  ON P.id = II.product_id
                 JOIN inventory AS I
                  ON I.id = II.inventory_id
                 WHERE I.container_id = "' . $shelf . '"
                 ORDER BY II.inventory_id ASC';
        $aInventoriesItems = $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);

        include_once('Form/FormTable.class.php');
        $form = new FormTable('edit_shelf', sprintf(_('Niezgodności półki %s'), $shelf), [], [], true);
        $form->AddHidden('do', 'save');
        $form->AddHidden('sc_shelf', $shelf);
        foreach ($aInventoriesItems as $iProductId => $aInventoryItems) {
            $aInventoryItem = $aInventoryItems[0];
            $sPreText = '<div style="width: 500px; float:left;">' .
                trimString($aInventoryItem['name'], 80) . '</div> - ' .
                $aInventoryItem['streamsoft_indeks'] . ' - ' . $aInventoryItems[0]['scanned_by'] . ' - <strong>' . $aInventoryItems[0]['quantity'] . '</strong> ';
            $sPostText = '  <strong>' . (isset($aInventoryItems[1]['quantity']) ? $aInventoryItems[1]['quantity'] . '</strong> -  ' . $aInventoryItems[1]['scanned_by'] : ' BRAK </strong>');
            $input = ' - ';
            if (true !== $bView) {
                $input = $form->GetTextHTML('items[' . $iProductId . ']', $sPreText, '', ['style' => 'width:50px;'], '', 'uint', true);
            }
            $form->addMergedRow($sPreText . $input . $sPostText, array('style' => 'text-align: left; font-weight: normal;'));
        }
        if (true !== $bView) {
            $form->AddInputButton("submit", _('Zapisz'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
        }
        return $form->ShowForm();
    }

    /**
     *
     * @return string
     */
    public function doView()
    {

        return $this->doEdit(true);
    }
    /**
     *
     * @return string
     */
    public function doSaveStock()
    {

        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        $shelf = $_POST['sc_shelf'];
        $aItems = $_POST['items'];
        foreach ($aItems as $iKey => $iValue) {
            if ($iValue === '') {
                $oValidator->sError .= '<li>Brak wartości jednego z produktów';
            }
        }

        if (!$oValidator->Validate($_POST)) {
            $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $_GET['container_id'] = $shelf;
            return $this->doEdit();
        }
        $this->pDbMgr->BeginTransaction('profit24');
        $aInventories = $this->getInventoryByContainerId($shelf);
        foreach ($aItems as $iProductId => $quantity) {
            $this->inserOrUpdateValueInventory($aInventories[0], $iProductId, $quantity);
            // zapis nowej ilości na lokalizacji magazynowej
            try {
                $this->changeQuantityOnStockLocation($shelf, $iProductId, $quantity);
            } catch (\Exception $ex) {
                $this->pDbMgr->RollbackTransaction('profit24');
                $msg = _($ex->getMessage().' <br /> Wystąpił błąd podczas zapisywania ilości na lokalizacjach ' . print_r($aInventories, true));
                $this->sMsg .= GetMessage($msg);
                AddLog($msg);
                return $this->doDefault();
            }

        }
        $this->pDbMgr->CommitTransaction('profit24');
        include_once('Module_inventory_stock.class.php');
        $inventory = new Module__raporty__inventory_stock($this->oClassRepository);
        $inventory->defineInventoryStatus($shelf);
        $this->sMsg .= GetMessage(_('Zapisano konflikty inwentaryzacji'), false);
        return $this->doDefault();
    }

    /**
     * @param $shelf
     * @param $iProductId
     * @param $quantity
     * @return bool
     * @throws Exception
     */
    private function changeQuantityOnStockLocation($shelf, $iProductId, $quantity) {

        $stockLocationObject = new StockLocation($this->pDbMgr);

        $sSql = 'SELECT * 
                FROM stock_location 
                WHERE container_id = "'.$shelf.'" 
                  AND products_stock_id = '.$iProductId.'
                ORDER BY quantity DESC
                LIMIT 1
                FOR UPDATE';
        $stockLocation = $this->pDbMgr->GetRow('profit24', $sSql);
        if (!empty($stockLocation)) {
            if ($stockLocation['reservation'] > $quantity) {
                throw new \Exception('Ilośc produktów w rezerwacji jest wieksza niż ta która jest na lokalizacji, proszę przebić zamówienia na te produkt o id ' . $iProductId);
            } else {

                if (false === $stockLocationObject->changeQuantityByStockLocationQuantity($stockLocation, $quantity)) {
                    throw new \Exception('Błąd zmiany ilości na lokalizacji');
                }
            }
        } else {
            // dodajemy nowy produkt na tej lokalizacji
            $container = $this->pDbMgr->getTableRow('containers', $shelf, ['magazine_type']);
            if (false === $stockLocationObject->add($container['magazine_type'], $iProductId, $shelf, $quantity, 0, $quantity)) {
                throw new \Exception('Błąd dodawania produktu na lokalizacji');
            }
        }
    }

    /**
     *
     * @return string
     */
    public function doSave()
    {

        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        $shelf = $_POST['sc_shelf'];
        $aItems = $_POST['items'];
        foreach ($aItems as $iKey => $iValue) {
            if ($iValue === '') {
                $oValidator->sError .= '<li>Brak wartości jednego z produktów';
            }
        }

        if (!$oValidator->Validate($_POST)) {
            $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $_GET['container_id'] = $shelf;
            return $this->doEdit();
        }
        $aInventories = $this->getInventoryByContainerId($shelf);
        foreach ($aItems as $iProductId => $iValue) {
            $this->inserOrUpdateValueInventory($aInventories[0], $iProductId, $iValue);
            $this->inserOrUpdateValueInventory($aInventories[1], $iProductId, $iValue);
        }
        include_once('Module_inventory.class.php');
        $inventory = new Module__raporty__inventory($this->oClassRepository);
        $inventory->defineInventoryStatus($shelf);
        $this->sMsg .= GetMessage(_('Zapisano konflikty inwentaryzacji'), false);
        return $this->doDefault();
    }

    /**
     *
     * @param int $iInventoryId
     * @param int $iProductId
     * @param int $iValue
     */
    private function inserOrUpdateValueInventory($iInventoryId, $iProductId, $iValue)
    {

        if ($this->pDbMgr->GetOne('profit24', 'SELECT id FROM inventory_items WHERE product_id = "' . $iProductId . '" AND inventory_id = "' . $iInventoryId . '"') > 0) {
            $aValues = [
                'quantity' => $iValue
            ];
            $this->pDbMgr->Update('profit24', 'inventory_items', $aValues, ' product_id = "' . $iProductId . '" AND inventory_id = "' . $iInventoryId . '"');
        } else {
            $aValues = [
                'quantity' => $iValue,
                'inventory_id' => $iInventoryId,
                'product_id' => $iProductId
            ];
            $this->pDbMgr->Insert('profit24', 'inventory_items', $aValues);
        }
    }

    /**
     *
     * @param int $shelf
     * @return array
     */
    private function getInventoryByContainerId($shelf)
    {

        $sSql = 'SELECT id FROM inventory WHERE container_id = "' . $shelf . '"';
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

    public function getMsg()
    {
        return $this->sMsg;
    }

    /**
     *
     * @param array $aItem
     * @return string
     */
    public function parseViewItem(array $aItem)
    {

        if ($aItem['status'] != 'N') {
            $aItem['disabled'][] = 'edit';
        }

        switch ($aItem['status']) {
            case 'E':
                $aItem['status'] = 'Zgodne';
                break;

            case 'N':
                $aItem['status'] = 'Niezgodne';
                break;

            case 'S':
                $aItem['status'] = 'Raz zeskanowana';
                break;
        }
        return $aItem;
    }

    public function resetViewState()
    {
        resetViewState($this->sModule);
    }

    /**
     *
     * @return array
     */
    private function getShelfPrefixes()
    {
        $sSql = 'SELECT SUBSTR(container_id, 1, 3) AS label, SUBSTR(container_id, 1, 3) AS value FROM inventory GROUP BY value';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     *
     */
    public function doGetReport()
    {
        $sPrefixContainer = $_POST['containers_prefix'];
        $sMagazineFile = $_POST['magazine_file'];
        $sDateFrom = $_POST['date_from'];
        $aMagazineTypes = $_POST['magazine_types'];
        $aInventory = $this->getInventoryItemsQuantity($sPrefixContainer, $sDateFrom, $aMagazineTypes);
        $aMagazine = $this->getMagazineFilesItemsQuantity($sMagazineFile);
        $this->getReportArray($aInventory, $aMagazine);
    }

    /**
     * @param $aInventory
     * @param $aMagazine
     */
    private function getReportArray($aInventory, $aMagazine)
    {
        $aResult = [];
        /*
          I | M
          3 - 5
         */
        foreach ($aInventory as $indeks => $aItem) {
            $iInventoryQuantity = $aItem['quantity'];
            $iQuantiyInMagazine = 0;
            if (isset($aMagazine[$indeks])) {
                $iQuantiyInMagazine = $aMagazine[$indeks];
            }
            $iResult = $iInventoryQuantity - $iQuantiyInMagazine;
            $aResult[$indeks]['quantity'] = $iResult;
            $aResult[$indeks]['container_id'] = $aItem['container_id'];
        }

        foreach ($aMagazine as $indeks => $iMagazineQuantity) {
            if (!isset($aResult[$indeks])) {
                $aResult[$indeks]['quantity'] = $iMagazineQuantity * (-1);
            }
        }

        foreach ($aResult as $indeks => $aRow) {
            $quantity = $aRow['quantity'];
            if ($quantity > 0) {
                $aResultGreaterZero[$indeks]['quantity'] = $quantity;
                $aResultGreaterZero[$indeks]['container_id'] = $aRow['container_id'];
            } elseif ($quantity < 0) {
                $aResultLessZero[$indeks]['quantity'] = $quantity * (-1);
                $aResultLessZero[$indeks]['container_id'] = $aRow['container_id'];
            } else {
                $aResultEqualZero[$indeks]['quantity'] = $quantity;
            }
        }

        $_GET['hideHeader'] = '1';

        header('Content-Encoding: UTF-8');
        header("Content-type: text/html; charset=UTF-8");
        header('Content-Disposition: attachment; filename="Report_' . date('YmdHis') . '.xls"');
        header("Pragma: no-cache");
        header("Expires: 0");

        echo 'NADWYŻKI WZGLĘDEM MAGAZYNU w STREAMSOFT:' . "\r\n";
        if (!empty($aResultGreaterZero) && is_array($aResultGreaterZero)) {
            $this->echoResults($aResultGreaterZero);
        }

        echo 'BRAKI WZGLĘDEM MAGAZYNU w STREAMSOFT:' . "\r\n";
        if (!empty($aResultLessZero) && is_array($aResultLessZero)) {
            $this->echoResults($aResultLessZero);
        }
        die;
    }

    /**
     *
     * @param array $aResults
     */
    private function echoResults($aResults)
    {
        include_once('../import/CommonSynchro.class.php');
        $oCommonSynchro = new CommonSynchro();


        if (!empty($aResults) && is_array($aResults)) {
            foreach ($aResults as $indeks => $aRow) {
                $aProduct = $oCommonSynchro->refMatchProductByISBNs(['ean_13' => $indeks], ['id', 'name'], false, false);
//      if (!isset($aProduct['id'])) {
//        if (substr($indeks, 0, 3) != '978') {
//          echo $indeks . "\r\n";
//        }
//      }
                echo $aProduct['name'] . "\t" . $indeks . "\t" . $aRow['quantity'] . "\t" . $aRow['container_id'] . "\t" . "\r\n";
            }
        }
    }

    /**
     *
     * @param string $sPrefixContainer
     * @return array
     */
    private function getInventoryItemsQuantity($sPrefixContainer, $sDateFrom, $aMagazineTypes)
    {

        if (!empty($aMagazineTypes) && is_array($aMagazineTypes)) {
            $sMagazineTypeSQL = ' AND C.magazine_type IN ("' . implode('", "', $aMagazineTypes) . '")';
        }
        if ($sPrefixContainer != '') {
            $sPrefixSQL = ' AND SUBSTR(I.container_id, 1, 3) = "' . $sPrefixContainer . '" ';
        }

        $sDateFrom = preg_replace('/(\d+)-(\d+)-(\d+)/', '$3-$2-$1', $sDateFrom);
        $sSql = 'SELECT P.streamsoft_indeks, SUM(II.quantity) AS quantity, I.container_id
             FROM inventory_items AS II
             JOIN inventory AS I
              ON I.id = II.inventory_id
             JOIN products AS P
              ON P.id = II.product_id
             JOIN containers AS C
              ON C.id = I.container_id
             WHERE 
             inventory_id IN (
              SELECT id FROM inventory AS I
               WHERE 1=1 
               ' . $sPrefixSQL . '
              GROUP BY container_id
             )
             AND DATE(I.created) > "' . $sDateFrom . '"
             ' . $sMagazineTypeSQL . '
             GROUP BY P.streamsoft_indeks';
        return $this->pDbMgr->GetAssoc('profit24', $sSql, false, array(), DB_FETCHMODE_ASSOC, false);
    }

    /**
     *
     * @param string $sMagazineFile
     * @return array
     */
    private function getMagazineFilesItemsQuantity($sMagazineFile)
    {
        $books = [];
        $xml = simplexml_load_file($sMagazineFile);
        foreach ($xml->STAN as $stan) {
            if (intval($stan->ILOSC) > 0) {
                $indeks = (string)$stan->INDEKS;
                if (!isset($books[$indeks])) {
                    $books[$indeks] = (int)$stan->ILOSC;
                } else {
                    $books[$indeks] += (int)$stan->ILOSC;
                }
            }
        }
        return $books;
    }

    /**
     *
     * @return string
     */
    public function doAdd()
    {
        include_once('Form/FormTable.class.php');
        $form = new \FormTable('get_inventory_report', _('Pobierz plik remanentu'));
        $form->AddHidden('do', 'getReport');

        $aPrefix = $this->getShelfPrefixes();
        $form->AddSelect('containers_prefix', _('Prefix lokalizacji'), [], addDefaultValue($aPrefix), '', '', false);
        $form->AddText('date_from', _('Od '), '', [], '', 'date');
        $sSql = 'SELECT type AS value, name AS label FROM magazine ';
        $aOptions = $this->pDbMgr->GetAll('profit24', $sSql);
        $form->AddCheckBoxSet('magazine_types', _('Typy magazynów'), $aOptions, [], '', false);

        $aMagazines = $this->getMagazinesFiles();
        $form->AddSelect('magazine_file', _('Plik magazynu'), [], addDefaultValue($aMagazines));
        $form->AddInputButton("submit", _('Pobierz'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
        return $form->ShowForm();
    }

    public function doAdd_Packet()
    {

        $_GET['hideHeader'] = '1';

        header('Content-Encoding: UTF-8');
        header("Content-type: text/html; charset=UTF-8");
        header('Content-Disposition: attachment; filename="Report_' . date('YmdHis') . '.xls"');
        header("Pragma: no-cache");
        header("Expires: 0");
        $aNotEqualShelf = $this->getNotScannedShelf();
        foreach ($aNotEqualShelf as $aRow) {
            echo implode("\t", $aRow) . "\r\n";
        }
        die;
    }



    /**
     *
     * @return array
     */
    private function getNotScannedShelf()
    {

        $sSql = "SELECT DISTINCT SL.container_id
                 FROM stock_location AS SL
                 LEFT JOIN inventory AS I
                  ON I.container_id = SL.container_id
                 WHERE SL.quantity > 0 AND I.id IS NULL
                 GROUP BY SL.container_id
                 ";
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     *
     * @return array
     */
    private function getMagazinesFiles()
    {

        $path = $_SERVER['DOCUMENT_ROOT'] . '/LIB/communicator/sources/Internal/streamsoft/import_files/xml/Stany/old/';
        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    $files[filemtime($path . $file)] = $path . $file;
                }
            }
            closedir($handle);

            // sort
            krsort($files);
//      $files = array_slice($files, 0, 20);
            $files_options = [];
            foreach ($files as $file) {
                $files_options[] = [
                    'label' => $file,
                    'value' => $file
                ];
            }
            return $files_options;
        }
    }

}
