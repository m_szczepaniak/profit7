<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2016-02-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_raporty;

use Admin;
use DatabaseManager;
use Exception;
use FormTable;
use omniaCMS\lib\interfaces\ModuleEntity;

include_once('Module_inventory.class.php');
/**
 * Description of Module_inventory
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__raporty__inventory_stock extends Module__raporty__inventory implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected $sMsg;

  /**
   *
   * @var Smarty
   */
  public $pSmarty;

  /**
   *
   * @var string
   */
  public $sModule;

  /**
   *
   * @var DatabaseManager
   */
  protected $pDbMgr;

    protected $maxScannedShelfQuantity = 1;


  public function __construct(Admin $oClassRepository) {
    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
      parent::__construct($oClassRepository);
  }


    /**
     * @param $shelf
     * @throws Exception
     */
    final protected function validateShelf($shelf) {

        $sSql = 'SELECT id FROM containers WHERE id = "' . $shelf . '" AND magazine_type IN("SH", "SL") ';
        $isContainer = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($isContainer <= 0) {
            throw new Exception('Brak półki o nr "' . $shelf . '" lub nieprawidłowa półka');
        }
    }

    /**
     *
     * @return string
     */
    protected function getInventoryStatus($aInventories, $iShelf) {

        $sSql = 'SELECT product_id, quantity 
                 FROM inventory_items 
                 WHERE inventory_id = ' . $aInventories[0] . '
                  AND  quantity > 0
                 ORDER BY product_id DESC';
        $firstElements = md5(json_encode($this->pDbMgr->GetAll('profit24', $sSql)));

        $sSql = 'SELECT products_stock_id as product_id, SUM(quantity) AS quantity 
                 FROM stock_location 
                 WHERE container_id = "'.$iShelf.'"
                  AND  quantity > 0
                 GROUP BY products_stock_id 
                 ORDER BY products_stock_id DESC';
        $secondElements = md5(json_encode($this->pDbMgr->GetAll('profit24', $sSql)));
        if ($firstElements === $secondElements) {
            return 'E'; // equal
        } else {
            return 'N'; // not equal
        }
    }
}
