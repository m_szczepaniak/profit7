<?php

// plik jezykowy raportow
$aConfig['lang']['m_raporty_exports']['exports'] = 'Eksporty';
$aConfig['lang']['m_raporty_exports']['export_option'] = 'Wybierz eksport';
$aConfig['lang']['m_raporty_exports']['eksport_1'] = 'Eksport składowych zamówień otwartych';
$aConfig['lang']['m_raporty_exports']['eksport_2'] = 'Eksport z Bazy danych WFM, puste ISBN i STAN > 0';
$aConfig['lang']['m_raporty_exports']['eksport_3'] = 'Eksport sum przelewów pobraniowych z poszczególnych dni';
$aConfig['lang']['m_raporty_exports']['eksport_4'] = 'Eksport wysłanych paczek z poszczególnych dni';
$aConfig['lang']['m_raporty_exports']['eksport_5'] = 'Eksport pliku XLS, na podstawie DBF z WFM - dopasowanie produktów';
$aConfig['lang']['m_raporty_exports']['eksport_6'] = 'Eksport pliku DBF, na podstawie CSV z WFM - zmiana formatu';
$aConfig['lang']['m_raporty_exports']['eksport_7'] = 'Zamówienia, które powinny zostać wysłane na dany dzień a jeszcze nie są';
$aConfig['lang']['m_raporty_exports']['eksport_8'] = 'Raport logowania użytkowników';
$aConfig['lang']['m_raporty_exports']['eksport_9'] = 'Raport ilości produktów na tramwaju';
$aConfig['lang']['m_raporty_exports']['eksport_10'] = 'Raport produktów dostępnych z nakładem wyczerpanym';

$aConfig['lang']['m_raporty_exports']['eksport_description_label'] = 'Opis dostępnych eksportów';
$aConfig['lang']['m_raporty_exports']['eksport_description'] = '
<b>Eksport składowych zamówień otwartych:</b>
<blockquote>
 Eksport składowych zamówień w statusie "jest potwierdzone", 
  które znajdują się w zamówieniu o statusie różnym od "zrealizowanego", "zatwierdzonego", "anulowanego",
  pomijamy składowe które są usunięte, oraz załączniki do książek (płyta CD)
</blockquote>

<b>Eksport z Bazy danych WFM, puste ISBN i STAN > 0:</b>
<blockquote>
 Eksport rekordów z Wf-Mag, które mają pusty ISBN(`CERTYFOPIS`) i `STAN` jest większy od zera,
 Przeszukuje w pliku DBF BAZA DANYCH.
</blockquote>

<b>Eksport sum przelewów Pobraniowych z poszczególnych dni.</b>
<blockquote>
 Eksport zestawia głowny przelew Pobraniowy z poszczególnymi przelewami z konkretnych dni(data wystawienia faktury), i sumuje wartość tych przelewów.
 Grupowanie po dniu, serwisie, metodzie transportu (FedEx, TBA)
 Eksport jest ograniczony do 30 rekordów ze względu na skomplikowanie zapytania wybierającego.
</blockquote>

<b>Eksport wysłanych paczek z poszczególnych dni.</b>
<blockquote>
 Eksport ilości wyslanych zamówień przez daną osobę, oraz z ilością pozycji w zamówieniach z podziałem na dni.
</blockquote>

<b>Eksport pliku XLS, na podstawie DBF z WFM - dopasowanie produktów.</b>
<blockquote>
 Eksport importuje plik DBF wystawiony z WFM, na podstwie kolumny CERTFOPIS dopasowuje produkty z oferty produktowej, 
 a następnie dopasowane produkty zwraca do pliku XLS, dodakowo dołączone zostają kolumny pochodzące z oferty produktowej profit24: TYTUŁ, WYDAWNICTWO, AZYMUT BOOKINDEX 
</blockquote>

<b>Eksport pliku DBF, na podstawie CSV z WFM - zmiana formatu.</b>
<blockquote>
  Na postawie przekazanego pliku CSV, tworzony jest DBF w formacie zgodnym z programem Wf-Mag
  
  Format pliku CSV:
  CERTFOPIS;ILOSC
  CERTFOPIS;ILOSC
  CERTFOPIS;ILOSC
  ...
</blockquote>


<b>Zamówienia, które powinny zostać wysłane na dany dzień a jeszcze nie są.</b>
<blockquote>
  Zamówienia, które zostały opłacone lub met. płatności P14 lub PPO
  i data wysyłki ustawiona przez obsługę D.H. ustatawiona na DATA
  jeśli nie ustawiona to aktualna planowana data wystłki ustawiona na DATA
  i zamówienie w statusie w realizacji lub skompletowane.
</blockquote>


<b>Raport logowania użytkowników.</b>
<blockquote>
  Na podstawie tabeli logowań, pobierana jest data pierwszego logowania uzytkownika
  Oraz data ostatniego wylogowania lub logowania z adresu ip 78.11.78.195 lub 95.48.111.146
  W obrębie danego dnia
</blockquote>


<b>Raport ilości produktów na tramwaju.</b>
<blockquote>
  Raport ilości produktów, które są na tramwaju, czyli są potwierdzone, a ich źródło jest inne niż Stock.
</blockquote>
';

$aConfig['lang']['m_raporty_exports']['err_no_option'] = 'Wybierz eksport do wygenerowania';
$aConfig['lang']['m_raporty_exports']['no_items'] = 'Brak elementów';

$aConfig['lang']['m_raporty_diff_report']['header'] = 'Odznacz produkty';
$aConfig['lang']['m_raporty_diff_report']['not_exists'] = 'Nadwyżki:';
$aConfig['lang']['m_raporty_diff_report']['check'] = 'Sprawdź listę';

$aConfig['lang']['m_raporty_diff_report']['delete_q'] = "Czy na pewno usunąć wybrane pozycje?";
$aConfig['lang']['m_raporty_diff_report']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_raporty_diff_report']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_raporty_diff_report']['delete_all_q'] = "Czy na pewno usunąć wybrane pozycje?";
$aConfig['lang']['m_raporty_diff_report']['delete_all_err'] = "Nie wybrano żadnej pozycji do usunięcia!";

$aConfig['lang']['m_raporty_inventory_shelf_not_equal']['add'] = "Pobierz plik remanentu między skanowaniem na inwentaryzacji a Stanem w streamsoft";
$aConfig['lang']['m_raporty_inventory_shelf_not_equal']['add_packet'] = "Pobierz nie zeskanowane półki na STOCKU";
$aConfig['lang']['m_raporty_inventory_shelf_not_equal']['move_products_to_locations'] = "Wprowadź ilości na lokalizacje";