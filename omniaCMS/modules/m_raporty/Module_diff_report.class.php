<?php
/**
 * Klasa weryfikacji za mówionych pozycji na bazie danych
 *
 * @author    Arkadiusz Golba <arekgl0@op.pl>
 * @copyright 2013
 * @version   1.0
 */

class Module__raporty__diff_report {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// Id strony modulu
	var $iPageId;
	
	// katalog plikow log
	var $sLogsDir;

  var $pDbMgr;
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function __construct(&$pSmarty) {
    global $pDbMgr;

    $this->pDbMgr = $pDbMgr;
		$this->sErrMsg = '';
		$this->iPageId = getModulePageId('m_zamowienia');
		$this->sModule = $_GET['module'];
		$this->iLangId = $_SESSION['lang']['id'];
		
	
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
      if (!hasModulePrivileges($_GET['module_id'])){
        showPrivsAlert($pSmarty);
      return;
      }
		}

		switch ($sDo) {
      case 'do':
      default:
        $this->GetPackageNumber($pSmarty);
      break;
    
			case 'change_status': $this->ChangeStatus($pSmarty); 				break;
     
      case 'setPackage' :
        $this->setPackage($pSmarty, $_POST['pack_number']);
      break;
    
      case 'details':
        $this->getDetails($pSmarty, $iId);
      break;
    
      case 'delete':
        $this->Delete($pSmarty, $iId);
      break;
    
			case 'get_package_items': 			
        $this->getPackageItems($pSmarty); 	
      break;
    
      case 'get_insufficient':
        $this->getInsufficient($pSmarty);
      break;

      case 'diff_remanent_streamsoft':
         $this->diffRemanentStreamsoft($pSmarty);
      break;

      case 'get_shelf_not_scanned':
          $this->getShelfNotScanned($pSmarty);
      break;
		}
	} // end Module() function


    /**
     * @param $pSmarty
     */
    public function getShelfNotScanned($pSmarty) {

        $sSql = "
        SELECT 
          DISTINCT OSH.pack_number
        FROM orders_items AS OI
        JOIN  `orders` AS O 
          ON OI.order_id = O.id
        JOIN orders_send_history_items AS OSHI
            ON OSHI.item_id = OI.id
        JOIN orders_send_history AS OSH
            ON OSH.id = OSHI.send_history_id
            
        WHERE O.order_status <> '3'
          AND O.order_status <> '4'
          AND O.order_status <> '5'
          AND OI.status = '4'
          AND OI.item_type <> 'A'
          AND OI.deleted <> '1'
          AND OI.source <> '5'
        GROUP BY OSH.pack_number
        ORDER BY OI.quantity DESC
        ";

        $shelfInOrder = $this->pDbMgr->GetCol('profit24', $sSql);

        $sSql = "SELECT R.package_ident
            FROM remanent as R
            WHERE R.deleted = '0'";
        $shelfScanned = $this->pDbMgr->GetCol('profit24', $sSql);

        $_GET['hideHeader'] = '1';
        header('Content-Encoding: UTF-8');
        header("Content-type: text/html; charset=UTF-8");
        header("Content-Disposition:attachment;filename='shelf_not_scanned_".date('YmdHis').".xls'");
        echo "\xEF\xBB\xBF";

        foreach ($shelfInOrder as $shelfNumber) {
            if (false === in_array($shelfNumber, $shelfScanned)) {
                echo $shelfNumber."\r\n";
            }
        }
    }
  
  
  /**
   * Metoda zwraca braki na półkach, które nie zostały usunięte
   * 
   * @param \Smarty $pSmarty
   * @return void
   */
  private function getInsufficient($pSmarty) {
    
    $sSql = "SELECT DISTINCT RI.id AS ID, R.package_ident AS POLKA, RI.ISBN, P.name AS NAZWA, RI.quantity AS ILOSC_SKAN, RI.expected_quantity AS ILOSC_OCZEKIWANA_DB
             FROM remanent_items AS RI
             JOIN remanent AS R
              ON R.id = RI.remanent_id 
              AND R.deleted = '0'
             LEFT JOIN orders_items AS OI
              ON RI.product_id = OI.id
             LEFT JOIN products AS P
              ON P.id = OI.product_id
             WHERE 
              RI.quantity < RI.expected_quantity
              ";
    $aData = $this->pDbMgr->GetAll('profit24', $sSql);
    if (empty($aData)) {
      $this->sMsg .= _('Brak braków na półkach');
      $this->GetPackageNumber($pSmarty);
    }
    
    $_GET['hideHeader'] = '1';
    header('Content-Encoding: UTF-8');
    header("Content-type: text/html; charset=UTF-8");
    header("Content-Disposition:attachment;filename='export_".date('YmdHis').".xls'");
    echo "\xEF\xBB\xBF";
    
    echo implode(';', array_keys($aData[0]))."\n";
    foreach ($aData as $aItem) {
      echo implode(';', $aItem)."\n";
    }
  }// end of getInsufficient() method
  
  
  /**
   * Metoda usuwa półkę
   * 
   * @param object $pSmarty
   * @param int $iId
   * @return int package number
   */
  private function Delete($pSmarty, $iId) {
    if ($iId > 0) {
      $sSql = 'SELECT package_ident FROM remanent WHERE id = '.$iId;
      $sPackageIdent = $this->pDbMgr->GetOne('profit24', $sSql);
      
      $aValues = array('deleted' => '1');
      if ($this->pDbMgr->Update('profit24', 'remanent', $aValues, ' id = '.$iId) === false) {
        $sLog = sprintf(_('Wystąpił błąd podczas usuwania remanentu półki "%s"'), $sPackageIdent);
        $this->sMsg .= GetMessage($sLog);
        AddLog($sLog);
      } else {
        $sLog = sprintf(_('Półka "%s" została usunięta.'), $sPackageIdent);
        $this->sMsg .= GetMessage($sLog);
        AddLog($sLog);
      }
    }
    return $this->GetPackageNumber($pSmarty);
  }// end of Delete() method
  
  
  /**
   * Pobieramy listę elementów na półce
   * 
   * @global array $aConfig
   * @param object $pSmarty
   * @param int $iId
   */
  private function getDetails($pSmarty, $iId) {
		global $aConfig;
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		
		// zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> _('Remanent półki '),
			'refresh'	=> false,
			'search'	=> false,
			'checkboxes'	=> false,
      'per_page' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
        array(
				'db_field'	=> 'isbn',
				'content'	=> _('ISBN'),
				'sortable'	=> true,
				'width' => '200'
			),
			array(
				'db_field'	=> 'name',
				'content'	=> _('Nazwa'),
				'sortable'	=> true,
				'width' => '200'
			),
      array(
				'db_field'	=> 'quantity',
				'content'	=> _('Zeskanowana ilość'),
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'expected_quantity',
				'content'	=> _('Ilość prawidłowa - wskazywana przez system'),
				'sortable'	=> true,
			)
		);
		
		
		// pobranie liczby wszystkich kodow
		$sSql = "SELECT COUNT(DISTINCT RI.id) 
						 FROM ".$aConfig['tabls']['prefix']."remanent_items AS RI
						 WHERE remanent_id = ".$iId;
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(DISTINCT R.id) 
							 FROM ".$aConfig['tabls']['prefix']."remanent_items AS R
               LEFT JOIN remanent_items AS RI
                 ON R.id = RI.remanent_id
							 WHERE remanent_id = ".$iId;
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('remanent_items', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			//$iCurrentPage = $pView->AddPager($iRowCount);
			//$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			//$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			// pobranie zamówień bankowych
			$sSql = "SELECT RI.isbn, P.name, RI.quantity, RI.expected_quantity
							 FROM ".$aConfig['tabls']['prefix']."remanent_items AS RI
               LEFT JOIN orders_items AS P
                ON RI.product_id = P.id
							 WHERE remanent_id = " . $iId .
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' RI.id DESC ').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '');
			$aRecords =& Common::GetAll($sSql);
      foreach ($aRecords as $iKey => $aRecord) {}
      
      $aColSettings = array();
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('go_back')
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
                                       $pView->Show());
  }// end of Details() method
  
  
  /**
   * Ustawiamy dane do zatwierdzenia
   * 
   * @param object $pSmarty
   * @param string $sPackageIdent
   */
  private function setPackage(&$pSmarty, $sPackageIdent) {
    
    if ($sPackageIdent != '') {
      $this->getPackageItems($pSmarty, $sPackageIdent);
    } else {
      $this->sMsg = getMessage("Brak listy o podanym numerze");
      $this->GetPackageNumber($pSmarty);
    }
  }// end of setPackage() method
  

  /**
   * Metoda generuje widok listy półek
   * 
   * @global array $aConfig
   * @return type
   */
  private function getShelfList() {
		global $aConfig;
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		
		// zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> _('Remanent półek'),
			'refresh'	=> true,
			'search'	=> true,
			'checkboxes'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'package_ident',
				'content'	=> _('Numer półki'),
				'sortable'	=> true,
				'width' => '200'
			),
      array(
				'db_field'	=> 'deleted',
				'content'	=> _('USUNIĘTA'),
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'created',
				'content'	=> _('Zeskanowano półkę'),
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> _('Zeskanował półkę'),
				'sortable'	=> true,
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
		
		
		// pobranie liczby wszystkich kodow
		$sSql = "SELECT COUNT(DISTINCT R.id) 
						 FROM ".$aConfig['tabls']['prefix']."remanent AS R
             LEFT JOIN remanent_items AS RI
              ON R.id = RI.remanent_id
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND package_ident LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(DISTINCT R.id) 
							 FROM ".$aConfig['tabls']['prefix']."remanent AS R
               LEFT JOIN remanent_items AS RI
                 ON R.id = RI.remanent_id
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND package_ident LIKE \'%'.$_POST['search'].'%\'' : '');
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('sort_history', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			// pobranie zamówień bankowych
			$sSql = "SELECT DISTINCT R.id, package_ident, deleted, created, created_by, IF(RI.id > 0, '1', '0') AS items_exists
							 FROM ".$aConfig['tabls']['prefix']."remanent AS R
               LEFT JOIN remanent_items AS RI
                 ON R.id = RI.remanent_id
							 WHERE 1 = 1 " .
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND id LIKE \'%'.$_POST['search'].'%\'' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' id DESC ').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
      foreach ($aRecords as $iKey => $aRecord) {
        if ($aRecord['items_exists'] == '0') {
          $aRecords[$iKey]['disabled'][] = 'details';
        }
        if ($aRecord['deleted'] == '1') {
          $aRecords[$iKey]['deleted'] = 'USUNIĘTA';
        } else {
          $aRecords[$iKey]['deleted'] = '-';
        }
      }
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'items_exists'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('details', 'delete'),
					'params' => array (
												'details'	=> array('id' => '{id}'),
                        'delete'	=> array('id' => '{id}'),
											)
				) 
 			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		
		return (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show();
  }// end of getShelfList() method
  
  
  /**
   * Metoda wyśw widok pobierania numeru paczki
   * 
   * @param object $pSmarty
   */
  private function GetPackageNumber(&$pSmarty) {
    
    $aData = array();
		if (!empty($_POST)) {
			$aData = & $_POST;
		}
    
    $sHtml = $this->getShelfList();
    
    // wybieramy numer paczki
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('ordered_itm', _("Nowy remanent Baza danych"), array('action'=>phpSelf()), array('col_width'=>155), false);
    $pForm->AddHidden('do', 'setPackage');
    $pForm->AddText('pack_number', _('Numer półki:'), $aData['pack_number']);
    $pForm->AddInputButton('send', _('Dalej'), array('style' => 'font-size: 40px;'), 'submit');
    $sJS = '
    <script type="text/javascript">
      $(function() {
        $("#pack_number").focus();
      });
    </script>';
    $pForm->AddInputButton('get_insufficient', _('Pobierz braki remanentu z tramwaju (stany w zamóweniu vs zczytanie)'), array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'get_insufficient')).'\');'), 'button');
    $pForm->AddInputButton('diff_remanent_streamsoft', _('Pobierz porównanie Remanentu z magazynem Baza w streamsoft '), array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'diff_remanent_streamsoft')).'\');'), 'button');
    $pForm->AddInputButton('get_shelf_not_scanned', _('Pobierz plik z półkami, które nie zostały zeskanowane podczas remanentu a moze coś na nich leżeć'), array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'get_shelf_not_scanned')).'\');'), 'button');

    $pSmarty->assign('sContent', $sHtml . (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()).$sJS);
  }// end of GetPackageNumber() method


    /**
     * @param $pSmarty
     * @return array
     */
    public function diffRemanentStreamsoft($pSmarty) {
        $filePathNewest = $this->getNewestFile();
        if (!empty($filePathNewest)) {
            $fileXML = simplexml_load_file($filePathNewest);
            $productsXML = $this->getXMLToArray($fileXML);
            $productsRemanent = $this->getProductsRemanent();
            $result = $this->compareProducts($productsXML, $productsRemanent);

            $_GET['hideHeader'] = '1';
            header('Content-Encoding: UTF-8');
            header("Content-type: text/html; charset=UTF-8");
            header("Content-Disposition:attachment;filename='export_rem_".date('YmdHis').".xls'");

            echo "\xEF\xBB\xBF";

            echo "Na Remanencie więcej niż na Magazynie Baza: \r\n";
            echo 'EAN;RÓŻNICA'."\r\n";
            foreach ($result['tooMuch'] as $ean => $quantity) {
                echo $ean.";".$quantity."\r\n";
            }
            echo "Na Remanencie mniej niż na Magazynie Baza: \r\n";
            echo 'EAN;RÓŻNICA'."\r\n";
            foreach ($result['notEnough'] as $ean => $quantity) {
                echo $ean.";".$quantity."\r\n";
            }
        } else {
            echo 'Brak pliku źródłowego';
        }
    }

    /**
     * @param $productsXML
     * @param $productsRemanent
     * @return array
     */
    private function compareProducts($productsXML, $productsRemanent) {

        $result = [];
        foreach ($productsRemanent as $ean => $quantity) {
            if (!isset($productsXML[$ean]) || intval($productsXML[$ean]) < intval($quantity)) {
                // za dużo na remanencie
                $result['tooMuch'][$ean] = intval($quantity) - intval($productsXML[$ean]);
            } elseif (intval($productsXML[$ean]) > intval($quantity)) {
                // za mało na remanencie
                $result['notEnough'][$ean] = intval($quantity) - intval($productsXML[$ean]);
            }
        }

        foreach ($productsXML as $ean => $quantity) {
            if (!isset($productsRemanent[$ean])) {
                // za malo - brak produktu na remanencie
                $result['notEnough'][$ean] = $quantity * -1;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    private function getProductsRemanent() {
       $sSql = 'SELECT IF(P.streamsoft_indeks IS NULL, RI.ISBN, P.streamsoft_indeks) as streamsof, SUM(RI.quantity)
                FROM remanent_items AS RI
                LEFT JOIN orders_items AS OI
                  ON OI.id = RI.product_id
                LEFT JOIN products AS P
                  ON OI.product_id = P.id
                JOIN remanent AS R
                  ON RI.remanent_id = R.id 
                  AND R.deleted = "0" 
                GROUP BY streamsof
                  ';
        return $this->pDbMgr->GetAssoc('profit24', $sSql);
    }


    /**
     * @param $fileXML
     * @return array
     */
    private function getXMLToArray($fileXML) {
        $products = [];
        foreach ($fileXML as $item) {
            $indeks = (string)$item->INDEKS;
            $ilosc = (string)$item->ILOSC;
            if (!isset($products[$indeks])) {
                $products[$indeks] += intval(\Common::formatPrice2($ilosc));
            }
        }
        return $products;
    }

    /**
     * @return mixed
     */
    private function getNewestFile() {
        $filesSort = [];

        $filePath = $_SERVER['DOCUMENT_ROOT'].'/LIB/communicator/sources/Internal/streamsoft/import_files/xml/Stany/old/01_*\.xml';
        $files = glob($filePath);
        foreach ($files as $filename) {
            $filesSort[filemtime($filename)] = $filename;
        }
        ksort($filesSort);
        $filePathNewest = array_pop($filesSort);
        return $filePathNewest;
    }
  
  
  /**
   * Metoda dodaje nowy remanent
   * 
   * @param type $sPackageIdent
   * @return type
   */
  private function addNewRemanent($sPackageIdent) {
    
    $aValues = array(
        'package_ident' => $sPackageIdent,
        'created' => 'NOW()',
        'created_by' => $_SESSION['user']['name']
    );
     
    return $this->pDbMgr->Insert('profit24', 'remanent', $aValues);
  }// end of addNewRemanent() method
  
  
	/**
	 * Metoda tworzy formularz z informacjami na temat uzytkownika
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$sPackageIdent identyfikator półki
	 * @return	void
	 */
	function getPackageItems(&$pSmarty, $sPackageIdent) {
		global $aConfig;

		$aLang =& $aConfig['lang'][$this->sModule];

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

    /*
		// pobranie z bazy danych szczegolow konta uzytkownika
		$sSql = "SELECT A.id, A.number, A.saved_type, A.email, A.date_send, A.content, A.send_by
							 FROM ".$aConfig['tabls']['prefix']."orders_send_history AS A
							 WHERE A.id=".$iId;
		$aData =& Common::GetRow($sSql);
		$aData['source'] = in_array($aData['source'], array(0,1,5))?($aData['source']==0?'Profit J':($aData['source']=='1'?'':'Profit M')):$aData['source_name'];
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['details_header_'.$aData['saved_type']],
											 Common::convert($aData['email']));
*/
    // dodajemy już id półki
    $iShelfId = $this->addNewRemanent($sPackageIdent);
    
		$pForm = new FormTable('ordered_itm', $aLang['header'], array('action'=>phpSelf()), array('col_width'=>155), false);
		$pForm->AddHidden('do', 'change_status');
    $pForm->AddHidden('package_ident', $sPackageIdent);
    $pForm->AddHidden('remanent_id', $iShelfId);
		// dane wiadomości
    
    $pForm->AddTextArea('not_exists', $aLang['not_exists'], '', array('rows' => '20', 'style' => 'width: 230px;'), '', FALSE);
    
		// dane do faktury
		$pForm->AddMergedRow($aLang['details_content_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		
		// wyszukiwanie ksiazek 
		$aBooks =& $this->getBooksNew($sPackageIdent);
		if(!empty($aBooks)){
      // wyswietlenie listy pasujacych uzytkownikow
      $pForm->AddMergedRow( $this->GetBooksList($aBooks) );
      if(!empty($aBooks)){
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send2', _('Zapisz'), array('style' => 'font-size: 40px;')));
      }
    } else {
      $this->sMsg .= GetMessage(_('Brak ksiażek na półce '.$sPackageIdent));
    }
		
    $sInput = '
    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" /></div>
    <script type="text/javascript" src="js/validateDiffReport.js"></script>';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sInput.ShowTable($pForm->ShowForm()));
	} // end of Details() function
  
  
  /**
   * Metoda sprawdza czy jest nadwyżka czy nie
   * 
   * @param int $iQuantity
   * @param int $iCurrQuantity
   * @return int
   */
  private function _checkEqual($iQuantity, $iCurrQuantity) {
    $iQuantity = intval($iQuantity);
    $iCurrQuantity = intval($iCurrQuantity);
    
    return $iCurrQuantity-$iQuantity;
  }// end of checkEqual method
  
  
  /**
   * Metoda zmienia status zamówienia na zweryfikowane
   * 
   * @global array $aConfig
   * @param object $pSmarty
   */
	function ChangeStatus(&$pSmarty) {
    
    $iRemanentId = $_POST['remanent_id'];
    
    $_GET['hideHeader'] = '1';
    header('Content-Encoding: UTF-8');
    header("Content-type: text/html; charset=UTF-8");
    header("Content-Disposition:attachment;filename='export_".date('YmdHis').".xls'");
    
    echo "\xEF\xBB\xBF";
    echo "Nadwyżki \n";
    echo $_POST['not_exists'];
    echo "\n\n Niezgodności \n";
    
    $aNotExists = explode("\n", $_POST['not_exists']);
    foreach ($aNotExists as $sItemNotExists) {
      $sItemNotExists = trim($sItemNotExists);
      if ($sItemNotExists != '') {
        $this->addRemanentItem($iRemanentId, 0, 1, $sItemNotExists, 'NULL');
      }
    }
    
    $aNewTmpArray = array(
        'Isbn',
        'Tytuł',
        'Wydawca',
        'Autorzy',
        'Rok wydania',
        'Zamówiono',
        'Odznaczono',
        'Różnica'
    );
    echo implode(';', $aNewTmpArray)."\n";
    // zestawiamy z produktami
    
    
    
    $aBooksList = $this->getBooksDiff($_POST['package_ident']);
    foreach ($aBooksList as $iKey => $aBook) {
      $iCurrQuantity = 0;
      if (isset($_POST['editable'][$aBook['id']]) && isset($_POST['editable'][$aBook['id']]['currquantity'])) {
        $iCurrQuantity = intval($_POST['editable'][$aBook['id']]['currquantity']);
      }
      
      $iDiff = $this->_checkEqual($aBook['quantity'], $iCurrQuantity);
      
      $this->addRemanentItem($iRemanentId, $aBook['quantity'], $iCurrQuantity, $aBook['isbn'], $aBook['id']);
      
      $aNewTmpArray = array(
          'Isbn' => $aBook['isbn'],
          'Tytuł' => $aBook['name'],
          'Wydawca' => $aBook['publisher'],
          'Autorzy' => $aBook['authors'],
          'Rok wydania' => $aBook['publication_year'],
          'Zamówiono' => $aBook['quantity'],
          'Odznaczono' => $iCurrQuantity,
          'Różnica' => $iDiff
      );
      // uwaga pokazujemy tylko te niezgodne
      if ($iDiff != 0) {
        echo implode(';', $aNewTmpArray)."\n";
      }

    }
	}// end of ChangeStatus() method
  

  /**
   * Metoda dodaje nową składową remanentu, niezależnie czy jest to nadwyżka, czy nie
   * 
   * @param int $iRemanentId
   * @param int $iExpectedQuantity
   * @param int $iQuantity
   * @param string $sISBN
   * @param int $iProductId
   * @return int
   */
  private function addRemanentItem($iRemanentId, $iExpectedQuantity, $iQuantity, $sISBN, $iProductId = 'NULL') {
    
    $aValues = array(
        'remanent_id' => $iRemanentId,
        'product_id' => $iProductId,
        'ISBN' => $sISBN,
        'expected_quantity' => $iExpectedQuantity,
        'quantity' => $iQuantity
    );
    return $this->pDbMgr->Insert('profit24', 'remanent_items', $aValues);
  }// end of addRemanentItem() method
  
	
  /**
   * Convert BR tags to nl
   * @source: http://php.net/manual/en/function.nl2br.php
   *
   * @param string The string to convert
   * @return string The converted string
   */
  function br2nl($string)
  {
      return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
  }
  
	/**
	 * Metoda wyswietla liste ksiazek z tablicy $aBooks
	 *
	 * @param		object	$pSmarty
	 * @param	array ref	$aBooks	- lista produktów
	 * @return	void
	 */
	function GetBooksList(&$aBooks) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/EditableView.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'checkboxes'	=> true,
			'editable' => true,
			'count_checkboxes' => true,
			'count_checkboxes_by' => 'quantity',
			'form'	=>	false,
      'count_header' => true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
      array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '20'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_cover']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_name']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_quantity'],
        'style' => 'font-size: 20px!important; font-weight: bold;'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_curr_quantity'],
        'style' => 'font-size: 20px!important; font-weight: bold;'
			)
    );

		$pView = new EditableView('ordered_itm', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array(
			'id' => array(
				'show'	=> false
			),
			'currquantity'=> array(
					'editable'	=> true
			),
		);
			// ustawienia dla kolumn edytowalnych
		$aEditableSettings = array(
			'currquantity' => array(
				'type' => 'text'
			)
		);
    foreach ($aBooks as $iKey => $aBook) {
      $aBooks[$iKey]['currquantity'] = '0';
    }
		// dodanie rekordow do widoku
		$pView->AddRecords($aBooks, $aColSettings,$aEditableSettings);
		// dodanie stopki do widoku
    
		// przepisanie langa - info o braku rekordow
		$aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_books_to_show'];
				// walidacja js kolumn edytowalnych
		$sAdditionalScript='';
		foreach($aBooks as $iKey=>$aItem) {
			if($aItem['weight']=='0,00')	
				$sAdditionalScript.='document.getElementById(\'delete['.$aItem['id'].']\').disabled=true;
				';		
			}		
		$sJS = '
		<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
		'.$sAdditionalScript.'
			$("#ordered_itm").submit(function() {
        if ($("input[name^=\'delete\']:not(:checked)").length === 0) {
          return true;
        } else {
          return true;
//          alert("Błąd zatwierdzania zamówienia, nie zatwierdzono wszystkich pozycji !!!");
        }
        return false;
			});
		});
	// ]]>
</script>
		';
		return $sJS.$pView->Show();
	} // end of GetBooksList() function
  
  
  /**
   * Metoda pobiera liste ksiazek dla wygenerowanego wydruku
   * 
   * @param string $sPackageIdent numer półki
   * @return array - lista ksiazek
   */
	function &getBooksNew($sPackageIdent) {
		global $aConfig;

    /*
        $sSql = "
        SELECT 
          OI.id, PI.photo, OI.order_id,
											OI.isbn, 
                      OI.name, SUM(OI.quantity) as quantity, OI.publisher, OI.authors, OI.publication_year, OI.edition,
                      PI.directory
        FROM orders_items AS OI
        JOIN  `orders` AS O ON OI.order_id = O.id
        LEFT JOIN products_images AS PI
          ON PI.product_id = OI.product_id
        LEFT JOIN orders_send_history_items AS OSHI
          ON OSHI.item_id = OI.id
        LEFT JOIN orders_send_history AS OSH
          ON OSH.id = OSHI.send_history_id
        WHERE O.order_status <> '3'
          AND O.order_status <> '4'
          AND O.order_status <> '5'
          AND OI.status = '4'
          AND OI.item_type <> 'A'
          AND OI.deleted <> '1'
          AND OI.source <> '5'
          AND 
          ( SELECT OSH.id
            FROM orders_send_history AS OSH
            JOIN orders_send_history_items AS OSHI
              ON OSH.id = OSHI.send_history_id
            WHERE OSHI.item_id = OI.id AND OSH.pack_number = '".$sPackageIdent."'
            ORDER BY OSH.id DESC
            LIMIT 1 ) IS NOT NULL
        GROUP BY OI.isbn
        ORDER BY SUM(OI.quantity) DESC
        ";
			$aBooks = Common::GetAll($sSql);
			*/
      $aBooks = $this->getBooksDiff($sPackageIdent);
			foreach($aBooks as $iKey=>$aItem){
				$aBooks[$iKey]['name'] = $aItem['name'].
											($aItem['isbn']?'<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="'.$aItem['isbn'].'" item_id="'.$aItem['id'].'">'.$aItem['isbn'].'</span>':'').
											($aItem['publication_year']?'<br />'.$aItem['publication_year']:'').($aItem['edition']?' wyd. '.$aItem['edition']:'').
											($aItem['publisher']?'<br />'.$aItem['publisher']:'').
											($aItem['authors']?'<br />'.$aItem['authors']:'').'</span>';
				unset($aBooks[$iKey]['isbn']);
				unset($aBooks[$iKey]['publication_year']);
				unset($aBooks[$iKey]['edition']);
				unset($aBooks[$iKey]['publisher']);
				unset($aBooks[$iKey]['authors']);
        
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aItem['photo'])){
          $aBooks[$iKey]['photo'] = '<img src="/images/photos/'.$aItem['photo'].'" />';	
				} else {
          $aBooks[$iKey]['photo'] = 'BRAK OKŁADKI';
        }
        unset($aBooks[$iKey]['directory']);
				unset($aBooks[$iKey]['order_id']);
			}
		
		return $aBooks;
	} // end of getBooksNew() method
  
  
  /**
   * Metoda pobiera liste ksiazek dla wygenerowanego wydruku
   * 
   * @param int $iId id zamówienia
   * @return array - lista ksiazek
   */
	function &getBooksDiff($sPackageIdent) {
		global $aConfig;

    /*
        $sSql = "
        SELECT OI.id, OI.isbn, OI.name, SUM(OI.quantity) as quantity, OI.publisher, OI.authors, OI.publication_year, OI.edition
        FROM orders_items AS OI
        JOIN  `orders` AS O ON OI.order_id = O.id
        LEFT JOIN products_images AS PI
          ON PI.product_id = OI.product_id
        WHERE O.order_status <> '3'
          AND O.order_status <> '4'
          AND O.order_status <> '5'
          AND OI.status = '4'
          AND OI.item_type <> 'A'
          AND OI.deleted <> '1'
        GROUP BY OI.isbn
        ORDER BY SUM(OI.quantity) DESC
        ";
        */
        $sSql = "
        SELECT 
          DISTINCT OI.id, 
          (SELECT CONCAT(PI.directory, '/__t_', PI.photo) FROM products_images AS PI WHERE PI.product_id = OI.product_id LIMIT 1) AS photo,
          OI.order_id,
											OI.isbn, 
                      OI.name, SUM(OI.quantity) as quantity, OI.publisher, OI.authors, OI.publication_year, OI.edition
        FROM orders_items AS OI
        JOIN  `orders` AS O ON OI.order_id = O.id
        WHERE O.order_status <> '3'
          AND O.order_status <> '4'
          AND O.order_status <> '5'
          AND OI.status = '4'
          AND OI.item_type <> 'A'
          AND OI.deleted <> '1'
          AND OI.source <> '5'
          AND 
          ( SELECT OSH.pack_number
            FROM orders_send_history AS OSH
            JOIN orders_send_history_items AS OSHI
              ON OSH.id = OSHI.send_history_id
            WHERE OSHI.item_id = OI.id
            ORDER BY OSH.id DESC
            LIMIT 1 ) = '".$sPackageIdent."'
        GROUP BY OI.isbn
        ORDER BY OI.quantity DESC
        ";
		//dump($sSql);
		return Common::GetAll($sSql);
	} // end of getBooksNew() method
} // end of Module Class