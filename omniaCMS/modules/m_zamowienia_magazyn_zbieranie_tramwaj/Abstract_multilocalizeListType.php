<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj;

use Admin;
use Common_get_ready_orders_books;
use DatabaseManager;
use FormTable;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\SellPredict;
use LIB\EntityManager\Entites\StockLocation;
use LIB\orders\listType\filters\ordersItemsLimit;
use LIB\orders\listType\manageGetOrdersItemsList;
use LIB\orders_semaphore\collectingSemaphore;
use Memcached;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;

require_once('Common_get_ready_orders_books.class.php');
/**
 * Description of Module_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Abstract_multilocalizeListType extends Abstract_getListType implements ModuleEntity
{

    const COLLECTING_TYPE_MULTILOCALIZATION = 2;

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected $sMsg;
  
  /**
   *
   * @var Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  /**
   *
   * @var DatabaseManager
   */
  protected $pDbMgr;
  
  /**
   *
   * @var manageGetOrdersItemsList 
   */
  protected $oManageLists;
  
  protected $cListType;

  protected $stockLocation;
    protected $sellPredict;

    public function __construct(Admin $oClassRepository) {
      parent::__construct($oClassRepository);
  }

  /**
   * 
   * @return string
   */
  public function doDefault() {
    return $this->Show();
  }
  
  /**
   * 
   */
  public function doGetCountOrders() {
    
    $aResults = $this->oManageLists->getCountOrdersGroupByTransport($_POST['shipment_date_expected'], $_POST['transport_id'], false);
    $this->sMsg .= GetMessage(sprintf(_('Ilość zamówień %s'), array_sum($aResults)), false);
    return $this->Show();
  }
  
  /**
   * 
   */
  public function doSave_list_data() {
    $this->saveListData($this->pSmarty);
  }
  
  protected  function getType() {
    return $this->cListType;
  }

    /**
     * @param $websiteId
     * @return array
     */
  protected function getWebsitesOtherThen($websiteId) {
      $sSql = 'SELECT id FROM websites WHERE id <> '.$websiteId;
      return $this->pDbMgr->GetCol('profit24', $sSql);
  }
  
  /**
   * 
   * @return boolean
   */
  public function doGenerate() {
      global $aConfig;

    $bIsErr = false;


      $oMemcache = new Memcached();
      $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
      $this->collectingSemaphore = new collectingSemaphore($oMemcache);

      if (false === $this->collectingSemaphore->isLocked(self::COLLECTING_TYPE_MULTILOCALIZATION)) {
          $this->collectingSemaphore->lock(self::COLLECTING_TYPE_MULTILOCALIZATION);
      } else {
          $bLocked = true;
          for ($i = 0; $i < 4; $i++) {
              sleep(3);
              if (false === $this->collectingSemaphore->isLocked(self::COLLECTING_TYPE_MULTILOCALIZATION)) {
                  $bLocked = false;
                  $this->collectingSemaphore->lock(self::COLLECTING_TYPE_MULTILOCALIZATION);
                  break;
              }
          }
          if (true === $bLocked) {
              $sMsg = _('Zbieranie zamówień jest aktualnie zablokowane przez inną osobę.<br /> Spróbuj ponownie za chwilę');
              AddLog($sMsg);
              $this->sMsg = GetMessage($sMsg);
              $this->Show();
              return;
          }
      }

    $aOrdersItems = $this->oManageLists->getOrdersItemsListsType($_POST['shipment_date_expected'], $_POST['transport_id'], true);
    $aOItems = $this->getOrdersItemsList($aOrdersItems);
    $aOItems = $this->updateMinimumAvailableMagazineQuantity($aOItems);

    $sPackageNumber = $_POST['package_number'];
    $iTransportId = $_POST['transport_id'];
    
    if (!empty($aOItems)) {
      $this->pDbMgr->BeginTransaction('profit24');
      $mReturn = $this->doCreateList($aOItems, $this->getType(), $this->oManageLists->getGetFromTrain(), $sPackageNumber, $iTransportId, base64_encode(serialize($aDebug)));
      if ($mReturn === FALSE || $mReturn <= 0) {
        $bIsErr = true;
      } else {
        $iILId = intval($mReturn);
      }
      
      // tutaj takżę powinniśmy potwierdzić te zamówienia jak po sortowniku
    } else {
      $bIsErr = true;
      // błąd
      $this->pDbMgr->RollbackTransaction('profit24');
      $this->collectingSemaphore->unlock(self::COLLECTING_TYPE_MULTILOCALIZATION);
      $sMsg = _('Brak produktów do zbierania.');
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      $this->Show();
      return;
    }
    
    if ($bIsErr === false) {
      $this->pDbMgr->CommitTransaction('profit24');
      $this->collectingSemaphore->unlock(self::COLLECTING_TYPE_MULTILOCALIZATION);
      return $this->getProductsScanner($this->pSmarty, $iILId, $aOItems, $sPackageNumber);
    } else {
      // błąd
      $this->pDbMgr->RollbackTransaction('profit24');
      $this->collectingSemaphore->unlock(self::COLLECTING_TYPE_MULTILOCALIZATION);
      switch ($bIsErr) {
        case -1:
          $sMsg = _('Podana kuweta jest zablokowana');
        break;
        case -2:
          $sMsg = _('Brak takiej kuwety');
        break;
        default:
          $sMsg = _('Wystąpił błąd podczas pobierania listy do zebrania');
        break;
      }
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      return $this->Show();
    }
  }

    /**
     * @param $aOItems
     */
  private function updateMinimumAvailableMagazineQuantity($aOItems){
      $this->stockLocation = new StockLocation($this->pDbMgr);
      $this->sellPredict = new SellPredict($this->pDbMgr);

      foreach ($aOItems as $iKey => $aOItem) {
          if (empty($aOItem['magazine_stock_localizations_reserved'])) {
              // nie zarezerwowaliśmy towaru - nie zbieramy
              unset($aOItems[$iKey]);

              $availableMagazineStockLocation = $this->stockLocation->getProductMagazineAvailableQuantity($aOItem['product_id'], Magazine::TYPE_HIGH_STOCK_SUPPLIES);
              if ($availableMagazineStockLocation > 0) {
                  $this->sellPredict->updateQuantity($aOItem['SP_id'], $availableMagazineStockLocation);
              } else {
                  \Common::sendMail('kontakt@profit24.pl', 'kontakt@profit24.pl', 'a.golba@profit24.pl;k.malz@profit24.pl',
                      'Brak produktu na lokalizacji na doładowaniach', 'Brak produktu o '.$aOItem['ean_13'].' - '.$aOItem['name']);
              }
          }
      }
      return $aOItems;
  }

    /**
     * @return string
     */
  protected function Show() {
    
    $aData = array();
    if (isset($_POST)) {
      $aData = $_POST;
    }
    
    $aQuantityLists = $this->getCountLists();
    if (!empty($aQuantityLists)) {
      $sQuantity = '<ul>';
      foreach ($aQuantityLists as $aRow) {
        $sQuantity .= '<li style="font-size: 18px; font-weight: bold;">'.$aRow['name'] .': '. $aRow['count_orders'].' ('.$aRow['count_orders_items'].')'.'</li>';
      }
      $sQuantity .= '</ul>';
    } else {
      $sQuantity = _('Brak zamówień');
    }
    
//    $bManageReadySend = $this->checkManageGetReadySend($_SESSION['user']);
    
    include_once('Form/FormTable.class.php');
    $aAddData = [];
    if (isset($_GET['ws']) && $_GET['ws'] != '') {
        $aAddData['ws'] = $_GET['ws'];
    }
      if (isset($_GET['type']) && $_GET['type'] != '') {
          $aAddData['type'] = $_GET['type'];
      }
      if (isset($_GET['is_double']) && $_GET['is_double'] != '') {
          $aAddData['is_double'] = $_GET['is_double'];
      }

      if (isset($_GET['magazine']) && $_GET['magazine'] != '') {
        $aAddData['magazine'] = $_GET['magazine'];
      }

    if (isset($_GET['is_available_employee']) && $_GET['is_available_employee'] != '') {
      $aAddData['is_available_employee'] = $_GET['is_available_employee'];
    }


    $pForm = new FormTable('ordered_itm', _("Pobierz listę ").$this->sListName, array('action'=>phpSelf($aAddData), 'target' => '_blank'), array('col_width'=>355), true);
    $pForm->AddHidden('do', 'generate');
    $pForm->AddHidden('type', $this->getType());
//    if ($this->checkIsContainer($this->cListType)) {
//        $pForm->AddText('package_number', _('Numer kuwety'), '', array('style' => 'font-size: 40px;'), '', 'uint', true);
//    }
//    $pForm->AddHidden('max_order_item', '1');
//    $pForm->AddHidden('min_order_item', '1');
    
    $bManageReadySend = true;
    if ($bManageReadySend === true) {
      $pForm->AddMergedRow('Pobierz listę', array('class' => 'merged'));

      if ('1' == $this->checkShowCompletionDate()) {
          // dzień do którego pobieramy zamówienia
          $pForm->AddText('shipment_date_expected', _('Do daty wysyłki'), (!isset($aData['shipment_date_expected']) ? '' : (empty($aData['shipment_date_expected']) ? '00-00-0000' : $aData['shipment_date_expected'])), array('style' => 'width: 75px;'), '', 'date', false);
      } else {
          $pForm->AddHidden('shipment_date_expected', date('d-m-Y'));
      }

//      $pForm->AddCheckBoxSet('transport_id', _('Metoda transportu'), $aTransportRadio, $aData['transport_id']);

        if ($this->checkManageGetReadySend($_SESSION['user'])) {
            $aFilters = $this->oManageLists->getFilters();
            foreach ($aFilters as $filter) {
                if ($filter instanceof ordersItemsLimit) {
                    $pForm->AddText('orders_items_limit', _('Limit ilości produktów na liście'), (empty($aData['orders_items_limit']) ? '120' : $aData['orders_items_limit']), array('style' => 'width: 75px;'), '', 'text', true);
                }
            }
        }

    } else {
      // ukryte pole do metody transportu
//      $iMaxTransportId = $this->getMaxQuantityTransportId($aQuantityLists);
//      if ($iMaxTransportId > 0) {
//        $pForm->AddHidden('transport_id', $iMaxTransportId);
//      }
    }

    $pForm->AddInputButton("generate", _('Pobierz listę'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');

    if ($bManageReadySend === true) {
      $pForm->AddInputButton("getCountOrders", _('Sprawdź ilość zamówień / ПОКАЗАТЬ КОЛИЧЕСТВО ЗАКАЗОВ'), array('onclick' => '$(\'#do\').val(\'getCountOrders\'); submit(); ', 'style' => 'font-size: 40px; font-weight: bold; color: green;'), 'button');


//      $pForm->AddMergedRow('Statystyki', array('class' => 'merged'));

//      $iCountInCompletation = $this->getCountInCompletation(array('1'));
//      $pForm->AddRow('Ilość list w trakcie kompletowania : ', '<span style="font-size: 18; font-weight: bold;">'.$iCountInCompletation.'</span>');
//
//      $iCountInSort = $this->getCountInSort(array('1'));
//      $pForm->AddRow('Ilość list w trakcie sortowania : ', '<span style="font-size: 18; font-weight: bold;">'.$iCountInSort.'</span>');
    }
    
    $sJS = '
      <script type="text/javascript">
        $( function () {
          $("#package_number").focus();
        });
      </script>
      
      ';
    return ShowTable($pForm->ShowForm()).$sJS;
  }

  protected function checkShowCompletionDate() {

      $sSql = 'SELECT show_completion_date 
               FROM orders_magazine_settings';
      return $this->pDbMgr->GetOne('profit24', $sSql);
  }
  
  /**
   * Metoda pobiera tablicę do radio
   * 
   * @param array $aArray
   * @return array
   */
  protected function getRadioByArr($aArray) {
    
    $aRadio = array();
    if (!empty($aArray)) {
      foreach($aArray as $iKey => $aItem) {
        $aRadio[$iKey]['label'] = $aItem['name'];
        $aRadio[$iKey]['value'] = $aItem['id'];
      }
    }
    return $aRadio;
  }// end of getRadioByArr() method
  
  /**
   * 
   * @return array
   */
  protected  function getCountLists() {
    $aReturnCount = array();
    $aOrdersTransportCounts = $this->oManageLists->getCountOrdersItemsGroupByTransport();
    if (!empty($aOrdersTransportCounts)) {
      $sSql = 'SELECT id, name FROM orders_transport_means WHERE id IN ('.implode(', ', array_keys($aOrdersTransportCounts)).')';
      $aTransports = $this->pDbMgr->GetAll('profit24', $sSql);
      $this->_aTransports = $aTransports;

      foreach ($aTransports as $aTransport) {
        $aReturnCount[$aTransport['id']]['name'] = $aTransport['name'];
        $aReturnCount[$aTransport['id']]['count_orders'] = $aOrdersTransportCounts[$aTransport['id']]['count_orders'];
        $aReturnCount[$aTransport['id']]['count_orders_items'] = $aOrdersTransportCounts[$aTransport['id']]['count_orders_items'];
      }
    }
    return $aReturnCount;
  }
  
  /**
   * Metoda sprawdza, czy istnieje maksymalna wartość pomiędzy metodami transprotu,
   *  jesli istnieje, zaznacza opcję domyślnie
   * 
   * @param array $aQuantityLists
   * @param string $aTransportRadio
   * @return string
   */
  protected function checkMaximumValueTransport($aQuantityLists, $aTransportRadio) {
    
    foreach ($aTransportRadio as $iKey => $aTransport) {
      if (empty($_POST)) {
        $aTransportRadio[$iKey]['checked'] = 'checked';
      }
    }
    return $aTransportRadio;
  }// end of checkMaximumValueTransport() method
  
  public function getMsg() {
    return $this->sMsg;
  }
}