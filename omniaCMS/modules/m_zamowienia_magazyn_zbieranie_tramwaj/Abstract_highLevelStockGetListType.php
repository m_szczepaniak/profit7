<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj;

use Admin;
use magazine\HighLevelStock;
use omniaCMS\lib\interfaces\ModuleEntity;

require_once('Common_get_ready_orders_books.class.php');

/**
 * Description of Module_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Abstract_highLevelStockGetListType extends Abstract_getListType implements ModuleEntity
{

    protected $cListType;

    protected $destinationMagazineDeficiencies;

    public function __construct(Admin $oClassRepository)
    {
        parent::__construct($oClassRepository);
    }


    /**
     * Metoda tworzy listę do zebrania na podstawie przekazanej tablicy danych
     *
     * @param array $aList
     * @param char $cListType - przyjmuje typy:
     *      '0' - zwykłe
     *      '1' - single
     *      '2' - wysyłka razem
     *      '4' - anulowane
     *      '5' - wysokie składowanie
     * @param bool $bGetFromTrain
     * @param string(uint) $sPackageNumber - numer kuwety
     * @param mixed $mTransportId - id metody transportu
     * @param string $sDebug
     * @return bool
     */
    final public function doCreateList($aList, $cListType, $bGetFromTrain, $sPackageNumber, $mTransportId, $sDebug = '')
    {
        global $aConfig;
        $bIsErr = false;
        include_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/magazine/HighLevelStock.class.php');
        if (!$this->highLevelStock instanceof HighLevelStock) {
            $this->highLevelStock = new HighLevelStock($this->pDbMgr, $this->destinationMagazineDeficiencies);
        }


        include_once($aConfig['common']['client_base_path'] . 'LIB/autoloader.php');
        foreach ($this->aOrdersAccepted as $iOrderId => $sVal) {
            // zmieniamy składową w zamówieniu
        }

        // dodajemy teraz dane listy
        $aValuesOIL = array(
            'user_id' => $_SESSION['user']['id'],
            'document_number' => ($this->pDbMgr->GetOne('profit24', 'SELECT MAX( id ) FROM orders_items_lists') + 1),
            'package_number' => $sPackageNumber,
            'closed' => '0',
            'type' => $cListType,
            'destination_magazine' => $this->destinationMagazineDeficiencies != ''? $this->destinationMagazineDeficiencies : 'SH',
            'get_from_train' => $bGetFromTrain,
            'created' => 'NOW()',
            'debug' => $sDebug,
            'big' => (stristr($_GET['action'], '_big') ? '1' : '0')
        );
        if (is_array($mTransportId)) {
            $aValuesOIL['transport_id'] = 'NULL';
        } elseif ($mTransportId <= 0) {
            $aValuesOIL['transport_id'] = 'NULL';
        } else {
            $aValuesOIL['transport_id'] = $mTransportId;
        }
        $iILId = $this->pDbMgr->Insert('profit24', 'orders_items_lists', $aValuesOIL);

        if ($iILId > 0) {

            // dodajemy transport
            if ($this->addOILTransport($iILId, $mTransportId) === false) {
                $bIsErr = true;
            }

            // dodajemy składowe
            foreach ($aList as $aItem) {
                $this->highLevelStock->markCollecting($aItem['CHL_id']);

                if ($aItem['item_type'] == 'I' || $aItem['item_type'] == 'P') {
                    $aValuesOILI = array(
                        'orders_items_lists_id' => $iILId,
                        'orders_items_id' => $aItem['id'],
                        'orders_id' => $aItem['order_id'],
                        'products_id' => $aItem['product_id'],
                        'quantity' => $aItem['quantity'],
                        'is_g' => $aItem['is_g'],
                        'isbn_plain' => $aItem['isbn_plain'],
                        'isbn_10' => $aItem['isbn_10'],
                        'isbn_13' => $aItem['isbn_13'],
                        'ean_13' => $aItem['ean_13'],
                    );
                    if ($this->pDbMgr->Insert('profit24', 'orders_items_lists_items', $aValuesOILI) === FALSE) {
                        $bIsErr = true;
                    }
                }
            }
        }
        if ($bIsErr === TRUE) {
            return false;
        } else {
            return $iILId;
        }
    }// end of doCreateList() method


    /**
     * Metoda zapisuje dane z zebranej listy
     *
     * @param type $pSmarty
     * @return \omniaCMS\modules\m_zamowienia_magazyn_przyjecia\HTML
     */
    protected function saveListData($pSmarty)
    {
        include_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/magazine/HighLevelStock.class.php');
        if (!$this->highLevelStock instanceof HighLevelStock) {
            $this->highLevelStock = new HighLevelStock($this->pDbMgr, $this->destinationMagazineDeficiencies);
        }

        $bIsErr = false;

        // ok mamy sobie dwie tablice
        $aBooksScanned = json_decode($_POST['books']);
        $aDeficienciesBooks = json_decode($_POST['not_scanned_books']);
        $iOLIId = json_decode($_POST['orders_items_lists_id']);


        if (empty($aBooksScanned)) {
            $_POST['books'] = preg_replace("/([{,])([a-zA-Z][^: ]+):/", "$1\"$2\":", $_POST['books']);
            $aBooksScanned = json_decode($_POST['books']);
        }

        if (empty($aDeficienciesBooks)) {
            $_POST['not_scanned_books'] = preg_replace("/([{,])([a-zA-Z][^: ]+):/", "$1\"$2\":", $_POST['not_scanned_books']);
            $aDeficienciesBooks = json_decode($_POST['not_scanned_books']);
        }


        if (!empty($aBooksScanned)) {
            foreach ($aBooksScanned as $aBook) {
                if ($this->AddOILITimes($iOLIId, $aBook->ident, $aBook->time, $aBook->currquantity, '1') === false) {
                    $bIsErr = true;
                }
            }
        }

        if (!empty($aDeficienciesBooks)) {
            foreach ($aDeficienciesBooks as $aBook) {
                if (isset($aBook->time) && $aBook->time > 0) {
                    $aBook->time = 0;
                }
                if ($this->AddOILITimes($iOLIId, $aBook->ident, $aBook->time, $aBook->currquantity, '1') === false) {
                    $bIsErr = true;
                }
            }
        }



        if (false === $this->closeListCompletation($iOLIId)) {
            $bIsErr = true;
        }

        try {
            $this->backDeficienciesToCompletation($aDeficienciesBooks, $iOLIId);
            $this->markDeactivateScanned($aBooksScanned, $iOLIId);
        } catch (\Exception $ex) {
            AddLog($ex->getMessage());
            echo '<script type="text/javascript">alert("' . $ex->getMessage() . '");</script>';
            return false;
        }



        if ($bIsErr === false) {
            echo '<script type="text/javascript">window.close();</script>';
            die;
        } else {
            echo '<script type="text/javascript">alert("Wystąpił błąd zapisywania czasów listy !"); window.close();</script>';
            die;
        }
    }

    /**
     * @param $iListId
     * @return bool
     */
    public function closeListCompletation($iListId) {

        $value = [
            'closed' => '1'
        ];
        if (false === $this->pDbMgr->Update('profit24', 'orders_items_lists', $value, ' id = '.$iListId)) {
            return false;
        }
        return true;
    }

    private function backDeficienciesToCompletation($aDeficienciesBooks, $iOLIId)
    {
        /**
         *
        stdClass Object
        (
            [weight] => 0.90
            [authors] =>
            [publisher] => EGMONT
            [publication_year] => 2015
            [photo] => okladki/868/867649|9788328110465.jpg
            [ident] => 663,1874
            [quantity] => 2
            [product_id] => 867649
            [name] => Batman
            [pack_number] => 1507007042
            [isbn_plain] => 9788328110465
            [isbn_10] =>
            [isbn_13] =>
            [ean_13] => 9788328110465
            [shelf_number] => 1507007042
            [checked] => 0
            [currquantity] => 1
            [time] => 8059
        )
         */
        foreach ($aDeficienciesBooks as $books) {
            if (intval($books->currquantity) <= 0) {
                // nic nie zeskanowano idzie spowrotem do zebrania
                $this->highLevelStock->unmarkCollecting($books->CHL_id);
            } else {
                $this->highLevelStock->deactivateHighLevelStock($books->CHL_id);
            }
        }
    }

    private function markDeactivateScanned($aBooksScanned, $iOLIId)
    {
        foreach ($aBooksScanned as $books) {
            $this->highLevelStock->deactivateHighLevelStock($books->CHL_id);
        }
    }

    protected function getOverrideJS() {
        return '<script type="text/javascript">
                    isHighLevelStock = true;
                    $(function() {
                        $(".main_profit_g_act_stock").show();
                    });
                </script>';

    }
}
