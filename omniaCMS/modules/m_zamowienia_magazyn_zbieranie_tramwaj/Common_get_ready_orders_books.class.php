<?php

use LIB\communicator\sources\Internal\streamsoft\MMdocumentCanceledList;
use LIB\EntityManager\Entites\StockLocation;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use magazine\Containers;
use magazine\HighLevelStock;
use omniaCMS\modules\m_zamowienia_magazyn_przyjecia\Module__zamowienia_magazyn_przyjecia__unpacking;
use omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj\CanceledList;
use orders\Shipment;
use orders_containers\OrdersContainers;
/**
 * Klasa wspólna dla pobierania zamówień i produktów gotowych do wysyłki
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-10-08
 * @copyrights Marcin Chudy - Profit24.pl
 */

require_once('tcpdf/tcpdf.php');

function sortItems($a, $b) {

  if ($a['pack_number'] != '' && $b['pack_number'] != '') {
    $firstNumberPrefix = substr($a['pack_number'], 0, 2);
    $lastNumberPrefix = substr($b['pack_number'], 0, 2);

      if ($firstNumberPrefix == '20' && $lastNumberPrefix == '17') {
          return -1;
      } elseif ($firstNumberPrefix == '17' && $lastNumberPrefix == '20') {
          return 1;
      }

      if ($firstNumberPrefix == '20' && $lastNumberPrefix == '15') {
          return -1;
      } elseif ($firstNumberPrefix == '15' && $lastNumberPrefix == '20') {
          return 1;
      }


    if ($firstNumberPrefix == '17' && $lastNumberPrefix == '15') {
      return -1;
    } elseif ($firstNumberPrefix == '15' && $lastNumberPrefix == '17') {
      return 1;
    }

    if ($firstNumberPrefix == '17' && $lastNumberPrefix == '17') {
        $packA = intval($a['pack_number']);
        $packB = intval($b['pack_number']);
        if ($packA > $packB) {
            return -1;
        } elseif ($packA < $packB) {
            return 1;
        } else {
            // ta sama półka
            return strcmp($a['name'], $b['name']);
        }
    } else {
        $packA = intval($a['pack_number']);
        $packB = intval($b['pack_number']);
        if ($packA > $packB) {
            return 1;
        } elseif ($packA < $packB) {
            return -1;
        } else {
            // ta sama półka
            return strcmp($a['name'], $b['name']);
        }
    }
  } else if ($a['pack_number'] != '') {
    // tylko a wypełnione
    return -1;
  } else if ($b['pack_number'] != ''){
    // tylko b wypelnione
    return 1;
  } else {
    // żadne nie jest wypełnione
    return strcmp($a['name'], $b['name']);
  }
}

class MYPDF extends TCPDF {
    public $headerImage;
    public $headerText;

    //Page header
    public function Header() {
        // Logo
        //$image_file = K_PATH_IMAGES.'logo_example.jpg';
        $this->Image('@'.$this->headerImage, 129);
        // Set font
        //$this->SetFont('helvetica', 'B', 20);
        // Title
        $this->writeHTMLCell(0,0,1,1, $this->headerText, 0, 1, 0, true, '', true);
    }
}

class Common_get_ready_orders_books {

    /**
     * @var bool
     */
    protected $isSaved = false;


  /**
   *
   * @var DatabaseManager
   */

  /**
   *
   * @var Admin
   */
  protected $oParent;

  /**
   *
   * @var int
   */
  protected $iUId;

    /**
     * @var HighLevelStock
     */
  protected $highLevelStock;

  protected $destinationMagazineDeficiencies;

  function __construct($oParent) {
    $this->oParent = $oParent;
    $this->iUId = $_SESSION['user']['id'];
    $this->pDbMgr = $oParent->pDbMgr;
    include_once($_SERVER['DOCUMENT_ROOT'].'LIB/magazine/HighLevelStock.class.php');

    $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_HIGH_STOCK_SUPPLIES;
    if (isset($_GET['magazine']) && $_GET['magazine'] == \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES) {
      $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES;
    }
    if (isset($_POST['magazine']) && $_POST['magazine'] == \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES) {
      $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES;
    }
    $this->highLevelStock = new HighLevelStock($this->pDbMgr, $this->destinationMagazineDeficiencies);
  }

  /**
   *
   * @param DatabaseManager $pDbMgr
   */
  public function setDbMgr(DatabaseManager $pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }

  /**
   * Metoda ustawia wartość pozycji które mogą zostać razem zebrane
   *
   * @return void
   */
  protected function setCountAvailableLinkedTransport() {
    unset($this->aOrdersAccepted);
    unset($this->aOrdersWaiting);

    $sSql = 'SELECT DISTINCT main_order_id, "1" as key_
             FROM orders_linked_transport';
    $aMainOrderIds = $this->pDbMgr->GetAssoc('profit24', $sSql);
    array_unique($aMainOrderIds);


    $aPacketList = $this->getRecordsListLimited(NULL, NULL, NULL, NULL, '', true, $aMainOrderIds, true);
    // tablice globalne poniżej są uzupełniane
    $aList = $this->countOrders($aPacketList, 99999);
  }// end of setCountAvailableLinkedTransport() method


  /**
   * Metoda pobiera punkt odbioru paczki, jeśli wybrany zostal paczkomat
   *
   * @param int $iOrderId
   * @return string
   */
  protected function getPointOfReceipt($iOrderId) {

    $sSql = 'SELECT O.point_of_receipt
               FROM orders AS O
               WHERE O.id = '.$iOrderId;
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of getPaczkomatyPointOfReceipt() method

    private function getOrderListItemStockLocation($iOILIId,$sl) {

        $sSql = 'SELECT A.id
                 FROM stock_location_orders_items_lists_items AS A
                 WHERE orders_items_lists_items_id = '.$iOILIId.' AND stock_location_id = '.$sl;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    private function getLocationArea($location) {

        $sSql = 'SELECT area
                 FROM containers 
                 WHERE id = '.$location;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

  /**
   *
   * @param type $iOrderId
   * @return type
   */
  protected function getOrderTransportSymbol($iOrderId) {

    $sSql = 'SELECT OTM.symbol
               FROM orders AS O
               JOIN orders_transport_means AS OTM
                ON O.transport_id = OTM.id
               WHERE O.id = '.$iOrderId;
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }


  /**
   * Metoda sprawdza czy użytkownik może zarządzać parametrami zbieranej listy
   *
   * @param type $aUser
   * @return boolean
   */
  protected function checkManageGetReadySend($aUser) {

    if ($aUser['type'] == '0') {
      $sSql = 'SELECT manage_get_ready_send FROM users WHERE id = '.$aUser['id'];
      return ($this->pDbMgr->GetOne('profit24', $sSql) == '1' ? true : false);
    } elseif ($aUser['type'] == '1' || $aUser['type'] == '2') {
      return true;
    }
  }// end of checkManageGetReadySend() method


  /**
   *
   * @param array $aOrdersItems
   * @return array
   */
  protected function getOrdersItemsList($aOrdersItems) {

    $aItems = array();
    foreach ($aOrdersItems as $iOrderId => $aOrderItems) {
      $this->aOrdersAccepted[$iOrderId] = '1';
      foreach ($aOrderItems as $aOrderItem) {
        $aItems[] = $aOrderItem;
      }
    }
    usort($aItems, 'sortItems');
    return $aItems;
  }


  /**
   * Metoda pobiera unikatowe powiązane zamówienia
   *
   * @deprecated since version 1.0
   * @param \Smarty $pSmarty
   * @return bool
   */
  protected function getUniqeLinkedOrders(&$pSmarty) {
    $bIsErr = false;

    $sPackageNumber = $_POST['package_number'];
    $iTransportId = $_POST['transport_id'];
    // tutaj zotanie ustawione $this->aOrdersAccepted
    $this->setCountAvailableLinkedTransport();

    if (!empty($this->aOrdersAccepted)) {
      while ($iOrderId = array_pop(array_keys($this->aOrdersAccepted))) {

        if ($iOrderId > 0) {
          $aLinkedOrders = $this->getLinkedOrders($iOrderId);
          if (!empty($aLinkedOrders)) {
            // czyślimy tablice

            unset($this->aOrdersAccepted);
            unset($this->aOrdersWaiting);
            $aPacketList = $this->getRecordsListLimited(NULL, NULL, NULL, NULL, '', true, $aLinkedOrders, true);
            $sGetListSQL = $this->sGetListSQL;
            // tablice globalne poniżej są uzupełniane
            $aList = $this->countOrders($aPacketList, 99999);
            if (!empty($aList) && !empty($sPackageNumber) && count($this->aOrdersAccepted) == count($aLinkedOrders)) {
              usort($aList, 'sortItems');
              // ok lecimy z wyświetlaniem zamówienia
              $this->pDbMgr->BeginTransaction('profit24');

              $aDebug['sql'] = $sGetListSQL;
              $aDebug['post'] = $_POST;
              $mReturn = $this->doCreateList($aList, '2', TRUE, $sPackageNumber, $iTransportId, base64_encode(serialize($aDebug)));
              if ($mReturn === FALSE || $mReturn <= 0) {
                $bIsErr = $mReturn;
              } else {
                $iILId = intval($mReturn);
              }

              if ($bIsErr !== false) {
                switch ($bIsErr) {
                  case -1:
                    $sMsg = _('Wystąpił błąd podczas blokowania kuwety !');
                  break;

                  case -2:
                    $sMsg = _('Brak takiego kontenera !');
                  break;

                  default:
                  case false:
                    $sMsg = _('Wystąpił błąd podczas pobierania listy produktów do zbierania.');
                  break;
                }
                // błąd
                $this->pDbMgr->RollbackTransaction('profit24');
                AddLog($sMsg);
                $this->sMsg = GetMessage($sMsg);
                $this->Show($pSmarty);
                return false;
              } else {
                // ok pdf i commit
                $this->pDbMgr->CommitTransaction('profit24');
                $sReturn = $this->getProductsScanner($pSmarty, $iILId, $aList, $sPackageNumber);
                $pSmarty->assign('sContent', $sReturn);
                return true;
              }
            }
          }
        }

      }
    } else {
      $sMsg = _('Brak produktów do zebrania.');
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
      return;
    }

    $sMsg = _('Wystąpił błąd podczas pobierania listy produktów do zbierania 2.');
    AddLog($sMsg);
    $this->sMsg = GetMessage($sMsg);
    $this->Show($pSmarty);
    return;
  }// end of getUniqeLinkedOrders() method


  /**
   * Metoda pobiera połączone zamówienia które mają zostać razem wysłane
   *
   * @param int $iMainOrderId
   * @return string
   */
  protected function getLinkedOrders($iMainOrderId) {

    $sSql = 'SELECT DISTINCT linked_order_id, "1" as key_
             FROM orders_linked_transport
             WHERE main_order_id = '.$iMainOrderId;
    $aOrderIds = $this->pDbMgr->GetAssoc('profit24', $sSql);
    $aOrderIds[$iMainOrderId] = '1';
    array_unique($aOrderIds);
    return $aOrderIds;
  }// end of getLinkedOrders() method


  /**
   * Metoda pobiera transport
   *
   * @return array
   */
  protected function getTransports() {

    $sSql = 'SELECT name, id 
             FROM orders_transport_means';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getTransports() method


  /**
   * Metoda zwraca podstawowe zamówienie wybierania zamówień
   *
   * @param string $sSelect
   * @return string
   */
  public function getQuery($sSelect) {

    /*
     * 1) opłacone, lub odroczony termin płatności,
     * 2) nie "nowe", "zatwierdzone", "zrealizowane", "anulowane"
     * 3) jeśli z G to status musi być "jest nie G"
     * 4) jeśli zewn. to status musi być "jest pot."
     * 5) data wysyłki jeśli została zmieniona to ma być to dziś
     * 6) nie pobrane wcześniej,
     */
    $sSql = "
SELECT ".$sSelect."
FROM orders_items AS OI
    JOIN orders AS O
    ON OI.order_id = O.id
        AND IF (OI.source = '51', (OI.status = '3' OR OI.status = '4'), OI.status = '4')
        AND OI.get_ready_list = '0'
        AND OI.deleted = '0'
        AND (OI.item_type = 'I' OR OI.item_type = 'P')
        AND OI.packet = '0'
    -- uwaga warunki muszą dotyczyć wszystkich składowych, a nie tylko jednej
    LEFT JOIN orders_items AS OI_TEST
    ON OI_TEST.order_id = O.id
      AND IF (OI_TEST.source = '51', OI_TEST.status <> '3' AND OI_TEST.status <> '4', OI_TEST.status <> '4')
      AND OI_TEST.get_ready_list = '0'
      AND OI_TEST.deleted = '0'
      AND (OI_TEST.item_type = 'I' OR OI_TEST.item_type = 'P')
      AND OI_TEST.packet = '0'
    LEFT JOIN products AS P
      ON P.id = OI.product_id
    WHERE 
      OI_TEST.id IS NULL

      AND
      (
        (
          (
            O.payment_type = 'bank_transfer' OR
            O.payment_type = 'platnoscipl' OR
            O.payment_type = 'card'
          )
          AND O.to_pay <= O.paid_amount
        )
        OR
        (
          O.payment_type = 'postal_fee' OR
          O.payment_type = 'bank_14days'
        )
        OR
        (
          O.second_payment_type = 'postal_fee' OR
          O.second_payment_type = 'bank_14days'
        )
      )


      AND 
      (
        O.order_status = '1' OR
        O.order_status = '2' 
      )

      AND 
      ( O.send_date = '0000-00-00' OR (O.send_date <> '0000-00-00' AND O.send_date < NOW()) )
      
      /*AND OI.order_id = 131872*/
    ";
    return $sSql;
  }// end of getQuery() method


  /**
   * Metoda pobiera ilość zamówień, które czekają na pobranie
   *
   * @return type
   */
  protected function getCountLists($bSingle = false) {

    $sSql = $this->getQuery(' O.transport_id, count(DISTINCT O.id) as count, (SELECT name FROM orders_transport_means WHERE id = O.transport_id) AS name ');

    if ($bSingle === TRUE) {
      $sSql .= '
       AND (
        (SELECT sum(quantity) from orders_items WHERE order_id = O.id AND deleted = "0") = 1
       ) ';
    } else {
      $sSql .= '
       AND (
        (SELECT sum(quantity) from orders_items WHERE order_id = O.id AND deleted = "0") >= 2
       ) ';
    }

    $sSql .= ' GROUP BY O.transport_id ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getCountLists() method


  /**
   * Metoda sprawdza, która metoda transportu ma najwięcej zamówień
   *
   * @param array $aQuantityLists
   * @return
   */
  protected function getMaxQuantityTransportId($aQuantityLists) {

    $iMaxCount = NULL;
    $iMaximumTransportId = NULL;
    if (!empty($aQuantityLists)) {
      foreach ($aQuantityLists as $aItemTransport) {
        if ($iMaxCount === NULL) {
          $iMaxCount = $aItemTransport['count'];
          $iMaximumTransportId = $aItemTransport['transport_id'];
        } elseif ($iMaxCount < $aItemTransport['count']) {
          $iMaxCount = $aItemTransport['count'];
          $iMaximumTransportId = $aItemTransport['transport_id'];
        }
      }
      return $iMaximumTransportId;
    }
  }// end of getMaxQuantityTransportId() method


  /**
   * Metoda sprawdza, czy istnieje maksymalna wartość pomiędzy metodami transprotu,
   *  jesli istnieje, zaznacza opcję domyślnie
   *
   * @param array $aQuantityLists
   * @param string $aTransportRadio
   * @return string
   */
  protected function checkMaximumValueTransport($aQuantityLists, $aTransportRadio) {

    $iMaxTransportId = $this->getMaxQuantityTransportId($aQuantityLists);
    if ($iMaxTransportId > 0) {
      foreach ($aTransportRadio as $iKey => $aTransport) {
//        if ($aTransport['value'] == $iMaxTransportId) {
        if (empty($_POST)) {
          $aTransportRadio[$iKey]['checked'] = 'checked';
        }
//        }
      }
    }
    return $aTransportRadio;
  }// end of checkMaximumValueTransport() method


  /**
   * Metoda pobiera tablicę do radio
   *
   * @param array $aArray
   * @return array
   */
  protected function getRadioByArr($aArray) {

    $aRadio = array();
    foreach($aArray as $iKey => $aItem) {
      $aRadio[$iKey]['label'] = $aItem['name'];
      $aRadio[$iKey]['value'] = $aItem['id'];
    }
    return $aRadio;
  }// end of getRadioByArr() method


  /**
   * Metoda sprawdza, czy dane zamówienie oczekuje do wysłania na jakieś inne
   * i czy w związku z tym mogą zostać wysłane razem
   *
   * @param integer $iOrderId
   * @return char
   */
  protected function checkOrdersAccepted($iOrderId) {

    /*
CALL check_linked_orders(, @order_accepted);
SELECT @order_accepted;
     */
    $sSql = 'CALL check_linked_orders('.$iOrderId.', @order_accepted);';
    $this->pDbMgr->Query('profit24', $sSql);
    $cIsAccepted = $this->pDbMgr->GetOne('profit24', 'SELECT @order_accepted');
    return ($cIsAccepted == '1' ? true : false);
  }// end of checkOrdersAccepted() method


  /**
   * Metoda przelicza ilosć zamówień
   *
   * @param array $aList
   */
  protected function countOrders($aList, $iOrdersLimit) {

    $aNewTMPLIST = array();
    foreach ($aList as $aItem) {
      if (!isset($this->aOrdersAccepted[$aItem['order_id']]) && !isset($this->aOrdersWaiting[$aItem['order_id']])) {

        $bErrDeliveryAddress = $this->validateTransportsMethods($aItem['order_id']);

        if ($bErrDeliveryAddress !== true) {
          $this->addErrorOrderToContainer($aItem['order_id']);
        }

        $mIsOrderLocked = $this->oParent->oOrdersSemaphore->isLocked($aItem['order_id'], $this->iUId);
        if ($this->checkOrdersAccepted($aItem['order_id']) == true &&
            $bErrDeliveryAddress === true &&
            $mIsOrderLocked === false) {
          // sprawdźmy czy możemy uwzględnić
          $this->aOrdersAccepted[$aItem['order_id']] = 1;
          $aNewTMPLIST[] = $aItem;
        } else {
          $this->aOrdersWaiting[$aItem['order_id']] = 0;
        }
        if (count($this->aOrdersAccepted) >= $iOrdersLimit) {
          return $aNewTMPLIST;
        }
      } else {
        $aNewTMPLIST[] = $aItem;
      }
    }
    return $aNewTMPLIST;
  }// end of countOrders() method

  /**
   *
   * @param int $iOrderId
   * @return void
   */
  public function addErrorOrderToContainer($iOrderId) {

    $oContainers = new OrdersContainers();
    $oContainers->addOrderToContainers($iOrderId, 'T', true);
  }

  /**
   * @param int $iOrderId
   * @return boolean
   */
  public function validateTransportsMethods($iOrderId) {
    global $pDbMgr;

    $sSymbol = $this->getOrderTransportSymbol($iOrderId);

    switch ($sSymbol) {
      case 'odbior-osobisty':
        return true;
      break;

      case 'poczta_polska':
        if ($this->checkOrderTransportAddress($iOrderId) === true) {
          $sPointOfReceipt = $this->getPointOfReceipt($iOrderId);
        } else {
          return false;
        }
      break;

      case 'poczta-polska-doreczenie-pod-adres':
        return $this->checkOrderTransportAddress($iOrderId);
      break;

      case 'opek-przesylka-kurierska':
        return $this->checkOrderTransportAddress($iOrderId);
      break;

      case 'tba':
        $sPostal = $this->getOrderTransportPostal($iOrderId);
        $sPointOfReceipt = $sPostal;
      break;

      case 'paczkomaty_24_7':
        $sPointOfReceipt = $this->getPointOfReceipt($iOrderId);
        $oShipment = new Shipment($sSymbol, $pDbMgr);
        return $oShipment->checkDestinationPointAvailableDB('PointsOfReceipt', $sPointOfReceipt);
      break;

      default :
        $sPointOfReceipt = $this->getPointOfReceipt($iOrderId);
      break;
    }
    if ($sPointOfReceipt != '') {
      $oShipment = new Shipment($sSymbol, $pDbMgr);
      $result = $oShipment->checkDestinationPointAvailable($sPointOfReceipt);
      return $result;
//      if ($_SESSION['user']['type'] == '1') {
//        $oShipment = new Shipment($sSymbol, $pDbMgr);
//        $result = $oShipment->checkDestinationPointAvailable($sPointOfReceipt);
//        if ($result === false) {
//          dump(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
//          var_dump($result);
//          var_dump($sPointOfReceipt);
//          die;
//        }
//      }
    } else {
      return false;
    }
  }

  /**
   * @param int $iOrderId
   * @return bool
   */
  protected function getOrderTransportPostal($iOrderId) {

    $sSql = 'SELECT postal 
             FROM orders_users_addresses
             WHERE order_id = ' . $iOrderId. ' 
               AND address_type = "1" ';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of checkOrderTransportAddress() method


  /**
   * Metoda sprawedza, czy adres do transportu,
   *  jest zgodny z kodami podanymi w tabeli
   *
   * @param int $iOrderId
   * @return bool
   */
  protected function checkOrderTransportAddress($iOrderId) {

    $sSql = 'SELECT city, postal 
             FROM orders_users_addresses
             WHERE order_id = ' . $iOrderId. ' 
               AND address_type = "1" ';
    $aOrdersAddress = $this->pDbMgr->GetRow('profit24', $sSql);
    if (!empty($aOrdersAddress)) {
      return $this->checkCityPostal($aOrdersAddress['city'], $aOrdersAddress['postal']);
    } else {
      // domyślnie
      return false;
    }
  }// end of checkOrderTransportAddress() method


	/**
	 * Metoda sprawdza czy kod pocztowy pasuje do miasta
	 *
	 * @global array $aConfig
	 * @param string $sCity
	 * @param string $sPostal
	 * @return bool
	 */
	protected function checkCityPostal($sCity, $sPostal) {
		global $aConfig;

		$sSql = "SELECT count(1)
						FROM ".$aConfig['tabls']['prefix']."users_accounts_postals
						WHERE city = ".Common::Quote(stripslashes($sCity)).
						 " AND postal = ".Common::Quote(stripslashes($sPostal));
		return (intval(Common::GetOne($sSql)) > 0);
	}	// end of checkCityPostal() method


  /**
   * Metoda pobiera rekordy na listę ograniczone co do ilości
   *  Uwaga modyfikacja, wyświetlamy aktualną półkę:
   *  Jeśli jest to źródło "jest nie" na G to pobieramy wyłącznie półkę dla wysłanych zamówień do "tramwaj" i "tramwaj migracja", ponieważ tylko cofnięty produkt może być zebrany z Tramwaju.
   *  Jeśli jest to inne źróło to standardowo pobieramy
   *
   * @param int $iLimitRecords
   * @param array $aTransportId
   * @param string $sMaxOrderQuantity
   * @param string $sMinOrderQuantity - minimalna ilość egz. w zamówieniu
   * @param date $dExpectedShipmentDate
   * @param bool $bForceOrdersItems
   * @param array $aOrderItems
   * @param bool $bGetPackets
   * @return array
   */
  public function getRecordsListLimited($iLimitRecords = 0, $aTransportId = NULL, $sMaxOrderQuantity = 0, $sMinOrderQuantity = 0, $dExpectedShipmentDate = '', $bForceOrdersItems = false, $aOrderItems = array(), $bGetPackets = false) {

    $sSql = $this->getQuery(' CONCAT(OI.order_id, ",", OI.id) AS ident, O.order_number, OI.id, OI.order_id, OI.quantity,
                              OI.name, OI.publisher, OI.authors, OI.publication_year, OI.edition, OI.product_id, 
                              OI.item_type,
                              P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13, IF(OI.source = 51, "1", "0") AS is_g,
                               
                              
(
IF (
  OI.source = 51 AND OI.status = "3", 
    (
      SELECT OSH.pack_number
      FROM orders_send_history AS OSH
      JOIN orders_send_history_items AS OSHI
        ON OSH.id = OSHI.send_history_id
      WHERE OSHI.item_id = OI.id AND
            OSH.source IN ("33", "34")
      ORDER BY OSH.id DESC
      LIMIT 1
     )
  ,
    (
      SELECT OSH.pack_number
      FROM orders_send_history AS OSH
      JOIN orders_send_history_items AS OSHI
        ON OSH.id = OSHI.send_history_id
      WHERE OSHI.item_id = OI.id 
      ORDER BY OSH.id DESC
      LIMIT 1
    )
  )
) AS pack_number,


                              (SELECT id FROM orders_items WHERE parent_id = OI.id AND item_type = "A") AS is_cd,
                              (SELECT profit_g_act_stock FROM products_stock AS PS WHERE PS.id = OI.product_id) as profit_g_act_stock,
                              (SELECT CONCAT(directory, "|", photo) FROM products_images AS PI WHERE PI.product_id = OI.product_id LIMIT 1) AS photo,

                              OI.weight
                              ');

    if ($bForceOrdersItems === true) {
      if (!empty($aOrderItems)) {
        $sSql .= " AND O.id IN (".implode(', ', array_keys($aOrderItems)).") ";
      } else {
        return false;
      }
    }
    $sSql .= ' 
      AND 
      (
        O.remarks = ""
        OR O.remarks IS NULL 
        OR O.remarks_read_1 IS NOT NULL
      )
    ';

    $sMaxOrderQuantity = intval($sMaxOrderQuantity);
    if ($sMaxOrderQuantity > 0) {
      $sSql .= '
       AND (
        (SELECT sum(quantity) from orders_items WHERE order_id = O.id AND deleted = "0") <= '.$sMaxOrderQuantity.'
       ) ';
    }

    $sMinOrderQuantity = intval($sMinOrderQuantity);
    if ($sMinOrderQuantity > 0) {
      $sSql .= '
       AND (
        (SELECT sum(quantity) from orders_items WHERE order_id = O.id AND deleted = "0") >= '.$sMinOrderQuantity.'
       ) ';
    }

    if (is_numeric($aTransportId) && !is_array($aTransportId)) {
      $iTransportId = intval($aTransportId);
      $aTransportId[$iTransportId] = $iTransportId;
    }

    if (!empty($aTransportId) && is_array($aTransportId)) {
      $sSql .= '
       AND (
          O.transport_id IN ('.implode(', ', $aTransportId).')
        ) ';
    }

    if ($bGetPackets == FALSE) {
      $sSql .= ' AND O.id NOT IN (SELECT main_order_id FROM orders_linked_transport) ';
    }

    if ($dExpectedShipmentDate != '' && $dExpectedShipmentDate != '00-00-0000') {
      $dFormedExpectedDate = preg_replace('/(\d{2})-(\d{2})-(\d{4})/', '$3-$2-$1', $dExpectedShipmentDate);
      $sSql .= ' AND O.shipment_date_expected <= "'.$dFormedExpectedDate.'" ';
    }

    $sSql .= " 
      GROUP BY OI.id     
      ORDER BY O.magazine_get_ready_list_VIP DESC, O.shipment_date_expected ASC, IF(O.payment_type = 'postal_fee', '1', '0') DESC, O.id ASC
      ". ($iLimitRecords > 0 ? " LIMIT ".$iLimitRecords : '');
    $this->sGetListSQL = $sSql;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getRecordsListLimited() method


  /**
   * Metoda pobiera liczbę list, które są aktualnie zbierane
   *
   * @return int
   */
  protected function getCountInCompletation($aType) {

    $sSql = 'SELECT count(id)
             FROM orders_items_lists
             WHERE 
              closed = "0" AND
              type IN ('.implode(',', $aType).') ';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of getCountInCompletation() method


  /**
   * Metoda pobiera liczbę list, które są aktualnie kompletowane
   *
   * @return int
   */
  protected function getCountInSort($aType) {

    $sSql = 'SELECT count(id)
             FROM orders_items_lists
             WHERE 
                closed = "2" AND
                type IN ('.implode(',', $aType).') ';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of getCountInSort() method


  /**
   * Metoda stara się pobrać zamówienia do określonej liczby
   *
   * @param int $iOrdersLimit
   * @param array $aTransportIds
   * @param string $sMaxOrderQuantity
   * @param string $sMinOrderQuantity
   * @param date $dExpectedShipmentDate
   */
  protected function getLimitedOrders($iOrdersLimit, $aTransportIds = NULL, $sMaxOrderQuantity = 0, $sMinOrderQuantity = 0, $dExpectedShipmentDate = '') {
    $iLimitIterations = 2;// maks 6 pobrania listy
    $iCountItems = 40; // ilość elementów do pobrania na starcie
    $i = 0;
    unset($this->aOrdersAccepted);
    unset($this->aOrdersWaiting);

    while($i <= $iLimitIterations) {
      $aFirstList = $this->getRecordsListLimited($iCountItems, $aTransportIds, $sMaxOrderQuantity, $sMinOrderQuantity, $dExpectedShipmentDate);
      $aList = $this->countOrders($aFirstList, $iOrdersLimit);
      if (count($this->aOrdersAccepted) < $iOrdersLimit) {
        $iCountItems += 40;
      } else {
        return $aList;
      }
      $i++;
    }
    return $aList;
  }// end of getLimitedOrders() method


  /**
   * Metoda oblicza ilość zamówień które można pobrać
   *
   * @param object $pSmarty
   */
  protected function getCountOrders(&$pSmarty) {
    unset($this->aOrdersAccepted);
    unset($this->aOrdersWaiting);

    $aFirstList = $this->getRecordsListLimited(0, $_POST['transport_id'], $_POST['max_order_item'], $_POST['min_order_item'], $_POST['shipment_date_expected']);
    $this->countOrders($aFirstList, 99999);
    $iCount = count($this->aOrdersAccepted);

    $this->sMsg .= GetMessage(sprintf(_('Ilosć zamówień gotowych do pobrania: %s'), $iCount), false);
    $this->Show($pSmarty);
  }// end of getCountOrders() method


  /**
   * Metoda pobiera listę produktów do pobrania
   *
   * @deprecated since version 1.0
   * @param object $pSmarty
   * @return void
   */
  protected function getList(&$pSmarty) {

    $bIsErr = false;

    $iOrdersLimit = 15;
    $aList = $this->getLimitedOrders($iOrdersLimit, $_POST['transport_id'], $_POST['max_order_item'], $_POST['min_order_item'], $_POST['shipment_date_expected']);
    $sGetListSQL = $this->sGetListSQL;
    $aList = $this->getRecordsListLimited(0, $_POST['transport_id'], $_POST['max_order_item'], $_POST['min_order_item'], $_POST['shipment_date_expected'], true, $this->aOrdersAccepted, false);
    $cType = $_POST['type'];
    $sPackageNumber = $_POST['package_number'];
    $mTransportId = $_POST['transport_id'];

    if (!empty($aList) && !empty($sPackageNumber)) {
      usort($aList, 'sortItems');

      // posortować na liście od magazynu do tramwaju w kolejności narastającej
      $this->pDbMgr->BeginTransaction('profit24');
      // zmieniamy w zam: print_ready_list = '1'
      // , w skł. zam: get_ready_list = '1'
      $aDebug['sql'] = $sGetListSQL;
      $aDebug['post'] = $_POST;
      $mReturn = $this->doCreateList($aList, $cType, TRUE, $sPackageNumber, $mTransportId, base64_encode(serialize($aDebug)));
      if ($mReturn === FALSE || $mReturn <= 0) {
        $bIsErr = -1;
      } else {
        $iILId = intval($mReturn);
      }

    } else {
      $bIsErr = true;
      // błąd
      $this->pDbMgr->RollbackTransaction('profit24');
      $sMsg = _('Brak produktów do zbierania.');
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
      return;
    }

    if ($bIsErr !== false) {
      switch ($bIsErr) {
        case -1:
          $sMsg = _('Wystąpił błąd podczas blokowania kuwety !');
        break;
        default:
        case false:
          $sMsg = _('Wystąpił błąd podczas pobierania listy produktów do zbierania.');
        break;
      }
      // błąd
      $this->pDbMgr->RollbackTransaction('profit24');

      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
      return;
    } else {
      $this->pDbMgr->CommitTransaction('profit24');
      $sReturn = $this->getProductsScanner($pSmarty, $iILId, $aList, $sPackageNumber);
      $pSmarty->assign('sContent', $sReturn);
      return true;
    }
  }// end of getList() method


    /**
     * @param $cListType
     * @return bool
     */
    protected function checkIsContainer($cListType) {

        if ($cListType !== '5' && $cListType !== '8') {
            return true;
        }
        return false;
    }


  /**
   * Metoda tworzy listę do zebrania na podstawie przekazanej tablicy danych
   *
   * @param array $aList
   * @param char $cListType - przyjmuje typy:
   *      '0' - zwykłe
   *      '1' - single
   *      '2' - wysyłka razem
   *      '4' - anulowane
   *      '5' - wysokie składowanie
   * @param bool $bGetFromTrain
   * @param string(uint) $sPackageNumber - numer kuwety
   * @param mixed $mTransportId - id metody transportu
   * @param string $sDebug
   * @return bool
   */
    public function doCreateList(&$aList, $cListType, $bGetFromTrain, $sPackageNumber, $mTransportId, $sDebug = '') {
    global $aConfig;
    $bIsErr = false;

      if (!isset($this->destinationMagazineDeficiencies)) {
        $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_HIGH_STOCK_SUPPLIES;
        if (isset($_GET['magazine']) && $_GET['magazine'] == \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES) {
          $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES;
        }
        if (isset($_POST['magazine']) && $_POST['magazine'] == \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES) {
          $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES;
        }
      }

    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
    if ($this->checkIsContainer($cListType)) {
        $oContainers = new Containers($this->pDbMgr);
        $mReturn = $oContainers->lockContainer($sPackageNumber);
        if ($mReturn !== true) {
            // coś poszło nie tak
            return $mReturn;
        }
    }

    foreach ($this->aOrdersAccepted as $iOrderId => $sVal) {
      // zmieniamy składową w zamówieniu
      $aOrderValues = array(
          'print_ready_list' => '1',
          'order_on_list_locked' => '1'
      );
      if ($this->pDbMgr->Update('profit24', 'orders', $aOrderValues, 'id = '. $iOrderId) === FALSE) {
        $bIsErr = true;
      }
    }

    $iILId = $this->doCreateListWithItems($aList, $sPackageNumber, $cListType, $bGetFromTrain, $sDebug, $mTransportId);

    if ($bIsErr === TRUE) {
      return false;
    } else {
      return $iILId;
    }
  }// end of doCreateList() method


    /**
     * @param $aList
     * @param $sPackageNumber
     * @param $cListType
     * @param $bGetFromTrain
     * @param $sDebug
     * @param $mTransportId
     * @return mixed
     */
    public function doCreateListWithItems(&$aList, $sPackageNumber, $cListType, $bGetFromTrain, $sDebug, $mTransportId) {
        $bIsErr = false;

        // dodajemy teraz dane listy
        $aValuesOIL = array(
            'user_id' => $_SESSION['user']['id'],
            'document_number' => ($this->pDbMgr->GetOne('profit24', 'SELECT MAX( id ) FROM orders_items_lists') + 1),
            'package_number' => ($sPackageNumber == '' ? 'TMP' : $sPackageNumber),
            'closed' => '0',
            'type' => $cListType,
            'destination_magazine' => $this->destinationMagazineDeficiencies != ''? $this->destinationMagazineDeficiencies : 'SH',
            'get_from_train' => $bGetFromTrain,
            'created' => 'NOW()',
            'debug' => $sDebug,
            'big' => (stristr($_GET['action'], '_big') ? '1' : '0')
        );
        if (is_array($mTransportId)) {
            $aValuesOIL['transport_id'] = 'NULL';
        } elseif ($mTransportId <= 0) {
            $aValuesOIL['transport_id'] = 'NULL';
        } else {
            $aValuesOIL['transport_id'] = $mTransportId;
        }
        $iILId = $this->pDbMgr->Insert('profit24', 'orders_items_lists', $aValuesOIL);

        if ($iILId > 0) {

            // dodajemy transport
            if ($this->addOILTransport($iILId, $mTransportId) === false) {
                $bIsErr = true;
            }

            // dodajemy składowe
            foreach ($aList as $iKey => $aItem) {
                if ($aItem['item_type'] == 'I' || $aItem['item_type'] == 'P') {
                    // uwaga to jest nadpisane..
                    $iOILIId = $this->doInsertOrderListItems($iILId, $aItem);
                    if ($iOILIId === false) {
                        $bIsErr = true;
                        break;
                    }
                    $aList[$iKey]['OILI_ID'] = $iOILIId;

                    if ($aItem['source'] == '51') {
                        // rezerwujemy bo mamy tutaj składowe
                        if (false === $this->reserveOnMagazine($iOILIId, $aItem)) {
                            $bIsErr = true;
                            break;
                        }
                    }

                    $aOrderItemValues = array('get_ready_list' => '1');
                    if ($this->pDbMgr->Update('profit24', 'orders_items', $aOrderItemValues, 'id = '. $aItem['id']) === FALSE) {
                        $bIsErr = true;
                        break;
                    }
                }
            }
        }

        if ($bIsErr === TRUE) {
            return false;
        } else {
            return $iILId;
        }
    }

  /**
   * @param $aItem
   * @param $iOIlId
   * @return bool
   */
  public function reserveOnMagazine($iOILIId, $aItem) {

    $stockLocation = new StockLocation($this->pDbMgr);
    // tutaj sobie rezerwujemy na lokalizacji wysokiego/niskiego
    if (!empty($aItem['magazine_stock_localizations_reserved'])) {
      foreach ($aItem['magazine_stock_localizations_reserved'] as $key => $row) {
        $values = [
            'stock_location_id' => $row['id'],
            'stock_location_magazine_type' => $row['magazine_type'],
            'orders_items_lists_items_id' => $iOILIId,
            'reserved_quantity' =>  $row['own_reserved'],
            'type' => StockLocationOrdersItemsListsItems::TYPE_MM,
            'created' => 'NOW()',
            'created_by' => $_SESSION['user']['name']
        ];
        if (false === $this->pDbMgr->Insert('profit24', 'stock_location_orders_items_lists_items', $values)) {
          return false;
        }
        // rezerwujemy na magazynie
        if (false === $stockLocation->reserve($row['id'], $row['own_reserved'])) {
          return false;
        }
      }

      return true;
    } else {
      return false;
    }
  }

    /**
     * @param $iILId
     * @param $aItem
     * @return bool
     */
    public function doInsertOrderListItems($iILId, $aItem) {

        $a = 1;
        $aValuesOILI = array(
            'orders_items_lists_id' => $iILId,
            'orders_items_id' => $aItem['id'],
            'orders_id' => $aItem['order_id'],
            'products_id' => $aItem['product_id'],
            'quantity' => $aItem['quantity'],
            'is_g' => $aItem['is_g'],
            'isbn_plain' => $aItem['isbn_plain'],
            'isbn_10' => $aItem['isbn_10'],
            'isbn_13' => $aItem['isbn_13'],
            'ean_13' => $aItem['ean_13'],
        );
        $resultId = $this->pDbMgr->Insert('profit24', 'orders_items_lists_items', $aValuesOILI);
        if ($resultId === FALSE) {
            return false;
            dump($aValuesOILI);
            var_dump($mReturn);
        }

        return $resultId;
    }

  /**
   *
   * @param int $iILId
   * @param mixed $mTransportId
   * @return bool
   */
  protected function addOILTransport($iILId, $mTransportId) {

    if (!is_array($mTransportId)) {
      if ($mTransportId > 0) {
        $iTransportId = intval($mTransportId);
        $mTransportId[$iTransportId] = $iTransportId;
      } else {
        return true;// nic tu nie dodajemy
      }
    }

    foreach ($mTransportId as $iTransportItemId) {
      $aValues = array(
        'orders_items_lists_id' => $iILId,
        'orders_transport_means_id' => $iTransportItemId
      );
      if ($this->pDbMgr->Insert('profit24', 'orders_items_lists_tranport', $aValues) === FALSE) {
        return false;
      }
    }
    return true;
  }


  /**
   * Metoda sumuje wskazaną kolumnę tablicy na podstawie kolumny
   *
   * @param array $aList
   * @param string $sSumKey
   * @param string $sSumByKey
   * @return string
   */
  public function sumByArray($aList, $sSumKey, $sSumByKey) {

    foreach ($aList as $iKey => $aItem) {
      //if ($aItem[$sSumByKey] != '') {// nie pusty
      if ($aItem['product_id'] != '') {
        $iCurrNextRow = 1;
          if (isset($aList[$iKey]['ident'])) {
              $aList[$iKey]['ident_summary'] = $aList[$iKey]['ident'].','.$aList[$iKey]['quantity'];
              $aList[$iKey]['sub_products'][] = $this->getFilterSubProducts($aItem);
          }
        while(isset($aList[$iKey+$iCurrNextRow]) && $aList[$iKey+$iCurrNextRow][$sSumByKey] === $aItem[$sSumByKey] && $aList[$iKey+$iCurrNextRow]['product_id'] == $aItem['product_id']) {// dopuki następny klucz nie pusty i wartość kolumny się zgadza
          $aList[$iKey]['sub_products'][] = $this->getFilterSubProducts($aList[$iKey+$iCurrNextRow]);
          $aList[$iKey][$sSumKey] += intval($aList[$iKey+$iCurrNextRow][$sSumKey]);
            if (!empty($aList[$iKey+$iCurrNextRow]['magazine_stock_localizations_reserved'])) {
                if (empty($aList[$iKey]['magazine_stock_localizations_reserved'])) {
                    $aList[$iKey]['magazine_stock_localizations_reserved']=$aList[$iKey+$iCurrNextRow]['magazine_stock_localizations_reserved'];
                } else {
                    $aList[$iKey]['magazine_stock_localizations_reserved'] = array_merge($aList[$iKey]['magazine_stock_localizations_reserved'], $aList[$iKey+$iCurrNextRow]['magazine_stock_localizations_reserved']);
                }
            }
            if (isset($aList[$iKey+$iCurrNextRow]['ident'])) {
                $aList[$iKey]['ident_summary'] .= '|'.$aList[$iKey+$iCurrNextRow]['ident'].','.$aList[$iKey+$iCurrNextRow]['quantity'];
            }
          unset($aList[$iKey+$iCurrNextRow]);
          $iCurrNextRow++;
        }
      }
      //}
    }
    $aList = array_merge($aList);
    return $aList;
  }// end of sumByArray() method

  /**
   * @param $aItem
   * @return array
   */
  private function getFilterSubProducts($aItem) {
    $aAvailableKeys = [
        'order_id',
        'id',
        'product_id',
        'quantity',
        'status',
        'source'
    ];

    $result = [];
    foreach ($aItem as $key => $value) {
      if (in_array($key, $aAvailableKeys)) {
        $result[$key] = $value;
      }
    }
    return $result;
  }


  /**
   * Metoda tworzy i zwraca PDF'a
   *
   * @param \Smarty $pSmarty
   * @param int $iILId
   * @param array $aList
   */
  protected function getProductsScanner(&$pSmarty, $iILId, $aList, $sPackageNumber) {
//    include_once 'barcode/barcode.php';

    //$sCode = str_pad($iILId, 13, '0', STR_PAD_LEFT);

    if (!isset($this->destinationMagazineDeficiencies)) {
      $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_HIGH_STOCK_SUPPLIES;
      if (isset($_GET['magazine']) && $_GET['magazine'] == \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES) {
        $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES;
      }
      if (isset($_POST['magazine']) && $_POST['magazine'] == \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES) {
        $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES;
      }
    }

    $iRefCountMagazine = 0;
    $iRefCountTramwaj = 0;
    $this->countQuantityItems($aList, $iRefCountMagazine, $iRefCountTramwaj);
    $aViewList = $this->sumByArray($aList, 'quantity', 'pack_number');

    $aBooks = array();
    $aBooks['number'] = $sCode;
    $aBooks['items'] = $this->parseBooksToList($aViewList);
//    $aBooks['author'] = $_SESSION['user']['author'];
//    $aBooks['count_magazine'] = $iRefCountMagazine;
//    $aBooks['count_tramwaj'] = $iRefCountTramwaj;


    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('ordered_itm', _('Zbieranie towaru. Listy o id: '.$iILId), array('action'=>phpSelf(array('id'=>$iILId))), array('col_width'=>155), false);
    $pForm->AddHidden('do', 'save_list_data');
    $pForm->AddHidden('magazine', $this->destinationMagazineDeficiencies);
    $pForm->AddHidden('orders_items_lists_id', $iILId);
    $pForm->AddMergedRow(
            '<div style="float:left">'.
              $pForm->GetLabelHTML('search_isbn', _('ISBN '), false).
              $pForm->GetTextHTML('search_isbn', _('ISBN'), '', array('style' => 'font-size: 40px; margin-right: 50px;'), '', 'uint', true).
//        $pForm->GetTextHTML('package_number', _('Numer kuwety'), '', array('style' => 'font-size: 40px; width: 260px;'), '', 'uint', true).
            '</div>'.
            'Czas: <span id="timer"></span> &nbsp; &nbsp; '.
         $pForm->GetLabelHTML('package_number', _('Numer kuwety: '), false).' '.$sPackageNumber
    );

    $sHTML = $this->getViewProductsScanner($pSmarty, $aBooks);
    $pForm->AddMergedRow($sHTML);
    $pForm->AddMergedRow(
            '<div style="text-align: center;">'
              .$pForm->GetInputButtonHTML('save', _('Zakończ / ЗАПИСАТЬ '), array('style'=>'background-color: #51a351; font-size: 40px; text-align: center; color: #000;margin-right:20px;'))
              .'<button type="button" name="transfer" style="background-color: #51a351; font-size: 40px; text-align: center; color: #000;">Przekaż</button>'
            .'</div>'
    );
    $pForm->AddMergedRow(
//          $pForm->GetInputButtonHTML('show_not_scanned', _('Pokaż braki'), array('style'=>'font-size: 40px; float:left ;','onclick'=>'veryfiList();'), 'button').' &nbsp; '.
//          $pForm->GetInputButtonHTML('cancel', _('Anuluj'), array('style'=>'font-size: 40px; float:left;','onclick'=>'window.close();'), 'button').
          $pForm->GetInputButtonHTML('go_back', _('Pomiń / ОТЛОЖИТЬ'), array('style'=>'font-size: 40px; float:right;'), 'button')
    );


    $sInput = '
    <script type="text/javascript" src="js/CoolEctor.js?date=20181109"></script>';
    $sInput .= $this->getOverrideJS();

     /*
    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" style="font-size: 40px;" /></div>


    <script type="text/javascript">
      $(function() {
        $("#pack_number").focus();
      });
    </script>';
    */
    return (!empty($this->sMsg) ? $this->sMsg : '').$sInput.ShowTable($pForm->ShowForm());
    /*
    $pSmarty->assign_by_ref('aBooks', $aBooks);
    $sHtml = $pSmarty->fetch('PDF/pdf_ready_list.tpl');
    $pSmarty->clear_assign('aBooks');



    $im     = imagecreatetruecolor(300, 100);
    $black  = ImageColorAllocate($im,0x00,0x00,0x00);
    $white  = ImageColorAllocate($im,0xff,0xff,0xff);
    imagefilledrectangle($im, 0, 0, 300, 100, $white);
    $data = Barcode::gd($im, $black, 150, 50, 0, "code128", $sCode, 2, 50);

    // kod kreskowy
    ob_start();
    imagepng($im);
    imagedestroy( $im );
    $imagedata = ob_get_clean();


    //$this->getPDF($sHtml, $imagedata, $aBooks);
     *
     */
  }// end of getProductsScanner() method

    protected function getOverrideJS()
    {
        return '<script type="text/javascript">
                    userId = "'.$_SESSION['user']['id'].'";
                    isHighLevelStock = false;
                    extraVerification = true;
                </script>';
    }

    /**
     * @param array $aBooksScanned
     * @param $iOLIId
     */
    protected function markASDeficiencies(array $aBooksScanned, $iOLIId)
    {
        $productsStock = new \LIB\EntityManager\Entites\ProductsStock($this->pDbMgr);
        foreach ($aBooksScanned as $book) {
            $book = $this->setConfirmedOnSubProducts($book);
            $diffQuantity = intval($book->quantity) - intval($book->currquantity);
            if ($diffQuantity > 0) {
                // mamy brak
                if ($book->product_id > 0 && $book->sub_products[0]->source == '51') {
                    $productsStock->clearStockLocationByType($book->product_id, $this->destinationMagazineDeficiencies);
                    if (true === $this->checkProductStock($book->product_id)) {
                        // leży na wysokim składowaniu
                        $quantityToOrder = $this->highLevelStock->getProductQuantityToLowStock($book->product_id, $diffQuantity);
                        $this->highLevelStock->addHighLevelDeficiency($book->product_id, $iOLIId, $quantityToOrder, $book->ident);
                    }
                }
            }
        }
    }

  /**
   * @param $book
   * @return mixed
   */
    public function setConfirmedOnSubProducts($book) {
      $subProducts = $book->sub_products;
      $confirmedQuantity = $book->currquantity;
      if (!empty($subProducts)) {
        foreach ($subProducts AS &$product) {
          if ($confirmedQuantity >= $product->quantity) {
            $product->confirmedQuantity = $product->quantity;
            $confirmedQuantity = intval($confirmedQuantity - $product->quantity);
          } else {
            $product->confirmedQuantity = intval($confirmedQuantity);
            break;
          }
        }
      }
      return $book;
    }

    /**
     * @param $productId
     * @return bool
     */
    protected function checkProductStock($productId) {

      $sSql = 'SELECT profit_g_act_stock
               FROM products_stock 
               WHERE id = '.$productId;
      $quantity = $this->pDbMgr->GetOne('profit24', $sSql);
      if ($quantity > 0) {
          return true;
      }
      return false;
    }

    /**
     * Metoda zapisuje dane z zebranej listy
     *
     * @param type $pSmarty
     * @return \omniaCMS\modules\m_zamowienia_magazyn_przyjecia\HTML
     */
  protected function  saveListData($pSmarty) {
    $bIsErr = false;


    include_once('Form/Validator.class.php');
    $_POST['search_isbn'] = '9788375069693';//fix
    $oValidator = new Validator();
    if (!$oValidator->Validate($_POST)) {
      $sMsg = $oValidator->GetErrorString();
      $this->sMsg = GetMessage($sMsg, true);
      // wystapil blad wypelnienia formularza
      // wyswietlenie formularza wraz z komunikatem bledu
      AddLog($sMsg);
      echo '<script type="text/javascript">alert("' . $sMsg . '");</script>';
      return false;
    }

    if (false === $bIsErr) {
      $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_HIGH_STOCK_SUPPLIES;
      if (isset($_GET['magazine']) && $_GET['magazine'] == \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES) {
        $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES;
      }
      if (isset($_POST['magazine']) && $_POST['magazine'] == \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES) {
        $this->destinationMagazineDeficiencies = \LIB\EntityManager\Entites\Magazine::TYPE_LOW_STOCK_SUPPLIES;
      }

      include_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/magazine/HighLevelStock.class.php');
      if (!$this->highLevelStock instanceof HighLevelStock) {
        $this->highLevelStock = new HighLevelStock($this->pDbMgr, $this->destinationMagazineDeficiencies);
      }


      // ok mamy sobie dwie tablice
      $aBooksScanned = json_decode($_POST['books']);
      $aDeficienciesBooks = json_decode($_POST['not_scanned_books']);
      $iOLIId = json_decode($_POST['orders_items_lists_id']);

      $isCancelledList = $this->checkIsCanceledList($iOLIId);

      if (empty($aBooksScanned)) {
        $_POST['books'] = preg_replace("/([{,])([a-zA-Z][^: ]+):/", "$1\"$2\":", $_POST['books']);
        $aBooksScanned = json_decode($_POST['books']);
      }

      if (empty($aDeficienciesBooks)) {
        $_POST['not_scanned_books'] = preg_replace("/([{,])([a-zA-Z][^: ]+):/", "$1\"$2\":", $_POST['not_scanned_books']);
        $aDeficienciesBooks = json_decode($_POST['not_scanned_books']);
      }

      if ($isCancelledList === false) {
        try {
          // czy ciągniemy z A4 czy z B6...
          $this->markASDeficiencies($aDeficienciesBooks, $iOLIId);
          $this->markASDeficiencies($aBooksScanned, $iOLIId);
        } catch (\Exception $ex) {
          AddLog($ex->getMessage());
          echo '<script type="text/javascript">alert("' . $ex->getMessage() . '");</script>';
          return false;
        }
      }

      if (!empty($aBooksScanned)) {
        $aBooksScanned = $this->sumEqualListItems($aBooksScanned);
        foreach ($aBooksScanned as $aBook) {
            // TODO poprawić ilości - żeby zliczaly podprodukty
          if ($this->AddOILITimes($iOLIId, $aBook->ident, $aBook->time, $aBook->currquantity, '1') === false) {
            $bIsErr = true;
          }
        }
      }

      if (!empty($aDeficienciesBooks)) {
          $aDeficienciesBooks = $this->sumEqualListItems($aDeficienciesBooks);
        foreach ($aDeficienciesBooks as $aBook) {
          if (isset($aBook->time) && $aBook->time > 0) {
            $aBook->time = 0;
          }
          if ($this->AddOILITimes($iOLIId, $aBook->ident, $aBook->time, $aBook->currquantity, '1') === false) {
            $bIsErr = true;
          }
        }
      }

      if ($isCancelledList === TRUE) {
        $oCanceledList = new CanceledList($this->oParent, $this->pDbMgr);
        $oCanceledList->markOrdersItemsAsCollected($iOLIId, $aBooksScanned);
        $oCanceledList->markOrdersItemsAsCollected($iOLIId, $aDeficienciesBooks);
        $oMMDocument = new MMdocumentCanceledList($this->pDbMgr);
        $oMMDocument->doSendMMByOrdersItemsList($iOLIId);
        $oCanceledList->closeList($iOLIId);
        return $this->showUnpackingList($iOLIId);
      }
      //$this->clearComplatationData($iOLIId);
    }

    if ( $bIsErr === false) {
      echo '<script type="text/javascript">window.close();</script>';
//      die;
    } else {
      echo '<script type="text/javascript">alert("Wystąpił błąd zapisywania czasów listy !"); window.close();</script>';
//      die;
    }
  }// end of saveListData() method


    /**
     * @param $aBooksScanned
     * @return stdClass
     */
    private function sumEqualListItems($aBooksScanned) {

        $newObj = [];
        foreach ($aBooksScanned as $aBook) {
            if (isset($newObj[$aBook->ident])) {
                $newObj[$aBook->ident]->currquantity += $aBook->currquantity;
            } else {
                $newObj[$aBook->ident] = $aBook;
            }
        }
        return $newObj;
    }


  /**
   *
   * @param int $iOLIId
   */
  protected function showUnpackingList($iOLIId) {

    $_POST['products_ean'] = $this->getEANConfirmedItemsLists($iOLIId);
    include_once('modules/m_zamowienia_magazyn_przyjecia/Module_unpacking.class.php');
    $oUnpacking = new Module__zamowienia_magazyn_przyjecia__unpacking($this->oParent);
    return $oUnpacking->doShowList();
  }

  /**
   *
   * @param type $iOLIId
   * @return type
   */
  protected function getEANConfirmedItemsLists($iOLIId) {
    $sSql = '
      SELECT isbn_plain, confirmed_quantity
      FROM orders_items_lists_items
      WHERE orders_items_lists_id = '.$iOLIId.' AND confirmed_quantity > 0
      ';
    $aConfirmedItems = $this->pDbMgr->GetAll('profit24', $sSql);
    return $this->getEANFromConfirmedItems($aConfirmedItems);
  }

  /**
   *
   * @param array $aConfirmedItems
   * @return array
   */
  protected function getEANFromConfirmedItems($aConfirmedItems) {

    $aEAN = [];
    foreach ($aConfirmedItems as $aItem) {
      for($i = 1; $i <= $aItem['confirmed_quantity']; $i++) {
        $aEAN[] = $aItem['isbn_plain'];
      }
    }
    $sEANs = implode("\r\n", $aEAN);
    return $sEANs;
  }

  /**
   *
   * @param int $iOLIId
   * @return bool
   */
  protected function clearComplatationData($iOLIId) {

    return $this->pDbMgr->Update('profit24', 'orders_items_lists', ['books_serialized' => 'NULL', 'books_to_send_serialized' => 'NULL'], ' id = '.$iOLIId.' LIMIT 1');
  }


  /**
   *
   * @param int $iOLIId
   * @return bool
   */
  protected function checkIsCanceledList($iOLIId) {

    $sSql = 'SELECT type FROM orders_items_lists WHERE id = '.$iOLIId;
    return ($this->pDbMgr->GetOne('profit24', $sSql) == CanceledList::type);
  }


  /**
   * Dodanie nowego rekordu czasów zeskanowania
   *
   * @param int $iOLIId
   * @param string $sIdent
   * @param int $iTime
   * @param int|string $scQuantity
   * @return boolean
   */
  protected function AddOILITimes($iOLIId, $sIdent, $iTime, $scQuantity, $cType) {

    $sSql = 'SELECT id FROM orders_items_lists_items WHERE orders_items_lists_id ='.$iOLIId.' AND CONCAT(orders_id, ",", orders_items_id) = "'.$sIdent.'"';
    $iOILIId = $this->pDbMgr->GetOne('profit24', $sSql);

    // dopasowanie
    if ($iOILIId > 0) {
        return $this->AddOIILITimeItem($iOILIId, $iTime, $scQuantity, $cType);
    } else {
      return false;
    }
  }// end of AddOILITimes() method

    /**
     * @param $iOILIId
     * @param $iTime
     * @param $scQuantity
     * @param $cType
     * @return bool
     */
    public function AddOIILITimeItem($iOILIId, $iTime, $scQuantity, $cType)
    {
        $sSql = 'SELECT id FROM orders_items_lists_items_times WHERE id = ' . $iOILIId . ' AND type = "' . $cType . '"';
        $iOILITId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($iOILITId > 0) {
            // przcież nie będziemy aktualizować czasu zbierania
            return true;
        } else {
            $scWeight = $this->getProductWeight($iOILIId);

            $aValues = array(
                'id' => $iOILIId,
                'type' => $cType,
                'time' => $iTime,
                'sc_quantity' => $scQuantity,
                'sc_weight' => $scWeight,
                'user_id' => $_SESSION['user']['id'],
                'created' => 'NOW()'
            );
            file_put_contents("orders_items_lists_items_times.log", print_r($aValues, true), FILE_APPEND | LOCK_EX);
            return ($this->pDbMgr->Insert('profit24', 'orders_items_lists_items_times', $aValues) === false ? false : true);
        }
    }

    /**
     * @param $iOILIId
     * @return mixed
     */
    protected function getProductWeight($iOILIId) {

        $sSql = 'SELECT P.weight
                 FROM orders_items_lists_items AS OILI
                 JOIN orders_items AS OI
                  ON OI.id = OILI.orders_items_id
                 JOIN products AS P
                  ON OI.product_id = P.id
                 WHERE OILI.id = '.$iOILIId.'
                 ';
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

  /**
   * Dowiązanie widoku do listy produktów do odznaczenia
   *
   * @param \Smarty $pSmarty
   * @param array $aBooks
   * @return string
   */
  protected function getViewProductsScanner(&$pSmarty, $aBooks, $bChangeLocation = false) {

    $aChars = $this->charsArrayLine();
    $pSmarty->assign('aChars', $aChars);

    $sJS = json_encode($aBooks['items']);
    $pSmarty->assign('sProductsEncoded', $sJS);
    $pSmarty->assign('iChangeStatus', $bChangeLocation == true ? '1' : '0');
    $sHtml = $pSmarty->fetch('collect_products.tpl');
    $pSmarty->clear_assign('sProductsEncoded');
    $pSmarty->clear_assign('aChars');
    $pSmarty->clear_assign('iChangeStatus');
    return $sHtml;
  }// end of getViewProductsScanner() method


  /**
   * Metoda pobiera tablicę zanków
   *
   * @return array
   */
  protected function charsArrayLine() {

    $aChars =
      array(
          'chars' => array(
          array('Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'),
          array('A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'),
          array('', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '&#8629;'),
          ),
          'int' => array(
              array(7, 8, 9),
              array(4, 5, 6),
              array(1, 2, 3),
              array(0, 'X', '&#8629;')
          )
      );
    return $aChars;
  }// end of charsArrayLine() method


  /**
   * Metoda przelicza ile egz jest na tramwaju a ile na magazynie
   *
   * @param type $aBooks
   * @param int $iCountMagazine
   * @param int $iCountTramwaj
   */
  public function countQuantityItems($aBooks, &$iCountMagazine, &$iCountTramwaj) {
    $iCountMagazine = 0;
    $iCountTramwaj = 0;
    foreach ($aBooks as $aBook) {
      if ($aBook['pack_number'] != '') {
        // tramwaj
        $iCountTramwaj += $aBook['quantity'];
      } else {
        // magazyn
        $iCountMagazine += $aBook['quantity'];
      }
    }
  }// end of countQuantityItems() method


  /**
   * Parsuje listę produktów
   *
   * @param array $aBooks
   * @return array
   */
  public function parseBooksToList($aBooks) {
    $aAllovedCols = array(
        'ident',
        'photo',
        'quantity',
        'name',
        'authors',
        'publisher',
        'isbn_plain',
        'isbn_10',
        'isbn_13',
        'ean_13',
        'pack_number',
        'pack_number_area',
        'stock_pack_number',
        'checked',
        'weight',
        'publication_year',
        'product_id',
        'random_pack_number',
        'CHL_id',
        'profit_g_act_stock',
        'ident_summary',
        'magazine_stock_localizations_reserved',
        'sub_products',
        'SL_id',
        'OILI_ID',
        'SLOILI_ID',
    );

      $newArrayBooks = [];
    foreach ($aBooks as &$aBook) {
        //var_dump($aBook);die;
      foreach ($aBook as $sCol => $sVal) {
        if (!in_array($sCol, $aAllovedCols)) {
          unset($aBook[$sCol]);
        }
        $aBook['shelf_number'] = $aBook['pack_number'];
      }
      $aBook['checked'] = '0';
      $aBook['currquantity'] = '0';
        if (isset($aBook['magazine_stock_localizations_reserved']) && !empty($aBook['magazine_stock_localizations_reserved'])) {
            foreach ($aBook['magazine_stock_localizations_reserved'] as $supply) {
                $aBook['shelf_number'] = (string)$supply['container_id'];
                $aBook['pack_number'] = (string)$supply['container_id'];
                $aBook['quantity'] = (string)$supply['own_reserved'];
                unset($aBook['magazine_stock_localizations_reserved']);
                if ($aBook['OILI_ID'] > 0 && $supply['id'] > 0) {
                    $aBook['SLOILI_ID'] = $this->getOrderListItemStockLocation($aBook['OILI_ID'], $supply['id']);
                }
                $aBook['pack_number_area'] = intval($this->getLocationArea($aBook['pack_number']));
                $newArrayBooks[] = $aBook;
            }
        } else {
            unset($aBook['magazine_stock_localizations_reserved']);
            if ($aBook['pack_number'] != '') {
                $aBook['pack_number_area'] = intval($this->getLocationArea($aBook['pack_number']));
            }
            $newArrayBooks[] = $aBook;
        }
    }
    usort($newArrayBooks, 'sortItems');
    return $newArrayBooks;
  }// end of parseBooksToList() method


  /**
   * Metoda generuje pdf'a
   *
   * @param string $sHTML
   * @param imagedata $im
   */
  public function getPDF($sHTML, $im, $aBooks) {

//    require_once('tcpdf/config/lang/pl.php');
		require_once('tcpdf/tcpdf.php');

		// create new PDF document
		$pdf = new MYPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('profit24');
		$pdf->SetTitle('Lista produktów do zebrania');
		$pdf->SetSubject('Lista produktów do zebrania');
		$pdf->SetKeywords('');
		$pdf->setPrintHeader(true);

    $pdf->headerImage = $im;
    $pdf->headerText = '<span style="font-size: 10pt; font-weight: bold; ">Lista produktów do zebrania</span><br /><span style="font-size: 10pt;">Numer listy: '.$aBooks['number'].'<br />
Data generowania: '.date('d.m.Y H:i:s').'<br />
Wygenerowal: '.$aBooks['author'].
      '</span>';

		$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(2, 28, 2);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 10);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		//set some language-dependent strings
		$pdf->setLanguageArray($l);

		// set font
		$pdf->SetFont('freesans', '', 8);


		// add a page
		$pdf->AddPage();
    $pdf->setJPEGQuality(100);

    //$pdf->Image('@'.$im, 80, 0);
		$pdf->writeHTML($sHTML, true, false, false, false, '');

		$pdf->Output('lista_do_zebrania.pdf', 'D');
  }// end fo getPDF)( method
}
