<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia - historia wysłanych emaili z zamówieniami'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 *
 */
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\StockLocation;
use LIB\orders\listType\filters\checkMagazine;

require_once('Common_get_ready_orders_books.class.php');

/**
 * Klasa pobiera listę produktów gotowych do zebrania i wysłania
 *
 */
class Module__zamowienia_magazyn_zbieranie_tramwaj__list_history extends Common_get_ready_orders_books
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    /**
     *
     * @var \DatabaseManager
     */
    var $pDbMgr;

    /**
     * Konstruktor klasy
     *
     * @return    void
     */
    function __construct(&$pSmarty, $bInit)
    {
        global $aConfig, $pDbMgr;
        $this->pDbMgr = $pDbMgr;

        $this->sErrMsg = '';
        $this->iLangId =& $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];


        $sDo = '';
        $iId = 0;
        $iPId = 0;

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        if (isset($_GET['pid'])) {
            $iPId = intval($_GET['pid']);
        }
        if (isset($_GET['id'])) {
            $iId = intval($_GET['id']);
        }

        $this->aPrivileges =& $_SESSION['user']['privileges'];
        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }
        parent::__construct($bInit);

        switch ($sDo) {
            case 'get':
                $this->GetPDFFile($iId);
                break;

            case 'save_list_data':
                $this->saveListData($pSmarty);
                break;

            case 'get_list':
                $this->GetList($pSmarty, $iId);
                break;

            case 'dump_data':
                $this->GetDumpData($iId);
                break;

            case 'open_list':
                $this->openCloseList($pSmarty, $iId);
                break;

            case 'save_list_data':
                $this->doSave_list_data($pSmarty, $iId);
                break;

            case 'edit':
                $this->doEditt($pSmarty, $iId);
                break;

            case 'update':
                $this->doUpdatee($pSmarty, $iId);
                break;

            case 'reopen_list':
                $this->reopenList($pSmarty, $iId);
                break;

            case 'history':
                $this->history($pSmarty, $iId);
                break;
            default:
                $this->Show($pSmarty);
                break;
        }
    } // end Module() function


    /**
     *
     * @global array $aConfig
     * @param Smarty $pSmarty
     * @param int $iId
     * @return html
     */
    public function doUpdatee(&$pSmarty, $iId)
    {
        global $aConfig;

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty);
            return;
        }


        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->Show($pSmarty);
            return;
        }

        $aValues = [
            'package_number' => $_POST['package_number']
        ];
        if ($this->pDbMgr->Update('profit24', 'orders_items_lists', $aValues, ' id = ' . $iId) === false) {
            $sMsg = _('Wystąpił błąd podczas aktualizacji listy');
            AddLog($sMsg, true);
            $this->sMsg = GetMessage($sMsg, true);
        } else {
            $sMsg = _('Wprowadzono zmiany w liście');
            AddLog($sMsg, false);
            $this->sMsg = GetMessage($sMsg, false);
        }
        return $this->Show($pSmarty);
    }

    /**
     *
     * @param Smarty $pSmarty
     * @param int $iId
     * @return HTML
     */
    private function doEditt(&$pSmarty, $iId)
    {
        global $aConfig;

        $sSql = 'SELECT package_number 
             FROM orders_items_lists AS OIL
             WHERE id = ' . $iId;
        $aData = $this->pDbMgr->GetRow('profit24', $sSql);
        if (!empty($_POST)) {
            $aData = $_POST;
        }

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('list_data', _('Edytuj listę'), array('action' => phpSelf(['id' => $iId])));
        $pForm->AddHidden('do', 'update');
        $pForm->AddText('package_number', _('Numer kuwety'), $aData['package_number'], array());
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));
        return $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
    }


    /**
     *
     */
    public function doSave_list_data()
    {
        $this->saveListData($this->pSmarty);
    }

    /**
     *
     * @param \Smarty $pSmarty
     * @param int $iId
     */
    public function openCloseList($pSmarty, $iId)
    {

        $sSql = '
            UPDATE orders_items_lists 
            SET closed = IF(closed = "1", "0", "1") 
            WHERE id = ' . $iId;

        if ($this->pDbMgr->Query('profit24', $sSql) === false) {
            $sMsg = sprintf(_('Wystąpił błąd podczas zmiany statusu listy %s na kompletowana'), $iId);
            AddLog($sMsg);
            $this->sMsg = GetMessage($sMsg);
        } else {
            $sMsg = sprintf(_('Status listy %s został zmieniony na kompletowana'), $iId);
            AddLog($sMsg, false);
            $this->sMsg = GetMessage($sMsg, false);
        }
        $this->Show($pSmarty);
    }

    /**
     *
     * @param \Smarty $pSmarty
     * @param int $iId
     */
    public function reopenList($pSmarty, $iId)
    {
        $sSql = '
            UPDATE orders_items_lists 
            SET books_serialized = NULL, books_to_send_serialized = NULL, scanned_books_serialized = NULL
            WHERE id = ' . $iId;

        if ($this->pDbMgr->Query('profit24', $sSql) === false) {
            $sMsg = sprintf(_('Wystąpił błąd podczas zmiany statusu listy %s na kompletowana'), $iId);
            AddLog($sMsg);
            $this->sMsg = GetMessage($sMsg);
        } else {
            $sMsg = sprintf(_('Rozpocznij zbieranie %s od nowa'), $iId);
            AddLog($sMsg, false);
            $this->sMsg = GetMessage($sMsg, false);
        }
        $this->Show($pSmarty);
    }


    /**
     *
     *
     * 
     * @param \Smarty $pSmarty
     * @param int $iILId
     * @return void
     */
    protected function GetList($pSmarty, $iILId)
    {

        $sSql = 'SELECT package_number, get_from_train, destination_magazine FROM orders_items_lists WHERE id = ' . $iILId;
        $aOIL = $this->pDbMgr->GetRow('profit24', $sSql);
        $_GET['magazine'] = $aOIL['destination_magazine'];
        $this->destinationMagazineDeficiencies = $aOIL['destination_magazine'];
        $sPackageNumber = $aOIL['package_number'];
        $bGetFromTrain = ($aOIL['get_from_train'] == '1' ? TRUE : FALSE);

        /*
        $sSql = "SELECT orders_id
                 FROM orders_items_lists_items
                 WHERE orders_items_lists_id = ".$iILId;
        $aItems = Common::GetCol($sSql);

        foreach ($aItems as $iItemId) {
          $this->aOrdersAccepted[$iItemId] = '1';
        }

        $aList = $this->getRecordsListLimited(0, null, 0, 0, '', TRUE, $this->aOrdersAccepted, true);
        */
        $oGetOrderData = new \LIB\orders\listType\getOrderData($this->pDbMgr, $aOIL['destination_magazine']);
        $aList = $oGetOrderData->getOrdersItemsListDataByOILId($iILId, $bGetFromTrain);
        // trzeba odpalić mechanizm dzielenia na lokalziacje

        $this->stockLocation = new StockLocation($this->pDbMgr); //addOrderListReservation();
        $aList = $this->stockLocation->getStockLocationReservation($aList);
//        $checkMagazine = new checkMagazine($this->pDbMgr);
//        $checkMagazine->forceReserveItems(false);
//        $checkMagazine->addSellPredict(false);
//        $checkMagazine->unsetIFNotExists(false);
//        $checkMagazine->unsetIFExists(false);
//        $checkMagazine->reserveItem(true);
//        $checkMagazine->setOrdersItems($groupedOrders);
//
//
//        if ($aOIL['destination_magazine'] == Magazine::TYPE_HIGH_STOCK_SUPPLIES) {
//            $checkMagazine->setMagazine(Magazine::TYPE_LOW_STOCK_SUPPLIES);
//        } else {
//            $checkMagazine->setMagazine(Magazine::TYPE_HIGH_STOCK_SUPPLIES);
//        }
//        $checkMagazine->applyFilter();
//        $groupedOrders = $checkMagazine->getFilteredOrdersItems();

        usort($aList, 'sortItems');

        if (!empty($aList) && !empty($sPackageNumber)) {
            $sReturn = $this->getProductsScanner($pSmarty, $iILId, $aList, $sPackageNumber);
            $pSmarty->assign('sContent', $sReturn);
        }

        return;
    }

    /**
     * @param $aList
     * @return array
     */
    public function groupByOrderId($aList) {
        foreach ($aList as $iKey => $item) {
            $orderListItemStockLocation = $this->getOrderListItemStockLocation($item['OILI_ID']);
            if (!empty($orderListItemStockLocation) && count($orderListItemStockLocation) > 1) {
                foreach ($orderListItemStockLocation as $row) {
                    $supply = [
                        'container_id' => $row['container_id'],
                        'own_reserved' => $row['reserved_quantity'],
                    ];
                    $aList[$iKey]['magazine_stock_localizations_reserved'][] = $supply;
                }
            }
        }
        return $aList;
    }

    /**
     * @param $iOILIId
     * @return array
     */
    private function getOrderListItemStockLocation($iOILIId) {

        $sSql = 'SELECT * 
                 FROM stock_location_orders_items_lists_items AS A
                 JOIN stock_location AS B
                  ON A.stock_location_id = B.id
                 WHERE orders_items_lists_items_id = '.$iOILIId;
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * @param $groupedOrders
     * @return array
     */
    public function ungroupArray($groupedOrders) {

        $aList = [];
        foreach ($groupedOrders as $groupedOrder) {
            foreach ($groupedOrder as $item) {
                $aList[] = $item;
            }
        }
        return $aList;
    }


    /**
     * Metoda wyswietla wszystkie dane odnośnieie tej listy
     *
     * @param type $iId
     */
    private function GetDumpData($iId)
    {

        echo '<h1>Waga zamówienia:</h1> <br />';
        $sSql = "SELECT O.order_number, SUM(OI.weight * OI.quantity)
             FROM orders_items_lists_items AS OILI
             JOIN orders AS O
              ON OILI.orders_id = O.id
             JOIN orders_items AS OI
              ON OILI.orders_items_id = OI.id
             WHERE orders_items_lists_id = " . $iId . '
             GROUP BY O.id';
        dump($this->pDbMgr->GetAll('profit24', $sSql));


        echo '<h1>Zamówienia w liście:</h1> <br />';
        $sSql = "SELECT DISTINCT order_number, O.transport_number
             FROM orders_items_lists_items AS OILI
             JOIN orders AS O
              ON OILI.orders_id = O.id
             WHERE orders_items_lists_id = " . $iId;
        dump($this->pDbMgr->GetAssoc('profit24', $sSql));

        echo '<h1>Dane listy:</h1> <br />';
        $sSql = "SELECT * FROM orders_items_lists WHERE id = " . $iId;
        $aList = $this->pDbMgr->GetRow('profit24', $sSql);
        dump($aList);

        $aData = unserialize(base64_decode($aList['debug']));
        dump($aData);

        echo '<h1>Składowe listy:</h1> <br />';
        $sSql = "SELECT * FROM orders_items_lists_items WHERE orders_items_lists_id = " . $iId;
        dump($this->pDbMgr->GetAll('profit24', $sSql));

        echo '<h1>Składowe zamówień z listy:</h1> <br />';
        $sSql = "SELECT OI.*
             FROM orders_items_lists_items AS OILI
             JOIN orders_items AS OI
              ON OILI.orders_items_id = OI.id
             WHERE orders_items_lists_id = " . $iId . '
             ORDER BY OI.order_id DESC';
        $aOI = $this->pDbMgr->GetAll('profit24', $sSql);
        dump($aOI);
        echo '<h1>ERRR duplikaty: </h1> <br />';
        foreach ($aOI as $aItem) {
            $this->getOrdersItemOnDuplicatedList($aItem['id']);
        }
        die;
    }// end of GetDumpData() method

    /**
     *
     */
    private function getOrdersItemOnDuplicatedList($iItemId)
    {

        $sSql = 'SELECT *
             FROM orders_items_lists_items AS OILI
             JOIN orders_items_lists AS OIL
              ON OIL.id = OILI.orders_items_lists_id AND get_from_train = "1"
             JOIN orders AS O
              ON O.id = OILI.orders_id
             WHERE orders_items_id = ' . $iItemId;
        $aLists = $this->pDbMgr->GetAll('profit24', $sSql);
        if (count($aLists) > 1) {
            dump(\LIB\Helpers\ArrayHelper::arrayColumn($aLists, 'order_number'));
        }
    }

    function history($pSmarty,$iId)
    {
        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => _('Historia listy ').$iId,
            'refresh' => true,
            'search' => false,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            /*array(
                'db_field' => 'orders_items_lists_id',
                'content' => _('Numer listy'),
                'sortable' => true,
            ),*/
            array(
                'db_field' => 'orders_items_lists_items_id',
                'content' => _('Numer na liście'),
                'sortable' => false,
            ),
            array(
                 'db_field' => 'stock_location_orders_items_lists_items',
                'content' => ('Numer Lokacji'),
                'sortable' => false,
            ),
            array(
                'db_field' => 'username',
                'content' => _('Użytkownik'),
                'sortable' => false,
            )
        );

        $sSql = 'SELECT /*orders_items_lists_id*/orders_items_lists_items_id,stock_location_orders_items_lists_items_id,CONCAT(u.name, " ", u.surname) as username FROM orders_items_lists_data oild
                 LEFT JOIN users u
                 ON oild.user_id = u.id
                 WHERE orders_items_lists_id  = '.intval($iId);
        $history = $this->pDbMgr->GetAll('profit24',$sSql);

        $pView = new View('list_history', $aHeader, $aAttribs);

        $aColSettings = array();

        $pView->AddRecords($history, $aColSettings);
        $pView->AddRecordsHeader($aRecordsHeader);
            // dodanie stopki do widoku
        $aRecordsFooter = array(
        array()
        );
        $pView->AddRecordsFooter($aRecordsFooter);
        $sJS = '';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') .
        $pView->Show() . $sJS);
        /*
        if (count($history) > 0)
        {
            echo "<table>
                  <thead>
                  <td>orders_items_lists</td>
                  <td>orders_items_lists_items</td>
                  <td>stock_location_orders_items_lists_items</td>
                  <td>Kto</td>
                  </thead>";
            foreach($history as $history_item)
            {
                echo '<tr>
                      <td>'.$history_item['orders_items_lists_id'].'</td>
                      <td>'.$history_item['orders_items_lists_items_id'].'</td>
                      <td>'.$history_item['stock_location_orders_items_lists_items_id'].'</td>
                      <td>'.$history_item['name'].' '.$history_item['surname']."</td>
                      </tr>";
            }
            echo "</table>";
        }
        */
        //var_dump($history);
    }
    /**
     * Generuje liste pozycji w formacie pdf do pobrania
     * @return unknown_type
     */
    function GetPDFFile($iId)
    {
        global $aConfig, $pSmarty;

        include_once('Common_get_ready_orders_books.class.php');
        $oRS = new Common_get_ready_orders_books($pSmarty, false);

        $sSql = 'SELECT get_from_train FROM orders_items_lists WHERE id = ' . $iId;
        $aOIL = $this->pDbMgr->GetRow('profit24', $sSql);
        $bGetFromTrain = ($aOIL['get_from_train'] == '1' ? TRUE : FALSE);

        $oGetOrderData = new \LIB\orders\listType\getOrderData($this->pDbMgr);
        $aOrdersItems = $oGetOrderData->getOrdersItemsListDataByOILId($iId, $bGetFromTrain);
        usort($aOrdersItems, 'sortItems');
        $aList = $aOrdersItems;
//    $aList = $this->getOrdersItemsList($aOrdersItems);

        $_GET['hideHeader'] = '1';
        include_once 'barcode/barcode.php';

        $sCode = str_pad($iId, 13, '0', STR_PAD_LEFT);


        $im = imagecreatetruecolor(300, 100);
        $black = ImageColorAllocate($im, 0x00, 0x00, 0x00);
        $white = ImageColorAllocate($im, 0xff, 0xff, 0xff);
        imagefilledrectangle($im, 0, 0, 300, 100, $white);
        $data = Barcode::gd($im, $black, 150, 50, 0, "code128", $sCode, 2, 50);
        $iRefCountMagazine = 0;
        $iRefCountTramwaj = 0;
        $oRS->countQuantityItems($aList, $iRefCountMagazine, $iRefCountTramwaj);
        $aViewList = $oRS->sumByArray($aList, 'quantity', 'pack_number');

        $aBooks = array();
        $aBooks['number'] = $sCode;
        $aBooks['items'] = $aViewList;
        $aBooks['author'] = $_SESSION['user']['author'];
        $aBooks['count_magazine'] = $iRefCountMagazine;
        $aBooks['count_tramwaj'] = $iRefCountTramwaj;
        $aBooks = $this->parseBooksToListSelf($aBooks);

        $pSmarty->assign_by_ref('aBooks', $aBooks);
        $sHtml = $pSmarty->fetch('PDF/pdf_ready_list.tpl');
        $pSmarty->clear_assign('aBooks');

        // kod kreskowy
        ob_start();
        imagepng($im);
        imagedestroy($im);
        $imagedata = ob_get_clean();

        $oRS->getPDF($sHtml, $imagedata, $aBooks);
    } // end of GetPDF() funciton


    /**
     * Parsuje listę produktów
     *
     * @param array $aBooks
     * @return array
     */
    public function parseBooksToListSelf($aBooks)
    {

        foreach ($aBooks['items'] as $sKey => $aBook) {
            $aBooks['items'][$sKey]['publisher'] = $aBooks['items'][$sKey]['publisher'] == '' ? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$sKey]['publisher']), 0, 25, 'UTF-8');
            $aBooks['items'][$sKey]['authors'] = $aBooks['items'][$sKey]['authors'] == '' ? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$sKey]['authors']), 0, 25, 'UTF-8');
            $aBooks['items'][$sKey]['name'] = $aBooks['items'][$sKey]['name'] == '' ? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$sKey]['name']), 0, 80, 'UTF-8');
        }
        return $aBooks;
    }// end of parseBooksToListSelf() method


    /**
     * Metoda pobiera rekordy na listę ograniczone co do ilości
     *
     * @param int $iLimitRecords
     * @return array

    public function getRecordsListLimited($iLimitRecords = 0, $aOrderItems = array()) {
     *
     * $sSql = $this->getQuery(' O.order_number, OI.id, OI.order_id, OI.quantity,
     * OI.name, OI.publisher, OI.authors, OI.publication_year, OI.edition, OI.product_id,
     * OI.item_type,
     * P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13, IF(OI.source = 51, "1", "0") AS is_g,
     *
     *
     * (
     * SELECT OSH.pack_number
     * FROM orders_send_history AS OSH
     * JOIN orders_send_history_items AS OSHI
     * ON OSH.id = OSHI.send_history_id
     * WHERE OSHI.item_id = OI.id
     * ORDER BY OSH.id DESC
     * LIMIT 1
     * ) AS pack_number,
     *
     *
     * (SELECT id FROM orders_items WHERE parent_id = OI.id AND item_type = "A") AS is_cd,
     * (SELECT profit_g_act_stock FROM products_stock AS PS WHERE PS.id = OI.product_id) as profit_g_act_stock
     * ');
     *
     * if (!empty($aOrderItems)) {
     * $sSql .= " AND O.id IN (".implode(', ', array_keys($aOrderItems)).") ";
     * }
     *
     * $sSql .= "
     * GROUP BY OI.id
     * ORDER BY O.send_date DESC, O.id DESC
     * ". ($iLimitRecords > 0 ? " LIMIT ".$iLimitRecords : '');
     * return $this->pDbMgr->GetAll('profit24', $sSql);
     * }// end of getRecordsListLimited() method
     */

    /**
     * Metoda zwraca podstawowe zamówienie wybierania zamówień
     *
     * @param string $sSelect
     * @return string
     */
    public function getQuery($sSelect)
    {

        /*
         * 1) opłacone, lub odroczony termin płatności,
         * 2) nie "nowe", "zatwierdzone", "zrealizowane", "anulowane"
         * 3) jeśli z G to status musi być "jest nie G"
         * 4) jeśli zewn. to status musi być "jest pot."
         * 5) data wysyłki jeśli została zmieniona to ma być to dziś
         * 6) nie pobrane wcześniej,
         */
        $sSql = "
SELECT " . $sSelect . "
FROM orders_items AS OI
    JOIN orders AS O
    ON OI.order_id = O.id
        /*AND IF (OI.source = '51', (OI.status = '3' OR OI.status = '4'), OI.status = '4')*/
        /*AND OI.get_ready_list = '0'*/
        AND OI.deleted = '0'
        AND (OI.item_type = 'I' OR OI.item_type = 'P')
        AND OI.packet = '0'

    LEFT JOIN products AS P
      ON P.id = OI.product_id
    WHERE 
      1=1 
    ";
        return $sSql;
    }// end of getQuery() method


    /**
     * Metoda wyswietla liste zdefiniowanych kodow Premium SMS
     *
     * @param        object $pSmarty
     * @return    void
     */
    function Show(&$pSmarty)
    {
        global $aConfig;
        $sFilterSql = '';

        // dolaczenie klasy View
        include_once('View/View.class.php');

        // zapamietanie opcji
        rememberViewState($this->sModule);
        $aHeader = array(
            'header' => _('Historia list ZT/ZM'),
            'refresh' => true,
            'search' => true,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'id',
                'content' => _('Numer dokumentu'),
                'sortable' => true,
                'width' => '20'
            ),
            array(
                'db_field' => 'package_number',
                'content' => _('Numer kuwety'),
                'sortable' => true,
            ),
            array(
                'db_field' => 'type',
                'content' => _('Typ'),
                'sortable' => true,
            ),
            array(
                'db_field' => 'transport',
                'content' => _('Spedytor'),
                'sortable' => true,
            ),
            array(
                'db_field' => 'closed',
                'content' => _('Status'),
                'sortable' => true,
            ),
            array(
                'db_field' => 'created',
                'content' => _('Pobrano listę do zebrania'),
                'sortable' => true,
            ),
            array(
                'db_field' => 'created_by',
                'content' => _('Utworzona/Posortowane przez'),
                'sortable' => true,
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '40'
            )
        );


        // pobranie liczby wszystkich kodow
        $sSql = "SELECT COUNT(id) 
						 FROM " . $aConfig['tabls']['prefix'] . "orders_items_lists
						 WHERE 1 = 1" .
            (isset($_POST['f_type']) && $_POST['f_type'] != '' ? ' AND type = "' . $_POST['f_type'] . '" ' : '') .
            (isset($_POST['f_closed']) && $_POST['f_closed'] != '' ? ' AND closed = "' . $_POST['f_closed'] . '" ' : '') .
            (isset($_POST['search']) && !empty($_POST['search']) ? ' AND (id LIKE \'%' . $_POST['search'] . '%\' OR package_number LIKE \'%' . $_POST['search'] . '%\')' : '');
        $iRowCount = intval(Common::GetOne($sSql));
        if ($iRowCount == 0 && !isset($_GET['reset'])) {
            // resetowanie widoku
            resetViewState($this->sModule);
            // ponowne okreslenie liczby rekordow
            $sSql = "SELECT COUNT(id) 
							 FROM " . $aConfig['tabls']['prefix'] . "orders_items_lists
							 WHERE 1 = 1" .
                (isset($_POST['f_type']) && $_POST['f_type'] != '' ? ' AND type = "' . $_POST['f_type'] . '" ' : '') .
                (isset($_POST['f_closed']) && $_POST['f_closed'] != '' ? ' AND closed = "' . $_POST['f_closed'] . '" ' : '') .
                (isset($_POST['search']) && !empty($_POST['search']) ? ' AND (id LIKE \'%' . $_POST['search'] . '%\' OR package_number LIKE \'%' . $_POST['search'] . '%\')' : '');
            $iRowCount = intval(Common::GetOne($sSql));
        }

        if ($_SESSION['user']['priv_order_status'] == '1' || $_SESSION['user']['type'] == '1') {
            $bPrivilagesRemoveReservations = true;
        } else {
            $bPrivilagesRemoveReservations = false;
        }

        $pView = new View('sort_history', $aHeader, $aAttribs);
        if ($iRowCount > 0) {
            // dodanie Pagera do widoku
            $iCurrentPage = $pView->AddPager($iRowCount);
            $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
            $iStartFrom = ($iCurrentPage - 1) * $iPerPage;
            // pobranie zamówień bankowych
            $sSql = "SELECT OIL.id, OIL.package_number, OIL.type, OTM.name AS transport, OIL.closed, OIL.created, (SELECT CONCAT(U.name, ' ', U.surname) FROM users AS U WHERE U.id = OIL.user_id LIMIT 1) as created_by, OIL.big
							 FROM " . $aConfig['tabls']['prefix'] . "orders_items_lists AS OIL
               LEFT JOIN orders_transport_means AS OTM
                ON OIL.transport_id = OTM.id
							 WHERE 1 = 1 " .
                (isset($_POST['f_type']) && $_POST['f_type'] != '' ? ' AND OIL.type = "' . $_POST['f_type'] . '" ' : '') .
                (isset($_POST['f_closed']) && $_POST['f_closed'] != '' ? ' AND closed = "' . $_POST['f_closed'] . '" ' : '') .
                (isset($_POST['search']) && !empty($_POST['search']) ? ' AND (id LIKE \'%' . $_POST['search'] . '%\' OR OIL.package_number LIKE \'%' . $_POST['search'] . '%\')' : '') .
                ' ORDER BY ' . (isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' OIL.id DESC ') .
                (isset($_GET['order']) && !empty($_GET['order']) ? ' ' . $_GET['order'] : '') .
                " LIMIT " . $iStartFrom . ", " . $iPerPage;
            $aRecords =& Common::GetAll($sSql);
            foreach ($aRecords as $iKey => $aRecord) {
                switch ($aRecord['closed']) {
                    case '0':
                        $aRecords[$iKey]['closed'] = _('Kompletowana');
//                        $aRecords[$iKey]['disabled'][] = 'activate';
                        break;
                    case '1':
                        $aRecords[$iKey]['closed'] = _('Zatwierdzona');
                        $aRecords[$iKey]['disabled'][] = 'get_list';
                        break;
                    case '2':
                        $aRecords[$iKey]['closed'] = _('Sortowana');
                        $aRecords[$iKey]['disabled'][] = 'get_list';
                        break;
                    case '3':
                        $aRecords[$iKey]['closed'] = _('Oczekuje na list przewozowy - wstrzymana');
                        $aRecords[$iKey]['disabled'][] = 'get_list';
                        break;
                }
                if ($bPrivilagesRemoveReservations === false) {
                    $aRecords[$iKey]['disabled'][] = 'activate';
                    $aRecords[$iKey]['disabled'][] = 'edit';
                }

                switch ($aRecord['type']) {
                    case '0':
                        $aRecords[$iKey]['type'] = _('Lista Zwykła');
                        break;
                    case '1':
                        $aRecords[$iKey]['type'] = _('Lista Single');
                        break;
                    case '6':
                        $aRecords[$iKey]['type'] = _('Lista Double');
                        break;
                    case '2':
                        $aRecords[$iKey]['type'] = _('Lista Połączone (wspólna wysyłka)');
                        break;
                    case '3':
                        $aRecords[$iKey]['type'] = _('Lista Tramwajowa');
                        break;
                    case '4':
                        $aRecords[$iKey]['type'] = _('Lista Anulowane');
                        break;
                    case '5':
                        $aRecords[$iKey]['type'] = _('Lista Wysokie składowanie');
                        break;
                    case '7':
                        $aRecords[$iKey]['type'] = _('Dostawa niezależna');
                        break;
                    case '8':
                      $aRecords[$iKey]['type'] = _('Zbieranie do przesunięcia z B6 na A4');
                      break;
                    case '9':
                        $aRecords[$iKey]['type'] = _('Zwrot do dostawcy');
                        break;
                }
                if ($aRecord['big'] == '1') {
                    $aRecords[$iKey]['type'] .= _(' - Duże');
                }
                unset($aRecords[$iKey]['big']);
            }
            $aColSettings = array(
                'action' => array(
                    'actions' => array('get_list', 'details', 'dump_data', 'edit', 'activate','reopen_list','history'),
                    'params' => array(
                        'details' => array('id' => '{id}', 'do' => 'get'),
                        'dump_data' => array('id' => '{id}', 'do' => 'dump_data'),
                        'get_list' => array('id' => '{id}', 'do' => 'get_list'),
                        'edit' => array('id' => '{id}', 'do' => 'edit'),
                        'activate' => array('id' => '{id}', 'do' => 'open_list'),
                        'reopen_list' => array('id' => '{id}', 'do' => 'reopen_list'),
                        'history' => array('id' => '{id}', 'do' => 'history')
                    ),
                    'show' => false,
                    'icon' => array(
                        'dump_data' => 'comments',
                        'get_list' => 'books',
                        'activate' => 'publish',
                        'reopen_list' => 'back',
                        'history' => 'preview'
                    )
                )
            );

            foreach ($aRecords as $iKey => $aItem) {
                $aRecords[$iKey]['transport'] .= $this->getAdditionalsTransport($aItem['id']);
            }
            $types = [
                [
                    'label' => 'Zwykła',
                    'value' => '0'
                ],
                ['label' => 'Single',
                    'value' => '1'
                ],
                ['label' => 'Połączone (wspólna wysyłka)',
                    'value' => '2'
                ],
                [
                    'label' => 'Tramwajowa',
                    'value' => '3'
                ],
                [
                    'label' => 'Anulowana',
                    'value' => '4'
                ],
                [
                    'label' => 'Wysokie składowanie',
                    'value' => '5'
                ],
                [
                    'label' => 'Dostawa niezależna',
                    'value' => '7'
                ]

            ];
            $pView->AddFilter('f_type', _('Typ'), addDefaultValue($types, _('Wszystkie')), $_POST['f_type']);

            $status = [
                [
                    'label' => 'Kompletowana',
                    'value' => '0',
                ],
                [
                    'label' => 'Zatwierdzona',
                    'value' => '1',
                ],
                [
                    'label' => 'Sortowana',
                    'value' => '2',
                ],
                [
                    'label' => 'Oczekuje na list przewozowy - wstrzymana',
                    'value' => '3',
                ],
            ];

            $pView->AddFilter('f_closed', _('Status'), addDefaultValue($status, _('Wszystkie')), $_POST['f_closed']);

            // dodanie rekordow do widoku
            $pView->AddRecords($aRecords, $aColSettings);
        }
        $pView->AddRecordsHeader($aRecordsHeader);
        // dodanie stopki do widoku
        $aRecordsFooter = array(
            array()
        );
        $pView->AddRecordsFooter($aRecordsFooter);
        $sJS = '
      <script type="text/javascript">
      $( function() {
        $("a[href$=\'reopen_list\']").click(function(){
          var isGood2=confirm("Czy na pewno chcesz rozpocząć zbieranie listy od nowa ?");
          if (isGood2) {
            window.localStorage.clear();
            return true;
          } else {
            return false;
          }
        });
        $("a[title^=\'Zbieranie\']").attr("target", "_blank");
        $("a[title^=\'Zmień status\']").click( function() {
          var isGood=confirm("Czy na pewno chcesz zmienić status listy ?");
          if (isGood) {
            return true;
          } else {
            return false;
          }
        });
      });
      </script>';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') .
            $pView->Show() . $sJS);
    } // end of Show() function


    /**
     *
     * @param int $iOLId
     * @return string
     */
    private function getAdditionalsTransport($iOLId)
    {

        $sSql = 'SELECT GROUP_CONCAT(OTM2.name SEPARATOR ", <br />") AS transport
               FROM orders_items_lists_tranport AS OILT
               LEFT JOIN orders_transport_means AS OTM2
                ON OILT.orders_transport_means_id = OTM2.id 
               WHERE 
                OILT.orders_items_lists_id = ' . $iOLId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }
} // end of Module Class
