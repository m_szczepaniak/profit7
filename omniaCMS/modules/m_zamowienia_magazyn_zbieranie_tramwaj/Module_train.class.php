<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj;

use Admin;
use DatabaseManager;
use LIB\orders\listType\filters;
use LIB\orders\listType\manageGetOrdersItemsList;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;

/**
 * Description of Module_part_linked_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_zbieranie_tramwaj__train extends Abstract_getListType implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected  $sMsg;
  
  /**
   *
   * @var Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  /**
   *
   * @var DatabaseManager
   */
  protected  $pDbMgr;
  
  /**
   *
   * @var manageGetOrdersItemsList 
   */
  protected $oManageLists;
  
  public function __construct(Admin $oClassRepository) {

      dump('tu');
      die;
    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    $this->oManageLists = new manageGetOrdersItemsList($this->pDbMgr);
    $this->oManageLists->setListType(array('PART', 'SORTER_TRAIN'));
    $this->oManageLists->setGetFromTrain(TRUE);
    $this->oManageLists->initOrderListType();
    $this->sListName = 'Pełne Tramwaj, Częściowe';
    $oFilter = new filters\zero_quantity21($this->pDbMgr);
    $this->oManageLists->addFilter($oFilter);
    $this->oManageLists->addRequiredFilter($oFilter);
    $oFilter = new filters\quantity15_20($this->pDbMgr);
    $this->oManageLists->addFilter($oFilter);
    $oFilter = new filters\quantity11_14($this->pDbMgr);
    $this->oManageLists->addFilter($oFilter);
    $oFilter = new filters\quantity7_10($this->pDbMgr);
    $this->oManageLists->addFilter($oFilter);
    $oFilter = new filters\ordersLimit15($this->pDbMgr);
    $this->oManageLists->addFilter($oFilter);
    $this->cListType = '0';
  }
}
