<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj;

use Admin;
use DatabaseManager;
use LIB\orders\listType\filters\ordersItemsLimit20;
use LIB\orders\listType\filters\ordersItemsLimit45;
use LIB\orders\listType\filters\typeFilter;
use LIB\orders\listType\manageGetOrdersItemsList;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;

require_once('Common_get_ready_orders_books.class.php');
require_once('Common_get_ready_orders_books.class.php');
/**
 * Description of Module_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_zbieranie_tramwaj__train_single extends Abstract_getListType implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected  $sMsg;
  
  /**
   *
   * @var Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  /**
   *
   * @var DatabaseManager
   */
  protected  $pDbMgr;
  
  /**
   *
   * @var manageGetOrdersItemsList 
   */
  protected $oManageLists;
  
  public function __construct(Admin $oClassRepository) {
    global $aConfig;

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    $this->oManageLists = new manageGetOrdersItemsList($this->pDbMgr);
    $this->oManageLists->setListType('SINGLE_SORTER_TRAIN');
    $this->oManageLists->setGetFromTrain(TRUE);
    $this->oManageLists->initOrderListType();
    $this->sListName = 'Single Tramwaj';
    $this->cListType = '1';


      $oFilter = new typeFilter($this->pDbMgr);
      $oFilter->setFilterCollectingType($_GET['type']);

      $this->oManageLists->addFilter($oFilter);
      $this->oManageLists->addRequiredFilter($oFilter);
	  
	  if ($_GET['type'] == typeFilter::TYPE_MIXED) {
          $oFilter = new ordersItemsLimit20($this->pDbMgr);
          $this->oManageLists->addFilter($oFilter);
	  } else {
          $oFilter = new ordersItemsLimit45($this->pDbMgr);
          $this->oManageLists->addFilter($oFilter);
	  }
  }
}
