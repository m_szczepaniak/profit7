<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-09-17 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj;

use Admin;
use Common_get_ready_orders_books;
use DatabaseManager;

/**
 * Description of CanceledList
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class CanceledList extends Common_get_ready_orders_books {

  protected $pDbMgr;
  
  private $aList;
  private $iListId;
  
  CONST type = 4;

  public function __construct(Admin $oParent, DatabaseManager $pDbMgr) {
    $this->pDbMgr = $pDbMgr;
    parent::__construct($oParent);
  }
  
  
  /**
   * 
   * @return array
   */
  public function createList($aPost) {
    
    $sDebug = base64_encode(serialize($aPost));
    $this->aList = $this->getOrdersCanceled();
    usort($this->aList, 'sortItems');

    $this->iListId = $this->addList($sDebug);
    $this->AddListItems($this->iListId, $this->aList);
    return $this->aList;
  }
  
  /**
   * 
   * @return array
   */
  public function getListArray() {
    return $this->aList;
  }
  
  public function getListId() {
    return $this->iListId;
  }
  
  /**
   * 
   * @param array $aCollected
   */
  public function markOrdersItemsAsCollected($iOLIId, $aCollected) {

    foreach ($aCollected as $aItem) {
        $rowItems = explode('|', $aItem->ident_summary);
        foreach ($rowItems as $idents) {
            $aIdent = explode(',', $idents);

            $itemQuantity = $aIdent[2];
            if ($aItem->currquantity > 0) {
                $aItem->currquantity = $aItem->currquantity - $itemQuantity;
                if ($aItem->currquantity < 0) {
                    $confirmQuantity = $aItem->currquantity + $itemQuantity;
                } else {
                    $confirmQuantity = $itemQuantity;
                }

                $iOrderId = $aIdent[0];
                $iOrderItemId = $aIdent[1];
                $this->changeOrderCollectedQuantity($iOrderId, $iOrderItemId, $confirmQuantity);
                $this->changeOrderItemListItem($iOLIId, $iOrderId, $iOrderItemId, $confirmQuantity);
            } else {
                break;
            }
        }
    }
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @param int $iConfirmedQuantity
   * @return bool
   */
  private function changeOrderItemListItem($iOLIId, $iOrderId, $iOrderItemId, $iConfirmedQuantity) {
    
    $aValues = [
        'confirmed_quantity' => $iConfirmedQuantity
    ];
    return $this->pDbMgr->Update('profit24', 'orders_items_lists_items', $aValues, ' orders_items_id = ' . $iOrderItemId . ' AND orders_id = ' . $iOrderId . ' AND orders_items_lists_id = ' . $iOLIId );
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @param int $iConfirmedQuantity
   * @return bool
   */
  private function changeOrderCollectedQuantity($iOrderId, $iOrderItemId, $iConfirmedQuantity) {
    
    $sSql = 'UPDATE orders_items
             SET canceled_collected = canceled_collected + '.$iConfirmedQuantity.'
             WHERE id = '.$iOrderItemId.' AND order_id = '.$iOrderId;
    return $this->pDbMgr->Query('profit24', $sSql);
  }
  
  /**
   * 
   * @return int
   */
  protected function getCountOrdersCancelled() {
    
    $iSum = 0;
    $aItems = $this->getOrdersCanceled();
    foreach ($aItems as $aItem) {
      $iSum += $aItem['quantity'];
    }
    return $iSum;
  }
  
  
  /**
   * 
   * @return array
   */
  private function getOrdersCanceled() {
    $sSql = 'SELECT 
      CONCAT(OI.order_id, ",", OI.id) AS ident, 
      O.order_number, 
      OI.id, 
      OI.order_id, 
      
      OI.quantity - OI.canceled_collected AS quantity, 


      OI.name, OI.publisher, OI.authors, OI.publication_year, OI.edition, OI.product_id, 
      OI.item_type,
      P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13, 
      IF(OI.source = 51, "1", "0") AS is_g,

      (
      IF (
        OI.source = 51 AND OI.status = "3", 
          (
            SELECT OSH.pack_number
            FROM orders_send_history AS OSH
            JOIN orders_send_history_items AS OSHI
              ON OSH.id = OSHI.send_history_id
            WHERE OSHI.item_id = OI.id AND
                  OSH.source IN ("33", "34")
            ORDER BY OSH.id DESC
            LIMIT 1
           )
        ,
          (
            SELECT OSH.pack_number
            FROM orders_send_history AS OSH
            JOIN orders_send_history_items AS OSHI
              ON OSH.id = OSHI.send_history_id
            WHERE OSHI.item_id = OI.id 
                AND OSH.pack_number <> ""
                AND OSH.pack_number IS NOT NULL
            ORDER BY OSH.id DESC
            LIMIT 1
          )
        )
      ) AS pack_number,
      (SELECT id FROM orders_items WHERE parent_id = OI.id AND item_type = "A") AS is_cd,
      (SELECT profit_g_act_stock FROM products_stock AS PS WHERE PS.id = OI.product_id) as profit_g_act_stock,
      (SELECT CONCAT(directory, "|", photo) FROM products_images AS PI WHERE PI.product_id = OI.product_id LIMIT 1) AS photo,
      OI.weight

             FROM orders_items AS OI
             JOIN orders AS O
               ON O.id = OI.order_id
               AND O.order_date > DATE_SUB(CURRENT_DATE(), INTERVAL 3 MONTH)
             JOIN products AS P
               ON P.id = OI.product_id
             WHERE 
                (O.order_status = "5" OR (OI.deleted = "1" AND (O.order_status = "4" OR O.order_status = "3"))) 
                AND (OI.item_type = "I" OR OI.item_type = "P")
                AND OI.packet = "0"
                
                AND OI.quantity > OI.canceled_collected
                AND OI.source != "51"
                AND OI.status = "4"
                
                ';
    // AND OI.source <> "51"
    // AND OI.canceled_collected = 0
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

  /**
   * 
   * @param int $iId
   * @param array $aList
   * @return boolean
   */
  private function AddListItems($iId, $aList) {
    
    $this->pDbMgr->BeginTransaction('profit24');
    foreach ($aList as $aItem) {
      $aValuesOILI = array(
         'orders_items_lists_id' => $iId,
         'orders_items_id' => $aItem['id'],
         'orders_id' => $aItem['order_id'],
         'products_id' => $aItem['product_id'],
         'quantity' => $aItem['quantity'],
         'is_g' => $aItem['is_g'],
         'isbn_plain' => $aItem['isbn_plain'],
         'isbn_10' => $aItem['isbn_10'],
         'isbn_13' => $aItem['isbn_13'],
         'ean_13' => $aItem['ean_13'],
     );
     if ($this->pDbMgr->Insert('profit24', 'orders_items_lists_items', $aValuesOILI) === FALSE) {
       $this->pDbMgr->RollbackTransaction('profit24');
       return false;
     }
    }
    $this->pDbMgr->CommitTransaction('profit24');
    return true;
  }// end of AddListItems() method
  
  
  /**
   * Metoda zamyka listę
   * 
   * @return boolean
   */
  public function closeList($iOLIId) {
    
    // zamykamy listę
    $aValuesOIL = array(
        'closed' => '1',
        'user_id' => $_SESSION['user']['id']
    );
    if ($this->pDbMgr->Update('profit24', 'orders_items_lists', $aValuesOIL, ' id = '.$iOLIId) === FALSE) {
      return false;
    }
    return true;
  }// end of closeList() method
  
  
  private function addList($sDebug) {
    
    // dodajemy teraz dane listy
    $aValuesOIL = array(
        'user_id' => $_SESSION['user']['id'],
        'document_number' => ($this->pDbMgr->GetOne('profit24', 'SELECT MAX( id ) FROM orders_items_lists') + 1),
        'package_number' => '1',
        'closed' => '0',
        'type' => self::type, // TYP zbieranie anulowanych
        'get_from_train' => '0',
        'created' => 'NOW()',
        'debug' => $sDebug
    );
    return $iILId = $this->pDbMgr->Insert('profit24', 'orders_items_lists', $aValuesOIL);
  }
}
