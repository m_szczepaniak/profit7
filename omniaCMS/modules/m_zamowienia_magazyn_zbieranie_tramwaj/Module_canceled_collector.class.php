<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj;

use Admin;
use DatabaseManager;
use FormTable;
use omniaCMS\lib\interfaces\ModuleEntity;

require_once('Common_get_ready_orders_books.class.php');
/**
 * Description of Module_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_zbieranie_tramwaj__canceled_collector extends CanceledList implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected  $sMsg;
  
  /**
   *
   * @var Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  /**
   *
   * @var DatabaseManager
   */
  protected  $pDbMgr;
  
  public function __construct(Admin $oClassRepository) {

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    parent::__construct($oClassRepository, $this->oClassRepository->pDbMgr);
  }

  /**
   * 
   */
  public function doDefault() {
    
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('canceled_get', _("Pobierz produkty anulowane lub usunięte z zamówień"), array('action' => phpSelf(), 'target' => '_blank'), array('col_width'=>355), true);
    $pForm->AddHidden('do', 'getList');
    $pForm->AddRow('Ilość anulowanych/usuniętych, nie zebranych', '<span style="font-size: 3em">'.$this->getCountOrdersCancelled().'</span> ');
    $pForm->AddInputButton("generate", _('Pobierz listę'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
    return $pForm->ShowForm();
  }
  
  /**
   * 
   * @return string
   */
  public function doGetList() {
    $aList = $this->createList($_POST);

    if (empty($aList)) {
      $sMsg = _('Brak produktów do zebrania');
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      return $this->doDefault();
    }
    
    $sReturn = $this->getProductsScanner($this->pSmarty, $this->getListId(), $aList, '0000000');
    return $sReturn;
  }
  
  
  /**
   * 
   */
  public function doSave_list_data() {
    $this->saveListData($this->pSmarty);
  }

  public function getMsg() {
    return $this->sMsg;
  }
}
