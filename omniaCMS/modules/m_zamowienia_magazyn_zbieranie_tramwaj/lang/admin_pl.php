<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-10-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */
include_once('modules/m_zamowienia/lang/admin_pl.php');
//do filtrów
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj_tramwaj_list_history']['get_list'] = "Zbieranie - Tablet";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj_tramwaj_list_history']['details'] = "Zbieranie - PDF";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj_tramwaj_list_history']['dump_data'] = "Szczegóły listy";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj_tramwaj_list_history']['activate'] = "Zmień status listy na kompletowana";