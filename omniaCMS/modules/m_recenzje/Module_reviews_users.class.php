<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 29.11.17
 * Time: 13:19
 */

namespace omniaCMS\modules\m_recenzje;


use Admin;
use LIB\Helpers\ArrayHelper;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use View;

class Module__recenzje__reviews_users implements ModuleEntity, SingleView
{
    protected $sMsg;

    /**
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    public function __construct(Admin $oClassRepository) {

        $this->oClassRepository = $oClassRepository;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule;
    }


    public function getMsg()
    {
        return $this->sMsg;
    }

    public function doDefault()
    {
        return $this->doShow();
    }

    public function doShow()
    {
        global $aConfig;
        $aData = array();

        if (!empty($_POST)) {
            $aData =& $_POST;
        }
        if ($_GET['ppid'] > 0) {
            $iId = $_GET['ppid'];
        }


        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header' => sprintf(_('Recenzenci')),
            'refresh' => true,
            'search' => true,
            'items_per_page' => 20,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 2,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'email',
                'content' => _('Email'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'regulations_agreement',
                'content' => _('Zgoda na przetważanie danych osobowych'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'promotions_agreement',
                'content' => _('Zgoda na promocje'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'reviews_count',
                'content' => _('Liczba recenzji'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'vote_sum',
                'content' => _('Suma łapewk pod recenzjami (od łapek w górę odjęte są łapki w dół)'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '300'
            ),
        );

        $aColSettings = array(
            'id' => array(
                'show' => false
            ),
            'action' => array(
            )
        );


        include_once('View/View.class.php');
        $oView = new View(_('products_reviews_users'), $aHeader, $aAttribs);

        $aCols = array(
            'PRU.id',
            'PRU.email',
            'PRU.regulations_agreement',
            'PRU.promotions_agreement',
            'PRU.reviews_count',
            'PRU.vote_sum',
            'PRU.email',
            'PRU.created'
        );

        $sSql = 'SELECT %cols
             FROM products_reviews_users AS PRU
             WHERE
             1=1 
              %filters
             ';
        $sGroupBySQL = 'GROUP BY PRU.id';
        $aSearchCols = array('PRU.email');
        $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, '', $sGroupBySQL, ' PRU.id DESC ');
        $oView->AddRecordsHeader($aRecordsHeader);
        $oView->setActionColSettings($aColSettings);

        $aRecordsFooterParams = array(
            [],
            'export_csv' => array(),
        );
        $aRecordsFooter = array([], array('export_csv'));
        $oView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
        return ShowTable($oView->Show());
    }

    public function doExport_csv() {
        $aRecordsHeader = array(
            array(
                'db_field' => 'email',
                'content' => _('Email'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'regulations_agreement',
                'content' => _('Zgoda na przetważanie danych osobowych'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'promotions_agreement',
                'content' => _('Zgoda na promocje'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'reviews_count',
                'content' => _('Liczba opinii'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'vote_sum',
                'content' => _('Suma łapewk pod recenzjami (od łapek w górę odjęte są łapki w dół)'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '300'
            ),
        );

        $sSql = 'SELECT email as "Email", 
                        regulations_agreement as "Zgoda na przetważanie danych osobowych",
                        promotions_agreement as "Zgoda na promocje",
                        reviews_count as "Liczba opinii",
                        vote_sum as "Suma łapewk pod recenzjami (od łapek w górę odjęte są łapki w dół)",
                        created as "Utworzono"
                        
             FROM products_reviews_users AS PRU
             ';
        $data = $this->pDbMgr->GetAll('profit24', $sSql);
        if (!empty($data)) {
            $_GET['hideHeader'] = '1';
            array_unshift($data, array_keys($data[0]));
            $this->array_to_csv_download($data, 'users-'.date('YmdHis').'_.csv');
            die;
        } else {
            $this->sMsg .= _('Brak danych');
            return $this->doDefault();
        }
    }

    /**
     * @param $array
     * @param string $filename
     * @param string $delimiter
     */
    private function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w');
        // loop over the input array
        foreach ($array as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
    }

    public function parseViewItem(array $aItem)
    {
        return $aItem;
    }

    public function resetViewState()
    {
        $this->resetViewState();
    }
}