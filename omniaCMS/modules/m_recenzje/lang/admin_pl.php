<?php
$aConfig['lang']['m_recenzje_reviews_users']['export_csv'] = 'Eksportuj CSV';



$aConfig['lang']['m_recenzje_reviews_list']['check_all'] = 'Zaznacz wszystkie wszystkie';
$aConfig['lang']['m_recenzje_reviews_list']['delete_all'] = 'Usuń wszystkie';

$aConfig['lang']['m_recenzje_reviews_list']['delete'] = "Usuń recenzję";
$aConfig['lang']['m_recenzje_reviews_list']['delete_q'] = "Czy na pewno usunąć wybraną recenzję?";

$aConfig['lang']['m_recenzje_reviews_list']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_recenzje_reviews_list']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_recenzje_reviews_list']['delete_all_q'] = "Czy na pewno usunąć wybrane elementy?";
$aConfig['lang']['m_recenzje_reviews_list']['delete_all_err'] = "Nie wybrano żadnego elementu do usunięcia!";

$aConfig['lang']['m_recenzje_reviews_list']['clear'] = "Wyczyść konkurs";