<?php
namespace omniaCMS\modules\m_recenzje;

use Admin;
use Common;
use Exception;
use FormTable;
use LIB\EntityManager\Entites\Containers;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\EntityManager\Entites\StockLocation;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use omniaCMS\lib\interfaces\ModuleEntity;
use ProductReview\ProductReview;
use View;

class Module__recenzje__reviews_list implements ModuleEntity {

    protected $sMsg;
    /**
     * @var Smarty
     */
    private $pSmarty;
    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;
    private $containers;

    /**
     * Module__recenzje__reviews_list constructor.
     * @param Admin $oClassRepository
     */
    public function __construct(Admin $oClassRepository) {

        $this->oClassRepository = $oClassRepository;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule;
        $this->containers = new Containers();
    }

    /**
     * @return string
     */
    public function doUpdate()
    {
        $reviewId = $_POST['review_id'];

        $acceptedValue = 'NULL';
        $acceptedBy = 'NULL';

        if (isset($_POST['active'])) {
            $acceptedValue = 'NOW()';
            $acceptedBy = $_SESSION['user']['name'];
        }
        $row = $this->pDbMgr->getTableRow('products_reviews', $reviewId, [' * ']);

        $websiteCode = $this->pDbMgr->GetOne('profit24', 'SELECT code FROM websites WHERE id = '.$row['website_id']);


        $this->pDbMgr->BeginTransaction('profit24');
        $this->pDbMgr->BeginTransaction($websiteCode);
        try {

            $values = [
                'active' => isset($_POST['active']) ? 1 : 0,
                'review' => $_POST['review'],
                'accepted' => $acceptedValue,
                'accepted_by' => $acceptedBy,
            ];

            if (isset($_POST['contest'])) {
                $values['contest_by'] = $_SESSION['user']['name'];
            } else {
                $values['contest_by'] = 'NULL';
            }

            if (false === $this->pDbMgr->Update('profit24', 'products_reviews', $values, "id = $reviewId")) {
                throw new Exception('Wystapil blad podczas zapisu 1');
            }

            $this->recountAverage($websiteCode, $row['product_id'], $row['website_id']);


            $productReview = new ProductReview($this->pDbMgr);
            $productReview->recountProductsReviewsUser($row['products_reviews_users_id']);

            $this->pDbMgr->CommitTransaction('profit24');
            $this->pDbMgr->CommitTransaction($websiteCode);
            $this->sMsg = GetMessage('Zmiany zostały zapisane', false);
        } catch (\Exception $ex) {
            $this->sMsg = GetMessage($ex->getMessage());

            $this->pDbMgr->RollbackTransaction('profit24');
            $this->pDbMgr->RollbackTransaction($websiteCode);
            return $this->doEdit($reviewId);
        }


        return $this->doDefault();
    }

    /**
     * @param $productId
     * @param $websiteId
     */
    private function recountAverage($websiteCode, $productId, $websiteId)
    {
        $sSql = 'SELECT id FROM products WHERE profit24_id = '.$productId;
        $websiteProductId = $this->pDbMgr->GetOne($websiteCode, $sSql);

        $existAverage = $this->pDbMgr->GetRow($websiteCode, "SELECT * FROM products_rating_avg WHERE product_id = $websiteProductId");

        $avg = $this->pDbMgr->GetOne('profit24', "
SELECT AVG(PR.book_rating) 
FROM products_rating AS PR 
JOIN products_reviews AS PRE
  ON PRE.id = PR.products_reviews_id AND PRE.active = '1'
WHERE PR.product_id = $productId AND 
      PR.website_id = $websiteId ");

        if (null === $avg || false === $avg || 0 == $avg) {
            return;
        }

        if (empty($existAverage)) {
            $values = [
                'product_id' => $websiteProductId,
                'average' => $avg
            ];

            if (false === $this->pDbMgr->Insert($websiteCode, 'products_rating_avg', $values)) {
                throw new Exception('Wystapil blad podczas zapisu 2');
            }
        } else {
            if (false === $this->pDbMgr->Update($websiteCode, 'products_rating_avg', ['average' => $avg], "id = ".$existAverage['id'])) {
                throw new Exception('Wystapil blad podczas zapisu 3');
            }
        }
    }

    /**
     * @return string
     */
    public function doEdit()
    {
        if (isset($_POST['review_id'])) {
            $id = $_POST['review_id'];
        } elseif ($_GET['id']) {
            $id = $_GET['id'];
        }
        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        $sql = "
        SELECT PR.id, PR.review, PR.website_id, PR.active, (ROUND(PRR.book_rating, 1)) as book_rating, PR.accepted, PR.accepted_by, PR.contest_by
						 	 FROM products_reviews AS PR
						 	 LEFT JOIN products_rating AS PRR On PRR.products_reviews_id = PR.id
						 	 WHERE PR.id = $id
						 	 ";

        $row = $this->pDbMgr->GetRow('profit24', $sql);

        $sHeader = _('Edycja recenzji');
        $pForm = new FormTable('review_edit', $sHeader, array('action'=>phpSelf()), array('col_width'=>420), true);
        $pForm->AddWYSIWYG('review', _('Recenzja'), $row['review']);
        $pForm->AddCheckBox('active', _('Aktywna'), "", false, (bool)$row['active'], false);
        $pForm->AddCheckBox('contest', _('Konkurs'), "", false, ((bool)$row['contest_by'] != '' ? true : false), false);
//        $pForm->AddCheckBox('accepted', _('Zaakceptowana'), "", false, empty($row['accepted']) ? false : true, false);
        $pForm->AddMergedRow('<label for="">Ocena&nbsp;<span id="_label" class="gwiazdka" style="display: inline;"></span></label><td>'.$row['book_rating'].'</td>', array('colspan'=>'0'));
        $pForm->AddInputButton('save', _('Zapisz'));
        $pForm->AddHidden('do', 'update');
        $pForm->AddHidden('review_id', $row['id']);
        return $pForm->ShowForm();
    }


    /**
     * @return string
     */
    private function doShow()
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header'	=> _('Lista recenzji'),
            'refresh'	=> true,
            'search'	=> true
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 0,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'content'	=> '&nbsp;',
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'review',
                'content'	=> _('Treść'),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'product',
                'content'	=> _('Produkt'),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'ean_13',
                'content'	=> _('EAN 13'),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'alias',
                'content'	=> _('Nick'),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'email',
                'content'	=> _('Email'),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'website_id',
                'content'	=> _('Sklep'),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'active',
                'content'	=> _('Aktywna'),
                'sortable'	=> false
            ),array(
                'db_field'	=> 'book_rating',
                'content'	=> _('Ocena'),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'PR.contest_by',
                'content'	=> _('KONKURS - dodany przez'),
                'sortable'	=> true
            ),
            array(
                'db_field'	=> 'str_len',
                'content'	=> _('Ilość znaków'),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'PR.created',
                'content'	=> _('Dodano'),
                'sortable'	=> true
            ),
            array(
                'db_field'	=> 'PR.accepted',
                'content'	=> _('Zaakceptowana'),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'accepted_by',
                'content'	=> _('Zaakceptowana przez'),
                'sortable'	=> false
            ),
            array(
                'content'	=> $aConfig['lang']['common']['action'],
                'sortable'	=> false,
                'width'	=> '40'
            )
        );

//        // pobranie liczby wszystkich rekordow
//        $sSql = "SELECT COUNT(PR.id)
//						 	 FROM products_reviews AS PR
//						 	 JOIN websites AS W On PR.website_id = W.id";
//        $iRowCount = intval(Common::GetOne($sSql));

        $sSql = "SELECT COUNT(DISTINCT PR.id)
						 	 FROM products_reviews AS PR
						 	 JOIN websites AS W On PR.website_id = W.id
						 	 JOIN products AS P On P.id = PR.product_id
						 	 LEFT JOIN products_rating AS PRR On PRR.products_reviews_id = PR.id
							 WHERE 1=1 ".
            (isset($_POST['search']) && !empty($_POST['search']) ? ' AND P.name LIKE \'%'.$_POST['search'].'%\'' : '').
            (isset($_POST['f_active']) && $_POST['f_active'] != '' ? " AND PR.active = '" . $_POST['f_active'] . "'" : '') .
            (isset($_POST['f_contest']) && $_POST['f_contest'] != '' ? ' AND PR.contest_by != "" ': '') .
            ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' PR.id DESC ');
        $iRowCount = intval(Common::GetOne($sSql));

        $pView = new View('transport_means', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);

        $aActives = [
            [
                'value' => _('1'),
                'label' => _('Tak')
            ],
            [
                'value' => _('0'),
                'label' => _('Nie')
            ]
        ];
        $pView->AddFilter('f_active', _('Aktywna'), addDefaultValue($aActives), $_POST['f_active']);


        $aActives = [
            [
                'value' => _('1'),
                'label' => _('Tak')
            ]
        ];
        $pView->AddFilter('f_contest', _('Konkurs'), addDefaultValue($aActives), $_POST['f_contest']);


        if ($iRowCount > 0) {
            // dodanie Pagera do widoku
            $iCurrentPage = $pView->AddPager($iRowCount);
            $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
            $iStartFrom = ($iCurrentPage - 1) * $iPerPage;

            // pobranie wszystkich rekordow
            $sSql = "SELECT PR.id, PR.review, P.name as product, P.ean_13, PR.alias, IF(PRU.email = '', UA.email, PRU.email) as email, W.code, PR.active, (ROUND(PRR.book_rating, 1)) as book_rating, PR.contest_by, char_length(review) AS str_len, PR.created, PR.accepted, PR.accepted_by,
                              W.url, P.id AS P_ID
						 	 FROM products_reviews AS PR
						 	 JOIN websites AS W On PR.website_id = W.id
						 	 JOIN products AS P On P.id = PR.product_id
						 	 JOIN products_reviews_users AS PRU
						 	  ON PR.products_reviews_users_id = PRU.id
						 	 LEFT JOIN users_accounts AS UA
						 	  ON PRU.users_accounts_id = UA.id
						 	 LEFT JOIN products_rating AS PRR On PRR.products_reviews_id = PR.id
							 WHERE 1=1 ".
                (isset($_POST['search']) && !empty($_POST['search']) ? ' AND P.name LIKE \'%'.$_POST['search'].'%\'' : '').
                (isset($_POST['f_active']) && $_POST['f_active'] != '' ? " AND PR.active = '" . $_POST['f_active'] . "'" : '') .
                (isset($_POST['f_contest']) && $_POST['f_contest'] != '' ? ' AND PR.contest_by != "" ': '') .
                ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' PR.id ').
                (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : ' DESC ').
                " LIMIT ".$iStartFrom.", ".$iPerPage;

            $aRecords =& Common::GetAll($sSql);

            foreach($aRecords as &$record) {

                $record['review'] = trimString($record['review'], 100);
                $record['active'] = $record['active'] == 0 ? _('Nie') : _('Tak');

                $serviceProductId = $this->pDbMgr->GetOne($record['code'], 'SELECT id FROM products WHERE profit24_id = '.$record['P_ID']);

                $record['ean_13'] = '<a href="'.$record['url'].'/a,product'.$serviceProductId.'.html" target="_blank">'.$record['ean_13'].'</a>';

                unset($record['P_ID']);
                unset($record['url']);
            }


            // ustawienia dla poszczegolnych kolumn rekordu
            $aColSettings = array(
                'id'	=> array(
                    'show'	=> false
                ),
                'name' => array (
                    'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
                ),
                'action' => array (
                    'actions'	=> array ('edit', 'delete'),
                    'params' => array (
                        'edit'	=> array('id'=>'{id}'),
                        'delete'	=> array('id'=>'{id}')
                    )
                )
            );

            // dodanie rekordow do widoku
            $pView->AddRecords($aRecords, $aColSettings);
        }
        // przyciski stopki stopki do widoku
        $aRecordsFooter = array(
            array('check_all', 'delete_all'),
            array('clear')
        );

        $pView->AddRecordsFooter($aRecordsFooter);

        return $pView->Show();
    }


    /**
     *
     */
    public function doClear() {
        global $aConfig;

        $values = [
            'contest_by' => 'NULL'
        ];
        if (false === $this->pDbMgr->Update('profit24', 'products_reviews', $values, ' id > 0')) {
            $sMsg = _('Wystąpił błąd podczas czyszczenia konkursów ');
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
        } else {
            $sMsg = _('Wyczyszczono konkursy');
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
        }
        return $this->doDefault();
    }



    /**
     * Metoda usuwa wybrane akapity
     *
     * @param	object	$pSmarty
     * @param	integer	$iId	- Id usuwanego akapitu
     * @return 	void
     */
    function doDelete() {
        global $aConfig;
        $bIsErr = false;
        $aTmp = array();

        if (isset($_GET['id']) && $_GET['id'] > 0) {
            $iId = $_GET['id'];
        }


        if ($iId > 0) {
            $_POST['delete'][$iId] = '1';
        }
        $iI = 0;
        foreach($_POST['delete'] as $sKey => $sVal) {
            $aTmp[$iI++] = $sKey;
        }
        $_POST['delete'] =& $aTmp;

        if (!empty($_POST['delete'])) {
            if (!$bIsErr) {
                // usuwanie akapitow
                $sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_reviews
								 WHERE id IN (".implode(',', $_POST['delete']).")";
                if (Common::Query($sSql) === false) {
                    $bIsErr = true;
                }
            }
            if (!$bIsErr) {
                $sMsg = _('Usunięto recenzje o id: '."(".implode(',', $_POST['delete']).")");
                $this->sMsg = GetMessage($sMsg, false);
                // dodanie informacji do logow
                AddLog($sMsg, false);
            }
            else {
                $sMsg = _('Wystąpił błąd podczas usuwania recenzji o id: '."(".implode(',', $_POST['delete']).")");
                $this->sMsg = GetMessage($sMsg);
                // dodanie informacji do logow
                AddLog($sMsg);
            }
        }
        return $this->doShow();
    } // end of Delete() funciton

    /**
     * @return mixed
     */
    public function getMsg()
    {
        return $this->sMsg;
    }

    /**
     * @return string
     */
    public function doDefault()
    {
        return $this->doShow();
    }
}