<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia - historia wysłanych emaili z zamówieniami'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 *
 */
require_once('Common_get_ready_orders_books.class.php');

/**
 * Klasa pobiera listę produktów gotowych do zebrania i wysłania
 * 
 */
class Module__zamowienia_magazyn_zbieranie__list_history extends Common_get_ready_orders_books {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   *
   * @var \DatabaseManager
   */
  var $pDbMgr;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function __construct(&$pSmarty, $bInit) {
		global $aConfig, $pDbMgr;
    $this->pDbMgr = $pDbMgr;
    
		$this->sErrMsg = '';
		$this->iLangId =& $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];


		$sDo = '';
		$iId = 0;
		$iPId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    parent::__construct($bInit);

    switch ($sDo) {
			case 'get': 
        $this->GetPDFFile($iId); 
      break;
    
      case 'save_list_data':
        $this->saveListData($pSmarty);
      break;
    
      case 'get_list':
        $this->GetList($pSmarty, $iId);
      break;
    
			case 'dump_data': 
        $this->GetDumpData($iId); 
      break;
    
      case 'open_list': 
        $this->openList($pSmarty, $iId); 
      break;
    
			default: $this->Show($pSmarty); break;
		}
	} // end Module() function
  
  /**
   * 
   * @param \Smarty $pSmarty
   * @param int $iId
   */
  public function openList($pSmarty, $iId) {
    
    $aValues = array('closed' => '0');
    if ($this->pDbMgr->Update('profit24', 'orders_items_lists', $aValues, ' id = '.$iId) === false) {
      $sMsg = sprintf(_('Wystąpił błąd podczas zmiany statusu listy %s na kompletowana'), $iId);
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
    } else {
      $sMsg = sprintf(_('Status listy %s został zmieniony na kompletowana'), $iId);
      AddLog($sMsg, false);
      $this->sMsg = GetMessage($sMsg, false);
    }
    $this->Show($pSmarty);
  }
  
  
  /**
   * 
   * @param \Smarty $pSmarty
   * @param int $iILId
   * @return void
   */
  protected  function GetList($pSmarty, $iILId) {
    
    $sSql = 'SELECT package_number FROM orders_items_lists WHERE id = '.$iILId;
    $sPackageNumber = $this->pDbMgr->GetOne('profit24', $sSql);
    
    $sSql = "SELECT orders_id 
             FROM orders_items_lists_items
             WHERE orders_items_lists_id = ".$iILId;
    $aItems = Common::GetCol($sSql);
    foreach ($aItems as $iItemId) {
      $this->aOrdersAccepted[$iItemId] = '1';
    }
    $aList = $this->getRecordsListLimited(0, null, 0, 0, '', TRUE, $this->aOrdersAccepted, true);
    
    usort($aList, 'sortItems');
    
    if (!empty($aList) && !empty($sPackageNumber)) {
      $this->getProductsScanner($pSmarty, $iILId, $aList, $sPackageNumber);
    }
    
    return ;
  }
  
  /**
   * Metoda wyswietla wszystkie dane odnośnieie tej listy
   * 
   * @param type $iId
   */
  private function GetDumpData($iId) {
    
    echo '<h1>Zamówienia w liście:</h1> <br />';
    $sSql = "SELECT DISTINCT order_number, O.transport_number
             FROM orders_items_lists_items AS OILI
             JOIN orders AS O
              ON OILI.orders_id = O.id
             WHERE orders_items_lists_id = ".$iId;
    dump($this->pDbMgr->GetAssoc('profit24', $sSql));
    
    echo '<h1>Dane listy:</h1> <br />';
    $sSql = "SELECT * FROM orders_items_lists WHERE id = ".$iId;
    $aList = $this->pDbMgr->GetRow('profit24', $sSql);
    dump($aList);
    
    $aData = unserialize(base64_decode($aList['debug']));
    dump($aData);
    
    echo '<h1>Składowe listy:</h1> <br />';
    $sSql = "SELECT * FROM orders_items_lists_items WHERE orders_items_lists_id = ".$iId;
    dump($this->pDbMgr->GetAll('profit24', $sSql));
    
    echo '<h1>Składowe zamówień z listy:</h1> <br />';
    $sSql = "SELECT OI.*
             FROM orders_items_lists_items AS OILI
             JOIN orders_items AS OI
              ON OILI.orders_items_id = OI.id
             WHERE orders_items_lists_id = ".$iId;
    dump($this->pDbMgr->GetAll('profit24', $sSql));
  }// end of GetDumpData() method
  
  
  /**
	 * Generuje liste pozycji w formacie pdf do pobrania
	 * @return unknown_type
	 */
	function GetPDFFile($iId) {
		global $aConfig,$pSmarty;
    
    include_once('Module_get_ready_to_send.class.php');
    $oRS = new Module__zamowienia_magazyn_zbieranie__get_ready_to_send($pSmarty, false);
    
    $sSql = "SELECT orders_id 
             FROM orders_items_lists_items
             WHERE orders_items_lists_id = ".$iId;
    $aItems = Common::GetCol($sSql);
    foreach ($aItems as $iItemId) {
      $this->aOrdersAccepted[$iItemId] = '1';
    }
    
    $aList = $this->getRecordsListLimited(0, NULL, 0, 0, '', true, $this->aOrdersAccepted, true);
    
    usort($aList, 'sortItems');
    $_GET['hideHeader'] = '1';
      include_once 'barcode/barcode.php';

      $sCode = str_pad($iId, 13, '0', STR_PAD_LEFT);


      $im     = imagecreatetruecolor(300, 100);  
      $black  = ImageColorAllocate($im,0x00,0x00,0x00);  
      $white  = ImageColorAllocate($im,0xff,0xff,0xff);  
      imagefilledrectangle($im, 0, 0, 300, 100, $white);  
      $data = Barcode::gd($im, $black, 150, 50, 0, "code128", $sCode, 2, 50);  
      $iRefCountMagazine = 0;
      $iRefCountTramwaj = 0;
      $oRS->countQuantityItems($aList, $iRefCountMagazine, $iRefCountTramwaj);
      $aViewList = $oRS->sumByArray($aList, 'quantity', 'pack_number');
      
      $aBooks = array();
      $aBooks['number'] = $sCode;
      $aBooks['items'] = $aViewList; 
      $aBooks['author'] = $_SESSION['user']['author']; 
      $aBooks['count_magazine'] = $iRefCountMagazine;
      $aBooks['count_tramwaj'] = $iRefCountTramwaj;
      $aBooks = $this->parseBooksToListSelf($aBooks);
      
      $pSmarty->assign_by_ref('aBooks', $aBooks);
      $sHtml = $pSmarty->fetch('PDF/pdf_ready_list.tpl');
      $pSmarty->clear_assign('aBooks');

      // kod kreskowy
      ob_start();
      imagepng($im);
      imagedestroy( $im );
      $imagedata = ob_get_clean();


      $oRS->getPDF($sHtml, $imagedata, $aBooks);

//echo($sHtml); die(); 
	} // end of GetPDF() funciton
  
  
  /**
   * Parsuje listę produktów
   * 
   * @param array $aBooks
   * @return array
   */
  public function parseBooksToListSelf($aBooks) {
    
    foreach ($aBooks['items'] as $sKey => $aBook) {
      $aBooks['items'][$sKey]['publisher'] = 	$aBooks['items'][$sKey]['publisher']==''	? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$sKey]['publisher']),0,25,'UTF-8');
			$aBooks['items'][$sKey]['authors'] = 		$aBooks['items'][$sKey]['authors']==''		? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$sKey]['authors']),0,25,'UTF-8');
			$aBooks['items'][$sKey]['name'] = 			$aBooks['items'][$sKey]['name']==''				? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$sKey]['name']),0,80,'UTF-8');
    }
    return $aBooks;
  }// end of parseBooksToListSelf() method
  
  
  /**
   * Metoda pobiera rekordy na listę ograniczone co do ilości
   * 
   * @param int $iLimitRecords
   * @return array

  public function getRecordsListLimited($iLimitRecords = 0, $aOrderItems = array()) {
    
    $sSql = $this->getQuery(' O.order_number, OI.id, OI.order_id, OI.quantity,
                              OI.name, OI.publisher, OI.authors, OI.publication_year, OI.edition, OI.product_id, 
                              OI.item_type,
                              P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13, IF(OI.source = 51, "1", "0") AS is_g,
                               
                              
(
SELECT OSH.pack_number
FROM orders_send_history AS OSH
JOIN orders_send_history_items AS OSHI
  ON OSH.id = OSHI.send_history_id
WHERE OSHI.item_id = OI.id 
ORDER BY OSH.id DESC
LIMIT 1
      ) AS pack_number,


                              (SELECT id FROM orders_items WHERE parent_id = OI.id AND item_type = "A") AS is_cd,
                              (SELECT profit_g_act_stock FROM products_stock AS PS WHERE PS.id = OI.product_id) as profit_g_act_stock
                              ');
    
    if (!empty($aOrderItems)) {
      $sSql .= " AND O.id IN (".implode(', ', array_keys($aOrderItems)).") ";
    }
    
    $sSql .= " 
      GROUP BY OI.id     
      ORDER BY O.send_date DESC, O.id DESC
      ". ($iLimitRecords > 0 ? " LIMIT ".$iLimitRecords : '');
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getRecordsListLimited() method
  */
  
  /**
   * Metoda zwraca podstawowe zamówienie wybierania zamówień 
   * 
   * @param string $sSelect
   * @return string
   */
  public function getQuery($sSelect) {
    
    /*
     * 1) opłacone, lub odroczony termin płatności,
     * 2) nie "nowe", "zatwierdzone", "zrealizowane", "anulowane"
     * 3) jeśli z G to status musi być "jest nie G"
     * 4) jeśli zewn. to status musi być "jest pot." 
     * 5) data wysyłki jeśli została zmieniona to ma być to dziś
     * 6) nie pobrane wcześniej,
     */
    $sSql = "
SELECT ".$sSelect."
FROM orders_items AS OI
    JOIN orders AS O
    ON OI.order_id = O.id
        /*AND IF (OI.source = '51', (OI.status = '3' OR OI.status = '4'), OI.status = '4')*/
        /*AND OI.get_ready_list = '0'*/
        AND OI.deleted = '0'
        AND (OI.item_type = 'I' OR OI.item_type = 'P')
        AND OI.packet = '0'

    LEFT JOIN products AS P
      ON P.id = OI.product_id
    LEFT JOIN products_stock AS PS2
      ON PS2.id = OI.product_id
    WHERE 
      1=1 
    ";
    return $sSql;
  }// end of getQuery() method
  

	/**
	 * Metoda wyswietla liste zdefiniowanych kodow Premium SMS
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		$sFilterSql = '';
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		
		// zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> _('Historia list ZT/ZM'),
			'refresh'	=> true,
			'search'	=> true,
			'checkboxes'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'id',
				'content'	=> _('Numer dokumentu'),
				'sortable'	=> true,
				'width' => '200'
			),
      array(
				'db_field'	=> 'package_number',
				'content'	=> _('Numer kuwety / НОМЕР ЯЩИКА'),
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'type',
				'content'	=> _('Typ'),
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'transport',
				'content'	=> _('Spedytor'),
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'closed',
				'content'	=> _('Status'),
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'created',
				'content'	=> _('Pobrano listę do zebrania'),
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> _('Utworzona/Posortowane przez'),
				'sortable'	=> true,
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
		
		
		// pobranie liczby wszystkich kodow
		$sSql = "SELECT COUNT(id) 
						 FROM ".$aConfig['tabls']['prefix']."orders_items_lists
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND (id LIKE \'%'.$_POST['search'].'%\' OR package_number LIKE \'%'.$_POST['search'].'%\')' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(id) 
							 FROM ".$aConfig['tabls']['prefix']."orders_items_lists
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND (id LIKE \'%'.$_POST['search'].'%\' OR package_number LIKE \'%'.$_POST['search'].'%\')' : '');
			$iRowCount = intval(Common::GetOne($sSql));
		}
    
    if ($_SESSION['user']['priv_order_status'] == '1' || $_SESSION['user']['type'] == '1') {
      $bPrivilagesRemoveReservations = true;
    } else {
      $bPrivilagesRemoveReservations = false;
    }
    
		$pView = new View('sort_history', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			// pobranie zamówień bankowych
			$sSql = "SELECT OIL.id, OIL.package_number, OIL.type, OTM.name AS transport, OIL.closed, OIL.created, (SELECT CONCAT(U.name, ' ', U.surname) FROM users AS U WHERE OIL.id = OIL.user_id) as created_by
							 FROM ".$aConfig['tabls']['prefix']."orders_items_lists AS OIL
               LEFT JOIN orders_transport_means AS OTM
                ON OIL.transport_id = OTM.id
							 WHERE 1 = 1 " .
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND (id LIKE \'%'.$_POST['search'].'%\' OR OIL.package_number LIKE \'%'.$_POST['search'].'%\')' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' OIL.id DESC ').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
      foreach ($aRecords as $iKey => $aRecord) {
        switch ($aRecord['closed']) {
          case '0':
            $aRecords[$iKey]['closed'] = _('Kompletowana');
            $aRecords[$iKey]['disabled'][] = 'activate';
          break;
          case '1': 
            $aRecords[$iKey]['closed'] = _('Zatwierdzona');
            $aRecords[$iKey]['disabled'][] = 'get_list';
          break;
          case '2':
            $aRecords[$iKey]['closed'] = _('Sortowana');
            $aRecords[$iKey]['disabled'][] = 'get_list';
          break;
          case '3':
            $aRecords[$iKey]['closed'] = _('Oczekuje na list przewozowy - wstrzymana');
            $aRecords[$iKey]['disabled'][] = 'get_list';
          break;
        }
        if ($bPrivilagesRemoveReservations === false) {
          $aRecords[$iKey]['disabled'][] = 'activate';
        }
        
        switch ($aRecord['type']) {
          case '0':
            $aRecords[$iKey]['type'] = _('Zwykła');
          break;
          case '1': 
            $aRecords[$iKey]['type'] = _('Single');
          break;
          case '2':
            $aRecords[$iKey]['type'] = _('Połączone (wspólna wysyłka)');
          break;
        }
      }
			$aColSettings = array(
				'action' => array (
					'actions'	=> array ('get_list', 'details', 'dump_data', 'activate'),
					'params' => array (
												'details'	=> array('id' => '{id}', 'do' => 'get'),
                        'dump_data'	=> array('id' => '{id}', 'do' => 'dump_data'),
                        'get_list'	=> array('id' => '{id}', 'do' => 'get_list'),
                        'activate' => array('id' => '{id}', 'do' => 'open_list')
											),
					'show' => false,
            'icon' => array(
                'dump_data' => 'comments',
                'get_list' => 'books'
            )
				) 
 			);
      
      foreach ($aRecords as $iKey => $aItem) {
        $aRecords[$iKey]['transport'] .= $this->getAdditionalsTransport($aItem['id']);
      }
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array()
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		$sJS = '
      <script type="text/javascript">
      $( function() {
        $("a[title^=\'Zbieranie\']").attr("target", "_blank");
        $("a[title^=\'Zmień status\']").click( function() {
          var isGood=confirm("Czy na pewno chcesz zmienić status listy ?");
          if (isGood) {
            return true;
          } else {
            return false;
          }
        });
      });
      </script>';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show().$sJS);
	} // end of Show() function
  
  
  /**
   * 
   * @param int $iOLId
   * @return string
   */
  private function getAdditionalsTransport($iOLId) {
    
    $sSql = 'SELECT GROUP_CONCAT(OTM2.name SEPARATOR ", <br />") AS transport
               FROM orders_items_lists_tranport AS OILT
               LEFT JOIN orders_transport_means AS OTM2
                ON OILT.orders_transport_means_id = OTM2.id 
               WHERE 
                OILT.orders_items_lists_id = '.$iOLId;
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
} // end of Module Class
