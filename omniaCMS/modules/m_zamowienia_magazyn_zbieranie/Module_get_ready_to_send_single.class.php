<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-06-24 
 * @copyrights Marcin Chudy - Profit24.pl
 */

require_once('Common_get_ready_orders_books.class.php');

/**
 * Klasa pobiera listę produktów gotowych do zebrania i wysłania
 * 
 */
class Module__zamowienia_magazyn_zbieranie__get_ready_to_send_single extends Common_get_ready_orders_books {

  // komunikat
	public $sMsg;

	// nazwa modulu - do langow
	public $sModule;

	// id wersji jezykowej
	public $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	protected $aPrivileges;
	
	// id strony modulu
	protected $iPageId;
  
  // id uzytkownika
  protected $iUId;
  
	// id listy zamowien
	protected $iPPId;
  
  // id zamowienia
  protected $iId;
  
  protected $_aTransports;

  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  protected $pDbMgr;
  
  protected $aOrdersAccepted = array();
  protected $aOrdersWaiting = array();
  
  public $sGetListSQL;
  
  function __construct(&$pSmarty, $bInit = true) {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
    
		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];

		if (isset($_GET['pid'])) {
			$iId = $_GET['pid'];
		}
    
		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
    
    $this->_aTransports = $this->getTransports();

		$this->iUId = $_SESSION['user']['id'];
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    if ($bInit == false) {
      return;
    }
    parent::__construct($bInit);
    
    switch ($sDo) {
      case 'generate':
        $this->getList($pSmarty);
      break;
    
      case 'get_count_orders':
        $this->getCountOrders($pSmarty);
      break;
      
      case 'save_list_data':
        $this->saveListData($pSmarty);
      break;
    
      case 'do':
      default:
        $this->Show($pSmarty);
      break;
    }
  }// end of __construct()
  
  
  protected function saveListData($pSmarty) {
    parent::saveListData($pSmarty);
  }
  
  
  /**
   * Metoda pobiera listę produktów do pobrania
   * 
   * @param object $pSmarty
   * @return void
   */
  protected function getList(&$pSmarty) {

    $bIsErr = false;
    
    $iOrdersLimit = 40;// ustalono max 40
    $aList = $this->getLimitedOrders($iOrdersLimit, $_POST['transport_id'], $_POST['max_order_item'], $_POST['min_order_item'], $_POST['shipment_date_expected']);
    $sGetListSQL = $this->sGetListSQL;
    $aList = $this->getRecordsListLimited(0, $_POST['transport_id'], $_POST['max_order_item'], $_POST['min_order_item'], $_POST['shipment_date_expected'], true, $this->aOrdersAccepted, false);
    $cType = $_POST['type'];
    $sPackageNumber = $_POST['package_number'];
    $iTransportId = $_POST['transport_id'];
    
    if (!empty($aList)) {
      usort($aList, 'sortItems');

      // posortować na liście od magazynu do tramwaju w kolejności narastającej
      $this->pDbMgr->BeginTransaction('profit24');
      // zmieniamy w zam: print_ready_list = '1'
      // , w skł. zam: get_ready_list = '1'
      $aDebug['sql'] = $sGetListSQL;
      $aDebug['post'] = $_POST;
      $mReturn = $this->doCreateList($aList, $cType, TRUE, $sPackageNumber, $iTransportId, base64_encode(serialize($aDebug)));
      if ($mReturn === FALSE || $mReturn <= 0) {
        $bIsErr = $mReturn;
      } else {
        $iILId = intval($mReturn);
      }
      
      // tutaj takżę powinniśmy potwierdzić te zamówienia jak po sortowniku
      
    } else {
      $bIsErr = true;
      // błąd
      $this->pDbMgr->RollbackTransaction('profit24');
      $sMsg = _('Brak produktów do zbierania.');
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
      return;
    }
    
    if ($bIsErr === false) {
      $this->pDbMgr->CommitTransaction('profit24');
      $sReturn = $this->getProductsScanner($pSmarty, $iILId, $aList, $sPackageNumber);
      $pSmarty->assign('sContent', $sReturn);
      return true;
    } else {
      // błąd
      $this->pDbMgr->RollbackTransaction('profit24');
      switch ($bIsErr) {
        case -1:
          $sMsg = _('Podana kuweta jest zablokowana');
        break;
        case -2:
          $sMsg = _('Brak takiej kuwety');
        break;
        default:
          $sMsg = _('Wystąpił błąd podczas pobierania listy do zebrania');
        break;
      }
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
      return;
    }
  }// end of getList() method
  
  
  /**
   * Wyświetla formularz
   * 
   * @param \Smarty $pSmarty
   */
  protected function Show($pSmarty) {
    
    $aData = array();
    if (isset($_POST)) {
      $aData = $_POST;
    }
    
    /*
    // pobieramy sobie informację o tym czy, istnieją jakieś listy do wygenerowania
    $aQuantityLists = $this->getCountLists();
    
    if (!empty($aQuantityLists)) {
      $sQuantity = '<ul>';
      foreach ($aQuantityLists as $aRow) {
        $sQuantity .= '<li style="font-size: 18; font-weight: bold;">'.$aRow['name'] .': '. $aRow['count'].'</li>';
      }
      $sQuantity .= '</ul>';
    } else {
      $sQuantity = _('Brak zamówień');
    }
    */
    $aQuantityLists = $this->getCountLists(true);
    if (!empty($aQuantityLists)) {
      $sQuantity = '<ul>';
      foreach ($aQuantityLists as $aRow) {
        $sQuantity .= '<li style="font-size: 18; font-weight: bold;">'.$aRow['name'] .': '. $aRow['count'].'</li>';
      }
      $sQuantity .= '</ul>';
    } else {
      $sQuantity = _('Brak zamówień');
    }
    
    $bManageReadySend = $this->checkManageGetReadySend($_SESSION['user']);
    
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('ordered_itm', _("Zamówienia gotowe do wysłania z 1 egz. produktu - Single"), array('action'=>phpSelf(), 'target' => '_blank'), array('col_width'=>355), true);
    $pForm->AddHidden('do', 'generate');
    $pForm->AddHidden('type', '1');// oznaczenie singli
    $pForm->AddText('package_number', _('Numer kuwety / НОМЕР ЯЩИКА'), _(''), array('style' => 'font-size: 40px;'), '', 'uint', true);
    $pForm->AddHidden('max_order_item', '1');
    $pForm->AddHidden('min_order_item', '1');
      
    if ($bManageReadySend === true) {
      $pForm->AddMergedRow('Pobierz single', array('class' => 'merged'));
      
      $pForm->AddRow('Ilość zamówień gotowych do zbierania i kompletowania: ', $sQuantity);
      $pForm->AddRow('', 'UWAGA !! <br />Ilość zamówień może być niedokładna ponieważ są uwzględniane:<ol>'.
              '<li>zamówienia, które mają zostać wysłane razem i oczekują na pozostałe</li>'.
              '<li><span style="color: red">zamówienia w których istnieje nieprzeczytana uwaga do zamówienia</span></li>'.
              '</ol>');
      
      $aTransportRadio = $this->getRadioByArr($this->_aTransports);
      $aTransportRadio = $this->checkMaximumValueTransport($aQuantityLists, $aTransportRadio);
      
      // dzień do którego pobieramy zamówienia
      $pForm->AddText('shipment_date_expected', _('Do daty wysyłki'), (!isset($aData['shipment_date_expected']) ? '' : (empty($aData['shipment_date_expected']) ? '00-00-0000' : $aData['shipment_date_expected'])), array('style'=>'width: 75px;'), '', 'date', false );

      $pForm->AddCheckBoxSet('transport_id', _('Metoda transportu'), $aTransportRadio, $aData['transport_id']);

    } else {
      // ukryte pole do metody transportu
      $iMaxTransportId = $this->getMaxQuantityTransportId($aQuantityLists);
      if ($iMaxTransportId > 0) {
        $pForm->AddHidden('transport_id', $iMaxTransportId);
      }
    }
    
    $pForm->AddInputButton("generate", _('Pobierz single'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
    

    if ($bManageReadySend === true) {
      $pForm->AddInputButton("get_count_orders", _('Sprawdź ilość zamówień / ПОКАЗАТЬ КОЛИЧЕСТВО ЗАКАЗОВ'), array('onclick' => '$(\'#do\').val(\'get_count_orders\'); submit(); ', 'style' => 'font-size: 40px; font-weight: bold; color: green;'), 'button');


      $pForm->AddMergedRow('Statystyki', array('class' => 'merged'));

      $iCountInCompletation = $this->getCountInCompletation(array('1'));
      $pForm->AddRow('Ilość list w trakcie kompletowania : ', '<span style="font-size: 18; font-weight: bold;">'.$iCountInCompletation.'</span>');

      $iCountInSort = $this->getCountInSort(array('1'));
      $pForm->AddRow('Ilość list w trakcie sortowania : ', '<span style="font-size: 18; font-weight: bold;">'.$iCountInSort.'</span>');
    }
    
    $sJS = '
      <script type="text/javascript">
        $( function () {
          $("#package_number").focus();
        });
      </script>
      
      ';
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()).$sJS);
  }// end of Show() method
}// end of class