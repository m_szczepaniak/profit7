<?php
/**
 * Klasa wspólna dla pobierania zamówień i produktów gotowych do wysyłki
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-10-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

require_once('tcpdf/tcpdf.php');	

function sortItems($a, $b) {
  
  if ($a['pack_number'] != '' && $b['pack_number'] != '') {
    $packA = intval($a['pack_number']);
    $packB = intval($b['pack_number']);
    if ($packA > $packB) {
      return 1;
    } elseif ($packA < $packB) {
      return -1;
    } else {
      // ta sama półka
      return strcmp($a['name'], $b['name']);
    }
  } else if ($a['pack_number'] != '') {
    // tylko a wypełnione
    return -1;
  } else if ($b['pack_number'] != ''){
    // tylko b wypelnione
    return 1;
  } else {
    // żadne nie jest wypełnione
    return strcmp($a['name'], $b['name']);
  }
}

class MYPDF extends TCPDF {
    public $headerImage;
    public $headerText;

    //Page header
    public function Header() {
        // Logo
        //$image_file = K_PATH_IMAGES.'logo_example.jpg';
        $this->Image('@'.$this->headerImage, 129);
        // Set font
        //$this->SetFont('helvetica', 'B', 20);
        // Title
        $this->writeHTMLCell(0,0,1,1, $this->headerText, 0, 1, 0, true, '', true);
    }
}

class ____CZY_TEGO_UZYWAMY__Common_get_ready_orders_books {

  /**
   *
   * @var \DatabaseManager
   */
  
  /**
   *
   * @var \Admin
   */
  private $oParent;
  
  /**
   *
   * @var int
   */
  private $iUId;
  
  
  function __construct($oParent) {
    $this->oParent = $oParent;
    $this->iUId = $_SESSION['user']['id'];
  }
  
  /**
   * Metoda ustawia wartość pozycji które mogą zostać razem zebrane
   * 
   * @return void
   */
  protected function setCountAvailableLinkedTransport() {
    unset($this->aOrdersAccepted);
    unset($this->aOrdersWaiting);
    
    $sSql = 'SELECT DISTINCT main_order_id, "1" as key_
             FROM orders_linked_transport';
    $aMainOrderIds = $this->pDbMgr->GetAssoc('profit24', $sSql);
    array_unique($aMainOrderIds);
    
    
    $aPacketList = $this->getRecordsListLimited(NULL, NULL, NULL, NULL, '', true, $aMainOrderIds, true);
    // tablice globalne poniżej są uzupełniane
    $aList = $this->countOrders($aPacketList, 99999);
  }// end of setCountAvailableLinkedTransport() method
  
  
  /**
   * Metoda pobiera punkt odbioru paczki, jeśli wybrany zostal paczkomat
   * 
   * @param int $iOrderId
   * @return string
   */
  protected function getPointOfReceipt($iOrderId) {
   
    $sSql = 'SELECT O.point_of_receipt
               FROM orders AS O
               WHERE O.id = '.$iOrderId;
    return $this->pDbMgr->GetOne('profit24', $sSql); 
  }// end of getPaczkomatyPointOfReceipt() method
  
  
  /**
   * 
   * @param type $iOrderId
   * @return type
   */
  protected function getOrderTransportSymbol($iOrderId) {
    
    $sSql = 'SELECT OTM.symbol
               FROM orders AS O
               JOIN orders_transport_means AS OTM
                ON O.transport_id = OTM.id
               WHERE O.id = '.$iOrderId;
    return $this->pDbMgr->GetOne('profit24', $sSql); 
  }
  
  
  /**
   * Metoda sprawdza czy użytkownik może zarządzać parametrami zbieranej listy
   * 
   * @param type $aUser
   * @return boolean
   */
  protected function checkManageGetReadySend($aUser) {
    
    if ($aUser['type'] == '0') {
      $sSql = 'SELECT manage_get_ready_send FROM users WHERE id = '.$aUser['id'];
      return ($this->pDbMgr->GetOne('profit24', $sSql) == '1' ? true : false);
    } elseif ($aUser['type'] == '1' || $aUser['type'] == '2') {
      return true;
    }
  }// end of checkManageGetReadySend() method
  
  
  /**
   * Metoda pobiera unikatowe powiązane zamówienia
   * 
   * @param \Smarty $pSmarty
   * @return bool
   */
  protected function getUniqeLinkedOrders(&$pSmarty) {
    $bIsErr = false;
    
    $sPackageNumber = $_POST['package_number'];
    $iTransportId = $_POST['transport_id'];
    // tutaj zotanie ustawione $this->aOrdersAccepted
    $this->setCountAvailableLinkedTransport();

    if (!empty($this->aOrdersAccepted)) {
      while ($iOrderId = array_pop(array_keys($this->aOrdersAccepted))) {
        
        if ($iOrderId > 0) {
          $aLinkedOrders = $this->getLinkedOrders($iOrderId);
          if (!empty($aLinkedOrders)) {
            // czyślimy tablice

            unset($this->aOrdersAccepted);
            unset($this->aOrdersWaiting);
            $aPacketList = $this->getRecordsListLimited(NULL, NULL, NULL, NULL, '', true, $aLinkedOrders, true);
            $sGetListSQL = $this->sGetListSQL;
            // tablice globalne poniżej są uzupełniane
            $aList = $this->countOrders($aPacketList, 99999);
            if (!empty($aList) && !empty($sPackageNumber) && count($this->aOrdersAccepted) == count($aLinkedOrders)) {
              usort($aList, 'sortItems');
              // ok lecimy z wyświetlaniem zamówienia
              $this->pDbMgr->BeginTransaction('profit24');
              
              $aDebug['sql'] = $sGetListSQL;
              $aDebug['post'] = $_POST;
              $mReturn = $this->doCreateList($aList, '2', $sPackageNumber, $iTransportId, base64_encode(serialize($aDebug)));
              if ($mReturn === FALSE || $mReturn <= 0) {
                $bIsErr = $mReturn;
              } else {
                $iILId = intval($mReturn);
              }
              
              if ($bIsErr !== false) {
                switch ($bIsErr) {
                  case -1:
                    $sMsg = _('Wystąpił błąd podczas blokowania kuwety !');
                  break;
                
                  case -2:
                    $sMsg = _('Brak takiego kontenera !');
                  break;
                
                  default:
                  case false:
                    $sMsg = _('Wystąpił błąd podczas pobierania listy produktów do zbierania.');
                  break;
                }
                // błąd
                $this->pDbMgr->RollbackTransaction('profit24');
                AddLog($sMsg);
                $this->sMsg = GetMessage($sMsg);
                $this->Show($pSmarty);
                return false;
              } else {
                // ok pdf i commit
                $this->pDbMgr->CommitTransaction('profit24');
                $this->getProductsScanner($pSmarty, $iILId, $aList, $sPackageNumber);
                return true;
              }
            }
          }
        }
        
      }
    } else {
      $sMsg = _('Brak produktów do zebrania.');
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
      return;
    }
    
    $sMsg = _('Wystąpił błąd podczas pobierania listy produktów do zbierania 2.');
    AddLog($sMsg);
    $this->sMsg = GetMessage($sMsg);
    $this->Show($pSmarty);
    return;
  }// end of getUniqeLinkedOrders() method
  
  
  /**
   * Metoda pobiera połączone zamówienia które mają zostać razem wysłane
   * 
   * @param int $iMainOrderId
   * @return string
   */
  protected function getLinkedOrders($iMainOrderId) {
    
    $sSql = 'SELECT DISTINCT linked_order_id, "1" as key_
             FROM orders_linked_transport
             WHERE main_order_id = '.$iMainOrderId;
    $aOrderIds = $this->pDbMgr->GetAssoc('profit24', $sSql);
    $aOrderIds[$iMainOrderId] = '1';
    array_unique($aOrderIds);
    return $aOrderIds;
  }// end of getLinkedOrders() method
  
  
  /**
   * Metoda pobiera transport
   * 
   * @return array
   */
  protected function getTransports() {
    
    $sSql = 'SELECT name, id 
             FROM orders_transport_means';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getTransports() method
  
  
  /**
   * Metoda zwraca podstawowe zamówienie wybierania zamówień 
   * 
   * @param string $sSelect
   * @return string
   */
  public function getQuery($sSelect) {
    
    /*
     * 1) opłacone, lub odroczony termin płatności,
     * 2) nie "nowe", "zatwierdzone", "zrealizowane", "anulowane"
     * 3) jeśli z G to status musi być "jest nie G"
     * 4) jeśli zewn. to status musi być "jest pot." 
     * 5) data wysyłki jeśli została zmieniona to ma być to dziś
     * 6) nie pobrane wcześniej,
     */
    $sSql = "
SELECT ".$sSelect."
FROM orders_items AS OI
    JOIN orders AS O
    ON OI.order_id = O.id
        AND IF (OI.source = '51', (OI.status = '3' OR OI.status = '4'), OI.status = '4')
        AND OI.get_ready_list = '0'
        AND OI.deleted = '0'
        AND (OI.item_type = 'I' OR OI.item_type = 'P')
        AND OI.packet = '0'
    -- uwaga warunki muszą dotyczyć wszystkich składowych, a nie tylko jednej
    LEFT JOIN orders_items AS OI_TEST
    ON OI_TEST.order_id = O.id
      AND IF (OI_TEST.source = '51', OI_TEST.status <> '3' AND OI_TEST.status <> '4', OI_TEST.status <> '4')
      AND OI_TEST.get_ready_list = '0'
      AND OI_TEST.deleted = '0'
      AND (OI_TEST.item_type = 'I' OR OI_TEST.item_type = 'P')
      AND OI_TEST.packet = '0'
    LEFT JOIN products AS P
      ON P.id = OI.product_id
    LEFT JOIN products_stock AS PS2
      ON PS2.id = OI.product_id
    WHERE 
      OI_TEST.id IS NULL

      AND
      (
        (
          (
            O.payment_type = 'bank_transfer' OR
            O.payment_type = 'platnoscipl' OR
            O.payment_type = 'card'
          )
          AND O.to_pay <= O.paid_amount
        )
        OR
        (
          O.payment_type = 'postal_fee' OR
          O.payment_type = 'bank_14days'
        )
        OR
        (
          O.second_payment_type = 'postal_fee' OR
          O.second_payment_type = 'bank_14days'
        )
      )


      AND 
      (
        O.order_status = '1' OR
        O.order_status = '2' 
      )

      AND 
      ( O.send_date = '0000-00-00' OR (O.send_date <> '0000-00-00' AND O.send_date < NOW()) )
      
      /*AND OI.order_id = 131872*/
    ";
    return $sSql;
  }// end of getQuery() method
  
  
  /**
   * Metoda pobiera ilość zamówień, które czekają na pobranie
   * 
   * @return type
   */
  protected function getCountLists($bSingle = false) {
    
    $sSql = $this->getQuery(' O.transport_id, count(DISTINCT O.id) as count, (SELECT name FROM orders_transport_means WHERE id = O.transport_id) AS name ');
    
    if ($bSingle === TRUE) {
      $sSql .= '
       AND (
        (SELECT sum(quantity) from orders_items WHERE order_id = O.id AND deleted = "0") = 1
       ) ';
    } else {
      $sSql .= '
       AND (
        (SELECT sum(quantity) from orders_items WHERE order_id = O.id AND deleted = "0") >= 2
       ) ';
    }
    
    $sSql .= ' GROUP BY O.transport_id '; 
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getCountLists() method
  
  
  /**
   * Metoda sprawdza, która metoda transportu ma najwięcej zamówień
   * 
   * @param array $aQuantityLists
   * @return
   */
  protected function getMaxQuantityTransportId($aQuantityLists) {
    
    $iMaxCount = NULL;
    $iMaximumTransportId = NULL;
    if (!empty($aQuantityLists)) {
      foreach ($aQuantityLists as $aItemTransport) {
        if ($iMaxCount === NULL) {
          $iMaxCount = $aItemTransport['count'];
          $iMaximumTransportId = $aItemTransport['transport_id'];
        } elseif ($iMaxCount < $aItemTransport['count']) {
          $iMaxCount = $aItemTransport['count'];
          $iMaximumTransportId = $aItemTransport['transport_id'];
        }
      }
      return $iMaximumTransportId;
    }
  }// end of getMaxQuantityTransportId() method
  
  
  /**
   * Metoda sprawdza, czy istnieje maksymalna wartość pomiędzy metodami transprotu,
   *  jesli istnieje, zaznacza opcję domyślnie
   * 
   * @param array $aQuantityLists
   * @param string $aTransportRadio
   * @return string
   */
  protected function checkMaximumValueTransport($aQuantityLists, $aTransportRadio) {
    
    $iMaxTransportId = $this->getMaxQuantityTransportId($aQuantityLists);
    if ($iMaxTransportId > 0) {
      foreach ($aTransportRadio as $iKey => $aTransport) {
//        if ($aTransport['value'] == $iMaxTransportId) {
        if (empty($_POST)) {
          $aTransportRadio[$iKey]['checked'] = 'checked';
        }
//        }
      }
    }
    return $aTransportRadio;
  }// end of checkMaximumValueTransport() method
  
  
  /**
   * Metoda pobiera tablicę do radio
   * 
   * @param array $aArray
   * @return array
   */
  protected function getRadioByArr($aArray) {
    
    $aRadio = array();
    foreach($aArray as $iKey => $aItem) {
      $aRadio[$iKey]['label'] = $aItem['name'];
      $aRadio[$iKey]['value'] = $aItem['id'];
    }
    return $aRadio;
  }// end of getRadioByArr() method
  
  
  /**
   * Metoda sprawdza, czy dane zamówienie oczekuje do wysłania na jakieś inne 
   * i czy w związku z tym mogą zostać wysłane razem
   * 
   * @param integer $iOrderId
   * @return char
   */
  protected function checkOrdersAccepted($iOrderId) {
    
    /*
CALL check_linked_orders(, @order_accepted);
SELECT @order_accepted;
     */
    $sSql = 'CALL check_linked_orders('.$iOrderId.', @order_accepted);';
    $this->pDbMgr->Query('profit24', $sSql);
    $cIsAccepted = $this->pDbMgr->GetOne('profit24', 'SELECT @order_accepted');
    return ($cIsAccepted == '1' ? true : false);
  }// end of checkOrdersAccepted() method
  
  
  /**
   * Metoda przelicza ilosć zamówień
   * 
   * @param array $aList
   */
  protected function countOrders($aList, $iOrdersLimit) {
    
    $aNewTMPLIST = array();
    foreach ($aList as $aItem) {
      if (!isset($this->aOrdersAccepted[$aItem['order_id']]) && !isset($this->aOrdersWaiting[$aItem['order_id']])) {
        
        $bErrDeliveryAddress = $this->validateTransportsMethods($aItem['order_id']);
        
        if ($bErrDeliveryAddress !== true) {
          $this->addErrorOrderToContainer($aItem['order_id']);
        }
        
        $mIsOrderLocked = $this->oParent->oOrdersSemaphore->isLocked($aItem['order_id'], $this->iUId);
        if ($this->checkOrdersAccepted($aItem['order_id']) == true && 
            $bErrDeliveryAddress === true && 
            $mIsOrderLocked === false) {
          // sprawdźmy czy możemy uwzględnić
          $this->aOrdersAccepted[$aItem['order_id']] = 1;
          $aNewTMPLIST[] = $aItem;
        } else {
          $this->aOrdersWaiting[$aItem['order_id']] = 0;
        }
        if (count($this->aOrdersAccepted) >= $iOrdersLimit) {
          return $aNewTMPLIST;
        }
      } else {
        $aNewTMPLIST[] = $aItem;
      }
    }
    return $aNewTMPLIST;
  }// end of countOrders() method
  
  /**
   * 
   * @param int $iOrderId
   * @return void
   */
  private function addErrorOrderToContainer($iOrderId) {
    
    $oContainers = new \orders_containers\OrdersContainers();
    $oContainers->addOrderToContainers($iOrderId, 'T', true);
  }
  
  /**
   * @param int $iOrderId
   * @return boolean
   */
  private function validateTransportsMethods($iOrderId) {
    global $pDbMgr;
    
    $sSymbol = $this->getOrderTransportSymbol($iOrderId);
    
    switch ($sSymbol) {
      case 'odbior-osobisty':
        return true;
      break;
    
      case 'poczta_polska':
        if ($this->checkOrderTransportAddress($iOrderId) === true) {
          $sPointOfReceipt = $this->getPointOfReceipt($iOrderId);
        } else {
          return false;
        }
      break;
      
      case 'opek-przesylka-kurierska':
        return $this->checkOrderTransportAddress($iOrderId);
      break;
      
      case 'tba':
        $sPostal = $this->getOrderTransportPostal($iOrderId);
        $sPointOfReceipt = $sPostal;
      break;
    
      default :
        $sPointOfReceipt = $this->getPointOfReceipt($iOrderId);
      break;
    }
    if ($sPointOfReceipt != '') {
      $oShipment = new \orders\Shipment($sSymbol, $pDbMgr);
      return $oShipment->checkDestinationPointAvailable($sPointOfReceipt);
    } else {
      return false;
    }
  }
  
  /**
   * @param int $iOrderId
   * @return bool
   */
  private function getOrderTransportPostal($iOrderId) {
    
    $sSql = 'SELECT postal 
             FROM orders_users_addresses
             WHERE order_id = ' . $iOrderId. ' 
               AND address_type = "1" ';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of checkOrderTransportAddress() method
  
  
  /**
   * Metoda sprawedza, czy adres do transportu, 
   *  jest zgodny z kodami podanymi w tabeli
   * 
   * @param int $iOrderId
   * @return bool
   */
  private function checkOrderTransportAddress($iOrderId) {
    
    $sSql = 'SELECT city, postal 
             FROM orders_users_addresses
             WHERE order_id = ' . $iOrderId. ' 
               AND address_type = "1" ';
    $aOrdersAddress = $this->pDbMgr->GetRow('profit24', $sSql);
    if (!empty($aOrdersAddress)) {
      return $this->checkCityPostal($aOrdersAddress['city'], $aOrdersAddress['postal']);
    } else {
      // domyślnie 
      return false;
    }
  }// end of checkOrderTransportAddress() method
  
  
	/**
	 * Metoda sprawdza czy kod pocztowy pasuje do miasta
	 *
	 * @global array $aConfig
	 * @param string $sCity
	 * @param string $sPostal
	 * @return bool 
	 */
	private function checkCityPostal($sCity, $sPostal) {
		global $aConfig;
		
		$sSql = "SELECT count(1)
						FROM ".$aConfig['tabls']['prefix']."users_accounts_postals
						WHERE city = ".Common::Quote(stripslashes($sCity)).
						 " AND postal = ".Common::Quote(stripslashes($sPostal));
		return (intval(Common::GetOne($sSql)) > 0);
	}	// end of checkCityPostal() method
  
  
  /**
   * Metoda pobiera rekordy na listę ograniczone co do ilości
   *  Uwaga modyfikacja, wyświetlamy aktualną półkę:
   *  Jeśli jest to źródło "jest nie" na G to pobieramy wyłącznie półkę dla wysłanych zamówień do "tramwaj" i "tramwaj migracja", ponieważ tylko cofnięty produkt może być zebrany z Tramwaju.
   *  Jeśli jest to inne źróło to standardowo pobieramy
   * 
   * @param int $iLimitRecords
   * @param array $aTransportId
   * @param string $sMaxOrderQuantity
   * @param string $sMinOrderQuantity - minimalna ilość egz. w zamówieniu 
   * @param date $dExpectedShipmentDate
   * @param bool $bForceOrdersItems
   * @param array $aOrderItems
   * @param bool $bGetPackets
   * @return array
   */
  public function getRecordsListLimited($iLimitRecords = 0, $aTransportId = NULL, $sMaxOrderQuantity = 0, $sMinOrderQuantity = 0, $dExpectedShipmentDate = '', $bForceOrdersItems = false, $aOrderItems = array(), $bGetPackets = false) {
    
    $sSql = $this->getQuery(' CONCAT(OI.order_id, ",", OI.id) AS ident, O.order_number, OI.id, OI.order_id, OI.quantity,
                              OI.name, OI.publisher, OI.authors, OI.publication_year, OI.edition, OI.product_id, 
                              OI.item_type,
                              P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13, IF(OI.source = 51, "1", "0") AS is_g,
                               
                              
(
IF (
  OI.source = 51 AND OI.status = "3", 
    CONCAT(
      IFNULL(
        (SELECT OSH.pack_number
        FROM orders_send_history AS OSH
        JOIN orders_send_history_items AS OSHI
          ON OSH.id = OSHI.send_history_id
        WHERE OSHI.item_id = OI.id AND
              OSH.source IN ("33", "34")
        ORDER BY OSH.id DESC
        LIMIT 1), 
        ""
       ),
       IFNULL(PS2.profit_g_location, "")
     )
  ,
    (
      SELECT OSH.pack_number
      FROM orders_send_history AS OSH
      JOIN orders_send_history_items AS OSHI
        ON OSH.id = OSHI.send_history_id
      WHERE OSHI.item_id = OI.id 
      ORDER BY OSH.id DESC
      LIMIT 1
    )
  )
) AS pack_number,


                              (SELECT id FROM orders_items WHERE parent_id = OI.id AND item_type = "A") AS is_cd,
                              (SELECT profit_g_act_stock FROM products_stock AS PS WHERE PS.id = OI.product_id) as profit_g_act_stock,
                              (SELECT CONCAT(directory, "|", photo) FROM products_images AS PI WHERE PI.product_id = OI.product_id LIMIT 1) AS photo,
                              OI.weight
                              ');
    
    if ($bForceOrdersItems === true) {
      if (!empty($aOrderItems)) {
        $sSql .= " AND O.id IN (".implode(', ', array_keys($aOrderItems)).") ";
      } else {
        return false;
      }
    }
    $sSql .= ' 
      AND 
      (
        O.remarks = ""
        OR O.remarks IS NULL 
        OR O.remarks_read_1 IS NOT NULL
      )
    ';
    
    $sMaxOrderQuantity = intval($sMaxOrderQuantity);
    if ($sMaxOrderQuantity > 0) {
      $sSql .= '
       AND (
        (SELECT sum(quantity) from orders_items WHERE order_id = O.id AND deleted = "0") <= '.$sMaxOrderQuantity.'
       ) ';
    }
    
    $sMinOrderQuantity = intval($sMinOrderQuantity);
    if ($sMinOrderQuantity > 0) {
      $sSql .= '
       AND (
        (SELECT sum(quantity) from orders_items WHERE order_id = O.id AND deleted = "0") >= '.$sMinOrderQuantity.'
       ) ';
    }
    
    if (is_numeric($aTransportId) && !is_array($aTransportId)) {
      $iTransportId = intval($aTransportId);
      $aTransportId[$iTransportId] = $iTransportId;
    }
    
    if (!empty($aTransportId) && is_array($aTransportId)) {
      $sSql .= '
       AND (
          O.transport_id IN ('.implode(', ', $aTransportId).')
        ) ';
    }
    
    if ($bGetPackets == FALSE) {
      $sSql .= ' AND O.id NOT IN (SELECT main_order_id FROM orders_linked_transport) ';
    }
    
    if ($dExpectedShipmentDate != '' && $dExpectedShipmentDate != '00-00-0000') {
      $dFormedExpectedDate = preg_replace('/(\d{2})-(\d{2})-(\d{4})/', '$3-$2-$1', $dExpectedShipmentDate);
      $sSql .= ' AND O.shipment_date_expected <= "'.$dFormedExpectedDate.'" ';
    }
    
    $sSql .= " 
      GROUP BY OI.id     
      ORDER BY O.magazine_get_ready_list_VIP DESC, O.shipment_date_expected ASC, IF(O.payment_type = 'postal_fee', '1', '0') DESC, O.id ASC
      ". ($iLimitRecords > 0 ? " LIMIT ".$iLimitRecords : '');
    $this->sGetListSQL = $sSql;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getRecordsListLimited() method
  
  
  /**
   * Metoda pobiera liczbę list, które są aktualnie zbierane
   * 
   * @return int
   */
  protected function getCountInCompletation($aType) {
    
    $sSql = 'SELECT count(id)
             FROM orders_items_lists
             WHERE 
              closed = "0" AND
              type IN ('.implode(',', $aType).') ';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of getCountInCompletation() method
  
  
  /**
   * Metoda pobiera liczbę list, które są aktualnie kompletowane
   * 
   * @return int
   */
  protected function getCountInSort($aType) {
    
    $sSql = 'SELECT count(id)
             FROM orders_items_lists
             WHERE 
                closed = "2" AND
                type IN ('.implode(',', $aType).') ';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of getCountInSort() method
  
  
  /**
   * Metoda stara się pobrać zamówienia do określonej liczby
   * 
   * @param int $iOrdersLimit
   * @param array $aTransportIds
   * @param string $sMaxOrderQuantity
   * @param string $sMinOrderQuantity
   * @param date $dExpectedShipmentDate
   */
  protected function getLimitedOrders($iOrdersLimit, $aTransportIds = NULL, $sMaxOrderQuantity = 0, $sMinOrderQuantity = 0, $dExpectedShipmentDate = '') {
    $iLimitIterations = 2;// maks 6 pobrania listy
    $iCountItems = 40; // ilość elementów do pobrania na starcie
    $i = 0;
    unset($this->aOrdersAccepted);
    unset($this->aOrdersWaiting);
    
    while($i <= $iLimitIterations) {
      $aFirstList = $this->getRecordsListLimited($iCountItems, $aTransportIds, $sMaxOrderQuantity, $sMinOrderQuantity, $dExpectedShipmentDate);
      $aList = $this->countOrders($aFirstList, $iOrdersLimit);
      if (count($this->aOrdersAccepted) < $iOrdersLimit) {
        $iCountItems += 40;
      } else {
        return $aList;
      }
      $i++;
    }
    return $aList;
  }// end of getLimitedOrders() method
  
  
  /**
   * Metoda oblicza ilość zamówień które można pobrać 
   * 
   * @param object $pSmarty
   */
  protected function getCountOrders(&$pSmarty) {
    unset($this->aOrdersAccepted);
    unset($this->aOrdersWaiting);
    
    $aFirstList = $this->getRecordsListLimited(0, $_POST['transport_id'], $_POST['max_order_item'], $_POST['min_order_item'], $_POST['shipment_date_expected']);
    $this->countOrders($aFirstList, 99999);
    $iCount = count($this->aOrdersAccepted);
    
    $this->sMsg .= GetMessage(sprintf(_('Ilosć zamówień gotowych do pobrania: %s'), $iCount), false);
    $this->Show($pSmarty);
  }// end of getCountOrders() method
  
  
  /**
   * Metoda pobiera listę produktów do pobrania
   * 
   * @param object $pSmarty
   * @return void
   */
  protected function getList(&$pSmarty) {

    $bIsErr = false;
    
    $iOrdersLimit = 15;
    $aList = $this->getLimitedOrders($iOrdersLimit, $_POST['transport_id'], $_POST['max_order_item'], $_POST['min_order_item'], $_POST['shipment_date_expected']);
    $sGetListSQL = $this->sGetListSQL;
    $aList = $this->getRecordsListLimited(0, $_POST['transport_id'], $_POST['max_order_item'], $_POST['min_order_item'], $_POST['shipment_date_expected'], true, $this->aOrdersAccepted, false);
    $cType = $_POST['type'];
    $sPackageNumber = $_POST['package_number'];
    $mTransportId = $_POST['transport_id'];
    
    if (!empty($aList) && !empty($sPackageNumber)) {
      usort($aList, 'sortItems');

      // posortować na liście od magazynu do tramwaju w kolejności narastającej
      $this->pDbMgr->BeginTransaction('profit24');
      // zmieniamy w zam: print_ready_list = '1'
      // , w skł. zam: get_ready_list = '1'
      $aDebug['sql'] = $sGetListSQL;
      $aDebug['post'] = $_POST;
      $mReturn = $this->doCreateList($aList, $cType, $sPackageNumber, $mTransportId, base64_encode(serialize($aDebug)));
      if ($mReturn === FALSE || $mReturn <= 0) {
        $bIsErr = -1;
      } else {
        $iILId = intval($mReturn);
      }
      
    } else {
      $bIsErr = true;
      // błąd
      $this->pDbMgr->RollbackTransaction('profit24');
      $sMsg = _('Brak produktów do zbierania.');
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
      return;
    }
    
    if ($bIsErr !== false) {
      switch ($bIsErr) {
        case -1:
          $sMsg = _('Wystąpił błąd podczas blokowania kuwety !');
        break;
        default:
        case false:
          $sMsg = _('Wystąpił błąd podczas pobierania listy produktów do zbierania.');
        break;
      }
      // błąd
      $this->pDbMgr->RollbackTransaction('profit24');
      
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
      return;
    } else {
      $this->pDbMgr->CommitTransaction('profit24');
      $this->getProductsScanner($pSmarty, $iILId, $aList, $sPackageNumber);
      return true;
    }
  }// end of getList() method
  
  
  /**
   * Metoda tworzy listę do zebrania na podstawie przekazanej tablicy danych
   * 
   * @param array $aList
   * @param char $cListType - przyjmuje typy:
   *      '0' - zwykłe
   *      '1' - single
   *      '2' - wysyłka razem
   * @param string(uint) $sPackageNumber - numer kuwety
   * @param mixed $mTransportId - id metody transportu
   * @param string $sDebug
   * @return bool
   */
  protected function doCreateList($aList, $cListType, $sPackageNumber, $mTransportId, $sDebug = '') {
    global $aConfig;
    $bIsErr = false;

    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
    $oContainers = new \magazine\Containers($this->pDbMgr);
    $mReturn = $oContainers->lockContainer($sPackageNumber);
    if ($mReturn !== true) {
      // coś poszło nie tak
      return $mReturn;
    }
    
    foreach ($this->aOrdersAccepted as $iOrderId => $sVal) {
      // zmieniamy składową w zamówieniu
      $aOrderValues = array('print_ready_list' => '1');
      if ($this->pDbMgr->Update('profit24', 'orders', $aOrderValues, 'id = '. $iOrderId) === FALSE) {
        $bIsErr = true;
      }
    }

    // dodajemy teraz dane listy
    $aValuesOIL = array(
        'user_id' => $_SESSION['user']['id'],
        'document_number' => ($this->pDbMgr->GetOne('profit24', 'SELECT MAX( id ) FROM orders_items_lists') + 1),
        'package_number' => $sPackageNumber,
        'closed' => '0',
        'type' => $cListType,
        'created' => 'NOW()',
        'debug' => $sDebug,
        'big' => (stristr($_GET['action'], '_big') ? '1' : '0')
    );
    if (is_array($mTransportId)) {
      $aValuesOIL['transport_id'] = 'NULL';
    } elseif ($mTransportId <= 0) {
      $aValuesOIL['transport_id'] = 'NULL';
    } else {
      $aValuesOIL['transport_id'] = $mTransportId;
    }
    $iILId = $this->pDbMgr->Insert('profit24', 'orders_items_lists', $aValuesOIL);
    
    if ($iILId > 0) {

      // dodajemy transport
      if ($this->addOILTransport($iILId, $mTransportId) === false) {
        $bIsErr = true;
      }
      
      // dodajemy składowe
      foreach ($aList as $aItem) {
        if ($aItem['item_type'] == 'I' || $aItem['item_type'] == 'P') {
          $aValuesOILI = array(
              'orders_items_lists_id' => $iILId,
              'orders_items_id' => $aItem['id'],
              'orders_id' => $aItem['order_id'],
              'products_id' => $aItem['product_id'],
              'quantity' => $aItem['quantity'],
              'is_g' => $aItem['is_g'],
              'isbn_plain' => $aItem['isbn_plain'],
              'isbn_10' => $aItem['isbn_10'],
              'isbn_13' => $aItem['isbn_13'],
              'ean_13' => $aItem['ean_13'],
          );
          if ($this->pDbMgr->Insert('profit24', 'orders_items_lists_items', $aValuesOILI) == FALSE) {
            $bIsErr = true;
          }

          $aOrderItemValues = array('get_ready_list' => '1');
          if ($this->pDbMgr->Update('profit24', 'orders_items', $aOrderItemValues, 'id = '. $aItem['id']) == FALSE) {
            $bIsErr = true;
          }
        }
      }
    }
    
    if ($bIsErr === TRUE) {
      return false;
    } else {
      return $iILId;
    }
  }// end of doCreateList() method
  
  /**
   * 
   * @param int $iILId
   * @param mixed $mTransportId
   * @return bool
   */
  private function addOILTransport($iILId, $mTransportId) {
    
    if (!is_array($mTransportId)) {
      if ($mTransportId > 0) { 
        $iTransportId = intval($mTransportId);
        $mTransportId[$iTransportId] = $iTransportId;
      } else {
        return true;// nic tu nie dodajemy
      }
    }
    
    foreach ($mTransportId as $iTransportItemId) {
      $aValues = array(
        'orders_items_lists_id' => $iILId,
        'orders_transport_means_id' => $iTransportItemId
      );
      if ($this->pDbMgr->Insert('profit24', 'orders_items_lists_tranport', $aValues) === FALSE) {
        return false;
      }
    }
    return true;
  }
  
  
  /**
   * Metoda sumuje wskazaną kolumnę tablicy na podstawie kolumny
   * 
   * @param array $aList
   * @param string $sSumKey
   * @param string $sSumByKey
   * @return string
   */
  public function sumByArray($aList, $sSumKey, $sSumByKey) {
    
    foreach ($aList as $iKey => $aItem) {
      //if ($aItem[$sSumByKey] != '') {// nie pusty
      if ($aItem['product_id'] != '') {
        $iCurrNextRow = 1;
        while(isset($aList[$iKey+$iCurrNextRow]) && $aList[$iKey+$iCurrNextRow][$sSumByKey] === $aItem[$sSumByKey] && $aList[$iKey+$iCurrNextRow]['product_id'] == $aItem['product_id']) {// dopuki następny klucz nie pusty i wartość kolumny się zgadza
          $aList[$iKey][$sSumKey] += intval($aList[$iKey+$iCurrNextRow][$sSumKey]);
          unset($aList[$iKey+$iCurrNextRow]);
          $iCurrNextRow++;
        }
      }
      //}
    }
    $aList = array_merge($aList);
    return $aList;
  }// end of sumByArray() method
  
  
  /**
   * Metoda tworzy i zwraca PDF'a
   * 
   * @param \Smarty $pSmarty
   * @param int $iILId
   * @param array $aList
   */
  protected function getProductsScanner(&$pSmarty, $iILId, $aList, $sPackageNumber) {
//    include_once 'barcode/barcode.php';

    //$sCode = str_pad($iILId, 13, '0', STR_PAD_LEFT);

    
    $iRefCountMagazine = 0;
    $iRefCountTramwaj = 0;
    $this->countQuantityItems($aList, $iRefCountMagazine, $iRefCountTramwaj);
    $aViewList = $this->sumByArray($aList, 'quantity', 'pack_number');

    $aBooks = array();
    $aBooks['number'] = $sCode;
    $aBooks['items'] = $this->parseBooksToList($aViewList); 
//    $aBooks['author'] = $_SESSION['user']['author']; 
//    $aBooks['count_magazine'] = $iRefCountMagazine;
//    $aBooks['count_tramwaj'] = $iRefCountTramwaj;

    
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('ordered_itm', _('Zbieranie towaru. Listy o id: '.$iILId), array('action'=>phpSelf(array('id'=>$iILId)), 'onsubmit' => 'return false;'), array('col_width'=>155), false);
    $pForm->AddHidden('do', 'save_list_data');
    $pForm->AddHidden('orders_items_lists_id', $iILId);
    $pForm->AddMergedRow( 
            '<div style="float:left">'.
              $pForm->GetLabelHTML('search_isbn', _('ISBN '), false).
              $pForm->GetTextHTML('search_isbn', _('ISBN'), '', array('style' => 'font-size: 40px; margin-right: 50px;'), '', 'uint', true).
//        $pForm->GetTextHTML('package_number', _('Numer kuwety'), '', array('style' => 'font-size: 40px; width: 260px;'), '', 'uint', true).
            '</div>'.
            'Czas: <span id="timer"></span> &nbsp; &nbsp; '.
         $pForm->GetLabelHTML('package_number', _('Numer kuwety: '), false).' '.$sPackageNumber
    );
    
    $sHTML = $this->getViewProductsScanner($pSmarty, $aBooks);
    $pForm->AddMergedRow($sHTML);
    $pForm->AddMergedRow( 
            '<div style="text-align: center;">'
              .$pForm->GetInputButtonHTML('save', _('Zakończ / ЗАПИСАТЬ'), array('style'=>'background-color: #51a351; font-size: 40px; text-align: center; color: #000;','onclick'=>'submit();'), 'button')
            .'</div>'
    );
    $pForm->AddMergedRow( 
//          $pForm->GetInputButtonHTML('show_not_scanned', _('Pokaż braki'), array('style'=>'font-size: 40px; float:left ;','onclick'=>'veryfiList();'), 'button').' &nbsp; '.
          $pForm->GetInputButtonHTML('cancel', _('Anuluj'), array('style'=>'font-size: 40px; float:left;','onclick'=>'window.close();'), 'button').
          $pForm->GetInputButtonHTML('go_back', _('Pomiń / ОТЛОЖИТЬ'), array('style'=>'font-size: 40px; float:right;'), 'button')
    );

    $sInput = '
    <script type="text/javascript" src="js/CoolEctor.js?date=20181109"></script>';
    
     /*
    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" style="font-size: 40px;" /></div>

    
    <script type="text/javascript">
      $(function() {
        $("#pack_number").focus();
      });
    </script>';
    */
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sInput.ShowTable($pForm->ShowForm()));
    return true;
    
    /*
    $pSmarty->assign_by_ref('aBooks', $aBooks);
    $sHtml = $pSmarty->fetch('PDF/pdf_ready_list.tpl');
    $pSmarty->clear_assign('aBooks');

    
    
    $im     = imagecreatetruecolor(300, 100);  
    $black  = ImageColorAllocate($im,0x00,0x00,0x00);  
    $white  = ImageColorAllocate($im,0xff,0xff,0xff);  
    imagefilledrectangle($im, 0, 0, 300, 100, $white);  
    $data = Barcode::gd($im, $black, 150, 50, 0, "code128", $sCode, 2, 50);  
    
    // kod kreskowy
    ob_start();
    imagepng($im);
    imagedestroy( $im );
    $imagedata = ob_get_clean();


    //$this->getPDF($sHtml, $imagedata, $aBooks);
     * 
     */
  }// end of getProductsScanner() method
  
  
  /**
   * Metoda zapisuje dane z zebranej listy
   * 
   * @param type $pSmarty
   */
  protected function  saveListData($pSmarty) {
    $bIsErr = false;

    // ok mamy sobie dwie tablice
    $aBooksScanned = json_decode($_POST['books']);
    $aDeficienciesBooks = json_decode($_POST['not_scanned_books']);
    $iOLIId = json_decode($_POST['orders_items_lists_id']);
    
    
    foreach ($aBooksScanned as $aBook) { 
      if ($this->AddOILITimes($iOLIId, $aBook->ident, $aBook->time, $aBook->currquantity) === false) {
        $bIsErr = true;
      }
    }
    
    foreach ($aDeficienciesBooks as $aBook) { 
      if (isset($aBook->time) && $aBook->time > 0) {
        $aBook->time = 0;
      }
      if ($this->AddOILITimes($iOLIId, $aBook->ident, $aBook->time, $aBook->currquantity) === false) {
        $bIsErr = true;
      }
    }
    if ( $bIsErr === false) {
      echo '<script type="text/javascript">window.close();</script>';
    } else {
      echo '<script type="text/javascript">alert("Wystąpił błąd zapisywania czasów listy !"); window.close();</script>';
    }
  }// end of saveListData() method
  
  
  /**
   * Dodanie nowego rekordu czasów zeskanowania
   * 
   * @param int $iOLIId
   * @param string $sIdent
   * @param int $iTime
   * @param int|string $scQuantity
   * @return boolean
   */
  private function AddOILITimes($iOLIId, $sIdent, $iTime, $scQuantity) {
    
    $sSql = 'SELECT id FROM orders_items_lists_items WHERE orders_items_lists_id ='.$iOLIId.' AND CONCAT(orders_id, ",", orders_items_id) = "'.$sIdent.'"';
    $iOILIId = $this->pDbMgr->GetOne('profit24', $sSql);
    
    // dopasowanie
    if ($iOILIId > 0) {
      $sSql = 'SELECT id FROM orders_items_lists_items_times WHERE id = '.$iOILIId;
      $iOILITId = $this->pDbMgr->GetOne('profit24', $sSql);
      if ($iOILITId > 0) {
        // przcież nie będziemy aktualizować czasu zbierania
        return true;
      } else {
        $aValues = array(
            'id' => $iOILIId,
            'time' => $iTime,
            'sc_quantity' => $scQuantity
        );
        return ($this->pDbMgr->Insert('profit24', 'orders_items_lists_items_times', $aValues) === false ?  false : true );
      }
    } else {
      return false;
    }
  }// end of AddOILITimes() method
  
  
  /**
   * Dowiązanie widoku do listy produktów do odznaczenia 
   * 
   * @param \Smarty $pSmarty
   * @param array $aBooks
   * @return string
   */
  private function getViewProductsScanner(&$pSmarty, $aBooks) {
    
    $aChars = $this->charsArrayLine();
    $pSmarty->assign('aChars', $aChars);
    
    $sJS = json_encode($aBooks['items']);
    $pSmarty->assign('sProductsEncoded', $sJS);
    $sHtml = $pSmarty->fetch('collect_products.tpl');
    $pSmarty->clear_assign('sProductsEncoded');
    $pSmarty->clear_assign('aChars');
    return $sHtml;
  }// end of getViewProductsScanner() method
  
  
  /**
   * Metoda pobiera tablicę zanków
   * 
   * @return array
   */
  private function charsArrayLine() {
    
    $aChars = 
      array( 
          'chars' => array(
          array('Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'),
          array('A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'),
          array('', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '&#8629;'),
          ),
          'int' => array(
              array(7, 8, 9),
              array(4, 5, 6),
              array(1, 2, 3),
              array(0, 'X', '&#8629;')
          )
      );
    return $aChars;
  }// end of charsArrayLine() method
  
  
  /**
   * Metoda przelicza ile egz jest na tramwaju a ile na magazynie
   * 
   * @param type $aBooks
   * @param int $iCountMagazine
   * @param int $iCountTramwaj
   */
  public function countQuantityItems($aBooks, &$iCountMagazine, &$iCountTramwaj) {
    $iCountMagazine = 0;
    $iCountTramwaj = 0;
    foreach ($aBooks as $aBook) {
      if ($aBook['pack_number'] != '' && substr($aBook['pack_number'], 0, 2) != '10') {
        // tramwaj
        $iCountTramwaj += $aBook['quantity'];
      } else {
        // magazyn
        $iCountMagazine += $aBook['quantity'];
      }
    }
  }// end of countQuantityItems() method
  
  
  /**
   * Parsuje listę produktów
   * 
   * @param array $aBooks
   * @return array
   */
  public function parseBooksToList($aBooks) {
    $aAllovedCols = array(
        'ident',
        'photo',
        'quantity',
        'name',
        'authors',
        'publisher',
        'isbn_plain',
        'isbn_10',
        'isbn_13',
        'ean_13',
        'pack_number',
        'checked',
        'weight',
        'publication_year'
    );
    foreach ($aBooks as &$aBook) {
      foreach ($aBook as $sCol => $sVal) {
        if (!in_array($sCol, $aAllovedCols)) {
          unset($aBook[$sCol]);
        }
        $aBook['shelf_number'] = $aBook['pack_number'];
      }
      $aBook['checked'] = '0';
      $aBook['currquantity'] = '0';
    }
    return $aBooks;
  }// end of parseBooksToList() method
  
  
  /**
   * Metoda generuje pdf'a
   * 
   * @param string $sHTML
   * @param imagedata $im
   */
  public function getPDF($sHTML, $im, $aBooks) {
    
//    require_once('tcpdf/config/lang/pl.php');
		require_once('tcpdf/tcpdf.php');		

		// create new PDF document
		$pdf = new MYPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('profit24');
		$pdf->SetTitle('Lista produktów do zebrania');
		$pdf->SetSubject('Lista produktów do zebrania');
		$pdf->SetKeywords('');
		$pdf->setPrintHeader(true);
    
    $pdf->headerImage = $im;
    $pdf->headerText = '<span style="font-size: 10pt; font-weight: bold; ">Lista produktów do zebrania</span><br /><span style="font-size: 10pt;">Numer listy: '.$aBooks['number'].'<br />
Data generowania: '.date('d.m.Y H:i:s').'<br />
Wygenerowal: '.$aBooks['author'].
      '</span>';

		$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(2, 28, 2);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 10);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 

		//set some language-dependent strings
		$pdf->setLanguageArray($l); 

		// set font
		$pdf->SetFont('freesans', '', 8);
    

		// add a page
		$pdf->AddPage();
    $pdf->setJPEGQuality(100);
    
    //$pdf->Image('@'.$im, 80, 0);
		$pdf->writeHTML($sHTML, true, false, false, false, '');
    
		$pdf->Output('lista_do_zebrania.pdf', 'D');
  }// end fo getPDF)( method
}
