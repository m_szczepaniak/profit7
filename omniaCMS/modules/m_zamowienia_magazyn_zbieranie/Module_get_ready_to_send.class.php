<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-06-24 
 * @copyrights Marcin Chudy - Profit24.pl
 */

require_once('modules/m_zamowienia_magazyn_zbieranie_tramwaj/Common_get_ready_orders_books.class.php');

/**
 * Klasa pobiera listę produktów gotowych do zebrania i wysłania
 * 
 */
class Module__zamowienia_magazyn_zbieranie__get_ready_to_send extends Common_get_ready_orders_books {

  // komunikat
	public $sMsg;

	// nazwa modulu - do langow
	public $sModule;

	// id wersji jezykowej
	public $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	protected $aPrivileges;
	
	// id strony modulu
	protected $iPageId;
  
  // id uzytkownika
  protected $iUId;
  
	// id listy zamowien
	protected $iPPId;
  
  // id zamowienia
  protected $iId;
  
  protected $_aTransports;

  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  protected $pDbMgr;
  
  protected $aOrdersAccepted = array();
  protected $aOrdersWaiting = array();
  
  function __construct(&$pSmarty, $bInit = true) {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
    
		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];

		if (isset($_GET['pid'])) {
			$iId = $_GET['pid'];
		}
    
		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
    
    $this->_aTransports = $this->getTransports();

		$this->iUId = $_SESSION['user']['id'];
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    if ($bInit == false) {
      return;
    }
    parent::__construct($bInit);
    
    switch ($sDo) {
      case 'generate':
        $this->getList($pSmarty);
      break;
    
      case 'get_count_orders':
        $this->getCountOrders($pSmarty);
      break;
    
      case 'save_list_data':
        $this->saveListData($pSmarty);
      break;
      
      case 'do':
      default:
        $this->Show($pSmarty);
      break;
    
    
      case 'get_linked_transport':
        $this->getUniqeLinkedOrders($pSmarty);
      break;
    }
  }// end of __construct()
  
  
  /**
   * Wyświetla formularz
   * 
   * @param \Smarty $pSmarty
   */
  protected function Show($pSmarty) {
    
    $aData = array();
    if (isset($_POST)) {
      $aData = $_POST;
    }
    
    // pobieramy sobie informację o tym czy, istnieją jakieś listy do wygenerowania
    $aQuantityLists = $this->getCountLists();
    if (!empty($aQuantityLists)) {
      $sQuantity = '<ul>';
      foreach ($aQuantityLists as $aRow) {
        $sQuantity .= '<li style="font-size: 18; font-weight: bold;">'.$aRow['name'] .': '. $aRow['count'].'</li>';
      }
      $sQuantity .= '</ul>';
    } else {
      $sQuantity = _('Brak zamówień');
    }
    
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('ordered_itm', _("Zamówienia gotowe do wysłania - pobierz listę"), array('action'=>phpSelf(), 'target' => '_blank'), array('col_width'=>355), true);
    $pForm->AddHidden('do', 'generate');
    $pForm->AddHidden('type', '0');// oznaczenie zwykłej listy, jeśli pobieramy wysyłkę razem to zostaje zmienione
    $pForm->AddText('package_number', _('Numer kuwety'), _(''), array('style' => 'font-size: 40px;'), '', 'uint', true);
    
    $bManageReadySend = $this->checkManageGetReadySend($_SESSION['user']);
    if ($bManageReadySend === true) {

      $pForm->AddMergedRow('Statystyki', array('class' => 'merged'));

      $iCountInCompletation = $this->getCountInCompletation(array('0'), array('2'));
      $pForm->AddRow('Ilość list w trakcie kompletowania : ', '<span style="font-size: 18; font-weight: bold;">'.$iCountInCompletation.'</span>');

      $iCountInSort = $this->getCountInSort(array('0'), array('2'));
      $pForm->AddRow('Ilość list w trakcie sortowania : ', '<span style="font-size: 18; font-weight: bold;">'.$iCountInSort.'</span>');

      $pForm->AddMergedRow('Pobierz produkty', array('class' => 'merged'));

      $pForm->AddRow('Ilość zamówień gotowych do zbierania i kompletowania: ', $sQuantity);
      $pForm->AddRow('', 'UWAGA !! <br />Ilość zamówień może być niedokładna ponieważ są uwzględniane:<ol>'.
              '<li>zamówienia, które mają zostać wysłane razem i oczekują na pozostałe</li>'.
              '<li><span style="color: red">zamówienia w których istnieje nieprzeczytana uwaga do zamówienia</span></li>'.
              '</ol>');

      $aTransportRadio = $this->getRadioByArr($this->_aTransports);
      $aTransportRadio = $this->checkMaximumValueTransport($aQuantityLists, $aTransportRadio);

      // dzień do którego pobieramy zamówienia
      $pForm->AddText('shipment_date_expected', _('Do daty wysyłki'), (!isset($aData['shipment_date_expected']) ? '' : (empty($aData['shipment_date_expected']) ? '00-00-0000' : $aData['shipment_date_expected'])), array('style'=>'width: 75px;'), '', 'date', false );

      $pForm->AddCheckBoxSet('transport_id', _('Metoda transportu'), $aTransportRadio, $aData['transport_id']);

      $pForm->AddText('max_order_item', _('Maksymalna ilość produktów w zamówieniu'), $aData['max_order_item'], array(), '', 'uint', false);
      $pForm->AddText('min_order_item', _('Minimalna ilość produktów w zamówieniu'), (!isset($aData['min_order_item']) ? '2' : $aData['min_order_item']), 
                    array(), '', 'uint', true, '', array(2, 9999));

    } else {
      $pForm->AddHidden('min_order_item', '2');// bez singli
      
      $iMaxTransportId = $this->getMaxQuantityTransportId($aQuantityLists);
      if ($iMaxTransportId > 0) {
        $pForm->AddHidden('transport_id', $iMaxTransportId);
      }
      
      $pForm->AddMergedRow('Pobierz produkty', array('class' => 'merged'));
    }
    
    
    $pForm->AddInputButton("generate", _('Pobierz produkty'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
    
    if ($bManageReadySend === true) {
      // przycisk zmienia POST i przesyła zmieniony formularz
      $pForm->AddInputButton("get_count_orders", _('Sprawdź ilość zamówień / ПОКАЗАТЬ КОЛИЧЕСТВО ЗАКАЗОВ'), array('onclick' => '$(\'#ordered_itm\').attr(\'target\', \'\'); $(\'#do\').val(\'get_count_orders\'); submit(); ', 'style' => 'font-size: 40px; font-weight: bold; color: green;'), 'button');
    }
   
    
    $pForm->AddMergedRow('Pobierz wysyłane razem', array('class' => 'merged'));
    
    $this->setCountAvailableLinkedTransport();
    $iQuantityLinkedReady = count($this->aOrdersAccepted);
    $pForm->AddRow('Ilość zamówień wysyłanych razem do pobrania: ', '<span style="font-size: 18; font-weight: bold;">'.$iQuantityLinkedReady.'</span>');
    
    $pForm->AddInputButton("get_linked_transport", _('Pobierz wysyłane razem'), array('onclick' => '$(\'#transport_id\').val(\'\'); $(\'#do\').val(\'get_linked_transport\'); $(\'#ordered_itm\').submit();', 'style' => 'font-size: 40px; font-weight: bold; color: red;'), 'button');
    
    
    $sJS = '
      <script type="text/javascript">
        $( function () {
          $("#package_number").focus();
        });
      </script>
      
      ';
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()).$sJS);
  }// end of Show() method
}// end of class