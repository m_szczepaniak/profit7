<?php
/**
 * Klasa Module do obslugi konfiguracji stron modulu 'Oferta produktowa'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Settings {
  
  // nazwa modulu - do langow
	var $sModule;
	
	// Id strony dla ktorej edytowane sa ustawienia
	var $iId;
	
	// sciezka do katalogu zawierajacego szablony czesci klienckiej modulu
	var $sTemplatesDir;
	
	// lista szablonow czesci klienckiej modulu
	var $sTemplatesList;

	// plik XML z domyslna konfiguracja
	var $sXMLFile;
	
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$sModule, &$iId) {
		global $aConfig;
		$this->sModule = $sModule;
		$this->iId = $iId;
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/modules/'.$this->sModule;
		$this->sTemplatesList =& GetTemplatesList($this->sTemplatesDir);
		$this->sXMLFile = 'config/'.$sModule.'/default_config.xml';
		
		// pobranie konfiguracji dla strony
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."products_settings 
						 WHERE page_id = ".$this->iId;
						 
		$aConfig['settings'][$this->sModule] =& Common::GetRow($sSql);
		
		if (empty($aConfig['settings'][$this->sModule])) {
			// brak ustawien - pobranie domyslnych z pliku XML
			$aConfig['settings'][$this->sModule] =& getModuleConfig($this->sXMLFile);
		}
	} // end Settings() function
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach strony newslettera
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;
		$aValues = array();
		
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products_settings
						 WHERE page_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);
		
		$aValues = array(
			'list_template' => $_POST['list_template'],
		/*	'item_template' => $_POST['item_template'],
			'thumb_size' => $_POST['thumb_size'],
			'small_size' => $_POST['small_size'],
			'big_size' => $_POST['big_size'],*/
			'items_per_page' => (int) $_POST['authors_per_page'],
		);

		if (!empty($aSettings)) {
			// Update
			return Common::Update($aConfig['tabls']['prefix']."products_settings",
														$aValues,
														"page_id = ".$this->iId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('page_id' => $this->iId), $aValues);
			return Common::Insert($aConfig['tabls']['prefix']."products_settings",
														$aValues,
														"",
														false) !== false;
		}
	} // end of UpdateSettings() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji konfiguracji strony
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;
		
		$aData = array();
		$sHtml = '';
		
		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_mtype') {
			$aData =& $_POST;
		}
		
		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule.'_settings']['settings'], array('class'=>'merged', 'style'=>'padding-left: 208px'));
		// szablon listy, produktu
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule.'_settings']['templates'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
		$pForm->AddSelect('list_template', $aConfig['lang'][$this->sModule.'_settings']['list_template'], array(), $this->sTemplatesList, $aData['list_template'], '', false);
				
		$pForm->AddText('authors_per_page', $aConfig['lang'][$this->sModule.'_settings']['publishers_per_page'], $aData['items_per_page'], array('maxlength'=>3, 'style'=>'width: 30px;'), '', 'uinteger');
	} // end of SettingsForm() function
} // end of Settings Class
?>