<?php
/**
* Plik jezykowy dla interfejsu modulu 'Powiazane oferta produktowa' Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*---------- ustawienia oferta produktowa */
$aConfig['lang']['m_powiazane_oferta_produktowa_config']['header'] = "Konfiguracja domyślna stron modułu \"".$aConfig['lang']['c_modules']['m_aktualnosci']."\"";
$aConfig['lang']['m_powiazane_oferta_produktowa_config']['edit_err'] = "Wystąpił błąd podczas aktualizacji domyślnej konfiguracji modułu \"".$aConfig['lang']['c_modules']['m_aktualnosci']."\"";
$aConfig['lang']['m_powiazane_oferta_produktowa_config']['edit_ok'] = "Zaktualizowano domyślną konfigurację modułu \"".$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa']."\"";


/*---------- ustawienia oferty produktowej */
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['settings'] = "Konfiguracja strony oferty produktowej";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['templates'] = "Szablony";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['list_template'] = "Szablon listy";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['item_template'] = "Szablon produktu";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['photos'] = "Zdjęcia";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['show_page_id'] = "Strona";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['thumb_size'] = "Rozmiar miniaturki zdjęcia";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['small_size'] = "Rozmiar małego zdjęcia";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['medium_size'] = "Rozmiar średniego zdjęcia";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['big_size'] = "Rozmiar dużego zdjęcia";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['items_per_page'] = "Produktów na stronie";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['authors_per_page'] = "Autorów na stronie";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['publishers_per_page'] = "Wydawców na stronie";
$aConfig['lang']['m_powiazane_oferta_produktowa_settings']['series_per_page'] = "Serii na stronie";

/*---------- ustawienia boksow */
$aConfig['lang']['m_powiazane_oferta_produktowa_common_box_settings']['page_id'] = "Strona";
$aConfig['lang']['m_powiazane_oferta_produktowa_common_box_settings']['items_in_box'] = "Produktów w boksie";
$aConfig['lang']['m_powiazane_oferta_produktowa_wyrozniona_box_settings']['product_id'] = "Wyróżniona książka";

?>