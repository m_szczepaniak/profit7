<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - bestsellery
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['no_data'] = 'W chwili obecnej nie posiadamy żadnych nowości.';

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['language'] = "Język książki";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['binding'] = "Oprawa";

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['shipment'] = "Dostępność";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['shipment_preview'] = "Dostępność od";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['isbn']= 'ISBN: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['shipment_3'] = "na zamówienie";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['publisher']	= 'Wydawnictwo: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['series']	= 'Seria: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['avalaibility']	= 'Dostępny: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_nowosci']['edition']	= 'Wydanie: ';

?>