<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - zapowiedzi
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['no_data'] = 'W chwili obecnej nie posiadamy żadnych produktów zapowiedzi.';

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['language'] = "Język książki";

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['shipment'] = "Dostępność";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['shipment_preview'] = "Dostępność od";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['isbn']= 'ISBN: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['shipment_3'] = "na zamówienie";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['publisher']	= 'Wydawnictwo: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['avalaibility']	= 'Dostępny: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['edition']	= 'Wydanie: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['series']	= 'Seria: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_zapowiedzi']['binding'] = "Oprawa";
?>