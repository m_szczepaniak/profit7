<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - bestsellery
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['no_data'] = 'Ta książka nie posiada pozostałych wydań.';

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['language'] = "Język książki";

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['shipment'] = "Dostępność";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['shipment_preview'] = "Dostępność od";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['isbn']= 'ISBN: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['shipment_3'] = "na zamówienie";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['publisher']	= 'Wydawnictwo: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['avalaibility']	= 'Dostępny: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['edition']	= 'Wydanie: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydania']['series']	= 'Seria: ';
?>