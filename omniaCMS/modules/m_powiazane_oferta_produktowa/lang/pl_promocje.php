<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - promocje
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['no_data'] = 'W chwili obecnej nie posiadamy żadnych produktów w promocji.';

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['shipment'] = "Dostępność";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['shipment_preview'] = "Dostępność od";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['isbn']= 'ISBN: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['shipment_3'] = "na zamówienie";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['publisher']	= 'Wydawnictwo: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['avalaibility']	= 'Dostępny: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['edition']	= 'Wydanie: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['series']	= 'Seria: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['binding'] = "Oprawa";
?>