<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - serie wydawnicze
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['no_data'] = 'W chwili obecnej nie posiadamy zdefiniowanych serii wydawniczych.';


// pager
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['current_page']	= 'strona';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['from'] = 'z';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['total_items']	= 'wszystkich wydawnictw: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['page'] = 'Strona';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['page_dots'] = 'Strona:';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['pages'] = 'Strony: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['first_page'] = 'Pierwsza strona';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['previous_page'] = 'Poprzednia strona';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['next_page'] = 'Następna strona';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['last_page'] = 'Ostatnia strona';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['per_page'] = 'Rekordów na stronie:';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_wydawcy']['pager']['sites'] = 'Stron ';

?>