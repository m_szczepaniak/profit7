<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - indeks rzeczowy
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_index']['no_data'] = 'W chwili obecnej nie posiadamy zdefiniowanego indeksu rzeczowego.';
?>