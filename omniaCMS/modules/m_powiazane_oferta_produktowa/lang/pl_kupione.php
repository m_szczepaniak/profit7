<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - bestsellery
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['no_data'] = 'W chwili obecnej nie posiadamy żadnych nowości.';

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['language'] = "Język książki";

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['shipment'] = "Dostępność";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['isbn']= 'ISBN: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['shipment_3'] = "na zamówienie";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['publisher']	= 'Wydawnictwo: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['avalaibility']	= 'Dostępny: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['edition']	= 'Wydanie: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_kupione']['alternate_name']	= 'Nowości z działu:';

?>