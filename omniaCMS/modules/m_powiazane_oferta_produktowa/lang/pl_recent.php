<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - ostatnio odwiedzne
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_recent']['no_data'] = 'Nie przeglądałeś jeszcze żadnych produktów.';
?>