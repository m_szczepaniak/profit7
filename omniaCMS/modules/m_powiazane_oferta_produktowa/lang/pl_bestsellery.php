<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - bestsellery
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['no_data'] = 'W chwili obecnej nie posiadamy żadnych bestsellerów.';

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['language'] = "Język książki";

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['shipment'] = "Dostępność";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['shipment_preview'] = "Dostępność od";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['isbn']= 'ISBN: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['shipment_3'] = "na zamówienie";
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['publisher']	= 'Wydawnictwo: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['avalaibility']	= 'Dostępny: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['edition']	= 'Wydanie: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['series']	= 'Seria: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa_bestsellery']['binding'] = "Oprawa";
?>