<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - pakiety
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_pakiety']['no_data'] = 'W chwili obecnej nie posiadamy żadnych pakietów.';
?>