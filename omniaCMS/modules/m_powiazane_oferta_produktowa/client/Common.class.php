<?php
/**
 * Klasa Common_m_oferta_produktowa do obslugi modulu i jego boksow oferty produktowej
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Common_m_powiazane_oferta_produktowa {
    
  /**
   * Konstruktor klasy
   */
	function Common_m_powiazane_oferta_produktowa() {

	} // end Common_m_oferta_produktowa() function
	
	/**
	 * Metoda pobiera nazwe strony oferty produktowej
	 * 
	 * @param	integer	$iId	- Id strony
	 * @return	string - nazwa strony
	 */
	function getName($iId){
		global $aConfig;
		
		$sSql = "SELECT name
						FROM ".$aConfig['tabls']['prefix']."menus_items
						WHERE id = ".$iId."";						
		return Common::GetOne($sSql);
	}
	
	/**
	 * Metoda na podstawie filtrow oraz zmienych sortowania tworzy link
	 * Jezeli $ForPager true i istnieje $_GET['sorder'] nastepuje odwrocenie
	 * kolejnosci sortowania, np. z ASC na DESC
	 * 
	 * @param	bool	$bSorter	- true: dodaj sorter; false: bez sortera
	 * @return	string	- link
	 */
	function getLink($bSorter=false) {
		$sLink = '';
		if ($bSorter) {
			if (isset($_GET['sort'])) {
				$sLink .= 'sort:'.$_GET['sort'].',';
			}
		}
		return $sLink;
	} // end of getLink() method
	
	

} // end of Common_m_oferta_produktowa Class
?>