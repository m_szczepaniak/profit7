<?php
/**
 * Klasa odule do obslugi autorow
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2008
 * @version   1.0
 */
 

class Module {
  
  // Id strony
  var $iPageId;
  
  // katalog szablonow
  var $sTemplatesPath;
  
  // symbol modulu
  var $sModule;
  var $sOption;
  
  // ustawienia strony autorow
  var $aSettings;
  
  // link do strony
  var $sPageLink;
  
  // czesc nazwy pliku cache - na podstawie sciezki URI
  var $sCacheName;
    
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;
		
		$this->sCacheName = md5($_SERVER['REQUEST_URI']);
		$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->sOption = $aConfig['_tmp']['page']['moption'];
		$this->aSettings =& $this->getSettings();
		$this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/';
		$this->aSettings['list_template']='autorzy_lista.tpl';
	} // end Module() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla strony autorow
	 * 
	 * @return	array
	 */
	function getSettings() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aCfg = unserialize($oCache->get(md5($aConfig['_tmp']['page']['symbol']), 'modules_settings')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT * 
							 FROM ".$aConfig['tabls']['prefix']."products_settings 
							 WHERE page_id = ".$this->iPageId;
							 
			$aCfg = Common::GetRow($sSql);
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aCfg), md5($aConfig['_tmp']['page']['symbol']), 'modules_settings');
		}		
		return $aCfg;
	} // end of getSettings() function
	
	
	/**
	 * Metoda pobiera tresc strony o id $this->iPageId
	 * dowiazuje ja do Smartow oraz zwraca xHTML z trescia strony po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty, $oCache;
		$sHtml = '';
			$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule.'_'.$this->sOption];
		
		if (!$aConfig['common']['caching'] || false === ($sHtml = $oCache->get($this->sCacheName, 'modules'))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie i przetworzenie przez szablon listy autorow
			$aModule['page_link'] =& $this->sPageLink;
		
			// wyswietlenie listy autorow
			$aModule['letters'] =& $this->getLetters();
			
			$sLetter = $_GET['action'];
			if(empty($sLetter)) $sLetter = "a";
			
			$aModule['items'] = $this->getItems($sLetter);
			
			if (!empty($aModule['items'])) {
				$pSmarty->assign_by_ref('aModule', $aModule);
				$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.
																 $this->aSettings['list_template']);
				$pSmarty->clear_assign('aModule', $aModule);
			}
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save($sHtml, $this->sCacheName, 'modules');
		}
		if (trim($sHtml) == '') {
			setMessage($aModule['lang']['no_data']);
		}
		//dump($aModule);
		return $sHtml;
	} // end of getContent()

	
	function &getLetters(){
		global $aConfig;
		
		$sSql = "SELECT distinct(index_letter) AS letter
						FROM ".$aConfig['tabls']['prefix']."products_authors
						ORDER BY index_letter";
						
		//$aData =& Common::GetAll($sSql);
		$aUsed =& Common::GetCol($sSql);
		
		$aLetters=array();
		$iNr=0;
		for($sLetter=ord('A');$sLetter<=ord('Z');$sLetter++){
			$aLetters[$iNr]['letter'] = chr($sLetter);
			if(in_array(chr($sLetter),$aUsed)) $aLetters[$iNr]['link'] = createLink($this->sPageLink, urlencode(strtolower($aLetters[$iNr]['letter'])));
			$iNr++;
		}
	
		
//		foreach($aData as $iKey => $aValue){
//			$aData[$iKey]['letter'] = $aValue['letter'];
//			$aData[$iKey]['link'] = createLink($this->sPageLink, urlencode(strtolower($aData[$iKey]['letter'])));
//		}		
		return $aLetters;
	}
	
	/**
	 * Metoda pobiera liste autorow
	 *
	 * @return	array	- lista autorów
	 */
	function &getItems($sLetter) {
		global $aConfig;
		$iPerPage = 0;
		$aItems = array();

		// okreslenie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(C.id)
						 FROM ".$aConfig['tabls']['prefix']."products_authors C
						 WHERE C.index_letter = ".Common::Quote($sLetter); //AND (SELECT COUNT(A.product_id) FROM ".$aConfig['tabls']['prefix']."products_to_authors A JOIN ".$aConfig['tabls']['prefix']."products B ON B.id = A.product_id WHERE A.author_id = C.id AND B.published = '1') > 0";
//dump($sSql);
		if (($iRecords = (int) Common::GetOne($sSql)) > 0) {
			// okreslenie liczby rekordow na stronie
			$iPerPage = (int) $this->aSettings['items_per_page'];

			// zapytanie wybierajace liste rekordow
			$sSql = "SELECT A.id, CONCAT(A.surname,' ',A.name) AS name
						 	 FROM ".$aConfig['tabls']['prefix']."products_authors A
						 	 WHERE A.index_letter = ".Common::Quote($sLetter)." ORDER BY A.surname,A.name"; //AND (SELECT COUNT(B.product_id) FROM ".$aConfig['tabls']['prefix']."products_to_authors B JOIN ".$aConfig['tabls']['prefix']."products C ON C.id = B.product_id JOIN ".$aConfig['tabls']['prefix']."menus_items D ON D.id = C.page_id WHERE B.author_id = A.id AND D.published = '1' AND C.published = '1') > 0
							 
			
			if ($iPerPage > 0) {
				// uzycie pagera do stronicowania,
				include_once('Pager/CPager.class.php');
				$aPagerParams = array();
				$aPagerParams['perPage'] = $iPerPage;
				$aPagerParams['path'] = $aConfig['common']['base_url_http_no_slash'].$this->sPageLink.'/';
				$aPagerParams['fileName'] = urlencode($sLetter).',p%d.html';
				$aPagerParams['append'] = false;

				$oPager = new CPager($iRecords, $aPagerParams);
				$iStartFrom = $oPager->getStartFrom();

				// linki Pagera
				$aItems['pager']['links'] =& $oPager->getLinks('all');
				$aItems['pager']['total_pages'] =& $oPager->getLinks('total_pages');
				$aItems['pager']['total_items'] =& $oPager->getLinks('total_items');
				$aItems['pager']['current_page'] =& $oPager->getLinks('current_page');

				// dodanie wybrania zakresu do zapytania
				$sSql .= " LIMIT ".$iStartFrom.", ".$iPerPage;
			}
			
			// wybranie rekordow
			$aItems['items'] =& Common::GetAll($sSql);
			
			foreach ($aItems['items'] as $iKey => $aItem) {
				// utworzenie linku do rekordu
				$aItems['items'][$iKey]['link'] = getAuthorLink($aItem['name']);
			}
		}
		$aItems['letter'] = $sLetter;
		return $aItems;
	} // end of getItems() method
} // end of Module Class
?>