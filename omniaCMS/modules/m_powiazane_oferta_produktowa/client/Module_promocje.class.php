<?php
/**
 * Klasa odule do obslugi oferty produktowej - lista produktow z kategorii
 * karta produktu
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */

use LIB\Assets\additionalHead;

include_once('modules/m_oferta_produktowa/client/Common.class.php');

class Module extends Common_m_oferta_produktowa {
    
  // Id strony
  var $iPageId;
  
  // symbol modulu
  var $sModule;
  
  // opcja
  var $sOption;
  
  // ustawienia strony aktualnosci
  var $aSettings;
  
  // czesc nazwy pliku cache - na podstawie sciezki URI
  var $sCacheName;
    
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;
		//odpowiada za zmiane title dla podstrony - usuniecie efektu Bestselery Bestselery dział naukowy
		$aConfig['_tmp']['title']=$aConfig['_tmp']['page']['name'];
		
		$this->sCacheName = md5($_SERVER['REQUEST_URI']);
		$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->sOption = $aConfig['_tmp']['page']['moption'];
		$this->aSettings =& $this->getSettings();
		$this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
	} // end Module() function
	
	
	/**
	 * Metoda pobiera konfiguracje strony modulu
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aCfg = unserialize($oCache->get(md5($aConfig['_tmp']['page']['symbol']), 'modules_settings')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT * 
							 FROM ".$aConfig['tabls']['prefix']."products_settings 
							 WHERE page_id=".$this->iPageId;
			$aCfg = Common::GetRow($sSql);
			$aCfg['items_per_page'] = (int) $aCfg['items_per_page'];
			//$aCfg['show_page_id']=intval($aCfg['item_template']);
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aCfg), md5($aConfig['_tmp']['page']['symbol']), 'modules_settings');
		}
		return $aCfg;
	} // end of getSettings() function
	
	
	/**
	 * Metoda w zaleznosci od tego czy uzytkowik jest na stronie kategorii
	 * czy karcie produktu pobiera i wyswietla liste produktow z danej
	 * kategorii lub karte produktu
	 */
	function getContent() {
		global $aConfig, $pSmarty, $oCache;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule.'_'.$this->sOption];
		
		if (!$aConfig['common']['caching'] || false === ($sHtml = $oCache->get($this->sCacheName, 'modules'))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie i przetworzenie przez szablon listy promocyjnych produktow
			$aModule['page_link'] = $this->sPageLink;
			$aModule['rss_link'] = 'promotions';
			if(!empty($_GET['id'])){
				$aModule['name'] = $this->getPromoName($_GET['id']);
				setPageHeader($aModule['name']);
				//setSubTitle($aModule['name']);
				//odpowiada za zmiane title dla podstrony - usuniecie efektu Bestselery Bestselery dział naukowy
				$aConfig['_tmp']['title']=$aModule['name'];
				addToPath(array(createLink($this->sPageLink, 'id'.$_GET['id'], $aModule['name']),$aModule['name']));
        $aModule['page_link'] = createLink($this->sPageLink, 'id'.$_GET['id'], $aModule['name']);
			//	dump($aModule['name']);
			} else {
			$aModule['name'] = $aConfig['_tmp']['page']['name'];
			}
			// pobranie promocji
			$aModule['offer'] =& $this->getProductsList($_GET['id']);
			
			if (!empty($aModule['offer']['items'])) {
				$pSmarty->assign_by_ref('aModule', $aModule);
				$sHtml = $pSmarty->fetch('modules/'.$this->sModule.'/'.$this->aSettings['list_template']);
				$pSmarty->clear_assign('aModule', $aModule);
				
				// zapis do cache'a
				if ($aConfig['common']['caching'])
					$oCache->save($sHtml, $this->sCacheName, 'modules');
			}
		}
		if (trim($sHtml) == '')
			setMessage($aModule['lang']['no_data']);
		return $sHtml;
	} // end of getContent() method
	
	
	/**
	 * Metoda pobiera liste produktow
	 * 
	 * @param	string	$iPromoId	- id promocji
	 * @param	string	$sFiltersSql	- kod SQL filtrow
	 * @param	string	$sSorterSql	- kod SQL sortowania
	 * @return	array	- lista produktow
	 */
	function &getProductsList($iPromoId=0) {
		global $aConfig;
		$iPerPage = 0;
		$aRecords = array();
		$aIDs = array();
		$sSorterSql = $this->getSorterSql(' C.order_by ASC, B.order_by ASC, A.order_by DESC ');
		$sFilterSql = $this->getFilterSql();
		
		// okreslenie liczby wszystkich produktow
		$sSql = "SELECT COUNT(DISTINCT A.id)
						 FROM ".$aConfig['tabls']['prefix']."products_shadow A
							 JOIN ".$aConfig['tabls']['prefix']."products_tarrifs B
							 ON B.product_id=A.id
							 JOIN ".$aConfig['tabls']['prefix']."products_promotions C
							 ON C.id=B.promotion_id
							 WHERE B.start_date <= UNIX_TIMESTAMP() AND B.end_date >= UNIX_TIMESTAMP()
							  AND C.page_id = ".$this->iPageId.
								($iPromoId > 0?" AND B.promotion_id = ".$iPromoId:'').
								$sFilterSql;

		if (($iRecords = intval(Common::GetOne($sSql))) > 0) {
			// okreslenie liczby produktow na stronie
			$iPerPage = (int) $this->aSettings['items_per_page'];
			
			// zapytanie wybierajace liste produktow
			$sSql = "SELECT A.id, A.title AS name, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, 
											A.isbn, A.isbn_plain, A.publication_year, A.binding, A.packet, A.type,
											A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
					JOIN ".$aConfig['tabls']['prefix']."products_tarrifs B
					 ON B.product_id=A.id
					 JOIN ".$aConfig['tabls']['prefix']."products_promotions C
							 ON C.id=B.promotion_id
					 WHERE B.start_date <= UNIX_TIMESTAMP() AND B.end_date >= UNIX_TIMESTAMP()
					 AND C.page_id = ".$this->iPageId.
						($iPromoId > 0?" AND B.promotion_id = ".$iPromoId:'').
						$sFilterSql.
					 " GROUP BY A.id".
					 $sSorterSql;
		if ($iPerPage > 0) {
				// uzycie pagera do stronicowania
				include_once('Pager/CPager.class.php');
				//$aPagerParams = array('firstPageText' => $aConfig['lang']['pager']['first_page'], 'lastPageText' => $aConfig['lang']['pager']['last_page'], 'prevImg' => $aConfig['lang']['pager']['previous_page'], 'nextImg' => $aConfig['lang']['pager']['next_page']);
				$aPagerParams = array();
				$aPagerParams['perPage'] = $iPerPage;
				if ($aConfig['common']['mod_rewrite']) {
					$aPagerParams['path'] = $aConfig['common']['base_url_http_no_slash'].$this->sPageLink;
					$aPagerParams['append'] = false;
					$aReplace=array('%', ' ', 'ą', 'ż', 'ś','ć','ń');
					$aTReplace=array('-', '-', 'a', 'z', 's','c','n');
          if ($iPromoId>0) {
            $aPagerParams['fileName'] = ($iPromoId>0?('id'.$iPromoId.','.'p%d,'.$this->getLink(true).str_replace($aReplace, $aTReplace, Common::GetOne('SELECT name FROM products_promotions WHERE id='.$iPromoId))):'p%d').'.html';
              if (isset($_GET['sort']) || isset($_GET['filter'])) {
                  // sortujemy/filtrujemy
                  $additionalHead = new additionalHead();
                  $sLinkPage = ('id'.$iPromoId.','.'p%d,'.str_replace($aReplace, $aTReplace, Common::GetOne('SELECT name FROM products_promotions WHERE id='.$iPromoId))).'.html';
                  $baseLink = $this->getBaseLinkPromotion($this->sPageLink, $sLinkPage);
                  $additionalHead->addHeadLinkRel('canonical', $baseLink);
              }
          }
          else {
            $aPagerParams['fileName'] = $this->getLink(true).($iPromoId>0?('id'.$iPromoId.','.'p%d,'.str_replace($aReplace, $aTReplace, Common::GetOne('SELECT name FROM products_promotions WHERE id='.$iPromoId))):'p%d').'.html';
          }
					//$aPagerParams['fileName']=str_replace($aReplace, $aTReplace, $aPagerParams['fileName']);
				}
				else {
					if (count($_SESSION['_settings']['lang']['versions']['symbols']) === 1) {
						$aPagerParams['excludeVars'][] = 'lang';
					}
				}
				$oPager = new CPager($iRecords, $aPagerParams);

				$iStartFrom = $oPager->getStartFrom();
				$sSql .= " LIMIT ".$iStartFrom.",".$aPagerParams['perPage']."";

				$aRecords['items'] =& Common::GetAll($sSql);
				
				// dodanie linek Pagera do Smartow
				$aRecords['pager']['links'] =& $oPager->getLinks('all');
				$aRecords['pager']['total_pages'] =& $oPager->getLinks('total_pages');
				$aRecords['pager']['total_items'] =& $oPager->getLinks('total_items');
				$aRecords['pager']['current_page'] =& $oPager->getLinks('current_page');
			}
			else {
				// brak stronicowania archiwum
				$aRecords['items'] =& Common::GetAll($sSql);
			}
			include_once('FreeTransportPromo.class.php');
			$oFreeTransportPromo = new Lib_FreeTransportPromo();
			
			foreach ($aRecords['items'] as $iKey => $aItem) {
                //pobranie cennika
				$aRecords['items'][$iKey]['tarrif'] = $this->getTarrif($aItem['id']);
				$aRecords['items'][$iKey]['tarrif']['packet'] = ($aItem['packet']=='1');
				// przeliczenie i formatowanie cen
				$aRecords['items'][$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aRecords['items'][$iKey]['price_brutto'], $aRecords['items'][$iKey]['tarrif'], $aRecords['items'][$iKey]['promo_text'],$aItem['discount_limit'], $this->getSourceDiscountLimit($aItem['id'])));
				$aRecords['items'][$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']);

			    // pobranie info o wydawcy
//				if($aItem['publisher_id']){
//					$aRecords['items'][$iKey]['publisher'] = $this->getPublisher($aItem['publisher_id']);
//				}
                $pShipmentTime = new \orders\Shipment\ShipmentTime();
                $aRecords['items'][$iKey]['image'] =  $this->getImages($aItem['id'], true);
                $aRecords['items'][$iKey]['authors'] =$this->getAuthors($aItem['id']);
                $aRecords['items'][$iKey]['link'] = $aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['name']);
                $aRecords['items'][$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aItem['id'], 'add');
                $aRecords['items'][$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aItem['id'],'add_to_repository,');
                $aRecords['items'][$iKey]['shipment']=$pShipmentTime->getShipmentTime($aItem['id'], $aItem['shipment_time']);
                $aRecords['items'][$iKey]['shipment_date'] = FormatDate($aItem['shipment_date']);
                $aRecords['items'][$iKey]['free_delivery'] = $oFreeTransportPromo->getLogoNameBySymbol($oFreeTransportPromo->getBookFreeTransport($aItem['id'], $aRecords['items'][$iKey]['promo_price']));
            }
		}
		return $aRecords;
	} // end of getProductsList() method
	
	/**
	 * Pobiera nazwę promocji
	 * @param $iId - id promocji
	 * @return string - nazwa promocji
	 */
	function getPromoName($iId){
		global $aConfig;
		
		$sSql="SELECT A.name
					FROM ".$aConfig['tabls']['prefix']."products_promotions A
					WHERE A.id = ".$iId;
		return Common::GetOne($sSql);
	}
} // end of Module Class
?>