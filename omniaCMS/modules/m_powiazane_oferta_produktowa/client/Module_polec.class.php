<?php
/**
 * Klasa odule do obslugi oferty produktowej - lista produktow z kategorii
 * karta produktu
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */

include_once('modules/m_powiazane_oferta_produktowa/client/Common.class.php');

class Module extends Common_m_powiazane_oferta_produktowa {
  
  // Id strony
  var $iPageId;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // symbol modulu
  var $sModule;
  
  // opcja
  var $sOption;
  
  // ustawienia strony aktualnosci
  var $aSettings;
    
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;
		
		$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->sOption = $aConfig['_tmp']['page']['moption'];
		$this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/polec/';		
	} // end Module() function
	
	
	/**
	 * Metoda w zaleznosci od tego czy uzytkowik jest na stronie kategorii
	 * czy karcie produktu pobiera i wyswietla liste produktow z danej
	 * kategorii lub karte produktu
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];

		switch($_GET['action']){
			case 'send':
				$aModule['step'] = 2;
				$this->sendMessage($_POST['id']);
			break;
			
			default:
				$aModule['step'] = 1;
				$aModule['image'] = "<img src='/images/okladki/".$_GET['id'].".gif' alt ='' />";
				$aModule['item_name'] = $this->getItemName($_GET['id']);				
				
				// dolaczenie klasy FormTable
				include_once('Form/FormTable.class.php');
				$oForm = new FormTable('polec_znajomemu', '', array('action' => createLink($this->sPageLink, '', '/send')));
		
				$aForm['fields']['id'] = $oForm->GetHiddenHTML('id', $_GET['id']);
		
				$aForm['fields']['s_mail']['label'] = $oForm->GetLabelHTML('s_mail', $aModule['lang']['s_mail'], true);
				$aForm['fields']['s_mail']['input'] = $oForm->GetTextHTML('s_mail', $aModule['lang']['s_mail'], isset($_SESSION['w_user']['email']) ? $_SESSION['w_user']['email'] : '', array('maxlength' => 64), '', 'email', true);
		
				$aForm['fields']['s_name']['label'] = $oForm->GetLabelHTML('s_name', $aModule['lang']['s_name'], true);
				$aForm['fields']['s_name']['input'] = $oForm->GetTextHTML('s_name', $aModule['lang']['s_name'], isset($_SESSION['w_user']['user_name']) ? $_SESSION['w_user']['user_name'] : '', array('maxlength' => 64), '', 'text', true);
		
				$aForm['fields']['r_mail']['label'] = $oForm->GetLabelHTML('r_mail', $aModule['lang']['r_mail'], true);
				$aForm['fields']['r_mail']['input'] = $oForm->GetTextHTML('r_mail', $aModule['lang']['r_mail'], '', array('maxlength' => 64), '', 'email', true);
				
			 	$aForm['fields']['description']['label'] = $oForm->GetLabelHTML('description', $aModule['lang']['description'], false);
				$aForm['fields']['description']['input'] = $oForm->GetTextAreaHTML('description', $aModule['lang']['description'], '', array(), '', '', '', '', false);
				
				$aForm['header'] = $oForm->GetFormHeader();
				$aForm['footer'] = $oForm->GetFormFooter();
				$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																										 $oForm->GetErrorPrefix(),
																										 array('id' => 'aiF__ErrPrefix'));
				$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																										 $oForm->GetErrorPostfix(),
																										 array('id' => 'aiF__ErrPostfix'));
				$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																										 $oForm->GetValidator(),
																										 array('id' => 'aiF__Validator'));
				$aForm['JS'] = $oForm->GetJS();
				
				$aModule['form'] =& $aForm;
			break;
			
		}
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch('modules/'.$this->sModule.'/polec/polec_znajomemu.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
				
		return $sHtml;
	} // end of getContent()


	/** 
	 *	Metoda zwraca nazwe ksiazki
	 *  
	 */
	function getItemName($iId){
		global $aConfig;
		
		$sSql = "SELECT name FROM ".$aConfig['tabls']['prefix']."products where id = ".$iId;
		
		return Common::GetOne($sSql);
	} // end of getItemName() method


	/**
	 * Metoda pobiera wszystkie informacje o ksiazce oraz wysyla maila
	 * 
	 */
	function sendMessage($iId) {
		global $aConfig, $pSmarty;
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		
		$aModule['logo_link'] = $aConfig['common']['base_url_http']."images/gfx/pl/mail_powiadomienie_logo.jpg";
		$aModule['s_name'] = $_POST['s_name'];
		$aModule['s_mail'] = $_POST['s_mail'];
		$aModule['description'] = trim($_POST['description']);
		$aModule['footer'] = $aConfig['_settings']['site']['mail_footer'];
		
		// sprawdzenie czy jest pakietem
		if (!$this->isPacket($iId)){		
			$aModule['item'] = $this->getProduct($iId);
		} else {
			$aModule['item'] = $this->getProduct($iId, true);
		}
		
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/mail_polec_znajomemu.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		
		$sTitle = $aConfig['lang']['mod_m_powiazane_oferta_produktowa']['mail_title'];

		Common::sendMail($_POST['s_name'], $aConfig['_settings']['site']['recommendations_email'], $_POST['r_mail'], $sTitle, $sHtml, true, $_POST['s_mail']);
	} // end of sendMessage() method
	
	
	/**
	 * Metoda pobiera produkt o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @param	boolean $bIsPacket	- czy jest pakietem
	 * @param	boolean	$bSmallThumb - czy najmniejsza miniaturka zdjecia
	 * @return	array	- dane produktu
	 */
	function &getProduct($iId, $isPacket = false, $bSmallThumb = false) {
		global $aConfig, $pSmarty;
		
		// zapytanie wybierajace dane produktu
		$sSql = "SELECT A.id, A.name, A.series_id, A.isbn, A.publication_year, A.pages, A.short_description, A.issue, B.name as series_name, C.symbol 
						 FROM ".$aConfig['tabls']['prefix']."products A
						 JOIN ".$aConfig['tabls']['prefix']."products_series B
						 ON A.series_id = B.id
						 JOIN ".$aConfig['tabls']['prefix']."menus_items C
						 ON A.page_id = C.id
						 WHERE A.id = ".$iId.
					 			 	 (!isPreviewMode() ? " AND A.published = '1'" : '');
					 			 	 
		$aRecord =& Common::GetRow($sSql);
		
		// jesli znaleziono
		if ($aRecord !== false) {			
			// link do produktu
			$aRecord['link'] = createLink($aConfig['common']['base_url_http'].$aRecord['symbol'], 'id'.$aRecord['id'], $aRecord['name']);
			
			// okladka produktu
			$aRecord['image'] = $aConfig['common']['base_url_http']."images/okladki/".$iId.".gif";	
			
			// indeks rzeczowy
			$aRecord['index'] = $this->getIndex($iId);

			// pobranie autorow produktu
			$aRecord['authors'] =& getProductAuthors($iId);

		}
		return $aRecord;
	} // end of getProduct() method

	
	/**
	 * Metoda sprawdza czy produkt o podanym Id jest pakietem
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @return	bool	- true: jest; false: nie
	 */
	function isPacket($iId) {
		global $aConfig;
		
		$sSql = "SELECT packet
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE 
						 			 id = ".$iId.
						 			 (!isPreviewMode() ? " AND published = '1'" : '');
		return intval(Common::GetOne($sSql)) != 0;
	} // end of isPacket() function
	
	
	/**
	 * Metoda pobiera nazwe indeksu rzeczowego
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @return	string - nazwa indeksu
	 */
	function getIndex($iId){
		global $aConfig;
		
		$sSql = "SELECT A.name
						 FROM ".$aConfig['tabls']['prefix']."products_index A
						 JOIN ".$aConfig['tabls']['prefix']."products_to_index B
						 ON A.id = B.index_id
						 WHERE B.product_id = ".$iId."";
						
		return Common::GetOne($sSql);
	} // end of getIndex() method
} // end of Module Class
?>