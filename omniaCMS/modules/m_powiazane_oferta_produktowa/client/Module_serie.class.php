<?php
/**
 * Klasa odule do obslugi oferty produktowej - lista serii wydawniczych
 * karta produktu
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */

class Module  {
    
  // Id strony
  var $iPageId;
  
  // symbol modulu
  var $sModule;
  
  // opcja
  var $sOption;
  
  // ustawienia strony aktualnosci
  var $aSettings;
  
  // czesc nazwy pliku cache
  var $sCacheName;
    
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;
		
		$this->sCacheName = md5($_SERVER['REQUEST_URI']);
		$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->sOption = $aConfig['_tmp']['page']['moption'];
		$this->aSettings =& $this->getSettings();
		$this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
	} // end Module() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla strony aktualnosci
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aCfg = unserialize($oCache->get($this->sCacheName, 'modules_settings')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT * 
							 FROM ".$aConfig['tabls']['prefix']."products_settings 
							 WHERE page_id=".$this->iPageId;
							 
			$aCfg = Common::GetRow($sSql);
			$aCfg['items_per_page'] = (int) $aCfg['items_per_page'];
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aCfg), $this->sCacheName, 'modules_settings');
		}
		return $aCfg;
	} // end of getSettings() function
	
	
	/**
	 * Metoda w zaleznosci od tego czy uzytkowik jest na stronie kategorii
	 * czy karcie produktu pobiera i wyswietla liste produktow z danej
	 * kategorii lub karte produktu
	 */
	function getContent() {
		global $aConfig, $pSmarty, $oCache;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule.'_'.$this->sOption];
		$aModule['logo_dir'] =$aConfig['common']['http_base_url'].$aConfig['common']['publishers_logos_dir'].'/';
	
		if (isset($_GET['id'])) {
			if (!$aConfig['common']['caching'] || false === ($sHtml = $oCache->get($this->sCacheName, 'modules'))) {
				// wylaczone cache'owanie lub danych nie ma w cache'u
				// pobranie i przetworzenie przez szablon listy serii wydawniczych
				$aModule['items'] =& $this->getSeries(intval($_GET['id']));
				$aModule['count'] = ceil(count($aModule['items'])/3);
				$aModule['name'] = $aConfig['_tmp']['page']['name'];
			
				$pSmarty->assign_by_ref('aModule', $aModule);
				$sHtml = $pSmarty->fetch('modules/'.$this->sModule.'/serie/'.$this->aSettings['item_template']);
				$pSmarty->clear_assign('aModule', $aModule);
				
				// zapis do cache'a
				if ($aConfig['common']['caching'])
					$oCache->save($sHtml, $this->sCacheName, 'modules');
			}
			if (trim($sHtml) == '')
				setMessage($aModule['lang']['no_data']);
		}else {
			if (!$aConfig['common']['caching'] || false === ($sHtml = $oCache->get($this->sCacheName, 'modules'))) {
				// wylaczone cache'owanie lub danych nie ma w cache'u
				// pobranie i przetworzenie przez szablon listy serii wydawniczych
				$aModule['items'] =& $this->getPublishers();
				$aModule['name'] = $aConfig['_tmp']['page']['name'];
			
				$pSmarty->assign_by_ref('aModule', $aModule);
				$sHtml = $pSmarty->fetch('modules/'.$this->sModule.'/serie/'.$this->aSettings['list_template']);
				$pSmarty->clear_assign('aModule', $aModule);
				
				// zapis do cache'a
				if ($aConfig['common']['caching'])
					$oCache->save($sHtml, $this->sCacheName, 'modules');
			}
			if (trim($sHtml) == '')
				setMessage($aModule['lang']['no_data']);
		}
		
		
		
		return $sHtml;
	} // end of getContent() method
	
	
	/**
	 * Metoda pobiera liste serii wydawniczych
	 * 
	 * @return	array ref
	 */
	function &getSeries($iId) {
	global $aConfig;
		
	
		$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."products_publishers
							 WHERE id = ".$iId;
		$aItems['publisher']= Common::GetRow($sSql);
	
		$sSql = "SELECT count(id)
						 FROM ".$aConfig['tabls']['prefix']."products_series
						 WHERE publisher_id = ".$iId."
						 GROUP BY publisher_id, name";
		if (($iRecords = (int) Common::GetOne($sSql)) > 0) {
			// okreslenie liczby rekordow na stronie
			$iPerPage = (int) $this->aSettings['items_per_page'];
			$sSql = "SELECT id, name
							 FROM ".$aConfig['tabls']['prefix']."products_series
							 WHERE publisher_id = ".$iId."
							 GROUP BY publisher_id, name
							 ORDER BY name
							 ";
			if ($iPerPage > 0) {
				// uzycie pagera do stronicowania,
				include_once('Pager/CPager.class.php');
				$aPagerParams = array();
				$aPagerParams['perPage'] = $iPerPage;
				$aPagerParams['path'] = $aConfig['common']['base_url_http_no_slash'].$this->sPageLink.'/';
				$aPagerParams['fileName'] = 'id'.$iId.',p%d,'.$aItems['publisher']['name'].'.html';
				$aPagerParams['append'] = false;
	
				$oPager = new CPager($iRecords, $aPagerParams);
				$iStartFrom = $oPager->getStartFrom();
	
				// linki Pagera
				$aItems['pager']['links'] =& $oPager->getLinks('all');
				$aItems['pager']['total_pages'] =& $oPager->getLinks('total_pages');
				$aItems['pager']['total_items'] =& $oPager->getLinks('total_items');
				$aItems['pager']['current_page'] =& $oPager->getLinks('current_page');
	
				// dodanie wybrania zakresu do zapytania
				$sSql .= " LIMIT ".$iStartFrom.", ".$iPerPage;
			}
			
			// wybranie rekordow
			$aItems['items'] =& Common::GetAll($sSql);
			
			foreach ($aItems['items'] as $iKey2 => $aValue){
				$aItems['items'][$iKey2]['link'] = getSeriesLink($aItems['publisher']['name'], $aValue['name']);
			}		
		} else {
			doRedirect(getPublisherLink($aItems['publisher']['name']));
		}
		//dump($aItems);
		return $aItems;
	} // end of getSeries() method
	
	/**
	 * Metoda pobiera liste wydawnictw
	 *
	 * @return	array	- lista wydawnictw
	 */
	function &getPublishers() {
		global $aConfig;
		$iPerPage = 0;
		$aItems = array();

		// okreslenie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(C.id)
						 FROM ".$aConfig['tabls']['prefix']."products_publishers C
						 WHERE C.show_series = '1' "; 

		if (($iRecords = (int) Common::GetOne($sSql)) > 0) {
			// okreslenie liczby rekordow na stronie
			$iPerPage = (int) $this->aSettings['items_per_page'];

			// zapytanie wybierajace liste rekordow
			$sSql = "SELECT A.id, A.name, A.photo, IF((SELECT count(id) FROM products_series WHERE publisher_id=A.id)>0, 1, 0) AS series
						 	 FROM ".$aConfig['tabls']['prefix']."products_publishers A
						 	 WHERE A.show_series = '1' ORDER BY A.name"; 
							 
			
			if ($iPerPage > 0) {
				// uzycie pagera do stronicowania,
				include_once('Pager/CPager.class.php');
				$aPagerParams = array();
				$aPagerParams['perPage'] = $iPerPage;
				$aPagerParams['path'] = $aConfig['common']['base_url_http_no_slash'].$this->sPageLink.'/';
				$aPagerParams['fileName'] = 'p%d.html';
				$aPagerParams['append'] = false;

				$oPager = new CPager($iRecords, $aPagerParams);
				$iStartFrom = $oPager->getStartFrom();

				// linki Pagera
				$aItems['pager']['links'] =& $oPager->getLinks('all');
				$aItems['pager']['total_pages'] =& $oPager->getLinks('total_pages');
				$aItems['pager']['total_items'] =& $oPager->getLinks('total_items');
				$aItems['pager']['current_page'] =& $oPager->getLinks('current_page');

				// dodanie wybrania zakresu do zapytania
				$sSql .= " LIMIT ".$iStartFrom.", ".$iPerPage;
			}
			
			// wybranie rekordow
			$aItems['items'] =& Common::GetAll($sSql);
			foreach ($aItems['items'] as $iKey => $aItem) {
				// utworzenie linku do rekordu
				if ($aItem['series'] == '1') {
					// wybierz liste serii
					$aItems['items'][$iKey]['link'] = createLink($this->sPageLink, 'id'.$aItem['id'], $aItem['name']);
				} else {
					// wybierz od razu elementy wydawcy
					$aItems['items'][$iKey]['link'] = getPublisherLink($aItem['name']);
				}
			}
		}
		return $aItems;
	} // end of getItems() method
} // end of Module Class
?>