<?php
/**
 * Klasa odule do obslugi oferty produktowej - nowosci
 * karta produktu
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
include_once('modules/m_oferta_produktowa/client/Common.class.php');

class Module extends Common_m_oferta_produktowa {
    
  // Id strony
  var $iPageId;
  
  // symbol modulu
  var $sModule;
  
  // opcja
  var $sOption;
  
  // ustawienia strony aktualnosci
  var $aSettings;
  
  // czesc nazwy pliku cache - na podstawie sciezki URI
  var $sCacheName;
  
  // czy cache'owac czy tez nie
  var $bDoCache;
  
  // rabat uzytkownika
  var $fUserDiscount;
    
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;

		$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->sOption = $aConfig['_tmp']['page']['moption'];
		$this->aSettings =& $this->getSettings();
		$this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
		$this->sCacheName = md5($_SERVER['REQUEST_URI']);
		$this->bDoCache = $aConfig['common']['caching'];
		if (isLoggedIn() && ($this->fUserDiscount = $_SESSION['w_user']['discounts']) > 0)
			$this->bDoCache = false;
	} // end Module() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla strony aktualnosci
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aCfg = unserialize($oCache->get(md5($aConfig['_tmp']['page']['symbol']), 'modules_settings')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT * 
							 FROM ".$aConfig['tabls']['prefix']."products_settings 
							 WHERE page_id=".$this->iPageId;
			$aCfg =& Common::GetRow($sSql);
			$aCfg['items_per_page'] = (int) $aCfg['items_per_page'];
			$aCfg['show_page_id']=intval($aCfg['item_template']);
		
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aCfg), md5($aConfig['_tmp']['page']['symbol']), 'modules_settings');
		}
		return $aCfg;
	} // end of getSettings() function
	
	
	/**
	 * Metoda w zaleznosci od tego czy uzytkowik jest na stronie kategorii
	 * czy karcie produktu pobiera i wyswietla liste produktow z danej
	 * kategorii lub karte produktu
	 */
	function getContent() {
		global $aConfig, $pSmarty, $oCache;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule.'_'.$this->sOption];

		if (!$this->bDoCache || false === ($sHtml = $oCache->get($this->sCacheName, 'modules'))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie i przetworzenie przez szablon listy nowosci
			$aModule['page_link'] = $this->sPageLink.'/id'.$_GET['id'].','.$_GET['action'];
			$aModule['rss_link'] = 'news';
			$aModule['name'] = $aConfig['_tmp']['page']['name'];
			// pobranie listy nowosci
			$aModule['offer'] =& $this->getProductsList(intval($_GET['id']));
			
			if (!empty($aModule['offer']['items'])) {
				$pSmarty->assign_by_ref('aModule', $aModule);
				$sHtml = $pSmarty->fetch('modules/'.$this->sModule.'/'.$this->aSettings['list_template']);
				$pSmarty->clear_assign('aModule', $aModule);
				
				// zapis do cache'a
				if ($this->bDoCache)
					$oCache->save($sHtml, $this->sCacheName, 'modules');
			}
		}
		if (trim($sHtml) == '')
			setMessage($aModule['lang']['no_data']);
		return $sHtml;
	} // end of getContent() method
	
	
	/**
	 * Metoda pobiera liste produktow
	 * 
	 * @param	string	$sPageLink	- link do strony kategorii oferty produktowej
	 * @param	string	$sFiltersSql	- kod SQL filtrow
	 * @param	string	$sSorterSql	- kod SQL sortowania
	 * @return	array	- lista produktow
	 */
	function &getProductsList($iId) {
		global $aConfig;
		$iPerPage = 0;
		$aRecords = array();
		$aIDs = array();
		$sSorterSql = $this->getSorterSql();
		$sFilterSql = $this->getFilterSql();
		
		$sSql = 	"SELECT id,plain_name 
							 FROM ".$aConfig['tabls']['prefix']."products
							 WHERE id = ".$iId;
		$aProduct=Common::GetRow($sSql);
		if(!empty($aProduct)){
			// okreslenie liczby wszystkich produktow
			$sSql = "SELECT COUNT(A.id)
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							 JOIN ".$aConfig['tabls']['prefix']."products_editions AS B
							 ON A.id = B.linked_id
							 WHERE B.product_id = ".$iId.
								$sFilterSql;
							 			 
			if (($iRecords = intval(Common::GetOne($sSql))) > 0) {
				// okreslenie liczby produktow na stronie
				$iPerPage = (int) $this->aSettings['items_per_page'];
				
				
				
				// zapytanie wybierajace liste produktow
			$sSql = "SELECT A.id, A.title AS name, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, 
											A.isbn, A.isbn_plain, A.publication_year, A.binding,
											A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews, A.packet, A.type
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							  JOIN ".$aConfig['tabls']['prefix']."products_editions AS B
								 ON A.id = B.linked_id
						 	 WHERE B.product_id = ".$iId.
								$sFilterSql.
								 $sSorterSql;

	
				if ($iPerPage > 0) {
					// uzycie pagera do stronicowania
					include_once('Pager/CPager.class.php');
					$aPagerParams = array();
					$aPagerParams['perPage'] = $iPerPage;
					$aPagerParams['path'] = $aConfig['common']['base_url_http_no_slash'].$this->sPageLink;
					$aPagerParams['append'] = false;
					$aPagerParams['fileName'] = 'id'.$_GET['id'].',p%d,'.$this->getLink(true).$aProduct['name'].'.html';
					
					$oPager = new CPager($iRecords, $aPagerParams);
					$iStartFrom = $oPager->getStartFrom();
					$sSql .= " LIMIT ".$iStartFrom.",".$aPagerParams['perPage']."";
									
					// dodanie linek Pagera do Smartow
					$aRecords['pager']['links'] =& $oPager->getLinks('all');
					$aRecords['pager']['total_pages'] =& $oPager->getLinks('total_pages');
					$aRecords['pager']['total_items'] =& $oPager->getLinks('total_items');
					$aRecords['pager']['current_page'] =& $oPager->getLinks('current_page');
				}
				// wybor rekordow
				$aRecords['items'] =& Common::GetAll($sSql);
				include_once('FreeTransportPromo.class.php');
				$oFreeTransportPromo = new Lib_FreeTransportPromo();
				foreach ($aRecords['items'] as $iKey => $aItem) {
					//pobranie cennika
				$aRecords['items'][$iKey]['tarrif'] = $this->getTarrif($aItem['id']);
				$aRecords['items'][$iKey]['tarrif']['packet'] = ($aItem['packet']=='1');
				// przeliczenie i formatowanie cen
				$aRecords['items'][$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aRecords['items'][$iKey]['price_brutto'], $aRecords['items'][$iKey]['tarrif'], $aRecords['items'][$iKey]['promo_text'],$aItem['discount_limit'], $this->getSourceDiscountLimit($aItem['id'])));
				$aRecords['items'][$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']);

				// pobranie info o wydawcy
//				if($aItem['publisher_id']){
//					$aRecords['items'][$iKey]['publisher'] = $this->getPublisher($aItem['publisher_id']);
//				}

                    $pShipmentTime = new \orders\Shipment\ShipmentTime();
					$aRecords['items'][$iKey]['image'] =  $this->getImages($aItem['id'], true);
					$aRecords['items'][$iKey]['authors'] =$this->getAuthors($aItem['id']);
					$aRecords['items'][$iKey]['link'] = $aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['name']);
					$aRecords['items'][$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aItem['id'], 'add');
					$aRecords['items'][$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aItem['id'],'add_to_repository,');
					$aRecords['items'][$iKey]['shipment']=$pShipmentTime->getShipmentTime($aItem['id'], $aItem['shipment_time']);
					$aRecords['items'][$iKey]['shipment_date'] = FormatDate($aItem['shipment_date']);
					$aRecords['items'][$iKey]['free_delivery'] = $oFreeTransportPromo->getLogoNameBySymbol($oFreeTransportPromo->getBookFreeTransport($aItem['id'], $aRecords['items'][$iKey]['promo_price']));
				}
			}
		}
		
		return $aRecords;
	} // end of getProductsList() method
} // end of Module Class
?>