<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-30
 * Time: 13:11
 */

use DatabaseManager;
use FormTable;
use Smarty;

class Module
{
    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     * @param $pSmarty
     */
    public function __construct(&$pSmarty)
    {
        global $pDbMgr;

        $this->pSmarty = $pSmarty;
        $this->pDbMgr = $pDbMgr;

        if (isset($_FILES['file'])) {

            $this->generateFile($_FILES['file']);
        }

        if ($_GET['action']) {

            $this->locationGenerator();
            return;
        }
    }

    private function generateFile($file)
    {
        global $aConfig;

        $fileContent = explode("\n", file_get_contents($file['tmp_name']));

        $newData = [
            ['indeks', 'nazwa produktu', 'ilość', 'lokalizacja 1', 'lokalizacja 2', 'lokalizacja rem.']
        ];

        foreach($fileContent as $element) {

            $elementData = preg_replace('/\s+/', '', $element);
            $elementData = explode(',', $element);
            $indents = $this->findProductByIdents($elementData[0]);
            $indents['quantity'] = $elementData[1];
//            P.name, PS.profit_g_location, PS.profit_x_location, INV.container_id as rem_loc
            $final = [
                $indents[0],
                $indents['name'],
                $indents['quantity'],
                $indents['profit_g_location'],
                $indents['profit_x_location'],
                $indents['rem_loc'],
            ];

            $newData[] = $final;
        }

        $this->array_to_csv_download($newData);
        exit;
    }

    function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {

        ob_clean();
        ob_start();

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');

        // open the "output" stream
        // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
        $f = fopen('php://output', 'w');

        foreach ($array as $line) {
            fputcsv($f, $line, $delimiter);
        }
    }

    private function locationGenerator()
    {
        include_once('Form/FormTable.class.php');
        $form = new FormTable('form', '', ['enctype' => 'multipart/form-data']);
        $form->AddFile('file', 'Załaduj plik');
        $form->AddInputButton('save', 'Wygeneruj plik');

        $this->pSmarty->assign('sContent', $form->ShowForm());
    }

    function findProductByIdents($sProdIdent) {

        global $pDbMgr;

        $sSql = 'SELECT P.name, PS.profit_g_location, PS.profit_x_location, INV.container_id as rem_loc
             FROM products AS P
             JOIN products_stock AS PS
              ON P.id = PS.id
             LEFT JOIN inventory_items AS II ON P.id = II.product_id
             LEFT JOIN inventory AS INV ON II.inventory_id = INV.id
             WHERE
                (
                  P.isbn_plain = "%1$s" OR
                  P.isbn_10 = "%1$s" OR
                  P.isbn_13 = "%1$s" OR
                  P.ean_13 = "%1$s"
                )'; // OR streamsoft_indeks = "%1$s"
        $sSql = sprintf($sSql, $sProdIdent);

        $res = $pDbMgr->GetRow('profit24', $sSql);
        array_unshift($res , $sProdIdent);

        return $res;
    }
}