<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 08.02.18
 * Time: 14:31
 */


namespace omniaCMS\modules\m_zamowienia_magazyn;

use Admin;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\MagazineReturn;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\EntityManager\Entites\StockLocation;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use LIB\orders\listType\filters\checkMagazineZD;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;

include_once('modules/m_zamowienia_magazyn_zbieranie_tramwaj/Common_get_ready_orders_books.class.php');

class Module__zamowienia_magazyn__return extends \Common_get_ready_orders_books implements ModuleEntity, SingleView
{
    protected $orderItemsLists;
    protected $stockLocation;
    protected $stockLocationOrdersItemsListsItems;


    /**
     *
     * @var DatabaseManager
     */
    protected $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    private $sModule;


    /**
     * Module__zamowienia_magazyn__packages constructor.
     * @param Admin $oClassRepository
     */
    public function __construct(Admin $oClassRepository)
    {
        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
        $this->stockLocation = new StockLocation($this->pDbMgr);
        $this->orderItemsLists = new OrdersItemsLists($this->pDbMgr);
    }

    /**
     *
     */
    public function doDefault()
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Zwroty')),
            'refresh' => true,
            'search' => false,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'number',
                'content' => _('Numer dok.'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'contractor_name',
                'content' => _('Kontrahent'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'magazine_type',
                'content' => _('Zacznij zdejmować z magazynu'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'count',
                'content' => _('Ilość egz. produktów'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'confirmed_quantity',
                'content' => _('Potwierdzona ilość'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'status',
                'content' => _('Status'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created_by',
                'content' => _('przez'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '55'
            )

        );

        $pView = new \View('magazine_packages', $aHeader, $aAttribs);

        $status = [
            [
                'label' => _('Aktywne'),
                'value' => '1'
            ],
            [
                'label' => _('Nieaktywne'),
                'value' => '0'
            ],
        ];
        $pView->AddFilter('f_status', _('Status'), addDefaultValue($status), $_POST['f_status']);
        // pobranie liczby wszystkich uzytkownikow

        $iRowCount = $this->getRecordsCount();
        if ($iRowCount == 0) {
            // resetowanie widoku
            resetViewState($this->sModule);
            // ponowne okreslenie liczny rekordow
            $iRowCount = $this->getRecordsCount(true);
        }

        $iCurrentPage = $pView->AddPager($iRowCount);
        $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
        $iStartFrom = ($iCurrentPage - 1) * $iPerPage;
        $aRecords = $this->getRecords($iStartFrom, $iPerPage);
        foreach ($aRecords as &$row) {
            $row = $this->parseViewItem($row);
        }
        $iRowCount = count($aRecords);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'name' => array(
                    'link' => phpSelf(array('do' => 'edit', 'id' => '{id}'))
                ),
                'action' => array(
                    'actions' => array('details', 'history'),
                    'params' => array(
                        'history' => array('id' => '{id}'),
                        'edit' => array('id' => '{id}'),
                        'details' => array('id' => '{id}'),
                        'close' => array('id' => '{id}')
                    ),
                    'icon' => array('details' => 'books',
                        'close' => 'activate',
                        'history' => 'details'
                    ),
                    'show' => false,
                )
            );
            if ($_SESSION['user']['type'] == '1' ||
                $_SESSION['user']['type'] == '2' ||
                $_SESSION['user']['name'] == 'mtopolowski' ||
                $_SESSION['user']['name'] == 'kkowalczyk'
            ) {
                $aColSettings['action']['actions'][] = 'edit';
            }
            if ($_SESSION['user']['name'] == 'agolba') {
                $aColSettings['action']['actions'][] = 'close';
            }
            $pView->AddRecords($aRecords, $aColSettings);
        }
        $pView->AddRecordsHeader($aRecordsHeader);
        $aRecordsFooter = array(array('add' => 'add'), array('add'));
//        $aFooterParams['add'][2] = 'add';
        $pView->AddRecordsFooter($aRecordsFooter, []);
        return $pView->Show();
    }

    public function doClose() {
        $this->sMsg .= GetMessage('DUPA0');
        $this->closeReturn($_GET['id']);
    }

    public function doHistory() {
        $aAddData = [];
        $returnId = $_GET['id'];

        $magazineReturn = new MagazineReturn($this->pDbMgr);
        $rows = $magazineReturn->getListReturnItems($returnId);


        include_once('View/View.class.php');

        // zapamietanie opcji
        rememberViewState($this->sModule);
        $aHeader = array(
            'header'	=> _('Szczegóły listy'),
            'refresh'	=> true,
            'search'	=> true
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 0,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'content'	=> '&nbsp;',
                'sortable'	=> false,
                'width'	=> '20'
            ),
            array(
                'db_field'	=> 'product_id',
                'content'	=> _('Id produktu'),
                'sortable'	=> true
            ),
            array(
                'db_field'	=> 'name',
                'content'	=> _('Nazwa'),
                'sortable'	=> true,
                'width'	=> '100'
            ),
            array(
                'db_field'	=> 'ean_13',
                'content'	=> _('EAN 13'),
                'sortable'	=> true,
                'width'	=> '100'
            ),
            array(
                'db_field'	=> 'quantity',
                'content'	=> _('Ilość do zwrotu'),
                'sortable'	=> true,
                'width'	=> '100'
            ),
            array(
                'db_field'	=> 'confirmed_quantity',
                'content'	=> _('Ilość potwierdzona'),
                'sortable'	=> true,
                'width'	=> '100'
            ),
            array(
                'db_field'	=> 'container_id',
                'content'	=> _('Lokalizacja'),
                'sortable'	=> true,
                'width'	=> '100'
            )
        );

        $pView = new \View('banners', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);
        $pView->AddRecords($rows, []);
        return $pView->Show();
    }

    /**
     * @return string
     */
    public function doDetails()
    {

        include_once('Form/FormTable.class.php');
        $aAddData = [];
        if (isset($_GET['ws']) && $_GET['ws'] != '') {
            $aAddData['ws'] = $_GET['ws'];
        }
        if (isset($_GET['type']) && $_GET['type'] != '') {
            $aAddData['type'] = $_GET['type'];
        }
        if (isset($_GET['is_double']) && $_GET['is_double'] != '') {
            $aAddData['is_double'] = $_GET['is_double'];
        }

        if (isset($_GET['magazine']) && $_GET['magazine'] != '') {
            $aAddData['magazine'] = $_GET['magazine'];
        }

        if (isset($_GET['is_available_employee']) && $_GET['is_available_employee'] != '') {
            $aAddData['is_available_employee'] = $_GET['is_available_employee'];
        }

        $pForm = new \FormTable('ordered_itm', _("Pobierz listę ") . $this->sListName, array('action' => phpSelf($aAddData), 'target' => '_blank'), array('col_width' => 355), true);
        $pForm->AddHidden('do', 'generate');
        $pForm->AddHidden('magazine_return_id', intval($_GET['id']));

        $aOptions = [
            [
                'value' => Magazine::TYPE_HIGH_STOCK_SUPPLIES,
                'label' => 'B'
            ],
            [
                'value' => Magazine::TYPE_LOW_STOCK_SUPPLIES,
                'label' => 'A'
            ],
        ];
        $pForm->AddRadioSet('magazine_type', _('Zbieraj zwrot z magazynu: '), $aOptions);


        $pForm->AddInputButton("generate", _('Pobierz listę'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
        return $pForm->ShowForm();
    }

    /**
     * @return string
     */
    public function doGenerate()
    {
        $aList = [];

        $magazineType = $_POST['magazine_type'];
        $magazineReturnId = intval($_POST['magazine_return_id']);


        $oGetOrderData = new \LIB\orders\listType\getOrderDataZD($this->pDbMgr, Magazine::TYPE_HIGH_STOCK_SUPPLIES);
        $aList = $oGetOrderData->getOrdersItemsListDataByOILId($magazineReturnId, false, $magazineType);
        // trzeba odpalić mechanizm dzielenia na lokalziacje

//        $this->stockLocation = new StockLocation($this->pDbMgr); //addOrderListReservation();
//        $aList = $this->stockLocation->getStockLocationReservation($aList, $magazineType);


        $returnSign = 'R_' . $magazineReturnId . '_' . $magazineType;
        return $this->getProductsScanner($this->pSmarty, $returnSign, $aList, '');
    }


    /**
     * @return bool
     */
    public function doSave_list_data()
    {
        include_once('Form/Validator.class.php');
        $_POST['search_isbn'] = '9788375069693';//fix
        $oValidator = new \Validator();
        if (!$oValidator->Validate($_POST)) {
            $sMsg = $oValidator->GetErrorString();
            $this->sMsg = GetMessage($sMsg, true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            AddLog($sMsg);
            echo '<script type="text/javascript">alert("' . $sMsg . '");</script>';
            return false;
        }

        $this->stockLocationOrdersItemsListsItems = new StockLocationOrdersItemsListsItems();


        $aBooksScanned = json_decode($_POST['books']);
        $aDeficienciesBooks = json_decode($_POST['not_scanned_books']);
        $returnSign = $_POST['orders_items_lists_id'];
        preg_match('/R_(\d+)_(\w+)/', $returnSign, $matches);
        $iReturnId = $matches[1];
        $magazineType = $matches[2];

        $this->pDbMgr->BeginTransaction('profit24');

        try {
            if (!empty($aBooksScanned)) {
                foreach ($aBooksScanned as $book) {
                    if ($book->currquantity == $book->quantity) {
                        $stockLocationOrders = $this->getStockLocationId($book->ident);
                        // ściągamy z ilości
                        // ilość powinnna być pełna z return_items
                        $sSql = 'SELECT MRI.quantity
                                 FROM magazine_return_items AS MRI
                                 WHERE MRI.id = ' . $book->ident;
                        $allQuantity = $this->pDbMgr->GetOne('profit24', $sSql);
                        $this->stockLocation->unreserveByStockLocationItems($stockLocationOrders['id'], $stockLocationOrders['stock_location_id'], $allQuantity);
                    }
                    if ($book->currquantity > 0) {
                        $sSql = 'UPDATE magazine_return_items AS MRI
                             SET confirmed_quantity = confirmed_quantity + "' . $book->currquantity . '"
                             WHERE MRI.id = ' . $book->ident;
                        $this->pDbMgr->Query('profit24', $sSql);
                    }
                }
            }

            if (!empty($aDeficienciesBooks)) {
                foreach ($aDeficienciesBooks as $book) {
                    if ($book->currquantity > 0) {
                        $sSql = 'UPDATE magazine_return_items AS MRI
                             SET confirmed_quantity = confirmed_quantity + "' . $book->currquantity . '"
                             WHERE MRI.id = ' . $book->ident;
                        $this->pDbMgr->Query('profit24', $sSql);
                    }
                }
            }
            $this->tryCloseList($iReturnId);
            $this->pDbMgr->CommitTransaction('profit24');
            echo '<script type="text/javascript">window.close();</script>';
        } catch (\Exception $ex) {
            $this->pDbMgr->RollbackTransaction('profit24');
            echo '<script type="text/javascript">alert("Wystąpił błąd podczas zapisu '.$ex->getMessage().'"); window.close();</script>';
        }
    }

    /**
     * @param $reservationId
     * @return array
     */
    private function getStockLocationId($reservationId)
    {

        $sSql = 'SELECT SLO.stock_location_id, SLO.id
                 FROM magazine_return_items AS MRI
                 JOIN stock_location_orders_items_lists_items SLO
                  ON MRI.stock_location_orders_items_lists_items_id = SLO.id
                 WHERE MRI.id = ' . $reservationId . '
                    AND SLO.status = "1"
                 ';
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }


    /**
     * @param $iStartFrom
     * @param $iPerPage
     * @return array
     */
    private function getRecordsCount($reset = false)
    {

        $sSql = 'SELECT 
                      count(R.id)
                 FROM magazine_return AS R
                 WHERE 1=1
                 ' . (false === $reset && isset($_POST['f_search']) && $_POST['f_search'] != '' ? ' AND R.number LIKE "%' . $_POST['f_search'] . '%"' : '') . '
                 ' . (false === $reset && isset($_POST['f_status']) && $_POST['f_status'] != '' ? ' AND R.status = "' . $_POST['f_status'] . '"' : '') . '
                 ORDER BY R.id DESC';

        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * @param $iStartFrom
     * @param $iPerPage
     * @return array
     */
    private function getRecords($iStartFrom, $iPerPage)
    {

        $sSql = 'SELECT 
                      R.id, 
                      R.number, 
                      R.contractor_name,
                      R.magazine_type,
                      (SELECT sum(RI.quantity) FROM magazine_return_items AS RI WHERE RI.magazine_return_id = R.id) AS count,
                      (SELECT sum(RI.confirmed_quantity) FROM magazine_return_items AS RI WHERE RI.magazine_return_id = R.id) AS confirmed_quantity, 
                      R.status,
                      R.created, 
                      R.created_by
                 FROM magazine_return AS R
                 WHERE 1=1
                 ' . (isset($_POST['f_search']) && $_POST['f_search'] != '' ? ' AND R.number LIKE "%' . $_POST['f_search'] . '%"' : '') . '
                 ' . (isset($_POST['f_status']) && $_POST['f_status'] != '' ? ' AND R.status = "' . $_POST['f_status'] . '"' : '') . '
                 ORDER BY R.id DESC
                 LIMIT '.$iStartFrom.', '.$iPerPage;

        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @return string
     */
    public function doAdd()
    {
        global $aConfig;

        include_once('Form/FormTable.class.php');
        $pForm = new \FormTable('magazine_return', _('Dodaj zwrot magazynowy'), array('enctype' => 'multipart/form-data'));

        $pForm->AddHidden('do', 'createReturn');
        $pForm->AddText('contractor_name', _('Kontrahent'));
        $pForm->AddText('number', _('Numer dokumentu'));
        $aOptions = [
            [
                'value' => Magazine::TYPE_HIGH_STOCK_SUPPLIES,
                'label' => 'B'
            ],
            [
                'value' => Magazine::TYPE_LOW_STOCK_SUPPLIES,
                'label' => 'A'
            ],
        ];
        $pForm->AddRadioSet('magazine_type', _('Rozpocznij zbieranie od magazynu: '), $aOptions);
        $pForm->AddFile('file_return', _('Wybierz plik ze zwrotem'), [], '', false);
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));
        return $pForm->ShowForm();
    }

    /**
     * @return string
     */
    public function doEdit()
    {
        global $aConfig;

        $returnId = $_GET['id'];

        include_once('Form/FormTable.class.php');
        $pForm = new \FormTable('magazine_return', _('Zweryfikuj zwrot'), array('enctype' => 'multipart/form-data'));
        $pForm->AddHidden('do', 'saveVerification');
        $pForm->AddHidden('magazine_return_id', $returnId);

        $sSql = 'SELECT MRI.*, SLO.stock_location_id, SLO.id, SL.container_id, P.ean_13, P.isbn_plain, P.name
                 FROM stock_location_orders_items_lists_items AS SLO
                 JOIN stock_location AS SL 
                  ON SLO.stock_location_id = SL.id AND SLO.stock_location_magazine_type = SL.magazine_type 
                 JOIN magazine_return_items AS MRI
                  ON MRI.stock_location_orders_items_lists_items_id = SLO.id
                 JOIN products AS P
                  ON MRI.product_id = P.id
                 WHERE MRI.magazine_return_id = ' . $returnId . '
                    AND SLO.status = "1"
                    AND MRI.quantity <> MRI.confirmed_quantity
                    ';
        $stockLocationItems = $this->pDbMgr->GetAll('profit24', $sSql);
        foreach ($stockLocationItems as $stockLocationItem) {
            $pForm->AddMergedRow('');
            $desc = ' Produkt EAN: '.$stockLocationItem['ean_13'].' ISBN: '.$stockLocationItem['isbn_plain'];
            $pForm->AddRow($stockLocationItem['name'], $desc);
            $pForm->AddText('stock_location_items[' . $stockLocationItem['id'] . ']', _('<span style="font-size: 25px;" class="location-style">'.$stockLocationItem['container_id'].'</span> Brakująca ilość: <span style="font-size:28px; color: green;">'.($stockLocationItem['quantity'] - $stockLocationItem['confirmed_quantity']).'</span>'), '', ['style' => 'font-size: 35px;']);
        }
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf() . '\');'), 'button'));
        return $pForm->ShowForm();
    }


    /**
     * @return string|void
     */
    public function doSaveVerification() {
        global $aConfig;

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new \Validator();
        if ($aConfig['common']['status'] != 'development') {
            if (!$oValidator->Validate($_POST) || $oValidator->sError != '') {
                $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
                // wystapil blad wypelnienia formularza
                // wyswietlenie formularza wraz z komunikatem bledu
                $this->doAdd();
                return;
            }
        }

        $this->pDbMgr->BeginTransaction('profit24');

        try {

            $returnId = $_POST['magazine_return_id'];
            // potwierdzamy wszystko jak leci
            $sSql = 'SELECT MRI.*, MRI.id AS MRI_ID, SLO.stock_location_id, SLO.id, SL.container_id, P.ean_13, P.isbn_plain, P.name, MRI.quantity, (MRI.quantity - MRI.confirmed_quantity) AS diff_quantity
                 FROM stock_location_orders_items_lists_items AS SLO
                 JOIN stock_location AS SL 
                  ON SLO.stock_location_id = SL.id AND SLO.stock_location_magazine_type = SL.magazine_type 
                 JOIN magazine_return_items AS MRI
                  ON MRI.stock_location_orders_items_lists_items_id = SLO.id
                 JOIN products AS P
                  ON MRI.product_id = P.id
                 WHERE MRI.magazine_return_id = ' . $returnId . '
                    AND SLO.status = "1"
                    AND MRI.quantity <> MRI.confirmed_quantity
                    ';
            $stockLocationItems = $this->pDbMgr->GetAll('profit24', $sSql);

            $bSendMail = false;
            $sEANQuantiy = '';
            $sEANQuantiy .= 'EAN 13;ISBN;PÓŁKA;ILOŚĆ DO ZEBRANIA;ILOŚĆ POTWIERDZONA' . "\r\n";
            foreach ($stockLocationItems as $stockLocationItem) {
                $confirmedValidation = intval($_POST['stock_location_items'][$stockLocationItem['id']]);
                $this->stockLocation->unreserveByStockLocationItems($stockLocationItem['id'], $stockLocationItem['stock_location_id'], $stockLocationItem['quantity']);
                if (intval($stockLocationItem['diff_quantity']) != $confirmedValidation) {

                    $sEANQuantiy .= $stockLocationItem['ean_13'] . ';' . $stockLocationItem['isbn_plain'] . ';' . $stockLocationItem['container_id'] . ';' . $stockLocationItem['quantity'] . ';' . ($stockLocationItem['confirmed_quantity'] + $confirmedValidation) . "\r\n";
                    $bSendMail = true;
                }
                if ($confirmedValidation > 0) {
                    $sSql = 'UPDATE magazine_return_items AS MRI
                             SET confirmed_quantity = confirmed_quantity + "' . $confirmedValidation . '"
                             WHERE MRI.id = ' . $stockLocationItem['MRI_ID'];
                    $this->pDbMgr->Query('profit24', $sSql);
                }
            }
            $this->pDbMgr->CommitTransaction('profit24');
            $this->sMsg = GetMessage(_('Zapisano weryfikację zwrotu'), false);
        } catch (\Exception $ex) {
            $this->pDbMgr->RollbackTransaction('profit24');
            $this->sMsg = GetMessage(_('Wystąpił błąd podczas zapisywania weryfikacji'.$ex->getMessage()));
        }

        $this->closeReturn($returnId);

        if (true === $bSendMail) {
            $this->sMsg .= GetMessage(_('Wysłano maila do k.malz z niezgodnościami'));
            $this->sMsg .= 'DANE: <br />'.nl2br($sEANQuantiy);
            \Common::sendMail('raporty', 'raporty@profit24.pl', 'a.golba@profit24.pl;k.malz@profit24.pl', 'Niezgodności na lokalizacjach', $sEANQuantiy);

        }
        return $this->doDefault();
    }


    /**
     * @param $returnId
     */
    public function tryCloseList($returnId) {
        $sSql = '
            SELECT id 
            FROM magazine_return_items 
            WHERE
              magazine_return_id = '.$returnId.'
              AND quantity > confirmed_quantity 
            LIMIT 1
';
        $notEqualReturnItems = $this->pDbMgr->GetOne('profit24', $sSql);
        if (intval($notEqualReturnItems) == 0) {
            $this->closeReturn($returnId);
        }
    }

    /**
     * @param $returnId
     * @return int
     */
    public function closeReturn($returnId) {
        $sSql = 'SELECT OILI.orders_items_lists_id
                 FROM magazine_return_items AS MRI
                 JOIN orders_items_lists_items AS OILI
                  ON MRI.orders_items_lists_items_id = OILI.id
                 WHERE MRI.magazine_return_id = '.$returnId.'
                 LIMIT 1 ';
        $iOILId = $this->pDbMgr->GetOne('profit24', $sSql);

        $value = [
            'closed' => '1'
        ];
        $this->pDbMgr->Update('profit24', 'orders_items_lists', $value, ' id = '.$iOILId);



        $sSql = 'UPDATE magazine_return
                 SET status = "0"
                 WHERE id = '.$returnId;
        return $this->pDbMgr->Query('profit24', $sSql);
    }


    /**
     * @return string|void
     */
    public function doCreateReturn()
    {
        global $aConfig;
        $bIsErr = false;


        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->doDefault();
            return;
        }

        $aData = $_POST;

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new \Validator();
        if ($aConfig['common']['status'] != 'development') {
            if (!$oValidator->Validate($_POST) || $oValidator->sError != '') {
                $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
                // wystapil blad wypelnienia formularza
                // wyswietlenie formularza wraz z komunikatem bledu
                $this->doAdd();
                return;
            }
        }

        $this->pDbMgr->BeginTransaction('profit24');
        $values = [
            'number' => $aData['number'],
            'contractor_name' => $aData['contractor_name'],
            'magazine_type' => $aData['magazine_type'],
            'status' => '1',
            'created' => 'NOW()',
            'created_by' => $_SESSION['user']['name']
        ];
        $returnId = $this->pDbMgr->Insert('profit24', 'magazine_return', $values);

        // dodajemy składowe zwrotu, rezerwujemy na lokalizacjach

        $filename = $_FILES['file_return']['tmp_name'];

        try {
            $this->parseProductFileCSV($filename, $aData['magazine_type'], $returnId);
            $this->pDbMgr->CommitTransaction('profit24');


            $this->sMsg .= GetMessage(_('Dodano nowy zwrot produktów'), false);
            return $this->doDefault();
        } catch (\Exception $ex) {
            $this->pDbMgr->RollbackTransaction('profit24');
            $this->sMsg .= GetMessage($ex->getMessage());
            return $this->doDefault();
        }
    }

    /**
     * @param $filename
     * @return string
     * @throws \Exception
     */
    private function parseProductFileCSV($filename, $magazineOrderType, $returnId)
    {

        $sumQuantityArrays = [];

        if (($handle = fopen($filename, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                if (count($data) != 2) {
                    throw new \Exception(_('Błędny format pliku: EAN;ILOŚĆ'));
                } else {
                    $ean13 = trim($data[0]);
                    $quantity = intval($data[1]);
                    if ($ean13 != '') {
                        $sumQuantityArrays[$data[0]] += $quantity;
                    }

                }
            }
            fclose($handle);

            $orderItemsListItems = [];
            foreach ($sumQuantityArrays as $ean13 => $quantity) {
                $product = $this->pDbMgr->getTableRow('products', $ean13, ['id'], 'profit24', 'streamsoft_indeks');
                if (!empty($product)) {
                    $orderItemsListItems[][0] = [
                        'product_id' => $product['id'],
                        'ean_13' => $ean13,
                        'quantity' => $quantity,
                        'source' => '51'
                    ];
                } else {
                    throw new \Exception(_('W pliku bazie danych nie znaleziono produktu o streamsoft_indeks: ' . $ean13));
                }
            }

            $filteredOrdersItems = $this->tryReserve($orderItemsListItems, $magazineOrderType);

            $this->orderItemsLists->addListItemsWithStockLocationReservation(OrdersItemsLists::RETURN_TYPE, $filteredOrdersItems, $returnId);
        } else {
            throw new \Exception(_('Błąd przesyłania pliku'));
        }

    }

    /**
     * @param $orderItemsListItems
     * @return array
     * @throws \Exception
     */
    private function tryReserve($orderItemsListItems, $magazineOrderType)
    {


        $checkMagazine = new checkMagazineZD($this->pDbMgr);
        $checkMagazine->setMagazine($magazineOrderType);
        $checkMagazine->reserveItem(true);
        $checkMagazine->forceReserveItems(true);
        $checkMagazine->addSellPredict(false);
        $checkMagazine->unsetIFNotExists(true);
        $checkMagazine->setOrdersItems($orderItemsListItems);
        $checkMagazine->applyFilter();
        $filteredOrdersItems = $checkMagazine->getFilteredOrdersItems();

        $arrayDiff = array_diff_assoc($orderItemsListItems, $filteredOrdersItems);
        if (!empty($arrayDiff)) {
            throw new \Exception(_('Brakuje ilości produktów na lokalizacjach : <q><blockquote>' . print_r($arrayDiff, true) . '</blockquote></q>'));
        } else {
            return $filteredOrdersItems;
        }
    }


    /**
     * @param array $aItem
     * @return array
     */
    public function parseViewItem(array $aItem)
    {
        if ($aItem['status'] == '1') {
            $aItem['status'] = _('Aktywne');
        } else {
            $aItem['status'] = _('Nieaktywne');
        }
        $aItem['magazine_type'] = Magazine::getMagazineNameByType($aItem['magazine_type']);
        return $aItem;
    }

    /**
     *
     */
    public function resetViewState()
    {
        resetViewState();
    }

    /**
     * @return string
     */
    public function getMsg()
    {
        $message = strip_tags($this->sMsg);
        if ($message != '') {
            AddLog($message);
        }
        return $this->sMsg;
    }

}