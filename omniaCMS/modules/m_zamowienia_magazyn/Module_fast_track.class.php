<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 08.02.18
 * Time: 14:31
 */


namespace omniaCMS\modules\m_zamowienia_magazyn;

use Admin;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\MagazineReturn;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\EntityManager\Entites\StockLocation;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use LIB\orders\listType\filters\checkMagazineZD;
use magazine\FastTrack\containerFastTrack;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;

include_once('modules/m_zamowienia_magazyn_zbieranie_tramwaj/Common_get_ready_orders_books.class.php');

class Module__zamowienia_magazyn__fast_track extends \Common_get_ready_orders_books implements ModuleEntity, SingleView
{
    protected $orderItemsLists;
    protected $stockLocation;
    protected $stockLocationOrdersItemsListsItems;


    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    private $sModule;


    /**
     * Module__zamowienia_magazyn__packages constructor.
     * @param Admin $oClassRepository
     */
    public function __construct(Admin $oClassRepository)
    {
        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    /**
     *
     */
    public function doDefault()
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Listy FastTrack')),
            'refresh' => true,
            'search' => false,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'name_surname',
                'content' => _('Użytkownik'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'container_id',
                'content' => _('Kuweta'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'open_datetime',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'closed',
                'content' => _('Czy kuweta zamknięta ?'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'close_datetime',
                'content' => _('Data zamknięcia'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '55'
            )

        );

        $pView = new \View('magazine_packages', $aHeader, $aAttribs);

        $status = [
            [
                'label' => _('Zamknięte'),
                'value' => '1'
            ],
            [
                'label' => _('Otwarte'),
                'value' => '0'
            ],
        ];
        $pView->AddFilter('f_closed', _('Status'), addDefaultValue($status), $_POST['f_closed']);
        // pobranie liczby wszystkich uzytkownikow

        $iRowCount = $this->getRecordsCount();
        if ($iRowCount == 0) {
            // resetowanie widoku
            resetViewState($this->sModule);
            // ponowne okreslenie liczny rekordow
            $iRowCount = $this->getRecordsCount(true);
        }

        $iCurrentPage = $pView->AddPager($iRowCount);
        $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
        $iStartFrom = ($iCurrentPage - 1) * $iPerPage;
        $aRecords = $this->getRecords($iStartFrom, $iPerPage);
        foreach ($aRecords as &$row) {
            $row = $this->parseViewItem($row);
        }
        $iRowCount = count($aRecords);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'user_id' => array(
                    'show' => false
                ),
                'action' => array(
//                    'actions' => array('details', 'close'),
                    'params' => array(
                        'details' => array('id' => '{id}'),
                        'close' => array('id' => '{id}', 'user_id' => '{user_id}')
                    ),
                    'icon' => array('details' => 'books',
                        'close' => 'activate',
                    ),
                    'show' => false,
                )
            );
            if ($_SESSION['user']['type'] == '1' ||
                $_SESSION['user']['type'] == '2' ||
                $_SESSION['user']['name'] == 'mtopolowski' ||
                $_SESSION['user']['name'] == 'kkowalczyk' ||
                $_SESSION['user']['name'] == 'ehejda' ||
                $_SESSION['user']['name'] == 'ablinska' ||
                $_SESSION['user']['name'] == 'plemieszko'
            ) {
                $aColSettings['action']['actions'][] = 'details';
                $aColSettings['action']['actions'][] = 'close';
            }
            $pView->AddRecords($aRecords, $aColSettings);
        }
        $pView->AddRecordsHeader($aRecordsHeader);
        $aRecordsFooter = array();
        $pView->AddRecordsFooter($aRecordsFooter, []);
        return $pView->Show();
    }

    /**
     * @return mixed
     */
    public function doClose() {

        $containerFastTrack = new containerFastTrack($this->pDbMgr, $_GET['user_id']);
        $fastTrackData = $containerFastTrack->useFastTrackById($_GET['id'], $_GET['user_id']);
        try {
            $containerFastTrack->closeConfirmFastTruck($fastTrackData);
            $msg = _('Zamknięto pomyślnie fast tracka '.$fastTrackData['container_id'].' użytkownika '.$_SESSION['user']['name']);
            $this->sMsg .= GetMessage($msg, false);
            AddLog($msg, false);
            return $this->doDefault();
        } catch (\Exception $ex) {
            $msg = $ex->getMessage();
            $this->sMsg .= GetMessage($msg);
            AddLog($msg);
            return $this->doDefault();
        }
    }

    /**
     * @return string
     */
    public function doDetails()
    {
        include_once('Form/FormTable.class.php');

        $containerFastTrack = new containerFastTrack($this->pDbMgr, $_GET['user_id']);
        $items = $containerFastTrack->getFastTruckItems($_GET['id']);

        $pForm = new \FormTable('ordered_itm', _("Książki w kuwecie fast trucka ".$items[0]['container_id']), array(), array('col_width' => 355), true);
        foreach ($items as $item) {
            $pForm->AddRow($item['number'], $item['name'].' - '.$item['isbn'] );
        }

        $pForm->AddInputButton('cancel', _('Powrót'),
                array('style'=>'float:left;','onclick'=>'window.location.href=\''.phpSelf().'\'; return false;'));


        return $pForm->ShowForm();
    }


    /**
     * @param bool $reset
     * @return array
     */
    private function getRecordsCount($reset = false)
    {

        $sSql = 'SELECT 
                      count(FT.id)
                 FROM fast_track AS FT
                 WHERE 1=1
                 ' . (false === $reset && isset($_POST['f_search']) && $_POST['f_search'] != '' ? ' AND FT.id LIKE "%' . $_POST['f_search'] . '%"' : '') . '
                 ' . (false === $reset && isset($_POST['f_closed']) && $_POST['f_closed'] != '' ? ' AND FT.closed = "' . $_POST['f_closed'] . '"' : '') . '
                 ORDER BY FT.id DESC';

        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * @param $iStartFrom
     * @param $iPerPage
     * @return array
     */
    private function getRecords($iStartFrom, $iPerPage)
    {

        $sSql = 'SELECT 
                      FT.id, 
                      (SELECT CONCAT(name, " ", surname) FROM users AS U WHERE U.id = FT.user_id) as name_surname, 
                      FT.container_id,
                      FT.open_datetime,
                      FT.closed,
                      FT.close_datetime,
                      FT.user_id
                      
                 FROM fast_track AS FT
                 WHERE 1=1
                 ' . (isset($_POST['f_search']) && $_POST['f_search'] != '' ? ' AND FT.id LIKE "%' . $_POST['f_search'] . '%"' : '') . '
                 ' . (isset($_POST['f_closed']) && $_POST['f_closed'] != '' ? ' AND FT.closed = "' . $_POST['f_closed'] . '"' : '') . '
                 ORDER BY FT.id DESC
                 LIMIT '.$iStartFrom.', '.$iPerPage;

        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * @param array $aItem
     * @return array
     */
    public function parseViewItem(array $aItem)
    {
        if ($aItem['closed'] == '1') {
            $aItem['closed'] = _('Zamknięta');
            $aItem['disabled'] = ['close'];
        } else {
            $aItem['closed'] = _('Otwarta');
        }
        return $aItem;
    }

    /**
     *
     */
    public function resetViewState()
    {
        resetViewState();
    }

    /**
     * @return string
     */
    public function getMsg()
    {
        $message = strip_tags($this->sMsg);
        if ($message != '') {
            AddLog($message);
        }
        return $this->sMsg;
    }

}