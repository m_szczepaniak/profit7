<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-30
 * Time: 13:11
 */

namespace omniaCMS\modules\m_zamowienia_magazyn;

use Admin;
use DatabaseManager;
use FormTable;
use LIB\EntityManager\Entites\Magazine;
use magazine\Containers;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;

class Module__zamowienia_magazyn__collecting_high_level implements ModuleEntity, SingleView
{

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    private $sModule;

    /**
     *
     * @param Admin $oClassRepository
     */
    public function __construct(Admin $oClassRepository)
    {
        global $aConfig;

        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }


    public function getMsg()
    {
        return $this->sMsg;
    }

    public function parseViewItem(array $aItem)
    {
    }

    public function resetViewState()
    {
        $this->resetViewState();
    }

    public function doDefault()
    {
        return $this->View();
    }

    /**
     * @param $sEAN
     * @return mixed
     */
    public function doEanLocation() {
        $aLocalizations = [];

        $this->pDbMgr->BeginTransaction('profit24');
        $sEAN = $_POST['ean'];
        $sLocation = $_POST['location'];

        // walidacja czy wysokie składowanie
        $conteiners = new Containers($this->pDbMgr, [2]);
        if ($conteiners->checkContainer($sLocation) != 2) {
            $sMsg = _('Podana lokalizacja jest nieprawidłowa, proszę podać lokalizację niskiego składowania.');
            $this->sMsg = GetMessage($sMsg, true);
            AddLog($sMsg, true);
            return $this->doDefault();
        }


        $sSqlSelect = 'SELECT id
									 FROM products
									 WHERE 
										isbn_plain = "%1$s" OR 
										isbn_10 = "%1$s" OR 
										isbn_13 = "%1$s" OR 
										ean_13 = "%1$s" OR 
                                        streamsoft_indeks = "%1$s"
                    ';
        $sSql = sprintf($sSqlSelect, $sEAN);
        $productId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($productId > 0) {
            $sSql = 'SELECT DISTINCT CHL.id, PS.id AS product_id, PS.profit_g_location
                     FROM collecting_high_level AS CHL
                     JOIN products_stock AS PS
                        ON PS.id = CHL.product_id
                     WHERE CHL.product_id = '.$productId .' 
                      AND (PS.profit_g_location = "'.$sLocation.'" OR PS.profit_g_location = "" OR PS.profit_g_location)
                      AND CHL.completed = "0" 
                      AND CHL.active = "0" 
                     ';
            $ids = $this->pDbMgr->GetAll('profit24', $sSql);
            if (empty($ids)) {
                $sMsg = _('Nie pasujące lokalizacja lub nie odnaleziono produktu');
                $this->sMsg = GetMessage($sMsg, true);
                AddLog($sMsg, true);
            } else {
                $data = $ids[0];
                if ('' == $data['profit_g_location']) {
                    $conteiners->updateProductLocation($data['product_id'], $sLocation);
                }

                foreach ($ids as $record) {
                    $iId = $record['id'];

                    $values = [
                        'completed' => '1'
                    ];
                    $this->pDbMgr->Update('profit24', 'collecting_high_level', $values, ' id = ' . $iId);

                    $aLoc = $this->getLinkedHighLevel($iId);
                    if (!empty($aLoc)) {
                        $aLocalizations = array_merge($aLoc, $aLocalizations);
                    }
                }

                $sMsgAdd = '';
                foreach ($aLocalizations as $locationData) {
                    $dec = '<li>Odłóż brak w ilości <strong>%s</strong> do kuwety <span class="location-style">%s</span> </li>';
                    $sMsgAdd .= sprintf($dec, $locationData['quantity'], $locationData['package_number']);
                }
                $sMsg = sprintf(_('Potwierdzono lokalizację niskiego składowania "%s" ean "%s" <br /><br /> <u>'), $sLocation, $sEAN).$sMsgAdd.'</ul>';
                $this->sMsg = GetMessage($sMsg, false);
                AddLog($sMsg, false);

//                if ('agolba' == $_SESSION['user']['name']) {
//                    $this->pDbMgr->RollbackTransaction('profit24');
//                }
            }

        } else {
            $sMsg = _('Nie znaleziono produktu');
            $this->sMsg = GetMessage($sMsg, true);
            AddLog($sMsg, true);
        }

        $this->pDbMgr->CommitTransaction('profit24');
        return $this->doDefault();
    }

    /**
     * @param $iId
     * @return array
     */
    private function getLinkedHighLevel($iId) {

        $sSql = 'SELECT OIL.package_number, CHLL.quantity
                 FROM collecting_high_level_lists AS CHLL
                 JOIN orders_items_lists AS OIL
                  ON OIL.id = CHLL.orders_items_lists_id
                 WHERE CHLL.collecting_high_level_id = '.$iId.'';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @return string
     */
    private function eanLocationConfirmForm()
    {

        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('ean_location', _('Potwierdź EAN / ПОДТВЕРДИТЬ '));
        $pForm->AddHidden('do', 'eanLocation');
        $pForm->AddText('ean', 'EAN / КОД КНИГИ', '', ['id' => 'ean_c', 'autofocus' => 'autofocus'], '', 'text', true);
        $pForm->AddText('location', 'Lokalizacja / НОМЕР ЛОКАЛИЗАЦИИ', '', ['id' => 'location_c'], '', 'text', true);
        $pForm->AddInputButton("submit", _('Zapisz'), array('style' => 'font-size: 12px; font-weight: bold; color: red;'), 'submit');

        $sJS = '
        <script>
            $( function() {
                $(document).keydown(function (e) {
                    if ($("#ean_c").is(":focus") === true) {
                        if (e.which === 13) {
                            $("#location_c").focus();
                            return false;
                        }
                    }
                    else {
                        if (false === $("#location_c").is(":focus") &&
                            false === $("#search").is(":focus")
                        ) {
                            $("#ean_c").focus();   
                        }
                    }
                });
            });
        </script>
        ';
        return $pForm->ShowForm() . $sJS;
    }

    /**
     * @return string
     */
    private function View()
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Braki na zbieraniu')),
            'refresh' => true,
            'search' => true,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        // DISTINCT CHL.id, P.name, P.ean_13, PS.profit_x_location, PS.profit_g_act_stock, CHL.active, GROUP_CONCAT(DISTINCT CONCAT(OIL.package_number, " - ", CHLL.quantity) SEPARATOR ", ") as packages_numbers, CHL.created, CHL.created_by
        $aRecordsHeader = array(
            array(
                'db_field' => 'name',
                'content' => _('Tytuł'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'ean_13',
                'content' => _('EAN 13'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'profit_g_act_stock',
                'content' => _('Lokalizacja niskiego skl.'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'profit_g_act_stock',
                'content' => _('Lokalizacja wysokiego skl.'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'profit_g_act_stock',
                'content' => _('Stan na G'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'completed',
                'content' => _('Przyjęte'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'collecting',
                'content' => _('Zbierane'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'collecting_datetime',
                'content' => _('Data zbierania'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'active',
                'content' => _('Zebrane'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'packages_numbers',
                'content' => _('Numery kuwet'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'destination_magazine',
                'content' => _('Magazyn do zebrania braku'),
                'sortable' => true,
                'width' => '180'
            ),

            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created_by',
                'content' => _('przez'),
                'sortable' => true,
                'width' => '180'
            )
//            array(
//                'content' => $aConfig['lang']['common']['action'],
//                'sortable' => false,
//                'width' => '55'
//            )

        );

        $pView = new \View('collecting_high_level', $aHeader, $aAttribs);
        // pobranie liczby wszystkich uzytkownikow
        $aRecords = $this->getRecords();
        $iRowCount = count($aRecords);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'type' => array(
                    'show' => false
                ),
                'name' => array(),
                'action' => array(
                    'actions' => array(),
                    'show' => false,
                )
            );
            /**
             '0' - zwykłe
             '1' - single
             '2' - wysyłka razem
             */
            foreach ($aRecords as &$aItem) {
                if ($aItem['active'] == '0') {
                    $aItem['name'] .= '<span class="parent_blue"></span>';
                }
                else if ($aItem['collecting'] == '1') {
                    $aItem['name'] .= '<span class="parent_orange"></span>';
                }
                switch ($aItem['type']) {
                    case '0':
                        $type = 'Z';
                        break;
                    case '1':
                        $type = 'S';
                        break;
                    case '2':
                        $type = 'W';
                        break;
                }
                $aItem['packages_numbers'] .= ' - '.$type;

                switch ($aItem['active']) {
                    case '0':
                        $aItem['active'] = 'TAK';
                        break;
                    case '1':
                        $aItem['active'] = 'NIE';
                        break;
                }

              $aItem['destination_magazine'] = Magazine::getMagazineNameByType($aItem['destination_magazine']);
            }
            $pView->AddRecords($aRecords, $aColSettings);
        }
        if (!isset($_POST['f_completed'])) {
            $data['f_completed'] = '1';
        }

        $opt = [
            [
                'label' => 'Nie',
                'value' => '0',
            ],
            [
                'label' => 'Tak',
                'value' => '1',
            ],
        ];
        $pView->AddFilter('f_completed', _('Przyjęte'), $opt, $_POST['f_completed']);
        $pView->AddFilter('f_collecting', _('Zbierane'), addDefaultValue($opt), $_POST['f_collecting']);
        $pView->AddFilter('f_created', _('Utworzono'), '', empty($_POST['f_created']) ? '00-00-0000' : $_POST['f_created'], 'date');

        $pView->AddRecordsHeader($aRecordsHeader);
        $pView->AddRecordsFooter($aRecordsFooter, []);

        $sJs = '
        <script>
        
            $(".parent_blue").parent().parent().css("background-color", "rgb(175, 238, 238)");
            $(".parent_orange").parent().parent().css("background-color", "rgb(255, 224, 176)");
</script>
        ';
        $form = $this->eanLocationConfirmForm();
        return $form . $pView->Show() . $sJs;
    }


    /**
     * @return array
     */
    private function getRecords()
    {
        $sSql = 'SELECT DISTINCT CHL.id, P.name, P.ean_13, PS.profit_g_location, PS.profit_x_location, PS.profit_g_act_stock, CHL.completed,
                  CHL.collecting,
                  CHL.collecting_datetime,
                  CHL.active, 
                  CONCAT(OIL.package_number, " - ", CHLL.quantity) as packages_numbers, 
                  CHL.destination_magazine,
                  CHL.created, CHL.created_by,
                  OIL.type
                 FROM collecting_high_level AS CHL
                 JOIN products AS P
                  ON P.id = CHL.product_id
                 JOIN products_stock AS PS
                  ON PS.id = CHL.product_id
                 JOIN collecting_high_level_lists AS CHLL
                  ON CHL.id = CHLL.collecting_high_level_id
                 JOIN orders_items_lists AS OIL
                  ON OIL.id = CHLL.orders_items_lists_id
                 WHERE 1=1
                 '.(isset($_POST['search']) && !empty($_POST['search']) ? " AND (PS.profit_g_location = '".$_POST['search']."' OR P.ean_13 LIKE '".$_POST['search']."')" : '').'
                 AND CHL.completed = "'.($_POST['f_completed'] == '1' ? '1' : '0').'"
                 '.(isset($_POST['f_collecting']) && $_POST['f_collecting'] !== '' ?  ' AND CHL.collecting = "'.$_POST['f_collecting'].'" ' : '').'
                 '.(isset($_POST['f_created']) && $_POST['f_created'] != '' ? 'AND DATE(CHL.created) = "'. date('Y-m-d', strtotime($_POST['f_created'])).'"'  : '').'
                 ORDER BY CHL.collecting_datetime, CHL.id DESC
                 ';

        return $this->pDbMgr->GetAll('profit24', $sSql);
    }
}