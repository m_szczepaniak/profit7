<?php
/**
 * Plik jezykowy dla interfejsu modulu 'Zamowienia' Panelu Administracyjnego
 * jezyk polski
 *
 * @author Marcin Korecki <m.korecki@omnia.pl>
 * @version 1.0
 *
 */
$aConfig['lang']['m_zamowienia_magazyn_containers']['activate'] = 'Odblokuj';
$aConfig['lang']['m_zamowienia_magazyn_defect']['add'] = 'Pobierz Defekty';


$aConfig['lang']['m_zamowienia_magazyn_packages']['add'] = 'Dodaj typ opakowania';
$aConfig['lang']['m_zamowienia_magazyn_packages']['add_supply'] = 'Dodaj dostawę opakowania';
$aConfig['lang']['m_zamowienia_magazyn_packages']['history'] = 'Historia opakowań';

$aConfig['lang']['m_zamowienia_magazyn_return']['details'] = 'Zbieranie';
$aConfig['lang']['m_zamowienia_magazyn_return']['history'] = 'Szczegóły';

$aConfig['lang']['m_zamowienia_magazyn_fast_track']['close'] = 'Zamknij listę';
$aConfig['lang']['m_zamowienia_magazyn_fast_track']['details'] = 'Książki w kuwecie';