<?php
/**
 * @author Paweł Bromka
 * @created 2014-11-12
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__zamowienia_magazyn__containers {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  private $pDbMgr;
  
  
  function __construct($pSmarty) {
    global $pDbMgr;

    $this->pDbMgr = $pDbMgr;
    
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		$this->aPrivileges =& $_SESSION['user']['privileges'];
       
    // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
		if ($_SESSION['user']['type'] === 0) {
      $sManageGetReadySendStatus = $this->getManageGetReadySendStatus($_SESSION['user']['id']);
			if ($sManageGetReadySendStatus === "0" || !hasModulePrivileges($_GET['module_id'])){
				showPrivsAlert($pSmarty);
				return;
			}
    }
    
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
    switch ($sDo) {
      case 'activate':
        $this->unlockContainer($_GET['id']);
        
      default:
        $this->Show($pSmarty);
      break;
    }
  }
  
  
  /**
   * Metoda wyświetla listę kuwet i umożliwia ich odblokowanie 
   * 
   * @global array $aConfig
   * @param object $pSmarty
   */
  private function Show($pSmarty) {

    $sHTML = $this->ShowHTML();
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($sHTML));
  }// end of Show() method
  
  
  /**
   * Metoda wyświetla kod html widoku
   * 
   * @global array $aConfig
   * @return string HTML
   */
  private function ShowHTML() {
    global $aConfig;
    
    $aData = array();
    if (isset($_POST)) {
      $aData = $_POST;
    }

		// dolaczenie klasy View
		include_once('View/View.class.php');
    
		// zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> _('Lista kuwet'),
			'refresh'	=> true,
			'search'	=> true,
      'checkboxes'	=> false
		);

    $aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);

    $aRecordsHeader = array(
			array(
				'db_field'	=> 'id',
				'content'	=> _('Id'),
			),
			array(
				'db_field'	=> 'type',
				'content'	=> _('Typ'),
				'width' => '160'
			),
			array(
				'db_field'	=> 'locked',
				'content'	=> _('Blokada'),
				'width' => '60'
			),
      array(
				'content'	=> $aConfig['lang']['common']['action'],
				'width' => '60'
			),  
		);    

      
    // pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM containers
             WHERE 1 = 1".
                  (isset($_POST['search']) && !empty($_POST['search']) ? ' AND id LIKE \'%'.$_POST['search'].'%\'' : '').
            " ORDER BY id";

		$iRowCount = intval(Common::GetOne($sSql));
    
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
      
			// ponowne okreslenie liczby rekordow
      $sSql = "SELECT COUNT(id)
               FROM containers
               WHERE 1 = 1".
                    (isset($_POST['search']) && !empty($_POST['search']) ? ' AND id LIKE \'%'.$_POST['search'].'%\'' : '').
              " ORDER BY id";
      $iRowCount = intval(Common::GetOne($sSql));
		}
      
		$pView = new View('containers', $aHeader, $aAttribs);
    $pView->AddRecordsHeader($aRecordsHeader);

    
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
      $aRecords = $this->getContainers($iStartFrom, $iPerPage);
      foreach ($aRecords as $iKey => $aRecord) {
        if ($aRecord['locked'] == '0') {
          $aRecords[$iKey]['disabled'][] = 'activate';
        }
        if ($aRecord['locked'] == '0') {
          $aRecords[$iKey]['locked'] = _('Nie');
        } else {
          $aRecords[$iKey]['locked'] = _('Tak');
        }
        if ($aRecord['type'] == '0') {
          $aRecords[$iKey]['type'] = _('Kuweta');
        } else if ($aRecord['type'] == '1') {
          $aRecords[$iKey]['type'] = _('Tramwaj');
        } else if ($aRecord['type'] == '2') {
          $aRecords[$iKey]['type'] = _('Magazyn');
        }
      }
      // ustawienia dla poszczegolnych kolumn rekordu
      $aColSettings = array(
        'action' => array (
          'actions'	=> array ('activate'),
          'params' => array (
                        'activate' => array('id' => '{id}')
                      ),
          'show' => false	
        )  
      );

      if ($_SESSION['user']['priv_order_status'] != '1' &&
            $_SESSION['user']['priv_order_5_status'] != '1' &&
            $_SESSION['user']['type'] == '0' &&
            false === in_array($_SESSION['user']['name'], [
                'jbaca',
                'plemieszko',
                'agolba', 'ehejda', 'ablinska', 'mtopolowski', 'dwieprzkowicz', 'akrynicka'])
      )
      {
          unset($aColSettings['action']['actions']);
      }
        
			// dodanie rekordow do widoku
      $pView->AddRecords($aRecords, $aColSettings);
		}    
        
		return $pView->Show();
  }// end of ShowHTML() method
  
  
  /**
   * Metoda pobiera listę kuwet
   * 
   * @return array
   */
  private function getContainers($iStartFrom, $iPerPage) {

    $sSql = "SELECT id, type, locked
             FROM containers
             WHERE 1 = 1".
                (isset($_POST['search']) && !empty($_POST['search']) ? ' AND id LIKE \'%'.$_POST['search'].'%\'' : '').
             " ORDER BY type ASC, id ASC
             LIMIT ".$iStartFrom.", ".$iPerPage;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getContainers() method

  
  /**
   * Metoda odblokowuje kuwetę
   * 
   */
  private function unlockContainer($iId) {

    $sSql = 'UPDATE containers
            SET locked = 0
            WHERE id = '.$iId;
    if ($this->pDbMgr->Query('profit24', $sSql) === false) {
      return false;
    }
    return true;
  }// end of unlockContainer() method

  
  /**
   * Sprawdza, czy użytkownik ma uprawnienia do zarządzanie parametrami pobieranej listy do zebrania
   * 
   * @param	integer	$iId	- Id użytkownika
   * @return string
   */
  private function getManageGetReadySendStatus($iId) {

    $sSql = "SELECT manage_get_ready_send
             FROM users
             WHERE id = ".$iId;
             
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of getManageGetReadySendStatus() method

}