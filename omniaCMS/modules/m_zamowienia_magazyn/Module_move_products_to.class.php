<?php

use LIB\communicator\sources\Internal\streamsoft\MMdocument;
use FtpClient\FtpClient;

class Module__zamowienia_magazyn__move_products_to
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    var $location;

    var $allProductsForLocation;

    var $availableProducts;

    var $fv = array();

    var $suppliers = array();

    var $products_to_mm = array();

    var $json_output_directory = '/files/merlin_json/';//'/outputMerlin/'

    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    private $pDbMgr;


    function __construct($pSmarty)
    {
        global $pDbMgr;
        global $aConfig;

        ini_set('display_errors', 1);
        error_reporting(E_ALL ^ E_STRICT);

        //json output path depends on main dir
        $this->json_output_directory = $aConfig['common']['client_base_path'] . $this->json_output_directory;

        $this->pDbMgr = $pDbMgr;

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        if (isset($_POST['location'])) {
            $this->location = trim($_POST['location']);
        }

        $this->aPrivileges =& $_SESSION['user']['privileges'];

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        switch ($sDo) {
            default:
                $this->Show($pSmarty);
                break;
        }
    }

    /**
     * @global array $aConfig
     * @param object $pSmarty
     */
    private function Show($pSmarty)
    {
        global $aConfig;

        if (isset($this->location)) {

            if ($this->scannedAlready($this->location))
            {
                echo 'Półka już została zeskanowana , nie możesz jej zeskanować drugi raz';
            }
            else
            {
                $hasReservations = $this->hasReservations($this->location);
                if ($hasReservations)
                {
                    $this->getAvailability();
                    $this->takeFromStreamsoft();
                }
            }
        }

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        $pForm = new FormTable('images', null, array('action' => phpSelf() . '&do=doCheck', 'enctype' => 'multipart/form-data'), array('col_width' => 135), $aConfig['common']['js_validation']);
        // maksymalna suma rozmiarow przesylanych zdjec
        $pForm->AddRow('', 'Zeskanuj lokacje', '', array(), array('class' => 'redText'));

        $pForm->AddText('location', "Lokacja", '', array('style' => 'width:150px;', 'maxlength' => 10), '', 'text', false);

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
    }// end of Show() method


    /**
     * @param $location
     * @return bool
     */
    private function hasReservations($location)
    {
        $sSql = '
                SELECT PS.*, M.name AS magazine_type, P.streamsoft_indeks, P.name
                FROM stock_location_18012019 AS PS
                JOIN magazine AS M
                  ON  M.type = PS.magazine_type
                JOIN products AS P
                  ON P.id = PS.products_stock_id
                WHERE PS.container_id = "' . $location . '" AND PS.quantity > 0
                ';

        $this->allProductsForLocation = $this->pDbMgr->GetAll('profit24', $sSql);

        if (empty($this->allProductsForLocation))
        {
            echo 'Nieprawidlowa półka ' . $location . ' lub brak pozycji na półce<br>';
            return false;
        }
        return true;
    }

    private function scannedAlready($location)
    {
        $sSql = 'SELECT * FROM merlin_sent_documents WHERE location = "'.$location.'"';

        $location_already_scanned = $this->pDbMgr->GetAll('profit24', $sSql );

        if (empty($location_already_scanned))
        {
            return false;
        }
        else
        {
            return true;
        }
    }


    private function scannedAlready($location)
    {
        $sSql = 'SELECT * FROM merlin_sent_documents WHERE location = "'.$location.'"';

        $location_already_scanned = $this->pDbMgr->GetAll('profit24', $sSql );

        if (empty($location_already_scanned))
        {
            return false;
        }
        else
        {
            return true;
        }
    }


    /**
     * 1.Pobrać wszystkie dostępności dla danego produktu dla danego magazynu STOCK
     */
    private function getAvailability()
    {
        foreach ($this->allProductsForLocation as $product) {
            //var_dump($product);
            $sSql = 'SELECT SS.*,P.streamsoft_indeks,P.name FROM streamsoft_stock SS
                     JOIN products P
                     ON SS.product_id = P.id AND product_id = "' . $product['products_stock_id'] . '"
                     WHERE warehouse = \'7\' 
                     ORDER BY purchase_price ASC';
            $result = $this->pDbMgr->GetAll('profit24', $sSql);

            foreach ($result as $record) {
                $this->availableProducts[$product['products_stock_id']][] = $record;
            }
        }

        //var_dump($this->availableProducts);
    }

    private function getRealQuantity($product_id)
    {
        $sSql = 'SELECT SUM(quantity) as quantity FROM `products_stock_supplies`
                 WHERE source = "10" 
                 AND product_id = "'.$product_id.'"
                 GROUP BY product_id;'; //source 10 - stock

        $result = $this->pDbMgr->GetOne('profit24', $sSql);

        if (empty($result)) {
            throw new \Exception('Produkt nie posiada danych w tabeli products_stock_supplies ' . $product_id . '<br>');
        } else {
            return $result;
        }
    }


    /**
     * 2.Ściągać z streamsoft_stock ilości tak aby zaspokoić pełną ilość z danej półki dla danego produktu
     */
    private function takeFromStreamsoft()
    {
        try {
            $this->pDbMgr->BeginTransaction('profit24');
            foreach ($this->allProductsForLocation as $product) {
                $countToTake = $product['quantity'];

                $realQuantity = $this->getRealQuantity($product['products_stock_id']); //prawdziwa ilość na lokalizacji
                $merlinSku = $this->findMerlinSku($product['products_stock_id']);

                if ($realQuantity < $countToTake) {
                    $countToTake = $realQuantity;
                }



                if ($countToTake > 0) {
                    echo 'Udało się ściągnąć odpowiednią ilość dla ' . $product['products_stock_id']." - <br>";


                    $pozMM = [];
                    $pozMM['streamsoft_indeks'] = $product['streamsoft_indeks'];

                    $this->push_to_mm_products($countToTake,$product);
                } else {
                    throw new \Exception('Nie udało się ściągnąć wystarczającej ilości dla ' . $product['products_stock_id'] . ' ' . $countToTake.'<br>');
                }
            }

            $this->moveProducts($this->location);
            //$this->createFiles();
            $this->pDbMgr->CommitTransaction('profit24');
        } catch (\Exception $ex) {
            $this->pDbMgr->RollbackTransaction('profit24');
            echo $ex->getMessage();
        }

        //var_dump($this->products_to_mm);

        /*
        echo "<br>";

        foreach($this->products_to_mm as $prod)
        {
            echo $prod['products_stock_id'].' - '.$prod['quantity'].'<br>';
        }
        */
    }

    /**
     * 3.Sprawdzić czy się udało jesli się nie udało wyrzucamy błąd x
     * 4.ustawiamy quantity_taken x
     * 5.Json osobny dla faktury i osobny dla każdego dostawcy
     * @param $product
     * @param $quantity
     */
    private function addToJson($product, $quantity)
    {
        $product['merlin_sku'] = $this->findMerlinSku($product['product_id']);
        $product['quantity'] = $quantity;
        $product['shelf_id'] = $this->location;
        unset($product['id']);
        unset($product['product_id']);
        unset($product['warehouse']);
        unset($product['quantity_taken']);
        unset($product['streamsoft_indeks']);

        if ($product['document_type'] == 'MM') {
            $this->suppliers[$product['supplier_id']][] = $product;
        } else if ($product['document_type'] == 'FA') {
            $this->fv[] = $product;
        }
    }

    private function push_to_mm_products($quantity,$product)
    {
        $product['quantity'] = $quantity;
        $this->products_to_mm[] = $product;
    }

    /**
     * @param $product_id
     * @return mixed
     * @throws Exception
     */
    private function findMerlinSku($product_id)
    {
        $sSql = 'SELECT merlin_sku FROM products_merlin_mappings WHERE product_id = "' . $product_id . '" ';
        $result = $this->pDbMgr->GetOne('profit24', $sSql);

        if (empty($result)) {
            throw new \Exception('Produkt nie posiada mapowania SKU -> ' . $product_id.' <br>');
        } else {
            return $result;
        }
    }

    /**
     * Ustawiamy dokument mm
     *
     * @param $location
     */
    private function moveProducts($location)
    {
        $mMdocument = new MMdocument($this->pDbMgr);
        $mMdocument->doSendMM_custom($location,$this->products_to_mm);
    }

    /**
     *
     */
    private function createFiles()
    {
        $file_list = array();
        //FA
        $json_content = json_encode($this->fv);

        if (!file_exists($this->json_output_directory)) {
            mkdir($this->json_output_directory,0766,true);
        }

        file_put_contents($this->json_output_directory . 'FA_'.$this->location.'.json', $json_content);
        $file_list[] = 'FA_'.$this->location.'.json';

        //MM
        foreach ($this->suppliers as $supplier => $data) {
            //echo $supplier;
            $json_content = json_encode($data);
            file_put_contents($this->json_output_directory . 'MM_' . $supplier.'_'.$this->location.'.json', $json_content);
            $file_list[] = 'MM_' . $supplier . '_' . $this->location. '.json';
        }

        $this->sendFilesToFtp($file_list);
    }

    /**
     * @param $file_list
     * @throws Exception
     */
    private function sendFilesToFtp($file_list)
    {
        $host = 'ftp.merlin.pl';
        $login = 'migracja_profit';
        $password = 'xiKDmsS6BgILs0S7ZYDM';

        $migration_dir = $this->location;

        $ftp = new FtpClient();
        $ftp->connect($host);
        $ftp->login($login, $password);
        $ftp->pasv(true);

        $isDir = $ftp->isDir($migration_dir);

        if (false == $isDir) {
            $mkDirRes = $ftp->mkdir($migration_dir, true);
            if ($mkDirRes === false) {
                throw new \Exception('Nie można utworzyć folderu prawdopodobnie brak uprawnień' . '<br>');
            }
        }

        foreach ($file_list as $filename) {
            $uploadRes = $ftp->put($migration_dir . DIRECTORY_SEPARATOR . $filename, $this->json_output_directory . $filename, FTP_ASCII);
            if ($uploadRes === false) {
                throw new \Exception('Nie można utworzyć wysłać plików' . '<br>');
            }
        }

        //$ftp->close();
    }
}