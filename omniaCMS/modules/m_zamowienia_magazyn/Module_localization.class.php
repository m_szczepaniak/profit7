<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn;

use Admin;
use DatabaseManager;
use FormTable;
use magazine\Containers;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;
use Validator;

/**
 * Description of Module
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn__localization implements ModuleEntity
{

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;
    private $sModule;

    public function __construct(Admin $oClassRepository)
    {

        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function doDefault()
    {
        // wczytaj identyfikator książki
        return $this->getProductIdentView();
    }

    /**
     *
     * @param string $sProdIdent
     * @return array
     */
    private function findProductByIdent($sProdIdent)
    {

        $sSql = 'SELECT PS.id, PS.profit_g_location, PS.profit_x_location, PS.profit_g_act_stock, P.name
             FROM products AS P
             JOIN products_stock AS PS
              ON P.id = PS.id
             WHERE 
                (
                  P.isbn_plain = "%1$s" OR 
                  P.isbn_10 = "%1$s" OR 
                  P.isbn_13 = "%1$s" OR 
                  P.ean_13 = "%1$s"
                )'; // OR streamsoft_indeks = "%1$s"
        $sSql = sprintf($sSql, $sProdIdent);
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }

    /**
     *
     * @return string HTML
     */
    private function getProductIdentView()
    {
        if (!empty($_POST)) {
            $this->getLocalizationMessage($_POST['product_ident']);
        }

        include_once('Form/FormTable.class.php');
        $oForm = new FormTable('scan_product_ident', _('Wczytaj produkt na magazyn Stock / ПРИПИШИТЕ ПРОДУКТ НА РЕЗЕРВНОМ СКЛАДЕ'), array('action' => phpSelf(array('do' => 'default'))), array('col_width' => 155), false);
        $oForm->AddText('product_ident', _('EAN / КОД КНИГИ'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
        $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
        return $oForm->ShowForm();
    }


    /**
     * @param $sProdIdent
     */
    private function getLocalizationMessage($sProdIdent)
    {
        $aProduct = $this->findProductByIdent($sProdIdent);
        if (!empty($aProduct)) {
            $productsStockLocations = $this->findProductLocationHistory($aProduct['id']);
            $historyLocation = '';
            foreach ($productsStockLocations as $stockLocations) {
                if ($stockLocations['new_x_location'] != null || $stockLocations['old_x_location'] != null) {
                  $historyLocation .= '<span class="location-style">'.$stockLocations['old_x_location'].'</span> -> <span class="location-style">'.$stockLocations['new_x_location'].'</span> <br />';
                }
                if ($stockLocations['new_g_location'] != null || $stockLocations['old_g_location'] != null) {
                  $historyLocation .= '<span class="location-style">'.$stockLocations['old_g_location'].'</span> -> <span class="location-style">'.$stockLocations['new_g_location'].'</span> <br />';
                }
            }

            $imgHtml = 'Brak obrazka';

            global $aConfig;
            $imgPath = $this->getProductImage($aProduct['id']);

            if (!empty($imgPath)) {
                $imgHtml = '<img src="'.str_replace('omniaCMS', '', $aConfig['common']['base_url_https'].$imgPath).'" />';
            }

            $newLocationMsg = '<b>Nowa</b> Lokalizacja produktu: 
                  <h3> %s </h3>';

            $sSql = 'SELECT SL.*, M.name AS magazine_name 
                     FROM stock_location AS SL 
                     JOIN magazine as M
                      ON SL.magazine_type = M.type
                     WHERE products_stock_id = '.$aProduct['id'].' ORDER BY available DESC';
            $stockMultilocations = $this->pDbMgr->GetAll('profit24', $sSql);
            $sLocations = '';
            foreach ($stockMultilocations as $stockMultilocation) {
                $sLocations .= $stockMultilocation['magazine_name'].' - <span class="location-style">'.$stockMultilocation['container_id'].'</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ilość : '.$stockMultilocation['quantity'].' W Rez: '.$stockMultilocation['reservation'].' <hr />';
            }

            $this->sMsg .= GetMessage(sprintf($newLocationMsg, $sLocations), false);



            $this->sMsg .= GetMessage('<b>Stara</b> Lokalizacja produktu: 
                  <h3>
                  <span class="location-style">' . $aProduct['name'] . '</span> <br />
                  <span class="location-style">' . $aProduct['profit_g_location'] . '</span> <br />
                  <span class="location-style">' . $aProduct['profit_x_location'] . '</span>
                  <span class="location-style">' . $imgHtml . '</span>

                  '.
                          ($historyLocation != '' ? ' <br /><br />Historia Lokalizacji: <br />'.$historyLocation : '')
                .'
                  </h3> w ilości: <h2>' . $aProduct['profit_g_act_stock'] . '<br /> '
          .'</h2>', false);
        } else {
            $this->sMsg .= GetMessage('Nie znaleziono produktu o EAN "' . $sProdIdent . '" ');
        }
    }

    private function getProductImage($iId)
    {
        global $aConfig;

        $sSql = "SELECT directory, photo, mime
					 FROM products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
        $aImg =& $this->pDbMgr->GetRow('profit24', $sSql);

        if (!empty($aImg)) {
            $sDir = $aConfig['common']['photo_dir'];
            if (!empty($aImg['directory'])) {
                $sDir .= '/'.$aImg['directory'];
            }

            if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])) {

                if (!empty($aSize)) {
                    return $sDir.'/'.$aImg['photo'];
                }
            }
        }

        return null;
    }

  /**
   * @param $productId
   * @return array
   */
    private function findProductLocationHistory($productId) {

      $sSql = 'SELECT * 
               FROM products_stock_locations
               WHERe products_stock_id = '.$productId.'
               ORDER BY id DESC';
      return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @return string
     */
    public function getMsg()
    {

        $sJS = '<script>
      $(function() {
        var komunikat = $(".message1").html();
        if (komunikat !== undefined && komunikat !== "") {
          new Messi(komunikat, {title: "Błąd", modal: true});
        }
      });
      </script>';
        return $this->sMsg . $sJS;
    }
}
