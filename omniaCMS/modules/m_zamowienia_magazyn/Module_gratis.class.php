<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-30
 * Time: 13:11
 */

namespace omniaCMS\modules\m_zamowienia_magazyn;

use Admin;
use DatabaseManager;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;

class Module__zamowienia_magazyn__gratis implements ModuleEntity, SingleView
{

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    private $sModule;

    /**
     *
     * @param Admin $oClassRepository
     */
    public function __construct(Admin $oClassRepository)
    {
        global $aConfig;

        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }


    public function getMsg()
    {
        return $this->sMsg;
    }

    public function parseViewItem(array $aItem)
    {

    }

    public function resetViewState()
    {

    }

    public function doDefault()
    {
        return $this->View();
    }

    private function View()
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Gratisy')),
            'refresh' => true,
            'search' => false,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'name',
                'content' => _('Nazwa'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'minimum_price',
                'content' => _('Cena minimalna'),
                'sortable' => true,
                'width' => '180'
            ),

            array(
                'db_field' => 'unique',
                'content' => _('Raz dodawany gratis'),
                'sortable' => true,
                'width' => '180'
            ),

            array(
                'db_field' => 'circle',
                'content' => _('Kolejka dodawania'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created_by',
                'content' => _('przez'),
                'sortable' => true,
                'width' => '180'
            ),

            array(
                'db_field' => 'modified',
                'content' => _('Zmodyfikowano'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'modified_by',
                'content' => _('przez'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '55'
            )

        );

        $pView = new \View('magazine_gratis', $aHeader, $aAttribs);
        // pobranie liczby wszystkich uzytkownikow
        $aRecords = $this->getRecords();
        $iRowCount = count($aRecords);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'id' => array (
                    'show'	=> false
                ),
                'name' => array (
                    'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
                ),
                'action' => array(
                    'actions' => array('edit', 'delete'),
                    'params' => array(
                        'edit' => array('id' => '{id}'),
                        'delete' => array('id' => '{id}')
                    ),
                    'show' => false,
                )
            );
            $pView->AddRecords($aRecords, $aColSettings);
        }
        $pView->AddRecordsHeader($aRecordsHeader);
        $aRecordsFooter = array(array(), array('add'));
        $pView->AddRecordsFooter($aRecordsFooter);
        return $pView->Show();
    }


    public function doCategories() {
        $iId = $_GET['id'];

        $aChecked = $_POST['categories'];



        include_once('Form/FormTable.class.php');
        $pForm = new \FormTable('add_gratis', _('Gratis'));
        $pForm->AddHidden('do', 'save');
        $pForm->AddHidden('_id', $iId);



        return $pForm->ShowForm();
    }


    /**
     * @return mixed
     */
    private function getCategories(){
        global $aConfig;

        $sSql1="SELECT id
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".getModuleId('m_oferta_produktowa')."
					 AND mtype = '0'
					 AND parent_id IS NULL
					 AND published = '1'";
        $aMenusItems = $this->pDbMgr->GetCol('profit24', $sSql1);

        $sSql2 = "SELECT id AS value, name AS label
						FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".getModuleId('m_oferta_produktowa')."
					 AND mtype = '0'
					 AND parent_id IN (".implode(',', $aMenusItems).")
					 ORDER BY priority";
        return $this->pDbMgr->GetAll('profit24', $sSql2);
    }

    /**
     * @return string
     */
    public function doAdd()
    {

        return $this->doAddEdit();
    }

    /**
     * @return string
     */
    public function doEdit()
    {
        $iId = $_GET['id'];
        return $this->doAddEdit($iId);
    }


    /**
     * @return string
     */
    public function doDelete()
    {
        $iId = $_GET['id'];
        $sSql = 'DELETE FROM magazine_gratis WHERE id = '.$iId;
        if ($this->pDbMgr->Query('profit24', $sSql) === false) {
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania gratisu'));
        } else {
            $this->sMsg .= GetMessage(_('Usunięto gratis o id '.$iId), false);
        }
        return $this->doDefault();
    }


    /**
     *
     * @return string
     */
    public function doAddEdit($iId = 0)
    {
        global $aConfig;

        $aData = [];

        if ($iId > 0) {
            $aData = $this->pDbMgr->getTableRow('magazine_gratis', $iId, ['*']);
            $aData['websites'] = $this->getAllRowsTable('magazine_gratis_website', ['website_id'], ' magazine_gratis_id = '.$iId);
            $aData['categories'] = $this->getAllRowsTable('magazine_gratis_categories', ['page_id'], ' magazine_gratis_id = '.$iId);
            $aData['publishers'] = $this->GetPublisher($iId);
        }

        if (isset($_POST) && !empty($_POST)) {
            $aData = $_POST;
        }

        include_once('Form/FormTable.class.php');
        $pForm = new \FormTable('add_gratis', _('Gratis'));
        $pForm->AddHidden('do', 'save');
        $pForm->AddHidden('_id', $iId);

        $pForm->AddText('name', _('Nazwa'), $aData['name'], [], '', 'text', true);
        $pForm->AddText('code', _('Kod kreskowy'), $aData['code'], [], '', 'text', true);
        $pForm->AddText('minimum_price', _('Cena minimalna'), $aData['minimum_price'], [], '', 'float', false);
        $pForm->AddCheckBox('unique_on_email', _('Tylko raz dodawany gratis na adres email - użytkownika'), [], '', $aData['unique_on_email'], false);
        $pForm->AddCheckBox('multiple_addition', _('Drugi gratis - może zostać dodany niezależnie czy gratis już został dodany. '), [], '', $aData['multiple_addition'], false);
        $pForm->AddCheckBox('circle', _('Kolejka dodawania'), [], '', $aData['circle'], false);

        $pForm->AddText('publishers', _('Wydawnictwo'), $aData['publishers'] != '' ? $aData['publishers'] : '', array('id' => 'publisher_aci', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', false);

        $aWebsites = $this->getWebsites();
        $pForm->AddCheckBoxSet('websites', _('Serwisy'), $aWebsites, $aData['websites'], '', false);

        $aCategories = $this->getCategories();
        $pForm->AddCheckBoxSet('categories', _('Kategorie'), $aCategories, $aData['categories'], '', false);

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz')) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'default')) . '\');'), 'button'));


        $sJS = '
        <script type="text/javascript">
            $(document).ready(function(){
                  $("#publisher_aci").autocomplete({
                            source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetPublishers.php"
                  });
            });
        </script>
      ';
        return ShowTable($pForm->ShowForm()).$sJS;
    }

    /**
     * @param $iId
     * @return mixed
     */
    private function GetPublisher($iId) {

        $sSql = 'SELECT PP.name FROM magazine_gratis_publisher AS MGP
                  JOIN products_publishers AS PP
                  ON PP.id = MGP.products_publishers_id
                  WHERE magazine_gratis_id = '.$iId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * @param $sTable
     * @param $aSelCols
     * @param $sWhere
     * @return array
     */
    private function getAllRowsTable($sTable, $aSelCols, $sWhere) {

        $sSql = 'SELECT '.implode(', ', $aSelCols).' FROM '.$sTable.' WHERE '.$sWhere;
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

    /**
     * @return array
     */
    private function getWebsites() {

        $sSql = 'SELECT name AS label, id AS value
                 FROM websites
                 ORDER BY id ASC';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    public function doSave()
    {
        $iId = 0;
        if (isset($_POST['_id'])) {
            $iId = $_POST['_id'];
        }

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            return $this->doAddEdit($iId);
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new \Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            return $this->doAddEdit($iId);
        }

        $aValues = [
            'name' => $_POST['name'],
            'code' => $_POST['code'],
            'minimum_price' => $_POST['minimum_price'],
            'unique_on_email' => $_POST['unique_on_email'],
            'multiple_addition' => isset($_POST['multiple_addition']) ? '1' : '0',
            'circle' => $_POST['circle'],
        ];

        if ($iId > 0) {
            $aValues['modified'] = 'NOW()';
            $aValues['modified_by'] = $_SESSION['user']['name'];
            if ($this->pDbMgr->Update('profit24', 'magazine_gratis', $aValues, 'id = ' . $iId) === false) {
                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas aktualizacji gratisu'));
                return $this->doAddEdit($iId);
            }
        } else {
            $aValues['created'] = 'NOW()';
            $aValues['created_by'] = $_SESSION['user']['name'];
            $iId = $this->pDbMgr->Insert('profit24', 'magazine_gratis', $aValues);
            if ($iId === false) {
                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas dodawania gratisu'));
                return $this->doAddEdit();
            }
        }


        $aCategories = $_POST['categories'];
        $sPublishers = $_POST['publishers'];
        $aWebsites = $_POST['websites'];

        if (!empty($aCategories)) {
            $this->fillCategories($iId, $aCategories);
        }

        $this->fillPublishers($iId, $sPublishers);

        if (!empty($aWebsites)) {
            $this->fillWebsites($iId, $aWebsites);
        }

        $this->sMsg .= GetMessage(_('Wprowadzono zmiany'), false);
        return $this->doDefault();
    }


    /**
     * @param $iId
     * @param $aWebsites
     * @return bool
     */
    private function fillWebsites($iId, $aWebsites) {

        $sSql = 'DELETE FROM magazine_gratis_website WHERE magazine_gratis_id = ' . $iId;
        $this->pDbMgr->Query('profit24', $sSql);

        foreach ($aWebsites as $iWebsiteId) {
            $aValues = [
                'magazine_gratis_id' => $iId,
                'website_id' => $iWebsiteId
            ];
            $this->pDbMgr->Insert('profit24', 'magazine_gratis_website', $aValues);
        }
        return true;
    }


    /**
     * @param $iId
     * @param $aCategories
     * @return bool
     */
    private function fillPublishers($iId, $sPublishers) {

        $sSql = 'DELETE FROM magazine_gratis_publisher WHERE magazine_gratis_id = ' . $iId;
        $this->pDbMgr->Query('profit24', $sSql);

        $sSql = 'SELECT id FROM products_publishers WHERE name = "'.$sPublishers.'" ';
        $aPublishers = $this->pDbMgr->GetCol('profit24', $sSql);

        foreach ($aPublishers as $iPublisherId) {
            $aValues = [
                'magazine_gratis_id' => $iId,
                'products_publishers_id' => $iPublisherId
            ];
            $this->pDbMgr->Insert('profit24', 'magazine_gratis_publisher', $aValues);
        }
        return true;
    }

    /**
     * @param $iId
     * @param $aCategories
     * @return bool
     */
    private function fillCategories($iId, $aCategories) {

        $sSql = 'DELETE FROM magazine_gratis_categories WHERE magazine_gratis_id = ' . $iId;
        $this->pDbMgr->Query('profit24', $sSql);

        foreach ($aCategories as $iPageId) {
            $aValues = [
                'magazine_gratis_id' => $iId,
                'page_id' => $iPageId
                ];
            $this->pDbMgr->Insert('profit24', 'magazine_gratis_categories', $aValues);
        }
        return true;
    }


    /**
     * @return array
     */
    private function getRecords()
    {

        $sSql = 'SELECT id, name, minimum_price, unique_on_email, circle, created, created_by, modified, modified_by
                 FROM magazine_gratis
                 WHERE 1=1
                 ' . (isset($_POST['f_search']) ? ' AND name LIKE "%' . $_POST['f_search'] . '%"' : '') . '
                 ORDER BY id DESC';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }
}