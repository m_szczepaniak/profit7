<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn;

use Admin;
use DatabaseManager;
use FormTable;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use magazine\Containers;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;
use Validator;

/**
 * Description of Module
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn__product_stock_location_history implements ModuleEntity
{

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;
    private $sModule;

    public function __construct(Admin $oClassRepository)
    {

        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function doDefault()
    {
        // wczytaj identyfikator książki
        return $this->getProductIdentView();
    }

    /**
     *
     * @return string HTML
     */
    private function getProductIdentView()
    {
        $sHTML = '';
        if (!empty($_POST)) {
            $sHTML = $this->getLocalizationMessage($_POST['product_ident']);
        }

        include_once('Form/FormTable.class.php');
        $oForm = new FormTable('scan_product_ident', _('Historia ruchów magazynowych produktu'), array('action' => phpSelf(array('do' => 'default'))), array('col_width' => 155), false);
        $oForm->AddText('product_ident', _('EAN / КОД КНИГИ'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
        $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
        $oForm->AddMergedRow($sHTML);
        return $oForm->ShowForm();
    }


    /**
     *
     * @param string $sProdIdent
     * @return array
     */
    private function findProductByIdent($sProdIdent)
    {

        $sSql = 'SELECT P.id
             FROM products AS P
             WHERE 
                (
                  P.isbn_plain = "%1$s" OR 
                  P.isbn_10 = "%1$s" OR 
                  P.isbn_13 = "%1$s" OR 
                  P.ean_13 = "%1$s"
                )';
        $sSql = sprintf($sSql, $sProdIdent);
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * @param $sProdIdent
     * @return string
     */
    private function getLocalizationMessage($sProdIdent)
    {
        $productId = $this->findProductByIdent($sProdIdent);
        $sSql = 'SELECT  SL.container_id, OILI.id, OIL.type AS document_type, SLOILI.type as item_document_type, SLOILI.reserved_quantity, SLOILI.confirmed_quantity, SLOILI.created, SLOILI.created_by,
                        SL.magazine_type, OIL.big
                 FROM stock_location AS SL
                 JOIN stock_location_orders_items_lists_items AS SLOILI
                  ON SL.id = SLOILI.stock_location_id AND SL.magazine_type = SLOILI.stock_location_magazine_type
                 JOIN orders_items_lists_items AS OILI
                  ON SLOILI.orders_items_lists_items_id = OILI.id
                 JOIN orders_items_lists AS OIL
                  ON OILI.orders_items_lists_id = OIL.id
                 LEFT JOIN sell_predict AS SP
                  ON SP.orders_items_lists_items_id = OILI.id
                 WHERE SL.products_stock_id = '.$productId.'
                 ORDER BY SLOILI.id DESC
        ';
        $data = $this->pDbMgr->GetAll('profit24', $sSql);
        foreach ($data as &$row) {
            if ($row['document_type'] != OrdersItemsLists::HIGH_STOCK_LEVEL_DOCUMENT_TYPE || $row['magazine_type'] == Magazine::TYPE_LOW_STOCK_SUPPLIES) {
                $row['confirmed_quantity'] = $row['reserved_quantity'];
            }

            if ($row['magazine_type'] == Magazine::TYPE_HIGH_STOCK_SUPPLIES &&
                $row['document_type'] == OrdersItemsLists::HIGH_STOCK_LEVEL_DOCUMENT_TYPE &&
                $row['item_document_type'] == 'MM'
            ) {
                $row['item_document_type'] = 'MM-';
            }
            if ($row['magazine_type'] == Magazine::TYPE_LOW_STOCK_SUPPLIES &&
                $row['document_type'] == OrdersItemsLists::HIGH_STOCK_LEVEL_DOCUMENT_TYPE &&
                $row['item_document_type'] == 'MM'
            ) {
                $row['item_document_type'] = 'MM+';
            }

            if ($row['magazine_type'] == Magazine::TYPE_LOW_STOCK_SUPPLIES &&
                $row['item_document_type'] == 'MM'
            ) {
                $row['item_document_type'] = 'MM-';
            }

            if ($row['item_document_type'] == 'MM-') {
                $row['item_document_type'] = '<span style="color: #94100e">'.$row['item_document_type'].'</span>';

            } elseif ($row['item_document_type'] == 'MM+') {
                $row['item_document_type'] = '<span style="color: #00bb00">'.$row['item_document_type'].'</span>';
            }
            elseif ($row['item_document_type'] == 'PZ') {
                $row['item_document_type'] = '<span style="color: blue;">'.$row['item_document_type'].'</span>';
            }
            elseif ($row['item_document_type'] == 'ZD') {
                $row['item_document_type'] = '<span style="color: orangered;">'.$row['item_document_type'].'</span>';
            }


            switch ($row['document_type']) {
                case '0':
                    $row['document_type'] = _('Lista Zwykła');
                    break;
                case '1':
                    $row['document_type'] = _('Lista Single');
                    break;
                case '6':
                    $row['document_type'] = _('Lista Double');
                    break;
                case '2':
                    $row['document_type'] = _('Lista Połączone (wspólna wysyłka)');
                    break;
                case '3':
                    $row['document_type'] = _('Lista Tramwajowa');
                    break;
                case '4':
                    $row['document_type'] = _('Lista Anulowane');
                    break;
                case '5':
                    $row['document_type'] = _('Lista Wysokie składowanie');
                    break;
                case '7':
                    $row['document_type'] = _('Dostawa niezależna');
                    break;
                case '8':
                    $row['document_type'] = _('Zbieranie do przesunięcia z B6 na A4');
                    break;
                case '9':
                    $row['document_type'] = _('Zwrot do dostawcy');
                    break;
            }
            if ($row['big'] == '1') {
                $row['document_type'] .= _(' - Duże');
            }

            $row['magazine_type'] = Magazine::getMagazineNameByType($row['magazine_type']);

            $row['container_id'] = '<strong>'.$row['container_id'].'</strong>';
        }

        $sHTML = '<table>';
        $sHTML .= '<tr><th>Lokalizacja</th>
<th>Id listy/dokumentu</th>
<th>Typ dokumentu</th>
<th>Typ ruchu</th>
<th>Ilość zarezerwowana</th><th>Ilość zdjęta/Ilość rozłożona</th>
<th>Data</th>
<th>przez</th>
<th>Magazyn</th>
</tr>';
        foreach ($data as &$row) {
            $sHTML .= '<tr>';
            foreach ($row as $column => $value) {
                $sHTML .= '<td>'.$value.'</td>';
            }
            $sHTML .= '</tr>';

        }
        $sHTML .= '</table>';

        return $sHTML;
    }


    /**
     * @return string
     */
    public function getMsg()
    {

        $sJS = '<script>
      $(function() {
        var komunikat = $(".message1").html();
        if (komunikat !== undefined && komunikat !== "") {
          new Messi(komunikat, {title: "Błąd", modal: true});
        }
      });
      </script>';
        return $this->sMsg . $sJS;
    }
}
