<?php
namespace omniaCMS\modules\m_zamowienia_magazyn;

use Admin;
use DatabaseManager;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;

/**
 * Description of Module_defekty
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn__defect implements ModuleEntity, SingleView  {
  
  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var Smarty
   */
  private $pSmarty;
  
  /**
   *
   * @var string - komunikat
   */
  private $sMsg;
  
  /**
   * @var Admin
   */
  public $oClassRepository;
  
  private $sModule;
  
  const FILEPATH = '/../files/defect/';
  
  /**
   * 
   * @param Admin $oClassRepository
   */
  public function __construct(Admin $oClassRepository) {
    global $aConfig;
    
    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule.$this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }

  public function doDefault() {
    return $this->View();
  }
  
  public function View() {
    global $aConfig;
    
		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'header'	=> sprintf(_('Defekty')),
			'refresh'	=> true,
			'search'	=> false,
      'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
      array(
        'db_field'	=> 'source',
        'content'	=> _('Źródło'),
        'sortable'	=> true,
        'width'	=> '180'
      ),
      array(
        'db_field'	=> 'count',
        'content'	=> _('Ilość'),
        'sortable'	=> true,
        'width'	=> '180'
      ),
      array(
        'db_field'	=> 'created',
        'content'	=> _('Data'),
        'sortable'	=> true,
        'width'	=> '180'
      )
    );

    $pView = new \View('users', $aHeader, $aAttribs);
    // pobranie liczby wszystkich uzytkownikow
    $aYesNo = [['label' => 'Tak', 'value' => '1'], ['label' => 'Nie', 'value' => '0']];
    $pView->AddFilter('f_taken', _('Pobrane'), addDefaultValue($aYesNo),  $_POST['f_taken']);
    $aRecords = $this->getRecords();
    $iRowCount = count($aRecords);
		if ($iRowCount > 0) {
			$aColSettings = array(
				'user_id' => array (
					'show'	=> false
				),
				'action' => array (
					'show' => false,
				)
			);
			$pView->AddRecords($aRecords, $aColSettings);
    }
		$pView->AddRecordsHeader($aRecordsHeader);
    $aRecordsFooter = array(array(),array('add'));
		$pView->AddRecordsFooter($aRecordsFooter);
    return $pView->Show();
  }
  
  /**
   * 
   * @return string
   */
  public function doAdd() {
    global $aConfig;
    
    $aData = [];
    if (isset($_POST)) {
      $aData = $_POST;
    }
    
    include_once('Form/FormTable.class.php');
    $pForm = new \FormTable('get_defect', _('Pobierz defekty'));
    $pForm->AddHidden('do', 'take');
    $pForm->AddSelect('source', _('Źródło'), [], addDefaultValue($this->getSourcesToTake()), $aData['source'], '', true);
    $pForm->AddText('date', _('Data'), $aData['source'], [], '', 'date', true);
    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Pobierz')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'default')).'\');'), 'button'));
    $pForm = $this->getManageFiles($pForm, $aConfig['common']['client_base_path'].self::FILEPATH, 'xls');

      $js = "
    <script type='text/javascript'>

        $(document).ready(function(){

            $('#send').on('click', function(){
                setTimeout(function () {
                    location.reload();
                }, 2000);
            })
        });
    </script>
    ";

      return $pForm->ShowForm().$js;
  }

  /**
   *
   * @param FormTable $oForm
   * @param string $sFilePath
   * @param string $sExt
   * @return FormTable
   */
  private function getManageFiles($oForm, $sFilePath, $sExt) {

    $aFiles = glob($sFilePath.'/*.'.$sExt);
    $aFiles = array_reverse($aFiles);
    foreach ($aFiles as $sFilenamePath) {
      $sFileName = array_pop(explode('/', $sFilenamePath));
      $sURL = phpSelf(array('do'=>'getFile', 'file_name' => rawurldecode($sFileName)));
      $oForm->AddRow(sprintf(_(' %s '), $sFileName), '<a href="'.$sURL.'">'._('Pobierz').'</a> &nbsp; &nbsp;');
    }
    return $oForm;
  }

  /**
   *
   * @global array $aConfig
   */
  public function doGetFile() {
    global $aConfig;

    $_GET['hideHeader'] = 1;
    $sFileName = rawurldecode($_GET['file_name']);

    header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$sFileName.'"');
		header("Pragma: no-cache");
		header("Expires: 0");

    echo file_get_contents($aConfig['common']['client_base_path'].self::FILEPATH.$sFileName);
    exit(0);
  }

  public function doTake() {
    global $aConfig;

    $aItems = $this->getItems($_POST['source'], $_POST['date']);
    if (empty($aItems)) {
      $this->sMsg .= GetMessage(_('Brak produktów'), true);
      return $this->doAdd();
    }
    $sFilename = $aItems[0]['symbol'].'_'.rand(1000, 9999)."_".$_POST['date'].".xls";
    $sFilePathName = $aConfig['common']['client_base_path'].self::FILEPATH.$sFilename;

    $_GET['hideHeader'] = '1';
    header('Content-Encoding: UTF-8');
    header("Content-type: text/html; charset=UTF-8");
    header("Content-Disposition:attachment;filename='".$sFilename."'");
    echo "\xEF\xBB\xBF";

    $sContent = '';
    unset($aItems[0]['symbol']);
    foreach ($aItems as $aItem) {
      unset($aItem['symbol']);
      $this->markAsTaken($aItem['ids']);
      unset($aItem['ids']);
      $sContent .= implode(';', $aItem)."\n";
    }
    unset($aItems[0]['ids']);
    $sContent = implode(';', array_keys($aItems[0]))."\n".$sContent;

    file_put_contents($sFilePathName, $sContent);
    echo $sContent;

    die;
  }

  /**
   *
   * @param string $sIds
   * @return string
   */
  private function markAsTaken($sIds) {

    $sSql = 'UPDATE orders_send_history_additional_items SET taken = "1" WHERE id IN ('.$sIds.')';
    return $this->pDbMgr->Query('profit24', $sSql);
  }


  private function getItems($sSource, $sDate) {
    global $aConfig;

    $sSql = 'SELECT GROUP_CONCAT(OSHAI.id SEPARATOR ",") as ids, S.symbol, IFNULL(CONVERT(P.name USING utf8), CONVERT(OSHAI.indeks USING utf8)) AS TYTUL, OSHAI.indeks AS EAN13, COUNT(OSHAI.indeks) AS ILOŚĆ, OSHA.fv_nr AS FV, OSHAI.created AS DATA_SKANOWANIA
             FROM orders_send_history_additional_items AS OSHAI
             JOIN orders_send_history AS OSH
              ON OSH.id = OSHAI.orders_send_history_id
             JOIN external_providers AS S
              ON S.id = OSH.source
             LEFT JOIN products AS P
              ON P.streamsoft_indeks = OSHAI.streamsoft_id
             LEFT JOIN orders_send_history_attributes AS OSHA
              ON OSHA.orders_send_history_id = OSHAI.orders_send_history_id
             WHERE OSHAI.defect = "1"
             AND taken = "0"
             AND DATE_FORMAT(OSHAI.created, "'.$aConfig['common']['cal_sql_date_format'].'") = "'.$sDate.'"
             AND OSH.source = "'.$sSource.'"
             GROUP BY OSHAI.indeks
             ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }


    private function getRecords() {

        $sSql = 'SELECT S.symbol, COUNT(OSHAI.id), OSHAI.created
             FROM orders_send_history_additional_items AS OSHAI
             JOIN orders_send_history AS OSH
              ON OSH.id = OSHAI.orders_send_history_id
             JOIN external_providers AS S
              ON S.id = OSH.source
             WHERE OSHAI.defect = "1"
             '.( isset($_POST['f_taken']) ?  ($_POST['f_taken'] === '1' ? 'AND taken = "1" ' : 'AND taken = "0" ') : '').'
             GROUP BY OSH.source, OSHAI.created
             ORDER BY OSHAI.id DESC
             ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }



  /**
   *
   * @return array
   */
  private function getSourcesToTake() {

    $sSql = 'SELECT S.symbol AS label, OSH.source AS value
             FROM orders_send_history_additional_items AS OSHAI
             JOIN orders_send_history AS OSH
              ON OSH.id = OSHAI.orders_send_history_id
             JOIN external_providers AS S
              ON S.id = OSH.source
             WHERE OSHAI.defect = "1"
             AND taken = "0"
             GROUP BY S.symbol
             ORDER BY S.symbol ASC
             ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  
  public function getMsg() {
    return $this->sMsg;
  }

  public function parseViewItem(array $aItem) {
    
  }

  public function resetViewState() {
    
  }

}
