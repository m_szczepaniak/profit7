<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-30
 * Time: 13:11
 */

namespace omniaCMS\modules\m_zamowienia_magazyn;

use Admin;
use DatabaseManager;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;

class Module__zamowienia_magazyn__packages implements ModuleEntity, SingleView
{

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    private $sModule;

    /**
     *
     * @param Admin $oClassRepository
     */
    public function __construct(Admin $oClassRepository)
    {
        global $aConfig;

        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }


    public function getMsg()
    {
        return $this->sMsg;
    }

    public function parseViewItem(array $aItem)
    {

    }

    public function resetViewState()
    {

    }

    public function doDefault()
    {
        return $this->View();
    }


    private function View()
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Opakowania')),
            'refresh' => true,
            'search' => false,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'name',
                'content' => _('Nazwa'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'code',
                'content' => _('Kod kreskowy'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'count',
                'content' => _('Ilość'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created_by',
                'content' => _('przez'),
                'sortable' => true,
                'width' => '180'
            ),

            array(
                'db_field' => 'modified',
                'content' => _('Zmodyfikowano'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'modified_by',
                'content' => _('przez'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '55'
            )

        );

        $pView = new \View('magazine_packages', $aHeader, $aAttribs);
        // pobranie liczby wszystkich uzytkownikow
        $aRecords = $this->getRecords();
        $iRowCount = count($aRecords);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'name' => array(
                    'link' => phpSelf(array('do' => 'edit', 'id' => '{id}'))
                ),
                'action' => array(
                    'actions' => array('history', 'edit', 'delete'),
                    'params' => array(
                        'history' => array('id' => '{id}'),
                        'edit' => array('id' => '{id}'),
                        'delete' => array('id' => '{id}')
                    ),
                    'icon' => array('history' => 'details'),
                    'show' => false,
                )
            );
            $pView->AddRecords($aRecords, $aColSettings);
        }
        $pView->AddRecordsHeader($aRecordsHeader);
        $aRecordsFooter = array(array('add_supply' => 'add'), array('add', 'add_supply'));
        $aFooterParams['add_supply'][2] = 'add';
        $pView->AddRecordsFooter($aRecordsFooter, $aFooterParams);
        return $pView->Show();
    }

    public function doHistory()
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Dostawy opakowania')),
            'refresh' => false,
            'search' => false,
            'checkboxes' => false,
            'per_page' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'name',
                'content' => _('Nazwa'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'count',
                'content' => _('Ilość'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created_by',
                'content' => _('przez'),
                'sortable' => true,
                'width' => '180'
            )
        );

        $pView = new \View('magazine_packages_supplies', $aHeader, $aAttribs);
        // pobranie liczby wszystkich uzytkownikow
        $aRecords = $this->getHistoryRecords($_GET['id']);
        $iRowCount = count($aRecords);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
            );
            $pView->AddRecords($aRecords, $aColSettings);
        }
        $pView->AddRecordsHeader($aRecordsHeader);

        // przyciski stopki stopki do widoku
        $aRecordsFooter = array(
            array('go_back')
        );
        $aRecordsFooterParams = array(
            'go_back' => array(1 => array('id'))
        );
        $pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
        return $pView->Show();
    }

    /**
     * @return array
     */
    private function getHistoryRecords($iMagazinePackagesId)
    {

        $sSql = 'SELECT MPS.id, MP.name, MPS.count, MPS.created, MPS.created_by
                 FROM magazine_packages_supplies AS MPS
                 JOIN magazine_packages AS MP
                  ON MP.id = MPS.magazine_packages_id
                 WHERE MP.id = ' . $iMagazinePackagesId . '
                 ' . (isset($_POST['f_search']) ? ' AND name LIKE "%' . $_POST['f_search'] . '%"' : '') . '
                 ORDER BY id DESC';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @return string
     */
    public function doAdd_supply()
    {
        global $aConfig;

        $aData = [];
        if (isset($_POST) && !empty($_POST)) {
            $aData = $_POST;
        }

        include_once('Form/FormTable.class.php');
        $pForm = new \FormTable('add_package', _('Opakowanie'));
        $pForm->AddHidden('do', 'saveSupply');

        $pForm->AddText('count', _('Ilość'), $aData['code'], [], '', 'uint', true);
        $aMagazinePackages = $this->getMagazinePackages();
        $pForm->AddSelect('magazine_packages_id', _('Opakowanie'), [], addDefaultValue($aMagazinePackages), $aData['magazine_packages_id']);

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz')) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'default')) . '\');'), 'button'));

        return ShowTable($pForm->ShowForm());
    }


    public function doSaveSupply()
    {

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            return $this->doAdd_supply();
        }

        $this->pDbMgr->BeginTransaction('profit24');
        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new \Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            return $this->doAdd_supply();
        }

        $aValues = [
            'count' => $_POST['count'],
            'magazine_packages_id' => $_POST['magazine_packages_id']
        ];

        $aValues['created'] = 'NOW()';
        $aValues['created_by'] = $_SESSION['user']['name'];
        $iId = $this->pDbMgr->Insert('profit24', 'magazine_packages_supplies', $aValues);
        if ($iId === false) {
            $this->pDbMgr->RollbackTransaction('profit24');
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas dodawania dostawy opakowania'));
            return $this->doAddEdit();
        }

        $sSql = 'UPDATE magazine_packages SET count = count + ' . $_POST['count'] . ' WHERE id = ' . $_POST['magazine_packages_id'];

        if ($this->pDbMgr->Query('profit24', $sSql) === false) {
            $this->pDbMgr->RollbackTransaction('profit24');
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas dodawania dostawy opakowania'));
            return $this->doAddEdit();
        }
        $this->pDbMgr->CommitTransaction('profit24');

        $this->sMsg .= GetMessage(_('Wprowadzono zmiany'), false);
        return $this->doDefault();
    }

    /**
     * @return array
     */
    private function getMagazinePackages()
    {

        $sSql = 'SELECT name AS label, id AS value FROM magazine_packages';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * @return string
     */
    public function doSave()
    {
        $iId = 0;
        if (isset($_POST['_id'])) {
            $iId = $_POST['_id'];
        }

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            return $this->doAddEdit($iId);
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new \Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            return $this->doAddEdit($iId);
        }

        $aValues = [
            'name' => $_POST['name'],
            'code' => $_POST['code'],
            'count' => $_POST['count'],
        ];

        if ($iId > 0) {
            $aValues['modified'] = 'NOW()';
            $aValues['modified_by'] = $_SESSION['user']['name'];
            if ($this->pDbMgr->Update('profit24', 'magazine_packages', $aValues, 'id = ' . $iId) === false) {
                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas aktualizacji opakowania'));
                return $this->doAddEdit($iId);
            }
        } else {
            $aValues['created'] = 'NOW()';
            $aValues['created_by'] = $_SESSION['user']['name'];
            $iId = $this->pDbMgr->Insert('profit24', 'magazine_packages', $aValues);
            if ($iId === false) {
                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas dodawania opakowania'));
                return $this->doAddEdit();
            }
        }

        $this->sMsg .= GetMessage(_('Wprowadzono zmiany'), false);
        return $this->doDefault();
    }


    /**
     * @return array
     */
    private function getRecords()
    {

        $sSql = 'SELECT id, name, code, count, created, created_by, modified, modified_by
                 FROM magazine_packages
                 WHERE 1=1
                 ' . (isset($_POST['f_search']) ? ' AND name LIKE "%' . $_POST['f_search'] . '%"' : '') . '
                 ORDER BY id DESC';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * @return string
     */
    public function doDelete()
    {
        $iId = $_GET['id'];
        $sSql = 'DELETE FROM magazine_packages WHERE id = ' . $iId;
        if ($this->pDbMgr->Query('profit24', $sSql) === false) {
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania opakowania'));
        } else {
            $this->sMsg .= GetMessage(_('Usunięto opakowanie o id ' . $iId), false);
        }
        return $this->doDefault();
    }


    /**
     * @return string
     */
    public function doAdd()
    {

        return $this->doAddEdit();
    }

    /**
     * @return string
     */
    public function doEdit()
    {
        $iId = $_GET['id'];
        return $this->doAddEdit($iId);
    }


    /**
     *
     * @return string
     */
    public function doAddEdit($iId = 0)
    {
        global $aConfig;

        $aData = [];

        if ($iId > 0) {
            $aData = $this->pDbMgr->getTableRow('magazine_packages', $iId, ['*']);
        }

        if (isset($_POST) && !empty($_POST)) {
            $aData = $_POST;
        }

        include_once('Form/FormTable.class.php');
        $pForm = new \FormTable('add_package', _('Opakowanie'));
        $pForm->AddHidden('do', 'save');
        $pForm->AddHidden('_id', $iId);

        $pForm->AddText('name', _('Nazwa'), $aData['name'], [], '', 'text', true);
        $pForm->AddText('code', _('Kod kreskowy'), $aData['code'], [], '', 'text', true);
        $pForm->AddText('count', _('Ilość'), $aData['count'], [], '', 'uint', true);

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz')) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'default')) . '\');'), 'button'));

        return ShowTable($pForm->ShowForm());
    }


}