<?php

use LIB\communicator\sources\Internal\streamsoft\MMdocument;

class Module__zamowienia_magazyn__move_products_to_after
{
    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    var $location;

    var $allProductsForLocation;

    var $availableProducts;

    var $fv = array();

    var $suppliers = array();

    var $products_to_mm = array();

    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    private $pDbMgr;

    function __construct($pSmarty)
    {
        global $pDbMgr;
        global $aConfig;

        ini_set('display_errors', 1);
        error_reporting(E_ALL ^ E_STRICT ^ E_DEPRECATED);

        $this->pDbMgr = $pDbMgr;

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        if (isset($_POST['location'])) {
            $this->location = trim($_POST['location']);

            $this->prefix = mb_substr($this->location,0,2);
        }

        $this->aPrivileges =& $_SESSION['user']['privileges'];

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        switch ($sDo) {
            default:
                $this->Show($pSmarty);
                break;
        }
    }

    /**
     * @global array $aConfig
     * @param object $pSmarty
     */
    private function Show($pSmarty)
    {
        global $aConfig;

        if (isset($this->location)) {

            if ($this->scannedAlready($this->location))
            {
                echo 'Półka już została zeskanowana , nie możesz jej zeskanować drugi raz';
            }
            else
            {
                if ($this->hasReservations($this->location)) {
                    $this->getAvailability();
                    $this->takeFromStreamsoft();
                }
            }
        }

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        $pForm = new FormTable('images', null, array('action' => phpSelf() . '&do=doCheck', 'enctype' => 'multipart/form-data'), array('col_width' => 135), $aConfig['common']['js_validation']);
        // maksymalna suma rozmiarow przesylanych zdjec
        $pForm->AddRow('', 'Zeskanuj lokacje', '', array(), array('class' => 'redText'));

        $pForm->AddText('location', "Lokacja", '', array('style' => 'width:150px;', 'maxlength' => 10), '', 'text', false);

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
    }// end of Show() method


    /**
     * @param $location
     * @return bool
     */
    private function hasReservations($location)
    {
        if ($this->prefix == '17')
        {
            $sSql = '
                SELECT PS.*, M.name AS magazine_type, P.streamsoft_indeks, P.name
                FROM stock_location_18012019 AS PS
                JOIN magazine AS M
                  ON  M.type = PS.magazine_type
                JOIN products AS P
                  ON P.id = PS.products_stock_id
                WHERE PS.container_id = "' . $location . '" AND PS.quantity > 0
                ';
        }
        else if ($this->prefix == '15')
        {
            $sSql = '
                SELECT PS.*, M.name AS magazine_type, P.streamsoft_indeks, P.name
                FROM stock_location AS PS
                JOIN magazine AS M
                  ON  M.type = PS.magazine_type
                JOIN products AS P
                  ON P.id = PS.products_stock_id
                WHERE PS.container_id = "' . $location . '" AND PS.quantity > 0
                ';
        }
        else if ($this->prefix == '20')
        {
            $sSql = '
                SELECT 
                  DISTINCT OI.id,
                  OI.order_id,OI.isbn,OI.name, SUM(OI.quantity) as quantity, OI.publisher, OI.authors, OI.publication_year, OI.edition,product_id as products_stock_id, P.streamsoft_indeks
                FROM orders_items AS OI
                JOIN  `orders` AS O ON OI.order_id = O.id
                JOIN  `products` AS P ON P.id = OI.product_id
                WHERE O.order_status <> \'3\'
                  AND O.order_status <> \'4\'
                  AND O.order_status <> \'5\'
                  AND OI.status = \'4\'
                  AND OI.item_type <> \'A\'
                  AND OI.deleted <> \'1\'
                  AND OI.source <> \'5\'
                  AND 
                  ( SELECT OSH.pack_number
                    FROM orders_send_history AS OSH
                    JOIN orders_send_history_items AS OSHI
                      ON OSH.id = OSHI.send_history_id
                    WHERE OSHI.item_id = OI.id
                    ORDER BY OSH.id DESC
                    LIMIT 1 ) = "'.$location.'"
                GROUP BY OI.isbn
                ORDER BY OI.quantity DESC
            ';
        }
        else
        {
            echo "Nieznany prefix";
            return false;
        }

        $this->allProductsForLocation = $this->pDbMgr->GetAll('profit24', $sSql);

        if (empty($this->allProductsForLocation))
        {
            echo 'Nieprawidlowa półka ' . $location . ' lub brak pozycji na półce<br>';
            return false;
        }
        return true;
    }

    private function scannedAlready($location)
    {
        $sSql = 'SELECT * FROM merlin_sent_documents WHERE location = "'.$location.'"';

        $location_already_scanned = $this->pDbMgr->GetAll('profit24', $sSql );

        if (empty($location_already_scanned))
        {
            return false;
        }
        else
        {
            return true;
        }
    }


    /**
     * 1.Pobrać wszystkie dostępności dla danego produktu dla danego magazynu STOCK
     */
    private function getAvailability()
    {
        foreach ($this->allProductsForLocation as $product) {
            //var_dump($product);
            $sSql = 'SELECT SS.*,P.streamsoft_indeks,P.name FROM streamsoft_stock SS
                     JOIN products P
                     ON SS.product_id = P.id AND product_id = "' . $product['products_stock_id'] . '"
                     WHERE warehouse = \'7\' 
                     ORDER BY purchase_price ASC';
            $result = $this->pDbMgr->GetAll('profit24', $sSql);

            foreach ($result as $record) {
                $this->availableProducts[$product['products_stock_id']][] = $record;
            }
        }
    }

    private function getRealQuantity($product_id)
    {
        if ($this->prefix == '20')
        {
            // baza ma id 1
            $sSql = 'SELECT SUM(SS.quantity) FROM streamsoft_stock SS
                     JOIN products P
                     ON SS.product_id = P.id AND product_id = "' . $product_id . '"
                     WHERE warehouse = \'1\' 
                     GROUP BY product_id
                     ORDER BY purchase_price ASC';
        }
        else
        {
            // stock ma id 7
            $sSql = 'SELECT SUM(SS.quantity) FROM streamsoft_stock SS
                     JOIN products P
                     ON SS.product_id = P.id AND product_id = "' . $product_id . '"
                     WHERE warehouse = \'7\' 
                     GROUP BY product_id';
        }


        $result = $this->pDbMgr->GetOne('profit24', $sSql);

        if (empty($result)) {
		//throw new \Exception('Produkt nie posiada danych w tabeli products_stock_supplies ' . $product_id . '<br>');
		return 0;
        } else {
            return $result;
        }
    }

    private function getRealQuantityTramwaj($product_id)
    {
        $sSql = 'SELECT SUM(SS.quantity) FROM streamsoft_stock SS
                     WHERE warehouse = \'1\' AND SS.product_id = "' . $product_id . '"
                     GROUP BY product_id';

        $result = $this->pDbMgr->GetOne('profit24', $sSql);

        if (empty($result)) {
		    //throw new \Exception('Produkt nie posiada danych w tabeli streamsoft_stock ' . $product_id . '<br>');
		    return 0;
        } else {
            return $result;
        }
    }

    private function takeFromStreamsoft()
    {
        try {
            $this->pDbMgr->BeginTransaction('profit24');
            foreach ($this->allProductsForLocation as $product) {
                $countToTake = $product['quantity'];

                if ($this->prefix == '20')
                {
                    $realQuantity = $this->getRealQuantityTramwaj($product['products_stock_id']); //prawdziwa ilość na lokalizacji
                }
                else
                {
                    $realQuantity = $this->getRealQuantity($product['products_stock_id']); //prawdziwa ilość na lokalizacji
                }

                $merlinSku = $this->findMerlinSku($product['products_stock_id']);

                if ($realQuantity < $countToTake) {
                    $countToTake = $realQuantity;
                }

                if ($countToTake > 0) {
                    echo 'Udało się ściągnąć odpowiednią ilość dla ' . $product['products_stock_id']." - <br>";

                    $this->push_to_mm_products($countToTake,$product);
                } else {
                    //throw new \Exception('Nie udało się ściągnąć wystarczającej ilości dla ' . $product['products_stock_id'] . ' ' . $countToTake.'<br>');
                }
            }

            $this->moveProducts($this->location);
            //$this->createFiles();
            $this->pDbMgr->CommitTransaction('profit24');
        } catch (\Exception $ex) {
            $this->pDbMgr->RollbackTransaction('profit24');
            echo $ex->getMessage();
        }

        //var_dump($this->products_to_mm);

        echo "<br>";

        foreach($this->products_to_mm as $prod)
        {
            echo $prod['products_stock_id'].' - '.$prod['quantity'].'<br>';
        }
    }

    private function push_to_mm_products($quantity,$product)
    {
        $product['quantity'] = $quantity;
        $this->products_to_mm[] = $product;
    }

    /**
     * @param $product_id
     * @return mixed
     * @throws Exception
     */
    private function findMerlinSku($product_id)
    {
        $sSql = 'SELECT merlin_sku FROM products_merlin_mappings WHERE product_id = "' . $product_id . '" ';
        $result = $this->pDbMgr->GetOne('profit24', $sSql);

        if (empty($result)) {
            throw new \Exception('Produkt nie posiada mapowania SKU -> ' . $product_id.' <br>');
        } else {
            return $result;
        }
    }

    /**
     * Ustawiamy dokument mm
     *
     * @param $location
     */
    private function moveProducts($location)
    {
        $mMdocument = new MMdocument($this->pDbMgr);
        if ($this->prefix == '20')
        {
            $mMdocument->doSendMM_custom($location,$this->products_to_mm,'01');
        }
        else
        {
            $mMdocument->doSendMM_custom($location,$this->products_to_mm,'02');
        }
    }
}
