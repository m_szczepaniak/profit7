<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn;

include_once 'modules/m_zamowienia_magazyn/Module_change_product_localization.class.php';

use Admin;
use DatabaseManager;
use FormTable;
use LIB\EntityManager\Entites\Product;
use LIB\EntityManager\Entites\StockLocation;
use magazine\Containers;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;
use Validator;
use omniaCMS\modules\m_zamowienia_magazyn\Module__zamowienia_magazyn__change_product_localization;

/**
 * Description of Module
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn__move_product_localization extends Module__zamowienia_magazyn__change_product_localization implements ModuleEntity
{
    /**
     * @var StockLocation
     */
    protected $stockLocation;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    private $sModule;

    /**
     * @var Product
     */
    protected $product;

    public function __construct(Admin $oClassRepository)
    {
        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
        $this->stockLocation = new StockLocation($this->pDbMgr);
        $this->product = new Product($this->pDbMgr);
        parent::__construct($oClassRepository);
    }

    public function doDefault()
    {
        // wczytaj identyfikator książki
        return $this->doGetSourceLocation();
    }


    /**
     * @return string
     */
    public function doGetQuantity() {
        global $aConfig;

        $ean = $_POST['EAN'];
        $sourceLocationContainerId = $_POST['source_location'];

        if ($ean == '') {
            $msg = _('Pusty EAN '.$ean);
            AddLog($msg);
            $this->sMsg .= GetMessage($msg);
            return $this->doDefault();
        }

        // znajdź produkt
        $product = $this->product->findProductByIdent($ean);
        $productId = $product['id'];
        if (intval($productId) <= 0) {
            $msg = _('Nie znaleziono produktu o identyfikatorze '.$ean);
            AddLog($msg);
            $this->sMsg .= GetMessage($msg);
            return $this->doDefault();
        }

        $sourceLocation = $this->stockLocation->getStockLocation($productId, $sourceLocationContainerId);
        if ($sourceLocation['reservation'] > 0 && $sourceLocation['available'] === 0) {
            $msg = _('Wybrany produkt ma założoną rezerwację w ilości '.$sourceLocation['reservation']);
            AddLog($msg);
            $this->sMsg .= GetMessage($msg);
            return $this->doDefault();
        }

        $productImagePath = $this->product->getProductImagePath($productId);
        if (!empty($productImagePath)) {
            $imgHtml = '<img src="'.$aConfig['common']['client_base_url_https'].$productImagePath.'" />';
        }

        // Wyświetlamy ilość i okładkę
        $this->sMsg .= GetMessage('
                  <h3>
                  <span class="location-style">' . $product['name'] . '</span> <br />
                  <span class="location-style">' . $imgHtml . '</span>

            <span class="location-style">'.$sourceLocation['container_id'].' <hr />'
            .'</h3>', false);


        include_once('Form/FormTable.class.php');
        $oForm = new \FormTable('scan_quantity', _('Wprowadź ilość produktów'), array('action' => phpSelf(array('do' => 'destinationLocalization'))), array('col_width' => 155), false);
        $oForm->AddHidden('source_location', $sourceLocation['container_id']);
        $oForm->AddHidden('EAN', $ean);
        $oForm->AddText('quantity', _('Ilość'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
        $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
        return $oForm->ShowForm();
    }

    /**
     * @return string
     */
    public function doDestinationLocalization()
    {
        global $aConfig;
        $sourceLocationContainerId = $_POST['source_location'];
        $ean = $_POST['EAN'];

        if ($ean == '') {
            $msg = _('Pusty EAN '.$ean);
            AddLog($msg);
            $this->sMsg .= GetMessage($msg);
            return $this->doDefault();
        }


        // znajdź produkt
        $product = $this->product->findProductByIdent($ean);
        $productId = $product['id'];
        if (intval($productId) <= 0) {
            $msg = _('Nie znaleziono produktu o identyfikatorze '.$ean);
            AddLog($msg);
            $this->sMsg .= GetMessage($msg);
            return $this->doDefault();
        }

        $sourceLocation = $this->stockLocation->getStockLocation($productId, $sourceLocationContainerId);

        // validuj ilość
        if (intval($_POST['quantity']) > intval($sourceLocation['available'])) {
            $msg = _('Podano nieprawidłową ilość produktu o EAN '.$ean.' podano: '.intval($_POST['quantity']).' ilość w bazie: '.intval($sourceLocation['available']));
            AddLog($msg);
            $this->sMsg .= GetMessage($msg);
            return $this->doDefault();
        }


        $productImagePath = $this->product->getProductImagePath($productId);
        if (!empty($productImagePath)) {
            $imgHtml = '<img src="'.$aConfig['common']['client_base_url_https'].$productImagePath.'" />';
        }

        // Wyświetlamy ilość i okładkę
        $this->sMsg .= GetMessage('
                  <h3>
                  <span class="location-style">' . $product['name'] . '</span> <br />
                  <span class="location-style">' . $imgHtml . '</span>
                  
            <span class="location-style">'.$sourceLocation['container_id'].'</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ilość : '.$sourceLocation['quantity'].' W Rez: '.$sourceLocation['reservation'].' <hr />'
            .'</h3>', false);


        $mContainerType = $this->validateLocationForm($_POST['source_location']);
        if ($mContainerType === false) {
            return $this->doDefault();
        }

        include_once('Form/FormTable.class.php');
        $oForm = new FormTable('scan_location', _('Wczytaj lokalizację docelową'), array('action' => phpSelf(array('do' => 'changeLocalization'))), array('col_width' => 155), false);
        $oForm->AddHidden('source_location', $sourceLocation['container_id']);
        $oForm->AddHidden('product_id', $productId);
        $oForm->AddHidden('quantity', $_POST['quantity']);

        $oForm->AddText('destination_location', _('Lokalizacja docelowa'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
        $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));

        return $oForm->ShowForm();
    }


    /**
     * @return string
     */
    public function doChangeLocalization()
    {
        $sourceLocation = $_POST['source_location'];
        $destinyLocation = $_POST['destination_location'];
        $productId = $_POST['product_id'];


        $mDestinyContainerType = $this->validateLocationForm($destinyLocation);
        if ($mDestinyContainerType === false) {
            $msg = _('Wprowadzono nieprawidłową lokalizację docelową - '.$destinyLocation);
            AddLog($msg);
            $this->sMsg .= GetMessage($msg);
            return $this->doDefault();
        }


        $mContainerType = $this->validateLocationForm($sourceLocation);
        if ($mContainerType === false) {
            $msg = _('Wprowadzono nieprawidłową lokalizację źródłową - '.$sourceLocation);
            AddLog($msg);
            $this->sMsg .= GetMessage($msg);
            return $this->doDefault();
        }

        $stockLocation = $this->stockLocation->getStockLocation($productId, $sourceLocation);
        if ($stockLocation['reservation'] > 0 && intval($_POST['quantity']) > intval($stockLocation['available']) ) {
            $msg = _('Wybrany produkt ma założoną rezerwację w ilości '.$stockLocation['reservation']);
            AddLog($msg);
            $this->sMsg .= GetMessage($msg);
            return $this->doDefault();
        }

        $this->pDbMgr->BeginTransaction('profit24');

        try {
            $this->stockLocation->moveContainerProduct($sourceLocation, $destinyLocation, $productId, $_POST['quantity']);
            $this->sMsg .= GetMessage(_('Zmianiono lokalizację produktów z "'.$sourceLocation.'" na "'.$destinyLocation.'" produkt o id = '.$productId . ' w ilosci ' . $_POST['quantity']), false);
//            throw new \Exception();
            $this->pDbMgr->CommitTransaction('profit24');
        } catch (\Exception $ex) {
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas zmiany lokalizacji'));
            $this->pDbMgr->RollbackTransaction('profit24');
        }

        return $this->doDefault();
    }

    /**
     * @return string
     */
    public function getMsg()
    {

        $sJS = '<script>
      $(function() {
        var komunikat = $(".message1").html();
        if (komunikat !== undefined && komunikat !== "") {
          new Messi(komunikat, {title: "Błąd", modal: true});
        }
      });
      </script>';
        return $this->sMsg . $sJS;
    }
}
