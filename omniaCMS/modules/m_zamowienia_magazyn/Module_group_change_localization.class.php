<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn;

use Admin;
use DatabaseManager;
use FormTable;
use magazine\Containers;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;
use Validator;

/**
 * Description of Module
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn__group_change_localization implements ModuleEntity
{

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;

  /**
   *
   * @var string - komunikat
   */
  private $sMsg;

  /**
   * @var Admin
   */
  public $oClassRepository;
  private $sModule;

  public function __construct(Admin $oClassRepository)
  {

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }

  public function doDefault()
  {
    // wczytaj identyfikator książki
    return $this->doGetSourceLocation();
  }

  /**
   * @return string
   */
  public function doGetSourceLocation()
  {
    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('scan_location', _('Wczytaj lokalizację źródłowa'), array('action' => phpSelf(array('do' => 'getEans'))), array('col_width' => 155), false);
    $oForm->AddText('source_location', _('Lokalizacja źródłowa'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
    return $oForm->ShowForm();
  }

  /**
   * @return bool|int
   */
  private function validateLocationForm($magazineLocation)
  {

    if (empty($_POST)) {
      // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
      return false;
    }

    // dolaczenie klasy walidatora
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    if (!$oValidator->Validate($_POST)) {
      $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
      // wystapil blad wypelnienia formularza
      // wyswietlenie formularza wraz z komunikatem bledu
      return false;
    }

    $containers = new Containers($this->pDbMgr, Containers::$TYPE_MAGAZINE_LOCATION);
    $containerType = $containers->checkContainer($magazineLocation);
    if (-2 === $containerType) {
      $this->sMsg .= GetMessage(_('Brak podanej lokalizacji'));
      return false;
    } else {
      return $containerType;
    }
  }

  /**
   * @return string
   */
  public function doGetEans()
  {

    $mContainerType = $this->validateLocationForm($_POST['source_location']);
    if ($mContainerType === false) {
      return $this->doDefault();
    }

    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('scan_location', _('Wczytaj lokalizację'), array('action' => phpSelf(array('do' => 'getDestinyLocation'))), array('col_width' => 155), false);
    $oForm->AddHidden('source_location', $_POST['source_location']);
    $oForm->AddHidden('source_location_type', $mContainerType);
    $oForm->AddTextArea('products_ean', _('EANy produktów'), $_POST['products_ean'], ['autofocus' => 'autofocus', 'style' => 'font-size: 45px; width:450px; height:600px']);
    $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
    return $oForm->ShowForm();
  }


  /**
   * @return string
   */
  public function doGetDestinyLocation()
  {

    if (empty($_POST)) {
      // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
      return $this->doDefault();
    }

    // dolaczenie klasy walidatora
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    if (!$oValidator->Validate($_POST)) {
      $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
      // wystapil blad wypelnienia formularza
      // wyswietlenie formularza wraz z komunikatem bledu
      return $this->doDefault();
    }

    $sEANs = $_POST['products_ean'];
    $aEANs = explode("\r\n", $sEANs);

    $products = [];
    foreach ($aEANs as $ean) {
      $ean = trim($ean);
      if ($ean != '') {
        $productId = $this->findProductByEAN($ean);
        if ($productId > 0) {
          $products[] = $productId;
        } else {
          $this->sMsg .= GetMessage(_('Nie znaleziono produktu o EAN "' . $ean . '" '));
          return $this->doDefault();
        }
      }
    }


    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('scan_location', _('Wczytaj lokalizację docelową'), array('action' => phpSelf(array('do' => 'changeLocations'))), array('col_width' => 155), false);
    $oForm->AddHidden('source_location', $_POST['source_location']);
    $oForm->AddHidden('source_location_type', $_POST['source_location_type']);
    $oForm->AddHidden('products_ids', implode(',', $products));
    $oForm->AddText('destiny_location', _('Lokalizacja docelową'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $oForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
    return $oForm->ShowForm();
  }


  /**
   * @return string
   */
  public function doChangeLocations()
  {
    $mDestinyContainerType = $this->validateLocationForm($_POST['destiny_location']);
    if ($mDestinyContainerType === false) {
      return $this->doDefault();
    }

    $sourceLocation = $_POST['source_location'];
    $sourceLocationType = $_POST['source_location_type'];
    $productsIds = $_POST['products_ids'];

    if ($mDestinyContainerType != $_POST['source_location_type']) {
      $this->sMsg .= GetMessage(_('Typy lokalizacji źródłowej i docelowej są inne'));
      return $this->doDefault();
    }

    $sNotChanged = '';
    $containers = new Containers($this->pDbMgr, Containers::$TYPE_MAGAZINE_LOCATION);
    $productsLocations = $containers->getProductsTypeLocations($productsIds, $sourceLocationType);
    foreach ($productsLocations as $productLocation) {
      if ($productLocation['location'] == $sourceLocation || $productLocation['location'] == '') {
        $containers->updateProductLocation($productLocation['id'], $_POST['destiny_location']);
      } else {
        $sNotChanged .= $productLocation['name'].' - '.$productLocation['ean_13'].' => '.$productLocation['location'].' <br />';
      }
    }

    if (!empty($sNotChanged)) {
      $this->sMsg .= GetMessage(_('Część lokalizacji produktów nie została zmieniona: <br /> ').$sNotChanged);
    } else {
      $this->sMsg .= GetMessage(_('Zmieniono lokalizację produktów'), false);
    }

    return $this->doDefault();
  }


  /**
   * @param $ean
   * @return array
   */
  private function findProductByEAN($ean)
  {

    $sSql = 'SELECT PS.id
             FROM products AS P
             JOIN products_stock AS PS
              ON P.id = PS.id
             WHERE 
                (
                  P.isbn_plain = "%1$s" OR 
                  P.isbn_10 = "%1$s" OR 
                  P.isbn_13 = "%1$s" OR 
                  P.ean_13 = "%1$s" OR 
                  P.streamsoft_indeks = "%1$s"
                )';
    $sSql = sprintf($sSql, $ean);
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }


  /**
   * @return string
   */
  public function getMsg()
  {

    $sJS = '<script>
      $(function() {
        var komunikat = $(".message1").html();
        if (komunikat !== undefined && komunikat !== "") {
          new Messi(komunikat, {title: "Błąd", modal: true});
        }
      });
      </script>';
    return $this->sMsg . $sJS;
  }
}
