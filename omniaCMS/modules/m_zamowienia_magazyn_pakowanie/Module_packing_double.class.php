<?php
/**
 * Klasa odpowiedzialna za drukowanie singli
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-10-22 
 * @copyrights Marcin Chudy - Profit24.pl
 */

use Assets\MagazineRemarksBlockJavascript;
use LIB\EntityManager\Entites\OrdersPacking;
use magazine\Gratis;
use omniaCMS\modules\m_zamowienia_magazyn_pakowanie\Module__zamowienia_magazyn_pakowanie__packing;

include_once($_SERVER['DOCUMENT_ROOT'].'omniaCMS/modules/m_zamowienia_magazyn_sorter/Module_sorter.class.php');
class Module__zamowienia_magazyn_pakowanie__packing_double extends Module__zamowienia_magazyn_sorter__sorter {

  // komunikat
	public $sMsg;

	// nazwa modulu - do langow
	public $sModule;

	// id wersji jezykowej
	public $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	private $aPrivileges;
	
  // id uzytkownika
  private $iUId;
  
  /**
   *
   * @var \DatabaseManager $pDbMgr
   */
  public $pDbMgr;
  
  /**
   *
   * @var \Module
   */
  private $oModuleZamowienia;
  
  /**
   *
   * @var \orders\getTransportList
   */
  protected $oGetTransport;
  
  private $oAdmin;

  private $orderPacking;
  
  function __construct(&$pSmarty, $bInit = true) {
    //parent::__construct($pSmarty, false);
    global $pDbMgr, $aConfig;
    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
    $this->pSmarty = $pSmarty;
    $this->pDbMgr = $pDbMgr;

    $this->orderPacking = new OrdersPacking($this->pDbMgr);

      $this->sMsg .= '<script type="text/javascript">
$("body").children().css("background", "pink");
$("td").css("background", "pink!important");
</script>'.getMessage(_('UWAGA PAKUJESZ DOUBLE'));
    
		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];

		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		$this->sModule .= '_alert';
    
		$this->iUId = $_SESSION['user']['id'];
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

    include_once('modules/m_zamowienia/Module.class.php');
    $pSmartyyyy = $oParent = new stdClass();
    $this->oModuleZamowienia = new Module($pSmartyyyy, $oParent, true);
    
    $this->oGetTransport = new \orders\getTransportList($this->pDbMgr);
    
    
    if ($bInit == false) {
      return;
    } else {
      $this->oAdmin = $bInit;
    }

    if (isset($_POST['pack_number']) && $_POST['pack_number'] != '') {
      if ($this->setOILIdSession($pSmarty, $_POST['pack_number']) === false) {
        return false;
      }
    }

    $iOILId = $this->getOILIdSession();

    switch ($sDo) {
      case 'do':
      default:
        // wyświetlamy formularz wprowadzania numeru kuwety
        $this->GetPackageNumber($pSmarty);
      break;
    
      case 'setPackage':
        // wyświetlamy formularz pobrania ISBN
        $this->getIsbnForm($pSmarty, $iOILId);
      break;
    
      case 'get_product':
        // pobieramy list przewozowy wyświetlamy towar i pobieramy nr listu przewozowego
        $this->showProductGetSecondProduct($pSmarty, $iOILId);

      break;

      case 'confirm_second_product':
        $this->showProductGetTransport($pSmarty, $iOILId);
      break;
    
      case 'confirm_transport':
        // potwierdzamy zamówienie
          try {
              $this->confirmOrderTransportNumber($pSmarty, $iOILId);
          } catch (Exception $ex) {
              $this->sMsg .= $ex->getMessage();
              return $this->GetPackageNumber($pSmarty);
          }
      break;
    
      case 'get_invoice_shelf':
        // potwierdzamy zamówienie 
        $sHTML = $this->veryfiInvoiceConfirmTransport($pSmarty, $iOILId);
          if (!empty($this->sMsg) && !stristr($sHTML, $this->sMsg)) {
              $sAddMSG = $this->sMsg;
          }
        $pSmarty->assign('sContent', (!empty($sAddMSG) ? $sAddMSG : '').ShowTable($sHTML));
      break;
    
      case 'close_list':
        // zamykamy listę
        // zwalniamy kuwetę
        // wykazujemy braki
        $this->closeListNewBack($pSmarty, $iOILId);
      break;

      case 'choose_package':
          $sHTML = $this->__doChoosePackage();
          if (!empty($this->sMsg) && !stristr($sHTML, $this->sMsg)) {
              $sAddMSG = $this->sMsg;
          }
          $pSmarty->assign('sContent', (!empty($sAddMSG) ? $sAddMSG : '').ShowTable($sHTML));
        break;

        case 'confirmGratis':
          $sAddMSG = '';
          $sHTML = $this->__doConfirmGratis();
          if (!empty($this->sMsg) && !stristr($sHTML, $this->sMsg)) {
              $sAddMSG = $this->sMsg;
          }
          $pSmarty->assign('sContent', (!empty($sAddMSG) ? $sAddMSG : '').ShowTable($sHTML));
        break;

        case 'confirmSecondGratis':
            $sHTML = $this->__doConfirmSecondGratis();
            if (!empty($this->sMsg) && !stristr($sHTML, $this->sMsg)) {
                $sAddMSG = $this->sMsg;
            }
            $pSmarty->assign('sContent', (!empty($sAddMSG) ? $sAddMSG : '').ShowTable($sHTML));
        break;
    }
  }// end of __construct

    /**
     * Metoda wyśw widok pobierania numeru paczki
     *
     * @param object $pSmarty
     */
    public function GetPackageNumber(&$pSmarty)
    {
        global $aConfig;

        if ($this->sMsg != '') {
            setSessionMessage($this->sMsg);
        }
        doRedirect($aConfig['common']['base_url_https'].'/'.phpSelf(array('module'=>'m_zamowienia_magazyn_sorter', 'action' => 'sorter_single')));
    }
  
  /**
   * 
   * @param Smarty $pSmarty
   * @param int $iOILId
   * @return string HTML
   */
  private function veryfiInvoiceConfirmTransport($pSmarty, $iOILId) {
    $iOrderId = $_GET['id'];
    $sInvoiceId = $_POST['invoice_id'];
    $iOILIId = $_POST['OILI_ID'];
    $iOILIIdSECOND = $_POST['OILI_ID_SECOND'];
    $iOILId = $_POST['OILId'];
    
    if ($this->checkInvoice($iOrderId, $sInvoiceId)) {
      $iFVQuantity = $this->getFVToPrintQuantity($iOILIId);
      $iLeftFVQuantity = $iFVQuantity - 1;
      $this->setFVToPrintQuantity($iOILIId, $iLeftFVQuantity);
      if ($iLeftFVQuantity > 0) {
        return $this->GetScanFVView($pSmarty, $iOILId, $iOrderId, $iOILIId, $iOILIIdSECOND);
      } else {
        $aOrderListData = $this->getOrderSelData($iOrderId);

        $sSqlCol = '(SELECT CONCAT(directory, "|", photo) 
                     FROM products_images AS PI 
                     JOIN orders_items AS OI
                     ON OI.product_id = PI.product_id
                     WHERE OI.id = OILI.orders_items_id
                     LIMIT 1
                     ) AS photo';
        $aProduct = $this->getProductDataByOILIId($iOILIId, $iOILId, array('OILI.id', 'OILI.orders_id', $sSqlCol, 'OILI.isbn_plain', 'OILI.isbn_10', 'OILI.isbn_13', 'OILI.ean_13', 'OI.name', 'OI.authors', 'OI.publisher'));

          $sSqlCol = '(SELECT CONCAT(directory, "|", photo) 
                     FROM products_images AS PI 
                     JOIN orders_items AS OI
                     ON OI.product_id = PI.product_id
                     WHERE OI.id = OILI.orders_items_id
                     LIMIT 1
                     ) AS photo';
          $aSecondProduct = $this->getProductDataByOILIId($iOILIIdSECOND, $iOILId, array('OILI.id', 'OILI.orders_id', $sSqlCol, 'OILI.isbn_plain', 'OILI.isbn_10', 'OILI.isbn_13', 'OILI.ean_13', 'OI.name', 'OI.authors', 'OI.publisher'));

          if (!empty($aProduct)) {
          $aProduct = $this->getPhotoArr($aProduct);
          $aSecondProduct = $this->getPhotoArr($aSecondProduct);
          return $this->confirmTransportProductView($pSmarty, $iOILId, $aOrderListData, $aProduct, $aSecondProduct);
        } else {
          $this->sMsg .= GetMessage(_('Brak produktu'));
        }
      }
    } else {
      $this->sMsg .= GetMessage(_('Błędny numer FV'));
      return $this->GetScanFVView($pSmarty, $iOILId, $iOrderId, $iOILIId, $iOILIIdSECOND);
    }
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param string $sInvoiceId
   * @return bool
   */
  private function checkInvoice($iOrderId, $sInvoiceId) {
    
    $sSql = 'SELECT id
             FROM orders AS O
             WHERE id = '.$iOrderId.' AND 
               (
                O.invoice_id = "'.$sInvoiceId.'" OR 
                O.invoice2_id = "'.$sInvoiceId.'"
               )';
    return ($this->pDbMgr->GetOne('profit24', $sSql) > 0);
  }
  
  /**
   * 
   * @param Smarty $pSmarty
   * @param int $iOILId
   * @return void
   */
  protected function GetScanFVView(Smarty $pSmarty, $iOILId, $iOrdersId, $iOILIItemId, $iOILIIdSECOND) {
    
    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('scan_fv', _('Wczytaj FV / ВВЕДИТЕ КОД НАКЛАДНОЙ'), array('action' => phpSelf(array('id' => $iOrdersId, 'do' => 'get_invoice_shelf'))), array('col_width'=>155), false);
    $oForm->AddText('invoice_id', _('Faktura numer'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
    $oForm->AddInputButton('save',_('Zapisz'), array('style' => 'font-size: 25;'));
    $oForm->AddHidden('OILI_ID', $iOILIItemId);
    $oForm->AddHidden('OILI_ID_SECOND', $iOILIIdSECOND);
    $oForm->AddHidden('OILId', $iOILId);
    $sHTML = $oForm->ShowForm().$this->checkGetHTMLDelicate($iOrdersId);
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($sHTML));
    return $sHTML;
  }
  
  
  /**
   * Metoda ma za zadanie zamknąć aktualną kuwetę, wykazać braki i powrócić je do '---'
   * 
   * @param \Smarty $pSmarty
   * @param int $iOILId
   */
  private function closeListNewBack($pSmarty, $iOILId) {
    $bIsErr = false;

    $aGoBack = [];
    require_once('OrderRecount.class.php');
    $oOrderRecount = new OrderRecount();
        
    
    $this->pDbMgr->BeginTransaction('profit24');
    
    // wycofujemy do '---' pozycje które nie zostały potwierdzone na tej liście
    $sSql = 'SELECT orders_items_id, orders_id
             FROM orders_items_lists_items
             WHERE orders_items_lists_id = '.$iOILId.'
                   AND confirmed = "0"';
    $aOrdersItemsIds = $this->pDbMgr->GetAll('profit24', $sSql);
    
    foreach ($aOrdersItemsIds as $aItemUselRow) {
      $aGoBack[] = $aItemUselRow['orders_items_id'];
      // wycofujemy do nowych produkty, które nie zostały zeskanowane w obrębie tej listy
      if ($this->backNewOrderItem($oOrderRecount, $aItemUselRow['orders_items_id'], $aItemUselRow['orders_id']) === false) {
        $bIsErr = true;
      }
    }
    if ($this->closeList($iOILId) === false) {
      $bIsErr = true;
    }
    
    
    if ($bIsErr === false) {
      // ok 
      $this->pDbMgr->CommitTransaction('profit24');
      $sMsg = _('Lista została zamknięta, brakujące pozycje zostały cofnięte do nowych ('.implode(',', $aGoBack).') Pobierz następną kuwetę');
      addLog($sMsg, false);
      $this->sMsg .= GetMessage($sMsg, false);
    } else {
      $this->pDbMgr->RollbackTransaction('profit24');
      $sMsg = _('Wystąpił błąd podczas zamykania dostawy ('.implode(',', $aGoBack).') ');
      addLog($sMsg);
      $this->sMsg .= GetMessage($sMsg);
    }
    $this->GetPackageNumber($pSmarty);
  }// end of closeListNewBack() method
  
  
  /**
   * Metoda powoduje zatwierdzenie zamówienia oraz jeśli nie istnieje to pobranie listu przewozowego i jego wydrukwoanie
   * 
   * @param \Smarty $pSmarty
   * @param int $iOILId
   * @throws Exception
   */
  private function confirmOrderTransportNumber(&$pSmarty, $iOILId) {
    global $aConfig;
    
    $bIsErr = false;
    // sprawdźmy czy wszystko się zgadza...
    $aOListData = $this->getConfirmOrder($iOILId, $_POST['OILI_ID'], $_POST['transport_number']);

    $aOListDataSecond = $this->getConfirmOrder($iOILId, $_POST['OILI_ID_SECOND'], $_POST['transport_number']);

    if (!empty($aOListData)) {
      $iOId = $aOListData['id'];
        if (true === $this->orderPacking->exist($iOId)) {

            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas pakowania - list przewozowy został już wcześniej potwierdzony <br /> Zgłoś to pilnie przełożonemu.'));
            return $this->GetPackageNumber($pSmarty);
        }

      $iOILIId = $_POST['OILI_ID'];
      $iOILIIdSECOND = $_POST['OILI_ID_SECOND'];
      $this->pDbMgr->BeginTransaction('profit24');
      // ok zatwierdzamy to zamówienie, aby przeszło do paczkomaty/paczki/odbior osobisty
      include_once('modules/m_zamowienia/Module.class.php');
      $pSmartyy = $oParent = new stdClass();
      $oModuleZamowienia = new Module($pSmartyy, $oParent, true);
      require_once('OrderRecount.class.php');
      $oOrderRecount = new OrderRecount();
      
      try {
          if ($iOILIId == $iOILIIdSECOND) {
              if ($this->updateOILItem($iOILIId, 1, 2) === false) {
                  throw new Exception(_('Wystąpił błąd podczas zatwierdznia zeskanowanej ilości na liście ' . $iOILIId));
              }

              if ($this->updateConfirmItem($aOListData['orders_items_id']) === false) {
                  throw new Exception(_('Wystąpił błąd podczas potwierdzania składowej w DB '.$aOListData['orders_items_id']));
              }

          } else {

              if ($this->updateOILItem($iOILIId, 1, 1) === false) {
                  throw new Exception(_('Wystąpił błąd podczas zatwierdznia zeskanowanej ilości na liście ' . $iOILIId));
              }

              if ($this->updateOILItem($iOILIIdSECOND, 1, 1) === false) {
                  throw new Exception(_('Wystąpił błąd podczas zatwierdznia zeskanowanej ilości na liście ' . $iOILIIdSECOND));
              }

              if ($this->updateConfirmItem($aOListData['orders_items_id']) === false) {
                  throw new Exception(_('Wystąpił błąd podczas potwierdzania składowej w DB '.$aOListData['orders_items_id']));
              }

              if ($this->updateConfirmItem($aOListDataSecond['orders_items_id']) === false) {
                  throw new Exception(_('Wystąpił błąd podczas potwierdzania składowej w DB '.$aOListDataSecond['orders_items_id']));
              }
          }
        


        if ($oOrderRecount->recountOrder($iOId, false, true, false) === false) {
          throw new Exception(_('Wystąpił błąd podczas przeliczania zamówienia '.$iOId));
        }
      
        $aLinkedOrders = $oModuleZamowienia->getOrdersLinked($iOId);
        $aOrder = $oModuleZamowienia->getOrderData($iOId);
        $aOrderListData = $this->getOrderSelData($iOId);

        $this->confirmOrder($oModuleZamowienia, $iOId, $aOrder, ($aOrderListData['transport_symbol'] == 'odbior-osobisty' ? true : false), $aLinkedOrders);
        $this->setStatusAsConfirmed($iOId, $aOrder);
        if ($oOrderRecount->recountOrder($iOId, true, true, true) === false) {
          throw new Exception(_('Wystąpił błąd podczas przeliczania zamówienia '.$iOId));
        }
        
        include_once('modules/m_zamowienia_magazyn_pakowanie/Module_packing.class.php');
        $oPacking = new omniaCMS\modules\m_zamowienia_magazyn_pakowanie\Module__zamowienia_magazyn_pakowanie__packing($this->oAdmin);
        $oPacking->addTimes($iOId, 9);


          $this->orderPacking->add($iOId, $_POST['transport_number']);
      
        $this->pDbMgr->CommitTransaction('profit24');
      } catch (\Exception $ex) {
        $this->pDbMgr->RollbackTransaction('profit24');
        $sMsg = _('Wystąpił błąd podczas potwierdznia zamówienia <br />'.$ex->getMessage());
        $bIsErr = true;
      }
    } else {
      // jakiś hujowy ten nr tansportu
      $this->pDbMgr->RollbackTransaction('profit24');
      $sMsg = _('Numer listu przewozowego '.$_POST['transport_number'].' nie pasuje do tego produktu na liście '.$_POST['OILI_ID'].' ... <br /> Nie psuj ;)');
      AddLog($sMsg, false);
      $this->sMsg .= GetMessage($sMsg, false);
      unset($_POST);
      return $this->GetPackageNumber($pSmarty);
      die;
    }
    
    if ($bIsErr === false) {
      // sprawdźmy czy z tej listy zostały jakieś zamówienia, które nie są potwierdzone
      if ($this->checkListHaveOpenedOrders($iOILId) === false) {
        // jeśli już nie ma to oznacza to ze możemy zamknąć dane kuwetę
        include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
        $oContainers = new \magazine\Containers($this->pDbMgr);
        $oContainers->unlockContainer($aOListData['package_number']);
        $this->closeList($iOILId);
        $sMsg = _('Cała kuweta została potwierdzona, rozpocznij następną.');
        AddLog($sMsg, false);
        $this->sMsg .= GetMessage($sMsg, false);
        return $this->GetPackageNumber($pSmarty);
      }
    }
    
    if ($bIsErr === false) {
      // ok 
      $sMsg = _('Zatwierdzono zamówienie / ЗАКАЗ БЫЛ УСПЕШНО ПОТДВЕРЖДЁН '.$aOrder['order_number']);
      AddLog($sMsg, false);
      $this->sMsg .= GetMessage($sMsg, false);
    } else {
      // err
      AddLog($sMsg);
      $this->sMsg .= GetMessage($sMsg);
    }
    $this->getIsbnForm($pSmarty, $iOILId);
  }// end of confirmOrderTransportNumber() method
  
  
  /**
   * 
   * @param int $iOId
   * @param array $aOrder
   * @return boolean
   */
  private function setStatusAsConfirmed($iOId, $aOrder) {
    
    if($aOrder['order_status'] == '2'){
      $aValues['order_status'] = '3';
      $aValues['status_3_update'] = 'NOW()';
      $aValues['status_3_update_by'] = $_SESSION['user']['name'];
      return $this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOId);
    } else {
      return true;
    }
  }
  
  /**
   * Metoda sprawdza, czy w obrębie danej listy znajdują sie jeszcze jakieś zamówienia które są różne od zatwierdzone zrealizowane i anulowane
   * 
   * @param int $iOILId
   * @return bool
   */
  private function checkListHaveOpenedOrders($iOILId) {
    
    $sSql = 'SELECT OILI.id 
             FROM orders_items_lists_items AS OILI
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status NOT IN("3", "4", "5")
             WHERE OILI.orders_items_lists_id = '.$iOILId.'
             LIMIT 1';
    return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
  }// end of checkListHaveOpenedOrders() method
  
  
  /**
   * Metoda pobiera numer id zamówienia, którego numer listu przewozowego oraz numer listy został podany
   * 
   * @param int $iOILId id aktualnie wybranej listy na podstawie nr kuwety
   * @param int $iOILIId id elementu na liscie
   * @param string $sTransportNumber
   * @return array
   */
  private function getConfirmOrder($iOILId, $iOILIId, $sTransportNumber) {
    
    $sSql = 'SELECT O.id, OILI.orders_items_id, OIL.package_number
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OILI.orders_items_lists_id = OIL.id
             JOIN orders AS O
              ON O.id = OILI.orders_id
             WHERE 
                OIL.id = '.$iOILId.'
                AND OILI.id = '.$iOILIId.' 
                ' . ( $sTransportNumber != '' ? ' AND O.transport_number = "'.$sTransportNumber.'" ' : ' AND O.transport_number IS NULL ') .'
                AND OILI.confirmed = "0" ';
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of getConfirmOrder() method
  
  
  /**
   * Metoda ustawia w sesji id 
   * 
   * @param string $sPackageNumber
   * @return bool
   */
  private function setOILIdSession($pSmarty, $sPackageNumber) {
    global $aConfig;
    
    $aPackage = $this->getOrdersItemsListsByPackageIdent($sPackageNumber, array('id', 'closed', 'package_number'), false, true, true);
    if (!empty($aPackage)) {
      
      switch ($aPackage['closed']) {
        case '3':
          $this->packageWaiting($pSmarty, $aPackage);
          return false;
        break;
      
        default:
          // XXX TODO Tutaj tylko tymczasowo, powinno zostać zablokowane po zakończeniu sortowania
          include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
          $oContainers = new \magazine\Containers($this->pDbMgr);
          $oContainers->unlockContainer($sPackageNumber);


          $_SESSION['user']['sorted_single_orders_items_list'] = $aPackage['id'];
          // ok można kontynuować
          return true;
        break;
      }
    } else {
      $sMsg = _('Podano nieprawidłowy numer kuwety: /<br>НЕПРАВИЛЬНЫЙ НОМЕР ЯЩИКА:'.$sPackageNumber);
      $this->sMsg .= GetMessage($sMsg);
      AddLog($sMsg);
      $this->GetPackageNumber($pSmarty);
      return false;
    }
  }// end of setOILIdSession() method
  
  
  /**
   * Metoda pobiera id listy z sesji
   * 
   * @return int|bool
   */
  private function getOILIdSession() {

    if (isset($_SESSION['user']['sorted_single_orders_items_list']) && $_SESSION['user']['sorted_single_orders_items_list'] > 0) {
      return $_SESSION['user']['sorted_single_orders_items_list'];
    } else {
      return false;
    }
  }// end of getOILIdSession() method
  
  /**
   * Metoda wyświetla formularz wprowadzenia kodu isbn produktu
   * 
   * @param \Smarty $pSmarty
   */
  private function getIsbnForm(&$pSmarty, $iOILId) {
    if ($iOILId === false) {
      $sMsg = _('Brak id listy w sesji');
      $this->sMsg .= GetMessage($sMsg);
      AddLog($sMsg);
      return $this->GetPackageNumber($pSmarty);
    }

    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('get_product_isbn', _('Zeskanuj ISBN EAN'), array('action' => phpSelf(array('id' => $iOILId, 'do' => 'get_product'))), array('col_width'=>155), true);
    $pForm->AddText('ISBN', _('ISBN \ EAN'), '', array('style' => 'font-size: 25px;'), '', 'text', true);
    $pForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25px;'));
    
    $sJS = '
    <script type="text/javascript">
      $(function() {
        $("#ISBN").focus();
      });
    </script>';
    
    $sCloseForm = $this->addEndList($iOILId);
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm().$sCloseForm));
  }// end of getIsbnForm() method
  
  
  /**
   * Metoda towrzy formularz zamknięcia listy
   * 
   * @param int $iOILId
   * @return string - HTML
   */
  private function addEndList($iOILId) {
    global $aConfig;

    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('end_of_list', _(''), array('action' => phpSelf(array('id' => $iOILId, 'do' => 'close_list'))), array('col_width'=>155), true);

    if ($_SESSION['user']['type'] != '0' ||
        in_array($_SESSION['user']['name'], ['agolba', 'mtopolowski', 'mtopolowskip', 'dwieprzkowicz', 'dwieprzkowiczp', 'akrynicka', 'akrynickap', 'kkowalczyk', 'kkowalczykp'])
    )
    {
        $pForm->AddHidden('do', 'close_list');
        $pForm->AddInputButton('save', _('Zamknij listę'), array('style' => 'float: right; font-size: 25px; background-color: #AAAAAA'));
    }

    $aProducts = $this->getItemsLeft($iOILId);
      $iSum = 0;
      foreach ($aProducts as $product) {
          $iSum += $product['quantity'];
      }
    $sLeftButtonHTML = $pForm->GetInputButtonHTML('show_left', _('Pokaż pozostałe do zeskanowania / ПОКАЗАТЬ ОСТАВШИЕСЯ ЗАКАЗЫ'), array('style' => 'font-size: 40px;', 'onclick' => 'showLeft()'), 'button');
    $pForm->AddMergedRow('
      <div>Pozostało do zeskanowania /<br>ОСТАЛОСЬ ВВЕСТИ</br>:</div> 
      <span style="color: red; font-size: 80px;">'.$iSum.'</span><br />'.
            $sLeftButtonHTML
            
            , array('style' => 'vertical-align: center; float: none; text-align: center;'));
    if (count($aProducts) <= 6) {
        $sHTML = '<div id="left_products" style="display: none;">';
        foreach ($aProducts as $iKey => $aProduct) {
          $iCount = $iKey + 1;
          $sHTML .= '<hr /><br /><b> Produkt: '.$iCount.'</b><br />';
          $aImg = explode('|', $aProduct['photo']);
          $sImg = 'https://profit24.pl/'.$aConfig['common']['photo_dir'].'/'.$aImg[0].'/'.$aImg[1];
          $sHTML .= '<img src="'.$sImg.'" style="float: left;  padding: 20px;" /><br />';
          $sHTML .= '<div>';
          $sHTML .= 'Tytuł '.$iCount.': <b>'. $aProduct['name'].'</b><br />';
          $sHTML .= 'ISBN '.$iCount.': <b>'. $aProduct['isbn_plain'].'</b><br />';
          $sHTML .= '</div><div class="clear"></div>';
        }
        $sHTML .= '</div>';
        $sJS = '<script type="text/javascript">
          function showLeft() {
              $("#left_products").show();
          }
    </script>';
    }
    return $pForm->ShowForm().$sHTML.$sJS;
  }// end of addEndList() method
  
  
  /**
   * Pobieramy produkty, które pozostały do zeskanowania
   * 
   * @param int $iOILId
   * @return array
   */
  private function getItemsLeft($iOILId) {

    $sSqlCol = '(SELECT CONCAT(directory, "|", photo) 
                 FROM products_images AS PI 
                 JOIN orders_items AS OI
                 ON OI.product_id = PI.product_id
                 WHERE OI.id = OILI.orders_items_id
                 LIMIT 1
                 ) AS photo';
    $aProducts = $this->getProductListItems($iOILId, array('OILI.quantity', 'OILI.id', 'OILI.orders_id', $sSqlCol, 'OILI.isbn_plain', 'OILI.isbn_10', 'OILI.isbn_13', 'OILI.ean_13', 'OI.name', 'OI.authors', 'OI.publisher'));
    return $aProducts;
  }// end of getItemsLeft() method


    /**
     * @param $pSmarty
     * @param $iOILId
     */
    private function showProductGetSecondProduct(&$pSmarty, $iOILId) {
        global $aConfig;

        if ($iOILId === false) {
            $sMsg = _('Brak id listy w sesji');
            $this->sMsg .= GetMessage($sMsg);
            AddLog($sMsg);
            return $this->GetPackageNumber($pSmarty);
        }

        $sSqlCol = '(SELECT CONCAT(directory, "|", photo) 
                 FROM products_images AS PI 
                 JOIN orders_items AS OI
                 ON OI.product_id = PI.product_id
                 WHERE OI.id = OILI.orders_items_id
                 LIMIT 1
                 ) AS photo';
        $aProduct = $this->getProductDataByIsbn($_POST['ISBN'], $iOILId, array('OILI.id', 'OILI.quantity', 'OILI.orders_id', $sSqlCol, 'OILI.isbn_plain', 'OILI.isbn_10', 'OILI.isbn_13', 'OILI.ean_13', 'OI.name', 'OI.authors', 'OI.publisher'));

        if ($aProduct['orders_id'] > 0) {
            if (true === $this->orderPacking->exist($aProduct['orders_id'])) {

                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas pakowania - list przewozowy został już wcześniej potwierdzony <br /> Zgłoś to pilnie przełożonemu.'));
                return $this->GetPackageNumber($pSmarty);
            }
        } else {
            $sMsg = _('Brak produktów o podanym ISBN: '.$_POST['ISBN'].' Numer listu: '.$iOILId);
            $this->sMsg .= GetMessage($sMsg);
            AddLog($sMsg);
            return $this->getIsbnForm($pSmarty, $iOILId);
        }

        if ($aProduct['quantity'] == 2) {
            $aSecondProduct = $aProduct;
        } else {
            $sSqlCol = '(SELECT CONCAT(directory, "|", photo) 
                 FROM products_images AS PI 
                 JOIN orders_items AS OI
                 ON OI.product_id = PI.product_id
                 WHERE OI.id = OILI.orders_items_id
                 LIMIT 1
                 ) AS photo';
            $aSecondProduct = $this->getSecondProductDataByIsbn($_POST['ISBN'], $aProduct['orders_id'], $iOILId, $aProduct['id'], array('OILI.id', 'OILI.orders_id', $sSqlCol, 'OILI.isbn_plain', 'OILI.isbn_10', 'OILI.isbn_13', 'OILI.ean_13', 'OI.name', 'OI.authors', 'OI.publisher'));
        }

        if (!empty($aSecondProduct)) {

            /*
            if ($aSecondProduct['orders_id'] > 0) {
                $mSeconds = $this->getOrderExhaustionTime($aSecondProduct['orders_id']);
                if ($mSeconds !== true) {
                    $sMsg .= sprintf(_('Zamówienie jest zablokowane do ponownego wskanowania, spróbuj ponownie za %s sekund'), $mSeconds);
                    $this->sMsg .= GetMessage($sMsg);
                    AddLog($sMsg);
                    return $this->getIsbnForm($pSmarty, $iOILId);
                }
            }
            */

            include_once('modules/m_zamowienia_magazyn_pakowanie/Module_packing.class.php');
            $oPacking = new omniaCMS\modules\m_zamowienia_magazyn_pakowanie\Module__zamowienia_magazyn_pakowanie__packing($this->oAdmin);
            $oPacking->addTimes($aSecondProduct['orders_id'], 0);

            $_SESSION['single_order']['product'] = json_encode($aProduct);
            $_SESSION['single_order']['second_product'] = json_encode($aSecondProduct);
            $_SESSION['single_order']['iOILId'] = $iOILId;
            // ok, potwierdzamy drugi
            $sHTML = $this->getProductInfoGetISBN($aSecondProduct, $iOILId);
            $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($sHTML));
        } else {
            $sMsg = _('Brak produktów o podanym ISBN: '.$_POST['ISBN'].' Numer listu: '.$iOILId);
            $this->sMsg .= GetMessage($sMsg);
            AddLog($sMsg);
            return $this->getIsbnForm($pSmarty, $iOILId);
        }
    }

    /**
     * @param $aProduct
     * @return string
     */
    private function getProductInfoGetISBN($aProduct, $iOILId) {
        global $aConfig;

        $sHTML = '';
//        $second = array (
//            'id' => '378',
//            'orders_id' => '737',
//            'photo' => 'okladki/867/866369|9788326480812.jpg',
//            'isbn_plain' => '9788326480812',
//            'isbn_10' => '',
//            'isbn_13' => '9788326480812',
//            'ean_13' => '9788326480812',
//            'name' => 'Metodyka pracy sędziego w sprawach administracyjnych',
//            'authors' => 'Borkowski Janusz, Adamiak Barbara',
//            'publisher' => 'Wolters Kluwer Polska',
//        );

        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('get_product_isbn', _('Zeskanuj ISBN EAN'), array('action' => phpSelf(array('id' => $iOILId, 'do' => 'confirm_second_product'))), array('col_width'=>155), true);
        $pForm->AddText('ISBN', _('ISBN \ EAN'), '', array('style' => 'font-size: 25px;'), '', 'text', true);
        $pForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25px;'));

        $aImg = explode('|', $aProduct['photo']);
        $sImg = 'https://profit24.pl/'.$aConfig['common']['photo_dir'].'/'.$aImg[0].'/__b_'.$aImg[1];
        $sHTML .= '<div style="text-align: center;">Znajdź drugi produkt /<br>НАЙДИТЕ ТАКУЮ ПАРУ К ДАННОМУ ЗАКАЗУ:<br /><br /><img src="'.$sImg.'"  style="padding: 20px; max-width: 200px;" /><br />';
        $sHTML .= 'Tytuł: <b>'. $aProduct['name'].'</b><br />';
        $sHTML .= 'ISBN: <b>'. $aProduct['isbn_plain'].'</b><br />';
        $sHTML .= '</div><div class="clear"></div>';

        $sJS = '
    <script type="text/javascript">
      $(function() {
        $("#ISBN").focus();
      });
    </script>';

        $pForm->AddMergedRow(
          $sHTML,
          array('style' => 'vertical-align: center; float: none; text-align: center;')
        );
        return $pForm->ShowForm().$sJS;
    }

  
  /**
   * Metoda pokazuje dane zeskanowanego produktu oraz drukuje list przewozowy zamówienia
   * 
   * @param \Smarty $pSmarty
   * @param int $iOILId
   * @return void
   */
  private function showProductGetTransport(&$pSmarty, $iOILId) {
    global $aConfig;

    $sMsg = '';

    if ($iOILId === false) {
      $sMsg = _('Brak id listy w sesji');
      $this->sMsg .= GetMessage($sMsg);
      AddLog($sMsg);
      return $this->GetPackageNumber($pSmarty);
    }

    $aProduct = [];
    if (!empty($_SESSION['single_order']['second_product'])) {
        $aSecondProduct = (array)json_decode($_SESSION['single_order']['second_product']);
        if ($_POST['ISBN'] != '') {
            if (
                $_POST['ISBN'] == $aSecondProduct['isbn_plain'] ||
                $_POST['ISBN'] == $aSecondProduct['isbn_10'] ||
                $_POST['ISBN'] == $aSecondProduct['isbn_13'] ||
                $_POST['ISBN'] == $aSecondProduct['ean_13']
            ) {
                // dobra jest to ten produkt
                $aProduct = (array)json_decode($_SESSION['single_order']['product']);
            } else {
                $sMsg = _('Brak produktów o podanym ISBN: '.$_POST['ISBN'].' Numer listu: '.$iOILId);
                $this->sMsg .= GetMessage($sMsg);
                AddLog($sMsg);
                return $this->getIsbnForm($pSmarty, $iOILId);
            }
        }
    }
    if (!empty($aProduct)) {
//      if ($aProduct['orders_id'] > 0) {
//        $mSeconds = $this->getOrderExhaustionTime($aProduct['orders_id']);
//        if ($mSeconds !== true) {
//          $sMsg .= sprintf(_('Zamówienie jest zablokowane do ponownego wskanowania, spróbuj ponownie za %s sekund'), $mSeconds);
//          $this->sMsg .= GetMessage($sMsg);
//          AddLog($sMsg);
//          return $this->getIsbnForm($pSmarty, $iOILId);
//        }
//      }

      include_once('modules/m_zamowienia_magazyn_pakowanie/Module_packing.class.php');
      $oPacking = new omniaCMS\modules\m_zamowienia_magazyn_pakowanie\Module__zamowienia_magazyn_pakowanie__packing($this->oAdmin);
      $oPacking->addTimes($aProduct['orders_id'], 0);

      $_SESSION['single_order']['second_product'] = json_encode($aSecondProduct);
//      $_SESSION['single_order']['product'] = json_encode($aProduct);
      $_SESSION['single_order']['iOILId'] = $iOILId;
      // ok
      $aGratis = $this->getGratis($aProduct['orders_id'], false);
      $aSecondGratis = $this->getGratis($aProduct['orders_id'], true);
      if (!empty($aGratis)) {
        // gratis
        $sHTML = $this->showGratis($aProduct['orders_id'], $aGratis, null, false);
      } elseif (!empty($aSecondGratis)) {
        $sHTML = $this->showGratis($aProduct['orders_id'], $aSecondGratis, null, true);
      } else {
        $sHTML = $this->getChoosePackage($aProduct['orders_id']);
      }

      return $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($sHTML));
    } else {
      $sMsg = _('Brak produktów o podanym ISBN: '.$_POST['ISBN'].' Numer listu: '.$iOILId);
      $this->sMsg .= GetMessage($sMsg);
      AddLog($sMsg);
      return $this->getIsbnForm($pSmarty, $iOILId);
    }
  }// end of showProductGetTransport() method



  /**
   * @param $iOrderId
   * @return string
   */
  public function getChoosePackage($iOrderId)
  {
      $aProduct = (array)json_decode($_SESSION['single_order']['product']);
      $aSecondProduct = (array)json_decode($_SESSION['single_order']['second_product']);

      $iOILId = $_SESSION['single_order']['iOILId'];
      unset($_SESSION['single_order']);
      // probojemy pobrać LP
      return $this->proceedFVTransport($this->pSmarty, $iOILId, $aProduct, $aSecondProduct);

      /*
      include_once('Form/FormTable.class.php');

    $pForm = new FormTable('choosePackage', _('Wybierz opakowanie'), array('action' => phpSelf(array('id' => $iOrderId))));
    $pForm->AddHidden('do', 'choose_package');
    $pForm->AddText('package', _('Kod opakowania'), '', array('style' => 'width: 500px; font-size: 26px;', 'autofocus' => 'autofocus'), '', 'uint', true);
    $pForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
    return $pForm->ShowForm().$this->checkGetHTMLDelicate($iOrderId);
      */
  }

  /**
   * @return string
   */
  public function __doChoosePackage() {

    $iOrderId = $_GET['id'];
    $iPackageCode = $_POST['package'];

    if ($this->checkPackage($iPackageCode) === false) {
      $this->sMsg .= GetMessage(_('Brak podanego opakowania o kodzie '.$iPackageCode));
      return $this->getChoosePackage($iOrderId);

    } else {
      $aProduct = (array)json_decode($_SESSION['single_order']['product']);
      $aSecondProduct = (array)json_decode($_SESSION['single_order']['second_product']);

      $iOILId = $_SESSION['single_order']['iOILId'];
      unset($_SESSION['single_order']);
      // probojemy pobrać LP
      return $this->proceedFVTransport($this->pSmarty, $iOILId, $aProduct, $aSecondProduct);
    }
  }


    /**
     * @param $iOrderId
     * @param $aGratis
     * @return string
     */
    public function showGratis($iOrderId, $aGratis = [], $gratisId, $bSecondGratis = false) {


        if (empty($aGratis)) {
            if ($gratisId > 0) {
                $aGratis = $this->getGratisById($gratisId);
            }
        }

        include_once('Form/FormTable.class.php');
        $gratisMsg = _('Gratis');
        if ($bSecondGratis) {
            $gratisMsg = _('<strong>Drugi</strong> gratis');
        }
        $pForm = new FormTable('confirm_transport_address', $gratisMsg, array('action' => phpSelf(array('id' => $iOrderId))));
        if (false === $bSecondGratis) {
            $pForm->AddHidden('do', 'confirmGratis');
        } else {
            $pForm->AddHidden('do', 'confirmSecondGratis');
        }
        $pForm->AddMergedRow('<div style="padding: 2em; text-align: center; font-size: 2em; color: red; ">Dołącz '.mb_strtolower($gratisMsg, 'UTF-8').' "'.$aGratis['name'].'" o kodzie '.$aGratis['code'].' </div>');
        $pForm->AddText('code', _('Kod gratisu'), '', array('style' => 'width: 300px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'uint', true);
        $pForm->AddHidden('gratis_validate_code', $aGratis['code']);
        $pForm->AddHidden('gratis_id', $aGratis['id']);

        $pForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
        return $pForm->ShowForm().$this->checkGetHTMLDelicate($iOrderId);
    }


    /**
     * @param $gratisId
     * @return array
     */
    private function getGratisById($gratisId) {

        $oGratis = new Gratis($this->pDbMgr);
        return $oGratis->getGratisById($gratisId);
    }


  /**
   * @return string
   * *
  public function __doConfirmGratis() {
    $iOrderId = $_GET['id'];
    $iGratisId = $_POST['gratis_id'];
    $iGratisCode = $_POST['gratis_validate_code'];
    $iPutCode = $_POST['code'];

    if ($iGratisCode !== $iPutCode || $iPutCode == '') {
      $this->sMsg .= GetMessage(_('Podany kod gratisu "'.$iPutCode.'" jest nieprawidłowoy, wpowadź kod ponownie'));
      unset($_POST);
      return $this->showGratis($iOrderId);
    } else {
      // kod ok, dodajemy do logów i generujemy LP
      $gratis = new Gratis($this->pDbMgr);
      $gratis->addUseGratisOrder($iOrderId, $iGratisId);
      $this->sMsg .= GetMessage(_('Dodano gratis "'.$iPutCode.'" '), false);


      $aProduct = (array)json_decode($_SESSION['single_order']['product']);
      return $this->getChoosePackage($aProduct['orders_id']);
    }
  }
*/
    /**
     * @return string
     */
    public function __doConfirmGratis($bIsSecond = false) {
        $iOrderId = $_GET['id'];
        $iGratisId = $_POST['gratis_id'];
        $iGratisCode = $_POST['gratis_validate_code'];
        $iPutCode = $_POST['code'];
        if ($iGratisCode !== $iPutCode || $iPutCode == '') {
            $this->sMsg .= GetMessage(_('Podany kod gratisu "'.$iPutCode.'" jest nieprawidłowoy, wpowadź kod ponownie'));
            unset($_POST);
            return $this->showGratis($iOrderId, [], $iGratisId, $bIsSecond);
        } else {
            // kod ok, dodajemy do logów i generujemy LP
            $gratis = new Gratis($this->pDbMgr);
            $gratis->addUseGratisOrder($iOrderId, $iGratisId);
            $secondMsg = '';
            if (true === $bIsSecond) {
                $secondMsg = 'drugi ';
            }
            $this->sMsg .= GetMessage(_('Dodano '.$secondMsg.'gratis "'.$iPutCode.'" '), false);
            $aProduct = (array)json_decode($_SESSION['single_order']['product']);

            if (false === $bIsSecond) {
                $aSecondGratis = $this->getGratis($iOrderId, true);
                if (!empty($aSecondGratis)) {
                    // pokaż form drugiego gratisu
                    return $this->showGratis($iOrderId, $aSecondGratis, null, true);
                } else {
                    // wybierz opakowanie
                    return $this->getChoosePackage($iOrderId);
                }
            } else {
                // wybierz opakowanie
                return $this->getChoosePackage($iOrderId);
            }
        }
    }

    /**
     * @return string
     */
    public function __doConfirmSecondGratis() {
        $bIsSecond = true;
        return $this->__doConfirmGratis($bIsSecond);
    }

  /**
   * @return bool|mixed
   */
  private function getGratis($iOrderId, $bSecondGratis) {

    $oGratis = new Gratis($this->pDbMgr, $bSecondGratis);
    return $oGratis->getGratis($iOrderId);
  }


  /**
   * @param $iPackageCode
   * @return bool
   */
  private function checkPackage($iPackageCode) {

    $sSql = 'SELECT id FROM magazine_packages WHERE code = "'.$iPackageCode.'"';
    $iId = $this->pDbMgr->GetOne('profit24', $sSql);

    if ($iId > 0) {
      $sSql = 'UPDATE magazine_packages SET count = count - 1 WHERE id = '.$iId;
      $this->pDbMgr->Query('profit24', $sSql);
      return true;
    } else {
      return false;
    }
  }


  /**
   * @param $pSmarty
   * @param $iOILId
   * @param $aProduct
   * @throws Exception
   */
  private function proceedFVTransport($pSmarty, $iOILId, $aProduct, $aSecondProduct) {

    $aProduct = $this->getPhotoArr($aProduct);
    $aSecondProduct = $this->getPhotoArr($aSecondProduct);
    $aOrderListData = $this->getOrderSelData($aProduct['orders_id']);

    $sTransportSymbol = $aOrderListData['transport_symbol'];

    $aOrderPrintInfo = $this->getCountPrintFV($aProduct['orders_id']);
    if ($aOrderPrintInfo['print_fv'] == '1') {
//        $aLinkedOrders = $this->oModuleZamowienia->getOrdersLinked($aProduct['orders_id']);
//        $aOrder = $this->oModuleZamowienia->getOrderData($aProduct['orders_id']);
//        if ($this->proceedConfirm($sTransportSymbol, $this->oModuleZamowienia, $this->oGetTransport, $aProduct['orders_id'], $aOrder, $aLinkedOrders) === false) {
//          return false;
//        }
      if ($sTransportSymbol != 'odbior-osobisty') {
        $this->printoutOrder($sTransportSymbol, $this->oModuleZamowienia, $this->oGetTransport, $aProduct['orders_id'], 1, $this->oModuleZamowienia->getOrderData($aProduct['orders_id']), array(), false);
        $this->PrintMultipleFV(array($aProduct['orders_id']));
      } else {
        $this->PrintMultipleFV(array($aProduct['orders_id']));
        return $this->confirmTransportProductView($pSmarty, $iOILId, $aOrderListData, $aProduct, $aSecondProduct);
      }
    }

    $aOrderPrintInfo = $this->getCountPrintFV($aProduct['orders_id']);
    if ($aOrderPrintInfo['SUM'] > 0 && $aOrderPrintInfo['print_fv'] == '1') {
      $this->setFVToPrintQuantity($aProduct['id'], $aOrderPrintInfo['SUM']);
      $this->AddInfoGetFV();
      return $this->GetScanFVView($pSmarty, $iOILId, $aProduct['orders_id'], $aProduct['id'], $aSecondProduct['id']);
    } else {
      // mamy produkt, wyświetlamy jak on wygląda, oraz wydrukujmy list przewozowy
      if ($sTransportSymbol != 'odbior-osobisty') {
        try {
          $this->pDbMgr->BeginTransaction('profit24');
          $this->printoutOrder($sTransportSymbol, $this->oModuleZamowienia, $this->oGetTransport, $aProduct['orders_id'], 1, $this->oModuleZamowienia->getOrderData($aProduct['orders_id']), array(), false);
          $this->pDbMgr->CommitTransaction('profit24');
          return $this->confirmTransportProductView($pSmarty, $iOILId, $aOrderListData, $aProduct, $aSecondProduct);
        } catch (\Exception $ex) {
          $sMsg = _('Wystąpił błąd podczas drukowania listu przewozowego i zatwierdzania zamówienia o id '.$aProduct['orders_id'].' <br />'.$ex->getMessage());
          $this->pDbMgr->RollbackTransaction('profit24');
          $this->sMsg .= GetMessage($sMsg);
          AddLog($sMsg);
          return $this->getIsbnForm($pSmarty, $iOILId);
        }
      } else {
        return $this->confirmTransportProductView($pSmarty, $iOILId, $aOrderListData, $aProduct, $aSecondProduct);
      }
    }
  }
  
  /**
   * 
   * @return int
   */
  private function getOrderExhaustionTime($iOrderId) {
    $iMinutesExhaustion = 5;
    
    $sSql = 'SELECT TIME_TO_SEC(TIMEDIFF(NOW(), created))
             FROM orders_packing_times
             WHERE status = "0" AND order_id = ' . $iOrderId . '
             ORDER BY id DESC';
    $iSeconds = $this->pDbMgr->GetOne('profit24', $sSql);
    
    if ($iSeconds > 0) {
      if (($iSeconds/60) < $iMinutesExhaustion) {
        return (($iMinutesExhaustion * 60) - $iSeconds);
      } else {
        return true;
      }
    } else {
      return true;
    }
  }
  
  
  /**
   * 
   * @global array $aConfig
   * @param array $aProduct
   * @return array
   */
  private function getPhotoArr($aProduct) {
    global $aConfig;
    
    if ($aProduct['photo'] != '') {
      $aPhotoData = explode('|', $aProduct['photo']);
      $aProduct['photo_path'] = 'https://www.profit24.pl/'.$aConfig['common']['photo_dir'].'/'.$aPhotoData[0];
      $aProduct['photo_name'] = $aPhotoData[1];
    }
    return $aProduct;
  }
  
  
  
  
  /**
   * 
   * @param int $iOId
   * @return char
   */
  private function getCountPrintFV($iOId) {
    
    $sSql = 'SELECT COUNT(invoice_id) + COUNT(invoice2_id) AS SUM, print_fv
      FROM orders 
      WHERE id = '.$iOId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of getOrderItemPrintFV() method
  
  
  /**
   * Metoda wyświetla widok produktu
   * 
   * @param \Smarty $pSmarty
   * @param int $iOILId
   * @param array $aOrderListData
   * @param array $aProduct
   * @return void
   */
  private function confirmTransportProductView($pSmarty, $iOILId, $aOrderListData, $aProduct, $aSecondProduct) {
    // serwis,
    // odbiór osobisty,
    // okładka,
    // potwierdź nr listu przewozowego.
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('confirm_transport', _('Potwierdzenie listu przewozowego zamówienia'), array('action'=>phpSelf(array('do' => 'confirm_transport'))), array('col_width'=>155), true);

    if ($aOrderListData['transport_symbol'] != "odbior-osobisty") {
      $pForm->AddText('transport_number', _('Numer listu przewozowego'), '', array('style' => 'font-size: 45px;'), '', 'test', true);
    } else {
      $pForm->AddInputButton('ok', _('Odłożyłem na półkę z odbiorem osobistym / ОТЛОЖИТЬ НА ПОЛКУ С ПРИВАТНЫМ ОТБОРОМ'), array('style' => 'font-size: 45px;'));
    }
    $pForm->AddMergedRow($this->getProductView($pSmarty, $aOrderListData, $aProduct, $aSecondProduct));
    if (false === empty($aOrderListData['magazine_remarks'])){

    }
    $remarksHiddenValue = false === empty($aOrderListData['magazine_remarks']) ? 0 : 1;

    $pForm->AddHidden('OILI_ID', $aProduct['id']);
    $pForm->AddHidden('OILI_ID_SECOND', $aSecondProduct['id']);
    $pForm->AddHidden('remarks_has_been_read', $remarksHiddenValue);

    $magazineRemarksBlock = new MagazineRemarksBlockJavascript();

    $sJS = $magazineRemarksBlock->getJavascript();

    $sCloseForm = $this->addEndList($iOILId);
    $iOrderId = $aProduct['orders_id'];
    $okButton = $magazineRemarksBlock->getHiddenInput();
    return (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()).$okButton.ShowTable($sCloseForm).$this->checkGetHTMLDelicate($iOrderId);
  }// end of confirmTransportProductView() method
  
  
  /**
   * Metoda generuje widok pojedynczego produktu
   * 
   * @param \Smarty $pSmarty
   * @param array $aOrderListData
   * @param array $aProduct
   * @return string
   */
  private function getProductView(&$pSmarty, $aOrderListData, $aProduct, $aSecondProduct) {

    $pSmarty->assign('aProduct', $aProduct);
    $pSmarty->assign('aSecondProduct', $aSecondProduct);
    $pSmarty->assign('aData', $aOrderListData);
    $sHtml = $pSmarty->fetch('sorter_double.tpl');
    $pSmarty->clear_assign('aBooks');
    return $sHtml;
  }// end of getProductView() method
  
  
  /**
   * Metoda pobiera dane produktu o przekazanym ISBN
   * 
   * @param string $sISBN
   * @param int $iOILId
   * @param array $aCols
   * @return array
   */
  private function getProductListItems($iOILId, $aCols) {
    
    $sSql = 'SELECT '.implode(',', $aCols).'
             FROM orders_items_lists_items AS OILI
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id
             WHERE OILI.orders_items_lists_id = '.$iOILId.'
              AND confirmed <> "1" ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getProductDataByIsbn() method


    /**
     * Metoda pobiera dane produktu o przekazanym ISBN
     *
     * @param string $sISBN
     * @param int $iOILId
     * @param array $aCols
     * @return array
     */
    private function getSecondProductDataByOILIId($iOILIId, $iOILId, $iOrderId, $aCols) {

        $sSql = 'SELECT '.implode(',', $aCols).'
             FROM orders_items_lists_items AS OILI
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.order_id = '.$iOrderId.'
             WHERE OILI.orders_items_lists_id = '.$iOILId.' AND
              OILI.id <> '.$iOILIId.'
              AND confirmed <> "1" ';

        return $this->pDbMgr->GetRow('profit24', $sSql);
    }
  
  /**
   * Metoda pobiera dane produktu o przekazanym ISBN
   * 
   * @param string $sISBN
   * @param int $iOILId
   * @param array $aCols
   * @return array
   */
  private function getProductDataByOILIId($iOILIId, $iOILId, $aCols) {
    
    $sSql = 'SELECT '.implode(',', $aCols).'
             FROM orders_items_lists_items AS OILI
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id
             WHERE OILI.orders_items_lists_id = '.$iOILId.' AND
              OILI.id = '.$iOILIId.'
              AND confirmed <> "1" ';
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }


    /**
     * Metoda pobiera dane produktu o przekazanym ISBN
     *
     * @param string $sISBN
     * @param int $iOILId
     * @param array $aCols
     * @return array
     */
    private function getSecondProductDataByIsbn($sISBN, $iOrderId, $iOILId, $iOILIId, $aCols) {

        $sSql = 'SELECT '.implode(',', $aCols).'
             FROM orders_items_lists_items AS OILI
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id
             WHERE OILI.orders_items_lists_id = '.$iOILId.'
              AND OI.order_id = "'.$iOrderId.'"
              AND OILI.id <> '.$iOILIId.'
              AND confirmed <> "1" ';
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }// end of getProductDataByIsbn() method



    /**
   * Metoda pobiera dane produktu o przekazanym ISBN
   * 
   * @param string $sISBN
   * @param int $iOILId
   * @param array $aCols
   * @return array
   */
  private function getProductDataByIsbn($sISBN, $iOILId, $aCols) {
    
    $sSql = 'SELECT '.implode(',', $aCols).'
             FROM orders_items_lists_items AS OILI
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id
             WHERE OILI.orders_items_lists_id = '.$iOILId.'
              AND (
                OILI.isbn_plain = "'.$sISBN.'" OR
                OILI.isbn_10 = "'.$sISBN.'" OR
                OILI.isbn_13 = "'.$sISBN.'" OR
                OILI.ean_13 = "'.$sISBN.'"
              )
              AND confirmed <> "1" ';
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of getProductDataByIsbn() method
  
  
  /**
   * Metoda drukuje etykietę
   * 
   * @param \Module $oModuleZamowienia
   * @param \orders\getTransportList $oGetTransportList
   * @param int $iOrderId
   * @param array $aOrder
   * @param array $aLinkedOrders
   */
  protected function proceedConfirm($sTransportSymbol, \Module $oModuleZamowienia, \orders\getTransportList $oGetTransportList, $iOrderId, $aOrder, $aLinkedOrders) {
    
    try {
      $bPersonalReception = ($sTransportSymbol == 'odbior-osobisty') ? true: false;
      if ($aOrder['transport_number'] == '') {
        if ($oGetTransportList->proceedAddressLabel($aOrder['transport_id'], $iOrderId, $aOrder, $aLinkedOrders) === false) {
          throw new Exception("Wystąpił błąd podczas tworzenia listu przewozowego w FedEx ");
        }
      }
      
      if  ($this->remarksRead($iOrderId) === false) {
        throw new Exception(_('Wystąpił błąd podczas ustawiania uwag zamówienia jako przeczytane'), 1);
      }
      if ($oModuleZamowienia->setOrderConfirmed($aOrder, $iOrderId) === false) {
        throw new Exception(_('Wystąpił błąd podczas ustawiania zamówienia jako potwierdzone'), 2);
      }
      if ($oModuleZamowienia->proceedSetInvoice($iOrderId, $aOrder, $bPersonalReception) === false) {
        throw new Exception(_('Wystąpił błąd podczas ustawiania faktury jako gotowa do pobrania'), 3);
      }
      
      require_once('OrderRecount.class.php');
      $oOrderRecount = new OrderRecount();
      if ($oOrderRecount->recountOrder($iOrderId, false, true) === false) {
        throw new Exception(_('Wystąpił błąd podczas przeliczania zamówienia '.$iOrderId));
      }
    } 
    catch (\Exception $ex) {
      throw new Exception($ex->getMessage());
    }
  }// end of proceedRuchConfirm() method
  
  
  /**
   * Metoda pobiera pełną wagę zamówienia
   * 
   * @param int $iOrderId
   * @return float
   */
  protected function getOrderSelData($iOrderId) {
    
    $sSql = 'SELECT W.code as bookstore, O.transport_id, OTM.symbol AS transport_symbol, O.magazine_remarks, O.remarks, O.transport_remarks, O.order_number, O.transport_number,
                    O.website_id
             FROM orders AS O
             JOIN websites AS W
              ON O.website_id = W.id
             JOIN orders_transport_means AS OTM
              ON OTM.id = O.transport_id
             WHERE O.id = '.$iOrderId;
      
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of getOrderWeight() method



    private function checkGetHTMLDelicate($iOrderId) {
        if (true === $this->checkIsDelicate($iOrderId)) {
            return $this->getHTMLDelicate();
        }
        return '';
    }

    /**
     * @param $iOrderId
     * @return bool
     */
    private function checkIsDelicate($iOrderId) {
        $sSql = '
               SELECT id 
               FROM menus_items
               WHERE name LIKE "%komiks%"
                ';
        $categories = $this->pDbMgr->GetCol('profit24', $sSql);

        $sSql = 'SELECT id 
               FROM orders_items AS OI
               JOIN products_extra_categories AS PEC 
                ON PEC.product_id = OI.product_id 
               WHERE PEC.page_id IN ('.implode(',', $categories).')
               AND OI.order_id = '.$iOrderId;
        $orderId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($orderId > 0) {
            return true;
        }
        return false;
    }

    private function getHTMLDelicate() {
        global $aConfig;

        $sHTML = '
        <div style="position: absolute; top: 10%; right: 10px;">
            <img style="width: 280px; height: 350px;" src="'.$aConfig['common']['base_url_http'].'/gfx/fragile_comics_red.png" />
        </div> 
';
        return $sHTML;
    }

}