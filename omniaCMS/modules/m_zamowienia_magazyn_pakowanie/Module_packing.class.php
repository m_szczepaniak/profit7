<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace omniaCMS\modules\m_zamowienia_magazyn_pakowanie;

use Admin;
use Assets\MagazineRemarksBlockJavascript;
use DatabaseManager;
use Exception;
use FormTable;
use LIB\EntityManager\Entites\OrdersPacking;
use magazine\Containers;
use magazine\Gratis;
use Module;
use omniaCMS\lib\interfaces\ModuleEntity;
use OrderRecount;
use orders\getTransportList;
use orders\Shipment;
use Smarty;
use stdClass;


/**
 * Description of Module_packing
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_pakowanie__packing implements ModuleEntity {

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    private $sModule;

    /**
     *
     * @var Module
     */
    private $oModuleZamowienia;

    /**
     *
     * @var getTransportList
     */
    private $oGetTransport;

    private $orderPacking;

    /**
     *
     * @param Admin $oClassRepository
     */
    public function __construct(Admin $oClassRepository) {
        global $aConfig;

        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule.$this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;

        include_once('modules/m_zamowienia/Module.class.php');
        $pSmartyyyy = $oParent = new stdClass();
        $this->oModuleZamowienia = new Module($pSmartyyyy, $oParent, true);

        include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
        $this->oGetTransport = new getTransportList($this->pDbMgr);

        $this->orderPacking = new OrdersPacking($this->pDbMgr);
    }

    /**
     *
     * @return string
     */
    public function doDefault() {
        return $this->GetContainerView();
    }


    public function getMsg() {
        return $this->sMsg;
    }

    /**
     *
     */
    public function DoSetContainer() {
        $iOrderId = $this->GetOrderToSend($_POST['container_id']);
        if ($iOrderId > 0) {
            if (true === $this->orderPacking->exist($iOrderId)) {

                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas pakowania - list przewozowy został już wcześniej potwierdzony <br /> Zgłoś to pilnie przełożonemu.'));
                return $this->doDefault();
            }


            $mSeconds = $this->getOrderExhaustionTime($iOrderId);
            $mSeconds = true;
            if ($mSeconds !== true) {
                $sMsg .= sprintf(_('Zamówienie jest zablokowane do ponownego wskanowania, spróbuj ponownie za %s sekund'), $mSeconds);
                AddLog($sMsg);
                $this->sMsg .= GetMessage($sMsg);
                return $this->GetContainerView();
            }

            $this->addTimes($iOrderId, 0);
            return $this->DoGetOrderItemsQuantity($iOrderId);
        } else {
            $this->sMsg .= GetMessage(_('Zbyt wiele zamówień jest przypisanych do tej kuwety, lub brak zamówień w kuwecie'));
            return $this->GetContainerView();
        }
    }

    /**
     *
     */
    public function DoValidateQuantity() {
        $iOrderId = $_GET['id'];
        $iOrderItemsQuantity = $_POST['quantity'];

        if ($this->validateOrdersItemsQuantity($iOrderId, $iOrderItemsQuantity) === true) {
            // ok
            $aGratis = $this->getGratis($iOrderId);
            $aSecondGratis = $this->getGratis($iOrderId, true);
            if (!empty($aGratis)) {
                // gratis
                return $this->showGratis($iOrderId, $aGratis, null, false);
            } elseif (!empty($aSecondGratis)) {
                return $this->showGratis($iOrderId, $aSecondGratis, null, true);
            } else {
                return $this->getChoosePackage($iOrderId);
            }

        } else {
            $this->sMsg .= GetMessage(_('Podano nieprawidłową ilość egzemplarzy w zamówieniu /<br>КОЛИЧЕСТВО КНИГ В ЗАКАЗЕ УКАЗАНО НЕПРАВИЛЬНО'));
            return $this->doDefault();
        }
    }

    /**
     * @param $iOrderId
     * @return string
     */
    public function getChoosePackage($iOrderId)
    {
        return $this->tryGetQuantityForm($iOrderId);

        /*
      include_once('Form/FormTable.class.php');

      $pForm = new FormTable('choosePackage', _('Wybierz opakowanie'), array('action' => phpSelf(array('id' => $iOrderId))));
      $pForm->AddHidden('do', 'choosePackage');
      $pForm->AddText('package', _('Kod opakowania'), '', array('style' => 'width: 500px; font-size: 26px;', 'autofocus' => 'autofocus'), '', 'uint', true);
      $pForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
      return $pForm->ShowForm().$this->checkGetHTMLDelicate($iOrderId);
        */
    }

    private function checkGetHTMLDelicate($iOrderId) {
        if (true === $this->checkIsDelicate($iOrderId)) {
            return $this->getHTMLDelicate();
        }
        return '';
    }

    /**
     * @param $iOrderId
     * @return bool
     */
    private function checkIsDelicate($iOrderId) {
        $sSql = '
               SELECT id 
               FROM menus_items
               WHERE name LIKE "%komiks%"
                ';
        $categories = $this->pDbMgr->GetCol('profit24', $sSql);

        $sSql = 'SELECT id 
               FROM orders_items AS OI
               JOIN products_extra_categories AS PEC 
                ON PEC.product_id = OI.product_id 
               WHERE PEC.page_id IN ('.implode(',', $categories).')
               AND OI.order_id = '.$iOrderId;
        $orderId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($orderId > 0) {
            return true;
        }
        return false;
    }

    private function getHTMLDelicate() {
        global $aConfig;

        $sHTML = '
        <div style="position: absolute; top: 10%; right: 10px;">
            <img style="width: 280px; height: 350px;" src="'.$aConfig['common']['base_url_http'].'/gfx/fragile_comics_red.png" />
        </div> 
';
        return $sHTML;
    }

    /**
     * @return string
     */
    public function doChoosePackage() {

        $iOrderId = $_GET['id'];
        $iPackageCode = $_POST['package'];

        if ($this->checkPackage($iPackageCode) === false) {
            $this->sMsg .= GetMessage(_('Brak podanego opakowania o kodzie '.$iPackageCode));
            return $this->getChoosePackage($iOrderId);

        } else {
            return $this->tryGetQuantityForm($iOrderId);
        }
    }


    /**
     * @param $iOrderId
     * @param $aGratis
     * @return string
     */
    public function showGratis($iOrderId, $aGratis = [], $gratisId, $bSecondGratis = false) {


        if (empty($aGratis)) {
            if ($gratisId > 0) {
                $aGratis = $this->getGratisById($gratisId);
            }
        }

        include_once('Form/FormTable.class.php');
        $gratisMsg = _('Gratis');
        if ($bSecondGratis) {
            $gratisMsg = _('<strong>Drugi</strong> gratis');
        }
        $pForm = new FormTable('confirm_transport_address', $gratisMsg, array('action' => phpSelf(array('id' => $iOrderId))));
        if (false === $bSecondGratis) {
            $pForm->AddHidden('do', 'confirmGratis');
        } else {
            $pForm->AddHidden('do', 'confirmSecondGratis');
        }
        $pForm->AddMergedRow('<div style="padding: 2em; text-align: center; font-size: 2em; color: red; ">Dołącz '.mb_strtolower($gratisMsg, 'UTF-8').' "'.$aGratis['name'].'" o kodzie '.$aGratis['code'].' /<br> ДОБАВЬТЕ АКЦИОННЫЙ ПОДАРОК... "'.$aGratis['name'].'" КОДОМ '.$aGratis['code'].'</div>');
        $pForm->AddText('code', _('Kod gratisu'), '', array('style' => 'width: 300px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'uint', true);
        $pForm->AddHidden('gratis_validate_code', $aGratis['code']);
        $pForm->AddHidden('gratis_id', $aGratis['id']);

        $pForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
        return $pForm->ShowForm().$this->checkGetHTMLDelicate($iOrderId);
    }


    /**
     * @return string
     */
    public function doConfirmGratis($bIsSecond = false) {
        $iOrderId = $_GET['id'];
        $iGratisId = $_POST['gratis_id'];
        $iGratisCode = $_POST['gratis_validate_code'];
        $iPutCode = $_POST['code'];
        if ($iGratisCode !== $iPutCode || $iPutCode == '') {
            $this->sMsg .= GetMessage(_('Podany kod gratisu "'.$iPutCode.'" jest nieprawidłowoy, wpowadź kod ponownie'));
            unset($_POST);
            return $this->showGratis($iOrderId, [], $iGratisId, $bIsSecond);
        } else {
            // kod ok, dodajemy do logów i generujemy LP
            $gratis = new Gratis($this->pDbMgr);
            $gratis->addUseGratisOrder($iOrderId, $iGratisId);
            $secondMsg = '';
            if (true === $bIsSecond) {
                $secondMsg = 'drugi ';
            }
            $this->sMsg .= GetMessage(_('Dodano '.$secondMsg.'gratis "'.$iPutCode.'" '), false);

            if (false === $bIsSecond) {
                $aSecondGratis = $this->getGratis($iOrderId, true);
                if (!empty($aSecondGratis)) {
                    // pokaż form drugiego gratisu
                    return $this->showGratis($iOrderId, $aSecondGratis, null, true);
                } else {
                    // wybierz opakowanie
                    return $this->getChoosePackage($iOrderId);
                }
            } else {
                // wybierz opakowanie
                return $this->getChoosePackage($iOrderId);
            }
        }
    }


    /**
     * @return string
     */
    public function doConfirmSecondGratis() {
        $bIsSecond = true;
        return $this->doConfirmGratis($bIsSecond);
    }


    /**
     * @return bool|mixed
     */
    private function getGratis($iOrderId, $bSecondGratis = false) {

        $oGratis = new Gratis($this->pDbMgr, $bSecondGratis);
        return $oGratis->getGratis($iOrderId);
    }


    /**
     * @param $gratisId
     * @return array
     */
    private function getGratisById($gratisId) {

        $oGratis = new Gratis($this->pDbMgr);
        return $oGratis->getGratisById($gratisId);
    }


    /**
     * @param $iPackageCode
     * @return bool
     */
    private function checkPackage($iPackageCode) {

        $sSql = 'SELECT id FROM magazine_packages WHERE code = "'.$iPackageCode.'"';
        $iId = $this->pDbMgr->GetOne('profit24', $sSql);

        if ($iId > 0) {
            $sSql = 'UPDATE magazine_packages SET count = count - 1 WHERE id = '.$iId;
            $this->pDbMgr->Query('profit24', $sSql);
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param int $iOrderId
     */
    public function tryGetQuantityForm($iOrderId) {

        $aOrder = $this->getOrderSelData($iOrderId);
        $aLinkedOrders = $this->oModuleZamowienia->getOrdersLinked($iOrderId);
        // ta funkcja dobrze sie też zachowuje dla paczkomatów
        $iAmount = $this->oGetTransport->getSuggestedCountPackages($iOrderId, $aOrder['bookstore'], $aOrder['transport_id'], $aLinkedOrders);
        $sTransportSymbol = $aOrder['transport_symbol'];
        $bGetAmountPackages = $this->decidesInputAmountPackegesOrPrintLabel($iAmount, $sTransportSymbol);

        if ($bGetAmountPackages === TRUE) {
            return $this->getTransportPackages($iOrderId);
        } elseif ($bGetAmountPackages === FALSE) {
            if ($aOrder['transport_number'] == '') {
                $this->doSaveTransportPackages($aOrder, $iAmount, $aLinkedOrders);
                try {
                    $this->oGetTransport->proceedAddressLabel($aOrder['transport_id'], $iOrderId, $aOrder, $aLinkedOrders);
                } catch (Exception $ex) {
                    $this->sMsg .= GetMessage(_('Uwaga, wystąpił błąd pobierania listu przewozowego od kuriera/dostawcy: <br /> '.$ex->getMessage()));
                    return $this->doDefault();
                }
            } else {
                $this->addToPrintQueue($aOrder['transport_symbol'], $aOrder['transport_number']);
            }
            return $this->getConfirmTransportAddressLabel($iOrderId);
        } elseif ($bGetAmountPackages === -1) {

            $iContainerId = $this->getContainerByOrderId($iOrderId);
            $oContainers = new Containers($this->pDbMgr);
            $oContainers->unlockContainer($iContainerId);
            $oContainers->clearOrderContainer($iContainerId);

            $this->sMsg .= GetMessage(_('UWAGA zamówienie na odbiór osobisty !!!! /<br>ВНИМАНИЕ ПРИВАТНАЯ ПОСЫЛКА ОТЛОЖИТЬ НА ПОЛКУ!!!!'));
            return $this->doDefault();
        }
    }

    /**
     *
     * @global array $aConfig
     * @param string $sTransportSymbol
     * @param string $sTransportNumber
     * @throws Exception
     */
    private function addToPrintQueue($sTransportSymbol, $sTransportNumber) {
        global $aConfig;

        $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
        $oSpeditor = new Shipment($sTransportSymbol, $this->pDbMgr, $bTestMode);
        if ($this->oGetTransport->addToPrintQueue($oSpeditor->getTransportListFilename($aConfig, $sTransportNumber), $_COOKIE['printer_labels']) === false) {
            throw new Exception(_("Wystąpił błąd podczas dodawania pliku do kolejki drukowania"));
        }
    }


    /**
     *
     * @param int $iOrderId
     * @return string HTML
     */
    public function getConfirmTransportAddressLabel($iOrderId) {
        global $aConfig;

        $magazineRemarksBlockJs = new MagazineRemarksBlockJavascript();
        $sJS = $magazineRemarksBlockJs->getJavascript();

        $aOrder = $this->getOrderSelData($iOrderId);
        $remarksHiddenValue = false === empty($aOrder['magazine_remarks']) ? 0 : 1;
        $okButton = $magazineRemarksBlockJs->getHiddenInput();
        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('confirm_transport_address', _('Potwierdź numer listu przewozowego /<br>НОМЕР КУРЬЭРСКОГО БЛАНКА'), array('action' => phpSelf(array('id' => $iOrderId))));
        $pForm->AddHidden('do', 'ConfirmOrderTransportAddress');
        $pForm->AddHidden('remarks_has_been_read', $remarksHiddenValue);
        $pForm->AddMergedRow(
            '<div style="text-align: center">'.
            '<img src="'.$aConfig['common']['base_url_http'].'gfx/websites_logos/logo_'.$aOrder['website_id'].'.png" /><br />'.
            '<img src="'.$aConfig['common']['base_url_http'].'gfx/transport_logos/transport_'.$aOrder['transport_id'].'.jpg" style="width: 200px;" />'.
            $sJS.
            $okButton.
            '</div>'
        );
        if ($aOrder['magazine_remarks'] != '') {
            $pForm->AddRow(_('Uwagi:'), '<span style="font-size: 30px; font-weight: bold; color: #d14836;">'.$aOrder['magazine_remarks'].'</span>');
        }
        $pForm->AddText('transport_number', _('Numer listu przewozowego /<br>НОМЕР КУРЬЭРСКОГО БЛАНКА'), '', array('style' => 'width: 500px; font-size: 26px;', 'autofocus' => 'autofocus'), '', 'uint', true);
        $pForm->AddInputButton('save',_('Zatwierdź'), array('style' => 'font-size: 25'));
        return $pForm->ShowForm().$this->checkGetHTMLDelicate($iOrderId);
    }

    /**
     *
     * @return string
     */
    public function DoConfirmOrderTransportAddress() {
        $sTransportNumber = $_POST['transport_number'];
        $iOrderId = $_GET['id'];

        if ($iOrderId > 0) {
            if (true === $this->orderPacking->exist($iOrderId)) {

                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas pakowania - list przewozowy został już wcześniej potwierdzony <br /> Zgłoś to pilnie przełożonemu.'));
                return $this->doDefault();
            }
        }

        if ($this->validateTransportAddressLabel($iOrderId, $sTransportNumber)) {

            $iContainerId = $this->getContainerByOrderId($iOrderId);
            $oContainers = new Containers($this->pDbMgr);
            $oContainers->unlockContainer($iContainerId);
            $oContainers->clearOrderContainer($iContainerId);

            $this->orderPacking->add($iOrderId, $sTransportNumber);

            // Ok, ... zatwierdzamy, ale jest juz zatwierdzone, więc nie wiem co
            $this->addTimes($iOrderId, 9);
            $this->sMsg .= GetMessage(_('Zamówienie zostało zatwierdzone /<br>ЗАКАЗ БЫЛ УСПЕШНО ПОТДВЕРЖДЁН'), false);
            require_once('OrderRecount.class.php');
            $oOrderRecount = new OrderRecount();
            if ($oOrderRecount->recountOrder($iOrderId, true, true, true) === false) {
                throw new Exception(_('Wystąpił błąd podczas przeliczania zamówienia '.$iOrderId));
            }
            return $this->doDefault();
        } else {
            // nie OK, ponownie do pakowania
            $this->sMsg .= GetMessage(_('Podany list przewozowy nie pasuje do zamówienia ! /<br>ВВЕДЁННЫЙ НОМЕР БЛАНКА НЕ СООТВЕСТВУЕТ ЗАКАЗУ. ПОПРОБУЙТЕ ЕЩЁ РАЗ. '));
            return $this->getConfirmTransportAddressLabel($iOrderId);
        }
    }

    /**
     * @param $iOrderId
     * @return mixed
     */
    private function getContainerByOrderId($iOrderId) {

        $sSql = 'SELECT id FROM containers WHERE item_id = '.$iOrderId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }


    /**
     *
     * @param int $iOrderId
     * @param string $sTransportNumber
     * @return bool
     */
    private function validateTransportAddressLabel($iOrderId, $sTransportNumber) {

        $sSql = 'SELECT transport_number 
      FROM orders
      WHERE id = '.$iOrderId;
        $sDBTransportNumber = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($sDBTransportNumber == $sTransportNumber) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param int $iOrderId
     * @return HTML
     */
    private function getTransportPackages($iOrderId) {

        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('transport_packages', _('Podaj liczbę paczek / УКАЖИТЕ КОЛИЧЕСТВО НУЖНЫХ ПАЧЕК'), array('action' => phpSelf(array('id' => $iOrderId))));
        $pForm->AddHidden('do', 'SetTransportPackages');
        $pForm->AddText('transport_packages', _('Podaj liczbę paczek / УКАЖИТЕ КОЛИЧЕСТВО НУЖНЫХ ПАЧЕК'), '', array('style' => 'width: 300px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'uint', true);
        $pForm->AddInputButton('save',_('Dalej'), array('style' => 'font-size: 25'));
        return $pForm->ShowForm().$this->checkGetHTMLDelicate($iOrderId);
    }

    /**
     *
     */
    public function DoSetTransportPackages() {
        $iAmountOfPackages = $_POST['transport_packages'];
        $iOrderId = $_GET['id'];

        $aOrder = $this->getOrderSelData($iOrderId);
        $aLinkedOrders = $this->oModuleZamowienia->getOrdersLinked($iOrderId);

        if ($aOrder['transport_number'] == '') {
            $this->doSaveTransportPackages($aOrder, $iAmountOfPackages, $aLinkedOrders);
            try {
                $this->oGetTransport->proceedAddressLabel($aOrder['transport_id'], $iOrderId, $aOrder, $aLinkedOrders);
            } catch (Exception $ex) {
                $this->sMsg .= GetMessage(_('Uwaga, wystąpił błąd pobierania listu przewozowego od kuriera/dostawcy. Pokaż błąd przełożonemu: <br /> '.$ex->getMessage()));
                return $this->doDefault();
            }
        } else {
            $this->addToPrintQueue($aOrder['transport_symbol'], $aOrder['transport_number']);
        }
        return $this->getConfirmTransportAddressLabel($iOrderId);
    }

    /**
     *
     * @param array $aOrder
     * @param int $iAmountOfPackages
     * @param array $aLinkedOrders
     * @return boolean
     * @throws Exception
     */
    private function doSaveTransportPackages(&$aOrder, $iAmountOfPackages, $aLinkedOrders) {

        try {
            switch ($aOrder['transport_id']) {
                case '1':
                case '7':
                case '8':
                    $this->oGetTransport->setTransportPackages($aOrder['id'], $iAmountOfPackages, $aLinkedOrders);
                    $aOrder['transport_packages'] = $iAmountOfPackages;
                    break;
                case '3':
                    $iAmountOfPackages = $this->oGetTransport->getSuggestedCountPackages($aOrder['id'], 'profit24', $aOrder['transport_id'], $aLinkedOrders);
                    if ($iAmountOfPackages >= 2) {
                        $iAmountOfPackages = 2;
                        // TODO To z DB powinno być pobrane
                        $sCharAmount = 'B';
                    } else {
                        $iAmountOfPackages = 1;
                        $sCharAmount = 'A';
                    }
                    $this->oGetTransport->setPaczkomatyPackages($aOrder['id'], $iAmountOfPackages, $sCharAmount, $aOrder['point_of_receipt'], $aLinkedOrders);
                    $aOrder['transport_option_id'] = $iAmountOfPackages;
                    $aOrder['transport_option_symbol'] = $sCharAmount;
                    break;
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
        return true;
    }

    /**
     *
     * @param int $iAmount
     * @param string $sTransportSymbol
     * @return boolean
     */
    private function decidesInputAmountPackegesOrPrintLabel($iAmount, $sTransportSymbol) {

        switch ($sTransportSymbol) {

            case 'ruch':
            case 'orlen':
                return FALSE;
                break;

            case 'tba':
                // tba
                if ($iAmount > 1) {
                    // dać do wyboru
                    return TRUE;
                } else {
                    // drukuj
                    return FALSE;
                }
                break;

            case 'poczta-polska-doreczenie-pod-adres':
            case 'poczta_polska':
            case 'opek-przesylka-kurierska':
                // fedex
                if ($iAmount > 1) {
                    // dać do wyboru
                    return TRUE;
                } else {
                    // drukuj
                    return FALSE;
                }
                break;

            case 'paczkomaty_24_7':
                return FALSE;
                break;

            case 'odbior-osobisty':
                return -1;
                break;

            default:
                return -1;
                break;
        }
    }


    /**
     * Metoda pobiera pełną wagę zamówienia
     *
     * @param int $iOrderId
     * @return float
     */
    protected function getOrderSelData($iOrderId) {

        $sSql = 'SELECT W.code as bookstore, O.transport_id, OTM.symbol AS transport_symbol, O.magazine_remarks, O.remarks, O.transport_remarks, O.order_number, O.*
             FROM orders AS O
             JOIN websites AS W
              ON O.website_id = W.id
             JOIN orders_transport_means AS OTM
              ON OTM.id = O.transport_id
             WHERE O.id = '.$iOrderId;

        return $this->pDbMgr->GetRow('profit24', $sSql);
    }// end of getOrderWeight() method


    /**
     *
     * @param int $iOrderId
     * @param int $iOrderItemsQuantity
     * @return boolean
     */
    private function validateOrdersItemsQuantity($iOrderId, $iOrderItemsQuantity) {

        $aOrderData = $this->getOrderItemsWeightAndQuantity($iOrderId);
        $iQuantity = $aOrderData['quantity'];
        if ($iOrderItemsQuantity == $iQuantity) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param int $iOrderId
     * @return array
     */
    private function getOrderItemsWeightAndQuantity($iOrderId) {
        $aOrdersIds = array();

        if ($this->checkLinkedOrders($iOrderId) === true) {
            $aOrdersIds = $this->getOrdersLinked($iOrderId);
            $aOrdersIds[] = $iOrderId;
        } else {
            $aOrdersIds[] = $iOrderId;
        }
        return $this->getOrderWeightQuantityData($aOrdersIds);
    }

    /**
     *
     * @param int $iOrderId
     * @return array
     */
    private function getOrdersLinked($iOrderId) {

        $sSql = 'SELECT linked_order_id 
             FROM orders_linked_transport
             WHERE main_order_id = '.$iOrderId;
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

    /**
     *
     * @param array $aOrdersIds
     * @return array
     */
    private function getOrderWeightQuantityData($aOrdersIds) {

        $sSql = 'SELECT SUM(OI.quantity) as quantity, SUM(OI.quantity * OI.weight) as weight
             FROM orders_items AS OI
             JOIN orders AS O
              ON O.id = OI.order_id AND order_status <> "5"
             WHERE 
              OI.order_id IN ('.implode(', ', $aOrdersIds).')
              AND OI.deleted = "0" 
              AND (OI.item_type = "I" OR OI.item_type = "P")
              AND OI.packet = "0"
              ';
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }


    /**
     *
     * @param int $iOrderId
     * @return bool
     */
    private function checkLinkedOrders($iOrderId) {

        $sSql = 'SELECT linked_order FROM orders WHERE id = '.$iOrderId;
        return $this->pDbMgr->GetOne('profit24', $sSql) == '1' ? true : false;
    }

    /**
     *
     * @param int $iOrderId
     * @return int
     */
    public function DoGetOrderItemsQuantity($iOrderId) {
        global $aConfig;

        $bIsLinkedOrder = $this->checkIsLinkedOrder($iOrderId);
        if ($bIsLinkedOrder === true) {
            $this->sMsg .= GetMessage(_('UWAGA zamówienie łączone z innym, pamiętaj żeby wysłać zamówienia razem'));
        }

        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('get_order_items_quantity', _('Podaj produktów w zamówieniu'), array('action' => phpSelf(array('id' => $iOrderId))));
        $pForm->AddHidden('do', 'ValidateQuantity');
        $pForm->AddMergedRow(_('Podaj liczbę produktów w kuwecie, bez płyt CD - załączników'), array('class' => 'merged'));
        $pForm->AddText('quantity', _('Ilość produktów w kuwecie /<br>УКАЖИТЕ КОЛ-ВО КНИГ В ЗАКАЗЕ'), '', array('style' => 'width: 70px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'uint');
        $pForm->AddInputButton('save',_('Dalej'), array('style' => 'font-size: 25'));

        $aChars = $this->charsArrayLine();
        $this->pSmarty->assign_by_ref('aChars', $aChars);
        $sHtml = $this->pSmarty->fetch('touchKeyboard.tpl');
        $this->pSmarty->clear_assign('aChars', $aChars);
        $pForm->AddMergedRow($sHtml);
        return $pForm->ShowForm().$this->checkGetHTMLDelicate($iOrderId);
    }

    /**
     *
     * @param int $iOrderId
     * @return bool
     */
    private function checkIsLinkedOrder($iOrderId) {

        $sSql = 'SELECT linked_order FROM orders WHERE id = '.$iOrderId;
        return $this->pDbMgr->GetOne('profit24', $sSql) == '1' ? true : false;
    }

    /**
     * Metoda pobiera tablicę zanków
     *
     * @return array
     */
    private function charsArrayLine() {

        $aChars =
            array(
//          'chars' => array(
//          array('Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'),
//          array('A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'),
//          array('', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '&#8629;'),
//          ),
            'int' => array(
                array(7, 8, 9),
                array(4, 5, 6),
                array(1, 2, 3),
                array(0, 'X', '&#8629;')
            )
        );
        return $aChars;
    }// end of charsArrayLine() method

    /**
     *
     * @param string $sContainerId
     * @return boolean
     */
    private function GetOrderToSend($sContainerId) {

        $sSql = '
      SELECT item_id
      FROM containers
      WHERE id = "'.$sContainerId.'"
      AND locked = "1"
      ';
        $iOrderId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($iOrderId > 0) {
            return $iOrderId;
        } else {
            return false;
        }
    }

    /**
     *
     * @return array
     */
    private function GetContainerView() {

        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('get_container', _('Wczytaj kuwetę'));
        $pForm->AddHidden('do', 'SetContainer');
        $pForm->AddText('container_id', _('Numer kuwety /<br> НОМЕР ЯЩИКА:<br>'), '', array('style' => 'font-size: 26px; width: 300px;', 'autofocus' => 'autofocus'));
        $pForm->AddInputButton('save',_('Dalej'), array('style' => 'font-size: 25'));
        return $pForm->ShowForm();
    }

    /**
     *
     * @param int $iOrderId
     * @param int $iStatus
     */
    public function addTimes($iOrderId, $iStatus) {

        if ($iStatus == 0) {
            $this->setOrderTimePacking($iOrderId);
        } else {
            $iTime = $this->getOrderTimePacking($iOrderId, $iStatus);
        }
        $aOrderData = $this->getOrderItemsWeightAndQuantity($iOrderId);
        $aValues = array(
            'order_id' => $iOrderId,
            'user_id' => $_SESSION['user']['id'],
            'time' => $iTime,
            'quantity' => $aOrderData['quantity'],
            'weight' => $aOrderData['weight'],
            'status' => $iStatus,
            'created' => 'NOW()'
        );
        return $this->pDbMgr->Insert('profit24', 'orders_packing_times', $aValues);
    }

    /**
     *
     * @param int $iOrderId
     */
    private function setOrderTimePacking($iOrderId) {
        $_SESSION['packing'][$iOrderId]['start_time'] = microtime(true);
    }

    /**
     *
     * @param int $iOrderId
     * @return int
     */
    private function getOrderTimePacking($iOrderId, $iStatus) {

        $end = microtime(true);
        $start = $_SESSION['packing'][$iOrderId]['start_time'];
        $iTime = number_format(($end - $start), 2);
        if ($iStatus == 9) {
            unset($_SESSION['packing'][$iOrderId]);
        }
        return $iTime;
    }


    /**
     *
     * @return int
     */
    private function getOrderExhaustionTime($iOrderId) {
        $iSecondsExhaustion = 30;

        $sSql = 'SELECT TIME_TO_SEC(TIMEDIFF(NOW(), created))
             FROM orders_packing_times
             WHERE status = "0" AND order_id = ' . $iOrderId . '
             ORDER BY id DESC';
        $iSeconds = $this->pDbMgr->GetOne('profit24', $sSql);

        if ($iSeconds > 0) {
            if ($iSeconds < $iSecondsExhaustion) {
                return ($iSecondsExhaustion - $iSeconds);
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
