<?php

/*
 * @author Marcin Janowski <janowski@windowslive.com>
 * @created 2015-06-05 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_pakowanie;

use LIB\orders\counter\OrdersCounter;
use orders\getTransportList;
use Admin;
use DatabaseManager;
use FormTable;
use Module;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;
use stdClass;

class Module__zamowienia_magazyn_pakowanie__sorting implements ModuleEntity {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;

  /**
   *
   * @var string - komunikat
   */
  private $sMsg;

  /**
   * @var Admin
   */
  public $oClassRepository;
  private $sModule;

  /**
   *
   * @var Module
   */
  private $oModuleZamowienia;

  function __construct(Admin $oClassRepository) {
    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;

    include_once('modules/m_zamowienia/Module.class.php');
    $pSmartyyyy = $oParent = new stdClass();
    $this->oModuleZamowienia = new Module($pSmartyyyy, $oParent, true);
  }


  public function doDefault() {
    return $this->getCheckTransportNumberView();
  }

    /**
     * @return string
     */
    public function getMsg()
    {

        $sJS = '<script>
      $(function() {
        var komunikat = $(".message1").html();
        if (komunikat !== undefined && komunikat !== "") {
          new Messi(komunikat, {title: "Błąd", modal: true});
        }
      });
      </script>';
        return $this->sMsg . $sJS;
    }

  private function setFontWeight($sStr) {
      $sStr = '<span style="font-size: 40px!important;">'.$sStr.'</span>';
      return $sStr;
  }

  /**
   * Metoda dodaje nowy rekord do bufora
   * 
   * @param type $iOrderId
   * @param type $iTransportId
   * @param type $sCreatedBy
   */
  private function insertIntoBuffer($iOrderId, $iTransportId, $iWebsiteId) {

    $sSql = 'SELECT COUNT(id) 
            FROM orders_transport_buffer
            WHERE order_id = "' . $iOrderId . '"';
    $iExists = $this->pDbMgr->GetOne('profit24', $sSql);
    if ($iExists>0) {
      $sMsg = sprintf(_('Zamówienie o id: "%s" jest już w buforze! - Czyli prawdopodobnie produkt już został odsortowany !!!!'), $iOrderId);
      AddLog($sMsg);
      $this->sMsg .= GetMessage($this->setFontWeight($sMsg), TRUE);
      return false;
    }
    
    $aValues = array(
        'order_id' => $iOrderId,
        'transport_id' => $iTransportId,
        'website_id' => $iWebsiteId,
        'created' => 'NOW()',
        'created_by' => $_SESSION['user']['name'],
    );

    if ($this->pDbMgr->Insert('profit24', 'orders_transport_buffer', $aValues) === false) {
      // błąd
      $sMsg = sprintf(_('Wystąpił błąd podczas dodawania danych do bufura. Id zamówienia: "%s", id transportu: "%s"'), $iOrderId, $iTransportId);
      AddLog($sMsg);
      $this->sMsg .= GetMessage($this->setFontWeight($sMsg), TRUE);
      return false;
    } else {
      return true;
    }
  }

  /**
   * Metoda wyświetla formularz oraz loga kurierów
   * @return type
   */
  private function getCheckTransportNumberView() {
      $bIsErr = false;

    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('get_transport_number', _('Zeskanuj nr listu przewozowego'));
    $pForm->AddText('transport_number', _('Nr listu'), '', array('style' => 'font-size: 26px; width: 400px;', 'autofocus' => 'autofocus'));
    $pForm->AddHidden('do', 'verificationDestinyLocation');

    

    $sTransportNumber = $_POST['transport_number'];
    $sTransportLocation = $_POST['transport_location'];
    if ($sTransportNumber != '') {
      
      $this->pDbMgr->BeginTransaction('profit24');
      $aOrdersData = $this->getOrderData($sTransportNumber);
      if (empty($aOrdersData)) {
        $this->sMsg .= GetMessage($this->setFontWeight(_('Podany numer listu jest nieprawidłowy!')));
        $bIsErr = true;
      } else {
        foreach ($aOrdersData as $aSingleOrderData) {
            if (false === in_array($aSingleOrderData['order_status'], ['2', '3'])) {
                $sMsg = _('!!! Status zamówienia "'.$aSingleOrderData['order_number'].'" jest nieprawidłowy !!!');
                $this->sMsg .= GetMessage($this->setFontWeight($sMsg));
                AddLog($sMsg);
                return '';
            }

            $result = $this->validateContainerByTransportSymbol($aSingleOrderData['transport_symbol'], $sTransportLocation);
            if ($result !== true) {
                if ($result === -2) {
                    $sMsg = _('!!! Wybrany spedytor nie jest obsługiwany '. $sTransportNumber.' kontener - '. $sTransportLocation .' !!!');
                    $this->sMsg .= GetMessage($this->setFontWeight($sMsg));
                    AddLog($sMsg);
                    return '';
                } elseif ($result === -1) {
                    $sMsg = _('!!! NIEPRAWIDŁOWY KONTENER - Numer kontenera nie pasuje do listu przewozowego '. $sTransportNumber.' kontener - '. $sTransportLocation .' !!!');
                    $this->sMsg .= GetMessage($this->setFontWeight($sMsg));
                    AddLog($sMsg);
                    return '';
                }
            }

//          $mLogoHTML = $this->getTransportLogo($aSingleOrderData['transport_id']);
//          $mWebsiteLogoHTML = $this->getWebsiteLogo($aSingleOrderData['website_id']);
//          $mDelicateHTML = $this->checkGetHTMLDelicate($aSingleOrderData['id']);
//          $pForm->AddMergedRow($mLogoHTML.' &nbsp; '.$mWebsiteLogoHTML . ' &nbsp; ' . $mDelicateHTML);
          
          if ($this->changeOrderStatus($aSingleOrderData['id']) === FALSE) {
            $bIsErr = true;
              $sMsg = _('Wystąpił błąd podczas zmiany statusu zamówienia! - id zamówienia '.$aSingleOrderData['id']);
            $this->sMsg .= GetMessage($this->setFontWeight($sMsg));
              AddLog($sMsg);
               return '';
          } else {
            if ($this->insertIntoBuffer($aSingleOrderData['id'], $aSingleOrderData['transport_id'], $aSingleOrderData['website_id']) === FALSE) {
              $bIsErr = true;
                return '';
            } else {
                $ordersCounter = new OrdersCounter($this->pDbMgr);
                $ordersCounter->incCounter(OrdersCounter::TYPE_SEND_SORTOUT);
            }
          }
        }
      }

      if ($bIsErr === TRUE) {
        $this->pDbMgr->RollbackTransaction('profit24');
      } else {
        $this->pDbMgr->CommitTransaction('profit24');
        $this->sMsg .= GetMessage($this->setFontWeight(_('Zmieniono status zamówienia na: ZREALIZOWANE')), false);
      }
    }
    
    $pForm->AddInputButton('save', _('Dalej &raquo;'), array('style' => 'font-size: 25'));
    return $pForm->ShowForm();
  }//end of getCheckTransportNumberView()


    /**
     * @param $transportSymbol
     * @param $transportLocationId
     * @return bool|int
     */
    private function validateContainerByTransportSymbol($transportSymbol, $transportLocationId) {

        $mappingTransportContainer = [
            'opek-przesylka-kurierska' => '7790221232124',
            'poczta_polska' => '2132122333152',
            'poczta-polska-doreczenie-pod-adres' => '2132122333152',
            'ruch' => '9820547210323',
            'paczkomaty_24_7' => '1229926542122',
            'odbior-osobisty' => '2224123782232'
        ];

        if (!isset($mappingTransportContainer[$transportSymbol])) {
            return -2;
        }
        else {
            if ($mappingTransportContainer[$transportSymbol] == $transportLocationId) {
                return true;
            } else {
                return -1;
            }
        }
    }


    /**
     * @return string|void
     */
    public function doVerificationDestinyLocation() {

        $sTransportNumber = $_POST['transport_number'];
        if ($sTransportNumber == '') {
            $this->sMsg .= GetMessage($this->setFontWeight(_('NIE PODANO NUMERU LISTU PRZEWOZOWEGO !!!')));
            return ;
        }


        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('get_transport_location', _('Zeskanuj nr kontenera przewoźnika'));
        $pForm->AddText('transport_location', _('Nr kontenera przewoźnika'), '', array('style' => 'font-size: 26px; width: 400px;', 'autofocus' => 'autofocus'));
        $pForm->AddHidden('do', 'default');
        $pForm->AddHidden('transport_number', $sTransportNumber);

        $aOrdersData = $this->getOrderData($sTransportNumber);
        if (empty($aOrdersData)) {
            $this->sMsg .= GetMessage($this->setFontWeight(_('Podany numer listu jest nieprawidłowy!')));
            return ;
        } else {
            foreach ($aOrdersData as $aSingleOrderData) {
                if ($aSingleOrderData['tmp_transport_number'] === '1') {
                    $this->sMsg .= GetMessage($this->setFontWeight(_('Przekaż zamówienie do kierownika magazynu!')));
                    return ;
                }
                $mLogoHTML = $this->getTransportLogo($aSingleOrderData['transport_id']);
                $mWebsiteLogoHTML = $this->getWebsiteLogo($aSingleOrderData['website_id']);
                $mDelicateHTML = $this->checkGetHTMLDelicate($aSingleOrderData['id']);
                $pForm->AddMergedRow($mLogoHTML . ' &nbsp; ' . $mWebsiteLogoHTML . ' &nbsp; ' . $mDelicateHTML);
            }
        }
        $pForm->AddInputButton('save', _('Dalej &raquo;'), array('style' => 'font-size: 25'));
        return $pForm->ShowForm();
    }


    /**
     * @param $iOrderId
     * @return string
     */
    private function checkGetHTMLDelicate($iOrderId) {
        if (true === $this->checkIsDelicate($iOrderId)) {
            return $this->getHTMLDelicate();
        }
        return '';
    }

    /**
     * @param $iOrderId
     * @return bool
     */
    private function checkIsDelicate($iOrderId) {
        $sSql = '
               SELECT id 
               FROM menus_items
               WHERE name LIKE "%komiks%"
                ';
        $categories = $this->pDbMgr->GetCol('profit24', $sSql);

        $sSql = 'SELECT id 
               FROM orders_items AS OI
               JOIN products_extra_categories AS PEC 
                ON PEC.product_id = OI.product_id 
               WHERE PEC.page_id IN ('.implode(',', $categories).')
               AND OI.order_id = '.$iOrderId;
        $orderId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($orderId > 0) {
            return true;
        }
        return false;
    }

    private function getHTMLDelicate() {
        global $aConfig;

        $sHTML = '
        <div style="width: 100%; text-align: center;">
            <img style="width: 280px; height: 350px;" src="'.$aConfig['common']['base_url_http'].'/gfx/fragile_comics_red.png" />
        </div> 
';
        return $sHTML;
    }


  /**
   * Metoda zwraca id zamówienia oraz transport_id 
   * @param type $sTransportNumber
   * @return type
   */
  private function getOrderData($sTransportNumber) {
    $sSql = 'SELECT O.id, O.tmp_transport_number, O.transport_id, O.website_id, OTM.symbol as transport_symbol, O.order_status, O.order_number
            FROM orders AS O
            JOIN orders_transport_means AS OTM
              ON OTM.id = O.transport_id 
            WHERE O.transport_number = "' . $sTransportNumber . '"
            ORDER BY O.id DESC
            ';

    return $this->pDbMgr->GetAll('profit24', $sSql);
  }


  /**
   * Metoda wyświetla logo kuriera
   * 
   * @param int $iWebsiteId
   * @return string
   */
  private function getWebsiteLogo($iWebsiteId) {

    $sFilePath = '/omniaCMS/gfx/websites_logos/logo_' . $iWebsiteId . '.png';
    return '<div style="text-align: center; float: right;"><img style="float: right: 150px; min-height: 260px; min-width: 260px;" src="' . $sFilePath . '" alt="Website Logo" /></div>';
  }
  
  /**
   * Metoda wyświetla logo kuriera
   * 
   * @param int $iTransportId
   * @return string
   */
  private function getTransportLogo($iTransportId) {

    $sFilePath = '/omniaCMS/gfx/transport_logos/transport_' . $iTransportId . '.jpg';
    return '<div style="text-align: center; float: left;"><img src="' . $sFilePath . '" alt="Transport Logo" /></div>';
  }

  /**
   * Metoda zmienia status zamówienia na ZREALIZOWANE
   * @param type $iOId
   * @return boolean
   */
  private function changeOrderStatus($iOId) {

    $aOrderData = $this->getOrderByIdSelCols($iOId, array('order_status',  'internal_status', 'status_4_update', 'status_4_update_by'));
    
    if ($aOrderData['order_status'] == 3 || $aOrderData['order_status'] == 4) {
      $aValues = array(
          'order_status' => '4',
          'internal_status' => '9',
          'status_4_update' => 'NOW()',
          'status_4_update_by' => $_SESSION['user']['name']
      );

      $aValues = $this->pDbMgr->getChangedUpdateValues($aValues, $aOrderData);
      if (!empty($aValues)) {
        $this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOId.' LIMIT 1');
      }
    } else {
      $sMsg = sprintf(_("Zamówienie nie ma statusu ZATWIERDZONE. Status: %s"), $aOrderData['order_status']);
      $this->sMsg = GetMessage($this->setFontWeight($sMsg), TRUE);
      return false;
    }
  }//end of changeOrderStatus() function 
  
  /**
   * 
   * @param int $iOId
   * @param array $aCols
   * @return array
   */
  private function getOrderByIdSelCols($iOId, $aCols) {
    
    $sSql = 'SELECT '.implode(',', $aCols).'
              FROM orders 
              WHERE id = ' . $iOId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
}// end of Module_zamowienia_magazyn_pakowanie_sorting()
