<?php
/**
 * Plik jezykowy dla interfejsu modulu 'Wyszukiwarka' Panelu Administracyjnego
 * jezyk polski
 *
 * @author Marcin Korecki <m.korecki@omnia.pl>
 * @version 1.0
 *
 */

/*---------- szukane frazy */
$aConfig['lang']['m_szukaj']['list'] = "Lista wyszukiwanych fraz strony: \"%s\"";
$aConfig['lang']['m_szukaj']['f_date'] = "Data";
$aConfig['lang']['m_szukaj']['list_phrase'] = "Szukana fraza";
$aConfig['lang']['m_szukaj']['list_title'] = "Szukany tytuł";
$aConfig['lang']['m_szukaj']['list_author'] = "Szukany autor";
$aConfig['lang']['m_szukaj']['list_isbn'] = "Szukany ISBN";
$aConfig['lang']['m_szukaj']['list_searches'] = "Szukano";
$aConfig['lang']['m_szukaj']['go_back'] = "powrót do listy stron";
$aConfig['lang']['m_szukaj']['no_items'] = "Brak wyszukiwanych fraz";

/*---------- ustawienia boksu wyszukiwarki */
$aConfig['lang']['m_szukaj_settings']['menus'] = "Szukaj w menu";
$aConfig['lang']['m_szukaj_settings']['items_per_page'] = "Wyników na stronie (0 - brak stronicowania)";
$aConfig['lang']['m_szukaj_settings']['asterisk_suggestion'] = "Podpowiedź do stowowania gwiazdki";

/*---------- ustawienia boksu wyszukiwarki */
$aConfig['lang']['m_szukaj_box_settings']['page'] = "Strona wyszukiwarki";

?>