<?php
/**
* Plik jezykowy modulu 'WYSZUKIWARKA'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/

$aConfig['lang']['mod_m_szukaj']['phrase_err']	= 'Wyszukiwana fraza powinna składać się z conajmniej 3 znaków!';
$aConfig['lang']['mod_m_szukaj']['no_results']	= 'Brak pozycji spełniających Twoje kryteria wyszukiwania';
$aConfig['lang']['mod_m_szukaj']['no_results_author']	= 'Aktualnie nie posiadamy w ofercie książek tego autora';
$aConfig['lang']['mod_m_szukaj']['searched_for']	= 'Szukaj:';
$aConfig['lang']['mod_m_szukaj']['search_aval']	= 'szukaj również w niedostępnych';
$aConfig['lang']['mod_m_szukaj']['searched_phrase']	= 'Szukana fraza:&nbsp;<span>%s</span>&nbsp;|&nbsp;';
$aConfig['lang']['mod_m_szukaj']['results']	= 'Znalezionych pozycji:&nbsp;&nbsp;<span>%d</span>';
$aConfig['lang']['mod_m_szukaj']['phrase_length_err']	= 'Szukana fraza: <span>%s</span> jest zbyt krótka. Fraza powinna mieć co najmniej 2 znaki.';
$aConfig['lang']['mod_m_szukaj']['pager_total_items'] = 'wszystkich wyników';
$aConfig['lang']['mod_m_szukaj']['phrase'] = 'Szukaj: ';
$aConfig['lang']['mod_m_szukaj']['sphrase'] = 'Fraza: ';
$aConfig['lang']['mod_m_szukaj']['title'] = 'Tytuł książki: ';
$aConfig['lang']['mod_m_szukaj']['author'] = 'Autor książki: ';
$aConfig['lang']['mod_m_szukaj']['publisher'] = 'Wydawnictwo / producent: ';
$aConfig['lang']['mod_m_szukaj']['year'] = 'Rok wydania: ';
$aConfig['lang']['mod_m_szukaj']['isbn'] = 'ISBN: ';
$aConfig['lang']['mod_m_szukaj']['series'] = 'Seria wydawnicza: ';
$aConfig['lang']['mod_m_szukaj']['tag'] = 'Tag: ';
$aConfig['lang']['mod_m_szukaj']['edition'] = 'Wydanie';
$aConfig['lang']['mod_m_szukaj']['wyczerpany'] = 'Nakład wyczerpany';
$aConfig['lang']['mod_m_szukaj']['price_from'] = 'Zakres cen od: ';
$aConfig['lang']['mod_m_szukaj']['price_zak'] = 'Zakres cen ';
$aConfig['lang']['mod_m_szukaj']['price_from2'] = 'od: ';
$aConfig['lang']['mod_m_szukaj']['price_to'] = ' do: ';
$aConfig['lang']['mod_m_szukaj']['index'] = 'Indeks rzeczowy: ';
$aConfig['lang']['mod_m_szukaj']['seria'] = 'Seria wydawnicza: ';
$aConfig['lang']['mod_m_szukaj']['back_to'] = 'wróć do wyszukiwarki';
$aConfig['lang']['mod_m_szukaj']['criteria_search'] = 'Kryteria wyszukiwania:';
$aConfig['lang']['mod_m_szukaj']['found_items'] = 'Znalezionych pozycji:';
$aConfig['lang']['mod_m_szukaj']['about_author'] = 'Informacje o autorze: ';
$aConfig['lang']['mod_m_szukaj']['hide_adv'] = 'ukryj zaawansowane opcje wyszukiwania';
$aConfig['lang']['mod_m_szukaj']['show_adv'] = 'pokaż zaawansowane opcje wyszukiwania';
$aConfig['lang']['mod_m_szukaj']['only_pl'] = 'wśród książek w języku polskim';
$aConfig['lang']['mod_m_szukaj']['only_en'] = 'wśród książek w języku angielskim';
$aConfig['lang']['mod_m_szukaj']['searched_el'] = '%s <span>%s</span>';
$aConfig['lang']['mod_m_szukaj']['searched_el2'] = 'Szukaj: <span>%s</span>';
$aConfig['lang']['mod_m_szukaj']['searched_sep'] = '<br />';

$aConfig['lang']['mod_m_szukaj']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['mod_m_szukaj']['language'] = "Język";

$aConfig['lang']['mod_m_szukaj']['shipment'] = "Dostępność";

$aConfig['lang']['mod_m_szukaj']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_szukaj']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_szukaj']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_szukaj']['shipment_3'] = "na zamówienie";
$aConfig['lang']['mod_m_szukaj']['publisher']	= 'Wydawnictwo: ';
$aConfig['lang']['mod_m_szukaj']['series_name']	= 'Seria: ';
$aConfig['lang']['mod_m_szukaj']['avalaibility']	= 'Dostępny: ';
$aConfig['lang']['mod_m_szukaj']['binding'] = "Oprawa";
$aConfig['lang']['mod_m_szukaj']['shipment_preview'] = "Dostępność od";

$aConfig['lang']['mod_m_szukaj']['top_cats'] = 'W kategorii';
$aConfig['lang']['mod_m_szukaj']['sub_cats'] = 'W podkategorii';
$aConfig['lang']['mod_m_szukaj']['all_categories'] = 'Wszystkie działy';

$aConfig['lang']['mod_m_szukaj']['author_books'] = 'książki autora';
?>