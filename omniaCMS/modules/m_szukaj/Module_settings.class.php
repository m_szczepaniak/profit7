<?php
/**
 * Klasa Module do obslugi konfiguracji stron modulu 'Wyszukiwarka'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2007
 * @version   1.0
 */
 
class Settings {
  
  // nazwa modulu - do langow
	var $sModule;
	
	// Id strony dla ktorej edytowane sa ustawienia
	var $iId;
	
	// ID wersji jez.
	var $iLangId;
	
	// ustawienia strony modulu
	var $aSettings;
	
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$sModule, &$iId) {
		global $aConfig;
		$this->sModule = $sModule;
		$this->iId = $iId;
		$this->iLangId = intval($_SESSION['lang']['id']);
		$this->aSettings =& $this->getSettings();
	} // end Settings() function
	
	
	/**
	 * Metoda pobiera konfiguracje strony modulu
	 * @return	array ref	- konfiguracja
	 */
	function &getSettings() {
		global $aConfig;
		
		// pobranie konfiguracji dla modulu
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."search_settings 
						 WHERE page_id = ".$this->iId;
						 
		$aCfg =& Common::GetRow($sSql);
		
		$aCfg['menu_ids'] = explode(',', $aCfg['menu_ids']);
		return $aCfg;
	} // end of getSettings() method
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach modulu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;
		
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."search_settings
						 WHERE page_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);
		
		// dodanie ustawien
		$aValues = array(
			'items_per_page' => intval($_POST['items_per_page']),
			'asterisk_suggestion' => $_POST['asterisk_suggestion']
		);
		if (!empty($aSettings)) {
			// Update
			return Common::Update($aConfig['tabls']['prefix']."search_settings",
														$aValues,
														"page_id = ".$this->iId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('page_id' => $this->iId), $aValues);
			
			return Common::Insert($aConfig['tabls']['prefix']."search_settings",
														$aValues,
														'',
														false) !== false;
		}
	} // end of UpdateSettings() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji konfiguracji modulu wyszukiwarki
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;
		$aData = array();
				
		$aData =& $this->aSettings;
		if (!empty($_POST) && $_POST['do'] != 'change_mtype') {
			$aData =& $_POST;
		}
				
		// liczba wynikow na stronie
		$pForm->AddText('items_per_page', $aConfig['lang'][$this->sModule.'_settings']['items_per_page'], empty($aData['items_per_page']) ? 0 : $aData['items_per_page'], array('maxlength'=>3, 'style'=>'width: 30px;'), '', 'uinteger');
		// podpowiedz do uzywania gwiazdki
		$pForm->AddText('asterisk_suggestion', $aConfig['lang'][$this->sModule.'_settings']['asterisk_suggestion'],$aData['asterisk_suggestion'],array('maxlength'=>1024,'style'=>'width:300px;'));
		
	} // end of SettingsForm() function
} // end of Settings Class
?>