<?php
/**
 * Klasa Module do obslugi modulu 'Wyszukiwarka'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */
  
class Module {

  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig, $oCache;
		
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$iPId = getModulePageId('m_szukaj');
		
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
		if ($_SESSION['user']['type'] === 0) {
			if (!hasModulePrivileges($_GET['module_id'])){
				showPrivsAlert($pSmarty);
				return;
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end Module() function


	/**
	 * Metoda wyswietla liste zdefiniowanych aktualnosci
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId) {
		global $aConfig;
		$aIDs = array();
		$sDateSql = '';
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		
		// zapamietanie opcji
		rememberViewState($this->sModule);
		
		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iPId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> false,
			'checkboxes'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '20'
			),
			array(
				'db_field'	=> 'phrase',
				'content'	=> $aConfig['lang'][$this->sModule]['list_phrase'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'counter',
				'content'	=> $aConfig['lang'][$this->sModule]['list_searches'],
				'sortable'	=> true,
				'width'	=> '85'
			)
		);
		
			
		// pobranie liczby wszystkich aktualnosci strony
		$sSql = "SELECT COUNT(*) 
						 FROM ".$aConfig['tabls']['prefix']."search_phrases
						 WHERE 1 = 1".
						 			 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND phrase LIKE \'%'.$_POST['search'].'%\'' : '');

		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(*) 
						 	 FROM ".$aConfig['tabls']['prefix']."search_phrases";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		
		$pView = new View('phrases', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
				
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
			// pobranie wszystkich wyszukiwanych fraz dla danego serwisu
			$sSql = "SELECT '&nbsp;', phrase,counter
							 FROM ".$aConfig['tabls']['prefix']."search_phrases
							 WHERE 1 = 1
							 GROUP BY phrase
							 ORDER BY ".(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'counter DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array();
			
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show());
	} // end of Show() function


	
	
} // end of Module Class
?>