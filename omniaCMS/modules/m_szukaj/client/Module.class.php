<?php
/**
 * Klasa odule do obslugi wyszukiwarki
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

use Elasticsearch\Profit\ElasticSearchBuilder;
use Elasticsearch\Profit\ElasticSearchStrategy;
use LIB\Assets\additionalHead;

include_once('modules/m_oferta_produktowa/client/Common.class.php');

class Module extends Common_m_oferta_produktowa {

  // Id strony
  var $iPageId;

  // katalog szablonow
  var $sTemplatesPath;

  // symbol modulu
  var $sModule;

  // ustawienia strony aktualnosci
  var $aSettings;

  // link do strony galerii
  var $sPageLink;

  // wyszukiwanie zaawansowane
  var $bAdvanced;

  // wpisao fraze
  var $bPhrase;

  // wyszukiwanie po autorze
  var $bAuthor;

  // wyszukiwanie po wydawnictwie
  var $bPublisher;

  // wyszukiwanie po serii
  var $bSeries;

  // wyszukiwanie po indeksie
  var $bIndex;

  // wyszukiwanie po roku wydania
  var $bYear;

  // wyszukiwanie w przedziale cenowym
  var $bPriceFrom;
  var $bPriceTo;

  // wyszukiwanie po ISBN
  var $bISBN;

  // wyszukiwanie po tagu
  var $bTag;

   var $bTopCat;
   var $sTopCatName;
   var $bSubCat;
   var $sSubCatName;
	 var $bShowMessage = true;

  // wyszukiwanie tylko wsrod dostepnych
  var $bAval;

  // wyszukiwanie tylko wsrod polskich
  var $bOnlyPl;

  // wyszukiwanie tylko wsrod angielskich
  var $bOnlyEn;

  // blad - zbyt krotka fraza
  var $bPhraseLengthErr;

  // czy wprowadzono lub wybrano jakas wartosc do wyszukiwania - wyszukiwarka
  // zaawansowana
  var $bPerformSearch;

  // czesc nazwy pliku cache
  var $sCacheName;

  // czy cache'owac czy tez nie
  var $bDoCache;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;

		// XXX  - NIE RUSZAĆ jeśli ktoś poda w zaawansowanej wyszukiwarce
		//	fraze z ' dodaje się \' i później \\\' wychodzi
		$_GET['series'] = stripslashes(stripslashes(($_GET['series'])));
		$_GET['tag'] = stripslashes(stripslashes($_GET['tag']));
		$_GET['author'] = stripslashes(stripslashes($_GET['author']));
		$_GET['publisher'] = stripslashes(stripslashes($_GET['publisher']));

  	$this->sCacheName = md5($_SERVER['REQUEST_URI']);
		$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->sTemplatesPath = 'modules/'.$aConfig['_tmp']['page']['module'].'/';
		$this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
		$this->aSettings =& $this->getSettings();
		$this->bAdvanced = false;
		$this->bAuthor = false;
		$this->bPublisher = false;
		$this->bSeries = false;
		$this->bIndex = false;
		$this->bPhrase = false;
		$this->bYear = false;
		$this->bPriceFrom = false;
		$this->bPriceTo = false;
		$this->bISBN = false;
		$this->bTag = false;
		$this->bAval = false;
		$this->bTopCat = false;
		$this->bSubCat = false;
		$this->bPhraseLengthErr = false;
		$this->bPerformSearch = false;
		$this->bDoCache = false;//$aConfig['common']['caching'];
		$this->allGeturldecode();
	} // end Module() function

	private function allGeturldecode() {
		foreach ($_GET AS $key => $val) {
			if (is_string($val) && $val != '') {
				$_GET[$key] = urldecode($val);
			}
		}
	}


	/**
	 * Metoda pobiera konfiguracje strony modulu
	 * @return	array ref	- konfiguracja
	 */
	function &getSettings() {
		global $aConfig, $oCache;

		if (!$aConfig['common']['caching'] || !is_array($aCfg = unserialize($oCache->get(md5($aConfig['_tmp']['page']['symbol']), 'modules_settings')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT *
						 	 FROM ".$aConfig['tabls']['prefix']."search_settings
						 	 WHERE page_id = ".$this->iPageId;

			$aCfg =& Common::GetRow($sSql);
			$aCfg['items_on_list'] = (int) $aCfg['items_on_list'];
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aCfg), md5($aConfig['_tmp']['page']['symbol']), 'modules_settings');
		}
		return $aCfg;
	} // end of getSettings() method


	/**
	 * Metoda pobiera tresc strony o id $this->iPageId
	 * dowiazuje ja do Smartow oraz zwraca xHTML z trescia strony po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty, $oTimer;
		//$oTimer->setMarker($this->sModule.'_START');

    if (isset($_GET['q'])) {
      $_GET['q'] = strip_tags($_GET['q']);
    }
    if (isset($_GET['publisher'])) {
      $_GET['publisher'] = strip_tags($_GET['publisher']);
    }
    if (isset($_GET['series'])) {
      $_GET['series'] = strip_tags($_GET['series']);
    }
    if (isset($_GET['autor'])) {
      $_GET['autor'] = strip_tags($_GET['autor']);
    }
    if (isset($_GET['tag'])) {
      $_GET['tag'] = strip_tags($_GET['tag']);
    }

		if ($_GET['q'] == '' && isset($_GET['path']) && $_GET['path'] != 'szukaj' && $_GET['path'] != 'szukaj/' ) {
			if (isset($_GET['publisher']) && $_GET['publisher'] != '' && isset($_GET['series']) && $_GET['series'] != '') {
				$aConfig['_tmp']['path'][0]['link'] = getModulePageLink('m_powiazane_oferta_produktowa', 'wydawcy');
				$aItem = array(
						'0' => getPublisherLink($_GET['publisher']),
						'1' => $_GET['publisher']
				);
				addToPath($aItem);
				/*
				$aItem = array(
						'0' => getModulePageLink('m_powiazane_oferta_produktowa', 'serie'),
						'1' => 'seria'
				);
				addToPath($aItem);
				*/
				$aItem = array(
						'0' => getSeriesLink($_GET['publisher'], $_GET['series']),
						'1' => $_GET['series']
				);
				addToPath($aItem);
				setPageHeader($_GET['series']);
				$this->bShowMessage = false;
			}
			elseif (isset($_GET['publisher']) && $_GET['publisher'] != '') {
				$aConfig['_tmp']['path'][0]['link'] = getModulePageLink('m_powiazane_oferta_produktowa', 'wydawcy');
				$aItem = array(
						'0' => getPublisherLink($_GET['publisher']),
						'1' => $_GET['publisher']
				);
				addToPath($aItem);
				setPageHeader($_GET['publisher']);
				$this->bShowMessage = false;
			}
			elseif (isset($_GET['tag']) && $_GET['tag'] != '') {
				$aConfig['_tmp']['path'][0]['link'] = getModulePageLink('m_tagi');
				$aItem = array(
						'0' => getTagLink($_GET['tag']),
						'1' => $_GET['tag']
				);
				addToPath($aItem);
				setPageHeader($_GET['tag']);
				$this->bShowMessage = false;
			}
			elseif (isset($_GET['autor']) && $_GET['autor'] != '') {
				$aConfig['_tmp']['path'][0]['link'] = getModulePageLink('m_powiazane_oferta_produktowa', 'autorzy');
				$aItem = array(
						'0' => getAuthorLink($_GET['autor']),
						'1' => $_GET['autor']
				);
				addToPath($aItem);
				setPageHeader($_GET['autor']);
				$this->bShowMessage = false;
			}
		}

		$sHtml = '';
		$sTemplate = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['page_link'] =& $this->sPageLink;
		$aModule['adv'] = '0';

    if (!isset($_GET['result']) && isset($_GET['path']) && ($_GET['path'] == 'szukaj' || $_GET['path'] == 'szukaj/')) {
      $aConfig['header_js'][]="$(document).ready(function(){";
      $aConfig['header_js'][]='
        $("#publisher_aci").autocomplete({
          source: "/internals/GetPublishers.php",
          minLength: 3
        })
        .data( "ui-autocomplete" )._renderItem = function( ul, item ) {

          var termsString = $.trim(this.term);
          var searchMask = termsString;
          var regEx = new RegExp("("+searchMask+")", "ig");
          var replaceMask = "<span style=\'font-weight:bold;\'>$1</span>";

          var t = item.label.replace(regEx, replaceMask);

          return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + t + "</a>" )
                .appendTo( ul );
        };
  ';
      $aConfig['header_js'][]= '
        $("#tag_aci").autocomplete({
          source: "/internals/GetTags.php",
          minLength: 3
        })
        .data( "ui-autocomplete" )._renderItem = function( ul, item ) {

          var termsString = $.trim(this.term);
          var searchMask = termsString;
          var regEx = new RegExp("("+searchMask+")", "ig");
          var replaceMask = "<span style=\'font-weight:bold;\'>$1</span>";

          var t = item.label.replace(regEx, replaceMask);

          return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( "<a>" + t + "</a>" )
                .appendTo( ul );
        };
  ';

      $aConfig['header_js'][]="	$(\"#publisher_aci\").bind(\"blur\", function(e){";
      $aConfig['header_js'][]="	 var elSelSeries = document.getElementById('series_aci');";
      $aConfig['header_js'][]="	 if(e.target.value != '') {  ";
      $aConfig['header_js'][]="  $.get(\"/internals/GetSeries.php\", { publisher: e.target.value },";
      $aConfig['header_js'][]="	  function(data){";
      $aConfig['header_js'][]="	    var brokenstring=data.split(';');";
      $aConfig['header_js'][]="	    elSelSeries.length = 0;";
      $aConfig['header_js'][]="	    elSelSeries.options[elSelSeries.length] =  new Option(\"".$aConfig['lang']['common']['choose']."\",\"\");";
      $aConfig['header_js'][]="	    for (x in brokenstring){";
      $aConfig['header_js'][]="				if(brokenstring[x]){";
      $aConfig['header_js'][]="					var item=brokenstring[x].split('|');";
      $aConfig['header_js'][]="					elSelSeries.options[elSelSeries.length] = new Option(item[0],item[0]);";
      $aConfig['header_js'][]="				}";
      $aConfig['header_js'][]="			}";
      $aConfig['header_js'][]="	 });";
      $aConfig['header_js'][]="	 } else { elSelSeries.length = 0; elSelSeries.options[elSelSeries.length] =  new Option(\"".$aConfig['lang']['common']['choose']."\",\"\"); }";
      $aConfig['header_js'][]="});";
      $aConfig['header_js'][]="	$(\"#top_cat_ajx\").bind(\"change\", function(e){";
      $aConfig['header_js'][]="	 var elSelCats = document.getElementById('cats_ajx');";
      $aConfig['header_js'][]="	 if(e.target.value != '') { elSelCats.disabled = false; ";
      $aConfig['header_js'][]="  $.get(\"/internals/GetCats.php\", { top_cat: e.target.value },";
      $aConfig['header_js'][]="	  function(data){";
      $aConfig['header_js'][]="	    var brokenstring=data.split(';');";
      $aConfig['header_js'][]="	    elSelCats.length = 0;";
      $aConfig['header_js'][]="	    elSelCats.options[elSelCats.length] =  new Option(\"".$aConfig['lang']['common']['choose']."\",\"\");";
      $aConfig['header_js'][]="	    for (x in brokenstring){";
      $aConfig['header_js'][]="				if(brokenstring[x]){";
      $aConfig['header_js'][]="					var item=brokenstring[x].split('|');";
      $aConfig['header_js'][]="					elSelCats.options[elSelCats.length] = new Option(item[0],item[1]);";
      $aConfig['header_js'][]="				}";
      $aConfig['header_js'][]="			}";
      $aConfig['header_js'][]="	 });";
      $aConfig['header_js'][]="	 } else { elSelCats.length = 0; elSelCats.disabled = true; }";
      $aConfig['header_js'][]="});";
      //$aConfig['header_js'][]="	$(\"#series_aci\").autocomplete(\"/internals/GetSeries.php\",{ minChars: 3, max: 50, matchContains: true });";
  //		$aConfig['header_js'][]="	$(\"#qi\").autocomplete(\"/internals/GetPhrases.php\",{ minChars: 2, max: 50, matchContains: true, matchSubset: false, selectFirst: false });";


      $aConfig['header_js'][]='
                                $("#qi").autocomplete({
                                  source: "/internals/GetPhrases.php",
                                  minLength: 2
                                 })
                                 .data( "ui-autocomplete" )._renderItem = function( ul, item ) {

                                  var termsString = $.trim(this.term);
                                  var searchMask = termsString;
                                  var regEx = new RegExp("("+searchMask+")", "ig");
                                  var replaceMask = "<span style=\'font-weight:bold;\'>$1</span>";

                                  var t = item.label.replace(regEx, replaceMask);

                                  return $( "<li></li>" )
                                        .data( "item.autocomplete", item )
                                        .append( "<a>" + t + "</a>" )
                                        .appendTo( ul );
                                  };


								 $("#qi").autocomplete({
								  select: function( event, ui ) {
								  mytext = $("<testest>").html(ui.item.value).text();
								  	ui.item.value = mytext;
								  }
								});

								$("#qi").autocomplete({
								  focus: function( event, ui ) {
							  		mytext = $("<testest>").html(ui.item.value).text();
								  	ui.item.value = mytext;
								  }
								});
                                   ';



      $aConfig['header_js'][]="});";
    }

		$aModule['top_cats'] = $this->getTopPages();
		$aModule['years'] = $this->getYears();
		if(isset($_GET['filter'])){
					if($_GET['filter']=="all") {
						$_GET['aval']=1;
					} else {
						unset($_GET['aval']);
					}
			}elseif(isset($_GET['aval'])) {
					$_GET['filter'] = "all";
				}
		$aCurrSeries = array();
		if(isset($_GET['publisher'])) {
			$sSql = "SELECT A.name AS value, A.name As label
			 FROM ".$aConfig['tabls']['prefix']."products_series A
			 JOIN ".$aConfig['tabls']['prefix']."products_publishers B
			 ON A.publisher_id=B.id
			 WHERE B.name LIKE ".Common::Quote($_GET['publisher'])."
			  ORDER BY A.name";
			$aCurrSeries =& Common::GetAll($sSql);
//			if(!empty($aCurrSeries)) {
//				$aModule['curr_series'] = array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),$aCurrSeries);
//			}
		}
		$aModule['curr_series'] = array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),$aCurrSeries);
	if(!empty($_GET['top_cat'])) {
		$sSql = "SELECT A.id AS value, A.name AS label 
					 FROM ".$aConfig['tabls']['prefix']."menus_items A
					 WHERE A.parent_id = ".intval($_GET['top_cat'])."
					 AND A.module_id = ".getModuleId('m_oferta_produktowa')." 
					  ORDER BY A.name";
			$aSubCats =& Common::GetAll($sSql);
			if(!empty($aSubCats)) {
				$aModule['sub_cats'] = $aSubCats;
			}
		}

		/**
		 *
		 * POCZATEK WYSZUKIWANIA
		 */

		$aModule['results_template'] = $this->sTemplatesPath.'/wyszukiwarka_wyniki.tpl';
		if(isset($_GET['adv'])  || isset($_GET['q']) || (isset($_GET['action']) && $_GET['action'] == 'zaawansowane')) {
			// WYSZUKIWARKA
			if ((isset($_GET['action']) && $_GET['action'] == 'zaawansowane') || isset($_GET['adv'])) {
				// wyszukiwarka zaawansowana
				$this->bAdvanced = true;
				$aModule['adv'] = '1';
				$aModule['message'] = '';
				//$aModule['publishers_list']=$this->getPublishers();
				//$aModule['indexes'] = $this->getIndexes();
				//$aModule['series'] = $this->getSeries();
				$sTemplate = 'wyszukiwarka_zaawansowana.tpl';

				if(isset($_GET['result'])){
					$sWhereSql = $this->prepareAdvancedWhereSql();
//					if ($this->bPhrase) {
//						setTitle(stripslashes($_GET['q']).' - '.$aConfig['_tmp']['page']['name']);
//					}
					setPageHeader($aConfig['lang']['common']['wyniki_wyszukiwania']);
					$aModule['mod_link'] = '';
//					if ($sWhereSql != '') {
						// zostaly wybrane jakies warunki wyszukiwania - wyszukiwanie
						$this->performAdvancedSearch($aModule, $sWhereSql, $fUserDiscount);
						$this->writePhrase();
						if(strpos($_GET['q'],'*')===false){
							$aModule['asterisk_suggestion'] = $this->aSettings['asterisk_suggestion'];
//						}
					}
				}
			}
			else {

				// wyszukiwarka podstawowa - po tytule, opisie i autorach
				$sTemplate = 'wyszukiwarka_podstawowa.tpl';
				$sWhereSql = $this->prepareBasicWhereSql();
				//setTitle(stripslashes($_GET['q']).' - '.$aConfig['_tmp']['page']['name']);
				$aModule['mod_link'] = 'zaawansowane.html';
				if(isset($_GET['result'])){
					setPageHeader($aConfig['lang']['common']['wyniki_wyszukiwania']);
//					if ($sWhereSql != '') {
						// zostaly wybrane jakies warunki wyszukiwania - wyszukiwanie
						$this->performBasicSearch($aModule, $sWhereSql, $fUserDiscount);
						$this->writePhrase();
						if(strpos($_GET['q'],'*')===false){
							$aModule['asterisk_suggestion'] = $this->aSettings['asterisk_suggestion'];
						}
//					}
				}
			}

				if ($this->bPhraseLengthErr) {
					// blad - zbyt krotka fraza
					$aModule['message'] = sprintf($aModule['lang']['phrase_length_err'], stripslashes($_GET['q']));
				}

			if($this->bAdvanced) {
				$aModule['return_link']='/szukaj?'.str_replace('&','&amp;',$this->formatPagerLink(false));
			}
		}
		elseif ($this->isItemSearch()) {
			// ELEMENT - autor, seria, indeks, tag
			$sTemplate = 'wyszukiwarka_element.tpl';
			$this->performItemSearch($aModule, $fUserDiscount);
			///$aModule['message'] .= sprintf($aModule['lang']['results'], $aModule['results_no']);
		}
		else {
			// WYSZUKIWARKA PROSTA - wyswietlenie formularza

			$sTemplate = 'wyszukiwarka_zaawansowana.tpl';
			$aModule['adv'] = '1';
			//	$aModule['mod_link'] = 'zaawansowane.html';
		}

		/**
		 *
		 * KONIEC WYSZUKIWANIA
		 */

		if ($aModule['results_no'] == 0) {
			$aModule['message_found'] = $aModule['lang']['no_results'];
		}
		else {
			$aModule['message_found'] = $aModule['results_no'];
		}
		if(!empty($_GET['q'])){
			$aModule['message'].= sprintf($aModule['lang']['searched_el'],$aModule['lang']['sphrase'],stripslashes($_GET['q'])).$aModule['lang']['searched_sep'];
		}

		if($this->bAuthor){
			$aModule['message'].= sprintf($aModule['lang']['searched_el'],$aModule['lang']['author'],stripslashes($_GET['autor'])).$aModule['lang']['searched_sep'];
		}
		if($this->bPublisher){
			$aModule['message'].= sprintf($aModule['lang']['searched_el'],$aModule['lang']['publisher'],stripslashes($_GET['publisher'])).$aModule['lang']['searched_sep'];
		}
		if($this->bSeries){
			$aModule['message'].= sprintf($aModule['lang']['searched_el'],$aModule['lang']['series'],stripslashes($_GET['series'])).$aModule['lang']['searched_sep'];
		}
		if($this->bYear){
			$aModule['message'] .= sprintf($aModule['lang']['searched_el'],$aModule['lang']['year'],stripslashes($_GET['year'])).$aModule['lang']['searched_sep'];
		}
		if($this->bISBN){
			$aModule['message'] .= sprintf($aModule['lang']['searched_el'],$aModule['lang']['isbn'],stripslashes($_GET['isbn'])).$aModule['lang']['searched_sep'];
		}
		if($this->bTag){
			$aModule['message'] .= sprintf($aModule['lang']['searched_el'],$aModule['lang']['tag'],stripslashes($_GET['tag'])).$aModule['lang']['searched_sep'];
		}
		if($this->bTopCat){
			$aModule['message'] .= sprintf($aModule['lang']['searched_el'],$aModule['lang']['top_cats'],$this->sTopCatName).$aModule['lang']['searched_sep'];
		}
		if($this->bSubCat){
			$aModule['message'] .= sprintf($aModule['lang']['searched_el'],$aModule['lang']['sub_cats'],$this->sSubCatName).$aModule['lang']['searched_sep'];
		}
		if($this->bPriceFrom){
			$aModule['message'] .= $aModule['lang']['price_zak'].sprintf($aModule['lang']['searched_el'],$aModule['lang']['price_from2'],$_GET['price_from']).$aModule['lang']['searched_sep'];
		}
		if($this->bPriceTo){
			$aModule['message'] .= $aModule['lang']['price_zak'].sprintf($aModule['lang']['searched_el'],$aModule['lang']['price_to'],$_GET['price_to']).$aModule['lang']['searched_sep'];
		}
		if($this->bAval){
			$aModule['message'] .= sprintf($aModule['lang']['searched_el2'],$aModule['lang']['search_aval']).$aModule['lang']['searched_sep'];
		}

		if (!$this->bPhraseLengthErr) {
			$aModule['message']=substr($aModule['message'],0,-1*strlen($aModule['lang']['searched_sep']));
			setSubTitle(strip_tags($aModule['message']));
			setDescription(strip_tags($aModule['message']));
			setKeywords(preg_replace('/\s+/', ', ', strip_tags($aModule['message'])));
		}
		if ($this->bShowMessage == false) {
			$aModule['message'] = '';
			$aModule['message_found'] = '';
		}

		$ids = [];
        if ($aModule['results_no'] !== 0) {
            foreach ($aModule['results'] as $result) {
                $ids[] = $result['id'];
            }
        }

        $aConfig['header_js'][]="</script>";
        $aConfig['header_js'][]="<script type=\"text/javascript\">";
        $aConfig['header_js'][]="window.criteo_q = window.criteo_q || [];";
	    $aConfig['header_js'][]="var deviceType = /iPad/.test(navigator.userAgent) ? \"t\" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? \"m\" : \"d\";";
	    $aConfig['header_js'][]="window.criteo_q.push(";
        $aConfig['header_js'][]="{ event: \"setAccount\", account: 57447},";
		$aConfig['header_js'][]="{ event: \"setEmail\", email: \"" .( isset($_SESSION['w_user']['email'])?$_SESSION['w_user']['email']:"" ). "\" },";
		$aConfig['header_js'][]="{ event: \"setSiteType\", type: deviceType},";
		$aConfig['header_js'][]="{ event: \"viewList\", item: [" . implode(',', $ids) . "]});";
        $aConfig['header_js'][]="</script>";
        $aConfig['header_js'][]="<script type=\"text/javascript\">";

        include_once($_SERVER['DOCUMENT_ROOT'].'omniaCMS/boxes/m_seo/client/Box.class.php');
        $aBox['module'] = 'm_seo';
        $aBox['template'] = 'domyslny.tpl';
        $box = new Box_m_seo($aBox);
        $aModule['box_m_seo'] = $box->getContent();

		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatesPath.$sTemplate);
		$pSmarty->clear_assign('aModule');

		//$oTimer->setMarker($this->sModule.'_STOP');
		return $sHtml;
	} // end of getContent()


	/**
	 * Metoda na podstawie przeslanych w GET danych tworzy czesc zapytania SQL
	 * odpowiedzialna za wyszukanie w klauzuli WHERE
	 *
	 * @return	string
	 */
	function prepareAdvancedWhereSql() {
		global $aConfig;
		$sSql = '';

		if (isset($_GET['q'])) {
			if (($_GET['q'] = trim($_GET['q'])) != '') {
				$this->bPhrase = true;
				if (mb_strlen(stripslashes($_GET['q']), 'utf8') >= 2) {
					$_GET['q'] = htmlspecialchars($_GET['q']);
					$aSearch = preg_split('/\s/', htmlspecialchars($_GET['q']), -1, PREG_SPLIT_NO_EMPTY);
					$sSearch='';
					foreach($aSearch as $sSearchItem) {
						$sSearch .= ' +'.$sSearchItem;
					}
						$sSql .= " AND MATCH (A.title, A.name2) 
														AGAINST (".Common::Quote(trim(htmlspecialchars_decode($sSearch)))." IN BOOLEAN MODE)";
				}
				else {
					// zbyt krotka fraza
					$this->bPhraseLengthErr = true;
				}
			}
		}
		if (isset($_GET['autor'])) {
			if (($_GET['autor'] = trim($_GET['autor'])) != '') {
				$this->bPerformSearch = true;
				$_GET['autor'] = htmlspecialchars($_GET['autor']);
				if (mb_strlen(stripslashes($_GET['autor']), 'utf8') >= 2) {
					$this->bAuthor = true;
					$aSearch = preg_split('/\s/', htmlspecialchars($_GET['autor']), -1, PREG_SPLIT_NO_EMPTY);
					$sSearch='';
					foreach($aSearch as $sSearchItem) {
						$sSearch .= ' +'.$sSearchItem;
					}
					$sSql .= " AND MATCH (A.authors) 
													AGAINST (".Common::Quote(trim(htmlspecialchars_decode($sSearch)))." IN BOOLEAN MODE)";
				}
			}
		}
		if (isset($_GET['tag'])) {
			if (($_GET['tag'] = trim($_GET['tag'])) != '') {
				$this->bPerformSearch = true;
				$_GET['tag'] = htmlspecialchars($_GET['tag']);
				if (mb_strlen(stripslashes($_GET['tag']), 'utf8') >= 2) {
					$this->bTag = true;
					$aSearch = preg_split('/\s/', htmlspecialchars($_GET['tag']), -1, PREG_SPLIT_NO_EMPTY);
					$sSearch='';
					foreach($aSearch as $sSearchItem) {
						$sSearch .= ' +'.$sSearchItem;
					}
					$sSql .= " AND MATCH (A.tags) 
													AGAINST (".Common::Quote(trim(htmlspecialchars_decode($sSearch)))." IN BOOLEAN MODE)";
				}
			}
		}
		if (isset($_GET['publisher'])) {
			if (($_GET['publisher'] = trim(htmlspecialchars($_GET['publisher']))) != '') {
				$this->bPerformSearch = true;
				//$_GET['publisher'] = htmlspecialchars($_GET['publisher']);
				if (mb_strlen(stripslashes($_GET['publisher']), 'utf8') >= 2) {
					$this->bPublisher = true;
					$sSql .= " AND A.publisher = ".Common::Quote(htmlspecialchars_decode($_GET['publisher']));
				}
			}
		}
		if (isset($_GET['series'])) {
			if (($_GET['series'] = trim($_GET['series'])) != '') {
				$this->bPerformSearch = true;
				$_GET['series'] = htmlspecialchars($_GET['series']);
				if (mb_strlen(stripslashes($_GET['series']), 'utf8') >= 2) {
					$this->bSeries = true;
					$sSql .= " AND A.series = ".Common::Quote(htmlspecialchars_decode($_GET['series']));
				}
			}
		}
		if (isset($_GET['year'])) {
			if (($_GET['year'] = (int) $_GET['year']) != '') {
				$this->bPerformSearch = true;
				if ($_GET['year'] > 0) {
					$this->bYear = true;
					$sSql .= " AND A.publication_year = ".$_GET['year'];
				}
			}
		}
		if (isset($_GET['isbn'])) {
			if (($_GET['isbn'] = trim($_GET['isbn'])) != '') {
				$this->bPerformSearch = true;
				$_GET['isbn'] = htmlspecialchars($_GET['isbn']);
				if ($_GET['isbn'] != '') {
					$this->bISBN = true;
					$sSql .= " AND (A.isbn_plain = '".$this->fixedIsbn2plain($_GET['isbn'])."' OR 
													A.isbn_10 = '".$this->fixedIsbn2plain($_GET['isbn'])."' OR
													A.isbn_13 = '".$this->fixedIsbn2plain($_GET['isbn'])."' OR
													A.ean_13 = '".$this->fixedIsbn2plain($_GET['isbn'])."' )";
				}
			}
		}
		if (isset($_GET['price_from'])) {
			if (($_GET['price_from'] = trim($_GET['price_from'])) != '') {
				$this->bPerformSearch = true;
				$_GET['price_from'] = Common::formatPrice2($_GET['price_from']);
				if ($_GET['price_from'] > 0) {
					$this->bPriceFrom = true;
					$sSql .= " AND A.price >= ".$_GET['price_from'];
				}
			}
		}
		if (isset($_GET['price_to'])) {
			if (($_GET['price_to'] = trim($_GET['price_to'])) != '') {
				$this->bPerformSearch = true;
				$_GET['price_to'] = Common::formatPrice2($_GET['price_to']);
				if ($_GET['price_to'] > 0) {
					$this->bPriceTo = true;
					$sSql .= " AND A.price <= ".$_GET['price_to'];
				}
			}
		}

		if (isset($_GET['aval'])) {
			$this->bAval = true;
		} else {
			$sSql .= " AND (A.prod_status = '1' OR A.prod_status = '3')";
		}


		if(!empty($_GET['top_cat'])){
				$this->bTopCat = true;
				$this->sTopCatName = $this->getCatName($_GET['top_cat']);
				$sSql.=" AND ( A.page_id_1 = ".intval($_GET['top_cat']).
												" OR A.page_id_2 = ".intval($_GET['top_cat']).
												" OR A.page_id_3 = ".intval($_GET['top_cat']).")";
		}

	if(!empty($_GET['cat'])){
				$this->bSubCat = true;
				$this->sSubCatName = $this->getCatName($_GET['cat']);
				$sSql.=" AND C.page_id =  ".intval($_GET['cat']);
		}

		return $sSql;
	} // end of prepareAdvancedWhereSql() method


	/**
	 * Metoda tworzy zapytania SQL i wykonuje wyszukiwanie
	 *
	 * @param	array ref	$aModule	- tablica z danymi modulu
	 * @param	string	$sWhereSql	- czesc zapytania SQL do klauzuli WHERE
	 * @param	float	$fUserDiscount	- rabat uzytkownika
	 * @return	void
	 */
	function performAdvancedSearch(&$aModule, $sWhereSql, $fUserDiscount) {
		global $aConfig, $oCache;
		$aResults = array();
		$sSorterSql = $this->getSearchSorterSql();

		if (!$this->bDoCache || !is_array($aResults = unserialize($oCache->get($this->sCacheName, 'modules')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u


		$sQSql = "SELECT COUNT(A.id)
							 FROM ".$aConfig['tabls']['prefix']."products_shadow A"
						 .($this->bSubCat?"
						 JOIN ".$aConfig['tabls']['prefix']."products_extra_categories C
						 ON C.product_id=A.id
						 ":'').
						 " WHERE 1=1
						 		".$sWhereSql;

		$sSql = "SELECT A.id, A.title, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, A.isbn, A.isbn_plain, A.publication_year, A.binding,
											A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews, A.packet, A.type
						 FROM ".$aConfig['tabls']['prefix']."products_shadow A "
						 .($this->bSubCat?"
						 JOIN ".$aConfig['tabls']['prefix']."products_extra_categories C
						 ON C.product_id=A.id
						 ":'').
						 " WHERE 1=1
						 		".$sWhereSql.$sSorterSql;
			// sprawdzenie czy istnieja rekordy odpowiadajace zadanym kryteriom wyszukiwania
			$iRecords = (int) Common::GetOne($sQSql);

			// istnieja rekordy - wybranie
			$this->getRecords($aResults, $sSql);

			// zapis do cache'a
			if ($this->bDoCache)
				$oCache->save(serialize($aResults), $this->sCacheName, 'modules');
		}
		if (is_array($aResults))
			$aModule = array_merge($aModule, $aResults);
		else
			$aModule['results_no'] = 0;
	} // end of performAdvancedSearch() method


  /**
   * Poprawiona wersja czyszcząca isbn z syfu
   *
   * @param type $sIsbn
   * @return type
   */
	private function fixedIsbn2plain($sIsbn){

		return preg_replace('/[^\dX]/', '', $sIsbn);
	}// end of fixedIsbn2plain() method


	/**
	 * Metoda na podstawie przeslanych w GET danych tworzy czesc zapytania SQL
	 * odpowiedzialna za wyszukanie w klauzuli WHERE
	 *
	 * @return	string
	 */
	function prepareBasicWhereSql() {
		global $aConfig;
		$sSql = '';
		$aMatches = array();

		if (isset($_GET['q'])) {
			$_GET['q'] = trim(htmlspecialchars($_GET['q']));
			$this->bPhrase = true;
			if (mb_strlen(stripslashes($_GET['q']), 'utf8') >= 2) {
				$aSearch = preg_split('/\s/', $_GET['q'], -1, PREG_SPLIT_NO_EMPTY);
				$sSearch='';
				foreach($aSearch as $sSearchItem) {
					$sSearch .= ' +'.$sSearchItem;
				}
				$sQuery = trim(htmlspecialchars_decode($sSearch));

				// metoda sprawdza czy wprowadzony został ISBN
				$aIsbnPatterns = array(
					"/^[0-9]{1,7}[-]{0,1}[0-9]{1,7}[-]{0,1}[0-9]{1,7}[-]{0,1}[0-9X]{1}$/",
					"/^[0-9]{1,9}[-]{0,1}[0-9]{1,5}[-]{0,1}[0-9]{1,9}[-]{0,1}[0-9]{1,9}[-]{0,1}[0-9]{1}$/",
          "/^[0-9]{1,7}[-]{0,1}[0-9]{1,7}[-]{0,1}[0-9]{1,7}[-]{0,1}[0-9X]{1}$/"
				);

				$bMatches = false;
				foreach ($aIsbnPatterns as $sPattern) {
					if ((preg_match($sPattern, $_GET['q'], $aMatches) > 0) == TRUE) {
            $bMatches = true;
						$sQuery = $this->fixedIsbn2plain($sQuery);
						break;
					}
				}
				// metoda sprawdza czy wprowadzony został ISBN
//				$bMatches = preg_match($sPattern, $sQuery, $aMatches);
				if ($bMatches == TRUE) {
					$sQuery = $this->fixedIsbn2plain($_GET['q']);
				}
        if ($bMatches == TRUE) {
          $sSql="(A.isbn_plain = ".Common::Quote($sQuery)." OR
                  A.isbn_10 = ".Common::Quote($sQuery)." OR
                  A.isbn_13 = ".Common::Quote($sQuery)." OR 
                  A.ean_13 = ".Common::Quote($sQuery).")";
        } else {
          $sSql=" MATCH (A.title, A.authors, A.tags, A.name2) 
								AGAINST (".Common::Quote($sQuery)." IN BOOLEAN MODE) ";
        }
				if(!empty($_GET['top_cat'])){
					$this->sTopCatName = $this->getCatName($_GET['top_cat']);
					$sSql.=" AND ( A.page_id_1 = ".intval($_GET['top_cat']).
												" OR A.page_id_2 = ".intval($_GET['top_cat']).
												" OR A.page_id_3 = ".intval($_GET['top_cat']).")";
				}
				if (isset($_GET['aval'])) {
					$this->bAval = true;
				} else {
					$sSql .= " AND (A.prod_status = '1' OR A.prod_status = '3')";
				}
			}
			else {
				// zbyt krotka fraza
				$this->bPhraseLengthErr = true;
			}
		}
		return $sSql;
	} // end of prepareBasicWhereSql() method


  /**
   * Metoda sprawdza czy podany string to ISBN
   *
   * @param string $sStr
   * @return boolean
   */
  function SimpleCheckISBN($sStr) {

    $aMatched = array();
    preg_match("/^([0-9X]{8,13})$/", $sStr, $aMatched);
    if (isset($aMatched[1]) || (is_numeric($sStr) && $sStr > 0 && $sStr < 2018688)) {
      return true;
    }
    return false;
  }// end of SimpleCheckISBN() method


	/**
	 * Metoda tworzy zapytania SQL i wykonuje wyszukiwanie podstawowe
	 *
	 * @param	array ref	$aModule	- tablica z danymi modulu
	 * @param	string	$sWhereSql	- czesc zapytania SQL do klauzuli WHERE
	 * @param	float	$fUserDiscount	- rabat uzytkownika
	 * @return	void
	 */
	function performBasicSearch(&$aModule, $sWhereSql, $fUserDiscount) {
		global $aConfig, $oCache;
		$aResults = array();
		$sSorterSql = $this->getSearchSorterSql();

		if (!$this->bDoCache || !is_array($aResults = unserialize($oCache->get($this->sCacheName, 'modules')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu

		$sQSql = "SELECT COUNT(A.id)
							 FROM ".$aConfig['tabls']['prefix']."products_shadow A
						 WHERE
						 		".$sWhereSql;

		$sSql = "SELECT A.id, A.title, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, A.isbn, A.isbn_plain, 
										A.publication_year, A.binding, A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews, A.packet, A.type
						 FROM ".$aConfig['tabls']['prefix']."products_shadow A
						 WHERE
						 		".$sWhereSql.$sSorterSql;


			// sprawdzenie czy istnieja rekordy odpowiadajace zadanym kryteriom wyszukiwania
//				$iRecords = (int) Common::GetOne($sQSql);
//			if ($iRecords > 0) {
				// istnieja rekordy - wybranie
				$this->getRecords($aResults, $sSql);
//			}
			// zapis do cache'a
			if ($this->bDoCache)
				$oCache->save(serialize($aResults), $this->sCacheName, 'modules');
		}
		if (is_array($aResults))
			$aModule = array_merge($aModule, $aResults);
		else
			$aModule['results_no'] = 0;
	} // end of performBasicSearch() method


	/**
	 * Metoda tworzy zapytania SQL i wykonuje wyszukiwanie
	 *
	 * @param	array ref	$aModule	- tablica z danymi modulu
	 * @param	float	$fUserDiscount	- rabat uzytkownika
	 * @return	void
	 */
	function performItemSearch(&$aModule, $fUserDiscount) {
		global $aConfig, $oCache;
		$aResults = array();
		$sSearchAuthorQuery = '';
		$sSorterSql = $this->getSearchSorterSql();
		$sFilterSql = $this->getFilterSql();

		if ($this->bAuthor) {
			$_GET['autor'] = htmlspecialchars(rawurldecode($_GET['autor']));
			$aSearch = preg_split('/\s/', $_GET['autor'], -1, PREG_SPLIT_NO_EMPTY);
			$sSearch='';
			foreach($aSearch as $sSearchItem) {
				$sSearch .= ' +'.$sSearchItem;
			}

				$sWhereSql .= " AND MATCH (A.authors) 
													AGAINST (".Common::Quote(trim(htmlspecialchars_decode($sSearch)))." IN BOOLEAN MODE)";

			$aModule['item'] = "Autor: <span>".stripslashes($_GET['autor'])."</span>";
			// tytul strony
			setTitle(stripslashes($_GET['autor']).' - '.$aConfig['lang']['mod_'.$this->sModule]['author_books']);

		}
		if ($this->bTag) {
			$_GET['tag'] = htmlspecialchars(rawurldecode($_GET['tag']));
			$aSearch = preg_split('/\s/', $_GET['tag'], -1, PREG_SPLIT_NO_EMPTY);
			$sSearch='';
			foreach($aSearch as $sSearchItem) {
				$sSearch .= ' +'.$sSearchItem;
			}

				$sWhereSql .= " AND MATCH (A.tags) 
													AGAINST (".Common::Quote(trim(htmlspecialchars_decode($sSearch)))." IN BOOLEAN MODE)";

			$aModule['item'] = "Tag: <span>".stripslashes($_GET['tag'])."</span>";
			// tytul strony
			setTitle(stripslashes($_GET['tag']).' - '.$aConfig['_tmp']['page']['name']);

		}
		if ($this->bPublisher) {
			$_GET['publisher'] = htmlspecialchars(rawurldecode($_GET['publisher']));
			$sWhereSql .= " AND A.publisher = ".Common::Quote(htmlspecialchars_decode($_GET['publisher']));

			//$aModule['item_info'] = $this->getPublisherInfo();
			$aModule['item'] .= "Wydawnictwo: <span>".stripslashes($_GET['publisher'])."</span>";
			// tytul strony
			setTitle(stripslashes($_GET['publisher']).' - '.$aConfig['_tmp']['page']['name']);
		}
		if ($this->bSeries) {
			$_GET['series'] = htmlspecialchars(rawurldecode($_GET['series']));
			$sWhereSql .= " AND A.series = ".Common::Quote(htmlspecialchars_decode($_GET['series']));
			//$aModule['item_info'] = $this->getSeriesInfo();
			$aModule['item'] .= "Seria: <span>".stripslashes($_GET['series'])."</span>";
			// tytul strony
			setTitle(stripslashes($_GET['series']).' - '.$aConfig['_tmp']['page']['name']);

		}


		if (!$this->bDoCache || !is_array($aResults = unserialize($oCache->get($this->sCacheName, 'modules')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu

			$sQSql = "SELECT COUNT(A.id)
						 FROM ".$aConfig['tabls']['prefix']."products_shadow A
					 WHERE 1=1
					 		".$sFilterSql.$sWhereSql;

			$sSql = "SELECT A.id, A.title, A.name2,  A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, A.isbn, A.isbn_plain, A.publication_year, A.binding, 
											A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews, A.packet, A.type
						 FROM ".$aConfig['tabls']['prefix']."products_shadow A
						 WHERE 1=1
						 		".$sFilterSql.$sWhereSql.$sSorterSql;

			// sprawdzenie czy istnieja rekordy odpowiadajace zadanym kryteriom wyszukiwania
//				$iRecords = (int) Common::GetOne($sQSql);
//			if ($iRecords > 0) {
			// istnieja rekordy - wybranie
			$this->getRecords($aResults, $sSql);
//			}
			// zapis do cache'a
			if ($this->bDoCache)
				$oCache->save(serialize($aResults), $this->sCacheName, 'modules');
		}
		if (is_array($aResults))
			$aModule = array_merge($aModule, $aResults);
		else
			$aModule['results_no'] = 0;
	} // end of performItemSearch() method


	/**
	 * Metoda pobiera wynik wyszukiwania
	 *
	 * @param	array ref	$aModule	- tablica z danymi modulu
	 * @param	string	$sSql	- zapytanie SQL
	 * @param	integer	$iRecords	- liczba znalezionych rekordow
	 * @return	void
	 */
	function getRecords(&$aModule, $sSql) {
		global $aConfig;

		$isElastic = true;

		$elastic = new ElasticSearchStrategy();
		//TODO jezeli nic to lipa

		if (true === $isElastic) {
			$ids = $elastic->regularSearch($this->aSettings['items_per_page'], false, true);
			if (!empty($ids['ids'])) {
				$impldt = implode(',', $ids['ids']);
				$sSql = "SELECT A.id, A.title, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, A.isbn, A.isbn_plain,
										A.publication_year, A.binding, A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews, A.packet, A.type
							 FROM products_shadow A
							 WHERE A.id in (%s) ORDER BY FIELD(A.id, %s)
							 ";
				$sSql = sprintf($sSql, $impldt, $impldt);
				$aModule['results'] =& Common::GetAll($sSql);
			}
		}

		if ($this->aSettings['items_per_page'] > 0) {
			// uzycie pagera do stronicowania
			include_once('Pager/CPager.class.php');
			$aPagerParams = array('firstPageText' => $aConfig['lang']['pager']['first_page'], 'lastPageText' => $aConfig['lang']['pager']['last_page'], 'prevImg' => $aConfig['lang']['pager']['previous_page'], 'nextImg' => $aConfig['lang']['pager']['next_page']);
			$aPagerParams['perPage'] = $this->aSettings['items_per_page'];
			$aPagerParams['path'] = $this->sPageLink;
			$aPagerParams['fileName'] = $this->formatPagerLink();
			$aPagerParams['append'] = false;

			if (true === $isElastic) {
				$total = (int)($ids['total']);
				//TODO jezeli nic to lipa
				$iRecords = $total;
				$aModule['results_no'] = $iRecords;

				$oPager = new CPager($iRecords, $aPagerParams);
				$iStartFrom = $oPager->getStartFrom();

				// linki Pagera
				$aModule['pager']['links'] =& $oPager->getLinks('all');
				$aModule['pager']['total_pages'] = $oPager->getLinks('total_pages');
				$aModule['pager']['total_items'] = $total;
				$aModule['pager']['current_page'] =& $oPager->getLinks('current_page');
			} else {

				//TODO jezeli nic to lipa
				$iRecords = (int) Common::GetOne($sSql);
				$aModule['results_no'] = $iRecords;
				$oPager = new CPager($iRecords, $aPagerParams);
				$iStartFrom = $oPager->getStartFrom();

				$aModule['pager']['links'] =& $oPager->getLinks('all');
				$aModule['pager']['total_pages'] =& $oPager->getLinks('total_pages');
				$aModule['pager']['total_items'] =& $oPager->getLinks('total_items');
				$aModule['pager']['current_page'] =& $oPager->getLinks('current_page');
				$aModule['results'] =& Common::GetAll($sSql);
			}

			// dodanie zakresu wybieranych rekordow do zapytania SQL
			$sSql .= " LIMIT ".$iStartFrom.", ".$this->aSettings['items_per_page'];
		}
		// pobranie wynikow

		// TODO zakomentowane
		if (true === empty($aModule['results_no'])) {
			return;
		}

		$aModule['results_no'] = $iRecords;
		include_once('FreeTransportPromo.class.php');
		$oFreeTransportPromo = new Lib_FreeTransportPromo();
		foreach ($aModule['results'] as $iKey => $aItem) {
			// okladka
			$aModule['results'][$iKey]['image'] =  $this->getImages($aItem['id'], true);

			// link do produktu
			$aModule['results'][$iKey]['link'] = createProductLink($aModule['results'][$iKey]['id'], $aModule['results'][$iKey]['title']);
			// autorzy
			$aModule['results'][$iKey]['authors'] =$this->getAuthors($aItem['id']);

			//pobranie cennika
			$aModule['results'][$iKey]['tarrif'] = $this->getTarrif($aItem['id']);
			$aModule['results'][$iKey]['tarrif']['packet'] = ($aItem['packet']=='1');
			// przeliczenie i formatowanie cen
			$aModule['results'][$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aModule['results'][$iKey]['price_brutto'], $aModule['results'][$iKey]['tarrif'], $aModule['results'][$iKey]['promo_text'],$aModule['results'][$iKey]['discount_limit'], $this->getSourceDiscountLimit($aItem['id'])));
			$aModule['results'][$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']);

			$aModule['results'][$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aItem['id'], 'add');
			$aModule['results'][$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aItem['id'],'add_to_repository,');

            $pShipmentTime = new \orders\Shipment\ShipmentTime();
			$aModule['results'][$iKey]['shipment']=$pShipmentTime->getShipmentTime($aItem['id'], $aItem['shipment_time']);
			$aModule['results'][$iKey]['shipment_date'] = FormatDate($aItem['shipment_date']);
			$aModule['results'][$iKey]['free_delivery'] = $oFreeTransportPromo->getLogoNameBySymbol($oFreeTransportPromo->getBookFreeTransport($aItem['id'], $aModule['results'][$iKey]['promo_price']));
		}
	} // end of getRecords() method


	/**
	 * Metoda formatuje link dla pagera
	 *
	 * @return	string
	 */
	function formatPagerLink($bAdvanced=true) {
        global $aConfig;

		$sPrefix = '';
		$sLink = $this->bPhrase ? 'q='.urlencode(stripslashes($_GET['q'])).'&':'';
		$sLink .= $this->bAdvanced ? 'adv=1&' : '';
		$sLink .= $this->bYear ? 'year='.$_GET['year'].'&' : '';
		$sLink .= $this->bISBN ? 'isbn='.stripslashes($_GET['isbn']).'&' : '';
		$sLink .= $this->bPriceFrom ? 'price_from='.$_GET['price_from'].'&' : '';
		$sLink .= $this->bPriceTo ? 'price_to='.$_GET['price_to'].'&' : '';

		$sLink .= isset($_GET['top_cat']) ? 'top_cat='.$_GET['top_cat'].'&' : '';
		$sLink .= isset($_GET['cat']) ? 'cat='.$_GET['cat'].'&' : '';
		if (($this->bAuthor || $this->bPublisher || $this->bTag || $this->bSeries) && $_GET['result'] != '1') {
			$sItemLink = $this->formatPathLink(false);
			$sItemLink = str_replace(array('%2F','%5C'), array('%252F','%255C'), $sItemLink);
			$sLink .= $bAdvanced ? $sItemLink.',%d?' : '';
			$sLink = str_replace('%26amp%3B', ';amp;', $sLink);
			$sPrefix = '';
		} else {
			if ($bAdvanced) {
				$sLink .= $this->formatPathLink(true);
				$sLink .= '&p=%d&result=1&';
				$sPrefix = '?';
			}
		}

        if (isset($_GET['sort']) || isset($_GET['filter']) || isset($_GET['aval'])) {
            // sortujemy/filtrujemy
            if (isset($_GET['p']) && intval($_GET['p']) > 1) {

                // podmieniamy %d
                $sLinkTMP = str_replace('%d', $_GET['p'], $sLink);
                $baseLink = $aConfig['common']['client_base_url_https_no_slash'] . $this->sPageLink.$sPrefix.substr($sLinkTMP,0,-1);
            } else {
                // usuwamy
                $sLinkTMP = str_replace('&p=%d', '', $sLink);
                $sLinkTMP = str_replace(',%d?', '', $sLinkTMP);
                $baseLink = $aConfig['common']['client_base_url_https_no_slash'] . $this->sPageLink.$sPrefix.substr($sLinkTMP,0,-1);
            }


            $additionalHead = new additionalHead();
            $additionalHead->addHeadLinkRel('canonical', $baseLink);
        }

		$sLink .= isset($_GET['aval']) ? 'aval='.$_GET['aval'].'&' : '';
		$sLink .= isset($_GET['sort']) ? 'sort='.$_GET['sort'].'&' : '';
		$sLink .= isset($_GET['filter']) ? 'filter='.$_GET['filter'].'&' : '';

		return $sPrefix.substr($sLink,0,-1);
	} // end of formatPagerLink() method


	/**
	 * Metoda generuje symbol strony dla zmodyfikowanych linków
	 *
	 * @return type
	 */
	function formatPathLink($bQueryAmp) {

		if ($bQueryAmp) {
			$sLink = '';
			$sLink .= $this->bAuthor ? '&autor='.urlencode(stripslashes($_GET['autor'])) : '';
			$sLink .= $this->bPublisher ? '&publisher='.urlencode(stripslashes($_GET['publisher'])) : '';
			$sLink .= $this->bTag ? '&tag='.urlencode(stripslashes($_GET['tag'])) : '';
			$sLink .= $this->bSeries ? '&series='.urlencode(stripslashes($_GET['series'])) : '';
		} else {
			$sLink = '';
			$sLink .= $this->bAuthor ? urlencode(stripslashes($_GET['autor'])) : '';
			$sLink .= $this->bPublisher ? urlencode(stripslashes($_GET['publisher'])) : '';
			$sLink .= $this->bTag ? urlencode(stripslashes($_GET['tag'])) : '';
			if ($this->bSeries && $this->bPublisher) {
				$sLink .= '/';
			}
			$sLink .= $this->bSeries ? urlencode(stripslashes($_GET['series'])) : '';
			$sLink = '/'.$sLink;
		}

		return $sLink;
	}// end of formatPathLink() method


	/**
	 * Metoda sprawdza czy jest wybrane wyszukiwanie dla elementu - autor, seria,
	 * indeks rzeczowy, tag
	 * Przy okazji przetwarza dane z GET
	 *
	 * @return	bool
	 */
	function isItemSearch() {
		$bItemSearch = false;
		if (isset($_GET['autor'])) {
			$_GET['autor'] = trim($_GET['autor']);
			if (mb_strlen($_GET['autor'], 'utf8') >= 2) {
				$this->bAuthor = true;
				$bItemSearch =  true;
			}
		}
		if (isset($_GET['publisher'])) {
			$_GET['publisher'] = trim($_GET['publisher']);
			if (mb_strlen($_GET['publisher'], 'utf8') >= 2) {
				$this->bPublisher = true;
				$bItemSearch =  true;
			}
		}
		if (isset($_GET['series'])) {
			$_GET['series'] = trim($_GET['series']);
			if (mb_strlen($_GET['series'], 'utf8') >= 2) {
				$this->bSeries = true;
				$bItemSearch =  true;
			}
		}
		if (isset($_GET['tag'])) {
			$_GET['tag'] = trim($_GET['tag']);
			if (mb_strlen($_GET['tag'], 'utf8') >= 2) {
				$this->bTag = true;
				$bItemSearch =  true;
			}
		}
		return $bItemSearch;
	} // end of isItemSearch() method




	/**
	 * Metoda zapisuje do bazy informacje o wyszukiwanej frazie. Przed dodaniem
	 * sprawdza czy w danej sesji fraza nie zostala juz zapisana
	 *
	 * @return	void
	 */
	function writePhrase() {
		global $aConfig;
		// sprawdzenie czy fraza dla podanej sesji juz zostala zapisana
		$sSql = "SELECT COUNT(*) FROM ".$aConfig['tabls']['prefix']."search_phrases WHERE phrase = ".Common::Quote($_GET['q']);
		if((int) Common::GetOne($sSql) == 0) {
			// dodanie informacji o wyszukiwanej frazie
			$aValues = array(
				'phrase' => $_GET['q'],
				'counter' => 1
			);
			Common::Insert($aConfig['tabls']['prefix']."search_phrases", $aValues, '', false);
		}
		else {
			$sSql="UPDATE ".$aConfig['tabls']['prefix']."search_phrases 
							SET counter=counter+1 
							WHERE phrase = ".Common::Quote($_GET['q']);
			Common::Query($sSql);
		}
	} // end of writePhrase() method


	function getTopPages(){
		global $aConfig;
		$sSql = "SELECT menu1_id, menu2_id, menu3_id 
						 FROM ".$aConfig['tabls']['prefix']."menus_box_glowna_settings";
		$aMenus = Common::GetRow($sSql);

		$sSql="SELECT menu_id, id AS value, name AS label
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".getModuleId('m_oferta_produktowa')."
					 AND mtype = '0'
					 AND parent_id IS NULL
					 AND published = '1'";
		$aTopCats = Common::GetAssoc($sSql);
		$aTopPages[] = array('value'=>'','label'=>$aConfig['lang']['mod_'.$this->sModule]['all_categories']);
		$aTopPages[] = $aTopCats[$aMenus['menu1_id']];
		$aTopPages[] = $aTopCats[$aMenus['menu2_id']];
		$aTopPages[] = $aTopCats[$aMenus['menu3_id']];
		return $aTopPages;
	}

	function &getYears(){
		global $aConfig;
		// pobranie wszystkich jezykow
			$sSql = "SELECT DISTINCT year as value, year as label
							 FROM ".$aConfig['tabls']['prefix']."products_years
							 ORDER BY year DESC";
			$aItems= Common::GetAll($sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}

	function getCatName($iCId) {
		global $aConfig;
		// pobranie wszystkich jezykow
			$sSql = "SELECT name
							 FROM ".$aConfig['tabls']['prefix']."menus_items
							 WHERE id=".intval($iCId);
			return Common::GetOne($sSql);
	}
} // end of Module Class
?>