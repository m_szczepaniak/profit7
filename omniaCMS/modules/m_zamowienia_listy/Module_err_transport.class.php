<?php
/**
 * Klasa zakładki błędnych transportów
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-12 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__zamowienia_listy__err_transport {

  // komunikat
	public $sMsg;

	// nazwa modulu - do langow
	public $sModule;

	// id wersji jezykowej
	public $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	private $aPrivileges;
	
  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  public $pDbMgr;
  
  /**
   * Typ listy
   */
  CONST REASON_TYPE = 'T';
  
  function __construct(&$pSmarty, $bInit = true) {
    global $pDbMgr, $aConfig;
    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');

    $this->pDbMgr = $pDbMgr;
    
		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];

		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		$this->sModule .= '_duplicates';
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
    
    
    if ($bInit == false) {
      return;
    }
    switch ($sDo) {
      default:
        $this->Show($pSmarty);
      break;
      case 'activate':
        $this->Activate($pSmarty, $iId);
      break;
      case 'auto-source':
        $this->AutoSources($pSmarty, $iId);
      break;
    }
  }
  
//  private function AutoSources($pSmarty, $iOrderId) {
//    $oAutoChangeStatus = new \orders\auto_change_status\autoProviderToOrders(false, $this->pDbMgr);
//    $this->pDbMgr->BeginTransaction('profit24');
//    $aOrdersItems = $oAutoChangeStatus->getSingleOrderData($iOrderId);
//    if (!empty($aOrdersItems)) {
//      $aInformData = $oAutoChangeStatus->UpdateOrdersItems($aOrdersItems);
//
//      if ($aInformData === false) {
//        // nie spelnia innych warunkow obrobienia zamowienia
//        $sMsg .= sprintf(_('Wystąpił błąd podczas obrabiania zamówienia o id = %s'), $iOrderId);
//        AddLog($sMsg);
//        $this->sMsg .= GetMessage($sMsg);
//
//        $this->pDbMgr->RollbackTransaction('profit24');
//      } else {
//        $sMsg .= sprintf(_('Zamówienie o id %s zostało odznaczone'), $iOrderId);
//        AddLog($sMsg, false);
//        $this->sMsg .= GetMessage($sMsg, false);
//
//        $oAutoChangeStatus->informAboutStatusChanged($aInformData);
//        $this->pDbMgr->CommitTransaction('profit24');
//      }
//    } else {
//      // nie spelnia innych warunkow obrobienia zamowienia
//      $sMsg .= sprintf(_('Brak produktow do obrobienia w zamówieniu o id %s'), $iOrderId);
//      AddLog($sMsg);
//      $this->sMsg .= GetMessage($sMsg);
//    }
//    $this->Show($pSmarty);
//  }

	private function AutoSources($pSmarty, $iOrderId)
	{
		$oAutoChangeStatus = new \orders\auto_change_status\autoProviderToOrders(false, $this->pDbMgr);

		$aOrdersItems = $oAutoChangeStatus->getSingleOrderData($iOrderId);
		if (!empty($aOrdersItems)) {

			try {

				$aOrdersItems = [
						$iOrderId => $aOrdersItems
				];

				$oAutoChangeStatus->UpdateOrdersItemsAndReservations($aOrdersItems, true);
			} catch (\Exception $e) {
				$this->sMsg .= GetMessage($e->getMessage());
				return $this->Show($pSmarty);
			}

			$sMsg = sprintf(_('Zamówienie o id %s zostało odznaczone'), $iOrderId);
			AddLog($sMsg, false);
			$this->sMsg .= GetMessage($sMsg, false);

		} else {
			$this->sMsg = sprintf(_('Brak produktow do obrobienia w zamówieniu o id %s'), $iOrderId);
		}

		$this->Show($pSmarty);
	}

  /**
   * Lista duplikatow
   * 
   * @param type $pSmarty
   */
  private function Show(&$pSmarty) {
    
    $oDuplicates = new \orders_containers\OrdersContainers();
    $sColAdress = ' (
      SELECT CONCAT(OUA.postal, " ", OUA.city) 
      FROM orders_users_addresses AS OUA 
      WHERE 
        OUA.order_id = O.id AND 
        OUA.address_type = "1"
      ) AS postal_city ';
    
    $aCols = array('ORC.id', 'O.id AS order_id', 'O.order_number', 'O.transport_id', 'ORC.checked', 'ORC.checked_by', 'O.order_date', 'O.point_of_receipt', 'O.order_status', $sColAdress);
    $aRecordsHeader = $this->getViewHeaderRecords();
    $sHTML = $oDuplicates->getOrdersContainersView(self::REASON_TYPE, $aCols, $aRecordsHeader, _('Bledny transport'));
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
                                    $sHTML);
  }
  
  /**
   * Nagłówki tabeli
   * 
   * @global type $aConfig
   * @return array
   */
  private function getViewHeaderRecords() {
    global $aConfig;
    
    $aRecordsHeader = array(
			array(
				'db_field'	=> 'order_id',
				'content'	=> _('#OID'),
				'sortable'	=> true,
				'width' => '150'
			),
			array(
				'db_field'	=> 'order_number',
				'content'	=> _('Numer zamówienia'),
				'sortable'	=> true,
				'width' => '180'
			),
			array(
				'db_field'	=> 'transport_id',
				'content'	=> _('Transport'),
				'sortable'	=> true,
				'width' => '180'
			),
			array(
				'db_field'	=> 'checked',
				'content'	=> _('Wyłączone'),
				'sortable'	=> true,
				'width' => '100'
			),
			array(
				'db_field'	=> 'checked_by',
				'content'	=> _('Wyłączone przez'),
				'sortable'	=> true,
				'width' => '180'
			),
      array(
				'db_field'	=> 'order_date',
				'content'	=> _('Data złożenia zamówienia'),
				'sortable'	=> true,
				'width' => '260'
			),
      array(
				'db_field'	=> 'point_of_receipt',
				'content'	=> _('Punkt odbioru przesyłki'),
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'postal_city',
				'content'	=> _('Kod/Miasto'),
				'sortable'	=> true,
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '30'
			)
		);    
    return $aRecordsHeader;
  }
  
  private function Activate(&$pSmarty, $iId) {
    $oDuplicates = new \orders_containers\OrdersContainers();
    $oDuplicates->checkContainer($iId, self::REASON_TYPE);
    
    $this->Show($pSmarty);
  }
}

