<?php

/**
 * Klasa duplikatów
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-12
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__zamowienia_listy__deliver {

  // komunikat
  public $sMsg;
  // nazwa modulu - do langow
  public $sModule;
  // id wersji jezykowej
  public $iLangId;
  // uprawnienia uzytkownika dla wersji jez.
  private $aPrivileges;

  private static $aTransportMethodsMethodsIdsEqual = [8,4];

  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  public $pDbMgr;

  /**
   *
   * @global \DatabaseManager $pDbMgr
   * @global array $aConfig
   * @param \Smarty $pSmarty
   * @param bool $bInit
   */
  function __construct(&$pSmarty, $bInit = true) {
    global $pDbMgr, $aConfig;
    include_once($aConfig['common']['client_base_path'] . 'LIB/autoloader.php');


    $this->pDbMgr = $pDbMgr;

    $this->iLangId = $_SESSION['lang']['id'];
    $_GET['lang_id'] = $this->iLangId;
    $this->sModule = $_GET['module'];

    $sDo = '';

    if (isset($_GET['do'])) {
      $sDo = $_GET['do'];
    }
    if (isset($_POST['do'])) {
      $sDo = $_POST['do'];
    }
    $this->sModule .= '_deliver';

    $this->aPrivileges = & $_SESSION['user']['privileges'];
    // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
    if ($_SESSION['user']['type'] === 0) {
      if (!hasModulePrivileges($_GET['module_id'])) {
        showPrivsAlert($pSmarty);
        return;
      }
    }

    if (isset($_GET['id'])) {
      $iId = $_GET['id'];
    }


    if ($bInit == false) {
      return;
    }

    switch ($sDo) {
      default:
        $this->Show($pSmarty);
        break;
      case 'get_deliver':
        $this->getDeliver($pSmarty);
        break;
        case 'own_deliver':
            return $this->doOwnDeliver($pSmarty);
        break;
        case 'check_repair_packages_status':
            return $this->doCheckRepairPackagesStatus($pSmarty);
        break;
    }
  }

  /**
   * Metoda usuwa rekordy z bufora
   * @param integer $mData
   */
  private function clearTransportBuffer($mData, $iWebsiteId = null) {
    $aData = explode('_',$mData);
    $iTransportId = $aData[0];
    if(!isset($iWebsiteId)) {
      $iWebsiteId = $aData[1];
    }


    $iTransportId = $this->mergeTransportIfEqual($iTransportId);
    $sWhereSql = ' transport_id IN('.$iTransportId.')';
    if ($iWebsiteId > 0 ) {
      $sWhereSql .= ' AND website_id = '.$iWebsiteId;
    }
    $aValues = array('get_deliver' => '1');

    if ($this->pDbMgr->Update('profit24', 'orders_transport_buffer', $aValues, $sWhereSql) === false) {
      $sMsg = _('Nie udało się wyczyścić bufora');
      $this->sMsg = GetMessage($sMsg);
      return false;
    } else {
      return true;
    }
  }

  /**
   * Metoda łączy metody transportu jeśli jest to metoda równoważna
   *
   * @param $iTransportId
   * @return array
   */
  private function mergeTransportIfEqual($iTransportId) {

    if (in_array($iTransportId,  self::$aTransportMethodsMethodsIdsEqual)) {
      $iTransportId = implode(', ', self::$aTransportMethodsMethodsIdsEqual);
    }
    return $iTransportId;
  }


  /**
   *
   * @global \DatabaseManager $pDbMgr
   */
  private function getDeliver($pSmarty) {
    global $pDbMgr;

    $aDeliverWebsite = $_POST['get_deliver'];
    $d = array_pop(array_keys($aDeliverWebsite));
    $aTMP = explode('_', $d);
    $iTransportId = intval($aTMP[0]);
    if (intval($aTMP[1]) > 0) {
      $iWebsiteId = intval($aTMP[1]);
    }
    $aOrdersIdTransportsNumber = $this->getOrdersIdTransportNumberAssoc($iTransportId, $iWebsiteId);
    if (!empty($aOrdersIdTransportsNumber)) {
      try {
        $this->pDbMgr->BeginTransaction('profit24');
        $oShipment = new \orders\Shipment($iTransportId, $pDbMgr);
        $sPDF = $oShipment->getShipmentList(array_values($aOrdersIdTransportsNumber), array_keys($aOrdersIdTransportsNumber));
        $this->clearTransportBuffer($iTransportId, $iWebsiteId);
        $this->pDbMgr->CommitTransaction('profit24');

        $sFileNamePath = $oShipment->getOrderDeliverFilePath(array_pop(array_keys($aOrdersIdTransportsNumber)));
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $this->getFileNameByFilePath($sFileNamePath) . '"');
        header('Content-Transfer-Encoding: binary');
        echo file_get_contents($sFileNamePath);
        exit;
      } catch (\Exception $ex) {
        $this->pDbMgr->RollbackTransaction('profit24');
        $sMsg = _('Wystąpił błąd podczs pobierania delivera - lista błedów') . '<br />' . $ex->getMessage();
        $oAlert = new \orders_alert\OrdersAlert();
        $oAlert->createAlert('NULL', $_SESSION['user']['name'], $sMsg);
        $oAlert->addAlerts();
        AddLog($sMsg, false);
        $this->sMsg = GetMessage($sMsg);
        return $this->Show($pSmarty);
      }
      $sMsg = sprintf(_('Pobrano delivera z %s'), $this->getTransportMethodName($iTransportId));
      AddLog($sMsg, false);
      $this->sMsg = GetMessage($sMsg, false);
      $this->Show($pSmarty);
    } else {
      $sMsg = sprintf(_('Brak zamówień zatwierdzonych w %s'), $this->getTransportMethodName($iTransportId));
      AddLog($sMsg, true);
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
    }
  }

  /**
   * Pobieramy nazwę pliku ze ścieżki
   *
   * @param string $sFilePathName
   * @return string
   */
  private function getFileNameByFilePath($sFilePathName) {

    $aData = explode(DIRECTORY_SEPARATOR, $sFilePathName);
    return array_pop($aData);
  }

  /**
   *
   * @global \DatabaseManager $pDbMgr
   * @param int $iTransportId
   * @return string
   */
  private function getTransportMethodName($iTransportId) {

    $sSql = 'SELECT name FROM orders_transport_means WHERE id = ' . $iTransportId;
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }

  /**
   * Lista duplikatow
   *
   * @param type $pSmarty
   */
  private function Show(&$pSmarty) {
    global $aConfig;

    $sConfirmedOrdersHTML = '';
    $confirmedOrders = $this->getInfoAboutConfirmedOrders();
    if (!empty($confirmedOrders)) {
        $sConfirmedOrdersHTML = $this->getConfirmedOrdersHTML($confirmedOrders);
    }

    if ($_GET['clear_buffer'] == 1) {
      if ($this->clearTransportBuffer($_GET['transport_id']) === TRUE) {
        $sMsg = _('Bufor został wyczyszczony');
        $this->sMsg = GetMessage($sMsg, false);
      }
    }
    include_once('Form/FormTable.class.php');
    $pForm = new \FormTable('get_deliver', _('Pobierz Delivera'), array('action' => phpSelf(array('do' => 'get_deliver'))), array('col_width' => 455));

    $aSpeditorsServices = $this->getOrdersToSpeditors();
    $aOtherDeliverToService = array('poczta-polska-doreczenie-pod-adres', 'poczta_polska', 'paczkomaty_24_7');
    $aOmmitTransportIds = array();
    $aDelivers = array();
    foreach ($aSpeditorsServices as $iWebsiteId => $aGetDelivers) {
      foreach ($aGetDelivers as $iTransportId => $aDeliver) {
        if (!in_array($iTransportId, $aOmmitTransportIds)) {
          if (in_array($aDeliver['symbol'], $aOtherDeliverToService)) {
            // inny przycisk w zależności od serwisu
            $aDelivers[$iTransportId . '_' . $iWebsiteId] = $aDeliver;
          } else {
            // ten sam przycisk dla wielu serwisów
            $aDelivers[$iTransportId] = $this->SumOtherDeliverReference($aSpeditorsServices, $aDeliver, $iWebsiteId, $iTransportId);
            $aOmmitTransportIds[] = $iTransportId;
          }
        }
      }
    }
    ksort($aDelivers);
    foreach ($aDelivers as $mTransportIdWebsiteId => $aDeliver) {
      if (strstr($mTransportIdWebsiteId, '_')) {
        // wiele przycisków
        $pForm->AddRow(
                _('Pobierz Delivera ' . $aDeliver['name']), $this->getButtonInfoHTML($pForm, $aDeliver, $mTransportIdWebsiteId, $aDeliver['website_name'])
                , array(), array('style' => 'padding: 15px')
        );
      } else {
          $sAddButtonsHTML = '';
          if ($aDeliver['symbol'] == 'opek-przesylka-kurierska') {
            // dodatkowe przyciski
              // nasz deliver

              // sprawdź statusy przesyłek i wyczyść z buffora przesyłki z nieprawidłowym statusem
            $sAddButtonsHTML .= '<div style="float: left"><a href="' .
                phpSelf(array(
                    'do' => 'own_deliver',
                    'transport_id' => $mTransportIdWebsiteId,
                ))
            . '" style="display:block;border:1px solid #999477;background:#D1CCAB;color:#3C373F;padding:10px;text-decoration:none;box-shadow:5px 5px 5px 0px #3C373F"><b>Pobierz nasz Deliver (jeśli zwykły nie działa)</b></a> <br /><br />';

            $sAddButtonsHTML .= '<a href="' .
                  phpSelf(array(
                      'do' => 'check_repair_packages_status',
                      'transport_id' => $mTransportIdWebsiteId,
                  ))
                  . '" style="display:block;border:1px solid #999477;background:#D1CCAB;color:#3C373F;padding:10px;text-decoration:none;box-shadow:5px 5px 5px 0px #3C373F"><b>Napraw błędne przesyłki (jeśli błąd w FedEx)</b></a>
                  </div>';


          }
        // jeden przycisk na metodę
        $pForm->AddRow(
                _('Pobierz Delivera ' . $aDeliver['name']), $this->getButtonInfoHTML($pForm, $aDeliver, $mTransportIdWebsiteId, $aDeliver['website_name']).$sAddButtonsHTML, array(), array('style' => 'padding: 15px')
        );
      }
    }

    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') .
        $sConfirmedOrdersHTML.$pForm->ShowForm());
  }

    /**
     * @param $pSmarty
     */
  public function doOwnDeliver($pSmarty) {
      global $pDbMgr;

      $iTransportId = $_GET['transport_id'];

      $aOrdersIdTransportsNumber = $this->getOrdersIdTransportNumberAssoc($iTransportId);
      if (!empty($aOrdersIdTransportsNumber)) {
          try {
              $this->pDbMgr->BeginTransaction('profit24');
              $oShipment = new \orders\Shipment($iTransportId, $pDbMgr);
              $sPDF = $oShipment->getOwnDeliver('Deliver', array_values($aOrdersIdTransportsNumber), array_keys($aOrdersIdTransportsNumber));

              $this->clearTransportBuffer($iTransportId);
              $this->pDbMgr->CommitTransaction('profit24');

              $sFileNamePath = $oShipment->getOrderDeliverFilePath(array_pop(array_keys($aOrdersIdTransportsNumber)));
              header('Content-Type: application/pdf');
              header('Content-Disposition: attachment; filename="' . $this->getFileNameByFilePath($sFileNamePath) . '"');
              header('Content-Transfer-Encoding: binary');
              echo file_get_contents($sFileNamePath);
              exit;
          } catch (\Exception $ex) {
              $this->pDbMgr->RollbackTransaction('profit24');
              $sMsg = _('Wystąpił błąd podczs pobierania delivera - lista błedów') . '<br />' . $ex->getMessage();
              $oAlert = new \orders_alert\OrdersAlert();
              $oAlert->createAlert('NULL', $_SESSION['user']['name'], $sMsg);
              $oAlert->addAlerts();
              AddLog($sMsg, false);
              $this->sMsg = GetMessage($sMsg);
              return $this->Show($pSmarty);
          }
          $sMsg = sprintf(_('Pobrano delivera z %s'), $this->getTransportMethodName($iTransportId));
          AddLog($sMsg, false);
          $this->sMsg = GetMessage($sMsg, false);
          $this->Show($pSmarty);
      } else {
          $sMsg = sprintf(_('Brak zamówień zatwierdzonych w %s'), $this->getTransportMethodName($iTransportId));
          AddLog($sMsg, true);
          $this->sMsg = GetMessage($sMsg);
          $this->Show($pSmarty);
      }


      return $this->Show($pSmarty);
  }

    /**
     * @param $pSmarty
     */
  public function doCheckRepairPackagesStatus($pSmarty) {
      global $pDbMgr;

      echo 'check';
      $transportFedEx = $_GET['transport_id'];
      $transportFedEx = 1;

      $sSql = 'SELECT O.transport_number
         FROM orders_transport_buffer AS OTB
         JOIN orders AS O
            ON O.id = OTB.order_id
         WHERE OTB.transport_id = '.$transportFedEx.'
         ';
      $transportNumbersFedEx = $pDbMgr->GetCol('profit24', $sSql);
//      $transportNumbersFedEx[] = '6231286361262';
//      $transportNumbersFedEx[] = '6231286727921';
      $oShipment = new \orders\Shipment($transportFedEx, $pDbMgr);
      $errorStatusTransports = $oShipment->doSprawdzStatusyPrzesylek('Deliver', $transportNumbersFedEx);
      if (!empty($errorStatusTransports)) {
          foreach ($errorStatusTransports as $transportNumber => $errorStatusTransport) {
              $sSql = 'SELECT OTB.id
                 FROM orders_transport_buffer AS OTB
                 JOIN orders AS O
                    ON O.id = OTB.order_id
                 WHERE OTB.transport_id = '.$transportFedEx.' AND O.transport_number = "'.$transportNumber.'"
                 ';
              $OTBId = $pDbMgr->GetOne('profit24', $sSql);

              var_dump($OTBId);
//              if ($OTBId > 0) {
//                  $aValues = [
//                      'get_deliver' => '1'
//                  ];
//                  if ($this->pDbMgr->Update('profit24', 'orders_transport_buffer', $aValues, ' id=' . $OTBId.' LIMIT 1') === false) {
//                      $sMsg = _('Nie udało się naprawić buffora: ' . print_r($aValues, true));
//                      $this->sMsg = GetMessage($sMsg);
//                      return false;
//                  }
//              }
          }
          $msg = 'Błędny status przesyłek, przesyłki zostaly usuniete z delivera: "'.implode('", "', array_keys($errorStatusTransports)).'" <br /> Dane pomocnicze'.print_r($errorStatusTransports, true);
          $this->sMsg .= GetMessage($msg);
          AddLog($msg);
      } else {
          $msg = 'Wszystkie przesyłki mają prawidłowy status. Lista sprawdzonych przesyłek - '.print_r($transportNumbersFedEx, true);
          $this->sMsg = GetMessage($msg, false);
          AddLog($msg, false);
      }


      return $this->Show($pSmarty);
  }

    /**
     * @param $confirmedOrders
     * @return string
     */
  private function getConfirmedOrdersHTML($confirmedOrders) {

      include_once('Form/FormTable.class.php');
      $pForm = new \FormTable('transport_orders', _('Zatwierdzone zamówienia'));
      foreach ($confirmedOrders as $transportOrders) {
          $pForm->AddRow($transportOrders['transport_name'] .' - '.$transportOrders['count'], $transportOrders['orders_confirmed']);
      }
      return $pForm->ShowForm();
  }

    /**
     * @return array
     */
  private function getInfoAboutConfirmedOrders() {

      $sSql = '
               SELECT count(O.id) as count, 
                      GROUP_CONCAT(O.order_number SEPARATOR ", ") as orders_confirmed, 
                      TM.name as transport_name
               FROM orders AS O
               JOIN orders_transport_means AS TM
                ON TM.id = O.transport_id
			   JOIN orders_packing_times AS OPT
				ON OPT.order_id = O.id And OPT.status = "9"
               WHERE O.order_status = "3"
               GROUP BY O.transport_id
               ORDER BY count DESC
               ';
      return $this->pDbMgr->GetAll('profit24', $sSql);
  }

  /**
   *
   * @param array $aSpeditorsServices
   * @param array $aDeliver
   * @param int $iTransportId
   * @return array
   */
  private function SumOtherDeliverReference(&$aSpeditorsServices, $aDeliver, $iCurrentWebsiteId, $iCurrentTransportId) {
    // usuwamy, dane delivera
    unset($aSpeditorsServices[$iCurrentWebsiteId][$iCurrentTransportId]);

    foreach ($aSpeditorsServices as $iWebsiteId => $aGetDelivers) {
      if (isset($aGetDelivers[$iCurrentTransportId])) {
        $aDeliverOther = $aGetDelivers[$iCurrentTransportId];
        $aDeliver['postal_fee_amount'] += $aDeliverOther['postal_fee_amount'];
        $aDeliver['count'] += $aDeliverOther['count'];
        unset($aSpeditorsServices[$iWebsiteId][$iCurrentTransportId]);
      }
    }

    return $aDeliver;
  }

  /**
   *
   * @param \FormTable $pForm
   * @param array $aDeliver
   * @param int $mTransportIdWebsiteId
   * @param string $sWebsite
   * @return string
   */
  private function getButtonInfoHTML(&$pForm, $aDeliver, $mTransportIdWebsiteId, $sWebsite = '') {
    global $aConfig;
    $sLogoPrefix = '/' . $aConfig['common']['transport_logos_dir'];

    $sStr = '<div style="float: left; margin: 10px;">' .
            ($sWebsite != '' ? '<b>' . $sWebsite . '</b><br />' : '') .
            'Zamówień: <b>' . $aDeliver['count'] . '</b><br />' .
            'Pobranie: <b>' . $aDeliver['postal_fee_amount'] . '</b><br />' .
            $pForm->GetInputButtonHTML('get_deliver[' . $mTransportIdWebsiteId . ']', $mTransportIdWebsiteId, array('style' => 'padding: 15px; font-size: 20px;', 'src' => $sLogoPrefix . '/' . $aDeliver['logo']), 'image') . '<br /><br />
            <a href="' .
            phpSelf(array(
                'clear_buffer' => 1,
                'transport_id' => $mTransportIdWebsiteId,
            ))
            . '" style="display:block;border:1px solid #999477;background:#D1CCAB;color:#3C373F;padding:10px;text-decoration:none;box-shadow:5px 5px 5px 0px #3C373F"><b>Wyczyść bufor</b></a></div>';
    return $sStr;
  }

  /**
   *
   * @param int $iTransportId
   * @param int $iWebsiteId
   * @return int
   */
  private function getOrdersIdTransportNumberAssoc($iTransportId, $iWebsiteId = NULL) {

    $iTransportId = $this->mergeTransportIfEqual($iTransportId);

    $sSql = 'SELECT O.id, O.transport_number
             FROM orders_transport_buffer AS OTB
             JOIN orders AS O
              ON OTB.order_id = O.id 
             WHERE 
              O.transport_id IN ( ' . $iTransportId . ' ) AND
              ' . ($iWebsiteId > 0 ? ' O.website_id = ' . $iWebsiteId . ' AND ' : '') . '
              ' . $this->getConfirmedOrdersToGetDeliverWhereSQL();
    return $this->pDbMgr->GetAssoc('profit24', $sSql, false);
  }

  private function getConfirmedOrdersToGetDeliverWhereSQL() {

    $sWhereSQL = ' O.order_status = "4"
              AND O.transport_number IS NOT NULL 
              AND O.transport_number <> ""
              AND O.confirm_printout_speditor <> "1"
              AND OTB.get_deliver = "0"
              ';
    return $sWhereSQL;
  }

  /**
   *
   * @return array
   */
  private function getOrdersToSpeditors() {

    $sSql = 'SELECT O.website_id, O.transport_id, OTM.symbol, COUNT(O.id) AS count, OTM.name,
                SUM(IF (((O.payment_type = "postal_fee" || O.second_payment_type = "postal_fee") && (O.to_pay - O.paid_amount) > 0), O.to_pay - O.paid_amount, 0)) AS postal_fee_amount,
                (SELECT name FROM websites AS W WHERE W.id = O.website_id) AS website_name,
                OTM.logo
             FROM orders_transport_buffer AS OTB
             JOIN orders AS O
             ON OTB.order_id = O.id 
             JOIN orders_transport_means AS OTM
             ON OTB.transport_id = OTM.id
             WHERE ' . $this->getConfirmedOrdersToGetDeliverWhereSQL() . '
             GROUP BY O.website_id, O.transport_id
             ';
    $aSpeditorsServices = $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);
    $aNewSpeditors = array();
    foreach ($aSpeditorsServices as $iWebsiteId => &$aSpeditors) {
      foreach ($aSpeditors as $aSpeditor) {
        $aNewSpeditors[$iWebsiteId][$aSpeditor['transport_id']] = $aSpeditor;
      }
    }
    return $aNewSpeditors;
  }
}
