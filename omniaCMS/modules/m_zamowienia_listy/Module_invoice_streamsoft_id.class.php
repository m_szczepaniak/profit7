<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-07 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_listy;

use Admin;
use DatabaseManager;
use FormTable;
use omniaCMS\lib\interfaces\ModuleEntity;
use orders\OrderStreamsoftId;
use Smarty;
use Validator;

/**
 * Description of Module_invoice_streamsoft_id
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_listy__invoice_streamsoft_id implements ModuleEntity
{

    /**
     *
     * @var string
     */
    private $sMsg;

    /**
     *
     * @var string
     */
    private $sModule;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    public function __construct(Admin $oClassRepository)
    {

        $this->oClassRepository = $oClassRepository;
        $this->em = $this->oClassRepository->entityManager;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }


    public function getMsg()
    {
        return $this->sMsg;
    }

    /**
     *
     */
    public function doDefault()
    {
        $aUserAddress = $this->getItems();
        if (!empty($aUserAddress)) {
            return $this->getFormView($aUserAddress);
        }
        return _('Brak danych do FV do przeniesienia');
    }

    /**
     *
     * @return string html
     */
    public function doUpdate()
    {

        if (!empty($_POST)) {
            if ($this->validateAllForm() === true) {
                if (isset($_GET['uoa_id']) && intval($_GET['uoa_id']) > 0 &&
                    isset($_POST['value_id']) && intval($_POST['value_id']) > 0
                ) {
                    $iUOAId = intval($_GET['uoa_id']);
                    $iStreamsoftId = intval($_POST['value_id']);
                    $aValues = array(
                        'streamsoft_id' => $iStreamsoftId
                    );

                    $error = false;

                    $this->pDbMgr->BeginTransaction('profit24');


                    $orderUserAddress = $this->pDbMgr->GetRow('profit24', "SELECT user_adress_id, is_company, company, `name`, surname, street, number, number2, postal, city, nip, phone FROM orders_users_addresses WHERE id = $iUOAId");
                    $userAddressId = $orderUserAddress['user_adress_id'];

                    if ($this->pDbMgr->Update('profit24', 'orders_users_addresses', $aValues, ' id=' . $iUOAId) === false) {
                        $this->sMsg .= GetMessage(_('Wystąpił błąd podczas wprowadzania id adresu streamsoft'));
                        $error = true;
                    }

                    if (null != $userAddressId) {

                        $UserAccountAddress = $this->pDbMgr->GetRow('profit24', "SELECT is_company, company, `name`, surname, street, number, number2, postal, city, nip, phone FROM users_accounts_address WHERE id = $userAddressId");

                        $OrderStreamsoftId = new OrderStreamsoftId();
                        $addressesAreTheSame = $OrderStreamsoftId->compareOrderArrays($UserAccountAddress, $orderUserAddress, ['is_postal_mismatch', 'address_type', 'default_invoice', 'default_transport']);

                        if ($addressesAreTheSame === true) {
                            if ($this->pDbMgr->Update('profit24', 'users_accounts_address', $aValues, ' id=' . $userAddressId) === false) {
                                $this->sMsg .= GetMessage(_('Brak powiazania z adresem uzytkownika'));
                                $error = true;
                            }
                        }
                    }

                    if ($error == true) {
                        $this->pDbMgr->RollbackTransaction('profit24');
                    } else {
                        $this->sMsg .= GetMessage(_('Wprowadzono id kontrahenta w streamsoft'), false);
                        $this->pDbMgr->CommitTransaction('profit24');
                    }

                    return $this->doDefault();
                }
            }
        } else {
            return $this->doDefault();
        }
    }


    /**
     *
     * @return boolean
     */
    private function validateAllForm()
    {
        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $sErrLog = $oValidator->GetErrorString();
            $this->sMsg .= GetMessage($sErrLog, true);
            addLog($sErrLog, true);
            return false;
        }
        return true;
    }

    /**
     *
     * @param type $aUserAdress
     * @return array
     */
    private function getFormView($aUserAdress)
    {
        include_once('Form/FormTable.class.php');
        $sFirstHeader = _('<h3 style="text-align:center;">Lista adresów FV do uzupełnienia. <b>Maksymalnie 40</b> </h3>');
        $bIsFirst = true;
        foreach ($aUserAdress as $aUAItem) {
            $pForm = new FormTable('user_address_streamsoft_id_' . $aUAItem['id'], ($bIsFirst ? $sFirstHeader : ''),
                array('action' => phpSelf(array('do' => 'update', 'uoa_id' => $aUAItem['id']))),
                array('col_width' => 135));

            $pForm->AddRow('Zamówienie: ', $aUAItem['order_number']);
            $pForm->AddRow('Nr FV', ($aUAItem['address_type'] == 0 ? '1' : '2'));

            if ($aUAItem['is_company'] == '1') {
                $pForm->AddRow('NIP', $aUAItem['nip']);
                $pForm->AddRow('Firma', $aUAItem['company']);
            }

            $sAddDate = $aUAItem['name'] . ' ' .
                $aUAItem['surname'] . ', ' .
                $aUAItem['street'] . ' ' .
                $aUAItem['number'] . ' ' .
                $aUAItem['number2'] . ' ' .
                $aUAItem['postal'] . ' ' .
                $aUAItem['city'] . ' ' .
                $aUAItem['phone'];
            $pForm->AddRow('Adres: ', $sAddDate);

            $pForm->AddText('value_id', 'Streamsoft ID', '', array(), '', 'int', true);
            $pForm->AddInputButton(_('Zapisz'), _('Zapisz'));
            $sHTML .= $pForm->ShowForm() . '<hr>';
            $bIsFirst = false;
        }

        return ShowTable($sHTML);
    }

    /**
     *
     * @return array
     */
    private function getItems()
    {

        $sSql = '
    SELECT OUA.*,O.order_number
    FROM orders AS O
    JOIN orders_users_addresses AS OUA
            ON OUA.order_id = O.id AND (OUA.streamsoft_id IS NULL OR OUA.streamsoft_id = "0")
    WHERE 
      (   
        O.payment_type = "bank_14days" OR
        (O.second_payment_enabled = "1" AND O.second_payment_type = "bank_14days")
      )
    AND (OUA.address_type = "0" OR OUA.address_type = "2")
    AND (O.payment_status = "0" OR O.payment_status = "2" OR DATE(O.payment_date) >= "2015-05-04")
    AND O.order_status <> "5"
    AND (O.order_status = "3" OR O.order_status = "4") 
    ORDER BY OUA.id DESC
    LIMIT 40
      ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }
}
