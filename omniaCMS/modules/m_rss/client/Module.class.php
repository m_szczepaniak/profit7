<?php
/**
 * Klasa module do obslugi modulu rss
 *
 */
include_once('modules/m_oferta_produktowa/client/Common.class.php');
class Module extends Common_m_oferta_produktowa  {  
	
	/**
	 * Metoda pobiera tresc strony o id $this->iPageId
	 * dowiazuje ja do Smartow oraz zwraca xHTML z trescia strony po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		
		$aSiteSettings =& getSiteSettings();
		
		$this->setCMSVersion();
		
		if(isset($_GET['action']) && $_GET['action'] != ""){
			$_GET['no_output'] = '1';
									
			$aRSSData = array(
				'_attributes' => array('version' => '2.0'),
				'channel' => array(
					'title' => $aSiteSettings['title'],
					'link' => $aConfig['common']['base_url_http'],
					'description' => $aSiteSettings['description'],
					'language' => $this->sLang,
					'generator' => $aConfig['common']['name'].' '.
												 $aConfig['common']['version'],
					'pubDate' => date('r'),
					'lastBuildDate' => date('r')
				)
			);
			
			$iReqPageId=0;
			$sMOption = 'nowosci';
			if(!empty($_GET['action'])){
				$sSql="SELECT id, name, moption
							 FROM ".$aConfig['tabls']['prefix']."menus_items
							 WHERE symbol = '".$_GET['action']."'";
				$aPage=Common::GetRow($sSql);
				if(!empty($aPage)){
					$iReqPageId=$aPage['id'];
					$aRSSData['channel']['title'].=' - '.$aPage['name'];
					$sMOption = $aPage['moption'];
				}
			}
			
			$aRSSData['channel'] = array_merge($aRSSData['channel'], $this->getRSSList($sMOption, $iReqPageId));
			
			// dolaczenie klasy XML/Serializer
			require_once ("XML/Serializer.php");
			// Serializer options
			$aOptions = array(
				'addDecl'	=> true,
				'encoding'	=> 'utf-8',
				'indent'	=>	"\t",
				'rootName'	=>	'rss',
				'defaultTagName'	=>	'item',
				'attributesArray'	=> '_attributes'
			);
			
			$oSerializer = new XML_Serializer($aOptions);
			$oResult = $oSerializer->serialize($aRSSData);
	
			if (PEAR::isError($oResult)) {
				if ($aConfig['common']['status'] == 'development') {
					TriggerError($status->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
				}
				return false;
			}
			header("Content-type: text/xml");
			echo $oSerializer->getSerializedData();
				
			return $sHtml;
		} else {
			doRedirect('/');
		}
	} // end of getContent()
	
	
	/**
	 * Metoda pobiera liste aktualnosci i zwraca je w formacie wymaganym dos tworzenia
	 * kanalu RSS
	 * @param string $sMOption - opcja modułu 'powiazane z oferta produktową'
	 * @param int $iPage - id strony
	 * 
	 * @return	array	- lista aktualnosci
	 */
	function &getRSSList($sMOption, $iPage=0){
		global $aConfig;
		$aTmp = array();
		$aRecords2=array();
		$aModule['lang'] =& $aConfig['lang']['mod_m_rss'];
		
		switch($sMOption){
			case 'nowosci':
				// zapytanie wybierajace liste produktow
			$sSql = "SELECT A.id, A.title AS name, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, 
											A.isbn, A.isbn_plain, A.publication_year, A.binding,
											A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							  JOIN ".$aConfig['tabls']['prefix']."products_news AS B
								 ON A.id = B.product_id
						 	 	WHERE (A.prod_status = '1' || A.prod_status = '3') ".
						($iPage>0?" AND B.page_id = ".$iPage:'')."
						 ORDER BY B.order_by ASC, A.order_by DESC
						 LIMIT 200";
			break;
			case 'promocje':
					// zapytanie wybierajace liste produktow
			$sSql = "SELECT A.id, A.title AS name, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, 
											A.isbn, A.isbn_plain, A.publication_year, A.binding,
											A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							 JOIN ".$aConfig['tabls']['prefix']."products_tarrifs B
					 			ON B.product_id=A.id
							 JOIN ".$aConfig['tabls']['prefix']."products_promotions C
							 ON C.id=B.promotion_id
						 	 	WHERE (A.prod_status = '1' || A.prod_status = '3') ".
						($iPage>0?" AND C.page_id = ".$iPage:'')."
						 ORDER BY A.order_by DESC
						 LIMIT 200";
			break;
			case 'bestsellery':
				// zapytanie wybierajace liste produktow
			$sSql = "SELECT A.id, A.title AS name, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, 
											A.isbn, A.isbn_plain, A.publication_year, A.binding,
											A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							 JOIN ".$aConfig['tabls']['prefix']."products_bestsellers B
								 ON A.id = B.product_id
						 	 	WHERE (A.prod_status = '1' || A.prod_status = '3') ".
						($iPage>0?" AND B.page_id = ".$iPage:'')."
						 ORDER BY B.order_by ASC, A.order_by DESC
						 LIMIT 200";
			break;
			case 'zapowiedzi':
				// zapytanie wybierajace liste produktow
			$sSql = "SELECT A.id, A.title AS name, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, 
											A.isbn, A.isbn_plain, A.publication_year, A.binding,
											A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							 JOIN ".$aConfig['tabls']['prefix']."products_previews B
								 ON A.id = B.product_id
						 	 	WHERE (A.prod_status = '3') ".
						($iPage>0?" AND B.page_id = ".$iPage:'')."
						 ORDER BY B.order_by ASC, A.order_by DESC
						 LIMIT 200";
			break;
			default:
				return $aRecords2;
			break;
		}
		// zapytanie wybierajace liste aktualnosci
			// zapytanie wybierajace liste produktow
		/*	$sSql = "SELECT A.id, A.title AS name, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, 
											A.isbn, A.isbn_plain, A.publication_year, A.binding,
											A.prod_status, A.publisher AS publisher_name, A.series, A.is_news, A.is_bestseller
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							  JOIN ".$aConfig['tabls']['prefix']."products_news AS B
								 ON A.id = B.product_id
						 	 	WHERE (A.prod_status = '1' || A.prod_status = '3') ".
						($iPage>0?" AND B.page_id = ".$iPage:'')."
						 ORDER BY B.order_by ASC, A.order_by DESC
						 LIMIT 200";*/
			/*$sSql = "SELECT A.id, A.page_id, A.publisher_id, A.name, A.plain_name, A.name2, 
											A.short_description, A.price_brutto, A.price_netto, A.vat, A.published, 
											A.prod_status, A.shipment_time, A.binding, A.shipment_date, A.isbn, A.isbn_plain, A.publication_year, 
											A.edition, UNIX_TIMESTAMP(A.created) date
							 FROM ".$aConfig['tabls']['prefix']."products AS A
							  JOIN ".$aConfig['tabls']['prefix']."products_news AS B
								 ON A.id = B.product_id
							WHERE (A.prod_status = '1' || A.prod_status = '3') ".
						($iPage>0?" AND B.page_id = ".$iPage:'')						 
						 			 .(!isPreviewMode() ? " AND A.published = '1'" : '')."
						 ORDER BY B.order_by ASC
						 LIMIT 200";*/
	// dump($sSql);
		$aRecords =& Common::GetAll($sSql);
		foreach ($aRecords as $iKey => $aItem) {
			// formatowanie daty
			$aRecords2[$iKey]['pubDate'] = date('r', $aItem['date']);
			$aRecords2[$iKey]['title'] = strip_tags($aItem['name']);
			
			//pobranie cennika
			$aRecords[$iKey]['tarrif'] = $this->getTarrif($aItem['id']);
			// przeliczenie i formatowanie cen
			$aRecords[$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aRecords[$iKey]['price_brutto'], $aRecords[$iKey]['tarrif'], $aRecords[$iKey]['promo_text'], $aRecords[$iKey]['discount_limit'], $this->getSourceDiscountLimit($aItem['id'])));
			$aRecords[$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']);
			
			if(!empty($aItem['name2'])){
				$aRecords2[$iKey]['description'] .= $aItem['name2']."<br />";
			}
			
			if($aRecords[$iKey]['promo_price'] > 0){
				$aRecords2[$iKey]['description'] .= $aModule['lang']['price_brutto'].'<strike>'.Common::formatPrice($aRecords[$iKey]['price_brutto']).$aConfig['lang']['common']['currency'].'</strike>, '.$aRecords[$iKey]['promo_text'].Common::formatPrice($aRecords[$iKey]['promo_price']).$aConfig['lang']['common']['currency']."<br />";
			} else {
				$aRecords2[$iKey]['description'] .= $aConfig['lang']['common']['cena_w_ksiegarni'].Common::formatPrice($aRecords[$iKey]['price_brutto']).$aConfig['lang']['common']['currency']."<br />";
			}
		
			
			$aAuthors2 = $this->getAuthors($aItem['id']);

			
			if(!empty($aAuthors2)){
				$aRecords2[$iKey]['description'] .= $aModule['lang']['autor']." ";
				$sAuthors='';
				//dump($aAuthors2);
				foreach($aAuthors2 as $sRole => $aAuthors){
					foreach($aAuthors as $sKey => $aAuthor){
						$sAuthors.= $aAuthor['name'].($sRole!='Autor'?' ('.$sRole.')':'').", ";
					}
				}
				$aRecords2[$iKey]['description'] .= substr($sAuthors,0,-2).'<br />';
			}
			
		// pobranie info o wydawcy
				if(!empty($aItem['publisher_name'])){
					$aRecords2[$iKey]['description'] .= $aModule['lang']['publisher']." ".$aItem['publisher_name'].'<br />';
				}
			
			$aRecords2[$iKey]['link'] = $aConfig['common']['base_url_http_no_slash'].createProductLink($aItem['id'], $aItem['name']);
					
			// utworzenie linku do aktualnosci
			$aRecords2[$iKey]['image'] =& $this->getItemImage($aItem['id']);
			//$aRecords2[$iKey]['image'] =

			
			if (!empty($aRecords2[$iKey]['image'])) {
				$aRecords2[$iKey]['enclosure'] = array(
					'_attributes' => array(
						'url' => $aConfig['common']['base_url_http_no_slash'].$aRecords2[$iKey]['image']['location'],
						'type' => $aRecords2[$iKey]['image']['mime']
					)
				);
			}
			unset($aRecords2[$iKey]['id']);
			unset($aRecords2[$iKey]['image']);
		}
		return $aRecords2;
	} // end of getRSSNewsList() method
	
		
	/**
	 * Metoda pobiera okladke ksiazki
	 */
	 function getItemImage($iId){
	 	global $aConfig;

	 	$aResult =getItemImage('products_images',array('product_id'=> $iId),'__t_');
	 	if(!empty($aResult)){
	 		$aImage['location'] = $aResult['src'];
			
		 	$aImage['mime'] = $aResult['mime'];
	 	}
	 	 return $aImage;
	 }
	
	/**
	 * Funkcja pobiera z bazy danych nazwe i wersje CMSa i ustawia je w tablicy $aConfig
	 * @return	void
	 */
	function setCMSVersion() {
		global $aConfig;
	
		$sSql = "SELECT name, version
						 FROM ".$aConfig['tabls']['prefix']."version";
		list ($aConfig['common']['name'], $aConfig['common']['version']) = Common::GetRow($sSql, array(), DB_FETCHMODE_ORDERED);
	} // end of setCMSVersion() function
} // end of Module Class
?>