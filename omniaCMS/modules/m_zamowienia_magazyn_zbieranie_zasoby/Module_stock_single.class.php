<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_zasoby;

use Admin;
use DatabaseManager;
use LIB\EntityManager\Entites\Magazine;
use LIB\orders\listType\filters\containerAvailableForEmployee;
use LIB\orders\listType\filters\containerAvailableForEmployeeThreatOrderAsAll;
use LIB\orders\listType\filters\detectHighLocation;
use LIB\orders\listType\filters\ordersItemsLimit;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\orders\listType\filters\checkMagazine;
use LIB\orders\listType\filters\ordersItemsLimit20;
use LIB\orders\listType\filters\ordersItemsLimit45;
use LIB\orders\listType\filters\sortPopular;
use LIB\orders\listType\filters\typeFilter;
use LIB\orders\listType\manageGetOrdersItemsList;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj\Abstract_getListType;
use Smarty;

/**
 * Description of Module_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_zbieranie_zasoby__stock_single extends Abstract_getListType implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected  $sMsg;
  
  /**
   *
   * @var Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  /**
   *
   * @var DatabaseManager
   */
  protected  $pDbMgr;
  
  /**
   *
   * @var manageGetOrdersItemsList 
   */
  protected $oManageLists;
  
  public function __construct(Admin $oClassRepository) {
      global $aConfig;

      $this->oClassRepository = $oClassRepository;
      $this->pSmarty = $this->oClassRepository->pSmarty;
      $this->sModule = $this->oClassRepository->sModule;
      $this->pDbMgr = $this->oClassRepository->pDbMgr;
      $destinationMagazineDeficiencies = Magazine::TYPE_HIGH_STOCK_SUPPLIES;
      if ($_GET['type'] == typeFilter::TYPE_MIXED && isset($_GET['magazine']) && $_GET['magazine'] == Magazine::TYPE_LOW_STOCK_SUPPLIES) {
        $destinationMagazineDeficiencies = Magazine::TYPE_LOW_STOCK_SUPPLIES;
      }
      $this->oManageLists = new manageGetOrdersItemsList($this->pDbMgr, $destinationMagazineDeficiencies);
      $this->oManageLists->setListType('SINGLE_SORTER_STOCK');
      $this->oManageLists->setGetFromTrain(FALSE);
      $this->oManageLists->initOrderListType();
      $this->sListName = 'Stock Single';
      $this->cListType = '1';

      $oFilter = new typeFilter($this->pDbMgr);
      $oFilter->setFilterCollectingType($_GET['type']);

      $this->oManageLists->addFilter($oFilter);
      $this->oManageLists->addRequiredFilter($oFilter);


      if ($_GET['type'] == typeFilter::TYPE_MIXED) {
        $detectHighLocation = new detectHighLocation($this->pDbMgr);
        if (isset($_GET['magazine']) && $_GET['magazine'] == Magazine::TYPE_LOW_STOCK_SUPPLIES) {
          $detectHighLocation->unsetIFNotSH(true);
          $detectHighLocation->unsetIFSH(false);
        } else {
          $detectHighLocation->unsetIFNotSH(false);
          $detectHighLocation->unsetIFSH(true);
        }
        $detectHighLocation->setMinQuantityStock(detectHighLocation::MIN_QUANTITY_HIGH_LOCATION);

        $this->oManageLists->addFilter($detectHighLocation);
        $this->oManageLists->addRequiredFilter($detectHighLocation);

        // sortujemy wg. popularnych pozycji
        $oFilter = new sortPopular($this->pDbMgr);
        $this->oManageLists->addFilter($oFilter);
        $this->oManageLists->addRequiredFilter($oFilter);

        if (isset($_GET['magazine']) && $_GET['magazine'] == Magazine::TYPE_LOW_STOCK_SUPPLIES) {
          if (isset($_GET['is_available_employee']) && $_GET['is_available_employee'] != '') {

              $checkMagazine = new checkMagazine($this->pDbMgr);
              $checkMagazine->setMagazine(Magazine::TYPE_HIGH_STOCK_SUPPLIES);
              $checkMagazine->unsetIFNotExists(true);
              $checkMagazine->unsetIFExists(false);
              $checkMagazine->reserveItem(true);
              $checkMagazine->addSellPredict(false);
              $this->oManageLists->addFilter($checkMagazine);
              $this->oManageLists->addRequiredFilter($checkMagazine);

            $oFilter = new containerAvailableForEmployeeThreatOrderAsAll($this->pDbMgr);
            if ($_GET['is_available_employee'] == '1') {
              $oFilter->setAvailableForEmpoyee(true);
            } else {
              $oFilter->setAvailableForEmpoyee(false);
            }
            $this->oManageLists->addFilter($oFilter);
            $this->oManageLists->addRequiredFilter($oFilter);
          }

          $oFilter = new ordersItemsLimit($this->pDbMgr);
          $oFilter->setLimit(60);
          $this->oManageLists->addFilter($oFilter);
        } else {
          $oFilter = new ordersItemsLimit20($this->pDbMgr);
          $this->oManageLists->addFilter($oFilter);
        }


        /*

                  //  @TODO wykluczamy jeśli wszystkie składowe zamówienia są dostępne na Stock wysokie
          $checkMagazine = new checkMagazine($this->pDbMgr);
          $checkMagazine->setMagazine(Magazine::TYPE_HIGH_STOCK_SUPPLIES);
          if ($_GET['magazine'] == Magazine::TYPE_HIGH_STOCK_SUPPLIES) {
            $checkMagazine->unsetIFNotExists(true);
            $checkMagazine->unsetIFExists(false);
            $checkMagazine->reserveItem(true);
          } else {
            $checkMagazine->unsetIFNotExists(false);
            $checkMagazine->unsetIFExists(true);
            $checkMagazine->reserveItem(false);
          }

          $this->oManageLists->addFilter($checkMagazine);
          $this->oManageLists->addRequiredFilter($checkMagazine);


          $checkMagazine = new checkMagazine($this->pDbMgr);
          $checkMagazine->setMagazine(Magazine::TYPE_LOW_STOCK_SUPPLIES);
          if ($_GET['magazine'] == Magazine::TYPE_LOW_STOCK_SUPPLIES) {
            $checkMagazine->unsetIFNotExists(true);
            $checkMagazine->unsetIFExists(false);
            $checkMagazine->reserveItem(true);
          } else {
            $checkMagazine->unsetIFNotExists(false);
            $checkMagazine->unsetIFExists(true);
            $checkMagazine->reserveItem(false);
          }

          $this->oManageLists->addFilter($checkMagazine);
          $this->oManageLists->addRequiredFilter($checkMagazine);



          $oFilter = new ordersItemsLimit20($this->pDbMgr);
          $this->oManageLists->addFilter($oFilter);



         */





      } else {
          // sortujemy wg. popularnych pozycji
          $oFilter = new sortPopular($this->pDbMgr);
          $this->oManageLists->addFilter($oFilter);
          $this->oManageLists->addRequiredFilter($oFilter);

          $oFilter = new ordersItemsLimit45($this->pDbMgr);
          $this->oManageLists->addFilter($oFilter);
      }
  }
}
