<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_zasoby;

use Admin;
use DatabaseManager;
use LIB\orders\listType\filters;
use LIB\orders\listType\filters\ommitDouble;
use LIB\orders\listType\filters\onlyDouble;
use LIB\orders\listType\filters\typeFilter;
use LIB\orders\listType\manageGetOrdersItemsList;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj\Abstract_getListType;
use Smarty;

/**
 * Description of Module_part_linked_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_zbieranie_zasoby__stock_full extends Abstract_getListType implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected  $sMsg;
  
  /**
   *
   * @var Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  /**
   *
   * @var DatabaseManager
   */
  protected  $pDbMgr;
  
  /**
   *
   * @var manageGetOrdersItemsList 
   */
  protected $oManageLists;
  
  public function __construct(Admin $oClassRepository) {
    global $aConfig;

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    $this->oManageLists = new manageGetOrdersItemsList($this->pDbMgr);
    $this->oManageLists->setListType(array('SORTER_STOCK'));
    $this->oManageLists->setGetFromTrain(FALSE);
    $this->oManageLists->initOrderListType();
    $this->sListName = 'Pełne Zasoby';
    $this->cListType = '0';


      if (!isset($_GET['is_double'])) {
          $oFilterDuplicates = new ommitDouble($this->pDbMgr);
          $this->oManageLists->addFilter($oFilterDuplicates);
          $this->oManageLists->addRequiredFilter($oFilterDuplicates);
      }
      else {
          $oFilterDuplicates = new onlyDouble($this->pDbMgr);
          $this->oManageLists->addFilter($oFilterDuplicates);
          $this->oManageLists->addRequiredFilter($oFilterDuplicates);
      }


      if ($_GET['type'] == typeFilter::TYPE_MIXED) {

          $oFilter = new typeFilter($this->pDbMgr);
          $oFilter->setFilterCollectingType($_GET['type']);

          $this->oManageLists->addFilter($oFilter);
          $this->oManageLists->addRequiredFilter($oFilter);

          if (!isset($_GET['is_double'])) {
//              $iMaxWeight = 6.00;
//              $oFilterMax = new filters\zero_weight_max($this->pDbMgr);
//              $oFilterMax->setMaxWeight($iMaxWeight);
//              $this->oManageLists->addFilter($oFilterMax);
//              $this->oManageLists->addRequiredFilter($oFilterMax);

              $iMaxQuantity = 21;
              $oFilter = new filters\zero_quantity_max($this->pDbMgr);
              $oFilter->setMaxQuantity($iMaxQuantity);

              $this->oManageLists->addRequiredFilter($oFilter);
              $this->oManageLists->addFilter($oFilter);


              $oFilter = new filters\quantity11_14($this->pDbMgr);
              $this->oManageLists->addFilter($oFilter);
              $oFilter = new filters\quantity7_10($this->pDbMgr);
              $this->oManageLists->addFilter($oFilter);
              $orderLimit = 15;
          } else {

              // wykluczamy z listy MIX-double, które są wszystkie składwowe dostępne dostępne na wysokim składowaniu
              $detectHighLocation = new filters\detectHighLocation($this->pDbMgr);
              $detectHighLocation->unsetIFNotSH(false);
              $detectHighLocation->unsetIFSH(true);
              $detectHighLocation->setMinQuantityStock(filters\detectHighLocation::MIN_QUANTITY_HIGH_LOCATION);
              $this->oManageLists->addFilter($detectHighLocation);
              $this->oManageLists->addRequiredFilter($detectHighLocation);

              $orderLimit = 30;
          }
      } else {
          $oFilter = new typeFilter($this->pDbMgr);
          $oFilter->setFilterCollectingType($_GET['type']);

          $this->oManageLists->addFilter($oFilter);
          $this->oManageLists->addRequiredFilter($oFilter);

          if (!isset($_GET['is_double'])) {

              $iMaxWeight = 20.00;
              $oFilter = new filters\zero_weight_max($this->pDbMgr);
              $oFilter->setMaxWeight($iMaxWeight);
              $this->oManageLists->addRequiredFilter($oFilter);
              $this->oManageLists->addFilter($oFilter);


              $oFilter = new filters\ordersItemsLimit($this->pDbMgr);
              $iFilterLimit = 120;
              if (isset($_POST['orders_items_limit']) && $_POST['orders_items_limit'] > 0) {
                  $iFilterLimit = $_POST['orders_items_limit'];
              }
              $oFilter->setLimit($iFilterLimit);
//          $this->oManageLists->addRequiredFilter($oFilter);
              $this->oManageLists->addFilter($oFilter);

              $limits = [
                  1 => [13.01, 20.00, 2],
                  2 => [8.01, 13.00, 4],
                  3 => [3.01, 8.00, 6],
              ];
              foreach ($limits as $limit) {
                  $oFilter = new filters\weightLimit($this->pDbMgr);
                  $oFilter->setLimit($limit[0], $limit[1], $limit[2]);
                  $this->oManageLists->addFilter($oFilter);
              }
              $orderLimit = 15;
          } else {
              $orderLimit = 30;
          }
      }


    $oFilter = new filters\ordersLimit($this->pDbMgr);
    $oFilter->setLimit($orderLimit);
    $this->oManageLists->addFilter($oFilter);

      if (isset($_GET['is_double'])) {
          $this->cListType = '6';
      } else {
          $this->cListType = '0';
      }

  }
}
