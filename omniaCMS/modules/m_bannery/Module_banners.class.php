<?php
/**
 * Klasa Module do obslugi modulu 'System bannerowy'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */
  
class Module {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// sciezka do katalogu bazowego
	var $sBaseDir;
	
	// sciazka do katalogu dla zdjec, plikow
	var $sDir;
	
	// konfiguracja strony modulu
	var $aSettings;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;
		
		$this->sErrMsg = '';
		$this->iLangId = intval($_SESSION['lang']['id']);
		$this->sModule = $_GET['module'];
		
		$sDo = '';
		$iId = 0;
		$iPId = 0;
		
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}
		// katalog bazowy
		$this->sBaseDir = $aConfig['common']['client_base_path'].
											$aConfig['common']['file_dir'];
		// katalog na zdjecia, pliki
		$this->sDir = $this->iLangId.'/'.$iPId;

		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    // pibranie konfiguracji strony modulu
		$this->aSettings =& $this->setSettings($iPId);
		$aSett = $this->getGlobalSettings();
		$this->aSettings = array_merge($this->aSettings, $aSett);
		
		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iId); break;
			case 'edit': $this->AddEdit($pSmarty, $iPId, $iId); break;
			case 'add': $this->AddEdit($pSmarty, $iPId, $iId); break;
						
			case 'sort': 
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show(getPagePath($iPId,
																									 $this->iLangId, true),
																			 $this->getRecords($iPId),
																			 phpSelf(array('do'=>'proceed_sort'),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."banners",
												$this->getRecords($iPId),
												array(
													'page_id' => $iPId
											 	));
			break;
						
			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end Module() function
	
	
	/**
	 * Metoda pobiera i ustawia konfiguracje strony modulu
	 * 
	 * @param	integer	$iPId	- Id strony
	 */
	function &setSettings($iPId) {
		global $aConfig;
		
		// pobranie konfiguracji dla modulu
		$sSql = "SELECT banner_size
						 FROM ".$aConfig['tabls']['prefix']."banners_settings 
						 WHERE page_id = ".$iPId;
		return Common::GetRow($sSql);
	} // end of setSettings() method
	
	
	/**
	 * Metoda usuwa wybrane galerie
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony usuwanej galerii
	 * @param	integer	$iId	- Id usuwanej galerii
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			if (count($_POST['delete']) == 1) {
				$sErrPostfix = '0';
			}
			else {
				$sErrPostfix = '1';
			}
			
			Common::BeginTransaction();
			// pobranie tytulow oraz sciezek do plikow usuwanych bannerow
			$sSql = "SELECT id, name, src
					 		 FROM ".$aConfig['tabls']['prefix']."banners
						 	 WHERE page_id = ".$iPId." AND
						 	 			 id IN (".implode(',', $_POST['delete']).")";
			$aRecords =& Common::GetAll($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				$sPagePath = getPagePath($iPId, $this->iLangId, true);
				foreach ($aRecords as $aItem) {
					$sDel .= '"'.$sPagePath.' / '.$aItem['name'].'",<br>';
				}
				$sDel = substr($sDel, 0, -5);
			}

			if (!$bIsErr) {
				// usuwanie galerii
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."banners
								 WHERE page_id = ".$iPId." AND
						 	 			 	 id IN (".implode(',', $_POST['delete']).")";
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.$sErrPostfix],
												$sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
												
				// usuniecie plikow
				foreach ($aRecords as $aItem) {
					@unlink($this->sBaseDir.'/'.$aItem['src']);
				}
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_'.$sErrPostfix],
												$sDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowa galerie
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
		
		// operacja na pliku
		if (!$this->proceedFile()) {
			$bIsErr = true;
		}
		$aFile =& $_FILES['banner'];
		Common::BeginTransaction();
		
		if (!$bIsErr) {
			if (($sUrl = trim($_POST['url'])) != '') {
				if (stripos($_POST['url'], 'www.') === 0 ||
						strpos($_POST['url'], '/') === false) {
                    $sUrl = 'https://'.$_POST['url'];
				}
			}
			else {
				$sUrl = 'NULL';
			}
			$aValues = array(
				'page_id' => $iPId,
				'name' => decodeString($_POST['name']),
				'src' => $this->sDir.'/'.$aFile['final_name'],
				'filename' => $aFile['name'],
				'mime' => $aFile['type'],
				'size' => $aFile['size'],
				'url' => $sUrl,
				'flash' => $aFile['flash'],
				'new_window' => isset($_POST['new_window']) ? '1' : '0',
				'flash' => $aFile['flash'],
				'max_views' => intval($_POST['max_views']),
				'capping' => intval($_POST['capping']),
				'start_date' => $_POST['start_date'] != '' ? FormatDateHour($_POST['start_date'], $_POST['start_hour']) : 'NULL',
				'end_date' => $_POST['end_date'] != '' ? FormatDateHour($_POST['end_date'], $_POST['end_hour']) : 'NULL',
				'width' => ($_POST['width'] = trim($_POST['width'])) != '' ? intval($_POST['width']) : $aFile['width'],
				'height' => ($_POST['height'] = trim($_POST['height'])) != '' ? intval($_POST['height']) : $aFile['height'],
				'all_pages' => isset($_POST['all_pages']) ? '1' : '0',
				'logged_only' => isset($_POST['logged_only']) ? '1' : '0',
				'order_by' => '#order_by#',
				'created' => 'NOW()',
				'created_by ' => $_SESSION['user']['name']
			);
			if (($iId = Common::Insert($aConfig['tabls']['prefix']."banners",
												 $aValues)) === false) {
				$bIsErr = true;
			}
		}
		// dodawanie stron wyswietlania bannerów
		if (!$bIsErr && !empty($_POST['pages'])) {
			foreach($_POST['pages'] as $iKey => $mItem){
				// kategoria ma dzieci
				if(is_array($mItem)){
					// kategoria główna jest zaznaczona
					if($mItem['top'] == '1'){
						$aValues = array(
							'banner_id' => $iId,
							'page_id' => $iKey,
							'level' => '0'
						);	
						if(Common::Insert($aConfig['tabls']['prefix']."banners_to_pages",$aValues,'',false) === false){
							$bIsErr = true;
						}
					}
					// dzieci są zaznaczone
					if(!empty($mItem['childs'])){
						foreach($mItem['childs'] as $iCKey=>$iCItem){
							$aValues = array(
								'banner_id' => $iId,
								'page_id' => $iCKey,
								'level' => '1'
							);	
							if(Common::Insert($aConfig['tabls']['prefix']."banners_to_pages",$aValues,'',false) === false){
								$bIsErr = true;
							}
						}
					}
				} else {
				// nie ma dzieci
					$aValues = array(
						'banner_id' => $iId,
						'page_id' => $iKey,
						'level' => '0'
					);	
					if(Common::Insert($aConfig['tabls']['prefix']."banners_to_pages",$aValues,'',false) === false){
						$bIsErr = true;
					}
				}
			}
		}	
				
		if ($bIsErr) {
			Common::RollbackTransaction();
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											getPagePath($iPId, $this->iLangId, true),
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId);
		}
		else {
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											getPagePath($iPId, $this->iLangId, true),
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
							
			$this->Show($pSmarty, $iPId);
		}
	} // end of Insert() funciton
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach bannera do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$sPublishedSql = '';
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return; 
		}
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iId);
			return;
		}
		
		// operacja na pliku
		if (!$this->proceedFile(true)) {
			$bIsErr = true;
		}
		$aFile =& $_FILES['banner'];
		Common::BeginTransaction();
		if (!$bIsErr) {
			if (($sUrl = trim($_POST['url'])) != '') {
				if (stripos($_POST['url'], 'www.') === 0 ||
						strpos($_POST['url'], '/') === false) {
                    $sUrl = 'https://'.$_POST['url'];
				}
			}
			else {
				$sUrl = 'NULL';
			}
			$aValues = array(
				'name' => decodeString($_POST['name']),
				'url' => $sUrl,
				'new_window' => isset($_POST['new_window']) ? '1' : '0',
				'max_views' => intval($_POST['max_views']),
				'capping' => intval($_POST['capping']),
				'start_date' => $_POST['start_date'] != '' ? FormatDateHour($_POST['start_date'], $_POST['start_hour']) : 'NULL',
				'end_date' => $_POST['end_date'] != '' ? FormatDateHour($_POST['end_date'], $_POST['end_hour']) : 'NULL',
				'all_pages' => isset($_POST['all_pages']) ? '1' : '0',
				'logged_only' => isset($_POST['logged_only']) ? '1' : '0',
				'modified' => 'NOW()',
				'modified_by ' => $_SESSION['user']['name']
			);
			if (trim($_POST['width']) != '') {
				$aValues['width'] = intval($_POST['width']);
			}
			if (trim($_POST['height']) != '') {
				$aValues['height'] = intval($_POST['height']);
			}
			if (!empty($aFile['name'])) {
				// wybrano aktualizacje dotychczasowego bannera
				$aValues['src'] = $this->sDir.'/'.$aFile['final_name'];
				$aValues['filename'] = $aFile['name'];
				$aValues['mime'] = $aFile['type'];
				$aValues['size'] = $aFile['size'];
				$aValues['flash'] = $aFile['flash'];
				$aValues['width'] = $aFile['width'];
				$aValues['height'] = $aFile['height'];
			}
		
			if (Common::Update($aConfig['tabls']['prefix']."banners",
												 $aValues,
												 "id = ".$iId." AND
												  page_id = ".$iPId) === false) {
				$bIsErr = true;
			}
		}	
		// czyszczenie obecnych stron wyswietlania bannerów
		if (!$bIsErr) {
			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."banners_to_pages
								WHERE banner_id = ".$iId;
			if(Common::Query($sSql) === false){
				$bIsErr = true;
			}
		}
		// dodawanie stron wyswietlania bannerów
		if (!$bIsErr && !empty($_POST['pages'])) {
			foreach($_POST['pages'] as $iKey => $mItem){
				// kategoria ma dzieci
				if(is_array($mItem)){
					// kategoria główna jest zaznaczona
					if($mItem['top'] == '1'){
						$aValues = array(
							'banner_id' => $iId,
							'page_id' => $iKey,
							'level' => '0'
						);	
						if(Common::Insert($aConfig['tabls']['prefix']."banners_to_pages",$aValues,'',false) === false){
							$bIsErr = true;
						}
					}
					// dzieci są zaznaczone
					if(!empty($mItem['childs'])){
						foreach($mItem['childs'] as $iCKey=>$iCItem){
							$aValues = array(
								'banner_id' => $iId,
								'page_id' => $iCKey,
								'level' => '1'
							);	
							if(Common::Insert($aConfig['tabls']['prefix']."banners_to_pages",$aValues,'',false) === false){
								$bIsErr = true;
							}
						}
					}
				} else {
				// nie ma dzieci
					$aValues = array(
						'banner_id' => $iId,
						'page_id' => $iKey,
						'level' => '0'
					);	
					if(Common::Insert($aConfig['tabls']['prefix']."banners_to_pages",$aValues,'',false) === false){
						$bIsErr = true;
					}
				}
			}
		}	
		
		if ($bIsErr) {
			Common::RollbackTransaction();
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											getPagePath($iPId, $this->iLangId, true),
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId, $iId);
		}
		else {
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											getPagePath($iPId, $this->iLangId, true),
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			
			$this->Show($pSmarty, $iPId);
		}
	} // end of Update() funciton
	
	
	/**
	 * Metoda tworzy formularz dodawania / edycji bannera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	ID edytowanego bannera
	 */
	function AddEdit(&$pSmarty, $iPId, $iId=0) {
		global $aConfig;
		
		$aData = array();
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if ($iId > 0) {
			// pobranie z bazy danych edytowanej galerii
			$sSql = "SELECT *,
											DATE_FORMAT(start_date, '".$aConfig['common']['cal_sql_date_format']."') AS start_date, 
											DATE_FORMAT(start_date, '".$aConfig['common']['sql_hour_format']."') AS start_hour,
											DATE_FORMAT(end_date, '".$aConfig['common']['cal_sql_date_format']."') AS end_date, 
											DATE_FORMAT(end_date, '".$aConfig['common']['sql_hour_format']."') AS end_hour
							 FROM ".$aConfig['tabls']['prefix']."banners
							 WHERE id = ".$iId." AND
										 page_id = ".$iPId;
			$aData =& Common::GetRow($sSql);
			
			$sSql = "SELECT page_id, page_id
							FROM ".$aConfig['tabls']['prefix']."banners_to_pages
							 WHERE banner_id = ".$iId;
			$aPages = Common::GetAssoc($sSql);
			//dump($aPages);

		}
		if (!empty($_POST)) {
			$aData =& $_POST;
			
			$aPages = array();
			foreach($_POST['pages'] as $iKey => $mItem){
				// kategoria ma dzieci
				if(is_array($mItem)){
					// kategoria główna jest zaznaczona
					if($mItem['top'] == '1'){
						$aPages[$iKey] = $iKey;
					}
					// dzieci są zaznaczone
					if(!empty($mItem['childs'])){
						foreach($mItem['childs'] as $iCKey=>$iCItem){
							$aPages[$iCKey] = $iCKey;
						}
					}
				} else {
				// nie ma dzieci
					$aPages[$iKey] = $iKey;
				}
			}
		}
		if (!isset($aData['start_date']) || $aData['start_date'] == '') {
			$aData['start_date'] = '00-00-0000';
		}
		if (!isset($aData['end_date']) || $aData['end_date'] == '') {
			$aData['end_date'] = '00-00-0000';
		}
				
		// okreslenie nazwy strony dla ktorej dodawany / edytowany jest banner
		$sName = getPagePath($iPId, $this->iLangId, true);
		
		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['name']) && !empty($aData['name'])) {
			$sHeader .= ' "'.$aData['name'].'"';
		}
		
		$pForm = new FormTable('banners', $sHeader, array('action'=>phpSelf(array('pid' => $iPId, 'id' => $iId)), 'enctype' => 'multipart/form-data'), array('col_width'=>135), $aConfig['common']['js_validation']);
		if ($iId > 0) {
			$pForm->AddHidden('do', 'update');
			$pForm->AddHidden('src', $aData['src']);
		}
		else {
			$pForm->AddHidden('do', 'insert');
		}
		
		$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], $sName, '', array(), array('class'=>'redText'));
		
		// tytul
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>255, 'style'=>'width: 350px;'));
		
		// maksymalne rozmiary bannera
		$pForm->AddRow($aConfig['lang'][$this->sModule]['max_banner_size'], str_replace('x', ' x ', $this->aSettings['banner_size']).' px', '', array(), array('class'=>'redText'));
		
		// plik - jpg, png, gif lub swf
		$pForm->AddFile('banner', $aConfig['lang'][$this->sModule]['banner'], array('style'=>'width: 350px;'), '', $iId == 0);
		
		// wysokosc x szerokosc
		$pForm->AddRow($aConfig['lang'][$this->sModule]['width_height'],
									 $pForm->GetTextHTML('width', $aConfig['lang'][$this->sModule]['width'], $aData['width'], array('maxlength'=>4, 'style'=>'width: 50px;'), '', 'uinteger', false).' x '.$pForm->GetTextHTML('height', $aConfig['lang'][$this->sModule]['height'], $aData['height'], array('maxlength'=>3, 'style'=>'width: 50px;'), '', 'uinteger', false));
		
		// URL
		$pForm->AddText('url', $aConfig['lang'][$this->sModule]['url'], $aData['url'], array('maxlength'=>255, 'style'=>'width: 350px;'), '', 'text', false);
		
		// otwieraj w nowym oknie
		$pForm->AddCheckBox('new_window', $aConfig['lang'][$this->sModule]['new_window'], array(), '', !empty($aData['new_window']), false);
		
		// maksymalna liczba wyswietlen
		$pForm->AddText('max_views', $aConfig['lang'][$this->sModule]['max_views'], !empty($aData['max_views']) ? $aData['max_views'] : '', array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'uinteger', false);
		
		//capping
		$pForm->AddText('capping', $aConfig['lang'][$this->sModule]['capping'], $aData['capping'], array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'uinteger', true);
		
		// wyswietlaj od
		$pForm->AddRow($aConfig['lang'][$this->sModule]['start_date'],
									 '<div style="float: left;">'.$pForm->GetTextHTML('start_date', $aConfig['lang'][$this->sModule]['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date', false).'</div><div style="float: left; padding-top: 3px; padding-left: 10px;">'.$pForm->GetLabelHTML('start_hour', $aConfig['lang'][$this->sModule]['hour'], false).' '.$pForm->GetTextHTML('start_hour', $aConfig['lang'][$this->sModule]['start_hour'], $aData['start_hour'], array('style'=>'width: 50px;'), '', 'hour', false).'</div>');
		
		// wyswietlaj do
		$pForm->AddRow($aConfig['lang'][$this->sModule]['end_date'],
									 '<div style="float: left;">'.$pForm->GetTextHTML('end_date', $aConfig['lang'][$this->sModule]['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date', false).'</div><div style="float: left; padding-top: 3px; padding-left: 10px;">'.$pForm->GetLabelHTML('end_hour', $aConfig['lang'][$this->sModule]['hour'], false).' '.$pForm->GetTextHTML('end_hour', $aConfig['lang'][$this->sModule]['end_hour'], $aData['end_hour'], array('style'=>'width: 50px;'), '', 'hour', false).'</div>');
		
		
		$aMenuPages = $this->getProductOfferMenu($this->aSettings['menus']);
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['show'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		$pForm->AddCheckBox('all_pages',$aConfig['lang'][$this->sModule]['all_pages'],array('onchange'=>'changeAllPages();'),'',$aData['all_pages'],false);
		foreach($aMenuPages as $aItems){
			$pForm->AddRow('',$pForm->GetCheckBoxHTML('pages['.$aItems['value'].'][top]',$aItems['label'],array('class'=>'chpages','id'=>'pages_'.$aItems['value'],'onclick'=>'return checkAllPages();'),'',isset($aPages[$aItems['value']]),false).
												$pForm->GetLabelHTML('pages['.$aItems['value'].'][top]','<b>'.$aItems['label'].'</b>',false)
												.(!empty($aItems['childs'])?'&nbsp;&nbsp;<a href="javascript:void(0);" onclick="changeSubCatsCheckbox('.$aItems['value'].');">Zastosuj do podkategorii</a>':''));
			
			if(!empty($aItems['childs'])){
				$pForm->AddCheckboxSet('pages['.$aItems['value'].'][childs]','',$aItems['childs'],$aPages,'',false,false);
			}
			
		}
		
		/*$iCartPageId = getModulePageId('m_zamowienia','');
		$iSearchPageId = getModulePageId('m_szukaj','');
		$iAccountPageId = getModulePageId('m_konta','');
		$pForm->AddCheckBox('pages['.$iCartPageId.']',$aConfig['lang'][$this->sModule]['cart_page'],array('class'=>'chpages','id'=>'pages_'.$iCartPageId,'onclick'=>'return checkAllPages();'),'',isset($aPages[$iCartPageId]),false);
		$pForm->AddCheckBox('pages['.$iSearchPageId.']',$aConfig['lang'][$this->sModule]['search_page'],array('class'=>'chpages','id'=>'pages_'.$iSearchPageId,'onclick'=>'return checkAllPages();'),'',isset($aPages[$iSearchPageId]),false);
		$pForm->AddCheckBox('pages['.$iAccountPageId.']',$aConfig['lang'][$this->sModule]['account_page'],array('class'=>'chpages','id'=>'pages_'.$iAccountPageId,'onclick'=>'return checkAllPages();'),'',isset($aPages[$iAccountPageId]),false);
		*/
		$pForm->AddCheckBox('logged_only',$aConfig['lang'][$this->sModule]['logged_only'],array(),'',$aData['logged_only'],false);
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));
		$sJS = '
		<script type="text/javascript">
			function changeSubCatsCheckbox(iId){
				$(".pidp_"+iId).attr("checked",$("#pages_"+iId).attr("checked"));
			}
			function changeAllPages(){
				if($("#all_pages").attr("checked")){
					$(".chpages").attr("checked",true);
				}
			}
			function checkAllPages(){
				if($("#all_pages").attr("checked")){
					return false;
				} else {
					return true;
				}
			}
		</script>
		';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function


	/**
	 * Metoda wyswietla liste zdefiniowanych aktualnosci
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId) {
		global $aConfig;
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		
		// zapamietanie opcji
		rememberViewState($this->sModule);
		
		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iPId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '20'
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'current_views',
				'content'	=> $aConfig['lang'][$this->sModule]['list_current_views'],
				'sortable'	=> true,
				'width'	=> '100'
			),
			array(
				'db_field'	=> 'max_views',
				'content'	=> $aConfig['lang'][$this->sModule]['list_max_views'],
				'sortable'	=> true,
				'width'	=> '100'
			),
			array(
				'db_field'	=> 'hits',
				'content'	=> $aConfig['lang'][$this->sModule]['list_hits'],
				'sortable'	=> true,
				'width'	=> '100'
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);
		
		// pobranie liczby wszystkich galerii strony
		$sSql = "SELECT COUNT(id) 
						 FROM ".$aConfig['tabls']['prefix']."banners
						 WHERE page_id = ".$iPId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id) 
						 	 FROM ".$aConfig['tabls']['prefix']."banners
						 	 WHERE page_id = ".$iPId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		
		$pView = new View('banners', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
			// pobranie wszystkich galerii dla danego serwisu
			$sSql = "SELECT id, page_id AS pid, name,
											current_views, max_views, hits, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."banners
							 WHERE page_id = ".$iPId.
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'pid'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'pid' => '{pid}', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('pid' => '{pid}', 'id' => '{id}'),
												'delete'	=> array('pid' => '{pid}', 'id' => '{id}')
											),
					'show' => false
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('sort', 'add')
		);
		
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		);
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function
	
	
	/**
	 * Metoda pobiera liste rekordow
	 * 
	 * @param	integer	$iPId	- Id strony
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($iPId) {
		global $aConfig;
		
		// pobranie wszystkich bannerow dla danej strony
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."banners
						 WHERE page_id = ".$iPId."
						 ORDER BY order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
	

	
	/**
	 * Metoda sprawdza czy zostal przeslany plik - nastepnie przenosi go do
	 * wyznaczonego katalogu, okresla wysokosc i szerokosc pliku oraz
	 * czy przeslany plik jest zwyklym obrazkiem czy tez flashem
	 * 
	 * @return	bool	- true: sukces; false: wystapil blad
	 */
	function proceedFile($bUpdate=false) {
    global $aConfig;
    
		// nie moze byc pusta talica $_FILES w przypadku gdy
		// banner jest dodawany a nie aktualizowany
		if (empty($_FILES['banner']['name'])) return $bUpdate;
		
		if (!is_dir($this->sBaseDir.'/'.$this->sDir)) {
			// nie ma jeszcze takiego katalogu - tworzenie
			if (!$this->createDirectory()) return false;
		}
		
		if ($bUpdate) {
			// aktualizacja - usuniecie poprzedniego pliku
			@unlink($this->sBaseDir.'/'.$_POST['src']);
		}
		
		if (stripos($_FILES['banner']['name'], '.swf') !== false) {
			// plik swf
			// dolaczeie klasy File - do zarzadzani plikami
			include_once('File.class.php');
			$oFile = new MyFile();
			$oFile->setFile($this->sBaseDir.'/'.$this->sDir, $_FILES['banner']);
			if ($oFile->proceedFile(array('gif', 'jpg', 'jpeg', 'png', 'swf'), false, true) != 1) {
        return false;
      }
			// okreslenie czy jest to flash czy tez zwykly banner
			$_FILES['banner']['flash'] = '1';
			// okreslenie rozmiaru bannera
			$this->getDimensions();
		}
		else {
			// plik graficzny - jpg, gif lub png
			// dolaczeie klasy Image - do zarzadzani grafikami
			include_once('Image.class.php');
			$oFile = new Image();
			$aSettings['small_size'] = $this->aSettings['banner_size'];
			$oFile->setImage($this->sBaseDir.'/'.$this->sDir, $_FILES['banner'], $aSettings);
			if ($oFile->proceedImage(false) != 1) {
        return false;
      }
			// okreslenie czy jest to flash czy tez zwykly banner
			$_FILES['banner']['flash'] = '0';
			// okreslenie rozmiaru bannera
			$this->getDimensions();
		}
    /*
    if ($aConfig['common']['status'] != 'development') {
      if(!empty($this->aSettings['banners_dir'])){
        if (file_exists($this->aSettings['banners_dir'].'/'.$_FILES['banner']['name'])) {
          @unlink($this->aSettings['banners_dir'].'/'.$_FILES['banner']['name']);
        }

        if (!@copy($_FILES['banner']['tmp_name'], $this->aSettings['banners_dir'].'/'.$_FILES['banner']['name'])) {
          return false;
        }
      }
    }
     */
		return true;
	} // end of proceedFile() method
	
	
	/**
	 * Metoda okresla rozmiar bannera
	 * 
	 * @return	void
	 */
	function getDimensions() {
		if ($_FILES['banner']['flash'] == '1') {
			// jest to flash
			// dolaczenie klay do czytania plikow SWF
			include_once('File/SWF.php');
			$oFlash = new File_SWF($this->sBaseDir.'/'.$this->sDir.'/'.$_FILES['banner']['final_name']);
 			if($oFlash->is_valid()) {
				$aSize = $oFlash->getMovieSize();
				$_FILES['banner']['width'] = intval($aSize['width']);
				$_FILES['banner']['height'] = intval($aSize['height']);
			}
		}
		else {
			// zwykly obraz - uzycie funkcji getimagesize()
			if ($aSize = @getimagesize($this->sBaseDir.'/'.$this->sDir.'/'.$_FILES['banner']['final_name'])) {
				$_FILES['banner']['width'] = intval($aSize['0']);
				$_FILES['banner']['height'] = intval($aSize['1']);
			}
		}
		return true;
	} // end of createDirectory() method
	
	
	/**
	 * Metoda tworzy katalog(i)
	 * 
	 * @return	bool	- true: utworzono; false: wystapil blad
	 */
	function createDirectory() {
		global $aConfig;
		
		$aDirs = explode('/', $this->sDir);
		foreach ($aDirs as $iKey => $sDir) {
			$sPath .= '/'.$sDir;
			if (!is_dir($this->sBaseDir.$sPath)) {
				if (!mkdir($this->sBaseDir.$sPath,
									 $aConfig['default']['new_dir_mode'])) {
					return false;
				};
			}
		}
		return true;
	} // end of createDirectory() method
	
	
	function getNode($iParent=0,$sMenus=''){
		global $aConfig;
		$sSql = "SELECT A.id AS value, A.name AS label, CONCAT('chpages pidp_',IFNULL(A.parent_id,0)) AS class,
						'return checkAllPages();' as onclick
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."menus B
						ON B.id = A.menu_id
						WHERE A.mtype = '0'".
						(!empty($sMenus)?" AND A.menu_id IN (".$sMenus.")":" AND 1=0").
						($iParent==0?" AND A.parent_id IS NULL":" AND A.parent_id = ".$iParent); //dump($sSql);
		return Common::GetAll($sSql);
		
	}
	
	function getProductOfferMenu($sMenus){
		global $aConfig;
		$aTop = $this->getNode(0,$sMenus);
		foreach($aTop as $iKey=>$aItem){
			$aTop[$iKey]['childs'] = $this->getNode($aItem['value'],$sMenus);
			if(empty($aTop[$iKey]['childs'])){
				unset($aTop[$iKey]['childs']);
			}
		} //dump($aTop);
		return $aTop;
	}
	
		/**
	 * Metoda pobiera ustawienia dla modulu
	 */
	function &getGlobalSettings() {
		global $aConfig;
		// pobranie konfiguracji dla modulu
		$sSql = "SELECT menus, banners_dir
						 FROM ".$aConfig['tabls']['prefix']."banners_config 
						 WHERE id = ".getModuleId('m_bannery');
		return Common::GetRow($sSql);
	} // end of setSettings() method
} // end of Module Class
?>