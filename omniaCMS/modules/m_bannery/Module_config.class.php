<?php
/**
 * Klasa Module do obslugi domyslnej konfiguracji stron modulu 'Bannery'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright Omnia Marcin Korecki
 * @version   1.0
 *
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// sciezka do katalogu zawierajacego szablony czesci klienckiej modulu
	var $sTemplatesDir;

	// lista szablonow czesci klienckiej modulu
	var $sTemplatesList;

	// plik XML z domyslna konfiguracja
	var $sXMLFile;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->sModule = $_GET['module'];
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/modules/'.$this->sModule;
		//$this->sTemplatesList =& GetTemplatesList($this->sTemplatesDir);
		$this->sXMLFile = 'config/'.$_GET['module'].'/default_config.xml';

		$sDo = '';
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu i akcji
		if ($_SESSION['user']['type'] == 0) {
			// brak uprawnien do modulu, menu, strony
			showPrivsAlert($pSmarty);
			return;
    }

    switch ($sDo) {
			case 'update': $this->Update($pSmarty); break;
			default: $this->Edit($pSmarty); break;
		}
	} // end Module() function


	/**
	 * Metoda wprowadza zmiany w aktualnosci do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Edit($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Edit($pSmarty);
			return;
		}
		
		$sMenus='';
		//dump($_POST['menus']);
		if(!empty($_POST['menus'])){
			foreach($_POST['menus'] as $iKey=>$iVal){
				$sMenus .= $iKey.',';
			}
		}
		//dump($sMenus);
		if(!empty($sMenus)){
			$sMenus = substr($sMenus,0,-1);
		}
		$_POST['menus'] = $sMenus;
		
	$aValues = array();
		$iOPId = getModuleId('m_bannery');
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."banners_config
						 WHERE id = ".$iOPId;
		$aSettings =& Common::GetRow($sSql);
		
		$aValues = array(
			'menus' => $sMenus,
			'banners_dir' => $_POST['banners_dir']
		);

		if (!empty($aSettings)) {
			// Update
			$bIsErr = ! Common::Update($aConfig['tabls']['prefix']."banners_config",
														$aValues,
														"id = ".$iOPId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('id' => $iOPId), $aValues);
			$bIsErr = ! Common::Insert($aConfig['tabls']['prefix']."banners_config",
														$aValues,
														"",
														false) !== false;
		}

		$bIsErr = !writeModuleConfig($this->sXMLFile);

		if ($bIsErr) {
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = $aConfig['lang'][$this->sModule]['edit_err'];
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->Edit($pSmarty);
		}
		else {
			$sMsg = $aConfig['lang'][$this->sModule]['edit_ok'];
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Edit($pSmarty);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz edycji domyslnej konfiguracji stron modulu
	 *
	 * @param		object	$pSmarty
	 */
	function Edit(&$pSmarty) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');


	
			// pobranie konfiguracji z pliku XML
			$aData =& getModuleConfig($this->sXMLFile);
			
			
			//dump($aData['menus']);
			$aMenus = explode(',',$aData['menus']);
			//dump($aData['menus']);
			$aData['menus'] = array();
			if(!empty($aMenus)){
				foreach($aMenus as $iMId) {
					$aData['menus'][$iMId] = $iMId;
				}
			}
			//dump($aData['menus']);


		$pForm = new FormTable('config',
													 $aConfig['lang'][$this->sModule]['header'],
													 array('action'=>phpSelf()),
													 array('col_width'=>225),
													 $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');

		$aLang =& $aConfig['lang'][$_GET['module'].'_config'];

		$pForm->AddText('banners_dir', $aLang['banners_dir'], $aData['banners_dir'], array('maxlength'=>255, 'style'=>'width: 300px;'), '', 'text');
		//$pForm->AddSelect('menu_id', $aLang['menu_id'], array(), $this->getMenus(), $aData['menu_id'], '', true);
		
		$pForm->AddCheckboxSet('menus',$aLang['menus'],$this->getMenus(),$aData['menus'],'',false,false);
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Edit() function
	
	function getMenus(){
		global $aConfig;
		$sSql = "SELECT id AS value, name AS label
						 FROM ".$aConfig['tabls']['prefix']."menus
						ORDER BY order_by";
		return Common::GetAll($sSql);
	}
} // end of Module Class
?>