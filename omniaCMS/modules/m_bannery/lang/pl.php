<?php
/**
* Plik jezykowy modulu 'Galeria'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/

$aConfig['lang']['mod_m_galeria']['no_data']	= 'W chwili obecnej nie posiadamy żadnych informacji.';
$aConfig['lang']['mod_m_galeria']['more'] = "więcej";
$aConfig['lang']['mod_m_galeria']['page'] = "Strona:";
$aConfig['lang']['mod_m_galeria']['pager_total_items']	= 'wszystkich galerii:';

?>