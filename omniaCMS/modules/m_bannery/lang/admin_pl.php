<?php
/**
* Plik jezykowy dla interfejsu modulu 'System bannerowy' Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*---------- lista stron systemu bannerowego */
$aConfig['lang']['m_bannery']['list'] = "Lista stron systemu bannerowego";
$aConfig['lang']['m_bannery']['no_items'] = "Brak zdefiniowanych stron systemu bannerowego";

/*---------- bannery */
$aConfig['lang']['m_bannery_banners']['list'] = "Lista bannerów strony: \"%s\"";
$aConfig['lang']['m_bannery_banners']['name'] = "Tytuł";
$aConfig['lang']['m_bannery_banners']['list_current_views'] = "Aktual. wyśw.";
$aConfig['lang']['m_bannery_banners']['list_max_views'] = "Max. wyśw.";
$aConfig['lang']['m_bannery_banners']['list_hits'] = "Kliknięć";
$aConfig['lang']['m_bannery_banners']['no_items'] = "Brak zdefiniowanych bannerów";
$aConfig['lang']['m_bannery_banners']['edit'] = "Edytuj banner";
$aConfig['lang']['m_bannery_banners']['delete'] = "Usuń banner";
$aConfig['lang']['m_bannery_banners']['delete_q'] = "Czy na pewno usunąć wybrany banner?";
$aConfig['lang']['m_bannery_banners']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_bannery_banners']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_bannery_banners']['delete_all_q'] = "Czy na pewno usunąć wybrane bannery?";
$aConfig['lang']['m_bannery_banners']['delete_all_err'] = "Nie wybrano żadnego bannera do usunięcia!";
$aConfig['lang']['m_bannery_banners']['go_back'] = "powrót do listy stron";
$aConfig['lang']['m_bannery_banners']['sort'] = "Sortuj bannery";
$aConfig['lang']['m_bannery_banners']['add'] = "Dodaj banner";

$aConfig['lang']['m_bannery_banners']['header_0'] = "Dodawanie bannera";
$aConfig['lang']['m_bannery_banners']['header_1'] = "Edycja bannera";
$aConfig['lang']['m_bannery_banners']['for'] = "Dla strony";
$aConfig['lang']['m_bannery_banners']['max_banner_size'] = "Maks. rozmiar bannera";
$aConfig['lang']['m_bannery_banners']['name'] = "Tytuł";
$aConfig['lang']['m_bannery_banners']['banner'] = "Banner";
$aConfig['lang']['m_bannery_banners']['url'] = "Adres URL bannera";
$aConfig['lang']['m_bannery_banners']['new_window'] = "Otwieraj w nowym oknie";
$aConfig['lang']['m_bannery_banners']['width_height'] = "Szerokość x wysokość";
$aConfig['lang']['m_bannery_banners']['max_views'] = "Maksymalna liczba wyświetleń";
$aConfig['lang']['m_bannery_banners']['start_date'] = "Wyświetlaj od";
$aConfig['lang']['m_bannery_banners']['end_date'] = "Wyświetlaj do";
$aConfig['lang']['m_bannery_banners']['hour'] = "godz.";
$aConfig['lang']['m_bannery_banners']['capping'] = "Capping";

$aConfig['lang']['m_bannery_banners']['show'] = "Wyświetlaj";
$aConfig['lang']['m_bannery_banners']['all_pages'] = "W całym serwisie";
$aConfig['lang']['m_bannery_banners']['search_page'] = "Wyszukiwarka";
$aConfig['lang']['m_bannery_banners']['cart_page'] = "Koszyk";
$aConfig['lang']['m_bannery_banners']['account_page'] = "Konto użytkownika";
$aConfig['lang']['m_bannery_banners']['logged_only'] = "Tylko dla zalogowanych";

$aConfig['lang']['m_bannery_banners']['add_ok'] = "Dodano banner \"%s / %s\"";
$aConfig['lang']['m_bannery_banners']['add_err'] = "Wystąpił błąd podczas dodawania bannera \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_bannery_banners']['edit_ok'] = "Zmieniono banner \"%s / %s\"";
$aConfig['lang']['m_bannery_banners']['edit_err'] = "Wystąpił błąd podczas próby zmiany bannera \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_bannery_banners']['del_ok_0']		= "Usunieto banner %s";
$aConfig['lang']['m_bannery_banners']['del_ok_1']		= "Usunieto bannery:<br>%s";
$aConfig['lang']['m_bannery_banners']['del_err_0']		= "Wystąpił błąd podczas próby usunięcia bannera %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_bannery_banners']['del_err_1']		= "Wystąpił błąd podczas próby usunięcia bannerów:<br>%s!<br>Spróbuj ponownie";

$aConfig['lang']['m_bannery_banners']['sort_items'] = "Sortowanie bannerów strony: \"%s\"";

/*---------- ustawienia bannerow */
$aConfig['lang']['m_bannery_settings']['banner_size'] = "Max. rozmiar bannerów";

/*---------- ustawienia boksu systemu bannerowego */
$aConfig['lang']['m_bannery_box_settings']['page_id'] = "Strona systemu bannerowego";
$aConfig['lang']['m_bannery_box_settings']['items_in_box'] = "Bannerów w boksie";

/*---------- ustawienia oferta produktowa */
$aConfig['lang']['m_bannery_config']['header'] = "Konfiguracja domyślna stron modułu \"System bannerowy\"";
$aConfig['lang']['m_bannery_config']['edit_err'] = "Wystąpił błąd podczas aktualizacji domyślnej konfiguracji modułu \"System bannerowy\"";
$aConfig['lang']['m_bannery_config']['edit_ok'] = "Zaktualizowano domyślną konfigurację modułu \"System bannerowy\"";
$aConfig['lang']['m_bannery_config']['menus'] = "Menu systemu bannerowego";
$aConfig['lang']['m_bannery_config']['banners_dir'] = "Folder do którego kopiowane będą bannery";

?>