<?php
/**
 * Klasa Module do obslugi konfiguracji stron modulu 'System Bannery'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */
 
class Settings {
  
  // nazwa modulu - do langow
	var $sModule;
	
	// Id kategorii dla ktorej edytowane sa ustawienia
	var $iId;
	
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$sModule, &$iId) {
		global $aConfig;
		$this->sModule = $sModule;
		$this->iId = $iId;
		
		// pobranie konfiguracji dla modulu
		$this->setSettings();
	} // end Settings() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla strony modulu lub w przypadku
	 * braku konfiguracji (tworzenie nowej strony) domyslna konfiguracje
	 * z pliku XML
	 * 
	 * @return	void
	 */
	function setSettings() {
		global $aConfig;
		$aCfg = array();
		
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."banners_settings 
						 WHERE page_id = ".$this->iId;
		$aConfig['settings'][$this->sModule] =& Common::GetRow($sSql);
	} // end of setSettings() method
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach aktualnosci
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;
		
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."banners_settings
						 WHERE page_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);
		
		if (!empty($aSettings)) {
			// update
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."banners_settings SET
								 			banner_size = '".$_POST['banner_size']."'
							 WHERE page_id = ".$this->iId;
			if (Common::Query($sSql) === false) {
				return false;
			}
		}
		else {
			// nie zaktualizowano ustawien - nie ma ich jeszcze w bazie danych
			// insert
			$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."banners_settings (
												page_id,
								 				banner_size
								 )
								 VALUES (
								 				".$this->iId.",
								 				'".$_POST['banner_size']."'
								 )";
			if (Common::Query($sSql) === false) {
				return false;
			}
		}
		return true;
	} // end of UpdateSettings() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji konfiguracji strony modulu
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;
		
		$aData = array();
		$sHtml = '';
		
		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_mtype') {
			$aData =& $_POST;
		}
		
		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['settings'], array('class'=>'merged', 'style'=>'padding-left: 208px'));
		
		// rozmiary bannera
		$pForm->AddText('banner_size', $aConfig['lang'][$this->sModule.'_settings']['banner_size'], $aData['banner_size'], array('maxlength'=>7, 'style'=>'width: 60px;'), '', 'text', true, '/^[0-9]{2,3}x[0-9]{2,3}$/');
	} // end of SettingsForm() function
} // end of Settings Class
?>