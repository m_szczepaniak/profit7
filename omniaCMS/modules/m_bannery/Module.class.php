<?php
/**
 * Klasa Module do obslugi modulu 'System bannerowy'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2007
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$this->aPrivileges =& $_SESSION['user']['privileges'];

		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}


	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			default: $this->Show($pSmarty); break;
		}
	} // end Module() function


	/**
	 * Metoda wyswietla liste zdefiniowanych dla serwisu stron opisowych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		$sPages = '';

		// dolaczenie klasy TreeView
		include_once('TreeView/TreeView.class.php');

		// zapamietanie opcji
		rememberTreeViewState($this->sModule);

		// okreslenie listy menu w ktorych istnieja strony aktualnosci i do ktorych
		$sSql = "SELECT DISTINCT A.id, A.name
						 FROM ".$aConfig['tabls']['prefix']."menus AS A
						 			JOIN ".$aConfig['tabls']['prefix']."menus_items AS B
						 			ON B.language_id = A.language_id AND
						 				 B.menu_id = A.id
						 WHERE A.language_id = ".$this->iLangId." AND
						 			 B.module_id = ".$_GET['module_id']." AND
						 			 B.moption IS NULL
						 ORDER BY A.id";
		$aMenus =& Common::GetAll($sSql);

		foreach ($aMenus as $iMKey => $aMenu) {
			// pobranie z bazy danych listy wszystkich stron menu
			$sSql = "SELECT IFNULL(A.parent_id, 0), A.id, A.name, A.mtype, A.menu_id, A.module_id, A.symbol, IFNULL(A.link_to_id, 0) AS link_to_id, A.url, A.menu, A.moption
							 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
							 			LEFT JOIN ".$aConfig['tabls']['prefix']."modules AS B
							 			ON B.id = A.module_id
							 WHERE A.menu_id = ".$aMenu['id']." AND
							 			 A.language_id = ".$this->iLangId."
							 ORDER BY A.parent_id, A.order_by";
			$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);

			foreach ($aItems as $iParent => $aPages) {
				foreach ($aPages as $iKey => $aItem) {
					$aItems[$iParent][$iKey]['disabled'] = array();
					if (!($aItem['module_id'] == $_GET['module_id'] &&
							empty($aItem['moption']))) {
						// wylaczenie kolumny z przyciskami
						$aItems[$iParent][$iKey]['show_actions'] = 0;
						$aItems[$iParent][$iKey]['hover'] = 0;
						$aItems[$iParent][$iKey]['disabled'][] = 'link_name';
					}
				}
			}
			// utworzenie drzewa stron
			$aMenu['menu'] = '1';
			$aMenu['show_actions'] = 0;
			$aMenu['hover'] = 0;
			$aMenu['class'] = 'menu';
			$aMenu['id'] = 'm'.$aMenu['id'];
			$aRecords[$iMKey][0] = $aMenu;
			$aRecords[$iMKey][0]['children'] = getModuleMenuTree($aItems, 0,  $_GET['module_id'], array());
		}

		// ustawienia dostepnych akcji dla rekordow
		$aActions = array (
			'actions'	=> array (),
			'params' => array (),
			'lang' => array (
					'preview' => $aConfig['lang'][$this->sModule]['preview']
			),
			'links'	=> array(
				'name' => phpSelf(array('action'=>'banners', 'pid'=>'{id}'))
			)
		);
		$pTreeView = new TreeView($aConfig['lang'][$this->sModule]['list'],
															$aConfig['lang'][$this->sModule]['no_items'],
															$aConfig['common']['mod_rewrite']);

		// dodanie rekordow do widoku
		$pTreeView->AddRecords($aRecords,
													 $aActions,
													 $_SESSION['_treeViewState'][$this->sModule]['expanded'],
													 true);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pTreeView->Show());
	} // end of Show() function
} // end of Module Class
?>