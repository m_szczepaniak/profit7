<?php
/**
* Plik jezykowy dla interfejsu modulu 'Opisy' Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*---------- lista stron opisowych */
$aConfig['lang']['m_opisy']['list'] = "Lista stron opisowych";
$aConfig['lang']['m_opisy']['preview'] = "Podgląd";
$aConfig['lang']['m_opisy']['no_items'] = "Brak zdefiniowanych stron opisowych";

/*---------- akapity stron opisowych */
$aConfig['lang']['m_opisy_paragraphs']['list'] = "Lista akapitów strony: \"%s\"";
$aConfig['lang']['m_opisy_paragraphs']['list_name'] = "Tytuł";
$aConfig['lang']['m_opisy_paragraphs']['list_published'] = "Opublikowany";
$aConfig['lang']['m_opisy_paragraphs']['paragraph_number'] = "Akapit %d";
$aConfig['lang']['m_opisy_paragraphs']['paragraph_name'] = "Akapit %d: %s";
$aConfig['lang']['m_opisy_paragraphs']['no_items'] = "Brak zdefiniowanych paragrafów strony opisowej";

$aConfig['lang']['m_opisy_paragraphs']['files'] = "Załączniki akapit";
$aConfig['lang']['m_opisy_paragraphs']['images'] = "Zdjęcia akapit";
$aConfig['lang']['m_opisy_paragraphs']['publish'] = "Zmień stan publikacji akapitu";
$aConfig['lang']['m_opisy_paragraphs']['edit'] = "Edytuj akapit";
$aConfig['lang']['m_opisy_paragraphs']['delete'] = "Usuń akapit";
$aConfig['lang']['m_opisy_paragraphs']['delete_q'] = "Czy na pewno usunąć wybrany akapit?";
$aConfig['lang']['m_opisy_paragraphs']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_opisy_paragraphs']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_opisy_paragraphs']['delete_all_q'] = "Czy na pewno usunąć wybrane akapity?";
$aConfig['lang']['m_opisy_paragraphs']['delete_all_err'] = "Nie wybrano żadnego akapitu do usunięcia!";
$aConfig['lang']['m_opisy_paragraphs']['add'] = "Dodaj akapit";
$aConfig['lang']['m_opisy_paragraphs']['del_ok_0']		= "Usunięto akapit %s strony \"%s\"";
$aConfig['lang']['m_opisy_paragraphs']['del_ok_1']		= "Usunięto akapity:<br>%s<br>strony \"%s\"";
$aConfig['lang']['m_opisy_paragraphs']['del_err_0']		= "Wystąpił błąd podczas próby usunięcia akapitu %s strony \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_opisy_paragraphs']['del_err_1']		= "Wystąpił błąd podczas próby usunięcia akapitów:<br>%s<br>strony \"%s\"!<br>Spróbuj ponownie";

$aConfig['lang']['m_opisy_paragraphs']['sort'] = "Sortuj akapity strony";
$aConfig['lang']['m_opisy_paragraphs']['sort_items'] = "Sortowanie akapitów strony: \"%s\"";

$aConfig['lang']['m_opisy_paragraphs']['go_back'] = "Powrót do listy stron";

$aConfig['lang']['m_opisy_paragraphs']['header_0'] = "Dodawanie akapitu";
$aConfig['lang']['m_opisy_paragraphs']['header_1'] = "Edycja akapitu";
$aConfig['lang']['m_opisy_paragraphs']['for'] = "Dla strony";
$aConfig['lang']['m_opisy_paragraphs']['position'] = "Dodaj";
$aConfig['lang']['m_opisy_paragraphs']['after'] = "Po: ";
$aConfig['lang']['m_opisy_paragraphs']['end'] = "Na końcu";
$aConfig['lang']['m_opisy_paragraphs']['beginning'] = "Na początku";
$aConfig['lang']['m_opisy_paragraphs']['name'] = "Tytuł";
$aConfig['lang']['m_opisy_paragraphs']['content'] = "Treść";
$aConfig['lang']['m_opisy_paragraphs']['published'] = "Opublikowany";

$aConfig['lang']['m_opisy_paragraphs']['add_ok'] = "Dodano akapit \"%s\" dla strony \"%s\"";
$aConfig['lang']['m_opisy_paragraphs']['add_err'] = "Wystąpił błąd podczas próby dodania akapitu \"%s\" dla strony \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_opisy_paragraphs']['edit_ok'] = "Zmieniono akapit \"%s\" strony \"%s\"";
$aConfig['lang']['m_opisy_paragraphs']['edit_err'] = "Wystąpił błąd podczas próby zmiany akapitu \"%s\" strony \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_opisy_paragraphs']['published_0'] = "nieopublikowany";
$aConfig['lang']['m_opisy_paragraphs']['published_1'] = "opublikowany";
$aConfig['lang']['m_opisy_paragraphs']['publ_ok']		= "Zmieniono stan publikacji akapitu \"%s\" strony \"%s\" na \"%s\"";
$aConfig['lang']['m_opisy_paragraphs']['publ_err']		= "Wystąpił błąd podczas próby zmiany stanu publikacji akapitu \"%s\" strony \"%s\" na \"%s\"!<br><br>Spróbuj ponownie";

/*---------- ustawienia stron opisowych */
$aConfig['lang']['m_opisy_config']['header'] = "Konfiguracja domyślna stron modułu \"".$aConfig['lang']['c_modules']['m_opisy']."\"";
$aConfig['lang']['m_opisy_config']['edit_err'] = "Wystąpił błąd podczas aktualizacji domyślnej konfiguracji modułu \"".$aConfig['lang']['c_modules']['m_opisy']."\"";
$aConfig['lang']['m_opisy_config']['edit_ok'] = "Zaktualizowano domyślną konfigurację modułu \"".$aConfig['lang']['c_modules']['m_opisy']."\"";

$aConfig['lang']['m_opisy_settings']['template'] = "Szablon strony";
$aConfig['lang']['m_opisy_settings']['photos'] = "Zdjęcia";
$aConfig['lang']['m_opisy_settings']['thumb_size'] = "Rozmiar miniaturki zdjęcia";
$aConfig['lang']['m_opisy_settings']['small_size'] = "Rozmiar małego zdjęcia";
$aConfig['lang']['m_opisy_settings']['big_size'] = "Rozmiar dużego zdjęcia";
$aConfig['lang']['m_opisy_settings']['photo_desc_shortcut'] = "Liczba znaków w skrócie opisu zdjęć (0 - 255)";

/*---------- ustawienia boksu stron opisowych */
$aConfig['lang']['m_opisy_box_settings']['page'] = "Strona opisowa";

/*---------- zdjecia akapitu */
$aConfig['lang']['m_opisy_paragraphs']['images_header'] = "Zdjęcia akapitu: \"%s\" strony \"%s\"";
$aConfig['lang']['m_opisy_paragraphs']['img_align'] = "Wyrównanie";
$aConfig['lang']['m_opisy_paragraphs']['align_top'] = "nad tekstem";
$aConfig['lang']['m_opisy_paragraphs']['align_left'] = "do lewej";
$aConfig['lang']['m_opisy_paragraphs']['align_right'] = "do prawej";
$aConfig['lang']['m_opisy_paragraphs']['align_bottom'] = "pod tekstem";
$aConfig['lang']['m_opisy_paragraphs']['image'] = "Zdjęcie";
$aConfig['lang']['m_opisy_paragraphs']['edit_img_ok'] = "Zmieniono ustawienia zdjęć akapitu \"%s\" dla strony \"%s\"";
$aConfig['lang']['m_opisy_paragraphs']['edit_img_err'] = "Nie udało się zmienić ustawień zdjęć akapitu \"%s\" dla strony \"%s\"!<br><br>Spróbuj ponownie";

/*---------- pliki paragrafu */
$aConfig['lang']['m_opisy_paragraphs']['files_header'] = "Załączniki akapitu: \"%s\" strony \"%s\"";
$aConfig['lang']['m_opisy_paragraphs']['file'] = "Plik";
$aConfig['lang']['m_opisy_paragraphs']['edit_file_ok'] = "Zmieniono ustawienia załączników akapitu \"%s\" dla strony \"%s\"";
$aConfig['lang']['m_opisy_paragraphs']['edit_file_err'] = "Nie udało się zmienić ustawień załączników akapitu \"%s\" dla strony \"%s\"!<br><br>Spróbuj ponownie";
?>
