<?php
/**
* Plik jezykowy modulu 'STRONY OPISOWE'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/
$aConfig['lang']['mod_m_opisy']['zalaczniki'] = 'Załączniki: ';
$aConfig['lang']['mod_m_opisy']['no_data']	= 'W chwili obecnej nie posiadamy żadnych informacji.';
$aConfig['lang']['mod_m_opisy']['files'] = "Pliki do pobrania";
$aConfig['lang']['mod_m_opisy']['fot'] = "Fot.";
?>