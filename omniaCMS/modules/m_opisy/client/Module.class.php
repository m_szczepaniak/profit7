<?php
/**
 * Klasa odule do obslugi stron opisowych
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczene wspolnej klasy modulu Strony opisowe
include_once('modules/m_opisy/client/Common.class.php');

class Module extends Common_m_opisy {

  // ustawienia strony aktualnosci
  var $aSettings;
  
  // czesc nazwy pliku cache - na podstawie sciezki URI
  var $sCacheName;
    
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;
		
		$this->sCacheName = md5($_SERVER['REQUEST_URI']);
		$this->aSettings =& $this->getSettings($aConfig);
		// konstruktor klas Common
		parent::Common_m_opisy($aConfig['_tmp']['page']['id'],
																 '/'.$aConfig['_tmp']['page']['symbol'],
																 $aConfig['_tmp']['page']['module'],
																 'modules/'.$aConfig['_tmp']['page']['module'],
																 $this->aSettings['template']);
	} // end Module() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla strony opisowej
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aCfg = unserialize($oCache->get(md5($aConfig['_tmp']['page']['symbol']), 'modules_settings')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT * 
							 FROM ".$aConfig['tabls']['prefix']."texts_settings 
							 WHERE page_id = ".$aConfig['_tmp']['page']['id'];
			$aCfg = Common::GetRow($sSql);
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aCfg), md5($aConfig['_tmp']['page']['symbol']), 'modules_settings');
		}
		return $aCfg;
	} // end of getSettings() function
	
	
	/**
	 * Metoda pobiera dane oraz akapity strony
	 * dowiazuje ja do Smartow oraz zwraca xHTML z trescia strony po renderowaniu
	 * szablonu
	 */
	function getContent() {
		global $aConfig, $pSmarty, $oCache;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		if (!$aConfig['common']['caching'] || false === ($sHtml = $oCache->get($this->sCacheName, 'modules'))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie i przetworzenie przez szablon listy paragrafow
			$aModule['page_link'] = $this->sPageLink;
			
			// pobranie danych strony i jej paragrafow
			$aModule = array_merge($aModule, $this->getItems((int) $this->aSettings['photo_desc_shortcut']));
	
			if (!empty($aModule['items'])) {
				$pSmarty->assign_by_ref('aModule', $aModule);
				$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
				$pSmarty->clear_assign('aModule', $aModule);
			}
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save($sHtml, $this->sCacheName, 'modules');
		}
		if (trim($sHtml) == '')
			setMessage($aModule['lang']['no_data']);
		return $sHtml;
	} // end of getContent()
} // end of Module Class
?>