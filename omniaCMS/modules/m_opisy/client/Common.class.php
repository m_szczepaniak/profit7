<?php
/**
 * Klasa wspolna dla modulu oraz boksu Strony opisowe
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2007
 * @version   1.0
 */
 
class Common_m_opisy {
  
  // Id strony
  var $iPageId;
  
	// sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon listy
  var $sTemplate;
  
  // szablon elementu
  var $sTemplate2;
  
  // nazwa modulu
  var $sModule;
  
  // link do strony
  var $sPageLink;
    
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Common_m_opisy($iPId, $sPageLink, $sModule, $sTplsPath, $sTpl, $sTpl2='') {
		$this->iPageId = $iPId;
		$this->sModule = $sModule;
		$this->sTemplatesPath = $sTplsPath;
		$this->sTemplate = $sTpl;
		$this->sTemplate2 = $sTpl2;
		$this->sPageLink = $sPageLink;
	} // end Common_m_opisy() function

	/**
	 * @return bool
	 */
	private function userLoggedAccess() {
		global $pDbMgr;

		if (isset ($_SESSION['user']) && $_SESSION['user']['id'] > 0) {
			$sSql = 'SELECT id
					 FROM users
					 WHERE active = "1" AND id = '.$_SESSION['user']['id'];
			return ($pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
		}
		return false;
	}


	/**
	 * Metoda pobiera dane i paragrafy strony opisowej
	 * 
	 * @param	integer	$iTrimTo	- pzytnij opis zdjec do podanej liczby znakow
	 * @param	bool	$bAllPhotos	- true: wszystkie zdjecia dla akapitow (male + duze)
	 * 														false: pierwsza miniaturka dla kazdego akapitu
	 * @return	array ref	- dane i paragrafy strony
	 */
	function &getItems($iTrimTo=0, $bAllPhotos=true, $bFiles=true) {
		global $aConfig, $pSmarty;
		// dane jezykowe
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		
		// okreslenie uzytkownika tworzacego i modyfikujacego strone
		$sSql = "SELECT DATE_FORMAT(A.created, '".$aConfig['common']['sql_date_hour_format']."') AS created, A.created_by,
										B.name AS creator_name, B.surname AS creator_surname,
										DATE_FORMAT(A.modified, '".$aConfig['common']['sql_date_hour_format']."') AS modified, A.modified_by,
										C.name AS modifier_name, C.surname AS modifier_surname
						 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."users AS B
						 ON B.login = A.created_by
						 LEFT JOIN ".$aConfig['tabls']['prefix']."users AS C
						 ON C.login = A.created_by
						 WHERE A.id = ".$this->iPageId;
		$aData =& Common::GetRow($sSql);
		
		// pobranie listy paragrafow strony
		$sSql = "SELECT id, name, content, img_align, published
						 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
						 WHERE page_id = ".$this->iPageId.
						 			 (isPreviewMode() === true && $this->userLoggedAccess() === true ? '' : " AND published = '1'")."
						 ORDER BY number";
		$aData['items'] =& Common::GetAll($sSql);
		
		
		$pSmarty->assign_by_ref('aModule', $aModule);
		foreach ($aData['items'] as $iKey => $aItem) {
			// pobranie zdjec akapitu
			if ($bAllPhotos) {
				$aData['items'][$iKey]['images'] = getItemImages($aConfig['tabls']['prefix']."texts_images", array('paragraph_id' => $aItem['id']), $iTrimTo);
				if (!empty($aData['items'][$iKey]['images'])) {
					// parsowanie szablonu zdjec
					$pSmarty->assign_by_ref('aImages', $aData['items'][$iKey]['images']);
					$aData['items'][$iKey]['images'] = $pSmarty->fetch($this->sTemplatesPath.'/_images_'.$aItem['img_align'].'.tpl');
					$pSmarty->clear_assign('aImages');
				}
				else {
					unset($aData['items'][$iKey]['images']);
				}
			}
			else {
				$aData['items'][$iKey]['image'] = getItemImage($aConfig['tabls']['prefix']."texts_images", array('paragraph_id' => $aItem['id']), '__t_');
			}
			// zalaczniki paragrafu
			if ($bFiles) {
				$aData['items'][$iKey]['files'] = getItemFiles($aConfig['tabls']['prefix']."texts_files", array('paragraph_id' => $aItem['id']));
				if (!empty($aData['items'][$iKey]['files'])) {
					// parsowanie szablonu plikow
					$pSmarty->assign_by_ref('aFiles', $aData['items'][$iKey]['files']);
					$aData['items'][$iKey]['files'] = $pSmarty->fetch($this->sTemplatesPath.'/_files.tpl');
					$pSmarty->clear_assign('aFiles');
				}
				else {
					unset($aData['items'][$iKey]['files']);
				}
			}
		}
		$pSmarty->clear_assign('aModule');
		
		return $aData;
	} // end of getItems() method
} // end of Common_m_opisy Class
?>