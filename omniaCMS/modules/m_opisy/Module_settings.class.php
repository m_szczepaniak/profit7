<?php
/**
 * Klasa Module do obslugi konfiguracji stron modulu 'Strony opisowe'
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Settings {

  // nazwa modulu - do langow
	var $sModule;

	// Id strony dla ktorej edytowane sa ustawienia
	var $iId;

	// sciezka do katalogu zawierajacego szablony czesci klienckiej modulu
	var $sTemplatesDir;

	// lista szablonow czesci klienckiej modulu
	var $sTemplatesList;

	// plik XML z domyslna konfiguracja
	var $sXMLFile;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$sModule, &$iId) {
		global $aConfig;
		$this->sModule = $sModule;
		$this->iId = $iId;
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/modules/'.$this->sModule;
		$this->sTemplatesList =& GetTemplatesList($this->sTemplatesDir);
		$this->sXMLFile = 'config/'.$sModule.'/default_config.xml';

		// pobranie konfiguracji dla modulu
		$this->setSettings();
	} // end Settings() function


	/**
	 * Metoda pobiera konfiguracje dla strony modulu lub w przypadku
	 * braku konfiguracji (tworzenie nowej strony) domyslna konfiguracje
	 * z pliku XML
	 *
	 * @return	void
	 */
	function setSettings() {
		global $aConfig;
		$aCfg = array();

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."texts_settings
						 WHERE page_id = ".$this->iId;
		$aConfig['settings'][$this->sModule] =& Common::GetRow($sSql);

		if (empty($aConfig['settings'][$this->sModule])) {
			// brak ustawien - pobranie domyslnych z pliku XML
			$aConfig['settings'][$this->sModule] =& getModuleConfig($this->sXMLFile);
		}
	} // end of setSettings() method


	/**
	 * Metoda wprowadza zmiany w ustawieniach aktualnosci
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;

		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."texts_settings
						 WHERE page_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);

		$aValues = array(
			'template' => $_POST['template'],
			'thumb_size' => $_POST['thumb_size'],
			'small_size' => $_POST['small_size'],
			'big_size' => $_POST['big_size'],
			'photo_desc_shortcut' => (int) $_POST['photo_desc_shortcut']
		);

		if (!empty($aSettings)) {
			// Update
			return Common::Update($aConfig['tabls']['prefix']."texts_settings",
														$aValues,
														"page_id = ".$this->iId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('page_id' => $this->iId), $aValues);
			return Common::Insert($aConfig['tabls']['prefix']."texts_settings",
														$aValues) !== false;
		}
	} // end of UpdateSettings() funciton


	/**
	 * Metoda tworzy formularz edycji konfiguracji aktualnosci
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_mtype') {
			$aData =& $_POST;
		}
		if ($aData['photo_desc_shortcut'] == '') {
			$aData['photo_desc_shortcut'] = '0';
		}

		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['settings'], array('class'=>'merged', 'style'=>'padding-left: 208px'));

		// szablon strony
		$pForm->AddSelect('template', $aConfig['lang'][$this->sModule.'_settings']['template'], array(), $this->sTemplatesList, $aData['template'], '', false);

		// rozmiary zdjec
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule.'_settings']['photos'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
		$pForm->AddText('thumb_size', $aConfig['lang'][$this->sModule.'_settings']['thumb_size'], $aData['thumb_size'], array('maxlength'=>7, 'style'=>'width: 60px;'), '', 'text', true, '/^[0-9]{2,3}x[0-9]{2,3}$/');
		$pForm->AddText('small_size', $aConfig['lang'][$this->sModule.'_settings']['small_size'], $aData['small_size'], array('maxlength'=>7, 'style'=>'width: 60px;'), '', 'text', true, '/^[0-9]{2,3}x[0-9]{2,3}$/');
		$pForm->AddText('big_size', $aConfig['lang'][$this->sModule.'_settings']['big_size'], $aData['big_size'], array('maxlength'=>9, 'style'=>'width: 80px;'), '', 'text', true, '/^[0-9]{2,4}x[0-9]{2,4}$/');

		// liczba znakow w skrocie opisu zdjecia
		$pForm->AddText('photo_desc_shortcut', $aConfig['lang'][$this->sModule.'_settings']['photo_desc_shortcut'], $aData['photo_desc_shortcut'], array('maxlength'=>3, 'style'=>'width: 30px;'), '', 'text', true, '', array(0, 255));
	} // end of SettingsForm() function
} // end of Settings Class
?>