<?php
/**
 * Klasa Module do obslugi modulu 'strony opisowe'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// sciazka do katalogu dla zdjec, plikow
	var $sDir;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}
		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		// katalog na zdjecia, pliki
		$this->sDir = $this->iLangId.'/'.$iPId.'/'.$iId;



	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		// pibranie konfiguracji strony modulu
		$this->aSettings =& $this->setSettings($iPId);

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iId); break;
			case 'publish': $this->Publish($pSmarty, $iPId, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iPId, $iId); break;
			case 'update_images': $this->UpdateImages($pSmarty, $iPId, $iId); break;
			case 'images': $this->AddEditImages($pSmarty, $iPId, $iId); break;
			case 'add_photo': $this->SaveAndAddImage($pSmarty, $iPId, $iId); break;
			case 'update_files': $this->UpdateFiles($pSmarty, $iPId, $iId); break;
			case 'files': $this->AddEditFiles($pSmarty, $iPId, $iId); break;
			case 'add_file': $this->SaveAndAddFile($pSmarty, $iPId, $iId); break;

			case 'sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show(getPagePath($iPId,
																													 $this->iLangId, true),
																			 $this->getRecords($iPId),
																			 phpSelf(array('do'=>'proceed_sort'),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."texts_paragraphs",
												$this->getRecords($iPId),
												array(
													'page_id' => $iPId
											 	),
											 	'number');
			break;

			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda pobiera i ustawia konfiguracje strony modulu
	 *
	 * @param	integer	$iPId	- Id strony
	 * @return	array	- konfiguracja strony modulu
	 */
	function &setSettings($iPId) {
		global $aConfig;

		// pobranie konfiguracji dla modulu
		$sSql = "SELECT thumb_size, small_size, big_size
						 FROM ".$aConfig['tabls']['prefix']."texts_settings
						 WHERE page_id = ".$iPId;
						 
		return Common::GetRow($sSql);
	} // end of setSettings() method

	/**
	 * Metoda wyswietla liste zdefiniowanych dla strony paragrafow
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'published',
				'content'	=> $aConfig['lang'][$this->sModule]['list_published'],
				'sortable'	=> true,
				'width'	=> '100'
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '105'
			)
		);

		// pobranie liczby wszystkich paragrafow strony
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
						 WHERE page_id = ".$iId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
							 WHERE page_id = ".$iId.
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('paragraphs', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich paragrafow strony
			$sSql = "SELECT id, name, published, number, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
							 WHERE page_id = ".$iId.
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'number').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			for ($i = 0; $i < count($aRecords); $i++) {
				if (!empty($aRecords[$i]['name'])) {
					$aRecords[$i]['name'] = sprintf($aConfig['lang'][$this->sModule]['paragraph_name'], $aRecords[$i]['number'], $aRecords[$i]['name']);
				}
				else {
					$aRecords[$i]['name'] = sprintf($aConfig['lang'][$this->sModule]['paragraph_number'], $aRecords[$i]['number']);
				}
				if ($aRecords[$i]['published'] == '1') {
					$aRecords[$i]['published'] = $aConfig['lang']['common']['yes'];
				}
				else {
					$aRecords[$i]['published'] = $aConfig['lang']['common']['no'];
				}
			
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'number'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'pid' => $iId, 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('images', 'files', 'publish', 'edit', 'delete'),
					'params' => array (
												'images'	=> array('pid' => $iId, 'id'=>'{id}'),
												'files'	=> array('pid' => $iId, 'id'=>'{id}'),
												'publish'	=> array('pid' => $iId, 'id'=>'{id}'),
												'edit'	=> array('pid' => $iId, 'id'=>'{id}'),
												'delete'	=> array('pid' => $iId, 'id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('sort', 'add')
		);
		
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		);
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda zmienia stan publikacji wybranego paragrafu
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$sId	- ID aktualnosci i strony
	 * @return 	void
	 */
	function Publish(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		// pobranie tytulu akapitu
		$sSql = "SELECT IF (published = '0', '1', '0') AS published
				 		 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
					 	 WHERE id = ".$iId." AND
					 	 			 page_id = ".$iPId;
		if (($iPublished =& Common::GetOne($sSql)) === false) {
			$bIsErr = true;
		}

		if (!$bIsErr) {
			// zmiana stanu publikacji akapitu
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."texts_paragraphs
							 SET published = IF (published = '0', '1', '0')
							 WHERE id = ".$iId." AND
					 	 			 	 page_id = ".$iPId;
			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
		}
		if ($bIsErr) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_err'],
											$this->getItemName($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true),
											$aConfig['lang'][$this->sModule]['published_'.$iPublished]);
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
		}
		else {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_ok'],
											$this->getItemName($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true),
											$aConfig['lang'][$this->sModule]['published_'.$iPublished]);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
		}
		$this->Show($pSmarty, $iPId);
	} // end of Publish() funciton


	/**
	 * Metoda usuwa wybrane akapity
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony usuwanego akapitu
	 * @param	integer	$iId	- Id usuwanego akapitu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();

		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			if (count($_POST['delete']) == 1) {
				$sErrPostfix = '0';
			}
			else {
				$sErrPostfix = '1';
			}

			Common::BeginTransaction();
			// pobranie tytulow usuwanych akapitow
			$sSql = "SELECT id, name, number, page_id
					 		 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
						 	 WHERE page_id = ".$iPId." AND
						 	 			 id IN  (".implode(',', $_POST['delete']).")";
			$aRecords =& Common::GetAll($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				$sPagePath = getPagePath($iPId, $this->iLangId, true);
				foreach ($aRecords as $aItem) {
					$sDel .= '"'.(!empty($aItem['name']) ? sprintf($aConfig['lang'][$this->sModule]['paragraph_name'], $aItem['number'], $aItem['name']) : sprintf($aConfig['lang'][$this->sModule]['paragraph_number'], $aItem['number'])).'",<br>';
				}
				$sDel = substr($sDel, 0, -5);
			}

			if (!$bIsErr) {
				// usuwanie akapitow
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
								 WHERE page_id = ".$iPId." AND
						 	 			 	 id IN (".implode(',', $_POST['delete']).")";
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
				if (!$bIsErr) {
					// pobranie pozostalych akapitow
					$sSql = "SELECT id, number
							 		 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
								 	 WHERE page_id = ".$iPId."
								 	 ORDER BY number";
					$aRecords =& Common::GetAll($sSql);
					if ($aRecords === false) {
						$bIsErr = true;
					}
					if (!$bIsErr && !empty($aRecords)) {
						// wprowadzenie od nowa numeracji akapitow
						$i = 1;
						foreach ($aRecords as $aPar) {
							$sSql = "UPDATE ".$aConfig['tabls']['prefix']."texts_paragraphs
											 SET number = ".($i++)."
											 WHERE page_id = ".$iPId." AND
								 	 			 		 id = ".$aPar['id'];
							if (Common::Query($sSql) === false) {
								$bIsErr = true;
							}
						}
					}
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.$sErrPostfix],
												$sDel,
												$sPagePath);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);

				// usuniecie katalogow ze zdjeciami i plikami
				deleteDir($this->sDir);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_'.$sErrPostfix],
												$sDel,
												$sPagePath);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowy akapit aktualnosci
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
		Common::BeginTransaction();
		// okreslenie numeru dla nowego akapitu
		$sSql = "SELECT IFNULL(MAX(number), 0) + 1 AS number
						 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
						 WHERE page_id = ".$iPId;
		if (($iNumber =& Common::GetOne($sSql)) === false) {
			$bIsErr = true;
		}
		else {
			switch (intval($_POST['position'])) {
				case -1:
					// na poczatku - numer 1
					$iNumber = 1;
				break;

				case 0:
					// na koncu - bez zmian
				break;

				default:
					$iNumber = intval($_POST['position']) + 1;
				break;
			}

			// zmiana numerow aktualnych akapitow
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."texts_paragraphs SET
											number = number + 1
							 WHERE page_id = ".$iPId." AND
								 		 number >= ".$iNumber;
			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
		}

		if (!$bIsErr) {
			// dodanie akapitu
			$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."texts_paragraphs (
									 page_id,
									 name,
									 content,
									 number,
									 published,
									 created,
									 created_by
									)
									VALUES (
									 ".$iPId.",
									 ".(trim($_POST['name']) != '' ? "'".decodeString($_POST['name'])."'" : 'NULL').",
									 '".$_POST['content']."',
									 ".$iNumber.",
									 '".(isset($_POST['published']) ? '1' : '0')."',
									 NOW(),
									 '".$_SESSION['user']['name']."'
									)";
			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
		}

		// nazwa akapitu
		if (!empty($_POST['name'])) {
			$sName = sprintf($aConfig['lang'][$this->sModule]['paragraph_name'],
											 $iNumber,
											 $_POST['name']);
		}
		else {
			$sName = sprintf($aConfig['lang'][$this->sModule]['paragraph_number'],
											 $iNumber);
		}
		if (!$bIsErr) {
			// akapit zostal dodany
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											$sName,
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();

			AddLog($sMsg, false);

			$this->Show($pSmarty, $iPId);
		}
		else {
			// akapit nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$sName,
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId);
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych akapit aktualnosci
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id aktualizowanego akapitu
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iId);
			return;
		}
		Common::BeginTransaction();
		// aktualizacja akapitu
		
		$aValues = [
			'name' => (trim($_POST['name']) != '' ? decodeString($_POST['name']) : 'NULL'),
			'content' => $_POST['content'],
			'published' => (isset($_POST['published']) ? '1' : '0'),
			'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name'],
		];

		$bIsErr = (Common::Update('texts_paragraphs', $aValues, ' id = '.$iId.' AND
							 		 page_id = '.$iPId) === false);

		if (!$bIsErr) {
			// akapit zostal zaktualizowany
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$this->getItemName($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();

			AddLog($sMsg, false);

			$this->Show($pSmarty, $iPId);
		}
		else {
			// akapit nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$this->getItemName($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji akapitu
	 *
	 * @param		object	$pSmarty
	 * @param		string	$sId	- ID strony
	 * @param		integer	$iId	- ID edytowanego akapitu
	 * @param	bool	$bAddPhoto	- true: dodaj pole na zdjecie
	 * @param	bool	$bAddFile	- true: dodaj pole na plik
	 */
	function AddEdit(&$pSmarty, $iPId, $iId=0, $bAddPhoto = false, $bAddFile = false) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego akapitu
			$sSql = "SELECT name, content, number, published, created_by
							 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
							 WHERE id = ".$iId." AND
							 			 page_id = ".$iPId;
			$aData =& Common::GetRow($sSql);

			// nazwa akapitu
			if (!empty($aData['name'])) {
				$sParName = sprintf($aConfig['lang'][$this->sModule]['paragraph_name'], $aData['number'], $aData['name']);
			}
			else {
				$sParName = sprintf($aConfig['lang'][$this->sModule]['paragraph_number'], $aData['number']);
			}
		}
		else {
			// pobanie nazwy uzytkownika tworzacego strone do ktorej dodawany bedzie akapit
			$aData['created_by'] = getPageCreatorName($this->iLangId, $iPId);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		// okreslenie nazwy strony dla ktorej dodawany / edytowany jest akapit
		$sName = getPagePath($iPId, $this->iLangId, true);

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($sParName) && !empty($sParName)) {
			$sHeader .= ' "'.trimString($sParName, 50).'"';
		}

		$pForm = new FormTable('paragraphs', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>115), $aConfig['common']['js_validation']);
		$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], $sName, '', array(), array('class'=>'redText'));
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));

		// tytul paragrafu
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>255, 'style'=>'width: 350px;'), '', 'text', false);

		// polozenie paragrafu
		if ($iId == 0) {
			// pobranie listy dotychczasowych akapitow aktualnosci
			$sSql = "SELECT number AS value,
											CONCAT('".$aConfig['lang'][$this->sModule]['after']."', IFNULL(name, CONCAT('".$aConfig['lang'][$this->sModule]['paragraph']."', number))) AS label
							 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
							 WHERE page_id = ".$iPId."
							 ORDER BY number";
			$aParagraphs =& Common::GetAll($sSql);
			$aParagraphs = array_merge(
				array(array('value'=>'0', 'label'=>$aConfig['lang'][$this->sModule]['end'])),
				array(array('value'=>'-1', 'label'=>$aConfig['lang'][$this->sModule]['beginning'])),
				$aParagraphs);

			$pForm->AddSelect('position', $aConfig['lang'][$this->sModule]['position'], array(), $aParagraphs, $aData['position'], '', false);
		}

		// tresc
		$pForm->AddWYSIWYG('content', $aConfig['lang'][$this->sModule]['content'], $aData['content'], array());

		// status publikacji 
			$pForm->AddCheckBox('published', $aConfig['lang'][$this->sModule]['published'], array(), '', !empty($aData['published']), false);
	

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function


	/**
	 * Metoda pobiera liste rekordow
	 *
	 * @param	integer	$iPId	- Id strony
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($iPId) {
		global $aConfig;

		// pobranie wszystkich akapitów dla danej strony
		$sSql = "SELECT id, name, number
						 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
						 WHERE page_id = ".$iPId."
						 ORDER BY number";
		$aRecords =& Common::GetAssoc($sSql);
		foreach ($aRecords as $iKey => $aRecord) {
			if (empty($aRecord['name'])) {
				$aRecords[$iKey] = sprintf($aConfig['lang'][$this->sModule]['paragraph_number'], $aRecord['number']);
			}
			else {
				$aRecords[$iKey] = sprintf($aConfig['lang'][$this->sModule]['paragraph_name'], $aRecord['number'], $aRecord['name']);
			}
		}
		return $aRecords;
	} // end of getRecords() method


	/**
	 * Metoda wprowadza zmiany w zdjeciach akapitu do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateImages(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		if (!$this->ProceedImages($pSmarty, $iPId, $iId)) {
			// wyswietlenie komunikatu o niepowodzeniu
			// wycofanie transakcji
			// oraz ponowne wyswietlenie formularza edycji
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_img_err'],
											$this->getItemName($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true),
											$aConfig['lang']['common']['image_upload']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEditImages($pSmarty, $iPId, $iId);
		}
		else {
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_img_ok'],
											$this->getItemName($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
	} // end of UpdateImages() funciton


	/**
	 * Metoda wprowadza zmiany w zdjeciach akapitu do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function SaveAndAddImage(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		if (!$this->ProceedImages($pSmarty, $iPId, $iId)) {
			// wyswietlenie komunikatu o niepowodzeniu
			// wycofanie transakcji
			// oraz ponowne wyswietlenie formularza edycji
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_img_err'],
											$this->getItemName($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true),
											$aConfig['lang']['common']['image_upload']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEditImages($pSmarty, $iPId, $iId);
		}
		else {
			Common::CommitTransaction();
			$this->AddEditImages($pSmarty, $iPId, $iId, true);
		}
	} // end of SaveAndAddImage() funciton


	/**
	 * Metoda wykonuje operacje na zdjeciach akapitu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId
	 * @param	integer	$iId
	 *
	 * @return	integer
	 */
	function ProceedImages(&$pSmarty, $iPId, $iId) {
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditImages($pSmarty, $iPId, $iId);
			return;
		}

		Common::BeginTransaction();
		// dolaczenie klasy Images
		include_once('Images.class.php');
		$oImages = new Images($iPId,
													$iId,
													$this->aSettings,
													$this->sDir,
													'paragraph_id',
													'texts_images',
													'texts_paragraphs');
		return $oImages->proceed();
	} // end of ProceedImages() method


	/**
	 * Metoda tworzy formularz dodawania / edycji zdjec akapitu
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iPId	- I strony akapitu
	 * @param		integer	$iId	ID edytowanego akapitu
	 * @param		bool		$bAddPhoto	- true: zwieksz liczbe zdjec
	 */
	function AddEditImages(&$pSmarty, $iPId, $iId, $bAddPhoto=false) {
		global $aConfig;

		$aData = array();
		$aOrderBy = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych edytowanego akapitu
		$sSql = "SELECT img_align
						 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
						 WHERE id = ".$iId." AND
									 page_id = ".$iPId;
		$aData =& Common::GetRow($sSql);

		if ($aData !== false) {
			// pobranie zdjec
			$sSql = "SELECT id, directory, photo, description, name, order_by, author
							 FROM ".$aConfig['tabls']['prefix']."texts_images
							 WHERE paragraph_id = ".$iId."
						 	 ORDER BY order_by, id";
			$aImgs =& Common::GetAll($sSql);
		}
		if (!empty($_POST)) {
			$aData['img_align'] =& $_POST['img_align'];
		}
		if (!isset($aData['img_align']) || empty($aData['img_align'])) {
			$aData['img_align'] = 'right';
		}

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['images_header'],
											 $this->getItemName($iPId, $iId),
											 getPagePath($iPId, $this->iLangId, true));

		$pForm = new FormTable('images', $sHeader, array('action'=>phpSelf(array('pid' => $iPId, 'id' => $iId)), 'enctype'=>'multipart/form-data'), array('col_width'=>135), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_images');

		// wyrownanie zdjec
		$aImgAlign = array(
			array('value'=>'top', 'label'=>$aConfig['lang'][$this->sModule]['align_top']),
			array('value'=>'left', 'label'=>$aConfig['lang'][$this->sModule]['align_left']),
			array('value'=>'right', 'label'=>$aConfig['lang'][$this->sModule]['align_right']),
			array('value'=>'bottom', 'label'=>$aConfig['lang'][$this->sModule]['align_bottom'])
		);
		$pForm->AddRadioSet('img_align', $aConfig['lang'][$this->sModule]['img_align'], $aImgAlign, $aData['img_align'], '', false, false);

		// maksymalna suma rozmiarow przesylanych zdjec
		$pForm->AddRow($aConfig['lang']['common']['max_img_upload_size'], getMaxUploadSize(), '', array(), array('class'=>'redText'));

		// dopuszczalen formaty zdjec
		$pForm->AddRow($aConfig['lang']['common']['img_available_exts'], implode(', ', $aConfig['common']['img_extensions']), '', array(), array('class'=>'redText'));

		$iImages = 0;
		if (!empty($_POST['desc_image'])) {
    	$iImages = count($_POST['desc_image']);
		}
		elseif (!empty($aImgs)) {
    	$iImages = count($aImgs) + 1;
		}
		else {
			$iImages = $aConfig['default']['photos_no'];
		}
		if ($iImages < $aConfig['default']['photos_no']) {
			$iImages = $aConfig['default']['photos_no'];
		}
		if ($bAddPhoto) {
			// wybranmo dodanie kolejnego zdjecia
			$iImages += $aConfig['default']['photos_no'];
		}
		// przygotwanie tablicy do ustalania kolejnosci wyswietlania zdjec
		// dolaczenie klasy Images
		include_once('Images.class.php');
		$oImages = new Images($iPId,
													$iId,
													$this->aSettings,
													$this->sDir,
													'paragraph_id',
													'texts_images');
		$aOrderBy =& $oImages->getOrderBy(count($aImgs));

		for ($i = 0; $i < $iImages; $i++) {
			if (isset($aImgs[$i])) {
				$pForm->AddImage('image['.$i.'_'.$aImgs[$i]['id'].']', $aConfig['lang'][$this->sModule]['image'].' '.($i+1), $aImgs[$i], $aOrderBy);
			}
			else {
				$pForm->AddImage('image['.$i.'_0]', $aConfig['lang'][$this->sModule]['image'].' '.($i+1));
			}
		}
		$pForm->AddRow('&nbsp;', '<div class="existingPhoto">'.$pForm->GetInputButtonHTML('add_photo', $aConfig['lang']['common']['button_add_photo'], array('onclick'=>'GetObject(\'do\').value=\'add_photo\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'</div>');


		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEditImages() function


	/**
	 * Metoda wprowadza zmiany w plikach akapitu do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateFiles(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		if (!$this->ProceedFiles($pSmarty, $iPId, $iId)) {
			// wyswietlenie komunikatu o niepowodzeniu
			// wycofanie transakcji
			// oraz ponowne wyswietlenie formularza edycji
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_file_err'],
											$this->getItemName($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true),
											$aConfig['lang']['common']['file_upload']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEditFiles($pSmarty, $iPId, $iId);
		}
		else {
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_file_ok'],
											$this->getItemName($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
	} // end of UpdateFiles() funciton


	/**
	 * Metoda wprowadza zmiany w plikach akapitu do bazy danych
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId
	 * @param	integer	$iId
	 * @return	void
	 */
	function SaveAndAddFile(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		if (!$this->ProceedFiles($pSmarty, $iPId, $iId)) {
			// wyswietlenie komunikatu o niepowodzeniu
			// wycofanie transakcji
			// oraz ponowne wyswietlenie formularza edycji
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_file_err'],
											$this->getItemName($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true),
											$aConfig['lang']['common']['file_upload']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEditFiles($pSmarty, $iPId, $iId);
		}
		else {
			Common::CommitTransaction();
			$this->AddEditFiles($pSmarty, $iPId, $iId, true);
		}
	} // end of SaveAndAddFile() funciton


	/**
	 * Metoda wykonuje operacje na plikach akapitu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId
	 * @param	integer	$iId
	 * @return	integer
	 */
	function ProceedFiles(&$pSmarty, $iPId, $iId) {
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditFiles($pSmarty, $iPId, $iId);
			return;
		}

		Common::BeginTransaction();
		// dolaczenie klasy Files
		include_once('Files.class.php');
		$oFiles = new Files($iPId,
												$iId,
												$this->sDir,
												'paragraph_id',
												'texts_files',
												'texts_paragraphs');
		return $oFiles->proceed();
	} // end of ProceedFiles() method


	/**
	 * Metoda tworzy formularz dodawania / edycji plikow akapitu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony akapitu
	 * @param	integer	$iId	Id edytowanego akapitu
	 * @param	bool		$bAdd	- true: zwieksz liczbe plikow
	 */
	function AddEditFiles(&$pSmarty, $iPId, $iId, $bAdd=false) {
		global $aConfig;

		$aData = array();
		$aOrderBy = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie plikow
		$sSql = "SELECT id, directory, filename, description, name, order_by
						 FROM ".$aConfig['tabls']['prefix']."texts_files
						 WHERE paragraph_id = ".$iId."
					 	 ORDER BY order_by, id";
		$aFiles =& Common::GetAll($sSql);

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['files_header'],
											 $this->getItemName($iPId, $iId),
											 getPagePath($iPId, $this->iLangId, true));

		$pForm = new FormTable('files', $sHeader, array('action'=>phpSelf(array('pid' => $iPId, 'id' => $iId)), 'enctype'=>'multipart/form-data'), array('col_width'=>135), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_files');

		// maksymalna suma rozmiarow przesylanych plikow
		$pForm->AddRow($aConfig['lang']['common']['max_files_upload_size'], getMaxUploadSize(), '', array(), array('class'=>'redText'));

		// dopuszczalne formaty plikow
		$pForm->AddRow($aConfig['lang']['common']['files_available_exts'], implode(', ', $aConfig['common']['file_extensions']), '', array(), array('class'=>'redText'));

		$iFiles = 0;
		if (!empty($_POST['desc_file'])) {
    	$iFiles = count($_POST['desc_file']);
		}
		elseif (!empty($aFiles)) {
    	$iFiles = count($aFiles) + 1;
		}
		else {
			$iFiles = $aConfig['default']['files_no'];
		}
		if ($iFiles < $aConfig['default']['files_no']) {
			$iFiles = $aConfig['default']['files_no'];
		}
		if ($bAdd) {
			// wybranmo dodanie kolejnego zdjecia
			$iFiles += $aConfig['default']['files_no'];
		}
		// przygotwanie tablicy do ustalania kolejnosci wyswietlania plikow
		// dolaczenie klasy Files
		include_once('Files.class.php');
		$oFiles = new Files($iPId,
												$iId,
												$this->sDir,
												'paragraph_id',
												'texts_files');
		$aOrderBy =& $oFiles->getOrderBy(count($aFiles));

		for ($i = 0; $i < $iFiles; $i++) {
			if (isset($aFiles[$i])) {
				$pForm->AddAttachment('file['.$i.'_'.$aFiles[$i]['id'].']', $aConfig['lang'][$this->sModule]['file'].' '.($i+1), $aFiles[$i], $aOrderBy);
			}
			else {
				$pForm->AddAttachment('file['.$i.'_0]', $aConfig['lang'][$this->sModule]['file'].' '.($i+1));
			}
		}
		$pForm->AddRow('&nbsp;', '<div class="existingPhoto">'.$pForm->GetInputButtonHTML('add_file', $aConfig['lang']['common']['button_add_file'], array('onclick'=>'GetObject(\'do\').value=\'add_file\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'</div>');


		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEditFiles() function




	/**
	 * Metoda pbiera i zwraca nazwe rekordu o podanym ID
	 *
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id rekordu
	 * @return	string	- nazwa rekordu
	 */
	function getItemName($iPId, $iId) {
		global $aConfig;

		$sSql = "SELECT name, number
						 FROM ".$aConfig['tabls']['prefix']."texts_paragraphs
						 WHERE id = ".$iId." AND
					 	 			 page_id = ".$iPId;
		$aData =& Common::GetRow($sSql);

		// nazwa akapitu
		if (!empty($aData['name'])) {
			$aData['name'] = sprintf($aConfig['lang'][$this->sModule]['paragraph_name'], $aData['number'], $aData['name']);
		}
		else {
			$aData['name'] = sprintf($aConfig['lang'][$this->sModule]['paragraph_number'], $aData['number']);
		}
		return $aData['name'];
	} // end of getItemName() method
} // end of Module Class
?>