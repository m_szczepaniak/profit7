<?php
/**
 * Klasa zarzadzająca danymi do autoryzacji u dostawcy 
 * 
 * @author Marcin Janowski <janowski@windowslive.com>
 * @created 2015-05-18 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module {
  
    // komunikat
	private $sMsg;

	// nazwa modulu - do langow
	private $sModule;

	// ID wersji jezykowej
	private $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	private $aPrivileges;
  
  private $oKeyValue;
  
  private $sProviderName;
  private $oProviderClass;
  
  
  
  public function __construct($pSmarty) {
    global $aConfig, $pDbMgr;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
    
  	if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		} 
    
 		$sDo = '';
    $this->oKeyValue = new \table\keyValue('external_providers');
    $sSql = 'SELECT symbol 
             FROM external_providers 
             WHERE id = '.$iId;
    $sResult = $pDbMgr->getOne('profit24',$sSql);
    $this->sProviderName = ucfirst($sResult);
    $sProviderClassName = '\communicator\sources\\'.$this->sProviderName.'\\'.$this->sProviderName;
    require_once($_SERVER['DOCUMENT_ROOT'].'LIB/communicator/sources/'.$this->sProviderName.'/'.$this->sProviderName.'.class.php');
    $this->oProviderClass = new $sProviderClassName($pDbMgr);
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
    
    switch($sDo)
    {
      case 'SaveData':
        $this->SaveData($pSmarty, $iId);
      break;
      default: $this->Show($pSmarty, $iId);
      break;
    }

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    
    
  }
  //end of function __construct()
  
  /**
   * Metoda wyświetla formularz dodawania / edycji danych do autoryzacji 
   * @global type $aConfig
   * @param type $pSmarty
   */
  private function Show($pSmarty, $iId) {
    global $aConfig;
    
    include_once('Form/FormTable.class.php');
    
    $aData = $this->getAuthData($iId);
    
    $sHeader = $aConfig['lang'][$this->sModule]['providers_auth_header'];
    $pForm = new FormTable('set_auth_data', $sHeader, array('action'=>phpSelf(array('id' => $iId, 'do' => 'SaveData'))), array('col_width'=>150), $aConfig['common']['js_validation']);
    
    //login
    $pForm->AddRow($aConfig['lang'][$this->sModule]['auth_login'], $pForm->GetTextHTML('auth_login', $aConfig['lang'][$this->sModule]['auth_login'], $aData['login'], '', '', 'text', true));
		
    //haslo
    $pForm->AddRow($aConfig['lang'][$this->sModule]['auth_pasw'], $pForm->GetTextHTML('auth_pasw', $aConfig['lang'][$this->sModule]['auth_pasw'], $aData['passw'], array('maxlength' => '1024'), '', '', true));
    
    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['providers_auth_save']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show', 'action'=>'external_providers'), array('id')).'\');'), 'button'));
    
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
    
  }//end of show() function 
  

  /**
   * Metoda zapisująca dane logowania do bazy 
   * @param type $pSmarty
   * @param type $iId
   */
  private function SaveData($pSmarty, $iId) {
    global $aConfig; 
    $sLogin = $_POST['auth_login'];
    $sPassword = $_POST['auth_pasw'];
    $bIsError = false;
    
    if($this->CheckAuthData($sLogin, $sPassword)) {
      
      
      $bLoginRes = $this->oKeyValue->insertUpdate($iId, 'login', $sLogin);
      $bPasswRes = $this->oKeyValue->insertUpdate($iId, 'passw', $sPassword);
      
      if(!$bLoginRes && !$bPasswRes) {
        $sMsgPrefix = $aConfig['lang'][$this->sModule]['auth_insert_fail'];
        $bIsError = true;
      }
      
      
      
      $sMsg = $sMsgPrefix.sprintf($aConfig['lang'][$this->sModule]['auth_success'],$this->sProviderName);
      setSessionMessage($sMsg, $bIsError);
      header("Location: ".phpSelf(array('do'=>'show', 'action'=>'external_providers')));
    }
    else {
      $sMsg = sprintf($aConfig['lang'][$this->sModule]['auth_err'],$this->sProviderName);
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty, $iId);
    }
    
    
    
  }
  //end of SaveData() function 
  
  /**
   * Klasa sprawdzająca czy dane do logowania są poprawne
   * @param string $sLogin
   * @param string $sPassword
   * @return boolean 
   */
  private function CheckAuthData($sLogin, $sPassword) {
    
    $sCardHTML = $this->oProviderClass->_doLogin($sLogin, $sPassword, true);
    $aMatches = array();
    
    preg_match('/Zalogowany jako/', $sCardHTML, $aMatches);
    if (!empty($aMatches)) {
      return TRUE;
    }
    return FALSE;
    
     
  }
  //end of CheckAuthData() function
  
  /**
   * Klasa pobierająca dane do logowania z bazy 
   * @param integer $iId - id dostawcy
   */
  private function getAuthData($iId) {
  
     $aData['login'] =  $this->oKeyValue->getByIdDestKey($iId,'login');
     $aData['passw'] = $this->oKeyValue->getByIdDestKey($iId,'passw');

     return $aData;

  }
  //end of getAuthData() function 
  


}


