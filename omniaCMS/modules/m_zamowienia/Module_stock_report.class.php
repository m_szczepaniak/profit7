<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - statystyki sprzedazy produktow
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('modules/m_oferta_produktowa/Module_Common.class.php');
set_time_limit('3600');
ini_set('memory_limit', '512M');
    
class Module extends Module_Common {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// ID strony modulu zamowien
	var $iPageId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  private $sDBFFilePath = 'import/DBF/magazine';
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty, $bInit = FALSE) {
		global $aConfig;

    if ($bInit === TRUE) {
      return true;
    }
		
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_zamowienia', $_GET['action']);
		$this->sModule = $_GET['module'];
		
		$sDo = '';
		$iId = 0;
				
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		$this->aPrivileges =& $_SESSION['user']['privileges'];
    
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    $sItem = '';
    if (isset($_GET['item'])) {
      $sItem = $_GET['item'];
    }
    
		// wyswietlenie
		switch ($sDo) {
			case 'show_stats':
				$this->showStats($pSmarty);
			break;
      case 'delete':
        $this->delete($pSmarty, $sItem);
      break;
			case 'choose_conditions':
			default:
				$this->chooseConditions($pSmarty);
			break;
		}
	} // end of Module() method
  
  
  /**
   * Metoda usuwa plik z eksportem
   * 
   * @global array $aConfig
   * @param type $pSmarty
   * @param type $sFile
   * @return type
   */
  public function delete($pSmarty, $sFile) {
    global $aConfig;
    $bIsErr = FALSE;
    
    $sPath = $aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath;
    if (file_exists($sPath.'/'.$sFile)) {
      if (unlink($sPath.'/'.$sFile) === FALSE) {
        $bIsErr = TRUE;
      }
    } else {
      $bIsErr = TRUE;
    }
    
    if ($bIsErr === TRUE) {
      $this->sMsg = sprintf(_('Wystąpił błąd usuwania pliku pdf "%s"'), $sFile);
      AddLog($this->sMsg, true);
      $this->sMsg = GetMessage($this->sMsg, true);
      return $this->chooseConditions($pSmarty);
    } else {
      $this->sMsg = sprintf(_('Plik pdf "%s" został usunięty'), $sFile);
      AddLog($this->sMsg, false);
      $this->sMsg = GetMessage($this->sMsg, false);
      return $this->chooseConditions($pSmarty);
    }
  }// end of delete() method
  
  
  /**
   * Metoda zwraca listę plików do zapakowania zipem
   * 
   * @global array $aConfig
   * @return array
   */
  private function getPDFList() { 
    global $aConfig;
    
    $sURL = $aConfig['common']['client_base_url_https'].$this->sDBFFilePath.'/';
    $aZipFiles = array();
    
    $sPath = $aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath;
    
    if ($handle = opendir($sPath)) {
      while (false !== ($sFile = readdir($handle))) {
        if ($sFile != "." && $sFile != "..") {
          $aMatches = array();
          preg_match('/([^\/]*(\.pdf))$/', $sFile, $aMatches);
          if (isset($aMatches[1]) && !empty($aMatches[1])) {
            $aZipFiles[] = array(
                'label' => $sFile,
                'value' => $sURL.$sFile,
                'last_modified' => date("d.m.Y H:i:s", filemtime($sPath.'/'.$sFile))
            );
          }
        }
      }
      closedir($handle);
    }
    arsort($aZipFiles);
    return $aZipFiles;
  }// end of getZIPList() method
  
  
	/**
	 * Metoda tworzy formularz wyboru danych do statystyk
	 * oraz wyswietla statystyki
	 *
	 * @param		object	$pSmarty
	 */
	function chooseConditions(&$pSmarty) {
		global $aConfig;
		$aLang =& $aConfig['lang'][$this->sModule];
				
		$aData = array();
		$aConditions = array('start_date', 'end_date');
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			// zczytanie z GET
			foreach ($aConditions as $sCond) {
				$aData[$sCond] = isset($_GET[$sCond]) ? $_GET[$sCond] : '';
			}
		}
//		if (!isset($aData['start_date']) || $aData['start_date'] == '') {
//			$aData['start_date'] = '00-00-0000';
//		}
		//if (!isset($aData['order_status']) || $aData['order_status'] == '') {
	//	$aData['order_status'] = '-1';
		//}
					
		$pForm = new FormTable('stats_conditions', $aLang['header'], array('action'=>phpSelf(array())), array('col_width'=>190), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'show_stats');
		
		// zakres dat od
		$pForm->AddRow($pForm->GetLabelHTML('', $aLang['dates_range']), '<div style="float: left; margin-top: 8px;">'.$pForm->GetLabelHTML('start_date', $aLang['start_date_from'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('start_date', $aLang['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date').'</div><div style="float: left; margin-top: 8px; padding-left: 25px;">'.$pForm->GetLabelHTML('end_date', $aLang['end_date_to'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('end_date', $aLang['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date').'</div>');
								
		// przyciski		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aLang['button_show']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('action'=>'statistics')).'\');'), 'button'));
		
		$sJS = '<script type="text/javascript">
		var sSourceRowSel = "'.($aData['source'] != '-1' ? $aData['source'] : '').'";
		</script>';
    
    $pForm->AddMergedRow(_('Wygenerowane raporty magazynowe w formacie DBF'), array('class' => 'merged'));
    $aZipFiles = $this->getPDFList();
    if (!empty($aZipFiles)) {
      foreach ($aZipFiles as $aZipFile) {
        $pForm->AddRow('', '<a style="font-size: 15px!important;" target="_blank" href="'.$aZipFile['value'].'">'.$aZipFile['label'].'</a> 
                                            ('.$aZipFile['last_modified'].') 
                                            <a title="Usuń raport" href="'.phpSelf(array('do' => 'delete', 'item' => $aZipFile['label'])).'"><img src="gfx/icons/delete_ico.gif" /></a>');
      }
    }
    $pForm->AddMergedRow(_(''), array('class' => 'merged'));
    
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of chooseConditions() function
	
	
	/**
	 * Metoda wyswietla statystyki dla wybranych warunkow
	 *
	 * @param		object	$pSmarty
	 */
	function showStats(&$pSmarty) {
		global $aConfig;
		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';
		$sSourceSql = '';
		$sSourceIt = '';
		$aSource = array();
		
		$aReport = array();
		
    if ($_SESSION['user']['id'] > 0) {
      $sSql = "SELECT name, surname
                FROM ".$aConfig['tabls']['prefix']."users
                 WHERE id = ".intval($_SESSION['user']['id']);
    }			 			 
		$aGeneratorName=Common::GetRow($sSql);
		$aReport['lang'] =& $aConfig['lang'][$this->sModule];
		$aReport['date_start'] = formatDateClient(FormatDate($_POST['start_date']));
		$aReport['date_end'] = formatDateClient(FormatDate($_POST['end_date']));
		$aReport['generator_name']=$aGeneratorName['name'].' '.$aGeneratorName['surname'];
		$aReport['generator_login']=$_SESSION['user']['name'];
		
		
		// pobierzmy id i symbole dostepnych serwiwsów
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."websites";
		$aWebsites =& Common::GetAll($sSql);
		foreach ($aWebsites as $aWebsite) {
			$aProducts[$aWebsite['id']] =& $this->getProductOrdersReport('postal_fee', $aWebsite['id'], null);
			if(!empty($aProducts[$aWebsite['id']]['items'])){
				$aReport['postal_fee'][$aWebsite['id']] =& $this->getProductListHtml($aProducts[$aWebsite['id']]);
				$aReport['postal_fee_total'][$aWebsite['id']] = $aProducts[$aWebsite['id']]['total_val_brutto'];
			}

			$aProducts[$aWebsite['id']] =& $this->getProductOrdersReport('platnoscipl', $aWebsite['id'], null);
			if(!empty($aProducts[$aWebsite['id']]['items'])){
				$aReport['platnoscipl'][$aWebsite['id']] =& $this->getProductListHtml($aProducts[$aWebsite['id']]);
				$aReport['platnoscipl_total'][$aWebsite['id']] = $aProducts[$aWebsite['id']]['total_val_brutto'];
				$aReport['platnoscipl_payments'][$aWebsite['id']] = $this->getOrdersPayments('platnoscipl', $aWebsite['id']);
			}

			$aProducts[$aWebsite['id']] =& $this->getProductOrdersReport('bank_transfer', $aWebsite['id'], null);
			if(!empty($aProducts[$aWebsite['id']]['items'])){
				$aReport['bank_transfer'][$aWebsite['id']] =& $this->getProductListHtml($aProducts[$aWebsite['id']]);
				$aReport['bank_transfer_total'][$aWebsite['id']] = $aProducts[$aWebsite['id']]['total_val_brutto'];
				$aReport['bank_transfer_payments'][$aWebsite['id']] = $this->getOrdersPayments('bank_transfer', $aWebsite['id']);
			}

			$aReport['bank_14days'][$aWebsite['id']] = $this->getVatOrders($aWebsite['id']);
			if(!empty($aReport['bank_14days'][$aWebsite['id']])){
				foreach($aReport['bank_14days'][$aWebsite['id']] as $iKey=>$aOrder){
					$aReport['bank_14days'][$aWebsite['id']][$iKey]['invoice_address'] = $this->getOrderInvoiceAddress($aOrder['id']);
					$aProducts[$aWebsite['id']] = $this->getProductOrdersReport('bank_14days', $aWebsite['id'],$aOrder['id']);
					$aReport['bank_14days'][$aWebsite['id']][$iKey]['items'] = $this->getProductListHtml($aProducts[$aWebsite['id']]);	
					$aReport['bank_14days'][$aWebsite['id']][$iKey]['total'] = $aProducts[$aWebsite['id']]['total_val_brutto'];
				}
			}
		}
		
		
		$pSmarty->assign_by_ref('aRaport', $aReport);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'report.tpl');
		$pSmarty->clear_assign('aRaport', $aReport);
		
		require_once('OLD_tcpdf/config/lang/pl.php');
		require_once('OLD_tcpdf/tcpdf.php');		
		// create new PDF document
		$pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('profit24');
		$pdf->SetTitle($aLang['title']);
		$pdf->SetSubject($aLang['title']);
		$pdf->SetKeywords('');
		$pdf->setPrintHeader(false);
		$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));
			
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
		//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 10);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
		
		//set some language-dependent strings
		$pdf->setLanguageArray($l); 
		
		// set font
		$pdf->SetFont('freesans', '', 8);
		
		$pdf->AddPage();
		$pdf->writeHTML($sHtml, true, false, false, false, '');
		
		// nazwa pliku - uwzgledniajaca daty
		$sFileName = $aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath.'/raport_magazynowy-'.($_POST['start_date'] == $_POST['end_date'] ? str_replace('-', '', $_POST['start_date']) : str_replace('-', '', $_POST['start_date']).'-'.str_replace('-', '', $_POST['end_date'])).'.pdf';
		$pdf->Output($sFileName, 'F');
    
    $sMsg = _('Plik pdf został wygenerowany i jest gotowy do pobrania.');
    $this->sMsg = GetMessage($sMsg, false);
    $this->chooseConditions($pSmarty);
	} // end of showStats() function

	
	/**
	 * 
	 * @param $sPaymentType
	 * @return unknown_type
	 */
	function &getProductOrdersReport($sPaymentType, $iWebsiteId, $iOrderId=0){
		global $aConfig;
		
		$sPaymentSql = '';
		if ($sPaymentType != '') {
			if ($sPaymentType == 'platnoscipl') {
				$sPaymentSql = " AND IF(A.payment_type<>'', 
															 (A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay'), 
															 (B.payment_type = 'platnoscipl' OR B.payment_type = 'card' OR B.payment_type = 'dotpay')) ";
			}
			else {
				$sPaymentSql = " AND IF(A.payment_type<>'', (A.payment_type = '".$sPaymentType."'), (B.payment_type = '".$sPaymentType."')) ";
			}
		}
		/*
SELECT A.id, A.product_id, A.name, A.authors,                                        
        A.discount, A.price_brutto, A.price_netto, A.vat, A.promo_price_brutto, A.isbn,                                        
        SUM(A.quantity) quantity, SUM(A.value_brutto) val_brutto, SUM(A.value_netto) val_netto, B.order_number,                    
        GROUP_CONCAT(A.id SEPARATOR ',') as ids                            
        FROM orders_items A                            
        JOIN orders B                             
        ON B.id = A.order_id                             
        WHERE A.deleted ='0'                                         
        AND A.packet = '0'                                        
        AND A.item_type <> 'A'                                        
        AND B.website_id = 1                                          
        AND IF(A.payment_type<>'', (A.payment_type = 'postal_fee'), (B.payment_type = 'postal_fee'))  
                AND                                          
                B.invoice_date >= '2014-05-01 00:00:00' 
                AND                                          
                B.invoice_date <= '2014-05-01 23:59:59'                                      
                GROUP BY A.product_id, A.price_brutto, A.discount, A.vat, A.name                                      
                ORDER BY A.discount, A.name
    */
    
		$sSql = "SELECT A.id, A.product_id, A.name, A.authors,
										A.discount, A.price_brutto, A.price_netto, A.vat, A.promo_price_brutto, A.isbn,
										SUM(A.quantity) quantity, SUM(A.value_brutto) val_brutto, SUM(A.value_netto) val_netto, B.order_number,
                    GROUP_CONCAT(A.id SEPARATOR ',') as ids
							FROM ".$aConfig['tabls']['prefix']."orders_items A
							JOIN ".$aConfig['tabls']['prefix']."orders B
							 ON B.id = A.order_id
							 WHERE A.deleted ='0'
							 			AND A.packet = '0'
										AND A.item_type <> 'A'
										AND B.website_id = ".$iWebsiteId."
							 			".$sPaymentSql." AND
							 			 B.invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			 B.invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'".
										($iOrderId>0?" AND A.order_id = ".$iOrderId:'').
							 			 "
							 		 GROUP BY A.product_id, A.price_brutto, A.discount, A.vat, A.name
							 		 ORDER BY A.discount, A.name";
		$aProducts['items'] =& Common::GetAll($sSql);
		$aProducts2=array();
		$aProducts2['total_val_brutto'] = 0.0;
		$aProducts2['total_price_brutto'] = 0.0;
		$aProducts2['total_promo_price_brutto'] = 0.0;
		$aProducts2['total_quantity'] = 0;
		$aAttachments=array();
		$aIds=array();
		$aIds[0]=0;
		$aCounts=array();
		$iLp=1;
		
		// dodanie załączników zamówienia
		$aIOIds = array();
		$aNProducts = array();
		$aProductsTMP = array();
		foreach ($aProducts['items'] as $aPItem) {
			 $aIOIds[] = $aPItem['id'];
			 
					 $sSql = "SELECT A.id, A.product_id, A.name, A.authors,
										A.discount, A.price_brutto, A.price_netto, A.vat, A.promo_price_brutto, A.isbn,
										SUM(A.quantity) quantity, SUM(A.value_brutto) val_brutto, SUM(A.value_netto) val_netto, B.order_number
							FROM ".$aConfig['tabls']['prefix']."orders_items A
							JOIN ".$aConfig['tabls']['prefix']."orders B
							 ON B.id = A.order_id
							 WHERE A.deleted ='0'
							 			AND A.packet = '0'
										AND A.item_type = 'A'
										AND B.website_id = ".$iWebsiteId."
							 			AND A.parent_id IN (".$aPItem['ids'].")
							 		 GROUP BY A.product_id, A.price_brutto, A.discount, A.vat, A.name
							 		 ORDER BY IFNULL( A.parent_id, A.id ), A.id, A.discount";
				$aProductsTMP =& Common::GetAll($sSql);
				
				// dołączenie ojca
				$aNProducts[] = $aPItem;
				foreach ($aProductsTMP as $aPTMP) {
					// nazwy analogicznie jak były
					$aPTMP['name'] = $aPTMP['name'].'-'.$aPItem['name'];
					$aNProducts[] = $aPTMP;
				}
		}
		// nadpisanie zamowień
		$aProducts['items'] = $aNProducts;
		// koniec dodanie załączników zamówienia
		
		if(!empty($aProducts['items'])){
			foreach($aProducts['items'] as $iKey=>$aProduct){
				$aItem=array();
				$aItem['lp']=$iLp++;
				$aItem['isbn']=$aProduct['isbn'];
				$aItem['authors'] = mb_substr(strip_tags($aProduct['authors']),0,12,'UTF-8').(mb_strlen($aProduct['authors'], 'UTF-8')>12?'...':'');
				$aItem['name'] = mb_substr(strip_tags($aProduct['name']),0,38,'UTF-8').(mb_strlen($aProduct['name'], 'UTF-8')>38?'...':'');
				$aProducts2['total_quantity'] += $aProduct['quantity'];
				$aIds[]=$aProduct['id'];
				$aCounts[$aProduct['id']][0]=$aProduct['quantity'];
				$aCounts[$aProduct['id']][1]=$aProduct['name'];
				
				$aItem['val_brutto'] = $aProduct['val_brutto'];//Common::formatPrice(($aProduct['price_brutto']*(1-$aProduct['discount']/100))*$aProduct['quantity']);
				$aItem['discount'] = Common::formatPrice($aProduct['discount']);
				$aItem['price_netto'] = $aProduct['val_netto'];//=$aProduct['val_brutto']/(1+$aProduct['vat']/100);//$aProduct['val_netto'];//Common::formatPrice($aProduct['val_brutto']/(1+$aProduct['vat']/100));//
				
				$aItem['price_brutto'] = Common::formatPrice($aProduct['price_brutto']);
				$aItem['price_netto'] = Common::formatPrice($aItem['price_netto']);
				$aItem['val_brutto'] = Common::formatPrice($aItem['val_brutto']);
				$aItem['promo_price_brutto'] = Common::formatPrice($aProduct['promo_price_brutto']);
				$aItem['vat'] = round($aProduct['vat'],0);//Common::formatPrice($aProduct['vat']);
				$aItem['quantity'] = $aProduct['quantity'];
				
				$aProducts2['total_price_brutto'] += Common::formatPrice2($aItem['price_brutto']);
				$aProducts2['total_price_netto'] += Common::formatPrice2($aProduct['val_netto']);//Common::formatPrice2($aItem['price_netto']);
				$aProducts2['total_promo_price_brutto'] += Common::formatPrice2($aItem['promo_price_brutto']);
				$aProducts2['total_val_brutto'] += $aProduct['val_brutto'];//Common::formatPrice2($aItem['val_brutto']);
		
				$aItem['order_number'] = $aProduct['order_number'];
				$aProducts2['items'][]=$aItem;
				if(!empty($aAt))
					$aProducts2['items'][]=$aAt;
			}
		}
	
		$aTransports = $this->getOrdersTransports($sPaymentType, $iWebsiteId, $iOrderId);
		
		if(!empty($aTransports)){
			foreach($aTransports as $iKey=>$aTransport){
				$aProducts2['total_val_brutto'] += $aTransport['transport_cost']*$aTransport['quantity'];
				$aProducts2['total_price_netto'] += Common::formatPrice2($aTransport['transport_cost']/(1.0+($aTransport['transport_vat']/100.0)))*$aTransport['quantity'];
				$aProducts2['total_price_brutto'] += $aTransport['transport_cost'];
				$aProducts2['total_promo_price_brutto'] += $aTransport['transport_cost'];
				$aProducts2['total_quantity'] += $aTransport['quantity'];
				$aItem = array();
				$aItem['price_brutto'] = Common::formatPrice($aTransport['transport_cost']);
				$aItem['price_netto'] = Common::formatPrice(($aTransport['transport_cost']/(1.0+($aTransport['transport_vat']/100.0)))*$aTransport['quantity']);
				$aItem['discount'] = '0,00';
				$aItem['vat'] = round($aTransport['transport_vat'],0);//Common::formatPrice($aTransport['transport_vat']);
				$aItem['promo_price_brutto'] = Common::formatPrice($aTransport['transport_cost']);
				$aItem['val_brutto'] = Common::formatPrice($aTransport['transport_cost']*$aTransport['quantity']);
				$aItem['name'] = $aTransport['transport'];
				$aItem['quantity'] = $aTransport['quantity'];
				$aProducts2['items'][] = $aItem;
			}
		}
		
		//$aProducts['total'] = $fTotalValBrutto;
		$aProducts2['total_val_brutto'] = Common::formatPrice($aProducts2['total_val_brutto']);
		$aProducts2['total_price_netto'] = Common::formatPrice($aProducts2['total_price_netto']);
		$aProducts2['total_price_brutto'] = Common::formatPrice($aProducts2['total_price_brutto']);
		$aProducts2['total_promo_price_brutto'] = Common::formatPrice($aProducts2['total_promo_price_brutto']);
		
		if($sPaymentType!='bank_14days') {
			$aVals2=$this->getOrdersReport($sPaymentType, $iWebsiteId);
			$aProducts2['total_price_netto']=$aVals2['total_netto'];
		}
		return $aProducts2;
	} // end of getProductOrdersReport() method 
	
	
	
	function getProductListHtml(&$aProducts){
		global $aConfig,$pSmarty;
		$aModule = array();
		$aModule['lang'] =& $aConfig['lang'][$this->sModule];
		
		$aModule['products'] =& $aProducts;
		
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'products_table.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	} // end of getProductListHtml() method
	

	function &getVatOrders($iWebsiteId){
	global $aConfig;
		$sSql = "SELECT *
							FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE (payment_type = 'bank_14days' OR  second_payment_type = 'bank_14days') AND
										 website_id = ".$iWebsiteId." AND
							 			 invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			 invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'";
		return Common::GetAll($sSql);
	} // end of getVatOrders() method

	function &getOrdersPayments($sPaymentType, $iWebsiteId){
		global $aConfig;
		$sPaymentSql = '';
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(payment_type = 'platnoscipl' OR payment_type = 'card' OR payment_type = 'dotpay')";
		}
		else {
			$sPaymentSql = "payment_type = '".$sPaymentType."'";
		}
		$sSql = "SELECT DATE(payment_date) payment_date, DATE_FORMAT(payment_date, '%Y-%m-%d') payment_date2, SUM(paid_amount) paid_amount
							FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE ".$sPaymentSql." AND
										 website_id = ".$iWebsiteId." AND
							 			 invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			 invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'
							 GROUP BY payment_date2";
		$aPayments = Common::GetAll($sSql);
		foreach($aPayments as $iKey=>$aPayment){
			$aPayments[$iKey]['payment_date'] = formatDateClient($aPayment['payment_date']);
			$aPayments[$iKey]['paid_amount'] = Common::formatPrice($aPayment['paid_amount']);
		}
		return $aPayments;
	} // end of getOrdersPayments() method 
	
	function &getOrdersTransports($sPaymentType, $iWebsiteId, $iOrderId=0){
		global $aConfig;
    
		$sPaymentSql = '';
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay')";
		}
		else {
			$sPaymentSql = "A.payment_type = '".$sPaymentType."'";
		}
		$sSql = "SELECT A.transport_id, A.transport_cost,A.transport, A.transport_vat, count(A.id) as quantity 
							FROM ".$aConfig['tabls']['prefix']."orders A
							LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
							ON B.id=A.transport_id 
							WHERE B.personal_reception <> '1' AND
										".$sPaymentSql." AND
										A.website_id = ".$iWebsiteId." AND
							 			A.invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			A.invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'"
							 			.($iOrderId>0?" AND A.id = ".$iOrderId:'').
							" GROUP BY A.transport_id, A.transport_vat, A.transport_cost";
		return Common::GetAll($sSql);
	} // end of getOrdersTransports() method 


	function &getOrderInvoiceAddress($iId){
		global $aConfig;
		
		$sSql = "SELECT is_company, company, name, surname, street, number, number2, postal, city, nip, phone
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
						 " AND 
						 address_type = '0'
						  ORDER BY address_type"; 
		return Common::GetRow($sSql);
	} // end of getOrderInvoiceAddress() method
	
	
	function &getOrdersReport($sPaymentType, $iWebsiteId){
		global $aConfig;
		$sPaymentSql = '';
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(   (A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay')
											 OR (A.second_payment_type = 'platnoscipl' OR A.second_payment_type = 'card' OR A.second_payment_type = 'dotpay')
											)
											 ";
		}
		else {
			$sPaymentSql = " (A.payment_type = '".$sPaymentType."' OR A.second_payment_type = '".$sPaymentType."') ";
		}
		
		$sSql = "SELECT id, second_invoice
							FROM ".$aConfig['tabls']['prefix']."orders A
							 WHERE ".$sPaymentSql."  AND
										 A.website_id = ".$iWebsiteId." AND
							 			 A.invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			 A.invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'";

		$aOrders =& Common::GetAll($sSql);
		$aTable= array();
		$iLp=1;
		if(!empty($aOrders)){
			$sSql='SELECT value FROM '.$aConfig['tabls']['prefix'].'products_vat_stakes WHERE 1';
			$aVatStakesT=Common::GetAll($sSql);
			$aVatStakes=array();
			//przepisanie do tablicy
			foreach($aVatStakesT as $aVat)
				$aVatStakes[]=$aVat['value'];
				
			foreach($aOrders as $iKey=>$aOrder) {
				$aItem = $this->getOrderVat($aOrder['id'],false,$iLp++, $sPaymentType);
				$aTable['items'][] = $aItem;
				//sprawdzenie czy są jakieś inne stawki
				foreach($aItem['vat'] as $iOVats=>$aVaVa)
					if(!in_array($iOVats,$aVatStakes))
						$aVatStakes[]=$iOVats;
						
				foreach($aVatStakes as $iKey => $aVatItem) {
					$aTable['vat_'.$aVatItem.'_brutto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_brutto']);
					$aTable['vat_'.$aVatItem.'_netto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_netto']);
					//$aTable['vat_7_brutto'] += Common::formatPrice2($aItem['vat'][7]['value_brutto']);
					//$aTable['vat_22_brutto'] += Common::formatPrice2($aItem['vat'][22]['value_brutto']);
					}
					
				if($aOrder['second_invoice'] == '1') {
					$aItem = $this->getOrderVat($aOrder['id'],true,$iLp++, $sPaymentType);
					$aTable['items'][] = $aItem;
					foreach($aVatStakes as $iKey => $aVatItem) { 
						$aTable['vat_'.$aVatItem.'_brutto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_brutto']);
						$aTable['vat_'.$aVatItem.'_netto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_netto']);
						//$aTable['vat_7_brutto'] += Common::formatPrice2($aItem['vat'][7]['value_brutto']);
						//$aTable['vat_22_brutto'] += Common::formatPrice2($aItem['vat'][22]['value_brutto']);
					}
				}
			}
		}
    if (!empty($aVatStakes)) {
      foreach($aVatStakes as $iKey => $aVatItem) {
        // przeliczanie dla stawki VAT 0 - sformatowane z '.' jako separator dziesietny
        $aTable['vat_'.$aVatItem.'_brutto'] = Common::formatPrice2($aTable['vat_'.$aVatItem.'_brutto']);
        $aTable['vat_'.$aVatItem.'_netto'] = Common::FormatPrice2($aTable['vat_'.$aVatItem.'_brutto']/(1+$aVatItem/100));// =  Common::FormatPrice2($aTable['vat_'.$aVatItem.'_netto']);
        //$aTable['vat_'.$aVatItem.'_netto'] = Common::formatPrice2($aTable['vat_'.$aVatItem.'_brutto'] / (1 + $aVatItem/100));
        $aTable['vat_'.$aVatItem.'_currency'] = $aTable['vat_'.$aVatItem.'_brutto'] - $aTable['vat_'.$aVatItem.'_netto'];
        $aTable['total_brutto'] += $aTable['vat_'.$aVatItem.'_brutto'];
        $aTable['total_netto'] += $aTable['vat_'.$aVatItem.'_netto'];
        $aTable['total_vat'] += $aTable['vat_'.$aVatItem.'_currency'];

        /*// przeliczanie dla stawki VAT 7 - sformatowane z '.' jako separator dziesietny
        $aTable['vat_7_brutto'] = Common::formatPrice2($aTable['vat_7_brutto']);
        $aTable['vat_7_netto'] = Common::formatPrice2($aTable['vat_7_brutto'] / (1 + 7/100));
        $aTable['vat_7_currency'] = $aTable['vat_7_brutto'] - $aTable['vat_7_netto'];
        // przeliczanie dla stawki VAT 22 - sformatowane z '.' jako separator dziesietny
        $aTable['vat_22_brutto'] = Common::formatPrice2($aTable['vat_22_brutto']);
        $aTable['vat_22_netto'] = Common::formatPrice2($aTable['vat_22_brutto'] / (1 + 22/100));
        $aTable['vat_22_currency'] = $aTable['vat_22_brutto'] - $aTable['vat_22_netto'];*/
      }
    }
		// obliczanie sumy zamowienia - sformatowanie z ',' jako separator dziesietny
		$aTable['total_brutto'] = Common::formatPrice($aTable['total_brutto']);
		$aTable['total_netto'] = Common::formatPrice($aTable['total_netto']);
		$aTable['total_vat'] = Common::formatPrice($aTable['total_vat']);
    if (!empty($aVatStakes)) {
      foreach($aVatStakes as $iKey => $aVatItem) {
        // formatowanie wartosci stawki VAT 0 z ',' jako separator dziesietny
        $aTable['vat_'.$aVatItem.'_value'] = Common::formatPrice($aTable['vat_'.$aVatItem.'_netto']);
        $aTable['vat_'.$aVatItem.'_currency'] = Common::formatPrice($aTable['vat_'.$aVatItem.'_currency']);
        $aTable['show_vat_'.intval($aVatItem)]=true;
      }
    }
		/*// formatowanie wartosci stawki VAT 7 z ',' jako separator dziesietny
		$aTable['vat_7_value'] = Common::formatPrice($aTable['vat_7_netto']);
		$aTable['vat_7_currency'] = Common::formatPrice($aTable['vat_7_currency']);
		// formatowanie wartosci stawki VAT 22 z ',' jako separator dziesietny
		$aTable['vat_22_value'] = Common::formatPrice($aTable['vat_22_netto']);
		$aTable['vat_22_currency'] = Common::formatPrice($aTable['vat_22_currency']);*/
		return $aTable;
	} // end of getOrdersReport() method 
	
	
	/**
	 * Metoda generuje vat
	 *
	 * @global type $aConfig
	 * @global type $pSmarty
	 * @param type $iId
	 * @param type $bSecondInvoice
	 * @param type $iLp
	 * @return string 
	 */
	function getOrderVat($iId,$bSecondInvoice=false,$iLp, $sPaymentType){
	global $aConfig,$pSmarty;
	
		include_once('OrderItemRecount.class.php');
		$oOrderItemRecount = new OrderItemRecount(TRUE);
		$aOrderData = $oOrderItemRecount->getOrderPricesDetail($iId, $bSecondInvoice, $sPaymentType);
		$aOrder = $aOrderData['order'];
		$aOrder['lp'] = $iLp;

		if($bSecondInvoice){
			$aOrder['invoice_number'] = $aOrder['invoice2_id'];
			$aAddress = $this->getOrderInvoiceAddress($aOrder['id'], true);
		} else {
			$aOrder['invoice_number'] = $aOrder['invoice_id'];
			$aAddress = $this->getOrderInvoiceAddress($aOrder['id']);
		}
		
		if($aAddress['is_company'] == '1'){
			$aOrder['name'] = $aAddress['company'];
			$aOrder['nip'] = $aAddress['nip'];
		} else {
			$aOrder['name'] = $aAddress['surname'].' '.$aAddress['name'];
			$aOrder['nip'] = '&nbsp;';
		}
    
		return $aOrder;
	} // end of getInvoiceHtml() method
} // end of Module Class
?>