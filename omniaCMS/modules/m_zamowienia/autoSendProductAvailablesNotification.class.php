<?php
/**
 * Klasa automatycznej rozsyłki infomacji, o dostępności książki
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-12-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class autoSendProductAvailablesNotification {
  public $pDbMgr;
  private $bTestMode;
  
  function __construct($pDbMgr, $bTestMode) {
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }
  
  /**
   * Metoda pobiera powiadomienia do rozesłania o dostępności pozycji
   * 
   * @return array
   */
  private function _getNotificationsToSend() {
    
    $sSql = 'SELECT PAN.email, PAN.id AS notif_id, P.name, P.id
             FROM products_available_notifications AS PAN
             JOIN products AS P
              ON PAN.product_id = P.id
             WHERE 
              P.prod_status = "1" AND 
              P.published = "1"
             ';
    // pobranie zdjec produktu
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of _getNotificationsToSend() method
  
  
  /**
   * Metoda rozsyła wszystkie powiadomienia
   * 
   * 
   */
  public function sendNotifications() {
    $aNotifications = $this->_getNotificationsToSend();
    foreach ($aNotifications as $iKey => $aNotify) {
      $aNotify = $this->_parseNotificationsData($aNotify);
      if ($this->_sendUserNotification($aNotify)) {
        $this->_deleteUserNotification($aNotify['notif_id']);
      }
    }
  }// end of sendNotifications() method
  
  
  /**
   * Metoda usuwa zapisane powiadomienie dla użytkownika
   * 
   * @param int $iNotifId
   * @return bool
   */
  private function _deleteUserNotification($iNotifId) {
    
    $sSql = 'DELETE FROM products_available_notifications WHERE id = '.$iNotifId.';';
    return $this->pDbMgr->Query('profit24', $sSql);
  }// end of _deleteUserNotification() method
  
  
  /**
   * Metoda formuje dane do wysłania użytkownikowi
   * 
   * @global array $aConfig
   * @param array $aNotify
   * @return array
   */
  private function _parseNotificationsData($aNotify) {
    global $aConfig;
    
    $aNotify['link'] = 'https://www.profit24.pl'.createProductLink($aNotify['id'], $aNotify['name']);
    return $aNotify;
  }// end of _parseNotificationsData() method
  
  
  private function _sendUserNotification($aNotify) {
   global $aConfig;

    $aSettings = getSiteSettings();
    $sFromMail = $aSettings['email'];
    $sFrom = $aSettings['name'];

    
    $aVars = array('{link}','{name}');
    $aValues = array($aNotify['link'], $aNotify['name']);

    
    $aMail = Common::getWebsiteMail('auto_pow_o_dostepnosci', 'profit24');
    if ($this->bTestMode === FALSE) {
      return Common::sendWebsiteMail($aMail, $sFrom, $sFromMail, $aNotify['email'], $aMail['content'], $aVars, $aValues, array(), array(), 'profit24');
      //return
    } else {
      var_dump($sFromMail);
      var_dump($sFrom);
      var_dump($aMail);
      var_dump($aNotify);
      die;
    }
  }
}

error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 

$_GET['lang_id'] = '1';
$bTestMode = FALSE;
$oAutoSendProductAvailablesNotification = new autoSendProductAvailablesNotification($pDbMgr, $bTestMode);
$oAutoSendProductAvailablesNotification->sendNotifications();
