<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - statystyki sprzedazy produktow
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
// dolaczenie wspolnej klasy Module_Common
include_once('modules/m_oferta_produktowa/Module_Common.class.php');
set_time_limit('3600');
ini_set('memory_limit', '512M');

class Module extends Module_Common {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// ID strony modulu zamowien
	var $iPageId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_zamowienia', $_GET['action']);
		$this->sModule = $_GET['module'];
		
		$sDo = '';
		$iId = 0;
				
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		// wyswietlenie
		switch ($sDo) {
			case 'show_stats':
				$this->showStats($pSmarty);
			break;
			case 'choose_conditions':
			default:
				$this->chooseConditions($pSmarty);
			break;
		}
	} // end of Module() method


	/**
	 * Metoda tworzy formularz wyboru danych do statystyk
	 * oraz wyswietla statystyki
	 *
	 * @param		object	$pSmarty
	 */
	function chooseConditions(&$pSmarty) {
		global $aConfig;
		$aLang =& $aConfig['lang'][$this->sModule];
				
		$aData = array();
		$aConditions = array('start_date', 'end_date');
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			// zczytanie z GET
			foreach ($aConditions as $sCond) {
				$aData[$sCond] = isset($_GET[$sCond]) ? $_GET[$sCond] : '';
			}
		}
//		if (!isset($aData['start_date']) || $aData['start_date'] == '') {
//			$aData['start_date'] = '00-00-0000';
//		}
		//if (!isset($aData['order_status']) || $aData['order_status'] == '') {
	//	$aData['order_status'] = '-1';
		//}
					
		$pForm = new FormTable('stats_conditions', $aLang['header'], array('action'=>phpSelf(array())), array('col_width'=>190), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'show_stats');
		
		// zakres dat od
		$pForm->AddRow($pForm->GetLabelHTML('', $aLang['dates_range']), '<div style="float: left; margin-top: 8px;">'.$pForm->GetLabelHTML('start_date', $aLang['start_date_from'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('start_date', $aLang['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date').'</div><div style="float: left; margin-top: 8px; padding-left: 25px;">'.$pForm->GetLabelHTML('end_date', $aLang['end_date_to'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('end_date', $aLang['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date').'</div>');
								
		// przyciski		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aLang['button_show']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('action'=>'statistics')).'\');'), 'button'));
		
		$sJS = '<script type="text/javascript">
		var sSourceRowSel = "'.($aData['source'] != '-1' ? $aData['source'] : '').'";
		</script>';

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of chooseConditions() function
	
	
	/**
	 * Metoda wyswietla statystyki dla wybranych warunkow
	 *
	 * @param		object	$pSmarty
	 */
	function showStats(&$pSmarty) {
		global $aConfig;
		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';
		$sSourceSql = '';
		$sSourceIt = '';
		$aSource = array();
		
		$aReport = array();
		
		$sSql = "SELECT name, surname
							FROM ".$aConfig['tabls']['prefix']."users
							 WHERE id = ".intval($_SESSION['user']['id']);
							 			 
		$aGeneratorName=Common::GetRow($sSql);
		$aReport['lang'] =& $aConfig['lang'][$this->sModule];
		$aReport['date_start'] = formatDateClient(FormatDate($_POST['start_date']));
		$aReport['date_end'] = formatDateClient(FormatDate($_POST['end_date']));
		$aReport['generator_name']=$aGeneratorName['name'].' '.$aGeneratorName['surname'];
		$aReport['generator_login']=$_SESSION['user']['name'];
		
		$_GET['hideHeader'] = '1';
		$aProducts =& $this->getProductOrdersReport('postal_fee');
		if(!empty($aProducts['items'])){
			$aReport['postal_fee'] =& $this->getProductListHtml($aProducts);
			$aReport['postal_fee_total'] = $aProducts['total_val_brutto'];
		}
		
		$aProducts =& $this->getProductOrdersReport('platnoscipl');
		if(!empty($aProducts['items'])){
			$aReport['platnoscipl'] =& $this->getProductListHtml($aProducts);
			$aReport['platnoscipl_total'] = $aProducts['total_val_brutto'];
			$aReport['platnoscipl_payments'] = $this->getOrdersPayments('platnoscipl');
		}
		
		$aProducts =& $this->getProductOrdersReport('bank_transfer');
		if(!empty($aProducts['items'])){
			$aReport['bank_transfer'] =& $this->getProductListHtml($aProducts);
			$aReport['bank_transfer_total'] = $aProducts['total_val_brutto'];
			$aReport['bank_transfer_payments'] = $this->getOrdersPayments('bank_transfer');
		}
		
		$aReport['bank_14days'] = $this->getVatOrders();
		if(!empty($aReport['bank_14days'])){
			foreach($aReport['bank_14days'] as $iKey=>$aOrder){
				$aReport['bank_14days'][$iKey]['invoice_address'] = $this->getOrderInvoiceAddress($aOrder['id']);
				$aProducts = $this->getProductOrdersReport('bank_14days',$aOrder['id']);
				$aReport['bank_14days'][$iKey]['items'] = $this->getProductListHtml($aProducts);	
				$aReport['bank_14days'][$iKey]['total'] = $aProducts['total_val_brutto'];
			}
		}
		
		$pSmarty->assign_by_ref('aRaport', $aReport);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'report.tpl');
		$pSmarty->clear_assign('aRaport', $aReport);
		//dump($sHtml);
		
		require_once('OLD_tcpdf/config/lang/pl.php');
		require_once('OLD_tcpdf/tcpdf.php');		
		
		// create new PDF document
		$pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('profit24');
		$pdf->SetTitle($aLang['title']);
		$pdf->SetSubject($aLang['title']);
		$pdf->SetKeywords('');
		$pdf->setPrintHeader(false);
		$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));
			
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
		//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 10);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
		
		//set some language-dependent strings
		$pdf->setLanguageArray($l); 
		
		// set font
		$pdf->SetFont('freesans', '', 8);
		
		$pdf->AddPage();
		$pdf->writeHTML($sHtml, true, false, false, false, '');
		
		// nazwa pliku - uwzgledniajaca daty
		$sFileName = 'raport_magazynowy-'.($_POST['start_date'] == $_POST['end_date'] ? str_replace('-', '', $_POST['start_date']) : str_replace('-', '', $_POST['start_date']).'-'.str_replace('-', '', $_POST['end_date'])).'.pdf';
		$pdf->Output($sFileName, 'D');
	
	} // end of showStats() function

	
	/**
	 * 
	 * @param $sPaymentType
	 * @return unknown_type
	 */
	function &getProductOrdersReport($sPaymentType, $iOrderId=0){
		global $aConfig;
		
		$sPaymentSql = '';
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(B.payment_type = 'platnoscipl' OR B.payment_type = 'card' OR B.payment_type = 'dotpay')";
		}
		else {
			$sPaymentSql = "B.payment_type = '".$sPaymentType."'";
		}
		
		$sSql = "SELECT A.id, A.product_id, A.name, A.authors,
										A.discount, A.price_brutto, A.price_netto, A.vat, A.promo_price_brutto, A.isbn,
										SUM(A.quantity) quantity, SUM(A.value_brutto) val_brutto, SUM(A.value_netto) val_netto
							FROM ".$aConfig['tabls']['prefix']."orders_items A
							JOIN ".$aConfig['tabls']['prefix']."orders B
							 ON B.id = A.order_id
							 WHERE A.deleted ='0'
							 			AND A.packet = '0'
							 			AND ".$sPaymentSql." AND
							 			 B.invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			 B.invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'".
										($iOrderId>0?" AND A.order_id = ".$iOrderId:'').
							 			 "
							 		 GROUP BY A.product_id, A.discount, A.vat
							 		 ORDER BY A.discount";
		$aProducts['items'] =& Common::GetAll($sSql);
		
		$aProducts2=array();
		$aProducts2['total_val_brutto'] = 0.0;
		$aProducts2['total_price_brutto'] = 0.0;
		$aProducts2['total_promo_price_brutto'] = 0.0;
		$aProducts2['total_quantity'] = 0;
		$aAttachments=array();
		$aIds=array();
		$aIds[0]=0;
		$aCounts=array();
		$iLp=1;
		if(!empty($aProducts['items'])){
			foreach($aProducts['items'] as $iKey=>$aProduct){
				$aItem=array();
				$aItem['lp']=$iLp++;
				$aItem['isbn']=$aProduct['isbn'];
				$aItem['authors'] = mb_substr(strip_tags($aProduct['authors']),0,12,'UTF-8').(mb_strlen($aProduct['authors'], 'UTF-8')>12?'...':'');
				$aItem['name'] = mb_substr(strip_tags($aProduct['name']),0,38,'UTF-8').(mb_strlen($aProduct['name'], 'UTF-8')>38?'...':'');
				$aProducts2['total_quantity'] += $aProduct['quantity'];
				$aIds[]=$aProduct['id'];
				$aCounts[$aProduct['id']][0]=$aProduct['quantity'];
				$aCounts[$aProduct['id']][1]=$aProduct['name'];
				
				$aItem['val_brutto'] = $aProduct['val_brutto'];//Common::formatPrice(($aProduct['price_brutto']*(1-$aProduct['discount']/100))*$aProduct['quantity']);
				$aItem['discount'] = Common::formatPrice($aProduct['discount']);
				$aItem['price_netto'] = $aProduct['val_netto'];//=$aProduct['val_brutto']/(1+$aProduct['vat']/100);//$aProduct['val_netto'];//Common::formatPrice($aProduct['val_brutto']/(1+$aProduct['vat']/100));//
				
				//zalaczniki
				$aAt=array();
				$sSql='SELECT * FROM '.$aConfig['tabls']['prefix'].'orders_items_attachments
				WHERE order_item_id ='.$aProduct['id'];
				$aAttachments=Common::GetAll($sSql);
				foreach($aAttachments as $iKey=>$aAttachment) {	
					$aCounts[$aAttachment['order_item_id']][1]=$aAttachment['name'].'-'.$aCounts[$aAttachment['order_item_id']][1];
					$aAttachment['price_netto']=$aAttachment['price_brutto']/(1.0+$aAttachment['vat']/100.0);
					
					//$aProducts2['total_val_brutto'] += $aAttachment['price_brutto']*$aCounts[$aAttachment['order_item_id']][0];
					
					
					$aItem2 = array();
					$aItem2['price_brutto'] = Common::formatPrice2($aAttachment['price_brutto']);
					//$aItem2['price_netto'] = Common::formatPrice(($aAttachment['price_netto']/(1+$aProduct['discount']/100))*$aCounts[$aAttachment['order_item_id']][0]);
					$aItem2['discount'] = Common::formatPrice($aProduct['discount']);
					$aItem2['vat'] = round($aAttachment['vat'],0);//Common::formatPrice($aAttachment['vat']);
					$aItem2['promo_price_brutto'] = Common::formatPrice2($aAttachment['price_brutto'] - $aAttachment['price_brutto']*($aProduct['discount']/100));
					$aItem2['val_brutto'] = Common::formatPrice($aItem2['promo_price_brutto']*$aCounts[$aAttachment['order_item_id']][0]);
					$aItem2['price_netto']=Common::formatPrice(($aItem2['promo_price_brutto']/(1.0+$aAttachment['vat']/100.0))*$aCounts[$aAttachment['order_item_id']][0]);
					$aItem2['name'] = mb_substr(strip_tags($aCounts[$aAttachment['order_item_id']][1]),0,38,'UTF-8').(mb_strlen($aCounts[$aAttachment['order_item_id']][1], 'UTF-8')>38?'...':'');
					$aItem2['quantity'] = $aCounts[$aAttachment['order_item_id']][0];
					$aItem2['lp']=$iLp++;
					$aAt=$aItem2;
					
					$aProduct['price_brutto']-=$aAttachment['price_brutto'];
					$aProduct['price_netto']-=Common::formatPrice2($aAttachment['price_brutto']);
					$aProduct['total_val_brutto'] -= $aItem2['promo_price_brutto']*$aCounts[$aAttachment['order_item_id']][0];
					$aProducts2['total_price_brutto'] += Common::formatPrice2($aItem2['price_brutto']);
					$aProducts2['total_promo_price_brutto'] += Common::formatPrice2($aItem2['promo_price_brutto']);
					$aProducts2['total_quantity'] += $aCounts[$aAttachment['order_item_id']][0];
					//$aProducts2['total_price_netto'] += Common::formatPrice2($aItem2['price_netto']);
					//$aProducts2['total_val_brutto'] += Common::formatPrice2($aItem2['val_brutto']);
					//$aProducts2['items'][] = $aItem2;
					//$aItem['price_netto']-=Common::formatPrice2($aItem2['price_netto']);
					$aItem['val_brutto']-=Common::formatPrice2($aItem2['val_brutto']);//($aAttachment['price_brutto']/(1+$aProduct['discount']/100))*$aCounts[$aAttachment['order_item_id']][0];
					$aItem['price_netto']-=Common::formatPrice2($aItem2['val_brutto']);//($aAttachment['price_brutto']/(1+$aProduct['discount']/100))*$aCounts[$aAttachment['order_item_id']][0];
					//kompensacja sumy
					$aProducts2['total_price_netto']-=((Common::formatPrice2($aItem2['val_brutto'])-Common::formatPrice2($aItem2['price_netto']))/$aCounts[$aAttachment['order_item_id']][0])*($aCounts[$aAttachment['order_item_id']][0]>1?$aCounts[$aAttachment['order_item_id']][0]-1:0);
					}
				//koniec zalaczniki
				$aItem['price_brutto'] = Common::formatPrice($aProduct['price_brutto']);
				$aItem['price_netto'] = Common::formatPrice($aItem['price_netto']);
				$aItem['val_brutto'] = Common::formatPrice($aItem['val_brutto']);
				$aItem['promo_price_brutto'] = Common::formatPrice($aProduct['promo_price_brutto']);
				$aItem['vat'] = round($aProduct['vat'],0);//Common::formatPrice($aProduct['vat']);
				$aItem['quantity'] = $aProduct['quantity'];
				
				$aProducts2['total_price_brutto'] += Common::formatPrice2($aItem['price_brutto']);
				$aProducts2['total_price_netto'] += Common::formatPrice2($aProduct['val_netto']);//Common::formatPrice2($aItem['price_netto']);
				$aProducts2['total_promo_price_brutto'] += Common::formatPrice2($aItem['promo_price_brutto']);
				$aProducts2['total_val_brutto'] += $aProduct['val_brutto'];//Common::formatPrice2($aItem['val_brutto']);
					
				$aProducts2['items'][]=$aItem;
				if(!empty($aAt))
					$aProducts2['items'][]=$aAt;
			}
		}
		/*
		//jezeli są jakieś produkty to szukamy załączników
		if(count($aIds)>1) {
			$sSql='SELECT * FROM '.$aConfig['tabls']['prefix'].'orders_items_attachments
			WHERE order_item_id IN ('.implode(',', $aIds).')';
			$aAttachments=Common::GetAll($sSql);
			foreach($aAttachments as $iKey=>$aAttachment) {	
				$aCounts[$aAttachment['order_item_id']][1]=$aAttachment['name'].'-'.$aCounts[$aAttachment['order_item_id']][1];
				$aAttachment['price_netto']=$aAttachment['price_brutto']/(1.0+$aAttachment['vat']/100.0);
				
				$aProducts['total_val_brutto'] += $aAttachment['price_brutto']*$aCounts[$aAttachment['order_item_id']][0];
				$aProducts['total_price_brutto'] += $aAttachment['price_brutto'];
				$aProducts['total_promo_price_brutto'] += $aAttachment['price_brutto'];
				$aProducts['total_quantity'] += $aCounts[$aAttachment['order_item_id']][0];
				$aProducts['total_price_netto'] += $aAttachment['price_netto'];
				
				$aItem = array();
				$aItem['price_brutto'] = Common::formatPrice($aAttachment['price_brutto']);
				$aItem['price_netto'] = Common::formatPrice($aAttachment['price_netto']);
				$aItem['discount'] = '0,00';
				$aItem['vat'] = Common::formatPrice($aAttachment['vat']);
				$aItem['promo_price_brutto'] = Common::formatPrice($aAttachment['price_brutto']);
				$aItem['val_brutto'] = Common::formatPrice($aAttachment['price_brutto']*$aCounts[$aAttachment['order_item_id']][0]);
				$aItem['name'] = mb_substr($aCounts[$aAttachment['order_item_id']][1],0,48,'UTF-8').(mb_strlen($aCounts[$aAttachment['order_item_id']][1], 'UTF-8')>48?'...':'');
				$aItem['quantity'] = $aCounts[$aAttachment['order_item_id']][0];
				$aItem['lp']=$iLp++;
				$aProducts2['items'][] = $aItem;
				}
			
			}*/
		
		$aTransports = $this->getOrdersTransports($sPaymentType, $iOrderId);
		//dump($aTransports);
		
		if(!empty($aTransports)){
			foreach($aTransports as $iKey=>$aTransport){
				$aProducts2['total_val_brutto'] += $aTransport['transport_cost']*$aTransport['quantity'];
				$aProducts2['total_price_netto'] += Common::formatPrice2($aTransport['transport_cost']/(1.0+($aTransport['transport_vat']/100.0)))*$aTransport['quantity'];
				$aProducts2['total_price_brutto'] += $aTransport['transport_cost'];
				$aProducts2['total_promo_price_brutto'] += $aTransport['transport_cost'];
				$aProducts2['total_quantity'] += $aTransport['quantity'];
				$aItem = array();
				$aItem['price_brutto'] = Common::formatPrice($aTransport['transport_cost']);
				$aItem['price_netto'] = Common::formatPrice(($aTransport['transport_cost']/(1.0+($aTransport['transport_vat']/100.0)))*$aTransport['quantity']);
				$aItem['discount'] = '0,00';
				$aItem['vat'] = round($aTransport['transport_vat'],0);//Common::formatPrice($aTransport['transport_vat']);
				$aItem['promo_price_brutto'] = Common::formatPrice($aTransport['transport_cost']);
				$aItem['val_brutto'] = Common::formatPrice($aTransport['transport_cost']*$aTransport['quantity']);
				$aItem['name'] = $aTransport['transport'];
				$aItem['quantity'] = $aTransport['quantity'];
				$aProducts2['items'][] = $aItem;
			}
		}
		
		//$aProducts['total'] = $fTotalValBrutto;
		$aProducts2['total_val_brutto'] = Common::formatPrice($aProducts2['total_val_brutto']);
		$aProducts2['total_price_netto'] = Common::formatPrice($aProducts2['total_price_netto']);
		$aProducts2['total_price_brutto'] = Common::formatPrice($aProducts2['total_price_brutto']);
		$aProducts2['total_promo_price_brutto'] = Common::formatPrice($aProducts2['total_promo_price_brutto']);
		
		if($sPaymentType!='bank_14days') {
			$aVals2=$this->getOrdersReport($sPaymentType);
			$aProducts2['total_price_netto']=$aVals2['total_netto'];
			}
		return $aProducts2;
	} // end of getProductOrdersReport() method 
	
	function getProductListHtml(&$aProducts){
		global $aConfig,$pSmarty;
		$aModule = array();
		$aModule['lang'] =& $aConfig['lang'][$this->sModule];
		
		$aModule['products'] =& $aProducts;
		
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'products_table.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	} // end of getProductListHtml() method
	

	function &getVatOrders(){
	global $aConfig;
		$sSql = "SELECT *
							FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE payment_type = 'bank_14days'  AND
							 			 invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			 invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'";
		return Common::GetAll($sSql);
	} // end of getVatOrders() method

	function &getOrdersPayments($sPaymentType){
		global $aConfig;
		$sPaymentSql = '';
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(payment_type = 'platnoscipl' OR payment_type = 'card' OR payment_type = 'dotpay')";
		}
		else {
			$sPaymentSql = "payment_type = '".$sPaymentType."'";
		}
		$sSql = "SELECT DATE(payment_date) payment_date, DATE_FORMAT(payment_date, '%Y-%m-%d') payment_date2, SUM(paid_amount) paid_amount
							FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE ".$sPaymentSql." AND
							 			 invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			 invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'
							 GROUP BY payment_date2";
		$aPayments = Common::GetAll($sSql);
		foreach($aPayments as $iKey=>$aPayment){
			$aPayments[$iKey]['payment_date'] = formatDateClient($aPayment['payment_date']);
			$aPayments[$iKey]['paid_amount'] = Common::formatPrice($aPayment['paid_amount']);
		}
		return $aPayments;
	} // end of getOrdersPayments() method 
	
	function &getOrdersTransports($sPaymentType, $iOrderId=0){
		global $aConfig;
		$sPaymentSql = '';
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay')";
		}
		else {
			$sPaymentSql = "A.payment_type = '".$sPaymentType."'";
		}
		$sSql = "SELECT A.transport_id, A.transport_cost,A.transport, A.transport_vat, count(A.id) as quantity 
							FROM ".$aConfig['tabls']['prefix']."orders A
							LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
							ON B.id=A.transport_id 
							WHERE B.personal_reception <> '1' AND
										".$sPaymentSql." AND
							 			A.invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			A.invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'"
							 			.($iOrderId>0?" AND A.id = ".$iOrderId:'').
							" GROUP BY A.transport_vat, A.transport_id, A.transport_cost";
		return Common::GetAll($sSql);
	} // end of getOrdersTransports() method 


	function &getOrderInvoiceAddress($iId){
		global $aConfig;
		
		$sSql = "SELECT is_company, company, name, surname, street, number, number2, postal, city, nip, phone
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
						 " AND 
						 address_type = '0'
						  ORDER BY address_type"; 
		return Common::GetRow($sSql);
	} // end of getOrderInvoiceAddress() method
	
	
	function &getOrdersReport($sPaymentType){
		global $aConfig;
		$sPaymentSql = '';
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay')";
		}
		else {
			$sPaymentSql = "A.payment_type = '".$sPaymentType."'";
		}
		$sSql = "SELECT id, second_invoice
							FROM ".$aConfig['tabls']['prefix']."orders A
							 WHERE ".$sPaymentSql."  AND
							 			 A.invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			 A.invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'";

		$aOrders =& Common::GetAll($sSql);
		$aTable= array();
		$iLp=1;
		if(!empty($aOrders)){
			$sSql='SELECT value FROM '.$aConfig['tabls']['prefix'].'products_vat_stakes WHERE 1';
			$aVatStakesT=Common::GetAll($sSql);
			$aVatStakes=array();
			//przepisanie do tablicy
			foreach($aVatStakesT as $aVat)
				$aVatStakes[]=$aVat['value'];
				
			foreach($aOrders as $iKey=>$aOrder) {
				$aItem = $this->getOrderVat($aOrder['id'],false,$iLp++);
				$aTable['items'][] = $aItem;
				//sprawdzenie czy są jakieś inne stawki
				foreach($aItem['vat'] as $iOVats=>$aVaVa)
					if(!in_array($iOVats,$aVatStakes))
						$aVatStakes[]=$iOVats;
						
				foreach($aVatStakes as $iKey => $aVatItem) {
					$aTable['vat_'.$aVatItem.'_brutto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_brutto']);
					$aTable['vat_'.$aVatItem.'_netto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_netto']);
					//$aTable['vat_7_brutto'] += Common::formatPrice2($aItem['vat'][7]['value_brutto']);
					//$aTable['vat_22_brutto'] += Common::formatPrice2($aItem['vat'][22]['value_brutto']);
					}
					
				if($aOrder['second_invoice'] == '1') {
					$aItem = $this->getOrderVat($aOrder['id'],true,$iLp++);
					$aTable['items'][] = $aItem;
					foreach($aVatStakes as $iKey => $aVatItem) { 
						$aTable['vat_'.$aVatItem.'_brutto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_brutto']);
					$aTable['vat_'.$aVatItem.'_netto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_netto']);
						//$aTable['vat_7_brutto'] += Common::formatPrice2($aItem['vat'][7]['value_brutto']);
						//$aTable['vat_22_brutto'] += Common::formatPrice2($aItem['vat'][22]['value_brutto']);
					}
				}
			}
		}
		foreach($aVatStakes as $iKey => $aVatItem) {
			// przeliczanie dla stawki VAT 0 - sformatowane z '.' jako separator dziesietny
			$aTable['vat_'.$aVatItem.'_brutto'] = Common::formatPrice2($aTable['vat_'.$aVatItem.'_brutto']);
			$aTable['vat_'.$aVatItem.'_netto'] =  Common::FormatPrice2($aTable['vat_'.$aVatItem.'_brutto']/(1+$aVatItem/100));
			//$aTable['vat_'.$aVatItem.'_netto'] = Common::formatPrice2($aTable['vat_'.$aVatItem.'_brutto'] / (1 + $aVatItem/100));
			$aTable['vat_'.$aVatItem.'_currency'] = $aTable['vat_'.$aVatItem.'_brutto'] - $aTable['vat_'.$aVatItem.'_netto'];
			$aTable['total_brutto'] += $aTable['vat_'.$aVatItem.'_brutto'];
			$aTable['total_netto'] += $aTable['vat_'.$aVatItem.'_netto'];
			$aTable['total_vat'] += $aTable['vat_'.$aVatItem.'_currency'];
		
			/*// przeliczanie dla stawki VAT 7 - sformatowane z '.' jako separator dziesietny
			$aTable['vat_7_brutto'] = Common::formatPrice2($aTable['vat_7_brutto']);
			$aTable['vat_7_netto'] = Common::formatPrice2($aTable['vat_7_brutto'] / (1 + 7/100));
			$aTable['vat_7_currency'] = $aTable['vat_7_brutto'] - $aTable['vat_7_netto'];
			// przeliczanie dla stawki VAT 22 - sformatowane z '.' jako separator dziesietny
			$aTable['vat_22_brutto'] = Common::formatPrice2($aTable['vat_22_brutto']);
			$aTable['vat_22_netto'] = Common::formatPrice2($aTable['vat_22_brutto'] / (1 + 22/100));
			$aTable['vat_22_currency'] = $aTable['vat_22_brutto'] - $aTable['vat_22_netto'];*/
			}
		// obliczanie sumy zamowienia - sformatowanie z ',' jako separator dziesietny
		$aTable['total_brutto'] = Common::formatPrice($aTable['total_brutto']);
		$aTable['total_netto'] = Common::formatPrice($aTable['total_netto']);
		$aTable['total_vat'] = Common::formatPrice($aTable['total_vat']);
		foreach($aVatStakes as $iKey => $aVatItem) {
			// formatowanie wartosci stawki VAT 0 z ',' jako separator dziesietny
			$aTable['vat_'.$aVatItem.'_value'] = Common::formatPrice($aTable['vat_'.$aVatItem.'_netto']);
			$aTable['vat_'.$aVatItem.'_currency'] = Common::formatPrice($aTable['vat_'.$aVatItem.'_currency']);
			$aTable['show_vat_'.intval($aVatItem)]=true;
		}
		/*// formatowanie wartosci stawki VAT 7 z ',' jako separator dziesietny
		$aTable['vat_7_value'] = Common::formatPrice($aTable['vat_7_netto']);
		$aTable['vat_7_currency'] = Common::formatPrice($aTable['vat_7_currency']);
		// formatowanie wartosci stawki VAT 22 z ',' jako separator dziesietny
		$aTable['vat_22_value'] = Common::formatPrice($aTable['vat_22_netto']);
		$aTable['vat_22_currency'] = Common::formatPrice($aTable['vat_22_currency']);*/
		
		return $aTable;
	} // end of getOrdersReport() method 
	
	
	function getOrderVat($iId,$bSecondInvoice=false,$iLp){
	global $aConfig,$pSmarty;
	
		$aOrder = array();
		$aOrder['lp'] = $iLp;
		
		$sSql = "SELECT A.*, DATE(order_date) order_date, DATE(invoice_date) invoice_date, DATE(status_3_update) status_3_update, (A.transport_cost + A.value_brutto) as total_value_brutto, B.personal_reception
						 FROM ".$aConfig['tabls']['prefix']."orders A
						LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						ON B.id=A.transport_id
						 WHERE A.id = ".$iId;
		$aModule['order'] =& Common::GetRow($sSql);
		$bEnabledSecondInvoice = ($aModule['order']['second_invoice'] == '1');
	
		$sSql = "SELECT A.*, B.quantity AS parent_quantity
						 FROM ".$aConfig['tabls']['prefix']."orders_items A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."orders_items B
						 ON B.id=A.parent_id
						 WHERE A.order_id = ".$iId."
						 AND A.packet='0'
						 AND A.deleted = '0'".
						($bEnabledSecondInvoice?($bSecondInvoice?" AND A.second_invoice='1'":" AND A.second_invoice='0'"):'');
		$aModule['items'] =& Common::GetAll($sSql);
		
		
		

		if($bSecondInvoice){
			$aOrder['invoice_number'] = $aModule['order']['invoice2_id'];
			$aAddress = $this->getOrderInvoiceAddress($aModule['order']['id'], true);
		} else {
			$aOrder['invoice_number'] = $aModule['order']['invoice_id'];
			$aAddress = $this->getOrderInvoiceAddress($aModule['order']['id']);
		}
		
		$aOrder['order_date'] = formatDateClient($aModule['order']['order_date']);
		$aOrder['invoice_date'] = formatDateClient($aModule['order']['invoice_date']);

		if($aAddress['is_company'] == '1'){
			$aOrder['name'] = $aAddress['company'];
			$aOrder['nip'] = $aAddress['nip'];
		} else {
			$aOrder['name'] = $aAddress['surname'].' '.$aAddress['name'];
			$aOrder['nip'] = '&nbsp;';
		}
		
		
		$aOrder['value_netto'] = 0;
		$aOrder['value_brutto'] = 0;
		$i=1;
		
		$aOrder['vat'][0] = array();
		$aOrder['vat'][7] = array();
		$aOrder['vat'][22] = array();
		foreach ($aModule['items'] as $iKey => $aItem){
			if($aItem['promo_price_brutto'] == 0){
					$aItem['promo_price_brutto'] = $aItem['price_brutto'];
			}

			
			if($aItem['item_type'] == 'P'){
				$aItem['quantity'] = $aItem['parent_quantity'];
			}
			
			$aOrder['vat'][$aItem['vat']]['quantity'] += $aItem['quantity'];
			// produkt ma zalacznik - dorzucamy dodatkowy wiersz i uwzgledniamy ceny zalacznika na fakturze
			if ($aItem['attachments'] == '1') {
				// produkt posiada zalaczniki - pobranie ich
				$sSql = "SELECT *
								 FROM ".$aConfig['tabls']['prefix']."orders_items_attachments
								 WHERE order_id = ".$iId." AND
								 			 order_item_id = ".$aItem['id'];
				$aAttach =& Common::GetRow($sSql);
				// składowa ceny - załącznik jako osobna pozycja na fakturze
				$aAttach['promo_price_brutto'] =  Common::formatPrice2($aAttach['price_brutto'] - $aAttach['price_brutto']*($aItem['discount']/100));
				$aAttach['value_brutto'] =  Common::formatPrice2($aItem['quantity'] * $aAttach['promo_price_brutto']);
				$aAttach['value_netto'] =  Common::formatPrice2($aItem['quantity'] * ($aAttach['promo_price_brutto'] / (1 + $aAttach['vat']/100)));
				$aAttach['vat_currency'] =  Common::formatPrice2($aAttach['value_brutto'] - $aAttach['value_netto']);
				$aAttach['discount'] = $aItem['discount'];
				$aAttach['quantity'] = $aItem['quantity'];
				
				// dodawanie wartości załącznika do sumy vat dla wartości odpowiedniej dla zalacznika
				$aOrder['vat'][$aAttach['vat']]['quantity'] += $aAttach['quantity'];
				$aOrder['vat'][$aAttach['vat']]['vat_currency'] += $aAttach['vat_currency'];
				$aOrder['vat'][$aAttach['vat']]['value_netto'] += $aAttach['value_netto'];
				$aOrder['vat'][$aAttach['vat']]['value_brutto'] += $aAttach['value_brutto'];
				// vat calkowity
				$aOrder['order_vat'] += $aAttach['vat_currency'];
				// suma netto zamowienia
				$aOrder['value_netto'] += $aAttach['value_netto'];
				$aOrder['value_brutto'] += $aAttach['value_brutto'];
				
				// składowa ceny - produkt jako pozycja na fakturze
				$aItem['price_brutto'] = Common::formatPrice2($aItem['price_brutto'] - $aAttach['price_brutto']);
				$aItem['promo_price_brutto'] = Common::formatPrice2($aItem['promo_price_brutto'] - $aAttach['promo_price_brutto']);
				$aItem['value_brutto'] =  Common::formatPrice2($aItem['quantity'] * $aItem['promo_price_brutto']);
				$aItem['value_netto'] = Common::formatPrice2($aItem['quantity'] * ($aItem['promo_price_brutto'] / (1 + $aItem['vat']/100)));
				$aItem['vat_currency'] =  Common::formatPrice2($aItem['value_brutto'] - $aItem['value_netto']);
				// dodawanie produktu do sumy vat dla wartości odpowiedniej
				$aOrder['vat'][$aItem['vat']]['vat_currency'] += $aItem['vat_currency'];
				$aOrder['vat'][$aItem['vat']]['value_netto'] += $aItem['value_netto'];
				$aOrder['vat'][$aItem['vat']]['value_brutto'] += $aItem['value_brutto'];
				// vat calkowity
				$aOrder['order_vat'] += $aItem['vat_currency'];
				// suma netto zamowienia
				$aOrder['value_netto'] += $aItem['value_netto'];
				$aOrder['value_brutto'] += $aItem['value_brutto'];
			} else {
				// produkt zwykly
				$aItem['vat_currency'] =  Common::formatPrice2($aItem['value_brutto'] - $aItem['value_netto']);
				// dodawanie produktu do sumy vat dla wartości odpowiedniej
				$aOrder['vat'][$aItem['vat']]['vat_currency'] += $aItem['vat_currency'];
				$aOrder['vat'][$aItem['vat']]['value_netto'] += $aItem['value_netto'];
				$aOrder['vat'][$aItem['vat']]['value_brutto'] += $aItem['value_brutto'];
				// vat calkowity
				$aOrder['order_vat'] += $aItem['vat_currency'];
				// suma netto zamowienia
				$aOrder['value_netto'] += $aItem['value_netto'];
				$aOrder['value_brutto'] += $aItem['value_brutto'];
			}
			
		}
		// dodanie kuriera do pozycji zamówienia
		if($aModule['order']['personal_reception'] == '0' && !$bSecondInvoice){
			$aItem = array();
			$aItem['discount'] = 0;
			$aItem['quantity'] = 1;
			$aItem['price_brutto'] = $aModule['order']['transport_cost'];
			$aItem['promo_price_brutto'] = $aItem['price_brutto'];
			$aItem['value_brutto'] = $aItem['price_brutto'];
			$aItem['vat'] = $aModule['order']['transport_vat'];//$this->getPaymentVat($aModule['order']['payment_id']);
			$aItem['value_netto'] =  Common::formatPrice2($aItem['price_brutto'] / (1 + $aItem['vat']/100));
			$aItem['vat_currency'] =  Common::formatPrice2($aItem['value_brutto'] - $aItem['value_netto']);
			// dodawanie transportu do sumy vat dla wartości odpowiedniej
			$aOrder['vat'][$aItem['vat']]['vat_currency'] += Common::formatPrice2($aItem['vat_currency']);
			$aOrder['vat'][$aItem['vat']]['value_netto'] += Common::formatPrice2($aItem['value_netto']);
			$aOrder['vat'][$aItem['vat']]['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
			// vat calkowity
			$aOrder['order_vat'] += Common::formatPrice2($aItem['vat_currency']);
			// dodanie kosztu transportu do calk wartosci zamowienia
			// suma netto zamowienia
			$aOrder['value_netto'] += Common::formatPrice2($aItem['value_netto']);
			$aOrder['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
			
		}
		
		$aOrder['value_netto'] = Common::formatPrice($aOrder['value_netto']);
		$aOrder['value_brutto'] = Common::formatPrice($aOrder['value_brutto']);
		$aOrder['order_vat'] = Common::formatPrice($aOrder['order_vat']);
		$aOrder['total_value_brutto'] = Common::formatPrice($aOrder['total_value_brutto']);
		foreach($aOrder['vat'] as $iVat=>$aVat){
			$aOrder['vat'][$iVat]['vat_currency'] = Common::formatPrice($aOrder['vat'][$iVat]['vat_currency']);
			$aOrder['vat'][$iVat]['value_netto'] = Common::formatPrice($aOrder['vat'][$iVat]['value_netto']);
			$aOrder['vat'][$iVat]['value_brutto'] = Common::formatPrice($aOrder['vat'][$iVat]['value_brutto']);
		}
		
		return $aOrder;
	} // end of getInvoiceHtml() method
	
} // end of Module Class
?>