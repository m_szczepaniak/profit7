<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - sposoby transportu
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Module
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID strony zamowien
    var $iPageId;

    // ID wersji jezykowej
    var $iLangId;

    /**
     *
     * @var \table\keyValue
     */
    private $oKeyValue;

    /**
     * Konstruktor klasy
     *
     * @return    void
     */
    function Module(&$pSmarty)
    {
        global $aConfig;

        $this->sErrMsg = '';
        $this->iPageId = getModulePageId('m_zamowienia');
        $this->sModule = $_GET['module'];
        $this->iLangId = $_SESSION['lang']['id'];

        $sDo = '';
        $iId = 0;


        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        if (isset($_GET['id'])) {
            $iId = $_GET['id'];
        }

        $this->oKeyValue = new \table\keyValue('orders_transport_means');

        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu i akcji
        //if ($_SESSION['user']['type'] != 1) {
        // brak uprawnien do modulu, menu, strony
        //	showPrivsAlert($pSmarty);
        //	return;
        //}

        switch ($sDo) {
            case 'delete':
                $this->Delete($pSmarty, $iId);
                break;
            case 'insert':
                $this->Insert($pSmarty);
                break;
            case 'update':
                $this->Update($pSmarty, $iId);
                break;
            case 'change_ptype':
            case 'edit':
            case 'add':
                $this->AddEdit($pSmarty, $iId);
                break;
            case 'sort':
                // dolaczenie pliku klasy MySort
                include_once('Sort.class.php');
                // obiekt klasy MySort
                $oSort = new MySort();
                // utwozenie widoku do sortowania
                $pSmarty->assign('sContent',
                    $oSort->Show(getPagePath($iId,
                        $this->iLangId, true),
                        $this->getRecords(),
                        phpSelf(array('do' => 'proceed_sort'),
                            array(), false)));
                break;
            case 'proceed_sort':
                // dolaczenie pliku klasy MySort
                include_once('Sort.class.php');
                // obiekt klasy MySort
                $oSort = new MySort();
                // sortowanie rekordow
                $oSort->Proceed($aConfig['tabls']['prefix'] . "orders_transport_means",
                    $this->getRecords(),
                    array(),
                    'order_by');
                break;
            case 'showExcluded':
                $this->showExcluded($pSmarty, $iId);
                break;
            case 'insertExcluded':
                $this->insertExcluded($pSmarty , $iId);
                break;
            case 'removeExcluded':
                $this->removeExcluded($pSmarty,$id);
                break;
            default:
                $this->Show($pSmarty);
                break;
        }
    } // end of Module() method


    /**
     * Metoda pobiera liste rekordow
     *
     * @param    integer $iPId - Id strony
     * @return    array ref    - lista rekordow
     */
    function &getRecords()
    {
        global $aConfig;

        // pobranie wszystkich akapitów dla danej strony
        $sSql = "SELECT id, name, order_by
						 FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means
						 ORDER BY order_by";
        $aRecords =& Common::GetAssoc($sSql);
        foreach ($aRecords as $iKey => $aRecord) {
            $aRecords[$iKey] = $aRecord['name'];
        }
        return $aRecords;
    } // end of getRecords() method


    /**
     * Metoda wyswietla liste form platnosci
     *
     * @param    object $pSmarty
     * @param    integer $iPId - Id strony
     * @return    void
     */
    function Show(&$pSmarty)
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header' => $aConfig['lang'][$this->sModule]['list'],
            'refresh' => true,
            'search' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'content' => '&nbsp;',
                'sortable' => false
            ),
            array(
                'db_field' => 'name',
                'content' => $aConfig['lang'][$this->sModule]['list_name'],
                'sortable' => false
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '40'
            )
        );

        // pobranie liczby wszystkich rekordow
        $sSql = "SELECT COUNT(id)
						 FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means
						 ORDER BY id";
        $iRowCount = intval(Common::GetOne($sSql));

        $pView = new View('transport_means', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);

        if ($iRowCount > 0) {
            // dodanie Pagera do widoku
            $iCurrentPage = $pView->AddPager($iRowCount);
            $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
            $iStartFrom = ($iCurrentPage - 1) * $iPerPage;

            // pobranie wszystkich rekordow
            $sSql = "SELECT id, name
						 	 FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means
							 ORDER BY order_by
							 LIMIT " . $iStartFrom . ", " . $iPerPage;
            $aRecords =& Common::GetAll($sSql);


            // ustawienia dla poszczegolnych kolumn rekordu
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'name' => array(
                    'link' => phpSelf(array('do' => 'edit', 'id' => '{id}'))
                ),
                'action' => array(
                    'actions' => array('edit', 'delete', 'showExcluded'),
                    'params' => array(
                        'edit' => array('id' => '{id}'),
                        'delete' => array('id' => '{id}'),
                        'showExcluded' => array('id' => '{id}')
                    ),
                    'icon' => array(
                        'showExcluded' => 'books'
                    )
                ),
            );

            // dodanie rekordow do widoku
            $pView->AddRecords($aRecords, $aColSettings);
        }
        // przyciski stopki stopki do widoku
        $aRecordsFooter = array(
            array('check_all', 'delete_all'),
            array('sort', 'add')
        );
        $pView->AddRecordsFooter($aRecordsFooter);

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') .
            $pView->Show());
    } // end of Show() function


    /**
     * Metoda usuwa wybrane rekordy
     *
     * @param    object $pSmarty
     * @param    integer $iId - Id usuwanego rekordu
     * @return    void
     */
    function Delete(&$pSmarty, $iId)
    {
        global $aConfig;
        $bIsErr = false;

        if ($iId > 0) {
            $_POST['delete'][$iId] = '1';
        }
        $iI = 0;
        foreach ($_POST['delete'] as $sKey => $sVal) {
            unset($_POST['delete'][$sKey]);
            $_POST['delete'][$iI] = $sKey;
            $iI++;
        }
        if (!empty($_POST['delete'])) {
            if (count($_POST['delete']) == 1) {
                $sErrPostfix = '0';
            } else {
                $sErrPostfix = '1';
            }

            // pobranie nazw usuwanych rekordow
            $sDel = $this->getItemsNames($_POST['delete']);

            // usuwanie
            $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means
							 WHERE id IN (" . implode(',', $_POST['delete']) . ")";
            if (Common::Query($sSql) === false) {
                $bIsErr = true;
            }

            if (!$bIsErr) {
                $sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_' . $sErrPostfix],
                    $sDel);
                $this->sMsg = GetMessage($sMsg, false);
                // dodanie informacji do logow
                AddLog($sMsg, false);
            } else {
                $sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_' . $sErrPostfix],
                    $sDel);
                $this->sMsg = GetMessage($sMsg);
                // dodanie informacji do logow
                AddLog($sMsg);
            }
        }
        $this->Show($pSmarty);
    } // end of Delete() funciton


    /**
     * Metoda dodaje do bazy danych nowy rekord
     *
     * @param    object $pSmarty
     * @return    void
     */
    function Insert(&$pSmarty)
    {
        global $aConfig;
        $bIsErr = false;

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty);
            return;
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (strtotime($_POST['start_date'] . ' ' . $_POST['start_time']) >= strtotime($_POST['end_date'] . ' ' . $_POST['end_time'])) {
            $oValidator->sError .= '<li>' . $aConfig['lang'][$this->sModule]['edit_err_1'] . '</li>';
        }
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = Common::GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEdit($pSmarty);
            return;
        }

        $aValues = array(
            'name' => decodeString($_POST['name']),
            'symbol' => $_POST['symbol'],
            'personal_reception' => isset($_POST['personal_reception']) ? '1' : '0',
            'max_weight' => Common::formatPrice2($_POST['max_weight']),
            'max_order_weight' => Common::formatPrice2($_POST['max_order_weight']),
            'no_costs_from' => Common::formatPrice2($_POST['no_costs_from']) > 0 ? Common::formatPrice2($_POST['no_costs_from']) : 'NULL',
            'date_no_costs_from' => Common::formatPrice2($_POST['date_no_costs_from']) > 0 ? Common::formatPrice2($_POST['date_no_costs_from']) : 'NULL',
            'date_start_date' => FormatDateHour($_POST['start_date'], $_POST['start_time']),
            'date_end_date' => FormatDateHour($_POST['end_date'], $_POST['end_time']),
            'mail_info' => nl2br(trim($_POST['mail_info'])),
            'limit_to_expand_shipping_date' => $_POST['limit_to_expand_shipping_date']
        );


        if ($_FILES['logo']['name'] != '') {
            // dolaczenie klasy ProductImage
            include_once('Image.class.php');
            $oImage = new Image();
            $aSettings = array();
            $aSettings['small_size'] = $aConfig['common']['transport_logos_size'];

            // przetwarzanie zdjecia
            $oImage->SetImage($aConfig['common']['client_base_path'] . $aConfig['common']['transport_logos_dir'], $_FILES['logo'], $aSettings);
            $oImage->proceedImage();
            $aValues['logo'] = $oImage->GetFileProperty('final_name');
        }

        // dodanie
        if (Common::Insert($aConfig['tabls']['prefix'] . "orders_transport_means", $aValues, '', false) === false) {
            $bIsErr = true;
        }

        if (!$bIsErr) {
            // rekord zostal dodany
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
                $_POST['name']);
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            $this->Show($pSmarty);
        } else {
            // rekord nie zostal dodany,
            // wyswietlenie komunikatu o niepowodzeniu
            // oraz ponowne wyswietlenie formularza dodawania
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
                $aConfig['lang'][$this->sModule]['ptype_' . $_POST['ptype']]);
            $this->sMsg = GetMessage($sMsg);

            AddLog($sMsg);
            $this->AddEdit($pSmarty);
        }

    } // end of Insert() funciton


    /**
     * Metoda aktualizuje w bazie danych krekord
     *
     * @param        object $pSmarty
     * @param    integer $iId - Id aktualizowanego rekordu
     * @return    void
     */
    function Update(&$pSmarty, $iId)
    {
        global $aConfig;
        $bIsErr = false;

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty);
            return;
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if ($_POST['start_date'] != '' || $_POST['start_time'] != '') {
            if (strtotime($_POST['start_date'] . ' ' . $_POST['start_time']) >= strtotime($_POST['end_date'] . ' ' . $_POST['end_time'])) {
                $oValidator->sError .= '<li>' . $aConfig['lang'][$this->sModule]['edit_err_1'] . '</li>';
            }
        }
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEdit($pSmarty, $iId);
            return;
        }

        if (isset($_POST['time_border']) && intval($_POST['time_border'])) {
            $timeBorder = intval($_POST['time_border']);
            $values = [
                'time_border' => $timeBorder
            ];
            \Common::Update('orders_payment_types', $values, ' transport_mean = ' . $iId);
        }

        $aValues = array(
            'name' => decodeString($_POST['name']),
            'symbol' => $_POST['symbol'],
            'personal_reception' => isset($_POST['personal_reception']) ? '1' : '0',
            'max_weight' => Common::formatPrice2($_POST['max_weight']),
            'max_order_weight' => Common::formatPrice2($_POST['max_order_weight']),
            'no_costs_from' => Common::formatPrice2($_POST['no_costs_from']) > 0 ? Common::formatPrice2($_POST['no_costs_from']) : 'NULL',
            'date_no_costs_from' => Common::formatPrice2($_POST['date_no_costs_from']) > 0 ? Common::formatPrice2($_POST['date_no_costs_from']) : 'NULL',
            'date_start_date' => $_POST['start_date'] != '' ? FormatDateHour($_POST['start_date'], $_POST['start_time']) : 'NULL',
            'date_end_date' => $_POST['end_date'] != '' ? FormatDateHour($_POST['end_date'], $_POST['end_time']) : 'NULL',
            'mail_info' => nl2br(trim($_POST['mail_info'])),
            'border_time_deadline' => $_POST['border_time_deadline'],
            'limit_to_expand_shipping_date' => $_POST['limit_to_expand_shipping_date']
        );

        if ($_FILES['logo']['name'] != '') {

            // dolaczenie klasy ProductImage
            include_once('Image.class.php');
            $oImage = new Image();
            $aSettings = array();
            $aSettings['small_size'] = $aConfig['common']['transport_logos_size'];
            // przetwarzanie zdjecia
            $oImage->SetImage($aConfig['common']['client_base_path'] . $aConfig['common']['transport_logos_dir'], $_FILES['logo'], $aSettings);
            if (($iRes = $oImage->proceedImage()) != 1) {
                $bIsErr = true;
            } else {
                $aValues['logo'] = $oImage->GetFileProperty('final_name');

                if (!empty($_POST['old_logo']) && !empty($aValues['logo'])) {
                    @unlink($aConfig['common']['client_base_path'] . $aConfig['common']['transport_logos_dir'] . '/' . $_POST['old_logo']);
                }
            }
        }

        if (Common::Update($aConfig['tabls']['prefix'] . "orders_transport_means", $aValues, "id = " . $iId) === false) {
            $bIsErr = true;
        }

        if ($this->oKeyValue->insertUpdateByPost($iId, $_POST) === false) {
            $bIsErr = true;
        }

        if (!$bIsErr) {
            // rekord zostal zaktualizowany
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
                $_POST['name']);
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            $this->Show($pSmarty);
        } else {
            // rekord nie zostal zmieniony,
            // wyswietlenie komunikatu o niepowodzeniu
            // oraz ponowne wyswietlenie formularza edycji
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
                $_POST['name']);
            $this->sMsg = GetMessage($sMsg);

            AddLog($sMsg);
            $this->AddEdit($pSmarty, $iId);
        }

    } // end of Update() funciton


    /**
     * Metoda tworzy formularz dodawania / edycji
     *
     * @param        object $pSmarty
     * @param        integer $iId - ID edytowanego rekordu
     */
    function AddEdit(&$pSmarty, $iId = 0)
    {
        global $aConfig;

        $aData = array();
        $sHtml = '';

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        if ($iId > 0) {
            // pobranie z bazy danych edytowanego rekordu
            $sSql = "SELECT name, symbol, logo, logo AS old_logo, personal_reception, max_weight, mail_info, no_costs_from, date_no_costs_from,
											DATE_FORMAT(date_start_date, '" . $aConfig['common']['cal_sql_date_format'] . "') AS start_date, 
											DATE_FORMAT(date_start_date, '" . $aConfig['common']['sql_hour_format'] . "') AS start_time,
											DATE_FORMAT(date_end_date, '" . $aConfig['common']['cal_sql_date_format'] . "') AS end_date,
											DATE_FORMAT(date_end_date, '" . $aConfig['common']['sql_hour_format'] . "') AS end_time,
                      border_time_deadline, max_order_weight , limit_to_expand_shipping_date
							 FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means
							 WHERE id = " . $iId;
            $aData =& Common::GetRow($sSql);
            $aData[\table\keyValue::POST_KEY_VALUE_FIELD] = $this->oKeyValue->getAllByIdDest($iId);
        }

        if (!empty($_POST)) {
            $aData =& $_POST;
            $_SESSION['_settings'][$this->sModule]['synchronize'] = !empty($aData['synchronize']);
        }

        if (!isset($aData['synchronize'])) {
            if (isset($_SESSION['_settings'][$this->sModule]['synchronize'])) {
                $aData['synchronize'] = intval($_SESSION['_settings'][$this->sModule]['synchronize']);
            } else {
                $aData['synchronize'] = '0';
            }
        }

        $sHeader = $aConfig['lang'][$this->sModule]['header_' . ($iId > 0 ? 1 : 0)];
        if ($iId > 0 && isset($aData['name']) && !empty($aData['name'])) {
            $sHeader .= ' "' . $aData['name'] . '"';
        }

        $pForm = new FormTable('transport_means', $sHeader, array('action' => phpSelf(array('id' => $iId)), 'enctype' => 'multipart/form-data'), array('col_width' => 200), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));

        // nazwa
        $pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength' => 64, 'style' => 'width: 250px;', 'onkeyup' => 'synchronizeSymbol(document.getElementById(\'synchronize\').checked);'), '', 'text');

        // symbol
        $sLabel = $pForm->GetLabelHTML('symbol', $aConfig['lang'][$this->sModule]['symbol']);
        $sInput = $pForm->GetTextHTML('symbol', $aConfig['lang'][$this->sModule]['symbol'], $aData['symbol'], array('style' => 'width: 350px;', 'maxlength' => 255), '', 'text', true, '/^[a-z0-9_-]{3,128}$/') .
            '&nbsp;&nbsp;' . $aConfig['lang'][$this->sModule]['synchronize'] . '&nbsp;&nbsp;' .
            $pForm->GetCheckBoxHTML('synchronize', '', array(), '', !empty($aData['synchronize']), false) .
            '&nbsp;&nbsp;<a href="javascript: void(0);" onclick="synchronizeSymbol(true);" title="' . $aConfig['lang'][$this->sModule]['synchronize_now'] . '"><img src="gfx/icons/synchronize_ico.gif" alt="' . $aConfig['lang'][$this->sModule]['synchronize_now'] . '" border="0" align="absmiddle" /></a>';
        $pForm->AddRow($sLabel, $sInput);

        if (file_exists($aConfig['common']['client_base_path'] . $aConfig['common']['transport_logos_dir'] . '/' . $aData['logo'])) {
            $pForm->AddHidden('old_logo', $aData['logo']);
            $pForm->AddProductImage('logo', $aConfig['lang'][$this->sModule]['logo'], $aConfig['common']['transport_logos_dir'], $aData['logo']);
        } else {
            $pForm->AddProductImage('logo', $aConfig['lang'][$this->sModule]['logo'], $aConfig['common']['transport_logos_dir']);
        }

        // brak kosztow transportu od wartosci zamowienia
        $pForm->AddText('no_costs_from', $aConfig['lang'][$this->sModule]['no_costs_from'], Common::formatPrice3((double)$aData['no_costs_from']), array('maxlength' => 8, 'style' => 'width: 50px;'), '', 'float');

        $pForm->AddText('limit_to_expand_shipping_date', $aConfig['lang'][$this->sModule]['limit_to_expand_shipping_date'], Common::formatPrice3((double)$aData['limit_to_expand_shipping_date']), array('maxlength' => 8, 'style' => 'width: 50px;'), '', 'uint');

        $pForm->AddCheckBox('personal_reception', $aConfig['lang'][$this->sModule]['personal_reception'], array(), '', $aData['personal_reception'], false);
        $pForm->AddText('max_order_weight', _('Maksymalna waga zamówienia'), Common::formatPrice($aData['max_order_weight']), array('maxlength' => 8, 'style' => 'width: 60px;'), '', 'ufloat');
        $pForm->AddText('max_weight', $aConfig['lang'][$this->sModule]['max_weight'], Common::formatPrice($aData['max_weight']), array('maxlength' => 8, 'style' => 'width: 60px;'), '', 'ufloat');
        $pForm->AddTextArea('mail_info', $aConfig['lang'][$this->sModule]['mail_info'], br2nl($aData['mail_info']), array('style' => 'width: 350px;', 'rows' => 8), 1024);

        if ($iId > 0) {
            $timeBorder = \Common::GetOne('SELECT time_border FROM orders_payment_types WHERE transport_mean = ' . $iId . ' LIMIT 1');
            $pForm->AddText('time_border', _('Godzina graniczna'), intval($timeBorder), array('maxlength' => 2, 'style' => 'width: 60px;'), '', 'uint');
        }
        $pForm->AddText('border_time_deadline', _('Godzina graniczna, dla zamówień, które mają dzisiaj wyjechać<br />(mechanizm używany przy oznaczaniu przeterminowanych)'), intval($aData['border_time_deadline']), array('maxlength' => 2, 'style' => 'width: 60px;'), '', 'uint');

        if ($_SESSION['user']['type'] === 1) {
            $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[class_symbol]', _('Nazwa katalogu klas i prefix klas, obsługi metody transportu.'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['class_symbol'], array('maxlength' => 255, 'style' => 'width: 255px;'), '', 'text', false);
            $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[type]', _('Type'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['type'], array('maxlength' => 255, 'style' => 'width: 255px;'), '', 'text', false);
            $aTransportTypes = $this->getTransportTypesOptions();
            $pForm->AddSelect(\table\keyValue::POST_KEY_VALUE_FIELD . '[type]', _('Typ'), array(), $aTransportTypes, $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['type'], '', true);
        }
        switch ($aData['symbol']) {
            case 'poczta_polska':
                $pForm = $this->getPocztaPolskaFields($pForm, $aData);
                break;

            case 'poczta-polska-doreczenie-pod-adres':
                $pForm = $this->getPocztaPolskaFields($pForm, $aData);
                break;

            case 'ruch':
                $pForm = $this->getRuchFields($pForm, $aData);
                break;

            case 'orlen':
                $pForm = $this->getOrlenFields($pForm, $aData);
                break;

            case 'tba':
                $pForm = $this->getTBAFields($pForm, $aData);
                break;

            case 'paczkomaty_24_7':
                $pForm = $this->getPaczkomatyFields($pForm, $aData);
                break;
        }

        // przyciski
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_' . ($iId > 0 ? 1 : 0)]) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));


        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
    } // end of AddEdit() function


    /**
     *
     * @return array
     */
    private function getTransportTypesOptions()
    {
        $aTranspotTypes = array(
            array(
                'label' => _('Odbiór osobisty'),
                'value' => 'personal_reception'
            ),
            array(
                'label' => _('Odbiór w punkcie'),
                'value' => 'reception_at_point'
            ),
            array(
                'label' => _('Doręczenie pod adres'),
                'value' => 'delivery_to_address'
            )
        );
        return addDefaultValue($aTranspotTypes);
    }


    /**
     * @param \FormTable $pForm
     * @param array $aData
     * @return \FormTable
     */
    private function getRuchFields($pForm, $aData)
    {

        $pForm->AddMergedRow(_('Konfiguracja') . $aData['name'], array('class' => 'merged'));
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[login]', _('Login'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['login'], array('maxlength' => 255, 'style' => 'width: 200px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[password]', _('Hasło'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['password'], array('maxlength' => 255, 'style' => 'width: 200px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[api_url]', _('API URL'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['api_url'], array('maxlength' => 255, 'style' => 'width: 350px;'), '', 'text');

        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[city]', _('Miasto'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['city'], array('maxlength' => 255, 'style' => 'width: 350px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[postal]', _('Kod pocztowy'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['postal'], array('maxlength' => 255, 'style' => 'width: 150px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[street]', _('Ulica'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['street'], array('maxlength' => 255, 'style' => 'width: 250px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[number]', _('Budynek'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['number'], array('maxlength' => 255, 'style' => 'width: 50px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[number2]', _('Mieszkanie'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['number2'], array('maxlength' => 255, 'style' => 'width: 50px;'), '', 'text');

        return $pForm;
    }


    /**
     * @param \FormTable $pForm
     * @param array $aData
     * @return \FormTable
     */
    private function getPocztaPolskaFields(\FormTable $pForm, $aData)
    {

        $pForm->AddMergedRow(_('Konfiguracja ') . $aData['name'], array('class' => 'merged'));
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[point_of_giving]', _('Punkt nadania przesyłki'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['point_of_giving'], array('maxlength' => 255, 'style' => 'width: 50px;'), '', 'uint');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[login]', _('Login'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['login'], array('maxlength' => 255, 'style' => 'width: 200px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[password]', _('Hasło'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['password'], array('maxlength' => 255, 'style' => 'width: 200px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[api_url]', _('API URL'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['api_url'], array('maxlength' => 255, 'style' => 'width: 350px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[password_recipients]', _('Odbiorcy powiadomienia o automat. zmianie hasła'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['password_recipients'], array('maxlength' => 255, 'style' => 'width: 350px;'), '', 'text');
        return $pForm;
    }


    /**
     * @param \FormTable $pForm
     * @param array $aData
     * @return \FormTable
     */
    private function getTBAFields($pForm, $aData)
    {

        $pForm->AddMergedRow(_('Konfiguracja ') . $aData['name'], array('class' => 'merged'));

        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[login]', _('Login'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['login'], array('maxlength' => 255, 'style' => 'width: 200px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[password]', _('Hasło'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['password'], array('maxlength' => 255, 'style' => 'width: 200px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[client_id]', _('ID klienta'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['client_id'], array('maxlength' => 255, 'style' => 'width: 350px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[api_url]', _('API URL'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['api_url'], array('maxlength' => 255, 'style' => 'width: 350px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[COD_RetAccount]', _('Nr konta dla pobrań'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['COD_RetAccount'], array('maxlength' => 255, 'style' => 'width: 350px;'), '', 'text');


        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[city]', _('Miasto'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['city'], array('maxlength' => 255, 'style' => 'width: 350px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[postal]', _('Kod pocztowy'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['postal'], array('maxlength' => 255, 'style' => 'width: 150px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[street]', _('Ulica'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['street'], array('maxlength' => 255, 'style' => 'width: 250px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[number]', _('Budynek'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['number'], array('maxlength' => 255, 'style' => 'width: 50px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[number2]', _('Mieszkanie'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['number2'], array('maxlength' => 255, 'style' => 'width: 50px;'), '', 'text');

        return $pForm;
    }


    /**
     * @param \FormTable $pForm
     * @param array $aData
     * @return \FormTable
     */
    private function getOrlenFields($pForm, $aData)
    {

        $pForm->AddMergedRow(_('Konfiguracja ') . $aData['name'], array('class' => 'merged'));
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[login]', _('Login'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['login'], array('maxlength' => 255, 'style' => 'width: 200px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[password]', _('Hasło'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['password'], array('maxlength' => 255, 'style' => 'width: 200px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[api_url]', _('API URL'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['api_url'], array('maxlength' => 255, 'style' => 'width: 350px;'), '', 'text');

        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[city]', _('Miasto'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['city'], array('maxlength' => 255, 'style' => 'width: 350px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[postal]', _('Kod pocztowy'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['postal'], array('maxlength' => 255, 'style' => 'width: 150px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[street]', _('Ulica'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['street'], array('maxlength' => 255, 'style' => 'width: 250px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[number]', _('Budynek'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['number'], array('maxlength' => 255, 'style' => 'width: 50px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[number2]', _('Mieszkanie'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['number2'], array('maxlength' => 255, 'style' => 'width: 50px;'), '', 'text');

        return $pForm;
    }

    /**
     * @param \FormTable $pForm
     * @param array $aData
     * @return \FormTable
     */
    private function getPaczkomatyFields($pForm, $aData)
    {

        $pForm->AddMergedRow(_('Konfiguracja ') . $aData['name'], array('class' => 'merged'));
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[login]', _('Login'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['login'], array('maxlength' => 255, 'style' => 'width: 200px;'), '', 'text');
        $pForm->AddText(\table\keyValue::POST_KEY_VALUE_FIELD . '[password]', _('Hasło'), $aData[\table\keyValue::POST_KEY_VALUE_FIELD]['password'], array('maxlength' => 255, 'style' => 'width: 200px;'), '', 'text');

        return $pForm;
    }


    /**
     * Metoda zwraca liste srodków transportu o przekazanych do niej Id
     *
     * @param    array $aIds - Id rekordow dla ktorych zwrocone zostana nazwy
     * @return    string    - ciag z nazwami
     */
    function getItemsNames($aIds)
    {
        global $aConfig;
        $sList = '';

        // pobranie nazw usuwanych rekordow
        $sSql = "SELECT name
				 		 FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means
					 	 WHERE id IN  (" . implode(',', $aIds) . ")";
        $aRecords =& Common::GetAll($sSql);
        $sList = implode(',', $aRecords);
        return $sList;
    } // end of getItemsNames() method

    function showExcluded($pSmarty, $iId)
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');
        include_once('Form/FormTable.class.php');

        // zapamietanie opcji
        rememberViewState($this->sModule . '_add_dimensions');

        // pobranie liczby wszystkich rekordow
        $sSql = "SELECT name
							 FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means
								WHERE id = " . $iId;
        $sPromoName = Common::GetOne($sSql);

        $aHeader = array(
            //'header'	=> sprintf($aConfig['lang'][$this->sModule]['book_list'],$sPromoName),
            'action' => phpSelf(array('action' => 'global_promotions', 'do' => 'books', 'id' => $iId)),
            'refresh_link' => phpSelf(array('action' => 'global_promotions', 'do' => 'books', 'id' => $iId, 'reset' => '1')),
            'refresh' => false,
            'search' => false,
            'action' => phpSelf(array('do' => 'remove'), array(), false),
            'second' => true,
            'per_page' => false);

        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'content' => '&nbsp;',
                'sortable' => false
            ),
            array(
                'db_field' => 'B.name',
                'content' => $aConfig['lang'][$this->sModule]['list_name'],
                'sortable' => true
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '45'
            )
        );


        $aColSettings = array(
            'id' => array(
                'show' => false
            ),
            'order_by' => array(
                'show' => false
            ),
            'action' => array(
                'actions' => array('delete', 'preview'),
                'params' => array(
                    'delete' => array('do' => 'removeExcluded', 'id' => '{id}')
                ),
                'show' => false
            )
        );

        $aRecordsFooter = array(
            //array('check_all', 'remove_all')
        );
        $aRecordsFooterParams = array();

        $pForm = new FormTable('transport_means_products', $sHeader, array('action' => phpSelf()), array('col_width' => 150), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', 'add_to_promo');

        //$sSql = 'SELECT COUNT(id) FROM ' . $aConfig['tabls']['prefix'] . 'orders_transport_means_products WHERE orders_transport_means_id=' . $iId;
        //$sSql = 1;
        $iRowCount = 1;//intval(Common::GetOne($sSql));
        $pForm->AddMergedRow(sprintf($aConfig['lang'][$this->sModule]['book_list2'], $sPromoName), array('class' => 'merged', 'style' => 'padding-left: 10px; background-color:#736f54; color:#fff; '));

        $iViewCounter = 0;
        if ($iRowCount > 0) {
            $sSql = 'SELECT id, \'\' AS book_list FROM ' . $aConfig['tabls']['prefix'] . 'orders_transport_means WHERE id=' . $iId .' LIMIT 1';
            $aRecords =& Common::GetAll($sSql);
            foreach ($aRecords as $iKey0 => $aItem) {
                $sShowHideButton = $pForm->GetInputButtonHTML('btnShowHide' . $aItem['id'], $aConfig['lang'][$this->sModule]['show'], array('style' => 'margin-right:100px; width:70px;', 'onclick' => 'showHideRow(' . $aItem['id'] . ');'), 'button') . '&nbsp;';
                $pForm->AddMergedRow($sShowHideButton, array('class' => 'merged', 'style' => 'padding-left: 10px; background-color:#736f54; color:#fff; '));

                //dla kazdej stawki pobieramy liste książek
                $pView = new View('promotions_books' . (++$iViewCounter), $aHeader, $aAttribs);
                $pView->AddRecordsHeader($aRecordsHeader);

                $sSql = "SELECT COUNT(A.id)
							 FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means_products A
							 JOIN " . $aConfig['tabls']['prefix'] . "products B
							 ON B.id = A.product_id
							 WHERE  A.orders_transport_means_id = " . $iId .
                    (isset($_POST['search']) && !empty($_POST['search']) ? (is_numeric($_POST['search']) ? ' AND B.id = ' . (int)$_POST['search'] : ' AND B.name LIKE \'%' . $_POST['search'] . '%\'') : '');
                $iRowCount2 = intval(Common::GetOne($sSql));
                $aBooks = array();
                if ($iRowCount2 > 0) {
                    //jeżeli są jakieś ksiazki
                    $sSql = "SELECT A.id, B.name, C.name AS publisher, A.product_id, B.publication_year, (SELECT order_by FROM " . $aConfig['tabls']['prefix'] . "products_shadow S WHERE S.id=B.id) AS order_by 
									FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means_products A
								 JOIN " . $aConfig['tabls']['prefix'] . "products B
								 ON B.id = A.product_id
								 JOIN " . $aConfig['tabls']['prefix'] . "products_stock PS
								 ON PS.id = A.product_id
								 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers C
									 ON B.publisher_id = C.id
                                 WHERE A.orders_transport_means_id = " . $iId ."
                        ORDER BY ". (isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'order_by') .
                        (isset($_GET['order']) && !empty($_GET['order']) ? ' ' . $_GET['order'] : '');/*.
								 " LIMIT ".$iStartFrom.", ".$iPerPage;*/
                    $aBooks =& Common::GetAll($sSql);

                    foreach ($aBooks as $iKey => $aBook) {
                        $aBooks[$iKey]['name'] = '<b><a href="' . createProductLink($aBook['product_id'], $aBook['name']) . '" target="_blank">' . $aBook['name'] . '</a></b>' . (!empty($aBook['publisher']) ? '<br /><span style="font-weight: normal; font-style: italic;">' . mb_substr($aBook['publisher'], 0, 40, 'UTF-8') . '</span>' : '');

                        unset($aBooks[$iKey]['publisher']);
                        //$aBooks[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aBook['product_id']),0,40,'UTF-8').' '.$aBook['publication_year'].'</span>';
                        unset($aBooks[$iKey]['publication_year']);
                        // link podgladu
                        $aBooks[$iKey]['action']['links']['preview'] = (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'] . '/' : '') . (mb_strlen($aBook['name'], 'UTF-8') > 210 ? mb_substr(link_encode($aBook['name']), 0, 210, 'UTF-8') : link_encode($aBook['name'])) . ',product' . $aBook['product_id'] . '.html?preview=' . md5($_SERVER['REMOTE_ADDR'] . $aConfig['common']['client_base_url_http']);
                        unset($aBooks[$iKey]['product_id']);

                    }
                }
                $pView->AddRecords($aBooks, $aColSettings);
                $pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
                //dodanie tej formy jest konieczne ze względu na błąd interpretacjis truktury DOM w firefox
                $pForm->AddMergedRow(('<form name="NiewidocznaWDOM"></form>') . $pView->Show(), array(), array('id' => 'listContainer' . $aItem['id'], 'style' => ' display:none;'));
                $pForm->AddMergedRow($pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['add_product_w_disc'], array('onclick' => 'openSearchWindow(' . $aItem['value'] . '); return false;', 'style' => 'float:left; margin-left:10px;')) );

            }
        }

        $sJs = '<script>function openSearchWindow(disc) {
					window.open("ajax/SearchProduct.php?pid=' . $iId . '&site=profit24&mode=10&disc="+disc,"","menubar=0,location=0,scrollbars=1,resizable=1,toolbar=0,width=700,height=500");
					};
					
					function createCookie(name,value, days) {
						if (days) {
							var date = new Date();
							date.setTime(date.getTime()+(days*24*60*60*1000));
							var expires = "; expires="+date.toGMTString();
						}
						else var expires = "";
						document.cookie = name+"="+value+expires+"; path=/";
					}
					
					function eraseCookie(name) {
						createCookie(name,"",-1);
					}
					
					function showHideRow(id) {
						var status = 		document.getElementById("listContainer"+id).style.display;
						var newStatus=	"none";
						var label=			"' . $aConfig['lang'][$this->sModule]['show'] . '";
						eraseCookie("displayedPromotionVal"+id);
						if(status=="none") {
							newStatus="";
							label=			"' . $aConfig['lang'][$this->sModule]['hide'] . '";
							createCookie("displayedPromotionVal"+id,1);
							}
						
						if (document.getElementById("listContainer"+id) !== null)
						    document.getElementById("listContainer"+id).style.display=newStatus;
						if (document.getElementById("listContainer2"+id) !== null)
						    document.getElementById("listContainer2"+id).style.display=newStatus;
						document.getElementById("btnShowHide"+id).value=label;
						}
					</script>';

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sJs .
            ShowTable($pForm->ShowForm()));
    } // end of Show() function

    function insertExcluded(&$pSmarty,$iPId) {

        global $aConfig;
        $bIsErr = false;
        set_time_limit(3600);

        $aSelItems=explode(',', $_GET['aid']);
        Common::BeginTransaction();

        foreach ($aSelItems as $iBId) {
            $sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_transport_means_products WHERE orders_transport_means_id='.(int)$_GET['pid'].' AND product_id='.$iBId;
            $iBooksCountById=Common::GetOne($sSql);
            if($iBooksCountById>0) {$sMsg1=$aConfig['lang'][$this->sModule]['info001']; continue;}

            $aValues['product_id']= (int)$iBId;
            $aValues['orders_transport_means_id']= (int)$_GET['pid'];


            if (Common::Insert($aConfig['tabls']['prefix']."orders_transport_means_products",
                    $aValues,'',
                    false) === false) {
                $bIsErr = true;
            } else { $sAdded.=$aProduct['name'].", "; }

        }

        $sAdded=substr($sAdded,0,-2);

        if (!$bIsErr) {
            // dodano
            Common::CommitTransaction();
            if(!empty($sAdded))
                $sMsg .= sprintf($aConfig['lang'][$this->sModule]['add_topromo_ok'],
                    $sAdded,
                    $aPromo['name']);
            if($bDiffrentDiscount)
                $sMsg2=$aConfig['lang'][$this->sModule]['info002'];
            if($sAdded2!='')
                $sMsg3=$aConfig['lang'][$this->sModule]['info003'];

            if(!empty($sMsg)) $this->sMsg = GetMessage($sMsg, false);
            if(!empty($sMsg1)) $this->sMsg .= GetMessage($sMsg1, true);
            if(!empty($sMsg2)) $this->sMsg .= GetMessage($sMsg2, true);
            if(!empty($sMsg3)) $this->sMsg .= GetMessage($sMsg3, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            // reset ustawien widoku
            $_GET['reset'] = 2;
            $this->ShowExcluded($pSmarty,$_GET['pid']);
        }
        else {
            // blad
            // wyswietlenie komunikatu o niepowodzeniu
            // oraz ponowne wyswietlenie formularza dodawania
            Common::RollbackTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_topromo_err'],
                $aPromo['name']);
            $this->sMsg = GetMessage($sMsg);

            AddLog($sMsg);
            $_GET['do']='books';
            $this->ShowExcluded($pSmarty, $_GET['pid']);
        }
        $_GET['do']='books';
        $this->ShowExcluded($pSmarty, $_GET['pid']);
    } // end of AddItem() funciton\

    function RemoveExcluded(&$pSmarty, $id)
    {
        global $aConfig;
        $bIsErr = false;
        Common::BeginTransaction();
        $bIsErr=!$this->deleteVal(intval($_GET['id']));
        if (!$bIsErr) {
            // usunieto
            Common::CommitTransaction();

            $sMsg =$aConfig['lang'][$this->sModule]['del_val_ok'];
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);

        }
        else {
            // blad
            Common::RollbackTransaction();
            $sMsg = $aConfig['lang'][$this->sModule]['del_val_err'];
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
        }

        $this->Show($pSmarty,$id);
    } // end of Delete() funciton

    function deleteVal($iId) {
        global $aConfig;

        $sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."orders_transport_means_products
						 	 WHERE id = ".$iId;
        return (Common::Query($sSql) != false);
    } // end of deleteItem() method
}// end of Module Class
?>
