<?php
/**
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2008
 * @version   1.0
 *
 */

class Module {

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    // Id strony modulu
    var $iPageId;

    // katalog plikow log
    var $sLogsDir;


    /**
     * Konstruktor klasy
     *
     * @return	void
     */
    function Module(&$pSmarty) {
        global $aConfig, $pDB2;

        $this->sErrMsg = '';
        $this->iPageId = getModulePageId('m_zamowienia');
        $this->sModule = $_GET['module'];
        $this->iLangId = $_SESSION['lang']['id'];


        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_'.$_GET['action'];
        }

        if (isset($_GET['id'])) {
            $iId = $_GET['id'];
        }

        $this->aPrivileges =& $_SESSION['user']['privileges'];
        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])){
                showPrivsAlert($pSmarty);
                return;
            }
        }

        switch ($sDo) {
            case 'get_csv': $this->GetCSV(); break;
            case 'get_txt': $this->GetTXT(); break;
            case 'get_pdf': $this->GetPDF($pSmarty); break;
            case 'get_pdf_ext': $this->GetPDF_Ext($pSmarty); break;
            case 'get_mail': $this->GetEmailForm($pSmarty); break;
            case 'send_email': $this->SendEmail($pSmarty); break;
            case 'show_items': $this->Show($pSmarty,true); break;
            default: $this->Show($pSmarty); break;
        }
    } // end Module() function


    /**
     * Metoda aktualizuje rabat uzytkownika o podanym Id
     *
     * @param		object	$pSmarty
     * @return	void
     */
    function GetCSV() {
        global $aConfig;
        //dump($_POST); die();
        $bIsErr = false;
        $sFile = '';
        $aBooks2=array();
        $_GET['hideHeader'] = '1';

        $sListNumber = $_POST['status']==3 ? $this->genListNumberInternal() : $this->genListNumberExternal();

        // rozpoczęcie transakcji
        Common::BeginTransaction();

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="zamowione_pozycje.xls"');
        header("Pragma: no-cache");
        header("Expires: 0");
        // zapis naglowka
        $sLine = "\t\t".sprintf($aConfig['lang'][$this->sModule]['csv_header_'.$_POST['status']],$sListNumber)."\r\n";
        $sFile .= $sLine;
        echo $sLine;
        $sLine = "\t\t".$aConfig['lang'][$this->sModule]['source_'.$_POST['source']]."\r\n\r\n";
        $sFile .= $sLine;
        echo $sLine;
        // zapis rekordow
        $sLine = $aConfig['lang'][$this->sModule]['books_list_id']."\t".
            $aConfig['lang'][$this->sModule]['books_list_name']."\t".
            $aConfig['lang'][$this->sModule]['books_list_isbn']."\t".
            $aConfig['lang'][$this->sModule]['books_list_ile']."\r\n";
        $sFile .= $sLine;
        echo $sLine;
        $iI=1;
        foreach ($_POST['books'] as $iProductId=>$aItem) {

            //przerwanie po 25 pozycjach
            if($iI>25 && $_POST['status']==3) break;
            ++$iI;

            $aBook = $this->getBookData($iProductId);
            $sLine = $iProductId."\t".$aBook['name']."\t".$aBook['isbn']."\t".$aItem['quantity']."\r\n";

            $aItemList=explode(',', $aItem['items']);
            foreach($aItemList as $iOneItem)
                $aBooks2[]=$iOneItem;

            $sFile .= $sLine;
            echo $sLine;

            if($_POST['status'] == '1'){
                if ($this->setOrderedItems($aItem['items']) === false) {
                    $bIsErr = true;
                }
            }
            if($_POST['status'] == '3'){
                if ($this->setHasNCItems($aItem['items']) === false) {
                    $bIsErr = true;
                }
            }

        }
        // dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
        $aValues = array(
            'content' => nl2br($sFile),
            'saved_type' => '0',
            'status' => $_POST['status'],
            'source' => $_POST['source'],
            'date_send' => 'NOW()',
            'send_by' => $_SESSION['user']['name'],
            'number'=>$sListNumber
        );
        $iExportId=false;
        if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
            $bIsErr = true;
        }
        else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego

            foreach ($aBooks2 as $iProductId=>$iItem) {
                $aValues = array(
                    'send_history_id'=>$iExportId,
                    'item_id'=>$iItem
                );
                if ( Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false) === false) {
                    // błąd
                    $bIsErr = true;
                }
            }
        }

        if ($bIsErr === true) {
            // błąd
            Common::RollbackTransaction();
        } else {
            // wszystko ok
            Common::CommitTransaction();
        }
    } // end of GetCSV() funciton

    /**
     * Metoda aktualizuje rabat uzytkownika o podanym Id
     *
     * @param		object	$pSmarty
     * @return	void
     */
    function GetTXT() {
        global $aConfig;
        $bIsErr = false;
        $_GET['hideHeader'] = '1';
        $aBooks2=array();


        $sNow=$_POST['sNow'];
        $sSql='SELECT id FROM '.$aConfig['tabls']['prefix'].'orders_send_history WHERE `date_send`=\''.$sNow.'\' AND `status`=\''.$_POST['status'].'\' AND `send_by`=\''.$_SESSION['user']['name'].'\' AND `source`=\''.$_POST['source'].'\'';
        $iSent=Common::GetOne($sSql);

        if($iSent){
            //przekierowanie do historii - inaczej nie chce dzialac
            header('Location: ?frame=main&module_id=39&module=m_zamowienia&action=send_history');
            return;
        }

        $sFile = '';
        $sListNumber=$_POST['status']==3?$this->genListNumberInternal():$this->genListNumberExternal();

        // rozpoczęcie transakcji
        Common::BeginTransaction();

        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="zamowione_pozycje.txt"');
        header("Pragma: no-cache");
        header("Expires: 0");
        // zapis naglowka
        $sLine = "\t\t".sprintf($aConfig['lang'][$this->sModule]['txt_header_'.$_POST['status']], $sListNumber)."\r\n";
        $sFile .= $sLine;
        echo $sLine;
        $sLine = "\t\t".$aConfig['lang'][$this->sModule]['source_'.$_POST['source']]."\r\n\r\n";
        $sFile .= $sLine;
        echo $sLine;
        // zapis rekordow

        $sLine = $aConfig['lang'][$this->sModule]['books_list_azymut_index']."\t".
            $aConfig['lang'][$this->sModule]['books_list_ile']."\r\n";
        /*
       $sLine = $aConfig['lang'][$this->sModule]['books_list_isbn']."\t".
           $aConfig['lang'][$this->sModule]['books_list_ile']."\r\n";
        *
        */
        $sFile .= $sLine;
        echo $sLine;
        $iI=1;

        //dump($_POST['books']);
        $_POST['books'] = $this->sortBooksByName($_POST['books']);
        //dump($_POST['books']);
        //die;
        foreach ($_POST['books'] as $iProductId=>$aItem) {

            //przerwanie po 25 pozycjach jeśli jest niepotwierdzone
            if($iI>25 && $_POST['status']==3) break;
            //przerwanie po 35 pozycjach jesli jest do zamówienia USTALONE Z Marcinem Chudym 01.09.2011
            if($iI>35 && $_POST['status']==1) break;

            ++$iI;

            $aBook = $this->getBookData($iProductId);
            $sLine = $aBook['azymut_index']."\t".$aItem['quantity']."\r\n";
            //$sLine = $aBook['isbn']."\t".$aItem['quantity']."\r\n";

            $aItemList=explode(',', $aItem['items']);
            foreach($aItemList as $iOneItem)
                $aBooks2[]=$iOneItem;

            $sFile .= $sLine;
            echo $sLine;

            if ($_POST['status'] == '1') {
                if ($this->setOrderedItems($aItem['items']) === false) {
                    $bIsErr = true;
                }
            }
            if ($_POST['status'] == '3') {
                if ($this->setHasNCItems($aItem['items']) === false) {
                    $bIsErr = true;
                }
            }
        }
        // dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
        $aValues = array(
            'content' => nl2br($sFile),
            'saved_type' => '1',
            'status' => $_POST['status'],
            'source' => $_POST['source'],
            'date_send' => 'NOW()',
            'send_by' => $_SESSION['user']['name'],
            'number'=>$sListNumber
        );
        $iExportId=false;
        if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
            $bIsErr = true;
        }
        else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego

            foreach ($aBooks2 as $iProductId=>$iItem) {
                $aValues = array(
                    'send_history_id'=>$iExportId,
                    'item_id'=>$iItem
                );
                if (Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false) === false) {
                    $bIsErr = true;
                }
            }
        }

        if ($bIsErr === true) {
            // błąd
            Common::RollbackTransaction();
        } else {
            // wszystko ok
            Common::CommitTransaction();
        }
    } // end of GetTXT() funciton


    /**
     * Metoda generuje PDF'a zewnętrznego
     *
     * @author Arkadiusz Golba
     * @version 1.0
     * @global type $aConfig
     * @return type
     */
    private function GetPDF_Ext(&$pSmarty) {
        global $aConfig;

        $bIsErr = false;
        $_GET['hideHeader'] = '1';
        $sFile = '';
        $aBooks = array();
        $aBooks2 = array();

        // rozpoczęcie transakcji
        Common::BeginTransaction();

        $sNow = $_POST['sNow'];
        $sSql = "SELECT id 
						 FROM ".$aConfig['tabls']['prefix']."orders_send_history 
						 WHERE date_send='".$sNow."' AND 
									 status ='".$_POST['status']."' AND 
									 send_by='".$_SESSION['user']['name']."' AND 
									 source = '".$_POST['source']."'";
        $iSent = Common::GetOne($sSql);

        if($iSent){
            // przekierowanie do historii - inaczej nie chce dzialac
            header('Location: ?frame=main&module_id=39&module=m_zamowienia&action=send_history');
            //doRedirect($aConfig['base_url'].'?frame=main&module_id=39&module=m_zamowienia&action=send_history');
            die;
        }

        $sSql = "SELECT name, surname
							FROM ".$aConfig['tabls']['prefix']."users
							 WHERE id = ".intval($_SESSION['user']['id']);
        $aGeneratorName = Common::GetRow($sSql);

        $_POST['books'] = $this->sortBooksByName($_POST['books']);

        $i=1;
        $iSuma=0;
        foreach ($_POST['books'] as $iProductId => $aItem) {
            $aBooks['items'][$iProductId] = $this->getBookData($iProductId);
            $aBooks['items'][$iProductId]['authors']=$this->getAuthors($iProductId);

            $aBooks['items'][$iProductId]['quantity'] = 	$aItem['quantity'];
            $aBooks['items'][$iProductId]['location'] = 	$aItem['location']==''													? '&nbsp;' : $aItem['location'];
            $aBooks['items'][$iProductId]['publisher'] = 	$aBooks['items'][$iProductId]['publisher']==''	? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$iProductId]['publisher']),0,35,'UTF-8');
            $aBooks['items'][$iProductId]['authors'] = 		$aBooks['items'][$iProductId]['authors']==''		? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$iProductId]['authors']),0,50,'UTF-8');
            $aBooks['items'][$iProductId]['name'] = 			$aBooks['items'][$iProductId]['name']==''				? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$iProductId]['name']),0,80,'UTF-8');
            //$aBooks['items'][$iProductId]['authors'] = 		$this->getAuthors($iProductId);
            $aBooks['items'][$iProductId]['lp'] =$i++;
            $iSuma+=$aItem['quantity'];

            $aItemList=explode(',', $aItem['items']);
            foreach($aItemList as $iOneItem)
                $aBooks2[]=$iOneItem;

            if($_POST['status'] == '1'){
                if ($this->setOrderedItems($aItem['items']) === false) {
                    $bIsErr = true;
                }
            }
            if($_POST['status'] == '3'){
                if ($this->setHasNCItems($aItem['items']) === false) {
                    $bIsErr = true;
                }
            }
            //przerwanie po 25 pozycjach
            if($i>25 && $_POST['status']==3) break;
        }

        if ($bIsErr === false) {
            Common::CommitTransaction();
        } else {
            Common::RollbackTransaction();
            return false;
        }
        $sListNumber = $_POST['status'] == 3 ? $this->genListNumberInternal() : $this->genListNumberExternal();

        // rozpoczęcie transakcji
        Common::BeginTransaction();

        $sSourceName = in_array($_POST['source'], array(0,1,5,51,52)) ? ($aConfig['lang'][$this->sModule]['source_'.intval($_POST['source'])]) : Common::GetOne('SELECT name FROM `'.$aConfig['tabls']['prefix'].'external_providers` WHERE id='.$_POST['source']);
        $aBooks['orders_header'] = sprintf($aConfig['lang'][$this->sModule]['orders_header_'.$_POST['status']], $sSourceName, $sListNumber);
        $aBooks['orders_header2'] = $aConfig['lang'][$this->sModule]['orders_genby'].': <strong>'.$aGeneratorName['name'].' '.$aGeneratorName['surname'].' - '.$sNow.'</strong>';
        $aBooks['orders_foot1'] = $aConfig['lang'][$this->sModule]['orders_footer_1'];
        $aBooks['orders_foot2'] = $aConfig['lang'][$this->sModule]['orders_footer_2'];
        $aBooks['orders_foot3'] = $aConfig['lang'][$this->sModule]['orders_footer_3'];
        $aBooks['Qsum'] = $iSuma;

        $pSmarty->assign_by_ref('aLang', $aConfig['lang'][$this->sModule]);
        $pSmarty->assign_by_ref('aBooks', $aBooks);
        //if($_POST['status'] == 1)
        //	$sHtml = $pSmarty->fetch('PDF/pdf_orders2_zewn.tpl');
        //else
        //$sHtml = $pSmarty->fetch('PDF/pdf_orders_zewn.tpl');
        $sHtml = $pSmarty->fetch('PDF/pdf_orders2_zewn.tpl');

        $pSmarty->clear_assign('aBooks');

        require_once('OLD_tcpdf/config/lang/pl.php');
        require_once('OLD_tcpdf/tcpdf.php');

        // create new PDF document
        $pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('profit24');
        $pdf->SetTitle($aBooks['orders_header']);
        $pdf->SetSubject($aBooks['orders_header']);
        $pdf->SetKeywords('');
        $pdf->setPrintHeader(false);

        $pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 10);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l);

        // set font
        $pdf->SetFont('freesans', '', 8);


        // add a page
        $pdf->AddPage();
        $pdf->writeHTML($sHtml, true, false, false, false, '');

        $pdf->Output('lista_zewnetrzna.pdf', 'D');

        // wyjatek - już wygenerowaliśmy dokładnie takiego samego pdf'a
        if(!$iSent){
            // dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
            $aValues = array(
                'content' => $sHtml,
                'saved_type' => '3',
                'status' => $_POST['status'],
                'source' => $_POST['source'],
                'date_send' => $sNow,
                'send_by' => $_SESSION['user']['name'],
                'number' => $sListNumber
            );
            $iExportId = false;
            if ( ($iExportId = Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
                $bIsErr = true;
            }
            else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego

                foreach ($aBooks2 as $iProductId=>$iItem) {
                    $aValues = array(
                        'send_history_id'=>$iExportId,
                        'item_id'=>$iItem
                    );
                    if ( (Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false)) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }

        if ($bIsErr === true) {
            // błąd
            Common::RollbackTransaction();
        } else {
            // wszystko ok
            Common::CommitTransaction();
        }
    } // end of GetPDF_Ext() method



    /**
     * Generuje liste pozycji w formacie pdf do pobrania
     * @return unknown_type
     */
    function GetPDF(&$pSmarty) {
        global $aConfig;
        $bIsErr = false;
        $_GET['hideHeader'] = '1';
        $sFile = '';
        $aBooks = array();
        $aBooks2 = array();

        // rozpoczęcie transakcji
        Common::BeginTransaction();

        $sNow=$_POST['sNow'];
        $sSql='SELECT id FROM '.$aConfig['tabls']['prefix'].'orders_send_history WHERE `date_send`=\''.$sNow.'\' AND `status`=\''.$_POST['status'].'\' AND `send_by`=\''.$_SESSION['user']['name'].'\' AND `source`=\''.$_POST['source'].'\'';
        $iSent=Common::GetOne($sSql);

        if($iSent){
            //przekierowanie do historii - inaczej nie chce dzialac
            header('Location: ?frame=main&module_id=39&module=m_zamowienia&action=send_history');
            die;
        }

        $sSql = "SELECT name, surname
							FROM ".$aConfig['tabls']['prefix']."users
							 WHERE id = ".intval($_SESSION['user']['id']);

        $aGeneratorName=Common::GetRow($sSql);

        $_POST['books'] = $this->sortBooksByName($_POST['books']);

        $i=1;
        $iSuma=0;
        foreach ($_POST['books'] as $iProductId=>$aItem) {
            $aBooks['items'][$iProductId] = $this->getBookData($iProductId);

            $aBooks['items'][$iProductId]['authors']=$this->getAuthors($iProductId);

            $aBooks['items'][$iProductId]['quantity'] = 	$aItem['quantity'];
            $aBooks['items'][$iProductId]['location'] = 	$aItem['location']==''													?'&nbsp;':$aItem['location'];
            $aBooks['items'][$iProductId]['publisher'] = 	$aBooks['items'][$iProductId]['publisher']==''	?'&nbsp;':mb_substr(strip_tags($aBooks['items'][$iProductId]['publisher']),0,35,'UTF-8');
            $aBooks['items'][$iProductId]['authors'] = 		$aBooks['items'][$iProductId]['authors']==''		?'&nbsp;':mb_substr(strip_tags($aBooks['items'][$iProductId]['authors']),0,50,'UTF-8');
            $aBooks['items'][$iProductId]['name'] = 			$aBooks['items'][$iProductId]['name']==''				?'&nbsp;':mb_substr(strip_tags($aBooks['items'][$iProductId]['name']),0,80,'UTF-8');
            //$aBooks['items'][$iProductId]['authors'] = 		$this->getAuthors($iProductId);
            $aBooks['items'][$iProductId]['lp'] =$i++;
            $iSuma+=$aItem['quantity'];

            $aItemList=explode(',', $aItem['items']);
            foreach($aItemList as $iOneItem)
                $aBooks2[]=$iOneItem;

            if($_POST['status'] == '1'){
                if ($this->setOrderedItems($aItem['items']) === false) {
                    $bIsErr = true;
                }
            }
            if($_POST['status'] == '3'){
                if ($this->setHasNCItems($aItem['items']) === false) {
                    $bIsErr = true;
                }
            }
            //przerwanie po 25 pozycjach
            if($i>25 && $_POST['status']==3) break;
        }

        if ($bIsErr === false) {
            // XXX zmiana
            Common::CommitTransaction();
        } else {
            Common::RollbackTransaction();
            return false;
        }
        $sListNumber=$_POST['status']==3?$this->genListNumberInternal():$this->genListNumberExternal();
        Common::BeginTransaction();

        $sSourceName=in_array($_POST['source'], array(0,1,5,51,52))?($aConfig['lang'][$this->sModule]['source_'.intval($_POST['source'])]):Common::GetOne('SELECT name FROM `'.$aConfig['tabls']['prefix'].'external_providers` WHERE id='.$_POST['source']);
        $aBooks['orders_header'] = sprintf($aConfig['lang'][$this->sModule]['orders_header_'.$_POST['status']], $sSourceName, $sListNumber);
        $aBooks['orders_header2']=$aConfig['lang'][$this->sModule]['orders_genby'].': <strong>'.$aGeneratorName['name'].' '.$aGeneratorName['surname'].' - '.$sNow.'</strong>';
        $aBooks['orders_foot1']=$aConfig['lang'][$this->sModule]['orders_footer_1'];
        $aBooks['orders_foot2']=$aConfig['lang'][$this->sModule]['orders_footer_2'];
        $aBooks['orders_foot3']=$aConfig['lang'][$this->sModule]['orders_footer_3'];
        $aBooks['Qsum']=$iSuma;
        $pSmarty->assign_by_ref('aLang', $aConfig['lang'][$this->sModule]);
        $pSmarty->assign_by_ref('aBooks', $aBooks);
        if($_POST['status']==1)
            $sHtml = $pSmarty->fetch('PDF/pdf_orders2.tpl');
        else
            $sHtml = $pSmarty->fetch('PDF/pdf_orders.tpl');
        $pSmarty->clear_assign('aBooks');

        require_once('tcpdf/config/lang/pl.php');
        require_once('tcpdf/tcpdf.php');

        // create new PDF document
        $pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('profit24');
        $pdf->SetTitle($aBooks['orders_header']);
        $pdf->SetSubject($aBooks['orders_header']);
        $pdf->SetKeywords('');
        $pdf->setPrintHeader(false);

        $pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(5, 5, 5);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 10);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l);

        // set font
        $pdf->SetFont('freesans', '', 8);


        // add a page
        $pdf->AddPage();
        $pdf->writeHTML($sHtml, true, false, false, false, '');


        //wyjatek - już wygenerowaliśmy dokładnie takiego samego pdf'a
        if(!$iSent){
            // dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
            $aValues = array(
                'content' => $sHtml,
                'saved_type' => '3',
                'status' => $_POST['status'],
                'source' => $_POST['source'],
                'date_send' => $sNow,
                'send_by' => $_SESSION['user']['name'],
                'number'=>$sListNumber
            );
            $iExportId=false;
            if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
                $bIsErr = true;
            }
            else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
                foreach ($aBooks2 as $iProductId=>$iItem) {
                    $aValues = array(
                        'send_history_id'=>$iExportId,
                        'item_id'=>$iItem
                    );
                    if ( (Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false)) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }

        if ($bIsErr === true) {
            // błąd
            Common::RollbackTransaction();
        } else {
            // wszystko ok
            // XXX zmiana
            Common::CommitTransaction();
        }

        $pdf->Output('lista.pdf', 'D');
    } // end of GetPDF() funciton


    /**
     * Metoda generuje formularz do wysłania emaila
     *
     * @param		object	$pSmarty
     * @return	void
     */
    function GetEmailForm(&$pSmarty) {
        global $aConfig;
        $bIsErr = false;
        $aData = array();

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
        $sHeader = $aConfig['lang'][$this->sModule]['send_ordered_items_header'];
        $pForm = new FormTable('ordered_itm', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);

        // sprawdzenie czy jakies ksiazki zostały wybrane
        if (!empty($_POST['books'])) {
            foreach ($_POST['books'] as $iProductId=>$aItem) {
                $pForm->AddHidden('books['.$iProductId.'][quantity]', $aItem['quantity']);
                $pForm->AddHidden('books['.$iProductId.'][items]', $aItem['items']);
            }
            //$sSource,$sDateFrom,$sDateTo
            if (!empty($_POST)) {
                $aData =& $_POST;
            }

            $pForm->AddCheckBox('define_section', $aConfig['lang'][$this->sModule]['define_section'], array('onchange'=>'ChangeObjValue(\'do\', \'get_mail\'); this.form.submit();'), '', !empty($aData['define_section']), false);
            if(!empty($aData['define_section'])){
                $pForm->AddText('email', $aConfig['lang'][$this->sModule]['define_email'], '', array(), '', 'email', false);
                $pForm->AddText('name', $aConfig['lang'][$this->sModule]['define_name'], '', array(), '', 'text', false);
                $pForm->AddText('surname', $aConfig['lang'][$this->sModule]['define_surname'], '', array(), '', 'text', false);
            } else{
                $pForm->AddSelect('address_book', $aConfig['lang'][$this->sModule]['address_book'], array(), $this->getEmails(), $aData['email'], '', false);
            }
            $pForm->AddHidden('do', 'send_email');
            $pForm->AddHidden('source', $_POST['source']);
            $pForm->AddHidden('status', $_POST['status']);
            $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['send']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('action'=>'ordered_items')).'\');'), 'button'));

        } else{
            //	$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_items']);
            $pForm->AddRow('&nbsp;', $aConfig['lang'][$this->sModule]['no_items'], 'info');
            $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('action'=>'ordered_items')).'\');'), 'button'));
        }

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
    } // end of GetEmailForm() funciton


    /**
     * Metoda aktualizuje rabat uzytkownika o podanym Id
     *
     * @param		object	$pSmarty
     * @return	void

    function GetMailto($sSource,$sDateFrom,$sDateTo) {
    global $aConfig;

    $sMailto = "mailto:?SUBJECT=".$aConfig['lang'][$this->sModule]['mail_header']."&BODY=";

    $sBody = "";

    $bIsErr = false;
    // wyszukiwanie ksiazek spelniajacych zadane kryteria
    $aBooks =& $this->getBooks($sSource,$sDateFrom, $sDateTo);

    foreach ($aBooks as $aItem) {
    $sBody .= $aItem['isbn']."      ".$aItem['ile']."\n";

    }
    return $sMailto.str_replace('+','%20',urlencode($sBody));
    } // end of GetTXT() funciton
     */

    /**
     * Metoda aktualizuje rabat uzytkownika o podanym Id
     *
     * @param		object	$pSmarty
     * @return	void
     */
    function SendEmail(&$pSmarty){
        global $aConfig;
        $bIsErr = false;
        $sErrStr='';
        $aBooks2=array();

        Common::BeginTransaction();

        $aEmail = $this->getEmails($_POST['address_book'], $_POST['email']);
        // pobranie emaila
        $sEmail = $aEmail[0]['email'];
        if ($_POST['define_section'] == '1' && empty($aEmail) ) {
            $aValue['email'] = $_POST['email'];
            $aValue['name'] = $_POST['name'];
            $aValue['surname'] = $_POST['surname'];
            // dodanie adresu do książki adresowej
            if (Common::Insert($aConfig['tabls']['prefix']."orders_address_book", $aValue, $aValues, '', false) === false) {
                $bIsErr = true;
            }
            $sEmail = $_POST['email'];
        } elseif($_POST['define_section'] == '1' && !empty($aEmail)){
            // jeśli dublujemy adres email error
            $sErrStr .= sprintf($aConfig['lang'][$this->sModule]['duplicate_err'], $sEmail);
            $bIsErr = true;
        }
        // jeśli wcześniej nie wystąpił błąd
        if (!$bIsErr) {
            //$aBooks['items'] =& $this->getBooks($_POST['source'],$_POST['date_from'], $_POST['date_to']);
            $i=0;
            $iI=1;
            $iSuma=0;
            foreach ($_POST['books'] as $iProductId=>$aItem) {

                //przerwanie po 25 pozycjach
                if($iI>25 && $_POST['status']==3) break;
                ++$iI;

                $aBooks['items'][$iProductId] = $this->getBookData($iProductId);
                $aBooks['items'][$iProductId]['quantity'] = $aItem['quantity'];
                $aBooks['items'][$iProductId]['link'] = $aConfig['common']['client_base_url_http_no_slash'].createProductLink($iProductId, $aBooks['items'][$iProductId]['name']);
                $aBooks['items'][$iProductId]['lp'] =$i++;
                $iSuma+=$aItem['quantity'];

                $aItemList=explode(',', $aItem['items']);
                foreach($aItemList as $iOneItem)
                    $aBooks2[]=$iOneItem;

                if($_POST['status'] == '1'){
                    if ($this->setOrderedItems($aItem['items']) === false) {
                        $bIsErr = true;
                    }
                }
                if($_POST['status'] == '3'){
                    if ($this->setHasNCItems($aItem['items']) === false) {
                        $bIsErr = true;
                    }
                }
            }
            if ($bIsErr === false) {
                Common::CommitTransaction();
            } else {
                Common::RollbackTransaction();
                return false;
            }
            $sListNumber=$_POST['status']==3?$this->genListNumberInternal():$this->genListNumberExternal();
            Common::BeginTransaction();

            $aBooks['orders_header'] = sprintf($aConfig['lang'][$this->sModule]['orders_header_'.$_POST['status']], $aConfig['lang']['m_zamowienia_ordered_items']['source_'.$_POST['source']],$sListNumber);
            $aBooks['Qsum']=$iSuma;
            $pSmarty->assign_by_ref('aLang', $aConfig['lang'][$this->sModule]);
            $pSmarty->assign_by_ref('aBooks', $aBooks);
            $sBody = $pSmarty->fetch('email_orders.tpl');
            $pSmarty->clear_assign('aBooks');
            if (!$bIsErr){
                // dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
                $aValues = array(
                    'email' => $sEmail,
                    'content' => addslashes($sBody),
                    'saved_type' => '2',
                    'status' => $_POST['status'],
                    'source' => $_POST['source'],
                    'date_send' => 'NOW()',
                    'send_by' => $_SESSION['user']['name'],
                    'number'=>$sListNumber
                );
                $iExportId=false;
                if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
                    $bIsErr = true;
                }
                else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego

                    foreach ($aBooks2 as $iProductId=>$iItem) {
                        $aValues = array(
                            'send_history_id'=>$iExportId,
                            'item_id'=>$iItem
                        );
                        if ( Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false) === false) {
                            $bIsErr = true;
                        }
                    }
                }
            }
            if (Common::sendTxtHtmlMail($aConfig['default']['website_name'], $aConfig['default']['website_email'], $sEmail, changeCharcodes($aConfig['lang'][$this->sModule]['orders'], 'utf8', 'no'), '', $sBody,'') === false) {
                // błąd wysyłania emaila
                $sErrStr .= sprintf($aConfig['lang'][$this->sModule]['send_err'], $sEmail);
                $bIsErr = true;
            }
        }
        if (!$bIsErr) {
            // zatwierdzenie tranzakcji
            Common::CommitTransaction();
            // wysłano
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['send_ok'], $sEmail);
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
        }
        else {
            // wycofanie tranzakcji
            Common::RollbackTransaction();
            // blad
            $sMsg = $sErrStr;
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
        }
        $this->Show($pSmarty, true);
    }// end of SendEmail() method


    /**
     * Metoda tworzy formularz edycji rabatu grupowego
     * dla uzytkownikow, ktorzy spelniaja okreslone zalozenia:
     * przedzial wartosci zlozoych dotychczas zamowien, wartosc aktualnego
     * rabatu
     *
     * @param	object	$pSmarty
     * @param	bool	$bSearch	- true: wyswietl uzytkownikow
     */
    function Show(&$pSmarty, $bSearch=false) {
        global $aConfig;
        $sHtml = '';
        $aUsers = array();
        if ($bSearch && ($_POST['source'] != '' || $_POST['status'] != '')) {
            // wyszukiwanie ksiazek spelniajacych zadane kryteria
            $aBooks =& $this->getBooks($_POST['source'], $_POST['status']);
//			foreach($aBooks as $iKey => $aBook){
//				unset($aBooks[$iKey]['id']);
//			}
        }
        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
        $sHeader = $aConfig['lang'][$this->sModule]['ordered_items_header'].($_POST['source'] != ''?' - '.$aConfig['lang'][$this->sModule]['source_'.$_POST['source']]:'');

        $pForm = new FormTable('ordered_itm', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', 'get_csv');
        $pForm->AddHidden('source', $_POST['source']);
        $pForm->AddHidden('status', empty($_POST['status'])?3:$_POST['status']);
        if ($bSearch && $_POST['status'] != '') {
            $pForm->AddHidden('status', $_POST['status']);
        }

        // typ do pobrania
        $aStatus = array(
            array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1']),
            array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['status_3'])
        );
        $sSql = "SELECT id, icon, name
							 FROM ".$aConfig['tabls']['prefix']."external_providers
							 WHERE 1 ORDER BY name ASC";
        $aProviders =& Common::GetAll($sSql);

        $iCnt=5;
        $sExternalProvidersHtml='';
        foreach ($aProviders as $iKey =>$aProvider) {
            ++$iCnt;
            $sExternalProvidersHtml.='<td style="border:0px;"><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'1\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\''.$aProvider['id'].'\'; document.getElementById(\'do\').form.submit();" class="abe_ico" style="background-image:url('.$aProvider['icon'].');"><span>'.$aProvider['name'].'</span></a>'.($this->countBooks($aProvider['id'], 1)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>';
            if($iCnt>6) {$iCnt=0; $sExternalProvidersHtml.='</tr><tr>';}
        }

        $pForm->AddRow('&nbsp;', '<div><table><tr>
										<td style="border:0px;"<a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'3\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'0\'; document.getElementById(\'do\').form.submit();" class="profitj_ico"><span>Profit J</span></a>'.($this->countBooks(0, 3)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										<td style="border:0px;"><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'3\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'1\'; document.getElementById(\'do\').form.submit();" class="profitn_ico"><span>Profit N</span></a>'.($this->countBooks(1, 3)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										<td style="border:0px;"><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'3\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'5\'; document.getElementById(\'do\').form.submit();" class="profitn_ico"><span>Profit M</span></a>'.($this->countBooks(5, 3)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										<td style="border:0px;"><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'3\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'51\'; document.getElementById(\'do\').form.submit();" class="profitn_ico"><span>Stock</span></a>'.($this->countBooks(51, 3)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										<td style="border:0px;"><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'3\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'52\'; document.getElementById(\'do\').form.submit();" class="profitn_ico"><span>Profit X</span></a>'.($this->countBooks(52, 3)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										<td style="border:0px;"><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'-1\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'\'; document.getElementById(\'do\').form.submit();" class="profitn_ico"><span>Zapowiedzi</span></a>'.($this->countBooks(NULL, -1)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										'
            /*.
            $pForm->GetLabelHTML('status',$aConfig['lang'][$this->sModule]['status'])
            .$pForm->GetSelectHTML('status',$aConfig['lang'][$this->sModule]['status'],array('onchange'=>'document.getElementById(\'do\').value=\'show_items\'; this.form.submit();'),$aStatus,$_POST['status'])*/
            .'</tr></table></div>');
        /*<a href="javascript:void(0);" onclick="document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'2\'; document.getElementById(\'do\').form.submit();" class="azymut_ico"><span>Azymut</span></a>&nbsp;&nbsp;
                                            <a href="javascript:void(0);" onclick="document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'3\'; document.getElementById(\'do\').form.submit();" class="abe_ico"><span>ABE</span></a>&nbsp;&nbsp;
                                            <a href="javascript:void(0);" onclick="document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'4\'; document.getElementById(\'do\').form.submit();" class="helion_ico"><span>Helion</span></a>&nbsp;&nbsp;
                                            */

        // przycisk wyszukaj
        if(!empty($aBooks)){
            $pForm->AddHidden('sNow', date('Y-m-d H:i:s'));
            $iLp=1;
            $iSumaEgz=0;
            foreach($aBooks as $iKey=>$aBook){
                $pForm->AddHidden('books['.$aBook['id'].'][quantity]', $aBook['quantity']);
                $pForm->AddHidden('books['.$aBook['id'].'][items]', implode(',',$aBook['items']));
                $pForm->AddHidden('books['.$aBook['id'].'][location]', $aBook['location']);
                unset($aBooks[$iKey]['items']);
                $aBooks[$iKey]['id']=$iLp++;
                $iSumaEgz+=$aBook['quantity'];
                //unset($aBooks[$iKey]['location']);
            }
            $aBooks[]=array('','','','','','','<strong>'.$aConfig['lang'][$this->sModule]['sum'].'</strong>','<strong>'.$iSumaEgz.'</strong>');
            if ($_POST['status'] != '-1' && $_POST['source'] != '51') {
                $pForm->AddRow('&nbsp;', '<div style="margin-left: 115px;">'.
                    $pForm->GetInputButtonHTML('get_pdf', $aConfig['lang'][$this->sModule]['button_get_pdf'], array('onclick'=>'document.getElementById(\'do\').value=\'get_pdf\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'&nbsp;&nbsp;</div>');
            }
        }
        $pForm->AddMergedRow($aConfig['lang'][$this->sModule]['found_books'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));

        if ($bSearch) {
            // wyswietlenie listy pasujacych uzytkownikow
            $pForm->AddMergedRow( $this->GetBooksList($pSmarty, $aBooks) );
        }
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));

    } // end of EditGroupDiscount() function


    /**
     * Metoda wyswietla liste ksiazek z tablicy $aBooks
     *
     * @param		object	$pSmarty
     * @param	array ref	$aBooks	- lista produktów
     * @return	void
     */
    function GetBooksList(&$pSmarty, &$aBooks) {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'refresh'	=> false,
            'search'	=> false,
            'per_page'	=> false,
            'checkboxes'	=> false,
            'form'	=>	false
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 0,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'content'	=> $aConfig['lang'][$this->sModule]['books_list_lp'],
                'width' => '30'
            ),
            array(
                'content'	=> $aConfig['lang'][$this->sModule]['books_list_isbn'],
                'width' => '60'
            ),
            array(
                'content'	=> $aConfig['lang'][$this->sModule]['books_list_name']
            ),
            array(
                'content'	=> $aConfig['lang'][$this->sModule]['books_list_publisher'],
                'width' => '60'
            ),
            array(
                'content'	=> $aConfig['lang'][$this->sModule]['books_list_year'],
                'width' => '50'
            ),
            array(
                'content'	=> $aConfig['lang'][$this->sModule]['books_bookindeks'],
                'width' => '50'
            ),
            array(
                'content'	=> $aConfig['lang'][$this->sModule]['books_list_ile'],
                'width' => '50'
            ),
            array(
                'content'	=> _("Stan"),
                'width' => '50'
            ),
            array(
                'content'	=> _("Dostępny w źródle"),
                'width' => '50'
            ),
        );

        $pView = new View('ordered_itm', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);

        // ustawienia dla poszczegolnych kolumn rekordu
        $aColSettings = array('location'	=> array(
            'show'	=> false
        ),);
        // dodanie rekordow do widoku
        $pView->AddRecords($aBooks, $aColSettings);
        // dodanie stopki do widoku
        $aRecordsFooter = array(

        );
        $pView->AddRecordsFooter($aRecordsFooter);
        // przepisanie langa - info o braku rekordow
        $aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_books_to_show'];
        return $pView->Show();
    } // end of GetBooksList() function


    /**
     * Metoda pobiera mapowanie źródła na literę
     *
     * @param type $iSource
     * @return string
     */
    private function getSourceMapping($iSource) {

        switch ($iSource) {
            case '0':
                $cSymbol = 'j';
                break;
            case '1':
                $cSymbol = 'e';
                break;
            case '5':
                $cSymbol = 'm';
                break;
            case '51':
                $cSymbol = 'g';
                break;
            case '52':
                $cSymbol = 'X';
                break;
        }
        return $cSymbol;
    }// end of getSourceMapping() method


    /**
     * metoda pobiera liste ksiazek zamowionych
     * @param $iSource - zrodlo zamowienia
     * @param $iStatus - stan pozycji zamówienia
     * @return array - lista ksiazek
     */
    function &getBooks($iSource,$iStatus) {
        global $aConfig;
        $aOrders = array();

        if ($iStatus != '') {

            $cSymbol = $this->getSourceMapping($iSource);

            // pobranie ID uzytkownikow spelniajacych zadane kryteria
            $sSql = "SELECT A.product_id, A.id, A.order_id, A.quantity, A.shipment_date".($cSymbol != '' ? ($cSymbol == 'g' ? " , PS.profit_g_act_stock, PS.profit_".$cSymbol."_status as profit_".$cSymbol."_status " : ", PS.profit_".$cSymbol."_status ") : '').",
                IF ((PS.azymut_status + PS.abe_status + PS.helion_status + PS.dictum_status + PS.siodemka_status + PS.olesiejuk_status + PS.ateneum_status + PS.profit_e_status + PS.profit_j_status + PS.profit_m_status + PS.profit_g_status) > 0, '1', '0' ) AS other_stock
							 FROM ".$aConfig['tabls']['prefix']."orders_items A
							 JOIN ".$aConfig['tabls']['prefix']."products C
							 ON C.id = A.product_id
               JOIN ".$aConfig['tabls']['prefix']."products_stock PS
                 ON A.product_id = PS.id
							 JOIN ".$aConfig['tabls']['prefix']."orders O
                  ON O.id=A.order_id
							 WHERE 1=1 
							 ".($iSource != '' ? " AND A.source = '".$iSource."'" : '')."
							 ".($iStatus === '-1' ? " AND (O.internal_status = '2' OR O.internal_status = '3') " : '')."
							 AND O.order_status <> '5'
							 AND O.order_status <> '4'
							 AND A.deleted = '0'
							 AND A.packet = '0'
							 AND A.status = '".$iStatus."'".
                ($iStatus == '3'?" AND A.sent_hasnc = '0'":'').
                ($iStatus === '-1' ? ' ORDER BY shipment_date ASC ' : '');

            $aBooks = Common::GetAssoc($sSql,true,array(),DB_FETCHMODE_ASSOC,true);
            $sJS = '<script type="text/javascript">
        $("#item_%s").parent().parent().attr("style", "background-color: grey;");
        </script>';
            foreach($aBooks as $iProduct=>$aItem){
                if ($iStatus !== '-1') {
                    $aOrders[$iProduct] = $this->getBookData($iProduct);
                    unset($aOrders[$iProduct]['profit_source_stock']);
                } else {
                    $aOrders[$iProduct] = $this->getPreviewBookData($iProduct);
                    $aOrders[$iProduct]['publication_year'] = $aItem[0]['shipment_date'];
                }
                $aOrders[$iProduct]['quantity'] = 0;
                foreach($aItem as $OItem){
                    $aOrders[$iProduct]['quantity'] += $OItem['quantity'];
                    $aOrders[$iProduct]['items'][] =& $OItem['id'];
                    $aOrders[$iProduct]['stock'] = $OItem["profit_".$cSymbol."_status"];
                    $aOrders[$iProduct]['other_stock'] = '';
                    if ($OItem['other_stock'] == '1') {
                        $aOrders[$iProduct]['other_stock'] = ' <img src="gfx/icons/alert_ico.gif" /> ';
                    }
                }

                if ($cSymbol == 'g') {
                    $fActStock = $OItem['profit_g_act_stock'];
                } else {
                    $fActStock = $aOrders[$iProduct]['stock'];
                }
                if (($fActStock - $aOrders[$iProduct]['quantity']) < 0) {

                    $sStrJs = sprintf($sJS, $iProduct);
                    $aOrders[$iProduct]['stock'] .= '<span id="item_' . $iProduct . '"></span>'.$sStrJs;
                } else {
                    $aOrders[$iProduct]['other_stock'] = '';
                }
            }
        }

        return $aOrders;
    } // end of getBooks() method

    /**
     * metoda pobiera liczbę ksiazek zamowionych
     * @param $iSource - zrodlo zamowienia
     * @param $iStatus - stan pozycji zamówienia
     * @return int - liczba ksiazek
     */
    function countBooks($iSource,$iStatus) {
        global $aConfig;
        $aOrders = array();

        // pobranie ID uzytkownikow spelniajacych zadane kryteria
        $sSql = "SELECT COUNT(A.id)
						 FROM ".$aConfig['tabls']['prefix']."orders_items A
						 JOIN ".$aConfig['tabls']['prefix']."products C
						 ON C.id = A.product_id
						 JOIN ".$aConfig['tabls']['prefix']."orders O
						 ON O.id=A.order_id
						 WHERE 1=1 
						 ".(isset($iSource) ? "AND A.source = '".$iSource."'" : '')."
						 AND O.order_status <> '5'
						 AND A.deleted = '0'
						 AND A.packet = '0'
						 AND A.status = '".$iStatus."'".
            ($iStatus=='3'?" AND A.sent_hasnc = '0'":'');
        return Common::GetOne($sSql);
    } // end of countBooks() method

    /**
     * Metoda pobiera autorow produktu o podanym id
     *
     * @param	intger	$iId	- Id produktu
     * @return	array	- autorzy produktu
     */
    function getAuthors($iId){
        global $aConfig;

        // pobranie autorow ksiazki
        $sSql = "SELECT B.name as role_name, CONCAT(C.surname,' ',C.name) as author
						FROM ".$aConfig['tabls']['prefix']."products_to_authors A
						JOIN ".$aConfig['tabls']['prefix']."products_authors_roles B
						ON B.id = A.role_id
						JOIN ".$aConfig['tabls']['prefix']."products_authors C
						ON C.id = A.author_id
						WHERE A.product_id = ".$iId."
						ORDER BY B.order_by";

        $aResult =& Common::GetAll($sSql);
        $sAuthors='';

        foreach($aResult as $aAuthor){
            $sAuthors .= $aAuthor['author'].', ';
        }
        return substr($sAuthors,0,-2);
    } // end of getAuthors() method


    /**
     * Metoda pobiera emaile
     *
     * @param	intger	$iId	- Id produktu
     * @param	string	$sEmail	- email użytkownika
     * @return	array	- autorzy produktu
     */
    function &getEmails($iId=0, $sEmail=''){
        global $aConfig;

        // pobranie autorow ksiazki
        $sSql = "SELECT CONCAT(email, ' ', name,' ' , surname) AS label, id AS value, email AS email
						FROM ".$aConfig['tabls']['prefix']."orders_address_book
						WHERE 1=1 
						".(!$iId==0? " AND id=".$iId : '').
            (!empty($sEmail) ? " AND email='".$sEmail."'" : '').
            " ORDER BY id";

        return Common::GetAll($sSql);
    } // end of getEmails() method

    function &getBookData($iId) {
        global $aConfig;

        $cSymbol = $this->getSourceMapping($_POST['source']);

        $sSql = "SELECT A.id, A.isbn, A.name, B.name AS publisher, A.publication_year, A.location, A.azymut_index ".($cSymbol != '' ? ", PS.profit_".$cSymbol."_status as profit_source_stock " : '')."
							 FROM ".$aConfig['tabls']['prefix']."products A
               JOIN products_stock AS PS
                ON PS.id = A.id
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers AS B
							 ON B.id = A.publisher_id
							 WHERE A.id = ".$iId;
        return Common::GetRow($sSql);
    }

    function &getPreviewBookData($iId){
        global $aConfig;
        $sSql = "SELECT A.id, A.isbn, A.name, B.name AS publisher, A.publication_year, A.location, A.azymut_index
							 FROM ".$aConfig['tabls']['prefix']."products A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers AS B
							 ON B.id = A.publisher_id
							 WHERE A.id = ".$iId;
        return Common::GetRow($sSql);
    }

    function setOrderedItems($sItems){
        global $aConfig;
        if(!empty($sItems)){
            $sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders_items
								SET status='2'
								WHERE id IN (".$sItems.") AND status='1'";
            if(Common::Query($sSql) === false){
                return false;
            }
            return true;
        }
        return false;
    }

    function setHasNCItems($sItems){
        global $aConfig;
        if(!empty($sItems)){
            $sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders_items
								SET sent_hasnc='1'
								WHERE id IN (".$sItems.") AND status='3'";
            if(Common::Query($sSql) === false){
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Pobiera i generuje kolejny nr listy dla dostawcy wewnetrznego
     * @return string - nr listy
     */
    function genListNumberInternal() {
        global $aConfig;

        Common::Query("LOCK TABLES ordered_items_numbering_int WRITE;");

        $sSql="SELECT * FROM ".$aConfig['tabls']['prefix']."ordered_items_numbering_int";
        $aNumber = Common::GetRow($sSql);
        if(!empty($aNumber)){
            if($aNumber['number_year'] == date('Y')){
                $sSql = "UPDATE ".$aConfig['tabls']['prefix']."ordered_items_numbering_int SET id=id+1";
                if (Common::Query($sSql)===false){
                    Common::Query("UNLOCK TABLES");
                    return false;
                } else{
                    Common::Query("UNLOCK TABLES");
                    return sprintf("W%05d/%s",$aNumber['id'],date('Y'));
                }
            } else {
                $aValues = array(
                    'id' => 2,
                    'number_year' => date('Y')
                );
                if (Common::Update($aConfig['tabls']['prefix']."ordered_items_numbering_int", $aValues,'',false) === false) {
                    Common::Query("UNLOCK TABLES");
                    return false;
                } else {
                    Common::Query("UNLOCK TABLES");
                    return sprintf("W%05d/%s",1,date('Y'));
                }
            }
        } else {
            $aValues = array(
                'id' => 2,
                'number_year' => date('Y')
            );
            if (Common::Insert($aConfig['tabls']['prefix']."ordered_items_numbering_int", $aValues,'',false) === false) {
                Common::Query("UNLOCK TABLES");
                return false;
            } else {
                Common::Query("UNLOCK TABLES");
                return sprintf("W%05d/%s",1,date('Y'));
            }
        }
        Common::Query("UNLOCK TABLES");
    } // end of genListNumberInternal() method
    /**
     * Pobiera i generuje kolejny nr listy dla dostawcy zewnetrznego
     * @return string - nr listy
     */
    function genListNumberExternal() {
        global $aConfig;

        Common::Query("LOCK TABLES ordered_items_numbering_ext WRITE;");

        $sSql="SELECT * FROM ".$aConfig['tabls']['prefix']."ordered_items_numbering_ext";
        $aNumber = Common::GetRow($sSql);
        if(!empty($aNumber)){
            if($aNumber['number_year'] == date('Y')){
                $sSql = "UPDATE ".$aConfig['tabls']['prefix']."ordered_items_numbering_ext SET id=id+1";
                if (Common::Query($sSql)===false){
                    Common::Query("UNLOCK TABLES");
                    return false;
                } else{
                    Common::Query("UNLOCK TABLES");
                    return sprintf("Z%05d/%s",$aNumber['id'],date('Y'));
                }
            } else {
                $aValues = array(
                    'id' => 2,
                    'number_year' => date('Y')
                );
                if (Common::Update($aConfig['tabls']['prefix']."ordered_items_numbering_ext", $aValues,'',false) === false) {
                    Common::Query("UNLOCK TABLES");
                    return false;
                } else {
                    Common::Query("UNLOCK TABLES");
                    return sprintf("Z%05d/%s",1,date('Y'));
                }
            }
        } else {
            $aValues = array(
                'id' => 2,
                'number_year' => date('Y')
            );
            if (Common::Insert($aConfig['tabls']['prefix']."ordered_items_numbering_ext", $aValues,'',false) === false) {
                Common::Query("UNLOCK TABLES");
                return false;
            } else {
                Common::Query("UNLOCK TABLES");
                return sprintf("Z%05d/%s",1,date('Y'));
            }
        }
        Common::Query("UNLOCK TABLES");
    } // end of genListNumberExternal() method


    /**
     * Metoda sortuje książki po nazwie
     *
     * @global type $aConfig
     * @param type $aBooks
     */
    function sortBooksByName($aBooks) {
        global $aConfig;
        $aNewArr = array();

        $aBIds = array_keys($aBooks);
        $sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id IN (".implode(', ', $aBIds).")
						ORDER BY name";
        $aIdsSorted = Common::GetCol($sSql);

        $i=0;
        foreach ($aIdsSorted as $iBId) {

            if (is_array($aBooks[$iBId]) && !empty($aBooks[$iBId])) {
                $aNewArr['n_'.$i] = $aBooks[$iBId];
                $aNewArr['n_'.$i]['id'] = $iBId;

                $i++;
            }
        }
        // znowu przepisanie do aBookx
        $aBooks = array();
        foreach ($aNewArr as $aBook) {
            $aBooks[$aBook['id']] = $aBook;
            unset($aBooks[$aBook['id']]['id']);
        }
        return $aBooks;
    }// end of sortBooksByName() method
} // end of Module Class
?>