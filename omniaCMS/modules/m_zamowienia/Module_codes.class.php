<?php
/**
 * Klasa Module do obslugi modulu 'Promocje' - Kody Rabatowe
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
//include_once('modules/m_oferta_produktowa/Module_Common.class.php');
 
class Module  {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// katalog plikow log
	var $sLogsDir;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;
		
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$this->sLogsDir = $aConfig['common']['base_path'].'logs';
		
		$sDo = '';
		$iId = 0;		
		
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
		if (isset($_GET['bid'])) {
			$iBId = $_GET['bid'];
		}
		
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
		
		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iId); break;
      case 'update_categories': $this->UpdateCategories($pSmarty, $iId); break;
			case 'edit':
			case 'change_ctype':
			case 'add': $this->AddEdit($pSmarty, $iPId, $iId); break;
      case 'categories': $this->AddEditCategories($pSmarty, $iId); break;
			case 'users': $this->ShowUsers($pSmarty, $iId); break;
			case 'send': $this->ChooseUsersToSend($pSmarty, $iId); break;
			case 'get_users_to_send': $this->ChooseUsersToSend($pSmarty, $iId, true); break;
			case 'proceed_send': $this->proceedSend($pSmarty, $iId); break;
			case 'books' : $this->ShowBooks($pSmarty, $iId); break;
			case 'add_books': $this->AddItems($pSmarty, $iId); break;
			case 'remove': $this->RemoveFromPromotion($pSmarty, $iId, $iBId); break;
			
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method
	
	
	/**
	 * Metoda wyswietla liste produktow strony o id $iPId oferty produktowej
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		if (!empty( $_SESSION['_viewState']['m_zamowienia_codes']['get']['do'] )) {
			$_SESSION['_viewState']['m_zamowienia_codes']['get']['page'] = '1';
		}
		// zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'code',
				'content'	=> $aConfig['lang'][$this->sModule]['list_code'],
				'sortable'	=> true,
				'width'	=> '75'
			),
      array(
				'db_field'	=> 'website',
				'content'	=> _('Dotyczy serwisu'),
				'sortable'	=> true,
				'width'	=> '75'
			),  
			array(
				'db_field'	=> 'code_discount',
				'content'	=> $aConfig['lang'][$this->sModule]['list_code_discount'],
				'sortable'	=> true,
				'width'	=> '60'
			),
			array(
				'db_field'	=> 'code_active_from',
				'content'	=> $aConfig['lang'][$this->sModule]['list_code_active_from'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'db_field'	=> 'code_expiry',
				'content'	=> $aConfig['lang'][$this->sModule]['list_code_expiry'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'db_field'	=> 'used_count',
				'content'	=> $aConfig['lang'][$this->sModule]['used_count'],
				'sortable'	=> true,
				'width'	=> '50'
			),
      array(
				'db_field'	=> 'extra',
				'content'	=> $aConfig['lang'][$this->sModule]['extra'],
				'sortable'	=> true,
        'width'	=> '50'
			),
			array(
				'db_field'	=> 'desc',
				'content'	=> $aConfig['lang'][$this->sModule]['desc'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '25'
			)
		);
		
		$sDeletedSql = " 1 = 1 ";
		
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."promotions_codes
						 WHERE ".$sDeletedSql.
						 (isset($_POST['search']) && !empty($_POST['search']) ? " AND (code = '".$_POST['search']."' OR `descr` LIKE '%".$_POST['search']."%')" : '').
						 (isset($_POST['f_active']) && $_POST['f_active'] != '' ? ($_POST['f_active'] == '1' ? " AND code_active_from <= NOW() AND code_expiry >= NOW()" : ($_POST['f_active'] == '2' ? " AND code_expiry < NOW()" : " AND code_active_from > NOW()")) : '');
		$iRowCount = intval(Common::GetOne($sSql));

		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."promotions_codes
							 WHERE ".$sDeletedSql;
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('promotions_codes', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// FILTRY
		// stan aktywnosci
		$aActive = array(
			array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_active']),
			array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_active']),
			array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_used'])
		);
		$pView->AddFilter('f_active', $aConfig['lang'][$this->sModule]['f_activation_state'], $aActive, $_POST['f_active']);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
			// pobranie wszystkich rekordow
			$sSql = "SELECT PC.id, PC.code, W.name AS website, PC.code_discount, 
                      ignore_discount_limit,
                      DATE_FORMAT(code_active_from, '".$aConfig['common']['cal_sql_date_format']."') active_from, 
                      DATE_FORMAT(code_expiry, '".$aConfig['common']['cal_sql_date_format']."') expiry, 
                      (SELECT COUNT(id) FROM `".$aConfig['tabls']['prefix']."promotions_codes_to_users` WHERE code_id=PC.id) as used_count, 
                      extra, `descr`, created, created_by, deleted,
                      PCTU.id AS ID_USER,
                      PCTB.id AS ID_BOOK
						 	 FROM ".$aConfig['tabls']['prefix']."promotions_codes AS PC
               LEFT JOIN ".$aConfig['tabls']['prefix']."promotions_codes_to_users AS PCTU
                 ON PC.id = PCTU.code_id
               LEFT JOIN ".$aConfig['tabls']['prefix']."promotions_codes_to_books AS PCTB
                 ON PC.id = PCTB.code_id
               LEFT JOIN websites AS W
                ON W.id = PC.website_id
						 	 WHERE ".$sDeletedSql.
						 	 (isset($_POST['search']) && !empty($_POST['search']) ? " AND (code = '".$_POST['search']."' OR `descr` LIKE '%".$_POST['search']."%')" : '').
						 	 (isset($_POST['f_active']) && $_POST['f_active'] != '' ? ($_POST['f_active'] == '1' ? " AND code_active_from <= NOW() AND code_expiry >= NOW()" : ($_POST['f_active'] == '2' ? " AND code_expiry < NOW()" : " AND code_active_from > NOW()")) : '').
							 ' GROUP BY PC.id 
                 ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'PC.id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " 
                LIMIT ".$iStartFrom.", ".$iPerPage." ";
			$aRecords =& Common::GetAll($sSql);
      
			foreach ($aRecords as $iKey => $aItem) {
				$aRecords[$iKey]['code_discount'] = Common::formatPrice3($aItem['code_discount']);
				
        $bExtra = false;
        $aExtra = unserialize($aItem['extra']);
        if (!empty($aExtra)) {
          foreach ($aExtra as $aExtraVal) {
            if (!empty($aExtraVal)) {
              $bExtra = true;
            }
          }
        }
        if ($bExtra === true) {
          $aRecords[$iKey]['extra'] = ' ograniczenia kategorii lub wydawnictwa lub serii ';
        } else {
          $aRecords[$iKey]['extra'] = '--';
        }
				
        if ($aItem['ID_USER'] > 0) {
          $aRecords[$iKey]['extra'] .= '- zdefiniowano użytkowników '; 
        }
        if ($aItem['ID_BOOK'] > 0) {
          $aRecords[$iKey]['extra'] .= '- dodano książki '; 
        }
        if ($aItem['ignore_discount_limit'] == '1') {
          $aRecords[$iKey]['extra'] .= '- ign. limit rabatu '; 
        }
        
				if ($aItem['deleted'] == '1') {
					$aRecords[$iKey]['code'] = '<span style="color: red;">'.$aItem['code'].'</span>';
					$aRecords[$iKey]['disabled'][] = 'delete';
					$aRecords[$iKey]['disabled'][] = 'edit';
					$aRecords[$iKey]['disabled'][] = 'code';
				}
				unset($aRecords[$iKey]['deleted']);
        unset($aRecords[$iKey]['ID_USER']);
        unset($aRecords[$iKey]['ID_BOOK']);
        unset($aRecords[$iKey]['ignore_discount_limit']);
        if ($aItem['website'] == '') {
          $aRecords[$iKey]['website'] = _('Wszystkie serwisy');
        }
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'code' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('books', 'categories', 'users', 'edit', 'delete'),
					'params' => array (
												'books'	=> array('id'=>'{id}'),
                        'categories'	=> array('id'=>'{id}'),
												'users'	=> array('id'=>'{id}'),
												'send'	=> array('id'=>'{id}'),
												'edit'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											),
					'icon' => array('users' => 'authors'),
					'show' => false
				)
			);
			
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('add')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		);

		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show());
	} // end of Show() function
	

	/**
	 * Metoda usuwa wybrane rekordy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego rekordu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['code'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['code'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton

	
	/**
	 * Metoda dodaje do bazy danych nowy kod rabatowy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
		$sStartDate = FormatDate($_POST['active_from']);
		$sEndDate = FormatDate($_POST['expiry']);
		
		if (!$this->checkDates($sStartDate, $sEndDate)) { 
			// nieprawidlowy zakres godzin dla promocji
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['dates_err']);
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
    
    if ($_POST['code'] == '') {
      $_POST['code'] = $this->generateCode();
    }
		
		$aValues = array(
			'code' => $_POST['code'],
      'website_id' => (isset($_POST['website_id']) && $_POST['website_id'] > 0 ? $_POST['website_id'] : 'NULL'),
			'code_discount' => Common::formatPrice2($_POST['discount']),
			'code_max_use' => (int) $_POST['max_use'],
			'code_active_from' => $sStartDate,
			'code_expiry' => $sEndDate,
			'off_cost_transport' => isset($_POST['off_cost_transport']) ? '1' : '0',
      'ignore_discount_limit' => isset($_POST['ignore_discount_limit']) ? '1' : '0',
      'discount_price_after_promo' => isset($_POST['discount_price_after_promo']) ? '1' : '0',
			'descr' => $_POST['descr'],
			'code_type' => $_POST['code_type'],
			'created' => 'NOW()',
			'created_by' => $_SESSION['user']['name']
		);
		
		// dodanie
		if (($iCId = Common::Insert($aConfig['tabls']['prefix']."promotions_codes", $aValues)) === false) {
			$bIsErr = true;
		}
		
		// metoda transportu
		if ( $this->InsertUpdateFreeTransport($iCId, $_POST['tranport_means'], $_POST['tranport_means_min_value'], true) === false) {
			$bIsErr = true;
		}
		
		if (!$bIsErr) {
			// rekord zostal dodany
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											$_POST['code']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			// rekord nie zostal dodany
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$_POST['code']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId);
		}
	} // end of Insert() funciton
	
	
	/**
	 * Metoda dodaje darmowe metody transportu dla kodu rabatowego oraz 
	 *	wartość minimalna zamówienia (o ile zdefiniowana) po przekroczeniu której 
	 *	zostanie koszty transportu zostaną wyzerowane/usuniete
	 *
	 * @param type $aTransportMeans
	 * @param type $aTransportMeansMinValue 
	 */
	function InsertUpdateFreeTransport($iCodeId, $aTransportMeans, $aTransportMeansMinValue, $bAdd) {
		global $aConfig, $pDbMgr;
		
		if ($bAdd == false) {
			// usuwamy tylko i wyłącznie podczas update
			// usun wstepnie metody 
			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_transport
							WHERE code_id=".$iCodeId;
			if ($pDbMgr->Query('profit24', $sSql) === false) {
				// błąd
				return false;
			}
		}
		if (!empty($aTransportMeans) && is_array($aTransportMeans)) {
			foreach ($aTransportMeans as $iTransport) {
				$aValues = array(
						'code_id' => $iCodeId
				);
				if ($iTransport > 0) {
					$aValues['transport_id'] = $iTransport;
					if (Common::formatPrice2($aTransportMeansMinValue[$iTransport]) > 0.00) {
						$aValues['min_value'] = Common::formatPrice2($aTransportMeansMinValue[$iTransport]);
					}
					if ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix'].'promotions_codes_to_transport', $aValues) === false) {
						return false;
					}
				}
			}
		}
		return true;
	}// end of InsertUpdateFreeTransport() method
	
	
	/**
	 * Metoda aktualizuje w bazie danych kod rabatowy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id kodu
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iId);
			return;
		}
		$sStartDate = FormatDate($_POST['active_from']);
		$sEndDate = FormatDate($_POST['expiry']);
		
		if (!$this->checkDates($sStartDate, $sEndDate)) { 
			// nieprawidlowy zakres dat dla kodu
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['dates_err']);
			$this->AddEdit($pSmarty, $iPId, $iId);
			return;
		}

		
		$aValues = array(
      'website_id' => (isset($_POST['website_id']) && $_POST['website_id'] > 0 ? $_POST['website_id'] : 'NULL'),
			'code_discount' => Common::formatPrice2($_POST['discount']),
			'code_max_use' => (int) $_POST['max_use'],
			'code_active_from' => $sStartDate,
			'code_expiry' => $sEndDate,
			'descr' => $_POST['descr'],
			'off_cost_transport' => isset($_POST['off_cost_transport']) ? '1' : '0',
      'ignore_discount_limit' => isset($_POST['ignore_discount_limit']) ? '1' : '0',
      'discount_price_after_promo' => isset($_POST['discount_price_after_promo']) ? '1' : '0',
			'code_type' => $_POST['code_type']
		);
		// aktualizacja
		if (Common::Update($aConfig['tabls']['prefix']."promotions_codes",
											 $aValues,
											 "id = ".$iId) === false) {
			$bIsErr = true;
		}
		
		// metoda transportu
		if ( $this->InsertUpdateFreeTransport($iId, $_POST['tranport_means'], $_POST['tranport_means_min_value'], false) === false) {
			$bIsErr = true;
		}
		
		if (!$bIsErr) {
			// rekord zostal zmieniony
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$_POST['code']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// rekord nie zostal zmieniony
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$_POST['code']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId, $iId);
		}
	} // end of Update() funciton

  /**
   * Metoda zapusje wybrane opcje kodu rabatowego
   * 
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iId
   */
  private function UpdateCategories($pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
    $aMatched = array();
    $aData = array();
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditCategories($pSmarty, $iId);
			return;
		}
   
    if (!empty($_POST['extra_categories'])) {
      $aData['extra_categories'] = array_values($_POST['extra_categories']);
    } else {
      $aData['extra_categories'] = array();
    }
    
    preg_match_all('/\((\d+)\)/', $_POST['publishers'], $aMatched);
    $aData['publishers'] = $aMatched[1];
    
    preg_match_all('/\((\d+)\)/', $_POST['series'], $aMatched);
    $aData['series'] = $aMatched[1];
    
    if (!empty($aData['series']) || !empty($aData['publishers']) || !empty($aData['extra_categories'])) {
      // coś tam jest
      $aValues = array(
          'extra' => serialize($aData)
      );
    } else {
      // puste
      $aValues = array(
          'extra' => ''
      );
    }
    if (Common::Update('promotions_codes', $aValues, 'id = '.$iId) === false) {
      $bIsErr = true;
    }
    
    $sCode = $this->getItemName($iId);
    
		if (!$bIsErr) {
			// rekord zostal zmieniony
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$sCode);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
      return true;
		}
		else {
			// rekord nie zostal zmieniony
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$sCode);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEditCategories($pSmarty, $iId);
		}
  }// end of UpdateCategories() method
  
  
  /**
   * Metoda pobiera dodatkowe dane dla kodu rabatowego
   * 
   * @global array $aConfig
   * @param int $iId
   * @return array
   */
  private function getCodeExtraData($iId) {
    
    $sSql = "SELECT extra FROM promotions_codes
             WHERE id = ".$iId;
    $sExtra = Common::GetOne($sSql);
    if ($sExtra != '' && !empty($sExtra)) {
      return unserialize($sExtra);
    }
    return array();
  }// end of getCodeExtraData() method
  
  
  /**
   * Metoda pobiera tablice id serii i zwraca sformatowane dane do pola tekstowego
   * 
   * @global type $aConfig
   * @param array $aIds
   * @return string
   */
  private function getSeriesNameTextarea($aIds) {
    global $aConfig;
    
    $sSql = "SELECT CONCAT( A.name, '(', B.name, ') ', '(', A.id, ')' )
					 FROM ".$aConfig['tabls']['prefix']."products_series AS A
             JOIN ".$aConfig['tabls']['prefix']."products_publishers AS B
               ON A.publisher_id = B.id
					 WHERE A.id IN (".implode(',', $aIds).")";
    $aSeries = Common::GetCol($sSql);
    return implode("\n", $aSeries);
  }// end of getSeriesNameTextarea() method
  
  
  /**
   * Metoda pobiera tablice id serii i zwraca sformatowane dane do pola tekstowego
   * 
   * @global type $aConfig
   * @param array $aIds
   * @return string
   */
  private function getPublishersNameTextarea($aIds) {
    global $aConfig;
    
    $sSql = "SELECT CONCAT(name, ' (', id, ')' )
             FROM ".$aConfig['tabls']['prefix']."products_publishers 
             WHERE id IN (".implode(',', $aIds).")";
    $aPub = Common::GetCol($sSql);
    return implode("\n", $aPub);
  }// end of getPublishersNameTextarea() method
  
  
  /**
   * Metoda generuje formularz wyboru kategorii/wydawcy/serii dla kodów rabatowych
   * 
   * @global type $aConfig
   * @param type $pSmaty
   * @param type $iId
   */
  private function AddEditCategories(&$pSmarty, $iId) {
    global $aConfig;
    
    include_once('modules/m_oferta_produktowa/Module_Common.class.php');
    $oCommon = new Module_Common();
    
    // dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$pForm = new FormTable('categories', $aConfig['lang'][$this->sModule]['header_categories'], 
                              array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), $aConfig['common']['js_validation']);
    $pForm->AddHidden('do', 'update_categories');
    
    if (empty($_POST)) {
      $aData = $this->getCodeExtraData($iId);
      if (!empty($aData['series']) && is_array($aData['series'])) {
        $aData['series'] = $this->getSeriesNameTextarea($aData['series']);
      } else {
        $aData['series'] = '';
      }
      if (!empty($aData['publishers']) && is_array($aData['publishers'])) {
        $aData['publishers'] = $this->getPublishersNameTextarea($aData['publishers']);
      } else {
        $aData['publishers'] = '';
      }
    } else {
      $aData = $_POST;
    }
    
    // drzewo kategorii
		$pForm->AddRow($aConfig['lang'][$this->sModule]['code_categories'], '<div style="height:300px; overflow:auto;">' .
						Form::GetCheckTree('extra_categories[]', array('free_choice' => true), $oCommon->getProductsCategoriesChTree(Common::getProductsMenu($this->iLangId)), $aData['extra_categories'])
						. '</div>'
		);
    $pForm->AddRow($aConfig['lang'][$this->sModule]['new_publisher'], 
      $pForm->GetTextHTML('publisher', $aConfig['lang'][$this->sModule]['publisher'], '', array('id' => 'publisher_aci', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', false).
      $pForm->GetInputButtonHTML('add_publisher', $aConfig['lang'][$this->sModule]['add_publisher_button'], array('id' => 'add_publisher_button'), 'button'));
    
    $pForm->AddTextArea('publishers', $aConfig['lang'][$this->sModule]['publishers'], $aData['publishers'], array('id' => 'publishers_list', 'rows' => '8', 'style' => 'width: 450px;'), 0, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);
    
    
    $pForm->AddRow($aConfig['lang'][$this->sModule]['new_series'], 
      $pForm->GetTextHTML('serie', $aConfig['lang'][$this->sModule]['serie'], '', array('id' => 'serie_aci', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', false).
      $pForm->GetInputButtonHTML('add_serie', $aConfig['lang'][$this->sModule]['add_serie_button'], array('id' => 'add_serie_button'), 'button'));
    
    $pForm->AddTextArea('series', $aConfig['lang'][$this->sModule]['series'], $aData['series'], array('id' => 'series_list', 'rows' => '8', 'style' => 'width: 450px;'), 0, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);
    
    // przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		
    		$sJS = '
					<script type="text/javascript">
						$(document).ready(function(){
              function addPub() {
                var sCur = $("#publishers_list").val();
                var sNew = $("#publisher_aci").val();
                if(sNew != ""){
                  if(sCur != ""){
                    sCur = sCur+"\n";
                  }
                  $("#publishers_list").val(sCur+sNew);
                  $("#publisher_aci").val("");
                }
              }
              
                $("#publisher_aci").autocomplete({
                    source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetPublishers.php?opt=id",
                    select: function( event, ui ) {
                      $("#add_publisher_button").focus();
                    }
                });

                $("#add_publisher_button").click(function () {
                  addPub();
                });
                
                function addSeries() {
                  var sCur = $("#series_list").val();
                  var sNew = $("#serie_aci").val();
                  if(sNew != ""){
                    if(sCur != ""){
                      sCur = sCur+"\n";
                    }
                    $("#series_list").val(sCur+sNew);
                    $("#serie_aci").val("");
                  }
                }


                $("#serie_aci").autocomplete({
                    source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetSeriesFix.php",
                    select: function( event, ui ) {
                      $("#add_serie_button").focus();
                    }
                });
                
                $("#add_serie_button").click(function () {
                  addSeries();
                });
                
            });
          </script>

';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()).$sJS);
  }
	
	/**
	 * Metoda tworzy formularz dodawania / edycji kodu rabatowego
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- ID strony edytowanego rekordu
	 * @param	integer	$iId	- ID edytowanego rekordu
	 */
	function AddEdit(&$pSmarty, $iPId, $iId=0) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if ($iId > 0) {
			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT code, website_id, code_discount discount, code_max_use max_use, off_cost_transport, ignore_discount_limit, discount_price_after_promo, DATE_FORMAT(code_active_from, '".$aConfig['common']['cal_sql_date_format']."') active_from, DATE_FORMAT(code_expiry, '".$aConfig['common']['cal_sql_date_format']."') expiry, `descr`,code_type
							 FROM ".$aConfig['tabls']['prefix']."promotions_codes
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
			
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_transport
							 WHERE code_id = ".$iId;
			$aCodesTransport =& Common::GetAll($sSql);
			foreach ($aCodesTransport as $iKey => $aCodes) {
				$aData['tranport_means'][$aCodes['transport_id']] = $aCodes['transport_id'];
				$aData['tranport_means_min_value'][$aCodes['transport_id']] = $aCodes['min_value'];
			}
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0) {
			$sHeader .= ' "'.$aData['code'].'"';
		}
		
		$pForm = new FormTable('products', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
    
    if ($iId > 0) {
      $pForm->AddHidden('code', $aData['code']);
    } else {
      if (empty($_POST)) {
        $aData['code'] = $this->generateCode();
      }
      // kod rabatowy
      $pForm->AddText('code', $aConfig['lang'][$this->sModule]['code'], $aData['code'], array('maxlength'=>12, 'style'=>'width: 180px;'), '', 'text', true);
    }
    
    $pForm->AddSelect('website_id', _('Serwis'), [], addDefaultValue($this->getWebsites()), $aData['website_id'], '', false);
    
		// wartosc rabatu kodu
		$pForm->AddText('discount', $aConfig['lang'][$this->sModule]['discount'], isset($aData['discount']) && Common::formatPrice2($aData['discount']) > 0 ? Common::formatPrice3($aData['discount']) : '', array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'range', false, '', array(1, 100));
    $pForm->AddInfo($aConfig['lang'][$this->sModule]['discount_price_after_promo_info']);

    // kod wyłącza koszty transportu
		$pForm->AddCheckBox('discount_price_after_promo', $aConfig['lang'][$this->sModule]['discount_price_after_promo'], array(), '', !empty($aData['discount_price_after_promo']), false);
    
    // ignoruj limit rabatu
		$pForm->AddCheckBox('ignore_discount_limit', $aConfig['lang'][$this->sModule]['ignore_discount_limit'], array(), '', !empty($aData['ignore_discount_limit']), false);
    
		// data poczatku obowiazywania kodu
		$pForm->AddText('active_from', $aConfig['lang'][$this->sModule]['active_from'], $aData['active_from'], array('style'=>'width: 75px;'), '', 'date');
		
		// dzien konca obowiazywania kodu
		$pForm->AddText('expiry', $aConfig['lang'][$this->sModule]['expiry'], $aData['expiry'], array('style'=>'width: 75px;'), '', 'date');
		
		// opis
		$pForm->AddText('descr', $aConfig['lang'][$this->sModule]['desc'], $aData['descr'], array('maxlength'=>120, 'style'=>'width: 180px;'), '', 'text', false);
		
		// kod wyłącza koszty transportu
		$pForm->AddCheckBox('off_cost_transport', $aConfig['lang'][$this->sModule]['off_cost_transport'], array('onclick' => 'ChangeObjValue(\'do\', \'change_ctype\'); this.form.submit();'), '', !empty($aData['off_cost_transport']), false);
    
		// sekcja wyboru darmowej metody transportu
		if ($aData['off_cost_transport'] == '1') {
			// wybierz metody transportu dla ktorych kod rabatowy wyłącza koszty transportu
			$aTransportMeans = $this->getTransportMeansList();
			
			$sRadioSetHTML = '';
			foreach ($aTransportMeans as $iKey => $aTransport) {
				
				$iTrasportId = $aTransport['value'];// UWAGA zastąpienie tutaj klucza
				$sRadioSetHTML .= '<div style="text-align: left;">';
				$sRadioSetHTML .= $pForm->GetCheckboxHTML('tranport_means['.$iTrasportId.']', $aTransport['label'],
															array('value' => $aTransport['value']), '', $aData['tranport_means'][$iTrasportId], false, true);
				$sRadioSetHTML .= '<strong>'.$pForm->GetLabelHTML('tranport_means['.$iTrasportId.']', $aTransport['label'], false).'</strong>';
				
				$sRadioSetHTML .= $pForm->GetLabelHTML('tranport_means['.$iTrasportId.']', 'od kwoty: ', false);
				$sRadioSetHTML .= $pForm->GetTextHTML('tranport_means_min_value['.$iTrasportId.']', $aConfig['lang'][$this->sModule]['min_value'], $aData['tranport_means_min_value'][$iTrasportId], array('style' => 'width: 60px;', 'maxlength' => '8'), '', 'ufloat', false);
				//$sRadioSetHTML .= ;
				
				$sRadioSetHTML .= '<br /></div>';
				
			}
			
			$pForm->AddRow('Wyłącz koszty transportu dla metod transportu: ', $sRadioSetHTML);
		}
		// status
		$aCodeTypes = array(
			array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['ctype_0'], 'onclick' => 'ChangeObjValue(\'do\', \'change_ctype\'); this.form.submit();'),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['ctype_1'], 'onclick' => 'ChangeObjValue(\'do\', \'change_ctype\'); this.form.submit();'),
			array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['ctype_2'], 'onclick' => 'ChangeObjValue(\'do\', \'change_ctype\'); this.form.submit();'),
			array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['ctype_3'], 'onclick' => 'ChangeObjValue(\'do\', \'change_ctype\'); this.form.submit();')
		);
		$pForm->AddRadioSet('code_type', $aConfig['lang'][$this->sModule]['code_type'], $aCodeTypes, $aData['code_type'], '', true, false);
		
		if($aData['code_type'] == '1' || $aData['code_type'] == '3') {
			// maksymalna liczba uzycia kodu przez jednego uzytkownika
			$pForm->AddText('max_use', $aConfig['lang'][$this->sModule]['max_use'], isset($aData['max_use']) ? $aData['max_use'] : '', array('maxlength' => 3, 'style'=>'width: 50px;'), '', 'range', true, '', array(0, 999));
		
		}
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
  
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @return array
   */
  private function getWebsites() {
    global $pDbMgr;
    
    $sSql = 'SELECT name AS label, id AS value FROM websites ORDER BY id ASC';
    return $pDbMgr->GetAll('profit24', $sSql);
  }
	
	/**
	 * Metoda zwraca dostępne metody transportu
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @return type 
	 */
	function getTransportMeansList() {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT id AS value, name AS label 
						 FROM orders_transport_means WHERE personal_reception = '0'";
		return $pDbMgr->GetAll('profit24', $sSql);
	}// end of getTransportMeansList() method
	
	
	/**
	 * Metoda wyswietla liste uzytkownikow korzystajacych z kodu o podanym Id
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function ShowUsers(&$pSmarty, $iId) {
		global $aConfig;
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		if (!empty( $_SESSION['_viewState']['m_zamowienia_codes']['get']['do'] )) {
			$_SESSION['_viewState']['m_zamowienia_codes']['get']['page'] = '1';
		}
		// zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['code_users_list'], $this->getItemName($iId)),
			'action' => phpSelf(array('action' => 'codes', 'do' => 'users', 'id' => $iId)),
			'refresh_link' => phpSelf(array('action' => 'codes', 'do' => 'users', 'id' => $iId, 'reset' => '1')),
			'refresh'	=> true,
			'search'	=> false,
			'checkboxes'	=> false,
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'nazwa',
				'content'	=> $aConfig['lang'][$this->sModule]['list_nazwa'],
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'time_of_use',
				'content'	=> $aConfig['lang'][$this->sModule]['list_time_of_use'],
				'sortable'	=> false,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'used',
				'content'	=> $aConfig['lang'][$this->sModule]['list_used'],
				'sortable'	=> false,
				'width'	=> '100'
			)
		);
		
		// okreslenie liczby wszystkich uzytkownikow kodu
		$sSql = "SELECT COUNT(1)
						 FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_users
						 WHERE code_id = ".$iId."
						 GROUP BY user_id";
						 //(isset($_POST['search']) && !empty($_POST['search']) ? " AND konto_nazwa LIKE '%".$_POST['search']."%'" : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(1)
							 FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_users
							 WHERE code_id = ".$iId."
							 GROUP BY user_id";
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('code_users', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount, true);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
			// pobranie wszystkich rekordow
			$sSql = "SELECT A.user_id, B.email AS konto_nazwa, A.time_of_use, COUNT(A.id) AS used
						 	 FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_users A
						 	 LEFT JOIN ".$aConfig['tabls']['prefix']."users_accounts B
						 	 ON B.id=A.user_id
						 	 WHERE A.code_id = ".$iId.
						 	 			// (isset($_POST['search']) && !empty($_POST['search']) ? " AND konto_nazwa LIKE '%".$_POST['search']."%'" : '').
							 ' GROUP BY user_id 
							 ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'time_of_use DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'user_id'	=> array(
					'show'	=> false
				)
				);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		else {
			$aConfig['lang']['m_promocje_codes']['no_items'] = $aConfig['lang']['m_promocje_codes']['no_code_users'];
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('go_back')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('id'))
		);
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show());
	} // end of ShowUsers() function
	
	
	/**
	 * Metoda tworzy formularz okreslania uzytkownikow,
	 * ktorzy spelniaja okreslone zalozenia:
	 * przedzial wartosci zlozonych dotychczas zamowien, przedzial
	 * czasowy zlozonych zamowien, nazwa uzytkownika zawiera, uzytkownik kupil
	 * okreslona ksiazke, uzytkownik kupil ksiazki z okreslonej serii
	 * lub
	 * adres email uzytkownika zawiera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id kodu rabatowego
	 * @param	bool	$bSearch	- true: wyswietl uzytkownikow
	 */
	function ChooseUsersToSend(&$pSmarty, $iId, $bSearch=false) {
		global $aConfig;
		$sHtml = '';
		$aUsers = array();
		
		if ($bSearch) {
			// wyszukiwanie uzytkownikow spelniajacych zadane kryteria
			$aUsers =& $this->getUsersToSend(trim($_POST['name_includes']), trim($_POST['email_includes']), Common::formatPrice2($_POST['value_from']), Common::formatPrice2($_POST['value_to']), $_POST['date_from'], $_POST['date_to'], Common::formatPrice2($_POST['current_discount']), intval($_POST['book_id']), intval($_POST['series_id']));
		}
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['users_to_send_header'],
											 $this->getItemName($iId));
		
		$pForm = new FormTable('users_to_send', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'proceed_send');
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['conditions_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		
		// nazwa zawiera
		$pForm->AddText('name_includes', $aConfig['lang'][$this->sModule]['name_includes'], isset($_POST['name_includes']) && $_POST['name_includes'] != '' ? $_POST['name_includes'] : '', array('style'=>'width: 250px;'), '', 'text', false);
		
		// zakupil ksiazke
		$pForm->AddSelect('book_id', $aConfig['lang'][$this->sModule]['book_id'], array(), $this->getProducts(), $_POST['book_id'], '', false);
		
		// zakupil ksiazke/i z serii
		$pForm->AddSelect('series_id', $aConfig['lang'][$this->sModule]['series_id'], array(), $this->getSeries(), $_POST['series_id'], '', false);
		
		// przedzial wartosci zlozonych do tej pory zamowien
		$pForm->AddRow($aConfig['lang'][$this->sModule]['value_range'], $pForm->GetTextHTML('value_from', '', isset($_POST['value_from']) && $_POST['value_from'] != '' ? Common::formatPrice3($_POST['value_from']) : '', array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'ufloat', false).'&nbsp;&nbsp;&nbsp;&nbsp;'.$aConfig['lang'][$this->sModule]['value_to'].'&nbsp;&nbsp;'.$pForm->GetTextHTML('value_to', '', isset($_POST['value_to']) && $_POST['value_to'] != '' ? Common::formatPrice3($_POST['value_to']) : '', array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'ufloat', false));
		
		// przedzial czasowy w ktorym zostaly zlozone zamowienia
		$pForm->AddRow($aConfig['lang'][$this->sModule]['date_range'], '<table border="0"><tr><td style="border: 0;">'.$pForm->GetTextHTML('date_from', '', isset($_POST['date_from']) && $_POST['date_from'] != '' ? $_POST['date_from'] : '00-00-0000', array('style' => 'width: 75px;'), '', 'date', false).'</td><td style="border: 0;">'.$aConfig['lang'][$this->sModule]['date_to'].'</td><td style="border: 0;">'.$pForm->GetTextHTML('date_to', '', isset($_POST['date_to']) && $_POST['date_to'] != '' ? Common::formatPrice3($_POST['date_to']) : '00-00-0000', array('style' => 'width: 75px;'), '', 'date', false).'</td></tr></table>');
		
		// lub
		$pForm->AddRow('&nbsp;', $aConfig['lang'][$this->sModule]['or']);
		
		// adres e-mail zawiera
		$pForm->AddText('email_includes', $aConfig['lang'][$this->sModule]['email_includes'], isset($_POST['email_includes']) && $_POST['email_includes'] != '' ? $_POST['email_includes'] : '', array('style'=>'width: 150px;'), '', 'text', false);
		
		// lub
		$pForm->AddRow('&nbsp;', $aConfig['lang'][$this->sModule]['or']);
		
		// aktualny rabat rabat
		$pForm->AddText('current_discount', $aConfig['lang'][$this->sModule]['current_discount'], isset($_POST['current_discount']) && floatval($_POST['current_discount']) > 0 ? Common::formatPrice3($_POST['current_discount']) : '', array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat', false);
		
		// przycisk wyszukaj
		$pForm->AddRow('&nbsp;', '<div style="margin-left: 215px;">'.$pForm->GetInputButtonHTML('get_users_to_send', $aConfig['lang'][$this->sModule]['button_show_users'], array('onclick'=>'document.getElementById(\'do\').value=\'get_users_to_send\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'</div>');
		
		if ($bSearch) {
			// wyswietlenie listy pasujacych uzytkownikow
			$pForm->AddMergedRow($this->GetUsersList($pSmarty, $aUsers));
		}
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['message_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		
		// temat maila
		$pForm->AddText('subject', $aConfig['lang'][$this->sModule]['subject'], $_POST['subject'], array('style'=>'width: 350px;'));
		
		// informacja o tagach na kod
		$pForm->AddRow('&nbsp;', $aConfig['lang'][$this->sModule]['message_code_info']);
		
		// tresc TXT
		$pForm->AddTextArea('body_txt', $aConfig['lang'][$this->sModule]['body_txt'], $_POST['body_txt'], array('rows'=>'8', 'style'=>'width: 450px;'), 0, '', '', false);
		
		// tresc HTML
		$pForm->AddWYSIWYG('body_html', $aConfig['lang'][$this->sModule]['body_html'], $_POST['body_html'], array('strip_urls' =>'0'), '', false);

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_send_code']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of ChooseUsersToSend() function
	
	
	/**
	 * Metoda pobiera liste uzytkownikow spelniajacych zadane kryteria
	 * 
	 * @param	string	$sNameIncludes	- nazwa uzytkownika zawiera
	 * @param	string	$sEmailIncludes	- email uzytkownika zawiera
	 * @param	float	$fValueFrom	- poczatkowy zakres wartosci zamowien
	 * @param	float	$fValueTo	- koncowy zakres wartosci zamowien
	 * @param	string	$sDateFrom	- poczatkowy zakres dat skladanych zamowien
	 * @param	string	$sDateTo	- koncowy zakres dat skladanych zamowien
	 * @param	float	$fCurrentDiscout	- aktualny rabat
	 * @return	array ref
	 */
	function &getUsersToSend($sNameIncludes, $sEmailIncludes, $fValueFrom, $fValueTo, $sDateFrom, $sDateTo, $fCurrentDiscount, $iProdId, $iSeriesId) {
		global $aConfig, $pDB2;
		$aUsers = array();
		
		/** DB 2 CONFIG **/
		$sDSN2 = $aConfig['db']['dbns'].'://'.$aConfig['db2']['user'].':'
						.$aConfig['db2']['passwd'].'@'.$aConfig['db2']['host'].'/'.$aConfig['db2']['name'];
		$pDB2 = DB::connect($sDSN2);
		if (DB::isError($pDB2)) {
			die("Could not connect to database 2!");
		}
		
		
		if ($sNameIncludes != '' || $iProdId > 0 || $iSeriesId > 0 || ($fValueFrom > 0 && $fValueTo > 0 && $fValueTo > $fValueFrom) || ($sDateFrom != '' && $sDateTo != '')) {
			$_POST['current_discount'] = '';
			$_POST['email_includes'] = '';
			$sDateSql = '';
			$sIncludesSql = '';
			$sProdSeriesSql = '';
			$sHavingSql = '';
			if ($sNameIncludes != '') {
				$sIncludesSql = " AND company LIKE '%".$sNameIncludes."%'";
			}
			if ($fValueFrom > 0 && $fValueTo > 0 && $fValueTo > $fValueFrom) {
				$sHavingSql = " HAVING ile >= ".$fValueFrom." AND ile <= ".$fValueTo;
			}
			if ($sDateFrom != '' && $sDateTo != '') {
				$sDateSql = " AND DATE_FORMAT(order_date, '%Y-%m-%d') >= '".FormatDate($sDateFrom)."' AND DATE_FORMAT(order_date, '%Y-%m-%d') <= '".FormatDate($sDateTo)."'";
			}
			if ($iProdId > 0) {
				$_POST['series_id'] = '';
				$sProdSeriesSql = " AND id IN (SELECT order_id FROM ".$aConfig['tabls']['prefix']."orders_items WHERE product_id = ".$iProdId.")";
			}
			elseif ($iSeriesId > 0) {
					$sProdSeriesSql = " AND id IN (SELECT A.order_id FROM ".$aConfig['tabls']['prefix']."orders_items A JOIN ".$aConfig['tabls']['prefix']."products B ON A.product_id = B.id WHERE B.series_id = ".$iSeriesId.")";
			}
			if ($sProdSeriesSql != '' || $sIncludesSql != '' || $sDateSql != '' || $sHavingSql != '') {
				// pobranie ID uzytkownikow spelniajacych zadane kryteria
				$sSql = "SELECT konto_id, SUM(value_brutto) ile
								 FROM ".$aConfig['tabls']['prefix']."orders
								 WHERE konto_id IS NOT NULL AND konto_id > 0".$sProdSeriesSql.$sIncludesSql.$sDateSql."
								 GROUP BY konto_id".$sHavingSql;
				$aOrders =& Common::GetAssoc($sSql);
				if (!empty($aOrders)) {
					// pobranie uzytkownikow spelniajacych zadane kryteria
					$sSql = "SELECT id, nazwa, login, email, ksiegarnia_rabat
									 FROM ".$aConfig['tabls']['users_accounts']."
									 WHERE id IN (".implode(',', array_keys($aOrders)).")";
					$aUsers =& Common::GetAll2($sSql);
					
					foreach ($aUsers as $iKey => $aUser) {
						if (!Common::isUTF8($aUser['nazwa']))
							$aUsers[$iKey]['nazwa'] = Common::convert($aUser['nazwa']);
						$aUsers[$iKey]['nazwa'] = str_replace('\\', '', $aUsers[$iKey]['nazwa']);
						if (!Common::isUTF8($aUser['login']))
							$aUsers[$iKey]['login'] = Common::convert($aUser['login']);
						$aUsers[$iKey]['ksiegarnia_rabat'] = Common::formatPrice3($aUser['ksiegarnia_rabat']).' %';
						$aUsers[$iKey]['wartosc_zamowien'] = Common::formatPrice3($aOrders[$aUser['id']]).' '.$aConfig['lang']['common']['currency'];
					}
				}
			}
		}
		elseif ($fCurrentDiscount > 0 || $sEmailIncludes != '') {
			$_POST['name_includes'] = '';
			$_POST['value_from'] = '';
			$_POST['value_to'] = '';
			$_POST['date_from'] = '00-00-0000';
			$_POST['date_to'] = '00-00-0000';
			$_POST['book_id'] = '';
			$_POST['series_id'] = '';
			
			if ($sEmailIncludes != '') {
				$_POST['current_discount'] = '';
				// pobranie uzytkownikow ktorych adres email zawiera okreslona fraze
				$sSql = "SELECT id uid, id, nazwa, login, email, ksiegarnia_rabat
								 FROM ".$aConfig['tabls']['users_accounts']."
								 WHERE email LIKE '%".$sEmailIncludes."%'";
				$aUsers =& Common::GetAssoc2($sSql, true);
			}
			elseif ($fCurrentDiscount > 0) {
				$_POST['email_includes'] = '';
				// pobranie uzytkownikow posiadajacych okreslony rabat
				$sSql = "SELECT id uid, id, nazwa, login, email, ksiegarnia_rabat
								 FROM ".$aConfig['tabls']['users_accounts']."
								 WHERE ksiegarnia_rabat = ".$fCurrentDiscount;
				$aUsers =& Common::GetAssoc2($sSql, true);
			}
			// pobranie wartosci zamowien uzytkownikow
			$sSql = "SELECT konto_id, SUM(value_brutto) ile
							 FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE konto_id IN (".(!empty($aUsers) ? implode(',', array_keys($aUsers)) : '0').")
							 GROUP BY konto_id";
			$aOrders =& Common::GetAssoc($sSql);
			foreach ($aUsers as $iKey => $aUser) {
				if (!Common::isUTF8($aUser['nazwa']))
					$aUsers[$iKey]['nazwa'] = Common::convert($aUser['nazwa']);
				$aUsers[$iKey]['nazwa'] = str_replace('\\', '', $aUsers[$iKey]['nazwa']);
				if (!Common::isUTF8($aUser['login']))
					$aUsers[$iKey]['login'] = Common::convert($aUser['login']);
				$aUsers[$iKey]['ksiegarnia_rabat'] = Common::formatPrice3($aUser['ksiegarnia_rabat']).' %';
				$aUsers[$iKey]['wartosc_zamowien'] = Common::formatPrice3($aOrders[$aUser['id']]).' '.$aConfig['lang']['common']['currency'];
			}
		}
		return $aUsers;
	} // end of getUsersToSend() method
	
	
	/**
	 * Metoda wyswietla liste uzytkownikow z tablicy $aUsers
	 *
	 * @param		object	$pSmarty
	 * @param	array ref	$aUsers	- lista uzytkownikow
	 * @return	void
	 */
	function GetUsersList(&$pSmarty, &$aUsers) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'form'	=>	false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'width'	=> '20'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_id'],
				'width' => '30'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_name'],
				'width' => '150'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_login']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_email']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_discount']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_orders_value']
			)
		);

		$pView = new View('users_to_send', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array();
		// dodanie rekordow do widoku
		$pView->AddRecords($aUsers, $aColSettings);
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all')
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		// przepisanie langa - info o braku rekordow
		$aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_users_to_show'];
		return $pView->Show();
	} // end of GetUsersList() function
	
	
	/**
	 * Metoda wysyla wiadomosc z kodem rabatowym do grupy uzytkownikow
	 * 
	 * @param	integer	$iId	- Id kodu rabatowego
	 * @return	void
	 */
	function proceedSend(&$pSmarty, $iId) {
		global $aConfig, $pDB2;
		set_time_limit(3600);
		$iSent = 0;
		
		if (empty($_POST['delete'])) {
			// nie zaznaczono zadnego uzytkownika
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_users_checked_err'], true);
			$this->ChooseUsersToSend($pSmarty, $iId, true);
			return;
		}
		
		if (!($bTxt = !(strpos($_POST['body_txt'], '{kod_rabatowy}') === false)) && !($bHtml = !(strpos($_POST['body_html'], '{kod_rabatowy}') === false))) {
			// brak tresci wiadomosci
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_body_err'], true);
			$this->ChooseUsersToSend($pSmarty, $iId, true);
			return;
		}
				
		// nazwy atrybutow do pobrania
		$sAttribs = 'email, nazwa, login';
		// kod rabatowy
		$sCode = $this->getItemName($iId);
		// nazwa pliku - otwarcie / utworzenie
		$sFileName = 'kod_rabatowy_'.$sCode.'__rozsylka_'.date('dmY_His');
		$sLogFile = $this->sLogsDir.'/'.$sFileName;
		// odwrocenie tablicy z ID
		$aUIDs = array_keys($_POST['delete']);

		if ($oF = @fopen($sLogFile, 'w')) {
			// polaczenie do drugiej bazy	
			/** DB 2 CONFIG **/
			$sDSN2 = $aConfig['db']['dbns'].'://'.$aConfig['db2']['user'].':'
							.$aConfig['db2']['passwd'].'@'.$aConfig['db2']['host'].'/'.$aConfig['db2']['name'];
			$pDB2 = DB::connect($sDSN2);
			if (DB::isError($pDB2)) {
				die("Could not connect to database 2!");
			}
			
			// zapis daty
			fwrite($oF, sprintf($aConfig['lang'][$this->sModule]['log_date'], date('d-m-Y H:i:s')));
			// zapis kodu rabatowego
			fwrite($oF, sprintf($aConfig['lang'][$this->sModule]['log_code'], $sCode));
			// zapis warunkow dla ktorych wybrano uzytkownikow do nadania rabatu
			fwrite($oF, $this->getConditionsString(trim($_POST['name_includes']), trim($_POST['email_includes']), Common::formatPrice2($_POST['value_from']), Common::formatPrice2($_POST['value_to']), $_POST['date_from'], $_POST['date_to'], Common::formatPrice2($_POST['current_discount']), intval($_POST['book_id']), intval($_POST['series_id'])));
			// naglowek rekordow
			fwrite($oF, $aConfig['lang'][$this->sModule]['log_users_header']);
			
			foreach ($aUIDs as $iId) {
				$aData =& $this->getUserAttribs($iId, $sAttribs);
				// zamiana {kod_rabatowy} na kod w tresci wiadomosci
				$sContent = stripslashes(str_replace('{kod_rabatowy}', $sCode, $_POST[$bTxt ? 'body_txt' : 'body_html']));
				if (Common::sendMail('', $aConfig['default']['website_users_email'], $aData['email'], changeCharcodes(stripslashes($_POST['subject']), 'utf8', 'no'), $sContent, !$bTxt)) {
					// wiadomosc wyslana
					$iSent++;
					// dodanie informacji do pliku
					fwrite($oF, $this->getUserLogData($iId, $aData));
				}				
			}
			fclose($oF);
		}
		if ($iSent > 0) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['send_msg_ok'],
											$sCode,
											$iSent,
											$sFileName);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// nie udalo sie wyslac wiadomosci
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['send_msg_err'],
											$sCode);
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
			$this->Show($pSmarty);
		}
	} // end of proceedSend() method
	
	
	/**
	 * Metoda zwraca string z informacja na temat kryteriow na podstawie ktorych
	 * przyznano rabat uzytkownikom
	 * 
	 * @param	string	$sNameIncludes	- nazwa uzytkownika zawiera
	 * @param	string	$sEmailIncludes	- email uzytkownika zawiera
	 * @param	float	$fValueFrom	- poczatkowy zakres wartosci zamowien
	 * @param	float	$fValueTo	- koncowy zakres wartosci zamowien
	 * @param	string	$sDateFrom	- poczatkowy zakres dat skladanych zamowien
	 * @param	string	$sDateTo	- koncowy zakres dat skladanych zamowien
	 * @param	float	$fCurrentDiscout	- aktualny rabat
	 * @return	string
	 */
	function &getConditionsString($sNameIncludes, $sEmailIncludes, $fValueFrom, $fValueTo, $sDateFrom, $sDateTo, $fCurrentDiscount, $iBookId, $iSeriesId) {
		global $aConfig;
		$sStr = $aConfig['lang'][$this->sModule]['log_conditions'];
		
		if ($sNameIncludes != '' || ($fValueFrom > 0 && $fValueTo > 0 && $fValueTo > $fValueFrom) || ($sDateFrom != '' && $sDateTo != '')) {
			if ($sNameIncludes != '') {
				$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_name_includes'], $sNameIncludes);
			}
			if ($iBookId > 0) {
				$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_book'], $iBookId, parent::getItemName($iBookId));
			}
			elseif ($iSeriesId > 0) {
				$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_series'], $this->getSeriesName($iSeriesId));
			}
			$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_value_range'], Common::formatPrice3($fValueFrom), Common::formatPrice3($fValueTo));
			if ($sDateFrom != '' && $sDateTo != '') {
				$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_date_range'], $sDateFrom, $sDateTo);
			}
		}
		elseif ($sEmailIncludes != '') {
			$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_email_includes'], $sEmailIncludes);
		}
		elseif ($fCurrentDiscount > 0) {
			$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_current_discount'], $fCurrentDiscount);
		}
		return $sStr."\n";
	} // end of getConditionsString() method
	
	
	/**
	 * Metoda formatuje i zwraca dane uzytkownika na potrzeby logow
	 *
	 * @param	integer	$iId	- Id uzytkownika
	 * @param	array ref	$aData	- dane uzytkownika
	 * @return	string
	 */
	function getUserLogData($iId, &$aData) {
		global $aConfig;
		
		$aData['nazwa'] = str_replace('\\', '', Common::convert($aData['nazwa']));
		$aData['login'] = Common::convert($aData['login']);
			
		return sprintf($aConfig['lang'][$this->sModule]['log_user_data'], $iId, $aData['nazwa'], $aData['login'], $aData['email']);
	} // end of getUserLogData() method
	

	/**
	 * Metoda pobiera nazwe serii o podanym Id
	 * 
	 * @param	integer	$iId	- Id serii
	 * @return	array ref
	 */
	function getSeriesName($iId) {
		global $aConfig;
		
		$sSql = "SELECT name
						 FROM ".$aConfig['tabls']['prefix']."products_series
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getSeriesName() method
	
	
	/**
	 * Metoda pobiera i zwraca wartosci podanych atrybutow z konta uzytkownia
	 *
	 * @param	integer	$iId	- Id uzytkownika
	 * @param	string	$sAttribs	- nazwa atrybutow (jezeli wiecej niz jeden - 
	 * 														oddzielone przeinkami - jak do zapytania SQL
	 * @return	array
	 */
	function getUserAttribs($iId, $sAttribs) {
		global $aConfig;

		$sSql = "SELECT ".$sAttribs."
				 		 FROM ".$aConfig['tabls']['users_accounts']."
					 	 WHERE id =  ".$iId;
		return Common::GetRow2($sSql);
	} // end of getUserAttribs() method
	
	
	/**
	 * Metoda pobiera kod
	 * 
	 * @param	integer	$iId	- Id rekordu
	 * @return	array ref
	 */
	function getItemName($iId) {
		global $aConfig;
		
		$sSql = "SELECT code
						 FROM ".$aConfig['tabls']['prefix']."promotions_codes
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getItemName() method
	
	
	/**
	 * Metoda sprawdza czy wprowadzono poprawne daty - data poczatkowa
	 * wczesniejsza od daty koncowej
	 * 
	 * @param	string	$sStartDate	- data poczatkowa
	 * @param	integer	$sEndDate	- data koncowa
	 * @return	bool	- true: OK; false: niepoprawne daty
	 */
	function checkDates($sStartDate, $sEndDate) {
		$sSql = "SELECT '".$sStartDate."' < '".$sEndDate."'";
		return intval(Common::GetOne($sSql)) == 1;
	} // end of checkDates() method
	
	
	/**
	 * Metoda pobiera sprawdza czy istnieje juz aktywny kod przyznawany
	 * nowozarejestrowanym uzytkownikom
	 * 
	 * @param	string	$sStartDate	- data poczatkowa
	 * @param	integer	$sEndDate	- data koncowa
	 * @param	integer	$iId	- Id kodu pomijanego przy sprawdzaniu
	 * @return	string	- kod
	 */
	function grantCodeAlreadyExists($sStartDate, $sEndDate, $iId=0) {
		global $aConfig;
		
		$sSql = "SELECT code
						 FROM ".$aConfig['tabls']['prefix']."promotions_codes
						 WHERE deleted = '0' AND
						 			 grant_to_new_users = '1' AND
						 			 (('".$sStartDate."' <= code_active_from AND '".$sEndDate."' >= code_active_from) || ('".$sStartDate."' <= code_expiry AND '".$sEndDate."' >= code_expiry))".
						 			 ($iId > 0 ? " AND id <> ".$iId : '');
		return Common::GetOne($sSql);
	} // end of grantCodeAlreadyExists() method
	
	
	/**
	 * Metoda generuje kod rabatowy
	 * 
	 * @return	string	- wygenerowany kod
	 */
	function generateCode($iLength=12) {
		$sCode = '';
		$sPool = '';
		
		for($i = 0x41; $i <= 0x5A; $i++) {
			$sPool .= chr($i);
		}
		
		srand((double) microtime() * 1000000);
  	while(strlen($sCode)< $iLength) {
      $sCode .= substr($sPool, (rand() % (strlen($sPool))), 1);
  	}
  	return $sCode;
	} // end of generateCode() method
	
	
	/**
	 * Metoda sprawdza czy kod rabatowy moze zostac usuniety
	 * 
	 * @param	integer	$iId	- Id rekordu
	 * @return	array ref
	 */
	function itemCanBeDeleted($iId) {
		global $aConfig;
		
		// sprawdzenie czy kod jest kodem aktywnym
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."promotions_codes
						 WHERE id = ".$iId." AND
						 			 code_active_from <= NOW() AND
						 			 code_expiry >= NOW()";
		return intval(Common::GetOne($sSql)) == 0;
	} // end of itemCanBeDeleted() method
	
	
	/**
	 * Metoda pobiera dane rekordow ktore maja zostac usuniete
	 * 
	 * @param	array ref	$aIDs	- Id rekordow do usuniecia
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIDs) {
		global $aConfig;
		
		$sSql = "SELECT id, code
						 FROM ".$aConfig['tabls']['prefix']."promotions_codes
						 WHERE id IN (".implode(",", $aIDs).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa kod promocyjny
	 * 
	 * @param	integer	$iId	- Id kodu promocyjnego do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."promotions_codes
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method
	
	
	/**
	 * Metooda pobiera nazwe uzytkownika ktory utworzyl kod promocyjny o podanym Id
	 * 
	 * @param	integer	$iId	 - Id kodu promocyjnego
	 * @return	string
	 */
	function getCreatorName($iId) {
		global $aConfig;
		
		$sSql = "SELECT created_by
						 FROM ".$aConfig['tabls']['prefix']."promotions_codes
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getCreatorName() method
	
	

	/**
	 * Metoda wyswietla liste ksiazek promocji
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function ShowBooks($pSmarty, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');
		include_once('Form/FormTable.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule.'_books');

		$aHeader = array(
			//'header'	=> sprintf($aConfig['lang'][$this->sModule]['book_list'],$sPromoName),
			'action' => phpSelf(array('action' => 'promotions', 'do' => 'books', 'id' => $iId)),
			'refresh_link' => phpSelf(array('action' => 'promotions', 'do' => 'books', 'id' => $iId, 'reset' => '1')),
			'refresh'	=> false,
			'search'	=> false,
			'action' => phpSelf(array('do'=>'remove'), array(), false),
			'second' => true,
			'per_page' => false		);

		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'B.name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);
		
		
		$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'order_by'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('delete','preview'),
					'params' => array (
												'delete'	=> array('do'=>'remove','id'=>'{code_id}', 'bid'=>'{id}')
											),
					'show' => false
				)
			);
			
		$aRecordsFooter = array(
			array('check_all', 'remove_all')
		);
		$aRecordsFooterParams = array();

		$pForm = new FormTable('books_discount_search', '', array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'add_to_promo');
		
		//$pForm->AddText('discount',$aConfig['lang'][$this->sModule]['discount_stake'],$aData['s_product_name'],array('style'=>'width:300px;'),'','text',false);
		
			
		//$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'books', 'pid'=>$iPId)).'\');'), 'button'));
												 
		/*$pView2 = new View('promotions_books', $aHeader2, $aAttribs2);
		$pView2->AddRecordsHeader($aRecordsHeader2);
		
		*/

			//$sShowHideButton=$pForm->GetInputButtonHTML('btnShowHide'.$aItem['id'], $aConfig['lang'][$this->sModule]['show'], array('style'=>'margin-right:100px; width:70px;', 'onclick'=>'showHideRow('.$aItem['id'].');'), 'button').'&nbsp;';
			//$sShowHideButton.$aConfig['lang'][$this->sModule]['discount2'].' '.Common::formatPrice($aItem['value']).$aConfig['lang'][$this->sModule]['discount3']
			$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['list_books'], array('class'=>'merged', 'style'=>'padding-left: 10px; background-color:#736f54; color:#fff; '));


			//dla kazdej stawki pobieramy liste książek
			$pView = new View('promotions_books'.(++$iViewCounter), $aHeader, $aAttribs);
			$pView->AddRecordsHeader($aRecordsHeader);

			$sSql = "SELECT COUNT(DISTINCT A.id)
					FROM ".$aConfig['tabls']['prefix']."products_tarrifs A
					JOIN ".$aConfig['tabls']['prefix']."products B
						ON B.id = A.product_id
					JOIN ".$aConfig['tabls']['prefix']."promotions_codes_to_books C
						ON B.id = C.product_id
					WHERE C.code_id=".$iId.
								(isset($_POST['search']) && !empty($_POST['search']) ? (is_numeric($_POST['search']) ? ' AND B.id = '.(int) $_POST['search'] : ' AND B.name LIKE \'%'.$_POST['search'].'%\'') : '')."
					GROUP BY B.id";
			$iRowCount2 = intval(Common::GetOne($sSql));
			$aBooks=array();
			if ($iRowCount2 > 0) {
			//jeżeli są jakieś ksiazki
			$sSql = "SELECT D.id, B.name, C.name AS publisher, A.product_id, B.publication_year, (SELECT order_by FROM ".$aConfig['tabls']['prefix']."products_shadow S WHERE S.id=B.id) AS order_by 
					FROM ".$aConfig['tabls']['prefix']."products_tarrifs A
					JOIN ".$aConfig['tabls']['prefix']."products B
					ON B.id = A.product_id
					LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers C
						ON B.publisher_id = C.id
					JOIN ".$aConfig['tabls']['prefix']."promotions_codes_to_books D
						ON B.id = D.product_id
					WHERE D.code_id=".$iId.
					" GROUP BY B.id ".
					'ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'order_by').
					(isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '');
			$aBooks =& Common::GetAll($sSql);

			foreach ($aBooks as $iKey => $aBook) {
				$aBooks[$iKey]['name'] = '<b><a href="'.createProductLink($aBook['product_id'], $aBook['name']).'" target="_blank">'.$aBook['name'].'</a></b>'.(!empty($aBook['publisher'])?'<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($aBook['publisher'],0,40,'UTF-8').'</span>':'');
				$aBooks[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aBook['product_id']),0,40,'UTF-8').' '.$aBook['publication_year'].'</span>';
				//$aBooks[$iKey]['placeOnMain']='<span style="color:#'.($aBook['placeOnMain']?'0c0;">TAK':'c00;">NIE').'</span>';
				unset($aBooks[$iKey]['publisher']);
				//$aBooks[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aBook['product_id']),0,40,'UTF-8').' '.$aBook['publication_year'].'</span>';
				unset($aBooks[$iKey]['publication_year']);
				// link podgladu
				$aBooks[$iKey]['action']['links']['preview'] = (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'].'/' : '').(mb_strlen($aBook['name'], 'UTF-8')>210?mb_substr(link_encode($aBook['name']),0,210, 'UTF-8'):link_encode($aBook['name'])).',product'.$aBook['product_id'].'.html?preview='.md5($_SERVER['REMOTE_ADDR'].$aConfig['common']['client_base_url_http']);
				unset($aBooks[$iKey]['product_id']);	
			}		 
		}
		$pView->AddRecords($aBooks, $aColSettings);
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		// dodanie tej formy jest konieczne ze względu na błąd interpretacjis truktury DOM w firefox
		$pForm->AddMergedRow(('<form name="NiewidocznaWDOM"></form>').(!empty($aBooks) ? $pView->Show() : ''), array(), array( 'id'=>'listContainer'.$aItem['id']));
		
		$pForm->AddMergedRow(
						$pForm->GetInputButtonHTML('add_product', 
										$aConfig['lang'][$this->sModule]['add_product'], 
										array('onclick'=>'openSearchWindow('.$aItem['value'].'); return false;', 'style'=>'float:left; margin-left:10px;')
										).
						$pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['go_back'], 
										array('style'=>'float:right;','onclick'=>'window.location.href=\''.phpSelf().'\'; return false;'))
						, 
										array(), 
										array( 'id'=>'listContainer2_'.$aItem['id'])
						);

		/*
		$pForm->AddRow($pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['save'], 
																							array('onclick'=>'window.location.href=\'?frame=main&module_id=26&module=m_zamowienia&action=codes&do=save&pid='.$iId.'\'; return false;')),
									 $pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['go_back'], array('style'=>'float:right;','onclick'=>'window.location.href=\''.phpSelf().'\'; return false;')));
			*/		
		$sJs='<script>function openSearchWindow(disc) {
				window.open("ajax/SearchProduct.php?ppid='.$iPId.'&pid='.$iId.'&site=profit24&mode=5&disc="+disc,"","menubar=0,location=0,scrollbars=1,resizable=1,toolbar=0,width=700,height=500");
				};
				
				function createCookie(name,value, days) {
					if (days) {
						var date = new Date();
						date.setTime(date.getTime()+(days*24*60*60*1000));
						var expires = "; expires="+date.toGMTString();
					}
					else var expires = "";
					document.cookie = name+"="+value+expires+"; path=/";
				}
				
				function eraseCookie(name) {
					createCookie(name,"",-1);
				}
				
				function showHideRow(id) {
					var status = 		document.getElementById("listContainer"+id).style.display;
					var newStatus=	"none";
					var label=			"'.$aConfig['lang'][$this->sModule]['show'].'";
					eraseCookie("displayedPromotionVal"+id);
					if(status=="none") {
						newStatus="";
						label=			"'.$aConfig['lang'][$this->sModule]['hide'].'";
						createCookie("displayedPromotionVal"+id,1);
						}
					
					document.getElementById("listContainer"+id).style.display=newStatus;
					document.getElementById("listContainer2_"+id).style.display=newStatus;
					document.getElementById("btnShowHide"+id).value=label;
					}
				</script>';
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJs.
																				ShowTable($pForm->ShowForm()));
	} // end of Show() function
	
	
	/**
	 * Pobierz nazwę 
	 *
	 * @global type $aConfig
	 * @param type $iId
	 * @return type 
	 */
	function &getPromotion($iId) {
		global $aConfig;
		
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."promotions_codes
						WHERE id = ".$iId;
		return Common::GetRow($sSql);	
	}// end of getPromotion() method
	
	
	/**
	 * Metoda dodaje ksiazki do kodu rabatowego
	 *
	 * @global type $aConfig
	 * @param type $pSmarty
	 * @param type $iPId 
	 */
	function AddItems(&$pSmarty,$iId) {
		global $aConfig;
		$bIsErr = false;
		set_time_limit(3600);
		//dump($_GET['discount']);
		//dump($_GET['aid']);
		
		$aSelItems=explode(',', $_GET['aid']);
		Common::BeginTransaction();
		$aPromo = $this->getPromotion($iId);

		foreach ($aSelItems as $iBId) {
			$sSql = "SELECT name, id FROM ".$aConfig['tabls']['prefix']."products
							 WHERE id=".$iBId;
			$aBook = Common::GetRow($sSql);
			
			if ($aBook['id'] > 0) {
				$aValues = array(
						'code_id' => $iId,
						'product_id' => $aBook['id']
				);
				if (Common::Insert($aConfig['tabls']['prefix']."promotions_codes_to_books",
													$aValues,'',
													false) === false) {
					$bIsErr = true;
				} else { $sAdded.=$aBook['name'].", "; }
			}

		}
			
		$sAdded=substr($sAdded,0,-2);

		if (!$bIsErr) {
			// dodano
			Common::CommitTransaction();
			if(!empty($sAdded))
				$sMsg .= sprintf($aConfig['lang'][$this->sModule]['add_codes_ok'],
											$sAdded,
											$aPromo['code']);
	

			if(!empty($sMsg)) $this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			// blad
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_codes_err'],
											$aPromo['code']);
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			$_GET['do']='books';
			$this->ShowBooks($pSmarty, $iId);
		}
		$_GET['do']='books';
		$this->ShowBooks($pSmarty, $iId);
	} // end of AddItems() funciton
	
	/**
	* Metoda pobiera autorow produktu o podanym id
	* 
	* @param	intger	$iId	- Id produktu
	* @return	array	- autorzy produktu
	*/
	function getAuthors($iId){
		global $aConfig;

		// pobranie autorow ksiazki
		$sSql = "SELECT B.name as role_name, CONCAT(C.surname,' ',C.name) as author
						FROM ".$aConfig['tabls']['prefix']."products_to_authors A
						JOIN ".$aConfig['tabls']['prefix']."products_authors_roles B
						ON B.id = A.role_id
						JOIN ".$aConfig['tabls']['prefix']."products_authors C
						ON C.id = A.author_id
						WHERE A.product_id = ".$iId."
						ORDER BY B.order_by";

		$aResult =& Common::GetAll($sSql);
		$sAuthors='';

		foreach($aResult as $aAuthor){
			$sAuthors .= $aAuthor['author'].', ';
		}
		return substr($sAuthors,0,-2);
	} // end of getAuthors() method
	
	
	
/**
	 * Metoda usuwa wybrane produkty z promocji
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego produktu
	 * @return 	void
	 */
	function RemoveFromPromotion(&$pSmarty, $iId, $iBId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		
		if ($iBId > 0) {
			$_POST['delete'][$iBId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;
		
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			foreach ($_POST['delete'] as $iItem) {
				// usuwanie
				if ($iId <= 0) {
					$sSql = "SELECT code_id FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_books WHERE id=".$iItem;
					$iId = Common::GetOne($sSql);
				}
				if ($iId > 0) {
					$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_books WHERE id=".$iItem;
					if (($iRecords = Common::Query($sSql)) === false) {
						$bIsErr = true;
						break;
					}
				} else {
					$bIsErr = true;
					break;
				}
			}
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				$sMsg =$aConfig['lang'][$this->sModule]['del_code_ok'];
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['del_code_err'];
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$_GET['do']='books';
		$this->ShowBooks($pSmarty, $iId);
	} // end of Delete() funciton
} // end of Module Class
?>