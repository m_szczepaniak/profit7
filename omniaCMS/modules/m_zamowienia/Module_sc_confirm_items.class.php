<?php

use \Exception;
use LIB\communicator\sources\Internal\streamsoft\sendPZ;
use magazine\Containers;
use magazine\FastTrack\containerFastTrack;
use magazine\FastTrack\dataAnalyzer;
use orders\OrderMagazineData;

/**
 * Klasa przyjęcia dostawy
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @copyright (c) 2014, Arkadiusz Golba
 */
include_once('Common_confirm_items.class.php');

class Module__zamowienia__sc_confirm_items extends Common_confirm_items
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    // Id strony modulu
    var $iPageId;

    // katalog plikow log
    var $sLogsDir;

    public $pDbMgr;

    /**
     * Konstruktor klasy
     *
     * @return    void
     */
    function Module__zamowienia__sc_confirm_items(&$pSmarty, $bInit = false)
    {
        global $pDbMgr;
        $this->pDbMgr = $pDbMgr;
        if ($bInit === TRUE) {
            return true;
        }

        $this->sErrMsg = '';
        $this->iPageId = getModulePageId('m_zamowienia');
        $this->sModule = $_GET['module'];
        $this->iLangId = $_SESSION['lang']['id'];

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        if (isset($_GET['id'])) {
            $iId = $_GET['id'];
        }

        $this->aPrivileges =& $_SESSION['user']['privileges'];
        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }

        switch ($sDo) {
            case 'close_fast_track':
                $this->closeFastTrack($pSmarty);
            break;
            case 'confirm_fast_track_quantity':
                $this->closeConfirmFastTrack($pSmarty);
            break;
            case 'save_items':
                $this->SaveItems($pSmarty, $iId);
                break;
            case 'details':
                $this->showDetails($pSmarty, $iId);
                break;
            case 'items_view':
                $this->ShowItems($pSmarty, $iId);
                break;
            case 'package_closed':
                $this->getIdependentSupply($pSmarty);
                break;
            case 'old_view':
                $this->ShowOld($pSmarty);
                break;
            case 'add':
                $this->getPDFRaport($pSmarty);
            break;
            case 'set_fast_track_container':
                $this->setFastTrackContainer($pSmarty);
            break;
            case 'save_independent_stock':
                $this->DoSaveIndependentStock($pSmarty);
            break;
            default:
                $this->Show($pSmarty);
                break;
        }
    } // end Module() function


    /**
     * Metoda zwraca listę wszystkich źródeł
     *
     * @return array
     */
    private function _getSources()
    {

        $sSql = "SELECT name AS label, id AS value
             FROM external_providers ORDER BY label ASC";
        $aExtProv = Common::GetAll($sSql);
        return $aExtProv;
    }// end of _getSources() method


    public function getIdependentSupply(&$pSmarty)
    {
        global $aConfig, $pDbMgr;

        $_SESSION['sc_confirm_lists_independent'] = time();

        $aData = array();
        $aLang =& $aConfig['lang'][$this->sModule];
        $sHtml = '';

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        // pobranie z bazy danych szczegolow konta uzytkownika
        $sHeader = sprintf($aConfig['lang'][$this->sModule]['details_header']);

        $pForm = new \FormTable('ordered_itm', $sHeader, [], array('col_width' => 155,'class'=>'formTable sc_confirm_items'), true);
        $pForm->AddHidden('do', 'save_independent_stock');
        $pForm->AddHidden('sc_date', date('YmdHis'));
        $pForm->AddSelect('source', _('Źródło'), array('style' => 'font-size: 20'), addDefaultValue($this->_getSources()), $aData['source'], '', true);
        $pForm->AddText("fv_nr", _("Numer FV"), $aData['fv_nr'], array('class' => 'editable_input', 'style' => 'font-size: 45px;'), '', 'text', true);
        $pForm->AddCheckBox('omit_create_doc_streamsoft', _('<b>POMIŃ</b> tworzenie dokumentu PZ/MM'), array(), '', false, false);
        $pForm->AddCheckBox('in_document_type', _('Czy przejęcie dokumentem MM'), array(), '', false, false);
        if ($_SESSION['user']['priv_order_status'] == '1' || $_SESSION['user']['type'] == '1' ||
            $_SESSION['user']['name'] == 'mluczak' || $_SESSION['user']['name'] == 'plemieszko' || $_SESSION['user']['name'] == 'mklawinski' ||
            $_SESSION['user']['name'] == 'ppiskorek' || $_SESSION['user']['name'] == 'nzienkiewicz' || $_SESSION['user']['name'] == 'mnajduk' ||
            $_SESSION['user']['name'] == 'mbebenkowski' || $_SESSION['user']['name'] == 'jkornatowski' ||
            $_SESSION['user']['name'] == 'apiwonski' || $_SESSION['user']['name'] == 'kchmielewski'
        ) {
            $pForm->AddCheckBox('independent_supply_get_fv', _('Pobierz składowe z FV'), array(), '', false, false);
            $pForm->AddText('date_pz', _('Data dokumentu'), $aData['date_pz'], array(), '', 'date', false);
        }
        $pForm->AddMergedRow($aLang['details_content_section'], array('class' => 'merged', 'style' => 'padding-left: 160px;'));
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz dostawę'), array('style' => 'font-size: 40px; font-weight: bold; color: red;')));

        $pForm->AddMergedRow('', array('id' => 'sc_products', 'readonly' => 'readonly', 'style' => 'font-size:15px; width: 90%; height: 300px', 'class' => 'editable_input'));


        $pForm->AddMergedRow(
            $pForm->GetLabelHTML('additional_ean', _('Dodatkowe EANy')) .
            $pForm->GetTextAreaHTML('additional_ean', _('Dodatkowe EANy'), '', array('class' => 'editable_input', 'style' => 'width: 180px; height: 400px;'), 0, '', '', false) .
            ' &nbsp; &nbsp; ' .
            $pForm->GetLabelHTML('defect_ean', _('Defekty:')) .
            $pForm->GetTextAreaHTML('defect_ean', _('Dodatkowe EANy'), '', array('class' => 'editable_input', 'style' => 'width: 180px; height: 400px;'), 0, '', '', false) .
            ' &nbsp; &nbsp; ' .
            $pForm->GetLabelHTML('defect_ean',  _('Wprowadź EANy masowo na dokument format: EAN;Ilość')) .
            $pForm->GetTextAreaHTML('insert_csv_eans', _('Wprowadź EANy masowo na dokument format: EAN;Ilość'), '', array('class' => 'editable_input', 'style' => 'width: 300px; height: 400px;'), 0, '', '', false) .
            $pForm->GetInputButtonHTML('insert_csv_eans_button', _('Wprowadź EANY'), ['onclick' => 'insertCSVEans()'], 'button')
            , ['style' => 'text-align: left; ']);

        $sCheckbox = $pForm->GetCheckBoxHTML('ask_quantity', _('Zapytaj o ilość'), [], '', true, false);
        $sButton = $pForm->GetInputButtonHTML("submit", _('Zapisz dostawę'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
        $pForm->AddMergedRow(_('Zapytaj o ilość:') . $sCheckbox . ' &nbsp; &nbsp; &nbsp;' . $sButton);


        $input = '
    <script type="text/javascript">
        function insertCSVEans() {
            var csvEAN = $("#insert_csv_eans").val();
            var linesEAN = csvEAN.split(\'\n\');
            for(var i = 0;i < linesEAN.length;i++){
                var eanQuantity = linesEAN[i]
                var arrayEanQuantity = eanQuantity.split(\';\');
                var ean = arrayEanQuantity[0];
                var quantity = arrayEanQuantity[1];
                ean = ean.trim();
                if (ean != "" && quantity > 0) {
                    quantity = quantity.trim();
                    var scDate = $("#sc_date").val();
                    addProductQuantity(ean, quantity, scDate, false);
                }
                
            }
        }
      $(function() {
        $("#fv_nr").autocomplete({
          source: function( request, response ) {
            $.getJSON( "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetSourceFile.php?source=" + $("#source").val(), {
              term: request.term
            }, response );
          }
        });
        
        setInterval(function() {
            $.ajax({
              type: "GET",
              cache: false,
              async: false,
              url: "ajax/continueSession.php"
            });
         }, 60000);
      });
    </script>

    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" /></div>
    <script type="text/javascript" src="js/independent_supply.js?date=201809313"></script>';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $input . ShowTable($pForm->ShowForm()));
    }

    /**
     *
     * @param array $aItems
     * @return array
     */
    private function getProductsFromXMLToSupply($aItems)
    {
        /*
  Array
  (
      [0] => Array
          (
              [ean_13] => 9788377760185
              [price_netto] => 11.5
              [discount] => 0
              [vat] => 5
              [quantity] => 10.0
          )
  }
         */
        $newArr = [];

        foreach ($aItems as $iKey => $aValue) {
            $iProductId = $this->searchProductByIndexGetId($aValue['ean_13']);
            if ($iProductId > 0) {
                if (isset($newArr[$iProductId])) {
                    $newArr[$iProductId]['quantity'] += intval($aValue['quantity']);
                } else {
                    $newArr[$iProductId] =
                        [
                            'quantity' => intval($aValue['quantity']),
                            'price_netto' => $aValue['price_netto'],
                            'vat' => $aValue['vat'],
                            'ean_13' => $aValue['ean_13'],
                            'id' => $iProductId
                        ];
                }
            } else {
                return $aValue['ean_13'];
            }
        }
        return $newArr;
    }


    /**
     *
     * @param \Smarty $pSmarty
     * @return string
     */
    public function DoSaveIndependentStock($pSmarty)
    {

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty);
            return;
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->getIdependentSupply($pSmarty);
            return;
        }

        $bIsErr = false;
        $orderMagazineData = new OrderMagazineData($this->pDbMgr);
        if (isset($_POST['independent_supply_get_fv']) && $_POST['independent_supply_get_fv'] == '1') {
            try {
                $oXMLParse = $this->getFVObj($_POST['fv_nr'], $_POST['source']);
                $aItems = $oXMLParse->getProductsToStreamsoft();
                $aProductsScanned = $this->getProductsFromXMLToSupply($aItems);
            } catch (Exception $ex) {
                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas importowania danych ze źródła - ' . $ex->getMessage()), true);
                return $this->Show($pSmarty);
            }
        } else {
            $aProductsScanned = $_SESSION['inventory'][$_POST['sc_date']];
        }
        if (empty($aProductsScanned) || !is_array($aProductsScanned)) {
            $this->sMsg .= GetMessage(_('Nie rozpoznano produktu o EAN: ' . $aProductsScanned), true);
            return $this->Show($pSmarty);
        }

        $this->pDbMgr->BeginTransaction('profit24');

        $sListNumber = $orderMagazineData->getNewListNumber(1);

        $sContent = '';
        foreach ($aProductsScanned as $iProductId => $aProduct) {
            $sContent .= $iProductId . ' | ' . implode(' | ', $aProduct) . "<br />";
        }
        $aValues = [
            'source' => $_POST['source'],
            'status' => 1,
            'date_send' => 'NOW()',
            'send_by' => $_SESSION['user']['name'],
            'number' => $sListNumber,
            'content' => $sContent,
            'post_array' => print_r($_POST, true) . print_r($aProductsScanned, true),
            'independent_stock' => '1',
            'post_array' => serialize($_POST),
            'in_document_type' => isset($_POST['in_document_type']) ? '2' : '1',
            'omit_create_doc_streamsoft' => isset($_POST['omit_create_doc_streamsoft']) ? '1' : '0',
        ];
        if (isset($_POST['date_pz']) && $_POST['date_pz'] != '') {
            $aValues['date_pz'] = FormatDate($_POST['date_pz']); // $_POST['date_pz'];
        }

        if (($iOSHId = $this->pDbMgr->Insert('profit24', "orders_send_history", $aValues, '', true)) === false) {
            $bIsErr = true;
        } else {
            foreach ($aProductsScanned as $iProductId => $aProduct) {
                $aValues = array(
                    'send_history_id' => $iOSHId,
                    'product_id' => $iProductId,
                    'ordered_quantity' => $aProduct['quantity'],
                    'in_confirmed_quantity' => $aProduct['quantity'],
                );
                if (isset($aProduct['price_netto']) && isset($aProduct['vat'])) {
                    $aValues['price_netto'] = $aProduct['price_netto'];
                    $aValues['vat'] = $aProduct['vat'];
                } else {
                    $aValues = $this->fillPricesWholesalePrices($aValues, $iProductId, $_POST['source']);
                }

                if ($this->pDbMgr->Insert('profit24', "orders_send_history_items", $aValues, '', false) === false) {
                    // błąd
                    $bIsErr = true;
                }
            }
        }

        try {
            if ($this->proceedAdditionalProducts($_POST['additional_ean'], $iOSHId, false) === false) {
                $bIsErr = true;
            }

            if ($this->proceedAdditionalProducts($_POST['defect_ean'], $iOSHId, true) === false) {
                $bIsErr = true;
            }
        } catch (Exception $ex) {
            $bIsErr = true;
            $sAddMsg .= $ex->getMessage();
        }

        if (stristr($_POST['fv_nr'], '.XML') !== false && file_exists($_POST['fv_nr'])) {
            $oXMLParse = $this->getFVObj($_POST['fv_nr'], $_POST['source']);
            $aItems = $oXMLParse->getProductsToStreamsoft();
            $this->validateUpdateItems($iOSHId, $aItems);
            $_POST['fv_nr'] = $oXMLParse->getFVNumber();
        }

        if (isset($_POST['fv_nr']) && $_POST['fv_nr'] != '') {
            $oOrderMagazineData = new OrderMagazineData($this->pDbMgr);
            $oOrderMagazineData->addSetHistorySendAttr($iOSHId, 'fv_nr', $_POST['fv_nr']);
        }


        if ($_POST['omit_create_doc_streamsoft'] != '1') {
            $oSendPZ = new sendPZ($this->pDbMgr);
            try {
                $oSendPZ->doSendPZ($iOSHId);
                $aUpdateValues = [
                    'magazine_status' => '5'
                ];
                $this->pDbMgr->Update('profit24', 'orders_send_history', $aUpdateValues, ' id = ' . $iOSHId);

            } catch (Exception $ex) {
                echo $ex->getMessage();
                $this->sMsg .= GetMessage($ex->getMessage());
                $bIsErr = true;
            }
        } else {
            $this->sMsg .= GetMessage(_('Przyjęto poprawnie, ale dokument PZ nie został utworzony w streamsoft'), false);
        }

        $independentSupply = new \Supply\IndependentSupply();
        if (false == $independentSupply->createIndependentSupplyList($aProductsScanned, $iOSHId)) {
            $bIsErr = true;
        }

        if ($bIsErr === true) {
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas przyjmowania dostawy niezależnej ') . $sAddMsg);
            $this->pDbMgr->RollbackTransaction('profit24');
            return $this->getIdependentSupply($pSmarty);
        } else {
            $this->sMsg .= GetMessage(_('Wprowadzono dostawę niezależną o nr "' . $sListNumber . '"'), false);
            $this->pDbMgr->CommitTransaction('profit24');

            if (!isset($_POST['independent_supply_get_fv']) || $_POST['independent_supply_get_fv'] != '1') {
                $scTime = 0;
                if (isset($_SESSION['sc_confirm_lists_independent'])) {
                    $scTime = time() - $_SESSION['sc_confirm_lists_independent'];
                }
                $this->AddOIILITimeItem($iOSHId, $scTime);

                unset($_SESSION['sc_confirm_lists_independent']);
            }

            return $this->Show($pSmarty);
        }
    }

    /**
     *
     * @param array $aValues
     * @param int $iOSHId
     * @param int $iProductId
     * @param int $iSourceProviderId
     * @return array
     */
    private function fillPricesWholesalePrices($aValues, $iProductId, $iSourceProviderId)
    {
        $aStock = $this->getSourceStockPrices($iSourceProviderId, $iProductId);
        if (!empty($aStock)) {
            $aValues['price_netto'] = Common::formatPrice2($aStock['price_brutto'] / (($aStock['vat'] / 100) + 1));
            $aValues['vat'] = $aStock['vat'];
        }
        return $aValues;
    }

    private function getSourceStockPrices($iSourceProviderId, $iProductId)
    {

        $sSql = 'SELECT *
             FROM sources 
             WHERE provider_id = ' . $iSourceProviderId;
        $aSourceData = $this->pDbMgr->GetRow('profit24', $sSql);
        if (!empty($aSourceData)) {
            $sColPrefix = $aSourceData['column_prefix_products_stock'];
            $sSql = 'SELECT ' . $sColPrefix . '_wholesale_price AS price_brutto, ' . $sColPrefix . '_vat AS vat FROM products_stock WHERE id = ' . $iProductId;
            return $this->pDbMgr->GetRow('profit24', $sSql);
        } else {
            return false;
        }
    }


    /**
     * @param $pSmarty
     */
    private function setFastTrackContainer($pSmarty)
    {

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->Show($pSmarty);
            return;
        }

        $containerId = $_POST['container_id'];

        $this->pDbMgr->BeginTransaction('profit24');

        $oContainers = new Containers($this->pDbMgr);
        $resultContainer = $oContainers->checkContainer($_POST['container_id']);
        if (intval($resultContainer) > 0) {
            $oContainers->lockContainer($containerId);
            $fastTrackContainer = new containerFastTrack($this->pDbMgr, $_SESSION['user']['id']);
            if (false === $fastTrackContainer->openUserFastTrack($containerId)) {
                $this->pDbMgr->RollbackTransaction('profit24');
                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas ustawiania fast tracka '.$containerId.' użytkownikowi '.$_SESSION['user']['id']));
                $sFormHTML = $this->getFastTrackForm($_GET['id']);
                $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($sFormHTML));
                return ;
            } else {
                $this->pDbMgr->CommitTransaction('profit24');
                $this->sMsg .= GetMessage(_('Ustawiono kuwetę dla FastTracka '.$containerId.' użytkownikowi '.$_SESSION['user']['name']), false);
                $this->showDetails($pSmarty, $_GET['id']);
                return ;
            }
        } elseif ($resultContainer === -1) {
            $this->pDbMgr->RollbackTransaction('profit24');
            $this->sMsg .= GetMessage(_('Podany kontener '.$containerId.' jest zablokowany'));
            $sFormHTML = $this->getFastTrackForm($_GET['id']);
            $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($sFormHTML));
            return ;
        } else {
            $this->pDbMgr->RollbackTransaction('profit24');
            $this->sMsg .= GetMessage(_('Podany kontener '.$containerId.' nie istnieje'));
            $sFormHTML = $this->getFastTrackForm($_GET['id']);
            $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($sFormHTML));
            return ;
        }
    } // end of getProductWeight() method


    /**
     * @param $iId
     * @return string
     */
    private function getFastTrackForm($iId) {
        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('get_fast_track', _('Kuweta na FastTracki'), array('action' => phpSelf(array('id' => $iId))), array('col_width' => 155), true);
        $pForm->AddHidden('do', 'set_fast_track_container');

        $pForm->AddText('container_id', _('Kuweta na FastTracki'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
        $pForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
        return $pForm->ShowForm();
    }


    /**
     * @param $pSmarty
     * @return mixed
     */
    private function closeConfirmFastTrack($pSmarty) {
        $containerFastTrack = new containerFastTrack($this->pDbMgr, $_SESSION['user']['id']);
        $fastTrackData = $containerFastTrack->useFastTrack();
        $quantityFastTrack = $containerFastTrack->getProductQuantityFastTrack($fastTrackData['id']);

        if ($quantityFastTrack == intval($_POST['fast_track_quantity'])) {
            try {
                $containerFastTrack->closeConfirmFastTruck($fastTrackData);
                $msg = _('Zamknięto pomyślnie fast tracka '.$fastTrackData['container_id'].' użytkownika '.$_SESSION['user']['name']);
                $this->sMsg .= GetMessage($msg, false);
                AddLog($msg, false);
                return $this->Show($pSmarty);
            } catch (Exception $ex) {
                $msg = $ex->getMessage();
                $this->sMsg .= GetMessage($msg);
                AddLog($msg);
                return $this->closeFastTrack($pSmarty);
            }
        } else {
            $msg = _('Podana ilość produktów "'.intval($_POST['fast_track_quantity']).'" nie zgadza się z ilością produktów "'.$quantityFastTrack.'" na fast tracku  w kuwecie '.$fastTrackData['container_id'].' użytkownika '.$_SESSION['user']['name']);
            $this->sMsg .= GetMessage($msg);
            AddLog($msg);
            return $this->closeFastTrack($pSmarty);
        }
    }

    /**
     * @param $pSmarty
     * @return mixed
     */
    private function closeFastTrack($pSmarty) {

        $containerFastTrack = new containerFastTrack($this->pDbMgr, $_SESSION['user']['id']);
        $fastTrackData = $containerFastTrack->useFastTrack();
        if (!empty($fastTrackData)) {
            include_once('Form/FormTable.class.php');
            $pForm = new FormTable('confirm_fast_track_quantity', _('Ile produktów w kuwecie fast track '.$fastTrackData['container_id']), array('action' => phpSelf()), array('col_width' => 155), true);
            $pForm->AddHidden('do', 'confirm_fast_track_quantity');

            $pForm->AddText('fast_track_quantity', _('Ilość produktów w kuwecie fast tracka<br />'.$fastTrackData['container_id']), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
            $pForm->AddInputButton('save', _('Dalej'), array('style' => 'font-size: 25'));
            $sFormHTML = $pForm->ShowForm();
            $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($sFormHTML));
        } else {
            $this->sMsg .= GetMessage(_('Brak otwartych fast tracków użytkownika '.$_SESSION['user']['name']));
            return $this->Show($pSmarty);
        }
    }


    /**
     * Metoda tworzy formularz z informacjami na temat uzytkownika
     *
     * @global DatabaseManager $pDbMgr
     * @param        object $pSmarty
     * @param        integer $iId - ID konta uzytkownika
     * @return    void
     */
    function showDetails(&$pSmarty, $iId)
    {
        global $aConfig, $pDbMgr;
        $containerFastTrack = new containerFastTrack($this->pDbMgr, $_SESSION['user']['id']);
        $fastTrackData = $containerFastTrack->useFastTrack();
        if (false === $fastTrackData) {
            // ustaw id fastTracka'
            $sFormHTML = $this->getFastTrackForm($iId);
            $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($sFormHTML));
            return;
        }



        enableScale($pSmarty);
        $aData = array();
        $aLang =& $aConfig['lang'][$this->sModule];
        $sHtml = '';

        $_SESSION['sc_confirm_lists_' . $iId] = time();

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        $fastTrackDataAnalyzer = new dataAnalyzer($this->pDbMgr);
        $fastTrackItems = $fastTrackDataAnalyzer->analyzeOrders($iId);
        // lockowanie rekordu następuje po zeskanowaniu eanu - order-item, który jest przeznaczony na fustTrucka

        // pobranie z bazy danych szczegolow konta uzytkownika
        $sSql = "SELECT A.id, A.number, A.pack_number, A.saved_type, A.email, A.date_send, A.content, A.send_by, OSHA.fv_nr, OSHA.ident_nr, A.source
							 FROM " . $aConfig['tabls']['prefix'] . "orders_send_history AS A
               LEFT JOIN " . $aConfig['tabls']['prefix'] . "orders_send_history_attributes AS OSHA
                 ON A.id = OSHA.orders_send_history_id
							 WHERE A.id=" . $iId;
        $aData =& Common::GetRow($sSql);

//		$aData['source']=in_array($aData['source'], array(0,1,5))?($aData['source']==0?'Profit J':($aData['source']=='1'?'Profit E':'Profit M')):$aData['source_name'];
        $sHeader = sprintf($aConfig['lang'][$this->sModule]['details_header']);
        $pForm = new FormTable('ordered_itm', $sHeader, array('action' => phpSelf(array('id' => $iId))), array('col_width' => 155), true);
        $pForm->AddHidden('do', 'save_items');

        $pForm->AddHidden('fast_track_id', $fastTrackData['id'], ['id' => 'fast_track_id']);
        $pForm->AddHidden('fast_track_scanned_order_items', '', ['id' => 'fast_track_scanned_order_items']);

        if ($aData['pack_number'] == '') {
            $pForm->AddText("pack_number", _("Numer kuwety"), $aData['pack_number'], array('class' => 'important', 'style' => 'font-size: 45px;'), '', 'int', true);
        } else {
            $pForm->AddRow(_('Półka/kuweta'), $aData['pack_number']);
        }

        // dane wiadomości
        if (!empty($aData['email'])) {
            $pForm->AddRow($aLang['details_email'], Common::convert($aData['email']));
        }

        $pForm->AddSelect('source', _('Źródło'), array('style' => 'font-size: 20'), $this->_getSources(), $aData['source'], '', false);
        // zmieniamy kontrahenta
        if ($aData['fv_nr'] != '') {
            $pForm->AddRow($aLang['fv_nr'], $aData['fv_nr'], '', array(), array('style' => 'color: red; font-size: 14px; font-weight: bold;'));
        } else {
            $pForm->AddText("fv_nr", _("Numer FV"), $aData['fv_nr'], array('class' => 'important', 'style' => 'font-size: 45px;'), '', 'text', true);
        }

        $pForm->AddCheckBox('omit_create_doc_streamsoft', _('<b>POMIŃ</b> tworzenie dokumentu PZ/MM'), array(), '', false, false);
        $pForm->AddCheckBox('in_document_type', _('Czy przejęcie dokumentem MM'), array(), '', false, false);

        $pForm->AddRow($aLang['number'], $aData['number'], '', array(), array('style' => 'font-weight:bold; font-size:14px;'));
        if ($aData['ident_nr'] != '') {
            $pForm->AddRow($aLang['ident_nr'], $aData['ident_nr']);
        }
        $pForm->AddRow($aLang['details_date_send'], $aData['date_send']);
        $pForm->AddRow($aLang['details_send_by'], Common::convert($aData['send_by']));
        // dane do faktury
        $pForm->AddMergedRow($aLang['details_content_section'], array('class' => 'merged', 'style' => 'padding-left: 160px;'));

        // wyszukiwanie ksiazek
        $aBooks =& array_merge($this->getBooksNew($iId), $this->getBooksDeleted($iId));
        if (!empty($aBooks)) {
            $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_change_status']));
            if ($this->checkSource($aData['source'], ['azymut', 'ateneum'])) {
                $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('check_all', _('Zaznacz wszystko'), ['style' => 'font-weight:bold; font-size:24px;', 'onclick' => 'selectAllRows()'], 'button'));
            }
        }

        // wyswietlenie listy pasujacych uzytkownikow
        $pForm->AddMergedRow($this->GetBooksList($pSmarty, $aBooks, $fastTrackItems));
        if (!empty($aBooks)) {
            $pForm->AddMergedRow(
                $pForm->GetLabelHTML('additional_ean', _('Dodatkowe EANy')) .
                $pForm->GetTextAreaHTML('additional_ean', _('Dodatkowe EANy'), '', array('class' => 'important', 'style' => 'width: 180px; height: 400px;'), 0, '', '', false) .
                ' &nbsp; &nbsp; ' .
                $pForm->GetLabelHTML('defect_ean', _('Defekty:')) .
                $pForm->GetTextAreaHTML('defect_ean', _('Dodatkowe EANy'), '', array('class' => 'important', 'style' => 'width: 180px; height: 400px;'), 0, '', '', false)
                , ['style' => 'text-align: left; ']);
            $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send2', $aConfig['lang'][$this->sModule]['button_change_status']));
        }


        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('style' => 'float:right;', 'onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));

        $currentProductQuantityFastTrack = intval($containerFastTrack->getProductQuantityFastTrack($fastTrackData['id']));
        $sInput = '
    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" /></div>

    <script type="text/javascript" src="js/selectOrderItemQuantity.js?date=20181017"></script>
    <script type="text/javascript">
      $(function() {
        $("#pack_number").focus();
        $("#fv_nr").autocomplete({
          source: function( request, response ) {
            $.getJSON( "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetSourceFile.php?source=" + $("#source").val(), {
              term: request.term
            }, response );
          }
        });
        
        $("#ordered_itm").on( \'submit\', function (e) {
            var ordersFastTrack = $("#fast_track_scanned_order_items").val();
            if (ordersFastTrack != "") {
                var currentFastTrack = JSON.parse(ordersFastTrack);
                var correctQuantity = '.$currentProductQuantityFastTrack.' + currentFastTrack.length;
                while (correctQuantity != prompt( "Ile jest teraz produktów w kuwecie fast track "+'.$fastTrackData['container_id'].' )) {
                
                }
            }
        });
        
      });
      function selectAllRows() {
        var showMessage = false;
        $(".list_quantity").each( function (key, obj) {
            
        
            var checkbox = $(this).parent().find("input[type=\'checkbox\']");
            if (checkbox.attr("disabled") != "disabled") {
                checkbox.click();
                $(this).parent().attr("style", "background-color: #d7d3bc");
                
                //ilosci
                var quantity = $(this).html();
                var currQuantity = $(this).next();
                var hhh = $(currQuantity).children("input");
                hhh.val(quantity);
                
                var ddd = $(currQuantity).find("input");
                currQuantity.html(quantity);
                $(currQuantity).append(hhh);
            
            } else {
                if (showMessage == false) {
                    alert("UWAGA produkty bez wagi nie zostały odznaczone");
                    showMessage = true;
                }
            }

        })
      }
    </script>
';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sInput . ShowTable($pForm->ShowForm()));
    } // end of Details() function


    private function checkSource($iSourceId, $aAcceptSource)
    {
        $sSql = 'SELECT id FROM external_providers WHERE symbol IN ("' . implode('","', $aAcceptSource) . '")';
        $aSourcesAccept = $this->pDbMgr->GetCol('profit24', $sSql);
        if (in_array($iSourceId, $aSourcesAccept)) {
            return true;
        }
        return false;
    }


    /**
     * @param        object $pSmarty
     * @return    void
     */
    function Show(&$pSmarty)
    {
        global $aConfig;
        $sFilterSql = '';

        // dolaczenie klasy View
        include_once('View/View.class.php');
        include_once('Form/FormTable.class.php');

        // zapamietanie opcji
        rememberViewState($this->sModule);
        $aHeader = array(
            'header' => $aConfig['lang'][$this->sModule]['history'],
            'refresh' => true,
            'search' => true,
            'checkboxes' => false,
//            'per_page' => false
//            'hide_per_page' => true
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'number',
                'content' => $aConfig['lang'][$this->sModule]['number'],
                'sortable' => true,
                'width' => '100'
            ),
            array(
                'db_field' => 'pack_number',
                'content' => _('Półka/kuweta'),
                'sortable' => true,
                'width' => '100'
            ),
            array(
                'db_field' => 'date_send',
                'content' => $aConfig['lang'][$this->sModule]['date_send'],
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'source',
                'content' => $aConfig['lang'][$this->sModule]['source'],
                'sortable' => true,
            ),
            array(
                'db_field' => 'saved_type',
                'content' => $aConfig['lang'][$this->sModule]['saved_type'],
                'sortable' => true,
            ),
            array(
                'db_field' => 'fv_nr',
                'content' => $aConfig['lang'][$this->sModule]['fv_nr'],
                'sortable' => true,
            ),
            array(
                'db_field' => 'in_document_type',
                'content' => _('Typ dokumentu przyjęcia'),
                'sortable' => true,
            ),
            array(
                'db_field' => 'omit_create_doc_streamsoft',
                'content' => _('Pomiń tworzenie dokumentu PZ/MM'),
                'sortable' => true,
            ),
            array(
                'db_field' => 'send_by',
                'content' => $aConfig['lang'][$this->sModule]['send_by'],
                'sortable' => true,
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '40'
            )
        );


        // pobranie liczby wszystkich list
        $sSql = "SELECT COUNT(DISTINCT OSH.id) 
						 FROM " . $aConfig['tabls']['prefix'] . "orders_send_history AS OSH
             JOIN orders_send_history_items AS OSHI
              ON OSH.id = OSHI.send_history_id
             JOIN orders_items AS OI
              ON OI.id = OSHI.item_id
              AND OI.status = '2'
              AND OI.deleted = '0'
              AND OSH.source = OI.source
             JOIN orders AS O
              ON O.id = OI.order_id
              AND O.order_status <> '5'
              AND O.order_status <> '4'
              AND O.order_status <> '3'
						 WHERE OSH.status = '1'
             AND OSH.magazine_status <> '5'
            " .
            (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND OSH.source = ' . $_POST['f_source'] : '') .
            (isset($_POST['search']) && !empty($_POST['search']) ? ' AND OSH.number LIKE \'%' . $_POST['search'] . '%\'' : '');
        $iRowCount = intval(Common::GetOne($sSql));
        if ($iRowCount == 0 && !isset($_GET['reset'])) {
            // resetowanie widoku
            resetViewState($this->sModule);
            // ponowne okreslenie liczby rekordow
            $sSql = "SELECT COUNT(DISTINCT OSH.id) 
						 FROM " . $aConfig['tabls']['prefix'] . "orders_send_history AS OSH
             JOIN orders_send_history_items AS OSHI
              ON OSH.id = OSHI.send_history_id
             JOIN orders_items AS OI
              ON OI.id = OSHI.item_id
              AND OI.status = '2'
              AND OI.deleted = '0'
              AND OSH.source = OI.source
             JOIN orders AS O
              ON O.id = OI.order_id
              AND O.order_status <> '5'
              AND O.order_status <> '4'
              AND O.order_status <> '3'
						 WHERE OSH.status = '1'
             AND OSH.magazine_status <> '5'
            " .
                (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND OSH.source = ' . $_POST['f_source'] : '') .
                (isset($_POST['search']) && !empty($_POST['search']) ? ' AND OSH.number LIKE \'%' . $_POST['search'] . '%\'' : '');
            $iRowCount = intval(Common::GetOne($sSql));
        }

        $pView = new View('send_history', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);

        // dodanie filtru grupy
        $pView->AddFilter('f_source', $aConfig['lang'][$this->sModule]['f_source'], array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])), $this->_getSources()), $_POST['f_source']);

        if ($iRowCount > 0) {
            // dodanie Pagera do widoku
            $iCurrentPage = $pView->AddPager($iRowCount);
            $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
            $iStartFrom = ($iCurrentPage - 1) * $iPerPage;
            // pobranie zamówień bankowych
            $sSql = "SELECT DISTINCT OSH.id, OSH.number, OSH.pack_number,  OSH.date_send, OSH.source, OSH.saved_type, OSH.status, OSHA.fv_nr, OSH.in_document_type, OSH.omit_create_doc_streamsoft, OSH.send_by, (SELECT name FROM " . $aConfig['tabls']['prefix'] . "external_providers AS EP WHERE EP.id=OSH.source) AS source_name,
                OSH.independent_stock
                FROM " . $aConfig['tabls']['prefix'] . "orders_send_history AS OSH
                JOIN orders_send_history_items AS OSHI
                 ON OSH.id = OSHI.send_history_id
                JOIN orders_items AS OI
                 ON OI.id = OSHI.item_id
                 AND OI.status = '2'
                 AND OI.deleted = '0'
                 AND OSH.source = OI.source
                JOIN orders AS O
                 ON O.id = OI.order_id
                 AND O.order_status <> '5'
                 AND O.order_status <> '4'
                 AND O.order_status <> '3'
                LEFT JOIN orders_send_history_attributes AS OSHA
                 ON OSH.id = OSHA.orders_send_history_id
                WHERE OSH.status = '1' 
                AND OSH.magazine_status <> '5'
                " . $sFilterSql .
                (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND OSH.source = ' . $_POST['f_source'] : '') .
                (isset($_POST['search']) && !empty($_POST['search']) ? ' AND OSH.number LIKE \'%' . $_POST['search'] . '%\'' : '') .
                ' ORDER BY ' . (isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' OSH.date_send DESC') .
                (isset($_GET['order']) && !empty($_GET['order']) ? ' ' . $_GET['order'] : '') .
                " LIMIT " . $iStartFrom . ", " . $iPerPage;
            $aRecords =& Common::GetAll($sSql);
            foreach ($aRecords as $iKey => $aItem) {
                $aRecords[$iKey]['source'] = in_array($aRecords[$iKey]['source'], array(0, 1, 5)) ? ($aRecords[$iKey]['source'] == 0 ? 'Profit J' : ($aRecords[$iKey]['source'] == '1' ? 'Profit E' : 'Profit M')) : $aRecords[$iKey]['source_name'];
                $aRecords[$iKey]['saved_type'] = $aConfig['lang'][$this->sModule]['saved_' . $aItem['saved_type']];
                $aRecords[$iKey]['status'] = $aConfig['lang'][$this->sModule]['status_' . $aItem['status']];
                $aRecords[$iKey]['date_send'] = str_replace(' ', '<br />', $aRecords[$iKey]['date_send']);
                $aRecords[$iKey]['fv_nr'] = '<span style="color: red; font-size: 12px; font-weight: bold;">' . $aItem['fv_nr'] . '</span>';
                $aRecords[$iKey]['fv_nr_escaped'] = htmlspecialchars('<span style="color: red; font-size: 12px; font-weight: bold;">' . $aItem['fv_nr'] . '</span>');
                if ($aItem['in_document_type'] == '2') {
                    $aRecords[$iKey]['in_document_type'] = 'MM';
                } else {
                    $aRecords[$iKey]['in_document_type'] = 'FV';
                }
                if ($aItem['independent_stock'] == '1') {
                    $aRecords[$iKey]['in_document_type'] .= ' - DOSTAWA NIEZALEŻNA';
                }
                unset($aRecords[$iKey]['independent_stock']);
            }

            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'status' => array(
                    'show' => false
                ),
                'source_name' => array(
                    'show' => false
                ),
                'fv_nr_escaped' => array(
                    'show' => false
                ),

                'date_send' => array('link' => phpSelf(array('do' => 'details', 'id' => '{id}'))),

                'number' => array('link' => phpSelf(array('do' => 'details', 'id' => '{id}'))),

                'action' => array(
                    'actions' => array('details', 'items_view'),
                    'params' => array(
                        'details' => array('id' => '{id}'),
                        'items_view' => array('id' => '{id}', 'fv_nr' => '{fv_nr_escaped}')
                    ),
                    'show' => false,
                    'icon' => array('items_view' => 'preview'
                    ),
                )
            );
            // dodanie rekordow do widoku
            $pView->AddRecords($aRecords, $aColSettings);
        }

        // dodanie stopki do widoku
        $aRecordsFooter = array(
            [],
            array('add', 'package_closed'),
        );

        $pView->AddRecordsFooter($aRecordsFooter);
        //przycisk przejdz do starego widoku

        $sHeader = $aConfig['lang'][$this->sModule]['ordered_items_header'] . ($_POST['source'] != '' ? ' - ' . $aConfig['lang'][$this->sModule]['source_' . $_POST['source']] : '');

        $pForm = new FormTable('old_view_button', '', array('action' => phpSelf()), array('col_width' => 150), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', 'old_view');
        $pForm->AddRow('&nbsp;', '<div style="float:right;">' .
            $pForm->GetInputButtonHTML('get_csv', $aConfig['lang'][$this->sModule]['old_view'], array('onclick' => 'this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button'));

        $fastTrackHTML = '';
        $containerFastTrack = new containerFastTrack($this->pDbMgr, $_SESSION['user']['id']);
        $fastTrackData = $containerFastTrack->useFastTrack();
        if (false !== $fastTrackData) {
            $productQuantity = $containerFastTrack->getProductQuantityFastTrack($fastTrackData['id']);
            $buttonFastTrackHTML = '<a href="'.phpSelf(['do' => 'close_fast_track']).'" style="text-decoration: none; display: box; padding: 15px; background-color: #a5af72; border: 1px solid grey;">Zamknij fast tracka</a>';
            $fastTrackHTML .= '<h2>Masz otwarty fast track <span style="color: darkred">'.$fastTrackData['container_id'].'</span> 
             </h2>'.$buttonFastTrackHTML;
        }
        $input = $fastTrackHTML.'
            

            <script type="text/javascript">
              $(function() {
                setInterval(function() {
                    $.ajax({
                      type: "GET",
                      cache: false,
                      async: false,
                      url: "ajax/continueSession.php"
                    });
                 }, 60000);
              });
            </script>';

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $input . $pView->Show() . ShowTable($pForm->ShowForm()));

    } // end of Show() function

    /**
     *
     * @return array
     */
    private function getProvidersAutoOrders($aSources)
    {

        $sSql = 'SELECT id 
             FROM external_providers AS EP 
             WHERE stock_col IS NOT NULL 
               AND symbol IN("' . implode('", "', $aSources) . '")';
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

    /**
     *
     * @return array
     */
    private function getNotConfirmedOrdersBySource()
    {
        global $aConfig;

        $aProvidersSymbols = $this->getProvidersAutoOrders(['azymut', 'ateneum', 'platon', 'siodemka']);
        $sSql = "SELECT CONCAT(DATE_FORMAT(OSH.date_send, '%d.%m.%Y'), '/', floor(hour(OSH.date_send) / 4)), OSH.source, OSH.number, OSHA.fv_nr, (SELECT name FROM external_providers AS EP WHERE EP.id=OSH.source) AS source_name, OSH.date_send, 
                DATE_FORMAT(order_date, '" . $aConfig['common']['sql_date_format'] . "') AS order_date, 
                floor(hour(OSH.date_send) / 4) AS hourgroup
                FROM orders_send_history AS OSH
                JOIN orders_send_history_items AS OSHI
                 ON OSH.id = OSHI.send_history_id
                JOIN orders_items AS OI
                 ON OI.id = OSHI.item_id
                 AND OI.status = '2'
                 AND OI.deleted = '0'
                 AND OSH.source = OI.source
                JOIN orders AS O
                 ON O.id = OI.order_id
                 AND O.order_status <> '5'
                 AND O.order_status <> '4'
                 AND O.order_status <> '3'
                LEFT JOIN orders_send_history_attributes AS OSHA
                 ON OSH.id = OSHA.orders_send_history_id
                WHERE OSH.status = '1' 
                AND OSH.magazine_status <> '2'
                AND OSH.source IN (" . implode(', ', $aProvidersSymbols) . ")
                GROUP BY OSH.id
							  ORDER BY OSH.number ASC, OSH.source ASC
                ";
        return $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);
    }

    /**
     *
     * @param array $aOrdersWaiting
     * @return array
     */
    private function getSourcesWaiting($aOrdersWaiting)
    {
        $aSourcesWaiting = [];
        foreach ($aOrdersWaiting as $aRows) {
            foreach ($aRows as $aRow) {
                foreach ($aRow as $aItem) {
                    if (!in_array($aItem['source_name'], $aSourcesWaiting)) {
                        $aSourcesWaiting[$aItem['source']] = $aItem['source_name'];
                    }
                }
            }
        }
        return $aSourcesWaiting;
    }

    /**
     *
     * @param array $aOrdersWaiting
     * @return array
     */
    private function groupBySource($aOrdersWaiting)
    {
        $aOrdersNew = [];
        foreach ($aOrdersWaiting as $iDateKey => $aOrdersDate) {
            foreach ($aOrdersDate as $aOrder) {
                $aOrdersNew[$iDateKey][$aOrder['source']][] = $aOrder;
            }
        }
        return $aOrdersNew;
    }

    /**
     *
     */
    public function getPDFRaport($pSmarty)
    {
        $aOrdersWaiting = $this->getNotConfirmedOrdersBySource();
        $aOrdersWaiting = $this->groupBySource($aOrdersWaiting);
        $aSourcesWaiting = $this->getSourcesWaiting($aOrdersWaiting);

        $pSmarty->assign_by_ref('aSourcesWaiting', $aSourcesWaiting);
        $pSmarty->assign_by_ref('aOrdersWaiting', $aOrdersWaiting);
        $sHtml = $pSmarty->fetch('m_zamowienia/raportOrderedItems.tpl');
        $pSmarty->clear_assign('aOrdersWaiting', $aOrdersWaiting);
        $pSmarty->clear_assign('aSourcesWaiting', $aSourcesWaiting);
        echo $sHtml;
        die;
        $this->getPDFfromHTML($sHtml);
        exit();
    }

    /**
     *
     */
    private function getPDFfromHTML($sHtml)
    {
        require_once('tcpdf/tcpdf.php');

        // create new PDF document
        $pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('profit24');
        $pdf->SetTitle($aLang['title']);
        $pdf->SetSubject($aLang['title']);
        $pdf->SetKeywords('');
        $pdf->setPrintHeader(false);

        // set default header data
        //$pdf->SetHeaderData('logo.gif', PDF_HEADER_LOGO_WIDTH, $aLang['logo_title'], $aLang['logo_string']);

        // set header and footer fonts
        //	$pdf->setHeaderFont(Array('freesans', '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(5, 5, 5);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 10);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l);

        // set font
        $pdf->SetFont('freesans', '', 10);
        $pdf->AddPage();
        $pdf->writeHTML($sHtml, true, false, false, false, '');

        //Close and output PDF document
        $pdf->Output('zamowienia.pdf', 'D');
    }

    /**
     *
     * @global array $aConfig
     * @param string $sFilePath
     * @param string $sSource
     * @return array
     */
    private function getFVObj($sFilePath, $sSource)
    {
        global $aConfig;

        if ($sSource == '26') {
            // super-7
            $sSourcePath = 'Siodemka';
        } else if ($sSource == '31') {
            // ateneum
            $sSourcePath = 'Ateneum';
        } else if ($sSource == '114') {
            // ateneum
            $sSourcePath = 'Panda';
        } else {
            return $sFilePath;
        }

        $sParseFile = $aConfig['common']['client_base_path'] .
            'LIB/communicator/sources/' . $sSourcePath . '/FV_XML.class.php';
        include_once($sParseFile);
        $sClassName = 'communicator\sources\\' . $sSourcePath . '\FV_XML';
        return new $sClassName($sFilePath);
    }

    /**
     *
     * @param string $sAdditionalIndeks
     * @param type $iOSHId
     * @return boolean
     */
    public function proceedAdditionalProducts($sAdditionalIndeks, $iOSHId, $bIsDefect)
    {

        $aAdditionalIndeks = explode("\n", $sAdditionalIndeks);
        if (!empty($aAdditionalIndeks)) {
            foreach ($aAdditionalIndeks as $sIndeks) {
                $sIndeks = trim($sIndeks);
                if ($sIndeks != '') {
                    $iStreamsoftId = $this->searchProductByIndex($sIndeks);
                    if ($iStreamsoftId != '') {
                        if ($this->addOrdersSendHistoryAdditionalItems($iOSHId, $sIndeks, $iStreamsoftId, $bIsDefect) === false) {
                            return false;
                        }
                    } else {
                        throw new Exception("Brak produktu o Indeksie w naszej bazie: " . $sIndeks);
                    }
                }
            }
        }
    }

    /**
     *
     * @param int $iOSHId
     * @param string $sIndeks
     * @param int $iStreamsoftId
     * @paran bool $bIsDefect
     * @return array
     */
    private function addOrdersSendHistoryAdditionalItems($iOSHId, $sIndeks, $iStreamsoftId, $bIsDefect)
    {

        $aValues = [
            'orders_send_history_id' => $iOSHId,
            'indeks' => $sIndeks,
            'streamsoft_id' => $iStreamsoftId,
            'created' => 'NOW()',
            'created_by' => $_SESSION['user']['name'],
            'defect' => (isset($bIsDefect) && $bIsDefect == '1' ? '1' : '0')
        ];
        return $this->pDbMgr->Insert('profit24', 'orders_send_history_additional_items', $aValues);
    }


    /**
     *
     * @param string $sIndeks
     * @param int $iOSHId
     * @return int
     */
    public function searchProductByIndexGetId($sIndeks)
    {

        $sSqlSelect = 'SELECT id
           FROM products AS P
           WHERE 
              isbn_plain = "%1$s" OR 
              isbn_10 = "%1$s" OR 
              isbn_13 = "%1$s" OR 
              ean_13 = "%1$s" OR
              streamsoft_indeks = "%1$s"
            ';
        $sSqlSelect = sprintf($sSqlSelect, $sIndeks);
        return $this->pDbMgr->GetOne('profit24', $sSqlSelect);
    }


    /**
     *
     * @param string $sIndeks
     * @param int $iOSHId
     * @return int
     */
    private function searchProductByIndex($sIndeks)
    {

        $sSqlSelect = 'SELECT streamsoft_indeks
           FROM products AS P
           WHERE 
              isbn_plain = "%1$s" OR 
              isbn_10 = "%1$s" OR 
              isbn_13 = "%1$s" OR 
              ean_13 = "%1$s" OR
              streamsoft_indeks = "%1$s"
            ';
        $sSqlSelect = sprintf($sSqlSelect, $sIndeks);
        return $this->pDbMgr->GetOne('profit24', $sSqlSelect);
    }

    /**
     * Metoda zapisuje dostawę
     *
     * @global array $aConfig
     * @param \Smarty $pSmarty
     * @param int $iId
     * @return void
     */
    function SaveItems(&$pSmarty, $iOSHId = 0)
    {
        global $aConfig;
        $bIsErr = false;
        $sErr = '';
        $iPackNumber = 0;


        $sSql = "SELECT pack_number FROM orders_send_history WHERE id = " . $iOSHId;
        $sDbPackNumber = Common::GetOne($sSql);

        // sprawdzamy czy do tej pory w bazie był wpisany nr kuwety
        if ($sDbPackNumber == '') {

            if (isset($_POST['pack_number'])) {
                $iPackNumber = intval($_POST['pack_number']);
            }
            if ($iPackNumber <= 0) {
                $sMsg = _('Wprowadź numer kuwety !!');
                $this->sMsg = GetMessage($sMsg);
                $this->showDetails($pSmarty, $iOSHId);
                return;
            }
        }


        $fastTrackOrdersSendHistoryItemsScanned = [];
        if (isset($_POST['fast_track_scanned_order_items']) && $_POST['fast_track_scanned_order_items'] != '') {
            $fastTrackOrdersSendHistoryItemsScanned = json_decode($_POST['fast_track_scanned_order_items']);
        }

        if (!empty($_POST['editable'])) {
            foreach ($_POST['editable'] as $sKey => &$aElement) {
                $aElement['weight'] = trim($aElement['weight']);
                if ($aElement['weight'] != "" && (!preg_match('/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/', $aElement['weight']) || $aElement['weight'] < 0 || $aElement['weight'] > 15.0)) {
                    $sErr .= "<li>Waga</li>";
                }

                if ($aElement['currquantity'] > 0 && Common::formatPrice2($aElement['weight']) <= 0.00) {
                    $sErr .= "<li>Waga</li>";
                }
            }
            if (!empty($sErr)) {
                $sErr = $aConfig['lang']['form']['error_prefix'] .
                    '<ul class="formError">' . $sErr . '</ul>' .
                    $aConfig['lang']['form']['error_postfix'];
                $this->sMsg .= GetMessage($sErr, true);
                // wystapil blad wypelnienia formularza
                // wyswietlenie formularza wraz z komunikatem bledu
                $this->showDetails($pSmarty, $iOSHId);
                return;
            }
        }

        if (stristr($_POST['fv_nr'], '.XML') !== false && file_exists($_POST['fv_nr'])) {
            $oXMLParse = $this->getFVObj($_POST['fv_nr'], $_POST['source']);
            $aItems = $oXMLParse->getProductsToStreamsoft();
            $this->validateUpdateItems($iOSHId, $aItems);
            $_POST['fv_nr'] = $oXMLParse->getFVNumber();
        }

        if (isset($_POST['fv_nr']) && $_POST['fv_nr'] != '') {
            $oOrderMagazineData = new OrderMagazineData($this->pDbMgr);
            $oOrderMagazineData->addSetHistorySendAttr($iOSHId, 'fv_nr', $_POST['fv_nr']);
        }

        $iI = 0;

//		if (!empty($_POST['delete'])) {
//      foreach($_POST['delete'] as $sKey => $sVal) {
//        unset($_POST['delete'][$sKey]);
//        $_POST['delete'][$iI] = $sKey;
//        $iI++;
//      }
        Common::BeginTransaction();


        if (!empty($fastTrackOrdersSendHistoryItemsScanned) && is_array($fastTrackOrdersSendHistoryItemsScanned)) {
            try {
                // wrzucamy rekordy do fastTracka i kuwety
                $containerFastTrack = new containerFastTrack($this->pDbMgr, $_SESSION['user']['id']);
                $containerFastTrack->addOrderSendHistoryItemToFastTrack($fastTrackOrdersSendHistoryItemsScanned);
            } catch (Exception $ex) {
                Common::RollbackTransaction();
                $sMsg = _('Błąd przyjmowania dostawy - '.$ex->getMessage());
                $this->sMsg = GetMessage($sMsg);
                $this->showDetails($pSmarty, $iOSHId);
                return;
            }
        }




        $sAddMsg = '';
        try {
            if ($this->proceedAdditionalProducts($_POST['additional_ean'], $iOSHId, false) === false) {
                $bIsErr = true;
            }

            if ($this->proceedAdditionalProducts($_POST['defect_ean'], $iOSHId, true) === false) {
                $bIsErr = true;
            }
        } catch (Exception $ex) {
            echo '1';
            $bIsErr = true;
            $sAddMsg .= $ex->getMessage();
        }

        if (!$bIsErr) {
            // ok zapisujemy dokładnie ilość
            foreach ($_POST['editable'] as $iOSHIId => $aData) {
                if ($aData['currquantity'] > 0) {
                    if ($this->saveInItem($aData, $iOSHIId) === false) {
                        $bIsErr = true;
                        echo '2';
                    }
                }
            }


            // zamknijmy tą dostawę
            if (!$bIsErr && $sDbPackNumber == '') {
                $mReturn = $this->closeInPackage($_POST['pack_number'], $iOSHId, true);
                if ($mReturn !== true) {
                    $bIsErr = $mReturn;
                    echo '3';
                }
            }

            if (!empty($_POST)) {
                $aValues = array(
                    'post_array' => serialize($_POST),
                    'in_document_type' => isset($_POST['in_document_type']) ? '2' : '1',
                    'omit_create_doc_streamsoft' => isset($_POST['omit_create_doc_streamsoft']) ? '1' : '0',
                    'source' => $_POST['source']
                );
                Common::Update('orders_send_history', $aValues, ' id = ' . $iOSHId);
            }
        }

        if ($bIsErr === false) {

            Common::CommitTransaction();

            $scTime = 0;
            if (isset($_SESSION['sc_confirm_lists_' . $iOSHId])) {
                $scTime = time() - $_SESSION['sc_confirm_lists_' . $iOSHId];
            }
            $this->AddOIILITimeItem($iOSHId, $scTime);
            unset($_SESSION['sc_confirm_lists_' . $iOSHId]);

            $sMsg = _('Dostawa została przyjęta <br /><strong>Pamiętaj o przypisaniu dostawy nawet, jeśli Kuweta jest PUSTA</strong>');
            $this->sMsg .= GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
        } else {
            Common::RollbackTransaction();

            switch (intval($bIsErr)) {
                case -1:
                    $sMsg = _('Wystąpił błąd podczas przyjmowania dostawy, podana kuweta jest zablokowana.');
                    break;

                case -2:
                    $sMsg = _('Wystąpił błąd podczas przyjmowania dostawy, taka kuweta nie istnieje.');
                    break;

                case true:
                default :
                    $sMsg = _('Wystąpił błąd podczas przyjmowania dostawy - ' . $sAddMsg);
                    break;
            }

            $this->sMsg .= GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
        }
        $this->Show($pSmarty);
//		} else {
//			$this->Show($pSmarty);
//		}
    }// end of SaveItems() method


    /**
     * @param $iOSHIId
     * @param $iTime
     * @return bool
     */
    public function AddOIILITimeItem($iOSHIId, $iTime)
    {
        $sSql = 'SELECT id FROM orders_send_history_times WHERE id = ' . $iOSHIId . ' ';
        $iOILITId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($iOILITId > 0) {
            // przcież nie będziemy aktualizować czasu zbierania
            return true;
        } else {
            $aProductsWeightQuantity = $this->getProductsWeightQuantity($iOSHIId);

            $aValues = array(
                'orders_send_history_id' => $iOSHIId,
                'time' => $iTime,
                'sc_quantity' => $aProductsWeightQuantity['quantity'],
                'sc_weight' => $aProductsWeightQuantity['weight'],
                'user_id' => $_SESSION['user']['id'],
                'created' => 'NOW()'
            );
            return ($this->pDbMgr->Insert('profit24', 'orders_send_history_times', $aValues) === false ? false : true);
        }
    }

    /**
     * @param $iOSHIId
     * @return array
     */
    private function getProductsWeightQuantity($iOSHIId)
    {

        $sSql = 'SELECT SUM(OI.weight * OSHI.in_confirmed_quantity) AS weight, SUM(OSHI.in_confirmed_quantity) AS quantity
                 FROM orders_send_history_items AS OSHI
                 JOIN orders_items AS OI
                  ON OI.id = OSHI.item_id
                 WHERE OSHI.send_history_id = ' . $iOSHIId;
        $aData = $this->pDbMgr->GetRow('profit24', $sSql);
        if (isset($aData['quantity'])) {
            return $aData;
        } else {
            $sSql = 'SELECT SUM(P.weight * OSHI.in_confirmed_quantity) AS weight, SUM(OSHI.in_confirmed_quantity) AS quantity
                 FROM orders_send_history_items AS OSHI
                 JOIN products AS P
                  ON P.id = OSHI.product_id
                 WHERE OSHI.send_history_id = ' . $iOSHIId;
            return $this->pDbMgr->GetRow('profit24', $sSql);
        }
    }

    /**
     *
     * @param int $iOSHId
     * @param array $aXMLItems
     * @param array $aPostData
     */
    public function validateUpdateItems($iOSHId, $aXMLItems)
    {

        foreach ($aXMLItems as $aItem) {
            $this->updateOSHItemPrices($iOSHId, $aItem);
        }
    }

    /**
     *
     * @param int $iOSHId
     * @param array $aItem
     * @return array
     */
    private function updateOSHItemPrices($iOSHId, $aItem)
    {

        $sSql = 'SELECT OSHI.id 
             FROM orders_send_history_items AS OSHI
             LEFT JOIN orders_items AS OI
              ON OSHI.item_id = OI.id
             LEFT JOIN products AS P
              ON P.id = OI.product_id
             LEFT JOIN products AS PP
              ON PP.id = OSHI.product_id
             WHERE
             OSHI.send_history_id = ' . $iOSHId . ' AND
             (P.ean_13 = "' . $aItem['ean_13'] . '" OR PP.ean_13 = "' . $aItem['ean_13'] . '" )';
        $aItems = $this->pDbMgr->GetCol('profit24', $sSql);
        $aToUpdate = [
            'discount' => Common::formatPrice2($aItem['discount']),
            'price_netto' => Common::formatPrice2($aItem['price_netto']),
            'vat' => $aItem['vat']
        ];
        foreach ($aItems as $iItemId) {
            if ($this->pDbMgr->Update('profit24', 'orders_send_history_items', $aToUpdate, ' id=' . $iItemId) === false) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param        object $pSmarty
     * @return    void
     */
    function ShowItems(&$pSmarty, $iId)
    {
        global $aConfig;
        $aIDs = array();
        $sDateSql = '';

        // dolaczenie klasy View
        include_once('View/View.class.php');

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header'	=> $aConfig['lang'][$this->sModule]['history'] . ' FV: ' . $_GET['fv_nr'],
            'refresh'	=> false,
            'search'	=> false,
            'checkboxes'	=> false
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 0,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'content'	=> '&nbsp;',
                'sortable'	=> false,
                'width'	=> '20'
            ),
            array(
                'db_field'	=> 'image',
                'content'	=> _("Okładka"),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'name',
                'content'	=> _("Nazwa"),
                'sortable'	=> true
            ),
            array(
                'db_field'	=> 'ean_13',
                'content'	=> _("EAN13"),
                'sortable'	=> true,
            ),
            array(
                'db_field'	=> 'ordered_quantity',
                'content'	=> _("Zamówiona ilość"),
                'sortable'	=> true,
            ),
            array(
                'db_field'	=> 'in_confirmed_quantity',
                'content'	=> _("Potwierdzona ilość"),
                'sortable'	=> true,
            ),
            array(
                'db_field'	=> 'order_number',
                'content'	=> _("Numer zamówienia"),
                'sortable'	=> true,
            ),
        );


        // pobranie liczby wszystkich aktualnosci strony
        $sSql = "SELECT COUNT(*) 
						 FROM orders_send_history_items
						 WHERE send_history_id = " . $iId;

        $iRowCount = intval($this->pDbMgr->GetOne('profit24', $sSql));

        $pView = new View('phrases', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);


        if ($iRowCount > 0) {
            // pobranie wszystkich wyszukiwanych fraz dla danego serwisu
            $sSql = "SELECT '&nbsp;', OI.name, P.ean_13, PI.directory, PI.photo, OSHI.ordered_quantity, OSHI.in_confirmed_quantity, O.order_number
							 FROM orders_send_history_items OSHI
							  LEFT JOIN orders_items OI
							  ON OI.id = OSHI.item_id
							  LEFT JOIN products P 
							  ON OI.product_id = P.id
							  LEFT JOIN products_images PI 
							  ON P.id = PI.product_id
							  LEFT JOIN orders O 
							  ON OI.order_id = O.id
							 WHERE OSHI.send_history_id = " . $iId;
            $aRecords =& $this->pDbMgr->GetAll('profit24', $sSql);

            foreach ($aRecords as $iKey => $aRecord) {
                $atmp = [
                    '&nbsp;' => array_shift($aRecord),
                    'image' => '<img src="/images/photos/' . $aRecord['directory'] . '/' . $aRecord['photo'] . '">'
                ];
                $aRecords[$iKey] = ($atmp + $aRecord);
            }

            // ustawienia dla poszczegolnych kolumn rekordu
            $aColSettings = array(
                'directory' => array(
                    'show' => false
                ),
                'photo' => array(
                    'show' => false
                )
            );

            // dodanie rekordow do widoku
            $pView->AddRecords($aRecords, $aColSettings);
        }

        $pView->AddRecordsHeader($aRecordsHeader);
        $aRecordsFooter = array(
            array('go_back')
        );
        $aRecordsFooterParams = array(
            'go_back' => array(1 => array('action', 'id', 'pid'))
        );
        $pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show());
    }


    /**
     * Metoda wyswietla liste ksiazek z tablicy $aBooks
     *
     * @param        object $pSmarty
     * @param    array ref    $aBooks    - lista produktów
     * @return    void
     */
    function GetBooksList(&$pSmarty, &$aBooks, $fastTrackItems)
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/EditableView.class.php');

        $aHeader = array(
            'refresh' => false,
            'search' => false,
            'per_page' => false,
            'checkboxes' => true,
            'editable' => true,
            'count_checkboxes' => true,
            'count_checkboxes_by' => 'quantity',
            'form' => false,
            'count_header' => true
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'content' => '&nbsp;',
                'sortable' => false,
                'width' => '20'
            ),
            array(
                'content' => $aConfig['lang'][$this->sModule]['books_list_cover']
            ),
            array(
                'content' => $aConfig['lang'][$this->sModule]['books_list_order_nr'],
                'width' => '120'
            ),
            array(
                'content' => $aConfig['lang'][$this->sModule]['books_list_name']
            ),
            array(
                'content' => $aConfig['lang'][$this->sModule]['books_list_quantity'],
                'width' => '50',
                'style' => 'font-size: 14px; font-weight: bold;',
                'class' => 'list_quantity'
            ),
            array(
                'content' => $aConfig['lang'][$this->sModule]['books_list_curr_quantity'],
                'width' => '50',
                'style' => 'font-size: 20px!important; font-weight: bold;'
            ),
            array(
                'content' => $aConfig['lang'][$this->sModule]['books_list_weight'],
                'width' => '30',
                'style' => 'font-size: 14px; font-weight: bold;'
            )
        );

        $pView = new EditableView('ordered_itm', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);

        // ustawienia dla poszczegolnych kolumn rekordu
        $aColSettings = array(
            'id' => array(
                'show' => false
            ),
            'ean_13' => array(
                'show' => false
            ),
            'isbn_13' => array(
                'show' => false
            ),
            'isbn_10' => array(
                'show' => false
            ),
            'currquantity' => array(
                'editable' => true
            ),
            'weight' => array(
                'editable' => true
            ),
        );
        // ustawienia dla kolumn edytowalnych
        $aEditableSettings = array(
            'weight' => array(
                'type' => 'text'
            ),
            'currquantity' => array(
                'type' => 'text'
            )
        );
        // dodanie rekordow do widoku
        $pView->AddRecords($aBooks, $aColSettings, $aEditableSettings);
        // dodanie stopki do widoku
        $aRecordsFooter = array(
            array('')
        );
        $pView->AddRecordsFooter($aRecordsFooter);
        // przepisanie langa - info o braku rekordow
        $aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_books_to_show'];
        // walidacja js kolumn edytowalnych
        $sAdditionalScript = '';
        foreach ($aBooks as $iKey => $aItem) {
            $itemId = $this->getOrderItemIdByOSHIId($aItem['id']);
            foreach ($fastTrackItems as $rowFastTrack) {
                if ($itemId == $rowFastTrack['id']) {
                    $sAdditionalScript .= ' 
                    var tr = $("span[item_id=\''.$aItem['id'].'\']").parent().parent()
                     tr.css("background-color", "#ffcc99");
                     tr.attr("fast_track", "1");
                     ';
                }
            }

            if ($aItem['weight'] == '0,00')
                $sAdditionalScript .= 'document.getElementById(\'delete[' . $aItem['id'] . ']\').disabled=true;
				';
        }
        $sJS = '
		<script type="text/javascript">
	// <![CDATA[

		$(document).ready(function(){
		' . $sAdditionalScript . '
			$("#ordered_itm").submit(function() {
			//alert("this");
				var sErr = "";
				$(".heditable_input, .editable_input").each(function(){
					regex = /^editable\[([A-Za-z0-9_]+)\]\[([A-Za-z0-9_]+)\]$/;
					matches = regex.exec(this.name);
					if(matches[2] == "weight"){
						$(this).val($(this).val().replace(/&nbsp;/g, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
            var sWaga = parseFloat($(this).val().replace(",", "."));
            /* || $(this).val() < 0 !$(this).val().match(/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) ||  */
						if ($(this).val() != "" && (sWaga <= 0.00 || $(this).val() > 15.0)) {
							sErr += "\t- Waga\n";
						}
					}
					
				});
				if(sErr != ""){
					alert("' . $aConfig['lang']['form']['error_prefix'] . '\n"+sErr+"' . $aConfig['lang']['form']['error_postfix'] . '");
					return false;
				}
			});
		});
	// ]]>
</script>
		';
        $sJS = '
		<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
		' . $sAdditionalScript . '
      $(".order_deleted").each(function() {
                                  $(this).parent().parent().css("background", "#FFA3A3");
                                          });
			$("#ordered_itm").submit(function() {
			  var strconfirm = confirm("Czy na pewno chcesz zamknąć dostawę ?");
              if (strconfirm == false)
              {
                return false;
              }
			//alert("this");
				var sErr = "";
				$(".heditable_input, .editable_input").each(function(){
					regex = /^editable\[([A-Za-z0-9_]+)\]\[([A-Za-z0-9_]+)\]$/;
					matches = regex.exec(this.name);
					if(matches[2] == "weight"){
						$(this).val($(this).val().replace(/&nbsp;/g, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
						if ($(this).val() != "" && (!$(this).val().match(/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) || $(this).val() < 0 || $(this).val() > 15.0)) {
							sErr += "\t- Waga\n";
						}
					}
					
				});
				if(sErr != ""){
					alert("' . $aConfig['lang']['form']['error_prefix'] . '"+sErr+"' . $aConfig['lang']['form']['error_postfix'] . '");
					return false;
				}
			});
		});
	// ]]>
</script>
		';
        return $sJS . $pView->Show();
    } // end of GetBooksList() function

    /**
     * @param $iOSHIId
     * @return mixed
     */
    private function getOrderItemIdByOSHIId($iOSHIId) {

        $sSql = 'SELECT item_id FROM orders_send_history_items WHERE id = '.$iOSHIId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }


    /**
     * metoda pobiera liste ksiazek dla wygenerowanego wydruku
     * @return array - lista ksiazek
     */
    function &getBooksNew($iId)
    {
        global $aConfig;
        $aOrders = array();

        // ksiazek spelniajacych zadane kryteria dla danego wydruku
        $sSql = "SELECT O.id, PI.photo, R.order_number, R.id AS order_id,
											A.isbn, A.name, A.quantity, '0' AS currquantity, A.weight, A.publisher, A.authors, A.publication_year, A.edition,
                      PI.directory, P.ean_13, P.isbn_13, P.isbn_10
							 FROM " . $aConfig['tabls']['prefix'] . "orders R, " . $aConfig['tabls']['prefix'] . "orders_items A
							 JOIN " . $aConfig['tabls']['prefix'] . "orders_send_history_items O
								ON O.item_id=A.id AND O.send_history_id = " . $iId . "
							 JOIN " . $aConfig['tabls']['prefix'] . "orders_send_history AS SH
								 ON O.send_history_id = SH.id AND SH.source = A.source
               LEFT JOIN products AS P
                ON A.product_id = P.id
               LEFT JOIN products_images AS PI
                ON PI.product_id = A.product_id
							 WHERE 
                    A.status = '2'
                AND A.deleted = '0'
                AND A.packet = '0'
                AND R.order_status <> '5'
                AND R.order_status <> '4'
                AND R.order_status <> '3'
                AND R.id = A.order_id
               
               GROUP BY A.id
							 ORDER BY IF(saved_type='1' || saved_type='3', A.name, A.order_id) ASC
              ";
        $aBooks = Common::GetAll($sSql);

        foreach ($aBooks as $iKey => $aItem) {
            $aBooks[$iKey]['name'] = $aItem['name'] .
                ($aItem['isbn'] ? '<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="' . $aItem['isbn'] . '" item_id="' . $aItem['id'] . '">' . $aItem['isbn'] . '</span>' : '') .
                ($aItem['ean_13'] && $aItem['ean_13'] != $aItem['isbn'] ? '<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="' . $aItem['ean_13'] . '" item_id="' . $aItem['id'] . '">' . $aItem['ean_13'] . '</span>' : '') .
                ($aItem['isbn_13'] && $aItem['isbn_13'] != $aItem['isbn'] && $aItem['isbn_13'] != $aItem['ean_13'] ? '<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="' . $aItem['isbn_13'] . '" item_id="' . $aItem['id'] . '">' . $aItem['isbn_13'] . '</span>' : '') .
                ($aItem['isbn_10'] && $aItem['isbn_10'] != $aItem['isbn'] ? '<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="' . $aItem['isbn_10'] . '" item_id="' . $aItem['id'] . '">' . $aItem['isbn_10'] . '</span>' : '') .
                ($aItem['publication_year'] ? '<br />' . $aItem['publication_year'] : '') . ($aItem['edition'] ? ' wyd. ' . $aItem['edition'] : '') .
                ($aItem['publisher'] ? '<br />' . $aItem['publisher'] : '') .
                ($aItem['authors'] ? '<br />' . $aItem['authors'] : '') . '</span>';

            if (file_exists($aConfig['common']['client_base_path'] . 'images/photos/' . $aItem['directory'] . '/__t_' . $aItem['photo'])) {
                $aBooks[$iKey]['photo'] = '<img src="/images/photos/' . $aItem['directory'] . '/__t_' . $aItem['photo'] . '" />';
            } else {
                $aBooks[$iKey]['photo'] = ' - ';
            }
            unset($aBooks[$iKey]['isbn']);
            unset($aBooks[$iKey]['publication_year']);
            unset($aBooks[$iKey]['edition']);
            unset($aBooks[$iKey]['publisher']);
            unset($aBooks[$iKey]['authors']);
            unset($aBooks[$iKey]['directory']);
            $sPrefix = '';
            /*
                    $aBooks[$iKey]['shipment_time'] = $aConfig['lang']['m_zamowienia']['shipment_'.$aItem['shipment_time']];
                    if ($aItem['promo_price_brutto'] > 0) {
                        $sPrefix = '<br>';
                        $aBooks[$iKey]['price_brutto'] = '<span style="text-decoration: line-through;">'.Common::formatPrice($aItem['price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span></span><br>'.Common::formatPrice($aItem['promo_price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
                    } else {
                        $aBooks[$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
                    }
                    unset($aBooks[$iKey]['promo_price_brutto']);
                    if ($aItem['discount'] > 0) {
                        $aBooks[$iKey]['discount'] = Common::formatPrice3($aItem['discount']);
                    }
                    else {
                        $aBooks[$iKey]['discount'] = '&nbsp;';
                    }
                    $aBooks[$iKey]['value_brutto'] = $sPrefix.Common::formatPrice($aItem['value_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
                    $aBooks[$iKey]['vat'] = Common::formatPrice3($aItem['vat']).'<span style="color: gray">%</span>';
             */
            $aBooks[$iKey]['weight'] = Common::formatPrice($aItem['weight']);
            $aBooks[$iKey]['order_number'] = '<a href="admin.php?frame=main&module_id=38&module=m_zamowienia&do=details&ppid=0&id=' . $aBooks[$iKey]['order_id'] . '">' . $aBooks[$iKey]['order_number'] . '</a>';
            unset($aBooks[$iKey]['order_id']);
        }


        return $aBooks;
    } // end of getBooks() method

    /**
     * metoda pobiera liste ksiazek usuniętych z zamówień lub zamówień anulowanych
     * @return array - lista ksiazek
     */
    function getBooksDeleted($iId)
    {
        global $aConfig;
        $aOrders = array();

        // ksiazek spelniajacych zadane kryteria dla danego wydruku
        $sSql = "SELECT O.id, PI.photo, R.order_number, R.id AS order_id,
											A.isbn, A.name, A.quantity, '0' AS currquantity, A.weight, A.publisher, A.authors, A.publication_year, A.edition,
                      PI.directory, P.ean_13, P.isbn_13, P.isbn_10
							 FROM " . $aConfig['tabls']['prefix'] . "orders R, " . $aConfig['tabls']['prefix'] . "orders_items A
							 JOIN " . $aConfig['tabls']['prefix'] . "orders_send_history_items O
								ON O.item_id=A.id AND O.send_history_id = " . $iId . "
							 JOIN " . $aConfig['tabls']['prefix'] . "orders_send_history AS SH
								 ON O.send_history_id = SH.id AND SH.source = A.source
               LEFT JOIN products AS P
                ON A.product_id = P.id
               LEFT JOIN products_images AS PI
                ON PI.product_id = A.product_id
							 WHERE 
                    A.status = '2'
                AND (A.deleted = '1' OR R.order_status = '5')
                AND A.packet = '0'
                AND R.id = A.order_id

                AND R.order_status <> '4'
                AND R.order_status <> '3'
               GROUP BY A.id
							 ORDER BY IF(saved_type='1' || saved_type='3', A.name, A.order_id) ASC
              ";
        $aBooks = Common::GetAll($sSql);

        foreach ($aBooks as $iKey => $aItem) {
            $aBooks[$iKey]['name'] = $aItem['name'] .
                ($aItem['isbn'] ? '<br /><span style="font-weight: bold; color: white;" class="isbn" isbn="' . $aItem['isbn'] . '" item_id="' . $aItem['id'] . '">' . $aItem['isbn'] . '</span>' : '') .
                ($aItem['ean_13'] && $aItem['ean_13'] != $aItem['isbn'] ? '<br /><span style="font-weight: bold; color: white;" class="isbn" isbn="' . $aItem['ean_13'] . '" item_id="' . $aItem['id'] . '">' . $aItem['ean_13'] . '</span>' : '') .
                ($aItem['isbn_13'] && $aItem['isbn_13'] != $aItem['isbn'] ? '<br /><span style="font-weight: bold; color: white;" class="isbn" isbn="' . $aItem['isbn_13'] . '" item_id="' . $aItem['id'] . '">' . $aItem['isbn_13'] . '</span>' : '') .
                ($aItem['isbn_10'] && $aItem['isbn_10'] != $aItem['isbn'] ? '<br /><span style="font-weight: bold; color: white;" class="isbn" isbn="' . $aItem['isbn_10'] . '" item_id="' . $aItem['id'] . '">' . $aItem['isbn_10'] . '</span>' : '') .
                ($aItem['publication_year'] ? '<br />' . $aItem['publication_year'] : '') . ($aItem['edition'] ? ' wyd. ' . $aItem['edition'] : '') .
                ($aItem['publisher'] ? '<br />' . $aItem['publisher'] : '') .
                ($aItem['authors'] ? '<br />' . $aItem['authors'] : '') . '</span>';

            if (file_exists($aConfig['common']['client_base_path'] . 'images/photos/' . $aItem['directory'] . '/__t_' . $aItem['photo'])) {
                $aBooks[$iKey]['photo'] = '<img src="/images/photos/' . $aItem['directory'] . '/__t_' . $aItem['photo'] . '" />';
            } else {
                $aBooks[$iKey]['photo'] = ' - ';
            }
            unset($aBooks[$iKey]['isbn']);
            unset($aBooks[$iKey]['publication_year']);
            unset($aBooks[$iKey]['edition']);
            unset($aBooks[$iKey]['publisher']);
            unset($aBooks[$iKey]['authors']);
            unset($aBooks[$iKey]['directory']);
            $sPrefix = '';

            $aBooks[$iKey]['weight'] = Common::formatPrice($aItem['weight']);
            $aBooks[$iKey]['order_number'] = '<a class="order_deleted" style="color:white" href="admin.php?frame=main&module_id=38&module=m_zamowienia&do=details&ppid=0&id=' . $aBooks[$iKey]['order_id'] . '">' . $aBooks[$iKey]['order_number'] . '<br /><span style="color:white">(anulowane)</span></a>';
            unset($aBooks[$iKey]['order_id']);

        }
        return $aBooks;
    }


    function &getBookData($iId)
    {
        global $aConfig;
        $sSql = "SELECT A.id, A.isbn, A.name, B.name AS publisher, A.publication_year
							 FROM " . $aConfig['tabls']['prefix'] . "products A
							 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers AS B
							 ON B.id = A.publisher_id
							 WHERE A.id = " . $iId;
        return Common::GetRow($sSql);
    }

    function setOrderedItems($sItems)
    {
        global $aConfig;
        if (!empty($sItems)) {
            $sSql = "UPDATE " . $aConfig['tabls']['prefix'] . "orders_items
								SET status='2'
								WHERE id IN (" . $sItems . ") AND status='1'";
            if (Common::Query($sSql) === false) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Metoda pobiera wagę produktu z tabeli products
     * @param $iProductId - id produktu
     * @return float - waga
     */
    function getProductWeight($iProductId)
    {
        global $aConfig;
        $sSql = "SELECT weight
						 FROM " . $aConfig['tabls']['prefix'] . "products
						 WHERE id = " . $iProductId;
        return Common::GetOne($sSql);
    }
} // end of Module Class
