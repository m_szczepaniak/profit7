<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - typy plików
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		//$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'opisy_statusow');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}
    
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'insert': $this->Insert($pSmarty); break;
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iId); break;
						
			case 'sort': 
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show('sort',
																			 $this->_getRecords(),
																			 phpSelf(array('do'=>'proceed_sort'),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."external_providers",
												$this->_getRecords(), array(), 'mapping_order_by');
			break;
    
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method
  
  
	/**
	 * Metoda pobiera liste rekordow
	 * 
	 * @param	integer	$iPId	- Id strony
	 * @return	array ref	- lista rekordow
	 */
	function &_getRecords() {
		global $aConfig;
		
		// pobranie wszystkich bannerow dla danej strony
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."external_providers
             WHERE stock_col IS NOT NULL
						 ORDER BY mapping_order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method


	/**
	 * Metoda wyswietla liste zdefiniowanych typów plików
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'icon',
				'content'	=> $aConfig['lang'][$this->sModule]['list_icon'],
				'sortable'	=> true,
				'width'	=> '45'
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'stock_col',
				'content'	=> $aConfig['lang'][$this->sModule]['list_stock_col'],
				'sortable'	=> true,
        'width'	=> '285'
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."external_providers
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
                   (isset($_POST['f_border_time']) && $_POST['f_border_time'] != '' ? ($_POST['f_border_time']=='1'?" AND border_time IS NOT NULL ":" AND border_time IS NULL ") : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_vat_stakes";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_file_types', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
    
		// stan publikacji
		$aBorderTime = array(
			array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_set']),
      array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_null']),  
		);
		$pView->AddFilter('f_border_time', $aConfig['lang'][$this->sModule]['f_border_time'], $aBorderTime, $_POST['f_border_time']);
    
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT id, CONCAT('<img src=\"',icon,'\" alt=\"\" />') AS icon2, name, symbol, stock_col, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."external_providers
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
                     (isset($_POST['f_border_time']) && $_POST['f_border_time'] != '' ? ($_POST['f_border_time']=='1'?" AND border_time IS NOT NULL ":" AND border_time IS NULL ") : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'name').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
        'symbol' => array( 
          'show' => false  
        ),  
				'icon'	=> array(
					'filter'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('publishers_list', 'edit','providers_auth', 'delete'),
					'params' => array (
                        'publishers_list' => array('action' => 'publishers_to_providers', 'pid'=>'{id}'),
												'edit'	=> array('id'=>'{id}'),
                        'providers_auth' => array('action' => 'providers_authorization', 'id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											),
          'icon' => array(
              'publishers_list' => 'linked2',
              'providers_auth' => 'details'
          ),
				)
			);
      
      foreach($aRecords as $iKey => $aItem) {

        if($this->checkNeedAuth($aItem)) {
          $aRecords[$iKey]['disabled'] = array('providers_auth');
        }
      }   

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('sort', 'add')
		);

		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function

  /**
   * Metoda sprawdza czy dla dostawcy potrzebne są dane logowania / czy istnieje klasa do obsługi połączenia
   * @param array $aItem
   * @return boolean
   */
	private function checkNeedAuth($aItem)
	{
		$sPath = $_SERVER['DOCUMENT_ROOT'] . 'LIB/communicator/sources/' . ucfirst($aItem['symbol']) . '/' . ucfirst($aItem['symbol']) . '.class.php';
		$sNamespace = 'communicator\sources\\';
		$sClassName = $sNamespace . ucfirst($aItem['symbol']) . '\\' . ucfirst($aItem['symbol']);
		require_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/communicator/sources/CommonSources.class.php');

		if (file_exists($sPath) && class_exists($sClassName)) {

			if (!array_key_exists($sNamespace . 'AutoOrders', class_implements($sClassName))) {
				return true;
			}
		} else {
			return true;
		}
	}
  

	/**
	 * Metoda usuwa wybrane wymiary
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego opisu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
			
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['value'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['value'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowy typ pliku
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
    if (!empty($_POST['border_time'])) {
      if (!preg_match('/^\d{2}:\d{2}:\d{2}$/', $_POST['border_time'], $aMatches)) {
        $oValidator->sError .= _('Podaj godzinę w formacie HH:MM:SS');
      }
    }
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}
		// dodanie
		
	if(!empty($_FILES)) {
			include_once('Image.class.php');
			
			$oIcon= new Image();
			$aSettings=array('thumb_size'=>'20x20');
			$oIcon->setImage('gfx/sources', $_FILES['icon'], $aSettings);
			$oIcon->proceedImage();
			$iRet=$oIcon->MoveResizedImageTo('gfx/sources', '20x20');
			switch($iRet) {
				case 1: $sIconName='gfx/sources/'.$_FILES['icon']['name'];
			}
		}
		
		
		$aValues = array(
			'name' => $_POST['name'],
      'symbol' => $_POST['symbol'],
      'border_time' => !empty($_POST['border_time']) ? $_POST['border_time'] : 'NULL',
      'stock_col' => (isset($_POST['stock_col']) && !empty($_POST['stock_col']) ? $_POST['stock_col'] : 'NULL'),
      'streamsoft_id' => $_POST['streamsoft_id'],
			'hidden_in_ordered_positions' => 1 == $_POST['hidden_in_ordered_positions'] ? 1 : 0,
			'created' => 'NOW()',
			'created_by' => $_SESSION['user']['name']
		);
		if(strlen($sIconName))
			$aValues['icon']=$sIconName;
			
		if (Common::Insert($aConfig['tabls']['prefix']."external_providers",
						 					 $aValues,
											 "",
											 false) === false) {
			$bIsErr = true;
		}
				
		if (!$bIsErr) {
			// dodano
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			// nie dodano
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			$this->AddEdit($pSmarty);
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych jezyk
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id opisu
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
    if (!empty($_POST['border_time'])) {
      if (!preg_match('/^\d{2}:\d{2}:\d{2}$/', $_POST['border_time'], $aMatches)) {
        $oValidator->sError .= _('Podaj godzinę w formacie HH:MM:SS');
      }
    }
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}
		// aktualizacja
		if(!empty($_FILES)) {
			include_once('Image.class.php');
			
			$oIcon= new Image();
			$aSettings=array('thumb_size'=>'20x20');
			$oIcon->setImage('gfx/sources', $_FILES['icon'], $aSettings);
			$oIcon->proceedImage();
			$iRet=$oIcon->MoveResizedImageTo('gfx/sources', '20x20');
			switch($iRet) {
				case 1: $sIconName='gfx/sources/'.$_FILES['icon']['name'];
			}
		}
    
		$aValues = array(
			'name' => $_POST['name'],
      'symbol' => $_POST['symbol'],
      'border_time' => !empty($_POST['border_time']) ? $_POST['border_time'] : 'NULL',
      'stock_col' => (isset($_POST['stock_col']) && !empty($_POST['stock_col']) ? $_POST['stock_col'] : 'NULL'),
      'streamsoft_id' => $_POST['streamsoft_id'],
      'hidden_in_ordered_positions' => 1 == $_POST['hidden_in_ordered_positions'] ? 1 : 0,
		  'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name']
		);
		if(strlen($sIconName))
			$aValues['icon']=$sIconName;
		
		if (Common::Update($aConfig['tabls']['prefix']."external_providers",
											 $aValues,
											 "id = ".$iId) === false) {
			$bIsErr = true;
		}
		
		if (!$bIsErr) {
			// akapit zostal zaktualizowany
//			SetModification('products_file_types',$iId);
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// akapit nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iId);
		}
	} // end of Update() funciton

  
	/**
	 * Metoda tworzy formularz dodawania / edycji typu pliku
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanego opisu statusu
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT name, symbol, icon, stock_col, border_time, streamsoft_id, hidden_in_ordered_positions
							 FROM ".$aConfig['tabls']['prefix']."external_providers
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0) {
			$sHeader .= ' "'.$aData['name'].'"';
		}

		$pForm = new FormTable('products_file_types', $sHeader, array('enctype'=>'multipart/form-data', 'action'=>phpSelf(array('id'=>$iId))), array('col_width'=>350), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
		//dump($_POST);
		// opis
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>'80', 'onkeyup'=>'synchronizeSymbol(document.getElementById(\'synchronize\').checked);'));
    
		// symbol
		$sLabel = $pForm->GetLabelHTML('symbol', $aConfig['lang'][$this->sModule]['symbol']);
		$sInput = $pForm->GetTextHTML('symbol', $aConfig['lang'][$this->sModule]['symbol'], $aData['symbol'], array('style'=>'width: 350px;', 'maxlength'=>255), '', 'text', true, '/^[a-z0-9_-]{3,128}$/').
							'&nbsp;&nbsp;'.$aConfig['lang'][$this->sModule]['synchronize'].'&nbsp;&nbsp;'.
							$pForm->GetCheckBoxHTML('synchronize', '', array(), '', false, false).
							'&nbsp;&nbsp;<a href="javascript: void(0);" onclick="synchronizeSymbol(true);" title="'.$aConfig['lang'][$this->sModule]['synchronize_now'].'"><img src="gfx/icons/synchronize_ico.gif" alt="'.$aConfig['lang'][$this->sModule]['synchronize_now'].'" border="0" align="absmiddle" /></a>';
		$pForm->AddRow($sLabel, $sInput);
    
    $pForm->AddText('border_time', $aConfig['lang'][$this->sModule]['border_time'], $aData['border_time'], array('maxlength'=>8, 'style'=>'width: 80px;'), '', 'text', false);
    
    $pForm->AddText('stock_col', $aConfig['lang'][$this->sModule]['stock_col'], $aData['stock_col'], array('maxlength'=>'128'), '', 'text', false);
    $pForm->AddText('streamsoft_id', 'Id kontrahenta Streamsoft', $aData['streamsoft_id'], array('maxlength'=>'128'), '', 'text', false);
    $pForm->AddCheckBox('hidden_in_ordered_positions', _('Ukryty w zamówionych pozycjach'), [], [], $aData['hidden_in_ordered_positions'], false);
		$pForm->AddRow($aConfig['lang'][$this->sModule]['file'], $pForm->GetFileHTML('icon', $aConfig['lang'][$this->sModule]['file'], '', array(),false));
		if($iId>0)
			$pForm->AddRow($aConfig['lang'][$this->sModule]['list_icon2'], !empty($aData['icon'])?'<img src="'.$aData['icon'].'" />':'brak');
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda pobiera liste jezykow do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id opisu
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, name as value
					 		 FROM ".$aConfig['tabls']['prefix']."external_providers
						 	 WHERE id IN  (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa stawke
	 * 
	 * @param	integer	$iId	- Id opisu statusu do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."external_providers
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method
	

} // end of Module Class
?>
