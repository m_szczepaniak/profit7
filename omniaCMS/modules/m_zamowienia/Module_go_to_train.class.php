<?php

use Items\Confirmer;
use LIB\communicator\sources\Internal\streamsoft\MMdocument;
use LIB\communicator\sources\Internal\streamsoft\sendPZ;
use LIB\Helpers\ArrayHelper;
use LIB\Supplies\StockSuppliceService;
use magazine\Containers;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__zamowienia__go_to_train {

  // komunikat
  public $sMsg;
  // nazwa modulu - do langow
  public $sModule;
  // id wersji jezykowej
  public $iLangId;
  // uprawnienia uzytkownika dla wersji jez.
  private $aPrivileges;
  // id strony modulu
  private $iPageId;
  // id uzytkownika
  private $iUId;
  // id listy zamowien
  private $iPPId;
  // id zamowienia
  private $iId;

  private $internalSources = [0, 1, 51];

  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  private $pDbMgr;

  function __construct(&$pSmarty, $bInit = true) {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;

    $this->iLangId = $_SESSION['lang']['id'];
    $_GET['lang_id'] = $this->iLangId;
    $this->sModule = $_GET['module'];

    if (isset($_GET['pid'])) {
      $iId = $_GET['pid'];
    }

    $sDo = '';

    if (isset($_GET['do'])) {
      $sDo = $_GET['do'];
    }
    if (isset($_POST['do'])) {
      $sDo = $_POST['do'];
    }

    $this->iUId = $_SESSION['user']['id'];

    $this->aPrivileges = & $_SESSION['user']['privileges'];
    // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
    if ($_SESSION['user']['type'] === 0) {
      if (!hasModulePrivileges($_GET['module_id'])) {
        showPrivsAlert($pSmarty);
        return;
      }
    }

    if ($bInit == false) {
      return;
    }
    switch ($sDo) {
      case 'get_new_pack_number':
        $this->getNewPackageNumber($pSmarty);
        break;

      case 'update_pack_number':
        $this->getUpdatePackageNumber($pSmarty);
        break;

      case 'do':
      default:
        $this->getNumber($pSmarty);
        break;
    }
  }

  /**
   * Metoda zmienia status składowych na potiwerdzone, a jeśli wszystkie są 
   *  potwierdzone i waga została podana to stają sie skompletowane
   * 
   * @param type $iOILId
   * @return bool
   */
  public function confirmOrders($iOILId) {
    require_once('OrderRecount.class.php');
    $oOrderRecount = new OrderRecount();

    $bIsErr = false;
    $confirmer = new Confirmer();
    $aInConfirmedItems = $confirmer->getOrderedItems($iOILId, array('item_id', 'in_confirmed_quantity'));

//    dump($aInConfirmedItems);
    // zaktualizuj stany pozycji
    foreach ($aInConfirmedItems as $orderItemId => $aInConfirmedItem) {
      // pobierz id zamówienia

      $aOrderItem = $confirmer->changeConfirmedQuantityItem($aInConfirmedItem);
//      dump($aOrderItem);

      if ($aOrderItem['order_id'] > 0 && $aOrderItem['deleted'] != '1' && $aOrderItem['order_status'] != '5') {
        if (intval($aOrderItem['quantity']) == ($aInConfirmedItem['in_confirmed_quantity'])) {
          // confirm
//          dump('tuu1');
          $this->confirmOrder($aOrderItem);
        } else {
//          dump('tuu2');
          $source = $aInConfirmedItems[$aOrderItem['id']]['source'];

          if (!in_array($source, $this->internalSources)) {

            if (false === $this->checkIsNewestOrderToSource($iOILId, $orderItemId)) {
                if (false === $this->checkIsConfirmedOrderItem($orderItemId)) { // jak produkt został potwierdzony z innego źródła lub z fast tracka
                    if (false === $this->pDbMgr->Query('profit24', "update orders_items set status = '0' WHERE id  = $orderItemId LIMIT 1")) {
                        $bIsErr = true;
                    }
                }
            }
          }
        }
        if ($bIsErr === false) {
          if ($oOrderRecount->recountOrder($aOrderItem['order_id'], false) === false) {
            $bIsErr = true;
          }
        }
      }
    }

    return ($bIsErr === false ? true : false);
  }// end of confirmOrders() method


    /**
     * @param $orderItemId
     * @return bool
     */
    private function checkIsConfirmedOrderItem($orderItemId) {

        $sSql = 'SELECT id 
                 FROM orders_items AS OI
                 WHERE OI.id = '.$orderItemId.' 
                 AND OI.status = "4" 
                 ';
        return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
    }


  /**
   * @param $iOSHId
   * @param $iOId
   * @return mixed
   */
  private function checkIsNewestOrderToSource($iOSHId, $iOId) {
    global $pDbMgr;

    $sSql = 'SELECT id FROM orders_send_history_items WHERE send_history_id > '.$iOSHId.' AND send_history_id <> '.$iOSHId.' AND item_id = '.$iOId;
    return ($pDbMgr->GetOne('profit24', $sSql) > 0 ? TRUE : FALSE);
  }


  /**
   * Metoda zmienia status składowych na potiwerdzone, a jeśli wszystkie są 
   *  potwierdzone i waga została podana to stają sie skompletowane
   * 
   * @param type $iOILId
   * @return bool
   */
  private function confirmOrdersOIL($iOILId) {
    require_once('OrderRecount.class.php');
    $oOrderRecount = new OrderRecount();
    require_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/Items/Confirmer.php');
    $bIsErr = false;
    $aInConfirmedItems = $this->getOrderedItemsOIL($iOILId, array('orders_items_id AS item_id', 'quantity AS in_confirmed_quantity'));
    // zaktualizuj stany pozycji
    foreach ($aInConfirmedItems as $aInConfirmedItem) {
      // pobierz id zamówienia
      $confirmer = new Confirmer();
      $aOrderItem = $confirmer->changeConfirmedQuantityItem($aInConfirmedItem);

      if ($aOrderItem['order_id'] > 0 && $aOrderItem['deleted'] != '1' && $aOrderItem['order_status'] != '5') {
        if (intval($aOrderItem['quantity']) == ($aInConfirmedItem['in_confirmed_quantity'])) {
          // confirm
          $this->confirmOrder($aOrderItem);
        }
        if ($this->changeOrderGetReadyList($aOrderItem['order_id']) === false) {
          $bIsErr = true;
        }
        if ($bIsErr === false) {
          if ($oOrderRecount->recountOrder($aOrderItem['order_id'], false) === false) {
            $bIsErr = true;
          }
        }
      }


    }
    return ($bIsErr === false ? true : false);
  }// end of confirmOrders() method
  
  /**
   * 
   * @param int $iOrderId
   * @return int
   */
  private function changeOrderGetReadyList($iOrderId) {
    
    $aValues = [
        'print_ready_list' => '0'
    ];
    if ($this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOrderId) === false) {
      return false;
    }
    $aValues = [
        'get_ready_list' => '0'
    ];
    if ($this->pDbMgr->Update('profit24', 'orders_items', $aValues, ' order_id = '.$iOrderId) === false) {
      return false;
    }
    return true;
  }


  /**
   * przeliczenie parentow
   * jezeli odznaczylismy skladowe pakietu to sprawdzamy czy pakiety sa kompletne
   * jesli tak to zmieniamy im status
    if(!$bIsErr && !empty($aParentsToRecount)){
    foreach($aParentsToRecount as $iParentId){
    if(!$bIsErr && $iParentId>0){
    $sSql='SELECT COUNT(id) FROM orders_items WHERE parent_id='.$iParentId.' AND status <> "4"';
    //echo Common::GetOne($sSql);
    if(Common::GetOne($sSql)==0) {
    //wszystkie zmatchowane zatem aktualizujemy
    $aValues2 = array(
    'status' => '4'
    );
    if(Common::Update("orders_items", $aValues2, "id = ".$iParentId) === false) {
    $bIsErr = true;
    }
    }
    }
    }
    }

    // przeliczenie zamówień
    if(!$bIsErr && !empty($aOrdersToRecount)){
    require_once('OrderRecount.class.php');
    $oOrderRecount = new OrderRecount();
    foreach($aOrdersToRecount as $iOrderId){
    if(!$bIsErr){
    if($oOrderRecount->recountOrder($iOrderId) === false){
    $bIsErr = true;
    }
    }
    }
    }
   */



  /**
   * Metoda pobiera dane na temat listy zamówionych pozyji
   *
   * @global DatabaseManager $pDbMgr
   * @param int $iOILId
   * @param array $aCols
   * @return array
   */
  private function getOrderedItemsOIL($iOILId, $aCols) {
    global $pDbMgr;

    $sSql = 'SELECT ' . implode(',', $aCols) . '
             FROM orders_items_lists_items
             WHERE orders_items_lists_id = ' . $iOILId;
    return $pDbMgr->GetAll('profit24', $sSql);
  }// end of getOrderedItems() method



  /**
   * Metoda pobiera składową zamóienia
   *
   * @global DatabaseManager $pDbMgr
   * @param int $iOrderItemId
   * @return array
   */
  private function getOrderItemData($iOrderItemId) {
    global $pDbMgr;

    $sSql = "SELECT A.id, A.quantity, A.order_id, A.product_id, A.weight, B.order_number, A.name, A.parent_id, A.deleted, B.order_status
            FROM orders_items A
            JOIN orders B
            ON B.id = A.order_id
            WHERE A.id = " . $iOrderItemId;
    return $pDbMgr->GetRow('profit24', $sSql);
  }// end of getOrderItemData() method



  /**
   * Potiwerdzamy zamówienie
   * 
   * @param array $aOrderItem
   * @return bool
   */
  private function confirmOrder($aOrderItem) {

    $aValues = array(
        'status' => '4'
    );

    if (Common::Update("orders_items", $aValues, " id = " . $aOrderItem['id']) === false) {
      return false;
    } else {
      if ($aOrderItem['parent_id'] > 0) {
        // przeliczmy rodzica, jeśli istnieje
        if ($this->confirmOrderParent($aOrderItem['parent_id']) === false) {
          return false;
        }
      }
    }
    return true;
  }// end of confirmOrder() method



  /**
   * Metoda potwierdza rodzica produktu
   * 
   * @param int $iParentId
   * @return boolean
   */
  private function confirmOrderParent($iParentId) {
    global $pDbMgr;

    $sSql = 'SELECT COUNT(id) FROM orders_items WHERE parent_id = ' . $iParentId . ' AND status <> "4"';
    if ($pDbMgr->GetOne('profit24', $sSql) == 0) {

      //wszystkie zmatchowane zatem aktualizujemy
      $aValues2 = array(
          'status' => '4'
      );
      if ($pDbMgr->Update('profit24', "orders_items", $aValues2, "id = " . $iParentId) === false) {
        return false;
      }
    }
    return true;
  }// end of confirmOrderParent() method


  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param int $iOILId
   * @param string $sNewPackNumber
   * @return boolean
   */
  private function doConfirmByOIL($iOILId, $sNewPackNumber) {
    global $pDbMgr;
    $bIsErr = false;

    // ok zmieniamy na zatwierdzone w zamówieniach i przeliczamy
    if ($this->confirmOrdersOIL($iOILId) === false) {
      $bIsErr = true;
    }

    $aValues = array(
        'package_number' => $sNewPackNumber,
        'closed' => '1'
    );
    if ($pDbMgr->Update('profit24', 'orders_items_lists', $aValues, ' id = ' . $iOILId) === false) {
      $bIsErr = true;
    }

    // @TODO leci sobie MM na te pozycje
    /**
    $sSql = "SELECT status FROM orders_send_history WHERE id = " . $iOILId;
    $iSourceStatus = Common::GetOne($sSql);

    if ($iSourceStatus == '1') {
      $oSendPZ = new LIB\communicator\sources\Internal\streamsoft\sendPZ($pDbMgr);
      try {
        $oSendPZ->doSendPZ($iOILId);
      } catch (Exception $ex) {
        echo $ex->getMessage();
        $this->sMsg .= GetMessage($ex->getMessage());
        $bIsErr = true;
      }
    }
    */
    return !$bIsErr;
  }

  /**
   * Metoda potwierdza przyjazd dostawy na magazyn głowny
   *
   * @param $iId
   *
   * @return bool
   */
  private function packageComeIn($iId) {

    global $pDbMgr;
    $bIsErr = false;

    $sSql = 'SELECT post_array, number
             FROM orders_send_history
             WHERE id = '.$iId.' AND magazine_status = "1" ';
    $aOSHData = $pDbMgr->GetRow('profit24', $sSql);

    if (!empty($aOSHData)) {

      $pDbMgr->BeginTransaction('profit24');

      include_once('Common_confirm_items.class.php');
      $oModuleConfirm = new Common_confirm_items();
      if ($oModuleConfirm->setMagazineStatus($iId, '2', $pDbMgr) === false) {
        $bIsErr = true;
      }

      if ($bIsErr == false) {
        $pDbMgr->CommitTransaction('profit24');
        $sMsg = _('Dostawa o numerze '.$aOSHData['number'].' została przyjęta na magazyn główny.');
        AddLog($sMsg, false);
        $this->sMsg .= GetMessage($sMsg, false);
      } else {
        $pDbMgr->RollbackTransaction('profit24');
        $sMsg = _('Wystąpił błąd podczas potwierdzania przyjęcia dostawy numer '.$aOSHData['number'].' na magazyn głowny.');
        AddLog($sMsg, false);
        $this->sMsg .= GetMessage($sMsg);
      }
    } else {
      $sMsg = _('Brak dostawy do potwierdzenia.');
      AddLog($sMsg);
      $this->sMsg .= GetMessage($sMsg);
    }

    return $bIsErr;

  }// end of PackageComeIn() method

  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param int $iOILId
   * @param string $sNewPackNumber
   * @return boolean
   */
  private function doConfirmOSH($iOILId, $sNewPackNumber) {
    global $pDbMgr, $aConfig;
    $bIsErr = false;

    // ok zmieniamy na zatwierdzone w zamówieniach i przeliczamy
    if ($this->confirmOrders($iOILId) === false) {
      $bIsErr = true;
    }

    $aValues = array(
        'pack_number' => $sNewPackNumber
    );
    if ($pDbMgr->Update('profit24', 'orders_send_history', $aValues, ' id = ' . $iOILId) === false) {
      $bIsErr = true;
    }

    $sSql = "SELECT status, omit_create_doc_streamsoft FROM orders_send_history WHERE id = " . $iOILId;
    $aOSHData = Common::GetRow($sSql);
    $iOmitCreateDocStramsoft = $aOSHData['omit_create_doc_streamsoft'];
    $iSourceStatus = $aOSHData['status'];

    if ($iSourceStatus == '1') {
      if ($iOmitCreateDocStramsoft != '1') {
        $oSendPZ = new sendPZ($pDbMgr);
        try {
          $test = $oSendPZ->doSendPZ($iOILId);

        } catch (Exception $ex) {
          echo $ex->getMessage();
          $this->sMsg .= GetMessage($ex->getMessage());
          $bIsErr = true;
        }
      } else {
        $this->sMsg .= GetMessage(_('Przyjęto poprawnie, ale dokument PZ nie został utworzony w streamsoft'), false);
      }
    } else {
      // akceptacja mmki i akceptacja zamowienia
      $mmDoc = new MMdocument($pDbMgr);
      $oshi = $mmDoc->getOSHI($iOILId);

      if ($oshi['mm_doc'] == 1 && true == $mmDoc->canBeCreated($oshi)) {
        try {
          if ($iOmitCreateDocStramsoft != '1') {
            if($_POST['accept_mm'] == 0){
              $mmDoc->acceptMMdoc($oshi);
              $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['mm_has_been_accepted'], false);
            }
          } else {
            $this->sMsg .= GetMessage(_('Przyjęto poprawnie, ale dokument MM nie został utworzony w streamsoft'), false);
          }
            
          //akceptacja zamowienia
          if ($this->packageComeIn($iOILId) === true) {
            $bIsErr = true;
          }
        }
        catch(Exception $e) {
          $this->sMsg .= GetMessage($e->getMessage());
          $bIsErr = true;
        }
      }
      else if($oshi['mm_doc'] == 0)
      {
        $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['mm_not_exists']);
        $bIsErr = true;
      }
    }
    return !$bIsErr;
  }

  
  /**
   * 
   * @param string $sLocation
   * @return int
   */
  private function checkContainer($sLocation) {
    $oContainers = new Containers($this->pDbMgr, 1);
    return $oContainers->checkContainer($sLocation);
  }
  
  
  /**
   * Metoda przypisuje kuwete do półki
   * 
   * @global array $aConfig
   * @global DatabaseManager $pDbMgr
   * @param type $pSmarty
   * @return boolean
   */
  private function getUpdatePackageNumber(&$pSmarty) {
    global $aConfig, $pDbMgr;
    $bIsErr = false;

    $sNewPackNumber = $_POST['new_pack_number'];

    if ($this->checkContainer($sNewPackNumber) === -2) {
      $this->sMsg = GetMessage(_('Nieprawidłowy numer tramwaju, spróbuj ponownie !'));
      return $this->getNumber($pSmarty);
    }

    $pDbMgr->BeginTransaction('profit24');
    $iOILId = $_POST['_item_id'];
    $iType = $_POST['type'];
    
    if ($iOILId > 0 && !empty($sNewPackNumber) && $sNewPackNumber > 0) {
      if ($iType == 0) {
        $sOldPackNumber = $this->getPackageNumber($iOILId);
        if ($this->doConfirmOSH($iOILId, $sNewPackNumber) === false) {
          $bIsErr = true;
        }
      } else {
        $sOldPackNumber = $this->getPackageNumberByOIL($iOILId);
        if ($this->doConfirmByOIL($iOILId, $sNewPackNumber) === false) {
          $bIsErr = true;
        }
      }

      if ($bIsErr === false) {
        include_once($aConfig['common']['client_base_path'] . 'LIB/autoloader.php');
        $oContainers = new Containers($pDbMgr);
        if ($oContainers->unlockContainer($sOldPackNumber) === false) {
          // err
          $pDbMgr->RollbackTransaction('profit24');
          $this->sMsg .= GetMessage(_('Wystąpił błąd podczas zmiany numeru kuwety'));
        } else {
          // ok
          $pDbMgr->CommitTransaction('profit24');
          $this->sMsg .= GetMessage(_('Przypisano numer półki na tramwaju: ' . $sNewPackNumber), false);
        }
      } else {
        // err
        $pDbMgr->RollbackTransaction('profit24');
        $this->sMsg .= GetMessage(_('Wystąpił błąd podczas potwierdzania jednej ze składowych zamówienia'));
      }
    } else {
      // err
      $pDbMgr->RollbackTransaction('profit24');
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas zmiany numeru kuwety'));
    }
    return $this->getNumber($pSmarty);
  }// end of getUpdatePackageNumber() method


  /**
   * Metoda pobiera numer kuwety
   * 
   * @global DatabaseManager $pDbMgr
   * @param int $iOSHId
   * @return int
   */
  private function getPackageNumberByOIL($iOSHId) {
    global $pDbMgr;

    $sSql = 'SELECT package_number 
             FROM orders_items_lists
             WHERE id = ' . $iOSHId;
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of getPackageNumberByOIL() method
  
  
  /**
   * Metoda pobiera numer kuwety
   * 
   * @global DatabaseManager $pDbMgr
   * @param int $iOSHId
   * @return int
   */
  private function getPackageNumber($iOSHId) {
    global $pDbMgr;

    $sSql = 'SELECT pack_number 
             FROM orders_send_history
             WHERE id = ' . $iOSHId;
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of getPackageNumber() method


  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param type $sPackNumber
   * @return type
   */
  private function getCountOSH($sPackNumber) {
    global $pDbMgr;
    
    $sSql = 'SELECT count(id) as ile, id 
               FROM orders_send_history
               WHERE pack_number = "' . $sPackNumber . '"
                 AND magazine_status <> "5" ';
    return $pDbMgr->GetRow('profit24', $sSql);
  }
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param type $sPackNumber
   * @return type
   */
  private function getCountOIL($sPackNumber) {
    global $pDbMgr;
    
    $sSql = 'SELECT count(id) as ile, id, `type`
               FROM orders_items_lists
               WHERE package_number = "' . $sPackNumber . '"
                 AND (closed = "0" OR closed = "3")
             ORDER BY id DESC';
    return $pDbMgr->GetRow('profit24', $sSql);
  }
  
  /**
   * 
   * @param \Smarty $pSmarty
   * @param array $aRow
   * @param int $iType
   * @return boolean
   */
  private function checkTypeContainer($pSmarty, $aRow, $iType) {
    
    if ($iType === 1 && $aRow['type'] != '3') {
      switch ($aRow['type']) {
        case '0':
          $sType = _('Zwykła');
        break;
        case '1': 
          $sType = _('Single');
        break;
        case '3': 
          $sType = _('Tramwajowa');
        break;
        case '2':
          $sType = _('Połączone (wspólna wysyłka)');
        break;
      }
      $sMsg = sprintf(_('Kuweta nie jest przeznaczona na tramwaj - Kuweta %s'), $sType);
      $this->sMsg = GetMessage($sMsg);
      $this->getNumber($pSmarty);
      return false;
    }
    return true;
  }// end of checkTypeContainer() method
  
  /**
   * 
   * @param \Smarty $pSmarty
   * @param string $sPackNumber
   * @param array $aRow
   * @param int $iType
   */
  private function getNumberFormType($pSmarty, $sPackNumber, $aRow, $iType) {
    if ($aRow['ile'] > 1) {
      $this->sMsg = GetMessage(_('Wprowadzono zbyt wiele kuwet do jednego zamówienia'));
      $this->getNumber($pSmarty);
      return false;
    }
    
    if ($this->checkTypeContainer($pSmarty, $aRow, $iType) === false) {
      return false;
    }
    
    if ($aRow['ile'] == 1 && $aRow['id'] > 0) {
      $this->sMsg = GetMessage(_('Wybrano kuwetę o nr: ' . $sPackNumber), false);
      $this->getNumber($pSmarty, $iType, $aRow['id'], true);
      return true;
    } elseif ($aRow['ile'] > 1) {
      $this->sMsg = GetMessage(_('Wprowadzono zbyt wiele kuwet do jednego zamówienia'));
      $this->getNumber($pSmarty);
    } else {
      $this->sMsg = GetMessage(_('Wprowadzono nieprawidłowy numer kuwety'));
      $this->getNumber($pSmarty);
    }
    return false;
  }

  /**
   * Metoda pobiera numer półki na tramwaju
   * 
   * @global DatabaseManager $pDbMgr
   * @global array $aConfig
   * @param \Smarty $pSmarty
   */
  private function getNewPackageNumber(&$pSmarty) {
    global $pDbMgr, $aConfig;

    $sPackNumber = $_POST['pack_number'];
    include_once($aConfig['common']['client_base_path'] . 'LIB/autoloader.php');
    $oContainers = new Containers($pDbMgr);
    $iReturn = $oContainers->checkContainer($sPackNumber);

    if ($iReturn === -1) {
      
      $aRowOSH = $this->getCountOSH($sPackNumber);
      if ($this->getNumberFormType($pSmarty, $sPackNumber, $aRowOSH, 0) === false) {
        $aRowOIL = $this->getCountOIL($sPackNumber);
        $this->getNumberFormType($pSmarty, $sPackNumber, $aRowOIL, 1);
      }
      
    } else {
      if ($iReturn === 1) {
        $this->sMsg = GetMessage(_('Ta kuweta jest zwolniona'));
        $this->getNumber($pSmarty);
      } elseif ($iReturn === -2) {
        $this->sMsg = GetMessage(_('Brak takiej kuwety w systemie'));
        $this->getNumber($pSmarty);
      } else {
        $this->sMsg = GetMessage(_('Wystąpił błąd ogólny uniemożliwiający przepisanie kuwety'));
        $this->getNumber($pSmarty);
      }
    }
  }// end of getNewPackageNumber() method



  /**
   * Wyświetla formularz wprowadzenia numeru kuwety
   * 
   * @param type $pSmarty
   */
  private function getNumber(&$pSmarty, $iType = 0, $iOILId = 0, $bUpdate = false) {

    $aData = array();
    if (!empty($_POST)) {
      $aData = $_POST;
    }
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('ordered_itm', _("Przypisz kuwetę do półki na tramwaju // ПРИПИШИТЕ ЗАКАЗ К ПОЛКЕ"), array('action' => phpSelf()), array('col_width' => 155), false);

    if ($bUpdate == false) {
      $pForm->AddText('pack_number', _('Numer kuwety'), $aData['pack_number'], array('style' => 'font-size: 45px'), '', 'uint', true);
      $pForm->AddHidden('do', 'get_new_pack_number');
      $pForm->AddInputButton("save", _('Dalej'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
    } else {
      $pForm->AddText('new_pack_number', _('Numer półki na tramwaju'), $aData['new_pack_number'], array('style' => 'font-size: 45px'), '', 'uint', true);
        $pForm->AddHidden('accept_mm', 0);
      $pForm->AddHidden('do', 'update_pack_number');

      $pForm->AddHidden('type', $iType);
      $pForm->AddHidden('_item_id', $iOILId);
      
      $pForm->AddInputButton("save", _('Zmień'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
    }
    $sJs = '
    <script type="text/javascript">
      $(function() {
        if ($("#pack_number").length > 0) {
         $("#pack_number").focus();
        } else {
         $("#new_pack_number").focus();
        }
     });
     </script>
';

    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()) . $sJs);
  }

}
