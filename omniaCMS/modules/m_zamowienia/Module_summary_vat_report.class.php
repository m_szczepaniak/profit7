<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - statystyki sprzedazy produktow
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
// dolaczenie wspolnej klasy Module_Common
include_once('modules/m_oferta_produktowa/Module_Common.class.php');
 
class Module extends Module_Common {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// ID strony modulu zamowien
	var $iPageId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  private $sDBFFilePath = 'import/DBF/summary_vat';
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty, $bInit = FALSE) {
		global $aConfig;
    
    if ($bInit === TRUE) {
      return true;
    }
    
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_zamowienia', $_GET['action']);
		$this->sModule = $_GET['module'];
		
		$sDo = '';
		$iId = 0;
				
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		// wyswietlenie
		switch ($sDo) {
			case 'show_stats':
				$this->showStats($pSmarty);
			break;
      case 'delete':
        $this->delete($pSmarty, $_GET['item']);
			case 'choose_conditions':
			default:
				$this->chooseConditions($pSmarty);
			break;
		}
	} // end of Module() method

  
  /**
   * Metoda usuwa plik z eksportem
   * 
   * @global array $aConfig
   * @param type $pSmarty
   * @param type $sFile
   * @return type
   */
  public function delete($pSmarty, $sFile) {
    global $aConfig;
    $bIsErr = FALSE;
    
    $sPath = $aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath;
    if (file_exists($sPath.'/'.$sFile)) {
      if (unlink($sPath.'/'.$sFile) === FALSE) {
        $bIsErr = TRUE;
      }
    } else {
      $bIsErr = TRUE;
    }
    
    if ($bIsErr === TRUE) {
      $this->sMsg = sprintf(_('Wystąpił błąd usuwania pliku pdf "%s"'), $sFile);
      AddLog($this->sMsg, true);
      $this->sMsg = GetMessage($this->sMsg, true);
      return $this->chooseConditions($pSmarty);
    } else {
      $this->sMsg = sprintf(_('Plik pdf "%s" został usunięty'), $sFile);
      AddLog($this->sMsg, false);
      $this->sMsg = GetMessage($this->sMsg, false);
      return $this->chooseConditions($pSmarty);
    }
  }// end of delete() method
  
  
	/**
	 * Metoda tworzy formularz wyboru danych do statystyk
	 * oraz wyswietla statystyki
	 *
	 * @param		object	$pSmarty
	 */
	function chooseConditions(&$pSmarty) {
		global $aConfig;
		$aLang =& $aConfig['lang'][$this->sModule];
				
		$aData = array();
		$aConditions = array('start_date', 'end_date');
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			// zczytanie z GET
			foreach ($aConditions as $sCond) {
				$aData[$sCond] = isset($_GET[$sCond]) ? $_GET[$sCond] : '';
			}
		}
//		if (!isset($aData['start_date']) || $aData['start_date'] == '') {
//			$aData['start_date'] = '00-00-0000';
//		}
		//if (!isset($aData['order_status']) || $aData['order_status'] == '') {
	//	$aData['order_status'] = '-1';
		//}
						
		$pForm = new FormTable('stats_conditions', $aLang['header'], array('action'=>phpSelf(array())), array('col_width'=>190), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'show_stats');
		
		// zakres dat od
		$pForm->AddRow($pForm->GetLabelHTML('', $aLang['dates_range']), '<div style="float: left; margin-top: 8px;">'.$pForm->GetLabelHTML('start_date', $aLang['start_date_from'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('start_date', $aLang['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date').'</div><div style="float: left; margin-top: 8px; padding-left: 25px;">'.$pForm->GetLabelHTML('end_date', $aLang['end_date_to'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('end_date', $aLang['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date').'</div>');
									 

		// przyciski		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aLang['button_show']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('action'=>'statistics')).'\');'), 'button'));
		
    
    $sJS = '<script type="text/javascript">
		var sSourceRowSel = "'.($aData['source'] != '-1' ? $aData['source'] : '').'";
		</script>';
    
    $pForm->AddMergedRow(_('Wygenerowane raporty magazynowe w formacie DBF'), array('class' => 'merged'));
    $aZipFiles = $this->getPDFList();
    if (!empty($aZipFiles)) {
      foreach ($aZipFiles as $aZipFile) {
        $pForm->AddRow('', '<a style="font-size: 15px!important;" target="_blank" href="'.$aZipFile['value'].'">'.$aZipFile['label'].'</a> 
                                            ('.$aZipFile['last_modified'].') 
                                            <a title="Usuń raport" href="'.phpSelf(array('do' => 'delete', 'item' => $aZipFile['label'])).'"><img src="gfx/icons/delete_ico.gif" /></a>');
      }
    }
    $pForm->AddMergedRow(_(''), array('class' => 'merged'));
    
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of chooseConditions() function
  
  
  /**
   * Metoda zwraca listę plików do zapakowania zipem
   * 
   * @global array $aConfig
   * @return array
   */
  private function getPDFList() { 
    global $aConfig;
    
    $sURL = $aConfig['common']['client_base_url_https'].$this->sDBFFilePath.'/';
    $aZipFiles = array();
    
    $sPath = $aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath;
    
    if ($handle = opendir($sPath)) {
      while (false !== ($sFile = readdir($handle))) {
        if ($sFile != "." && $sFile != "..") {
          $aMatches = array();
          preg_match('/([^\/]*(\.pdf))$/', $sFile, $aMatches);
          if (isset($aMatches[1]) && !empty($aMatches[1])) {
            $aZipFiles[] = array(
                'label' => $sFile,
                'value' => $sURL.$sFile,
                'last_modified' => date("d.m.Y H:i:s", filemtime($sPath.'/'.$sFile))
            );
          }
        }
      }
      closedir($handle);
    }
    arsort($aZipFiles);
    return $aZipFiles;
  }// end of getZIPList() method
  
	
	/**
	 * Metoda wyswietla statystyki dla wybranych warunkow
	 *
	 * @param		object	$pSmarty
	 */
	function showStats(&$pSmarty) {
		global $aConfig;
		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';
		$sSourceSql = '';
		$sSourceIt = '';
		$aSource = array();
		
		$aReport = array();
		
		$aReport['lang'] =& $aConfig['lang'][$this->sModule];
		$aReport['date_start'] = formatDateClient(FormatDate($_POST['start_date']));
		$aReport['date_end'] = formatDateClient(FormatDate($_POST['end_date']));
		
		$_GET['hideHeader'] = '1';
		
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_seller_data";
		$aReport['seller_data'] =& Common::GetRow($sSql);
		
		// pobierzmy id i symbole dostepnych serwiwsów
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."websites";
		$aWebsites =& Common::GetAll($sSql);
		foreach ($aWebsites as $aWebsite) {
      $aMultiOrders = array();
			$aMultiOrders['postal_fee'] =& $this->getOrdersReport('postal_fee', $aWebsite['id'], $aWebsite['name']);
			if(!empty($aMultiOrders['postal_fee']['items'])){
				$aReport['postal_fee'][$aWebsite['id']] =& $this->getOrderListHtml($aMultiOrders['postal_fee']);
			}
      

			$aMultiOrders['platnoscipl'] =& $this->getOrdersReport('platnoscipl', $aWebsite['id'], $aWebsite['name']);
			if(!empty($aMultiOrders['platnoscipl']['items'])){
				$aReport['platnoscipl'][$aWebsite['id']] =& $this->getOrderListHtml($aMultiOrders['platnoscipl']);
			}

			$aMultiOrders['bank_transfer'] =& $this->getOrdersReport('bank_transfer', $aWebsite['id'], $aWebsite['name']);
			if(!empty($aMultiOrders['bank_transfer']['items'])){
				$aReport['bank_transfer'][$aWebsite['id']] =& $this->getOrderListHtml($aMultiOrders['bank_transfer']);
			}

			$aMultiOrders['bank_14days'] =& $this->getOrdersReport('bank_14days', $aWebsite['id'], $aWebsite['name']);
			if(!empty($aMultiOrders['bank_14days']['items'])){
				$aReport['bank_14days'][$aWebsite['id']] =& $this->getOrderListHtml($aMultiOrders['bank_14days']);
			}
      
			if(!empty($aMultiOrders)){
				$aReport['summary'][$aWebsite['id']] =& $this->getOrderSummaryListHtml($aMultiOrders);
			}
		}
		
		$pSmarty->assign_by_ref('aRaport', $aReport);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'summary_report_vat.tpl');
		$pSmarty->clear_assign('aRaport', $aReport);
		
		require_once('OLD_tcpdf/config/lang/pl.php');
		require_once('OLD_tcpdf/tcpdf.php');		
		
		// create new PDF document
		$pdf = new TCPDF ('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('profit24');
		$pdf->SetTitle($aLang['title']);
		$pdf->SetSubject($aLang['title']);
		$pdf->SetKeywords('');
		$pdf->setPrintHeader(false);
		$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));
			
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$pdf->SetMargins(8, 5, PDF_MARGIN_RIGHT);
		//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 10);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
		
		//set some language-dependent strings
		$pdf->setLanguageArray($l); 
		
		// set font
		$pdf->SetFont('freesans', '', 8);
		
		$pdf->AddPage();
		$pdf->writeHTML($sHtml, true, false, false, false, '');
		
		// nazwa pliku - uwzgledniajaca daty
		$sFileName = $aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath.'/raport_zbiorczy_vat-'.($_POST['start_date'] == $_POST['end_date'] ? str_replace('-', '', $_POST['start_date']) : str_replace('-', '', $_POST['start_date']).'-'.str_replace('-', '', $_POST['end_date'])).'.pdf';
		$pdf->Output($sFileName, 'F');
		
	} // end of showStats() function
	

	
	/**
	 * 
	 * @param $sPaymentType
	 * @return unknown_type
	 */
	function &getOrdersReport($sPaymentType, $iWId, $sWName){
		global $aConfig;
		$sPaymentSql = '';
		/*
		if ($sPaymentType == 'platnoscipl') {
						$sPaymentSql = "(   (A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay')
											 OR (A.second_payment_type = 'platnoscipl' OR A.second_payment_type = 'card' OR A.second_payment_type = 'dotpay')
											)
											 ";
		}
		else {
			$sPaymentSql = "((A.payment_type = '".$sPaymentType."' AND A.second_payment_type='') OR (A.second_payment_type = '".$sPaymentType."'))";
		}
		 */
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(   (A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay')
											 OR (A.second_payment_type = 'platnoscipl' OR A.second_payment_type = 'card' OR A.second_payment_type = 'dotpay')
											)
											 ";
		}
		else {
			$sPaymentSql = " (A.payment_type = '".$sPaymentType."' OR A.second_payment_type = '".$sPaymentType."') ";
		}
		$sSql = "SELECT id, second_invoice
							FROM ".$aConfig['tabls']['prefix']."orders A
							 WHERE ".$sPaymentSql."  AND
										 A.website_id = ".$iWId." AND
							 			 A.invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			 A.invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'
							 ORDER BY 	A.invoice_date";

		$aOrders =& Common::GetAll($sSql);
		$aTable= array();
		$iLp=1;
		$aVatStakes=array();
		$aTable['items'] = array();
		
		if(!empty($aOrders)){
			$sSql='SELECT value FROM '.$aConfig['tabls']['prefix'].'products_vat_stakes WHERE 1';
			$aVatStakesT=Common::GetAll($sSql);
			//przepisanie do tablicy
			foreach($aVatStakesT as $aVat)
				$aVatStakes[]=$aVat['value'];
				
			foreach($aOrders as $iKey=>$aOrder) {
				$aItem = $this->getOrderVat($aOrder['id'],false,$iLp++, $sPaymentType);
				$aTable['items'][] = $aItem;
				//sprawdzenie czy są jakieś inne stawki
				foreach($aItem['vat'] as $iOVats=>$aVaVa)
					if(!in_array($iOVats,$aVatStakes))
						$aVatStakes[]=$iOVats;
						
				foreach($aVatStakes as $iKey => $aVatItem) {
					$aTable['vat_'.$aVatItem.'_brutto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_brutto']);
					$aTable['vat_'.$aVatItem.'_netto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_netto']);

					}
				if($aOrder['second_invoice'] == '1') {
					$aItem = $this->getOrderVat($aOrder['id'],true,$iLp++, $sPaymentType);
					$aTable['items'][] = $aItem;
					foreach($aVatStakes as $iKey => $aVatItem) { 
						$aTable['vat_'.$aVatItem.'_brutto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_brutto']);
					  $aTable['vat_'.$aVatItem.'_netto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_netto']);

					}
				}
			}
		}
		foreach($aVatStakes as $iKey => $aVatItem) {
			// przeliczanie dla stawki VAT 0 - sformatowane z '.' jako separator dziesietny
			$aTable['vat_'.$aVatItem.'_brutto'] = Common::formatPrice2($aTable['vat_'.$aVatItem.'_brutto']);
			//$aTable['vat_'.$aVatItem.'_netto'] =  Common::FormatPrice2($aTable['vat_'.$aVatItem.'_netto']);
			$aTable['vat_'.$aVatItem.'_netto'] = Common::formatPrice2($aTable['vat_'.$aVatItem.'_brutto'] / (1 + $aVatItem/100));
			
			$aTable['vat_'.$aVatItem.'_currency'] = $aTable['vat_'.$aVatItem.'_brutto'] - $aTable['vat_'.$aVatItem.'_netto'];
			$aTable['total_brutto'] += $aTable['vat_'.$aVatItem.'_brutto'];
			$aTable['total_netto'] += $aTable['vat_'.$aVatItem.'_netto'];
			$aTable['total_vat'] += $aTable['vat_'.$aVatItem.'_currency'];
		
		
			}
		
		// obliczanie sumy zamowienia - sformatowanie z ',' jako separator dziesietny
		$aTable['total_brutto'] = Common::formatPrice($aTable['total_brutto']);
		$aTable['total_netto'] = Common::formatPrice($aTable['total_netto']);
		$aTable['total_vat'] = Common::formatPrice($aTable['total_vat']);
		foreach($aVatStakes as $iKey => $aVatItem) {
			// formatowanie wartosci stawki VAT 0 z ',' jako separator dziesietny
			$aTable['vat_'.$aVatItem.'_value'] = Common::formatPrice($aTable['vat_'.$aVatItem.'_netto']);
			$aTable['vat_'.$aVatItem.'_currency'] = Common::formatPrice($aTable['vat_'.$aVatItem.'_currency']);
			if($aTable['vat_'.$aVatItem.'_value']>0.0)
				$aTable['show_vat_'.intval($aVatItem)]=true;
		}
		$aNewItems=array();
		$aMapping=array();
		$iPosition=0;
		foreach($aTable['items'] as $aRow) {
			//dump($aRow);
			if(($iPos=$aMapping[$aRow['invoice_date']])===null) {
				//nie ma jeszcze takiej daty dodajemy pozycje
				$aTmpItem=array(
					'lp'=>$iPosition+1,
					'order_date'=>$aRow['invoice_date'],
					'value_netto' =>Common::FormatPrice2($aRow['value_netto']),
    			'value_brutto'=>Common::FormatPrice2($aRow['value_brutto']),
					'vat'=>array(),
    			'order_vat'=>Common::FormatPrice2($aRow['order_vat'])
					);
					
				foreach($aRow['vat'] as $iKey=>$aStake) {
					$aTmpItem['vat'][$iKey]=array(
						'quantity' => $aStake['quantity'],
	          'vat_currency' => Common::FormatPrice2($aStake['vat_currency']),
	          'value_netto' => Common::FormatPrice2($aStake['value_netto']),
	          'value_brutto' => Common::FormatPrice2($aStake['value_brutto'])
						);
					}
				$aNewItems[$iPosition]=$aTmpItem;	
				$aMapping[$aRow['invoice_date']]=$iPosition;
				++$iPosition;
				}
			else {
				
				//istnieje opcja sumujemy brutto netto i stawki vat
				$aNewItems[$iPos]['value_netto']+=Common::FormatPrice2($aRow['value_netto']);
				$aNewItems[$iPos]['value_brutto']+=Common::FormatPrice2($aRow['value_brutto']);
				$aNewItems[$iPos]['order_vat']+=Common::FormatPrice2($aRow['order_vat']);
				foreach($aRow['vat'] as $iKey=>$aStake) {
					$aNewItems[$iPos]['vat'][$iKey]['quantity']+=$aStake['quantity'];
					$aNewItems[$iPos]['vat'][$iKey]['vat_currency']+=Common::FormatPrice2($aStake['vat_currency']);
					$aNewItems[$iPos]['vat'][$iKey]['value_netto']+=Common::FormatPrice2($aStake['value_netto']);
					$aNewItems[$iPos]['vat'][$iKey]['value_brutto']+=Common::FormatPrice2($aStake['value_brutto']);
					}
				}	
			}		
		
		foreach($aNewItems as $iPos =>$aItem) {
			//$aNewItems[$iPos]['value_netto']	=Common::FormatPrice($aItem['value_netto']);
			$aNewItems[$iPos]['value_brutto']	=Common::FormatPrice($aItem['value_brutto']);
			//$aNewItems[$iPos]['order_vat']		=Common::FormatPrice($aItem['order_vat']);
			$fTmpNetto=0.0;
			foreach($aItem['vat'] as $iKey=>$aStake) {
				$fTmpNetto+= Common::FormatPrice2($aNewItems[$iPos]['vat'][$iKey]['value_netto']	=Common::FormatPrice($aStake['value_netto']));//$aStake['value_netto']
				$aNewItems[$iPos]['vat'][$iKey]['value_brutto']	=Common::FormatPrice($aStake['value_brutto']);
				$aNewItems[$iPos]['vat'][$iKey]['vat_currency']	=Common::FormatPrice(Common::FormatPrice2($aNewItems[$iPos]['vat'][$iKey]['value_brutto'])-Common::FormatPrice2($aNewItems[$iPos]['vat'][$iKey]['value_netto']));//$aStake['vat_currency']
				}
			$aNewItems[$iPos]['value_netto']	=Common::FormatPrice($fTmpNetto);
			$aNewItems[$iPos]['order_vat']		=Common::FormatPrice($aItem['value_brutto']-$fTmpNetto);	
		}	
			
		
		$aTable['items'] = $aNewItems;
		$aTable['name'] = $sWName;
		return $aTable;
	} // end of getOrdersReport() method 
	
	function getOrderListHtml(&$aOrders){
		global $aConfig,$pSmarty;
		$aModule = array();
		$aModule['lang'] =& $aConfig['lang'][$this->sModule];
		
		$aModule['table'] =& $aOrders;
		
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'orders_table3.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	} // end of getProductListHtml() method
  
  
	function getOrderSummaryListHtml(&$aMultiOrders){
		global $aConfig,$pSmarty;
		$aModule = array();
		$aModule['lang'] =& $aConfig['lang'][$this->sModule];
		
		$aModule['table'] =& $aMultiOrders;
    $aModule['summary'] = $this->_getSummary($aMultiOrders);
    
    $aModule['table_cols'] = $this->_checkShowVatStates($aMultiOrders);
//		dump($aMultiOrders);
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'summary_summary_report_vat.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	} // end of getProductListHtml() method
  
  
  /**
   * Metoda generuje podsumowania
   * 
   * @param type $aMultiOrders
   * @return type
   */
  private function _getSummary($aMultiOrders) {
    $aSummary = array();
    
    foreach ($aMultiOrders as $sMethod => $aOrders) {
      foreach ($aOrders as $sKey => $mData) {
        if (!is_array($mData) && $sKey != 'name') {
          $aSummary[$sKey] += Common::formatPrice2($mData);
        }
      }
    }
    
    foreach ($aSummary as $sKey => $mData) {
      $aSummary[$sKey] = Common::formatPrice($mData);
    }
    return $aSummary;
  }// end of _getSummary() method
  
  /**
   * Metoda sprawdza na listach, które podsumowania potrzebują jakich stawek Vatt
   * 
   * @param type $aMultiOrders
   * @return boolean
   */
  private function _checkShowVatStates($aMultiOrders) {
    $aVatStakes = array();
    $aTableCols = array();
    
    $sSql='SELECT value FROM '.$aConfig['tabls']['prefix'].'products_vat_stakes WHERE 1';
			$aVatStakesT=Common::GetAll($sSql);
			//przepisanie do tablicy
			foreach($aVatStakesT as $aVat)
				$aVatStakes[]=$aVat['value'];
      
    foreach ($aMultiOrders as $aOrder) {
      foreach ($aVatStakes as $iVatState) {
        if ($aOrder['show_vat_'.intval($iVatState)] == true) {
          $aTableCols['show_vat_'.intval($iVatState)] = true;
        }
      }
    }
    return $aTableCols;
  }// end of _checkShowVatStates() method
	

	function &getOrderInvoiceAddress($iId, $bSecondInvoice=false){
		global $aConfig;
		
		$sSql = "SELECT is_company, company, name, surname, street, number, number2, postal, city, nip, phone
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
						 " AND address_type = '".($bSecondInvoice?'2':'0')."'
						  ORDER BY address_type"; 
		return Common::GetRow($sSql);
	} // end of getOrderInvoiceAddress() method
	
	
	/**
	 * Metoda generuje vat
	 *
	 * @global type $aConfig
	 * @global type $pSmarty
	 * @param type $iId
	 * @param type $bSecondInvoice
	 * @param type $iLp
	 * @return string 
	 */
	function getOrderVat($iId,$bSecondInvoice=false,$iLp, $sPaymentType){
	global $aConfig,$pSmarty;
	
		include_once('OrderItemRecount.class.php');
		$oOrderItemRecount = new OrderItemRecount(TRUE);
		$aOrderData = $oOrderItemRecount->getOrderPricesDetail($iId, $bSecondInvoice, $sPaymentType);
		$aOrder = $aOrderData['order'];
		$aOrder['lp'] = $iLp;

		if($bSecondInvoice){
			$aOrder['invoice_number'] = $aOrder['invoice2_id'];
			$aAddress = $this->getOrderInvoiceAddress($aOrder['id'], true);
		} else {
			$aOrder['invoice_number'] = $aOrder['invoice_id'];
			$aAddress = $this->getOrderInvoiceAddress($aOrder['id']);
		}
		
		if($aAddress['is_company'] == '1'){
			$aOrder['name'] = $aAddress['company'];
			$aOrder['nip'] = $aAddress['nip'];
		} else {
			$aOrder['name'] = $aAddress['surname'].' '.$aAddress['name'];
			$aOrder['nip'] = '&nbsp;';
		}
		
		return $aOrder;
	} // end of getInvoiceHtml() method
	
	
		/**
	 * Pobiera wysokosc stawki VAT dla metody płatności/transportu 
	 * @param int $iId - id zamówienia
	 * @return float - stawka vat
	 */
	function getPaymentVat($iId){
		global $aConfig;
		$sSql = "SELECT vat
						FROM ".$aConfig['tabls']['prefix']."orders_payment_types
						WHERE id = ".$iId;
		return intval(Common::GetOne($sSql));
	}
} // end of Module Class
?>