<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - rabat ogolny
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'opisy_statusow');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty); break;
			default: $this->AddEdit($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda aktualizuje w bazie danych opis statusu
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id opisu
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}

	// aktualizacja
			$aValues = array(
				'pay_reminder_days' => intval($_POST['pay_reminder_days']),
				'collect_info_days' => intval($_POST['collect_info_days']),
				'platnosci_reminder_days' => intval($_POST['platnosci_reminder_days']),
				'cancel_days' => intval($_POST['cancel_days']),
        'unactive_reminder' => intval($_POST['unactive_reminder']),
			);
			$sSql = "SELECT COUNT(1)
								FROM ".$aConfig['tabls']['prefix']."orders_mails_times
								WHERE id = ".getModuleId('m_zamowienia');
			if(intval(Common::GetOne($sSql))>0){
				if (Common::Update($aConfig['tabls']['prefix']."orders_mails_times",
													 $aValues,
													 "id = ".getModuleId('m_zamowienia')) === false) {
					$bIsErr = true;
				}
			} else {
				$aValues['id'] = getModuleId('m_zamowienia');
				if (Common::Insert($aConfig['tabls']['prefix']."orders_mails_times",
													 $aValues,
													 '', false) === false) {
					$bIsErr = true;
				}
			}
		
		if (!$bIsErr) {
			// akapit zostal zaktualizowany
			$sMsg = $aConfig['lang'][$this->sModule]['edit_ok'];
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->AddEdit($pSmarty);
		}
		else {
			// akapit nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = $aConfig['lang'][$this->sModule]['edit_err'];
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji opisu statusu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanego opisu statusu
	 */
	function AddEdit(&$pSmarty) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."orders_mails_times
							 WHERE id = ".getModuleId('m_zamowienia');
			$aData =& Common::GetRow($sSql);
		

		$sHeader = $aConfig['lang'][$this->sModule]['header'];


		$pForm = new FormTable('mail_times', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>450), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');
		
		$pForm->AddText('pay_reminder_days', $aConfig['lang'][$this->sModule]['pay_reminder_days'], $aData['pay_reminder_days'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
		$pForm->AddText('collect_info_days', $aConfig['lang'][$this->sModule]['collect_info_days'], $aData['collect_info_days'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
		$pForm->AddText('platnosci_reminder_days', $aConfig['lang'][$this->sModule]['platnosci_reminder_days'], $aData['platnosci_reminder_days'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
		$pForm->AddText('cancel_days', $aConfig['lang'][$this->sModule]['cancel_days'], $aData['cancel_days'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
    $pForm->AddText('unactive_reminder', $aConfig['lang'][$this->sModule]['unactive_reminder'], $aData['unactive_reminder'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	

} // end of Module Class
?>