<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - statystyki sprzedazy produktow
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
// dolaczenie wspolnej klasy Module_Common
include_once('modules/m_oferta_produktowa/Module_Common.class.php');
 
class Module extends Module_Common {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// ID strony modulu zamowien
	var $iPageId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;
		
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_zamowienia', $_GET['action']);
		$this->sModule = $_GET['module'];
		
		$sDo = '';
		$iId = 0;
				
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		// wyswietlenie
		switch ($sDo) {
			case 'show_stats':
				$this->showStats($pSmarty);
			break;
			case 'choose_conditions':
			default:
				$this->chooseConditions($pSmarty);
			break;
		}
	} // end of Module() method


	/**
	 * Metoda tworzy formularz wyboru danych do statystyk
	 * oraz wyswietla statystyki
	 *
	 * @param		object	$pSmarty
	 */
	function chooseConditions(&$pSmarty) {
		global $aConfig;
		$aLang =& $aConfig['lang'][$this->sModule];
				
		$aData = array();
		$aConditions = array('start_date', 'end_date');
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			// zczytanie z GET
			foreach ($aConditions as $sCond) {
				$aData[$sCond] = isset($_GET[$sCond]) ? $_GET[$sCond] : '';
			}
		}
//		if (!isset($aData['start_date']) || $aData['start_date'] == '') {
//			$aData['start_date'] = '00-00-0000';
//		}
		//if (!isset($aData['order_status']) || $aData['order_status'] == '') {
	//	$aData['order_status'] = '-1';
		//}
						
		$pForm = new FormTable('stats_conditions', $aLang['header'], array('action'=>phpSelf(array())), array('col_width'=>190), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'show_stats');
		
		// zakres dat od
		$pForm->AddRow($pForm->GetLabelHTML('', $aLang['dates_range']), '<div style="float: left; margin-top: 8px;">'.$pForm->GetLabelHTML('start_date', $aLang['start_date_from'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('start_date', $aLang['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date').'</div><div style="float: left; margin-top: 8px; padding-left: 25px;">'.$pForm->GetLabelHTML('end_date', $aLang['end_date_to'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('end_date', $aLang['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date').'</div>');
									 

		// przyciski		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aLang['button_show']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('action'=>'statistics')).'\');'), 'button'));
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of chooseConditions() function
	
	
	/**
	 * Metoda wyswietla statystyki dla wybranych warunkow
	 *
	 * @param		object	$pSmarty
	 */
	function showStats(&$pSmarty) {
		global $aConfig;
		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';
		$sSourceSql = '';
		$sSourceIt = '';
		$aSource = array();
		
		$aReport = array();
		
		$aReport['lang'] =& $aConfig['lang'][$this->sModule];
		$aReport['date_start'] = formatDateClient(FormatDate($_POST['start_date']));
		$aReport['date_end'] = formatDateClient(FormatDate($_POST['end_date']));
		
		$_GET['hideHeader'] = '1';
		
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_seller_data";
		$aReport['seller_data'] =& Common::GetRow($sSql);
		
		$aOrders =& $this->getOrdersReport('postal_fee');
		if(!empty($aOrders['items'])){
			$aReport['postal_fee'] =& $this->getOrderListHtml($aOrders);
		}
		
		$aOrders =& $this->getOrdersReport('platnoscipl');
		if(!empty($aOrders['items'])){
			$aReport['platnoscipl'] =& $this->getOrderListHtml($aOrders);
		}
		
		$aOrders =& $this->getOrdersReport('bank_transfer');
		/*//testowo
		for($i=0;$i<1000;++$i) {
			$aItem['lp']=123;
			$aItem['invoice_number']='43434/434343/4343';
			$aItem['order_date']=date('d.m.Y', rand(0, 1000000000));
			$aItem['invoice_date']=date('d.m.Y', rand(0, 1000000000));
			$aItem['name']='fsdf fsdfsd';
			$aItem['nip']='';
			$aItem['value_netto']=rand(0, 120);
			$aItem['value_brutto']=rand(0, 120);
			$aItem['order_vat']='';
			$aItem['total_value_brutto']=rand(0, 220);
			$aItem['vat']=array();
			$aOrders['items'][]=$aItem;
			}
		//dump($aOrders);
		//koniec testowo*/
		if(!empty($aOrders['items'])){
			$aReport['bank_transfer'] =& $this->getOrderListHtml($aOrders);
		}
		
		$aOrders =& $this->getOrdersReport('bank_14days');
		if(!empty($aOrders['items'])){
			$aReport['bank_14days'] =& $this->getOrderListHtml($aOrders);
		}
		$pSmarty->assign_by_ref('aRaport', $aReport);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'report_vat.tpl');
		$pSmarty->clear_assign('aRaport', $aReport);
		
		require_once('OLD_tcpdf/config/lang/pl.php');
		require_once('OLD_tcpdf/tcpdf.php');		
		
		// create new PDF document
		$pdf = new TCPDF ('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('profit24');
		$pdf->SetTitle($aLang['title']);
		$pdf->SetSubject($aLang['title']);
		$pdf->SetKeywords('');
		$pdf->setPrintHeader(false);
		$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));
			
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$pdf->SetMargins(8, 5, PDF_MARGIN_RIGHT);
		//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 10);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
		
		//set some language-dependent strings
		$pdf->setLanguageArray($l); 
		
		// set font
		$pdf->SetFont('freesans', '', 8);
		
		$pdf->AddPage();
		$pdf->writeHTML($sHtml, true, false, false, false, '');
		
		// nazwa pliku - uwzgledniajaca daty
		$sFileName = 'raport_vat-'.($_POST['start_date'] == $_POST['end_date'] ? str_replace('-', '', $_POST['start_date']) : str_replace('-', '', $_POST['start_date']).'-'.str_replace('-', '', $_POST['end_date'])).'.pdf';
		$pdf->Output($sFileName, 'D');
		
	} // end of showStats() function
	

	
	/**
	 * 
	 * @param $sPaymentType
	 * @return unknown_type
	 */
	function &getOrdersReport($sPaymentType){
		global $aConfig;
		$sPaymentSql = '';
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay')";
		}
		else {
			$sPaymentSql = "A.payment_type = '".$sPaymentType."'";
		}
		$sSql = "SELECT id, second_invoice
							FROM ".$aConfig['tabls']['prefix']."orders A
							 WHERE ".$sPaymentSql."  AND
							 			 A.invoice_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
							 			 A.invoice_date <= '".formatDate($_POST['end_date'])." 23:59:59'";

		$aOrders =& Common::GetAll($sSql);
		$aTable= array();
		$iLp=1;
		if(!empty($aOrders)){
			$sSql='SELECT value FROM '.$aConfig['tabls']['prefix'].'products_vat_stakes WHERE 1';
			$aVatStakesT=Common::GetAll($sSql);
			$aVatStakes=array();
			//przepisanie do tablicy
			foreach($aVatStakesT as $aVat)
				$aVatStakes[]=$aVat['value'];
				
			foreach($aOrders as $iKey=>$aOrder) {
				$aItem = $this->getOrderVat($aOrder['id'],false,$iLp++);
				$aTable['items'][] = $aItem;
				//sprawdzenie czy są jakieś inne stawki
				foreach($aItem['vat'] as $iOVats=>$aVaVa)
					if(!in_array($iOVats,$aVatStakes))
						$aVatStakes[]=$iOVats;
						
				foreach($aVatStakes as $iKey => $aVatItem) {
					$aTable['vat_'.$aVatItem.'_brutto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_brutto']);
					$aTable['vat_'.$aVatItem.'_netto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_netto']);
					//$aTable['vat_7_brutto'] += Common::formatPrice2($aItem['vat'][7]['value_brutto']);
					//$aTable['vat_22_brutto'] += Common::formatPrice2($aItem['vat'][22]['value_brutto']);
					}
					
				if($aOrder['second_invoice'] == '1') {
					$aItem = $this->getOrderVat($aOrder['id'],true,$iLp++);
					$aTable['items'][] = $aItem;
					foreach($aVatStakes as $iKey => $aVatItem) { 
						$aTable['vat_'.$aVatItem.'_brutto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_brutto']);
						$aTable['vat_'.$aVatItem.'_netto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_netto']);
						//$aTable['vat_7_brutto'] += Common::formatPrice2($aItem['vat'][7]['value_brutto']);
						//$aTable['vat_22_brutto'] += Common::formatPrice2($aItem['vat'][22]['value_brutto']);
					}
				}
			}
		}
		foreach($aVatStakes as $iKey => $aVatItem) {
			// przeliczanie dla stawki VAT 0 - sformatowane z '.' jako separator dziesietny
			$aTable['vat_'.$aVatItem.'_brutto'] = Common::formatPrice2($aTable['vat_'.$aVatItem.'_brutto']);
			$aTable['vat_'.$aVatItem.'_netto'] =  Common::FormatPrice2($aTable['vat_'.$aVatItem.'_brutto']/(1+$aVatItem/100));
			//$aTable['vat_'.$aVatItem.'_netto'] = Common::formatPrice2($aTable['vat_'.$aVatItem.'_brutto'] / (1 + $aVatItem/100));
			$aTable['vat_'.$aVatItem.'_currency'] = $aTable['vat_'.$aVatItem.'_brutto'] - $aTable['vat_'.$aVatItem.'_netto'];
			$aTable['total_brutto'] += $aTable['vat_'.$aVatItem.'_brutto'];
			$aTable['total_netto'] += $aTable['vat_'.$aVatItem.'_netto'];
			$aTable['total_vat'] += $aTable['vat_'.$aVatItem.'_currency'];
			//echo ' '.$iKey.'='.$aTable['vat_'.$aVatItem.'_brutto'];
			/*// przeliczanie dla stawki VAT 7 - sformatowane z '.' jako separator dziesietny
			$aTable['vat_7_brutto'] = Common::formatPrice2($aTable['vat_7_brutto']);
			$aTable['vat_7_netto'] = Common::formatPrice2($aTable['vat_7_brutto'] / (1 + 7/100));
			$aTable['vat_7_currency'] = $aTable['vat_7_brutto'] - $aTable['vat_7_netto'];
			// przeliczanie dla stawki VAT 22 - sformatowane z '.' jako separator dziesietny
			$aTable['vat_22_brutto'] = Common::formatPrice2($aTable['vat_22_brutto']);
			$aTable['vat_22_netto'] = Common::formatPrice2($aTable['vat_22_brutto'] / (1 + 22/100));
			$aTable['vat_22_currency'] = $aTable['vat_22_brutto'] - $aTable['vat_22_netto'];*/
			}
		// obliczanie sumy zamowienia - sformatowanie z ',' jako separator dziesietny
		$aTable['total_brutto'] = Common::formatPrice($aTable['total_brutto']);
		$aTable['total_netto'] = Common::formatPrice($aTable['total_netto']);
		$aTable['total_vat'] = Common::formatPrice($aTable['total_vat']);
		foreach($aVatStakes as $iKey => $aVatItem) {
			// formatowanie wartosci stawki VAT 0 z ',' jako separator dziesietny
			$aTable['vat_'.$aVatItem.'_value'] = Common::formatPrice($aTable['vat_'.$aVatItem.'_netto']);
			$aTable['vat_'.$aVatItem.'_currency'] = Common::formatPrice($aTable['vat_'.$aVatItem.'_currency']);
			$aTable['show_vat_'.intval($aVatItem)]=true;
		}
		/*// formatowanie wartosci stawki VAT 7 z ',' jako separator dziesietny
		$aTable['vat_7_value'] = Common::formatPrice($aTable['vat_7_netto']);
		$aTable['vat_7_currency'] = Common::formatPrice($aTable['vat_7_currency']);
		// formatowanie wartosci stawki VAT 22 z ',' jako separator dziesietny
		$aTable['vat_22_value'] = Common::formatPrice($aTable['vat_22_netto']);
		$aTable['vat_22_currency'] = Common::formatPrice($aTable['vat_22_currency']);*/
		
		return $aTable;
	} // end of getOrdersReport() method 
	
	function getOrderListHtml(&$aOrders){
		global $aConfig,$pSmarty;
		$aModule = array();
		$aModule['lang'] =& $aConfig['lang'][$this->sModule];
		
		$aModule['table'] =& $aOrders;
		
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatePath.'orders_table2.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	} // end of getProductListHtml() method
	

	function &getOrderInvoiceAddress($iId, $bSecondInvoice=false){
		global $aConfig;
		
		$sSql = "SELECT is_company, company, name, surname, street, number, number2, postal, city, nip, phone
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
						 " AND address_type = '".($bSecondInvoice?'2':'0')."'
						  ORDER BY address_type"; 
		return Common::GetRow($sSql);
	} // end of getOrderInvoiceAddress() method
	
	
	function getOrderVat($iId,$bSecondInvoice=false,$iLp){
	global $aConfig,$pSmarty;
	
		$aOrder = array();
		$aOrder['lp'] = $iLp;
		
		$sSql = "SELECT A.*, DATE(order_date) order_date, DATE(invoice_date) invoice_date, DATE(status_3_update) status_3_update, (A.transport_cost + A.value_brutto) as total_value_brutto, B.personal_reception
						 FROM ".$aConfig['tabls']['prefix']."orders A
						LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						ON B.id=A.transport_id
						 WHERE A.id = ".$iId;
		$aModule['order'] =& Common::GetRow($sSql);
		$bEnabledSecondInvoice = ($aModule['order']['second_invoice'] == '1');
	
		$sSql = "SELECT A.*, B.quantity AS parent_quantity
						 FROM ".$aConfig['tabls']['prefix']."orders_items A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."orders_items B
						 ON B.id=A.parent_id
						 WHERE A.order_id = ".$iId."
						 AND A.packet='0'
						 AND A.deleted = '0'".
						($bEnabledSecondInvoice?($bSecondInvoice?" AND A.second_invoice='1'":" AND A.second_invoice='0'"):'');
		$aModule['items'] =& Common::GetAll($sSql);
		
		
		

		if($bSecondInvoice){
			$aOrder['invoice_number'] = $aModule['order']['invoice2_id'];
			$aAddress = $this->getOrderInvoiceAddress($aModule['order']['id'], true);
		} else {
			$aOrder['invoice_number'] = $aModule['order']['invoice_id'];
			$aAddress = $this->getOrderInvoiceAddress($aModule['order']['id']);
		}
		
		$aOrder['order_date'] = formatDateClient($aModule['order']['order_date']);
		$aOrder['invoice_date'] = formatDateClient($aModule['order']['invoice_date']);

		if($aAddress['is_company'] == '1'){
			$aOrder['name'] = $aAddress['company'];
			$aOrder['nip'] = $aAddress['nip'];
		} else {
			$aOrder['name'] = $aAddress['surname'].' '.$aAddress['name'];
			$aOrder['nip'] = '&nbsp;';
		}
		
		
		$aOrder['value_netto'] = 0;
		$aOrder['value_brutto'] = 0;
		$i=1;
		
		$aOrder['vat'][0] = array();
		$aOrder['vat'][7] = array();
		$aOrder['vat'][22] = array();
		foreach ($aModule['items'] as $iKey => $aItem){
			if($aItem['promo_price_brutto'] == 0){
					$aItem['promo_price_brutto'] = $aItem['price_brutto'];
			}

			
			if($aItem['item_type'] == 'P'){
				$aItem['quantity'] = $aItem['parent_quantity'];
			}
			
			$aOrder['vat'][$aItem['vat']]['quantity'] += $aItem['quantity'];
			// produkt ma zalacznik - dorzucamy dodatkowy wiersz i uwzgledniamy ceny zalacznika na fakturze
			if ($aItem['attachments'] == '1') {
				// produkt posiada zalaczniki - pobranie ich
				$sSql = "SELECT *
								 FROM ".$aConfig['tabls']['prefix']."orders_items_attachments
								 WHERE order_id = ".$iId." AND
								 			 order_item_id = ".$aItem['id'];
				$aAttach =& Common::GetRow($sSql);
				// składowa ceny - załącznik jako osobna pozycja na fakturze
				$aAttach['promo_price_brutto'] =  Common::formatPrice2($aAttach['price_brutto'] - $aAttach['price_brutto']*($aItem['discount']/100));
				$aAttach['value_brutto'] =  Common::formatPrice2($aItem['quantity'] * $aAttach['promo_price_brutto']);
				$aAttach['value_netto'] =  Common::formatPrice2($aItem['quantity'] * ($aAttach['promo_price_brutto'] / (1 + $aAttach['vat']/100)));
				$aAttach['vat_currency'] =  Common::formatPrice2($aAttach['value_brutto'] - $aAttach['value_netto']);
				$aAttach['discount'] = $aItem['discount'];
				$aAttach['quantity'] = $aItem['quantity'];
				
				// dodawanie wartości załącznika do sumy vat dla wartości odpowiedniej dla zalacznika
				$aOrder['vat'][$aAttach['vat']]['quantity'] += Common::formatPrice2($aAttach['quantity']);
				$aOrder['vat'][$aAttach['vat']]['vat_currency'] += Common::formatPrice2($aAttach['vat_currency']);
				$aOrder['vat'][$aAttach['vat']]['value_netto'] += Common::formatPrice2($aAttach['value_netto']);
				$aOrder['vat'][$aAttach['vat']]['value_brutto'] += Common::formatPrice2($aAttach['value_brutto']);
				// vat calkowity
				$aOrder['order_vat'] += $aAttach['vat_currency'];
				// suma netto zamowienia
				$aOrder['value_netto'] += $aAttach['value_netto'];
				$aOrder['value_brutto'] += $aAttach['value_brutto'];
				
				// składowa ceny - produkt jako pozycja na fakturze
				$aItem['price_brutto'] = Common::formatPrice2($aItem['price_brutto'] - $aAttach['price_brutto']);
				$aItem['promo_price_brutto'] = Common::formatPrice2($aItem['promo_price_brutto'] - $aAttach['promo_price_brutto']);
				$aItem['value_brutto'] =  Common::formatPrice2($aItem['quantity'] * $aItem['promo_price_brutto']);
				$aItem['value_netto'] = Common::formatPrice2($aItem['quantity'] * ($aItem['promo_price_brutto'] / (1 + $aItem['vat']/100)));
				$aItem['vat_currency'] =  Common::formatPrice2($aItem['value_brutto'] - $aItem['value_netto']);
				// dodawanie produktu do sumy vat dla wartości odpowiedniej
				$aOrder['vat'][$aItem['vat']]['vat_currency'] += Common::formatPrice2($aItem['vat_currency']);
				$aOrder['vat'][$aItem['vat']]['value_netto'] += Common::formatPrice2($aItem['value_netto']);
				$aOrder['vat'][$aItem['vat']]['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
				// vat calkowity
				$aOrder['order_vat'] += Common::formatPrice2($aItem['vat_currency']);
				// suma netto zamowienia
				$aOrder['value_netto'] += Common::formatPrice2($aItem['value_netto']);
				$aOrder['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
			} else {
				// produkt zwykly
				$aItem['vat_currency'] =  Common::formatPrice2($aItem['value_brutto'] - $aItem['value_netto']);
				// dodawanie produktu do sumy vat dla wartości odpowiedniej
				$aOrder['vat'][$aItem['vat']]['vat_currency'] += Common::formatPrice2($aItem['vat_currency']);
				$aOrder['vat'][$aItem['vat']]['value_netto'] += Common::formatPrice2($aItem['value_netto']);
				$aOrder['vat'][$aItem['vat']]['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
				// vat calkowity
				$aOrder['order_vat'] += Common::formatPrice2($aItem['vat_currency']);
				// suma netto zamowienia
				$aOrder['value_netto'] += Common::formatPrice2($aItem['value_netto']);
				$aOrder['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
			}
			
		}
		// dodanie kuriera do pozycji zamówienia
		if($aModule['order']['personal_reception'] == '0' && !$bSecondInvoice){
			$aItem = array();
			$aItem['discount'] = 0;
			$aItem['quantity'] = 1;
			$aItem['price_brutto'] = $aModule['order']['transport_cost'];
			$aItem['promo_price_brutto'] = $aItem['price_brutto'];
			$aItem['value_brutto'] = $aItem['price_brutto'];
			$aItem['vat'] = $aModule['order']['transport_vat'];//$this->getPaymentVat($aModule['order']['payment_id']);
			$aItem['value_netto'] =  Common::formatPrice2($aItem['price_brutto'] / (1 + $aItem['vat']/100));
			$aItem['vat_currency'] =  Common::formatPrice2($aItem['value_brutto'] - $aItem['value_netto']);
			// dodawanie transportu do sumy vat dla wartości odpowiedniej
			$aOrder['vat'][$aItem['vat']]['vat_currency'] += Common::formatPrice2($aItem['vat_currency']);
			$aOrder['vat'][$aItem['vat']]['value_netto'] += Common::formatPrice2($aItem['value_netto']);
			$aOrder['vat'][$aItem['vat']]['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
			// vat calkowity
			$aOrder['order_vat'] += Common::formatPrice2($aItem['vat_currency']);
			// dodanie kosztu transportu do calk wartosci zamowienia
			// suma netto zamowienia
			$aOrder['value_netto'] += Common::formatPrice2($aItem['value_netto']);
			$aOrder['value_brutto'] += Common::formatPrice2($aItem['value_brutto']);
			
		}
		
		$aOrder['value_netto'] = Common::formatPrice($aOrder['value_netto']);
		$aOrder['value_brutto'] = Common::formatPrice($aOrder['value_brutto']);
		$aOrder['order_vat'] = Common::formatPrice($aOrder['order_vat']);
		$aOrder['total_value_brutto'] = Common::formatPrice($aOrder['total_value_brutto']);
		foreach($aOrder['vat'] as $iVat=>$aVat){
			$aOrder['vat'][$iVat]['vat_currency'] = Common::formatPrice($aOrder['vat'][$iVat]['vat_currency']);
			$aOrder['vat'][$iVat]['value_netto'] = Common::formatPrice($aOrder['vat'][$iVat]['value_netto']);
			$aOrder['vat'][$iVat]['value_brutto'] = Common::formatPrice($aOrder['vat'][$iVat]['value_brutto']);
		}
		
		return $aOrder;
	} // end of getInvoiceHtml() method
	
		/**
	 * Pobiera wysokosc stawki VAT dla metody płatności/transportu 
	 * @param int $iId - id zamówienia
	 * @return float - stawka vat
	 */
	function getPaymentVat($iId){
		global $aConfig;
		$sSql = "SELECT vat
						FROM ".$aConfig['tabls']['prefix']."orders_payment_types
						WHERE id = ".$iId;
		return intval(Common::GetOne($sSql));
	}
} // end of Module Class
?>