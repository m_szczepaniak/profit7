<?php
/**
 * Klasa wysyłania maila zamówienia
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__zamowienia__mail {

  // komunikat
	public $sMsg;

	// nazwa modulu - do langow
	public $sModule;

	// id wersji jezykowej
	public $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	private $aPrivileges;
	
	// id strony modulu
	private $iPageId;
  
  // id uzytkownika
  private $iUId;
  
	// id listy zamowien
	private $iPPId;
  
  // id zamowienia
  private $iOId;
  
  public $sMailsPath;
  
  function __construct(&$pSmarty, $bInit = true) {
    global $aConfig;

    
		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];
    $this->sMailsPath = '/omniaCMS/modules/'.$this->sModule.'/orders_emails/';
    
    if ($bInit == false) {
      return;
    }
    
    if (isset($_GET['id'])) {
			$iTId = $_GET['id'];
		}
    
		if (isset($_GET['pid'])) {
			$iOId = $_GET['pid'];
		}
    
		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
    
		$this->iUId = $_SESSION['user']['id'];
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    switch ($sDo) {
      case 'delete': 
        $this->Delete($pSmarty, $iTId); 
      break;
    
      case 'add_template':
        $this->addTemplate($pSmarty);
      break;
    
      default:
        $this->ShowTemplates($pSmarty);
      break;
    
      case 'add':
      case 'change_option':
        $this->AddSendMail($pSmarty, $iOId);
      break;
    
      case 'insert_send':
        $this->InsertSend($pSmarty, $iOId);
      break;
    
      case 'insert_template':
        $this->InsertTemplate($pSmarty);
      break;
    }
  }// end of __construct()
  
  
  /**
   * Metoda wyświetla formularz dodawania nowego alertu
   * 
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iOId
   */
  public function AddSendMail(&$pSmarty, $iOId) {
    global $aConfig;

    $aData = array();
    $aLang = $aConfig['lang'][$this->sModule];
    // dolaczenie klasy FormTable
    include_once('Form/FormTable.class.php');

    if (!empty($_POST)) {
      $aData = $_POST;
    }

    if (isset($aData['templates']) && $aData['templates'] > 0) {
      $aAddData = $this->_getTemplateData(intval($aData['templates']), array('title', 'content', 'mail_name'));
      if ($aAddData['title'] != '') {
        $aData['title'] = $aAddData['title'];
      }
      if ($aAddData['content'] != '') {
        $aData['content'] = $aAddData['content'];
      }
    }

    $aOrderData = $this->_GetOrderData($iOId, array('order_number', 'email'));
    $sHeader = sprintf($aLang['header'], $aOrderData['email'], $aOrderData['order_number']);

    $pForm = new FormTable('mails', $sHeader, array('action' => phpSelf(array('pid' => $iOId)), 'enctype' => 'multipart/form-data'), array('col_width' => 155), $aConfig['common']['js_validation']);
    $pForm->AddHidden('do', 'insert_send');

    $pForm->addRow(
            $aLang['templates'], 
            $pForm->GetSelectHTML('templates', $aLang['templates'], 
                                  array('onchange' => 'document.getElementById(\'do\').value =\'change_option\'; this.form.submit();'), 
                                  addDefaultValue($this->_getTemplates()), (isset($aData['templates']) ? $aData['templates'] : ''), '', false)
            . ' <a style="font-size: 12px" href="' . phpSelf(array('do' => '')) . '"> Zarządzaj szablonami emaili &raquo; </a>'
    );
    
    

    $pForm->AddMergedRow('TREŚĆ MAILA', array('class' => 'merged'));
    
    $pForm->AddText('title', $aLang['mail_title'], (isset($aData['title']) ? $aData['title'] : ''), array('style' => 'width: 320px;'), '', 'text', true);


    $pForm->AddTextArea('content', $aLang['mail_content'], (isset($aData['content']) ? $aData['content'] : ''), array('rows' => '20'), '');

    $pForm->AddFile('attachment', $aLang['attachment'], '', '', false);
    // przyciski
    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_send']) . '&nbsp;&nbsp;' .
            $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'details', 'id' => $iOId), array('action')) . '\');'), 'button'));


    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
  }// end of AddSendMail() method
  
  
  /**
   * Metoda pobiera szablony maili
   * 
   * @return array
   */
  private function _getTemplates() {
    
    $sSql = "SELECT id AS value, mail_name AS label 
             FROM orders_mails_templates
             ORDER BY id DESC";
    return Common::GetAll($sSql);
  }// end of _getTemplates() method
  
  
  /**
   * Metoda pobiera kolumny z zamówienia
   * 
   * @param int $iOId
   * @return array
   */
  private function _GetOrderData($iOId, $aCols) {
    
    $sSql = "SELECT ".implode(',', $aCols)."
             FROM orders
             WHERE id = ".$iOId;
    return Common::GetRow($sSql);
  }// end of _GetOrderNumber() method
  
  
  /**
   * Metoda wyświetla formularz dodawania szablonu
   * 
   * @global array $aConfig
   * @param object $pSmarty
   * @param int $iTId
   */
  private function addTemplate($pSmarty) {
   global $aConfig;
    
		$aData = array();
		$aLang = $aConfig['lang'][$this->sModule];
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
    
    if (!empty($_POST)) {
      $aData = $_POST;
    }
    $sHeader = sprintf($aLang['header_template']);
		
		$pForm = new FormTable('mails', $sHeader, array('action'=>phpSelf()), array('col_width'=>155), $aConfig['common']['js_validation']);
    $pForm->AddHidden('do', 'insert_template');
    
    $pForm->AddText('mail_name', $aLang['mail_name'], (isset($aData['mail_name']) ? $aData['mail_name'] : ''), array('style' => 'width: 320px;'), '', 'text', true);
    
    $pForm->AddMergedRow('TREŚĆ MAILA', array('class' => 'merged'));
    $pForm->AddText('title', $aLang['mail_title'], (isset($aData['title']) ? $aData['title'] : ''), array('style' => 'width: 320px;'), '', 'text', true);
    
    
    $pForm->AddTextArea('content', $aLang['mail_content'], (isset($aData['content']) ? $aData['content'] : ''), array('rows' => '20'), '');
    
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_send']).'&nbsp;&nbsp;'.
            $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array()).'\');'), 'button'));
		
    
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
  }// end of addTemplate() method
  
  
  /**
   * Metoda dodaje nowy szablon
   * 
   * @global array $aConfig
   * @param object $pSmarty
   * @param int $iOId
   * @return void
   */
  public function InsertSend($pSmarty, $iOId) {
    global $aConfig;
    $aAttachments = array();
    
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->AddSendMail($aConfig, $iOId);
			return;
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddSendMail($aConfig, $iOId);
			return;
		}
    
    
    if (!empty($_FILES) && !empty($_FILES['attachment']) && $_FILES['attachment']['tmp_name'] != '') {
      // przesłany zostal załącznik
      $sAttachmentFile = $_SERVER['DOCUMENT_ROOT'].$this->sMailsPath.$_FILES['attachment']['name'];
      if (copy($_FILES['attachment']['tmp_name'], $sAttachmentFile) === FALSE) {
        echo 'Błąd przesyłania załącznika !';
        exit(0);
      }
      $aAttachments[] = $sAttachmentFile;
    }
    
    $aOrderData = $this->_GetOrderData($iOId, array('website_id', 'email', 'order_number'));
    $sWebsiteSymbol = $this->getWebsiteSymbol($aOrderData['website_id']);
    $aWebsiteSettings = $this->getWebisteSettings($sWebsiteSymbol);
    
		$sFrom = $aWebsiteSettings['name'];
		$sFromEmail = $aWebsiteSettings['orders_email'];
    $sFooter = $aWebsiteSettings['mail_footer_order'];
            
		$sSql = "SELECT CONCAT(name)
							FROM ".$aConfig['tabls']['prefix']."users
							 WHERE id = ".intval($_SESSION['user']['id']);
		$sSenderNameSurname = Common::GetOne($sSql);
    
    
    $sContent = $_POST['content']."\n\n--\n".str_ireplace('{imie_nazwisko}', $sSenderNameSurname, $sFooter);
    
    // wyślijmy maila
    $sTitleTmp = '=?UTF-8?B?'.base64_encode($_POST['title']).'?=';
    if (Common::sendTxtHtmlMail($sFrom, $sFromEmail, $aOrderData['email'], $sTitleTmp, $sContent, '', '', $sAttachmentFile) !== FALSE) {
      
      $sSql = "SELECT name, surname
                FROM ".$aConfig['tabls']['prefix']."users
                 WHERE id = ".intval($_SESSION['user']['id']);

      $aGeneratorName=Common::GetRow($sSql);
		
      // jeśli oki dodajmy do bazy
      $aValues = array(
          'order_id' => $iOId,
          'title' => $_POST['title'],
          'content' => $_POST['content'],
          'attachments' => serialize($aAttachments),
          'created' => 'NOW()',
          'created_by' => $aGeneratorName['name'].' '.$aGeneratorName['surname']
      );
      $mRet = Common::Insert('orders_mails', $aValues);
    } else{
      $mRet = -1;
    }
    
    if ($mRet > 0) {
      // ok
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_send_ok'],
                      $_POST['title'],
                      $aOrderData['order_number'],
                      $aOrderData['email']
              );
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
      setSessionMessage($sMsg);
      doRedirect($aConfig['common']['base_url_https'].'/'.phpSelf(array('module' => 'm_zamowienia', 'do' => 'details', 'id' => $_GET['pid']), array('pid', 'action')));
    } else {
      // err
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_send_err'],
                      $_POST['title'],
                      $aOrderData['order_number'],
                      $aOrderData['email']
              );
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->addTemplate($pSmarty);
    }
  }// end of InsertTemplaten() method
  
  
  /**
   * Metoda pobiera ustawienia serwisu
   * 
   * @param string $sWebsiteSymbol
   * @global object $pDbMgr
   * @return array
   */
  private function getWebisteSettings($sWebsiteSymbol) {
    global $pDbMgr;
    
		$sSql = "SELECT name,
                    email, 
                    users_email, 
                    orders_email,
                    mail_footer_order, 
                    mail_footer
						 FROM website_settings";
    return $pDbMgr->GetRow($sWebsiteSymbol, $sSql);
  }// end of getWebisteSettings() method
  
  
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type 
	 */
	function getWebsiteSymbol($iWebsiteId) {
		global $aConfig;
		
		$sSql = "SELECT code FROM ".$aConfig['tabls']['prefix']."websites
						 WHERE id=".$iWebsiteId;
		return Common::GetOne($sSql);
	}// end of getWebsiteSymbol() method
  
  
  /**
   * Metoda dodaje nowy szablon
   * 
   * @global array $aConfig
   * @param object $pSmarty
   * @param int $iOId
   * @return void
   */
  public function InsertTemplate($pSmarty) {
    global $aConfig;
    
    
    $aValues = array(
        'mail_name' => $_POST['mail_name'],
        'title' => $_POST['title'],
        'content' => $_POST['content'],
    );
    $mRet = Common::Insert('orders_mails_templates', $aValues);
    if ($mRet > 0) {
      // ok
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
                      $_POST['title']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
      $this->ShowTemplates($pSmarty);
    } else {
      // err
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
                      $_POST['title']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->addTemplate($pSmarty);
    }
  }// end of InsertTemplaten() method
  
  
  /**
   * Metoda wyświetla listę szablonów
   * 
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iTId
   */
  public function ShowTemplates($pSmarty) {
    global $aConfig;
    
    include_once('View/View.class.php');
    // zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true,
		);
    $aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
    
    $aRecordsHeader = array(
      array(
        'content'	=> '&nbsp;',
        'sortable'	=> false
      ),
      array(
        'db_field'	=> 'mail_name',
        'content'	=> $aConfig['lang'][$this->sModule]['list_mail_name'],
        'sortable'	=> true,
        'width'	=> '165'
      ),
      array(
        'db_field'	=> 'title',
        'content'	=> $aConfig['lang'][$this->sModule]['list_title'],
        'sortable'	=> true,
        'width'	=> '165'
      ),
      array(
        'db_field'	=> 'content',
        'content'	=> $aConfig['lang'][$this->sModule]['list_content'],
        'sortable'	=> true,
      ),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '25'
			)
    );
        
    // pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."orders_mails_templates
						 WHERE 1=1 ".
						  (isset($_POST['search']) && !empty($_POST['search']) ? " AND title = '%".$_POST['search']."%' " : '');
		$iRowCount = intval(Common::GetOne($sSql));

		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."orders_mails_templates";
			$iRowCount = intval(Common::GetOne($sSql));
		}
    
    $pView = new View('orders_mails_templates', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
    
    if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
      
      // pobranie wszystkich rekordow
			$sSql = "SELECT id, mail_name, title, content
						 	 FROM ".$aConfig['tabls']['prefix']."orders_mails_templates
						 	 WHERE 1=1 "
                .(isset($_POST['search']) && !empty($_POST['search']) ? " AND title = '%".$_POST['search']."%' " : '')
							  .'  ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' id DESC')
							  .(isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '')
							  ." LIMIT ".$iStartFrom.", ".$iPerPage." ";
			$aRecords =& Common::GetAll($sSql);
      foreach ($aRecords as $iKey => $aRecord) {
        $aRecords[$iKey]['content'] = trimString($aRecord['content'], 255);
      }
      
      
      // ustawienia dla poszczegolnych kolumn rekordu
      $aColSettings = array(
        'id'	=> array(
          'show'	=> false
        ),
        'action' => array (
          'actions'	=> array ('delete'),
          'params' => array (
                        'delete'	=> array('id'=>'{id}')
                      )
        )
      );

      // dodanie rekordow do widoku
      $pView->AddRecords($aRecords, $aColSettings);
    }
    
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('add')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(0 => array('do' => 'add'), 1 => array('id')),
      'add' => array(0 => array('do' => 'add_template'))
		);

		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show());
  }// end of ShowTemplates() method
  
  
	/**
	 * Metoda usuwa wybrane rekordy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego rekordu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
    if (!empty($_POST['delete'])) {
      foreach($_POST['delete'] as $sKey => $sVal) {
        $aTmp[$iI++] = $sKey;
      }
    }
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->_getTemplateData($_POST['delete'], array('title', 'id'));
      
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['title'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['title'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->ShowTemplates($pSmarty);
	} // end of Delete() funciton
  
  
	/**
	 * Metoda usuwa kod promocyjny
	 * 
	 * @param	integer	$iId	- Id kodu promocyjnego do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;

		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."orders_mails_templates
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method
	
  
	/**
	 * Metoda pobiera dane rekordow ktore maja zostac usuniete
	 * 
   * @param mixed array|int $mOId - id rekordu/lub tablica rekordów
	 * @param	array           $aIDs	- Id rekordow do usuniecia
	 * @return	array
	 */
	public function getMailsData($mOId, $aCols) {
		global $aConfig;
		
    if (is_array($mOId)) {
      $sSql = "SELECT ".implode(',', $aCols)."
               FROM ".$aConfig['tabls']['prefix']."orders_mails
               WHERE order_id IN (".implode(",", $mOId).")";
      return Common::GetAll($sSql);
      
    } elseif (intval($mOId) > 0) {
      $sSql = "SELECT ".implode(',', $aCols)."
               FROM ".$aConfig['tabls']['prefix']."orders_mails
               WHERE order_id = ".$mOId;
      return Common::GetAll($sSql);
    }
	} // end of _getMailsData() method
  
  
	/**
	 * Metoda pobiera dane rekordow ktore maja zostac pobrane
	 * 
   * @param mixed array|int $mID - id rekordu/lub tablica rekordów
	 * @param	array           $aCols	- Id rekordow do usuniecia
	 * @return	array
	 */
	private function _getTemplateData($mID, $aCols) {
		global $aConfig;
		
    if (is_array($mID)) {
      $sSql = "SELECT ".implode(',', $aCols)."
               FROM ".$aConfig['tabls']['prefix']."orders_mails_templates
               WHERE id IN (".implode(",", $mID).")";
      return Common::GetAll($sSql);
      
    } elseif (intval($mID) > 0) {
      $sSql = "SELECT ".implode(',', $aCols)."
               FROM ".$aConfig['tabls']['prefix']."orders_mails_templates
               WHERE id = ".$mID;
      return Common::GetRow($sSql);
    }
	} // end of _getTemplateData() method
}// end of class

?>