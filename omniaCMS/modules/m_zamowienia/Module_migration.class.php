<?php
/**
 * Moduł przeprowadzania migracji między starym systemem a nowym
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-03 
 * @copyrights Marcin Chudy - Profit24.pl
 */
include_once($_SERVER['DOCUMENT_ROOT'].'LIB/orders/OrderMagazineData.class.php');
class Module__zamowienia__migration extends Orders\OrderMagazineData  {

  // komunikat
	public $sMsg;

	// nazwa modulu - do langow
	public $sModule;

	// id wersji jezykowej
	public $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	private $aPrivileges;
	
  // id uzytkownika
  private $iUId;
  
  // id zamowienia
  private $iId;
  
  private $aLetters;
  /*
  private $aLetters = array(
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
        'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 
        'V', 'W', 'X', 'Y', 'Z', 'Pozostałe'
  );
  */
  
  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  private $pDbMgr;
  
  function __construct(&$pSmarty, $bInit = true) {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
    
		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];

		if (isset($_GET['pid'])) {
			$iId = $_GET['pid'];
		}
    
		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
    
		$this->iUId = $_SESSION['user']['id'];
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    if ($bInit == false) {
      return;
    }
    switch ($sDo) {
      case 'details':
        $this->getListLetter($pSmarty);
      break;
    
      case 'create_list':
        $this->createList($pSmarty);
      break;
      
      case 'do':
      default:
        $this->Show($pSmarty);
      break;
    }
  }
  
  /**
   * Metoda tworzy listę
   * 
   * @param type $pSmarty
   */
  private function createList(&$pSmarty) {

    try {
      $sListNumber = $this->getNewListNumber(1);
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
    
    
    // dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
		$aValues = array(
			'content' => '',
			'saved_type' => '0',
			'status' => '1',
			'source' => '33',
			'date_send' => 'NOW()',
			'send_by' => $_SESSION['user']['name'],
      'pack_number' => $_POST['pack_number'],
			'number' => $sListNumber,
      'magazine_status' => '5'
		);
		$iExportId=false;
		if ( ($iExportId=Common::Insert("orders_send_history", $aValues, '', true)) === false) {
			$bIsErr = true;
		}
		else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
			
			foreach ($_POST['delete'] as $iItem => $aTMP) {
				$aValues = array(
            'send_history_id'=>$iExportId,
            'item_id'=>$iItem
				);
				if ( Common::Insert("orders_send_history_items", $aValues, '', false) === false) {
					// błąd
					$bIsErr = true;
				}
			}
		}
		if ($bIsErr === true) {
			// błąd
      $sMsg = _('Wystąpił błąd podczas przenoszenia listy na tramwaj !');
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();
		} else {
			// wszystko ok
      $sMsg = _('Zapisano nową listę przesunięcia towaru na tramwaj o numerze: '.$sListNumber);
      AddLog($sMsg, false);
      $this->sMsg = GetMessage($sMsg, false);
			Common::CommitTransaction();
		}
    $this->Show($pSmarty);
  }
  
  
  /**
   * Metoda pobiera listę produktów rozpoczynających się na daną literę
   * 
   * @global array $aConfig
   * @param object $pSmarty
   */
  private function getListLetter(&$pSmarty) {
    global $aConfig;
    
    $aLang =& $aConfig['lang'][$this->sModule];
    $sLetter = $_GET['letter'];
    
    if (!$_POST) {
      $aData = $_POST;
    }
    
    $aBooks = $this->getBooksNew($sLetter);
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
    
		$pForm = new FormTable('ordered_itm', sprintf($aLang['header'], $sLetter), array('action'=>phpSelf()), array('col_width'=>155), false);
		$pForm->AddHidden('do', 'create_list');
		// dane wiadomości
    
    //$pForm->AddTextArea('not_exists', $aLang['not_exists'], '', array('rows' => '20', 'style' => 'width: 230px;'), '', FALSE);
    
    $pForm->AddText('pack_number', _('Numer półki'), $aData['pack_number'], array('class' => 'editable_input'), '', 'uint', true);
    
		// dane do faktury
		$pForm->AddMergedRow($aLang['details_content_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		
		// wyswietlenie listy pasujacych uzytkownikow
		$pForm->AddMergedRow( $this->GetBooksList($aBooks) );
		if(!empty($aBooks)){
      $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send2', $aConfig['lang'][$this->sModule]['check'], array('style' => 'font-size: 40px; font-weight: bold; color: red;')));
    }
    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', _('Anuluj'), array('style'=>'font-size: 40px; float:right;','onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
    
    $sInput = '
    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" /></div>
    <script type="text/javascript" src="js/migrationSelection.js"></script>
    <script type="text/javascript">
    $( function() {
      $("#pack_number").focus();
    });
    </script>
    ';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sInput.ShowTable($pForm->ShowForm()));
  }// end of getListLetter() method
  
  
	/**
	 * Metoda wyswietla liste ksiazek z tablicy $aBooks
	 *
	 * @param		object	$pSmarty
	 * @param	array ref	$aBooks	- lista produktów
	 * @return	void
	 */
	function GetBooksList(&$aBooks) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/EditableView.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'checkboxes'	=> true,
			'editable' => true,
			'count_checkboxes' => true,
			'count_checkboxes_by' => 'quantity',
			'form'	=>	false,
      'count_header' => true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
      array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '20'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_cover']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_name']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_quantity'],
        'style' => 'font-size: 20px!important; font-weight: bold;'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_curr_quantity'],
        'style' => 'font-size: 20px!important; font-weight: bold;'
			)
    );

		$pView = new EditableView('ordered_itm', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array(
			'id' => array(
				'show'	=> false
			),
			'currquantity'=> array(
					'editable'	=> true
			),
		);
			// ustawienia dla kolumn edytowalnych
		$aEditableSettings = array(
			'currquantity' => array(
				'type' => 'text'
			)
		);
    foreach ($aBooks as $iKey => $aBook) {
      $aBooks[$iKey]['currquantity'] = '0';
    }
		// dodanie rekordow do widoku
		$pView->AddRecords($aBooks, $aColSettings,$aEditableSettings);
		// dodanie stopki do widoku
		$aRecordsFooter = array(
				array('check_all')
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		// przepisanie langa - info o braku rekordow
		$aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_books_to_show'];
				// walidacja js kolumn edytowalnych
		$sAdditionalScript='';
		foreach($aBooks as $iKey=>$aItem) {
			if($aItem['weight']=='0,00')	
				$sAdditionalScript.='document.getElementById(\'delete['.$aItem['id'].']\').disabled=true;
				';		
			}		
		$sJS = '
		<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
		'.$sAdditionalScript.'
			$("#ordered_itm").submit(function() {
        if ($("input[name^=\'delete\']:not(:checked)").length === 0) {
          return true;
        } else {
          return true;
//          alert("Błąd zatwierdzania zamówienia, nie zatwierdzono wszystkich pozycji !!!");
        }
        return false;
			});
		});
	// ]]>
</script>
		';
		return $sJS.$pView->Show();
	} // end of GetBooksList() function
  
  
  /**
   * Metoda pobiera liste ksiazek dla wygenerowanego widoku
   * 
   * @param int $sLetter litera książki
   * @return array - lista ksiazek
   */
	function &getBooksNew($sLetter) {
		global $aConfig;
    
    $sCols = ' DISTINCT OI.id, "0" AS photo, OI.order_id,
                       OI.isbn, OI.name, OI.quantity, OI.publisher, OI.authors, OI.publication_year, OI.edition,
                       
      ( 
        SELECT CONCAT(PI.directory, "\/__t_", PI.photo)
        FROM products_images AS PI
        WHERE PI.product_id = OI.product_id
        LIMIT 1
        ) as photo_dir
';
    
    $sSql = $this->getSQLBooks($sCols);
    $sSql .= ' AND LEFT(OI.name, 1) = "'.$sLetter.'"
      GROUP BY OI.id
        ORDER BY SUM(OI.quantity) DESC
        
      ';
    $aBooks = Common::GetAll($sSql);

    dump($sSql);
    foreach($aBooks as $iKey=>$aItem){
      $aBooks[$iKey]['name'] = $aItem['name'].
                    ($aItem['isbn']?'<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="'.$aItem['isbn'].'" item_id="'.$aItem['id'].'">'.$aItem['isbn'].'</span>':'').
                    ($aItem['publication_year']?'<br />'.$aItem['publication_year']:'').($aItem['edition']?' wyd. '.$aItem['edition']:'').
                    ($aItem['publisher']?'<br />'.$aItem['publisher']:'').
                    ($aItem['authors']?'<br />'.$aItem['authors']:'').'</span>';
      unset($aBooks[$iKey]['isbn']);
      unset($aBooks[$iKey]['publication_year']);
      unset($aBooks[$iKey]['edition']);
      unset($aBooks[$iKey]['publisher']);
      unset($aBooks[$iKey]['authors']);
      

      if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aItem['photo_dir'])){
        $aBooks[$iKey]['photo'] = '<img src="/images/photos/'.$aItem['photo_dir'].'" />';	
      } else {
        $aBooks[$iKey]['photo'] = 'BRAK OKŁADKI';
      }
      unset($aBooks[$iKey]['directory']);
      unset($aBooks[$iKey]['photo_dir']);
      unset($aBooks[$iKey]['order_id']);
    }
		return $aBooks;
	} // end of getBooksNew() method
  
  
  /**
   * Wyświetlamy listę liter
   * 
   * @param object $pSmarty
   */
  private function Show(&$pSmarty) {
    
    // wyświetlamy listę liter
    // 
    // 
		// dolaczenie klasy View
		include_once('View/View.class.php');
    
		// zapamietanie opcji
		rememberViewState($this->sModule);
    
		$aHeader = array(
			'header'	=> _('Produkty do przeniesienia na tramwaj'),
			'refresh'	=> true,
			'search'	=> true,
			'checkboxes'	=> false
		);
    $aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
    $aRecordsHeader = array(
			array(
				'db_field'	=> 'letter',
				'content'	=> _('Pierwsza litera tytułu '),
				'sortable'	=> true,
				'width' => '100'
			),
			array(
				'content'	=> _('Akcja'),
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
    $pView = new View('send_history', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
    $aRecords = array();

    $this->aLetters = $this->getArrFirstLetterBooks();
    foreach ($this->aLetters as $iKey => $sLetter) {
      $aRecords[$iKey]['number'] = $iKey;
      $aRecords[$iKey]['letter'] = $sLetter;
    }
    $aColSettings = array(
      'number'	=> array(
        'show'	=> false
      ),
      'action' => array (
        'actions'	=> array ('details'),
        'params' => array (
          'details'	=> array('letter' => '{letter}')
        ),
      )
    );
    $pView->AddRecords($aRecords, $aColSettings);
		$aRecordsFooter = array(
			array()
		);
		$pView->AddRecordsFooter($aRecordsFooter);
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
  }
  
  
  /**
   * Metoda pobiera tablicę liter lierwszych
   * 
   * @return array
   */
  private function getArrFirstLetterBooks () {
    
    $sSql = $this->getSQLBooks(' LEFT(OI.name, 1) AS letter ');
    
    $sSql .= ' 
             GROUP BY LEFT(OI.name, 1) 
             ORDER BY OI.name ASC ';
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }// end of getArrFirstLetterBooks() method
  
  
  /**
   * Metoda pobiera sql pobierania książek
   * 
   * @param string $sCols
   * @return string
   */
  private function getSQLBooks ($sCols) {
     $sSql = "
      SELECT
        ".$sCols."
      FROM orders_items AS OI
      JOIN  `orders` AS O ON OI.order_id = O.id
      WHERE
        (
          SELECT OSH.id 
          FROM orders_send_history_items AS OSHI
          LEFT JOIN orders_send_history AS OSH
            ON OSH.id = OSHI.send_history_id
          WHERE
            OSHI.item_id = OI.id AND OSH.source = '33'
          LIMIT 1
        ) IS NULL
        AND O.order_status <> '3'
        AND O.order_status <> '4'
        AND O.order_status <> '5'
        AND OI.status = '4'
        AND OI.item_type <> 'A'
        AND OI.deleted <> '1'
        AND OI.get_ready_list = '0'
      ";        
     return $sSql;
  }// end of getSQLBooks() method
}