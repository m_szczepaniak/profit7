<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - statystyki zamowien
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Module {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// ID strony modulu zamowien
	var $iPageId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;
		
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_zamowienia', $_GET['action']);
		$this->sModule = $_GET['module'];
		
		$sDo = '';
		$iId = 0;
		
		
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
		
		// wyswietlenie
		switch ($sDo) {
			case 'show_stats':
				$this->showStats($pSmarty);
			break;
			
			default:
				$this->chooseConditions($pSmarty);
			break;
		}
	} // end of Module() method


	/**
	 * Metoda tworzy formularz wyboru danych do statystyk
	 * oraz wyswietla statystyki
	 *
	 * @param		object	$pSmarty
	 */
	function chooseConditions(&$pSmarty) {
		global $aConfig;
		$aLang =& $aConfig['lang'][$this->sModule];
				
		$aData = array();
		$aConditions = array('start_date', 'end_date', 'order_status', 'payment', 'transport');
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			// zczytanie z GET
			foreach ($aConditions as $sCond) {
				$aData[$sCond] = isset($_GET[$sCond]) ? $_GET[$sCond] : '';
			}
		}
		if (!isset($aData['start_date']) || $aData['start_date'] == '') {
			$aData['start_date'] = '00-00-0000';
		}
		if (!isset($aData['order_status']) || $aData['order_status'] == '') {
			$aData['order_status'] = '-1';
		}
		if (!isset($aData['payment']) || $aData['payment'] == '') {
			$aData['payment'] = 'payment_all';
		}
		if (!isset($aData['transport']) || $aData['transport'] == '') {
			$aData['transport'] = '-1';
		}

		$pForm = new FormTable('stats_conditions', $aLang['header'], array('action'=>phpSelf(array())), array('col_width'=>190), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'show_stats');
		
		// zakres dat od
		$pForm->AddRow($pForm->GetLabelHTML('', $aLang['dates_range']), '<div style="float: left; margin-top: 8px;">'.$pForm->GetLabelHTML('start_date', $aLang['start_date_from'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('start_date', $aLang['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date').'</div><div style="float: left; margin-top: 8px; padding-left: 25px;">'.$pForm->GetLabelHTML('end_date', $aLang['end_date_to'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('end_date', $aLang['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date').'</div>');
									 
		// zamowienia ze statusem
		$aOptions = array(
			array('value' => '-1', 'label' => $aLang['status_all']),
			array('value' => '0', 'label' => $aConfig['lang'][$_GET['module']]['status_0']),
			array('value' => '1', 'label' => $aConfig['lang'][$_GET['module']]['status_1']),
			array('value' => '2', 'label' => $aConfig['lang'][$_GET['module']]['status_2']),
			array('value' => '3', 'label' => $aConfig['lang'][$_GET['module']]['status_3']),
			array('value' => '4', 'label' => $aConfig['lang'][$_GET['module']]['status_4']),
			array('value' => '5', 'label' => $aConfig['lang'][$_GET['module']]['status_5'])
			
		);
		$pForm->AddRow($pForm->GetLabelHTML('order_status', $aLang['order_status']), $pForm->GetRadioSetHTML('order_status', $aLang['order_status'], $aOptions, $aData['order_status'], '', true, false), '', array(), array('style' => 'padding-top: 10px;'));
		
		// metoda platosci
		$aPayments = array(
			array('value' => 'payment_all', 'label' => $aLang['payment_all']),
			array('value' => 'postal_fee', 'label' => $aConfig['lang'][$_GET['module'].'_payment_types']['ptype_postal_fee']),
			array('value' => 'bank_transfer', 'label' => $aConfig['lang'][$_GET['module'].'_payment_types']['ptype_bank_transfer']),
			array('value' => 'dotpay', 'label' => $aConfig['lang'][$_GET['module'].'_payment_types']['ptype_dotpay']),
			array('value' => 'vat', 'label' => $aConfig['lang'][$_GET['module'].'_payment_types']['ptype_vat']),
			array('value' => 'platnoscipl', 'label' => $aConfig['lang'][$_GET['module'].'_payment_types']['ptype_platnoscipl']),
			array('value' => 'card', 'label' => $aConfig['lang'][$_GET['module'].'_payment_types']['ptype_card']),
			array('value' => 'bank_14days', 'label' => $aConfig['lang'][$_GET['module'].'_payment_types']['ptype_bank_14days'])
		);
		$pForm->AddRow($pForm->GetLabelHTML('payment', $aLang['payment']), $pForm->GetRadioSetHTML('payment', $aLang['payment'], $aPayments, $aData['payment'], '', true, false), '', array(), array('style' => 'padding-top: 10px;'));
		
		// forma transportu
		$aTransports = @array_merge(array(array('value' => '-1', 'label' => $aLang['transport_all'])), $this->getPossibleTransports());

		$pForm->AddRow($pForm->GetLabelHTML('transport', $aLang['transport']), $pForm->GetRadioSetHTML('transport', $aLang['transport'], $aTransports, $aData['transport'], '', true, false), '', array(), array('style' => 'padding-top: 10px;'));
		// przyciski		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aLang['button_show']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('action'=>'statistics')).'\');'), 'button'));
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of chooseConditions() function
	
	
	/**
	 * Metoda wyswietla statystyki dla wybranych warunkow
	 *
	 * @param		object	$pSmarty
	 */
	function showStats(&$pSmarty) {
		global $aConfig;
		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';
		$sSourceSql = '';
		$sSourceIt = '';
		$aSource = array();
		
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		$pForm = new FormTable('statistics', $aLang['header_statistics'], array('action'=>phpSelf(array())), array('col_width'=>30), $aConfig['common']['js_validation']);
		
		// naglowek warunkow dla ktorych wyswietlane sa statystyki
		$pForm->AddMergedRow($aLang['conditions'], array('class'=>'merged', 'style'=>'padding-left: 25px'));
		// przedzial czasowy
		$pForm->AddRow('&nbsp;', $aLang['dates_range'].': '.$aLang['start_date_from'].' <b>'.$_POST['start_date'].'</b> '.$aLang['end_date_to'].' <b>'.$_POST['end_date'].'</b>');
		// status zamowienia
		$pForm->AddRow('&nbsp;', $aLang['order_status'].': <b>'.$aConfig['lang'][$_GET['module']]['status_'.$_POST['order_status']].'</b>');
		// metoda platnosci
		$pForm->AddRow('&nbsp;', $aLang['payment'].': <b>'.$aConfig['lang'][$_GET['module'].'_payment_types']['ptype_'.$_POST['payment']].'</b>');
		// srodek transportu
		$pForm->AddRow('&nbsp;', $aLang['transport'].': <b>'.($_POST['transport'] == '-1' ? $aLang['transport_all'] : $_POST['transport']).'</b>');
				
		// naglowek statystyk
		$pForm->AddMergedRow($aLang['statistics'], array('class'=>'merged', 'style'=>'padding-left: 25px'));
		// pobranie liczby i wartosci zamowien
		$aOrders =& $this->getOrdersSummary($sSourceSql);
		// liczba zamowien
		$pForm->AddRow('&nbsp;', $aLang['total_orders'].': <b>'.$aOrders['quantity'].'</b>');
		// liczba uzytkownikow skladajacych zamowienia
		$pForm->AddRow('&nbsp;', $aLang['total_users'].': <b>'.$aOrders['users'].'</b>');
		// calkowita wartosc zamowien
		$pForm->AddRow('&nbsp;', $aLang['total_value'].': <b>'.Common::formatPrice($aOrders['orders_value_netto'], ' ').' <span style="color: gray;">'.$aConfig['lang']['common']['currency'].'</span></b> (<b>'.Common::formatPrice($aOrders['orders_value_brutto'], ' ').' <span style="color: gray;">'.$aConfig['lang']['common']['currency'].'</span></b>)');
		// srednia wartosc zamowienia
		$pForm->AddRow('&nbsp;', $aLang['medial_value'].': <b>'.((int) $aOrders['quantity'] > 0 ? Common::formatPrice($aOrders['orders_value_netto'] / $aOrders['quantity'], ' ') : '0').' <span style="color: gray;">'.$aConfig['lang']['common']['currency'].'</span></b> (<b>'.((int) $aOrders['quantity'] > 0 ? Common::formatPrice($aOrders['orders_value_brutto'] / $aOrders['quantity'], ' ') : '0').' <span style="color: gray;">'.$aConfig['lang']['common']['currency'].'</span></b>)');
		// pobranie liczby zamowionych produktow
		$aProducts =& $this->getOrdersProducts($sSourceSql);
		// liczba zamowionych produktow
		$pForm->AddRow('&nbsp;', $aLang['total_products'].': <b>'.$aProducts['products'].'</b>');
		// srednia liczba zamowionych produktow
		$pForm->AddRow('&nbsp;', $aLang['medial_products'].': <b>'.((int) $aOrders['quantity'] > 0 ? Common::formatPrice3($aProducts['products'] / $aOrders['quantity']) : '0').'</b>');
		// liczba zamowionych sztuk produktow
		$pForm->AddRow('&nbsp;', $aLang['total_products_quantity'].': <b>'.$aProducts['products_quantity'].'</b>');
		// srednia liczba zamowionych sztuk produktow
		$pForm->AddRow('&nbsp;', $aLang['medial_products_quantity'].': <b>'.((int) $aOrders['quantity'] > 0 ? Common::formatPrice3($aProducts['products_quantity'] / $aOrders['quantity']) : '0').'</b>');
		
		if ((int) $aOrders['quantity'] > 0) {
			$aMaxMinOrders =& $this->getMaxMinOrders($sSourceSql);
			// zamowienia z najwyzsza wartoscia (10 zamowien)
			$pForm->AddMergedRow('<div style="float: left;">'.$aLang['max_value_orders'].'</div><div style="float: left; padding-left: 30px;"><a href="javascript:void(0);" onclick="toggleRowVisibility(this, \'th_max\', \'td_max\', \'showLink\', \'hideLink\', \''.$aConfig['lang'][$_GET['module'].'_statistics']['show'].'\', \''.$aConfig['lang'][$_GET['module'].'_statistics']['hide'].'\');" class="showLink">'.$aConfig['lang'][$_GET['module'].'_statistics']['show'].'</a></div>', array('class'=>'mergedLight', 'style'=>'padding-left: 30px'));
			// tabelka z zamowieniami
			$pForm->AddRow('&nbsp;', $this->formatOrdersTable($aMaxMinOrders['max']), '', array('id' => 'th_max','style' => 'display: none;'), array('id' => 'td_max', 'style' => 'display: none;'));
						
			// zamowienia z najizsza wartoscia (10 zamowien)
			$pForm->AddMergedRow('<div style="float: left;">'.$aLang['min_value_orders'].'</div><div style="float: left; padding-left: 30px;"><a href="javascript:void(0);" onclick="toggleRowVisibility(this, \'th_min\', \'td_min\', \'showLink\', \'hideLink\', \''.$aConfig['lang'][$_GET['module'].'_statistics']['show'].'\', \''.$aConfig['lang'][$_GET['module'].'_statistics']['hide'].'\');" class="showLink">'.$aConfig['lang'][$_GET['module'].'_statistics']['show'].'</a></div>', array('class'=>'mergedLight', 'style'=>'padding-left: 30px'));
			// tabelka z zamowieniami
			$pForm->AddRow('&nbsp;', $this->formatOrdersTable($aMaxMinOrders['min']), '', array('id' => 'th_min','style' => 'display: none;'), array('id' => 'td_min', 'style' => 'display: none;'));
			
			$aMostLeastPopularProducts =& $this->getMostLeastPopularProducts($sSourceSql);
			// najczesciej zamawiane produkty
			$pForm->AddMergedRow('<div style="float: left;">'.$aLang['most_popular_products'].'</div><div style="float: left; padding-left: 30px;"><a href="javascript:void(0);" onclick="toggleRowVisibility(this, \'th_most\', \'td_most\', \'showLink\', \'hideLink\', \''.$aConfig['lang'][$_GET['module'].'_statistics']['show'].'\', \''.$aConfig['lang'][$_GET['module'].'_statistics']['hide'].'\');" class="showLink">'.$aConfig['lang'][$_GET['module'].'_statistics']['show'].'</a></div>', array('class'=>'mergedLight', 'style'=>'padding-left: 30px'));
			// tabelka z produktami
			$pForm->AddRow('&nbsp;', $this->formatProductsTable($aMostLeastPopularProducts['most']), '', array('id' => 'th_most','style' => 'display: none;'), array('id' => 'td_most', 'style' => 'display: none;'));
						
			// najrzadziej zamawiane produkty
			$pForm->AddMergedRow('<div style="float: left;">'.$aLang['least_popular_products'].'</div><div style="float: left; padding-left: 30px;"><a href="javascript:void(0);" onclick="toggleRowVisibility(this, \'th_least\', \'td_least\', \'showLink\', \'hideLink\', \''.$aConfig['lang'][$_GET['module'].'_statistics']['show'].'\', \''.$aConfig['lang'][$_GET['module'].'_statistics']['hide'].'\');" class="showLink">'.$aConfig['lang'][$_GET['module'].'_statistics']['show'].'</a></div>', array('class'=>'mergedLight', 'style'=>'padding-left: 30px'));
			// tabelka z produktami
			$pForm->AddRow('&nbsp;', $this->formatProductsTable($aMostLeastPopularProducts['least']), '', array('id' => 'th_least','style' => 'display: none;'), array('id' => 'td_least', 'style' => 'display: none;'));
		}
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$_GET['module']]['button_change_conditions'], array('onclick'=>'MenuNavigate(\''.phpSelf(array_merge(array('start_date' => $_POST['start_date'], 'end_date' => $_POST['end_date'], 'order_status' => $_POST['order_status'], 'payment' => $_POST['payment'], 'transport' => $_POST['transport'], 'source' => $_POST['source']), $aSource), array('do')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of showStats() function
	
	
	/**
	 * Metoda pobiera mozliwe (wykorzystane juz w zamowieniach) metody
	 * transportu i zwraca je w formacie dla pola SELECT
	 * 
	 * @return 	array ref
	 */
	function getPossibleTransports() {
		global $aConfig;
		$aTransports = array();
		
		$sSql = "SELECT DISTINCT transport AS value, transport AS label
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE transport IS NOT NULL AND
						 			 transport <> ''
						 ORDER BY transport";
		return Common::GetAll($sSql);
	} // end of getPossibleTransports() method
	

	
	/**
	 * Metoda pobiera liczbe zlozonych zamowien oraz ich wartosc na podstawie
	 * przeslanych w POST warunkow
	 * 
	 * @param	string	$sSourceSql - SQL zrodla zamowien
	 * @return 	array ref
	 */
	function &getOrdersSummary($sSourceSql) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(id) quantity, SUM(value_brutto) orders_value_brutto, SUM(value_netto) orders_value_netto, COUNT(DISTINCT user_id) users
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE order_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
						 			 order_date <= '".formatDate($_POST['end_date'])." 23:59:59'".
						 			 ($_POST['order_status'] != '-1' ? " AND order_status = '".$_POST['order_status']."'" : '').
						 			 ($_POST['payment'] != 'payment_all' ? " AND payment_type = '".$_POST['payment']."'" : '').
						 			 ($_POST['transport'] != '-1' ? " AND transport = '".$_POST['transport']."'" : '').
						 			 $sSourceSql;
		return Common::GetRow($sSql);
	} // end of getOrdersSummary() method
	
	
	/**
	 * Metoda pobiera liczbe zamowionych w ramach zlozonych zamowien dla
	 * przeslanych w POST warunkow
	 * 
	 * @param	string	$sSourceSql - SQL zrodla zamowien
	 * @return 	array ref
	 */
	function &getOrdersProducts($sSourceSql) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(id) products, SUM(quantity) products_quantity
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id IN (
						 	SELECT id 
						 	FROM ".$aConfig['tabls']['prefix']."orders
						 	WHERE order_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
						 			 	order_date <= '".formatDate($_POST['end_date'])." 23:59:59'".
						 			 	($_POST['order_status'] != '-1' ? " AND order_status = '".$_POST['order_status']."'" : '').
						 			 	($_POST['payment'] != 'payment_all' ? " AND payment_type = '".$_POST['payment']."'" : '').
						 			 	($_POST['transport'] != '-1' ? " AND transport = '".$_POST['transport']."'" : '').
						 			  $sSourceSql.")";
		return Common::GetRow($sSql);
	} // end of getOrdersProducts() method
	
	
	/**
	 * Metoda pobiera 10 zamowien o najwiekszej wartosci
	 * i 10 zamowien o najmniejszej dla przeslanych w POST warunkow
	 * 
	 * @param	string	$sSourceSql - SQL zrodla zamowien
	 * @return 	array ref
	 */
	function &getMaxMinOrders($sSourceSql) {
		global $aConfig;
		$aIts = array();
		// najwieksza wartosc
		$sSql = "SELECT A.id, A.value_netto, A.value_brutto, A.user_id, A.email, CONCAT(B.name,' ',B.surname,' ',B.company) company, B.street, B.postal, B.city
						 FROM ".$aConfig['tabls']['prefix']."orders A
						 JOIN ".$aConfig['tabls']['prefix']."orders_users_addresses B 
						 ON B.order_id=A.id AND B.address_type='0'
						 WHERE A.order_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
						 			 A.order_date <= '".formatDate($_POST['end_date'])." 23:59:59'".
						 			 ($_POST['order_status'] != '-1' ? " AND A.order_status = '".$_POST['order_status']."'" : '').
						 			 ($_POST['payment'] != 'payment_all' ? " AND A.payment_type = '".$_POST['payment']."'" : '').
						 			 ($_POST['transport'] != '-1' ? " AND A.transport = '".$_POST['transport']."'" : '').
						 			 $sSourceSql."
						 ORDER BY A.value_netto DESC
						 LIMIT 0, 10";
		$aIts['max'] =&  Common::GetAll($sSql);
		// najmniejsza wartosc
		$sSql = "SELECT A.id, A.value_netto, A.value_brutto, A.user_id, A.email, CONCAT(B.name,' ',B.surname,' ',B.company) company, B.street, B.postal, B.city
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE A.order_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
						 			 A.order_date <= '".formatDate($_POST['end_date'])." 23:59:59'".
						 			 ($_POST['order_status'] != '-1' ? " AND A.order_status = '".$_POST['order_status']."'" : '').
						 			 ($_POST['payment'] != 'payment_all' ? " AND A.payment_type = '".$_POST['payment']."'" : '').
						 			 ($_POST['transport'] != '-1' ? " AND A.transport = '".$_POST['transport']."'" : '').
						 			 $sSourceSql."
						 ORDER BY A.value_netto ASC
						 LIMIT 0, 10";
		$aIts['min'] =&  Common::GetAll($sSql);
		return $aIts;
	} // end of getMaxMinOrders() method
	
	
	/**
	 * Metoda formatuje tabele z lista zamowien przekazanych w $aOrders
	 * 
	 * @param	array ref	$aOrders	- lista zamowien
	 * @return	string
	 */
	function formatOrdersTable(&$aOrders) {
		global $aConfig;
		$aLang =& $aConfig['lang'][$_GET['module'].'_statistics'];
		$sStr = '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="recordsTable">';
		$sStr .= '<tr>';
		$sStr .= '<th width="10%" style="text-align: left;">'.$aLang['id'].'</th>';
		$sStr .= '<th width="10%" style="text-align: left;">'.$aLang['value_netto'].'</th>';
		$sStr .= '<th width="10%" style="text-align: left;">'.$aLang['value_brutto'].'</th>';
		$sStr .= '<th width="10%" style="text-align: left;">'.$aLang['user'].'</th>';
		$sStr .= '<th width="20%" style="text-align: left;">'.$aLang['user_name'].'</th>';
		$sStr .= '<th width="20%" style="text-align: left;">'.$aLang['user_street'].'</th>';
		$sStr .= '<th width="20%" style="text-align: left;">'.$aLang['user_city'].'</th>';
		$sStr .= '</tr>';
		foreach ($aOrders as $aOrd) {
			$sStr .= '<tr>';
			$sStr .= '<td>'.$aOrd['id'].'</td>';
			$sStr .= '<td><b>'.Common::formatPrice($aOrd['value_netto'], ' ').' <span style="color: gray;">'.$aConfig['lang']['common']['currency'].'</span></b></td>';
			$sStr .= '<td><b>'.Common::formatPrice($aOrd['value_brutto'], ' ').' <span style="color: gray;">'.$aConfig['lang']['common']['currency'].'</span></b></td>';
			$sStr .= '<td>[ '.$aOrd['user_id'].' ]<br><b>'.$aOrd['email'].'</b></td>';
			$sStr .= '<td>'.Common::convert($aOrd['company']).'</td>';
			$sStr .= '<td>'.Common::convert($aOrd['street']).'</td>';
			$sStr .= '<td>'.$aOrd['postal'].' '.Common::convert($aOrd['city']).'</td>';
			$sStr .= '</tr>';
		}
		$sStr .= '</table>';
		return $sStr;
	} // end of formatOrdersTable() method
	
	
	/**
	 * Metoda pobiera 10 najczesciej zamawianych
	 * i 10 najrzadziej zamawianych produktow dla przeslanych w POST warunkow
	 * 
	 * @param	string	$sSourceSql - SQL zrodla zamowien
	 * @return 	array ref
	 */
	function &getMostLeastPopularProducts($sSourceSql) {
		global $aConfig;
		$aIts = array();
		// najczesciej zamawiane
		$sSql = "SELECT product_id, name, COUNT(id) orders, SUM(quantity) total_ordered
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id IN (
						 	SELECT id 
						 	FROM ".$aConfig['tabls']['prefix']."orders
						 	WHERE order_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
						 			 	order_date <= '".formatDate($_POST['end_date'])." 23:59:59'".
						 			 	($_POST['order_status'] != '-1' ? " AND order_status = '".$_POST['order_status']."'" : '').
						 			 	($_POST['payment'] != 'payment_all' ? " AND payment_type = '".$_POST['payment']."'" : '').
						 			 	($_POST['transport'] != '-1' ? " AND transport = '".$_POST['transport']."'" : '').
										$sSourceSql.")
						 GROUP BY product_id
						 ORDER BY orders DESC
						 LIMIT 0, 10";
		$aIts['most'] =&  Common::GetAll($sSql);
		// najrzadziej zamawiane
		$sSql = "SELECT product_id, name, COUNT(id) orders, SUM(quantity) total_ordered
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id IN (
						 	SELECT id 
						 	FROM ".$aConfig['tabls']['prefix']."orders
						 	WHERE order_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
						 			 	order_date <= '".formatDate($_POST['end_date'])." 23:59:59'".
						 			 	($_POST['order_status'] != '-1' ? " AND order_status = '".$_POST['order_status']."'" : '').
						 			 	($_POST['payment'] != 'payment_all' ? " AND payment_type = '".$_POST['payment']."'" : '').
						 			 	($_POST['transport'] != '-1' ? " AND transport = '".$_POST['transport']."'" : '').
										$sSourceSql.")
						 GROUP BY product_id
						 ORDER BY orders ASC
						 LIMIT 0, 10";
		$aIts['least'] =&  Common::GetAll($sSql);
		return $aIts;
	} // end of getMostLeastPopularProducts() method
	
	
	/**
	 * Metoda formatuje tabele z lista produktow przekazanych w $aItems
	 * 
	 * @param	array ref	$aItems	- lista produktow
	 * @return	string
	 */
	function formatProductsTable(&$aItems) {
		global $aConfig;
		$aLang =& $aConfig['lang'][$_GET['module'].'_statistics'];
		$sStr = '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="recordsTable">';
		$sStr .= '<tr>';
		$sStr .= '<th width="10%" style="text-align: left;">'.$aLang['id'].'</th>';
		$sStr .= '<th width="60%" style="text-align: left;">'.$aLang['name'].'</th>';
		$sStr .= '<th width="15%" style="text-align: left;">'.$aLang['orders'].'</th>';
		$sStr .= '<th width="15%" style="text-align: left;">'.$aLang['total_ordered'].'</th>';
		$sStr .= '</tr>';
		foreach ($aItems as $aIt) {
			$sStr .= '<tr>';
			$sStr .= '<td>'.$aIt['product_id'].'</td>';
			$sStr .= '<td><b>'.$aIt['name'].'</b></td>';
			$sStr .= '<td><b>'.$aIt['orders'].'</b></td>';
			$sStr .= '<td>'.$aIt['total_ordered'].'</td>';
			$sStr .= '</tr>';
		}
		$sStr .= '</table>';
		return $sStr;
	} // end of formatProductsTable() method
	
	
	
} // end of Module Class
?>