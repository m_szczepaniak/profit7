<?php
/**
 * Klasa weryfikacji zamówień
 *
 * @author    Arkadiusz Golba <arekgl0@op.pl>
 * @copyright 2013
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// Id strony modulu
	var $iPageId;
	
	// katalog plikow log
	var $sLogsDir;


  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {

		$this->sErrMsg = '';
		$this->iPageId = getModulePageId('m_zamowienia');
		$this->sModule = $_GET['module'];
		$this->iLangId = $_SESSION['lang']['id'];
		
	
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
      if (!hasModulePrivileges($_GET['module_id'])){
        showPrivsAlert($pSmarty);
      return;
      }
		}

		switch ($sDo) {
			case 'change_status': $this->ChangeStatus($pSmarty, $iId); 				break;
			case 'details': 			$this->showDetails($pSmarty, $iId); 	break;
			default: 							$this->Show($pSmarty); 								break;
		}
	} // end Module() function
  
  
	/**
	 * Metoda tworzy formularz z informacjami na temat uzytkownika
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID konta uzytkownika
	 * @return	void
	 */
	function showDetails(&$pSmarty, $iId) {
		global $aConfig;

		$aLang =& $aConfig['lang'][$this->sModule];

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych szczegolow konta uzytkownika
		$sSql = "SELECT A.id, A.number, A.saved_type, A.email, A.date_send, A.content, A.send_by
							 FROM ".$aConfig['tabls']['prefix']."orders_send_history AS A
							 WHERE A.id=".$iId;
		$aData =& Common::GetRow($sSql);
		$aData['source'] = in_array($aData['source'], array(0,1,5))?($aData['source']==0?'Profit J':($aData['source']=='1'?'Profit E':'Profit M')):$aData['source_name'];
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['details_header_'.$aData['saved_type']],
											 Common::convert($aData['email']));

		$pForm = new FormTable('ordered_itm', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), false);
		$pForm->AddHidden('do', 'change_status');
		// dane wiadomości
		if(!empty($aData['email'])){
			$pForm->AddRow($aLang['details_email'], Common::convert($aData['email']));
		}
		// dane do faktury
		$pForm->AddMergedRow($aLang['details_content_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		
		// wyszukiwanie ksiazek 
		$aBooks =& $this->getBooksNew($iId);
		if(!empty($aBooks)){
				$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_change_validate']));
			}
		
	
		// wyswietlenie listy pasujacych uzytkownikow
		$pForm->AddMergedRow( $this->GetBooksList($aBooks) );
		if(!empty($aBooks)){
				$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send2', $aConfig['lang'][$this->sModule]['button_change_validate']));
			}
		

			
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('style'=>'float:right;','onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

    $sInput = '
    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" /></div>
    <script type="text/javascript" src="js/validateOrderItem.js"></script>';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sInput.ShowTable($pForm->ShowForm()));
	} // end of Details() function
  

	/**
	 * Metoda wyswietla liste
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		$sFilterSql = '';
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		include_once('Form/FormTable.class.php');
		
		// zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['header'],
			'refresh'	=> true,
			'search'	=> true,
			'checkboxes'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'invoice_id',
				'content'	=> $aConfig['lang'][$this->sModule]['list_invoice_id'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'order_number',
				'content'	=> $aConfig['lang'][$this->sModule]['list_order_number'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'verification_date',
				'content'	=> $aConfig['lang'][$this->sModule]['list_verification_date'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'verification_by',
				'content'	=> $aConfig['lang'][$this->sModule]['list_verification_by'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
    
    $sSearchSQL = '';
    if (isset($_POST['search']) && $_POST['search'] != '') {
      $sSearchSQL = ' AND (order_number LIKE "'.$_POST['search'].'%" OR invoice_id LIKE "'.$_POST['search'].'%") ';
    }
		
		
		// pobranie liczby wszystkich list
		$sSql = "SELECT COUNT(id) 
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE order_status <> '5'  ".
									 $sSearchSQL;
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(id) 
							 FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE order_status <> '5'  ".
										 $sSearchSQL;
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('send_history', $aHeader, $aAttribs);
		
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			// pobranie zamówień bankowych
			$sSql = "SELECT A.id, A.invoice_id, A.order_number, A.verification_date, A.verification_by
							 FROM ".$aConfig['tabls']['prefix']."orders AS A
							 WHERE order_status <> '5'  ".$sFilterSql.
										 $sSearchSQL.
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' A.order_date DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			foreach ($aRecords as $iKey => $aRecord) {
        if (!empty($aRecord['verification_date'])) {
          $aRecords[$iKey]['invoice_id'] = '<span style="color: green;">'.$aRecord['invoice_id'].'</span>';
        }
      }
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
          'invoice_id' => array (
            'link'	=> phpSelf(array('do' => 'details', 'id' => '{id}'))
          ),
				'action' => array (
					'actions'	=> array ('details'),
					'params' => array (
												'details'	=> array('id' => '{id}')
											),
					'show' => false	
				) 
 			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array()
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		//przycisk przejdz do starego widoku
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
																				
	} // end of Show() method
	
  
  /**
   * Metoda zmienia status zamówienia na zweryfikowane
   * 
   * @global array $aConfig
   * @param object $pSmarty
   * @param int $iId
   */
	function ChangeStatus(&$pSmarty, $iId=0) {
		global $aConfig;
		$bIsErr = false;
		
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			// zaktualizuj stany pozycji
			
      $aValues = array(
          'verification_date' => 'NOW()', 
          'verification_by' => $_SESSION['user']['name']
      );
			Common::Update('orders', $aValues, ' id = '.$iId.' LIMIT 1; ');
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['verify_ok'];
				$this->sMsg .= GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['verify_err'];
				$this->sMsg .= GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
			$this->Show($pSmarty);
		} else {
			$this->Show($pSmarty);
		}
	}// end of ChangeStatus() method
	
	
	/**
	 * Metoda wyswietla liste ksiazek z tablicy $aBooks
	 *
	 * @param		object	$pSmarty
	 * @param	array ref	$aBooks	- lista produktów
	 * @return	void
	 */
	function GetBooksList(&$aBooks) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/EditableView.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'checkboxes'	=> true,
			'editable' => true,
			'count_checkboxes' => true,
			'count_checkboxes_by' => 'quantity',
			'form'	=>	false,
      'count_header' => true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
      array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '20'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_cover']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_name']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_quantity'],
        'style' => 'font-size: 20px!important; font-weight: bold;'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_curr_quantity'],
        'style' => 'font-size: 20px!important; font-weight: bold;'
			)
    );

		$pView = new EditableView('ordered_itm', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array(
			'id' => array(
				'show'	=> false
			),
			'currquantity'=> array(
					'editable'	=> true
			),
		);
			// ustawienia dla kolumn edytowalnych
		$aEditableSettings = array(
			'currquantity' => array(
				'type' => 'text'
			)
		);
    foreach ($aBooks as $iKey => $aBook) {
      $aBooks[$iKey]['currquantity'] = '0';
    }
		// dodanie rekordow do widoku
		$pView->AddRecords($aBooks, $aColSettings,$aEditableSettings);
		// dodanie stopki do widoku
		$aRecordsFooter = array(
				array('check_all')
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		// przepisanie langa - info o braku rekordow
		$aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_books_to_show'];
				// walidacja js kolumn edytowalnych
		$sAdditionalScript='';
		foreach($aBooks as $iKey=>$aItem) {
			if($aItem['weight']=='0,00')	
				$sAdditionalScript.='document.getElementById(\'delete['.$aItem['id'].']\').disabled=true;
				';		
			}		
		$sJS = '
		<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
		'.$sAdditionalScript.'
			$("#ordered_itm").submit(function() {
        if ($("input[name^=\'delete\']:not(:checked)").length === 0) {
          return true;
        } else {
          alert("Błąd zatwierdzania zamówienia, nie zatwierdzono wszystkich pozycji !!!");
        }
        return false;
			});
		});
	// ]]>
</script>
		';
		return $sJS.$pView->Show();
	} // end of GetBooksList() function
  
  
  /**
   * Metoda pobiera liste ksiazek dla wygenerowanego wydruku
   * 
   * @param int $iId id zamówienia
   * @return array - lista ksiazek
   */
	function &getBooksNew($iId) {
		global $aConfig;

			// ksiazek spelniajacych zadane kryteria dla danego wydruku
			$sSql = "SELECT A.id, PI.photo, R.id AS order_id,
											A.isbn, A.name, SUM(A.quantity) as quantity, A.publisher, A.authors, A.publication_year, A.edition,
                      PI.directory
							 FROM ".$aConfig['tabls']['prefix']."orders R, ".$aConfig['tabls']['prefix']."orders_items A
               LEFT JOIN products_images AS PI
               ON PI.product_id = A.product_id
							 WHERE
							 A.deleted = '0'
							 AND A.packet = '0'
							 AND R.order_status <> '5'
							 AND R.id=A.order_id
							 AND R.id=".$iId.'
               GROUP BY A.isbn ';
			$aBooks = Common::GetAll($sSql);
			
			foreach($aBooks as $iKey=>$aItem){
				$aBooks[$iKey]['name'] = $aItem['name'].
											($aItem['isbn']?'<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="'.$aItem['isbn'].'" item_id="'.$aItem['id'].'">'.$aItem['isbn'].'</span>':'').
											($aItem['publication_year']?'<br />'.$aItem['publication_year']:'').($aItem['edition']?' wyd. '.$aItem['edition']:'').
											($aItem['publisher']?'<br />'.$aItem['publisher']:'').
											($aItem['authors']?'<br />'.$aItem['authors']:'').'</span>';
				unset($aBooks[$iKey]['isbn']);
				unset($aBooks[$iKey]['publication_year']);
				unset($aBooks[$iKey]['edition']);
				unset($aBooks[$iKey]['publisher']);
				unset($aBooks[$iKey]['authors']);
        
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aItem['directory'].'/__t_'.$aItem['photo'])){
          $aBooks[$iKey]['photo'] = '<img src="/images/photos/'.$aItem['directory'].'/__t_'.$aItem['photo'].'" />';	
				} else {
          $aBooks[$iKey]['photo'] = 'BRAK OKŁADKI';
        }
        unset($aBooks[$iKey]['directory']);
        
				unset($aBooks[$iKey]['order_id']);
			}
		
		return $aBooks;
	} // end of getBooksNew() method
} // end of Module Class
?>