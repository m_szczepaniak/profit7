<?php
/**
 * Klasa odule do obslugi Zamowien - przechowalnia
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczene wspolnej klasy modulu Zamowienia
include_once('modules/m_zamowienia/client/Common.class.php');

class Module extends Common_m_zamowienia {

  // Id wersji jezykowej
  var $iLangId;
  
  // symbol wersji jezykowej
  var $sLang;
  
  // Id strony
  var $iPageId;
  
  // nazwa strony
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon strony
  var $sTemplate;
  
  // symbol modulu
  var $sModule;
  
  // ustawienia strony modulu
  var $aSettings;
  
  // opcja modulu
  var $sOption;
  
  // link do strony modulu
  var $sPageLink;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module($sSymbol, $iPageId, $sName) {
		global $aConfig;
		
		$this->iLangId =& $_GET['lang_id'];
		$this->sLang =& $_GET['lang'];
		$this->iPageId = $iPageId;
		$this->sName = $sName;
		$this->sModule = $sSymbol;
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/';
		// konstruktor klasy Common
		parent::Common_m_zamowienia($aConfig['_tmp']['page']['id'],
																'/'.$aConfig['_tmp']['page']['symbol'],
																$aConfig['_tmp']['page']['module'],
																'modules/'.$aConfig['_tmp']['page']['module']);
	} // end Module() function


	/**
	 * Metoda realizuje zadania koszyka - dodawanie, przeliczanie, usuwanie produktow
	 * z koszyka, realizacja zamowienia
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];

		if(isset($_SESSION['order']['products'])){
			$this->calculateCart();
			
			$aModule['items'] =& $_SESSION['order']['products'];
			
			
			foreach ($aModule['items'] as $iKey => $aValue){
				$aModule['items'][$iKey]['price_brutto'] = Common::formatPrice($aModule['items'][$iKey]['price_brutto']);
				$aModule['items'][$iKey]['promo_price_brutto'] = Common::formatPrice($aModule['items'][$iKey]['promo_price_brutto']);
				$aModule['items'][$iKey]['user_price'] = Common::formatPrice($aModule['items'][$iKey]['user_price']);
			}
			
			
			
			$aModule['data'] =& $_SESSION['order']['data'];
			
			// sprawdzenie czy wybrana przesylka zagraniczna oraz pobranie kosztu wysylki
			$sSql = "SELECT name, price, international, no_costs_from FROM ".$aConfig['tabls']['prefix']."orders_transport_means WHERE id = ".$_SESSION['order']['transport']."";
			
			$aModule['transport']=& Common::GetRow($sSql);
			
			if(Common::formatPrice2($_SESSION['order']['total_price_brutto']) > $aModule['transport']['no_costs_from'] && $aModule['transport']['no_costs_from'] > 0){
				$aModule['transport']['price'] = 0;
			}
			
			$aModule['total_cost'] = Common::formatPrice(Common::formatPrice2($_SESSION['order']['total_price_brutto']) + $aModule['transport']['price']);
			
			$aModule['transport']['price'] = Common::formatPrice($aModule['transport']['price']);

			
			$aModule['total_price_brutto'] = $_SESSION['order']['total_price_brutto'];
			
			//dump($aModule);
			
			$pSmarty->assign_by_ref('aModule', $aModule);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/order_print.tpl');
			$pSmarty->clear_assign('aModule', $aModule);
			
		} else {
			doRedirect("/");
		}
		return $sHtml;
	} // end of getContent()
} // end of Module Class
?>