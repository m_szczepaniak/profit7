<?php
/**
 * Klasa odule do obslugi Zamowien - przechowalnia
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczene wspolnej klasy modulu Zamowienia
include_once('modules/m_zamowienia/client/Common.class.php');

class Module extends Common_m_zamowienia {

  // Id wersji jezykowej
  var $iLangId;
  
  // symbol wersji jezykowej
  var $sLang;
  
  // Id strony
  var $iPageId;
  
  // nazwa strony
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon strony
  var $sTemplate;
  
  // symbol modulu
  var $sModule;
  
  // ustawienia strony modulu
  var $aSettings;
  
  // opcja modulu
  var $sOption;
  
  // link do strony modulu
  var $sPageLink;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module($sSymbol, $iPageId, $sName) {
		global $aConfig;
		
		$this->iLangId =& $_GET['lang_id'];
		$this->sLang =& $_GET['lang'];
		$this->iPageId = $iPageId;
		$this->sName = $sName;
		$this->sModule = $sSymbol;
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/';
		// konstruktor klas Common
		parent::Common_m_zamowienia($aConfig['_tmp']['page']['id'],
																'/'.$aConfig['_tmp']['page']['symbol'],
																$aConfig['_tmp']['page']['module'],
																'modules/'.$aConfig['_tmp']['page']['module']);
	} // end Module() function


	/**
	 * Metoda realizuje zadania koszyka - dodawanie, przeliczanie, usuwanie produktow
	 * z koszyka, realizacja zamowienia
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['page_link'] =& $this->sPageLink;

		
		return $sHtml;
	} // end of getContent()

} // end of Module Class
?>