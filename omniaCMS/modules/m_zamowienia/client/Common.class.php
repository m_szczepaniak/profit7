<?php
use table\keyValue;

/**
 * Klasa Common_m_zamowienia do obslugi modulu Zamowienia i jego boksow
 *
 * @property $oPromotions Promotions
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */
 
class Common_m_zamowienia {
  
  // Id strony
  var $iPageId;
  
	// sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // nazwa modulu
  var $sModule;
  
  // link do strony
  var $sPageLink;
  
	// egzemplarz obiektu klasy - OrderItemRecount
	var $oOrderItemRecount;
	
	var $aTransportOptions;
	
	var $aTimeTransportOptions;
  
  var $oPromotions;

	public $markUpCookie;

	private $tarrifOnRequest = [];

	private $productsAttachmentsTMP = [];
	
  /**
   * Konstruktor klasy
   *
   */
	function Common_m_zamowienia($iPId, $sPageLink, $sModule, $sTplsPath) {
		$this->iPageId = $iPId;
		$this->sModule = $sModule;
		$this->sTemplatesPath = $sTplsPath;
		$this->sPageLink = $sPageLink;
    
		//unset($_SESSION['wu_cart']['discount_code']);
		//unset($_SESSION['wu_cart']['discount']);
		include_once('OrderItemRecount.class.php');
		$this->oOrderItemRecount = new OrderItemRecount();
    
    include_once('Promotions/Promotions.class.php');
    $this->oPromotions = new Promotions();
    $this->oPromotions->checkProductPromotions(0, null);
		$this->tarrifsTMP = [];
		$this->productsAttachmentsTMP = [];
	} // end Common_m_zamowienia() function
	

	/**
	 * Metoda przelicza wartosc calego koszyka
	 * 
	 * @param	array ref	$aModule	- dane modulu
	 * @return	void
	 */
	function calculateCart() {
		global $aConfig;
    
    $aShipmentTimes =& $this->getShipmentTimes();
    $aFreeDays = $this->getFreeDays();
    $bIsFreeDay = $this->todayIsFreeDay($aFreeDays);

		$_SESSION['wu_cart']['total_without_promo'] = 0;
		$_SESSION['wu_cart']['total_price_brutto'] = 0;
		$_SESSION['wu_cart']['total_price_netto'] = 0;
		$_SESSION['wu_cart']['total_quantity'] = 0;
		$_SESSION['wu_cart']['total_cost'] = 0;
		$_SESSION['wu_cart']['to_pay'] = 0;
		$_SESSION['wu_cart']['shipment_days'] = 0;
		$_SESSION['wu_cart']['transport_days'] = 0;
		$_SESSION['wu_cart']['preview_shipment'] = 0;
    unset($_SESSION['wu_cart']['old']['promo_price']);
		
		if (!$this->cartIsEmpty()) {
			$bIsDiscountCodeBook = false;
			foreach ($_SESSION['wu_cart']['products'] as $iKey => $aItems) {
				$this->calculateItem($iKey);
        
        unset($_SESSION['wu_cart']['products'][$iKey]['promo_types']['code_discount_books']);
				// sprawdzenie rabatu ograniczonego do ksiązek w koszyku
        
        $iCodeMatchedType = $this->oPromotions->oPromotion->CodeDiscount->checkBookDiscount($_SESSION['wu_cart']['discount_code'], $iKey);
        if ($iCodeMatchedType > 1) {
            // kod zaakceptowany ze względu na pasujące książki
            $_SESSION['wu_cart']['products'][$iKey]['promo_types'] = array('code_discount_books' => true);
        }
          
				if (!empty($_SESSION['wu_cart']['discount_code']) && $iCodeMatchedType > 0) {
					$bIsDiscountCodeBook = true;
				}
      }
      // fix 30.01.2014 przeliczmy jeszcze netto całego zamówienia
      $_SESSION['wu_cart'] = $this->_RecountOrderValueNetto($_SESSION['wu_cart']);
      
			// usun rabat ograniczony na ksiazki - brak ksiażek w koszyku pasuja do kodu
			if (!empty($_SESSION['wu_cart']['discount_code']) && (!empty($_SESSION['wu_cart']['discount_code']['books']) || !empty($_SESSION['wu_cart']['discount_code']['extra'])) && $bIsDiscountCodeBook === false) {
				// usun rabat
				unset($_SESSION['wu_cart']['discount_code']);
			}
			
					
			// koszty transportu
			$this->setPaymentTransportCost();
			
			$_SESSION['wu_cart']['to_pay'] = Common::formatPrice2($_SESSION['wu_cart']['total_price_brutto'] + $_SESSION['wu_cart']['payment']['cost']);
						
			/** KALENDARZYK  **/
			// jesli ustawiony sposob transportu
			if (isset($_SESSION['wu_cart']['payment']['id'])) {
				// godzina graniczna
				// UWAGA modyfikacja z dnia 20.01.2012 - wyłaczyć sprawdzanie godziny granicznej
				// dla dni wolnych
				
				
				if(intval(date('G')) < $_SESSION['wu_cart']['payment']['time_border'] || $bIsFreeDay == true) {
					$iTransportDays = $_SESSION['wu_cart']['payment']['transport_days_before'];
					$iShipmentDays = $_SESSION['wu_cart']['payment']['shipment_days_before'];
				}
				else {
					$iTransportDays = $_SESSION['wu_cart']['payment']['transport_days_after'];
					$iShipmentDays = $_SESSION['wu_cart']['payment']['shipment_days_after'];
				}
                $_SESSION['wu_cart']['total_shipment_days'] = $_SESSION['wu_cart']['shipment_days'] + $iShipmentDays;
                $_SESSION['wu_cart']['total_transport_days'] = $_SESSION['wu_cart']['transport_days'] + $iTransportDays;
				$_SESSION['wu_cart']['total_transport_days_max'] = $_SESSION['wu_cart']['total_transport_days'] + 1;

				$_SESSION['wu_cart']['total_shipment_date'] =  $this->getDateAfterWorkingDays($_SESSION['wu_cart']['total_shipment_days'], $aFreeDays, $_SESSION['wu_cart']['preview_shipment'], ($iShipmentDays + $aShipmentTimes['3']['shipment_days']));
				$_SESSION['wu_cart']['total_transport_date'] = $this->getDateAfterWorkingDays($_SESSION['wu_cart']['total_transport_days'], $aFreeDays, $_SESSION['wu_cart']['preview_shipment'], ($iTransportDays + $aShipmentTimes['3']['transport_days']));
				$_SESSION['wu_cart']['total_transport_date_max'] = $this->getDateAfterWorkingDays($_SESSION['wu_cart']['total_transport_days_max'], $aFreeDays, $_SESSION['wu_cart']['preview_shipment'], ($iTransportDays + ($aShipmentTimes['3']['transport_days'] + 1)));
			}
		}
	} // end of calculateCart() method
  
 
  /**
   * Metoda przelicza wartość netto zamówienia
   * fix 30.01.2014 r.
   * 
   * @param array $aWuCart
   * @return array
   */
  private function _RecountOrderValueNetto($aWuCart) {
    $aVBruttoOnVat = array();
    $fSumValueNetto = 0;
    
    foreach ($aWuCart['products'] as $aProduct) {
      $aVBruttoOnVat[$aProduct['vat']] += $aProduct['total_price_brutto'];
    }
    
    foreach ($aVBruttoOnVat as $iVat => $fValueBrutto) {
      $fSumValueNetto += Common::formatPrice2($fValueBrutto / (1 + $iVat/100));
    }
    
    $aWuCart['total_price_netto'] = $fSumValueNetto;
    return $aWuCart;
  }// end of _RecountOrderValueNetto() method
  
  
  /**
   * Metoda odejmuję datę od ustalonej daty z Łukaszem, tak aby wysyłka szła 
   *  z daty 08-01
   * 
   * @return int
   */
  private function _calculateDiffDate($aFreeDays, $dDiffDate) {
    $dStart = new DateTime(date('Y-m-d'));
    $dEnd  = new DateTime($dDiffDate);
    $iDate = time();
    
    $iDaysCount = 0;
    $iAllDayCount = 1;
    $dDiff = $dStart->diff($dEnd);
		while($dDiff->format('%d') > 0){
      
			// dodaj dzień
			$iDate += (24 * 60 * 60);
			$aDate = getdate($iDate);
			// jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
			if($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
				$iDaysCount++;
			}
			$dStart = new DateTime(date('Y-m-d', strtotime("+".$iAllDayCount." day")));
      $dDiff = $dStart->diff($dEnd);
      $iAllDayCount++;
		}
    return intval($iDaysCount);
  }// end of _calculateDiffDate() method
	
	
	/**
	 * Metoda przelicza i formatuje ceny produktu, zlicza calkowita liczbe
	 * produktow w koszyku oraz calkowita wartosc netto i brutto koszyka
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @return	void	
	 */
	function calculateItem($iId) {
		global $aConfig;
		$aCart =& $_SESSION['wu_cart'];
		$aCartProduct =& $_SESSION['wu_cart']['products'][$iId];

		if($aCartProduct['price_brutto'] > 0 && $aCartProduct['quantity'] > 0){
			// calkowita liczba produktow w koszyku
			$aCart['total_quantity'] += $aCartProduct['quantity'];

			// calkowita wartosc brutto produktu 
			$aCartProduct['total_price_brutto'] = Common::formatPrice2(($aCartProduct['promo_price'] > 0 ? $aCartProduct['promo_price'] : $aCartProduct['price_brutto']) * $aCartProduct['quantity']);
			$aCartProduct['total_price_netto'] = Common::formatPrice2(($aCartProduct['promo_price_netto'] > 0 ? $aCartProduct['promo_price_netto'] : $aCartProduct['price_netto']) * $aCartProduct['quantity']); //zmienione - Konrad
			// calkowita wartosc brutto produktu bez promocji
			$aCartProduct['total_without_promo'] = Common::formatPrice2($aCartProduct['price_brutto'] * $aCartProduct['quantity']);
			// calkowita wartosc koszyka brutto bez promocji
			$aCart['total_without_promo'] += Common::formatPrice2($aCartProduct['total_without_promo']);
			// calkowita wartosc koszyka brutto
			$aCart['total_price_brutto'] += Common::formatPrice2($aCartProduct['total_price_brutto']);
//			$aCart['total_price_netto'] += Common::formatPrice2($aCartProduct['total_price_netto']);
			
			if($aCartProduct['shipment_days'] > $aCart['shipment_days']){
				$aCart['shipment_days'] = $aCartProduct['shipment_days'];
			}
			if($aCartProduct['transport_days'] > $aCart['transport_days']){
				$aCart['transport_days'] = $aCartProduct['transport_days'];
			}
			if(!empty($aCartProduct['shipment_date'])){
				$iPreviewTime=strtotime($aCartProduct['shipment_date']);
				if($iPreviewTime > $aCart['preview_shipment']){
					$aCart['preview_shipment'] = $iPreviewTime;
				}
			}
      $aTarrif = $this->getTarrif($iId);
      $aTarrif['packet'] = ($aCartProduct['packet']=='1');
      
      // przeliczanie starego rabatu
      $aOldP = $this->getPricesWithoutDiscountCode($aCartProduct, $aTarrif, $aCartProduct['discount_limit'], $iId);
      
      $_SESSION['wu_cart']['old']['promo_price'] += Common::formatPrice2($aOldP['promo_price'] * $aCartProduct['quantity']);
		}
		else {
			unset($_SESSION['wu_cart']['products'][$iId]);
		}
		
	} // end of calculateItem() method
	
	
 /**
  * Metoda ustawia koszt transportu
  * 
  * @return void
  */
  function setPaymentTransportCost() {
  	global $aConfig;

  	//dump($_POST);
  	if (isset($_POST['payment_type']) || isset($_POST['payment_id'])) {
  		// transport z formularza
  		$_SESSION['wu_cart']['payment']['id'] = (int) isset($_POST['payment_type']) ? $_POST['payment_type'] : $_POST['payment_id'];
  	}
    
  	if (isset($_SESSION['wu_cart']['payment']['id'])) {
  		$aUserPriv = $this->getSpecialUserPrivileges();
	  	$sSql = "SELECT A.ptype, A.pdata, A.cost, A.no_costs_from, 
	  						A.shipment_days_before, A.shipment_days_after, A.transport_days_before,
	  						A.transport_days_after, A.message_before, A.message_after, A.time_border,
	  						B.name AS transport, B.id AS transport_id, B.symbol, B.limit_to_expand_shipping_date ,A.cart_calendar_legend
	  					 FROM ".$aConfig['tabls']['prefix']."orders_payment_types A
	  					 JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
	  					 ON A.transport_mean=B.id
	  					 WHERE A.id = ".$_SESSION['wu_cart']['payment']['id'].
	  						(!empty($aUserPriv)&&($aUserPriv['allow_14days'] == '1')?'':" AND A.ptype <> 'bank_14days'").
  							(!empty($aUserPriv)&&($aUserPriv['allow_personal_reception'] == '1')?'':" AND B.personal_reception = '0'");
	  	
	  	$aData =& Common::GetRow($sSql);
	  	if(!empty($aData)){
        // jeśli nie załadowany lang
        if (!isset($aConfig['lang']['mod_m_zamowienia']['payment_type'][$aData['ptype']])) {
          include_once($aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].'modules/m_zamowienia/lang/pl.php');
        }
		  	$_SESSION['wu_cart']['payment']['name'] = $aConfig['lang']['mod_m_zamowienia']['payment_type'][$aData['ptype']];
		  	$_SESSION['wu_cart']['payment']['ptype'] = $aData['ptype'];
		  	$_SESSION['wu_cart']['payment']['no_costs_from'] = Common::formatPrice2($aData['no_costs_from']);
            $_SESSION['wu_cart']['payment']['limit_to_expand_shipping_date'] = $aData['limit_to_expand_shipping_date'];
				$_SESSION['wu_cart']['payment']['cost'] = Common::formatPrice2($aData['cost']);
				$_SESSION['wu_cart']['payment']['transport'] = $aData['transport'];
				$_SESSION['wu_cart']['payment']['transport_id'] = $aData['transport_id'];
        $_SESSION['wu_cart']['payment']['cart_calendar_legend'] = $aData['cart_calendar_legend'];
				
				$_SESSION['wu_cart']['payment']['shipment_days_before'] = $aData['shipment_days_before'];
				$_SESSION['wu_cart']['payment']['shipment_days_after'] = $aData['shipment_days_after'];
				$_SESSION['wu_cart']['payment']['transport_days_before'] = $aData['transport_days_before'];
				$_SESSION['wu_cart']['payment']['transport_days_after'] = $aData['transport_days_after'];
				$_SESSION['wu_cart']['payment']['message_before'] = $aData['message_before'];
				$_SESSION['wu_cart']['payment']['message_after'] = $aData['message_after'];
				$_SESSION['wu_cart']['payment']['time_border'] = $aData['time_border'];

				$bDiscountFreeTransport = false;
				if (isset($_SESSION['wu_cart']['discount_code']) && $_SESSION['wu_cart']['discount_code']['off_cost_transport'] == '1') {
					if ($_SESSION['wu_cart']['payment']['transport_id'] > 0
						&& !empty($_SESSION['wu_cart']['discount_code']['off_cost_transport_methods'])
						&& is_array($_SESSION['wu_cart']['discount_code']['off_cost_transport_methods'])
					) {
						foreach ($_SESSION['wu_cart']['discount_code']['off_cost_transport_methods'] as $aTransportMethod) {
							if (intval($aTransportMethod['transport_id']) == intval($_SESSION['wu_cart']['payment']['transport_id'])
								&& (Common::formatPrice2($aTransportMethod['min_value']) == 0.00 || Common::formatPrice2($_SESSION['wu_cart']['total_price_brutto']) > $aTransportMethod['min_value'])
							) {
								if ($aData['ptype'] !== 'postal_fee') {
                                    $bDiscountFreeTransport = true;
								}
							}
						}
					}
				}
				
				include_once('FreeTransportPromo.class.php');
				$oFreeTransportPromo = new Lib_FreeTransportPromo();
				// sprawdzmy czy istnieją w koszyku książki które posiadają darmową przesyłkę
				$aFreeTransport = array();
				foreach ($_SESSION['wu_cart']['products'] as $iKey => $aVal) {
					$aFreeTransportTMP = $oFreeTransportPromo->getBookFreeTransport($iKey, $aVal['total_price_brutto']);
					if (!empty($aFreeTransportTMP)) {
						$aFreeTransport = array_merge($aFreeTransport, $aFreeTransportTMP);
						$aFreeTransport = array_unique($aFreeTransport);
					}
				}
				
				if (!empty($_SESSION['wu_cart']) && $_SESSION['wu_cart']['total_price_brutto'] > 0.00) {
					$aFreeTransportTMP = $oFreeTransportPromo->getBookFreeTransport('-1', $_SESSION['wu_cart']['total_price_brutto']);
					if (!empty($aFreeTransportTMP)) {
						$aFreeTransport = array_merge($aFreeTransport, $aFreeTransportTMP);
						$aFreeTransport = array_unique($aFreeTransport);
					}
				}
				$bFreeTransport = false;
				if(($aData['no_costs_from'] > 0 && $_SESSION['wu_cart']['total_price_brutto'] > $aData['no_costs_from']) ||
	  				(!empty($aUserPriv) && ($aUserPriv['allow_free_transport'] == '1')) ||
						$bDiscountFreeTransport == true || 
						in_array($aData['symbol'], $aFreeTransport)
								) {
					$_SESSION['wu_cart']['payment']['cost'] = 0;
					// Modyfikacja 21.05.2012 Ustalona przez M. Korecki z M.Chudy
					// flaga darmowej dostawy, jesli jest ustalona dd to blokujemy koszty transportu
					$bFreeTransport = true; 
				}
				
				if((!empty($aUserPriv) && ($aUserPriv['allow_free_transport'] == '1')) || 
								$bDiscountFreeTransport == true ||
								$bFreeTransport == true) {
					$_SESSION['wu_cart']['payment']['block_transport_cost'] = true;
				}
				else {
					$_SESSION['wu_cart']['payment']['block_transport_cost'] = false;
				}
				if ($aData['ptype'] == 'dotpay') {
					
				}
				
		//		if ($aData['ptype'] == 'dotpay') {
		//			$aData['pdata'] = unserialize(base64_decode($aData['pdata']));
	//				if (isset($aData['pdata']['fee']) && ($aData['pdata']['fee'] = Common::formatPrice2($aData['pdata']['fee'])) > 0) {
	//					// do kosztow transportu doliczenie prowizji DotPay od wartosci koszyka
	//					$_SESSION['wu_cart']['payment']['cost'] += Common::formatPrice2($_SESSION['wu_cart']['total_price_brutto'] * ($aData['pdata']['fee'] / 100));
	//				}
	//			}
	  	}
	  	else {
	  		unset($_SESSION['wu_cart']['payment']);
	  	}
  	}
  	// ustawienie calkowitego kosztu transportu - wartosc zamowienia brutto + transport
		$_SESSION['wu_cart']['total_cost'] = Common::formatPrice2($_SESSION['wu_cart']['total_price_brutto'] + $_SESSION['wu_cart']['payment']['cost']);
  } // end of setTransportCost() method

	
	/**
	 * Metoda zwraca czy koszyk jest pusty
	 * 
	 * @return	bool	- true: pusty; false: niepusty
	 */
	function cartIsEmpty() {
		if (isset($_SESSION['wu_cart']['products'])) {
			foreach ($_SESSION['wu_cart']['products'] as $sType => $aItems) {
				if (!empty($aItems) && $aItems['quantity'] > 0) return false;
			}
		}
		return true;
	}	// end of cartIsEmpty() method


	/**
	 * Metoda sprawdza czy produkt istnieje juz w koszyku
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @return	bool	- true: istnieje: false: nie istnieje
	 */
	function isAlreadyInCart($iId) {
		return isset($_SESSION['wu_cart']['products'][$iId]);
	} // end of isAlreadyInCart() method

	/**
	 * Metoda sprawdza czy ustawione sa dane uzytkownika
	 * 
	 * @return	bool
	 */
	function userDataIsEmpty() {
		if (!isset($_SESSION['wu_cart']['user_data'])) return true;
		foreach($_SESSION['wu_cart']['user_data'] as $sKey => $aValue) {
			if (!empty($_SESSION['wu_cart']['user_data'][$sKey])) return false;	
		}
		return true;
	} // end of userDataIsEmpty() method

	
	/**
	 * Metoda usuwa produkt z przechowalni
	 * 
	 * @param	id - id produktu
	 */
	function deleteFromRepository($iId) {
		global $aConfig;
		$sProductId = $iId;
				
		if(isLoggedIn()) {
			$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_briefcase
								WHERE konto_id = ".$_SESSION['w_user']['id']."
								AND product_id = ".$iId;
						
			if((int) Common::GetRow($sSql) == 1){
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."orders_briefcase
									WHERE konto_id = ".$_SESSION['w_user']['id']."
									AND product_id = ".$iId;
				
				Common::Query($sSql);
			}
		}
		else {
			if(isset($_COOKIE['repository'])){
				
				$sRepository = base64_decode($_COOKIE['repository']);
				
				$sTemp = "";				
				foreach(explode(",", $sRepository) as $iKey => $sValue) {
					if($sValue != $sProductId){
						if($sTemp == "")
							$sTemp = $sValue;
						else
							$sTemp .= ",".$sValue;
					}
				}
				
				$sRepository = base64_encode($sTemp);
				
				addcookie("repository", $sRepository, time() + 60*60*24*366);
			}
			else {
				$sRepository = base64_encode($iId);
				addcookie("repository", $sRepository, time() + 60*60*24*366);
			}
		}
	}
	

	 /**
	  * Metoda pobiera autorów produktu o podanym id
	  */
	  function getAuthors($iId){
	  	global $aConfig;
	  	// wybiera grupy kategorii dla tego produktu
	  	$sSql = "SELECT B.name
	  					 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
	  					 JOIN ".$aConfig['tabls']['prefix']."products_authors B
	  					 ON A.author_id=B.id
	  					 JOIN ".$aConfig['tabls']['prefix']."products_authors_roles C
	  					 ON A.role_id=C.id
	  					 WHERE A.product_id =".$iId."
	  					ORDER BY A.order_by";
	  	$sAuthors='';
	  	$aAuthors =& Common::GetAll($sSql);
	  	foreach($aAuthors as $aAuthor)
	  		$sAuthors .= $aAuthor['name'].', ';
	  	
	  	return substr($sAuthors,0,-2);
	  }
	
		/**
	 * Metoda wpisuje do sesji wszystkie potrzebne dane uzytkownika
	 * 
	 * @return	void
	 */
	function getUserDataFromPost() {
		global $aConfig;

			if(!empty($_POST)) {
				
			$_SESSION['wu_cart']['user_data']['invoice_data'] = isset($_POST['invoice_data'])?'1':'0';
			$_SESSION['wu_cart']['user_data']['name'] = filterInput($_POST['name']);
			$_SESSION['wu_cart']['user_data']['surname'] = filterInput($_POST['surname']);
			$_SESSION['wu_cart']['user_data']['company'] = filterInput($_POST['company']);
			$_SESSION['wu_cart']['user_data']['nip'] = filterInput($_POST['nip']);
			$_SESSION['wu_cart']['user_data']['city'] = filterInput($_POST['city']);
			$_SESSION['wu_cart']['user_data']['street'] = filterInput($_POST['street']);
			$_SESSION['wu_cart']['user_data']['number'] = filterInput($_POST['number']);
			$_SESSION['wu_cart']['user_data']['number2'] = filterInput($_POST['number2']);
			$_SESSION['wu_cart']['user_data']['postal'] = filterInput($_POST['postal']);
			$_SESSION['wu_cart']['user_data']['invoice_city'] = filterInput($_POST['invoice_city']);
			$_SESSION['wu_cart']['user_data']['invoice_postal'] = filterInput($_POST['invoice_postal']);
			$_SESSION['wu_cart']['user_data']['invoice_street'] = filterInput($_POST['invoice_street']);
			$_SESSION['wu_cart']['user_data']['invoice_number'] = filterInput($_POST['invoice_number']);
			$_SESSION['wu_cart']['user_data']['invoice_number2'] = filterInput($_POST['invoice_number2']);
			$_SESSION['wu_cart']['user_data']['invoice_company'] = filterInput($_POST['invoice_company']);
			$_SESSION['wu_cart']['user_data']['invoice_nip'] = filterInput($_POST['invoice_nip']);
			$_SESSION['wu_cart']['user_data']['phone'] = filterInput($_POST['phone']);
			if(isset($_POST['email'])) {
				$_SESSION['wu_cart']['user_data']['email'] = trim($_POST['email']);
			}
			$_SESSION['wu_cart']['user_data']['vat'] = isset($_POST['invoice_data'])?'1':'0';
		//	$_SESSION['wu_cart']['user_data']['remarks'] = nl2br(trim($_POST['remarks']));
		
		// zapis kategorii newslettera
			if (!isLoggedIn() && Common::moduleExists('m_newsletter') && !empty($_POST['n_categories'])) {
		 		// dolaczenie klasy Newsletter
		 		include_once('modules/m_newsletter/client/Newsletter.class.php');
		 		$oNewsletter = new Newsletter('m_newsletter', 0, array(), '');
				if($oNewsletter->add($_POST['email'], $_POST['n_categories'], true, true) == -1){
				}
			}
			// - koniec zabawy z newsletterem
			
		} 
	} // end of getUserDataFromPost() method

	
	/**
	 * pobiera informacje o specjanych uprawnieniach użytkownika - przelew 14 dni,
	 * odbór osobisty, zwolnienie z kosztów transportu 
	 * @return array
	 */
	function getSpecialUserPrivileges(){
  	global $aConfig;
  	if(!empty($_SESSION['w_user']) && $_SESSION['w_user']['id'] > 0) {
	  	$sSql = "SELECT allow_14days, allow_personal_reception, allow_free_transport
		  					FROM ".$aConfig['tabls']['prefix']."users_accounts 
		  					WHERE id = ".$_SESSION['w_user']['id'];
  		return Common::GetRow($sSql);
  	}
  	else {
  		return array();
  	}
  } // end of getSpecialUserPrivileges() method
	
	/**
	 * Metoda przelicza ceny produktow dla zalogowanego uzytkownika
	 * 
	 * @return void
	 */
	function loggedInRecalculateProducts() {
		global $aConfig;
    $iUId = $_SESSION['w_user']['id'];
		
    if (isset($_SESSION['wu_cart'])) {
			
			$aUserPriv = $this->getSpecialUserPrivileges();
			if(!empty($aUserPriv) && !empty($_SESSION['wu_cart']['payment']['id'])){
				if($aUserPriv['allow_free_transport'] == '1') {
					$_SESSION['wu_cart']['payment']['cost'] = 0;
				}
				if($aUserPriv['allow_14days'] == '1') {
					$_SESSION['special_user_priv_notif'][] = '14days';
				}
				if($aUserPriv['allow_personal_reception'] == '1') {
					$_SESSION['special_user_priv_notif'][] = 'personal_reception';
				}
			}
			if(!empty($_SESSION['wu_cart']['products'])) {
		    foreach($_SESSION['wu_cart']['products'] as $iId => $aValue) {
          
		    	if ($_SESSION['w_user']['discounts'] > 0 || isset($_SESSION['wu_cart']['discount_code'])) {
		    		// cena podstawowa
						$sSql = "SELECT A.id, C name, A.packet,
														A.price_brutto, A.discount_limit, C.vat, C.product_id
										 FROM ".$aConfig['tabls']['prefix']."products_shadow A
										 JOIN ".$aConfig['tabls']['prefix']."products C
										 ON C.id=A.id
										 WHERE A.id = ".$iId;
						 
						$aData =& Common::GetRow($sSql);
						$aTarrif = $this->getTarrif($iId);
						$aTarrif['packet'] = ($aData['packet']=='1');
						// przeliczenie i formatowanie cen
						$fTMPPromoPrice = Common::formatPrice2($this->getPromoPrice($aData['price_brutto'], $aTarrif, $_SESSION['wu_cart']['products'][$iId]['promo_text'],$aData['discount_limit'],true,false, $this->getSourceDiscountLimit($iId), $iId));
            $_SESSION['wu_cart']['products'][$iId]['price_brutto'] = Common::formatPrice2($aData['price_brutto']);
						
						// przeliczanie cen w przypadku kiedy książka ma załącznik
						$aAttachments = $this->getProductAttachments($iId);
						if (!empty($aAttachments) && is_array($aAttachments)) {
							$aPrices = $this->oOrderItemRecount->getAttProductOrderPrices($aAttachments, $aData['price_brutto'], $fTMPPromoPrice, $aData['vat'], $aTarrif['discount']);
							$_SESSION['wu_cart']['products'][$iId]['price_netto'] = $aPrices['bundle']['price_netto'];
							$_SESSION['wu_cart']['products'][$iId]['promo_price_netto'] = $aPrices['bundle']['promo_price_netto'];
							$_SESSION['wu_cart']['products'][$iId]['promo_price'] = $aPrices['bundle']['promo_price_brutto'];
						} else {
							$_SESSION['wu_cart']['products'][$iId]['price_netto'] = Common::formatPrice2($aData['price_brutto'] / (1 + $aData['vat']/100));
							$_SESSION['wu_cart']['products'][$iId]['promo_price'] = $fTMPPromoPrice;
							$_SESSION['wu_cart']['products'][$iId]['promo_price_netto'] = Common::formatPrice2($_SESSION['wu_cart']['products'][$iId]['promo_price'] / (1 + $aData['vat']/100));
							
						}
						
						/*
						$_SESSION['wu_cart']['products'][$iId]['price_netto'] = Common::formatPrice2($aData['price_brutto'] / (1 + $aData['vat']/100));
						
						$_SESSION['wu_cart']['products'][$iId]['price_brutto'] = Common::formatPrice2($aData['price_brutto']);
						$_SESSION['wu_cart']['products'][$iId]['promo_price_netto'] = Common::formatPrice2($_SESSION['wu_cart']['products'][$iId]['promo_price'] / (1 + $aData['vat']/100));
						*/
						$_SESSION['wu_cart']['products'][$iId]['discount'] = Common::formatPrice2($aTarrif['discount']);
						$_SESSION['wu_cart']['products'][$iId]['discount_currency'] = Common::formatPrice2($aData['price_brutto'] * (Common::formatPrice2($aTarrif['discount']) / 100));
					}
		    }
			}
		  $this->calculateCart();
    }
	} // end of loggedInRecalculateProducts() method


	/**
	 * Metoda przelicza ceny produktow dla zalogowanego uzytkownika
	 * 
	 * @return void
	 */
	function codeRecalculateProducts() {
		global $aConfig;
		
		if (isset($_SESSION['wu_cart'])) {
			
			$aUserPriv = $this->getSpecialUserPrivileges();
			if(!empty($aUserPriv) && !empty($_SESSION['wu_cart']['payment']['id'])){
				if($aUserPriv['allow_free_transport'] == '1') {
					$_SESSION['wu_cart']['payment']['cost'] = 0;
				}
				if($aUserPriv['allow_14days'] == '1') {
					$_SESSION['special_user_priv_notif'][] = '14days';
				}
				if($aUserPriv['allow_personal_reception'] == '1') {
					$_SESSION['special_user_priv_notif'][] = 'personal_reception';
				}
			}
			unset($_SESSION['wu_cart']['old']);
	    foreach($_SESSION['wu_cart']['products'] as $iId => $aValue) {
	    	if ($_SESSION['w_user']['discounts'] > 0 || isset($_SESSION['wu_cart']['discount_code'])) {
	    		// cena podstawowa
					$sSql = "SELECT A.id, C. name, A.packet,
													A.price_brutto, A.discount_limit, C.vat,
													D.user_code_ignore_discount_limit
									 FROM ".$aConfig['tabls']['prefix']."products_shadow A
									 JOIN ".$aConfig['tabls']['prefix']."products C
										 ON C.id=A.id
									 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers D
										 ON C.publisher_id=D.id
									 WHERE A.id = ".$iId;
					 
					$aData =& Common::GetRow($sSql);
					$bIgnoreDiscountLimit = false;
					if ($aData['user_code_ignore_discount_limit'] == '1') {
						$bIgnoreDiscountLimit = true;
					}
					$aTarrif = $this->getTarrif($iId);
					$aTarrif['packet'] = ($aData['packet']=='1');
					
          
					if (isset($_POST['itm'][$aValue['id']]) && !empty($_POST['itm'][$aValue['id']]) && $_POST['itm'][$aValue['id']] != $aValue['quantity']) {
						// zmieniona ilość postem
						$aValue['quantity'] = intval($_POST['itm'][$aValue['id']]);
					}
          
					// przeliczanie starego rabatu
					$aOldP = $this->getPricesWithoutDiscountCode($aData, $aTarrif, $aData['discount_limit'], $iId);
          
					$_SESSION['wu_cart']['old']['promo_price'] += Common::formatPrice2($aOldP['promo_price'] * $aValue['quantity']);
					// przeliczenie i formatowanie cen
					$fTMPPromoPrice = Common::formatPrice2($this->getPromoPrice($aData['price_brutto'], 
																																			$aTarrif, 
																																			$_SESSION['wu_cart']['products'][$iId]['promo_text'],
																																			$aData['discount_limit'], 
																																			false, 
																																			false, 
																																			$this->getSourceDiscountLimit($iId), 
																																			$iId, 
																																			$bIgnoreDiscountLimit));
					
					$_SESSION['wu_cart']['products'][$iId]['price_brutto'] = Common::formatPrice2($aData['price_brutto']);
          
					// przeliczanie cen w przypadku kiedy książka ma załącznik
					$aAttachments = $this->getProductAttachments($iId, $aData['discount_limit']);
					if (!empty($aAttachments) && is_array($aAttachments)) {
						$aPrices = $this->oOrderItemRecount->getAttProductOrderPrices($aAttachments, $aData['price_brutto'], $fTMPPromoPrice, $aData['vat'], $aTarrif['discount']);
						$_SESSION['wu_cart']['products'][$iId]['price_netto'] = $aPrices['bundle']['price_netto'];
						$_SESSION['wu_cart']['products'][$iId]['promo_price_netto'] = $aPrices['bundle']['promo_price_netto'];
						$_SESSION['wu_cart']['products'][$iId]['promo_price'] = $aPrices['bundle']['promo_price_brutto'];
					} else {
						$_SESSION['wu_cart']['products'][$iId]['price_netto'] = Common::formatPrice2($aData['price_brutto'] / (1 + $aData['vat']/100));
						$_SESSION['wu_cart']['products'][$iId]['promo_price'] = $fTMPPromoPrice;
						$_SESSION['wu_cart']['products'][$iId]['promo_price_netto'] = Common::formatPrice2($_SESSION['wu_cart']['products'][$iId]['promo_price'] / (1 + $aData['vat']/100));
					}
					
					/*
					$_SESSION['wu_cart']['products'][$iId]['price_netto'] = Common::formatPrice2($aData['price_brutto'] / (1 + $aData['vat']/100));
					
					$_SESSION['wu_cart']['products'][$iId]['price_brutto'] = Common::formatPrice2($aData['price_brutto']);
					$_SESSION['wu_cart']['products'][$iId]['promo_price_netto'] = Common::formatPrice2($_SESSION['wu_cart']['products'][$iId]['promo_price'] / (1 + $aData['vat']/100));
					*/
					$_SESSION['wu_cart']['products'][$iId]['discount'] = Common::formatPrice2($aTarrif['discount']);
					$_SESSION['wu_cart']['products'][$iId]['discount_currency'] = Common::formatPrice2($aData['price_brutto'] * (Common::formatPrice2($aTarrif['discount']) / 100));
					}
	    	}

	    $this->calculateCart();
    }
	} // end of codeRecalculateProducts() method
	
	
	
	function getPricesWithoutDiscountCode($aData, $aTarrif, $sDiscountLimit, $iId) {
		$aPricesWDC = array();
		$sPromoText = '';
		$aDiscTMP = $_SESSION['wu_cart']['discount_code'];
		unset($_SESSION['wu_cart']['discount_code']);
		
		$fTMPPromoPrice = Common::formatPrice2($this->getPromoPrice($aData['price_brutto'], 
																																$aTarrif, 
																																$sPromoText,
																																$sDiscountLimit, 
																																false, 
																																false, 
																																$this->getSourceDiscountLimit($iId), 
																																$iId, 
																																false, 
                                                                TRUE));
					
		
		$aPricesWDC['price_brutto'] = Common::formatPrice2($aData['price_brutto']);

		// przeliczanie cen w przypadku kiedy książka ma załącznik
		$aAttachments = $this->getProductAttachments($iId);
		if (!empty($aAttachments) && is_array($aAttachments)) {
			$aPrices = $this->oOrderItemRecount->getAttProductOrderPrices($aAttachments, $aData['price_brutto'], $fTMPPromoPrice, $aData['vat'], $aTarrif['discount']);
			$aPricesWDC['price_netto'] = $aPrices['bundle']['price_netto'];
			$aPricesWDC['promo_price_netto'] = $aPrices['bundle']['promo_price_netto'];
			$aPricesWDC['promo_price'] = $aPrices['bundle']['promo_price_brutto'];
		} else {
			$aPricesWDC['price_netto'] = Common::formatPrice2($aData['price_brutto'] / (1 + $aData['vat']/100));
			$aPricesWDC['promo_price'] = $fTMPPromoPrice;
			$aPricesWDC['promo_price_netto'] = Common::formatPrice2($aPricesWDC['promo_price'] / (1 + $aData['vat']/100));
		}
		
		$aPricesWDC['discount'] = Common::formatPrice2($aTarrif['discount']);
		$aPricesWDC['discount_currency'] = Common::formatPrice2($aData['price_brutto'] * (Common::formatPrice2($aTarrif['discount']) / 100));
		//dump($aPricesWDC);
		$_SESSION['wu_cart']['discount_code'] = $aDiscTMP;
		return $aPricesWDC;
	}
	
	/**
	 * Metoda przelicza ceny produktow dla wylogowanego uzytkownika
	 * 
	 * @return void
	 */
	function loggedOutRecalculateProducts() {
		global $aConfig;
		
		if (isset($_SESSION['wu_cart'])) {
			if(!empty($_SESSION['wu_cart']['payment']['id'])){
				if($_SESSION['wu_cart']['payment']['ptype']=='bank_14days'){
					unset($_SESSION['wu_cart']['payment']);
				}
			}
			$this->CalculateAgain();
    }
	} // end of loggedOutRecalculateProducts() method
	
  
 /**
 * Metoda ponownie przelicza koszyk
 */
  public function CalculateAgain() {

    if (!empty($_SESSION['wu_cart']['products'])) {
      foreach ($_SESSION['wu_cart']['products'] as $iId => $aValue) {
        // cena podstawowa
        $sSql = "SELECT A.id, C.name, A.packet,
													C.price_brutto, A.discount_limit, C.vat
									 FROM " . $aConfig['tabls']['prefix'] . "products_shadow A
									 JOIN " . $aConfig['tabls']['prefix'] . "products C
									 ON C.id=A.id
									 WHERE A.id = " . $iId;

        $aData = & Common::GetRow($sSql);
        $aTarrif = $this->getTarrif($iId);
        $aTarrif['packet'] = ($aData['packet'] == '1');
        // przeliczenie i formatowanie cen
        $fTMPPromoPrice = Common::formatPrice2($this->getPromoPrice($aData['price_brutto'], $aTarrif, $_SESSION['wu_cart']['products'][$iId]['promo_text'], $aData['discount_limit'], false, true, $this->getSourceDiscountLimit($iId), $iId));
        $_SESSION['wu_cart']['products'][$iId]['price_brutto'] = Common::formatPrice2($aData['price_brutto']);


        // przeliczanie cen w przypadku kiedy książka ma załącznik
        $aAttachments = $this->getProductAttachments($iId);
        if (!empty($aAttachments) && is_array($aAttachments)) {
          $aPrices = $this->oOrderItemRecount->getAttProductOrderPrices($aAttachments, $aData['price_brutto'], $fTMPPromoPrice, $aData['vat'], $aTarrif['discount']);
          $_SESSION['wu_cart']['products'][$iId]['price_netto'] = $aPrices['bundle']['price_netto'];
          $_SESSION['wu_cart']['products'][$iId]['promo_price_netto'] = $aPrices['bundle']['promo_price_netto'];
          $_SESSION['wu_cart']['products'][$iId]['promo_price'] = $aPrices['bundle']['promo_price_brutto'];
        } else {
          $_SESSION['wu_cart']['products'][$iId]['price_netto'] = Common::formatPrice2($aData['price_brutto'] / (1 + $aData['vat'] / 100));
          $_SESSION['wu_cart']['products'][$iId]['promo_price'] = $fTMPPromoPrice;
          $_SESSION['wu_cart']['products'][$iId]['promo_price_netto'] = Common::formatPrice2($_SESSION['wu_cart']['products'][$iId]['promo_price'] / (1 + $aData['vat'] / 100));
        }

        $_SESSION['wu_cart']['products'][$iId]['price_brutto'] = Common::formatPrice2($aData['price_brutto']);


        $_SESSION['wu_cart']['products'][$iId]['discount'] = Common::formatPrice2($aTarrif['discount']);
        $_SESSION['wu_cart']['products'][$iId]['discount_currency'] = Common::formatPrice2($aData['price_brutto'] * (Common::formatPrice2($aTarrif['discount']) / 100));
      }
    }
    $this->calculateCart();
  } 
  
	
	/**
	 * Metoda dodaje produkt / zwieksza jego liczbe w koszyku
	 *
	 * @param	string	$sType	- typ produktu
	 * @param	integer	$iId	- Id produktu
	 * @return	void
	 */
	function addProduct($iId,$bSave=true,$iQuantity=1,$bLogin=false,$omitCalculations = false) {
		global $aConfig;
		if ($this->productExists($iId)) {
			if ($this->isAlreadyInCart($iId)) {
				if($bSave){
					// w koszyku juz znajduje sie ten produkt - zwiekszenie liczby o 1
					$_SESSION['wu_cart']['products'][$iId]['quantity'] += $iQuantity; 
					$_SESSION['wu_cart']['last_item_name'] = $_SESSION['wu_cart']['products'][$iId]['name'];
					$_SESSION['wu_cart']['last_item_link'] = $_SESSION['wu_cart']['products'][$iId]['link'];
					 
					$this->storeAddToCart($iId,$_SESSION['wu_cart']['products'][$iId]['quantity']);
					$_SESSION['wu_cart']['just_added'] = 1;
				}
			}
			else {
					// cena podstawowa
					$sSql = "SELECT A.id, A.title AS name, C.source, C.packet, A.price_brutto, A.discount_limit, A.packet,
													C.vat,A.shipment_date,A.shipment_time, A.prod_status, B.name AS category
									 FROM ".$aConfig['tabls']['prefix']."products_shadow A
									 JOIN ".$aConfig['tabls']['prefix']."products C
									 ON C.id=A.id
									 JOIN ".$aConfig['tabls']['prefix']."menus_items B
									 ON C.page_id = B.id
									 WHERE A.id = ".$iId;
					 
				$aData =& Common::GetRow($sSql);
				
				// emergency exit
				if(empty($aData))
					return false;		

				$sProductId=$iId;
				
							
				$_SESSION['wu_cart']['products'][$sProductId]['id']=$iId;
				$_SESSION['wu_cart']['products'][$sProductId]['quantity'] = $iQuantity;
				$_SESSION['wu_cart']['products'][$sProductId]['name'] = $aData['name'];
				$_SESSION['wu_cart']['products'][$sProductId]['source'] = $aData['source'];
				$_SESSION['wu_cart']['products'][$sProductId]['vat'] = $aData['vat'];
				$_SESSION['wu_cart']['products'][$sProductId]['category'] = $aData['category'];
				$_SESSION['wu_cart']['products'][$sProductId]['link'] = createProductLink($aData['id'], $aData['name']);
				
				// link usun z koszyka
				// ponowne uzupełnienie danymi
				if (!isset($aConfig['_tmp']['search_symbol'])) {
					$aConfig['_tmp']['search_symbol'] = getModulePageLink('m_szukaj');
				}
				if (!isset($aConfig['_tmp']['accounts_symbol'])) {
					$aConfig['_tmp']['accounts_symbol'] = getModulePageLink('m_konta');
				}
				if (!isset($aConfig['_tmp']['cart_symbol'])) {
					$aConfig['_tmp']['cart_symbol'] = getModulePageLink('m_zamowienia');
				}
				if (!isset($aConfig['_tmp']['repository_symbol'])) {
					$aConfig['_tmp']['repository_symbol'] = getModulePageLink('m_zamowienia', 'przechowalnia');
				}

				$_SESSION['wu_cart']['products'][$sProductId]['delete_link'] = createLink($aConfig['_tmp']['cart_symbol'], 'id'.$aData['id'], 'delete');
				$_SESSION['wu_cart']['products'][$sProductId]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'], 'id'.$aData['id'], 'move_from_cart');
				$aTarrif = $this->getTarrif($iId);
				$aTarrif['packet'] = ($aData['packet']=='1');
				// przeliczenie i formatowanie cen
				$fTMPPromoPrice = Common::formatPrice2($this->getPromoPrice($aData['price_brutto'], $aTarrif, $_SESSION['wu_cart']['products'][$sProductId]['promo_text'],$aData['discount_limit'],$bLogin, false, $this->getSourceDiscountLimit($iId), $iId));
				
				//$aPromo=$this->getPromo($aData);
				//$_SESSION['wu_cart']['products'][$sProductId]['promo_price']=Common::formatPrice2($aPromo['promo_price']);
				$_SESSION['wu_cart']['products'][$sProductId]['price_brutto'] = Common::formatPrice2($aData['price_brutto']);
				
				// przeliczanie cen w przypadku kiedy książka ma załącznik
				$aAttachments = $this->getProductAttachments($iId);
				if (!empty($aAttachments) && is_array($aAttachments)) {
					$aPrices = $this->oOrderItemRecount->getAttProductOrderPrices($aAttachments, $aData['price_brutto'], $fTMPPromoPrice, $aData['vat'], $aTarrif['discount']);
					$_SESSION['wu_cart']['products'][$sProductId]['price_netto'] = $aPrices['bundle']['price_netto'];
					$_SESSION['wu_cart']['products'][$sProductId]['promo_price_netto'] = $aPrices['bundle']['promo_price_netto'];
					$_SESSION['wu_cart']['products'][$sProductId]['promo_price'] = $aPrices['bundle']['promo_price_brutto'];
				} else {
					$_SESSION['wu_cart']['products'][$sProductId]['price_netto'] = Common::formatPrice2($aData['price_brutto'] / (1 + $aData['vat']/100));
					$_SESSION['wu_cart']['products'][$sProductId]['promo_price'] = $fTMPPromoPrice;
					$_SESSION['wu_cart']['products'][$sProductId]['promo_price_netto'] = Common::formatPrice2($_SESSION['wu_cart']['products'][$sProductId]['promo_price'] / (1 + $aData['vat']/100));
				}
				
				$_SESSION['wu_cart']['products'][$sProductId]['discount'] = Common::formatPrice2($aTarrif['discount']);
				$_SESSION['wu_cart']['products'][$sProductId]['discount_currency'] = Common::formatPrice2($aData['price_brutto'] * (Common::formatPrice2($aTarrif['discount']) / 100));
				
				$_SESSION['wu_cart']['products'][$sProductId]['packet'] = $aData['packet'];
				
				$aShipmentTimes =& $this->getShipmentTimes();
				if($aData['prod_status'] == '3'){
					$_SESSION['wu_cart']['products'][$sProductId]['preview'] = true;
					$_SESSION['wu_cart']['products'][$sProductId]['shipment_date'] = $aData['shipment_date'];
					
					$_SESSION['wu_cart']['products'][$sProductId]['shipment_days'] = $aShipmentTimes['3']['shipment_days'];
					$_SESSION['wu_cart']['products'][$sProductId]['transport_days'] = $aShipmentTimes['3']['transport_days'];
				} else {
					$_SESSION['wu_cart']['products'][$sProductId]['preview'] = false;
					$_SESSION['wu_cart']['products'][$sProductId]['shipment_days'] = $aShipmentTimes[$aData['shipment_time']]['shipment_days'];
					$_SESSION['wu_cart']['products'][$sProductId]['transport_days'] = $aShipmentTimes[$aData['shipment_time']]['transport_days'];
				}
                $pShipmentTimes = new \orders\Shipment\ShipmentTime();
                $_SESSION['wu_cart']['products'][$sProductId]['shipment_time'] = $pShipmentTimes->getShipmentTime($aData['id'], $aData['shipment_time']);

				$_SESSION['wu_cart']['last_item_link'] = $_SESSION['wu_cart']['products'][$sProductId]['link'];
				$_SESSION['wu_cart']['last_item_name'] = $_SESSION['wu_cart']['products'][$sProductId]['name'];
				$_SESSION['wu_cart']['products'][$sProductId]['image'] =getItemImage('products_images',array('product_id'=> $aData['id']),'__t_');
				
				
				if($bSave){
					$_SESSION['wu_cart']['just_added'] = 1;
					$this->storeAddToCart($iId,1);
				}
			}
			if ($omitCalculations === false)
			{
				$this->calculateCart();
            }
			return true;
		}
		return false;
	} // end of addProduct() method


	/**
	 * Metoda sprawdza czy produkt o podanym typie i Id istnieje
	 *
	 * @param	string	$sType	- typ produktu
	 * @param	integer	$iId	- Id produktu
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function productExists($iId) {
		global $aConfig;

		$iAllowAddUnavailable = 0;
		if (isset($_SESSION['w_user']['id']) && $_SESSION['w_user']['id'] > 0) {
			$sSql = 'SELECT allow_add_unavailable FROM users_accounts WHERE id = '.intval($_SESSION['w_user']['id']);
			$iAllowAddUnavailable = Common::GetOne($sSql);
		}

		$availableSQL = " AND
						 			 (A.prod_status = '1' OR A.prod_status = '3')
						 			 ";
		if ($iAllowAddUnavailable == '1') {
			$availableSQL = '';
		}

		$sSql = "SELECT A.id
						 FROM ".$aConfig['tabls']['prefix']."products_shadow A
						 WHERE A.id = ".$iId."
						 ".$availableSQL;
		return  ((double) Common::GetOne($sSql)) > 0;
	} // end of productExists() function

function storeAddToCart($iId,$iQuantity) {
		global $aConfig;
		
		if(isLoggedIn()){
			storeAddToDB($iId,$iQuantity);
		} else {
			serializeCartToCookie();
		}
	}
	
	function storeRemoveFromCart($iId) {
		global $aConfig;
		
		if(isLoggedIn()){
			$sSql = "DELETE	FROM ".$aConfig['tabls']['prefix']."orders_cart
								WHERE user_id = ".$_SESSION['w_user']['id']." AND product_id =".$iId;
			Common::Query($sSql);
		} else {
			serializeCartToCookie();
		}
	}
	
	/**
	 * Metoda zwraca czy wybrano metode transportu
	 * 
	 * @return	bool	- true: wybrano; false: niewybrano
	 */
	function transportIsChosen() {
		return (isset($_SESSION['wu_cart']['payment']['id']) || isset($_POST['payment_type']) || isset($_POST['payment_id'])) ;
	}	// end of transportIsChosen() method
	
	/**
	 * Metoda zwraca aktualny cennik dla produktu 
	 * @param $iId - id produktu
	 * @return array cennik
	 */
	function &getTarrif($iId) {
		global $aConfig;

		if (isset($this->tarrifsTMP[$iId]) && !empty($this->tarrifsTMP[$iId])) {
			return $this->tarrifsTMP[$iId];
		} else {
			$sSql = "SELECT promotion_id, discount, price_brutto, price_netto, discount_value, product_id
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1";
			$data = Common::GetRow($sSql);
			$this->tarrifsTMP[$iId] = $data;
			return $data;
		}
	} // end of getTarrif()
	
		/**
	 * Metoda zwraca bazowy cennik dla produktu 
	 * @param $iId - id produktu
	 * @return array cennik
	 */
	function &getBaseTarrif($iId) {
		global $aConfig;
		$sSql = "SELECT promotion_id, discount, price_brutto, price_netto, discount_value
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE isdefault = '1'
						LIMIT 0,1";
		return Common::GetRow($sSql);
	} // end of getBaseTarrif()

  /**
   * Metoda pobiera nazwę promocji do zaprezentowania w koszyku
   * 
   * @param int $iPromoId
   * @return string
   */
  private function getPromoName($iPromoId) {
    
    $sSql = "SELECT name FROM products_promotions WHERE id = ".$iPromoId;
    return Common::GetOne($sSql);
  }
  
  /**
   * Metoda ustawia w przekazanym obiekcie $oPr klasy Promotions informacje o aktualnie wybranej promocji
   * 
   * @param object $oPr
   * @param int $iProductId
   * @param array $aMainDiscountType
   * @param bool $bLimit
   * @param int $iCDiscount
   * @param array $aTarrifDiscount
   * @return void
   */
  private function getPromoPriceDiscountInfo($oPr, $iProductId, $aMainDiscountType, $bLimit, $iCDiscount, $aTarrifDiscount) {
    
      // -----
      if ($bLimit == true) {
        if ($aMainDiscountType[1] == 'additional') {
          $fDiscountCode = $iCDiscount - $aTarrifDiscount;
        } else {
          $fDiscountCode = $iCDiscount;
        }
      } else {
        $fDiscountCode = $_SESSION['wu_cart']['discount_code']['code_discount'];
      }

      if ($fDiscountCode > 0.00) {
        if ($aMainDiscountType[1] != 'additional') {
          // promocja nie łączy się z innymi, jeśli to rabat inny niz doliczany
          $oPr->clearProductPromotions($iProductId);
        }

        // zastosowanie rabatu kodu rabatowego
        $oPr->addProductPromotion($iProductId, $aMainDiscountType[0], $fDiscountCode, $aMainDiscountType[1], $bLimit);
      }
      // -----
  }// end of getPromoPriceDiscountInfo() method


	/**
	 * @return \orders\MarkUpCookie
	 */
	private function getMarkUpCookeInstance() {
		if (!isset($this->markUpCookie)) {
			$this->markUpCookie = new \orders\MarkUpCookie();
		}

		return $this->markUpCookie;
	}


  /**
	 * Metoda oblicza cenę promocyjną
   * 
	 * @param $fPriceBrutto - cena brutto produktu
	 * @param $aTarrif - tablica z cennikiem
	 * @param $sPriceLang - referencja do stringa do ktorego zostanie zwrócony lang z typem ceny
	 * @return float - cena promocyjna
	 */
	function getPromoPrice($fPriceBrutto, &$aTarrif, &$sPriceLang, $fPublisherLimit=0, $bLogin=false,$bLogout=false, $fSourceLimit, $iProductId, $bIgnoreLimit=false, $bTestCalculate = false){
		global $aConfig;
		$markUpCookie = $this->getMarkUpCookeInstance();

		$sPromoText='';
		$fPromoPrice=0;
		if ($bTestCalculate == FALSE) {
			$this->oPromotions->clearProductPromotions($iProductId);
		}
		$aMainDiscountType = array();
		$bLimit = false;


		if (($bIgnoreLimit == true || $_SESSION['wu_cart']['discount_code']['ignore_discount_limit'] === '1') && ($fPublisherLimit > 0.00 || $fSourceLimit > 0.00) && isset($_SESSION['wu_cart']['discount_code']['code_discount'])) {
			// jeśli ustawione jest ignorowanie limitu i limity zostay zdefiniowane i wprowadzony został kod rabatowy
			$fSourceLimit = 0;
			$fPublisherLimit = 0;
		}

		// użyj ceny z cennika jeśli jest różna od ceny glownej produktu
		if ($aTarrif['price_brutto'] != $fPriceBrutto) {
			$fPromoPrice = $aTarrif['price_brutto'];
			$sPromoText = $aConfig['lang']['common']['cena_w_ksiegarni'];
		} else {
			$fPromoPrice = $fPriceBrutto;
		}

		if ($bTestCalculate == FALSE) {
			if ($aTarrif['promotion_id'] > 0) {
				// promocja
				$this->oPromotions->addProductPromotion($iProductId, 'promotion_discount', $aTarrif['discount'], '', false, $this->getPromoName($aTarrif['promotion_id']));
			} else {
				// rabat ogólny
				$this->oPromotions->addProductPromotion($iProductId, 'general_discount', $aTarrif['discount'], '', false);
			}
		}
		// przeliczamy rabat z kodu rabatowego
		if (!empty($_SESSION['wu_cart']['discount_code']) && ($this->oPromotions->oPromotion->CodeDiscount->checkBookDiscount($_SESSION['wu_cart']['discount_code'], $iProductId) > 0) && !$aTarrif['packet']) {
			if (isset($_SESSION['wu_cart']['discount_code']['code_discount']) && $_SESSION['wu_cart']['discount_code']['discount_price_after_promo'] === '1') {
				$aMainDiscountType = array('code_discount', 'additional');
				// ceny obliczane od ceny Profit24.pl, a nie ceny podstawowej
				// rabat to suma rabatu kodu rabatowego i rabatu cennika
				$iCodeDiscount = $aTarrif['discount'] + $_SESSION['wu_cart']['discount_code']['code_discount'];
				if ($iCodeDiscount > 100) {
					//obliczamy rabat który trzeba ustawić aby wyszło 0,01 zł
					// !! UWAGA przy cenach ponad 150 zł za ksiaze bedzie pojawiała się już nieprawidłowa cena 0,02 zł
					$iCodeDiscount = 100 - round(1 / $fPriceBrutto, 2);
					$bLimit = true;
				}
			} else {
				$aMainDiscountType = array('code_discount', '');
				$iCodeDiscount = $_SESSION['wu_cart']['discount_code']['code_discount'];
			}

			if ($fPublisherLimit > 0 && $iCodeDiscount > $fPublisherLimit) {
				$bLimit = true;
				$iCDiscount = $fPublisherLimit;
			} else {
				$iCDiscount = $iCodeDiscount;
			}

			if ($fSourceLimit > 0 && $iCDiscount > $fSourceLimit) {
				$bLimit = true;



				$iCDiscount = $fSourceLimit;
			} else {
				$iCDiscount = $iCDiscount; // dla jasności
			}

			$fCPrice = $fPriceBrutto - $this->formatPrice2($fPriceBrutto * ($iCDiscount / 100));

			if ($fPromoPrice != 0) {
				if ($fCPrice < $fPromoPrice) {
					$fPromoPrice = $fCPrice;
					$sPromoText = $aConfig['lang']['common']['cena_kod_rabatowy'];

					if ($bTestCalculate == FALSE) {
						// wrzucenie informacji o rabatach produktu
						$this->getPromoPriceDiscountInfo($this->oPromotions, $iProductId, $aMainDiscountType, $bLimit, $iCDiscount, $aTarrif['discount']);
					}

					$aTarrif['discount'] = $iCDiscount;
				} else {

					if ($bTestCalculate == FALSE) {
//            $this->oPromotions->deleteProductPromotion($iProductId, $aMainDiscountType[0]);
						$this->getPromoPriceDiscountInfo($this->oPromotions, $iProductId, $aMainDiscountType, $bLimit, $iCDiscount, $aTarrif['discount']);
					}
				}
			} else {
				$fPromoPrice = $fCPrice;
				$sPromoText = $aConfig['lang']['common']['cena_kod_rabatowy'];

				if ($bTestCalculate == FALSE) {
					// wrzucenie informacji o rabatach produktu
					$this->getPromoPriceDiscountInfo($this->oPromotions, $iProductId, $aMainDiscountType, $bLimit, $iCDiscount, $aTarrif['discount']);
				}

				$aTarrif['discount'] = $iCDiscount;
			}
		}

		// cena z rabatu uzytkownika - wykluczamy pakiety
		if (!$bLogout) {
			if (($bLogin || isLoggedIn()) && ($_SESSION['w_user']['discounts'] > 0) && !$aTarrif['packet']) {
				$bLimit = false;

				if ($fPublisherLimit > 0 && $_SESSION['w_user']['discounts'] > $fPublisherLimit) {
					$iDiscount = $fPublisherLimit;
					$bLimit = true;
				} else {
					$iDiscount = $_SESSION['w_user']['discounts'];
				}
				if ($fSourceLimit > 0 && $iDiscount > $fSourceLimit) {
					$iDiscount = $fSourceLimit;
					$bLimit = true;
				} else {
					$iDiscount = $iDiscount; // dla jasności zapisu
				}

				$fUserPrice = $fPriceBrutto - $this->formatPrice2($fPriceBrutto * ($iDiscount / 100));
				if ($fPromoPrice != 0) {
					if ($fUserPrice < $fPromoPrice) {
						$fPromoPrice = $fUserPrice;
						$sPromoText = $aConfig['lang']['common']['cena_uzytkownika'];
						$aTarrif['discount'] = $iDiscount;

						if ($bTestCalculate == FALSE) {
							$this->oPromotions->clearProductPromotions($iProductId);
							$this->oPromotions->addProductPromotion($iProductId, 'user_discount', $iDiscount, '', $bLimit);
						}
					}
				} else {
					$fPromoPrice = $fUserPrice;
					$sPromoText = $aConfig['lang']['common']['cena_uzytkownika'];
					$aTarrif['discount'] = $iDiscount;

					if ($bTestCalculate == FALSE) {
						$this->oPromotions->clearProductPromotions($iProductId);
						$this->oPromotions->addProductPromotion($iProductId, 'user_discount', $iDiscount, '', $bLimit);
					}
				}
			}
		}


		$aResultTarrif = $markUpCookie->getMarkup($fPriceBrutto, $aTarrif, $iDiscount);
		if ($markUpCookie->isMarkUpActive() && !isset($_SESSION['wu_cart']['discount_code']['code_discount'])) {
			$this->oPromotions->clearProductPromotions($iProductId);
			$fPromoPrice = $aResultTarrif['price_brutto'];
			$aTarrif = $aResultTarrif;
		}

		if ($bTestCalculate == FALSE) {
		  $_SESSION['wu_cart']['products'][$iProductId]['promotions'] = $this->oPromotions->aItemPromotions[$iProductId];
		  $this->oPromotions->clearProductPromotions($iProductId);
		}


		$sPriceLang=$sPromoText; //echo ($fPromoPrice);

		return $fPromoPrice;
	} // end of getPromoPrice() method

  
  /**
	 * Metoda oblicza cenę promocyjną
   * 
	 * @param $fPriceBrutto - cena brutto produktu
	 * @param $aTarrif - tablica z cennikiem
	 * @param $sPriceLang - referencja do stringa do ktorego zostanie zwrócony lang z typem ceny
	 * @return float - cena promocyjna
	 */
	function getPromoPrice_TEST($fPriceBrutto, &$aTarrif, &$sPriceLang, $fPublisherLimit=0, $bLogin=false,$bLogout=false, $fSourceLimit, $iProductId, $bIgnoreLimit=false){
		global $aConfig;
		$sPromoText='';
		$fPromoPrice=0;
    
		if ($bIgnoreLimit == true && ($fPublisherLimit > 0.00 || $fSourceLimit > 0.00) && isset($_SESSION['wu_cart']['discount_code']['code_discount'])) {
			// jesli ignorowany jest limit to limit rabatu rowny zero
			$fSourceLimit = 0;
			$fPublisherLimit = 0;
		}
		
		// cena z cennika
		if($aTarrif['price_brutto'] != $fPriceBrutto){
			$fPromoPrice = $aTarrif['price_brutto'];
			$sPromoText = $aConfig['lang']['common']['cena_w_ksiegarni'];
		}
		// cena z kodu rabatowego - wykluczamy pakiety
		if(!empty($_SESSION['wu_cart']['discount_code']) && ($this->oPromotions->oPromotion->CodeDiscount->checkBookDiscount($_SESSION['wu_cart']['discount_code'], $iProductId) > 0) && !$aTarrif['packet']){
      if (isset($_SESSION['wu_cart']['discount_code']['code_discount']) && $_SESSION['wu_cart']['discount_code']['discount_price_after_promo'] === '1') {
        // ceny obliczane od ceny Profit24.pl, a nie ceny podstawowej
        // rabat to suma rabatu kodu rabatowego i rabatu cennika
        $iCodeDiscount = $aTarrif['discount'] + $_SESSION['wu_cart']['discount_code']['code_discount'];
      } else {
        $iCodeDiscount = $_SESSION['wu_cart']['discount_code']['code_discount']; 
      }
      
			$iCDiscount = ($fPublisherLimit > 0 && $iCodeDiscount > $fPublisherLimit) ? $fPublisherLimit : $iCodeDiscount;
			$iCDiscount = ($fSourceLimit > 0 && $iCDiscount > $fSourceLimit) ? $fSourceLimit : $iCDiscount;
			$fCPrice = $fPriceBrutto - $this->formatPrice2($fPriceBrutto * ($iCDiscount / 100));
      
			if($fPromoPrice != 0){
				if($fCPrice < $fPromoPrice){
					$fPromoPrice = $fCPrice;
					$sPromoText = $aConfig['lang']['common']['cena_kod_rabatowy'];
					$aTarrif['discount'] = $iCDiscount;
				}
			} else {
				$fPromoPrice = $fCPrice;
				$sPromoText = $aConfig['lang']['common']['cena_kod_rabatowy'];
				$aTarrif['discount'] = $iCDiscount;
			}
		}
		// cena z rabatu uzytkownika - wykluczamy pakiety
		if(!$bLogout){
			if(($bLogin || isLoggedIn()) && ($_SESSION['w_user']['discounts'] > 0) && !$aTarrif['packet']){
				$iDiscount = ($fPublisherLimit > 0 && $_SESSION['w_user']['discounts'] > $fPublisherLimit)?$fPublisherLimit:$_SESSION['w_user']['discounts'];
				$iDiscount = ($fSourceLimit > 0 && $iDiscount > $fSourceLimit) ? $fSourceLimit : $iDiscount;
				$fUserPrice = $fPriceBrutto - $this->formatPrice2($fPriceBrutto * ($iDiscount / 100));
				if($fPromoPrice != 0){
					if($fUserPrice < $fPromoPrice){
						$fPromoPrice = $fUserPrice;
						$sPromoText = $aConfig['lang']['common']['cena_uzytkownika'];
						$aTarrif['discount'] = $iDiscount;
					}
				} else {
					$fPromoPrice = $fUserPrice;
					$sPromoText = $aConfig['lang']['common']['cena_uzytkownika'];
					$aTarrif['discount'] = $iDiscount;
				}
			}
		}
    
		$sPriceLang=$sPromoText; //echo ($fPromoPrice);
		return $fPromoPrice;
	} // end of getPromoPrice() method
  
    
  /**
	 * Metoda oblicza czenę promocyjną
   * 
   * @deprecated since version 1.0
	 * @param $fPriceBrutto - cena brutto produktu
	 * @param $aTarrif - tablica z cennikiem
	 * @param $sPriceLang - referencja do stringa do ktorego zostanie zwrócony lang z typem ceny
	 * @return float - cena promocyjna
	function OLD_getPromoPrice($fPriceBrutto, &$aTarrif, &$sPriceLang, $fPublisherLimit=0, $bLogin=false,$bLogout=false, $fSourceLimit, $iProductId, $bIgnoreLimit=false){
		global $aConfig;
		$sPromoText='';
		$fPromoPrice=0;
		
		if ($bIgnoreLimit == true && isset($_SESSION['wu_cart']['discount_code']['code_discount'])) {
			// jesli ignorowany jest limit to limity rabatów sa równe rabatowi z kodu rabatowego
			$fSourceLimit = $_SESSION['wu_cart']['discount_code']['code_discount'];
			$fPublisherLimit = $_SESSION['wu_cart']['discount_code']['code_discount'];
		}
		
		// cena z cennika
		if($aTarrif['price_brutto'] != $fPriceBrutto){
			$fPromoPrice = $aTarrif['price_brutto'];
			$sPromoText = $aConfig['lang']['common']['cena_w_ksiegarni'];
		}
		// cena z kodu rabatowego - wykluczamy pakiety
		if(!empty($_SESSION['wu_cart']['discount_code']) && $this->checkBookDiscount($_SESSION['wu_cart']['discount_code'], $iProductId) && !$aTarrif['packet']){
			$iCDiscount = ($fPublisherLimit > 0 && $_SESSION['wu_cart']['discount_code']['code_discount'] > $fPublisherLimit)?$fPublisherLimit:$_SESSION['wu_cart']['discount_code']['code_discount'];
			$iCDiscount = ($fSourceLimit > 0 && $iCDiscount > $fSourceLimit) ? $fSourceLimit : $iCDiscount;
			$fCPrice = $fPriceBrutto - $this->formatPrice2($fPriceBrutto * ($iCDiscount / 100));
			if($fPromoPrice != 0){
				if($fCPrice < $fPromoPrice){
					$fPromoPrice = $fCPrice;
					$sPromoText = $aConfig['lang']['common']['cena_kod_rabatowy'];
					$aTarrif['discount'] = $iCDiscount;
				}
			} else {
				$fPromoPrice = $fCPrice;
				$sPromoText = $aConfig['lang']['common']['cena_kod_rabatowy'];
				$aTarrif['discount'] = $iCDiscount;
			}
		} 
		// cena z rabatu uzytkownika - wykluczamy pakiety
		if(!$bLogout){
			if(($bLogin || isLoggedIn()) && ($_SESSION['w_user']['discounts'] > 0) && !$aTarrif['packet']){
				$iDiscount = ($fPublisherLimit > 0 && $_SESSION['w_user']['discounts'] > $fPublisherLimit)?$fPublisherLimit:$_SESSION['w_user']['discounts'];
				$iDiscount = ($fSourceLimit > 0 && $iDiscount > $fSourceLimit) ? $fSourceLimit : $iDiscount;
				$fUserPrice = $fPriceBrutto - $this->formatPrice2($fPriceBrutto * ($iDiscount / 100));
				if($fPromoPrice != 0){
					if($fUserPrice < $fPromoPrice){
						$fPromoPrice = $fUserPrice;
						$sPromoText = $aConfig['lang']['common']['cena_uzytkownika'];
						$aTarrif['discount'] = $iDiscount;
					}
				} else {
					$fPromoPrice = $fUserPrice;
					$sPromoText = $aConfig['lang']['common']['cena_uzytkownika'];
					$aTarrif['discount'] = $iDiscount;
				}
			}
		}
		$sPriceLang=$sPromoText; //echo ($fPromoPrice);
		return $fPromoPrice;
	} // end of OLD_getPromoPrice() method
  */
  
  /**
   * Metoda pobiera informacje konfiguracyjne, na temat czasów dostarczenia przesyłki
   * 
   * @global array $aConfig
   * @return array
   */
	function getShipmentTimes(){
	global $aConfig;	
		// pobranie z bazy danych edytowanego rekordu
		$sSql = "SELECT shipment_time,shipment_days,transport_days
						 FROM ".$aConfig['tabls']['prefix']."orders_shipment_times";
		return Common::GetAssoc($sSql);
	}
	
  
  /**
   * Metoda pobiera dni wolne od pracy
   * 
   * @return array
   */
	function getFreeDays(){
		$sSql = "SELECT UNIX_TIMESTAMP(free_day)
							 FROM ".$aConfig['tabls']['prefix']."orders_free_days";
		return Common::GetCol($sSql);
	}
  
  
  /**
   * Metoda pobiera wylicza datę wysyłki zamówienia, uwzględniając, wolne dni, oraz zapowiedź
   * 
   * @global array $aConfig
   * @param int $iDays - dni do wysyłki pozycji
   * @param array $aFreeDays - tablica dni wolnych
   * @param int $iPreviewTime - data wydania zapowiedzi
   * @param int $iPreviewAdd - ile dni powinno być dodanych do daty wyd. zapowiedzi
   * @return date
   */
	function getDateAfterWorkingDays($iDays,&$aFreeDays,$iPreviewTime=0,$iPreviewAdd=0){
	global $aConfig;
		$iDate = time();
		$iDaysCount=0;
		$aDate = getdate($iDate);
		if($aDate['wday'] == 0 || $aDate['wday'] == 6 || in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
					++$iDays;
				}	
		// dopóki nie osiągniemy potrzebnej ilości dni
		while($iDaysCount < $iDays){
			// dodaj dzień
			$iDate += (24 * 60 * 60);
			$aDate = getdate($iDate);
			// jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
			if($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
				$iDaysCount++;
			}
			
		}
		// jesli zapowiedz jest pozniej niz wyliczona data
    $iPreviewTime = $this->_getDatePreview($iPreviewTime, $iPreviewAdd, $aFreeDays);
		if($iPreviewTime > $iDate){
      $iDate = $iPreviewTime;
      /*
			// edodja dodakowe dni robocze do daty zapowiedzi
			if($iPreviewAdd > 0){
				$iDaysCount=0;
				// dopóki nie osiągniemy potrzebnej ilości dni
				while($iDaysCount < $iPreviewAdd){
					// dodaj dzień
					$iDate += (24 * 60 * 60);
					$aDate = getdate($iDate);
					// jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
					if($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
						$iDaysCount++;
					}
				}
			}
       */
		}
		return date($aConfig['common']['date_format'],$iDate);
	}// end of _getDatePreview() method
	
  
  /**
   * Metoda pobiera przesuniętą datę wydania zapowiedzi
   * 
   * @param int(timestamp) $iPreviewTime 
   * @param int $iPreviewAdd
   * @param array $aFreeDays
   * @return int - timestamp
   */
  private function _getDatePreview($iPreviewTime, $iPreviewAdd, $aFreeDays) {
    // edodja dodakowe dni robocze do daty zapowiedzi
    $iDate = $iPreviewTime;
    if($iPreviewAdd > 0){
      $iDaysCount=0;
      // dopóki nie osiągniemy potrzebnej ilości dni
      while($iDaysCount < $iPreviewAdd){
        // dodaj dzień
        $iDate += (24 * 60 * 60);
        $aDate = getdate($iDate);
        // jeśli to nie sobota, niedziela lub wolny, to dodajemy dzień roboczy
        if($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
          $iDaysCount++;
        }
      }
    }
    return $iDate;
  }// end of _getDatePreview() method
  
	
	/**
	 * Metoda sprawdza czy dzisiejszy dzień jest dniem wolnym
	 *
	 * @param array ref $aFreeDays tablica z dniami wolnymi
	 * @return bool
	 */
	function todayIsFreeDay(&$aFreeDays) {
		
		$aDate = getdate(time()); // dziś
		// jeśli to nie sobota, niedziela lub wolny, zwracamy false
		
		if($aDate['wday'] != 0 && $aDate['wday'] != 6 && !in_array(mktime(0,0,0,$aDate['mon'],$aDate['mday'],$aDate['year']),$aFreeDays)){
			return false;
		}
		return true;
	}// end of todayIsFreeDay() method
	
	
	/**
	 * Metoda usuwa zawartosc koszyka
	 *
	 * @return	void
	 */
	function emptyCart() {
		unset($_SESSION['wu_cart']);
		serializeCartToCookie();
		if(isLoggedIn()) {
			$sSql = "DELETE	FROM ".$aConfig['tabls']['prefix']."orders_cart
								WHERE user_id = ".$_SESSION['w_user']['id'];
			Common::Query($sSql);
		}
	} // end of emptyCart() method
	
	/**
	 * Skłąda informację tekstową o przewidywanym czasie wysyłki i 
	 * transportu na podstawie danych w sesji
	 * @return string - info o czasach
	 */
	function getShipmentInfo(){
		$dateTransportDate = '';
		$oTransportDate = new DateTime($_SESSION['wu_cart']['total_transport_date']);
		$oTransportDateMax = new DateTime($_SESSION['wu_cart']['total_transport_date_max']);
		if ($oTransportDate->format('m') == $oTransportDateMax->format('m')) {
			$dateTransportDate = $oTransportDate->format('j').'-'.$oTransportDateMax->format('j.n.Y');
		} elseif ($oTransportDate->format('Y') != $oTransportDateMax->format('Y')) {
			$dateTransportDate = $oTransportDate->format('j.n.Y').'-'.$oTransportDateMax->format('j.n.Y');
		} elseif ($oTransportDate->format('m') != $oTransportDateMax->format('m')) {
			$dateTransportDate = $oTransportDate->format('j.n').'-'.$oTransportDateMax->format('j.n.Y');
		}

		if(intval(date('G')) < $_SESSION['wu_cart']['payment']['time_border']){
			return str_replace(array('{czas_wysylki}','{czas_transportu}'),array($_SESSION['wu_cart']['total_shipment_date'],$dateTransportDate),$_SESSION['wu_cart']['payment']['message_before']);
		} else {
			return str_replace(array('{czas_wysylki}','{czas_transportu}'),array($_SESSION['wu_cart']['total_shipment_date'],$dateTransportDate),$_SESSION['wu_cart']['payment']['message_after']);
		}
	} // end of getShipmentInfo() method
  
  
	/**
	 * Skłąda informację tekstową o przewidywanym czasie wysyłki i 
	 * transportu na podstawie danych w sesji
	 * @return string - info o czasach
	 */
	function getShipmentInfoCalendar(){
    
    $oCalendar = new \Assets\htmlCalendar();
    $oNow = new DateTime();
    
    $oShipmentDate = new DateTime($_SESSION['wu_cart']['total_shipment_date']);
    $oTransportDate = new DateTime($_SESSION['wu_cart']['total_transport_date']);
	$oTransportDateMax = new DateTime($_SESSION['wu_cart']['total_transport_date_max']);

	$mM = $oTransportDateMax->format("m");
	$mF = $oTransportDate->format("m");

	//suma ilość produktów w koszyku
	$sum = 0;
	foreach($_SESSION['wu_cart']['products'] as $product)
	{
		$sum = $sum + $product['quantity'];
	}
	// zwiększanie daty wysyłki (tylko dla klienta) jeśli suma ilość produktów większa niż limit
	if ((int)$_SESSION['wu_cart']['payment']['limit_to_expand_shipping_date'] <= $sum && (int)$_SESSION['wu_cart']['payment']['limit_to_expand_shipping_date'] !== 0)
	{
		$oTransportDate = $oTransportDate->add(new DateInterval('P1D'));
		$oTransportDateMax = $oTransportDateMax->add(new DateInterval('P1D'));
	}

	if ($mM == $mF) {
		//$transportString = $oTransportDate->format("j").'-'.$oTransportDateMax->format("j.n");
		$transportString = $oTransportDate->format("j").'-'.$oTransportDateMax->format("j.n");
	} else {
		$transportString = $oTransportDate->format("j.n").'-'.$oTransportDateMax->format("j.n");
	}

	$sAdditionalMessage = str_replace(array('{czas_nadania}', '{czas_wysylki}','{czas_transportu}'),array($oNow->format("j.n"), $oShipmentDate->format("j.n"), $transportString), $_SESSION['wu_cart']['payment']['cart_calendar_legend']);

    $sHTML = $oCalendar->generateCalendar($oNow, $oShipmentDate, $oTransportDate, $oTransportDateMax, $sAdditionalMessage);
    return $sHTML;
	} // end of getShipmentInfo() method
	
	
	/**
	 * Metoda pobiera metody transportu dla których jest darmowy
	 *		oraz limity wartości zamówienia (po rabatach) od których powinny być uwzględniane
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $iCodeId
	 * @return type 
	 */
	function getCodeFreeTranspot($iCodeId) {
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT transport_id, min_value 
						 FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_transport 
						 WHERE code_id=".$iCodeId;
		return $pDbMgr->GetAll('profit24', $sSql);
	}// end of getCodeFreeTranspot() method
	
	
	/**
	 * Sprawdza poprawnośc uzycia kodu
	 * @param string $sCode - kod
	 * @return array/bool - tablica z kodem lub false w przypadku niepowodzenia
	 */
	function checkDiscountCode($sCode, &$aProducts){
		global $aConfig;
		
		// sprawdzenie czy istnieje kod
 		$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."promotions_codes
 						WHERE code = ".Common::Quote($sCode)." AND
 						DATE_FORMAT(NOW(),'%Y-%m-%d') >= code_active_from AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= code_expiry
 						AND deleted = '0'
            AND (website_id IS NULL OR website_id = ".$aConfig['website_id'].")
            ";
 		$aCode =& Common::GetRow($sSql);
 		if(!empty($aCode)){
			// czy rabat ma ograniczenie na ksiazki
			$aABooks = $this->getDiscountCodeBooks($aCode['id']);
			
			if ($aCode['off_cost_transport'] == '1') {
				// jeśli darmowy koszt tranposrtu
				// pobierz metody transportu dla których jest darmowy
				// oraz limity wartości zamówienia od których powinny być uwzględniane
				$aCode['off_cost_transport_methods'] = $this->getCodeFreeTranspot($aCode['id']);
			}
			$aCode['books'] = $aABooks;
      
			if (!empty($aABooks) || !empty($aCode['extra'])) {
				// ma ograniczenie na ksiazki
				$bHaveBook = false;
				foreach ($aProducts as $iKey => $aItem) {
          /*
					if (in_array($aItem['id'], $aABooks) == true) {
						// ta ksiazka powinna otrzymać rabat
						$bHaveBook = true;
					}
          */
          if ($this->oPromotions->oPromotion->CodeDiscount->checkBookDiscount($aCode, $aItem['id']) > 0) {
            // ta ksiazka powinna otrzymać rabat
						$bHaveBook = true;
          }
				}
				if ($bHaveBook == false) return -2;
			}
 			// kod jednokrotnego użytku - na koncie jednego użytkownika
 			if($aCode['code_type'] == '0'){
 				$sSql = "SELECT COUNT(1)
 							FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_users
 							WHERE code_id = ".$aCode['id'];
 				if(intval(Common::GetOne($sSql)) > 0){
 					return false;
 				} else {
 					return $aCode;
 				}
 			}
 			//kod wielokrotnego użytku (liczba definiowana) - na koncie jednego użytkownika; 
 			elseif($aCode['code_type'] == '1'){
 				$sSql = "SELECT COUNT(1)
 							FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_users
 							WHERE code_id = ".$aCode['id']."
 							AND user_id <> ".$_SESSION['w_user']['id'];
 				// czy inny uzytkownik wykorzystal kod
 				if(intval(Common::GetOne($sSql)) > 0){
 					return false;
 				} else {
 					if($aCode['code_max_use'] > 0){
	 					// czy user nie przekroczyl limitu uzycia kodu
		 				$sSql = "SELECT COUNT(1)
		 							FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_users
		 							WHERE code_id = ".$aCode['id']."
		 							AND user_id = ".$_SESSION['w_user']['id'];
		 				if(intval(Common::GetOne($sSql)) > $aCode['code_max_use']){
		 					return false;
		 				} else {
		 					return $aCode;
		 				}
 					} else {
 						return $aCode;
 					}
 				}
 			}
 		//kod jednokrotnego użytku - na wielu kontach; 
 			elseif($aCode['code_type'] == '2'){
 				$sSql = "SELECT COUNT(1)
 							FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_users
 							WHERE code_id = ".$aCode['id']."
 							AND user_id = ".$_SESSION['w_user']['id'];
 				if(intval(Common::GetOne($sSql)) > 0){
 					return false;
 				} else {
 					return $aCode;
 				}
 			}
 			//kod wielokrotnego użytku - na wielu kontach; 
 			elseif($aCode['code_type'] == '3'){
 				$sSql = "SELECT COUNT(1)
 							FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_users
 							WHERE code_id = ".$aCode['id'];/*."
 							AND user_id = ".$_SESSION['w_user']['id'];*/
 				if(intval(Common::GetOne($sSql)) >= $aCode['code_max_use'] && $aCode['code_max_use']>0){
 					return false;
 				} else {
 					return $aCode;
 				}
 			} else
 					return false;
 			
 		} else 
 			return false;
	}// end of checkDiscountCode() method
	
	
	/**
	 * Metoda pobiera dostępne książki dla kodu rabatowego
	 *
	 * @global type $aConfig
	 * @param type $iCodeId
	 * @return type 
	 */
	function getDiscountCodeBooks ($iCodeId) {
		global $aConfig;
		
		$sSql = "SELECT product_id FROM ".$aConfig['tabls']['prefix']."promotions_codes_to_books
						 WHERE code_id=".$iCodeId;
		return Common::GetCol($sSql);
	}// end of getDiscountCodeBooks() method
	
	
	/**
	 * Funkcja formatuje cene do formatu MySQL
	 *
	 * @param	float	$fPrice	- cena
	 * @return	string	- sformatowana cena
	 */
	function formatPrice2($fPrice) {
		return number_format(str_replace(',', '.', $fPrice), 2, '.', '');
	} // end of formatPrice2() method
	
	
	/**
	 * Metoda pobiera limit dla danego pierwszego źródła
	 *
	 * @param type $iProdId 
	 */
	function getSourceDiscountLimit($iProdId) {
		global $aConfig;

		return 0.00;
		$sSql = "SELECT B.discount_limit 
						 FROM ".$aConfig['tabls']['prefix']."products AS A
						 JOIN ".$aConfig['tabls']['prefix']."products_source_discount AS B
							 ON A.created_by=B.first_source
						 WHERE A.id=".$iProdId;
		$fRet = Common::GetOne($sSql);
		if (!isset($fRet) || is_null($fRet)) {
			$fRet = 0.00;
		}
		return $fRet;
	}// end of getSourceDiscountLimit() method
	

	/**
	 * Metoda pobiera dane o załącznikach produktu
	 *
	 * @global type $aConfig
	 * @param type $iId
	 * @return type 
	 */
	function getProductAttachments($iId) {
		global $aConfig;

		if (isset($this->productsAttachmentsTMP[$iId])) {
			return $this->productsAttachmentsTMP[$iId];
		} else {
			$sSql = "SELECT A.*, B.name FROM ".$aConfig['tabls']['prefix']."products_attachments A
									JOIN ".$aConfig['tabls']['prefix']."products_attachment_types B
									ON B.id = A.attachment_type
									WHERE A.product_id = ".$iId;
			$data = Common::GetAll($sSql);
			$this->productsAttachmentsTMP[$iId] = $data;
			return $data;
		}
	}// end of getProductAttachments() method
	
	
	/**
	 * Metoda pobiera symbol dla metody transportu
	 *
	 * @global array $aConfig
	 * @param type $iTransportId
	 * @return type 
	 */
	function getTransportMeansSymbol($iTransportId) {
		global $aConfig;
		
		$sSql = "SELECT symbol FROM ".$aConfig['tabls']['prefix']."orders_transport_means
						 WHERE id=".$iTransportId;
		return Common::GetOne($sSql);
	}// end of () method
  
  
	/**
	 * Metoda usuwa produkt z koszyka
	 *
	 * @param	string	$sType	- typ produktu
	 * @param	integer	$iId	- Id produktu
	 * @return	void
	 */
	public function deleteProduct($iId, $bRecountStepOne,$omitCalculation = false) {
		$sProductId=$iId;
		if (isset($_SESSION['wu_cart']['products'][$sProductId])) {
			// w koszyku znajduje sie ten produkt - usuwanie
			unset($_SESSION['wu_cart']['products'][$sProductId]);
			if (isset($_SESSION['order']['itm'][$sProductId])) {
				unset($_SESSION['order']['itm'][$sProductId]);
			}
			$this->storeRemoveFromCart($iId);
			if ($omitCalculation === false)
			{
				$this->calculateCart();
            }
		}
    
    if ($bRecountStepOne === FALSE) {
      // wywal wszystko, ale nie na 1 kroku koszyka
      if ($this->cartIsEmpty()) {
        // calkowite usuniecie $_SESSION['wu_cart']
        $this->emptyCart();
      }
    }
	} // end of deleteProduct() method
  
  
	/**
	 * Metoda przelicza koszyk
	 * 
	 * @return	void
	 */
	public function recount() {
		if(isset($_POST['itm'])){
			foreach ($_POST['itm'] as $iId => $iQuantity) {
				if(isset($_SESSION['wu_cart']['products'][$iId]) && !empty($_SESSION['wu_cart']['products'][$iId])){
					if (intval($iQuantity) <= 0) {
						// usuniecie produktu z koszyka
						$this->deleteProduct($iId, true);
					}
					else {
						$this->setProductQuantity($iId, intval($iQuantity));
						
					}
				}
			}
		}
		$this->calculateCart();
	} // end of recount() method
  

	/**
	 * Metoda ustawia liczbe danego produktu
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @param	integer	$iQuantity	- liczba	
	 */
	public function setProductQuantity($iId, $iQuantity) {
		$_SESSION['wu_cart']['products'][$iId]['quantity'] = $iQuantity;
		$this->storeAddToCart($iId,$iQuantity);
	} // end of setProductQuantity() method
  
	
	/**
	 * Method try recount products
	 *
	 * @global array $aConfig 
	 */
	public function RecountAll($bRecountStepOne = FALSE, $bWithPreviusDelete = TRUE) {

		$aTMP['last_item_link'] = $_SESSION['wu_cart']['last_item_link'];
		$aTMP['last_item_name'] = $_SESSION['wu_cart']['last_item_name'];
		$aTMP['just_added'] = $_SESSION['wu_cart']['just_added'];
		
		if (isset($_SESSION['wu_cart']['discount_code']) && !empty($_SESSION['wu_cart']['discount_code'])) {
			$aTMPCode = $_SESSION['wu_cart']['discount_code'];
		}
		
    if ($bWithPreviusDelete === true) {
      if (!empty($_SESSION['wu_cart']['products'])) {
        foreach ($_SESSION['wu_cart']['products'] as $iKey => $aVal) {
          $this->deleteProduct($iKey, $bRecountStepOne,true);
          for ($i=0; $i < $aVal['quantity']; $i++ ) {
            $this->addProduct($iKey,true,1,false,true);
          }
        }
      }
    }
        $this->calculateCart();
		$_SESSION['wu_cart']['last_item_link'] = $aTMP['last_item_link'];
		$_SESSION['wu_cart']['last_item_name'] = $aTMP['last_item_name'];
		$_SESSION['wu_cart']['just_added'] = $aTMP['just_added'];
		
		if (isset($aTMPCode) && !empty($aTMPCode)) {
			$_SESSION['wu_cart']['discount_code'] = $aTMPCode;
			$this->codeRecalculateProducts();
		}
	}// end of RecountAll() method
  
  
  /**
   * Metoda rekurencyjna filtruje dane wejściowe z kroków do zapisania w sesji
   * 
   * @param array $aStepData
   * @return array
   */
  function filterStepsData($aStepData) {
    $aMatches = array();
    
    foreach ($aStepData as $sKey => $mVar) {
      if (preg_match('/^__/', $sKey, $aMatches)) {
        unset($aStepData[$sKey]);
      } else {
        if (isset($aStepData[$sKey])) {
          if (!empty($mVar) && is_array($mVar)) {
            // rekurencja
            $aStepData[$sKey] = $this->filterStepsData($mVar);
          } else {
            $aStepData[$sKey] = filterInput($mVar);
          }
        }
      }
    }
    return $aStepData;
  }// end of filterStepsData() method
} // end of Common_m_zamowienia Class
?>
