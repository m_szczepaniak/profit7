
<?php
/**
 * Klasa odule do obslugi Zamowien - przechowalnia
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczene wspolnej klasy modulu Zamowienia
include_once('modules/m_zamowienia/client/Common.class.php');

class Module extends Common_m_zamowienia {

  // Id wersji jezykowej
  var $iLangId;
  
  // symbol wersji jezykowej
  var $sLang;
  
  // Id strony
  var $iPageId;
  
  // nazwa strony
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon strony
  var $sTemplate;
  
  // symbol modulu
  var $sModule;
  
  // ustawienia strony modulu
  var $aSettings;
  
  // opcja modulu
  var $sOption;
  
  // link do strony modulu
  var $sPageLink;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module($sSymbol, $iPageId, $sName) {
		global $aConfig;
		//echo 'test';
		$this->iLangId =& $_GET['lang_id'];
		$this->sLang =& $_GET['lang'];
		$this->iPageId = $iPageId;
		$this->sName = $sName;
		$this->sModule = $sSymbol;
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/';
		// konstruktor klas Common
		parent::Common_m_zamowienia($aConfig['_tmp']['page']['id'],
																'/'.$aConfig['_tmp']['page']['symbol'],
																$aConfig['_tmp']['page']['module'],
																'modules/'.$aConfig['_tmp']['page']['module']);
	} // end Module() function


	/**
	 * Metoda realizuje zadania koszyka - dodawanie, przeliczanie, usuwanie produktow
	 * z koszyka, realizacja zamowienia
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['page_link'] =& $this->sPageLink;

		
		switch($_GET['action']) {
			case 'add_to_repository':
				$this->addToRepository((int) $_GET['id']);
				doRedirect($_SERVER['HTTP_REFERER']);
			break;
			
			case 'delete_from_repository':
				$this->deleteFromRepository((int) $_GET['id']);
				doRedirect($_SERVER['HTTP_REFERER']);
			break;
	
			case 'move_from_cart':
				$this->addToRepository((int) $_GET['id']);
				$this->deleteProduct((int) $_GET['id']);
				doRedirect($_SERVER['HTTP_REFERER']);
			break;			

			default:
				$sHtml = $this->showRepository();
			break;
		}
		
		return $sHtml;
	} // end of getContent()
	

	/**
	 * Metoda zwraca kod html z przechowalnia
	 * 
	 * @param	id - id produktu
	 */
	function showRepository(){
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		
		$bShowCart = false;
				
		if(isLoggedIn()){
		// jesli uzytkownik zalogowany pobranie przechowalni z bazy danych
			$sSql = "SELECT product_id FROM ".$aConfig['tabls']['prefix']."orders_briefcase
								WHERE konto_id =".$_SESSION['w_user']['id']."";
								
			$aData =& Common::GetCol($sSql);
		} else {
			// jesli niezalogowany pobranie przechowalni z cookies
			if(isset($_COOKIE['repository'])){
				$sRepository = base64_decode($_COOKIE['repository']);
				$aData = explode(",", $sRepository);
			} else {
				// jesli cookie nieznaleziony
				$aData = array();
			}
		}
		
		// jesli znajduje sie cos w tablicy
		if( count($aData) > 0 ) $bShowCart = true;
		
		// znizki indywidualne klienta
		if (isLoggedIn()){
			$fUserDiscount = $_SESSION['w_user']['discounts'];
		} else {
			$fUserDiscount = 0;	
		}
		if ( $bShowCart === true) {
			foreach ($aData as $iKey => $iId){
				$iId=intval($iId);
				if($iId>0){
				$sSql = "SELECT A.id, A.title AS name, A.prod_status, A.shipment_time, A.discount_limit, A.packet,
													A.price_brutto, C.vat
									 FROM ".$aConfig['tabls']['prefix']."products_shadow A
									 JOIN ".$aConfig['tabls']['prefix']."products C
									 ON C.id=A.id
									 WHERE A.id = ".$iId;
					 
				$aModule['items'][$iKey] =& Common::GetRow($sSql);		
				if(!empty($aModule['items'][$iKey])){
		
					$aModule['items'][$iKey]['link'] = createProductLink($aModule['items'][$iKey]['id'], $aModule['items'][$iKey]['name']);
					$aModule['items'][$iKey]['add_to_cart_link'] = createLink($aConfig['_tmp']['cart_symbol'], 'id'.$aModule['items'][$iKey]['id'], 'add_from_repository');
					$aModule['items'][$iKey]['delete_link'] = createLink($aConfig['_tmp']['repository_symbol'], 'id'.$aModule['items'][$iKey]['id'], 'delete_from_repository');
					
					$aTarrif = $this->getTarrif($iId);
					$aModule['items'][$iKey]['tarrif']['packet'] = ($aModule['items'][$iKey]['packet']=='1');
					// przeliczenie i formatowanie cen
					$aModule['items'][$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aModule['items'][$iKey]['price_brutto'], $aTarrif, $aModule['items'][$iKey]['promo_text'],$aModule['items'][$iKey]['discount_limit'], false, false, $this->getSourceDiscountLimit($iId), 0));
					$aModule['items'][$iKey]['price_brutto'] = Common::formatPrice2($aModule['items'][$iKey]['price_brutto']);
					
					$aModule['items'][$iKey]['price_netto'] = Common::formatPrice2($aModule['items'][$iKey]['price_brutto'] / (1 + $aModule['items'][$iKey]['vat']/100));
					
					$aModule['items'][$iKey]['price_brutto'] = Common::formatPrice2($aModule['items'][$iKey]['price_brutto']);
					$aModule['items'][$iKey]['promo_price_netto'] = Common::formatPrice2($aModule['items'][$iKey]['promo_price'] / (1 + $aModule['items'][$iKey]['vat']/100));
					
					$aModule['items'][$iKey]['discount'] = Common::formatPrice2($aTarrif['discount']);
					$aModule['items'][$iKey]['discount_currency'] = Common::formatPrice2($aModule['items'][$iKey]['price_brutto'] * (Common::formatPrice2($aTarrif['discount']) / 100));
				} else {
					unset($aModule['items'][$iKey]);
				}
			}
		}
			
			$pSmarty->assign_by_ref('aModule', $aModule);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/domyslny_przechowalnia.tpl');
			$pSmarty->clear_assign('aModule', $aModule);
		} else {
			setMessage($aModule['lang']['no_data']);
		}
		
		return $sHtml;
	}	
	
	/**
	 * Metoda dodaje produkt do przechowalni
	 * 
	 * @param	id - id produktu
	 */
	function addToRepository($iId){
		global $aConfig;
		
		if ($this->productExists($iId)){
			$sProductId=$iId;
			$_SESSION['repository']['just_added'] = 1;
			$_SESSION['repository']['last_item'] = $sProductId;
			
			if(isLoggedIn()){
				$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_briefcase
									WHERE konto_id = ".$_SESSION['w_user']['id']."
									AND product_id = ".$iId;
							
				if((int) Common::GetRow($sSql) != 1){
					$aValues = array(
						'konto_id' => $_SESSION['w_user']['id'],
						'product_id' => $sProductId
					);
					
					
					Common::Insert($aConfig['tabls']['prefix']."orders_briefcase",
														$aValues, "", false);
				}
			} else {
				if(isset($_COOKIE['repository'])){
					
					$sRepository = base64_decode($_COOKIE['repository']);
					
					$bIs = false;
					
					foreach(explode(",", $sRepository) as $iKey => $sValue) {
						if($sValue == $sProductId) $bIs = true;
					}
					
					if($bIs === false) $sRepository .= ",".$sProductId;
					$sRepository = base64_encode($sRepository);
					
					AddCookie("repository", $sRepository, time() + 60*60*24*366);
				} else {
					$sRepository = base64_encode($sProductId);
					AddCookie("repository", $sRepository, time() + 60*60*24*366);
				}
			}
		} else {
			doRedirect();
		}
	}
	

	
	/**
	 * Metoda usuwa produkt z koszyka
	 *
	 * @param	string	$sType	- typ produktu
	 * @param	integer	$iId	- Id produktu
	 * @return	void
	 */
	function deleteProduct($iId) {
	$sProductId=$iId;
		if (isset($_SESSION['wu_cart']['products'][$sProductId])) {
			// w koszyku znajduje sie ten produkt - usuwanie
			unset($_SESSION['wu_cart']['products'][$sProductId]);
			if (isset($_SESSION['order']['itm'][$sProductId])) {
				unset($_SESSION['order']['itm'][$sProductId]);
			}
			$this->storeRemoveFromCart($iId);
			$this->calculateCart();
		}
		if ($this->cartIsEmpty()) {
			// calkowite usuniecie $_SESSION['wu_cart']
			$this->emptyCart();
		}
		
	} // end of deleteProduct() method
	
		/**
	 * Metoda sprawdza czy produkt o podanym typie i Id istnieje
	 *
	 * @param	string	$sType	- typ produktu
	 * @param	integer	$iId	- Id produktu
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function productExists($iId) {
		global $aConfig;

		$sSql = "SELECT A.id
						 FROM ".$aConfig['tabls']['prefix']."products A
						 JOIN ".$aConfig['tabls']['prefix']."menus_items B
						 ON B.id = A.page_id
						 WHERE A.id = ".$iId." AND
						 			 B.published = '1' AND
						 			 A.published = '1' ";
		return  ((double) Common::GetOne($sSql)) > 0;
	} // end of productExists() function
	
} // end of Module Class
?>