<?php
/**
 * Klasa odule do obslugi Zamowien - koszyk
 *
 * @property $oPromotions Promotions
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */
// dolaczene wspolnej klasy modulu Zamowienia
include_once('modules/m_zamowienia/client/Common.class.php');

class Module extends Common_m_zamowienia {

  // Id wersji jezykowej
  var $iLangId;
  
  // symbol wersji jezykowej
  var $sLang;
  
  // Id strony
  var $iPageId;
  
  // nazwa strony
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon strony
  var $sTemplate;
  
  // symbol modulu
  var $sModule;
  
  // ustawienia strony modulu
  var $aSettings;
  
  // opcja modulu
  var $sOption;
  
  // link do strony modulu
  var $sPageLink;
  
  var $oPromotion;

  var $ordersNoduplicatesSemaphore;
  
  private $bSelectedOdbiorWPunkcie = FALSE;
  private $bSelectedPaczkaWRuchu = FALSE;
  private $bSelectedOrlen = FALSE;
  private $bSelectedTBA = FALSE;
  private $bSelectedInpost = FALSE;
  private $bSelectedOpek = FALSE;
  private $bIsReceptionAtPoint = FALSE;
  private $bIsDeliveryToAddress = FALSE;

  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module($sSymbol, $iPageId, $sName) {
		global $aConfig;

//    dump($_SESSION['wu_cart']);
//    unset($_SESSION['wu_cart']['discount_code']);
//    dump($_SESSION);die;
		$this->iLangId =& $_GET['lang_id'];
		$this->sLang =& $_GET['lang'];
		$this->iPageId = $iPageId;
		$this->sName = $sName;
		$this->sModule = $sSymbol;
		$this->sOption = 'koszyk';
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/'.$this->sOption.'/';
		$aPage = array('mtype' => '0', 'id' => $this->iPageId);
		$this->sPageLink = $sName;
    
    if (isset($_SESSION['wu_cart']['payment']['transport_id'])) {
      $sTransportSymbol = $this->getTransportMeansSymbol($_SESSION['wu_cart']['payment']['transport_id']);
      $this->bSelectedOdbiorWPunkcie = $this->checkTransportOdbiorWPunkcie($sTransportSymbol);      
      $this->bSelectedPaczkaWRuchu = $this->checkTransportPaczkaWRuchu($sTransportSymbol);
      $this->bSelectedOrlen = $this->checkTransportOrlen($sTransportSymbol);
      $this->bSelectedTBA = $this->checkTransportTBA($sTransportSymbol);
      $this->bSelectedInpost = $this->checkTransportInpost($sTransportSymbol);
      $this->bSelectedOpek = $this->checkTransportOpek($sTransportSymbol);
      $this->bIsReceptionAtPoint = $this->checkIsReceptionAtPoint($sTransportSymbol);
      $this->bIsDeliveryToAddress = $this->checkIsDeliveryToAddress($sTransportSymbol);   
    }  
    
    // konstruktor klas Common
		parent::Common_m_zamowienia($aConfig['_tmp']['page']['id'],
																'/'.$aConfig['_tmp']['page']['symbol'],
																$aConfig['_tmp']['page']['module'],
																'modules/'.$aConfig['_tmp']['page']['module']);
	} // end Module() function
   
  
  /**
   * Metoda dodaje objekt kroku pierwszego
   * 
   * @return \stepOne
   */
  public function getStepOneObj() {

    include_once('steps/stepOne.class.php');
    return new stepOne($this);
  }// end of getStepOneObj() method
  
  
  /**
   * Metoda dodaje objekt kroku drugiego
   * 
   * @return \stepTwo
   */
  public function getStepTwoObj() {

    include_once('steps/stepTwo.class.php');
    return new stepTwo($this);
  }// end of getStepTwoObj() method
  
  
  /**
   * Metoda dodaje objekt kroku trzeciego
   * 
   * @return \stepThree
   */
  public function getStepThreeObj() {
    
    if (isLoggedIn()) {
      $this->getUserData();
    }
    include_once('steps/stepThree.class.php');
    return new stepThree($this);
  }// end of getStepThreeObj() method

  
  /**
   * Metoda dodaje objekt kroku czwartego
   * 
   * @return \stepFour
   */
  public function getStepFourObj() {

    include_once('steps/stepFour.class.php');
    return new stepFour($this);
  }// end of getStepFourObj() method
  

   /**
	 * Metoda realizuje zadania koszyka - dodawanie, przeliczanie, usuwanie produktow
	 * z koszyka, realizacja zamowienia
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['page_link'] =& $this->sPageLink;
		if (empty($_GET['action']) && !empty($_POST) && isset($_POST['add'])) {
			$_GET['action'] = 'add';
		}
		// akcje dopuszczalne gdy koszyk jest pusty
		$aAllowedWhenEmpty = array(
      'order_register_active',
      'order_active',
			'add',
			'dotpay',
			'dotpay_c',
			'add_from_repository',
			'platnosci',
			'platnosci_ok',
			'platnosci_err',
			'platnosci_pay',
      'thank_you'
		);
		if ($this->cartIsEmpty() && !in_array($_GET['action'], $aAllowedWhenEmpty)) {
			$_GET['action'] = '';
		}

      if (isset($_POST['group_action']) && !empty($_POST['group_action']) && isset($_POST['selected_products']) && !empty($_POST['selected_products'])) {

        switch($_POST['group_action']) {
          case 'group_delete':

            foreach($_POST['selected_products'] as $prodId) {
              $this->deleteProduct((int) $prodId, true);
            }
            break;
        }
        doCartSafeRedirect($_SERVER['HTTP_REFERER']);
        die;
      }

		switch($_GET['action']) {
			case 'add':
				// dodanie produktu do koszyka
        
                $oStepOne = $this->getStepOneObj();
                //remove cart items limit
                if ( count(array_keys($_SESSION['wu_cart']['products'])) >= ($aConfig['common']['max_cart_items']))
                {
                    setMessage("Nie można dodać więcej niż 100 produktów do koszyka, jeśli chcesz kupić więcej złóż kolejne zamówienie", true, true);
                }
                else
                {
                    $this->addProduct((int)$_GET['id']);
                    $this->RecountAll(FALSE, FALSE);
                }

                doCartSafeRedirect($_SERVER['HTTP_REFERER']);
                break;
			
			case 'add_from_repository':
				// dodanie produktu do koszyka z przechowalni
                //remove cart items limit
                if ( count(array_keys($_SESSION['wu_cart']['products'])) >= ($aConfig['common']['max_cart_items']))
                {
                    setMessage("Nie można dodać więcej niż 100 produktów do koszyka, jeśli chcesz kupić więcej złóż kolejne zamówienie", true, true);
                }
                else
                {
                    if($this->addProduct((int) $_GET['id']) === true){
                        $this->deleteFromRepository((int) $_GET['id']);
                    }
                }

				doCartSafeRedirect($_SERVER['HTTP_REFERER']);
				
			    break;
			
			case 'empty':
        // oproznienie koszyka
        
        $oStepOne = $this->getStepOneObj();
				$this->emptyCart();
				$sHtml = $oStepOne->showCart();
			break;

			case 'delete':
        // usuniecie produktu z koszyka
				$this->deleteProduct((int) $_GET['id'], true);
				doCartSafeRedirect($this->sPageLink);
			break;

			case 'recount':
        // przeliczenie koszyka
        
        $oStepOne = $this->getStepOneObj();
				$this->RecountAll();
				if(isset($_POST['itm']) && !empty($_POST['itm'])){
					$this->recount();
				}
				$sHtml = $oStepOne->showCart();
			break;
      
      /** 2 **/
      case 'save_step2':
        
        try {
          $oStepTwo = $this->getStepTwoObj();
          $oStepTwo->saveStepTwo();
        } catch (Exception $exc) {
          setMessage($exc->getMessage(), true, true);
          doRedirectHttps(createLink($this->sPageLink, 'step2').'#cart');
        }
        doRedirectHttps(createLink($this->sPageLink, 'step3'));
      break;
			case 'step2':
				$this->recount();
				if (!$this->cartIsEmpty()) {
						$oStepTwo = $this->getStepTwoObj();
            $oStepTwo->checkRecountDiscountCode();
						$sHtml = $oStepTwo->getStep2();
				}
				else {
          $oStepOne = $this->getStepOneObj();
					$sHtml = $oStepOne->showCart();
				}
			break;
      
      /** 3 **/
      case 'step3':
        if (!$this->cartIsEmpty()) {
          $oStepThree = $this->getStepThreeObj();
          $sHtml = $oStepThree->getStep3();
        }
				else {
          $oStepOne = $this->getStepOneObj();
					$sHtml = $oStepOne->showCart();
				}
      break;
      case 'save_step3_log_out':
        try {
          $oStepThree = $this->getStepThreeObj();
          $oStepThree->saveStepThreeLogOut();
        } catch (Exception $exc) {
          setMessage($exc->getMessage(), true, true);
          doRedirectHttps(createLink($this->sPageLink, 'step3').'#cart');
        }
        doRedirectHttps(createLink($this->sPageLink, 'step4'));
      break;
      case 'save_step3_logged_in':
        try {
          $oStepThree = $this->getStepThreeObj();
          $oStepThree->saveStepThreeLogIn();
        } catch (Exception $exc) {
          setMessage($exc->getMessage(), true, true);
          doRedirectHttps(createLink($this->sPageLink, 'step3').'#cart');
        }
        doRedirectHttps(createLink($this->sPageLink, 'step4'));
      break;
    /** 4 **/
      case 'save_step4':
        try {
          $oStepFour = $this->getStepFourObj();
          $oStepFour->saveStepFour();
        } catch (Exception $exc) {
          setMessage($exc->getMessage(), true, true);
          doRedirectHttps(createLink($this->sPageLink, 'step4').'#cart');
        }
        doRedirectHttps(createLink($this->sPageLink, 'order'));
      break;
			case 'step4':
				if (!$this->cartIsEmpty()) {
						$oStepFour = $this->getStepFourObj();
						$sHtml = $oStepFour->getStep4();
				}
				else {
          $oStepOne = $this->getStepOneObj();
					$sHtml = $oStepOne->showCart();
				}
			break;
    
      case 'order_register_active':
        
        // aktywacja konta i zamówienia
        $sOrderNumber = str_replace('-', '/', $_GET['on']);
        $aMatches = array();
        if (preg_match('/^[\w\/]+$/', $sOrderNumber, $aMatches)) {
          $iOId = $this->_getOrderIdByOrderNumber($sOrderNumber);
          if ($iOId > 0) {
            $aOrderSelData = $this->getOrderDataParam(array('id', 'order_number', 'order_date', 'user_id'), $iOId);
            $sOrderNumber = rawurldecode(htmlspecialchars(str_replace('/', '-', $aOrderSelData['order_number'])));
            $sToken = md5($aOrderSelData['order_date'].':'.$aOrderSelData['id'].':'.'order_register_active');
            if ($sToken === $_GET['token']) {
              // aktywacja konta
              include_once('modules/m_konta/client/User.class.php');
              $aTMP = array();
              $oUser = new User('', 0, $aTMP, '');
              $oUser->aUser['id'] = $aOrderSelData['user_id'];
              if ($oUser->activate() !== false) {
                include_once('modules/m_konta/client/UserLogin.class.php');
								$oLogin = new UserLogin('');
								if($oLogin->doLoginById($oUser->aUser['id'])){
									$_COOKIE['wu_check'] = $_SESSION['wu_check'];
								}
                
                // aktywacja zamówienia
                if ($this->_activate($iOId) !== false) {
                  setMessage(printWebsiteMessage(getWebsiteMessage('order_register_activtion_ok'), array('email'), array($aConfig['_settings']['site']['email'])), false, true);
                  doRedirect('/');
                } else {
                  setMessage(_('Wystąpił błąd podczas aktywacji konta oraz zamówienia ERR 101. <br /><br /> Spróbuj ponownie, jeżeli błąd będzie się powtarzał prosimy o kontakt '.$aConfig['_settings']['site']['email']), true, true);
                  doRedirect('/');
                }
              } else {
                setMessage(_('Wystąpił błąd podczas aktywacji konta oraz zamówienia ERR 102. <br /><br /> Spróbuj ponownie, jeżeli błąd będzie się powtarzał prosimy o kontakt '.$aConfig['_settings']['site']['email']), true, true);
                doRedirect('/');
              }
            }
          } else {
            setMessage(_('Wystąpił błąd podczas aktywacji konta oraz zamówienia ERR 103. <br /><br /> Spróbuj ponownie, jeżeli błąd będzie się powtarzał prosimy o kontakt '.$aConfig['_settings']['site']['email']), true, true);
            doRedirect('/');
          }
        } else {
          setMessage(_('Wystąpił błąd podczas aktywacji konta oraz zamówienia ERR 104. <br /><br /> Spróbuj ponownie, jeżeli błąd będzie się powtarzał prosimy o kontakt '.$aConfig['_settings']['site']['email']), true, true);
          doRedirect('/');
        }
      break;
      case 'order_active':
        // aktywacja zamówienia
 
        $sOrderNumber = str_replace('-', '/', $_GET['on']);
        $aMatches = array();
        if (preg_match('/^[\w\/]+$/', $sOrderNumber, $aMatches)) {
          $iOId = $this->_getOrderIdByOrderNumber($sOrderNumber);
          if ($iOId > 0) {
            $aOrderSelData = $this->getOrderDataParam(array('id', 'order_number', 'order_date'), $iOId);
            $sOrderNumber = rawurldecode(htmlspecialchars(str_replace('/', '-', $aOrderSelData['order_number'])));
            $sToken = md5($aOrderSelData['order_date'].':'.$aOrderSelData['id'].':'.'order_active');
            if ($sToken === $_GET['token']) {
              // aktywacja zamówienia
              if ($this->_activate($iOId) !== false) {
                setMessage(printWebsiteMessage(getWebsiteMessage('order_activtion_ok'), array('email'), array($aConfig['_settings']['site']['email'])), false, true);
                doRedirect('/');
              } else {
                setMessage(_('Wystąpił błąd podczas aktywacji konta oraz zamówienia ERR 101. <br /><br /> Spróbuj ponownie, jeżeli błąd będzie się powtarzał prosimy o kontakt '.$aConfig['_settings']['site']['email']), true, true);
                doRedirect('/');
              }
            }
          } else {
            setMessage(_('Wystąpił błąd podczas aktywacji konta oraz zamówienia ERR 101. <br /><br /> Spróbuj ponownie, jeżeli błąd będzie się powtarzał prosimy o kontakt '.$aConfig['_settings']['site']['email']), true, true);
            doRedirect('/');
          }
        } else {
          setMessage(_('Wystąpił błąd podczas aktywacji konta oraz zamówienia ERR 102. <br /><br /> Spróbuj ponownie, jeżeli błąd będzie się powtarzał prosimy o kontakt '.$aConfig['_settings']['site']['email']), true, true);
          doRedirect('/');
        }
      break;
    
			case 'dotpay':
				$sHtml = $this->showDotPayInfo();
			break;
			
			case 'register':
				$sHtml = $this->getRegisterForm();
			break;

			case 'dotpay_c':
				if ($_SERVER['REMOTE_ADDR'] == '217.17.41.5' || $_SERVER['REMOTE_ADDR'] == '195.150.9.37') {
					$this->setDotPayTransactionStatus();
				}
				else {
					die;
				}
			break;	
			
			case 'platnosci':
				$this->setPlatnosciTransactionStatus();
			break;
			
			case 'platnosci_ok':
				if(intval($_GET['id']) == $_SESSION['wu_order']['last_order']){

                    $this->getCriteoJS(intval($_GET['id']));

					$sHtml = $this->showNewOrderInfo(intval($_GET['id']),true,true);
					unset($_SESSION['wu_order']['last_order']);
				} else {
					setMessage($aModule['lang']['no_data']);
				}
			break;
			
			case 'platnosci_err':
				if(intval($_GET['id']) == $_SESSION['wu_order']['last_order']){

                    $this->getCriteoJS(intval($_GET['id']));

                    $sHtml = $this->showNewOrderInfo(intval($_GET['id']),true,false);
					unset($_SESSION['wu_order']['last_order']);
				} else {
					setMessage($aModule['lang']['no_data']);
				}
			break;
      
			case 'thank_you':
                $sSql = 'SELECT id FROM orders WHERE order_number = "' . $_GET['id'] . '"';
                $iOId = Common::GetOne($sSql);

				if(intval($iOId) == $_SESSION['wu_order']['last_order']){

                    $this->getCriteoJS($iOId);

                    $sHtml = $this->showNewOrderInfo(intval($_GET['id']),true,false);
					unset($_SESSION['wu_order']['last_order']);
				} else {
					setMessage($aModule['lang']['no_data']);
				}
			break;
			
			case 'platnosci_pay':
				$sUrl = $this->prepareNewPayment(intval($_GET['id']));
				if($sUrl !== false){
					$_SESSION['wu_order']['last_order'] = intval($_GET['id']);
					header('Location: '.$sUrl);
					exit();
				} else {
					doRedirect(base64_decode($_POST['ref']));
				}
				
			break;

			case 'order':
                // check for duplicates

                $oMemcached = new Memcached();
                $oMemcached->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
                $this->ordersNoduplicatesSemaphore = new LIB\orders_semaphore\ordersNoDuplicatesSemaphore($oMemcached,$_SESSION,$_SERVER['HTTP_X_FORWARDED_FOR']);

                $mDuplicates = $this->checkDuplicated();
                if ($mDuplicates === true)
                {
                    $this->emptyCart();
                    return;
                } elseif (false !== $mDuplicates && $mDuplicates > 0) {

                    $iOId = $mDuplicates;
                    if($_SESSION['wu_cart']['payment']['ptype'] == 'platnoscipl' || $_SESSION['wu_cart']['payment']['ptype'] == 'payu_bank_transfer' || $_SESSION['wu_cart']['payment']['ptype'] == 'card') {
                        // przyjmujemy że mamy id zamówienia
                        $sUrl = $this->prepareNewPayment($iOId, $_SESSION['wu_cart']['payment']['ptype'] == 'card');
                        if ($aConfig['common']['status'] != 'development') {
                            $this->emptyCart();
                            $this->ordersNoduplicatesSemaphore->unlock();
                            $_SESSION['logged_out']['authorized_orders'][] = $iOId;
                            $this->_getEmptyPageAndRedirect($sUrl);
                            exit();
                        }
                    } else {
                        if ($aConfig['common']['status'] != 'development') {
                            $this->emptyCart(); ///XXX TODO ODKOMENTOWAC
                            $this->ordersNoduplicatesSemaphore->unlock();
                            $_SESSION['logged_out']['authorized_orders'][] = $iOId;
                            $_SESSION['logged_out']['header_message'] = _('<span style="font-size: 14px;">Dziękujemy za złożenie zamówienia.</span><br /><span style="font-size: 12px;">Poniżej znajdują się jego szczegóły.</span>');
//                        $sPath = getModulePageLink('m_konta', 'sprawdzanie');
                            $sSql = 'SELECT order_number FROM orders WHERE id = '.$iOId;
                            $sNewOrderNumber = Common::GetOne($sSql);
                            doRedirectHttps(createLink($aConfig['_tmp']['cart_symbol'], 'id'.$sNewOrderNumber, 'thank_you' ));
                        }
                        else
                        {
                            setMessage("Złożyłeś zamówienie!");
                        }
                    }

                }
        // składamy zamówienie
        if ($this->__checkValidAllSteps() === TRUE) {
          if (!$this->cartIsEmpty()){
              $aData =& $_SESSION['wu_cart'];
              $aDataTwo =& $_SESSION['wu_cart']['step_2'];
                $aData['check_status_key'] = rand(100000, 999999);
                if($_SESSION['wu_cart']['payment']['ptype'] != 'platnoscipl' ||  isset($aDataTwo['payment_bank'])) {
                  // dodawanie zamowienia
                  if (!isLoggedIn()) {
                    $iRegisterUserId = $this->_doRegisterUser($_SESSION['wu_cart']['step_3_logged_out']);
                  }
                  
                  if (($iOId = $this->doOrder($aData, $iRegisterUserId)) !== false) {
                    $iTypeActivation = 0;

                    $aOrderAddress = $this->getOrderAddresses($iOId); 
                    $iAddressType = 1;
                    
                    if ($this->checkCityPostal($aOrderAddress[$iAddressType]['city'], $aOrderAddress[$iAddressType]['postal']) === FALSE &&
                        $this->bSelectedInpost === FALSE) {
                      
                      $this->addContainer($iOId);
                    }
                    
                    $_SESSION['wu_order']['order_activation_type'] = $iTypeActivation;

                    $_SESSION['wu_order']['last_order'] = $iOId;
                    $this->sendNewOrderUserEmail($iOId, $iTypeActivation);
                    $this->sendNewOrderAdminEmail($iOId, $iTypeActivation);
                    if($_SESSION['wu_cart']['payment']['ptype'] == 'platnoscipl' || $_SESSION['wu_cart']['payment']['ptype'] == 'payu_bank_transfer' || $_SESSION['wu_cart']['payment']['ptype'] == 'card'){
                      $sUrl = $this->prepareNewPayment($iOId, $_SESSION['wu_cart']['payment']['ptype'] == 'card');
                      /*
                       *  ///XXX TODO ODKOMENTOWAC
                       */
                      if ($aConfig['common']['status'] != 'development') {
                        $this->emptyCart();
                        $this->ordersNoduplicatesSemaphore->unlock();
                        $_SESSION['logged_out']['authorized_orders'][] = $iOId;
                        $_SESSION['wu_order']['last_order'] = $iOId;
                        $this->_getEmptyPageAndRedirect($sUrl);
                        exit();
                      }
                    }
                    else {
                      if ($aConfig['common']['status'] != 'development') {
                        $this->emptyCart(); ///XXX TODO ODKOMENTOWAC
                        $this->ordersNoduplicatesSemaphore->unlock();
                        $_SESSION['logged_out']['authorized_orders'][] = $iOId;
                        $_SESSION['logged_out']['header_message'] = _('<span style="font-size: 14px;">Dziękujemy za złożenie zamówienia.</span><br /><span style="font-size: 12px;">Poniżej znajdują się jego szczegóły.</span>');
//                        $sPath = getModulePageLink('m_konta', 'sprawdzanie');
                        $sSql = 'SELECT order_number FROM orders WHERE id = '.$iOId;
                        $sNewOrderNumber = Common::GetOne($sSql);
                        doRedirectHttps(createLink($aConfig['_tmp']['cart_symbol'], 'id'.$sNewOrderNumber, 'thank_you' ));
                      }
                      else
                      {
                          setMessage("Złożyłeś zamówienie!");
                      }
                    }									
                  }
                  else {
                    // nie udalo sie dodac zamowienia
                    // wyswietlenie informacji i ponowne wyswietlenie 2 kroku
                    setMessage(printWebsiteMessage(getWebsiteMessage('order_err'), array('email'), array($aConfig['_settings']['site']['email'])), true);
                  }
                }
                else {
                  // metoda płatności to platnosci.pl i nie wybrano banku
                  setMessage($aConfig['lang']['mod_'.$this->sModule]['no_bank_chosen'], true);
                }
          }
          else {
            // coś dziwnie ze tu wpadło
            $oStepOne = $this->getStepOneObj();
            $sHtml = $oStepOne->showCart();
          }
        } else {
          // coś dziwnie ze tu wpadło
          $oStepOne = $this->getStepOneObj();
          $sHtml = $oStepOne->showCart();
        }
			break;
      
			default:
                $oStepOne = $this->getStepOneObj();
				$this->RecountAll(TRUE);
				$sHtml = $oStepOne->showCart();
			break;
		}
		return $sHtml;
	} // end of getContent() method


    /**
     * @return mixed
     */
  private function checkDuplicated()
  {
      if (false === $this->ordersNoduplicatesSemaphore->isLocked()) {
          $this->ordersNoduplicatesSemaphore->lock();
      } else {
          $bLocked = true;
          if (true === $bLocked) {
              if (isset($_SESSION['wu_order']['last_order']) && $_SESSION['wu_order']['last_order'] > 0) {
                  return $_SESSION['wu_order']['last_order'];
              } else {
                  sleep(5);
                  if (isset($_SESSION['wu_order']['last_order']) && $_SESSION['wu_order']['last_order'] > 0) {
                      return $_SESSION['wu_order']['last_order'];
                  }
                  else
                  {
                    $sSql = 'SELECT id FROM orders WHERE user_id = '.(int)$_SESSION['w_user']['id'].' AND order_date > (now() - interval 5 minute)';
                    $latest_order = Common::GetOne($sSql);
                    
                    if ( $latest_order > 0 )
                    {
                        $sMsg = 'Twoje zamówienie prawdopodobnie zostało złożone.Jeśli nie otrzymałeś maila potwierdzającego złożenie zamówienia skontaktuj się z obsługą sklepu';
                    }
                    else
                    {
                        $sMsg = 'Twoje zamówienie prawdopodobnie nie zostało złożone,Jeśli nie otrzymałeś maila potwierdzającego złożenie zamówienia skontaktuj się z obsługą sklepu';
                    }

                    setMessage($sMsg);
                    return true;
                  }
              }
          }
      }

      return false;
  }
  /**
   * Metoda dodaje zamówienie do zakładki "Błedny transport"
   * 
   * @param string $iOId
   */
  private function addContainer($iOId) {
    global $aConfig;
    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
    $oOrdersContainers = new \orders_containers\OrdersContainers();
    $oOrdersContainers->addOrderToContainers($iOId, 'T', false);      
  }
  
  
  /**
   * Metoda wyświetla przez 2 sekundy JS, aby wysłać requesty do przeglądarki, następnie może przekierować w inne miejsce
   * 
   * @param string $sRedirectURL
   */
  private function _getEmptyPageAndRedirect($sRedirectURL) {

      $gtm = "<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
              'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WT5QKXN');</script>
	<!-- End Google Tag Manager -->";
    $_GET['hideHeader'] = '1';
  //   <meta http-equiv="refresh" content="2; url='.$sRedirectURL.'" />
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="refresh" content="1; url='.$sRedirectURL.'" />
  '.$gtm.'
</head>
<body>

'.$_SESSION['logged_out']['js'].'
'.$aConfig['common']['google_analytics'].'
</body>
</html>
';

    unset($_SESSION['logged_out']['js']);
    exit();
  }// end of _getEmptyPageAndRedirect() method
  
  
  /**
   * Metoda pobiera id zamówienia na podstawie nr zamówienia
   * 
   * @global object $pDbMgr
   * @param string $sOrderNumber
   * @return int
   */
  private function _getOrderIdByOrderNumber($sOrderNumber) {
    global $pDbMgr;
    
    $sSql = "SELECT id FROM orders WHERE order_number = '".$sOrderNumber."'";
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of _getOrderIdByOrderNumber() method
  
  
  /**
   * Metoda dodaje użytkownika
   * uwaga metoda $oUser->doAddUser używa transakcji
   * 
   * @param type $aDataThreeLogOut
   */
  private function _doRegisterUser($aDataThreeLogOut) {
    
    include_once('modules/m_konta/client/User.class.php');
    $aTMP = array();
    $oUser = new User('', 0, $aTMP, '');
    $sSalt = $oUser->getRandomString(255);
    $aUserData = array(
        'email' => $aDataThreeLogOut['email'],
        'phone' => $aDataThreeLogOut['phone'],
        'passwd' => encodePasswd($aDataThreeLogOut['passwd'], $sSalt),
        'salt' => $sSalt
    );
    $aRegulationsData = array(
        'accept_priv' => $aDataThreeLogOut['accept_priv'],
        'accept_reg' => $aDataThreeLogOut['accept_reg'],
        'promotions_agreement' => $aDataThreeLogOut['promotions_agreement'],
        'registred' => $aDataThreeLogOut['registration'] == '1' ? '1' : '0'
    );
    // na transakcji
    $iUId = $oUser->doAddUser($aUserData, $aRegulationsData, true, 0);
    if ($iUId > 0) {
      // dodajmy adresy do konta
      $bIsDefaultTransport = false;
      if (empty($aDataThreeLogOut['delivery']) || $aDataThreeLogOut['delivery']['delivery_data_from_invoice'] == '1') {
        $bIsDefaultTransport = true;
      }
      $oUser->doAddUserAddressNew($iUId, $aDataThreeLogOut['invoice'], true, $bIsDefaultTransport);
      if ($aDataThreeLogOut['delivery']['delivery_data_from_invoice'] == '2') {
        $oUser->doAddUserAddressNew($iUId, $aDataThreeLogOut['delivery'], false, true);
      }
    }
    return $iUId;
  }// end of _doRegisterUser() method


	/**
	 * Metoda pobiera komunikat dla metody transportu Paczkomaty
	 *
	 * @global array $aConfig
	 * @return type 
	 */
	function getDiscountInfo() {
		
		return printWebsiteMessage(getWebsiteMessage('kod_rabatowy_info'), array(), array());
	}// end of getPaczkomatyInfo() method
	
	
	/**
	 * Metoda wyswietla informacje o statusie transakcji dotpay.pl
	 *
	 * @param	integer	$iOId	- Id zamowienia
	 * @return	string
	 */
	function showDotPayInfo() {
		global $aConfig, $pSmarty;
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];

		$sStatus = isset($_POST['status']) ? $_POST['status'] : isset($_GET['status']) ? $_GET['status'] : '';
		if (in_array($sStatus, array('OK', 'FAIL'))) {
			$aConfig['_tmp']['page']['name'] = $aModule['lang']['dotpay_status_info'];
			// informacja o stanie realizacji transakcji dotpay.pl
			$aModule['dotpay_status'] = printWebsiteMessage(getWebsiteMessage('dotpay_'.$sStatus), array('email'),array($aConfig['_settings']['site']['email']));
		}
		else {
			$aModule['dotpay_status'] = printWebsiteMessage(getWebsiteMessage('dotpay_NO_DATA'), array('email'),array($aConfig['_settings']['site']['email']));
		}
		$aModule['dotpay_status_code'] = $sStatus;
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/_dotpay_status_info.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	} // end of showDotPayInfo() method
	
	
	/**
	 * Metoda zwraca od HTML formularza logowania
	 *
	 * @return	string	- formularz logowania
	 */
	function getRecipientDataForm(&$oForm) {
		global $aConfig;
		$aData = array();
    
    if (isLoggedIn()) {
      $aRecipientData = $_SESSION['wu_cart']['step_3_logged_in']['recipient'];
    } else {
      $aRecipientData = $_SESSION['wu_cart']['step_3_logged_out']['recipient'];
    }

		if (isset($aRecipientData['name']) && isset($aRecipientData['surname']) && isset($aRecipientData['phone'])) {
			$aData['name'] = htmlspecialchars($aRecipientData['name']);
			$aData['surname'] = htmlspecialchars($aRecipientData['surname']);
			$aData['recipient_phone'] = htmlspecialchars($aRecipientData['phone']);
		} else {
			$aData['name'] = htmlspecialchars($_SESSION['wu_cart']['user_data']['name']);
			$aData['surname'] = htmlspecialchars($_SESSION['wu_cart']['user_data']['surname']);
			$aData['recipient_phone'] = htmlspecialchars($_SESSION['wu_cart']['user_data']['phone']);
		}
		
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aFields = array();
	 	$aFields['name']['label'] = $oForm->GetLabelHTML('name', $aModule['lang']['name'], true);
		$aFields['name']['input'] = $oForm->GetTextHTML('name', $aModule['lang']['name'], $aData['name'], array('style' => 'width: 225px','class' => 'text','maxlength' => 255), '', 'text', true);
		
		$aFields['surname']['label'] = $oForm->GetLabelHTML('surname', $aModule['lang']['surname'], true);
		$aFields['surname']['input'] = $oForm->GetTextHTML('surname', $aModule['lang']['surname'], $aData['surname'], array('style' => 'width: 225px','class' => 'text','maxlength' => 255), '', 'text', true);
		
		$aFields['recipient_phone']['label'] = $oForm->GetLabelHTML('recipient_phone', $aModule['lang']['phone'], true);
		$aFields['recipient_phone']['input'] = $oForm->GetTextHTML('recipient_phone', $aModule['lang']['phone'], $aData['recipient_phone'], array('style' => 'width: 225px','class' => 'text','maxlength' => 255), '', 'phone', true);
		
		return $aFields;
	} // end of getLoginForm() method


	/**
	 * Metoda zwraca od HTML formularza z danymi adresowymi dostawy
	 * oraz danymi do faktury
	 * 
	 * @return	string	- formularz z danymi adresowymi i do faktury
	 */
	function getPaymentAddressDataForm(&$aModule) {
		global $aConfig, $pSmarty;
		
		$aModule['template'] .= $this->sTemplatesPath.'/_addressDataForm.tpl';
		$aModule['page_link'] =& $this->sPageLink;
		
		// dolaczenie klasy User
		$aSettings = array();
		include_once('modules/m_konta/client/User.class.php');
		$oUser = new User($this->sModule.'_'.$this->sOption,
											$this->iPageId,
											$aSettings,
											$this->sPageLink,
											$_SESSION['wu_check']);
		
		$aModule['form'] =& $oUser->getAddressDataForm();
		$aModule['payment_id'] = $_SESSION['wu_cart']['payment']['id'];
		
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($aModule['template']);
		$pSmarty->clear_assign('aModule');
		
		return $sHtml;
	} // end of getAddressDataForm() method
	

   /**
   * Metoda zwraca symbol transportu na podstawie Id transportu
   * 
   * @param int $iTransportId
   * @return string
   */
  function getTransportMeansSymbol($iTransportId) {
    
    $sSql = "SELECT symbol
             FROM orders_transport_means
             WHERE id = ".$iTransportId;
               
    return Common::GetOne($sSql);
  }// end of getTransportMeansSymbol() method
  
  
	/**
	 * Metoda sprawdza czy przekazane id transportu to id paczkomatow
	 *
	 * @param integer $sTransportSymbol
	 * @return bool
	 */
	function checkTransportInpost($sTransportSymbol) {
    if ($sTransportSymbol != 'paczkomaty_24_7') {
      return FALSE;
    }
    
    return TRUE;
	}// end of checkTransportInpost() method
	

	/**
	 * Metoda sprawdza czy przekazane id transportu to id Poczty Polskiej - Odbiór w punkcie
	 *
	 * @param integer $sTransportSymbol
	 * @return bool
	 */
	function checkTransportOdbiorWPunkcie($sTransportSymbol) {
    if ($sTransportSymbol != 'poczta_polska') {
      return FALSE;
    }
    
    return TRUE;
	}// end of checkTransportOdbiorWPunkcie() method
  

	/**
	 * Metoda sprawdza czy przekazane id transportu to id Ruch - Paczka w Ruchu
	 *
	 *
	 * @param integer $sTransportSymbol
	 * @return bool
	 */
	function checkTransportPaczkaWRuchu($sTransportSymbol) {
    if ($sTransportSymbol != 'ruch') {
      return FALSE;
    }
    
    return TRUE;
	}// end of checkTransportPaczkaWRuchu() method  

  
	/**
	 * Metoda sprawdza czy przekazane id transportu to id Orlen - Stacja z paczką
	 *
	 * @param integer $sTransportSymbol
	 * @return bool
	 */
	function checkTransportOrlen($sTransportSymbol) {
    if ($sTransportSymbol != 'orlen') {
      return FALSE;
    }
    
    return TRUE;
	}// end of checkTransportOrlen() method  
  
  
	/**
	 * Metoda sprawdza czy przekazane id transportu to id Opek/FedEx
	 *
	 * @param integer $sTransportSymbol
	 * @return bool
	 */
	function checkTransportOpek($sTransportSymbol) {
    if ($sTransportSymbol != 'opek-przesylka-kurierska') {
      return FALSE;
    }
    
    return TRUE;
	}// end of checkTransportOpek() method


	/**
	 * Metoda sprawdza czy przekazane id transportu to id TBA
	 *
	 * @param integer $sTransportSymbol
	 * @return bool
	 */
	function checkTransportTBA($sTransportSymbol) {
    if ($sTransportSymbol != 'tba') {
      return FALSE;
    }
    
    return TRUE;
	}// end of checkTransportTBA() method    
  
  
   /**
   * Metoda sprawdza czy przekazany Transport Symbol to Punkt Odbioru
   *  
   * @param int $sTransportSymbol
   * @return bool
   */
  private function checkIsReceptionAtPoint($sTransportSymbol) {
    global $aConfig, $pDbMgr;
    
    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');  
    $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
    $oShipment = new \orders\Shipment($sTransportSymbol, $pDbMgr);  
               
    return $oShipment->isReceptionAtPoint();
  }// end of checkIsReceptionAtPoint() method    
  
  
   /**
   * Metoda sprawdza czy przekazany Transport Symbol to Doręczenie pod wskazany adres 
   * 
   * @param int $sTransportSymbol
   * @return bool
   */
  private function checkIsDeliveryToAddress($sTransportSymbol) {
    global $aConfig, $pDbMgr;
    
    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');  
    $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
    $oShipment = new \orders\Shipment($sTransportSymbol, $pDbMgr);  
               
    return $oShipment->isDeliveryToAddress();
  }// end of checkIsDeliveryToAddress() method    


    /**
     * @param $userAccountSellerStreamsoftId
     * @return mixed
     */
    private function getSellerStreamsoftId($userAccountSellerStreamsoftId){

        $buyBox = new \orders\MarkUpCookie\BuyBox();
        $cookieSellerStreamsoftId = $buyBox->getCookieValue();

        if ($cookieSellerStreamsoftId != null && $cookieSellerStreamsoftId != '') {
            return $cookieSellerStreamsoftId;
        } else {
            if ($userAccountSellerStreamsoftId != '') {
                return $userAccountSellerStreamsoftId;
            }
        }
        return null;
    }

  
	/**
	 * Metoda dodaje zamowienie do bazy danych
	 * 
	 * TODO ISBN bez pauz:
	 *			$sReg = '823-123-3213123 21312321';
	 *			$aMatch = preg_replace('/[^\d]/', '', $sReg);
	 * 
	 * @return	mixed	- > 0: Id zamowienia; false: blad
	 */
	function doOrder($aData, $iRegisterUserId = 0) {
		global $aConfig, $pDB2;
    
    $aCeneoIdItems = array();
    
    $aDataTwo =& $aData['step_2'];
    $aDataThreeLogIn =& $aData['step_3_logged_in'];
    $aDataThreeLogOut =& $aData['step_3_logged_out'];
    
    if (isLoggedIn()) {
      $aDataThree =& $aDataThreeLogIn;
    } else {
      $aDataThree =& $aDataThreeLogOut;
    }
    
    $aDataFour =& $aData['step_4'];
		include_once('OrderItemRecount.class.php');
		$oOrderItemRecount = new OrderItemRecount();
		
		$this->calculateCart();
			
		$sPaymentName = $aData['payment']['name'];
		$fPaymentCost = $aData['payment']['cost'];
				

		// przepisanie danych uzytkownika z sesji
    if (isLoggedIn()) {
      $this->getUserData();
      // TODO zdarzyło się że te dane były puste odnie emaila, po zalogowaniu
      $aUser =& $_SESSION['wu_cart']['user_data'];
      $iUId = $_SESSION['w_user']['id'];
    } else {
      $aUser = array(
                  'email' => $aDataThreeLogOut['email'],
                  'name' => '',
                  'surname' => '',
                  'phone' => $aDataThreeLogOut['phone']
                );
    }
		
		if(($sOrderNumber = $this->getOrderNumberRandom())===false){
			return false;
		}

        Common::BeginTransaction();
		if (empty($aUser['email']) || !preg_match($aConfig['class']['form']['email']['pcre'], $aUser['email'])) {
			Common::RollbackTransaction();
			return false;
		}
    
    $iTransportVat = $oOrderItemRecount->getTransportVat($aData['payment']['id'], $aData['products']);
    $iUserDayToPay = 0;
    if ($aData['payment']['ptype'] == 'bank_14days' && $iUId > 0 && isLoggedIn()) {
      $iUserDayToPay = $this->getUserInvoiceDaysToPay($iUId);
    }

        $sellerStreamsoftId = $this->getSellerStreamsoftId($aUser['seller_streamsoft_id']);

		$aValues = array (
			'user_id' => ($iUId > 0 ? $iUId : ($iRegisterUserId > 0 ? $iRegisterUserId : 'NULL')),
			'order_date' => 'NOW()',
			'order_number' => $sOrderNumber,
            'invoice_to_pay_days' => $iUserDayToPay > 0 ? $iUserDayToPay : '',
			'transport_cost' => Common::formatPrice2($aData['payment']['cost']),
			'value_netto' => Common::formatPrice2($aData['total_price_netto']),
			'value_brutto' => Common::formatPrice2($aData['total_price_brutto']),
			'nopromo_value_brutto' => Common::formatPrice2($aData['total_without_promo']),
			'discount_code' => isset($aData['discount_code']) ? $aData['discount_code']['code'] : 'NULL',
			'discount_code_value' => isset($aData['discount_code']) ? $aData['discount_code']['code_discount'] : '0',
			'payment' => $sPaymentName,	
			'payment_type' => $aData['payment']['ptype'],
			'payment_id' => $aData['payment']['id'],
			'transport' => $aData['payment']['transport'],
			'transport_id' => $aData['payment']['transport_id'],
			'block_transport_cost' => ($aData['payment']['block_transport_cost']?'1':'0'),
			'name' => $aUser['name'] != '' ? $aUser['name'] : 'NULL',
			'surname' => $aUser['surname'] != '' ? $aUser['surname'] : 'NULL',
			'phone' => $aUser['phone'] != '' ? $aUser['phone'] : 'NULL',
			'email' => $aUser['email'],
			'second_invoice' => (!empty($aDataTwo['item_2nd_invoice'])) ? '1' : '0',
			'remarks' => $aDataFour['remarks'] != '' ? $aDataFour['remarks'] : 'NULL',
			'transport_remarks' => $aDataFour['transport_remarks'] != '' ? $aDataFour['transport_remarks'] : 'NULL',
			'client_ip' => $_SERVER['REMOTE_ADDR'],
      'client_host' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
			'source' => isset($_SESSION['source']['source']) ? $_SESSION['source']['source'] : 'NULL',
			'source_id' => isset($_SESSION['source']['id']) ? $_SESSION['source']['id'] : 0 ,
			'shipment_date' => formatDateMysql($aData['total_shipment_date']),
      'shipment_date_expected' => formatDateMysql($aData['total_shipment_date']),
			'transport_date' => formatDateMysql($aData['total_transport_date']),
			'registred_user' => isLoggedIn()? '1':'0',
			'bookstore' => $aConfig['common']['bookstore_code'],
			'internal_status' => ($aData['payment']['ptype'] == 'platnoscipl' || $aData['payment']['ptype'] == 'card' || $aData['payment']['ptype'] == 'payu_bank_transfer' ? '0' : '1'), //  || !isLoggedIn() ? '0' : '1'
			'to_pay' => $aData['to_pay'],
			'session_id' => session_id(),
			'transport_vat' => $iTransportVat,
			'order_status' => '0',
      'check_status_key' => $aData['check_status_key'],
      'active' => '1',//isLoggedIn() ? '1' : '0'
      'ceneo_agreement' => (isset($aDataThree['ceneo_agreement']) && $aDataThree['ceneo_agreement'] == '1' ? '1' : '0'),
            'seller_streamsoft_id' => isset($sellerStreamsoftId) && $sellerStreamsoftId != '' ? $sellerStreamsoftId : ''
		);
		
		if ($this->bSelectedInpost === TRUE) {
			// trasport to paczkomaty zapisujemy dane paczkomatow
			$aValues['point_of_receipt'] = $aDataTwo['point_of_receipt'] != '' ? $aDataTwo['point_of_receipt'] : 'NULL';
			$aValues['phone_paczkomaty'] = $aDataTwo['phone_paczkomaty'] != '' ? $aDataTwo['phone_paczkomaty'] : 'NULL';
			$aValues['regulations_paczkomaty'] = $aDataTwo['regulations_paczkomaty'] == '1' ? '1' : '0';
		} 

		if ($this->bSelectedOdbiorWPunkcie === TRUE) {
			// trasport to Odbiór w punkcie zapisujemy dane placówki pocztowej

			$aValues['point_of_receipt'] = $aDataTwo['point_of_receipt_poczta_polska'] != '' ? $aDataTwo['point_of_receipt_poczta_polska'] : 'NULL';
    }

		if ($this->bSelectedPaczkaWRuchu === TRUE) {
			// trasport to Paczka w Ruchu to zapisujemy dane punktu sieci Ruch

			$aValues['point_of_receipt'] = $aDataTwo['point_of_receipt_ruch'] != '' ? $aDataTwo['point_of_receipt_ruch'] : 'NULL';
    }    

		if ($this->bSelectedOrlen === TRUE) {
			// trasport to Stacja z Paczką to zapisujemy dane punktu sieci Orlen

			$aValues['point_of_receipt'] = $aDataTwo['point_of_receipt_orlen'] != '' ? $aDataTwo['point_of_receipt_orlen'] : 'NULL';
    }

        if ($aValues['payment_type'] == 'bank_14days' || $aValues['transport_id'] == 2) {
            $aValues['print_fv'] = '1';
        }

    if ((Common::Insert($aConfig['tabls']['prefix']."orders", $aValues, '', false)) === false) {
			Common::RollbackTransaction(); 
			return false;
		}

		$sSql = "SELECT MAX(id) FROM ".$aConfig['tabls']['prefix']."orders 
						 WHERE session_id='".session_id()."'
						 ORDER BY id DESC LIMIT 1";
		$iOId = Common::GetOne($sSql);
		if ($iOId <= 0) {
			// nie wybrano id złożonego zamówienia
			Common::RollbackTransaction();
			return false;
		}
		
		$this->logUserCart($iOId);

    // jeśli user zalogowany
    if (isLoggedIn()) {
      if ($this->_addOrderAdressLogin($aData, $aUser, $iOId) === false) {
        Common::RollbackTransaction();
        return false;
      }
    } else {
      // gość nie jest zalogowany, przygotowanie danych
      if ($this->_addOrderAdressLogout($aData, $aUser, $iOId) === false) {
        Common::RollbackTransaction();
        return false;
      }
    }
    
		foreach($aData['products'] as $iId => $aProduct) {
			if(!empty($aProduct)){
			$sSql = "SELECT A.*, B.name AS publisher_name
								FROM ".$aConfig['tabls']['prefix']."products A
								LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
								ON B.id=A.publisher_id
								WHERE A.id=".$iId;
			$aItem =& Common::GetRow($sSql);
			$aValues = array (
				'order_id' => $iOId,
				'product_id' => $iId,
				'name' => $aProduct['name'],
				'price_netto' => Common::formatPrice2($aProduct['price_netto']),
				'price_brutto' => Common::formatPrice2($aProduct['price_brutto']),
				'vat' => $aProduct['vat'],
				'vat_currency' => Common::formatPrice2($aProduct['price_brutto'] - $aProduct['price_netto']),
				'total_vat_currency' => Common::formatPrice2($aProduct['total_price_brutto'] - $aProduct['total_price_netto']),
				'value_netto' => Common::formatPrice2($aProduct['total_price_netto']),
				'value_brutto' => Common::formatPrice2($aProduct['total_price_brutto']),
				'quantity' => $aProduct['quantity'],
				'discount' => Common::formatPrice2($aProduct['discount']),
				'discount_currency' => Common::formatPrice2($aProduct['discount_currency']),
				'total_discount_currency' => Common::formatPrice2($aProduct['discount_currency'] * $aProduct['quantity']),
				'promo_price_netto' => Common::formatPrice2($aProduct['promo_price_netto']),
				'promo_price_brutto' => Common::formatPrice2($aProduct['promo_price']),
				'source' => '0',//$aProduct['source'],
				'packet' => $aItem['packet'],
				'isbn' => (!empty($aItem['isbn'])?isbn2plain($aItem['isbn']):''),
				'publication_year' => (!empty($aItem['publication_year'])?$aItem['publication_year']:''),
				'edition' => (!empty($aItem['edition'])?$aItem['edition']:'NULL'),
				'shipment_time' => $aItem['shipment_time'],
				'publisher' => (!empty($aItem['publisher_name'])?$aItem['publisher_name']:''),
				'authors' => $this->getAuthorsStr($iId),
				'preview' => ($aItem['prod_status']=='3'?'1':'0'),
				'status' => '0',
				'shipment_date' => ($aItem['prod_status']=='3'?$aItem['shipment_date']:'NULL'),
				'weight' => $aItem['weight'],
				'second_invoice' => (!empty($aDataTwo['item_2nd_invoice']) && $aDataTwo['item_2nd_invoice'][$iId] == '1'?'1':'0')
			);		
			if (($iItemId = Common::Insert($aConfig['tabls']['prefix']."orders_items", $aValues)) === false) {
				Common::RollbackTransaction();
				return false;
			}

			$aCeneoIdItems[] = $iId;
			if (isset($aConfig['common']['google_analytics_conversion_order'])) {
				// kod GoogleAnalytics - do zliczania konwersji - dane o produkcie
				$sGoogleConversion .= '
pageTracker._addItem(
	"'.$sOrderNumber.'",
	"'.$aItem['isbn'].'",
	"'.str_replace('"', '&quot;', $aItem['plain_name']).'", 
	"'.str_replace('"', '&quot;', $aProduct['category']).'",
	"'.($aProduct['promo_price'] > 0 ? $aProduct['promo_price'] : $aProduct['price_brutto']).'",
	"'.$aProduct['quantity'].'"
);
';
			}
			
			// czy pakiet
			$bPacket = $aItem['packet'];
			
			if ($bPacket == '1') {
				$fTotalPacketWeight = 0;
				// pobranie skladowych pakietu
				$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."products_packets_items WHERE packet_id = ".$iId;
				// przetworzenie skladowych pakietu - dodanie do bazy
				$aPckItems =& Common::GetAll($sSql);
				foreach($aPckItems as $iKey => $aValue){
					$sSql = "SELECT A.price_netto, A.price_brutto, A.name, A.isbn, A.publication_year, B.name AS publisher_name, A.edition, A.shipment_time, A.weight, A.source
										FROM ".$aConfig['tabls']['prefix']."products A
										JOIN ".$aConfig['tabls']['prefix']."products_publishers B
											ON B.id=A.publisher_id
										WHERE A.id = ".$aValue['product_id'];
		
					$aItemInfo =& Common::GetRow($sSql);
					
					$fPackValueNetto = ($aValue['promo_price_netto']*$aProduct['quantity']);
					$fPackValueBrutto = ($aValue['promo_price_brutto']*$aProduct['quantity']);
					$aValuesPckItems = array(
						'order_id' => $iOId,
						'product_id' => $aValue['product_id'],
						'name' => $aItemInfo['name'],
						'price_netto' => $aItemInfo['price_netto'],
						'price_brutto' => $aItemInfo['price_brutto'],
						'vat' => $aValue['vat'],
						'vat_currency' => Common::formatPrice2($aItemInfo['price_brutto'] - $aItemInfo['price_netto']),
						'total_vat_currency' => Common::formatPrice2($fPackValueBrutto - $fPackValueNetto),
						'value_netto' => $fPackValueNetto,
						'value_brutto' => $fPackValueBrutto,
						'quantity' => $aProduct['quantity'],//1,
						'promo_price_brutto' => $aValue['promo_price_brutto'],
						'promo_price_netto' => $aValue['promo_price_netto'],
						'discount' => $aValue['discount']+$aItem['additional_discount'],
						'discount_currency' => $aValue['discount_currency'],
						'total_discount_currency' => Common::formatPrice2($aValue['discount_currency'] * $aProduct['quantity']),
						'source' => $aItemInfo['source'],
						'item_type' => 'P',
						'parent_id' => $iItemId,
						'isbn' => (!empty($aItemInfo['isbn'])?isbn2plain($aItemInfo['isbn']):'NULL'),
						'publication_year' => (!empty($aItemInfo['publication_year'])?$aItemInfo['publication_year']:'NULL'),
						'edition' => (!empty($aItemInfo['edition'])?$aItemInfo['edition']:'NULL'),
						'shipment_time' => $aItemInfo['shipment_time'],
						'publisher' => (!empty($aItemInfo['publisher_name'])?$aItemInfo['publisher_name']:'NULL'),
						'authors' => $this->getAuthorsStr($aValue['product_id']),
						'weight' => $aItemInfo['weight'],
						'second_invoice' => (!empty($aDataTwo['item_2nd_invoice']) && $aDataTwo['item_2nd_invoice'][$iId] == '1'?'1':'0')
					);
					if (($iPacketItemId = Common::Insert($aConfig['tabls']['prefix']."orders_items", $aValuesPckItems)) === false) {
						Common::RollbackTransaction();
			 			return false;
					}

					$fTotalPacketWeight += $aItemInfo['weight'];
					// dodanie informacji o zalacznikach produktu pakietu
					if ($this->checkAttachments($aValue['product_id']) == 1) {
						$sSql = "SELECT A.*, B.name FROM ".$aConfig['tabls']['prefix']."products_attachments A
													JOIN ".$aConfig['tabls']['prefix']."products_attachment_types B
													ON B.id = A.attachment_type
													WHERE A.product_id = ".$aValue['product_id'];
						$aPacketAttachments =& Common::GetAll($sSql); 
						foreach ($aPacketAttachments as $iPAttKey => $aPAttVal) {
							// rabat
							$aPAttVal['product_discount'] = ($aValue['discount']+$aItem['additional_discount']);
							$aPAttVal['product_quantity'] = $aProduct['quantity'];
							$aPAttVal['product_shipment_time'] = $aItemInfo['shipment_time'];
							
							$aValuesAttPacket = $oOrderItemRecount->getAttrValues($aPAttVal, $iPacketItemId, $iOId);
							
							if (Common::Insert($aConfig['tabls']['prefix']."orders_items", $aValuesAttPacket) === false) {
								Common::RollbackTransaction();
				 				return false;
							}
						}

						$aValues = $oOrderItemRecount->getParentAttData($aValuesPckItems,  $aPacketAttachments, $aValuesPckItems['promo_price_brutto']);
						
						if (Common::Update($aConfig['tabls']['prefix']."orders_items", $aValues, "id = ".$iPacketItemId." AND order_id = ".$iOId." AND product_id = ".$aValue['product_id']) === false) {
							Common::RollbackTransaction();
				 			return false;
						}
					}
				}
				$aUValues = array(
					'weight' => $fTotalPacketWeight
				);
				if(Common::Update($aConfig['tabls']['prefix']."orders_items", $aUValues, 'id = '.$iItemId) === true){
						Common::RollbackTransaction();
				 		return false;
				}
			}
			
			// dodanie informacji o zalacznikach produktu
			if ($this->checkAttachments($iId) == 1) {
				$sSql = "SELECT A.*, B.name FROM ".$aConfig['tabls']['prefix']."products_attachments A
													JOIN ".$aConfig['tabls']['prefix']."products_attachment_types B
													ON B.id = A.attachment_type
													WHERE A.product_id = ".$iId;
				$aAttachments =& Common::GetAll($sSql);
				foreach ($aAttachments as $iAttKey => $aAttVal) {
					
					// domyślnie wszystkie wartości z załącznika
					$aAttRecountData = $aAttVal;
					// nadpisanie pojedyńczych z produktu nadrzędnego
					$aAttRecountData['product_discount'] = $aValues['discount'];
					$aAttRecountData['product_quantity'] = $aValues['quantity'];
					$aAttRecountData['product_shipment_time'] = $aValues['shipment_time'];

					$aValuesAtt = $oOrderItemRecount->getAttrValues($aAttRecountData, $iItemId, $iOId);
					if (Common::Insert($aConfig['tabls']['prefix']."orders_items", $aValuesAtt) === false) {
						Common::RollbackTransaction();
		 				return false;
					}

					
					$aValues = $oOrderItemRecount->getParentAttData($aValues,  $aAttachments, $aProduct['promo_price']);
					if (Common::Update($aConfig['tabls']['prefix']."orders_items", $aValues, "id = ".$iItemId." AND order_id = ".$iOId." AND product_id = ".$iId) === false) {
						Common::RollbackTransaction();
						return false;
					}
				}
			}
		}
		}
		
		if(!empty($aData['discount_code'])){
			$aCode = $this->checkDiscountCode($aData['discount_code']['code'], $aData['products']);
			if($aCode === false) {
				unset($aData['discount_code']);
				setMessage($aConfig['lang']['mod_'.$this->sModule]['discount_code_err'],true);
				Common::RollbackTransaction();
		 		return false;
			} elseif ($aCode == -2) {
				unset($aData['discount_code']);
				setMessage($aConfig['lang']['mod_'.$this->sModule]['discount_code_err_2'],true);
				Common::RollbackTransaction();
		 		return false;
			}
			else {
				$aValuesP = array(
					'user_id' => ($iUId > 0 ? $iUId : ($iRegisterUserId > 0 ? $iRegisterUserId : 'NULL')),
					'code_id' => $aData['discount_code']['id'],
					'order_id' => $iOId,
					'time_of_use' => 'NOW()'
				);
				if(Common::Insert($aConfig['tabls']['prefix']."promotions_codes_to_users",$aValuesP)===false){
					Common::RollbackTransaction();
		 			return false;
				}
			}
		}
    
    $aOrderItems = $this->getOrderItemsDataParamByOrderId(array('deleted', 'vat', 'packet'), $iOId);
    $iVatTransportCorrection = $oOrderItemRecount->getTransportVat($aData['payment']['id'], $aOrderItems);
    if ($iTransportVat != $iVatTransportCorrection) {
      $aValuesCorrection = array(
          'transport_vat' => $iVatTransportCorrection
      );
      if(Common::Update('orders', $aValuesCorrection, ' id='.$iOId) === false){
        Common::RollbackTransaction();
        return false;
      }
    }
		Common::CommitTransaction();

    $sStrConversion = '';
    if (isset($aConfig['common']['google_analytics_conversion_order'])) {
        // kod GoogleAnalytics - do zliczania konwersji - dane o zamowieniu
        $sGoogleConversion = '
  pageTracker._addTrans(
    "'.$sOrderNumber.'",
    "",
    "'.Common::formatPrice2($aData['total_price_brutto']).'",
    "'.(Common::formatPrice2($aData['total_price_brutto']) - Common::formatPrice2($aData['total_price_netto'])).'",
    "'.Common::formatPrice2($aData['payment']['cost']).'",
    "'.$aUser['city'].'",
    "",
    "Poland"
    );
    '.$sGoogleConversion;

        // przepisanie kodu GoogleAnalytics z konwersja
        // do zmiennej ze zwyklym kodem GoogleAnalytic
      if(strpos($aUser['email'], 'a.golba@profit24.pl') === false) {
        $aConfig['common']['google_analytics'] = str_replace('{CONVERSION_CODE}', $sGoogleConversion, $aConfig['common']['google_analytics_conversion_order']);
        $sStrConversion = $aConfig['common']['google_analytics'];
      }
      else {
      //	(htmlspecialchars(str_replace('{CONVERSION_CODE}', $sGoogleConversion, $aConfig['common']['google_analytics_conversion_order'])));					
      }
    }


        if (in_array($aUser['email'], ['arekgolba@gmail.com', 'm.k@eactive.pl'])) {
            $simpleCode = '<script type="text/javascript">
                    try {
                        var pageTracker = _gat._getTracker("UA-11882157-2");
                        pageTracker._initData();
                        pageTracker._trackPageview();
                    
                        {CONVERSION_CODE}
                    
                        pageTracker._trackTrans();
                    } catch(err) {}</script>';
            $aConfig['common']['google_analytics'] = str_replace('{CONVERSION_CODE}', $sGoogleConversion, $simpleCode);
            $sStrConversion = $aConfig['common']['google_analytics'];
        }

        if ($aConfig['common']['facebook_pixel_purchase'] != '') {
            $sStrConversion .= sprintf($aConfig['common']['facebook_pixel_purchase'], Common::formatPrice2($aData['total_price_brutto']));
        }

      $sBuyBox = '';
      if (isset($aConfig['common']['buybox_commision_id'])) {
        $sBuyBox = \BuyBox\ScriptGenerator::generateOrderScript($sOrderNumber, $aData);
      }
    // ceneo + analytycs do sesji
    if (isset($aDataThree['ceneo_agreement']) && $aDataThree['ceneo_agreement'] == '1') {
        // YYYY-MM-DD
        // $estimatedDeliveryDate
        $sGoogleSurvey = sprintf($aConfig['common']['google_survey'], $sOrderNumber, $aUser['email'], formatDateMysql($aData['total_shipment_date']));
      $_SESSION['logged_out']['js'] = sprintf($aConfig['common']['ceneo_zaufane_opinie'], $aUser['email'], $sOrderNumber, '#'.implode('#', $aCeneoIdItems).'#').
                                        $sGoogleSurvey.
                                      $sStrConversion.
                                      $sBuyBox;


    } else {
      $_SESSION['logged_out']['js'] = $sStrConversion.
                                      $sBuyBox;
    }
    
		return $iOId;
	} // end of doOrder() method


  /**
   * @param $iUserId
   * @return mixed
   */
  private function getUserInvoiceDaysToPay($iUserId) {
    global $pDbMgr;

    $sSql = 'SELECT invoice_to_pay_days 
	             FROM users_accounts
	             WHERE id = '.$iUserId;
    return $pDbMgr->GetOne('profit24', $sSql);
  }

  
  /**
   * Metoda dodaje nowy adres w przypadku osoby niezalogowanej
   * 
   * @param array $aData
   * @param int $iOId
   * @return boolean
   */
  private function _addOrderAdressLogout($aData, $aUser, $iOId) {
    $aDataTwo =& $aData['step_2'];
    $aDataThreeLogOut =& $aData['step_3_logged_out'];

    if (!empty($aDataThreeLogOut['invoice'])) {
      if($this->addAddressToOrder($iOId,$aDataThreeLogOut['invoice'],'0') === false){
        return false;
      }
    }
    else {
      return false;
    }

    //var_dump($aDataThreeLogOut['delivery']);die;
    // dane do dostawy
    //if ($this->bIsReceptionAtPoint === FALSE) {
      // różne od paczkomatów i innych punktów odbioru, zapisz dane dostawy
      if (!empty($aDataThreeLogOut['delivery']) || !empty($aDataThreeLogOut['recipient'])) {
        if ($aDataThreeLogOut['delivery']['delivery_data_from_invoice'] == '1') {
          // dane takie jak na fakturze, 
          // ale jeśli dane na fakturze są na firme, 
          // zapisz osobę odbierającą przesyłkę
            $aDataThreeLogOut['invoice'] = $this->_addRecipientData($aDataThreeLogOut['recipient'], $aDataThreeLogOut['invoice']);
            // osoba prywatna pole company w takim przypadku to imie i nazwisko
            if ($aDataThreeLogOut['invoice']['is_company'] !== '1') {
                $aDataThreeLogOut['invoice']['company'] = '';
            }
            if (empty($aDataThreeLogOut['invoice']['phone'])) {
              $aDataThreeLogOut['invoice']['phone'] = $aUser['phone'];
            }
          if (!empty($aDataThreeLogOut['invoice'])) {
            if($this->addAddressToOrder($iOId,$aDataThreeLogOut['invoice'],'1') === false){
              return false;
            }
          }
          else {
            return false;
          }
        } else {
            if (empty($aDataThreeLogOut['delivery']['city']) || empty($aDataThreeLogOut['delivery']['postal']))
            {
                $aDataThreeLogOut['delivery'] = $this->_addRecipientData($aDataThreeLogOut['recipient'], $aDataThreeLogOut['invoice']);
            }
            else
            {
                $aDataThreeLogOut['delivery'] = $this->_addRecipientData($aDataThreeLogOut['recipient'], $aDataThreeLogOut['delivery']);
            }
            // osoba prywatna pole company w takim przypadku to imie i nazwisko
            //$aDataThreeLogOut['delivery']['company'] = '';//s$aDataThreeLogOut['delivery']['name'].' '.$aDataThreeLogOut['delivery']['surname'];
            if ($aDataThreeLogOut['delivery']['is_company'] !== '1') {
                $aDataThreeLogOut['delivery']['company'] = '';
            }
            if (empty($aDataThreeLogOut['delivery']['phone'])) {
              $aDataThreeLogOut['delivery']['phone'] = $aUser['phone'];
            }
          }

          if (!empty($aDataThreeLogOut['delivery']) && $aDataThreeLogOut['delivery']['delivery_data_from_invoice'] != '1') {
            if($this->addAddressToOrder($iOId,$aDataThreeLogOut['delivery'],'1') === false){
              return false;
            }
          }
      }
      else {
        return false;
      }
    //}
    /*
    if ($this->bIsReceptionAtPoint === TRUE && 
            $this->bSelectedInpost === FALSE) {
      $aTransportAddress = $this->createTransportAddress($aDataThreeLogOut['recipient'], $aDataThreeLogOut['invoice']);
      if($this->addAddressToOrder($iOId,$aTransportAddress,'1') === false){
        return false;
      }
    }
    
    // jesli druga faktura, dodanie adresu do zamowienia
    if (!empty($aDataTwo['item_2nd_invoice'])) {
      if (!empty($aDataThreeLogOut['second_invoice'])) {
        if($this->addAddressToOrder($iOId,$aDataThreeLogOut['second_invoice'],'2') === false){
          return false;
        }
      }
      else {
        return false;
      }
    }
    */
    return true;
  }// end of _addOrderAdressLogout() method
  

  /**
   * 
   * @param array $aRecipient
   * @param array $aInvoiceAddress
   * @return array
   */
  private function createTransportAddress($aRecipient, $aInvoiceAddress) {
    $aTransportAddress = array(
        'city' => $aInvoiceAddress['city'],
        'postal' => $aInvoiceAddress['postal'],
        'street' => $aInvoiceAddress['street'],
        'number' => $aInvoiceAddress['number'],
        'number2' => $aInvoiceAddress['number2'],
        'order_id' => $aInvoiceAddress['order_id'],
        'is_company' => '0',
        'address_type' => '1',// transport
        'is_postal_mismatch' => '0'// błędny kod
    );
    $aRecipient = $this->_addRecipientData($aRecipient, $aTransportAddress);
    return $aRecipient;
  }  
  
  /**
   * Metoda dodaje nowy adres w przypadku osoby zalogowanej
   * 
   * @param array $aData
   * @param int $iOId
   * @return boolean
   */
    private function _addOrderAdressLogin($aData, $aUser, $iOId) {
        $aDataTwo =& $aData['step_2'];
        $aDataThreeLogIn =& $aData['step_3_logged_in'];

        // dodanie adresu faktury do zamówienia
        $aInvoiceAddress = $this->getAddress($aDataThreeLogIn['invoice_address']);
        if (!empty($aInvoiceAddress)) {
            if($this->addAddressToOrder($iOId,$aInvoiceAddress,'0') === false){
                return false;
            }
        }
        else {
            return false;
        }

        if ($aDataThreeLogIn['transport_address'] > 0) {
            $aTransportAddress = $this->getAddress($aDataThreeLogIn['transport_address']);
            // dodanie adresu transportu do zamówienia
            if ($aTransportAddress['is_company'] == '0') {
                // osoba prywatna to comapny to imię i nazwisko przekopiowane
                $aTransportAddress['company'] = $aTransportAddress['name'].' '.$aTransportAddress['surname'];
            }

            if(!empty($aTransportAddress)){
                $aTransportAddress = $this->_addRecipientData($aDataThreeLogIn['recipient'], $aTransportAddress);
                if (empty($aTransportAddress['phone'])) {
                    $aTransportAddress['phone'] = $aUser['phone'];
                }
                if($this->addAddressToOrder($iOId,$aTransportAddress,'1') === false){
                    return false;
                }
            }
            else {
                return false;
            }
        } else {
            $aTransportAddress = $this->createTransportAddress($aDataThreeLogIn['recipient'], $aInvoiceAddress);
            if($this->addAddressToOrder($iOId,$aTransportAddress,'1') === false){
                return false;
            }
        }

        return true;
    }// end of _addOrderAdressLogin() method
	
  
  /**
   * Metoda dopisuje do adresu dane osoby dobierającej przesyłkę
   * 
   * @param array $aRecipientArr
   * @param array $aReturnAdress
   * @return array
   */
  private function _addRecipientData($aRecipientArr, $aReturnAdress) {
    
    
    if ($aRecipientArr['name'] != '') {
      // nadpisujemy imię
      $aReturnAdress['name'] = $aRecipientArr['name'];
    }
    if ($aRecipientArr['surname'] != '') {
      // nadpisujemy imię
      $aReturnAdress['surname'] = $aRecipientArr['surname'];
    }
    if ($aRecipientArr['phone'] != '') {
      $aReturnAdress['phone'] = $aRecipientArr['phone'];
    }
    return $aReturnAdress;
  }// end of _addRecipientData() method
  
	
	/**
	 * Metoda sprawdza czy zamówienie posiada produkt z zapowiedzią
	 * 
	 * @param array $aProductsOrder
	 * @return boolean
	 */
	function checkPreview($aProductsOrder) {
		
		foreach ($aProductsOrder as $aProduct) {
			if ($aProduct['preview'] == true) {
				return true;
			}
		}
		return false;
	}// end of checkPreview() method

	
	/**
  * Metoda sprawdza czy produkt ma dodane zalaczniki
  */
  function checkAttachments($iId){
  	global $aConfig;
  	
  	$sSql = "SELECT COUNT(*) FROM ".$aConfig['tabls']['prefix']."products_attachments WHERE product_id = '".$iId."'";
  	
  	return (double) Common::GetOne($sSql) > 0;
  } // end of checkAttachments() method

	/**
	 * Metoda zwraca kod HTML z informacjami po zlozeniu zamowienia
	 */
	function showNewOrderInfo($iOId,$bPlatnosci=false,$bOk=false) {
		global $aConfig, $pSmarty, $pDbMgr;
		
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
    $sPath = getModulePageLink('m_konta', 'sprawdzanie');
    
    $aModule['links']['browse'] = $aConfig['common']['client_client_base_url_http'];
    $aModule['links']['details'] = createLink($sPath, 'id'.$iOId, 'details');
    $aModule['script']['js'] = $_SESSION['logged_out']['js'];
        unset($_SESSION['logged_out']['js']);

		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/_new_order_info.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	}

    private function getCriteoJS($orderId) {
        global $aConfig, $pDbMgr;


        $products = $pDbMgr->GetAll("profit24", "SELECT product_id as id, oi.value_brutto as price, quantity, o.email FROM orders_items oi JOIN orders o on oi.order_id = o.id WHERE o.id = " . $orderId);

        $ids = [];
        $email = "";
        if ( empty($products) === false) {
            $email = $products[0]['email'];
            foreach ($products as $product) {
                $ids[] = json_encode([
                    "id" => $product['id'],
                    "price" => $product['price'],
                    "quantity" => $product['quantity']
                ]);
            }
        }

        $aConfig['header_js'][]="</script>";
        $aConfig['header_js'][]="<script type=\"text/javascript\">";
        $aConfig['header_js'][]="window.criteo_q = window.criteo_q || [];";
        $aConfig['header_js'][]="var deviceType = /iPad/.test(navigator.userAgent) ? \"t\" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? \"m\" : \"d\";";
        $aConfig['header_js'][]="window.criteo_q.push(";
        $aConfig['header_js'][]="{ event: \"setAccount\", account: 57447},";
        $aConfig['header_js'][]="{ event: \"setEmail\", email: \"" . $email . "\" },";
        $aConfig['header_js'][]="{ event: \"setSiteType\", type: deviceType},";
        $aConfig['header_js'][]="{ event: \"trackTransaction\", id: " . $orderId . ", item: [" . implode(',', $ids) . "]});";
        $aConfig['header_js'][]="</script>";
        $aConfig['header_js'][]="<script type=\"text/javascript\">";
    }
	
	/**
	 * Metoda zwraca kod HTML z informacjami po zlozeniu zamowienia
	 */
	function prepareNewPayment($iOId, $bIsCardPayment=false) {
		global $aConfig, $pSmarty;
		
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id = ".$iOId;
		$aData =& Common::GetRow($sSql);
		if(!empty($aData)) {
			if (empty($aData['name']) || empty($aData['surname'])) {
				$sSql = "SELECT name, surname, company FROM orders_users_addresses WHERE `address_type` = '0' AND order_id = ".$iOId;
				$aTMPData = Common::GetRow($sSql);
				$aData['name'] = trim($aTMPData['name']);
				$aData['surname'] = trim($aTMPData['surname']);
				if (empty($aData['name'])) {
					$aData['name'] = trim($aTMPData['company']);
				}
				if (empty($aData['surname'])) {
                    $aData['surname'] = ' ';//$aTMPData['company'];
				}
			}
		
			include_once ('Platnoscipl.class.php');
			$pPlatnosci = new Platnoscipl();
			$pPlatnosci->getConfig();
			$sUrl = $pPlatnosci->newOrderLink($iOId,($bIsCardPayment ? 'c': $_SESSION['wu_cart']['step_2']['payment_bank']),
																				Common::formatPrice2($aData['to_pay']-$aData['paid_amount']),
																				$aData['name'], $aData['surname'], $aData['email'],
																				sprintf($aConfig['lang']['mod_'.$this->sModule]['order_number'], $aData['order_number'], $aData['email']));
			return $sUrl;
		}
		else
			return false;
	}


	/**
	 * Metoda pobiera dane sprzedawcy
	 *
	 * @return	array ref	- tablica z danymi
	 */
	function getPaymentDetails($iOId) {
		global $aConfig;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_seller_data";						 
		$aData =& Common::GetRow($sSql);
		
		/*$sPaymentDetails = sprintf($aConfig['lang']['mod_m_zamowienia']['payment_details_prepay'],
									$aData['name'], $aData['street'], $aData['number'], 
									$aData['number2'] != '' ? '/ '.$aData['number2'] : '',
									$aData['postal'], $aData['city'], $aData['bank_name'],
									$aData['bank_account'], $iOId);
		*/
		return $aData;
		//return $sPaymentDetails;
	} // end of getSellerData method


	/**
	 * Metoda formatuje tresc maila wysylanego po zlozeniu zamowienia
	 * 
	 * @param	integer	$iOId	- Id zamowienia
	 * @param	array ref	$aSellerData	- dane sprzedajacego
	 * @param	bool	$bForUser	- true: dla uzytkownika; false: dla administratora
	 * @return	string	- sformatowany mail w formacie HTML
	 */
	function formatEmail($iOId, &$aSellerData, $bForUser=false) {
		global $aConfig, $pSmarty;
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['cart'] =& $_SESSION['wu_cart'];
		$aModule['oid'] =& $iOId;
		$aModule['seller_data'] =& $aSellerData;
		$aModule['footer'] =& $aConfig['_settings']['site']['mail_footer'];
		
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/_new_order_'.($bForUser ? 'user' : 'admin').'_email.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		
		return $sHtml;
	} // end of formatEmail() method


	/**
   * Metoda wysyla maila z zamowieniem do uzytkownika
   * 
   * @global array $aConfig
   * @global object $pSmarty
   * @param int $iId id zamówienia
   * @param int $iTypeActivation - typ aktywacji konta: 0 - bez aktywacji, 1 - aktywacja zamówienia, 2 - aktywacja konta oraz zamówienia
   * @param string $sActivationLink - adres linku aktywacji konta
   * @return bool
   */
	function sendNewOrderUserEmail($iId, $iTypeActivation = 0, $sActivationLink = '') {
		global $aConfig, $pSmarty, $pDbMgr;
		
		$aModule = array();
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		
		$sFrom = $aConfig['_settings']['site']['name'];
		$sFromMail = $aConfig['_settings']['site']['orders_email'];
		
		$sSql = "SELECT A.*, (A.transport_cost + A.value_brutto) as total_value_brutto, 
								B.mail_info AS mail_transport_info, B.symbol as transport_symbol
						 FROM ".$aConfig['tabls']['prefix']."orders A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						 ON B.id=A.transport_id
						 WHERE A.id = ".$iId;
		
		$aModule['order'] =& Common::GetRow($sSql);
    switch ($aModule['order']['transport_symbol']) {
      case 'paczkomaty_24_7':
      case 'ruch':
      case 'orlen':
      case 'poczta_polska':
        include_once('LIB/autoloader.php');
        $oShipment = new \orders\Shipment($aModule['order']['transport_symbol'], $pDbMgr);
        $aModule['order']['point_of_receipt'] = $oShipment->getSinglePointDetails($aModule['order']['point_of_receipt']);
      break;
    }
    
		$aModule['order']['activate_type'] = $iTypeActivation;
    $aModule['order']['activate_link'] = $sActivationLink;
      
		$sSubject = sprintf($aModule['lang']['new_order_user_email_subject'], $aModule['order']['order_number']);
		
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id = ".$aModule['order']['id']. " AND item_type<>'A'";
		$aModule['items'] =& Common::GetAll($sSql);
		
		/*if($aModule['order']['user_id']>0){
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."users_accounts
							 WHERE id = ".$aModule['order']['user_id'];
			$aModule['user'] =& Common::GetRow($sSql);
		} else {
			$aModule['user'] =&$_SESSION['wu_cart']['user_data'];
		}*/
		$aModule['addresses'] = $this->getOrderAddresses($iId);
		
		$aModule['shipment_info'] = $this->getShipmentInfo();
		
		$aModule['footer'] = $aConfig['_settings']['site']['mail_footer'];
		
		foreach ($aModule['items'] as $iKey => $aValue) {
			$aModule['items'][$iKey]['price_brutto'] = Common::formatPrice($aModule['items'][$iKey]['price_brutto']);
			
			if($aModule['items'][$iKey]['bundle_value_brutto'] > 0.00){
				// jeśli zdefiniowana cena bundla, to wyświetlamy, poniewaz załączniki nie wchodzą do maila
				$aModule['items'][$iKey]['value_brutto'] = Common::formatPrice($aModule['items'][$iKey]['bundle_value_brutto']);
			} else {
				$aModule['items'][$iKey]['value_brutto'] = Common::formatPrice($aModule['items'][$iKey]['value_brutto']);
			}
			
			$aModule['items'][$iKey]['promo_price_brutto'] = Common::formatPrice2($aModule['items'][$iKey]['promo_price_brutto']);

            $pShipmentTime = new \orders\Shipment\ShipmentTime();
            $aModule['items'][$iKey]['shipment_time'] = $pShipmentTime->getShipmentTime($aValue['id'], $aValue['shipment_time']);
			if($aValue['preview']=='1'){
				$aModule['items'][$iKey]['shipment_date'] = FormatDate($aValue['shipment_date']);
			}
		}

		$aModule['order']['value_brutto'] = Common::formatPrice($aModule['order']['value_brutto']);
		$aModule['order']['transport_cost'] = Common::formatPrice($aModule['order']['transport_cost']);
		$aModule['order']['total_value_brutto'] = Common::formatPrice($aModule['order']['total_value_brutto']);
		$aModule['order']['to_pay'] = Common::formatPrice($aModule['order']['to_pay']);
    $aModule['order']['check_status_link'] = $aConfig['common']['client_base_url_http_no_slash'].'/sprawdz-status-zamowienia?check_status_key='.$aModule['order']['check_status_key']."&email=".$aModule['order']['email'];
		//$aModule['order']['transport'] = $aModule['lang']['kurier'];

		$aModule['seller_data'] = $this->getPaymentDetails($iId);
		
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch('modules/m_zamowienia/_new_order_user_email.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		
		//return Common::sendMail($sFrom, $sFromMail, $aModule['user']['email'], $sSubject, $sHtml, true);
		$aHtmlImages = array();
		if(!empty($aConfig['common']['email_logo_file'])){
			$sImgF = $aConfig['common']['client_base_path'].$aConfig['common']['email_logo_file'];
			$aImage = getimagesize($sImgF);
			$aHtmlImages[] = array($sImgF, $aImage['mime']);
		}
		return Common::sendTxtHtmlMail($sFrom, $sFromMail, $aModule['order']['email'], $sSubject, '', $sHtml, '', '', $aHtmlImages);
	}


	/**
	 * Metoda wysyla maila informujacego o zlozeniu nowego zamowienia
	 * przez uzytkownika
	 *
	 * @param	integer	$iOId	- Id zamowienia
	 * @return	bool	- true: mail wyslany; false: mail nie wyslany
	 */
	function sendNewOrderAdminEmail($iId, $iTypeActivation = 0, $sActivationLink = '') {
		global $aConfig, $pSmarty;
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		
		$sFrom = $aConfig['_settings']['site']['name'];
		$sFromMail = $aConfig['_settings']['site']['orders_email'];
		
		$sTo = $aConfig['_settings']['site']['orders_email'];
		
		
		$sSql = "SELECT A.*, (A.transport_cost + A.value_brutto) as total_value_brutto, B.mail_info AS mail_transport_info, B.symbol as transport_symbol
						 FROM ".$aConfig['tabls']['prefix']."orders A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						 ON B.id=A.transport_id
						 WHERE A.id = ".$iId;
		
		$aModule['order'] =& Common::GetRow($sSql);
		
		if ($aModule['order']['transport_symbol'] == 'paczkomaty_24_7') {
			// pobierz paczkomat
			include_once('paczkomaty/inpost.php');
			$machines_all = inpost_get_machine_list();
			$machines_with_names_as_keys = array();
			
			if (count($machines_all)) {
				foreach ($machines_all as $id => $machine) {
					$machines_with_names_as_keys[$machine['name']] = $machine;
				}
			}
			
			if (!empty($machines_with_names_as_keys)) {
				foreach ($machines_with_names_as_keys as $machine) {
					if ($aModule['order']['point_of_receipt'] == $machine['name']) {
						$aModule['order']['point_of_receipt'] = $machine;
					}
				}
			}
		}
    
		$aModule['order']['activate_type'] = $iTypeActivation;
    $aModule['order']['activate_link'] = $sActivationLink;
    
		$sSubject = sprintf($aModule['lang']['new_order_user_email_subject'], $aModule['order']['order_number']);
		
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id = ".$aModule['order']['id']. " AND item_type<>'A'";
		$aModule['items'] =& Common::GetAll($sSql);
		
		$aModule['addresses'] = $this->getOrderAddresses($iId);
		
		$aModule['shipment_info'] = $this->getShipmentInfo();
		
		$aModule['footer'] = $aConfig['_settings']['site']['mail_footer'];
		
		foreach ($aModule['items'] as $iKey => $aValue) {
			$aModule['items'][$iKey]['price_brutto'] = Common::formatPrice($aModule['items'][$iKey]['price_brutto']);
			
			if($aModule['items'][$iKey]['bundle_value_brutto'] > 0.00){
				// jeśli zdefiniowana cena bundla, to wyświetlamy, poniewaz załączniki nie wchodzą do maila
				$aModule['items'][$iKey]['value_brutto'] = Common::formatPrice($aModule['items'][$iKey]['bundle_value_brutto']);
			} else {
				$aModule['items'][$iKey]['value_brutto'] = Common::formatPrice($aModule['items'][$iKey]['value_brutto']);
			}
			
			$aModule['items'][$iKey]['promo_price_brutto'] = Common::formatPrice2($aModule['items'][$iKey]['promo_price_brutto']);
            $pShipmentTime = new \orders\Shipment\ShipmentTime();
            $aModule['items'][$iKey]['shipment_time'] = $pShipmentTime->getShipmentTime($aValue['id'], $aValue['shipment_time']);
			if($aValue['preview']=='1'){
				$aModule['items'][$iKey]['shipment_date'] = FormatDate($aValue['shipment_date']);
			}
		}

		$aModule['order']['value_brutto'] = Common::formatPrice($aModule['order']['value_brutto']);
		$aModule['order']['transport_cost'] = Common::formatPrice($aModule['order']['transport_cost']);
		$aModule['order']['total_value_brutto'] = Common::formatPrice($aModule['order']['total_value_brutto']);
		$aModule['order']['to_pay'] = Common::formatPrice($aModule['order']['to_pay']);
    $aModule['order']['check_status_link'] = $aConfig['common']['client_base_url_http_no_slash'].'/sprawdz-status-zamowienia?check_status_key='.$aModule['order']['check_status_key']."&email=".$aModule['order']['email'];
		//$aModule['order']['transport'] = $aModule['lang']['kurier'];

		$aModule['seller_data'] = $this->getPaymentDetails($iId);
		
					
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch('modules/m_zamowienia/_new_order_user_email.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		
		$aHtmlImages = array();
		if(!empty($aConfig['common']['email_logo_file'])){
			$sImgF = $aConfig['common']['client_base_path'].$aConfig['common']['email_logo_file'];
			$aImage = getimagesize($sImgF);
			$aHtmlImages[] = array($sImgF, $aImage['mime']);
		}
		return Common::sendTxtHtmlMail($sFrom, $sFromMail, $sTo, $sSubject, '', $sHtml, '', '', $aHtmlImages);
	} // end of sendNewOrderAdminEmail() method

	/**
	 * Metoda jest wykonywana tylko wtedy gdy nastepuje zmiana statusu DotPay
	 *
	 * @return vaoid
	 */
	function setDotPayTransactionStatus() {
		global $aConfig;
		$bIsErr = false;
		$bPayed = false;

		$_GET['no_output'] = true;

		if (!empty($_POST)) {
			
			if ($_POST['md5'] == md5(getDotPayPin().':'.getDotPayId().':'.$_POST['control'].':'.$_POST['t_id'].':'.$_POST['amount'].':'.$_POST['email'].':::::'.$_POST['t_status'])) {

				$aControlData = explode(':', base64_decode($_POST['control']));
				
				if (($aOrder =& $this->getOrderData($aControlData[0])) !== false) {
					if ($aOrder['dotpay_t_status'] != $_POST['t_status']) {
						Common::BeginTransaction();
						$aValues = array(
							'dotpay_t_date' => 'NOW()',
							'dotpay_t_id' => $_POST['t_id'],
							'dotpay_t_status' => $_POST['t_status']
						);
						// aktualizacja tylko gdy status z DotPay inny z zapisanym
						if (intval($_POST['t_status']) == 2) {
							// status zamowienia - zrealizowane
							$aValues['payment_status'] = '1';
							$aValues['payment_date'] = 'NOW()';
							$aValues['paid_amount'] = Common::formatPrice2($_POST['amount']);
							$bPayed = true;
						} elseif (intval($_POST['t_status']) == 3){
							$aValues['payment_status'] = '2';
						}
						
						$aOrder = $this->getOrderInternalStatus($aControlData[0]);
						/***  statusy wewnętrzne   ***/
						if(!empty($aOrder)){
							if($aOrder['internal_status'] == '0'){
								$aValues['internal_status'] = '1'; // nowe
							}
							elseif($aOrder['internal_status'] == '4'){
								if($aValues['paid_amount'] >= $aOrder['to_pay']){
									if($aOrder['personal_reception'] == '1'){
										$aValues['internal_status'] = '6'; // odbiór osobisty
									} else {
										$aValues['internal_status'] = '5'; // wysyłka
									}
								}
							}
						}
						
						$bIsErr = Common::Update($aConfig['tabls']['prefix']."orders", $aValues, "id = ".$aControlData[0]) === false;
						if ($bIsErr) {
							Common::RollbackTransaction();
						}
						else {
							Common::CommitTransaction();
						}
						if(!$bIsErr && $bPayed){
							$this->sendOrderPayedMail($aControlData[0]);
						}
					}
				}
				else {
					$bIsErr = true;
				}
				if (!$bIsErr) {
					echo "OK";
				}
			}
		}
	} // end of setDotPayTransactionStatus() method
	
	/**
	 * Metoda jest wykonywana gdy nastepuje zmiana statusu Platnosci.pl
	 *
	 * @return void
	 */
	function setPlatnosciTransactionStatus() {
		global $aConfig, $pDbMgr;
		$bIsErr = false;

		$_GET['no_output'] = true;

		$hourLockStart = '0400';
        $hourLockStop = '0410';
        $hourCurrent = date('Hi');
        if ($hourCurrent == '2359' || ($hourCurrent >= $hourLockStart && $hourCurrent <= $hourLockStop)) {
            die('ERROR: TIME LOCK');
        }

		if(!isset($_POST['pos_id']) || !isset($_POST['session_id']) || !isset($_POST['ts']) || !isset($_POST['sig'])) {
			die('ERROR: INCOMPLETE PARAMS'); //-- brak wszystkich parametrow
		} else {
			include_once ('Platnoscipl.class.php');
			$pPlatnosci = new Platnoscipl();
			$pPlatnosci->getConfig();
			if ($_POST['pos_id'] != $pPlatnosci->pos_id){
				die('ERROR: WRONG POS');   //--- bledny numer POS
			}
			
			$sig = md5( $_POST['pos_id'] . $_POST['session_id'] . $_POST['ts'] . $pPlatnosci->key2);
			
			if ($_POST['sig'] != $sig){
				die('ERROR: WRONG SIGNATURE');   //--- bledny podpis
			}
			// odpytujemy platnosci o stan
			$aStatus = $pPlatnosci->getPaymentStatus($_POST['session_id']);
//			$fp = fopen($aConfig['common']['base_path'].$aConfig['common']['cms_dir'].'logs/payment'.date('dmY').'.log', 'a');
//			fwrite($fp, print_r($aStatus,true));
//			
			// jesli poszlo ok
			if ($aStatus !== false) {
				$sig = md5($aStatus['trans']['pos_id'] . $aStatus['trans']['session_id'] .
									 $aStatus['trans']['order_id'] . $aStatus['trans']['status'] .
									 $aStatus['trans']['amount'] . $aStatus['trans']['desc'] .
									 $aStatus['trans']['ts'] . $pPlatnosci->key2);
				// sprawdzenie poprawnosci podpisu
				if ($aStatus['status'] == 'OK' && $aStatus['trans']['sig'] == $sig) {
					$aCurPayment = $pPlatnosci->getPaymentSession($aStatus['trans']['session_id']);
					//fwrite($fp, print_r($aCurPayment,true));
					// jesli sesja platnosc i w bazie i stan rozni sie od nowego
					if (!empty($aCurPayment) && $aStatus['trans']['status'] != $aCurPayment['status']) {
						Common::BeginTransaction();
						$aValues = array();
						$bIsErr = false;
						// aktualizacja sesji platnosci
						if ($pPlatnosci->updatePaymentSession($aStatus['trans']['session_id'],$aStatus['trans'])) {
							// status - transakcja zakonczona
							// pobranie danych zamowienia
							$aOrder = $this->getOrderInternalStatus($aStatus['trans']['order_id']);
							if ($aStatus['trans']['status'] == 99) {
                include_once('Orders/ShipmentDateExpected.class.php');
                $oOrderShipmentDateExpected = new ShipmentDateExpected($pDbMgr, $aConfig);
                $dDate = $oOrderShipmentDateExpected->getOrderExpectedShipmentDate($aStatus['trans']['order_id']);
                
                $aValues['active'] = '1'; // od razu jest aktywne po opłaceniu
                $aValues['activation_date'] = 'NOW()';
                $aValues['activation_by'] = 'payu';
                $aValues['shipment_date_expected'] = $dDate != '' ? $dDate : 'NULL';
                
								/*
								$sSql = "SELECT email FROM orders WHERE id=".$aStatus['trans']['order_id'];
								$sEmail = Common::GetOne($sSql);
								if ($sEmail == $aConfig['common']['platosci_pl_test_mode_email']) {
								*/	
									// jeśli to zamówienie zostało już opłacone 1 metodą płatnosci
									// to dodajemy drugą metodę płatności
									$sSql = "SELECT payment_status, transport_id, IF(second_payment_enabled='1', second_payment_type, payment_type) AS payment_type 
													 FROM ".$aConfig['tabls']['prefix']."orders 
													 WHERE id=".$aStatus['trans']['order_id'];
									$aOldOrder = Common::GetRow($sSql);

									if ($aOldOrder['payment_status'] != '1') {
										// niezrealizowane
										$aValues['payment_status'] = '1';
										$aValues['payment_date'] = 'NOW()';
									} else {
										$aValues['second_payment_enabled'] = '1';
										$aValues['second_payment_status'] = '1';
										$aValues['second_payment_date'] = 'NOW()';
										if ($aOldOrder['payment_type'] != 'card' ) {
											$aValues['second_payment'] = 'przelew elektroniczny';
											$aValues['second_payment_type'] = 'platnoscipl';
											$aValues['second_payment_id'] = $aOldOrder['transport_id']==2?'11':'9';
										} else {
											$aValues['second_payment'] = 'Karta kredytowa';
											$aValues['second_payment_type'] = 'card';
											$aValues['second_payment_id'] = 10;
										}
									}
									/*
								} else {
										$aValues['payment_status'] = '1';
										$aValues['payment_date'] = 'NOW()';
								}
									 */
								$aValues['paid_amount'] = $aOrder['paid_amount'] + Common::formatPrice2($pPlatnosci->zlotowki($aStatus['trans']['amount']));
								/***  statusy wewnętrzne   ***/
								
								if(!empty($aOrder)){
									if($aOrder['internal_status'] == '0'){
										$aValues['internal_status'] = '1'; // nowe
									}
									elseif($aOrder['internal_status'] == '4'){
										if($aValues['paid_amount'] >= $aOrder['to_pay']){
											if($aOrder['personal_reception'] == '1'){
												$aValues['internal_status'] = '6'; // odbiór osobisty
											} else {
												$aValues['internal_status'] = '5'; // wysyłka
											}
										}
									}
								}
								/***  /statusy wewnętrzne   ***/
								//fwrite($fp, print_r($aValues,true));
								if(Common::Update($aConfig['tabls']['prefix']."orders", $aValues, "id = ".$aStatus['trans']['order_id']) === false){
									$bIsErr = true;
								}
								if (!$bIsErr) {
									$this->sendOrderPayedMail($aStatus['trans']['order_id']);
								}
							}
							// status - anulowane
							elseif($aOrder['payment_status'] != '1' && $aStatus['trans']['status'] == 2) {
								$aValues['payment_status'] = '2';
								//fwrite($fp, print_r($aValues,true));
								if(Common::Update($aConfig['tabls']['prefix']."orders", $aValues, "id = ".$aStatus['trans']['order_id']) === false){
									$bIsErr = true;
								}
							}
						}
						else {
							$bIsErr = true;
						}
						// jesli blad wycofujemy
						if ($bIsErr) {
              echo 'ERR 1';
							Common::RollbackTransaction();
              exit(0);
						}
						// jesli nie - zatwierdzamy
						else {
							Common::CommitTransaction();
						}
					}
				}
			}
				// odpowiedz dla platnosci
				echo "OK";
			//fclose($fp);
		}
	} // end of setPlatnosciTransactionStatus() method

	/**
	 * Pobiera dane dla statusu wewn
	 * @param $iId - id zamówienia
	 * @return array
	 */
	function getOrderInternalStatus($iId){
		global $aConfig;
		$sSql = "SELECT A.internal_status, A.payment_status, A.payment, A.value_brutto, 
										A.transport_cost, B.personal_reception, A.to_pay, A.paid_amount
						FROM ".$aConfig['tabls']['prefix']."orders A
						LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						ON B.id=A.transport_id
						WHERE A.id = ".$iId;
		return Common::GetRow($sSql);
	} // end of getOrderInternalStatus() method

	/**
	 * Metoda pobiera dane zamowienia o podanym Id
	 *
	 * @param	integer	$iId	- Id zamowienia
	 * @return	array ref	- dane zamowienia
	 */
	function &getOrderData($iId) {
		global $aConfig;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id = ".$iId;
		return Common::GetRow($sSql);
	} // end of getOrderData() method
  
  
	/**
	 * Metoda pobiera dane zamowienia o podanym Id
	 *
   * @param array   $aCols - tablica kolumn
	 * @param	integer	$iId	- Id zamowienia
	 * @return	array ref	- dane zamowienia
	 */
	function &getOrderDataParam($aCols, $iId) {
		global $aConfig;

		$sSql = "SELECT ".implode(', ', $aCols)."
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id = ".$iId;
		return Common::GetRow($sSql);
	} // end of getOrderData() method
  
  
	/**
	 * Metoda pobiera dane składowych zamówienia o id zamówienia
	 *
   * @param array   $aCols - tablica kolumn
	 * @param	integer	$iId	- Id zamowienia
	 * @return	array ref	- dane zamowienia
	 */
	function &getOrderItemsDataParamByOrderId($aCols, $iId) {
		global $aConfig;

		$sSql = "SELECT ".implode(', ', $aCols)."
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id = ".$iId;
		return Common::GetAll($sSql);
	} // end of getOrderItemsDataParamByOrderId() method


 /**
  * Metoda pobiera informacje o zalogowanym uzytkowniku
  */
  function getUserData() {
  	global $aConfig;
  	
  	if(empty($_SESSION['wu_cart']['user_data']) && $_SESSION['w_user']['id'] > 0) {
	  	$sSql = "SELECT *
	  			 		 FROM ".$aConfig['tabls']['prefix']."users_accounts
	  			 		 WHERE id = ".$_SESSION['w_user']['id']."";
	  	$aUserData=& Common::GetRow($sSql);
	  	$_SESSION['wu_cart']['user_data']['name'] = $aUserData['name'];
			$_SESSION['wu_cart']['user_data']['surname'] = $aUserData['surname'];
			$_SESSION['wu_cart']['user_data']['phone'] = $aUserData['phone'];
			$_SESSION['wu_cart']['user_data']['email'] = $aUserData['email'];
      $_SESSION['wu_cart']['user_data']['seller_streamsoft_id'] = $aUserData['seller_streamsoft_id'];
  	}
  } // end of getUserData() method
  
  


	/**
	 * Metoda zwraca formularz z ukrytymi polami niezbednymi do dokonania platnosci dotpay.pl
	 */
	function getDotpayForm($aData){
 		global $aConfig;
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		
		$aValues = array(
			'id'	=> getDotPayId(),
			'amount'	=> Common::formatPrice2($aData['value_brutto'] + $aData['transport_cost']),
			'currency'	=>	'PLN',
			'description'	=>	sprintf($aModule['lang']['order_number'], $aData['order_number'], $aData['email']),
			'lang'	=>	'pl',
			'URL'	=> $aConfig['common']['base_url_https_no_slash'].createLink($aConfig['_tmp']['cart_symbol'], '', 'dotpay'),
			'URLC'	=> $aConfig['common']['base_url_https_no_slash'].createLink($aConfig['_tmp']['cart_symbol'], '', 'dotpay_c'),
			'type'	=>	3,
			'control'	=> base64_encode($aData['id'].':'.md5($_SESSION['w_user']['email'])),
			'firstname'	=> $aData['name'],
			'lastname'	=> $aData['surname'],
			'email'	=>	$aData['email'],
			'street'	=>	$aData['street'],
			'street_n1'	=>	$aData['number'],
			'street_n2'	=>	$aData['number2'],
			'city'	=>	$aData['city'],
			'postcode'	=>	$aData['postal'],
			'country'	=> 'POL',
			'onlinetransfer'	=> '0',
			'podatek'	=> '1'
		);
	 	return $aValues;
	} // end of getDotpayForm method


	/**
	 * Metoda pobiera autorow produktu o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @return	array	- autorzy produktu
	 */
	function getAuthorsStr($iId) {
		global $aConfig;
		
		// pobranie autorow ksiazki
		$sSql = "SELECT B.name as role_name, CONCAT(C.surname,' ',C.name) as author
						FROM ".$aConfig['tabls']['prefix']."products_to_authors A
						JOIN ".$aConfig['tabls']['prefix']."products_authors_roles B
						ON B.id = A.role_id
						JOIN ".$aConfig['tabls']['prefix']."products_authors C
						ON C.id = A.author_id
						WHERE A.product_id = ".$iId."
						ORDER BY B.order_by";
						
		$aResult =& Common::GetAll($sSql);
		$sAuthors='';
	
		foreach($aResult as $aAuthor){
			$sAuthors .= $aAuthor['author'].', ';
		}
		if(!empty($sAuthors))
			$sAuthors=substr($sAuthors,0,-2);
		return $sAuthors;
	} // end of getAuthorsStr() method


    /**
     * Pobiera i generuje kolejny nr zamówienia
     * @return string - nr zamówienia
     */
    function getOrderNumber() {
        global $aConfig, $pDbMgr;

        //Common::Query("LOCK TABLES orders_numbering WRITE;");

        $sSql="SELECT * FROM ".$aConfig['tabls']['prefix']."orders_numbering FOR UPDATE";
        $aNumber = $pDbMgr->GetRow('profit24', $sSql);

        if ($aNumber !== FALSE) {
            if(!empty($aNumber) && $aNumber['number_date'] == date('Ymd')){
                $sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders_numbering SET id=id+1";
                if ($pDbMgr->Query('profit24', $sSql)===false){
                    //Common::Query("UNLOCK TABLES");
                    return false;
                } else{
                    //Common::Query("UNLOCK TABLES");
                    return sprintf("24%02d%02d%04d%02d", date('m'), date('d'), $aNumber['id'], substr(date('Y'), -2));
                }
            } else {
                // czyścimy wszystko z tabeli numeracji, pierwsze zamówienie w tym dniu
                $sSql = "DELETE FROM orders_numbering";
                @$pDbMgr->Query('profit24', $sSql);

                $aValues = array(
                    'id' => 2,
                    'number_date' => date('Ymd'),
                    'number_year' => date('Y'),
                );
                if ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."orders_numbering", $aValues,'',false) === false) {
                    return false;
                } else {
                    return sprintf("24%02d%02d%04d%02d", date('m'), date('d'), 1, substr(date('Y'), -2));
                }
            }
        } else {
            throw new Exception("Wystąpił błąd podczas przyznawania nowego numeru");
        }
    } // end of getOrderNumber() method


    /**
     * @return string
     * @throws Exception
     */
	function getOrderNumberRandom() {
		global $pDbMgr;
		
        $pDbMgr->BeginTransaction('profit24');
		$sSql='SELECT rand
               FROM orders_numbering_rand
               WHERE `date` = '.date('Ymd').'
               FOR UPDATE
		       ';
		$currentRandomNumbers = $pDbMgr->GetCol('profit24', $sSql);
        if (empty($currentRandomNumbers)) {
            $currentRandomNumbers = [];
        }

        do {
            $newRandom = mt_rand(1,9999);
        } while (in_array($newRandom, $currentRandomNumbers));
        $aValues = [
            'rand' => $newRandom,
            'date' => date('Ymd')
        ];
        if (false === $pDbMgr->Insert('profit24', 'orders_numbering_rand', $aValues)){
            $pDbMgr->RollbackTransaction('profit24');
            throw new \Exception('88829 - Wystąpił błąd podczas losowania kolejnego numeru zamówienia ');
        }
        $pDbMgr->CommitTransaction('profit24');
        return sprintf("24%02d%02d%04d%02d", date('m'), date('d'), $newRandom, substr(date('Y'), -2));
	} // end of getOrderNumber() method
  
	
	/**
	 * Loguje sesję koszyka i usera do pliku
	 * @return void
	 */
	function logUserCart($iOId) {
		global $aConfig;
    
    $aValues = array(
      'oid' => $iOId,
      'order_session' => serialize($_SESSION['wu_cart'])
    );
    @Common::Insert('orders_debug', $aValues, '', false);
    
		//$fp = fopen($aConfig['common']['base_path'].$aConfig['common']['cms_dir'].'logs/orders'.date('dmY').'.log', 'a');
		//fwrite($fp, '['.date("d.m.Y H:m:s").'] '.serialize($_SESSION['w_user']).' | '.serialize($_SESSION['wu_cart'])."\n");
		//fclose($fp);
	} // end of logUserCart() method
	
	
	/**
	 * Pobiera listę adresów użytkownika
	 * @return array ref - lista adresów
	 */
	function &getAddresses($bInvoice = false) {
		global $aConfig;
		$aAddresses = array();
		
		/*
		$sWhereSql = '';
		if ($bInvoice == true) {
			$sWhereSql = ' AND (is_invoice = "1" OR (is_invoice = "0" AND is_transport="0")) ';
		}
		*/
		if ($_SESSION['w_user']['id'] > 0) {
			$sSql = "SELECT id as value, address_name as label
								FROM ".$aConfig['tabls']['prefix']."users_accounts_address
								WHERE user_id = ".$_SESSION['w_user']['id']." ".$sWhereSql."
								ORDER BY id ASC";
			$aAddresses =& Common::GetAll($sSql);
		}
		return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aAddresses);
	} // end of getAddresses() method


	/**
	 * Pobiera id domyślnego adresu
	 * @param bool $bInvoice - dla faktury
	 * @param bool $bTransport - dla transportu
	 * @return int - id adresu
	 */
	function getDefaultAddressId($bInvoice=false,$bTransport=false){
		global $aConfig;
    
    $sOrderBy = '';
    if ($bInvoice == TRUE || $bTransport == TRUE) {
      $sOrderBy .= ' ORDER BY ';
      if ($bInvoice == TRUE) {
        $sOrderBy .= ' default_invoice DESC ';
      }
      if ($bTransport == TRUE) {
        $sOrderBy .= ' default_transport DESC ';
      }
    }
		
		$iUAId = '';
		if ($_SESSION['w_user']['id'] > 0 ) {
			$sSql = "SELECT id
								FROM ".$aConfig['tabls']['prefix']."users_accounts_address
								WHERE user_id = ".$_SESSION['w_user']['id'].
								($bInvoice?" AND (is_invoice = '1' OR default_invoice='1')":'').
								($bTransport?" AND (is_transport = '1' OR default_transport='1')":'')
								.$sOrderBy;
			$iUAId = intval(Common::GetOne($sSql));
		}
		return $iUAId;
	} // end of getDefaultAddressId() method


	/**
	 * Pobiera dane adresu z bazy
	 * @param int $iId - id adresu
	 * @return array ref
	 */
	function &getAddress($iId, $sType = ''){
		global $aConfig;

		/*
		$sWhereSql = '';
		if ($sType == 'invoice') {
			$sWhereSql = ' AND (is_invoice = "1" OR (is_invoice = "0" AND is_transport = "0")) ';
		}
		*/
		$aUAdrr = array();
		if ($iId > 0 && $_SESSION['w_user']['id'] > 0 ) {
			$sSql = "SELECT * 
								FROM ".$aConfig['tabls']['prefix']."users_accounts_address 
								WHERE id = ".$iId.
								" AND user_id = ".$_SESSION['w_user']['id'].
								$sWhereSql; 
			$aUAdrr = Common::GetRow($sSql);
		}

        unset($aUAdrr['address_checksum']);
		return $aUAdrr;
	} // end of getAddress() method


	/**
	 * Pobiera dane adresowe z zamówienai z bazy
	 * @param int $iId - id adresu
	 * @return array ref
	 */
	function &getOrderAddresses($iId){
		global $aConfig;

		$sSql = "SELECT address_type, is_company, company, name, surname, street, number, number2, postal, city, nip, phone
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
						 " ORDER BY address_type"; 
		return Common::GetAssoc($sSql);
	} // end of getOrderAddresses() method


	/**
	 * Dodanie adresu do zamówienia
	 * @param int $iOId - id zamówienia
	 * @param array ref $aAddress - adres
	 * @param int $iAddressType - typ adresu
	 * @return bool - success
	 */
	function addAddressToOrder($iOId,&$aAddress,$iAddressType){
	global $aConfig;

		if($aAddress['id'] != '') {
      $aAddress['user_adress_id'] = $aAddress['id'];
    }
    unset($aAddress['id']);
		unset($aAddress['address_name']);
		unset($aAddress['user_id']);
		unset($aAddress['is_invoice']);
		unset($aAddress['is_transport']);
		unset($aAddress['created']);
		unset($aAddress['modified']);
    unset($aAddress['delivery_data_from_invoice']);
		$aAddress['order_id'] = $iOId;
		$aAddress['address_type'] = $iAddressType;
		$aAddress['is_postal_mismatch'] = ($this->checkCityPostal($aAddress['city'],$aAddress['postal'])?'0':'1');
		if(Common::Insert($aConfig['tabls']['prefix']."orders_users_addresses",	$aAddress) === false) {
			return false;
		}
		return true;
	}
	
	function sendOrderPayedMail($iId) {
		global $aConfig, $pSmarty;
		
		$sFrom = $aConfig['_settings']['site']['name'];
		$sFromMail = $aConfig['_settings']['site']['orders_email'];
		
		$sSql = "SELECT *, (transport_cost + value_brutto) as total_value_brutto, DATE_FORMAT(order_date, '".$aConfig['common']['sql_date_hour_format']."') as order_date, DATE_FORMAT(status_1_update, '".$aConfig['common']['sql_date_hour_format']."') as status_1_update, DATE_FORMAT(status_2_update, '".$aConfig['common']['sql_date_hour_format']."') as status_2_update, DATE_FORMAT(status_3_update, '".$aConfig['common']['sql_date_hour_format']."') as status_3_update
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id = ".$iId;
		$aModule['order'] =& Common::GetRow($sSql);
		$aVars = array('{nr_zamowienia}','{data_statusu}','{kwota}');
		$aValues = array($aModule['order']['order_number'],$aModule['order']['payment_date'],Common::formatPrice($aModule['order']['paid_amount']));

		$aMail = Common::getWebsiteMail('status_oplacone', 'profit24');
		return Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,$aModule['order']['email'],  $aMail['content'],$aVars,$aValues,array(), array(), 'profit24');
	} // end of sendUserPasswordEmail() method
	
	// metoda weryfikuje, czy przekazany kod pocztowy jest poprawnym kodem dla przekazanej miejscowości
	function checkCityPostal($sCity,$sPostal){
		global $aConfig;
		$sSql = "SELECT count(1)
						FROM ".$aConfig['tabls']['prefix']."users_accounts_postals
						WHERE city = ".Common::Quote(stripslashes($sCity)).
						 " AND postal = ".Common::Quote(stripslashes($sPostal));
		return (intval(Common::GetOne($sSql)) > 0);
	}
  
  /**
   * Metoda sprawdza czy dane osoby odbierającej powinny być wymagane
   *  na podstawie tablicy danych z kroku 3 dla osoby nie zalogowanej
   * 
   * @param type $aDataStepThreeLogOut
   * @return boolean
   */
  public function checkRecipientRequired($aDataStepThreeLogOut) {

    if (!empty($aDataStepThreeLogOut)) {
          if ($aDataStepThreeLogOut['invoice']['is_company'] && $aDataStepThreeLogOut['delivery']['delivery_data_from_invoice'] == '1') {
            // faktura dla firmy i dane do dostawy takie jak dla faktury
            return true;
          } elseif ($aDataStepThreeLogOut['delivery']['delivery_data_from_invoice'] == '2' && $aDataStepThreeLogOut['delivery']['is_company']) {
            // dane do dostawy inne niż do faktury i dane do dostawy dla firmy
            return true;
          } else {
            return false;
          }
    } else {
      return false;
    }
  }// end of checkRecipientRequired() method
  
  
  /**
   * Metoda pobiera szczegóły przekazanego paczkomatu
   * 
   * @global type $pDbMgr
   * @param type $sCode
   * @return type
   */
  public function getPaczkomatyMachinesDetail($sCode) {
    global $pDbMgr;
    
    $sSql = "SELECT data 
             FROM paczkomaty_machines 
             WHERE code='".$sCode."'";
    return unserialize($pDbMgr->GetOne($sSql));
  }// end of getPaczkomatyMachinesDetail() method
  
  
  /**
   * Metoda sprawdza wszystkie kroki koszyka pod katem 
   *  poprawnosci wprowadzonych danych
   * 
   * @return boolean
   */
  protected function __checkValidAllSteps() {
    
    try {
      $oStepOne = $this->getStepOneObj();
      $oStepOne->validateStepOne($_SESSION['wu_cart']);
    } catch (Exception $e) {
      setMessage($e->getMessage(), true, true);
      doRedirectHttps($this->sPageLink);
      return false;
    }
    
    if ($_SESSION['wu_cart']['payment']['transport_id'] > 0) {
      $sTransportSymbol = $this->getTransportMeansSymbol($_SESSION['wu_cart']['payment']['transport_id']);
    } else {
      $sTransportSymbol = '';
    }
    
    try {
      $oStepTwo = $this->getStepTwoObj();
      $oStepTwo->validateStepTwo($_SESSION['wu_cart'], $sTransportSymbol);
    } catch (Exception $e) {
      setMessage($e->getMessage(), true, true);
      doRedirectHttps(createLink($this->sPageLink, 'step2'));
      return false;
    }
    
    try {
      $oStepThree = $this->getStepThreeObj();
      $oStepThree->validateStepThree($_SESSION['wu_cart']);
    } catch (Exception $e) {
      setMessage($e->getMessage(), true, true);
      doRedirectHttps(createLink($this->sPageLink, 'step3'));
      return false;
    }
    
    return true;
  }// end of __checkValidAllSteps() method
  
  /**
   * Metoda aktywuje zamówienie
   * 
   * @global object $pDbMgr
   * @param int $iOId
   * @return bool
   */
  private function _activate($iOId) {
    global $pDbMgr, $aConfig;
    
    // jeśli internal status = '0' jeśli typ zamówienia inny niż playnoscipl lub karta kretydowa 
    // to należy zmienić internal status na = '1'
    include_once('Orders/ShipmentDateExpected.class.php');
    $oOrderShipmentDateExpected = new ShipmentDateExpected($pDbMgr, $aConfig);
    $dDate = $oOrderShipmentDateExpected->getOrderExpectedShipmentDate($iOId);
    
    $aOrderData = $this->getOrderDataParam(array('internal_status', 'payment_type', 'active'), $iOId);
    if ($aOrderData['active'] == '1') {
      return true;// tutaj sobie wcześniej wyskakujemy jeśli jest już aktywne
    }
    
    $aValues = array(
        'active' => '1',
        'activation_date' => 'NOW()',
        'activation_by' => 'link aktywacyjny',
        'shipment_date_expected' => ($dDate != '' ? $dDate : 'NULL')
    );
    if ($aOrderData['internal_status'] == '0' && $aOrderData['payment_type'] != 'platnoscipl' && $aOrderData['payment_type'] != 'card' && $aOrderData['payment_type'] != 'payu_bank_transfer') {
      // wskakuje następny status
      $aValues['internal_status'] = '1';
    }
    return $pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOId);
  }// end of _activate() method
} // end of Module Class
?>
