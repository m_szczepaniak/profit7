<?php

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-05-20 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class phpTestRecount {

  function __construct() {
    $aPrices = array();
    for ($i = 0; $i<50; $i++) {
      $fPost = number_format(100/rand(1, 99), 2);
      $fPref = rand(10, 2000);
      array_push($aPrices, $fPref+$fPost);
    }
    var_dump($aPrices);
    $fDiscountSum = 20.00;
    var_dump($this->getAllPriceList($aPrices, $fDiscountSum));
  }
  
  /**
   * Metoda przelicza wszystkie ceny
   * 
   * @param array $aPrices
   * @param float $fDiscountSum
   */
  public function getAllPriceList($aPrices, $fDiscountSum) {
    
    bcscale(2);
    $fSumPrice = 0.00;
    $fSumPriceAfterRabat = 0.00;
    // obliczmy rabat na wszystkie pozycje
    foreach ($aPrices as $fPrice) {
      $fSumPrice = bcadd($fSumPrice, $fPrice);
    }
    $fMul = bcmul($fDiscountSum,100, 16);
    $fDiv = bcdiv($fMul, $fSumPrice, 16);
    $fDiscountPercent = round($fDiv, 4, PHP_ROUND_HALF_DOWN);
    var_dump($fDiscountPercent);
    var_dump($fSumPrice);
    $aNewPrices = array();
    foreach ($aPrices as $iKey => $fPrice) {
      $fPriceAfterRabat = number_format((double)$this->getOnePrice($fPrice, $fDiscountPercent), 2, '.', '');
      $fSumPriceAfterRabat = bcadd($fPriceAfterRabat, $fSumPriceAfterRabat);
      $aNewPrices[$iKey][0] = $fPrice;
      $aNewPrices[$iKey][1] = $fPriceAfterRabat;
    }
    var_dump($aNewPrices);
    
    return bcsub($fSumPriceAfterRabat, bcsub($fSumPrice, $fDiscountSum));
  }// end of getAllPriceList() method
  
  
  /**
   * Metoda przelicza rabat pojedyńczej książki
   * 
   * @param float $fPrice
   * @param float $fDiscountPercent
   * @return float
   */
  public function getOnePrice($fPrice, $fDiscountPercent) {
    return $fPrice = bcsub($fPrice, $fPrice * ($fDiscountPercent/100));
  }// end of getOnePrice() method
}

$oTEST = new phpTestRecount();

?>