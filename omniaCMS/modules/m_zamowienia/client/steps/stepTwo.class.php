<?php

use orders\Shipment;
use orders\ShipmentsCollection;
/**
 * Klasa do obsługi zamówień - krok 2
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class stepTwo {
  public $oOrders;
  public $sModule;
  
  function __construct($oOrders) {
    $this->oOrders = $oOrders;
    $this->sModule = $this->oOrders->sModule; // fix
  }

  
  /**
   * Metoda zapisuje wyniki z kroku 2
   * 
   * @return boolean
   * @throws Exception
   */
  public function saveStepTwo() {
    global $aConfig, $pDbMgr;
    
    // Walidator numerów telefonów komórkowych zgodny z UKE
    include_once($aConfig['common']['client_base_path'].'/omniaCMS/lib/Form/ValidateFields/ValidatePhone.class.php');
    $oValidatePhone = new ValidatePhone();
    
    if (!$this->oOrders->cartIsEmpty()){
      $_SESSION['wu_cart']['step_2'] = $this->oOrders->filterStepsData($_POST);
      include_once('Form/Validator.class.php');
      $oValidator = new Validator();

       // musi być wybrany sposób dostawy
      if (!isset($_POST['payment_type']) || intval($_POST['payment_type']) <= 0) {
        // brak sposobu dostawy
        $oValidator->sError .= '<li>'._('Wybierz sposób dostawy oraz metodę płatności').'</li>';
      }     

     // jeśli metoda platnosci paczkomaty
      $aPaymentData = $this->_getTransportSymbolByPaymentId($_POST['payment_type']);
      $sTransportSymbol = $aPaymentData['symbol'];
      
      switch ($sTransportSymbol) {
        case 'paczkomaty_24_7':

          if (!isset($_POST['point_of_receipt']) || $_POST['point_of_receipt'] == '') {
            // brak punktu odbioru
            $oValidator->sError .= '<li>'.$aConfig['lang']['mod_'.$this->sModule]['no_paczkomat_chosen'].'</li>';
          }
          if (!isset($_POST['phone_paczkomaty']) || $_POST['phone_paczkomaty'] == '' 
                  || !$oValidatePhone->validate($_POST['phone_paczkomaty'])) {
            // brak punktu odbioru
            $oValidator->sError .= '<li>'.$aConfig['lang']['mod_'.$this->sModule]['no_phone'].'</li>';
          }
          if (!isset($_POST['regulations_paczkomaty']) || $_POST['regulations_paczkomaty'] != '1') {
            // brak punktu odbioru
            $oValidator->sError .= '<li>'.$aConfig['lang']['mod_'.$this->sModule]['no_regulations_paczkomaty'].'</li>';
          }          
        break;
        

        case 'ruch':
            if (!isset($_POST['phone_'.$sTransportSymbol]) || $_POST['phone_'.$sTransportSymbol] == ''
                || !$oValidatePhone->validate($_POST['phone_'.$sTransportSymbol])) {
                $oValidator->sError .= '<li>'.$aConfig['lang']['mod_'.$this->sModule]['no_phone'].'</li>';
            }

            if ($_SESSION['wu_cart']['payment']['ptype'] == 'postal_fee') {
                include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
                $shipment = new Shipment($_SESSION['wu_cart']['payment']['transport_id'], $pDbMgr);
                $bValidatePostcode = $shipment->checkDestinationPointAvailablePayCashOnDelivery('PointsOfReceipt', intval($_POST['point_of_receipt_ruch']));
                if (false === $bValidatePostcode) {
                    $oValidator->sError .= '<li>' . _('Wybrany punkt odbioru nie obsługuje płatności przy odbiorze') . '</li>';
                }
            }
        break;

        case 'poczta_polska':
        case 'orlen':  
          if (!isset($_POST['phone_'.$sTransportSymbol]) || $_POST['phone_'.$sTransportSymbol] == '' 
                  || !$oValidatePhone->validate($_POST['phone_'.$sTransportSymbol])) {
            $oValidator->sError .= '<li>'.$aConfig['lang']['mod_'.$this->sModule]['no_phone'].'</li>';
          }         
        break;  
       
        case 'tba':
          include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
          $oTBAPointsOfReceipt = new Shipment($_SESSION['wu_cart']['payment']['transport_id'], $pDbMgr);
          $bValidatePostcode = $oTBAPointsOfReceipt->checkDestinationPointAvailable(intval($_POST['point_of_receipt_tba']));

          if (!isset($_POST['point_of_receipt_tba'])) {
            $oValidator->sError .= '<li>'.$aConfig['lang']['mod_'.$this->sModule]['bad_postcode'].'</li>';
          } else if (!$bValidatePostcode) {
            $oValidator->sError .= '<li>'.$aConfig['lang']['mod_'.$this->sModule]['no_postcode'].'</li>';
          }          
        break; 
      }  
      
      // jeśli PayU
      if ($aPaymentData['ptype'] === 'platnoscipl' && !isset($_POST['payment_bank'])) {
        // brak punktu odbioru
        $oValidator->sError .= '<li>'._('Wybierz swój bank, z którego chcesz wykonać przelew elektroniczny').'</li>';
      }

      if ($aPaymentData['ptype'] === 'payu_bank_transfer' && !isset($_POST['payment_bank'])) {
          // brak punktu odbioru
          $oValidator->sError .= '<li>'._('Wybierz przelew bankowy').'</li>';
      }

      if (! $oValidator->Validate($_POST) || $oValidator->sError != '') {
        throw new Exception($oValidator->GetErrorString(true));
        return false;
      } else {
        // tutaj jest juz oki
        return true;
      }
    } else {
      throw new Exception(_('Koszyk jest pusty'));
      return false;
    }
  }// end of saveStepTwo() method
  
  
  /**
   * Metoda sprawdza poprawność wprowadzonego kodu rabatowego i przelicza koszyk
   * 
   * @global array $aConfig
   * @return boolean
   */
  public function checkRecountDiscountCode() {
    global $aConfig;
    
    
    $this->oOrders->getUserData();
    $_POST['discount_code'] = trim($_POST['discount_code']);
    if(!empty($_POST['discount_code'])){
      if(preg_match('/^[\w]+$/',$_POST['discount_code'])){
        $aCode = $this->oOrders->checkDiscountCode($_POST['discount_code'], $_SESSION['wu_cart']['products']);
        if($aCode === false) {
          setMessage($aConfig['lang']['mod_'.$this->sModule]['discount_code_err'],true);
        } 
        elseif ($aCode == -2) {
          unset($_SESSION['wu_cart']['discount_code']);
          setMessage($aConfig['lang']['mod_'.$this->sModule]['discount_code_err_2'],true);
        }
        else {
          $_SESSION['wu_cart']['discount_code'] = $aCode;
          if ($_SESSION['wu_cart']['discount_code']['code_discount'] > 0.00 && $_SESSION['wu_cart']['discount_code']['off_cost_transport'] != '1') {
            // rabat
            setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['discount_code_ok_0'],$_SESSION['wu_cart']['discount_code']['code_discount']),false);
          } elseif ($_SESSION['wu_cart']['discount_code']['code_discount'] > 0.00 && $_SESSION['wu_cart']['discount_code']['off_cost_transport'] == '1') {
            // rabat + darmowy transport
            setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['discount_code_ok_1'],$_SESSION['wu_cart']['discount_code']['code_discount']),false);
          } elseif ($_SESSION['wu_cart']['discount_code']['off_cost_transport'] == '1') {
            // darmowy transport
            setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['discount_code_ok_2']),false);
          } else {
            // inny...
            setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['discount_code_ok_3'],$_SESSION['wu_cart']['discount_code']['code_discount']),false);
          }
          $this->oOrders->codeRecalculateProducts();
        }
      } else {
        setMessage($aConfig['lang']['mod_'.$this->sModule]['discount_code_err'],true);
      }
    } elseif ( !empty($_SESSION['wu_cart']['discount_code'])) { // && $_GET['login'] == 'true'
      $this->oOrders->codeRecalculateProducts();
    }
    return true;
  }// end of checkRecountDiscountCode() method

    private function getTotalBasketWeight($aModule)
    {
        $implodedProductIds = implode(',', array_keys($aModule['cart']['products']));

        $res = Common::GetAssoc("select id, weight from products where id in ($implodedProductIds)");

        $totalOrderWeight = 0;

        foreach($res as $productId => $weight) {

            if (0 == $weight || null == $weight) {
                $weight = 0.4;
            }

            $productQuantity = $aModule['cart']['products'][$productId]['quantity'];

            $productTotalWeight = $weight * $productQuantity;

            $totalOrderWeight += $productTotalWeight;
        }

        return $totalOrderWeight;
    }

	/**
   * Metoda zwraca HTML kroku 2 koszyka
   * 
   * @global array $aConfig
   * @global object $pSmarty
   * @return string
   */
	public function getStep2() {

		global $aConfig, $pSmarty;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		
		$this->oOrders->getUserData();
		
		if (!$this->oOrders->cartIsEmpty()) {

            if (!empty($_SESSION['wu_cart']['products'])) {

                $limiter = new \Basket\Limit\Limiter();
                $checkedData = $limiter->determineMaximumAmount($_SESSION['wu_cart']['products']);
                $limitResult = $limiter->checkLimits($checkedData);
                if (true !== $limitResult) {
                    setMessage($limitResult, false, true, 36000);
                    $this->oOrders->RecountAll(FALSE, FALSE);
                    doRedirectHttps('/koszyk');
                }
            }
      
      $aData = $_SESSION['wu_cart']['step_2'];
			$aModule['cart'] =& $_SESSION['wu_cart'];
			$aModule['shipment_info'] = $this->oOrders->getShipmentInfoCalendar();
			$aModule['kod_rabatowy_info'] = $this->oOrders->getDiscountInfo();
			if ($aConfig['common']['status'] !== 'development')
            {
			    $aModule['paczkomaty'] = $this->_getPaczkomatySelect($_SESSION['w_user']['id']);
            }
			
			// dolaczenie klasy FormTable
			include_once('Form/FormTable.class.php');
			$oForm = new FormTable('step2Form',
														'',
														array('action' => $this->oOrders->sPageLink.'/save_step2.html#cart'),
														array(),
														false);
      

			include_once ('Platnoscipl.class.php');
			$pPlatnosci = new Platnoscipl();
			$aModule['payment_bank'] = $pPlatnosci->getPaymentSelection($_SESSION['wu_cart']['total_cost'], false, $aConfig['common']['platosci_pl_show_test_mode'], $aData['payment_bank']);

      $aModule['invoice2']['items'] = array();
      foreach($aModule['cart']['products'] as $iKey => $aItem){
        $aModule['invoice2']['items'][$iKey] = $oForm->GetCheckBoxHTML('item_2nd_invoice['.$iKey.']', '', array(), '', isset($aData['item_2nd_invoice'][$iKey]), false);
      }

      foreach($aModule['payment_bank'] as $payment)
      {
          if ($payment['code'] == 'b')
          {
              $payment['input'] = mb_substr($payment['input'],0,-2).' checked '."/>";
              $aModule['payu_payment_bank'][] = $payment;

          }
      }

      $totalBasketWeight = $this->getTotalBasketWeight($aModule);

      $aModule['payment_form'] = $this->_getPaymentForm($totalBasketWeight);

      //remove row if all payment fields are disabled
      foreach($aModule['payment_form'] as $payment_name => $values)
      {
            $disabled = 0;
            foreach($values as $fields)
            {
                if ($fields['disabled'] == 'true')
                {
                    $disabled++;
                }
            }

            if (count($values) == $disabled)
            {
                unset($aModule['payment_form'][$payment_name]);
            }
      }


      $aForm['payment_platnosci'] = $oForm->GetHiddenHTML('payment_platnosci', ($_SESSION['wu_cart']['payment']['ptype']=='platnoscipl') , array('id'=>'__payment_platnosci'));
      $aModule['payment_bank_show'] = ($_SESSION['wu_cart']['payment']['ptype'] == 'platnoscipl'?'1':'0');
      $aModule['payment_onebank_show'] = ($_SESSION['wu_cart']['payment']['ptype'] == 'payu_bank_transfer'?'1':'0');
      $aForm['header'] = $oForm->GetFormHeader();
      $aForm['footer'] = $oForm->GetFormFooter();
      $aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
                                               $oForm->GetErrorPrefix(),
                                               array('id' => 'uaF__ErrPrefix'));
      $aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
                                               $oForm->GetErrorPostfix(),
                                               array('id' => 'uaF__ErrPostfix'));
      $aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
                                               $oForm->GetValidator(),
                                               array('id' => 'uaF__Validator'));

      foreach($aModule['cart']['products'] as $iKey => $aLitem){
        if($aLitem['shipment_date']!='') {
          $aModule['cart']['products'][$iKey]['shipment_date_f'] = FormatDate($aLitem['shipment_date']);
        }
      }
      $aModule['form'] =& $aForm;

      $pSmarty->assign_by_ref('aModule', $aModule);
      $sHtml = $this->_getJS().
               $pSmarty->fetch($this->oOrders->sTemplatesPath.'/koszyk_step_2.tpl');
      $pSmarty->clear_assign('aModule', $aModule);
		}
		else {
			setMessage($aModule['lang']['no_data']);
		}
		return $sHtml;
	} // end of showCart() method
  
  
  /**
   * Metoda pobiera formularz płatności
   * 
   * @global array $aConfig
   * @return type
   */
  private function _getPaymentForm($totalBasketWeight) {
  	global $aConfig;

		include_once('FreeTransportPromo.class.php');
		$oFreeTransportPromo = new Lib_FreeTransportPromo();
		// sprawdzmy czy istnieją w koszyku książki które posiadają darmową przesyłkę
		$aFreeTransport = array();
		foreach ($_SESSION['wu_cart']['products'] as $iKey => $aVal) {
			$aFreeTransportTMP = $oFreeTransportPromo->getBookFreeTransport($iKey, $aVal['total_price_brutto']);
			if (!empty($aFreeTransportTMP)) {
				$aFreeTransport = array_merge($aFreeTransport, $aFreeTransportTMP);
				$aFreeTransport = array_unique($aFreeTransport);
			}

			$cartProducts[] = $iKey;
		}
		
		if (!empty($_SESSION['wu_cart']) && $_SESSION['wu_cart']['total_price_brutto'] > 0.00) {
			$aFreeTransportTMP = $oFreeTransportPromo->getBookFreeTransport('-1', $_SESSION['wu_cart']['total_price_brutto']);
			if (!empty($aFreeTransportTMP)) {
				$aFreeTransport = array_merge($aFreeTransport, $aFreeTransportTMP);
				$aFreeTransport = array_unique($aFreeTransport);
			}
		}
//      AND (B.max_order_weight >= $totalBasketWeight || B.max_order_weight = 0)
  	$aUserPriv = $this->oOrders->getSpecialUserPrivileges();
  	$sSql = "SELECT B.symbol, A.id, A.ptype, A.pdata, A.description, A.cost, 
										A.no_costs_from, B.name AS transport_name, B.logo AS transport_logo, 
										B.personal_reception, B.id AS transport_id,
										A.max_order_value
  					 FROM ".$aConfig['tabls']['prefix']."orders_payment_types A
  					 JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
  					 ON A.transport_mean=B.id
  					 WHERE A.published='1'  					 ".
  					(!empty($aUserPriv)&&($aUserPriv['allow_14days'] == '1')?'':" AND A.ptype <> 'bank_14days'").
  					(!empty($aUserPriv)&&($aUserPriv['allow_personal_reception'] == '1')?'':" AND B.personal_reception = '0'").
  					" ORDER BY B.order_by, A.order_by";
  	$aData =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);

  	$sSql2 = "SELECT B.symbol,product_id FROM orders_transport_means_products A
              LEFT JOIN  ".$aConfig['tabls']['prefix']."orders_transport_means B
              ON B.id = A.orders_transport_means_id";

  	$aData2 = Common::GetAll($sSql2);
  	$excluded = array();

  	foreach($aData2 as $transportMean)
    {
        $excluded[$transportMean['symbol']][$transportMean['product_id']] = '';
    }

    foreach ($aData as $sTransport => $aPayments) {
      foreach ($aPayments as $iKey => $aValue) {

          // exclude transport if product is in transport excluded list
          if (false === $this->checkAvailablePaymentTransportForProducts($excluded[$sTransport],$cartProducts)){
              unset($aData[$sTransport][$iKey]);
          } else {

              if (
                  (
                      Common::formatPrice2($_SESSION['wu_cart']['total_price_brutto']) >= $aValue['max_order_value'] &&
                      $aValue['max_order_value'] > 0
                  )
                  ||
                  (
                      ($aValue['ptype'] == 'platnoscipl' || $aValue['ptype'] == 'payu_bank_transfer' || $aValue['ptype'] == 'card') &&
                      Common::formatPrice2($_SESSION['wu_cart']['total_price_brutto']) > 2999.99
                  )
              ) {
                  // jeśli payu i kk ukrywamy tę metodę płatności
                  if (isset($aData[$sTransport]) && isset($aData[$sTransport][$iKey])) {
                      unset($aData[$sTransport][$iKey]);
                  }
              } else {

                  $aData[$sTransport][$iKey]['name'] = $aConfig['lang']['mod_m_zamowienia']['payment_type'][$aValue['ptype']];
                  $aData[$sTransport][$iKey]['cost'] = Common::formatPrice2($aValue['cost']);
                  if (!empty($aValue['transport_logo'])) {
                      $aData[$sTransport][$iKey]['transport_logo'] = $aConfig['common']['transport_logos_dir'] . '/' . $aValue['transport_logo'];
                  }
                  $aData[$sTransport][$iKey]['no_costs_from'] = Common::formatPrice2($aValue['no_costs_from']);

                  $bDiscountFreeTransport = false;
                  if (isset($_SESSION['wu_cart']['discount_code']) && $_SESSION['wu_cart']['discount_code']['off_cost_transport'] == '1') {
                      if ($aData[$sTransport][$iKey]['transport_id'] > 0
                          && !empty($_SESSION['wu_cart']['discount_code']['off_cost_transport_methods'])
                          && is_array($_SESSION['wu_cart']['discount_code']['off_cost_transport_methods'])
                      ) {
                          foreach ($_SESSION['wu_cart']['discount_code']['off_cost_transport_methods'] as $aTransportMethod) {
                              if (intval($aTransportMethod['transport_id']) == intval($aData[$sTransport][$iKey]['transport_id'])
                                  && (Common::formatPrice2($aTransportMethod['min_value']) == 0.00 || Common::formatPrice2($_SESSION['wu_cart']['total_price_brutto']) > $aTransportMethod['min_value'])
                              ) {
                                  if ($aValue['ptype'] != 'postal_fee') {
                                      $bDiscountFreeTransport = true;
                                  }
                              }
                          }
                      }
                  }

                  if (($aValue['no_costs_from'] > 0 && Common::formatPrice2($_SESSION['wu_cart']['total_price_brutto']) > $aValue['no_costs_from']) ||
                      (!empty($aUserPriv) && ($aUserPriv['allow_free_transport'] == '1')) ||
                      $bDiscountFreeTransport == true ||
                      in_array($sTransport, $aFreeTransport) === true
                  ) {
                      $aData[$sTransport][$iKey]['cost'] = 0;
                  }
                  if ($aValue['ptype'] == 'dotpay') {
                      $aValue['pdata'] = unserialize(base64_decode($aValue['pdata']));
                  }
                  $aData[$sTransport][$iKey]['cost'] = Common::formatPrice($aData[$sTransport][$iKey]['cost']);
              }

          }
      }
    }
    
    $aTransformedData = $this->transformPaymentTransportArray($aUserPriv, $aData, $totalBasketWeight);
  	return $aTransformedData;
  } // end of getPaymentForm() method
  
  private function checkAvailablePaymentTransportForProducts($excluded_list,$cartProducts)
  {
      if($excluded_list != NULL)
      {
          foreach($cartProducts as $product)
          {
              if (isset($excluded_list[$product]))
              {
                  return false;
              }
          }
      }
      return true;
  }
  /**
   * 
   * @return boolean
   */
  private function transformPaymentTransportArray($aUserPriv, $aData, $totalBasketWeight) {
    global $aConfig;
    
    $aNewArray = array();
    $aPTypes = $this->_getAllPTypes($aUserPriv);
    $transportMaxWeights = Common::GetAssoc("select symbol, max_order_weight from orders_transport_means");

    foreach ($aData as $sTransport => $aPayments) {
      foreach ($aPTypes as $pType) {
        foreach ($aPayments as $iKey => $aValue) {
          if ($aValue['ptype'] == $pType) {
            $aNewArray[$pType][$sTransport] = $aValue;
          }
        }

        $maxTransportWeight = $transportMaxWeights[$sTransport];

        if (!isset($aNewArray[$pType][$sTransport]) || ($maxTransportWeight < $totalBasketWeight && $maxTransportWeight > 0) ) {
          $aNewArray[$pType][$sTransport]['disabled'] = true;
          $aNewArray[$pType][$sTransport]['name'] = $aData[$sTransport][$iKey]['name'] = $aConfig['lang']['mod_m_zamowienia']['payment_type'][$pType];
          $aNewArray[$pType][$sTransport]['transport_logo'] = $aData[$sTransport][$iKey]['transport_logo'];
          $aNewArray[$pType][$sTransport]['ptype'] = $pType;
        }
      }
    }
    return $aNewArray;
  }
  
  /**
   * 
   * @return array
   */
  private function _getAllPTypes($aUserPriv) {
    
    $sSql = 'SELECT ptype 
             FROM orders_payment_types AS A
             WHERE 
              published = "1"
              '.(!empty($aUserPriv)&&($aUserPriv['allow_14days'] == '1')?'':" AND A.ptype <> 'bank_14days'").'
             GROUP BY ptype
             ORDER BY A.order_by';
    return Common::GetCol($sSql);
  }
  
	
	/**
	 * Metoda pobiera domyślny adres
	 *
	 * @param integer $iUId 
	 * @return array 
	 */
	private function _getDefaultTransportAdress($iUId) {
		global $aConfig;
    
		$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."users_accounts_address
						 WHERE user_id=".$iUId." 
						 ORDER BY default_transport DESC";
		return Common::GetRow($sSql);
	} // end of getDefaultTransportAdress() method
  
  
	/**
	 * Metoda generuje select z paczkomatami
	 *
	 * @param integer $iUId id użytkownika
	 */
	private function _getPaczkomatySelect($iUId) {
		global $aConfig;
		include_once('paczkomaty/inpost.php');
		
		// pobranie domyślnych danych
		if ($iUId > 0){
			$aAdress = $this->_getDefaultTransportAdress($iUId);
		} else {
			$aAdress = '';// brak kodu pocztowego
		}
		// mail lokalnie ustawiamy na testowy !!!
		if ($aConfig['common']['status'] == 'development' && $aConfig['common']['paczkomaty']['email'] != '') {
			$sEmail = $aConfig['common']['paczkomaty']['email'];
		} else {
			$sEmail = $_SESSION['w_user']['email'];
		}
		$aParam = array('email' => $sEmail,
										'postcode' => (isset($aAdress['postal']) ? $aAdress['postal'] : null),
										'name' => 'point_of_receipt',
										'paymentavailable' => '0',
										//'selected' => '', // ?? sami mozemy zapisywać ostatnio wybraną opcję
										'class' => 'point_of_receipt',
		);
		
		return inpost_machines_dropdown($aParam);
	}// end of getPaczkomatySelect() method
  
  
  /**
   * Metoda pobirea transport na podstawie id płatności
   * 
   * @param int $iPaymentId
   * @return string
   */
  private function _getTransportSymbolByPaymentId($iPaymentId) {
    
    if ($iPaymentId > 0) {
      $sSql = "SELECT B.symbol, A.ptype
               FROM orders_payment_types AS A
               JOIN orders_transport_means AS B
                ON A.transport_mean = B.id
               WHERE A.id = ".$iPaymentId;
      return Common::GetRow($sSql);
    }
  }// end of getTransportSymbolByPaymentId() method
  
  
  /**
   * Metoda pobiera JS do formularza na 2 kroku
   * 
   * @global type $aConfig
   * @return string
   */
  private function _getJS() {
		global $aConfig;
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
$sJS = '
					<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
			$("#editAddressesButton").click(function(){
				$("#editAddressesForm").submit();
				return false;
			});
			$("#confirmDiscountCode").click(function(){
				$("#step2Form").attr("action","/koszyk/step2.html");
				$("#step2Form").submit();
			});
			$("#payment_type").change(function(){
				$.ajax({
				   type: "POST",
				   url: "/internals/calculateCart.php",
				   data: ({payment_type: $(this).val()}),
				   success: function(msg){
						var smsg = msg+"";
						if(smsg != ""){
							var aFields = smsg.split("###");
							$("#total_transport_cost").html(aFields[0]);
							$("#total_cost").html(aFields[1]);
							$("#total_cost2").html(aFields[1]);
							if (aFields[0] != "0,00") {
								$("#total_transport_cost").parent().parent().show();
							} else {
								$("#total_transport_cost").parent().parent().hide();
							}
							if(aFields[2] == "platnoscipl"){
								$("#paymentBankSection").show();
								$("#__payment_platnosci").val("1");
							} else {
								$("#paymentBankSection").hide();
								$("#__payment_platnosci").val("0");
							}
							
							if (aFields[2] == "payu_bank_transfer"){
							    $("#paymentOneBankSection").show();
								$("#__payment_platnosci").val("1");
							} 
							else {
								$("#paymentOneBankSection").hide();
								$("#__payment_platnosci").val("0");
							}
							
							if(aFields[3] != "" && aFields[3] != "0,00"){
								$(".koszykWysInfo").html(aFields[3]);
								$(".koszykWysInfo").show();
                $(".koszykWysCena").show();
							}							
						}
					 },
				   error: function(msg){
				    
				   }
				});
			});
			$("#step2Form").submit(function(){
				if($("#step2Form").attr("action") == "/koszyk/order.html"){
					if($("input[name=\'payment_type\']:checked").length == 0){
						alert("'.$aModule['lang']['no_payment_chosen'].'");
						return false;
					}
					if($("#invoice_address").val() == ""){
						alert("Wybierz adres dla faktury");
						return false;
					}
					if($("#transport_address").val() == ""){
						alert("Wybierz adres dla przesyłki");
						return false;
					}
					if($("#__second_invoice").val() == "1" && $("#invoice2_address").val() == ""){
						alert("Wybierz adres dla drugiej faktury");
						return false;
					}
					if($("#__second_invoice").val() == "1" && $("input[name^=\'item_2nd_invoice\']:checked").length == 0){
						alert("Wybierz ksiażki dla drugiej faktury");
						return false;
					}
					if($("#__second_invoice").val() == "1" && $("input[name^=\'item_2nd_invoice\']:checked").length == '.count($_SESSION['wu_cart']['products']).'){
						alert("Pierwsza faktura nie może pozostać pusta!");
						return false;
					}
					if($("#__payment_platnosci").val() == "1" && $("input[name=\'payment_bank\']:checked").length == 0){
						alert("'.$aModule['lang']['no_bank_chosen'].'");
						return false;
					}
					if($("input[name=\'payment_type\']:checked").parent().parent().parent().attr("id") == "paczkomaty_24_7" && $("#phone_paczkomaty").val() == "") {
						alert("Podaj numer telefonu");
						return false;
					}
					if($("input[name=\'payment_type\']:checked").parent().parent().parent().attr("id") == "paczkomaty_24_7" && $("#regulations_paczkomaty").attr("checked") == false) {
						alert("Zaakceptuj regulamin Paczkomaty 24/7");
						return false;
					}
					if($("input[name=\'payment_type\']:checked").parent().parent().parent().attr("id") == "paczkomaty_24_7" && ($(".point_of_receipt").val() == "" || $(".point_of_receipt").val() == null)) {
						alert("Wybierz paczkomat");
						return false;
					}
				}
				return true;
			});
		});
	// ]]>
</script>
				';
    return $sJS;
  }// end of _getJS() method
  
  public function getExcludedProducts($sTransportSymbol){
      $sSql = ' SELECT 
                otmp.product_id
                FROM orders_transport_means_products otmp
                JOIN orders_transport_means otm
                ON otm.id = otmp.orders_transport_means_id AND otm.symbol = \''.$sTransportSymbol.'\'';
      return Common::GetCol($sSql);
  }
  /**
   * Metoda validuje krok 2
   * 
   * @param array $aCard
   * @throws Exception
   */
  public function validateStepTwo(&$aCard, $sTransportSymbol) {
    global $pDbMgr;
    
    $aNowStep =& $aCard['step_2'];
    //sprawdz czy produkty nie z listy odrzuconych środka transportu
    $excluded_products = $this->getExcludedProducts($sTransportSymbol);//$sTransportSymbol

    if (count($excluded_products) > 0)
    {
      foreach($aCard['products'] as $product)
      {
          if (in_array($product['id'],$excluded_products))
          {
              throw new Exception('Wybrano niedozwolony środek transportu dla jednego z produktów');
          }
      }
    }
    
    // dwie faktury i jeden produkt
    if (count($aCard['products']) <= 1 && isset($aNowStep['item_2nd_invoice']) && !empty($aNowStep['item_2nd_invoice'])) {
      throw new Exception('Wybrano dwie faktury dla jednego produktu');
    }
    
    // czy podano formę płatności i transportu
    if (!empty($aCard['payment']) && $aCard['payment']['transport_id'] > 0) {
      // sprawdźmy typ metody płatności
      
      if ($sTransportSymbol === 'paczkomaty_24_7') {
        if (empty($aNowStep['point_of_receipt'])) {
          throw new Exception('Wybierz ponownie paczkomat');
        }
        if (empty($aNowStep['phone_paczkomaty'])) {
          throw new Exception('Podaj ponownie telefon dla paczkomaty');
        }
      } else {
        $oShipmentCollection = new ShipmentsCollection($pDbMgr);
        $aShipmentAtPoint = $oShipmentCollection->getShipmentReceptionAtPoint();
        if (in_array($aCard['payment']['transport_id'], $aShipmentAtPoint)) {
          $sDestinationPointValue = $aNowStep['point_of_receipt_'.$sTransportSymbol];
          $oShipment = new Shipment($aCard['payment']['transport_id'], $pDbMgr);
          if ($oShipment->checkDestinationPointAvailable($sDestinationPointValue) === false) {
            throw new Exception('Wybrano nieprawidłowy punkt odbioru.');
          }
        }
      }
    
      // czy wybrano bank dla elektronicznej metody płatności
      if ($aCard['payment']['ptype'] === 'platnoscipl' && $aNowStep['payment_bank'] == '') {
        throw new Exception('Wybierz ponownie swój bank, z którego chcesz wykonać przelew elektroniczny');
      }
    } else {
      throw new Exception('Wybierz ponownie metodę płatności');
    }
  }
// end of validateStepOne() method
}// end of class
?>