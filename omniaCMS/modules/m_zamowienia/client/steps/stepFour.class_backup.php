<?php
/**
 * Klasa do obsługi zamówień - krok 4
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-25 
 * @copyrights Marcin Chudy - Profit24.pl
 * @property Module oOrders klasa zamowien
 */
class stepFour {
  /**
   *
   * @var \Module
   */
  private $oOrders;
  private $pTransportType;

  private $bIsPaczkomaty = FALSE;
  private $bIsOdbiorWPunkcie = FALSE;
  private $bIsPaczkaWRuchu = FALSE;
  private $bIsOrlen = FALSE;
  private $bIsTBA = FALSE;
  
  /**
   * 
   * @param \Module $oOrders
   */
  function __construct($oOrders) {
    $this->oOrders = $oOrders;
    
    if ($_SESSION['wu_cart']['payment']['transport_id'] > 0) {
      $this->pTransportType = $this->oOrders->getTransportMeansSymbol($_SESSION['wu_cart']['payment']['transport_id']);
      if ($this->pTransportType == 'paczkomaty_24_7') {
        $this->bIsPaczkomaty = TRUE;
        if ($this->_filterStepDataPaczkomaty($_SESSION['wu_cart']) === FALSE) {
          setMessage(_('Uzupełnij ponownie dane zamówienia'), true, true);
          doRedirect($this->oOrders->sPageLink);
        }
      } else if ($this->pTransportType == 'poczta_polska') {
        $this->bIsOdbiorWPunkcie = TRUE;
        if ($this->_filterStepDataOdbiorWPunkcie($_SESSION['wu_cart']) === FALSE) {
          setMessage(_('Uzupełnij ponownie dane zamówienia'), true, true);
          doRedirect($this->oOrders->sPageLink);
        }      
      } else if ($this->pTransportType == 'ruch') {
        $this->bIsPaczkaWRuchu = TRUE;
        if ($this->_filterStepDataPaczkaWRuchu($_SESSION['wu_cart']) === FALSE) {
          setMessage(_('Uzupełnij ponownie dane zamówienia'), true, true);
          doRedirect($this->oOrders->sPageLink);
        }      
      }  else if ($this->pTransportType == 'orlen') {
        $this->bIsOrlen = TRUE;
        if ($this->_filterStepDataOrlen($_SESSION['wu_cart']) === FALSE) {
          setMessage(_('Uzupełnij ponownie dane zamówienia'), true, true);
          doRedirect($this->oOrders->sPageLink);
        }      
      } else if ($this->pTransportType == 'tba') {
        $this->bIsTBA = TRUE;
        if ($this->_filterStepDataTBA($_SESSION['wu_cart']) === FALSE) {
          setMessage(_('Uzupełnij ponownie dane zamówienia'), true, true);
          doRedirect($this->oOrders->sPageLink);
        }      
      } else {
        if ($this->_filterStepDataDef($_SESSION['wu_cart']) === FALSE) {
          setMessage(_('Uzupełnij ponownie dane zamówienia'), true, true);
          doRedirect($this->oOrders->sPageLink);
        }
      }
    } else {
      setMessage(_('Uzupełnij ponownie dane zamówienia'), true, true);
      doRedirect($this->oOrders->sPageLink);
    }
  }// end of __construct()
  
  private function _filterStepDataPaczkomaty($aData) {
    
    if ($aData['step_2']['point_of_receipt'] != '') {
      return true;
    }
  }

  private function _filterStepDataOdbiorWPunkcie($aData) {
    
    if ($aData['step_2']['point_of_receipt_poczta_polska'] != '') {
      return true;
    }
  }  

  private function _filterStepDataPaczkaWRuchu($aData) {
    
    if ($aData['step_2']['point_of_receipt_ruch'] != '') {
      return true;
    }
  }    

  private function _filterStepDataOrlen($aData) {
    
    if ($aData['step_2']['point_of_receipt_orlen'] != '') {
      return true;
    }
  }   

  private function _filterStepDataTBA($aData) {
    
    if ($aData['step_2']['point_of_receipt_tba'] != '') {
      return true;
    }
  }     
  
  /**
   * Metoda tworzy formularz kroku 4
   * 
   * @global array $aConfig
   * @global \Smarty $pSmarty
   * @global \DatabaseManager $pDbMgr 
   * @return string
   */
  public function getStep4(){
    global $aConfig, $pSmarty, $pDbMgr;
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->oOrders->sModule]['step_4'];
    $aForm = array();

    if (isLoggedIn()) {
      $aForm = $this->getFormArrayStep4LoggedIn();
      $aData =& $_SESSION['wu_cart']['step_3_logged_in'];
    } else {
      $aForm = $this->getFormArrayStep4LoggedOut();
      $aData =& $_SESSION['wu_cart']['step_3_logged_out'];
    }
    
    if ($this->bIsPaczkomaty === TRUE) {
      $aForm['paczkomaty']['point_of_receipt'] = $_SESSION['wu_cart']['step_2']['point_of_receipt'];
      $aForm['paczkomaty']['phone_paczkomaty'] = $_SESSION['wu_cart']['step_2']['phone_paczkomaty'];
      $aForm['paczkomaty']['point_of_receipt_sel'] = $_SESSION['wu_cart']['step_2']['point_of_receipt_sel'];
    }
    $aForm['is_paczkomaty'] = $this->bIsPaczkomaty;

    if ($this->bIsOdbiorWPunkcie === TRUE) {
      $aForm['poczta_polska']['point_of_receipt_poczta_polska'] = $_SESSION['wu_cart']['step_2']['point_of_receipt_poczta_polska'];
      $aForm['poczta_polska']['phone_poczta_polska'] = $_SESSION['wu_cart']['step_2']['phone_poczta_polska'];
      $aForm['poczta_polska']['point_of_receipt_sel_poczta_polska'] = $_SESSION['wu_cart']['step_2']['point_of_receipt_sel_poczta_polska'];
    }    
    $aForm['is_poczta_polska'] = $this->bIsOdbiorWPunkcie;
    
    if ($this->bIsPaczkaWRuchu === TRUE) {
      $aForm['ruch']['point_of_receipt_ruch'] = $_SESSION['wu_cart']['step_2']['point_of_receipt_ruch'];
      $aForm['ruch']['phone_ruch'] = $_SESSION['wu_cart']['step_2']['phone_ruch'];
      $aForm['ruch']['point_of_receipt_sel_ruch'] = $_SESSION['wu_cart']['step_2']['point_of_receipt_sel_ruch'];
    }    
    $aForm['is_ruch'] = $this->bIsPaczkaWRuchu;    

    if ($this->bIsOrlen === TRUE) {
      $aForm['orlen']['point_of_receipt_orlen'] = $_SESSION['wu_cart']['step_2']['point_of_receipt_orlen'];
      $aForm['orlen']['phone_orlen'] = $_SESSION['wu_cart']['step_2']['phone_orlen'];
      $aForm['orlen']['point_of_receipt_sel_orlen'] = $_SESSION['wu_cart']['step_2']['point_of_receipt_sel_orlen'];
    }    
    $aForm['is_orlen'] = $this->bIsOrlen;      

    if ($this->bIsTBA === TRUE) {
      $sTransportAddress = $this->oOrders->getAddress($aData['transport_address']);
      $sPostcode = $sTransportAddress['postal'];
      
      include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
      $oTBAPointsOfReceipt = new \orders\Shipment($_SESSION['wu_cart']['payment']['transport_id'], $pDbMgr);
      $bValidatePostcode = $oTBAPointsOfReceipt->checkDestinationPointAvailable($sPostcode);
      
      if (!$bValidatePostcode) {
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        $oValidator->sError .= '<li>'._('Wybierz swój bank, z którego chcesz wykonać przelew elektroniczny').'</li>';
      }
      
    }    
          
    
    // pola uwag do zamowienia i do transportu
    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('step4Form',
                          '',
                          array('action' => $this->oOrders->sPageLink.'/save_step4.html#cart'),
                          array(),
                          false);
    $aDataStep4 =& $_SESSION['wu_cart']['step_4'];
    $aForm['remarks'] = $this->_getRemarksForm($oForm, $aDataStep4);
    $aForm['validator'] = $this->_getValidation($oForm, 'step4_remarks');
    
    $aModule['cart'] = $_SESSION['wu_cart'];
    $aModule['form'] = $aForm;
    $sHtml = $this->_smartyParseForm($aModule, $pSmarty, 'koszyk_step_4.tpl');
    return $sHtml;
  }// end of getStep4() method
  
  
  /**
   * 
   * @global array $aConfig
   * @global \Smarty $pSmarty
   * @return array
   */
  public function getFormArrayStep4LoggedIn(){

    $aData =& $_SESSION['wu_cart']['step_3_logged_in'];
    // zalogowany
    if ($aData['invoice_address'] > 0) {
      $aForm['invoice'] = $this->oOrders->getAddress($aData['invoice_address']);
    }
    if ($aData['invoice2_address'] > 0) {
      $aForm['second_invoice'] = $this->oOrders->getAddress($aData['invoice2_address']);
    }
    if ($this->bIsPaczkomaty === FALSE && $aData['transport_address'] > 0) {
      $aForm['delivery'] = $this->oOrders->getAddress($aData['transport_address']);
    }
    if (!empty($aData['recipient'])) {
      $aForm['recipient'] = $aData['recipient'];
    }
    if (!empty($aData['recipient_phone'])) {
      $aForm['recipient_phone'] = $aData['recipient_phone'];
    }    
    
    return $aForm;
  }// end of getFormArrayStep4LoggedIn() method
  
  
  /**
   * 
   * @return array
   */
  public function getFormArrayStep4LoggedOut(){

    $aData =& $_SESSION['wu_cart']['step_3_logged_out'];
    $aForm = $aData;

    return $aForm;
  }// end of getFormArrayStep4LoggedOut() method
  
  
  /**
   * Metoda filtruje dane
   * 
   * @param type $aData
   * @return boolean
   */
  private function _filterStepDataDef(&$aData) {
    
    if (empty($aData['step_2'])) {
      return false;
    }
    
    
    if (isLoggedIn() && !empty($aData['step_3_logged_in'])) {
      unset($aData['step_3_logged_out']);
      return true;
    } elseif (!isLoggedIn() && !empty($aData['step_3_logged_out'])) {
      if ($aData['step_3_logged_out']['delivery']['delivery_data_from_invoice'] == '1') {
        // wywalamy wszystko
//        unset($aData['step_3_logged_out']['delivery']);
        // wywalamy ponownie ustawiamy na '1'
//        $aData['step_3_logged_out']['delivery']['delivery_data_from_invoice'] = '1';
      }
      /*
      // czy dane osoby odbierającej powinny pozostać
      if (!empty($aData['step_3_logged_out']['recipient'])) {
        // jeśli 
        if ($this->oOrders->checkRecipientRequired($aData['step_3_logged_out']) === FALSE) {
          // dane nie są potrzebne i wymagane
          unset($aData['step_3_logged_out']['recipient']);
        }
      }
      */
      
      unset($aData['step_3_logged_in']);
      return true;
    } else {
      return false;
    }
  }// end of _filterStepDataDef() method
  

  /**
   * Metoda generuje formularz komentarzy
   * 
   * @param \FormTable $oForm
   * @param array $aData
   * @return array
   */
  private function _getRemarksForm(&$oForm, $aData) {
    global $aConfig;
    $aModule['lang'] =& $aConfig['lang']['mod_'.$this->oOrders->sModule];
    
    $aForm = array();

    $aForm['remarks']['input'] = $oForm->GetTextAreaHTML('remarks', $aModule['lang']['remarks_plain'], 
                                                          isset($aData['remarks']) ? filterInput(br2nl($aData['remarks'])) : NULL, 
                                                          array('class' => '', 'id' => 'd_remarks'), '', $aModule['lang']['remarks_err'], '', false);
    $aForm['remarks']['label'] = $oForm->GetLabelHTML('remarks', $aModule['lang']['remarks_plain'], false);

    if ($this->bIsPaczkomaty === FALSE ) {
      $aForm['transport_remarks']['input'] = $oForm->GetTextHTML('transport_remarks', $aModule['lang']['transport_remarks'], 
                                                                  isset($aData['transport_remarks']) ? filterInput(br2nl($aData['transport_remarks'])) : NULL, 
                                                                  array('class' => 'step_2_textarea_', 'maxlength' => 150), 150, $aModule['lang']['remarks_err'], '', false);
      $aForm['transport_remarks']['label'] = $oForm->GetLabelHTML('transport_remarks', $aModule['lang']['transport_remarks'], false);
    }
    return $aForm;
  }// end of _getRemarksForm() method
  
  
  /**
   * Metoda zapisuje wyniki z kroku 4
   * 
   * @TODO można dodać validację pól osoby odbierającej przesyłkę - w przypadku kiedy z selecta wybrana jest jako dostawa do firmy
   * @return boolean
   * @throws Exception
   */
  public function saveStepFour() {
    
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
      
    if (!$this->oOrders->cartIsEmpty()){
      $_SESSION['wu_cart']['step_4'] = $this->oOrders->filterStepsData($_POST);

      // @todo TUTAJ
      if (! $oValidator->Validate($_POST)) {
        throw new Exception($oValidator->GetErrorString(true));
      } else {
        // tutaj jest juz oki
        return true;
      }
    } else {
      throw new Exception(_('Koszyk jest pusty'));
    }
  }// end of saveStepFour() method
  
  
  /**
   * Metoda parsuje zmienne przez szablon
   * 
   * @param array $aModule
   * @param object $pSmarty
   * @param string $sTemplate
   * @return string - HTML
   */
  private function _smartyParseForm(&$aModule, $pSmarty, $sTemplate) {
    
    $pSmarty->assign_by_ref('aModule', $aModule);
    $sHtml = $pSmarty->fetch($this->oOrders->sTemplatesPath.'/'.$sTemplate);
    $pSmarty->clear_assign('aModule', $aModule);
    return $sHtml;
  }// end of _smartyParseForm() method
  
  
  /**
   * Metoda generuja pola validacji
   * 
   * @param \FormTable $oForm
   * @param string $sAjaxValidationName
   * @return array
   */
  private function _getValidation(&$oForm, $sAjaxValidationName) {
    
    $aForm['header'] = $oForm->GetFormHeader();
    $aForm['footer'] = $oForm->GetFormFooter();
    
    $aForm['JS'] = $this->_getJSValidation($oForm, $sAjaxValidationName);
    
    $aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));
	  $aForm['ajaxval'] = $oForm->GetHiddenHTML('__ajaxValidator', $sAjaxValidationName,
																								 array('id' => 'uaF__ajaxValidator'));
    return $aForm;
  }// end of _getValidation() method
  
  
  /**
   * Metoda dodaje js do pól validacji
   * 
   * @param object $oForm
   * @return string
   */
  private function _getJSValidation(&$oForm, $sAjaxValidationName) {
    return $oForm->GetBlurFieldValidatorJS($sAjaxValidationName);
  }// end of _getJSValidation() method
}// end of class
