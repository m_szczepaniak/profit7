<?php
/**
 * Klasa do obsługi zamówień - krok 3
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-24 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class stepOne {
  public $oOrders;
  
  function __construct($oOrders) {
    $this->oOrders = $oOrders;
  }
  
  
	/**
	 * Metoda zwraca kod HTML z koszykiem
	 *
	 * @param	aray ref	$aModule	- dane modulu
	 * @return	array	- kod HTML koszyka
	 */
	public function showCart() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->oOrders->sModule];
		$aModule['logged_in'] =isLoggedIn();
		$bShowCart = true;

		if (!$this->oOrders->cartIsEmpty()) {	
			$aModule['cart'] =& $_SESSION['wu_cart'];
			//($aModule['cart']);
				
			//$aModule['payment_form'] = $this->oOrders->getPaymentForm();
		//	($_SESSION['wu_cart']);
			if ($this->oOrders->cartIsEmpty()) {
				$bShowCart = false;
			}
			if ($bShowCart) {

                if (!empty($_SESSION['wu_cart']['products'])) {

                    $limiter = new \Basket\Limit\Limiter();
                    $checkedData = $limiter->determineMaximumAmount($_SESSION['wu_cart']['products']);
                    $limitResult = $limiter->checkLimits($checkedData);
                    if (true !== $limitResult) {
                        setMessage($limitResult, false, true, 36000);
                        $this->oOrders->RecountAll(FALSE, FALSE);
                        doRedirectHttps('/koszyk');
                    }
                }

        $iLateShipmentDate = 0;
        $iEarliestShipmentDate = 99999999;
				foreach($aModule['cart']['products'] as $iKey =>$aLitem) {
					if($aLitem['shipment_date'] != '') {
						$aModule['cart']['products'][$iKey]['shipment_date_f'] = FormatDate($aLitem['shipment_date']);
            // najwcześniejsza data
						$iShipmentDate = intval(str_replace('-', '', $aLitem['shipment_date']));
            // XXX TODO dokonczyc
            if ($iShipmentDate > 0 && $iShipmentDate > $iLateShipmentDate) {
              $iLateShipmentDate = $iShipmentDate;
            } 
            if ($iShipmentDate > 0 && $iShipmentDate < $iEarliestShipmentDate) {
              $iEarliestShipmentDate = $iShipmentDate;
            }
          }
        }
        if ($iLateShipmentDate > 0) {
          $sDateNow = date('Y-m-d');
          $iDatePastThreeDays = date('Ymd', strtotime ( '-3 day' , strtotime ( $sDateNow )));
          if ($iEarliestShipmentDate <= $iDatePastThreeDays) {
            // przeterminowana o 3 dni
            $sLateShipmentDate = preg_replace('/^(\d{4})(\d{2})(\d{2})$/', '$3.$2.$1', $iLateShipmentDate);
            $sMessage = printWebsiteMessage(getWebsiteMessage('koszyk_zawiera_przeterminowana_z'), array('najpozniejsza_data_wydania'), array($sLateShipmentDate));
            $aModule['transport_preview_info'] = $sMessage;
          } else {
            // przeterminowana o 3 dni
            $sLateShipmentDate = preg_replace('/^(\d{4})(\d{2})(\d{2})$/', '$3.$2.$1', $iLateShipmentDate);
            $sMessage = printWebsiteMessage(getWebsiteMessage('koszyk_zawiera_zapowiedz'), array('najpozniejsza_data_wydania'), array($sLateShipmentDate));
            $aModule['transport_preview_info'] = $sMessage;
          }
        }

//				dump($_SESSION['wu_cart']);
                $this->getCriteoJS($_SESSION['wu_cart']['products']);
				$pSmarty->assign_by_ref('aModule', $aModule);
				$sHtml = $pSmarty->fetch($this->oOrders->sTemplatesPath.'/domyslny_koszyk.tpl');
				$pSmarty->clear_assign('aModule', $aModule);
			}
			
		}
		else {
			setMessage($aModule['lang']['no_data'], false, true);
      $aMatches = array();
      $sClientBaseURL = str_replace('http://', '', $aConfig['common']['client_base_url_http_no_slash']);
      if (preg_match('/^https?:\/\/'.$sClientBaseURL.'/', $_SERVER['HTTP_REFERER'], $aMatches)) {
        doRedirect($_SERVER['HTTP_REFERER']);
      } else {
        doRedirect('/');
      }
		}
		return $sHtml;
	} // end of showCart() method

    private function getCriteoJS($products) {
        global $aConfig;

        $ids = [];
        if ( empty($products) === false) {
            foreach ($products as $product) {
                $ids[] = json_encode([
                    "id" => $product['id'],
                    "price" => $product['promo_price'],
                    "quantity" => $product['quantity']
                ]);
            }
        }

        $aConfig['header_js'][]="</script>";
        $aConfig['header_js'][]="<script type=\"text/javascript\">";
        $aConfig['header_js'][]="window.criteo_q = window.criteo_q || [];";
        $aConfig['header_js'][]="var deviceType = /iPad/.test(navigator.userAgent) ? \"t\" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? \"m\" : \"d\";";
        $aConfig['header_js'][]="window.criteo_q.push(";
        $aConfig['header_js'][]="{ event: \"setAccount\", account: 57447},";
        $aConfig['header_js'][]="{ event: \"setEmail\", email: \"" .( isset($_SESSION['w_user']['email'])?$_SESSION['w_user']['email']:"" ). "\" },";
        $aConfig['header_js'][]="{ event: \"setSiteType\", type: deviceType},";
        $aConfig['header_js'][]="{ event: \"viewBasket\", item: [" . implode(',', $ids) . "]});";
        $aConfig['header_js'][]="</script>";
        $aConfig['header_js'][]="<script type=\"text/javascript\">";
    }
  
  
  /**
   * Metoda validuje krok 1
   * 
   * @param array $aCard
   * @throws Exception
   */
  public function validateStepOne($aCard) {
    
    if ($this->oOrders->cartIsEmpty()) {
      throw new Exception(_('Koszyk jest pusty'));
    }
  }// end of validateStepOne() method
}// end of class
?>