<?php
/**
 * Klasa do obsługi zamówień - krok 3
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-25 
 * @copyrights Marcin Chudy - Profit24.pl
 * @property Module oOrders klasa zamowien
 */
class stepThree {
  public $oOrders;

  private $bSelectedOdbiorWPunkcie;
  private $bSelectedPaczkaWRuchu;
  private $bSelectedOrlen;
  private $bSelectedTBA;
  private $bSelectedInpost;
  private $bIsReceptionAtPoint;
  
  
  function __construct($oOrders) {
    $this->oOrders = $oOrders;
    $sTransportSymbol = $this->_getTransportMeansSymbol($_SESSION['wu_cart']['payment']['transport_id']);

    $this->bSelectedOdbiorWPunkcie = $this->_checkSelectedOdbiorWPunkcie($sTransportSymbol);      
    $this->bSelectedPaczkaWRuchu = $this->_checkSelectedPaczkaWRuchu($sTransportSymbol);
    $this->bSelectedOrlen = $this->_checkSelectedOrlen($sTransportSymbol);
    $this->bSelectedTBA = $this->_checkSelectedTBA($sTransportSymbol);
    $this->bSelectedInpost = $this->_checkSelectedInpost($sTransportSymbol);
    $this->bIsReceptionAtPoint = $this->_checkIsReceptionAtPoint($sTransportSymbol);    
  }

  
  /**
   * Metoda tworzy formularz kroku 3
   * 
   * @global type $aConfig
   * @global type $pSmarty
   * @return string
   */
  public function getStep3(){
    global $aConfig, $pSmarty;
		$sHtml = '';
    
		if (!$this->oOrders->cartIsEmpty()) {
      
      if (!isLoggedIn()) {
        $sHtml = $this->getStep3LoggedOut();
      } else {
        $sHtml = $this->getStep3LoggedIn();
      }
    }
		else {
			setMessage($aModule['lang']['no_data']);
		}
    
    return $sHtml;
  }// end of getStep3() method  
  
    
   /**
   * Metoda tworzy formularz kroku 3 dla niezalogowanych
   * 
   * @global type $aConfig
   * @global type $pSmarty
   * @return string
   */
  public function getStep3LoggedOut(){
    global $aConfig, $pSmarty;
		$sHtml = '';
    
    $aModule['lang'] =& $aConfig['lang']['mod_'.$this->oOrders->sModule]['step_3'];
    // jeśli nie zalogowany
    if(isset($_SESSION['wu_cart']['step_3_logged_out']) && !empty($_SESSION['wu_cart']['step_3_logged_out'])){
      $aData =& $_SESSION['wu_cart']['step_3_logged_out'];
    }

    $aModule['login_box'] = $this->_getLoginForm();
    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('order_without_register',
                           '',
                           array('action' => createLink($this->oOrders->sPageLink, 'save_step3_log_out').'#cart'),
                           array(),
                           false);


    if ($this->bSelectedOdbiorWPunkcie === TRUE) {
      $aData['phone'] = $_SESSION['wu_cart']['step_2']['phone_poczta_polska'];
    }

    if ($this->bSelectedPaczkaWRuchu === TRUE) {
      $aData['phone'] = $_SESSION['wu_cart']['step_2']['phone_ruch'];
    }

    if ($this->bSelectedOrlen === TRUE) {
      $aData['phone'] = $_SESSION['wu_cart']['step_2']['phone_orlen'];
    }        

    $aModule['form']['main_data'] = $this->_getMainDataForm($oForm, $aData, $_SESSION['wu_cart']);
    $aModule['form']['invoice'] = $this->_getUserCommonDataForm($oForm, $aData['invoice'], 'invoice');
    $aModule['form']['recipient'] = $this->_getRecipientForm($oForm, $aData, true);


    if ($this->bIsReceptionAtPoint === FALSE) {
      $aModule['form']['delivery'] = $this->_getUserCommonDataForm($oForm, $aData['delivery'], 'delivery', 'delivery[delivery_data_from_invoice]', '2');
      $aModule['form']['delivery']['delivery_data_from_invoice'] = $this->_getDeliveryDataFromInviceRadio($oForm, $aData['delivery'], 'delivery');
      $aModule['form']['recipient'] = $this->_getRecipientForm($oForm, $aData['recipient'], false);
    }

    // jeśli poczta polska i użytkownik niezalogowany, generujemy panel: osoba odbierająca przesyłkę - i przepisujemy nr tel z kroku 2 do kroku 3
    if ($this->bSelectedOdbiorWPunkcie === TRUE) {
      if (empty($aData['phone'])) {
        $aData['phone'] = $_SESSION['wu_cart']['step_2']['phone_poczta_polska'];
      }
      $aModule['form']['is_poczta_polska'] = '1';
      $aModule['form']['is_poczta_polska_unregister'] = true;
      $aModule['form']['recipient'] = $this->_getRecipientForm($oForm, $aData, true);
    }

    // jeśli ruch - paczka w ruchu - i użytkownik niezalogowany, generujemy panel: osoba odbierająca przesyłkę - i przepisujemy nr tel z kroku 2 do kroku 3
    if ($this->bSelectedPaczkaWRuchu === TRUE) {
      if (empty($aData['phone'])) {
        $aData['phone'] = $_SESSION['wu_cart']['step_2']['phone_ruch'];
      }
      $aModule['form']['is_ruch'] = '1';
      $aModule['form']['is_ruch_unregister'] = true;
      $aModule['form']['recipient'] = $this->_getRecipientForm($oForm, $aData, true);
    }

    // jeśli orlen - i użytkownik niezalogowany, generujemy panel: osoba odbierająca przesyłkę - i przepisujemy nr tel z kroku 2 do kroku 3
    if ($this->bSelectedOrlen === TRUE) {
      if (empty($aData['phone'])) {
        $aData['phone'] = $_SESSION['wu_cart']['step_2']['phone_orlen'];
      }
      $aModule['form']['is_orlen'] = '1';
      $aModule['form']['is_orlen_unregister'] = true;
      $aModule['form']['recipient'] = $this->_getRecipientForm($oForm, $aData, true);
    }

    if ($this->_checkSecondInvoice($_SESSION['wu_cart']['step_2']) === TRUE) {
      $aModule['form']['second_invoice'] = $this->_getUserCommonDataForm($oForm, $aData['second_invoice'], 'second_invoice');
    }
    $aModule['form']['accept'] = $this->_getRegulationsAcceptForm($oForm, $aData);
    $aModule['form']['ceneo'] = $this->_getCeneoAcceptForm($oForm, $aData);
    $aModule['form']['validator'] = $this->_getValidation($oForm, "order_without_register");
    $sHtml = $this->_smartyParseForm($aModule, $pSmarty, 'koszyk_step_3_logged_out.tpl');   
  
    return $sHtml;
  }// end of getStep3LoggedOut() method

  
   /**
   * Metoda tworzy formularz kroku 3 dla zalogowanych
   * 
   * @global type $aConfig
   * @global type $pSmarty
   * @return string
   */
  public function getStep3LoggedIn(){
    global $aConfig, $pSmarty;
		$sHtml = '';
        
    $aModule['lang'] =& $aConfig['lang']['mod_'.$this->oOrders->sModule];
    // pola dla zalogowanego
    if(isset($_SESSION['wu_cart']['step_3_logged_in']) && !empty($_SESSION['wu_cart']['step_3_logged_in'] )){
      $aData =& $_SESSION['wu_cart']['step_3_logged_in'];
    }

    if ($this->bSelectedOdbiorWPunkcie && empty($aData['phone'])) {
      $aData['phone'] = $_SESSION['wu_cart']['step_2']['phone_poczta_polska'];
    }

    if ($this->bSelectedPaczkaWRuchu && empty($aData['phone'])) {
      $aData['phone'] = $_SESSION['wu_cart']['step_2']['phone_ruch'];
    }

    if ($this->bSelectedOrlen && empty($aData['phone'])) {
      $aData['phone'] = $_SESSION['wu_cart']['step_2']['phone_orlen'];
    }        

    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('order_logged_in',
                           '',
                           array('action' => createLink($this->oOrders->sPageLink, 'save_step3_logged_in')),
                           array(),
                           false);

    $aModule['form'] = $this->_getSelectAdressForm($oForm, $aData, $_SESSION['wu_cart']);
    if ($this->bSelectedOdbiorWPunkcie === TRUE) {
      $aModule['form']['is_poczta_polska'] = '1';
    }
    if ($this->bSelectedPaczkaWRuchu === TRUE) {
      $aModule['form']['is_ruch'] = '1';
    }
    if ($this->bSelectedOrlen === TRUE) {
      $aModule['form']['is_orlen'] = '1';
    }

    if ($this->bSelectedTBA === TRUE) {
      $addressesAvailableForTBA = count($this->getAddressesAvailableForTBA());
      if ($addressesAvailableForTBA < 2) {
        $aModule['form']['is_tba_warning'] = '1';
      }
    }

    $aModule['form']['recipient'] = $this->_getRecipientForm($oForm, $aData, true);
    $aModule['form']['recipient_phone'] = $this->_getRecipientPhoneForm($oForm, $aData);
    $aModule['links'] = $this->_getEditAdressLinks();
    $aModule['form']['ceneo'] = $this->_getCeneoAcceptForm($oForm, $aData);
    $aModule['form']['validator'] = $this->_getValidation($oForm, "order_logged_in");

    return $sHtml = $this->_smartyParseForm($aModule, $pSmarty, 'koszyk_step_3_logged_in.tpl');    
  }// end of getStep3LoggedIn() method  
  

   /**
   * 
   * @param int $sTransportSymbol
   * @return bool
   */
  private function _checkIsReceptionAtPoint($sTransportSymbol) {
    global $aConfig, $pDbMgr;
    
    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');  
    $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
    $oShipment = new \orders\Shipment($sTransportSymbol, $pDbMgr);  
               
    return $oShipment->isReceptionAtPoint();
  }// end of _checkIsReceptionAtPoint() method  
  
  
   /**
   * Metoda zwraca symbol transportu na podstawie Id transportu
   * 
   * @param int $iTransportId
   * @return string
   */
  private function _getTransportMeansSymbol($iTransportId) {

    $sSql = "SELECT symbol
             FROM orders_transport_means
             WHERE id = ".$iTransportId;

    return Common::GetOne($sSql);
  }// end of _getTransportMeansSymbol() method
  
  
  /**
   * Metoda sprawdza czy metoda transportu to inpost
   * 
   * @param int $iPaymentId
   * @return string
   */
  private function _checkSelectedInpost($sTransportSymbol) {
    if ($sTransportSymbol != 'paczkomaty_24_7') {
      return FALSE;
    }
    
    return TRUE;
  }// end of _checkSelectedInpost() method
  

  /**
   * Metoda sprawdza czy metoda transportu to Poczta Polska Odbiór w punkcie
   * 
   * @param int $iPaymentId
   * @return string
   */
  private function _checkSelectedOdbiorWPunkcie($sTransportSymbol) {
    if ($sTransportSymbol != 'poczta_polska') {
      return FALSE;
    }
    
    return TRUE;
  }// end of _checkSelectedOdbiorWPunkcie() method


  /**
   * Metoda sprawdza czy metoda transportu to Ruch - Paczka w Ruchu
   * 
   * @param int $iPaymentId
   * @return string
   */
  private function _checkSelectedPaczkaWRuchu($sTransportSymbol) {
    if ($sTransportSymbol != 'ruch') {
      return FALSE;
    }
    
    return TRUE;
  }// end of _checkSelectedPaczkaWRuchu() method

  
  /**
   * Metoda sprawdza czy metoda transportu to Orlen
   * 
   * @param int $sTransportSymbol
   * @return string
   */
  private function _checkSelectedOrlen($sTransportSymbol) {
    if ($sTransportSymbol != 'orlen') {
      return FALSE;
    }
    
    return TRUE;
  }// end of _checkSelectedOrlen() method
  

  /**
   * Metoda sprawdza czy metoda transportu to TBA
   * 
   * @param int $iPaymentId
   * @return string
   */
  private function _checkSelectedTBA($sTransportSymbol) {
    if ($sTransportSymbol != 'tba') {
      return FALSE;
    }
    
    return TRUE;
  }// end of _checkSelectedTBA() method
  
  
   /**
   * Metoda zwraca kod pocztowy na podstawie przekazanego id adresu
   * 
   * @param array $iId;
   * @return string
   */
  private function getAddressPostcode($iId) { 
 	
    $sSql = "SELECT postal 
					 FROM ".$aConfig['tabls']['prefix']."users_accounts_address 
					 WHERE id = ".$iId;
					 
    return (Common::GetOne($sSql));  
  }
  
  
  /**
   * Metoda zwraca przefiltrowane adresy znajdujące się w profilu użytkownika, które są w obszarze doręczeń TBA Express
   * 
   * @return array
   */
  private function getAddressesAvailableForTBA() {
    $aAddresses = $this->oOrders->getAddresses();
    

    // przy filtrowaniu adresów pomijamy pierwszy klucz w $aAddresses - to defaul value select'a  
    foreach ($aAddresses as $iKey => $aAddress) {
      if ($iKey != 0) {
         $sPostcode = $this->getAddressPostcode($aAddress['value']);
         $bValidatePostcode = $this->validateTBAPostcode($sPostcode);

         if (!$bValidatePostcode) {
           unset($aAddresses[$iKey]);
         }
      }
    }

    return $aAddresses;   
  }
  
  
  /**
   * Metoda wyświetla dane do faktury, faktury2 i wysyłki w selectach
   * 
   * @global array $aConfig
   * @param object $oForm
   * @param array $aData
   * @param array $aAllCardData
   * @return array
   */
  private function _getSelectAdressForm(&$oForm, $aData, &$aAllCardData) {
    global $aConfig;
    $aLang =& $aConfig['lang']['mod_'.$this->oOrders->sModule];
    
    $aAddresses = $this->oOrders->getAddresses();
    $aAddressesInvoice = $this->oOrders->getAddresses(true);
    
    // jeśli jest jakiś adres, poza domyślnym
    if ((!empty($aAddresses) && count(array_keys($aAddresses))) > 1
        || (!empty($aAddressesInvoice) && count(array_keys($aAddresses)) > 1)) {
      
      $iDefaultInvoiceAddressId = $this->oOrders->getDefaultAddressId(true,false);
      $iDefaultTransportAddressId = $this->oOrders->getDefaultAddressId(false,true);

      
      if ($_GET['adress_added'] == '1' && 
          $iDefaultInvoiceAddressId <= 0 && 
          $iDefaultTransportAddressId <= 0) {
        // jeśli domyślne adresy puste i ostatnio dodany został adres to wrzucamy 
        // pola do selecta jako domyślne
        $iDefaultInvoiceAddressId = $this->oOrders->getDefaultAddressId(false,false);
        $iDefaultTransportAddressId = $this->oOrders->getDefaultAddressId(false,false);
      }

      
      if (!isset($aData['invoice_address'])) {
        $aData['invoice_address'] = $iDefaultInvoiceAddressId;
      }

      if (!isset($aData['transport_address'])) {
        $aData['transport_address'] = $iDefaultTransportAddressId;
      }
      
      $aForm = array();
      $aForm['invoice_address']['label'] = $oForm->GetLabelHTML('invoice_address', $aLang['invoice_address'], true);
      $aForm['invoice_address']['input'] = $oForm->GetSelectHTML('invoice_address', $aLang['invoice_address'], array(), $aAddressesInvoice, $aData['invoice_address']);

      
      if ($this->bIsReceptionAtPoint === FALSE &&
          $this->bSelectedTBA === FALSE) {
          $aForm['transport_address']['label'] = $oForm->GetLabelHTML('transport_address', $aLang['transport_address'], true);
          $aForm['transport_address']['input'] = $oForm->GetSelectHTML('transport_address',$aLang['transport_address'], array(), $aAddresses, $aData['transport_address']);
      }
      
      
      if ($this->bSelectedTBA === TRUE) {
        // jeśli TBA, przed wygenerowaniem listy wyboru adresów transportu, pobieramy przefiltrowane adresy znajdujące się w obszarem doręczeń TBA
        $aAddresses = $this->getAddressesAvailableForTBA();
        
        // jeśli tablica adresów $aAddresses po walidacji kodów pocztowych zawiera adresy, prezentujemy listę wyboru adresów
        if (count($aAddresses) > 1) {
          $aForm['transport_address']['label'] = $oForm->GetLabelHTML('transport_address', $aLang['transport_address'], true);
          $aForm['transport_address']['input'] = $oForm->GetSelectHTML('transport_address',$aLang['transport_address'], array(), $aAddresses, $aData['transport_address']);
        } 
      }     
      

      if ($this->_checkSecondInvoice($aAllCardData['step_2']) === TRUE) {
        $aForm['invoice2_address']['label'] = $oForm->GetLabelHTML('invoice2_address', $aLang['invoice2_address'], false);
        $aForm['invoice2_address']['input'] = $oForm->GetSelectHTML('invoice2_address', $aLang['invoice2_address'], array(), $aAddressesInvoice, $aData['invoice2_address']);
      }
    } else {
      $aForm = array();
    }
    return $aForm;
  }// end of _getSelectAdressForm() method
  
  
  /**
   * Metoda generuje linki dla formularza zmiany danych adresowych
   * Pawel Bromka
   * @return array
   */
  private function _getEditAdressLinks() {
    
    $sMyAccountPageLink = getModulePageLink('m_konta');
          
    $aForm = array();
    $aForm['add_addresses'] = createLink($sMyAccountPageLink, 'add_address');
    $aForm['edit_addresses'] = createLink($sMyAccountPageLink, 'edit_addresses');
		$aForm['reflink'] = base64_encode($this->oOrders->sPageLink.'/step3.html#cart');
    return $aForm;
  }// end of _getEditAdressLinks() method
  

  /**
   * Metoda parsuje zmienne przez szablon
   * 
   * @param array $aModule
   * @param object $pSmarty
   * @param string $sTemplate
   * @return string - HTML
   */
  private function _smartyParseForm(&$aModule, $pSmarty, $sTemplate) {
    
    $pSmarty->assign_by_ref('aModule', $aModule);
    $sHtml = $pSmarty->fetch($this->oOrders->sTemplatesPath.'/'.$sTemplate);
    $pSmarty->clear_assign('aModule', $aModule);
    return $sHtml;
  }// end of _smartyParseForm() method
  
  
  /**
   * Metoda zapisuje wyniki z kroku 3
   * 
   * @return boolean
   * @throws Exception
   */
  public function saveStepThreeLogOut() {
    global $aConfig;


    if (!$this->oOrders->cartIsEmpty()){
        if (isset($_POST['accept_reg']) && $_POST['accept_reg'] == "1") {
            $_POST['accept_priv'] = "1";
        }
      $_SESSION['wu_cart']['step_3_logged_out'] = $this->oOrders->filterStepsData($_POST);
      
      include_once('Form/Validator.class.php');
      $oValidator = new Validator();
      
      // Walidator numerów telefonów komórkowych zgodny z UKE
      include_once($aConfig['common']['client_base_path'].'/omniaCMS/lib/Form/ValidateFields/ValidatePhone.class.php');
      $oValidatePhone = new ValidatePhone(); 
      
      if ($_POST['email'] == 'tanialis@o2.pl') {
        $oValidator->sError = '<li>Twój adres email został zablokowany</li>';
      }
      
      // walidacja kodu pocztowego dla TBA Express - użytkownik niezarejestrowany
      if ($this->bSelectedTBA === true) {
        
        if ($_SESSION['wu_cart']['step_3_logged_out']['delivery']['delivery_data_from_invoice'] == '1') {
          $sPostcode = $_SESSION['wu_cart']['step_3_logged_out']['invoice']['postal'];
        } else if ($_SESSION['wu_cart']['step_3_logged_out']['delivery']['delivery_data_from_invoice'] == '2') {
          $sPostcode = $_SESSION['wu_cart']['step_3_logged_out']['delivery']['postal']; 
        }
        
        if (!$this->validateTBAPostcode($sPostcode)) {
          $oValidator->sError = '<li>Wprowadzony kod pocztowy znajduje się poza obszarem doręczeń firmy TBA Express</li>';
        }
      }
      
      // walidacja telefonu komórkowego dla punktu odbioru: Poczta, Orlen, Ruch, podawanego na 3 kroku
      if ($this->bIsReceptionAtPoint === TRUE && 
              $this->bSelectedInpost === FALSE) {
        if (!empty($_POST['recipient']['phone'])) {
          
          if (!$oValidatePhone->validate($_POST['recipient']['phone'])) {
            $oValidator->sError .= '<li>'._('Telefon osoby odbierającej przesyłkę').'</li>';
          }
        } 
      }
      
      if (! $oValidator->Validate($_POST) || $oValidator->sError != '') {
        throw new Exception($oValidator->GetErrorString(true));
      } else {
        // tutaj jest juz oki
        return true;
      }
    } else {
      throw new Exception(_('Koszyk jest pusty'));
    }
  }// end of saveStepThreeLogOut() method
  
  
  /**
   * Metoda zapisuje wyniki z kroku 3
   * 
   * @return boolean
   * @throws Exception
   */
  public function saveStepThreeLogIn() {
    global $aConfig;
    
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    
    // Walidator numerów telefonów komórkowych zgodny z UKE
    include_once($aConfig['common']['client_base_path'].'/omniaCMS/lib/Form/ValidateFields/ValidatePhone.class.php');
    $oValidatePhone = new ValidatePhone();        

    if (!$this->oOrders->cartIsEmpty()){
      if (isset($_POST['invoice_address'])) {
        $_POST['invoice_address'] = intval($_POST['invoice_address']);
      }
      if (isset($_POST['transport_address'])) {
        $_POST['transport_address'] = intval($_POST['transport_address']);
      }
      if ($_POST['invoice2_address']) {
        $_POST['invoice2_address'] = intval($_POST['invoice2_address']);
      }
      $_SESSION['wu_cart']['step_3_logged_in'] = $this->oOrders->filterStepsData($_POST);
      

      if ($this->oOrders->getAddress($_POST['transport_address']) > 0) {
        $aTransportAdress = $this->oOrders->getAddress($_POST['transport_address']);
      }
      //if ($aTransportAdress['is_company'] == '1') {
        if ($_POST['recipient']['name'] == '') {
          $oValidator->sError .= '<li>'._('Imię osoby odbierającej przesyłkę').'</li>';
        }
        if ($_POST['recipient']['surname'] == '') {
          $oValidator->sError .= '<li>'._('Nazwisko osoby odbierającej przesyłkę').'</li>';
        }
        $aMatches = array();
        if (!preg_match($aConfig['class']['form']['phone']['pcre'], $_POST['recipient']['phone'], $aMatches)) {
          $oValidator->sError .= '<li>'._('Telefon osoby odbierającej przesyłkę').'</li>';
        }
        
        if ($this->bIsReceptionAtPoint === TRUE && 
            $this->bSelectedInpost === FALSE) {// paczkomaty mają inne pole odbioru
          
          if (!$oValidatePhone->validate($_POST['recipient']['phone'])) {
            $oValidator->sError .= '<li>'._('Telefon osoby odbierającej przesyłkę').'</li>';
          }
        }
      //}
      
      // walidacja telefonu komórkowego dla punktu odbioru: Poczta, Orlen, Ruch, podawanego na 3 kroku
      if ($this->bIsReceptionAtPoint === TRUE && 
          $this->bSelectedInpost === FALSE) {
        
        if (!empty($_POST['recipient']['phone'])) {      
          if (!$oValidatePhone->validate($_POST['recipient']['phone'])) {
            $oValidator->sError .= '<li>'._('Telefon osoby odbierającej przesyłkę').'</li>';
          }
        } 
      }  
     
      if (empty($_SESSION['wu_cart']['user_data']['phone']) && 
          empty($_SESSION['wu_cart']['step_2']['phone_paczkomaty']) && 
          empty($_POST['recipient']['phone'])) {

        $aMatches = array();

        if ($this->bIsReceptionAtPoint === TRUE && 
            $this->bSelectedInpost === FALSE) {
          if (!$oValidatePhone->validate($_POST['recipient']['phone'])) {
            $oValidator->sError .= '<li>'._('Telefon osoby odbierającej przesyłkę').'</li>';
          }
        } else {
          if (!preg_match($aConfig['class']['form']['phone']['pcre'], $_POST['recipient_phone']['phone'], $aMatches)) {
            $oValidator->sError .= '<li>'._('Telefon osoby odbierającej przesyłkę').'</li>';
          } else {
            $_POST['recipient']['phone'] = $_POST['recipient_phone']['phone'];
            $_SESSION['wu_cart']['user_data']['phone'] = $_POST['recipient_phone']['phone'];
          }
        }
      }

      if (! $oValidator->Validate($_POST)) {
        throw new Exception($oValidator->GetErrorString(true));
      } else {
        return true;
      }
    } else {
      throw new Exception(_('Koszyk jest pusty'));
    }
  }// end of saveStepThree() method
  
  
  /**
   * Metoda generuje pole do czy dane do dostawy mają być takie jak do faktury
   * 
   * @global array $aConfig
   * @param object $oForm
   * @param array $aData
   * @param string $sNameGroup
   * @return array
   */
  private function _getDeliveryDataFromInviceRadio(&$oForm, &$aData, $sNameGroup) {
    
    $aType = array(
			array('value' => '1', 'label' => _('Taki jak na fakturze'), 
          'id' => $sNameGroup.'[delivery_data_from_invoice][1]'),
			array('value' => '2', 'label' => _('Inny adres dostawy'), 
          'id' => $sNameGroup.'[delivery_data_from_invoice][2]')
		);
    
    $aForm = array();
		$aForm['label'] = $oForm->GetLabelHTML($sNameGroup.'[delivery_data_from_invoice]', _('Adres dostawy:'), false);
		$aForm['input'] = $oForm->GetRadioSetHTML($sNameGroup.'[delivery_data_from_invoice]', _('Adres dostawy'), $aType, $aData['delivery_data_from_invoice'], '', true, false);// , $sDependsOn, $sDependsVal
    return $aForm;
  }// end of getDeliveryDataFromInviceRadio() method
  
  
  /**
   * Metoda sprawdza czy dane do drugiej faktury mają zostać wyświetlone
   * 
   * @param array $aData
   * @return boolean
   */
  private function _checkSecondInvoice($aData) {
    
    if (!empty($aData['item_2nd_invoice'])) {
      return true;
    }
    return false;
  }// end of _checkSecondInvoice() method
  
  
  /**
   * Metoda generuja pola validacji
   * 
   * @param type $oForm
   * @param type $aData
   */
  private function _getValidation(&$oForm, $sAjaxValidationName) {
    
    $aForm['header'] = $oForm->GetFormHeader();
    $aForm['footer'] = $oForm->GetFormFooter();
    
    $aForm['JS'] = $this->_getJSValidation($oForm, $sAjaxValidationName);
    
    $aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));
	  $aForm['ajaxval'] = $oForm->GetHiddenHTML('__ajaxValidator', $sAjaxValidationName,
																								 array('id' => 'uaF__ajaxValidator'));
    return $aForm;
  }// end of _getValidation() method
  
  
  /**
   * Metoda dodaje js do pól validacji
   * 
   * @param object $oForm
   * @return string
   */
  private function _getJSValidation(&$oForm, $sAjaxValidationName) {
    return $oForm->GetBlurFieldValidatorJS($sAjaxValidationName);
  }// end of _getJSValidation() method
  
  
	/**
	 * Metoda pobiera formularz akceptacji regulaminów
	 *
	 * @global array $aConfig
	 * @param object ref $oForm
	 * @param array $aData
	 * @return array 
	 */
	private function _getCeneoAcceptForm(&$oForm, &$aData) {
	 	global $aConfig;
	 	$aLang =& $aConfig['lang']['mod_'.$this->oOrders->sModule]['step_3'];

    $aForm = array();
		$aAttr = array();
		
		$aForm['ceneo_agreement']['label'] = $oForm->GetLabelHTML('ceneo_agreement', $aLang['ceneo_agreement'], false);
		$aForm['ceneo_agreement']['input'] = $oForm->GetCheckBoxHTML('ceneo_agreement', $aLang['ceneo_agreement_err'], $aAttr, '', (isset($aData['ceneo_agreement']))?true:false, FALSE);
		return $aForm;
	}// end of _getCeneoAcceptForm() method
  
  
	/**
	 * Metoda pobiera formularz akceptacji regulaminów
	 *
	 * @global array $aConfig
	 * @param object ref $oForm
	 * @param array $aData
	 * @return array 
	 */
	private function _getRegulationsAcceptForm(&$oForm, &$aData) {
	 	global $aConfig;
	 	$aLang =& $aConfig['lang']['mod_'.$this->oOrders->sModule]['step_3'];

    $aForm = array();
		$aAttr = array();
		$bDefRequired = true;
		
		$aForm['reg']['label'] = $oForm->GetLabelHTML('accept_reg', $aLang['accept_reg'], true);
		$aForm['reg']['input'] = $oForm->GetCheckBoxHTML('accept_reg', $aLang['accept_reg_err'], $aAttr, '', (isset($aData['accept_reg'])/* || !isset($aData['sentForm'])*/)?true:false, $bDefRequired);

 		$aForm['promotions_agreement']['label'] = $oForm->GetLabelHTML('promotions_agreement', $aLang['promotions_agreement_text'], false);
		$aForm['promotions_agreement']['input'] = $oForm->GetCheckBoxHTML('promotions_agreement', $aLang['promotions_agreement'], $aAttr, '', (isset($aData['promotions_agreement']) ? $aData['promotions_agreement'] : false) , false);
    
//		$aForm['priv']['label'] = $oForm->GetLabelHTML('accept_priv', $aLang['accept_priv'], true);
//		$aForm['priv']['input'] = $oForm->GetCheckBoxHTML('accept_priv', $aLang['accept_priv_err'], $aAttr, '', (isset($aData['accept_priv']))?true:false, $bDefRequired); //  || !isset($aData['sentForm'])
    
		return $aForm;
	}// end of _getRegulationsAcceptForm() method
  

	/**
	 * Metoda zwraca od HTML formularza logowania
	 *
	 * @return	string	- formularz logowania
	 */
	private function _getLoginForm() {
		global $aConfig, $pSmarty;

    // dołączenie klasy integracji z facebook
    include_once($aConfig['common']['base_path'].'LIB/fb_connector/fbConnector.class.php');
    $oFB = fbConnector::getInstance();
    $aModule['links']['fb_login'] = $oFB->getLoginURL();
    
		// dolaczenie klasy UserLogin
		include_once('modules/m_konta/client/UserLogin.class.php');
		$sMyAccountPageLink = getModulePageLink('m_konta');
		//$aModule['shipment_info'] = $this->getShipmentInfo();
		$aModule['links']['register'] = createLink($sMyAccountPageLink, 'register');
		$aModule['links']['reflink'] = base64_encode($this->oOrders->sPageLink.'/step3.html#cart');
		$aModule['links']['change_passwd'] = createLink($sMyAccountPageLink, 'remind-passwd');
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->oOrders->sModule];
		$oLogin = new UserLogin($sMyAccountPageLink, $this->oOrders->sPageLink.'/step3.html?login=true#cart');

		$aModule['form'] =& $oLogin->getLoginForm(true);

		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch('modules/m_konta/zamowienia_loginForm2.tpl');
		$pSmarty->clear_assign('aModule');

		return $sHtml;
	} // end of getLoginForm() method
  
  
  /**
   * Metoda generuje główne pola do zamówienia bez rejestracjis
   * 
   * @global array $aConfig
   * @param object $oForm
   * @return array
   */
  private function _getMainDataForm(&$oForm, &$aData, &$aAllCardData) {
    global $aConfig;
    $aLang =& $aConfig['lang']['mod_'.$this->oOrders->sModule]['step_3'];
		$aTextAttribs = array(
			'class' => 'text'
		);

    if ($this->bIsReceptionAtPoint === TRUE && 
            $this->bSelectedInpost === FALSE) {
      // Walidator numerów telefonów komórkowych zgodny z UKE
      include_once($aConfig['common']['client_base_path'].'/omniaCMS/lib/Form/ValidateFields/ValidatePhone.class.php');
      $oValidatePhone = new ValidatePhone();
      $sPhonePCRE = $oValidatePhone->getPrefixRegExp();
      $aRange = array();
    } else {
      $sPhonePCRE = $aConfig['class']['form']['phone']['pcre'];
      $aRange = array(9,9);
    }
    
    $aForm = array();
    
		$aForm['registration']['input'] = $oForm->GetHiddenHTML('registration', $aData['registration']);
		
		$aForm['phone']['label'] = $oForm->GetLabelHTML('phone', $aLang['phone'], true);
		$aForm['phone']['input'] = $oForm->GetTextHTML('phone', $aLang['phone'], (!empty($aData['phone'])?$aData['phone']:$aAllCardData['step_2']['phone_paczkomaty']), array_merge($aTextAttribs, array('maxlength' => 16)), $aLang['phone_err'], 'text', true, $sPhonePCRE, $aRange);
		
    // adres email
		$aForm['email']['label'] = $oForm->GetLabelHTML('email', $aLang['email'], true);
		$aForm['email']['input'] = $oForm->GetTextHTML('email', $aLang['email'], $aData['email'], array_merge($aTextAttribs, array('maxlength' => 64)), $aLang['email_err'], 'email', true);
    
    // potwierdz adres email
    $aForm['confirm_email']['label'] = $oForm->GetLabelHTML('confirm_email', $aLang['confirm_email'], true);
    $aForm['confirm_email']['input'] = $oForm->GetTextHTML('confirm_email', $aLang['confirm_email'], $aData['confirm_email'], array_merge($aTextAttribs, array('maxlength' => 64)), '', 'email');

    // password
    $aForm['passwd']['label'] = $oForm->GetLabelHTML('passwd', $aLang['passwd'], true);
    $aForm['passwd']['input'] = $oForm->GetPasswordHTML('passwd', $aLang['passwd'], '', array_merge($aTextAttribs, array('maxlength' => 16)), $aLang['passwd_err'], true, '', serialize(array('registration' => '1')));

    $aForm['passwd2']['label'] = $oForm->GetLabelHTML('passwd2', $aLang['passwd2'], true);
    $aForm['passwd2']['input'] = $oForm->GetConfirmPasswordHTML('passwd2', $aLang['passwd2'], '', 'passwd', array_merge($aTextAttribs, array()), $aLang['confirm_passwd_err'], serialize(array('registration' => '1')));

		return $aForm;
  }// end of _getMainDataForm() method
  
  
  /**
   * Metoda tworzy uniwersalny formularz danych uzytkownika dostawy i faktury
   * 
   * @global type $aConfig
   * @param type $oForm
   * @param type $aData
   * @param type $sNameGroup
   * @param type $sDependsOn
   * @param type $sDependsVal
   * @return type
   */
  private function _getUserCommonDataForm(&$oForm, $aData, $sNameGroup, $sDependsOn = '', $sDependsVal = '') {
    global $aConfig;
    $aLang =& $aConfig['lang']['mod_'.$this->oOrders->sModule]['step_3'];
		$aMainAttribs = array(
			'class' => 'text'
		);
		$aPrivateAttribs = array(
			'class' => 'text priv' //addr_'.$iNr
		);
    $aDependsPoolsCompany = array(
        $sNameGroup.'[is_company]' => '1'
    );
    $aDependsPoolsPrivate = array(
        $sNameGroup.'[is_company]' => '0'
    );
    if ($sDependsOn != '') {
      $aDependsPoolsPrivate[$sDependsOn] = $sDependsVal;
      $aDependsPoolsCompany[$sDependsOn] = $sDependsVal;
    }

    $aType = array(
			array('value' => '0', 'label' => $aLang['is_company_0'], 
          'id' => 'addr_'.$sNameGroup.'_is_company_0'), // , 'onclick' => 'toggleAddresType('.$sNameGroup.',0)'
			array('value' => '1', 'label' => $aLang['is_company_1'], 
          'id' => 'addr_'.$sNameGroup.'_is_company_1') // , 'onclick' => 'toggleAddresType('.$sNameGroup.',1)'
		);
    
    $aForm = array();
		$aForm['is_company']['label'] = $oForm->GetLabelHTML($sNameGroup.'[is_company]', $aLang['is_company'], true);
		$aForm['is_company']['input'] = $oForm->GetRadioSetHTML($sNameGroup.'[is_company]', $aLang['is_company'], 
            $aType, $aData['is_company'], '', true, false, $sDependsOn, $sDependsVal);
    
		$aForm['company']['label'] = $oForm->GetLabelHTML($sNameGroup.'[company]', $aLang['company'], true);
		$aForm['company']['input'] = $oForm->GetTextHTML($sNameGroup.'[company]', $aLang['company'], $aData['company'], 
            array_merge($aMainAttribs, array('maxlength' => 255)), '', 'text', true, '', array(), serialize($aDependsPoolsCompany));

		$aForm['name']['label'] = $oForm->GetLabelHTML($sNameGroup.'[name]', $aLang['name'], true);
		$aForm['name']['input'] = $oForm->GetTextHTML($sNameGroup.'[name]', $aLang['name'], $aData['name'], 
            array_merge($aMainAttribs, array('maxlength' => 45)), $aLang['name_err'], 'text', true, '', array(), serialize($aDependsPoolsPrivate)); //, $sDependsOn,$sDependsVal
		
    $aForm['surname']['label'] = $oForm->GetLabelHTML($sNameGroup.'[surname]', $aLang['surname'], true);
		$aForm['surname']['input'] = $oForm->GetTextHTML($sNameGroup.'[surname]', $aLang['surname'], $aData['surname'], 
            array_merge($aPrivateAttribs, array('maxlength' => 45)), $aLang['surname_err'], 'text', true,'',array(), serialize($aDependsPoolsPrivate)); // ,'is_company_'.$iNr,'0'

    if ($sNameGroup == 'invoice' || $sNameGroup == 'second_invoice' ) {
      $aForm['nip']['label'] = $oForm->GetLabelHTML($sNameGroup.'[nip]', $aLang['nip'], true);
      $aForm['nip']['input'] = $oForm->GetTextHTML($sNameGroup.'[nip]', $aLang['nip'], $aData['nip'], 
              array_merge($aPrivateAttribs, array('maxlength' => 13)), $aLang['nip_err'], 'nip', true, '', array(), serialize($aDependsPoolsCompany));
    }
		
 		$aForm['street']['label'] = $oForm->GetLabelHTML($sNameGroup.'[street]', $aLang['street'], true);
		$aForm['street']['input'] = $oForm->GetTextHTML($sNameGroup.'[street]', $aLang['street'], $aData['street'], 
            array_merge($aPrivateAttribs, array('maxlength' => 150)), $aLang['street_err'], 'text', true,'',array(), $sDependsOn, $sDependsVal); // ,'is_company_'.$iNr,'0'

 		$aForm['number']['label'] = $oForm->GetLabelHTML($sNameGroup.'[number]', $aLang['number'], true);
		$aForm['number']['input'] = $oForm->GetTextHTML($sNameGroup.'[number]', $aLang['number'], $aData['number'], 
            array_merge($aPrivateAttribs, array('maxlength' => 8)), $aLang['number_err'], 'text', true,'',array(), $sDependsOn, $sDependsVal); // ,'is_company_'.$iNr,'0'
 
 		$aForm['number2']['label'] = $oForm->GetLabelHTML($sNameGroup.'[number2]', $aLang['number2'], false);
		$aForm['number2']['input'] = $oForm->GetTextHTML($sNameGroup.'[number2]', $aLang['number2'], $aData['number2'], 
            array_merge($aPrivateAttribs, array('maxlength' => 8)), $aLang['number2_err'], 'text', false);
     
 	  $aForm['postal']['label'] = $oForm->GetLabelHTML($sNameGroup.'[postal]', $aLang['postal'], true);
		$aForm['postal']['input'] = $oForm->GetTextHTML($sNameGroup.'[postal]', $aLang['postal'], $aData['postal'], 
            array_merge($aPrivateAttribs, array('maxlength' => 6)), $aLang['postal_err'], 'postal', true,'',array(), $sDependsOn, $sDependsVal); // ,'is_company_'.$iNr,'0'
            
 		$aForm['city']['label'] = $oForm->GetLabelHTML($sNameGroup.'[city]', $aLang['city'], true);
		$aForm['city']['input'] = $oForm->GetTextHTML($sNameGroup.'[city]', $aLang['city'], $aData['city'], 
            array_merge($aPrivateAttribs, array('maxlength' => 32)), $aLang['city_err'], 'text', true,'',array(), $sDependsOn, $sDependsVal); // ,'is_company_'.$iNr,'0'

    return $aForm;
  }// end of _getUserCommonDataForm() method
  
  
  /**
   * Metoda zwraca pola do wprowadzenia danych osoby odbierającej przesyłkę
   * 
   * @global array $aConfig
   * @param object $oForm
   * @param array $aData
   * @param bool $bLogin
   * @return array
   */
  private function _getRecipientForm($oForm, $aData, $bLogin) {
    global $aConfig;
    
    if ($this->bIsReceptionAtPoint === TRUE && 
            $this->bSelectedInpost === FALSE) {
      
      // Walidator numerów telefonów komórkowych zgodny z UKE
      include_once($aConfig['common']['client_base_path'].'/omniaCMS/lib/Form/ValidateFields/ValidatePhone.class.php');
      $oValidatePhone = new ValidatePhone();
      $sPhonePCRE = $oValidatePhone->getPrefixRegExp();
      $aRange = array(9,9);

    } else {
      $sPhonePCRE = $aConfig['class']['form']['phone']['pcre'];
      $aRange = array(9,9);
    }
    $aLang =& $aConfig['lang']['mod_'.$this->oOrders->sModule]['step_3'];
    $sNameGroup = 'recipient';
    
    if ($bLogin === FALSE) {
      $aDependsOn = array(
          // faktura dla firmy i dane do dostawy takie jak dla faktury
          array('invoice[is_company]' => '1', 'delivery[delivery_data_from_invoice]' => '1'),
          // dane do dostawy inne niż do faktury i dane do dostawy dla firmy
          array('delivery[delivery_data_from_invoice]' => '2', 'delivery[is_company]' => '1'),
          // poczta polska
          array('is_poczta_polska' => '1'),
          // ruch
          array('is_ruch' => '1'),
          // orlen
          array('is_orlen' => '1')

      );
    } else {
      $aDependsOn = array(
          array('is_delivery_invoice' => '1'),
          array('is_poczta_polska' => '1'),
          array('is_ruch' => '1'),
          array('is_orlen' => '1'),
          );
    }
    
		$aMainAttribs = array(
			'class' => 'text'
		);
    
    $aForm = array();
		$aForm['name']['label'] = $oForm->GetLabelHTML($sNameGroup.'[name]', $aLang['name'], true);
		$aForm['name']['input'] = $oForm->GetTextHTML($sNameGroup.'[name]', $aLang['name'], $aData['name'], 
            array_merge($aMainAttribs, array('maxlength' => 45)), $aLang['name_err'], 'text', true, '', array()); //, $sDependsOn,$sDependsVal //, serialize($aDependsOn)
		
    $aForm['surname']['label'] = $oForm->GetLabelHTML($sNameGroup.'[surname]', $aLang['surname'], true);
		$aForm['surname']['input'] = $oForm->GetTextHTML($sNameGroup.'[surname]', $aLang['surname'], $aData['surname'], 
            array_merge($aMainAttribs, array('maxlength' => 45)), $aLang['surname_err'], 'text', true, '', array()); // ,'is_company_'.$iNr,'0' , serialize($aDependsOn)

		$aForm['phone']['label'] = $oForm->GetLabelHTML($sNameGroup.'[phone]', $aLang['phone'], true);
		$aForm['phone']['input'] = $oForm->GetTextHTML($sNameGroup.'[phone]', $aLang['phone'], $aData['phone'], 
            array_merge($aMainAttribs, array('maxlength' => 16)), $aLang['phone_err'], 'text', true, $sPhonePCRE, $aRange); //, serialize($aDependsOn)
    return $aForm;
  }// end of _getRecipientForm() method
  
  
  /**
   * Metoda zwraca pola do wprowadzenia danych osoby odbierającej przesyłkę
   * 
   * @global array $aConfig
   * @param object $oForm
   * @param array $aData
   * @return array
   */
  private function _getRecipientPhoneForm($oForm, $aData) {
    global $aConfig;
    
    $aLang =& $aConfig['lang']['mod_'.$this->oOrders->sModule]['step_3'];
    $sNameGroup = 'recipient_phone';
    
    $aDependsOn = array('is_delivery_invoice' => '0', 'recipient_email_hidden' => '');
    
		$aMainAttribs = array(
			'class' => 'text'
		);
    
    $aForm = array();

		$aForm['phone']['label'] = $oForm->GetLabelHTML($sNameGroup.'[phone]', $aLang['phone'], true);
		$aForm['phone']['input'] = $oForm->GetTextHTML($sNameGroup.'[phone]', $aLang['phone'], $aData['phone'], 
            array_merge($aMainAttribs, array('maxlength' => 16)), $aLang['phone_err'], 'phone', true, '', array(), serialize($aDependsOn));
		
    return $aForm;
  }// end of _getRecipientPhoneForm() method

  
  /**
   * Metoda validuje krok koszyka
   * 
   * @param array $aCard - tablica danych koszyka
   * @throws Exception
   */
  public function validateStepThree(&$aCard) {
    
    if (!empty($aCard['step_2']['item_2nd_invoice'])) {
      $bIsSecondInvoice = true;
    } else {
      $bIsSecondInvoice = false;
    }
      
    if (isLoggedIn()) {
      $this->validateStepThreeLoggedIn($aCard, $bIsSecondInvoice);
    } else {
      $this->validateStepThreeLoggedOut($aCard, $bIsSecondInvoice);
    }
  }// end of validateStepThree() method

  
  /**
   * Metoda validuje krok koszyka dla zalogowanych
   * 
   * @param array $aCard - tablica danych koszyka
   * @param bool $bIsSecondInvoice
   * @throws Exception
   */
  public function validateStepThreeLoggedIn(&$aCard, $bIsSecondInvoice) {
    
    // zalogowany
    $aNowStep =& $aCard['step_3_logged_in'];
    if ($aNowStep['invoice_address'] <= 0) {
      throw new Exception('Wybierz ponownie dane do faktury');
    }
    if ($bIsSecondInvoice === true && $aNowStep['invoice2_address'] <= 0) {
      throw new Exception('Wybierz ponownie dane do drugiej faktury');
    }

    $aAddressData = $this->oOrders->getAddress($aNowStep['invoice_address']);
    if ($aAddressData['is_company'] == '1' && empty($aAddressData['nip'])) {
      throw new Exception('Uzupełnij NIP w danych do faktury');
    }

    if ($aNowStep['invoice2_address'] > 0 && $bIsSecondInvoice === true) {
      $aInvoiceAddressData = $this->oOrders->getAddress($aNowStep['invoice2_address']);
      if ($aInvoiceAddressData['is_company'] == '1') {
        if (empty($aInvoiceAddressData['nip'])) {
          throw new Exception('Uzupełnij NIP w danych do drugiej faktury');
        }
      }
    }

    if ($this->bIsReceptionAtPoint === FALSE) {
      // dane adresowe nalezy podać

      if ($aNowStep['transport_address'] > 0) {
        // jeśli adres dostawy to firma, to należy podać imię, nazwisko oraz telefon
        if ($this->oOrders->getAddress($aNowStep['transport_address']) > 0) {
          $aTransportAdress = $this->oOrders->getAddress($aNowStep['transport_address']);
        }

        if (/*$aTransportAdress['is_company'] == '1'
             && */
            (empty($aNowStep['recipient']) 
            || empty($aNowStep['recipient']['name'])
            || empty($aNowStep['recipient']['surname'])
            || empty($aNowStep['recipient']['phone']))) 
        {
          // dane osoby odbierającej są wymagane
          throw new Exception('Podaj ponownie dane osoby odbierającej przesyłkę');
        }
      } else {
        throw new Exception('Wybierz ponownie adres dostawy');
      }
      if (empty($aCard['user_data']['phone']) && empty($aNowStep['recipient']['phone'])) {
        throw new Exception('Uzupełnij numer telefonu osoby odbierającej przesyłkę');
      } elseif(empty($aCard['user_data']['phone']) && !empty($aNowStep['recipient']['phone'])) {
        $aCard['user_data']['phone'] = $aNowStep['recipient']['phone']; 
      }
    }

    /*if ($this->bIsReceptionAtPoint === TRUE &&
            $this->bSelectedInpost === FALSE) {*/

      try {
        $this->validateRecipient($aNowStep);
      } catch (Exception $ex) {
        throw new Exception($ex->getMessage());
      }
    //}
  }// end of validateStepThree() method
  
  
  /**
   * Metoda validuje krok koszyka dla niezalogowanych
   * 
   * @param array $aCard - tablica danych koszyka
   * @param bool $bIsSecondInvoice
   * @throws Exception
   */
  public function validateStepThreeLoggedOut(&$aCard, $bIsSecondInvoice) {

    $aNowStep =& $aCard['step_3_logged_out'];

    if ($aNowStep['email'] != $aNowStep['confirm_email']) {
      throw new Exception('Wprowadzono różne adresy e-mail');
    }

    if ($aNowStep['registration'] == '1' && empty($aNowStep['passwd'])) {
      throw new Exception('Podaj ponownie hasło do konta');
    }

    if (empty($aNowStep['invoice']['street'])) {
      throw new Exception('Podaj ponownie dane do faktury');
    }

    if ($bIsSecondInvoice === true && empty($aNowStep['second_invoice']['street'])) {
      throw new Exception('Podaj ponownie dane do drugiej faktury');
    }

    //if ($this->bIsReceptionAtPoint === FALSE) {
      // różne od paczkomaty
      if ($aNowStep['delivery']['delivery_data_from_invoice'] != '1' && empty($aNowStep['delivery']['street']) && $this->bIsReceptionAtPoint === FALSE) {
        throw new Exception('Podaj ponownie dane do dostawy');
      }

      // osoba odbierająca czy dane wymagane
      if (/*$this->oOrders->checkRecipientRequired($aNowStep) === true
           && */
          (empty($aNowStep['recipient']) 
          || empty($aNowStep['recipient']['name'])
          || empty($aNowStep['recipient']['surname'])
          || empty($aNowStep['recipient']['phone'])
          )) 
      {
        throw new Exception('Podaj ponownie dane osoby odbierającej przesyłkę');
      }
    //}

    /*if ($this->bIsReceptionAtPoint === TRUE &&
            $this->bSelectedInpost === FALSE) {*/

      try {
        $this->validateRecipient($aNowStep);
      } catch (Exception $ex) {
        throw new Exception($ex->getMessage());
      }
    /*}*/
    
  }// end of validateStepThreeLoggedOut() method
  
  
  /**
   * 
   * @param array $aCard
   * @param array $aNowStep
   * @throws Exception
   */
  private function validateRecipient($aNowStep) {
    
    if (  empty($aNowStep['recipient']) 
          || empty($aNowStep['recipient']['name'])
          || empty($aNowStep['recipient']['surname'])
          || empty($aNowStep['recipient']['phone']))
    {
      // dane osoby odbierającej są wymagane
      throw new Exception('Podaj ponownie dane osoby odbierającej przesyłkę');
    }
    
    
    if (empty($aNowStep['recipient']['phone'])) {
      throw new Exception('Uzupełnij numer telefonu osoby odbierającej przesyłkę');
    } 
    elseif (!empty($aNowStep['recipient']['phone'])) {
      
      include_once('lib/Form/ValidateFields/ValidatePhone.class.php');
      $oValidatePhone = new ValidatePhone();
      
      if ($oValidatePhone->validate($aNowStep['recipient']['phone']) === FALSE) {
        throw new Exception('Telefon osoby odbierającej przesyłkę nie jest poprawny');
      }  
    }
  }
  
  
  private function validateTBAPostcode($sPostcode) {
    global $aConfig, $pDbMgr;
    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
    $oTBAPointsOfReceipt = new \orders\Shipment($_SESSION['wu_cart']['payment']['transport_id'], $pDbMgr);
    $bValidatePostcode = $oTBAPointsOfReceipt->checkDestinationPointAvailable($sPostcode);
    
    return $bValidatePostcode;
  }
  
}// end of class
?>
