<?php
/**
 * Klasa Module do obslugi modulu 'zamowienia - do Przelewy Pobrania
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;
		$iCId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
		if (isset($_GET['cid'])) {
			$iCId = $_GET['cid'];
		}
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
		
		if (!empty($_SESSION['_tmp']['message'])) {
			$this->sMsg = GetMessage($_SESSION['_tmp']['message']['content'], $_SESSION['_tmp']['is_error']);
			unset($_SESSION['_tmp']['message']);
		}

		switch ($sDo) {
			case 'save': $this->saveOpek($pSmarty); break;
			case 'details': $this->Details($pSmarty, $iId); break;
			case 'remove': $this->removeOpek($pSmarty, $iId); break;
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method

	function Details(&$pSmarty, $iId) {
		global $aConfig;
		
		$bDisabled = false;
		// sprawdzamy czy przelew tylko do podglądu
		if ($this->checkCanMatchOrder($iId) === false) {
			$bDisabled = true;
		}

		// dolaczenie klasy View
		include_once('View/EditableView.class.php');
		include_once('Form/Form.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> false,
			'search'	=> false,
			'editable' => (( $bDisabled == false )? true :false),
			'send_button' => (( $bDisabled == false )?'save':false),
			'checkboxes'=>false,
			'per_page' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'title',
				//'content'	=> $aConfig['lang'][$this->sModule]['list_title'],
				'sortable'	=> false,
			  'width'=>'100'
			),
			array(
				'db_field'	=> 'realization_date',
				//'content'	=> '',//$aConfig['lang'][$this->sModule]['list_matched_order'],
				'sortable'	=> false,
				'width'	=> '102'
			),
			array(
				'db_field'	=> 'matched_order',
				//'content'	=> $aConfig['lang'][$this->sModule]['list_matched_order'],
				'sortable'	=> false,
				'width'	=> '220'
			),
			array(
				'db_field'	=> 'topay',
				'content'	=> $aConfig['lang'][$this->sModule]['list_paid_amount'],
				'sortable'	=> false,
				'width'	=> '60'
			),
			array(
				'db_field'	=> 'paidamount',
				//'content'	=> $aConfig['lang'][$this->sModule]['list_created'],
				'sortable'	=> false,
				'width'	=> '60'
			),
			array(
				'db_field'	=> 'end',
				//'content'	=> $aConfig['lang'][$this->sModule]['list_created'],
				'sortable'	=> false
			)
		);
		$sJS3='';
		$sJS4='';
		$sJS5='';
		
		$pView = new EditableView('orders_bank_statements', $aHeader, $aAttribs, $iRowCount);
		$pView->AddRecordsHeader($aRecordsHeader);

		{
			
			// pobranie wszystkich rekordow
			$sSql = "SELECT DISTINCT A.id, A.title, A.order_nr, A.matched_order, A.paid_amount paidamount, A.created
						 	 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements AS A
						 	 WHERE A.is_Opek='1' AND A.id=".$iId." AND A.finished='0' AND `deleted`='0' ";
			$aRecords =& Common::GetAll($sSql);
			$iCnt=0;
			$aRecords2=array();
			$sElements='';
			foreach($aRecords as $iKey=>$aItem){
				$sJS4.='recount('.$aItem['id'].'); ';
				$aRecords2[$iCnt]['title'] = $aConfig['lang'][$this->sModule]['payment_from_day'];
				$aRecords2[$iCnt]['realization_date'] = date('d.m.Y H:i', strtotime($aItem['created']));
				$aRecords2[$iCnt]['matched_order']=$aConfig['lang'][$this->sModule]['for_ammount'].Common::formatPrice($aItem['paidamount']);
				$aRecords2[$iCnt]['paidammount']='';
				$aRecords2[$iCnt]['topay']='';
								
				if ($bDisabled == true) {
					$aRecords2[$iCnt]['end']='';
				} else {
					$aRecords2[$iCnt]['end']=Form::GetInputButton('send', $aConfig['lang'][$this->sModule]['delete_from_opek'],array('onclick'=>'window.location.href=\''.phpSelf(array('do'=>'remove', 'id'=>$aItem['id'])).'\'; return false;'));
				}
				
				
				$sJS3.='paymentValues['.$aItem['id'].']='.$aItem['paidamount'].';';
			
				//zwiększenie licznika			
				++$iCnt;
				
				$aRecords2[$iCnt]['title']='<span style="margin-left:20px;">'.'Nr listu'.'</span>';	
				$aRecords2[$iCnt]['realization_date'] = 'Data realizacji';
				$aRecords2[$iCnt]['matched_order']='Zamówienie';
				$aRecords2[$iCnt]['topay']='Wartość';
				$aRecords2[$iCnt]['paidamount']='Opłacono';
				$aRecords2[$iCnt]['end']='<input type="hidden" name="przelew['.($iQ++).']" value="'.$aItem['id'].'" /><input type="hidden" name="send_order_status_'.$aItem['id'].'" value="1" />';	
				++$iCnt;	
				
				
				// część własciwa, wyświetlenie zamówień
				// pobranie numerów transportu które należą do przelewu
				$aTransports = $this->getLinkingTransportNumbers($iId);
				$fPayedSuma=$fToPaySuma=0.0;	
				
				// utworzenie pól do formularza matchowania
				
				foreach ($aTransports as $i => $aTransport){ 
					if ($aTransport['order_id'] <= 0) {
						$aTransport['order_id'] = $this->getOrderByTransportNum($aTransport['transport_number']);
					}

					if ($aTransport['order_id']>0) {
						$sOrd = Common::GetOne('SELECT status_4_update FROM '.$aConfig['tabls']['prefix'].'orders where id='.$aTransport['order_id']);
					}
					
					$ftopay = $this->getOrderByTransportToPay($aTransport['transport_number'], $aTransport['order_id']);
					$fpayed = $this->getOrderByTransportPayed($aTransport['transport_number'], $aTransport['order_id']);

					if($fpayed <= 0.00 && $aTransport['order_id']>0 && !$bNextisNUll) {
						$fpayed=$ftopay;
						if($fPayedSuma + $fpayed > $aItem['paidamount']) {
							$fpayed=$aItem['paidamount']-$fPayedSuma;
							$bNextisNUll=true;
						}
					}
					
					$sItem = $aTransport['id'];
					$sElements.=' document.getElementById(\'topay'.$sItem.'\'),';
					$aRecords2[$iCnt]['title']='<span style="margin-left:20px;">'.$aTransport['transport_number'].'</span>';
					$aRecords2[$iCnt]['realization_date'] = $sOrd != ''? date('d.m.Y H:i', strtotime($sOrd)) : '';
					if ($bDisabled == false) {
						$aRecords2[$iCnt]['matched_order']=Form::GetSelect('order['.$sItem.']', array('style'=>'width:240px;', 'onchange'=>'setPay('.$sItem.', this.value, \''.$aItem['id'].'\', \''.$aItem['id'].'\');'), $this->findOrders(), $aTransport['order_id']);
					} else {
						$aRecords2[$iCnt]['matched_order'] = $aTransport['order_id'] > 0 ? $this->getOrderNumber($aTransport['order_id']) : $aConfig['lang'][$this->sModule]['no_linked'];
					}
					$aRecords2[$iCnt]['topay']='<span name="topay'.$aItem['id'].'" id="topay'.$sItem.'">'.Common::FormatPrice($ftopay).'</span><input type="hidden" name="topayF['.$sItem.']" id="topayF['.$sItem.']" value="'.$ftopay.'" />';
					if ($bDisabled == false) {
						$aRecords2[$iCnt]['paidamount']='<input onblur="recount('.$aItem['id'].');" style="width:50" name="payed'.$aItem['id'].'['.($i+0).']" id="payed'.$aItem['id'].'['.($i+0).']" value="'.Common::FormatPrice($fpayed).'" />';
					} else {
						$aRecords2[$iCnt]['paidamount']='<span>'.Common::FormatPrice($fpayed).' </span><input type="hidden" name="payed'.$aItem['id'].'['.($i+0).']" id="payed'.$aItem['id'].'['.($i+0).']" value="'.Common::FormatPrice($fpayed).'" />';
					}
					
					$aRecords2[$iCnt]['end']='';
					$fPayedSuma+=$fpayed;
					$fToPaySuma+=$ftopay;
					
					++$iCnt;
				}
				
				$iCountL = count($aTransports);
				
			$fPayedSuma = floatval($fPayedSuma).'';
			$aItem['paidamount'] = floatval($aItem['paidamount']).'';				
					
			$sButtonAdd='';
			// $iBylo == 8 && <- Usunięcie skonsultowane telefonicznie z Marcinem K. 15.07.2011
			//  && $bAllMatched == true
			if($fPayedSuma < $aItem['paidamount']) {
				$ftopay=0.00;
				$fpayed=0.00;
				
				$sJS5.='haveAdditional['.$aItem['id'].']=1;';
				//predykcja zamówienia które powinno tutaj 
				$sSql = "SELECT id, to_pay, transport_number
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE order_status = '4'
						AND (payment_status = '0' OR IF(second_payment_enabled='1', second_payment_status = '0', 1=2) OR paid_amount < to_pay)
						AND (payment_type = 'postal_fee' OR second_payment_type = 'postal_fee')
						AND to_pay='".(Common::FormatPrice2($aItem['paidamount']-$fPayedSuma))."' 
						AND transport_number>".intval($iMinListNum)."
						AND correction <> '1'";
				$aPerdict=Common::GetRow($sSql);
				$ftopay=$fpayed=$aPerdict['to_pay'];
				
				$sItem= $aItem['id'].'additional_0';
				$sElements.=' document.getElementById(\'topay'.$sItem.'\'),';
				
				$sButtonAdd='
		<script type="text/javascript">
			// na wstepie ukrywamy wszystkie dodatkowe pola
			$(\'div[id^="additional_row_'.$aItem['id'].'_"][id$="_3"]\').each(function (i) {
				if (this.style.display == "none") {
					$(this).parent().parent().hide();
				}
			});

			var iAdditional = 0;
			
			function additionalScope() {
				showAdditionalRow(\''.$aItem['id'].'_\'+iAdditional); 
				haveAdditional[\''.$aItem['id'].'_\'+iAdditional]=0; 
				recount(\''.$aItem['id'].'\'); 
				iAdditional++;
				
				if (iAdditional==20) {
					$("#additional_row_'.$aItem['id'].'_0_show").hide();
				}
				return false; 
			}
		</script>
					<input type="hidden" name="__'.$aItem['id'].'_0_additional_enabled" 
														id="__'.$aItem['id'].'_0_additional_enabled" value="0" />
															<input id="additional_row_'.$aItem['id'].'_0_show" 
																		 style="margin-left:14px;" 
																		 type="button" 
																		 value="Dodaj nr" 
																		 onclick="additionalScope();" />';
				
				$aRecords2[$iCnt]['title']='<div id="additional_row_'.$aItem['id'].'_0_1" style="display:none;">				</div>';
				$aRecords2[$iCnt]['realization_date']='&nbsp;';
				$aRecords2[$iCnt]['matched_order']='<div id="additional_row_'.$aItem['id'].'_0_2" style="display:none;">'.Form::GetSelect('order[__'.$sItem.']', array('style'=>'width:240px;', 'onchange'=>'setPay(\''.$sItem.'\', this.value, \''.$aItem['id'].'\', \''.$aItem['id'].'_'.$iCountL.'\');'), $this->findOrders(), $aPerdict['id']).'</div>';
				$aRecords2[$iCnt]['topay']='<div id="additional_row_'.$aItem['id'].'_0_3" style="display:none;"><span name="topay'.$aItem['id'].'_0" id="topay'.$sItem.'">'.Common::FormatPrice($ftopay).'</span><input type="hidden" name="topayF['.$sItem.']" id="topayF['.$sItem.']" value="'.$ftopay.'" /></div>';
				$aRecords2[$iCnt]['paidamount']='<div id="additional_row_'.$aItem['id'].'_0_4" style="display:none;">		<input onblur="recount('.$aItem['id'].');" style="width:50" name="payed'.$aItem['id'].'['.($iCountL).']" id="payed'.$aItem['id'].'['.($iCountL).']" value="'.Common::FormatPrice($fpayed).'" /></div>';
				$aRecords2[$iCnt]['end']='<div id="additional_row_'.$aItem['id'].'_0_5" style="display:none;">					</div>';
				
				$iCountRecords = count($aRecords2);
				// dodatkowe pola 
				for ($iAddPool=1; $iAddPool<20; $iAddPool++) {
					$aPerdict = array();
					$ftopay = 0;
					$fpayed = 0;

					$sItem = $aItem['id'].'additional_'.$iAddPool;
					$sElements.=' document.getElementById(\'topay'.$sItem.'\'),';

					$sButtonAdd .='<input type="hidden" name="__'.$aItem['id'].'_'.$iAddPool.'_additional_enabled" id="__'.$aItem['id'].'_'.$iAddPool.'_additional_enabled" value="0" />';

					$aRecords2[$iCountRecords+$iAddPool]['title'] = '<div id="additional_row_'.$aItem['id'].'_'.$iAddPool.'_1" style="display:none;">				</div>';
					$aRecords2[$iCountRecords+$iAddPool]['realization_date']='&nbsp;';
					$aRecords2[$iCountRecords+$iAddPool]['matched_order'] = '<div id="additional_row_'.$aItem['id'].'_'.$iAddPool.'_2" style="display:none;">'.Form::GetSelect('order[__'.$sItem.']', array('style'=>'width:240px;', 'onchange'=>'setPay(\''.$sItem.'\', this.value, \''.$aItem['id'].'\', \''.$aItem['id'].'_'.$iAddPool.'\');'), $this->findOrders(), $aPerdict['id']).'</div>';
					$aRecords2[$iCountRecords+$iAddPool]['topay'] = '<div id="additional_row_'.$aItem['id'].'_'.$iAddPool.'_3" style="display:none;"><span name="topay'.$aItem['id'].'_'.$iAddPool.'" id="topay'.$sItem.'">'.Common::FormatPrice($ftopay).'</span><input type="hidden" name="topayF['.$sItem.']" id="topayF['.$sItem.']" value="0.00" /></div>';
					$aRecords2[$iCountRecords+$iAddPool]['paidamount'] = '<div id="additional_row_'.$aItem['id'].'_'.$iAddPool.'_4" style="display:none;">		<input onblur="recount('.$aItem['id'].');" style="width:50" name="payed'.$aItem['id'].'['.($iCountL+$iAddPool).']" id="payed'.$aItem['id'].'['.($iCountL+$iAddPool).']" value="0.00" /></div>';
					$aRecords2[$iCountRecords+$iAddPool]['end'] = '<div id="additional_row_'.$aItem['id'].'_'.$iAddPool.'_5" style="display:none;">					</div>';
				}
				
				++$iCnt;
			}	
			else {
				$sJS5.='haveAdditional['.$aItem['id'].']=0;';
			}
					
			
			/** 
			 * TODO
			 * XXX UWAGA tymczasowo z warunku usunięto $fPayedSuma==$aItem['paidamount'] &&
			 */
			if( isset($_POST['send_order_status_'.$aItem['id']])) {
				$this->updateOrdersStatus($aItem['id']);
			};	
			
			// sprawdź czy można dodawać
			$aRecords2[$iCnt]['title']=$sButtonAdd;	
			$aRecords2[$iCnt]['realization_date']='&nbsp;';	
			$aRecords2[$iCnt]['matched_order']='<span style="font-weight:bold; color:#f00;" id="matchedStatus'.$aItem['id'].'"></span>';
			$aRecords2[$iCnt]['topay']='<span style="font-weight:bold;" id="topaysum'.$aItem['id'].'">'.Common::FormatPrice($fToPaySuma).'</span>';
			$aRecords2[$iCnt]['paidamount']='<span style="font-weight:bold;" id="payedsum'.$aItem['id'].'">'.Common::FormatPrice($fPayedSuma).'</span>';
			$aRecords2[$iCnt]['end']='';	
			++$iCnt;		
		}
					
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'paidamount'=> array(
					'editable'	=> true
					)
			);
			$aEditableSettings = array(
			'paidamount' => array(
				'type' => 'text'
			)
		);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords2, $aColSettings,$aEditableSettings);
		}

			$aRecordsFooter = array(
			array('go_back'),
			array()	
			);
			$aRecordsFooterParams = array(
				
			);


		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		
		//wczytanie tablicy z wartośiami zamówień
		$sSql = "SELECT CONCAT('orderValues[',id, ']=', (to_pay-paid_amount),'; ') AS label
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE order_status = '4'
						AND (payment_status = '0' OR IF(second_payment_enabled='1', second_payment_status = '0', 1=2) OR paid_amount < to_pay)
						AND (payment_type = 'postal_fee' OR second_payment_type = 'postal_fee') 
						AND correction <> '1'";
						//."LIMIT 5";
		// XXX TODO USUNĄĆ LIMIT
		$aValues=Common::GetAll($sSql);
		foreach($aValues as $sItem)
			$sJS2.=$sItem['label']."\n";
		
		$sJS = '
		<script type="text/javascript">
		var iAdditional = 0 ;
		var haveAdditional=Array();
		'.$sJS5.'
		function showAdditionalRow(id) {
			document.getElementById("additional_row_"+id+"_1").style.display="block";
			document.getElementById("additional_row_"+id+"_2").style.display="block";
			document.getElementById("additional_row_"+id+"_3").style.display="block";
			document.getElementById("additional_row_"+id+"_4").style.display="block";
			document.getElementById("additional_row_"+id+"_5").style.display="block";
			$("#additional_row_"+id+"_3").parent().parent().show();
			
			//document.getElementById("additional_row_"+id+"_show").style.display="none";
			document.getElementById("__"+id+"_additional_enabled").value="1";
			haveAdditional[id]="0";
		}
		
		
		var orderValues= new Array();
		var paymentValues= new Array();
		'.$sJS3.'
		'.$sJS2.'
		
		function setPay(tNum, oNum, pId, sPId) {
			if(oNum>0) {
				document.getElementById("topay"+tNum).innerHTML=roundNumber(orderValues[oNum],2);
				document.getElementById("topayF["+tNum+"]").value=roundNumber(orderValues[oNum],2);
				
				document.getElementById("topay"+tNum).innerHTML=document.getElementById("topay"+tNum).innerHTML.replace(".",",");
				}
			else {
				document.getElementById("topay"+tNum).innerHTML="0.00";
				document.getElementById("topayF["+tNum+"]").value=0.00;
			}	
			var pola=new Array('.substr($sElements, 0,-1).');
			var suma=0;
			var suma2=0;
			var skipNext=0;
			
			/**
				Obliczanie ilości elementów wyświetlonych do sumowania
			**/
			var iAddRowCount = 0;
			$(\'div[id^="additional_row_\'+pId+\'_"][id$="_3"]\').each(function (i) {
				if (this.style.display == "none") {
					iAddRowCount++;
				}
			});
			
			
			var iCntCnt = pola.length - iAddRowCount;
			if(haveAdditional[pId]=="1")
				//--iCntCnt;
			
			for(i=0;i<iCntCnt;++i) {
				//if (parseFloat(pola[i].innerHTML) > 0) {
					suma= parseFloat(suma+parseFloat(pola[i].innerHTML.replace(",",".")));
				//}
				
				if(paymentValues[pId]-suma2-parseFloat(pola[i].innerHTML.replace(",","."))>=0) {
					document.getElementById("payed"+pId+"["+i+"]").value=parseFloat(pola[i].innerHTML.replace(",","."));
				}
				else {
					document.getElementById("payed"+pId+"["+i+"]").value=roundNumber(paymentValues[pId]-suma2,2);
				}	
					
				//if (parseFloat(document.getElementById("payed"+pId+"["+i+"]").value) > 0) {
					suma2+=parseFloat(document.getElementById("payed"+pId+"["+i+"]").value.replace(",","."));	
				//}
				
				document.getElementById("payed"+pId+"["+i+"]").value=document.getElementById("payed"+pId+"["+i+"]").value.replace(".",",");
				}
				
			document.getElementById("topaysum"+pId).innerHTML=roundNumber(suma,2);
			document.getElementById("topaysum"+pId).innerHTML=document.getElementById("topaysum"+pId).innerHTML.replace(".",",");
			
			
			var roznica=roundNumber(paymentValues[pId]-suma, 2);
			if(roznica>0) {
				document.getElementById("matchedStatus"+pId).innerHTML="Nadpłata "+roznica+" zł";
				document.getElementById("payedsum"+pId).innerHTML=roundNumber(suma,2);
				}
			else if(roznica<0) {
				document.getElementById("matchedStatus"+pId).innerHTML="Niedopłata "+(-roznica)+" zł" ;
				document.getElementById("payedsum"+pId).innerHTML=roundNumber(paymentValues[pId],2);
				}
			else {
				document.getElementById("matchedStatus"+pId).innerHTML="<span style=\"color:#0c0;\">Kwoty zgodne</span>";
				document.getElementById("payedsum"+pId).innerHTML=roundNumber(suma,2);
				} 
				
			document.getElementById("matchedStatus"+pId).innerHTML=document.getElementById("matchedStatus"+pId).innerHTML.replace(".",",");
			document.getElementById("payedsum"+pId).innerHTML=document.getElementById("payedsum"+pId).innerHTML.replace(".",",");
			recount(pId);
	}
	
	function recount(pId) {
			var pola=new Array('.substr($sElements, 0,-1).');
			var suma=0;
			var suma2=0;
			
			/**
				Obliczanie ilości elementów wyświetlonych do sumowania
			**/
			var iAddRowCount = 0;
			$(\'div[id^="additional_row_\'+pId+\'_"][id$="_3"]\').each(function (i) {
				if (this.style.display == "none") {
					iAddRowCount++;
				}
			});
			
			var iCntCnt = pola.length - iAddRowCount;
			
			if(haveAdditional[pId]=="1") {
				//--iCntCnt;
			}
				
			for(i=0;i<iCntCnt;++i) {
				//if (parseFloat(pola[i].innerHTML) > 0) {
					suma= parseFloat(suma+parseFloat(pola[i].innerHTML.replace(",",".")));
				//}
				//alert(pId);
				//alert(i);
				if (parseFloat(document.getElementById("payed"+pId+"["+i+"]").value) > 0) {
					suma2+=parseFloat(document.getElementById("payed"+pId+"["+i+"]").value.replace(",","."));	
				}
				document.getElementById("payed"+pId+"["+i+"]").value=document.getElementById("payed"+pId+"["+i+"]").value.replace(".",",");
			}
			document.getElementById("topaysum"+pId).innerHTML=roundNumber(suma,2);
			document.getElementById("topaysum"+pId).innerHTML=document.getElementById("topaysum"+pId).innerHTML.replace(".",",");
			
			
			var roznica=roundNumber(paymentValues[pId]-suma, 2);
			var roznica2=roundNumber(paymentValues[pId]-suma2, 2);
			if(roznica2>0) {
				document.getElementById("matchedStatus"+pId).innerHTML="Nadpłata "+(roznica2)+" zł";
				document.getElementById("payedsum"+pId).innerHTML=roundNumber(suma2,2);
				}
			else if(roznica2<0) {
				document.getElementById("matchedStatus"+pId).innerHTML="Niedopłata "+(-roznica2)+" zł" ;
				document.getElementById("payedsum"+pId).innerHTML=roundNumber(suma2,2);
				}
			else {
				document.getElementById("matchedStatus"+pId).innerHTML="<span style=\"color:#0c0;\">Kwoty zgodne</span>";
				document.getElementById("payedsum"+pId).innerHTML=roundNumber(suma2,2);
				}
			 
			document.getElementById("matchedStatus"+pId).innerHTML=document.getElementById("matchedStatus"+pId).innerHTML.replace(".",",");
			document.getElementById("payedsum"+pId).innerHTML=document.getElementById("payedsum"+pId).innerHTML.replace(".",",");
	}
			
		function roundNumber(num, dec) {
			var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
			return result;
			}
	// <![CDATA[
		$(document).ready(function(){
		
		'.$sJS4.'
			$("#orders_bank_statements").submit(function() {
				var sErr = "";
				$(".heditable_input, .editable_input").each(function(){
					regex = /^editable\[([A-Za-z0-9_]+)\]\[([A-Za-z0-9_]+)\]$/;
					matches = regex.exec(this.name);
					if(matches[2] == "paidamount"){
						$(this).val($(this).val().replace(/&nbsp;/g, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
						if ($(this).val() != "" && (!$(this).val().match(/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) || $(this).val() < 0)) {
							sErr += "\t- Zapłacono\n";
						}
					}
					
				});
				if(sErr != ""){
					alert("'.$aConfig['lang']['form']['error_prefix'].'\n"+sErr+"'.$aConfig['lang']['form']['error_postfix'].'");
					return false;
				}
			});
		});
		
	// ]]>
	</script>';
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.
																				$pView->Show());
		
	}// end of Details() method
	
	
	/**
	 * Metoda sprawdza czy można zmatchować przelew
	 *
	 * @global type $aConfig
	 * @return bool 
	 */
	function checkCanMatchOrder($iId) {
		global $aConfig;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders_bank_statements 
						 WHERE is_Opek='1' AND `deleted`='0' AND ISNULL(matched_order) AND id=".$iId;
		return intval(Common::GetOne($sSql))>0 ? true : false;
	} // end of checkCanMatchOrder() method
	
	
	/**
	 * Metoda wyswietla liste subskrybentow newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');
		include_once('Form/Form.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true,
			'editable' => true,
			'checkboxes'=>false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'title',
				'content'	=> 'Data',
				'sortable'	=> false,
			  'width'=>'150'
			),
			array(
				'db_field'	=> 'matched_order',
				'content'	=> 'Nr listów',
				'sortable'	=> false,
			),/*
			array(
				'db_field'	=> 'topay',
				'content'	=> 'Kwota',
				'sortable'	=> false,
				'width'	=> '60'
			),*/
			array(
				'db_field'	=> 'paidamount',
				'content'	=> 'Kwota',
				'sortable'	=> false,
				'width'	=> '100'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
		$sJS3='';
		$sJS4='';
		$sJS5='';
		
		
		$sFilter='';

		switch($_POST['f_status']) {
			default: $sFilter=' AND matched_order IS NULL AND `deleted`=\'0\''; 	break;
			case'3': $sFilter=' AND matched_order IS NOT NULL AND `deleted`=\'0\''; 	break;
			case'1': $sFilter=' AND `deleted`=\'1\' '; 	break;
			case'2': $sFilter=''; 	break;
		}
		
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(DISTINCT A.id)
						 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements AS A
						 WHERE is_Opek='1' ".$sFilter.
		(isset($_POST['search']) && !empty($_POST['search']) ? (" AND (A.title LIKE '%".$_POST['search']."%' OR A.id LIKE '%".$_POST['search']."%') ") : '');
		$iRowCount = intval(Common::GetOne($sSql));

		$pView = new View('orders_bank_statements', $aHeader, $aAttribs, $iRowCount);
		$pView->AddRecordsHeader($aRecordsHeader);
		//  is_Opek='1' AND `deleted`='0' AND finished='0' AND ISNULL(matched_order)

		// FILTRY
		// wszystkie/usuniete/nierozliczone
		$aStatuses = array(
			array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_new']),
			array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['f_matched']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_deleted']),
			array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
		);
		$pView->AddFilter('f_status', $aConfig['lang'][$this->sModule]['f_status'], $aStatuses, $_POST['f_status']);
		
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT  DISTINCT id, A.created, A.title, A.paid_amount paidamount
						 	 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements AS A
						 	 WHERE is_Opek='1' ".$sFilter.
			(isset($_POST['search']) && !empty($_POST['search']) ? (" AND (A.title LIKE '%".$_POST['search']."%' OR A.id LIKE '%".$_POST['search']."%') ") : '').
							 " ORDER BY ".(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			foreach($aRecords as $iKey =>$aItem) {
				$aRecords[$iKey]['title']=str_replace('OPEK SPOLKA Z OGRANICZONA','',$aItem['title']);
				$aRecords[$iKey]['paidamount'] = Common::formatPrice($aRecords[$iKey]['paidamount']);
				$aRecords[$iKey]['paidamount'].= ' zł';
			}
					
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'paidamount'=> array(
					'editable'	=> false
					),
				'title' => array (
					'link'	=> phpSelf(array('do' => 'details', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('details'),
					'params' => array (
												'details'	=> array('id'=>'{id}'),
												//'delete'	=> array('id'=>'{id}')
											)
				)	
					
			);
			$aEditableSettings = array(
		
		);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings,$aEditableSettings);
		}

			$aRecordsFooter = array(
			array(),
			array()	
			);
			$aRecordsFooterParams = array(
				
			);


		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.
																				$pView->Show());
	} // end of Show() function
	
	
	/**
	 * Metoda sprawdza czy niepuste wartości tablicy powtarzają się
	 *
	 * @param array $aArr
	 * @return bool 
	 */
	function arrayHaveDuplicateKey($aArr) {
		
		foreach ($aArr as $sKey => $sVal) {
			if ($sVal != '') {
				$aTmp = $aArr[$sKey];
				unset($aArr[$sKey]);
				if (array_search($sVal, $aArr) !== false) {
					// jest duplikat
					return true;
				} else {
					$aArr[$sKey] = $sVal;
				}
			}
		}
		
		// brak duplikatow
		return false;
	}// end of arrayHaveDuplicateKey() method
	
	
	/**
	 * Metoda zapisuje do Przelewy Pobrania
	 * 
	 * Uwaga modyfikacja 22.07.2011 nadpłata może wystąpić i możemy zatwierdzić 
	 * niezgodny przelew ponieważ istnieją paczki które nie występują w systemie
	 *
	 * @global array $aConfig
	 * @param object $pSmarty
	 * @return array
	 */
	function saveOpek($pSmarty) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		
		// wyczyścmy puste wpisy
		$sKeyPrg = '';
		foreach ($_POST['order'] as $iKey => $sVal) {
			if ($sVal == '' && strstr($iKey, 'additional')) {
				unset($_POST['order'][$iKey]);
			}
			$sKeyPrg = preg_replace("/__(\d+)additional_(\d+)/", '__$1_$2_additional_enabled', $iKey);
			
			if (strstr($iKey, 'additional') && $_POST[$sKeyPrg] != '1') {
				unset($_POST['order'][$iKey]);
			}
			$sKeyPrg = '';
		}
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		// czy tabela zamówień ma duplikaty
		if ($this->arrayHaveDuplicateKey($_POST['order']) === true) {
			$oValidator->sError .= $aConfig['lang'][$this->sModule]['save_err_unique'];
		}	
		if (!$oValidator->Validate($_POST) || ($oValidator->sError != '')) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$iOrd = $_POST['przelew'][0];
			unset($_POST);
			$this->Details($pSmarty, $iOrd);	
			return;
		}

		// rozpoczęcie transakcji
		Common::BeginTransaction();
		
		$aPayed=array();	
		foreach($_POST['przelew'] as $iId)
				$aPayed=array_merge($aPayed,$_POST['payed'.$iId]);
		
		// tu zawsze powinien być jeden przelew
		$iBankStatementId = $_POST['przelew'][0];
			
		$iCnt=0;	
		foreach($_POST['order'] as $iKey =>$iItem) {
			// flaga określa czy ta iteracja dodaje nowe listy przewozowe do przelewu
			$bItAdd = false;
			//echo 'ITERACJA '.$iKey."<br />";
			// nr listu przewozowego
			if ($iKey > 0 && is_numeric($iKey)) {
				$sTransportName = $this->getTransportNumberById($iKey);
			}
			// pobierz nr listów przewozowych
			
			$sNamStart=substr($iKey, 0, 2);
			// Czy pola dodatkowe
			if($sNamStart=='__') {
				//pomijamy - pole zbedne
				if(strpos($iKey, 'enabled')) continue;
				
				//pole specjalne
				//wyciagamy id przelewu
				$iNum = str_replace('__','',$iKey);
				preg_match("/additional_(\d)$/", $iKey, $aMatches);
				$iNum = str_replace($aMatches[0],'',$iNum);
				//jeśli kliknelismy przycisk - oznacza ze chcemy zapisac doatkowe
				// SPRAWDŹ DODATKOWE POZYCJE
				$sEnableName = '__'.$iNum.'_'.$aMatches[1].'_additional_enabled';
				if ($_POST[$sEnableName] == '1') {
					//zmieniamy iKey
					$sTransportName = Common::GetOne('SELECT transport_number FROM '.$aConfig['tabls']['prefix'].'orders WHERE id='.$iItem);
					if ($sTransportName <= 0) {
						$bIsErr = true;
					}
					
					// dodajemy nowe powiązanie przelew -> zamówienie
					$iKey = str_replace('__', '', $iKey);
					$aValues = array(
							'orders_bank_statement_id' => $iNum,
							'transport_number' => $sTransportName,
							'order_id' => $iItem,
							'payed' => str_replace(',', '.', $aPayed[$iCnt]),
							'topay' => $_POST['topayF'][$iKey]
					);
					if (Common::Insert($aConfig['tabls']['prefix']."orders_linking_transport_numbers", $aValues) === false) {
						$bIsErr = true;
					}
					
					//dopsujemy list przewozowy do tytulu
					$sSql='UPDATE '.$aConfig['tabls']['prefix'].'orders_bank_statements SET title=CONCAT(title,\' \',\''.$sTransportName.'\') WHERE id='.$iNum;
					if(Common::Query($sSql) === false)	{ 
						$bIsErr = true; 
					}
					$bItAdd = true;
				}
			}
						
			
			if ($_POST['topayF'][$iKey] > 0 && $iItem > 0 && $sTransportName != '' && $bItAdd == false) {
				
				// aktualizacja odpowiednich rekordów
				$aValues = array(
						//'transport_number' => // nie zmieniamy numeru listu przewozowego
						'order_id' => $iItem,
						'payed' => str_replace(',', '.', $aPayed[$iCnt]),
						'topay' => $_POST['topayF'][$iKey],
				);
				if ($iItem > 0) {
					if (Common::Update($aConfig['tabls']['prefix']."orders_linking_transport_numbers", $aValues, ' id='.$iKey) === false) { 
								$bIsErr = true; 
					}
				}
			}
			$iCnt++;	
		}
		
		if (!$bIsErr) {
			// XXX TODO zmienić na commit
			Common::CommitTransaction();
			// rekord zostal zaktualizowany
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_ok'], false);
			// dodanie informacji do logow
			AddLog($aConfig['lang'][$this->sModule]['edit_ok'], false);
		}
		else {
			Common::RollbackTransaction();
			// rekord nie zostal zmieniony, 
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_err']);
			AddLog($aConfig['lang'][$this->sModule]['edit_ok']);
		}
		//$this->Show($pSmarty);
		$this->Details($pSmarty, $_POST['przelew'][0]);	
	}// end of Details() method


	/**
	 * Metoda oznacza przelew jako usunięty
	 *
	 * @global type $aConfig
	 * @param type $pSmarty
	 * @param type $iId 
	 */
	function removeOpek($pSmarty, $iId) {
		global $aConfig;

		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders_bank_statements
			SET `deleted`='1'			
			WHERE id = ".$iId;

		if(!Common::Query($sSql)) {
			//blad
			$sMsg = $aConfig['lang'][$this->sModule]['del_err'];
			$this->sMsg = GetMessage($sMsg);
		}
		else {
			//ok
			$sMsg = $aConfig['lang'][$this->sModule]['del_ok'];
			$this->sMsg = GetMessage($sMsg, false);
		}
		
		unset($_GET['do']);
		$this->Show($pSmarty);
	}// end of removeOpek() method
	
		
	/**
	 *metoda pobiera identyfikatory zmatchowanych zamowien z tym rpzelewem i ustawia im statusy na zaplacone i datę
	 *Params
	 * 
	 *$iStateId - identyfikator platnosci opek
	 * 
	 */
	function updateOrdersStatus($iStateId) {
				global $aConfig;
				$aValues=array('matched_order'=>1);
				//zapisac status dla przelewu
				$bIsErr=false;
				Common::BeginTransaction();
				
				if(Common::Update($aConfig['tabls']['prefix']."orders_bank_statements", $aValues, "id = ".$iStateId) === false)
							$bIsErr=true;
						
				//pobrac listę zmatchowanych
				$sSql='SELECT title, created FROM '.$aConfig['tabls']['prefix'].'orders_bank_statements WHERE id='.$iStateId;
				$aItem=Common::GetRow($sSql);
				/*
				$sListaTmp=preg_replace('[ ]', '', $aItem['title']);
				if(($iPos=strpos($sListaTmp, 'REF'))>0)
					$sListaTmp=substr($sListaTmp, 0, $iPos);
					
				$sListaTmp=preg_replace('[a-zA-Z]', '', $sListaTmp);
				*/
				$fPayedSuma=$fToPaySuma=0.0;	
				$aLista=array();
				$iStart=0;
				$aValues=array(
					'payment_date'=>$aItem['created'],
					'payment_status' => '1',
				);
				//$iCountL = strlen($sListaTmp)/8;
				
				$aTransports = $this->getLinkingTransportNumbers($iStateId);
				$iCountL = count($aTransports);
				/*
				for($i=0;$i<$iCountL;++$i){
					$sItem=substr($sListaTmp, $iStart, 8);
					if(strlen($sItem)==8){
						*/

				foreach ($aTransports as $i => $aTransport){
					if ($aTransport['order_id'] <= 0) {
						$iOrderId = $this->getOrderByTransportNum($aTransport['transport_number']);
					} else {
						$iOrderId = $aTransport['order_id'];
					}
					if ($iOrderId<=0) {continue;}
						
						//$sSql='SELECT order_id FROM `'.$aConfig['tabls']['prefix'].'orders_linking_transport_numbers` WHERE transport_number='.$sItem;
						//$iOrderId=Common::GetOne($sSql);
						//jezeli nie przypisano zamowienia pomijamy
						//if(!$iOrderId) {$iStart+=8; continue;}
						
						$aValues['paid_amount'] = $aTransport['payed'];//Common::GetOne('SELECT payed FROM `'.$aConfig['tabls']['prefix'].'orders_linking_transport_numbers` WHERE transport_number='.$sItem);
						
						$sGetPTypeSql = "SELECT payment_type, second_payment_type, payment_date, second_payment_date 
														 FROM ".$aConfig['tabls']['prefix']."orders
														 WHERE id=".$iOrderId;
						$aOData = Common::GetRow($sGetPTypeSql);
						$bFirst = true;
						if ($aOData['second_payment_type'] == 'postal_fee') {
							$bFirst = false;
						}
						$sPoolPrefix = '';
						if ($bFirst == false) {
							$sPoolPrefix = 'second_';
						}
						// XXX TODO trzeba tu wprowadzić zmienę drugiej a nie pierwszej w przypadku wybranej drugiej metody płatnosci
						$sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders
										 SET paid_amount = (paid_amount+".$aValues['paid_amount']."), 
												 ".$sPoolPrefix."payment_date = '".$aValues['payment_date']."',
												 ".$sPoolPrefix."payment_status = '".$aValues['payment_status']."' 
										 WHERE id = ".$iOrderId;
						if(Common::Query($sSql) === false) {
							$bIsErr=true;
							break;
						}
						/*
						if(Common::Update($aConfig['tabls']['prefix']."orders", $aValues, "id = ".$iOrderId) === false) {
							$bIsErr=true;
							break;
							}
						 */
						$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."orders_bank_statements
						WHERE id = ".$iStateId;
						$aStatement=Common::getRow($sSql);
						$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id = ".$iOrderId;
						$aOrder=Common::getRow($sSql);
						//dodanie informacji o platnosci do zamowienia
						$aValues2=array(
							'order_id'=>$iOrderId,
              'statement_id' => $iStateId,
							'date'=>$aStatement['created'],
							'ammount' => $aTransport['payed'],//Common::GetOne('SELECT payed FROM `'.$aConfig['tabls']['prefix'].'orders_linking_transport_numbers` WHERE transport_number='.$sItem), // TODO zmienić na ID !!!
							'type'=>'opek',
							'iban'=>$aStatement['iban'],
							'reference'=>$aStatement['reference'],
							'title'=>$aStatement['title'],
							);

						if(Common::Insert($aConfig['tabls']['prefix']."orders_payments_list", $aValues2,'', false) === false){
								$bIsErr = true;		
						} 	
					}
					//else break;
				
				//$iStart+=8;
				//}
				//dla kazdego ze zmatchowanych ustawic odpowiednia datę i status
			if ($bIsErr) {
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				$sMsg = $aConfig['lang']['m_zamowienia_list_opek']['status_edit_err'];
				$this->sMsg = GetMessage($sMsg);
			}
			else {
				Common::CommitTransaction();
				$sMsg = $aConfig['lang']['m_zamowienia_list_opek']['status_edit_ok'];
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
			}
					
			if ( !empty($sMsg) ) {
				// komunikat
				$_SESSION['_tmp']['message'] = array(
						'content' => $sMsg,
						'is_error' => $bIsErr == true ? '1' : '0'
				);
				doRedirect($aConfig['common']['base_url_https'].phpSelf());
				// uciekamy na listę
			}
			
		}	
	
	function getOrderByTransportNum($iNum) {
	global $aConfig;
		$sSql = "SELECT order_id FROM ".$aConfig['tabls']['prefix']."orders_linking_transport_numbers
						WHERE transport_number = '".$iNum."'";
		$iONum=Common::GetOne($sSql);
		if($iONum>0)	return $iONum;
		
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders A
						WHERE A.transport_number = '".$iNum."'";
		return Common::GetOne($sSql);	
	}
	
	
	/**
	 * Metoda pobiera ile zapłacono za zamówienie z tabeli wiązącej zamówienia z przelewami,
	 *	jeśli tam nie istnieje dopasowuje z tabeli zamówień
	 *
	 * @global type $aConfig
	 * @param type $sNum
	 * @param type $iOId
	 * @return type 
	 */
	function getOrderByTransportPayed($sNum, $iOId) {
	global $aConfig;
	
		$sSql = "SELECT payed FROM ".$aConfig['tabls']['prefix']."orders_linking_transport_numbers
						WHERE transport_number = '".$sNum."'".( $iOId > 0? ' AND order_id='.$iOId : '');
		return Common::GetOne($sSql);
	}// end of getOrderByTransportPayed() method
	
	
	/**
	 * Metoda pobiera wartość do zapłaty z tabeli wiążącej zamówienia z przelewami, 
	 *	jeśli tam nie istnieje dopasowuje z tabeli zamówień
	 *
	 * @global type $aConfig
	 * @param type $iNum
	 * @param type $iOId
	 * @return type 
	 */
	function getOrderByTransportToPay($sNum, $iOId) {
	global $aConfig;
		
		$sSql = "SELECT (topay) FROM ".$aConfig['tabls']['prefix']."orders_linking_transport_numbers
						WHERE transport_number = '".$sNum."'".( $iOId > 0? ' AND order_id='.$iOId : '');
		$fToPay = Common::GetOne($sSql);
		
		
		if ( floatval($fToPay) <= 0.00 && intval($iOId) > 0 ) {
			// jeśli wartość nie została zdefiniowana ale jest podany numer zamówienia dopasowanego
			$sSql = 'SELECT (to_pay-paid_amount) FROM '.$aConfig['tabls']['prefix'].'orders where id='.$iOId;
			$fToPay = Common::GetOne($sSql);
		}
		if (floatval($fToPay) <= 0.00) {
			$fToPay = 0.00;
		}
		return $fToPay;
	}// end of getOrderByTransportToPay() method
	
	
	function getOrderInternalStatus($iId){
		global $aConfig;
		$sSql = "SELECT A.internal_status, A.payment_status, A.payment, A.value_brutto, 
										A.transport_cost, B.personal_reception, A.to_pay, A.paid_amount
						FROM ".$aConfig['tabls']['prefix']."orders A
						LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						ON B.id=A.transport_id
						WHERE A.id = ".$iId;
		return Common::GetRow($sSql);
	} // end of getOrderInternalStatus() method
	
	
	/**
	 * Metoda pobiera dane o powiązaniu transportów z przelewami
	 * 
	 * @global type $aConfig
	 * @param integer $iSId
	 * @return array
	 */
	function getLinkingTransportNumbers($iSId) {
		global $aConfig;
	
		$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_linking_transport_numbers
						WHERE orders_bank_statement_id = '".$iSId."'";
		
		return Common::GetAll($sSql);
	}// end of getLinkingTransportNumbers() method
	
	
	/**
	 * Metoda pobiera numer listu przewozowego na podstawie id
	 * 
	 * @global type $aConfig
	 * @param integer $iSId
	 * @return array
	 */
	function getTransportNumberById($iId) {
		global $aConfig;
	
		$sSql = "SELECT transport_number FROM ".$aConfig['tabls']['prefix']."orders_linking_transport_numbers
						WHERE id = ".$iId;
		return Common::GetOne($sSql);
	}// end of getLinkingTransportNumbers() method
	
	
	/**
	 * Metoda pobiera liste subskrybentow do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id subskrybentow
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, title
				 		 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements
					 	 WHERE id IN (".implode(',', $_POST['delete']).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	

	/**
	 * Pobiera id zamówienia dla danego numeru
	 * @param string $sNr - nr zamowienia
	 * @return in - id zamowienia
	 */
	function matchOrderNumber($sNr){
		global $aConfig;
		$sSql = "SELECT id
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE order_number = ".Common::Quote($sNr);
		return intval(Common::GetOne($sSql)); 
	}
	
	
	/**
	 * Pobiera nr zamowienia dla danego id
	 * 
	 * @param string $iOId - id zamowienia
	 * @return int - nr zamowienia
	 */
	function getOrderNumberByOrderId($iOId){
		global $aConfig;
		
		if ($iOId > 0) {
			$sSql = "SELECT order_number
							FROM ".$aConfig['tabls']['prefix']."orders
							WHERE id = ".$iOId;
			return Common::GetOne($sSql); 
		}
		return $aConfig['lang'][$this->sModule]['no_linked_order'];
	}// end of getOrderNumberByOrderId() method
	
	
/**
	 * Pobiera zamówienia dla dopasowania
	 * @return array - zamowienia
	 */
	function findOrders(){
		global $aConfig;
		//oszczędzamy zasoby
		if(is_array($this->aFoundOrders))
			return $this->aFoundOrders;
		
		$sSql = "SELECT id AS value, CONCAT(order_number, ' ', IF(ISNULL(transport_number),' ',transport_number)) AS label
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE order_status = '4'
						AND (payment_status = '0' OR IF(second_payment_enabled='1', second_payment_status = '0', 1=2) OR paid_amount < to_pay )
						AND (payment_type = 'postal_fee' OR second_payment_type = 'postal_fee')
						AND correction != '1'";
		
		$this->aFoundOrders=array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),Common::GetAll($sSql)); 
		return $this->aFoundOrders;
	}
	
	/**
	 * Pobiera dane przelewu
	 * @param int $iId - id przelewu
	 * @return array - dane przelewu
	 */
	function getStatement($iId){
		global $aConfig;
		
		$sSql = "SELECT id, paid_amount
						FROM ".$aConfig['tabls']['prefix']."orders_bank_statements
						WHERE id = ".$iId;
		return Common::GetRow($sSql); 
	}
	
	/**
	 * Pobiera numer zamówienia
	 * @param int $iId - id zamówienia
	 * @return string - nr zamówienia
	 */
	function getOrderNumber($iId){
		global $aConfig;
		$sSql = "SELECT order_number
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id = ".$iId;
		return Common::GetOne($sSql); 
	}
	

} // end of Module Class
?>
