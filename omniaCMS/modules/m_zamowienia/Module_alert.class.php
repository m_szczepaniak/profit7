<?php
/**
 * Klasa do dodawania nowego alertu ręcznie przez obsługę
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__zamowienia__alert {

  // komunikat
	public $sMsg;

	// nazwa modulu - do langow
	public $sModule;

	// id wersji jezykowej
	public $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	private $aPrivileges;
	
	// id strony modulu
	private $iPageId;
  
  // id uzytkownika
  private $iUId;
  
	// id listy zamowien
	private $iPPId;
  
  // id zamowienia
  private $iId;
  
  function __construct(&$pSmarty, $bInit = true) {

		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];
		$this->iPageId = getModulePageId('m_konta');

		if (isset($_GET['pid'])) {
			$iId = $_GET['pid'];
		}
    
		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		$this->sModule .= '_alert';
    
		$this->iUId = $_SESSION['user']['id'];
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    if ($bInit == false) {
      return;
    }
    switch ($sDo) {
      case 'insert':
        $this->Insert($pSmarty, $iId);
      break;
      case 'deactivate':
        $this->Deactivate($iId);
      break;
      case 'insert_to_group':
        $this->InsertToGroup($pSmarty);
      break;
      case 'add_to_group':
        $this->AddToGroup($pSmarty);
      break;
      default:
        $this->Add($pSmarty, $iId);
      break;
    }
  }// end of __construct()
  
  
  /**
   * Metoda deaktywuje alert
   * 
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iId
   */
  public function Deactivate($iId) {
    global $aConfig;
    $aUnsetGet = array('pid');
    if (isset($_GET['action_goback']) && $_GET['action_goback'] == '') {
      $aUnsetGet = array('pid', 'action');
    }
            
    if ($this->doDeactivate($iId, $_SESSION['user']['author']) === false) {
			// rekord nie zostal zmieniony
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['deactivate_err'],
											$this->_getItemName($iId),
                      $iId);
			$this->sMsg = GetMessage($sMsg);
      // dodanie informacji do logow
			AddLog($sMsg);
      setSessionMessage($sMsg);
      doRedirect($aConfig['common']['base_url_https'].''.phpSelf(array('do' => 'show', 'ppid' => $_GET['ppid'], 'id' => $iId, 'action' => $_GET['action_goback']), $aUnsetGet));
    } else {
      // rekord zostal zmieniony
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['deactivate_ok'],
											$this->_getItemName($iId),
                      $iId);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
      setSessionMessage($sMsg, false);
			doRedirect($aConfig['common']['base_url_https'].'/'.phpSelf(array('do' => 'show', 'ppid' => $_GET['ppid'], 'id' => $iId, 'action' => $_GET['action_goback']), $aUnsetGet));
    }
  }// end of Deactivate() method
  
  
  /**
   * Metoda pobiera nazwę elementu
   * 
   * @global type $aConfig
   * @param type $iId
   * @return type
   */
  private function _getItemName($iId) {
    
    $sSql = "SELECT message FROM orders_to_review WHERE id=".$iId;
    return Common::GetOne($sSql);
  }// end of _getItemName() method
  
  
  /**
   * Metoda deaktywuje alert
   * 
   * @param int $iId
   * @param string $sAuthor
   * @return type
   */
  public function doDeactivate($iId, $sAuthor) {
    
    $aValues = array(
        'disabled' => '1',
        'disabled_by' => $sAuthor,
        'disabled_datetime' => 'NOW()'
    );
    return Common::Update('orders_to_review', $aValues, ' id='.$iId);
  }// end of Deactivate() method
  
  
  /**
   * Metoda pobiera alerty
   * 
   * @param type $iOId
   */
  public function getAlertsUserOrderLists($iOId, $iUId) {
    
    $sSql = "SELECT * 
             FROM orders_to_review 
             WHERE order_id = ".$iOId." AND (user_id = ".$iUId." OR user_id IS NULL OR created_by_user_id = ".$iUId.")";
    return Common::GetAll($sSql);
  }// end of getAlertsLists() method
  
  
  /**
   * Metoda wyświetla formularz dodawania nowego alertu
   * 
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iId
   */
  public function Add(&$pSmarty, $iId) {
   global $aConfig;
    
		$aData = array();
		$aLang = $aConfig['lang'][$this->sModule];
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
    
    if (!empty($_POST)) {
      $aData = $_POST;
    }
    
    $sHeader = sprintf($aConfig['lang'][$this->sModule]['header'], $this->_GetOrderNumber($iId));
		
		$pForm = new FormTable('user_comments', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), $aConfig['common']['js_validation']);
    $pForm->AddHidden('do', 'insert');
    
    // komunikat
    $pForm->AddTextArea('message', $aLang['message'], $aData['message'], array('style' => 'width: 600px; height: 120px;'), 65535);
    
    // select
    $pForm->AddSelect('user_id', $aConfig['lang'][$this->sModule]['user_id'], array(), addDefaultValue($this->_getWebsiteUserList(), 'Wszyscy'), $aData['user_id'], '', false);
    
    $pForm->AddCheckBox('alert', $aConfig['lang'][$this->sModule]['alert'], array('onclick'=>'GetObject(\'do\').value=\'add\'; this.form.submit();'), '', isset($aData['alert']), false);
    
    if($aData['alert'] == '1') {
      $pForm->AddText('alert_datetime', $aConfig['lang'][$this->sModule]['alert_datetime'], (isset($aData['alert_datetime']) ? $aData['alert_datetime'] : date("Y-m-d H:i:s")), array('class' => 'datetimepicker'), '', 'text', true);
    }
    
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_0']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('action', 'pid')).'\');'), 'button'));
		
    
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
  }// end of add() method

    /**
     * Metoda wyświetla formularz dodawania nowego alertu
     *
     * @global type $aConfig
     * @param type $pSmarty
     * @param type $iId
     */
    public function AddToGroup(&$pSmarty) {
        global $aConfig;

        $aData = array();
        $aLang = $aConfig['lang'][$this->sModule];
        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        if (!empty($_POST)) {
            $aData = $_POST;
        }

        $aGroups = $this->getGroups();
        $aGroupsPrepared = [];

        foreach ($aGroups as $aGroup) {
            $aGroupsPrepared[] = [
                'value' => $aGroup['id'],
                'label' => $aGroup['name']
            ];
        }

        $sHeader = $aConfig['lang'][$this->sModule]['header_group'];

        $pForm = new FormTable('user_comments', $sHeader, array('action'=>phpSelf(array())), array('col_width'=>155), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', 'insert_to_group');

        // komunikat
        $pForm->AddTextArea('message', $aLang['message'], $aData['message'], array('style' => 'width: 600px; height: 120px;'), 65535);

        // select
        $pForm->AddSelect('group_id', $aConfig['lang'][$this->sModule]['group_id'], array(), $aGroupsPrepared, $aData['group_id'], '', false);

        $pForm->AddCheckBox('alert', $aConfig['lang'][$this->sModule]['alert'], array('onclick'=>'GetObject(\'do\').value=\'add_to_group\'; this.form.submit();'), '', isset($aData['alert']), false);

        if($aData['alert'] == '1') {
            $pForm->AddText('alert_datetime', $aConfig['lang'][$this->sModule]['alert_datetime'], (isset($aData['alert_datetime']) ? $aData['alert_datetime'] : date("Y-m-d H:i:s")), array('class' => 'datetimepicker'), '', 'text', true);
        }

        // przyciski
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_0']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('action', 'pid')).'\');'), 'button'));


        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
}// end of add() method
  
  /**
   * Metoda pobira uzytkowników do wybrania  w select
   * 
   * @return array
   */
  private function _getWebsiteUserList() {
    
    $sSql = "SELECT CONCAT(name, ' ', surname, ' (', login, ')') AS label, id AS value
             FROM users 
             WHERE active = '1'
             ORDER BY CONCAT(name, ' ', surname, ' (', login, ')')";
    return Common::GetAll($sSql);
  }// end of _getWebsiteUserList() method
  
  
  /**
   * Metoda pobiera numer zamówienia
   * 
   * @param int $iId
   * @return string
   */
  public function _GetOrderNumber($iId) {
    
    $sSql = "SELECT order_number 
             FROM orders
             WHERE id = ".$iId;
    return Common::GetOne($sSql);
  }// end of _GetOrderNumber() method

    /**
     * Pobranie grup
     *
     * @return mixed
     */
  function getGroups() {
    $sSql = 'SELECT id, name FROM users_groups LIMIT 100';

    return Common::GetAll($sSql);
  }
  
  /**
   * Metoda dodaje nowe zamowienie do sprawdzenia
   * 
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iId
   */
  public function Insert($pSmarty, $iId) {
    global $aConfig;
    
		$sSql = "SELECT name, surname
							FROM ".$aConfig['tabls']['prefix']."users
							 WHERE id = ".intval($_SESSION['user']['id']);
							 			 
		$aGeneratorName=Common::GetRow($sSql);
    
    $aValues = array(
        'order_id' => $iId,
        'created_by_user_id' => intval($_SESSION['user']['id']),
        'user_id' => ($_POST['user_id'] > 0 ? $_POST['user_id'] : 'NULL'),
        'alert' => (isset($_POST['alert']) ? '1' : '0'),
        'alert_datetime' => (isset($_POST['alert_datetime']) ? $_POST['alert_datetime'] : 'NULL'),
        'message' => $_POST['message'],
        'created' => 'NOW()',
        'created_by' => $aGeneratorName['name'].' '.$aGeneratorName['surname'],
    );
    $mRet = Common::Insert('orders_to_review', $aValues);
    if ($mRet > 0) {
      // ok
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
                      $this->_GetOrderNumber($iId));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
      setSessionMessage($sMsg, false);
      doRedirect($aConfig['common']['base_url_https'].'/');
    } else {
      // err
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
                      $this->_GetOrderNumber($iId));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Add($pSmarty, $iId);
    }
  }

    /**
     * Metoda dodaje alert dla grupy
     *
     * @global type $aConfig
     * @param type $pSmarty
     * @param type $iId
     */
    public function InsertToGroup($pSmarty) {
        global $aConfig;

        $sSql = "SELECT name, surname
							FROM ".$aConfig['tabls']['prefix']."users
							 WHERE id = ".intval($_SESSION['user']['id']);

        $aGeneratorName=Common::GetRow($sSql);

        $aValues = array(
            'order_id' => 'NULL',
            'group_id' => ($_POST['group_id'] > 0 ? $_POST['group_id'] : 'NULL'),
            'created_by_user_id' => intval($_SESSION['user']['id']),
            'user_id' => 'NULL',
            'alert' => (isset($_POST['alert']) ? '1' : '0'),
            'alert_datetime' => (isset($_POST['alert_datetime']) ? $_POST['alert_datetime'] : 'NULL'),
            'message' => $_POST['message'],
            'created' => 'NOW()',
            'created_by' => $aGeneratorName['name'].' '.$aGeneratorName['surname'],
        );
        $mRet = Common::Insert('orders_to_review', $aValues);
        if ($mRet > 0) {
            // ok
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_group_ok'],
                $_POST['group_id']);
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            setSessionMessage($sMsg, false);
            doRedirect($aConfig['common']['base_url_https'].'?frame=main');
        } else {
            // err
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_group_err'],
                $_POST['group_id']);
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            $this->AddToGroup($pSmarty);
        }
    }
}// end of class

?>