<?php
/**
 * Klasa zarządzania Magazynem - zamowione produkty
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-06-28 
 * @copyrights Marcin Chudy - Profit24.pl
 */
include_once($_SERVER['DOCUMENT_ROOT'].'LIB/orders/OrderMagazineData.class.php');
class Module extends orders\OrderMagazineData {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// Id strony modulu
	var $iPageId;
	
	// katalog plikow log
	var $sLogsDir;
  
  var $pDbMgr;


  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty, $bInit = FALSE) {
		global $aConfig, $pDB2, $pDbMgr;
    if ($bInit === TRUE) {
      return true;
    }
    
    $this->pDbMgr = $pDbMgr;
		$this->sErrMsg = '';
		$this->iPageId = getModulePageId('m_zamowienia');
		$this->sModule = $_GET['module'];
		$this->iLangId = $_SESSION['lang']['id'];

    
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
//    if (isset($_POST['do']) && $_POST['do'] != 'show_items') {
//      if (!isset($_POST['token']) || !isset($_SESSION['token']) || $_POST['token'] != $_SESSION['token']) {
//        $sMsg = _('Zabezpieczenie przed podwójnym przesłaniem formularza, pobierz teraz ponownie plik.');
//        $this->sMsg .= GetMessage($sMsg);
//        // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
//        $this->Show($pSmarty);
//        AddLog($sMsg);
//        return;
//      }
//      unset($_SESSION['token']);
//    }
    
    if (!empty($_POST)) {
      if ($this->validateAllForm() === false) {
          $this->Show($pSmarty);
      }
    }
    
		switch ($sDo) {
			case 'get_csv': $this->GetCSV(); break;
      case 'get_ateneum': $this->GetAteneum($pSmarty); break;
			case 'get_azymut': $this->GetAzymut($pSmarty); break;
      case 'get_super_siodemka': $this->GetSuperSiodemka($pSmarty); break;
      case 'get_olesiejuk': $this->GetOlesiejuk($pSmarty); break;
      case 'get_platforma': $this->GetPlatforma($pSmarty); break;
			case 'get_pdf': $this->GetPDF($pSmarty); break;
			case 'get_pdf_ext': $this->GetPDF_Ext($pSmarty); break;
			case 'get_mail': $this->GetEmailForm($pSmarty); break;
			case 'send_email': $this->SendEmail($pSmarty); break;
			case 'show_items': $this->Show($pSmarty,true); break;
			default: $this->Show($pSmarty); break;
		}
	} // end Module() function
  
  
  /**
   * 
   * @return boolean
   */
  private function validateAllForm() {
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
      $sErrLog = $oValidator->GetErrorString();
			$this->sMsg .= GetMessage($sErrLog, true);
      addLog($sErrLog, true);
			return false;
		}
    return true;
  }
  
  /**
	 * Metoda aktualizuje rabat uzytkownika o podanym Id
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function GetCSV() {
		global $aConfig;
    $aBooksToConfirm = unserialize(base64_decode(stripslashes($_POST['books_arr'])));
    if (empty($aBooksToConfirm)) {
      $this->sMsg = _('Wystąpił błąd podczas generowania pliku lub lista jest pusta.');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
    }
    
    //przerwanie po 25 pozycjach jeśli jest niepotwierdzone
    if($_POST['status'] == 3) {
      $iLimit = 25;
    }
    //przerwanie po 35 pozycjach jesli jest do zamówienia USTALONE Z Marcinem Chudym 01.09.2011
    if($_POST['status'] == 1) {
      $iLimit = 40;
    }
    $aNewBooks = $this->getLimitedBooks($aBooksToConfirm, $iLimit);
    
    // sprawdzamy czy nie puste i sortuejemy
    if (!empty($aNewBooks)) {
      $aNewBooks = $this->_sortArr($aNewBooks);
    }
 
		$bIsErr = false;
		$sFile = '';
		$aBooks2=array();
		$_GET['hideHeader'] = '1';
		
    try {
      $sListNumber = $this->getNewListNumber(intval($_POST['status']));
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }

		// rozpoczęcie transakcji
		Common::BeginTransaction();
			
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="zamowione_pozycje.xls"');
		header("Pragma: no-cache");
		header("Expires: 0");
		// zapis naglowka
		$sLine = "\t\t".sprintf($aConfig['lang'][$this->sModule]['csv_header_'.$_POST['status']],$sListNumber)."\r\n";
		$sFile .= $sLine;
		echo $sLine;
		$sLine = "\t\t".$aConfig['lang'][$this->sModule]['source_'.$_POST['source']]."\r\n\r\n";
		$sFile .= $sLine;
		echo $sLine;
		// zapis rekordow
		$sLine = $aConfig['lang'][$this->sModule]['books_list_id']."\t".
			$aConfig['lang'][$this->sModule]['books_list_name']."\t".
			$aConfig['lang'][$this->sModule]['books_list_isbn']."\t".
			$aConfig['lang'][$this->sModule]['books_list_ile']."\r\n";
		$sFile .= $sLine;
		echo $sLine;
		$iI=1;
		foreach ($aNewBooks as $aItem) {
			$iProductId = $aItem['id'];
      
			//przerwanie po 25 pozycjach
			if($iI>25 && $_POST['status']==3) break;
			++$iI;
			
			$aBook = $this->_getBookData($iProductId);
			$sLine = $iProductId."\t".$aBook['name']."\t".$aBook['isbn']."\t".$aItem['quantity']."\r\n";
			
			$aItemList=explode(',', $aItem['items']);
			foreach($aItemList as $iOneItem)
				$aBooks2[]=$iOneItem;
				
			$sFile .= $sLine;
			echo $sLine;
			if($_POST['status'] == '1'){
				if ($this->setOrderedItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
			if($_POST['status'] == '3'){
				if ($this->setHasNCItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
		}	
	// dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
		$aValues = array(
			'content' => nl2br($sFile),
			'saved_type' => '0',
			'status' => $_POST['status'],
			'source' => $_POST['source'],
			'date_send' => 'NOW()',
			'send_by' => $_SESSION['user']['name'],
			'number'=>$sListNumber
		);
		$iExportId=false;
		if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
			$bIsErr = true;
		}
		else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
			
			foreach ($aBooks2 as $iProductId=>$iItem) {
				$aValues = array(
					'send_history_id'=>$iExportId,
					'item_id'=>$iItem
					);
				if ( Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false) === false) {
					// błąd
					$bIsErr = true;
				}
			}
		}
		if ($bIsErr === true) {
			// błąd
			Common::RollbackTransaction();
		} else {
			// wszystko ok
			Common::CommitTransaction();
		}
	} // end of GetCSV() funciton
  
  
  /**
   * Metoda pobiera limit wartości po której należy przerwać pobieranie listy do Azymutu
   * 
   * @return double
   */
  private function getAzymutLimitPrice() {
    
    $sSql = "SELECT azymut_limit_price 
             FROM orders_magazine_settings 
             WHERE id = 1";
    return Common::GetOne($sSql);
  }// end of getAzymutLimitPrice method
  
  
	/**
   * Metoda pobiera limit wartości po której należy przerwać pobieranie listy do super siódemki
   * 
   * @return string
   */
  private function getPlatonLimitPrice() {

    $sSql = "SELECT platon_limit_price 
             FROM orders_magazine_settings 
             WHERE id = 1";
    return Common::GetOne($sSql);
  }// end of getAteneumLimitPrice method


	/**
	 * Metoda pobiera limit wartości po której należy przerwać pobieranie listy do super siódemki
	 *
	 * @return string
	 */
	private function getPandaLimitPrice() {

		$sSql = "SELECT panda_limit_price 
             FROM orders_magazine_settings 
             WHERE id = 1";
		return Common::GetOne($sSql);
	}// end of getAteneumLimitPrice method
  
  
	/**
   * Metoda pobiera limit wartości po której należy przerwać pobieranie listy do super siódemki
   * 
   * @return string
   */
  private function getAteneumLimitPrice() {

    $sSql = "SELECT ateneum_limit_price 
             FROM orders_magazine_settings 
             WHERE id = 1";
    return Common::GetOne($sSql);
  }// end of getAteneumLimitPrice method
  
	
	/**
   * Metoda pobiera limit wartości po której należy przerwać pobieranie listy do super siódemki
   * 
   * @return string
   */
  private function getSuperSiodemkaLimitPrice() {

    $sSql = "SELECT super_siodemka_limit_price 
             FROM orders_magazine_settings 
             WHERE id = 1";
    return Common::GetOne($sSql);
  }// end of getSuperSiodemkaLimitPrice method
  
	
	/**
	 * Metoda aktualizuje rabat uzytkownika o podanym Id
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function GetAzymut(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		$aBooks2=array();
		
    $aBooksToConfirm = unserialize(base64_decode(stripslashes($_POST['books_arr'])));
    if (empty($aBooksToConfirm)) {
      $this->sMsg = _('Wystąpił błąd podczas generowania pliku lub lista jest pusta.');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
    }
    
    //przerwanie po 25 pozycjach jeśli jest niepotwierdzone
    if($_POST['status'] == 3) {
      $iLimit = 25;
    }
    //przerwanie po 35 pozycjach jesli jest do zamówienia USTALONE Z Marcinem Chudym 01.09.2011
    if($_POST['status'] == 1) {
      $iLimit = 35;
    }
	
    $fLimitPrice = $this->getAzymutLimitPrice();
    $aNewBooks = $this->getLimitedBooksByPrice($aBooksToConfirm, $fLimitPrice, 'azymut');
    if ($aNewBooks === FALSE) {
      $this->sMsg = GetMessage(_('Lista produktów nie przekroczyła minimum o wartości '.Common::formatPrice($fLimitPrice).' zł'), true);
      $this->Show($pSmarty);
      return true;
    }
    $_GET['hideHeader'] = '1';
    // sprawdzamy czy nie puste i sortuejemy
    if (!empty($aNewBooks)) {
      $aNewBooks = $this->_sortArr($aNewBooks);
    }
		$sNow=$_POST['sNow'];
			$sSql='SELECT id FROM '.$aConfig['tabls']['prefix'].'orders_send_history WHERE `date_send`=\''.$sNow.'\' AND `status`=\''.$_POST['status'].'\' AND `send_by`=\''.$_SESSION['user']['name'].'\' AND `source`=\''.$_POST['source'].'\'';
				$iSent=Common::GetOne($sSql);
					
				if($iSent){
					//przekierowanie do historii - inaczej nie chce dzialac
					header('Location: ?frame=main&module_id=39&module=m_zamowienia&action=send_history');
					return;
				}
		
		$sFile = '';
    try {
      $sListNumber = $this->getNewListNumber(intval($_POST['status']));
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
		
		// rozpoczęcie transakcji
		Common::BeginTransaction();
		
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="zamowione_pozycje.txt"');
		header("Pragma: no-cache");
		header("Expires: 0");
		// zapis naglowka
		$sLine = "\t\t".sprintf($aConfig['lang'][$this->sModule]['txt_header_'.$_POST['status']], $sListNumber)."\r\n";
		$sFile .= $sLine;
		echo $sLine;
		$sLine = "\t\t".$aConfig['lang'][$this->sModule]['source_'.$_POST['source']]."\r\n\r\n";
		$sFile .= $sLine;
		echo $sLine;
		// zapis rekordow
		
		$sLine = $aConfig['lang'][$this->sModule]['books_list_azymut_index']."\t".
             $aConfig['lang'][$this->sModule]['books_list_ile']."\r\n";
		$sFile .= $sLine;
		echo $sLine;
		$iI=1;
		
		foreach ($aNewBooks as $aItem) {
			$iProductId = $aItem['id'];
      
			++$iI;
			
			$aBook = $this->_getBookData($iProductId);
			$sLine = $aBook['azymut_index']."\t".$aItem['quantity']."\r\n";
			//$sLine = $aBook['isbn']."\t".$aItem['quantity']."\r\n";

			$aItemList=explode(',', $aItem['items']);
			foreach($aItemList as $iOneItem)
				$aBooks2[]=$iOneItem;
				
			$sFile .= $sLine;
			echo $sLine;
			
			if ($_POST['status'] == '1') {
				if ($this->setOrderedItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
			if ($_POST['status'] == '3') {
				if ($this->setHasNCItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
		}
	// dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
		$aValues = array(
			'content' => nl2br($sFile),
			'saved_type' => '1',
			'status' => $_POST['status'],
			'source' => $_POST['source'],
			'date_send' => 'NOW()',
			'send_by' => $_SESSION['user']['name'],
			'number'=>$sListNumber
		);
		$iExportId=false;
		if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
			$bIsErr = true;
		}
		else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
			
			foreach ($aBooks2 as $iProductId=>$iItem) {
				$aValues = array(
					'send_history_id'=>$iExportId,
					'item_id'=>$iItem
					);
				if (Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false) === false) {
					$bIsErr = true;
				}
			}
		}
		
		if ($bIsErr === true) {
			// błąd
			Common::RollbackTransaction();
      $this->sMsg = _('Wystąpił błąd podczas generowania listy Azymut');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
		} else {
			// wszystko ok
			Common::CommitTransaction();
		}
	} // end of GetAzymut() funciton
	
  
  /**
   * Metoda generuje plik dla Platformy
   * 
   * @global array $aConfig
   * @param object $pSmarty
   */
	public function GetPlatforma(&$pSmarty) {
    global $aConfig;
    $aBooksToConfirm = unserialize(base64_decode(stripslashes($_POST['books_arr'])));
    if (empty($aBooksToConfirm)) {
      $this->sMsg = _('Wystąpił błąd podczas generowania pliku lub lista jest pusta.');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
    }
    
    //przerwanie po 25 pozycjach jeśli jest niepotwierdzone
    $iLimit = 9999999;
    $aNewBooks = $this->getLimitedBooks($aBooksToConfirm, $iLimit);
    
    // sprawdzamy czy nie puste i sortuejemy
    if (!empty($aNewBooks)) {
      $aNewBooks = $this->_sortArr($aNewBooks);
    }
 
		$bIsErr = false;
		$sFile = '';
		$aBooks2=array();
		$_GET['hideHeader'] = '1';
		
		try {
      $sListNumber = $this->getNewListNumber(intval($_POST['status']));
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
		
		// rozpoczęcie transakcji
		Common::BeginTransaction();
			
		// zapis naglowka
    include_once('PHPExcel/PHPExcel.php');
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->createSheet();
    $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Arkusz1'); 
    $objPHPExcel->addSheet($myWorkSheet, 0);
    $objPHPExcel->setActiveSheetIndexByName('Arkusz1');
    $objPHPExcel->getActiveSheet()
            ->setCellValue('A1', 'ISBN')
            ->setCellValue('B1', 'Ilosc')
            ->setCellValue('C1', 'Tytul')
            ->setCellValue('D1', 'Note');
    
    $iId = 1;
		foreach ($aNewBooks as $aItem) {
      
			$iProductId = $aItem['id'];
      
      $objPHPExcel->getActiveSheet()
              ->setCellValue('A'.($iId+1), $aItem['isbn'])
              ->setCellValue('B'.($iId+1), $aItem['quantity'])
              ->setCellValue('C'.($iId+1), $aItem['name'])
              ->setCellValue('D'.($iId+1), '');
      $iId++;
      
      $sFile .= $aItem['isbn'].','.$aItem['quantity'].','.$aItem['name']."\n";
      
      
			$aItemList=explode(',', $aItem['items']);
			foreach($aItemList as $iOneItem)
				$aBooks2[]=$iOneItem;
				

			if($_POST['status'] == '1'){
				if ($this->setOrderedItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
			if($_POST['status'] == '3'){
				if ($this->setHasNCItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
		}	
    
    // dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
		$aValues = array(
			'content' => $sFile,
			'saved_type' => '5',
			'status' => $_POST['status'],
			'source' => $_POST['source'],
			'date_send' => 'NOW()',
			'send_by' => $_SESSION['user']['name'],
			'number'=>$sListNumber
		);
		$iExportId=false;
		if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
			$bIsErr = true;
		}
		else {
      //jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
			foreach ($aBooks2 as $iProductId => $iItem) {
				$aValues = array(
					'send_history_id' => $iExportId,
					'item_id' => $iItem
					);
				if ( Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false) === false) {
					// błąd
					$bIsErr = true;
				}
			}
		}
		if ($bIsErr === true) {
			// błąd
			Common::RollbackTransaction();
      $this->sMsg = _('Wystąpił błąd podczas generowania listy Platforma');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
		} else {
      header('Content-type: text/xml');
      header('Content-Disposition: attachment; filename="zamowione_pozycje_platf_'.date('Ymd-His').'.xlsx"');
      header("Pragma: no-cache");
      header("Expires: 0");
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
      $objWriter->save('php://output');
      
			// wszystko ok
			Common::CommitTransaction();
		}
  }// end of GetPlatforma() method
  
  
	/**
	 * Metoda aktualizuje rabat uzytkownika o podanym Id
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function GetOlesiejuk(&$pSmarty) {
		global $aConfig;
    $aBooksToConfirm = unserialize(base64_decode(stripslashes($_POST['books_arr'])));
    if (empty($aBooksToConfirm)) {
      $this->sMsg = _('Wystąpił błąd podczas generowania pliku lub lista jest pusta.');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
    }
    
    //przerwanie po 25 pozycjach jeśli jest niepotwierdzone
    if($_POST['status'] == 3) {
      $iLimit = 25;
    }
    //przerwanie po 35 pozycjach jesli jest do zamówienia USTALONE Z Marcinem Chudym 01.09.2011
    if($_POST['status'] == 1) {
      $iLimit = 35;
    }
    $aNewBooks = $this->getLimitedBooks($aBooksToConfirm, $iLimit);
    // sprawdzamy czy nie puste i sortuejemy
    if (!empty($aNewBooks)) {
      $aNewBooks = $this->_sortArr($aNewBooks);
    }
 
		$bIsErr = false;
		$aBooks2=array();
		$_GET['hideHeader'] = '1';
		
		try {
      $sListNumber = $this->getNewListNumber(intval($_POST['status']));
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
		
		// rozpoczęcie transakcji
		Common::BeginTransaction();
    
		// zapis naglowka
		$iI=1;
    $oXml = new SimpleXMLElement('<products/>');
		foreach ($aNewBooks as $aItem) {
      
			$iProductId = $aItem['id'];
      
			//przerwanie po 25 pozycjach
			if($iI>25 && $_POST['status']==3) break;
			++$iI;
			
      $aProductAttr = $this->_getProductAttr($aItem['id'], array('ean_13', 'isbn_plain'));
      $oProduct = $oXml->addChild('product');
      $oProduct->addChild('ean', ($aProductAttr['ean_13'] != '' ? $aProductAttr['ean_13'] : ( $aProductAttr['isbn_plain'] != '' ? $aProductAttr['isbn_plain'] : '' ) ));
      $oProduct->addChild('quantity', $aItem['quantity']);
      
      
			$aItemList=explode(',', $aItem['items']);
			foreach($aItemList as $iOneItem)
				$aBooks2[]=$iOneItem;
				

			if($_POST['status'] == '1'){
				if ($this->setOrderedItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
			if($_POST['status'] == '3'){
				if ($this->setHasNCItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
		}	
    $sXML = $oXml->asXML();

    // dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
		$aValues = array(
			'content' => $sXML,
			'saved_type' => '4',
			'status' => $_POST['status'],
			'source' => $_POST['source'],
			'date_send' => 'NOW()',
			'send_by' => $_SESSION['user']['name'],
			'number' => $sListNumber
		);
		$iExportId=false;
		if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
			$bIsErr = true;
		}
		else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
			
			foreach ($aBooks2 as $iProductId=>$iItem) {
				$aValues = array(
					'send_history_id'=>$iExportId,
					'item_id'=>$iItem
					);
				if ( Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false) === false) {
					// błąd
					$bIsErr = true;
				}
			}
		}
		if ($bIsErr === true) {
			// błąd
			Common::RollbackTransaction();
      $this->sMsg = _('Wystąpił błąd podczas generowania listy Olesiejuk');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
		} else {
      header('Content-type: text/xml');
      header('Content-Disposition: attachment; filename="zamowione_pozycje_ole_'.date('Ymd-His').'.xml"');
      header("Pragma: no-cache");
      header("Expires: 0");
      echo $sXML;
			// wszystko ok
			Common::CommitTransaction();
		}
	} // end of GetOlesiejuk() funciton
  
  /**
   * Metoda pobiera atrybuty produktu
   * 
   * @param int $iPId
   * @param array $aCols
   * @return array
   */
  private function _getProductAttr($iPId, $aCols) {
    
    $sSql = 'SELECT '.implode(',', $aCols).' FROM products WHERE id = '.$iPId;
    return Common::GetRow($sSql);
  }// end of _getProductAttr() method
  
  /**
	 * Metoda aktualizuje rabat uzytkownika o podanym Id
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function GetAteneum(&$pSmarty) {
		global $aConfig;
    $aBooksToConfirm = unserialize(base64_decode(stripslashes($_POST['books_arr'])));
    if (empty($aBooksToConfirm)) {
      $this->sMsg = _('Wystąpił błąd podczas generowania pliku lub lista jest pusta.');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
    }
    
    //przerwanie po 25 pozycjach jeśli jest niepotwierdzone
    if($_POST['status'] == 3) {
      $iLimit = 25;
    }
    //przerwanie po 35 pozycjach jesli jest do zamówienia USTALONE Z Marcinem Chudym 01.09.2011
    if($_POST['status'] == 1) {
      $iLimit = 35;
    }
    
    $fLimitPrice = $this->getAteneumLimitPrice();
    $aNewBooks = $this->getLimitedBooksByPrice($aBooksToConfirm, $fLimitPrice, 'ateneum');
    if ($aNewBooks === FALSE) {
      $this->sMsg = GetMessage(_('Lista produktów nie przekroczyła minimum o wartości '.Common::formatPrice($fLimitPrice).' zł'), true);
      $this->Show($pSmarty);
      return true;
    }
    // sprawdzamy czy nie puste i sortuejemy
    if (!empty($aNewBooks)) {
      $aNewBooks = $this->_sortArr($aNewBooks);
    }
 
		$bIsErr = false;
		$aBooks2=array();
		$_GET['hideHeader'] = '1';
		
    try {
      $sListNumber = $this->getNewListNumber(intval($_POST['status']));
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
    
		// rozpoczęcie transakcji
		Common::BeginTransaction();
			
		// zapis naglowka
		$iI=1;
    $sLine = '';
    
		foreach ($aNewBooks as $aItem) {
      
			$iProductId = $aItem['id'];
      
			//przerwanie po 25 pozycjach
			if($iI>25 && $_POST['status']==3) break;
			++$iI;
			$aProductAttr = $this->_getProductAttr($aItem['id'], array('ean_13', 'isbn_plain'));
      $sLine .= ($aProductAttr['ean_13'] != '' ? $aProductAttr['ean_13'] : $aProductAttr['isbn_plain']).';'.$aItem['quantity'].";".$aItem['name']."\n";
      
			$aItemList=explode(',', $aItem['items']);
			foreach($aItemList as $iOneItem)
				$aBooks2[]=$iOneItem;
				

			if($_POST['status'] == '1'){
				if ($this->setOrderedItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
			if($_POST['status'] == '3'){
				if ($this->setHasNCItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
		}	
    
	// dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
		$aValues = array(
			'content' => nl2br($sLine),
			'saved_type' => '0',
			'status' => $_POST['status'],
			'source' => $_POST['source'],
			'date_send' => 'NOW()',
			'send_by' => $_SESSION['user']['name'],
			'number' => $sListNumber
		);
		$iExportId=false;
		if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
			$bIsErr = true;
		}
		else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
			
			foreach ($aBooks2 as $iProductId=>$iItem) {
				$aValues = array(
					'send_history_id'=>$iExportId,
					'item_id'=>$iItem
					);
				if ( Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false) === false) {
					// błąd
					$bIsErr = true;
				}
			}
		}
		if ($bIsErr === true) {
			// błąd
			Common::RollbackTransaction();
      $this->sMsg = _('Wystąpił błąd podczas generowania listy Super siódemka');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
		} else {
      header('Content-type: text/csv');
      header('Content-Disposition: attachment; filename="zamowione_poz_ateneum_'.date('YmdHis').'.txt"');
      header("Pragma: no-cache");
      header("Expires: 0");
      echo $sLine;
			// wszystko ok
			Common::CommitTransaction();
		}
	} // end of GetSuperSiodemka() funciton
  
  
  /**
	 * Metoda aktualizuje rabat uzytkownika o podanym Id
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function GetSuperSiodemka(&$pSmarty) {
		global $aConfig;
    $aBooksToConfirm = unserialize(base64_decode(stripslashes($_POST['books_arr'])));
    if (empty($aBooksToConfirm)) {
      $this->sMsg = _('Wystąpił błąd podczas generowania pliku lub lista jest pusta.');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
    }
    
    //przerwanie po 25 pozycjach jeśli jest niepotwierdzone
    if($_POST['status'] == 3) {
      $iLimit = 25;
    }
    //przerwanie po 35 pozycjach jesli jest do zamówienia USTALONE Z Marcinem Chudym 01.09.2011
    if($_POST['status'] == 1) {
      $iLimit = 35;
    }
    
    $fLimitPrice = $this->getSuperSiodemkaLimitPrice();
    $aNewBooks = $this->getLimitedBooksByPrice($aBooksToConfirm, $fLimitPrice, 'siodemka');
    if ($aNewBooks === FALSE) {
      $this->sMsg = GetMessage(_('Lista produktów nie przekroczyła minimum o wartości '.Common::formatPrice($fLimitPrice).' zł'), true);
      $this->Show($pSmarty);
      return true;
    }
    // sprawdzamy czy nie puste i sortuejemy
    if (!empty($aNewBooks)) {
      $aNewBooks = $this->_sortArr($aNewBooks);
    }
 
		$bIsErr = false;
		$aBooks2=array();
		
    try {
      $sListNumber = $this->getNewListNumber(intval($_POST['status']));
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
    
		// rozpoczęcie transakcji
		Common::BeginTransaction();
			
		// zapis naglowka
		$iI=1;
    $sLine = '';
    
		foreach ($aNewBooks as $aItem) {
			$iProductId = $aItem['id'];
      
			//przerwanie po 25 pozycjach
			if($iI>25 && $_POST['status']==3) break;
			++$iI;
      
      // tutaj indeks powinien być zapisywany w momencie składania zamówienia, niestety nie jest
      $sSiodemkaIndex = $this->_getSuperSiodemkaMainIndex($iProductId);
      if ($sSiodemkaIndex != '') {
        $sLine .= $sSiodemkaIndex.','.$aItem['quantity']."\n";

        $aItemList=explode(',', $aItem['items']);
        foreach($aItemList as $iOneItem)
          $aBooks2[]=$iOneItem;


        if($_POST['status'] == '1'){
          if ($this->setOrderedItems($aItem['items']) === false) {
            $bIsErr = true;
            break;
          }
        }
        if($_POST['status'] == '3'){
          if ($this->setHasNCItems($aItem['items']) === false) {
            $bIsErr = true;
            break;
          }
        }
      } else {
        $bIsErr = -1;
        break;
      }
		}	
    
	// dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
		$aValues = array(
			'content' => nl2br($sLine),
			'saved_type' => '0',
			'status' => $_POST['status'],
			'source' => $_POST['source'],
			'date_send' => 'NOW()',
			'send_by' => $_SESSION['user']['name'],
			'number' => $sListNumber
		);
		$iExportId=false;
		if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
			$bIsErr = true;
		}
		else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
			
			foreach ($aBooks2 as $iProductId=>$iItem) {
				$aValues = array(
					'send_history_id'=>$iExportId,
					'item_id'=>$iItem
					);
				if ( Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false) === false) {
					// błąd
					$bIsErr = true;
				}
			}
		}
		if ($bIsErr === true) {
			// błąd
			Common::RollbackTransaction();
      $this->sMsg = _('Wystąpił błąd podczas generowania listy Super siódemka');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
		} 
    elseif ($bIsErr === -1) {
			// błąd
			Common::RollbackTransaction();
      $this->sMsg = _('Towar o isbn '.$aItem['isbn'].' nie posiada głównego indeksu źródła');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
    } else {
      $_GET['hideHeader'] = '1';
      header('Content-type: text/csv');
      header('Content-Disposition: attachment; filename="zamowione_poz_super_s_'.date('YmdHis').'.txt"');
      header("Pragma: no-cache");
      header("Expires: 0");
      echo $sLine;
			// wszystko ok
			Common::CommitTransaction();
		}
	} // end of GetSuperSiodemka() funciton
  
  
  /**
   * Metoda pobiera głowny indeks produktu super siodemki
   * 
   * @param type $iProductId
   * @return type
   */
  private function _getSuperSiodemkaMainIndex($iProductId) {
    
    $sSql = "SELECT source_index 
             FROM products_to_sources AS PTS
             JOIN sources AS S
              ON S.id = PTS.source_id AND S.symbol = 'siodemka'
             WHERE 
              PTS.product_id = ".$iProductId." AND 
              PTS.main_index = '1'";
    return Common::GetOne($sSql);
  }// end of _getSuperSiodemkaMainIndex() method
  
  
	/**
	 * Metoda generuje PDF'a zewnętrznego
	 *
	 * @author Arkadiusz Golba
	 * @version 1.0
	 * @global type $aConfig
	 * @return type 
	 */
	private function GetPDF_Ext(&$pSmarty) {
	global $aConfig;
	
		$bIsErr = false;
		$_GET['hideHeader'] = '1';
		$aBooks = array();
		$aBooks2 = array();
    
    $aBooksToConfirm = unserialize(base64_decode(stripslashes($_POST['books_arr'])));
    if (empty($aBooksToConfirm)) {
      $this->sMsg = _('Wystąpił błąd podczas generowania pliku lub lista jest pusta.');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
    }
    
    //przerwanie po 25 pozycjach jeśli jest niepotwierdzone
    if($_POST['status'] == 3) {
      $iLimit = 25;
    }
    //przerwanie po 35 pozycjach jesli jest do zamówienia USTALONE Z Marcinem Chudym 01.09.2011
    if($_POST['status'] == 1) {
      $iLimit = 35;
    }
    $aNewBooks = $this->getLimitedBooks($aBooksToConfirm, $iLimit);
    
    // sprawdzamy czy nie puste i sortuejemy
    if (!empty($aNewBooks)) {
      $aNewBooks = $this->_sortArr($aNewBooks);
    }
		
		// rozpoczęcie transakcji
		Common::BeginTransaction();
		
		$sNow = $_POST['sNow'];
		$sSql = "SELECT id 
						 FROM ".$aConfig['tabls']['prefix']."orders_send_history 
						 WHERE date_send='".$sNow."' AND 
									 status ='".$_POST['status']."' AND 
									 send_by='".$_SESSION['user']['name']."' AND 
									 source = '".$_POST['source']."'";
		$iSent = Common::GetOne($sSql);

		if($iSent){
			// przekierowanie do historii - inaczej nie chce dzialac
			header('Location: ?frame=main&module_id=39&module=m_zamowienia&action=send_history');
			//doRedirect($aConfig['base_url'].'?frame=main&module_id=39&module=m_zamowienia&action=send_history');
			die;
		}
		
		$sSql = "SELECT name, surname
							FROM ".$aConfig['tabls']['prefix']."users
							 WHERE id = ".intval($_SESSION['user']['id']);
		$aGeneratorName = Common::GetRow($sSql);
		
//		$_POST['books'] = $this->sortBooksByName($_POST['books']);
		
		$i=1;
		$iSuma=0;
		foreach ($aNewBooks as $sKey => $aItem) {
      $iProductId = $aItem['id'];
      
			$aBooks['items'][$sKey] = $this->_getBookData($iProductId);
			$aBooks['items'][$sKey]['authors']=$this->getAuthors($iProductId);
			
			$aBooks['items'][$sKey]['quantity'] = 	$aItem['quantity'];
			$aBooks['items'][$sKey]['location'] = 	$aItem['location']==''													? '&nbsp;' : $aItem['location'];
			$aBooks['items'][$sKey]['publisher'] = 	$aBooks['items'][$sKey]['publisher']==''	? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$sKey]['publisher']),0,35,'UTF-8');
			$aBooks['items'][$sKey]['authors'] = 		$aBooks['items'][$sKey]['authors']==''		? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$sKey]['authors']),0,50,'UTF-8');
			$aBooks['items'][$sKey]['name'] = 			$aBooks['items'][$sKey]['name']==''				? '&nbsp;' : mb_substr(strip_tags($aBooks['items'][$sKey]['name']),0,80,'UTF-8');
			//$aBooks['items'][$sKey]['authors'] = 		$this->getAuthors($iProductId);
			$aBooks['items'][$sKey]['lp'] =$i++;
			$iSuma+=$aItem['quantity'];
			
			$aItemList=explode(',', $aItem['items']);
			foreach($aItemList as $iOneItem)
				$aBooks2[]=$iOneItem;
			
			if($_POST['status'] == '1'){
				if ($this->setOrderedItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
			if($_POST['status'] == '3'){
				if ($this->setHasNCItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
			//przerwanie po 25 pozycjach
			if($i>25 && $_POST['status']==3) break;
		}
		
		if ($bIsErr === false) {
			Common::CommitTransaction();
		} else {
			// błąd
			Common::RollbackTransaction();
      $this->sMsg = _('Wystąpił błąd podczas generowania listy Zewnętrzny PDF');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
		}
    try {
      $sListNumber = $this->getNewListNumber(intval($_POST['status']));
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }

		// rozpoczęcie transakcji
		Common::BeginTransaction();
		
		$sSourceName = in_array($_POST['source'], array(0,1,5,51,52)) ? ($aConfig['lang'][$this->sModule]['source_'.intval($_POST['source'])]) : Common::GetOne('SELECT name FROM `'.$aConfig['tabls']['prefix'].'external_providers` WHERE id='.$_POST['source']);
		$aBooks['orders_header'] = sprintf($aConfig['lang'][$this->sModule]['orders_header_'.$_POST['status']], $sSourceName, $sListNumber);
		$aBooks['orders_header2'] = $aConfig['lang'][$this->sModule]['orders_genby'].': <strong>'.$aGeneratorName['name'].' '.$aGeneratorName['surname'].' - '.$sNow.'</strong>';
		$aBooks['orders_foot1'] = $aConfig['lang'][$this->sModule]['orders_footer_1'];
		$aBooks['orders_foot2'] = $aConfig['lang'][$this->sModule]['orders_footer_2'];
		$aBooks['orders_foot3'] = $aConfig['lang'][$this->sModule]['orders_footer_3'];
		$aBooks['Qsum'] = $iSuma;
		
		$pSmarty->assign_by_ref('aLang', $aConfig['lang'][$this->sModule]);
		$pSmarty->assign_by_ref('aBooks', $aBooks);
		//if($_POST['status'] == 1)
		//	$sHtml = $pSmarty->fetch('PDF/pdf_orders2_zewn.tpl');
		//else
		//$sHtml = $pSmarty->fetch('PDF/pdf_orders_zewn.tpl');
		$sHtml = $pSmarty->fetch('PDF/pdf_orders2_zewn.tpl');
		
		$pSmarty->clear_assign('aBooks');
		
		require_once('OLD_tcpdf/config/lang/pl.php');
		require_once('OLD_tcpdf/tcpdf.php');		

		// create new PDF document
		$pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('profit24');
		$pdf->SetTitle($aBooks['orders_header']);
		$pdf->SetSubject($aBooks['orders_header']);
		$pdf->SetKeywords('');
		$pdf->setPrintHeader(false);

		$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
		//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 10);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 

		//set some language-dependent strings
		$pdf->setLanguageArray($l); 

		// set font
		$pdf->SetFont('freesans', '', 8);


		// add a page
		$pdf->AddPage();
		$pdf->writeHTML($sHtml, true, false, false, false, '');

		$pdf->Output('lista_zewnetrzna.pdf', 'I');

		// wyjatek - już wygenerowaliśmy dokładnie takiego samego pdf'a
		if(!$iSent){	
		// dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
			$aValues = array(
				'content' => $sHtml,
				'saved_type' => '3',
				'status' => $_POST['status'],
				'source' => $_POST['source'],
				'date_send' => $sNow,
				'send_by' => $_SESSION['user']['name'],
				'number' => $sListNumber
			);
			$iExportId = false;
			if ( ($iExportId = Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
				$bIsErr = true;
			}
			else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego

				foreach ($aBooks2 as $iProductId=>$iItem) {
					$aValues = array(
						'send_history_id'=>$iExportId,
						'item_id'=>$iItem
						);
					if ( (Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false)) === false) {
							$bIsErr = true;
						}
					}
			}
		}
		
		if ($bIsErr === true) {
			// błąd
			Common::RollbackTransaction();
      $this->sMsg = _('Wystąpił błąd podczas generowania listy Zewnętrzny PDF');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
		} else {
			// wszystko ok
			Common::CommitTransaction();
		}
	} // end of GetPDF_Ext() method
	
  
	/**
	 * Generuje liste pozycji w formacie pdf do pobrania
	 * @return unknown_type
	 */
	function GetPDF(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		$_GET['hideHeader'] = '1';
		$aBooks = array();
		$aBooks2 = array();
		
    $aBooksToConfirm = unserialize(base64_decode(stripslashes($_POST['books_arr'])));
    if (empty($aBooksToConfirm)) {
      $this->sMsg = _('Wystąpił błąd podczas generowania pliku lub lista jest pusta.');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
    }
    
    //przerwanie po 25 pozycjach jeśli jest niepotwierdzone
    if($_POST['status'] == 3) {
		if ($_POST['source'] == '0') {
			$iLimit = 15;
		} else {
			$iLimit = 25;
		}
    }
    //przerwanie po 35 pozycjach jesli jest do zamówienia USTALONE Z Marcinem Chudym 01.09.2011
    if($_POST['status'] == 1) {
      $iLimit = 35;
    }
    $aNewBooks = $this->getLimitedBooks($aBooksToConfirm, $iLimit);
    
    // sprawdzamy czy nie puste i sortuejemy
    if (!empty($aNewBooks)) {
      $aNewBooks = $this->_sortArr($aNewBooks);
    }
    $aMrgNewBooks = $this->_groupByProductFunctionList($aNewBooks, 'azymut');

		// rozpoczęcie transakcji
		Common::BeginTransaction();
		
		$sNow=$_POST['sNow'];
		$sSql='SELECT id FROM '.$aConfig['tabls']['prefix'].'orders_send_history WHERE `date_send`=\''.$sNow.'\' AND `status`=\''.$_POST['status'].'\' AND `send_by`=\''.$_SESSION['user']['name'].'\' AND `source`=\''.$_POST['source'].'\'';
		$iSent=Common::GetOne($sSql);
		
			if($iSent){
				//przekierowanie do historii - inaczej nie chce dzialac
				header('Location: ?frame=main&module_id=39&module=m_zamowienia&action=send_history');
				die;
			}
		
		$sSql = "SELECT name, surname
							FROM ".$aConfig['tabls']['prefix']."users
							 WHERE id = ".intval($_SESSION['user']['id']);
							 			 
		$aGeneratorName=Common::GetRow($sSql);

//		$_POST['books'] = $this->sortBooksByName($_POST['books']);
		
		$i=1;
		$iSuma=0;
		foreach ($aMrgNewBooks as $sKey => $aItem) {
      $iProductId = $aItem['id'];
      
			$aBooks['items'][$sKey] = $this->_getBookData($iProductId);
			$aBooks['items'][$sKey]['authors']=$this->getAuthors($iProductId);
			
			$aBooks['items'][$sKey]['quantity'] = 	$aItem['quantity'];
			$aBooks['items'][$sKey]['location'] = 	$aItem['location']==''													?'&nbsp;':$aItem['location'];
			$aBooks['items'][$sKey]['publisher'] = 	$aBooks['items'][$sKey]['publisher']==''	?'&nbsp;':mb_substr(strip_tags($aBooks['items'][$sKey]['publisher']),0,35,'UTF-8');
			$aBooks['items'][$sKey]['authors'] = 		$aBooks['items'][$sKey]['authors']==''		?'&nbsp;':mb_substr(strip_tags($aBooks['items'][$sKey]['authors']),0,50,'UTF-8');
			$aBooks['items'][$sKey]['name'] = 			$aBooks['items'][$sKey]['name']==''				?'&nbsp;':mb_substr(strip_tags($aBooks['items'][$sKey]['name']),0,80,'UTF-8');
			//$aBooks['items'][$sKey]['authors'] = 		$this->getAuthors($iProductId);
			$aBooks['items'][$sKey]['lp'] =$i++;
			$iSuma+=$aItem['quantity'];
			
			$aItemList=explode(',', $aItem['items']);
			foreach($aItemList as $iOneItem)
				$aBooks2[]=$iOneItem;
			
			if($_POST['status'] == '1'){
				if ($this->setOrderedItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
			if($_POST['status'] == '3'){
				if ($this->setHasNCItems($aItem['items']) === false) {
					$bIsErr = true;
				}
			}
			//przerwanie po 25 pozycjach
			if($i>25 && $_POST['status']==3) break;
		}
		if ($bIsErr === false) {
			Common::CommitTransaction();
		} else {
			// błąd
			Common::RollbackTransaction();
      $this->sMsg = _('Wystąpił błąd podczas generowania listy PDF');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
		}
    try {
      $sListNumber = $this->getNewListNumber(intval($_POST['status']));
    } catch (Exception $exc) {
      echo $exc->getTraceAsString();
    }
		Common::BeginTransaction();

		$sSourceName=in_array($_POST['source'], array(0,1,5,51,52))?($aConfig['lang'][$this->sModule]['source_'.intval($_POST['source'])]):Common::GetOne('SELECT name FROM `'.$aConfig['tabls']['prefix'].'external_providers` WHERE id='.$_POST['source']);
		$aBooks['orders_header'] = sprintf($aConfig['lang'][$this->sModule]['orders_header_'.$_POST['status']], $sSourceName, $sListNumber);
		$aBooks['orders_header2']=$aConfig['lang'][$this->sModule]['orders_genby'].': <strong>'.$aGeneratorName['name'].' '.$aGeneratorName['surname'].' - '.$sNow.'</strong>';
		$aBooks['orders_foot1']=$aConfig['lang'][$this->sModule]['orders_footer_1'];
		$aBooks['orders_foot2']=$aConfig['lang'][$this->sModule]['orders_footer_2'];
		$aBooks['orders_foot3']=$aConfig['lang'][$this->sModule]['orders_footer_3'];
		$aBooks['Qsum']=$iSuma;
		$pSmarty->assign_by_ref('aLang', $aConfig['lang'][$this->sModule]);
		$pSmarty->assign_by_ref('aBooks', $aBooks);
		if($_POST['status']==1)
			$sHtml = $pSmarty->fetch('PDF/pdf_orders2.tpl');
		else
			$sHtml = $pSmarty->fetch('PDF/pdf_orders.tpl');
		$pSmarty->clear_assign('aBooks');
		
      require_once('OLD_tcpdf/config/lang/pl.php');
			require_once('OLD_tcpdf/tcpdf.php');		
			
			// create new PDF document
			$pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('profit24');
			$pdf->SetTitle($aBooks['orders_header']);
			$pdf->SetSubject($aBooks['orders_header']);
			$pdf->SetKeywords('');
			$pdf->setPrintHeader(false);
			
			$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));
			
			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			
			//set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
			//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			
			//set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, 10);
			
			//set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
			
			//set some language-dependent strings
			$pdf->setLanguageArray($l); 
			
			// set font
			$pdf->SetFont('freesans', '', 8);
			
			
			// add a page
			$pdf->AddPage();
			$pdf->writeHTML($sHtml, true, false, false, false, '');
			
			
			//wyjatek - już wygenerowaliśmy dokładnie takiego samego pdf'a
			if(!$iSent){	
			// dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
				$aValues = array(
					'content' => $sHtml,
					'saved_type' => '3',
					'status' => $_POST['status'],
					'source' => $_POST['source'],
					'date_send' => $sNow,
					'send_by' => $_SESSION['user']['name'],
					'number'=>$sListNumber
				);
				$iExportId=false;
				if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
					$bIsErr = true;
				}
				else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
					foreach ($aBooks2 as $iProductId=>$iItem) {
						$aValues = array(
							'send_history_id'=>$iExportId,
							'item_id'=>$iItem
							);
						if ( (Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false)) === false) {
								$bIsErr = true;
							}
						}
      
				}
		}
		if ($bIsErr === true) {
			// błąd
			Common::RollbackTransaction();
      $this->sMsg = _('Wystąpił błąd podczas generowania listy PDF');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
		} else {
			// wszystko ok
			Common::CommitTransaction();
		}
		
		$pdf->Output('lista.pdf', 'I');
	} // end of GetPDF() funciton
	
	
	/**
	 * Metoda generuje formularz do wysłania emaila
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function GetEmailForm(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		$aData = array();
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = $aConfig['lang'][$this->sModule]['send_ordered_items_header'];
		$pForm = new FormTable('ordered_itm', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		
		// sprawdzenie czy jakies ksiazki zostały wybrane
		if (!empty($_POST['books'])) {
			foreach ($_POST['books'] as $iProductId=>$aItem) {
				$pForm->AddHidden('books['.$iProductId.'][quantity]', $aItem['quantity']);
				$pForm->AddHidden('books['.$iProductId.'][items]', $aItem['items']);
			}	
			//$sSource,$sDateFrom,$sDateTo
			if (!empty($_POST)) {
				$aData =& $_POST;
			}
			
			$pForm->AddCheckBox('define_section', $aConfig['lang'][$this->sModule]['define_section'], array('onchange'=>'ChangeObjValue(\'do\', \'get_mail\'); this.form.submit();'), '', !empty($aData['define_section']), false);
			if(!empty($aData['define_section'])){
				$pForm->AddText('email', $aConfig['lang'][$this->sModule]['define_email'], '', array(), '', 'email', false);
				$pForm->AddText('name', $aConfig['lang'][$this->sModule]['define_name'], '', array(), '', 'text', false);
				$pForm->AddText('surname', $aConfig['lang'][$this->sModule]['define_surname'], '', array(), '', 'text', false);
			} else{
				$pForm->AddSelect('address_book', $aConfig['lang'][$this->sModule]['address_book'], array(), $this->getEmails(), $aData['email'], '', false);
			}
			$pForm->AddHidden('do', 'send_email');
			$pForm->AddHidden('source', $_POST['source']);
			$pForm->AddHidden('status', $_POST['status']);
			$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['send']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('action'=>'ordered_items')).'\');'), 'button'));
			
		} else{
		//	$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_items']);
			$pForm->AddRow('&nbsp;', $aConfig['lang'][$this->sModule]['no_items'], 'info');
			$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('action'=>'ordered_items')).'\');'), 'button'));
		}
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of GetEmailForm() funciton
	
	
	/**
	 * Metoda aktualizuje rabat uzytkownika o podanym Id
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function SendEmail(&$pSmarty){
		global $aConfig;
		$bIsErr = false;
		$sErrStr='';
		$aBooks2=array();
		
    $aBooksToConfirm = unserialize(base64_decode(stripslashes($_POST['books_arr'])));
    if (empty($aBooksToConfirm)) {
      $this->sMsg = _('Wystąpił błąd podczas generowania pliku lub lista jest pusta.');
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg, true);
      $this->Show($pSmarty);
      return false;
    }
    
    //przerwanie po 25 pozycjach jeśli jest niepotwierdzone
    if($_POST['status'] == 3) {
      $iLimit = 25;
    }
    //przerwanie po 35 pozycjach jesli jest do zamówienia USTALONE Z Marcinem Chudym 01.09.2011
    if($_POST['status'] == 1) {
      $iLimit = 35;
    }
    $aNewBooks = $this->getLimitedBooks($aBooksToConfirm, $iLimit);
    
    // sprawdzamy czy nie puste i sortuejemy
    if (!empty($aNewBooks)) {
      $aNewBooks = $this->_sortArr($aNewBooks);
    }
    
		Common::BeginTransaction();
		
		$aEmail = $this->getEmails($_POST['address_book'], $_POST['email']);
		// pobranie emaila
		$sEmail = $aEmail[0]['email']; 
		if ($_POST['define_section'] == '1' && empty($aEmail) ) {
			$aValue['email'] = $_POST['email'];
			$aValue['name'] = $_POST['name'];
			$aValue['surname'] = $_POST['surname'];
			// dodanie adresu do książki adresowej
			if (Common::Insert($aConfig['tabls']['prefix']."orders_address_book", $aValue, '', false) === false) {
				$bIsErr = true;
			}
			$sEmail = $_POST['email'];
		} elseif($_POST['define_section'] == '1' && !empty($aEmail)){
			// jeśli dublujemy adres email error
			$sErrStr .= sprintf($aConfig['lang'][$this->sModule]['duplicate_err'], $sEmail);
			$bIsErr = true;
		}
		// jeśli wcześniej nie wystąpił błąd
		if (!$bIsErr) {
			$i=0;
			$iI=1;
			$iSuma=0;
      $aBooks = array();
			foreach ($aNewBooks as $sKey => $aItem) {
				$iProductId = $aItem['id'];
        
				//przerwanie po 25 pozycjach
				if($iI>25 && $_POST['status']==3) break;
				++$iI;
				
				$aBooks['items'][$sKey] = $this->_getBookData($iProductId);
				$aBooks['items'][$sKey]['quantity'] = $aItem['quantity'];
				$aBooks['items'][$sKey]['link'] = $aConfig['common']['client_base_url_http_no_slash'].createProductLink($iProductId, $aBooks['items'][$sKey]['name']);
				$aBooks['items'][$sKey]['lp'] = $i++;
				$iSuma+=$aItem['quantity'];
			
				$aItemList=explode(',', $aItem['items']);
				foreach($aItemList as $iOneItem)
					$aBooks2[]=$iOneItem;
					
				if($_POST['status'] == '1'){
					if ($this->setOrderedItems($aItem['items']) === false) {
						$bIsErr = true;
					}
				}
				if($_POST['status'] == '3'){
					if ($this->setHasNCItems($aItem['items']) === false) {
						$bIsErr = true;
					}
				}
			}	
			if ($bIsErr === false) {
				Common::CommitTransaction();
			} else {
				Common::RollbackTransaction();
				return false;
			}
      try {
        $sListNumber = $this->getNewListNumber(intval($_POST['status']));
      } catch (Exception $exc) {
        echo $exc->getTraceAsString();
      }
			Common::BeginTransaction();
			
			$aBooks['orders_header'] = sprintf($aConfig['lang'][$this->sModule]['orders_header_'.$_POST['status']], $aConfig['lang']['m_zamowienia_ordered_items']['source_'.$_POST['source']],$sListNumber);
			$aBooks['Qsum'] = $iSuma;
			$pSmarty->assign_by_ref('aLang', $aConfig['lang'][$this->sModule]);
			$pSmarty->assign_by_ref('aBooks', $aBooks);
			$sBody = $pSmarty->fetch('email_orders.tpl');
			$pSmarty->clear_assign('aBooks');
			if (!$bIsErr){
				// dodaj rekord do history wysłanych zamówionych pozycji orders_send_history
				$aValues = array(
					'email' => $sEmail,
					'content' => addslashes($sBody),
					'saved_type' => '2',
					'status' => $_POST['status'],
					'source' => $_POST['source'],
					'date_send' => 'NOW()',
					'send_by' => $_SESSION['user']['name'],
					'number'=>$sListNumber
				);
				$iExportId=false;
				if ( ($iExportId=Common::Insert($aConfig['tabls']['prefix']."orders_send_history", $aValues, '', true)) === false) {
					$bIsErr = true;
				}
				else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
			
					foreach ($aBooks2 as $iProductId=>$iItem) {
						$aValues = array(
							'send_history_id'=>$iExportId,
							'item_id'=>$iItem
						);
						if ( Common::Insert($aConfig['tabls']['prefix']."orders_send_history_items", $aValues, '', false) === false) {
							$bIsErr = true;
						}
					}
				}
			}
			if (Common::sendTxtHtmlMail($aConfig['default']['website_name'], $aConfig['default']['website_email'], $sEmail, changeCharcodes($aConfig['lang'][$this->sModule]['orders'], 'utf8', 'no'), '', $sBody,'') === false) {
				// błąd wysyłania emaila
				$sErrStr .= sprintf($aConfig['lang'][$this->sModule]['send_err'], $sEmail);
				$bIsErr = true;
			}
		}
		if (!$bIsErr) {
			// zatwierdzenie tranzakcji
			Common::CommitTransaction();
			// wysłano
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['send_ok'], $sEmail);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
		}
		else {
			// wycofanie tranzakcji
			Common::RollbackTransaction();
			// blad
			$sMsg = $sErrStr;
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
		}
		$this->Show($pSmarty, true);
	}// end of SendEmail() method
	
	
	/**
	 * Metoda tworzy formularz edycji rabatu grupowego
	 * dla uzytkownikow, ktorzy spelniaja okreslone zalozenia:
	 * przedzial wartosci zlozoych dotychczas zamowien, wartosc aktualnego
	 * rabatu
	 *
	 * @param	object	$pSmarty
	 * @param	bool	$bSearch	- true: wyswietl uzytkownikow
	 */
	function Show(&$pSmarty, $bSearch=false) {
		global $aConfig;

		if ($bSearch && ($_POST['source'] != '' || $_POST['status'] != '')) {
			// wyszukiwanie ksiazek spelniajacych zadane kryteria
			$aBooks =& $this->getBooks($_POST['source'], $_POST['status'], true);
		}
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = $aConfig['lang'][$this->sModule]['ordered_items_header'].($_POST['source'] != ''?' - '.$aConfig['lang'][$this->sModule]['source_'.$_POST['source']]:'');
	
		$pForm = new FormTable('ordered_itm', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'get_csv');
    $pForm->setTokenSession($_SESSION);
    
//    $_SESSION['token'] = md5(session_id(). time());
//    $pForm->AddHidden('token', $_SESSION['token']);
    
		$pForm->AddHidden('source', $_POST['source']);
		$pForm->AddHidden('status', empty($_POST['status'])?3:$_POST['status']);
		if ($bSearch && $_POST['status'] != '') {
			$pForm->AddHidden('status', $_POST['status']);
		}
		
		// typ do pobrania
		$aStatus = array(
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1']),
			array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['status_3'])
		);
		$sSql = "SELECT id, icon, name
							 FROM ".$aConfig['tabls']['prefix']."external_providers
							 WHERE 1 AND hidden_in_ordered_positions = 0 ORDER BY name ASC";
		$aProviders =& Common::GetAll($sSql);
		
		$iCnt=0;
		$sExternalProvidersHtml='';
		foreach ($aProviders as $iKey =>$aProvider) {
			++$iCnt;
			$sExternalProvidersHtml .= '<td style="">
        <a href="javascript:void(0);" 
        onclick="document.getElementById(\'status\').value=\'1\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\''.$aProvider['id'].'\'; document.getElementById(\'do\').form.submit();" 
          class="abe_ico" 
          '.($aProvider['icon']!='' ? 'style="background-image:url(\''.$aProvider['icon'].'\');"' : ' style="background-image:none;" ').
              '><span>'.$aProvider['name'].'</span></a>'.($this->countBooks($aProvider['id'], 1)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>';
			if($iCnt>6) {$iCnt=0; $sExternalProvidersHtml.='</tr><tr>';}
		}
		
		$pForm->AddRow('&nbsp;', '<div><table border="1"><tr style="background: #d9d5b9;">
										<td><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'3\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'0\'; document.getElementById(\'do\').form.submit();" class="profitj_ico"><span>Profit J</span></a>'.($this->countBooks(0, 3)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										<td style=""><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'3\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'1\'; document.getElementById(\'do\').form.submit();" class="profitn_ico"><span>Profit E</span></a>'.($this->countBooks(1, 3)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										<td style=""><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'3\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'5\'; document.getElementById(\'do\').form.submit();" class="profitn_ico"><span>Profit M</span></a>'.($this->countBooks(5, 3)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										<td style=""><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'3\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'51\'; document.getElementById(\'do\').form.submit();" class="profitn_ico"><span>Stock</span></a>'.($this->countBooks(51, 3)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										<td style=""><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'3\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'52\'; document.getElementById(\'do\').form.submit();" class="profitn_ico"><span>Profit X</span></a>'.($this->countBooks(52, 3)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										<td style=""><a href="javascript:void(0);" onclick="document.getElementById(\'status\').value=\'-1\'; document.getElementById(\'do\').value=\'show_items\'; document.getElementById(\'source\').value=\'\'; document.getElementById(\'do\').form.submit();" class="profitn_ico"><span>Zapowiedzi</span></a>'.($this->countBooks(NULL, -1)>0?'<img style="border:0px; height:20px;" src="gfx/msg_1.gif" alt="NOWE">':'').'</td>
										</tr><tr>'.$sExternalProvidersHtml
										.'</tr></table></div>');
		
		// przycisk wyszukaj
		if(!empty($aBooks)){
			$pForm->AddHidden('sNow', date('Y-m-d H:i:s'));
			$iLp=1;
			$iSumaEgz=0;
      $pForm->AddHidden('books_arr', base64_encode(serialize($aBooks)));
      $fSum = $this->_sumBooksPriceNetto($aBooks);
			foreach($aBooks as $iKey=>$aBook){
				$pForm->AddHidden('books['.$aBook['id'].'][quantity]', $aBook['quantity']);
				$pForm->AddHidden('books['.$aBook['id'].'][items]', implode(',',$aBook['items']));
				$pForm->AddHidden('books['.$aBook['id'].'][location]', $aBook['location']);
				unset($aBooks[$iKey]['items']);
        unset($aBooks[$iKey]['price_netto']);
				$aBooks[$iKey]['id']=$iLp++;
				$iSumaEgz+=$aBook['quantity'];
				unset($aBooks[$iKey]['order_id']);
			}
      
			$aBooks[]=array('','','','','', '', '<strong>'.$aConfig['lang'][$this->sModule]['sum'].'</strong>','<strong>'.$iSumaEgz.'</strong>');
			if ($_POST['status'] != '-1' && $_POST['source'] != '51') {
				$pForm->AddRow('&nbsp;', '<div style="margin-left: 115px;">'.
                    $pForm->GetInputButtonHTML('get_ateneum', $aConfig['lang'][$this->sModule]['button_get_ateneum'], array('onclick'=>'document.getElementById(\'do\').value=\'get_ateneum\'; this.form.submit();', 'style' => 'margin-bottom: 10px; '), 'button').'&nbsp;&nbsp;'.
										$pForm->GetInputButtonHTML('get_csv', $aConfig['lang'][$this->sModule]['button_get_csv'], array('onclick'=>'document.getElementById(\'do\').value=\'get_csv\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'&nbsp;&nbsp;'.
										$pForm->GetInputButtonHTML('get_azymut', $aConfig['lang'][$this->sModule]['button_get_azymut'], array('onclick'=>'document.getElementById(\'do\').value=\'get_azymut\'; this.form.submit();', 'style' => 'margin-bottom: 10px; '), 'button').'&nbsp;&nbsp;'.
                    $pForm->GetInputButtonHTML('get_super_siodemka', 'N '.$aConfig['lang'][$this->sModule]['button_get_super_siodemka'].' (kod hurtowni)', array('onclick'=>'document.getElementById(\'do\').value=\'get_super_siodemka\'; this.form.submit();', 'style' => 'color: red; font-weight: bold; margin-bottom: 10px; '), 'button').'&nbsp;&nbsp;'.
                    $pForm->GetInputButtonHTML('get_olesiejuk', $aConfig['lang'][$this->sModule]['button_get_olesiejuk'], array('onclick'=>'document.getElementById(\'do\').value=\'get_olesiejuk\'; this.form.submit();', 'style' => 'margin-bottom: 10px; '), 'button').'&nbsp;&nbsp;'.
                    $pForm->GetInputButtonHTML('get_platforma', $aConfig['lang'][$this->sModule]['button_get_platforma'], array('onclick'=>'document.getElementById(\'do\').value=\'get_platforma\'; this.form.submit();', 'style' => 'margin-bottom: 10px; '), 'button').'&nbsp;&nbsp;'.
										$pForm->GetInputButtonHTML('get_mail', $aConfig['lang'][$this->sModule]['button_get_mail'], array('onclick'=>'document.getElementById(\'do\').value=\'get_mail\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'&nbsp;&nbsp;'.
										$pForm->GetInputButtonHTML('get_pdf', $aConfig['lang'][$this->sModule]['button_get_pdf'], array('onclick'=>'$(\'#ordered_itm\').attr(\'target\', \'_blank\'); document.getElementById(\'do\').value=\'get_pdf\'; this.form.submit(); $(\'#ordered_itm\').removeAttr(\'target\');', 'style' => 'margin-bottom: 10px;'), 'button').'&nbsp;&nbsp;'.
										$pForm->GetInputButtonHTML('get_pdf_ext', $aConfig['lang'][$this->sModule]['button_get_pdf_ext'], array('onclick'=>'$(\'#ordered_itm\').attr(\'target\', \'_blank\'); document.getElementById(\'do\').value=\'get_pdf_ext\'; this.form.submit(); $(\'#ordered_itm\').removeAttr(\'target\');', 'style' => 'margin-bottom: 10px;'), 'button').'&nbsp;&nbsp;'.'</div>');
			}
		}
    
    $sSumStr = '';
    if (($_POST['source'] == '7' || $_POST['source'] == '26' || $_POST['source'] == '31' || $_POST['source'] == '41' || $_POST['source'] == '114') && $_POST['status'] == '1') {
      $sSource = NULL;
      // jeśli azymut lub super siodemka, to sumuj pozycje
      if ($_POST['source'] == '7') {
        $fLimitPrice = $this->getAzymutLimitPrice();
        $sSource = 'azymut';
      } 
      elseif ($_POST['source'] == '26') {
        $fLimitPrice = $this->getSuperSiodemkaLimitPrice();
        $sSource = 'siodemka';
      }
      elseif ($_POST['source'] == '31') {
        $fLimitPrice = $this->getAteneumLimitPrice();
        $sSource = 'ateneum';
      }
      elseif ($_POST['source'] == '41') {
        $fLimitPrice = $this->getPlatonLimitPrice();
        $sSource = 'platon';
      }
	  elseif ($_POST['source'] == '114') {
		  $fLimitPrice = $this->getPandaLimitPrice();
		  $sSource = 'panda';
	  }
      $fSum = $this->_sumBooksPrice($aBooks, $sSource);
      $fLists = ($fSum/$fLimitPrice);
      if ($fLists < 1.00) {
        $sSumStr = sprintf('<b>Wartość zakupu pozycji w '.$sSource.': %s zł. <span style="color: red">Nie osiągnięto minimalnej wartości listy (%s zł).</span></b>', Common::FormatPrice($fSum), Common::FormatPrice($fLimitPrice));
      } else {
        $sSumStr = sprintf('<b>Wartość zakupu pozycji w '.$sSource.': <span style="color: red">%s</span> zł. Pełnych List do wysłania <span style="color: red">%s</span> (%s)</b>', Common::FormatPrice($fSum), floor($fLists), number_format($fLists, 4));
      }
      $pForm->AddMergedRow($sSumStr, array('class'=>'merged', 'style'=>'padding-left: 15px;'));
    } elseif (isset($_POST['source'])) {
      if ($fSum > 0.00) {
        $sSumStr = sprintf('<b>Wartość netto pozycji: <span style="color: red">%s</span> zł.', Common::FormatPrice($fSum));
        $pForm->AddMergedRow($sSumStr, array('class'=>'merged', 'style'=>'padding-left: 15px;'));
      }
    }
    
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['found_books'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		
		if ($bSearch) {
			// wyswietlenie listy pasujacych uzytkownikow
			$pForm->AddMergedRow( $this->GetBooksList($pSmarty, $aBooks) );
		}
		$js = "
		<script type=text/javascript>
			$(document).ready(function(){

				$('.stockTooltipAjax').cluetip({
						width: '335px',
						showTitle: false,
						sticky: true,
						arrows: true,
						closePosition: 'top',
						closeText: 'Zamknij',
						mouseOutClose: true
					  });

			});
		</script>
		";

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()).$js);
		
	} // end of EditGroupDiscount() function
	
	
	/**
	 * Metoda wyswietla liste ksiazek z tablicy $aBooks
	 *
	 * @param		object	$pSmarty
	 * @param	array ref	$aBooks	- lista produktów
	 * @return	void
	 */
	function GetBooksList(&$pSmarty, &$aBooks) {
		global $aConfig, $pDbMgr;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$sources = $pDbMgr->GetAssoc('profit24', "SELECT id, symbol FROM sources");

		foreach($aBooks as $key => $book) {

			$id = explode('-', $key)[1];

			$attr = "/omniaCMS/ajax/ProductStockSuppliesAjax.php?product_id=$id";

			$src = $sources[$book['source']];

			$src = $src != null ? ucfirst($src) : null;

			$aBooks[$key]['source'] = '<a class="stockTooltip stockTooltipAjax" href="'.$attr.'" rel="'.$attr.'">' . $src . '</a>';
		}

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'checkboxes'	=> false,
			'form'	=>	false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_lp'],
				'width' => '30'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_isbn'],
				'width' => '60'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_name']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_publisher'],
				'width' => '60'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_year'],
				'width' => '50'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_bookindeks'],
				'width' => '50'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_ile'],
				'width' => '50'
			),
			array(
				'content'	=> _('Magazyn w S7'),
				'width' => '50'
			),
			array(
				'content'	=> _('Aktualne źródło'),
				'width' => '50'
			)
		);

		$pView = new View('ordered_itm', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array(
        'location'	=> array(
					'show'	=> false
					),
        'azymut_wholesale_price'	=> array(
					'show'	=> false
					),
        'siodemka_wholesale_price'	=> array(
					'show'	=> false
					),
        'ateneum_wholesale_price'	=> array(
					'show'	=> false
					),
        'platon_wholesale_price'	=> array(
					'show'	=> false
					),
		'panda_wholesale_price'	=> array(
				'show'	=> false
			),
        );
		// dodanie rekordow do widoku
		$pView->AddRecords($aBooks, $aColSettings);
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		// przepisanie langa - info o braku rekordow
		$aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_books_to_show'];
		return $pView->Show();
	} // end of GetBooksList() function
  
  
  /**
   * Metoda pobiera liste ksiazek zamowionych
   * 
   * @param $iSource - zrodlo zamowienia
   * @param $iStatus - stan pozycji zamówienia
   * @return array - lista ksiazek
   */
	function &getBooks($iSource,$iStatus) {
		global $aConfig;
		$aBooksToOrder = array();

		if ($iStatus != '') {
			// pobranie ID uzytkownikow spelniajacych zadane kryteria
			$sSql = "SELECT CONCAT(O.id, '-', A.product_id), 
                      A.product_id, GROUP_CONCAT(A.id SEPARATOR ',') as items, 
                      A.order_id, SUM(A.quantity) as quantity, A.shipment_date,
                      (SUM(A.quantity) * PS.azymut_wholesale_price) AS azymut_wholesale_price,
                      (SUM(A.quantity) * PS.siodemka_wholesale_price) AS siodemka_wholesale_price,
                      (SUM(A.quantity) * PS.ateneum_wholesale_price) AS ateneum_wholesale_price,
                      (SUM(A.quantity) * PS.platon_wholesale_price) AS platon_wholesale_price, 
                      (SUM(A.quantity) * PS.panda_wholesale_price) AS panda_wholesale_price,
                      C.source,
                      PS.siodemka_location
							 FROM ".$aConfig['tabls']['prefix']."orders_items A
							 JOIN ".$aConfig['tabls']['prefix']."products C
							 ON C.id = A.product_id
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_stock PS
							 ON PS.id = A.product_id
							 JOIN ".$aConfig['tabls']['prefix']."orders O
							 ON O.id=A.order_id
							 WHERE 1=1 
							 ".($iSource != '' ? " AND A.source = '".$iSource."'" : '')."
							 ".($iStatus === '-1' ? " AND (O.internal_status = '2' OR O.internal_status = '3') " : '')."
							 AND O.order_status <> '5'
							 AND A.deleted = '0'
							 AND A.packet = '0'
							 AND A.status = '".$iStatus."'".
								($iStatus == '3'?" AND A.sent_hasnc = '0'":'').
               " GROUP BY CONCAT(A.product_id, '-', O.id) 
							  ORDER BY status_1_update ASC ";//      $iStatus === '-1' ? '
				
			$aBooks = Common::GetAssoc($sSql,true,array(),DB_FETCHMODE_ASSOC,FALSE);
			$aBooksToOrder = $this->parseBooksArray($aBooks, $iStatus);
		}
		return $aBooksToOrder;
	} // end of getBooks() method

	/**
 * metoda pobiera liczbę ksiazek zamowionych
 * @param $iSource - zrodlo zamowienia
 * @param $iStatus - stan pozycji zamówienia
 * @return int - liczba ksiazek
 */
	function countBooks($iSource,$iStatus) {
		global $aConfig;

		// pobranie ID uzytkownikow spelniajacych zadane kryteria
		$sSql = "SELECT A.id
						 FROM ".$aConfig['tabls']['prefix']."orders_items A
						 JOIN ".$aConfig['tabls']['prefix']."orders O
						 ON O.id=A.order_id
						 WHERE 1=1 
						 ".(isset($iSource) ? "AND A.source = '".$iSource."'" : '')."
						 AND O.order_status <> '5'
						 AND A.deleted = '0'
						 AND A.packet = '0'
						 AND A.status = '".$iStatus."'".
							($iStatus=='3'?" AND A.sent_hasnc = '0'":'').'
                LIMIT 1';
		return Common::GetOne($sSql);
	} // end of countBooks() method
	
  
  /**
   * Metoda pobiera autorow produktu o podanym id
   * 
   * @param	intger	$iId	- Id produktu
   * @return	array	- autorzy produktu
   */
	function getAuthors($iId){
		global $aConfig;
		
		// pobranie autorow ksiazki
		$sSql = "SELECT B.name as role_name, CONCAT(C.surname,' ',C.name) as author
						FROM ".$aConfig['tabls']['prefix']."products_to_authors A
						JOIN ".$aConfig['tabls']['prefix']."products_authors_roles B
						ON B.id = A.role_id
						JOIN ".$aConfig['tabls']['prefix']."products_authors C
						ON C.id = A.author_id
						WHERE A.product_id = ".$iId."
						ORDER BY B.order_by";
						
		$aResult =& Common::GetAll($sSql);
		$sAuthors='';
	
		foreach($aResult as $aAuthor){
			$sAuthors .= $aAuthor['author'].', ';
		}
		return substr($sAuthors,0,-2);
	} // end of getAuthors() method
	
	
  /**
   * Metoda pobiera emaile 
   * 
   * @param	intger	$iId	- Id produktu
   * @param	string	$sEmail	- email użytkownika
   * @return	array	- autorzy produktu
   */
	function &getEmails($iId=0, $sEmail=''){
		global $aConfig;
		
		// pobranie autorow ksiazki
		$sSql = "SELECT CONCAT(email, ' ', name,' ' , surname) AS label, id AS value, email AS email
						FROM ".$aConfig['tabls']['prefix']."orders_address_book
						WHERE 1=1 
						".(!$iId==0? " AND id=".$iId : '').
						(!empty($sEmail) ? " AND email='".$sEmail."'" : '').
						" ORDER BY id";

	return Common::GetAll($sSql);
	} // end of getEmails() method
	
  
	/**
	 * Metoda sortuje książki po nazwie
	 *
	 * @global type $aConfig
	 * @param type $aBooks 
	 */
	function sortBooksByName($aBooks) {
		global $aConfig;
		$aNewArr = array();
		
		$aBIds = array_keys($aBooks);
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id IN (".implode(', ', $aBIds).")
						ORDER BY name";
		$aIdsSorted = Common::GetCol($sSql);
		
		$i=0;
		foreach ($aIdsSorted as $iBId) {
			
			if (is_array($aBooks[$iBId]) && !empty($aBooks[$iBId])) {
				$aNewArr['n_'.$i] = $aBooks[$iBId];
				$aNewArr['n_'.$i]['id'] = $iBId;
				
				$i++;
			}
		}
		// znowu przepisanie do aBookx
		$aBooks = array();
		foreach ($aNewArr as $aBook) {
			$aBooks[$aBook['id']] = $aBook;
			unset($aBooks[$aBook['id']]['id']);
		}
		return $aBooks;
	}// end of sortBooksByName() method
} // end of Module Class
?>
