<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - statystyki sprzedazy produktow
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
// dolaczenie wspolnej klasy Module_Common
include_once('modules/m_oferta_produktowa/Module_Common.class.php');
set_time_limit('3600');
ini_set('memory_limit', '512M');

class Module extends Module_Common {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// ID strony modulu zamowien
	var $iPageId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  private $sDBFFilePath = 'import/DBF/';
  
  private $aCols = array (
      0 => 
      array ('TOW_USL', 'N', 1, 0
      ),      1 => 
      array ('NAZWA_ART', 'C', 35, 0
      ),      2 => 
      array (
        'OPIS_ART','C',35,0,      
      ),      3 => 
      array (
        'NR_HANDL','C',20,0,      
      ),      4 => 
      array (
        'NR_KATAL','C',20,0,      
      ),  5 => 
      array (
        'PLU','N',6,0,
      ),      6 => 
      array (
        'BARKOD','C',20,0,
      ),      7 => 
      array (
        'IDENT_KAT','N',8,0,
      ),      8 => 
      array (
        'STAN','N',11,3,
      ),      9 => 
      array (
        'STAN_FAKT','N',11,3,
      ),      10 => 
      array (
        'STAN_MIN','N',11,3,
      ),      11 => 
      array (
        'STAN_MAX','N',11,3,
      ),      12 => 
      array (
        'CENA','N',11,2,
      ),      13 => 
      array (
        'CENAX','N',11,2,
      ),      14 => 
      array (
        'NARZUT_A','N',8,4,
      ),      15 => 
      array (
        'CENA_A','N',11,2,
      ),      16 => 
      array (
        'CENA_AX','N',11,2,
      ),      17 => 
      array (
        'UPUST_B','N',6,2,
      ),      18 => 
      array (
        'CENA_B','N',11,2,
      ),      19 => 
      array (
        'CENA_BX','N',11,2,
      ),      20 => 
      array (
        'UPUST_C','N',6,2,
      ),      21 => 
      array (
        'CENA_C','N',11,2,
      ),      22 => 
      array (
        'CENA_CX','N',11,2,
      ),      23 => 
      array (
        'CENAEWID','N',11,2,
      ),      24 => 
      array (
        'ZAMOWIENIA','N',11,3,
      ),      25 => 
      array (
        'ZAMREZER','N',11,3,
      ),      26 => 
      array (
        'ZAMOWDOST','N',11,3,
      ),      27 => 
      array (
        'CENAWALWE','N',15,4,
      ),      28 => 
      array (
        'NARZUTWAL','N',8,4,
      ),      29 => 
      array (
        'CENAWALWY','N',15,4,
      ),      30 => 
      array (
        'JEDN_M','C',7,0,
      ),      31 => 
      array (
        'JM_PODZIEL','L',1,0,
      ),      32 => 
      array (
        'STAWKA_VAT','N',5,2,
      ),      33 => 
      array (
        'ST_VAT_ZAK','N',5,2,
      ),      34 => 
      array (
        'SWW_KU','C',10,0,
      ),      35 => 
      array (
        'PKWIU','C',14,0,
      ),      36 => 
      array (
        'PCN','C',9,0,
      ),      37 => 
      array (
        'WAL_KOD','C',3,0,
      ),      38 => 
      array (
        'WAL_KURS','N',12,6,
      ),      39 => 
      array (
        'WAL_KURSS','N',12,6,
      ),      40 => 
      array (
        'WAL_DATA','D',8,0,
      ),      41 => 
      array (
        'CLO','N',6,2,      
      ),      42 => 
      array (
        'TRANSPORT','N',6,2,      
      ),      43 => 
      array (
        'AKCYZA_PR','N',6,2,
      ),      44 => 
      array (
        'AKCYZA','N',9,2,
      ),      45 => 
      array (
        'PODATEK','N',6,2,
      ),      46 => 
      array (
        'KOSZT_PROC','N',6,2,
      ),      47 => 
      array (
        'CERTYFIKAT','L',1,0,
      ),      48 => 
      array (
        'CERTYFOPIS','C',35,0,
      ),      49 => 
      array (
        'CERTYFDATA','D',8,0,
      ),      50 => 
      array (
        'PROD_MNOZ','N',11,3,
      ),      51 => 
      array (
        'LOKACJA','C',15,0,
      ),      52 => 
      array (
        'WAGA_JM_KG','N',10,4,
      ),      53 => 
      array (
        'ID_KAT','N',4,0,
      ),      54 => 
      array (
        'NAZWA_KAT','C',25,0,
      ),      55 => 
      array (
        'KSTAWKAVAT','N',6,2,
      ),      56 => 
      array (
        'CENAWE','N',13,2,
      ),      57 => 
      array (
        'CENAWEX','N',13,2,
      ),      58 => 
      array (
        'DATA_WAZN','D',8,0,
      ),      59 => 
      array (
        'NR_SERII','C',10,0,
      ),      60 => 
      array (
        'STAWKAVATP','N',5,2,
      ),      61 => 
      array (
        'DATA','D',8,0,
      ),      62 => 
      array (
        'ILOSC','N',11,3,      
      ),      63 => 
      array (
        'deleted','L',1,0
      )
    );
  
  private $aPaymentMethods = array(
      array(
          'value' => 'postal_fee', 
          'label' => 'PO'),      
      array(
          'value' => 'platnoscipl', 
          'label' => 'PE'), 
      array(
          'value' => 'bank_transfer', 
          'label' => 'PP'
          ),
      array(
          'value' => 'bank_14days', 
          'label' => '14'
          )
  );
  
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty, $bInit = FALSE) {
		global $aConfig;
    if ($bInit === TRUE) {
      return true;
    }
    
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_zamowienia', $_GET['action']);
		$this->sModule = $_GET['module'];
		
		$sDo = '';
		$iId = 0;
				
    if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    $sItem = '';
    if (isset($_GET['item'])) {
      $sItem = $_GET['item'];
    }

		// wyswietlenie
		switch ($sDo) {
      case 'new_process_generate':
        //$sNewProcess = phpSelf(array('do' => 'generate', 'start_date' => $_POST['start_date']));
        $sCMDExec = 'setsid php -f '.__DIR__.'/../../../import/DBF/createDBF.php '.$_POST['start_date'].' & echo $!';
        echo $sCMDExec;
        exec($sCMDExec);
      break;
			case 'generate':
				$this->generate($pSmarty);
			break;
      case 'delete':
        $this->delete($pSmarty, $sItem);
      break;
			case 'choose_conditions':
			default:
				$this->chooseConditions($pSmarty);
			break;
		}
	} // end of Module() method
  
  
  /**
   * Metoda usuwa plik z eksportem
   * 
   * @global array $aConfig
   * @param type $pSmarty
   * @param type $sFile
   * @return type
   */
  public function delete($pSmarty, $sFile) {
    global $aConfig;
    $bIsErr = FALSE;
    
    $sPath = $aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath;
    if (file_exists($sPath.'/'.$sFile)) {
      if (unlink($sPath.'/'.$sFile) === FALSE) {
        $bIsErr = TRUE;
      }
    } else {
      $bIsErr = TRUE;
    }
    
    if ($bIsErr === TRUE) {
      $this->sMsg = sprintf(_('Wystąpił błąd usuwania pliku ZIP "%s"'), $sFile);
      AddLog($this->sMsg, true);
      $this->sMsg = GetMessage($this->sMsg, true);
      return $this->chooseConditions($pSmarty);
    } else {
      $this->sMsg = sprintf(_('Plik ZIP "%s" został usunięty'), $sFile);
      AddLog($this->sMsg, false);
      $this->sMsg = GetMessage($this->sMsg, false);
      return $this->chooseConditions($pSmarty);
    }
  }// end of delete() method
  
  
	/**
	 * Metoda wyswietla statystyki dla wybranych warunkow
	 *
	 * @param		object	$pSmarty
	 */
	function generate($pSmarty) {
		
    $sStartDate = $_POST['start_date'];
    $aGeneretedFiles = $this->genenerateAllProductsDBF($sStartDate);
    if ($this->compressAndDelOld($aGeneretedFiles, $sStartDate) === false) {
      sleep(3);
      $this->sMsg = sprintf(_('Wystąpił błąd podczas generowania plików DBF %s'), implode(', ', $aGeneretedFiles));
      AddLog($this->sMsg, false);
      $this->sMsg = GetMessage($this->sMsg, false);
      return $this->chooseConditions($pSmarty);
    } else{ 
      sleep(3);
      $this->sMsg = sprintf(_('Pliki DBF zostały wygenerowane %s'), implode(', ', $aGeneretedFiles));
      AddLog($this->sMsg, false);
      $this->sMsg = GetMessage($this->sMsg, false);
      return $this->chooseConditions($pSmarty);
    }
	} // end of generate() function

  
	/**
	 * Metoda tworzy formularz wyboru danych do statystyk
	 * oraz wyswietla statystyki
	 *
	 * @param		object	$pSmarty
	 */
	function chooseConditions(&$pSmarty) {
		global $aConfig;
		$aLang =& $aConfig['lang'][$this->sModule];
				
		$aData = array();
		$aConditions = array('start_date', 'end_date');
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			// zczytanie z GET
			foreach ($aConditions as $sCond) {
				$aData[$sCond] = isset($_GET[$sCond]) ? $_GET[$sCond] : '';
			}
		}
					
		$pForm = new FormTable('stats_conditions', $aLang['header'], array('action'=>phpSelf(array())), array('col_width'=>260), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'new_process_generate');
		
    /*
    $pForm->AddRadioSet('website_id', _('Serwis'), $this->getWebsites(), '', true, true);
    
    $pForm->AddRadioSet('payment_method', _('Metoda płatności'), $this->aPaymentMethods, '', true, true);
    */
		// zakres dat od
		$pForm->AddRow($pForm->GetLabelHTML('', _('Zamówienia zrealizowane w dniu: ')), '<div style="float: left; margin-top: 8px;">'.'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('start_date', '', $aData['start_date'], array('style'=>'width: 75px;'), '', 'date').'</div>');
		
		// przyciski		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Generuj')).'&nbsp;&nbsp;');
		
    $pForm->AddMergedRow(_('Wygenerowane raporty magazynowe w formacie DBF'), array('class' => 'merged'));
    $aZipFiles = $this->getZIPList();
    if (!empty($aZipFiles)) {
      foreach ($aZipFiles as $aZipFile) {
        $pForm->AddRow('', '<a style="font-size: 15px!important;" target="_blank" href="'.$aZipFile['value'].'">'.$aZipFile['label'].'</a> 
                                            ('.$aZipFile['last_modified'].') 
                                            <a title="Usuń raport" href="'.phpSelf(array('do' => 'delete', 'item' => $aZipFile['label'])).'"><img src="gfx/icons/delete_ico.gif" /></a>');
      }
    }
    $pForm->AddMergedRow(_(''), array('class' => 'merged'));
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of chooseConditions() function
  
  
  /**
   * Metoda zwraca listę plików do zapakowania zipem
   * 
   * @global array $aConfig
   * @return array
   */
  private function getZIPList() { 
    global $aConfig;
    
    $sURL = $aConfig['common']['client_base_url_https'].'import/DBF/';
    $aZipFiles = array();
    
    $sPath = $aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath;
    
    if ($handle = opendir($sPath)) {
      while (false !== ($sFile = readdir($handle))) {
        if ($sFile != "." && $sFile != "..") {
          $aMatches = array();
          preg_match('/([^\/]*(\.zip))$/', $sFile, $aMatches);
          if (isset($aMatches[1]) && !empty($aMatches[1])) {
            $aZipFiles[] = array(
                'label' => $sFile,
                'value' => $sURL.$sFile,
                'last_modified' => date("d.m.Y H:i:s", filemtime($sPath.'/'.$sFile))
            );
          }
        }
      }
      closedir($handle);
    }
    arsort($aZipFiles);
    return $aZipFiles;
  }// end of getZIPList() method
	
  
  /**
   * Metoda pobiera listę serwisów
   * 
   * @global type $aConfig
   * @return type
   */
	public function getWebsites() {
    global $aConfig;
    
    // pobierzmy id i symbole dostepnych serwiwsów
		$sSql = "SELECT id as value, name as label
						 FROM ".$aConfig['tabls']['prefix']."websites";
		return Common::GetAll($sSql);
  }// end of getWebsites() method
  
  
  /**
   * Metoda kompresuje katalog z DBF'ami i generuje .zipa
   * 
   * @global array $aConfig
   * @param array $aGeneretedFiles
   * @param string $sStartDate
   */
  public function compressAndDelOld($aGeneretedFiles, $sStartDate) {
    global $aConfig;
    
    $sCurrDate = str_replace('-', '', $sStartDate);
    $sZipCurrDate = preg_replace('/(\d{2})-(\d{2})-(\d{4})/', '$3$2$1', $sStartDate);
    $sFileZipName = $sZipCurrDate.'.zip';
    $sDBFZip = $aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath;
    
    if (!empty($aGeneretedFiles)) {
      // pakujemy
      if (file_exists($sDBFZip.$sFileZipName)) {
        unlink($sDBFZip.$sFileZipName);
      }
      exec('cd '.$sDBFZip.' && zip -r "'.$sFileZipName.'" '.$sCurrDate.'  & echo $!');

      // usuwamy stare pliki
      foreach ($aGeneretedFiles as $sFile) {
        if (file_exists($sDBFZip.$sCurrDate.'/'.$sFile)) {
          unlink($sDBFZip.$sCurrDate.'/'.$sFile);
        }
      }
      rmdir($sDBFZip.$sCurrDate);
      return true;
    } else {
      return false;
    }
  }// end of compressAndDelOld() method
  
  
  /**
   * Metoda generuje wszystkie pliki DBF
   * 
   * @return type
   */
  public function genenerateAllProductsDBF($sStartDate) {
    global $aConfig;
    $aGeneretedFiles = array();
    
     // pobierzmy id i symbole dostepnych serwiwsów
		$sSql = "SELECT id, name
						 FROM websites";
		$aWebsites =& Common::GetAll($sSql);
    foreach ($aWebsites as $aWebsite) {
      foreach ($this->aPaymentMethods as $aPaymentMethod) {
        $sWebsiteSymbol = $aConfig['invoice_website_symbol_'.$aWebsite['id']];
        $aProducts =& $this->getProductOrdersReport($sStartDate, $aPaymentMethod['value'], $aWebsite['id'], null);
        if (isset($aProducts['items']) && !empty($aProducts['items'])) {
          $aGeneretedFiles[] = $this->generateProductDBF($aProducts, $sStartDate, $sWebsiteSymbol, $aPaymentMethod['label']);
        }
      }
    }
    return $aGeneretedFiles;
  }// end of genenerateAllProductsDBF() method

	
	/**
	 * 
	 * @param $sPaymentType
	 * @return unknown_type
	 */
	function &getProductOrdersReport($sStartDate, $sPaymentType, $iWebsiteId, $iOrderId=0){
		global $aConfig;
		
		$sPaymentSql = '';
		if ($sPaymentType != '') {
			if ($sPaymentType == 'platnoscipl') {
				$sPaymentSql = " AND IF(A.payment_type<>'', 
															 (A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay'), 
															 (B.payment_type = 'platnoscipl' OR B.payment_type = 'card' OR B.payment_type = 'dotpay')) ";
			}
			else {
				$sPaymentSql = " AND IF(A.payment_type<>'', (A.payment_type = '".$sPaymentType."'), (B.payment_type = '".$sPaymentType."')) ";
			}
		}
		
		$sSql = "SELECT A.id, A.product_id, A.name, A.authors,										A.discount, A.price_brutto, A.price_netto, A.vat, A.promo_price_brutto, A.promo_price_netto, A.isbn,										SUM(A.quantity) quantity, SUM(A.value_brutto) val_brutto, SUM(A.value_netto) val_netto, B.order_number,            GROUP_CONCAT(A.id SEPARATOR ',') as ids, B.invoice_date
							FROM ".$aConfig['tabls']['prefix']."orders_items A
							JOIN ".$aConfig['tabls']['prefix']."orders B
							 ON B.id = A.order_id
							 WHERE A.deleted ='0'
							 			AND A.packet = '0'
										AND A.item_type <> 'A'
										AND B.website_id = ".$iWebsiteId."
							 			".$sPaymentSql." AND
							 			 B.invoice_date >= '".formatDate($sStartDate)." 00:00:00' AND
							 			 B.invoice_date <= '".formatDate($sStartDate)." 23:59:59'".
										($iOrderId>0?" AND A.order_id = ".$iOrderId:'').
							 			 "
							 		 GROUP BY A.product_id, A.price_brutto, A.discount, A.vat, A.name
							 		 ORDER BY A.discount, A.name";
		$aProducts['items'] =& Common::GetAll($sSql);
		$aProducts2=array();
		$aProducts2['total_val_brutto'] = 0.0;
		$aProducts2['total_price_brutto'] = 0.0;
		$aProducts2['total_promo_price_brutto'] = 0.0;
		$aProducts2['total_quantity'] = 0;
		$aAttachments=array();
		$aIds=array();
		$aIds[0]=0;
		$aCounts=array();
		$iLp=1;
		
		// dodanie załączników zamówienia
		$aIOIds = array();
		$aNProducts = array();
		$aProductsTMP = array();
		foreach ($aProducts['items'] as $aPItem) {
			 $aIOIds[] = $aPItem['id'];
			 
					 $sSql = "SELECT A.id, A.product_id, A.name, A.authors,										A.discount, A.price_brutto, A.price_netto, A.vat, A.promo_price_brutto, A.promo_price_netto, A.isbn,										SUM(A.quantity) quantity, SUM(A.value_brutto) val_brutto, SUM(A.value_netto) val_netto, B.order_number, B.invoice_date
							FROM ".$aConfig['tabls']['prefix']."orders_items A
							JOIN ".$aConfig['tabls']['prefix']."orders B
							 ON B.id = A.order_id
							 WHERE A.deleted ='0'
							 			AND A.packet = '0'
										AND A.item_type = 'A'
										AND B.website_id = ".$iWebsiteId."
							 			AND A.parent_id IN (".$aPItem['ids'].")
							 		 GROUP BY A.product_id, A.price_brutto, A.discount, A.vat, A.name
							 		 ORDER BY IFNULL( A.parent_id, A.id ), A.id, A.discount";
				$aProductsTMP =& Common::GetAll($sSql);
				
				// dołączenie ojca
				$aNProducts[] = $aPItem;
				foreach ($aProductsTMP as $aPTMP) {
					// nazwy analogicznie jak były
					$aPTMP['name'] = $aPTMP['name'].'-'.$aPItem['name'];
					$aNProducts[] = $aPTMP;
				}
		}
		// nadpisanie zamowień
		$aProducts['items'] = $aNProducts;
		// koniec dodanie załączników zamówienia
		
		if(!empty($aProducts['items'])){
			foreach($aProducts['items'] as $iKey=>$aProduct){
				$aItem=array();
				$aItem['lp']=$iLp++;
				$aItem['isbn']=$aProduct['isbn'];
				$aItem['authors'] = mb_substr(strip_tags($aProduct['authors']),0,12,'UTF-8').(mb_strlen($aProduct['authors'], 'UTF-8')>12?'...':'');
				$aItem['name'] = mb_substr(strip_tags($aProduct['name']),0,38,'UTF-8').(mb_strlen($aProduct['name'], 'UTF-8')>38?'...':'');
				$aProducts2['total_quantity'] += $aProduct['quantity'];
				$aIds[]=$aProduct['id'];
				$aCounts[$aProduct['id']][0]=$aProduct['quantity'];
				$aCounts[$aProduct['id']][1]=$aProduct['name'];
				
				$aItem['val_brutto'] = $aProduct['val_brutto'];//Common::formatPrice(($aProduct['price_brutto']*(1-$aProduct['discount']/100))*$aProduct['quantity']);
				$aItem['discount'] = Common::formatPrice($aProduct['discount']);
				$aItem['price_netto'] = $aProduct['val_netto'];//=$aProduct['val_brutto']/(1+$aProduct['vat']/100);//$aProduct['val_netto'];//Common::formatPrice($aProduct['val_brutto']/(1+$aProduct['vat']/100));//
				
				$aItem['price_brutto'] = Common::formatPrice($aProduct['price_brutto']);
				$aItem['price_netto'] = Common::formatPrice($aItem['price_netto']);
				$aItem['val_brutto'] = Common::formatPrice($aItem['val_brutto']);
				$aItem['promo_price_brutto'] = Common::formatPrice($aProduct['promo_price_brutto']);
        $aItem['promo_price_netto'] = Common::formatPrice($aProduct['promo_price_netto']);
				$aItem['vat'] = round($aProduct['vat'],0);//Common::formatPrice($aProduct['vat']);
        $aInvData = explode(' ', str_replace('-', '', $aProduct['invoice_date']));
        $aItem['invoice_date'] = $aInvData[0];
				$aItem['quantity'] = $aProduct['quantity'];
				
				$aProducts2['total_price_brutto'] += Common::formatPrice2($aItem['price_brutto']);
				$aProducts2['total_price_netto'] += Common::formatPrice2($aProduct['val_netto']);//Common::formatPrice2($aItem['price_netto']);
				$aProducts2['total_promo_price_brutto'] += Common::formatPrice2($aItem['promo_price_brutto']);
				$aProducts2['total_val_brutto'] += $aProduct['val_brutto'];//Common::formatPrice2($aItem['val_brutto']);
		
				$aItem['order_number'] = $aProduct['order_number'];
				$aProducts2['items'][]=$aItem;
				if(!empty($aAt))
					$aProducts2['items'][]=$aAt;
			}
		}
	
		$aTransports = $this->getOrdersTransports($sPaymentType, $iWebsiteId, $sStartDate, $iOrderId);
		//dump($aTransports);
		
		if(!empty($aTransports)){
			foreach($aTransports as $iKey=>$aTransport){
				$aProducts2['total_val_brutto'] += $aTransport['transport_cost']*$aTransport['quantity'];
				$aProducts2['total_price_netto'] += Common::formatPrice2($aTransport['transport_cost']/(1.0+($aTransport['transport_vat']/100.0)))*$aTransport['quantity'];
				$aProducts2['total_price_brutto'] += $aTransport['transport_cost'];
				$aProducts2['total_promo_price_brutto'] += $aTransport['transport_cost'];
				$aProducts2['total_quantity'] += $aTransport['quantity'];
				$aItem = array();
				$aItem['price_brutto'] = Common::formatPrice($aTransport['transport_cost']);
				$aItem['price_netto'] = Common::formatPrice(($aTransport['transport_cost']/(1.0+($aTransport['transport_vat']/100.0)))*$aTransport['quantity']);
				$aItem['discount'] = '0,00';
				$aItem['vat'] = round($aTransport['transport_vat'],0);//Common::formatPrice($aTransport['transport_vat']);
				$aItem['promo_price_brutto'] = Common::formatPrice($aTransport['transport_cost']);
				$aItem['val_brutto'] = Common::formatPrice($aTransport['transport_cost']*$aTransport['quantity']);
				$aItem['name'] = $aTransport['transport'];
				$aItem['quantity'] = $aTransport['quantity'];
				$aProducts2['items'][] = $aItem;
			}
		}
		
		//$aProducts['total'] = $fTotalValBrutto;
		$aProducts2['total_val_brutto'] = Common::formatPrice($aProducts2['total_val_brutto']);
		$aProducts2['total_price_netto'] = Common::formatPrice($aProducts2['total_price_netto']);
		$aProducts2['total_price_brutto'] = Common::formatPrice($aProducts2['total_price_brutto']);
		$aProducts2['total_promo_price_brutto'] = Common::formatPrice($aProducts2['total_promo_price_brutto']);
		
		if($sPaymentType!='bank_14days') {
			$aVals2=$this->getOrdersReport($sPaymentType, $iWebsiteId, $sStartDate);
			$aProducts2['total_price_netto']=$aVals2['total_netto'];
		}
		return $aProducts2;
	} // end of getProductOrdersReport() method 
	
	
  /**
   * Metoda wydobywa isbn z tablicy produktów
   * 
   * @param array $aProducts
   * @return array
   */
  private function _getArrIsbn($aProducts) {
    
    $aISBN = array();
    if (isset($aProducts['items']) && !empty($aProducts['items'])) {
      foreach ($aProducts['items'] as $aItem) {
        $aISBN[] = $aItem['isbn'];
      }
    }
    return $aISBN;
  }// end of _getArrIsbn method
  
  
  /**
   * Metoda przeszukuje DBF'a w poszukiwaniu rekordów o przekazanych isbn
   * 
   * @global array $aConfig
   * @param array $aProductsItems
   * @param array $aISBN tablica ISBN
   * @return array
   */
  private function _getArrRecordsDBF($aProductsItems, $aISBN) {
    global $aConfig;
    
    $iDBF = dbase_open($aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath.'ARTYKULY_BAZA_DANYCH.DBF','0');
    
    $iCountRecords = dbase_numrecords($iDBF);
    for ($i = 1; $i <= $iCountRecords; $i++) {
      $aRecordTMP = dbase_get_record_with_names($iDBF, $i);
      foreach ($aRecordTMP as &$sCol) {
        $sCol = trim($sCol);
      }
      
      $aMatchedKeys = array_keys($aISBN, $aRecordTMP['CERTYFOPIS']);
      foreach ($aMatchedKeys as $iMatchedProductsItemKey) {
        if ($iMatchedProductsItemKey !== FALSE) {
          // jeśli już zdefiniowany, a ilość większa od 1 to nadpisujemy
          if (isset($aProductsItems[$iMatchedProductsItemKey]['DBF']) && ($aRecordTMP['STAN'] > $aProductsItems[$iMatchedProductsItemKey]['DBF']['STAN'])) {
            // nadpisujemy
            $aProductsItems[$iMatchedProductsItemKey]['DBF'] = $aRecordTMP;
          } elseif (!isset($aProductsItems[$iMatchedProductsItemKey]['DBF'])) {
            // definiujemy
            $aProductsItems[$iMatchedProductsItemKey]['DBF'] = $aRecordTMP;
          }
        }
      }
    }
    return $aProductsItems;
  }// end of _getArrRecordsDBF() method
  
  
  /**
   * Metoda z tablicy produktów tworzy kod html
   * 
   * @global type $aConfig
   * @global type $pSmarty
   * @param type $aProducts
   * @return type
   */
	function generateProductDBF(&$aProducts, $sStartDate, $sWebsite, $sExportType){
		global $aConfig;
    
    $sDirDate = str_replace('-', '', $sStartDate);
    $sStDate = substr(str_replace('-', '', $sStartDate), 0, 4);
    $sFilename = substr($sWebsite, 0 , 2).$sStDate.substr($sExportType, 0, 3).'.dbf';
		$aModule = array();
		$aModule['lang'] =& $aConfig['lang'][$this->sModule];
    $sFileDir = $aConfig['common']['client_base_path'].'/'.$this->sDBFFilePath.$sDirDate.'/';
    if (!is_dir($sFileDir)) {
      mkdir($sFileDir);
    }
		$sFile = $sFileDir.$sFilename;
    
    
    $aISBNs = $this->_getArrIsbn($aProducts);
    $aDBFRecords = $this->_getArrRecordsDBF($aProducts['items'], $aISBNs);
    $this->_saveDBF($aDBFRecords, $sFile);
    return $sFilename;
	} // end of generateProductDBF() method
  
  
    
  /**
   * Metoda tworzy i zapisuje plik DBF
   * 
   * @param array $aDBFRecords
   * @param string $sFilePath
   */
  private function _saveDBF($aDBFRecords, $sFilePath) {
    
    if (file_exists($sFilePath)) {
      unlink($sFilePath);
    }
    if (!empty($aDBFRecords)) {
      $iDB = dbase_create($sFilePath, $this->aCols);
      foreach ($aDBFRecords as $aRecord) {
        if (!empty($aRecord['DBF'])) {
          if ($aRecord['promo_price_netto'] <= 0) {
            $aRecord['promo_price_netto'] = $aRecord['price_netto'];
          }
          if ($aRecord['promo_price_brutto'] <= 0) {
            $aRecord['promo_price_brutto'] = $aRecord['price_brutto'];
          }
          unset($aRecord['DBF']['deleted']);
          $aRecord['DBF']['CENA'] = str_replace(',', '.', $aRecord['promo_price_netto']);
          $aRecord['DBF']['CENAX'] = str_replace(',', '.', $aRecord['promo_price_brutto']);
          $aRecord['DBF']['STAN'] = '';
          $aRecord['DBF']['STAN_FAKT'] = '';
          $aRecord['DBF']['ID_KAT'] = '';
          $aRecord['DBF']['NAZWA_KAT'] = '';
          $aRecord['DBF']['KSTAWKAVAT'] = '';
          $aRecord['DBF']['CENAWE'] = '';
          $aRecord['DBF']['CENAWEX'] = '';
          $aRecord['DBF']['DATA_WAZN'] = '';
          $aRecord['DBF']['NR_SERII'] = '';
          $aRecord['DBF']['STAWKAVATP'] = $aRecord['vat'];
          $aRecord['DBF']['DATA'] = $aRecord['invoice_date'];
          $aRecord['DBF']['ILOSC'] = $aRecord['quantity'];
          $aRecord['DBF']['deleted'] = 0;
  //        dump(count(array_values($aRecord['DBF'])));
  //        dump(array_values($aRecord['DBF']));
          dbase_add_record($iDB, array_values($aRecord['DBF']));
        }
      }
      dbase_close($iDB);
    }
  }// end of saveDBF methodi
	
  
	function &getOrdersTransports($sPaymentType, $iWebsiteId, $sStartDate, $iOrderId=0){
		global $aConfig;
		$sPaymentSql = '';
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay')";
		}
		else {
			$sPaymentSql = "A.payment_type = '".$sPaymentType."'";
		}
		$sSql = "SELECT A.transport_id, A.transport_cost,A.transport, A.transport_vat, count(A.id) as quantity 
							FROM ".$aConfig['tabls']['prefix']."orders A
							LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
							ON B.id=A.transport_id 
							WHERE B.personal_reception <> '1' AND
										".$sPaymentSql." AND
										A.website_id = ".$iWebsiteId." AND
							 			A.invoice_date >= '".formatDate($sStartDate)." 00:00:00' AND
							 			A.invoice_date <= '".formatDate($sStartDate)." 23:59:59'"
							 			.($iOrderId>0?" AND A.id = ".$iOrderId:'').
							" GROUP BY A.transport_vat, A.transport_id, A.transport_cost";
		return Common::GetAll($sSql);
	} // end of getOrdersTransports() method 


	function &getOrderInvoiceAddress($iId){
		global $aConfig;
		
		$sSql = "SELECT is_company, company, name, surname, street, number, number2, postal, city, nip, phone
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
						 " AND 
						 address_type = '0'
						  ORDER BY address_type"; 
		return Common::GetRow($sSql);
	} // end of getOrderInvoiceAddress() method
	
	
	function &getOrdersReport($sPaymentType, $iWebsiteId, $sStartDate){
		global $aConfig;
		$sPaymentSql = '';
		if ($sPaymentType == 'platnoscipl') {
			$sPaymentSql = "(   (A.payment_type = 'platnoscipl' OR A.payment_type = 'card' OR A.payment_type = 'dotpay')
											 OR (A.second_payment_type = 'platnoscipl' OR A.second_payment_type = 'card' OR A.second_payment_type = 'dotpay')
											)
											 ";
		}
		else {
			$sPaymentSql = " (A.payment_type = '".$sPaymentType."' OR A.second_payment_type = '".$sPaymentType."') ";
		}
		
		$sSql = "SELECT id, second_invoice
							FROM ".$aConfig['tabls']['prefix']."orders A
							 WHERE ".$sPaymentSql."  AND
										 A.website_id = ".$iWebsiteId." AND
							 			 A.invoice_date >= '".formatDate($sStartDate)." 00:00:00' AND
							 			 A.invoice_date <= '".formatDate($sStartDate)." 23:59:59'";

		$aOrders =& Common::GetAll($sSql);
		$aTable= array();
		$iLp=1;
		if(!empty($aOrders)){
			$sSql='SELECT value FROM '.$aConfig['tabls']['prefix'].'products_vat_stakes WHERE 1';
			$aVatStakesT=Common::GetAll($sSql);
			$aVatStakes=array();
			//przepisanie do tablicy
			foreach($aVatStakesT as $aVat)
				$aVatStakes[]=$aVat['value'];
				
			foreach($aOrders as $iKey=>$aOrder) {
				$aItem = $this->getOrderVat($aOrder['id'],false,$iLp++, $sPaymentType);
				$aTable['items'][] = $aItem;
				//sprawdzenie czy są jakieś inne stawki
				foreach($aItem['vat'] as $iOVats=>$aVaVa)
					if(!in_array($iOVats,$aVatStakes))
						$aVatStakes[]=$iOVats;
						
				foreach($aVatStakes as $iKey => $aVatItem) {
					$aTable['vat_'.$aVatItem.'_brutto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_brutto']);
					$aTable['vat_'.$aVatItem.'_netto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_netto']);
					//$aTable['vat_7_brutto'] += Common::formatPrice2($aItem['vat'][7]['value_brutto']);
					//$aTable['vat_22_brutto'] += Common::formatPrice2($aItem['vat'][22]['value_brutto']);
					}
					
				if($aOrder['second_invoice'] == '1') {
					$aItem = $this->getOrderVat($aOrder['id'],true,$iLp++, $sPaymentType);
					$aTable['items'][] = $aItem;
					foreach($aVatStakes as $iKey => $aVatItem) { 
						$aTable['vat_'.$aVatItem.'_brutto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_brutto']);
						$aTable['vat_'.$aVatItem.'_netto'] += Common::formatPrice2($aItem['vat'][$aVatItem]['value_netto']);
						//$aTable['vat_7_brutto'] += Common::formatPrice2($aItem['vat'][7]['value_brutto']);
						//$aTable['vat_22_brutto'] += Common::formatPrice2($aItem['vat'][22]['value_brutto']);
					}
				}
			}
		}
    if (!empty($aVatStakes)) {
      foreach($aVatStakes as $iKey => $aVatItem) {
        // przeliczanie dla stawki VAT 0 - sformatowane z '.' jako separator dziesietny
        $aTable['vat_'.$aVatItem.'_brutto'] = Common::formatPrice2($aTable['vat_'.$aVatItem.'_brutto']);
        $aTable['vat_'.$aVatItem.'_netto'] = Common::FormatPrice2($aTable['vat_'.$aVatItem.'_brutto']/(1+$aVatItem/100));// =  Common::FormatPrice2($aTable['vat_'.$aVatItem.'_netto']);
        //$aTable['vat_'.$aVatItem.'_netto'] = Common::formatPrice2($aTable['vat_'.$aVatItem.'_brutto'] / (1 + $aVatItem/100));
        $aTable['vat_'.$aVatItem.'_currency'] = $aTable['vat_'.$aVatItem.'_brutto'] - $aTable['vat_'.$aVatItem.'_netto'];
        $aTable['total_brutto'] += $aTable['vat_'.$aVatItem.'_brutto'];
        $aTable['total_netto'] += $aTable['vat_'.$aVatItem.'_netto'];
        $aTable['total_vat'] += $aTable['vat_'.$aVatItem.'_currency'];

        /*// przeliczanie dla stawki VAT 7 - sformatowane z '.' jako separator dziesietny
        $aTable['vat_7_brutto'] = Common::formatPrice2($aTable['vat_7_brutto']);
        $aTable['vat_7_netto'] = Common::formatPrice2($aTable['vat_7_brutto'] / (1 + 7/100));
        $aTable['vat_7_currency'] = $aTable['vat_7_brutto'] - $aTable['vat_7_netto'];
        // przeliczanie dla stawki VAT 22 - sformatowane z '.' jako separator dziesietny
        $aTable['vat_22_brutto'] = Common::formatPrice2($aTable['vat_22_brutto']);
        $aTable['vat_22_netto'] = Common::formatPrice2($aTable['vat_22_brutto'] / (1 + 22/100));
        $aTable['vat_22_currency'] = $aTable['vat_22_brutto'] - $aTable['vat_22_netto'];*/
      }
    }
		// obliczanie sumy zamowienia - sformatowanie z ',' jako separator dziesietny
		$aTable['total_brutto'] = Common::formatPrice($aTable['total_brutto']);
		$aTable['total_netto'] = Common::formatPrice($aTable['total_netto']);
		$aTable['total_vat'] = Common::formatPrice($aTable['total_vat']);
    if (!empty($aVatStakes)) {
      foreach($aVatStakes as $iKey => $aVatItem) {
        // formatowanie wartosci stawki VAT 0 z ',' jako separator dziesietny
        $aTable['vat_'.$aVatItem.'_value'] = Common::formatPrice($aTable['vat_'.$aVatItem.'_netto']);
        $aTable['vat_'.$aVatItem.'_currency'] = Common::formatPrice($aTable['vat_'.$aVatItem.'_currency']);
        $aTable['show_vat_'.intval($aVatItem)]=true;
      }
    }
		/*// formatowanie wartosci stawki VAT 7 z ',' jako separator dziesietny
		$aTable['vat_7_value'] = Common::formatPrice($aTable['vat_7_netto']);
		$aTable['vat_7_currency'] = Common::formatPrice($aTable['vat_7_currency']);
		// formatowanie wartosci stawki VAT 22 z ',' jako separator dziesietny
		$aTable['vat_22_value'] = Common::formatPrice($aTable['vat_22_netto']);
		$aTable['vat_22_currency'] = Common::formatPrice($aTable['vat_22_currency']);*/
		return $aTable;
	} // end of getOrdersReport() method 
	
	
	/**
	 * Metoda generuje vat
	 *
	 * @global type $aConfig
	 * @global type $pSmarty
	 * @param type $iId
	 * @param type $bSecondInvoice
	 * @param type $iLp
	 * @return string 
	 */
	function getOrderVat($iId, $bSecondInvoice, $iLp, $sPaymentType){
	
		include_once('OrderItemRecount.class.php');
		$oOrderItemRecount = new OrderItemRecount(TRUE);
		$aOrderData = $oOrderItemRecount->getOrderPricesDetail($iId, $bSecondInvoice, $sPaymentType);
		$aOrder = $aOrderData['order'];
		$aOrder['lp'] = $iLp;

		if($bSecondInvoice){
			$aOrder['invoice_number'] = $aOrder['invoice2_id'];
			$aAddress = $this->getOrderInvoiceAddress($aOrder['id'], true);
		} else {
			$aOrder['invoice_number'] = $aOrder['invoice_id'];
			$aAddress = $this->getOrderInvoiceAddress($aOrder['id']);
		}
		
		if($aAddress['is_company'] == '1'){
			$aOrder['name'] = $aAddress['company'];
			$aOrder['nip'] = $aAddress['nip'];
		} else {
			$aOrder['name'] = $aAddress['surname'].' '.$aAddress['name'];
			$aOrder['nip'] = '&nbsp;';
		}
		
		return $aOrder;
	} // end of getInvoiceHtml() method
} // end of Module Class
