<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - dane sprzedajacego
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Module {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID strony zamowien
	var $iPageId;

	// ID wersji jezykowej
	var $iLangId;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;
		
		$this->sErrMsg = '';
		$this->iPageId = getModulePageId('m_zamowienia');
		$this->sModule = $_GET['module'];
		$this->iLangId = $_SESSION['lang']['id'];
		
		$sDo = '';		
		
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu i akcji
		if ($_SESSION['user']['type'] === 0) {
			// brak uprawnien do modulu, menu, strony
			showPrivsAlert($pSmarty);
			return;
		}
		
		switch ($sDo) {
			case 'update': $this->Update($pSmarty); break;
						
			default: $this->Edit($pSmarty); break;
		}
	} // end of Module() method
	
	
	/**
	 * Metoda aktualizuje w bazie danych krekord
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id aktualizowanego rekordu
	 * @return	void
	 */
	function Update(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Edit($pSmarty);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = Common::GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Edit($pSmarty);
			return;
		}
		$aValues = array(
			'name' => decodeString($_POST['name']),
			'invoice_name' => decodeString($_POST['invoice_name']),
			'paczkomaty_name' => decodeString($_POST['paczkomaty_name']),
			'street' => $_POST['street'],
			'number' => $_POST['number'],
			'number2' => trim($_POST['number2']) != '' ? $_POST['number2'] : 'NULL',
			'postal' => $_POST['postal'],
			'phone' => $_POST['phone'],
			'city' => $_POST['city'],
			'nip' => $_POST['nip'],
			'regon' => $_POST['regon'],
			'bank_name' => $_POST['bank_name'],
			'bank_account' => $_POST['bank_account'],
      'fedex_id' => $_POST['fedex_id'],
      'fedex_key' => $_POST['fedex_key'],
      'fedex_courier_id' => $_POST['fedex_courier_id'],
		);
		// pobranie anych
		$aData =& $this->getData();
		if (empty($aData)) {
			// Insert
			if (Common::Insert($aConfig['tabls']['prefix']."orders_seller_data",
												 $aValues,
												 '',
												 false) === false) {
				$bIsErr = true;
			}
		}
		else {
			// update
			if (Common::Update($aConfig['tabls']['prefix']."orders_seller_data",
												 $aValues) === false) {
				$bIsErr = true;
			}
		}
			
		if (!$bIsErr) {
			// rekord zostal zaktualizowany
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_ok'], false);
			// dodanie informacji do logow
			AddLog($aConfig['lang'][$this->sModule]['edit_ok'], false);
		}
		else {
			// rekord nie zostal zmieniony, 
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_ok']);
			AddLog($aConfig['lang'][$this->sModule]['edit_ok']);
		}
		$this->Edit($pSmarty);
	} // end of Update() funciton

	
	/**
	 * Metoda tworzy formularz edycji danych sklepu
	 *
	 * @param		object	$pSmarty
	 */
	function Edit(&$pSmarty) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		// pobranie z bazy danych edytowanego rekordu
		$aData =& $this->getData();
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		$sHeader = $aConfig['lang'][$this->sModule]['header'];
		
		$pForm = new FormTable('seller_data', $sHeader, array('action'=>phpSelf()), array('col_width'=>175), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');

		// nazwa sprzedawcy
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['seller_name'], $aData['name'], array('maxlength'=>128, 'style'=>'width: 350px;'), '', 'text');
		$pForm->AddText('invoice_name', $aConfig['lang'][$this->sModule]['seller_invoice_name'], $aData['invoice_name'], array('maxlength'=>128, 'style'=>'width: 350px;'), '', 'text');
		

		// adres sprzedawcy
		$pForm->AddRow($aConfig['lang'][$this->sModule]['seller_address'], $pForm->GetTextHTML('street', $aConfig['lang'][$this->sModule]['seller_street'], $aData['street'], array('maxlength'=>64, 'style'=>'width: 221px;')).'&nbsp;&nbsp;'.$pForm->GetTextHTML('number', $aConfig['lang'][$this->sModule]['seller_number'], $aData['number'], array('maxlength'=>8, 'style'=>'width: 50px;'),'','text',false).'&nbsp;&nbsp;/&nbsp;&nbsp;'.$pForm->GetTextHTML('number2', $aConfig['lang'][$this->sModule]['seller_number2'], $aData['number2'], array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'text', false), '', array(), array(), true);

		// kod pocztowy i miasto sprzedawcy
		$pForm->AddRow($aConfig['lang'][$this->sModule]['seller_postal_city'], $pForm->GetTextHTML('postal', $aConfig['lang'][$this->sModule]['seller_postal'], $aData['postal'], array('maxlength'=>6, 'style'=>'width: 50px;')).'&nbsp;&nbsp;'.$pForm->GetTextHTML('city', $aConfig['lang'][$this->sModule]['seller_city'], $aData['city'], array('maxlength'=>32, 'style'=>'width: 163px;')), '', array(), array(), true);

		// nip sprzedawcy
		$pForm->AddText('nip', $aConfig['lang'][$this->sModule]['seller_nip'], $aData['nip'], array('maxlength'=>15, 'style'=>'width: 150px;'));
		
		// regon sprzedawcy
		$pForm->AddText('regon', $aConfig['lang'][$this->sModule]['seller_regon'], $aData['regon'], array('maxlength'=>15, 'style'=>'width: 150px;'));
		

		// bank sprzedawcy
		$pForm->AddText('bank_name', $aConfig['lang'][$this->sModule]['seller_bank_name'], $aData['bank_name'], array('maxlength'=>64, 'style'=>'width: 280px;'));

		// konto sprzedawcy
		$pForm->AddText('bank_account', $aConfig['lang'][$this->sModule]['seller_bank_account'], $aData['bank_account'], array('maxlength'=>32, 'style'=>'width: 280px;'));

    $pForm->AddMergedRow(_('Paczkomaty'), array('class' => 'merged'));
    
    // nazwa paczkomaty
		$pForm->AddText('paczkomaty_name', $aConfig['lang'][$this->sModule]['seller_paczkomaty_name'], $aData['paczkomaty_name'], array('maxlength'=>255, 'style'=>'width: 350px;'), '', 'text');
		
		// regon sprzedawcy
		$pForm->AddText('phone', $aConfig['lang'][$this->sModule]['seller_phone'], $aData['phone'], array('maxlength'=>15, 'style'=>'width: 150px;'));

    $pForm->AddMergedRow('FedEx', array('class' => 'merged'));
    
    $pForm->AddText('fedex_id', _('ID konta płatnika'), $aData['fedex_id'], array('maxlength' => 10, 'style' => 'width: 130px;'), '', 'int', true);
    
    $pForm->AddText('fedex_key', _('Klucz dostępu'), $aData['fedex_key'], array('maxlength' => 32, 'style' => 'width: 330px;'), '', 'text', true);
    
    $pForm->AddText('fedex_courier_id', _('Numer kuriera'), $aData['fedex_courier_id'], array('maxlength' => 6, 'style' => 'width: 60px;'), '', 'int', true);
    
		// przyciski		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda pobiera dane sprzedawcy
	 * 
	 * @return	array ref	- tablica z danymi
	 */
	function &getData() {
		global $aConfig;
		
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_seller_data";
		return Common::GetRow($sSql);
	} // end of getData method
} // end of Module Class
?>