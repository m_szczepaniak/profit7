<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - metody platnosci
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Module {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID strony zamowien
	var $iPageId;

	// ID wersji jezykowej
	var $iLangId;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;
		
		$this->sErrMsg = '';
		$this->iPageId = getModulePageId('m_zamowienia');
		$this->sModule = $_GET['module'];
		$this->iLangId = $_SESSION['lang']['id'];
		
		$sDo = '';
		$iId = 0;
		
		
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu i akcji
		//if ($_SESSION['user']['type'] != 1) {
			// brak uprawnien do modulu, menu, strony
		//	showPrivsAlert($pSmarty);
		//	return;
		//}
		
		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'insert': $this->Insert($pSmarty); break;
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'change_ptype':
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iId); break;
						
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method
	
	
	/**
	 * Metoda wyswietla liste form platnosci
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		
		// zapamietanie opcji
		rememberViewState($this->sModule);
		
		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'ptype',
				'content'	=> $aConfig['lang'][$this->sModule]['list_ptype'],
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'transport',
				'content'	=> $aConfig['lang'][$this->sModule]['list_transport'],
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'published',
				'content'	=> $aConfig['lang'][$this->sModule]['list_published'],
				'sortable'	=> false,
				'width'	=> '40'
			),
			array(
				'db_field'	=> 'order_by',
				'content'	=> $aConfig['lang'][$this->sModule]['list_order_by'],
				'sortable'	=> false,
				'width'	=> '80'
			),
      array(
				'db_field'	=> 'own_cost',
				'content'	=> _('Koszt własny'),
				'sortable'	=> false,
				'width'	=> '80'
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> false,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> false,
				'width'	=> '85'
			),
      array(
				'db_field'	=> 'modified',
				'content'	=> $aConfig['lang']['common']['modified'],
				'sortable'	=> false,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'modified_by',
				'content'	=> $aConfig['lang']['common']['modified_by'],
				'sortable'	=> false,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."orders_payment_types
						 ORDER BY id";
		$iRowCount = intval(Common::GetOne($sSql));

		$pView = new View('attributes_groups', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
			// pobranie wszystkich rekordow
			$sSql = "SELECT A.id, A.ptype, B.name AS transport, A.published, A.order_by, A.own_cost, A.created, A.created_by,
        A.modified, A.modified_by
						 	 FROM ".$aConfig['tabls']['prefix']."orders_payment_types A
						 	 JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						 	 ON A.transport_mean=B.id
							 ORDER BY A.transport_mean,A.order_by
							 LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			foreach ($aRecords as $iKey => $mValue) {
				$aRecords[$iKey]['ptype'] = $aConfig['lang'][$this->sModule]['ptype_'.$aRecords[$iKey]['ptype']];
				$aRecords[$iKey]['published'] = $aConfig['lang'][$this->sModule]['published_'.$aRecords[$iKey]['published']];
			}
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'ptype' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											)
				)
			);
			
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('add')
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function
	

	/**
	 * Metoda usuwa wybrane rekordy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego rekordu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
		if (!empty($_POST['delete'])) {
			if (count($_POST['delete']) == 1) {
				$sErrPostfix = '0';
			}
			else {
				$sErrPostfix = '1';
			}
			
			// pobranie nazw usuwanych rekordow
			$sDel = $this->getItemsNames($_POST['delete']);
			
			// usuwanie
			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."orders_payment_types
							 WHERE id IN (".implode(',', $_POST['delete']).")";
			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
			
			if (!$bIsErr) {
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.$sErrPostfix],
												$sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
			else {
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_'.$sErrPostfix],
												$sDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton
	
	
	/**
	 * Metoda dodaje do bazy danych nowy rekord
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = Common::GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}
		
		if (!$this->paymentMethodExists($_POST['ptype'],$_POST['transport_mean'])) {
			$aValues = array(
				'ptype' => $_POST['ptype'],
				'transport_mean' => (int)$_POST['transport_mean'],
				'description' =>  nl2br(trim($_POST['description'])),
				'published' => isset($_POST['published']) ? '1' : '0',
				'cost' => Common::formatPrice2($_POST['cost']),
				'vat' => intval($_POST['vat']),
				'no_costs_from' => 'NULL', //Common::formatPrice2($_POST['no_costs_from']) > 0 ? Common::formatPrice2($_POST['no_costs_from']) : 'NULL',
				'time_border' => (int)$_POST['time_border'],
				'shipment_days_before' => (int)$_POST['shipment_days_before'],
				'shipment_days_after' => (int)$_POST['shipment_days_after'],
				'transport_days_before' => (int)$_POST['transport_days_before'],
				'transport_days_after' => (int)$_POST['transport_days_after'],
				'message_before' =>  nl2br(trim($_POST['message_before'])),
				'message_after' =>  nl2br(trim($_POST['message_after'])),
        'own_cost' =>  Common::formatPrice2(trim($_POST['own_cost'])),
				'max_order_value' =>  Common::formatPrice2(trim($_POST['max_order_value'])),
				'created' => 'NOW()',
				'created_by' => $_SESSION['user']['name'],
        'cart_calendar_legend' => $_POST['cart_calendar_legend'],
				'order_by' => intval($_POST['order_by']) > 0 ? intval($_POST['order_by']) : '#order_by#',
			);
			switch($_POST['ptype']) {
				case 'dotpay':
				case 'platnoscipl':
                case 'payu_bank_transfer':
					$_POST[$_POST['ptype']]['fee'] = Common::formatPrice2($_POST[$_POST['ptype']]['fee']);
					$aValues['pdata'] = base64_encode(serialize($_POST[$_POST['ptype']]));
				break;
				case 'card':
					$_POST['platnoscipl']['fee'] = Common::formatPrice2($_POST['platnoscipl']['fee']);
					$aValues['pdata'] = base64_encode(serialize($_POST['platnoscipl']));
				break;
				
				default:
					$aValues['pdata'] = 'NULL';
				break;
			}
				
			// dodanie
			if (Common::Insert($aConfig['tabls']['prefix']."orders_payment_types", $aValues, 'transport_mean='.$_POST['transport_mean'], false) === false) {
				$bIsErr = true;
			}
			
			if (!$bIsErr) {
				// rekord zostal dodany
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
												$aConfig['lang'][$this->sModule]['ptype_'.$_POST['ptype']]);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				$this->Show($pSmarty);
			}
			else {
				// rekord nie zostal dodany, 
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
												$aConfig['lang'][$this->sModule]['ptype_'.$_POST['ptype']]);
				$this->sMsg = GetMessage($sMsg);
				
				AddLog($sMsg);
				$this->AddEdit($pSmarty);
			}
		}
		else {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err2'],
											$aConfig['lang'][$this->sModule]['ptype_'.$_POST['ptype']]);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEdit($pSmarty);
		}
	} // end of Insert() funciton
	
	
	/**
	 * Metoda aktualizuje w bazie danych krekord
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id aktualizowanego rekordu
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}
		if (!$this->paymentMethodExists($_POST['ptype'],$_POST['transport_mean'], $iId)) {
			$aValues = array(
				'ptype' => $_POST['ptype'],
				'transport_mean' => (int)$_POST['transport_mean'],
				'description' =>  nl2br(trim($_POST['description'])),
				'published' => isset($_POST['published']) ? '1' : '0',
				'cost' => Common::formatPrice2($_POST['cost']),
				'vat' => intval($_POST['vat']),
				'no_costs_from' => 'NULL', //Common::formatPrice2($_POST['no_costs_from']) > 0 ? Common::formatPrice2($_POST['no_costs_from']) : 'NULL',
				'time_border' => (int)$_POST['time_border'],
				'shipment_days_before' => (int)$_POST['shipment_days_before'],
				'shipment_days_after' => (int)$_POST['shipment_days_after'],
				'transport_days_before' => (int)$_POST['transport_days_before'],
				'transport_days_after' => (int)$_POST['transport_days_after'],
				'message_before' =>  nl2br(trim($_POST['message_before'])),
				'message_after' =>  nl2br(trim($_POST['message_after'])),
        'own_cost' =>  Common::formatPrice2(trim($_POST['own_cost'])),
                'max_order_value' =>  Common::formatPrice2(trim($_POST['max_order_value'])),
        'cart_calendar_legend' => $_POST['cart_calendar_legend'],
				'modified' => 'NOW()',
				'modified_by' => $_SESSION['user']['name'],
				'order_by' => intval($_POST['order_by'])
			);
			switch($_POST['ptype']) {
				case 'dotpay':
				case 'platnoscipl':
				//	$_POST[$_POST['ptype']]['fee'] = Common::formatPrice2($_POST[$_POST['ptype']]['fee']);
					$aValues['pdata'] = base64_encode(serialize($_POST[$_POST['ptype']]));
				break;
				case 'card':
					$_POST['platnoscipl']['fee'] = Common::formatPrice2($_POST['platnoscipl']['fee']);
					$aValues['pdata'] = base64_encode(serialize($_POST['platnoscipl']));
				break;
				
				default:
					$aValues['pdata'] = 'NULL';
				break;
			}
			if (Common::Update($aConfig['tabls']['prefix']."orders_payment_types", $aValues, "id = ".$iId) === false) {
				$bIsErr = true;
			}
			
			if (!$bIsErr) {
				// rekord zostal zaktualizowany
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
												$aConfig['lang'][$this->sModule]['ptype_'.$_POST['ptype']]);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);				
				$this->Show($pSmarty);
			}
			else {
				// rekord nie zostal zmieniony, 
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza edycji
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
												$aConfig['lang'][$this->sModule]['ptype_'.$_POST['ptype']]);
				$this->sMsg = GetMessage($sMsg);
				
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iId);
			}
		}
		else {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err2'],
											$aConfig['lang'][$this->sModule]['ptype_'.$_POST['ptype']]);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEdit($pSmarty, $iId);
		}
	} // end of Update() funciton

	
	/**
	 * Metoda tworzy formularz dodawania / edycji
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID edytowanego rekordu
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if ($iId > 0) {
			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT ptype, transport_mean, pdata, cost, vat, description, no_costs_from,time_border,shipment_days_before,
											shipment_days_after, transport_days_before, transport_days_after, message_before, message_after, order_by, published,
                      own_cost, cart_calendar_legend, max_order_value
							 FROM ".$aConfig['tabls']['prefix']."orders_payment_types
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
			if (!empty($aData['pdata'])) {
				if($aData['ptype'] == 'card' || $aData['ptype'] == 'payu_bank_transfer'){
					$aData['platnoscipl'] = unserialize(base64_decode($aData['pdata']));
				} else {
					$aData[$aData['ptype']] = unserialize(base64_decode($aData['pdata']));
				}
			}
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		if (!empty($aData['discount'])) {
			$aData['discount'] = Common::formatPrice($aData['discount']);
		}
		if (empty($aData['order_by'])) {
			$aData['order_by'] = 0;
		}
		
		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['name']) && !empty($aData['name'])) {
			$sHeader .= ' "'.$aData['name'].'"';
		}
		
		$pForm = new FormTable('attributes_groups', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>175), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));
		
		// typ platnosci
		$aTypes = array(
			array('value' => '', 'label' => $aConfig['lang']['common']['choose']),
			array('value' => 'postal_fee', 'label' => $aConfig['lang'][$this->sModule]['ptype_postal_fee']),
			array('value' => 'bank_transfer', 'label' => $aConfig['lang'][$this->sModule]['ptype_bank_transfer']),
			array('value' => 'dotpay', 'label' => $aConfig['lang'][$this->sModule]['ptype_dotpay']),
			array('value' => 'platnoscipl', 'label' => $aConfig['lang'][$this->sModule]['ptype_platnoscipl']),
			array('value' => 'payu_bank_transfer', 'label' => 'przelew bankowy(PayU.pl)'),
			array('value' => 'card', 'label' => $aConfig['lang'][$this->sModule]['ptype_card']),
		//	array('value' => 'self_collect', 'label' => $aConfig['lang'][$this->sModule]['ptype_self_collect']),
			array('value' => 'bank_14days', 'label' => $aConfig['lang'][$this->sModule]['ptype_bank_14days'])
		);
		
		$pForm->AddSelect('ptype', $aConfig['lang'][$this->sModule]['ptype'], array('onchange'=>'ChangeObjValue(\'do\', \'change_ptype\'); this.form.submit();'), $aTypes, $aData['ptype']);
		
		$pForm->AddSelect('transport_mean', $aConfig['lang'][$this->sModule]['transport_means'], array('onchange'=>'ChangeObjValue(\'do\', \'add\'); this.form.submit();'), $this->getTransportMeans(), $aData['transport_mean']);

        $pForm->AddText('max_order_value', _('Maksymalna wartość zamówienia, do której metoda płatności jest aktywna'), $aData['max_order_value'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'float');

    $pForm->AddText('own_cost', _('Koszt własny'), $aData['own_cost'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'float');
    
		$pForm->AddTextArea('description',$aConfig['lang'][$this->sModule]['description'],br2nl($aData['description']),array('style'=>'width: 350px;', 'rows'=>8),1024,'','',false);
		
		$pForm->AddCheckBox('published', $aConfig['lang'][$this->sModule]['published'], array(), '', !empty($aData['published']), false);
		
		// rabat %
		// $pForm->AddText('discount', $aConfig['lang'][$this->sModule]['discount'], $aData['discount'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat', false);
		
		switch ($aData['ptype']) {
			case 'dotpay':
				$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['dotpay_section'], array('class'=>'merged', 'style'=>'padding-left: 180px;'));
				
				// prowizja w procentach - doliczana do kosztu transportu
			//	$pForm->AddText('dotpay[fee]', $aConfig['lang'][$this->sModule]['dotpay_fee'], Common::formatPrice3($aData['dotpay']['fee']), array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'float');
				
				// ID uzytkownika dotpay.pl
				$pForm->AddText('dotpay[id]', $aConfig['lang'][$this->sModule]['dotpay_id'], $aData['dotpay']['id'], array('maxlength'=>10, 'style'=>'width: 75px;'), '', 'uinteger');
				
				// NIP
				$pForm->AddText('dotpay[pin]', $aConfig['lang'][$this->sModule]['dotpay_pin'], $aData['dotpay']['pin'], array('maxlength'=>32, 'style'=>'width: 250px;'), '', 'text');
			break;	
			case 'card':
			case 'platnoscipl':
            case 'payu_bank_transfer':
				$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['platnoscipl_section'], array('class'=>'merged', 'style'=>'padding-left: 180px;'));
				
				// pos ID
				$pForm->AddText('platnoscipl[posid]', $aConfig['lang'][$this->sModule]['platnoscipl_posid'], $aData['platnoscipl']['posid'], array('maxlength'=>10, 'style'=>'width: 75px;'), '', 'uinteger');
				
				// klucze
				$pForm->AddText('platnoscipl[key1]', $aConfig['lang'][$this->sModule]['platnoscipl_key1'], $aData['platnoscipl']['key1'], array('maxlength'=>32, 'style'=>'width: 250px;'), '', 'text');
				$pForm->AddText('platnoscipl[key2]', $aConfig['lang'][$this->sModule]['platnoscipl_key2'], $aData['platnoscipl']['key2'], array('maxlength'=>32, 'style'=>'width: 250px;'), '', 'text');
				
				//pos auth key
				$pForm->AddText('platnoscipl[authkey]', $aConfig['lang'][$this->sModule]['platnoscipl_authkey'], $aData['platnoscipl']['authkey'], array('maxlength'=>32, 'style'=>'width: 250px;'), '', 'text');
			break;	
		}
		
		// koszt transportu
		$pForm->AddText('cost', $aConfig['lang'][$this->sModule]['cost'], Common::formatPrice3($aData['cost']), array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'float');
		// vat
		$pForm->AddText('vat', $aConfig['lang'][$this->sModule]['vat'], $aData['vat'], array('maxlength'=>2, 'style'=>'width: 50px;'), '', 'uint');
		
		// brak kosztow transportu od wartosci zamowienia
		//$pForm->AddText('no_costs_from', $aConfig['lang'][$this->sModule]['no_costs_from'], Common::formatPrice3($aData['no_costs_from']), array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'float');
		
		// kolejnosc wyswietlania na liscie metod platnosci w koszyku klienta
		$pForm->AddText('order_by', $aConfig['lang'][$this->sModule]['order_by'], (double) $aData['order_by'], array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'uinteger');
				
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['shipment_time_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		$pForm->AddText('time_border', $aConfig['lang'][$this->sModule]['time_border'], $aData['time_border'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['shipment_time_before_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		$pForm->AddText('shipment_days_before', $aConfig['lang'][$this->sModule]['shipment_days'], $aData['shipment_days_before'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
		$pForm->AddText('transport_days_before', $aConfig['lang'][$this->sModule]['transport_days'], $aData['transport_days_before'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
		$pForm->AddTextArea('message_before',$aConfig['lang'][$this->sModule]['message'],br2nl($aData['message_before']),array('style'=>'width: 350px;', 'rows'=>8),1024);
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['shipment_time_after_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		$pForm->AddText('shipment_days_after', $aConfig['lang'][$this->sModule]['shipment_days'], $aData['shipment_days_after'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
		$pForm->AddText('transport_days_after', $aConfig['lang'][$this->sModule]['transport_days'], $aData['transport_days_after'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
		$pForm->AddTextArea('message_after',$aConfig['lang'][$this->sModule]['message'],br2nl($aData['message_after']),array('style'=>'width: 350px;', 'rows'=>8),1024);
    
    $pForm->AddMergedRow(_('Legenda w koszyku przy kalendarzu'), array('class'=>'merged', 'style'=>'padding-left: 155px;'));
    $pForm->AddWYSIWYG('cart_calendar_legend', _('Legenda w koszyku przy kalendarzu'), $aData['cart_calendar_legend'],array('style'=>'width: 350px;', 'rows'=>8));
		
		// przyciski		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda zwraca liste grup atrybutow o przekazanych do niej Id
	 * 
	 * @param	array	$aIds	- Id rekordow dla ktorych zwrocone zostana nazwy
	 * @return	string	- ciag z nazwami
	 */
	function getItemsNames($aIds) {
		global $aConfig;
		$sList = '';
		
		// pobranie nazw usuwanych rekordow
		$sSql = "SELECT ptype
				 		 FROM ".$aConfig['tabls']['prefix']."orders_payment_types
					 	 WHERE id IN  (".implode(',', $aIds).")";
		$aRecords =& Common::GetAll($sSql);
		foreach ($aRecords as $aItem) {
			$sList .= '"'.$aConfig['lang'][$this->sModule]['ptype_'.$aItem['ptype']].'", ';
		}
		$sList = substr($sList, 0, -2);
		return $sList;
	} // end of getItemsNames() method
	
	function &getTransportMeans(){
		global $aConfig;

		$sSql = "SELECT id AS value, name AS label
				 		 FROM ".$aConfig['tabls']['prefix']."orders_transport_means
					 	 ORDER BY id";
		$aRecords=&Common::GetAll($sSql);
		$aItems = array(
			array('value' => '', 'label' => $aConfig['lang']['common']['choose'])
		);
		return array_merge($aItems,$aRecords);
	}
	/**
	 * Metoda sprawdza czy metoda platnosci juz istnieje
	 * 
	 * @param	string	$sType	- symbol metody platnosci
	 * @param integer $iTransport - id metody transportu
	 * @param	integer	$iPId	- Id platnosci dla ktorej pomijane jest sprawdzanie
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function paymentMethodExists($sType,$iTransport, $iPId=0) {
		global $aConfig;
		$sList = '';
		
		$sSql = "SELECT id
				 		 FROM ".$aConfig['tabls']['prefix']."orders_payment_types
					 	 WHERE ptype = '".$sType."'
					 	 				AND transport_mean = ".$iTransport.
					 	 			 ($iPId > 0 ? " AND id <> ".$iPId : '');
		return (int) Common::GetOne($sSql) > 0;
	} // end of paymentMethodExists() method
} // end of Module Class
?>