<?php

use Items\Confirmer;
use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\communicator\sources\Internal\streamsoft\MMdocument;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use LIB\Helpers\ArrayHelper;
use LIB\Supplies\StockSuppliceService;
use omniaCMS\lib\Products\ProductsStockReservations;

/**
 * Klasa przyjęcia dostawy
 * 
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @copyright (c) 2014, Arkadiusz Golba
 */
include_once('Common_confirm_items.class.php');
class Module__zamowienia__sc_confirm_hasnc_items extends Common_confirm_items {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// Id strony modulu
	var $iPageId;
	
	// katalog plikow log
	var $sLogsDir;
  
  /**
   *
   * @var DatabaseManager
   */
  public $pDbMgr;


  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function __construct(&$pSmarty, $bInit = true) {
    global $pDbMgr;
    
		$this->sErrMsg = '';
		$this->iPageId = getModulePageId('m_zamowienia');
		$this->sModule = $_GET['module'];
		$this->iLangId = $_SESSION['lang']['id'];
		$this->pDbMgr = $pDbMgr;
	
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
    
    if ($bInit == false) {
      return;
    }
		
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

    
		switch ($sDo) {
			case 'recreateMM' : $this->recreateMM($pSmarty);
			case 'save_items': $this->SaveItems($pSmarty, $iId); 				break;
			case 'details': 			$this->showDetails($pSmarty, $iId); 	break;
			case 'old_view': 			$this->ShowOld($pSmarty); 						break;
			default: 							$this->Show($pSmarty); 								break;
		}
	} // end Module() function


	/**
	 * Metoda tworzy formularz z informacjami na temat uzytkownika
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID konta uzytkownika
	 * @return	void
	 */
	function showDetails(&$pSmarty, $iId) {
		global $aConfig;

		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych szczegolow konta uzytkownika
		$sSql = "SELECT A.id, A.number, A.pack_number, A.saved_type, A.email, A.date_send, A.content, A.send_by, A.source, OSHA.fv_nr, OSHA.ident_nr, 
            (SELECT name FROM ".$aConfig['tabls']['prefix']."external_providers WHERE id=A.source) AS source_name
							 FROM ".$aConfig['tabls']['prefix']."orders_send_history AS A
               LEFT JOIN ".$aConfig['tabls']['prefix']."orders_send_history_attributes AS OSHA
                 ON A.id = OSHA.orders_send_history_id
							 WHERE A.id=".$iId;
		$aData =& Common::GetRow($sSql);
		$aData['source']=in_array($aData['source'], array(0,1,5))?($aData['source']==0?'Profit J':($aData['source']=='1'?'Profit E':'Profit M')):$aData['source_name'];
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['details_header']);

		$pForm = new FormTable('ordered_itm', _("Potwierdź zamówione pozycje"), array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), true);
		$pForm->AddHidden('do', 'save_items');
    
    if ($aData['pack_number'] == '') {
      $pForm->AddText("pack_number", _("Numer kuwety"), $aData['pack_number'], array('class' => 'important', 'style' => 'font-size: 45px;'), '', 'int', true);
    } else {
      $pForm->AddRow(_('Półka/kuweta'), $sContent);
    }
    
		// dane wiadomości
		if(!empty($aData['email'])){
			$pForm->AddRow($aLang['details_email'], Common::convert($aData['email']));
		}
    if ($aData['fv_nr'] != '') {
      $pForm->AddRow($aLang['fv_nr'], $aData['fv_nr'], '', array(), array('style'=>'color: red; font-size: 14px; font-weight: bold;'));
    }
		$pForm->AddRow($aLang['number'], $aData['number'], '', array(), array('style'=>'font-weight:bold; font-size:14px;'));
    if ($aData['ident_nr'] != '') {
      $pForm->AddRow($aLang['ident_nr'], $aData['ident_nr']);
    }
		$pForm->AddRow($aLang['details_date_send'], $aData['date_send']);
		$pForm->AddRow($aLang['details_send_by'], Common::convert($aData['send_by']));
		// dane do faktury
		$pForm->AddMergedRow($aLang['details_content_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		
			// wyszukiwanie ksiazek 
		$aBooks =& $this->getBooksNew($iId);
		if(!empty($aBooks)){
				$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_change_status']));
			}
		
	
		// wyswietlenie listy pasujacych uzytkownikow
		$pForm->AddMergedRow( $this->GetBooksList($pSmarty, $aBooks) );
		if(!empty($aBooks)){
				$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send2', $aConfig['lang'][$this->sModule]['button_change_status']));
			}
		

			
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('style'=>'float:right;','onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
    
    $sInput = '
    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" /></div>

    <script type="text/javascript" src="js/selectOrderItemQuantity.js"></script>
    <script type="text/javascript">
      $(function() {
        $("#pack_number").focus();
      });
    </script>
';
    $mmDoc = $sSql = "SELECT mm_doc FROM orders_send_history WHERE id = ".$iId;
    $result = Common::GetOne($sSql);

    $mmInfo = '';
    if (0 != $result) {
      $mmInfo = '<div><br><b style="color:red; border:3px solid red; padding:5px;">'.$aConfig['lang'][$this->sModule]['has_mm'].'</b></div>';
    }

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$mmInfo.$sInput.ShowTable($pForm->ShowForm()));
	} // end of Details() function
  
  
  /**
   * Metoda zwraca listę wszystkich źródeł
   * 
   * @return array
   */
  private function _getSources() {
    
    $aStatic = array(
        array('value' => '0', 'label' => 'Profit J'),
        array('value' => '1', 'label' => 'Profit E'),
        array('value' => '5', 'label' => 'Profit M'),
        array('value' => '51', 'label' => 'Stock'),
        array('value' => '52', 'label' => 'Profit X'),
    );
    return $aStatic;
  }// end of _getSources() method
  
  
	/**
	 * Metoda wyswietla liste zdefiniowanych kodow Premium SMS
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		$sFilterSql = '';
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		include_once('Form/FormTable.class.php');
		
		// zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['history'],
			'refresh'	=> true,
			'search'	=> true,
			'checkboxes'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'number',
				'content'	=> $aConfig['lang'][$this->sModule]['number'],
				'sortable'	=> true,
				'width' => '100'
			),
			array(
				'db_field'	=> 'pack_number',
				'content'	=> _('Półka/kuweta'),
				'sortable'	=> true,
				'width' => '100'
			),
			array(
				'db_field'	=> 'date_send',
				'content'	=> $aConfig['lang'][$this->sModule]['date_send'],
				'sortable'	=> true,
				'width' => '300'
			),
			array(
				'db_field'	=> 'source',
				'content'	=> $aConfig['lang'][$this->sModule]['source'],
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'saved_type',
				'content'	=> $aConfig['lang'][$this->sModule]['saved_type'],
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'fv_nr',
				'content'	=> $aConfig['lang'][$this->sModule]['fv_nr'],
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'send_by',
				'content'	=> $aConfig['lang'][$this->sModule]['send_by'],
				'sortable'	=> true,
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
		
    $sSql = "SELECT COUNT(DISTINCT A.id)
               FROM orders_send_history AS A
               JOIN orders_send_history_items AS OSHI
                ON OSHI.send_history_id = A.id
               JOIN orders_items AS OI
                ON OSHI.item_id = OI.id AND OI.`status` = '3' AND OI.`deleted` = '0' AND A.source = OI.source
               JOIN orders AS O
                ON OI.order_id = O.id AND order_status <> '5'
               WHERE A.status  = '3' ".
		// pobranie liczby wszystkich eksportow
                   (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = '.$_POST['f_source'] : '').
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND O.email LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(DISTINCT A.id)
               FROM orders_send_history AS A
               JOIN orders_send_history_items AS OSHI
                ON OSHI.send_history_id = A.id
               JOIN orders_items AS OI
                ON OSHI.item_id = OI.id AND OI.`status` = '3' AND OI.`deleted` = '0' AND A.source = OI.source
               JOIN orders AS O
                ON OI.order_id = O.id AND order_status <> '5'
               WHERE A.status  = '3'";
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('send_history', $aHeader, $aAttribs);
		
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// dodanie filtru grupy
		$pView->AddFilter('f_source', $aConfig['lang'][$this->sModule]['f_source'], array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])), $this->_getSources()), $_POST['f_source']);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			// pobranie zamówień bankowych
      $sSql = "SELECT DISTINCT A.id, A.number, A.pack_number, A.date_send, A.source, A.saved_type, A.status, OSHA.fv_nr, A.send_by, 
                  (SELECT name FROM ".$aConfig['tabls']['prefix']."external_providers WHERE id=A.source) AS source_name 
               FROM orders_send_history AS A
               LEFT JOIN orders_send_history_attributes AS OSHA
                ON A.id = OSHA.orders_send_history_id
               JOIN orders_send_history_items AS OSHI
                ON OSHI.send_history_id = A.id
               JOIN orders_items AS OI
                ON OSHI.item_id = OI.id AND OI.`status` = '3' AND OI.`deleted` = '0' AND A.source = OI.source
               JOIN orders AS O
                ON OI.order_id = O.id AND order_status <> '5'
               WHERE A.status  = '3'
              "
              .$sFilterSql.
                     (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = '.$_POST['f_source'] : '').
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND O.email LIKE \'%'.$_POST['search'].'%\'' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' A.date_send DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			//dump($aRecords);
			foreach($aRecords as $iKey=>$aItem){
				//$aRecords[$iKey]['source']='dd';
				$aRecords[$iKey]['saved_type'] = $aConfig['lang'][$this->sModule]['saved_'.$aItem['saved_type']];
				//$aRecords[$iKey]['status'] = $aConfig['lang'][$this->sModule]['status_'.$aItem['status']];
				$aRecords[$iKey]['source']=in_array($aRecords[$iKey]['source'], array(0,1,5,51,52))?($aConfig['lang'][$this->sModule]['source_'.intval($aRecords[$iKey]['source'])]):$aRecords[$iKey]['source_name'];
				$aRecords[$iKey]['date_send']=str_replace(' ', '<br />', $aRecords[$iKey]['date_send']);
                $aRecords[$iKey]['fv_nr'] = '<span style="color: red; font-size: 12px; font-weight: bold;">'.$aItem['fv_nr'].'</span>';
			}
			
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'status'	=> array(
					'show'	=> false
				),
				'source_name'	=> array(
					'show'	=> false
				),
				'date_send' => array ('link'	=> phpSelf(array('do' => 'details', 'id' => '{id}') ))
				,
				
				'number' => array ('link'	=> phpSelf(array('do' => 'details', 'id' => '{id}') )),
				
				'action' => array (
					'actions'	=> array ('details', 'publish_all'),
					'params' => array (
								'publish_all' => [
									'do' => 'recreateMM',
										'id' => '{id}'
								],
								'details'	=> array('id' => '{id}')
									),
					'show' => false	
				) 
 			);
            if ($_SESSION['user']['type'] !== 1) {
                $aColSettings['action']['actions'] = ['details'];
            }
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array()
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		//przycisk przejdz do starego widoku
		
		$sHeader = $aConfig['lang'][$this->sModule]['ordered_items_header'].($_POST['source'] != ''?' - '.$aConfig['lang'][$this->sModule]['source_'.$_POST['source']]:'');
	
		$pForm = new FormTable('old_view_button', '', array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'old_view');
		$pForm->AddRow('&nbsp;', '<div style="float:right;">'.
									$pForm->GetInputButtonHTML('get_csv', $aConfig['lang'][$this->sModule]['old_view'], array('onclick'=>'this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show().ShowTable($pForm->ShowForm()));
																				
	} // end of Show() function

	/**
	 * @param $iOSHId
	 * @return bool
	 */
	private function checkAllIsChanged($iOSHId)
	{
		$sql = "SELECT OSHI.id, OI.quantity from orders_send_history_items AS OSHI
				JOIN orders_items AS OI ON OI.id = OSHI.item_id
				where OSHI.send_history_id = $iOSHId
				";

		$currentData = $this->pDbMgr->GetAssoc('profit24', $sql);

		foreach($_POST['editable'] as $itemId => $changedItem){
			if ($currentData[$itemId] != $changedItem['currquantity']){
				return false;
			}
		}

		return true;
	}

	/**
	 * @return array
	 *
	 */
	private function prepareItemsToReduce($dataProvider, $iOSHId)
	{
		$itemsToReduce = [];

		foreach($_POST['editable'] as $itemId => $changedItem) {
			if ($changedItem['currquantity'] > 0){
				$itemsToReduce[$itemId] = $changedItem['currquantity'];
			}
		}

		if (empty($itemsToReduce)){
			return [];
		}

		$imploded = implode(',', array_keys($itemsToReduce));

		$oshiOrders = $dataProvider->findOrderByOshi($iOSHId, true);

//		$sql =
//			"select
//			PSS2.id, PSS2.reservation_to_reduce, PSS2.quantity, PSSTR.reservated_quantity, PSSTR.id AS psstr_id, OSHI.id as oshiid
//			from orders_send_history_items as OSHI
//			join orders_items AS OI ON OSHI.item_id = OI.id
//			join products_stock_reservations AS PSS ON OI.id = PSS.orders_items_id
//			right join products_stock_supplies_to_reservations AS PSSTR on PSS.id = PSSTR.products_stock_reservations_id
//			join products_stock_supplies as PSS2 on PSS2.id = PSSTR.products_stock_supplies_id
//			where PSS.order_id = %d
//			AND OSHI.id IN(%s)
//			";

		$sql = "
		SELECT
		PSSTR.*, OSHI.id as oshiid
		from orders_send_history_items as OSHI
		join orders_items AS OI ON OSHI.item_id = OI.id
		join products_stock_reservations AS PSS ON OI.id = PSS.orders_items_id
		JOIN products_stock_supplies_to_reservations AS PSSTR on PSS.id = PSSTR.products_stock_reservations_id
		where PSS.order_id = %d
		AND OSHI.id IN(%s)
		";

		$result = [];

		foreach($oshiOrders as $order){
			$supplies = $this->pDbMgr->GetAll('profit24', sprintf($sql, $order['id'], $imploded));

			if (true == empty($supplies)){
				continue;
			}

//			$supplis = ArrayHelper::toKeyValues('oshiid', $supplies);

			$newSupplies = [];
			foreach($supplies as $itemId => $supply){
//				$supply['reservated_quantity'] = $itemsToReduce[$itemId];
				$newSupplies[] = $supply;
			}

			$result[$order['id']] = $newSupplies;
		}

		return $result;
	}


  /**
   * Metoda zapisuje dostawę
   * 
   * @global array $aConfig
   * @param Smarty $pSmarty
   * @param int $iId
   * @return void
   */
	function SaveItems(&$pSmarty, $iOSHId=0) {
		global $aConfig;
		$bIsErr = false;
		$sErr = '';
    $iPackNumber = 0;

    $sSql = "SELECT pack_number, mm_doc FROM orders_send_history WHERE id = ".$iOSHId;
    $result = Common::GetRow($sSql);
    $sDbPackNumber = $result['pack_number'];
    $mmDoc = $result['mm_doc'];

    // sprawdzamy czy do tej pory w bazie był wpisany nr kuwety
    if ($sDbPackNumber == '') {

      if (isset($_POST['pack_number'])) {
        $iPackNumber = intval($_POST['pack_number']);
      }
      if ($iPackNumber <= 0) {
        $sMsg = _('Wprowadź numer kuwety !!');
        $this->sMsg = GetMessage($sMsg);
        $this->showDetails($pSmarty, $iOSHId);
        return;
      }
    }
    
		if(!empty($_POST['editable'])){
			foreach($_POST['editable'] as $sKey=>&$aElement){
				$aElement['weight'] = trim($aElement['weight']);
				if ($aElement['weight'] != "" && (!preg_match('/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/',$aElement['weight']) || $aElement['weight'] < 0 || $aElement['weight'] > 15.0)) {
						$sErr .= "<li>Waga</li>";
				}
			}
			if(!empty($sErr)){
				$sErr = $aConfig['lang']['form']['error_prefix'].
					'<ul class="formError">'.$sErr.'</ul>'.
					$aConfig['lang']['form']['error_postfix'];
				$this->sMsg .= GetMessage($sErr, true);
				// wystapil blad wypelnienia formularza
				// wyswietlenie formularza wraz z komunikatem bledu
				$this->showDetails($pSmarty, $iOSHId);
				return;
			}
		}
		$iI = 0;


//    $confirmer = new Confirmer();
//    $aInConfirmedItems = $confirmer->getOrderedItems($iOSHId, array('item_id', 'in_confirmed_quantity'));
//
//    $dd = [];
//
//    foreach($aInConfirmedItems as $item) {
//      $dd[] = $confirmer->proceedConfirmedItems($item);
//    }
//
//    $this->changeElementsStatusToLines($aInConfirmedItems);

		if (empty($_POST['delete'])) {
      $this->Show($pSmarty);
    }

//		$allIsChanged = $this->checkAllIsChanged($iOSHId);
		$dataProvider = new DataProvider($this->pDbMgr);
		$itemsToReduce = $this->prepareItemsToReduce($dataProvider, $iOSHId);

      foreach($_POST['delete'] as $sKey => $sVal) {
        unset($_POST['delete'][$sKey]);
        $_POST['delete'][$iI] = $sKey;
        $iI++;
      }
			Common::BeginTransaction();

		$dataProvider = new DataProvider($this->pDbMgr);

      // ok zapisujemy dokładnie ilość
      foreach($_POST['editable'] as $iOSHIId => $aData){
        if ($this->saveInItem($aData, $iOSHIId) === false) {
          $bIsErr = true;
        }
      }

      // zamknijmy tą dostawę
      if(!$bIsErr && $sDbPackNumber == '') {
        $mReturn = $this->closeInPackage($_POST['pack_number'], $iOSHId, false);
        if ($mReturn !== true) {
          $bIsErr = $mReturn;
        }
      }

		// TEST
//		if ($bIsErr == false && false == $allIsChanged){
//			$bIsErr = $this->updateReservationsByOshi($oshiOrders, $dataProvider);
//		}

		if ($bIsErr === false) {
			try {
				$this->updateReservationsToReduceByOshId($this->pDbMgr, $itemsToReduce);
			} catch (Exception $ex) {
				$bIsErr = true;
			}
		}
		// TEST

      
      if (!empty($_POST)) {
        $aValues = array(
            'post_array' => serialize($_POST),
        );
        Common::Update('orders_send_history', $aValues, ' id = '.$iOSHId);
      }
      
      
      // wysylanie dokumentu MM START
      require_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/Items/Confirmer.php');

      $mMdocument = new MMdocument($this->pDbMgr);
      $oshi = $mMdocument->getOSHI($iOSHId);


      $confirmer = new Confirmer();
      $changeResult = $confirmer->changeConfirmedQuantityItems($iOSHId);

        if (false === $changeResult){
            $this->sMsg .= GetMessage(_('Wystapil problem podczas zmian ilosci produktow'));
            $bIsErr = true;
        }

      if (0 == $mmDoc && true === $mMdocument->canBeCreated($oshi) && $bIsErr == false) {
        try {
          $mMdocument->doSendMM($oshi);
          $mMdocument->saveMMtoOrder(1, $iOSHId);

          $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['mm_doc_success'], false);
        } catch (Exception $e) {
          $this->sMsg .= GetMessage($e->getMessage());
          $bIsErr = true;
        }
      }

        if ($bIsErr === false) {

            Common::CommitTransaction();
            $sMsg = $aConfig['lang'][$this->sModule]['update_ok'];
            $this->sMsg .= GetMessage($sMsg, false);

    // wysylanie dokumentu MM STOP

            // dodanie informacji do logow
            AddLog($sMsg, false);
        }
        else {
            Common::RollbackTransaction();
        
        switch (intval($bIsErr)) {
          case -1:
            $sMsg = _('Wystąpił błąd podczas przyjmowania dostawy, podana kuweta jest zablokowana.');
          break;
        
          case -2:
            $sMsg = _('Wystąpił błąd podczas przyjmowania dostawy, taka kuweta nie istnieje.');
          break;
        
          case true:
          default :
            $sMsg = _('Wystąpił błąd ogólny podczas przyjmowania dostawy.');
          break;
        }
        
				$this->sMsg .= GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
			$this->Show($pSmarty);

	}// end of SaveItems() method


	public function recreateMM($pSmarty)
	{
		global $aConfig;
		$iOSHId = $_GET['id'];

		$mMdocument = new MMdocument($this->pDbMgr);
		$oshi = $mMdocument->getOSHI($iOSHId);

		Common::BeginTransaction();

		if (true === $mMdocument->canBeCreated($oshi)) {
			try {
				$mMdocument->doSendMM($oshi);
				$mMdocument->saveMMtoOrder(1, $iOSHId);

				$this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['mm_doc_success'], false);
				Common::CommitTransaction();
			} catch (Exception $e) {
				$this->sMsg .= GetMessage($e->getMessage());
				Common::RollbackTransaction();
			}
		}

		$this->Show($pSmarty);
	}

	/**
	 * @param $beforeReservationsByOshi
	 *
	 * @return bool
	 */
	private function updateReservationsByOshi($oshiOrders, $dataProvider)
	{
		foreach($oshiOrders as $orderId => $oldReservations){
//			if (false == $createresRes){
//				return false;
//			}
			$reservationManager = new ReservationManager($this->pDbMgr);
			try {
				$dataProvider->deleteReservationsByOrderId($orderId);

				$createresRes = $reservationManager->createLocalReservationsByOrderId($orderId);

				if (false == $createresRes) {
					continue;
				}

				$reservationManager->createStreamsoftReservation($oldReservations, $orderId, false);
			} catch(\Exception $e){
				$reservationManager->rollbackChanges();
				return true;
			}

		}

		return false;
	}

  /**
   * @param $pDbMgr
   * @param $oshi
   * @return bool
   * @throws Exception
   */
  private function updateReservationsToReduceByOshId($pDbMgr, $itemsToReduce)
  {
    $stockSuppliceService = new StockSuppliceService($pDbMgr);

	  if (empty($itemsToReduce)){
		  return false;
	  }

    foreach($itemsToReduce as $orderId => $orderSupplies){
      	$stockSuppliceService->updateReservationsToReduceByOrderId($orderId, $itemsToReduce[$orderId]);

		if ($this->proceedMoveERPReservations($orderSupplies) === false) {
			throw new \Exception("Wystapil blad podczas aktualizacji move_to_erp");
		}
    }

    return true;
  }
  
  
  /**
   * 
   * @param array $aConfirmed
   * @return boolean
   * @throws Exception
   */
  function proceedMoveERPReservations($aConfirmed) {
  
    foreach($aConfirmed as $item){
      try {
        $aOSHI = $this->pDbMgr->getTableRow('orders_send_history_items', $item['oshiid'], array('item_id'));
        $aOrder = $this->pDbMgr->getTableRow('orders_items', $aOSHI['item_id'], array('order_id'));
        $this->moveToERPReservation($aOrder['order_id'], $aOSHI['item_id'], '2');
      } catch (Exception $ex) {
        throw $ex;
      }
    }
    return true;
  }
  
  /**
   * Metoda przetwaza składowe zamówienia do dostawcy,
   * 1) zatwierdzenie składowych na liście do dostawcy
   * 2) zatwierdzenie składowych w zamówieniu od klienta
   * 
   * @param array $aConfirmed 
   * @param array $aEditable
   * @return boolean
   */
  function proceedOrdersItems($aConfirmed, $aEditable) {
    global $aConfig;
    $bIsErr = false;
    $aOrdersToRecount = array();
    $aParentsToRecount = array();

    foreach($aConfirmed as $iId){
      // pobierz id zamówienia
      $sSql = "SELECT A.order_id, A.product_id, A.weight, B.order_number, A.name, A.parent_id
              FROM orders_items A
              JOIN orders B
              ON B.id=A.order_id
              WHERE A.id = ".$iId;
      $aOrder = Common::GetRow($sSql);
      if ($aOrder['order_id'] > 0) {
        $aValues = array(
          'status' => '4'
        );

        if ($aOrder['weight'] != Common::formatPrice2($aEditable[$iId]['weight']) && Common::formatPrice2($aEditable[$iId]['weight']) > 0) {
          $aValues['weight'] = Common::formatPrice2($aEditable[$iId]['weight']);
          if (floatval($aValues['weight']) > 0 && $aValues['weight'] != $this->getProductWeight($aOrder['product_id'])) {
            $aPValues = array(
              'weight' => $aValues['weight']
            );
            if(Common::Update("products", $aPValues, "id = ".$aOrder['product_id']) === false){
              $bIsErr = true;					 
            }
            if(Common::Update("orders_items", $aPValues, ' (product_id = '.$aOrder['product_id'].' AND weight = "0.00" AND `deleted` = "0")') === false){
              $bIsErr = true;					 
            }
          }
        }
        if (Common::formatPrice2($aEditable[$iId]['weight']) == 0) {
          $sMsg = sprintf($aConfig['lang'][$this->sModule]['no_weight_err'],$aOrder['name'],$aOrder['order_number']);
          $this->sMsg .= GetMessage($sMsg);
          $bIsErr = true;
        }
        if (Common::Update("orders_items", $aValues,"id = ".$iId) === false) {
          $bIsErr = true;
        } else {
          // jeśli nie jest w liście zamowień do przeliczenia, dodaj
          if(!in_array($aOrder['order_id'],$aOrdersToRecount)){
            $aOrdersToRecount[] = $aOrder['order_id'];
          }

          // jesli jest składową pakietu to dodajemy do listy parrentów
          if(!in_array($aOrder['parent_id'],$aParentsToRecount)){
            $aParentsToRecount[] = $aOrder['parent_id'];
          }
        }
      } else {
        $bIsErr = true;
      }
    }


    /**
     * przeliczenie parentow
     * jezeli odznaczylismy skladowe pakietu to sprawdzamy czy pakiety sa kompletne
     * jesli tak to zmieniamy im status
    */
    if(!$bIsErr && !empty($aParentsToRecount)){
      foreach($aParentsToRecount as $iParentId){
        if(!$bIsErr && $iParentId>0){
          $sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE parent_id='.$iParentId.' AND status<>\'4\'';
          //echo Common::GetOne($sSql);
          if(Common::GetOne($sSql)==0) {
            //wszystkie zmatchowane zatem aktualizujemy
            $aValues2 = array(
              'status' => '4'
            );
            if(Common::Update("orders_items",$aValues2,"id = ".$iParentId) === false) {
              $bIsErr = true;
            }
          }
        }
      }
    }

    // przeliczenie zamówień
    if(!$bIsErr && !empty($aOrdersToRecount)){
      require_once('OrderRecount.class.php');
      $oOrderRecount = new OrderRecount();
      foreach($aOrdersToRecount as $iOrderId){
        if(!$bIsErr){
          if($oOrderRecount->recountOrder($iOrderId) === false){
            $bIsErr = true;
          }
        }
      }
    }
    
    return !$bIsErr;
  }// end of proceedOrdersItems() method
	
  /**
   * 
   * @param int $iOrderId
   * @param int $iOrderItemId
   */
  private function moveToERPReservation($iOrderId, $iOrderItemId, $cMode) {
	
    $oReservations = new ProductsStockReservations($this->pDbMgr);
    return $oReservations->moveToERPReservation($iOrderId, $iOrderItemId, $cMode);
  }
	
	
	/**
	 * Metoda wyswietla liste ksiazek z tablicy $aBooks
	 *
	 * @param		object	$pSmarty
	 * @param	array ref	$aBooks	- lista produktów
	 * @return	void
	 */
	function GetBooksList(&$pSmarty, &$aBooks) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/EditableView.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'checkboxes'	=> true,
			'editable' => true,
			'count_checkboxes' => true,
			'count_checkboxes_by' => 'quantity',
			'form'	=>	false,
      'count_header' => true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
		array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '20'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_cover']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_order_nr'],
				'width' => '120'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_name']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_quantity'],
				'width' => '50',
        'style' => 'font-size: 14px; font-weight: bold;'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_curr_quantity'],
        'width' => '50',
        'style' => 'font-size: 20px!important; font-weight: bold;'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_weight'],
				'width' => '30',
        'style' => 'font-size: 14px; font-weight: bold;'
			)
		);

		$pView = new EditableView('ordered_itm', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array(
			'id' => array(
				'show'	=> false
			),
      'ean_13' => array(
				'show'	=> false
			),
      'isbn_13' => array(
				'show'	=> false
			),
      'isbn_10' => array(
				'show'	=> false
			),  
			'currquantity'=> array(
					'editable'	=> true
			),
			'weight'=> array(
					'editable'	=> true
					),
		);
			// ustawienia dla kolumn edytowalnych
		$aEditableSettings = array(
			'weight' => array(
				'type' => 'text'
			),
			'currquantity' => array(
				'type' => 'text'
			)
		);
		// dodanie rekordow do widoku
		$pView->AddRecords($aBooks, $aColSettings,$aEditableSettings);
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all')
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		// przepisanie langa - info o braku rekordow
		$aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_books_to_show'];
				// walidacja js kolumn edytowalnych
		$sAdditionalScript='';
		foreach($aBooks as $iKey=>$aItem) {
			if($aItem['weight']=='0,00')	
				$sAdditionalScript.='document.getElementById(\'delete['.$aItem['id'].']\').disabled=true;
				';		
			}
		$sJS = '
		<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
		'.$sAdditionalScript.'
			$("#ordered_itm").submit(function() {
			  var strconfirm = confirm("Czy na pewno chcesz zamknąć dostawę ?");
              if (strconfirm == false)
              {
                return false;
              }
			//alert("this");
				var sErr = "";
				$(".heditable_input, .editable_input").each(function(){
					regex = /^editable\[([A-Za-z0-9_]+)\]\[([A-Za-z0-9_]+)\]$/;
					matches = regex.exec(this.name);
					if(matches[2] == "weight"){
						$(this).val($(this).val().replace(/&nbsp;/g, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
            var sWaga = parseFloat($(this).val().replace(",", "."));
            /* || $(this).val() < 0 !$(this).val().match(/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) ||  */
						if ($(this).val() != "" && (sWaga <= 0.00 || $(this).val() > 15.0)) {
							sErr += "\t- Waga\n";
						}
					}
					
				});
				if(sErr != ""){
					alert("'.$aConfig['lang']['form']['error_prefix'].'\n"+sErr+"'.$aConfig['lang']['form']['error_postfix'].'");
					return false;
				}
			});
		});
	// ]]>
</script>
		';
    $sJS = '
		<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
		'.$sAdditionalScript.'
			$("#ordered_itm").submit(function() {
			//alert("this");
				var sErr = "";
				$(".heditable_input, .editable_input").each(function(){
					regex = /^editable\[([A-Za-z0-9_]+)\]\[([A-Za-z0-9_]+)\]$/;
					matches = regex.exec(this.name);
					if(matches[2] == "weight"){
						$(this).val($(this).val().replace(/&nbsp;/g, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
						if ($(this).val() != "" && (!$(this).val().match(/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) || $(this).val() < 0 || $(this).val() > 15.0)) {
							sErr += "\t- Waga\n";
						}
					}
					
				});
				if(sErr != ""){
					alert("'.$aConfig['lang']['form']['error_prefix'].'\n"+sErr+"'.$aConfig['lang']['form']['error_postfix'].'");
					return false;
				}
			});
		});
	// ]]>
</script>
		';
		return $sJS.$pView->Show();
	} // end of GetBooksList() function
  

/**
 * metoda pobiera liste ksiazek dla wygenerowanego wydruku
 * @return array - lista ksiazek
 */
	function &getBooksNew($iId) {
		global $aConfig;
		$aOrders = array();

			// ksiazek spelniajacych zadane kryteria dla danego wydruku
			$sSql = "SELECT O.id, PI.photo, R.order_number, R.id AS order_id, 
											A.isbn, A.name, A.quantity, '0' AS currquantity, A.weight, A.publisher, A.authors, A.publication_year, A.edition,
                      PI.directory, P.ean_13, P.isbn_13, P.isbn_10
							 FROM ".$aConfig['tabls']['prefix']."orders R, ".$aConfig['tabls']['prefix']."orders_items A
							 JOIN ".$aConfig['tabls']['prefix']."orders_send_history_items O
                 ON O.item_id=A.id
							 JOIN ".$aConfig['tabls']['prefix']."orders_send_history AS SH
								 ON O.send_history_id = ".$iId." AND SH.source = A.source
               LEFT JOIN products AS P
                ON A.product_id = P.id
               LEFT JOIN products_images AS PI
                ON PI.product_id = A.product_id
							 WHERE 
                    A.status = '3'
                AND A.deleted = '0'
                AND A.packet = '0'
                AND R.order_status <> '5'
                AND R.id = A.order_id
                  
               GROUP BY A.id
							 ORDER BY IF(saved_type='1' || saved_type='3', A.name, A.order_id) ASC
              ";
			$aBooks = Common::GetAll($sSql);
			foreach($aBooks as $iKey=>$aItem){
				$aBooks[$iKey]['name'] = $aItem['name'].
											($aItem['isbn']?'<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="'.$aItem['isbn'].'" item_id="'.$aItem['id'].'">'.$aItem['isbn'].'</span>':'').
                      ($aItem['ean_13'] && $aItem['ean_13'] != $aItem['isbn'] ?'<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="'.$aItem['ean_13'].'" item_id="'.$aItem['id'].'">'.$aItem['ean_13'].'</span>':'').
                      ($aItem['isbn_13'] && $aItem['isbn_13'] != $aItem['isbn'] && $aItem['isbn_13'] != $aItem['ean_13'] ?'<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="'.$aItem['isbn_13'].'" item_id="'.$aItem['id'].'">'.$aItem['isbn_13'].'</span>':'').
                      ($aItem['isbn_10'] && $aItem['isbn_10'] != $aItem['isbn'] ?'<br /><span style="font-weight: bold; color: red;" class="isbn" isbn="'.$aItem['isbn_10'].'" item_id="'.$aItem['id'].'">'.$aItem['isbn_10'].'</span>':'').
											($aItem['publication_year']?'<br />'.$aItem['publication_year']:'').($aItem['edition']?' wyd. '.$aItem['edition']:'').
											($aItem['publisher']?'<br />'.$aItem['publisher']:'').
											($aItem['authors']?'<br />'.$aItem['authors']:'').'</span>';
        
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aItem['directory'].'/__t_'.$aItem['photo'])){
          $aBooks[$iKey]['photo'] = '<img src="/images/photos/'.$aItem['directory'].'/__t_'.$aItem['photo'].'" />';	
				} else {
          $aBooks[$iKey]['photo'] = ' - ';
        }
				unset($aBooks[$iKey]['isbn']);
				unset($aBooks[$iKey]['publication_year']);
				unset($aBooks[$iKey]['edition']);
				unset($aBooks[$iKey]['publisher']);
				unset($aBooks[$iKey]['authors']);
        unset($aBooks[$iKey]['directory']);
				$sPrefix = '';
				$aBooks[$iKey]['weight'] = Common::formatPrice($aItem['weight']);
				$aBooks[$iKey]['order_number']='<a href="admin.php?frame=main&module_id=38&module=m_zamowienia&do=details&ppid=0&id='.$aBooks[$iKey]['order_id'].'">'.$aBooks[$iKey]['order_number'].'</a>';
				unset($aBooks[$iKey]['order_id']);
			}

		
		return $aBooks;
	} // end of getBooks() method
	
	
	function &getBookData($iId){
		global $aConfig;
		$sSql = "SELECT A.id, A.isbn, A.name, B.name AS publisher, A.publication_year
							 FROM ".$aConfig['tabls']['prefix']."products A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers AS B
							 ON B.id = A.publisher_id
							 WHERE A.id = ".$iId;
			return Common::GetRow($sSql);
	}
	
	function setOrderedItems($sItems){
		global $aConfig;
		if(!empty($sItems)){
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders_items
								SET status='2'
								WHERE id IN (".$sItems.") AND status='1'";
			if(Common::Query($sSql) === false){
				return false;
			}
			return true;
		}
		return false; 
	}
	
	/**
	 * Metoda pobiera wagę produktu z tabeli products
	 * @param $iProductId - id produktu
	 * @return float - waga
	 */
	function getProductWeight($iProductId){
		global $aConfig;
		$sSql = "SELECT weight
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id = ".$iProductId;
		return Common::GetOne($sSql);
	} // end of getProductWeight() method
	
} // end of Module Class