<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - dane sprzedajacego
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID strony zamowien
	var $iPageId;

	// ID wersji jezykowej
	var $iLangId;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iPageId = getModulePageId('m_zamowienia');
		$this->sModule = $_GET['module'];
		$this->iLangId = $_SESSION['lang']['id'];

		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu i akcji
    /*
		if ($_SESSION['user']['type'] === 0) {
			// brak uprawnien do modulu, menu, strony
			showPrivsAlert($pSmarty);
			return;
		}
     */

    if ($_SESSION['user']['type'] === 0) {
      if (!hasModulePrivileges($_GET['module_id'])){
        showPrivsAlert($pSmarty);
      return;
      }
		}

		switch ($sDo) {
			case 'update': $this->Update($pSmarty); break;

			default: $this->Edit($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda aktualizuje w bazie danych krekord
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id aktualizowanego rekordu
	 * @return	void
	 */
	function Update(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Edit($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = Common::GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Edit($pSmarty);
			return;
		}
		$aValues = array(
			'azymut_limit_price' => Common::formatPrice2($_POST['azymut_limit_price']),
            'super_siodemka_limit_price' => Common::formatPrice2($_POST['super_siodemka_limit_price']),
            'ateneum_limit_price' => Common::formatPrice2($_POST['ateneum_limit_price']),
            'platon_limit_price' => Common::formatPrice2($_POST['platon_limit_price']),
            'panda_limit_price' => Common::formatPrice2($_POST['panda_limit_price']),
            'show_completion_date' => isset($_POST['show_completion_date']) ? '1' : '0',
            'sort_by_popular' => isset($_POST['sort_by_popular']) ? '1' : '0'
		);
		// pobranie anych
		$aData =& $this->getData();
		if (empty($aData)) {
			// Insert
			if (Common::Insert($aConfig['tabls']['prefix']."orders_magazine_settings",
												 $aValues,
												 '',
												 false) === false) {
				$bIsErr = true;
			}
		}
		else {
			// update
			if (Common::Update($aConfig['tabls']['prefix']."orders_magazine_settings",
												 $aValues, ' id = 1') === false) {
				$bIsErr = true;
			}
		}

		if (!$bIsErr) {
			// rekord zostal zaktualizowany
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_ok'], false);
			// dodanie informacji do logow
			AddLog($aConfig['lang'][$this->sModule]['edit_ok'], false);
		}
		else {
			// rekord nie zostal zmieniony,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_ok']);
			AddLog($aConfig['lang'][$this->sModule]['edit_ok']);
		}
		$this->Edit($pSmarty);
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz edycji danych sklepu
	 *
	 * @param		object	$pSmarty
	 */
	function Edit(&$pSmarty) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych edytowanego rekordu
		$aData =& $this->getData();

		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header'];

		$pForm = new FormTable('seller_data', $sHeader, array('action'=>phpSelf()), array('col_width'=>420), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');

		// nazwa sprzedawcy
        $pForm->AddMergedRow($aConfig['lang'][$this->sModule]['limit_price_info'], array('class'=>'merged'));
		$pForm->AddText('azymut_limit_price', $aConfig['lang'][$this->sModule]['azymut_limit_price'], Common::formatPrice($aData['azymut_limit_price']), array('maxlength'=>10, 'style'=>'width: 64px;'), '', 'text');
        $pForm->AddText('super_siodemka_limit_price', $aConfig['lang'][$this->sModule]['super_siodemka_limit_price'], Common::formatPrice($aData['super_siodemka_limit_price']), array('maxlength'=>10, 'style'=>'width: 64px;'), '', 'text');
        $pForm->AddText('ateneum_limit_price', $aConfig['lang'][$this->sModule]['ateneum_limit_price'], Common::formatPrice($aData['ateneum_limit_price']), array('maxlength'=>10, 'style'=>'width: 64px;'), '', 'text');
        $pForm->AddText('platon_limit_price', _('Platon'), Common::formatPrice($aData['platon_limit_price']), array('maxlength'=>10, 'style'=>'width: 64px;'), '', 'text');
        $pForm->AddText('panda_limit_price', _('Panda'), Common::formatPrice($aData['panda_limit_price']), array('maxlength'=>10, 'style'=>'width: 64px;'), '', 'text');
        $pForm->AddCheckBox('show_completion_date', _('Pokaż datę na zbieraniu'), array(), '', !empty($aData['show_completion_date']), false);
        $pForm->AddCheckBox('sort_by_popular', _('Zmień mechanizm zbierania wg Ilości powtarzających się tytułów w zamówieniach do zebrania'), array(), '', !empty($aData['sort_by_popular']), false);

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function


	/**
	 * Metoda pobiera dane sprzedawcy
	 *
	 * @return	array ref	- tablica z danymi
	 */
	function &getData() {
		global $aConfig;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_magazine_settings";
		return Common::GetRow($sSql);
	} // end of getData method
} // end of Module Class
?>