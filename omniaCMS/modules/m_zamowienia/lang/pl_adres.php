<?php
/**
* Plik jezykowy modulu 'Zamowienia'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_zamowienia_adres']['company']	= 'Firma';
$aConfig['lang']['mod_m_zamowienia_adres']['nip']	= 'NIP';
$aConfig['lang']['mod_m_zamowienia_adres']['address']	= 'Adres';
$aConfig['lang']['mod_m_zamowienia_adres']['postal']	= 'Kod pocztowy';
$aConfig['lang']['mod_m_zamowienia_adres']['namesurname']	= 'Imię i nazwisko';

?>