<?php
/**
 * Plik jezykowy dla interfejsu modulu 'Zamowienia' Panelu Administracyjnego
 * jezyk polski
 *
 * @author Marcin Korecki <m.korecki@omnia.pl>
 * @version 1.0
 *
 */
//Finanse	


/*---------- lista zamowien */
;


//do filtrów
$aConfig['lang']['m_oferta_produktowa']['shipment_0'] = "Od ręki";
$aConfig['lang']['m_oferta_produktowa']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['m_oferta_produktowa']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['m_oferta_produktowa']['shipment_3'] = "tylko na zamówienie";

$aConfig['lang']['m_oferta_produktowa']['status_-1'] = "nowe - zapo";
$aConfig['lang']['m_oferta_produktowa']['status_niedost'] = "niedost.";
$aConfig['lang']['m_oferta_produktowa']['status_0'] = "nowe";
$aConfig['lang']['m_oferta_produktowa']['status_1'] = "w realizacji";
$aConfig['lang']['m_oferta_produktowa']['status_2'] = "skompletowane";
$aConfig['lang']['m_oferta_produktowa']['status_3'] = "zatwierdzone";
$aConfig['lang']['m_oferta_produktowa']['status_4'] = "zrealizowane";
$aConfig['lang']['m_oferta_produktowa']['status_5'] = "anulowane";
$aConfig['lang']['m_oferta_produktowa']['list'] = "Lista produktów";
$aConfig['lang']['m_oferta_produktowa']['f_published'] = "Stan publikacji";
$aConfig['lang']['m_oferta_produktowa']['f_all'] = "Wszystkie";
$aConfig['lang']['m_oferta_produktowa']['f_published'] = "Opublikowane";
$aConfig['lang']['m_oferta_produktowa']['f_not_published'] = "Nieopublikowane";
$aConfig['lang']['m_oferta_produktowa']['f_author'] = "Autor";
$aConfig['lang']['m_oferta_produktowa']['f_isbn'] = "ISBN";
$aConfig['lang']['m_oferta_produktowa']['f_status'] = "Status";
$aConfig['lang']['m_oferta_produktowa']['f_publ_status'] = "Stan";
$aConfig['lang']['m_oferta_produktowa']['no_attributes_groups'] = "Dla produktów tej kategorii nie ma ustanowionych żadnych grup atrybutów";
$aConfig['lang']['m_oferta_produktowa']['f_category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa']['f_publisher'] = "Wydawca";
$aConfig['lang']['m_oferta_produktowa']['f_series'] = "Seria";
$aConfig['lang']['m_oferta_produktowa']['f_language'] = "Język";
$aConfig['lang']['m_oferta_produktowa']['f_orig_language'] = "Język oryg.";
$aConfig['lang']['m_oferta_produktowa']['f_binding'] = "Oprawa";
$aConfig['lang']['m_oferta_produktowa']['f_reviewed'] = "Przejrzane";
$aConfig['lang']['m_oferta_produktowa']['f_not_reviewed'] = "Nie przejrzane";
$aConfig['lang']['m_oferta_produktowa']['f_category_btn'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa']['f_source'] = "Akt.źr";
$aConfig['lang']['m_oferta_produktowa']['f_shipment'] = "Czas wysyłki";
$aConfig['lang']['m_oferta_produktowa']['f_source_internalj'] = "Profit J";
$aConfig['lang']['m_oferta_produktowa']['f_source_internaln'] = "Profit E";
$aConfig['lang']['m_oferta_produktowa']['f_source_azymut'] = "Azymut";
$aConfig['lang']['m_oferta_produktowa']['f_source_ateneum'] = "Ateneum";
$aConfig['lang']['m_oferta_produktowa']['f_source_abe'] = "ABE";
$aConfig['lang']['m_oferta_produktowa']['f_source_helion'] = "Helion";
$aConfig['lang']['m_oferta_produktowa']['f_source_dictum'] = "Dictum";
$aConfig['lang']['m_oferta_produktowa']['f_source_siodemka'] = "Siodemka";
$aConfig['lang']['m_oferta_produktowa']['f_source_olesiejuk'] = "Olesiejuk";
$aConfig['lang']['m_oferta_produktowa']['f_image'] = "Z okładką";
$aConfig['lang']['m_oferta_produktowa']['f_not_image'] = "Bez okładki";
$aConfig['lang']['m_oferta_produktowa']['f_year'] = "Rok publikacji";
$aConfig['lang']['m_oferta_produktowa']['f_yearto'] = "do";
$aConfig['lang']['m_oferta_produktowa']['f_type'] = "Typ";
$aConfig['lang']['m_oferta_produktowa']['f_packet'] = "Pakiet";
$aConfig['lang']['m_oferta_produktowa']['f_regular_product'] = "Książka";
//koniec do filtrow

$aConfig['lang']['m_zamowienia']['internal_status']	= 'Status wewnętrzny';
$aConfig['lang']['m_zamowienia']['is_modified'] = 'Zmodyfikowane';
$aConfig['lang']['m_zamowienia']['modified'] = 'Zmodyfikowano';
$aConfig['lang']['m_zamowienia']['modified_by'] = 'Zmodyfikowano przez';
$aConfig['lang']['m_zamowienia']['transport_option_id'] = 'Opcje transportu';
$aConfig['lang']['m_zamowienia']['transport_option_symbol'] = 'Symbol opcji transportu';
$aConfig['lang']['m_zamowienia']['locked_auto_order'] = "Blokada automatycznego odznaczania zamówienia";
$aConfig['lang']['m_zamowienia']['mm_has_been_accepted'] = "Dokument MM został zaakceptowany";
$aConfig['lang']['m_zamowienia']['mm_not_exists'] = "Dokument MM nie został wystawiony dla tego zamówienia";
$aConfig['lang']['m_zamowienia']['source'] = "Aktualne źródło";
$aConfig['lang']['m_zamowienia']['source_0'] = "Profit J";
$aConfig['lang']['m_zamowienia']['source_1'] = "Profit E";
$aConfig['lang']['m_zamowienia']['source_2'] = "Azymut";
$aConfig['lang']['m_zamowienia']['source_3'] = "ABE";
$aConfig['lang']['m_zamowienia']['source_4'] = "Helion";

$aConfig['lang']['m_zamowienia']['name']	= 'Imię';
$aConfig['lang']['m_zamowienia']['invoice_name']	= 'Nazwa';
$aConfig['lang']['m_zamowienia']['surname']	= 'Nazwisko';
$aConfig['lang']['m_zamowienia']['company']	= 'Firma';
$aConfig['lang']['m_zamowienia']['street']	= 'Ulica';
$aConfig['lang']['m_zamowienia']['number']	= 'Nr domu';
$aConfig['lang']['m_zamowienia']['number']	= 'Nr domu:';
$aConfig['lang']['m_zamowienia']['number2']	= 'Nr lokalu';
$aConfig['lang']['m_zamowienia']['number2_info']	= 'Nr lokalu - pole nieobowiązkowe';
$aConfig['lang']['m_zamowienia']['postal']	= 'Kod';
$aConfig['lang']['m_zamowienia']['phone']	= 'Nr telefonu';
$aConfig['lang']['m_zamowienia']['city']	= 'Miejscowość';
$aConfig['lang']['m_zamowienia']['nip']	= 'NIP';
$aConfig['lang']['m_zamowienia']['email']	= 'Email';
$aConfig['lang']['m_zamowienia']['send_info_mail']	= 'Wyślij email do użytkownika';
$aConfig['lang']['m_zamowienia']['back_ready_list']	= "Przywróć zamówienie, ponownie na tramwaj \naby mogło pojawić się na kolejnej liście do zebrania";

$aConfig['lang']['m_zamowienia']['renouncement']	= 'Odstąpienie';

$aConfig['lang']['m_zamowienia']['search_in'] = "szukaj wew. Zam.";
$aConfig['lang']['m_zamowienia']['list'] = "Lista zamówień";
$aConfig['lang']['m_zamowienia']['list_0'] = "Lista zamówień - Przegląd";
$aConfig['lang']['m_zamowienia']['list_1'] = "Lista zamówień - Nowe";
$aConfig['lang']['m_zamowienia']['list_2'] = "Lista zamówień - Częściowe";
$aConfig['lang']['m_zamowienia']['list_3'] = "Lista zamówień - Do realizacji";
$aConfig['lang']['m_zamowienia']['list_4'] = "Lista zamówień - Skompletowane";
$aConfig['lang']['m_zamowienia']['list_5'] = "Lista zamówień - Wysyłka";
$aConfig['lang']['m_zamowienia']['list_6'] = "Lista zamówień - Odbiór osobisty";
$aConfig['lang']['m_zamowienia']['list_7'] = "Lista zamówień - Faktury";
$aConfig['lang']['m_zamowienia']['list_8'] = "Lista zamówień - Zatwierdzone";
$aConfig['lang']['m_zamowienia']['list_8_1'] = "Lista zamówień - FedEx";
$aConfig['lang']['m_zamowienia']['list_8_3'] = "Lista zamówień - Paczkomaty";
$aConfig['lang']['m_zamowienia']['list_8_4'] = "Lista zamówień - Poczta-Polska";
$aConfig['lang']['m_zamowienia']['list_8_5'] = "Lista zamówień - RUCH";
$aConfig['lang']['m_zamowienia']['list_8_6'] = "Lista zamówień - Orlen";
$aConfig['lang']['m_zamowienia']['list_8_7'] = "Lista zamówień - TBA";
$aConfig['lang']['m_zamowienia']['list_-9999'] = "Lista zamówień - Zapowiedzi";
$aConfig['lang']['m_zamowienia']['list_9'] = "Lista zamówień - Zrealizowane";
$aConfig['lang']['m_zamowienia']['list_A'] = "Lista zamówień - Anulowane";
$aConfig['lang']['m_zamowienia']['list_-1'] = "Lista zamówień - Wyszukiwanie po tytule oraz ISBN";
$aConfig['lang']['m_zamowienia']['list_by_user'] = " dla użytkownika %s";
$aConfig['lang']['m_zamowienia']['button_change_conditions'] = "Zmień warunki dla statystyk";

$aConfig['lang']['m_zamowienia']['f_payment_date_from'] = "Zapłacono od";
$aConfig['lang']['m_zamowienia']['f_payment_date_to'] = "do";
$aConfig['lang']['m_zamowienia']['f_date_from'] = "Złożono od";
$aConfig['lang']['m_zamowienia']['f_date_to'] = "do";
$aConfig['lang']['m_zamowienia']['f_status_4_from'] = "Zreal. od";
$aConfig['lang']['m_zamowienia']['f_status'] = "Status";
$aConfig['lang']['m_zamowienia']['f_internal_status'] = "Status wewn.";
$aConfig['lang']['m_zamowienia']['f_order_items_shipment_availablity'] = "Wszystkie składowe";
$aConfig['lang']['m_zamowienia']['f_payment_status'] = "Status płatności";
$aConfig['lang']['m_zamowienia']['f_balance'] = "Bilans płatniczy";
$aConfig['lang']['m_zamowienia']['f_all_statuses'] = "Wszystkie";
$aConfig['lang']['m_zamowienia']['f_all'] = "Wszystkie";

$aConfig['lang']['m_zamowienia']['f_transport'] = "Metoda transportu";
$aConfig['lang']['m_zamowienia']['f_website_id'] = "Serwis";
$aConfig['lang']['m_zamowienia']['f_payment'] = "Metoda pł.";
$aConfig['lang']['m_zamowienia']['f_second_payment'] = "Druga metoda pł.";
$aConfig['lang']['m_zamowienia']['f_all_ptypes'] = "Wszystkie metody";


$aConfig['lang']['m_zamowienia']['user_no_data'] = "bez rejestracji";

$aConfig['lang']['m_zamowienia']['list_order_id'] = "Numer";
$aConfig['lang']['m_zamowienia']['list_user'] = "Zamawiający";
$aConfig['lang']['m_zamowienia']['list_total_cost'] = 'Wartość';
$aConfig['lang']['m_zamowienia']['list_value_brutto'] = "Brutto";
$aConfig['lang']['m_zamowienia']['list_order_date'] = "Złożono";
$aConfig['lang']['m_zamowienia']['list_activation_date'] = "Aktywowano";
$aConfig['lang']['m_zamowienia']['list_payment'] = "Transp/Płatno";
$aConfig['lang']['m_zamowienia']['list_status'] = "Status";
$aConfig['lang']['m_zamowienia']['no_items'] = "Brak złożonych zamówień";
$aConfig['lang']['m_zamowienia']['attachments'] = 'Załączniki:';
$aConfig['lang']['m_zamowienia']['list_payment_date'] = 'Opłacono';

$aConfig['lang']['m_zamowienia']['completed'] = 'Skompletowane';
$aConfig['lang']['m_zamowienia']['list_completed'] = 'Kompl.';
$aConfig['lang']['m_zamowienia']['order_completed_err'] = "Nie udało się zmienić skompletowania zamówienia nr %d!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['order_completed_ok'] = "Zmieniono status skompletowania  zamówienia nr %d";

$aConfig['lang']['m_zamowienia']['add_item_err'] = "Nie udało się dodać produktu \"%s\" do zamówienia!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['add_item_ok'] = "Dodano produkt \"%s\" do zamówienia";

$aConfig['lang']['m_zamowienia']['previous_book_status_err'] = "Nie mozna zmienić statusu książki na wcześniejszy!";
$aConfig['lang']['m_zamowienia']['no_selected_source'] = "Nie wybrano poprawnie źródeł książek!";
$aConfig['lang']['m_zamowienia']['no_weight_err'] = "Brak ustawionej wagi produktów!";

$aConfig['lang']['m_zamowienia']['status_-1'] = "nowe - zapo";
$aConfig['lang']['m_zamowienia']['status_0'] = "nowe";
$aConfig['lang']['m_zamowienia']['status_0_disabled'] = "nowe - nie aktywne";
$aConfig['lang']['m_zamowienia']['internal_status_0'] = "Nowe - nie aktywne";
$aConfig['lang']['m_zamowienia']['internal_status_5'] = "Wysyłka";
$aConfig['lang']['m_zamowienia']['status_niedost'] = "niedost.";
$aConfig['lang']['m_zamowienia']['status_1'] = "w realizacji";
$aConfig['lang']['m_zamowienia']['status_2'] = "skompletowane";
$aConfig['lang']['m_zamowienia']['status_3'] = "zatwierdzone";
$aConfig['lang']['m_zamowienia']['status_4'] = "zrealizowane";
$aConfig['lang']['m_zamowienia']['status_5'] = "anulowane";

$aConfig['lang']['m_zamowienia']['pstatus_0'] = "nieopłacone";
$aConfig['lang']['m_zamowienia']['pstatus_1'] = "opłacone";

$aConfig['lang']['m_zamowienia']['mail_status_0'] = "nowe";
$aConfig['lang']['m_zamowienia']['mail_status_1'] = "status_w_realizacji";
$aConfig['lang']['m_zamowienia']['mail_status_2'] = "status_skompletowane";
$aConfig['lang']['m_zamowienia']['mail_status_3'] = "status_zatwierdzone";
$aConfig['lang']['m_zamowienia']['mail_status_4'] = "status_zrealizowane";
$aConfig['lang']['m_zamowienia']['mail_status_5'] = "status_anulowane";

$aConfig['lang']['m_zamowienia']['payment_status_0'] = "nie opłacone";
$aConfig['lang']['m_zamowienia']['payment_status_1'] = "opłacone";
$aConfig['lang']['m_zamowienia']['payment_status_2'] = "transakcja odmowna";

$aConfig['lang']['m_zamowienia']['payment_status'] = "Status płatności";

$aConfig['lang']['m_zamowienia']['shipment_0'] = "Od ręki";
$aConfig['lang']['m_zamowienia']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['m_zamowienia']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['m_zamowienia']['shipment_3'] = "tylko na zamówienie";

$aConfig['lang']['m_zamowienia']['platnosci_booked'] = "Zapłacono";
$aConfig['lang']['m_zamowienia']['platnosci_failed'] = "Płatność odrzucona";
$aConfig['lang']['m_zamowienia']['booked_payment'] = "Zaksięgowano";
$aConfig['lang']['m_zamowienia']['platnosci_transaction'] = "Nr transakcji:";
$aConfig['lang']['m_zamowienia']['underpayment'] = "niedopłata";
$aConfig['lang']['m_zamowienia']['overpayment'] = "nadpłata";

$aConfig['lang']['m_zamowienia']['details'] = "Szczegóły zamówienia";
$aConfig['lang']['m_zamowienia']['delete'] = "Usuń zamówienie";
$aConfig['lang']['m_zamowienia']['send'] = "Wyślij informacje o statusie zamówienia";

$aConfig['lang']['m_zamowienia']['review_all'] = 'Wyślij razem';
$aConfig['lang']['m_zamowienia']['review_all_q'] = "Czy na pewno wysłać razem wybrane zamówienia?";
$aConfig['lang']['m_zamowienia']['review_all_err'] = "Nie wybrano żadnego zamówienia !";



$aConfig['lang']['m_zamowienia']['publish_all'] = "Zmień status";
$aConfig['lang']['m_zamowienia']['publish_all_q'] = "Czy na pewno zmienić status wybranych zamówień?";
$aConfig['lang']['m_zamowienia']['publish_all_err'] = "Nie wybrano żadnego zamówienia do zmiany statusu!";
$aConfig['lang']['m_zamowienia']['payment_sum'] = "SUMUJ";
$aConfig['lang']['m_zamowienia']['package_closed'] = "Gotowe do wysłania";
$aConfig['lang']['m_zamowienia']['package_closed_q'] = "Czy na pewno zmienić status wybranych zamówień?";
$aConfig['lang']['m_zamowienia']['package_closed_err'] = "Nie wybrano żadnego zamówienia do zmiany statusu!";

$aConfig['lang']['m_zamowienia']['get_package_status'] = "Sprawdź status przesyłki";
$aConfig['lang']['m_zamowienia']['auto-source'] = "Obrób zamowienie";

$aConfig['lang']['m_zamowienia']['send_mail'] = "Wyślij powiadomienie o statusie";
$aConfig['lang']['m_zamowienia']['delete_invoice'] = "<span style='color:red; font-size:12px;'>Usuń fakturę";
$aConfig['lang']['m_zamowienia']['change_status_header'] = "Grupowa zmiana statusu";
$aConfig['lang']['m_zamowienia']['point_of_receipt'] = "Punkt odbioru";
$aConfig['lang']['m_zamowienia']['inpost_err'] = "Wystąpił błąd podczas potwierdzania nadania paczki";
$aConfig['lang']['m_zamowienia']['send_print_orders'] = "Wysłano fakturę na drukarkę";

$aConfig['lang']['m_zamowienia']['delete_q'] = "Czy na pewno usunąć wybrane zamówienie?";
$aConfig['lang']['m_zamowienia']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia']['delete_all_q'] = "Czy na pewno usunąć wybrane zamówienia?";
$aConfig['lang']['m_zamowienia']['delete_all_err'] = "Nie wybrano żadnego zamówienia do usunięcia!";
$aConfig['lang']['m_zamowienia']['add'] = "Dodaj zamówienie";

$aConfig['lang']['m_zamowienia']['print_all'] = "Drukuj zaznaczone";
$aConfig['lang']['m_zamowienia']['print_all_q'] = "Czy na pewno wydrukować wybrane zamówienia?";
$aConfig['lang']['m_zamowienia']['print_all_err'] = "Nie wybrano żadnego zamówienia do druku!";
$aConfig['lang']['m_zamowienia']['print_invoice_list'] = "Drukuj fakturę";

$aConfig['lang']['m_zamowienia']['print_invoice'] = "Drukuj fakturę";
$aConfig['lang']['m_zamowienia']['print_invoice_proforma'] = "Drukuj fakturę Proforma";
$aConfig['lang']['m_zamowienia']['print_transport'] = "Drukuj List Przewozowy";

$aConfig['lang']['m_zamowienia']['transport_remarks'] = "Uwagi dla kuriera";
$aConfig['lang']['m_zamowienia']['edit_transport_remarks'] = "Edytuj";
$aConfig['lang']['m_zamowienia']['transport_remarks_send'] = "Zapisz";
$aConfig['lang']['m_zamowienia']['transport_remarks_hide'] = "Zamknij edycję";

$aConfig['lang']['m_zamowienia']['send_date'] = "Data wysyłki";
$aConfig['lang']['m_zamowienia']['shipment_date'] = "Planowana data wysyłki";
$aConfig['lang']['m_zamowienia']['shipment_date_expected'] = "Aktualna planowana data wysyłki";
$aConfig['lang']['m_zamowienia']['total_order_weight'] = 'Waga zamówienia';
$aConfig['lang']['m_zamowienia']['transport_date'] = "Planowana data odbioru";

$aConfig['lang']['m_zamowienia']['order_details_header'] = "Szczegóły zamówienia nr: %s  &nbsp;  (id: %d)";

$aConfig['lang']['m_zamowienia']['order_data'] = 'Zawartość zamówienia';

$aConfig['lang']['m_zamowienia']['items_list_lp'] = 'Lp.';
$aConfig['lang']['m_zamowienia']['items_list_name'] = 'Nazwa';
$aConfig['lang']['m_zamowienia']['items_list_shipment'] = 'Wysyłamy';
$aConfig['lang']['m_zamowienia']['items_list_promotion'] = 'Promocja / rabat:';
$aConfig['lang']['m_zamowienia']['items_list_price_netto'] = 'Netto';

$aConfig['lang']['m_zamowienia']['items_list_vat'] = 'VAT';
$aConfig['lang']['m_zamowienia']['items_list_price_brutto'] = 'Brutto';
$aConfig['lang']['m_zamowienia']['items_list_discount'] = 'Rabat (%)';
$aConfig['lang']['m_zamowienia']['items_list_discount2'] = 'R (%)';
$aConfig['lang']['m_zamowienia']['items_list_discount_currency'] = 'Wart. rabatu';
$aConfig['lang']['m_zamowienia']['items_list_value_netto'] = 'Wart. netto';
$aConfig['lang']['m_zamowienia']['items_list_value_brutto'] = 'Wart. brutto';
$aConfig['lang']['m_zamowienia']['items_list_value_brutto2'] = 'W. brutto';
$aConfig['lang']['m_zamowienia']['items_list_transport'] = 'Transport';
$aConfig['lang']['m_zamowienia']['items_list_status'] = 'Stan';
$aConfig['lang']['m_zamowienia']['items_list_source2'] = 'Źródło';
$aConfig['lang']['m_zamowienia']['items_list_self_collection'] = 'Odbiór osobisty';
$aConfig['lang']['m_zamowienia']['items_list_transport_price'] = 'Cena transportu:';
$aConfig['lang']['m_zamowienia']['items_list_total_transport_price'] = 'Suma transport';
$aConfig['lang']['m_zamowienia']['items_list_quantity'] = 'Ilość';
$aConfig['lang']['m_zamowienia']['items_list_weight'] = 'Waga';
$aConfig['lang']['m_zamowienia']['items_list_items_total_value_netto'] = 'Suma wartość netto:';
$aConfig['lang']['m_zamowienia']['items_list_items_total_value_brutto'] = 'Suma wartość brutto:';
$aConfig['lang']['m_zamowienia']['items_list_items_transport_price'] = 'Suma transport:';
$aConfig['lang']['m_zamowienia']['items_list_postal_fee'] = 'Pobranie:';
$aConfig['lang']['m_zamowienia']['items_list_other_cost'] = 'Manipulacyjne:';
$aConfig['lang']['m_zamowienia']['items_list_total_cost'] = 'SUMA:';

$aConfig['lang']['m_zamowienia']['ptype_postal_fee'] = "płatność przy odbiorze za pobraniem";
$aConfig['lang']['m_zamowienia']['ptype_bank_transfer'] = "przelew - przedpłata na konto księgarni";
$aConfig['lang']['m_zamowienia']['ptype_bank_14days'] = "przelew 14 dni";
$aConfig['lang']['m_zamowienia']['ptype_dotpay'] = "karta płatnicza lub e-przelew";
$aConfig['lang']['m_zamowienia']['ptype_vat'] = "faktura VAT";
$aConfig['lang']['m_zamowienia']['ptype_self_collect'] = "odbiór osobisty";
$aConfig['lang']['m_zamowienia']['ptype_platnoscipl'] = "przelew elektroniczny przez PayU";
$aConfig['lang']['m_zamowienia']['ptype_payu_bank_transfer'] = "przelew przedpłata przez PayU";
$aConfig['lang']['m_zamowienia']['ptype_card'] = "karta kredytowa przez PayU";
$aConfig['lang']['m_zamowienia']['ptype_tba'] = "TBA pobranie";
$aConfig['lang']['m_zamowienia']['ptype_poczta_polska'] = "Poczta Polska pobranie";
$aConfig['lang']['m_zamowienia']['ptype_ruch'] = "Ruch pobranie";

$aConfig['lang']['m_zamowienia']['transport_mean'] = "Metoda transportu";
$aConfig['lang']['m_zamowienia']['payment_method'] = "Sposób płatności";
$aConfig['lang']['m_zamowienia']['second_payment_method'] = "Drugi sposób płatności";
$aConfig['lang']['m_zamowienia']['items_list_transport_price'] = "Koszt transportu";
$aConfig['lang']['m_zamowienia']['items_list_block_transport_cost'] = "Nie modyfikuj ustalonego kosztu transportu";
$aConfig['lang']['m_zamowienia']['block_transport_cost'] = "Nie modyfikuj ustalonego kosztu transportu";
$aConfig['lang']['m_zamowienia']['items_list_items_total_value'] = "Wartość zamówienia";
$aConfig['lang']['m_zamowienia']['items_list_items_total_value_transport'] = "Wartość z transportem";
$aConfig['lang']['m_zamowienia']['items_list_items_to_pay'] = "Do zapłaty";
$aConfig['lang']['m_zamowienia']['items_list_items_paid_amount'] = "Zapłacono";

$aConfig['lang']['m_zamowienia']['send_modified'] = 'Wyślij informacje do klienta';
$aConfig['lang']['m_zamowienia']['modified_info'] = 'Zamówienie zostało zmodyfikowane %s przez %s';

$aConfig['lang']['m_zamowienia']['user_type'] = 'Użytkownik';
$aConfig['lang']['m_zamowienia']['user_type_registred'] = 'Zarejestrowany';
$aConfig['lang']['m_zamowienia']['user_type_not_registred'] = 'Niezarejestrowany';

$aConfig['lang']['m_zamowienia']['items_status_-1'] = 'zapo';
$aConfig['lang']['m_zamowienia']['items_status_0'] = '---';
$aConfig['lang']['m_zamowienia']['items_status_1'] = 'do zam.';
$aConfig['lang']['m_zamowienia']['items_status_2'] = 'zam.';
$aConfig['lang']['m_zamowienia']['items_status_3'] = 'jest niepotwierdzone';
$aConfig['lang']['m_zamowienia']['items_status_4'] = 'jest potwierdzone';

$aConfig['lang']['m_zamowienia']['items_status']['-1'] = 'zapo';
$aConfig['lang']['m_zamowienia']['items_status']['0'] = '---';
$aConfig['lang']['m_zamowienia']['items_status']['1'] = 'do zam.';
$aConfig['lang']['m_zamowienia']['items_status']['2'] = 'zam.';
$aConfig['lang']['m_zamowienia']['items_status']['3'] = 'jest nie';
$aConfig['lang']['m_zamowienia']['items_status']['4'] = 'jest pot';

$aConfig['lang']['m_zamowienia']['changes'] = 'Historia zmian';
$aConfig['lang']['m_zamowienia']['reset-balance'] = 'Wyrównaj saldo';

$aConfig['lang']['m_zamowienia']['delete_item_q'] = "Czy na pewno usunąć wybraną pozycję zamówienia?";
$aConfig['lang']['m_zamowienia']['delete_item'] = "Usuń pozycję zamówienia";
$aConfig['lang']['m_zamowienia']['add_to_order'] = "Dodaj pozycję do zamówienia";

$aConfig['lang']['m_zamowienia']['send_button_items'] = "Przelicz ponownie";
$aConfig['lang']['m_zamowienia']['edit_user_data'] = "Edytuj dane";
$aConfig['lang']['m_zamowienia']['edit_user_data_hide'] = "Zamknij edycję danych";

$aConfig['lang']['m_zamowienia']['no_user_data'] = 'Brak danych zamawiającego';
$aConfig['lang']['m_zamowienia']['no_user_invoice_data'] = 'Brak danych do faktury';
$aConfig['lang']['m_zamowienia']['user_data'] = 'Dane zamawiającego';
$aConfig['lang']['m_zamowienia']['user_data_details'] = 'Zamawiający';
$aConfig['lang']['m_zamowienia']['user_data_delivery'] = 'Dane do dostawy';
$aConfig['lang']['m_zamowienia']['user_data_invoice'] = 'Dane do faktury';
$aConfig['lang']['m_zamowienia']['user_data_invoice2'] = 'Dane do drugiej faktury';

$aConfig['lang']['m_zamowienia']['is_company'] = 'Typ klient';
$aConfig['lang']['m_zamowienia']['is_company_0'] = 'Indywidualny';
$aConfig['lang']['m_zamowienia']['is_company_1'] = 'Firma';

$aConfig['lang']['m_zamowienia']['payment'] = 'Metoda płatności';
$aConfig['lang']['m_zamowienia']['remarks'] = 'Uwagi do zamówienia';
$aConfig['lang']['m_zamowienia']['order_nip'] = 'NIP: ';
$aConfig['lang']['m_zamowienia']['phone'] = 'Tel.: ';
$aConfig['lang']['m_zamowienia']['cell_phone'] = 'Tel. kom.: ';
$aConfig['lang']['m_zamowienia']['user_remarks'] = 'Uwagi do zamówienia: ';

$aConfig['lang']['m_zamowienia']['client_ip'] = 'IP komputera';
$aConfig['lang']['m_zamowienia']['client_host'] = 'Host';
$aConfig['lang']['m_zamowienia']['source'] = 'Źródło';
$aConfig['lang']['m_zamowienia']['opineo_dont_send'] = 'Nie wysyłaj prośby o opinię';

$aConfig['lang']['m_zamowienia']['invoice_id'] = 'Nr faktury';

$aConfig['lang']['m_zamowienia']['transport_invoice_item'] = 'Koszt transportu';

$aConfig['lang']['m_zamowienia']['get_sticker'] = 'Pobierz etykietę';
$aConfig['lang']['m_zamowienia']['get_confirm_sticker'] = 'Pobierz potwierdzenie nadania paczki';
$aConfig['lang']['m_zamowienia']['transport_type'] = 'Forma transportu';
$aConfig['lang']['m_zamowienia']['transport_1'] = 'Transport poprzez firmę spedycyjną';
$aConfig['lang']['m_zamowienia']['transport_2'] = 'Transport firmowy';
$aConfig['lang']['mod_m_zamowienia']['payment_details_prepay'] = '<strong>%s</strong><br/>ul. %s %s %s<br />%s %s<br />%s<br />%s<br />Tytuł: Zamówienie numer %s';

$aConfig['lang']['m_zamowienia']['order_print_header'] = 'Zamówienie nr';

$aConfig['lang']['m_zamowienia']['toggle_invoice_2_on'] = 'Dodaj drugą fakturę do zamówienia';
$aConfig['lang']['m_zamowienia']['toggle_invoice_2_off'] = 'Usuń drugą fakturę z zamówienia';

$aConfig['lang']['m_zamowienia']['toggle_invoice_err_0'] = 'Nie udało sie włączyc drugiej faktury!<br>Spróbuj ponownie';
$aConfig['lang']['m_zamowienia']['toggle_invoice_err_1'] = 'Nie udało sie usunąć drugiej faktury!<br>Spróbuj ponownie';
$aConfig['lang']['m_zamowienia']['toggle_invoice_ok_0'] = 'Dodano drugą fakturę do zamówienia';
$aConfig['lang']['m_zamowienia']['toggle_invoice_ok_1'] = 'Usunięto drugą fakturę z zamówienia';

$aConfig['lang']['m_zamowienia']['enable_second_payment'] = 'Dodaj płatność';
$aConfig['lang']['m_zamowienia']['enable_second_payment_enabled'] = 'Druga płatność dla zamówienia numer %s została dodana.';
$aConfig['lang']['m_zamowienia']['enable_second_payment_err'] = 'Wystąpił błąd podczas dodawania drugiej formy płatności %s. Spróbuj ponownie.';

// dotpay
$aConfig['lang']['m_zamowienia']['dotpay_'] = "&nbsp;";
$aConfig['lang']['m_zamowienia']['dotpay_1'] = "NOWA";
$aConfig['lang']['m_zamowienia']['dotpay_2'] = "WYKONANA";
$aConfig['lang']['m_zamowienia']['dotpay_3'] = "ODMOWNA";
$aConfig['lang']['m_zamowienia']['dotpay_4'] = "ANULOWANA";
$aConfig['lang']['m_zamowienia']['dotpay_5'] = "REKLAMACJA";

$aConfig['lang']['m_zamowienia']['order_status'] = 'Status zamówienia';
$aConfig['lang']['m_zamowienia']['order_status_change_remarks'] = 'Uwagi';
$aConfig['lang']['m_zamowienia']['order_status_0'] = 'nowe';
$aConfig['lang']['m_zamowienia']['order_status_1'] = 'opłacone';
$aConfig['lang']['m_zamowienia']['order_status_2'] = 'wysłane / zrealizowane';
$aConfig['lang']['m_zamowienia']['order_status_3'] = 'transakcja odmowna';
$aConfig['lang']['m_zamowienia']['order_status_3'] = 'anulowane';

$aConfig['lang']['m_zamowienia']['status_change_err'] = 'Nie mozna zmienić statusu na wcześniejszy od aktualnego!';
$aConfig['lang']['m_zamowienia']['status_change_1_err'] = "Status <strong>w realizacji</strong> ustawi się automatycznie po zmianie statusu książki na 'do zamówienia'!";
$aConfig['lang']['m_zamowienia']['status_change_2_err'] = "Status <strong>skompletowane<strong> ustawi się automatycznie po zmianie statusu wszystkich książek na 'jest'!";
$aConfig['lang']['m_zamowienia']['status_change_3_err'] = "Status <strong>zatwierdzone<strong> ustawi się automatycznie po wybraniu drukowania faktury!";

$aConfig['lang']['m_zamowienia']['status_change_4_err'] = "Aby zmienić status na <strong>zrealizowane</strong> zamówienie musi być <strong>zatwierdzone<strong>";
$aConfig['lang']['m_zamowienia']['status_change_5_err'] = "Nie można anulowac zamówienia zrealizowanego!";
$aConfig['lang']['m_zamowienia']['status_change_6_err'] = "Wybierz typ przesyłki!";
$aConfig['lang']['m_zamowienia']['status_change_4_invoice_err'] = "Aby zmienić status na <strong>zrealizowane</strong> zamówienie musi posiadać wygenerowaną fakturę";

$aConfig['lang']['m_zamowienia']['status_change_pay_err'] = "Aby zmienić status na <strong>skompletowane</strong> zamówienie musi być <strong>opłacone<strong>, i zapłacona kwota musi zgadzać sie z wartością zamówienia";
$aConfig['lang']['m_zamowienia']['status_change_pay_amount_err'] = "Aby zmienić status na <strong>zrealizowane</strong> wartość całkowita zamówienia musi pokrywać sie z zapłaconą kwotą!";
$aConfig['lang']['m_zamowienia']['payment_cancel_err'] = "Nie można zmienić statusu płatności anulowanego zamówienia!";

$aConfig['lang']['m_zamowienia']['invoice_pay_err'] = "Aby wygenerować fakturę zamówienie musi być <strong>opłacone<strong>, i zapłacona kwota musi zgadzać sie z wartością zamówienia";
$aConfig['lang']['m_zamowienia']['invoice_gen_err'] = "Błąd generowania faktury! Spróbuj ponownie!";
$aConfig['lang']['m_zamowienia']['invoice_gen_err_1'] = "Jeśli zmieniłeś metodę transportu/płatności kliknij wprowadź zmiany lub przelicz zamówienie a nastepnie wydrukuj list przewozowy!";
$aConfig['lang']['m_zamowienia']['invoice_gen_err_2'] = "Przesłano niepoprawny kod paczkomatów !";
$aConfig['lang']['m_zamowienia']['invoice_gen_opek'] = "Przesłano dane do drukarki listów przewozowych.";
$aConfig['lang']['m_zamowienia']['invoice_exist_opek'] = "Zamówienie oczekuje na zwrot numeru listu przewozowego.";

$aConfig['lang']['m_zamowienia']['additional_info'] = 								'Dodatkowe informacje do zamówienia';
$aConfig['lang']['m_zamowienia']['edit_additional_info'] = 						'Dodaj informację do zamówienia';
$aConfig['lang']['m_zamowienia']['close_additional_info'] = 					'Zamknij dodawanie';
$aConfig['lang']['m_zamowienia']['order_additional_info_edit_ok'] = 	'Zapisano dodatkową informację do zamówienia.';
$aConfig['lang']['m_zamowienia']['order_additional_info_edit_err'] = 	'Wystąpił błąd podczas zapisu dodatkowej informacji do zamówienia. Spróbój ponownie.';

$aConfig['lang']['m_zamowienia']['user_comments'] =                   'Komentarze do użytkownika ';
$aConfig['lang']['m_zamowienia']['add_user_comments'] =              'Dodaj komentarz do użytkownika';
$aConfig['lang']['m_zamowienia']['user_comments_hide'] =              'Zamknij dodawanie';
$aConfig['lang']['m_zamowienia']['user_comments_edit_ok'] = 	'Dodano komentarz do użytkownika.';
$aConfig['lang']['m_zamowienia']['user_comments_edit_err'] = 	'Wystąpił błąd podczas dodawania komentarza do użytkownika. Spróbój ponownie.';


$aConfig['lang']['m_zamowienia']['order_status_history'] = 'Historia zmian statusów';
$aConfig['lang']['m_zamowienia']['status_update'] = "%s przez %s";
$aConfig['lang']['m_zamowienia']['status_0_update'] = "złożone";
$aConfig['lang']['m_zamowienia']['status_1_update'] = "w realizacji";
$aConfig['lang']['m_zamowienia']['status_2_update'] = "skompletowane";
$aConfig['lang']['m_zamowienia']['status_int_5_update'] = "posortował";
$aConfig['lang']['m_zamowienia']['status_3_update'] = "zatwierdzone";
$aConfig['lang']['m_zamowienia']['status_4_update'] = "zrealizowane";
$aConfig['lang']['m_zamowienia']['status_5_update'] = "anulowane";
$aConfig['lang']['m_zamowienia']['remarks_read_1'] = "przeczytano uwagę";
$aConfig['lang']['m_zamowienia']['activation_date'] = "aktywowano";

$aConfig['lang']['m_zamowienia']['transport_number_waiting'] = "Zamówienie czeka na pobranie danych przez drukarkę listów przewozowych";
$aConfig['lang']['m_zamowienia']['transport_number_print'] = "Dane pobrane przez drukarkę listów przewozowych, oczekuję na numer listu przewozowego";
$aConfig['lang']['m_zamowienia']['transport_number_import_error'] = "Import danych przez drukarkę listów przewozowych nie powiódł się";
$aConfig['lang']['m_zamowienia']['transport_number'] = "Numer listu przewozowego";

$aConfig['lang']['m_zamowienia']['order_status_user_email_subject'] = 'Zmiana statusu zamowienia %d';
$aConfig['lang']['m_zamowienia']['order_status_change_email_body'] =' 
UWAGI DO ZAMÓWIENIA:
%s

';
$aConfig['lang']['m_zamowienia']['order_status_user_email_body'] = 'W dniu dzisiejszym Twoje zamówienie nr %d otrzymało status: \'%s\'.

%s%s';
$aConfig['lang']['m_zamowienia']['status_edit_ok'] = "Zmieniono status zamówienia";
$aConfig['lang']['m_zamowienia']['status_edit_err'] = "Nie udało się zmienić statusu zamówienia!<br>Spróbuj ponownie";

$aConfig['lang']['m_zamowienia']['order_user_data_edit_ok'] = "Zmieniono dane użytkownika dla zamówienia";
$aConfig['lang']['m_zamowienia']['order_user_data_edit_err'] = "Nie udało się zmienić danych użytkownika zamówienia!<br>Spróbuj ponownie";

$aConfig['lang']['m_zamowienia']['order_transport_remarks_edit_ok'] = "Zmieniono uwagi dla kuriera dla zamówienia";
$aConfig['lang']['m_zamowienia']['order_transport_remarks_edit_err'] = "Nie udało się zmienić uwag dla kuriera zamówienia!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['order_transport_number_edit_ok'] = "Ustawiono numer listu przewozowego";
$aConfig['lang']['m_zamowienia']['order_transport_number_edit_err'] = "Nie udało się zapisać numeru listu przewozowego!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['order_send_date_edit_ok'] = "Zmieniono datę wysyłki dla zamówienia";
$aConfig['lang']['m_zamowienia']['order_send_date_edit_err'] = "Nie udało się zmienić daty wysyłki!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['order_opineo_edit_ok'] = "Zmieniono stan wysyłki opineo dla zamówienia";
$aConfig['lang']['m_zamowienia']['order_opineo_edit_err'] = "Nie udało się zmienić stanu wysyłki opineo dla zamówienia!<br>Spróbuj ponownie";


$aConfig['lang']['m_zamowienia']['order_items_edit_ok'] = "Przeliczono wartość i koszt transportu zamówienia";
$aConfig['lang']['m_zamowienia']['order_items_edit_err'] = "Nie udało się przeliczyć wartości i kosztu transportu zamówienia!<br>Spróbuj ponownie";

$aConfig['lang']['m_zamowienia']['activate'] = "Aktywuj";
$aConfig['lang']['m_zamowienia']['activate_ok'] = "Aktywowano zamówienie %s";
$aConfig['lang']['m_zamowienia']['activate_err'] = "Nie udało się aktywować zamówienia %s!<br>Spróbuj ponownie";

$aConfig['lang']['m_zamowienia']['del_ok_0'] = "Usunięto zamówienie %s";
$aConfig['lang']['m_zamowienia']['del_err_0'] = "Nie udało się usunąć zamówienia %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['del_ok_1'] = "Usunięto zamówienia %s";
$aConfig['lang']['m_zamowienia']['del_err_1'] = "Nie udało się usunąć zamówień %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['delete_item_ok'] = "Usunięto pozycję \"%s\" z zamówienia";
$aConfig['lang']['m_zamowienia']['delete_item_err'] = "Nie udało się usunąć pozycji \"%s\" z zamówienia !<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['delete_item_err_last'] = "Nie można usunać pozycji z zamówienia które posiada tylko jedną pozycję!";
$aConfig['lang']['m_zamowienia']['change_invoice_ok'] = "Przesunięto pozycje \"%s\" do innej faktury";
$aConfig['lang']['m_zamowienia']['change_invoice_err'] = "Nie udało się przesunąć pozycji \"%s\" do innej faktury !<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['change_payment_ok'] = "Przesunięto pozycje \"%s\" do innej metody płatności";
$aConfig['lang']['m_zamowienia']['change_payment_err'] = "Nie udało się przesunąć pozycji \"%s\" do innej metody płatności !<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['change_invoice_1'] = "Przesuń pozycję do 1 faktury";
$aConfig['lang']['m_zamowienia']['change_invoice_2'] = "Przesuń pozycję do 2 faktury";
$aConfig['lang']['m_zamowienia']['change_payment_1'] = "Przesuń pozycję na 1 metodę płatności";
$aConfig['lang']['m_zamowienia']['change_payment_2'] = "Przesuń pozycję na 2 metodę płątnosci";
$aConfig['lang']['m_zamowienia']['undelete_item'] = "Przywróć pozycję do zamówienia";
$aConfig['lang']['m_zamowienia']['change_invoice_err_fv1'] = "Pierwsza faktura nie może pozostać pusta!";
$aConfig['lang']['m_zamowienia']['change_invoice_err_fv2'] = "Druga faktura nie może pozostać pusta!";
$aConfig['lang']['m_zamowienia']['undelete_item_ok'] = "Przywrócono pozycję \"%s\" do zamówienia";
$aConfig['lang']['m_zamowienia']['undelete_item_err'] = "Nie udało się przywrócić pozycji \"%s\" do zamówienia !<br>Spróbuj ponownie";

$aConfig['lang']['m_zamowienia']['group_status_ok'] = "Zmieniono status %s zamówień";
$aConfig['lang']['m_zamowienia']['group_delete_ok'] = "<br />Usunięto faktury z %s zamówień";
$aConfig['lang']['m_zamowienia']['group_status_err'] = "Nie udało się zmienić statusów %s zamówień!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia']['group_status_err_2'] = "Nie można zmienić statusu zamówienia na anulowane jeśli wystawiona została faktura.";

$aConfig['lang']['m_zamowienia']['connect_to']='Połącz z wybranym zamówieniem';
$aConfig['lang']['m_zamowienia']['connect_to_err']='Wystąpił błąd podczas łączenia zamówień. Spróbuj ponownie.';
$aConfig['lang']['m_zamowienia']['connect_to_mustselect']='Aby połączyć zamówienia musisz wybrać zamówienie do połaczenia';
$aConfig['lang']['m_zamowienia']['connect_to_ok']='Pomyślnie połączono zamówienia';
$aConfig['lang']['m_zamowienia']['select_to_connect']='-- Wybierz zamówienie do połaczenia --';
$aConfig['lang']['m_zamowienia']['connect_to_connect']='Połącz';

$aConfig['lang']['m_zamowienia']['remarks_read'] = "Przeczytane";
$aConfig['lang']['m_zamowienia']['err_remarks_read'] = "Przeczytaj uwagi do zamówienia";
$aConfig['lang']['m_zamowienia']['print_fv'] = "Drukuj fakturę";

$aConfig['lang']['m_zamowienia']['correction'] = "Korekta zamówienia";
$aConfig['lang']['m_zamowienia']['correction_ok'] = "Zmieniono status korekty w zamówieniu %s ";
$aConfig['lang']['m_zamowienia']['correction_err'] = "Wystąpił błąd podczas zmiany statusu korekty w zamówieniu %s ";
$aConfig['lang']['m_zamowienia']['transport_option'] = "Typ przesyłki";

$aConfig['lang']['m_zamowienia']['transport_gen_error_0'] = 'Wystąpił błąd podczas wysyłania paczki danych do Paczkomaty <br /> "%s" : "%s"';
$aConfig['lang']['m_zamowienia']['transport_gen_error_1'] = 'Wystąpił błąd podczas wysyłania paczki danych do Paczkomaty';
$aConfig['lang']['m_zamowienia']['transport_gen_error_2'] = 'Wystąpił błąd podczas zapisywania numeru paczki';

$aConfig['lang']['m_zamowienia']['paczkomaty_point_of_recipt'] = 'Punkt odbioru paczki: ';
$aConfig['lang']['m_zamowienia']['city'] = 'Miasto: ';
$aConfig['lang']['m_zamowienia']['adress'] = 'Adres: ';
$aConfig['lang']['m_zamowienia']['phone_paczkomaty'] = 'Telefon dla InPost:';

$aConfig['lang']['m_zamowienia']['save_shipment_date_ok'] = "Wprowadzono aktualną datę premiery zapowiedzi '%s' w zamówieniu o id = %s, składowa zamówienia = %s";
$aConfig['lang']['m_zamowienia']['save_shipment_date_err'] = "Wystąpił błąd podczas zapisywania premiery zapowiedzi '%s' w zamówieniu o id = %s, składowa zamówienia = %s";
$aConfig['lang']['m_zamowienia']['save_shipment_date_err_data'] = "Popraw datę wydania zapowiedzi";

$aConfig['lang']['m_zamowienia']['alert'] = 'Utwórz przypomnienie do zamówienia';
$aConfig['lang']['m_zamowienia']['order_locked'] = 'Zamówienie zostało zablokowane przez "%s" ';

$aConfig['lang']['m_zamowienia']['send_update_err'] = 'Wystąpił błąd podczas wysyłania powiadomienia o zmianie zamówienia.';
        
/*----------platnosci do zamowienia -----------------------*/
$aConfig['lang']['m_zamowienia_payhistory']['date'] = "Data";
$aConfig['lang']['m_zamowienia_payhistory']['order_payments_header'] = "Płatności do zamówienia nr: %s";
$aConfig['lang']['m_zamowienia_payhistory']['order_payments_header2'] = "Statusy opłat z serwisu PayU.pl: %s";
$aConfig['lang']['m_zamowienia_payhistory']['ammount'] = "Wartość";
$aConfig['lang']['m_zamowienia_payhistory']['type'] = "Typ";
$aConfig['lang']['m_zamowienia_payhistory']['iban'] = "Nr konta klienta";
$aConfig['lang']['m_zamowienia_payhistory']['reference'] = "Nr referencyjny";
$aConfig['lang']['m_zamowienia_payhistory']['title'] = 'Tytułem';
$aConfig['lang']['m_zamowienia_payhistory']['transaction_id'] = 'ID transakcji';
$aConfig['lang']['m_zamowienia_payhistory']['status'] = 'Status';
$aConfig['lang']['m_zamowienia_payhistory']['platnosci_status_1'] = 'nowa';
$aConfig['lang']['m_zamowienia_payhistory']['platnosci_status_2'] = 'anulowana';
$aConfig['lang']['m_zamowienia_payhistory']['platnosci_status_3'] = 'odrzucona';
$aConfig['lang']['m_zamowienia_payhistory']['platnosci_status_4'] = 'rozpoczęta';
$aConfig['lang']['m_zamowienia_payhistory']['platnosci_status_5'] = 'oczekuje na odbiór';
$aConfig['lang']['m_zamowienia_payhistory']['platnosci_status_7'] = 'odrzucona, środki otrzymano';
$aConfig['lang']['m_zamowienia_payhistory']['platnosci_status_99'] = 'zakończona';
$aConfig['lang']['m_zamowienia_payhistory']['platnosci_status_888'] = 'błędny status';

$aConfig['lang']['m_zamowienia']['date'] = "Data";
$aConfig['lang']['m_zamowienia']['order_payments_header'] = "Płatności do zamówienia";
$aConfig['lang']['m_zamowienia']['order_payments_header2'] = "Statusy opłat z serwisu PayU.pl";
$aConfig['lang']['m_zamowienia']['ammount'] = "Wartość";
$aConfig['lang']['m_zamowienia']['type'] = "Typ";
$aConfig['lang']['m_zamowienia']['iban'] = "Nr konta klienta";
$aConfig['lang']['m_zamowienia']['reference'] = "Nr referencyjny";
$aConfig['lang']['m_zamowienia']['title'] = 'Tytułem';
$aConfig['lang']['m_zamowienia']['transaction_id'] = 'ID transakcji';
$aConfig['lang']['m_zamowienia']['status'] = 'Status';
$aConfig['lang']['m_zamowienia']['platnosci_status_1'] = 'nowa';
$aConfig['lang']['m_zamowienia']['platnosci_status_2'] = 'anulowana';
$aConfig['lang']['m_zamowienia']['platnosci_status_3'] = 'odrzucona';
$aConfig['lang']['m_zamowienia']['platnosci_status_4'] = 'rozpoczęta';
$aConfig['lang']['m_zamowienia']['platnosci_status_5'] = 'oczekuje na odbiór';
$aConfig['lang']['m_zamowienia']['platnosci_status_7'] = 'odrzucona, środki otrzymano';
$aConfig['lang']['m_zamowienia']['platnosci_status_99'] = 'zakończona';
$aConfig['lang']['m_zamowienia']['platnosci_status_888'] = 'błędny status';
$aConfig['lang']['m_zamowienia']['resetBalance_author'] = 'Zmodyfikowane przez';

$aConfig['lang']['m_zamowienia']['providers_auth'] = 'Edytuj dane autoryzacji';
$aConfig['lang']['m_zamowienia']['providers_auth_header'] = 'Dane do autoryzacji';
$aConfig['lang']['m_zamowienia']['auth_login'] = 'Login';
$aConfig['lang']['m_zamowienia']['auth_pasw'] = 'Hasło';
$aConfig['lang']['m_zamowienia']['providers_auth_save'] = 'Zapisz';
$aConfig['lang']['m_zamowienia']['auth_success'] = 'Dane logowania dla: %s prawidłowe!';
$aConfig['lang']['m_zamowienia']['auth_err'] = 'Dane logowania dla: %s nieprawidłowe!';
$aConfig['lang']['m_zamowienia']['auth_insert_fail'] = 'Nie można zapisać danych w bazie! ';
$aConfig['lang']['m_zamowienia']['streamsoft-details'] = 'Wysyłka do streama';

/*----------platnosci do zamowienia -----------------------*/

/*---------- wydruk zamówienia */
$aConfig['lang']['m_zamowienia_print']['data_to_pay'] = "Data złożenia zamówienia:";
$aConfig['lang']['m_zamowienia_print']['data_to_realize'] = "Data zrealizowania:";
$aConfig['lang']['m_zamowienia_print']['printout'] = "Data wydruku:";
$aConfig['lang']['m_zamowienia_print']['nr_order'] = "Nr zamówienia";
$aConfig['lang']['m_zamowienia_print']['invoice_id'] = "Faktura VAT nr:";
$aConfig['lang']['m_zamowienia_print']['data_buyer'] = "Dane kupującego";
$aConfig['lang']['m_zamowienia_print']['data_invoice'] = "Dane do faktury";
$aConfig['lang']['m_zamowienia_print']['data_user'] = "Dane użytkownika";
$aConfig['lang']['m_zamowienia_print']['client_ip'] = "IP komputera:";
$aConfig['lang']['m_zamowienia_print']['client_host'] = "Host:";
$aConfig['lang']['m_zamowienia_print']['user'] = "Użytkownik:";
$aConfig['lang']['m_zamowienia_print']['user_register'] = "Zarejestrowany";
$aConfig['lang']['m_zamowienia_print']['user_not_register'] = "Niezarejestrowany";
$aConfig['lang']['m_zamowienia_print']['lp'] = "Lp.";
$aConfig['lang']['m_zamowienia_print']['name'] = "Nazwa";
$aConfig['lang']['m_zamowienia_print']['status'] = "Stan";
$aConfig['lang']['m_zamowienia_print']['weight'] = "Waga [kg]";
$aConfig['lang']['m_zamowienia_print']['quantity'] = "Ilość";
$aConfig['lang']['m_zamowienia_print']['shipment'] = "Wysyłamy";
$aConfig['lang']['m_zamowienia_print']['vat'] = "VAT [%]";
$aConfig['lang']['m_zamowienia_print']['price_brutto'] = "Brutto [zł]";
$aConfig['lang']['m_zamowienia_print']['discount'] = "Rabat [zł]";
$aConfig['lang']['m_zamowienia_print']['value_brutto'] = "Wartość brutto [zł]";
$aConfig['lang']['m_zamowienia_print']['summary'] = "RAZEM";
$aConfig['lang']['m_zamowienia_print']['order_status_header'] = "Status zamówienia:";
$aConfig['lang']['m_zamowienia_print']['order_status'] = "Stan zamówienia:";
$aConfig['lang']['m_zamowienia_print']['payment_status'] = "Status płatności:";
$aConfig['lang']['m_zamowienia_print']['compled_status'] = "Skomptetowane:";
$aConfig['lang']['m_zamowienia_print']['cancel_status'] = "Anulowane:";
$aConfig['lang']['m_zamowienia_print']['value_order'] = "Wartość zamówienia:";
$aConfig['lang']['m_zamowienia_print']['value_order_transport'] = "Wartość z transportem:";
$aConfig['lang']['m_zamowienia_print']['value_pay'] = "Zapłacono:";
$aConfig['lang']['m_zamowienia_print']['transport_and_pay'] = "Transport i płatność:";
$aConfig['lang']['m_zamowienia_print']['transport_metod'] = "Metoda transportu:";
$aConfig['lang']['m_zamowienia_print']['number_list'] = "Nr listu przewozowego:";
$aConfig['lang']['m_zamowienia_print']['pay_way'] = "Sposób płatności:";
$aConfig['lang']['m_zamowienia_print']['cost_transport'] = "Koszt transportu:";
$aConfig['lang']['m_zamowienia_print']['info'] = "Nie modyfikuj ustalonego kosztu transportu:";

$aConfig['lang']['m_zamowienia']['inpost_get_confirm_printout'] = 'Pobierz potwierdzenia nadania';

//$aConfig['lang']['m_zamowienia']['inpost_get_confirm_printout_all_err'] = "Nie wybrano żadnego zamówienia do !";



/*---------- metody platnosci */
$aConfig['lang']['m_zamowienia_payment_types']['list'] = "Lista metod płatności";
$aConfig['lang']['m_zamowienia_payment_types']['list_ptype'] = "Metoda płatności";
$aConfig['lang']['m_zamowienia_payment_types']['list_order_by'] = "Kolejność";
$aConfig['lang']['m_zamowienia_payment_types']['list_transport'] = "Środek transportu";
$aConfig['lang']['m_zamowienia_payment_types']['no_items'] = "Brak zdefiniowanych metod płatności";
$aConfig['lang']['m_zamowienia_payment_types']['edit'] = "Edytuj metodę płatności";
$aConfig['lang']['m_zamowienia_payment_types']['delete'] = "Usuń metodę płatności";
$aConfig['lang']['m_zamowienia_payment_types']['delete_q'] = "Czy na pewno usunąć wybraną metodę płatności?";
$aConfig['lang']['m_zamowienia_payment_types']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_payment_types']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_payment_types']['delete_all_q'] = "Czy na pewno usunąć wybrane metody płatności?";
$aConfig['lang']['m_zamowienia_payment_types']['delete_all_err'] = "Nie wybrano żadnej metody płatności do usunięcia!";
$aConfig['lang']['m_zamowienia_payment_types']['add'] = "Dodaj metodę płatności";

$aConfig['lang']['m_zamowienia_payment_types']['header_0'] = "Dodawanie metody płatności";
$aConfig['lang']['m_zamowienia_payment_types']['header_1'] = "Edycja metody płatności";
$aConfig['lang']['m_zamowienia_payment_types']['ptype'] = "Typ";
$aConfig['lang']['m_zamowienia_payment_types']['description'] = "Opis";
$aConfig['lang']['m_zamowienia_payment_types']['cost'] = "Koszt tansportu";
$aConfig['lang']['m_zamowienia_payment_types']['vat'] = "Stawka VAT";
$aConfig['lang']['m_zamowienia_payment_types']['transport_means'] = "Środek transportu";
$aConfig['lang']['m_zamowienia_payment_types']['no_costs_from'] = "Brak kosztów tansportu od";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_payment_all'] = "wszystkie metody";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_postal_fee'] = "płatność przy odbiorze";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_bank_transfer'] = "przelew - przedpłata na konto";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_payu_bank_transfer'] = "przelew - przedpłata przez payu";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_bank_14days'] = "przelew 14 dni";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_dotpay'] = "karta płatnicza lub e-przelew (dotpay)";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_platnoscipl'] = "e-przelew (PayU.pl)";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_card'] = "karta kredytowa (PayU.pl)";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_vat'] = "faktura VAT";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_self_collect'] = "odbiór osobisty";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_opek'] = "Przelew z Opek";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_nodata'] = "-brak danych-";
$aConfig['lang']['m_zamowienia_payment_types']['ptype_tba'] = "TBA pobranie";
$aConfig['lang']['m_zamowienia_payment_types']['published'] = "Aktywna";
$aConfig['lang']['m_zamowienia_payment_types']['list_published'] = "Aktywna";
$aConfig['lang']['m_zamowienia_payment_types']['order_by'] = "Kolejność wyświetlania";
$aConfig['lang']['m_zamowienia_payment_types']['published_0'] = "NIE";
$aConfig['lang']['m_zamowienia_payment_types']['published_1'] = "TAK";

$aConfig['lang']['m_zamowienia_payment_types']['dotpay_section'] = "Konfiguracja konta w dotpay.pl";
$aConfig['lang']['m_zamowienia_payment_types']['dotpay_fee'] = "Prowizja [w %]";
$aConfig['lang']['m_zamowienia_payment_types']['dotpay_id'] = "ID konta";
$aConfig['lang']['m_zamowienia_payment_types']['dotpay_pin'] = "PIN";

$aConfig['lang']['m_zamowienia_payment_types']['platnoscipl_section'] = "Konfiguracja konta w PayU.pl";
$aConfig['lang']['m_zamowienia_payment_types']['platnoscipl_posid'] = "Id punktu płatności (pos_id)";
$aConfig['lang']['m_zamowienia_payment_types']['platnoscipl_key1'] = "Klucz";
$aConfig['lang']['m_zamowienia_payment_types']['platnoscipl_key2'] = "Drugi klucz";
$aConfig['lang']['m_zamowienia_payment_types']['platnoscipl_authkey'] = "Klucz autoryzacji płatności (pos_auth_key)";

$aConfig['lang']['m_zamowienia_payment_types']['add_ok'] = "Dodano metodę płatności \"%s\"";
$aConfig['lang']['m_zamowienia_payment_types']['add_err'] = "Nie udało się dodać metody płatności \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_payment_types']['add_err2'] = "Metoda płatności \"%s\" już istnieje!";
$aConfig['lang']['m_zamowienia_payment_types']['edit_ok'] = "Zmieniono metodę płatności \"%s\"";
$aConfig['lang']['m_zamowienia_payment_types']['edit_err'] = "Nie udało się zmienić metody płatności \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_payment_types']['del_ok_0']		= "Usunięto metodę płatności %s";
$aConfig['lang']['m_zamowienia_payment_types']['del_ok_1']		= "Usunięto metodt płatności %s";
$aConfig['lang']['m_zamowienia_payment_types']['del_err_0']		= "Nie udało się usunąć metody płatności %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_payment_types']['del_err_1']		= "Nie udało się usunąć metod płatności %s!<br>Spróbuj ponownie";

$aConfig['lang']['m_zamowienia_payment_types']['shipment_days'] = "Czas wysyłki";
$aConfig['lang']['m_zamowienia_payment_types']['transport_days'] = "Czas transportu";
$aConfig['lang']['m_zamowienia_payment_types']['message'] = "Komunikat";
$aConfig['lang']['m_zamowienia_payment_types']['shipment_time_section'] = "Czasy wysyłki";
$aConfig['lang']['m_zamowienia_payment_types']['time_border'] = "Godzina graniczna";
$aConfig['lang']['m_zamowienia_payment_types']['shipment_time_before_section'] = "Czasy wysyłki przed godziną graniczną";
$aConfig['lang']['m_zamowienia_payment_types']['shipment_time_after_section'] = "Czasy wysyłki po godzinie granicznej";

/*---------- srodki transportu */
$aConfig['lang']['m_zamowienia_transport_means']['list'] = "Lista środków transportu";
$aConfig['lang']['m_zamowienia_transport_means']['list_name'] = "Nazwa";
$aConfig['lang']['m_zamowienia_transport_means']['mail_info'] = "Informacja w mailu po złożeniu zamówienia";
$aConfig['lang']['m_zamowienia_transport_means']['no_items'] = "Brak zdefiniowanych środków transportu";
$aConfig['lang']['m_zamowienia_transport_means']['edit'] = "Edytuj środek transportu";
$aConfig['lang']['m_zamowienia_transport_means']['delete'] = "Usuń środek transportu";
$aConfig['lang']['m_zamowienia_transport_means']['delete_q'] = "Czy na pewno usunąć wybrany środek transportu?";
$aConfig['lang']['m_zamowienia_transport_means']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_transport_means']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_transport_means']['delete_all_q'] = "Czy na pewno usunąć wybrane środki transportu?";
$aConfig['lang']['m_zamowienia_transport_means']['delete_all_err'] = "Nie wybrano żadnego środka transportu do usunięcia!";
$aConfig['lang']['m_zamowienia_transport_means']['go_back'] = "Powrót do listy środków transportu";
$aConfig['lang']['m_zamowienia_transport_means']['max_weight'] = "Maksymalna waga dla pojedynczej przesyłki<br /><br /> (w przypadku paczkomatów maksymalny rozmiar dla paczki A)";
$aConfig['lang']['m_zamowienia_transport_means']['no_costs_from'] = "Brak kosztów tansportu od";
$aConfig['lang']['m_zamowienia_transport_means']['limit_to_expand_shipping_date'] = "Ilość sztuk produktu która zwiększa czas transportu o jeden dzień <br>( zwiększa maksymalnie o 1 dzień!<br> 0 - wyłączone) ";
$aConfig['lang']['m_zamowienia_transport_means']['date_no_costs_from'] = "Brak kosztów tansportu od <br />(pozostaw puste jeśli promocja nieaktywna)";
$aConfig['lang']['m_zamowienia_transport_means']['free_delivery_header'] = "Czasowo ograniczona darmowa dostawa na wszystkie produkty";
$aConfig['lang']['m_zamowienia_transport_means']['start_date']	= "Data początkowa";
$aConfig['lang']['m_zamowienia_transport_means']['start_date_from']	= "zakres od";
$aConfig['lang']['m_zamowienia_transport_means']['end_date_to']	= "zakres do";
$aConfig['lang']['m_zamowienia_transport_means']['end_date']	= "Data końcowa";

$aConfig['lang']['m_zamowienia_transport_means']['add'] = "Dodaj środek transportu";

$aConfig['lang']['m_zamowienia_transport_means']['sort'] = "Sortuj metody transportu";
$aConfig['lang']['m_zamowienia_transport_means']['sort_items'] = "Sortowanie metod transportu";

$aConfig['lang']['m_zamowienia_transport_means']['header_0'] = "Dodawanie środka transportu";
$aConfig['lang']['m_zamowienia_transport_means']['header_1'] = "Edycja środka transportu";
$aConfig['lang']['m_zamowienia_transport_means']['name'] = "Przewoziciel";
$aConfig['lang']['m_zamowienia_transport_means']['symbol'] = "Symbol";
$aConfig['lang']['m_zamowienia_transport_means']['logo'] = "Logotyp";
$aConfig['lang']['m_zamowienia_transport_means']['personal_reception'] = "Odbiór osobisty";
$aConfig['lang']['m_zamowienia_transport_means']['synchronize'] = "Synchronizuj z nazwą";
$aConfig['lang']['m_zamowienia_transport_means']['add_ok'] = "Dodano środek transportu \"%s\"";
$aConfig['lang']['m_zamowienia_transport_means']['add_err'] = "Nie udało się dodać środka transportu \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_transport_means']['edit_ok'] = "Zmieniono środek transportu \"%s\"";
$aConfig['lang']['m_zamowienia_transport_means']['edit_err'] = "Nie udało się zmienić środka transportu \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_transport_means']['edit_err_1'] = "Data początkowa powinna być wcześniejsza od daty zakończenia !";
$aConfig['lang']['m_zamowienia_transport_means']['del_ok_0']		= "Usunięto środek transportu %s";
$aConfig['lang']['m_zamowienia_transport_means']['del_ok_1']		= "Usunięto środki transportu %s";
$aConfig['lang']['m_zamowienia_transport_means']['del_err']		= "Nie udało się usunąć środka transportu \"%s\"!<br>Spróbuj ponownie";

$aConfig['lang']['m_zamowienia_transport_means']['header_0'] = "Dodawanie ";
$aConfig['lang']['m_zamowienia_transport_means']['header_1'] = "Edycja ";
$aConfig['lang']['m_zamowienia_transport_means']['edit'] = "Edytuj";
$aConfig['lang']['m_zamowienia_transport_means']['add'] = "Dodaj ";
$aConfig['lang']['m_zamowienia_transport_means']['name'] = "Nazwa";
$aConfig['lang']['m_zamowienia_transport_means']['start_date'] = "Obowiązuje od";
$aConfig['lang']['m_zamowienia_transport_means']['end_date'] = "Obowiązuje do";
$aConfig['lang']['m_zamowienia_transport_means']['books'] = "Lista produktów";
$aConfig['lang']['m_zamowienia_transport_means_books']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_transport_means_books']['delete'] = "Usuń";
$aConfig['lang']['m_zamowienia_transport_means_books']['delete_q'] = "Czy na pewno usunąć?";
$aConfig['lang']['m_zamowienia_transport_means_books']['preview'] = "Podgląd";
$aConfig['lang']['m_zamowienia_transport_means_books']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_transport_means_books']['remove_all_q'] = "Czy na pewno usunąć wybrane produkty";
$aConfig['lang']['m_zamowienia_transport_means_books']['remove_all_err'] = "Nie wybrano żadnego produktu do usunięcia";
$aConfig['lang']['m_zamowienia_transport_means_remove']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_transport_means_remove']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_transport_means_remove']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_transport_means_insertBooks']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_transport_means_insertBooks']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_transport_means_insertBooks']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_transport_means_removeVal']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_transport_means_removeVal']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_transport_means_removeVal']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_transport_means']['add_books'] = "Dodaj stawkę rabatu";
$aConfig['lang']['m_zamowienia_transport_means']['add_from_file'] = "Dodaj z pliku";
$aConfig['lang']['m_zamowienia_transport_means']['add_ok'] = "Dodano \"%s\"";
$aConfig['lang']['m_zamowienia_transport_means']['add_err'] = "Nie udało się utworzyć!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_transport_means']['add_topromo_ok'] = "Dodano produkty  \"%s\"";
$aConfig['lang']['m_zamowienia_transport_means']['add_topromo_err'] = "Nie udało się dodać produktów \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_transport_means']['book_list2'] = "Lista książek wykluczonych z danego środka transportu \"%s\"";
$aConfig['lang']['m_zamowienia_transport_means']['info001'] = "Niektóre produkty były już dodane wcześniej i nie można dodać ich ponownie.";
$aConfig['lang']['m_zamowienia_transport_means']['date_start'] = "Początek";
$aConfig['lang']['m_zamowienia_transport_means']['start_time'] = "Od godz.";
$aConfig['lang']['m_zamowienia_transport_means']['date_end'] = "Koniec";
$aConfig['lang']['m_zamowienia_transport_means']['end_time'] = "Do godz.";

$aConfig['lang']['m_zamowienia_transport_means']['list_id'] = "Id";
$aConfig['lang']['m_zamowienia_transport_means']['list_name'] = "Tytuł";
$aConfig['lang']['m_zamowienia_transport_means']['book_list'] = "Lista książek";
$aConfig['lang']['m_zamowienia_transport_means']['list_pname'] = "Nazwa";
$aConfig['lang']['m_zamowienia_transport_means']['list_base_price'] = "Cena bazowa";
$aConfig['lang']['m_zamowienia_transport_means']['list_price_brutto'] = "Cena brutto";
$aConfig['lang']['m_zamowienia_transport_means']['no_books_to_show'] = "Brak pasujących pozycji";
$aConfig['lang']['m_zamowienia_transport_means']['new_discount_section'] = "Rabat";
$aConfig['lang']['m_zamowienia_transport_means']['start_date'] = "Obowiązuje od";
$aConfig['lang']['m_zamowienia_transport_means']['end_date'] = "Obowiązuje do";
$aConfig['lang']['m_zamowienia_transport_means']['del_discount'] = "Usuń wartość rabatu";
$aConfig['lang']['m_zamowienia_transport_means']['list_category'] = "Dział promocji";
$aConfig['lang']['m_zamowienia_transport_means']['hide'] = "Zwiń";
$aConfig['lang']['m_zamowienia_transport_means']['show'] = "Rozwiń";
$aConfig['lang']['m_zamowienia_transport_means']['new_discount_section'] = "Rabat";
$aConfig['lang']['m_zamowienia_transport_means']['discount'] = "Zmień wartość rabatu";
$aConfig['lang']['m_zamowienia_transport_means']['discount2'] = "Rabat";
$aConfig['lang']['m_zamowienia_transport_means']['discount3'] = "%";
$aConfig['lang']['m_zamowienia_transport_means']['product_name'] = "Nazwa produktu";
$aConfig['lang']['m_zamowienia_transport_means']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_zamowienia_transport_means']['choose_all'] = "Wszystkie";
$aConfig['lang']['m_zamowienia_transport_means']['add_product_w_disc'] = "Dodaj produkt do wykluczonych";
$aConfig['lang']['m_zamowienia_transport_means']['page_id'] = "Strona promocji";

$aConfig['lang']['m_zamowienia_transport_means']['add_ok'] = "Dodano promocję  \"%s\"";
$aConfig['lang']['m_zamowienia_transport_means']['add_err'] = "Nie udało się utworzyć promocji!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_transport_means']['add_topromo_ok'] = "Dodano produkty  \"%s\" do wykluczonych \"%s\"";
$aConfig['lang']['m_zamowienia_transport_means']['add_discount_ok'] = "Dodano stawki rabatu  \"%s\" do promocji \"%s\"";
$aConfig['lang']['m_zamowienia_transport_means']['add_topromo_err'] = "Nie udało się dodać produktów do promocji \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_transport_means']['edit_ok'] = "Wprowadzono zmianę w promocji \"%s\"";
$aConfig['lang']['m_zamowienia_transport_means']['edit_err'] = "Wystąpił błąd podczas próby zmiany w promocji \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_transport_means']['discount_value_err'] = "Błędna wartość rabatu - wybierz wartośc z zakresu 1-99!";
$aConfig['lang']['m_zamowienia_transport_means']['date_value_err'] = "Podane daty nie są prawidłowe!";
$aConfig['lang']['m_zamowienia_transport_means']['del_ok']		= "Usunięto promocję";
$aConfig['lang']['m_zamowienia_transport_means']['del_err']		= "Nie udało się usunąć promocji!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_transport_means']['del_prom_ok']		= "Usunięto produkt z promocji";
$aConfig['lang']['m_zamowienia_transport_means']['del_prom_err']		= "Nie udało się usunąć produktu z promocji!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_transport_means']['del_val_ok']		= "Usunięto produkt z wykluczonych produktów";
$aConfig['lang']['m_zamowienia_transport_means']['del_val_err']		= "Nie udało się usunąć produktu z wykluczonych!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_transport_means']['no_items'] = "Brak zdefiniowanych promocji";
$aConfig['lang']['m_zamowienia_transport_means_books']['no_items'] = "Brak książek w wybranej promocji";
$aConfig['lang']['m_zamowienia_transport_means']['edit'] = "Edytuj promocję";
$aConfig['lang']['m_zamowienia_transport_means']['delete'] = "Dodaj do wykluczonych";
$aConfig['lang']['m_zamowienia_transport_means']['delete_q'] = "Czy na pewno usunąć wybrany produkt z promocji?";
$aConfig['lang']['m_zamowienia_transport_means']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_transport_means']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_transport_means']['delete_all_q'] = "Czy na pewno usunąć wybrane produkty z promocji?";
$aConfig['lang']['m_zamowienia_transport_means']['delete_all_err'] = "Nie wybrano żadnego produktu do usunięcia z promocji!";

/*---------- dane sprzedawcy */
$aConfig['lang']['m_zamowienia_seller_data']['header'] = "Dane sprzedawcy";
$aConfig['lang']['m_zamowienia_seller_data']['seller_name'] = "Nazwa do przelewu";
$aConfig['lang']['m_zamowienia_seller_data']['seller_invoice_name'] = "Nazwa do faktury";
$aConfig['lang']['m_zamowienia_seller_data']['seller_paczkomaty_name'] = "Nazwa do paczkomatów";
$aConfig['lang']['m_zamowienia_seller_data']['seller_address'] = "Adres";
$aConfig['lang']['m_zamowienia_seller_data']['seller_street'] = "Ulica";
$aConfig['lang']['m_zamowienia_seller_data']['seller_number'] = "Numer";
$aConfig['lang']['m_zamowienia_seller_data']['seller_postal_city'] = "Kod i miasto";
$aConfig['lang']['m_zamowienia_seller_data']['seller_postal'] = "Kod pocztowy";
$aConfig['lang']['m_zamowienia_seller_data']['seller_phone'] = "Numer telefonu (paczkomaty)";
$aConfig['lang']['m_zamowienia_seller_data']['seller_city'] = "Miasto";
$aConfig['lang']['m_zamowienia_seller_data']['seller_nip'] = "NIP";
$aConfig['lang']['m_zamowienia_seller_data']['seller_regon'] = "REGON";
$aConfig['lang']['m_zamowienia_seller_data']['seller_bank_name'] = "Bank";
$aConfig['lang']['m_zamowienia_seller_data']['seller_bank_account'] = "Nr konta";
$aConfig['lang']['m_zamowienia_seller_data']['edit_ok'] = "Zmieniono dane sprzedawcy";
$aConfig['lang']['m_zamowienia_seller_data']['edit_err'] = "Nie udało się zmienić danych sprzedawcy!<br>Spróbuj ponownie";

/*---------- dane sprzedawcy */
$aConfig['lang']['m_zamowienia_mail_times']['header'] = "Dane sprzedawcy";
$aConfig['lang']['m_zamowienia_mail_times']['pay_reminder_days'] = "Ilośc dni roboczych po jakich zostanie wysłane przypomnienie o zapłacie (przelew)";
$aConfig['lang']['m_zamowienia_mail_times']['collect_info_days'] = "Ilośc dni roboczych po jakich zostanie wysłana informacja o odbiorze osobistym";
$aConfig['lang']['m_zamowienia_mail_times']['platnosci_reminder_days'] = "Ilośc dni roboczych po jakich zostanie wysłane przypomnienie o zapłacie (platnosci)";
$aConfig['lang']['m_zamowienia_mail_times']['cancel_days'] = "Ilośc dni roboczych po jakich zamówienie zostanie anulowane";
$aConfig['lang']['m_zamowienia_mail_times']['unactive_reminder'] = "Ilośc dni po których wysyłamy przypomnienie o niepotwierdzonym zamówieniu";
$aConfig['lang']['m_zamowienia_mail_times']['edit_ok'] = "Zmieniono ustawienia maili";
$aConfig['lang']['m_zamowienia_mail_times']['edit_err'] = "Nie udało się zmienić ustawień maili!<br>Spróbuj ponownie";


/*---------- ustawienia boksu */
$aConfig['lang']['m_zamowienia_box_settings']['page']		= "Strona zamówień";

/*---------- statystyki handlowe */
$aConfig['lang']['m_zamowienia_statistics']['list']	= "Statystyki handlowe";
$aConfig['lang']['m_zamowienia_statistics']['no_items']	= "Brak zdefiniowanych stron statystyk handlowych";
$aConfig['lang']['m_zamowienia_statistics']['show']	= "POKAŻ";
$aConfig['lang']['m_zamowienia_statistics']['hide']	= "UKRYJ";
$aConfig['lang']['m_zamowienia_statistics']['id']	= "ID";
$aConfig['lang']['m_zamowienia_statistics']['value_netto']	= "Wart. netto";
$aConfig['lang']['m_zamowienia_statistics']['value_brutto']	= "Wart. brutto";
$aConfig['lang']['m_zamowienia_statistics']['user']	= "[ID] login";
$aConfig['lang']['m_zamowienia_statistics']['hidden']	= "UKRYTE";
$aConfig['lang']['m_zamowienia_statistics']['user_name']	= "Nazwa";
$aConfig['lang']['m_zamowienia_statistics']['user_street']	= "Ulica";
$aConfig['lang']['m_zamowienia_statistics']['user_city']	= "Miasto";
$aConfig['lang']['m_zamowienia_statistics']['name']	= "Tytuł";
$aConfig['lang']['m_zamowienia_statistics']['orders']	= "Zamówień";
$aConfig['lang']['m_zamowienia_statistics']['total_ordered']	= "Sztuk";

/*---------- statystyki sprzedazy */
$aConfig['lang']['m_zamowienia_stats_zamowienia']['header']	= "Statystyki wielkości sprzedaży";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['button_show']	= "Pokaż statystyki";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['dates_range']	= "Przedział czasowy";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['start_date']	= "Data początkowa";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['start_date_from']	= "zakres od";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['end_date_to']	= "zakres do";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['end_date']	= "Data końcowa";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['order_status']	= "Zamówienia ze statusem";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['status_all']	= "wszystkie zamówienia";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['payment']	= "Metoda płatności";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['payment_all']	= "wszystkie metody";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['transport']	= "Środek transportu";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['transport_all']	= "wszystkie środki";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['orders_source'] = "Źródło zamówień";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['source'] = "Typ źródła";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['source_all'] = "wszystkie źródła";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['source_internal'] = "Profit";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['source_azymut'] = "Azymut";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['source_abe'] = "ABE";

$aConfig['lang']['m_zamowienia_stats_zamowienia']['header_statistics']	= "Statystyki sprzedaży dla wybranych warunków";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['conditions']	= "Warunki dla statystyk";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['statistics']	= "Statystyki";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['total_orders']	= "Złożonych zamówień";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['total_users']	= "Użytkownicy składający zamówienia";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['total_value']	= "Wartość zamówień - netto (brutto)";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['medial_value']	= "Średnia wartość zamówienia - netto (brutto)";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['total_products']	= "Zamówionych produktów";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['medial_products']	= "Średnio produktów na zamówienie";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['total_products_quantity']	= "Zamówionych sztuk produktów";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['medial_products_quantity']	= "Średnio sztuk produktów na zamówienie";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['max_value_orders']	= "10 zamówień o największej wartości";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['min_value_orders']	= "10 zamówień o najmniejszej wartości";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['most_popular_products']	= "10 najczęściej zamawianych produktów";
$aConfig['lang']['m_zamowienia_stats_zamowienia']['least_popular_products']	= "10 najrzadziej zamawianych produktów";

/*---------- statystyki produktow */
$aConfig['lang']['m_zamowienia_stats_produkty']['header']	= "Statystyki sprzedaży produktów";
$aConfig['lang']['m_zamowienia_stats_produkty']['button_show']	= "Pokaż statystyki";
$aConfig['lang']['m_zamowienia_stats_produkty']['dates_range']	= "Przedział czasowy";
$aConfig['lang']['m_zamowienia_stats_produkty']['start_date']	= "Data początkowa";
$aConfig['lang']['m_zamowienia_stats_produkty']['start_date_from']	= "zakres od";
$aConfig['lang']['m_zamowienia_stats_produkty']['end_date_to']	= "zakres do";
$aConfig['lang']['m_zamowienia_stats_produkty']['end_date']	= "Data końcowa";
$aConfig['lang']['m_zamowienia_stats_produkty']['order_status']	= "Zamówienia ze statusem";
$aConfig['lang']['m_zamowienia_stats_produkty']['status_all']	= "wszystkie zamówienia";
$aConfig['lang']['m_zamowienia_stats_produkty']['category']	= "Kategoria";
$aConfig['lang']['m_zamowienia_stats_produkty']['product_section']	= "Produkt";
$aConfig['lang']['m_zamowienia_stats_produkty']['product']	= "Produkt";
$aConfig['lang']['m_zamowienia_stats_produkty']['button_search']	= "Wyszukaj produkt";
$aConfig['lang']['m_zamowienia_stats_produkty']['product_name']	= "Id/Nazwa produktu";
$aConfig['lang']['m_zamowienia_stats_produkty']['product_isbn']	= "ISBN";

$aConfig['lang']['m_zamowienia_stats_produkty']['header_statistics']	= "Statystyki sprzedaży produktu dla wybranych warunków";
$aConfig['lang']['m_zamowienia_stats_produkty']['conditions']	= "Warunki dla statystyk";
$aConfig['lang']['m_zamowienia_stats_produkty']['statistics']	= "Statystyki";
$aConfig['lang']['m_zamowienia_stats_produkty']['total_orders']	= "Złożonych zamówień";
$aConfig['lang']['m_zamowienia_stats_produkty']['total_users']	= "Użytkownicy składający zamówienia";
$aConfig['lang']['m_zamowienia_stats_produkty']['total_value']	= "Wartość zamówień - netto (brutto)";
$aConfig['lang']['m_zamowienia_stats_produkty']['total_quantity']	= "Zamówionych sztuk produktu";
$aConfig['lang']['m_zamowienia_stats_produkty']['medial_value']	= "Średnia wartość zamówienia - netto (brutto)";
$aConfig['lang']['m_zamowienia_stats_produkty']['total_products']	= "Zamówionych produktów";
$aConfig['lang']['m_zamowienia_stats_produkty']['medial_products']	= "Średnio produktów na zamówienie";
$aConfig['lang']['m_zamowienia_stats_produkty']['total_products_quantity']	= "Zamówionych sztuk produktów";
$aConfig['lang']['m_zamowienia_stats_produkty']['medial_products_quantity']	= "Średnio sztuk produktów na zamówienie";
$aConfig['lang']['m_zamowienia_stats_produkty']['max_value_orders']	= "10 zamówień o największej wartości";
$aConfig['lang']['m_zamowienia_stats_produkty']['min_value_orders']	= "10 zamówień o najmniejszej wartości";
$aConfig['lang']['m_zamowienia_stats_produkty']['most_popular_products']	= "10 najczęściej zamawianych produktów";
$aConfig['lang']['m_zamowienia_stats_produkty']['least_popular_products']	= "10 najrzadziej zamawianych produktów";

$aConfig['lang']['m_zamowienia']['status_change_date']	= 'W dniu %s Twoje zamówienie nr %s otrzymało status: <strong>%s</strong>.';
$aConfig['lang']['m_zamowienia']['subject']	= 'Informacja o statusie twojego zamówienia';

/*-----------wysylanie maila ze statusem zamowienia */
$aConfig['lang']['m_zamowienia_mail']['list_name'] = 'Pozycja';
$aConfig['lang']['m_zamowienia_mail']['list_mail_name'] = 'Nazwa szablonu';
$aConfig['lang']['m_zamowienia_mail']['mail_name'] = 'Nazwa szablonu'; 
$aConfig['lang']['m_zamowienia_mail']['mail_title'] = 'Tytuł emaila'; 
$aConfig['lang']['m_zamowienia_mail']['mail_content'] = 'Treść emaila'; 
$aConfig['lang']['m_zamowienia_mail']['isbn'] = 'ISBN:';
$aConfig['lang']['m_zamowienia_mail']['publication_year'] = 'Rok wydania:';
$aConfig['lang']['m_zamowienia_mail']['edition'] = 'Wydanie:';
$aConfig['lang']['m_zamowienia_mail']['publisher']	= 'Wydawnictwo:';
$aConfig['lang']['m_zamowienia_mail']['author']	= 'Autor:';
$aConfig['lang']['m_zamowienia_mail']['shipment_time']	= 'Wysyłamy:';
$aConfig['lang']['m_zamowienia_mail']['shipment_preview']	= 'od';
$aConfig['lang']['m_zamowienia_mail']['list_price_brutto']	= 'Cena brutto';
$aConfig['lang']['m_zamowienia_mail']['list_quantity']	= 'Sztuk';
$aConfig['lang']['m_zamowienia_mail']['total_products_netto']	= 'Wartość netto';
$aConfig['lang']['m_zamowienia_mail']['total_products_brutto']	= 'Wartość brutto';
$aConfig['lang']['m_zamowienia_mail']['total_value']	= 'Wartość';
$aConfig['lang']['m_zamowienia_mail']['choose_payment_type']	= 'Sposób płatności';
$aConfig['lang']['m_zamowienia_mail']['transport_cost']	= 'Koszt transportu:';
$aConfig['lang']['m_zamowienia_mail']['total']	= 'Suma';
$aConfig['lang']['m_zamowienia_mail']['total_brutto'] = 'Łączna suma zamówienia:';
$aConfig['lang']['m_zamowienia_mail']['transport_type']	= 'Sposób wysyłki:';
$aConfig['lang']['m_zamowienia_mail']['total_cost']	= 'Całkowity koszt Twojego zamówienia:';
$aConfig['lang']['m_zamowienia_mail']['user_data_header']	= 'Dane zamawiającego';
$aConfig['lang']['m_zamowienia_mail']['seller_data_header']	= 'Dane niezbędne do dokonania przelewu';
$aConfig['lang']['m_zamowienia_mail']['address_data_header']	= 'Adres dostawy';
$aConfig['lang']['m_zamowienia_mail']['invoice_data_header']	= 'Dane do faktury';
$aConfig['lang']['m_zamowienia_mail']['order_content']	= 'Zawartość zamówienia:';
$aConfig['lang']['m_zamowienia_mail']['order_number']	= 'Zamówienie numer: ';
$aConfig['lang']['m_zamowienia_mail']['subject']	= 'Informacja o statusie twojego zamówienia';
$aConfig['lang']['m_zamowienia_mail']['send_ok']	= 'Informacja o statusie zamówienia została wysłana do klienta na adres %s';
$aConfig['lang']['m_zamowienia_mail']['send_err']	= 'Nie udało się wysłać informacji o statusie zamówienia';
$aConfig['lang']['m_zamowienia_mail']['send_payment_ok']	= 'Informacja o zmianie metody płatności została wysłana do klienta na adres %s';
$aConfig['lang']['m_zamowienia_mail']['send_payment_err']	= 'Nie udało się wysłać informacji o zmianie metody płatności';
$aConfig['lang']['m_zamowienia_mail']['status_change_date']	= 'W dniu %s Twoje zamówienie nr %s otrzymało status: <strong>%s</strong>.';
$aConfig['lang']['m_zamowienia_mail']['subject']	= 'Informacja o statusie twojego zamówienia';
$aConfig['lang']['m_zamowienia_mail']['subject_payment']	= 'Informacja o zmianie metody płatności twojego zamówienia';
$aConfig['lang']['m_zamowienia_mail']['selected_transport']	= 'Metoda transportu';
$aConfig['lang']['m_zamowienia_mail']['selected_payment']	= 'Metoda płatności';
$aConfig['lang']['m_zamowienia_mail']['seller_data_header']	= 'Dane do przelewu';
$aConfig['lang']['m_zamowienia_mail']['total_cost']	= 'Łączna suma zamówienia:';
$aConfig['lang']['m_zamowienia_mail']['to_pay']	= 'Kwota do zapłaty:';
$aConfig['lang']['m_zamowienia_mail']['total_cost_brutto']	= 'Suma zamówienia:';
$aConfig['lang']['m_zamowienia_mail']['total_transport_cost']	= 'Koszt dostawy:';
$aConfig['lang']['m_zamowienia_mail']['name']	= 'Imię';
$aConfig['lang']['m_zamowienia_mail']['surname']	= 'Nazwisko';
$aConfig['lang']['m_zamowienia_mail']['company']	= 'Firma';
$aConfig['lang']['m_zamowienia_mail']['street']	= 'Ulica';
$aConfig['lang']['m_zamowienia_mail']['number']	= 'Nr domu:';
$aConfig['lang']['m_zamowienia_mail']['number2']	= 'Nr lokalu';
$aConfig['lang']['m_zamowienia_mail']['postal']	= 'Kod pocztowy';
$aConfig['lang']['m_zamowienia_mail']['phone']	= 'Nr telefonu';
$aConfig['lang']['m_zamowienia_mail']['city']	= 'Miejscowość';
$aConfig['lang']['m_zamowienia_mail']['nip']	= 'NIP';
$aConfig['lang']['m_zamowienia_mail']['namesurname']	= 'Imię i nazwisko';
$aConfig['lang']['m_zamowienia_mail']['address']	= 'Adres';
$aConfig['lang']['m_zamowienia_mail']['seller_name']	= 'Nazwa:';
$aConfig['lang']['m_zamowienia_mail']['seller_bank']	= 'Bank:';
$aConfig['lang']['m_zamowienia_mail']['seller_bank_account']	= 'Nr konta:';
$aConfig['lang']['m_zamowienia_mail']['inv_postal']	= 'Kod pocztowy:';
$aConfig['lang']['m_zamowienia_mail']['inv_city']	= 'Miasto:';
$aConfig['lang']['m_zamowienia_mail']['inv_company']	= 'Firma lub instytucja';
$aConfig['lang']['m_zamowienia_mail']['inv_street']	= 'Ulica:';
$aConfig['lang']['m_zamowienia_mail']['remarks']	= 'Uwagi dotyczące Twojego zamówienia';
$aConfig['lang']['m_zamowienia_mail']['transport_remarks']	= 'Uwagi dla kuriera:';
$aConfig['lang']['m_zamowienia_mail']['new_order_user_email_client_ip'] = 'Twoje zamówienie zostało złożone z adresu IP:';
$aConfig['lang']['m_zamowienia_mail']['new_order_user_email_footer_info'] = 'Pozdrawiamy,<br>
Zespół Profit24.pl<br>
<br>
Profit M Spółka z ograniczoną odpowiedzialnością<br>
Al. Jerozolimskie 134<br>
02-305 Warszawa<br>
<a href="https://www.profit24.pl/kontakt-z-nami/" target="_blank" style="color: #c30500;">https://www.profit24.pl/kontakt-z-nami/</a>';
$aConfig['lang']['m_zamowienia_mail']['new_order_user_email_footer_info2'] = 'Prosimy NIE odpowiadać na tę wiadomość, ponieważ wysłana jest z adresu e-mail przeznaczonego do przesyłania komunikatów systemowych i poczta przychodząca nie jest akceptowana.<br>
<br>
W przypadku pytań lub wątpliwości proszę odwiedzić Centrum Pomocy Profit24.pl na stronie <a href="https://www.profit24.pl/kontakt-z-nami/" target="_blank" style="color: #c30500;">https://www.profit24.pl/kontakt-z-nami/</a> i wybrać dogodną dla siebie formę kontaktu z naszym zespołem.';

// rabat grupowy
$aConfig['lang']['m_zamowienia_ordered_items']['csv_header_1'] = 'Pozycje do zamówienia nr %s';
$aConfig['lang']['m_zamowienia_ordered_items']['txt_header_1'] = 'Pozycje do zamówienia nr %s';
$aConfig['lang']['m_zamowienia_ordered_items']['mail_header_1'] = 'Pozycje do zamówienia';
$aConfig['lang']['m_zamowienia_ordered_items']['csv_header_3'] = 'Pozycje niepotwierdzone nr %s';
$aConfig['lang']['m_zamowienia_ordered_items']['txt_header_3'] = 'Pozycje niepotwierdzone nr %s';
$aConfig['lang']['m_zamowienia_ordered_items']['mail_header_3'] = 'Pozycje niepotwierdzone';
$aConfig['lang']['m_zamowienia_ordered_items']['ordered_items_header'] = 'Zamówione pozycje';
$aConfig['lang']['m_zamowienia_ordered_items']['send_ordered_items_header'] = 'Wyślij zamówione pozycje';
$aConfig['lang']['m_zamowienia_ordered_items']['address_book'] = 'Wybierz email z księżki adresowej';
$aConfig['lang']['m_zamowienia_ordered_items']['define_section'] = 'Zdefiniuj nowy kontakt';
$aConfig['lang']['m_zamowienia_ordered_items']['define_email'] = 'Email';
$aConfig['lang']['m_zamowienia_ordered_items']['define_name'] = 'Imię';
$aConfig['lang']['m_zamowienia_ordered_items']['define_surname'] = 'Nazwisko';
$aConfig['lang']['m_zamowienia_ordered_items']['value_to'] = 'Do';
$aConfig['lang']['m_zamowienia_ordered_items']['send'] = 'Wyślij';
$aConfig['lang']['m_zamowienia_ordered_items']['orders'] = 'Zamówione pozycje';
$aConfig['lang']['m_zamowienia_ordered_items']['orders_header_1'] = 'Zamówienie do %s nr %s';
$aConfig['lang']['m_zamowienia_ordered_items']['orders_header_3'] = 'Zamówienie do %s nr %s';
$aConfig['lang']['m_zamowienia_ordered_items']['orders_footer_1'] = 'Zebrał';
$aConfig['lang']['m_zamowienia_ordered_items']['orders_footer_2'] = 'Odznaczył';
$aConfig['lang']['m_zamowienia_ordered_items']['orders_genby'] = 'Wygenerowany przez';
$aConfig['lang']['m_zamowienia_ordered_items']['orders_footer_3'] = 'Przesunął';

$aConfig['lang']['m_zamowienia_ordered_items']['date_range'] = 'Przedział czasowy składanych zamówień';
$aConfig['lang']['m_zamowienia_ordered_items']['date_to'] = 'Do';
$aConfig['lang']['m_zamowienia_ordered_items']['no_items'] = "<strong>Brak zdefiniowanych pozycji</strong>";

$aConfig['lang']['m_zamowienia_ordered_items']['source'] = "Źródło dostawy";
$aConfig['lang']['m_zamowienia_ordered_items']['select_source'] = "Wybierz źródło";

$aConfig['lang']['m_zamowienia_ordered_items']['source_0'] = "Profit J";
$aConfig['lang']['m_zamowienia_ordered_items']['source_1'] = "Profit E";
$aConfig['lang']['m_zamowienia_ordered_items']['source_5'] = "Profit M";
$aConfig['lang']['m_zamowienia_ordered_items']['source_51'] = "Stock";

$aConfig['lang']['m_zamowienia_ordered_items']['sources'][0] = 'Profit J';
$aConfig['lang']['m_zamowienia_ordered_items']['sources'][1] = 'Profit E';
$aConfig['lang']['m_zamowienia_ordered_items']['sources'][5] = 'Profit M';
$aConfig['lang']['m_zamowienia_ordered_items']['sources'][51] = 'Stock';


$aConfig['lang']['m_zamowienia_ordered_items']['source_52'] = "Profit X";
$aConfig['lang']['m_zamowienia_ordered_items']['source_2'] = "Azymut";
$aConfig['lang']['m_zamowienia_ordered_items']['source_3'] = "ABE";
$aConfig['lang']['m_zamowienia_ordered_items']['source_4'] = "Helion";
// XXX tutaj do orders_items, ale jakoś źle to wygląda na pewno będzie to trzeba pozmieniać
$aConfig['lang']['m_zamowienia_ordered_items']['source_5'] = "Ateneum";
$aConfig['lang']['m_zamowienia_ordered_items']['saved_0'] = "CSV";
$aConfig['lang']['m_zamowienia_ordered_items']['saved_1'] = "TXT";
$aConfig['lang']['m_zamowienia_ordered_items']['saved_2'] = "Mail";
$aConfig['lang']['m_zamowienia_ordered_items']['saved_4'] = "XML";
$aConfig['lang']['m_zamowienia_ordered_items']['status_1'] = "do zamówienia";
$aConfig['lang']['m_zamowienia_ordered_items']['status_3'] = "jest - niepotwierdzony";
$aConfig['lang']['m_zamowienia_ordered_items']['status'] = "Produkty z statusem";
$aConfig['lang']['m_zamowienia_ordered_items']['button_show_users'] = "Wyszukaj";
$aConfig['lang']['m_zamowienia_ordered_items']['found_books'] = 'Wynik wyszukiwania';
$aConfig['lang']['m_zamowienia_ordered_items']['sum'] = 'SUMA';
$aConfig['lang']['m_zamowienia_ordered_items']['button_get_csv'] = 'Pobierz CSV';
$aConfig['lang']['m_zamowienia_ordered_items']['button_get_azymut'] = 'Pobierz Azymut';
$aConfig['lang']['m_zamowienia_ordered_items']['button_get_ateneum'] = 'Pobierz Ateneum';
$aConfig['lang']['m_zamowienia_ordered_items']['button_get_super_siodemka'] = 'Pobierz Super Siódemka';
$aConfig['lang']['m_zamowienia_ordered_items']['button_get_olesiejuk'] = 'Pobierz Olesiejuk';
$aConfig['lang']['m_zamowienia_ordered_items']['button_get_platforma'] = 'Pobierz Platforma';
$aConfig['lang']['m_zamowienia_ordered_items']['button_get_mail'] = 'Wyślij mail';
$aConfig['lang']['m_zamowienia_ordered_items']['button_get_pdf'] = 'Pobierz PDF';
$aConfig['lang']['m_zamowienia_ordered_items']['button_get_pdf_ext'] = 'Pobierz PDF zewnętrzny';
$aConfig['lang']['m_zamowienia_ordered_items']['book_id'] = 'Kupili produkt';
$aConfig['lang']['m_zamowienia_ordered_items']['books_list_id'] = 'Id';
$aConfig['lang']['m_zamowienia_ordered_items']['books_list_lp'] = 'Lp';
$aConfig['lang']['m_zamowienia_ordered_items']['books_list_publisher'] = 'Wydawnictwo';
$aConfig['lang']['m_zamowienia_ordered_items']['books_list_year'] = 'Rok wydania';
$aConfig['lang']['m_zamowienia_ordered_items']['books_list_name'] = 'Nazwa';
$aConfig['lang']['m_zamowienia_ordered_items']['books_list_isbn'] = 'ISBN';
$aConfig['lang']['m_zamowienia_ordered_items']['books_list_azymut_index'] = 'Azymut BookIndeks';
$aConfig['lang']['m_zamowienia_ordered_items']['books_list_ile'] = 'Ilość zam. egz.';
$aConfig['lang']['m_zamowienia_ordered_items']['books_bookindeks'] = 'Bookindeks';
$aConfig['lang']['m_zamowienia_ordered_items']['no_books_to_show'] = "Brak zamówień spełniających zadane kryteria";
$aConfig['lang']['m_zamowienia_ordered_items']['send_ok'] = "Email został wysłany do %s";
$aConfig['lang']['m_zamowienia_ordered_items']['send_err'] = "Wystąpił błąd podczas próby przesłania emaila %s<br />";
$aConfig['lang']['m_zamowienia_ordered_items']['duplicate_err'] = "Podany email występuje już w bazie adresów %s<br />";

$aConfig['lang']['m_zamowienia_ordered_items']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_ordered_items']['button_set_ordered'] = 'Oznacz jako zamówione';

$aConfig['lang']['m_zamowienia_ordered_items_OLD'] = $aConfig['lang']['m_zamowienia_ordered_items'];
// wysłane emaile
$aConfig['lang']['m_zamowienia_send_history']['print_next_time'] = "Drukuj ponownie";
$aConfig['lang']['m_zamowienia_send_history']['details'] = 'Szczegóły';
$aConfig['lang']['m_zamowienia_send_history']['email'] = 'Email';
$aConfig['lang']['m_zamowienia_send_history']['number'] = 'Numer'; 
$aConfig['lang']['m_zamowienia_send_history']['date_send'] = 'Data wysłania'; 
$aConfig['lang']['m_zamowienia_send_history']['send_by'] = 'Wygenerował'; 
$aConfig['lang']['m_zamowienia_send_history']['source'] = 'Dostawca'; 
$aConfig['lang']['m_zamowienia_send_history']['history'] = 'Historia wysłanych/pobranych zamówionych pozycji'; 
$aConfig['lang']['m_zamowienia_send_history']['f_source'] = 'Dostawca'; 
$aConfig['lang']['m_zamowienia_send_history']['come_in'] = 'Dostawa przyjechała na magazyn';

$aConfig['lang']['m_zamowienia_send_history']['fv_nr'] = 'Numer faktury'; 
$aConfig['lang']['m_zamowienia_send_history']['ident_nr'] = 'Numer zamówienia w źródle'; 
$aConfig['lang']['m_zamowienia_confirm_items']['fv_nr'] = $aConfig['lang']['m_zamowienia_send_history']['fv_nr']; 
$aConfig['lang']['m_zamowienia_confirm_items']['ident_nr'] = $aConfig['lang']['m_zamowienia_send_history']['ident_nr']; 
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['fv_nr'] = $aConfig['lang']['m_zamowienia_send_history']['fv_nr']; 
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['ident_nr'] = $aConfig['lang']['m_zamowienia_send_history']['ident_nr']; 

$aConfig['lang']['m_zamowienia_send_history']['details_header_0'] = 'Szczegóły pobranego pliku CSV';
$aConfig['lang']['m_zamowienia_send_history']['details_header_1'] = 'Szczegóły pobranego pliku TXT'; 
$aConfig['lang']['m_zamowienia_send_history']['details_header_2'] = 'Szczegóły wysłanego maila';
$aConfig['lang']['m_zamowienia_send_history']['details_header_3'] = 'Szczegóły pobranego pliku PDF';   
$aConfig['lang']['m_zamowienia_send_history']['details_header_4'] = 'Szczegóły pobranego pliku XML';   
$aConfig['lang']['m_zamowienia_send_history']['details_header_5'] = 'Szczegóły pobranego pliku XLSX';   
$aConfig['lang']['m_zamowienia_send_history']['details_email'] = 'Odbiorca';
$aConfig['lang']['m_zamowienia_send_history']['details_send_by'] = 'Wygenerował';
$aConfig['lang']['m_zamowienia_send_history']['saved_type'] = "Typ wpisu";
$aConfig['lang']['m_zamowienia_send_history']['items_type'] = "Produkty";
$aConfig['lang']['m_zamowienia_send_history']['status_1'] = "do zam.";
$aConfig['lang']['m_zamowienia_send_history']['status_3'] = "jest - niepotw.";

$aConfig['lang']['m_zamowienia_send_history']['saved_0'] = "CSV";
$aConfig['lang']['m_zamowienia_send_history']['saved_1'] = "TXT";
$aConfig['lang']['m_zamowienia_send_history']['saved_2'] = "Mail";
$aConfig['lang']['m_zamowienia_send_history']['saved_3'] = "PDF";
$aConfig['lang']['m_zamowienia_send_history']['saved_4'] = "XML";
$aConfig['lang']['m_zamowienia_send_history']['saved_5'] = "XLSX";

$aConfig['lang']['m_zamowienia_send_history']['source_0'] = "Profit J";
$aConfig['lang']['m_zamowienia_send_history']['source_1'] = "Profit E";
$aConfig['lang']['m_zamowienia_send_history']['source_5'] = "Profit M";
$aConfig['lang']['m_zamowienia_send_history']['source_51'] = "Stock";
$aConfig['lang']['m_zamowienia_send_history']['source_52'] = "Profit X";

$aConfig['lang']['m_zamowienia_send_history']['details_date_send'] = 'Data wysłania';
$aConfig['lang']['m_zamowienia_send_history']['details_login_section'] = 'Dane wysłanego zamówienia';  

$aConfig['lang']['m_zamowienia_send_history']['details_content_section'] = 'Treść wysłanego/pobranego zamówienia';  
$aConfig['lang']['m_zamowienia_send_history']['details_content'] = 'Treść wysłanego/pobranego zamówienia';

/*---------- rabat ogólny */
$aConfig['lang']['m_zamowienia_shipment_times']['header'] = "Edycja czasów wysyłki";
$aConfig['lang']['m_zamowienia_shipment_times']['shipment_days'] = "Czas wysyłki";
$aConfig['lang']['m_zamowienia_shipment_times']['transport_days'] = "Czas transportu";
$aConfig['lang']['m_zamowienia_shipment_times']['shipment_0_section'] = "Od ręki";
$aConfig['lang']['m_zamowienia_shipment_times']['shipment_1_section'] = "1-2 dni";
$aConfig['lang']['m_zamowienia_shipment_times']['shipment_2_section'] = "7-9 dni";
$aConfig['lang']['m_zamowienia_shipment_times']['shipment_3_section'] = "zapowiedź";
$aConfig['lang']['m_zamowienia_shipment_times']['edit_ok'] = "Zmieniono czasy wysyłki";
$aConfig['lang']['m_zamowienia_shipment_times']['edit_err'] = "Nie udało się zmienić czasów wysyłki!<br><br>Spróbuj ponownie";
/*---------- dni wolne */
$aConfig['lang']['m_zamowienia_free_days']['list'] = "Lista dni wolnych";
$aConfig['lang']['m_zamowienia_free_days']['list_free_day'] = "Dzień wolny";
$aConfig['lang']['m_zamowienia_free_days']['no_items'] = "Brak zdefiniowanych dni wolnych";
$aConfig['lang']['m_zamowienia_free_days']['edit'] = "Edytuj dzień wolny";
$aConfig['lang']['m_zamowienia_free_days']['delete'] = "Usuń dzień wolny";
$aConfig['lang']['m_zamowienia_free_days']['delete_q'] = "Czy na pewno usunąć wybrany dzień wolny?";
$aConfig['lang']['m_zamowienia_free_days']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_free_days']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_free_days']['delete_all_q'] = "Czy na pewno usunąć wybrane dni wolne?";
$aConfig['lang']['m_zamowienia_free_days']['delete_all_err'] = "Nie wybrano żadnego dnia wolnego do usunięcia!";
$aConfig['lang']['m_zamowienia_free_days']['add'] = "Dodaj dzień wolny";

$aConfig['lang']['m_zamowienia_free_days']['header_0'] = "Dodawanie dnia wolnego";
$aConfig['lang']['m_zamowienia_free_days']['header_1'] = "Edycja dnia wolnego";
$aConfig['lang']['m_zamowienia_free_days']['free_day'] = "Dzień wolny";

$aConfig['lang']['m_zamowienia_free_days']['add_ok'] = "Dodano dzień wolny \"%s\"";
$aConfig['lang']['m_zamowienia_free_days']['add_err'] = "Nie udało się dodać dnia wolnego \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_free_days']['edit_ok'] = "Zmieniono dzien wolny \"%s\"";
$aConfig['lang']['m_zamowienia_free_days']['edit_err'] = "Nie udało się zmienić dnia wolnego \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_free_days']['del_ok_0']		= "Usunięto dzień wolny %s";
$aConfig['lang']['m_zamowienia_free_days']['del_ok_1']		= "Usunięto dni wolne %s";
$aConfig['lang']['m_zamowienia_free_days']['del_err_0']		= "Nie udało się usunąć dnia wolnego %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_free_days']['del_err_1']		= "Nie udało się usunąć dni wolnych %s!<br>Spróbuj ponownie";

/*$aConfig['lang']['m_zamowienia_statements']['list'] = '';
$aConfig['lang']['m_zamowienia_statements']['list'] = '';*/

/*-----------Przelewy Pobrania */
$aConfig['lang']['m_zamowienia_list_opek']['f_status'] = "Status przelewu";
$aConfig['lang']['m_zamowienia_list_opek']['f_all'] = "Wszystkie";
$aConfig['lang']['m_zamowienia_list_opek']['f_deleted'] = "Usunięty";
$aConfig['lang']['m_zamowienia_list_opek']['f_new'] = "Nierozliczony";
$aConfig['lang']['m_zamowienia_list_opek']['f_matched'] = "Rozliczony";
$aConfig['lang']['m_zamowienia_list_opek']['go_back'] = "Powrót na listy przelewów";
$aConfig['lang']['m_zamowienia_list_opek']['no_linked_order'] = "<span style='color: red'>Poza zamówieniami</span>";
$aConfig['lang']['m_zamowienia_list_opek']['no_linked'] = "<span style='color: red'>Nie powiązane</span>";

$aConfig['lang']['m_zamowienia_list_opek']['list'] = 'Przelewy Pobrania';
$aConfig['lang']['m_zamowienia_list_opek']['no_items'] = 'Brak przelewów';
$aConfig['lang']['m_zamowienia_list_opek']['payment_from_day'] = 'Przelew z dnia ';
$aConfig['lang']['m_zamowienia_list_opek']['for_ammount'] = ' na kwotę ';
$aConfig['lang']['m_zamowienia_list_opek']['send_button_save'] = 'Zatwierdź';
$aConfig['lang']['m_zamowienia_list_opek']['delete_from_opek'] = 'Usuń';
$aConfig['lang']['m_zamowienia_list_opek']['del_err'] = 'Wystąpił błąd podczas usuwania przelewu. Spróbuj ponownie.';
$aConfig['lang']['m_zamowienia_list_opek']['del_ok'] = 'Usunięto przelew z OPEK';
$aConfig['lang']['m_zamowienia_list_opek']['status_edit_err'] = 'Wystąpił bład podczas zatwierdzania przelewów. Spróbuj ponownie.';
$aConfig['lang']['m_zamowienia_list_opek']['status_edit_ok'] = 'Przelewy zostały zatwierdzone';
$aConfig['lang']['m_zamowienia_list_opek']['save_err_unique'] = 'Wystąpił błąd podczas edycji przelewu: <li>Użyto dwa razy tego samego nr zamówienia</li>';

$aConfig['lang']['m_zamowienia_list_opek']['edit_err'] = 'Wystąpił błąd podczas edycji przelewu. Spróbuj ponownie.';
$aConfig['lang']['m_zamowienia_list_opek']['edit_ok'] = 'Zmieniono przelew z OPEK';

/*-----------faktura */
$aConfig['lang']['m_zamowienia_invoice']['list_lp'] = 'Lp.';
$aConfig['lang']['m_zamowienia_invoice']['list_name'] = 'Tytuł';
$aConfig['lang']['m_zamowienia_invoice']['list_symbol']	= 'Symbol<br /> ISBN / PKWiU';
$aConfig['lang']['m_zamowienia_invoice']['list_quantity']	= 'Ilość';
$aConfig['lang']['m_zamowienia_invoice']['list_price_brutto']	= 'Cena detaliczna [zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_discount']	= 'Rabat [%]';
$aConfig['lang']['m_zamowienia_invoice']['list_promo_price_brutto']	= 'Cena po<br /> rabacie<br /> [zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_value_netto']	= 'Wartość bez podatku VAT [zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_st']	= 'st [%]';
$aConfig['lang']['m_zamowienia_invoice']['list_price']	= 'Kwota <br />[zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_vat']	= 'Podatek';
$aConfig['lang']['m_zamowienia_invoice']['list_jm']	= 'jed. m.';
$aConfig['lang']['m_zamowienia_invoice']['list_value_brutto']	= 'Wartość z podatkiem [zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_street']	= 'ul.';
$aConfig['lang']['m_zamowienia_invoice']['list_nip']	= 'NIP';
$aConfig['lang']['m_zamowienia_invoice']['list_regon'] = "REGON";
$aConfig['lang']['m_zamowienia_invoice']['list_vat_nr'] = 'Faktura VAT nr';
$aConfig['lang']['m_zamowienia_invoice']['list_city_day'] = 'Warszawa, dnia:';
$aConfig['lang']['m_zamowienia_invoice']['list_data_order'] = 'Data sprzedaży:';
$aConfig['lang']['m_zamowienia_invoice']['oryginal_copy'] = 'oryginał | kopia';
$aConfig['lang']['m_zamowienia_invoice']['list_price_vat']	= 'Kwota podatku [zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_vat_reception']	= 'Podpis i pieczęć osoby upoważnionej<br />do odbioru faktury VAT';
$aConfig['lang']['m_zamowienia_invoice']['list_vat_acting']	= 'podpis i pieczęć osoby upoważnionej<br />do wystawienia faktury VAT';
$aConfig['lang']['m_zamowienia_invoice']['list_vat_data']	= 'data odbioru';
$aConfig['lang']['m_zamowienia_invoice']['list_pay']	= 'Do zapłaty';
$aConfig['lang']['m_zamowienia_invoice']['list_dictionary']	= 'Słownie:';
$aConfig['lang']['m_zamowienia_invoice']['list_transfer']	= 'Tytuł przelewu:';
$aConfig['lang']['m_zamowienia_invoice']['list_number_account']	= 'Nr konta:';
$aConfig['lang']['m_zamowienia_invoice']['list_bank']	= 'Bank:';
$aConfig['lang']['m_zamowienia_invoice']['list_reciptient']	= 'Odbiorca:';
$aConfig['lang']['m_zamowienia_invoice']['list_together']	= 'RAZEM:';
$aConfig['lang']['m_zamowienia_invoice']['list_transport_nubmer']	= 'Nr listu przewozowego';

$aConfig['lang']['m_zamowienia_invoice']['isbn'] = 'ISBN:';
$aConfig['lang']['m_zamowienia_invoice']['publication_year'] = 'Rok wydania:';
$aConfig['lang']['m_zamowienia_invoice']['edition'] = 'Wydanie:';
$aConfig['lang']['m_zamowienia_invoice']['publisher']	= 'Wydawnictwo:';
$aConfig['lang']['m_zamowienia_invoice']['author']	= 'Autor:';
$aConfig['lang']['m_zamowienia_invoice']['shipment_time']	= 'Wysyłamy:';
$aConfig['lang']['m_zamowienia_invoice']['shipment_preview']	= 'od';
$aConfig['lang']['m_zamowienia_invoice']['jm_book']	= 'egz.';
$aConfig['lang']['m_zamowienia_invoice']['jm_attach']	= 'szt.';

$aConfig['lang']['m_zamowienia_invoice']['total_products_netto']	= 'Wartość netto';
$aConfig['lang']['m_zamowienia_invoice']['total_products_brutto']	= 'Wartość brutto';
$aConfig['lang']['m_zamowienia_invoice']['total_value']	= 'Wartość';
$aConfig['lang']['m_zamowienia_invoice']['choose_payment_type']	= 'Sposób płatności';
$aConfig['lang']['m_zamowienia_invoice']['transport_cost']	= 'Koszt transportu:';
$aConfig['lang']['m_zamowienia_invoice']['total']	= 'Suma';
$aConfig['lang']['m_zamowienia_invoice']['total_brutto'] = 'Łączna suma zamówienia:';
$aConfig['lang']['m_zamowienia_invoice']['transport_type']	= 'Sposób wysyłki:';
$aConfig['lang']['m_zamowienia_invoice']['total_cost']	= 'Całkowity koszt Twojego zamówienia:';
$aConfig['lang']['m_zamowienia_invoice']['user_data_header']	= 'Dane sprzedawcy:';
$aConfig['lang']['m_zamowienia_invoice']['seller_data_header']	= 'Dane niezbędne do dokonania przelewu';
$aConfig['lang']['m_zamowienia_invoice']['address_data_header']	= 'Adres dostawy';
$aConfig['lang']['m_zamowienia_invoice']['invoice_data_header']	= 'Dane nabywcy:';
$aConfig['lang']['m_zamowienia_invoice']['order_content']	= 'Zawartość zamówienia:';
$aConfig['lang']['m_zamowienia_invoice']['order_number']	= 'Zamówienia numer: ';
$aConfig['lang']['m_zamowienia_invoice']['subject']	= 'Informacja o statusie twojego zamówienia';
$aConfig['lang']['m_zamowienia_invoice']['send_ok']	= 'Informacja o statusie zamówienia została wysłana do klienta na adres %s';
$aConfig['lang']['m_zamowienia_invoice']['send_err']	= 'Nie udało się wysłać informacji o statusie zamówienia';
$aConfig['lang']['m_zamowienia_invoice']['send_payment_ok']	= 'Informacja o zmianie metody płatności została wysłana do klienta na adres %s';
$aConfig['lang']['m_zamowienia_invoice']['send_payment_err']	= 'Nie udało się wysłać informacji o zmianie metody płatności';
$aConfig['lang']['m_zamowienia_invoice']['status_change_date']	= 'W dniu %s Twoje zamówienie nr %s otrzymało status: <strong>%s</strong>.';
$aConfig['lang']['m_zamowienia_invoice']['subject']	= 'Informacja o statusie twojego zamówienia';
$aConfig['lang']['m_zamowienia_invoice']['subject_payment']	= 'Informacja o zmianie metody płatności twojego zamówienia';
$aConfig['lang']['m_zamowienia_invoice']['selected_transport']	= 'Transportu:';
$aConfig['lang']['m_zamowienia_invoice']['selected_payment']	= 'Płatności:';
$aConfig['lang']['m_zamowienia_invoice']['seller_data_header']	= 'Dane do przelewu';
$aConfig['lang']['m_zamowienia_invoice']['total_cost_brutto']	= 'Suma brutto';
$aConfig['lang']['m_zamowienia_invoice']['total_transport_cost']	= 'Koszt transportu';
$aConfig['lang']['m_zamowienia_invoice']['transport_invoice_item']	= 'Koszt transportu';

/*---------- wagi opakowań */
$aConfig['lang']['m_zamowienia_package_weight']['header'] = "Edycja wagi opakowań";
$aConfig['lang']['m_zamowienia_package_weight']['package_weight'] = "Waga opakowania (kg)";
$aConfig['lang']['m_zamowienia_package_weight']['edit_ok'] = "Zmieniono wagi opakowań";
$aConfig['lang']['m_zamowienia_package_weight']['edit_err'] = "Nie udało się zmienić wag opakowań!<br><br>Spróbuj ponownie";

/*---------- Kody rabatowe */
$aConfig['lang']['m_zamowienia_codes']['list'] = "Lista kodów rabatowych";
$aConfig['lang']['m_zamowienia_codes']['f_activation_state'] = "Czas obowiazywania";
$aConfig['lang']['m_zamowienia_codes']['f_all'] = "Wszystkie";
$aConfig['lang']['m_zamowienia_codes']['f_active'] = "Aktualne";
$aConfig['lang']['m_zamowienia_codes']['f_not_active'] = "Nie rozpoczęte";
$aConfig['lang']['m_zamowienia_codes']['f_used'] = "Wykorzystane";
$aConfig['lang']['m_zamowienia_codes']['list_code'] = "Kod";
$aConfig['lang']['m_zamowienia_codes']['code'] = "Kod";
$aConfig['lang']['m_zamowienia_codes']['list_code_discount'] = "Rabat (%)";
$aConfig['lang']['m_zamowienia_codes']['list_code_active_from'] = "Data początkowa";
$aConfig['lang']['m_zamowienia_codes']['list_code_expiry'] = "Ważny do";
$aConfig['lang']['m_zamowienia_codes']['books'] = "Książki kodu rabatowego";
$aConfig['lang']['m_zamowienia_codes']['used_count'] = "Użyty";
$aConfig['lang']['m_zamowienia_codes']['desc'] = "Opis";
$aConfig['lang']['m_zamowienia_codes']['extra'] = "Atrybyty";
$aConfig['lang']['m_zamowienia_codes']['list_code_max_use'] = "Użyć / użytkownika";
$aConfig['lang']['m_zamowienia_codes']['list_grant_to_new_users'] = "Po rej.";
$aConfig['lang']['m_zamowienia_codes']['list_grant_after_poll'] = "Po ankiecie";
$aConfig['lang']['m_zamowienia_codes']['no_items'] = "Brak zdefiniowanych kodów rabatowych";
$aConfig['lang']['m_zamowienia_codes']['edit'] = "Edytuj kod rabatowy";
$aConfig['lang']['m_zamowienia_codes']['delete'] = "Usuń kod rabatowy";
$aConfig['lang']['m_zamowienia_codes']['delete_q'] = "Czy na pewno usunąć wybrany kod rabatowy?";
$aConfig['lang']['m_zamowienia_codes']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_codes']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_codes']['delete_all_q'] = "Czy na pewno usunąć wybrane kody rabatowe?";
$aConfig['lang']['m_zamowienia_codes']['delete_all_err'] = "Nie wybrano żadnego kodu rabatowego do usunięcia!";
$aConfig['lang']['m_zamowienia_codes']['logs'] = "Logi rozsyłki";
$aConfig['lang']['m_zamowienia_codes']['add'] = "Dodaj kod rabatowy";
$aConfig['lang']['m_zamowienia_codes']['users'] = "Użytkownicy korzystający z kodu";
$aConfig['lang']['m_zamowienia_codes']['send'] = "Wyślij kod rabatowy do użytkowników";
$aConfig['lang']['m_zamowienia_codes']['go_back'] = "Powrót";
$aConfig['lang']['m_zamowienia_codes']['add_product'] = 'Dodaj książki';
$aConfig['lang']['m_zamowienia_codes']['save'] = 'Zapisz';
$aConfig['lang']['m_zamowienia_codes']['list_books'] = "Lista książek";
$aConfig['lang']['m_zamowienia_codes']['list_name'] = "Książka";
$aConfig['lang']['m_zamowienia_codes']['delete_all'] = "Zaznacz wszystkie";
$aConfig['lang']['m_zamowienia_codes']['add_codes_ok'] = "Dodano \"%s\" do kodu rabatowego \"%s\"";
$aConfig['lang']['m_zamowienia_codes']['add_codes_err'] = "Wystąpił błąd podczas dodawania  \"%s\"";
$aConfig['lang']['m_zamowienia_codes']['off_cost_transport'] = 'Wyłącz koszty transportu';
$aConfig['lang']['m_zamowienia_codes']['ignore_discount_limit'] = "Ignoruj limit rabatu";

$aConfig['lang']['m_zamowienia_codes']['header_categories'] = 'Kategorie, wydawcy oraz serie kodu rabatowego';
$aConfig['lang']['m_zamowienia_codes']['categories'] = 'Kategorie, wydawcy oraz serie kodu rabatowego';
$aConfig['lang']['m_zamowienia_codes']['code_categories'] = 'Kategorie';
$aConfig['lang']['m_zamowienia_codes']['new_publisher'] = 'Wprowadź wydawcę';
$aConfig['lang']['m_zamowienia_codes']['add_publisher_button'] = 'Dodaj';
$aConfig['lang']['m_zamowienia_codes']['publishers'] = 'Wydawcy';
$aConfig['lang']['m_zamowienia_codes']['new_series'] = 'Wprowadź serię';
$aConfig['lang']['m_zamowienia_codes']['add_serie_button'] = 'Dodaj';
$aConfig['lang']['m_zamowienia_codes']['series'] = 'Serie wydawnicze';

$aConfig['lang']['m_zamowienia_codes']['code_users_list'] = "Lista użytkowników dla kodu \"%s\"";
$aConfig['lang']['m_zamowienia_codes']['list_nazwa'] = "Użytkownik";
$aConfig['lang']['m_zamowienia_codes']['list_time_of_use'] = "Data użycia";
$aConfig['lang']['m_zamowienia_codes']['list_used'] = "Użyto";

$aConfig['lang']['m_zamowienia_codes']['header_0'] = "Dodawanie kodu rabatowego";
$aConfig['lang']['m_zamowienia_codes']['header_1'] = "Edycja kodu rabatowego";
$aConfig['lang']['m_zamowienia_codes']['active_from'] = "Data początkowa";
$aConfig['lang']['m_zamowienia_codes']['expiry'] = "Data ważności";
$aConfig['lang']['m_zamowienia_codes']['dates_err'] = "Data początkowa powinna być wcześniejsza od daty ważności!";
$aConfig['lang']['m_zamowienia_codes']['max_use'] = "kod ważny dla liczby zamówień (0 - brak ograniczenia)";
$aConfig['lang']['m_zamowienia_codes']['discount'] = "Rabat (%) <1; 100>";
$aConfig['lang']['m_zamowienia_codes']['discount_price_after_promo'] = "Rabat doliczany do rabatu ogólnego";
$aConfig['lang']['m_zamowienia_codes']['discount_price_after_promo_info'] = "Rabat ograniczony limitem rabatów np. wydawnictwa
  W wydawnictwie możemy zdefiniować czy kod rabatowy <b>ignoruje</b> limit rabatu";
$aConfig['lang']['m_zamowienia_codes']['code_type'] = "Typ kodu";
$aConfig['lang']['m_zamowienia_codes']['ctype_0'] = "kod jednokrotnego użytku - na koncie jednego użytkownika";
$aConfig['lang']['m_zamowienia_codes']['ctype_1'] = "kod wielokrotnego użytku - na koncie jednego użytkownika";
$aConfig['lang']['m_zamowienia_codes']['ctype_2'] = "kod jednokrotnego użytku - na wielu kontach";
$aConfig['lang']['m_zamowienia_codes']['ctype_3'] = "kod wielokrotnego użytku - na wielu kontach";
$aConfig['lang']['m_zamowienia_codes']['grant_code_exists_err'] = "Istnieje już automatycznie przyznawany nowozarejestrowanym użytkownikom kod \"%s\" o okresie obowiązywania zachodzącym na okres obowiązywania nowododawanego kodu!";

$aConfig['lang']['m_zamowienia_codes']['add_ok'] = "Dodano kod rabatowy \"%s\"";
$aConfig['lang']['m_zamowienia_codes']['add_err'] = "Wystąpił błąd podczas próby dodania kodu rabatowego \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_codes']['edit_ok'] = "Zmieniono kod rabatowy \"%s\"";
$aConfig['lang']['m_zamowienia_codes']['edit_err'] = "Wystąpił błąd podczas próby zmiany kodu rabatowego \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_codes']['del_ok_0']		= "Usunięto kod rabatowy %s";
$aConfig['lang']['m_zamowienia_codes']['del_ok_1']		= "Usunięto kody rabatowe %s";
$aConfig['lang']['m_zamowienia_codes']['del_err']		= "Wystąpił błąd podczas próby usunięcia kodu rabatowego %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_codes']['code_in_use_err']		= "Nie można usunąć aktywnego kodu \"%s\" bedącego w użyciu, dopóki nie zakończy się okres jego obowiązywania!";


$aConfig['lang']['m_zamowienia_codes_books']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_codes_books']['delete'] = "Usuń książkę z promocji";
$aConfig['lang']['m_zamowienia_codes_books']['delete_q'] = "Czy na pewno usunąć wybrany produkt z kodu rabatowego?";
$aConfig['lang']['m_zamowienia_codes_books']['preview'] = "Podgląd";
$aConfig['lang']['m_zamowienia_codes_books']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_codes_books']['remove_all_q'] = "Czy na pewno usunąć wybrane produkty z kodu rabatowego?";
$aConfig['lang']['m_zamowienia_codes_books']['remove_all_err'] = "Nie wybrano żadnego produktu do usunięcia z kodu rabatowego!";
$aConfig['lang']['m_zamowienia_codes']['del_code_ok'] = "Usunięto pozycje z kodu rabatowego";
$aConfig['lang']['m_zamowienia_codes']['del_code_err'] = "Wystąpił błąd podczas próby usunięcia pozycji kodu rabatowego";

/*  */
$aConfig['lang']['m_zamowienia_stock_report']['header']	= 'Raport magazynowy';
$aConfig['lang']['m_zamowienia_stock_report']['dates_range']	= 'Zakres dat';
$aConfig['lang']['m_zamowienia_stock_report']['start_date_from']	= 'od';
$aConfig['lang']['m_zamowienia_stock_report']['end_date_to']	= 'do';
$aConfig['lang']['m_zamowienia_stock_report']['button_show']	= 'Pobierz raport';
$aConfig['lang']['m_zamowienia_stock_report']['lp']	= 'Lp';
$aConfig['lang']['m_zamowienia_stock_report']['name']	= 'Pozycja';
$aConfig['lang']['m_zamowienia_stock_report']['quantity']	= 'Ilość';
$aConfig['lang']['m_zamowienia_stock_report']['discount']	= 'Rabat';
$aConfig['lang']['m_zamowienia_stock_report']['price_brutto']	= 'Cena detaliczna';
$aConfig['lang']['m_zamowienia_stock_report']['price_netto']	= 'Wartość netto';
$aConfig['lang']['m_zamowienia_stock_report']['vat']	= 'Stawka VAT';
$aConfig['lang']['m_zamowienia_stock_report']['promo_price_brutto']	= 'Cena z rabatem';
$aConfig['lang']['m_zamowienia_stock_report']['val_brutto']	= 'Wartość brutto';
$aConfig['lang']['m_zamowienia_stock_report']['isbn']	= 'ISBN';
$aConfig['lang']['m_zamowienia_stock_report']['publisher']	= 'Wydawnictwo';
$aConfig['lang']['m_zamowienia_stock_report']['author']	= 'Autor';
$aConfig['lang']['m_zamowienia_stock_report']['edition']	= 'Wydanie';
$aConfig['lang']['m_zamowienia_stock_report']['publication_year']	= 'Rok wydania';
$aConfig['lang']['m_zamowienia_stock_report']['title']	= 'Raport magazynowy';
$aConfig['lang']['m_zamowienia_stock_report']['no_orders']	= 'Brak zamówień';
/*$aConfig['lang']['m_zamowienia_stock_report']['']	= '';
$aConfig['lang']['m_zamowienia_stock_report']['']	= '';
$aConfig['lang']['m_zamowienia_stock_report']['']	= '';
$aConfig['lang']['m_zamowienia_stock_report']['']	= '';
$aConfig['lang']['m_zamowienia_stock_report']['']	= '';
$aConfig['lang']['m_zamowienia_stock_report']['']	= '';*/
$aConfig['lang']['m_zamowienia_stock_DBF_report'] = $aConfig['lang']['m_zamowienia_stock_report'];

/* raport vat*/
$aConfig['lang']['m_zamowienia_vat_report']['header']	= 'Raport VAT';
$aConfig['lang']['m_zamowienia_vat_report']['dates_range']	= 'Zakres dat';
$aConfig['lang']['m_zamowienia_vat_report']['page_format']	= 'Układ strony';
$aConfig['lang']['m_zamowienia_vat_report']['format0']	= 'pionowy';
$aConfig['lang']['m_zamowienia_vat_report']['format1']	= 'poziomy';
$aConfig['lang']['m_zamowienia_vat_report']['start_date_from']	= 'od';
$aConfig['lang']['m_zamowienia_vat_report']['end_date_to']	= 'do';
$aConfig['lang']['m_zamowienia_vat_report']['button_show']	= 'Pobierz raport';
$aConfig['lang']['m_zamowienia_vat_report']['name']	= 'Nazwa';
$aConfig['lang']['m_zamowienia_vat_report']['lp']	= 'Lp.';
$aConfig['lang']['m_zamowienia_vat_report']['invoice_nr']	= 'Nr dowodu';
$aConfig['lang']['m_zamowienia_vat_report']['order_date']	= 'Data';
$aConfig['lang']['m_zamowienia_vat_report']['nip']	= 'NIP';
$aConfig['lang']['m_zamowienia_vat_report']['value_brutto']	= 'Wart. sprzedaży brutto';
$aConfig['lang']['m_zamowienia_vat_report']['value_netto']	= 'Wart. sprzedaży netto';
$aConfig['lang']['m_zamowienia_vat_report']['vat_22_value_netto']	= 'Wartość netto';
$aConfig['lang']['m_zamowienia_vat_report']['vat_22_currency']	= 'VAT';
$aConfig['lang']['m_zamowienia_vat_report']['vat_0_value_netto']	= 'Wartość';
$aConfig['lang']['m_zamowienia_vat_report']['order_vat']	= 'Podatek należny od sprzedaży';
$aConfig['lang']['m_zamowienia_vat_report']['summary']	= 'PODSUMOWANIE';
$aConfig['lang']['m_zamowienia_vat_report']['recipient']	= 'Odbiorca';
$aConfig['lang']['m_zamowienia_vat_report']['vat_5']	= 'Stawka 5%';
$aConfig['lang']['m_zamowienia_vat_report']['vat_7']	= 'Stawka 5%';
$aConfig['lang']['m_zamowienia_vat_report']['vat_8']	= 'Stawka 8%';
$aConfig['lang']['m_zamowienia_vat_report']['vat_22']	= 'Stawka 22%';
$aConfig['lang']['m_zamowienia_vat_report']['vat_23']	= 'Stawka 23%';
$aConfig['lang']['m_zamowienia_vat_report']['vat_0']	= 'Stawka 0%';

/*raport zbiorczy vat*/

$aConfig['lang']['m_zamowienia_summary_vat_report']['header']	= 'Raport zbiorczy VAT';
$aConfig['lang']['m_zamowienia_summary_vat_report']['dates_range']	= 'Zakres dat';
$aConfig['lang']['m_zamowienia_summary_vat_report']['page_format']	= 'Układ strony';
$aConfig['lang']['m_zamowienia_summary_vat_report']['format0']	= 'pionowy';
$aConfig['lang']['m_zamowienia_summary_vat_report']['format1']	= 'poziomy';
$aConfig['lang']['m_zamowienia_summary_vat_report']['start_date_from']	= 'od';
$aConfig['lang']['m_zamowienia_summary_vat_report']['end_date_to']	= 'do';
$aConfig['lang']['m_zamowienia_summary_vat_report']['button_show']	= 'Pobierz raport';
$aConfig['lang']['m_zamowienia_summary_vat_report']['name']	= 'Nazwa';
$aConfig['lang']['m_zamowienia_summary_vat_report']['lp']	= 'Lp.';
$aConfig['lang']['m_zamowienia_summary_vat_report']['invoice_nr']	= 'Nr dowodu';
$aConfig['lang']['m_zamowienia_summary_vat_report']['order_date']	= 'Data';
$aConfig['lang']['m_zamowienia_summary_vat_report']['nip']	= 'NIP';
$aConfig['lang']['m_zamowienia_summary_vat_report']['value_brutto']	= 'Wart. sprzedaży brutto';
$aConfig['lang']['m_zamowienia_summary_vat_report']['value_netto']	= 'Wart. sprzedaży netto';
$aConfig['lang']['m_zamowienia_summary_vat_report']['vat_22_value_netto']	= 'Wartość netto';
$aConfig['lang']['m_zamowienia_summary_vat_report']['vat_22_currency']	= 'VAT';
$aConfig['lang']['m_zamowienia_summary_vat_report']['vat_0_value_netto']	= 'Wartość';
$aConfig['lang']['m_zamowienia_summary_vat_report']['order_vat']	= 'Podatek należny od sprzedaży';
$aConfig['lang']['m_zamowienia_summary_vat_report']['summary']	= 'PODSUMOWANIE';
$aConfig['lang']['m_zamowienia_summary_vat_report']['recipient']	= 'Odbiorca';
$aConfig['lang']['m_zamowienia_summary_vat_report']['vat_5']	= 'Stawka 5%';
$aConfig['lang']['m_zamowienia_summary_vat_report']['vat_7']	= 'Stawka 5%';
$aConfig['lang']['m_zamowienia_summary_vat_report']['vat_8']	= 'Stawka 8%';
$aConfig['lang']['m_zamowienia_summary_vat_report']['vat_22']	= 'Stawka 22%';
$aConfig['lang']['m_zamowienia_summary_vat_report']['vat_23']	= 'Stawka 23%';
$aConfig['lang']['m_zamowienia_summary_vat_report']['vat_0']	= 'Stawka 0%';

$aConfig['lang']['m_zamowienia_summary_vat_report']['payment_methods']['postal_fee'] = 'pobranie';
$aConfig['lang']['m_zamowienia_summary_vat_report']['payment_methods']['platnoscipl'] = 'Platnoscipl';
$aConfig['lang']['m_zamowienia_summary_vat_report']['payment_methods']['bank_transfer'] = 'przelew przedpłata';
$aConfig['lang']['m_zamowienia_summary_vat_report']['payment_methods']['bank_14days'] = 'przelew 14 dni';
/*  */
//nowe definicje
$aConfig['lang']['m_zamowienia_confirm_items']['print_next_time'] = "Drukuj ponownie";
$aConfig['lang']['m_zamowienia_confirm_items']['details'] = 'Szczegóły';
$aConfig['lang']['m_zamowienia_confirm_items']['email'] = 'Email';
$aConfig['lang']['m_zamowienia_confirm_items']['number'] = 'Numer';
$aConfig['lang']['m_zamowienia_confirm_items']['date_send'] = 'Data wygenerowania'; 
$aConfig['lang']['m_zamowienia_confirm_items']['send_by'] = 'Wygenerował'; 
$aConfig['lang']['m_zamowienia_confirm_items']['history'] = 'Listy pozycji do potwierdzenia "Zamówione"'; 

$aConfig['lang']['m_zamowienia_confirm_items']['details_header'] = 'Pozycje do potwierdzenia "Zamówione"';
$aConfig['lang']['m_zamowienia_confirm_items']['details_email'] = 'Odbiorca';
$aConfig['lang']['m_zamowienia_confirm_items']['details_send_by'] = 'Wygenerował';
$aConfig['lang']['m_zamowienia_confirm_items']['saved_type'] = "Typ wpisu";
$aConfig['lang']['m_zamowienia_confirm_items']['items_type'] = "Produkty";
$aConfig['lang']['m_zamowienia_confirm_items']['status_1'] = "do zam.";
$aConfig['lang']['m_zamowienia_confirm_items']['status_3'] = "jest - niepotw.";
$aConfig['lang']['m_zamowienia_confirm_items']['old_view'] = "Przełącz do starego widoku";
$aConfig['lang']['m_zamowienia_confirm_items']['f_source'] = 'Dostawca'; 

$aConfig['lang']['m_zamowienia_confirm_items']['saved_0'] = "CSV";
$aConfig['lang']['m_zamowienia_confirm_items']['saved_1'] = "TXT";
$aConfig['lang']['m_zamowienia_confirm_items']['saved_2'] = "Mail";
$aConfig['lang']['m_zamowienia_confirm_items']['saved_3'] = "PDF";
$aConfig['lang']['m_zamowienia_confirm_items']['saved_4'] = "XML";
$aConfig['lang']['m_zamowienia_confirm_items']['saved_5'] = "XLSX";

$aConfig['lang']['m_zamowienia_confirm_items']['details_date_send'] = 'Data wygenerowania';
$aConfig['lang']['m_zamowienia_confirm_items']['details_login_section'] = 'Dane wysłanego zamówienia';  

$aConfig['lang']['m_zamowienia_confirm_items']['details_content_section'] = 'Lista pozycji';  
$aConfig['lang']['m_zamowienia_confirm_items']['details_content'] = 'Lista pozycji';
//koniec nowych

$aConfig['lang']['m_zamowienia_confirm_items']['ordered_items_header'] = 'Pozycje ze stanem "Zamówione"';
$aConfig['lang']['m_zamowienia_confirm_items']['button_change_status'] = 'Zmień stan na Jest-potwierdzone';
$aConfig['lang']['m_zamowienia_confirm_items']['send'] = 'Wyślij';
$aConfig['lang']['m_zamowienia_confirm_items']['orders'] = 'Zamówione pozycje';
$aConfig['lang']['m_zamowienia_confirm_items']['orders_header'] = 'Pozycje zamówione';


$aConfig['lang']['m_zamowienia_confirm_items']['no_items'] = "<strong>Brak zdefiniowanych pozycji</strong>";
$aConfig['lang']['m_zamowienia_confirm_items']['found_books'] = 'Wynik wyszukiwania';

$aConfig['lang']['m_zamowienia_confirm_items']['books_list_id'] = 'Id';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_order_nr'] = 'Nr zamówienia';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_quantity'] = 'Ilość';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_isbn'] = 'ISBN';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_name'] = 'Nazwa';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_publisher'] = 'Wydawnictwo';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_author'] = 'Autor';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_curr_quantity'] = 'Odznaczona ilość';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_cover'] = 'Okładka';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_weight'] = 'Waga';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_year'] = 'Rok wydania';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_shipment'] = 'Wysyłamy';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_vat'] = 'VAT';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_price_brutto'] = '	Brutto';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_discount'] = 'Rabat (%)';
$aConfig['lang']['m_zamowienia_confirm_items']['books_list_value_brutto'] = 'Wart. brutto';

$aConfig['lang']['m_zamowienia_confirm_items']['no_books_to_show'] = "Brak zamówień spełniających zadane kryteria";
$aConfig['lang']['m_zamowienia_confirm_items']['update_ok'] = "Dostawa została przyjęta";
$aConfig['lang']['m_zamowienia_confirm_items']['update_err'] = "Wystąpił błąd podczas próby zmiany stanu wybranych pozycji";

$aConfig['lang']['m_zamowienia_confirm_items']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_confirm_items']['button_set_ordered'] = 'Oznacz jako zamówione';
$aConfig['lang']['m_zamowienia_confirm_items']['no_weight_err'] = "Brak ustawionej wagi produktu \"%s\" w zamówieniu nr \"%s\"!";
$aConfig['lang']['m_zamowienia_confirm_items']['add'] = 'Pobierz Raport PDF';
$aConfig['lang']['m_zamowienia_confirm_items']['package_closed'] = 'Wprowadź dostawę niezależną';

/*  */

//nowe definicje
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['print_next_time'] = "Drukuj ponownie";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details'] = 'Szczegóły';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['email'] = 'Email';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['number'] = 'Numer'; 
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['date_send'] = 'Data wygenerowania'; 
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['send_by'] = 'Wygenerował'; 
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['history'] = 'Listy pozycji do potwierdzenia "Jest-niepotwierdzone"';

$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['source_0'] = "Profit J";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['source_1'] = "Profit E";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['source_5'] = "Profit M";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['source_51'] = "Stock";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['source_52'] = "Profit X";
 
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['source'] = 'Dostawca'; 
$aConfig['lang']['m_zamowienia_confirm_items']['source'] = 'Dostawca'; 
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['f_source'] = 'Dostawca'; 

$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details_header_0'] = 'Pozycje do potwierdzenia "Jest-niepotwierdzone"';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details_header_1'] = 'Pozycje do potwierdzenia "Jest-niepotwierdzone"'; 
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details_header_2'] = 'Pozycje do potwierdzenia "Jest-niepotwierdzone"';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details_header_3'] = 'Pozycje do potwierdzenia "Jest-niepotwierdzone"';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['mm_doc_success'] = 'Dokument MM został wystawiony';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['has_mm'] = "To zamówienie ma już wystawiony dokument MM";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details_email'] = 'Odbiorca';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details_send_by'] = 'Wygenerował';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['saved_type'] = "Typ wpisu";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['items_type'] = "Produkty";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['status_1'] = "do zam.";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['status_3'] = "jest - niepotw.";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['old_view'] = "Przełącz do starego widoku";

$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['saved_0'] = "CSV";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['saved_1'] = "TXT";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['saved_2'] = "Mail";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['saved_3'] = "PDF";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['saved_4'] = "XML";

$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details_date_send'] = 'Data wygenerowania';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details_login_section'] = 'Dane wysłanego zamówienia';  

$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details_content_section'] = 'Lista pozycji';  
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['details_content'] = 'Lista pozycji';
//koniec nowych
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['ordered_items_header'] = 'Pozycje ze stanem "Jest-niepotwierdzone"';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['button_change_status'] = 'Zmień stan na Jest-potwierdzone';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['send'] = 'Wyślij';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['orders'] = 'Zamówione pozycje';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['orders_header'] = 'Pozycje zamówione';


$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['no_items'] = "<strong>Brak zdefiniowanych pozycji</strong>";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['found_books'] = 'Wynik wyszukiwania';

$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_id'] = 'Id';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_order_nr'] = 'Nr zamówienia';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_quantity'] = 'Ilość';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_isbn'] = 'ISBN';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_name'] = 'Nazwa';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_publisher'] = 'Wydawnictwo';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_author'] = 'Autor';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_year'] = 'Rok wydania';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_shipment'] = 'Wysyłamy';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_vat'] = 'VAT';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_weight'] = 'Waga';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_price_brutto'] = '	Brutto';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_discount'] = 'Rabat (%)';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['books_list_value_brutto'] = 'Wart. brutto';

$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['no_books_to_show'] = "Brak zamówień spełniających zadane kryteria";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['update_ok'] = "Dostawa została przyjęta";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['update_err'] = "Wystąpił błąd podczas próby zmiany stanu wybranych pozycji";

$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['button_set_ordered'] = 'Oznacz jako zamówione';
$aConfig['lang']['m_zamowienia_confirm_hasnc_items']['no_weight_err'] = "Brak ustawionej wagi produktu \"%s\" w zamówieniu nr \"%s\"!";

//dostawcy zewnetrzni
$aConfig['lang']['m_zamowienia_external_providers']['add'] = "Dodaj dostawcę";
$aConfig['lang']['m_zamowienia_external_providers']['edit'] = "Edytuj dostawcę";
$aConfig['lang']['m_zamowienia_external_providers']['list'] = "Dostawcy zewnętrzni";
$aConfig['lang']['m_zamowienia_external_providers']['header'] = "Dostawcy zewnętrzni";
$aConfig['lang']['m_zamowienia_external_providers']['delete'] = "Usuń dostawcę";
$aConfig['lang']['m_zamowienia_external_providers']['delete_q'] = "Czy na pewno usunąć wybranego dostawcę?";
$aConfig['lang']['m_zamowienia_external_providers']['check_all'] = "Zaznacz / odznacz wszystkich";
$aConfig['lang']['m_zamowienia_external_providers']['delete_all'] = "Usuń zaznaczonych";
$aConfig['lang']['m_zamowienia_external_providers']['delete_all_q'] = "Czy na pewno usunąć wybranych dostawców?";
$aConfig['lang']['m_zamowienia_external_providers']['delete_all_err'] = "Nie wybrano żadnego dostawcy do usunięcia!";
$aConfig['lang']['m_zamowienia_external_providers']['no_items'] = "Brak zdefiniowanych dostawców";
$aConfig['lang']['m_zamowienia_external_providers']['header_1'] = "Edytuj dostawcę";
$aConfig['lang']['m_zamowienia_external_providers']['header_0'] = "Dodaj dostawcę";
$aConfig['lang']['m_zamowienia_external_providers']['name'] = "Nazwa dostawcy";
$aConfig['lang']['m_zamowienia_external_providers']['symbol'] = "Symbol dostawcy";
$aConfig['lang']['m_zamowienia_external_providers']['file'] = "Nowa ikona dostawcy";
$aConfig['lang']['m_zamowienia_external_providers']['list_name'] = "Nazwa dostatawcy";
$aConfig['lang']['m_zamowienia_external_providers']['list_icon2'] = "Aktualna ikona";
$aConfig['lang']['m_zamowienia_external_providers']['list_icon'] = "Ikona";
$aConfig['lang']['m_zamowienia_external_providers']['add_ok'] = "Dodano dostawcę";
$aConfig['lang']['m_zamowienia_external_providers']['add_err'] = "Nie udało się dodać dostawcy \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_external_providers']['edit_ok'] = "Zmieniono dostawcę \"%s\"";
$aConfig['lang']['m_zamowienia_external_providers']['edit_err'] = "Nie udało się zmienić dostawcy \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_external_providers']['tags_invalid_err'] = "Nazwa dostawcy zawiera nieprawidłowe znaki!";
$aConfig['lang']['m_zamowienia_external_providers']['del_ok_0']		= "Usunięto dostawcę %s";
$aConfig['lang']['m_zamowienia_external_providers']['del_ok_1']		= "Usunięto dostawców %s";
$aConfig['lang']['m_zamowienia_external_providers']['del_err_0']		= "Nie udało się usunąć dostawcy %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_external_providers']['del_err_1']		= "Nie udało się usunąć dostawców %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_external_providers']['publishers_list'] = 'Wydawcy związani z dostawcą';
$aConfig['lang']['m_zamowienia_external_providers']['list_stock_col'] = 'Nazwa kolumny w products_stock';
$aConfig['lang']['m_zamowienia_external_providers']['stock_col'] = 'Nazwa kolumny w products_stock (zaawansowane)<br /> Pozostaw puste jeśli brak';
$aConfig['lang']['m_zamowienia_external_providers']['sort'] = "Priotytet mapowań";
$aConfig['lang']['m_zamowienia_external_providers']['go_back'] = "Powrót";
$aConfig['lang']['m_zamowienia_external_providers']['f_border_time'] = "Godzina graniczna";
$aConfig['lang']['m_zamowienia_external_providers']['f_null'] = "Brak";
$aConfig['lang']['m_zamowienia_external_providers']['f_set'] = "Ustawiona";
$aConfig['lang']['m_zamowienia_external_providers']['border_time'] = "Godzina graniczna HH:MM:SS<br /> (dla auto-odznadzania źródeł)";

/** MAPOWANIA DOSTAWCA <- WYDAWNICTWO **/
$aConfig['lang']['m_zamowienia_publishers_to_providers']['list_provider'] = 'Powiązane wydawnictwa z dostawcą "%s"';
$aConfig['lang']['m_zamowienia_publishers_to_providers']['list_publisher'] = 'Powiązani dostawcy z wydawnictwem "%s"';
$aConfig['lang']['m_zamowienia_publishers_to_providers']['name'] = 'Nazwa';
$aConfig['lang']['m_zamowienia_publishers_to_providers']['add'] = 'Dodaj mapowanie';
$aConfig['lang']['m_zamowienia_publishers_to_providers']['sort'] = 'Sortuj';
$aConfig['lang']['m_zamowienia_publishers_to_providers']['go_back'] = 'Powrót';
$aConfig['lang']['m_zamowienia_publishers_to_providers']['list_publisher_name'] = 'Wydawca';
$aConfig['lang']['m_zamowienia_publishers_to_providers']['list_provider_name'] = 'Dostawca';
$aConfig['lang']['m_zamowienia_publishers_to_providers']['add_provider'] = 'Dodawanie mapowania dostawcy "%s"';
$aConfig['lang']['m_zamowienia_publishers_to_providers']['add_publisher'] = 'Dodawanie mapowania wydawcy "%s"';

$aConfig['lang']['m_zamowienia_publishers_to_providers']['del_ok_0']		= "Usunięto mapowanie %s";
$aConfig['lang']['m_zamowienia_publishers_to_providers']['del_ok_1']		= "Usunięto mapowania %s";
$aConfig['lang']['m_zamowienia_publishers_to_providers']['del_err_0']		= "Nie udało się usunąć mapowania %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_publishers_to_providers']['del_err_1']		= "Nie udało się usunąć mapowań %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_zamowienia_publishers_to_providers']['delete'] = "Usuń mapowanie";
$aConfig['lang']['m_zamowienia_publishers_to_providers']['delete_q'] = "Czy na pewno usunąć mapowanie?";
$aConfig['lang']['m_zamowienia_publishers_to_providers']['check_all'] = "Zaznacz / odznacz wszystkich";
$aConfig['lang']['m_zamowienia_publishers_to_providers']['delete_all'] = "Usuń zaznaczone mapowania";
$aConfig['lang']['m_zamowienia_publishers_to_providers']['delete_all_q'] = "Czy na pewno usunąć wybrane mapowania?";
$aConfig['lang']['m_zamowienia_publishers_to_providers']['delete_all_err'] = "Nie wybrano żadnego mapowania do usunięcia!";

/** DO PRZEJRZENIA **/
$aConfig['lang']['m_zamowienia_to_review']['list'] = 'Lista alertów';
$aConfig['lang']['m_zamowienia_to_review']['list_message'] = 'Komunikat';
$aConfig['lang']['m_zamowienia_to_review']['list_comments'] = 'Komentarze';
$aConfig['lang']['m_zamowienia_to_review']['list_alert'] = 'Uruchom Alert';
$aConfig['lang']['m_zamowienia_to_review']['list_alert_datetime'] = 'Uruchom o';
$aConfig['lang']['m_zamowienia_to_review']['list_disabled'] = 'Wyłączone';
$aConfig['lang']['m_zamowienia_to_review']['list_disabled_by'] = 'Wyłączone przez';
$aConfig['lang']['m_zamowienia_to_review']['list_disabled_datetime'] = 'Data i godzina wyłączenia';
$aConfig['lang']['m_zamowienia_to_review']['list_user_id'] = 'Do użytkownika';
$aConfig['lang']['m_zamowienia_to_review']['list_mail_sent'] = 'Czy mail przypominający został wysłany';

$aConfig['lang']['m_zamowienia_to_review']['all_users'] = 'Wszyscy użytkownicy';
$aConfig['lang']['m_zamowienia_to_review']['deactivate'] = 'Wyłącz';
$aConfig['lang']['m_zamowienia_to_review']['details'] = 'Szczegóły';
$aConfig['lang']['m_zamowienia_to_review']['comments'] = 'Komentarze';

$aConfig['lang']['m_zamowienia_alert']['deactivate_err'] = 'Wystąpił błąd podczas deaktywowania alertu "%s" o id "%s" ';
$aConfig['lang']['m_zamowienia_alert']['deactivate_ok'] = 'Wyłączono alert "%s" o id "%s"';

/** DODAWANIE ZAMOWIENIA DO PRZEJRZENIA **/
$aConfig['lang']['m_zamowienia_alert']['header'] = 'Dodawanie alertu "%s"';
$aConfig['lang']['m_zamowienia_alert']['header_group'] = 'Dodawanie alertu';
$aConfig['lang']['m_zamowienia_alert']['message'] = 'Komunikat';
$aConfig['lang']['m_zamowienia_alert']['user_id'] = 'Przypisany użytkownik';
$aConfig['lang']['m_zamowienia_alert']['group_id'] = 'Przypisana grupa';
$aConfig['lang']['m_zamowienia_alert']['alert'] = 'Uruchom alert';
$aConfig['lang']['m_zamowienia_alert']['alert_datetime'] = 'Uruchomienie alertu';

$aConfig['lang']['m_zamowienia_alert']['add_ok'] = 'Dodano do przeglądnięcia zamówienie "%s" ';
$aConfig['lang']['m_zamowienia_alert']['add_err'] = 'Wystąpił błąd podczas dodawania zamówienienia "%s" do przeglądnięcia ';

$aConfig['lang']['m_zamowienia_alert']['add_group_ok'] = 'Dodano alert do grupy "%s" ';
$aConfig['lang']['m_zamowienia_alert']['add_group_err'] = 'Wystąpił błąd podczas dodawania alertu do do grupy "%s"';

/** WYSYŁKA MAILA ZAMÓWIENIA **/
$aConfig['lang']['m_zamowienia_mail']['header'] = 'Wyślij email na adres: "%s", zamówienia: "%s".';
$aConfig['lang']['m_zamowienia_mail']['title'] = 'Tytuł';
$aConfig['lang']['m_zamowienia_mail']['content'] = 'Treść';
$aConfig['lang']['m_zamowienia_mail']['templates'] = 'Szablon emaila';

$aConfig['lang']['m_zamowienia_mail']['header_template'] = 'Dodaj szablony emaili';
$aConfig['lang']['m_zamowienia_mail']['list'] = 'Lista szablonów emaili';
$aConfig['lang']['m_zamowienia_mail']['list_title'] = 'Tytuł emaila';
$aConfig['lang']['m_zamowienia_mail']['list_content'] = 'Treść emaila';
$aConfig['lang']['m_zamowienia_mail']['add'] = 'Dodaj szablon';
$aConfig['lang']['m_zamowienia_mail']['add_ok'] = 'Dodno szablon emaila "%s"';
$aConfig['lang']['m_zamowienia_mail']['add_err'] = 'Wystąpił błąd poczas dodawania szablonu emaila "%s", spróbuj ponownie!';
$aConfig['lang']['m_zamowienia_mail']['delete_all'] = 'Usuń zaznaczone';
$aConfig['lang']['m_zamowienia_mail']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_zamowienia_mail']['delete'] = 'Usuń szablon';
$aConfig['lang']['m_zamowienia_mail']['del_ok_0'] = 'Usunięto szablon emaila %s';
$aConfig['lang']['m_zamowienia_mail']['del_ok_1'] = 'Usunięto szablony emaili %s';
$aConfig['lang']['m_zamowienia_mail']['del_err'] = 'Wystąpił błąd poczas usuwania szablonu emaila "%s", spróbuj ponownie!';

$aConfig['lang']['m_zamowienia_mail']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_zamowienia_mail']['delete_all_q'] = "Czy na pewno usunąć wybrane elementy?";
$aConfig['lang']['m_zamowienia_mail']['delete_all_err'] = "Nie wybrano żadnego elementu do usunięcia!";
$aConfig['lang']['m_zamowienia_mail']['attachment'] = 'Załącznik';
$aConfig['lang']['m_zamowienia_mail']['add_send_ok'] = 'Email o tytule "%s" został wysłany na adres "%s" oraz zapisany w zamówieniu "%s"';
$aConfig['lang']['m_zamowienia_mail']['add_send_err'] = 'Wystąpił bląd podczas wysyłania emaila o tytule "%s" na adres "%s" zamówienie "%s"';

$aConfig['lang']['m_zamowienia_sc_confirm_hasnc_items'] = $aConfig['lang']['m_zamowienia_confirm_hasnc_items'];
$aConfig['lang']['m_zamowienia_sc_confirm_items'] = $aConfig['lang']['m_zamowienia_confirm_items'];

$aConfig['lang']['m_zamowienia_conf_magazine']['header'] = 'Konfiguracja magazynu';
$aConfig['lang']['m_zamowienia_conf_magazine']['limit_price_info'] = 'Limit wartości cen hurtowych produktów na liście w magazynie:';
$aConfig['lang']['m_zamowienia_conf_magazine']['azymut_limit_price'] = 'Azymut';
$aConfig['lang']['m_zamowienia_conf_magazine']['super_siodemka_limit_price'] = 'Super Siódemka';
$aConfig['lang']['m_zamowienia_conf_magazine']['ateneum_limit_price'] = 'Ateneum';
$aConfig['lang']['m_zamowienia_conf_magazine']['edit_ok'] = 'Wprowadzono zmiany w konfiguracji magazynu';
$aConfig['lang']['m_zamowienia_conf_magazine']['edit_err'] = 'Wystąpił błąd podczas zmiany w konfiguracji magazynu';

$aConfig['lang']['m_zamowienia_sc_validate_order'] = $aConfig['lang']['m_zamowienia_confirm_items'];
$aConfig['lang']['m_zamowienia_sc_validate_order']['header'] = 'Lista zamówień';
$aConfig['lang']['m_zamowienia_sc_validate_order']['list_invoice_id'] = 'Numer faktury';
$aConfig['lang']['m_zamowienia_sc_validate_order']['list_order_number'] = 'Numer zamówienia';
$aConfig['lang']['m_zamowienia_sc_validate_order']['list_verification_date'] = 'Data weryfikacji';
$aConfig['lang']['m_zamowienia_sc_validate_order']['list_verification_by'] = 'Zweryfikowano przez';
$aConfig['lang']['m_zamowienia_sc_validate_order']['books_list_cover'] = 'Okładka';
$aConfig['lang']['m_zamowienia_sc_validate_order']['books_list_quantity'] = 'Zamówiona ilość';
$aConfig['lang']['m_zamowienia_sc_validate_order']['books_list_curr_quantity'] = 'Odznaczona ilość';
$aConfig['lang']['m_zamowienia_sc_validate_order']['button_change_validate'] = 'Zweryfikowano';
$aConfig['lang']['m_zamowienia_sc_validate_order']['verify_ok'] = 'Zamówienie zostało poprawnie zweryfikowane';
$aConfig['lang']['m_zamowienia_sc_validate_order']['verify_err'] = 'Wystąpił błąd podczas weryfikacji zamówienia';

$aConfig['lang']['m_zamowienia_migration']['details'] = 'Przenieś produkty rozpoczynające się na wskazaną literę';
$aConfig['lang']['m_zamowienia_migration']['header'] = 'Odznacz produkty rozpoczynające się na literę "%s"';
$aConfig['lang']['m_zamowienia_migration']['not_exists'] = 'Nadwyżki:';
$aConfig['lang']['m_zamowienia_migration']['check'] = 'Zapisz listę';

$aConfig['lang']['m_zamowienia_back_lack_train']['details'] = 'Przenieś produkty rozpoczynające się na wskazaną literę';
$aConfig['lang']['m_zamowienia_back_lack_train']['header'] = 'Odznacz produkty rozpoczynające się na literę "%s"';
$aConfig['lang']['m_zamowienia_back_lack_train']['not_exists'] = 'Nadwyżki:';
$aConfig['lang']['m_zamowienia_back_lack_train']['check'] = 'Zapisz listę';

$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj_list_history']['activate'] = "Otwórz/zamknij listę";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj_list_history']['reopen_list'] = "Otwórz listę ponownie";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj_list_history']['history'] = "Zobacz historię listy";