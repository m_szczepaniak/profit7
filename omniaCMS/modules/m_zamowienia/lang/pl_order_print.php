<?php
/**
* Plik jezykowy modulu 'Zamowienia'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_zamowienia']['list_name']	= 'Tytuł';
$aConfig['lang']['mod_m_zamowienia']['list_quantity']	= 'Sztuk';
$aConfig['lang']['mod_m_zamowienia']['list_price_brutto']	= 'Cena brutto';
$aConfig['lang']['mod_m_zamowienia']['total_value']	= 'Wartość';
$aConfig['lang']['mod_m_zamowienia']['total']	= 'Suma:';
$aConfig['lang']['mod_m_zamowienia']['transport_type']	= 'Sposób wysyłki:';
$aConfig['lang']['mod_m_zamowienia']['total_cost']	= 'Całkowity koszt twojego zamówienia:';
$aConfig['lang']['mod_m_zamowienia']['bank_transfer_type']	= 'przelew';
$aConfig['lang']['mod_m_zamowienia']['postal_fee_type']	= 'pobranie';
$aConfig['lang']['mod_m_zamowienia']['vat_type']	= 'płatność VAT';

?>