<?php
/**
* Plik jezykowy modulu 'Zamowienia'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_zamowienia']['no_data']	= 'Twój koszyk jest pusty';
$aConfig['lang']['mod_m_zamowienia']['removed_elements_from_cart']	= 'Niektóre elementy z Twojego koszyka zostały usunięte!<br />Opłaciłeś już wcześniej do nich dostęp.';
$aConfig['lang']['mod_m_zamowienia']['cart_info']	= '';
$aConfig['lang']['mod_m_zamowienia']['list_name']	= 'Pozycja';
$aConfig['lang']['mod_m_zamowienia']['list_quantity']	= 'Ilość';
$aConfig['lang']['mod_m_zamowienia']['isbn']	= 'ISBN:';
$aConfig['lang']['mod_m_zamowienia']['publisher']	= 'Wydawnictwo:';
$aConfig['lang']['mod_m_zamowienia']['author']	= 'Autor:';
$aConfig['lang']['mod_m_zamowienia']['publication_year']	= 'Rok wydania:';
$aConfig['lang']['mod_m_zamowienia']['edition']	= 'Wydanie:';
$aConfig['lang']['mod_m_zamowienia']['shipment_time']	= 'Dostępność:';
$aConfig['lang']['mod_m_zamowienia']['days']	= 'dni';
$aConfig['lang']['mod_m_zamowienia']['shipment_now']	= 'Od ręki';
$aConfig['lang']['mod_m_zamowienia']['shipment_preview']	= 'od';
$aConfig['lang']['mod_m_zamowienia']['email_shipment_time']	= 'Dostępność';
$aConfig['lang']['mod_m_zamowienia']['list_price_netto']	= 'Cena netto';
$aConfig['lang']['mod_m_zamowienia']['list_price_brutto']	= 'Cena brutto';
$aConfig['lang']['mod_m_zamowienia']['list_total_price_brutto']	= 'Suma - cena brutto';
$aConfig['lang']['mod_m_zamowienia']['total_products_netto']	= 'Wartość netto';
$aConfig['lang']['mod_m_zamowienia']['total_products_brutto']	= 'Wartość brutto';
$aConfig['lang']['mod_m_zamowienia']['total_value']	= 'Wartość';
$aConfig['lang']['mod_m_zamowienia']['list_repository']	= 'Do schowka';
$aConfig['lang']['mod_m_zamowienia']['list_delete']	= 'Usuń';
$aConfig['lang']['mod_m_zamowienia']['total_weight']	= 'Masa zamówionego towaru:';
$aConfig['lang']['mod_m_zamowienia']['transport_cost']	= 'Koszt transportu:';
$aConfig['lang']['mod_m_zamowienia']['transport_gratis']	= 'dostawa gratis';
$aConfig['lang']['mod_m_zamowienia']['kurier']	= 'Transport za pośrednictwem firmy spedycyjnej';
$aConfig['lang']['mod_m_zamowienia']['wlasny']	= 'Odbiór osobisty';
$aConfig['lang']['mod_m_zamowienia']['total']	= 'Suma:';
$aConfig['lang']['mod_m_zamowienia']['total_netto']	= 'Wartość netto';
$aConfig['lang']['mod_m_zamowienia']['total_brutto']	= 'Wartość brutto';
$aConfig['lang']['mod_m_zamowienia']['total_cost_netto']	= 'Suma netto:';
$aConfig['lang']['mod_m_zamowienia']['total_cost_brutto']	= 'Suma zamówienia:';
$aConfig['lang']['mod_m_zamowienia']['total_cost']	= 'Łączna kwota do zapłaty';
$aConfig['lang']['mod_m_zamowienia']['total_transport_cost']	= 'Koszt dostawy:';
$aConfig['lang']['mod_m_zamowienia']['total_postal_fee_cost']	= 'Koszt pobrania:';
$aConfig['lang']['mod_m_zamowienia']['transport_type']	= 'Sposób wysyłki:';
$aConfig['lang']['mod_m_zamowienia']['bank_transfer_type']	= 'przelew';
$aConfig['lang']['mod_m_zamowienia']['payment_type']['bank_transfer']	= 'Przelew - przedpłata na konto księgarni';
$aConfig['lang']['mod_m_zamowienia']['payment_type']['bank_14days']	= 'Przelew 14 dni';
$aConfig['lang']['mod_m_zamowienia']['payment_type']['postal_fee']	= 'Płatność przy odbiorze';
$aConfig['lang']['mod_m_zamowienia']['payment_type']['dotpay']	= 'Karta płatnicza lub e-przelew';
$aConfig['lang']['mod_m_zamowienia']['payment_type']['platnoscipl']	= 'Szybki przelew lub BLIK';
$aConfig['lang']['mod_m_zamowienia']['payment_type']['payu_bank_transfer'] = "Zwykły przelew";
$aConfig['lang']['mod_m_zamowienia']['payment_type']['card']	= 'Płatność kartą';
$aConfig['lang']['mod_m_zamowienia']['payment_type']['self_collect']	= 'Odbiór osobisty';
$aConfig['lang']['mod_m_zamowienia']['postal_fee_type']	= 'pobranie';
$aConfig['lang']['mod_m_zamowienia']['vat_type']	= 'płatność VAT';
$aConfig['lang']['mod_m_zamowienia']['private_client']	= 'Osoba prywatna';
$aConfig['lang']['mod_m_zamowienia']['company_client']	= 'Firma lub instytucja';
$aConfig['lang']['mod_m_zamowienia']['payment_not_selected']	= 'Nie wybrano formy płatności';
$aConfig['lang']['mod_m_zamowienia']['content_order1']	= 'Zawartość zamówienia:';

$aConfig['lang']['mod_m_zamowienia']['step_1']	= '1. Zawartość koszyka';
$aConfig['lang']['mod_m_zamowienia']['step_2']	= '2. Twoje dane';
$aConfig['lang']['mod_m_zamowienia']['step_3']	= '3. Podsumowanie';
$aConfig['lang']['mod_m_zamowienia']['step_4']	= '4. Wybór płatności';

$aConfig['lang']['mod_m_zamowienia']['data_send']	= 'Dane do wysyłki';
$aConfig['lang']['mod_m_zamowienia']['data_facture'] = 'Dane do faktury';
$aConfig['lang']['mod_m_zamowienia']['comments_orders'] = 'Uwagi dotyczące zamówienia';

$aConfig['lang']['mod_m_zamowienia']['login_title']	= 'Zaloguj się:';
$aConfig['lang']['mod_m_zamowienia']['login_description']	= '<strong>Zaloguj się lub załóż konto, aby dokonać zakupu</strong>';
$aConfig['lang']['mod_m_zamowienia']['login_registration_description']	= '<h2>Załóż konto, które pozwoli Ci na:</h2>

<ul>
	<li>dokonywanie zakupów w naszej księgarni</li>
	<li>posiadanie dostępu do historii i statusów twoich zakupów</li> 
	<li>szybszą realizację zakupów</li>
	<li>otrzymywanie rabatów i informacji o promocjach</li>
</ul>
';
$aConfig['lang']['mod_m_zamowienia']['order_no_login']	= '<h2>Zakupy bez rejestracji:</h2>

<ul>
	<li>brak historii i statusów zamówień</li>
	<li>każdorazowe podawanie danych adresowych</li> 
</ul>
';

$aConfig['lang']['mod_m_zamowienia']['discount_code_err']= 'Wprowadzony kod rabatowy nie jest ważnym kodem!';
$aConfig['lang']['mod_m_zamowienia']['discount_code_err_2']= 'Książki w koszyku nie są obsługiwane przez podany kod rabatowy!';
$aConfig['lang']['mod_m_zamowienia']['discount_code_ok_0']= 'Wprowadzono kod rabatowy w wysokości %d%%';
$aConfig['lang']['mod_m_zamowienia']['discount_code_ok_1']= 'Wprowadzono kod zwalniający z kosztów transportu oraz nadający rabat w wysokości %d%%';
$aConfig['lang']['mod_m_zamowienia']['discount_code_ok_2']= 'Wprowadzono kod zwalniający z kosztów transportu';
$aConfig['lang']['mod_m_zamowienia']['discount_code_ok_3']= 'Wprowadzono kod rabatowy';

$aConfig['lang']['mod_m_zamowienia']['name']	= 'Imię';
$aConfig['lang']['mod_m_zamowienia']['surname']	= 'Nazwisko';
$aConfig['lang']['mod_m_zamowienia']['company']	= 'Firma';
$aConfig['lang']['mod_m_zamowienia']['street']	= 'Ulica';
$aConfig['lang']['mod_m_zamowienia']['number']	= 'Nr domu';
$aConfig['lang']['mod_m_zamowienia']['number']	= 'Nr domu:';
$aConfig['lang']['mod_m_zamowienia']['number2']	= 'Nr lokalu';
$aConfig['lang']['mod_m_zamowienia']['number2_info']	= 'Nr lokalu - pole nieobowiązkowe';
$aConfig['lang']['mod_m_zamowienia']['postal']	= 'Kod pocztowy';
$aConfig['lang']['mod_m_zamowienia']['phone']	= 'Nr telefonu';
$aConfig['lang']['mod_m_zamowienia']['city']	= 'Miejscowość';
$aConfig['lang']['mod_m_zamowienia']['nip']	= 'NIP';
$aConfig['lang']['mod_m_zamowienia']['namesurname']	= 'Imię i nazwisko';
$aConfig['lang']['mod_m_zamowienia']['address']	= 'Adres';
$aConfig['lang']['mod_m_zamowienia']['invoice']	= 'Proszę o wystawienie faktury VAT';
$aConfig['lang']['mod_m_zamowienia']['confirm_email']	= 'Powtórz e-mail:';
$aConfig['lang']['mod_m_zamowienia']['passwd']	= 'Hasło:';
$aConfig['lang']['mod_m_zamowienia']['passwd_info']	= 'Min. 6 - max. 32 znaki, NIE MOŻE zawierać znaków \, \' i "';
$aConfig['lang']['mod_m_zamowienia']['passwd2']	= 'Powtórz hasło:';
$aConfig['lang']['mod_m_zamowienia']['confirm_passwd_err']	= 'Podane hasła są różne';
$aConfig['lang']['mod_m_zamowienia']['header_login']	= 'Dane do logowania';
$aConfig['lang']['mod_m_zamowienia']['header_send']	= 'Dane adresowe dla dostawy i wystawienia faktury VAT';
$aConfig['lang']['mod_m_zamowienia']['header_invoice']	= 'Faktura VAT na inne dane';
$aConfig['lang']['mod_m_zamowienia']['newsletter'] = 'Newsletter';
$aConfig['lang']['mod_m_zamowienia']['second_invoice'] = 'Druga faktura';
$aConfig['lang']['mod_m_zamowienia']['show_second_invoice'] = 'Chcę otrzymać drugą fakturę VAT';
$aConfig['lang']['mod_m_zamowienia']['hide_second_invoice'] = 'Rezygnuję z drugiej faktury VAT';

$aConfig['lang']['mod_m_zamowienia']['new_product']	= '*** NOWOŚĆ ***';
$aConfig['lang']['mod_m_zamowienia']['price_netto']	= 'cena netto:';
$aConfig['lang']['mod_m_zamowienia']['price_brutto']	= 'cena brutto:';
$aConfig['lang']['mod_m_zamowienia']['old_price_netto']	= 'stara cena netto:';
$aConfig['lang']['mod_m_zamowienia']['old_price_brutto']	= 'stara cena:';
$aConfig['lang']['mod_m_zamowienia']['platnosci_pl']	= 'przy płatności \'PayU.pl\':';
$aConfig['lang']['mod_m_zamowienia']['transport_price']	= 'koszt transportu:';
$aConfig['lang']['mod_m_zamowienia']['add_to_cart']	= 'Kup teraz!';
$aConfig['lang']['mod_m_zamowienia']['delete_from_cart']	= 'Usuń';
$aConfig['lang']['mod_m_zamowienia']['move']	= 'Przenieś';
$aConfig['lang']['mod_m_zamowienia']['description']	= 'Opis produktu';
$aConfig['lang']['mod_m_zamowienia']['info_producer']	= 'Producent:';
$aConfig['lang']['mod_m_zamowienia']['info_producer_code']	= 'Kod producenta:';
$aConfig['lang']['mod_m_zamowienia']['info_product_url']	= 'Link produktu:';
$aConfig['lang']['mod_m_zamowienia']['info_product_url_face']	= 'przejdź do strony produktu';
$aConfig['lang']['mod_m_zamowienia']['move_to_repository']	= 'do&nbsp;przechowalni';

$aConfig['lang']['mod_m_zamowienia']['files']	= 'Do ściągnięcia:';

$aConfig['lang']['mod_m_zamowienia']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_zamowienia']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_zamowienia']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_zamowienia']['shipment_3'] = "tylko na zamówienie";
$aConfig['lang']['mod_m_zamowienia']['item_deleted'] = "Anulowane";


$aConfig['lang']['mod_m_zamowienia']['add_ok']	= 'Twoje konto zostało utworzone<br />Na adres <strong>%s</strong> został wysłany e-mail z potwierdzeniem rejestracji.';
$aConfig['lang']['mod_m_zamowienia']['add_err']	= 'Wystąpił błąd podczas tworzenia Twojego konta.<br /><br />Spróbuj ponownie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto: %s">%s</a>.';
$aConfig['lang']['mod_m_zamowienia']['update_ok']	= 'Twoje dane zostały zaktualizowane.';
$aConfig['lang']['mod_m_zamowienia']['update_err']	= 'Wystąpił błąd podczas aktualizacji Twoich danych.<br /><br />Spróbuj ponownie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto: %s">%s</a>.';
$aConfig['lang']['mod_m_zamowienia']['user_err']	= 'Użytkownik nie istnieje!';
$aConfig['lang']['mod_m_zamowienia']['email_confirm_err'] = 'Podane aresy email różnią się od siebie!';

$aConfig['lang']['mod_m_zamowienia']['registration_email_subject'] = 'Potwierdzenie rejestracji w serwisie %s';
$aConfig['lang']['mod_m_zamowienia']['registration_email_body_header'] = 'Witamy wśród użytkowników serwisu';
$aConfig['lang']['mod_m_zamowienia']['registration_email_body_content'] = 'Twoje konto zostało utworzone.';
$aConfig['lang']['mod_m_zamowienia']['registration_email_body_login_site'] = 'Strona logowania:';
$aConfig['lang']['mod_m_zamowienia']['registration_email_body_login_data'] = 'Dane logowania Twojego konta.';
$aConfig['lang']['mod_m_zamowienia']['registration_email_body_end'] = ''; 


$aConfig['lang']['mod_m_zamowienia']['login_exists'] = 'Istnieje już Konto Użytkownika o podanym loginie <strong>%s</strong>!';
$aConfig['lang']['mod_m_zamowienia']['email_exists'] = 'Istnieje już Konto Użytkownika o podanym adresie e-mail <strong>%s</strong>!';

$aConfig['lang']['mod_m_zamowienia']['passwd_change_ok']	= 'Twoje hasło zostało zmienione.';
$aConfig['lang']['mod_m_zamowienia']['current_passwd_err']	= 'Podałeś niepoprawne aktualne hasło!<br /><br />Spróbuj jeszcze raz.';
$aConfig['lang']['mod_m_zamowienia']['passwd_change_err']	= 'Wystąpił błąd podczas zmiany Twojego hasła.<br /><br />Spróbuj ponownie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto: %s">%s</a>.';
$aConfig['lang']['mod_m_zamowienia']['check_discount_code'] = 'Podany przez ciebie kod rabatowy jest nieprawidłowy. Rabat nie został przyznany.';
$aConfig['lang']['mod_m_zamowienia']['check_discount_code_ok'] = 'Twój kod rabatowy jest prawidłowy. Rabat został przyznany.';
$aConfig['lang']['mod_m_zamowienia']['discount_code'] = 'Wprowadź kod rabatowy';
$aConfig['lang']['mod_m_zamowienia']['discount_code_info'] = 'Rabat';
$aConfig['lang']['mod_m_zamowienia']['user_balance_info'] = 'Twoja nadpłata';
$aConfig['lang']['mod_m_zamowienia']['discount_code_info2'] = 'Kod rabatowy';

$aConfig['lang']['mod_m_zamowienia']['bank_transfer_end']	= 'Wybrałeś wysyłkę z zapłatą przelewem.';
$aConfig['lang']['mod_m_zamowienia']['postal_fee_end']	= 'Wybrałeś wysyłkę z zapłatą przy odbiorze.';
$aConfig['lang']['mod_m_zamowienia']['self_collection']	= 'Obdiór osobisty w siedzibie Firmy';
$aConfig['lang']['mod_m_zamowienia']['choose_payment_type']	= 'Sposób płatności';
$aConfig['lang']['mod_m_zamowienia']['payment']	= 'Wybierz sposób dostawy oraz metodę płatności:';
$aConfig['lang']['mod_m_zamowienia']['list_items']	= 'Podsumowanie koszyka - lista zamówionych pozycji:';
$aConfig['lang']['mod_m_zamowienia']['payment_postal']	= 'Jako formę płatności wybrałeś pobranie';
$aConfig['lang']['mod_m_zamowienia']['payment_vat']	= 'Jako formę płatności wybrałeś płatność na podstawie faktury VAT';
$aConfig['lang']['mod_m_zamowienia']['payment_types'][0] = "dotpay.pl";
$aConfig['lang']['mod_m_zamowienia']['payment_types'][1] = "Przedpłata";
$aConfig['lang']['mod_m_zamowienia']['payment_types'][2] = "Pobranie";
$aConfig['lang']['mod_m_zamowienia']['payment_types'][3] = "VAT";
$aConfig['lang']['mod_m_zamowienia']['payment_types_desc'][0] = "E-płatności: Karta kredytowa lub e-przelew (płatność odbywa się poprzez bezpieczny system autoryzacji Dotpay.eu).<br /><span style='padding-left: 24px;'>Obsługiwane karty: VISA, MasterCard, Diners Club International, JCB.</span><br /><span style='padding-left: 24px;'>E-przelewy: mTransfer, Płacę z Inteligo, Przelew z BPH, Przelew24, MultiTransfer, Płać z Nordea i inne (zobacz <a href='http://dotpay.eu/Oferta/Obsluga_E-Przelewow' target='_blank'>pełną listę</a>).</span>";
$aConfig['lang']['mod_m_zamowienia']['payment_types_desc'][1] = "Przedpłata - przelew bankowy wykonany samodzielnie (numer konta zostanie udostępniony po złożeniu zamówienia).";
$aConfig['lang']['mod_m_zamowienia']['payment_types_desc'][2] = "Za pobraniem (opłatę pobiera Poczta Polska lub kurier przy odbiorze przesyłki).";
$aConfig['lang']['mod_m_zamowienia']['payment_types_desc'][3] = "Faktura VAT z 14-dniowym terminem płatności, wysyłana wraz z towarem.";
$aConfig['lang']['mod_m_zamowienia']['no_payment_chosen']	= 'Musisz wybrać sposób płatności!';
$aConfig['lang']['mod_m_zamowienia']['no_bank_chosen']	= 'Wybierz bank, z którego chesz dokonać płatności elektronicznej!';
$aConfig['lang']['mod_m_zamowienia']['details'] = "zobacz szczegóły &raquo;";
$aConfig['lang']['mod_m_zamowienia']['no_paczkomat_chosen']	= 'Musisz wybrać paczkomat!';
$aConfig['lang']['mod_m_zamowienia']['no_phone']	= 'Podaj prawidłowy numer telefonu komórkowego!';
$aConfig['lang']['mod_m_zamowienia']['no_regulations_paczkomaty']	= 'Musisz zaakceptować regulamin Paczkomaty24/7!';
$aConfig['lang']['mod_m_zamowienia']['bad_postcode']	= 'Wprowadzony kod pocztowy znajduje się poza obszarem doręczeń firmy TBA Express';
$aConfig['lang']['mod_m_zamowienia']['no_postcode']	= 'Wprowadź kod pocztowy';
$aConfig['lang']['mod_m_zamowienia']['no_point_of_receipt']	= 'Wybrano nieprawidłowy punkt odbioru, spróbuj ponownie.';

$aConfig['lang']['mod_m_zamowienia']['privacy_text'] = 'Wyrażam zgodę na przetwarzanie moich danych osobowych do celów maketingowych';

$aConfig['lang']['mod_m_zamowienia']['payment_summary_header']	= 'Podsumowanie płatności';
$aConfig['lang']['mod_m_zamowienia']['costs_summary_header']	= 'Wartość zamówienia';
$aConfig['lang']['mod_m_zamowienia']['payment_summary_header']	= 'Sposób płatności';

$aConfig['lang']['mod_m_zamowienia']['user_data_header']	= 'Dane zamawiającego';
$aConfig['lang']['mod_m_zamowienia']['remarks_header']	= 'Uwagi';
$aConfig['lang']['mod_m_zamowienia']['remarks_err']	= 'Uwagi';
$aConfig['lang']['mod_m_zamowienia']['seller_data_header']	= 'Dane do przelewu:';
$aConfig['lang']['mod_m_zamowienia']['address_data_header']	= 'Adres dostawy';
$aConfig['lang']['mod_m_zamowienia']['invoice_data_header']	= 'Dane do faktury:';
$aConfig['lang']['mod_m_zamowienia']['name_surname']	= 'Imię i nazwisko';
$aConfig['lang']['mod_m_zamowienia']['name']	= 'Imię:';
$aConfig['lang']['mod_m_zamowienia']['surname']	= 'Nazwisko:';
$aConfig['lang']['mod_m_zamowienia']['street']	= 'Ulica:';
$aConfig['lang']['mod_m_zamowienia']['street_nu']	= 'Ulica i numer:';
$aConfig['lang']['mod_m_zamowienia']['city']	= 'Miasto:';
$aConfig['lang']['mod_m_zamowienia']['company']	= 'Firma:';
$aConfig['lang']['mod_m_zamowienia']['country']	= 'Kraj';
$aConfig['lang']['mod_m_zamowienia']['phone']	= 'Telefon:';
$aConfig['lang']['mod_m_zamowienia']['cellular_phone']	= 'Telefon kom:';
$aConfig['lang']['mod_m_zamowienia']['postal']	= 'Kod pocztowy:';
$aConfig['lang']['mod_m_zamowienia']['selected_transport']	= 'Wybrana forma transportu:';
$aConfig['lang']['mod_m_zamowienia']['selected_payment']	= 'Wybrana forma płatności:';
$aConfig['lang']['mod_m_zamowienia']['email']	= 'E-mail:';
$aConfig['lang']['mod_m_zamowienia']['remarks']	= 'Uwagi dotyczące Twojego zamówienia';
$aConfig['lang']['mod_m_zamowienia']['transport_remarks']	= 'Uwagi dla kuriera:';
$aConfig['lang']['mod_m_zamowienia']['nip']	= 'NIP:';
$aConfig['lang']['mod_m_zamowienia']['remarks_inter']	= '<strong>Uwagi</strong> (Wybrałeś przesyłkę międzynarodową, w poniższym polu wprowadź adres, na jaki mają zostać wysłane produkty)';
$aConfig['lang']['mod_m_zamowienia']['remarks_plain']	= 'Uwagi dotyczące Twojego zamówienia:';
$aConfig['lang']['mod_m_zamowienia']['total_cost']	= 'Łączna suma zamówienia:';
$aConfig['lang']['mod_m_zamowienia']['to_pay']	= 'Wartość do zapłaty:';
$aConfig['lang']['mod_m_zamowienia']['log_in']	= 'Zaloguj się';
$aConfig['lang']['mod_m_zamowienia']['without_register']	= 'Dokonaj zakupu bez rejestracji';
$aConfig['lang']['mod_m_zamowienia']['order_print']	= 'Wydrukuj zamówienie';
$aConfig['lang']['mod_m_zamowienia']['order_print_content']	= 'Kliknij, aby wydrukować zamówienie i przesłać je FAX-em na numer telefonu: <strong>0-22 33 77 601</strong>';
$aConfig['lang']['mod_m_zamowienia']['order_print_download']	= 'Drukuj zamówienie';
$aConfig['lang']['mod_m_zamowienia']['advantages'] = 'Przeczytaj o korzyściach z posiadania konta';
$aConfig['lang']['mod_m_zamowienia']['advantages_header'] = 'Korzyści z posiadania konta';

$aConfig['lang']['mod_m_zamowienia']['invoice2_help'] = 'Wybierz z listy powyżej z kolumny <strong>“Druga faktura”</strong> książki, które mają się znaleźć na drugiej fakturze VAT';

$aConfig['lang']['mod_m_zamowienia']['order_content'] = 'Zawartość zamówienia:';

$aConfig['lang']['mod_m_zamowienia']['inv_invoice']	= 'Proszę o wystawienie faktury VAT';
$aConfig['lang']['mod_m_zamowienia']['payment_data']	= 'Jeśli dane płatnika są identyczne zaznacz (w przypadku firmy uzupełnij poniższe pole NIP)';
$aConfig['lang']['mod_m_zamowienia']['inv_nip']	= 'NIP:';
$aConfig['lang']['mod_m_zamowienia']['inv_name']	= 'Imię';
$aConfig['lang']['mod_m_zamowienia']['inv_surname']	= 'Nazwisko';
$aConfig['lang']['mod_m_zamowienia']['inv_street']	= 'Ulica:';
$aConfig['lang']['mod_m_zamowienia']['inv_number']	= 'Nr / nr lokalu';
$aConfig['lang']['mod_m_zamowienia']['inv_number2']	= 'Nr lokalu';
$aConfig['lang']['mod_m_zamowienia']['inv_postal']	= 'Kod pocztowy:';
$aConfig['lang']['mod_m_zamowienia']['inv_city']	= 'Miasto:';
$aConfig['lang']['mod_m_zamowienia']['inv_company']	= 'Firma lub instytucja';
$aConfig['lang']['mod_m_zamowienia']['inv_name_surname']	= 'Imię i nazwisko / firma:';

$aConfig['lang']['mod_m_zamowienia']['nr_info']	= 'Używaj tego numeru we wszystkich kontaktach z nami dotyczących zamówienia';
$aConfig['lang']['mod_m_zamowienia']['transfer_value']	= 'Kwota przelewu';
$aConfig['lang']['mod_m_zamowienia']['payment_type_']	= 'Wybrana forma płatności';

$aConfig['lang']['mod_m_zamowienia']['invoice_address']	= 'Dane do faktury VAT';
$aConfig['lang']['mod_m_zamowienia']['invoice2_address']	= 'Dane do drugiej faktury VAT';
$aConfig['lang']['mod_m_zamowienia']['transport_address']	= 'Dane do dostawy';
$aConfig['lang']['mod_m_zamowienia']['contact_details']	= 'Dane adresowe:';
$aConfig['lang']['mod_m_zamowienia']['second_invoice_title']	= 'Dane do drugiej faktury VAT';
$aConfig['lang']['mod_m_zamowienia']['second_invoice_content']	= 'Jeżeli chcesz podzielić swoje zamówienie na dwie faktury VAT, kliknij poniższy przycisk.';
$aConfig['lang']['mod_m_zamowienia']['edit_addresses']	= 'Zmień lub dodaj dane adresowe';
$aConfig['lang']['mod_m_zamowienia']['edit_addresses_content']	= 'Kliknij na przycisk, jeśli chcesz zmodyfikować lub dodać nowy adres:';

$aConfig['lang']['mod_m_zamowienia']['seller_name']	= 'Nazwa:';
$aConfig['lang']['mod_m_zamowienia']['seller_bank']	= 'Bank:';
$aConfig['lang']['mod_m_zamowienia']['seller_bank_account']	= 'Nr konta:';

$aConfig['lang']['mod_m_zamowienia']['order_err']	= 'Wystąpił błąd podczas składania zamówienia.<br />Spróbuj ponownie, jeżeli błąd będzie się powtarzał skontaktuj się z nami <a href="mailto: %s">%s</a>.';

$aConfig['lang']['mod_m_zamowienia']['email_l']['order_no'] = 'ZAMÓWIENIE NR:';
$aConfig['lang']['mod_m_zamowienia']['email_l']['payment'] = 'METODA PŁATNOŚCI:';
$aConfig['lang']['mod_m_zamowienia']['email_l']['bank_transfer'] = 'PRZELEW ZWYKŁY';
$aConfig['lang']['mod_m_zamowienia']['email_l']['allpay'] = 'DOTPAY.PL';
$aConfig['lang']['mod_m_zamowienia']['order_content'] = 'Zawartość zamówienia nr ';
$aConfig['lang']['mod_m_zamowienia']['email_l']['customer_data'] = 'ZAMAWIAJĄCY:';
$aConfig['lang']['mod_m_zamowienia']['email_l']['customer_invoice_data'] = 'DANE DO FAKTURY:';
$aConfig['lang']['mod_m_zamowienia']['email_l']['phone'] = 'Telefon:';
$aConfig['lang']['mod_m_zamowienia']['email_l']['nip'] = 'NIP:';
$aConfig['lang']['mod_m_zamowienia']['email_l']['new_order_sender'] = 'Informacje o zamowieniach';
$aConfig['lang']['mod_m_zamowienia']['email_l']['bank_transfer_email_info'] = 'Aby zakończyć realizację zamówienia prosimy o dokonanie przelewu na poniższe dane:';
$aConfig['lang']['mod_m_zamowienia']['email_l']['amount'] = 'KWOTA:';
$aConfig['lang']['mod_m_zamowienia']['email_l']['transfer_title'] = 'Tytuł przelewu:';
$aConfig['lang']['mod_m_zamowienia']['email_l']['order_no_login'] = 'Zamówienie numer: ';
$aConfig['lang']['mod_m_zamowienia']['email_l']['amount_1'] = 'W celu opłacenia zamówienia przelej należność w wysokości';
$aConfig['lang']['mod_m_zamowienia']['email_l']['amount_2'] = 'na wskazany numer konta:';

$aConfig['lang']['mod_m_zamowienia']['new_order_admin_email_subject'] = 'Nowe zamowienie nr %s';
$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_subject'] = 'Twoje zamówienie nr %s';

$aConfig['lang']['mod_m_zamowienia']['new_order_info'] = 'Dziękujemy!<br />Twoje zamówienie zostało przyjęte do realizacji.<br /><br />Nr zamówienia <strong>%s</strong><br />Używaj tego numeru we wszystkich kontaktach z nami dotyczących zamówienia.'; 
$aConfig['lang']['mod_m_zamowienia']['transport_info'] = 'Jako formę transportu wybrałeś transport firmowy, w celu omówienia szczegółów skontaktuj się drogą mailową z działem sprzedaży pod adresem <a href="mailto: %s">%1$s</a>';
$aConfig['lang']['mod_m_zamowienia']['payment_details_prepay'] = '<strong>%s</strong><br/>%s %s %s<br />%s %s<br />%s<br />%s<br />Tytuł: Zamówienie numer %s';

$aConfig['lang']['mod_m_zamowienia']['thank_you'] = "Dziękujemy!";
$aConfig['lang']['mod_m_zamowienia']['order_thanks'] = 'Twoje zamówienie zostało zapisane i otrzymało numer: <strong>%d</strong><br /><br />Prosimy używać tego numeru we wszelkich kontaktach z nami dotyczącymi Twojego zamówienia.';
$aConfig['lang']['mod_m_zamowienia']['bank_transfer_info'] = 'Wybrana przez Ciebie metoda płatności to <strong>Przelew zwykły</strong>.<br /><br />
Aby zakończyć realizację zamówienia prosimy o przelanie kwoty <strong>%s zł</strong> na poniższe konto z dopiskiem <strong>Zamówienie nr %d - %s</strong>';
$aConfig['lang']['mod_m_zamowienia']['dotpay_button_text'] = 'Powrót do CROPA.org';
$aConfig['lang']['mod_m_zamowienia']['allpay_info1'] = 'Wybrana przez Ciebie metoda płatności to <strong>dotpay.pl</strong>.<br /><br />';
$aConfig['lang']['mod_m_zamowienia']['odbior'] = 'Wybrana przez Ciebie metoda płatności to płatność przy odbiorze.<br />Przed odbiorem towaru skontaktuj się ze sprzedawcą w celu ustalenia daty odbioru.<br /><br />';
$aConfig['lang']['mod_m_zamowienia']['allpay_info'] = 'Wybrana przez Ciebie metoda płatności to <strong>dotpay.pl</strong>.<br /><br />
Aby zakończyć realizację zamówienia i go opłacić kliknij poniższy przycisk "<strong>Zapłać online przez dotpay</strong>".<br />Zostaniesz przeniesiony na stronę transakcyjną www.dotpay.pl gdzie będziesz mógł opłacić zamówienie.<br /><br />Prosimy postepować zgodnie ze wskazówkami zawartymi na stronach dotpay.pl, <strong>nie wciskać przycisku WSTECZ lub ODŚWIEŻ w przeglądarce</strong><br /><br />';
$aConfig['lang']['mod_m_zamowienia']['dotpay_status_info'] = 'Informacja o statusie transakcji dotpay.pl';
$aConfig['lang']['mod_m_zamowienia']['dotpay_status_OK'] = 'Transakcja za pomocą systemu dotpay.pl przebiegła <strong>pomyślnie</strong>!<br /><br /><strong>Dziękujemy!</strong>';
$aConfig['lang']['mod_m_zamowienia']['dotpay_status_FAIL'] = '<strong>Wystąpił błąd</strong> transakcji za pomocą systemu dotpay.pl!<br /><br />Prosimy o <a href="mailto: %s">kontakt z nami</a> w celu wyboru innego sposobu płatności za złożone zamówienie.<br /><br /><strong>Dziękujemy!</strong>';
$aConfig['lang']['mod_m_zamowienia']['dotpay_status_NO_DATA'] = '<strong>Nie zostały przesłane</strong> dane o statusie transakcji!<br /><br />Prosimy o <a href="mailto: %s">kontakt z nami</a> w celu wyboru innego sposobu płatności za złożone zamówienie.<br /><br /><strong>Dziękujemy!</strong>';
$aConfig['lang']['mod_m_zamowienia']['order_number'] = 'Zamówienie nr: %s - %s';

$aConfig['lang']['mod_m_zamowienia']['dotpay_transaction_status_email_subject'] = 'Informacja o realizacji zamówienia nr %s';

$aConfig['lang']['mod_m_zamowienia']['dotpay_transaction_status_2_email_body'] = 'Szanowna Klientko, Szanowny Kliencie.

Transakcja %s w systemie dotpay.pl dla zamówienia %s w serwisie "%s" otrzymała status WYKONANA.
Twoje zamówienie zostało zrealizowane.

Zapraszamy do logowania się i korzystania z naszych zasobów.

%s';
$aConfig['lang']['mod_m_zamowienia']['dotpay_transaction_status_3_email_body'] = 'Szanowna Klientko, Szanowny Kliencie.

Transakcja %s w systemie dotpay.pl dla zamówienia %s w serwisie "%s" otrzymała status ODMOWNA.
Twoje zamówienie nie zostało zrealizowane.

Skontaktuj się z nami %s w celu określenia sposobu opłacenia zamówionego dostępu do naszych płatnych zasobów.

%s';

$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_thank_you'] = 'Dziękujemy za złożenie zamówienia w Profit24.pl';
$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_our_system'] = 'Potwierdzamy przyjęcie zamówienia o numerze';
$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_working_hours'] = 'W godzinach naszej pracy, od poniedziałku do piątku (10.00 – 18.00), nasi konsultanci handlowi rozpoczynają realizację zamówienia. <strong>W czasie 2 godzin roboczych</strong> ulegnie zmianie status Twojego zamówienia, co będzie widoczne w Twoim profilu na stronie <a href="https://www.profit24.pl/moje-konto/orders.html" target="_blank" style="color: #c30500;">https://www.profit24.pl/moje-konto/orders.html</a>. Dodatkowo prześlemy Ci e-mail z tymi samymi informacjami.';
$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_courier'] = 'Zamówione książki dostarczy Ci firma kurierska';
$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_until'] = 'W przypadku nie odnotowania wpłaty na naszym koncie przed podanym terminem, wysyłka zostanie przesunięta do czasu zaksięgowania płatności.';
$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_payment_booked'] = 'Zaksięgowana wpłata będzie widoczna w Twoim profilu na stronie <a href="https://www.profit24.pl/moje-konto/orders.html" target="_blank" style="color: #c30500;">https://www.profit24.pl/moje-konto/orders.html</a>. Dodatkowo prześlemy Ci e-mail z tymi samymi informacjami.';
$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_partial_realization'] = '<strong>Częściowa realizacja zamówienia…?</strong><br>
<br>
Pomimo częstych aktualizacji dostępności naszej oferty, może dojść do sytuacji, że tytuł zostanie wyprzedany. Wówczas nasz konsultant skontaktuje się z Państwem telefonicznie.';
$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_client_ip'] = 'Twoje zamówienie zostało złożone z adresu IP:';
$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_footer_info'] = 'Pozdrawiamy,<br>
Zespół Profit24.pl<br>
<br>
Profit M Spółka z ograniczoną odpowiedzialnością<br>
Al. Jerozolimskie 134<br>
02-305 Warszawa<br>
<a href="https://www.profit24.pl/kontakt-z-nami/" target="_blank" style="color: #c30500;">https://www.profit24.pl/kontakt-z-nami/</a>';
$aConfig['lang']['mod_m_zamowienia']['new_order_user_email_footer_info2'] = 'Prosimy NIE odpowiadać na tę wiadomość, ponieważ wysłana jest z adresu e-mail przeznaczonego do przesyłania komunikatów systemowych i poczta przychodząca nie jest akceptowana.<br>
<br>
W przypadku pytań lub wątpliwości proszę odwiedzić Centrum Pomocy Profit24.pl na stronie <a href="https://www.profit24.pl/kontakt-z-nami/" target="_blank" style="color: #c30500;">https://www.profit24.pl/kontakt-z-nami/</a> i wybrać dogodną dla siebie formę kontaktu z naszym zespołem.';


$aConfig['lang']['mod_m_zamowienia']['step_3'] = array();
$aConfig['lang']['mod_m_zamowienia']['step_3']['phone']	= 'Telefon:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['phone_err']	= 'Uzupełnij poprawnie "Telefon"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['phone_info']	= 'W formacie: 12 3456789 (stacjonarny) lub 123456789 (komórkowy)';

$aConfig['lang']['mod_m_zamowienia']['step_3']['email']	= 'Adres e-mail:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['email_err']	= 'Uzupełnij poprawnie "Adres e-mail"';

$aConfig['lang']['mod_m_zamowienia']['step_3']['confirm_email']	= 'Powtórz e-mail:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['email_err']	= 'Powtórz poprawnie e-mail';

$aConfig['lang']['mod_m_zamowienia']['step_3']['is_company']	= 'Typ';
$aConfig['lang']['mod_m_zamowienia']['step_3']['is_company_0']	= 'Osoba prywatna';
$aConfig['lang']['mod_m_zamowienia']['step_3']['is_company_1']	= 'Firma lub instytucja';

$aConfig['lang']['mod_m_zamowienia']['step_3']['name']	= 'Imię:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['name_err']	= 'Uzupełnij "Imię"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['surname']	= 'Nazwisko:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['surname_err']	= 'Uzupełnij "Nazwisko"';

$aConfig['lang']['mod_m_zamowienia']['step_3']['street']	= 'Ulica (nazwa wsi):';
$aConfig['lang']['mod_m_zamowienia']['step_3']['street_err']	= 'Uzupełnij pole "Ulica (nazwa wsi)"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['number']	= 'Nr domu:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['number_err']	= 'Uzupełnij pole "Nr domu"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['number2']	= 'Nr lokalu';
$aConfig['lang']['mod_m_zamowienia']['step_3']['number2_err']	= 'Uzupełnij pole "Nr lokalu"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['number2_info']	= 'Nr lokalu - pole nieobowiązkowe';
$aConfig['lang']['mod_m_zamowienia']['step_3']['postal']	= 'Kod pocztowy:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['postal_err']	= 'Uzupełnij pole "Kod pocztowy"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['postal_info']	= 'W formacie: 12-345';
$aConfig['lang']['mod_m_zamowienia']['step_3']['city']	= 'Miejscowość (poczta):';
$aConfig['lang']['mod_m_zamowienia']['step_3']['phone']	= 'Telefon:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['phone_err']	= 'Uzupełnij poprawnie "Telefon"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['phone_info']	= 'W formacie: 12 3456789 (stacjonarny) lub 123456789 (komórkowy)';
$aConfig['lang']['mod_m_zamowienia']['step_3']['cellular_phone']	= 'Tel. komórkowy:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['private_client'] = 'Osoba prywatna';
$aConfig['lang']['mod_m_zamowienia']['step_3']['company_client'] = 'Firma lub instytucja';
$aConfig['lang']['mod_m_zamowienia']['step_3']['company']	= 'Nazwa:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['company_err']	= 'Uzupełnij pole "Nazwa"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['nip']	= 'NIP:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['nip_err']	= 'Uzupełnij pole "NIP"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['ev_number'] = 'Numer ewidencyjny:';

$aConfig['lang']['mod_m_zamowienia']['step_3']['accept_reg']	= 'Zapoznałem się i akceptuję <a href="/regulamin" target="_blank">regulamin</a>';
$aConfig['lang']['mod_m_zamowienia']['step_3']['accept_priv']	= 'Wyrażam zgodę na przetwarzanie moich danych osobowych w celu realizacji zamówień, zgodnie z ustawą o ochronie danych osobowych z dnia 29 sierpnia 1997 r. (tekst jednolity: Dz. U. z 2002 r., Nr 101, poz. 926 z późn. zm.)  Poinformowano mnie, iż podanie moich danych osobowych ma charakter dobrowolny z prawem dostępu do treści moich danych, ich poprawiania, jak również usunięcia.';
$aConfig['lang']['mod_m_zamowienia']['step_3']['accept_reg_err']	= 'Akceptacja regulaminu';
$aConfig['lang']['mod_m_zamowienia']['step_3']['accept_priv_err']	= 'Zgoda na przetwarzanie danych osobowych';

$aConfig['lang']['mod_m_zamowienia']['step_3']['passwd2']	= 'Powtórz hasło:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['passwd2_err']	= 'Uzupełnij poprawnie "Powtórz hasło"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['confirm_passwd_err']	= 'Podane hasła są różne';

$aConfig['lang']['mod_m_zamowienia']['step_3']['passwd']	= 'Hasło:';
$aConfig['lang']['mod_m_zamowienia']['step_3']['passwd_err']	= 'Uzupełnij poprawnie "Hasło"';
$aConfig['lang']['mod_m_zamowienia']['step_3']['passwd_info']	= 'Min. 6 - max. 32 znaki, NIE MOŻE zawierać znaków \, \' i "';

$aConfig['lang']['mod_m_zamowienia']['step_3']['promotions_agreement_text'] = 'Proszę o przesłanie informacji o nowościach, promocjach i kodach rabatowych';
$aConfig['lang']['mod_m_zamowienia']['step_3']['promotions_agreement'] = 'Proszę o przesłanie informacji o nowościach, promocjach i kodach rabatowych';
$aConfig['lang']['mod_m_zamowienia']['step_3']['promotions_agreement_info'] = 'Wyrażam zgodę na otrzymywanie informacji handlowych od Administratora za pomocą środków komunikacji elektronicznej, zgodnie z ustawą o świadczeniu usług drogą elektroniczną. Zgoda może być odwołana w każdym czasie. Administratorem danych osobowych Klientów jest: Profit M Spółka z ograniczoną odpowiedzialnością z siedzibą w Warszawie (02-305), Aleje Jerozolimskie 134, NIP 525-22-45-459, Regon 015227002, KRS 0000756053';

$aConfig['lang']['mod_m_zamowienia']['step_3']['ceneo_agreement'] = 'Wyraź swoją opinię o zakupach';
$aConfig['lang']['mod_m_zamowienia']['step_3']['ceneo_agreement_info'] = 'Wyrażam zgodę na przekazanie mojego adresu e-mail oraz informacji o dokonanym zakupie, spółce Ceneo Sp. z o.o. z siedzibą w Poznaniu, 60-166 Poznań, ul. Grunwaldzka 182 oraz firmie Google Inc. w celu przesłania ankiety dotyczącej zakupu.';
$aConfig['lang']['mod_m_zamowienia']['step_3']['ceneo_agreement_text_err']	= 'Wyraź swoją opinię o zakupach';