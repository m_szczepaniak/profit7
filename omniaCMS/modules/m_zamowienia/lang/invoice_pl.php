<?php
/**
 * Plik jezykowy dla interfejsu modulu 'Zamowienia' Panelu Administracyjnego
 * jezyk polski
 *
 * @author Marcin Korecki <m.korecki@omnia.pl>
 * @version 1.0
 *
 */



/*-----------faktura */
$aConfig['lang']['m_zamowienia_invoice']['list_lp'] = 'Lp.';
$aConfig['lang']['m_zamowienia_invoice']['list_name'] = 'Tytuł';
$aConfig['lang']['m_zamowienia_invoice']['list_symbol']	= 'Symbol<br /> ISBN / PKWiU';
$aConfig['lang']['m_zamowienia_invoice']['list_quantity']	= 'Ilość';
$aConfig['lang']['m_zamowienia_invoice']['list_price_brutto']	= 'Cena detaliczna brutto [zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_discount']	= 'Rabat [%]';
$aConfig['lang']['m_zamowienia_invoice']['list_promo_price_netto']	= 'Cena po<br /> rabacie<br />netto [zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_value_netto']	= 'Wartość bez podatku [zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_st']	= 'st [%]';
$aConfig['lang']['m_zamowienia_invoice']['list_price']	= 'Kwota <br />[zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_vat']	= 'Podatek';
$aConfig['lang']['m_zamowienia_invoice']['list_jm']	= 'jed. m.';
$aConfig['lang']['m_zamowienia_invoice']['list_value_brutto']	= 'Wartość z podatkiem [zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_street']	= 'ul.';
$aConfig['lang']['m_zamowienia_invoice']['list_nip']	= 'NIP';
$aConfig['lang']['m_zamowienia_invoice']['list_regon'] = "REGON";
$aConfig['lang']['m_zamowienia_invoice']['list_vat_nr'] = 'Faktura VAT nr';
$aConfig['lang']['m_zamowienia_invoice']['list_proforma_nr'] = 'Faktura ProForma nr';
$aConfig['lang']['m_zamowienia_invoice']['list_city_day'] = 'Warszawa, dnia:';
$aConfig['lang']['m_zamowienia_invoice']['list_data_order'] = 'Data sprzedaży:';
$aConfig['lang']['m_zamowienia_invoice']['oryginal_copy'] = 'oryginał | kopia';
$aConfig['lang']['m_zamowienia_invoice']['list_price_vat']	= 'Kwota podatku [zł]';
$aConfig['lang']['m_zamowienia_invoice']['list_vat_reception']	= 'Podpis i pieczęć osoby upoważnionej<br />do odbioru faktury VAT';
$aConfig['lang']['m_zamowienia_invoice']['list_vat_acting']	= 'Osoba upoważniona<br /> do wystawienia faktury VAT';
$aConfig['lang']['m_zamowienia_invoice']['person_pack']	= 'Osoba pakująca';
$aConfig['lang']['m_zamowienia_invoice']['list_vat_data']	= 'data odbioru';
$aConfig['lang']['m_zamowienia_invoice']['list_pay']	= 'Do zapłaty';
$aConfig['lang']['m_zamowienia_invoice']['list_dictionary']	= 'Słownie:';
$aConfig['lang']['m_zamowienia_invoice']['list_transfer']	= 'Tytuł przelewu:';
$aConfig['lang']['m_zamowienia_invoice']['list_number_account']	= 'Nr konta:';
$aConfig['lang']['m_zamowienia_invoice']['list_bank']	= 'Bank:';
$aConfig['lang']['m_zamowienia_invoice']['list_reciptient']	= 'Odbiorca:';
$aConfig['lang']['m_zamowienia_invoice']['list_together']	= 'RAZEM:';
$aConfig['lang']['m_zamowienia_invoice']['list_transport_nubmer']	= 'Nr listu przewozowego';
$aConfig['lang']['m_zamowienia_invoice']['list_paid']	= 'Zapłacono:';

$aConfig['lang']['m_zamowienia_invoice']['isbn'] = 'ISBN:';
$aConfig['lang']['m_zamowienia_invoice']['publication_year'] = 'Rok wydania:';
$aConfig['lang']['m_zamowienia_invoice']['edition'] = 'Wydanie:';
$aConfig['lang']['m_zamowienia_invoice']['publisher']	= 'Wydawnictwo:';
$aConfig['lang']['m_zamowienia_invoice']['author']	= 'Autor:';
$aConfig['lang']['m_zamowienia_invoice']['shipment_time']	= 'Wysyłamy:';
$aConfig['lang']['m_zamowienia_invoice']['shipment_preview']	= 'od';
$aConfig['lang']['m_zamowienia_invoice']['jm_book']	= 'egz.';
$aConfig['lang']['m_zamowienia_invoice']['jm_attach']	= 'szt.';

$aConfig['lang']['m_zamowienia_invoice']['total_products_netto']	= 'Wartość netto';
$aConfig['lang']['m_zamowienia_invoice']['total_products_brutto']	= 'Wartość brutto';
$aConfig['lang']['m_zamowienia_invoice']['total_value']	= 'Wartość';
$aConfig['lang']['m_zamowienia_invoice']['choose_payment_type']	= 'Sposób płatności';
$aConfig['lang']['m_zamowienia_invoice']['transport_cost']	= 'Koszt transportu:';
$aConfig['lang']['m_zamowienia_invoice']['total']	= 'Suma';
$aConfig['lang']['m_zamowienia_invoice']['total_brutto'] = 'Łączna suma zamówienia:';
$aConfig['lang']['m_zamowienia_invoice']['transport_type']	= 'Sposób wysyłki:';
$aConfig['lang']['m_zamowienia_invoice']['total_cost']	= 'Całkowity koszt Twojego zamówienia:';
$aConfig['lang']['m_zamowienia_invoice']['user_data_header']	= 'Dane sprzedawcy:';
$aConfig['lang']['m_zamowienia_invoice']['seller_data_header']	= 'Dane niezbędne do dokonania przelewu';
$aConfig['lang']['m_zamowienia_invoice']['address_data_header']	= 'Adres dostawy';
$aConfig['lang']['m_zamowienia_invoice']['invoice_data_header']	= 'Dane nabywcy:';
$aConfig['lang']['m_zamowienia_invoice']['order_content']	= 'Zawartość zamówienia:';
$aConfig['lang']['m_zamowienia_invoice']['order_number']	= 'Zamówienia numer: ';
$aConfig['lang']['m_zamowienia_invoice']['subject']	= 'Informacja o statusie twojego zamówienia';
$aConfig['lang']['m_zamowienia_invoice']['send_ok']	= 'Informacja o statusie zamówienia została wysłana do klienta na adres %s';
$aConfig['lang']['m_zamowienia_invoice']['send_err']	= 'Nie udało się wysłać informacji o statusie zamówienia';
$aConfig['lang']['m_zamowienia_invoice']['send_payment_ok']	= 'Informacja o zmianie metody płatności została wysłana do klienta na adres %s';
$aConfig['lang']['m_zamowienia_invoice']['send_payment_err']	= 'Nie udało się wysłać informacji o zmianie metody płatności';
$aConfig['lang']['m_zamowienia_invoice']['status_change_date']	= 'W dniu %s Twoje zamówienie nr %s otrzymało status: <strong>%s</strong>.';
$aConfig['lang']['m_zamowienia_invoice']['subject']	= 'Informacja o statusie twojego zamówienia';
$aConfig['lang']['m_zamowienia_invoice']['subject_payment']	= 'Informacja o zmianie metody płatności twojego zamówienia';
$aConfig['lang']['m_zamowienia_invoice']['selected_transport']	= 'Transportu:';
$aConfig['lang']['m_zamowienia_invoice']['selected_payment']	= 'Płatności:';
$aConfig['lang']['m_zamowienia_invoice']['seller_data_header']	= 'Dane do przelewu';
$aConfig['lang']['m_zamowienia_invoice']['total_cost_brutto']	= 'Suma brutto';
$aConfig['lang']['m_zamowienia_invoice']['total_transport_cost']	= 'Koszt transportu';
$aConfig['lang']['m_zamowienia_invoice']['transport_invoice_item']	= 'Koszt transportu';

?>