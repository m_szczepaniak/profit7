<?php
/**
* Plik jezykowy modulu 'Zamowienia'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_zamowienia']['no_data']	= 'Twój schowek jest pusty';
$aConfig['lang']['mod_m_zamowienia']['name']	= 'Nazwa';
$aConfig['lang']['mod_m_zamowienia']['value']	= 'Wartość';
$aConfig['lang']['mod_m_zamowienia']['cart']	= 'do&nbsp;koszyka';
$aConfig['lang']['mod_m_zamowienia']['delete']	= 'Usuń';
$aConfig['lang']['mod_m_zamowienia']['move']	= 'Przenieś';

?>