<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia' - statystyki sprzedazy produktow
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
// dolaczenie wspolnej klasy Module_Common
include_once('modules/m_oferta_produktowa/Module_Common.class.php');
 
class Module extends Module_Common {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// ID strony modulu zamowien
	var $iPageId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;
		
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_zamowienia', $_GET['action']);
		$this->sModule = $_GET['module'];
		
		$sDo = '';
		$iId = 0;
				
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		// wyswietlenie
		switch ($sDo) {
			case 'show_stats':
				$this->showStats($pSmarty);
			break;
			case 'choose_conditions':
			default:
				$this->chooseConditions($pSmarty);
			break;
		}
	} // end of Module() method


	/**
	 * Metoda tworzy formularz wyboru danych do statystyk
	 * oraz wyswietla statystyki
	 *
	 * @param		object	$pSmarty
	 */
	function chooseConditions(&$pSmarty) {
		global $aConfig;
		$aLang =& $aConfig['lang'][$this->sModule];
				
		$aData = array();
		$aConditions = array('start_date', 'end_date', 'order_status', 'product_id', 'product_name', 'product_isbn');
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			// zczytanie z GET
			foreach ($aConditions as $sCond) {
				$aData[$sCond] = isset($_GET[$sCond]) ? $_GET[$sCond] : '';
			}
		}
		if (!isset($aData['start_date']) || $aData['start_date'] == '') {
			$aData['start_date'] = '00-00-0000';
		}
		if (!isset($aData['order_status']) || $aData['order_status'] == '') {
			$aData['order_status'] = '-1';
		}
						
		$pForm = new FormTable('stats_conditions', $aLang['header'], array('action'=>phpSelf(array())), array('col_width'=>190), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'show_stats');
		
		// zakres dat od
		$pForm->AddRow($pForm->GetLabelHTML('', $aLang['dates_range']), '<div style="float: left; margin-top: 8px;">'.$pForm->GetLabelHTML('start_date', $aLang['start_date_from'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('start_date', $aLang['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date').'</div><div style="float: left; margin-top: 8px; padding-left: 25px;">'.$pForm->GetLabelHTML('end_date', $aLang['end_date_to'], false).'</div><div style="float: left; padding-left: 10px;">'.$pForm->GetTextHTML('end_date', $aLang['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date').'</div>');
									 
		// zamowienia ze statusem
		$aOptions = array(
			array('value' => '-1', 'label' => $aLang['status_all']),
			array('value' => '0', 'label' => $aConfig['lang'][$_GET['module']]['status_0']),
			array('value' => '1', 'label' => $aConfig['lang'][$_GET['module']]['status_1']),
			array('value' => '2', 'label' => $aConfig['lang'][$_GET['module']]['status_2']),
			array('value' => '3', 'label' => $aConfig['lang'][$_GET['module']]['status_3']),
			array('value' => '4', 'label' => $aConfig['lang'][$_GET['module']]['status_4']),
			array('value' => '5', 'label' => $aConfig['lang'][$_GET['module']]['status_5'])
		);
		$pForm->AddRow($pForm->GetLabelHTML('order_status', $aLang['order_status']), $pForm->GetRadioSetHTML('order_status', $aLang['order_status'], $aOptions, $aData['order_status'], '', true, false), '', array(), array('style' => 'padding-top: 10px;'));
		
		// zrodlo zamowien
		//$pForm->AddMergedRow($aLang['orders_source'], array('class'=>'mergedLight', 'style'=>'padding-left: 195px'));
		
//		// typ zrodla
//		$aSources = @array_merge(array(array('value' => '-1', 'label' => $aLang['source_all'], 'onclick' => 'toggleSourceRowVisibility(this);')), $this->getPossibleSources());
//		$pForm->AddRow($pForm->GetLabelHTML('source', $aLang['source']), $pForm->GetRadioSetHTML('source', $aLang['source'], $aSources, $aData['source'], '', true, false), '', array(), array('style' => 'padding-top: 10px;'));
//		
//		// podzrodlo zamowien
//		$aSNewsletter = @array_merge(array(array('value' => '-1', 'label' => $aLang['s_newsletter_all'])), $this->getPossibleNewsletterSources());
//		$pForm->AddRow($pForm->GetLabelHTML('s_newsletter', $aLang['s_newsletter'], false), $pForm->GetSelectHTML('s_newsletter', $aLang['s_newsletter'], array(), $aSNewsletter, $aData['s_newsletter'], '', false), '', array('id' => 'th_s_newsletter', 'style' => 'display: '.($aData['source'] == 'newsletter' ? "" : 'none;').';'), array('id' => 'td_s_newsletter', 'style' => 'display: '.($aData['source'] == 'newsletter' ? "" : 'none;').';'));
//		
		// produkt
		$pForm->AddMergedRow($aLang['product_section'], array('class'=>'mergedLight', 'style'=>'padding-left: 195px'));
		
		$pForm->AddText('product_name',$aConfig['lang'][$this->sModule]['product_name'],$aData['product_name'],array('style'=>'width:300px;'),'','text',false);
		
		//$pForm->AddText('product_isbn',$aConfig['lang'][$this->sModule]['product_isbn'],$aData['product_isbn'],array('style'=>'width:300px;'),'','text',false);
		
		
		// przycisk wyszukaj
		$pForm->AddRow('&nbsp;', '<div style="margin-left: 225px;">'.
									$pForm->GetInputButtonHTML('send_search', $aConfig['lang'][$this->sModule]['button_search'], array('onclick'=>'document.getElementById(\'do\').value=\'choose_conditions\'; this.form.submit();'), 'button')
									.'</div>');
		// produkt
		//$pForm->AddSelect('product_id', $aLang['product'], array(), $this->getProducts(0, ''), $aData['product_id']);
		$pForm->AddSelect('product_id', $aConfig['lang'][$this->sModule]['product'], array('style'=>'width:300px;'), $this->getProducts('all',$aData['product_name'],'','',true), $aData['product_id']);
									
		// przyciski		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aLang['button_show']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('action'=>'statistics')).'\');'), 'button'));
		
		$sJS = '<script type="text/javascript">
		var sSourceRowSel = "'.($aData['source'] != '-1' ? $aData['source'] : '').'";
		</script>';

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of chooseConditions() function
	
	
	/**
	 * Metoda wyswietla statystyki dla wybranych warunkow
	 *
	 * @param		object	$pSmarty
	 */
	function showStats(&$pSmarty) {
		global $aConfig;
		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';
		$sSourceSql = '';
		$sSourceIt = '';
		$aSource = array();
		
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		$pForm = new FormTable('statistics', $aLang['header_statistics'], array('action'=>phpSelf(array())), array('col_width'=>30), $aConfig['common']['js_validation']);
		
		// naglowek warunkow dla ktorych wyswietlane sa statystyki
		$pForm->AddMergedRow($aLang['conditions'], array('class'=>'merged', 'style'=>'padding-left: 25px'));
		// produkt
		$pForm->AddRow('&nbsp;', $aLang['product'].': <b>'.$this->getItemName((int) $_POST['product_id']).'</b>');
		// przedzial czasowy
		$pForm->AddRow('&nbsp;', $aLang['dates_range'].': '.$aLang['start_date_from'].' <b>'.$_POST['start_date'].'</b> '.$aLang['end_date_to'].' <b>'.$_POST['end_date'].'</b>');
		// status zamowienia
		$pForm->AddRow('&nbsp;', $aLang['order_status'].': <b>'.$aConfig['lang'][$_GET['module']]['status_'.$_POST['order_status']].'</b>');
		// zrodlo zamowienia
		//$pForm->AddRow('&nbsp;', $aLang['orders_source'].': <b>'.$aLang['source_'.$_POST['source']].$sSourceIt.'</b>');
				
		// naglowek statystyk
		$pForm->AddMergedRow($aLang['statistics'], array('class'=>'merged', 'style'=>'padding-left: 25px'));
		// pobranie liczby zamowien i uzytkownikow
		$aProduct = $this->getProductOrdersSummary($sSourceSql);
		// liczba zamowien
		$pForm->AddRow('&nbsp;', $aLang['total_orders'].': <b>'.$aProduct['orders'].'</b>');
		// liczba uzytkownikow skladajacych zamowienia
		$pForm->AddRow('&nbsp;', $aLang['total_users'].': <b>'.$aProduct['users'].'</b>');
		// liczba sztuk produktu
		$pForm->AddRow('&nbsp;', $aLang['total_quantity'].': <b>'.$aProduct['quantity'].'</b>');
		// wartosc produktu
		$pForm->AddRow('&nbsp;', $aLang['total_value'].': <b>'.Common::formatPrice($aProduct['value_netto'], ' ').' <span style="color: gray;">'.$aConfig['lang']['common']['currency'].'</span></b> (<b>'.Common::formatPrice($aProduct['value_brutto'], ' ').' <span style="color: gray;">'.$aConfig['lang']['common']['currency'].'</span></b>)');
		
		if ((int) $aProduct['orders'] > 0) {
			// 10 statnich zamowien z wybranym produktem
			$aLastOrders =& $this->getLastOrders($sSourceSql);
			$pForm->AddMergedRow('<div style="float: left;">'.$aLang['last_orders'].'</div><div style="float: left; padding-left: 30px;"><a href="javascript:void(0);" onclick="toggleRowVisibility(this, \'th_last\', \'td_last\', \'showLink\', \'hideLink\', \''.$aConfig['lang'][$_GET['module'].'_statistics']['show'].'\', \''.$aConfig['lang'][$_GET['module'].'_statistics']['hide'].'\');" class="showLink">'.$aConfig['lang'][$_GET['module'].'_statistics']['show'].'</a></div>', array('class'=>'mergedLight', 'style'=>'padding-left: 30px'));
			// tabelka z zamowieniami
			$pForm->AddRow('&nbsp;', $this->formatOrdersTable($aLastOrders), '', array('id' => 'th_last','style' => 'display: none;'), array('id' => 'td_last', 'style' => 'display: none;'));
		}
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$_GET['module']]['button_change_conditions'], array('onclick'=>'MenuNavigate(\''.phpSelf(array_merge(array('start_date' => $_POST['start_date'], 'end_date' => $_POST['end_date'], 'order_status' => $_POST['order_status'], 'product_name' => $_POST['product_name'], 'product_isbn' => $_POST['product_isbn'], 'product_id' => $_POST['product_id']), $aSource), array('do')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of showStats() function
	

	
	/**
	 * Metoda pobiera liczbe zlozonych zamowien, wartosc sprzedazy,
	 * liczbe zamawiajacych dla wybranego produktu
	 * i na podstawie przeslanych w POST warunkow
	 * 
	 * @param	string	$sSourceSql - SQL zrodla zamowien
	 * @return 	array ref
	 */
	function getProductOrdersSummary($sSourceSql) {
		global $aConfig;
		
		// pobranie samej liczby zamowien (ID zamowien)
		$sSql = "SELECT DISTINCT A.order_id
						 FROM ".$aConfig['tabls']['prefix']."orders_items A
						 JOIN ".$aConfig['tabls']['prefix']."orders B
						 ON B.id = A.order_id
						 WHERE A.product_id = ".$_POST['product_id']." AND
						 			 B.order_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
						 			 B.order_date <= '".formatDate($_POST['end_date'])." 23:59:59'".
						 			 ($_POST['order_status'] != '-1' ? " AND B.order_status = '".$_POST['order_status']."'" : '').
						 			 $sSourceSql;
		$aOrders =& Common::GetCol($sSql);

		// pobranie liczby uzytkownikow kupujacych produkt
		if (!empty($aOrders)) {
			$sSql = "SELECT COUNT(DISTINCT user_id)
							 FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE id IN (".implode(',', $aOrders).")";
			$iUsers = (int) Common::GetOne($sSql);
			
			// pobranie wartosci zamowionego produktu
		$sSql = "SELECT SUM(quantity) quantity, SUM(value_netto) val_netto, SUM(value_brutto) val_brutto
						 FROM ".$aConfig['tabls']['prefix']."orders_items
						 WHERE order_id IN (".implode(',', $aOrders).") AND
						 			 product_id = ".$_POST['product_id']."
						 GROUP BY product_id";

			$aProdTot =& Common::GetRow($sSql);
			return array('orders' => count($aOrders), 'users' => $iUsers, 'quantity' => $aProdTot['quantity'], 'value_netto' => $aProdTot['val_netto'], 'value_brutto' => $aProdTot['val_brutto']);
		}
		return array('orders' => 0, 'users' => 0, 'quantity' => 0, 'value_netto' => 0, 'value_brutto' => 0);
	} // end of getProductOrdersSummary() method
	
	
	/**
	 * Metoda pobiera 10 ostatnich zamowien zawierajacych wybrany produkt
	 * dla przeslanych w POST warunkow
	 * 
	 * @param	string	$sSourceSql - SQL zrodla zamowien
	 * @return 	array ref
	 */
	function &getLastOrders($sSourceSql) {
		global $aConfig;
		$aIts = array();

		$sSql = "SELECT DISTINCT B.id, B.value_netto, B.value_brutto, B.user_id, B.email, CONCAT(C.name,' ',C.surname,' ',C.company) company, C.street, C.postal, C.city
						 FROM ".$aConfig['tabls']['prefix']."orders B
						 JOIN ".$aConfig['tabls']['prefix']."orders_items A
						 ON A.order_id = B.id
						 JOIN ".$aConfig['tabls']['prefix']."orders_users_addresses C 
						 ON C.order_id=B.id AND C.address_type='0'
						 WHERE A.product_id = ".(int) $_POST['product_id']." AND
						 			 B.order_date >= '".formatDate($_POST['start_date'])." 00:00:00' AND
						 			 B.order_date <= '".formatDate($_POST['end_date'])." 23:59:59'".
						 			 ($_POST['order_status'] != '-1' ? " AND B.order_status = '".$_POST['order_status']."'" : '').
									 $sSourceSql."
						 ORDER BY B.order_date DESC
						 LIMIT 0, 10";
		return Common::GetAll($sSql);
	} // end of getLastOrders() method
	
	
	/**
	 * Metoda formatuje tabele z lista zamowien przekazanych w $aOrders
	 * 
	 * @param	array ref	$aOrders	- lista zamowien
	 * @return	string
	 */
	function formatOrdersTable(&$aOrders) {
		global $aConfig;
		$aLang =& $aConfig['lang'][$_GET['module'].'_statistics'];
		$sStr = '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="recordsTable">';
		$sStr .= '<tr>';
		$sStr .= '<th width="10%" style="text-align: left;">'.$aLang['id'].'</th>';
		$sStr .= '<th width="10%" style="text-align: left;">'.$aLang['value_netto'].'</th>';
		$sStr .= '<th width="10%" style="text-align: left;">'.$aLang['value_brutto'].'</th>';
		$sStr .= '<th width="10%" style="text-align: left;">'.$aLang['user'].'</th>';
		$sStr .= '<th width="20%" style="text-align: left;">'.$aLang['user_name'].'</th>';
		$sStr .= '<th width="20%" style="text-align: left;">'.$aLang['user_street'].'</th>';
		$sStr .= '<th width="20%" style="text-align: left;">'.$aLang['user_city'].'</th>';
		$sStr .= '</tr>';
		foreach ($aOrders as $aOrd) {
			$sStr .= '<tr>';
			$sStr .= '<td>'.$aOrd['id'].'</td>';
			$sStr .= '<td><b>'.Common::formatPrice($aOrd['value_netto'], ' ').' <span style="color: gray;">'.$aConfig['lang']['common']['currency'].'</span></b></td>';
			$sStr .= '<td><b>'.Common::formatPrice($aOrd['value_brutto'], ' ').' <span style="color: gray;">'.$aConfig['lang']['common']['currency'].'</span></b></td>';
			$sStr .= '<td>[ '.$aOrd['user_id'].' ]<br><b>'.$aOrd['email'].'</b></td>';
			$sStr .= '<td>'.Common::convert($aOrd['company']).'</td>';
			$sStr .= '<td>'.Common::convert($aOrd['street']).'</td>';
			$sStr .= '<td>'.$aOrd['postal'].' '.Common::convert($aOrd['city']).'</td>';
			$sStr .= '</tr>';
		}
		$sStr .= '</table>';
		return $sStr;
	} // end of formatOrdersTable() method


	
} // end of Module Class
?>