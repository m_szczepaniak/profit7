<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - rabat ogolny
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'opisy_statusow');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty); break;
			default: $this->AddEdit($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda aktualizuje w bazie danych opis statusu
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id opisu
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}

		for($i=0;$i<4;$i++) {
			// aktualizacja
			$aValues = array(
				'shipment_days' => intval($_POST['shipment_days_'.$i]),
				'transport_days' => intval($_POST['transport_days_'.$i])
			);
			$sSql = "SELECT COUNT(1)
								FROM ".$aConfig['tabls']['prefix']."orders_shipment_times
								WHERE shipment_time = '".$i."'";
			if(intval(Common::GetOne($sSql))>0){
				if (Common::Update($aConfig['tabls']['prefix']."orders_shipment_times",
													 $aValues,
													 "shipment_time = '".$i."'") === false) {
					$bIsErr = true;
				}
			} else {
				$aValues['shipment_time'] = strval($i);
				if (Common::Insert($aConfig['tabls']['prefix']."orders_shipment_times",
													 $aValues,
													 '', false) === false) {
					$bIsErr = true;
				}
			}
		}
		
		if (!$bIsErr) {
			// akapit zostal zaktualizowany
			$sMsg = $aConfig['lang'][$this->sModule]['edit_ok'];
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->AddEdit($pSmarty);
		}
		else {
			// akapit nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = $aConfig['lang'][$this->sModule]['edit_err'];
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji opisu statusu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanego opisu statusu
	 */
	function AddEdit(&$pSmarty) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT shipment_time,shipment_days,transport_days
							 FROM ".$aConfig['tabls']['prefix']."orders_shipment_times";
			$aData =& Common::GetAssoc($sSql);
		

		$sHeader = $aConfig['lang'][$this->sModule]['header'];


		$pForm = new FormTable('shipment_times', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');
		
		for($i=0;$i<4;$i++) {
			$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['shipment_'.$i.'_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
			$pForm->AddText('shipment_days_'.$i, $aConfig['lang'][$this->sModule]['shipment_days'], $aData[$i]['shipment_days'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
			$pForm->AddText('transport_days_'.$i, $aConfig['lang'][$this->sModule]['transport_days'], $aData[$i]['transport_days'], array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'uint');
		}

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	

} // end of Module Class
?>