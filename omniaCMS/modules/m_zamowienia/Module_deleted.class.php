<?php
/**
 * Klasa wyświetlania zawartości zamówienia usuniętego
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-10-01 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__zamowienia__deleted {

  // komunikat
	public $sMsg;

	// nazwa modulu - do langow
	public $sModule;

	// id wersji jezykowej
	public $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	private $aPrivileges;
	
	// id strony modulu
	private $iPageId;
  
  // id uzytkownika
  private $iUId;
  
	// id listy zamowien
	private $iPPId;
  
  // id zamowienia
  private $iId;
  
  function __construct(&$pSmarty, $bInit = true) {
    $iOId = $_GET['oid'];
    $sOrderHTML = $this->doRestore($iOId);
    $sJs =
    '
     <script type="text/javascript"> 
      $(function() {
        if ($("#evt_order_details").is(":visible") === false) {
          toggleOrdersDetails();
        }

        $("a[id!=\'toggleOrdersDetails\']").removeAttr("href");
        doBlock(true);

      });
     </script>
      ';
    $pSmarty->assign('sContent', $sOrderHTML.$sJs);
  }
  
  /**
   * Metoda umożliwia wyświetlenie kodu html zamówienia, które zostało usunięte
   * 
   * @param type $iOId
   * @return type
   */
  public function doRestore($iOId) {
    
    $sSql = "SELECT content FROM orders_deleted WHERE id = ".$iOId;
    return Common::GetOne($sSql);
  }// end of doRestore() method
}

?>
