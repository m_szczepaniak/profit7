<?php

use Items\Confirmer;
use magazine\Containers;
/**
 * Klasa wspólna dla potwierdzania składowych zamówień
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-10-13 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Common_confirm_items {

  public $pDbMgr;
  
  /**
   * Metoda zamyka dostawę oraz ustawia numer kuwety tej dostawy
   * 
   * @global type $aConfig
   * @param string $sPackageNumber
   * @param int $iOSHId
   * @param bool $bIsExternalSource
   * @return boolean
   */
  protected function closeInPackage($sPackageNumber, $iOSHId, $bIsExternalSource) {
    global $aConfig;
    
    // zmieniamy tylko jeśli aktualny jest pusty
    $aValues = array(
        'pack_number' => $sPackageNumber
            );
    if ($bIsExternalSource === true) {
      $aValues['magazine_status'] = '2';// wjechalo na magazyn
    } else {
      $aValues['magazine_status'] = '1';// w drodze do mag głównego
    }
    $this->pDbMgr->Update('profit24', 'orders_send_history', $aValues, ' id = '.$iOSHId);

    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
    $oContainers = new Containers($this->pDbMgr);
    $mReturn = $oContainers->lockContainer($sPackageNumber);

    if ($mReturn !== true) {
      // coś poszło nie tak
      return $mReturn;
    }
    return true;
  }// end of closeInPackage() method
  
  
  /**
   * Metoda zapisuje dane dostawy
   * 
   * @param array $aItemData
   * @param int $iOSHIId
   * @return bool
   */
  protected function saveInItem($aItemData, $iOSHIId) {
    $bIsErr = false;
    
    
    $aOSHItem = $this->getOrdersSendHistoryItem($iOSHIId, array('item_id', 'current_quantity', 'send_history_id'));
    if ($this->updateWeight($aOSHItem['item_id'], $aItemData['weight']) === false) {
      $bIsErr = true;
    }
    
    $aUpdateValues = array(
        'in_confirmed_quantity' => $aItemData['currquantity']
    );
    
    if ($this->updateOrdersSendHistoryItem($iOSHIId, $aUpdateValues) === false) {
      $bIsErr = true;
    }
    $aOSHItem['in_confirmed_quantity'] = $aItemData['currquantity'];

    $confirmer = new Confirmer();
    $item = $confirmer->changeConfirmedQuantityItem($aOSHItem);

    include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/OrderRecount.class.php');
    $oOrderRecount = new \OrderRecount();
    if ($oOrderRecount->recountOrder($item['order_id'], false) === false) {
        $bIsErr = true;
    }
    
    return ($bIsErr === false ? true : false);
  }// end of saveInItem() method
  
  
  /**
   * Metoda ustawia status magazynu
   * 
   * @param int $iOSHId
   * @param char $cMagazineStatus
   * @return bool
   */
  public function setMagazineStatus($iOSHId, $cMagazineStatus, $db = null) {

    $pdbmgr = $this->pDbMgr;

    if ($db !== null) {
      $pdbmgr = $db;
    }

    return ($pdbmgr->Update('profit24', 'orders_send_history', array('magazine_status' => $cMagazineStatus), ' id = '.$iOSHId) === false ? false : true);
  }// end of setMagazineStatus() method
  
  
  /**
   * Metoda aktualizuje dane składowej zamówienia do dostawcy
   * 
   * @param int $iOSHIId
   * @param array $aUpdateValues
   * @return bool
   */
  private function updateOrdersSendHistoryItem($iOSHIId, $aUpdateValues) {
    
    return ($this->pDbMgr->Update('profit24', 'orders_send_history_items', $aUpdateValues, ' id = ' . $iOSHIId) === FALSE ? FALSE : TRUE);
  }// end of updateOrdersSendHistoryItem() method
	

  /**
   * Metoda pobiera informację na temat rekordu w zamówieniu do dostawcy
   * 
   * @param int $iOSHIId
   * @param array $aCols
   * @return array
   */
  private function getOrdersSendHistoryItem($iOSHIId, $aCols) {
    
    $sSql = 'SELECT '.implode($aCols, ',').' 
             FROM orders_send_history_items 
             WHERE id = '.$iOSHIId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of getOrdersSendHistoryItem() method
  
  
  /**
   * Metoda aktualizuje wagę w zamówieniu, i innych otwartych zamówieniach w których waga = '0'
   *  zmienia także wagę w samym produkcie
   * 
   * @param int $iOrderItemId
   * @param string $sWeight
   * @return bool
   */
  private function updateWeight($iOrderItemId, $sWeight) {
    $bIsErr = false;
    
    $aOrderItem = $this->getOrderItemData($iOrderItemId, array('weight', 'product_id'));
    $fWeight = Common::formatPrice2($sWeight);
    
    if($aOrderItem['weight'] != $fWeight && $fWeight > 0 && $aOrderItem['product_id'] > 0){
      $aValues = array(
          'weight' => $fWeight
      );
      if ($this->pDbMgr->Update('profit24', 'orders_items', $aValues, ' (product_id = '.$aOrderItem['product_id'].' AND weight = "0.00" AND `deleted` = "0") ') === FALSE) {
        $bIsErr = true;
      }
      if ($aOrderItem['product_id'] > 0) {
        if ($this->pDbMgr->Update('profit24', 'products', $aValues, ' id = '.$aOrderItem['product_id']) === FALSE) {
          $bIsErr = true;
        }
      }
    }
    return !$bIsErr;
  }// end of updateWeight() method
  
  
  /**
   * Metoda pobiera dane składowej zamówienia
   * 
   * @param int $iOrderItemId
   * @param array $aCols
   * @return array
   */
  private function getOrderItemData($iOrderItemId, $aCols) {
    
    $sSql = 'SELECT ' . implode(',', $aCols) . ' 
             FROM orders_items
             WHERE id = '.$iOrderItemId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of getOrderItemData() method
}