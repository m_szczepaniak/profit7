<?php

use communicator\sources\Orlen\Orlen;
use communicator\sources\PaczkaWRuchu\PaczkaWRuchu;
use communicator\sources\TBA\TBA;
use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use omniaCMS\lib\Orders\orderItemsActions;
use omniaCMS\lib\Orders\printMultipleOrdersPDF;
use orders\auto_change_status\autoProviderToOrders;
use orders\getTransportList;
use orders\logger;
use orders\Shipment;
use orders\Shipment\ShipmentTime;
use Reservations\ReservationRenewer;
use LIB\Helpers\ArrayHelper;

/**
 * Klasa Module do obslugi modulu 'Zamowienia' - lista zlozonych zamowien
 *
 * @author    Arkadiusz Golba 2012-2015, Marcin Korecki<korecki@pnet.pl> 2008-2012
 * @copyright 2015 Profit
 * @version   2.0
 */
class Module extends orderItemsActions {

    const MINIMAL_ADDITIONAL_DISCOUNT = 0.02;

  /**
   * @var string komunikat
   */
  var $sMsg;

  /**
   * @var string nazwa modulu - do langow
   */
  var $sModule;

  /**
   * @var int ID wersji jezykowej
   */
  var $iLangId;

  /**
   * @var int ID strony modulu zamowien
   */
  var $iPageId;

  /**
   * @var array uprawnienia uzytkownika dla wersji jez.
   */
  var $aPrivileges;

  /**
   * @var int
   */
  var $iWebsiteId;

  /**
   * @var string
   */
  var $sInvoicePostfix;

  /**
   * @var Admin oParent
   */
  public $oParent;

  /**
   * @var int
   */
  private $iUId;

  /**
   * @var int
   */
  private $iSendPackId;

  /**
   * @var string
   */
  protected $sErrMsg;
  public $pDbMgr;

  /**
   * Konstruktor klasy
   *
   * @return	void
   */
  public function __construct(&$pSmarty, &$oParent, $bInit = FALSE) {
    global $pDbMgr;
    if ($bInit === TRUE) {
      $this->oParent = $oParent;
      $this->sErrMsg = '';
      $this->iLangId = 1;
      $this->iPageId = getModulePageId('m_zamowienia');
      $this->sModule = 'm_zamowienia';
      $iOrderStatusId = NULL;
      return true;
    }
    $this->pDbMgr = $pDbMgr;
    $this->oParent = $oParent;
    $this->sErrMsg = '';
    $this->iLangId = $_SESSION['lang']['id'];
    $this->iPageId = getModulePageId('m_zamowienia');
    $this->sModule = $_GET['module'];
    $iOrderStatusId = NULL;
    $this->iUId = $_SESSION['user']['id'];

    $sDo = '';
    $iId = 0;
    $iPId = 0;
    $iInternalId = 0;
    $iMId = 0;

    if (isset($_GET['do'])) {
      $sDo = $_GET['do'];
    }
    if (isset($_POST['do'])) {
      $sDo = $_POST['do'];
    }
    if (isset($_GET['action'])) {
      $this->sModule .= '_' . $_GET['action'];
    }

    if (isset($_GET['pid'])) {
      $iPId = $_GET['pid'];
    }
    if (isset($_GET['ppid'])) {
      $iInternalId = $_GET['ppid'];
    }
    if (isset($_GET['mid'])) {
      $iMId = $_GET['mid'];
    }
    if (isset($_GET['id'])) {
      $iId = str_replace('true', '', $_GET['id']);
    }
    if (isset($_GET['it'])) {
      $iIt = $_GET['it'];
    }

    //$iTransportId = 0;
    if (isset($_GET['f_transport_id']) && intval($_GET['f_transport_id']) > 0) {
      $iTransportId = $_GET['f_transport_id'];
      $_POST['f_transport_id'] = $_GET['f_transport_id'];
    }
    if (isset($_POST['f_transport_id']) && intval($_POST['f_transport_id']) > 0) {
      $iTransportId = $_POST['f_transport_id'];
    }

    if (isset($_GET['f_order_status']) && $_GET['f_order_status'] != '') {
      $iOrderStatusId = $_GET['f_order_status'];
      $_POST['f_order_status'] = $_GET['f_order_status'];
    }
    if (isset($_POST['f_status']) && $_POST['f_order_status'] != '') {
      $iOrderStatusId = $_POST['f_order_status'];
    }

    $this->aPrivileges = & $_SESSION['user']['privileges'];

    if (isset($_GET['jobs']['auto_orders']) && $_GET['jobs']['auto_orders'] === true) {
      return true;
    }

    // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
    if ($_SESSION['user']['type'] === 0) {
      if (!hasModulePrivileges($_GET['module_id'])) {
        showPrivsAlert($pSmarty);
        return;
      }
    }
    if (!empty($_POST)) {
      if ($this->validateAllForm() === false) {
        if ($iId > 0) {
          if ($sDo == 'save_items' || $sDo == 'review') {
            return $this->Show($pSmarty, $iInternalId);
          } else {
            return $this->Details($pSmarty, $iInternalId, $iId);
          }
        } else {
          return $this->Show($pSmarty, $iInternalId, $iTransportId, $iOrderStatusId, $iMId);
        }
      }
    }
    if ($iPId > 0 && $iId > 0) {
      parent::__construct($pDbMgr, $this, $iPId, $iId);
    } elseif ($iId > 0) {
      parent::__construct($pDbMgr, $this, $iId);
    }

    $aOrderActions = array(
        'toggle_2nd_invoice' => 'toggleOrderInvoice',
        'save_items' => 'UpdateItems',
        'update_items' => 'UpdateItems',
        'send_modified' => 'SendModified',
        'update_user_data' => 'UpdateUserData',
        'change_opineo' => 'UpdateOpineo',
        'dont_send_info_not_send_on_time' => 'UpdateSendInfoDontSendOnTime',
        'dont_cancel' => 'dontCancel',
        'magazine_get_ready_list_VIP' => 'magazineGetReadyListVIP',
        'printFV' => 'printFV',
        'update_transport_remarks' => 'UpdateTransportRemarks',
        'update_transport_number' => 'UpdateTransportNumberAndDeleteInvoice',
        'update_send_date' => 'UpdateSendDateAndExpectedSendDate',
        'add_additional_info' => 'AddAdditionalInfo',
        'add_user_comments' => 'AddUserComments',
        'change_correction' => 'changeCorrection',
        'enable_second_payment' => 'EnableSecondPayment',
        'review' => 'addLinkedOrdersTransport',
        'del_relation' => 'deleteLinkedOrdersTransport',
        'invoice_remarks' => 'invoiceRemarks',
        'save_invoice_to_pay_days' => 'saveInvoiceToPayDays',
        'save_magazine_remarks' => 'saveMagazineRemarks',
        'change_paid_amount' => 'updateOrderPaidAmount',
        'detele_order_payment' => 'deleteOrderPayment',
        'save_seller_streamsoft_id' => 'UpdateSellerStreamsoftId'
    );


    $aOrderItemActions = array(
        'delete_item' => 'deleteItem',
        'undelete_item' => 'undeleteItem',
        'change_invoice' => 'changeInvoice',
        'change_payment' => 'changePayment',
    );

    if (isset($aOrderActions[$sDo])) {
      $bReturn = $this->$aOrderActions[$sDo]();
      if ($sDo == 'save_items' || $sDo == 'review') {
        return $this->Show($pSmarty, $iInternalId);
      } else {
        return $this->Details($pSmarty, $iInternalId, $iId);
      }
    } elseif (isset($aOrderItemActions[$sDo])) {
      if (isset($iIt)) {
        $this->setIt($iIt);
      }
      $bReturn = $this->$aOrderItemActions[$sDo]();
      $this->Details($pSmarty, $iInternalId, $iPId);
    } else {
      switch ($sDo) {
        case 'changes': $this->Changes($pSmarty, $iId); break;
          break;
        case 'bug': $this->getDebug($pSmarty, $iId); 
          break;
        case 'add':
        case 'details':
          $this->Details($pSmarty, $iInternalId, $iId);
          break;
        case 'send': $this->sendOrderStatus($iInternalId, $iId);
          break;
        case 'publish': $this->changeStatusForm($pSmarty, $iInternalId);
          break;
        case 'print': $this->PrintOrders($pSmarty, $iInternalId, $iId);
          break;
        case 'print_invoice': $this->getTransportListSetOrderConfirmed($pSmarty, $iInternalId, $iId, true);
          break;
        case 'get_package_status': $this->getPackageStatus($pSmarty, $iId);
          break;
        case 'get_invoice': $this->getInvoice($iId);
          break;
        case 'get_invoice_proforma': $this->getInvoiceProforma($iId);
          break;
        case 'add_product': $this->addProduct($pSmarty, $iInternalId, $iPId, $iId);
          break;
        case 'send': $this->sendOrderStatus($iInternalId, $iId);
          break;
        case 'save_shipment_date': $this->SaveShipmentDate($iId, $_GET['pppid']);
          break;
        case 'group_update_status': $this->GroupChangeStatus($pSmarty, $iInternalId);
          break;
        case 'connect_to': $this->ConnectOrderTo($pSmarty, $iPId, $iId);
          break;
        case 'get_sticker': $this->getSticker($iId);
          break;
        case 'get_sticker_confirm': $this->getStickerConfirm($iId);
          break;
        case 'back_ready_list': $this->backOrderReadyList($pSmarty, $iInternalId, $iId);
          break;
        case 'save_back_order_ready': $this->saveBackOrderReadyList($pSmarty, $iInternalId, $iId);
          break;
        case 'auto-source': $this->AutoSources($pSmarty, $iId);
          break;
          case 'streamsoft-details':
              $this->streamsoftDetails($pSmarty, $iId);
          break;
          case 'save-streamsoft-details':
              $this->saveStreamsoftDetails($pSmarty, $iId);
              break;
        case 'reset-balance': $this->ResetBalance($pSmarty, $iId);
          break;
        case 'renew_reservation': $this->renewReservation($iId, $pSmarty);
          break;
        default: $this->Show($pSmarty, $iInternalId, $iTransportId, $iOrderStatusId, $iMId);
          break;
      }
    }
  }
// end Module() function

    /**
     * @param $pSmarty
     * @param $iId
     */
    private function saveStreamsoftDetails($pSmarty, $iId) {

        if (empty($_POST) || $iId <= 0) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->streamsoftDetails($pSmarty, $iId);
            return;
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->streamsoftDetails($pSmarty, $iId);
            return;
        }

        include_once($_SERVER['DOCUMENT_ROOT'].'LIB/Supplies/StockSuppliceService.php');
        $stockSuppliceService = new \LIB\Supplies\StockSuppliceService($this->pDbMgr);
        $stockSuppliceService->updateReservationsToReduceByOrderId($iId);

        $sSql = 'DELETE FROM products_stock_reservations WHERE order_id = '.$iId;
        $this->pDbMgr->Query('profit24', $sSql);

        $a = 1;
        $aValues = [
            'erp_export' => empty($_POST['erp_export']) ? 'NULL' : FormatDate($_POST['erp_export']),
            'erp_send_ouz' => empty($_POST['erp_send_ouz']) ? 'NULL' : FormatDate($_POST['erp_send_ouz'])
        ];
        if ($this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iId.' LIMIT 1') === false) {
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas ustawiania na zamówieniu '.$iId.' danych ze streamsoftu '));
            return $this->streamsoftDetails($pSmarty, $iId);
        } else {
            $this->sMsg .= GetMessage(_('Ustawiono na zamówieniu ' . $iId . ' dane ' . print_r($aValues, true) . ' ze streamsoftu '), false);
            return $this->Show($pSmarty);
        }
    }

    /**
     * @param $pSmarty
     * @param $iId
     */
    private function streamsoftDetails($pSmarty, $iId) {
        global $aConfig;

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
        $sSql = 'SELECT 
                DATE_FORMAT(erp_export, "'.$aConfig['common']['cal_sql_date_format'].'") AS erp_export,
                DATE_FORMAT(erp_send_ouz, "'.$aConfig['common']['cal_sql_date_format'].'") AS erp_send_ouz
                FROM orders WHERE id = '.$iId;
        $aData = $this->pDbMgr->GetRow('profit24', $sSql);

        if (isset($_POST) && !empty($_POST)) {
            $aData = $_POST;
        }

        $pForm = new FormTable('order_status', _('Wysyłka do streama'), array('action' => phpSelf(['id' => $iId])), array('col_width' => 155), $aConfig['common']['js_validation'], array(), array('class' => 'bigger'));
        $pForm->AddHidden('do', 'save-streamsoft-details');

        $pForm->AddText('erp_export', _('Faktura w Streamsoft '), isset($aData['erp_export']) && $aData['erp_export'] != '' ? $aData['erp_export'] : '00-00-0000', array('maxlength' => 10,'style'=>'width:75px;'), '', 'date', false);
        $pForm->AddText('erp_send_ouz', _('Wysyłka do Streamsoft OUZ '), isset($aData['erp_send_ouz']) && $aData['erp_send_ouz'] != '' ? $aData['erp_send_ouz'] : '00-00-0000', array('maxlength' => 10,'style'=>'width:75px;'), '', 'date', false);
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));
        $str = $pForm->ShowForm();
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $str);
    }

  
  /**
   * 
   * @param int $iId
   * @return boolean
   */
  private function getDebug($pSmarty, $iId) {
    
    $sSql = 'SELECT order_session
             FROM orders_debug
             WHERE oid = '.$iId;
    
    $sData = $this->pDbMgr->GetOne('profit24', $sSql);
    dump(unserialize($sData));
    exit(0);
  }

  /**
   * 
   * @return boolean
   */
  private function validateAllForm() {
    // dolaczenie klasy walidatora
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    if (!$oValidator->Validate($_POST)) {
      $sErrLog = $oValidator->GetErrorString();
      $this->sMsg .= GetMessage($sErrLog, true);
      addLog($sErrLog, true);
      return false;
    }
    return true;
  }

  /**
   * 
   * @param string $sDefaultSortCol
   * @return string
   */
  private function getAdditionalSQL($sDefaultSortCol) {

    $sSql = " ORDER BY " . (isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : $sDefaultSortCol) .
            (isset($_GET['order']) && !empty($_GET['order']) ? ' ' . $_GET['order'] : '');
    return $sSql;
  }

  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param Smarty $pSmarty
   * @param int $iInternalId
   * @param array $iId
   */
  public function Changes($pSmarty, $iId) {
    global $pDbMgr, $aConfig;

    $oLogger = new logger($pDbMgr);
    $aChanges = $oLogger->getLogsOrders($iId, $this->getAdditionalSQL('id'));

    $aHeader = array(
        'refresh' => false,
        'search' => false,
        'per_page' => false,
        'form' => false,
        'checkboxes' => false,
        'second' => true,
        'action' => phpSelf(array('do' => 'changes', 'id' => $iId)),
        'refresh_link' => phpSelf(array('do' => 'changes', 'id' => $iId)),
    );

    $aAttribs = array(
        'width' => '100%',
        'border' => 0,
        'cellspacing' => 0,
        'cellpadding' => 0,
        'class' => 'viewHeaderTable',
    );

    $aRecordsHeader = array(
        array(
            'db_field' => 'id',
            'content' => _('#ID'),
            'sortable' => true,
            'width' => '10'
        ),
        array(
            'db_field' => 'created',
            'content' => _('Data'),
            'sortable' => true,
            'width' => '140'
        ),
        array(
            'db_field' => 'created_by',
            'content' => _('Zmienił'),
            'sortable' => true,
            'width' => '45'
        ),
        array(
            'db_field' => 'item_name',
            'content' => _('Dotyczy produktu'),
            'sortable' => true,
            'width' => '45',
        ),
        array(
            'db_field' => 'content',
            'content' => _('Ustawiono'),
            'sortable' => true,
        ),
        array(
            'db_field' => 'backtrace',
            'content' => _('backtrace'),
            'sortable' => true,
        ),
    );
  
    include_once('View/View.class.php');
    $oView = new View(_('Zmiany statusów'), $aHeader, $aAttribs);
    $oView->AddRecordsHeader($aRecordsHeader);

    foreach ($aChanges as $iKey => $aChange) {
      $aChanges[$iKey]['content'] = $oLogger->formateLogContentByLangArray($aConfig['lang'], $oLogger->getLogChangeArray($aChange['content']));
      if ($_SESSION['user']['type'] == '1') {
        $aChanges[$iKey]['backtrace'] = '<pre>'.print_r(json_decode(base64_decode($aChange['backtrace']), true), true).'</pre>';
      } else {
        $aChanges[$iKey]['backtrace'] = '<pre style="display:none;">'.print_r(json_decode(base64_decode($aChange['backtrace']), true), true).'</pre>';
      }
    }
    
    $aAttr = array();
    $oView->AddRecords($aChanges, $aAttr);
    $aRecordsFooter = array(
        array('go_back')
    );
    $oView->AddRecordsFooter($aRecordsFooter);

      $sStatusHTML = '<div style="text-align: left">';
      $sSql = 'SELECT id, symbol FROM external_providers ORDER BY id ASc';
      $aSources = $this->pDbMgr->GetAll('profit24', $sSql);
      foreach ($aSources as $aSource) {
          $sStatusHTML .= $aSource['id'] .' - '.$aSource['symbol'].'<br />';
      }
      $sStatusHTML .= '</div>';

      $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $oView->Show().$sStatusHTML);
  }

  /**
   * Metoda sprawdza status paczki u kontrahenta
   * 
   * @global array $aConfig
   * @global DatabaseManager $pDbMgr
   * @param object $pSmarty
   * @param int $iInternalId
   * @param int $iId
   */
  private function getPackageStatus($pSmarty, $iId) {
    global $pDbMgr, $aConfig;

    include_once('Form/FormTable.class.php');

    $sSql = 'SELECT O.transport_number, OTM.symbol as transport_symbol
             FROM orders AS O
             JOIN orders_transport_means AS OTM
              ON O.transport_id = OTM.id
              WHERE O.id = ' . $iId;

    $aOrderData = $pDbMgr->GetRow('profit24', $sSql);
    $pForm = new FormTable('package_status', _('Statusy Paczki'), array());

    if ($aOrderData['transport_number'] != '') {

      include_once($aConfig['common']['client_base_path'] . 'LIB/autoloader.php');
      $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
      $oShipment = new Shipment($aOrderData['transport_symbol'], $pDbMgr, $bTestMode);
      $oStatusesCollection = $oShipment->getDeliveryStatus($iId, $aOrderData['transport_number']);


      if (!empty($oStatusesCollection)) {

        foreach ($oStatusesCollection as $oStatus) {
          $pForm->AddRow(_('Status:'), $oStatus->sStatusName . ' - ' . date('d.m.Y H:i:s', strtotime($oStatus->sStatusDate)));
        }
      } else {
        $this->sMsg .= GetMessage(_('W chwili obecnej brak informacji na temat statusu tej przesyłki'));
      }
    }

    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));

    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $pForm->ShowForm());
  }
// end of getPackageStatus() method

  /**
   * Metoda usuwa plik PDF z wygenerowaną etykietą adresową
   * 
   * @deprecated since version 0.01 Uwaga metoda aktualnie nie używana, ale przewidziane aby była uzywana
   * @param string $sTransportNumber
   */
  private function deleteSticker($sTransportNumber) {
    global $aConfig, $pDbMgr;

    $sFilename = $aConfig['common']['paczkomaty_dir'] . 'Paczka_' . $sTransportNumber . '.pdf';
    if (file_exists($aConfig['common']['base_path'] . '/' . $sFilename)) {
      unlink($sFilename);
    }

    $sFilename = $aConfig['common']['fedex_dir'] . 'etykiete_' . $sTransportNumber . '.pdf';
    if (file_exists($aConfig['common']['base_path'] . '/' . $sFilename)) {
      unlink($sFilename);
    }

    $oPwR = new PaczkaWRuchu($aConfig['common']['status'] == 'development', $pDbMgr);
    $sPwRFileName = $oPwR->getTransportListFilename($aConfig, $sTransportNumber);
    if (file_exists($sPwRFileName)) {
      unlink($sPwRFileName);
    }

    $sFilename = $aConfig['common']['poczta_polska_dir'] . 'NalepkaAdresowa_' . $sTransportNumber . '.pdf';
    if (file_exists($aConfig['common']['base_path'] . '/' . $sFilename)) {
      unlink($sFilename);
    }

    $oSzP = new Orlen($aConfig['common']['status'] == 'development', $pDbMgr);
    $sSzPFileName = $oSzP->getTransportListFilename($aConfig, $sTransportNumber);
    if (file_exists($sSzPFileName)) {
      unlink($sSzPFileName);
    }

    $oTBA = new TBA($aConfig['common']['status'] == 'development', $pDbMgr);
    $sTBAFileName = $oTBA->getTransportListFilename($aConfig, $sTransportNumber);
    if (file_exists($sTBAFileName)) {
      unlink($sTBAFileName);
    }
  }
// end of deleteSticker() method

  /**
   * Metoda zwraca Id transportu na podstawie Id zamówienia
   * 
   * @param string $iOrderId
   * return string
   */
  private function getTransportNumber($iOrderId) {
    global $pDbMgr;

    $sSql = 'SELECT transport_number 
             FROM orders
             WHERE id = ' . $iOrderId;

    return $pDbMgr->GetOne('profit24', $sSql);
  }
// end of getTransportNumber() method  

  /**
   * Zapisujemy cofnięcie składowych na tramwaj tworzymy nową listę przesunięcia
   * 
   * @global array $aConfig
   * @global DatabaseManager $pDbMgr
   * @param object $pSmarty
   * @param int $iInternalId
   * @param int $iId
   * @return string
   */
  private function saveBackOrderReadyList($pSmarty, $iInternalId, $iId) {
    global $aConfig, $pDbMgr;
    $bIsErr = false;
    $sListNumber = '';
    include_once('modules/m_zamowienia_magazyn_sorter/Module_back_lack_train.class.php');

    if (empty($_POST)) {
      // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
      $this->Show($pSmarty, $iInternalId);
      return;
    }
    // dolaczenie klasy walidatora
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    if (!$oValidator->Validate($_POST)) {
      $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
      // wystapil blad wypelnienia formularza
      // wyswietlenie formularza wraz z komunikatem bledu
      $this->Show($pSmarty, $iInternalId);
      return;
    }

    // pobieramy Id transportu w celu usunięcia etykiety adresowej    
    $sTransportNumber = $this->getTransportNumber($iId);

    // pobieramy zamówienia powiązane w celu usunięcia z nich etykiety adresowej
    $aLinkedOrders = $this->getOrdersLinked($iId);

    $aOrdersToIterate[] = $iId;

    if ($aLinkedOrders) {
      foreach ($aLinkedOrders as $aLinkedOrder) {
        $aOrdersToIterate[] = $aLinkedOrder['id'];
      }
    }

    foreach ($aOrdersToIterate as $iId) {

      $pDbMgr->BeginTransaction('profit24');

      $sSql = 'SELECT id, "1" as ar_key 
               FROM orders_items
               WHERE order_id = ' . $iId . '
                 AND get_ready_list = "1" ';
      $aOrderItemsByKey = $pDbMgr->GetAssoc('profit24', $sSql);

      $oMzblt = new Module__zamowienia_magazyn_sorter__back_lack_train($pSmarty, false);
      // źródło to magazyn migracja
      if ($oMzblt->addMoveList($_POST['pack_number'], $aOrderItemsByKey, '34', $sListNumber) === false) {
        $bIsErr = true;
      }
      if ($bIsErr === false) {
        // zmieniamy składowe w zamówieniu 
        $sSql = 'UPDATE orders_items 
                  SET get_ready_list = "0"
                 WHERE order_id = ' . $iId . '
                  AND get_ready_list = "1" ';
        if ($pDbMgr->Query('profit24', $sSql) === false) {
          $bIsErr = true;
        }
      }

      if ($bIsErr === false) {
        // zmieniamy składowe w zamówieniu 
        $sSql = 'UPDATE orders
                  SET print_ready_list = "0"
                 WHERE id = ' . $iId . '
                  AND print_ready_list = "1" ';
        if ($pDbMgr->Query('profit24', $sSql) === false) {
          $bIsErr = true;
        }
      }

      if ($bIsErr === true) {
        // błąd
        $sMsg = _('Wystąpił błąd podczas przenoszenia listy na tramwaj<br /> DEBUG: ' . $_POST['pack_number'] . ' : ' . print_r($aOrderItemsByKey, true) . ' !');
        AddLog($sMsg);
        $this->sMsg = GetMessage($sMsg);
        $pDbMgr->RollbackTransaction('profit24');
      } else {
        // wszystko ok
        $sMsg = _('Zapisano nową listę przesunięcia towaru na tramwaj o numerze: ' . $sListNumber);
        AddLog($sMsg, false);
        $this->sMsg = GetMessage($sMsg, false);
        $pDbMgr->CommitTransaction('profit24');

        // usuwamy etykietę adresową
        // $this->deleteSticker($sTransportNumber);
      }
    }

    $this->Show($pSmarty, $iInternalId);
  }

  /**
   * Metoda pobiera numer półki, aby później cofnąć zamówienie na tramwaj
   * 
   * @param object $pSmarty
   * @param int $iInternalId
   * @param int $iId
   */
  private function backOrderReadyList($pSmarty, $iInternalId, $iId) {
    // pobierzmy numer półki na którą ma trafić 

    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('back_order_ready', _('Wprowadź numer półki na którą położysz składowe tego zamówienia'), array('action' => phpSelf(array('id' => $iId, 'ppid' => $iInternalId))));
    $pForm->AddText('pack_number', _('Numer półki'), '', array(), '', 'uint', true);
    $pForm->AddInputButton('send', _('Zapisz'));
    $pForm->AddHidden('do', 'save_back_order_ready');

    $sJS = '
      <script type="text/javascript">
        $( function() {
          $("#pack_number").focus();
        });
      </script>
      ';

    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $pForm->ShowForm() . $sJS);
  }
// end of backOrderReadyList() method

  /**
   * Metoda zapisuje aktualną/nową datę wydania zapowiedzi
   * 
   * @global type $aConfig
   * @param type $iOId
   * @param type $iOItemId
   */
  private function SaveShipmentDate($iOId, $iOItemId) {
    global $aConfig, $pDbMgr;
    $bIsErr = false;
    Common::BeginTransaction();

    if ($iOId > 0 && $iOItemId > 0 && preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $_POST['new_shipment_date'])) {
      $sNewShipmentTime = preg_replace("/^(\d{2})\.(\d{2})\.(\d{4})$/", "$3-$2-$1", $_POST['new_shipment_date']);

      $oLogger = new logger($pDbMgr);
      $aValues = array('current_shipment_date' => $sNewShipmentTime);
      $oLogger->addLog($iOId, $_SESSION['user']['id'], $aValues, $iOItemId);
      if (Common::Update($aConfig['tabls']['prefix'] . "orders_items", $aValues, 'id = ' . $iOItemId . ' AND order_id = ' . $iOId) === false) {
        $bIsErr = true;
      } else {
        // zmieniamy daty w profit i w it jeśli się da

        $sSql = "SELECT P.id 
                 FROM products  AS P
                 JOIN orders_items AS OI
                  ON OI.product_id = P.id
                 WHERE 
                  OI.id = " . $iOItemId . " AND 
                  OI.order_id = " . $iOId;
        $iProfit24ProductId = Common::GetOne($sSql);

        // zmieniamy w profit24
        if ($iProfit24ProductId > 0) {
          $sSql = "UPDATE products
                   SET shipment_date='" . $sNewShipmentTime . "'
                   WHERE id = " . $iProfit24ProductId;
          if ($pDbMgr->Query('profit24', $sSql) === false) {
            $bIsErr = true;
          }

          $sSql = "UPDATE products_shadow
                   SET shipment_date='" . $sNewShipmentTime . "'
                   WHERE id = " . $iProfit24ProductId;
          if ($pDbMgr->Query('profit24', $sSql) === false) {
            $bIsErr = true;
          }

          // pobieramy id produktu w it
          $sSql = "SELECT id FROM products WHERE profit24_id = " . $iProfit24ProductId;
          $iItProductId = @$pDbMgr->GetOne('it', $sSql);
          if ($iItProductId > 0) {
            $sSql = "UPDATE products
                     SET shipment_date='" . $sNewShipmentTime . "'
                     WHERE id = " . $iItProductId;
            @$pDbMgr->Query('it', $sSql);

            $sSql = "UPDATE products_shadow
                     SET shipment_date='" . $sNewShipmentTime . "'
                     WHERE id = " . $iItProductId;
            @$pDbMgr->Query('it', $sSql);
          }

          // pobieramy id produktu w np
          $sSql = "SELECT id FROM products WHERE profit24_id = " . $iProfit24ProductId;
          $iNPProductId = @$pDbMgr->GetOne('np', $sSql);
          if ($iNPProductId > 0) {
            $sSql = "UPDATE products
                     SET shipment_date='" . $sNewShipmentTime . "'
                     WHERE id = " . $iNPProductId;
            @$pDbMgr->Query('np', $sSql);

            $sSql = "UPDATE products_shadow
                     SET shipment_date='" . $sNewShipmentTime . "'
                     WHERE id = " . $iNPProductId;
            @$pDbMgr->Query('np', $sSql);
          }
        }
      }

      if ($bIsErr === TRUE) {
        Common::RollbackTransaction();
        $sMsgErr = sprintf($aConfig['lang'][$this->sModule]['save_shipment_date_err'], $_POST['new_shipment_date'], $iOId, $iOItemId);
        echo $sMsgErr;
        AddLog($sMsgErr, true);
        die;
      } else {
        Common::CommitTransaction();
        $sMsgErr = sprintf($aConfig['lang'][$this->sModule]['save_shipment_date_ok'], $_POST['new_shipment_date'], $iOId, $iOItemId);
        AddLog($sMsgErr, false);
        echo '1';
        die;
      }
    } else {
      $sMsgErr = $aConfig['lang'][$this->sModule]['save_shipment_date_err_data'];
      echo $sMsgErr;
      die;
    }
    echo '-1';
    die;
  }
// end of SaveShipmentDate() method

  /**
   * Metoda pobiera etykiete
   *
   * @global array $aConfig
   * @param object $pSmarty
   * @param integer $iId 
   */
  private function getSticker($iId) {
    global $aConfig, $pDbMgr;

    $sSql = "SELECT transport_number, tmp_transport_number, transport_id FROM " . $aConfig['tabls']['prefix'] . "orders
						 WHERE id=" . $iId;
    $aOrderSelData = $pDbMgr->GetRow('profit24', $sSql);

    $oShipment = new Shipment($aOrderSelData['transport_id'], $pDbMgr);
    $sFilePathName = $oShipment->getTransportListFilename($aConfig, $aOrderSelData['transport_number']);

    if (file_exists($sFilePathName)) {
      // pobianie PDF'a
      header('Content-Type: application/pdf');
      header('Content-Disposition: attachment; filename="' . $this->getFileNameByFilePath($sFilePathName) . '"');
      header('Content-Transfer-Encoding: binary');
      echo file_get_contents($sFilePathName);
      exit();
    }
    exit();
  }
// end of getSticker() method

  /**
   * Metoda pobiera potwierdzenie nadania, zamówienia
   *
   * @param int $iId 
   */
  private function getStickerConfirm($iId) {
    global $aConfig, $pDbMgr;

    $sSql = "SELECT transport_number, transport_id FROM " . $aConfig['tabls']['prefix'] . "orders
						 WHERE id=" . $iId;
    $aOrderSelData = $pDbMgr->GetRow('profit24', $sSql);

    $oShipment = new Shipment($aOrderSelData['transport_id'], $pDbMgr);
    $sFilePathName = $oShipment->getOrderDeliverFilePath($iId);
    if ($sFilePathName != '') {
      if (file_exists($sFilePathName)) {
        // pobianie PDF'a

        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="' . $this->getFileNameByFilePath($sFilePathName) . '"');
        header('Content-Transfer-Encoding: binary');
        echo file_get_contents($sFilePathName);
      }
    } else {
      echo 'Błąd brak pliku na dysku';
    }
  }
// end of getPaczkomatyConfirm() method

  /**
   * Pobieramy nazwę pliku ze ścieżki
   * 
   * @param string $sFilePathName
   * @return string
   */
  private function getFileNameByFilePath($sFilePathName) {

    $aData = explode(DIRECTORY_SEPARATOR, $sFilePathName);
    return array_pop($aData);
  }

  /**
   * Zmienia status zamówienia
   * @param int $iId - id zamówienia
   * @param array ref $aOrder - dane zamówienia
   * @return bool - success
   */
  private function updateOrderStatus($iId, &$aOrder, $bOmmitPrevStatus = false) {
    global $aConfig;
    $bRecount = false;
    $aValues = array();
    // okreslenie aktualnego statusu zamowienia
    $iCurrentStatus = (int) $aOrder['order_status'];
    $_POST['order_status'] = (int) $_POST['order_status'];

    $oLogger = new logger($this->pDbMgr);

    if ($iCurrentStatus > $_POST['order_status'] && $bOmmitPrevStatus == false) {
      // nie mozna zmienic statusu na wczesniejszy
      $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['status_change_err']);
      return false;
    } elseif ($iCurrentStatus < $_POST['order_status'] || $bOmmitPrevStatus == true) {
      if ($iCurrentStatus < $_POST['order_status'] || $bOmmitPrevStatus == true) {
        if ($bOmmitPrevStatus == false) {
          if ($_POST['order_status'] == 1) {
            // aby zmienic status na 'w realizacji' ustaw ksiazke na 'do zamowienia'
            $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['status_change_1_err']);
            //$this->Details($pSmarty, $iId);
            return false;
          }
          if ($_POST['order_status'] == 2) {
            // aby zmienic status na 'skompletowane' ustaw ksiazki na 'jest'
            $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['status_change_2_err']);
            //$this->Details($pSmarty, $iId);
            return false;
          }
          if ($_POST['order_status'] == 3) {
            // aby zmienic status na 'zatwierdzone' wydrukuj fakturę
            $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['status_change_3_err']);
            //$this->Details($pSmarty, $iId);
            return false;
          }
          if ($_POST['order_status'] == 4 && $iCurrentStatus != 3) {
            // aby zmienic status na 'zrealizowane' zamowienie musi być 'skompletowane'
            $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['status_change_4_err']);
            //$this->Details($pSmarty, $iId);
            return false;
          }
          if ($_POST['order_status'] == 4 && $aOrder['invoice_ready'] != '1') {
            // aby zmienic status na 'zrealizowane' zamowienie musi posiadac fakture
            $this->sMsg .= GetMessage(sprintf($aConfig['lang'][$this->sModule]['status_change_4_invoice_err'], $aOrder['order_number']));
            //$this->Details($pSmarty, $iId);
            return false;
          }
          // M. Korecki (01.07.2011) - ponizszy if jest dla mnie niezrozumialy i podejrzany
          if ($_POST['order_status'] == 5 && $aOrder['invoice_ready'] == 4) {
            // aby zmienic status na 'zrealizowane' zamowienie musi być opłacone
            $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['status_change_5_err']);
            //$this->Details($pSmarty, $iId);
            return false;
          }
        }
        $aValues['status_' . $_POST['order_status'] . '_update'] = 'NOW()';
        $aValues['status_' . $_POST['order_status'] . '_update_by'] = $_SESSION['user']['name'];
        $aValues['order_status'] = $_POST['order_status'];
        // statusy wewnętrzne
        if ($_POST['order_status'] == 5) { // anulowane
          $aValues['internal_status'] = 'A'; // anulowane
        }
        if ($_POST['order_status'] == 4) { // zrealizowane
          $aValues['internal_status'] = '9'; //wysłane
        }
        if ($_POST['order_status'] == 1) { // w realizacji
          $aValues['internal_status'] = '3'; // do realizacji
        }
        if ($_POST['order_status'] == 2) { // skompletowane
          $aValues['internal_status'] = '4'; // skompletowane
        }
        if ($_POST['order_status'] == 3) { // zatwierdzone
          $aValues['internal_status'] = '5'; // wysyłka
        }
      }

      // aktualizacja statusu
      if (!empty($aValues)) {
        $aChanges = $this->pDbMgr->getChangedUpdateValues($aValues, $aOrder);
        $oLogger->addLog($iId, $_SESSION['user']['id'], $aChanges);
        if ($aValues['order_status'] == 5) {
          $this->deleteOrderReservation($iId);
        }
        if (Common::Update($aConfig['tabls']['prefix'] . "orders", $aValues, "id = " . $iId) === false) {
          return false;
        }
        if ($bRecount) {
          if ($this->recountOrder($iId, $aOrder['payment_id']) === false) {
            return false;
          }
        }
      }
      return true;
    }
  }
// end of updateOrderStatus() method

  /**
   * 
   * @param int $iOrderId
   */
  private function deleteOrderReservation($iOrderId) {
    $oReservation = new \omniaCMS\lib\Products\ProductsStockReservations($this->pDbMgr);
    $orderItems = $this->getOrdersItemsToDeleteReservation($iOrderId);
    foreach ($orderItems as $aItem) {
      $oReservation->deleteReservation($aItem['source'], $iOrderId, $aItem['id']);
    }
  }

  /**
   * 
   * @param int $iOrderId
   * @return array
   */
  private function getOrdersItemsToDeleteReservation($iOrderId) {

    $sSql = 'SELECT id, source
             FROM orders_items
             WHERE order_id = ' . $iOrderId . '
               AND deleted = "0" AND packet = "0"';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
// end of getOrdersItemsToDeleteReservation() method

  /**
   * Metoda wyswietla szczegolowe informacje o zamowieniu z mozliwoscia
   * modyfikacji elementów, danych użytkownika i statusu zamówienia
   *
   * @deprecated since version 0.01
   * @param		object	$pSmarty
   * @param   integer $iPId id zakładki
   * @param		integer	$iId	Id wysiwetlanego zamowienia
   * @param   bool $bAssignSmarty
   * @return	void
   */
  public function Details(&$pSmarty, $iPId, $iId, $bAssignSmarty = TRUE) {
    global $aConfig, $pDbMgr;

    include_once('lib/Orders/orderDetails.class.php');
    $oOrderDetails = new orderDetails($pDbMgr, $aConfig, $pSmarty, $this->oParent, $this->iUId, $this->sModule);
    if ($this->sMsg != '') {
      $oOrderDetails->sMsg .= $this->sMsg;
    }
    return $oOrderDetails->Details($pSmarty, $iPId, $iId, $bAssignSmarty);
  }
// end of Details() function

  /**
   * Metoda pobiera symbol dla metody transportu
   * @external
   *
   * @global array $aConfig
   * @param type $iTransportId
   * @return type 
   */
  public function getTransportMeansSymbol($iTransportId) {
    global $aConfig;

    $sSql = "SELECT symbol FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means
						 WHERE id=" . $iTransportId;
    return Common::GetOne($sSql);
  }
// end of () method

  /**
   * Metoda pobiera id ksiegarnii źródłowej
   *
   * @global integer $aConfig
   * @global integer $pDbMgr
   * @param integer $iId
   * @return id
   */
  private function getProductIdProfit24($iId, $sWebsite) {
    global $aConfig, $pDbMgr;

    $sSql = "SELECT profit24_id FROM " . $aConfig['tabls']['prefix'] . "products 
						 WHERE id = " . $iId;
    return $pDbMgr->GetOne($sWebsite, $sSql);
  }
// end of getProductIdProfit24() method

  /**
   * Formaularz grupowej zmiany statusu
   * @param $pSmarty
   * @return unknown_type
   */
  private function changeStatusForm(&$pSmarty) {
    global $aConfig;
    // dolaczenie klasy FormTable
    include_once('Form/FormTable.class.php');


    $pForm = new FormTable('order_status', $aConfig['lang'][$this->sModule]['change_status_header'], array('action' => phpSelf()), array('col_width' => 155), $aConfig['common']['js_validation'], array(), array('class' => 'bigger'));
    $pForm->AddHidden('do', 'group_update_status');

    // statusy główne
    $aOptions = array(
        array('style' => 'float: left', 'value' => '4', 'label' => '<span style="font-size:18px; color:#007800;">' . $aConfig['lang'][$this->sModule]['status_4'] . '</span>'),
        array('style' => 'float: left', 'value' => '5', 'label' => '<span style="font-size:18px; color:#f00;">' . $aConfig['lang'][$this->sModule]['status_5'] . '</span>')
    );

    if ($_SESSION['user']['priv_order_status'] == '1' || $_SESSION['user']['type'] == '1') {
      $aOptions[] = array('style' => 'float: left', 'value' => '1', 'label' => '<span style="font-size:18px; color:#808000;">' . $aConfig['lang'][$this->sModule]['status_1'] . '</span>');
      $aOptions[] = array('style' => 'float: left', 'value' => '2', 'label' => '<span style="font-size:18px; color:#800080;">' . $aConfig['lang'][$this->sModule]['status_2'] . '</span>');
      $aOptions[] = array('style' => 'float: left', 'value' => '3', 'label' => '<span style="font-size:18px; color:#008080;">' . $aConfig['lang'][$this->sModule]['status_3'] . '</span>');
    }


    if (isset($_POST['order_id'])) {
      $_POST['delete'] = &$_POST['order_id'];
      $_POST['f_status'] = $_POST['prev_status'];
    }

    $pForm->AddMergedRow($aConfig['lang'][$this->sModule]['orders_list'], array('class' => 'merged', 'style' => 'padding-left: 10px'));
    $pForm->AddMergedRow($this->GetOrderList(array_keys($_POST['delete'])));


    $pForm->AddHidden('prev_status', $_POST['f_status']);

    foreach ($_POST['delete'] as $iKey => $iValue) {
      $pForm->AddHidden('order_id[' . $iKey . ']', $iValue);
    }

    $pForm->AddRadioSet('order_status', $aConfig['lang'][$this->sModule]['order_status'], $aOptions, $_POST['f_status'], '', false, false, array(), array('style' => 'line-height:72px;'));

    $pForm->AddCheckBox('send_mail', $aConfig['lang'][$this->sModule]['send_mail'], array(), '', true, false);
    // usuwanie faktury
    if ($_SESSION['user']['priv_order_status'] == '1' || $_SESSION['user']['type'] == '1') {
      $pForm->AddMergedRow(_('Zaawansowane'), array('class' => 'merged'));
      $pForm->AddCheckbox('delete_invoice', $aConfig['lang'][$this->sModule]['delete_invoice'], array(), '', !empty($_POST['delete_invoice']), false);
    }



    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));

    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
  }
// end of changeStatusForm() method

  /**
   * Grupowa zmiana statusu zamówień
   * 
   * @param $pSmarty
   * @return unknown_type
   */
  function GroupChangeStatus(&$pSmarty, $iPId) {
    global $aConfig;
    if (empty($_POST)) {
      // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
      $this->Show($pSmarty, $iPId);
      return;
    }
    $bIsErr = false;
    // dolaczenie klasy walidatora
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    if (!$oValidator->Validate($_POST)) {
      $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
      // wystapil blad wypelnienia formularza
      // wyswietlenie formularza wraz z komunikatem bledu
      $this->changeStatusForm($pSmarty);
      return;
    }

    Common::BeginTransaction();
    $sIds = '';
    $iCount = 0;

      global $pDbMgr;

    if (isset($_POST['order_status']) && $_POST['order_status'] != '') {
      foreach ($_POST['order_id'] as $iKey => $iVal) {

          $reservatonManager = new ReservationManager($pDbMgr);
          $oldOrderReservations = $reservatonManager->getDataProvider()->getReservationsByOrderId($iKey);
          $oldOrderReservations = ArrayHelper::toKeyValues('id', $oldOrderReservations);

        $aOrder = $this->getOrderData($iKey);
        // $this->insertDebug($pDbMgr, $aOrder);
        // aktualizacja statusu
        if (!$bIsErr) {

          if (!$aOrder['invoice_ready'] || $_POST['order_status'] != 5) {
            $bOmmitPrevStatus = false;
            // jeśli uprawnienia pozwalają to można cofnąć status
            if ($_SESSION['user']['priv_order_status'] == '1' || $_SESSION['user']['type'] == '1') {
              $bOmmitPrevStatus = true;
            }

            if ($this->updateOrderStatus($iKey, $aOrder, $bOmmitPrevStatus) === false) {
              $bIsErr = true;
              echo '1';
            }
            $sIds .= $aOrder['order_number'] . ', ';
            $iCount++;
            include_once('OrderRecount.class.php');
            $pOrderRecount = new OrderRecount();
            if ($bOmmitPrevStatus && $_POST['order_status'] != $aOrder['invoice_ready']) {
              // przelicz status wewnętrzny
              if ($pOrderRecount->recountOrder($iKey, false, false) === false) {
                $bIsErr = true;
                echo '2';
              }
            }
          } else {
            // wystawiona fatura i chce anulować -> błąd !!!
            $bIsErr = 2;
            break;
          }
        }

          // usuwanie rezerwacji
          if ($bIsErr != true && $aOrder['order_status'] != '3' && $aOrder['order_status'] != '4') {
              try {
                  //anulowanie zamowienia i usuwanie rezerwacji
                  if ($_POST['order_status'] == 5) {
                      $stockLocation = new \LIB\EntityManager\Entites\StockLocation($this->pDbMgr);
                      $stockLocation->unreserveStockLocationOrder($iKey);

                      // usunięcie pozycji z braków
                      $highLevelStock = new \magazine\HighLevelStock($this->pDbMgr, \LIB\EntityManager\Entites\Magazine::TYPE_HIGH_STOCK_SUPPLIES);
                      $highLevelStock->cancelOrderReservations($iKey);

                      $reservatonManager->removeOrder($aOrder, $oldOrderReservations);
                  } else if($_POST['order_status'] != 3 && $_POST['order_status'] != 4) {
                      $forceCreate = false;

                      if (empty($oldOrderReservations)) {
                          $reservatonManager->createLocalReservationsByOrderId($iKey);
                          $forceCreate = true;
                      }

                      $reservationResult = $reservatonManager->createStreamsoftReservation($oldOrderReservations, $iKey, $forceCreate);

                      // zamowienie czesciowe, wyswielamy info
                      if(false === $reservationResult){
                          $this->sMsg .= GetMessage($reservatonManager->getErrorMessage());
                      }
                  }
              } catch (Exception $e) {
                  $bIsErr = true;
                  $this->sMsg .= GetMessage($e->getMessage());
                  $reservatonManager->rollbackChanges();
              }
          }
      }
    }

    if (!empty($sIds)) {
      $sIds = substr($sIds, 0, -2);
    }

    // jeśli uprawnienia pozwalają to można usunac fakture
    if ($_SESSION['user']['priv_order_status'] == '1' || $_SESSION['user']['type'] == '1') {
      $iDeleteCount = 0;
      if ($_POST['delete_invoice']) {
        foreach ($_POST['order_id'] as $iKey => $iVal) {
          if ($this->deleteInvoice($iKey) === false) {
            $bIsErr = true;
          } else {
            $iDeleteCount++;
          }
        }
      }
    }


    if ($bIsErr != true) {
      if (isset($_POST['send_mail'])) {
        foreach ($_POST['order_id'] as $iKey => $iVal) {
          $this->sendOrderStatusMail($iKey);
        }
      }
    }

    if ($bIsErr !== false) {
      Common::RollbackTransaction();
      // wyswietlenie komunikatu o niepowodzeniu
      // oraz ponowne wyswietlenie formularza edycji
      if ($bIsErr === 2) {
        $sMsg = $aConfig['lang'][$this->sModule]['group_status_err_2'];
      } else {
        $sMsg = sprintf($aConfig['lang'][$this->sModule]['group_status_err'], $iCount);
      }
      $this->sMsg .= GetMessage($sMsg);
      AddLog($sMsg);
      $this->Show($pSmarty, $iPId);
    } else {

        if ($aOrder['order_status'] == 5) {

            $oosid = $aOrder['id'];
            $pDbMgr->Query('profit24', "UPDATE orders set is_streamsoft_canceled = 0 WHERE id = $oosid limit 1");
        }

      Common::CommitTransaction();

      $sMsg = sprintf($aConfig['lang'][$this->sModule]['group_status_ok'], $iCount);
      if ($iDeleteCount > 0) {
        $sMsg .= sprintf($aConfig['lang'][$this->sModule]['group_delete_ok'], $iDeleteCount);
      }
      $this->sMsg .= GetMessage($sMsg, false);
      // dodanie informacji do logow
      AddLog($sMsg, false);
      $this->Show($pSmarty, $iPId);
    }
  }
// end of GroupChangeStatus()

  private function insertDebug($pDbMgr, $aOrder)
  {
      $newOrderStatus = $_POST['order_status'];

    if ($aOrder['order_status'] == 3 && $newOrderStatus == 4) {

      $now = new \DateTime();

        $aValues = [
          'order_id' => $aOrder['id'],
          'transport_id' => $aOrder['transport_id'],
          'website_id' => $aOrder['website_id'],
          'get_deliver' => 0,
          'created' => $now->format('Y-m-d H:i:s'),
          'created_by' => $_SESSION['user']['name'],
        ];

      $pDbMgr->Insert('profit24', 'orders_transport_buffer', $aValues);
    }
  }

  /**
   * Metoda usuwa fakture z tabeli z fakturami oraz ustawia invoice_ready na '0'
   *
   * @global array $aConfig
   * @param type $iOId
   * @return type 
   */
  private function deleteInvoice($iOId) {
    global $aConfig;
    include_once('Invoice.class.php');
    $oInvoice = new Invoice();

    // aktualuzacja najpierw invoice_ready
    $aValues = array('invoice_ready' => '0');
    if (Common::Update($aConfig['tabls']['prefix'] . "orders", $aValues, ' id = ' . $iOId) === false) {
      return false;
    }

    return $oInvoice->deleteInvoice($iOId, false);
  }
// end of deleteInvoice() method

  /**
   * Metoda pobiera dostępność zakupu pozycji w zamówieniu
   * 
   * @return array
   */
  private function getFilterOrderItemsShipmentAvailablity() {

    $aListOptions = array(
        array(
            'label' => _('Dostępne od ręki'),
            'value' => '0',
        ),
    );
    return addDefaultValue($aListOptions);
  }
// end of getFilterOrderItemsShipmentAvailablity() method


    /**
     * @param $searchStr
     * @return string
     */
    private function detectQueryTypeSearch($searchStr) {
        global $aConfig;

        $aMatches = [];
        preg_match($aConfig['class']['form']['email']['pcre'], $searchStr, $aMatches);
        if (isset($aMatches[0])){
            return 'email';
        }
        return false;
    }

    /**
     * Metoda zmienia string na potrzeby wyszukiwania
     *  ciąg *mowa* zmieniany jest na %mowa%
     *  ciąg *mowa ciała* zamieniany jest na %mowa ciała%
     *
     * @param string $sSearchStr
     * @return string
     */
    function parseSearchString($sSearchStr)
    {

        // preg_replace('/\s+/', '%', )
        if (mb_substr($sSearchStr, 0, 1) === '*') {
            $sSearchStr = '%' . mb_substr($sSearchStr, 1);
        }
        if (mb_substr($sSearchStr, -1, 1) === '*') {
            $sSearchStr = mb_substr($sSearchStr, 0, -1) . '%';
        }
        return $sSearchStr;
    } // end of parseSearchString() method


    /**
     * @param $searchStr
     * @return string
     */
    private function getQuerySearchString($searchStr) {
        global $aConfig;

        $searchStr = $this->parseSearchString($searchStr);

        if (isset($_POST['f_search_invoice_number'])) {
            $sSql = " AND A.invoice_number LIKE '" . $searchStr . "' ";
        }
        elseif (isset($_POST['f_search_email'])) {
            $sSql = " AND A.email LIKE '" . $searchStr . "' ";
        }
        elseif (isset($_POST['f_search_transport_number'])) {
            $sSql = " AND A.transport_number LIKE '" . $searchStr . "' ";
        }
        elseif (isset($_POST['f_seller_streamsoft_id'])) {
            $_POST['f_seller_streamsoft_id'] = '1';
            $sSql = " AND A.seller_streamsoft_id LIKE '" . $searchStr . "' ";
        }
        else {
            $_POST['f_search_order_number'] = '1';
            $sSql = " AND A.order_number LIKE '" . $searchStr . "' ";
        }
        return $sSql;
    }


    /**
     * @return string
     */
    private function getOptimizationLimitRows() {

        $sLimit = ' ';
        if (!isset($_POST['per_page'])) {
            if (!isset($_POST['page']) && !isset($_GET['page'])) {
                $sLimit = ' LIMIT 160';
            } else {
                $iPageCurrrent = 0;
                if (isset($_POST['page'])) {
                    $iPageCurrrent = $_POST['page'];
                } elseif (isset($_GET['page'])) {
                    $iPageCurrrent = $_GET['page'];
                }
                if ($iPageCurrrent < 6) {
                    $sLimit = ' LIMIT 160';
                }
            }
        }

        return $sLimit;
    }

  /**
   * Metoda wyswietla liste zlozonych zamowien
   *
   * @param	object	$pSmarty
   * @return	void
   */
  private function Show(&$pSmarty, $iInternalStatus = '0', $iTransportId = '', $iNull = 0, $iMId = 0) {
    global $aConfig;

    if (!isset($_POST['f_specialSearchOption']) && isset($_POST['search'])) {
        $_POST['f_specialSearchOption'] = 0;
    }


    if (isset($_GET['f_shipment_date_expected'])) {
      $_POST['f_shipment_date_expected'] = $_GET['f_shipment_date_expected'];
      unset($_GET['f_shipment_date_expected']);
    }

    $_POST['search'] = trim($_POST['search']);
    //zeby sie nie kasowala zmienna widoku
    if ($_POST['search'] == '')
      unset($_POST['search']);

    // dolaczenie klasy View
    include_once('View/View.class.php');
    include_once('modules/m_oferta_produktowa/Module_Common.class.php');
    $oOfertaCommon = new Module_Common;

    // zapamietanie opcji
    if ($iInternalStatus == '8' && $iTransportId != '') {
      $_SESSION['_saveView'][$this->sModule]['transport_id'] = $iTransportId;
    } elseif ($iInternalStatus == '8' && $_SESSION['_saveView'][$this->sModule] != '') {
      $iTransportId = $_SESSION['_saveView'][$this->sModule]['transport_id'];
    }
    rememberViewState($this->sModule . ($iInternalStatus != '0' ? '_' . $iInternalStatus : '') . ($iTransportId != '' ? '_' . $iTransportId : ''));

    if (!isset($_POST['f_search_invoice_number']) &&
      !isset($_POST['f_search_email']) &&
      !isset($_POST['f_search_transport_number']) &&
      !isset($_POST['f_search_order_number']) &&
      !isset($_POST['f_seller_streamsoft_id'])
    ) {
      // tutaj może coś cofnąć
      $_POST['f_search_order_number'] = '1';
    }

    $aArrActRes = array('reset' => '1');
    $aArrAct = array();
    if ($iTransportId > 0) {
      $aArrActRes = array('f_transport_id' => $iTransportId, 'reset' => '1');
      $aArrAct = array('f_transport_id' => $iTransportId);
    }

    $aHeader = array(
        'header' => $aConfig['lang'][$this->sModule]['list_' . ($_POST['f_specialSearchOption'] ? '-1' : $iInternalStatus . ($iTransportId != '' ? '_' . $iTransportId : '')) . $_GET['action2']],
        'refresh' => true,
        'search' => true,
        'bigSearch' => true,
        'specialOption' => true,
        'specialOptionName' => $aConfig['lang'][$this->sModule]['search_in'],
        'count_checkboxes' => true,
        'action' => phpSelf($aArrAct),
        'refresh_link' => phpSelf($aArrActRes),
        'searchInfo' => 'W zapytaniu możesz użyć znaku * (gwiazdka), aby poszerzyć zakres wyników np. kontakt@profit*, *@profit24.pl lub *takt@profit*'

    );

    if (isset($_POST['f_user']) && $_POST['f_user'] != '') {
      $aHeader['header'] .= sprintf($aConfig['lang'][$this->sModule]['list_by_user'], $this->getUserDescr($_POST['f_user']));
    }

    $aAttribs = array(
        'width' => '100%',
        'border' => 0,
        'cellspacing' => 0,
        'cellpadding' => 0,
        'class' => 'viewHeaderTable',
    );
    $aRecordsHeader = array(
        array(
            'content' => '&nbsp;',
            'sortable' => false,
            'width' => '20'
        ),
        array(
            'db_field' => 'order_number',
            'content' => $aConfig['lang'][$this->sModule]['list_order_id'],
            'sortable' => true,
            'width' => '150'
        ),
        array(
            'content' => $aConfig['lang'][$this->sModule]['list_user'],
            'sortable' => false
        ),
        array(
            'db_field' => 'total_cost',
            'content' => $aConfig['lang'][$this->sModule]['list_total_cost'],
            'sortable' => true,
            'width' => '85'
        ),
        array(
            'db_field' => 'order_status',
            'content' => $aConfig['lang'][$this->sModule]['list_status'],
            'sortable' => true,
            'width' => '35'
        ),
        array(
            'db_field' => 'order_date',
            'content' => $aConfig['lang'][$this->sModule]['list_order_date'],
            'sortable' => true,
            'width' => '170'
        ),
        array(
            'db_field' => 'activation_date',
            'content' => $aConfig['lang'][$this->sModule]['list_activation_date'],
            'sortable' => true,
            'width' => '150'
        ),
        array(
            'db_field' => 'payment',
            'content' => $aConfig['lang'][$this->sModule]['list_payment'],
            'sortable' => true,
            'width' => '20'
        ),
        array(
            'content' => '&nbsp;',
            'sortable' => false,
            'width' => '25'
        ),
        array(
            'db_field' => 'payment_date',
            'content' => $aConfig['lang'][$this->sModule]['list_payment_date'],
            'sortable' => true,
            'width' => '80'
        ),
        array(
            'content' => $aConfig['lang']['common']['action'],
            'sortable' => false,
            'width' => '60'
        )
    );

    if (strlen($_POST['search']) > 3 && strpos($_POST['search'], ',')) {
      //wyszukiwanie po cenie brutto
      $fSearchValue = Common::formatPrice2($_POST['search']);
      $bSearchByPrice = true;
    }

    $aSearchOrdersId = array();
    $aSearchOrdersId[0] = 0;
    if ($_POST['f_specialSearchOption'] && isset($_POST['search']) && !empty($_POST['search'])) {

      $sSearchPhoneSQL = '';
      if ($this->_detectIsPhoneNumber($_POST['search'])) {
        $sSearchPhoneSQL = " OR REPLACE( REPLACE( A.phone,  '-',  '' ) ,  ' ',  '' ) = '" . $_POST['search'] . "' ";
      }

      //wyszukiwanie po isbn i tytule oraz dodatkowe filtry  
      $sSql = "SELECT DISTINCT B.order_id 
				FROM " . $aConfig['tabls']['prefix'] . "orders_items B
          "
              .
              ($this->iSendPackId > 0 ? ' JOIN orders_items_lists_items AS OILI ON OILI.orders_items_id = B.id AND OILI.orders_items_lists_id = ' . $this->iSendPackId . ' ' : '' )
              .
              "
          LEFT JOIN " . $aConfig['tabls']['prefix'] . "products AS A
            ON B.product_id=A.id" .
              (isset($_POST['f_category']) && $_POST['f_category'] != '' ?
                      " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_extra_categories C
							 	 ON B.product_id = C.product_id" : "") .
              (isset($_POST['f_author']) && $_POST['f_author'] != '' ?
                      " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_to_authors D
							 	 ON B.product_id = D.product_id
							 	 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_authors E
							 	 ON E.id = D.author_id" : "") .
              (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ?
                      " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers F
							 	 ON A.publisher_id = F.id" : "") .
              (isset($_POST['f_image']) ? " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_images I
							 	 ON I.product_id = B.product_id " : '') .
              (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ?
                      " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_to_series G
							 	 ON G.product_id = B.product_id" : '') . "
				WHERE B.isbn='" . $_POST['search'] . "' OR B.name LIKE '%" . $_POST['search'] . "%' " .
              (isset($_POST['f_page']) && $_POST['f_page'] != '' ? ' AND B.page_id = ' . $_POST['f_page'] : '') .
              (isset($_POST['f_category']) && $_POST['f_category'] != '' ? ' AND (A.page_id = ' . $_POST['f_category'] . ' OR C.page_id = ' . $_POST['f_category'] . ')' : '') .
              (isset($_POST['f_published']) && $_POST['f_published'] != '' ? ' AND A.published = \'' . $_POST['f_published'] . '\'' : '') .
              (isset($_POST['f_language']) && $_POST['f_language'] != '' ? ' AND A.language = ' . $_POST['f_language'] : '') .
              (isset($_POST['f_orig_language']) && $_POST['f_orig_language'] != '' ? ' AND A.original_language = ' . $_POST['f_orig_language'] : '') .
              (isset($_POST['f_binding']) && $_POST['f_binding'] != '' ? ' AND A.binding = ' . $_POST['f_binding'] : '') .
              (isset($_POST['f_year']) && $_POST['f_year'] != '' ? ' AND A.publication_year >= ' . $_POST['f_year'] : '') .
              (isset($_POST['f_yearto']) && $_POST['f_yearto'] != '' ? ' AND A.publication_year <= ' . $_POST['f_yearto'] : '') .
              //(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] != '' ? ' AND A.reviewed = \''.$_POST['f_reviewed'].'\'' : '').
              (isset($_POST['f_image']) && $_POST['f_image'] != '' ? ($_POST['f_image'] == '0' ? ' AND I.id IS NULL' : ' AND I.id IS NOT NULL') : '') .
              (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = \'' . $_POST['f_source'] . '\'' : '') .
              (isset($_POST['f_shipment']) && $_POST['f_shipment'] != '' ? ' AND A.shipment_time = \'' . $_POST['f_shipment'] . '\'' : '') .
              (isset($_POST['f_status']) && $_POST['f_status'] != '' ? ' AND A.prod_status = \'' . $_POST['f_status'] . '\'' : '') .
              (isset($_POST['f_author']) && $_POST['f_author'] != '' ? ' AND CONCAT(E.name,\' \',E.surname,\' \',E.name) LIKE \'%' . $_POST['f_author'] . '%\'' : '') .
              (isset($_POST['f_type']) && $_POST['f_type'] != '' ? ' AND A.packet = \'' . $_POST['f_type'] . '\'' : '') .
              (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ? ' AND F.name = \'' . $_POST['f_publisher'] . '\'' : '') .
              (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ? ' AND G.series_id = ' . $_POST['f_series'] . '' : '') .
              " GROUP BY B.order_id";
      $aOrdersId = & Common::GetAll($sSql);
      foreach ($aOrdersId as $iKey => $aItem) {
        $aSearchOrdersId[] = $aItem['order_id'];
      }
    }

    // jesli status = '-1' to znaczy ze chodzi o wybranie zamowien zawierajacych zapowiedzi w statusie albo czesciowe albo do realizacji
    $sFOrderStatus = NULL;
    $sPrevJoin = NULL;
    $sWhereSQL = NULL;
    if ($iInternalStatus === '-9999') {
      $sFOrderStatus = ' AND (internal_status = "3" OR internal_status = "2") AND preview = "1" AND B.status = "-1" ';
      $sPrevJoin = ' JOIN orders_items AS B
										 ON A.id = B.order_id';
      //unset($iInternalStatus);
    } elseif ($iInternalStatus === '3' || $iInternalStatus === '2') {
      $sFOrderStatus = NULL;
      $sPrevJoin = ' LEFT JOIN orders_items AS B
										 ON A.id = B.order_id AND
												B.preview = "1" AND B.status = "-1" ';
      $sWhereSQL = " AND B.id IS NULL";
    }

      if (isset($_POST['f_numer_faktury'])) {
          if (1 == (int)$_POST['f_numer_faktury']) {
              $sWhereSQL .= " AND A.invoice_id IS NULL";
          } elseif (2 == (int)$_POST['f_numer_faktury']){
              $sWhereSQL .= " AND A.invoice_id IS NOT NULL";
          }
      }

      if (isset($_POST['search']) && !$_POST['f_specialSearchOption'] && !$bSearchByPrice) {
          $querySearchString = $this->getQuerySearchString($_POST['search']);
      }

    // czesc SQL odpowiedzialna za filtr daty
    $sDateSql = $this->getDateSql();
    // pobranie liczby wszystkich zamowien
    $sSql = "SELECT COUNT(DISTINCT A.id)
						 FROM " . $aConfig['tabls']['prefix'] . "orders AS A
            " .
            ($this->iSendPackId > 0 ? ' JOIN orders_items_lists_items AS OILI ON OILI.orders_id = A.id AND OILI.orders_items_lists_id = ' . $this->iSendPackId . ' ' : '' )
            . $sPrevJoin . "
						 WHERE 1=1 " .
            ( $_POST['f_order_items_shipment_availablity'] === '0' ? ' 
                    AND NOT EXISTS 
                    (
                      SELECT product_id 
                      FROM orders_items AS OITMP
                      JOIN products as PTMP
                        ON PTMP.id = OITMP.product_id AND PTMP.shipment_time <> "0" 
                      WHERE OITMP.order_id = A.id
                    )
                    
                    ' : '') .
            (!empty($iInternalStatus) && $iInternalStatus > -1 && $iInternalStatus != 4 ? " AND internal_status = '" . $iInternalStatus . "'" : '') .
            ($iInternalStatus == 4 ? " AND ( internal_status = '" . $iInternalStatus . "' OR (internal_status = 5 AND `send_date`>NOW()))" : '') .
            ($iInternalStatus == 5 ? " AND (`send_date`='0000-00-00' OR (`send_date`<>'0000-00-00' AND `send_date`<NOW())) " : '') .
            (isset($_POST['search']) && !empty($_POST['search']) && $bSearchByPrice ? ' AND to_pay =' . $fSearchValue : '') .
            (isset($_POST['search']) && !empty($_POST['search']) && $_POST['f_specialSearchOption'] && !$bSearchByPrice ? ' AND (A.id IN(' . implode(',', $aSearchOrdersId) . ')' . $sSearchPhoneSQL . ')' : '') .
            (isset($_POST['search']) && !empty($_POST['search']) && !$_POST['f_specialSearchOption'] && !$bSearchByPrice ?
                    ( $querySearchString ) : '') .
            (isset($_POST['f_status']) && $_POST['f_status'] != '' ? " AND order_status = '" . $_POST['f_status'] . "'" : '') .
            (isset($_POST['f_internal_status']) && $_POST['f_internal_status'] != '' ? " AND internal_status = '" . $_POST['f_internal_status'] . "'" : '') .
            (isset($_POST['f_transport']) && $_POST['f_transport'] != '' ? " AND transport_id = '" . $_POST['f_transport'] . "'" : '') .
            (isset($_POST['f_website_id']) && $_POST['f_website_id'] != '' ? " AND website_id = '" . $_POST['f_website_id'] . "'" : '') .
            ($iMId == 1 ? "
                     AND (DATE_ADD(A.order_date, INTERVAL 25 MINUTE ) < NOW()
                          OR (A.remarks  <> '' AND A.remarks IS NOT NULL AND A.remarks_read_1 IS NULL)
                          )
                     " : '') .
            (isset($_POST['f_payment_status']) && $_POST['f_payment_status'] != '' ?
                    $_POST['f_payment_status'] == '1' ?
                            " AND payment_status = '1' " :
                            " AND payment_status = '0' AND IF (second_payment_enabled = '1', (second_payment_status = '0' OR balance <> '0.00'), 1=1) AND correction <> '1' "  // jeśli 1 met. płatności nie opłacona LUB druga jest nie opłacona i 
                    : '') .
            (isset($_POST['f_balance']) && $_POST['f_balance'] != '' ? ($_POST['f_balance'] == '1' ? ' AND balance > 0 ' : ' AND paid_amount > 0.00 AND balance < 0 ') : '') .
            (isset($_POST['f_shipment_date_expected']) && $_POST['f_shipment_date_expected'] != '' ? ' AND shipment_date_expected = "' . FormatDate($_POST['f_shipment_date_expected']) . '" ' : '') .
            (isset($_POST['f_payment']) && $_POST['f_payment'] != '' ? " AND payment_type = '" . $_POST['f_payment'] . "'" : '') .
            (isset($_POST['f_second_payment']) && $_POST['f_second_payment'] != '' ? " AND second_payment_type = '" . $_POST['f_second_payment'] . "'" : '') .
            (isset($_POST['f_user']) && $_POST['f_user'] != '' ? " AND user_id = '" . $_POST['f_user'] . "'" : '') .
            ($iTransportId != '' ? ' AND transport_id = \'' . $iTransportId . '\'' : '') .
            (isset($sFOrderStatus) ? $sFOrderStatus : '') .
            (isset($sWhereSQL) ? $sWhereSQL : '') .
            $sDateSql;

//      $sLimit = $this->getOptimizationLimitRows();
    $iRowCount = intval(Common::GetOne($sSql));
    //pobranie daty pierwszego zamówienia do wstawienia w pole filtru
//    $sSql = "SELECT order_date
//				FROM " . $aConfig['tabls']['prefix'] . "orders
//				WHERE 1 ORDER BY id ASC LIMIT 1";
//    $sDateOfFirstOrder = & Common::GetOne($sSql);

    if ($iRowCount == 0 && !isset($_GET['reset'])) {
      // resetowanie widoku
      resetViewState($this->sModule . ($iInternalStatus != '0' ? '_' . $iInternalStatus : '') . ($iTransportId != '' ? '_' . $iTransportId : ''));
      // ponowne okreslenie liczny rekordow
      $sSql = "SELECT COUNT(id)
							 FROM " . $aConfig['tabls']['prefix'] . "orders";
      $iRowCount = intval(Common::GetOne($sSql));
    }
    $pView = new View('orders' . ($iInternalStatus != '0' ? '_' . str_replace('-', '_', $iInternalStatus) : '') . ($iTransportId != '' ? '_' . $iTransportId : ''), $aHeader, $aAttribs);
    $pView->AddRecordsHeader($aRecordsHeader);

    //nowy filtr daty
    $pView->AddFilter('f_date_from', $aConfig['lang'][$this->sModule]['f_date_from'], array('clear' => 1), empty($_POST['f_date_from']) ? '' : $_POST['f_date_from'], 'date');
    $pView->AddFilter('f_date_to', $aConfig['lang'][$this->sModule]['f_date_to'], array('clear' => 1), $_POST['f_date_to'], 'date');

    $pView->AddFilter('f_status_4_from', $aConfig['lang'][$this->sModule]['f_status_4_from'], array('clear' => 1), empty($_POST['f_status_4_from']) ? '' : $_POST['f_status_4_from'], 'date');
    $pView->AddFilter('f_status_4_to', $aConfig['lang'][$this->sModule]['f_date_to'], array('clear' => 1), $_POST['f_status_4_to'], 'date');

    $pView->AddFilter('f_shipment_date_expected', _('Plan wysyłki'), array('clear' => 1), $_POST['f_shipment_date_expected'], 'date');

    if ($iInternalStatus == '0' || $iInternalStatus == '-1') {
      // FILTR STATUSU ZAMOWIENIA
      $pView->AddFilter('f_status', $aConfig['lang'][$this->sModule]['f_status'], $this->getFilterStatuses(), $_POST['f_status']);

      // FILTR STATUSU ZAMOWIENIA
      $pView->AddFilter('f_internal_status', $aConfig['lang'][$this->sModule]['f_internal_status'], $this->getFilterInternalStatuses(), $_POST['f_internal_status']);
    }

    // FILTR DOSTEPNOSCI SKLADOWYCH
    $pView->AddFilter('f_order_items_shipment_availablity', $aConfig['lang'][$this->sModule]['f_order_items_shipment_availablity'], $this->getFilterOrderItemsShipmentAvailablity(), $_POST['f_order_items_shipment_availablity']);

    $aListWebsites = $this->getWebsitesList();
    $aDefault = array(array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']));
    $aWebsites = array_merge($aDefault, $aListWebsites);
    $pView->AddFilter('f_website_id', $aConfig['lang'][$this->sModule]['f_website_id'], $aWebsites, $_POST['f_website_id']);

    $pView->AddFilter('a0', '', '</td></tr><tr><td>', '', 'html');
    // FILTR Transport
    $pView->AddFilter('f_transport', $aConfig['lang'][$this->sModule]['f_transport'], $this->getTransportMeans(), $_POST['f_transport']);

    // FILTR Płatności
    $pView->AddFilter('f_payment', $aConfig['lang'][$this->sModule]['f_payment'], $this->getFilterPaymentTypes(), $_POST['f_payment']);

    // FILTR Płatności
    $pView->AddFilter('f_second_payment', $aConfig['lang'][$this->sModule]['f_second_payment'], $this->getFilterPaymentTypes(), $_POST['f_second_payment']);

    //status opłacenia
    $pView->AddFilter('f_payment_status', $aConfig['lang'][$this->sModule]['f_payment_status'], $this->getFilterPaymentStatuses(), $_POST['f_payment_status']);

    //status opłacenia
    $pView->AddFilter('f_balance', $aConfig['lang'][$this->sModule]['f_balance'], $this->getFilterBalance(), $_POST['f_balance']);

    $pView->AddFilter('f_numer_faktury', _('Numer faktury'), $this->getNumerFaktury(), $_POST['f_numer_faktury']);

    $pView->AddFilter('a0', '', '</td></tr><tr><td>', '', 'html');

    //filtr dla daty zapłacenia
    $pView->AddFilter('f_payment_date_from', $aConfig['lang'][$this->sModule]['f_payment_date_from'], array('clear' => 1), (empty($_POST['f_payment_date_from']) ? '' : $_POST['f_payment_date_from']), 'date');
    $pView->AddFilter('f_payment_date_to', $aConfig['lang'][$this->sModule]['f_payment_date_to'], array('clear' => 1), $_POST['f_payment_date_to'], 'date');



    $pView->AddFilter('aa', '', '<input value="Filtruj" type="submit" style="margin-right:12px;"/></td></tr><tr' . ($_POST['f_specialSearchOption'] ? '' : ' style="display:none;"') . ' id="innerFilterLine1"><td>', '', 'html');
    // FILTRY nowe
    // kategoria Oferty produktowej
    $aCats = array();
    if ($_POST['f_category'] != '' && $_POST['f_category'] > 0) {
      $sSql = "SELECT name FROM " . $aConfig['tabls']['prefix'] . "menus_items WHERE id=" . $_POST['f_category'];
      $aCats = array(
          array(
              'label' => Common::GetOne($sSql),
              'value' => $_POST['f_category']
          )
      );
    }
    $pView->AddFilter('f_category', $aConfig['lang']['m_oferta_produktowa']['f_category'], $aCats, $_POST['f_category']);
    $pView->AddFilter('f_category_btn', '', '<input type="button" name="nvtree" value="' . $aConfig['lang']['m_oferta_produktowa']['f_category_btn'] . '" id="show_navitree" \>&nbsp;', '', 'html');

    $aTypes = array(
        array('value' => '', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_all']),
        array('value' => '1', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_packet']),
        array('value' => '0', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_regular_product'])
    );
    $pView->AddFilter('f_type', $aConfig['lang']['m_oferta_produktowa']['f_type'], $aTypes, $_POST['f_type']);
    $pView->AddFilter('f_year', $aConfig['lang']['m_oferta_produktowa']['f_year'], $oOfertaCommon->getYears(), $_POST['f_year']);
    $pView->AddFilter('f_yearto', $aConfig['lang']['m_oferta_produktowa']['f_yearto'], $oOfertaCommon->getYears(), $_POST['f_yearto']);

    $pView->AddFilter('a', '', '</td></tr><tr' . ($_POST['f_specialSearchOption'] ? '' : ' style="display:none;"') . ' id="innerFilterLine2"><td>', '', 'html');
    $pView->AddFilter('f_language', $aConfig['lang']['m_oferta_produktowa']['f_language'], $oOfertaCommon->getLanguages(), $_POST['f_language']);
    $pView->AddFilter('f_orig_language', $aConfig['lang']['m_oferta_produktowa']['f_orig_language'], $oOfertaCommon->getLanguages(), $_POST['f_orig_language']);
    $pView->AddFilter('f_binding', $aConfig['lang']['m_oferta_produktowa']['f_binding'], $oOfertaCommon->getBindings(), $_POST['f_binding']);
    $pView->AddFilter('b', '', '</td></tr><tr' . ($_POST['f_specialSearchOption'] ? '' : ' style="display:none;"') . ' id="innerFilterLine3"><td>', '', 'html');

    // stan publikacji
    $aPublished = array(
        array('value' => '', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_all']),
        array('value' => '1', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_published']),
        array('value' => '0', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_not_published'])
    );
    $pView->AddFilter('f_published', $aConfig['lang']['m_oferta_produktowa']['f_publ_status'], $aPublished, $_POST['f_published']);

    $aSources = array(
        array('value' => '', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_all']),
        array('value' => '0', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_internalj']),
        array('value' => '1', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_internaln']),
        array('value' => '2', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_azymut']),
        array('value' => '3', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_abe']),
        array('value' => '4', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_helion']),
        array('value' => '7', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_dictum']),
        array('value' => '8', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_siodemka']),
        array('value' => '9', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_olesiejuk']),
        array('value' => '5', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_source_ateneum']),
    );
    $pView->AddFilter('f_source', $aConfig['lang']['m_oferta_produktowa']['f_source'], $aSources, $_POST['f_source']);

    $pShipmentTime = new ShipmentTime();

    $aShipments = array(
      array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
        array('value' => '0', 'label' => $pShipmentTime->getShipmentTime(null, 0)),
      array('value' => '1', 'label' => $pShipmentTime->getShipmentTime(null, 1)),
      array('value' => '2', 'label' => $pShipmentTime->getShipmentTime(null, 2)),
      array('value' => '3', 'label' => $pShipmentTime->getShipmentTime(null, 3)),
      array('value' => '4', 'label' => $pShipmentTime->getShipmentTime(null, 4)),
      array('value' => '5', 'label' => $pShipmentTime->getShipmentTime(null, 5)),
      array('value' => '6', 'label' => $pShipmentTime->getShipmentTime(null, 6)),
      array('value' => '7', 'label' => $pShipmentTime->getShipmentTime(null, 7)),
      array('value' => '8', 'label' => $pShipmentTime->getShipmentTime(null, 8)),
      array('value' => '9', 'label' => $pShipmentTime->getShipmentTime(null, 9)),
      array('value' => '10', 'label' => $pShipmentTime->getShipmentTime(null, 10)),
    );
    $pView->AddFilter('f_shipment', $aConfig['lang']['m_oferta_produktowa']['f_shipment'], $aShipments, $_POST['f_shipment']);

    $aImagesF = array(
        array('value' => '', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_all']),
        array('value' => '0', 'label' => $aConfig['lang']['m_oferta_produktowa']['f_not_image'])
    );
    $pView->AddFilter('f_image', $aConfig['lang']['m_oferta_produktowa']['f_image'], $aImagesF, $_POST['f_image']);


    $pView->AddFilter('c', '', '</td></tr><tr' . ($_POST['f_specialSearchOption'] ? '' : ' style="display:none;"') . ' id="innerFilterLine4"><td>', '', 'html');


    $pView->AddFilter('f_author', $aConfig['lang']['m_oferta_produktowa']['f_author'], array(), $_POST['f_author'], 'text');
    $pView->AddFilter('f_publisher', $aConfig['lang']['m_oferta_produktowa']['f_publisher'], array(), $_POST['f_publisher'], 'text');

    $pView->AddFilter('f_series', $aConfig['lang']['m_oferta_produktowa']['f_series'], $oOfertaCommon->getPublisherSeriesList($_POST['f_publisher']), $_POST['f_series']);


    if ($iRowCount > 0) {
      // dodanie Pagera do widoku
      $iCurrentPage = $pView->AddPager($iRowCount);
      $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
      $iStartFrom = ($iCurrentPage - 1) * $iPerPage;

      // pobranie wszystkich zamowien
      $sSql = "SELECT DISTINCT A.id, website_id, transport_id, A.order_number, email AS user, A.name, surname, (A.value_brutto+transport_cost) AS total_cost, order_status, order_date, activation_date,  payment, dotpay_t_id, CONCAT('dotpay_', dotpay_t_status) dotpay_t_status,
											invoice_id, transport_number, invoice_ready, status_1_update,status_1_update_by,status_2_update,status_2_update_by,status_3_update,status_3_update_by,status_4_update,status_4_update_by,status_5_update,status_5_update_by,user_id,payment_status,payment_date,A.payment_type, paid_amount,to_pay, balance,
											second_payment_enabled,	A.second_payment,	second_payment_type, second_payment_id, second_payment_date, second_payment_status, correction, confirm_printout_speditor, internal_status, active,
                      GROUP_CONCAT(OD.order_number SEPARATOR ',') AS orders_connected_numbers, GROUP_CONCAT(OD.id SEPARATOR ',') AS orders_connected_ids,
                      (SELECT count(id) FROM orders_linked_transport WHERE main_order_id = A.id ) AS is_linked_transport,
                      print_ready_list, order_on_list_locked
							 FROM " . $aConfig['tabls']['prefix'] . "orders AS A
               LEFT JOIN orders_deleted AS OD
                ON OD.order_id = A.id
                " . $sPrevJoin . "
                " .
              ($this->iSendPackId > 0 ? ' JOIN orders_items_lists_items AS OILI ON OILI.orders_id = A.id AND OILI.orders_items_lists_id = ' . $this->iSendPackId . ' ' : '' )
              . "
						 WHERE  1=1 " .
              ( $_POST['f_order_items_shipment_availablity'] === '0' ? ' 
                    AND NOT EXISTS 
                    (
                      SELECT product_id 
                      FROM orders_items AS OITMP
                      JOIN products as PTMP
                        ON PTMP.id = OITMP.product_id AND PTMP.shipment_time <> "0" 
                      WHERE OITMP.order_id = A.id
                    )
                    
                    ' : '') .
              (!empty($iInternalStatus) && $iInternalStatus > -1 && $iInternalStatus != 4 ? " AND internal_status = '" . $iInternalStatus . "'" : '') .
              ($iInternalStatus == 4 ? " AND ( internal_status = '" . $iInternalStatus . "' OR (internal_status = 5 AND `send_date`>NOW()))" : '') .
              ($iInternalStatus == 5 ? " AND (`send_date`='0000-00-00' OR (`send_date`<>'0000-00-00' AND `send_date`<NOW())) " : '') .
              (isset($_POST['search']) && !empty($_POST['search']) && $bSearchByPrice ? ' AND to_pay =' . $fSearchValue : '') .
              (isset($_POST['search']) && !empty($_POST['search']) && $_POST['f_specialSearchOption'] && !$bSearchByPrice ? ' AND (A.id IN(' . implode(',', $aSearchOrdersId) . ') ' . $sSearchPhoneSQL . ')' : '') .
              (isset($_POST['search']) && !empty($_POST['search']) && !$_POST['f_specialSearchOption'] && !$bSearchByPrice ?
                  ( $querySearchString ) : '') .
              (isset($_POST['f_payment_status']) && $_POST['f_payment_status'] != '' ?
                      $_POST['f_payment_status'] == '1' ?
                              " AND payment_status = '1' " :
                              " AND payment_status = '0' AND IF (second_payment_enabled = '1', (second_payment_status = '0' OR balance <> '0.00'), 1=1) AND correction <> '1' "  // jeśli 1 met. płatności nie opłacona LUB druga jest nie opłacona i 
                      : '') .
              (isset($_POST['f_status']) && $_POST['f_status'] != '' ? " AND order_status = '" . $_POST['f_status'] . "'" : '') .
              (isset($_POST['f_internal_status']) && $_POST['f_internal_status'] != '' ? " AND internal_status = '" . $_POST['f_internal_status'] . "'" : '') .
              (isset($_POST['f_transport']) && $_POST['f_transport'] != '' ? " AND transport_id = '" . $_POST['f_transport'] . "'" : '') .
              (isset($_POST['f_website_id']) && $_POST['f_website_id'] != '' ? " AND website_id = '" . $_POST['f_website_id'] . "'" : '') .
              ($iMId == 1 ? "
                     AND (DATE_ADD(A.order_date, INTERVAL 25 MINUTE ) < NOW()
                          OR (A.remarks  <> '' AND A.remarks IS NOT NULL AND A.remarks_read_1 IS NULL)
                          )
                     " : '') .
              (isset($_POST['f_payment']) && $_POST['f_payment'] != '' ? " AND A.payment_type = '" . $_POST['f_payment'] . "'" : '') .
              (isset($_POST['f_second_payment']) && $_POST['f_second_payment'] != '' ? " AND A.second_payment_type = '" . $_POST['f_second_payment'] . "'" : '') .
              (isset($_POST['f_balance']) && $_POST['f_balance'] != '' ? ($_POST['f_balance'] == '1' ? ' AND A.balance > 0 ' : ' AND A.paid_amount > 0.00 AND A.balance < 0 ') : '') .
              (isset($_POST['f_shipment_date_expected']) && $_POST['f_shipment_date_expected'] != '' ? ' AND A.shipment_date_expected = "' . FormatDate($_POST['f_shipment_date_expected']) . '" ' : '') .
              (isset($_POST['f_user']) && $_POST['f_user'] != '' ? " AND user_id = '" . $_POST['f_user'] . "'" : '') .
              ($iTransportId != '' && $iTransportId != '' ? ' AND transport_id = \'' . $iTransportId . '\'' : '') .
              (isset($sFOrderStatus) ? $sFOrderStatus : '') .
              (isset($sWhereSQL) ? $sWhereSQL : '') .
              $sDateSql .
              " 
                 GROUP BY A.id
                 ORDER BY " . (isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ($iInternalStatus == '7' ? 'transport_file_date ASC' : 'A.id ' . ($_GET['ppid'] > 0 ? 'ASC' : 'DESC') . '')) .
              (isset($_GET['order']) && !empty($_GET['order']) ? ' ' . $_GET['order'] : '') .
              " 
                 
                 LIMIT " . $iStartFrom . ", " . $iPerPage;
      $aRecords = & Common::GetAll($sSql);
      $aRecordsSource = $aRecords; // tablica na potrzeby posumowania metod płatności poniżej
      $sBlockItems = '';
      foreach ($aRecords as $iKey => $aItem) {
        $aRecords[$iKey]['order_number'] = '<img src="gfx/' . $aConfig['invoice_website_symbol_' . $aItem['website_id']] . '.gif" /> ' . $aRecords[$iKey]['order_number'] .
                ($aItem['is_linked_transport'] > 0 ? ' <img src="gfx/msg_2.png" title="zamówienia które należy wysłać wraz z tym." /> (' . $aItem['is_linked_transport'] . ')' : '') .
                ($aItem['print_ready_list'] == '1' ? ' <img src="gfx/lock.png" title="zamówienie zostało pobrane na listę do zebrania" />' : '').
                ($aItem['order_on_list_locked'] == '1' ? ' <img src="gfx/lock_red.png" title="zamówienie zostało pobrane na listę do zebrania i nie może być edytowane" />' : '')
                ;

        if (!empty($aItem['invoice_id']) && $aItem['invoice_ready'] == '1') {
          $aRecords[$iKey]['order_number'] .= '<br /><span style="color: gray">FV: ' . $aItem['invoice_id'] . '</span>';
        }
        unset($aRecords[$iKey]['invoice_id']);
        unset($aRecords[$iKey]['invoice_ready']);
        if (!empty($aItem['transport_number'])) {
          $aRecords[$iKey]['order_number'] .= '<br /><span style="color: gray">LP: ' . $aItem['transport_number'] . '</span>';
        }
        unset($aRecords[$iKey]['transport_number']);

        $aInfoMessages = $this->getAdditionalInfoOrder($aItem['id']);

        $aRecords[$iKey]['order_number'] = '<a class="transport_numberTooltip" href="' . phpSelf(array('do' => 'details', 'ppid' => $iInternalStatus, 'id' => $aItem['id'])) . '#transport_numberTooltip_' . $aItem['id'] . '" rel="#transport_numberTooltip_' . $aItem['id'] . '">' . $aRecords[$iKey]['order_number'] . '</a>';
        $sClueTipHtml = '<div id="transport_numberTooltip_' . $aItem['id'] . '" style="display: none;">';

        // informacje o użytkowniku
        if ($aItem['user_id'] > 0) {
          $aUserComments = $this->getSimpleCommentsList($aItem['user_id']);
        }

        if (!empty($aUserComments)) {
          $sClueTipHtml .= '<b>Komentarze do użytkownika ' . $aItem['user'] . ':</b><br />';
          $sClueTipHtml .= '<span style="color: green;">';
          foreach ($aUserComments as $aUserComment) {
            $aUserComment['created'] = explode(' ', $aUserComment['created']);
            $sClueTipHtml .= $aUserComment['created'][0] . ' ' . substr($aUserComment['created'][1], 0, 5) . '&nbsp;&nbsp;&nbsp;<strong>' . $aUserComment['created_by'] . ':</strong> <span style="font-size:14px; font-weight:bold">' . nl2br($aUserComment['comment']) . '</span><br />';
          }
          $sClueTipHtml .= '</span>';
        }

        // informacje do zamówienia
        if (!empty($aInfoMessages)) {
          $sClueTipHtml .= '<br /><b>Informacje do zamówienia:</b><br />';
          foreach ($aInfoMessages as $aMessage) {
            $aMessage['created'] = explode(' ', $aMessage['created']);
            $sClueTipHtml .= $aMessage['created'][0] . ' ' . substr($aMessage['created'][1], 0, 5) . '&nbsp;&nbsp;&nbsp;<strong>' . $aMessage['created_by'] . ':</strong> <span style="font-size:14px; color:#f20000; font-weight:bold">' . $aMessage['value'] . '</span><br />';
          }
        } else {
          $sClueTipHtml .= 'brak informacji dodatkowych do zamówienia ';
        }

        $sClueTipHtml .= '</div>';
        $aRecords[$iKey]['order_number'] .= $sClueTipHtml;

        $aConnOrdersNumbers = explode(',', $aItem['orders_connected_numbers']);
        $aConnOrdersIds = explode(',', $aItem['orders_connected_ids']);
        if (!empty($aConnOrdersNumbers) && !empty($aConnOrdersIds)) {
          foreach ($aConnOrdersNumbers as $iCommOrdersKey => $sTMPNumber) {
            if ($sTMPNumber != '') {
              $aRecords[$iKey]['order_number'] .= '<div style="margin-top: 5px"><a href="' . phpSelf(array('action' => 'deleted', 'oid' => $aConnOrdersIds[$iCommOrdersKey])) . '">' . $sTMPNumber . ' &raquo; </a></div>';
            }
          }
        }

        if (empty($aItem['user'])) {
          $aRecords[$iKey]['user'] = '<span style="color: red">' . $aConfig['lang'][$this->sModule]['user_no_data'] . '</span>';
        } else {
          // uzytkownik - login + nazwa
          $aRecords[$iKey]['user'] = '<span style="color: gray">' . (!empty($aItem['user_id']) ? '<a class="userlinkTip" title="|Zamówienia użytkownika" href="' . phpSelf(array('f_user' => $aItem['user_id'])) . '">' : '') . $aItem['user'] . (!empty($aItem['user_id']) ? '</a>' : '') . '</span>';
        }
        unset($aRecords[$iKey]['user_id']);
        if (!empty($aItem['name']) || !empty($aItem['surname'])) {
          $aRecords[$iKey]['user'] .= '<br/>' . $aItem['name'] . "&nbsp;" . $aItem['surname'];
        }
        unset($aRecords[$iKey]['name']);
        unset($aRecords[$iKey]['surname']);
        // wartosc zamowienia netto
        $aRecords[$iKey]['total_cost'] = Common::formatPrice3($aItem['total_cost']) . ' <span style="color: gray">' . $aConfig['lang']['common']['currency'] . '</span>';

        switch ($aItem['order_status']) {
          case '-1':
            // nowe - zapo
            $sColor = '#00CCFF';
            break;
          case '0':
            if ($aItem['internal_status'] == '0') {
              // nowe
              $sColor = 'grey';
            } else {
              // nowe
              $sColor = 'blue';
            }
            break;

          case '1':
            // oplacone
            $sColor = 'green';
            break;

          case '2':
            // zrealizowane
            $sColor = 'black';
            break;

          case '3':
            // oplacone
            $sColor = 'orange';
            break;
          case '4':
            // oplacone
            $sColor = 'purple';
            break;
          case '5':
            // oplacone
            $sColor = 'red';
            break;
        }
        if ($aItem['order_status'] == 0) {
          $aRecords[$iKey]['disabled'][] = 'send';
        }
        // wysyłka
        if ($iInternalStatus == '5') {
          //|| $aItem['invoice_ready'] == '0'
          if ((empty($aItem['invoice_id']) || empty($aItem['transport_number']) || $aItem['order_status'] < 2)) {
            $sBlockItems .= "#delete\\\\[" . $aItem['id'] . "\\\\],";
          }
        }
        // odbiór osobisty
        if ($iInternalStatus == '6') {
          if (empty($aItem['invoice_id']) || $aItem['invoice_ready'] == '0' || $aItem['order_status'] < 2) {
            $sBlockItems .= "#delete\\\\[" . $aItem['id'] . "\\\\],";
          }
        }

        if ($aItem['order_status'] == '0' && $aItem['internal_status'] == '0') {
          $sDisabled = true;
        } else {
          $sDisabled = false;
        }
        if ($aItem['active'] == '1') {
          $aRecords[$iKey]['disabled'][] = 'activate';
        }
        // jeśli zamówienie jest zablokowane oraz status jest wcześniejszy niż zatwierdzone
        if ($aItem['print_ready_list'] != '1' || intval($aItem['order_status']) >= 3) {
          $aRecords[$iKey]['disabled'][] = 'back_ready_list';
        }

        unset($aRecords[$iKey]['internal_status']);
        $aRecords[$iKey]['order_status'] = '<span style="color: ' . $sColor . '">' . $aConfig['lang'][$this->sModule]['status_' . $aItem['order_status'] . ($sDisabled ? '_disabled' : '')] . '</span>';
        /* Chmurka z datami */
        $sClueTipHtml = '<div id="dateTooltip_' . $aItem['id'] . '" style="display:none;">';
        $sClueTipHtml .= '<span style="color: blue">' . $aConfig['lang'][$this->sModule]['status_0_update'] . ':&nbsp;' . $aItem['order_date'] . '</span><br />';
        if (!empty($aItem['status_1_update'])) {
          $sClueTipHtml .= '<span style="color: green">' . $aConfig['lang'][$this->sModule]['status_1_update'] . ':&nbsp;' . sprintf($aConfig['lang'][$this->sModule]['status_update'], $aItem['status_1_update'], $aItem['status_1_update_by']) . '</span><br />';
        }
        unset($aRecords[$iKey]['status_1_update']);
        unset($aRecords[$iKey]['status_1_update_by']);
        if (!empty($aItem['status_2_update'])) {
          $sClueTipHtml .= '<span style="color: black">' . $aConfig['lang'][$this->sModule]['status_2_update'] . ':&nbsp;' . sprintf($aConfig['lang'][$this->sModule]['status_update'], $aItem['status_2_update'], $aItem['status_2_update_by']) . '</span><br />';
        }
        unset($aRecords[$iKey]['status_2_update']);
        unset($aRecords[$iKey]['status_2_update_by']);
        if (!empty($aItem['status_3_update'])) {
          $sClueTipHtml .= '<span style="color: orange">' . $aConfig['lang'][$this->sModule]['status_3_update'] . ':&nbsp;' . sprintf($aConfig['lang'][$this->sModule]['status_update'], $aItem['status_3_update'], $aItem['status_3_update_by']) . '</span><br />';
        }
        unset($aRecords[$iKey]['status_3_update']);
        unset($aRecords[$iKey]['status_3_update_by']);
        if (!empty($aItem['status_4_update'])) {
          $sClueTipHtml .= '<span style="color: purple">' . $aConfig['lang'][$this->sModule]['status_4_update'] . ':&nbsp;' . sprintf($aConfig['lang'][$this->sModule]['status_update'], $aItem['status_4_update'], $aItem['status_4_update_by']) . '</span><br />';
        }
        unset($aRecords[$iKey]['status_4_update']);
        unset($aRecords[$iKey]['status_4_update_by']);
        if (!empty($aItem['status_5_update'])) {
          $sClueTipHtml .= '<span style="color: red">' . $aConfig['lang'][$this->sModule]['status_5_update'] . ':&nbsp;' . sprintf($aConfig['lang'][$this->sModule]['status_update'], $aItem['status_5_update'], $aItem['status_5_update_by']) . '</span><br />';
        }
        unset($aRecords[$iKey]['status_5_update']);
        unset($aRecords[$iKey]['status_5_update_by']);
        $sClueTipHtml .= '</div>';

        $aRecords[$iKey]['order_date'] = '<a class="dateTooltip" href="#dateTooltip_' . $aItem['id'] . '" rel="#dateTooltip_' . $aItem['id'] . '">' . str_replace('', '<br />', $aItem['order_date']) . '</a>' . $sClueTipHtml;


        $aRecords[$iKey]['dotpay_t_status'] = $this->_getPaymentHTML($aConfig['lang'][$this->sModule], $aItem['payment_status'], $aItem['payment_type'], $aItem['payment_date'], $aItem['dotpay_t_status']);
        $aRecords[$iKey]['dotpay_t_status'] .= $this->getBalanceHTML($aConfig['lang'][$this->sModule], $aItem['balance'], $aItem['payment_type'], $aItem['payment_status'], $aItem['payment_date']);

        $aRecords[$iKey]['payment'] = $this->_getPaymentChar(str_replace('(platnosci.pl)', '', $aRecords[$iKey]['payment_type']));
        $aRecords[$iKey]['payment'] = '<span style="font-size: 12px;">' . $this->getTransportChar($aItem['transport_id']) . ' - ' . $aRecords[$iKey]['payment'] . '</span>';


        //druga płatność
        if ($aRecords[$iKey]['second_payment_enabled'] == 1) {
          $aRecords[$iKey]['payment'].=' + ' . $this->_getPaymentChar(str_replace('(platnosci.pl)', '', $aRecords[$iKey]['second_payment_type']));

          $aRecords[$iKey]['dotpay_t_status'] .= '<br />' . $this->_getPaymentHTML($aConfig['lang'][$this->sModule], $aItem['second_payment_status'], $aItem['second_payment_type'], $aItem['second_payment_date'], $aItem['dotpay_t_status']);
        }
        if ($aItem['correction'] == '1') {
          $aRecords[$iKey]['dotpay_t_status'] .= '<br /><span style="color: red; font-weight: bold;">' . $aConfig['lang'][$this->sModule]['correction'] . '</span>';
        }

        if (empty($aItem['transport_number'])) {
          $aRecords[$iKey]['disabled'][] = 'get_package_status';
        }

        unset($aRecords[$iKey]['order_on_list_locked']);
        unset($aRecords[$iKey]['print_ready_list']);
        unset($aRecords[$iKey]['active']);
        unset($aRecords[$iKey]['confirm_printout_speditor']);
        unset($aRecords[$iKey]['correction']);
        unset($aRecords[$iKey]['payment_status']);
        unset($aRecords[$iKey]['payment_date']);
        unset($aRecords[$iKey]['payment_type']);
        unset($aRecords[$iKey]['paid_amount']);
        unset($aRecords[$iKey]['to_pay']);
        unset($aRecords[$iKey]['balance']);
        unset($aRecords[$iKey]['second_payment_enabled']);
        unset($aRecords[$iKey]['second_payment']);
        unset($aRecords[$iKey]['second_payment_type']);
        unset($aRecords[$iKey]['second_payment_id']);
        unset($aRecords[$iKey]['second_payment_status']);
        unset($aRecords[$iKey]['second_payment_date']);
        unset($aRecords[$iKey]['website_id']);
        unset($aRecords[$iKey]['orders_connected_numbers']);
        unset($aRecords[$iKey]['orders_connected_ids']);
        unset($aRecords[$iKey]['is_linked_transport']);

        if ($aItem['order_status'] != '0') {
          $aRecords[$iKey]['disabled'][] = 'auto-source';
        }

        if (!($this->checkNeedReset($aItem) && ($_SESSION['user']['priv_order_status'] == '1' || $_SESSION['user']['type'] == '1'))) {
          $aRecords[$iKey]['disabled'][] = 'reset-balance';
        }

        if ($aItem['order_status'] == '5' || $aItem['order_status'] == '3' || $aItem['order_status'] == '4') {
          $aRecords[$iKey]['disabled'][] = 'publish_all';
        }

      }
      // ustawienia dla poszczegolnych kolumn rekordu
      $aColSettings = array(
          'id' => array(
              'show' => false
          ),
          'transport_id' => array(
              'show' => false
          ),
          'order_number' => array(
              'link' => phpSelf(array('do' => 'details', 'ppid' => $iInternalStatus, 'id' => '{id}'))
          ),
          'user' => array(
              'link' => phpSelf(array('do' => 'details', 'ppid' => $iInternalStatus, 'id' => '{id}'))
          ),
          'action' => array(
              'actions' => array('auto-source', 'back_ready_list', 'alert', 'send', 'streamsoft-details', 'details', 'bug', 'get_package_status', 'changes', 'reset-balance', 'publish_all'),
              'lang' => array('publish_all' => 'Odnów rezerwacje'),
              'params' => array(
                  'auto-source' => array('id' => '{id}'),
                  'back_ready_list' => array('ppid' => $iInternalStatus, 'id' => '{id}'),
                  'activate' => array('ppid' => $iInternalStatus, 'id' => '{id}'),
                  'alert' => array('action' => 'alert', 'ppid' => $iInternalStatus, 'pid' => '{id}'),
                  'send' => array('ppid' => $iInternalStatus, 'id' => '{id}'),
                  'streamsoft-details' => array('id' => '{id}'),
                  'details' => array('ppid' => $iInternalStatus, 'id' => '{id}'),
                  'bug' => array('ppid' => $iInternalStatus, 'id' => '{id}'),
                  'delete' => array('ppid' => $iInternalStatus, 'id' => '{id}'),
                  'get_package_status' => array('ppid' => $iInternalStatus, 'id' => '{id}'),
                  'changes' => array('ppid' => $iInternalStatus, 'id' => '{id}'),
                  'reset-balance' => array('id' => '{id}'),
                  'publish_all' => array(
                      'ppid' => $iInternalStatus,
                      'id' => '{id}',
                      'do' => 'renew_reservation'
                  ),
              ),
              'show' => false,
              'icon' => array(
                  'reset-balance' => 'balance',
                  'back_ready_list' => 'back',
                  'get_package_status' => 'package_closed',
                  'streamsoft-details' => 'move_to'
              )
          )
      );
      // jeśli w zakladce przegląd to przycisk aktywacji zamówienia
      if ($iInternalStatus == 0) {
        array_push($aColSettings['action']['actions'], 'activate');
      }

      // dodanie rekordow do widoku
      $pView->AddRecords($aRecords, $aColSettings);
    }
    // dodanie stopki do widoku
    if ($iInternalStatus < 2)
      $aRecordsFooter = array(
          array('check_all', 'print_all', 'publish_all', 'review_all', 'payment_sum'),
          array('add')
      );
    else {
      $aRecordsFooter = array(
          array('check_all', 'print_all', 'publish_all', 'review_all', 'payment_sum')
      );
    }
    if (!empty($sBlockItems)) {
      $sBlockItems = substr($sBlockItems, 0, -1);
    }

    $sJS .='
				<script type="text/javascript">
	// <![CDATA[
			$(document).ready(function(){
				$("#search").focus().select();

				$("a.userlinkTip").cluetip({splitTitle: \'|\', showTitle: false, clickThrough: true});
				//$("#orders a.link").attr("title","|Szczegóły zamówienia");
				$("a.transport_numberTooltip").cluetip({local:true, showTitle: false, clickThrough: true});
				
				$("#orders a.link").cluetip({splitTitle: \'|\', showTitle: false, clickThrough: true});
				$("a.dateTooltip").cluetip({local:true, showTitle: false});
				' . (!empty($sBlockItems) ? '$("' . $sBlockItems . '").attr("disabled","disabled");' : '') . '
			});
			// ]]>
</script>
					<script>
					
						$(document).ready(function(){
							$("#show_navitree").click(function() {
								window.open("ajax/NaviPopup.php",\'\',\'menubar=0,location=0,scrollbars=1,toolbar=0,width=500,height=300\');
							});
							$("a.stockTooltip").cluetip({width: "365px", local:true, showTitle: false});
							$("#f_publisher").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetPublishers.php"
              });
							$("#f_publisher").bind("blur", function(e){
						   	 var elSelSeries = document.getElementById("f_series");
						   	 if(e.target.value != "") {  
				     	    $.get("ajax/GetSeries.php", { publisher: e.target.value },
									  function(data){
									    var brokenstring=data.split(";");
								    elSelSeries.length = 0;
									    elSelSeries.options[elSelSeries.length] =  new Option("' . $aConfig['lang']['common']['choose'] . '","");
									    for (x in brokenstring){
												if(brokenstring[x]){
													var item=brokenstring[x].split("|");
						  						elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
					  						}
					  				}
									 });
						 			} else { elSelSeries.length = 0; elSelSeries.options[elSelSeries.length] =  new Option("' . $aConfig['lang']['common']['choose'] . '",""); }
	    					});
						});
					</script>';

    // utworzenie tablicy metod płatności
    $aPaymentMethods = array();
    if (isset($aRecordsSource) && !empty($aRecordsSource) && is_array($aRecordsSource)) {
        foreach ($aRecordsSource as $aRecord) {
            // karta kredydowa tu jest brana jako płatność elektroniczna, Ustalono z Marcinem Ch. 24.08.2011
            if (stristr($aRecord['payment'], 'karta kredytowa') || stristr($aRecord['payment'], 'przelew elektroniczny')) {
              $aRecord['payment'] = 'płatność elektroniczna';
            }
            if ($aRecord['second_payment'] != '') {
              if (stristr($aRecord['second_payment'], 'karta kredytowa') || stristr($aRecord['second_payment'], 'przelew elektroniczny')) {
                  $aRecord['second_payment'] = 'płatność elektroniczna';
              }
            }

            $sMethod = $aRecord['payment'];
            if ($aRecord['second_payment'] != '' && $aRecord['second_payment'] != $aRecord['payment']) {
              $sMethod = $aRecord['payment'] . ' lub ' . $aRecord['second_payment'];
            }
            $aPaymentMethods[$sMethod]['total_cost'] += $aRecord['total_cost'];
            $aPaymentMethods[$sMethod]['paid_amount'] += $aRecord['paid_amount'];
        }
    }
    $sHTMLSumPayment = '';
    // HTML z kodem do metod płatności
    if (!empty($aPaymentMethods)) {
      $sHTMLSumPayment = '<div id="payment_sum_hidden" style="display: none">
													<table><tbody>';
      $sHTMLSumPayment .= '<tr>
														<td style="border-left: 1px solid black;">Metoda płatności</td><td style="border-left: 1px solid black;">Suma wszystkich zamówień</td><td style="border-left: 1px solid black;">Suma opłaconych</td>
													 </tr>';
      $fSumSumAll = 0.00;
      $fSumSumPaid = 0.00;
      foreach ($aPaymentMethods as $sMehtod => $aMethodData) {
        $sSumAll = Common::formatPrice($aMethodData['total_cost']) . '&nbsp;' . $aConfig['lang']['common']['currency'];
        $fSumSumAll += $aMethodData['total_cost'];
        $sSumPaid = Common::formatPrice($aMethodData['paid_amount']) . '&nbsp;' . $aConfig['lang']['common']['currency'];
        $fSumSumPaid += $aMethodData['paid_amount'];
        $sHTMLSumPayment .= '<tr>';
        $sHTMLSumPayment .= '<td style="border-left: 1px solid black;">' . $sMehtod . '</td><td style="border-left: 1px solid black;"> &nbsp;  &nbsp; ' . $sSumAll . '</td><td style="border-left: 1px solid black;"> &nbsp;  &nbsp; ' . $sSumPaid . '</td>';
        $sHTMLSumPayment .= '</tr>';
      }
      $sHTMLSumPayment .= '<tr>';
      $sHTMLSumPayment .= '<td style="border-left: 1px solid black; font-weight: bold;"> Podsumowanie </td>
                           <td style="border-left: 1px solid black; font-weight: bold;"> &nbsp;  &nbsp; ' . $fSumSumAll . '</td>
                           <td style="border-left: 1px solid black; font-weight: bold;"> &nbsp;  &nbsp; ' . $fSumSumPaid . '</td>';
      $sHTMLSumPayment .= '</tr>';

      $sHTMLSumPayment .= '</tbody></table></div>';
    }

    $pView->AddRecordsFooter($aRecordsFooter);

    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') .
            $sJS . $pView->Show() . $sHTMLSumPayment);
  }
// end of Show() function

  /**
   * Metoda generuje FV zakupu
   * 
   * @return FV
   */
  public function getInvoice($iId) {
    global $aConfig, $pDbMgr;

    $aOrder = $this->getOrderData($iId);
    if ($aOrder['order_status'] == '3' || $aOrder['order_status'] == '4') {
      $bPersonalReception = $this->checkPersonalReception($iId);
      if ($aOrder['invoice_ready'] == '0' || empty($aOrder['invoice_date'])) {
        if ($this->proceedSetInvoice($iId, $aOrder, $bPersonalReception) === FALSE) {
          return false;
        }
      }

      if (empty($aOrder['invoice_id'])) {
        $sInvoiceNumber = $this->getInvoiceNumber($aOrder['bookstore'], $aOrder['order_number'], $aOrder['website_id']);
        if ($sInvoiceNumber > 0) {
          $aValues = array(
              'invoice_id' => $sInvoiceNumber
          );
          $pDbMgr->Update('profit24', 'orders', $aValues, ' id = ' . $iId . ' LIMIT 1');
        } else {
          return false;
        }
      }
      if ($aOrder['second_invoice'] == '1' && empty($aOrder['invoice2_id'])) {
        $sInvoiceNumber2 = $this->getInvoiceNumber($aOrder['bookstore'], $aOrder['order_number'], $aOrder['website_id']);
        $aValues = array(
            'invoice2_id' => $sInvoiceNumber2
        );
        $pDbMgr->Update('profit24', 'orders', $aValues, ' id = ' . $iId . ' LIMIT 1');
      }
    }

    include_once('lib/Invoice.class.php');
    $oInvoice = new Invoice();
    $oInvoice->sTemplatePath = $aConfig['common']['client_base_path'] . 'omniaCMS/smarty/templates/';
    $oInvoice->getInvoicePDF($iId, $this->getOrderData($iId));
    exit();
  }
// end of getInvoice() method


    /**
     * Metoda generuje fakturę proforma zakupu
     *
     * @return FV
     */
    public function getInvoiceProforma($iId) {
        global $aConfig, $pDbMgr;

        include_once('lib/Invoice.class.php');
        $oInvoice = new Invoice();
        $oInvoice->sTemplatePath = $aConfig['common']['client_base_path'] . 'omniaCMS/smarty/templates/';
        $oInvoice->getInvoiceProforma($iId, $this->getOrderData($iId));
        exit();
    }

    /**
     * Flitr do sortowania po liscie zamowien (numer faktury uzupelniony / pusty)
     *
     * @return array
     */
    public function  getNumerFaktury(){
        $aValues = array(
            array(
                'label' => _('Wybierz'),
                'value' => ''
            ),
            array(
                'label' => _('Pusty'),
                'value' => '1'
            ),
            array(
                'label' => _('Uzupełniony'),
                'value' => '2'
            )
        );
        return $aValues;
    }

  /**
   * Metoda pobiera filtr nadpłaty i niedopłaty
   * 
   * @global array $aConfig
   * @return array
   */
  private function getFilterBalance() {

    $aValues = array(
        array(
            'label' => _('Wybierz'),
            'value' => ''
        ),
        array(
            'label' => _('Nadpłata'),
            'value' => '1'
        ),
        array(
            'label' => _('Niedopłata'),
            'value' => '2'
        )
    );
    return $aValues;
  }
// end of getFilterBalance() method

  /**
   * Metoda zwraca kod HTML dla płatności
   * 
   * @global array $aConfig
   * @param array $aLang
   * @param int $iPStatus
   * @param type $sPType
   * @param string $sPDate
   * @param float $fBalance
   * @param int $iDotpayTStatus
   * @return string HTML
   */
  private function _getPaymentHTML($aLang, $iPStatus, $sPType, $sPDate, $iDotpayTStatus) {
    $sStrHTML = '';

    if ($sPType == 'dotpay') {
      $sStrHTML = $aLang[$iDotpayTStatus];
    } elseif ($sPType == 'platnoscipl' || $sPType == 'payu_bank_transfer' || $sPType == 'card') {
      $sStrHTML = $this->_getPaymentDate($sPDate, 'green');
      if ($iPStatus == '2') {
        $sStrHTML = '<span style="color: red">' . $aLang['platnosci_failed'] . '</span>';
      }
    } elseif ($sPType == 'bank_transfer' && $iPStatus == '1') {
      $sStrHTML = $this->_getPaymentDate($sPDate, 'blue');
    } elseif ($sPType == 'bank_14days' && $iPStatus == '1') {
      $sStrHTML = $this->_getPaymentDate($sPDate, 'blue');
    } elseif ($sPType == 'postal_fee' && $iPStatus == '1' && $sPDate != '' && $sPDate != '0000-00-00 00:00:00') {
      $sStrHTML = $this->_getPaymentDate($sPDate, 'purple');
    }

    return $sStrHTML;
  }
// end of _getPaymentColors() method

  /**
   * Metoda pobiera kod html nadpłat i niedopłat
   * 
   * @global array $aConfig
   * @param bool $bSecondPayment
   * @param array $aLang
   * @param string $sBalance
   * @param string $sPType
   * @param int $iPStatus
   * @param string $sPDate
   * @return string
   */
  private function getBalanceHTML($aLang, $fBalance, $sPType, $iPStatus, $sPDate) {
    global $aConfig;
    $sBalance = Common::formatPrice($fBalance);
    $sStrHTML = '';

    if (($sPType == 'platnoscipl' || $sPType == 'payu_bank_transfer' || $sPType == 'card') || ($sPType == 'bank_14days' && $iPStatus == '1') || ($sPType == 'postal_fee' && $iPStatus == '1' && $sPDate != '' && $sPDate != '0000-00-00 00:00:00') || ($sPType == 'bank_transfer' && $iPStatus == '1')
    ) {
      if (Common::formatPrice2($sBalance) < 0) {
        // niedoplata
        $sStrHTML .= '<br /><span style="color: red">' . $aLang['underpayment'] . ': ' . Common::formatPrice($sBalance) . '&nbsp;' . $aConfig['lang']['common']['currency'] . '</span>';
      } elseif (Common::formatPrice2($sBalance) > 0) {
        // nadplata
        $sStrHTML .= '<br /><span style="color: red">' . $aLang['overpayment'] . ': ' . Common::formatPrice($sBalance) . '&nbsp;' . $aConfig['lang']['common']['currency'] . '</span>';
      }
    }
    return $sStrHTML;
  }
// end of getBalanceHTML() method

  /**
   * Metoda zwraca kod html dla daty płatności
   * 
   * @param string $sPDate
   * @param string $sColor
   * @return string HTML
   */
  private function _getPaymentDate($sPDate, $sColor) {

    return '<span style="color: ' . $sColor . ';">' . $sPDate . '</span>';
  }
// end of _getPaymentDate() method

  /**
   * Metoda wykrywa, czy przekazany string może być numerem telefonu
   * 
   * @param string $sStr
   * @return boolean
   */
  private function _detectIsPhoneNumber($sStr) {
    if (is_numeric($sStr) && strlen($sStr) == 9) {
      return true;
    }
    return false;
  }
// end of _detectIsPhoneNumber() method

  /**
   * Metoda pobiera metodę płatności 
   *  poprzez pierwsze litery z nazwy metody płatności z podniesioną literą
   * 
   * @param string $sPaymentType - metoda płatności
   * @return string
   */
  private function _getPaymentChar($sPaymentType) {

    switch ($sPaymentType) {
      case 'postal_fee':
        return 'PPO';
        break;
      case 'platnoscipl':
        return 'PE';
        break;
      case 'card':
        return 'KK';
        break;
      case 'bank_transfer':
        return 'PP'; // przelew przedpłata
        break;
      case 'bank_14days':
        return 'P14';
      case 'payu_bank_transfer':
        return 'PPPE';
      default:
        return ' --- ';
        break;
    }
  }
// end of _getPaymentChar method

  /**
   * Metoda zwraca symbol transportu
   * O - OPEK
   * OO - Odbiór osobisty
   * P - Paczkomaty
   *
   * @param	integer	$iTransportID	- ID metody transportu
   * @return	string 
   */
  private function getTransportChar($iTransportID) {
    switch ($iTransportID) {
      case '1': return '<span style="color: #4D148C; font-weight: bold;">FedEx</span>';
        break;
      case '2': return '<span style="color: grey; font-weight: bold;">Osobisty</span>';
        break;
      case '3': return '<span style="color: blue; font-weight: bold;">Paczko</span>';
        break;
      case '4': return '<span style="color: #e6392b; font-weight: bold;">Poczta</span>';
        break;
      case '5': return '<span style="color: green; font-weight: bold;">RUCH</span>';
        break;
      case '6': return '<span style="color: #e6392b; font-weight: bold;">Orlen</span>';
        break;
      case '7': return '<span style="color: #e6392b; font-weight: bold;">TBA</span>';
        break;
      case '8': return '<span style="color: #e6392b; font-weight: bold;">Poczta adr</span>';
            break;
      default: return '';
        break;
    }
  }
//end of getTransportChar() method

  /**
   * Dodaje produkt do zamówienia
   * @param $pSmarty
   * @param $iOrderId - id zamówienia
   * @param $iId - id produktu do dodania
   * @return unknown_type
   */
  private function addProduct(&$pSmarty, $iPId, $iOrderId, $iId) {
    global $aConfig, $pDbMgr;
    $aOrder = $this->getOrderData($iOrderId);

    $iOId = $iOrderId;
    include_once("OrderItemRecount.class.php");
    $oOrderItemRecount = new OrderItemRecount();


    $bIsErr = false;


    Common::BeginTransaction();
    $sSql = "SELECT A.*, B.name AS publisher_name, B.discount_limit
						FROM " . $aConfig['tabls']['prefix'] . "products A
						LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers B
						ON B.id=A.publisher_id
						WHERE A.id = " . $iId;
    $aProduct = $pDbMgr->GetRow($aOrder['bookstore'], $sSql);
    if (!empty($aOrder) && !empty($aProduct)) {

      $aTarrif = $this->getTarrifM($aOrder['bookstore'], $iId);
      $aTarrif['packet'] = ($aProduct['packet'] == '1');
      // przeliczenie i formatowanie cen
      $sPromoText = '';
      $fPromoPriceBrutto = Common::formatPrice2($this->getPromoPrice($aTarrif, $sPromoText, $aProduct['price_brutto'], $this->getSourceDiscountLimit($iId), $aProduct['discount_limit'], $iId, $iOrderId, false, false, $aOrder['bookstore']));
      $fPriceNetto = Common::formatPrice2($aProduct['price_brutto'] / (1 + $aProduct['vat'] / 100));
      $fPriceBrutto = Common::formatPrice2($aProduct['price_brutto']);
      $fPromoPriceNetto = Common::formatPrice2($fPromoPriceBrutto / (1 + $aProduct['vat'] / 100));
      $fDiscountCurrency = Common::formatPrice2($aProduct['price_brutto'] * (Common::formatPrice2($aTarrif['discount']) / 100));

      if ($fDiscountCurrency > 0) {
        $fValueNetto = $fPromoPriceNetto;
        $fValueBrutto = $fPromoPriceBrutto;
      } else {
        $fValueNetto = $fPriceNetto;
        $fValueBrutto = $fPriceBrutto;
      }

      $aValues = array(
          'order_id' => $iOrderId,
          'product_id' => $iId,
          'name' => $aProduct['name'],
          'price_netto' => $fPriceNetto,
          'price_brutto' => $fPriceBrutto,
          'vat' => $aProduct['vat'],
          'vat_currency' => Common::formatPrice2($fPriceBrutto - $fPriceNetto),
          'total_vat_currency' => Common::formatPrice2($fValueBrutto - $fValueNetto),
          'value_netto' => $fValueNetto,
          'value_brutto' => $fValueBrutto,
          'quantity' => 1,
          'discount' => Common::formatPrice2($aTarrif['discount']),
          'discount_currency' => $fDiscountCurrency,
          'total_discount_currency' => Common::formatPrice2($fDiscountCurrency * 1),
          'promo_price_netto' => $fPromoPriceNetto,
          'promo_price_brutto' => $fPromoPriceBrutto,
          'source' => '0',
          'packet' => $aProduct['packet'],
          'isbn' => (!empty($aProduct['isbn']) ? $aProduct['isbn'] : ''),
          'publication_year' => (!empty($aProduct['publication_year']) ? $aProduct['publication_year'] : ''),
          'edition' => (!empty($aProduct['edition']) ? $aProduct['edition'] : 'NULL'),
          'shipment_time' => $aProduct['shipment_time'],
          'publisher' => (!empty($aProduct['publisher_name']) ? $aProduct['publisher_name'] : ''),
          'authors' => $this->getAuthorsStrM($aOrder['bookstore'], $iId),
          'preview' => ($aProduct['prod_status'] == '3' ? '1' : '0'),
          'shipment_date' => ($aProduct['prod_status'] == '3' ? $aProduct['shipment_date'] : 'NULL'),
          'weight' => $aProduct['weight'],
          'added_by_employee' => 1
      );

      if ($aOrder['bookstore'] != 'profit24') {
        $iNewId = $this->getProductIdProfit24($iId, $aOrder['bookstore']);
        if ($iNewId > 0) {
          $aValues['product_id'] = $iNewId;
        } else {
          return false;
        }
      }

      if (($iItemId = Common::Insert($aConfig['tabls']['prefix'] . "orders_items", $aValues)) === false) {
        $bIsErr = true;
      }
      if (!$bIsErr && $aProduct['packet'] == '1') {
        $fTotalPacketWeight = 0;
        // pobranie skladowych pakietu
        $sSql = "SELECT * FROM " . $aConfig['tabls']['prefix'] . "products_packets_items WHERE packet_id = " . $iId;
        // przetworzenie skladowych pakietu - dodanie do bazy
        $aPckItems = & $pDbMgr->GetAll($aOrder['bookstore'], $sSql);
        foreach ($aPckItems as $aValue) {
          $sSql = "SELECT A.price_netto, A.price_brutto, A.name, A.isbn, A.publication_year, B.name AS publisher_name, A.edition, A.shipment_time, A.weight, A.source
										FROM " . $aConfig['tabls']['prefix'] . "products A
										JOIN " . $aConfig['tabls']['prefix'] . "products_publishers B
											ON B.id=A.publisher_id
										WHERE A.id = " . $aValue['product_id'];

          $aItemInfo = & $pDbMgr->GetRow($aOrder['bookstore'], $sSql);

          $fPackValueNetto = ($aValue['promo_price_netto'] * 1);
          $fPackValueBrutto = ($aValue['promo_price_brutto'] * 1);
          $aValuesPckItems = array(
              'order_id' => $iOrderId,
              'product_id' => $aValue['product_id'],
              'name' => $aItemInfo['name'],
              'price_netto' => $aItemInfo['price_netto'],
              'price_brutto' => $aItemInfo['price_brutto'],
              'vat' => $aValue['vat'],
              'vat_currency' => Common::formatPrice2($aItemInfo['price_brutto'] - $aItemInfo['price_netto']),
              'total_vat_currency' => Common::formatPrice2($fPackValueBrutto - $fPackValueNetto),
              'value_netto' => $fPackValueNetto,
              'value_brutto' => $fPackValueBrutto,
              'quantity' => 1,
              'promo_price_brutto' => $aValue['promo_price_brutto'],
              'promo_price_netto' => $aValue['promo_price_netto'],
              'discount' => $aValue['discount'] + $aProduct['additional_discount'],
              'discount_currency' => $aValue['discount_currency'],
              'total_discount_currency' => Common::formatPrice2($aValue['discount_currency'] * 1),
              'source' => $aItemInfo['source'],
              'item_type' => 'P',
              'parent_id' => $iItemId,
              'isbn' => (!empty($aItemInfo['isbn']) ? $aItemInfo['isbn'] : 'NULL'),
              'publication_year' => (!empty($aItemInfo['publication_year']) ? $aItemInfo['publication_year'] : 'NULL'),
              'edition' => (!empty($aItemInfo['edition']) ? $aItemInfo['edition'] : 'NULL'),
              'shipment_time' => $aItemInfo['shipment_time'],
              'publisher' => (!empty($aItemInfo['publisher_name']) ? $aItemInfo['publisher_name'] : 'NULL'),
              'authors' => $this->getAuthorsStrM($aOrder['bookstore'], $aValue['product_id']),
              'weight' => $aItemInfo['weight']
          );
          if (($iPacketItemId = Common::Insert($aConfig['tabls']['prefix'] . "orders_items", $aValuesPckItems)) === false) {
            $bIsErr = true;
          }
          $fTotalPacketWeight += $aItemInfo['weight'];
          // dodanie informacji o zalacznikach produktu pakietu

          if (!$bIsErr && $this->checkAttachmentsM($aOrder['bookstore'], $aValue['product_id'])) {
            $sSql = "SELECT A.*, B.name FROM " . $aConfig['tabls']['prefix'] . "products_attachments A
													JOIN " . $aConfig['tabls']['prefix'] . "products_attachment_types B
													ON B.id = A.attachment_type
													WHERE A.product_id = " . $aValue['product_id'];
            $aPacketAttachments = & $pDbMgr->GetAll($aOrder['bookstore'], $sSql);

            foreach ($aPacketAttachments as $aPAttVal) {

              $aPAttVal['product_discount'] = ($aValue['discount']);
              $aPAttVal['product_quantity'] = 1; // dodawany jest zawsze jeden element
              $aPAttVal['product_shipment_time'] = $aItemInfo['shipment_time'];

              $aValuesAttPacket = $oOrderItemRecount->getAttrValues($aPAttVal, $iPacketItemId, $iOId);

              if (Common::Insert($aConfig['tabls']['prefix'] . "orders_items", $aValuesAttPacket) === false) {
                Common::RollbackTransaction();
                return false;
              }
            }
            $aValues = $oOrderItemRecount->getParentAttData($aValuesPckItems, $aPacketAttachments, $aValuesPckItems['promo_price_brutto']);

            if (Common::Update($aConfig['tabls']['prefix'] . "orders_items", $aValues, "id = " . $iPacketItemId . " AND order_id = " . $iOId . " AND product_id = " . $aValue['product_id']) === false) {
              Common::RollbackTransaction();
              return false;
            }
          }
        }
        $aUValues = array(
            'weight' => $fTotalPacketWeight
        );
        if (Common::Update($aConfig['tabls']['prefix'] . "orders_items", $aUValues, 'id = ' . $iItemId) === true) {
          $bIsErr = true;
        }
      }
      if (!$bIsErr) {
        // dodanie informacji o zalacznikach produktu
        if ($this->checkAttachmentsM($aOrder['bookstore'], $iId)) {
          $sSql = "SELECT A.*, B.name FROM " . $aConfig['tabls']['prefix'] . "products_attachments A
													JOIN " . $aConfig['tabls']['prefix'] . "products_attachment_types B
													ON B.id = A.attachment_type
													WHERE A.product_id = " . $iId;
          $aAttachments = & $pDbMgr->GetAll($aOrder['bookstore'], $sSql);
          foreach ($aAttachments as $aAttVal) {
            // domyślnie wszystkie wartości z załącznika
            $aAttRecountData = $aAttVal;
            // nadpisanie pojedyńczych z produktu nadrzędnego
            $aAttRecountData['product_discount'] = $aValues['discount'];
            $aAttRecountData['product_quantity'] = $aValues['quantity'];
            $aAttRecountData['product_shipment_time'] = $aValues['shipment_time'];

            $aValuesAtt = $oOrderItemRecount->getAttrValues($aAttRecountData, $iItemId, $iOId);
            if (Common::Insert($aConfig['tabls']['prefix'] . "orders_items", $aValuesAtt) === false) {
              Common::RollbackTransaction();
              return false;
            }
          }
          // aktualizacja informacji o tym, ze produkt posiada zalaczniki
          $aValues = $oOrderItemRecount->getParentAttData($aValues, $aAttachments, $fPromoPriceBrutto);
          if (Common::Update($aConfig['tabls']['prefix'] . "orders_items", $aValues, "id = " . $iItemId . " AND order_id = " . $iOId . ' LIMIT 1') === false) {
            Common::RollbackTransaction();
            return false;
          }
        }
      }

      if (!$bIsErr) {
        if ($this->recountOrder($iOrderId, $aOrder['payment_id']) === false) {
          $bIsErr = true;
        }
      }
    } else {
      $bIsErr = true;
    }
    if ($bIsErr) {
      Common::RollbackTransaction();
      // wyswietlenie komunikatu o niepowodzeniu
      // oraz ponowne wyswietlenie formularza edycji
      $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_item_err'], $aProduct['name']);
      $this->sMsg .= GetMessage($sMsg);
      AddLog($sMsg);
      $this->Details($pSmarty, $iPId, $iOrderId);
    } else {
      Common::CommitTransaction();
      $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_item_ok'], $aProduct['name']);
      $this->sMsg .= GetMessage($sMsg, false);
      // dodanie informacji do logow
      AddLog($sMsg, false);
      $this->Details($pSmarty, $iPId, $iOrderId);
    }
  }
// end of addProduct() method

  /**
   * Metoda pobiera liste zdefiniowanych w serwisie platosci
   * w postaci dla pola COMBO
   * 
   * @return	array ref
   */
  private function getFilterPaymentTypes() {
    global $aConfig;

    $sSql = "SELECT DISTINCT ptype AS value
						 FROM " . $aConfig['tabls']['prefix'] . "orders_payment_types
						 ORDER BY id";
    $aPtypes = & Common::GetAll($sSql);
    foreach ($aPtypes as $iKey => $aPtype) {
      $aPtypes[$iKey]['label'] = $aConfig['lang'][$this->sModule . '_payment_types']['ptype_' . $aPtype['value']];
    }
    return array_merge(array(array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all_ptypes'])), $aPtypes);
  }
// end of getFilterPaymentTypes() method

  /**
   * Metoda zwraca tablice ze statusami zamowien do filtrow
   * 
   * @return	array ref
   */
  private function getFilterStatuses() {
    global $aConfig;
    $aStatuses = array(
        array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all_statuses']),
        array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['status_0']),
        array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1']),
        array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['status_2']),
        array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['status_3']),
        array('value' => '4', 'label' => $aConfig['lang'][$this->sModule]['status_4']),
        array('value' => '5', 'label' => $aConfig['lang'][$this->sModule]['status_5'])
    );
    return $aStatuses;
  }
// end of getFilterStatuses() method

  /**
   * Metoda zwraca tablice ze statusami zamowien wewnętrznych
   * 
   * @return	array ref
   */
  private function getFilterInternalStatuses() {
    global $aConfig;
    $aStatuses = array(
        array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all_statuses']),
        array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['internal_status_0']),
        array('value' => '5', 'label' => $aConfig['lang'][$this->sModule]['internal_status_5']),
    );
    return $aStatuses;
  }
// end of getFilterInternalStatuses() method

  /**
   * Metoda zwraca tablice ze statusami zamowien do filtrow
   * 
   * @return	array ref
   */
  private function getFilterPaymentStatuses() {
    global $aConfig;
    $aStatuses = array(
        array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all_statuses']),
        array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['pstatus_0']),
        array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['pstatus_1'])
    );
    return $aStatuses;
  }
// end of getFilterPaymentStatuses() method

  /**
   * Metoda zwraca czesc zapytania SQL odpowiedzialnego za filtrowanie
   * po dacie zlozenia zamowienia
   * 
   * @return	string
   */
  private function getDateSql() {
    $sDateSql = '';

    if (isset($_POST['f_date_from']) && $_POST['f_date_from'] != '')
      $sDateSql .= " AND DATE(order_date) >= '" . date('Y-m-d', strtotime($_POST['f_date_from'])) . "'";
    if (isset($_POST['f_date_to']) && $_POST['f_date_to'] != '')
      $sDateSql .= " AND DATE(order_date) <= '" . date('Y-m-d', strtotime($_POST['f_date_to'])) . "'";

    if (isset($_POST['f_status_4_from']) && $_POST['f_status_4_from'] != '')
      $sDateSql .= " AND DATE(status_4_update) >= '" . date('Y-m-d', strtotime($_POST['f_status_4_from'])) . "'";
    if (isset($_POST['f_status_4_to']) && $_POST['f_status_4_to'] != '')
      $sDateSql .= " AND DATE(status_4_update) <= '" . date('Y-m-d', strtotime($_POST['f_status_4_to'])) . "'";

    if (isset($_POST['f_payment_date_from']) && $_POST['f_payment_date_from'] != '' &&
            isset($_POST['f_payment_date_to']) && $_POST['f_payment_date_to'] != '') {
      $sDateSql .= " AND 
												(
												IF(payment_date <> '0000-00-00 00:00:00', 
															payment_date <= '" . date('Y-m-d', strtotime($_POST['f_payment_date_to'])) . " 23:59:59' AND 
															payment_date >= '" . date('Y-m-d', strtotime($_POST['f_payment_date_from'])) . " 00:00:00', 1=2)
												OR
												(IF(second_payment_enabled = '1' AND second_payment_date <> '0000-00-00 00:00:00', 
															second_payment_date <= '" . date('Y-m-d', strtotime($_POST['f_payment_date_to'])) . " 23:59:59' AND 
															second_payment_date >= '" . date('Y-m-d', strtotime($_POST['f_payment_date_from'])) . " 00:00:00', 1=2))
												)  ";
    } else {
      if (isset($_POST['f_payment_date_from']) && $_POST['f_payment_date_from'] != '') {
        $sDateSql .= " AND 
												(
												IF(payment_date <> '0000-00-00 00:00:00', payment_date >= '" . date('Y-m-d', strtotime($_POST['f_payment_date_from'])) . " 00:00:00', 1=2)
												OR
												(IF(second_payment_enabled = '1' AND second_payment_date <> '0000-00-00 00:00:00', second_payment_date >= '" . date('Y-m-d', strtotime($_POST['f_payment_date_from'])) . " 00:00:00', 1=2))
												) ";
      }
      if (isset($_POST['f_payment_date_to']) && $_POST['f_payment_date_to'] != '') {
        $sDateSql .= " AND 
												(
												IF(payment_date <> '0000-00-00 00:00:00', payment_date <= '" . date('Y-m-d', strtotime($_POST['f_payment_date_to'])) . " 23:59:59', 1=2)
												OR
												(IF(second_payment_enabled = '1' AND second_payment_date <> '0000-00-00 00:00:00', second_payment_date <= '" . date('Y-m-d', strtotime($_POST['f_payment_date_to'])) . " 23:59:59', 1=2))
												)  ";
      }
    }
    return $sDateSql;
  }
// end of getDateSql() method

  /**
   * Metoda wysyła maila z zawartością zamówienia do usera, używane w innych miejscach
   * 
   * @param int $iId - id zamówinia
   * @param string $sType - typ maila: status/items_change
   * @return unknown_type
   */
  public function sendOrderMail($iId) {
    global $aConfig, $pDbMgr, $pSmarty;

    $sWebsite = $this->getWebsiteSymbol($this->getWebsiteId($iId, false));

    setWebsiteSettings($this->iLangId, $sWebsite);

    $aModule['lang'] = & $aConfig['lang']['m_zamowienia_mail'];

    $_GET['lang_id'] = 1;
    $aSiteSettings = getSiteSettings($sWebsite);
    $sFrom = $aConfig['default']['website_name'];
    $sFromMail = $aSiteSettings['orders_email'];

    $sSubject = $aModule['lang']['subject'];

    $sSql = "SELECT A.*, (A.transport_cost + A.value_brutto) as total_value_brutto, DATE_FORMAT(A.order_date, '" . $aConfig['common']['sql_date_hour_format'] . "') as order_date, DATE_FORMAT(A.status_1_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_1_update, DATE_FORMAT(A.status_2_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_2_update, DATE_FORMAT(A.status_3_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_3_update, B.mail_info AS mail_transport_info
						 FROM " . $aConfig['tabls']['prefix'] . "orders A
						  LEFT JOIN " . $aConfig['tabls']['prefix'] . "orders_transport_means B
						 ON B.id=A.transport_id
						 WHERE A.id = " . $iId;
    $aModule['order'] = & Common::GetRow($sSql);
    $aModule['order']['change_status_date'] = sprintf($aModule['lang']['status_change_date'], $aModule['order']['status_' . $aModule['order']['order_status'] . '_update'], $aModule['order']['order_number'], $aConfig['lang']['m_zamowienia']['status_' . $aModule['order']['order_status']]);

    $sSql = "SELECT *
						 FROM " . $aConfig['tabls']['prefix'] . "orders_items
						 WHERE order_id = " . $iId;
    $aModule['items'] = & Common::GetAll($sSql);
    $aModule['footer'] = $aConfig['default']['website_mail_footer'];

    foreach ($aModule['items'] as $iKey => $aValue) {
      $aModule['items'][$iKey]['price_brutto'] = Common::formatPrice($aModule['items'][$iKey]['price_brutto']);
      $aModule['items'][$iKey]['value_brutto'] = Common::formatPrice($aModule['items'][$iKey]['value_brutto']);
      $aModule['items'][$iKey]['promo_price_brutto'] = Common::formatPrice($aModule['items'][$iKey]['promo_price_brutto']);
      $pShipmentTime = new \orders\Shipment\ShipmentTime();
      $aModule['items'][$iKey]['shipment_time'] = $pShipmentTime->getShipmentTime($aValue['id'], $aValue['shipment_time']);
      if ($aValue['preview'] == '1') {
        $aModule['items'][$iKey]['shipment_date'] = formatDateClient($aValue['shipment_date']);
      }
    }
    $aModule['order']['value_brutto'] = Common::formatPrice($aModule['order']['value_brutto']);
    $aModule['order']['transport_cost'] = Common::formatPrice($aModule['order']['transport_cost']);
    $aModule['order']['total_value_brutto'] = Common::formatPrice($aModule['order']['total_value_brutto']);
    //$aModule['order']['transport'] = $aConfig['lang'][$this->sModule]['transport_1'];
    $aModule['order']['path'] = $aConfig['common']['client_base_url_http_no_slash'];
    $aModule['order']['to_pay'] = Common::formatPrice($aModule['order']['to_pay']);
    $aModule['seller_data'] = $this->getPaymentOrderDetailsA($sWebsite);
    $aModule['addresses'] = $this->getOrderAddresses($iId);

    $sTransportId = $aModule['order']['transport_id'];
    $sTransportSymbol = $this->getTransportMeansSymbol($sTransportId);

    if ($sTransportSymbol == 'odbior-osobisty') {
      $aModule['order']['transport_symbol'] = $sTransportSymbol;
    } else {
      if ($sTransportSymbol != 'tba' && $sTransportSymbol != 'opek-przesylka-kurierska' && $sTransportSymbol != 'poczta-polska-doreczenie-pod-adres') {
        $aModule['order']['transport_symbol'] = $sTransportSymbol;
        $oShipment = new Shipment($sTransportSymbol, $pDbMgr);
        $aModule['order']['point_of_receipt'] = $oShipment->getSinglePointDetails($aModule['order']['point_of_receipt']);
      }
    }

    $pSmarty->assign_by_ref('aModule', $aModule);
    $sHtml = $pSmarty->fetch($aConfig['common']['client_base_path'] . 'smarty/templates/modules/m_zamowienia/_order_items_change_email_' . $sWebsite . '.tpl');
    $pSmarty->clear_assign('aModule', $aModule);


    if (!empty($aModule['order'])) {
      $aHtmlImages = array();
      if (!empty($aConfig['common'][$sWebsite]['email_logo_file'])) {
        $sImgF = $aConfig['common']['client_base_path'] . $aConfig['common'][$sWebsite]['email_logo_file'];
        $aImage = getimagesize($sImgF);
        $aHtmlImages[] = array($sImgF, $aImage['mime']);
      }

      return Common::sendTxtHtmlMail($sFrom, $sFromMail, $aModule['order']['email'], $sSubject, '', $sHtml, '', '', $aHtmlImages);


      $sMsg = sprintf($aModule['lang']['send_ok'], $aModule['order']['email']);
      $this->sMsg .= GetMessage($sMsg, false);
      // dodanie informacji do logow
      AddLog($sMsg, false);
      return true;
    } else {
      $sMsg = $aModule['lang']['send_err'];
      $this->sMsg .= GetMessage($sMsg, true);
      // dodanie informacji do logow
      AddLog($sMsg, false);
      return false;
    }
  }
// end of sendOrderMail() method

  /**
   * 
   * @global array $aConfig
   * @param int $iId
   * @return boolean
   */
  public function sendOrderStatusMail($iId) {
    global $aConfig;
    $aMatches = array();

    $sWebsite = $this->getWebsiteSymbol($this->getWebsiteId($iId, false));

    setWebsiteSettings(1, $sWebsite);

    $aModule['lang'] = & $aConfig['lang']['m_zamowienia_mail'];

    $_GET['lang_id'] = 1;
    $aSiteSettings = getSiteSettings($sWebsite);
    $sFrom = $aConfig['default']['website_name'];
    $sFromMail = $aSiteSettings['orders_email'];

    $sSql = "SELECT *, 
                    MD5(CONCAT(check_status_key, order_number)) as get_invoice_key,
                    (transport_cost + value_brutto) as total_value_brutto, 
                    DATE_FORMAT(order_date, '" . $aConfig['common']['sql_date_hour_format'] . "') as order_date, 
                    DATE_FORMAT(status_1_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_1_update, 
                    DATE_FORMAT(status_2_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_2_update, 
                    DATE_FORMAT(status_3_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_3_update
						 FROM " . $aConfig['tabls']['prefix'] . "orders
						 WHERE id = " . $iId;
    $aModule['order'] = & Common::GetRow($sSql);
	
	  if ($_SERVER['REMOTE_ADDR'] == '83.220.102.129') {
		  if ($aModule['order']['order_status'] == '0' || $aModule['order']['order_status'] == '2') {
			  $aModule['order']['order_status'] = 1;
		  }
	  }
	  
    if ($aModule['order']['order_status'] == '2') {
      return true;
    }
	
    if ($aModule['order']['order_status'] == '0') {
        $aModule['order']['order_status'] = '1';
    }
    $sMail = $aConfig['lang'][$this->sModule]['mail_status_' . $aModule['order']['order_status']];
    if ($aModule['order']['order_status'] == '4') {
      $sTMeansSymbol = $this->getTransportMeansSymbol($aModule['order']['transport_id']);
      switch ($sTMeansSymbol) {
        case 'odbior-osobisty':
          $sMail .= '_odb_osob';
          break;
        case 'paczkomaty_24_7':
          $sMail .= '_paczkomaty';
          break;
        case 'poczta_polska':
          $sMail .= '_poczta_pl';
          break;
        case 'opek-przesylka-kurierska':
          $sMail .= '_kurier';
          break;
	  case 'poczta-polska-doreczenie-pod-adres':
		  $sMail .= '_poczta_adres';
		  break;
        case 'ruch':
          $sMail .= '_ruch';
          break;
        case 'orlen':
          $sMail .= '_orlen';

          $sTransportNumber = $aModule['order']['transport_number'];
          preg_match("/^\d{4}(\d{13})/", $sTransportNumber, $aMatches);
          $sTransportNumber = $aMatches[1];
          $aModule['order']['transport_number'] = $sTransportNumber;
          break;
        case 'tba':
          $sMail .= '_tba';
          break;
      }
    }
    
    $sFVLink = str_replace('http://', 'https://', $aConfig['common'][$sWebsite]['client_base_url_http_no_slash']).'/sprawdz-status-zamowienia/id'.$iId.',invoice.html?key='.$aModule['order']['get_invoice_key'];
    $aVars = array('{nr_zamowienia}','{data_statusu}','{list_przewozowy}', '{link_do_faktury}');
    $aValues = array($aModule['order']['order_number'], $aModule['order']['status_'.$aModule['order']['order_status'].'_update'], $aModule['order']['transport_number'], $sFVLink);
    
	 	if($aModule['order']['order_status'] == '2'){
			if(($aModule['order']['payment_status'] == '1' && $aModule['order']['paid_amount'] >= $aModule['order']['to_pay']) || $aModule['order']['payment_type'] == 'bank_14days' || $aModule['order']['payment_type'] == 'postal_fee'){
				$sMail .= '_oplacone';
			} else {
				$sMail .= '_nieoplacone';
			}
		}	
		$aMail = Common::getWebsiteMail($sMail, $sWebsite);

      $attachments = [];
      if($aModule['order']['order_status'] == '3' && $sWebsite != 'np'){
          $attachments[] = [realpath($_SERVER['DOCUMENT_ROOT'].'/images/email_attachments/'.$sWebsite.'/Informacja.pdf')];
          $attachments[] = [realpath($_SERVER['DOCUMENT_ROOT'].'/images/email_attachments/'.$sWebsite.'/Zał.1 Odstąpienie od umowy.pdf')];
          $attachments[] = [realpath($_SERVER['DOCUMENT_ROOT'].'/images/email_attachments/'.$sWebsite.'/Zał.2 Reklamacja.pdf')];
      }

      if ($aMail['content'] != '') {
//          Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,'raporty@profit24.pl',  $aMail['content'],$aVars,$aValues,array(), array(), $sWebsite, $attachments);
          return Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,$aModule['order']['email'],  $aMail['content'],$aVars,$aValues,array(), array(), $sWebsite, $attachments);
      }
      return true;
	} // end of sendUserPasswordEmail() method
	
  
	 /**
	  * Metoda wysyła maila w związku z utworzeniem paczki w Paczkomaty.pl
	  *
	  * @global array $aConfig
	  * @global type $pSmarty
	  * @param type $iId
	  * @return boolean 
	  */
	 public function sendOrderTransportMail($iId) {
		global $aConfig;
		
		$sWebsite = $this->getWebsiteSymbol($this->getWebsiteId($iId, false));
		setWebsiteSettings(1, $sWebsite);
		
	 	$aModule['lang'] =& $aConfig['lang']['m_zamowienia_mail'];
	 	
	 	$_GET['lang_id']=1;
		$aSiteSettings = getSiteSettings($sWebsite);
	 	$sFrom = $aConfig['default']['website_name'];
		$sFromMail = $aSiteSettings['orders_email'];
		
		$sSql = "SELECT *, (transport_cost + value_brutto) as total_value_brutto, DATE_FORMAT(order_date, '".$aConfig['common']['sql_date_hour_format']."') as order_date, DATE_FORMAT(status_1_update, '".$aConfig['common']['sql_date_hour_format']."') as status_1_update, DATE_FORMAT(status_2_update, '".$aConfig['common']['sql_date_hour_format']."') as status_2_update, DATE_FORMAT(status_3_update, '".$aConfig['common']['sql_date_hour_format']."') as status_3_update
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id = ".$iId;
		$aModule['order'] =& Common::GetRow($sSql);
		if($aModule['order']['order_status'] == '3') return true;
		
		$aVars = array('{nr_zamowienia}', '{list_przewozowy}');
		$aValues = array($aModule['order']['order_number'], $aModule['order']['transport_number']);

    if ($aModule['order']['order_status'] == '2') {
      if (($aModule['order']['payment_status'] == '1' && $aModule['order']['paid_amount'] >= $aModule['order']['to_pay']) || $aModule['order']['payment_type'] == 'bank_14days' || $aModule['order']['payment_type'] == 'postal_fee') {
        $sMail .= '_oplacone';
      } else {
        $sMail .= '_nieoplacone';
      }
    }
    $aMail = Common::getWebsiteMail($sMail, $sWebsite);
    return Common::sendWebsiteMail($aMail, $sFrom, $sFromMail, $aModule['order']['email'], $aMail['content'], $aVars, $aValues, array(), array(), $sWebsite);
  }// end of sendUserPasswordEmail() method


  /**
   * Metoda wysyła maila z informacją o zminie statusu zamówienia
   * @param $iId - id zamówienia
   */
  public function sendOrderStatus($iPId, $iId) {
    global $pSmarty;

    if ($this->sendOrderStatusMail($iId) === false) {
      $this->sMsg .= GetMessage(_('Wiadomość nie została wysłana do klienta'), true);
    } else {
      $this->sMsg .= GetMessage(_('Wiadomość została wysłana do klienta'), false);
    }
    $this->Show($pSmarty, $iPId);
  }
// end of sendOrderStatus() method

  /**
   * Metoda pobiera dane sprzedawcy
   *
   * @return	array ref	- tablica z danymi
   */
  private function getPaymentOrderDetailsA($sWebsite = 'profit24') {
    global $aConfig, $pDbMgr;

    $sSql = "SELECT *
						 FROM " . $aConfig['tabls']['prefix'] . "orders_seller_data";
    return $pDbMgr->GetRow($sWebsite, $sSql);
  }
// end of getSellerData() method

  /**
   * Metoda pobiera listę zamówień na podstawie tablicy z ich id -
   *  wykorzystywana przy grupowej zmianie statusu
   * @param $pSmarty
   * @param $aIds - tablica z id zamówień do pobrania
   * @return string Html
   */
  private function GetOrderList($aIds) {
    global $aConfig;

    // dolaczenie klasy View
    include_once('View/View.class.php');

    // zapamietanie opcji
    rememberViewState($this->sModule);

    $aHeader = array(
        'refresh' => false,
        'search' => false,
        'per_page' => false,
        'form' => false,
        'checkboxes' => false
    );
    $aAttribs = array(
        'width' => '100%',
        'border' => 0,
        'cellspacing' => 0,
        'cellpadding' => 0,
        'class' => 'viewHeaderTable'
    );
    $aRecordsHeader = array(
        array(
            'db_field' => 'order_number',
            'content' => $aConfig['lang'][$this->sModule]['list_order_id'],
            'sortable' => true,
            'width' => '150'
        ),
        array(
            'content' => $aConfig['lang'][$this->sModule]['list_user'],
            'sortable' => false,
            'width' => '150'
        ),
        array(
            'db_field' => 'total_value_brutto',
            'content' => $aConfig['lang'][$this->sModule]['list_value_brutto'],
            'sortable' => true,
            'width' => '85'
        ),
        array(
            'db_field' => 'total_cost',
            'content' => $aConfig['lang'][$this->sModule]['list_total_cost'],
            'sortable' => true,
            'width' => '85'
        ),
        array(
            'db_field' => 'order_date',
            'content' => $aConfig['lang'][$this->sModule]['list_order_date'],
            'sortable' => true,
            'width' => '150'
        ),
        array(
            'db_field' => 'payment',
            'content' => $aConfig['lang'][$this->sModule]['list_payment'],
            'sortable' => true,
            'width' => '85'
        ),
        array(
            'content' => '&nbsp;',
            'sortable' => false,
            'width' => '75'
        ),
        array(
            'content' => '&nbsp;',
            'sortable' => false,
            'width' => '90'
        )
    );

    $pView = new View('orders_list', $aHeader, $aAttribs);
    $pView->AddRecordsHeader($aRecordsHeader);


    // pobranie wszystkich zamowien
    $sSql = "SELECT id, order_number, email AS user, value_brutto, (value_brutto+transport_cost) AS total_cost, order_date, payment, dotpay_t_id, CONCAT('dotpay_', dotpay_t_status) dotpay_t_status
             FROM " . $aConfig['tabls']['prefix'] . "orders
             WHERE id IN(" . implode(',', $aIds) . ")
             ORDER BY id DESC";
    $aRecords = & Common::GetAll($sSql);

    foreach ($aRecords as $iKey => $aItem) {
      if (empty($aItem['user'])) {
        $aRecords[$iKey]['user'] = '<span style="color: red">' . $aConfig['lang'][$this->sModule]['user_no_data'] . '</span>';
      } else {
        // uzytkownik - login + nazwa
        $aRecords[$iKey]['user'] = '<span style="color: gray">' . $aItem['user'] . '</span>';
      }
      // wartosc zamowienia netto
      $aRecords[$iKey]['total_cost'] = Common::formatPrice3($aItem['total_cost']) . ' <span style="color: gray">' . $aConfig['lang']['common']['currency'] . '</span>';
      // wartosc zamowienia brutto
      $aRecords[$iKey]['value_brutto'] = Common::formatPrice3($aItem['value_brutto']) . ' <span style="color: gray">' . $aConfig['lang']['common']['currency'] . '</span>';

      $aRecords[$iKey]['dotpay_t_status'] = $aConfig['lang'][$this->sModule][$aItem['dotpay_t_status']];
      unset($aRecords[$iKey]['id']);
    }
    $aColset = array();
    // dodanie rekordow do widoku
    $pView->AddRecords($aRecords, $aColset);
    return ShowTable($pView->Show());
  }
// end of GetOrderList() method

  /**
   * Metoda pobiera listę metod transportu dla coboboxa
   * 
   * @param bool $bTrim
   * @return array
   */
  private function &getTransportMeans($bTrim = false) {
    global $aConfig;

    $sSql = "SELECT id AS value, name AS label
				 		 FROM " . $aConfig['tabls']['prefix'] . "orders_transport_means
					 	 ORDER BY name";
    $aRecords = &Common::GetAll($sSql);
    if ($bTrim == true) {
      foreach ($aRecords as $iKey => $aItem) {
        $aRecords[$iKey]['label'] = trimString($aItem['label'], 16);
      }
    }
    $aItems = array(
        array('value' => '', 'label' => $aConfig['lang']['common']['choose'])
    );
    return array_merge($aItems, $aRecords);
  }
// end of getTransportMeans() method

  /**
   * Metoda przelicza zamówienie, używane w zewnętrznych miejscach
   * 
   * @param uint $iId
   * @param uint $iPaymentId
   * @return bool
   */
  public function recountOrder($iId, $iPaymentId = 0) {
    global $pDbMgr;

    include_once('lib/Orders/OrderRecountPost.class.php');
    $oOrderRecount = new OrderRecountPost($pDbMgr, FALSE);
    try {
      $mReturn = $oOrderRecount->recountOrderPost($iId, $_POST, $_POST['editable'], $iPaymentId);
      if ($mReturn === 2) {
        $this->sendOrderStatusMail($iId);
      }
    } catch (Exception $ex) {
      $this->sMsg .= GetMessage($ex->getMessage());
    }
	
	  $sMsg = $oOrderRecount->getMessage();
	  if ($sMsg != '') {
		  $this->sMsg .= GetMessage($oOrderRecount->getMessage(), false);
		  AddLog($sMsg, false);
	  }

  }
// end of recountOrder() method

  /**
   * Metoda pobiera autorow produktu o podanym id
   * 
   * @param string $sDb - identyfikator bazy
   * @param	intger	$iId	- Id produktu
   * @return	array	- autorzy produktu
   */
  private function getAuthorsStrM($sDb, $iId) {
    global $aConfig, $pDbMgr;

    // pobranie autorow ksiazki
    $sSql = "SELECT B.name as role_name, CONCAT(C.surname,' ',C.name) as author
						FROM " . $aConfig['tabls']['prefix'] . "products_to_authors A
						JOIN " . $aConfig['tabls']['prefix'] . "products_authors_roles B
						ON B.id = A.role_id
						JOIN " . $aConfig['tabls']['prefix'] . "products_authors C
						ON C.id = A.author_id
						WHERE A.product_id = " . $iId . "
						ORDER BY B.order_by";

    $aResult = & $pDbMgr->GetAll($sDb, $sSql);
    $sAuthors = '';

    foreach ($aResult as $aAuthor) {
      $sAuthors .= $aAuthor['author'] . ', ';
    }
    if (!empty($sAuthors))
      $sAuthors = substr($sAuthors, 0, -2);
    return $sAuthors;
  }
// end of getAuthorsStrM() method	

  /**
   * Metoda sprawdza czy produkt ma dodane zalaczniki
   * @param string $sDb - identyfikator bazy
   * @param int $iId - id produktu
   * @return bool
   */
  private function checkAttachmentsM($sDb, $iId) {
    global $aConfig, $pDbMgr;

    $sSql = "SELECT COUNT(*) FROM " . $aConfig['tabls']['prefix'] . "products_attachments WHERE product_id = '" . $iId . "'";

    return intval($pDbMgr->GetOne($sDb, $sSql)) > 0;
  }
// end of checkAttachmentsM() method

  /**
   * Metoda zwraca aktualny cennik dla produktu 
   * @param string $sDb - identyfikator bazy
   * @param $iId - id produktu
   * @return array cennik
   */
  private function &getTarrifM($sDb, $iId) {
    global $aConfig, $pDbMgr;
    $sSql = "SELECT promotion_id, discount, price_brutto, price_netto, discount_value
            FROM ".$aConfig['tabls']['prefix']."products_tarrifs
            WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
            ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1";
    return $pDbMgr->GetRow($sDb,$sSql);
  } // end of getTarrifM()


  /**
   * Metoda zwraca rabat na produkt zdef. dla danych fv 
   * @param type $iID
   * @param type $iUAID
   * @return mixed
   */
  private function getFVDiscount($iID, $iUAID = null, $iUID = null) {

    $mDiscountPublisher = $this->checkFVPublisherDiscount($iID, $iUAID, $iUID);
    if ($mDiscountPublisher !== FALSE) {

      $mDiscountSeries = $this->checkFVSeriesDiscount($iID, $iUAID, $iUID);
      if ($mDiscountSeries !== FALSE && $mDiscountSeries > 0) {
        return $mDiscountSeries;
      } elseif ($mDiscountPublisher > 0) {
        return $mDiscountPublisher;
      } else {
        return FALSE;
      }
    } else {
      return FALSE;
    }
  }

  /**
   * Metoda zwraca wartość rabatu na wydawnictwo zdef. dla danych do fv
   * @param integer $iID
   * @param integer $iUAID
   * @return mixed
   */
  private function checkFVPublisherDiscount($iID, $iUAID = null, $iUID = null) {
    global $pDbMgr;
    $sSql = 'SELECT utpp.discount 
            FROM products AS p 
            JOIN products_publishers AS pp
            ON p.publisher_id = pp.id 
            JOIN users_to_products_publishers AS utpp 
            ON pp.id = utpp.products_publishers_id 
            WHERE p.id = ' . $iID
             . ($iUAID > 0 ? ' AND utpp.user_adress_id = ' . $iUAID : '')
             . ($iUID > 0 ? ' AND utpp.users_accounts_id = ' . $iUID : '');
    $fDiscount = $pDbMgr->GetOne('profit24', $sSql);
    if ($fDiscount != '') {
      return $fDiscount;
    } else {
      return FALSE;
    }
  }

  /**
   * Metoda zwraca wartośc rabatu na serie wyd. zdef. dla danych do fv 
   * @global \DatabaseManager $pDbMgr
   * @param type $iID
   * @param type $IUAID
   * @return mixed
   */
  private function checkFVSeriesDiscount($iID, $iUAID = null, $iUID = null) {
    global $pDbMgr;
    $sSql = 'SELECT utps.discount 
             FROM products AS p
             JOIN products_to_series AS ps
             ON p.id = ps.product_id
             JOIN users_to_prod_series_publishers AS utps
             ON ps.series_id = utps.products_series_id
             JOIN users_to_products_publishers AS utpp
             ON utps.users_to_products_publishers_id = utpp.id
             WHERE p.id = ' . $iID
             . ($iUAID > 0 ? ' AND utpp.user_adress_id = ' . $iUAID : '')
             . ($iUID > 0 ? ' AND utpp.users_accounts_id = ' . $iUID : '') ;


    $fDiscount = $pDbMgr->GetOne('profit24', $sSql);
    if ($fDiscount > 0) {
      return $fDiscount;
    } else {
      return FALSE;
    }
  }

    /**
     * @param $iProductId
     * @return mixed
     */
    private function getProductData($iProductId, $sBookstore)
    {
        global $pDbMgr;

        $sSql = 'SELECT wholesale_price, id
                 FROM  products
                 WHERE id = ' . $iProductId;
        return $pDbMgr->GetRow($sBookstore, $sSql);
    }


    /**
     * Metoda oblicza czenę promocyjną
     *
     * @deprecated since version 2.0 UWAGA NIE UŻYWAĆ TEJ FUNKCJI
     * @param float $fPriceBrutto - cena brutto produktu
     * @param array ref $aTarrif - tablica z cennikiem
     * @param string ref $sPriceLang - referencja do stringa do ktorego zostanie zwrócony lang z typem ceny
     * @param float $fPublisherLimit - wartosc limitu promocji dla wydawnictwa
     * @param bool $bLogin - czy przy logowaniu
     * @param bool $bLogout - czy przy wylogowywanniu
     * @return float - cena promocyjna
     */
    private function getPromoPrice(&$aTarrif, &$sPriceLang, $fPriceBrutto, $fSourceLimit, $fPublisherLimit = 0, $iProductId, $iOrderId, $bLogin = false, $bLogout = false, $sBookstore = '')
    {
        global $aConfig;
        $sPromoText = '';
        $fPromoPrice = 0;

        $aProductData = $this->getProductData($iProductId, $sBookstore);
        $wholesalePrice = $aProductData['wholesale_price'];

        $sSql = 'SELECT user_id 
           FROM orders
           WHERE id = ' . $iOrderId;
        $iUID = Common::GetOne($sSql);

        //rabat na wydawnictwa i serie wyd.
        if (!$bLogout) {

            $sSql = 'SELECT user_adress_id 
               FROM orders_users_addresses
               WHERE order_id = ' . $iOrderId . '
                 AND address_type = "0"';

            $iUAID = Common::GetOne($sSql);
            if ($iUAID > 0) {

                $mFVDiscount = $this->getFVDiscount($iProductId, $iUAID);
                if ($mFVDiscount > 0) {
                    $fDiscount = $mFVDiscount;
                    $fPromoPrice = $fPriceBrutto - $fPriceBrutto * $fDiscount / 100;
                    $sPromoText = $aConfig['lang']['common']['cena_uzytkownika'];


                    // limit ceny zakupu
                    if ($fPromoPrice <= $wholesalePrice) {
                        // wyliczamy procent od WH price
                        $fDiscount = Common::formatPrice2((100 - (($wholesalePrice * 100) / $fPriceBrutto)) - self::MINIMAL_ADDITIONAL_DISCOUNT);
                        $fPromoPrice = Common::formatPrice2($fPriceBrutto - $fPriceBrutto * ($fDiscount / 100));
                    }

                    $aTarrif['discount'] = $fDiscount;
                    return $fPromoPrice;
                }
            }

            if ($iUID > 0) {

                $mFVDiscount = $this->getFVDiscount($iProductId, null, $iUID);
                if ($mFVDiscount > 0) {
                    $fDiscount = $mFVDiscount;
                    $fPromoPrice = $fPriceBrutto - $fPriceBrutto * $fDiscount / 100;
                    $sPromoText = $aConfig['lang']['common']['cena_uzytkownika'];


                    // limit ceny zakupu
                    if ($fPromoPrice <= $wholesalePrice) {
                        // wyliczamy procent od WH price
                        $fDiscount = Common::formatPrice2((100 - (($wholesalePrice * 100) / $fPriceBrutto)) - self::MINIMAL_ADDITIONAL_DISCOUNT);
                        $fPromoPrice = Common::formatPrice2($fPriceBrutto - $fPriceBrutto * ($fDiscount / 100));
                    }

                    $aTarrif['discount'] = $fDiscount;
                    return $fPromoPrice;
                }
            }
        }

        if ($aTarrif['price_brutto'] != $fPriceBrutto) {
            $fPromoPrice = $aTarrif['price_brutto'];
            $sPromoText = $aConfig['lang']['common']['cena_w_ksiegarni'];
        }
        if (!$bLogout) {
            $fUserDiscount = $this->getOrderUserDiscount($iUID);
            if (($fUserDiscount > 0) && !$aTarrif['packet']) { // ($bLogin || isLoggedIn()) &&
                $iFDiscount = ($fPublisherLimit > 0 && $fUserDiscount > $fPublisherLimit) ? $fPublisherLimit : $fUserDiscount;
                $iDiscount = ($fSourceLimit > 0 && $iFDiscount > $fSourceLimit) ? $fSourceLimit : $iFDiscount;
                $fUserPrice = $fPriceBrutto - $fPriceBrutto * ($iDiscount / 100);
                if ($fPromoPrice != 0) {
                    if ($fUserPrice < $fPromoPrice) {
                        $fPromoPrice = $fUserPrice;
                        $sPromoText = $aConfig['lang']['common']['cena_uzytkownika'];
                        $aTarrif['discount'] = $iDiscount;
                    }
                } else {
                    $fPromoPrice = $fUserPrice;
                    $sPromoText = $aConfig['lang']['common']['cena_uzytkownika'];
                    $aTarrif['discount'] = $iDiscount;
                }
            }
        }
        // limit ceny zakupu
        if ($fPromoPrice <= $wholesalePrice) {
            // wyliczamy procent od WH price
            $fDiscount = Common::formatPrice2((100 - (($wholesalePrice * 100) / $fPriceBrutto)) - self::MINIMAL_ADDITIONAL_DISCOUNT);
            $fPromoPrice = Common::formatPrice2($fPriceBrutto - $fPriceBrutto * ($fDiscount / 100));
            $aTarrif['discount'] = $fDiscount;
        }


        $sPriceLang = $sPromoText;
        return $fPromoPrice;
    }// end of getPromoPrice() method

    /**
     * @param $iUID
     * @return mixed
     */
    private function getOrderUserDiscount($iUID) {

        $sSql = 'SELECT discount FROM users_accounts WHERE id = '.$iUID;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

  /**
   * Generuje kolejny nr faktury
   * 
   * @param string $sBookstore - kod księgarni
   * @param string $sOrderNr - nr zamówienia
   * @return string - nr faktury
   */
  private function getInvoiceNumber($sBookstore, $sOrderNr, $iWebsiteId) {
    global $aConfig;
    unset($sBookstore); // kiedys moze byc potrzebne

    $sSql = "SELECT * FROM " . $aConfig['tabls']['prefix'] . "orders_invoice_numbers FOR UPDATE";
    $aNumber = Common::GetRow($sSql);
    // mamy numer
    if (!empty($aNumber)) {
      if ($aNumber['number_year'] == date('Y')) {
        $sSql = "UPDATE " . $aConfig['tabls']['prefix'] . "orders_invoice_numbers SET invoice_nr=invoice_nr+1";
        if (Common::Query($sSql) === false) {
          return false;
        } else {
          return sprintf($aConfig['invoice_number_' . $iWebsiteId], $aNumber['invoice_nr'], $sOrderNr);
        }
      } else {
        $aValues = array(
            'invoice_nr' => 2,
            'number_year' => date('Y')
        );
        if (Common::Update($aConfig['tabls']['prefix'] . "orders_invoice_numbers", $aValues, '', false) === false) {
          return false;
        } else {
          return sprintf($aConfig['invoice_number_' . $iWebsiteId], 1, $sOrderNr);
        }
      }
    } else {
      // tabela numerów jest pusta
      $aValues = array(
          'invoice_nr' => 2,
          'number_year' => date('Y')
      );
      if (Common::Insert($aConfig['tabls']['prefix'] . "orders_invoice_numbers", $aValues, '', false) === false) {
        return false;
      } else {
        return sprintf($aConfig['invoice_number_' . $iWebsiteId], 1, $sOrderNr);
      }
    }
  }
// end of getInvoiceNumber() method

  /**
   * Sprawdza czy sposob transportu dla amówienia to odbiór osobisty
   * @param int $iOrderId - id zamowienia
   * @return bool
   */
  private function checkPersonalReception($iOrderId) {
    global $aConfig;
    $sSql = "SELECT B.personal_reception
						FROM " . $aConfig['tabls']['prefix'] . "orders A
						LEFT JOIN " . $aConfig['tabls']['prefix'] . "orders_transport_means B
						ON B.id=A.transport_id
						WHERE A.id = " . $iOrderId;
    return (Common::GetOne($sSql) == '1');
  }
// end of checkPersonalReception() method

  /**
   * Pobiera dane zamówienia
   * @external
   * 
   * @param int $iOrderId - id zamowienia
   * @return array
   */
  public function getOrderData($iOrderId) {
    global $aConfig;
    $sSql = "SELECT *
						FROM " . $aConfig['tabls']['prefix'] . "orders
						WHERE id = " . $iOrderId;
    return Common::GetRow($sSql);
  }
// end of getOrderData() method

  /**
   * Metoda pobiera opcje dla metody transportu
   * @external
   *
   * @global array $aConfig
   * @param integer $iTransportOptionId
   * @return string
   */
  public function getTransportOptionSymbol($iTransportOptionId) {
    global $aConfig;

    $sSql = "SELECT symbol FROM " . $aConfig['tabls']['prefix'] . "orders_transport_options
						 WHERE id=" . $iTransportOptionId;
    return Common::GetOne($sSql);
  }
// end of getTransportOptionSymbol() method

  /**
   * Drukuje list przewozowy
   * 
	 * @param $pSmarty
	 * @param $iPId - filtr listy
	 * @param $iOrderId - id zamówienia
	 * @param $bFromList - czy kliknięto na liście
	 * @return void
	 */
  private function getTransportListSetOrderConfirmed(&$pSmarty,$iPId,$iOrderId, $bFromList=false){
		global $aConfig, $pDbMgr;
    
		$aOrder = $this->getOrderData($iOrderId);
    
    if ($aOrder['invoice_ready'] != '1' && !isset($_COOKIE['printer_labels'])) {
      // FV jeszcze nie gotowa wiec ktoś chce wydrukować list przewozowy
      $sMsg = _('Ustaw nazwę drukarki etykiet w Zarządzanie -> Drukarki');
      $this->sMsg = GetMessage($sMsg);
      AddLog($sMsg);
      if ($bFromList) {
        $this->Show($pSmarty, $iPId);
      } else {
        $this->Details($pSmarty, $iPId, $iOrderId);
      }
      return false;
    }

    Common::BeginTransaction();
    if (($aOrder['payment_type'] == 'bank_transfer' || $aOrder['payment_type'] == 'dotpay' || $aOrder['payment_type'] == 'platnoscipl' || $aOrder['payment_type'] == 'payu_bank_transfer' || $aOrder['payment_type'] == 'card') && ($aOrder['payment_status'] != 1 || ($aOrder['paid_amount'] < $aOrder['to_pay'])) && !($aOrder['second_payment_enabled'] == '1' && $aOrder['second_payment_type'] == 'postal_fee')) {//zezwala na wydruk faktury jesli wybrano 2 sposob platnosci jako pobranie
      // aby zmienic status na 'skompletowane' zamowienie musi być opłacone, i kwota musi sie zgadzac
      $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['invoice_pay_err']);
      Common::RollbackTransaction();
      if ($bFromList) {
        $this->Show($pSmarty, $iPId);
      } else {
        $this->Details($pSmarty, $iPId, $iOrderId);
      }
      return;
    } else {
      if ($this->setOrderConfirmed($aOrder, $iOrderId, false) === false) {
        $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['invoice_gen_err']);
        Common::RollbackTransaction();
        if ($bFromList) {
          $this->Show($pSmarty, $iPId);
        } else {
          $this->Details($pSmarty, $iPId, $iOrderId);
        }
        return;
      }

      // jesli odbior osobisty, generujemy od razu fakturę
      if (empty($aOrder['transport_number'])) {
        $oGetTransport = new getTransportList($pDbMgr);
        try {
          // sprawdź czy metoda transportu to paczkomaty
          $sTransportSymbol = $this->getTransportMeansSymbol($aOrder['transport_id']);
          if ($sTransportSymbol != 'odbior-osobisty') {
            $aOrder['linked_orders'] = $this->getOrdersLinked($iOrderId);
            $oGetTransport->getSimpleTransportAddressLabel($aOrder, $aOrder['linked_orders'], false);
            $this->sMsg .= GetMessage(_('Wygenerowano list przewozowy'), false);
            Common::CommitTransaction();
          }
        } catch (Exception $ex) {
          Common::RollbackTransaction();
          $sMsg = $ex->getMessage();
          addLog($sMsg);
          $this->sMsg .= GetMessage($sMsg);
        }
      }
    }

    if ($bFromList) {
      $this->Show($pSmarty, $iPId);
    } else {
      $this->Details($pSmarty, $iPId, $iOrderId);
    }
  }
// end of generateInvoice() method

  /**
   * Metoda pomija generowanie faktyczne faktury, zmieniany jest wyłącznie status 
   *  i oznaczenie o wygenerowaniu dokumentu
   * @external
   * @date 27-08-2014
   * 
   * @param int $iOrderId
   * @param array $aOrder
   * @param bool $bPersonalReception
   * @return bool
   */
  public function proceedSetInvoice($iOrderId, $aOrder, $bPersonalReception) {
    global $aConfig;

    if (empty($aOrder['invoice_date'])) {
      $aVals = array(
          'invoice_date' => 'NOW()',
          'invoice_ready' => '1'
      );
      if (!$bPersonalReception) {
        $aVals['internal_status'] = '8';
      } else {
        $aVals['internal_status'] = '6';
      }
      if ($aOrder['payment_type'] == 'bank_14days' && 
          $aOrder['invoice_date_pay'] == '' &&
          isset($aOrder['invoice_to_pay_days'])) {
        if ($aOrder['invoice_to_pay_days'] == 0) {
          $aOrder['invoice_to_pay_days'] = 14;
        }
        $aDate = explode(' ', $aOrder['invoice_date_pay']);
        $sInvoiceDataPay = date('Y-m-d', strtotime($aDate[0]. ' + '.$aOrder['invoice_to_pay_days'].' day'));
        $aVals['invoice_date_pay'] = $sInvoiceDataPay;
      }

      if (Common::Update("orders", $aVals, "id = " . $iOrderId) === false) {
        $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['invoice_gen_err']);
        return;
      }
    }
    return true;
  }
// end of ommitGetInvoice() method

  /**
   * Metoda ustawia na zatwierdzono zamówienie, 
   *  UWAGA ten status także jest ustawiany z poziomu kodu w innych miejscach, 
   *  ciężko nad tym teraz już zapanować
   * @external
   * 
   * @global DatabaseManager $pDbMgr
   * @param array $aOrder
   * @param int $iOrderId
   * @return bool
   */
  public function setOrderConfirmed($aOrder, $iOrderId, $bConfirm = true) {
    global $pDbMgr;

    if (empty($aOrder['invoice_id']) || ($aOrder['second_invoice'] == '1' && empty($aOrder['invoice2_id']))) {
      $aValues = array();
      // generowanie numeru faktury
      if (empty($aOrder['invoice_id'])) {
        $aValues['invoice_id'] = $this->getInvoiceNumber($aOrder['bookstore'], $aOrder['order_number'], $aOrder['website_id']);
        $aOrder['invoice_id'] = $aValues['invoice_id'];
      }
      if ($aOrder['second_invoice'] == '1' && empty($aOrder['invoice2_id'])) {
        $aValues['invoice2_id'] = $this->getInvoiceNumber($aOrder['bookstore'], $aOrder['order_number'], $aOrder['website_id']);
        $aOrder['invoice2_id'] = $aValues['invoice2_id'];
      }
      if ($aOrder['order_status'] == '2') {
        if ($bConfirm === true) {
          $aValues['order_status'] = '3';
        }
        $aValues['status_3_update'] = 'NOW()';
        $aValues['status_3_update_by'] = $_SESSION['user']['name'];
      }

      return $pDbMgr->Update('profit24', 'orders', $aValues, ' id = ' . $iOrderId);
    } else {
      return true;
    }
  }
// end of setOrderConfirmed() method

  /**
   * Metoda pobiera powiązane zamówienia z podanym,
   * @external
   * 
   * @global DatabaseManager $pDbMgr
   * @param int $iOrderItemId
   * @return array
   */
  public function getOrdersLinked($iOrderItemId) {
    global $pDbMgr;

    $sSql = 'SELECT O.*
             FROM orders_linked_transport AS OLT
             JOIN orders AS O
              ON O.id = OLT.linked_order_id
             WHERE OLT.main_order_id = ' . $iOrderItemId;
    return $pDbMgr->GetAll('profit24', $sSql);
  }
// end of getOrdersLinked() method

  /**
   * Metoda drukuje grupowo (z $_POST['delete']) i pojedynczo zamówienia do pdf
   * 
   * @param object $pSmarty
   * @param int $iId - id zamówienia
   * @return void
   */
  private function PrintOrders(&$pSmarty, $iPId, $iId) {
    $_GET['hideHeader'] = '1';
    if ($iId > 0) {
      $_POST['delete'][$iId] = '1';
    }
    $iI = 0;
    foreach ($_POST['delete'] as $sKey => $sVal) {
      unset($_POST['delete'][$sKey]);
      $_POST['delete'][$iI] = $sKey;
      $iI++;
    }

    $oPrintMultipleOrdersPDF = new printMultipleOrdersPDF($this->pDbMgr, $pSmarty);
    $oPrintMultipleOrdersPDF->setOrders($_POST['delete']);
    $oPrintMultipleOrdersPDF->getPDF();
  }
// end of PrintOrders() funciton

  /**
   * Pobiera dane adresowe z zamówienai z bazy
   * 
   * @param int $iId - id adresu
   * @return array ref
   */
  private function &getOrderAddresses($iId, $bIgnoreTransportAdress = false) {
    global $aConfig;

    $sSql = "SELECT address_type, is_company, company, name, surname, street, number, number2, postal, city, nip, phone, is_postal_mismatch
						 FROM " . $aConfig['tabls']['prefix'] . "orders_users_addresses 
						 WHERE order_id = " . $iId .
            ($bIgnoreTransportAdress === TRUE ? ' AND address_type="0"' : '') .
            " ORDER BY address_type";
    return Common::GetAssoc($sSql);
  }
// end of getOrderAddresses() method

  /**
   * Metoda pobiera opis użytkownika
   *
   * @global array $aConfig
   * @param integer $iUId
   * @return string 
   */
  private function getUserDescr($iUId) {
    global $aConfig;
    $sSql = "SELECT CONCAT(email, ' (',name,' ',surname,')')
						FROM " . $aConfig['tabls']['prefix'] . "users_accounts
						WHERE id = " . $iUId;
    return Common::GetOne($sSql);
  }
// end of getUserDescr() method

  /**
   * Metoda sprawdza czy kod pocztowy pasuje do miasta
   *
   * @global array $aConfig
   * @param string $sCity
   * @param string $sPostal
   * @return bool 
   */
  public function checkCityPostal($sCity, $sPostal) {
    global $aConfig;

    $sSql = "SELECT count(1)
						FROM " . $aConfig['tabls']['prefix'] . "users_accounts_postals
						WHERE city = " . Common::Quote(stripslashes($sCity)) .
            " AND postal = " . Common::Quote(stripslashes($sPostal));
    return (intval(Common::GetOne($sSql)) > 0);
  }
// end of checkCityPostal() method

  /**
   * Metoda łączy dwa zamówienia w jedno
   *
   * @param		object	$pSmarty
   * @param	int $iPId
   * @param bool $iId
   * @return	void
   */
  private function ConnectOrderTo(&$pSmarty, $iPId, $iId) {
    global $aConfig, $pDbMgr;
    $bIsErr = false;

    $oLogger = new logger($this->pDbMgr);
    $bBalanced = false;
    $reservationManager = new ReservationManager($pDbMgr);
    if ($_POST['connect_to'] > 0) {
      $aNewOrderValues = array();
      $aMainOrder = $this->getOrderData($iId);
      $aConnectedOrder = $this->getOrderData($_POST['connect_to']);
      $beforeConnectedReservation = $reservationManager->getDataProvider()->getReservationsByOrderId($aConnectedOrder['id']);
      $beforeConnectedReservation = ArrayHelper::toKeyValues('id', $beforeConnectedReservation);

      // pobieramy widok zamówienia,
      $sHTML = $this->Details($pSmarty, $iPId, $_POST['connect_to'], FALSE);
      $this->_addToOrdersDeleted($iId, $aConnectedOrder['order_number'], $sHTML);

      // pobieramy widok zamówienia,
      $sHTML = $this->Details($pSmarty, $iPId, $iId, FALSE);
      $this->_addToOrdersDeleted($iId, $aMainOrder['order_number'], $sHTML);


      $aNewOrderValues['paid_amount'] = $aMainOrder['paid_amount'] + $aConnectedOrder['paid_amount'];    //wartosc oplacono
      $aNewOrderValues['balance'] = ($aNewOrderValues['paid_amount'] - ($aMainOrder['to_pay'] + $aConnectedOrder['to_pay'])); //balans poprawiony
      $aNewOrderValues['invoice_ready'] = '';    //skasowanie faktury w bazie

      if ($aNewOrderValues['balance'] == 0.00) {
        // wartość zamówienia opłacona na tą samą kwotę co do zapłaty
        $bBalanced = true;
      }

      Common::BeginTransaction();
      $aValues['order_id'] = $iId;

      $beforeReservation = $reservationManager->getDataProvider()->getReservationsByOrderId($iId);
      $beforeReservation = ArrayHelper::toKeyValues('id', $beforeReservation);

      $testSql = "SELECT * FROM products_stock_reservations WHERE order_id = 296186";

      //przepisanie ksiazek
      if (Common::Update($aConfig['tabls']['prefix'] . "orders_items", $aValues, " deleted='0' AND order_id = " . $_POST['connect_to']) === false) {
        $bIsErr = true;
          echo '1';
      }

      //przpeisanie platnosci
      if (Common::Update($aConfig['tabls']['prefix'] . "orders_payments_list", $aValues, " order_id = " . $_POST['connect_to']) === false) {
        $bIsErr = true;
          echo '2';
      }

      //przpeisanie platnosci
      if (Common::Update($aConfig['tabls']['prefix'] . "orders_linking_transport_numbers", $aValues, " order_id = " . $_POST['connect_to']) === false) {
        $bIsErr = true;
          echo '3';
      }

      if (Common::Update($aConfig['tabls']['prefix'] . "orders_platnoscipl_sessions", $aValues, " order_id = " . $_POST['connect_to']) === false) {
        $bIsErr = true;
          echo '4';
      }

      $aNewOrderValues2['payment'] = $aMainOrder['payment'];
      $aNewOrderValues2['payment_type'] = $aMainOrder['payment_type'];
      $aNewOrderValues2['payment_id'] = $aMainOrder['payment_id'];
      $aNewOrderValues2['payment_date'] = $aMainOrder['payment_date'];
      $aNewOrderValues2['payment_status'] = ($bBalanced == true ? '1' : $aMainOrder['payment_status']); // jeśli zamówienia zostały opłacone to główna metoda płatności też ma być opłacona
      $aNewOrderValues2['second_payment'] = $aConnectedOrder['payment'];
      $aNewOrderValues2['second_payment_type'] = $aConnectedOrder['payment_type'];
      $aNewOrderValues2['second_payment_id'] = $aConnectedOrder['payment_id'];
      $aNewOrderValues2['second_payment_date'] = $aConnectedOrder['payment_date'];
      $aNewOrderValues2['second_payment_status'] = ($bBalanced == true ? '1' : $aConnectedOrder['payment_status']);
      $aNewOrderValues2['second_payment_enabled'] = '1';

      if (($aMainOrder['paid_amount'] < $aMainOrder['to_pay'] && $aMainOrder['paid_amount'] == '1') ||
              ($aConnectedOrder['paid_amount'] < $aConnectedOrder['to_pay'] && $aConnectedOrder['paid_amount'] == '1')) {
        //jeśli któreś z zamówień posiada niedopłatę
        //pozostawiamy niedopłatę - istnieje możlwiość że jest w trakcie opłacania lub po prostu klikniemy później opcję
      } elseif ($aMainOrder['payment_type'] != $aConnectedOrder['payment_type']) {
        //jeśli sposoby wysyłki różnią się	
        //jeśli jedno z nich jest za pobraniem to ustalamy payment2 za pobraniem a payment1 takie jakie było
        if ($aMainOrder['payment_type'] == 'postal_fee') {

          //ustalamy jako 2 sposob wysylki za pobraniem - on wygeneruje się na liscie przewozowym
          $aNewOrderValues2['second_payment'] = $aMainOrder['payment'];
          $aNewOrderValues2['second_payment_type'] = $aMainOrder['payment_type'];
          $aNewOrderValues2['second_payment_id'] = $aMainOrder['payment_id'];
          $aNewOrderValues2['second_payment_date'] = $aMainOrder['payment_date'];
          $aNewOrderValues2['second_payment_status'] = ($bBalanced == true ? '1' : $aMainOrder['payment_status']);
          $aNewOrderValues2['second_payment_enabled'] = '1';
          $aNewOrderValues2['payment'] = $aConnectedOrder['payment'];
          $aNewOrderValues2['payment_type'] = $aConnectedOrder['payment_type'];
          $aNewOrderValues2['payment_id'] = $aConnectedOrder['payment_id'];
          $aNewOrderValues2['payment_date'] = $aConnectedOrder['payment_date'];
          $aNewOrderValues2['payment_status'] = $aConnectedOrder['payment_status'];
        } elseif ($aConnectedOrder['payment_type'] == 'postal_fee') {
          //to 2 jest za pobraniem pierwszy zostaje a drugi dopisujemy	
          $aNewOrderValues2['second_payment'] = $aConnectedOrder['payment'];
          $aNewOrderValues2['second_payment_type'] = $aConnectedOrder['payment_type'];
          $aNewOrderValues2['second_payment_id'] = $aConnectedOrder['payment_id'];
          $aNewOrderValues2['second_payment_date'] = $aConnectedOrder['payment_date'];
          $aNewOrderValues2['second_payment_status'] = ($bBalanced == true ? '1' : $aConnectedOrder['payment_status']);
          $aNewOrderValues2['second_payment_enabled'] = '1';
        } else {
          //po prostu są różne zapisujemy obie formy płatności	
          $aNewOrderValues2['second_payment'] = $aConnectedOrder['payment'];
          $aNewOrderValues2['second_payment_type'] = $aConnectedOrder['payment_type'];
          $aNewOrderValues2['second_payment_id'] = $aConnectedOrder['payment_id'];
          $aNewOrderValues2['second_payment_date'] = $aConnectedOrder['payment_date'];
          $aNewOrderValues2['second_payment_status'] = ($bBalanced == true ? '1' : $aConnectedOrder['payment_status']);
          $aNewOrderValues2['second_payment_enabled'] = '1';
        }
      } else {
        //jeśli jest ta sama forma wysyłki to nie zmieniamy nic
      }

      $aNewOrderValues['linked_order'] = '0';
      //aktualizujemy zamowienia
      $oLogger->addLog($iId, $_SESSION['user']['id'], $aNewOrderValues);
      if (Common::Update($aConfig['tabls']['prefix'] . "orders", $aNewOrderValues, " id = " . $iId) === false) {
        $bIsErr = true;
          echo '5';
      }

      //przeliczamy zamówienie
      if ($this->recountOrder($iId, $aNewOrderValues['payment_id'] > 0 ? $aNewOrderValues['payment_id'] : $aMainOrder['payment_id']) === false) {
        $bIsErr = true;
          echo '6';
      }

      //aktualizujemy transporty
      $oLogger->addLog($iId, $_SESSION['user']['id'], $aNewOrderValues2);
      if (Common::Update($aConfig['tabls']['prefix'] . "orders", $aNewOrderValues2, " id = " . $iId) === false) {
        $bIsErr = true;
          echo '7';
      }

      $aValuesReservations = array(
          'order_id' => $iId
      );
      if (Common::Update($aConfig['tabls']['prefix'] . "products_stock_reservations", $aValuesReservations, " order_id = " . $_POST['connect_to']) === false) {
        $bIsErr = true;
          echo '8';
      }

      // Wywalamy dla pewności te rezerwacje
      $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products_stock_reservations
								 WHERE order_id = " . $_POST['connect_to'] . "";
      if (Common::Query($sSql) === false) {
        $bIsErr = true;
          echo '9';
      }
      $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "orders
								 WHERE id =" . $_POST['connect_to'] . "";
      if (Common::Query($sSql) === false) {
        $bIsErr = true;
          echo '10';
      }
    } else {
      $sMsg = sprintf($aConfig['lang'][$this->sModule]['connect_to_mustselect']);
      $this->sMsg .= GetMessage($sMsg);
      $this->Details($pSmarty, $iPId, $iId);
      return false;
    }

      if(false == $bIsErr){
          try{
              $reservationManager->removeOrder($aConnectedOrder, $beforeConnectedReservation, false);
              $reservationResult = $reservationManager->createStreamsoftReservation($beforeReservation, $this->iOrderId);

              // zamowienie czesciowe, wyswielamy info
              if(false === $reservationResult){
                  $this->sMsg .= GetMessage($reservationManager->getErrorMessage());
              }
          } catch(\Exception $e) {
              $bIsErr = true;
              echo '11';
          }
      }

    if ($bIsErr) {
      // wystąpił błąd
      Common::RollbackTransaction();
      $reservationManager->rollbackChanges($this->iOrderId);

      //tworzymy nowy obiekt zeby nie namieszac, cofamy rezerwacje z wsysanego zamowienia
      $connectedDataReservationManager = new ReservationManager($pDbMgr);
      $connectedDataReservationManager
          ->setOldReservations($beforeConnectedReservation)
          ->setOrderData($aConnectedOrder)
          ->rollbackChanges($aConnectedOrder['id']);

      $sMsg = sprintf($aConfig['lang'][$this->sModule]['connect_to_err']);
      $this->sMsg .= GetMessage($sMsg);
      AddLog($sMsg);
      $this->Details($pSmarty, $iPId, $iId);
    } else {
      // zatwierdzamy
      Common::CommitTransaction();

      //kasujemy fakturę i proforma
      $aOrder['invoice_id'] = Common::GetOne('SELECT invoice_id FROM ' . $aConfig['tabls']['prefix'] . 'orders WHERE id=' . $iId);
      $fName = 'faktura' . str_replace("/", "_", $aOrder['invoice_id']) . '.pdf';
      $sFile = $aConfig['common']['base_path'] . $aConfig['common']['invoice_dir'] . $fName;

      if (file_exists($sFile)) {
        unlink($sFile);
      }

      $fName = 'faktura_proforma' . str_replace("/", "_", $aOrder['order_number']) . '.pdf';
      $sFile = $aConfig['common']['base_path'] . $aConfig['common']['pro_forma_invoice_dir'] . $fName;

      if (file_exists($sFile)) {
        unlink($sFile);
      }

      $sMsg = sprintf($aConfig['lang'][$this->sModule]['connect_to_ok']);
      $this->sMsg .= GetMessage($sMsg, false);
      $this->Details($pSmarty, $iPId, $iId);
    }
  }
// end of ConnectOrderTo() function

  /**
   * Metoda dodaje zamówienie usunięte
   * 
   * @param int $iOrderId
   * @param string $sOrderNumber
   * @param string $sHTML
   * @return bool
   */
  private function _addToOrdersDeleted($iOrderId, $sOrderNumber, $sHTML) {

    $aValues = array(
        'order_id' => $iOrderId,
        'order_number' => $sOrderNumber,
        'content' => $sHTML
    );
    return Common::Insert('orders_deleted', $aValues);
  }
// end of _addToOrdersDeleted() method

  /**
   * Metoda czyści ze spacji tablicę
   *
   * @author akarmenia at gmail dot com http://www.php.net/manual/en/function.trim.php#103278
   * @param array $array
   * @return array
   */
  private function trim_r($array) {
    if (is_string($array)) {
      return trim($array);
    } else if (!is_array($array)) {
      return '';
    }
    $keys = array_keys($array);
    for ($i = 0; $i < count($keys); $i++) {
      $key = $keys[$i];
      if (is_array($array[$key])) {
        $array[$key] = $this->trim_r($array[$key]);
      } else if (is_string($array[$key])) {
        $array[$key] = trim($array[$key]);
      }
    }
    return $array;
  }
// end of trim_r() method

  /**
   * Metoda pobiera informacje dodatkowe o zamówieniu
   *
   * @global array $aConfig
   * @param type $iId
   * @return type 
   */
  private function getAdditionalInfoOrder($iId) {
    global $aConfig;

    $sSql = "SELECT *
						FROM " . $aConfig['tabls']['prefix'] . "orders_additional_info
						 WHERE order_id = " . $iId . ' ORDER BY created DESC;';
    return Common::GetAll($sSql);
  }
// end of getAdditionalInfoOrder() method

  /**
   * Metoda pobiera limit dla danego pierwszego źródła
   *
   * @param type $iProdId 
   */
  private function getSourceDiscountLimit($iProdId) {
    global $aConfig;

    $sSql = "SELECT B.discount_limit 
						 FROM " . $aConfig['tabls']['prefix'] . "products AS A
						 JOIN " . $aConfig['tabls']['prefix'] . "products_source_discount AS B
							 ON A.created_by=B.first_source
						 WHERE A.id=" . $iProdId;
    $fRet = Common::GetOne($sSql);
    if (!isset($fRet) || is_null($fRet)) {
      $fRet = 0.00;
    }
    return $fRet;
  }
// end of getSourceDiscountLimit() method

  /**
   * Metoda pobera listę serwisow dla SELECT'a
   *
   * @global array $aConfig
   * @return array
   */
  private function getWebsitesList() {
    global $aConfig;

    $sSql = "SELECT name AS label, id AS value
						 FROM " . $aConfig['tabls']['prefix'] . "websites";
    return Common::GetAll($sSql);
  }
// end of getWebsitesList() method

  /**
   * Metoda pobiera symbol serwisu na podstawie id serwisu
   *
   * @global type $aConfig
   * @param type $iWebsiteId
   * @return type 
   */
  private function getWebsiteSymbol($iWebsiteId) {
    global $aConfig;

    $sSql = "SELECT code FROM " . $aConfig['tabls']['prefix'] . "websites
						 WHERE id=" . $iWebsiteId;
    return Common::GetOne($sSql);
  }
// end of getWebsiteSymbol() method

  /**
   * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
   *
   * @global array $aConfig
   * @param int $iOId
   * @param bool $bRemember
   * @return type 
   */
  private function getWebsiteId($iOId, $bRemember = true) {
    global $aConfig;

    if ($bRemember) {
      if ($this->iWebsiteId <= 0) {
        $sSql = "SELECT website_id FROM " . $aConfig['tabls']['prefix'] . "orders
								WHERE id=" . $iOId;
        $this->iWebsiteId = Common::GetOne($sSql);
        return $this->iWebsiteId;
      }
    } else {
      $sSql = "SELECT website_id FROM " . $aConfig['tabls']['prefix'] . "orders
								WHERE id=" . $iOId;
      return Common::GetOne($sSql);
    }
  }
// end of getWebsiteId() method

  /**
   * Metoda pobiera prostą listę komentarzy
   * 
   * @param type $iUId
   * @return type
   */
  public function getSimpleCommentsList($iUId) {

    $sSql = 'SELECT comment, created, created_by
             FROM users_accounts_comments
             WHERE user_id = ' . $iUId . "
             ORDER BY id DESC";
    return Common::GetAll($sSql);
  }
// end of getSimpleCommentsList() method

  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param Smarty $pSmarty
   * @param int $iOrderId
   */
  private function AutoSources($pSmarty, $iOrderId) {
    global $pDbMgr;

    $oAutoChangeStatus = new autoProviderToOrders(false, $pDbMgr);

    $aOrdersItems = $oAutoChangeStatus->getSingleOrderData($iOrderId);
    if (!empty($aOrdersItems)) {

      try {

        $aOrdersItems = [
          $iOrderId => $aOrdersItems
        ];

        $oAutoChangeStatus->UpdateOrdersItemsAndReservations($aOrdersItems, true);
      } catch (\Exception $e) {
        $this->sMsg .= GetMessage($e->getMessage());
        return $this->Show($pSmarty);
      }

      $sMsg = sprintf(_('Zamówienie o id %s zostało odznaczone'), $iOrderId);
      AddLog($sMsg, false);
      $this->sMsg .= GetMessage($sMsg, false);

    } else {
      $this->sMsg = sprintf(_('Brak produktow do obrobienia w zamówieniu o id %s'), $iOrderId);
    }

    $this->Show($pSmarty);
  }
// end of AutoSources() method
  
  /**
   * Metoda sprawdza saldo w zamówieniu
   * @param array $aItem
   * @return boolean
   */
  private function checkNeedReset($aItem) {

    if ($aItem['balance'] == 0 || $aItem['balance'] > 0) {
      return false;
    } else {
      return true;
    }
  }

  /**
   * Metoda wyrównuje balans w zamówieniu
   * @param type $iId
   */
  private function ResetBalance($pSmarty, $iId) {

    $sSql = "SELECT to_pay, order_number, balance 
            FROM orders
            WHERE id = " . $iId;

    $aRow = $this->pDbMgr->GetRow('profit24', $sSql);
    $fToPay = $aRow['to_pay'];
    $sOrderNumber = $aRow['order_number'];
    $fBalance = $aRow['balance'];


    if ($this->ChangeUnderPayment($iId, $fBalance, $fToPay, $sOrderNumber) === FALSE) {
      $sMsg .= sprintf(_('Nie udało się wyrównać salda zamówienia %s'), $sOrderNumber);
      $this->sMsg .= GetMessage($sMsg);
    } else {
      $sMsg .= sprintf(_('Pomyślnie wyrównano saldo zamówienia %s'), $sOrderNumber);
      $this->sMsg .= GetMessage($sMsg, false);
    }


    $this->Show($pSmarty);
  }
// end of ResetBalance() function

  /**
   * Metoda zmienia saldo, zbilansowuje zamówienie
   * @param integer $iOrderId
   * @param float $fBalance
   * @return boolean
   */
  private function ChangeUnderPayment($iOrderId, $fBalance, $fToPay, $sOrderNumber) {

    $this->pDbMgr->BeginTransaction('profit24');
    $aValuesList = array(
        'balance' => 0,
        'paid_amount' => $fToPay
    );
    if ($this->pDbMgr->Update('profit24', 'orders', $aValuesList, 'id = ' . $iOrderId) === FALSE) {
      $this->pDbMgr->RollbackTransaction('profi24');
      return FALSE;
    }

    if ($this->AddBalancedPayment($iOrderId, $fBalance, $sOrderNumber) === FAlSE) {
      $this->pDbMgr->RollbackTransaction('profi24');
      return FALSE;
    }

    $this->pDbMgr->CommitTransaction('profit24');
    return TRUE;
  }

  /**
   * Metoda dodaje rekord do listy płatności na niedopłatę 
   * @param type $iOrderId
   * @param type $fAmount
   * @param type $sOrderNumber
   * @retun boolean
   */
  private function AddBalancedPayment($iOrderId, $fAmount, $sOrderNumber) {

    $aValues = array(
        'order_id' => $iOrderId,
        'date' => 'NOW()',
        'ammount' => $fAmount * (-1),
        'type' => 'PW-IN',
        'title' => 'Zbilansowanie zamówienia ' . $sOrderNumber,
        'matched_by' => $_SESSION['user']['name']
    );

    return $this->pDbMgr->Insert('profit24', 'orders_payments_list', $aValues);
  }
// end of AddBalancedPayment function

  private function renewReservation($iId, $pSmarty)
  {
      $reservationRenewer = new ReservationRenewer($this->pDbMgr, new ReservationManager($this->pDbMgr), new DataProvider($this->pDbMgr));
      $result = $reservationRenewer->repairSingleOrderReservations($iId);

      if (false == $result){
        $this->sMsg .= GetMessage("Wystąpił błąd podczas odtwarzania rzerwacji");
      } else if(true == is_string($result)){
        $this->sMsg .= GetMessage($result);
      } else {
        $this->sMsg .= GetMessage("Rezerwacje zostały odbudowane", false);
      }

      $this->Show($pSmarty);
  }
}// end of Module Class