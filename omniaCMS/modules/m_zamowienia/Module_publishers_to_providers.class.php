<?php
/**
 * Klasa zarządzania mapowaniem wydawcy do źródeł danych (dostawców)
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-09-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__zamowienia__publishers_to_providers {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;
  
  function __construct(&$pSmarty) {
    global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iProviderId = NULL;
    $iPublisherId = NULL;
		//$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'opisy_statusow');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}
		if (isset($_GET['pid']) && $_GET['pid'] > 0) {
			$iProviderId = intval($_GET['pid']);
		}
    
		if (isset($_GET['ppid']) && $_GET['ppid'] > 0) {
			$iPublisherId = intval($_GET['ppid']);
		}
    
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
      case 'delete': $this->Delete($pSmarty, $iId, $iProviderId, $iPublisherId); break;
      default: case 'publishers_list': $this->getPublishersList($pSmarty, $iProviderId, $iPublisherId); break;
      case 'add':
        if (isset($iProviderId) && $iProviderId > 0) {
          $this->AddProviderMapping($pSmarty, $iProviderId);
        } else {
          $this->AddPublisherMapping($pSmarty, $iPublisherId);
        }
      break;
      case 'insert':
        $this->Insert($pSmarty, $iProviderId, $iPublisherId);
      break;
			case 'sort': 
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show('SORTUJEMY',
																			 $this->_getRecords($iProviderId, $iPublisherId),
																			 phpSelf(array('do'=>'proceed_sort'),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
        if ($iProviderId > 0) {
          $aTMP = array(
            'provider_id' => $iProviderId,
          );
        } elseif ($iPublisherId > 0) {
          $aTMP = array(
            'publisher_id' => $iPublisherId,
          );
        } else {
          die('Błąd');
        }
				$oSort->Proceed($aConfig['tabls']['prefix']."publishers_to_providers",
												$this->_getRecords($iProviderId, $iPublisherId),
												$aTMP);
			break;
		}
  }// end of __construct() method
  
  
  /**
   * Metoda pobiera listę rekordów do posorotwania
   * 
   * @param int $iProviderId
   * @param int $iPublisherId
   * @return array
   */
  private function _getRecords($iProviderId = NULL, $iPublisherId = NULL) {
    
    $sSql = "SELECT PTP.id, CONCAT(EP.name, ' -> ', PP.name) AS value
         FROM publishers_to_providers AS PTP
         JOIN products_publishers AS PP
          ON PP.id = PTP.publisher_id
         JOIN external_providers AS EP
          ON EP.id = PTP.provider_id
         WHERE 1 = 1 ".
             (isset($iProviderId) ? " AND PTP.provider_id = ".$iProviderId : '').
             (isset($iPublisherId) ? " AND PTP.publisher_id = ".$iPublisherId : '').'
         ORDER BY PTP.order_by';
    return Common::GetAssoc($sSql);
  }// end of _getRecords() method
  

  /**
   * Metoda dodaje mapowanie wydawcy z dostawcą
   * 
   * @global array $aConfig
   * @param object $pSmarty
   * @param int $iProviderId
   * @param int $iPublisherId
   * @return void
   */
  public function Insert($pSmarty, $iProviderId = NULL, $iPublisherId = NULL) {
    global $aConfig;
    $aValues = array();
    
    
    if (isset($_POST['provider_id']) && $_POST['provider_id'] > 0) {
      // dodawanie wydawcy
      $sSql = "SELECT id
             FROM " . $aConfig['tabls']['prefix'] . "products_publishers 
             WHERE name LIKE " . Common::Quote($_POST['publisher']);
      $iPublisherIdTMP = (int) Common::GetOne($sSql);
      if ($iPublisherIdTMP > 0) {
        $aValues['publisher_id'] = $iPublisherIdTMP;
        $aValues['provider_id'] = $iProviderId;
      }
      
    } elseif (isset($_POST['publisher_id']) && $_POST['publisher_id'] > 0) {
      // dodawanie wydawcy
      $sSql = "SELECT id
             FROM " . $aConfig['tabls']['prefix'] . "external_providers 
             WHERE id = " . Common::Quote($_POST['provider']);
      $iProviderIdTMP = (int) Common::GetOne($sSql);
      if ($iProviderIdTMP > 0) {
        $aValues['provider_id'] = $iProviderIdTMP;
        $aValues['publisher_id'] = $iPublisherId;
      }
    }
    
    if (!empty($aValues)) {
      $aValues['created'] = 'NOW()';
      $aValues['order_by'] = '#order_by#';
      $aValues['created_by'] = $_SESSION['user']['name'];
      if (Common::Insert('publishers_to_providers', $aValues, '', false) === false) {
        // błąd
        $this->sMsg = _(sprintf('Wystąpił błąd podczas dodawania mapowania wydawcy "%s" z dostawcą "%s"', $this->_getProviderName($aValues['provider_id']), $this->_getPublisherName($aValues['publisher_id'])));
        AddLog($this->sMsg);
        $this->sMsg = GetMessage($this->sMsg, TRUE);
        return $this->getPublishersList($pSmarty, $iProviderId, $iPublisherId);
      }
    } else {
        // błąd
        $this->sMsg = _(sprintf('Wystąpił błąd - podczas dodawania mapowania wydawcy z dostawcą, podano nieprawidłowego wydawcę lub dostawcę'));
        AddLog($this->sMsg);
        $this->sMsg = GetMessage($this->sMsg, TRUE);
        return $this->getPublishersList($pSmarty, $iProviderId, $iPublisherId);
    }
    $this->sMsg = _(sprintf('Dodano mapowanie wydawcy "%s" z dostawcą "%s"', $this->_getProviderName($aValues['provider_id']), $this->_getPublisherName($aValues['publisher_id'])));
    AddLog($this->sMsg);
    $this->sMsg = GetMessage($this->sMsg, FALSE);
    return $this->getPublishersList($pSmarty, $iProviderId, $iPublisherId);
  }// end of Insert() method
  
  
  /**
   * Metoda wyświetla formularz dodawania mapowania dostawcy
   * 
   * @param type $pSmarty
   * @param type $iProviderId
   * @param type $iPublisherId
   */
  public function AddProviderMapping(&$pSmarty, $iProviderId = NULL) {
    global $aConfig;

		$aData = array();

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if (!empty($_POST)) {
			$aData =& $_POST;
		}

    $sHeader = '';
    if (isset($iProviderId)) {
      $sHeader = sprintf($aConfig['lang'][$this->sModule]['add_provider'], $this->_getProviderName($iProviderId));
    } else {
      die('Błąd !!! ');
    }
    
    $pForm = new FormTable('add_mapping', $sHeader, array('enctype'=>'multipart/form-data', 'action'=>phpSelf(array())), array('col_width'=>350), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'insert');
    
    $pForm->AddHidden('provider_id', $iProviderId);
    
		// wydawca
    $pForm->AddText('publisher', $aConfig['lang'][$this->sModule]['list_publisher_name'], '', array('id' => 'publisher_aci', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', true);
    
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_' . ($iId > 0 ? 1 : 0)], array('onclick' => 'selectExtraCategories();')) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => strpos($_SERVER['HTTP_REFERER'], 'mismatchvat') ? 'window.location.href=\'?frame=main&module_id=26&module=m_oferta_produktowa&action=mismatchvat\'' : 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));

    $sJS = '
		<script type="text/javascript">
      $(document).ready(function(){
        $("#publisher_aci").autocomplete({
                  source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetPublishers.php"
        });      
      });
    </script>
    ';
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sJS . ShowTable($pForm->ShowForm()));
  }// end of AddEdit() method
  
  
  /**
   * Metoda wyświetla formularz dodawania mapowania wydawcy
   * 
   * @param type $pSmarty
   * @param type $iProviderId
   * @param type $iPublisherId
   */
  public function AddPublisherMapping(&$pSmarty, $iPublisherId = NULL) {
    global $aConfig;

		$aData = array();

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

    $sHeader = '';
    if (isset($iPublisherId)) {
      $sHeader = sprintf($aConfig['lang'][$this->sModule]['add_publisher'], $this->_getPublisherName($iPublisherId));
    } else {
      die('Błąd !!! ');
    }
    
    $pForm = new FormTable('add_mapping', $sHeader, array('enctype'=>'multipart/form-data', 'action'=>phpSelf(array())), array('col_width'=>350), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'insert');
    
    $pForm->AddHidden('publisher_id', $iPublisherId);
    
		// wydawca
    $aProviders = addDefaultValue($this->_getProvidersListSelect());
    $pForm->AddSelect('provider', $aConfig['lang'][$this->sModule]['list_provider_name'], '', $aProviders, '');
    
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_0']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sJS . ShowTable($pForm->ShowForm()));
  }// end of AddEdit() method
  
  /**
   * Metoda wybiera dostawców do wybrania
   * 
   * @return array
   */
  private function _getProvidersListSelect() {
    
    $sSql = "SELECT id AS value, name AS label 
             FROM external_providers 
             ORDER BY name";
    return Common::GetAll($sSql);
  }// end of _getProvidersListSelect method
  
  
  /**
   * Metoda wyświetla mapowań wydawców do dostawców
   * 
   * @global type $pDbMgr
   * @global array $aConfig
   * @param object $pSmarty
   * @param int $iProviderId
   * @param int $iPublisherId
   * @return void
   */
  public function getPublishersList(&$pSmarty, $iProviderId = NULL, $iPublisherId = NULL) {
    global $aConfig;
    
    // dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);
    
    $sHeader = '';
    if (isset($iProviderId) && $iProviderId > 0) {
      unset($iPublisherId);
      $sHeader = sprintf($aConfig['lang'][$this->sModule]['list_provider'], $this->_getProviderName($iProviderId));
    } elseif (isset($iPublisherId) && $iPublisherId > 0) {
      unset($iProviderId);
      $sHeader = sprintf($aConfig['lang'][$this->sModule]['list_publisher'], $this->_getPublisherName($iPublisherId));
    } else {
      unset($iProviderId);
      unset($iPublisherId);
      $sHeader = 'Błąd !!! ';
    }

		$aHeader = array(
			'header'	=> $sHeader,
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'publisher_name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_publisher_name'],
				'sortable'	=> true
			),
      array(
				'db_field'	=> 'provider_name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_provider_name'],
				'sortable'	=> true
			),
      array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true
			),
      array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);
    
    // pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(PTP.id)
						 FROM ".$aConfig['tabls']['prefix']."publishers_to_providers AS PTP
             JOIN products_publishers AS PP
              ON PP.id = PTP.publisher_id
             JOIN external_providers AS EP
              ON EP.id = PTP.provider_id
						 WHERE 1 = 1 ".
                   (isset($iProviderId) ? " AND PTP.provider_id = ".$iProviderId : '').
                   (isset($iPublisherId) ? " AND PTP.publisher_id = ".$iPublisherId : '').
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND PP.name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
    
    if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(PTP.id)
							 FROM ".$aConfig['tabls']['prefix']."publishers_to_providers AS PTP
               JOIN products_publishers AS PP
                ON PP.id = PTP.publisher_id
               JOIN external_providers AS EP
                ON EP.id = PTP.provider_id
               WHERE 1 = 1 ".
                   (isset($iProviderId) ? " AND PTP.provider_id = ".$iProviderId : '').
                   (isset($iPublisherId) ? " AND PTP.publisher_id = ".$iPublisherId : '');
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_publishers', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
    
    
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT PTP.id, PP.name as publisher_name, EP.name as provider_name, PTP.created, PTP.created_by
							 FROM ".$aConfig['tabls']['prefix']."publishers_to_providers AS PTP
               JOIN products_publishers AS PP
                ON PP.id = PTP.publisher_id
               JOIN external_providers AS EP
                ON EP.id = PTP.provider_id
							 WHERE 1 = 1 ".
                   (isset($iProviderId) ? " AND PTP.provider_id = ".$iProviderId : '').
                   (isset($iPublisherId) ? " AND PTP.publisher_id = ".$iPublisherId : '').
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND PP.name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' PTP.order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'icon'	=> array(
					'filter'	=> false
				),
				'action' => array (
					'actions'	=> array ('delete'),
					'params' => array (
												'delete'	=> array('id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('sort', 'add')
		);
    if ($iProviderId > 0) {
      $aRecordsFooterParams = array(
          'go_back' => array(0 => array('action' => 'external_providers'))
      );
    } else {
      $aRecordsFooterParams = array(
          'go_back' => array(0 => array('module' => 'm_oferta_produktowa', 'action' => 'publishers'))
      );
    }

		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
  }// end of getPublishersList()
  
  
  /**
   * Metoda pobiera nazwę dostawcy zewnętrznego
   * 
   * @param int $iProviderId
   * @return string
   */
  private function _getProviderName($iProviderId) {
    
    $sSql = "SELECT name FROM external_providers WHERE id = ".$iProviderId;
    return Common::GetOne($sSql);
  }// end of getProviderName method
  
  
  /**
   * Metoda pobiera nazwę wydawnictwa
   * 
   * @param int $iProviderId
   * @return string
   */
  private function _getPublisherName($iPublisherId) {
    
    $sSql = "SELECT name FROM products_publishers WHERE id = ".$iPublisherId;
    return Common::GetOne($sSql);
  }// end of _getPublisherName method
  
  
	/**
	 * Metoda pobiera liste elementów do usuniecia
	 * 
	 * @param	array ref	$aIds	- Id opisu
	 * @return	array ref
	 */
	private function _getItemsToDelete($aIds) {
		global $aConfig;
		$sSql = "SELECT PTP.id, CONCAT(EP.name, ' -> ', PP.name) AS value
             FROM ".$aConfig['tabls']['prefix']."publishers_to_providers AS PTP
              JOIN products_publishers AS PP
               ON PP.id = PTP.publisher_id
              JOIN external_providers AS EP
               ON EP.id = PTP.provider_id
					 	 WHERE PTP.id IN  (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of _getItemsToDelete() method
  
  
	/**
	 * Metoda usuwa elementy
	 * 
	 * @param	integer	$iId	- Id opisu statusu do usuniecia
	 * @return	mixed	
	 */
	private function _deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."publishers_to_providers
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of _deleteItem() method
  
  
	/**
	 * Metoda usuwa elementy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego opisu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId, $iProviderId = NULL, $iPublisherId = NULL) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;
    
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->_getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
			
				// usuwanie
				if (($iRecords = $this->_deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['value'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['value'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->getPublishersList($pSmarty, $iProviderId, $iPublisherId);
	} // end of Delete() funciton
}// end of Class

?>