<?php
/**
 * Klasa Module do obslugi modulu 'Zamowienia - historia wysłanych emaili z zamówieniami'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 *
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig, $pDbMgr;

		$this->sErrMsg = '';
		$this->iLangId =& $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];


		$sDo = '';
		$iId = 0;
		$iPId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
		switch ($sDo) {
			case 'details': 
        $this->echoIsbns($iId);
        $this->showDetails($pSmarty, $iId); 
      break;
			case 'activate': $this->Activate($pSmarty, $iId); break;
			case 'confirm_scanned_order_items': 
        $this->confirmScannedOrderItems($pSmarty, $iId); 
      break;
			case 'cancel': $this->Cancel($pSmarty, $iId); break;
			case 'get_pdf': 
        $this->GetPDF($iId); 
      break;
			case 'get_txt': 
        $this->GetTXT($iId); 
      break;
			case 'get_csv': 
        $this->GetCSV($iId); 
      break;
    
      case 'come_in': $this->PackageComeIn($pSmarty, $iId); break;
			default: $this->Show($pSmarty); break;
		}
	} // end Module() function
  
  /**
   * @global DatabaseManager $pDbMgr
   * @param Smarty $pSmarty
   * @param int $iId
   */
  private function confirmScannedOrderItems($pSmarty, $iId) {
    include_once('Module_go_to_train.class.php');
    $oGoToTrain = new Module__zamowienia__go_to_train($pSmarty, false);
    var_dump($oGoToTrain->confirmOrders($iId));
  }
  
  
  private function echoIsbns($iOSHId) {
    $sSql = "
      SELECT OI.order_id, OI.isbn, OI.name, OI.quantity, OSHI.current_quantity, OSHI.in_confirmed_quantity, OSHI.ordered_quantity
      FROM orders_send_history_items AS OSHI
      JOIN orders_items AS OI
        ON OSHI.item_id = OI.id
      WHERE send_history_id = ".$iOSHId;
    $aData = Common::GetAll($sSql);
    dump($aData);
    echo 'CSV:'."\n";
    foreach ($aData as $aItem) {
      echo implode("\t", $aItem)."\n";
    }
    echo "\n".'<br /><br />';
  }
	
  
  /**
   * Metoda potwierdza przyjazd dostawy na magazyn głowny
   * 
   * @param object $pSmarty
   * @param int $iId
   */
  private function PackageComeIn($pSmarty, $iId) {

    global $pDbMgr;
    $bIsErr = false;
    
    $sSql = 'SELECT post_array, number
             FROM orders_send_history
             WHERE id = '.$iId.' AND magazine_status = "1" ';
    $aOSHData = $pDbMgr->GetRow('profit24', $sSql);
    
    if (!empty($aOSHData)) {
      
      $pDbMgr->BeginTransaction('profit24');
      
      include_once('Module_sc_confirm_hasnc_items.class.php');
      $oModuleConfirm = new Module__zamowienia__sc_confirm_hasnc_items($pSmarty);
      if ($oModuleConfirm->setMagazineStatus($iId, '2') === false) {
        $bIsErr = true;
      }
      
      if ($bIsErr == false) {
        $pDbMgr->CommitTransaction('profit24');
        $sMsg = _('Dostawa o numerze '.$aOSHData['number'].' została przyjęta na magazyn główny.');
        AddLog($sMsg, false);
        $this->sMsg = GetMessage($sMsg, false);
      } else {
        $pDbMgr->RollbackTransaction('profit24');
        $sMsg = _('Wystąpił błąd podczas potwierdzania przyjęcia dostawy numer '.$aOSHData['number'].' na magazyn głowny.');
        AddLog($sMsg, false);
        $this->sMsg = GetMessage($sMsg);
      }
    } else {
      $sMsg = _('Brak dostawy do potwierdzenia.');
      AddLog($sMsg);
      $this->sMsg = GetMessage($sMsg);
    }
    $this->Show($pSmarty);
  }// end of PackageComeIn() method
  
  
	/**
	 * Metoda aktualizuje rabat uzytkownika o podanym Id
	 *
	 * @param		int $iId
	 * @return	void
	 */
	function GetCSV($iId) {
		global $aConfig;
		//dump($_POST); die();
		$bIsErr = false;
		$sFile = '';
		$_GET['hideHeader'] = '1';
		
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="zamowione_pozycje.xls"');
		header("Pragma: no-cache");
		header("Expires: 0");
		$sSql = "SELECT A.content
							 FROM ".$aConfig['tabls']['prefix']."orders_send_history AS A
							 WHERE A.id=".$iId;
		
		$aData =& Common::GetRow($sSql);
		echo $aData['content'];
		
	} // end of GetCSV() funciton
	
	/**
	 * Metoda aktualizuje rabat uzytkownika o podanym Id
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function GetTXT($iId) {
		global $aConfig;
		$bIsErr = false;
		$_GET['hideHeader'] = '1';
		$sFile = '';
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="zamowione_pozycje.txt"');
		header("Pragma: no-cache");
		header("Expires: 0");
		$sSql = "SELECT A.content
							 FROM ".$aConfig['tabls']['prefix']."orders_send_history AS A
							 WHERE A.id=".$iId;
		
		$aData =& Common::GetRow($sSql);
		echo trim(strip_tags($aData['content']));
		//exit();
		
	
	} // end of GetTXT() funciton
	
	/**
	 * Generuje liste pozycji w formacie pdf do pobrania
	 * @return unknown_type
	 */
	function GetPDF($iId) {
		global $aConfig,$pSmarty;
		$bIsErr = false;
		$_GET['hideHeader'] = '1';
		$sFile = '';
		$aBooks = array();
		$sSql = "SELECT A.content
							 FROM ".$aConfig['tabls']['prefix']."orders_send_history AS A
							 WHERE A.id=".$iId;
		
		$aData =& Common::GetRow($sSql);
		$sHtml = $aData['content'];
		
		require_once('OLD_tcpdf/config/lang/pl.php');
			require_once('OLD_tcpdf/tcpdf.php');		
			
			// create new PDF document
			$pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
			
			// set document information
			$pdf->SetCreator(PDF_CREATOR);
			$pdf->SetAuthor('profit24');
			$pdf->SetTitle($aBooks['orders_header']);
			$pdf->SetSubject($aBooks['orders_header']);
			$pdf->SetKeywords('');
			$pdf->setPrintHeader(false);
			
			$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));
			
			// set default monospaced font
			$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
			
			//set margins
			$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
			//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
			
			//set auto page breaks
			$pdf->SetAutoPageBreak(TRUE, 10);
			
			//set image scale factor
			$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
			
			//set some language-dependent strings
			$pdf->setLanguageArray($l); 
			
			// set font
			$pdf->SetFont('freesans', '', 8);
			
			// add a page
			$pdf->AddPage();
			$pdf->writeHTML($sHtml, true, false, false, false, '');
			
			$pdf->Output('lista.pdf', 'D');

//echo($sHtml); die(); 
	} // end of GetPDF() funciton
  
  
  /**
   * Metoda zwraca listę wszystkich źródeł
   * 
   * @return array
   */
  private function _getSources() {
    
    $aStatic = array(
        array('value' => '0', 'label' => 'Profit J'),
        array('value' => '1', 'label' => 'Profit E'),
        array('value' => '5', 'label' => 'Profit M'),
        array('value' => '51', 'label' => 'Stock'),
        array('value' => '52', 'label' => 'Profit X'),
    );
    $sSql = "SELECT name AS label, id AS value
             FROM external_providers ORDER BY label ASC";
    $aExtProv = Common::GetAll($sSql);
    $aProv = array_merge($aStatic, $aExtProv);

	  function compareElems($elem1, $elem2) {
		  return strcmp($elem1['label'], $elem2['label']);
	  }

	  uasort($aProv, "compareElems");
    return $aProv;
  }// end of _getSources() method
  
  
  /**
   * Metoda zwraca tablicę wyboru statusu listy
   * 
   * @return array
   */
  private function _getPostStatus() {
    
    $aPostStatus = array(
        array(
            'value' => '1',
            'label' => _('W drodze')
        ),
        array(
            'value' => '0',
            'label' => _('Potwierdzone')
        )
    );
    return $aPostStatus;
  }// end of _getPostStatus() method
  
  
	/**
	 * Metoda wyswietla liste zdefiniowanych kodow Premium SMS
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		$sFilterSql = '';
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		
		// zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['history'],
			'refresh'	=> true,
			'search'	=> true,
			'checkboxes'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'number',
				'content'	=> $aConfig['lang'][$this->sModule]['number'],
				'sortable'	=> true,
				'width' => '150'
			),
			array(
				'db_field'	=> 'pack_number',
				'content'	=> _('Numer skrzyni'),
				'sortable'	=> true,
				'width' => '200'
			),
			array(
				'db_field'	=> 'date_send',
				'content'	=> $aConfig['lang'][$this->sModule]['date_send'],
				'sortable'	=> true,
				'width' => '300'
			),
			array(
				'db_field'	=> 'source',
				'content'	=> $aConfig['lang'][$this->sModule]['source'],
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'saved_type',
				'content'	=> $aConfig['lang'][$this->sModule]['saved_type'],
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'status',
				'content'	=> $aConfig['lang'][$this->sModule]['items_type'],
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'magazine_status',
				'content'	=> _('Status magazynowy'),
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'fv_nr',
				'content'	=> $aConfig['lang'][$this->sModule]['fv_nr'],
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'send_by',
				'content'	=> $aConfig['lang'][$this->sModule]['send_by'],
				'sortable'	=> true,
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
		
		
		// pobranie liczby wszystkich kodow
		$sSql = "SELECT COUNT(id) 
						 FROM ".$aConfig['tabls']['prefix']."orders_send_history
						 WHERE 1 = 1".
                   (isset($_POST['f_magazine_status']) && $_POST['f_magazine_status'] != '' ? ' AND magazine_status = '.$_POST['f_magazine_status'] : '').
                   (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND source = '.$_POST['f_source'] : '').
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND number LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(id) 
							 FROM ".$aConfig['tabls']['prefix']."orders_send_history
							 WHERE 1 = 1".
                     (isset($_POST['f_magazine_status']) && $_POST['f_magazine_status'] != '' ? ' AND magazine_status = '.$_POST['f_magazine_status'] : '').
                     (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND source = '.$_POST['f_source'] : '').
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND number LIKE \'%'.$_POST['search'].'%\'' : '');
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('send_history', $aHeader, $aAttribs);
		
		$pView->AddRecordsHeader($aRecordsHeader);

		// dodanie filtru grupy
		$pView->AddFilter('f_source', $aConfig['lang'][$this->sModule]['f_source'], array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])), $this->_getSources()), $_POST['f_source']);
		
    // dodanie filtru w drodze
		$pView->AddFilter('f_magazine_status', _('Status Mag.'), array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])), $this->_getPostStatus()), $_POST['f_magazine_status']);
    
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			// pobranie zamówień bankowych
			$sSql = "SELECT A.id, A.number, A.pack_number, A.date_send, A.source, A.saved_type, A.status, A.magazine_status, OSHA.fv_nr, A.send_by, (SELECT name FROM ".$aConfig['tabls']['prefix']."external_providers WHERE id=A.source) AS source_name
							 FROM ".$aConfig['tabls']['prefix']."orders_send_history AS A
               LEFT JOIN orders_send_history_attributes AS OSHA
                ON A.id = OSHA.orders_send_history_id
							 WHERE 1 = 1".$sFilterSql.
                     (isset($_POST['f_magazine_status']) && $_POST['f_magazine_status'] != '' ? ' AND A.magazine_status = '.$_POST['f_magazine_status'] : '').
                     (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = '.$_POST['f_source'] : '').
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND number LIKE \'%'.$_POST['search'].'%\'' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' A.date_send DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			foreach($aRecords as $iKey=>$aItem){
        switch ($aItem['magazine_status']) {
          case '0':
            $aRecords[$iKey]['magazine_status'] = _('Nowe');
          break;
          case '1':
            $aRecords[$iKey]['magazine_status'] = _('W drodze');
          break;
          case '2':
            $aRecords[$iKey]['magazine_status'] = _('Przyjęta na magazyn');
          break;
          case '5':
            $aRecords[$iKey]['magazine_status'] = _('Potwierdzona na tramwaju');
          break;
        }
        if ($aItem['magazine_status'] != '1') {
          $aRecords[$iKey]['disabled'] = array('come_in');
        }
        
				$aRecords[$iKey]['source']=in_array($aRecords[$iKey]['source'], array(0,1,5,51,52))?($aConfig['lang'][$this->sModule]['source_'.intval($aRecords[$iKey]['source'])]):$aRecords[$iKey]['source_name'];
				$aRecords[$iKey]['saved_type'] = $aConfig['lang'][$this->sModule]['saved_'.$aItem['saved_type']];
				$aRecords[$iKey]['status'] = $aConfig['lang'][$this->sModule]['status_'.$aItem['status']];
				$aRecords[$iKey]['date_send'] = str_replace(' ', '<br />',$aRecords[$iKey]['date_send']);
        $aRecords[$iKey]['fv_nr'] = '<span style="color: red; font-size: 12px; font-weight: bold;">'.$aItem['fv_nr'].'</span>';
				unset($aRecords[$iKey]['source_name']);
			}
			
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'date_send' => array ('link'	=> phpSelf(array('do' => 'details', 'id' => '{id}') ))
				,
				'number' => array ('link'	=> phpSelf(array('do' => 'details', 'id' => '{id}') ))
				,
				'action' => array (
					'actions'	=> array ('details', 'confirm_scanned_order_items'),
					'params' => array (
                        'come_in'	=> array('id' => '{id}'),
												'details'	=> array('id' => '{id}'),
                        'confirm_scanned_order_items'	=> array('id' => '{id}'),
											),
					'show' => false,
          'icon' => [
              'confirm_scanned_order_items' => 'activate',
              ],
				),
 			);
      if ($_SESSION['user']['type'] != '1') {
        $aColSettings['action']['actions'] = ['details'];
      }
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array()
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function
	
	
	/**
	 * Metoda tworzy formularz z informacjami na temat uzytkownika
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID konta uzytkownika
	 * @return	void
	 */
	function showDetails(&$pSmarty, $iId) {
		global $aConfig;

		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';
    $sStrAddArr = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych szczegolow konta uzytkownika
		$sSql = "SELECT A.id, A.number,  A.saved_type, A.email, A.date_send, A.content, OSHA.fv_nr, OSHA.ident_nr, A.send_by, A.post_array
							 FROM ".$aConfig['tabls']['prefix']."orders_send_history AS A
               LEFT JOIN ".$aConfig['tabls']['prefix']."orders_send_history_attributes AS OSHA
                 ON A.id = OSHA.orders_send_history_id
							 WHERE A.id=".$iId;
		$aData =& Common::GetRow($sSql);
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['details_header_'.$aData['saved_type']],
											 Common::convert($aData['email']));

		$pForm = new FormTable('attributes', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), false);
		// dane wiadomości
		if(!empty($aData['email'])){
			$pForm->AddRow($aLang['details_email'], Common::convert($aData['email']));
		}
    if ($aData['fv_nr'] != '') {
      $pForm->AddRow($aLang['fv_nr'], $aData['fv_nr'], '', array(), array('style'=>'color: red; font-size: 14px; font-weight: bold;'));
    }
		$pForm->AddRow($aLang['number'], $aData['number'], '', array(), array('style'=>'font-weight:bold; font-size:14px;'));
    if ($aData['ident_nr'] != '') {
      $pForm->AddRow($aLang['ident_nr'], $aData['ident_nr']);
    }
		$pForm->AddRow($aLang['details_date_send'], $aData['date_send']);
		$pForm->AddRow($aLang['details_send_by'], Common::convert($aData['send_by']));
		// dane do faktury
		$pForm->AddMergedRow($aLang['details_content_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
    if (!empty($aData['post_array'])) {
      
      $sStrAddArr = str_replace('  ', ' &nbsp;', nl2br(print_r(unserialize($aData['post_array']), true)));
      
      $aPostArray = unserialize($aData['post_array']);
      dump($aPostArray);
    }
    
		$sStr = Common::convert($aData['content']); 
    /**/
    if ($aData['saved_type'] == 0 || $aData['saved_type'] == 1) {
      $aArr = explode("\r\n", $sStr);
      foreach ($aArr as $iKey => $sIt) {
        $aArr[$iKey] = explode("\t", $sIt);
      }

      $sNewStr = '<table>';
      foreach ($aArr as $iPKey => $aIt) {
        $sNewStr .= '<tr>';
        foreach ($aIt as $iKey => $sIt) {
          $sNewStr .= '<td style="font-size:13px; '.($iKey==0 && $iPKey>2?'width:20px;':'').'" >';	
          $sNewStr .= $sIt;
          $sNewStr .= '</td>';	
        }
        $sNewStr .= '</tr>';
        if ($iPKey == 2) {
          $sNewStr .= '</table><table>';
        }
      }
      $sNewStr .= '</table>';
      $sStr = $sNewStr;
    }
    $sAddStyle = '';
    if ($aData['saved_type'] === '4') {
      if (!stristr($sStr, '&gt;')) {
        $sStr = htmlspecialchars($sStr);
      }
      $sStr = str_replace('&gt;&lt;', '&gt;<br />&lt;', $sStr);
      $sAddStyle = 'font-weight: normal;';
    } elseif ($aData['saved_type'] === '5') {
      $sStr = nl2br($sStr);
    }

		/**/
		$pForm->AddMergedRow('<div id="print_content" style="font-size: 13px; text-align: left; '.$sAddStyle.'">'.str_replace('<table', '<table cellspacing="0" ',$sStr).$sStrAddArr.'</div>');
		// przyciski

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button').($aData['saved_type']==2?'':('&nbsp;&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('print', $aConfig['lang'][$this->sModule]['print_next_time'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>($aData['saved_type']==0?'get_csv':($aData['saved_type']==1?'get_txt':'get_pdf')), 'id'=>$aData['id'])).'\');'), 'button'))));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sCss.ShowTable($pForm->ShowForm()));
	} // end of Details() function
} // end of Module Class
?>