<?php
/**
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2008
 * @version   1.0
 *
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// Id strony modulu
	var $iPageId;
	
	// katalog plikow log
	var $sLogsDir;


  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig, $pDB2;

		$this->sErrMsg = '';
		$this->iPageId = getModulePageId('m_zamowienia');
		$this->sModule = $_GET['module'];
		$this->iLangId = $_SESSION['lang']['id'];
		
	
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		
	$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'change_status': $this->ChangeStatus($pSmarty, $iId); 				break;
			case 'details': 			$this->showDetails($pSmarty, $iId); 	break;
			case 'old_view': 			$this->ShowOld($pSmarty); 						break;
			default: 							$this->Show($pSmarty); 								break;
		}
	} // end Module() function

  
  /**
   * Metoda zwraca listę wszystkich źródeł
   * 
   * @return array
   */
  private function _getSources() {
    
    $aStatic = array(
        array('value' => '0', 'label' => 'Profit J'),
        array('value' => '1', 'label' => 'Profit E'),
        array('value' => '5', 'label' => 'Profit M'),
        array('value' => '51', 'label' => 'Stock'),
        array('value' => '52', 'label' => 'Profit X'),
    );
    return $aStatic;
  }// end of _getSources() method
  
  
	/**
	 * Metoda tworzy formularz z informacjami na temat uzytkownika
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID konta uzytkownika
	 * @return	void
	 */
	function showDetails(&$pSmarty, $iId) {
		global $aConfig;

		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych szczegolow konta uzytkownika
		$sSql = "SELECT A.id, A.number, A.saved_type, A.email, A.date_send, A.content, A.send_by, A.source, OSHA.fv_nr, OSHA.ident_nr, 
              (SELECT name FROM ".$aConfig['tabls']['prefix']."external_providers WHERE id=A.source) AS source_name
							 FROM ".$aConfig['tabls']['prefix']."orders_send_history AS A
               LEFT JOIN ".$aConfig['tabls']['prefix']."orders_send_history_attributes AS OSHA
                 ON A.id = OSHA.orders_send_history_id
							 WHERE A.id=".$iId;
		$aData = Common::GetRow($sSql);
		$aData['source']=in_array($aData['source'], array(0,1,5))?($aData['source']==0?'Profit J':($aData['source']=='1'?'Profit E':'Profit M')):$aData['source_name'];
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['details_header']);

		$pForm = new FormTable('ordered_itm', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), false);
		$pForm->AddHidden('do', 'change_status');
		// dane wiadomości
		if(!empty($aData['email'])){
			$pForm->AddRow($aLang['details_email'], Common::convert($aData['email']));
		}
    if (isset($aData['fv_nr']) && $aData['fv_nr'] != '') {
      $pForm->AddRow($aLang['fv_nr'], $aData['fv_nr'], '', array(), array('style'=>'color: red; font-size: 14px; font-weight: bold;'));
    }
		$pForm->AddRow($aLang['number'], $aData['number'], '', array(), array('style'=>'font-weight:bold; font-size:14px;'));
    if (isset($aData['ident_nr']) && $aData['ident_nr'] != '') {
      $pForm->AddRow($aLang['ident_nr'], $aData['ident_nr']);
    }
		$pForm->AddRow($aLang['details_date_send'], $aData['date_send']);
		$pForm->AddRow($aLang['details_send_by'], Common::convert($aData['send_by']));
		$pForm->AddRow($aLang['source'], $aData['source']);
		// dane do faktury
		$pForm->AddMergedRow($aLang['details_content_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		
			// wyszukiwanie ksiazek 
		$aBooks =& $this->getBooksNew($iId);
		if(!empty($aBooks)){
				$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_change_status']));
			}
		
	
		// wyswietlenie listy pasujacych uzytkownikow
		$pForm->AddMergedRow( $this->GetBooksList($pSmarty, $aBooks) );
		if(!empty($aBooks)){
				$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send2', $aConfig['lang'][$this->sModule]['button_change_status']));
			}
		

			
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('style'=>'float:right;','onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Details() function
	
	/**
	 * Metoda wyswietla liste zdefiniowanych kodow Premium SMS
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		$sFilterSql = '';
		
		// dolaczenie klasy View
		include_once('View/View.class.php');
		include_once('Form/FormTable.class.php');
		
		// zapamietanie opcji
		rememberViewState($this->sModule);
		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['history'],
			'refresh'	=> true,
			'search'	=> true,
			'checkboxes'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'number',
				'content'	=> $aConfig['lang'][$this->sModule]['number'],
				'sortable'	=> true,
				'width' => '100'
			),
			array(
				'db_field'	=> 'date_send',
				'content'	=> $aConfig['lang'][$this->sModule]['date_send'],
				'sortable'	=> true,
				'width' => '300'
			),
			array(
				'db_field'	=> 'source',
				'content'	=> $aConfig['lang'][$this->sModule]['source'],
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'saved_type',
				'content'	=> $aConfig['lang'][$this->sModule]['saved_type'],
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'fv_nr',
				'content'	=> $aConfig['lang'][$this->sModule]['fv_nr'],
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'send_by',
				'content'	=> $aConfig['lang'][$this->sModule]['send_by'],
				'sortable'	=> true,
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
		
    $sSql = "SELECT COUNT(DISTINCT A.id)
               FROM orders_send_history AS A
               JOIN orders_send_history_items AS OSHI
                ON OSHI.send_history_id = A.id
               JOIN orders_items AS OI
                ON OSHI.item_id = OI.id AND OI.`status` = '3' AND OI.`deleted` = '0'
               JOIN orders AS O
                ON OI.order_id = O.id AND order_status <> '5'
               WHERE A.status  = '3' ".
		// pobranie liczby wszystkich eksportow
                   (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = '.$_POST['f_source'] : '').
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND O.email LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(DISTINCT A.id)
               FROM orders_send_history AS A
               JOIN orders_send_history_items AS OSHI
                ON OSHI.send_history_id = A.id
               JOIN orders_items AS OI
                ON OSHI.item_id = OI.id AND OI.`status` = '3' AND OI.`deleted` = '0'
               JOIN orders AS O
                ON OI.order_id = O.id AND order_status <> '5'
               WHERE A.status  = '3'";
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('send_history', $aHeader, $aAttribs);
		
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// dodanie filtru grupy
		$pView->AddFilter('f_source', $aConfig['lang'][$this->sModule]['f_source'], array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])), $this->_getSources()), $_POST['f_source']);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			// pobranie zamówień bankowych
      $sSql = "SELECT DISTINCT A.id, A.number, A.date_send, A.source, A.saved_type, A.status, OSHA.fv_nr, A.send_by, 
                  (SELECT name FROM ".$aConfig['tabls']['prefix']."external_providers WHERE id=A.source) AS source_name 
               FROM orders_send_history AS A
               LEFT JOIN orders_send_history_attributes AS OSHA
                ON A.id = OSHA.orders_send_history_id
               JOIN orders_send_history_items AS OSHI
                ON OSHI.send_history_id = A.id
               JOIN orders_items AS OI
                ON OSHI.item_id = OI.id AND OI.`status` = '3' AND OI.`deleted` = '0'
               JOIN orders AS O
                ON OI.order_id = O.id AND order_status <> '5'
               WHERE A.status  = '3'
              "
              .$sFilterSql.
                     (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = '.$_POST['f_source'] : '').
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND O.email LIKE \'%'.$_POST['search'].'%\'' : '').
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' A.date_send DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			//dump($aRecords);
			foreach($aRecords as $iKey=>$aItem){
				//$aRecords[$iKey]['source']='dd';
				$aRecords[$iKey]['saved_type'] = $aConfig['lang'][$this->sModule]['saved_'.$aItem['saved_type']];
				//$aRecords[$iKey]['status'] = $aConfig['lang'][$this->sModule]['status_'.$aItem['status']];
				$aRecords[$iKey]['source']=in_array($aRecords[$iKey]['source'], array(0,1,5,51,52))?($aConfig['lang'][$this->sModule]['source_'.intval($aRecords[$iKey]['source'])]):$aRecords[$iKey]['source_name'];
				$aRecords[$iKey]['date_send']=str_replace(' ', '<br />', $aRecords[$iKey]['date_send']);
        $aRecords[$iKey]['fv_nr'] = '<span style="color: red; font-size: 12px; font-weight: bold;">'.$aItem['fv_nr'].'</span>';
			}
			
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'status'	=> array(
					'show'	=> false
				),
				'source_name'	=> array(
					'show'	=> false
				),
				'date_send' => array ('link'	=> phpSelf(array('do' => 'details', 'id' => '{id}') ))
				,
				
				'number' => array ('link'	=> phpSelf(array('do' => 'details', 'id' => '{id}') )),
				
				'action' => array (
					'actions'	=> array ('details'),
					'params' => array (
												'details'	=> array('id' => '{id}')
											),
					'show' => false	
				) 
 			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array()
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		//przycisk przejdz do starego widoku
		
		$sHeader = $aConfig['lang'][$this->sModule]['ordered_items_header'].($_POST['source'] != ''?' - '.$aConfig['lang'][$this->sModule]['source_'.$_POST['source']]:'');
	
		$pForm = new FormTable('old_view_button', '', array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'old_view');
		$pForm->AddRow('&nbsp;', '<div style="float:right;">'.
									$pForm->GetInputButtonHTML('get_csv', $aConfig['lang'][$this->sModule]['old_view'], array('onclick'=>'this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button'));
		
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show().ShowTable($pForm->ShowForm()));
																				
	} // end of Show() function
	
	
	/**
	 * Metoda tworzy formularz edycji rabatu grupowego
	 * dla uzytkownikow, ktorzy spelniaja okreslone zalozenia:
	 * przedzial wartosci zlozoych dotychczas zamowien, wartosc aktualnego
	 * rabatu
	 *
	 * @param	object	$pSmarty
	 * @param	bool	$bSearch	- true: wyswietl uzytkownikow
	 */
	function ShowOld(&$pSmarty) {
		global $aConfig;
		$sHtml = '';


			// wyszukiwanie ksiazek 
			$aBooks =& $this->getBooks();

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = $aConfig['lang'][$this->sModule]['ordered_items_header'].($_POST['source'] != ''?' - '.$aConfig['lang'][$this->sModule]['source_'.$_POST['source']]:'');
	
		$pForm = new FormTable('ordered_itm', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'change_status');
				
		// przycisk wyszukaj
		if(!empty($aBooks)){
			$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_change_status']));
		}
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['found_books'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		
			// wyswietlenie listy pasujacych uzytkownikow
			$pForm->AddMergedRow( $this->GetBooksList($pSmarty, $aBooks) );
	if(!empty($aBooks)){
			$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send2', $aConfig['lang'][$this->sModule]['button_change_status']));
		}
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of EditGroupDiscount() function
	
	
	
	function ChangeStatus(&$pSmarty, $iId=0) {
		global $aConfig;
		$bIsErr = false;
		$aOrdersToRecount = array();
		$aParentsToRecount = array();
		$sErr = '';
		if(!empty($_POST['editable'])){
			foreach($_POST['editable'] as $sKey=>&$aElement){
				$aElement['weight'] = trim($aElement['weight']);
				if ($aElement['weight'] != "" && (!preg_match('/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/',$aElement['weight']) || $aElement['weight'] < 0 || $aElement['weight'] > 15.0)) {
						$sErr .= "<li>Waga</li>";
				}
			}
			if(!empty($sErr)){
				$sErr = $aConfig['lang']['form']['error_prefix'].
					'<ul class="formError">'.$sErr.'</ul>'.
					$aConfig['lang']['form']['error_postfix'];
				$this->sMsg .= GetMessage($sErr, true);
				// wystapil blad wypelnienia formularza
				// wyswietlenie formularza wraz z komunikatem bledu
				$this->showDetails($pSmarty, $iId);
				return;
			}
		}
		$iI = 0;
	//	dump ($_POST); die();
		
		if (!empty($_POST['delete'])) {
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
			Common::BeginTransaction();
			// zaktualizuj stany pozycji
			
			foreach($_POST['delete'] as $iId){
				// pobierz id zamówienia
				$sSql = "SELECT A.order_id, A.product_id, A.weight, B.order_number, A.name
								FROM ".$aConfig['tabls']['prefix']."orders_items A
								JOIN ".$aConfig['tabls']['prefix']."orders B
								ON B.id=A.order_id
								WHERE A.id = ".$iId;
				$aOrder = Common::GetRow($sSql);
				if($aOrder['order_id'] > 0){
					$aValues = array(
						'status' => '4'
					);
					
					if($aOrder['weight'] != Common::formatPrice2($_POST['editable'][$iId]['weight']) && Common::formatPrice2($_POST['editable'][$iId]['weight']) > 0){
						$aValues['weight'] = Common::formatPrice2($_POST['editable'][$iId]['weight']);
						if(floatval($aValues['weight']) > 0 && $aValues['weight'] != $this->getProductWeight($aOrder['product_id'])){
							$aPValues = array(
								'weight' => $aValues['weight']
							);
							if(Common::Update($aConfig['tabls']['prefix']."products", $aPValues, "id = ".$aOrder['product_id']) === false){
									$bIsErr = true;					 
							}
 							if(Common::Update($aConfig['tabls']['prefix']."orders_items", $aPValues, ' (product_id = '.$aOrder['product_id'].' AND weight = "0.00" AND `deleted` = "0")') === false){
									$bIsErr = true;					 
							}
							// TODO: aktualizacja wagi w bazie poprzez DbMgr
						}
					}
					if(Common::formatPrice2($_POST['editable'][$iId]['weight']) == 0){
						$sMsg = sprintf($aConfig['lang'][$this->sModule]['no_weight_err'],$aOrder['name'],$aOrder['order_number']);
						$this->sMsg .= GetMessage($sMsg);
						$bIsErr = true;
					}
					if(Common::Update($aConfig['tabls']['prefix']."orders_items", $aValues,"id = ".$iId) === false){
						$bIsErr = true;
					} else {
						// jeśli nie jest w liście zamowień do przeliczenia, dodaj
						if(!in_array($aOrder['order_id'],$aOrdersToRecount)){
							$aOrdersToRecount[] = $aOrder['order_id'];
						}
						
					//jesli jest składową pakietu to dodajemy do listy parrentów
						if(!in_array($aOrder['parent_id'],$aParentsToRecount)){
								$aParentsToRecount[] = $aOrder['parent_id'];
							}
					}
				} else {
					$bIsErr = true;
				}
			}
			
			
		/**
			 * przeliczenie parentow
			 * jezeli odznaczylismy skladowe pakietu to sprawdzamy czy pakiety sa kompletne
			 * jesli tak to zmieniamy im status
			*/
			//dump($aParentsToRecount);
			if(!$bIsErr && !empty($aParentsToRecount)){
					foreach($aParentsToRecount as $iParentId){
						if(!$bIsErr && $iParentId>0){
							$sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'orders_items WHERE parent_id='.$iParentId.' AND status<>\'4\'';
							//echo Common::GetOne($sSql);
							if(Common::GetOne($sSql)==0) {
								//wszystkie zmatchowane zatem aktualizujemy
								$aValues2 = array(
									'status' => '4'
								);
								if(Common::Update($aConfig['tabls']['prefix']."orders_items",$aValues2,"id = ".$iParentId) === false)
									$bIsErr = true;
							
							}
								
						}
					}
				}
			
			// przeliczenie zamówień
			if(!$bIsErr && !empty($aOrdersToRecount)){
				require_once('OrderRecount.class.php');
				$oOrderRecount = new OrderRecount();
				foreach($aOrdersToRecount as $iOrderId){
					if(!$bIsErr){
						if($oOrderRecount->recountOrder($iOrderId) === false){
							$bIsErr = true;
						}
					}
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['update_ok'];
				$this->sMsg .= GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['update_err'];
				$this->sMsg .= GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
			$this->Show($pSmarty);
		} else {
			$this->Show($pSmarty);
		}
	}
	
	
	/**
	 * Metoda wyswietla liste ksiazek z tablicy $aBooks
	 *
	 * @param		object	$pSmarty
	 * @param	array ref	$aBooks	- lista produktów
	 * @return	void
	 */
	function GetBooksList(&$pSmarty, &$aBooks) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/EditableView.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'checkboxes'	=> true,
			'editable' => true,
			'count_checkboxes' => true,
			'count_checkboxes_by' => 'quantity',
			'form'	=>	false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
		array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '20'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_order_nr'],
				'width' => '120'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_name']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_quantity'],
				'width' => '30'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_weight'],
				'width' => '30'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_shipment'],
				'width' => '50'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_vat'],
				'width' => '45'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_price_brutto'],
				'width' => '75'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_discount'],
				'width' => '30'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['books_list_value_brutto'],
				'width' => '45'
			)
		);

		$pView = new EditableView('ordered_itm', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array(
			'id' => array(
				'show'	=> false
			),
			'weight'=> array(
					'editable'	=> true
					),
		);
			// ustawienia dla kolumn edytowalnych
		$aEditableSettings = array(
			'weight' => array(
				'type' => 'text'
			)
		);
		// dodanie rekordow do widoku
		$pView->AddRecords($aBooks, $aColSettings,$aEditableSettings);
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all')
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		// przepisanie langa - info o braku rekordow
		$aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_books_to_show'];
				// walidacja js kolumn edytowalnych

		$sAdditionalScript='';
		foreach($aBooks as $iKey=>$aItem) {
			if($aItem['weight']=='0,00')	
				$sAdditionalScript.='document.getElementById(\'delete['.$aItem['id'].']\').disabled=true;
				
				';		
			}
		$sJS = '
		<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
		'.$sAdditionalScript.'
		
			$("#ordered_itm").submit(function() {
			//alert("this");
				var sErr = "";
				$(".heditable_input, .editable_input").each(function(){
					regex = /^editable\[([A-Za-z0-9_]+)\]\[([A-Za-z0-9_]+)\]$/;
					matches = regex.exec(this.name);
					if(matches[2] == "weight"){
						$(this).val($(this).val().replace(/&nbsp;/g, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
						if ($(this).val() != "" && (!$(this).val().match(/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) || $(this).val() < 0 || $(this).val() > 15.0)) {
							sErr += "\t- Waga\n";
						}
					}
					
				});
				if(sErr != ""){
					alert("'.$aConfig['lang']['form']['error_prefix'].'\n"+sErr+"'.$aConfig['lang']['form']['error_postfix'].'");
					return false;
				}
			});
		});
	// ]]>
</script>
		';
		return $sJS.$pView->Show();
	} // end of GetBooksList() function

/**
 * metoda pobiera liste ksiazek zamowionych
 * @return array - lista ksiazek
 */
	function &getBooks() {
		global $aConfig;
		$aOrders = array();

			// pobranie ID uzytkownikow spelniajacych zadane kryteria
			$sSql = "SELECT A.id, O.order_number, 
											A.isbn, A.name, A.quantity, A.weight, A.publisher, A.authors, A.publication_year, A.edition,
											A.shipment_time, A.vat, A.price_brutto, A.discount, A.value_brutto, A.promo_price_brutto
							 FROM ".$aConfig['tabls']['prefix']."orders_items A
							 JOIN ".$aConfig['tabls']['prefix']."orders O
							 ON O.id=A.order_id
							 WHERE A.status = '3'
							 AND A.deleted = '0'
							 AND A.packet = '0'
							 AND O.order_status <> '5'
							 AND A.deleted='0'
							 ORDER BY A.order_id ASC";
			$aBooks = Common::GetAll($sSql);
			
			foreach($aBooks as $iKey=>$aItem){
				$aBooks[$iKey]['name'] = $aItem['name'].
											($aItem['isbn']?'<br /><span style="bold; color: red;">'.$aItem['isbn'].'</span>':'').
											($aItem['publication_year']?'<br />'.$aItem['publication_year']:'').($aItem['edition']?' wyd. '.$aItem['edition']:'').
											($aItem['publisher']?'<br />'.$aItem['publisher']:'').
											($aItem['authors']?'<br />'.$aItem['authors']:'').'</span>';
				unset($aBooks[$iKey]['isbn']);
				unset($aBooks[$iKey]['publication_year']);
				unset($aBooks[$iKey]['edition']);
				unset($aBooks[$iKey]['publisher']);
				unset($aBooks[$iKey]['authors']);
				$sPrefix = '';

                $pShipmentTime = new \orders\Shipment\ShipmentTime();
                $aBooks[$iKey]['shipment_time'] = $pShipmentTime->getShipmentTime($aItem['id'], $aItem['shipment_time']);
				if ($aItem['promo_price_brutto'] > 0) {
					$sPrefix = '<br>';
					$aBooks[$iKey]['price_brutto'] = '<span style="text-decoration: line-through;">'.Common::formatPrice($aItem['price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span></span><br>'.Common::formatPrice($aItem['promo_price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
				} else {
					$aBooks[$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
				}
				unset($aBooks[$iKey]['promo_price_brutto']);
				if ($aItem['discount'] > 0) {
					$aBooks[$iKey]['discount'] = Common::formatPrice3($aItem['discount']);
				}
				else {
					$aBooks[$iKey]['discount'] = '&nbsp;';
				}
				$aBooks[$iKey]['value_brutto'] = $sPrefix.Common::formatPrice($aItem['value_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
				$aBooks[$iKey]['vat'] = Common::formatPrice3($aItem['vat']).'<span style="color: gray">%</span>';
				$aBooks[$iKey]['weight'] = Common::formatPrice($aItem['weight']);
			}

		
		return $aBooks;
	} // end of getBooks() method

/**
 * metoda pobiera liste ksiazek dla wygenerowanego wydruku
 * @return array - lista ksiazek
 */
	function &getBooksNew($iId) {
		global $aConfig;
		$aOrders = array();

			// ksiazek spelniajacych zadane kryteria dla danego wydruku
			$sSql = "SELECT A.id, R.order_number, R.id AS order_id, 
											A.isbn, A.name, A.quantity, A.weight, A.publisher, A.authors, A.publication_year, A.edition,
											A.shipment_time, A.vat, A.price_brutto, A.discount, A.value_brutto, A.promo_price_brutto
							 FROM ".$aConfig['tabls']['prefix']."orders R, ".$aConfig['tabls']['prefix']."orders_items A
							 JOIN ".$aConfig['tabls']['prefix']."orders_send_history_items O
							 ON O.item_id=A.id
							 LEFT JOIN ".$aConfig['tabls']['prefix']."orders_send_history AS SH
								 ON O.send_history_id = SH.id
							 WHERE A.status = '3'
							 AND A.deleted = '0'
							 AND A.packet = '0'
							 AND R.order_status <> '5'
							 AND R.id=A.order_id
							 AND O.send_history_id=$iId
							 ORDER BY IF(saved_type='1' || saved_type='3', A.name, A.order_id) ASC";
			$aBooks = Common::GetAll($sSql);
			
			foreach($aBooks as $iKey=>$aItem){
				$aBooks[$iKey]['name'] = $aItem['name'].
											($aItem['isbn']?'<br /><span style="bold; color: red;">'.$aItem['isbn'].'</span>':'').
											($aItem['publication_year']?'<br />'.$aItem['publication_year']:'').($aItem['edition']?' wyd. '.$aItem['edition']:'').
											($aItem['publisher']?'<br />'.$aItem['publisher']:'').
											($aItem['authors']?'<br />'.$aItem['authors']:'').'</span>';
				unset($aBooks[$iKey]['isbn']);
				unset($aBooks[$iKey]['publication_year']);
				unset($aBooks[$iKey]['edition']);
				unset($aBooks[$iKey]['publisher']);
				unset($aBooks[$iKey]['authors']);
				$sPrefix = '';
                $pShipmentTime = new \orders\Shipment\ShipmentTime();
                $aBooks[$iKey]['shipment_time'] = $pShipmentTime->getShipmentTime($aItem['id'], $aItem['shipment_time']);
				if ($aItem['promo_price_brutto'] > 0) {
					$sPrefix = '<br>';
					$aBooks[$iKey]['price_brutto'] = '<span style="text-decoration: line-through;">'.Common::formatPrice($aItem['price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span></span><br>'.Common::formatPrice($aItem['promo_price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
				} else {
					$aBooks[$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
				}
				unset($aBooks[$iKey]['promo_price_brutto']);
				if ($aItem['discount'] > 0) {
					$aBooks[$iKey]['discount'] = Common::formatPrice3($aItem['discount']);
				}
				else {
					$aBooks[$iKey]['discount'] = '&nbsp;';
				}
				$aBooks[$iKey]['value_brutto'] = $sPrefix.Common::formatPrice($aItem['value_brutto']).' <span style="color: gray">'.$aConfig['lang']['common']['currency'].'</span>';
				$aBooks[$iKey]['vat'] = Common::formatPrice3($aItem['vat']).'<span style="color: gray">%</span>';
				$aBooks[$iKey]['weight'] = Common::formatPrice($aItem['weight']);
				$aBooks[$iKey]['order_number']='<a href="admin.php?frame=main&module_id=38&module=m_zamowienia&do=details&ppid=0&id='.$aBooks[$iKey]['order_id'].'">'.$aBooks[$iKey]['order_number'].'</a>';
				unset($aBooks[$iKey]['order_id']);
			}

		
		return $aBooks;
	} // end of getBooks() method
	
	
	function &getBookData($iId){
		global $aConfig;
		$sSql = "SELECT A.id, A.isbn, A.name, B.name AS publisher, A.publication_year
							 FROM ".$aConfig['tabls']['prefix']."products A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers AS B
							 ON B.id = A.publisher_id
							 WHERE A.id = ".$iId;
			return Common::GetRow($sSql);
	}
	
	function setOrderedItems($sItems){
		global $aConfig;
		if(!empty($sItems)){
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders_items
								SET status='2'
								WHERE id IN (".$sItems.") AND status='1'";
			if(Common::Query($sSql) === false){
				return false;
			}
			return true;
		}
		return false; 
	}
	
	/**
	 * Metoda pobiera wagę produktu z tabeli products
	 * @param $iProductId - id produktu
	 * @return float - waga
	 */
	function getProductWeight($iProductId){
		global $aConfig;
		$sSql = "SELECT weight
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id = ".$iProductId;
		return Common::GetOne($sSql);
	} // end of getProductWeight() method
	
} // end of Module Class
?>