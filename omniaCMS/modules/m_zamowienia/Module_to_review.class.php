<?php
/**
 * Klasa zajmuje się zamówieniami do sprawdzenia
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-03 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module { // Module__zamowienia__to_review

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// Id strony modulu
	var $iPageId;
  
  // id uzytkownika
  var $iUId;
  
  private $bShowAll = false;
  
  function __construct($pSmarty) {
    
		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];
		$this->iPageId = getModulePageId('m_konta');
    
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
    
		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
    
		$this->iUId = $_SESSION['user']['id'];
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    // specjalne uprawnienia do wyświetlenia listy wszystkich zamowien do sprawdzenia
    if ($_SESSION['user']['priv_reviews'] == '1') {
      $this->bShowAll = true;
    }
    
    switch ($sDo) {
      case 'details':
        $this->Details($pSmarty, $iId);
      break;
      case 'deactivate':
        $this->Deactivate($pSmarty, $iId);
      break;
    
      default: $this->Show($pSmarty); break;
    }
  }// end of __construct() method
  
  
  /**
   * Metoda deaktywuje alert
   * 
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iId
   */
  public function Deactivate(&$pSmarty, $iId) {
    global $aConfig;
    include_once('Module_alert.class.php');
    $_GET['action_goback'] = 'to_review';
    $oModuleAlert = new Module__zamowienia__alert($oNull, false);
    $oModuleAlert->Deactivate($iId);
  }// end of Deactivate() method
  
  
  /**
   * Metoda pobiera nazwę uzytkownika
   * 
   * @param type $iId
   * @return type
   */
  private function _getUserName($iId) {
    
    $sSql = "SELECT CONCAT(name, ' ', surname)
             FROM users 
             WHERE id=".$iId;
    return Common::GetOne($sSql);
  }// end of _getUserName() method
  
  
  /**
   * Metoda wyświetla listę alertów
   * 
   * @global type $aConfig
   * @param type $pSmarty
   */
  public function Show(&$pSmarty) {
    global $aConfig;
    
    // dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);
    
		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> false,
      'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
    $aRecordsHeader = array(
        
			array(
				'db_field'	=> 'message',
				'content'	=> $aConfig['lang'][$this->sModule]['list_message'],
				'sortable'	=> true,
			),
//      array(
//				'db_field'	=> 'comments',
//				'content'	=> $aConfig['lang'][$this->sModule]['list_comments'],
//				'sortable'	=> true,
//			),
			array(
				'db_field'	=> 'user_id',
				'content'	=> $aConfig['lang'][$this->sModule]['list_user_id'],
				'sortable'	=> true,
				'width'	=> '50'
			),
			array(
				'db_field'	=> 'alert',
				'content'	=> $aConfig['lang'][$this->sModule]['list_alert'],
				'sortable'	=> true,
				'width'	=> '50'
			),
      array(
				'db_field'	=> 'alert_datetime',
				'content'	=> $aConfig['lang'][$this->sModule]['list_alert_datetime'],
				'sortable'	=> true,
        'width'	=> '40'
			),
      array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
        'width'	=> '40'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '50'
			),
      array(
				'db_field'	=> 'disabledd',
				'content'	=> $aConfig['lang'][$this->sModule]['list_disabled'],
				'sortable'	=> true,
				'width'	=> '90'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '30'
			)
    );
    
    if ($_POST['f_send_or_recivied'] == '1') {
      $sWhereUser = " (created_by_user_id = ".$this->iUId.") ";
    } elseif ($_POST['f_send_or_recivied'] == '2') {
      $sWhereUser = " (user_id = ".$this->iUId." OR user_id IS NULL) ";
    } else {
      $sWhereUser = " (created_by_user_id = ".$this->iUId." OR user_id = ".$this->iUId." OR user_id IS NULL) ";
      if ($this->bShowAll === TRUE) {
        $sWhereUser = ' 1=1';
      }
    }
    
    if ($_POST['f_user'] != '') {
      $sWhereUser .= ' AND user_id '.$_POST['f_user'];
    } 
      
    if ($_POST['f_user_id'] != '') {
      $sWhereUser .= ' AND user_id = '.$_POST['f_user_id'];
    } 
    
    if ($_POST['f_created_by'] != '') {
      $sWhereUser .= ' AND created_by = '.Common::Quote($_POST['f_created_by']);
    } 
    
    if (!isset($_POST['f_disabled'])) {
      $_POST['f_disabled'] = '0';
    }
    
    if ($_POST['f_disabled'] != '') {
      $sWhereUser .= ' AND disabled = '.Common::Quote($_POST['f_disabled']);
    } 
    
    // pobranie liczby wszystkich uzytkownikow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."orders_to_review
						 WHERE 1=1 AND ".$sWhereUser;
    $iRowCount = intval(Common::GetOne($sSql));

		$pView = new View('users', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
    
   $aDisabled = [
        [
            'label' => 'Tak',
            'value' => '0'
        ],
        [
            'label' => 'Nie',
            'value' => '1'
        ]
    ];
    $pView->AddFilter('f_disabled', _('Aktywny'), addDefaultValue($aDisabled), $_POST['f_disabled']);
    
    $aSendRecivent = array(
        array(
            'label' => _('Wysłane'),
            'value' => '1'
        ),
        array(
            'label' => _('Odebrane'),
            'value' => '2'
        ),
    );
    $pView->AddFilter('f_send_or_recivied', _('Wysłane/Odebrane'), addDefaultValue($aSendRecivent), $_POST['f_send_or_recivied']);
    
    
    $aUser = array(
        [
            'label' => _('Użytkownicy CMS'),
            'value' => _(' IS NOT NULL '),
        ],
        [
            'label' => _('Automaty'),
            'value' => _(' IS NULL '),
        ],
    );
    $pView->AddFilter('f_user', _('Użytkownik CMS'), addDefaultValue($aUser), $_POST['f_user']);
    
    $pView->AddFilter('f_user_id', _('Utworzono przez'), addDefaultValue($this->getUsers()), $_POST['f_user_id']);
    
    $pView->AddFilter('f_created_by', _('Automat'), addDefaultValue($this->getCreatedBy()), $_POST['f_created_by']);
    
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
      
      // pobranie wszystkich rekordow
			$sSql = "SELECT id, order_id, message, comments, user_id, alert, alert_datetime, created, created_by, disabled as disabledd, disabled_by, disabled_datetime
               FROM orders_to_review
               WHERE 1=1 AND ".$sWhereUser.
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'disabled ASC, id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
      foreach ($aRecords as $iKey => $aRecord) {
        
        if (!empty($aRecord['comments'])) {
          $aRecords[$iKey]['comments'] = implode(', ', unserialize($aRecord['comments']));
        } else {
          $aRecords[$iKey]['comments'] = '&nbsp;';
        }
        if (mb_strlen($aRecord['message'], 'UTF-8') > 255) {
          $aRecord['message'] = mb_substr($aRecord['message'], 0, 255).'...';
        }
        if ($aRecord['disabledd'] == '1') {
          $aRecords[$iKey]['disabledd'] = ' przez '.$aRecord['disabled_by'].' <br />'.$aRecord['disabled_datetime'];
          $aRecords[$iKey]['disabled'][] = 'deactivate';
          $aRecords[$iKey]['message'] = '<span style="color: grey">'.$aRecord['message'].'</span>';
        } else {
          $aRecords[$iKey]['message'] = '<span style="color: red">'.$aRecord['message'].'</span>';
          $aRecords[$iKey]['disabledd'] = $aConfig['lang']['common'][$aRecord['disabledd']];
        }
        $aRecords[$iKey]['message'] .= ' <a href="'.phpSelf(array('do' => 'details', 'id' => $aRecord['order_id'], 'ppid' => '0', 'save_referer_url' => urlencode('Zamówienia do sprawdzenia')), array('action')).'">&nbsp;Przejdź&nbsp;do&nbsp;zamówienia&nbsp;&raquo;</a> ';
        
        if ($aRecord['user_id'] > 0) {
          $aRecords[$iKey]['user_id'] = $this->_getUserName($aRecord['user_id']);
        } else {
          $aRecords[$iKey]['user_id'] = $aConfig['lang'][$this->sModule]['all_users'];
        }
        
        $aRecords[$iKey]['alert'] = $aConfig['lang']['common'][$aRecord['alert']];
      }
      
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id' => array (
					'show'	=> false
				),
        'disabled_by' => array (
					'show'	=> false
				),
        'comments' => array (
					'show'	=> false
				),
        'disabled_datetime' => array (
					'show'	=> false
				),
        'order_id' => array (
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('deactivate', 'details'),
					'params' => array (
              'deactivate'	=> array('id' => '{id}'),
              'details'	=> array('id' => '{id}'),
											),
					'show' => false,
          'icon' => array(
            'deactivate' => 'activate',
            'previeww' => 'preview'
          )
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
    }
    
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
  }// end of Show() method
  
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @return array
   */
  private function getCreatedBy() {
    global $pDbMgr;
    
    $sSql = 'SELECT created_by AS label, created_by AS value
             FROM orders_to_review
             WHERE user_id IS NULL
             GROUP BY created_by';
    return $pDbMgr->GetAll('profit24', $sSql);
  }
  
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @return array
   */
  private function getUsers() {
    global $pDbMgr;
    
    $sSql = 'SELECT CONCAT(name, " ", surname) AS label, id AS value 
             FROM users
             WHERE active = "1" ';
    return $pDbMgr->GetAll('profit24', $sSql);
  }
  
  
  public function Details(&$pSmarty, $iId) {
		global $aConfig;

		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
    $pForm = new FormTable('attributes', "Szczegóły", array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), false);

		// pobranie z bazy danych szczegolow konta uzytkownika
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."orders_to_review
						 WHERE id = ".$iId;
		$aRecord = Common::GetRow($sSql);
    
    $pForm->AddRow($aConfig['lang'][$this->sModule]['list_message'], '<pre style="font-size: 12px;line-height: 80%;">'.$aRecord['message'].'</pre>');
    $pForm->AddRow($aConfig['lang'][$this->sModule]['list_alert'], $aConfig['lang']['common'][$aRecord['alert']]);
    $pForm->AddRow($aConfig['lang'][$this->sModule]['list_alert_datetime'], $aRecord['alert_datetime']);
    if ($aRecord['user_id'] > 0) {
      $pForm->AddRow($aConfig['lang'][$this->sModule]['list_user_id'], $this->_getUserName($aRecord['user_id']));
    } else {
      $pForm->AddRow($aConfig['lang'][$this->sModule]['list_user_id'], $aConfig['lang'][$this->sModule]['all_users']);
    }
    $pForm->AddRow($aConfig['lang']['common']['created'], $aRecord['created'].'&nbsp;');
    $pForm->AddRow($aConfig['lang']['common']['created_by'], $aRecord['created_by'].'&nbsp;');
    $pForm->AddRow($aConfig['lang'][$this->sModule]['list_disabled'], $aConfig['lang']['common'][$aRecord['disabled']].'&nbsp;');
    $pForm->AddRow($aConfig['lang'][$this->sModule]['list_disabled_by'], $aRecord['disabled_by'].'&nbsp;');
    $pForm->AddRow($aConfig['lang'][$this->sModule]['list_disabled_datetime'], $aRecord['disabled_datetime'].'&nbsp;');
    $pForm->AddRow($aConfig['lang'][$this->sModule]['list_mail_sent'], $aConfig['lang']['common'][$aRecord['mail_sent']]);
    
		// przyciski
		$pForm->AddRow('&nbsp;', '&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
  }
}// end of class
?>