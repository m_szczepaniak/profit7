<?php
/**
 * Klasa Module do obslugi modulu 'Zamówienia' - waga opakowania
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'opisy_statusow');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'edit':
			case 'add': 
			default: $this->AddEdit($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda aktualizuje w bazie danych wagi opakowan
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}
		if(!empty($_POST['weight'])){
			foreach($_POST['weight'] as $sKey=>$fValue){
				$aValues = array(
					'weight' => Common::formatWeight2($fValue)
				);
				if($this->existsItem($sKey)){
					if (Common::Update($aConfig['tabls']['prefix']."orders_package_weight",
														 $aValues,
														 "bookstore = '".$sKey."'") === false) {
						$bIsErr = true;
					}
				} else {
					$aValues['bookstore'] = $sKey;
					if(Common::Insert($aConfig['tabls']['prefix']."orders_package_weight",
														 $aValues, '', false) === false){
							$bIsErr = true;							 	
					} 
				}
			}
		}
		
		
		if (!$bIsErr) {
			// akapit zostal zaktualizowany
			$sMsg = $aConfig['lang'][$this->sModule]['edit_ok'];
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->AddEdit($pSmarty);
		}
		else {
			// akapit nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = $aConfig['lang'][$this->sModule]['edit_err'];
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty);
		}
	} // end of Update() funciton
	
	function existsItem($sBookstore){
		global $aConfig;
		
		$sSql = "SELECT count(1)
						FROM ".$aConfig['tabls']['prefix']."orders_package_weight
						WHERE bookstore='".$sBookstore."'";
		return intval(Common::GetOne($sSql)) > 0;
	}


	/**
	 * Metoda tworzy formularz dodawania / edycji wag opakowań
	 *
	 * @param	object	$pSmarty
	 */
	function AddEdit(&$pSmarty) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych edytowanego rekordu
		$sSql = "SELECT A.code, A.name, B.weight
						 FROM ".$aConfig['tabls']['prefix']."websites A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."orders_package_weight B
						 ON B.bookstore=A.code";
		$aData =& Common::GetAssoc($sSql);
		

		$sHeader = $aConfig['lang'][$this->sModule]['header'];

		$pForm = new FormTable('package_weight', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');
		
		foreach($aData as $sKey=>$aItem) {
			$pForm->AddMergedRow($aItem['name'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
			$pForm->AddText('weight['.$sKey.']', $aConfig['lang'][$this->sModule]['package_weight'], Common::formatWeight($aItem['weight']), array('maxlength'=>8, 'style'=>'width: 60px;'), '', 'ufloat');
		}
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	

} // end of Module Class
?>