<?php
/**
 * Klasa Module do obslugi modulu 'Formularz kontaktu' - kategorie (odbiorcy)
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}


		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iPId, $iId); break;

			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste kategorii i osbiorcow formularza kontaktu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iPId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'recipients',
				'content'	=> $aConfig['lang'][$this->sModule]['list_recipients'],
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich kategorii
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."contact_forms_categories
						 WHERE page_id = ".$iPId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
						 	 FROM ".$aConfig['tabls']['prefix']."contact_forms_categories
							 WHERE page_id = ".$iPId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('recipients', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich kategorii i odbiorcow
			$sSql = "SELECT id, name, REPLACE(recipients, ';', '<br>') AS recipients,
											created, created_by
						 	 FROM ".$aConfig['tabls']['prefix']."contact_forms_categories
						 	 WHERE page_id = ".$iPId.
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'name').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'pid' => $iPId, 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('delete'),
					'params' => array (
												'delete'	=> array('pid' => $iPId, 'id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('add')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		);
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane rekordy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony usuwanego rekordu
	 * @param	integer	$iId	- Id usuwanego rekordu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
		if (!empty($_POST['delete'])) {
			if (count($_POST['delete']) == 1) {
				$sErrPostfix = '0';
			}
			else {
				$sErrPostfix = '1';
			}

			Common::BeginTransaction();
			// pobranie nazw usuwanych rekordow
			$sSql = "SELECT name
					 		 FROM ".$aConfig['tabls']['prefix']."contact_forms_categories
						 	 WHERE page_id = ".$iPId." AND
						 	 			 id IN  (".implode(',', $_POST['delete']).")";
			$aRecords =& Common::GetAll($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				$sPagePath = getPagePath($iPId, $this->iLangId, true);
				foreach ($aRecords as $aItem) {
					$sDel .= '"'.$sPagePath.' / '.$aItem['name'].'", ';
				}
				$sDel = substr($sDel, 0, -2);
			}

			if (!$bIsErr) {
				// usuwanie
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."contact_forms_categories
								 WHERE page_id = ".$iPId." AND
						 	 			 	 id IN (".implode(',', $_POST['delete']).")";
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.$sErrPostfix],
												$sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_'.$sErrPostfix],
												$sDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowy rekord
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId);
			return;
		}

		$aValues = array(
			'page_id' => $iPId,
			'name' => decodeString($_POST['name']),
			'recipients' => $_POST['recipients'],
			'created' => date('Y-m-d H:i:s'),
			'created_by' => $_SESSION['user']['name']
		);
		// dodanie
		if (Common::Insert($aConfig['tabls']['prefix']."contact_forms_categories",
											 $aValues,
											 '',
											 false) === false) {
			$bIsErr = true;
		}

		$sName = getPagePath($iPId, $this->iLangId, true);
		if (!$bIsErr) {
			// kategoria zostala dodana
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											$_POST['name'],
											$sName);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);

			$this->Show($pSmarty, $iPId);
		}
		else {
			// kategoria nie zostala dodana,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$_POST['name'],
											$sName);
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId);
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych kategorie strony newslettera
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id aktualizowanej strony
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iId);
			return;
		}
		$aValues = array(
			'name' => decodeString($_POST['name']),
			'recipients' => $_POST['recipients'],
			'modified' => date('Y-m-d H:i:s'),
			'modified_by' => $_SESSION['user']['name']
		);
		// dodanie
		if (Common::Update($aConfig['tabls']['prefix']."contact_forms_categories",
											 $aValues,
											 "page_id = ".$iPId." AND
											 	id = ".$iId) === false) {
			$bIsErr = true;
		}

		$sName = getPagePath($iPId, $this->iLangId, true);
		if (!$bIsErr) {
			// kategoria zostala zaktualizowana
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$_POST['name'],
											$sName);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();

			AddLog($sMsg, false);

			$this->Show($pSmarty, $iPId);
		}
		else {
			// kategoria nie zostala zmieniona,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$_POST['name'],
											$sName);
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji
	 *
	 * @param		object	$pSmarty
	 * @param		string	$sId	- ID strony
	 * @param		integer	$iId	- ID edytowanego rekordu
	 */
	function AddEdit(&$pSmarty, $iPId, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanej kategorii
			$sSql = "SELECT name, recipients
							 FROM ".$aConfig['tabls']['prefix']."contact_forms_categories
							 WHERE page_id = ".$iPId." AND
							 			 id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		// okreslenie nazwy strony dla ktorej dodawany / edytowany jest rekord
		$sName = getPagePath($iPId, $this->iLangId, true);

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['name']) && !empty($aData['name'])) {
			$sHeader .= ' "'.trimString($aData['name'], 50).'"';
		}

		$pForm = new FormTable('frecipients', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>115), $aConfig['common']['js_validation']);
		$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], $sName, '', array(), array('class'=>'redText'));
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));

		// nazwa
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>128, 'style'=>'width: 250px;'), '', 'text');

		// odbiorcy kategorii formularza
		$pForm->AddText('recipients', $aConfig['lang'][$this->sModule]['recipients'], $aData['recipients'], array('maxlength'=>255, 'style'=>'width: 350px;'), '', 'emails');

		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function



} // end of Module Class
?>