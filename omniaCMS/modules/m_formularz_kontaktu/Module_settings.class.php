<?php
/**
 * Klasa Module do obslugi konfiguracji stron modulu 'Formularz kontaktu'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Settings {

  // nazwa modulu - do langow
	var $sModule;

	// Id strony dla ktorej edytowane sa ustawienia
	var $iId;

	// sciezka do katalogu zawierajacego szablony czesci klienckiej modulu
	var $sTemplatesDir;

	// lista szablonow czesci klienckiej modulu
	var $sTemplatesList;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$sModule, &$iId) {
		global $aConfig;
		$this->sModule = $sModule;
		$this->iId = $iId;
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/modules/'.$this->sModule;
		$this->sTemplatesList =& GetTemplatesList($this->sTemplatesDir);

		// pobranie konfiguracji dla modulu
		$this->setSettings();
	} // end Settings() function


	/**
	 * Metoda pobiera konfiguracje dla strony modulu
	 *
	 * @return	void
	 */
	function setSettings() {
		global $aConfig;
		$aCfg = array();

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."contact_forms_settings
						 WHERE page_id = ".$this->iId;
		$aCfg =& Common::GetRow($sSql);
		$aConfig['settings'][$this->sModule] =& $aCfg;
	} // end of setSettings() method


	/**
	 * Metoda wprowadza zmiany w ustawieniach strony newslettera
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;

		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."contact_forms_settings
						 WHERE page_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);

		$aValues = array(
			'template' => $_POST['template']
			);

		if (!empty($aSettings)) {
			// Update
			return Common::Update($aConfig['tabls']['prefix']."contact_forms_settings",
														$aValues,
														"page_id = ".$this->iId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('page_id' => $this->iId), $aValues);
			return Common::Insert($aConfig['tabls']['prefix']."contact_forms_settings",
														$aValues, '', false) !== false;
		}
	} // end of UpdateSettings() funciton


	/**
	 * Metoda tworzy formularz edycji konfiguracji strony
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_mtype') {
			$aData =& $_POST;
		}

		// szablon strony
		$pForm->AddSelect('template', $aConfig['lang'][$this->sModule.'_settings']['template'], array(), $this->sTemplatesList, $aData['template'], '', false);

		} // end of SettingsForm() function
} // end of Settings Class
?>