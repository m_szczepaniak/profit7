<?php
/**
 * Klasa Module do obslugi modulu 'Formularz kontaktu' - pola formularza
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$this->aPrivileges =& $_SESSION['user']['privileges'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}


	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iId); break;
			case 'change_mtype':
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iPId, $iId); break;
			case 'add_option': $this->AddEdit($pSmarty, $iPId, $iId, true); break;

			case 'sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show(getPagePath($iPId,
																													 $this->iLangId, true),
																			 $this->getRecords($iPId, $_GET['gid']),
																			 phpSelf(array('do'=>'proceed_sort', 'gid'=>$_GET['gid']),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."contact_forms_fields",
												$this->getRecords($iPId, $_GET['gid']),
												array(
													'page_id' => $iPId,
													'group_id' => $_GET['gid']
											 	),
											 	'order_by');
			break;

			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych dla strony formularza pol
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iPId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'A.name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'B.name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_gname'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'A.required',
				'content'	=> $aConfig['lang'][$this->sModule]['list_required'],
				'sortable'	=> true,
				'width'	=> '100'
			),
			array(
				'db_field'	=> 'A.created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'A.created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '85'
			)
		);

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."contact_forms_fields
						 WHERE page_id = ".$iPId.
						 (isset($_POST['f_group']) && $_POST['f_group'] != '' ? ' AND group_id = '.$_POST['f_group'] : '').
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."contact_forms_fields
							 WHERE page_id = ".$iPId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('fields', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// dodanie filtru grupy
		$pView->AddFilter('f_group', $aConfig['lang'][$this->sModule]['f_group'], $this->getGroups($iPId, $aConfig['lang'][$this->sModule]['f_all_groups']), $_POST['f_group']);
		

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich newsletterow
			$sSql = "SELECT A.id, A.name, B.name AS gname, A.required, A.created, A.created_by
							 FROM ".$aConfig['tabls']['prefix']."contact_forms_fields AS A
							 JOIN ".$aConfig['tabls']['prefix']."contact_forms_groups AS B
							 ON B.id = A.group_id
							 WHERE A.page_id = ".$iPId.
							 		(isset($_POST['f_group']) && $_POST['f_group'] != '' ? ' AND group_id = '.$_POST['f_group'] : '').
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'B.order_by, A.order_by ').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			foreach ($aRecords as $iKey => $aRecord) {
				if ($aRecord['required'] == '1') {
					$aRecords[$iKey]['required'] = $aConfig['lang']['common']['yes'];
				}
				else {
					$aRecords[$iKey]['required'] = $aConfig['lang']['common']['no'];
				}
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'pid' => $iPId, 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('pid' => $iPId, 'id'=>'{id}'),
												'delete'	=> array('pid' => $iPId, 'id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('sort', 'add')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid')),
			'sort' => array(array('gid' => $_POST['f_group']))
		);
		if (!isset($_POST['f_group']) || $_POST['f_group'] == '') {
			unset($aRecordsFooter[1][0]);
			unset($aRecordsFooterParams['sort']);
		}
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane rekordy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony usuwanego rekordu
	 * @param	integer	$iId	- Id usuwanego rekordu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
		if (!empty($_POST['delete'])) {
			if (count($_POST['delete']) == 1) {
				$sErrPostfix = '0';
			}
			else {
				$sErrPostfix = '1';
			}

			Common::BeginTransaction();
			// pobranie nazwa usuwanych rekordow
			$sSql = "SELECT id, name
					 		 FROM ".$aConfig['tabls']['prefix']."contact_forms_fields
						 	 WHERE page_id = ".$iPId." AND
						 	 			 id IN  (".implode(',', $_POST['delete']).")";
			$aRecords =& Common::GetAll($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				$sPagePath = getPagePath($iPId, $this->iLangId, true);
				foreach ($aRecords as $aItem) {
					$sDel .= '"'.$sPagePath.' / '.$aItem['name'].'", ';
				}
				$sDel = substr($sDel, 0, -2);
			}

			if (!$bIsErr) {
				// usuwanie
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."contact_forms_fields
								 WHERE page_id = ".$iPId." AND
						 	 			 	 id IN (".implode(',', $_POST['delete']).")";
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}
			if (!$bIsErr) {
				// pobranie wszystkich pozostalych pol
				$sSql = "SELECT id
								 FROM ".$aConfig['tabls']['prefix']."contact_forms_fields
								 WHERE page_id = ".$iPId."
								 ORDER BY order_by";
				if (($aFields =& Common::GetCol($sSql)) === false) {
					$bIsErr = true;
				}
				else {
					// aktualizacja order_by
					foreach ($aFields as $iKey => $iFId) {
						if (!$bIsErr) {
							$aValues = array(
								'order_by' => $iKey + 1
							);
							if (Common::Update($aConfig['tabls']['prefix']."contact_forms_fields",
																 $aValues,
																 "page_id = ".$iPId." AND
								 			 					  id = ".$iFId) === false) {
								$bIsErr = true;
							}
						}
					}
				}
			}
			if (!$bIsErr) {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.$sErrPostfix],
												$sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_'.$sErrPostfix],
												$sDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowy newsletter
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
		Common::BeginTransaction();
		// dodanie rekordu
		$aValues = array(
			'page_id' => (int) $iPId,
			'group_id' => (int) $_POST['group_id'],
			'name' => decodeString($_POST['name']),
			'description' => $_POST['description'],
			'ftype' => $_POST['ftype'],
			'ftext' => isset($_POST['ftext']) && !empty($_POST['ftext']) ? nl2br($_POST['ftext']) : 'NULL',
			'required' => $_POST['required'],
			'order_by' => '#order_by#',
			'created' => date('Y-m-d H:i:s'),
			'created_by' => $_SESSION['user']['name']
		);
		switch ($_POST['ftype']) {
			case 'text':
				$aValues['maxlength'] = (int) $_POST['maxlength'];
				$aValues['trows'] = 'NULL';
			break;

			case 'email':
				$aValues['maxlength'] = 75;
				$aValues['trows'] = 'NULL';
				$aValues['sender'] = isset($_POST['sender']) ? '1' : '0';
			break;

			case 'textarea':
				$aValues['maxlength'] = 'NULL';
				$aValues['trows'] = (int) $_POST['trows'];
			break;

			default:
				$aValues['maxlength'] = 'NULL';
				$aValues['trows'] = 'NULL';
			break;
		}
		if (($iId =& Common::Insert($aConfig['tabls']['prefix']."contact_forms_fields",
									$aValues,
									"page_id = ".$iPId." AND
									 group_id = ".$_POST['group_id'])) === false) {
			$bIsErr = true;
		}

		if (isset($_POST['options']) && !empty($_POST['options'])) {
			// dodanie opcji pola
			$iOrderBy = 1;
			foreach ($_POST['options'] as $iKey => $sOption) {
				$sOption = trim($sOption);
				if (!$bIsErr && $sOption != '') {
					$aValues = array(
						'field_id' => (int) $iId,
						'name' => decodeString($sOption),
						'order_by' => $iOrderBy++
					);
					if (Common::Insert($aConfig['tabls']['prefix']."contact_forms_fields_options",
														 $aValues,
														 '',
														 false) === false) {
						$bIsErr = true;
					}
				}
			}
		}
		$sName = getPagePath($iPId, $this->iLangId, true);
		if (!$bIsErr) {
			// newsletter zostal dodany
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											$_POST['name'],
											$sName);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// newsletter nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$_POST['name'],
											$sName);
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId);
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych newsletter
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id aktualizowanego newslettera
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iId);
			return;
		}
		Common::BeginTransaction();
		// aktualizacja kategorii
		$aValues = array(
			'group_id' => (int) $_POST['group_id'],
			'name' => decodeString($_POST['name']),
			'ftype' => $_POST['ftype'],
			'description' => $_POST['description'],
			'ftext' => isset($_POST['ftext']) && !empty($_POST['ftext']) ? nl2br($_POST['ftext']) : 'NULL',
			'required' => $_POST['required'],
			'modified' => date('Y-m-d H:i:s'),
			'modified_by' => $_SESSION['user']['name']
		);
		switch ($_POST['ftype']) {
			case 'text':
				$aValues['maxlength'] = (int) $_POST['maxlength'];
				$aValues['trows'] = 'NULL';
			break;

			case 'email':
				$aValues['maxlength'] = 75;
				$aValues['trows'] = 'NULL';
				$aValues['sender'] = isset($_POST['sender']) ? '1' : '0';
			break;

			case 'textarea':
				$aValues['maxlength'] = 'NULL';
				$aValues['trows'] = (int) $_POST['trows'];
			break;

			default:
				$aValues['maxlength'] = 'NULL';
				$aValues['trows'] = 'NULL';
			break;
		}
		if (Common::Update($aConfig['tabls']['prefix']."contact_forms_fields",
											 $aValues,
											 "page_id = ".$iPId." AND
												id = ".$iId) === false) {
			$bIsErr = true;
		}
		if (!$bIsErr) {
			// uusniecie (jezeli istnieja) opcji
			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."contact_forms_fields_options
							 WHERE field_id = ".$iId;
			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
		}

		if (isset($_POST['options']) && !empty($_POST['options'])) {
			// dodanie opcji pola
			$iOrderBy = 1;
			foreach ($_POST['options'] as $iKey => $sOption) {
				$sOption = trim($sOption);
				if (!$bIsErr && $sOption != '') {
					$aValues = array(
						'field_id' => (int) $iId,
						'name' => decodeString($sOption),
						'order_by' => $iOrderBy++
					);
					if (Common::Insert($aConfig['tabls']['prefix']."contact_forms_fields_options",
														 $aValues,
														 '',
														 false) === false) {
						$bIsErr = true;
					}
				}
			}
		}

		$sName = getPagePath($iPId, $this->iLangId, true);
		if (!$bIsErr) {
			// zaktualizowano
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$_POST['name'],
											$sName);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// nie zaktualizowano,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$_POST['name'],
											$sName);
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji pola  formularza
	 *
	 * @param		object	$pSmarty
	 * @param		string	$sId	- ID strony
	 * @param		integer	$iId	- ID edytowanego pola
	 * @param	bool	$bAddOption - true: dodaj opcje
	 */
	function AddEdit(&$pSmarty, $iPId, $iId=0, $bAddOption=false) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego pola formularza
			$sSql = "SELECT group_id, name, ftype, description, required, maxlength, trows, ftext, sender
							 FROM ".$aConfig['tabls']['prefix']."contact_forms_fields
							 WHERE page_id = ".$iPId." AND
							 			 id = ".$iId;
			$aData =& Common::GetRow($sSql);
			$aData['ftext'] = br2nl($aData['ftext']);
			if ($aData['ftype'] == 'checkboxes' || $aData['ftype'] == 'radios' ||
					$aData['ftype'] == 'select' || $aData['ftype'] == 'checkboxes_modules') {
				// pobranie opcji
				$sSql = "SELECT order_by, name
								 FROM ".$aConfig['tabls']['prefix']."contact_forms_fields_options
								 WHERE field_id = ".$iId."
							 	 ORDER BY order_by";
				$aData['options'] =& Common::GetAssoc($sSql);
			}
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			if (!isset($aData['ftype'])) {
				$aData['ftype'] = 'text';
			}
			if (!isset($aData['required'])) {
				$aData['required'] = '1';
			}
		}

		// okreslenie nazwy strony dla ktorej dodawana / edytowana jest kategoria
		$sName = getPagePath($iPId, $this->iLangId, true);

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['name']) && !empty($aData['name'])) {
			$sHeader .= ' "'.trimString($aData['name'], 50).'"';
		}

		$pForm = new FormTable('categories', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>130), $aConfig['common']['js_validation']);
		$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], $sName, '', array(), array('class'=>'redText'));
		$pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
		
		// grupa pol
		$aGroups = $this->getGroups($iPId, $aConfig['lang']['common']['choose']);
		if (count($aGroups) == 2) {
			$pForm->AddHidden('group_id', $aGroups[1]['value']);
		}
		else {
			$pForm->AddSelect('group_id', $aConfig['lang'][$this->sModule]['group'], array(), $aGroups, $aData['group_id']);
		}
				
		// typ pola
		$aTypes = array(
			array('value' => 'text', 'label' => $aConfig['lang'][$this->sModule]['type_text']),
			array('value' => 'email', 'label' => $aConfig['lang'][$this->sModule]['type_email']),
			array('value' => 'textarea', 'label' => $aConfig['lang'][$this->sModule]['type_textarea']),
			array('value' => 'checkbox', 'label' => $aConfig['lang'][$this->sModule]['type_checkbox']),
			array('value' => 'agreement', 'label' => $aConfig['lang'][$this->sModule]['type_agreement']),
			array('value' => 'checkboxes', 'label' => $aConfig['lang'][$this->sModule]['type_checkboxes']),
			array('value' => 'radios', 'label' => $aConfig['lang'][$this->sModule]['type_radios']),
			array('value' => 'select', 'label' => $aConfig['lang'][$this->sModule]['type_select']),
      array('value' => 'captcha', 'label' => 'CAPTCHA'),
		);
		if (Common::moduleExists('m_oferta_produktowa')) {
			// dodanie pola typu Lista produktow z Oferty produktowej
			$aTypes[] = array('value' => 'products', 'label' => $aConfig['lang'][$this->sModule]['type_products']);
		}

		$pForm->AddSelect('ftype', $aConfig['lang'][$this->sModule]['ftype'], array('onchange'=>'ChangeObjValue(\'do\', \'change_mtype\'); this.form.submit();'), $aTypes, $aData['ftype'], '', false);

		// nazwa pola
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>255, 'style'=>'width: 350px;'));

		// opis
		$pForm->AddTextArea('description', $aConfig['lang'][$this->sModule]['description'], $aData['description'], array('rows'=>'8', 'style'=>'width: 100%;'), 0, '', '', false);
		
		// pole wymagane
		$aRequired = array(
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['radio_required']),
			array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['radio_not_required'])
		);
		$pForm->AddRadioSet('required', $aConfig['lang'][$this->sModule]['required'], $aRequired, $aData['required'], '', true, false);

		switch ($aData['ftype']) {
			case 'text':
				// maksymalna liczba znakow pola
				$pForm->AddText('maxlength', $aConfig['lang'][$this->sModule]['maxlength'], $aData['maxlength'], array('maxlenth'=>3, 'style'=>'width: 50px;'), '', 'uinteger');
			break;

			case 'textarea':
				// liczba wierszow obszaru tekstowego
				$pForm->AddText('trows', $aConfig['lang'][$this->sModule]['trows'], $aData['trows'], array('maxlenth'=>2, 'style'=>'width: 50px;'), '', 'uinteger');
			break;

			case 'email':
				// czy pole jest polem nadawcy
				$pForm->AddCheckBox('sender', $aConfig['lang'][$this->sModule]['sender'], array(), '', !empty($aData['sender']), false);
			break;

			case 'checkboxes':
			case 'radios':
			case 'select':
				$iOptions = 0;
				if (isset($aData['options']) && !empty($aData['options'])) {
		    	$iOptions = count($aData['options']);
		    	$i = 0;
					if ($iOptions < 5)
						$iOptions = 5;
				}
				else
					$iOptions = 5;
				if ($bAddOption) {
					// wybranmo dodanie kolejnej opcji
					$iOptions++;
				}
				for ($i = 1; $i <= $iOptions; $i++) {
					$pForm->AddText('options['.$i.']', $aConfig['lang'][$this->sModule]['option'].' '.$i, isset($aData['options'][$i]) ? $aData['options'][$i] : '', array('maxlenth'=>255, 'style'=>'width: 350px;'), '', 'text', $i > 2 ? false : true);
				}
				$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('add_option', $aConfig['lang']['common']['button_add_option'], array('onclick'=>'GetObject(\'do\').value=\'add_option\'; this.form.submit();', 'style'=>'margin-left: 255px;'), 'button'));
			break;

			case 'agreement':
				// tresc zgody
				$pForm->AddTextArea('ftext', $aConfig['lang'][$this->sModule]['agreement_text'], $aData['ftext'], array('rows'=>4, 'style'=>'width: 350px;'));
			break;
		}

		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common'][(!empty($aData['sent']) ? 'save_as' : 'button_'.($iId > 0 ? '1' : '0'))]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function


	/**
	 * Metoda pobiera liste rekordow
	 *
	 * @param	integer	$iPId	- Id strony
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($iPId, $iGId) {
		global $aConfig;

		// pobranie wszystkich akapitów dla danej strony
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."contact_forms_fields
						 WHERE page_id = ".$iPId." AND
						 	   group_id = ".$iGId."
						 ORDER BY order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
	
 	/** Metoda pobiera liste grup
	 *
	 * @param	integer	$iPId	- Id strony
	 * @param	string	$sLabel	- etykieta pierwszej opcji
	 * @return	array ref	- lista grup
	 */
	function getGroups($iPId, $sLabel) {
		global $aConfig;

		// pobranie wszystkich akapitów dla danej strony
		$sSql = "SELECT id AS value, name AS label
						 FROM ".$aConfig['tabls']['prefix']."contact_forms_groups
						 WHERE page_id = ".$iPId."
						 ORDER BY order_by";
		return @array_merge(
			array(array('value' => '', 'label' => $sLabel)),
			Common::GetAll($sSql)
		);
	} // end of getGroups() method
} // end of Module Class
?>