<?php
/**
 * Klasa Module do obslugi modulu 'Formularz kontaktu' - pola formularza
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$this->aPrivileges =& $_SESSION['user']['privileges'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}


	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'update': $this->Update($pSmarty, $iPId); break;
			case 'edit':
			default: $this->AddEdit($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda aktualizuje w bazie danych newsletter
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id aktualizowanego newslettera
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->AddEdit($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
		Common::BeginTransaction();
	// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."contact_forms_settings
						 WHERE page_id = ".$iPId;
		$aSettings =& Common::GetRow($sSql);

		$aValues = array(
			'subject' => decodeString($_POST['subject']),
			'recipients' => $_POST['recipients'],
			'sender' => $_POST['sender'],
			'message' => nl2br(trim($_POST['message'])),
			'ftext' => nl2br(trim($_POST['ftext'])),
			'fdescription' => (trim($_POST['fdescription']) != '' ? $_POST['fdescription']:'NULL')
		);

		if (!empty($aSettings)) {
			// Update
			$bIsErr= (Common::Update($aConfig['tabls']['prefix']."contact_forms_settings",
														$aValues,
														"page_id = ".$iPId) === false);
		}
		else {
			// Insert
			$aValues = array_merge(array('page_id' => $iPId), $aValues);
			$bIsErr= ( Common::Insert($aConfig['tabls']['prefix']."contact_forms_settings",
														$aValues) == false);
		}
		$sName = getPagePath($iPId, $this->iLangId, true);
		if (!$bIsErr) {
			// zaktualizowano
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$sName);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();
			AddLog($sMsg, false);
			$this->AddEdit($pSmarty, $iPId);
		}
		else {
			// nie zaktualizowano,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$sName);
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji pola  formularza
	 *
	 * @param		object	$pSmarty
	 * @param		string	$sId	- ID strony
	 * @param		integer	$iId	- ID edytowanego pola
	 * @param	bool	$bAddOption - true: dodaj opcje
	 */
	function AddEdit(&$pSmarty, $iPId) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

			// pobranie z bazy danych edytowanego pola formularza
			$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."contact_forms_settings
						 WHERE page_id = ".$iPId;
			$aData =& Common::GetRow($sSql);
			$aData['message'] = br2nl($aData['message']);
			$aData['ftext'] = br2nl($aData['ftext']);

		if (!empty($_POST)) {
			$aData =& $_POST;
		}
	
		// okreslenie nazwy strony dla ktorej dodawana / edytowana jest kategoria
		$sName = getPagePath($iPId, $this->iLangId, true);

		$sHeader = $aConfig['lang'][$this->sModule]['header'].$sName;


		$pForm = new FormTable('description', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>130), $aConfig['common']['js_validation']);
		$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], $sName, '', array(), array('class'=>'redText'));
		$pForm->AddHidden('do', 'update');
		
	// domyslni temat formularza
		$pForm->AddText('subject', $aConfig['lang'][$this->sModule]['subject'], $aData['subject'], array('maxlength'=>128, 'style'=>'width: 350px;'));

		// domyslni odbiorcy formularza
		$pForm->AddText('recipients', $aConfig['lang'][$this->sModule]['recipients'], $aData['recipients'], array('maxlength'=>255, 'style'=>'width: 350px;'), '', 'emails');

		// domyslny nadawca formularza
		$pForm->AddText('sender', $aConfig['lang'][$this->sModule]['sender'], $aData['sender'], array('maxlength'=>255, 'style'=>'width: 350px;'), '', 'emails');
		
		// komunikat wyswietlany po wyslaniu formularza
		$pForm->AddTextarea('message', $aConfig['lang'][$this->sModule]['message'], $aData['message'], array('style'=>'width: 350px;', 'rows'=>8));
		
		// dodatkowy opis formularza
		$pForm->AddWYSIWYG('fdescription', $aConfig['lang'][$this->sModule]['description'], $aData['fdescription'],array(),'',false);

		// tekst opisowy formularza
		//$pForm->AddTextarea('ftext', $aConfig['lang'][$this->sModule]['ftext'], $aData['ftext'], array('style'=>'width: 350px;', 'rows'=>8), 0, '', '', false);
	

		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array(),array('action')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function

} // end of Module Class
?>