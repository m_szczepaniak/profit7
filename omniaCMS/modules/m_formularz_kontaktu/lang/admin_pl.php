<?php
/**
* Plik jezykowy dla interfejsu modulu 'Formularz kontaktu' Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*---------- lista stron formularza kontaktu */
$aConfig['lang']['m_formularz_kontaktu']['list'] = "Lista stron formularz kontaktu";
$aConfig['lang']['m_formularz_kontaktu']['preview'] = "Podgląd";
$aConfig['lang']['m_formularz_kontaktu']['description'] = "Opis i ustawienia";
$aConfig['lang']['m_formularz_kontaktu']['preview'] = "Podgląd";
$aConfig['lang']['m_formularz_kontaktu']['no_items'] = "Brak zdefiniowanych stron formularz kontaktu";

$aConfig['lang']['m_formularz_kontaktu']['categories'] = "Grupy pól formularza";
$aConfig['lang']['m_formularz_kontaktu']['recipients'] = "Kategorie i odbiorcy formularza kontaktu";

/*---------- ustawienia stron formularz kontaktu */
$aConfig['lang']['m_formularz_kontaktu_settings']['template'] = "Szablon strony";
$aConfig['lang']['m_formularz_kontaktu_settings']['subject'] = "Domyślny temat";
$aConfig['lang']['m_formularz_kontaktu_settings']['recipients'] = "Domyślne adresy email odbiorców (oddzielone średnikami)";
$aConfig['lang']['m_formularz_kontaktu_settings']['message'] = "Komunikat wyświetlany po wysłaniu formularza";
$aConfig['lang']['m_formularz_kontaktu_settings']['ftext'] = "Tekst opisowy formularza";
$aConfig['lang']['m_formularz_kontaktu_settings']['description'] = "Opis formularza";

/*------------- ustawienia opisowe */
$aConfig['lang']['m_formularz_kontaktu_description']['header'] = "Edycja ustawień formularza: ";
$aConfig['lang']['m_formularz_kontaktu_description']['subject'] = "Domyślny temat";
$aConfig['lang']['m_formularz_kontaktu_description']['recipients'] = "Domyślne adresy email odbiorców (oddzielone średnikami)";
$aConfig['lang']['m_formularz_kontaktu_description']['message'] = "Komunikat wyświetlany po wysłaniu formularza";
$aConfig['lang']['m_formularz_kontaktu_description']['ftext'] = "Tekst opisowy formularza";
$aConfig['lang']['m_formularz_kontaktu_description']['description'] = "Opis formularza";
$aConfig['lang']['m_formularz_kontaktu_description']['edit_ok'] = "Zmiany w ustawieniach dla strony \"%s\" zostały wprowadzone";
$aConfig['lang']['m_formularz_kontaktu_description']['edit_err'] = "Nie udało się wprowadzić zmian dla strony \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_formularz_kontaktu_description']['sender'] = "Wyślij przez adres email: ";

/*---------- grupy pol formularza kontaktu */
$aConfig['lang']['m_formularz_kontaktu_categories']['list'] = "Lista grup pól formularza: \"%s\"";
$aConfig['lang']['m_formularz_kontaktu_categories']['list_name'] = "Nazwa";
$aConfig['lang']['m_formularz_kontaktu_categories']['list_default_group'] = "Domyślna";
$aConfig['lang']['m_formularz_kontaktu_categories']['no_items'] = "Brak zdefiniowanych grup pól formularza kontaktu";

$aConfig['lang']['m_formularz_kontaktu_categories']['edit'] = "Edytuj grupę pól";
$aConfig['lang']['m_formularz_kontaktu_categories']['delete'] = "Usuń grupę pól";
$aConfig['lang']['m_formularz_kontaktu_categories']['delete_q'] = "Czy na pewno usunąć wybraną grupę pól?";
$aConfig['lang']['m_formularz_kontaktu_categories']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_formularz_kontaktu_categories']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_formularz_kontaktu_categories']['delete_all_q'] = "Czy na pewno usunąć wybrane grupy pól?";
$aConfig['lang']['m_formularz_kontaktu_categories']['delete_all_err'] = "Nie wybrano żadnej grupy pól do usunięcia!";
$aConfig['lang']['m_formularz_kontaktu_categories']['add'] = "Dodaj grupę pól";
$aConfig['lang']['m_formularz_kontaktu_categories']['del_ok_0']		= "Grupa pól %s została usunięta";
$aConfig['lang']['m_formularz_kontaktu_categories']['del_ok_1']		= "Grupy pól %s zostały usunięte";
$aConfig['lang']['m_formularz_kontaktu_categories']['del_err_0']		= "Nie udało się usunąć grupy pól %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_formularz_kontaktu_categories']['del_err_1']		= "Nie udało się usunąć grup pól %s!<br>Spróbuj ponownie";

$aConfig['lang']['m_formularz_kontaktu_categories']['go_back'] = "Powrót do listy stron";

$aConfig['lang']['m_formularz_kontaktu_categories']['header_0'] = "Dodawanie grupy pól";
$aConfig['lang']['m_formularz_kontaktu_categories']['header_1'] = "Edycja grupy pól";
$aConfig['lang']['m_formularz_kontaktu_categories']['for'] = "Dla strony";
$aConfig['lang']['m_formularz_kontaktu_categories']['name'] = "Nazwa";
$aConfig['lang']['m_formularz_kontaktu_categories']['default_group'] = "Grupa domyślna";

$aConfig['lang']['m_formularz_kontaktu_categories']['add_ok'] = "Grupa pól \"%s\" dla strony \"%s\" została dodana";
$aConfig['lang']['m_formularz_kontaktu_categories']['add_err'] = "Nie udało się dodać grupy pól \"%s\" dla strony \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_formularz_kontaktu_categories']['edit_ok'] = "Zmiany w ustawieniach grupie pól \"%s\" dla strony \"%s\" zostały wprowadzone";
$aConfig['lang']['m_formularz_kontaktu_categories']['edit_err'] = "Nie udało się wprowadzić zmian w grupie pól \"%s\" dla strony \"%s\"!<br><br>Spróbuj ponownie";

$aConfig['lang']['m_formularz_kontaktu_categories']['sort'] = "Sortuj grupy";
$aConfig['lang']['m_formularz_kontaktu_categories']['sort_items'] = "Sortowanie grup formularza strony";

/*---------- odbiorcy i kategorie formularz kontaktu */
$aConfig['lang']['m_formularz_kontaktu_recipients']['list'] = "Lista kategorii i odbiorców formularza: \"%s\"";
$aConfig['lang']['m_formularz_kontaktu_recipients']['list_name'] = "Nazwa";
$aConfig['lang']['m_formularz_kontaktu_recipients']['list_recipients'] = "Odbiorcy";
$aConfig['lang']['m_formularz_kontaktu_recipients']['no_items'] = "Brak zdefiniowanych kategorii i odbiorców formularza kontaktu";

$aConfig['lang']['m_formularz_kontaktu_recipients']['edit'] = "Edytuj kategorię i odbirców";
$aConfig['lang']['m_formularz_kontaktu_recipients']['delete'] = "Usuń kategorię i odbiorców";
$aConfig['lang']['m_formularz_kontaktu_recipients']['delete_q'] = "Czy na pewno usunąć wybraną kategorię i odbiorców?";
$aConfig['lang']['m_formularz_kontaktu_recipients']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_formularz_kontaktu_recipients']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_formularz_kontaktu_recipients']['delete_all_q'] = "Czy na pewno usunąć wybrane kategorie i odbiorców?";
$aConfig['lang']['m_formularz_kontaktu_recipients']['delete_all_err'] = "Nie wybrano żadnej kategorii i odbiorców do usunięcia!";
$aConfig['lang']['m_formularz_kontaktu_recipients']['add'] = "Dodaj kategorię";
$aConfig['lang']['m_formularz_kontaktu_recipients']['del_ok_0']		= "Kategoria %s została usunięta";
$aConfig['lang']['m_formularz_kontaktu_recipients']['del_ok_1']		= "Kategorie %s zostały usunięte";
$aConfig['lang']['m_formularz_kontaktu_recipients']['del_err_0']		= "Nie udało się usunąć kategorii %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_formularz_kontaktu_recipients']['del_err_1']		= "Nie udało się usunąć kategorii %s!<br>Spróbuj ponownie";

$aConfig['lang']['m_formularz_kontaktu_recipients']['go_back'] = "Powrót do listy stron";

$aConfig['lang']['m_formularz_kontaktu_recipients']['header_0'] = "Dodawanie kategorii";
$aConfig['lang']['m_formularz_kontaktu_recipients']['header_1'] = "Edycja kategorii";
$aConfig['lang']['m_formularz_kontaktu_recipients']['for'] = "Dla strony";
$aConfig['lang']['m_formularz_kontaktu_recipients']['name'] = "Nazwa";
$aConfig['lang']['m_formularz_kontaktu_recipients']['recipients'] = "Odbiorcy";

$aConfig['lang']['m_formularz_kontaktu_recipients']['add_ok'] = "Kategoria \"%s\" dla strony \"%s\" została dodana";
$aConfig['lang']['m_formularz_kontaktu_recipients']['add_err'] = "Nie udało się dodać kategorii \"%s\" dla strony \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_formularz_kontaktu_recipients']['edit_ok'] = "Zmiany w ustawieniach kategorii \"%s\" dla strony \"%s\" zostały wprowadzone";
$aConfig['lang']['m_formularz_kontaktu_recipients']['edit_err'] = "Nie udało się wprowadzić zmian w kategorii \"%s\" dla strony \"%s\"!<br><br>Spróbuj ponownie";


/*---------- pola formularza */
$aConfig['lang']['m_formularz_kontaktu_fields']['list'] = "Lista pól strony formularza: \"%s\"";
$aConfig['lang']['m_formularz_kontaktu_fields']['list_name'] = "Nazwa";
$aConfig['lang']['m_formularz_kontaktu_fields']['list_gname'] = "Grupa pól";
$aConfig['lang']['m_formularz_kontaktu_fields']['list_type'] = "Typ pola";
$aConfig['lang']['m_formularz_kontaktu_fields']['list_required'] = "Wymagane";
$aConfig['lang']['m_formularz_kontaktu_fields']['f_group'] = "Grupa pól:";
$aConfig['lang']['m_formularz_kontaktu_fields']['f_all_groups'] = "Wszystkie grupy";
$aConfig['lang']['m_formularz_kontaktu_fields']['no_items'] = "Brak zdefiniowanych pól formularza";

$aConfig['lang']['m_formularz_kontaktu_fields']['edit'] = "Edytuj pole formularza";
$aConfig['lang']['m_formularz_kontaktu_fields']['delete'] = "Usuń pole formularza";
$aConfig['lang']['m_formularz_kontaktu_fields']['delete_q'] = "Czy na pewno usunąć wybrane pole formularza?";
$aConfig['lang']['m_formularz_kontaktu_fields']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_formularz_kontaktu_fields']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_formularz_kontaktu_fields']['delete_all_q'] = "Czy na pewno usunąć wybrane pola formularza?";
$aConfig['lang']['m_formularz_kontaktu_fields']['delete_all_err'] = "Nie wybrano żadnego pola formularza do usunięcia!";
$aConfig['lang']['m_formularz_kontaktu_fields']['add'] = "Dodaj pole formularza";
$aConfig['lang']['m_formularz_kontaktu_fields']['del_ok_0']		= "Pole formularza %s zostało usunięte";
$aConfig['lang']['m_formularz_kontaktu_fields']['del_ok_1']		= "Pola formularza %s zostały usunięte";
$aConfig['lang']['m_formularz_kontaktu_fields']['del_err_0']		= "Nie udało się usunąć pola formularza %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_formularz_kontaktu_fields']['del_err_1']		= "Nie udało się usunąć pól formularza %s!<br>Spróbuj ponownie";

$aConfig['lang']['m_formularz_kontaktu_fields']['go_back'] = "Powrót do listy stron";

$aConfig['lang']['m_formularz_kontaktu_fields']['header_0'] = "Dodawanie pola formularza";
$aConfig['lang']['m_formularz_kontaktu_fields']['header_1'] = "Edycja pola formularza";
$aConfig['lang']['m_formularz_kontaktu_fields']['for'] = "Dla strony";
$aConfig['lang']['m_formularz_kontaktu_fields']['group'] = "Grupa pól";
$aConfig['lang']['m_formularz_kontaktu_fields']['ftype'] = "Typ pola";
$aConfig['lang']['m_formularz_kontaktu_fields']['name'] = "Nazwa";
$aConfig['lang']['m_formularz_kontaktu_fields']['description'] = "Opis pola";
$aConfig['lang']['m_formularz_kontaktu_fields']['required'] = "Wymagane";
$aConfig['lang']['m_formularz_kontaktu_fields']['maxlength'] = "Maksymalna liczba znaków w polu";
$aConfig['lang']['m_formularz_kontaktu_fields']['trows'] = "Liczba wierszy obszaru";
$aConfig['lang']['m_formularz_kontaktu_fields']['sender'] = "Adres nadawcy wiadomości";
$aConfig['lang']['m_formularz_kontaktu_fields']['option'] = "Opcja";
$aConfig['lang']['m_formularz_kontaktu_fields']['option_extra'] = "Kancelaris";
$aConfig['lang']['m_formularz_kontaktu_fields']['agreement_text'] = "Treść zgody";

$aConfig['lang']['m_formularz_kontaktu_fields']['type_text'] = "Pole tekstowe";
$aConfig['lang']['m_formularz_kontaktu_fields']['type_textarea'] = "Obszar tekstowy";
$aConfig['lang']['m_formularz_kontaktu_fields']['type_email'] = "Adres email";
$aConfig['lang']['m_formularz_kontaktu_fields']['type_checkbox'] = "Checkbox";
$aConfig['lang']['m_formularz_kontaktu_fields']['type_checkboxes'] = "Lista checkboxów";
$aConfig['lang']['m_formularz_kontaktu_fields']['type_radios'] = "Lista radiowa";
$aConfig['lang']['m_formularz_kontaktu_fields']['type_select'] = "Lista rozwijana";
$aConfig['lang']['m_formularz_kontaktu_fields']['type_agreement'] = "Checkbox - Wyrażenie zgody";
$aConfig['lang']['m_formularz_kontaktu_fields']['type_products'] = "Lista produktów";

$aConfig['lang']['m_formularz_kontaktu_fields']['radio_required'] = "Pole obowiązkowe";
$aConfig['lang']['m_formularz_kontaktu_fields']['radio_not_required'] = "Pole nieobowiązkowe";

$aConfig['lang']['m_formularz_kontaktu_fields']['add_ok'] = "Pole formularza \"%s\" dla strony \"%s\" zostało dodane";
$aConfig['lang']['m_formularz_kontaktu_fields']['add_err'] = "Nie udało się dodać pola formularza \"%s\" dla strony \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_formularz_kontaktu_fields']['edit_ok'] = "Zmiany w ustawieniach pola formularza \"%s\" dla strony \"%s\" zostały wprowadzone";
$aConfig['lang']['m_formularz_kontaktu_fields']['edit_err'] = "Nie udało się wprowadzić zmian w polu formularza \"%s\" dla strony \"%s\"!<br><br>Spróbuj ponownie";

$aConfig['lang']['m_formularz_kontaktu_fields']['sort'] = "Sortuj pola";
$aConfig['lang']['m_formularz_kontaktu_fields']['sort_items'] = "Sortowanie pól formularza strony";
$aConfig['lang']['m_formularz_kontaktu_fields']['sort_info'] = "Kliknij po kolei na dwóch polach formularza które mają zostać posortowane";
$aConfig['lang']['m_formularz_kontaktu_fields']['sorting_items'] = "Trwa sortowanie pól formularza...";
$aConfig['lang']['m_formularz_kontaktu_fields']['sort_ok'] = "Wybrane pola formularza zostały posortowane";
$aConfig['lang']['m_formularz_kontaktu_fields']['sort_err'] = "Wystąpił błąd podczas sortowania pól formularza!";
?>