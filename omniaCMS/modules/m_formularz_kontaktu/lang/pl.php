<?php
/**
* Plik jezykowy modulu 'Formularz kontaktu'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/

$aConfig['lang']['mod_m_formularz_kontaktu']['no_data']	= 'Brak zdefiniowanych pól formularza!';
$aConfig['lang']['mod_m_formularz_kontaktu']['no_sender_field']	= 'Brak zdefiniowanego pola e-mail nadawcy formularza!';
$aConfig['lang']['mod_m_formularz_kontaktu']['choose']	= '-- wybierz';
$aConfig['lang']['mod_m_formularz_kontaktu']['choose_category']	= 'Temat';
$aConfig['lang']['mod_m_formularz_kontaktu']['send_err'] = 'Wystąpił błąd podczas próby przesłania formularza!<br /><br />Spróbuj ponownie.';
$aConfig['lang']['mod_m_formularz_kontaktu']['subject'] = 'Kontakt ze strony';
$aConfig['lang']['mod_m_formularz_kontaktu']['licences'] = 'Licencje';
$aConfig['lang']['mod_m_formularz_kontaktu']['months'] = 'Miesiące';
$aConfig['lang']['mod_m_formularz_kontaktu']['licences_short'] = 'Lic';
$aConfig['lang']['mod_m_formularz_kontaktu']['months_short'] = 'Mies';
$aConfig['lang']['mod_m_formularz_kontaktu']['computer_code'] = 'Kod komputera';
$aConfig['lang']['mod_m_formularz_kontaktu']['option_extra_text'] = "Kod komputera (dot. programu Kancelaris, gdy wybrano Kancelaris)";

?>