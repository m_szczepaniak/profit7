<?php
/**
 * Klasa odule do obslugi modulu 'Formularza kontaktu'
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {
    
  // Id wersji jezykowej
  var $iLangId;
  
  // Id strony
  var $iPageId;
  
  // szablon strony
  var $sTemplate;
  
  // symbol modulu
  var $sModule;
  
  // ustawienia strony modulu
  var $aSettings;
  
  // link do strony
  var $sPageLink;
  
    // czesc nazwy pliku cache - na podstawie sciezki URI
  var $sCacheName;
  
  // czy cache'owac czy tez nie
  var $bDoCache;
    
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;
		
		$this->iLangId =& $_GET['lang_id'];
		$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->sCacheName = md5($_SERVER['REQUEST_URI']);
		$this->bDoCache = $aConfig['common']['caching'];
		$this->aSettings =& $this->getSettings();
		$this->sTemplate = 'modules/'.$this->sModule.'/'.$this->aSettings['template'];
		$this->sPageLink .= '/'.$aConfig['_tmp']['page']['symbol'];
		
	} // end Module() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla strony
	 * 
	 * @return	array
	 */
	function getSettings() {
		global $aConfig, $oCache;
		
		if (!$this->bDoCache || !is_array($aCfg = unserialize($oCache->get(md5($aConfig['_tmp']['page']['symbol']), 'modules_settings')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			$sSql = "SELECT * 
							 FROM ".$aConfig['tabls']['prefix']."contact_forms_settings 
							 WHERE page_id=".$this->iPageId;
			$aCfg = Common::GetRow($sSql);
			// zapis do cache'a
			if ($this->bDoCache)
				$oCache->save(serialize($aCfg), md5($aConfig['_tmp']['page']['symbol']), 'modules_settings');
		}
		return $aCfg;
	} // end of getSettings() function
	
	
	/**
	 * Metoda pobiera tresc strony o id $this->iPageId
	 * dowiazuje ja do Smartow oraz zwraca xHTML z trescia strony po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty, $oCache;
		$sHtml = '';
		$bShowForm = true;
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['page_link'] = $this->sPageLink;
		$bSenderFieldExists = false;
		
		if ($this->bDoCache && empty($_POST) && false != ($sHtml = $oCache->get($this->sCacheName, 'modules'))) {
		}
		else{		
		// pobranie kategorii formularza
		$sSql = "SELECT id AS value, name AS label, recipients
						 FROM ".$aConfig['tabls']['prefix']."contact_forms_categories
						 WHERE page_id = ".$this->iPageId."
						 ORDER BY name";
		$aCategories =& Common::GetAll($sSql);
		
		// pobranie pol formularza
		$sSql = "SELECT A.id, A.ftype, A.name, A.description, A.required,
										A.maxlength, A.trows, A.sender, A.ftext,
										B.id gid, B.name gname, B.default_group
						 FROM ".$aConfig['tabls']['prefix']."contact_forms_fields A
						 JOIN ".$aConfig['tabls']['prefix']."contact_forms_groups B
						 ON B.id = A.group_id
						 WHERE A.page_id = ".$this->iPageId."
						 ORDER BY B.order_by, A.order_by";
		$aFields =& Common::GetAll($sSql);

		// sprawdzenie czy zostalo zdefiniowane pole nadawcy formularza
		foreach ($aFields as $aField) {
			if ($aField['sender'] == '1') {
				$bSenderFieldExists = true;
				break;
			}
		}
			
		if ($bSenderFieldExists) {
			if (!empty($aFields)) {
				// pobranie opcji pol formularza
				$sSql = "SELECT field_id, id AS value, name AS label
								 FROM ".$aConfig['tabls']['prefix']."contact_forms_fields_options
								 WHERE field_id IN (SELECT id FROM ".$aConfig['tabls']['prefix']."contact_forms_fields WHERE page_id = ".$this->iPageId.")
								 ORDER BY order_by";
				$aOptions =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);

				if (!empty($_POST)) {
					// dolaczenie klasy walidatora
					include_once('Form/Validator.class.php');
					$oValidator = new Validator();
					if (!$oValidator->Validate($_POST)) {
						$aConfig['_tmp']['message']['text'] = $oValidator->GetErrorString();
						$aConfig['_tmp']['message']['error'] = true;
					}
					else {
						// wysylanie formularza
						if ($this->sendForm($this->sPageLink,
																$aCategories,
																$aFields,
																$aOptions)) {
							$aConfig['_tmp']['message']['text'] = $this->aSettings['message'];
							$bShowForm = false;
						}
						else {
              if (empty($aConfig['_tmp']['message']['text'])) {
                $aConfig['_tmp']['message']['text'] = $aModule['lang']['send_err'];
                $aConfig['_tmp']['message']['error'] = true;
              }
						}
					}
				}
				
				if ($bShowForm) {
					$aModule['text'] = $this->aSettings['ftext'];
					$aModule['fdescription'] = $this->aSettings['fdescription'];
					$aModule['form'] = $this->getForm($this->sPageLink, $aCategories, $aFields, $aOptions);
					$pSmarty->assign_by_ref('aModule', $aModule);
					$sHtml = $pSmarty->fetch($this->sTemplate);
					$pSmarty->clear_assign('aModule');
					if ($this->bDoCache && empty($_POST)) {
						$oCache->save($sHtml, $this->sCacheName, 'modules');
					}
				}
			}
			else {
				$aConfig['_tmp']['message']['text'] = $aModule['lang']['no_data'];
			}
		}
		else {
			$aConfig['_tmp']['message']['text'] = $aModule['lang']['no_sender_field'];
		}
		
		//dump($aModule);
		}
		return $sHtml;
	} // end of getContent()
	
	
	/**
	 * Metoda tworzy pola formularza za pomoca klasy FormTable
	 * 
	 * @param	string	$sAction	- link strony formularza
	 * @param	array	$aCategories	- kategorie formularza
	 * @param	array	$aFields	- pola formularza
	 * @param	array	$aOptions	- opcje pol formularza
	 * @return	array	- talica z polami formularza
	 */
	function &getForm($sAction, &$aCategories, &$aFields, &$aOptions) {
		global $aConfig;
		$aForm = array();
		$iI = 0;
		$aExtraJS = array();
		
		// dolaczenie klasy Form
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('contact', '', array('action' => $sAction), array(), true, array(), array(), '', '', '');
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		if (count($aCategories) > 1) {
			// utworzenie pola wyboru tematu maila
			// usuniecie pola recipients
			foreach ($aCategories as $iKey => $aCategory) {
				unset($aCategories[$iKey]['recipients']);
			}
			$aCategories = array_merge(
				array(array('value' => '0', 'label' => $aConfig['lang']['mod_'.$this->sModule]['choose'])),
				$aCategories
			);
			$aForm['fields'][0][$iI]['label'] = $oForm->GetLabelHTML('category', $aConfig['lang']['mod_'.$this->sModule]['choose_category']);
			$aForm['fields'][0][$iI]['input'] = $oForm->GetSelectHTML('category', $aConfig['lang']['mod_'.$this->sModule]['choose_category'], array('style'=>'width:'.$aConfig['default']['form_field_width'], 'class'=>'select'), $aCategories, $aData['ftype']);
		//	$aForm['fields'][0][$iI]['description'] = 'test';
			$aForm['fields'][0][$iI++]['type'] = 'select';
		}
		foreach ($aFields as $aField) {
			if ($aField['default_group'] == '0' && !isset($aForm['groups'][$aField['gid']])) {
				$aForm['groups'][$aField['gid']] = $aField['gname'];
			}
			switch ($aField['ftype']) {
				case 'text':
				case 'email':
					$aAttribs = array(
						'style' => 'width: '.$aConfig['default']['form_field_width'],
						'class' => 'text'
					);
					if ($aField['maxlength'] > 0) {
						$aAttribs['maxlength'] = $aField['maxlength'];
					}
					$aForm['fields'][$aField['gid']][$iI]['label'] = $oForm->GetLabelHTML('field'.$aField['id'], $aField['name'], $aField['required'] == '1' ? true : false);
					$aForm['fields'][$aField['gid']][$iI]['input'] = $oForm->GetTextHTML('field'.$aField['id'], $aField['name'], $aData['field'.$aField['id']], $aAttribs, '', $aField['ftype'], $aField['required'] == '1' ? true : false);
					$aForm['fields'][$aField['gid']][$iI]['description'] = $aField['description'];
					$aForm['fields'][$aField['gid']][$iI++]['type'] = $aField['ftype'];
				break;
				
				case 'textarea':
					$aAttribs = array(
						'style' => 'width: '.$aConfig['default']['form_textarea_width'],
						'class' => 'textarea'
					);
					if ($aField['trows'] > 0) {
						$aAttribs['rows'] = $aField['trows'];
					}
					$aForm['fields'][$aField['gid']][$iI]['label'] = $oForm->GetLabelHTML('field'.$aField['id'], $aField['name'], $aField['required'] == '1' ? true : false);
					$aForm['fields'][$aField['gid']][$iI]['input'] = $oForm->GetTextAreaHTML('field'.$aField['id'], $aField['name'], $aData['field'.$aField['id']], $aAttribs, 0, '', '', $aField['required'] == '1' ? true : false);
					$aForm['fields'][$aField['gid']][$iI]['description'] = $aField['description'];
					$aForm['fields'][$aField['gid']][$iI++]['type'] = $aField['ftype'];
				break;
				
				case 'checkbox':
				case 'agreement':
					$aAttribs = array(
						'class' => 'check'
					);
					$aForm['fields'][$aField['gid']][$iI]['label'] = $oForm->GetLabelHTML('field'.$aField['id'], !empty($aField['ftext']) ? $aField['ftext'] : $aField['name'], $aField['required'] == '1' ? true : false);
					$aForm['fields'][$aField['gid']][$iI]['input'] = $oForm->GetCheckBoxHTML('field'.$aField['id'], $aField['name'], $aAttribs, '', isset($aData['field'.$aField['id']]), $aField['required'] == '1' ? true : false);
					$aForm['fields'][$aField['gid']][$iI]['description'] = $aField['description'];
					$aForm['fields'][$aField['gid']][$iI]['type'] = $aField['ftype'];
					$iI++;
				break;
				
				case 'select':
					if (isset($aOptions[$aField['id']]) && !empty($aOptions[$aField['id']])) {
						$aAttribs = array(
							'style' => 'width: '.$aConfig['default']['form_field_width'],
							'class' => 'select'
						);
						$aOptions[$aField['id']] = array_merge(
							array(array('value' => '0', 'label' => $aConfig['lang']['mod_'.$this->sModule]['choose'])),
							$aOptions[$aField['id']]
						);
						$aForm['fields'][$aField['gid']][$iI]['label'] = $oForm->GetLabelHTML('field'.$aField['id'], $aField['name'], $aField['required'] == '1' ? true : false);
						$aForm['fields'][$aField['gid']][$iI]['input'] = $oForm->GetSelectHTML('field'.$aField['id'], $aField['name'], $aAttribs, $aOptions[$aField['id']], $aData['field'.$aField['id']], '', $aField['required'] == '1' ? true : false);
						$aForm['fields'][$aField['gid']][$iI]['description'] = $aField['description'];
						$aForm['fields'][$aField['gid']][$iI++]['type'] = $aField['ftype'];
					}
				break;
				
				case 'checkboxes':
					if (isset($aOptions[$aField['id']]) && !empty($aOptions[$aField['id']])) {
						$aForm['fields'][$aField['gid']][$iI]['label'] = $oForm->GetLabelHTML('field'.$aField['id'], $aField['name'], $aField['required'] == '1' ? true : false);
						$aForm['fields'][$aField['gid']][$iI]['input'] = $oForm->GetCheckBoxSetHTML('field'.$aField['id'], $aField['name'], $aOptions[$aField['id']], $aData['field'.$aField['id']], '', $aField['required'] == '1' ? true : false, false);
						$aForm['fields'][$aField['gid']][$iI]['description'] = $aField['description'];
						$aForm['fields'][$aField['gid']][$iI++]['type'] = $aField['ftype'];
					}
				break;
				
				case 'radios':
					if (isset($aOptions[$aField['id']]) && !empty($aOptions[$aField['id']])) {
						
						$aForm['fields'][$aField['gid']][$iI]['label'] = $oForm->GetLabelHTML('field'.$aField['id'], $aField['name'], $aField['required'] == '1' ? true : false);
						$aForm['fields'][$aField['gid']][$iI]['input'] = $oForm->GetRadioSetHTML('field'.$aField['id'], $aField['name'], $aOptions[$aField['id']], $aData['field'.$aField['id']], '', $aField['required'] == '1' ? true : false, false);
						$aForm['fields'][$aField['gid']][$iI]['description'] = $aField['description'];
						$aForm['fields'][$aField['gid']][$iI++]['type'] = $aField['ftype'];
					}
				break;
        
				case 'captcha':
					$aAttribs = array(
						'style' => 'width: '.$aConfig['default']['form_field_width'],
						'class' => 'text'
					);
					$aForm['fields'][$aField['gid']][$iI]['label'] = $oForm->GetLabelHTML('field'.$aField['id'], !empty($aField['ftext']) ? $aField['ftext'] : $aField['name'], $aField['required'] == '1' ? true : false);
					$aForm['fields'][$aField['gid']][$iI]['input'] = '<img src="/captcha.php" alt="" style="margin-bottom: -5px;" /> '.' &nbsp; &nbsp; '.$oForm->GetTextHTML('field'.$aField['id'], $aField['name'], $aData['field'.$aField['id']], $aAttribs, '', $aField['ftype'], $aField['required'] == '1' ? true : false);
					$aForm['fields'][$aField['gid']][$iI]['description'] = $aField['description'];
					$aForm['fields'][$aField['gid']][$iI]['type'] = $aField['ftype'];
					$iI++;
				break;
				
				case 'products':
					// lista produktow Oferty produktowej
					// pobranie listy produktow
					$aProducts =& $this->getProductsList();
					if (!empty($aProducts)) {
						$aAttribs = array(
							'style' => 'width: '.$aConfig['default']['form_field_width'],
							'class' => 'select'
						);
						$aOptions[$aField['id']] = array_merge(
							array(array('value' => '0', 'label' => $aConfig['lang']['mod_'.$this->sModule]['choose'])),
							$aProducts
						);
						// sprawdzenei czy zostalo przeslane ID przez GET
						if (!isset($aData['field'.$aField['id']]) && isset($_GET['id'])) {
							$aData['field'.$aField['id']] = $_GET['id'];
						}
						$aForm['fields'][$aField['gid']][$iI]['label'] = $oForm->GetLabelHTML('field'.$aField['id'], $aField['name'], $aField['required'] == '1' ? true : false);
						$aForm['fields'][$aField['gid']][$iI]['input'] = $oForm->GetSelectHTML('field'.$aField['id'], $aField['name'], $aAttribs, $aOptions[$aField['id']], $aData['field'.$aField['id']], '', $aField['required'] == '1' ? true : false);
						$aForm['fields'][$aField['gid']][$iI]['description'] = $aField['description'];
						$aForm['fields'][$aField['gid']][$iI++]['type'] = $aField['ftype'];
					}
				break;
			}
		}
		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();
		$aForm['JS'] = $oForm->GetJS();
		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix());
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix());
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator());
		return $aForm;
	} // end of getForm() method
	
	
	/**
	 * Metoda pobiera liste produktow z Oferty produktowej
	 * 
	 * @return	array ref
	 */
	function &getProductsList() {
		global $aConfig;
		
		$sSql = "SELECT A.id value, A.name label
						 FROM ".$aConfig['tabls']['prefix']."products A
						 JOIN ".$aConfig['tabls']['prefix']."menus_items B
						 ON B.id = A.page_id
						 WHERE B.published = '1' AND
						 			 A.published = '1' AND
						 			 B.language_id = ".$this->iLangId;
		return Common::GetAll($sSql);
	} // end of getProductsList() method
	
	
	/**
	 * Metoda pobiera produkt o podanym Id
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @return	array ref
	 */
	function &getProduct($iId) {
		global $aConfig;
		
		$sSql = "SELECT A.name
						 FROM ".$aConfig['tabls']['prefix']."products A
						 JOIN ".$aConfig['tabls']['prefix']."menus_items B
						 ON B.id = A.page_id
						 WHERE B.published = '1' AND
						 			 A.published = '1' AND
						 			 A.id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getProduct() method
	
	
	/**
	 * Metoda wysyla formularz
	 * 
	 * @param	string	$sAction	- link strony formularza
	 * @param	array	$aCategories	- kategorie formularza
	 * @param	array	$aFields	- pola formularza
	 * @param	array	$aOptions	- opcje pol formularza
	 * @return	bool	- wyslano; false: nie wyslano - blad
	 */
	function sendForm($sAction, &$aCategories, &$aFields, &$aOptions) {
		global $aConfig;
		$aForm = array();
		$sTheme = '';
		$iPrevGId = 0;
		$aLang =& $aConfig['lang']['mod_'.$this->sModule];
				
		// okreslenie odbiorcy(ow) formularza
		if (isset($_POST['category'])) {
			// zostalo przeslane Id kategorii - istnieje wiecej
			// niz jedna kategoria formularza
			foreach ($aCategories as $aCategory) {
				if ($_POST['category'] == $aCategory['value']) {
					$aRecipients = explode(';', $aCategory['recipients']);
				}
			}
		}
		else {
			// odbiorcy z domyslnych ustawien dla strony
			$aRecipients = explode(';', $this->aSettings['recipients']);
		}
		if (empty($aRecipients)) {
			// brak odbiorcow - mogl wystapic blad
			return false;
		}
		
		$aBody['body'] = '';
		// tworzenie ciala maila
		foreach ($aFields as $aField) {
			if ($aField['gid'] != $iPrevGId) {
				if ($iPrevGId != 0) {
					$aBody['body'] .= "\n";
				}
				$aBody['body'] .= $aField['gname']."\n";
			}
			$aField['name'] = strip($aField['name']);
			switch ($aField['ftype']) {
				case 'text':
				case 'email':
					if (trim($_POST['field'.$aField['id']]) != '') {
						$aBody['body'] .= $aField['name']." ".
													strip($_POST['field'.$aField['id']])."\n\n";
						if ($aField['sender'] == '1') {
							$aHeaders['From'] = $this->aSettings['sender'];
							$aHeaders['Reply-To'] = $_POST['field'.$aField['id']];
						}
					}
				break;
				
				case 'textarea':
					if (trim($_POST['field'.$aField['id']]) != '') {
						$aBody['body'] .= $aField['name']."\n".
													strip($_POST['field'.$aField['id']])."\n\n";
					}
				break;
				
				case 'checkbox':
				case 'agreement':
					if (isset($_POST['field'.$aField['id']])) {
						$aBody['body'] .= $aField['name']." ".
															$aConfig['lang']['common']['yes']."\n\n";
					}
				break;
        
				case 'captcha':
					if (isset($_POST['field'.$aField['id']])) {
            /*
						$aBody['body'] .= $aField['name']." ".
															$aConfig['lang']['common']['yes']."\n\n";
             */
            $sCaptcha = $_POST['field'.$aField['id']];
            if (!isset($_COOKIE['captcha_sec']) || base64_encode(md5($sCaptcha)) != $_COOKIE['captcha_sec']) {
              $aConfig['_tmp']['message']['text'] .= '<li>'._('Wprowadzono nieprawidłowy kod z obrazka').'</li>';
              $aConfig['_tmp']['message']['error'] = true;
              unset($_COOKIE['captcha_sec']);
              return false;
            } else {
              unset($_COOKIE['captcha_sec']);
            }
					}
				break;

				case 'select':
				case 'radios':
					if (!empty($_POST['field'.$aField['id']])) {
						$aBody['body'] .= $aField['name']." ";
						foreach ($aOptions[$aField['id']] as $aOption) {
							if ($aOption['value'] == $_POST['field'.$aField['id']]) {
								$aBody['body'] .= strip($aOption['label'])."\n\n";
							}
						}
					}
				break;
				
				case 'checkboxes':
					if (!empty($_POST['field'.$aField['id']])) {
						$aBody['body'] .= $aField['name']."\n";
						foreach ($aOptions[$aField['id']] as $aOption) {
							if (in_array($aOption['value'], $_POST['field'.$aField['id']])) {
								$aBody['body'] .= "\t- ".strip($aOption['label'])."\n";
							}
						}
						$aBody['body'] .= "\n";
					}
				break;
				
				case 'products':
					if (!empty($_POST['field'.$aField['id']]) && ($sProduct = $this->getProduct((double) $_POST['field'.$aField['id']])) != '') {
						$aBody['body'] .= $aField['name']." ".$sProduct;
						$aBody['body'] .= "\n\n";
					}
				break;
			}
			$iPrevGId = $aField['gid'];
		}
		
		// temat
		$aHeaders['Subject'] = changeCharcodes($this->aSettings['subject'], 'utf8', 'no');
		
		// wysylanie maila
		$aGetParams = array('head_charset'=>$aConfig['default']['charset'],
												'text_charset'=>$aConfig['default']['charset']);

		foreach ($aRecipients as $sRecipient) {
			if (!Common::SendMimeEmail($sRecipient,
																 $aHeaders,
																 $aBody,
																 array(),
																 $aGetParams,
																 array(),
																 array(),
																 'mail',
																 '-f'.$aHeaders['From'])) {
				return false;								 	
			}
		}
		return true;
	} // end of sendForm() method
} // end of Module Class
?>