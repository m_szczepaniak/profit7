<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_stats;

use Admin;
use DatabaseManager;
use Module;
use omniaCMS\lib\interfaces\ModuleEntity;
use orders\getTransportList;
use Smarty;
use View;

/**
 * Description of Module_packing
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__stats__completing implements ModuleEntity
{

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    private $sModule;

    /**
     *
     * @var Module
     */
    private $oModuleZamowienia;

    /**
     *
     * @var getTransportList
     */
    private $oGetTransport;

    protected $statsType = '1';

    /**
     *
     * @param Admin $oClassRepository
     */
    public function __construct(Admin $oClassRepository)
    {
        global $aConfig;

        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    /**
     *
     * @return string
     */
    public function doDefault()
    {
        $this->markPackingUserAsLoggedOut();
        return $this->PackingView();
    }

    /**
     *
     * @global array $aConfig
     */
    public function doDetails()
    {

        // zapamietanie opcji
        rememberViewState($this->sModule . '_details');

        $aRecords = $this->getUserDatePackingTimes($_GET['f_date'], $_GET['user_id']);

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Statystyki pakowania')),
            'refresh' => true,
            'search' => false,
            'checkboxes' => false,
            'per_page' => false,
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'order_number',
                'content' => _('Zamówienie'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'name',
                'content' => _('Tytuł'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'czas',
                'content' => _('Czas'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'sum_sc_weight',
                'content' => _('Waga'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'quantity',
                'content' => _('Egz.'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '180'
            )
        );

        $pView = new View('orders_packing_times', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);
        if (!empty($aRecords)) {
            $aColSettings = array();
            $pView->AddRecords($aRecords, $aColSettings);
        }
        // przyciski stopki stopki do widoku
        $aRecordsFooter = array(array('go_back'));
        $aRecordsFooterParams = array();
        $pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
        return $pView->Show();
    }

    /**
     *
     * @param date $dDate
     * @param int $iUserId
     * @return array
     */
    private function getUserDatePackingTimes($dDate, $iUserId)
    {

        $sMyqlDate = \FormatDate($dDate);

        $sSql = '
      SELECT O.order_number, OI.name, OILIT.time, (OILIT.sc_weight * OILIT.sc_quantity) AS sum_sc_weight,  OILIT.sc_quantity AS quantity, OILIT.created
      FROM orders_items_lists_items_times AS OILIT
      JOIN orders_items_lists_items AS OILI
        ON OILI.id = OILIT.id
      JOIN orders_items AS OI
        ON OI.id = OILI.orders_items_id
      JOIN orders AS O
        ON O.id = OI.order_id
      WHERE 
        OILIT.type = "' . $this->statsType . '" AND
        OILIT.user_id = "' . $iUserId . '" AND DATE(OILIT.created) = "' . $sMyqlDate . '" 
      ORDER BY ' . (isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' OILIT.created DESC ');
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     *
     */
    private function markPackingUserAsLoggedOut()
    {

        $sSql = '
  UPDATE login_history
  SET
    session_expiry_date = (login_date + INTERVAL 10 MINUTE)
  WHERE session_id IN (
    SELECT session_id FROM (
      SELECT session_id
      FROM login_history AS LH
      JOIN users AS U
        ON U.id = LH.user_id AND priv_order_5_status = "1"
      WHERE 
      (NOW() - INTERVAL 1 MONTH) < LH.login_date AND
      (LH.login_date + INTERVAL 10 MINUTE) < NOW() AND
      LH.logout_date IS NULL AND
      LH.session_expiry_date IS NULL
    ) AS TMP
  )
  ORDER BY login_date DESC
  ';
        $this->pDbMgr->Query('profit24', $sSql);
    }


    /**
     *
     * @global array $aConfig
     * @return string
     */
    private function PackingView()
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Statystyki pakowania')),
            'refresh' => true,
            'search' => false,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'packing',
                'content' => _('Pakujący'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'quantity',
                'content' => _('Listy'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'weight',
                'content' => _('Waga'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'quantity',
                'content' => _('Egz.'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'time_netto',
                'content' => _('Czas netto'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'time_brutto',
                'content' => _('Czas brutto'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '65'
            )
        );
        // pobranie liczby wszystkich uzytkownikow
        $aRecords = $this->getTimesQuantityUsers($_POST['f_date'], $_POST['f_user_id']);
        $iRowCount = count($aRecords);
        if ($iRowCount == 0 && !isset($_GET['reset'])) {
            // resetowanie widoku
            resetViewState($this->sModule);
            // ponowne okreslenie liczny rekordow
            $aRecords = $this->getTimesQuantityUsers($_POST['f_date'], $_POST['f_user_id']);
            $iRowCount = count($iRowCount);
        }

        $pView = new View('users', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);
        if (!isset($_POST['f_date'])) {
            $_POST['f_date'] = date('d-m-Y');
        }
        $pView->AddFilter('f_date', _('Data'), [], $_POST['f_date'], 'date');
        $pView->AddFilter('f_user_id', _('Użytkownik'), addDefaultValue($this->getUsers()), $_POST['f_user_id']);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'user_id' => array(
                    'show' => false
                ),
                'action' => array(
                    'actions' => array('details'),
                    'params' => array(
                        'details' => array('do' => 'details', 'f_date' => $_POST['f_date'], 'user_id' => '{user_id}'),
                    ),
                    'show' => false,
                )
            );
            $pView->AddRecords($aRecords, $aColSettings);
        }

        // przyciski stopki stopki do widoku
        $aRecordsFooter = array();
        $aRecordsFooterParams = array();
        $pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
        return $pView->Show();
    }

    /**
     *
     * @return array
     */
    private function getUsers()
    {

        $sSql = 'SELECT id AS value, CONCAT(name, " ", surname) AS label
             FROM users
             WHERE active = "1"
             ORDER BY label';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     *
     * @param string $sDate
     * @param int $iUserId
     */
    private function getTimesQuantityUsers($sDate = '', $iUserId = 0)
    {

        $sMyqlDate = NULL;
        if ($sDate != '') {
            $sMyqlDate = \FormatDate($sDate);
        }
        $sSql = 'SELECT 
        CONCAT(U.name, " ", U.surname) as packing, 
        COUNT(DISTINCT OILI.orders_items_lists_id),
        SUM(sc_weight * sc_quantity) as weight,
        SUM(sc_quantity) AS quantity, 
        SEC_TO_TIME(SUM((time/1000))), 
      (
        SELECT 
          SEC_TO_TIME(
            IFNULL(
              SUM(
                TIME_TO_SEC(
                  TIMEDIFF( LH.session_expiry_date, LH.login_date )
                )
              ) 
            , 0)
             + 
            IFNULL(
              SUM(
                TIME_TO_SEC(
                  TIMEDIFF( LH.logout_date, LH.login_date )
                )
              )
            , 0)
          )
        FROM login_history AS LH
        WHERE LH.user_id = OILIT.user_id AND
        DATE(LH.login_date) = ' . ($sMyqlDate != '' ? '"' . $sMyqlDate . '"' : ' CURDATE() ') . '
          AND (LH.session_expiry_date IS NOT NULL OR LH.logout_date IS NOT NULL)
      ),
      OILIT.user_id
             FROM orders_items_lists_items_times AS OILIT
             JOIN orders_items_lists_items AS OILI
              ON OILI.id = OILIT.id
             LEFT JOIN users AS U
              ON U.id = OILIT.user_id
             WHERE 
             OILIT.type = "' . $this->statsType . '" AND
             DATE(OILIT.created) = ' . ($sMyqlDate != '' ? '"' . $sMyqlDate . '"' : ' CURDATE() ') . '
              ' . ($iUserId > 0 ? ' AND OILIT.user_id = ' . $iUserId : '') . '
             GROUP BY OILIT.user_id
             ORDER BY ' . (isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' quantity DESC ') . '
             ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    public function getMsg()
    {
        return $this->sMsg;
    }
}
