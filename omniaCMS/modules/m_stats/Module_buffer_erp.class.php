<?php

use omniaCMS\modules\m_stats\CompletationItems;
use omniaCMS\modules\m_stats\StatsListsTypes;
/**
 * Skrypt statystyk serwisu.
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */

class Module__stats__buffer_erp {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  private $pDbMgr;
  
  function __construct($pSmarty) {
    global $pDbMgr;

    $this->pDbMgr = $pDbMgr;
    

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
		if ($_SESSION['user']['type'] === 0) {
			if (!hasModulePrivileges($_GET['module_id'])){
				showPrivsAlert($pSmarty);
				return;
			}
		}
    

    $this->showStats($pSmarty);
  }
  
  /**
   * 
   * @param Smarty $pSmarty
   */
  private function showStats(&$pSmarty) {

      $sSql = '
SELECT O.id, O.order_number, O.order_date, O.invoice_date 
FROM orders AS O 
WHERE O.erp_send_ouz IS NULL 
AND O.invoice_id IS NOT NULL 
AND O.order_status IN("4", "3") 
AND DATE(O.invoice_date) >= "2015-05-04" 
ORDER BY O.invoice_date ASC
';
      $stats = Common::GetAll($sSql);

      $str .= '
<h2>Czeka na wysłanie do streama ('.count($stats).'): </h2>
<table class="statsTable">';
      $str .= '<td>'.implode('</td><td>', array_keys($stats[0])).'</td>';
      foreach ($stats as $row) {
          $str .= '<tr><td>'.implode('</td><td>', $row).'</td></tr>';
      }
      $str .= '</table>';


      $sSql = '
SELECT id, order_number, invoice_id, invoice2_id,
                value_netto, value_brutto, transport_cost, transport_vat,
                to_pay, second_invoice
             FROM orders AS O
             WHERE
              O.invoice_id IS NOT NULL
              AND O.order_status IN("4", "3")
              AND O.erp_export IS NULL
              AND O.erp_send_ouz IS NOT NULL
              AND O.order_date > "2015-09-25"
';
      $stats = Common::GetAll($sSql);

      $str .= '<table class="statsTable">

<h2>Czeka na sprawdzenie wartości streama ('.count($stats).'): </h2>
';
      $str .= '<td>'.implode('</td><td>', array_keys($stats[0])).'</td>';
      foreach ($stats as $row) {
          $str .= '<tr><td>'.implode('</td><td>', $row).'</td></tr>';
      }
      $str .= '</table>';





    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$str);
  }
}