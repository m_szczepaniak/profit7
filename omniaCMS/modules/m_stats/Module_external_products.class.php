<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-15
 * @copyrights Marcin Chudy - Profit24.pl
 */
use Pager\Pagination;

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_USER_NOTICE);
//ini_set('display_errors', 'On');

class Module__stats__external_products
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    var $generateCsv = false;

    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    private $pDbMgr;

    private $sources = [
        'profit_g_status' => 'Stock',
        'profit_e_status' => 'Profit E',
        'profit_j_status' => 'Profit J',
        'azymut_stock' => 'Azymut',
        'siodemka_stock' => 'Siodemka',
        'ateneum_stock' => 'Ateneum',
        'dictum_stock' => 'Dictum',
        'platon_stock' => 'Platon',
        'all' => 'Wszystkie'
    ];


    function __construct($pSmarty)
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;


        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        if (isset($_POST['csv']) && $_POST['csv'] != 0) {
            $this->generateCsv = true;
        }

        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        if (isset($_GET['pid'])) {
            $iPId = intval($_GET['pid']);
        }
        $this->aPrivileges =& $_SESSION['user']['privileges'];
        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }


        if (isset($_POST['per_page'])) {
            $sDo = 'get_stats';
        }

        if (isset($_GET['per_page'])) {
            $_POST['per_page'] = $_GET['per_page'];
            $sDo = 'get_stats';
        }

        if (isset($_GET['page'])) {
            $_POST['page'] = $_GET['page'];
            $sDo = 'get_stats';
        }

        switch ($sDo) {
            default:
                $this->Show($pSmarty);
                break;

            case 'get_stats':
                $this->GetStats($pSmarty);
                break;
        }
    }


    /**
     * Metoda wyświetla formularz do wyboru zakresu dat w obrębie których ma generować się raport
     *
     * @global array $aConfig
     * @param object $pSmarty
     */
    private function Show($pSmarty)
    {
        global $aConfig;

        $sHTML = $this->ShowHTML();

        $js = '
            <script type=text/javascript>
            $(document).ready(function(){

              $("#publisher_aci").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetPublishers.php"
                });
            });
            </script>
        ';


        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($sHTML).$js);
    }// end of Show() method


    /**
     * Metoda wyświetla kod html widoku
     *
     * @global array $aConfig
     * @return string HTML
     */
    private function ShowHTML()
    {
        global $aConfig, $pDbMgr;

        $aData = array();
        if (isset($_POST)) {
            $aData = $_POST;
        }

        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('stat_ordered_items', _('Najczęściej zamawiane pozycje'));

        $pForm->AddHidden('do', 'get_stats');

        $sources = [
            [
                'label' => 'Wszystkie (Stock, E i J)',
                'value' => 'all'
            ],
            [
                'label' => 'Stock',
                'value' => 'profit_g_status'
            ],
            [
                'label' => 'Profit J',
                'value' => 'profit_j_status'
            ],
            [
                'label' => 'Profit E',
                'value' => 'profit_e_status'
            ]
        ];

        $minimumDiff = Common::formatPrice(0.99);

        $pForm->AddSelect('source_not', _('Zródło'), [], $sources, [], '', false);
        $pForm->AddText('publisher_aci', _("Wydawca"), array('id' => 'publisher_aci', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', false, false);
        $pForm->AddCheckBox('csv', _('Generuj csv'), [], [], [], '', false);
        $pForm->AddText('minimum_diff', 'Minimalna różnica', $minimumDiff, array('maxlength' => 12, 'style' => 'width: 75px;'), '', 'ufloat', false);

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Generuj')) . '&nbsp;&nbsp;');
        return $pForm->ShowForm();
    }// end of ShowHTML() method


    private function getBooks()
    {
        $books = $this->getBooksFromDb();
        $booksToReturn = [];
        $minimumDiff = '-'.Common::formatPrice2($_SESSION['minimum_diff']);

        foreach($books as $book) {

            $lowestAvailableOurPrice = $this->getlowestAvailableOurPrice($book);


            if (null == $lowestAvailableOurPrice) {
                continue;
            }

            $theirPrices = $this->getTheirPrices($book);

            foreach($theirPrices as $price) {

                if ($lowestAvailableOurPrice['price'] > $price['price']) {

                    $book['internal_source'] = $this->sources[$lowestAvailableOurPrice['name']];
                    $book['internal_source_price'] = $lowestAvailableOurPrice['price'];
                    $book['internal_source_amount'] = $lowestAvailableOurPrice['amount'];
                    $book['external_source'] = $this->sources[$price['name']];
                    $book['external_source_price'] = $price['price'];
                    $book['external_source_amount'] = $price['amount'];
                    $book['price_difference'] = Common::formatPrice2($book['external_source_price'] - $book['internal_source_price']);

                    if ($book['price_difference'] > $minimumDiff) {

                        continue;
                    }

                    $booksToReturn[] = $book;
                    break;
                }
            }
        }

        usort($booksToReturn, function ($item1, $item2) {
            if ($item1['price_difference'] == $item2['price_difference']) return 0;
            return $item1['price_difference'] < $item2['price_difference'] ? -1 : 1;
        });

        return $booksToReturn;
    }

    private function isEmpty($value)
    {
        if ($value != '' && $value != null && $value > 0) {

            return false;
        }

        return true;
    }

    private function getTheirPrices(array $book)
    {
        $ourStocks = [];

        if (false === $this->isEmpty($book['azymut_stock']) && $book['azymut_wholesale_price'] > 0 && $book['azymut_status'] == 1) {
            $ourStocks['azymut_stock'] = $book['azymut_wholesale_price'];
        }

        if (false === $this->isEmpty($book['siodemka_stock']) && $book['siodemka_wholesale_price'] > 0 && $book['siodemka_status'] == 1) {
            $ourStocks['siodemka_stock'] = $book['siodemka_wholesale_price'];
        }

        if (false === $this->isEmpty($book['ateneum_stock']) && $book['ateneum_wholesale_price'] > 0 && $book['ateneum_status'] == 1) {
            $ourStocks['ateneum_stock'] = $book['ateneum_wholesale_price'];
        }

        if (false === $this->isEmpty($book['dictum_stock']) && $book['dictum_wholesale_price'] > 0 && $book['dictum_status'] == 1) {
            $ourStocks['dictum_stock'] = $book['dictum_wholesale_price'];
        }

        if (false === $this->isEmpty($book['platon_stock']) && $book['platon_wholesale_price'] > 0 && $book['platon_status'] == 1) {
            $ourStocks['platon_stock'] = $book['platon_wholesale_price'];
        }

        asort($ourStocks);

        $finalRows = [];

        foreach($ourStocks as $name => $ourStock) {

            $finalRows[] = [
                'name' => $name,
                'price' => $ourStock,
                'amount' => $book[$name]
            ];
        }

        return $finalRows;
    }

    private function getPriceFromSupplies(array $book)
    {
        if ($book['profit_g_wholesale_price'] == 0) {


        }
    }

    function array2csv2(array &$array)
    {
        $headers = [
            'Lp',
            'EAN',
            'Nazwa',
            'Wydawnictwo',
            'Ilość u nas',
            'Cena u nas',
            'Nasze źródło',
            'Ilość',
            'Cena',
            'Nazwa źródła',
            'Różnica w cenie',
        ];

        if (count($array) == 0) {
            return null;
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="data.csv";');
        ob_start();
        $df = fopen('php://output', 'w');
        fputcsv($df, $headers);
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        $data = ob_get_clean();

        echo $data;
        die;
    }

    private function getlowestAvailableOurPrice(array $book)
    {
        $ourStocks = [];
        $selectedSource = $_POST['source_not'];
//        $book = $this->fillEmptyPrices($book);


        if ($book['profit_g_status'] > 0 && ($selectedSource == 'all' || $selectedSource == 'profit_g_status')) {
            $ourStocks['profit_g_status'] = $book['profit_g_wholesale_price'];
        }

        if ($book['profit_j_status'] > 0 && ($selectedSource == 'all' || $selectedSource == 'profit_j_status')) {
            $ourStocks['profit_j_status'] = $book['profit_j_wholesale_price'];
        }

        if ($book['profit_e_status'] > 0 && ($selectedSource == 'all' || $selectedSource == 'profit_e_status')) {
            $ourStocks['profit_e_status'] = $book['profit_e_wholesale_price'];
        }

        asort($ourStocks);

        foreach($ourStocks as $key => $value) {

            return [
                'name' => $key,
                'price' => $value,
                'amount' => $book[$key]
            ];
        }
    }
    /**
     * @return array
     */
    private function getBooksFromDb()
    {
        global $pDbMgr;

        $sql = "
        SELECT PS.id, P.name as title, PPU.name as publisher_name, P.ean_13 as ean, P.isbn,
         PS.azymut_stock, PS.azymut_wholesale_price, PS.siodemka_stock, PS.siodemka_wholesale_price,
        PS.ateneum_stock, PS.ateneum_wholesale_price, PS.dictum_stock, PS.dictum_wholesale_price, PS.platon_stock,
        PS.platon_wholesale_price, PS.profit_g_status, PS.profit_g_wholesale_price, PS.profit_j_status, PS.profit_j_wholesale_price,
        PS.profit_e_status, PS.profit_e_wholesale_price,
        PS.azymut_status, PS.dictum_status, PS.ateneum_status, PS.platon_status, PS.siodemka_status
        FROM `products_stock` AS PS
        JOIN products AS P ON P.id = PS.id
        LEFT JOIN products_publishers AS PPU ON P.publisher_id = PPU.id
        WHERE (PS.profit_g_status > 0 || PS.profit_e_status > 0 || PS.profit_j_status > 0)
        AND
        (
           (
               (PS.azymut_wholesale_price < PS.profit_g_wholesale_price OR PS.azymut_wholesale_price < PS.profit_e_wholesale_price OR PS.azymut_wholesale_price < PS.profit_j_wholesale_price) AND PS.azymut_stock != 0 AND PS.azymut_stock != '' AND PS.azymut_stock is not null AND PS.azymut_wholesale_price > 0
           )
            OR
            (
               (PS.siodemka_wholesale_price < PS.profit_g_wholesale_price OR PS.siodemka_wholesale_price < PS.profit_e_wholesale_price OR PS.siodemka_wholesale_price < PS.profit_j_wholesale_price) AND PS.siodemka_stock != 0 AND PS.siodemka_stock != '' AND PS.siodemka_stock is not null AND PS.siodemka_wholesale_price > 0
           )
                OR
            (
               (PS.dictum_wholesale_price < PS.profit_g_wholesale_price OR PS.dictum_wholesale_price < PS.profit_e_wholesale_price OR PS.dictum_wholesale_price < PS.profit_j_wholesale_price) AND PS.dictum_stock != 0 AND PS.dictum_stock != '' AND PS.dictum_stock is not null AND PS.dictum_wholesale_price > 0
           )
                OR
            (
               (PS.ateneum_wholesale_price < PS.profit_g_wholesale_price OR PS.ateneum_wholesale_price < PS.profit_e_wholesale_price OR PS.ateneum_wholesale_price < PS.profit_j_wholesale_price) AND PS.ateneum_stock != 0 AND PS.ateneum_stock != '' AND PS.ateneum_stock is not null AND PS.ateneum_wholesale_price > 0
           )
                    OR
            (
               (PS.platon_wholesale_price < PS.profit_g_wholesale_price OR PS.platon_wholesale_price < PS.profit_e_wholesale_price OR PS.platon_wholesale_price < PS.profit_j_wholesale_price) AND PS.platon_stock != 0 AND PS.platon_stock != '' AND PS.platon_stock is not null AND PS.platon_wholesale_price > 0
           )
        )
        ";

        $publisher = $_SESSION['publisher_aci'];

        if (!empty($publisher)) {

            $publisherId = $pDbMgr->GetOne('profit24', "SELECT id FROM products_publishers WHERE name = '$publisher' LIMIT 1");

            if (!empty($publisherId)) {


                $sql .= "
                    AND P.publisher_id = $publisherId
                ";
            }
        }

//        AND PS.azymut_wholesale_price > 0
//    AND PS.siodemka_wholesale_price > 0
//    AND PS.dictum_wholesale_price > 0
//    AND PS.platon_wholesale_price > 0
//    AND PS.ateneum_wholesale_price > 0


        return $pDbMgr->GetAll('profit24', $sql);
    }

    /**
     * Pobiera html formularza
     *
     * @param type $pSmarty
     * @return type
     */
    private function GetStats(&$pSmarty)
    {

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty);
            return;
        }

        // pagination
        if (isset($_POST['per_page'])) {
            $_SESSION['per_page'] = $_POST['per_page'];
        }

        if (isset($_SESSION['per_page'])) {
            $_POST['per_page'] = $_SESSION['per_page'];
        }

        // selected source
        if (isset($_POST['source_not'])) {
            $_SESSION['source_not'] = $_POST['source_not'];
        }

        if (isset($_SESSION['source_not'])) {
            $_POST['source_not'] = $_SESSION['source_not'];
        }

        // selected publisher
        if (isset($_POST['publisher_aci'])) {
            $_SESSION['publisher_aci'] = $_POST['publisher_aci'];
        }

        if (isset($_SESSION['publisher_aci'])) {
            $_POST['publisher_aci'] = $_SESSION['publisher_aci'];
        }

        // selected publisher
        if (isset($_POST['minimum_diff'])) {
            $_SESSION['minimum_diff'] = $_POST['minimum_diff'];
        }

        if (isset($_SESSION['minimum_diff'])) {
            $_POST['minimum_diff'] = $_SESSION['minimum_diff'];
        }

        include_once('View/View.class.php');

        $aHeader = array(
            'refresh' => true,
            'search' => false,
            'checkboxes' => false,
            'form' => true
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'content' => _('Lp'),
            ),
            array(
                'content' => _('EAN'),
                'width' => '10%'
            ),
            array(
                'content' => _('Nazwa'),
                'width' => '25%'
            ),
            array(
                'content' => _('Wydawnictwo'),
                'width' => '15%'
            ),
            array(
                'content' => _('Ilość u nas'),
                'width' => '3%'
            ),
            array(
                'content' => _('Cena u nas'),
                'width' => '5%'
            ),
            array(
                'content' => _('Nasze źródło'),
                'width' => '8%'
            ),
            array(
                'content' => _('Ilość'),
                'width' => '3%'
            ),
            array(
                'content' => _('Cena'),
                'width' => '5%',
            ),
            array(
                'content' => _('Nazwa źródła'),
                'width' => '8%',
            ),
            array(
                'content' => _('Różnica w cenie'),
                'width' => '8%',
            ),
        );


        $rows = $this->getBooks();

        $finalRows = [];
        $i = 1;
        foreach($rows as $row) {

            $finalRows[] = [
                'Lp' => $i,
                'ean' => empty($row['ean']) ? $row['isbn'] : $row['ean'],
                'title' => $row['title'],
                'publisher_name' => $row['publisher_name'],
                'internal_source_amount' => $row['internal_source_amount'],
                'internal_source_price' => $row['internal_source_price'],
                'internal_source' => $row['internal_source'],
                'external_source_amount' => $row['external_source_amount'],
                'external_source_price' => $row['external_source_price'],
                'external_source' => $row['external_source'],
                'price_difference' => $this->generateCsv == false ? '<b style="color:red">'.$row['price_difference'].'</b>' : $row['price_difference'],
            ];

            $i++;
        }

        $perPage = $_POST['per_page'] != null ? $_POST['per_page'] : 20;
        $cPage = $_POST['page'] != null ? $_POST['page'] : 1;

        $pagination = new Pagination($finalRows, $perPage, $cPage);
        $aBooks = $pagination->getCurrentRows();

        $pView = new View('ordered_itm', $aHeader, $aAttribs);
        $pView->AddPager($pagination->getRowsAmount());
//        $pView->AddPager($pagination->getRowsAmount());
        $pView->AddRecordsHeader($aRecordsHeader);

        // ustawienia dla poszczegolnych kolumn rekordu
        $aColSettings = array(

        );

        // dodanie rekordow do widoku
        $pView->AddRecords($aBooks, $aColSettings);
        $aRecordsFooter = array(
            array()
        );

        $pView->AddRecordsFooter($aRecordsFooter);

        if ($this->generateCsv == true) {
            $this->array2csv2($finalRows);
        }

        // zawsze wyświetlamy show
        $sHTML = '<h1>Wybrane źródło: '.$this->sources[$_POST['source_not']].' <span style="color:red">Ilość produktów: '.$pagination->getRowsAmount().'</span></h1>';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sHTML . $pView->Show());
//        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '')  . $pView->Show());
    }
}

