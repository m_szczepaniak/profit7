<?php

use omniaCMS\modules\m_stats\CompletationItems;
use omniaCMS\modules\m_stats\StatsListsTypes;
/**
 * Skrypt statystyk serwisu.
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */

class Module__stats__v3 {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  private $pDbMgr;
  
  function __construct($pSmarty) {
    global $pDbMgr, $aConfig;

      $this->pDbMgr = $pDbMgr;
      $this->sErrMsg = '';
      $this->iLangId = $_SESSION['lang']['id'];
      $this->sModule = $_GET['module'];

      if (isset($_GET['action'])) {
          $this->sModule .= '_'.$_GET['action'];
      }

      if (isset($_GET['pid'])) {
          $iPId = intval($_GET['pid']);
      }
      $this->aPrivileges =& $_SESSION['user']['privileges'];
  // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
      if ($_SESSION['user']['type'] === 0) {
          if (!hasModulePrivileges($_GET['module_id'])){
              showPrivsAlert($pSmarty);
              return;
          }
      }

      $this->getTransportMeans();


      $sKey = 'AA_module_statistic_content_v3';
      if ($_SESSION['user']['name'] != 'agolba' && $aConfig['common']['status'] != 'development' && class_exists('memcached')) {

        $oMemcache = new Memcached();
        $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
        $sHtml = $oMemcache->get($sKey);
        if (isset($sHtml) && $sHtml != '') {
          $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sHtml);
          return;
        }
      }


      if ($_SESSION['user']['name'] != 'agolba' && $aConfig['common']['status'] != 'development' && class_exists('memcached')) {
        $oMemcache->set($sKey, 'AKTUALNIE JEST GENEROWANY WIDOK', 60);
      }

      $secondsWait = 3600;
      $dateTime = new DateTime('now');
      $sHtml = $this->showStats($pSmarty);
      $sHtml = 'Wygenerowano przez '.$_SESSION['user']['name'].' : '.$dateTime->format('d.m.Y H:i:s').' Za godzine można wygenerować następny <br />'.$sHtml;
      $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sHtml);
      if ($_SESSION['user']['name'] != 'agolba' && $aConfig['common']['status'] != 'development' && class_exists('memcached')) {
        $oMemcache->set($sKey, $sHtml, $secondsWait);
        return;
      }
  }


  /**
   * @param $ordersGroupWebsite
   */
  public function parseGroupWebsites($ordersGroupWebsite) {
    global $aConfig;

      foreach ($ordersGroupWebsite as &$items) {
        foreach ($items as &$item) {
          $item['ico'] = 'gfx/'.$aConfig['invoice_website_symbol_' . $item['website_id']].'.gif';
        }

      }
      return $ordersGroupWebsite;
  }
  
  /**
   * 
   * @param Smarty $pSmarty
   */
  private function showStats(&$pSmarty) {
    $aData = array();


    $aPlannedOrderItems = $this->getOpenPlannedItemsQuantityOrders();
    $aFirst = $this->getOpenPlannedOrders();
    $aFirst = $this->addLinkToOrders($aFirst, false);
    $aSecond = $this->getRealizedOrConfirmedToday();

    $ordersConfirmedTodayGroupWebsite = $this->getRealizedOrConfirmedTodayGroupWebsite();
    $ordersConfirmedTodayGroupWebsite = $this->parseGroupWebsites($ordersConfirmedTodayGroupWebsite);
    $ordersGroupWebsite = $this->getOpenPlannedOrdersGroupWebsites();
    $ordersGroupWebsite = $this->parseGroupWebsites($ordersGroupWebsite);

    $aData['table_dates'] = array_merge_recursive($aFirst, $aSecond);
    
    $statsListTypes = new StatsListsTypes($this->pDbMgr, $pSmarty, $this->aTransport);
    $sListTypeHTML = $statsListTypes->getHTML(date("Y-m-d"));
    
    $completationItems = new CompletationItems($this->pDbMgr, $pSmarty, $this->aTransport);
    $sCompletationHTML = $completationItems->getHTML(date("Y-m-d"));


    $aData['orders_items_planned_quantity'] = $aPlannedOrderItems;
    $aData['orders_confirmed_group_website'] = $ordersConfirmedTodayGroupWebsite;
    $aData['orders_group_website'] = $ordersGroupWebsite;
    $aData['sorting'] = $this->getSorting();
    $aData['out_sorting'] = $this->getOutSorting();
    $aData['confirm_or_realized'] = $this->getConfirmedOrRealized();
    
    $aSingle['sorting'] = $this->getSortingSingle();
    $aSingle['out_sorting'] = $this->getOutSortingSingle();
    $aSingle['confirm_or_realized'] = $this->getConfirmedOrRealizedSingle();
    $aData = $this->arrayMerge($aData, $aSingle);
    $aData = $this->addSummaries($aData);

      $sSql = 'SELECT count(O.id) 
               FROM orders AS O
                JOIN orders_transport_means AS OTM
                    ON O.transport_id = OTM.id AND OTM.symbol <> "odbior-osobisty"
               WHERE
               O.status_3_update > "'.date('Y-m-d').' 00:00:00"
                
               ';
      $iConfirmedCount = \Common::GetOne($sSql);

      $sSql = 'SELECT count(O.id) 
               FROM orders AS O
                JOIN orders_transport_means AS OTM
                    ON O.transport_id = OTM.id AND OTM.symbol <> "odbior-osobisty"
               WHERE O.status_4_update > "'.date('Y-m-d').' 00:00:00"
               
               ';
      $iRealizedCount = \Common::GetOne($sSql);
      $sSatsHTML = '<br /><br /><div>DZISIAJ ZATWIERDZONE(bez odbiorów osobistych) - <strong>'.$iConfirmedCount.'</strong><br />DZISIAJ ZREALIZOWANE(bez odbiorów osobistych) - <strong>'.$iRealizedCount.'</h2></div> ';

    $pSmarty->assign_by_ref('sListTypeHTML', $sListTypeHTML);
    $pSmarty->assign_by_ref('sCompletationHTML', $sCompletationHTML);
    $pSmarty->assign_by_ref('aData', $aData);
    $sHtml = $pSmarty->fetch('stats_v3.tpl');
    $pSmarty->clear_assign('sListTypeHTML');
    $pSmarty->clear_assign('sCompletationHTML');
    $pSmarty->clear_assign('aData');
      return $sSatsHTML.$sHtml;
  }
  
  /**
   * 
   * @param array $aData
   * @return array
   */
  private function addSummaries($aData) {
    
    foreach ($aData as $sType => $aItems) {
      foreach ($aItems as $mItemKey => $aItems) {
        if (is_array($aItems)) {
          foreach ($aItems as $sKey => $sValue) {
            if (stristr($sKey, 'count')) {
              $aData[$sType]['SUMA'][$sKey] += intval($sValue);
            }
          }
        }
      }
    }
    return $aData;
  }// end of addSummaries() method
  
  /**
   * 
   */
  private function arrayMerge($aData, $aSingle) {
    foreach ($aData as $sType => $aItemsData) {
      foreach ($aItemsData as $sKey => $aValue) {
        if (is_array($aValue) && isset($aSingle[$sType][$sKey]) && is_array($aSingle[$sType][$sKey])) {
          $aData[$sType][$sKey] = array_merge($aValue, $aSingle[$sType][$sKey]);
        }
      }
    }
    return $aData;
  }
  
  
  /**
   * Metoda dodaje link do statystyk
   * 
   * @param array $aDatesArray
   * @return array
   */
  private function addLinkToOrders($aDatesArray, $bSingle) {
    global $aConfig;

    foreach($aDatesArray as $dData => $aItemData) {
      $nData = preg_replace('/(\d{4})-(\d{2})-(\d{2})/', '$3-$2-$1', $dData);
      if ($bSingle === TRUE) {
        $aDatesArray[$dData]['single_link'] = phpSelf(array('module' => 'm_zamowienia', 'f_shipment_date_expected' => $nData), ['action']);
      } else {
        $aDatesArray[$dData]['link'] = phpSelf(array('module' => 'm_zamowienia', 'f_shipment_date_expected' => $nData), ['action']);
      }
    }
    return $aDatesArray;
  }// end of addLinkToOrders() method
  
  
  /**
   * Ustawiamy dane metody transportu
   */
  private function getTransportMeans() {
    $sSql = 'SELECT id, name FROM orders_transport_means';
    $this->aTransport = $this->pDbMgr->GetAssoc('profit24', $sSql, false, array(), DB_FETCHMODE_ASSOC);
  }
  
  
  /**
   * Metoda dodaje metodę transportu do tablicy, na postawie id metody transprotu
   * 
   * @param array $aData
   * @return array
   */
  private function addTransportName($aData) {
    if (!empty($aData)) {
      foreach ($aData as $iKey => $aItem) {
        $aData[$iKey]['transport'] =  $this->aTransport[$iKey];
      }
    }
    return $aData;
  }
  
  /**
   * 
   * @return string
   */
  private function getUniqeByOILI() {
    $sSql = '
              AND (
                SELECT OILI_2.id 
                FROM orders_items_lists_items AS OILI_2
                WHERE OILI.orders_id = OILI_2.orders_id AND 
                      OILI.orders_items_id <> OILI_2.orders_items_id
                LIMIT 1
              ) IS NULL
      ';
    return $sSql;
  }
  
  /**
   * Pobieramy zamówienia na sortowniku
   * 
   * @return array
   */
  private function getSorting() {
    // DATA
    //  AND shipment_date_expected = CURDATE()
    $sSql = 'SELECT O.transport_id, count(DISTINCT OILI.orders_id) as count
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5"
             WHERE
              OIL.closed = "2"
              AND DATE(OIL.created) = CURDATE()
             GROUP BY O.transport_id
             ';
    $dd = $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
    return  $dd;
  }// end of getSorting() method
  
  
  /**
   * Pobieramy zamówienia na sortowniku
   * 
   * @return array
   */
  private function getSortingSingle() {
    // DATA
    //  AND shipment_date_expected = CURDATE()
    $sSql = 'SELECT O.transport_id, count(DISTINCT OILI.orders_id) as single_count
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5"
             WHERE
              OIL.closed = "2"
              AND DATE(OIL.created) = CURDATE()
              '.$this->getUniqeByOILI().'
             GROUP BY O.transport_id
             ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getSorting() method
  
  
  /**
   * Pobieramy zamówienia posortowane
   * 
   * @return array
   */
  private function getOutSorting() {
    // DATA
    //  AND shipment_date_expected = CURDATE()
    $sSql = 'SELECT O.transport_id, count(DISTINCT OILI.orders_id) as count
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5"
             WHERE 
              OIL.closed = "1"
              AND DATE(OIL.created) = CURDATE()
             GROUP BY O.transport_id
             ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getOutSorting() method
  
  
  /**
   * Pobieramy zamówienia posortowane
   * 
   * @return array
   */
  private function getOutSortingSingle() {
    // DATA
    //  AND shipment_date_expected = CURDATE()
    $sSql = 'SELECT O.transport_id, count(DISTINCT OILI.orders_id) as single_count
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5"
             WHERE
              OIL.closed = "1"
              AND DATE(OIL.created) = CURDATE()
              '.$this->getUniqeByOILI().'
             GROUP BY O.transport_id
             ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getOutSorting() method
  
  
  /**
   * Metoda pobiera zamówienia dziś zatwierdzone zatwierdzone lub zrealizowane
   * 
   * @return array
   */
  private function getConfirmedOrRealized() {
    // dziś zatwierdzone zatwierdzone lub zrealizowane
    
    $sSql = '
      SELECT O.transport_id, count(1) as count
      FROM orders AS O
      WHERE
        (
          (
            O.order_status = "3" OR
            O.order_status = "4"
          ) AND
          DATE(O.status_3_update) = CURDATE()
        )
      GROUP BY O.transport_id
      ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getConfirmedOrRealized() method
  
  /**
   * Metoda pobiera zamówienia dziś zatwierdzone zatwierdzone lub zrealizowane
   * 
   * @return array
   */
  private function getConfirmedOrRealizedSingle() {
    // dziś zatwierdzone zatwierdzone lub zrealizowane
    
    $sSql = '
      SELECT O.transport_id, count(1) as single_count
      FROM orders AS O
      WHERE 
        (
          (
            O.order_status = "3" OR
            O.order_status = "4"
          ) AND
          DATE(O.status_3_update) = CURDATE()
        )
        '.$this->getUniqeByOI().'
      GROUP BY O.transport_id
      ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getConfirmedOrRealized() method


  /**
   * Pobieramy zamówienia nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
   *
   * @return array
   */
  private function getOpenPlannedItemsQuantityOrders() {
    // nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
    $sSql = '
             SELECT O.shipment_date_expected, O.id, SUM(OI.quantity) as quantity
             FROM orders_items AS OI
             JOIN orders AS O
              ON O.id = OI.order_id
             WHERE 
              (
                (
                  O.order_status <> "5" AND 
                  O.order_status <> "4" AND 
                  O.order_status <> "3" 
                )
                OR (
                    (
                     O.order_status = "4" OR
                     O.order_status = "3" 
                    )
                    AND 
                    (
                       DATE(O.status_3_update) = CURDATE()
                    OR DATE(O.shipment_date_expected) = CURDATE()
                    )
                    AND 
                    (DATE(O.status_4_update) = CURDATE() OR O.status_4_update IS NULL )
                )
              )
              AND DATE(O.shipment_date_expected) <= CURDATE()
              AND OI.deleted = "0"
              GROUP BY O.id
             ORDER BY O.shipment_date_expected DESC
             
             ';
    $data = $this->pDbMgr->GetAll('profit24', $sSql);
    $ordersMonth = [];
    foreach ($data as $key => $row) {
      $ordersMonth[$row['shipment_date_expected']][$row['id']] += $row['quantity'];
    }
    $ordersStatistic = [];
    foreach ($ordersMonth as $month => $orders) {
      foreach ($orders as $orderNumber => $ordersQuantities) {
        $ordersStatistic[$month][$ordersQuantities] ++;
      }
      $ordersMonthStats[$month] = count($orders);
      arsort($ordersStatistic[$month], SORT_NUMERIC);
    }
    return $ordersStatistic;
  }// end of getOpenPlannedOrders() method


  /**
   * Pobieramy zamówienia nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
   *
   * @return array
   */
  private function getOpenPlannedOrders() {
    // nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
    /*
    // pobierz jeszcze zrealizowane i zatwierdzone dzisiejsze, te rekordy należy dodać do aktualnego dnia
    $sSql = 'SELECT count(id)
             FROM orders
             WHERE
                  (
                   order_status = "4" OR
                   order_status = "3"
                  )
                  AND DATE(status_3_update) = CURDATE()
                  AND DATE(shipment_date_expected) = CURDATE()
             ';
    $iTodayRealizedCount = intval($this->pDbMgr->GetOne('profit24', $sSql));
    */
    $sSql = 'SELECT shipment_date_expected, count(1) as count_planned, "0" AS link
             FROM orders AS O
             WHERE 
              (
                (
                  order_status <> "5" AND 
                  order_status <> "4" AND 
                  order_status <> "3" 
                )
                OR (
                    (
                     order_status = "4" OR
                     order_status = "3" 
                    )
                    AND 
                    (
                       DATE(status_3_update) = CURDATE()
                    OR DATE(shipment_date_expected) = CURDATE()
                    )
                    AND 
                    (DATE(O.status_4_update) = CURDATE() OR O.status_4_update IS NULL )
                )
              )
             GROUP BY shipment_date_expected
             ORDER BY shipment_date_expected DESC
             
             ';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, true);
  }// end of getOpenPlannedOrders() method

  
  /**
   * Pobieramy zamówienia nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
   * 
   * @return array
   */
  private function getOpenPlannedOrdersGroupWebsites() {
    // nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
    /*
    // pobierz jeszcze zrealizowane i zatwierdzone dzisiejsze, te rekordy należy dodać do aktualnego dnia
    $sSql = 'SELECT count(id)
             FROM orders 
             WHERE 
                  (
                   order_status = "4" OR
                   order_status = "3" 
                  )
                  AND DATE(status_3_update) = CURDATE()
                  AND DATE(shipment_date_expected) = CURDATE()
             ';
    $iTodayRealizedCount = intval($this->pDbMgr->GetOne('profit24', $sSql));
    */
    $sSql = 'SELECT O.shipment_date_expected, O.website_id, count(1) as count_planned, "0" AS link,
                    W.code
             FROM orders AS O
             JOIN websites AS W
              ON O.website_id = W.id
             WHERE 
              (
                (
                  O.order_status <> "5" AND 
                  O.order_status <> "4" AND 
                  O.order_status <> "3" 
                )
                OR (
                    (
                     O.order_status = "4" OR
                     O.order_status = "3" 
                    )
                    AND
                    (
                         DATE(O.status_3_update) = CURDATE()
                      OR DATE(O.shipment_date_expected) = CURDATE()
                    )
                    AND 
                    (DATE(O.status_4_update) = CURDATE() OR O.status_4_update IS NULL )
                )
              )
             GROUP BY CONCAT(O.shipment_date_expected, "_", O.website_id)
             ORDER BY CONCAT(O.shipment_date_expected, "_", O.website_id) DESC
             
             ';
    $d = $this->pDbMgr->GetAssoc('profit24', $sSql, false, [], DB_FETCHMODE_ASSOC, true);
    return $d;
  }// end of getOpenPlannedOrders() method
  
  /**
   * Pobieramy zamówienia nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
   * 
   * @return array
   */
  private function getOpenPlannedOrdersSingle() {
    // nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
    /*
    // pobierz jeszcze zrealizowane i zatwierdzone dzisiejsze, te rekordy należy dodać do aktualnego dnia
    $sSql = 'SELECT count(id)
             FROM orders 
             WHERE 
                  (
                   order_status = "4" OR
                   order_status = "3" 
                  )
                  AND DATE(status_3_update) = CURDATE()
                  AND DATE(shipment_date_expected) = CURDATE()
             ';
    $iTodayRealizedCount = intval($this->pDbMgr->GetOne('profit24', $sSql));
    */
    $sSql = 'SELECT shipment_date_expected, count(1) as single_count_planned, "0" AS single_link
             FROM orders AS O
             WHERE 
              (
                (
                  order_status <> "5" AND 
                  order_status <> "4" AND 
                  order_status <> "3" 
                )
                OR (
                    (
                     order_status = "4" OR
                     order_status = "3" 
                    )
                    AND DATE(status_3_update) = CURDATE()
                    AND DATE(shipment_date_expected) = CURDATE()
                )
              )
              '.$this->getUniqeByOI().'
             GROUP BY shipment_date_expected
             ORDER BY shipment_date_expected DESC
             
             ';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, true);
  }// end of getOpenPlannedOrders() method


  /**
   * Pobieramy zamówienia zatwierdzone lub zrealizowane, a dzisiaj zatwierdzone
   *
   * @return array
   */
  private function getRealizedOrConfirmedToday() {

    // zatwierdzone lub zrealizowane, a dzisiaj zatwierdzone
    $sSql = '
      SELECT shipment_date_expected, count(1) as count_confirmed
      FROM orders AS O
      WHERE 
        (
          order_status = "3" OR
          order_status = "4"
        ) AND
        DATE(status_3_update) = CURDATE()
      GROUP BY shipment_date_expected
      ORDER BY shipment_date_expected DESC
      ';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, true);
  }// end of getRealizedOrConfirmedToday() method


  /**
   * Pobieramy zamówienia zatwierdzone lub zrealizowane, a dzisiaj zatwierdzone
   * 
   * @return array
   */
  private function getRealizedOrConfirmedTodayGroupWebsite() {
    
    // zatwierdzone lub zrealizowane, a dzisiaj zatwierdzone
    $sSql = '
      SELECT O.shipment_date_expected, O.website_id, count(1) as count_confirmed
      FROM orders AS O
      WHERE 
        (
          order_status = "3" OR
          order_status = "4"
        ) AND
        DATE(status_3_update) = CURDATE()
      GROUP BY CONCAT(O.shipment_date_expected, "_", O.website_id)
      ORDER BY O.shipment_date_expected, O.website_id DESC
      ';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, false, [], DB_FETCHMODE_ASSOC, true);
  }// end of getRealizedOrConfirmedToday() method
  
  /**
   * 
   * @return string
   */
  private function getUniqeByOI() {
    $sSql = '
        AND (
         SELECT SUM(OI_2.quantity)
         FROM orders_items AS OI_2
         WHERE OI_2.order_id = O.id
              AND OI_2.deleted = "0"
              AND (OI_2.item_type = "I" OR OI_2.item_type = "P")
              AND OI_2.packet = "0"
        ) = 1
      ';
    return $sSql;
  }
}