<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace omniaCMS\modules\m_stats;

use Admin;
use DatabaseManager;
use Module;
use omniaCMS\lib\interfaces\ModuleEntity;
use orders\getTransportList;
use Smarty;
use View;

include_once('Module_completing.class.php');

/**
 * Description of Module_packing
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__stats__sorting extends Module__stats__completing implements ModuleEntity {

  protected $statsType = '2';
  
  /**
   * 
   * @param Admin $oClassRepository
   */
  public function __construct(Admin $oClassRepository) {
    parent::__construct($oClassRepository);
  }
}
