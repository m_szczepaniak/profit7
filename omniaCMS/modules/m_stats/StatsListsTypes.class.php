<?php
/* 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-08-13 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace omniaCMS\modules\m_stats;

use LIB\EntityManager\Entites\Magazine;
use LIB\orders\listType\manageGetOrdersItemsList;
use Smarty;

/**
 * Description of StatsListsTypes
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class StatsListsTypes {

  /**
   * @var Smarty
   */
  private $pSmarty;

  /**
   *
   * @var manageGetOrdersItemsList 
   */
  private $oManageLists;
  
  /**
   *
   * @var array
   */
  private $aTransport;
  
  public function __construct($pDbMgr, Smarty $pSmarty, $aTransport) {
    $this->pDbMgr = $pDbMgr;
    $this->oManageLists = new manageGetOrdersItemsList($this->pDbMgr, Magazine::TYPE_HIGH_STOCK_SUPPLIES, true);
    $this->aTransport = $aTransport;
    $this->pSmarty = $pSmarty;
  }
  
  /**
   * 
   * @return string
   */
  public function getHTML($dExpectedShipmentDate = '') {
    $aDataListTypes = array();
    $aDataListTypes['to_collecting_stock_single'] = $this->getToCollectingStockSingle($dExpectedShipmentDate);
    $aDataListTypes['to_collecting_stock_full'] = $this->getToCollectingStockFull($dExpectedShipmentDate);
    $aDataListTypes['to_collecting_stock_part_linked'] = $this->getToCollectingStockPartLinked($dExpectedShipmentDate);
    
    
    $aDataListTypes['to_collecting_train_single'] = $this->getToCollectingTrainSingle($dExpectedShipmentDate);
    $aDataListTypes['to_collecting_train_default'] = $this->getToCollectingTrainDefault($dExpectedShipmentDate);
    $aDataListTypes['to_collecting_train_part_linked'] = $this->getToCollectingTrainLinked($dExpectedShipmentDate);
    
    $aDataListTypes = $this->addSummaries($aDataListTypes);
    $this->pSmarty->assign_by_ref('aData', $aDataListTypes);
    $sHtml = $this->pSmarty->fetch('stats_lists_types.tpl');
    $this->pSmarty->clear_assign('aData');
    return $sHtml;
  }

  /**
   * 
   * @return array
   */
  private function getToCollectingTrainSingle($dExpectedShipmentDate = '') {
    
    $this->oManageLists->setListType('SINGLE_SORTER_TRAIN');
    $this->oManageLists->setGetFromTrain(TRUE);
    $this->oManageLists->initOrderListType();
    $aGroupedTransport = $this->oManageLists->getCountOrdersGroupByTransport($dExpectedShipmentDate);
    $aGroupedTransport = $this->addTransportNameSimple($aGroupedTransport, 'count');
    return $aGroupedTransport;
  }
  
  /**
   * 
   * @return array
   */
  private function getToCollectingTrainDefault($dExpectedShipmentDate = '') {
    
    $this->oManageLists->setListType(array('PART', 'SORTER_TRAIN'));
    $this->oManageLists->setGetFromTrain(TRUE);
    $this->oManageLists->initOrderListType();
    $aGroupedTransport = $this->oManageLists->getCountOrdersGroupByTransport($dExpectedShipmentDate);
    $aGroupedTransport = $this->addTransportNameSimple($aGroupedTransport, 'count');
    return $aGroupedTransport;
  }
  
  /**
   * 
   * @return array
   */
  private function getToCollectingTrainLinked($dExpectedShipmentDate = '') {
    
    $this->oManageLists->setListType(array('LINKED_STOCK', 'LINKED_TRAIN', 'LINKED_PART'));
    $this->oManageLists->setGetFromTrain(TRUE);
    $this->oManageLists->initOrderListType();
    $aGroupedTransport = $this->oManageLists->getCountOrdersGroupByTransport($dExpectedShipmentDate);
    $aGroupedTransport = $this->addTransportNameSimple($aGroupedTransport, 'count');
    return $aGroupedTransport;
  }
  
  
  /**
   * 
   * @return array
   */
  private function getToCollectingStockFull($dExpectedShipmentDate = '') {
    
    $this->oManageLists->setListType(array('SORTER_STOCK'));
    $this->oManageLists->setGetFromTrain(FALSE);
    $this->oManageLists->initOrderListType();
    $aGroupedTransport = $this->oManageLists->getCountOrdersGroupByTransport($dExpectedShipmentDate);
    $aGroupedTransport = $this->addTransportNameSimple($aGroupedTransport, 'count');
    return $aGroupedTransport;
  }
  
  /**
   * 
   * @return array
   */
  private function getToCollectingStockPartLinked($dExpectedShipmentDate = '') {
    
    $this->oManageLists->setListType(array('PART', 'LINKED_PART', 'LINKED_STOCK'));
    $this->oManageLists->setGetFromTrain(FALSE);
    $this->oManageLists->initOrderListType();
    $aGroupedTransport = $this->oManageLists->getCountOrdersGroupByTransport($dExpectedShipmentDate);
    $aGroupedTransport = $this->addTransportNameSimple($aGroupedTransport, 'count');
    return $aGroupedTransport;
  }
  
  
  /**
   * 
   * @return array
   */
  private function getToCollectingStockSingle($dExpectedShipmentDate = '') {
    
    $this->oManageLists->setListType('SINGLE_SORTER_STOCK');
    $this->oManageLists->setGetFromTrain(FALSE);
    $this->oManageLists->initOrderListType();
    $aGroupedTransport = $this->oManageLists->getCountOrdersGroupByTransport($dExpectedShipmentDate);
    $aGroupedTransport = $this->addTransportNameSimple($aGroupedTransport, 'count');
    return $aGroupedTransport;
  }
  
  
  /**
   * Metoda dodaje metodę transportu do tablicy, na postawie id metody transprotu
   * 
   * @param array $aData
   * @return array
   */
  private function addTransportNameSimple($aData, $sCountName) {
    
    $aTransport = array();
    if (!empty($aData)) {
      foreach ($aData as $iKey => $iCount) {
        $aTransport[$iKey] = [
            $sCountName => $iCount,
            'transport' => $this->aTransport[$iKey]
        ];
      }
    }
    ksort($aTransport);
    return $aTransport;
  }
  
  
  /**
   * 
   * @param array $aData
   * @return array
   */
  private function addSummaries($aItemType) {
    
    if (!empty($aItemType) && is_array($aItemType)) {
      foreach ($aItemType as $sListType => $aData) {
        foreach ($aData as $sType => $aItems) {
          if (is_array($aItems)) {
            foreach ($aItems as $sKey => $sValue) {
              if (stristr($sKey, 'count')) {
                $aItemType[$sListType]['SUMA'][$sKey] += intval($sValue);
              }
            }
          }
        }
      }
    }
    return $aItemType;
  }// end of addSummaries() method
}
