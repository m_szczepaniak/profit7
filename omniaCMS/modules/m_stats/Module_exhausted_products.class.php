<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-15
 * @copyrights Marcin Chudy - Profit24.pl
 */
use Pager\Pagination;

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_USER_NOTICE);
//ini_set('display_errors', 'On');

class Module__stats__exhausted_products
{
    private $sources = [
        'internal' => [
            'profit_g_status' => 'profit_g_wholesale_price',
            'profit_j_status' => 'profit_j_wholesale_price',
            'profit_e_status' => 'profit_e_wholesale_price',
        ],
        'external' => [
            'ateneum_stock' => 'ateneum_wholesale_price',
            'azymut_stock' => 'azymut_wholesale_price',
            'platon_stock' => 'platon_wholesale_price',
            'dictum_stock' => 'dictum_wholesale_price',
            'siodemka_stock' => 'siodemka_wholesale_price',
        ]
    ];

    private $sourceNamesLink = [
        'internal' => "Wewnętrzne",
        'external' => "Zewnętrzne",
    ];

    private $sourceNames = [
        'profit_g_status' => 'Stock',
        'profit_j_status' => 'Profit J',
        'profit_e_status' => 'Profit E',
        'ateneum_stock' => 'Ateneum',
        'azymut_stock' => 'Azymut',
        'platon_stock' => 'Platon',
        'dictum_stock' => 'Dictum',
        'siodemka_stock' => 'Siodemka',
    ];
    private $generateCsv;

    function __construct($pSmarty)
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;

        if (isset($_POST['csv']) && $_POST['csv'] != 0) {
            $this->generateCsv = true;
        }

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        $sDo = $_POST['do'];

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }


        if (isset($_GET['per_page'])) {
            $_POST['per_page'] = $_GET['per_page'];
            $sDo = 'get_stats';
        }

        if (isset($_POST['per_page'])) {
            $sDo = 'get_stats';

            $_SESSION['per_page'] = $_POST['per_page'];
        }

        if (isset($_SESSION['per_page'])) {

            $_POST['per_page'] = $_SESSION['per_page'];
        }

        if (isset($_GET['page'])) {
            $_POST['page'] = $_GET['page'];
            $sDo = 'get_stats';
        }

        if (isset($_POST['source_not'])) {
            $_SESSION['source_not'] = $_POST['source_not'];
        }

        // selected publisher
        if (isset($_POST['publisher_aci'])) {
            $_SESSION['publisher_aci'] = $_POST['publisher_aci'];
        }

        if (isset($_SESSION['publisher_aci'])) {
            $_POST['publisher_aci'] = $_SESSION['publisher_aci'];
        }

        switch ($sDo) {
            default:
                $this->Show($pSmarty);
                break;

            case 'get_stats':

                $this->GetStats($pSmarty);
                break;
        }
    }

    /**
     * @param $pSmarty
     * @return string
     */
    private function Show($pSmarty)
    {
        global $aConfig, $pDbMgr;

        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('stat_ordered_items', _('Najczęściej zamawiane pozycje'));

        $pForm->AddHidden('do', 'get_stats');

        $sources = [
            [
                'label' => 'Wewnętrzne (Stock, E i J)',
                'value' => 'internal'
            ],
            [
                'label' => 'Zródła zwenętrzne',
                'value' => 'external'
            ],
        ];

        $pForm->AddSelect('source_not', _('Zródło'), [], $sources, [], '', false);
        $pForm->AddText('publisher_aci', _("Wydawca"), array('id' => 'publisher_aci', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', false, false);
        $pForm->AddCheckBox('csv', _('Generuj csv'), [], [], [], '', false);

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Generuj')) . '&nbsp;&nbsp;');

        $js = '
            <script type=text/javascript>
            $(document).ready(function(){

              $("#publisher_aci").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetPublishers.php"
                });
            });
            </script>
        ';

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()).$js);
    }

    private function GetStats($pSmarty)
    {
        global $pDbMgr;


        $perPage = $_POST['per_page'] != null ? $_POST['per_page'] : 20;
        $cPage = $_POST['page'] != null ? $_POST['page'] : 1;

        $iStartFrom = ($cPage - 1) * $perPage;
        if (null == $cPage){
            $iStartFrom = 0;
        }

        $sourceType = $_SESSION['source_not'];

        $sql = $this->prepareSql($sourceType);
        $rowsSql = $sql;

        $rows = $pDbMgr->GetAll('profit24', $rowsSql);

        $finalRows = $this->prapareRows($sourceType, $rows);

        $pagination = new Pagination($finalRows, $perPage, $cPage);
        $aBooks = $pagination->getCurrentRows();

        include_once('View/View.class.php');

        $aHeader = array(
            'refresh' => true,
            'search' => false,
            'checkboxes' => false,
            'form' => true
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'content' => _('EAN'),
                'width' => '10%'
            ),
            array(
                'content' => _('Nazwa'),
                'width' => '25%'
            ),
            array(
                'content' => _('Wydawnictwo'),
                'width' => '15%'
            ),
            array(
                'content' => _('Nazwa źródła'),
                'width' => '3%'
            ),
            array(
                'content' => _('Cena w źródle'),
                'width' => '5%'
            ),
            array(
                'content' => _('Ilość w źródle'),
                'width' => '8%'
            ),
//            array(
//                'content' => _('Nazwa źródła'),
//                'width' => '8%',
//            ),
//            array(
//                'content' => _('Różnica w cenie'),
//                'width' => '8%',
//            ),
        );

        $pView = new View('ordered_itm', $aHeader, $aAttribs);
        $pView->AddPager($pagination->getRowsAmount());
//        $pView->AddPager($pagination->getRowsAmount());
        $pView->AddRecordsHeader($aRecordsHeader);

        // ustawienia dla poszczegolnych kolumn rekordu
        $aColSettings = array(

        );

        // dodanie rekordow do widoku
        $pView->AddRecords($aBooks, $aColSettings);
        $aRecordsFooter = array(
            array()
        );

        $pView->AddRecordsFooter($aRecordsFooter);

//        if ($this->generateCsv == true) {
//            $this->array2csv2($finalRows);
//        }

        if ($this->generateCsv == true) {
            $this->array2csv2($aBooks);
        }

        // zawsze wyświetlamy show
        $sHTML = '<h1>Wybrane źródło: '.$this->sourceNamesLink[$sourceType].' <span style="color:red">Ilość produktów: '.$pagination->getRowsAmount().'</span></h1>';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sHTML . $pView->Show());
        $d = '';
    }

    function array2csv2(array &$array)
    {
        $headers = [
            'Ean',
            'Nazwa',
            'Wydawnictwo',
            'Nazwa źródła',
            'Cena w źródle',
            'Ilość w źródle',
        ];

        if (count($array) == 0) {
            return null;
        }
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="data.csv";');
        ob_start();
        $df = fopen('php://output', 'w');
        fputcsv($df, $headers);
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        $data = ob_get_clean();

        echo $data;
        die;
    }

    /**
     * @param $sourceType
     * @param $rows
     * @return mixed
     */
    private function prapareRows($sourceType, $rows)
    {
        $finalData = [];

        $i = 1;

        foreach($rows as $row) {

            $allData = [];

            foreach($this->sources[$sourceType] as $stock => $price) {

                $sourceStock = $row[$stock];
                $sourcePrice = $row[$price];

                if ($sourceStock > 0) {

                    $allData[] = [
                        'name' => $this->sourceNames[$stock],
                        'price' => $sourcePrice,
                        'amount' => $sourceStock,
                    ];
                }
            }

            usort($allData, function ($item1, $item2) {
                if ($item1['price'] == $item2['price']) return 0;
                return $item1['price'] < $item2['price'] ? -1 : 1;
            });

            $elements = reset($allData);

            $finalData[] = [
                'ean' => empty($row['ean']) ? $row['isbn'] : $row['ean'],
                'title' => $row['title'],
                'publisher_name' => $row['publisher_name'],
                'minimal_source' => $elements['name'],
                'minimal_price' => $elements['price'],
                'minimal_amount' => $elements['amount'],
//                'price_difference' => $this->generateCsv == false ? '<b style="color:red">'.$row['price_difference'].'</b>' : $row['price_difference'],
            ];

            $i++;
        }

        usort($finalData, function ($item1, $item2) {
            if ($item1['minimal_amount'] == $item2['minimal_amount']) return 0;
            return $item1['minimal_amount'] > $item2['minimal_amount'] ? -1 : 1;
        });

        return $finalData;
    }

    /**
     * @param $sourceType
     * @return string
     */
    private function prepareSql($sourceType, $count = false)
    {
        global $pDbMgr;

        if ($sourceType == 'internal') {

            $extraSql = "(PS.profit_g_status > 0 || PS.profit_e_status > 0 || PS.profit_j_status > 0)";
        } else {

            $extraSql = "((PS.ateneum_stock > 0 AND PS.ateneum_status = '1') ||
            (PS.azymut_stock > 0 AND PS.azymut_status = 1) ||
            (PS.platon_stock > 0 AND PS.platon_status = '1') ||
            (PS.dictum_stock > 0 AND PS.dictum_status = 1) ||
            (PS.siodemka_stock > 0 AND PS.siodemka_status = 1))";
        }

        if (false == $count) {
            $columns = "P.id, PS.profit_g_status, PS.profit_e_status, PS.profit_j_status, PPU.name as publisher_name,
        P.ean_13 as ean, P.isbn, P.name as title,
        PS.ateneum_stock, PS.azymut_stock, PS.platon_stock, PS.dictum_stock, PS.siodemka_stock,
        PS.ateneum_wholesale_price, PS.azymut_wholesale_price, PS.platon_wholesale_price,
        PS.dictum_wholesale_price, PS.siodemka_wholesale_price,
        PS.profit_g_wholesale_price, PS.profit_j_wholesale_price, PS.profit_e_wholesale_price";
        } else {
            $columns = "count(P.id)";
        }

        $sql = "
        SELECT $columns
        FROM products AS P
        LEFT JOIN products_publishers AS PPU ON P.publisher_id = PPU.id
        JOIN products_stock AS PS ON P.id = PS.id
        WHERE P.prod_status = '2'
        AND $extraSql
        ";

        $publisher = $_SESSION['publisher_aci'];

        if (!empty($publisher)) {

            $publisherId = $pDbMgr->GetOne('profit24', "SELECT id FROM products_publishers WHERE name = '$publisher' LIMIT 1");

            if (!empty($publisherId)) {


                $sql .= "
                    AND P.publisher_id = $publisherId
                ";
            }
        }


        return $sql;
    }
}

