<?php
/**
 * @author Paweł Bromka
 * @created 2014-11-07 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__stats__stat_generated_items {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  

  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  private $pDbMgr;
  
  
  function __construct($pSmarty) {
    global $pDbMgr;
    
    $this->pDbMgr = $pDbMgr;
    

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
		if ($_SESSION['user']['type'] === 0) {
			if (!hasModulePrivileges($_GET['module_id'])){
				showPrivsAlert($pSmarty);
				return;
			}
		}
    
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
    switch ($sDo) {
      case 'change_procedure':
      default:
        $this->Show($pSmarty);
      break;
      
      case 'get_stats':
        $this->GetStats($pSmarty);
      break;
    }
  }
  
  
  /**
   * Metoda wyświetla formularz do wyboru zakresu dat w obrębie których ma generować się raport
   * 
   * @global array $aConfig
   * @param object $pSmarty
   */
  private function Show($pSmarty) {

    $sHTML = $this->ShowHTML();
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($sHTML));
  }// end of Show() method
  
  
  /**
   * Metoda wyświetla kod html widoku
   * 
   * @global array $aConfig
   * @return string HTML
   */
  private function ShowHTML() {
    global $aConfig;
    
    $aProcedures = array();
    $aData = array();
    if (isset($_POST)) {
      $aData = $_POST;
    }
    
    include_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('stat_ordered_items', _('Raporty'));
    
    $pForm->AddHidden('do', 'get_stats');
    
    $aProcedures = $this->getProcedureOptions();
    $pForm->AddSelect('procedure', _('Wybierz procedurę'), array('onchange'=>'ChangeObjValue(\'do\', \'change_procedure\'); this.form.submit();'), $aProcedures, $aData['procedure'], '', false);

    if (isset($_POST['procedure']) && ($_POST['procedure'] != "")) {
      $pForm->AddMergedRow(_('Raporty'), array('class'=>'merged'));
     
      $sClassName = str_replace('\\reports\auto\\', '', $_POST['procedure']);
      $sReportsPath = 'LIB/reports/files/'.$sClassName.'/';
          
      $aFiles = glob('../'.$sReportsPath.'*.*');
      
      foreach ($aFiles as $sFilenamePath) {
        $sFilename = str_replace('../'.$sReportsPath, '', $sFilenamePath);
        $sURLFile = $aConfig['common']['client_base_url_https'].$sReportsPath.$sFilename;
                
        $pForm->AddRow(sprintf(_(' Raport %s '), $sFilename), '<a href="'.$sURLFile.'">'._('Pobierz').'</a>');
      }
    }

		return $pForm->ShowForm();
  }// end of ShowHTML() method
  
  
  /**
   * Metoda pobiera opcje procedur
   * 
   * @return array
   */
  private function getProcedureOptions() {
    $aProcedures = array();
    $aClasses = glob('../LIB/reports/auto/Report*.*');
    
    foreach ($aClasses as $iKey => $sClassName) {
      $sClassName = str_replace('.class.php', '', $sClassName);
      $sFullClassName = str_replace('../LIB/reports/auto/', '', $sClassName);
      $sFullClassName = '\reports\auto\\'.$sFullClassName;
      $oReport = new $sFullClassName;
      $sReportName = $oReport->getName();
      $aProcedures[$iKey]['label'] = $sReportName;
      $aProcedures[$iKey]['value'] = $sFullClassName;
    }
    return addDefaultValue($aProcedures, _('Wybierz procedurę'));
  }// end of getProcedureOptions() method


}

