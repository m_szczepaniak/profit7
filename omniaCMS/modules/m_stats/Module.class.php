<?php

use LIB\EntityManager\Entites\Magazine;
use LIB\Helpers\ArrayHelper;
use LIB\orders\listType\manageGetOrdersItemsList;
/**
 * Skrypt statystyk serwisu.
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_USER_NOTICE); 
ini_set('display_errors','On');
class Module__stats {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  private $pDbMgr;
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param Smarty $pSmarty
   * @return void
   */
  function __construct($pSmarty) {
    global $pDbMgr;

    $this->pDbMgr = $pDbMgr;
    

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
		if ($_SESSION['user']['type'] === 0) {
			if (!hasModulePrivileges($_GET['module_id'])){
				showPrivsAlert($pSmarty);
				return;
			}
		}
    
    $this->getTransportMeans();


      $sKey = 'module_statistic_content_v1';
      if (class_exists('memcached')) {

          $oMemcache = new Memcached();
          $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
          $sHtml = $oMemcache->get($sKey);
          if (isset($sHtml) && $sHtml != '') {
              $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sHtml);
              return;
          }
      }


      if (class_exists('memcached')) {
          $oMemcache->set($sKey, 'AKTUALNIE JEST GENEROWANY WIDOK', 60);
      }
      $secondsWait = 3600;
      $dateTime = new DateTime('now');
      $sHtml = $this->showStats($pSmarty);
      $sHtml = 'Wygenerowano przez '.$_SESSION['user']['name'].' : '.$dateTime->format('d.m.Y H:i:s').' Za godzine można wygenerować następny <br />'.$sHtml;
      $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sHtml);
      if (class_exists('memcached')) {
          $oMemcache->set($sKey, $sHtml, $secondsWait);
          return;
      }


  }
  
  /**
   * 
   * @param Smarty $pSmarty
   */
  private function showStats(&$pSmarty) {
    
    $aFirst = $this->getOpenPlannedOrders();
    $aFirst = $this->addLinkToOrders($aFirst, false);
    $aSecond = $this->getRealizedOrConfirmedToday();
    
    $aData['table_dates'] = array_merge_recursive($aFirst, $aSecond);
    $aData['ready_to_send'] = $this->getReadyToSend();
    $aData['completatnion'] = $this->getCompletation();
    $aData['sorting'] = $this->getSorting();
    $aData['out_sorting'] = $this->getOutSorting();
    $aData['confirm_or_realized'] = $this->getConfirmedOrRealized();
    
//    $aFirstSingle = $this->getOpenPlannedOrdersSingle();
//    $aFirstSingle = $this->addLinkToOrders($aFirstSingle, true);
//    $aSecondSingle = $this->getRealizedOrConfirmedTodaySingle();
//    $aSingle['table_dates'] = array_merge_recursive($aFirstSingle, $aSecondSingle);
    $aSingle['ready_to_send'] = $this->getReadyToSendSingle();
    $aSingle['completatnion'] = $this->getCompletationSingle();
    $aSingle['sorting'] = $this->getSortingSingle();
    $aSingle['out_sorting'] = $this->getOutSortingSingle();
    $aSingle['confirm_or_realized'] = $this->getConfirmedOrRealizedSingle();
    $aData = $this->arrayMerge($aData, $aSingle);
    $aData = $this->addSummaries($aData);
    $pSmarty->assign_by_ref('aData', $aData);
    $sHtml = $pSmarty->fetch('stats.tpl');
    $pSmarty->clear_assign('aData');
    return $sHtml;
  }
  
  /**
   * 
   * @param array $aData
   * @return array
   */
  private function addSummaries($aData) {
    
    foreach ($aData as $sType => $aItems) {
      foreach ($aItems as $mItemKey => $aItems) {
        if (is_array($aItems)) {
          foreach ($aItems as $sKey => $sValue) {
            if (stristr($sKey, 'count')) {
              $aData[$sType]['SUMA'][$sKey] += intval($sValue);
            }
          }
        }
      }
    }
    return $aData;
  }// end of addSummaries() method
  
  /**
   * 
   */
  private function arrayMerge($aData, $aSingle) {
    foreach ($aData as $sType => $aItemsData) {
      foreach ($aItemsData as $sKey => $aValue) {
        if (is_array($aValue) && isset($aSingle[$sType][$sKey]) && is_array($aSingle[$sType][$sKey])) {
          $aData[$sType][$sKey] = array_merge($aValue, $aSingle[$sType][$sKey]);
        }
      }
    }
    return $aData;
  }
  
  
  /**
   * Metoda dodaje link do statystyk
   * 
   * @param array $aDatesArray
   * @return array
   */
  private function addLinkToOrders($aDatesArray, $bSingle) {
    foreach($aDatesArray as $dData => $aItemData) {
      $nData = preg_replace('/(\d{4})-(\d{2})-(\d{2})/', '$3-$2-$1', $dData);
      if ($bSingle === TRUE) {
        $aDatesArray[$dData]['single_link'] = phpSelf(array('module' => 'm_zamowienia', 'f_shipment_date_expected' => $nData));
      } else {
        $aDatesArray[$dData]['link'] = phpSelf(array('module' => 'm_zamowienia', 'f_shipment_date_expected' => $nData));
      }
    }
    return $aDatesArray;
  }// end of addLinkToOrders() method
  
  
  /**
   * Ustawiamy dane metody transportu
   */
  private function getTransportMeans() {
    $sSql = 'SELECT id, name FROM orders_transport_means';
    $this->aTransport = $this->pDbMgr->GetAssoc('profit24', $sSql, false, array(), DB_FETCHMODE_ASSOC);
  }
  
  
  /**
   * Metoda dodaje metodę transportu do tablicy, na postawie id metody transprotu
   * 
   * @param array $aData
   * @return array
   */
  private function addTransportName($aData) {
    if (!empty($aData)) {
      foreach ($aData as $iKey => $aItem) {
        $aData[$iKey]['transport'] =  $this->aTransport[$iKey];
      }
    }
    return $aData;
  }
  
  /**
   * Metoda dodaje metodę transportu do tablicy, na postawie id metody transprotu
   * 
   * @param array $aData
   * @return array
   */
  private function addTransportNameSimple($aData, $sCountName) {
    
    $aTransport = array();
    if (!empty($aData)) {
      foreach ($aData as $iKey => $iCount) {
        $aTransport[$iKey] = [
            $sCountName => $iCount,
            'transport' => $this->aTransport[$iKey]
        ];
      }
    }
    
    return $aTransport;
  }
  
  /**
   * Pobieramy zamówienia na dzisiaj do zebrania
   * 
   * @var $oManageLists manageGetOrdersItemsList 
   * @return array
   */
  private function getReadyToSend() {
    
    /*
     * 1) opłacone, lub odroczony termin płatności,
     * 2) nie "nowe", "zatwierdzone", "zrealizowane", "anulowane"
     * 3) jeśli z G to status musi być "jest nie G"
     * 4) jeśli zewn. to status musi być "jest pot." 
     * 5) data wysyłki jeśli została zmieniona to ma być to dziś
     * 6) nie pobrane wcześniej,
     */
    /*
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia_magazyn_zbieranie/Module_get_ready_to_send.class.php');
    $oNull = null;
    $oModReadyToSend = new Module__zamowienia_magazyn_zbieranie__get_ready_to_send($oNull, FALSE);
    $sSelect = ' O.transport_id, count(DISTINCT O.id) as count';//DISTINCT O.id,
    $sSql = $oModReadyToSend->getQuery($sSelect);
    $sSql .= " GROUP BY O.transport_id ";
    */
    $aFullStock = $this->getFullSorterStock();
    $aReadyTrain = $this->getReadyTrain();

    $aMerged = ArrayHelper::sumColumnArrays($aFullStock, $aReadyTrain, 'count');
    return $aMerged;
  }// end of getReadyToSend() method
  
  
  /**
   * 
   * @return array
   */
  private function getFullSorterStock() {
    $oManageLists = new manageGetOrdersItemsList($this->pDbMgr, Magazine::TYPE_HIGH_STOCK_SUPPLIES, true);
    $oManageLists->setListType(array('SORTER_STOCK'));
    $oManageLists->setGetFromTrain(FALSE);
    $oManageLists->initOrderListType();
    
    $aGroupedTransport = $oManageLists->getCountOrdersGroupByTransport();
    $aGroupedTransport = $this->addTransportNameSimple($aGroupedTransport, 'count');
    return $aGroupedTransport;
  }
  
  /**
   * 
   * @return array
   */
  private function getReadyTrain() {
    $oManageLists = new manageGetOrdersItemsList($this->pDbMgr, Magazine::TYPE_HIGH_STOCK_SUPPLIES, true);
    $oManageLists->setListType(array('PART', 'SORTER_TRAIN'));
    $oManageLists->setGetFromTrain(TRUE);
    $oManageLists->initOrderListType();
    
    $aGroupedTransport = $oManageLists->getCountOrdersGroupByTransport();
    $aGroupedTransport = $this->addTransportNameSimple($aGroupedTransport, 'count');
    return $aGroupedTransport;
  }
  
  /**
   * Pobieramy zamówienia na dzisiaj do zebrania
   * 
   * @return array
   */
  private function getReadyToSendSingle() {
    
    /*
     * 1) opłacone, lub odroczony termin płatności,
     * 2) nie "nowe", "zatwierdzone", "zrealizowane", "anulowane"
     * 3) jeśli z G to status musi być "jest nie G"
     * 4) jeśli zewn. to status musi być "jest pot." 
     * 5) data wysyłki jeśli została zmieniona to ma być to dziś
     * 6) nie pobrane wcześniej,
     */
    /*
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia_magazyn_zbieranie/Module_get_ready_to_send.class.php');
    $oNull = null;
    $oModReadyToSend = new Module__zamowienia_magazyn_zbieranie__get_ready_to_send($oNull, FALSE);
    $sSelect = ' O.transport_id, count(DISTINCT O.id) as single_count';//DISTINCT O.id,
    $sSql = $oModReadyToSend->getQuery($sSelect);
    $sSql .= '
     AND (
      (SELECT sum(quantity) from orders_items WHERE order_id = O.id AND deleted = "0") = 1
     ) ';
    $sSql .= " GROUP BY O.transport_id ";
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
     * */
    
    $aSingleStock = $this->getSingleSorterStock();
    $aSingleTrain = $this->getSingleSorterTrain();

    $aMerged = ArrayHelper::sumColumnArrays($aSingleTrain, $aSingleStock, 'count');
    return $aMerged;
  }// end of getReadyToSend() method
  
  /**
   * 
   * @return array
   */
  private function getSingleSorterStock() {
    
    $oManageLists = new manageGetOrdersItemsList($this->pDbMgr, Magazine::TYPE_HIGH_STOCK_SUPPLIES, true);
    $oManageLists->setListType('SINGLE_SORTER_STOCK');
    $oManageLists->setGetFromTrain(FALSE);
    $oManageLists->initOrderListType();
    
    $aGroupedTransport = $oManageLists->getCountOrdersGroupByTransport();
    $aGroupedTransport = $this->addTransportNameSimple($aGroupedTransport, 'single_count');
    return $aGroupedTransport;
  }
  
  /**
   * 
   * @return array
   */
  private function getSingleSorterTrain() {
    
    $oManageLists = new manageGetOrdersItemsList($this->pDbMgr, Magazine::TYPE_HIGH_STOCK_SUPPLIES, true);
    $oManageLists->setListType('SINGLE_SORTER_TRAIN');
    $oManageLists->setGetFromTrain(TRUE);
    $oManageLists->initOrderListType();
    
    $aGroupedTransport = $oManageLists->getCountOrdersGroupByTransport();
    $aGroupedTransport = $this->addTransportNameSimple($aGroupedTransport, 'single_count');
    return $aGroupedTransport;
  }
  
  /**
   * Metoda pobiera zamówienia w trakcie zbierania
   * 
   * @return array
   */
  private function getCompletation() {
    // DATA
    // AND shipment_date_expected = CURDATE()
    $sSql = 'SELECT O.transport_id, count(DISTINCT OILI.orders_id) as count
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5"
             WHERE
              OIL.closed = "0"
              AND DATE(OIL.created) = CURDATE()
             GROUP BY O.transport_id
             ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getCompletation() method
  
  
  /**
   * Metoda pobiera zamówienia w trakcie zbierania
   * 
   * @return array
   */
  private function getCompletationSingle() {
    // DATA
    // AND shipment_date_expected = CURDATE()
    $sSql = 'SELECT O.transport_id, count(DISTINCT OILI.orders_id) as single_count
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5"
             WHERE 
              OIL.closed = "0"
              AND DATE(OIL.created) = CURDATE()
              '.$this->getUniqeByOILI().'
             GROUP BY O.transport_id
             ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getCompletation() method
  
  /**
   * 
   * @return string
   */
  private function getUniqeByOILI() {
    $sSql = '
              AND (
                SELECT OILI_2.id 
                FROM orders_items_lists_items AS OILI_2
                WHERE OILI.orders_id = OILI_2.orders_id AND 
                      OILI.orders_items_id <> OILI_2.orders_items_id
                LIMIT 1
              ) IS NULL
      ';
    return $sSql;
  }
  
  /**
   * Pobieramy zamówienia na sortowniku
   * 
   * @return array
   */
  private function getSorting() {
    // DATA
    //  AND shipment_date_expected = CURDATE()
    $sSql = 'SELECT O.transport_id, count(DISTINCT OILI.orders_id) as count
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5"
             WHERE
              OIL.closed = "2"
              AND DATE(OIL.created) = CURDATE()
             GROUP BY O.transport_id
             ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getSorting() method
  
  
  /**
   * Pobieramy zamówienia na sortowniku
   * 
   * @return array
   */
  private function getSortingSingle() {
    // DATA
    //  AND shipment_date_expected = CURDATE()
    $sSql = 'SELECT O.transport_id, count(DISTINCT OILI.orders_id) as single_count
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5"
             WHERE
              OIL.closed = "2"
              AND DATE(OIL.created) = CURDATE()
              '.$this->getUniqeByOILI().'
             GROUP BY O.transport_id
             ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getSorting() method
  
  
  /**
   * Pobieramy zamówienia posortowane
   * 
   * @return array
   */
  private function getOutSorting() {
    // DATA
    //  AND shipment_date_expected = CURDATE()
    $sSql = 'SELECT O.transport_id, count(DISTINCT OILI.orders_id) as count
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5"
             WHERE 
              OIL.closed = "1"
              AND DATE(OIL.created) = CURDATE()
             GROUP BY O.transport_id
             ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getOutSorting() method
  
  
  /**
   * Pobieramy zamówienia posortowane
   * 
   * @return array
   */
  private function getOutSortingSingle() {
    // DATA
    //  AND shipment_date_expected = CURDATE()
    $sSql = 'SELECT O.transport_id, count(DISTINCT OILI.orders_id) as single_count
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5"
             WHERE 
              OIL.closed = "1"
              AND DATE(OIL.created) = CURDATE()
              '.$this->getUniqeByOILI().'
             GROUP BY O.transport_id
             ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getOutSorting() method
  
  
  /**
   * Metoda pobiera zamówienia dziś zatwierdzone zatwierdzone lub zrealizowane
   * 
   * @return array
   */
  private function getConfirmedOrRealized() {
    // dziś zatwierdzone zatwierdzone lub zrealizowane
    
    $sSql = '
      SELECT O.transport_id, count(1) as count
      FROM orders AS O
      WHERE
        (
          (
            O.order_status = "3" OR
            O.order_status = "4"
          ) AND
          DATE(O.status_3_update) = CURDATE()
        )
      GROUP BY O.transport_id
      ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getConfirmedOrRealized() method
  
  /**
   * Metoda pobiera zamówienia dziś zatwierdzone zatwierdzone lub zrealizowane
   * 
   * @return array
   */
  private function getConfirmedOrRealizedSingle() {
    // dziś zatwierdzone zatwierdzone lub zrealizowane
    
    $sSql = '
      SELECT O.transport_id, count(1) as single_count
      FROM orders AS O
      WHERE 
        (
          (
            O.order_status = "3" OR
            O.order_status = "4"
          ) AND
          DATE(O.status_3_update) = CURDATE()
        )
        '.$this->getUniqeByOI().'
      GROUP BY O.transport_id
      ';
    return $this->addTransportName($this->pDbMgr->GetAssoc('profit24', $sSql, true));
  }// end of getConfirmedOrRealized() method
  
  
  /**
   * Pobieramy zamówienia nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
   * 
   * @return array
   */
  private function getOpenPlannedOrders() {
    // nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
    /*
    // pobierz jeszcze zrealizowane i zatwierdzone dzisiejsze, te rekordy należy dodać do aktualnego dnia
    $sSql = 'SELECT count(id)
             FROM orders 
             WHERE 
                  (
                   order_status = "4" OR
                   order_status = "3" 
                  )
                  AND DATE(status_3_update) = CURDATE()
                  AND DATE(shipment_date_expected) = CURDATE()
             ';
    $iTodayRealizedCount = intval($this->pDbMgr->GetOne('profit24', $sSql));
    */
    $sSql = 'SELECT shipment_date_expected, count(1) as count_planned, "0" AS link
             FROM orders AS O
             WHERE 
              (
                (
                  order_status <> "5" AND 
                  order_status <> "4" AND 
                  order_status <> "3" 
                )
                OR (
                    (
                     order_status = "4" OR
                     order_status = "3" 
                    )
                    AND DATE(status_3_update) = CURDATE()
                    AND DATE(shipment_date_expected) = CURDATE()
                )
              )
             GROUP BY shipment_date_expected
             ORDER BY shipment_date_expected DESC
             
             ';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, true);
  }// end of getOpenPlannedOrders() method
  
  /**
   * Pobieramy zamówienia nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
   * 
   * @return array
   */
  private function getOpenPlannedOrdersSingle() {
    // nie anulowane nie zrealizowane nie zatwierdzone i nie anulowane
    /*
    // pobierz jeszcze zrealizowane i zatwierdzone dzisiejsze, te rekordy należy dodać do aktualnego dnia
    $sSql = 'SELECT count(id)
             FROM orders 
             WHERE 
                  (
                   order_status = "4" OR
                   order_status = "3"
                  )
                  AND DATE(status_3_update) = CURDATE()
                  AND DATE(shipment_date_expected) = CURDATE()
             ';
    $iTodayRealizedCount = intval($this->pDbMgr->GetOne('profit24', $sSql));
    */
    $sSql = 'SELECT shipment_date_expected, count(1) as single_count_planned, "0" AS single_link
             FROM orders AS O
             WHERE 
              (
                (
                  order_status <> "5" AND 
                  order_status <> "4" AND 
                  order_status <> "3" 
                )
                OR (
                    (
                     order_status = "4" OR
                     order_status = "3" 
                    )
                    AND DATE(status_3_update) = CURDATE()
                    AND DATE(shipment_date_expected) = CURDATE()
                )
              )
              '.$this->getUniqeByOI().'
             GROUP BY shipment_date_expected
             ORDER BY shipment_date_expected DESC
             
             ';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, true);
  }// end of getOpenPlannedOrders() method
  
  
  /**
   * Pobieramy zamówienia zatwierdzone lub zrealizowane, a dzisiaj zatwierdzone
   * 
   * @return array
   */
  private function getRealizedOrConfirmedToday() {
    
    // zatwierdzone lub zrealizowane, a dzisiaj zatwierdzone
    $sSql = '
      SELECT shipment_date_expected, count(1) as count_confirmed
      FROM orders AS O
      WHERE 
        (
          order_status = "3" OR
          order_status = "4"
        ) AND
        DATE(status_3_update) = CURDATE()
      GROUP BY shipment_date_expected
      ORDER BY shipment_date_expected DESC
      ';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, true);
  }// end of getRealizedOrConfirmedToday() method
  
  
  /**
   * Pobieramy zamówienia zatwierdzone lub zrealizowane, a dzisiaj zatwierdzone
   * 
   * @return array
   */
  private function getRealizedOrConfirmedTodaySingle() {
    
    // zatwierdzone lub zrealizowane, a dzisiaj zatwierdzone
    $sSql = '
      SELECT shipment_date_expected, count(DISTINCT O.id) AS single_count_confirmed
      FROM orders AS O
      JOIN orders_items AS OI
      WHERE 
        (
          order_status = "3" OR
          order_status = "4"
        ) AND
        DATE(status_3_update) = CURDATE()
        '.$this->getUniqeByOI().'
      GROUP BY shipment_date_expected
      ORDER BY shipment_date_expected DESC
      ';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, true);
  }// end of getRealizedOrConfirmedToday() method
  
  
  /**
   * 
   * @return string
   */
  private function getUniqeByOI() {
    $sSql = '
        AND (
         SELECT COUNT(OI_2.id)
         FROM orders_items AS OI_2
         WHERE OI_2.order_id = O.id
              AND OI_2.deleted = "0"
              AND (OI_2.item_type = "I" OR OI_2.item_type = "P")
              AND OI_2.packet = "0"
        ) = 1
      ';
    return $sSql;
  }
}