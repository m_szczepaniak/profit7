<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
use omniaCMS\lib\Products\ProductsStockView;

class Module__stats__stat_ordered_items {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  /**
   *
   * @var DatabaseManager $pDbMgr
   */
  private $pDbMgr;
  
  
  function __construct($pSmarty) {
    global $pDbMgr;
    
    $this->pDbMgr = $pDbMgr;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		$this->aPrivileges =& $_SESSION['user']['privileges'];
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
		if ($_SESSION['user']['type'] === 0) {
			if (!hasModulePrivileges($_GET['module_id'])){
				showPrivsAlert($pSmarty);
				return;
			}
		}
    
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
    switch ($sDo) {
      case 'change_procedure':
      default:
        $this->Show($pSmarty);
      break;
      
      case 'get_stats':
        $this->GetStats($pSmarty);
      break;
    }
  }
  
  
  /**
   * Metoda wyświetla formularz do wyboru zakresu dat w obrębie których ma generować się raport
   * 
   * @global array $aConfig
   * @param object $pSmarty
   */
  private function Show($pSmarty) {

    $sHTML = $this->ShowHTML();
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($sHTML));
  }// end of Show() method
  
  
  /**
   * Metoda wyświetla kod html widoku
   * 
   * @global array $aConfig
   * @return string HTML
   */
  private function ShowHTML() {
    global $aConfig;
    
    $aData = array();
    if (isset($_POST)) {
      $aData = $_POST;
    }
    
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('stat_ordered_items', _('Najczęściej zamawiane pozycje'));
    
    $pForm->AddHidden('do', 'get_stats');

      $options = array(
          array('label' => _('Wyszukiwanie na podstawie historii zamówień - Używać jeśli mały zakres dat'), 'value' => '0'),
          array('label' => _('Wyszukiwanie na podstawie historii zamówień - Używać jeśli duży zakres dat - STARY MECHANIZM'), 'value' => '2'),
          array('label' => _('Wyszukiwanie z uwzględnieniem odchylenia w cenie zakupu'), 'value' => '1')
      );
    $pForm->AddSelect('procedure_id', _('Procedura'), array('onchange'=>'ChangeObjValue(\'do\', \'change_procedure\'); this.form.submit();'), $options, $aData['procedure_id'], '', false);
    
    if ($_POST['procedure_id'] != 1) {
    
      $pForm->AddMergedRow(_('Zamówienia'), array('class'=>'merged'));

      $pForm->AddSelect('website_id', _('Serwis'), array(), $this->getWebsitesSelect(), $aData['website_id'], '', false);

      // zakres dat od
      $pForm->AddRow($pForm->GetLabelHTML('', _('Data złożenia zamówienia ')), 
                    '<div style="float: left; margin-top: 8px;">'.
                    $pForm->GetLabelHTML('start_date', _('Od'), false).
                    '</div>
                      <div style="float: left; padding-left: 5px;">'.
                        $pForm->GetTextHTML('start_date', _('Od'), $aData['start_date'], array('style'=>'width: 75px;'), '', 'date').
              '</div><div style="float: left; margin-top: 8px; padding-left:5px;">'.
              $pForm->GetLabelHTML('end_date', 'do', false).
              '</div><div style="float: left; padding-left: 10px;">'.
              $pForm->GetTextHTML('end_date', _('do'), $aData['end_date'], array('style'=>'width: 75px;'), '', 'date').
              '</div>');

      $pForm->AddText('limit_rows', _('Max. ilosć produktów'), $aData['limit_rows'] > 0 ? $aData['limit_rows'] : 100, array(), '', 'uint', true);
    }
    
    $pForm->AddMergedRow(_('Produkty'), array('class'=>'merged'));
    
    if ($_POST['procedure_id'] == 1) {
       $pForm->AddCheckBox('is_available', _('Uwzględnij tylko dostępne produkty'), array(), '', $aData['is_available'], false);
    }
    
    $pForm->AddText('publisher', _('Wydawnictwo'), $aData['publisher'], array('id' => 'publisher_aci', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', false);

    $pForm->AddSelect('publisher_group', _('Grupa wydawnictw'), [], addDefaultValue($this->getPublishersGroups(), _('Wszystkie')), $aData['publisher_group'], '', false);
    $moduleId = getModuleId('m_oferta_produktowa');
    $pForm->AddRow(_('Link do grup wydawnictw'), '<a target="_blank" href="'.phpSelf(['module_id' => $moduleId, 'module' => 'm_oferta_produktowa', 'action' => 'publishers_groups']).'">Wydawnictwa</a>');

    $aPublicationYears = $this->getPublicationYears(); 
    $pForm->AddSelect('publication_year', _('Rok wydania'), array(), $aPublicationYears, $aData['publication_year'], '', false); 
    
    $aSources = $this->getSources();
    $pForm->AddSelect('source', _('Dostępne w źródle'), array(), $aSources, $aData['source'], '', false); 
    
    $pForm->AddSelect('source_not', _('Niedostępny w źródle'), array(), $aSources, $aData['source_not'], '', false); 

    /* if ($_POST['procedure_id'] == 1) {
      $pForm->AddText('difference_value', _('Wartość odchylenia'), $aData['difference_value'], array(), '', 'uint', false);
    } */
    
		// funkcja JS - setStatus
		$sJS = '
		<script type="text/javascript">
      $(document).ready(function(){
        $("#publisher_aci").autocomplete({
                  source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetPublishers.php"
        });
      });
    </script>';
    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Generuj')).'&nbsp;&nbsp;');
		return $pForm->ShowForm().$sJS;
  }// end of ShowHTML() method

  /**
   * @return array
   */
  private function getPublishersGroups() {

    $sSql = 'SELECT id AS value, name AS label FROM products_publishers_groups ORDER BY id ASC';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

  /**
   * Metoda pobiera tablice serwisów do wyboru
   * 
   * @return array
   */
  private function getWebsitesSelect() {
    
    $sSql = 'SELECT name AS label, id AS value 
             FROM websites';
    $aWebsites = $this->pDbMgr->GetAll('profit24', $sSql);
    return addDefaultValue($aWebsites, _('Wszystkie'));
  }// end of getWebsitesSelect() method
  
  
  /**
   * Metoda pobiera tablice lat wydania do wyboru
   * 
   * @return array
   */
  private function getPublicationYears() {

    $sSql = 'SELECT year AS label, year AS value
             FROM products_years
             ORDER BY year DESC ';
    $aPublicationYears = $this->pDbMgr->GetAll('profit24', $sSql);
    return addDefaultValue($aPublicationYears, _('Wszystkie'));
  }// end of getPublicationYears() method
  
  
  /**
   * Metoda zwraca tablice źródeł dla listy wyboru
   * 
   * @return array
   */
  private function getSources() {

    $aSources = array('azymut', 'ateneum', 'siodemka', 'platon', 'dictum', 'profit_g', 'profit_j', 'profit_m', 'profit_e');
    $aSourcesOptions = array();
    
    foreach ($aSources as $sSource) {
      $aSourcesOptions[] = array('label' => ucfirst(str_replace('profit_', '', $sSource)), 'value' => $sSource);
    }
    
    return addDefaultValue($aSourcesOptions, _('Wszystkie'));    
  }// end of getSources() method
  
  
  /**
   * Pobiera html formularza
   * 
   * @param type $pSmarty
   * @return type
   */
  private function GetStats(&$pSmarty) {

      //$oProductsStockView = new ProductsStockView($this->pDbMgr, $pSmarty);
      //$oProductsStockView->setHideOnStart();
      //$aSourcesData = $this->_getSourcesData();

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
    
    $bIsAvailable = false;
    if (isset($_POST['is_available'])) {
      $bIsAvailable = true;
    }

    $iProcedure = 1;
    if (!isset($_POST['procedure_id']) || $_POST['procedure_id'] == '0') {
      $iProcedure = 0;
    } elseif ($_POST['procedure_id'] == '2') {
      $iProcedure = 2;
    }
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Show($pSmarty);
			return;
		}
    
		include_once('View/View.class.php');
    
		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'checkboxes'	=> false,
			'form'	=>	false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> _('Lp'),
				'width' => '30'
			),
			array(
				'content'	=> _('ISBN'),
				'width' => '60'
			),
            array(
                'content'	=> _('Kod producenta'),
                'width' => '60'
            ),
			array(
				'content'	=> _('Nazwa'),
			),
			array(
				'content'	=> _('Wydawnictwo'),
				'width' => '60'
			),
			array(
				'content'	=> _('Min. cena zakupu'),
				'width' => '50'
			),
			array(
				'content'	=> _('Wydanie'),
				'width' => '60'
			),
			array(
				'content'	=> _('Zam.'),
				'width' => '50'
			),
			array(
				'content'	=> _('Egz.'),
				'width' => '50'
			),
			array(
				'content'	=> _('Stan G'),
				'width' => '50'
			),
            array(
                'content'	=> _(' N'),
                'width' => '50',
                  ),
            array(
                'content'	=> _(' J'),
                'width' => '50'
                  ),
            array(
                'content'	=> _(' Azymut'),
                'width' => '80'
                  ),
            array(
                'content'	=> _(' Ateneum'),
                'width' => '80'
                  ),
            array(
                'content'	=> _(' Siódemka'),
                'width' => '80'
                  ),
            array(
                'content'	=> _(' Dictum'),
                'width' => '80'
                  ),
            array(
                'content'	=> _(' Platon'),
                'width' => '80',
                  ),
            array(
                'content'	=> _(' Panda'),
                'width' => '80',
            ),
            array(
                'content'	=> _(' Odchylenie'),
                'width' => '80',
                  ),
              );

    if ($iProcedure == 0) {
        $aBooks = $this->getBooksOptimize($_POST['start_date'], $_POST['end_date'], $_POST['limit_rows'], $_POST['publisher'], $_POST['publisher_group'], $_POST['website_id'], $_POST['publication_year'], $_POST['source'], $_POST['source_not']);

        unset($aRecordsHeader[18]);
    } else if ($iProcedure == 2) {
      $aBooks = $this->getBooks($_POST['start_date'], $_POST['end_date'], $_POST['limit_rows'], $_POST['publisher'], $_POST['publisher_group'], $_POST['website_id'], $_POST['publication_year'], $_POST['source'], $_POST['source_not']);

      unset($aRecordsHeader[18]);
    } else {
      unset($aRecordsHeader[5]);
      unset($aRecordsHeader[6]);
      $aBooks = $this->getBooksAverageDifference($bIsAvailable, $_POST['publisher'], $_POST['publisher_group'], $_POST['publication_year'], $_POST['source']/*, $_POST['difference_value']*/);
    }
    
		$pView = new View('ordered_itm', $aHeader, $aAttribs);
    $pView->AddRecordsHeader($aRecordsHeader);
    
		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array(
//        'location'	=> array(
//					'show'	=> false
//					),
//        'azymut_wholesale_price'	=> array(
//					'show'	=> false
//					),
//        'siodemka_wholesale_price'	=> array(
//					'show'	=> false
//					),
//        'ateneum_wholesale_price'	=> array(
//					'show'	=> false
//					),
//        'ateneum_wholesale_price'	=> array(
//					'show'	=> false
//					),
        );
    
    $aSources = array('azymut', 'ateneum', 'siodemka', 'olesiejuk', 'dictum', 'platon','panda');

    if (!empty($aBooks)) {
      foreach ($aBooks as $iKey => $aBook) {
        //$aStocks[$aBook['product_id']] = $oProductsStockView->getProductsStockViewData($aBook['product_id']);
        $sMinSource = $this->checkMinSources($aSources, $aBook);
        $aBooks[$iKey] = $this->ConCatStock($aSources, $sMinSource, $aBook);
        if ($aBook['profit_g_status'] > 0) {
          $aBooks[$iKey]['profit_g_status'] = '<span style="font-weight: bold">' . $aBook['profit_g_status'] . ' S</span><hr /> ' . $aBook['profit_g_reservations'] . ' R';
        } else {
          $aBooks[$iKey]['profit_g_status'] = $aBook['profit_g_status'] . ' S<hr /> ' . $aBook['profit_g_reservations'] . ' R';
        }
        $aBooks[$iKey]['lp'] = $iKey + 1;
        $aBooks[$iKey]['name'] = '<a class="stockTooltip" href="#stockTooltip_' . $aBook['product_id'] . '" rel="#stockTooltip_' . $aBook['product_id'] . '">' . $aBook['name'] . '</a>';


        unset($aBooks[$iKey]['product_id']);
        unset($aBooks[$iKey]['diff_price']);
        unset($aBooks[$iKey]['profit_g_reservations']);
      }
    }
    
		// dodanie rekordow do widoku
		$pView->AddRecords($aBooks, $aColSettings);
    
		$aRecordsFooter = array();
		$pView->AddRecordsFooter($aRecordsFooter);
    
    // zawsze wyświetlamy show
    $sHTML = $this->ShowHTML();
    //$sStockHtml = $oProductsStockView->getMultipleStockView($aStocks, $aSourcesData);
	/*
    $sJS = '
        <script type="text/javascript">
					
			$(document).ready(function(){
    							
    							$("a.stockTooltip").cluetip({
                width: "335px", 
                local:true, 
                showTitle: false, 
                sticky: true,
                arrows: true,
                closePosition: "top",
                closeText: "Zamknij",
                mouseOutClose: true
              });
    		});
		</script>
              ';
			  */
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sHTML.$sStockHtml.$sJS.$pView->Show());
  }


    /**
     *
     * @return array
     */
    private function _getSourcesData() {
        $sSql = 'SELECT * FROM sources AS S
             ORDER BY order_by';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }
  
  
  /**
   * Metoda łączy stan z ceną
   * 
   * @param array $aSources
   * @param string $sMinSource
   * @param array $aBook
   * @return string
   */
  private function ConCatStock($aSources, $sMinSource, $aBook) {
    
    foreach ($aSources as $sSource) {
      if ($sMinSource == $sSource) {
        $aBook[$sSource.'_stock'] = '<span style="color: red;">'.$aBook[$sSource.'_stock'].'<hr />'.Common::FormatPrice($aBook[$sSource.'_wholesale_price']).'</span>';
      } else {
        if ($aBook[$sSource.'_stock'] != '' || Common::FormatPrice($aBook[$sSource.'_wholesale_price']) > 0) {
          $aBook[$sSource.'_stock'] = $aBook[$sSource.'_stock'].'<hr />'.Common::FormatPrice($aBook[$sSource.'_wholesale_price']);
        } else {
          $aBook[$sSource.'_stock'] = '';
        }
      }
      unset($aBook[$sSource.'_wholesale_price']);
    }
    
    return $aBook;
  }
  
  
  /**
   * Metoda znajduje najniższą cenę w tablicy elementów
   * 
   * @param array $aSources
   * @param array $aBook
   * @return boolean
   */
  private function checkMinSources($aSources, $aBook) {
    $fMinPrice = 0;
    $sMinSource = '';

    
    foreach ($aSources as $sSource) {
      if ($aBook[$sSource.'_wholesale_price'] > 0 && $aBook[$sSource.'_stock'] != '') {
        
        if (is_numeric($aBook[$sSource.'_stock'])) {
          if ($aBook[$sSource.'_stock'] > 0) {
            // jeśli jest numeryczny ma być większy od 0
            if ($fMinPrice > 0 && $fMinPrice > $aBook[$sSource.'_wholesale_price']) {
              $fMinPrice = $aBook[$sSource.'_wholesale_price'];
              $sMinSource = $sSource;
            } 
            elseif ($fMinPrice == 0) {
              $fMinPrice = $aBook[$sSource.'_wholesale_price'];
              $sMinSource = $sSource;
            }
          }
        } else {
          // jeśli nie numeryczny to bierzemy pod uwagę
          if ($fMinPrice > 0 && $fMinPrice > $aBook[$sSource.'_wholesale_price']) {
            $fMinPrice = $aBook[$sSource.'_wholesale_price'];
            $sMinSource = $sSource;
          } 
          elseif ($fMinPrice == 0) {
            $fMinPrice = $aBook[$sSource.'_wholesale_price'];
            $sMinSource = $sSource;
          }
        }
      }
    }
    if ($fMinPrice > 0) {
      return $sMinSource;
    }
    return false;
  }

  /**
   * Metoda pobiera ksiażki zamówione przez klientów w danym przedziale czasu
   * 
   * @param string $sStartDate
   * @param string $sEndDate
   * @return book
   */
  private function getBooks($sStartDate, $sEndDate, $iLimitRows, $sPublisher, $iPublisherGroupId, $iWebsiteId, $sPublicationYear, $sSource, $sSourceNot) {

    $sSql = '
      SELECT "0" as lp, IF(P.ean_13 <> "", P.ean_13, OI.isbn), PGA.lego_code, OI.name, PP.name as publisher, P.wholesale_price, OI.publication_year, COUNT(OI.id) AS orders_count, SUM(OI.quantity),
          PS.profit_g_status, PS.profit_g_reservations,
          PS.profit_e_status,
          PS.profit_j_status,
          IF(PS.azymut_status = "0", "0", PS.azymut_stock) AS azymut_stock, 
            PS.azymut_wholesale_price,
          IF(PS.ateneum_status = "0", "0", PS.ateneum_stock) AS ateneum_stock, 
            PS.ateneum_wholesale_price,
          IF(PS.siodemka_status = "0", "0", PS.siodemka_stock) AS siodemka_stock, 
            PS.siodemka_wholesale_price,
          IF(PS.dictum_status = "0", "0", PS.dictum_stock) AS dictum_stock, 
            PS.dictum_wholesale_price,
          IF(PS.platon_status = "0", "0", PS.platon_stock) AS platon_stock, 
            PS.platon_wholesale_price,
          IF(PS.panda_status = "0", "0", PS.panda_stock) AS panda_stock, 
            PS.panda_wholesale_price,
            P.id as product_id
      FROM orders_items AS OI
      JOIN orders AS O
        ON OI.order_id = O.id AND O.order_status <> "5"
      JOIN products_stock AS PS
        ON PS.id = OI.product_id
      JOIN products AS P
        ON P.id = OI.product_id
      JOIN products_publishers AS PP
        ON P.publisher_id = PP.id
      LEFT JOIN products_game_attributes AS PGA
        ON P.id = PGA.product_id
      WHERE 1 = 1 AND
            OI.deleted = "0"
                     ' . ($iWebsiteId > 0 ? ' AND O.website_id = '.$iWebsiteId : '') . '
      							 AND O.order_date >= "'.formatDate($sStartDate).' 00:00:00"
							 			 AND O.order_date <= "'.formatDate($sEndDate).' 23:59:59" 
                     ' . ($sPublisher != '' ? ' AND PP.name = "'.$sPublisher.'"' : '') . '
                     ' . ($iPublisherGroupId != '' ? '
                      AND 
                      (
                      SELECT PPTG.id FROM products_publishers_to_groups AS PPTG
                      WHERE PPTG.publisher_id = P.publisher_id
                       AND PPTG.group_id = "'.$iPublisherGroupId.'"  
                      ) IS NOT NULL
                      ' : '') . '
                     ' . ($sPublicationYear != '' ? ' AND OI.publication_year = "'.$sPublicationYear.'"' : '') . '
                     ' . ($sSource != '' ? ' AND PS.'.$sSource.'_status > 0' : '') . '    
                     ' . ($sSourceNot != '' ? ' AND PS.'.$sSourceNot.'_status <= 0' : '') . '    
      GROUP BY OI.product_id
      ORDER BY orders_count DESC
      LIMIT '.$iLimitRows;
    $data = $this->pDbMgr->GetAll('profit24', $sSql);
    return $data;
  }


    /**
     * @param $sStartDate
     * @param $sEndDate
     * @param $iLimitRows
     * @param $sPublisher
     * @param $iPublisherGroupId
     * @param $iWebsiteId
     * @param $sPublicationYear
     * @param $sSource
     * @param $sSourceNot
     * @return array
     */
    private function getBooksOptimize($sStartDate, $sEndDate, $iLimitRows, $sPublisher, $iPublisherGroupId, $iWebsiteId, $sPublicationYear, $sSource, $sSourceNot) {

      $sSql = '
SELECT "0" as lp, IF(P.ean_13 <> "", P.ean_13, OI.isbn), PGA.lego_code, OI.name, PP.name as publisher, P.wholesale_price, OI.publication_year, "0" AS orders_count, OI.quantity as sum_quantity,
          PS.profit_g_status, PS.profit_g_reservations,
          PS.profit_e_status,
          PS.profit_j_status,
          IF(PS.azymut_status = "0", "0", PS.azymut_stock) AS azymut_stock, 
            PS.azymut_wholesale_price,
          IF(PS.ateneum_status = "0", "0", PS.ateneum_stock) AS ateneum_stock, 
            PS.ateneum_wholesale_price,
          IF(PS.siodemka_status = "0", "0", PS.siodemka_stock) AS siodemka_stock, 
            PS.siodemka_wholesale_price,
          IF(PS.dictum_status = "0", "0", PS.dictum_stock) AS dictum_stock, 
            PS.dictum_wholesale_price,
          IF(PS.platon_status = "0", "0", PS.platon_stock) AS platon_stock, 
            PS.platon_wholesale_price,
          IF(PS.panda_status = "0", "0", PS.panda_stock) AS panda_stock, 
            PS.panda_wholesale_price,
            P.id as product_id
      FROM orders_items AS OI
      JOIN orders AS O
        ON OI.order_id = O.id AND O.order_status <> "5"
              							 AND O.order_date >= "'.formatDate($sStartDate).' 00:00:00"
							 			 AND O.order_date <= "'.formatDate($sEndDate).' 23:59:59" 
      JOIN products AS P
        ON P.id = OI.product_id
      JOIN products_publishers AS PP
        ON P.publisher_id = PP.id
      JOIN products_stock AS PS
        ON PS.id = P.id
      LEFT JOIN products_game_attributes AS PGA
        ON P.id = PGA.product_id
      WHERE 1 = 1 AND
            OI.deleted = "0"
                     ' . ($iWebsiteId > 0 ? ' AND O.website_id = '.$iWebsiteId : '') . '
                     ' . ($sPublisher != '' ? ' AND PP.name = "'.$sPublisher.'"' : '') . '
                     ' . ($iPublisherGroupId != '' ? '
                      AND 
                      (
                      SELECT PPTG.id FROM products_publishers_to_groups AS PPTG
                      WHERE PPTG.publisher_id = P.publisher_id
                       AND PPTG.group_id = "'.$iPublisherGroupId.'"  
                       LIMIT 1
                      ) IS NOT NULL
                      ' : '') . '
                     ' . ($sPublicationYear != '' ? ' AND OI.publication_year = "'.$sPublicationYear.'"' : '') . '
                     ' . ($sSource != '' ? ' AND PS.'.$sSource.'_status > 0' : '') . '    
                     ' . ($sSourceNot != '' ? ' AND PS.'.$sSourceNot.'_status <= 0' : '') . '';
        $data = $this->pDbMgr->GetAll('profit24', $sSql);
        $newData = $this->sumArrayColsGroup($data, 'product_id', ['sum_quantity'], 'orders_count', 'orders_count');
        unset($data);

        $newData = array_slice($newData, 0, $iLimitRows);

        return $newData;
    }

    /**
     * @param $aData
     * @param $sGroupByCol
     * @param $aSumCols
     * @param string $sortAfterByColumn
     * @return array
     */
    private function sumArrayColsGroup($aData, $sGroupByCol, $aSumCols, $sortAfterByColumn = '', $countGroupedBy = '') {

        $newArray = [];
        $sortByColumnArray = [];
        foreach ($aData as $iKey => $aItem) {
            $colGroupByValue = $aItem[$sGroupByCol];
            if (!isset($newArray[$colGroupByValue])) {
                // define
                $newArray[$colGroupByValue] = $aItem;
            }
            else {
                // only sum Values
                foreach ($aSumCols as $sSumColumn) {
                    $newArray[$colGroupByValue][$sSumColumn] += $aItem[$sSumColumn];
                }
            }
            if ($countGroupedBy != '') {
                // zliczenie ile jest zgrupowanych
                $newArray[$colGroupByValue][$countGroupedBy]++;
            }
            if ($sortAfterByColumn != '') {
                // tworzymy nową tabelkę pomocną później przy sortowaniu
                $sortByColumnArray[$colGroupByValue] = $newArray[$colGroupByValue][$sortAfterByColumn];
            }
        }
        if (!empty($sortByColumnArray)) {
            // sortujemy
            array_multisort($sortByColumnArray, SORT_DESC, $newArray);
        }
        return $newArray;
    }


  /**
   * Metoda pobiera produkty uwzględniając "odchylenie standardowe" 
   * 
   * @param bool $bIsAvailable
   * @param string $sPublisher
   * @param string $sPublicationYear
   * @param int $iDifferenceValue
   * @return array
   */
  private function getBooksAverageDifference($bIsAvailable, $sPublisher, $iPublisherGroupId, $sPublicationYear, $sSource/*, $iDifferenceValue*/) {
    $iDifferenceValue = str_replace(',', '.', $iDifferenceValue);
    
    $sSql = '
      SELECT "0" as lp, IF(P.ean_13 <> "", P.ean_13, P.isbn_plain), PGA.lego_code, P.name, PP.name as publisher, P.wholesale_price, P.publication_year,
          PS.profit_g_status, PS.profit_g_reservations,
          PS.profit_e_status,
          PS.profit_j_status,
          IF(PS.azymut_status = "0", "0", PS.azymut_stock) AS azymut_stock, 
            PS.azymut_wholesale_price,
          IF(PS.ateneum_status = "0", "0", PS.ateneum_stock) AS ateneum_stock, 
            PS.ateneum_wholesale_price,
          IF(PS.siodemka_status = "0", "0", PS.siodemka_stock) AS siodemka_stock, 
            PS.siodemka_wholesale_price,
          IF(PS.dictum_status = "0", "0", PS.dictum_stock) AS dictum_stock, 
            PS.dictum_wholesale_price,
          IF(PS.platon_status = "0", "0", PS.platon_stock) AS platon_stock, 
            PS.platon_wholesale_price,
          IF(PS.panda_status = "0", "0", PS.panda_stock) AS panda_stock, 
            PS.panda_wholesale_price,
      @diff_price:=
      IF (`azymut_wholesale_price` > 0, `azymut_wholesale_price`, 
              IF (`dictum_wholesale_price` > 0, `dictum_wholesale_price`, 
                  IF (`siodemka_wholesale_price` > 0, `siodemka_wholesale_price`, 
                      IF (`olesiejuk_wholesale_price` > 0, `olesiejuk_wholesale_price`, 
                          IF (`ateneum_wholesale_price` > 0, `ateneum_wholesale_price`, 
                            IF (`platon_wholesale_price` > 0, `platon_wholesale_price`, 
                                IF (`panda_wholesale_price` > 0, `panda_wholesale_price`, 
                                  0
                                )
                            )
                          )
                      )
                  )
              )
      ) AS diff_price
      ,
      ABS
      ((
      (    (
              (IF (azymut_wholesale_price > 0, azymut_wholesale_price - @diff_price, 0)) +
              (IF (dictum_wholesale_price > 0, dictum_wholesale_price - @diff_price, 0)) +
              (IF (siodemka_wholesale_price > 0, siodemka_wholesale_price - @diff_price, 0)) +
              (IF (olesiejuk_wholesale_price > 0, olesiejuk_wholesale_price - @diff_price, 0)) +
              (IF (platon_wholesale_price > 0, platon_wholesale_price - @diff_price, 0)) +
              (IF (ateneum_wholesale_price > 0, ateneum_wholesale_price - @diff_price, 0)) + 
              (IF (panda_wholesale_price > 0, panda_wholesale_price - @diff_price, 0))
          ) * 100 ) / @diff_price
      ) ) AS STD_S,
      P.id AS product_id
      FROM `products_stock` AS PS 
      JOIN products AS P
        ON P.id = PS.id
      JOIN products_publishers AS PP 
        ON P.publisher_id = PP.id
      LEFT JOIN products_publishers_to_groups AS PPTG
        ON PPTG.publisher_id = P.publisher_id
      LEFT JOIN products_game_attributes AS PGA
        ON P.id = PGA.product_id
      WHERE 
      1 = 1
       '.($bIsAvailable ? ' AND P.prod_status = "1" ' : '').'
       ' . ($sPublisher != '' ? ' AND PP.name = "'.$sPublisher.'"' : '') . '
       ' . ($iPublisherGroupId != '' ? ' AND PPTG.group_id = "'.$iPublisherGroupId.'" AND PPTG.id IS NOT NULL ' : '') . '
       ' . ($sPublicationYear != '' ? ' AND P.publication_year = "'.$sPublicationYear.'"' : '') . '
       ' . ($sSource != '' ? ' AND PS.'.$sSource.'_status = "1"' : '') . '  
      AND
      
      IF (`azymut_wholesale_price` > 0, `azymut_wholesale_price`, 
              IF (`dictum_wholesale_price` > 0, `dictum_wholesale_price`, 
                  IF (`siodemka_wholesale_price` > 0, `siodemka_wholesale_price`, 
                      IF (`olesiejuk_wholesale_price` > 0, `olesiejuk_wholesale_price`, 
                          IF (`ateneum_wholesale_price` > 0, `ateneum_wholesale_price`, 
                            IF (`platon_wholesale_price` > 0, `platon_wholesale_price`, 
                                IF (`panda_wholesale_price` > 0, `panda_wholesale_price`, 
                                    0
                                )
                            )
                          )
                      )
                  )
              )
      ) > 0
      
      GROUP BY P.id
      ORDER BY STD_S DESC
      LIMIT 30';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
}

