<?php

use LIB\orders\counter\OrdersCounter;
use omniaCMS\modules\m_stats\CompletationItems;
use omniaCMS\modules\m_stats\StatsListsTypes;

/**
 * Skrypt statystyk serwisu.
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-10
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__stats__packed_send
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    private $pDbMgr;

    function __construct($pSmarty)
    {
        global $pDbMgr, $aConfig;

        $this->pDbMgr = $pDbMgr;
        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        if (isset($_GET['pid'])) {
            $iPId = intval($_GET['pid']);
        }
        $this->aPrivileges =& $_SESSION['user']['privileges'];
        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }


        $sSql = 'SELECT count(DISTINCT order_id)
               FROM orders_packing_times
               WHERE status = "9"
               AND DATE(created) = CURRENT_DATE() 
                ';
        $packingOrdersQuantity = $this->pDbMgr->GetOne('profit24', $sSql);

        $ordersCounter = new OrdersCounter($this->pDbMgr);
        $quantityPackedSortOut = $ordersCounter->getCounter(OrdersCounter::TYPE_SEND_SORTOUT, date('Y-m-d'));



        $sHtml = '
<table>
    <tr>
        <td>DZISIAJ SPAKOWANE* - </td>
        <td><h1>' . $packingOrdersQuantity.'</h1></td>
    </tr>
    <tr>
        <td>DZISIAJ ODSORTOWANE** NA WYDAWCE - </td>
        <td><h1>'.$quantityPackedSortOut.'</h1></td>
    </tr>
</table>
<br /><hr /> 
<div style="text-align: left;">
    *Spakowane liczone są na podstawie zamówień które zostały spakowane przez osoby pakujące na magazynie.<br /> 
    **Odsortowane liczone są na podstawie ilości paczek, które pracownik magazynu odsortował na wydawce i oczekują na pobranie delivera.
</div>
';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sHtml);
        return;
    }

}