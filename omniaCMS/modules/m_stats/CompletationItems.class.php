<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-08-13 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_stats;

use DatabaseManager;
use Smarty;

/**
 * Description of CompletationItems
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class CompletationItems {

  /**
   * @var Smarty
   */
  private $pSmarty;

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  
  private $completationItems;
  
  private $aTransport;
  
  public function __construct(DatabaseManager $pDbMgr, Smarty $pSmarty, array $aTransport) {
    $this->pDbMgr = $pDbMgr;
    $this->aTransport = $aTransport;
    $this->completationItems = $this->getCompletationItems();
    $this->pSmarty = $pSmarty;
  }
  
  /**
   * 
   * @return string
   */
  public function getHTML() {
    $aDataCompletation = array();
    $aDataCompletation['completatnion_stock_single'] = $this->getCompletationStockSingle();
    $aDataCompletation['completatnion_stock_full'] = $this->getCompletationStockFull();
    $aDataCompletation['completatnion_stock_part_linked'] = $this->getCompletationStockPartLinked();
    
    $aDataCompletation['completatnion_train_single'] = $this->getCompletationTrainSingle();
    $aDataCompletation['completatnion_train_full'] = $this->getCompletationTrainFull();
    $aDataCompletation['completatnion_train_linked'] = $this->getCompletationTrainLinked();
    
    $aDataCompletation = $this->addSummaries($aDataCompletation);
    $this->pSmarty->assign_by_ref('aData', $aDataCompletation);
    $sHtml = $this->pSmarty->fetch('stats_completation.tpl');
    $this->pSmarty->clear_assign('aData');
    return $sHtml;
  }
  
  /**
   * 
   * @return array
   */
  private function getCompletationTrainSingle() {
    
    $aCompletationData = $this->getCompletationData("1", "1");
    return $this->addTransportName($aCompletationData);
  }
  
  /**
   * 
   * @return array
   */
  private function getCompletationTrainFull() {
    
    $aCompletationData = $this->getCompletationData("1", "0");
    return $this->addTransportName($aCompletationData);
  }
  
  /**
   * 
   * @return array
   */
  private function getCompletationTrainLinked() {
    
    $aCompletationData = $this->getCompletationData("1", "2");
    return $this->addTransportName($aCompletationData);
  }
  
  
  /**
   * 
   * @return array
   */
  private function getCompletationStockSingle() {
    
    $aCompletationData = $this->getCompletationData("0", "1");
    return $this->addTransportName($aCompletationData);
  }
  
  /**
   * 
   * @return array
   */
  private function getCompletationStockFull() {
    
    $aCompletationData = $this->getCompletationData("0", "0");
    return $this->addTransportName($aCompletationData);
  }
  
  /**
   * 
   * @return array
   */
  private function getCompletationStockPartLinked() {
    
    $aCompletationData = $this->getCompletationData("0", "3");
    return $this->addTransportName($aCompletationData);
  }
  
  /**
   * 
   * @param bool $bGetFromTrain
   * @param char $cType
   * @return array
   */
  private function getCompletationData($bGetFromTrain, $cType) {
    
    $aReturn = array();
    foreach ($this->completationItems as $iKey => $aItem) {
      if (intval($aItem['get_from_train']) == intval($bGetFromTrain) && $aItem['type'] == $cType) {
        $aReturn[$aItem['transport_id']]['count']++;
        unset($this->completationItems[$iKey]);
      }
    }
    return $aReturn;
  }
  
  /**
   * 
   * @param bool $bFromTrain
   * @param char $cType
   * @return array
   */
  private function getCompletationItems() {
    
    $sSql = 'SELECT DISTINCT OILI.orders_id,  O.transport_id, OIL.get_from_train, OIL.type
             FROM orders_items_lists_items AS OILI
             JOIN orders_items_lists AS OIL
              ON OIL.id = OILI.orders_items_lists_id
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id AND OI.deleted = "0"
             JOIN orders AS O
              ON OILI.orders_id = O.id AND O.order_status <> "5" AND O.order_status <> "4"
             WHERE 
              OIL.closed = "0"
              AND DATE(OIL.created) = CURDATE() ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * Metoda dodaje metodę transportu do tablicy, na postawie id metody transprotu
   * 
   * @param array $aData
   * @return array
   */
  private function addTransportName($aData) {
    if (!empty($aData)) {
      foreach ($aData as $iKey => $aItem) {
        $aData[$iKey]['transport'] =  $this->aTransport[$iKey];
      }
    }
    ksort($aData);
    return $aData;
  }
  
  /**
   * 
   * @param array $aData
   * @return array
   */
  private function addSummaries($aItemType) {
    
    if (!empty($aItemType) && is_array($aItemType)) {
      foreach ($aItemType as $sListType => $aData) {
        foreach ($aData as $sType => $aItems) {
          if (is_array($aItems)) {
            foreach ($aItems as $sKey => $sValue) {
              if (stristr($sKey, 'count')) {
                $aItemType[$sListType]['SUMA'][$sKey] += intval($sValue);
              }
            }
          }
        }
      }
    }
    return $aItemType;
  }// end of addSummaries() method
}
