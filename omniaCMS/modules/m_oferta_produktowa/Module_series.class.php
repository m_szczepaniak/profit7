<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - serie wydawnicze
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */
// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'serie');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}
		
		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': 						$this->Delete($pSmarty, $iPId, $iId); break;
			case 'insert': 						$this->Insert($pSmarty, $iPId); break;
			case 'update': 						$this->Update($pSmarty, $iPId, $iId); break;
			case 'move_to': 					$this->MoveTo($pSmarty, $iPId, $iId); break;
			case 'save_move_to':			$this->SaveMoveTo($pSmarty, $iPId, $iId); break;
			case 'toseries': 					$this->ToSeriesEdit($pSmarty, $iPId); 			break;
			case 'saveLinking': 			$this->SaveLinking($pSmarty, $iPId); 			break;
			case 'saveLinking2':			$this->SaveLinking2($pSmarty, $iPId); 			break;
			case 'categories': 				$this->EditCategories($pSmarty, $iPId, $iId); break;
			case 'update_categories': $this->UpdateCategories($pSmarty, $iPId, $iId); break;
			case 'linked': 						$this->EditLinked($pSmarty, $iPId, $iId); 	break;
			case 'edit':
			case 'add': 							$this->AddEdit($pSmarty, $iPId,$iId); break;
			
			default: $this->Show($pSmarty,$iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych serii wydawniczych
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId) {
		global $aConfig;
		
		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);
		
		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],$this->getPublisherName($iPId)),
			'refresh'	=> true,
			'search'	=> true,
			'send_button'=>'toseries'
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'discount_limit',
				'content'	=> $aConfig['lang'][$this->sModule]['discount_limit'],
				'sortable'	=> true,
				'width'	=> '100'
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich serii
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_series
						 WHERE publisher_id = ".$iPId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_series";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_series', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);	
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich serii
			$sSql = "SELECT id, publisher_id, name, discount_limit, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."products_series
							 WHERE publisher_id = ".$iPId.
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'publisher_id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'pid' => '{publisher_id}', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('move_to','linked', 'categories', 'edit', 'delete'),
					'params' => array (
												'move_to'			=> array('pid' => '{publisher_id}', 'id'=>'{id}'),
												'linked'			=> array('pid' => '{publisher_id}', 'id'=>'{id}'),
												'categories'	=> array('pid' => '{publisher_id}', 'id'=>'{id}'),
												'edit'				=> array('pid' => '{publisher_id}', 'id'=>'{id}'),
												'delete'			=> array('pid' => '{publisher_id}', 'id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all','go_back'),
			array('add')
		);
		
		// konfiguracja przyciskow stopki
		$aRecordsFooterParams = array(
			'go_back' => array(array('action'=>'publishers'), array('pid'))
		);
		
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda wyswietla formatke wyboru serii do zmapowania
	 * 
	 * @param object $pSmarty
	 * @return void 
	 *
	 */
	
	function ToSeriesEdit(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
				
		$aToLink=array();
		foreach($_POST['delete'] as $iKey => $aItem)
			$aToLink[]=$iKey;
		
		if(empty($aToLink)) {
			//$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['selected_none']);
			$this->Show($pSmarty, $iPId);
			return;
			}
		$aAuthors=array(array('value'=>'0', 'label'=>'-- wybierz --'));
		
		$sSql='SELECT name as label, id as value FROM '.$aConfig['tabls']['prefix'].'products_series WHERE `publisher_id`='.$iPId.' AND id NOT IN ('.implode(',',$aToLink).') ORDER BY name ASC';
		$aAuthors=array_merge($aAuthors,Common::GetAll($sSql));		
			
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_linked'],'"'.implode('", "', Common::GetCol('SELECT name FROM '.$aConfig['tabls']['prefix'].'products_series WHERE id IN ('.implode(',',$aToLink).')')).'"');
		
		$pForm = new FormTable('search_linked', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'toseries');
		
		foreach($aToLink as $iItem)
			$pForm->AddHidden('delete['.$iItem.']', '1');
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['mappings_select'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
	
		
		$pForm->AddRow($pForm->GetLabelHTML('author_search',$aConfig['lang'][$this->sModule]['author_search'],false)
		,$pForm->GetSelectHTML('selected_author', '', array(), $aAuthors, $_POST['selected_author'], '', false));

		$pForm->AddRow('', 
									$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_send'], array('onclick'=>'ChangeObjValue(\'do\', \'saveLinking\')')));
		
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
		
		
		
	}//koniec metody ToSeriesEdit

	/**
	 * Metoda tworzy formularz edycji powiazanych KATEGORII
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanej serii
	 * @param	integer	$ipId	- Id edytowanej serii
	 */
	function EditCategories(&$pSmarty, $iPId=0, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."products_series
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
			//$aData['publisher_mapping'] = $this->getMappings($iId);
		}
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_categories'],$aData['name']);
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		

		$pForm = new FormTable('publishers_categories', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_categories');
			
		$pForm->AddSelect('main_category',$aConfig['lang'][$this->sModule]['main_category'], array('style'=>'width:300px;'), $this->getTopProductsCategories(), $aData['main_category'],'',false);
		$pForm->AddSelect('news_category',$aConfig['lang'][$this->sModule]['news_category'],array(),array_merge(array(array('value'=>0,'label'=>$aConfig['lang']['common']['choose'])),getModulePages('m_powiazane_oferta_produktowa',$this->iLangId,true,'','nowosci')),$aData['news_category'],'',false);
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'sendMuliselects();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of EditCategories() function
	
		/**
	 * Metoda aktualizuje w bazie danych wydawnictwa
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id serii
	 * @return	void
	 */
	function UpdateCategories(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}

			Common::BeginTransaction();
			
			$aValues=array(
				'main_category' => intval($_POST['main_category'])>0?intval($_POST['main_category']):'NULL',
				'news_category' => intval($_POST['news_category'])>0?intval($_POST['news_category']):'NULL'
			);
		
		if (Common::Update($aConfig['tabls']['prefix']."products_series",
												 $aValues,
												 "id = ".$iId) === false) {
				$bIsErr = true;
			}

						
			if (!$bIsErr) {
				// zaktualizowano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_linked_ok'],
												$this->getSeriesName($iId));
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				$this->Show($pSmarty, $iPId);
			}
			else {
				// nie zaktualizowano
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_linked_err'],
												$this->getSeriesName($iId));
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->EditCategories($pSmarty, $iPId, $iId);
			}
		
	} // end of Update() funciton
	
		/**
	 * Metoda zapisuje linkowania autorów i modyfikuje produkty
	 * 
	 * @param object $pSmarty
	 * @return void 
	 *
	 */
	
	function SaveLinking(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;
		// dolaczenie klasy FormTable
		
		$aToLink=array();
		foreach($_POST['delete'] as $iKey => $aItem)
			$aToLink[]=$iKey;
		
		if(empty($aToLink)) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['selected_none']);
			$this->Show($pSmarty, $iPId);
			return;
			}
		if($_POST['selected_author']<=0) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['series_none']);
			$this->ToSeriesEdit($pSmarty, $iPId);
			return;
		}	
		//dump($_POST);
		
		$_GET['do']='series';
		
		$aValues['series_id']=intval($_POST['selected_author']);
		Common::BeginTransaction();
		//zaznaczone linkujemy do wybranego
		if (Common::Update($aConfig['tabls']['prefix']."products_series_mappings",$aValues,"series_import_id IN (".implode(',', $aToLink).")") === false) {
			$bIsErr = true;
			}
		//zlinkowane do zaznaczonych do wybranego	
		if (Common::Update($aConfig['tabls']['prefix']."products_series_mappings",$aValues,"series_id IN (".implode(',', $aToLink).")") === false) {
				$bIsErr = true;
				}	
		//wszystkie o id serii z powyzszej lsity do wybranego
		if (Common::Update($aConfig['tabls']['prefix']."products_to_series",$aValues, "series_id IN (".implode(',', $aToLink).")") === false) {
			$bIsErr = true;
			}

		
		//pobranie nazwy nowej serii
		$aValues['series']=Common::GetOne('SELECT name FROM '.$aConfig['tabls']['prefix'].'products_series WHERE id='.$_POST['selected_author']);
		
		//aktualizacja shadow (series_id i series)
	
			if (Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues," series_id IN (".implode(',', $aToLink).")") === false) {
				$bIsErr = true;
				}
			
		foreach($aToLink as $iItem)
			if($this->deleteItem2($iItem) === false)
				$bIsErr=true;
		
		
		if ($bIsErr) {
			Common::RollbackTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['linking_failed']);
			$this->Show($pSmarty, $iPId);
		}
		else {
			Common::CommitTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['linking_ok'], false);
			
			$this->Show($pSmarty, $iPId);
		}
		
		
	}//koniec metody SaveLinking
	
	/**
	 * Metoda kasuje wczesniejsze linkowania do serii i zapisuje linkowania serii i modyfikuje produkty
	 * 
	 * @param object $pSmarty
	 * @return void 
	 *
	 */
	
	function SaveLinking2(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;
		$aToLink=array();
		$aToLink=$_POST['publisher_mapping'];
		
		if(empty($aToLink)) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['selected_none']);
			return;
			}
		if($_POST['selected_author']<=0) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['author_none']);
			$this->Show($pSmarty, $iPId);
			
			return;
		}	
		
		Common::BeginTransaction();
		$aValues['series_id']=intval($_POST['selected_author']);
		$aValues2['series_id']=intval($_POST['selected_author']);
		
		//skasowac obecne linkowania
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_series_mappings
		WHERE series_id = ".$_POST['selected_author'];
		
		if(Common::Query($sSql) === false)
				$bIsErr=true;	
		foreach($aToLink as $iItem) {
			$aValues['series_import_id']=$iItem;
			//zaznaczonych linkujemy do wybranego
			if (Common::Insert($aConfig['tabls']['prefix']."products_series_mappings",$aValues,'',false) === false) {
				$bIsErr = true;
				}
			//wszystkich o id autora z powyzszej lsity do wybranego
			if (Common::Update($aConfig['tabls']['prefix']."products_to_series",$aValues2, "series_id = $iItem") === false) {
				$bIsErr = true;
				}
			if($iItem!=$_POST['selected_author'])	
				if($this->deleteItem2($iItem) === false)
					$bIsErr=true;	
			}
			
		/*	//zlinkowanych do zaznaczonych do wybranego	
			if (Common::Update($aConfig['tabls']['prefix']."products_series_mappings",$aValues2,"series_id IN (".implode(',', $aToLink).")") === false) {
				$bIsErr = true;
				}	*/
	
		//$this->UpdateShadowAuthorsForAuthor($_POST['selected_author']);
		
		if ($bIsErr) {
			Common::RollbackTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['linking_failed']);
			$this->Show($pSmarty, $iPId);
		}
		else {
			Common::CommitTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['linking_ok'], false);
			
			$this->Show($pSmarty, $iPId);
		}
		
		
	}//koniec metody SaveLinking2
	
	/**
	 * Metoda tworzy formularz edycji powiazanych
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanego autora
	 */
	function EditLinked(&$pSmarty, $iPId, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			$aData['publisher_mapping'] = $this->getMappings($iId);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_linked'],Common::GetOne('SELECT name FROM '.$aConfig['tabls']['prefix'].'products_series WHERE id='.$iId));

		

		$pForm = new FormTable('search_linked', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'linked');
		$pForm->AddHidden('selected_author	', $iId);
			// wybor kategorii ABE
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['mappings_section'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
	// przycisk wyszukaj
		$pForm->AddRow($pForm->GetLabelHTML('publisher_search',$aConfig['lang'][$this->sModule]['publisher_search'],false), 
									$pForm->GetTextHTML('publisher_search',$aConfig['lang'][$this->sModule]['publisher_search'],$_POST['publisher_search'],array('style'=>'width:300px;'),'','text',false).'&nbsp;&nbsp;'.
						//			$pForm->GetInputButtonHTML('search', $aConfig['lang'][$this->sModule]['button_search'], array('onclick'=>'document.getElementById(\'do\').value=\'add\'; sendMuliselects(); this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button'));
						$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_search'], array('onclick'=>'sendMuliselects();')));
//	$pForm2 = new FormTable('products_publishers', '', array('action'=>phpSelf(array('id'=>$iId)), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
	//	$pForm2->AddHidden('do', 'update_linked');					
		$pForm->AddMergedRow(
									$pForm->GetSelectHtml('found_publishers',$aConfig['lang'][$this->sModule]['found_publishers'], array('size'=>20,'style'=>'width:450px;'), $this->getSeries($_POST['publisher_search']),'','',false).'&nbsp;'.
									$pForm->GetInputButtonHTML('add_publ', $aConfig['lang'][$this->sModule]['add_publ'], array('onclick'=>'addPublisherMapping();'), 'button').'&nbsp;'.
									$pForm->GetInputButtonHTML('del_publ', $aConfig['lang'][$this->sModule]['del_publ'], array('onclick'=>'delPublisherMapping();'), 'button').'&nbsp;'.
									$pForm->GetSelectHtml('publisher_mapping',$aConfig['lang'][$this->sModule]['publisher_mapping'], array('size'=>20,'style'=>'width:450px;','multiple'=>1), $this->getAuthorsForIds($aData['publisher_mapping']),'','',false),array());
		
		//$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'sendMuliselects();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('save', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'document.getElementById(\'do\').value=\'saveLinking2\'; sendMuliselects(); this.form.submit();'), 'button').'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		$sJS.='
			function addPublisherMapping(){
				var elSelSrc = document.getElementById(\'found_publishers\'); 
	 			var elSelDest = document.getElementById(\'publisher_mapping\');
	 			
		 			var val = elSelSrc.options[elSelSrc.selectedIndex].value;
		 			var txt =  elSelSrc.options[elSelSrc.selectedIndex].title;
		 		
		 			if (elSelDest.options != undefined) {
	    			for (var i = 0; i < elSelDest.options.length; i++) {
	      			if (elSelDest.options[i].value == val) {
	        			return false;
	      			}
	    			}
	  			}	
	        elSelDest.options.add(new Option(txt,val));
			}
			
			function delPublisherMapping(){
				var elSelDest = document.getElementById(\'publisher_mapping\');
				elSelDest.remove(elSelDest.selectedIndex);
			}	

		';
		$pForm->setAdditionalJS($sJS);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of EditLinked() function
	
	
	/**
	 * Metoda przepisuje ksiazki z jednej serii do drugiej
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanej serii wydawniczej
	 * @return 	void
	 */
	function SaveMoveTo(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		
		$_POST['selected_author'];
		$iId;
		
		Common::BeginTransaction();
		//aktualizacja products_to series
		$aValues['series_id']=intval($_POST['selected_author']);
			
			if (Common::Update($aConfig['tabls']['prefix']."products_to_series",$aValues," series_id = $iId") === false) {
				$bIsErr = true;
				}
		//pobranie nazwy nowej serii
		$aValues['series']=Common::GetOne('SELECT name FROM '.$aConfig['tabls']['prefix'].'products_series WHERE id='.$_POST['selected_author']);
		
		//aktualizacja shadow (series_id i series)
	
			if (Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues," series_id = $iId") === false) {
				$bIsErr = true;
				}
				
		if ($bIsErr) {
			Common::RollbackTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['copy_failed']);
			$this->Show($pSmarty, $iPId);
			}
		else {
			Common::CommitTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['copy_ok'], false);
			
			$this->Show($pSmarty, $iPId);
		}		
	}//end of SaveMoveTo
	
	/**
	 * Metoda wyswietla formatkę wyboru serii do ktorej przenosimy ksiazki
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanej serii wydawniczej
	 * @return 	void
	 */
	function MoveTo(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		$aAuthors=array(array('value'=>'0', 'label'=>'-- wybierz --'));
		$sSql='SELECT name as label, id as value FROM '.$aConfig['tabls']['prefix'].'products_series WHERE `publisher_id`='.$iPId.' AND id <>'.$iId.' ORDER BY name ASC';
		$aAuthors=array_merge($aAuthors,Common::GetAll($sSql));		
			
			
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_move_to'],Common::GetOne('SELECT name FROM '.$aConfig['tabls']['prefix'].'products_series WHERE id='.$iId));
		
		$pForm = new FormTable('search_linked', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'save_move_to');
		
		$pForm->AddRow($pForm->GetLabelHTML('move_to_item',$aConfig['lang'][$this->sModule]['move_to_item'],false)
		,$pForm->GetSelectHTML('selected_author', '', array(), $aAuthors, $_POST['selected_author'], '', false));

		$pForm->AddRow('', 
									$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_send'], array()));
		
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
		
	}//end of MoveTo
	/**
	 * Metoda usuwa wybrane serie wydawnicze
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanej serii wydawniczej
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			// sprawdzenie czy ktorakolwiek z usuwanych serii jest uzywana przez produkty
			if (!$this->seriesAreInUse($_POST['delete'])) {
				Common::BeginTransaction();
				$aItems =& $this->getItemsToDelete($_POST['delete']);
				foreach ($aItems as $aItem) {
					// usuwanie
					if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
						$bIsErr = true;
						$sFailedToDel = $aItem['name'];
						break;
					}
					elseif ($iRecords == 1) {
						// usunieto
						$iDeleted++;
						$sDel .= '"'.$aItem['name'].'", ';
					}
				}
				$sDel = substr($sDel, 0, -2);
				
				if (!$bIsErr) {
					// usunieto
					Common::CommitTransaction();
					if ($iDeleted > 0) {
						$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
						$this->sMsg = GetMessage($sMsg, false);
						// dodanie informacji do logow
						AddLog($sMsg, false);
					}
				}
				else {
					// blad
					Common::RollbackTransaction();
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
					$this->sMsg = GetMessage($sMsg);
					// dodanie informacji do logow
					AddLog($sMsg);
				}
			}
			else {
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['series_in_use_err']);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowa serie
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId);
			return;
		}

			Common::BeginTransaction();	
			// dodanie serii
			$aValues = array(
				'name' => decodeString($_POST['name']),
				'publisher_id' =>  $iPId, 
				//'description' => trim($_POST['description']) != '' ? trim($_POST['description']) : 'NULL',
				'discount_limit' =>	Common::formatPrice2($_POST['discount_limit']),
				'recount_limit' => isset($_POST['recount_limit']) ? '1' : '0',
				'created' => 'NOW()',
				'created_by' => $_SESSION['user']['name']
			);
			if (($iNewId = Common::Insert($aConfig['tabls']['prefix']."products_series",
												 					  $aValues)) === false) {
				$bIsErr = true;
			}else {
				$aValues2=$aValues;
				unset($aValues2['discount_limit']);
				unset($aValues2['recount_limit']);
				$aValues2['id']=$iNewId;
				$aValues3=array('series_id'=>$iNewId, 'series_import_id'=>$iNewId);
				if ((Common::Insert($aConfig['tabls']['prefix']."products_series_import",
												 					  $aValues2,'', false)) === false)
				$bIsErr = true;								 
				if ((Common::Insert($aConfig['tabls']['prefix']."products_series_mappings",
												 					  $aValues3,'', false)) === false)
				$bIsErr = true;						  
				}
			
	
			if (!$bIsErr) {
				// dodano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
												$_POST['name']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				// reset ustawien widoku
				$_GET['reset'] = 2;
				$this->Show($pSmarty, $iPId);
			}
			else {
				// blad
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
												$_POST['name']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iPId);
			}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych serie wydawnicza
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id serii
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iId);
			return;
		}

			Common::BeginTransaction();
			// aktualizacja
			$aValues = array(
				'name' => decodeString($_POST['name']),
				'publisher_id' => $iPId, 
				//'description' => trim($_POST['description']) != '' ? trim($_POST['description']) : 'NULL',
				'discount_limit' =>	Common::formatPrice2($_POST['discount_limit']),
				'recount_limit' => isset($_POST['recount_limit']) ? '1' : '0',
				'modified' => 'NOW()',
				'modified_by' => $_SESSION['user']['name']
			);
			if (Common::Update($aConfig['tabls']['prefix']."products_series",
												 $aValues,
												 "id = ".$iId) === false) {
				$bIsErr = true;
			}
			
			$aShadows = $this->getShadowsWithSeries($iId);
			if(!empty($aShadows)) {
				foreach($aShadows as $iShadowId) {
					$aValues = array(
						'series' => decodeString($_POST['name'])
					);
					Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues,'id = '.$iShadowId);
				}
			}

	
			if (!$bIsErr) {
				// zaktualizowano
				SetModification('products_series',$iId);
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
												$_POST['name']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				$this->Show($pSmarty, $iPId);
			}
			else {
				// nie zaktualizowano
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
												$_POST['name']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iPId, $iId);
			}
		
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji serii wydawniczej
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanej serii
	 */
	function AddEdit(&$pSmarty, $iPId, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."products_series
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)],$this->getPublisherName($iPId));
		if ($iId > 0 && isset($sParName) && !empty($sParName)) {
			$sHeader .= ' "'.$aData['name'].'"';
		}

		$pForm = new FormTable('products_series', $sHeader, array('action'=>phpSelf(array('pid'=>$iPId,'id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		if ($iId > 0) {
			$pForm->AddHidden('do', 'update');
		}
		else {
			$pForm->AddHidden('do', 'insert');
		}
		//wydawnictwo
		//$pForm->AddSelect('publisher_id',$aConfig['lang'][$this->sModule]['publisher'],array(),$this->getPublishers(),$aData['publisher_id']);

		// tytul
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>255, 'style'=>'width: 350px;'));
		
		// opis
		//$pForm->AddWYSIWYG('description', $aConfig['lang'][$this->sModule]['description'], $aData['description'], array(), '', false);

		$pForm->AddText('discount_limit', $aConfig['lang'][$this->sModule]['discount_limit'], Common::formatPrice($aData['discount_limit']), array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');
		$pForm->AddCheckBox('recount_limit', $aConfig['lang'][$this->sModule]['recount_limit'], array(), '', !empty($aData['recount_limit']), false);
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	


	/**
	 * Metoda sprawdza czy serie o podanych Id sa uzywane w produktach
	 * 
	 * @param	array	$aIds	- Id usuwanych serii
	 * @return	bool
	 */
	function seriesAreInUse($aIds) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(product_id)
						 FROM ".$aConfig['tabls']['prefix']."products_to_series
						 WHERE series_id IN (".implode(',', $aIds).")";
		return intval(Common::GetOne($sSql)) > 0;
	} // end of seriesAreInUse method
	
	function &getPublishers($bFilter=false){
		global $aConfig;
		// pobranie wszystkich serii
			$sSql = "SELECT id as value, name as label
							 FROM ".$aConfig['tabls']['prefix']."products_publishers
							 ORDER BY name";
			$aItems= Common::GetAll($sSql);
			return array_merge(array(
		array('value' => '', 'label' => $bFilter?$aConfig['lang'][$this->sModule]['f_all']:$aConfig['lang']['common']['choose'])
		),$aItems);
	}
	
	/**
	 * Metoda pobiera liste serii wydawniczych do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id serii
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, name
				 		 FROM ".$aConfig['tabls']['prefix']."products_series
					 	 WHERE id IN (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa serie wydawnicza
	 * 
	 * @param	integer	$iId	- Id serii do usuniecia
	 * @return	mixed	
	 */
	function deleteItem2($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_series
						 WHERE id = ".$iId;
		
		return Common::Query($sSql);
	} // end of deleteItem() method
	/**
	 * Metoda usuwa serie wydawnicza
	 * 
	 * @param	integer	$iId	- Id serii do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_series
						 WHERE id = ".$iId;
		if(Common::Query($sSql)===false) return false;
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_series_import
						 WHERE id = ".$iId;
		if(Common::Query($sSql)===false) return false;
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_series_mappings
						 WHERE series_id = ".$iId;
		
		return Common::Query($sSql);
	} // end of deleteItem() method
	
	/**
	 * Metoda pobiera nazwe wydawnictwa
	 * @return	array ref
	 */
	function getPublisherName($iId) {
		global $aConfig;
		$sSql = "SELECT name
				 		 FROM ".$aConfig['tabls']['prefix']."products_publishers
					 	 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getPublisherName() method
	/**
	 * Metoda pobiera nazwe wydawnictwa
	 * @return	array ref
	 */
	function getSeriesName($iId) {
		global $aConfig;
		$sSql = "SELECT name
				 		 FROM ".$aConfig['tabls']['prefix']."products_series
					 	 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getSeriesName() method
	
	function &getShadowsWithSeries($iId) {
	global $aConfig;
		// wybiera grupy kategorii dla tego produktu
	  $sSql = "SELECT id
	  				 FROM ".$aConfig['tabls']['prefix']."products_shadow
	  				 WHERE series_id =".$iId;
	  return Common::GetCol($sSql);
	} // end of getShadowsWithSeries()


	function getMappings($iId) {
		global $aConfig;
		$sSql = "SELECT series_import_id
							FROM ".$aConfig['tabls']['prefix']."products_series_mappings
							WHERE series_id = ".$iId;
		return Common::GetCol($sSql);
	}
	/**
	 * Metoda zwraca listę znalezionych serii
	 * 
	 * @param object $pSmarty
	 * @return void 
	 *
	 */
	function getSeries($sSearch) {
		global $aConfig;
		$aItems=array();
		if(strlen($sSearch)) {
			$sSql = "SELECT id AS value, name AS label, name AS title
								 FROM ".$aConfig['tabls']['prefix']."products_series
								 WHERE `name` LIKE '%$sSearch%'";
			$aItems = Common::GetAll($sSql);
			}
		return $aItems;		 
	}
	
	/**
	 * Metoda pobiera serie z listy id
	 * 
	 * @return	array
	 */
	function &getAuthorsForIds($aIds) {
		global $aConfig;
		$aItems=array();
		if(!empty($aIds)) {
			foreach($aIds as $iKey=>$iId) {
				$sSql = "SELECT id AS value, name AS label
								 FROM ".$aConfig['tabls']['prefix']."products_series_import
								 WHERE id = ".$iId;
				$aItems[$iKey] = Common::GetRow($sSql); 
			}
		}
		return $aItems;
	} // end of getPublishers() method
	
} // end of Module Class
?>