<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - typy plików
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		//$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'opisy_statusow');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'insert': $this->Insert($pSmarty); break;
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iId); break;
						
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych typów plików
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'value',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_vat_stakes
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND value LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_vat_stakes";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_file_types', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT id, value, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."products_vat_stakes
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND value LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('add')
		);

		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane wymiary
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego opisu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
			
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['value'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['value'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowy typ pliku
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}
		// dodanie
		$aValues = array(
			'value' => $_POST['value'],
			'created' => 'NOW()',
			'created_by' => $_SESSION['user']['name']
		);
		if (Common::Insert($aConfig['tabls']['prefix']."products_vat_stakes",
						 					 $aValues,
											 "",
											 false) === false) {
			$bIsErr = true;
		}
				
		if (!$bIsErr) {
			// dodano
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			// nie dodano
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			$this->AddEdit($pSmarty);
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych jezyk
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id opisu
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}
		// aktualizacja
		$aValues = array(
			'value' => $_POST['value'],
		  'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name']
		);
		if (Common::Update($aConfig['tabls']['prefix']."products_vat_stakes",
											 $aValues,
											 "id = ".$iId) === false) {
			$bIsErr = true;
		}
		
		if (!$bIsErr) {
			// akapit zostal zaktualizowany
			SetModification('products_file_types',$iId);
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// akapit nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji typu pliku
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanego opisu statusu
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT value
							 FROM ".$aConfig['tabls']['prefix']."products_vat_stakes
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0) {
			$sHeader .= ' "'.$aData['value'].'"';
		}

		$pForm = new FormTable('products_file_types', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
		
		// opis
		$pForm->AddText('value', $aConfig['lang'][$this->sModule]['name'], $aData['value'], array('maxlength'=>'3', 'style'=>'width: 50px;'), '', 'uinteger');
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda pobiera liste jezykow do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id opisu
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, value
					 		 FROM ".$aConfig['tabls']['prefix']."products_vat_stakes
						 	 WHERE id IN  (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa stawke
	 * 
	 * @param	integer	$iId	- Id opisu statusu do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_vat_stakes
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method
	

} // end of Module Class
?>