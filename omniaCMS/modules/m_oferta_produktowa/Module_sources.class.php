<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - wydawnictwa
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		// $iPId = getModulePageId('m_powiazane_oferta_produktowa', 'serie');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': 						$this->Delete($pSmarty, $iId); break;
			case 'insert': 						$this->Insert($pSmarty); break;
			case 'update': 						$this->Update($pSmarty, $iId); break;
			case 'edit':
			case 'add': 							$this->AddEdit($pSmarty, $iId); break;
			default: 									$this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych wydawnictwa
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true,
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'discount_limit',
				'content'	=> $aConfig['lang'][$this->sModule]['discount_limit'],
				'sortable'	=> true,
				'width'	=> '100'
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang'][$this->sModule]['list_created'],
				'sortable'	=> true,
				'width'	=> '125'
			),
			array(
				'db_field'	=> 'modified',
				'content'	=> $aConfig['lang'][$this->sModule]['list_modified'],
				'sortable'	=> true,
				'width'	=> '125'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '35'
			)
		);

		// pobranie liczby wszystkich serii
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_source_discount
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
									 (isset($_POST['f_limited']) && $_POST['f_limited'] != '' ? ($_POST['f_limited']=='1'?" AND discount_limit > 0":" AND discount_limit = 0") : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_source_discount";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_source_discount', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// stan publikacji
		/*
		$aLimited = array(
			array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_limited']),
			array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_limited'])
		);
		$pView->AddFilter('f_limited', $aConfig['lang'][$this->sModule]['f_limited'], $aLimited, $_POST['f_limited']);
		*/
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich serii
			$sSql = "SELECT id, first_source as name, IF(discount_limit=0,'',discount_limit) AS discount_limit, created, modified
							 FROM ".$aConfig['tabls']['prefix']."products_source_discount
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
										 (isset($_POST['f_limited']) && $_POST['f_limited'] != '' ? ($_POST['f_limited']=='1'?" AND discount_limit > 0":" AND discount_limit = 0") : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('add')
		);
		
		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function

	
	/**
	 * Metoda usuwa wybrane wydawnictwa
	 *  *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanej wydawnictwa
	 * @return 	void
	 */
function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$iDeleted = 0;
		$sDel = '';
		$sFailedToDel = '';

		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			// zabezpieczenie
			// pobranie usuwanych rekordow
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRes = $this->deleteItem((double)$aItem['id'], $aItem['photo'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['name'];
					break;
				}
				elseif ($iRes === 1) {
					$iDeleted++;
					$sDel .= '"'.$aItem['name'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			if (!$bIsErr) {
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton	



	/**
	 * Metoda dodaje do bazy danych nowa wydawnictwa
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig,$pDB;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}

			Common::BeginTransaction();	
			// dodanie serii
			$aValues = array(
				'first_source' => trim($_POST['first_source']),
				'discount_limit' =>	Common::formatPrice2($_POST['discount_limit']),
				'recount_limit' => isset($_POST['recount_limit']) ? '1' : '0',
				'created' => 'NOW()',
				'created_by' => $_SESSION['user']['name']
			);
			
			if (($iNewId = Common::Insert($aConfig['tabls']['prefix']."products_source_discount",
												 					  $aValues)) === false) {
				$bIsErr = true;
			}
			
			if (!$bIsErr) {
				// dodano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
												$_POST['first_source']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				// reset ustawien widoku
				$_GET['reset'] = 2;
				$this->Show($pSmarty);
			}
			else {
				// blad
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
												$_POST['first_source']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty);
			}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych wydawnictwa
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id serii
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}

			Common::BeginTransaction();
			// aktualizacja
			$aValues = array(
				'first_source' => trim($_POST['first_source']),
				'discount_limit' =>	Common::formatPrice2($_POST['discount_limit']),
				'recount_limit' => isset($_POST['recount_limit']) ? '1' : '0',
				'modified' => 'NOW()',
				'modified_by' => $_SESSION['user']['name']
			);
			
			if (Common::Update($aConfig['tabls']['prefix']."products_source_discount",
												 $aValues,
												 "id = ".$iId) === false) {
				$bIsErr = true;
			}
			
			if (!$bIsErr) {
				// zaktualizowano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
												$_POST['first_source']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				$this->Show($pSmarty);
			}
			else {
				// nie zaktualizowano
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
												$_POST['first_source']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iId);
			}
		
	} // end of Update() funciton
				

	/**
	 * Metoda tworzy formularz dodawania / edycji wydawnictwa
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanej serii
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."products_source_discount
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($sParName) && !empty($sParName)) {
			$sHeader .= ' "'.$aData['name'].'"';
		}

		$pForm = new FormTable('products_source_discount', $sHeader, array('action'=>phpSelf(array('id'=>$iId)), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
		if ($iId > 0) {
			$pForm->AddHidden('do', 'update');
		}
		else {
			$pForm->AddHidden('do', 'insert');
		}
		// tytul
		$pForm->AddText('first_source', $aConfig['lang'][$this->sModule]['first_source'], $aData['first_source'], array('maxlength'=>255, 'style'=>'width: 350px;'));
		
				
		// limit rabatu w procentach
		$pForm->AddText('discount_limit', $aConfig['lang'][$this->sModule]['discount_limit'], Common::formatPrice($aData['discount_limit']), array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');
		$pForm->AddCheckBox('recount_limit', $aConfig['lang'][$this->sModule]['recount_limit'], array(), '', !empty($aData['recount_limit']), false);
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'sendMuliselects();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function

	
	/**
	 * Metoda pobiera liste wydawnictw do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id serii
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, first_source AS name
				 		 FROM ".$aConfig['tabls']['prefix']."products_source_discount
					 	 WHERE id IN (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa wydawnictwo
	 * 
	 * @param	integer	$iId	- Id serii do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId, $sPhoto) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_source_discount
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method

	
		/**
	 * Metoda pobiera wydawców
	 * 
	 * @return	array
	 */
	function &getPublishers($sName='') {
		global $aConfig;
		$aItems=array();
		if(!empty($sName)){
			$sSql = "SELECT id value, name AS label, name AS title
							 FROM ".$aConfig['tabls']['prefix']."products_source_discount
							 WHERE name LIKE '%".$sName."%'
							 ORDER BY name";
			$aItems =Common::GetAll($sSql);
		}
		return $aItems;
	} // end of getPublishers() method
	
	function getPublisherName($iId) {
		global $aConfig;
		$sSql = "SELECT name
							FROM ".$aConfig['tabls']['prefix']."products_publishers
							WHERE id = ".$iId;
		return Common::GetOne($sSql);
	}
	
	
	function getPublishersName($aList) {
		global $aConfig;
		$sBuf='';
		
		foreach($aList as $iItem) {
			$sSql = "SELECT name FROM ".$aConfig['tabls']['prefix']."products_publishers WHERE id = ".$iItem;
			$sBuf .= Common::GetOne($sSql).', ';
		}	 
			 
		return substr($sBuf,0,-2);
		
		return Common::GetOne($sSql);
	}
} // end of Module Class
?>