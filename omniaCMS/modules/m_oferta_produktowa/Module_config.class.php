<?php
/**
 * Klasa Module do obslugi domyslnej konfiguracji stron modulu 'Oferta produktowa'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright Omnia Marcin Korecki
 * @version   1.0
 *
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// sciezka do katalogu zawierajacego szablony czesci klienckiej modulu
	var $sTemplatesDir;

	// lista szablonow czesci klienckiej modulu
	var $sTemplatesList;

	// plik XML z domyslna konfiguracja
	var $sXMLFile;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->sModule = $_GET['module'];
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/modules/'.$this->sModule;
		$this->sTemplatesList =& GetTemplatesList($this->sTemplatesDir);
		$this->sXMLFile = 'config/'.$_GET['module'].'/default_config.xml';

		$sDo = '';
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu i akcji
		if ($_SESSION['user']['type'] !== 1) {
			// brak uprawnien do modulu, menu, strony
			showPrivsAlert($pSmarty);
			return;
    }

    switch ($sDo) {
			case 'update': $this->Update($pSmarty); break;
			default: $this->Edit($pSmarty); break;
		}
	} // end Module() function


	/**
	 * Metoda wprowadza zmiany w aktualnosci do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Edit($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Edit($pSmarty);
			return;
		}
		
	$aValues = array();
		$iOPId = getModuleId('m_oferta_produktowa');
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products_config
						 WHERE id = ".$iOPId;
		$aSettings =& Common::GetRow($sSql);
		
		$aValues = array(
			'thumb_size' => $_POST['thumb_size'],
			'small_size' => $_POST['small_size'],
			'big_size' => $_POST['big_size'],
			'list_template' => $_POST['list_template'],
			'item_template' => $_POST['item_template'],
			'items_per_page' => (int)$_POST['items_per_page']
		);

		if (!empty($aSettings)) {
			// Update
			$bIsErr = ! Common::Update($aConfig['tabls']['prefix']."products_config",
														$aValues,
														"id = ".$iOPId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('id' => $iOPId), $aValues);
			$bIsErr = ! Common::Insert($aConfig['tabls']['prefix']."products_config",
														$aValues,
														"",
														false) !== false;
		}

		$bIsErr = !writeModuleConfig($this->sXMLFile);

		if ($bIsErr) {
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = $aConfig['lang'][$this->sModule]['edit_err'];
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->Edit($pSmarty);
		}
		else {
			$sMsg = $aConfig['lang'][$this->sModule]['edit_ok'];
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Edit($pSmarty);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz edycji domyslnej konfiguracji stron modulu
	 *
	 * @param		object	$pSmarty
	 */
	function Edit(&$pSmarty) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');


		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			// pobranie konfiguracji z pliku XML
			$aData =& getModuleConfig($this->sXMLFile);
		}

		$pForm = new FormTable('config',
													 $aConfig['lang'][$this->sModule]['header'],
													 array('action'=>phpSelf()),
													 array('col_width'=>225),
													 $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');

		$aLang =& $aConfig['lang'][$_GET['module'].'_settings'];

		// szablon listy, aktualnosci
		$pForm->AddMergedRow($aLang['templates'], array('class'=>'mergedLight', 'style'=>'padding-left: 240px'));
		$pForm->AddSelect('list_template', $aLang['list_template'], array(), $this->sTemplatesList, $aData['list_template'], '', false);
		$pForm->AddSelect('item_template', $aLang['item_template'], array(), $this->sTemplatesList, $aData['item_template'], '', false);

		// liczba produktow na stronie
		$pForm->AddText('items_per_page', $aLang['items_per_page'], $aData['items_per_page'], array('maxlength'=>3, 'style'=>'width: 30px;'), '', 'uinteger');

		// rozmiary zdjec
		$pForm->AddMergedRow($aLang['photos'], array('class'=>'mergedLight', 'style'=>'padding-left: 240px'));
		$pForm->AddText('thumb_size', $aLang['thumb_size'], $aData['thumb_size'], array('maxlength'=>7, 'style'=>'width: 60px;'), '', 'text', true, '/^[0-9]{2,3}x[0-9]{2,3}$/');
		$pForm->AddText('small_size', $aLang['small_size'], $aData['small_size'], array('maxlength'=>7, 'style'=>'width: 60px;'), '', 'text', true, '/^[0-9]{2,3}x[0-9]{2,3}$/');
	//	$pForm->AddText('medium_size', $aLang['medium_size'], $aData['medium_size'], array('maxlength'=>7, 'style'=>'width: 60px;'), '', 'text', true, '/^[0-9]{2,3}x[0-9]{2,3}$/');
		$pForm->AddText('big_size', $aLang['big_size'], $aData['big_size'], array('maxlength'=>9, 'style'=>'width: 80px;'), '', 'text', true, '/^[0-9]{2,4}x[0-9]{2,4}$/');

		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Edit() function
} // end of Module Class
?>