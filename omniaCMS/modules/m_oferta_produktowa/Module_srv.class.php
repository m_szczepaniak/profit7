<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa - produkty'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

use orders\Shipment\ShipmentTime;

class Module extends Module_Common {

	// komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// ustawienia strony
	var $aSettings;

	// domyslna liczba pol na rodukty przy tworzeniu pakietu
	var $iDefaultProductsNo;

	/**
	 * Konstruktor klasy
	 *
	 * @return	void
	 */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		$this->iDefaultProductsNo = 3;

		$sDo = '';
		$iId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}

		$this->sDir = $this->iLangId.'/'.$iPId.'/'.$iId;


		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
		if ($_SESSION['user']['type'] === 0) {
			if (!hasModulePrivileges($_GET['module_id'])){
				showPrivsAlert($pSmarty);
				return;
			}
		}

		// pobranie ustawien modulu
		$this->aSettings =& $this->getGlobalSettings();
		
		if (isset($_POST['save_as'])) {
			$sDo = 'insert';
		}
//dump($_POST);die();
		switch ($sDo) {
			case 'delete_all':
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'insert': $this->Insert($pSmarty); break;
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'publish_all':
			case 'publish': $this->Publish($pSmarty, $iId); break;
			case 'review_all':
			case 'review': $this->Review($pSmarty, $iId); break;
			case 'tag_all':  $this->TagAllForm($pSmarty); break;
			case 'change_status':  $this->ChangeStatusForm($pSmarty); break;
			case 'update_status': $this->UpdateStatus($pSmarty); break;
			case 'update_tags': $this->UpdateTags($pSmarty); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iId); break;
			case 'images': $this->AddEditImages($pSmarty, $iPId, $iId); break;
			case 'add_photo': $this->SaveAndAddImage($pSmarty, $iPId, $iId); break;
			case 'update_images': $this->UpdateImages($pSmarty, $iPId, $iId); break;
			case 'edit_packet':
			case 'add_packet':
			case 'add_product':
			case 'add_product_field':
			case 'change_packet_category': 
				$this->AddEditPacket($pSmarty, $iId, $sDo == 'add_product_field');
			break;
			case 'insert_packet': $this->InsertPacket($pSmarty); break;
			case 'update_packet': $this->UpdatePacket($pSmarty, $iId); break;
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method

	/**
	 * Metoda pobiera ustawienia dla modulu
	 */
	function &getGlobalSettings() {
		global $aConfig;
		// pobranie konfiguracji dla modulu
		$sSql = "SELECT thumb_size, small_size, big_size
						 FROM ".$aConfig['tabls']['prefix']."products_config 
						 WHERE id = ".getModuleId('m_oferta_produktowa');
		return Common::GetRow($sSql);
	} // end of setSettings() method
	
	/**
	 * Metoda wyswietla liste produktow strony o id $iPId oferty produktowej
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
		array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
		),
		array(
				'content'	=> 'Pak.',
				'sortable'	=> false,
				'width'	=> '50'
			),
		array(
				'db_field'	=> 'A.name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
		),
		array(
				'db_field'	=> 'A.vat',
				'content'	=> $aConfig['lang'][$this->sModule]['list_vat'],
				'sortable'	=> true,
				'width'	=> '30'
		),
		array(
				'db_field'	=> 'A.price_brutto',
				'content'	=> $aConfig['lang'][$this->sModule]['list_base'],
				'sortable'	=> true,
				'width'	=> '80'
		),
		array(
				'db_field'	=> 'price',
				'content'	=> $aConfig['lang'][$this->sModule]['list_price'],
				'sortable'	=> false,
				'width'	=> '80'
		),
		array(
				'db_field'	=> 'A.published',
				'content'	=> $aConfig['lang'][$this->sModule]['list_published'],
				'sortable'	=> true,
				'width'	=> '35'
		),
		array(
				'db_field'	=> 'source',
				'content'	=> $aConfig['lang'][$this->sModule]['list_source'],
				'sortable'	=> true,
				'width'	=> '120'
		),
		array(
				'db_field'	=> 'A.created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '140'
		),
		array(
				'db_field'	=> 'A.modified',
				'content'	=> $aConfig['lang'][$this->sModule]['list_modified'],
				'sortable'	=> false,
				'width'	=> '140'
		),
		array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '180'
		)
				);
				$sSearch='';
				if(isset($_POST['search']) && !empty($_POST['search'])){
					$_POST['search']=trim($_POST['search']);
		//			$sSearch = preg_replace('/\s+/','%',$_POST['search'])		
					if(is_numeric($_POST['search']))	{
						$sSearch =  ' AND ( A.id = '.(int) $_POST['search'] .' OR A.isbn_plain LIKE \''.isbn2plain($_POST['search']).'\')';
					}
					elseif(preg_match('/[0-9]/',$_POST['search'])) {
						$sSearch = ' AND (A.name LIKE \'%'.preg_replace('/\s+/','%',$_POST['search']).'%\' OR A.isbn_plain LIKE \''.isbn2plain($_POST['search']).'\' OR A.name2 LIKE \'%'.preg_replace('/\s+/','%',$_POST['search']).'%\')';
					}
					else {
						$sSearch =  ' AND (A.name LIKE \'%'.preg_replace('/\s+/','%',$_POST['search']).'%\' OR A.name2 LIKE \'%'.preg_replace('/\s+/','%',$_POST['search']).'%\')';
					}
				}
//dump($_SESSION['_modified']);
				// pobranie liczby wszystkich rekordow
				$sSql = "SELECT COUNT(DISTINCT A.id)
						 FROM ".$aConfig['tabls']['prefix']."products A ".
						 	 (isset($_POST['f_category']) && $_POST['f_category'] != '' ?
							 	 " LEFT JOIN ".$aConfig['tabls']['prefix']."products_extra_categories C
							 	 ON A.id = C.product_id" : "").
							 (isset($_POST['f_author']) && $_POST['f_author'] != '' ?
								" LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_authors D
							 	 ON A.id = D.product_id
							 	 LEFT JOIN ".$aConfig['tabls']['prefix']."products_authors E
							 	 ON E.id = D.author_id" : "").
							 	 (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ?
								" LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers F
							 	 ON A.publisher_id = F.id" : "").
							 	 (isset($_POST['f_image'])?" LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id ":'').
							 	 (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ?
							 " LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series G
							 	 ON G.product_id = A.id" : '')."
						 WHERE 1=1".
							 	 ($sSearch != ''?$sSearch:'').
				(isset($_POST['f_category']) && $_POST['f_category'] != '' ? ' AND (A.page_id = '.$_POST['f_category'].' OR C.page_id = '.$_POST['f_category'].')' : '').
				(isset($_POST['f_published']) && $_POST['f_published'] != '' ? ' AND published = \''.$_POST['f_published'].'\'' : '').
				(isset($_POST['f_language']) && $_POST['f_language'] != '' ? ' AND A.language = '.$_POST['f_language'] : '').
					(isset($_POST['f_orig_language']) && $_POST['f_orig_language'] != '' ? ' AND A.original_language = '.$_POST['f_orig_language'] : '').
					(isset($_POST['f_binding']) && $_POST['f_binding'] != '' ? ' AND A.binding = '.$_POST['f_binding'] : '').
					(isset($_POST['f_year']) && $_POST['f_year'] != '' ? ' AND A.publication_year = '.$_POST['f_year'] : '').
				//	(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] != '' ? ' AND A.reviewed = \''.$_POST['f_reviewed'].'\'' : '').
					(isset($_POST['f_type']) && $_POST['f_type'] != '' ?' AND A.packet = \''.$_POST['f_type'].'\'': '').
					(isset($_POST['f_image']) && $_POST['f_image'] != '' ? ($_POST['f_image']=='0'?' AND I.id IS NULL':' AND I.id IS NOT NULL') : '').
					(isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ? ' AND F.name = \''.$_POST['f_publisher'].'\'' : '').
					(isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != ''? ' AND G.series_id = '.$_POST['f_series'].'' : '').
					(isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = \''.$_POST['f_source'].'\'' : '').
					(isset($_POST['f_shipment']) && $_POST['f_shipment'] != '' ? ' AND A.shipment_time = \''.$_POST['f_shipment'].'\'' : '').
					(isset($_POST['f_status']) && $_POST['f_status'] != '' ? ' AND A.prod_status = \''.$_POST['f_status'].'\'' : '').
						(isset($_POST['f_author']) && $_POST['f_author'] != '' ? ' AND CONCAT(E.name,\' \',E.surname,\' \',E.name) LIKE \'%'.$_POST['f_author'].'%\'' : '');
				$iRowCount = intval(Common::GetOne($sSql));

				if ($iRowCount == 0 && !isset($_GET['reset'])) {
					// resetowanie widoku
					resetViewState($this->sModule);
					// ponowne okreslenie liczny rekordow
					$sSql = "SELECT COUNT(id)
						 	 FROM ".$aConfig['tabls']['prefix']."products";
					$iRowCount = intval(Common::GetOne($sSql));
				
				}

				$pView = new View('products', $aHeader, $aAttribs);
				$pView->AddRecordsHeader($aRecordsHeader);

				// FILTRY
				// kategoria Oferty produktowej
				$pView->AddFilter('f_category', $aConfig['lang'][$this->sModule]['f_category'], $this->getProductsCategories(Common::getProductsMenu($this->iLangId), $aConfig['lang'][$this->sModule]['f_all']), $_POST['f_category']);
				$pView->AddFilter('f_category_btn', '', '<input type="button" name="nvtree" value="'.$aConfig['lang'][$this->sModule]['f_category_btn'].'" id="show_navitree" \>&nbsp;', '', 'html');
				
				
				// stan publikacji
				$aPublished = array(
				array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
				array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_published']),
				array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_published'])
				);
				$pView->AddFilter('f_published', $aConfig['lang'][$this->sModule]['f_publ_status'], $aPublished, $_POST['f_published']);

				// czy obrobiona
//				$aReviewed = array(
//				array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
//				array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_reviewed']),
//				array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_reviewed'])
//				);
//				$pView->AddFilter('f_reviewed', $aConfig['lang'][$this->sModule]['f_reviewed'], $aReviewed, $_POST['f_reviewed']);
//				
					// czy obrobiona
				$aSources = array(
				array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
				array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_source_internalj']),
				array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_source_internaln']),
				array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_source_azymut']),
				array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['f_source_abe']),
				array('value' => '4', 'label' => $aConfig['lang'][$this->sModule]['f_source_helion'])
				);
				$pView->AddFilter('f_source', $aConfig['lang'][$this->sModule]['f_source'], $aSources, $_POST['f_source']);
				
				
				
				
				// z okladka / bez okladki
				
				// typ produktu
				$aTypes = array(
					array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
					array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_packet']),
					array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_regular_product'])
				);
				$pView->AddFilter('f_type', $aConfig['lang'][$this->sModule]['f_type'], $aTypes, $_POST['f_type']);
				$pView->AddFilter('f_year', $aConfig['lang'][$this->sModule]['f_year'], $this->getYears(), $_POST['f_year']);
				
				$pView->AddFilter('a', '', '</td></tr><tr><td>', '', 'html');
				$pView->AddFilter('f_language', $aConfig['lang'][$this->sModule]['f_language'], $this->getLanguages(), $_POST['f_language']);
				$pView->AddFilter('f_orig_language', $aConfig['lang'][$this->sModule]['f_orig_language'], $this->getLanguages(), $_POST['f_orig_language']);
				$pView->AddFilter('f_binding', $aConfig['lang'][$this->sModule]['f_binding'], $this->getBindings(), $_POST['f_binding']);
				
				$aImagesF = array(
				array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
				array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_image'])
				);
				$pView->AddFilter('f_image', $aConfig['lang'][$this->sModule]['f_image'], $aImagesF, $_POST['f_image']);
				
				$pView->AddFilter('b', '', '</td></tr><tr><td>', '', 'html');
                $pShipmentTime = new ShipmentTime();

                $aShipments = array(
                    array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
                    array('value' => '0', 'label' => $pShipmentTime->getShipmentTime(null, 0)),
                    array('value' => '1', 'label' => $pShipmentTime->getShipmentTime(null, 1)),
                    array('value' => '2', 'label' => $pShipmentTime->getShipmentTime(null, 2)),
                    array('value' => '3', 'label' => $pShipmentTime->getShipmentTime(null, 3)),
                    array('value' => '4', 'label' => $pShipmentTime->getShipmentTime(null, 4)),
                    array('value' => '5', 'label' => $pShipmentTime->getShipmentTime(null, 5)),
                    array('value' => '6', 'label' => $pShipmentTime->getShipmentTime(null, 6)),
                    array('value' => '7', 'label' => $pShipmentTime->getShipmentTime(null, 7)),
                    array('value' => '8', 'label' => $pShipmentTime->getShipmentTime(null, 8)),
                    array('value' => '9', 'label' => $pShipmentTime->getShipmentTime(null, 9)),
                    array('value' => '10', 'label' => $pShipmentTime->getShipmentTime(null, 10)),
                );
				$pView->AddFilter('f_shipment', $aConfig['lang'][$this->sModule]['f_shipment'], $aShipments, $_POST['f_shipment']);
				$aStatuses = array(
					array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
					array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1']),
					array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['status_0']),
					array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['status_2']),
					array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['status_3'])
				);
				$pView->AddFilter('f_status', $aConfig['lang'][$this->sModule]['f_status'], $aStatuses, $_POST['f_status']);
				
				
				
				$pView->AddFilter('f_author', $aConfig['lang'][$this->sModule]['f_author'], array(), $_POST['f_author'], 'text');
				$pView->AddFilter('f_publisher', $aConfig['lang'][$this->sModule]['f_publisher'], array(), $_POST['f_publisher'], 'text');
				
				$pView->AddFilter('f_series', $aConfig['lang'][$this->sModule]['f_series'], $this->getPublisherSeriesList($_POST['f_publisher']), $_POST['f_series']);

				if ($iRowCount > 0) {
					// dodanie Pagera do widoku
					$iCurrentPage = $pView->AddPager($iRowCount);
					$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
					$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
						
					// pobranie wszystkich rekordow
					$sSql = "SELECT A.id, A.page_id, A.packet, A.name, A.vat, A.price_brutto,
							0	AS price, F.name AS publisher, A.publication_year, 
							 A.published, A.source, A.created, A.created_by, A.modified, A.modified_by
						 	 FROM ".$aConfig['tabls']['prefix']."products A ".
						 	 (isset($_POST['f_category']) && $_POST['f_category'] != '' ?
							 	 " LEFT JOIN ".$aConfig['tabls']['prefix']."products_extra_categories C
							 	 ON A.id = C.product_id" : "").
							 (isset($_POST['f_author']) && $_POST['f_author'] != '' ?
								" LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_authors D
							 	 ON A.id = D.product_id
							 	 LEFT JOIN ".$aConfig['tabls']['prefix']."products_authors E
							 	 ON E.id = D.author_id" : "").
							 (isset($_POST['f_image'])?" LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id ":'').
							 (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ?
							 " LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series G
							 	 ON G.product_id = A.id" : '').
								" LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers F
							 	 ON A.publisher_id = F.id"."
						 	 WHERE 1=1".
							 ($sSearch != ''?$sSearch:'').
					(isset($_POST['f_category']) && $_POST['f_category'] != '' ? ' AND (A.page_id = '.$_POST['f_category'].' OR C.page_id = '.$_POST['f_category'].')' : '').
					(isset($_POST['f_published']) && $_POST['f_published'] != '' ? ' AND A.published = \''.$_POST['f_published'].'\'' : '').
					(isset($_POST['f_language']) && $_POST['f_language'] != '' ? ' AND A.language = '.$_POST['f_language'] : '').
					(isset($_POST['f_orig_language']) && $_POST['f_orig_language'] != '' ? ' AND A.original_language = '.$_POST['f_orig_language'] : '').
					(isset($_POST['f_binding']) && $_POST['f_binding'] != '' ? ' AND A.binding = '.$_POST['f_binding'] : '').
					(isset($_POST['f_year']) && $_POST['f_year'] != '' ? ' AND A.publication_year = '.$_POST['f_year'] : '').
					//(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] != '' ? ' AND A.reviewed = \''.$_POST['f_reviewed'].'\'' : '').
					(isset($_POST['f_image']) && $_POST['f_image'] != '' ? ($_POST['f_image']=='0'?' AND I.id IS NULL':' AND I.id IS NOT NULL') : '').
					(isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = \''.$_POST['f_source'].'\'' : '').
					(isset($_POST['f_shipment']) && $_POST['f_shipment'] != '' ? ' AND A.shipment_time = \''.$_POST['f_shipment'].'\'' : '').
					(isset($_POST['f_status']) && $_POST['f_status'] != '' ? ' AND A.prod_status = \''.$_POST['f_status'].'\'' : '').
					(isset($_POST['f_author']) && $_POST['f_author'] != '' ? ' AND CONCAT(E.name,\' \',E.surname,\' \',E.name) LIKE \'%'.$_POST['f_author'].'%\'' : '').
					(isset($_POST['f_type']) && $_POST['f_type'] != '' ?' AND A.packet = \''.$_POST['f_type'].'\'': '').
					(isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ? ' AND F.name = \''.$_POST['f_publisher'].'\'' : '').
					(isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != ''? ' AND G.series_id = '.$_POST['f_series'].'' : '').
							 ' GROUP BY A.id ORDER BY '.((isset($_GET['sort']) && !empty($_GET['sort']) && $_GET['sort'] != 'A.created') ? $_GET['sort'] : 'A.id').
					(isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : ' DESC').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
					$aRecords =& Common::GetAll($sSql);
//dump($sSql);
					foreach ($aRecords as $iKey => $aItem) {
						// stan publikacji
						$aRecords[$iKey]['published'] = $aConfig['lang']['common'][$aItem['published'] == '1' ? 'yes' : 'no'];

						// link podgladu
						$aRecords[$iKey]['action']['links']['preview'] = (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'].'/' : '').(strlen($aItem['name'])>70?substr(link_encode($aItem['name']),0,70):link_encode($aItem['name'])).',product'.$aItem['id'].'.html?preview='.md5($_SERVER['REMOTE_ADDR'].$aConfig['common']['client_base_url_http']);

						$aRecords[$iKey]['created'] .= '<br />'.$aItem['created_by'];
						unset($aRecords[$iKey]['created_by']);
						
						$aRecords[$iKey]['modified'] .= '<br />'.$aItem['modified_by'];
						unset($aRecords[$iKey]['modified_by']);
						
						$aRecords[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($aItem['publisher'],0,40,'UTF-8').'</span>';
						unset($aRecords[$iKey]['publisher']);
						
						$aRecords[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aItem['id']),0,25,'UTF-8').' '.$aItem['publication_year'].'</span>';
						unset($aRecords[$iKey]['publication_year']);
						
						$aRecords[$iKey]['source'] = $aConfig['lang'][$this->sModule]['source_'.$aItem['source']];
						
						// formatowanie ceny brutto
						$aTarrif = $this->getTarrif($aItem['id']);
						$aRecords[$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']);
						$aRecords[$iKey]['price'] = Common::formatPrice($aTarrif['price_brutto']);
//						if(!isset($_POST['f_reviewed']) || $_POST['f_reviewed'] != '0'){
//								$aRecords[$iKey]['disabled'][] = 'review';
//						}
						unset($aRecords[$iKey]['symbol']);
						
						if ($aItem['packet'] == '1') {
							// wylaczona mozliwosc dodawania plikow i zalacznikow
							$aRecords[$iKey]['disabled'][] = 'tarrifs';

							$aRecords[$iKey]['action']['links']['edit'] = phpSelf(array('do' => 'edit_packet', 'id' => $aItem['id']));
							$aRecords[$iKey]['action']['links']['name'] = phpSelf(array('do' => 'edit_packet', 'id' => $aItem['id']));
							$aRecords[$iKey]['packet'] = '[ '.$aConfig['lang']['common']['yes'].' ]';
						} else {
							$aRecords[$iKey]['packet'] = '&nbsp;';
						}
					}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
					),
				'page_id'	=> array(
					'show'	=> false
					),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
					),
				'action' => array (
					'actions'	=> array ('authors','linked','tarrifs','editions','images', 'publish','preview', 'edit', 'delete'),
					'params' => array (
												'authors'	=> array('action' => 'authors', 'pid' => '{page_id}', 'ppid' => '{id}'),
												'linked'	=> array('action' => 'linked', 'pid' => '{page_id}', 'ppid' => '{id}'),
												'tarrifs'	=> array('action' => 'tarrifs', 'pid' => '{id}'),
												'editions'	=> array('action' => 'editions', 'pid' => '{page_id}', 'ppid' => '{id}'),
												'review'	=> array('pid' => '{page_id}', 'id' => '{id}'),
												'images'	=> array('pid' => '{page_id}', 'id' => '{id}'),
												'publish'	=> array('pid' => '{page_id}', 'id' => '{id}'),
												'edit'	=> array('pid' => '{page_id}', 'id'=>'{id}'),
												'delete'	=> array('pid' => '{page_id}', 'id'=>'{id}')
					),
					'show' => false
					)
					);
//					if(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] == '0'){
//						$aColSettings['action']['actions'][]= 'review';
//					}
					// dodanie rekordow do widoku
					$pView->AddRecords($aRecords, $aColSettings);
				}
				// przyciski stopki stopki do widoku
			//	if(!isset($_POST['f_reviewed']) || $_POST['f_reviewed'] != '0'){
					$aRecordsFooter = array(
						array('check_all'),
						array('add_packet', 'add')
					);
//				} else {
//					$aRecordsFooter = array(
//						array('check_all', 'delete_all', 'publish_all', 'review_all'),
//						array('add_packet', 'add')
//					);
//				}
				$aRecordsFooterParams = array(
				);
				$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

				$aFooterGroup = array('tag_all', 'publish_all',	'delete_all', 'change_status');
//				if(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] == '0'){
//					$aFooterGroup[]='review_all';
//				}
				$pView->AddFooterGroupActions($aFooterGroup);
				$sJS='
					<script>
					
						$(document).ready(function(){
							$("#show_navitree").click(function() {
								window.open("ajax/NaviPopup.php",\'\',\'menubar=0,location=0,scrollbars=1,toolbar=0,width=500,height=300\');
							});
							
							$("#f_publisher").autocomplete("ajax/GetPublishers.php",{ minChars: 2, max: 100, matchContains: true, cacheLength: 1, matchSubset: false  });
							$("#f_publisher").bind("blur", function(e){
						   	 var elSelSeries = document.getElementById("f_series");
						   	 if(e.target.value != "") {  
				     	    $.get("ajax/GetSeries.php", { publisher: e.target.value },
									  function(data){
									    var brokenstring=data.split(";");
								    elSelSeries.length = 0;
									    elSelSeries.options[elSelSeries.length] =  new Option("'.$aConfig['lang']['common']['choose'].'","");
									    for (x in brokenstring){
												if(brokenstring[x]){
													var item=brokenstring[x].split("|");
						  						elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
					  						}
					  				}
									 });
						 			} else { elSelSeries.length = 0; elSelSeries.options[elSelSeries.length] =  new Option("'.$aConfig['lang']['common']['choose'].'",""); }
	    					});
						});
					</script>';
			
			$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane rekordy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego rekordu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;

		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = getPagePath((double) $aItem['page_id'], $this->iLangId, true).' / '.$aItem['name'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.getPagePath((double) $aItem['page_id'], $this->iLangId, true).' / '.$aItem['name'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
				
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowy produkt
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony oferty produktowej
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}
		
		if($this->existBookWithISBN(isbn2plain($_POST['isbn']))){
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['isbn_err'], true);
			// istnieje juz ksiazka z ppodanym isbnem
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}
		
		// sprawdzenie tagow
		if (trim($_POST['tags']) != '' && !$this->checkTags($_POST['tags'])) { 
			// nieprawidlowo wypelnione pole tagi
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
			$this->AddEdit($pSmarty);
			return;
		}
		
		if(isset($_POST['published']) && $_POST['page_id'] == $aConfig['import']['unsorted_category']) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_unsorted_err'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEdit($pSmarty);
			return;
		}
		
		Common::BeginTransaction();

		$aValues = array(
			'page_id' => (double) $_POST['page_id'],
			'name' => $_POST['name'],
			'name2' => !empty($_POST['name2'])?$_POST['name2']:'NULL',
			'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($_POST['name'], ' '))),
			'short_description' => strip_tags(br2nl(trimString($_POST['description'],200),' ')),
			'description' => $_POST['description'],
			'price_netto' => Common::formatPrice2($_POST['price_netto']),
			'price_brutto' => (Common::formatPrice2($_POST['price_netto'])*(1+$_POST['vat']/100)),
			'vat' => $_POST['vat'],
			'shipment_time' => isset($_POST['shipment_time'])?$_POST['shipment_time']:'1',
			'weight' => ($_POST['weight']>0)?(float)$_POST['weight']:'NULL',
			'isbn' => $_POST['isbn'],
			'isbn_plain' => isbn2plain($_POST['isbn']),
			'publication_year' => !empty($_POST['publication_year'])?(int)$_POST['publication_year']:'NULL',
			'pages' => !empty($_POST['pages'])?(int)$_POST['pages']:'NULL',
			'binding' => !empty($_POST['binding'])?$_POST['binding']:'NULL',
			'dimension' => !empty($_POST['dimension'])?$_POST['dimension']:'NULL',
			'language' => !empty($_POST['language'])?$_POST['language']:'NULL',
			'translator' => $_POST['translator'],
			'volumes' => !empty($_POST['volumes'])?(int)$_POST['volumes']:'NULL',
			'volume_nr' => !empty($_POST['volume_nr'])?(int)$_POST['volume_nr']:'NULL',
			'edition' => !empty($_POST['edition'])?(int)$_POST['edition']:'NULL',
			'volume_name' => !empty($_POST['volume_name'])?$_POST['volume_name']:'NULL',
			'city' => !empty($_POST['city'])?$_POST['city']:'NULL',
			'original_language' => !empty($_POST['original_language'])?$_POST['original_language']:'NULL',
			'original_title' => !empty($_POST['original_title'])?$_POST['original_title']:'NULL',
			'prod_status' => isset($_POST['prod_status'])?$_POST['prod_status']:'0',
			'published' => isset($_POST['published']) ? '1' : '0',
			'legal_status_date'	=>	(!empty($_POST['legal_status_date'])&&($_POST['legal_status_date']!='00-00-0000'))?FormatDate($_POST['legal_status_date']):'NULL',
			'shipment_date'	=>	($_POST['prod_status']=='3' && !empty($_POST['shipment_date'])&&($_POST['shipment_date']!='00-00-0000'))?FormatDate($_POST['shipment_date']):'NULL',
			'created' => 'NOW()',
			'source' => '0',
			'created_by' => $_SESSION['user']['name']
		);
		$iPublished = $aValues['published'];
		if(!empty($_POST['attachment_type'])){
			$aValues['price_brutto']+=(Common::formatPrice2($_POST['attachment_price_netto'])*(1+$_POST['attachment_vat']/100));
		}
		if($_POST['prod_status']=='3'){
			$aValues['shipment_time'] = '0';
		}
		// dodawanie wydawcy
		$sSql = "SELECT id
					 FROM ".$aConfig['tabls']['prefix']."products_publishers 
					 WHERE name LIKE '".$_POST['publisher']."'"; 
		$iPublisher = (int)Common::GetOne($sSql);
		if($iPublisher > 0){
			$aValues['publisher_id']=$iPublisher;
		} elseif(!empty($_POST['publisher'])){
			// dodajemy nowego wydawcę
			
			$aPValues = array(
				'index_letter' => strtoupper(mb_substr($_POST['publisher'], 0, 1, 'utf8')),
				'name' => decodeString($_POST['publisher']),
				'created' => 'NOW()',
				'created_by' => $_SESSION['user']['name']
			);
			if (($iNewId = Common::Insert($aConfig['tabls']['prefix']."products_publishers",
												 					  $aPValues)) !== false) {
				$aValues['publisher_id']=$iNewId;
			} else {
				$bIsErr = true;
			}
			if(!$bIsErr){
				$sSql="SELECT id FROM ".$aConfig['tabls']['prefix']."products_publishers_import
					 WHERE name = ".Common::Quote(stripslashes($_POST['publisher']));
				$iIId=Common::GetOne($sSql);
				// wydawnictwo istnieje w tabeli ukrytej
				if($iIId == false) {
					$aPIValues = array(
						'name' => decodeString($_POST['publisher'])
					);
					if (($iIId = Common::Insert($aConfig['tabls']['prefix']."products_publishers_import", $aPIValues)) === false) {
						$bIsErr = true;
					}
				}
				if(!$bIsErr){
					$aPMValues = array(
						'publisher_id' => $iNewId,
						'publisher_import_id' => $iIId
					);
					if (Common::Insert($aConfig['tabls']['prefix']."products_publishers_mappings",$aPMValues,'',false) === false) {
							$bIsErr = true;
					}
				}
			}
		}
//dump($aValues);
		// dodanie
		if (($iId = Common::Insert($aConfig['tabls']['prefix']."products",
		$aValues)) === false) {
			$bIsErr = true;
		}
		
			// dodanie serii wydawniczych
		if(empty($_POST['series']) && !empty($_POST['series_name']) && !empty($aValues['publisher_id'])){
			$aValues = array(
				'name' => decodeString($_POST['series_name']),
				'publisher_id' => (int)$aValues['publisher_id'], 
				'created' => 'NOW()',
				'created_by' => $_SESSION['user']['name']
			);
			if (($iNewId = Common::Insert($aConfig['tabls']['prefix']."products_series",
												 					  $aValues)) !== false) {
				$_POST['series']=$iNewId;
			}
		}
		
		if(!empty($_POST['series'])){
				$aValues=array(
					'product_id' => $iId,
					'series_id' => $_POST['series']
				);
				if ((Common::Insert($aConfig['tabls']['prefix']."products_to_series",$aValues,'',false)) === false) {
					$bIsErr = true;
				}
		}
		
		if(!empty($_POST['attachment_type'])){
				$aValues=array(
					'attachment_type' => $_POST['attachment_type'],
					'product_id' => $iId,
					'price_netto' => Common::formatPrice2($_POST['attachment_price_netto']),
					'price_brutto' => (Common::formatPrice2($_POST['attachment_price_netto'])*(1+$_POST['attachment_vat']/100)),
					'vat' => (int)$_POST['attachment_vat']
				);
				if ((Common::Insert($aConfig['tabls']['prefix']."products_attachments",$aValues,'',false)) === false) {
					$bIsErr = true;
				}
		}
		
		$_POST['extra_categories'][]=$_POST['page_id'];
		// dodanie dodatkopwych kategorii
		if(!$bIsErr && !empty($_POST['extra_categories'])){
			foreach($_POST['extra_categories'] as $iCatId){
				if($iCatId != $aConfig['import']['unsorted_category'] && !$this->existExtraCategory($iCatId,$iId)){
					$aValues=array(
						'product_id' => $iId,
						'page_id' => $iCatId
					);
					if ((Common::Insert($aConfig['tabls']['prefix']."products_extra_categories",$aValues,'',false)) === false) {
						$bIsErr = true;
					}
					// pobranie kategorii - rodzicow wybranej kategorii
					$aParents=$this->getPathIDs($iCatId,$_SESSION['lang']['id']);
					// dodanie mapowan w gore drzewa lokalnego
					if(!empty($aParents)){
						foreach($aParents as $iParent){
							if(!$this->existExtraCategory($iParent,$iId)){
								$aValues=array(
									'product_id' => $iId,
									'page_id' => $iParent
								);
								if ((Common::Insert($aConfig['tabls']['prefix']."products_extra_categories",$aValues,'',false)) === false) {
									$bIsErr = true;
								}
							}
						}
					}
				}
			}
		}
		
		if (!$bIsErr) {
			if (trim($_POST['tags']) != '') {
				// dodanie tagow
				$bIsErr = !$this->addTags($iId, explode(',', $_POST['tags']));
			}
		}
		
		if($iPublished == '1') {
			if($this->addShadow($iId)==false){
				$bIsErr=true;
			}
		}
		
	// status nowości
		if (!$bIsErr) {
			$aNews = $this->getNews($iId);
			if(empty($aNews)){
			// produkt nie jest obecnie nowością
				if($_POST['news_status'] > 0){
					// dodajemy do nowości
						if($this->addNews($iId,true)===false){
							$bIsErr = true;
						}
				}
			}
		}


		if (!$bIsErr) {
			// rekord zostal dodany
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
			getPagePath((double) $_POST['page_id'], $this->iLangId, true),
			$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			// rekord nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
			getPagePath((double) $_POST['page_id'], $this->iLangId, true),
			$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty);
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych produkt
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iPId	- Id strony aktualizowanego rekordu
	 * @param	integer	$iId	- Id aktualizowanego rekordu
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}
		
		if($this->existBookWithISBN(isbn2plain($_POST['isbn']),$iId)){
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['isbn_err'], true);
			// istnieje juz ksiazka z ppodanym isbnem
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}
		// sprawdzenie tagow
		if (trim($_POST['tags']) != '' && !$this->checkTags($_POST['tags'])) {
			// nieprawidlowo wypelnione pole tagi
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
			$this->AddEdit($pSmarty, $iId);
			return;
		}
		
		if(isset($_POST['published']) && $_POST['page_id'] == $aConfig['import']['unsorted_category']) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_unsorted_err'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEdit($pSmarty, $iId);
			return;
		}
		
		// pobranie aktualnego stanu publikacji
		$aCurrent =& $this->getItem($iId);

		Common::BeginTransaction();
		$aValues = array(
			'page_id' => (double) $_POST['page_id'],
			'name' => $_POST['name'],
			'name2' => !empty($_POST['name2'])?$_POST['name2']:'NULL',
			'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($_POST['name'], ' '))),
			'short_description' => strip_tags(br2nl(trimString($_POST['description'],200),' ')),
			'description' => $_POST['description'],
			'price_netto' => Common::formatPrice2($_POST['price_netto']),
			'price_brutto' => (Common::formatPrice2($_POST['price_netto'])*(1+$_POST['vat']/100)),
			'vat' => $_POST['vat'],
			'shipment_time' => isset($_POST['shipment_time'])?$_POST['shipment_time']:'1',
			'weight' => ($_POST['weight']>0)?(float)$_POST['weight']:'NULL',
			'isbn' => $_POST['isbn'],
			'isbn_plain' => isbn2plain($_POST['isbn']),
			'publication_year' => !empty($_POST['publication_year'])?(int)$_POST['publication_year']:'NULL',
			'pages' => !empty($_POST['pages'])?(int)$_POST['pages']:'NULL',
			'binding' => !empty($_POST['binding'])?$_POST['binding']:'NULL',
			'dimension' => !empty($_POST['dimension'])?$_POST['dimension']:'NULL',
			'language' => !empty($_POST['language'])?$_POST['language']:'NULL',
			'translator' => $_POST['translator'],
			'volumes' => !empty($_POST['volumes'])?(int)$_POST['volumes']:'NULL',
			'volume_nr' => !empty($_POST['volume_nr'])?(int)$_POST['volume_nr']:'NULL',
			'edition' => !empty($_POST['edition'])?(int)$_POST['edition']:'NULL',
			'volume_name' => !empty($_POST['volume_name'])?$_POST['volume_name']:'NULL',
			'city' => !empty($_POST['city'])?$_POST['city']:'NULL',
			'original_language' => !empty($_POST['original_language'])?$_POST['original_language']:'NULL',
			'original_title' => !empty($_POST['original_title'])?$_POST['original_title']:'NULL',
			'prod_status' => isset($_POST['prod_status'])?$_POST['prod_status']:'0',
			'published' => isset($_POST['published']) ? '1' : '0',
			'legal_status_date'	=>	(!empty($_POST['legal_status_date'])&&($_POST['legal_status_date']!='00-00-0000'))?FormatDate($_POST['legal_status_date']):'NULL',
			'shipment_date'	=>	($_POST['prod_status']=='3' && !empty($_POST['shipment_date'])&&($_POST['shipment_date']!='00-00-0000'))?FormatDate($_POST['shipment_date']):'NULL',
		);
	if(!empty($_POST['attachment_type'])){
			$aValues['price_brutto']+=(Common::formatPrice2($_POST['attachment_price_netto'])*(1+$_POST['attachment_vat']/100));
		}
	if($_POST['prod_status'] == '3'){
			$aValues['shipment_time'] = '0';
		}
	// dodawanie wydawcy
		$sSql = "SELECT id
					 FROM ".$aConfig['tabls']['prefix']."products_publishers 
					 WHERE name LIKE '".$_POST['publisher']."'"; 
		$iPublisher = (int)Common::GetOne($sSql);
		if($iPublisher > 0){
			$aValues['publisher_id']=$iPublisher;
		} elseif(!empty($_POST['publisher'])){
			// dodajemy nowego wydawcę
			
			$aPValues = array(
				'index_letter' => strtoupper(mb_substr($_POST['publisher'], 0, 1, 'utf8')),
				'name' => decodeString($_POST['publisher']),
				'created' => 'NOW()',
				'created_by' => $_SESSION['user']['name']
			);
			if (($iNewId = Common::Insert($aConfig['tabls']['prefix']."products_publishers",
												 					  $aPValues)) !== false) {
				$aValues['publisher_id']=$iNewId;
			} else {
				$bIsErr = true;
			}
			if(!$bIsErr){
				$sSql="SELECT id FROM ".$aConfig['tabls']['prefix']."products_publishers_import
					 WHERE name = ".Common::Quote(stripslashes($_POST['publisher']));
				$iIId=Common::GetOne($sSql);
				// wydawnictwo istnieje w tabeli ukrytej
				if($iIId == false) {
					$aPIValues = array(
						'name' => decodeString($_POST['publisher'])
					);
					if (($iIId = Common::Insert($aConfig['tabls']['prefix']."products_publishers_import", $aPIValues)) === false) {
						$bIsErr = true;
					}
				}
				if(!$bIsErr){
					$aPMValues = array(
						'publisher_id' => $iNewId,
						'publisher_import_id' => $iIId
					);
					if (Common::Insert($aConfig['tabls']['prefix']."products_publishers_mappings",$aPMValues,'',false) === false) {
							$bIsErr = true;
					}
				}
			}
		}

		$aValues['published'] = isset($_POST['published']) ? '1' : '0';

		// aktualizacja
		if(!$bIsErr){
		$bIsErr = Common::Update($aConfig['tabls']['prefix']."products",
															$aValues,
											 			 "id = ".$iId) === false;
		}
		if(!$bIsErr){
			if($aCurrent['published'] != $aValues['published']){
				if($aValues['published'] == '1'){
					if($this->publishTags($iId)==false){
						$bIsErr=true;
					}
				} else {
					if($this->unpublishTags($iId)==false){
						$bIsErr=true;
					}
				}
			}
		}
				
		//usuniecie poprzednich serii wydawniczych
		if (!$bIsErr){
			$sSql="DELETE FROM ".$aConfig['tabls']['prefix']."products_to_series
						WHERE product_id = ".$iId;
			if(Common::Query($sSql)===false){
				$bIsErr = true;
			}
		}
		if (!$bIsErr){
		// dodanie serii wydawniczych
			if(empty($_POST['series']) && !empty($_POST['series_name']) && !empty($aValues['publisher_id'])){
				$aValues = array(
					'name' => decodeString($_POST['series_name']),
					'publisher_id' => (int)$aValues['publisher_id'], 
					'created' => 'NOW()',
					'created_by' => $_SESSION['user']['name']
				);
				if (($iNewId = Common::Insert($aConfig['tabls']['prefix']."products_series",
													 					  $aValues)) !== false) {
					$_POST['series']=$iNewId;
				} else {
					$bIsErr = true;
				}
			}
		}
		if (!$bIsErr){
			if(!empty($_POST['series'])){
				$aValues=array(
					'product_id' => $iId,
					'series_id' => $_POST['series']
				);
				if ((Common::Insert($aConfig['tabls']['prefix']."products_to_series",$aValues,'',false)) === false) {
					$bIsErr = true;
				}
			}
		}
		// załacznik
		$iCurrentAttach=$this->getCurrentAttachment($iId);
		if (!$bIsErr){
			if(!empty($_POST['attachment_type'])){
				$aValues=array(
					'attachment_type' => $_POST['attachment_type'],
					'price_netto' => Common::formatPrice2($_POST['attachment_price_netto']),
					'price_brutto' => (Common::formatPrice2($_POST['attachment_price_netto'])*(1+$_POST['attachment_vat']/100)),
					'vat' => (int)$_POST['attachment_vat']
				);
				if($iCurrentAttach > 0){
					$bIsErr = Common::Update($aConfig['tabls']['prefix']."products_attachments", $aValues,
											 			 				"id = ".$iCurrentAttach) === false;
				} else {
					$aValues['product_id'] = $iId;
					if ((Common::Insert($aConfig['tabls']['prefix']."products_attachments",$aValues,'',false)) === false) {
						$bIsErr = true;
					}
				}
		} else {
			if($iCurrentAttach > 0){
				$sSql="DELETE FROM ".$aConfig['tabls']['prefix']."products_attachments
							 WHERE product_id = ".$iId;
				$bIsErr=(Common::Query($sSql)===false);
			}
		}
	}
	//usuniecie poprzednich dodatkopwych kategorii
		if (!$bIsErr){
			$sSql="DELETE FROM ".$aConfig['tabls']['prefix']."products_extra_categories
						WHERE product_id = ".$iId;
			$bIsErr=(Common::Query($sSql)===false);
		}
	// dodanie dodatkopwych kategorii
	$_POST['extra_categories'][]=$_POST['page_id'];
		// dodanie dodatkopwych kategorii
		if(!$bIsErr && !empty($_POST['extra_categories'])){
			foreach($_POST['extra_categories'] as $iCatId){
				if($iCatId != $aConfig['import']['unsorted_category'] && !$this->existExtraCategory($iCatId,$iId)){
					$aValues=array(
						'product_id' => $iId,
						'page_id' => $iCatId
					);
					if ((Common::Insert($aConfig['tabls']['prefix']."products_extra_categories",$aValues,'',false)) === false) {
						$bIsErr = true;
					}
					// pobranie kategorii - rodzicow wybranej kategorii
					$aParents=$this->getPathIDs($iCatId,$_SESSION['lang']['id']);
					// dodanie mapowan w gore drzewa lokalnego
					if(!empty($aParents)){
						foreach($aParents as $iParent){
							if(!$this->existExtraCategory($iParent,$iId)){
								$aValues=array(
									'product_id' => $iId,
									'page_id' => $iParent
								);
								if ((Common::Insert($aConfig['tabls']['prefix']."products_extra_categories",$aValues,'',false)) === false) {
									$bIsErr = true;
								}
							}
						}
					}
				}
			}
		}
		
		if (!$bIsErr) {
			// aktualizacja tagow
			$bIsErr = !$this->addTags($iId, trim($_POST['tags']) != '' ? explode(',', $_POST['tags']) : array(), true);
		}

	// status nowości
	if (!$bIsErr) {
		$aNews = $this->getNews($iId);
		if(empty($aNews)){
		// produkt nie jest obecnie nowością
			if($_POST['news_status'] > 0){
				// dodajemy do nowości
					if($this->addNews($iId,true)===false){
						$bIsErr = true;
					}
			}
		} else {
			// produkt jest nowością
			if($_POST['news_status'] > 0){
				if($_POST['news_status'] != $aNews['page_id']){
				// mamy zmianę - aktualizujemy nowość
					$aValues = array (
				 		'page_id' => $_POST['news_status']
				 	);
					if ((Common::Update($aConfig['tabls']['prefix']."products_news",
													 $aValues,
													 'id = '.$aNews['id'])) === false) {
						$bIsErr = true;
					}
				}
			} else {
				// usuwamy nowość
				if($this->deleteNews($iId)===false){
					$bIsErr = true;
				}
			}
		}
	}

	if (!$bIsErr) {
			// stan publikacji
			$_POST['published'] = (int) $_POST['published'];
			$_POST['old_published'] = (int) $_POST['old_published'];

	// tabela cieni
	// produkt obecnie nie jest opublikowany - zmiana na opublikowany - dodanie cieia
			if($_POST['old_published'] == 0) {
				if($_POST['published'] == 1) {
					if($this->existShadow($iId)) {
						if($this->updateShadow($iId)===false){
							$bIsErr=true;
						}
					} else {
						if($this->addShadow($iId)===false){
							$bIsErr=true;
						}
					}
				}
			} else {
		// produkt jest opublikowany - zmiana na nieopublikowany - usuniecie cienia
				if($_POST['published'] == 0) {
					if($this->deleteShadow($iId)===false){
						$bIsErr=true;
					}
				} else {
		// brak zmiany stany publikacji - aktualizacja cienia
					if($this->existShadow($iId)) {
						if($this->updateShadow($iId)===false){
							$bIsErr=true;
						}
					} else {
						if($this->addShadow($iId)===false){
							$bIsErr=true;
						}
					}
				}	
			}
		}
		
	
	
		if (!$bIsErr) {
			// rekord zostal zaktualizowany
			SetModification('products',$iId);
			Common::CommitTransaction();
			
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
			getPagePath((double) $_POST['page_id'], $this->iLangId, true),
			$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// rekord nie zostal zmieniony,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
			getPagePath((double) $_POST['page_id'], $this->iLangId, true),
			$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda zmienia stan publikacji wybranego produktu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id produktu
	 * @return	void
	 */
	function Publish(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;
		
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			foreach ($_POST['delete'] as $iItem) {

			// pobranie aktualnego stanu publikacji
			$aProduct =& $this->getItem($iItem);
			if($aProduct['page_id'] == $aConfig['import']['unsorted_category']) {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_unsorted_err'],
												$aProduct['name']);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
				$this->Show($pSmarty, $iPId);
				return 0;
			}
			$iPublished = $aProduct['published'] == 1 ? 0 : 1;
	
			// zmiana stanu publikacji produktu
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							 SET published = IF (published = '0', '1', '0')
							 WHERE id = ".$iItem;
			if(($iRes = Common::Query($sSql)) === false){
				$bIsErr=true;
			}
		
			if($aProduct['published'] == '1'){
				if($this->unpublishTags($iItem)==false){
					$bIsErr=true;
				}
			} else {
				if($this->publishTags($iItem)==false){
					$bIsErr=true;
				}
			}
		

			if($aProduct['published'] == '1'){
				if($this->deleteShadow($iItem)==false){
					$bIsErr=true;
				}
			} else {
				if(!$this->existShadow($iItem)) {
					if($this->addShadow($iItem)==false){
						$bIsErr=true;
					}
				}
			}
			
			$sDel .= $aProduct['name'].', ';
				SetModification('products',$iItem);

			}
		}
		$sDel = substr($sDel, 0, -2);
		
		if ($bIsErr) {
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_err'],
			'',
			$sDel,
			$aConfig['lang'][$this->sModule]['published_'.$iPublished]);
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
		}
		else {
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_ok'],
			'',
			$sDel,
			$aConfig['lang'][$this->sModule]['published_'.$iPublished]);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
		}
		$this->Show($pSmarty, $iPId);
	} // end of Publish() funciton
	
	/**
	 * Metoda zmienia stan obrobienia wybranego produktu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id produktu
	 * @return	void
	 */
	function Review(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;
		
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			foreach ($_POST['delete'] as $iItem) {

				// pobranie aktualnego stanu publikacji
				$aProduct =& $this->getItem($iItem);
		
				// zmiana stanu publikacji produktu
				$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
								 SET reviewed = '1'
								 WHERE id = ".$iItem;
				if(Common::Query($sSql) === false){
					$bIsErr = true;
				}
				$sDel .= $aProduct['name'].', ';
				SetModification('products',$iItem);

			}
		}
		$sDel = substr($sDel, 0, -2);
		if ($bIsErr) {
				Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['review_err'],
			'',
			$sDel);
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
		}
		else {
				Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['review_ok'],
			'',
			$sDel);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
		}
		$this->Show($pSmarty, $iPId);
	} // end of Publish() funciton
	
	function UpdateTags(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$iI = 0;
		foreach($_POST['products'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['products'] =& $aTmp;
		
		$aTags = explode(',', $_POST['tags']);
	// sprawdzenie tagow
		if (trim($_POST['tags']) != '' && !$this->checkTags($_POST['tags'])) {
			// nieprawidlowo wypelnione pole tagi
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
			$this->Show($pSmarty);
			return;
		}
		if(trim($_POST['tags'])==''){
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_empty'], true);
		} else {
		
			if (!empty($_POST['products'])) {
				Common::BeginTransaction();
				foreach ($_POST['products'] as $iItem) {
					
					if (!$bIsErr) {
						if($this->addTags($iItem, $aTags) === false){
							$bIsErr = true;
						} else {
							if($this->updateShadowTags($iItem) === false){
								$bIsErr=true;
							}
						}
					}
					$sDel .= $this->getItemName($iItem).', ';

				}
			}
			$sDel = substr($sDel, 0, -2);
		
			if ($bIsErr) {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['tags_err'],$_POST['tags'],$sDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
			else {
				Common::CommitTransaction();
				foreach ($_POST['products'] as $iItem) {
					SetModification('products',$iItem);
				}
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['tags_ok'],$_POST['tags'],$sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
		} 
		$this->Show($pSmarty);
	} // end of Publish() funciton
	
	function UpdateStatus(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$iI = 0;
		foreach($_POST['products'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['products'] =& $aTmp;
		
		$aTags = explode(',', $_POST['tags']);
		if($_POST['prod_status']==''){
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['statuses_empty'], true);
		} else {
			Common::BeginTransaction();
			if (!empty($_POST['products'])) {
				foreach ($_POST['products'] as $iItem) {
					if (!$bIsErr) {
						$aValues = array(
							'prod_status' => isset($_POST['prod_status'])?$_POST['prod_status']:'0'							
						);
						if( Common::Update($aConfig['tabls']['prefix']."products", $aValues,"id = ".$iItem) === false){
							$bIsErr = true;
						}
					}	
					if(!$bIsErr && $this->existShadow($iItem)) {
						$aValues = array(
							'prod_status' => isset($_POST['prod_status'])?$_POST['prod_status']:'0'
						);
						if (Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues,'id = '.$iItem) === false) {
							$bIsErr = true;
						}
					}
					$sDel .= $this->getItemName($iItem).', ';
				}
				$sDel = substr($sDel, 0, -2);
			}
			
		
			if ($bIsErr) {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['statuses_err'],$aConfig['lang'][$this->sModule]['status_'.$_POST['prod_status']],$sDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
			else {
				Common::CommitTransaction();
				foreach ($_POST['products'] as $iItem) {
					SetModification('products',$iItem);
				}
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['statuses_ok'],$aConfig['lang'][$this->sModule]['status_'.$_POST['prod_status']],$sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
		} 
		$this->Show($pSmarty);
	} // end of UpdateStatus() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji produktu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- ID strony edytowanego rekordu
	 * @param	integer	$iId	- ID edytowanego rekordu
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
//		$pSmarty->assign('sBodyOnload', 'checkDiscounts(document.getElementById(\'discount\'), document.getElementById(\'exclude_discount\'), document.getElementById(\'users_discount\'), document.getElementById(\'total_value_discount\'));');

		$aExtraCat=$this->getProductsCategories(Common::getProductsMenu($this->iLangId),'');
		$aCats=array();
		$aData['legal_status_date']='00-00-0000';
		$aData['shipment_date']='00-00-0000';
		if ($iId > 0) {
			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT A.*, A.published old_published, A.page_id curr_page_id, 
								DATE_FORMAT(legal_status_date, '".$aConfig['common']['cal_sql_date_format']."')
											AS legal_status_date, 
								DATE_FORMAT(shipment_date, '".$aConfig['common']['cal_sql_date_format']."')
											AS shipment_date,	B.name AS publisher
							 FROM ".$aConfig['tabls']['prefix']."products A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
							 ON A.publisher_id=B.id
							 WHERE A.id = ".$iId;
			$aData =& Common::GetRow($sSql);

			if(empty($aData['legal_status_date'])){
				$aData['legal_status_date']='00-00-0000';
			}
			if(empty($aData['shipment_date'])){
				$aData['shipment_date']='00-00-0000';
			}
			$sName = $aData['plain_name'];
			$aData['series']=$this->getProductSeries($iId);
			$aData['news_status']=$this->getNewsStatus($iId);
			$aCats=$this->getExtraCategories($iId);
			$aAttachData=$this->getAttachmentForProduct($iId);
			if(!empty($aAttachData)){
				$aData=array_merge($aData,$aAttachData);
			}
			// tagi
			$aData['tags'] = $this->getProductTags($iId);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
			if(isset($aData['extra_categories'])){
				$aCats=$aData['extra_categories'];
			}
		}
		$sCurrentExtraCats='';
		foreach($aExtraCat as $aCat){
			if(in_array($aCat['value'],$aCats)){
				$sCurrentExtraCats.=getPagePath($aCat['value'],$this->iLangId,true,false,false)."<br />\n";
			}
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($sName) && !empty($sName)) {
			$sHeader .= ' "'.trimString($sName, 50).'"';
		}

		$pForm = new FormTable('products', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
		if ($iId > 0) {
			$pForm->AddHidden('old_published', $aData['old_published']);
			$pForm->AddHidden('curr_page_id', $aData['curr_page_id']);
		}

		// dla strony
		//$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], getPagePath((double) $aData['curr_page_id'], $this->iLangId, true), '', array(), array('class'=>'redText'));
		// kategoria
		$pForm->AddSelect('page_id',$aConfig['lang'][$this->sModule]['main_category'], array('style'=>'width:300px;'), $this->getTopProductsCategories(), $aData['page_id']);

		// obecne dodatkowe kategorie
		if(!empty($sCurrentExtraCats)){
			$pForm->AddRow($aConfig['lang'][$this->sModule]['current_extra_categories'], $sCurrentExtraCats);
		}
		
		// drzewo kategorii
		$pForm->AddRow($aConfig['lang'][$this->sModule]['extra_categories'],
			'<div style="height:300px; overflow:auto;">'.
			Form::GetCheckTree('extra_categories[]',array(),$this->getProductsCategoriesChTree(Common::getProductsMenu($this->iLangId)),$aCats)
			.'</div>'
			);
			
		
//		$pForm->AddRow($aConfig['lang'][$this->sModule]['extra_categories'], 
//									$pForm->GetSelectHtml('avaible_categories',$aConfig['lang'][$this->sModule]['avaible_categories'], array('size'=>10,'style'=>'width:300px;'), $aExtraCat,'','',false).'&nbsp;'.
//									$pForm->GetInputButtonHTML('add_cat', $aConfig['lang'][$this->sModule]['add_cat'], array('onclick'=>'addExtraCategory();'), 'button').'&nbsp;'.
//									$pForm->GetInputButtonHTML('del_cat', $aConfig['lang'][$this->sModule]['del_cat'], array('onclick'=>'delExtraCategory();'), 'button').'&nbsp;'.
//									$pForm->GetSelectHtml('extra_categories',$aConfig['lang'][$this->sModule]['extra_categories'], array('size'=>10,'style'=>'width:300px;','multiple'=>1), $aData['extra_categories'],'','',false));
//		

		$pForm->AddRadioSet('news_status', $aConfig['lang'][$this->sModule]['news_status'], array_merge(array(array('value'=>0,'label'=>$aConfig['lang'][$this->sModule]['not_news'])),getModulePages('m_powiazane_oferta_produktowa',$this->iLangId,false,'','nowosci')), $aData['news_status'], '', true, false);
//			$pForm2->AddSelect('page_id',$aConfig['lang'][$this->sModule]['page_id'],array(),,$aData['page_id']);
		// nazwa
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>500, 'style'=>'width: 450px;'), '', 'text');

			$pForm->AddText('name2', $aConfig['lang'][$this->sModule]['name2'], $aData['name2'], array('maxlength'=>500, 'style'=>'width: 450px;'), '', 'text',false);
			
			
		
			
	
		// wydawnictwo
		//$pForm->AddSelect('publisher_id',$aConfig['lang'][$this->sModule]['publisher'], array('onchange'=>'document.getElementById(\'do\').value =\'add\'; selectExtraCategories(); this.form.submit();'), $this->getPublishers(), $aData['publisher_id']);
		$pForm->AddText('publisher',$aConfig['lang'][$this->sModule]['publisher'], $aData['publisher'],array('id'=>'publisher_aci','style'=>'width:300px;','maxlength'=>255),'','text' ,true);
		
		
		// seria
		$pForm->AddRow($aConfig['lang'][$this->sModule]['series'],
										$pForm->GetSelectHtml('series', $aConfig['lang'][$this->sModule]['series'],array('style'=>'width:300px;'), (!empty($aData['publisher_id'])?$this->getSeriesList($aData['publisher_id']):array()), (!empty($aData['publisher_id'])?$this->getProductSeries($iId):''),'',false).
										'&nbsp;&nbsp;&nbsp;'.$aConfig['lang'][$this->sModule]['add_series'].'&nbsp;&nbsp;&nbsp;'.
										$pForm->GetTextHTML('series_name',$aConfig['lang'][$this->sModule]['series'], '',array('style'=>'width:200px;','maxlength'=>255),'','text' ,false)	);

		

		// isbn
		$pForm->AddText('isbn',$aConfig['lang'][$this->sModule]['isbn'], $aData['isbn'],array('maxlength'=>20),'','text' ,true);
		//rok wydania
		//$pForm->AddText('publication_year',$aConfig['lang'][$this->sModule]['publication_year'], $aData['publication_year'],array('maxlength'=>4),'','uinteger' ,false);
		$pForm->AddSelect('publication_year',$aConfig['lang'][$this->sModule]['publication_year'],array(), $this->getYears(), $aData['publication_year'],'',false);
		
		// wydanie
		$pForm->AddSelect('edition',$aConfig['lang'][$this->sModule]['edition'], array(), $this->getNumbersList(50), $aData['edition'],'',false);
		// stan prawny na 
		$pForm->AddText('legal_status_date',$aConfig['lang'][$this->sModule]['legal_status_date'], $aData['legal_status_date'],array(),'','date' ,false);
		// miejsce wydania
		$pForm->AddText('city',$aConfig['lang'][$this->sModule]['city'], $aData['city'],array('id'=>'city_aci','maxlength'=>45),'','text' ,false);
		// stron
		$pForm->AddText('pages',$aConfig['lang'][$this->sModule]['pages'], $aData['pages'],array('maxlength'=>6),'','text' ,false);
		//oprawa
		$pForm->AddSelect('binding',$aConfig['lang'][$this->sModule]['binding'],array(), $this->getBindings(), $aData['binding'],'',false);
		// wymiary
		$pForm->AddSelect('dimension',$aConfig['lang'][$this->sModule]['dimension'],array(), $this->getDimensions(), $aData['dimension'],'',false);
		// jezyk ksiazki
		$pForm->AddSelect('language',$aConfig['lang'][$this->sModule]['language'],array(), $this->getLanguages(), $aData['language'],'',false);
		// tłumacz
		$pForm->AddText('translator',$aConfig['lang'][$this->sModule]['translator'], $aData['translator'],array('maxlength'=>100),'','text' ,false);
		// jezyk oryginalny
		$pForm->AddSelect('original_language',$aConfig['lang'][$this->sModule]['original_language'],array(), $this->getLanguages(), $aData['original_language'],'',false);
		// tytul oryginalny
		$pForm->AddText('original_title',$aConfig['lang'][$this->sModule]['original_title'], $aData['original_title'],array('maxlength'=>500),'','text' ,false);
		// waga
		$pForm->AddText('weight', $aConfig['lang'][$this->sModule]['weight'], Common::formatPrice($aData['weight']), array('maxlength'=>12, 'style'=>'width: 75px;'), '', 'ufloat',false);
		// tomów
		$pForm->AddSelect('volumes',$aConfig['lang'][$this->sModule]['volumes'], array(),$this->getNumbersList(50),$aData['volumes'],'', false);
		// nr tomu
		$pForm->AddSelect('volume_nr',$aConfig['lang'][$this->sModule]['volume_nr'], array(),$this->getNumbersList(50), $aData['volume_nr'],'',false);
		// nazwa tomu
		$pForm->AddText('volume_name',$aConfig['lang'][$this->sModule]['volume_name'], $aData['volume_name'],array('maxlength'=>75),'','text' ,false);
		
		// tagi
		$pForm->AddRow($aConfig['lang'][$this->sModule]['new_tag'],
									 $pForm->GetTextHtml('new_tag',$aConfig['lang'][$this->sModule]['new_tag'], $aData['new_tag'],array('id'=>'tags_aci','style'=>'width:300px;','maxlength'=>64),'','text' ,false).'&nbsp;&nbsp;'.
									 $pForm->GetInputButtonHTML('add_tag',$aConfig['lang'][$this->sModule]['add_tag_button'],array('id'=>'add_tag_button'),'button'));
		$pForm->AddTextArea('tags', $aConfig['lang'][$this->sModule]['tags'], $aData['tags'], array('id'=>'tags_list','rows'=>'8', 'style'=>'width: 450px;'), 512, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);
		
		// krotki opis
		//$pForm->AddTextArea('short_description', $aConfig['lang'][$this->sModule]['short_description'], $aData['short_description'], array('rows'=>'8', 'style'=>'width: 450px;'), 255, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);

		// opis produktu
		$pForm->AddWYSIWYG('description', $aConfig['lang'][$this->sModule]['description'], $aData['description']);

		
		// CENA
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['price_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));

//
//	// bazowa
		$pForm->AddText('price_brutto', $aConfig['lang'][$this->sModule]['price_brutto'], isset($aData['price_brutto']) && $aData['price_brutto'] != '' ? Common::formatPrice($aData['price_brutto']) : '', array('readonly'=>'readonly','maxlength'=>12, 'style'=>'width: 75px;'), '', 'ufloat',false);
//	// netto
		$pForm->AddText('price_netto', $aConfig['lang'][$this->sModule]['price_netto'], isset($aData['price_netto']) && $aData['price_netto'] != '' ? Common::formatPrice($aData['price_netto']) : '', array('maxlength'=>12, 'style'=>'width: 75px;','onchange'=>'calculateBasePrice();'), '', 'ufloat',true);
		
		// stawka VAT
		//$pForm->AddText('vat', $aConfig['lang'][$this->sModule]['vat'], $aData['vat'], array('maxlength'=>3, 'style'=>'width: 50px;'), '', 'range', true, '', array(0,100));
		$pForm->AddSelect('vat',$aConfig['lang'][$this->sModule]['vat'], array('onchange'=>'calculateBasePrice();'),$this->getVATList(), $aData['vat'],'',true);
	// STATUS PRODUKTU
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['attachment_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		
		// załącznik
		$pForm->AddSelect('attachment_type',$aConfig['lang'][$this->sModule]['attachment_type'],array('onchange'=>'setAttachment(this); calculateBasePrice();'), $this->getAttachmentTypes(), $aData['attachment_type'],'',false);
		if( $aData['attachment_type'] > 0){
			$aAttachmentAttribs=array();
		} else {
			$aAttachmentAttribs=array('disabled'=>'disabled');
		}
		// netto
		$pForm->AddText('attachment_price_netto', $aConfig['lang'][$this->sModule]['attachment_price_netto'], isset($aData['attachment_price_netto']) && $aData['attachment_price_netto'] != '' ? Common::formatPrice($aData['attachment_price_netto']) : '', array_merge($aAttachmentAttribs,array('maxlength'=>12, 'style'=>'width: 75px;','onchange'=>'calculateBasePrice();')), '', 'ufloat',false);
		
		// stawka VAT
		//$pForm->AddText('vat', $aConfig['lang'][$this->sModule]['vat'], $aData['vat'], array('maxlength'=>3, 'style'=>'width: 50px;'), '', 'range', true, '', array(0,100));
		$pForm->AddSelect('attachment_vat',$aConfig['lang'][$this->sModule]['attachment_vat'], array_merge($aAttachmentAttribs,array('onchange'=>'calculateBasePrice();')),$this->getVATList(), $aData['attachment_vat'],'',false);
		
		
		// STATUS PRODUKTU
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['status_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));

		// status
		$aStatuses = array(
		array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true;'),
		array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['status_0'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true;'),
		array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['status_2'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true;'),
		array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['status_3'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=false;')
		);
		$pForm->AddRadioSet('prod_status', $aConfig['lang'][$this->sModule]['prod_status'], $aStatuses, $aData['prod_status'], '', true, false);

		if($aData['prod_status'] == '3'){
			$aShipmentDateAttribs = array('style'=>'width: 75px;');
		} else {
			$aShipmentDateAttribs = array('style'=>'width: 75px;','disabled'=>'disabled');
		}
		$pForm->AddText('shipment_date', $aConfig['lang'][$this->sModule]['shipment_date'], $aData['shipment_date'], $aShipmentDateAttribs, '', 'date', false);
		// zdefiniowane opisy statusu
		//$pForm->AddSelect('ready_statuses', $aConfig['lang'][$this->sModule]['ready_statuses'], array('onchange' => 'setStatus(this);'), $this->getStatusesDescriptions(), '', '', false);

		// opis statusu
		//$pForm->AddTextArea('prod_status_text', $aConfig['lang'][$this->sModule]['prod_status_text'], $aData['prod_status_text'], array('rows'=>'6', 'style'=>'width: 350px;'), 255, '', $aLang['too_long_prod_status_text'], false);

        // status
        $pShipmentTime = new ShipmentTime();

        $aShipments = array(
            array('value' => '0', 'label' => $pShipmentTime->getShipmentTime($iId, 0)),
            array('value' => '1', 'label' => $pShipmentTime->getShipmentTime($iId, 1)),
            array('value' => '2', 'label' => $pShipmentTime->getShipmentTime($iId, 2)),
            array('value' => '3', 'label' => $pShipmentTime->getShipmentTime($iId, 3)),
            array('value' => '4', 'label' => $pShipmentTime->getShipmentTime($iId, 4)),
            array('value' => '5', 'label' => $pShipmentTime->getShipmentTime($iId, 5)),
            array('value' => '6', 'label' => $pShipmentTime->getShipmentTime($iId, 6)),
            array('value' => '7', 'label' => $pShipmentTime->getShipmentTime($iId, 7)),
            array('value' => '8', 'label' => $pShipmentTime->getShipmentTime($iId, 8)),
            array('value' => '9', 'label' => $pShipmentTime->getShipmentTime($iId, 9)),
            array('value' => '10', 'label' => $pShipmentTime->getShipmentTime($iId, 10)),
        );
		$pForm->AddRadioSet('shipment_time', $aConfig['lang'][$this->sModule]['shipment_time'], $aShipments, $aData['shipment_time'], '', true, false);
		

		// STAN PUBLIKACJI
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['publ_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));

		// status publikacji

		$pForm->AddCheckBox('published', $aConfig['lang'][$this->sModule]['published'], array(), '', !empty($aData['published']), false);

				// status
		if($aData['source'] != ''){
			$pForm->AddRow($aConfig['lang'][$this->sModule]['source'],$aConfig['lang'][$this->sModule]['source_'.$aData['source']]);
		}
		if(!empty($aData['last_import'])){
			$pForm->AddRow($aConfig['lang'][$this->sModule]['last_import'],$aData['last_import']);
		}
		
		if(!empty($aData['modified'])){
			$pForm->AddRow($aConfig['lang'][$this->sModule]['modified'],$aData['modified']);
		}
		
		if(!empty($aData['modified_by'])){
			$pForm->AddRow($aConfig['lang'][$this->sModule]['modified_by'],$aData['modified_by']);
		}
		
//		if($aData['reviewed'] =='1'){
//			$pForm->AddRow($aConfig['lang'][$this->sModule]['f_reviewed'],($aData['reviewed']=='1'?$aConfig['lang'][$this->sModule]['f_reviewed']:$aConfig['lang'][$this->sModule]['f_not_reviewed']));
//		}
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'selectExtraCategories();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		// funkcja JS - setStatus
		$sJS = '
		<script type="text/javascript">
		
		$(document).ready(function(){
			$("#tags_aci").autocomplete("ajax/GetTags.php",{ minChars: 2, max: 100, matchContains: true, cacheLength: 1, matchSubset: false }).result(function(event, item) {
  			$("#add_tag_button").focus();
			});
			$("#publisher_aci").autocomplete("ajax/GetPublishers.php",{ minChars: 2, max: 100, matchContains: true, cacheLength: 1, matchSubset: false  });
			$("#city_aci").autocomplete("ajax/GetCities.php",{ minChars: 2, max: 100, matchContains: true, cacheLength: 1, matchSubset: false  });
			$("#publisher_aci").bind("blur", function(e){
     	 $.get("ajax/GetSeries.php", { publisher: e.target.value },
					  function(data){
					    var brokenstring=data.split(";");
					    var elSelSeries = document.getElementById(\'series\');
					    elSelSeries.length = 0;
					    elSelSeries.options[elSelSeries.length] =  new Option("'.$aConfig['lang']['common']['choose'].'","0");
					    for (x in brokenstring){
								if(brokenstring[x]){
									var item=brokenstring[x].split("|");
		  						elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
	  						}
	  					}
					 });
    	});
    	$("#add_tag_button").click(function () {
    		var sCurTags = $("#tags_list").val();
    		var sNewTag = $("#tags_aci").val();
    		if(sNewTag != ""){
    			if(sCurTags != ""){
    				sCurTags = sCurTags+",";
					}
      		$("#tags_list").val(sCurTags+sNewTag);
      		$("#tags_aci").val("");
      	}
   		});
    	
		});
		
		function setAttachment(oSel) {
				if (oSel.value > 0) {
					document.getElementById(\'attachment_price_netto\').disabled=false;
					document.getElementById(\'attachment_vat\').disabled=false;
				} else {
					document.getElementById(\'attachment_price_netto\').disabled=true;
					document.getElementById(\'attachment_price_netto\').value=\'\';
					document.getElementById(\'attachment_vat\').disabled=true;
					document.getElementById(\'attachment_vat\').value=\'\';
				}
			}
		
		function calculateBasePrice(){
			var fNetto = parseFloat(document.getElementById(\'price_netto\').value.replace(\',\',\'.\'));
			var iVat = parseFloat(document.getElementById(\'vat\').value);
			var iAttachment = document.getElementById(\'attachment_type\').value;
			var elBasePrice = document.getElementById(\'price_brutto\');
			var fPrice = fNetto * (1+iVat/100);
			if(iAttachment > 0){
				var fAttNetto = parseFloat(document.getElementById(\'attachment_price_netto\').value.replace(\',\',\'.\'));
				var iAttVat = parseFloat(document.getElementById(\'attachment_vat\').value);
				if(!isNaN(fAttNetto) && !isNaN(iAttVat)){
					fPrice=fPrice + fAttNetto * (1+iAttVat/100);
				}
			}
			fPrice = parseInt(fPrice*100)/100;
			elBasePrice.value = fPrice.toString().replace(\'.\',\',\');
		}
		
			function setStatus(oSel) {
				if (oSel.options.selectedIndex > 0) {
					document.getElementById(\'prod_status_text\').value = oSel.options[oSel.options.selectedIndex].value;
				}
			}
		</script>
		';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function

	
	function TagAllForm(&$pSmarty) {
		global $aConfig;

		$aData = array();
		$sHtml = '';
		if(empty($_POST['delete'])){
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_products_empty'], true);
			// dodanie informacji do logow
			AddLog($sMsg, true);
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
//		$pSmarty->assign('sBodyOnload', 'checkDiscounts(document.getElementById(\'discount\'), document.getElementById(\'exclude_discount\'), document.getElementById(\'users_discount\'), document.getElementById(\'total_value_discount\'));');

		$sNames='';
		foreach($_POST['delete'] as $sKey => $sVal) {
			$sNames.=$this->getItemName($sKey).', ';
		}
		$sNames=substr($sNames,0,-2);
		
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_tags'].' "'.$sNames.'"';


		$pForm = new FormTable('products_tags', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_tags');
		if ($iId > 0) {
			$pForm->AddHidden('old_published', $aData['old_published']);
			$pForm->AddHidden('curr_page_id', $aData['curr_page_id']);
		}
		foreach($_POST['delete'] as $sKey => $sVal) {
			$pForm->AddHidden('products['.$sKey.']', '1');
		}

		// tagi
		$pForm->AddRow($aConfig['lang'][$this->sModule]['new_tag'],
									 $pForm->GetTextHtml('new_tag',$aConfig['lang'][$this->sModule]['new_tag'], $aData['new_tag'],array('id'=>'tags_aci','style'=>'width:300px;','maxlength'=>64),'','text' ,false).'&nbsp;&nbsp;'.
									 $pForm->GetInputButtonHTML('add_tag',$aConfig['lang'][$this->sModule]['add_tag_button'],array('id'=>'add_tag_button'),'button'));
		$pForm->AddTextArea('tags', $aConfig['lang'][$this->sModule]['tags'], $aData['tags'], array('id'=>'tags_list','rows'=>'8', 'style'=>'width: 450px;'), 512, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);
		
		$pForm->AddSelect('page_id',$aConfig['lang'][$this->sModule]['main_category'], array('style'=>'width:300px;'), $this->getTopProductsCategories(true), $aData['page_id']);
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array()).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		// funkcja JS - setStatus
		$sJS = '
		<script type="text/javascript">
		
		$(document).ready(function(){
			$("#tags_aci").autocomplete("ajax/GetTags.php",{ minChars: 2, max: 100, matchContains: true, cacheLength: 1, matchSubset: false }).result(function(event, item) {
  			$("#add_tag_button").focus();
			});
    	$("#add_tag_button").click(function () {
    		var sCurTags = $("#tags_list").val();
    		var sNewTag = $("#tags_aci").val();
    		if(sNewTag != ""){
    			if(sCurTags != ""){
    				sCurTags = sCurTags+",";
					}
      		$("#tags_list").val(sCurTags+sNewTag);
      		$("#tags_aci").val("");
      	}
   		});
    	
		});
		</script>
		';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	} // end of TagAllForm() function
	
	function ChangeStatusForm(&$pSmarty) {
		global $aConfig;

		$aData = array();
		$sHtml = '';
		if(empty($_POST['delete'])){
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['statuses_products_empty'], true);
			// dodanie informacji do logow
			AddLog($sMsg, true);
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
//		$pSmarty->assign('sBodyOnload', 'checkDiscounts(document.getElementById(\'discount\'), document.getElementById(\'exclude_discount\'), document.getElementById(\'users_discount\'), document.getElementById(\'total_value_discount\'));');

		$sNames='';
		foreach($_POST['delete'] as $sKey => $sVal) {
			$sNames.=$this->getItemName($sKey).', ';
		}
		$sNames=substr($sNames,0,-2);
		
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_status'].' "'.$sNames.'"';


		$pForm = new FormTable('products_status', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_status');

		foreach($_POST['delete'] as $sKey => $sVal) {
			$pForm->AddHidden('products['.$sKey.']', '1');
		}

		// status
		$aStatuses = array(
		array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1']),
		array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['status_0']),
		array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['status_2'])
		);
		$pForm->AddRadioSet('prod_status', $aConfig['lang'][$this->sModule]['prod_status'], $aStatuses, '', '', true, false);
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array()).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of ChangeStatusForm() function
	/**
	 * Metoda pobiera dostępne opisy statusów produktu
	 * @return unknown_type
	 */
	function &getStatusesDescriptions() {
		global $aConfig;
		$aStatuses = array();

		$sSql = "SELECT status_text
						 FROM ".$aConfig['tabls']['prefix']."products_statuses
						 ORDER BY id";
		$aItems =& Common::GetCol($sSql);
		if (empty($aItems)) return $aStatuses;

		$aStatuses = array(
		array('value' => '', 'label' => $aConfig['lang']['common']['choose'])
		);
		foreach($aItems as $sStatus) {
			$aStatuses[] = array(
				'value' => Common::encodeString(br2nl($sStatus)), 'label' => trimString(br2nl($sStatus, " "), 75)
			);
		}
		return $aStatuses;
	} // end of getStatusesDescriptions() method

	/**
	 * Metoda wprowadza zmiany w zdjeciach akapitu do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateImages(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		if (!$this->ProceedImages($pSmarty, $iPId, $iId)) {
			// wyswietlenie komunikatu o niepowodzeniu
			// wycofanie transakcji
			// oraz ponowne wyswietlenie formularza edycji
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['image_err_0'],
			getPagePath($iPId, $this->iLangId, true),
			$this->getItemName($iId));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEditImages($pSmarty, $iPId, $iId);
		}
		else {
			SetModification('products',$iId);
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['image_ok'],
			getPagePath($iPId, $this->iLangId, true),
			$this->getItemName($iId));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->AddEditImages($pSmarty, $iPId, $iId);
		}
	} // end of UpdateImages() funciton


	/**
	 * Metoda wprowadza zmiany w zdjeciach produktu do bazy danych
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function SaveAndAddImage(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		if (!$this->ProceedImages($pSmarty, $iPId, $iId)) {
			// wyswietlenie komunikatu o niepowodzeniu
			// wycofanie transakcji
			// oraz ponowne wyswietlenie formularza edycji
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['image_err_0'],
			getPagePath($iPId, $this->iLangId, true),
			$this->getItemName($iId));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEditImages($pSmarty, $iPId, $iId);
		}
		else {
			SetModification('products',$iId);
			Common::CommitTransaction();
			$this->AddEditImages($pSmarty, $iPId, $iId, true);
		}
	} // end of SaveAndAddImage() funciton


	/**
	 * Metoda wykonuje operacje na zdjeciach akapitu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId
	 * @param	integer	$iId
	 *
	 * @return	integer
	 */
	function ProceedImages(&$pSmarty, $iPId, $iId) {

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditImages($pSmarty, $iPId, $iId);
			return;
		}

		Common::BeginTransaction();
		// dolaczenie klasy Images
		include_once('ProductImages.class.php');
		$oImages = new ProductImages($iPId,
		$iId,
		$this->aSettings,
		$this->sDir,
													'product_id',
													'products_images');
		return $oImages->proceed(true, false);
	} // end of ProceedImages() method



	/**
	 * Metoda pobiera i zwraca stan publikacji produktu
	 *
	 * @param	integer	$iId	- Id produktu
	 * @return	integer
	 */
	function getItemPublishState($iId) {
		global $aConfig;

		$sSql = "SELECT published
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id = ".$iId;
		return intval(Common::GetOne($sSql));
	} // end of getItemPublishState() method


	/**
	 * Metoda zwraca liste produktow o przekazanych do niej Id
	 *
	 * @param	array	$aIds	- Id rekordow dla ktorych zwrocone zostana nazwy
	 * @return	string	- ciag z nazwami
	 */
	function getItemsNames($aIds) {
		global $aConfig;
		$sList = '';

		// pobranie nazw usuwanych rekordow
		$sSql = "SELECT name
				 		 FROM ".$aConfig['tabls']['prefix']."products
					 	 WHERE id IN  (".implode(',', $aIds).")";
		$aRecords =& Common::GetAll($sSql);
		foreach ($aRecords as $aItem) {
			$sList .= '"'.$aItem['name'].'", ';
		}
		$sList = substr($sList, 0, -2);
		return $sList;
	} // end of getItemsNames() method

	/**
	 * Metoda zwraca nazwe produktu o podanym id
	 */
	function getItemName($iId){
		global $aConfig;
		 
		$sSql = "SELECT name FROM ".$aConfig['tabls']['prefix']."products
	 						WHERE id = ".$iId."";

		return Common::GetOne($sSql);
	}

	/**
	 * Metoda zwraca liste produktow do usuniecia na podstawie
	 * przekazanych do niej Id
	 *
	 * @param	array	$aIds	- Id produktow
	 * @return	array ref
	 */
	function getItemsToDelete($aIds) {
		global $aConfig;
		$sList = '';

		// pobranie nazw usuwanych rekordow
		$sSql = "SELECT id, page_id, name
				 		 FROM ".$aConfig['tabls']['prefix']."products
					 	 WHERE id IN  (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method


	/**
	 * Metoda usuwa produkt o podanym Id
	 *
	 * @param	integer	$iId	- Id produktu do usuniecia
	 * @return	mixed
	 */
	function deleteItem($iId) {
		global $aConfig;
		// pobranie obrazkow
		$aImages =& $this->getImages($iId);
		// usuwanie produktu
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products
							 WHERE id = ".$iId;
		if (($iRes = Common::Query($sSql)) === false) {
			return false;
		}
			else {
		 // usuwanie zdjec i plikow produktu
			$this->deleteImages($aImages);
			$this->deleteShadow($iId);
		 return 1;
			}
			//}*/
		return 1;
	} // end of deleteItem() method


	/**
	 * Metoda usuwa zdjecia produktu
	 *
	 * @param	integer	$iId	- Id produktu
	 * @param	string	$sExt	- rozszerzenie miniaturki, sredniego i duzego zdjecia
	 * @return	void
	 */
	function deleteImages($aImages) {
		global $aConfig;

		if(!empty($aImages)){
			foreach($aImages as $aImg){
				if (file_exists($aConfig['common']['client_base_path'].$aConfig['common']['photo_dir'].'/'.$aImg['directory'].'/'.$aImg['photo'])){
					@unlink($aConfig['common']['client_base_path'].$aConfig['common']['photo_dir'].'/'.$aImg['directory'].'/'.$aImg['photo']);
				}
				if (file_exists($aConfig['common']['client_base_path'].$aConfig['common']['photo_dir'].'/'.$aImg['directory'].'/__t_'.$aImg['photo'])){
					@unlink($aConfig['common']['client_base_path'].$aConfig['common']['photo_dir'].'/'.$aImg['directory'].'/__t_'.$aImg['photo']);
				}
				if (file_exists($aConfig['common']['client_base_path'].$aConfig['common']['photo_dir'].'/'.$aImg['directory'].'/__b_'.$aImg['photo'])){
					@unlink($aConfig['common']['client_base_path'].$aConfig['common']['photo_dir'].'/'.$aImg['directory'].'/__b_'.$aImg['photo']);
				}
			}
		}
	} // end of deleteImage() method


/**
	 * Metoda tworzy formularz dodawania / edycji zdjec akapitu
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iPId	- I strony akapitu
	 * @param		integer	$iId	ID edytowanego akapitu
	 * @param		bool		$bAddPhoto	- true: zwieksz liczbe zdjec
	 */
	function AddEditImages(&$pSmarty, $iPId, $iId, $bAddPhoto=false) {
		global $aConfig;

		$aData = array();
		$aOrderBy = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie zdjec
		$sSql = "SELECT id, directory, photo, photo AS name, description, order_by, author
						 FROM ".$aConfig['tabls']['prefix']."products_images
						 WHERE product_id = ".$iId."
					 	 ORDER BY order_by, id";
		$aImgs =& Common::GetAll($sSql);

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['images_header'],
											 $this->getItemName($iId));

		$pForm = new FormTable('images', $sHeader, array('action'=>phpSelf(array('pid' => $iPId, 'id' => $iId)), 'enctype'=>'multipart/form-data'), array('col_width'=>135), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_images');

		

		// maksymalna suma rozmiarow przesylanych zdjec
		$pForm->AddRow($aConfig['lang']['common']['max_img_upload_size'], getMaxUploadSize(), '', array(), array('class'=>'redText'));

		// dopuszczalne formaty zdjec
		$pForm->AddRow($aConfig['lang']['common']['img_available_exts'], implode(', ', $aConfig['common']['img_extensions']), '', array(), array('class'=>'redText'));

		$iImages = 2;
		/*if (!empty($_POST['desc_image'])) {
    	$iImages = count($_POST['desc_image']);
		}
		elseif (!empty($aImgs)) {
    	$iImages = count($aImgs) + 1;
		}
		else {
			$iImages = $aConfig['default']['photos_no'];
		}
		if ($iImages < $aConfig['default']['photos_no']) {
			$iImages = $aConfig['default']['photos_no'];
		}
		if ($bAddPhoto) {
			// wybranmo dodanie kolejnego zdjecia
			$iImages += $aConfig['default']['photos_no'];
		}*/
		// przygotwanie tablicy do ustalania kolejnosci wyswietlania zdjec
		// dolaczenie klasy Images
		include_once('ProductImages.class.php');
		$oImages = new ProductImages($iPId,
													$iId,
													$this->aSettings,
													$this->sDir,
													'product_id',
													'products_images');
		$aOrderBy =& $oImages->getOrderBy(count($aImgs));

		for ($i = 0; $i < $iImages; $i++) {
			if (isset($aImgs[$i])) {
				$pForm->AddImage('image['.$i.'_'.$aImgs[$i]['id'].']', $aConfig['lang'][$this->sModule]['image'].' '.($i+1), $aImgs[$i], $aOrderBy);
			}
			else {
				$pForm->AddImage('image['.$i.'_0]', $aConfig['lang'][$this->sModule]['image'].' '.($i+1));
			}
		}
		/*$pForm->AddRow('&nbsp;', '<div class="existingPhoto">'.$pForm->GetInputButtonHTML('add_photo', $aConfig['lang']['common']['button_add_photo'], array('onclick'=>'GetObject(\'do\').value=\'add_photo\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'</div>');*/


		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEditImages() function


	/**
	 * Metoda pobiera i zwraca produkt o podanym Id
	 *
	 * @param	integer	$iId	- Id produktu
	 * @return	array ref
	 */
	function &getItem($iId) {
		global $aConfig;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id = ".$iId;
		return Common::GetRow($sSql);
	} // end of getItem() method
	
	function &getPublishers(){
		global $aConfig;
		// pobranie wszystkich serii
			$sSql = "SELECT id as value, name as label
							 FROM ".$aConfig['tabls']['prefix']."products_publishers
							 ORDER BY name";
			$aItems= Common::GetAll($sSql);
			foreach($aItems as $iKey => $aItem){
				$aItems[$iKey]['label']=substr($aItem['label'],0,36);
			}
			return array_merge(array(
		array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])
		),$aItems);
	}
		
	function getImages($iId){
		global $aConfig;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products_images
						 WHERE product_id = ".$iId;
		return Common::GetAll($sSql);
	}
	
	/**
   * Funkcja tworzaca na podstawie przekazanego do niej Id strony
   * jej pelna sciezke od korzenia do niej
   *
   * @param	integer	$iId	- ID strony do ktorej ma byc tworzona sciezka
   * @param	integer	$iLangId	- Id wersji jezykowej
   * @return	array	- uporzadkowana tablica ze sciezka do strony
   */
  function getPathIDs($iId, $iLangId) {
	global $aConfig;
	$aPath		= array();
		
  	$sSql = "SELECT A.id, IFNULL(A.parent_id, 0) AS parent_id
  					 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
  					 WHERE A.id = ".$iId." AND
  					 			 A.language_id = ".$iLangId;
  	$aItem =& Common::GetRow($sSql);

  	while (intval($aItem['parent_id']) > 0) {
  		$sSql = "SELECT id, IFNULL(parent_id, 0) AS parent_id
  					 FROM ".$aConfig['tabls']['prefix']."menus_items
  					 WHERE id = ".$aItem['parent_id']." AND
  					 			 language_id = ".$iLangId;
	  	$aItem =& Common::GetRow($sSql);
	  	$aPath[] = $aItem['id'];
  	}
	return array_reverse($aPath);
  } // end getPathItems() function
	
  function existExtraCategory($iCategory,$iProduct){
  	global $aConfig;
		$sSql="SELECT count(*)
					 FROM ".$aConfig['tabls']['prefix']."products_extra_categories
					 WHERE page_id = ".$iCategory." AND product_id = ".$iProduct;
		return (Common::GetOne($sSql) > 0);
  }
  
  function existBookWithISBN($sISBN,$iId=0) {
		global $aConfig;
		
		$sSql = "SELECT count(id)
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE isbn_plain = '".$sISBN."'".
						($iId>0?' AND id <> '.$iId:'');	
		return (Common::GetOne($sSql) > 0);
	} // end of existBookWithISBN() function
	
function GetNaviTreeElem($sName, $aOptions=array()){
	global $aConfig;
	$sHtml = '';
		if (!empty($aOptions)) {
			foreach ($aOptions as $mKey => $aOAttribs) {
				$sHtml .= '<li><a href="javascript:void(0)" onclick="NaviSetFilterValue('.$aOAttribs['value'].')">'.$aOAttribs['label'].'</a>';
				
				if(isset($aOAttribs['items'])){
					$sHtml .= '<ul>';
					$sHtml .= $this->GetNaviTreeElem($sName, $aOAttribs['items']);
					$sHtml .= '</ul>';
				}
				$sHtml .= '</li>';
			}
		}
		return $sHtml;
	}
	
	function GetNaviTree() {
	global $aConfig;

		$sHtml='
		<script>
		
			$(document).ready(function(){
				$("#show_navitree").click(function() {
					$("#navi").dialog(\'open\');
					window.open("ajax/NaviPopup.php",\'\',\'menubar=0,location=0,scrollbars=1,toolbar=0,width=500,height=300\');
				});
			});
		</script>';
//		$aOptions = $this->getProductsCategoriesChTree(Common::getProductsMenu($this->iLangId));
//		$sHtml .= '<div id="navi"><div style="width:378px; height:355px; overflow:auto;">';
//		$sHtml .= '<ul id="categories_navi">';
//		$sHtml .= $this->GetNaviTreeElem($sName, $aOptions, $mSelected);
//
//		$sHtml .= '</ul></div></div>';
	//	dump($sHtml);
		return $sHtml;
	} // end of GetNaviTree() method
	



	/**
	 * Metoda tworzy formularz tworzenia / edycji pakietu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id pakietu
	 * @param	bool	$bAddProduct	- true: zwieksz liczbe pol
	 */
	function AddEditPacket(&$pSmarty, $iId=0, $bAddProduct=false) {
		global $aConfig;
				
		$aData = array();
		$aIdAttribs = array('maxlength'=>10, 'style'=>'width: 50px;');
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		$aExtraCat=$this->getProductsCategories(Common::getProductsMenu($this->iLangId),'');
		$aCats=array();
		$aData['legal_status_date']='00-00-0000';
		$aData['shipment_date']='00-00-0000';
		if ($iId > 0) {
			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT A.*, A.published old_published, A.page_id curr_page_id, 
								DATE_FORMAT(legal_status_date, '".$aConfig['common']['cal_sql_date_format']."')
											AS legal_status_date, 
								DATE_FORMAT(shipment_date, '".$aConfig['common']['cal_sql_date_format']."')
								AS shipment_date,	B.name AS publisher
							 FROM ".$aConfig['tabls']['prefix']."products A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
							 ON A.publisher_id=B.id
							 WHERE A.id = ".$iId;
			$aData =& Common::GetRow($sSql);
			$sName = $aData['plain_name'];
			if(empty($aData['legal_status_date'])){
				$aData['legal_status_date'] = '00-00-0000';
			}
			if(empty($aData['shipment_date'])){
				$aData['shipment_date'] = '00-00-0000';
			}
			$sName = $aData['plain_name'];
			$aData['series']=$this->getProductSeries($iId);
			$aCats=$this->getExtraCategories($iId);
			// produkty pakietu
			$this->getPacketProducts($iId, $aData);
			// tagi
			$aData['tags'] = $this->getProductTags($iId);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
			if(isset($aData['extra_categories'])){
				$aCats=$aData['extra_categories'];
			}
		}
		
		
		$sCurrentExtraCats='';
		foreach($aExtraCat as $aCat){
			if(in_array($aCat['value'],$aCats)){
				$sCurrentExtraCats.=getPagePath($aCat['value'],$this->iLangId,true,false,false)."<br />\n";
			}
		}
		
		if ($iId > 0) {
			$sHeader = sprintf($aLang['packet_header_1'], trimString($sName, 50), getPagePath((double) $aData['curr_page_id'], $this->iLangId, true));
		}
		else {
			$sHeader = $aLang['packet_header_0'];
		}
		
		$pForm = new FormTable('products', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), $aConfig['common']['js_validation']);
		if ($iId > 0) {
			$pForm->AddHidden('do', 'update_packet');
			$pForm->AddHidden('curr_page_id', $aData['curr_page_id']);
			$pForm->AddHidden('old_published', $aData['old_published']);
			// atrybuty pola Id - wylaczona mozliwosc edycji
			$aIdAttribs['readonly'] = 'readonly';
		}
		else {
			$pForm->AddHidden('do', 'insert_packet');
		}		
		
		$pForm->AddSelect('page_id',$aConfig['lang'][$this->sModule]['main_category'], array('style'=>'width:300px;'), $this->getTopProductsCategories(), $aData['page_id']);

		// obecne dodatkowe kategorie
		if(!empty($sCurrentExtraCats)){
			$pForm->AddRow($aConfig['lang'][$this->sModule]['current_extra_categories'], $sCurrentExtraCats);
		}
		
		// drzewo kategorii
		$pForm->AddRow($aConfig['lang'][$this->sModule]['extra_categories'],
			'<div style="height:300px; overflow:auto;">'.
			Form::GetCheckTree('extra_categories[]',array(),$this->getProductsCategoriesChTree(Common::getProductsMenu($this->iLangId)),$aCats)
			.'</div>'
			);
		// nazwa
		$pForm->AddText('name', $aLang['name'], $aData['name'], array('maxlength'=>500, 'style'=>'width: 450px;'), '', 'text');
		$pForm->AddText('name2', $aConfig['lang'][$this->sModule]['name2'], $aData['name2'], array('maxlength'=>500, 'style'=>'width: 450px;'), '', 'text',false);
			
		// tagi
		//$pForm->AddTextArea('tags', $aConfig['lang'][$this->sModule]['tags'], $aData['tags'], array('rows'=>'8', 'style'=>'width: 450px;'), 1024, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);
		// tagi
		$pForm->AddRow($aConfig['lang'][$this->sModule]['new_tag'],
									 $pForm->GetTextHtml('new_tag',$aConfig['lang'][$this->sModule]['new_tag'], $aData['new_tag'],array('id'=>'tags_aci','style'=>'width:300px;','maxlength'=>64),'','text' ,false).'&nbsp;&nbsp;'.
									 $pForm->GetInputButtonHTML('add_tag',$aConfig['lang'][$this->sModule]['add_tag_button'],array('id'=>'add_tag_button'),'button'));
		$pForm->AddTextArea('tags', $aConfig['lang'][$this->sModule]['tags'], $aData['tags'], array('id'=>'tags_list','rows'=>'8', 'style'=>'width: 450px;'), 512, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);
		
		
		// PRODUKTY WCHODZACE W SKLAD PAKIETU
		$pForm->AddMergedRow($aLang['packet_products_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		
		$iProducts = 0;
		if (!empty($_POST['product'])) {
    	$iProducts = count($_POST['product']);
		}
		elseif (!empty($aData['product'])) {
    	$iProducts = count($aData['product']);
		}
		else {
			$iProducts = $this->iDefaultProductsNo;
		}
		if ($iProducts < $this->iDefaultProductsNo) {
			$iProducts = $this->iDefaultProductsNo;
		}
		if ($bAddProduct) {
			// wybranmo dodanie kolejnego produktu
			$iProducts += 1;
		}
		
		// poranie listy dostepnych w serwisie kategorii
		$aCategories =& $this->getProductsCategories(Common::getProductsMenu($this->iLangId), $aConfig['lang']['common']['choose']);
	
		$sJS='
					<script>
						$(document).ready(function(){
						';
		for ($i = 0; $i < $iProducts; $i++) {
			$bRequired = $i <= 1;
			$iNo = ' '.($i + 1);
			
			$pForm->AddMergedRow($aLang['product'].' '.($iNo), array('class'=>'mergedLight', 'style'=>'padding-left: 160px'));

				// produkt
						$pForm->AddText('product_name['.$i.']',$aConfig['lang'][$this->sModule]['product_name'],$aData['product_name'][$i],array('style'=>'width:300px;'),'','text',false);
		
				//$pForm->AddText('product_isbn['.$i.']',$aConfig['lang'][$this->sModule]['product_isbn'],$aData['product_isbn'][$i],array('style'=>'width:300px;'),'','text',false);
				$pForm->AddText('product_author['.$i.']',$aConfig['lang'][$this->sModule]['product_author'], $aData['product_author'][$i],array('style'=>'width:300px;','maxlength'=>255),'','text' ,false);
				$pForm->AddText('product_publisher['.$i.']',$aConfig['lang'][$this->sModule]['product_publisher'], $aData['product_publisher'][$i],array('id'=>'publisher_aci_'.$i,'style'=>'width:300px;','maxlength'=>255),'','text' ,false);
				$sJS.='
								$("#publisher_aci_'.$i.'").autocomplete("ajax/GetPublishers.php",{ minChars: 2, max: 100, matchContains: true, cacheLength: 1, matchSubset: false });
				';
		
				// przycisk wyszukaj
				$pForm->AddRow('&nbsp;', '<div style="margin-left: 225px;">'.
											$pForm->GetInputButtonHTML('search'.$i, $aConfig['lang'][$this->sModule]['button_search'],array('onclick'=>'document.getElementById(\'do\').value=\'add_product\'; this.form.submit();'),'button')
											.'</div>');
		
				// produkt
				$pForm->AddSelect('product['.$i.']', $aConfig['lang'][$this->sModule]['product'], array('style'=>'width:300px;','onchange' => 'document.getElementById(\'do\').value=\'add_product\'; this.form.submit();'), $this->getProducts('packet',(($iId>0 && !empty($aData['product'][$i]) && empty($aData['product_name'][$i]))?$aData['product'][$i]:$aData['product_name'][$i]),$aData['product_author'][$i],$aData['product_publisher'][$i],true),$aData['product'][$i],'',$bRequired);
			
				if (isset($aData['product'][$i]) && !empty($aData['product'][$i])) {
					// dotychczasowa cena produktu
					$fPrice = $this->getProductAttrib($aData['product'][$i], 'price_brutto');
					$fPriceN = $this->getProductAttrib($aData['product'][$i], 'price_netto');
					// VAT
					$iVAT = $this->getProductAttrib($aData['product'][$i], 'vat');
					$pForm->AddRow($aLang['price_brutto'], Common::formatPrice($fPrice).' '.$aConfig['lang']['common']['currency'], '', array(), array('class'=>'redText'));
					// ukryte pole z cena
					$pForm->AddHidden('price_netto['.$i.']', $fPriceN);
					$pForm->AddHidden('price_brutto['.$i.']', $fPrice);
					$pForm->AddHidden('vat['.$i.']', $iVAT);
					
					// rabat w procentach
					$pForm->AddText('discount['.$i.']', $aLang['discount'], isset($aData['discount'][$i]) && is_numeric($aData['discount'][$i]) ? Common::formatPrice3($aData['discount'][$i]) : '', array('maxlength'=>5, 'style'=>'width: 50px;'), $aLang['discount'].$iNo, 'ufloat', true);
				}
			
				
				
					/************************************/
		
		}
		$pForm->AddRow('&nbsp;', '<div class="existingPhoto">'.$pForm->GetInputButtonHTML('add_product_field', $aLang['button_add_product'], array('onclick'=>'document.getElementById(\'do\').value=\'add_product_field\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'</div>');
		

		// opis produktu
		$pForm->AddWYSIWYG('description', $aLang['description'], $aData['description']);
		
		// STATUS PRODUKTU
		$pForm->AddMergedRow($aLang['status_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		
		// status

		// status
		$aStatuses = array(
		array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true;'),
		array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['status_0'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true;'),
		array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['status_2'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true;'),
		array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['status_3'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=false;')
		);
		$pForm->AddRadioSet('prod_status', $aConfig['lang'][$this->sModule]['prod_status'], $aStatuses, $aData['prod_status'], '', true, false);

		if($aData['prod_status'] == '3'){
			$aShipmentDateAttribs = array('style'=>'width: 75px;');
		} else {
			$aShipmentDateAttribs = array('style'=>'width: 75px;','disabled'=>'disabled');
		}
		$pForm->AddText('shipment_date', $aConfig['lang'][$this->sModule]['shipment_date'], $aData['shipment_date'], $aShipmentDateAttribs, '', 'date', false);
		// status
        $pShipmentTime = new ShipmentTime();

        $aShipments = array(
            array('value' => '0', 'label' => $pShipmentTime->getShipmentTime($iId, 0)),
            array('value' => '1', 'label' => $pShipmentTime->getShipmentTime($iId, 1)),
            array('value' => '2', 'label' => $pShipmentTime->getShipmentTime($iId, 2)),
            array('value' => '3', 'label' => $pShipmentTime->getShipmentTime($iId, 3)),
            array('value' => '4', 'label' => $pShipmentTime->getShipmentTime($iId, 4)),
            array('value' => '5', 'label' => $pShipmentTime->getShipmentTime($iId, 5)),
            array('value' => '6', 'label' => $pShipmentTime->getShipmentTime($iId, 6)),
            array('value' => '7', 'label' => $pShipmentTime->getShipmentTime($iId, 7)),
            array('value' => '8', 'label' => $pShipmentTime->getShipmentTime($iId, 8)),
            array('value' => '9', 'label' => $pShipmentTime->getShipmentTime($iId, 9)),
            array('value' => '10', 'label' => $pShipmentTime->getShipmentTime($iId, 10)),
        );
		$pForm->AddRadioSet('shipment_time', $aConfig['lang'][$this->sModule]['shipment_time'], $aShipments, $aData['shipment_time'], '', true, false);
		
		
		// STAN PUBLIKACJI
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['publ_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));

		// status publikacji

		$pForm->AddCheckBox('published', $aConfig['lang'][$this->sModule]['published'], array(), '', !empty($aData['published']), false);

				// status

		
		if(!empty($aData['modified'])){
			$pForm->AddRow($aConfig['lang'][$this->sModule]['modified'],$aData['modified']);
		}
		
		if(!empty($aData['modified_by'])){
			$pForm->AddRow($aConfig['lang'][$this->sModule]['modified_by'],$aData['modified_by']);
		}
		
		// w boksie na glownej stronie
		//$pForm->AddCheckBox('packet_in_box', $aConfig['lang'][$this->sModule]['packet_in_box'], array(), '', !empty($aData['packet_in_box']), false);
				
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array()).'\');'), 'button'));
		
		$sJS.='
			$("#add_tag_button").click(function () {
    		var sCurTags = $("#tags_list").val();
    		var sNewTag = $("#tags_aci").val();
    		if(sNewTag != ""){
    			if(sCurTags != ""){
    				sCurTags = sCurTags+",";
					}
      		$("#tags_list").val(sCurTags+sNewTag);
      		$("#tags_aci").val("");
      	}
   		});
						});
					</script>';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	} // end of AddEditPacket() function
	
	/**
	 * Metoda dodaje do bazy danych nowy pakiet
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function InsertPacket(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditPacket($pSmarty);
			return;
		}
		// sprawdzenie tagow
		if (trim($_POST['tags']) != '' && !$this->checkTags($_POST['tags'])) {
			// nieprawidlowo wypelnione pole tagi
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
			$this->AddEditPacket($pSmarty);
			return;
		}
		
		if(isset($_POST['published']) && $_POST['page_id'] == $aConfig['import']['unsorted_category']) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_unsorted_err'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEditPacket($pSmarty);
			return;
		}
	

		// przeliczanie cen
		$this->calculatePacketPrices();
		Common::BeginTransaction();
		
		$aValues = array(
			'page_id' => (double) $_POST['page_id'],
			'name' => $_POST['name'],
			'name2' => !empty($_POST['name2'])?$_POST['name2']:'NULL',
			'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($_POST['name'], ' '))),
			'short_description' => strip_tags(br2nl(trimString($_POST['description'],200),' ')),
			'description' => $_POST['description'],
			'price_netto' => $_POST['packet_price_netto'],
			'price_brutto' => $_POST['packet_price_brutto'],
			'vat' => 0,
			'shipment_time' => isset($_POST['shipment_time'])?$_POST['shipment_time']:'1',
			'shipment_date'	=>	($_POST['prod_status']=='3' && !empty($_POST['shipment_date'])&&($_POST['shipment_date']!='00-00-0000'))?FormatDate($_POST['shipment_date']):'NULL',
			'prod_status' => isset($_POST['prod_status'])?$_POST['prod_status']:'0',
			'published' => isset($_POST['published']) ? '1' : '0',
			'created' => 'NOW()',
			'source' => '0',
			'packet' => '1',
			'created_by' => $_SESSION['user']['name']
		);
		$iPublished = $aValues['published'];
		if($_POST['prod_status']=='3'){
			$aValues['shipment_time'] = '0';
		}
		// dodanie
		$bIsErr = ($iId = Common::Insert($aConfig['tabls']['prefix']."products", $aValues)) === false;
		if ($mId != '') {
			$iId = $mId;
		}

		if (!$bIsErr) {
			if (trim($_POST['tags']) != '') {
				// dodanie tagow
				$bIsErr = !$this->addTags($iId, explode(',', $_POST['tags']));
			}
		}
		
		if (!$bIsErr) {
			// dodanie produktow skladajacych sie na pakiet
			$bIsErr = !$this->addPacketProducts($iId);
		}
		
		$aValues=array(
			'product_id' => $iId,
			'discount' => 100,
			'price_brutto' => $_POST['packet_promo_price_brutto'],
			'price_netto' => $_POST['packet_promo_price_netto'],
			'discount_value' => $_POST['packet_discount_money']
		);
		if (Common::Insert($aConfig['tabls']['prefix']."products_tarrifs",
											 $aValues,'',
											 false) === false) {
			$bIsErr = true;
		}
		
	
		$_POST['extra_categories'][]=$_POST['page_id'];
		// dodanie dodatkopwych kategorii
		if(!$bIsErr && !empty($_POST['extra_categories'])){
			foreach($_POST['extra_categories'] as $iCatId){
				if($iCatId != $aConfig['import']['unsorted_category'] && !$this->existExtraCategory($iCatId,$iId)){
					$aValues=array(
						'product_id' => $iId,
						'page_id' => $iCatId
					);
					if ((Common::Insert($aConfig['tabls']['prefix']."products_extra_categories",$aValues,'',false)) === false) {
						$bIsErr = true;
					}
					// pobranie kategorii - rodzicow wybranej kategorii
					$aParents=$this->getPathIDs($iCatId,$_SESSION['lang']['id']);
					// dodanie mapowan w gore drzewa lokalnego
					if(!empty($aParents)){
						foreach($aParents as $iParent){
							if(!$this->existExtraCategory($iParent,$iId)){
								$aValues=array(
									'product_id' => $iId,
									'page_id' => $iParent
								);
								if ((Common::Insert($aConfig['tabls']['prefix']."products_extra_categories",$aValues,'',false)) === false) {
									$bIsErr = true;
								}
							}
						}
					}
				}
			}
		}
		
		if($iPublished == '1') {
			if($this->addShadow($iId)==false){
				$bIsErr=true;
			}
		}
		
		if (!$bIsErr) {
			// rekord zostal dodany
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['packet_add_ok'],
											getPagePath((double) $_POST['page_id'], $this->iLangId, true),
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			// rekord nie zostal dodany, 
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['packet_add_err'],
											getPagePath((double) $_POST['page_id'], $this->iLangId, true),
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEditPacket($pSmarty);
		}
	} // end of InsertPacket() funciton
	
	
	/**
	 * Metoda aktualizuje w bazie danych pakiet
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id pakietu
	 * @return	void
	 */
	function UpdatePacket(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditPacket($pSmarty, $iId);
			return;
		}
		// sprawdzenie tagow
		if (trim($_POST['tags']) != '' && !$this->checkTags($_POST['tags'])) {
			// nieprawidlowo wypelnione pole tagi
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
			$this->AddEditPacket($pSmarty, $iId);
			return;
		}
		
		if(isset($_POST['published']) && $_POST['page_id'] == $aConfig['import']['unsorted_category']) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_unsorted_err'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEditPacket($pSmarty, $iId);
			return;
		}
		
		// pobranie aktualnego stanu publikacji
		$aCurrent =& $this->getItem($iId);

		// przeliczanie cen
		$this->calculatePacketPrices();
		Common::BeginTransaction();
		
		$aValues = array(
			'page_id' => (double) $_POST['page_id'],
			'name' => $_POST['name'],
			'name2' => !empty($_POST['name2'])?$_POST['name2']:'NULL',
			'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($_POST['name'], ' '))),
			'short_description' => strip_tags(br2nl(trimString($_POST['description'],200),' ')),
			'description' => $_POST['description'],
			'price_netto' => $_POST['packet_price_netto'],
			'price_brutto' => $_POST['packet_price_brutto'],
			'vat' => 0,
			'shipment_time' => isset($_POST['shipment_time'])?$_POST['shipment_time']:'1',
			'prod_status' => isset($_POST['prod_status'])?$_POST['prod_status']:'0',
			'shipment_date'	=>	($_POST['prod_status']=='3' && !empty($_POST['shipment_date'])&&($_POST['shipment_date']!='00-00-0000'))?FormatDate($_POST['shipment_date']):'NULL',
			'published' => isset($_POST['published']) ? '1' : '0',
			'packet' => '1'
		);
		if($_POST['prod_status']=='3'){
			$aValues['shipment_time'] = '0';
		}
		
		// aktualizacja
		$bIsErr = Common::Update($aConfig['tabls']['prefix']."products", $aValues, "id = ".$iId) === false;

		if($aCurrent['published'] != $aValues['published']){
			if($aValues['published'] == '1'){
				if($this->publishTags($iId)==false){
					$bIsErr=true;
				}
			} else {
				if($this->unpublishTags($iId)==false){
					$bIsErr=true;
				}
			}
		}
		
		if (!$bIsErr) {
			// aktualizacja tagow
			$bIsErr = !$this->addTags($iId, trim($_POST['tags']) != '' ? explode(',', $_POST['tags']) : array(), true);
		}
		
		if (!$bIsErr) {
			// dodanie produktow skladajacych sie na pakiet
			$bIsErr = !$this->addPacketProducts($iId, true);
		}
		
		$aValues=array(
			'discount' => 100,
			'price_brutto' => $_POST['packet_promo_price_brutto'],
			'price_netto' => $_POST['packet_promo_price_netto'],
			'discount_value' => $_POST['packet_discount_money']
		);
		if (Common::Update($aConfig['tabls']['prefix']."products_tarrifs",
											 $aValues,"isdefault = '0' AND product_id = ".$iId) === false) {
			$bIsErr = true;
		}
		
	//usuniecie poprzednich dodatkopwych kategorii
		if (!$bIsErr){
			$sSql="DELETE FROM ".$aConfig['tabls']['prefix']."products_extra_categories
						WHERE product_id = ".$iId;
			$bIsErr=(Common::Query($sSql)===false);
		}
	// dodanie dodatkopwych kategorii
	$_POST['extra_categories'][]=$_POST['page_id'];
		// dodanie dodatkopwych kategorii
		if(!$bIsErr && !empty($_POST['extra_categories'])){
			foreach($_POST['extra_categories'] as $iCatId){
				if($iCatId != $aConfig['import']['unsorted_category'] && !$this->existExtraCategory($iCatId,$iId)){
					$aValues=array(
						'product_id' => $iId,
						'page_id' => $iCatId
					);
					if ((Common::Insert($aConfig['tabls']['prefix']."products_extra_categories",$aValues,'',false)) === false) {
						$bIsErr = true;
					}
					// pobranie kategorii - rodzicow wybranej kategorii
					$aParents=$this->getPathIDs($iCatId,$_SESSION['lang']['id']);
					// dodanie mapowan w gore drzewa lokalnego
					if(!empty($aParents)){
						foreach($aParents as $iParent){
							if(!$this->existExtraCategory($iParent,$iId)){
								$aValues=array(
									'product_id' => $iId,
									'page_id' => $iParent
								);
								if ((Common::Insert($aConfig['tabls']['prefix']."products_extra_categories",$aValues,'',false)) === false) {
									$bIsErr = true;
								}
							}
						}
					}
				}
			}
		}
		
	if (!$bIsErr) {
			// stan publikacji
			$_POST['published'] = (int) $_POST['published'];
			$_POST['old_published'] = (int) $_POST['old_published'];

	// tabela cieni
	// produkt obecnie nie jest opublikowany - zmiana na opublikowany - dodanie cieia
			if($_POST['old_published'] == 0) {
				if($_POST['published'] == 1) {
					if($this->existShadow($iId)) {
						if($this->updateShadow($iId)===false){
							$bIsErr=true;
						}
					} else {
						if($this->addShadow($iId)===false){
							$bIsErr=true;
						}
					}
				}
			} else {
		// produkt jest opublikowany - zmiana na nieopublikowany - usuniecie cienia
				if($_POST['published'] == 0) {
					if($this->deleteShadow($iId)===false){
						$bIsErr=true;
					}
				} else {
		// brak zmiany stany publikacji - aktualizacja cienia
					if($this->existShadow($iId)) {
						if($this->updateShadow($iId)===false){
							$bIsErr=true;
						}
					} else {
						if($this->addShadow($iId)===false){
							$bIsErr=true;
						}
					}
				}	
			}
		}
		
		if (!$bIsErr) {
			// rekord zostal zaktualizowany
			SetModification('products',$iId);
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['packet_edit_ok'],
											getPagePath((double) $_POST['page_id'], $this->iLangId, true),
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// rekord nie zostal zaktualizowany
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['packet_edit_err'],
											getPagePath((double) $_POST['page_id'], $this->iLangId, true),
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEditPacket($pSmarty, $iId);
		}
	} // end of UpdatePacket() funciton
	
		
	/**
	 * Metoda przelicza ceny pakietu
	 * 
	 * @return	void
	 */
	function calculatePacketPrices() {
		$_POST['packet_discount_money'] = 0;
		$_POST['packet_price_brutto'] = 0;
		$_POST['packet_price_netto'] = 0;
		$_POST['packet_promo_price_brutto'] = 0;
		$_POST['packet_promo_price_netto'] = 0;
		
		foreach ($_POST['product'] as $iKey => $iProdId) {
			$iProdId = (double) $iProdId;
			if ($iProdId > 0) {
				// cena brutto
				$_POST['price_brutto'][$iKey] = Common::formatPrice2($_POST['price_brutto'][$iKey]);
				// vat
				$_POST['vat'][$iKey] = (int) $_POST['vat'][$iKey];
				
				// okreslenie ceny netto
				$_POST['price_netto'][$iKey] = $_POST['vat'][$iKey] > 0 ? Common::formatPrice2($_POST['price_brutto'][$iKey] / (1 + ($_POST['vat'][$iKey] / 100))) : $_POST['price_brutto'][$iKey];
				// rabat
				$_POST['discount'][$iKey] = Common::formatPrice2($_POST['discount'][$iKey]);
				$_POST['discount_money'][$iKey] = 0;
				if ($_POST['discount'][$iKey] > 0) {
					// obliczenie wartosci brutto rabatu
					$_POST['discount_money'][$iKey] = Common::formatPrice2($_POST['price_brutto'][$iKey] * ($_POST['discount'][$iKey] / 100));
					// cena promocyjna brutto
					$_POST['promo_price_brutto'][$iKey] = $_POST['price_brutto'][$iKey] - $_POST['discount_money'][$iKey];
					// cena promocyjna netto
					$_POST['promo_price_netto'][$iKey] = $_POST['vat'][$iKey] > 0 ? Common::formatPrice2($_POST['promo_price_brutto'][$iKey] / (1 + ($_POST['vat'][$iKey] / 100))) : $_POST['promo_price_brutto'][$iKey];
					
					// cena promocyjna pakietu
					$_POST['packet_promo_price_brutto'] += $_POST['promo_price_brutto'][$iKey];
					$_POST['packet_promo_price_netto'] += $_POST['promo_price_netto'][$iKey];
					$_POST['packet_discount_money'] += $_POST['discount_money'][$iKey];
				}
				else {
					$_POST['packet_promo_price_brutto'] += $_POST['price_brutto'][$iKey];
					$_POST['packet_promo_price_netto'] += $_POST['price_netto'][$iKey];
				}
				$_POST['packet_price_brutto'] += $_POST['price_brutto'][$iKey];
				$_POST['packet_price_netto'] += $_POST['price_netto'][$iKey];
			}
		}
	} // end of calculatePacketPrices() method
		
	/**
	 * Metoda dodaje skladowe produkty pakietu
	 * 
	 * @param	integer	$iId	- id pakietu
	 * @param	bool	$bUpdate	- true: usun dotychczasowe
	 * @return	bool
	 */
	function addPacketProducts($iId, $bUpdate=false) {
		global $aConfig;
		
		if ($bUpdate) {
			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_packets_items
							 WHERE packet_id = ".$iId;
			if (Common::Query($sSql) === false) return false;
		}
		// dodawanie produktow pakietu
		foreach ($_POST['product'] as $iKey => $iProdId) {
			$iProdId = (double) $iProdId;
			if ($iProdId > 0) {
				$aValues = array(
					'packet_id' => $iId,
					'product_id' => $iProdId,
					'price_netto' => $_POST['price_netto'][$iKey],
					'price_brutto' => $_POST['price_brutto'][$iKey],
					'vat' => $_POST['vat'][$iKey],
					'discount' => $_POST['discount'][$iKey],
					'discount_currency' => $_POST['discount_money'][$iKey],
					'promo_price_netto' => $_POST['promo_price_netto'][$iKey],
					'promo_price_brutto' => $_POST['promo_price_brutto'][$iKey],
					'order_by' => '#order_by#'
				);
				// dodanie
				if (Common::Insert($aConfig['tabls']['prefix']."products_packets_items",
													 $aValues,
													 "packet_id = ".$iId,
													 false) === false) return false;
			}
		}
		return true;
	} // end of addPacketProducts() method
	
	/**
	 * Metoda pobiera produkty pakietu o podanym Id
	 * 
	 * @param	integer	$iId	- Id pakietu
	 * @param	array ref	$aData	- tablica z danymi pakietu
	 * @return	array
	 */
	function &getPacketProducts($iId, &$aData) {
		global $aConfig;
		
		$sSql = "SELECT B.product_id, B.discount
						 FROM ".$aConfig['tabls']['prefix']."products_packets_items B
						 JOIN ".$aConfig['tabls']['prefix']."products A
						 ON A.id = B.product_id
						 WHERE B.packet_id = ".$iId."
						 ORDER BY order_by";
		$aItems =& Common::GetAll($sSql);
		foreach ($aItems as $iKey => $aItem) {
			$aData['product'][$iKey] = $aItem['product_id'];
			$aData['discount'][$iKey] = $aItem['discount'];
		}
	} // end of getPacketProducts() method
	
	
	/**
	 * Metoda pobiera pakiety w ktorych wystepuje produkt o Id
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @param	array ref	$aData	- tablica z pakietami produktu
	 * @return	array
	 */
	function &getProductPackets($iId) {
		global $aConfig;
		
		$sSql = "SELECT A.page_id, A.id, A.plain_name
						 FROM ".$aConfig['tabls']['prefix']."products_packets_items B
						 JOIN ".$aConfig['tabls']['prefix']."products A
						 ON A.id = B.packet_id
						 WHERE B.product_id = ".$iId."
						 ORDER BY A.plain_name";
		return Common::GetAll($sSql);
	} // end of getProductPackets() method
	
	
	/**
	 * Zwraca id taga, w razie potrzeby tworzy wpis w tabeli products_tags
	 * @param $sTag - tag
	 * @return int id taga / false
	 */
	function getIdForTag($sTag){
		global $aConfig;
		$sTag=trim($sTag);
		if(!empty($sTag)){
			$sSql ="SELECT id 
							FROM ".$aConfig['tabls']['prefix']."products_tags
							WHERE tag = ".Common::Quote($sTag);
			$iId = intval(Common::GetOne($sSql));
			if($iId > 0){
				return $iId;
			} else {
				$aValues = array(
					'tag' => $sTag,
					'quantity' => 0,
					'page_id' => $_POST['page_id']
				);
				if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_tags",$aValues)) === false){
					return false;
				} else {
					return $iId;
				}
			}
		}
		return false;
	}
	
	/**
	 * Metoda sprawdza poprawosc wypelnienia tagow
	 * 
	 * @param	string	$sTags	- tagi
	 * @return	bool
	 */
	function checkTags() {
		return preg_match('/^[^`~!@#\$%\^&\*\(\)\+=\[\]\{\};:\'"\,\.<\\>\?\/](\s*[^`~!@#\$%\^&\*\(\)\+=\[\]\{\};:\'"\,\.<\\>\?\/])+(,\s*[^`~!@#\$%\^&\*\(\)\+=\[\]\{\};:\'"\,\.<\\>\?\/](\s*[^`~!@#\$%\^&\*\(\)\+=\[\]\{\};:\'"\,\.<\\>\?\/])+)*$/', $_POST['tags']);
	} // end of checkTags() method
	
	/**
	 * Funkcja sprawdza czy do produktu jest przywiązany tag o podanym id
	 * @param $iPId int - id produktu
	 * @param $iTagId int - id tagu
	 * @return bool
	 */
	function existsProductTag($iPId, $iTagId){
		global $aConfig;
		$sSql ="SELECT COUNT(1)
						FROM ".$aConfig['tabls']['prefix']."products_to_tags
						WHERE product_id = ".$iPId."
						AND tag_id = ".$iTagId;
		return (Common::GetOne($sSql) > 0);
	}
	 
	/**
	 * Metoda dodaje tagi produktu
	 * 
	 * @param	integer	$iId	- Id ksiazki
	 * @param	array	$aTags	- tagi
	 * @param	bool	$bUpdate	- true: update
	 * @return	bool
	 */
	function addTags($iId, $aTags, $bUpdate=false) {
		global $aConfig;
		
		if ($bUpdate && !$this->deleteTags($iId)) return false;
		
		foreach ($aTags as $sTag) {
			if (trim($sTag) != '') {
				$aValues = array(
					'product_id' => $iId,
					'tag_id' => $this->getIdForTag($sTag)
				);
				if($aValues['tag_id']>0){
					if($this->existsProductTag($iId,$aValues['tag_id'])) {
						return true;
					}
					if (Common::Insert($aConfig['tabls']['prefix']."products_to_tags",
												 		 $aValues,
												 		 "",
												 		 false) === false) {
						return false;
					}
				} else {
					return false;
				}
			}
		}
		return true;
	} // end of addTags() method
	
	
	/**
	 * Metoda usuwa tagi produktu o podanym Id
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @return	bool
	 */
	function deleteTags($iId) {
		global $aConfig;
		
		// usuniecie poprzednich tagow
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_to_tags
						 WHERE product_id = ".$iId;
		if (Common::Query($sSql) === false) return false;
		return true;
	} // end deleteTags() method
	
	/**
	 * Metoda publikuje tagi produktu o podanym Id
	 * @param $iId - id produktu
	 * @return bool
	 */
	function publishTags($iId){
		global $aConfig;
		
		$sSql = "SELECT tag_id
							FROM ".$aConfig['tabls']['prefix']."products_to_tags
							WHERE product_id = ".$iId;
		$bIsError=false;
		$aTags = Common::GetCol($sSql);
		//dump($aTags);
		if(!empty($aTags)){
			foreach($aTags as $iTag){
				//tag juz istnieje - zwiekszenie liczebnosci
	 			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_tags SET quantity = quantity + 1 WHERE id = ".$iTag;
	 			if(Common::Query($sSql)===false){
	 				$bIsError = true;
	 			}
			}
		}
		return !$bIsError;
	}
	
	/**
	 * Metoda odpublikowuje tagi produktu o podanym id
	 * @param $iId - id produktu
	 * @return bool
	 */
	function unpublishTags($iId){
		global $aConfig;
		
		$sSql = "SELECT tag_id
							FROM ".$aConfig['tabls']['prefix']."products_to_tags
							WHERE product_id = ".$iId;
		$bIsError=false;
		$aTags = Common::GetCol($sSql);
		if(!empty($aTags)){
			foreach($aTags as $iTag){
				$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_tags SET quantity = quantity - 1 WHERE id = ".$iTag;
				if(Common::Query($sSql)===false){
	 				$bIsError = true;
	 			}
			}
		}
		return !$bIsError;
	}
	
} // end of Module Class
?>