<?php
use Ceneo\CeneoDataProvider;

/**
 * Klasa Module do obslugi modulu porównywarki
 * 
 * @author Arkadiusz Golba
 * @date 2014-02-11
 * @copyright (c) 2014, Profit M. Chudy
 */
class Module {

  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID strony zamowien
	var $iPageId;

	// ID wersji jezykowej
	var $iLangId;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;
		
		$this->sErrMsg = '';
		$this->iPageId = getModulePageId('m_zamowienia');
		$this->sModule = $_GET['module'];
		$this->iLangId = $_SESSION['lang']['id'];
		
		$sDo = '';		
		
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu i akcji
    /*
		if ($_SESSION['user']['type'] === 0) {
			// brak uprawnien do modulu, menu, strony
			showPrivsAlert($pSmarty);
			return;
		}
     */
    
    if ($_SESSION['user']['type'] === 0) {
      if (!hasModulePrivileges($_GET['module_id'])){
        showPrivsAlert($pSmarty);
      return;
      }
		}
		
		switch ($sDo) {
			case 'update': $this->Update($pSmarty); break;
						
			default: $this->Edit($pSmarty); break;
		}
	} // end of Module() method
	
	
	/**
	 * Metoda aktualizuje w bazie danych krekord
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id aktualizowanego rekordu
	 * @return	void
	 */
	function Update(&$pSmarty) {
		global $aConfig, $pDbMgr;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Edit($pSmarty);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);

			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Edit($pSmarty);
			return;
		}
		// pobranie anych
    $sSqlDELETE = "DELETE FROM products_comparison_sites LIMIT 10";
    Common::Query($sSqlDELETE);
    // Insert
    if (!empty($_POST['shipment'])) {
      foreach ($_POST['shipment'] as $iShipment => $sTxtShipment) {

        if ((!is_int($iShipment) && $iShipment != 'o') && $iShipment != 'k') {
          $sTxtShipment = Common::formatPrice2($sTxtShipment);
        }

		if ($iShipment === 'k'){
			$sTxtShipment = str_replace(' ', '', $sTxtShipment);
		}

        $aValues = array(
            'shipment_time' => $iShipment,
            'text' => $sTxtShipment
        );
        if (Common::Insert($aConfig['tabls']['prefix']."products_comparison_sites",
                           $aValues,
                           '',
                           false) === false) {
          $bIsErr = true;
          break;
        }
      }
    }

		/** ZAPIS website_ceneo_settings */
		$ceneoDataProvider = new CeneoDataProvider();
		$websiteCeneoSettings = $ceneoDataProvider->getWebsiteCeneoSettings();

		foreach($_POST['website_ceneo_setting'] as $key => $websiteCeneoSetting) {

			$values = [
				'website_id' => $key,
				'minimal_overhead' => Common::formatPrice2($websiteCeneoSetting['minimal_overhead']),
				'price_modifier' => Common::formatPrice2($websiteCeneoSetting['price_modifier']),
				'catalogue_price_substractor' => Common::formatPrice2($websiteCeneoSetting['catalogue_price_substractor']),
				'minimal_opinions' => $websiteCeneoSetting['minimal_opinions'],
				'ignored_shops' => str_replace(' ', '', trim($websiteCeneoSetting['ignored_shops'])),
			];

			if (isset($websiteCeneoSettings[$key])) {

				$pDbMgr->Update('profit24', "website_ceneo_settings", $values, 'website_id = '.$key);

			} else {

				$pDbMgr->Insert('profit24', "website_ceneo_settings", $values);
			}
		}

		/** KONIEC ZAPISU website_ceneo_settings */
			
		if (!$bIsErr) {
			// rekord zostal zaktualizowany
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_ok'], false);
			// dodanie informacji do logow
			AddLog($aConfig['lang'][$this->sModule]['edit_ok'], false);
		}
		else {
			// rekord nie zostal zmieniony, 
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_err']);
			AddLog($aConfig['lang'][$this->sModule]['edit_err']);
		}
		$this->Edit($pSmarty);
	} // end of Update() funciton

	
	/**
	 * Metoda tworzy formularz edycji danych sklepu
	 *
	 * @param		object	$pSmarty
	 */
	function Edit(&$pSmarty) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		// pobranie z bazy danych edytowanego rekordu
		$aData =& $this->getData();
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		$sHeader = $aConfig['lang'][$this->sModule]['header'];
		
		$pForm = new FormTable('seller_data', $sHeader, array('action'=>phpSelf()), array('col_width'=>420), true);
		$pForm->AddHidden('do', 'update');

		// nazwa sprzedawcy
    $pForm->AddMergedRow($aConfig['lang'][$this->sModule]['add_text'], array('class'=>'merged'));

    foreach ($aConfig['lang'][$this->sModule]['shipment'] as $iKeyLang => $sValLang) {

		$required = false;
		$val = $aData['shipment'][$iKeyLang];

		if ($iKeyLang === 't' || $iKeyLang === 'd') {
			$required = true;
			$val = (int)$val;
		}

      $pForm->AddText('shipment['.$iKeyLang.']', $sValLang, $val, array('maxlength'=>512, 'style'=>'width: 712px;'), '', 'text', $required);
    }



		//$aConfig['lang']['m_oferta_produktowa']['shipment_2']
//    $pForm->AddText('shipment[n]', $aConfig['lang']['m_oferta_produktowa']['n'], Common::formatPrice($aData['shipment']['n']), array('id' => 'discount_np', 'class' => 'discount', 'maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');
//    $pForm->AddText('shipment[m]', $aConfig['lang']['m_oferta_produktowa']['m'], Common::formatPrice($aData['shipment']['m']), array('id' => 'discount_np', 'class' => 'discount', 'maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');
//    $pForm->AddText('shipment[p]', $aConfig['lang']['m_oferta_produktowa']['p'], Common::formatPrice($aData['shipment']['p']), array('id' => 'discount_np', 'class' => 'discount', 'maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');
//    $pForm->AddText('shipment[o]', $aConfig['lang']['m_oferta_produktowa']['o'], $aData['shipment']['o'], array('id' => 'discount_np', 'class' => 'discount', 'maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');
//    $pForm->AddText('shipment[k]', $aConfig['lang']['m_oferta_produktowa']['k'], $aData['shipment']['k'], array('maxlength'=>512, 'style'=>'width: 712px;'), '', 'text', false);

		$this->addWebsiteCeneoSettings($pForm);
    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function


	private function addWebsiteCeneoSettings(FormTable $pForm)
	{
		global $aConfig, $pDbMgr;

		$ceneoDataProvider = new CeneoDataProvider();
//		$websites = $ceneoDataProvider->getCeneoWebsites();
		$websites = $pDbMgr->GetAll('profit24', "SELECT * FROM websites WHERE ceneo is not null and ceneo != '' AND ceneo_active = 1 OR code = 'smarkacz'");;
		$websiteCeneoSettings = $ceneoDataProvider->getWebsiteCeneoSettings();

		foreach($websites as $website) {

			$pForm->AddMergedRow($website['name'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
			$pForm->AddText('website_ceneo_setting['.$website['id'].'][minimal_overhead]', $aConfig['lang']['m_oferta_produktowa']['n'], Common::formatPrice($websiteCeneoSettings[$website['id']]['minimal_overhead']), array('id' => 'discount_np', 'class' => 'discount', 'maxlength'=>5, 'style'=>'width: 50px;'), '');
			$pForm->AddText('website_ceneo_setting['.$website['id'].'][price_modifier]', $aConfig['lang']['m_oferta_produktowa']['m'], Common::formatPrice($websiteCeneoSettings[$website['id']]['price_modifier']), array('id' => 'discount_np', 'class' => 'discount', 'maxlength'=>5, 'style'=>'width: 50px;'), '');
			$pForm->AddText('website_ceneo_setting['.$website['id'].'][catalogue_price_substractor]', $aConfig['lang']['m_oferta_produktowa']['p'], Common::formatPrice($websiteCeneoSettings[$website['id']]['catalogue_price_substractor']), array('id' => 'discount_np', 'class' => 'discount', 'maxlength'=>5, 'style'=>'width: 50px;'), '');
			$pForm->AddText('website_ceneo_setting['.$website['id'].'][minimal_opinions]', $aConfig['lang']['m_oferta_produktowa']['o'], $websiteCeneoSettings[$website['id']]['minimal_opinions'], array('id' => 'discount_np', 'class' => 'discount', 'maxlength'=>5, 'style'=>'width: 50px;'), '');
			$pForm->AddText('website_ceneo_setting['.$website['id'].'][ignored_shops]', $aConfig['lang']['m_oferta_produktowa']['k'], $websiteCeneoSettings[$website['id']]['ignored_shops'], array('maxlength'=>512, 'style'=>'width: 712px;'), '', 'text', false);
		}
	}
	
	/**
	 * Metoda pobiera dane sprzedawcy
	 * 
	 * @return	array ref	- tablica z danymi
	 */
	function &getData() {
		global $aConfig;
		
    $aRecordsNew = array();
    
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products_comparison_sites";
		$aRecords = Common::GetAll($sSql);
    
    if (!empty($aRecords)) {
      foreach ($aRecords as $aItem) {
        $aRecordsNew['shipment'][$aItem['shipment_time']] = $aItem['text'];
      }
    }
    return $aRecordsNew;
	} // end of getData method
} // end of Module Class
?>