<?php
namespace omniaCMS\modules\m_oferta_produktowa;

use Admin;
use FormTable;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use View;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 26.09.16
 * Time: 08:47
 */
class Module__oferta_produktowa__publishers_groups implements ModuleEntity, SingleView
{

    /**
     *
     * @var string
     */
    private $sMsg;

    /**
     *
     * @var string
     */
    private $sModule;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    public function __construct(Admin $oClassRepository)
    {

        $this->oClassRepository = $oClassRepository;
        $this->em = $this->oClassRepository->entityManager;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function getMsg()
    {
        return $this->sMsg;
    }

    public function doDefault()
    {
        global $aConfig;
        $aData = array();

        if (!empty($_POST)) {
            $aData =& $_POST;
        }


        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header' => _('Grupy wydawnicze'),
            'refresh' => true,
            'search' => true,
            'items_per_page' => 20,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 2,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'name',
                'content' => _('Nazwa'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '150'
            ),
            array(
                'db_field' => 'created_by',
                'content' => _('Utworzył'),
                'sortable' => true,
                'width' => '100'
            ),
            array(
                'db_field' => 'modified',
                'content' => _('Zmodyfikowano'),
                'sortable' => true,
                'width' => '150'
            ),
            array(
                'db_field' => 'modified_by',
                'content' => _('Zmodyfikował'),
                'sortable' => true,
                'width' => '100'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '40'
            )
        );

        $aColSettings = array(
            'id' => array(
                'show' => false
            ),
            'name' => array(
                'link' => phpSelf(array('do' => 'edit', 'id' => '{id}'))
            ),
            'action' => array(
                'actions' => array('linked', 'edit', 'delete'),
                'params' => array(
                    'linked' => array('ppid' => '{id}', 'action' => 'publishers_to_groups', 'do' => 'default'),
                    'edit' => array('id' => '{id}'),
                    'delete' => array('id' => '{id}'),
                ),
            )
        );


        include_once('View/View.class.php');
        $oView = new View(_('publishers_groups'), $aHeader, $aAttribs);

        $aCols = array('PPG.id', 'PPG.name', 'PPG.created', 'PPG.created_by', 'PPG.modified', 'PPG.modified_by'
        );

        $sSql = 'SELECT %cols
             FROM products_publishers_groups AS PPG
             WHERE
             1=1
              %filters
             ';
        $sGroupBySQL = 'GROUP BY PPG.id';
        $aSearchCols = array('PPG.name');
        $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, '', $sGroupBySQL, ' PPG.id ASC ');
        $oView->AddRecordsHeader($aRecordsHeader);
        $oView->setActionColSettings($aColSettings);

        $aRecordsFooter = array(array(), array('add'));
        $oView->AddRecordsFooter($aRecordsFooter);
        return ShowTable($oView->Show());
    }


    /**
     *
     * @return string
     */
    public function doDelete()
    {

        $iId = $_GET['id'];
        if ($iId > 0) {

            $sSql = 'DELETE FROM products_publishers_groups WHERE id = ' . $iId . ' LIMIT 1';
            if ($this->pDbMgr->Query('profit24', $sSql) === false) {
                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania grupy'));
                return $this->doDefault();
            } else {
                $this->sMsg .= GetMessage(_('Grupa została usunięta'), false);
                return $this->doDefault();
            }
        } else {
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania grupy'));
            return $this->doDefault();
        }
    }

    public function parseViewItem(array $aItem)
    {
        return $aItem;
    }

    public function resetViewState()
    {
        resetViewState();
    }

    public function doAdd()
    {
        return $this->doAddEdit();
    }

    public function doEdit()
    {
        $iId = $_GET['id'];
        return $this->doAddEdit($iId);
    }

    public function doAddEdit($iId = 0)
    {
        global $aConfig;

        $aData = array();
        if (!empty($_POST)) {
            $aData = $_POST;
        } elseif ($iId > 0) {
            $aData = $this->pDbMgr->getTableRow('products_publishers_groups', $iId, [' * ']);
        }

        include_once('lib/Form/FormTable.class.php');
        $pForm = new FormTable('add_publisher_group', _('Dodaj grupę'));
        if ($iId > 0) {
            $pForm->AddHidden('do', 'update');
            $pForm->AddHidden('id', $iId);
        } else {
            $pForm->AddHidden('do', 'insert');
        }
        $pForm->AddText('name', _('Nazwa'), $aData['name']);

        $pForm->AddRow('&nbsp;',
            $pForm->GetInputButtonHTML('send', _('Zapisz')) . '&nbsp;&nbsp;' .
            $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'],
                array('onclick' => 'MenuNavigate(\'' . phpSelf() . '\');'), 'button'));
        return $pForm->ShowForm();
    }

    public function doInsert()
    {
        if (!empty($_POST)) {
            $aData = $_POST;
        } else {
            return $this->doDefault();
        }

        $aValues = [
            'name' => $aData['name'],
            'created' => 'NOW()',
            'created_by' => $_SESSION['user']['name']
        ];
        if ($this->pDbMgr->Insert('profit24', 'products_publishers_groups', $aValues) === false) {
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas dodawania grupy'));
            return $this->doAdd();
        } else {
            $this->sMsg .= GetMessage(_('Grupa została dodana'), false);
        }
        return $this->doDefault();
    }


    public function doUpdate()
    {
        if (!empty($_POST) || intval($_POST['id']) <= 0) {
            $aData = $_POST;
            $iId = intval($_POST['id']);
        } else {
            return $this->doDefault();
        }

        $aValues = [
            'name' => $aData['name'],
            'modified' => 'NOW()',
            'modified_by' => $_SESSION['user']['name']
        ];
        if ($this->pDbMgr->Update('profit24', 'products_publishers_groups', $aValues, ' id = ' . $iId) === false) {
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas zmiany grupy'));
            return $this->doAddEdit($iId);
        } else {
            $this->sMsg .= GetMessage(_('Grupa została zmieniona'), false);
        }
        return $this->doDefault();
    }
}