<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - tagi
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'serie');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'insert': $this->Insert($pSmarty); break;
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iId); break;
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych wydawnictwa
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			), 
			array(
				'db_field'	=> 'tag',
				'content'	=> $aConfig['lang'][$this->sModule]['list_tag'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'quantity',
				'content'	=> $aConfig['lang'][$this->sModule]['list_quantity'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'page_id',
				'content'	=> $aConfig['lang'][$this->sModule]['list_page'],
				'sortable'	=> true
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '35'
			)
		);

		// pobranie liczby wszystkich serii
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_tags
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND tag LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_tags";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_tags', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich serii
			$sSql = "SELECT id, tag, quantity, page_id
							 FROM ".$aConfig['tabls']['prefix']."products_tags
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND tag LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			foreach ($aRecords as $iKey => $aItem) {
				$aRecords[$iKey]['page_id'] = $this->getPageName($aItem['page_id']);
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'tag' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit','delete'),
					'params' => array (
												'edit'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}

		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all')
		);

		$pView->AddRecordsFooter($aRecordsFooter);
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function

	/**
	 * Metoda aktualizuje w bazie danych wydawnictwa
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id serii
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		// sprawdzenie tagow
		if (trim($_POST['tag']) != '' && !$this->checkTag($_POST['tag'])) {
			// nieprawidlowo wypelnione pole tagi
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
			$this->AddEdit($pSmarty, $iId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}

			Common::BeginTransaction();
			// aktualizacja
			$aValues = array(
				'tag' => trim($_POST['tag']),
				'page_id' => intval($_POST['page_id'])
			);
			
			
			if (Common::Update($aConfig['tabls']['prefix']."products_tags",
												 $aValues,
												 "id = ".$iId) === false) {
				$bIsErr = true;
			}
			
			$aShadows = $this->getShadowsWithTag($iId);
			if(!empty($aShadows)) {
				foreach($aShadows as $iShadowId) {
					if($this->updateShadowTags($iShadowId) === false){
						$bIsErr=true;
					}
				}
			}

	
			if (!$bIsErr) {
				// zaktualizowano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
												$_POST['tag']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				$this->Show($pSmarty);
			}
			else {
				// nie zaktualizowano
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
												$_POST['tag']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iId);
			}
		
	} // end of Update() funciton
	
	/**
	 * Metoda tworzy formularz dodawania / edycji wydawnictwa
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanej serii
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."products_tags
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
			//$aData['publisher_mapping'] = $this->getMappings($iId);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($sParName) && !empty($sParName)) {
			$sHeader .= ' "'.$aData['name'].'"';
		}

		$pForm = new FormTable('products_tags', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		if ($iId > 0) {
			$pForm->AddHidden('do', 'update');
		}
		else {
			$pForm->AddHidden('do', 'insert');
		}
		// tytul
		$pForm->AddText('tag', $aConfig['lang'][$this->sModule]['tag'], $aData['tag'], array('maxlength'=>64, 'style'=>'width: 350px;'));
		$pForm->AddSelect('page_id',$aConfig['lang'][$this->sModule]['main_category'], array('style'=>'width:300px;'), $this->getTopProductsCategories(true), $aData['page_id']);
		
	
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'sendMuliselects();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	
	function &getShadowsWithTag($iId) {
	global $aConfig;
		// wybiera grupy kategorii dla tego produktu
	  $sSql = "SELECT product_id
	  				 FROM ".$aConfig['tabls']['prefix']."products_to_tags
	  				 WHERE tag_id =".$iId;
	  return Common::GetCol($sSql);
	} // end of getShadowsWithPublisher()
	
	/**
	 * Metoda sprawdza poprawosc wypelnienia tagow
	 * 
	 * @param	string	$sTags	- tagi
	 * @return	bool
	 */
	function checkTag() {
		return preg_match('/^[^`~!@#\$%\^&\*\(\)\+=\[\]\{\};:\'"\,\.<\\>\?\/](\s*[^`~!@#\$%\^&\*\(\)\+=\[\]\{\};:\'"\,\.<\\>\?\/])+$/', $_POST['tag']);
	} // end of checkTags() method
	
	/**
	 * Metoda usuwa wybrane tagi
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego tagu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['tag'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['tag'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton
	
		/**
	 * Metoda pobiera liste tagow do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id tagu
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, tag
					 		 FROM ".$aConfig['tabls']['prefix']."products_tags
						 	 WHERE id IN  (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
		/**
	 * Metoda usuwa tag
	 * 
	 * @param	integer	$iId	- Id tagu do usuniecia
	 * @return	bool - success	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "SELECT product_id
						FROM ".$aConfig['tabls']['prefix']."products_to_tags
						WHERE tag_id = ".$iId;
		$aProducts = Common::GetCol($sSql);
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_tags
						 WHERE id = ".$iId;
		$bErr = (Common::Query($sSql) === false);
		if(!$bErr && !empty($aProducts)){
			foreach($aProducts as $iPId){
				if($this->updateShadowTags($iPId) === false){
					$bErr = true;
				}
			}
		}
		return !$bErr;
	} // end of deleteItem() method
	
} // end of Module Class
?>