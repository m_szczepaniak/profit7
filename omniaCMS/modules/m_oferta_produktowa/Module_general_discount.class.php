<?php

/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - rabat ogolny
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */
class Module
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    // dane konfiguracyjne
    var $aSettings;

    /**
     * Konstruktor klasy
     *
     * @return    void
     */
    function Module(&$pSmarty)
    {
        global $aConfig;

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        $sDo = '';
        $iId = 0;
        $iPId = getModulePageId('m_powiazane_oferta_produktowa', 'opisy_statusow');

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }
        if (isset($_GET['id'])) {
            $iId = intval($_GET['id']);
        }

        // wydzielenie uprawnien
        $this->aPrivileges =& $_SESSION['user']['privileges'];

        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }

        switch ($sDo) {
            case 'update':
                $this->Update($pSmarty, $iId);
                break;
            case 'edit':
            case 'add':
                $this->AddEdit($pSmarty);
                break;
            default:
                $this->AddEdit($pSmarty);
                break;
        }
    } // end of Module() method


    /**
     * Metoda aktualizuje w bazie danych opis statusu
     *
     * @param        object $pSmarty
     * @param    integer $iId - Id opisu
     * @return    void
     */
    function Update(&$pSmarty, $iId)
    {
        //die;
        global $aConfig, $pDbMgr;
        $bIsErr = false;

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty);
            return;
        }
//    if (empty($_POST['website'])) {
//      // zabezpieczenie przed wywolaniem akcji bez przeslania nazwy serwisu
//			$this->Show($pSmarty);
//			return;
//    }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEdit($pSmarty);
            return;
        }

        $websites = $pDbMgr->GetAssoc('profit24', "SELECT code, id FROM websites");

        foreach ($_POST['forms'] as $shop => $form) {
            $dbCon = end(explode('_', $shop));
            for ($i = 0; $i < 15; $i++) {
//				if ($i != 6) {
                // aktualizacja
                $aValues = array(
                    'discount' => Common::formatPrice2($form['discount']['discount_' . $i]),
                    'mark_up' => Common::formatPrice2($form['markup']['mark_up_' . $i]),
                    'minimal_overhead' => Common::formatPrice2($form['minimal_overhead']['minimal_overhead_' . $i]),
                    'catalogue_price_substractor' => Common::formatPrice2($form['catalogue_price_substractor']['catalogue_price_substractor_' . $i]),
                    'recount' => isset($form['recount']['recount_' . $i]) ? '1' : '0'
                );

                if ('profit24' === $dbCon && $i == 13) { // TYLKO DLA PANDY MAMY TO USTAWIENIE !!
                    if (isset($form['markup_wholesale_price']['markup_wholesale_price_'.$i])) {
                        $aValues['markup_wholesale_price'] = Common::formatPrice2($form['markup_wholesale_price']['markup_wholesale_price_'.$i]);
                    }
                }

                $currentData = $pDbMgr->GetRow($dbCon, "SELECT * FROM " . $aConfig['tabls']['prefix'] . "products_general_discount" . " WHERE source = '$i'");

                if (true == empty($currentData)) {
                    $aValues['source'] = $i;
                    $pDbMgr->Insert($dbCon, $aConfig['tabls']['prefix'] . "products_general_discount", $aValues, '', false);
                } else {
                    if ($pDbMgr->Update($dbCon, $aConfig['tabls']['prefix'] . "products_general_discount",
                            $aValues,
                            "source = '" . $i . "'") === false
                    ) {
                        $bIsErr = true;
                    }
                }

                // zapisujemy w tabelce profitowej
                $shopName = str_replace('general_discounts_', '', $shop);
                $websiteId = $websites[$shopName];
                $currentData = $pDbMgr->GetRow('profit24', "SELECT * FROM " . $aConfig['tabls']['prefix'] . "products_general_discount_common" . " WHERE source = '$i' AND website_id = $websiteId");

                if (isset($aValues['markup_wholesale_price'])) {
                    unset($aValues['markup_wholesale_price']);
                }
                $commonValues = $aValues;
                $commonValues['website_id'] = $websiteId;

                if (true == empty($currentData)) {
                    $commonValues['source'] = $i;
                    $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix'] . "products_general_discount_common", $commonValues);
                } else {
                    if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix'] . "products_general_discount_common",
                            $commonValues,
                            "source = '" . $i . "' AND website_id = $websiteId") === false
                    ) {
                        $bIsErr = true;
                    }
                }
//				}
            }
        }

        if (!$bIsErr) {
            // akapit zostal zaktualizowany
            $sMsg = $aConfig['lang'][$this->sModule]['edit_ok'];
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            // reset ustawien widoku
            $_GET['reset'] = 2;
            $this->AddEdit($pSmarty);
        } else {
            // akapit nie zostal dodany,
            // wyswietlenie komunikatu o niepowodzeniu
            // oraz ponowne wyswietlenie formularza dodawania
            $sMsg = $aConfig['lang'][$this->sModule]['edit_err'];
            $this->sMsg = GetMessage($sMsg);
            AddLog($sMsg);
            $this->AddEdit($pSmarty);
        }
    } // end of Update() funciton


    /**
     * Metoda tworzy formularz dodawania / edycji opisu statusu
     *
     * @param    object $pSmarty
     * @param    integer $iId - Id edytowanego opisu statusu
     */
    function AddEdit(&$pSmarty)
    {
        global $aConfig, $pDbMgr;

        $aDataPR24 = array();
        $aDataNP = array();
        $aDataMESTRO = array();
        $sHtml = '';

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        $sSql = 'SELECT id, symbol FROM sources';
        $sources = $pDbMgr->GetAssoc('profit24', $sSql);

        // pobranie z bazy danych edytowanego rekordu
        $sSql = "SELECT source,discount,mark_up,recount, minimal_overhead, catalogue_price_substractor, markup_wholesale_price
             FROM " . $aConfig['tabls']['prefix'] . "products_general_discount";
        $aDataPR24 =& $pDbMgr->GetAssoc('profit24', $sSql);

        $sHeader = $aConfig['lang'][$this->sModule]['header'];

        $formId = 'general_discounts_profit24';
        $pForm = new FormTable($formId, $sHeader . ' - Profit24 ', array('action' => phpSelf(array('id' => $iId))), array('col_width' => 150, 'style' => 'width: 310px; float:left;'), $aConfig['common']['js_validation']);
//		$pForm->AddHidden('do', 'update');
        $pForm->AddHidden('forms[' . $formId . ']' . 'website', 'profit24');

        foreach ($sources as $i => $symbol) {
            $pForm->AddMergedRow($aConfig['lang'][$this->sModule]['discount_' . $i . '_section'], array('class' => 'merged', 'style' => 'padding-left: 155px;'));
            $pForm->AddText('forms[' . $formId . '][discount][discount_' . $i . ']', $aConfig['lang'][$this->sModule]['discount_' . $i], Common::formatPrice($aDataPR24[$i]['discount']), array('id' => 'discount_' . $i . '_pr', 'class' => 'discount', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm->AddText('forms[' . $formId . '][markup][mark_up_' . $i . ']', $aConfig['lang'][$this->sModule]['mark_up_' . $i], Common::formatPrice($aDataPR24[$i]['mark_up']), array('id' => 'mark_up_' . $i . '_pr', 'class' => 'mark_up', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm->AddText('forms[' . $formId . '][minimal_overhead][minimal_overhead_' . $i . ']', _('Minimalny narzut'), Common::formatPrice($aDataPR24[$i]['minimal_overhead']), array('id' => 'minimal_overhead_' . $i . '_pr', 'class' => 'minimal_overhead', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm->AddText('forms[' . $formId . '][catalogue_price_substractor][catalogue_price_substractor_' . $i . ']', _('Procent kwoty odejmowany od ceny katalogowej'), Common::formatPrice($aDataPR24[$i]['catalogue_price_substractor']), array('id' => 'catalogue_price_substractor_' . $i . '_pr', 'class' => 'catalogue_price_substractor', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            if ($symbol == 'panda') {
                $pForm->AddText('forms[' . $formId . '][markup_wholesale_price][markup_wholesale_price_' . $i . ']', _('Procent narzutu na cenę zakupu do wyliczenia ceny katalogowej'), Common::formatPrice($aDataPR24[$i]['markup_wholesale_price']), array('id' => 'markup_wholesale_price_' . $i . '_pr', 'class' => 'markup_wholesale_price', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            }
            $pForm->AddCheckBox('forms[' . $formId . '][recount][recount_' . $i . ']', $aConfig['lang'][$this->sModule]['recount'], array(), '', !empty($aDataPR24[$i]['recount']), false);


        }
        $pForm->addRow('Informacje dodatkowe', 'Jeśli dla danego źródła podany został narzut oraz rabat ogólny, to dla wszystkich książek które posiadają cenę zakupu cena wyliczana jest na podstawie narzutu. <br />
      Jeśli produkt nie posiada podanej ceny zakupu, sprzedawany jest w cenie detalicznej. Jeśli produkt posiada załącznik w cenie >0 to cena wyliczana jest na podstawie rabatu ogólnego');
        //$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));


        // pobranie z bazy danych edytowanego rekordu
        $sSql = "SELECT source,discount,mark_up, recount, minimal_overhead, catalogue_price_substractor
             FROM " . $aConfig['tabls']['prefix'] . "products_general_discount";
        $aDataNP =& $pDbMgr->GetAssoc('np', $sSql);

        $formId = 'general_discounts_np';
        $pForm2 = new FormTable($formId, $sHeader . ' - niePrzeczytane.pl', array('action' => phpSelf(array('id' => $iId))), array('col_width' => 150, 'style' => 'width: 310px; float:left;'), $aConfig['common']['js_validation']);
        $pForm2->AddHidden('do', 'update');
        $pForm->AddHidden('forms[' . $formId . ']' . '[website]', 'np');

        foreach ($sources as $i => $symbol) {
            $pForm2->AddMergedRow($aConfig['lang'][$this->sModule]['discount_' . $i . '_section'], array('class' => 'merged', 'style' => 'padding-left: 155px;'));
            $pForm2->AddText('forms[' . $formId . '][discount][discount_' . $i . ']', $aConfig['lang'][$this->sModule]['discount_' . $i], Common::formatPrice($aDataNP[$i]['discount']), array('id' => 'discount_' . $i . '_np', 'class' => 'discount', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm2->AddText('forms[' . $formId . '][markup][mark_up_' . $i . ']', $aConfig['lang'][$this->sModule]['mark_up_' . $i], Common::formatPrice($aDataNP[$i]['mark_up']), array('id' => 'mark_up_' . $i . '_np', 'class' => 'mark_up', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm2->AddText('forms[' . $formId . '][minimal_overhead][minimal_overhead_' . $i . ']', _('Minimalny narzut'), Common::formatPrice($aDataNP[$i]['minimal_overhead']), array('id' => 'minimal_overhead_' . $i . '_pr', 'class' => 'minimal_overhead', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm2->AddText('forms[' . $formId . '][catalogue_price_substractor][catalogue_price_substractor_' . $i . ']', _('Procent kwoty odejmowany od ceny katalogowej'), Common::formatPrice($aDataNP[$i]['catalogue_price_substractor']), array('id' => 'catalogue_price_substractor_' . $i . '_pr', 'class' => 'catalogue_price_substractor', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm2->AddCheckBox('forms[' . $formId . '][recount][recount_' . $i . ']', $aConfig['lang'][$this->sModule]['recount'], array(), '', !empty($aDataNP[$i]['recount']), false);
        }
        //$pForm2->AddRow('&nbsp;', $pForm2->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));


        // pobranie z bazy danych edytowanego rekordu
        $sSql = "SELECT source,discount,mark_up, recount, minimal_overhead, catalogue_price_substractor
             FROM " . $aConfig['tabls']['prefix'] . "products_general_discount";
        $aDataMESTRO =& $pDbMgr->GetAssoc('mestro', $sSql);

        $formId = 'general_discounts_mestro';
        $pForm3 = new FormTable($formId, $sHeader . ' - mestro.pl', array('action' => phpSelf(array('id' => $iId))), array('col_width' => 150, 'style' => 'width: 310px; float:left;'), $aConfig['common']['js_validation']);
        $pForm3->AddHidden('do', 'update');
        $pForm->AddHidden('forms[' . $formId . ']' . 'website', 'mestro');

        foreach ($sources as $i => $symbol) {
            $pForm3->AddMergedRow($aConfig['lang'][$this->sModule]['discount_' . $i . '_section'], array('class' => 'merged', 'style' => 'padding-left: 155px;'));
            $pForm3->AddText('forms[' . $formId . '][discount][discount_' . $i . ']', $aConfig['lang'][$this->sModule]['discount_' . $i], Common::formatPrice($aDataMESTRO[$i]['discount']), array('id' => 'discount_' . $i . '_mestro', 'class' => 'discount', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm3->AddText('forms[' . $formId . '][markup][mark_up_' . $i . ']', $aConfig['lang'][$this->sModule]['mark_up_' . $i], Common::formatPrice($aDataMESTRO[$i]['mark_up']), array('id' => 'mark_up_' . $i . '_mestro', 'class' => 'mark_up', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm3->AddText('forms[' . $formId . '][minimal_overhead][minimal_overhead_' . $i . ']', _('Minimalny narzut'), Common::formatPrice($aDataMESTRO[$i]['minimal_overhead']), array('id' => 'minimal_overhead_' . $i . '_pr', 'class' => 'minimal_overhead', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm3->AddText('forms[' . $formId . '][catalogue_price_substractor][catalogue_price_substractor_' . $i . ']', _('Procent kwoty odejmowany od ceny katalogowej'), Common::formatPrice($aDataMESTRO[$i]['catalogue_price_substractor']), array('id' => 'catalogue_price_substractor_' . $i . '_pr', 'class' => 'catalogue_price_substractor', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm3->AddCheckBox('forms[' . $formId . '][recount][recount_' . $i . ']', $aConfig['lang'][$this->sModule]['recount'], array(), '', !empty($aDataMESTRO[$i]['recount']), false);
        }
        $pForm3->AddRow('&nbsp;', $pForm3->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));


        // pobranie z bazy danych edytowanego rekordu
        $sSql = "SELECT source,discount,mark_up, recount, minimal_overhead, catalogue_price_substractor
             FROM " . $aConfig['tabls']['prefix'] . "products_general_discount";
        $aDatasmarkacz =& $pDbMgr->GetAssoc('smarkacz', $sSql);

        $formId = 'general_discounts_smarkacz';
        $pForm4 = new FormTable($formId, $sHeader . ' - smarkacz.pl', array('action' => phpSelf(array('id' => $iId))), array('col_width' => 150, 'style' => 'width: 310px; float:left;'), $aConfig['common']['js_validation']);
        $pForm4->AddHidden('do', 'update');
        $pForm->AddHidden('forms[' . $formId . ']' . 'website', 'smarkacz');

        foreach ($sources as $i => $symbol) {
            $pForm4->AddMergedRow($aConfig['lang'][$this->sModule]['discount_' . $i . '_section'], array('class' => 'merged', 'style' => 'padding-left: 155px;'));
            $pForm4->AddText('forms[' . $formId . '][discount][discount_' . $i . ']', $aConfig['lang'][$this->sModule]['discount_' . $i], Common::formatPrice($aDatasmarkacz[$i]['discount']), array('id' => 'discount_' . $i . '_smarkacz', 'class' => 'discount', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm4->AddText('forms[' . $formId . '][markup][mark_up_' . $i . ']', $aConfig['lang'][$this->sModule]['mark_up_' . $i], Common::formatPrice($aDatasmarkacz[$i]['mark_up']), array('id' => 'mark_up_' . $i . '_smarkacz', 'class' => 'mark_up', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm4->AddText('forms[' . $formId . '][minimal_overhead][minimal_overhead_' . $i . ']', _('Minimalny narzut'), Common::formatPrice($aDatasmarkacz[$i]['minimal_overhead']), array('id' => 'minimal_overhead_' . $i . '_pr', 'class' => 'minimal_overhead', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm4->AddText('forms[' . $formId . '][catalogue_price_substractor][catalogue_price_substractor_' . $i . ']', _('Procent kwoty odejmowany od ceny katalogowej'), Common::formatPrice($aDatasmarkacz[$i]['catalogue_price_substractor']), array('id' => 'catalogue_price_substractor_' . $i . '_pr', 'class' => 'catalogue_price_substractor', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm4->AddCheckBox('forms[' . $formId . '][recount][recount_' . $i . ']', $aConfig['lang'][$this->sModule]['recount'], array(), '', !empty($aDatasmarkacz[$i]['recount']), false);
        }


        // pobranie z bazy danych edytowanego rekordu
        $sSql = "SELECT source,discount,mark_up, recount, minimal_overhead, catalogue_price_substractor
             FROM " . $aConfig['tabls']['prefix'] . "products_general_discount";
        $aDatanb =& $pDbMgr->GetAssoc('nb', $sSql);

        $formId = 'general_discounts_nb';
        $pForm5 = new FormTable($formId, $sHeader . ' - naszabiblioteka.pl', array('action' => phpSelf(array('id' => $iId))), array('col_width' => 150, 'style' => 'width: 310px; float:left;'), $aConfig['common']['js_validation']);
        $pForm5->AddHidden('do', 'update');
        $pForm->AddHidden('forms[' . $formId . ']' . 'website', 'nb');

        foreach ($sources as $i => $symbol) {
            $pForm5->AddMergedRow($aConfig['lang'][$this->sModule]['discount_' . $i . '_section'], array('class' => 'merged', 'style' => 'padding-left: 155px;'));
            $pForm5->AddText('forms[' . $formId . '][discount][discount_' . $i . ']', $aConfig['lang'][$this->sModule]['discount_' . $i], Common::formatPrice($aDatanb[$i]['discount']), array('id' => 'discount_' . $i . '_nb', 'class' => 'discount', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm5->AddText('forms[' . $formId . '][markup][mark_up_' . $i . ']', $aConfig['lang'][$this->sModule]['mark_up_' . $i], Common::formatPrice($aDatanb[$i]['mark_up']), array('id' => 'mark_up_' . $i . '_nb', 'class' => 'mark_up', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm5->AddText('forms[' . $formId . '][minimal_overhead][minimal_overhead_' . $i . ']', _('Minimalny narzut'), Common::formatPrice($aDatanb[$i]['minimal_overhead']), array('id' => 'minimal_overhead_' . $i . '_pr', 'class' => 'minimal_overhead', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm5->AddText('forms[' . $formId . '][catalogue_price_substractor][catalogue_price_substractor_' . $i . ']', _('Procent kwoty odejmowany od ceny katalogowej'), Common::formatPrice($aDatanb[$i]['catalogue_price_substractor']), array('id' => 'catalogue_price_substractor_' . $i . '_pr', 'class' => 'catalogue_price_substractor', 'maxlength' => 5, 'style' => 'width: 50px;'), '', 'ufloat');
            $pForm5->AddCheckBox('forms[' . $formId . '][recount][recount_' . $i . ']', $aConfig['lang'][$this->sModule]['recount'], array(), '', !empty($aDatanb[$i]['recount']), false);
        }
//		$pForm5->AddRow('&nbsp;', $pForm5->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));


        $sJsBadFix = '
      <script type="text/javascript">
        $(function() {
          $(".formTableContainer").parent().parent().parent().removeAttr("width");
          $(".formTableContainer").parent().parent().parent().attr("style", "float: left");

          /*!!!ZAKOMENTOWANE
          $(".mark_up").click( function () {
            var sAttrId = $(this).attr("id");
            var sId = sAttrId.replace("mark_up_", "");
            $("#discount_" + sId).attr("readonly", "readonly").attr("style", "background: grey; width: 50px;");
            $("#discount_" + sId).val("0,00");
            $(this).attr("style", "background: #fff; width: 50px;").removeAttr("readonly");
          });

          $(".discount").click( function () {
            var sAttrId = $(this).attr("id");
            var sId = sAttrId.replace("discount_", "");
            $("#mark_up_" + sId).attr("readonly", "readonly").attr("style", "background: grey; width: 50px;");
            $("#mark_up_" + sId).val("0,00");
            $(this).attr("style", "background: #fff; width: 50px;").removeAttr("readonly");
          });
          */
          $("input:text[value!=\'0,00\']").click();

          $("#general_discounts_mestro").submit( function(){

				var profitValidateResult = Validate_general_discounts_profit24_Form($("#general_discounts_profit24"));
				var npValdiateResult = Validate_general_discounts_np_Form($("#general_discounts_np"));
				var smarkaczValdiateResult = Validate_general_discounts_smarkacz_Form($("#general_discounts_smarkacz"));
				var nbValdiateResult = Validate_general_discounts_nb_Form($("#general_discounts_nb"));

				if (false === profitValidateResult || false === npValdiateResult || false === smarkaczValdiateResult || false === nbValdiateResult ){
					return false;
				}

         		var clonedProfitData = $("#general_discounts_profit24 .formTable input").clone();
         		var clonedNpData = $("#general_discounts_np .formTable input").clone();
         		var clonedSmarkaczData = $("#general_discounts_smarkacz .formTable input").clone();
         		var clonedNbData = $("#general_discounts_nb .formTable input").clone();

				$("#send").append(clonedProfitData);
				$("#send").append(clonedNpData);
				$("#send").append(clonedSmarkaczData);
				$("#send").append(clonedNbData);
//	          	return false;
          })

        });
      </script>';
        //$submitButton = '<br><br><input id="submit-all" value="'.$aConfig['lang']['common']['button_1'].'" type="submit" />';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()) . ShowTable($pForm2->ShowForm()) . ShowTable($pForm3->ShowForm()) . ShowTable($pForm4->ShowForm()) . ShowTable($pForm5->ShowForm()) . $sJsBadFix);
    } // end of AddEdit() function
} // end of Module Class
