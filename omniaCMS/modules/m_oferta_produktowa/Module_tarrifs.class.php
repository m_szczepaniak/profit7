<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - ceniki
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

	// komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	/**
	 * Konstruktor klasy
	 *
	 * @return	void
	 */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$this->aPrivileges =& $_SESSION['user']['privileges'];

		$sDo = '';

		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['it'])) {
			$iIt = $_GET['it'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
		


	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': 
				$this->Delete($pSmarty, $iPId, $iId); 
				break;
			case 'add':
				$this->addEditForm($pSmarty, $iPId);
				break;
			case 'edit':
				$this->addEditForm($pSmarty, $iPId,$iId);
				break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iPId,$iId); break;
	
			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end Module() function


	/**
	 * formularz dodawania grup atrybutow
	 * @param $pSmarty
	 * @param $iId
	 * @return unknown_type
	 */
	function addEditForm(&$pSmarty, $iPId, $iIt=0){
		global $aConfig;
		 
		$sSql = "SELECT price_brutto,name FROM ".$aConfig['tabls']['prefix']."products
	 						 WHERE id = ".$iPId;
		$aProduct=Common::GetRow($sSql);
				$fBasePrice =Common::formatPrice($aProduct['price_brutto']);
		if($iIt>0){
			$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."products_tarrifs
	 						 WHERE id = ".$iIt;
				$aData =& Common::GetRow($sSql);
				$aData['start_time']=date('H:i',$aData['start_date']);
				$aData['end_time']=date('H:i',$aData['end_date']);
				$aData['start_date']=date('d-m-Y',$aData['start_date']);
				$aData['end_date']=date('d-m-Y',$aData['end_date']);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader=sprintf($aConfig['lang'][$this->sModule]['header'],$aProduct['name']);
		$pForm = new FormTable('tarrifs', $sHeader, array('action'=>phpSelf(array('pid' => $iPId,'id' => $iIt))), array('col_width'=>135), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', $iIt >0 ?'update':'insert');

		$pForm->AddRow($aConfig['lang'][$this->sModule]['price'],$fBasePrice.$aConfig['lang']['common']['currency']);
		
		$pForm->AddText('discount', $aConfig['lang'][$this->sModule]['discount'], Common::formatPrice($aData['discount']), array('maxlength'=>12, 'style'=>'width: 75px;'), '', 'float',false);
		
		// data początkowa
		//$pForm->AddText('start_date', $aConfig['lang'][$this->sModule]['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date');
		$pForm->AddRow($aConfig['lang'][$this->sModule]['start_date'],
									 '<div style="float: left;">'.$pForm->GetTextHTML('start_date', $aConfig['lang'][$this->sModule]['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date', false).'</div><div style="float: left; padding-top: 3px; padding-left: 10px;">'.$pForm->GetLabelHTML('start_time', $aConfig['lang'][$this->sModule]['hour'], false).' '.$pForm->GetTextHTML('start_time', $aConfig['lang'][$this->sModule]['start_hour'], !empty($aData['start_time'])?$aData['start_time']:'07:00', array('style'=>'width: 50px;'), '', 'hour', false).'</div>');
		
		// data końcowa
		$pForm->AddRow($aConfig['lang'][$this->sModule]['end_date'],
									 '<div style="float: left;">'.$pForm->GetTextHTML('end_date', $aConfig['lang'][$this->sModule]['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date', false).'</div><div style="float: left; padding-top: 3px; padding-left: 10px;">'.$pForm->GetLabelHTML('end_time', $aConfig['lang'][$this->sModule]['hour'], false).' '.$pForm->GetTextHTML('end_time', $aConfig['lang'][$this->sModule]['end_hour'], !empty($aData['end_time'])?$aData['end_time']:'23:59', array('style'=>'width: 50px;'), '', 'hour', false).'</div>');
		
		//$pForm->AddText('end_date', $aConfig['lang'][$this->sModule]['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date');
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iIt > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		 
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of addAttributesForm() method

	/**
	 * Metoda wyswietla liste opcji cen dla produktu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');
		$aProduct=$this->getProduct($iPId);
		// zapamietanie opcji
		rememberViewState($this->sModule);
		
		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],$aProduct['id'],$aProduct['name']),
			'refresh'	=> false,
			'search'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'start_date',
				'content'	=> $aConfig['lang'][$this->sModule]['list_start_date'],
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'end_date',
				'content'	=> $aConfig['lang'][$this->sModule]['list_end_date'],
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'discount',
				'content'	=> $aConfig['lang'][$this->sModule]['list_discount'],
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'price_brutto',
				'content'	=> $aConfig['lang'][$this->sModule]['list_price'],
				'sortable'	=> false
			),
            array(
				'db_field'	=> 'type',
				'content'	=> $aConfig['lang'][$this->sModule]['type'],
				'sortable'	=> false
			),
            array(
                'db_field'	=> 'promotion',
                'content'	=> $aConfig['lang'][$this->sModule]['type_promotion'],
                'sortable'	=> false
            ),
			array(
				'content'	=> 'Cennik VIP',
				'sortable'	=> false,
				'width'	=> '50'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '50'
			)
		);
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						 WHERE product_id=".$iPId;
		$iRowCount = intval(Common::GetOne($sSql));

		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
						 	 FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						 	 WHERE product_id=".$iPId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
				
		$pView = new View('products_tarrifs', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
			// pobranie wszystkich rekordow
			$sSql = "SELECT PT.id, PT.product_id, PT.start_date, PT.end_date, PT.discount, PT.price_brutto, PT.isdefault, PT.general_discount, type, PP.name as promotion, PT.vip
						 	 FROM ".$aConfig['tabls']['prefix']."products_tarrifs AS PT
						 	 LEFT JOIN products_promotions AS PP ON PT.promotion_id = PP.id
						 	 WHERE  PT.product_id=".$iPId.
						 	 ' ORDER BY PT.id DESC'.
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			foreach ($aRecords as $iKey => $aItem) {
						// formatowanie ceny brutto
						$aRecords[$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']);
						$aRecords[$iKey]['discount'] = Common::formatPrice($aItem['discount']);
						$aRecords[$iKey]['start_date']=date('d-m-Y H:i',$aItem['start_date']);
						$aRecords[$iKey]['end_date']=date('d-m-Y H:i',$aItem['end_date']);
						if($aItem['isdefault']=='1'){
							$aRecords[$iKey]['disabled'][] = 'delete';
							$aRecords[$iKey]['disabled'][] = 'edit';
						}
						if($aItem['general_discount']=='1'){
							$aRecords[$iKey]['disabled'][] = 'delete';
						}

                        if ($aRecords[$iKey]['type'] == '1') {
                          $aRecords[$iKey]['type'] = $aConfig['lang'][$this->sModule]['type_ceneo'];
                        }
                        elseif(null != $aRecords[$iKey]['promotion']) {
                            $aRecords[$iKey]['type'] = $aConfig['lang'][$this->sModule]['type_promotion'];
                        }
                        else{
                          $aRecords[$iKey]['type'] = $aConfig['lang'][$this->sModule]['type_standard'];
                        }

						if ($aRecords[$iKey]['vip'] == '1') {
							$aRecords[$iKey]['vip'] = 'TAK';
						} else {
							$aRecords[$iKey]['vip'] = 'NIE';
						}
					}
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'product_id'	=> array(
					'show'	=> false
				),
				'title' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'pid' => '{product_id}', 'id' => '{id}'))
				),
				'isdefault'	=> array(
					'show'	=> false
				),
				'general_discount' => array(
					'show'	=> false
				), 
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('pid' => '{product_id}', 'id' => '{id}'),
												'delete'	=> array('pid' => '{product_id}', 'id' => '{id}')
											),
					'show' => false
				)
			);
			
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('add')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		
												 );
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show());
	} // end of AddEditFiles() function
	


	/**
	 * Metoda usuwa wybrane rekordy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego rekordu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId,$iIt) {
		global $aConfig;
		$bIsErr = false;
		
		if ($iIt > 0) {
			$_POST['delete'][$iIt] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
		
		$bIsErr=false;
	if (!empty($_POST['delete'])) {
	if (count($_POST['delete']) == 1) {
				$sErrPostfix = '0';
			}
			else {
				$sErrPostfix = '1';
			}
			// rozpoczecie transakcji
			Common::BeginTransaction();
			foreach ($_POST['delete'] as $iIdToDel) {
				// usuwamy
				$sSql="DELETE FROM ".$aConfig['tabls']['prefix']."products_tarrifs WHERE id=".$iIdToDel;
				if (($iRes = Common::Query($sSql)) === false) {
					$bIsErr=true;
				}
			}
			
			// przeliczmy teraz pakiety z tym produktem
			// jeśli produkt jest składową pakietu, aktualizacja tych pakietów
			$aPacketsProduct = $this->getPacketsProduct($iPId);
			foreach ($aPacketsProduct as $iPacketProduct) {
				if ($this->updateProductPacketPrice($iPId, $iPacketProduct) === false) {
					$bIsErr = true;
					break;
				}
				if ($this->updatePacketAuthorsPrices($iPacketProduct) === false) {
					$bIsErr = true;
					break;
				}
			}

			if (!$bIsErr) {
				// usunieto wszystkie grupy
				// zatwierdzenie transakcji
				SetModification('products',$iPId);
				Common::CommitTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['del_ok_'.($sDeleted!='' ? '1' : '0')];
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
			else {
				// albo wystapil jakis blad - albo niczego nie usunieto
				// wycofanie transakcji
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['del_err_'.($sDeleted!=''? '1' : '0')];
				$this->sMsg = GetMessage($sMsg, true);
				// dodanie informacji do logow
				AddLog($sMsg, true);
			}
		}
		
		$this->Show($pSmarty,$iPId);
	} // end of Delete() funciton
	
	
	/**
	 * Metoda dodaje do bazy danych nowy rekord
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty,$iPId) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty,$iPId);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
    $_POST['discount'] = Common::formatPrice2($_POST['discount']);
    if ($_POST['discount'] <= 0.00) {
      $oValidator->sError .= '<li>'._('Rabat nie może być mniejszy lub równy 0,00').'</li>';
    }
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->addEditForm($pSmarty,$iPId);
			return;
		}
    
		$aProduct=$this->getProduct((int)$iPId);
		if($aProduct['discount_limit'] > 0 && $_POST['discount'] > $aProduct['discount_limit']){
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['error_publisher_limit'], true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->addEditForm($pSmarty,$iPId);
			return;
		}

		$aValues=$this->calculateTarrif($aProduct['price_brutto'],$aProduct['vat'],$_POST['discount']);
		
		$aValues['start_date']=strtotime($_POST['start_date'].' '.$_POST['start_time']);//CalDateToTimestamp($_POST['start_date'].' '.$_POST['start_time']);
		$aValues['end_date']=strtotime($_POST['end_date'].' '.$_POST['end_time']);//CalDateToTimestamp($_POST['end_date'].' '.$_POST['end_time'], true);
		$aValues['product_id']= (int)$iPId;
		
		// dodanie
		if (Common::Insert($aConfig['tabls']['prefix']."products_tarrifs",
											 $aValues,'',
											 false) === false) {
			$bIsErr = true;
		}
		
		// przeliczmy teraz pakiety z tym produktem
		// jeśli produkt jest składową pakietu, aktualizacja tych pakietów
		$aPacketsProduct = $this->getPacketsProduct($iPId);
		foreach ($aPacketsProduct as $iPacketProduct) {
			if ($this->updateProductPacketPrice($iPId, $iPacketProduct) === false) {
				$bIsErr = true;
				break;
			}
			if ($this->updatePacketAuthorsPrices($iPacketProduct) === false) {
				$bIsErr = true;
				break;
			}
		}
		
		if (!$bIsErr) {
			SetModification('products',$iPId);
			// rekord zostal dodany
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											$aProduct['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			
		
				
			$this->Show($pSmarty,$iPId);
		}
		else {
			// rekord nie zostal dodany, 
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$aProduct['name']);
			$this->sMsg = GetMessage($sMsg);
			
			AddLog($sMsg);
			$this->addEditForm($pSmarty,$iPId);
		}
	} // end of Insert() funciton
	
	
	/**
	 * Metoda aktualizuje w bazie danych krekord
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id aktualizowanego rekordu
	 * @return	void
	 */
	function Update(&$pSmarty,$iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty,$iPId);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
    $_POST['discount'] = Common::formatPrice2($_POST['discount']);
    if ($_POST['discount'] <= 0.00) {
      $oValidator->sError .= '<li>'._('Rabat nie może być mniejszy lub równy 0,00').'</li>';
    }
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->addEditForm($pSmarty,$iPId, $iId);
			return;
		}
		$aProduct=$this->getProduct((int)$iPId);
		
		if($aProduct['discount_limit'] > 0 && $_POST['discount'] > $aProduct['discount_limit']){
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['error_publisher_limit'], true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->addEditForm($pSmarty,$iPId);
			return;
		}

		$aValues=$this->calculateTarrif($aProduct['price_brutto'],$aProduct['vat'],$_POST['discount']);
		
		$aValues['start_date']=strtotime($_POST['start_date'].' '.$_POST['start_time']);//CalDateToTimestamp($_POST['start_date'].' '.$_POST['start_time']);
		$aValues['end_date']=strtotime($_POST['end_date'].' '.$_POST['end_time']);//CalDateToTimestamp($_POST['end_date'].' '.$_POST['end_time'], true);
		
		
		if (Common::Update($aConfig['tabls']['prefix']."products_tarrifs",
											 $aValues,
											 "id = ".$iId) === false) {
			$bIsErr = true;
		}
		
		// przeliczmy teraz pakiety z tym produktem
		// jeśli produkt jest składową pakietu, aktualizacja tych pakietów
		$aPacketsProduct = $this->getPacketsProduct($iPId);
		foreach ($aPacketsProduct as $iPacketProduct) {
			if ($this->updateProductPacketPrice($iPId, $iPacketProduct) === false) {
				$bIsErr = true;
				break;
			}
			if ($this->updatePacketAuthorsPrices($iPacketProduct) === false) {
				$bIsErr = true;
				break;
			}
		}
		
		if (!$bIsErr) {
			SetModification('products',$iPId);
			// rekord zostal zaktualizowany
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$aProduct['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();
			
			AddLog($sMsg, false);
			
			
			
			$this->Show($pSmarty,$iPId);
		}
		else {
			// rekord nie zostal zmieniony, 
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$aProduct['name']);
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();
			
			AddLog($sMsg);
			$this->addEditForm($pSmarty,$iPId, $iId);
		}
	} // end of Update() funciton


		/**
	 * Metoda pobiera produkt o podanym id
	 * 
	 * @param	array	$aIds	- Id rekordu dla ktorych zwrocone zostana nazwy
	 * @return	string	- ciag z nazwami
	 */
	function getProduct($iId) {
		global $aConfig;
		// pobranie nazwy grupy
		$sSql = "SELECT A.id, A.price_brutto, A.vat, A.name, IFNULL(B.discount_limit,0) AS discount_limit
				 		 FROM ".$aConfig['tabls']['prefix']."products A
				 		 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
						 ON B.id = A.publisher_id
					 	 WHERE A.id = ".$iId;
		return Common::GetRow($sSql);
		
	} // end of getItemName() method

} // end of Module Class
?>