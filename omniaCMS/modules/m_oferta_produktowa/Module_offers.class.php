<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa - produkty'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

	// komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// ustawienia strony
	var $aSettings;

	// domyslna liczba pol na rodukty przy tworzeniu pakietu
	var $iDefaultProductsNo;

	/**
	 * Konstruktor klasy
	 *
	 * @return	void
	 */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		$this->iDefaultProductsNo = 3;

		$sDo = '';
		$iId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}

		$this->sDir = $this->iLangId.'/'.$iPId.'/'.$iId;


		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
		if ($_SESSION['user']['type'] === 0) {
			if (!hasModulePrivileges($_GET['module_id'])){
				showPrivsAlert($pSmarty);
				return;
			}
		}


		switch ($sDo) {
			case 'search_pdf': $this->getPdf($pSmarty); break;
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste produktow strony o id $iPId oferty produktowej
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$pForm = new FormTable('books_discount_search', $aConfig['lang'][$this->sModule]['header'], array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'search_pdf');
		
// FILTRY
		$pForm->AddText('search',$aConfig['lang'][$this->sModule]['main_pool'], $_POST['search'],array('style'=>'width: 300px;'),'','text' ,false);
		
		$pForm->AddText('f_tags', $aConfig['lang'][$this->sModule]['f_tags'], $_POST['f_tags'], array(), '', '', false);
    
		// kategoria Oferty produktowej
		$pForm->AddRow($aConfig['lang'][$this->sModule]['f_category'],
		$pForm->GetSelectHtml('f_category', $aConfig['lang'][$this->sModule]['f_category'], array(), $this->getProductsCategories(Common::getProductsMenu($this->iLangId), $aConfig['lang'][$this->sModule]['f_all']), $_POST['f_category'], '', false).'&nbsp;'.
		$pForm->GetInputButtonHTML('f_category_btn',$aConfig['lang'][$this->sModule]['f_category_btn'],array('id'=>'show_navitree'), 'button'));
		
		// stan publikacji
		$aPublished = array(
		array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
		array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_published']),
		array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_published'])
		);
		$pForm->AddSelect('f_published', $aConfig['lang'][$this->sModule]['f_status'], array(), $aPublished, $_POST['f_published'], '', false);
    
    $sOfferModule = 'm_oferta_produktowa';
		/*

		 */
		$aStatuses = array(
				array('value' => '', 'label' => $aConfig['lang'][$sOfferModule]['f_all']),
				array('value' => '1', 'label' => _('NOWOŚĆ')),
				array('value' => '2', 'label' => _('ZAPOWIEDŹ')),
		);
		$pForm->AddSelect('f_is_news', _('NOWOŚĆ/ZAPOWIEDŹ'), [], $aStatuses, $_POST['f_is_news'], '', false);
    
//		// czy obrobiona
//		$aReviewed = array(
//		array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
//		array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_reviewed']),
//		array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_reviewed'])
//		);
//		$pForm->AddSelect('f_reviewed', $aConfig['lang'][$this->sModule]['f_reviewed'], array(), $aReviewed, $_POST['f_reviewed'], '', false);
//			// czy obrobiona
		$aSources = array(
		array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
		array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_source_internalj']),
		array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_source_internaln']),
		array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_source_azymut']),
		array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['f_source_abe']),
		array('value' => '4', 'label' => $aConfig['lang'][$this->sModule]['f_source_helion']),
		array('value' => '7', 'label' => $aConfig['lang'][$this->sModule]['f_source_dictum']),
		array('value' => '8', 'label' => $aConfig['lang'][$this->sModule]['f_source_siodemka']),
		array('value' => '9', 'label' => $aConfig['lang'][$this->sModule]['f_source_olesiejuk']),
    array('value' => '11', 'label' => $aConfig['lang'][$this->sModule]['f_source_platon']),
		array('value' => '10', 'label' => $aConfig['lang'][$this->sModule]['f_source_X']),
		);
		$pForm->AddSelect('f_source', $aConfig['lang'][$this->sModule]['f_source'], array(), $aSources, $_POST['f_source'], '', false);
    
		$pForm->AddSelect('f_language', $aConfig['lang'][$this->sModule]['f_language'], array(), $this->getLanguages(), $_POST['f_language'], '', false);
		
		$pForm->AddSelect('f_orig_language', $aConfig['lang'][$this->sModule]['f_orig_language'], array(), $this->getLanguages(), $_POST['f_orig_language'], '', false);
		
		$pForm->AddSelect('f_binding', $aConfig['lang'][$this->sModule]['f_binding'], array(), $this->getBindings(), $_POST['f_binding'], '', false);
		
		$aImagesF = array(
		array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
		array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_image'])
		);
		$pForm->AddSelect('f_image', $aConfig['lang'][$this->sModule]['f_image'], array(), $aImagesF, $_POST['f_image'], '', false);
					
		$pForm->AddSelect('f_year', $aConfig['lang'][$this->sModule]['f_year'], array(), $this->getYears(), $_POST['f_year'], '', false);
		
		// wydawnictwo
		$pForm->AddText('f_author',$aConfig['lang'][$this->sModule]['f_author'], $_POST['f_author'],array('style'=>'width: 300px;'),'','text' ,false);

		
		// wydawnictwo
		$pForm->AddText('f_publisher',$aConfig['lang'][$this->sModule]['f_publisher'], $_POST['f_publisher'],array('style'=>'width: 300px;'),'','text' ,false);

		// data początkowa
		$pForm->AddText('f_start_date', $aConfig['lang'][$this->sModule]['f_start_date'], '00-00-0000', array('style'=>'width: 75px;'), '', 'date',false);
		// data końcowa
		$pForm->AddText('f_end_date', $aConfig['lang'][$this->sModule]['f_end_date'], '00-00-0000', array('style'=>'width: 75px;'), '', 'date',false);
		
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['config_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		$pForm->AddCheckBox('show_publisher', $aConfig['lang'][$this->sModule]['f_publisher'], array(), '', !empty($aData['show_publisher']), false);
		$pForm->AddCheckBox('show_authors', $aConfig['lang'][$this->sModule]['f_author'], array(), '', !empty($aData['show_authors']), false);
		$pForm->AddCheckBox('show_edition', $aConfig['lang'][$this->sModule]['edition'], array(), '', !empty($aData['show_edition']), false);
		$pForm->AddCheckBox('show_price', $aConfig['lang'][$this->sModule]['price'], array(), '', !empty($aData['show_price']), false);
		$pForm->AddCheckBox('show_tarrif', $aConfig['lang'][$this->sModule]['tarrif'], array(), '', !empty($aData['show_tarrif']), false);
		
				
					// przycisk wyszukaj
		$pForm->AddRow('&nbsp;', '<div style="margin-left: 225px;">'.
									$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_search']) .' '.$pForm->GetInputButtonHTML('send_csv', _('Pobierz CSV'))
									.'</div>');				

				$sJS='
					<script>
					
						$(document).ready(function(){
							$("#show_navitree").click(function() {
								window.open("ajax/NaviPopup.php",\'\',\'menubar=0,location=0,scrollbars=1,toolbar=0,width=500,height=300\');
							});
							
              $("#f_publisher").autocomplete({
                source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetPublishers.php"
              });
						});
					</script>';
			
			$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	} // end of Show() function

	function getPdf(&$pSmarty){
		global $aConfig;
		
		$aLang =& $aConfig['lang'][$this->sModule];
		
		//Column titles
		$aHeader = array(
			'name' => array(
				'label' => $aLang['name_label'],
				'width' => 185
			)
		);
		
		// query wyszukiwania
		$sSearch='';
		if(isset($_POST['search']) && !empty($_POST['search'])){
      $sTags = ' OR 1=2';
      if (isset($_POST['f_tags']) && $_POST['f_tags'] != '') {
        $sTags = ' OR PT.tag LIKE "%'.str_replace(',', '","', $_POST['f_tags']).'%" ';
      }
			$_POST['search']=trim($_POST['search']);
      
			if(is_numeric($_POST['search']))	{
				$sSearch =  ' AND ( A.id = '.(int) $_POST['search'] .' OR A.isbn_plain LIKE \''.isbn2plain($_POST['search']).'\' '.$sTags.' )';
			}
			elseif(preg_match('/[0-9]/',$_POST['search'])) {
				$sSearch = ' AND (A.name LIKE \'%'.preg_replace('/\s+/','%',$_POST['search']).'%\' OR A.isbn_plain LIKE \''.isbn2plain($_POST['search']).'\' '.$sTags.' )';
			}
			else {
				$sSearch =  ' AND (A.name LIKE \'%'.preg_replace('/\s+/','%',$_POST['search']).'%\'  '.$sTags.')';
			}
		} else {
      $sTags = ' AND 1=1';
      if (isset($_POST['f_tags']) && $_POST['f_tags'] != '') {
        $sTags = ' AND PT.tag LIKE "%'.str_replace(',', '","', $_POST['f_tags']).'%" ';
      }
    }
    if (isset($_POST['f_is_news']) && $_POST['f_is_news'] != '') {
      if ($_POST['f_is_news'] === '1') {
        $sSearch .= ' AND A.is_news = "1" ';
      }
      elseif ($_POST['f_is_news'] === '2') {
        $sSearch .= ' AND A.is_previews = "1" ';
      }
    }
		if(!empty($sSearch) || (isset($_POST['f_category']) && $_POST['f_category'] != '') || (isset($_POST['f_published']) && $_POST['f_published'] != '') 
				|| (isset($_POST['f_language']) && $_POST['f_language'] != '') || (isset($_POST['f_orig_language']) && $_POST['f_orig_language'] != '') || (isset($_POST['f_binding']) && $_POST['f_binding'] != '') 
				|| (isset($_POST['f_year']) && $_POST['f_year'] != '') || (isset($_POST['f_image']) && $_POST['f_image'] != '')  || (isset($_POST['f_tags']) && $_POST['f_tags'] != '') 
				|| (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '') || (isset($_POST['f_start_date']) && $_POST['f_start_date'] != '') || (isset($_POST['f_end_date']) && $_POST['f_end_date'] != '') 
				|| (isset($_POST['f_source']) && $_POST['f_source'] != '') || (isset($_POST['f_author']) && $_POST['f_author'] != '')){

							
			$sCSql = "SELECT COUNT(DISTINCT A.id)
				FROM ".$aConfig['tabls']['prefix']."products A ".
				 	 (isset($_POST['f_category']) && $_POST['f_category'] != '' ?
					 	 " LEFT JOIN ".$aConfig['tabls']['prefix']."products_extra_categories C
					 	 ON A.id = C.product_id" : "").
					 (isset($_POST['f_tags']) && $_POST['f_tags'] != '' ?
						" LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_tags AS PTT
              ON A.id = PTT.product_id
					 	 LEFT JOIN ".$aConfig['tabls']['prefix']."products_tags AS PT 
              ON PT.id = PTT.tag_id" : "").
					 (isset($_POST['f_author']) && $_POST['f_author'] != '' ?
						" LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_authors D
					 	 ON A.id = D.product_id
					 	 LEFT JOIN ".$aConfig['tabls']['prefix']."products_authors E
					 	 ON E.id = D.author_id" : "").
					 (isset($_POST['f_image'])?" LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
					 	 ON I.product_id = A.id ":'').
						" LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers F
					 	 ON A.publisher_id = F.id"."
				 	 WHERE 1=1".
					 ($sSearch != ''?$sSearch:'').
           ($sTags != ''?$sTags:'').   
			(isset($_POST['f_category']) && $_POST['f_category'] != '' ? ' AND (A.page_id = '.$_POST['f_category'].' OR C.page_id = '.$_POST['f_category'].')' : '').
			(isset($_POST['f_published']) && $_POST['f_published'] != '' ? ' AND A.published = \''.$_POST['f_published'].'\'' : '').
			(isset($_POST['f_language']) && $_POST['f_language'] != '' ? ' AND A.language = '.$_POST['f_language'] : '').
			(isset($_POST['f_orig_language']) && $_POST['f_orig_language'] != '' ? ' AND A.original_language = '.$_POST['f_orig_language'] : '').
			(isset($_POST['f_binding']) && $_POST['f_binding'] != '' ? ' AND A.binding = '.$_POST['f_binding'] : '').
			(isset($_POST['f_year']) && $_POST['f_year'] != '' ? ' AND A.publication_year = '.$_POST['f_year'] : '').
			//(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] != '' ? ' AND A.reviewed = \''.$_POST['f_reviewed'].'\'' : '').
			(isset($_POST['f_image']) && $_POST['f_image'] != '' ? ($_POST['f_image']=='0'?' AND I.id IS NULL':' AND I.id IS NOT NULL') : '').
			(isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = \''.$_POST['f_source'].'\'' : '').
			(isset($_POST['f_author']) && $_POST['f_author'] != '' ? ' AND CONCAT(E.name,\' \',E.surname,\' \',E.name) LIKE \'%'.$_POST['f_author'].'%\'' : '').
			(isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ? ' AND F.name = \''.$_POST['f_publisher'].'\'' : '').
			(isset($_POST['f_start_date']) && $_POST['f_start_date'] != '' ? ' AND DATE(A.created) >= \''.FormatDate($_POST['f_start_date']).'\'' : '').
			(isset($_POST['f_end_date']) && $_POST['f_end_date'] != '' ? ' AND DATE(A.created) <= \''.FormatDate($_POST['f_end_date']).'\'' : '');

			$iCount = Common::GetOne($sCSql);
      if ($iCount <= 0) {
				$this->sMsg = GetMessage(_('Brak ofert dla podanych parametrów.'));
				return $this->Show($pSmarty);
      }
			if($iCount < $aConfig['offer_generator']['max_records']){
			// pobranie wszystkich rekordow
			$sSql = "SELECT A.id, A.plain_name AS name, A.isbn". 
							(isset($_POST['show_publisher'])?",F.name AS publisher":'').
							(isset($_POST['show_edition'])?",A.publication_year,A.edition":'').
							(isset($_POST['show_price'])?",A.price_brutto":'')."
				 	 FROM ".$aConfig['tabls']['prefix']."products A ".
				 	 (isset($_POST['f_category']) && $_POST['f_category'] != '' ?
					 	 " LEFT JOIN ".$aConfig['tabls']['prefix']."products_extra_categories C
					 	 ON A.id = C.product_id" : "").
					 (isset($_POST['f_tags']) && $_POST['f_tags'] != '' ?
						" LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_tags AS PTT
              ON A.id = PTT.product_id
					 	 LEFT JOIN ".$aConfig['tabls']['prefix']."products_tags AS PT 
              ON PT.id = PTT.tag_id" : "").
					 (isset($_POST['f_author']) && $_POST['f_author'] != '' ?
						" LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_authors D
					 	 ON A.id = D.product_id
					 	 LEFT JOIN ".$aConfig['tabls']['prefix']."products_authors E
					 	 ON E.id = D.author_id" : "").
					 (isset($_POST['f_image'])?" LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
					 	 ON I.product_id = A.id ":'').
						" LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers F
					 	 ON A.publisher_id = F.id"."
				 	 WHERE 1=1".
					 ($sSearch != ''?$sSearch:'').
           ($sTags != ''?$sTags:'').   
			(isset($_POST['f_category']) && $_POST['f_category'] != '' ? ' AND (A.page_id = '.$_POST['f_category'].' OR C.page_id = '.$_POST['f_category'].')' : '').
			(isset($_POST['f_published']) && $_POST['f_published'] != '' ? ' AND A.published = \''.$_POST['f_published'].'\'' : '').
			(isset($_POST['f_language']) && $_POST['f_language'] != '' ? ' AND A.language = '.$_POST['f_language'] : '').
			(isset($_POST['f_orig_language']) && $_POST['f_orig_language'] != '' ? ' AND A.original_language = '.$_POST['f_orig_language'] : '').
			(isset($_POST['f_binding']) && $_POST['f_binding'] != '' ? ' AND A.binding = '.$_POST['f_binding'] : '').
			(isset($_POST['f_year']) && $_POST['f_year'] != '' ? ' AND A.publication_year = '.$_POST['f_year'] : '').
		//	(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] != '' ? ' AND A.reviewed = \''.$_POST['f_reviewed'].'\'' : '').
			(isset($_POST['f_image']) && $_POST['f_image'] != '' ? ($_POST['f_image']=='0'?' AND I.id IS NULL':' AND I.id IS NOT NULL') : '').
			(isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = \''.$_POST['f_source'].'\'' : '').
			(isset($_POST['f_author']) && $_POST['f_author'] != '' ? ' AND CONCAT(E.name,\' \',E.surname,\' \',E.name) LIKE \'%'.$_POST['f_author'].'%\'' : '').
			(isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ? ' AND F.name = \''.$_POST['f_publisher'].'\'' : '').
			(isset($_POST['f_start_date']) && $_POST['f_start_date'] != '' ? ' AND DATE(A.created) >= \''.FormatDate($_POST['f_start_date']).'\'' : '').
			(isset($_POST['f_end_date']) && $_POST['f_end_date'] != '' ? ' AND DATE(A.created) <= \''.FormatDate($_POST['f_end_date']).'\'' : '').
					 ' GROUP BY A.id ORDER BY A.name ASC';
			$aRecords =& Common::GetAll($sSql);

			if (isset($_POST['send_csv'])) {

				$aRecords = $this->prepareCsvColumns($aRecords);

				$this->download_send_headers("oferta.csv");
				echo $this->array2csv($aRecords);
				die();
			}

			if(!empty($aRecords)){
							$sHeaderHtml='';		
			$iNameWidth = 455;
			if(isset($_POST['show_authors'])){
				$iNameWidth = $iNameWidth - 80;
				$sHeaderHtml .= "<th width=\"80\">".$aLang['authors_label']."</th>
				";
			}
			if(isset($_POST['show_publisher'])){
				$iNameWidth = $iNameWidth - 80;
				$sHeaderHtml .= "<th width=\"80\">".$aLang['publisher_label']."</th>
				";
			}
			if(isset($_POST['show_price'])){
				$iNameWidth = $iNameWidth - 55;
				$sHeaderHtml .= "<th width=\"55\" align=\"center\">".$aLang['price_brutto_label']."</th>
				";
			}
			if(isset($_POST['show_tarrif'])){
				$iNameWidth = $iNameWidth - 55;
				$sHeaderHtml .= "<th width=\"55\" align=\"center\">".$aLang['price_label']."</th>
				";
			}
            $sHeaderHtml .= "<th width=\"55\" align=\"center\">Koszyk</th>";
			$sHeaderHtml .= "</tr>
			";
			$sHeaderHtml ="<table border=\"1\" cellspacing=\"0\" cellpadding=\"1\">
				<tr nobr=\"true\" style=\"background-color:#FF0000;color:#FFFFFF;\">
				<th width=\"".$iNameWidth."\">".$aLang['name_label']."</th>
				".$sHeaderHtml;
						
			$sHtml='';
				foreach ($aRecords as $iKey => $aItem) {
					if(mb_strlen($aItem['name'],'UTF-8') > $aConfig['offer_generator']['name_length']){
						$aRecords[$iKey]['name'] = mb_substr($aItem['name'],0,$aConfig['offer_generator']['name_length'],'UTF-8');
					}
					$aRecords[$iKey]['name'] .="<br /><small>".$aItem['isbn'];
				
					if(isset($_POST['show_edition'])){
						$sEdition='';
						if(!empty($aItem['publication_year'])){
							$sEdition.=' | rok wyd. '.$aItem['publication_year'];
						}
						if(!empty($aItem['edition'])){
							$sEdition.=' | wyd. '.$aItem['edition'];
						}
						if(!empty($sEdition)){
							$aRecords[$iKey]['name'] .=$sEdition;
						}
					}
					$aRecords[$iKey]['name'] .= "</small>";
					$sHtml .= "<tr nobr=\"true\"><td width=\"".$iNameWidth."\"><a href=\"".$aConfig['common']['profit24']['client_base_url_http_no_slash'].'/'.createProductLink($aItem['id'], $aItem['name'])."\">".$aRecords[$iKey]['name']."</a></td>";
					if(isset($_POST['show_authors'])){
						$sAuthors = $this->getAuthors($aItem['id']);
						if(mb_strlen($sAuthors,'UTF-8') > $aConfig['offer_generator']['authors_length']){
							$sAuthors = mb_substr($sAuthors,0,$aConfig['offer_generator']['authors_length'],'UTF-8');
						}
						$aRecords[$iKey]['authors'] = "<small>".$sAuthors."</small>";
						$sHtml .= "<td width=\"80\">".$aRecords[$iKey]['authors']."</td>";
					}
					if(isset($_POST['show_publisher'])){
						if(mb_strlen($aItem['publisher'],'UTF-8') > $aConfig['offer_generator']['publisher_length']){
							$aItem['publisher'] = mb_substr($aItem['publisher'],0,$aConfig['offer_generator']['publisher_length'],'UTF-8');
						}
						$aRecords[$iKey]['publisher'] = "<small>".$aItem['publisher']."</small>";
						$sHtml .= "<td width=\"80\">".$aRecords[$iKey]['publisher']."</td>";
					}
					
					if(isset($_POST['show_price'])){
						$aRecords[$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']).' '.$aConfig['lang']['common']['currency'];
						$sHtml .= "<td width=\"55\" align=\"center\">".$aRecords[$iKey]['price_brutto']."</td>";
					}
					if(isset($_POST['show_tarrif'])){
						$aTarrif = $this->getTarrif($aItem['id']);
						$aRecords[$iKey]['price'] = Common::formatPrice($aTarrif['price_brutto']).' '.$aConfig['lang']['common']['currency'];
						if(!empty($aTarrif['end_date']) && $aTarrif['end_date'] < 2145913200) {
							$aRecords[$iKey]['price'] .= "\n".$aLang['valid_to']." ". date('Y-m-d', $aTarrif['end_date']);
						}
						$sHtml .= "<td width=\"55\" align=\"center\">".$aRecords[$iKey]['price']."</td>";
					}
                    $sHtml .= "<td width=\"55\" align=\"center\"><a href=\"".$aConfig['common']['profit24']['client_base_url_http_no_slash'].'/internals/AddCard.class.php?product_id='.$aItem['id']."&count=1\"> Dodaj do koszyka</a></td>";
                    $sHtml .= "</tr>
				";	
				unset($aRecords[$iKey]['id']);
			}

			$sHtml .= "</table>
				";
			$sHtml = $sHeaderHtml.$sHtml;
//			dump($sHtml); die();
						
				require_once('OLD_tcpdf/config/lang/pl.php');
				require_once('OLD_tcpdf/tcpdf.php');		
				
				// create new PDF document
				$pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				$pdf->SetAuthor('profit24');
				$pdf->SetTitle($aLang['title']);
				$pdf->SetSubject($aLang['title']);
				$pdf->SetKeywords('');
				$pdf->setPrintHeader(false);
				
				// set default header data
				//$pdf->SetHeaderData('logo.gif', PDF_HEADER_LOGO_WIDTH, $aLang['logo_title'], $aLang['logo_string']);
				
				// set header and footer fonts
			//	$pdf->setHeaderFont(Array('freesans', '', PDF_FONT_SIZE_MAIN));
				$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));
				
				// set default monospaced font
				$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
				
				//set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
				//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
				$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
				
				//set auto page breaks
				$pdf->SetAutoPageBreak(TRUE, 10);
				
				//set image scale factor
				$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
				
				//set some language-dependent strings
				$pdf->setLanguageArray($l); 
				
				// ---------------------------------------------------------
				
				// set font
				$pdf->SetFont('freesans', '', 10);
				
				// add a page
				$pdf->AddPage();
				$pdf->writeHTML($sHtml, true, false, false, false, '');
				// print colored table
				//$this->ColoredTable($pdf, 185, $aHeader, $aRecords);
				//Close and output PDF document
				$pdf->Output('oferta.pdf', 'D');
										
				$_GET['hideHeader'] = '1';
			}
			} else {
				$this->sMsg = GetMessage(sprintf($aLang['maxed_out'],$iCount,$aConfig['offer_generator']['max_records']),true);
				return $this->Show($pSmarty);
			}
		} else {
			$this->sMsg = GetMessage($aLang['no_filters'],true);
			$this->Show($pSmarty);
		}	
	}
   /**
    * rysuje tabele w pdf
    * @param $oPdf - obiekt tcpdf
    * @param $iWidth int - szerokosc tabeli
    * @param $header ref array - tablica z etykietami i szerokościamy kolumn tabeli
    * 							( array( 'col1' => array('label' => 'label 1', 'width' => 100), 'col2' => ...  )
    * @param $data ref array- dane do wyświetlania w tabeli
    * @return unknown_type
    */
   function ColoredTable(&$oPdf,$iWidth,&$header,&$data) {
        
			// Colors, line width and bold font
        $oPdf->SetFillColor(255, 0, 0);
        $oPdf->SetTextColor(255);
        $oPdf->SetDrawColor(128, 0, 0);
        $oPdf->SetLineWidth(0.3);
        $oPdf->SetFont('', 'B');
				foreach($header as $key=>$aCol){
					$oPdf->Cell($aCol['width'],0,$aCol['label'],'LRTB',0,'',1);
				}
				$oPdf->Ln();
				// Color and font restoration
        $oPdf->SetFillColor(224, 235, 255);
        $oPdf->SetTextColor(0);
        $oPdf->SetFont('');
				
        $dimensions = $oPdf->getPageDimensions();
				$hasBorder = false; //flag for fringe case
        
        $fill=false;
        
        foreach($data as $row) {
					$rowcount = 0;
				 
					//work out the number of lines required
					foreach($header as $key=>$aCol){
						$iHL = $oPdf->getNumLines($row[$key], $aCol['width']);
						if($iHL > $rowcount)
							$rowcount = $iHL;
					}
					$startX = $oPdf->GetX();
					$startY = $oPdf->GetY();
				 
					if (($startY + $rowcount * 6) + $dimensions['bm'] > ($dimensions['hk'])) {
						//this row will cause a page break, draw the bottom border on previous row and give this a top border
						//we could force a page break and rewrite grid headings here
						if ($hasborder) {
							$hasborder = false;
						} else {
							$oPdf->Cell($iWidth,0,'','T'); //draw bottom border on previous row
							$oPdf->Ln();
						}
						$borders = 'LTR';
					} elseif ((ceil($startY) + $rowcount * 6) + $dimensions['bm'] == floor($dimensions['hk'])) {
						//fringe case where this cell will just reach the page break
						//draw the cell with a bottom border as we cannot draw it otherwise
						$borders = 'LRB';	
						$hasborder = true; //stops the attempt to draw the bottom border on the next row
					} else {
						//normal cell
						$borders = 'LR';
					}
				 	
					$iRealRowCount = 0;
					//now draw it
					foreach($header as $key=>$aCol){
						$iRow = $oPdf->MultiCell($aCol['width'],$rowcount * 6,$row[$key],$borders,'L',$fill,0,'','',true,0,true);
						if($iRow > $iRealRowCount){
							$iRealRowCount = $iRow;
						}
					}
					// correction necessary if number of rows actualy drawn is diffrent from 
					// number estimated by getNumLines function
					if($iRealRowCount != $rowcount){
						$oPdf->SetXY($startX,$startY);
						foreach($header as $key=>$aCol){
							$oPdf->MultiCell($aCol['width'],$iRealRowCount * 6,'',$borders,'L',$fill,0);
						}
					}
					$oPdf->Ln();
					// alternating row color
					$fill=!$fill;
				}
				 
				$oPdf->Cell($iWidth,0,'','T');  //last bottom border
        

    }

	private function prepareCsvColumns(array $aRecords)
	{
		$newArecords = [];
		global $aConfig;

		foreach($aRecords as $record) {

			$newProduct = [];
			$newProduct['name'] = $record['name'];

			if(isset($_POST['show_authors'])){
				$newProduct['authors'] = $this->getAuthors($record['id']);
			}
			if(isset($_POST['show_publisher'])){
				$newProduct['publisher'] = $record['publisher'];
			}
			if(isset($_POST['show_price'])){
				$newProduct['price_brutto'] = Common::formatPrice($record['price_brutto']).' '.$aConfig['lang']['common']['currency'];
			}
			if(isset($_POST['show_tarrif'])){
				$tarrif = $this->getTarrif($record['id']);
				$newProduct['price'] = Common::formatPrice($tarrif['price_brutto']).' '.$aConfig['lang']['common']['currency'];
			}

			$newArecords[] = $newProduct;
		}

		return $newArecords;
	}

	function array2csv(array &$array)
	{
		$titles = [
			'name' => 'Książka',
			'authors' => 'Autorzy',
			'publisher' => 'Wydawnictwo',
			'price_brutto' => 'Cena katalog.',
			'price' => 'Cena aktualna',
		];

		$fields = array_keys(reset($array));

		$headers = [];

		foreach($fields as $field) {
			$headers[] = $titles[$field];
		}

		if (count($array) == 0) {
			return null;
		}
		ob_start();
		$df = fopen("php://output", 'w');
		fputcsv($df, $headers);
		foreach ($array as $row) {
			fputcsv($df, $row);
		}
		fclose($df);
		return ob_get_clean();
	}

	function download_send_headers($filename) {
		// disable caching
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");
	}
} // end of Module Class
?>