<?php
/**
 * Klasa Module do obslugi modulu 'Autorzy' - role autorow
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;
	
	// Id strony
	var $iPageId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_autorzy');
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'insert': $this->Insert($pSmarty); break;
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iId); break;
			
			case 'sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show('',
																			 $this->getRecords(),
																			 phpSelf(array('do'=>'proceed_sort'),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."products_authors_roles",
												$this->getRecords(),
												array(),
											 	'order_by');
			break;
						
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych rol autorow
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_authors_roles
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_authors_roles";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_authors_roles', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT id, name, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."products_authors_roles
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('sort', 'add')
		);

		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane rol autorow
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanej roli
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			if (!$this->rolesAreInUse($_POST['delete'])) {
				Common::BeginTransaction();
				$aItems =& $this->getItemsToDelete($_POST['delete']);
				foreach ($aItems as $aItem) {
					// usuwanie
					if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
						$bIsErr = true;
						$sFailedToDel = $aItem['name'];
						break;
					}
					elseif ($iRecords == 1) {
						// usunieto
						$iDeleted++;
						$sDel .= '"'.$aItem['name'].'", ';
					}
				}
				$sDel = substr($sDel, 0, -2);
				
				if (!$bIsErr) {
					// usunieto
					Common::CommitTransaction();
					if ($iDeleted > 0) {
						$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
						$this->sMsg = GetMessage($sMsg, false);
						// dodanie informacji do logow
						AddLog($sMsg, false);
					}
				}
				else {
					// blad
					Common::RollbackTransaction();
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
					$this->sMsg = GetMessage($sMsg);
					// dodanie informacji do logow
					AddLog($sMsg);
				}
			}
			else {
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['roles_in_use_err']);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowa role autorow
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}
		// dodanie
		$aValues = array(
			'name' => decodeString($_POST['name']),
			'order_by' => '#order_by#',
			'created' => 'NOW()',
			'created_by' => $_SESSION['user']['name']
		);
		if (Common::Insert($aConfig['tabls']['prefix']."products_authors_roles",
						 					 $aValues,
											 "",
											 false) === false) {
			$bIsErr = true;
		}
				
		if (!$bIsErr) {
			// dodano
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// nie dodano
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty);
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych role autorow
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id opisu
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}
		// aktualizacja
		$aValues = array(
			'name' => decodeString($_POST['name']),
			'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name']
		);
		if (Common::Update($aConfig['tabls']['prefix']."products_authors_roles",
											 $aValues,
											 "id = ".$iId) === false) {
			$bIsErr = true;
		}
		
		if (!$bIsErr) {
			// akapit zostal zaktualizowany
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// akapit nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji roli autorow
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanej roli autorow
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT name
							 FROM ".$aConfig['tabls']['prefix']."products_authors_roles
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0) {
			$sHeader .= ' "'.$aData['name'].'"';
		}

		$pForm = new FormTable('products_authors_roles', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
		
		// nazwa roli
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>50, 'style'=>'width: 250px;'));
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda pobiera liste rekordow
	 *
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords() {
		global $aConfig;

		// pobranie wszystkich rol autorow
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."products_authors_roles
						 ORDER BY order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
	
	/**
	 * Metoda sprawdza czy role o podanych Id sa uzywane w produktach
	 * 
	 * @param	array	$aIds	- Id usuwanych rol
	 * @return	bool
	 */
	function rolesAreInUse($aIds) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(role_id)
						 FROM ".$aConfig['tabls']['prefix']."products_to_authors
						 WHERE role_id IN (".implode(',', $aIds).")";
		return intval(Common::GetOne($sSql)) > 0;
	} // end of rolesAreInUse method
	
	
	/**
	 * Metoda pobiera liste rol autorow do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id rol autorow
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, name
				 		 FROM ".$aConfig['tabls']['prefix']."products_authors_roles
					 	 WHERE id IN (".implode(',', $_POST['delete']).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa role autora
	 * 
	 * @param	integer	$iId	- Id roli autora do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_authors_roles
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method
	

	
} // end of Module Class
?>