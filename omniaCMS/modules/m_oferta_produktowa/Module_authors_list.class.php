<?php
/**
 * Klasa Module do obslugi modulu 'Autorzy'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
use Listener\ElasticListener;

include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;
	
	// Id strony
	var $iPageId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_autorzy');
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': 			$this->Delete($pSmarty, $iId); 			break;
			case 'insert': 			$this->Insert($pSmarty); 						break;
			case 'update': 			$this->Update($pSmarty, $iId); 			break;
			case 'toauthor': 		$this->ToAuthorEdit($pSmarty); 			break;
			case 'saveLinking': $this->SaveLinking($pSmarty); 			break;
			case 'saveLinking2':$this->SaveLinking2($pSmarty); 			break;
			case 'linked': 			$this->EditLinked($pSmarty, $iId); 	break;
			case 'edit':
			case 'add': 				$this->AddEdit($pSmarty, $iId); 		break;
			
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych rol autorow
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true,
			'send_button'=>'toauthor'
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'surname',
				'content'	=> $aConfig['lang'][$this->sModule]['list_surname'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_authors
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\' OR surname LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_authors";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_authors', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT id, name, surname, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."products_authors
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND (name LIKE \'%'.$_POST['search'].'%\') OR (surname LIKE \'%'.$_POST['search'].'%\')' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : ' DESC').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('linked', 'edit', 'delete'),
					'params' => array (
												'linked'	=> array('id'=>'{id}'),
												'edit'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('add')
		);
		

		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function

	/**
	 * Metoda tworzy formularz edycji powiazanych
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanego autora
	 */
	function EditLinked(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			$aData['publisher_mapping'] = $this->getMappings($iId);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_linked'],$this->getAuthorsNames(array($iId)));

		

		$pForm = new FormTable('search_linked', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'linked');
		$pForm->AddHidden('selected_author	', $iId);
			// wybor kategorii ABE
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['mappings_section'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
	// przycisk wyszukaj
		$pForm->AddRow($pForm->GetLabelHTML('publisher_search',$aConfig['lang'][$this->sModule]['publisher_search'],false), 
									$pForm->GetTextHTML('publisher_search',$aConfig['lang'][$this->sModule]['publisher_search'],$_POST['publisher_search'],array('style'=>'width:300px;'),'','text',false).'&nbsp;&nbsp;'.
						//			$pForm->GetInputButtonHTML('search', $aConfig['lang'][$this->sModule]['button_search'], array('onclick'=>'document.getElementById(\'do\').value=\'add\'; sendMuliselects(); this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button'));
						$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_search'], array('onclick'=>'sendMuliselects();')));
//	$pForm2 = new FormTable('products_publishers', '', array('action'=>phpSelf(array('id'=>$iId)), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
	//	$pForm2->AddHidden('do', 'update_linked');					
		$pForm->AddMergedRow(
									$pForm->GetSelectHtml('found_publishers',$aConfig['lang'][$this->sModule]['found_publishers'], array('size'=>20,'style'=>'width:450px;'), $this->getAuthors($_POST['publisher_search']),'','',false).'&nbsp;'.
									$pForm->GetInputButtonHTML('add_publ', $aConfig['lang'][$this->sModule]['add_publ'], array('onclick'=>'addPublisherMapping();'), 'button').'&nbsp;'.
									$pForm->GetInputButtonHTML('del_publ', $aConfig['lang'][$this->sModule]['del_publ'], array('onclick'=>'delPublisherMapping();'), 'button').'&nbsp;'.
									$pForm->GetSelectHtml('publisher_mapping',$aConfig['lang'][$this->sModule]['publisher_mapping'], array('size'=>20,'style'=>'width:450px;','multiple'=>1), $this->getAuthorsForIds($aData['publisher_mapping']),'','',false),array());
		
		//$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'sendMuliselects();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('save', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'document.getElementById(\'do\').value=\'saveLinking2\'; sendMuliselects(); this.form.submit();'), 'button').'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		$sJS.='
			function addPublisherMapping(){
				var elSelSrc = document.getElementById(\'found_publishers\'); 
	 			var elSelDest = document.getElementById(\'publisher_mapping\');
	 			
		 			var val = elSelSrc.options[elSelSrc.selectedIndex].value;
		 			var txt =  elSelSrc.options[elSelSrc.selectedIndex].title;
		 		
		 			if (elSelDest.options != undefined) {
	    			for (var i = 0; i < elSelDest.options.length; i++) {
	      			if (elSelDest.options[i].value == val) {
	        			return false;
	      			}
	    			}
	  			}	
	        elSelDest.options.add(new Option(txt,val));
			}
			
			function delPublisherMapping(){
				var elSelDest = document.getElementById(\'publisher_mapping\');
				elSelDest.remove(elSelDest.selectedIndex);
			}	

		';
		$pForm->setAdditionalJS($sJS);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of EditLinked() function
	
	/**
	 * Metoda zwraca listę znalezionych autorów
	 * 
	 * @param object $pSmarty
	 * @return void 
	 *
	 */
	function getAuthors($sSearch) {
		global $aConfig;
		$aItems=array();
		if(strlen($sSearch)) {
			$sSql = "SELECT id AS value, CONCAT(name,' ',surname) AS label, CONCAT(name,' ',surname) AS title
								 FROM ".$aConfig['tabls']['prefix']."products_authors
								 WHERE `name` LIKE '%$sSearch%' OR  `surname` LIKE '%$sSearch%' ";
			$aItems = Common::GetAll($sSql);
			}
		return $aItems;		 
	}
	
	/**
	 * Metoda wyswietla formatke wyboru authora do zmapowania
	 * 
	 * @param object $pSmarty
	 * @return void 
	 *
	 */
	
	function ToAuthorEdit(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		$aToLink=array();
		foreach($_POST['delete'] as $iKey => $aItem)
			$aToLink[]=$iKey;
		
		if(empty($aToLink)) {
			//$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['selected_none']);
			$this->Show($pSmarty);
			return;
			}
		$aAuthors=array(array('value'=>'0', 'label'=>'-- wybierz --'));
		
		if(strlen($_POST['author_search'])>2) {
			$sSql='SELECT CONCAT(name, \' \',surname) as label, id as value FROM '.$aConfig['tabls']['prefix'].'products_authors WHERE `name` LIKE \'%'.$_POST['author_search'].'%\' OR  `surname` LIKE \'%'.$_POST['author_search'].'%\' AND id NOT IN ('.implode(',', $aToLink).')';
			$aAuthors=array_merge($aAuthors,Common::GetAll($sSql));		
			}
			
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_linked'],$this->getAuthorsNames($aToLink));
		
		$pForm = new FormTable('search_linked', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'toauthor');
		
		foreach($aToLink as $iItem)
			$pForm->AddHidden('delete['.$iItem.']', '1');
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['mappings_select'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
	
		// przycisk wyszukaj
		$pForm->AddRow('', 
									$pForm->GetTextHTML('author_search',$aConfig['lang'][$this->sModule]['author_search'],$_POST['author_search'],array('style'=>'width:300px;'),'','text',false).'&nbsp;&nbsp;'.
									$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_search'], array()));
		
		$pForm->AddRow($pForm->GetLabelHTML('author_search',$aConfig['lang'][$this->sModule]['author_search'],false)
		,$pForm->GetSelectHTML('selected_author', '', array(), $aAuthors, $_POST['selected_author'], '', false));

		$pForm->AddRow('', 
									$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_send'], array('onclick'=>'ChangeObjValue(\'do\', \'saveLinking\')')));
		
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
		
		
		
	}//koniec metody ToAuthorEdit
	
	/**
	 * Metoda zapisuje linkowania autorów i modyfikuje produkty
	 * 
	 * @param object $pSmarty
	 * @return void 
	 *
	 */
	
	function SaveLinking(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		// dolaczenie klasy FormTable
		
		$aToLink=array();
		foreach($_POST['delete'] as $iKey => $aItem)
			$aToLink[]=$iKey;
		
		if(empty($aToLink)) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['selected_none']);
			$this->Show($pSmarty);
			return;
			}
		if($_POST['selected_author']<=0) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['author_none']);
			$this->ToAuthorEdit($pSmarty);
			return;
		}	
		
		$aValues['author_id']=intval($_POST['selected_author']);
		Common::BeginTransaction();
		//zaznaczonych linkujemy do wybranego
		if (Common::Update($aConfig['tabls']['prefix']."products_authors_mappings",$aValues,"author_import_id IN (".implode(',', $aToLink).")") === false) {
			$bIsErr = true;
			}
		//zlinkowanych do zaznaczonych do wybranego	
		if (Common::Update($aConfig['tabls']['prefix']."products_authors_mappings",$aValues,"author_id IN (".implode(',', $aToLink).")") === false) {
				$bIsErr = true;
				}	
		//wszystkich o id autora z powyzszej lsity do wybranego
		if (Common::Update($aConfig['tabls']['prefix']."products_to_authors",$aValues, "author_id IN (".implode(',', $aToLink).")") === false) {
			$bIsErr = true;
			}

		
		$this->UpdateShadowAuthorsForAuthor($_POST['selected_author']);
			
		foreach($aToLink as $iItem)
			if($this->deleteItem($iItem) === false)
				$bIsErr=true;
		
		
		if ($bIsErr) {
			Common::RollbackTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['linking_failed']);
			$this->Show($pSmarty);
		}
		else {
			Common::CommitTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['linking_ok'], false);
			
			$this->Show($pSmarty);
		}

		$authorListener = new ElasticListener();
		$authorListener->onAuthorUpdate($_POST['selected_author']);

	}//koniec metody SaveLinking
	
	
	/**
	 * Metoda kasuje wczesniejsze linkowania do autora i zapisuje linkowania autorów i modyfikuje produkty
	 * 
	 * @param object $pSmarty
	 * @return void 
	 *
	 */
	
	function SaveLinking2(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		
		$aToLink=array();
		$aToLink=$_POST['publisher_mapping'];
		
		if(empty($aToLink)) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['selected_none']);
			return;
			}
		if($_POST['selected_author']<=0) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['author_none']);
			$this->Show($pSmarty);
			$this->ToAuthorEdit($pSmarty);
			return;
		}	
		
		Common::BeginTransaction();
		$aValues['author_id']=intval($_POST['selected_author']);
		$aValues2['author_id']=intval($_POST['selected_author']);
		
		//skasowac obecne linkowania
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_authors_mappings
		WHERE author_id = ".$_POST['selected_author'];
		
		if(Common::Query($sSql) === false)
				$bIsErr=true;	
		
		foreach($aToLink as $iItem) {
			$aValues['author_import_id']=$iItem;
			//zaznaczonych linkujemy do wybranego
			if (Common::Insert($aConfig['tabls']['prefix']."products_authors_mappings",$aValues,'',false) === false) {
				$bIsErr = true;
				}
			//wszystkich o id autora z powyzszej lsity do wybranego
			if (Common::Update($aConfig['tabls']['prefix']."products_to_authors",$aValues2, "author_id = $iItem") === false) {
				$bIsErr = true;
				}
			if($iItem!=$_POST['selected_author'])	
				if($this->deleteItem($iItem) === false)
					$bIsErr=true;	
			}
			
			//zlinkowanych do zaznaczonych do wybranego	
			if (Common::Update($aConfig['tabls']['prefix']."products_authors_mappings",$aValues2,"author_id IN (".implode(',', $aToLink).")") === false) {
				$bIsErr = true;
				}	
	
		$this->UpdateShadowAuthorsForAuthor($_POST['selected_author']);
		
		if ($bIsErr) {
			Common::RollbackTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['linking_failed']);
			$this->Show($pSmarty);
		}
		else {
			Common::CommitTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['linking_ok'], false);
			
			$this->Show($pSmarty);
		}

		$authorListener = new ElasticListener();
		$authorListener->onAuthorUpdate($_POST['selected_author']);


	}//koniec metody SaveLinking2
	
	/**
	 * Metoda aktualizuje autorów produktów w shadow
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id autora
	 * @return 	void
	 */
	function UpdateShadowAuthorsForAuthor($iAuthor) {
		global $aConfig;
		require_once('Module_Common.class.php');
		//pobrac listę produktów o tych autorach																														
		$sSql= "SELECT product_id FROM ".$aConfig['tabls']['prefix'].'products_to_authors WHERE author_id='.$_POST['selected_author'];
		$aProductIds=Common::GetCol($sSql);
				
		//dla kazdego produktu zaktualizowac listę autorów w shadow																					
		foreach($aProductIds as $iItem) {
			$this->updateShadowAuthors($iItem);
			}
		}
	//end UpdateShadowAuthors	
	/**
	 * Metoda usuwa wybranych autorow
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego autora
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			if (!$this->authorsAreInUse($_POST['delete'])) {
				Common::BeginTransaction();
				$aItems =& $this->getItemsToDelete($_POST['delete']);
				foreach ($aItems as $aItem) {
					// usuwanie
					if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
						$bIsErr = true;
						$sFailedToDel = $aItem['name'];
						break;
					}
					elseif ($iRecords == 1) {
						// usunieto
						$iDeleted++;
						$sDel .= '"'.$aItem['name'].'", ';
					}
				}
				$sDel = substr($sDel, 0, -2);
				
				if (!$bIsErr) {
					// usunieto
					Common::CommitTransaction();
					if ($iDeleted > 0) {
						$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
						$this->sMsg = GetMessage($sMsg, false);
						// dodanie informacji do logow
						AddLog($sMsg, false);
					}
				}
				else {
					// blad
					Common::RollbackTransaction();
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
					$this->sMsg = GetMessage($sMsg);
					// dodanie informacji do logow
					AddLog($sMsg);
				}
			}
			else {
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['authors_in_use_err']);				
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowego autora
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}
		// dodanie
		Common::BeginTransaction();
		$aValues = array(
			'index_letter' => strtoupper(mb_substr($_POST['surname'], 0, 1, 'utf8')),
			'name' => decodeString($_POST['name']),
			'surname' => decodeString($_POST['surname']),
			'created' => 'NOW()',
			'created_by ' => $_SESSION['user']['name']
		);
		if (($iId = Common::Insert($aConfig['tabls']['prefix']."products_authors_import",
															 $aValues)) === false) {
			$bIsErr = true;
		}
		else {
			//dopisanie do tabeli autora i utworzenie mapowania
			$aValues2=$aValues;
			$aValues2['id']=$iId;
			Common::Insert($aConfig['tabls']['prefix']."products_authors",
															 $aValues2, '', false);
			$aValues3=array('author_id'=>$iId, 	'author_import_id'=>$iId);
			Common::Insert($aConfig['tabls']['prefix']."products_authors_mappings",
									 $aValues3, '', false);			
			//koniec dopisania						 			 
		}
		
		if ($bIsErr) {
			// nie dodano
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											(!empty($_POST['title']) ? $_POST['title'].' ' : '').
											$_POST['surname']." ".$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty);
		}
		else {
			// dodano
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											(!empty($_POST['title']) ? $_POST['title'].' ' : '').
											$_POST['surname']." ".$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
	} // end of Insert() funciton


	/**
	 * Metoda wprowadza zmiany w autorze do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}
				
		// aktualizacja
		Common::BeginTransaction();
		$aValues = array(
			'index_letter' => strtoupper(mb_substr($_POST['surname'], 0, 1, 'utf8')),
			'name' => decodeString($_POST['name']),
			'surname' => decodeString($_POST['surname'])
		);
		if (Common::Update($aConfig['tabls']['prefix']."products_authors",
											 $aValues,
											 "id = ".$iId) === false) {
			$bIsErr = true;
		}

		if ($bIsErr) {
			// niezaktualizowano
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											(!empty($_POST['title']) ? $_POST['title'].' ' : '').
											$_POST['surname']." ".$_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iId);
		}
		else {
			// zaktualizowano
			SetModification('products_authors',$iId);
			Common::CommitTransaction();
			$authorListener = new ElasticListener();
			$authorListener->onAuthorUpdate($iId);

			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											(!empty($_POST['title']) ? $_POST['title'].' ' : '').
											$_POST['surname']." ".$_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji autora
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	ID edytowanego autora
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego autora
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."products_authors
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0) {
			$sHeader .= ' "'.$aData['name'].'"';
		}

		$pForm = new FormTable('authors', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>135), $aConfig['common']['js_validation']);
		if ($iId > 0) {
			$pForm->AddHidden('do', 'update');
		}
		else {
			$pForm->AddHidden('do', 'insert');
		}

		// tytuł
		//$pForm->AddRow($aConfig['lang'][$this->sModule]['title'], $pForm->GetSelectHTML('title', '', array(), $this->getTitles(), $aData['title'], '', false).'&nbsp;&nbsp;'.$aConfig['lang'][$this->sModule]['or'].'&nbsp;&nbsp;'.$pForm->GetTextHTML('n_title', '', $aData['n_title'], array('maxlength'=>30, 'style'=>'width: 150px;'), '', 'text', false));

		// imię
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>60, 'style'=>'width: 200px;'));

		// nazwisko
		$pForm->AddText('surname', $aConfig['lang'][$this->sModule]['surname'], $aData['surname'], array('maxlength'=>50, 'style'=>'width: 200px;'));
		
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda sprawdza czy autorzy o podanych Id sa uzywani w produktach
	 * 
	 * @param	array	$aIds	- Id usuwanych autorow
	 * @return	bool
	 */
	function authorsAreInUse($aIds) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(author_id)
						 FROM ".$aConfig['tabls']['prefix']."products_to_authors
						 WHERE author_id IN (".implode(',', $aIds).")";
		return intval(Common::GetOne($sSql)) > 0;
	} // end of authorsAreInUse method
	
	
	/**
	 * Metoda pobiera liste autorow do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id autorow
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, CONCAT(name, ' ', surname) AS name
				 		 FROM ".$aConfig['tabls']['prefix']."products_authors
					 	 WHERE id IN (".implode(',', $_POST['delete']).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa autora
	 * 
	 * @param	integer	$iId	- Id autora do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		//kasowanie ze wszystkich tabel
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_authors
						 WHERE id = ".$iId;
		$bRet=Common::Query($sSql);
		
		if($bRet===false) return $bRet;
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_authors_import
						 WHERE id = ".$iId;
		Common::Query($sSql);
		
		if($bRet===false) return $bRet;
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_authors_mappings
						 WHERE author_id = ".$iId;
		return Common::Query($sSql);
		
		//koniec kasowanie ze wszystkich tabel
	} // end of deleteItem() method
	
	
	/**
	 * Metoda zwraca stringa z listą autorów z tablicy
	 * 
	 * @param	integer[]	$aId	- Lista Id autorów
	 * @return	string	
	 */
	function getAuthorsNames($aList) {
		global $aConfig;
		$sBuf='';
		
		foreach($aList as $iItem) {
			$sSql='SELECT CONCAT(surname, \' \', name,\', \') FROM '.$aConfig['tabls']['prefix'].'products_authors_import WHERE id='.$iItem;
			$sBuf.=Common::GetOne($sSql);
			}	 
			 
		return substr($sBuf,0,-2);
		
	} // end of getAuthorsNames() method
	

	function getMappings($iId) {
		global $aConfig;
		$sSql = "SELECT author_import_id
							FROM ".$aConfig['tabls']['prefix']."products_authors_mappings
							WHERE author_id = ".$iId;
		return Common::GetCol($sSql);
	}
	
			/**
	 * Metoda pobiera autorów z listy id
	 * 
	 * @return	array
	 */
	function &getAuthorsForIds($aIds) {
		global $aConfig;
		$aItems=array();
		if(!empty($aIds)) {
			foreach($aIds as $iKey=>$iId) {
				$sSql = "SELECT id AS value, CONCAT(name,' ',surname) AS label
								 FROM ".$aConfig['tabls']['prefix']."products_authors_import
								 WHERE id = ".$iId;
				$aItems[$iKey] = Common::GetRow($sSql); 
			}
		}
		return $aItems;
	} // end of getPublishers() method
	
} // end of Module Class
?>