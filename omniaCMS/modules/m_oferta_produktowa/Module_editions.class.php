<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - powiazane edycje produkty
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iPPId = 0;
		$iPId = 0;
		$iId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['ppid'])) {
			$iPPId = intval($_GET['ppid']);
		}
		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}


	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iPPId, $iId); break;
			case 'insert': $this->AddItem($pSmarty, $iPId, $iPPId); break;
			case 'add': $this->Add($pSmarty, $iPId, $iPPId); break;
			
			case 'sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show('',
																			 $this->getRecords($iPPId),
																			 phpSelf(array('do'=>'proceed_sort'),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."products_editions",
												$this->getRecords($iPPId),
												array(),
											 	'order_by');
			break;
			
			default: $this->Show($pSmarty, $iPId, $iPPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych powiazan produktow
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header' => sprintf($aConfig['lang'][$this->sModule]['list'], trimString($this->getItemName($iId), 50)),
			'refresh' => true,
			'action' => phpSelf(array('ppid' => $iId)),
			'refresh_link' => phpSelf(array('ppid' => $iId, 'reset' => '1')),
			'search' => true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'B.plain_name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'B.page_id',
				'content'	=> $aConfig['lang'][$this->sModule]['list_category'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(A.id)
						 FROM ".$aConfig['tabls']['prefix']."products_editions A
						 JOIN ".$aConfig['tabls']['prefix']."products B
						 ON B.id = A.linked_id
						 WHERE A.product_id = ".$iId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? (is_numeric($_POST['search']) ? ' AND A.linked_id = '.(int) $_POST['search'] : ' AND B.name LIKE \'%'.$_POST['search'].'%\'') : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(A.id)
							 FROM ".$aConfig['tabls']['prefix']."products_editions A
							 JOIN ".$aConfig['tabls']['prefix']."products B
							 ON B.id = A.linked_id
							 WHERE A.product_id = ".$iId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_editions', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich serii
			$sSql = "SELECT B.id, B.name, B.page_id category, C.name AS publisher, A.linked_id, B.publication_year
							 FROM ".$aConfig['tabls']['prefix']."products_editions A
							 JOIN ".$aConfig['tabls']['prefix']."products B
							 ON B.id = A.linked_id
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers C
							 	 ON B.publisher_id = C.id
							 WHERE A.product_id = ".$iId.
										 (isset($_POST['search']) && !empty($_POST['search']) ? (is_numeric($_POST['search']) ? ' AND A.linked_id = '.(int) $_POST['search'] : ' AND B.name LIKE \'%'.$_POST['search'].'%\'') : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			foreach ($aRecords as $iKey => $aItem) {
				$aRecords[$iKey]['category'] = getPagePath($aItem['category'], $this->iLangId, true, false, false);
				$aRecords[$iKey]['name'] = '<b>'.$aItem['name'].'</b>'.(!empty($aItem['publisher'])?'<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($aItem['publisher'],0,40,'UTF-8').'</span>':'');
				unset($aRecords[$iKey]['publisher']);
				$aRecords[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aItem['id']),0,40,'UTF-8').' '.$aItem['publication_year'].'</span>';
				unset($aRecords[$iKey]['publication_year']);
				// link podgladu
				$aRecords[$iKey]['action']['links']['preview'] = (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'].'/' : '').(mb_strlen($aItem['name'], 'UTF-8')>210?mb_substr(link_encode($aItem['name']),0,210, 'UTF-8'):link_encode($aItem['name'])).',product'.$aItem['linked_id'].'.html?preview='.md5($_SERVER['REMOTE_ADDR'].$aConfig['common']['client_base_url_http']);
				unset($aRecords[$iKey]['linked_id']);		
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('delete','preview'),
					'params' => array (
												'delete'	=> array('id'=>'{id}')
											),
					'show' => false
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('sort', 'add')
		);
		// konfiguracja przyciskow stopki
		$aRecordsFooterParams = array(
			'go_back' => array(array(), array('action', 'pid', 'ppid'))
		);
		
		// zmiana w langu
		$aConfig['lang']['m_oferta_produktowa_editions']['go_back'] = $aConfig['lang']['m_oferta_produktowa_editions']['go_back_products'];
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane produkty z polecanych produktow
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iPPId	- Id produktu zktorego usuwane sa powiazania produktow
	 * @param	integer	$iId	- id powiazanego porduktu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iPPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$iRecords = 0;

		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			if (count($_POST['delete']) == 1) {
				$sErrPostfix = '0';
			}
			else {
				$sErrPostfix = '1';
			}
			
			Common::BeginTransaction();
			// pobranie tytulow usuwanych powiazanych produktow
			$sSql = "SELECT id, page_id, name
					 		 FROM ".$aConfig['tabls']['prefix']."products
						 	 WHERE id IN  (".implode(',', $_POST['delete']).")";
			$aRecords =& Common::GetAll($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				foreach ($aRecords as $aItem) {
					// usuwanie produktow z polecanych
					$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_editions
									 WHERE product_id = ".$iPPId." AND
									 			 linked_id = ".$aItem['id'];
					if (($iRes = Common::Query($sSql)) === false) {
						$bIsErr = true;
					}
					elseif ($iRes == 1) {
						$iRecords++;
						// dodanie produktu do komunikatu
						$sDel .= '"'.getPagePath($aItem['page_id'], $this->iLangId, true, false, false).'  / '.$aItem['name'].'",<br>';
					}
				}
				$sDel = substr($sDel, 0, -5);
			}
			if (!$bIsErr) {
				if ($iRecords > 0) {
					SetModification('products',$iPId);
					Common::CommitTransaction();
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.$sErrPostfix],
													$sDel,
													getPagePath($iPId, $this->iLangId, true, false, false),
													$this->getItemName($iPPId));
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_'.$sErrPostfix],
												$sDel,
												getPagePath($iPId, $this->iLangId, true, false, false),
												$this->getItemName($iPPId));
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId, $iPPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje produkt do polecanych produktow
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function AddItem(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Add($pSmarty, $iPId, $iId);
			return;
		}
		$sNames='';
			Common::BeginTransaction();
		foreach($_POST['product_id'] as $iProduct){
			if (!$this->isEdition($iPId,(int) $iProduct)) {
				
				if (!$bIsErr) {
					// dodanie informacji do tabeli products_editions
					$bIsErr = !$this->addEditions($iId, (int) $iProduct);
				}
				if (!$bIsErr) {
					$aPrevEditions=$this->getPrevEditions((int) $iProduct);
					if(!empty($aPrevEditions)){
						foreach($aPrevEditions as $iPrevEdition){
							if (!$this->isEdition($iPId,(int) $iPrevEdition)) {
								// dodanie informacji do tabeli products_editions
								$bIsErr = !$this->addEditions($iId, (int) $iPrevEdition);
							}
						}
					}
				}
				
			}	
			$sNames.=$this->getItemName((int) $iProduct).',';
		}
			$sNames=substr($sNames,0,-1);
			
			if (!$bIsErr) {
				// dodano
				SetModification('products',$iPId);
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
												$sNames,
												$this->getItemName($iId));
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				// reset ustawien widoku
				$_GET['reset'] = 2;
				$this->Show($pSmarty, $iPId, $iId);
			}
			else {
				// blad
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
												$sNames,
												$this->getItemName($iId));
				$this->sMsg = GetMessage($sMsg);
	
				AddLog($sMsg);
				$this->Add($pSmarty, $iPId, $iId);
			}
		
	} // end of AddItem() funciton


	/**
	 * Metoda tworzy formularz wyboru produktu, ktory ma zostac dodany do listy
	 * bestsellerow
	 *
	 * @param	object	$pSmarty
	 */
	function Add(&$pSmarty, $iPId, $iId) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header'], trimString($this->getItemName($iId), 50));
				
		$pForm = new FormTable('search', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'add');
		
		$pForm->AddText('product_name',$aConfig['lang'][$this->sModule]['product_name'],$aData['product_name'],array('style'=>'width:300px;'),'','text',false);
		
		$pForm->AddText('product_author',$aConfig['lang'][$this->sModule]['product_author'], $aData['product_author'],array('style'=>'width:300px;','maxlength'=>255),'','text' ,false);
			$pForm->AddText('product_publisher',$aConfig['lang'][$this->sModule]['product_publisher'], $aData['product_publisher'],array('id'=>'publisher_aci','style'=>'width:300px;','maxlength'=>255),'','text' ,false);
				
		
		// przycisk wyszukaj
		$pForm->AddRow('&nbsp;', '<div style="margin-left: 225px;">'.
									$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_search'])
									.'</div>');
			
		$pForm2 = new FormTable('insert', '', array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm2->AddHidden('do', 'insert');
		// produkt
		$pForm2->AddSelect('product_id', $aConfig['lang'][$this->sModule]['product'], array('size'=>15,'multiple'=>1,'style'=>'width:100%;'), $this->getProducts('link',$_POST['product_name'],$_POST['product_author'],$_POST['product_publisher'],false,$iId));
		
		// przyciski
		$pForm2->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		$sJS='
					<script>
            $("#publisher_aci").autocomplete({
                 source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetPublishers.php"
             });
					</script>';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm().$pForm2->ShowForm()));
	} // end of Add() function
	
	
	/**
	 * Metoda dodaje ksiazke do tabeli polecanych
	 * 
	 * @param	integer	$iId	- Id ksiazki
	 * @param	integer	$ieditionsId	- Id polecanej ksiazki
	 * @return	bool
	 */
	function addEditions($iId, $iLinkedId) {
		global $aConfig;
		
		$aValues = array(
			'product_id' => $iId,
			'linked_id' => $iLinkedId,
			'order_by' => '#order_by#'
		);
		return Common::Insert($aConfig['tabls']['prefix']."products_editions",
									 				$aValues,
									 				"product_id = ".$iId,
									 				false) !== false;
	} // end of addLinked() method


	/**
	 * Metoda pobiera liste rekordow
	 *
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($iId) {
		global $aConfig;

		// pobranie wszystkich polecanych produktu o $iId
		$sSql = "SELECT A.id, B.name
						 FROM ".$aConfig['tabls']['prefix']."products_editions A
						 JOIN ".$aConfig['tabls']['prefix']."products B
						 ON B.id = A.linked_id
						 WHERE A.product_id = ".$iId."						 
						 ORDER BY A.order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
} // end of Module Class
?>