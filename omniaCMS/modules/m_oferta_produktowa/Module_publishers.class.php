<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - wydawnictwa
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
use Listener\ElasticListener;
use Listener\TabListener;
use Repositories\CeneoTabsRepository;

include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'serie');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': 						$this->Delete($pSmarty, $iId); break;
			case 'insert': 						$this->Insert($pSmarty); break;
			case 'update': 						$this->Update($pSmarty, $iId); break;
			case 'update_linked': 		$this->UpdateLinked($pSmarty, $iId); break;
			case 'edit':
			case 'add': 							$this->AddEdit($pSmarty, $iId); break;
			case 'linked': 						$this->EditLinked($pSmarty, $iId); break;
			case 'categories': 				$this->EditCategories($pSmarty, $iId); break;
			case 'update_categories': $this->UpdateCategories($pSmarty, $iId); break;
			case 'change': 						$this->ChangeBooksForm($pSmarty, $iId); break;
			case 'change_books':			$this->ChangeBooks($pSmarty, $iId); break;
			case 'topublishers':			$this->ToPublishersEdit($pSmarty, $iId); 			break;
			case 'saveLinking': 			$this->SaveLinking($pSmarty); 			break;
			default: 									$this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych wydawnictwa
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true,
			'send_button'=>'topublishers'
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'discount_limit',
				'content'	=> $aConfig['lang'][$this->sModule]['discount_limit'],
				'sortable'	=> true,
				'width'	=> '100'
			),
            array(
                'db_field'	=> 'ignore_discount',
                'content'	=> $aConfig['lang'][$this->sModule]['ignore_discount'],
                'sortable'	=> true,
                'width'	=> '100'
            ),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang'][$this->sModule]['list_created'],
				'sortable'	=> true,
				'width'	=> '155'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang'][$this->sModule]['list_created_by'],
				'sortable'	=> true,
				'width'	=> '105'
			),
			array(
				'db_field'	=> 'modified',
				'content'	=> $aConfig['lang'][$this->sModule]['list_modified'],
				'sortable'	=> true,
				'width'	=> '155'
			),
			array(
				'db_field'	=> 'modified_by',
				'content'	=> $aConfig['lang'][$this->sModule]['list_modified_by'],
				'sortable'	=> true,
				'width'	=> '105'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '35'
			)
		);

		// pobranie liczby wszystkich serii
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_publishers
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
									 (isset($_POST['f_limited']) && $_POST['f_limited'] != '' ? ($_POST['f_limited']=='1'?" AND discount_limit > 0":" AND discount_limit = 0") : '').
                   (isset($_POST['f_check_external_border_time']) && $_POST['f_check_external_border_time'] != '' ? ($_POST['f_check_external_border_time'] == '1' ? " AND check_external_border_time = '1' ":" AND check_external_border_time = '0' ") : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_publishers";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_series', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// stan publikacji
		$aLimited = array(
			array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_limited']),
			array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_limited'])
		);
		$pView->AddFilter('f_limited', $aConfig['lang'][$this->sModule]['f_limited'], $aLimited, $_POST['f_limited']);
    
		// stan publikacji
		$aBorderTime = array(
			array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_set']),
      array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_null']),  
		);
		$pView->AddFilter('f_check_external_border_time', $aConfig['lang'][$this->sModule]['f_check_external_border_time'], $aBorderTime, $_POST['f_check_external_border_time']);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich serii
			$sSql = "SELECT id, name, IF(discount_limit=0,'',discount_limit) AS discount_limit, ignore_discount, created, created_by, modified, modified_by
							 FROM ".$aConfig['tabls']['prefix']."products_publishers
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
										 (isset($_POST['f_limited']) && $_POST['f_limited'] != '' ? ($_POST['f_limited']=='1'?" AND discount_limit > 0":" AND discount_limit = 0") : '').
                     (isset($_POST['f_check_external_border_time']) && $_POST['f_check_external_border_time'] != '' ? ($_POST['f_check_external_border_time'] == '1' ? " AND check_external_border_time = '1' ":" AND check_external_border_time = '0' ") : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
      foreach ($aRecords as $iKey => $aRecord) {
        if ($aRecord['ignore_discount'] == '1') {
          $aRecords[$iKey]['ignore_discount'] = _('TAK');
        } else {
          $aRecords[$iKey]['ignore_discount'] = _('NIE');
        }
      }

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('providers_mapping_list', 'change','series','linked','categories', 'edit', 'delete'),
					'params' => array (
                        'providers_mapping_list' => array('module' => 'm_zamowienia', 'action' => 'publishers_to_providers', 'ppid'=>'{id}'),
												'change'	=> array('id'=>'{id}'),
												'series'	=> array('action' => 'series', 'pid'=>'{id}'),
												'edit'	=> array('id'=>'{id}'),
												'linked'	=> array('id'=>'{id}'),
												'categories'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											),
          'icon' => array(
              'providers_mapping_list' => 'linked2'
          ),
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('add')
		);
		
		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function

	/**
	 * Metoda wyswietla formatke wyboru wydawnictwa do zmapowania
	 * 
	 * @param object $pSmarty
	 * @return void 
	 *
	 */
	
	function ToPublishersEdit(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		$aToLink=array();
		foreach($_POST['delete'] as $iKey => $aItem)
			$aToLink[]=$iKey;
		
		if(empty($aToLink)) {
			//$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['selected_none']);
			$this->Show($pSmarty);
			return;
			}
		$aAuthors=array(array('value'=>'0', 'label'=>'-- wybierz --'));
		
		if(strlen($_POST['author_search'])>2) {
			$sSql='SELECT `name` as label, id as value FROM '.$aConfig['tabls']['prefix'].'products_publishers WHERE `name` LIKE \'%'.$_POST['author_search'].'%\' AND id NOT IN ('.implode(',', $aToLink).')';
			$aAuthors=array_merge($aAuthors,Common::GetAll($sSql));		
			}
			
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_linked2'],$this->getPublishersName($aToLink));
		
		$pForm = new FormTable('search_linked', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'topublishers');
		
		foreach($aToLink as $iItem)
			$pForm->AddHidden('delete['.$iItem.']', '1');
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['mappings_select'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
	
		// przycisk wyszukaj
		$pForm->AddRow('', 
									$pForm->GetTextHTML('author_search',$aConfig['lang'][$this->sModule]['author_search'],$_POST['author_search'],array('style'=>'width:300px;'),'','text',false).'&nbsp;&nbsp;'.
									$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_search'], array()));
		
		$pForm->AddRow($pForm->GetLabelHTML('author_search',$aConfig['lang'][$this->sModule]['author_search'],false)
		,$pForm->GetSelectHTML('selected_author', '', array(), $aAuthors, $_POST['selected_author'], '', false));

		$pForm->AddRow('', 
									$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_send'], array('onclick'=>'ChangeObjValue(\'do\', \'saveLinking\')')));
		
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
		
		
		
	}//koniec metody ToPublishersEdit
	
/**
	 * Metoda zapisuje linkowania wydawnictw i modyfikuje produkty
	 * 
	 * @param object $pSmarty
	 * @return void 
	 *
	 */
	
	function SaveLinking(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		// dolaczenie klasy FormTable
		
		$aToLink=array();
		foreach($_POST['delete'] as $iKey => $aItem)
			$aToLink[]=$iKey;
		
		if(empty($aToLink)) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['selected_none']);
			$this->Show($pSmarty);
			return;
			}
		if($_POST['selected_author']<=0) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['author_none']);
			$this->ToPublishersEdit($pSmarty);
			return;
		}	
		
		$aValues['publisher_id']=intval($_POST['selected_author']);
		Common::BeginTransaction();
		//zaznaczonych linkujemy do wybranego
	if (Common::Update($aConfig['tabls']['prefix']."products_publishers_mappings",$aValues,"publisher_import_id IN (".implode(',', $aToLink).")") === false) {
			$bIsErr = true;
			}
			
	//ksiazki	do wybranego
		if (Common::Update($aConfig['tabls']['prefix']."products",$aValues,"publisher_id IN (".implode(',', $aToLink).")") === false) {
			$bIsErr = true;
			}
			
		//zlinkowanych do zaznaczonych do wybranego	
		if (Common::Update($aConfig['tabls']['prefix']."products_publishers_mappings",$aValues,"publisher_id IN (".implode(',', $aToLink).")") === false) {
				$bIsErr = true;
				}	
		$aValues['publisher']=Common::GetOne('SELECT name FROM '.$aConfig['tabls']['prefix'].'products_publishers WHERE id='.intval($_POST['selected_author']));		
		if (Common::Update($aConfig['tabls']['prefix']."products_shadow",$aValues,"publisher_id IN (".implode(',', $aToLink).")") === false) {
				$bIsErr = true;
				}	

			
		foreach($aToLink as $iItem)
			if($this->deleteItem($iItem) === false)
				$bIsErr=true;
		
		
		if ($bIsErr) {
			Common::RollbackTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['linking_failed']);
			$this->Show($pSmarty);
		}
		else {
			Common::CommitTransaction();
			$this->sMsg.= GetMessage($aConfig['lang'][$this->sModule]['linking_ok'], false);
			
			$this->Show($pSmarty);
		}
		
		
	}//koniec metody SaveLinking
	
	/**
	 * Metoda usuwa wybrane wydawnictwa
	 *  *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanej wydawnictwa
	 * @return 	void
	 */
function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$iDeleted = 0;
		$sDel = '';
		$sFailedToDel = '';

		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			// zabezpieczenie
			if (!$this->publishersAreInUse($_POST['delete'])) {
				// pobranie usuwanych rekordow
				$aItems =& $this->getItemsToDelete($_POST['delete']);
				foreach ($aItems as $aItem) {
					// usuwanie
					if (($iRes = $this->deleteItem((double)$aItem['id'], $aItem['photo'])) === false) {
						$bIsErr = true;
						$sFailedToDel = $aItem['name'];
						break;
					}
					elseif ($iRes === 1) {
						$iDeleted++;
						$sDel .= '"'.$aItem['name'].'", ';
					}

					$authorListener = new ElasticListener();
					$authorListener->onPublisherUpdate($aItem['id']);
				}
				$sDel = substr($sDel, 0, -2);
				if (!$bIsErr) {
					if ($iDeleted > 0) {
						$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
						$this->sMsg = GetMessage($sMsg, false);
						// dodanie informacji do logow
						AddLog($sMsg, false);
					}
				}
				else {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
					$this->sMsg = GetMessage($sMsg);
					// dodanie informacji do logow
					AddLog($sMsg);
				}
			} else {
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['in_use_err']);
			}
		}

		$this->Show($pSmarty);
	} // end of Delete() funciton	



	/**
	 * Metoda dodaje do bazy danych nowa wydawnictwa
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig,$pDB;
		$bIsErr = false;
    $aMatches = NULL;
    
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}

			Common::BeginTransaction();	
			// dodanie serii
			$aValues = array(
				'index_letter' => strtoupper(mb_substr($_POST['name'], 0, 1, 'utf8')),
				'name' => decodeString($_POST['name']),
				'url' => trim($_POST['url']) != '' ? trim($_POST['url']) : 'NULL',
				'show_series' => isset($_POST['show_series']) ? '1' : '0',
        'omit_internal_provider' => isset($_POST['omit_internal_provider']) ? '1' : '0',
				'discount_limit' =>	Common::formatPrice2($_POST['discount_limit']),
				'recount_limit' => isset($_POST['recount_limit']) ? '1' : '0',
				'user_code_ignore_discount_limit' => isset($_POST['user_code_ignore_discount_limit']) ? '1' : '0',
        'check_external_border_time' => isset($_POST['check_external_border_time']) ? '1' : '0',
				'created' => 'NOW()',
				'created_by' => $_SESSION['user']['name']
			);
			
			if ($_FILES['logo']['name'] != '') {
				// dolaczenie klasy ProductImage
				include_once('Image.class.php');
				$oImage = new Image();
				$aSettings = array();
				$aSettings['small_size'] = '130x25';
				
				// przetwarzanie zdjecia
				$oImage->SetImage($aConfig['common']['client_base_path'].$aConfig['common']['publishers_logos_dir'], $_FILES['logo'], $aSettings);
				$oImage->proceedImage();
				$aValues['photo'] = $oImage->GetFileProperty('final_name');
			}		
			
			if (($iNewId = Common::Insert($aConfig['tabls']['prefix']."products_publishers",
												 					  $aValues)) === false) {
				$bIsErr = true;
			}
			
			$sSql="SELECT id FROM ".$aConfig['tabls']['prefix']."products_publishers_import
				 WHERE name = ".$pDB->quoteSmart(stripslashes($_POST['name']));
			$iIId=Common::GetOne($sSql);
			// wydawnictwo istnieje w tabeli ukrytej
			if($iIId == false) {
			
				$aValues = array(
					'name' => decodeString($_POST['name'])
				);
				if (($iIId = Common::Insert($aConfig['tabls']['prefix']."products_publishers_import", $aValues)) === false) {
					$bIsErr = true;
				}
			}
			
			$aValues = array(
				'publisher_id' => $iNewId,
				'publisher_import_id' => $iIId
			);
			if (Common::Insert($aConfig['tabls']['prefix']."products_publishers_mappings",$aValues,'',false) === false) {
					$bIsErr = true;
			}
				
			if (!$bIsErr) {
				// dodano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
												$_POST['name']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				// reset ustawien widoku
				$_GET['reset'] = 2;
				$this->Show($pSmarty);
			}
			else {
				// blad
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
												$_POST['name']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty);
			}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych wydawnictwa
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id serii
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
    $aMatches = NULL;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}

		if ($_POST['include_publishers_products'] == 1 && $_POST['exclude_publishers_products'] == 1) {

			$this->sMsg = GetMessage("Produkty nie mogą być dołaczone do walki i zarazem z niej wykluczone");
			return $this->AddEdit($pSmarty, $iId);
		}

			Common::BeginTransaction();
			// aktualizacja
			$aValues = array(
				'index_letter' => strtoupper(mb_substr($_POST['name'], 0, 1, 'utf8')),
				'name' => decodeString($_POST['name']),
				'url' => trim($_POST['url']) != '' ? trim($_POST['url']) : 'NULL',
				'show_series' => isset($_POST['show_series']) ? '1' : '0',
        'omit_internal_provider' => isset($_POST['omit_internal_provider']) ? '1' : '0',
				'discount_limit' =>	Common::formatPrice2($_POST['discount_limit']),
                'ignore_discount' => isset($_POST['ignore_discount']) ? '1' : '0',
                'include_publishers_products' => isset($_POST['include_publishers_products']) ? '1' : '0',
                'exclude_publishers_products' => isset($_POST['exclude_publishers_products']) ? '1' : '0',
				'recount_limit' => isset($_POST['recount_limit']) ? '1' : '0',
				'user_code_ignore_discount_limit' => isset($_POST['user_code_ignore_discount_limit']) ? '1' : '0',
        'check_external_border_time' => isset($_POST['check_external_border_time']) ? '1' : '0',
				'modified' => 'NOW()',
				'modified_by' => $_SESSION['user']['name']
			);
			
			if ($_FILES['logo']['name'] != '') {
				// DODAC USUWANIE STAREGO LOGOTYPU!!
				
				
				// dolaczenie klasy ProductImage
				include_once('Image.class.php');
				$oImage = new Image();
				$aSettings = array();
				$aSettings['small_size'] = '130x25';
				
				// przetwarzanie zdjecia
				$oImage->SetImage($aConfig['common']['client_base_path'].$aConfig['common']['publishers_logos_dir'], $_FILES['logo'], $aSettings);
				$oImage->proceedImage();
				$aValues['photo'] = $oImage->GetFileProperty('final_name');
				
				if(!empty($_POST['old_logo']) && !empty($aValues['photo'])){
					@unlink($aConfig['common']['client_base_path'].$aConfig['common']['publishers_logos_dir'].'/'.$_POST['old_logo']);
				}
			}
			
			if (Common::Update($aConfig['tabls']['prefix']."products_publishers",
												 $aValues,
												 "id = ".$iId) === false) {
				$bIsErr = true;
			}
			
			/*if(!$bErr) {
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_publishers_mappings
				WHERE publisher_id = ".$iId;
				if(Common::Query($sSql)===false) {
					$bIsErr = true;
				}
			}
			if(!$bErr) {
				if(!empty($_POST['publisher_mapping'])) {
					foreach($_POST['publisher_mapping'] as $iMId) {
						$aValues = array(
							'publisher_id' => $iId,
							'publisher_import_id' => $iMId
						);
						if (Common::Insert($aConfig['tabls']['prefix']."products_publishers_mappings",$aValues,'',false) === false) {
								$bIsErr = true;
						}
					}
				}
			}*/
			
			$aShadows = $this->getShadowsWithPublisher($iId);
			if(!empty($aShadows)) {
				foreach($aShadows as $iShadowId) {
					$aValues = array(
						'publisher' => decodeString($_POST['name']),
						'discount_limit' =>	Common::formatPrice2($_POST['discount_limit'])
					);
					Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues,'id = '.$iShadowId);
				}
			}

	
			if (!$bIsErr) {
				// zaktualizowano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
												$_POST['name']);

				$authorListener = new ElasticListener();
				$authorListener->onPublisherUpdate($_GET['id']);

				if ($_POST['exclude_publishers_products'] == 1) {

					global $pDbMgr;

					$publisherId = $_GET['id'];
					$productIds = $pDbMgr->GetCol('profit24', "select id from products where publisher_id = $publisherId");

					$tabListener = new TabListener($productIds, CeneoTabsRepository::CENEO_OUT_TYPE);
					$tabListener->onAddOut();
				}

				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				$this->Show($pSmarty);
			}
			else {
				// nie zaktualizowano
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
												$_POST['name']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iId);
			}
		
	} // end of Update() funciton
	
	/**
	 * Metoda aktualizuje w bazie danych wydawnictwa
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id serii
	 * @return	void
	 */
	function UpdateLinked(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}

			Common::BeginTransaction();
		
		

			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_publishers_mappings
			WHERE publisher_id = ".$iId;
			if(Common::Query($sSql)===false) {
				$bIsErr = true;
			}

			if(!$bErr) {
				if(!empty($_POST['publisher_mapping'])) {
					foreach($_POST['publisher_mapping'] as $iMId) {
						$aValues = array(
							'publisher_id' => $iId,
							'publisher_import_id' => $iMId
						);
						if (Common::Insert($aConfig['tabls']['prefix']."products_publishers_mappings",$aValues,'',false) === false) {
								$bIsErr = true;
						}
					}
				}
			}
			
			
			if (!$bIsErr) {
				// zaktualizowano
				Common::CommitTransaction();

				$authorListener = new ElasticListener();
				$authorListener->onPublisherUpdate($iId);

				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_linked_ok'],
												$this->getPublisherName($iId));
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				$this->Show($pSmarty);
			}
			else {
				// nie zaktualizowano
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_linked_err'],
												$this->getPublisherName($iId));
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iId);
			}
		
	} // end of Update() funciton
	
	/**
	 * Metoda aktualizuje w bazie danych wydawnictwa
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id serii
	 * @return	void
	 */
	function UpdateCategories(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}

			Common::BeginTransaction();
			
			$aValues=array(
				'main_category' => intval($_POST['main_category'])>0?intval($_POST['main_category']):'NULL',
				'news_category' => intval($_POST['news_category'])>0?intval($_POST['news_category']):'NULL'
			);
		
		if (Common::Update($aConfig['tabls']['prefix']."products_publishers",
												 $aValues,
												 "id = ".$iId) === false) {
				$bIsErr = true;
			}

						
			if (!$bIsErr) {
				// zaktualizowano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_linked_ok'],
												$this->getPublisherName($iId));
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				$this->Show($pSmarty);
			}
			else {
				// nie zaktualizowano
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_linked_err'],
												$this->getPublisherName($iId));
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iId);
			}
		
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji wydawnictwa
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanej serii
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT *, photo As old_logo
							 FROM ".$aConfig['tabls']['prefix']."products_publishers
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
			$aData['logo'] = $aData['photo'];
			//$aData['publisher_mapping'] = $this->getMappings($iId);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($sParName) && !empty($sParName)) {
			$sHeader .= ' "'.$aData['name'].'"';
		}

		$pForm = new FormTable('products_publishers', $sHeader, array('action'=>phpSelf(array('id'=>$iId)), 'enctype'=>'multipart/form-data'), array('col_width'=>350), $aConfig['common']['js_validation']);
		if ($iId > 0) {
			$pForm->AddHidden('do', 'update');
		}
		else {
			$pForm->AddHidden('do', 'insert');
		}
		// tytul
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>255, 'style'=>'width: 350px;'));
		
	// adres strony producenta
	//	$pForm->AddText('url', $aConfig['lang'][$this->sModule]['url'], $aData['url'], array('maxlength'=>255, 'style'=>'width: 250px;'), '', 'text', false);
		
		if (file_exists($aConfig['common']['client_base_path'].$aConfig['common']['publishers_logos_dir'].'/'.$aData['logo'])) {
			$pForm->AddHidden('old_logo',$aData['logo']);
			$pForm->AddProductImage('logo', $aConfig['lang'][$this->sModule]['logo'], $aConfig['common']['publishers_logos_dir'], $aData['logo']);
		}
		else {
			$pForm->AddProductImage('logo', $aConfig['lang'][$this->sModule]['logo'], $aConfig['common']['publishers_logos_dir']);
		}
    
		$pForm->AddCheckBox('show_series', $aConfig['lang'][$this->sModule]['show_series'], array(), '', !empty($aData['show_series']), false);
    
		/*
			// wybor kategorii ABE
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['mappings_section'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
	// przycisk wyszukaj
		$pForm->AddRow($pForm->GetLabelHTML('publisher_search',$aConfig['lang'][$this->sModule]['publisher_search'],false), 
									$pForm->GetTextHTML('publisher_search',$aConfig['lang'][$this->sModule]['publisher_search'],$_POST['publisher_search'],array('style'=>'width:300px;'),'','text',false).'&nbsp;&nbsp;'.
									$pForm->GetInputButtonHTML('search', $aConfig['lang'][$this->sModule]['button_search'], array('onclick'=>'document.getElementById(\'do\').value=\'add\'; sendMuliselects(); this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button'));
		$pForm->AddMergedRow(
									$pForm->GetSelectHtml('found_publishers',$aConfig['lang'][$this->sModule]['found_publishers'], array('size'=>20,'style'=>'width:450px;'), $this->getPublishers($_POST['publisher_search']),'','',false).'&nbsp;'.
									$pForm->GetInputButtonHTML('add_publ', $aConfig['lang'][$this->sModule]['add_publ'], array('onclick'=>'addPublisherMapping();'), 'button').'&nbsp;'.
									$pForm->GetInputButtonHTML('del_publ', $aConfig['lang'][$this->sModule]['del_publ'], array('onclick'=>'delPublisherMapping();'), 'button').'&nbsp;'.
									$pForm->GetSelectHtml('publisher_mapping',$aConfig['lang'][$this->sModule]['publisher_mapping'], array('size'=>20,'style'=>'width:450px;','multiple'=>1), $this->getPublishersForIds($aData['publisher_mapping']),'','',false),array());
		*/
    
    $pForm->AddMergedRow(_('Rabaty'), array('class' => 'merged'));
		// limit rabatu w procentach
		$pForm->AddText('discount_limit', $aConfig['lang'][$this->sModule]['discount_limit'], Common::formatPrice($aData['discount_limit']), array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');

		$pForm->AddCheckBox('ignore_discount', $aConfig['lang'][$this->sModule]['ignore_discount'], array(), '', !empty($aData['ignore_discount']), false);
		$pForm->AddCheckBox('recount_limit', $aConfig['lang'][$this->sModule]['recount_limit'], array(), '', !empty($aData['recount_limit']), false);
		$pForm->AddCheckBox('user_code_ignore_discount_limit', $aConfig['lang'][$this->sModule]['user_code_ignore_discount_limit'], array(), '', !empty($aData['user_code_ignore_discount_limit']), false);
		$pForm->AddCheckBox('include_publishers_products', _('Dołącz produkty wydawnictwa do walki ceneo'), array(), '', !empty($aData['include_publishers_products']), false);
		$pForm->AddCheckBox('exclude_publishers_products', _('Wyklucz wszystkie produkty wydawnictwa do walki ceneo (zaznaczenie spowoduje natychmiastowe usuniecie cenników wydawnictwa z każdej księgarni)'), array(), '', !empty($aData['exclude_publishers_products']), false);


		$pForm->AddMergedRow(_('Auto-odznadzanie zamówień'), array('class' => 'merged'));
    $pForm->AddCheckBox('check_external_border_time', $aConfig['lang'][$this->sModule]['check_external_border_time'], array(), '', !empty($aData['check_external_border_time']), false);
		$pForm->AddCheckBox('omit_internal_provider', $aConfig['lang'][$this->sModule]['omit_internal_provider'], array(), '', !empty($aData['omit_internal_provider']), false);
    
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'sendMuliselects();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
/*
		$sJS.='
			function addPublisherMapping(){
				var elSelSrc = document.getElementById(\'found_publishers\'); 
	 			var elSelDest = document.getElementById(\'publisher_mapping\');
	 			
		 			var val = elSelSrc.options[elSelSrc.selectedIndex].value;
		 			var txt =  elSelSrc.options[elSelSrc.selectedIndex].title;
		 			
		 			if (elSelDest.options != undefined) {
	    			for (var i = 0; i < elSelDest.options.length; i++) {
	      			if (elSelDest.options[i].value == val) {
	        			return false;
	      			}
	    			}
	  			}
	        elSelDest.options.add(new Option(txt,val));
			}
			
			function delPublisherMapping(){
				var elSelDest = document.getElementById(\'publisher_mapping\');
				elSelDest.remove(elSelDest.selectedIndex);
			}	

		';
		$pForm->setAdditionalJS($sJS);
		*/
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	/**
	 * Metoda tworzy formularz edycji powiazanych
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanej serii
	 */
	function EditLinked(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			$aData['publisher_mapping'] = $this->getMappings($iId);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_linked'],$this->getPublisherName($iId));

		

		$pForm = new FormTable('search_linked', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'linked');
			
			// wybor kategorii ABE
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['mappings_section'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
	// przycisk wyszukaj
		$pForm->AddRow($pForm->GetLabelHTML('publisher_search',$aConfig['lang'][$this->sModule]['publisher_search'],false), 
									$pForm->GetTextHTML('publisher_search',$aConfig['lang'][$this->sModule]['publisher_search'],$_POST['publisher_search'],array('style'=>'width:300px;'),'','text',false).'&nbsp;&nbsp;'.
						//			$pForm->GetInputButtonHTML('search', $aConfig['lang'][$this->sModule]['button_search'], array('onclick'=>'document.getElementById(\'do\').value=\'add\'; sendMuliselects(); this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button'));
						$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_search'], array('onclick'=>'sendMuliselects();')));
//	$pForm2 = new FormTable('products_publishers', '', array('action'=>phpSelf(array('id'=>$iId)), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
	//	$pForm2->AddHidden('do', 'update_linked');					
		$pForm->AddMergedRow(
									$pForm->GetSelectHtml('found_publishers',$aConfig['lang'][$this->sModule]['found_publishers'], array('size'=>20,'style'=>'width:450px;'), $this->getPublishers($_POST['publisher_search']),'','',false).'&nbsp;'.
									$pForm->GetInputButtonHTML('add_publ', $aConfig['lang'][$this->sModule]['add_publ'], array('onclick'=>'addPublisherMapping();'), 'button').'&nbsp;'.
									$pForm->GetInputButtonHTML('del_publ', $aConfig['lang'][$this->sModule]['del_publ'], array('onclick'=>'delPublisherMapping();'), 'button').'&nbsp;'.
									$pForm->GetSelectHtml('publisher_mapping',$aConfig['lang'][$this->sModule]['publisher_mapping'], array('size'=>20,'style'=>'width:450px;','multiple'=>1), $this->getPublishersForIds($aData['publisher_mapping']),'','',false),array());
		
		//$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'sendMuliselects();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('save', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'document.getElementById(\'do\').value=\'update_linked\'; sendMuliselects(); this.form.submit();'), 'button').'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		$sJS.='
      
		function sendMuliselects(){
			$("[id$=\'_mapping\']").each(function(index) {
					elSelDest = document.getElementById($(this).attr("id")); 
					if(elSelDest){
						setMultiSelectOnSend(elSelDest);
					}
			});
		}

		function setMultiSelectOnSend(oObj){

			if (oObj.options != undefined) {
				for (var i = 0; i < oObj.options.length; i++) {
					oObj.options[i].selected =true;
				}
			}
		}
    
			function addPublisherMapping(){
				var elSelSrc = document.getElementById(\'found_publishers\'); 
	 			var elSelDest = document.getElementById(\'publisher_mapping\');
	 			
		 			var val = elSelSrc.options[elSelSrc.selectedIndex].value;
		 			var txt =  elSelSrc.options[elSelSrc.selectedIndex].title;
		 			
		 			if (elSelDest.options != undefined) {
	    			for (var i = 0; i < elSelDest.options.length; i++) {
	      			if (elSelDest.options[i].value == val) {
	        			return false;
	      			}
	    			}
	  			}
	        elSelDest.options.add(new Option(txt,val));
			
			}
			
			function delPublisherMapping(){
				var elSelDest = document.getElementById(\'publisher_mapping\');
				elSelDest.remove(elSelDest.selectedIndex);
			}	

		';
		$pForm->setAdditionalJS($sJS);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	/**
	 * Metoda tworzy formularz edycji powiazanych
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanej serii
	 */
	function EditCategories(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."products_publishers
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
			//$aData['publisher_mapping'] = $this->getMappings($iId);
		}
		
		

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_categories'],$aData['name']);
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		

		$pForm = new FormTable('publishers_categories', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_categories');
			
		$pForm->AddSelect('main_category',$aConfig['lang'][$this->sModule]['main_category'], array('style'=>'width:300px;'), $this->getTopProductsCategories(), $aData['main_category'],'',false);
		$pForm->AddSelect('news_category',$aConfig['lang'][$this->sModule]['news_category'],array(),array_merge(array(array('value'=>0,'label'=>$aConfig['lang']['common']['choose'])),getModulePages('m_powiazane_oferta_produktowa',$this->iLangId,true,'','nowosci')),$aData['news_category'],'',false);
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'sendMuliselects();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function


	/**
	 * Metoda sprawdza czy wydawnictwa o podanych Id sa uzywane w produktach
	 * 
	 * @param	array	$aIds	- Id usuwanych serii
	 * @return	bool
	 */
	function publishersAreInUse($aIds) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE publisher_id IN (".implode(',', $aIds).")";
		return intval(Common::GetOne($sSql)) > 0;
	} // end of seriesAreInUse method
	

	
	/**
	 * Metoda pobiera liste wydawnictw do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id serii
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, name, photo
				 		 FROM ".$aConfig['tabls']['prefix']."products_publishers
					 	 WHERE id IN (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa wydawnictwo
	 * 
	 * @param	integer	$iId	- Id serii do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId, $sPhoto) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_publishers
						 WHERE id = ".$iId;
		$mResult = Common::Query($sSql);
		if ($sPhoto != '' && $mResult === 1) {
			// usuwanie logo producenta
			@unlink($aConfig['common']['client_base_path'].$aConfig['common']['publishers_logos_dir'].'/'.$sPhoto);
		}
		return $mResult;
	} // end of deleteItem() method
	/**
	 * Metoda usuwa wydawnictwo z importu
	 * 
	 * @param	integer	$iId	- Id serii do usuniecia
	 * @return	mixed	
	 */
	function deleteItem2($iId, $sPhoto) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_publishers_import
						 WHERE id = ".$iId;
		$mResult = Common::Query($sSql);
		if ($sPhoto != '' && $mResult === 1) {
			// usuwanie logo producenta
			@unlink($aConfig['common']['client_base_path'].$aConfig['common']['publishers_logos_dir'].'/'.$sPhoto);
		}
		return $mResult;
	} // end of deleteItem() method
	
	function &getShadowsWithPublisher($iId) {
	global $aConfig;
		// wybiera grupy kategorii dla tego produktu
	  $sSql = "SELECT id
	  				 FROM ".$aConfig['tabls']['prefix']."products_shadow
	  				 WHERE publisher_id =".$iId;
	  return Common::GetCol($sSql);
	} // end of getShadowsWithPublisher()
	
		/**
	 * Metoda pobiera wydawców
	 * 
	 * @return	array
	 */
	function &getPublishers($sName='') {
		global $aConfig;
		$aItems=array();
		if(!empty($sName)){
			$sSql = "SELECT id value, name AS label, name AS title
							 FROM ".$aConfig['tabls']['prefix']."products_publishers_import
							 WHERE name LIKE '%".$sName."%'
							 ORDER BY name";
			$aItems =Common::GetAll($sSql);
		}
		return $aItems;
	} // end of getPublishers() method
	
			/**
	 * Metoda pobiera wydawców
	 * 
	 * @return	array
	 */
	function &getPublishersForIds($aIds) {
		global $aConfig;
		$aItems=array();
		if(!empty($aIds)) {
			foreach($aIds as $iKey=>$iId) {
				$sSql = "SELECT id value, name AS label, name AS title
								 FROM ".$aConfig['tabls']['prefix']."products_publishers_import
								 WHERE id = ".$iId;
				$aItems[$iKey] = Common::GetRow($sSql); 
			}
		}
		return $aItems;
	} // end of getPublishers() method
	
	function getMappings($iId) {
		global $aConfig;
		$sSql = "SELECT publisher_import_id
							FROM ".$aConfig['tabls']['prefix']."products_publishers_mappings
							WHERE publisher_id = ".$iId;
		return Common::GetCol($sSql);
	}
	
	function getPublisherName($iId) {
		global $aConfig;
		$sSql = "SELECT name
							FROM ".$aConfig['tabls']['prefix']."products_publishers
							WHERE id = ".$iId;
		return Common::GetOne($sSql);
	}
	
	
	function getPublishersName($aList) {
		global $aConfig;
		$sBuf='';
		
		foreach($aList as $iItem) {
			$sSql = "SELECT name FROM ".$aConfig['tabls']['prefix']."products_publishers WHERE id = ".$iItem;
			$sBuf.=Common::GetOne($sSql).', ';
			}	 
			 
		return substr($sBuf,0,-2);
		
		return Common::GetOne($sSql);
	}
	
	/**
	 * Metoda tworzy formularz dodawania / edycji wydawnictwa
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanej serii
	 */
	function ChangeBooksForm(&$pSmarty, $iId) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT name
							 FROM ".$aConfig['tabls']['prefix']."products_publishers
							 WHERE id = ".$iId;
			$sName = Common::GetOne($sSql);

		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_change'];
		$sHeader .= ' "'.$sName.'"';


		$pForm = new FormTable('products_change', $sHeader, array('action'=>phpSelf(array('id'=>$iId)), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'change_books');
		$pForm->AddHidden('old_publisher', $sName);
		
		$pForm->AddText('product_publisher',$aConfig['lang'][$this->sModule]['publisher_search'], $aData['product_publisher'],array('id'=>'publisher_aci','style'=>'width:300px;','maxlength'=>255),'','text' ,false);

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'sendMuliselects();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		$sJS='
					<script>
						$(document).ready(function(){
              $("#publisher_aci").autocomplete({
                source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetPublishers.php"
              });
						});
					</script>';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	

	function ChangeBooks(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->ChangeBooksForm($pSmarty, $iId);
			return;
		}
		
		$sSql = "SELECT id, name 
					 FROM ".$aConfig['tabls']['prefix']."products_publishers
					 WHERE name LIKE ".Common::Quote(stripslashes($_POST['product_publisher'])); 
		$aNewPublisher = Common::GetRow($sSql);
		if(!empty($aNewPublisher)) {
			
			Common::BeginTransaction();
			$sSql = "SELECT id, name
							FROM ".$aConfig['tabls']['prefix']."products
							WHERE publisher_id = ".$iId;
			$aProducts = Common::GetAll($sSql);
			if(!empty($aProducts)) {
				foreach($aProducts as $aProduct) {
					$aValues = array(
						'publisher_id' => $aNewPublisher['id']
					);
					if (Common::Update($aConfig['tabls']['prefix']."products",$aValues,'id = '.$aProduct['id']) === false) {
							$bIsErr = true;
					} 
					// aktualizacja cienia
					if($this->existShadow($aProduct['id'])) {
						$aValues = array(
							'publisher_id' => $aNewPublisher['id'],
							'publisher' => $aNewPublisher['name']
						);
						if (Common::Update($aConfig['tabls']['prefix']."products_shadow",$aValues,'id = '.$aProduct['id']) === false) {
							$bIsErr = true;
						} 
					}
				}
			}
			if (!$bIsErr) {
				// zaktualizowano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['change_ok'],
												$_POST['old_publisher'],
												$aNewPublisher['name'],
												count($aProducts));
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				$this->Show($pSmarty);
			}
			else {
				// nie zaktualizowano
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['change_err'],
												$_POST['old_publisher'],
												$aNewPublisher['name']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->ChangeBooksForm($pSmarty, $iId);
			}
		} else {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['incorrect_publisher_err'],
												$_POST['product_publisher']);
			$this->sMsg = GetMessage($sMsg, true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->ChangeBooksForm($pSmarty, $iId);
		}
	} // end of Update() funciton
} // end of Module Class
?>