<?php

namespace omniaCMS\modules\m_oferta_produktowa;

use Admin;
use DatabaseManager;
use FormTable;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use Smarty;
use Validator;
use View;

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-09-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */
/**
 * Description of Module_additional_index
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__oferta_produktowa__additional_index implements ModuleEntity, SingleView  {

  /**
   *
   * @var string
   */
	private $sMsg;

  /**
   *
   * @var string
   */
	private $sModule;

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;
  
  /**
   *
   * @var int
   */
  private $iPId;
	
  public function __construct(Admin $oClassRepository) {
    
    $this->oClassRepository = $oClassRepository;
    $this->em = $this->oClassRepository->entityManager;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule.$this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    $this->iPId = $_GET['pid'];
  }
  
  /**
   * 
   * @global array $aConfig
   * @return array
   */
  public function doEdit() {
    $iId = $_GET['id'];
    return $this->addEditForm($iId);
  }
  
  /**
   * 
   * @global array $aConfig
   * @return array
   */
  public function doAdd() {
    return $this->addEditForm();
  }
  
  /**
   * 
   */
  public function doSaveAddEdit() {
    global $aConfig;
    $iId = $_GET['id'];
    
    $bIsErr = false;
    include_once('Form/Validator.class.php');
		$oValidator = new Validator();
    
    
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			return $this->doDefault();
		}
    
    $aValues = array(
        'source_index' => $_POST['source_index'],
        'main_index' => isset($_POST['main_index']),
        'source_id' => $_POST['source_id']
    );
    
    if ($iId > 0) {
      if ($this->pDbMgr->Update('profit24', 'products_to_sources', $aValues, ' id = '.$iId.' AND product_id = '.$this->iPId) === false) {
        $bIsErr = true;
      }
    } else {
      $aValues['product_id'] = $this->iPId;
      if ($this->pDbMgr->Insert('profit24', 'products_to_sources', $aValues) === false) {
        $bIsErr = true;
      }
    }
    
    if ($bIsErr === false) {
      $sMsg = _('Zmiany zostały zapisane');
      AddLog($sMsg, $bIsErr);
      $this->sMsg .= GetMessage($sMsg, $bIsErr);
    } else {
      $sMsg = _('Wystąpił błąd podczas zapisywania zmian ' . nl2br(print_r($aValues, true)));
      AddLog($sMsg, $bIsErr);
      $this->sMsg .= GetMessage($sMsg, $bIsErr);
    }
    return $this->doDefault();
  }
  
  /**
   * 
   * @return array
   */
  private function getSources() {
    
    $sSql = 'SELECT symbol AS label, id AS value 
             FROM sources ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iId
   * @return string
   */
  public function addEditForm($iId = 0) {
    global $aConfig;
    
    if ($iId > 0) {
      $aIndex = $this->getIndexData($iId, array('*'));
    }
    include_once('lib/Form/FormTable.class.php');
    $pForm = new FormTable('additional_index_addedit', _('Indeks produktu "'.$this->getProductName($this->iPId).'"'), array('action' => phpSelf(array('id' => $iId))));
    $pForm->AddHidden('do', 'saveAddEdit');
    $pForm->AddText('source_index', 'Indeks '.ucfirst($aIndex['symbol']), $aIndex['source_index'], array(), true);
    $pForm->AddCheckBox('main_index', _('Główny index'), array(), '', ($aIndex['main_index'] == '1' ? true : false), false);
    $pForm->AddSelect('source_id', _('Źródło'), array() , addDefaultValue($this->getSources()), $aIndex['source_id'], '', true);
    
    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_' . ($iId > 0 ? 1 : 0)], array()) . '&nbsp;&nbsp;' . 
            $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf() . '\');'), 'button'));
    return $pForm->ShowForm();
  }
  
  /**
   * 
   * @param int $iId
   * @param array $aCols
   * @return array
   */
  private function getIndexData($iId, $aCols) {
    
    $sSql = 'SELECT '.implode(',', $aCols).' 
             FROM products_to_sources AS PTS
             JOIN sources AS S
              ON S.id = PTS.source_id
             WHERE PTS.id = '.$iId.' AND PTS.product_id = '.$this->iPId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
  
  /**
   * 
   * @return string
   */
  public function doDelete() {
    
    $iId = $_GET['id'];
    if ($iId > 0) {
      
      $sSql = 'DELETE FROM products_to_sources WHERE id = '.$iId.' AND product_id = '.$this->iPId.' LIMIT 1';
      if ($this->pDbMgr->Query('profit24', $sSql) === false) {
        $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania rekordu'));
        return $this->doDefault();
      } else {
        $this->sMsg .= GetMessage(_('Rekord został usunięty'), false);
        return $this->doDefault();
      }
    } else {
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania rekordu'));
      return $this->doDefault();
    }
  }

  /**
   * 
   * @global array $aConfig
   * @return string
   */
  public function doDefault() {
    global $aConfig;
    $aData = array();
    
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
    
		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> _('Indeksy produktu: "'.$this->getProductName($this->iPId)).'"',
			'refresh'	=> true,
			'search'	=> false,
      'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'symbol',
				'content'	=> _('Źródło'),
				'sortable'	=> true,
				'width'	=> '45'
			),
			array(
				'db_field'	=> 'index',
				'content'	=> _('Index'),
				'sortable'	=> true,
				'width'	=> '45'
			),
			array(
				'db_field'	=> 'main_index',
				'content'	=> _('Główny index'),
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

    // ustawienia dla poszczegolnych kolumn rekordu
    $aColSettings = array(
      'id'	=> array(
        'show'	=> false
      ),
      'action' => array (
        'actions'	=> array ('edit','delete'),
        'params' => array (
                      'edit'	=> array('pid' => $this->iPId, 'id'=>'{id}'),
                      'delete'	=> array('pid' => $this->iPId, 'id'=>'{id}')
                    ),
        'show' => false
      )
    );
      
    include_once('View/View.class.php');
    $oView = new View(_('bank_accounts'), $aHeader, $aAttribs);
    
    $aCols = array('PTS.id', 'S.symbol', 'PTS.source_index', 'PTS.main_index');
    
    $sSql = 'SELECT %cols
             FROM products_to_sources AS PTS
             JOIN sources AS S
               ON PTS.source_id = S.id
             WHERE product_id = '.$this->iPId.'
             %filters
             ';
    $sGroupBySQL = '';
    $aSearchCols = array('PTS.source_index');
    $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, '', $sGroupBySQL, ' PTS.id DESC ');
    $oView->AddRecordsHeader($aRecordsHeader);
    $oView->setActionColSettings($aColSettings);
    
    $aRecordsFooter = array(array('go_back'), array('add'));
    $aRecordsFooterParametrs = array('go_back' => array(array(), array('action')));
		$oView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParametrs);
    return ShowTable($oView->Show());
  }

  /**
   * 
   * @return string
   */
  public function getMsg() {
    return $this->sMsg;
  }
  
  /**
   * 
   * @param int $iPId
   * @return string
   */
  private function getProductName($iPId) {
    
    $sSql = 'SELECT name FROM products WHERE id = '.$iPId;
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
  

  public function parseViewItem(array $aItem) {
    if ($aItem['main_index'] == '1') {
      $aItem['main_index'] = _('TAK');
    } else {
      $aItem['main_index'] = _('NIE');
    }
    return $aItem;
  }

  public function resetViewState() {
    resetViewState($this->sModule);
  }
}
