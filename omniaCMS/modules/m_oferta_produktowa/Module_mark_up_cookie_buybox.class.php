<?php
use table\keyValue;

/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-23
 * Time: 12:47
 */
class Module
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    // dane konfiguracyjne
    var $aSettings;

    private $oKeyValue;

    private $aServicesSupported = ['np', 'profit24', 'mestro'];

    const TABLE_NAME = 'mark_up_cookie';

    /**
     * Konstruktor klasy
     *
     * @return    void
     */
    function Module(&$pSmarty)
    {
        global $aConfig;

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        $sDo = '';
        $iId = 0;

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }
        if (isset($_GET['id'])) {
            $iId = intval($_GET['id']);
        }

        // wydzielenie uprawnien
        $this->aPrivileges =& $_SESSION['user']['privileges'];


        $this->setAllKeyValueObjects($_GET['action'], $this->aServicesSupported);

        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }

        switch ($sDo) {
            case 'insert_update':
                $this->InsertUpdate($pSmarty);
                break;

            default:
                $this->Edit($pSmarty);
                break;
        }
    } // end of Module() method


    /**
     * @param $sKeyValueType
     * @param $aServices
     */
    private function setAllKeyValueObjects($sKeyValueType, $aServices)
    {
        foreach ($aServices as $sService) {
            $this->oKeyValue[$sService] = new keyValue(self::TABLE_NAME, $sService);
        }
    }


    /**
     * @param $pSmarty
     */
    public function InsertUpdate($pSmarty)
    {
        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Edit($pSmarty);
            return;
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->Edit($pSmarty);
            return;
        }


        foreach ($this->aServicesSupported as $sService) {
            if ($this->oKeyValue[$sService]->insertUpdateByPost(1, $_POST[$sService]) === false) {
                $this->sMsg = GetMessage(_("Wystąpił błąd podczas zapisu"));
                return $this->Edit($pSmarty);
            } else {
                $this->sMsg = GetMessage(_("Zapisano zmiany"), false);
            }
        }

        return $this->Edit($pSmarty);
    }


    public function InsertUpdateMarkUpCookieAllOtherUsers($pSmarty)
    {
        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Edit($pSmarty);
            return;
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->Edit($pSmarty);
            return;
        }

        foreach ($this->aServicesSupported as $sService) {
            if ($this->oKeyValue[$sService]->insertUpdateByPost(1, $_POST[$sService]) === false) {
                $this->sMsg = GetMessage(_("Wystąpił błąd podczas zapisu"));
                return $this->Edit($pSmarty);
            } else {
                $this->sMsg = GetMessage(_("Zapisano zmiany"), false);
            }
        }

        return $this->Edit($pSmarty);
    }


    /**
     * @param $pSmarty
     */
    public function Edit($pSmarty)
    {
        global $aConfig;

        $aData = array();
        $sHtml = '';

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
        $sHeader = sprintf(_('Zmniejszenie rabatu na produkt po zejściu z BuyBox'));
        $pForm = new FormTable('mark_up_cookie', $sHeader);
        $pForm->AddHidden('do', 'insert_update');


        foreach ($this->aServicesSupported as $sService) {
            $aData = $this->oKeyValue[$sService]->getAllByIdDest(1);
            $pForm->AddMergedRow(_('Serwis - ' . $aConfig['common'][$sService]['client_base_url_http_no_slash']), ['class' => 'merged']);
            $pForm->AddText($sService.'[key_value][mark_up]', _('Odejmowany procent rabatu:'), isset($aData['mark_up']) && $aData['mark_up'] != '' ? Common::formatPrice($aData['mark_up']) : '', array('maxlength' => 12, 'style' => 'width: 75px;', 'onchange' => 'calculateBasePrice();'), '', 'ufloat', true);
            $pForm->AddText($sService.'[key_value][cookie_name]', _('Nazwa ciasteczka'), $aData['cookie_name']);
            $pForm->AddText($sService.'[key_value][lifetime]', _('Czas obowiązywania w dniach'), $aData['lifetime'], [], '', 'text');
        }


        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz'), array()) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
    }
}