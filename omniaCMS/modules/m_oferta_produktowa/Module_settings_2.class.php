<?php
/**
 * Klasa Module do obslugi konfiguracji stron modulu 'Oferta produktowa'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Settings {
  
  // nazwa modulu - do langow
	var $sModule;
	
	// Id strony dla ktorej edytowane sa ustawienia
	var $iId;
	
	// sciezka do katalogu zawierajacego szablony czesci klienckiej modulu
	var $sTemplatesDir;
	
	// lista szablonow czesci klienckiej modulu
	var $sTemplatesList;

	// plik XML z domyslna konfiguracja
	var $sXMLFile;
	
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$sModule, &$iId) {
		global $aConfig;
		$this->sModule = $sModule;
		$this->iId = $iId;
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/modules/'.$this->sModule;
		$this->sTemplatesList =& GetTemplatesList($this->sTemplatesDir);
		$this->sXMLFile = 'config/'.$sModule.'/default_config.xml';
		
		// pobranie konfiguracji dla strony
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."products_settings 
						 WHERE page_id = ".$this->iId;
		$aConfig['settings'][$this->sModule] =& Common::GetRow($sSql);
		
		
		if (empty($aConfig['settings'][$this->sModule])) {
			// brak ustawien - pobranie domyslnych z pliku XML
			$aConfig['settings'][$this->sModule] =& getModuleConfig($this->sXMLFile);
		}
	} // end Settings() function
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach strony newslettera
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;
		$aValues = array();
		$bIsErr=false;
		$aParents=$this->getPathIDs($this->iId,$_SESSION['lang']['id']);
	//usuniecie poprzednich mapowań kategorii azymut
		$sSql="DELETE FROM ".$aConfig['tabls']['prefix']."products_azymut_mapping
					WHERE page_id = ".$this->iId;
		Common::Query($sSql);
	// dodanie mapowań kategorii azymut
		if(!empty($_POST['azymut_mapping'])){
			foreach($_POST['azymut_mapping'] as $iCatId){
				$bIsErr =($this->AddAzymutMapping($this->iId,$iCatId)== false);
				// dodanie mapowan w gore drzewa lokalnego
				if(!empty($aParents)){
					foreach($aParents as $iParent){
						if(!$this->existAzymutMapping($iParent,$iCatId)){
							$bIsErr =($this->AddAzymutMapping($iParent,$iCatId)== false);
						}
					}
				}
				// dodanie mapowan w dol drzewa azymut
//				$aChilds=$this->getAzymutChilds($iCatId);
//				if(!empty($aChilds)){
//					foreach($aChilds as $iChild){
//						if(!$this->existAzymutMapping($this->iId,$iChild)){
//							$bIsErr =($this->AddAzymutMapping($this->iId,$iChild)== false);
//							// dodanie mapowan rowniez w gore drzewa lokalnego
//							if(!empty($aParents)){
//								foreach($aParents as $iParent){
//									if(!$this->existAzymutMapping($iParent,$iChild)){
//										$bIsErr =($this->AddAzymutMapping($iParent,$iChild)== false);
//									}
//								}
//							}
//						}
//					}
//				}
			}
		}
	//usuniecie poprzednich mapowań kategorii abe
		$sSql="DELETE FROM ".$aConfig['tabls']['prefix']."products_abe_mapping
					WHERE page_id = ".$this->iId;
		Common::Query($sSql);
	// dodanie mapowań kategorii abe
		if(!empty($_POST['abe_mapping'])){
			foreach($_POST['abe_mapping'] as $iCatId){
				$bIsErr =($this->AddABEMapping($this->iId,$iCatId)== false);
				if(!empty($aParents)){
					foreach($aParents as $iParent){
						if(!$this->existABEMapping($iParent,$iCatId)){
							$bIsErr =($this->AddABEMapping($iParent,$iCatId)== false);
						}
					}
				}
			}
		}
	//usuniecie poprzednich mapowań kategorii abe
		$sSql="DELETE FROM ".$aConfig['tabls']['prefix']."products_helion_mapping
					WHERE page_id = ".$this->iId;
		Common::Query($sSql);
	// dodanie mapowań kategorii abe
		
		if(!empty($_POST['helion_mapping'])){
			foreach($_POST['helion_mapping'] as $sCat){
				$bIsErr =($this->AddHELIONMapping($this->iId,$sCat)== false);
				if(!empty($aParents)){
					foreach($aParents as $iParent){
						if(!$this->existHELIONMapping($iParent,$sCat)){
							$bIsErr =($this->AddHELIONMapping($iParent,$sCat)== false);
						}
					}
				}
			}
		}
		return !$bIsErr;
	} // end of UpdateSettings() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji konfiguracji strony
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;
		
		$aData = array();
		$sHtml = '';
		
		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_mtype') {
			$aData =& $_POST;
		}
		
		// przygotowanie danych mapowania azymut
		$aAzymutCats=$this->getAzymutMappings($this->iId);
		$aAvaibleAzymutCats=$this->getAzymutCategories();
			
			if(!empty($aAzymutCats)){
				$aData['azymut_mapping']=array();
			//	dump($aExtraCat);
				foreach($aAvaibleAzymutCats as $aCat){
					if(in_array($aCat['value'],$aAzymutCats)){
						$aData['azymut_mapping'][]=array('value'=>$aCat['value'],'label'=>$aCat['title'],'title'=>$aCat['title']);
					}
				}
			}

		if (!empty($_POST)) {
			$aData =& $_POST;
			if(isset($aData['azymut_mapping'])){
				$aCats=$aData['azymut_mapping'];
				$aData['azymut_mapping']=array();
			//	dump($aExtraCat);
				foreach($aAvaibleAzymutCats as $aCat){
					if(in_array($aCat['value'],$aAzymutCats)){
						$aData['azymut_mapping'][]=array('value'=>$aCat['value'],'label'=>str_replace('&nbsp;','',($aCat['label'])),'title'=>str_replace('&nbsp;','',($aCat['label'])));
					}
				}
			}
		}
		
		// przygotowanie danych mapowania abe
		
	$aABECats=$this->getABEMappings($this->iId);
	$aAvaibleABECats=$this->getABECategories();
			
			if(!empty($aABECats)){
				$aData['abe_mapping']=array();
			//	dump($aExtraCat);
				foreach($aAvaibleABECats as $aCat){
					if(in_array($aCat['value'],$aABECats)){
						$aData['abe_mapping'][]=array('value'=>$aCat['value'],'label'=>$aCat['title'],'title'=>$aCat['title']);
					}
				}
			}

		if (!empty($_POST)) {
			$aData =& $_POST;
			if(isset($aData['abe_mapping'])){
				$aCats=$aData['abe_mapping'];
				$aData['abe_mapping']=array();
			//	dump($aExtraCat);
				foreach($aAvaibleABECats as $aCat){
					if(in_array($aCat['value'],$aABECats)){
						$aData['abe_mapping'][]=array('value'=>$aCat['value'],'label'=>str_replace('&nbsp;','',($aCat['label'])),'title'=>str_replace('&nbsp;','',($aCat['label'])));
					}
				}
			}
		}
		
		
		// przygotowanie danych mapowania helion
		
	$aHELIONCats=$this->getHELIONMappings($this->iId);
	$aAvaibleHELIONCats=$this->getHELIONCategories('helion');
	$aAvaibleHELIONCatsONEPRESS=$this->getHELIONCategories('onepress');
	$aAvaibleHELIONCatsSEPTEM=$this->getHELIONCategories('septem');
	$aAvaibleHELIONCatsSENSUS=$this->getHELIONCategories('sensus');
	
	$aAvaibleHELIONCats = array_merge($aAvaibleHELIONCats, $aAvaibleHELIONCatsONEPRESS);
	$aAvaibleHELIONCats = array_merge($aAvaibleHELIONCats, $aAvaibleHELIONCatsSEPTEM);
	$aAvaibleHELIONCats = array_merge($aAvaibleHELIONCats, $aAvaibleHELIONCatsSENSUS);
			if(!empty($aHELIONCats)){
				$aData['helion_mapping']=array();
			//	dump($aExtraCat);
				foreach($aAvaibleHELIONCats as $aCat){
					if(in_array($aCat['value'],$aHELIONCats)){
						$aData['helion_mapping'][]=array('value'=>$aCat['value'],'label'=>$aCat['title'],'title'=>$aCat['title']);
					}
				}
			}

		if (!empty($_POST)) {
			$aData =& $_POST;
			if(isset($aData['helion_mapping'])){
				$aCats=$aData['helion_mapping'];
				$aData['helion_mapping']=array();
			//	dump($aExtraCat);
				foreach($aAvaibleHELIONCats as $aCat){
					if(in_array($aCat['value'],$aHELIONCats)){
						$aData['helion_mapping'][]=array('value'=>$aCat['value'],'label'=>str_replace('&nbsp;','',($aCat['label'])),'title'=>str_replace('&nbsp;','',($aCat['label'])));
					}
				}
			}
		}
		
		
		// naglowek konfiguracji ustawien
		//$pForm->AddMergedRow($aConfig['lang'][$this->sModule.'_settings']['settings'], array('class'=>'merged', 'style'=>'padding-left: 208px'));
		
		// szablon listy, produktu
		//$pForm->AddMergedRow($aConfig['lang'][$this->sModule.'_settings']['templates'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
		//$pForm->AddSelect('list_template', $aConfig['lang'][$this->sModule.'_settings']['list_template'], array(), $this->sTemplatesList, $aData['list_template'], '', false);
		//$pForm->AddSelect('item_template', $aConfig['lang'][$this->sModule.'_settings']['item_template'], array(), $this->sTemplatesList, $aData['item_template'], '', false);
		
		// liczba produktow na stronie
		//$pForm->AddText('items_per_page', $aConfig['lang'][$this->sModule.'_settings']['items_per_page'], $aData['items_per_page'], array('maxlength'=>3, 'style'=>'width: 30px;'), '', 'uinteger');
		
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule.'_settings']['azymut_mappings'], array('class'=>'mergedLight'));
	
		// wybor kategorii Azymut
		$pForm->AddMergedRow($pForm->GetSelectHtml('azymut_categories',$aConfig['lang'][$this->sModule]['avaible_categories'], array('size'=>20,'style'=>'width:100%;'), $aAvaibleAzymutCats,'','',false),array());
		$pForm->AddMergedRow($pForm->GetInputButtonHTML('add_cat', $aConfig['lang'][$this->sModule]['add_cat'], array('onclick'=>'addAzymutCategoryMapping();'), 'button').'&nbsp;'.
													$pForm->GetInputButtonHTML('del_cat', $aConfig['lang'][$this->sModule]['del_cat'], array('onclick'=>'delAzymutCategoryMapping();'), 'button')
													,array());
		$pForm->AddMergedRow($pForm->GetSelectHtml('azymut_mapping',$aConfig['lang'][$this->sModule]['azymut_mapping'], array('size'=>20,'style'=>'width:100%;','multiple'=>1), $aData['azymut_mapping'],'','',false),array());
		
									
		// wybor kategorii ABE
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule.'_settings']['abe_mappings'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
	
		$pForm->AddMergedRow(
									$pForm->GetSelectHtml('abe_categories',$aConfig['lang'][$this->sModule]['avaible_categories'], array('size'=>20,'style'=>'width:450px;'), $aAvaibleABECats,'','',false).'&nbsp;'.
									$pForm->GetInputButtonHTML('add_cat', $aConfig['lang'][$this->sModule]['add_cat'], array('onclick'=>'addABECategoryMapping();'), 'button').'&nbsp;'.
									$pForm->GetInputButtonHTML('del_cat', $aConfig['lang'][$this->sModule]['del_cat'], array('onclick'=>'delABECategoryMapping();'), 'button').'&nbsp;'.
									$pForm->GetSelectHtml('abe_mapping',$aConfig['lang'][$this->sModule]['abe_mapping'], array('size'=>20,'style'=>'width:450px;','multiple'=>1), $aData['abe_mapping'],'','',false),array());
		

		// wybor kategorii HELION
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule.'_settings']['helion_mappings'], array('class'=>'mergedLight', 'style'=>'padding-left: 208px'));
	
		$pForm->AddMergedRow(
									$pForm->GetSelectHtml('helion_categories',$aConfig['lang'][$this->sModule]['avaible_categories'], array('size'=>20,'style'=>'width:450px;'), $aAvaibleHELIONCats,'','',false).'&nbsp;'.
									$pForm->GetInputButtonHTML('add_cat', $aConfig['lang'][$this->sModule]['add_cat'], array('onclick'=>'addHELIONCategoryMapping();'), 'button').'&nbsp;'.
									$pForm->GetInputButtonHTML('del_cat', $aConfig['lang'][$this->sModule]['del_cat'], array('onclick'=>'delHELIONCategoryMapping();'), 'button').'&nbsp;'.
									$pForm->GetSelectHtml('helion_mapping',$aConfig['lang'][$this->sModule]['helion_mapping'], array('size'=>20,'style'=>'width:450px;','multiple'=>1), $aData['helion_mapping'],'','',false),array());

		
		
		//$pForm->AddSelect('ab_id', $aConfig['lang'][$this->sModule.'_settings']['category'], array('size'=>15,'style'=>'width:400px;','onchange' => 'preventSelectDisabled(this);'), $this->getCategories(), $aData['ab_id'], '', false);	
		$sJS.='
			function addAzymutCategoryMapping(){
				var elSelSrc = document.getElementById(\'azymut_categories\'); 
	 			var elSelDest = document.getElementById(\'azymut_mapping\');
	 			
		 			var val = elSelSrc.options[elSelSrc.selectedIndex].value;
		 			var txt =  elSelSrc.options[elSelSrc.selectedIndex].title;
		 			
		 			if (elSelDest.options != undefined) {
	    			for (var i = 0; i < elSelDest.options.length; i++) {
	      			if (elSelDest.options[i].value == val) {
	        			return false;
	      			}
	    			}
	  			}
	        elSelDest.options.add(new Option(txt,val));
			
			}
			
			function addABECategoryMapping(){
				var elSelSrc = document.getElementById(\'abe_categories\'); 
	 			var elSelDest = document.getElementById(\'abe_mapping\');
	 			
		 			var val = elSelSrc.options[elSelSrc.selectedIndex].value;
		 			var txt =  elSelSrc.options[elSelSrc.selectedIndex].title;
		 			
		 			if (elSelDest.options != undefined) {
	    			for (var i = 0; i < elSelDest.options.length; i++) {
	      			if (elSelDest.options[i].value == val) {
	        			return false;
	      			}
	    			}
	  			}
	        elSelDest.options.add(new Option(txt,val));
			
			}
			

			function addHELIONCategoryMapping(){
				var elSelSrc = document.getElementById(\'helion_categories\'); 
	 			var elSelDest = document.getElementById(\'helion_mapping\');
	 			
		 			var val = elSelSrc.options[elSelSrc.selectedIndex].value;
		 			var txt =  elSelSrc.options[elSelSrc.selectedIndex].title;
		 			
		 			if (elSelDest.options != undefined) {
	    			for (var i = 0; i < elSelDest.options.length; i++) {
	      			if (elSelDest.options[i].value == val) {
	        			return false;
	      			}
	    			}
	  			}
	        elSelDest.options.add(new Option(txt,val));
			}


			function delAzymutCategoryMapping(){
				var elSelDest = document.getElementById(\'azymut_mapping\');
				elSelDest.remove(elSelDest.selectedIndex);
			}	
			
			function delABECategoryMapping(){
				var elSelDest = document.getElementById(\'abe_mapping\');
				elSelDest.remove(elSelDest.selectedIndex);
			}	
			
			function delHELIONCategoryMapping(){
				var elSelDest = document.getElementById(\'helion_mapping\');
				elSelDest.remove(elSelDest.selectedIndex);
			}	
		';
		$pForm->setAdditionalJS($sJS);
	} // end of SettingsForm() function

	function hasAzymutChildren($iAzId) {
		global $aConfig;
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_azymut_categories
						 WHERE parent_id = ".$iAzId;
		return ((double) Common::GetOne($sSql)) > 0;
	} // end of hasChildren() method
	
	
	function getAzymutCategories($mPId='NULL', $sPrefix='',$sPath='') {
		global $aConfig;
		$aCategories = array();
		if ($mPId == 'NULL') {
			// pierwsza opcja --wybierz
		//	$aCategories = array(array('value' => '', 'label' => $aConfig['lang']['common']['choose']));
		}
		
		// pobranie kategorii
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products_azymut_categories
						 WHERE parent_id ".($mPId == 'NULL' ? "IS NULL" : "= ".$mPId)."
						 ORDER BY id";
		$aCats =& Common::GetAll($sSql);

		foreach ($aCats as $aCat) {
			$aCategory = array(
				'label' => $sPrefix.$aCat['name'],
				'value' => $aCat['id'],
				'title' => $sPath.$aCat['name']
			); 
			if ($this->hasAzymutChildren((double) $aCat['id'])) {
				// posiada podaktegorie - niemozliwa do zaznaczenia
				$aCategory['class'] = 'disabled';
				$aCategories[] = $aCategory;
				$aSubCategories = $this->getAzymutCategories((double) $aCat['id'], $sPrefix.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$sPath.$aCat['name'].' / ');
				if (!empty($aSubCategories)) {
					$aCategories = array_merge($aCategories, $aSubCategories);
				}
			}
			else {
				$aCategories[] = $aCategory;
			}
		}
		return $aCategories;
	}
	
//	function getAzymutChilds($iCatId) {
//		global $aConfig;
//		$aCategories = array();
//		
//		// pobranie kategorii
//		$sSql = "SELECT id
//				 FROM ".$aConfig['tabls']['prefix']."products_azymut_categories
//				 WHERE parent_id = ".$iCatId."
//				 ORDER BY id";
//		$aCats =& Common::GetCol($sSql);
//
//		if(!empty($aCats)){
//			foreach ($aCats as $iCat) {
//				if ($this->hasAzymutChildren($iCat)) {
//					$aCategories[] = $iCat;
//					$aSubCategories = $this->getAzymutChilds($iCat);
//					if (!empty($aSubCategories)) {
//						$aCategories = array_merge($aCategories, $aSubCategories);
//					}
//				}
//				else {
//					$aCategories[] = $iCat;
//				}
//			}
//		}
//		return $aCategories;
//	}
	
	function &getAzymutMappings($iId){
		global $aConfig;
		$sSql="SELECT azymut_category
					 FROM ".$aConfig['tabls']['prefix']."products_azymut_mapping
					 WHERE page_id = ".$iId;
		return Common::GetCol($sSql);
	}
	
	function getAzymutPath(&$aItems,$iId){
		foreach($aItems as $aItem){
			if($sId==$aItem['value'])
				return $aItem['title'];
		}
		return '';
	}
	
	
	function hasHELIONChildren($iAId) {
		global $aConfig;
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_helion_categories
						 WHERE parent_id = ".$iAId;
		return ((double) Common::GetOne($sSql)) > 0;
	} // end of hasChildren() method
	
	
	function hasABEChildren($iAId) {
		global $aConfig;
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_abe_categories
						 WHERE parent_id = ".$iAId;
		return ((double) Common::GetOne($sSql)) > 0;
	} // end of hasChildren() method
	
	
	function getABECategories($mPId='NULL', $sPrefix='',$sPath='') {
		global $aConfig;
		$aCategories = array();
		if ($mPId == 'NULL') {
			// pierwsza opcja --wybierz
		//	$aCategories = array(array('value' => '', 'label' => $aConfig['lang']['common']['choose']));
		}
		
		// pobranie kategorii
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products_abe_categories
						 WHERE parent_id ".($mPId == 'NULL' ? "IS NULL" : "= ".$mPId)."
						 ORDER BY id";
		$aCats =& Common::GetAll($sSql);

		foreach ($aCats as $aCat) {
			$aCategory = array(
				'label' => $sPrefix.$aCat['name'],
				'value' => $aCat['id'],
				'title' => $sPath.$aCat['name']
			); 
			if ($this->hasABEChildren((double) $aCat['id'])) {
				// posiada podaktegorie - niemozliwa do zaznaczenia
				//$aCategory['class'] = 'disabled';
				$aCategories[] = $aCategory;
				$aSubCategories = $this->getABECategories((double) $aCat['id'], $sPrefix.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$sPath.$aCat['name'].' / ');
				if (!empty($aSubCategories)) {
					$aCategories = array_merge($aCategories, $aSubCategories);
				}
			}
			else {
				$aCategories[] = $aCategory;
			}
		}
		return $aCategories;
	}
	
		
	function &getABEMappings($iId){
		global $aConfig;
		$sSql="SELECT abe_category
					 FROM ".$aConfig['tabls']['prefix']."products_abe_mapping
					 WHERE page_id = ".$iId;
		return Common::GetCol($sSql);
	}
	
	function getABEPath(&$aItems,$iId){
		foreach($aItems as $aItem){
			if($sId==$aItem['value'])
				return $aItem['title'];
		}
		return '';
	}
	
	
	
	function getHELIONCategories($sSource, $mPId='NULL', $sPrefix='',$sPath='') {
		global $aConfig;
		$aCategories = array();
		if ($mPId == 'NULL') {
			// pierwsza opcja --wybierz
		//	$aCategories = array(array('value' => '', 'label' => $aConfig['lang']['common']['choose']));
		}
		
		// pobranie kategorii
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products_helion_categories
						 WHERE parent_id ".($mPId == 'NULL' ? "IS NULL" : "= ".$mPId)." AND source='".$sSource."'
						 ORDER BY id";
		$aCats =& Common::GetAll($sSql);
		
		foreach ($aCats as $aCat) {
			$aCategory = array(
				'label' => $sPrefix.$aCat['name'].' - '.$aCat['source'],
				'value' => $aCat['id'].'_'.$aCat['source'],
				'title' => $sPath.$aCat['name'].' - '.$aCat['source']
			); 
			if ($this->hasHELIONChildren((double) $aCat['id'])) {
				// posiada podaktegorie - niemozliwa do zaznaczenia
				//$aCategory['class'] = 'disabled';
				$aCategories[] = $aCategory;
				$aSubCategories = $this->getHELIONCategories($sSource, (double) $aCat['id'], $sPrefix.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',$sPath.$aCat['name'].' / ');
				if (!empty($aSubCategories)) {
					$aCategories = array_merge($aCategories, $aSubCategories);
				}
			}
			else {
				$aCategories[] = $aCategory;
			}
		}
		return $aCategories;
	}
	
		
	function &getHELIONMappings($iId){
		global $aConfig;
		$sSql="SELECT CONCAT(helion_id, '_', source) AS helion_id
					 FROM ".$aConfig['tabls']['prefix']."products_helion_mapping
					 WHERE page_id = ".$iId;
		return Common::GetCol($sSql);
	}
	
	function getHELIONPath(&$aItems,$iId){
		foreach($aItems as $aItem){
			if($sId==$aItem['value'])
				return $aItem['title'];
		}
		return '';
	}
	
	
	/**
   * Funkcja tworzaca na podstawie przekazanego do niej Id strony
   * jej pelna sciezke od korzenia do niej
   *
   * @param	integer	$iId	- ID strony do ktorej ma byc tworzona sciezka
   * @param	integer	$iLangId	- Id wersji jezykowej
   * @param	bool	$bSymbols	- true: sciezka symboli; false: sciezka nazw 
   * @return	array	- uporzadkowana tablica ze sciezka do strony
   */
  function getPathIDs($iId, $iLangId) {
	global $aConfig;
	$aPath		= array();
		
  	$sSql = "SELECT A.id, IFNULL(A.parent_id, 0) AS parent_id
  					 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
  					 WHERE A.id = ".$iId." AND
  					 			 A.language_id = ".$iLangId;
  	$aItem =& Common::GetRow($sSql);

  	while (intval($aItem['parent_id']) > 0) {
  		$sSql = "SELECT id, IFNULL(parent_id, 0) AS parent_id
  					 FROM ".$aConfig['tabls']['prefix']."menus_items
  					 WHERE id = ".$aItem['parent_id']." AND
  					 			 language_id = ".$iLangId;
	  	$aItem =& Common::GetRow($sSql);
	  	$aPath[] = $aItem['id'];
  	}
	return array_reverse($aPath);
  } // end getPathItems() function
  
  function AddAzymutMapping($iPageId, $iAzymutId){
  global $aConfig;
	$aValues=array(
		'page_id' => $iPageId,
		'azymut_category' => $iAzymutId
	);
	if ((Common::Insert($aConfig['tabls']['prefix']."products_azymut_mapping",$aValues,'',false)) === false) {
		return false;
	}
	return true;
  }
  
  function AddABEMapping($iPageId, $iABEId){
  global $aConfig;
	$aValues=array(
		'page_id' => $iPageId,
		'abe_category' => $iABEId
	);
	if ((Common::Insert($aConfig['tabls']['prefix']."products_abe_mapping",$aValues,'',false)) === false) {
		return false;
	}
	return true;
  }
	
	
  function AddHELIONMapping($iPageId, $sCat){
		global $aConfig;
		
		$aCatData = explode('_', $sCat);
		if ($aCatData['1'] == 'helion') {
			$sPublisher = 'Helion Wydawnictwo';
		} else {
			$sPublisher = 'Helion - '.ucfirst($aCatData['1']);
		}
		
		$aValues = array(
			'page_id' => $iPageId,
			'helion_id' => $aCatData['0'],
			'source' => $aCatData['1'],
			'publisher' => $sPublisher
		);
		if ((Common::Insert($aConfig['tabls']['prefix']."products_helion_mapping",$aValues,'',false)) === false) {
			return false;
		}
		return true;
  }
	
  
  function existAzymutMapping($iPId,$iCat){
		global $aConfig;
		$sSql="SELECT count(page_id)
					 FROM ".$aConfig['tabls']['prefix']."products_azymut_mapping
					 WHERE page_id = ".$iPId." AND azymut_category = ".$iCat;
		return (Common::GetOne($sSql) > 0);
	}
	
	function existABEMapping($iPId,$iCat){
		global $aConfig;
		$sSql="SELECT count(page_id)
					 FROM ".$aConfig['tabls']['prefix']."products_abe_mapping
					 WHERE page_id = ".$iPId." AND abe_category = ".$iCat;
		return (Common::GetOne($sSql) > 0);
	}
	
	function existHELIONMapping($iPId,$sCat){
		global $aConfig;
		
		$aCatData = explode('_', $sCat);
		
		$sSql="SELECT count(page_id)
					 FROM ".$aConfig['tabls']['prefix']."products_helion_mapping
					 WHERE page_id = ".$iPId." AND helion_id = ".$aCatData['0'];
		return (Common::GetOne($sSql) > 0);
	}
} // end of Settings Class
?>