<?php

use LIB\Helpers\ArrayHelper;
use PriceUpdater\Buffor\CyclicBuffor;

class Module
{
    private $sModule;

    public function __construct(&$pSmarty)
    {
        $this->sModule = $_GET['module'];

        switch (isset($_POST['send'])) {
            case 1:
                $this->updateAction($pSmarty);
                break;

            default:
                $this->editAction($pSmarty);
                break;
        }
    }

    private function getProductsAmount()
    {
        global $pDbMgr;

        $sql = "
        SELECT WCC.category_id, count(P.id) AS product_amount
        FROM website_ceneo_categories AS WCC
        JOIN products AS P
        ON P.main_category_id = WCC.category_id
        AND P.main_category_id IS NOT NULL
        AND P.published = '1'
        AND P.packet = '0'
        AND P.prod_status = '1'
        AND P.shipment_time='0'
        GROUP BY WCC.category_id";

        return $pDbMgr->GetAssoc('profit24', $sql);
    }

    public function editAction($pSmarty)
    {
        global $aConfig;

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('categories_groups', $aConfig['lang'][$this->sModule]['category_settings']);

        $categories = $this->getCategories();
        $websites = Common::GetAll("SELECT id as value, name as label, code FROM ".$aConfig['tabls']['prefix']."websites WHERE ceneo is not null and ceneo != ''");

        $websites[] = [
            'value' => null,
            'label' => $aConfig['lang'][$this->sModule]['not_selected'],
            'class' => 'non-editable'
        ];

        $selected = Common::GetAll("SELECT * FROM ".$aConfig['tabls']['prefix']."website_ceneo_categories");
        $selected = ArrayHelper::toKeyValues('category_id', $selected);
        $productsAmount = $this->getProductsAmount();

        $categoriesMapper = new \Ceneo\CategoriesMapper();
        $ceneoCategories = $categoriesMapper->findAllCeneoCategories();

        foreach ($categories as $category) {

            $id = $category['id'];

            foreach($websites as $iKey => $ws) {

                if (!isset($ws['code'])) {
                    continue;
                }

                $value = Common::formatPrice($selected[$id][$ws['code'].'_percentage_modifier']);
                $websites[$iKey]['additional_row_html'] = _(' - procent dodany ').$pForm->GetTextHTML('categories['.$id.']'.'['.$ws['code'].'_percentage_modifier'.']', "procent dodany ", $value, [], '', 'text', false, '/\d+[,\.]?\d{0,2}/');
            }

            $multiRadio = $pForm->GetRadioSetHTML('categories['.$id.'][value]', 1, $websites, $selected[$id]['website_id'], '', false, false, '', '');

            $parentCategory = '<br><div style="color: green">'.$category['parent'].'</div>';

            $game = '';

            if ($category['game'] == 1) {

                $game = '<br><div style="color: blue;">Kategoria zabawkowa</div>';
            }

            // ilosc produktow w kategori
            $categoryProductAmountHtml = sprintf("<br><br><div class=textRed>%s %s<div/>", $aConfig['lang'][$this->sModule]['product_amount'], $productsAmount[$id]);

            $pForm->AddRow($category['name'].$categoryProductAmountHtml. $parentCategory. $game, $multiRadio, '', array(), array('class'=>'redText'));
//            $pForm->AddSelect('ceneo_category', '', $ceneoCategories);
            $pForm->AddRow(_('Kategoria ceneo'), $this->getSelectbox($ceneoCategories, $id, $selected[$id]['ceneo_category_id']));
        }

        // przyciski
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

        $js = $this->getJs();

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()).$js);
    }

    private function getSelectbox($categories, $catId, $selected)
    {
        $html = '<select name="categories['.$catId.'][ceneo_category_id]">';
        $html .= '<option value="null" >Wybierz</option>';
        foreach($categories as $key => $cat) {

            $selectedElem = $selected == $key ? 'selected' : '';

            $html .= '<option '.$selectedElem.' value='.$key.'>'.$cat.'</option>';
        }

        $html .= '</select>';

        return $html;
    }

    private function getJs()
    {
        $js = "
        <script type='text/javascript'>
        $(document).ready(function(){

            $('input[type=radio]').each(function(){

                var ul = $(this).parent().parent();
                var li = $(this).parent();
                var textbox = li.find('input[type=text]');
                var ultextbox = ul.find('input[type=text]');

                if($(this).is(':checked')) {

                    textbox.attr('readonly', 'readonly')
                }

            });

            $('input[type=radio]').on('click', function(){
                var ul = $(this).parent().parent();
                var li = $(this).parent();
                var textbox = li.find('input[type=text]');
                var ultextbox = ul.find('input[type=text]');

                ultextbox.removeAttr('readonly');

                textbox.val('0,00');
                textbox.attr('readonly', 'readonly');


                if ($(this).hasClass('non-editable')) {
                    ultextbox.val('0,00');
                    ultextbox.attr('readonly', 'readonly');
                }

            })
        })
        </script>
        ";

        return $js;
    }

    private function getWebsiteCeneoCategories()
    {
        global $aConfig;

        return ArrayHelper::toKeyValues('category_id', Common::GetAll("SELECT * FROM ".$aConfig['tabls']['prefix']."website_ceneo_categories"));
    }

    function getCategories(){
        global $aConfig;

        $sSql1="SELECT id
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".getModuleId('m_oferta_produktowa')."
					 AND mtype = '0'
					 AND parent_id IS NULL
					 AND published = '1'";

        $sSql2 = "SELECT MI.id, MI.name, MI2.name AS parent, MENUS.game
                     FROM ".$aConfig['tabls']['prefix']."menus_items AS MI
                     LEFT JOIN menus as MENUS ON MI.menu_id = MENUS.id
                     JOIN menus_items as MI2 ON MI.parent_id = MI2.id
					 WHERE MI.module_id = ".getModuleId('m_oferta_produktowa')."
					 AND MI.mtype = '0'
					 AND MI.parent_id IN (".implode(',', Common::GetCol($sSql1)).")
					 ORDER BY MENUS.game, MI.priority";

        return Common::GetAll($sSql2);
    }

    public function updateAction($pSmarty)
    {
        global $pDbMgr, $aConfig;

        if (true === empty($_POST['categories'])) {
            return $this->editAction($pSmarty);
        }

        $websiteCeneoCategories = $this->getWebsiteCeneoCategories();
        $table = $aConfig['tabls']['prefix']."website_ceneo_categories";

        foreach($_POST['categories'] as $category => $website) {

            $value = $website['value'] == '' ? 'NULL' : $website['value'];

            $websiteCategoryData = $website;
            unset($websiteCategoryData['value']);
            $websiteCategoryData['website_id'] = $value;
            $websiteCategoryData['category_id'] = $category;

            $websiteCategoryData = $this->overrideDecimalValues($websiteCategoryData);

            if (true === isset($websiteCeneoCategories[$category])) {
                $pDbMgr->Update('profit24', $table, $websiteCategoryData, 'category_id = '.$category);

            } else {
                $pDbMgr->Insert('profit24', $table, $websiteCategoryData);
            }
        }

        //TODO odbudowac bufor
        //echo exec('php '.$aConfig['common']['base_path']."../LIB/PriceUpdater/UpdateBufforForAllWebsites.php");

        $this->editAction($pSmarty);
    }

    private function overrideDecimalValues($websiteCategoryData)
    {
        foreach($websiteCategoryData as $key => $wcd) {

            if (!strpos($key, '_percentage_modifier')) {

                continue;
            }

            $websiteCategoryData[$key] = Common::formatPrice2($wcd);
        }

        return $websiteCategoryData;
    }
}