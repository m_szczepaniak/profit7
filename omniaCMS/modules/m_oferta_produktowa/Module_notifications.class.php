<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - powiadomienia o wznowieniu nakladu oraz
 * ukazaniu sie ksiazki w sprzedazy ( do tej pory byla zapowiedzia )
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'powiadomienia');

		$sDo = '';
		$iId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'get_users_list': $this->getUsersList(); break;
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla formularz pozwalajacy wybrac dla jakiego typu powiadomien
	 * oraz dla jakiej ksiazki pobierane beda dane
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
				
		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = $aConfig['lang'][$this->sModule]['header'];
				
		$pForm = new FormTable('get_notifications', $aLang['header'], array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'change_type');
		
		
		// lista dosteonych dla powiadomien ksiazek
		$aProducts =& $this->getNotificationProducts();
		$pForm->AddSelect('product_id', $aLang['product'], array(), $aProducts);
		
		// przycisk pobrania danych
		if (count($aProducts) > 1) {
			$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aLang['get_users_list'], array('onclick'=>'if (document.getElementById(\'product_id\').options.selectedIndex==0){alert(\''.$aLang['no_product_chosen'].'\')}else{document.getElementById(\'do\').value=\'get_users_list\'; this.form.submit();}'), 'button'));
		}

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Show() function
	
	
	/**
	 * Metoda pobiera liste produktow dla wybranego typu powiadomien
	 * ( dla ktorych klienci podali swoj adres email lub numer telefonu )
	 * 
	 * @return	array ref	- lista produktow
	 */
	function &getNotificationProducts() {
		global $aConfig;
		
		$aItems = array(
			array('value' => '', 'label' => $aConfig['lang']['common']['choose'])
		);
		
			$sSql = "SELECT DISTINCT A.product_id value, A.title label
						 	 FROM ".$aConfig['tabls']['prefix']."products_notifications_subscribers A
						 	  ORDER BY A.product_id";
			$aProducts =& Common::GetAll($sSql);
			if (!empty($aProducts)) {
				$aItems = array_merge($aItems, $aProducts);
			}


		return $aItems;
	} // end of getNotificationProducts() method
	
	
	/**
	 * Metoda zwraca liste z numerami telefonow i adresami email
	 * dla powiadomien w postaci pliku CSV (\t) o rozszerzeniu .xls
	 * 
	 * @return void
	 */
	function getUsersList() {
		global $aConfig;
		$aLang =& $aConfig['lang'][$this->sModule];
		
		$aItems =& $this->getUsers();
		$sBook = $this->getItemName((double) $_POST['product_id']);
		
		$_GET['hideHeader'] = '1';
		
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="do_powiadomien_'.$_POST['product_id'].'.xls"');
		header("Pragma: no-cache");
		header("Expires: 0");
		// zapis typu listy powiadomien
		echo "\t\t".$aLang['notification']."\r\n";
		// zapis tytulu ksiazki
		echo "\t\t".$sBook."\r\n\r\n";
		// zapis naglowka rekordow
		// zapis rekordow
		echo $aLang['list_lp']."\t".$aLang['list_email']."\t".$aLang['list_content']."\r\n";
		foreach ($aItems as $iKey => $aItem) {
			echo ($iKey + 1)."\t".$aItem['email']."\t".$aItem['content']."\r\n";
		}
	} // end of getUsersList() method
	
	
	function getUsers() {
		global $aConfig;
		$sSql = "SELECT email,content
					 	 FROM ".$aConfig['tabls']['prefix']."products_notifications_subscribers
					 	 WHERE product_id = ".(double) $_POST['product_id']."
					 	 ORDER BY added DESC";
		return Common::GetAll($sSql);
	} // end of getUsers() method


} // end of Module Class
?>