<?php

class Module
{
    private $pSmarty;

    public function __construct(&$pSmarty)
    {
        $this->pSmarty = $pSmarty;
        $this->sModule = $_GET['module'];

        switch (isset($_GET['do'])) {

            default:
                $this->showListAction();
                break;
        }
    }

    public function showListAction()
    {
        global $aConfig, $pDbMgr;

        rememberViewState($this->sModule);

        $aHeader = array(
            'header' => 'Lista produktów',
            //            'header' => $aConfig['lang'][$this->sModule]['list'],
            'refresh' => true,
            'bigSearch' => true,
            'search' => true
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 0,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'content'	=> '&nbsp;',
                'width'	=> '20'
            ),
            array(
                'content'	=> _('Id'),
                'width' => '30'
            ),
            array(
                'content'	=> _('isbn'),
                'width' => '150'
            ),
            array(
                'content'	=> _('Nazwa')
            ),
            array(
                'content'	=> _('Cena zakupu')
            ),
            array(
                'content'	=> _('Cena katalogowa')
            )
        );

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $iRowCount = $pDbMgr->GetOne('profit24', "
        SELECT count(*)
        FROM products AS P
        WHERE P.wholesale_price > P.price_brutto
        ");

        $pView = new View('group_discount', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);
        $iCurrentPage = $pView->AddPager($iRowCount);

        $aTypes = array(
            array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_available_now']),
            array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_available_1_2_days']),
            array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['f_available_order'])
        );
        $pView->AddFilter('f_shipment_time', $aConfig['lang'][$this->sModule]['f_shipment_time'], $aTypes, $_POST['f_shipment_time']);

        $aConfig['lang'][$this->sModule]['no_items'] = _('Brak elementów');
//        return $pView->Show();

        $perPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];

        $iStartFrom = ($iCurrentPage - 1) * $perPage;
        if (null == $iCurrentPage){
            $iStartFrom = 0;
        }

        $aUsers = $pDbMgr->GetAll('profit24', "
        SELECT
        P.id, P.isbn_plain, P.name, P.wholesale_price, P.price_brutto, P.shipment_time
        FROM products AS P
        WHERE P.wholesale_price > P.price_brutto " .
        (isset($_POST['f_shipment_time']) && !empty($_POST['f_shipment_time']) ? ' AND shipment_time = '.$_POST['f_shipment_time'] : '') .
        " limit $iStartFrom, $perPage
        ");

        foreach($aUsers as &$element) {

            $element['wholesale_price'] = Common::FormatPrice($element['wholesale_price']);
            $element['price_brutto'] = Common::FormatPrice($element['price_brutto']);
        }


        // ustawienia dla poszczegolnych kolumn rekordu
        $aColSettings = array(
            'shipment_time' => array(
                'show' => false
            )
        );
        // dodanie rekordow do widoku
        $pView->AddRecords($aUsers, $aColSettings);
        // dodanie stopki do widoku

        $aRecordsFooter = array(

        );

        $pView->AddRecordsFooter($aRecordsFooter);
        // przepisanie langa - info o braku rekordow


        $this->pSmarty->assign('sContent', $pView->Show());
    }
}