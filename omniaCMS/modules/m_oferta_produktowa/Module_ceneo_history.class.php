<?php

class Module
{
    private $pSmarty;

    public function __construct(&$pSmarty)
    {
        $this->pSmarty = $pSmarty;
        $this->sModule = $_GET['module'];

        if (isset($_GET['id'])) {

            $_POST['id'] = $_GET['id'];
            $_POST['save'] = true;
            $_POST['search'] = $_GET['id'];
        }

        switch (isset($_POST['save'])) {
            case 1:
                $this->showAction();
                break;

            default:
                $this->searchProductAction();
                break;
        }
    }

    private function searchProductAction()
    {
        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('categories_groups', 'Historia odpytań ceneo');
        $pForm->AddText('search', 'Id produktu (profit), ');
        $pForm->AddInputButton('save', 'wyślij');

        $this->pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
    }

    private function prepareCeneoId($data)
    {
        foreach($data as $row) {
            if ($row['ceneo_id'] != null) {

                return $row['ceneo_id'];
            }
        }

        return null;
    }

    private function showAction()
    {
        global $pDbMgr;

        $productId = $_POST['search'];
        $productStory = $pDbMgr->GetAll('ceneo_reports', "SELECT * from ceneo_history where profit24_id = $productId ORDER BY date DESC");


        if (count($productStory) == 0) {

            return $this->pSmarty->assign('sContent', "<h2>Brak danych dla tego produktu<h2/>");
        }

        $ceneoId = $pDbMgr->GetOne("ceneo_reports", "SELECT ceneo_id FROM ceneo_product_ids WHERE profit24_id = $productId LIMIT 1");

        $unpackedData = function($data){

            $data['response_data'] = json_decode($data['response_data'], true);

            return $data;
        };

        $sortedData = function($data){

            /** sortujemy po cenie */
            usort($data['response_data']['offer'], function($a, $b) {
                return (float)$a['Price'] < (float)$b['Price'] ? -1 : 1;
            });

            return $data;
        };

        $coloredData = function($data){

            $ourShops = [
                'nieprzeczytane.pl',
                'mestro.pl',
                'profit24.pl',
                'smarkacz.pl',
            ];

            if (null == $data['response_data']['offer'] || empty($data['response_data']['offer'])) {
                return $data;
            }

            $res = array_map(function($data) use ($ourShops){

                $data['our_shop'] = in_array($data['CustName'], $ourShops) ? true : false;

                return $data;

            }, $data['response_data']['offer']);

            $data['response_data'] = $res;

            return $data;
        };

        $preparedElements = array_map(compose($coloredData, compose($sortedData, $unpackedData)), $productStory);

        $table = '';

        if ($ceneoId != null) {
            $table .= '<a target="_blank" href="'.'http://www.ceneo.pl/'.$ceneoId.'">Link do produktu na ceneo</a>';
        }

            foreach($preparedElements as $items) {

                $minimalPriceType = 'Brak danych';
                $minimalPrice = null == $items['minimum_price'] ? "Brak danych" : $items['minimum_price'];

                if ($items['minimum_price_type'] != null) {

                    if ($items['minimum_price_type'] == 'STANDARD') {
                        $minimalPriceType = 'Standardowa cena';
                    } else {
                        $minimalPriceType = 'Cena ustalana przez rabat wydawnictwa';
                    }
                }

                $table .= '<div style="margin:40px 0">';
                $table .= '<div>Data: '.$items['date'].'</div>';
                $table .= '<div>Cena minimalna: '.$minimalPrice.'</div>';
                $table .= '<div>Typ ceny minimalnej: '.$minimalPriceType.'</div>';
                $table .= '<div>Id produktu: '.$items['profit24_id'].'</div>';
                $table .= '</div>';


                $items = $items['response_data'];

                $table .= '<table class="recordsTable" style="width:100%">';

                $table .= '<tr>';

                $table .= '<th>Nazwa</th>';
                $table .= '<th>Cena na ceneo</th>';
                $table .= '<th>Produkt 24h</th>';
                $table .= '<th>Pozycja</th>';
                $table .= '<th>Ilość opinii</th>';
                $table .= '<th>Polecany</th>';

                $table .= '</tr>';

                    foreach($items as $offer) {

                        $style = $offer['our_shop'] === true ? ' style="background: green" ' : '';

                        $table .= '<tr class="row" '.$style.'>';


                        foreach($offer as $keyName => $offerData) {

                            if ($keyName == 'our_shop') {
                                continue;
                            }

                            $table .= '<td style="width:200px">'.$offerData.'</td>';
                        }
                        $table .= '</tr>';
                    }

                $table .= '</table>';

                $table .= '</div>';
            }

        $table .= '';

        $this->pSmarty->assign('sContent', $table);
    }
}
function compose($f, $g){
    return function($x) use($f, $g){
        return $f($g($x));
    };
}