<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - zapowiedzi
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'zapowiedzi');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do']) && !in_array($_GET['do'], array('placeOnMain', 'unplaceOnMain'))) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

		// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': 				$this->Delete($pSmarty, $iId); break;
			case 'placeOnMain': 	$this->PlaceOnMain($pSmarty, $iPId, true); break;
			case 'unplaceOnMain': $this->PlaceOnMain($pSmarty, $iPId, false); break;
			case 'insert': 				$this->AddItem($pSmarty); break;
			case 'add': 					$this->Add($pSmarty, $iId); break;
			case 'settings': 			$this->Settings($pSmarty); break;
			case 'saveSet': 			$this->saveSettings($pSmarty); break;
			
			case 'sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show('',
																			 $this->getRecords(),
																			 phpSelf(array('do'=>'proceed_sort'),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."products_previews",
												$this->getRecords(),
												array(),
											 	'order_by');
			break;
			
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda zapisuje ustawienia limitów
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function saveSettings(&$pSmarty) {
		global $aConfig;
		
		$bIsErr=0;
		Common::BeginTransaction();
		foreach($_POST['limit'] as $iKey =>$fValue){
			$aValues['bestsLimit']=Common::FormatPrice2($fValue);
			if((Common::Update($aConfig['tabls']['prefix']."menus_items",$aValues, "id = ".$iKey)) === false)	$bIsErr=1;
			}
		if($bIsErr) {
			Common::RollbackTransaction();
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['limit_valueErr']);
			}
		else {
			Common::CommitTransaction();
			$this->sMsg .= GetMessage('Zmiany zostały zapisane',false);
		}		
		
		$this->show($pSmarty);
	}// end of saveSettings() method
	
	
	/**
	 * Metoda wyswietla tabelkę z ustawieniami limitów
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Settings(&$pSmarty) {
		global $aConfig;
		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych edytowanego rekordu
		$aLimits=getModulePages('m_powiazane_oferta_produktowa',$this->iLangId,true,'','zapowiedzi');

		$sHeader = $aConfig['lang'][$this->sModule]['headerSet'];

		$pForm = new FormTable('general_discounts', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'saveSet');
		
		for($i=0;$i<3;$i++) {
			$pForm->AddMergedRow($aLimits[$i+1]['label'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
			$pForm->AddText('limit['.$aLimits[$i+1]['value'].']', $aConfig['lang'][$this->sModule]['limit_value'], 
												Common::FormatPrice(Common::GetOne('SELECT bestsLimit FROM '.$aConfig['tabls']['prefix'].'menus_items WHERE id='.$aLimits[$i+1]['value'])), 
												array('maxlength'=>7, 'style'=>'width: 50px;'), '', 'ufloat');
		}
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	}// end of Settings() method
	
	
	/**
	 * Metoda wyswietla liste zdefiniowanych zapowiedzi
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		// dolaczenie klasy View
		include_once('View/View.class.php');
		include_once('Form/Form.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'B.plain_name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'placeOnMain',
				'content'	=> $aConfig['lang'][$this->sModule]['placeOnMainLabel'],
				'sortable'	=> true,
				'width'	=> '45'
			),
			array(
				'db_field'	=> 'category',
				'content'	=> $aConfig['lang'][$this->sModule]['list_category'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		if(isset($_POST['page_id']))	$_POST['f_page']=$_POST['page_id'];
		// pobranie liczby wszystkich serii
		$sSql = "SELECT COUNT(A.id)
						 FROM ".$aConfig['tabls']['prefix']."products_previews A
						 JOIN ".$aConfig['tabls']['prefix']."products B
						 ON B.id = A.product_id
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? (is_numeric($_POST['search']) ? ' AND B.id = '.(int) $_POST['search'] : ' AND B.name LIKE \'%'.$_POST['search'].'%\'') : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(A.id)
							 FROM ".$aConfig['tabls']['prefix']."products_previews A
							 JOIN ".$aConfig['tabls']['prefix']."products B
							 ON B.id = A.product_id";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_previews', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		$pView->AddFilter('f_btn', $aConfig['lang'][$this->sModule]['f_page'], Form::GetInputButton('settings', $aConfig['lang'][$this->sModule]['settings'], array('onclick'=>'window.location.href=\''.phpSelf(array('do'=>'settings')).'\';return false;', 'style'=>'float:left; margin-left:10px;')), '','html');
		// dodanie filtru grupy
		
		$pView->AddFilter('f_page', $aConfig['lang'][$this->sModule]['f_page'], array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),getModulePages('m_powiazane_oferta_produktowa',$this->iLangId,true,'','zapowiedzi')), $_POST['f_page']);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich serii
			$sSql = "SELECT B.id, B.name,A.placeOnMain, A.page_id category, C.name AS publisher, A.product_id, B.publication_year
							 FROM ".$aConfig['tabls']['prefix']."products_previews A
							 JOIN ".$aConfig['tabls']['prefix']."products B
							 ON B.id = A.product_id
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers C
							 	 ON B.publisher_id = C.id
							 WHERE 1 = 1".
											(isset($_POST['f_page']) && $_POST['f_page'] != '' ? ' AND A.page_id = '.$_POST['f_page'] : '').
										 (isset($_POST['search']) && !empty($_POST['search']) ? (is_numeric($_POST['search']) ? ' AND B.id = '.(int) $_POST['search'] : ' AND B.name LIKE \'%'.$_POST['search'].'%\'') : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			foreach ($aRecords as $iKey => $aItem) {
				$aRecords[$iKey]['category'] = $this->getPageName($aItem['category']);
				$aRecords[$iKey]['name'] = '<b>'.$aItem['name'].'</b>'.(!empty($aItem['publisher'])?'<br /><span style="font-weight: normal; font-style: italic;">'.substr($aItem['publisher'],0,40).'</span>':'');
				unset($aRecords[$iKey]['publisher']);
				$aRecords[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aItem['product_id']),0,40,'UTF-8').' '.$aItem['publication_year'].'</span>';
				unset($aRecords[$iKey]['publication_year']);
				// link podgladu
				$aRecords[$iKey]['action']['links']['preview'] = (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'].'/' : '').(mb_strlen($aItem['name'], 'UTF-8')>210?mb_substr(link_encode($aItem['name']),0,210, 'UTF-8'):link_encode($aItem['name'])).',product'.$aItem['product_id'].'.html?preview='.md5($_SERVER['REMOTE_ADDR'].$aConfig['common']['client_base_url_http']);
				$aRecords[$iKey]['placeOnMain']='<span style="color:#'.($aItem['placeOnMain']?'0c0;">TAK':'c00;">NIE').'</span>';
				unset($aRecords[$iKey]['product_id']);		
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('delete','preview'),
					'params' => array (
												'delete'	=> array('id'=>'{id}')
											),
					'show' => false
											)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'place_on_main', 'unplace_on_main'),
			array('sort', 'add')
		);
		if (!isset($_POST['f_page']) || $_POST['f_page'] == '') {
			unset($aRecordsFooter[1][0]);
			unset($aRecordsFooterParams['sort']);
		}
		
		$aRecordsFooterParams = array(
			'add' => array(array('" onclick="openSearchWindow(); return false;"')),
			'sort' => array(array('id'=>$_POST['f_page']))

		);
		$pView->AddRecordsFooter($aRecordsFooter,$aRecordsFooterParams);
		$sJs='<script>function openSearchWindow() {
				window.open("ajax/SearchProduct.php?ppid='.$iPId.'&pid='.$_POST['f_page'].'&site=profit24&mode=7","","menubar=0,location=0,scrollbars=1,resizable=1,toolbar=0,width=700,height=500");
				};</script>';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$sJs.$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane produkty z zapowiedzi
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego produktu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = getPagePath($aItem['page_id'], $this->iLangId, true, false, false).' / '.$aItem['name'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.getPagePath($aItem['page_id'], $this->iLangId, true, false, false).' / '.$aItem['name'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje produkt do zapowiedzi
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function AddItem(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		/*if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}*/
		$_POST['product_id']=explode(',', $_GET['aid']);

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Show($pSmarty);
			return;
		}
		
		$_POST['page_id']=$_GET['dep']?$_GET['dep']:'2052';
		$_POST['f_page']=	$_GET['dep']?$_GET['dep']:'';
		$sNames='';
		foreach($_POST['product_id'] as $iProduct){
			if (!$this->isPreview((int) $iProduct)) {
				Common::BeginTransaction();	
				
				if (!$bIsErr) {
					// dodanie informacji do tabeli products_news
					$bIsErr = !$this->addPreview((int) $iProduct);
				}
				
			}	
			$sNames.=$this->getItemName((int) $iProduct).',';
		}
		$sNames=substr($sNames,0,-1);
		
		if (!$bIsErr) {
			// dodano
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											getPagePath((int) $_POST['parent_id'], $this->iLangId, true, false, false),
											$sNames);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			// blad
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											getPagePath((int) $_POST['parent_id'], $this->iLangId, true, false, false),
											$sNames);
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			$this->Show($pSmarty);
		}

	} // end of AddItem() funciton


	/**
	 * Metoda tworzy formularz wyboru produktu, ktory ma zostac dodany do listy
	 * zapowiedzi
	 *
	 * @param	object	$pSmarty
	 */
	function Add(&$pSmarty) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = $aConfig['lang'][$this->sModule]['header'];
				
		$pForm = new FormTable('search_news', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'add');
		
		$pForm->AddText('product_name',$aConfig['lang'][$this->sModule]['product_name'],$aData['product_name'],array('style'=>'width:300px;'),'','text',false);
		
		$pForm->AddText('product_author',$aConfig['lang'][$this->sModule]['product_author'], $aData['product_author'],array('style'=>'width:300px;','maxlength'=>255),'','text' ,false);
		$pForm->AddText('product_publisher',$aConfig['lang'][$this->sModule]['product_publisher'], $aData['product_publisher'],array('id'=>'publisher_aci','style'=>'width:300px;','maxlength'=>255),'','text' ,false);
		
		
		// przycisk wyszukaj
		$pForm->AddRow('&nbsp;', '<div style="margin-left: 225px;">'.
									$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_search'])
									.'</div>');
			
		$pForm2 = new FormTable('add_news', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm2->AddHidden('do', 'insert');
		// produkt
		$pForm2->AddSelect('product_id', $aConfig['lang'][$this->sModule]['product'], array('size'=>15,'multiple'=>1,'style'=>'width:100%;'), $this->getProducts('b',$_POST['product_name'],$_POST['product_author'],$_POST['product_publisher'],false));
		$pForm2->AddSelect('page_id',$aConfig['lang'][$this->sModule]['page_id'],array(),array_merge(array(array('value'=>0,'label'=>$aConfig['lang']['common']['choose'])),getModulePages('m_powiazane_oferta_produktowa',$this->iLangId,true,'','zapowiedzi')),$aData['page_id']);

		// przyciski
		$pForm2->AddRow('&nbsp;', $pForm2->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm2->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		$sJS='
					<script>
						$(document).ready(function(){
							$("#publisher_aci").autocomplete({
                source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetPublishers.php"
              });
						});
					</script>';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm().$pForm2->ShowForm()));
	} // end of Add() function
	
	
	/**
	 * Metoda pobiera liste produktow do usuniecia z zapowiedzi na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id zapowiedzi
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, page_id, name
					 		 FROM ".$aConfig['tabls']['prefix']."products
						 	 WHERE id IN  (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa produkt z zapowiedzi
	 * 
	 * @param	integer	$iId	- Id produktu do usuniecia z zapowiedzi
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		
		
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_previews='0'
							WHERE id = ".$iId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		
		if($this->existShadow($iId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_previews='0'
							WHERE id = ".$iId;
			if(Common::Query($sSql) === false) {
				$bIsError = true;
			}
		}
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_previews
						 WHERE product_id = ".$iId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		return !$bIsError;

	} // end of deleteItem() method
	
	
	/**
	 * Metoda pobiera liste rekordow
	 *
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords() {
		global $aConfig;

		// pobranie wszystkich zapowiedzi
		$sSql = "SELECT A.id, B.name
						 FROM ".$aConfig['tabls']['prefix']."products_previews A
						 JOIN ".$aConfig['tabls']['prefix']."products B
						 ON B.id = A.product_id
						 WHERE A.placeOnMain='1'
						 ".($_GET['id']>0?' AND A.`page_id`='.intval($_GET['id']):'')."
						 ORDER BY A.order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
	
	/**
	 * Metoda ustawia status placeOnMain oznaczajacy obecnosc danego rpoduktu w boxie na stronie głównej
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id promocji
	 * @param bool $bStat - status pola
	 * @return 	void
	 */
	function placeOnMain(&$pSmarty, $iId,$bStat) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 1;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;
	 	$aValues=array('placeOnMain'=> ($bStat?'1':'0'));
		
		//dump($_POST['delete']);
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			
			foreach ($_POST['delete'] as $iItem) {
				// aktualizowanie
				$aValues=array('placeOnMain'=> ($bStat?'1':'0'));
				if ((Common::Update($aConfig['tabls']['prefix']."products_previews",
						 $aValues,'product_id = '.$iItem,
						 false)) === false) {
							$bIsErr = true;
							break;
				}
			}
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg =$aConfig['lang']['m_oferta_produktowa_promotions_books']['plc_ok'];
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang']['m_oferta_produktowa_promotions_books']['plc_err'];
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$_POST['do']='books';
		$_GET['do']='books';
		$this->Show($pSmarty);
	} // end of placeOnMain() funciton
	
} // end of Module Class
?>