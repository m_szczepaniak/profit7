<?php

class Module
{
    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    private $pDbMgr;

    /**
     * @var Smarty
     */
    private $pSmarty;

    function __construct($pSmarty)
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;
        $this->pSmarty = $pSmarty;

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        $sDo = $_POST['do'];

        if (empty($sDo)) {
            $this->Show();
        } else {
            $this->Parse();
        }
    }

    private function Show()
    {
        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('boxy', _('Boxy'), ['enctype' => 'multipart/form-data']);

        $pForm->AddHidden('do', 'parse');

        $pForm->AddFile('boxy', 'Plik z boxami');
        $pForm->AddInputButton('send', 'Wyślij');

        $html = $pForm->ShowForm();

        $this->pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($html));
    }

    private function Parse()
    {
        $arrayToProceed = $this->csvs(file_get_contents($_FILES['boxy']['tmp_name']), 1, ';');

        global $pDbMgr;

        $statuses = [
            4 => 'Zrealizowane',
            5 => 'Anulowane',
            1 => 'W realizacji',
            2 => 'Skompletowane',
            3 => 'Zatwierdzone',
        ];

        $data = [];

        foreach($arrayToProceed as $element) {
            $ourEmail = false;

            $element = str_replace(["\r", "\n"], '', $element[1]);

            if (strlen($element) == 11) {

                $element = '0'.$element;
            }

            $status = $pDbMgr->GetOne('profit24', "SELECT order_status FROM orders WHERE order_number = '$element'");
            $mail = $pDbMgr->GetOne('profit24', "SELECT email FROM orders WHERE order_number = '$element'");
            $order_date = $pDbMgr->GetOne('profit24', "SELECT order_date FROM orders WHERE order_number = '$element'");

            $elementStatus = $statuses[$status];

            $ourDomains = [
                'profitm.pl',
                'profit24.pl',
                'mestro.pl',
                'okids.pl',
                'habano.pl',
                'nieprzeczytane.pl',
                'smarkacz.pl',
                'ksiegarnia.it',
                'naszabiblioteka.pl'
            ];
            foreach ($ourDomains as $domain) {
                if (stristr($mail, '@'.$domain)) {
                    $ourEmail = true;
                }
            }

            $sSql = "SELECT order_number FROM orders 
                    WHERE order_number <> '$element' AND 
                        email = '".$mail."' AND 
                        order_date >= DATE_SUB('".$order_date."', INTERVAL 1 YEAR) AND 
                        '".$order_date."' > order_date
";
            $otherOrderNumber = $pDbMgr->GetCol('profit24', $sSql);
            if ($status == "5" || true === $ourEmail || !empty($otherOrderNumber)) {
                $reason = '';
                if ($status == "5") {
                    $reason .= 'Anulowane ';
                }
                if (true == $ourEmail) {
                    $reason .= 'Nasz email ';
                }
                if (!empty($otherOrderNumber)) {
                    $reason .= 'W ciągu ostatnich 12 miesiący było inne zamówienie od tego użytkownika - '.implode(', ', $otherOrderNumber);
                }
                $data[] = [
                    'oid' => $element,
                    'status' => $elementStatus,
                    'email' => $mail,
                    'order_date' => $order_date,
                    'powód' => $reason,
                ];
            }
        }

//        usort($data, function ($item1, $item2) {
//            if ($item1['status'] == $item2['status']) return 0;
//            return $item1['status'] < $item2['status'] ? -1 : 1;
//        });

        if (!empty($data)) {
            $this->array2csv2($data, 'csv/transakcje_2.csv');
        } else {
            $this->sMsg = GetMessage('Wszystkie transakcje są zgodne', false);
        }

        return $this->Show();
    }

    function array2csv2(array &$array, $filename)
    {
        $_GET['hideHeader'] = 1;

        $headers = [
            'Nr zamówienia',
            'Status',
            'Email',
            'Data zamowienia',
            'Powod'
        ];

        if (count($array) == 0) {
            return null;
        }

        ob_clean();
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename=przeparsowane_'.str_replace('.csv', '', $_FILES['boxy']['name']).'.csv');
        $df = fopen('php://output', 'w');
        fputcsv($df, $headers);
        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        ob_flush();
        die;
    }

    function csvs($string, $skip_rows = 0, $separatorChar = ',', $enclosureChar = '"', $newlineChar = "\n")
    {
        // @author: Klemen Nagode
        // @source: http://stackoverflow.com/questions/1269562/how-to-create-an-array-from-a-csv-file-using-php-and-the-fgetcsv-function
        $array = array();
        $size = strlen($string);
        $columnIndex = 0;
        $rowIndex = 0;
        $fieldValue="";
        $isEnclosured = false;
        for($i=0; $i<$size;$i++) {

            $char = $string{$i};
            $addChar = "";

            if($isEnclosured) {
                if($char==$enclosureChar) {

                    if($i+1<$size && $string{$i+1}==$enclosureChar){
                        // escaped char
                        $addChar=$char;
                        $i++; // dont check next char
                    }else{
                        $isEnclosured = false;
                    }
                }else {
                    $addChar=$char;
                }
            }else {
                if($char==$enclosureChar) {
                    $isEnclosured = true;
                }else {

                    if($char==$separatorChar) {

                        $array[$rowIndex][$columnIndex] = $fieldValue;
                        $fieldValue="";

                        $columnIndex++;
                    }elseif($char==$newlineChar) {
                        echo $char;
                        $array[$rowIndex][$columnIndex] = $fieldValue;
                        $fieldValue="";
                        $columnIndex=0;
                        $rowIndex++;
                    }else {
                        $addChar=$char;
                    }
                }
            }
            if($addChar!=""){
                $fieldValue.=$addChar;

            }
        }

        if($fieldValue) { // save last field
            $array[$rowIndex][$columnIndex] = $fieldValue;
        }


        /**
         * Skip rows.
         * Returning empty array if being told to skip all rows in the array.
         */
        if ($skip_rows > 0) {
            if (count($array) == $skip_rows)
                $array = array();
            elseif (count($array) > $skip_rows)
                $array = array_slice($array, $skip_rows);

        }

        return $array;
    }
}

