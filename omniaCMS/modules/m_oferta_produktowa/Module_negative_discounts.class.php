<?php

use LIB\Helpers\StringHelper;

class Module
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    private $pDbMgr;

    function __construct($pSmarty)
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        if($_GET['reset'] == '1'){
            resetViewState($this->sModule);
        }

        if (isset($_POST['website_id'])) {
            $_SESSION['website_id'] = $_POST['website_id'];
        }

        if (isset($_POST['has_not_ceneo_tarrif'])) {
            $_SESSION['has_not_ceneo_tarrif'] = $_POST['has_not_ceneo_tarrif'];
        }

        if (isset($_GET['page'])) {

            $_POST['page'] = $_GET['page'];
        }

        if (empty($_POST)) {
            $this->Show($pSmarty);
        } else {
            $this->showListAction($pSmarty);
        }
    }


    /**
     * @param $pSmarty
     * @return string
     */
    private function Show($pSmarty)
    {
        global $aConfig, $pDbMgr;

        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('stat_ordered_items', _('Produkty z ujemnymi rabatami'));

        $pForm->AddHidden('do', 'get_stats');

        $ceneoDataProvider = new \Ceneo\CeneoDataProvider();
        $ceneoShops = $ceneoDataProvider->getActiveAndUnactiveCeneoWebsites();

        $newCeneoShops = [];
        foreach($ceneoShops as $shopId => $ceneoShopName) {

            $newCeneoShops[] = [
                'label' => $ceneoShopName,
                'value' => $shopId
            ];
        }

        $minimumDiff = Common::formatPrice(0.99);

        $pForm->AddSelect('website_id', _('Sklep'), [], $newCeneoShops, [], '', false);
        $pForm->AddHidden('has_not_ceneo_tarrif');
        $pForm->AddCheckBox('has_not_ceneo_tarrif', _('Tylko produkty bez cennika ceneo'), [], [], [], '', false);

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Pokaż wyniki')) . '&nbsp;&nbsp;');

        $html = $pForm->ShowForm();

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($html));
    }

    private function showListAction($pSmarty)
    {
        global $aConfig, $pDbMgr;

        $websiteData = $pDbMgr->GetRow('profit24', "SELECT code, name from websites WHERE id = ".$_SESSION['website_id']);
        $websiteCode = $websiteData['code'];
        $websiteName = $websiteData['name'];

        rememberViewState($this->sModule);

        $aHeader = array(
            'header' => 'Lista produktów',
            //            'header' => $aConfig['lang'][$this->sModule]['list'],
            'refresh' => true,
            'bigSearch' => true,
            'search' => true
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 0,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'content'	=> '&nbsp;',
                'width'	=> '20'
            ),
            array(
                'content'	=> _('Id'),
                'width' => '30'
            ),
            array(
                'content'	=> _('isbn'),
                'width' => '150'
            ),
            array(
                'content'	=> _('Nazwa')
            ),
            array(
                'content'	=> _('Rabat')
            )
        );

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $extra = '';

        if ($_SESSION['has_not_ceneo_tarrif'] == 1) {

            $extra = "
            AND (SELECT COUNT(*) FROM products_tarrifs WHERE product_id = P.id AND type = 1) = 0
            ";
        }

        $search = $_POST['search'];
        $sSearch = '';
        if (isset($search) && !empty($search)) {
            $search = trim($search);
            if (StringHelper::SimpleCheckISBN($search)) {
                $sSearch = ' AND (
                          P.id = ' . (int) $search . ' OR
                          P.isbn_plain LIKE \'' . isbn2plain($search) . '\' OR
                          P.isbn_10 LIKE \'' . isbn2plain($search) . '\' OR
                          P.isbn_13 LIKE \'' . isbn2plain($search) . '\' OR
                          P.ean_13 LIKE \'' . isbn2plain($search) . '\'
                          )';
            } else {
                $sSearchStr = StringHelper::parseSearchString($search);
                $sSearch = ' AND (
                          P.name LIKE \'' . $sSearchStr . '\'
                          )';
            }
        }

        $iRowCount = $pDbMgr->GetOne($websiteCode, "
        SELECT count(DISTINCT P.id)
        FROM products_tarrifs AS PT
        JOIN products as P on P.id = PT.product_id
        WHERE PT.discount < 0
        $sSearch
        $extra
        ");

        $pView = new View('group_discount', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);
        $iCurrentPage = $pView->AddPager($iRowCount);

        $aConfig['lang'][$this->sModule]['no_items'] = _('Brak elementów');
        $perPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];

        $iStartFrom = ($iCurrentPage - 1) * $perPage;
        if (null == $iCurrentPage){
            $iStartFrom = 0;
        }

        $column = $websiteCode == 'profit24' ? 'P.id' : 'P.profit24_id';

        $aUsers = $pDbMgr->GetAll($websiteCode, "
        SELECT $column, P.isbn, P.name, PT.discount_value
        FROM products_tarrifs AS PT
        JOIN products as P on P.id = PT.product_id
        WHERE PT.discount < 0
        $sSearch
        $extra
        GROUP BY PT.product_id
        limit $iStartFrom, $perPage
        ");


        // ustawienia dla poszczegolnych kolumn rekordu
        $aColSettings = array();
        // dodanie rekordow do widoku
        $pView->AddRecords($aUsers, $aColSettings);
        // dodanie stopki do widoku

        $aRecordsFooter = array(

        );

        $pView->AddRecordsFooter($aRecordsFooter);
        // przepisanie langa - info o braku rekordow

        $pSmarty->assign('sContent', '<div style="clear:both; font-size:24px">Sklep: '.$websiteName.' Ilość: '.$iRowCount.'</div>'.$pView->Show());
    }
}

