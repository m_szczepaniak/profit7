<?php

use Ceneo\CeneoDataProvider;
use Listener\TabListener;
use EntityManager\EntityManager;
use Repositories\CeneoTabsRepository;

include_once('View/View.class.php');

class Module
{
    const CENEO_TAB_PRODUCT_PRIORITY = 4;

    private $sModule;
    private $pSmarty;
    private $aHeader = array(
        'header' => 'Lista produktów',
        //            'header' => $aConfig['lang'][$this->sModule]['list'],
        'refresh' => true,
        'bigSearch' => true,
        'search' => true,
        'searchInfo' => 'W zapytaniu możesz użyć znaku * (gwiazdka), aby poszerzyć zakres wyników np. prawo*, *prawo lub *prawo*'
    );
    private $aAttribs = array(
        'width' => '100%',
        'border' => 0,
        'cellspacing' => 0,
        'cellpadding' => 0,
        'class' => 'viewHeaderTable',
    );

    private $tabs = [
        [
            'type' => CeneoTabsRepository::CENEO_IN_TYPE,
            'name' => 'Ceneo In'
        ],
        [
            'type' => CeneoTabsRepository::CENEO_OUT_TYPE,
            'name' => 'Ceneo Out'
        ],
        [
            'type' => CeneoTabsRepository::CENEO_VIP_TYPE,
            'name' => 'Ceneo Vip'
        ],
        [
            'type' => 3,
            'name' => 'Produkty bez kategori drugiego poziomu'
        ],
        [
            'type' => self::CENEO_TAB_PRODUCT_PRIORITY,
            'name' => 'Ceneo priorytet produktu dla sklepu'
        ]
    ];

    public function __construct(&$pSmarty)
    {
        $this->type = $_GET['type'];

        if (null !== $this->type && '' != $this->type) {
            $_SESSION['ceneo_type'] = $this->type;
        } else {
            $this->type = $_SESSION['ceneo_type'];
        }

        $this->aHeader['action'] = phpSelf(['type' => $this->type]);
        $this->aHeader['refresh_link'] = phpSelf(['do' => 'reset', 'type' => $this->type]);

        $this->sModule = $_GET['module'].'_'.$_GET['action'].'_'.$this->type;
        $this->pSmarty = $pSmarty;

        if ($_POST['do'] == 'delete'){
            $this->removeProductAction();
        } else if($_GET['do'] == 'reset'){
            $this->resetAction();
        }

        switch (isset($_GET['innerAction'])) {
            case 'add_product':
                $this->addProductAction();
                break;

            default:
                $this->showTabsAction();
                break;
        }
    }

    private function resetAction()
    {
        resetViewState('m_oferta_produktowa_ceneo_tabs_'.$this->type);
    }

    private function savePriorityChanges()
    {
        global $pDbMgr;
        $type = $this->type;

        foreach($_POST['product_priorities'] as $productId => $data) {

            foreach($data as $element => $elementValue) {

                if ($element == 'website_id') {

                    if (!is_numeric($elementValue)) {

                        $data[$element] = 'NULL';
                    }

                } else {

                    $data[$element] = Common::formatPrice2($elementValue);
                }
            }

            $exist = $pDbMgr->GetRow('profit24', 'SELECT * FROM ceneo_product_priority WHERE product_id = '.$productId);

            if (!empty($exist)) {

                $pDbMgr->Update('profit24', 'ceneo_product_priority', $data, 'product_id = '. $productId);
            } else {

                $pDbMgr->Insert('profit24', 'ceneo_product_priority', $data);
            }
        }
        $d = '';
    }

    public function showTabsAction()
    {
        if ($this->type == 4 && isset($_POST['save_priorities'])) {

            $this->savePriorityChanges();
        }

        rememberViewState($this->sModule);
        $type = $this->type;

        $js = '<script type="text/javascript" src="js/ceneo_tabs.js"></script>';

        $em = new EntityManager();
        $repository = $em->getRepository('Repositories\CeneoTabsRepository');
        $repository->init($this->type);
        $pView = new View('ceneo_tabs_items', $this->aHeader, $this->aAttribs);

        $iRowCount = intval($repository->getAllByType($type, $_POST['f_price_from'], $_POST['f_price_to'], $_POST['search'], $_POST['per_page'], null , true));
        $iCurrentPage = $pView->AddPager($iRowCount);

        /** @var View $list */
        $list = $this->getList($pView, $type, $repository->getAllByType($type, $_POST['f_price_from'], $_POST['f_price_to'], $_POST['search'], $_POST['per_page'], $iCurrentPage));

        $this->pSmarty->assign('current', $type);
        $this->pSmarty->assign('menu', $this->tabs);
        $this->pSmarty->assign('list', $list->Show());
        $this->pSmarty->assign('type', $this->type);
        $sStockHtml = $this->pSmarty->fetch('ceneo_tabs.tpl');

        global $aConfig;

       $ss = '
					<script>

						$(document).ready(function(){
							$("#show_navitree").click(function() {
								window.open("ajax/NaviPopup.php",\'\',\'menubar=0,location=0,scrollbars=1,toolbar=0,width=500,height=300\');
							});
							$("#show_publishers_choose").click(function() {
                var sParams = $("#f_hide_categories").val();
								window.open("ajax/CategoriesChoosePopup.php?params=" + sParams,\'\',\'menubar=0,location=0,scrollbars=1,toolbar=0,width=800,height=900\');
							});
							$("a.stockTooltip").cluetip({
                width: "335px",
                local:true,
                showTitle: false,
                sticky: true,
                arrows: true,
                closePosition: "top",
                closeText: "Zamknij",
                mouseOutClose: true
              });
              $("#f_publisher").autocomplete({
                source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetPublishers.php"
              });
							$("#f_publisher").bind("blur", function(e){
						   	 var elSelSeries = document.getElementById("f_series");
						   	 if(e.target.value != "") {
				     	    $.get("ajax/GetSeries.php", { publisher: e.target.value },
									  function(data){
									    var brokenstring=data.split(";");
								    elSelSeries.length = 0;
									    elSelSeries.options[elSelSeries.length] =  new Option("' . $aConfig['lang']['common']['choose'] . '","");
									    for (x in brokenstring){
												if(brokenstring[x]){
													var item=brokenstring[x].split("|");
						  						elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
					  						}
					  				}
									 });
						 			} else { elSelSeries.length = 0; elSelSeries.options[elSelSeries.length] =  new Option("' . $aConfig['lang']['common']['choose'] . '",""); }
	    					});
						});
					</script>';

        $this->pSmarty->assign('sContent', $sStockHtml.$js.$ss.$this->getExtraWindow($type));

        return $sStockHtml;
    }

    private function addProductAction()
    {
        $products = explode(',', $_GET['aid']);
        $type = $this->type;

        $ceneoTabsListener = new TabListener($products, $type);

        if($type == CeneoTabsRepository::CENEO_OUT_TYPE){
            $ceneoTabsListener->onAddOut();
        }

        $em = new EntityManager();
        $repository = $em->getRepository('Repositories\CeneoTabsRepository');
        $repository->init($this->type);
        $repository->addProductsToTabs($products, $this->type);

        $this->showTabsAction();
    }

    private function removeProductAction()
    {
        $productsIds = array_keys($_POST['delete']);

        if (empty($productsIds)){

        }

        $em = new EntityManager();
        $repo = $em->getRepository('Repositories\CeneoTabsRepository');
        $repo->init($this->type);

        $profitProductsIds = $repo->findProductsByTabId($productsIds);

        foreach($productsIds as $productId){
            $repo->deleteById($productId);
        }

        $ceneoTabsListener = new TabListener($profitProductsIds, null);
        $ceneoTabsListener->onAddOut();

        $this->showTabsAction();
    }

    private function getList(View $pView, $type, $data)
    {
        global $aConfig;


        $aRecordsHeader = array(
            array(
                'content' => '&nbsp;',
                'sortable' => false
            ),
            array(
                'content' => 'Isbn',
                'sortable' => false,
                'width' => '50'
            ),
            array(
                'content' => 'Id produktu',
                'sortable' => false,
                'width' => '50'
            ),
            array(
                'content' => 'Nazwa produktu',
                'sortable' => false,
                'width' => '50'
            ),
        );

        if ($this->type == 4) {

            $ceneoDataProvider = new CeneoDataProvider();
            $shops = $ceneoDataProvider->getCeneoWebsites();

            global $pDbMgr;

            $allData = $pDbMgr->GetAssoc('profit24', 'SELECT product_id, website_id FROM ceneo_product_priority');

            foreach($data as $key => $element) {

                $exist = $pDbMgr->GetRow('profit24', 'SELECT * FROM ceneo_product_priority WHERE product_id = '.$element['product_id']);

                $html = '';

                $select = '<select class="site_changer" name="product_priorities['.$element["product_id"].'][website_id]" style="width:200px">';

                    $select .= '<option>Wybierz sklep</option>';
                    foreach($shops as $shop) {

                        $selectedValue = $allData[$element["product_id"]] == $shop["id"] ? 'selected' : '';

                        $select .= '<option '.$selectedValue.' value="'.$shop["id"].'">'.$shop['ceneo'].'</option>';
                    }
                $select .= '</select>';

                $html .= $select;

                $radio = '<ul style="list-style-type: none">';
                foreach($shops as $shop) {

                    $value = 0.00;
                    $readonly = '';

                    if (isset($exist[$shop["code"].'_percentage_modifier'])) {

                        $value = $exist[$shop["code"].'_percentage_modifier'];
                    }

                    if ($shop['id'] == $exist['website_id']) {

                        $readonly = ' readonly ';
                    }

                    $value = Common::formatPrice($value);

                    $radio .= '<li style="width:100%;margin-bottom:10px">
                            <label style="width:100px;display:inline-block">'.$shop['ceneo'].'</label>
                            <input'.$readonly.' data-code="'.$shop["id"].'" type="text" name="product_priorities['.$element["product_id"].']['.$shop["code"].'_percentage_modifier]" value="'.$value.'">
                            </li>';
                }

                $radio .= '</ul>';
                $html .= $radio;

                $element['shop_id'] = $html;

                $data[$key] = $element;
            }

            $aRecordsHeader[] = [
                'content' => 'Sklep główny',
                'sortable' => false,
                'width' => '200'
            ];
        }

        $aColSettings = [
            'id' => array(
                'show' => false
            )
        ];

        if ($this->type == 3) {

            $backUri = urlencode(phpSelf(['type' => $this->type]));

            foreach($data as $key => $element) {

                $input = '<a href="%s" title="Edytuj"><img src="gfx/icons/edit_ico.gif" alt="Edytuj" border="0"></a>';
//                $link = str_replace('&action=', '', phpSelf(array('action' => false, 'do' => 'edit', 'back_uri' => phpSelf(['type' => $this->type]), 'id' => $element['product_id'])));
                $link = str_replace('&action=', '', phpSelf(array('action' => false, 'do' => 'edit', 'id' => $element['product_id'], 'back_uri' => $backUri)));

                $element['action'] = sprintf($input, $link);
                $data[$key] = $element;
            }

            $aRecordsHeader[] = [
                'content' => 'Akcje',
                'sortable' => false,
                'width' => '50'
            ];
        }
//        $aRecords[$iKey]['action']['links']['name'] = phpSelf(array('do' => 'edit_packet', 'id' => $aItem['id']));
        $pView->AddRecords($data, $aColSettings);
        $pView->AddRecordsHeader($aRecordsHeader);
        $pView->setActionColSettings($aColSettings);

        // filtry
        //filtr ceny
        $pView->AddFilter('f_price_from', '', '<label>' . $aConfig['lang']['m_oferta_produktowa']['f_price'] . '</label> <input type="text" name="f_price_from" value="' . $_POST['f_price_from'] . '" style="width:50px;" maxlength="7" />', $_POST['f_price_from'], 'html');
        $pView->AddFilter('f_price_to', '', '<label>' . $aConfig['lang']['m_oferta_produktowa']['f_price_to'] . '</label> <input type="text" name="f_price_to" value="' . $_POST['f_price_to'] . '" style="width:50px;" maxlength="7" /> ', $_POST['f_price_to'], 'html');
        $pView->AddFilter('f_publisher', $aConfig['lang']['m_oferta_produktowa']['f_publisher'], array(), $_POST['f_publisher'], 'text');
        $pView->AddFilter('type', '', array(), $type, 'text', true);
        // dodanie stopki do widoku


        if ($this->type != 3){
            $aRecordsFooter = array(
                array('check_all', 'delete_all')
            );
        }

        $pView->AddRecordsFooter($aRecordsFooter);

        return $pView;
    }

    private function getExtraWindow($type)
    {
        global $aConfig;

        $sJs='<script>function openSearchWindow(disc) {
				window.open("ajax/SearchProduct.php?ppid='.$iPId.'&pid='.$iId.'&site=profit24&tab_type='.$type.'&mode=8&disc="+disc,"","menubar=0,location=0,scrollbars=1,resizable=1,toolbar=0,width=700,height=500");
				};

				function createCookie(name,value, days) {
					if (days) {
						var date = new Date();
						date.setTime(date.getTime()+(days*24*60*60*1000));
						var expires = "; expires="+date.toGMTString();
					}
					else var expires = "";
					document.cookie = name+"="+value+expires+"; path=/";
				}

				function eraseCookie(name) {
					createCookie(name,"",-1);
				}

				function showHideRow(id) {
					var status = 		document.getElementById("listContainer"+id).style.display;
					var newStatus=	"none";
					var label=			"'.$aConfig['lang'][$this->sModule]['show'].'";
					eraseCookie("displayedPromotionVal"+id);
					if(status=="none") {
						newStatus="";
						label=			"'.$aConfig['lang'][$this->sModule]['hide'].'";
						createCookie("displayedPromotionVal"+id,1);
						}

					document.getElementById("listContainer"+id).style.display=newStatus;
					document.getElementById("listContainer2_"+id).style.display=newStatus;
					document.getElementById("btnShowHide"+id).value=label;
					}
				</script>';

        return $sJs;
    }
}