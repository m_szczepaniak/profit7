<?php
/**
 * Klasa Module do obslugi modulu 'Darmowa dostawa'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = getModulePageId('m_powiazane_oferta_produktowa');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (!empty($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}
		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}

		// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'remove': $this->RemoveFromPromotion($pSmarty, $iPId,$iId); break;
			case 'removeVal': $this->RemoveVal($pSmarty, $iPId,$iId); break;
			case 'insert': $this->Insert($pSmarty); break;
			case 'insertBooks': $this->AddBooks($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'edit': $this->AddEditForm($pSmarty,$iId); break;
			case 'add': $this->AddEditForm($pSmarty); break;
			case 'search_books_add': $this->AddBooksForm($pSmarty, $iPId, true); break;	
			//case 'add_books': $this->AddBooksForm($pSmarty, $iPId); break;					
			case 'books': $this->ShowBooks($pSmarty, $iPId); break;						
			/*
			case 'sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				if($_GET['pid']>0) {
					//widok dla sortowania książek
					$pSmarty->assign('sContent',$oSort->Show('', $this->getBooksInPromotion($_GET['pid']),
 						phpSelf(array('do'=>'proceed_sort','pid'=>$_GET['pid']),
 				 		array(), false)));
					}
				else {
					//widok do sortowania listy promocji w dziale	
				$pSmarty->assign('sContent',
													$oSort->Show('',
																			 $this->getRecords($_GET['fpid']),
																			 phpSelf(array('do'=>'proceed_sort','fpid'=>$_GET['fpid']),
																			 				 array(), false)));
					}
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				if($_GET['pid']>0) {
					$oSort->Proceed($aConfig['tabls']['prefix']."products_tarrifs",
												$this->getBooksInPromotion($_GET['pid']),
												array('promotion_id' => $_GET['pid']),
											 	'order_by');							
					}
				else {
					$oSort->Proceed($aConfig['tabls']['prefix']."products_free_delivery",
												$this->getRecords($_GET['fpid']),
												array('page_id' => $_GET['fpid']),
											 	'order_by');
				}
			break;
			 */
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych promocji
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		
		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'start_date',
				'content'	=> $aConfig['lang'][$this->sModule]['date_start'],
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'end_date',
				'content'	=> $aConfig['lang'][$this->sModule]['date_end'],
				'sortable'	=> true,
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '6%'
			)
		);
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(A.id)
						 FROM ".$aConfig['tabls']['prefix']."products_free_delivery A
						 WHERE 1 = 1";
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_free_delivery";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_free_delivery', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT A.id, A.name, A.start_date, A.end_date
								FROM ".$aConfig['tabls']['prefix']."products_free_delivery A
							 WHERE 1 = 1".
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.id').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('books', 'edit', 'delete'),
					'params' => array (
												'delete'	=> array('id'=>'{id}'),
												'edit'	=> array('id'=>'{id}'),
												'books'	=> array('pid'=>'{id}'),
											)
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('add')
		);
	
		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function
	
	
	/**
	 * Metoda wyswietla liste ksiazek promocji
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function ShowBooks($pSmarty, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');
		include_once('Form/FormTable.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule.'_books');
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT name
						 FROM ".$aConfig['tabls']['prefix']."products_free_delivery
							WHERE id = ".$iId;
		$sPromoName=Common::GetOne($sSql);

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'action' => phpSelf(array('do'=>'remove'), array(), false),
			'second' => true,
			'per_page' => false		
		);

		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'B.name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'base_price',
				'content'	=> $aConfig['lang'][$this->sModule]['list_base_price'],
				'sortable'	=> true,
				'width'	=> '45'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);
		
		
		$aColSettings = array(
				'order_by'	=> array(
					'show'	=> false
				),
				'id'	=> array(
					'show'	=> false
				),
				'fd_id'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('delete','preview'),
					'params' => array (
						'delete'	=> array('do'=>'remove', 'id' => '{id}')
					),
					'show' => false
				)
			);
			
		$aRecordsFooter = array(
			array('check_all', 'remove_all')
		);
		$aRecordsFooterParams = array(
			
		
												 );

		$pForm = new FormTable('books_discount_search', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'add_to_promo');
		
		$iViewCounter=0;

		$pForm->AddMergedRow(sprintf($aConfig['lang'][$this->sModule]['free_delivery'], $this->getPromotionName($iId)), array('class'=>'merged', 'style'=>'padding-left: 10px; background-color:#736f54; color:#fff; '));


		//dla kazdej stawki pobieramy liste książek
		$pView = new View('free_delivery_books'.(++$iViewCounter), $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);


			//jeżeli są jakieś ksiazki
			$sSql = "SELECT B.id, B.name, 
											B.price_brutto base_price, 
											C.name AS publisher, 
											A.product_id, 
											B.publication_year, 
												(SELECT order_by 
												FROM ".$aConfig['tabls']['prefix']."products_shadow S 
												WHERE S.id=B.id) AS order_by 
					FROM ".$aConfig['tabls']['prefix']."products_free_delivery_items A
					JOIN ".$aConfig['tabls']['prefix']."products B
					ON B.id = A.product_id
					LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers C
						ON B.publisher_id = C.id
					WHERE A.products_free_delivery_id=".$iId.
				' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'order_by').
					(isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '');
			$aBooks =& Common::GetAll($sSql);
			foreach ($aBooks as $iKey => $aBook) {
				$aBooks[$iKey]['name'] = '<b><a href="'.createProductLink($aBook['product_id'], $aBook['name']).'" target="_blank">'.$aBook['name'].'</a></b>'.(!empty($aBook['publisher'])?'<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($aBook['publisher'],0,40,'UTF-8').'</span>':'');
				$aBooks[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aBook['product_id']),0,40,'UTF-8').' '.$aBook['publication_year'].'</span>';
				unset($aBooks[$iKey]['publisher']);
				//$aBooks[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aBook['product_id']),0,40,'UTF-8').' '.$aBook['publication_year'].'</span>';
				unset($aBooks[$iKey]['publication_year']);
				// link podgladu
				$aBooks[$iKey]['action']['links']['preview'] = (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'].'/' : '').(mb_strlen($aBook['name'], 'UTF-8')>210?mb_substr(link_encode($aBook['name']),0,210, 'UTF-8'):link_encode($aBook['name'])).',product'.$aBook['product_id'].'.html?preview='.md5($_SERVER['REMOTE_ADDR'].$aConfig['common']['client_base_url_http']);
				unset($aBooks[$iKey]['product_id']);	

			}		 
		$pView->AddRecords($aBooks, $aColSettings);
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		//dodanie tej formy jest konieczne ze względu na błąd interpretacjis truktury DOM w firefox
		$pForm->AddMergedRow(('<form name="NiewidocznaWDOM"></form>').$pView->Show(), array(), array( 'id'=>'listContainer'.$aItem['id'], 'style'=>' '));
		$pForm->AddMergedRow($pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['add_product_w_disc'].Common::formatPrice($aItem['value']).$aConfig['lang'][$this->sModule]['discount3'], array('onclick'=>'openSearchWindow('.$aItem['value'].'); return false;', 'style'=>'float:left; margin-left:10px;')).
													(!$iRowCount2?$pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['del_discount'], array('onclick'=>'window.location.href=\'?frame=main&module_id=26&module=m_oferta_produktowa&action=free_delivery&do=removeVal&pid='.$iId.'&id='.$aItem['id'].'\'; return false;','style'=>'margin-top:-2px; margin-left:20px; float:right;')):'&nbsp;').
														($_COOKIE['displayedPromotionVal'.$aItem['id']]?'<script>showHideRow('.$aItem['id'].')</script>':''), 
														array(), array( 'id'=>'listContainer2_'.$aItem['id'], 'style'=>' display:none;'));

		$pForm->AddRow($pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['add_books'], array('onclick'=>'openSearchWindow('.$aItem['value'].'); return false;')),$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['m_oferta_produktowa']['go_back'], array('style'=>'float:right;','onclick'=>'window.location.href=\'?frame=main&module_id=26&module=m_oferta_produktowa&action=free_delivery\'; return false;')));
		$sJs='<script>function openSearchWindow(disc) {
				window.open("ajax/SearchProduct.php?ppid='.$iPId.'&pid='.$iId.'&site=profit24&mode=6","","menubar=0,location=0,scrollbars=1,resizable=1,toolbar=0,width=700,height=500");
				};
				
				function createCookie(name,value, days) {
					if (days) {
						var date = new Date();
						date.setTime(date.getTime()+(days*24*60*60*1000));
						var expires = "; expires="+date.toGMTString();
					}
					else var expires = "";
					document.cookie = name+"="+value+expires+"; path=/";
				}
				
				function eraseCookie(name) {
					createCookie(name,"",-1);
				}
				
				function showHideRow(id) {
					var status = 		document.getElementById("listContainer"+id).style.display;
					var newStatus=	"none";
					var label=			"'.$aConfig['lang'][$this->sModule]['show'].'";
					eraseCookie("displayedPromotionVal"+id);
					if(status=="none") {
						newStatus="";
						label=			"'.$aConfig['lang'][$this->sModule]['hide'].'";
						createCookie("displayedPromotionVal"+id,1);
						}
					
					document.getElementById("listContainer"+id).style.display=newStatus;
					document.getElementById("listContainer2_"+id).style.display=newStatus;
					document.getElementById("btnShowHide"+id).value=label;
					}
				</script>';
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJs.
																				ShowTable($pForm->ShowForm()));
	} // end of Show() function
	

	/**
	 * Metoda usuwa wybrane produkty z promocji
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego produktu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			foreach ($_POST['delete'] as $iItem) {
				// najpierw pobierzemy ksiażki w danej promocji
				$aProdIds = $this->getProductIdByPromotion($iItem);
				// usuwanie
				if (($iRecords = $this->deleteItem($iItem)) === false) {
					$bIsErr = true;
					break;
				}
				
				foreach ($aProdIds as $iProdId) {
					// przeliczmy teraz pakiety z tym produktem
					// jeśli produkt jest składową pakietu, aktualizacja tych pakietów
					$aPacketsProduct = $this->getPacketsProduct($iProdId);
					foreach ($aPacketsProduct as $iPacketProduct) {
						if ($this->updateProductPacketPrice($iProdId, $iPacketProduct) === false) {
							$bIsErr = true;
							break;
						}
						if ($this->updatePacketAuthorsPrices($iPacketProduct) === false) {
							$bIsErr = true;
							break;
						}
					}
				}
			}
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg =$aConfig['lang'][$this->sModule]['del_ok'];
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['del_err'];
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton
	
	
	/**
	 * Metoda pobiera idki książek które należą do pakietu na podstawie tabeli cenników
	 *
	 * @param integer $iPromotionId 
	 */
	function getProductIdByPromotion($iPromotionId) {
		global $aConfig;
		
		$sSql = "SELECT product_id FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						 WHERE promotion_id=".$iPromotionId;
		return Common::GetCol($sSql);
	}// end of getProductIdByPromotion() method
	
	
	/**
	 * Metoda usuwa wybrane produkty z promocji
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego produktu
	 * @return 	void
	 */
	function RemoveFromPromotion(&$pSmarty, $iPId,$iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			foreach ($_POST['delete'] as $iItem) {
				// usuwanie
				if (($iRecords = $this->deletePromotionItem($iItem, $iPId)) === false) {
					$bIsErr = true;
					break;
				}
			}
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if (1) {
					$sMsg =$aConfig['lang'][$this->sModule]['del_prom_ok'];
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['del_prom_err'];
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$_GET['do']='books';
		$this->ShowBooks($pSmarty,$iPId);
	} // end of Delete() funciton
	
	/**
	 * Metoda usuwa wybrane produkty z promocji
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego produktu
	 * @return 	void
	 */
	function RemoveVal(&$pSmarty, $iPId,$iId) {
		global $aConfig;
		$bIsErr = false;
		
		Common::BeginTransaction();
		$bIsErr =! $this->deleteVal($iId);	
		if (!$bIsErr) {
			// usunieto
			Common::CommitTransaction();
			$sMsg =$aConfig['lang'][$this->sModule]['del_val_ok'];
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
		}
		else {
			// blad
			Common::RollbackTransaction();
			$sMsg = $aConfig['lang'][$this->sModule]['del_val_err'];
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
		}	
		$_GET['do']='books';
		$this->ShowBooks($pSmarty,$iPId);
	} // end of RemoveVal() funciton

	/**
	 * Metoda dodaje produkt do promocji
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST) || $oValidator->sError != '') {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditForm($pSmarty);
			return;
		}
		
		$_POST['discount'] = Common::formatPrice2($_POST['discount']);
		if (strtotime($_POST['start_date'].' '.$_POST['start_time'])<strtotime($_POST['end_date'].' '.$_POST['end_time'])) {
			Common::BeginTransaction();
			
			$aValues=array(
				'name' => $_POST['name'],
				'start_date'	=>	FormatDateHour($_POST['start_date'], $_POST['start_time'] ),
				'end_date'	=>	FormatDateHour($_POST['end_date'], $_POST['end_time']),
				'created' => 'NOW()',
				'created_by' => $_SESSION['user']['name']
			);
			if (($iPId=Common::Insert($aConfig['tabls']['prefix']."products_free_delivery",
											 $aValues)) === false) {
				$bIsErr = true;
			}
			
			// dodaj metody transportu
			if (isset($_POST['transport_methods']) && !empty($_POST['transport_methods']) && is_array($_POST['transport_methods'])) {
				foreach ($_POST['transport_methods'] as $iTMethodId) {
					$aVal = array(
						'transport_id' => $iTMethodId,
						'free_delivery_id' => $iPId
					);
					if (Common::Insert($aConfig['tabls']['prefix']."products_free_delivery_transport_method", $aVal) === false) {
						$bIsErr = true;
						break;
					}
				}
			}
					
			if (!$bIsErr) {
				// dodano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
												$_POST['name']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				// reset ustawien widoku
				$_GET['reset'] = 2;
				$this->Show($pSmarty);
			}
			else {
				// blad
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err']);
				$this->sMsg = GetMessage($sMsg);
	
				AddLog($sMsg);
				$this->AddEditForm($pSmarty);
			}

		}
		else {
			// wysokosc rabatu musi byc wieksza od 0
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['date_value_err']);
			$this->AddEditForm($pSmarty);
		}
	} // end of AddItem() funciton\
	
	
	/**
	 * Metoda sprawdza czy książka jest już przypisana do darmowej dostawy
	 *
	 * @global type $aConfig
	 * @param type $iBookId
	 * @param type $iFDId
	 * @return type 
	 */
	function checkBookExist($iBookId, $iFDId) {
		global $aConfig;
		
		$sSql = "SELECT id FROM products_free_delivery_items 
						 WHERE product_id=".$iBookId." AND 
									 products_free_delivery_id = ".$iFDId;
		return Common::GetOne($sSql) > 0 ? true : false;
	}// end of checkBookExist() method
	
	
	function AddBooks(&$pSmarty,$iPId) {
		global $aConfig;
		$bIsErr = false;
		
		$aSelItems=explode(',', $_GET['aid']);
		Common::BeginTransaction();
		
		foreach ($aSelItems as $iBId) {
			if ($this->checkBookExist($iBId, $iPId) == false) {
				// ksiazka jeszcze nie jest przypisana do darmowej dostawy
				$aValues = array(
						'products_free_delivery_id' => $iPId,
						'product_id' => $iBId
				);
				if (Common::Insert($aConfig['tabls']['prefix']."products_free_delivery_items", $aValues) === false) {
					$bIsErr = true;
					continue;
				}
			}
		}
		
		$sFDName = $this->getPromotionName($iPId);
		$sPNames = $this->getProductsNames($_GET['aid']);
	
		if (!$bIsErr) {
			// dodano
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_topromo_ok'],
										$sPNames,
										$sFDName);
			if(!empty($sMsg)) $this->sMsg = GetMessage($sMsg, false);

			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			// blad
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_topromo_err'],
											$sFDName);
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			$_GET['do']='books';
			$this->ShowBooks($pSmarty, $iPId);
		}
		$_GET['do']='books';
		$this->ShowBooks($pSmarty, $iPId);
	} // end of AddItem() funciton\
	
	
	/**
	 * Metoda dodaje produkt do promocji
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty,$iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditForm($pSmarty);
			return;
		}
		
		$_POST['discount'] = Common::formatPrice2($_POST['discount']);
		if (strtotime($_POST['start_date'].' '.$_POST['start_time'])<strtotime($_POST['end_date'].' '.$_POST['end_time'])) {//$_POST['discount'] > 0 && 
			Common::BeginTransaction();
			
			$aValues=array(
				'name' => $_POST['name'],
				'start_date'	=>	FormatDateHour($_POST['start_date'], $_POST['start_time']),
				'end_date'	=>	FormatDateHour($_POST['end_date'], $_POST['end_time']),
				'modified' => 'NOW()',
				'modified_by' => $_SESSION['user']['name']
			);
			if ((Common::Update($aConfig['tabls']['prefix']."products_free_delivery",
											 $aValues,
											 'id = '.$iId)) === false) {
				$bIsErr = true;
			}
			
			// dodaj metody transportu
			if (isset($_POST['transport_methods']) && !empty($_POST['transport_methods']) && is_array($_POST['transport_methods'])) {
				$sSql = "DELETE FROM products_free_delivery_transport_method WHERE free_delivery_id=".$iId;
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				} else {
					foreach ($_POST['transport_methods'] as $iTMethodId) {
						$aVal = array(
							'transport_id' => $iTMethodId,
							'free_delivery_id' => $iId
						);
						if (Common::Insert($aConfig['tabls']['prefix']."products_free_delivery_transport_method", $aVal) === false) {
							$bIsErr = true;
							break;
						}
					}
				}
			}


			if (!$bIsErr) {
				// dodano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],$_POST['name']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				// reset ustawien widoku
				//$_GET['reset'] = 2;
				$this->Show($pSmarty);
			}
			else {
				// blad
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],$_POST['name']);
				$this->sMsg = GetMessage($sMsg);

				AddLog($sMsg);
				$this->AddEditForm($pSmarty);
			}
		}
		else {
			// wysokosc rabatu musi byc wieksza od 0
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['date_value_err']);
			$this->AddEditForm($pSmarty);
		}
	} // end of AddItem() funciton
	

	/**
	 * Metoda tworzy formularz wyboru produktu, ktory ma zostac dodany do listy
	 * promocji
	 *
	 * @param	object	$pSmarty
	 */
	function AddEditForm(&$pSmarty, $iId=0) {
		global $aConfig;
		if($_POST['id']>0)	$iId=intval($_POST['id']);		
		$aData = array();
		$sHtml = '';
		if ($iId > 0) {
			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT *,DATE_FORMAT(start_date, '".$aConfig['common']['cal_sql_date_format']."')
											AS start_date, 
											DATE_FORMAT(start_date, '".$aConfig['common']['sql_hour_format']."') AS start_time,
											DATE_FORMAT(end_date, '".$aConfig['common']['cal_sql_date_format']."')
											AS end_date,
											DATE_FORMAT(end_date, '".$aConfig['common']['sql_hour_format']."') AS end_time
							 FROM ".$aConfig['tabls']['prefix']."products_free_delivery
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId>0?1:0)];
		if ($iId > 0) {
			$sHeader .= ' "'.$aData['name'].'"';
		}
				
		$pForm = new FormTable('books_discount', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		if ($iId > 0) {
			$pForm->AddHidden('do', 'update');
			$pForm->AddHidden('id', $iId);
		}
		else {
			$pForm->AddHidden('do', 'insert');
		}
		
		// nazwa promocji
		$pForm->AddText('name',$aConfig['lang'][$this->sModule]['name'],$aData['name'],array('style'=>'width:300px;'));
		
		if (empty($aData['transport_methods']) && $iId > 0) {
			// pobierz z DB
			$aData['transport_methods'] = $this->getLinkedTransportMethods($iId);
		}
		// pobranie dostępnych metod transportu
		$aTMethods = $this->getTransportMethods();
		$pForm->AddCheckBoxSet('transport_methods', $aConfig['lang'][$this->sModule]['transport_methods'], $aTMethods, $aData['transport_methods'], '', true, false);
		
		$pForm->AddRow($aConfig['lang'][$this->sModule]['start_date'],
									 '<div style="float: left;">'.$pForm->GetTextHTML('start_date', $aConfig['lang'][$this->sModule]['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date', false).'</div><div style="float: left; padding-top: 3px; padding-left: 10px;">'.$pForm->GetLabelHTML('start_time', $aConfig['lang'][$this->sModule]['hour'], false).' '.$pForm->GetTextHTML('start_time', $aConfig['lang'][$this->sModule]['start_hour'], !empty($aData['start_time'])?$aData['start_time']:'07:00', array('style'=>'width: 50px;'), '', 'hour', false).'</div>');
		
		// data końcowa
		//$pForm->AddText('end_date', $aConfig['lang'][$this->sModule]['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date');
		//$pForm->AddText('end_time', $aConfig['lang'][$this->sModule]['end_time'], !empty($aData['end_time'])?$aData['end_time']:'23:59:59', array('style'=>'width: 40px;', 'maxlength'=>'5'), '');
		$pForm->AddRow($aConfig['lang'][$this->sModule]['end_date'],
									 '<div style="float: left;">'.$pForm->GetTextHTML('end_date', $aConfig['lang'][$this->sModule]['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date', false).'</div><div style="float: left; padding-top: 3px; padding-left: 10px;">'.$pForm->GetLabelHTML('end_time', $aConfig['lang'][$this->sModule]['hour'], false).' '.$pForm->GetTextHTML('end_time', $aConfig['lang'][$this->sModule]['end_hour'], !empty($aData['end_time'])?$aData['end_time']:'23:59', array('style'=>'width: 50px;'), '', 'hour', false).'</div>');
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	
	} // end of Add() function
	
	
	/**
	 * Metoda pobiera powiązane metody transportu
	 *
	 * @global type $aConfig
	 * @param integer $iId
	 * @return array 
	 */
	function getLinkedTransportMethods($iId) {
		global $aConfig;
		
		$sSql = "SELECT transport_id
						FROM ".$aConfig['tabls']['prefix']."products_free_delivery_transport_method
						WHERE free_delivery_id =".$iId;
		return Common::GetCol($sSql);
	}// end of getLinkedTransportMethods() method
	
	
	/**
	 * Metoda pobiera wszystkie dostępne metody transportu
	 *
	 * @global type $aConfig
	 * @return type 
	 */
	function getTransportMethods() {
		global $aConfig;
		
		$sSql = "SELECT id as value, name as label 
						FROM ".$aConfig['tabls']['prefix']."orders_transport_means
						ORDER BY order_by";
		return Common::GetAll($sSql);
	}// end of getTransportMethods() method
	

	function GetBooksList(&$pSmarty, &$aBooks) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'form'	=>	false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'width'	=> '20'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['list_id'],
				'width' => '30'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['list_name']
			),

			array(
				'content'	=> $aConfig['lang'][$this->sModule]['list_base_price'],
				'width' => '100'
			)
		);

		$pView = new View('books_discount', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array();
		// dodanie rekordow do widoku
		foreach($aBooks as $iKey=>$aBook){
			$aBooks[$iKey]['name'] = '<b>'.$aBook['name'].'</b>'.(!empty($aBook['publisher'])?'<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($aBook['publisher'],0,40,'UTF-8').'</span>':'');
			unset($aBooks[$iKey]['publisher']);
			$aBooks[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aBook['id']),0,40,'UTF-8').' '.$aBook['publication_year'].'</span>';
			unset($aBooks[$iKey]['publication_year']);
				
		}
		$pView->AddRecords($aBooks, $aColSettings);
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all')
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		// przepisanie langa - info o braku rekordow
		$aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_books_to_show'];
		return $pView->Show();
	} // end of GetUsersList() function
	
	

	function &GetBooksToDiscount($iCategoryId=0,$sTitle='',$sAuthor='',$iYear=0,$iPublisher=0,$iSeries=0,$fPriceFrom=0,$fPriceTo=0){
		global $aConfig;
		$aBooks=array();
		$sWhere='';
		if($iCategoryId>0){
			$sWhere.=' AND (A.page_id = '.$iCategoryId.' OR B.page_id = '.$iCategoryId.')';
		}
		if(!empty($iPublisher)){
			$sWhere.=' AND H.name LIKE \''.$iPublisher.'\'';
		}
		if($iSeries>0){
			$sWhere.=' AND D.series_id = '.$iSeries;
		}
		if(!empty($sTitle)){
			$sWhere.=" AND A.name LIKE '%".$sTitle."%'";
		}
		if(!empty($sAuthor)){
			$sWhere.=" AND CONCAT(G.name,' ',G.surname,' ',G.name) LIKE '%".$sAuthor."%'";
		}
		if($iYear>0){
			$sWhere.=' AND A.publication_year = '.$iYear;
		}
		
		if ($fPriceFrom > 0 && $fPriceTo > 0 && $fPriceTo > $fPriceFrom) {
			$sWhere.= " AND A.price_brutto >= ".$fPriceFrom." AND A.price_brutto <= ".$fPriceTo;
		}
		
		if(!empty($sWhere)){
			$sSQL="SELECT A.id, A.name, A.price_brutto, H.name AS publisher, A.publication_year
						 FROM ".$aConfig['tabls']['prefix']."products A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						 ON B.product_id=A.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series D
						 ON D.product_id=A.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_authors F
						 ON F.product_id=A.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_authors G
						 ON F.author_id=G.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers H
						 ON A.publisher_id=H.id
						 WHERE A.prod_status = '1'
						 ".$sWhere."
						 GROUP BY A.id
						 ORDER BY A.id";
			$aBooks =& Common::GetAll($sSQL);
				
			foreach ($aBooks as $iKey => $aBook) {
				$aBooks[$iKey]['price_brutto'] = Common::formatPrice($aBook['price_brutto']);
			}
		}
		return $aBooks;
	}
	
	
	function AddBooksForm($pSmarty, $iPId, $bSearch=false) {
		global $aConfig;
		
		$aPromo = $this->getPromotion($iPId);
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = $aConfig['lang'][$this->sModule]['header_add_books'];
			$sHeader .= ' "'.$aPromo['name'].'"';
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		// = new FormTable('books_discount', '', array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		//$pForm->AddHidden('do', 'add_to_promo');
		
		$pForm = new FormTable('books_discount_search', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'add_to_promo');
		
		$pForm->AddText('discount',$aConfig['lang'][$this->sModule]['discount_stake'],$aData['s_product_name'],array('style'=>'width:50px;', 'maxlength'=>10),'','text',false);
		
			
			$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'books', 'pid'=>$iPId)).'\');'), 'button'));

		$sJS = '
		<script type="text/javascript">
		
		$(document).ready(function(){
      $("#publisher_aci").autocomplete({
        source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetPublishers.php"
      });
			$("#publisher_aci").bind("blur", function(e){
     	 $.get("ajax/GetSeries.php", { publisher: e.target.value },
					  function(data){
					    var brokenstring=data.split(";");
					    var elSelSeries = document.getElementById(\'series_aci\');
					    elSelSeries.length = 0;
					    elSelSeries.options[elSelSeries.length] =  new Option("'.$aConfig['lang']['common']['choose'].'","0");
					    for (x in brokenstring){
								if(brokenstring[x]){
									var item=brokenstring[x].split("|");
		  						elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
	  						}
	  					}
					 });
    	});
		});
		</script>
		';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	}
			
	
	/**
	 * Metoda usuwa produkt z promocji
	 * 
	 * @param	integer	$iId	- Id produktu do usuniecia z promocji
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;

			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_free_delivery
						 	 WHERE id = ".$iId;
		return (Common::Query($sSql) != false);
	} // end of deleteItem() method
	
	
	/**
	 * Metoda usuwa produkt z promocji
	 * 
	 * @param	integer	$iId	- Id produktu do usuniecia z promocji
	 * @return	mixed	
	 */
	function deletePromotionItem($iPId, $iPFDId) {
		global $aConfig;

		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_free_delivery_items
						 	 WHERE product_id = ".$iPId." AND products_free_delivery_id = ".$iPFDId;
		return (Common::Query($sSql) != false);
	} // end of deletePromotionItem() method

		function &getProductPublishers(){
		global $aConfig;
		// pobranie wszystkich serii
			$sSql = "SELECT id as value, name as label
							 FROM ".$aConfig['tabls']['prefix']."products_publishers
							 ORDER BY name";
			$aItems= Common::GetAll($sSql);
			return array_merge(array(
		array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])
		),$aItems);
	}
	
	/**
	 * Metoda pobiera dane produktu
	 *
	 * @param	integer	$iId	- Id produktu
	 * @return	array ref	- lista rekordow
	 */
	function &getProductsNames($sPIds) {
		global $aConfig;

		// pobranie wszystkich promocji
		$sSql = "SELECT GROUP_CONCAT(DISTINCT A.name SEPARATOR '\", \"')
						 FROM ".$aConfig['tabls']['prefix']."products A
						 WHERE A.id IN (".$sPIds.")";
		return Common::GetOne($sSql);
	} // end of getProductData() method
	
	
	function &getPromotion($iId) {
		global $aConfig;
		
		$sSql = "SELECT *,DATE_FORMAT(start_date, '%d-%m-%Y %H:%i')
											AS start_date,
											DATE_FORMAT(end_date, '%d-%m-%Y %H:%i')
											AS end_date
						FROM ".$aConfig['tabls']['prefix']."products_free_delivery
						WHERE id = ".$iId;
		return Common::GetRow($sSql);	
	}
	
	/**
	 * Metoda pobiera nazwę promocji
	 *
	 * @global type $aConfig
	 * @param type $iId
	 * @return type 
	 */
	function &getPromotionName($iId) {
		global $aConfig;
		
		$sSql = "SELECT name
						FROM ".$aConfig['tabls']['prefix']."products_free_delivery
						WHERE id = ".$iId;
		return Common::GetOne($sSql);	
	}// end of getPromotionName() method
	
	
		/**
	 * Metoda pobiera liste rekordow
	 *
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($ifpid) {
		global $aConfig;

		// pobranie wszystkich nowosci
		$sSql = "SELECT A.id, A.name
						 FROM ".$aConfig['tabls']['prefix']."products_free_delivery A
						 ORDER BY A.id ASC";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
	
	/**
	 * Metoda sprawdza status produktu, Nie akceptowane produkty nieopublikowane, niedostepne, nakład wyczerpany.
	 *
	 * @param type $iProdId 
	 */
	function checkProductStatus($iProdId) {
		global $aConfig;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id=".$iProdId." AND published='1' AND prod_status<>'2'  AND prod_status<>'0' ";
		return Common::GetOne($sSql)>0;
	}// end of checkProductStatus() method
	

	/**
	 * Metoda zwraca nazwe produktu o podanym id
	 */
	function getItemName($iId) {
		global $aConfig;

		$sSql = "SELECT name FROM " . $aConfig['tabls']['prefix'] . "products
	 						WHERE id = " . $iId . "";

		return Common::GetOne($sSql);
	}
} // end of Module Class
?>