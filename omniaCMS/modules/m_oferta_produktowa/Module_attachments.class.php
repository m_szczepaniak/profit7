<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - zalaczniki produktow
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iPPId = 0;
		$iPId = 0;
		$iId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['ppid'])) {
			$iPPId = intval($_GET['ppid']);
		}
		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
		if ($_SESSION['user']['type'] === 0) {
			if (!hasModulePrivileges($_GET['module_id'])){
				showPrivsAlert($pSmarty);
				return;
			}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iPPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId, $iPPId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iPPId, $iId); break;
			case 'add':
			case 'edit': $this->AddEdit($pSmarty, $iPId, $iPPId, $iId); break;
			
			default: $this->Show($pSmarty, $iPId, $iPPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych autorow produktu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id kategorii
	 * @param	integer	$iId	- Id produktu
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header' => sprintf($aConfig['lang'][$this->sModule]['list'], trimString($this->getItemName($iId), 50), getPagePath($iPId, $this->iLangId, true, false, false)),
			'refresh' => true,
			'action' => phpSelf(array('ppid' => $iId)),
			'refresh_link' => phpSelf(array('ppid' => $iId, 'reset' => '1')),
			'search' => true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'price_brutto',
				'content'	=> $aConfig['lang'][$this->sModule]['price_brutto'],
				'sortable'	=> true,
				'width'	=> '80'
			),
			array(
				'db_field'	=> 'vat',
				'content'	=> $aConfig['lang'][$this->sModule]['vat'],
				'sortable'	=> true,
				'width'	=> '80'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_attachments
						 WHERE product_id = ".$iId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
						 	 FROM ".$aConfig['tabls']['prefix']."products_attachments
						 	 WHERE product_id = ".$iId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('product_attachments', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// pobranie wszystkich
			$sSql = "SELECT id, name, price_brutto, vat
							 FROM ".$aConfig['tabls']['prefix']."products_attachments
							 WHERE product_id = ".$iId.
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '');
			$aRecords =& Common::GetAll($sSql);
			
			foreach ($aRecords as $iKey => $aItem) {
				// formatowanie ceny netto
				$aRecords[$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']);

			}
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('add')
		);
		// konfiguracja przyciskow stopki
		$aRecordsFooterParams = array(
			'go_back' => array(array(), array('action', 'pid', 'ppid'))
		);

		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane zalaczniki produktu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iPPId	- Id produktu
	 * @param	integer	$iId	- id zalacznika
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iPPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['name'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['name'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				SetModification('products',$iPPId);
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel, $this->getItemName($iPPId), getPagePath($iPId, $this->iLangId, true, false, false));
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel, $this->getItemName($iPPId), getPagePath($iPId, $this->iLangId, true, false, false));
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId, $iPPId);
	} // end of Delete() method


	/**
	 * Metoda dodaje zalacznik do produktu
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId, $iPPId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId, $iPPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iPPId);
			return;
		}
		// dodanie zalacznika
		$bIsErr = !$this->addAttachment($iPPId);
		
		if (!$bIsErr) {
			// dodano
			SetModification('products',$iPPId);
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
												$_POST['name'],
												getPagePath($iPId, $this->iLangId, true, false, false),
												$this->getItemName($iPPId));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty, $iPId, $iPPId);
		}
		else {
			// blad
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$_POST['name'],
											getPagePath($iPId, $this->iLangId, true, false, false),
											$this->getItemName($iPPId));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId, $iPPId);
		}
	} // end of Insert() funciton
	
	
	/**
	 * Metoda aktualizuje zalacznik produktu
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iPPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId, $iPPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iPPId, $iId);
			return;
		}
		// aktualizacja zalacznika
		$bIsErr = !$this->updateAttachment($iId);
		
		if (!$bIsErr) {
			// zaktualizowano
			SetModification('products',$iPPId);
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
												$_POST['name'],
												getPagePath($iPId, $this->iLangId, true, false, false),
												$this->getItemName($iPPId));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId, $iPPId);
		}
		else {
			// blad
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$_POST['name'],
											getPagePath($iPId, $this->iLangId, true, false, false),
											$this->getItemName($iPPId));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId, $iPPId, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz wyboru dodawania autora produktu
	 *
	 * @param	object	$pSmarty
	 */
	function AddEdit(&$pSmarty, $iPId, $iPPId, $iId=0) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
		if ($iId > 0) {
			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."products_attachments
							 WHERE id = ".$iId." AND
							 			 product_id = ".$iPPId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)], trimString($this->getItemName($iPPId), 50));
				
		$pForm = new FormTable('product_attachments', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
		
		// dla strony
		$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], getPagePath($iPId, $this->iLangId, true), '', array(), array('class'=>'redText'));
		
		// nazwa
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>500, 'style'=>'width: 450px;'), '', 'text');
		
		// cena brutto
		$pForm->AddText('price_brutto', $aConfig['lang'][$this->sModule]['price_brutto'], isset($aData['price_brutto']) && $aData['price_brutto'] != '' ? Common::formatPrice($aData['price_brutto']) : '', array('maxlength'=>12, 'style'=>'width: 75px;'), '', 'ufloat');
		
		// vat
		$pForm->AddText('vat', $aConfig['lang'][$this->sModule]['vat'], $aData['vat'], array('maxlength'=>50, 'style'=>'width: 150px;'), '', 'uinteger');
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda dodaje do bazy zalacznikdo produktu
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @return	integer
	 */
	function addAttachment($iId) {
		global $aConfig;
		
		$aValues = array(
			'product_id' => $iId,
			'name' => $_POST['name'],
			'price_brutto' => Common::formatPrice2($_POST['price_brutto']),
			'vat' => (int)$_POST['vat']
		);
		return Common::Insert($aConfig['tabls']['prefix']."products_attachments",
											 	 	$aValues,
												  "",
											 		false) !== false;
	} // end of addAttachment() method
	
	
	/**
	 * Metoda aktualizuje zalacznik produktu
	 * 
	 * @param	integer	$iId	- Id zalacznika
	 * @return	integer
	 */
	function updateAttachment($iId) {
		global $aConfig;
		
		$aValues = array(
			'name' => $_POST['name'],
			'price_brutto' => Common::formatPrice2($_POST['price_brutto']),
			'vat' => (int)$_POST['vat']
		);
		return Common::Update($aConfig['tabls']['prefix']."products_attachments",
											 	 	$aValues,
												  "id = ".$iId) !== false;
	} // end of updateAttachment() method


	/**
	 * Metoda pobiera liste zalacznikow produktu do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id zalacznikow
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, name FROM ".$aConfig['tabls']['prefix']."products_attachments WHERE id IN  (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa zalacznik produktu
	 * 
	 * @param	integer	$iId	- Id zalacznika do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_attachments WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method
	
} // end of Module Class
?>