<?php

namespace omniaCMS\modules\m_oferta_produktowa;

use Admin;
use Common;
use FormTable;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 17.10.16
 * Time: 15:33
 */
class Module__oferta_produktowa__categories_types_mapping implements SingleView, ModuleEntity
{

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    /**
     * @var string
     */
    private $sModule;

    /**
     *
     * @var Module
     */
    private $oModuleZamowienia;

    function __construct(Admin $oClassRepository)
    {
        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function getMsg()
    {
        return $this->sMsg;
    }

    public function doDefault()
    {
        global $aConfig;

        $categories = $this->getCategoriesGames();
        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('sort_categories', _('Mapowanie głównych kategorii do typów produktów'), array('action' => phpSelf()), array('col_width' => 150), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', 'update');
        $aProductTypes = $this->getProductTypes();
        unset($aProductTypes[0]);
        $selectedTypes = $this->getSelectedTypes();

        foreach ($categories as $category) {
            $pForm->AddRow('Kategoria', $category['parent'] . ' / ' . $category['name']);
            $pForm->AddRadioSet('product_type[' . $category['id'] . ']', ' Typ: ', $aProductTypes, $selectedTypes[$category['id']]);
        }
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz')));
        return $pForm->ShowForm();
    }

    public function getSelectedTypes()
    {
        $sSql = 'SELECT menus_items_id, product_type FROM products_types_mapping';
        return $this->pDbMgr->GetAssoc('profit24', $sSql);
    }

    public function doUpdate()
    {
        global $aConfig;
        $sSql = 'DELETE FROM products_types_mapping WHERE 1=1';
        $this->pDbMgr->Query('profit24', $sSql);
        $this->pDbMgr->BeginTransaction('profit24');

        foreach ($_POST['product_type'] as $menusItemsId => $productType) {
            $aValues = [
                'menus_items_id' => $menusItemsId,
                'product_type' => $productType,
                'created' => 'NOW()',
                'created_by' => $_SESSION['user']['name']
            ];
            if ($this->pDbMgr->Insert('profit24', 'products_types_mapping', $aValues) === false) {
                $this->pDbMgr->RollbackTransaction('profit24');
                $sMsg = _('Wystąpił błąd podczas dodawania mapowania');
                $this->sMsg = GetMessage($sMsg);
                return $this->doDefault();
            }
        }
        $this->pDbMgr->CommitTransaction('profit24');
        $sMsg = _('Zaktualizowano mapowania');
        $this->sMsg = GetMessage($sMsg, false);
        return $this->doDefault();

    }

    private function getProductTypes()
    {
        $aPTypes = \LIB\EntityManager\Entites\Product::getProductTypes();
        $aPTypesList = $this->mapKeyValuToList($aPTypes, true);
        return $aPTypesList;
    }

    /**
     * @param $aPTypes
     * @return array
     */
    private function mapKeyValuToList($aPTypes, $pType = false)
    {

        $aPTypesList = [];
        $i = 0;
        foreach ($aPTypes as $name => $symbol) {
            $aPTypesList[$i] = [
                'label' => $name,
                'value' => $symbol,
            ];
            $i++;
        }
        return $aPTypesList;
    }


    /**
     * @return mixed
     */
    private function getCategoriesGames()
    {
        global $aConfig;

        $sSql1 = "SELECT id
					 FROM " . $aConfig['tabls']['prefix'] . "menus_items
					 WHERE module_id = " . getModuleId('m_oferta_produktowa') . "
					 AND mtype = '0'
					 AND parent_id IS NULL
					 AND published = '1'";

        $sSql2 = "SELECT MI.id, MI.name, MI2.name AS parent, MENUS.game
                     FROM " . $aConfig['tabls']['prefix'] . "menus_items AS MI
                     JOIN menus as MENUS ON MI.menu_id = MENUS.id AND MENUS.product_type <> 'K'
                     JOIN menus_items as MI2 ON MI.parent_id = MI2.id
					 WHERE MI.module_id = " . getModuleId('m_oferta_produktowa') . "
					 AND MI.mtype = '0'
					 AND MI.parent_id IN (" . implode(',', Common::GetCol($sSql1)) . ")
					 ORDER BY MENUS.game, MI.priority";

        return Common::GetAll($sSql2);
    }

    public function parseViewItem(array $aItem)
    {
        return $aItem;
    }

    public function resetViewState()
    {
        resetViewState();
    }
}