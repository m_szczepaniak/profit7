<?php
use table\keyValue;

/**
 * Klasa Common_m_oferta_produktowa do obslugi modulu i jego boksow oferty produktowej
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Common_m_oferta_produktowa {

    public $markUpCookie;

  /**
   * Konstruktor klasy
   */
	function Common_m_oferta_produktowa() {

	} // end Common_m_oferta_produktowa() function
	
	
	  

	 /**
	  * Metoda pobiera autorów produktu o podanym id
	  */
	  function getAuthors($iId, $iLimitAuthors=0){
	  	global $aConfig;
	  	$sHtml='';
	  	// wybiera grupy kategorii dla tego produktu
	  	$sSql = "SELECT B.id as authorid, CONCAT('<b>',B.surname,'</b> ',B.name) AS name, C.name AS role
	  					 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
	  					 JOIN ".$aConfig['tabls']['prefix']."products_authors B
	  					 ON A.author_id=B.id
	  					 JOIN ".$aConfig['tabls']['prefix']."products_authors_roles C
	  					 ON A.role_id=C.id
	  					 WHERE A.product_id =".$iId."
	  					ORDER BY C.order_by ASC, A.id ASC, A.order_by ASC ".
							( $iLimitAuthors > 0 ? " LIMIT ".$iLimitAuthors : '');
	  	$aItems =& Common::GetAll($sSql);
	  	foreach ($aItems as $iKey => $aItem) {
				// utworzenie linku do rekordu
				$aAuthors[$aItem['role']][$iKey]['name'] = $aItem['name'];
				$aAuthors[$aItem['role']][$iKey]['link'] = getAuthorLink($aItem['name']);
			}
	  	return $aAuthors;
	  }

		
	/**
	 * Metoda pobiera okladki produktu o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @param	intger	$bThumb	- mala miniaturka
	 * @return	array	- okladki produktu
	 */
	final function getImages($iId, $bThumb = false){
		global $aConfig, $pSmarty;
		
		// pobranie z bazy okladek
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."products_images
						WHERE product_id = ".$iId."";
						
		$aResult =& Common::GetAll($sSql);
		$aImages=array();
		foreach ($aResult as $iKey => $aValue){
			if ($bThumb){
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'])){
        $aImages[$iKey]['thumb'] = '/images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'];	
				}
			} else {
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/__b_'.$aValue['photo'])){
        $aImages[$iKey]['big'] = '/images/photos/'.$aValue['directory'].'/__b_'.$aValue['photo'];
				}
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/'.$aValue['photo'])){
        $aImages[$iKey]['small'] = '/images/photos/'.$aValue['directory'].'/'.$aValue['photo'];
				}
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'])){
        $aImages[$iKey]['thumb'] = '/images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'];
				}
			}
			$aImages[$iKey]['description']=$aValue['description'];
		}
		return $aImages;
	}
		
	/**
	 * Metoda pobiera okladki produktu o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @param	intger	$bThumb	- mala miniaturka
	 * @return	array	- okladki produktu
	function getImages($iId, $bThumb = false){
		global $aConfig, $pSmarty;
		
		// pobranie z bazy okladek
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."products_images
						WHERE product_id = ".$iId."";
						
		$aResult =& Common::GetAll($sSql);
		
		foreach ($aResult as $iKey => $aValue){
			if ($bThumb){
				$aImages[$iKey]['thumb'] = '/images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'];	
			} else {
				$aImages[$iKey]['big'] = '/images/photos/'.$aValue['directory'].'/__b_'.$aValue['photo'];
				$aImages[$iKey]['small'] = '/images/photos/'.$aValue['directory'].'/'.$aValue['photo'];
				$aImages[$iKey]['thumb'] = '/images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'];
			}
		}

		return $aImages;
	}
	*/
	
	
	/**
	 * Metoda pobiera nazwe strony oferty produktowej
	 * 
	 * @param	integer	$iId	- Id strony
	 * @return	string - nazwa strony
	 */
	function getName($iId){
		global $aConfig;
		
		$sSql = "SELECT name
						FROM ".$aConfig['tabls']['prefix']."menus_items
						WHERE id = ".$iId."";
						
		return Common::GetOne($sSql);
	}
	
	/**
	 * Metoda na podstawie filtrow oraz zmienych sortowania tworzy link
	 * Jezeli $ForPager true i istnieje $_GET['sorder'] nastepuje odwrocenie
	 * kolejnosci sortowania, np. z ASC na DESC
	 * 
	 * @param	bool	$bSorter	- true: dodaj sorter; false: bez sortera
	 * @return	string	- link
	 */
	function getLink($bSorter=false) {
		$sLink = '';
		if ($bSorter) {
			if (isset($_GET['sort'])) {
				$sLink .= 'sort:'.$_GET['sort'].',';
			}
			if (isset($_GET['filter'])) {
				$sLink .= 'filter:'.$_GET['filter'].',';
			}
		}
		return $sLink;
	} // end of getLink() method


    /**
     * @param $sPageLink
     * @return string
     */
    public function getBaseLink($sPageLink) {
        global $aConfig;

        $baseLink = '';
        if (isset($_GET['action'])) {
            preg_match('/^p(\d+)$/', $_GET['action'], $matches);
        }
        if (mb_substr($sPageLink, 0, 1, 'UTF-8') != '/') {
            $sPageLink = '/'.$sPageLink;
        }

        if (isset($_GET['p']) && intval($_GET['p']) > 1) {
            $p = intval($_GET['p']);
            $baseLink = $aConfig['common']['client_base_url_https_no_slash'] . $sPageLink . '/p'.$p.'.html';
        }
        elseif (isset($matches[1]) && intval($matches[1]) > 1) {
            $p = intval($matches[1]);
            $baseLink = $aConfig['common']['client_base_url_https_no_slash'] . $sPageLink . '/p'.$p.'.html';
        } else {
            $baseLink = $aConfig['common']['client_base_url_https_no_slash'] . $sPageLink;
        }
        return $baseLink;
    }

    /**
     * @param $sPageLink
     * @return string
     */
    public function getBaseLinkPromotion($sPageLink, $sLinkPage) {
        global $aConfig;

        $baseLink = '';
        if (isset($_GET['p']) && intval($_GET['p']) > 1) {
            $p = intval($_GET['p']);
            $baseLink = $aConfig['common']['client_base_url_https_no_slash'] . $sPageLink.'/'.str_replace('%d', $p, $sLinkPage);
        } else {
            $baseLink = $aConfig['common']['client_base_url_https_no_slash'] . $sPageLink.'/'.str_replace(',p%d', '', $sLinkPage);
        }
        return $baseLink;
    }
	
	/**
	 * Metoda formatuje czesc zapytania SQL odpowiedzialnej za sortowanie
	 * 
	 * @return	string	- SQL sortowania
	 */
	function getSorterSql($sDefaultSortPoolSQL = '') {


        $sSql = ' ORDER BY ';

        if (isset($_GET['sort'])) {
            if ($_GET['sort'] == "cena") {
                $sSql .= 'price_brutto ASC';
            } elseif ($_GET['sort'] == "cena_desc") {
                $sSql .= 'price_brutto DESC';
            } elseif ($_GET['sort'] == "nazwa") {
                $sSql .= 'A.title ASC';
            } elseif ($_GET['sort'] == "nazwa_desc") {
                $sSql .= 'A.title DESC';
            } elseif ($_GET['sort'] == "data") {
                //$sSql .= 'A.id DESC, A.shipment_time ASC';
                $sSql .= 'A.order_by DESC';
            } elseif ($_GET['sort'] == 'order_by_status_old_style') {
                $sSql .= 'A.order_by_status_old_style DESC';
            }
            else {
                $sSql .= 'A.title ASC';
            }
        } else {
            //$sSql .= 'A.id DESC, A.shipment_time ASC';
            //$sSql .= 'A.is_news DESC, A.publication_year DESC, A.shipment_time ASC';
            //$sSql .= 'A.id DESC, A.shipment_time ASC';
            //$sSql .= 'A.is_news DESC, A.publication_year DESC, A.shipment_time ASC';
            //$sSql .= 'A.is_news DESC, A.shipment_time ASC, A.publication_year DESC';
            //$sSql .= 'A.shipment_time ASC, A.is_news DESC, A.publication_year DESC';
            if ($sDefaultSortPoolSQL != '') {
                $sSql .= $sDefaultSortPoolSQL;
            } else {
                $sSql .= 'A.order_by DESC';
            }
        }
		return $sSql.'';
	} // end of getSorterSql() method
	
	/**
	 * Metoda formatuje czesc zapytania SQL odpowiedzialnej za filtrowanie
	 * 
	 * @return	string	- SQL sortowania
	 */
	function getFilterSql() {
		$sSql = '';
		
		if(isset($_GET['filter'])){
			if($_GET['filter']=="all"){
				$sSql .= '';			
			} else {
				$sSql .= " AND (A.prod_status = '1' OR A.prod_status = '3')";
			}
		}
		else 
			$sSql .= " AND (A.prod_status = '1' OR A.prod_status = '3')";
		return $sSql;
	} // end of getFilterSql() method
	
	
	/**
	 * Metoda formatuje czesc zapytania SQL odpowiedzialnej za sortowanie w wyszukiwarce
	 * 
	 * @return	string	- SQL sortowania
	 */
	function getSearchSorterSql() {
			
	
		$sSql = ' ORDER BY ';
		
		if(isset($_GET['sort'])){
			if($_GET['sort']=="cena"){
				$sSql .= 'price ASC';			
			} elseif ($_GET['sort']=="cena_desc"){
				$sSql .= 'price DESC';	
			} elseif ($_GET['sort']=="nazwa"){
				$sSql .= 'title ASC';	
			} elseif ($_GET['sort']=="nazwa_desc"){
				$sSql .= 'title DESC';	
			} elseif ($_GET['sort']=="data"){
				//$sSql .= 'A.id DESC, A.shipment_time ASC';
				$sSql .= 'A.order_by DESC';
            } elseif ($_GET['sort'] == 'order_by_status_old_style') {
                $sSql .= 'A.order_by_status_old_style DESC';
			} else {
				$sSql .= 'title ASC';
			}
			}
		else 
			//$sSql .= 'A.id DESC, A.shipment_time ASC';
			//$sSql .= 'A.is_news DESC, A.publication_year DESC, A.shipment_time ASC';
			//$sSql .= 'A.is_news DESC, A.shipment_time ASC, A.publication_year DESC';
			//$sSql .= 'A.shipment_time ASC, A.is_news DESC, A.publication_year DESC';
			$sSql .= 'A.order_by DESC';

		return $sSql.'';
	} // end of getSorterSql() method


    /**
     * @return \orders\MarkUpCookie
     */
    private function getMarkUpCookeInstance() {
        if (!isset($this->markUpCookie)) {
            $this->markUpCookie = new \orders\MarkUpCookie();
        }

        return $this->markUpCookie;
    }

	/**
	 * Metoda oblicza czenę promocyjną
	 * @param $fPriceBrutto - cena brutto produktu
	 * @param $aTarrif - tablica z cennikiem
	 * @param $sPriceLang - referencja do stringa do ktorego zostanie zwrócony lang z typem ceny
	 * @return float - cena promocyjna
	 */
	function getPromoPrice($fPriceBrutto, $aTarrif, &$sPriceLang, $fPublisherLimit=0, $fSourceLimit){

		global $aConfig;
		$sPromoText='';
		$fPromoPrice=0;
        $markUpCookie = $this->getMarkUpCookeInstance();

		/*
		if (!isset($fSourceLimit)) {
			dump(debug_backtrace(false));
			dump('Mamy błąd');
			die;
		}
		*/
		
		if($aTarrif['price_brutto'] != $fPriceBrutto){
			$fPromoPrice=$aTarrif['price_brutto'];
			$sPromoText=$aConfig['lang']['common']['cena_w_ksiegarni'];
		}
		if(isLoggedIn() && ($_SESSION['w_user']['discounts'] > 0) && !$aTarrif['packet']){
			$iDiscount = ($fPublisherLimit > 0 && $_SESSION['w_user']['discounts'] > $fPublisherLimit)?$fPublisherLimit:$_SESSION['w_user']['discounts'];
			$iDiscount = ($fSourceLimit > 0 && $iDiscount > $fSourceLimit) ? $fSourceLimit : $iDiscount;
			$fUserPrice = $fPriceBrutto - $fPriceBrutto * ($iDiscount / 100);
			if($fPromoPrice != 0){
				if($fUserPrice < $fPromoPrice){
					$fPromoPrice=$fUserPrice;
					$sPromoText=$aConfig['lang']['common']['cena_uzytkownika'];
				}
			} else {
				$fPromoPrice=$fUserPrice;
				$sPromoText=$aConfig['lang']['common']['cena_uzytkownika'];
			}
		}

        $aResultTarrif = $markUpCookie->getMarkup($fPriceBrutto, $aTarrif, $iDiscount);
        if ($markUpCookie->isMarkUpActive()) {
            $fPromoPrice = $aResultTarrif['price_brutto'];
            $aTarrif = $aResultTarrif;
        }

		$sPriceLang=$sPromoText;
		return $fPromoPrice;
	}


    /**
     * Metoda zwraca aktualny cennik dla produktu
     * @param $iId - id produktu
     * @param string $sWebsite
     * @return array cennik
     */
	function &getTarrif($iId, $sWebsite = 'profit24') {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT promotion_id, discount, price_brutto, price_netto, discount_value, product_id
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1";
		return $pDbMgr->GetRow($sWebsite, $sSql);
	} // end of getTarrif()
	
	/**
	 * Metoda zwraca informacje o wydawcy
	 * @param $iId - id wydawcy
	 * @return array (nazwa, logo)
	 */
	function getPublisher($iId) {
		global $aConfig;
		$sSql = "SELECT name, discount_limit, photo AS logo
						 FROM ".$aConfig['tabls']['prefix']."products_publishers
						 WHERE id = ".$iId;
		return Common::GetRow($sSql);
	} // end of getPublisher()


	/**
	 * Metoda pobiera limit dla danego pierwszego źródła
	 *
	 * @param type $iProdId 
	 */
	function getSourceDiscountLimit($iProdId) {
		global $aConfig;
		
		if ($iProdId > 0) {
			$sSql = "SELECT B.discount_limit 
							 FROM ".$aConfig['tabls']['prefix']."products AS A
							 JOIN ".$aConfig['tabls']['prefix']."products_source_discount AS B
								 ON A.created_by=B.first_source
							 WHERE A.id=".$iProdId;
			$fRet = Common::GetOne($sSql);
		} else {
			//dump(debug_backtrace(false));
		}
		if (!isset($fRet) || is_null($fRet)) {
			$fRet = 0.00;
		}
		return $fRet;
	}// end of getSourceDiscountLimit() method
} // end of Common_m_oferta_produktowa Class
?>