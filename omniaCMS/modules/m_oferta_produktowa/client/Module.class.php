<?php
/**
 * Klasa odule do obslugi oferty produktowej - lista produktow z kategorii
 * karta produktu
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
use LIB\Assets\additionalHead;

include_once('modules/m_oferta_produktowa/client/Common.class.php');
class Module extends Common_m_oferta_produktowa  {
    
  // Id wersji jezykowej
  var $iLangId;
  
  // symbol wersji jezykowej
  var $sLang;
  
  // Id strony
  var $iPageId;
  
  // szablon strony
  var $sTemplate;
  
  // symbol modulu
  var $sModule;
  
  // ustawienia strony aktualnosci
  var $aSettings;
    
  // link do strony
  var $sPageLink;
  
    // czesc nazwy pliku cache - na podstawie sciezki URI
  var $sCacheName;
  
  // czy cache'owac czy tez nie
  var $bDoCache;
	
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module($sSymbol, $iPageId, $sName) {
		global $aConfig;

		$this->iLangId =& $_GET['lang_id'];
		$this->sLang =& $_GET['lang'];
		$this->iPageId = $iPageId;
		$this->sName = $sName;
		$this->sModule = $sSymbol;
		$this->sCacheName = md5($_SERVER['REQUEST_URI']);
		$this->bDoCache = FALSE;//$aConfig['common']['caching'];
		$this->aSettings =& $this->getSettings();
		$this->sPageLink .= $aConfig['_tmp']['page']['symbol'];
	} // end Module() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla strony produktow
	 * 
	 * @return	array
	 */
	function getSettings() {
		global $aConfig, $oCache;
		
		if (!$this->bDoCache || !is_array($aCfg = unserialize($oCache->get(md5($aConfig['_tmp']['page']['symbol']), 'modules_settings')))) {
				// wylaczone cache'owanie lub danych nie ma w cache'u
			$sSql = "SELECT thumb_size, small_size, big_size,list_template,item_template,items_per_page
						 FROM ".$aConfig['tabls']['prefix']."products_config 
						 WHERE id = ".getModuleId('m_oferta_produktowa');
			$aCfg = Common::GetRow($sSql);
	
			if (!isset($aCfg['items_per_page']) || $aCfg['items_per_page'] < 0) {
				$aCfg['items_per_page'] = $aConfig['default']['per_page'];
			}
			// zapis do cache'a
				if ($this->bDoCache)
					$oCache->save(serialize($aCfg), md5($aConfig['_tmp']['page']['symbol']), 'modules_settings');
		}
		return $aCfg;
	} // end of getSettings() function
	
	
	/**
	 * Metoda w zaleznosci od tego czy uzytkowik jest na stronie kategorii
	 * czy karcie produktu pobiera i wyswietla liste produktow z danej
	 * kategorii lub karte produktu
	 */
	function getContent() {
		global $aConfig, $pSmarty, $oCache;
		$sHtml = '';	
		
		// link do strony
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['page_link'] = $this->sPageLink;
		$aModule['base_url'] =& $aConfig['common']['base_url_http_no_slash'];
		// przegladanie produktow
		if (isset($_GET['id'])) {	
			
			if ($this->itemExists($_GET['id'])) {
        switch ($_POST['do']) {
            case 'add_notif_available':
              if (isset($_POST['p_mail'])) {
                // dodanie maila uzytkownika do powiadomien
                $this->_addToNotificationsAvailable(trim($_POST['p_mail']), trim($_POST['p_captcha']), $_GET['id']);
                // przekeirwoanie z powrotem na strone produktu
                doRedirect($_SERVER['REQUEST_URI']);
              }
            break;
            case 'add_notif':
              if (isset($_POST['p_mail'])) {
                // dodanie maila uzytkownika do powiadomien
                $this->addToNotifications(trim($_POST['p_text']), trim($_POST['p_mail']), $_POST['p_title'], trim($_POST['p_captcha']), $_GET['id']);
                // przekeirwoanie z powrotem na strone produktu
                doRedirect($_SERVER['REQUEST_URI']);
              }
            break;
        }

                if (isset($_SESSION['w_user']['id'])) {
                    $orders = $this->getUserOrdersForProduct($_GET['id'], $_SESSION['w_user']['id']);
                    $aModule['ordersCount'] = count($orders);

                    foreach($orders as $id => $order)
                    {
                        $orders[$id]['link'] = createLink($aConfig['_tmp']['accounts_symbol'], 'id'.$order['order_number'],'orders');
                    }
                    $aModule['orders'] = $orders;
                }

				// wybrano wyswietlenie karty produktu
				if (!$this->isPacket($_GET['id'])) {
					// pobranie produktu	
					$aModule['item'] = $this->getProduct($_GET['id'],false);
					// tytul strony
					$sTitleStr = $this->getAuthorsString($aModule['item']['authors']);
					setTitle($aModule['item']['plain_name'].(!empty($aModule['item']['name'])?' '.$aModule['item']['name2']:'').(!empty($sTitleStr)?' - '.$sTitleStr:''));
					
					// slowa kluczowe
					if (!empty($aModule['item']['tags'])) {
							setKeywords($this->implodeTags(', ', $aModule['item']['tags']));
					} else {
						setKeywords(preg_replace('/\s+/', ', ', $aModule['item']['plain_name']));
					}
					setPageHeader('');
					// opis
					if (!empty($aModule['item']['description'])) {
						setDescription(trimString(html2text($aModule['item']['description'], ' '), 400));
					}	else {
						setDescription($aModule['item']['plain_name'].(!empty($aModule['item']['name2'])?' '.$aModule['item']['name2']:''));
					}
					if ($aModule['item']['images'][0]['big'] != '') {
						$aConfig['_settings']['site']['fbog_image'] = $aConfig['common']['base_url_http_no_slash'].$aModule['item']['images'][0]['big'];
					} elseif ($aModule['item']['images'][0]['small'] != '') {
            $aConfig['_settings']['site']['fbog_image'] = $aConfig['common']['base_url_http_no_slash'].$aModule['item']['images'][0]['small'];
          }
                    $this->getCriteoJSProduct($_GET['id']);
					$pSmarty->assign_by_ref('aModule', $aModule);
					$sHtml = $pSmarty->fetch('modules/'.$this->sModule.'/'.$this->aSettings['item_template']);
					$pSmarty->clear_assign('aModule', $aModule);
				}
				else {
					// pakiet				
					$aModule['item'] = $this->getProduct($_GET['id'], true);
					//dump($aModule['item']);
					// tytul strony
					setTitle($aModule['item']['plain_name'].(!empty($aModule['item']['name2'])?' '.$aModule['item']['name2']:''));
					// slowa kluczowe
					if (!empty($aModule['item']['tags'])) {
						setKeywords($this->implodeTags(', ', $aModule['item']['tags']));
					} else {
						setKeywords(preg_replace('/\s+/', ', ', $aModule['item']['plain_name']));
					}
					setPageHeader('');
					// opis
					if (!empty($aModule['item']['description'])) {
						setDescription(trimString(html2text($aModule['item']['description'], ' '), 400));
					}	else {
						setDescription($aModule['item']['plain_name'].(!empty($aModule['item']['name2'])?' '.$aModule['item']['name2']:''));
					}
                    $this->getCriteoJSProduct($_GET['id']);
					$pSmarty->assign_by_ref('sProductThumb', $aModule['item']['images'][0]['small']);
					$pSmarty->assign_by_ref('aModule', $aModule);
					$sHtml = $pSmarty->fetch('modules/'.$this->sModule.'/pakiet_'.
																		 $this->aSettings['item_template']);
					$pSmarty->clear_assign('aModule', $aModule);
				}
			}
			else {
				doRedirect('/');
			}
		}
		else {
			if(count($aConfig['_tmp']['path'])==1) {
				if($this->iPageId == $aConfig['import']['unsorted_category']){
					doRedirect('/');
				}
				$aConfig['_tmp']['page']['template'] = 'index.tpl';
				return '';
			} else {
				if (!$this->bDoCache || false === ($sHtml = $oCache->get($this->sCacheName, 'modules'))) {
					// wylaczone cache'owanie lub danych nie ma w cache'u
					// pobranie listy produktow kategorii
          
          $aCatTmp = $aConfig['_tmp']['path'];
          arsort($aCatTmp);
          if(empty($aCatTmp) === false)
              array_pop($aCatTmp);
          $sTitle = '';
          $iCountAll = count($aCatTmp);
          if ($iCountAll > 3) {
            $iCountAll = 3;
            array_pop($aCatTmp);
          }
          foreach ($aCatTmp as $aCatData) {
            $sTitle .= html2text($aCatData['name'], ' ').' - ';
          }
          $sTitle = substr($sTitle, 0, -2);
					setTitle($sTitle);
					
					// sformatowanie czesci zapytania SQL odpowiedzialnej za sortowanie
					$sSorterSql = $this->getSorterSql();
					$sFilterSql = $this->getFilterSql();
					$aModule['link_to_sort'] = $this->getLink();

//                    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/boxes/m_seo/client/Box_modern.class.php');
//                    $aBox['module'] = 'm_seo';
//                    $aBox['template'] = 'domyslny.tpl';
//                    $box = new Box_m_seo_modern($aBox);
//                    $aModule['box_m_seo'] = $box->getContent();

//                    include_once($_SERVER['DOCUMENT_ROOT'].'omniaCMS/boxes/m_seo/client/Box.class.php');
//                    $aBox['module'] = 'm_seo';
//                    $aBox['template'] = 'domyslny.tpl';
//                    $box = new Box_m_seo($aBox);
//                    $aModule['box_m_seo'] = $box->getContent();

					// pobranie listy produktow
					$aModule['offer'] =& $this->getProductsList($this->sPageLink, $sFilterSql, $sSorterSql);
                    $this->getCriteoJS($aModule['offer']['items']);
					if (!empty($aModule['offer']['items'])) {
						$pSmarty->assign_by_ref('aModule', $aModule);
						$sHtml = $pSmarty->fetch('modules/'.$this->sModule.'/'.$this->aSettings['list_template']);
						$pSmarty->clear_assign('aModule', $aModule);
					} else {
					    doRedirect('', '410');
                    }
					// zapis do cache'a
					if ($this->bDoCache) {
						$oCache->save($sHtml, $this->sCacheName, 'modules');
					}
				}
				if ($sHtml == '') {
					setMessage($aModule['lang']['no_data']);
				}
			}
		}

		if (isset($aConfig['common']['buybox_commision_id'])) {
			$sHtml .= \BuyBox\ScriptGenerator::generateProductScript($_GET['id']);
		}

		return $sHtml;
	} // end of getContent()

    private function getCriteoJSProduct($product) {
        global $aConfig;

        $aConfig['header_js'][]="</script>";
        $aConfig['header_js'][]="<script type=\"text/javascript\">";
        $aConfig['header_js'][]="window.criteo_q = window.criteo_q || [];";
        $aConfig['header_js'][]="var deviceType = /iPad/.test(navigator.userAgent) ? \"t\" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? \"m\" : \"d\";";
        $aConfig['header_js'][]="window.criteo_q.push(";
        $aConfig['header_js'][]="{ event: \"setAccount\", account: 57447},";
        $aConfig['header_js'][]="{ event: \"setEmail\", email: \"" .( isset($_SESSION['w_user']['email'])?$_SESSION['w_user']['email']:"" ). "\" },";
        $aConfig['header_js'][]="{ event: \"setSiteType\", type: deviceType},";
        $aConfig['header_js'][]="{ event: \"viewItem\", item: \"" . $product . "\"});";
        $aConfig['header_js'][]="</script>";
        $aConfig['header_js'][]="<script type=\"text/javascript\">";
    }

    private function getCriteoJS($products) {
        global $aConfig;

        $ids = [];
        if ( empty($products) === false) {
            foreach ($products as $product) {
                $ids[] = $product['id'];
            }
        }

        $aConfig['header_js'][]="</script>";
        $aConfig['header_js'][]="<script type=\"text/javascript\">";
        $aConfig['header_js'][]="window.criteo_q = window.criteo_q || [];";
        $aConfig['header_js'][]="var deviceType = /iPad/.test(navigator.userAgent) ? \"t\" : /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Silk/.test(navigator.userAgent) ? \"m\" : \"d\";";
        $aConfig['header_js'][]="window.criteo_q.push(";
        $aConfig['header_js'][]="{ event: \"setAccount\", account: 57447},";
        $aConfig['header_js'][]="{ event: \"setEmail\", email: \"" .( isset($_SESSION['w_user']['email'])?$_SESSION['w_user']['email']:"" ). "\" },";
        $aConfig['header_js'][]="{ event: \"setSiteType\", type: deviceType},";
        $aConfig['header_js'][]="{ event: \"viewList\", item: [" . implode(',', $ids) . "]});";
        $aConfig['header_js'][]="</script>";
        $aConfig['header_js'][]="<script type=\"text/javascript\">";
    }

    private function getUserOrdersForProduct($product_id, $user_id)
    {
        global $pDbMgr;

        $sSql = "SELECT o.id,o.order_number,o.invoice_date_pay,oi.quantity FROM orders as o
                 JOIN orders_items as oi
                 ON oi.order_id = o.id 
                 WHERE oi.product_id = '".(int)$product_id."' AND o.user_id ='".(int)$user_id."' AND o.internal_status != 'A'"; //wszystko bez anulowanych

        $orders = $pDbMgr->GetAll('profit24', $sSql);

        return $orders;
    }

	/**
	 * Metoda pobiera liste produktow
	 * 
	 * @param	string	$sPageLink	- link do strony kategorii oferty produktowej
	 * @param	string	$sFiltersSql	- kod SQL filtrow
	 * @param	string	$sSorterSql	- kod SQL sortowania
	 * @return	array	- lista produktow
	 */
	function &getProductsList($sPageLink, $sFilterSql, $sSorterSql) {
		global $aConfig;
		$iPerPage = 0;
		$aRecords = array();
		$aIDs = array();

		if(count($aConfig['_tmp']['path'])==1) {
				// okreslenie liczby wszystkich produktow - kategoria glowna
			$sSql = "SELECT COUNT(A.id)
							 FROM ".$aConfig['tabls']['prefix']."products_shadow A
						 	 WHERE ( A.page_id_1 = ".$this->iPageId.
						 	 				" OR A.page_id_2 = ".$this->iPageId.
						 	 				" OR A.page_id_3 = ".$this->iPageId.")"
											.$sFilterSql;
		} else {
			// okreslenie liczby wszystkich produktow
			$sSql = "SELECT COUNT(A.id)
							 FROM ".$aConfig['tabls']['prefix']."products_shadow A
							 JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
							 	ON B.product_id=A.id
							 WHERE 
							 			 B.page_id = ".$this->iPageId.
										$sFilterSql;
		}

		 			 
		if (($iRecords = intval(Common::GetOne($sSql))) > 0) {
			// okreslenie liczby produktow na stronie
			$iPerPage = (int) $this->aSettings['items_per_page'];
			
			if(count($aConfig['_tmp']['path'])==1) {
				// kategoria glowna
				$sSql = "SELECT A.id, A.title AS name, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, A.binding,
												A.isbn, A.isbn_plain, A.publication_year, 
												A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews, A.packet, A.type
								 FROM ".$aConfig['tabls']['prefix']."products_shadow A
								 WHERE ( A.page_id_1 = ".$this->iPageId.
						 	 				" OR A.page_id_2 = ".$this->iPageId.
						 	 				" OR A.page_id_3 = ".$this->iPageId.")"
											.$sFilterSql.$sSorterSql;
			} 
			else {
				$sSql = "SELECT A.id, A.title AS name, A.name2, A.price, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, A.binding,
												A.isbn, A.isbn_plain, A.publication_year, 
												A.prod_status, A.publisher AS publisher_name, A.discount_limit, A.series, A.is_news, A.is_bestseller, A.is_previews, A.packet, A.type
								 FROM ".$aConfig['tabls']['prefix']."products_shadow A
								 JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						 				ON B.product_id=A.id
								 WHERE B.page_id = ".$this->iPageId.
											$sFilterSql.$sSorterSql;
			}
								 
			if ($iPerPage > 0) {
				// uzycie pagera do stronicowania
				include_once('Pager/CPager.class.php');
				//$aPagerParams = array('firstPageText' => $aConfig['lang']['pager']['first_page'], 'lastPageText' => $aConfig['lang']['pager']['last_page'], 'prevImg' => $aConfig['lang']['pager']['previous_page'], 'nextImg' => $aConfig['lang']['pager']['next_page']);
				$aPagerParams = array();
				$aPagerParams['perPage'] = $iPerPage;
				if ($aConfig['common']['mod_rewrite']) {
					$aPagerParams['path'] = $aConfig['common']['base_url_http'].$sPageLink;
					$aPagerParams['append'] = false;
					$aPagerParams['fileName'] = $this->getLink(true).'p%d.html';

                    $additionalHead = new additionalHead();
                    if (isset($_GET['sort']) || isset($_GET['filter'])) {
                        // sortujemy/filtrujemy
                        $baseLink = $this->getBaseLink($sPageLink);
                        $additionalHead->addHeadLinkRel('canonical', $baseLink);
                    }
				}
				else {
					if (count($_SESSION['_settings']['lang']['versions']['symbols']) === 1) {
						$aPagerParams['excludeVars'][] = 'lang';
					}
				}
				$oPager = new CPager($iRecords, $aPagerParams);
				$iStartFrom = $oPager->getStartFrom();
				$sSql .= " LIMIT ".$iStartFrom.",".$aPagerParams['perPage']."";

				$aRecords['items'] =& Common::GetAll($sSql);
				//dump($sSql);
				// dodanie linek Pagera do Smartow
				$aRecords['pager']['links'] =& $oPager->getLinks('all');
				$aRecords['pager']['total_pages'] =& $oPager->getLinks('total_pages');
				$aRecords['pager']['total_items'] =& $oPager->getLinks('total_items');
				$aRecords['pager']['current_page'] =& $oPager->getLinks('current_page');
			}
			else {
				// brak stronicowania archiwum
				$aRecords['items'] =& Common::GetAll($sSql);
			}
//dump($sSql);
			include_once('FreeTransportPromo.class.php');
			$oFreeTransportPromo = new Lib_FreeTransportPromo();
			foreach ($aRecords['items'] as $iKey => $aItem) {
				//pobranie cennika
				$aRecords['items'][$iKey]['tarrif'] = $this->getTarrif($aItem['id']);
				$aRecords['items'][$iKey]['tarrif']['packet'] = ($aItem['packet']=='1');
				// przeliczenie i formatowanie cen
				$aRecords['items'][$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aRecords['items'][$iKey]['price_brutto'], $aRecords['items'][$iKey]['tarrif'], $aRecords['items'][$iKey]['promo_text'],$aItem['discount_limit'], $this->getSourceDiscountLimit($aItem['id'])));
				$aRecords['items'][$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']);

				$aRecords['items'][$iKey]['image'] = $this->getFirstImage($aItem['id'], true);
				$aRecords['items'][$iKey]['link'] = createProductLink($aItem['id'], $aItem['name']);
				// pobranie zakładek produktu
				$aRecords['items'][$iKey]['authors'] = $this->getAuthors($aItem['id']);
				$aRecords['items'][$iKey]['shipment_date'] = FormatDate($aItem['shipment_date']);
			// pobranie info o wydawcy
				//if($aItem['publisher_id']){
				//	$aRecords['items'][$iKey]['publisher'] = $this->getPublisher($aItem['publisher_id']);
				//}
				//$aRecords['items'][$iKey]['series'] = $this->getSeries($aItem['id']);
				$aRecords['items'][$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aItem['id'], 'add');
				$aRecords['items'][$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aItem['id'],'add_to_repository,');

				$pShipmentTime = new \orders\Shipment\ShipmentTime();
				$aRecords['items'][$iKey]['shipment']=$pShipmentTime->getShipmentTime($aItem['id'], $aItem['shipment_time']);
				$aRecords['items'][$iKey]['free_delivery'] = $oFreeTransportPromo->getLogoNameBySymbol($oFreeTransportPromo->getBookFreeTransport($aItem['id'], $aRecords['items'][$iKey]['promo_price']));
			}
		}
		
		return $aRecords;
	} // end of getProductsList() method
	
	
	/**
	 * Metoda pobiera produkt o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @param	boolean $bIsPacket	- czy jest pakietem
	 * @param	boolean	$bSmallThumb - czy najmniejsza miniaturka zdjecia
	 * @return	array	- dane produktu
	 */
	function &getProduct($iId,$isPacket = false, $bSmallThumb = false) {
		global $aConfig, $oCache;
		if (!$this->bDoCache || !is_array($aRecord = unserialize($oCache->get('product_'.$iId, 'queries')))) {
				// wylaczone cache'owanie lub danych nie ma w cache'u
				// pobranie produktu
			// zapytanie wybierajace dane produktu
			if(isPreviewMode()){
				$sSql = "SELECT A.id, A.name, A.price_brutto, A.edition, A.shipment_time, A.shipment_date, L.binding,
												A.isbn, A.isbn_plain, A.publication_year, A.publisher_id,
												A.prod_status, A.is_news, A.is_bestseller, A.is_previews, 
												A.plain_name, A.name2, A.short_description, A.description, A.pages, A.translator, A.volumes, 
												A.volume_nr, A.volume_name, A.city, A.legal_status_date, A.original_title, A.publisher_id,
												I.language, I.photo as language_flag,  J.language AS original_language, 
												J.photo as original_language_flag, K.dimension, A.show_packet_year, A.type
							 FROM ".$aConfig['tabls']['prefix']."products A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages I
						 ON A.language=I.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages J
						 ON A.original_language=J.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_dimensions K
						 ON A.dimension=K.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_bindings L
						 ON A.binding=L.id
								 WHERE 
							 			 A.id = ".$iId;
			} else {
			$sSql = "SELECT A.id, B.title AS name, B.price, B.price_brutto, B.edition, B.shipment_time, B.shipment_date, B.binding,
												B.isbn, B.isbn_plain, B.publication_year, 
												B.prod_status, B.series, B.is_news, B.is_bestseller, B.is_previews, B.discount_limit,
												A.plain_name, A.name2, A.short_description, A.description, A.pages, A.translator, A.volumes, 
												A.volume_nr, A.volume_name, A.city, A.legal_status_date, A.original_title, A.publisher_id,
												I.language, I.photo as language_flag,  J.language AS original_language, 
												J.photo as original_language_flag, K.dimension, A.show_packet_year, A.type
							 FROM ".$aConfig['tabls']['prefix']."products A
							 JOIN ".$aConfig['tabls']['prefix']."products_shadow B
							 ON B.id=A.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages I
						 ON A.language=I.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_languages J
						 ON A.original_language=J.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_dimensions K
						 ON A.dimension=K.id
								 WHERE 
							 			 A.id = ".$iId;
			}			 			 	 
			$aRecord = Common::GetRow($sSql);
		//dump($sSql);
			// jesli znaleziono
			if(!empty($aRecord)){
                $aRecord['description'] = str_ireplace( 'http://', 'https://', $aRecord['description'] );

				// pobranie zdjec produktu
				$aRecord['images'] = $this->getImages($iId,$bSmallThumb);
				
				// pobranie plikow
				$aRecord['files'] = $this->getFiles($iId);

				// pobranie zakładek produktu
				$aRecord['authors'] = $this->getAuthors($iId);
				
				// pobranie tagow
				$aRecord['tags'] = $this->getTags($iId);
				// pobranie serii produktu
				if(isPreviewMode()){
					$aSeries = $this->getSeries($iId);
					$aRecord['series'] = $aSeries[0]['name'];
				}
				// pobranie info o wydawcy
				if($aRecord['publisher_id']){
					$aRecord['publisher'] = $this->getPublisher($aRecord['publisher_id']);
					$aRecord['discount_limit'] = $aRecord['publisher']['discount_limit'];
				}
				// pobranie kategorii produktu
				$aRecord['categories'] = $this->getCategories($iId);

				$pShipmentTime = new \orders\Shipment\ShipmentTime();
				$aRecord['shipment']=$pShipmentTime->getShipmentTime($aRecord['id'], $aRecord['shipment_time']);
				$aRecord['shipment_date'] = FormatDate($aRecord['shipment_date']);
							
				$aRecord['short_link'] = '/,product'.$aRecord['id'].'.html';
				$aRecord['link'] = createProductLink($aRecord['id'], $aRecord['plain_name']);
				$aRecord['cart_link'] = createLink('/koszyk', 'id'.$aRecord['id'], 'add');
				$aRecord['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aRecord['id'],'add_to_repository,');
				// sformatowanie cen
				$aRecord['tarrif'] = $this->getTarrif($iId);
				$aRecord['tarrif']['packet'] = $isPacket;
				// przeliczenie i formatowanie cen
				$aRecord['promo_price'] = Common::formatPrice2($this->getPromoPrice($aRecord['price_brutto'], $aRecord['tarrif'], $aRecord['promo_text'],$aRecord['discount_limit'], $this->getSourceDiscountLimit($iId)));
				$aRecord['price_brutto'] = Common::formatPrice($aRecord['price_brutto']);
                $aRecord['transport_options'] = $this->getTransportOptions(Common::formatPrice2($aRecord['promo_price']));
				
//				if($aRecord['isnews']>0)
//					$aRecord['kapturek']['news']='1';
//				if($aRecord['ispromotion']>0)
//					$aRecord['kapturek']['promotion']='1';
					
			    // jesli jest pakietem
				if($isPacket) {
					$aRecord['packet'] = $this->getPacketItems($iId);
					
					if ($aRecord['show_packet_year'] == '0') {
						// ukryj rok publikacji
						unset($aRecord['publication_year']);
					}
				}
				include_once('FreeTransportPromo.class.php');
				$oFreeTransportPromo = new Lib_FreeTransportPromo();
				$aRecord['free_delivery'] = $oFreeTransportPromo->getLogoNameBySymbol($oFreeTransportPromo->getBookFreeTransport($aRecord['id'], $aRecord['promo_price']));
			} else {
				doRedirect('/');
			}
		
		// zapis do cache'a
			if ($this->bDoCache)
				$oCache->save(serialize($aRecord), 'product_'.$iId, 'queries');
		}	 
		return $aRecord;
	} // end of getProduct() method
  
  
  /**
   * Metoda zwraca listę dostępnych dla prod. opcji trans. wraz z min. cenami
   * 
   * @param float $fPriceBrutto
   * @return array
   */
  private function getTransportOptions($fPriceBrutto) {
    
    $aTransportOpt = array();
    
    $sSql = "SELECT A.id, A.name, A.no_costs_from 
             FROM orders_transport_means as A 
             WHERE A.personal_reception <> '1'";
    
    $aResult = Common::GetAll($sSql);
    
    foreach($aResult as $iKey => $aTransOpt) {
      $sSql = 'SELECT MIN(cost) 
               FROM orders_payment_types 
               WHERE transport_mean = '.$aTransOpt['id'].' AND
                 deleted = "0" AND
                 published = "1" ';
      $fMinCost = Common::GetOne($sSql);
      if($fMinCost !== null && $fPriceBrutto > $aTransOpt['no_costs_from']) {
        $fMinCost = 0;
      }
      if ($fMinCost !== null) {
        $aTransportOpt[] = array(
          'name' => $aTransOpt['name'],
          'cost' => $fMinCost
        );
      }
    }
    $this->sortByColumn($aTransportOpt, 'cost');
    return $aTransportOpt;
  }
  
  /**
   * Metoda sortuje opcje transportu wg ceny 
   * @param array $array
   * @param string $key
   * @param string $direction
   */
   public function sortByColumn(&$array, $key, $direction = "asc")
    {
        usort($array, function($a, $b) use ($key, $direction) {
            if($direction == 'asc') {
                $return = $a[$key] - $b[$key];
            } else {
                $return = $b[$key] - $a[$key];
            }

            return $return;
        });
    }  
    
	
	/**
	 * Metoda pobiera okladki produktu o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @param	intger	$bThumb	- mala miniaturka
	 * @return	array	- okladki produktu
	 */
	function getFirstImage($iId, $bThumb = false){
		global $aConfig, $pSmarty;
		$aImages=array();
		// pobranie z bazy okladek
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."products_images
						WHERE product_id = ".$iId." LIMIT 0,1";
						
		$aResult =& Common::GetAll($sSql);
		if(!empty($aResult)){
		foreach ($aResult as $iKey => $aValue){
			if ($bThumb){
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'])){
					$aImages[$iKey]['thumb'] = '/images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'];	
				}
			} else {
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/__b_'.$aValue['photo'])){
					$aImages[$iKey]['big'] = '/images/photos/'.$aValue['directory'].'/__b_'.$aValue['photo'];
				}
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/'.$aValue['photo'])){
					$aImages[$iKey]['small'] = '/images/photos/'.$aValue['directory'].'/'.$aValue['photo'];
				}
				if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'])){
					$aImages[$iKey]['thumb'] = '/images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'];
				}
			}
			$aImages[$iKey]['description']=$aValue['description'];
		}
		}
		return $aImages;
	}

		
  /**
   * Metoda dodaje nowego subskrybenta do powiadomienia mailowego o dostępności
   * 
   * @param string $sMail
   * @param string $sCaptcha
   * @param id $iId
   * @return bool
   */
  private function _addToNotificationsAvailable($sMail, $sCaptcha, $iId) {
    global $aConfig;
    
    if (!isset($_COOKIE['captcha_sec']) || base64_encode(md5($sCaptcha)) != $_COOKIE['captcha_sec']) {
       // niepoprawna captcha
       $_SESSION['notification'] = 8;
       return;
     }
     if ($sMail != '' && !preg_match($aConfig['class']['form']['email']['pcre'], $sMail)) {
       // niepoprawny adres e-mail
       $_SESSION['notification'] = 6;
       return;
     }

     $aValues = array(
         'product_id' => $iId,
         'email' => $sMail,
         'regulations_accept' => '1',
         'ip' => $_SERVER['REMOTE_ADDR'],
         'created' => 'NOW()',
     );
     if (@Common::Insert('products_available_notifications', $aValues)) {
       $_SESSION['notification'] = 3;
     } else {
       $_SESSION['notification'] = 7;
     }
  }// end of _addToNotificationsAvailable() method
  
	
	/**
	 * Metoda dodaje do bazy telefon lub adres e-mail na ktory ma zostac wyslane powiadomienie
	 * o wznowieniu nakladu lub zmianie statusu ksiazki z zapowiedz
	 * 
	 * @param	string	$sPhone	- telefon
	 * @param	string	$sEmail	- e-mail
	 * @param	integer	$iStatus	- status produktu
	 * @param	integer	$iId	- Id produktu
	 * @return	void
	 */
	 function addToNotifications($sText, $sMail, $sTitle,  $sCaptcha, $iId){
	 	global $aConfig;

	 	if ($sMail == '') {
	 		// nie podano ani telefonu ani adresu email lub podane dane sa nieprawidlowe
	 		$_SESSION['notification'] = 4;
	 		return;
	 	}
	 	
		 if (!isset($_COOKIE['captcha_sec']) || base64_encode(md5($sCaptcha)) != $_COOKIE['captcha_sec']) {
				// niepoprawna captcha
		 		$_SESSION['notification'] = 8;
		 		return;
			}

	 	if ($sMail != '' && !preg_match($aConfig['class']['form']['email']['pcre'], $sMail)) {
	 		// niepoprawny adres e-mail
	 		$_SESSION['notification'] = 6;
	 		return;
	 	}
	 	// dodawanie powiadomienia
	 	$aValues = array(
	 		'product_id' => $iId,
	 		'email' => $sMail,
	 		'content' => $sText != '' ? $sText : 'NULL',
	 		'title' => $sTitle,
	 		'added' => 'NOW()'
	 	);
	 
	 	$sFrom = $aConfig['_settings']['site']['name'];
	 	// adres odnośnie zamówień
		$sFromMail = $aConfig['_settings']['site']['recommendations_email'];

		$sTo = $aConfig['_settings']['site']['recommendations_email'];
		$sSubject = sprintf($aConfig['lang']['mod_'.$this->sModule]['ask_email_subject'], $iId);
		$sBody = sprintf($aConfig['lang']['mod_'.$this->sModule]['ask_email_body'], $sMail,$iId, $sTitle, $sText);

		
		if (Common::sendMail($sFrom, $sFromMail, $sTo, $sSubject, $sBody, false,$sMail)){
			$_SESSION['notification'] = 3;
	 	}
	 	else {
	 		$_SESSION['notification'] = 7;
	 	}
	 } // end of addToNotifications() method
	 
	 
	 function sendNewOrderAdminEmail($iId) {
		global $aConfig, $pSmarty;
		

		$sFrom = $aConfig['_settings']['site']['name'];
		$sFromMail = $aConfig['_settings']['site']['orders_email'];

		$sTo = $aConfig['_settings']['site']['email'];

		$sSubject = sprintf($aConfig['lang']['mod_'.$this->sModule]['ask_email_subject'], $iId);
		
		$sBody = sprintf($aConfig['lang']['mod_'.$this->sModule]['ask_email_body'], $iId, $sTitle, $sMail,$sText);

		
		return Common::sendMail($sFrom, $sFromMail, $sTo, $sSubject, $sBody, false,$sMail);

	} // end of sendNewOrderAdminEmail() method
	 
	
	  
 /**
	  * Metoda pobiera serie produktu o podanym id
	  */
	  function getSeries($iId){
	  	global $aConfig;
	  	$sHtml='';
	  	// wybiera grupy kategorii dla tego produktu
	  	$sSql = "SELECT B.name
	  					 FROM ".$aConfig['tabls']['prefix']."products_to_series A
	  					 JOIN ".$aConfig['tabls']['prefix']."products_series B
	  					 ON A.series_id=B.id
	  					 WHERE A.product_id =".$iId."
	  					ORDER BY B.id";
	  	$aSeries =& Common::GetAll($sSql);
	  	
	  	return $aSeries;
	  }
	  
 /**
	  * Metoda pobiera kategorie produktu o podanym id
	  */
	  function getCategories($iId){
	  	global $aConfig;
	  	$sHtml='';
	  	// wybiera grupy kategorii dla tego produktu
	  	$sSql = "SELECT A.page_id, B.name, B.symbol
	  					 FROM ".$aConfig['tabls']['prefix']."products_extra_categories A
	  					 JOIN ".$aConfig['tabls']['prefix']."menus_items B
	  					 ON A.page_id=B.id
	  					 WHERE A.product_id =".$iId."
	  					ORDER BY B.id";
	  	$aCats =& Common::GetAll($sSql);
	  	
	  	return $aCats;
	  }

	   	/**
	 * Metoda sprawdza czy produkt o podanym Id jest pakietem
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @return	bool	- true: jest; false: nie
	 */
	function isPacket($iId) {
		global $aConfig;
		
		$sSql = "SELECT packet
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE 
						 			 id = ".$iId.
						 			 (!isPreviewMode() ? " AND published = '1'" : '');
		return intval(Common::GetOne($sSql)) != 0;
	} // end of isPacket() function
	
		/**
	 * Metoda pobiera produkty z pakietu o podanym id
	 * 
	 * @param	intger	$iId	- Id pakietu
	 * @return	array	- produkty pakietu
	 */
	function getPacketItems($iId){
		global $aConfig, $pSmarty;
		$aPackItems = array();
		
		$sSql = "SELECT product_id, discount, discount_currency
						FROM ".$aConfig['tabls']['prefix']."products_packets_items
						WHERE packet_id = ".$iId."";
		$aItems =& Common::GetAssoc($sSql, true);
		
		foreach($aItems as $iPackId => $aItem){
			$aPackItems[$iPackId] =@ array_merge($this->getProduct($iPackId, false, true), $aItem);
		}
		return $aPackItems;
	} // end of getPacketItems() method
	
	/**
	 * Metoda pobiera tagi produktu o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @return	array	- tagi produktu
	 */
	function getTags($iId){
		global $aConfig, $pSmarty;
		
		$sSql = "SELECT B.tag
						FROM ".$aConfig['tabls']['prefix']."products_to_tags A
						JOIN ".$aConfig['tabls']['prefix']."products_tags B
						ON B.id=A.tag_id
						WHERE A.product_id = ".$iId."";
						
		$aResult =& Common::GetAll($sSql);
		
		// utworzenie linkow
		foreach($aResult as $iKey => $aValue){
			$aTags[$iKey]['tag'] = $aValue['tag'];
			$aTags[$iKey]['link'] = getTagLink($aValue['tag']);
		}
		
		return $aTags;
	} // end of getTags() method
	
		/**
	 * Metoda pobiera produkt o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @param	boolean $bIsPacket	- czy jest pakietem
	 * @param	boolean	$bSmallThumb - czy najmniejsza miniaturka zdjecia
	 * @return	array	- dane produktu
	 */
	function itemExists($iId) {
		global $aConfig;
		
		// pobranie produktu
		$sSql = "SELECT A.id
						 FROM ".$aConfig['tabls']['prefix']."products A
						 WHERE A.id = ".$iId.
					 			 	 (!isPreviewMode() ? " AND A.published = '1'" : '');
		return ((double) Common::GetOne($sSql)) > 0;
	} // end of getProduct() method
	
	/**
	  * Metoda laczy tagi lacznikiem podanym w pierwszym parametrze
	  * 
	  * @param	string	$sGlue	- lacznik
	  * @param	array ref	$aTags	- tagi
	  * @return	string
	  */
	function implodeTags($sGlue, &$aTags) {
		$sStr = '';
		foreach ($aTags as $aTag) {
			$sStr .= $aTag['tag'].$sGlue;
		}
		return substr($sStr, 0, -strlen($sGlue));
	} // end of implodeTags() method
	
	function getAuthorsString(&$aAuthors){
		global $aConfig;
		$sAuthors = '';
		$iCount=0;
		if(!empty($aAuthors)){
		foreach($aAuthors as $sRole=>$aList){
			foreach($aList as $aAuthor){
				$sAuthors.=$aAuthor['name'].', ';
				if(++$iCount>3) return substr($sAuthors,0,-2);
			}
		}
		return substr($sAuthors,0,-2);
		}
		return '';
	}
	
		/**
	 * Metoda pobiera pliki produktu o podanym id
	 * 
	 * @param	intger	$iId	- Id produktu
	 * @return	array	- pliki produktu
	 */
	function getFiles($iId){
		global $aConfig, $pSmarty;
		
		// pobranie z bazy
		$sSql = "SELECT B.name, A.filename
						FROM ".$aConfig['tabls']['prefix']."products_files A
						JOIN ".$aConfig['tabls']['prefix']."products_file_types B
						ON B.id=A.ftype
						WHERE product_id = ".$iId."";
						
		$aResult =& Common::GetAll($sSql);
		
		// utworzenie linkow
		foreach($aResult as $iKey => $aValue){
			$aFiles[$iKey]['name'] = $aValue['name'];
      $aFiles[$iKey]['filename'] = $aValue['filename'];
			$aFiles[$iKey]['link'] = $aConfig['common']['product_file_dir'].'/'.$iId.'/'.$aValue['filename'];
		}
		
		return $aFiles;
	} // end of getFiles() method
} // end of Module Class
?>