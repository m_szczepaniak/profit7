<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa - wspolna klasa'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Module_Common {
    
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module_Common() {} // end of Module_Common() method
	
  
  
  /**
   * Metoda zwraca tablicę źródeł
   * 
   * @global array $aConfig
   * @return array
   */
  public function _getInternalSources() {
    global $aConfig;

    return array(
        array('label' => 'e', 'value' => 'e'), 
        array('label' => 'j', 'value' => 'j'), 
        array('label' => 'm', 'value' => 'm'), 
        array('label' => 'g', 'value' => 'g'), 
        array('label' => 'x', 'value' => 'x'), 
    );
  }// end of _getInternalSources() function

  
  /**
   * Metoda generuje zapytanie wyszukujące
   * 
   * @param string $sInternalSource
   * @param array $aCategoriesIds
   * @return string
   */
  public function _getInternalSourcePublishers($sInternalSource, $aCategoriesIds) {

    $sWhereSQL = '';
    $aAllInternalSources = $this->_getInternalSources();
    foreach ($aAllInternalSources as $aItem) {

      if ($aItem['value'] == $sInternalSource) {
        $sWhereSQL .= ' AND PS.`profit_'.$aItem['value'].'_status` <> "0" ';
      } else {
        $sWhereSQL .= ' AND PS.`profit_'.$aItem['value'].'_status` = "0" ';
      }
    }
    foreach ($aCategoriesIds as $iKey => $iCatId) {
      if (intval($iCatId) <= 0) {
        unset($aCategoriesIds[$iKey]);
      }
    }
    if (!empty($aCategoriesIds)) {
      $sWhereSQL .= ' AND A.publisher_id IN ('.implode(',', $aCategoriesIds).') ';
    }

    return $sWhereSQL;
  }// end of getInternalSourcePublishers() method
  
  
	/**
	 * Metoda pobiera i zwraca nazwe produktu
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @return	string
	 */
	function getItemName($iId) {
		global $aConfig;
		
		$sSql = "SELECT name
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getItemName() method
	
	
	/**
	 * Metoda sprawdza czy ksiazka jest bestsellerem
	 * 
	 * @param	integer	$iId	- Id ksiazki
	 * @return	bool
	 */
	function isBestseller($iId) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."products_bestsellers
						 WHERE product_id = ".$iId;
		return intval(Common::GetOne($sSql)) > 0;
	} // end of isBestseller() method


	/**
	 * Metoda sprawdza czy ksiazka jest nowoscia
	 * 
	 * @param	integer	$iId	- Id ksiazki
	 * @return	bool
	 */
	function isNews($iId) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."products_news
						 WHERE product_id = ".$iId;
		return intval(Common::GetOne($sSql)) > 0;
	} // end of isNews() method
	
		/**
 * Metoda pobiera autorow produktu o podanym id
 * 
 * @param	intger	$iId	- Id produktu
 * @return	array	- autorzy produktu
 */
function getAuthors($iId){
	global $aConfig;
	
	// pobranie autorow ksiazki
	$sSql = "SELECT B.name as role_name, CONCAT(C.surname,' ',C.name) as author
					FROM ".$aConfig['tabls']['prefix']."products_to_authors A
					JOIN ".$aConfig['tabls']['prefix']."products_authors_roles B
					ON B.id = A.role_id
					JOIN ".$aConfig['tabls']['prefix']."products_authors C
					ON C.id = A.author_id
					WHERE A.product_id = ".$iId."
					ORDER BY B.order_by";
					
	$aResult =& Common::GetAll($sSql);
	$sAuthors='';

	foreach($aResult as $aAuthor){
		$sAuthors .= $aAuthor['author'].', ';
	}
	return substr($sAuthors,0,-2);
} // end of getAuthors() method

		/**
 * Metoda pobiera autorow produktu o podanym id z wybranej bazy
 * 
 * @param string $sDb - symbol bazy
 * @param	intger	$iId	- Id produktu
 * @return	array	- autorzy produktu
 */
function getAuthorsM($sDb, $iId){
	global $aConfig,$pDbMgr;
	
	// pobranie autorow ksiazki
	$sSql = "SELECT B.name as role_name, CONCAT(C.surname,' ',C.name) as author
					FROM ".$aConfig['tabls']['prefix']."products_to_authors A
					JOIN ".$aConfig['tabls']['prefix']."products_authors_roles B
					ON B.id = A.role_id
					JOIN ".$aConfig['tabls']['prefix']."products_authors C
					ON C.id = A.author_id
					WHERE A.product_id = ".$iId."
					ORDER BY B.order_by";
					
	$aResult =& $pDbMgr->GetAll($sDb,$sSql);
	$sAuthors='';

	foreach($aResult as $aAuthor){
		$sAuthors .= $aAuthor['author'].', ';
	}
	return substr($sAuthors,0,-2);
} // end of getAuthors() method

	/**
	 * Metoda sprawdza czy ksiazka jest nowoscia
	 * 
	 * @param	integer	$iId	- Id ksiazki
	 * @return	bool
	 */
	function isLinked($iId,$iLId) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."products_linked
						 WHERE product_id = ".$iId."
						 AND linked_id = ".$iLId;
		return intval(Common::GetOne($sSql)) > 0;
	} 
	
		/**
	 * Metoda sprawdza czy ksiazka jest nowoscia
	 * 
	 * @param	integer	$iId	- Id ksiazki
	 * @return	bool
	 */
	function isEdition($iId,$iLId) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."products_editions
						 WHERE product_id = ".$iId."
						 AND linked_id = ".$iLId;
		return intval(Common::GetOne($sSql)) > 0;
	} 
	
	function getPrevEditions($iId){
		global $aConfig;
		
		$sSql = "SELECT linked_id
						 FROM ".$aConfig['tabls']['prefix']."products_editions
						 WHERE product_id = ".$iId;
		return Common::GetCol($sSql);
	}
	
	/**
	 * Metoda sprawdza czy ksiazka jest zapowiedzia
	 * 
	 * @param	integer	$iId	- Id ksiazki
	 * @return	bool
	 */
	function isPreview($iId) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."products_previews
						 WHERE product_id = ".$iId;
		return intval(Common::GetOne($sSql)) > 0;
	} // end of isPreview() method
	

	/**
	 * Metoda sprawdza czy ksiazka jest promocja
	 * 
	 * @param	integer	$iId	- Id ksiazki
	 * @return	bool
	 */
	function isPromotion($iId) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."products_promotions
						 WHERE product_id = ".$iId;
		return intval(Common::GetOne($sSql)) > 0;
	} // end of isPromotion() method
	
	
	/**
	 * 
	 * @param	integer	$iMId	- Id menu oferty produktowej
	 */
	function getProductsCategories($aMId, $sLabel,$bAllowOther=false) {
		global $aConfig;
		$aItems = array();
		if (empty($aMId)) return $aItems;
		
		$sSql = "SELECT IFNULL(parent_id, 0), id, name, mtype, module_id,
										IFNULL(link_to_id, 0) AS link_to_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE menu_id IN (".implode(',',$aMId).") AND
						 			 language_id = ".$_SESSION['lang']['id']."
						 			 ORDER BY parent_id, order_by";
		$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
		// utworzenie drzewa menu
		$aMenuItems =& getMenuTree($aItems);

		if($bAllowOther){
			$iAllowedModule=0;
		} else {
			$iAllowedModule=getModuleId('m_oferta_produktowa');
		}
		
		// strona do ktorej linkuje
		if($sLabel != ''){
		$aMenuTree2 = array_merge(
			array(array('value' => '', 'label' => $sLabel)),
			getComboMenuTree($aMenuItems, 0, 0, 0, $iAllowedModule)
		);
		}
		else {
			$aMenuTree2=getComboMenuTree($aMenuItems,0, 0, 0, $iAllowedModule);
		}
		return $aMenuTree2;
	} // end of getProductsCategories method
	
	function getTopProductsCategories($bPublished=false){
		global $aConfig;
		$sSql="SELECT id AS value, name AS label
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".getModuleId('m_oferta_produktowa')."
					 AND mtype = '0'
					 AND parent_id IS NULL".
					($bPublished?" AND published = '1'":'');
		$aTopPages = Common::GetAll($sSql);
		return array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),$aTopPages);
	}
	
	/**
	 * 
	 * @param	integer	$iMId	- Id menu oferty produktowej
	 */
	function getProductsCategoriesM($sDb,$aMId, $sLabel,$bAllowOther=false) {
		global $aConfig,$pDbMgr;
		$aItems = array();
		if (empty($aMId)) return $aItems;
		
		$sSql = "SELECT IFNULL(parent_id, 0), id, name, mtype, module_id,
										IFNULL(link_to_id, 0) AS link_to_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE menu_id IN (".implode(',',$aMId).") AND
						 			 language_id = ".$_SESSION['lang']['id']."
						 			 ORDER BY parent_id, order_by";
		$aItems =& $pDbMgr->GetAssoc($sDb,$sSql, true, array(), DB_FETCHMODE_ASSOC, true);
		// utworzenie drzewa menu
		
		$aMenuItems =& getMenuTree($aItems);

		if($bAllowOther){
			$iAllowedModule=0;
		} else {
			$iAllowedModule=getModuleIdM($sDb,'m_oferta_produktowa');
		}
		
		// strona do ktorej linkuje
		if($sLabel != ''){
		$aMenuTree2 = array_merge(
			array(array('value' => '', 'label' => $sLabel)),
			getComboMenuTree($aMenuItems, 0, 0, 0, $iAllowedModule)
		);
		}
		else {
			$aMenuTree2=getComboMenuTree($aMenuItems,0, 0, 0, $iAllowedModule);
		}
		return $aMenuTree2;
	} // end of getProductsCategories method
	
	function getTopProductsCategoriesM($sDb,$bPublished=false){
		global $aConfig,$pDbMgr;
		$sSql="SELECT id AS value, name AS label
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".getModuleIdM($sDb,'m_oferta_produktowa')."
					 AND mtype = '0'
					 AND parent_id IS NULL".
					($bPublished?" AND published = '1'":'');
		$aTopPages = $pDbMgr->GetAll($sDb,$sSql);
		return array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),$aTopPages);
	}

	/**
	 * 
	 * @param	integer	$iMId	- Id menu oferty produktowej
	 */
	function getProductsCategoriesChTree($aMId, $bAllowOther=false) {
		global $aConfig;
		$aItems = array();
		if (empty($aMId)) return $aItems;
		
		$sSql = "SELECT IFNULL(parent_id, 0), id, name, mtype, module_id,
										IFNULL(link_to_id, 0) AS link_to_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE menu_id IN (".implode(',',$aMId).") AND
						 			 language_id = ".$_SESSION['lang']['id']."
						 			 ORDER BY parent_id, order_by";
		$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
		// utworzenie drzewa menu
		
		$aMenuItems =& getMenuTree($aItems);

		if($bAllowOther){
			$iAllowedModule=0;
		} else {
			$iAllowedModule=getModuleId('m_oferta_produktowa');
		}
		

		$aMenuTree2=getCheckboxMenuTree($aMenuItems, 0, 0, 0, $iAllowedModule);
		return $aMenuTree2;
	} // end of getProductsCategories method
	
	/**
	 * Metoda pobiera liste produktow z kategorii
	 * 
	 * @param	integer	$iPId	- Id strony
	 * @param	string	$sType	- typ: 'b' - bestseller
	 * 															 'n' - nowosc
	 * 															 'pre' - zapowiedz
	 * 															 'prom' - promocja
	 * @return	array ref	- lista produktow
	 */
	function &getProducts($sType='b', $sName='', $sAuthor='', $sPublisher='',$bNoSel=true,$iOwner=0) {
		global $aConfig;
		$sWhereSql = '';
		$sWhat = '';
		$aItems = array(
			array('value' => '', 'label' => $aConfig['lang']['common']['choose'])
		);
		if(!empty($sName) || !empty($sAuthor) || !empty($sPublisher)){
			switch ($sType) {
				case 'b':
					$sWhereSql = " AND A.prod_status='1' AND A.id NOT IN (SELECT product_id FROM ".$aConfig['tabls']['prefix']."products_bestsellers)";
					$sWhat = "CONCAT(A.id, ' - ', LEFT(A.plain_name,50), IFNULL(CONCAT(' - ', A.publication_year,' - ', LEFT(F.name,15)),''))";
				break;
	
				case 'n':
					$sWhereSql = " AND A.prod_status='1' AND A.id NOT IN (SELECT product_id FROM ".$aConfig['tabls']['prefix']."products_news)";
					$sWhat = "CONCAT(A.id, ' - ', LEFT(A.plain_name,50), IFNULL(CONCAT(' - ', A.publication_year,' - ', LEFT(F.name,15)),''))";
					
				break;
				
				case 'pre':
					$sWhereSql = " AND A.id NOT IN (SELECT product_id FROM ".$aConfig['tabls']['prefix']."products_previews)";
					$sWhat = "CONCAT(A.id, ' - ', LEFT(A.plain_name,50), IFNULL(CONCAT(' - ', A.publication_year,' - ', LEFT(F.name,15)),''))";
				break;
				
				case 'link':
					$sWhereSql = " AND A.prod_status='1' AND A.id <> ".$iOwner." AND A.id NOT IN (SELECT linked_id FROM ".$aConfig['tabls']['prefix']."products_linked WHERE product_id = ".$iOwner.")";
					$sWhat = "CONCAT(A.id, ' - ', LEFT(A.plain_name,50), IFNULL(CONCAT(' - ', A.publication_year,' - ', LEFT(F.name,15)),''))";
				break;
				
				case 'e':
					$sWhereSql = " AND A.id <> ".$iOwner." AND A.id NOT IN (SELECT linked_id FROM ".$aConfig['tabls']['prefix']."products_editions WHERE product_id = ".$iOwner.")";
					$sWhat = "CONCAT(A.id, ' - ', LEFT(A.plain_name,50), IFNULL(CONCAT(' - ', A.publication_year,' - ', LEFT(F.name,15)),''))";
				break;
				
				case 'prom':
					$sWhereSql = " AND A.prod_status='1' AND discount = 0";
					$sWhat = "CONCAT(A.id, ' - ', LEFT(A.plain_name,50), IFNULL(CONCAT(' - ', A.publication_year,' - ', LEFT(F.name,15)),''))";
				break;
				
				case 'packet':
				default:
					// wybranie wszystkich produktow
					$sWhat = "CONCAT(A.id, ' - ', LEFT(A.plain_name,50), IFNULL(CONCAT(' - ', A.publication_year,' - ', LEFT(F.name,15)),''))";
				break;
			}
			
			$sSql = "SELECT A.id value, ".$sWhat." label
							 FROM ".$aConfig['tabls']['prefix']."products A".
							 (!empty($sAuthor) ?
								" LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_authors D
							 	 ON A.id = D.product_id
							 	 LEFT JOIN ".$aConfig['tabls']['prefix']."products_authors E
							 	 ON E.id = D.author_id" : "").
								" LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers F
							 	 ON A.publisher_id = F.id".
							 " WHERE 1=1 ".(!empty($sName) ? (is_numeric($sName) ? ' AND A.id = '.(int) $sName : ' AND A.name LIKE \'%'.$sName.'%\'') : '').
													(!empty($sAuthor) ? ' AND CONCAT(E.surname,\' \',E.name) LIKE \'%'.$sAuthor.'%\'':'').
													(!empty($sPublisher) ? ' AND F.name = \''.$sPublisher.'\'':'').
							 			 $sWhereSql.
							 " ORDER BY A.id";
			$aProducts =& Common::GetAll($sSql);
			foreach($aProducts as $iKey=>$aProduct) {
//				$sPubl=$this->getPublisherName(intval($aProduct['publisher_id']));
					$sAuth = mb_substr($this->getAuthors($aProduct['value']),0,40,'UTF-8');
				if(!empty($sAuth)) {
					$aProducts[$iKey]['label'] = $aProducts[$iKey]['label'].' - '.$sAuth;
				}
//				unset($aProducts[$iKey]['publisher_id']);
			}
			if (!empty($aProducts)) {
				if(!$bNoSel)
					$aItems = $aProducts;
				else
					$aItems = array_merge($aItems, $aProducts);
			}
		}
		return $aItems;
	} // end of getProducts() method
	
	
	/**
	 * Metoda pobiera $sAttrib atrybut produktu o podanym Id
	 *
	 * @param	integer	$iId	- Id produktu
	 * @param	string	$sAttrib	- atrybut
	 * @return	array ref	- lista rekordow
	 */
	function &getProductAttrib($iId, $sAttrib) {
		global $aConfig;

		$sSql = "SELECT ".$sAttrib."
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getProductAttrib() method
  
  
	/**
	 * Metoda pobiera $sAttrib atrybut produktu o podanym Id
	 *
	 * @param	integer	$iId	- Id produktu
	 * @param	array	$aAttribs	- atrybuty
	 * @return	array ref	- lista rekordow
	 */
	function &getProductAttribs($iId, $aAttribs) {
		global $aConfig;

		$sSql = "SELECT ".implode(', ', $aAttribs)."
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id = ".$iId;
		return Common::GetRow($sSql);
	} // end of getProductAttribs() method
	
	
	function &getSeriesList($iPublisherId=0){
		global $aConfig;
		$sSql="SELECT id AS value, name AS label
					 FROM ".$aConfig['tabls']['prefix']."products_series".
					($iPublisherId>0?' WHERE publisher_id = '.$iPublisherId:'').
					" ORDER BY name";
		return 	array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),Common::GetAll($sSql));
	}
	
	function &getSeriesListM($sDb,$iPublisherId=0){
		global $aConfigm,$pDbMgr;
		$sSql="SELECT id AS value, name AS label
					 FROM ".$aConfig['tabls']['prefix']."products_series".
					($iPublisherId>0?' WHERE publisher_id = '.$iPublisherId:'').
					" ORDER BY name";
		return 	array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),$pDbMgr->GetAll($sDb,$sSql));
	}	
	
	function &getPublisherSeriesList($sPublisher=''){
		global $aConfig;
        $aPublishers = [];
        if ($sPublisher != '') {
            $sSql = "SELECT A.id AS value, A.name AS label
					 FROM " . $aConfig['tabls']['prefix'] . "products_series A
					 JOIN " . $aConfig['tabls']['prefix'] . "products_publishers B
					 ON A.publisher_id=B.id
					 WHERE B.name LIKE '" . $sPublisher . "'
					 ORDER BY A.name";
            $aPublishers = Common::GetAll($sSql);
        }
		return array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),$aPublishers);
	}
	
	function &getPublisherSeriesListM($sDb,$sPublisher=''){
		global $aConfig,$pDbMgr;
		$sSql="SELECT A.id AS value, A.name AS label
					 FROM ".$aConfig['tabls']['prefix']."products_series A
					 JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					 ON A.publisher_id=B.id
					 WHERE B.name LIKE '".$sPublisher."'
					 ORDER BY A.name";
		return array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),$pDbMgr->GetAll($sDb,$sSql));
	}
	
function &getNumbersList($iMax){
		global $aConfig;
		$aList=array(array('value'=>'','label'=>$aConfig['lang']['common']['choose']));
		for($i=1;$i<=$iMax;$i++){
			$aList[]=array('value'=>$i,'label'=>$i);
		}
		return $aList;
	}
	
	function &getVATList(){
		global $aConfig;
		$aList=array(
			0 => array('value'=>'','label'=>$aConfig['lang']['common']['choose']),
			/*1 => array('value'=>0,'label'=>'0'),
			2 => array('value'=>7,'label'=>'7'),
			3 => array('value'=>22,'label'=>'22')*/
		);
		//pobranie stawek vat z bazy
	 $sSql = "SELECT value FROM ".$aConfig['tabls']['prefix']."products_vat_stakes WHERE 1 ORDER BY value";
		
		$aStakes = Common::GetAll($sSql);
		$stakesList='';
		
		//pobranie jako lista
		foreach($aStakes as $aStake) {
			$aList[]=array('value'=>intval($aStake['value']),'label'=>intval($aStake['value']));
		}
		return $aList;
	}
	
	function &getExtraCategories($iId){
		global $aConfig;
		$sSql="SELECT page_id
					 FROM ".$aConfig['tabls']['prefix']."products_extra_categories
					 WHERE product_id = ".$iId;
		return Common::GetCol($sSql);
	}
	

	function &getProductSeries($iId){
		global $aConfig;
		$sSql="SELECT series_id
					 FROM ".$aConfig['tabls']['prefix']."products_to_series
					 WHERE product_id = ".$iId;
		return Common::GetCol($sSql);
	}
	
	function &getNewsStatus($iId){
		global $aConfig;
		$sSql="SELECT page_id
					 FROM ".$aConfig['tabls']['prefix']."products_news
					 WHERE product_id = ".$iId;
		return intval(Common::GetOne($sSql));
	}
	
	function &getNews($iId){
		global $aConfig;
		$sSql="SELECT *
					 FROM ".$aConfig['tabls']['prefix']."products_news
					 WHERE product_id = ".$iId;
		return Common::GetRow($sSql);
	}
	
	function &getPreviewsStatus($iId){
		global $aConfig;
		$sSql="SELECT page_id
					 FROM ".$aConfig['tabls']['prefix']."products_previews
					 WHERE product_id = ".$iId;
		return intval(Common::GetOne($sSql));
	}
	
	function &getPreviews($iId){
		global $aConfig;
		$sSql="SELECT *
					 FROM ".$aConfig['tabls']['prefix']."products_previews
					 WHERE product_id = ".$iId;
		return Common::GetRow($sSql);
	}
	
	/**
	 * Metoda przelicza ceny
	 * 
	 * @return	void
	 */
	function calculatePrices() {
		$_POST['discount_money'] = 0;
		$_POST['promo_price_brutto'] = 0;
		$_POST['promo_price_netto'] = 0;
		
		// cena brutto
		$_POST['price_brutto'] = Common::formatPrice2($_POST['price_brutto']);
		$_POST['weight'] = Common::formatPrice2($_POST['weight']);
		// vat
		$_POST['vat'] = (int) $_POST['vat'];
		if ($_POST['price_brutto'] > 0) {
			// zdefiniowano cene - okreslenie ceny netto
			$_POST['price_netto'] = $_POST['vat'] > 0 ? Common::formatPrice2($_POST['price_brutto'] / (1 + ($_POST['vat'] / 100))) : $_POST['price_brutto'];
			// rabat
			$_POST['discount'] = Common::formatPrice2($_POST['discount']);
			if ($_POST['discount'] > 0) {
				// obliczenie wartosci brutto rabatu
				$_POST['discount_money'] = Common::formatPrice2($_POST['price_brutto'] * ($_POST['discount'] / 100));
				// cena promocyjna brutto
				$_POST['promo_price_brutto'] = $_POST['price_brutto'] - $_POST['discount_money'];
				// cena promocyjna netto
				$_POST['promo_price_netto'] = $_POST['vat'] > 0 ? Common::formatPrice2($_POST['promo_price_brutto'] / (1 + ($_POST['vat'] / 100))) : $_POST['promo_price_brutto'];
			}
		}
		else {
			$_POST['price_netto'] = $_POST['discount'] = 0;
		}
	} // end of calculatePrices() method
	
		/**
	 * Metoda przelicza cennik
	 * 
	 * @return	void
	 */
	function calculateTarrif($fBaseBrutto,$iVat,$iDiscount) {
		
		$aTarrif=array();
		$aTarrif['discount']=Common::formatPrice2($iDiscount);
		$fBaseBrutto = Common::formatPrice2($fBaseBrutto);
		if ($fBaseBrutto > 0) {
			if ($iDiscount > 0) {
				// obliczenie wartosci brutto rabatu
				$aTarrif['discount_value'] = Common::formatPrice2($fBaseBrutto * ($iDiscount / 100));
				// cena promocyjna brutto
				$aTarrif['price_brutto'] = $fBaseBrutto - $aTarrif['discount_value'];
				// cena promocyjna netto
				$aTarrif['price_netto'] = $iVat > 0 ? Common::formatPrice2($aTarrif['price_brutto'] / (1 + ($iVat / 100))) : $aTarrif['price_brutto'];
			} else {
				$aTarrif['price_brutto'] = $fBaseBrutto;
				$aTarrif['discount_value'] = 0;
				$aTarrif['price_netto'] = $iVat > 0 ? Common::formatPrice2($aTarrif['price_brutto'] / (1 + ($iVat / 100))) : $aTarrif['price_brutto'];
			}
		}
		else {
			$aTarrif['price_brutto'] = $aTarrif['price_netto'] = $aTarrif['discount_value']=$aTarrif['discount'] = 0;
		}
		return $aTarrif;
	} // end of calculateTarrif() method
	
	/**
	 * Metoda dodaje produkt do tabeli bestsellerow
	 */
	 function addBestseller($iId){
	 	global $aConfig;
	 	
	 	$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_bestseller='1'
							WHERE id = ".$iId;
		if(Common::Query($sSql) === false) {
			
			$bIsError = true;
		}
		
		if($this->existShadow($iId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_bestseller='1'
							WHERE id = ".$iId;
			if(Common::Query($sSql) === false) {
				
				$bIsError = true;
			}
		}
	 	
	 	$aValues = array (
	 		'product_id' => $iId,
	 		'page_id' => $_POST['page_id'],
	 		'order_by' => '#order_by#'
	 	);
	 	
	 	if(Common::Insert($aConfig['tabls']['prefix']."products_bestsellers",
	 										$aValues,'page_id = '.intval($_POST['page_id']),false) === false) {
				$bIsError = true;
			}
			return !$bIsError;

	 } // end of addBestseller method
	 
	 /**
	  * Metoda usuwa produkt z tabeli bestsellerow
	  */
	  function deleteBestseller($iId){
	  	global $aConfig;
	  	
	  	
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_bestseller='0'
							WHERE id = ".$iId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		
		if($this->existShadow($iId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_bestseller='0'
							WHERE id = ".$iId;
			if(Common::Query($sSql) === false) {
				$bIsError = true;
			}
		}
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_bestsellers
						 WHERE product_id = ".$iId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		return !$bIsError;
	  	
	  } // end of deleteBestseller method
	 
	/**
	 * Metoda sprawdza czy produkt o podanym id znajduje sie w tabeli bestsellerow
	 */
	 function bestsellerExists($iId){
	 	global $aConfig;
	 	
	 	$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."products_bestsellers
	 					WHERE product_id = ".$iId."";
	 					
	 	return (int)Common::GetRow($sSql);	
	 } // end of bestsellerExists method
	 
	function getNextNewsOrderBy($iPageId){
		global $aConfig;
		$sSql="SELECT max(order_by)+1 
						FROM ".$aConfig['tabls']['prefix']."products_news
						WHERE order_by <> 4294967295
						AND page_id = ".$iPageId;
		return Common::GetOne($sSql);
	}
	 
	/**
	 * Metoda dodaje produkt do tabeli nowosci
	 */
	 function addNews($iId, $bAuto=false){
	 	global $aConfig;
	 	$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_news='1'
							WHERE id = ".$iId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		
		if($this->existShadow($iId)) {
			$aShadow = $this->getProductShadow($iId);
			$aShadow['is_news'] = 1; 
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_news = '1',
							order_by = ".$this->getOrderByNr($aShadow).",
							order_by_status_old_style = ".$this->getOrderByStatusOldStyle($aShadow)."
							WHERE id = ".$iId;
			if(Common::Query($sSql) === false) {
				$bIsError = true;
			}
		}
	 	
	 	$aValues = array (
	 		'product_id' => $iId,
	 		'news_from' => 'NOW()',
	 		'order_by' => $bAuto?4294967295:(($_POST['manual_sort']=='1')?$this->getNextNewsOrderBy($_POST['page_id']):4294967295),
	 		'page_id' => $bAuto?$_POST['news_status']:$_POST['page_id'],
	 		'manual_sort' => $bAuto?'0':$_POST['manual_sort']
	 	);
	 	
	 	if(Common::Insert($aConfig['tabls']['prefix']."products_news",
	 										$aValues,'',false) === false) {
				$bIsError = true;
			}
			return !$bIsError;
	 } // end of addNews method
	 
	 /**
	  * Metoda usuwa produkt z tabeli nowosci
	  */
	  function deleteNews($iId){
	  	global $aConfig;
	  	
	  	$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_news='0'
							WHERE id = ".$iId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		
	  if($this->existShadow($iId)) {
			$aShadow = $this->getProductShadow($iId);
			$aShadow['is_news'] = 0; 
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_news = '0',
							order_by = ".$this->getOrderByNr($aShadow).",
							order_by_status_old_style = ".$this->getOrderByStatusOldStyle($aShadow)."
							WHERE id = ".$iId;
			if(Common::Query($sSql) === false) {
				$bIsError = true;
			}
		}
		
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_news
						 WHERE product_id = ".$iId;
		if(Common::Query($sSql) === false) {
			$bIsError = true;
		}
		return !$bIsError;
	  	
	  } // end of deleteNews method
	 
	/**
	 * Metoda sprawdza czy produkt o podanym id znajduje sie w tabeli nowosci
	 */
	 function newsExists($iId){
	 	global $aConfig;
	 	
	 	$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."products_news
	 					WHERE product_id = ".$iId."";
	 					
	 	return (int)Common::GetRow($sSql);	
	 } // end of newsExists method
	 
	/**
	 * Metoda dodaje produkt do tabeli promocji
	 */
	 function addPromotion($iId){
	 	global $aConfig;
	 	
	 	$aValues = array (
	 		'product_id' => $iId,
	 		'order_by' => '#order_by#'
	 	);
	 	
	 	return Common::Insert($aConfig['tabls']['prefix']."products_promotions",
	 								$aValues);
	 } // end of addPromotion method
	 
	 /**
	  * Metoda usuwa produkt z tabeli promocji
	  */
	  function deletePromotion($iId){
	  	global $aConfig;
	  	
	  	$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_promotions
	  						WHERE product_id = ".$iId."";
	  						
	  	return Common::Query($sSql);
	  } // end of deletePromotions method
	 
	/**
	 * Metoda sprawdza czy produkt o podanym id znajduje sie w tabeli promocji
	 */
	 function promotionExists($iId){
	 	global $aConfig;
	 	
	 	$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."products_promotions
	 					WHERE product_id = ".$iId."";
	 					
	 	return (int)Common::GetRow($sSql);	
	 } // end of promotionExists method
	 
	 
	/**
	 * Metoda dodaje produkt do zapowiedzi
	 * 
	 * @global type $aConfig
	 * @param type $iId
	 * @return type
	 */
	 function addPreview($iId, $bAuto=false){
	 	global $aConfig;
	 	
	 	$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
							SET is_previews='1'
							WHERE id = ".$iId;
		if(Common::Query($sSql) === false) {
			
			$bIsError = true;
		}
		
		if($this->existShadow($iId)) {
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
							SET is_previews='1'
							WHERE id = ".$iId;
			if(Common::Query($sSql) === false) {
				
				$bIsError = true;
			}
		}
	 	
	 	$aValues = array (
	 		'product_id' => $iId,
	 		'order_by' => $bAuto?4294967295:(($_POST['manual_sort']=='1')?$this->getNextNewsOrderBy($_POST['page_id']):4294967295),
	 		'page_id' => $bAuto?$_POST['previews_status']:$_POST['page_id'],
	 		'manual_sort' => $bAuto?'0':$_POST['manual_sort']
	 	);
	 	
	 	if(Common::Insert($aConfig['tabls']['prefix']."products_previews",
	 										$aValues,'page_id = '.intval($_POST['page_id']),false) === false) {
				$bIsError = true;
			}
			return !$bIsError;

	 } // end of addPreviews method
	 
	/**
	 * Metoda usuwa produkt z tabeli zapowiedzi
	 */
	 function deletePreviews($iId){
		 global $aConfig;


	 $sSql = "UPDATE ".$aConfig['tabls']['prefix']."products
						 SET is_previews='0'
						 WHERE id = ".$iId;
	 if(Common::Query($sSql) === false) {
		 $bIsError = true;
	 }

	 if($this->existShadow($iId)) {
		 $sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_shadow
						 SET is_previews='0'
						 WHERE id = ".$iId;
		 if(Common::Query($sSql) === false) {
			 $bIsError = true;
		 }
	 }

	 $sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_previews
						WHERE product_id = ".$iId;
	 if(Common::Query($sSql) === false) {
		 $bIsError = true;
	 }
	 return !$bIsError;

	 } // end of deletePreviews method
	 
	 
	/**
	 * Metoda sprawdza czy produkt o podanym id znajduje sie w tabeli zapowiedzi
	 */
	 function previewExists($iId){
	 	global $aConfig;
	 	
	 	$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."products_previews
	 					WHERE product_id = ".$iId."";
	 					
	 	return (int)Common::GetRow($sSql);	
	 } // end of previewExists method
	 
	function &getLanguages(){
		global $aConfig;
		// pobranie wszystkich jezykow
			$sSql = "SELECT id as value, language as label
							 FROM ".$aConfig['tabls']['prefix']."products_languages
							 ORDER BY language";
			$aItems= Common::GetAll($sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}
	
	function &getLanguagesM($sDb){
		global $aConfig,$pDbMgr;
		// pobranie wszystkich jezykow
			$sSql = "SELECT id as value, language as label
							 FROM ".$aConfig['tabls']['prefix']."products_languages
							 ORDER BY language";
			$aItems= $pDbMgr->GetAll($sDb,$sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}
	
	function &getYears(){
		global $aConfig;
		// pobranie wszystkich jezykow
			$sSql = "SELECT year as value, year as label
							 FROM ".$aConfig['tabls']['prefix']."products_years
							 ORDER BY year DESC";
			$aItems= Common::GetAll($sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}
	
	function &getYearsM($sDb){
		global $aConfig,$pDbMgr;
		// pobranie wszystkich jezykow
			$sSql = "SELECT year as value, year as label
							 FROM ".$aConfig['tabls']['prefix']."products_years
							 ORDER BY year DESC";
			$aItems= $pDbMgr->GetAll($sDb,$sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}
	
	function &getAttachmentTypes(){
		global $aConfig;
		// pobranie wszystkich jezykow
			$sSql = "SELECT id as value, name as label
							 FROM ".$aConfig['tabls']['prefix']."products_attachment_types
							 ORDER BY name";
			$aItems= Common::GetAll($sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}
	
	function &getAttachmentTypesM($sDb){
		global $aConfig,$pDbMgr;
		// pobranie wszystkich jezykow
			$sSql = "SELECT id as value, name as label
							 FROM ".$aConfig['tabls']['prefix']."products_attachment_types
							 ORDER BY name";
			$aItems= $pDbMgr->GetAll($sDb,$sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}
	
	function &getBindings(){
		global $aConfig;
		// pobranie wszystkich opraw
			$sSql = "SELECT id as value, binding as label
							 FROM ".$aConfig['tabls']['prefix']."products_bindings
							 ORDER BY binding";
			$aItems= Common::GetAll($sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}
	
	function &getBindingsM($sDb){
		global $aConfig,$pDbMgr;
		// pobranie wszystkich opraw
			$sSql = "SELECT id as value, binding as label
							 FROM ".$aConfig['tabls']['prefix']."products_bindings
							 ORDER BY binding";
			$aItems= $pDbMgr->GetAll($sDb,$sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}
	
	function &getDimensions(){
		global $aConfig;
		// pobranie wszystkich wymiarow
			$sSql = "SELECT id as value, dimension as label
							 FROM ".$aConfig['tabls']['prefix']."products_dimensions
							 ORDER BY dimension";
			$aItems= Common::GetAll($sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}
	
	function &getDimensionsM($sDb){
		global $aConfig,$pDbMgr;
		// pobranie wszystkich wymiarow
			$sSql = "SELECT id as value, dimension as label
							 FROM ".$aConfig['tabls']['prefix']."products_dimensions
							 ORDER BY dimension";
			$aItems= $pDbMgr->GetAll($sDb,$sSql);
			return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])),
													$aItems);
	}
	
	function getCurrentAttachment($iId){
		global $aConfig;
		$sSql = "SELECT id
					 FROM ".$aConfig['tabls']['prefix']."products_attachments
					 WHERE product_id = ".$iId;	
		return intval(Common::GetOne($sSql));
	}
	
	function getCurrentAttachmentM($sDb,$iId){
		global $aConfig,$pDbMgr;
		$sSql = "SELECT id
					 FROM ".$aConfig['tabls']['prefix']."products_attachments
					 WHERE product_id = ".$iId;	
		return intval($pDbMgr->GetOne($sDb,$sSql));
	}
	
	function getAttachmentForProduct($iId){
		global $aConfig;
		$sSql = "SELECT id AS attachment_id, attachment_type, price_netto AS attachment_price_netto, vat AS attachment_vat
					 FROM ".$aConfig['tabls']['prefix']."products_attachments
					 WHERE product_id = ".$iId;	
		return Common::GetRow($sSql);
	}
	
	function getAttachmentForProductM($sDb,$iId){
		global $aConfig,$pDbMgr;
		$sSql = "SELECT id AS attachment_id, attachment_type, price_netto AS attachment_price_netto, vat AS attachment_vat
					 FROM ".$aConfig['tabls']['prefix']."products_attachments
					 WHERE product_id = ".$iId;	
		return $pDbMgr->GetRow($sDb,$sSql);
	}

		/**
	 * Metoda zwraca aktualny cennik dla produktu 
	 * @param $iId - id produktu
	 * @return array cennik
	 */
	function &getTarrif($iId) {
		global $aConfig;
		$sSql = "SELECT promotion_id, discount, price_brutto, price_netto, discount_value, start_date, end_date, id
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1";
		return Common::GetRow($sSql);
	} // end of getTarrif()
	
	
	/**
	 * Metoda pobiera id produktu dla danego id cennika
	 *
	 * @global type $aConfig
	 * @param integer $iTarrifId
	 * @return array
	 */
	function getProductIdByTarrif($iId) {
		global $aConfig;
		
		$sSql = "SELECT product_id 
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE id = ".$iId;
		return Common::GetOne($sSql);
	}// end of getProductIdByTarrif() method
	
	
	/**
	 * Metoda zwraca aktualny cennik dla produktu z wybranej bazy
	 * @param string $sDb - symbol bazy
	 * @param $iId - id produktu
	 * @return array cennik
	 */
	function &getTarrifM($sDb,$iId) {
		global $aConfig,$pDbMgr;
		$sSql = "SELECT promotion_id, discount, price_brutto, price_netto, discount_value, start_date, end_date
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1";
		return $pDbMgr->GetRow($sDb,$sSql);
	} // end of getTarrif()
	
	/**
	 * Metoda usuwa cien (wpis w tabeli pomocniczej wyszukiwarki) produktu o podanym Id
	 *
	 * @param	integer	$iId	- Id produktu do usuniecia
	 * @return	mixed
	 */
	function deleteShadow($iId) {
		global $aConfig;
		// usuwanie cienia
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_shadow
							 WHERE id = ".$iId;
		if (($iRes = Common::Query($sSql)) === false) {
			return false;
		}
		return true;
	} // end of deleteShadow() method
	
	function existShadow($iId) {
		global $aConfig;
		// usuwanie cienia
		$sSql = "SELECT count(id) FROM ".$aConfig['tabls']['prefix']."products_shadow
							 WHERE id = ".$iId;
		return (Common::GetOne($sSql) > 0);
	}
	
	function getProductShadowAuthors($iId) {
	global $aConfig;
	$sAuthors='';
	// wybiera grupy kategorii dla tego produktu
  $sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
  				 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
  				 JOIN ".$aConfig['tabls']['prefix']."products_authors B
  				 ON A.author_id=B.id
  				 WHERE A.product_id =".$iId."
  				ORDER BY A.order_by";
  $aItems =& Common::GetCol($sSql);
  foreach ($aItems as $sItem) {
  	$sAuthors .= $sItem.' ';
  }
  if(!empty($sAuthors)) {
  	$sAuthors = substr($sAuthors,0,-1);
  }
  return $sAuthors;
} // end of getProductShadowAuthors()


	function getTarrifPrice($iId) {
		global $aConfig;
		$sSql = "SELECT price_brutto
						FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						WHERE product_id = ".$iId." AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
						ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1";
		return Common::GetOne($sSql);
	} // end of getTarrifPrice()
	
	function getProductTopCats($iId) {
	global $aConfig;
		$sSql = "SELECT A.id
						FROM menus_items A
						JOIN products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId.
						" AND A.parent_id IS NULL";
		return Common::GetCol($sSql);
	} // end of getProductTopCats()

    /**
     * Generowanie inta do sortowania na listach
     * @param $aItem - rekord z tabeli shadow
     * @return int - liczba do sortowania
     */
    function getOrderByStatusOldStyle(&$aItem){
        if($aItem['prod_status'] == '1' || $aItem['prod_status'] == '3'){
            if($aItem['shipment_time'] == '0'){
                $iGroup = 2;
                $iAvail = 1;
            } else {
                $iGroup = 2;
                $iAvail = 0;
            }
        } else {
            $iGroup = 0;
            $iAvail = 0;
        }

        $bIsPreviewsORNews = 0;
        if (intval($aItem['is_news']) > 0 || intval($aItem['is_previews']) > 0) {
            $bIsPreviewsORNews = 1;
        }
        return sprintf("%01u%01u%04u%09u",$iGroup,intval($bIsPreviewsORNews),intval($aItem['publication_year']),$aItem['id']);
    }


	/**
     * Generowanie inta do sortowania na listach
     * @param $aItem - rekord z tabeli shadow
     * @return int - liczba do sortowania
     */
    function getOrderByNr(&$aItem){
        if($aItem['prod_status'] == '1' || $aItem['prod_status'] == '3'){
            if($aItem['shipment_time'] == '0'){
                $iGroup = 2;
                $iAvail = 1;
            }
            elseif($aItem['shipment_time'] == '1'){
                $iGroup = 2;
                $iAvail = 0;
            }
            elseif($aItem['shipment_time'] == '2'){
                $iGroup = 1;
                $iAvail = 1;
            }
            elseif($aItem['shipment_time'] == '3'){
                $iGroup = 1;
                $iAvail = 0;
            } else {
                $iGroup = 0;
                $iAvail = 0;
            }
        } else {
            $iGroup = 0;
            $iAvail = 0;
        }

        $bIsPreviewsORNews = 0;
        if (intval($aItem['is_news']) > 0 || intval($aItem['is_previews']) > 0) {
            $bIsPreviewsORNews = 1;
        }
        $countInOrders = $this->getCountProductInOrders($aItem['id']);
        return sprintf("%01u%03u%01u%04u%06u",$iGroup, $countInOrders, intval($bIsPreviewsORNews),intval($aItem['publication_year']),$aItem['id']);
    }


    /**
     * @return mixed
     */
    private function getCountProductInOrders($iProductId) {

        $sSql = '
                 SELECT count(O.id)
                 FROM orders AS O
                 JOIN orders_items AS OI 
                    ON O.id = OI.order_id
                 WHERE O.order_status = "4" AND O.order_date > DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)
                 AND OI.product_id = '.$iProductId.'
                 ';
        return Common::GetOne($sSql);
    }


	/**
	 * Metoda odpublikowuje tagi produktu o podanym id
   * 
	 * @param $iId - id produktu
	 * @return bool
	 */
	function unpublishProductTags($iId) {
		global $aConfig;

		$sSql = "SELECT tag_id
							FROM " . $aConfig['tabls']['prefix'] . "products_to_tags
							WHERE product_id = " . $iId;
		$bIsError = false;
		$aTags = Common::GetCol($sSql);
		if (!empty($aTags)) {
			foreach ($aTags as $iTag) {
				$sSql = "UPDATE " . $aConfig['tabls']['prefix'] . "products_tags SET quantity = quantity - 1 WHERE id = " . $iTag;
				if (Common::Query($sSql) === false) {
					$bIsError = true;
				}
			}
		}
		return!$bIsError;
	}// end of unpublishProductTags() method
  
  /**
   * Metoda pobiera zdjęcia produktu
   * 
   * @global type $aConfig
   * @param type $iId
   * @return type
   */
	function getProductImages($iId) {
		global $aConfig;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products_images
						 WHERE product_id = " . $iId;
		return Common::GetAll($sSql);
	}// end of getProductImages() method
  
  
	/**
	 * Metoda usuwa produkt o podanym Id
	 *
	 * @param	integer	$iId	- Id produktu do usuniecia
	 * @return	mixed
	 */
	function deleteProduct($iId) {
		global $aConfig;
		// pobranie obrazkow
		$aImages = & $this->getProductImages($iId);
		// usuwanie produktu
		$sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products
							 WHERE id = " . $iId;
		if (($iRes = Common::Query($sSql)) === false) {
			return false;
		} else {
			// usuwanie zdjec i plikow produktu
			$this->deleteProductImages($aImages);
			$this->deleteShadow($iId);
			return 1;
		}
		//}*/
		return 1;
	}// end of deleteProduct() method
  
	/**
	 * Metoda usuwa zdjecia produktu
	 *
	 * @param	integer	$iId	- Id produktu
	 * @param	string	$sExt	- rozszerzenie miniaturki, sredniego i duzego zdjecia
	 * @return	void
	 */
	function deleteProductImages($aImages) {
		global $aConfig;

		if (!empty($aImages)) {
			foreach ($aImages as $aImg) {
				if (file_exists($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/' . $aImg['photo'])) {
					@unlink($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/' . $aImg['photo']);
				}
				if (file_exists($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/__t_' . $aImg['photo'])) {
					@unlink($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/__t_' . $aImg['photo']);
				}
				if (file_exists($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/__b_' . $aImg['photo'])) {
					@unlink($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/__b_' . $aImg['photo']);
				}
			}
		}
	}// end of deleteProductImages() method
  
  
	/**
	 * Metoda dodaje cien (wpis w tabeli pomocniczej wyszukiwarki) produktu o podanym Id
	 *
	 * @param	integer	$iId	- Id produktu
	 * @return	mixed
	 */
	function addShadow($iId) {
		global $aConfig;

		$sSql = "SELECT A.id, A.publisher_id, C.series_id, A.publication_year, A.edition, A.shipment_time, A.isbn, A.isbn_plain, A.isbn_10, A.isbn_13, A.ean_13, A.prod_status,
									A.price_brutto, A.name AS title, A.name2,  A.shipment_date, B.name AS publisher, B.discount_limit, D.name AS series, E.binding, A.is_news, A.is_bestseller, A.is_previews, A.packet,
									A.show_packet_year, A.type, A.product_type
					 FROM ".$aConfig['tabls']['prefix']."products A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					 	ON B.id=A.publisher_id
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series C
					 	ON A.id = C.product_id
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_series D
					 	ON C.series_id = D.id
					 	LEFT JOIN ".$aConfig['tabls']['prefix']."products_bindings E
  				 ON E.id=A.binding
					 WHERE A.published = '1'
					   AND A.product_type IN (\"".implode('", "', $aConfig['common']['allowed_product_type'])."\")
					   AND A.id = ".$iId;
		$aItem = Common::GetRow($sSql);
		if(!empty($aItem)) {

			if (!in_array($aItem['product_type'], $aConfig['common']['allowed_product_type'])) {
				// nie dodajemy jeśli nie jest w dozwolonych typach produktu
				return true;
			}

			$aItem['authors'] = $this->getProductShadowAuthors($aItem['id']);
			$aItem['price'] = $this->getTarrifPrice($aItem['id']);
			$aItem['tags'] = $this->getProductTags($aItem['id']);
			$aTopCats = $this->getProductTopCats($aItem['id']);
			
			$aItem['order_by'] = $this->getOrderByNr($aItem);
            $aItem['order_by_status_old_style'] = $this->getOrderByStatusOldStyle($aItem);

			$aItem['page_id_1'] = $aTopCats[0];
			$aItem['page_id_2'] = $aTopCats[1];
			$aItem['page_id_3'] = $aTopCats[2];
					
			if ($aItem['packet'] == '1' && $aItem['show_packet_year'] == '0') {
				// jeśli pakiet i ukryj rok publikacji to nie pokazuj tego
				$aItem['publication_year'] = 'NULL';
			}
			unset($aItem['show_packet_year']);
			unset($aItem['product_type']);
			if ($aItem['isbn_10'] == '') unset($aItem['isbn_10']);
			if ($aItem['isbn_13'] == '') unset($aItem['isbn_13']);
			if ($aItem['ean_13'] == '') unset($aItem['ean_13']);
			if (Common::Insert($aConfig['tabls']['prefix']."products_shadow", $aItem,'',false) === false) {
				return false;
			}
			return true;
		}
		return true;
	} // end of deleteShadow() method
	

	
		/**
	 * Metoda aktualizuje cien (wpis w tabeli pomocniczej wyszukiwarki) produktu o podanym Id
	 *
	 * @param	integer	$iId	- Id produktu
	 * @return	mixed
	 */
	function updateShadow($iId) {
		global $aConfig;
		$sSql = "SELECT A.id, A.publisher_id, C.series_id, A.publication_year, A.edition, A.shipment_time, 
									A.isbn, A.isbn_plain, A.isbn_10,	A.isbn_13,	A.ean_13, A.prod_status,
									A.price_brutto, A.name AS title, A.name2, A.shipment_date, B.name AS publisher, 
									B.discount_limit, D.name AS series, E.binding, A.is_news, A.is_bestseller, A.is_previews,
									A.show_packet_year, A.packet, A.type, A.product_type
					 FROM ".$aConfig['tabls']['prefix']."products A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
					 	ON B.id=A.publisher_id
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series C
					 	ON A.id = C.product_id
					 LEFT JOIN ".$aConfig['tabls']['prefix']."products_series D
					 	ON C.series_id = D.id
					 	LEFT JOIN ".$aConfig['tabls']['prefix']."products_bindings E
  				 ON E.id=A.binding
					 WHERE A.published = '1'
					 				AND A.id = ".$iId;
		$aItem = Common::GetRow($sSql);

		$sOrdBy = $this->getOrderByNr($aItem);
        $orderByStatusOldStyle = $this->getOrderByStatusOldStyle($aItem);
		
		if ($aItem['packet'] == '1' && $aItem['show_packet_year'] == '0') {
			// jeśli pakiet i ukryj rok publikacji to nie pokazuj tego
			$aItem['publication_year'] = 'NULL';
		}
		if(!empty($aItem)) {
			if (!in_array($aItem['product_type'], $aConfig['common']['allowed_product_type'])) {
				// usuwamy jeśli jest to zabroniony typ produktu
				$this->deleteShadow($iId);
			}

			$aTopCats=$this->getProductTopCats($aItem['id']);
			$aValues = array(
				'page_id_1' => $aTopCats[0],
				'page_id_2' => $aTopCats[1],
				'page_id_3' => $aTopCats[2],
				'publisher_id' => $aItem['publisher_id'],
				'series_id' => $aItem['series_id'],
				'publication_year' => $aItem['publication_year'],
				'edition' => $aItem['edition'],
				'binding' => $aItem['binding'],
				'shipment_time' => $aItem['shipment_time'],
				'isbn' => $aItem['isbn'],
				'isbn_plain' => $aItem['isbn_plain'],
				'isbn_10' => ($aItem['isbn_10'] != '' ? $aItem['isbn_10'] : 'NULL'),
				'isbn_13' => ($aItem['isbn_13'] != '' ? $aItem['isbn_13'] : 'NULL'),
				'ean_13' => ($aItem['ean_13'] != '' ? $aItem['ean_13'] : 'NULL'),
				'price_brutto' => $aItem['price_brutto'],
				'discount_limit' => $aItem['discount_limit'],
				'title' => $aItem['title'],
				'name2' => $aItem['name2'],
				'publisher' => $aItem['publisher'],
				'series' => $aItem['series'],
				'prod_status' => $aItem['prod_status'],
				'shipment_date' => (!empty($aItem['shipment_date'])&&($aItem['shipment_date']!='00-00-0000'))?$aItem['shipment_date']:'NULL',
				'price' => $this->getTarrifPrice($aItem['id']),
				'tags' => $this->getProductTags($aItem['id']),
				'is_news' => $aItem['is_news'],
				'is_bestseller' => $aItem['is_bestseller'],
				'is_previews' => $aItem['is_previews'],
				'order_by' => $sOrdBy,
                'order_by_status_old_style' => $orderByStatusOldStyle,
				'type' => $aItem['type']
			);
			if (Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues,'id = '.$aItem['id']) === false) {
				ldump($aValues);
				return false;
			}
			return true;
		}
		return true;
	} // end of updateShadow() method
	
	/**
	 * Metoda aktualizuje autorow ciena (wpis w tabeli pomocniczej wyszukiwarki) produktu o podanym Id
	 *
	 * @param	integer	$iId	- Id produktu
	 * @return	mixed
	 */
	function updateShadowAuthors($iId) {
		global $aConfig;
		$sSql = "SELECT A.id, A.prod_status,	A.name AS title
					 FROM ".$aConfig['tabls']['prefix']."products A
					 WHERE A.published = '1'
					 				AND A.id = ".$iId;
		$aItem = Common::GetRow($sSql);
		
		if(!empty($aItem) && $this->existShadow($iId)) {
			$aValues = array(
				'authors' => $this->getProductShadowAuthors($aItem['id'])
			);
			if (Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues,'id = '.$aItem['id']) === false) {
				return false;
			}
			return true;
		}
		return true;
	} // end of updateShadowAuthors() method
	
	
	/**
	 * Metoda aktualizuje tagi ciena (wpis w tabeli pomocniczej wyszukiwarki) produktu o podanym Id
	 *
	 * @param	integer	$iId	- Id produktu
	 * @return	mixed
	 */
	function updateShadowTags($iId) {
		global $aConfig;
		$sSql = "SELECT A.id, A.prod_status,	A.name AS title
					 FROM ".$aConfig['tabls']['prefix']."products A
					 WHERE A.published = '1'
					 				AND A.id = ".$iId;
		$aItem = Common::GetRow($sSql);
		
		if(!empty($aItem) && $this->existShadow($iId)) {
			$aValues = array(
				'tags' => $this->getProductTags($aItem['id'])
			);
			if (Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues,'id = '.$aItem['id']) === false) {
				return false;
			}
			return true;
		}
		return true;
	} // end of updateShadowTags() method
	
		/**
	 * Metoda zwraca informacje o wydawcy
	 * @param $iId - id wydawcy
	 * @return array (nazwa, logo)
	 */
	function getPublisherName($iId) {
		global $aConfig;
		$sSql = "SELECT name
						 FROM ".$aConfig['tabls']['prefix']."products_publishers
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getPublisher()
	
	function getPageName($iId) {
		global $aConfig;
		
		$sSql = "SELECT name
						FROM ".$aConfig['tabls']['prefix']."menus_items
						WHERE id = ".$iId;
		return Common::GetOne($sSql);
	}
	
	/**
	 * Metoda pobiera tagi rzeczowe produktu o podanym Id
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @return	string
	 */
	function &getProductTags($iId) {
		global $aConfig;
		
		// pobranie indeksow rzeczowych produktu
		$sSql = "SELECT B.tag
						 FROM ".$aConfig['tabls']['prefix']."products_to_tags A
						 JOIN ".$aConfig['tabls']['prefix']."products_tags B
						 ON B.id=A.tag_id
						 WHERE A.product_id = ".$iId;
		return @implode(', ',Common::GetCol($sSql));
	} // end of getProductTags() method
	
	function &getProductShadow($iId) {
	global $aConfig;
	// wybiera grupy kategorii dla tego produktu
  $sSql = "SELECT *
  				 FROM ".$aConfig['tabls']['prefix']."products_shadow
  				 WHERE id =".$iId;
  return Common::GetRow($sSql);
	} // end of getProductShadow()

	
	/**
	 * Metoda aktualizuje cenę produktu w pakiecie
	 *
	 * @param type $iProdId
	 * @param type $iPacketId 
	 */
	function updateProductPacketPrice($iProdId, $iPacketId) {
		global $aConfig;
		
		
		//pobranie domyslnego rabatu
		$sSql = 'SELECT additional_discount FROM ' . $aConfig['tabls']['prefix'] . 'products WHERE id='.$iPacketId;
		$fDefDiscount = Common::GetOne($sSql);
		
		// pobranie rabatu produktu
		$aTarrif = $this->getTarrif($iProdId);
		$fProdPacketDiscount = $aTarrif['discount'];
		
		$bIsErr = false;
		
		$sSql = 'SELECT price_netto, price_brutto FROM ' . $aConfig['tabls']['prefix'] . 'products WHERE id=' . $iProdId . ' LIMIT 1';
		$aProd = Common::GetRow($sSql);

		//$aProd['price_brutto']=$aTarrif['price_brutto'];
		//$aProd['price_netto']=$aTarrif['price_netto'];
		if ($fProdPacketDiscount < 0)
			$fProdPacketDiscount = $fProdPacketDiscount;

		// aktalizacja ceny samego produktu
		$aValuesA = array(
				'discount' => Common::FormatPrice2($fProdPacketDiscount),
				'price_brutto' => Common::FormatPrice2($aProd['price_brutto'] - Common::FormatPrice2($aProd['price_brutto'] * Common::FormatPrice2($fProdPacketDiscount) / 100.0)),
				'price_netto' => Common::FormatPrice2($aProd['price_netto'] - Common::FormatPrice2($aProd['price_netto'] * Common::FormatPrice2($fProdPacketDiscount) / 100.0)),
				'discount_value' => Common::FormatPrice2($aProd['price_brutto'] * Common::FormatPrice2(Common::FormatPrice2($fProdPacketDiscount) / 100.0)),
		);
		if (Common::Update($aConfig['tabls']['prefix'] . "products_tarrifs", $aValuesA, "isdefault = '0' AND product_id = " . $iProdId." AND id=".$aTarrif['id']) === false) {
			$bIsErr = true;
		}

		// aktualizacja ceny elemtu pakietu
		$aValues = array(
				//'packet_id'=>$iId,
				//'product_id'=>$iKey,
				'discount' => Common::FormatPrice2($fProdPacketDiscount),
				'discount_currency' => Common::FormatPrice2($aProd['price_brutto'] * Common::FormatPrice2(Common::FormatPrice2($fProdPacketDiscount) / 100.0)),
				'price_brutto' => Common::FormatPrice2($aValuesA['price_brutto']),
				'price_netto' => Common::FormatPrice2($aValuesA['price_netto']),
				'promo_price_brutto' => Common::FormatPrice2($aProd['price_brutto'] - Common::FormatPrice2($aProd['price_brutto'] * Common::FormatPrice2($fProdPacketDiscount + $fDefDiscount) / 100.0)),
				'promo_price_netto' => Common::FormatPrice2($aProd['price_netto'] - Common::FormatPrice2($aProd['price_netto'] * Common::FormatPrice2($fProdPacketDiscount + $fDefDiscount) / 100.0)),
		);
		if (Common::Update($aConfig['tabls']['prefix'] . "products_packets_items", $aValues, "packet_id = " . $iPacketId . ' AND product_id=' . $iProdId) === false) {
			$bIsErr = true;
		}
		
		if ($bIsErr != false) {
			return false;
		}
		return true;
	}// end of updateProductPacketPrice() method
	

	/**
	 * Metoda pobiera listę autorów dla danego pakietu i zapisuje do bazy
	 * 
	 * @param	intger	$iId	- Id pakietu
	 * @return	czy poprawne czy nie
	 */
	function updatePacketAuthorsPrices($iId, $bUpdateAuthors = true) {
		global $aConfig;

		$iDiscountAll = 0;
		$iDiscountCount = 0;

		// pobranie rabatu domyslnego
		$sSql = 'SELECT additional_discount FROM  ' . $aConfig['tabls']['prefix'] . 'products WHERE id=' . $iId;
		$iAddDiscount = Common::GetOne($sSql);
		
		// pobranie aktualnej taryfy pakietu
		$aPacketTarrif = $this->getTarrif($iId);
		
		$sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products_to_authors
							 WHERE product_id = " . $iId;
		$bIsErr = Common::Query($sSql);

		$sSql = 'SELECT product_id, price_netto, price_brutto, promo_price_netto, promo_price_brutto, discount FROM ' . $aConfig['tabls']['prefix'] . 'products_packets_items WHERE packet_id=' . $iId;

		$aItems = Common::GetAll($sSql);
		$fSumN = 0;
		$fSumB = 0;
		$fSumPN = 0;
		$fSumPB = 0;
		$iNow = time();
		foreach ($aItems as $iKey => $aItem) {
			$iDiscountAll = $iDiscountAll + $aItem['discount'] + $iAddDiscount;
			$iDiscountCount++;
			
			//zsumowanie wartosci pakietu
			$sSql = 'SELECT price_brutto, price_netto FROM  ' . $aConfig['tabls']['prefix'] . 'products WHERE id=' . $aItem['product_id'];
			$aItem2 = Common::GetRow($sSql);
			$aTarrif = $this->getTarrif($aItem['product_id']);

			$fVal1 = $aItem2['price_netto']; //Common::GetOne($sSql='SELECT price_netto FROM '.$aConfig['tabls']['prefix'].'products_tarrifs WHERE product_id='.$aItem['product_id'].' AND start_date<'.$iNow.' AND end_date>='.$iNow.' ORDER BY price_brutto ASC LIMIT 1');
			$fVal2 = $aItem2['price_brutto']; //Common::GetOne($sSql='SELECT price_brutto FROM '.$aConfig['tabls']['prefix'].'products_tarrifs WHERE product_id='.$aItem['product_id'].' AND start_date<'.$iNow.' AND end_date>='.$iNow.' ORDER BY price_brutto ASC LIMIT 1');
			$fSumN +=$aItem2['price_netto'];
			$fSumB +=$aItem2['price_brutto'];
			$fSumPN += $aItem['promo_price_netto']; // ($aItem['promo_price_netto']==0.0|1)?($fVal1-$fVal1*($aItem['discount']/100.0)):$fVal1;//$aItem['promo_price_netto'];//
			$fSumPB += $aItem['promo_price_brutto']; //($aItem['promo_price_brutto']==0.0|1)?($fVal2-$fVal2*($aItem['discount']/100.0)):$fVal2;//$aItem['promo_price_brutto'];

			// aktualizuj autorów jesli pozwala na aktualizację
			if ($bUpdateAuthors === true) {
				$sSql2 = 'SELECT author_id, role_id FROM ' . $aConfig['tabls']['prefix'] . 'products_to_authors WHERE product_id=' . $aItem['product_id'];
				$aAuthors = Common::GetAll($sSql2);
				foreach ($aAuthors as $iKey2 => $aAuthor) {
					$aValues = array(
							'product_id' => $iId,
							'author_id' => $aAuthor['author_id'],
							'role_id' => $aAuthor['role_id']
					);
					$bIsErr += Common::Insert($aConfig['tabls']['prefix'] . "products_to_authors", $aValues);
				}
				$this->updateShadowAuthors($iId);
			}
		}

		$aValues = array(
				'price_brutto' => $fSumB,
				'price_netto' => $fSumN
		);
		$aValues2 = array(
				'price_brutto' => $fSumB,
				'price' => $fSumPB
		);
		$aValues3 = array(
				'price_brutto' => $fSumPB,
				'price_netto' => $fSumPN,
				'discount_value' => $fSumB - $fSumPB,
				'discount' => ($iDiscountAll/$iDiscountCount)
		);

		$bIsErr+=Common::Update($aConfig['tabls']['prefix'] . "products", $aValues, "id = " . $iId);
		$bIsErr+=Common::Update($aConfig['tabls']['prefix'] . "products_shadow", $aValues2, "id = " . $iId);
		$bIsErr+=Common::Update($aConfig['tabls']['prefix'] . "products_tarrifs", $aValues3, "product_id = " . $iId . ' AND discount='.$aPacketTarrif['discount']);
	}// end of updatePacketAuthorsPrices() method
	
	
	/**
	 * Metoda pobiera id pakietów do których należy produkt
	 *
	 * @global type $aConfig
	 * @param type $iId
	 * @return type 
	 */
	function getPacketsProduct($iId) {
		global $aConfig;
		
		$sSql = "SELECT packet_id FROM ".$aConfig['tabls']['prefix']."products_packets_items
						WHERE product_id=".$iId;
		return Common::GetCol($sSql);
	}// end of getPacketsProduct() method
} // end of Module_Common Class
?>