<?php
/**
* Plik jezykowy modulu 'Oferta produktowa'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_oferta_produktowa']['tags']	= 'Tagi: ';
$aConfig['lang']['mod_m_oferta_produktowa']['pages']	= 'Ilość stron: ';
$aConfig['lang']['mod_m_oferta_produktowa']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['mod_m_oferta_produktowa']['isbn']	= 'ISBN: ';
$aConfig['lang']['mod_m_oferta_produktowa']['series']	= 'Seria wydawnicza: ';
$aConfig['lang']['mod_m_oferta_produktowa']['index']	= 'Indeks rzeczowy: ';
$aConfig['lang']['mod_m_oferta_produktowa']['publisher']	= 'Wydawnictwo: ';
$aConfig['lang']['mod_m_oferta_produktowa']['avalaibility']	= 'Dostępny: ';
$aConfig['lang']['mod_m_oferta_produktowa']['normal_price']	= 'Normalna cena: ';
$aConfig['lang']['mod_m_oferta_produktowa']['price']	= 'Cena: ';
$aConfig['lang']['mod_m_oferta_produktowa']['you_price']	= 'Twoja cena: ';
$aConfig['lang']['mod_m_oferta_produktowa']['old_price']	= 'Stara cena: ';
$aConfig['lang']['mod_m_oferta_produktowa']['you_old_price']	= 'Cena katalogowa: ';
$aConfig['lang']['mod_m_oferta_produktowa']['currency']	= ' zł';
$aConfig['lang']['mod_m_oferta_produktowa']['check_also']	= 'Sprawdź również:';
$aConfig['lang']['mod_m_oferta_produktowa']['issue']	= 'Wydanie ';
$aConfig['lang']['mod_m_oferta_produktowa']['add']	= 'dodaj do koszyka';
$aConfig['lang']['mod_m_oferta_produktowa']['download']	= 'Do pobrania:';
$aConfig['lang']['mod_m_oferta_produktowa']['przechowalnia'] = 'dodaj do przechowalni';
$aConfig['lang']['mod_m_oferta_produktowa']['unavalaible'] = '<strong>Produkt niedostępny</strong>';
$aConfig['lang']['mod_m_oferta_produktowa']['powiadom'] = 'powiadom znajomego';
$aConfig['lang']['mod_m_oferta_produktowa']['powiadomienie'] = 'powiadomienie';
$aConfig['lang']['mod_m_oferta_produktowa']['recipients'] = 'Adresaci';
$aConfig['lang']['mod_m_oferta_produktowa']['reviews'] = 'Recenzje';
$aConfig['lang']['mod_m_oferta_produktowa']['in_packet'] = 'W skład pakietu wchodzą:';
$aConfig['lang']['mod_m_oferta_produktowa']['title'] = "Tytuł";
$aConfig['lang']['mod_m_oferta_produktowa']['ask_content'] = "Treść";
$aConfig['lang']['mod_m_oferta_produktowa']['powiadomienie_0'] = 'Podaj swój adres e-mail, aby otrzymać wiadomość gdy nakład książki zostanie uzupełniony';
$aConfig['lang']['mod_m_oferta_produktowa']['powiadomienie_3'] = 'Podaj swój adres e-mail, aby otrzymać wiadomość gdy książka będzie już w sprzedaży';
$aConfig['lang']['mod_m_oferta_produktowa']['your_mail'] = 'Twój e-mail: ';
$aConfig['lang']['mod_m_oferta_produktowa']['email_err'] = "Podany adres e-mail jest niepoprawny!";
$aConfig['lang']['mod_m_oferta_produktowa']['no_data_err'] = "Musisz podać adres e-mail, treść oraz kod z obrazka!";
$aConfig['lang']['mod_m_oferta_produktowa']['no_positions'] = 'Twój e-mail: ';

$aConfig['lang']['mod_m_oferta_produktowa']['name2'] = "Podtytuł";
$aConfig['lang']['mod_m_oferta_produktowa']['binding'] = "Oprawa";
$aConfig['lang']['mod_m_oferta_produktowa']['dimension'] = "Wymiary";
$aConfig['lang']['mod_m_oferta_produktowa']['language'] = "Język";
$aConfig['lang']['mod_m_oferta_produktowa']['translator'] = "Tłumacz";
$aConfig['lang']['mod_m_oferta_produktowa']['publication_year'] = "Rok wydania";
$aConfig['lang']['mod_m_oferta_produktowa']['pages'] = "Liczba stron";
$aConfig['lang']['mod_m_oferta_produktowa']['edition'] = "Wydanie";
$aConfig['lang']['mod_m_oferta_produktowa']['city'] = "Miasto";
$aConfig['lang']['mod_m_oferta_produktowa']['original_language'] = "Język oryginalny";
$aConfig['lang']['mod_m_oferta_produktowa']['original_title'] = "Tytuł oryginalny";
$aConfig['lang']['mod_m_oferta_produktowa']['attachment'] = "Załącznik";
$aConfig['lang']['mod_m_oferta_produktowa']['volume_nr'] = "Tom";
$aConfig['lang']['mod_m_oferta_produktowa']['volumes'] = "Tomów";
$aConfig['lang']['mod_m_oferta_produktowa']['volume_name'] = "Nazwa tomu";
$aConfig['lang']['mod_m_oferta_produktowa']['shipment'] = "Dostępność";
$aConfig['lang']['mod_m_oferta_produktowa']['shipment_preview'] = "Dostępność od";
$aConfig['lang']['mod_m_oferta_produktowa']['legal_status_date'] = "Stan prawny na";

$aConfig['lang']['mod_m_oferta_produktowa']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_oferta_produktowa']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_oferta_produktowa']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_oferta_produktowa']['shipment_3'] = "4 - 8 tygodni. Wycena indywidualna.";

$aConfig['lang']['mod_m_oferta_produktowa']['download']	= 'Do pobrania:';
$aConfig['lang']['mod_m_oferta_produktowa']['ftype'][1]	= 'Spis treści';
$aConfig['lang']['mod_m_oferta_produktowa']['ftype'][2]	= 'Wprowadzenie';
$aConfig['lang']['mod_m_oferta_produktowa']['ftype'][3]	= 'Fragment książki';


$aConfig['lang']['mod_m_oferta_produktowa']['sorter_links']['nazwa'] = 'Według nazwy rosnąco';
$aConfig['lang']['mod_m_oferta_produktowa']['sorter_links']['nazwa_desc'] = 'Według nazwy malejąco';
$aConfig['lang']['mod_m_oferta_produktowa']['sorter_links']['order_by_status_old_style'] = 'Najnowsze';
$aConfig['lang']['mod_m_oferta_produktowa']['sorter_links']['cena'] = 'Według ceny rosnąco';
$aConfig['lang']['mod_m_oferta_produktowa']['sorter_links']['cena_desc'] = 'Według ceny malejąco';
$aConfig['lang']['mod_m_oferta_produktowa']['sorter_links']['data'] = 'Według daty rosnąco';
$aConfig['lang']['mod_m_oferta_produktowa']['sorter_links']['data_desc'] = 'Według daty malejąco';
$aConfig['lang']['mod_m_oferta_produktowa']['sort'] = 'Sposób wyświetlania:';

/*****************************************************/
$aConfig['lang']['mod_m_oferta_produktowa']['no_data']	= 'W chwili obecnej nie posiadamy żadnych produktów w tej kategorii';
$aConfig['lang']['mod_m_oferta_produktowa']['no_data_for_filters']	= 'Brak produktów dla wybranych kryteriów filtrowania';
$aConfig['lang']['mod_m_oferta_produktowa']['f_producer_header']	= 'Producent:';
$aConfig['lang']['mod_m_oferta_produktowa']['f_price_range']	= 'Zakres cen:';
$aConfig['lang']['mod_m_oferta_produktowa']['f_price_range_from']	= 'od';
$aConfig['lang']['mod_m_oferta_produktowa']['f_price_range_to']	= 'do';
$aConfig['lang']['mod_m_oferta_produktowa']['f_button']	= 'filtruj';
$aConfig['lang']['mod_m_oferta_produktowa']['no_filters']	= 'resetuj filtrowanie';
$aConfig['lang']['mod_m_oferta_produktowa']['list_name']	= 'Nazwa';
$aConfig['lang']['mod_m_oferta_produktowa']['list_sort_name']	= 'sortuj po NAZWIE';
$aConfig['lang']['mod_m_oferta_produktowa']['list_producer']	= 'Producent';
$aConfig['lang']['mod_m_oferta_produktowa']['list_sort_producer']	= 'sortuj po PRODUCENCIE';
$aConfig['lang']['mod_m_oferta_produktowa']['list_availability']	= 'Dostępność';
$aConfig['lang']['mod_m_oferta_produktowa']['list_price_brutto']	= 'Cena brutto';
$aConfig['lang']['mod_m_oferta_produktowa']['list_sort_price_brutto']	= 'sortuj po CENIE BRUTTO';
$aConfig['lang']['mod_m_oferta_produktowa']['new_product']	= '*** NOWOŚĆ ***';
$aConfig['lang']['mod_m_oferta_produktowa']['you_save']	= 'oszczędzasz:';
$aConfig['lang']['mod_m_oferta_produktowa']['pager_total_items']	= 'wszystkich produktów:';
$aConfig['lang']['mod_m_oferta_produktowa']['price_netto']	= 'cena netto:';
$aConfig['lang']['mod_m_oferta_produktowa']['price_brutto']	= 'cena brutto: ';
$aConfig['lang']['mod_m_oferta_produktowa']['you_price_brutto']	= 'Twoja cena: ';
$aConfig['lang']['mod_m_oferta_produktowa']['you_old_price_brutto']	= 'Cena katalogowa: ';
$aConfig['lang']['mod_m_oferta_produktowa']['old_price_netto']	= 'stara cena netto:';
$aConfig['lang']['mod_m_oferta_produktowa']['category_settings']	= 'category_settings';
$aConfig['lang']['mod_m_oferta_produktowa']['old_price_brutto']	= 'stara cena: ';
$aConfig['lang']['mod_m_oferta_produktowa']['discount']	= 'oszczędzasz: ';
$aConfig['lang']['mod_m_oferta_produktowa']['platnosci_pl']	= 'przy płatności \'PayU.pl\':';
$aConfig['lang']['mod_m_oferta_produktowa']['transport_price']	= 'koszt transportu:';
$aConfig['lang']['mod_m_oferta_produktowa']['add_to_cart']	= 'Kup teraz!';
$aConfig['lang']['mod_m_oferta_produktowa']['description']	= 'Opis produktu';
$aConfig['lang']['mod_m_oferta_produktowa']['attributes']	= 'Atrybuty produktu';
$aConfig['lang']['mod_m_oferta_produktowa']['unavalaible']	= '<strong>Produkt niedostępny</strong>';
$aConfig['lang']['mod_m_oferta_produktowa']['info_producer']	= 'Producent:';
$aConfig['lang']['mod_m_oferta_produktowa']['info_producer_code']	= 'Kod producenta:';
$aConfig['lang']['mod_m_oferta_produktowa']['info_product_url']	= 'Link produktu:';
$aConfig['lang']['mod_m_oferta_produktowa']['info_product_url_face']	= 'przejdź do strony produktu';
$aConfig['lang']['mod_m_oferta_produktowa']['installment']	= 'Zakupy na raty:';
$aConfig['lang']['mod_m_oferta_produktowa']['installment_period']	= 'rat po';
$aConfig['lang']['mod_m_oferta_produktowa']['installment_per_month']	= ' miesięcznie';
$aConfig['lang']['mod_m_oferta_produktowa']['installment_cost']	= 'Opłata manipulacyjna:';
$aConfig['lang']['mod_m_oferta_produktowa']['installment_description']	= 'Zasady sprzedaży ratalnej';
$aConfig['lang']['mod_m_oferta_produktowa']['installment_info']	= 'Podane ceny są cenami orientacyjnymi i mogą sie nieznacznie różnić';
$aConfig['lang']['mod_m_oferta_produktowa']['composition']	= 'W skład pakietu wchodzą';
$aConfig['lang']['mod_m_oferta_produktowa']['captcha']	= 'Przepisz kod z obrazka';

$aConfig['lang']['mod_m_oferta_produktowa']['files']	= 'Do ściągnięcia:';

$aConfig['lang']['mod_m_oferta_produktowa']['ask_email_subject']	= 'Pytanie o dostępność produktu %s';
$aConfig['lang']['mod_m_oferta_produktowa']['ask_email_body']	= 'Użytkownik o mailu: %s
Zadał pytanie o książkę %s - %s

o treści:
%s';

?>