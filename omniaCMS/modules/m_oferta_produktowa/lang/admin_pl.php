<?php
/**
* Plik jezykowy dla interfejsu modulu 'Oferta produktowa' Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['m_oferta_produktowa_ceneo_tabs']['delete_all'] = 'Usuń elementy z listy';
$aConfig['lang']['m_oferta_produktowa_ceneo_tabs']['delete_all_q'] = 'Czy napewno chcesz usunąć wybrane pozycje ?';
$aConfig['lang']['m_oferta_produktowa_ceneo_tabs']['delete_all_err'] = 'Nie wybrano pozycji';

/*---------- produkty */

$aConfig['lang']['m_oferta_produktowa']['mange_bookindex'] = "Zarządzaj indeksami dostawców";
$aConfig['lang']['m_oferta_produktowa']['actual_azymut_index'] = "Aktualny azymut bookindex";
$aConfig['lang']['m_oferta_produktowa']['old_azymut_index'] = "Poprzednie azymut bookindexy";
$aConfig['lang']['m_oferta_produktowa']['list'] = "Lista produktów";
$aConfig['lang']['m_oferta_produktowa']['f_published'] = "Stan publikacji";
$aConfig['lang']['m_oferta_produktowa']['f_all'] = "Wszystkie";
$aConfig['lang']['m_oferta_produktowa']['f_published'] = "Opublikowane";
$aConfig['lang']['m_oferta_produktowa']['f_not_published'] = "Nieopublikowane";
$aConfig['lang']['m_oferta_produktowa']['f_author'] = "Autor";
$aConfig['lang']['m_oferta_produktowa']['f_isbn'] = "ISBN";
$aConfig['lang']['m_oferta_produktowa']['f_status'] = "Status";
$aConfig['lang']['m_oferta_produktowa']['f_publ_status'] = "Stan";
$aConfig['lang']['m_oferta_produktowa']['no_attributes_groups'] = "Dla produktów tej kategorii nie ma ustanowionych żadnych grup atrybutów";
$aConfig['lang']['m_oferta_produktowa']['f_category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa']['f_publisher'] = "Wydawca";
$aConfig['lang']['m_oferta_produktowa']['f_publisher_btn'] = "Wybierz wydawców";
$aConfig['lang']['m_oferta_produktowa']['f_series'] = "Seria";
$aConfig['lang']['m_oferta_produktowa']['f_language'] = "Język";
$aConfig['lang']['m_oferta_produktowa']['f_orig_language'] = "Język oryg.";
$aConfig['lang']['m_oferta_produktowa']['f_binding'] = "Oprawa";
$aConfig['lang']['m_oferta_produktowa']['f_reviewed'] = "Przejrzane";
$aConfig['lang']['m_oferta_produktowa']['f_not_reviewed'] = "Nie przejrzane";
$aConfig['lang']['m_oferta_produktowa']['f_category_btn'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa']['f_source'] = "Akt.źr";
$aConfig['lang']['m_oferta_produktowa']['f_shipment'] = "Czas wysyłki";
$aConfig['lang']['m_oferta_produktowa']['f_source_internalj'] = "Profit J";
$aConfig['lang']['m_oferta_produktowa']['f_source_internaln'] = "Profit E";
$aConfig['lang']['m_oferta_produktowa']['f_source_internals'] = "Stock";
$aConfig['lang']['m_oferta_produktowa']['f_source_internalx'] = "Profit X";
$aConfig['lang']['m_oferta_produktowa']['f_source_azymut'] = "Azymut";
$aConfig['lang']['m_oferta_produktowa']['f_source_abe'] = "ABE";
$aConfig['lang']['m_oferta_produktowa']['f_source_helion'] = "Helion";
$aConfig['lang']['m_oferta_produktowa']['f_source_dictum'] = "Dictum";
$aConfig['lang']['m_oferta_produktowa']['f_source_siodemka'] = "Siódemka";
$aConfig['lang']['m_oferta_produktowa']['f_source_olesiejuk'] = "Olesiejuk";
$aConfig['lang']['m_oferta_produktowa']['f_source_ateneum'] = "Ateneum";
$aConfig['lang']['m_oferta_produktowa']['f_source_platon'] = "Platon";
$aConfig['lang']['m_oferta_produktowa']['f_source_panda'] = "Panda";
$aConfig['lang']['m_oferta_produktowa']['f_source_azymutobc'] = "Azymut obcojęzyczny";
$aConfig['lang']['m_oferta_produktowa']['f_source_X'] = "X";
$aConfig['lang']['m_oferta_produktowa']['f_image'] = "Z okładką";
$aConfig['lang']['m_oferta_produktowa']['f_not_image'] = "Bez okładki";
$aConfig['lang']['m_oferta_produktowa']['f_year'] = "Rok publikacji";
$aConfig['lang']['m_oferta_produktowa']['f_yearto'] = "do";
$aConfig['lang']['m_oferta_produktowa']['f_type'] = "Typ";
$aConfig['lang']['m_oferta_produktowa']['f_packet'] = "Pakiet";
$aConfig['lang']['m_oferta_produktowa']['f_regular_product'] = "Książka";
$aConfig['lang']['m_oferta_produktowa']['f_price'] = "Cena od";
$aConfig['lang']['m_oferta_produktowa']['f_price_to'] = "do";
$aConfig['lang']['m_oferta_produktowa']['f_created_from'] = "Utworzono od";
$aConfig['lang']['m_oferta_produktowa']['f_created_to'] = "do";
$aConfig['lang']['m_oferta_produktowa']['invalid_price'] = "Wpisana wartość ceny nie jest prawidłowa!";
$aConfig['lang']['m_oferta_produktowa']['books'] = "Edycja pakietu";
$aConfig['lang']['m_oferta_produktowa']['type'] = "Typ produktu";
$aConfig['lang']['m_oferta_produktowa']['type_0'] = "Wersja drukowana";
$aConfig['lang']['m_oferta_produktowa']['type_1'] = "Audiobook";
$aConfig['lang']['m_oferta_produktowa']['locations'] = "Historia Lokalizacji";

$aConfig['lang']['m_oferta_produktowa']['list_name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa']['list_netto'] = "Cena netto";
$aConfig['lang']['m_oferta_produktowa']['list_vat'] = "VAT";
$aConfig['lang']['m_oferta_produktowa']['list_price'] = "Akt. cena";
$aConfig['lang']['m_oferta_produktowa']['list_wholesale_price'] = 'Cena zakupu';
$aConfig['lang']['m_oferta_produktowa']['list_base'] = "Cena baz.";
$aConfig['lang']['m_oferta_produktowa']['list_published'] = "Opubl.";
$aConfig['lang']['m_oferta_produktowa']['list_modified'] = "Zmodyfikowano";
$aConfig['lang']['m_oferta_produktowa']['list_source'] = "Aktualne źródło";
$aConfig['lang']['m_oferta_produktowa']['profit_j_status'] = "J";
$aConfig['lang']['m_oferta_produktowa']['profit_g_status'] = "G";
$aConfig['lang']['m_oferta_produktowa']['profit_e_status'] = "E";
$aConfig['lang']['m_oferta_produktowa']['f_shipment_time'] = "Wysyłamy";
$aConfig['lang']['m_oferta_produktowa']['f_available_now'] = "Od ręki";
$aConfig['lang']['m_oferta_produktowa']['f_available_1_2_days'] = " 1 - 2 dni";
$aConfig['lang']['m_oferta_produktowa']['f_available_order'] = "tylko na zamówienie";

$aConfig['lang']['m_oferta_produktowa']['prepare_connect'] = "Przygotuj produkt do połączenia";
$aConfig['lang']['m_oferta_produktowa']['no_items'] = "Brak zdefiniowanych produktów";
$aConfig['lang']['m_oferta_produktowa']['tarrifs'] = "Cenniki";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['type'] = "Typ cennika";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['type_ceneo'] = "Ceneo";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['type_standard'] = "Standardowy";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['type_promotion'] = "Promocja";
$aConfig['lang']['m_oferta_produktowa']['attributes'] = "Atrybuty produktu";
$aConfig['lang']['m_oferta_produktowa']['authors'] = "Autorzy produktu";
$aConfig['lang']['m_oferta_produktowa']['publisher'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa']['series'] = "Seria wydawnicze";
$aConfig['lang']['m_oferta_produktowa']['add_series'] = "lub dodaj nową";
$aConfig['lang']['m_oferta_produktowa']['images'] = "Zdjęcie produktu";
$aConfig['lang']['m_oferta_produktowa']['files'] = "Pliki produktu";
$aConfig['lang']['m_oferta_produktowa']['tabs'] = "Zakładki produktu";
$aConfig['lang']['m_oferta_produktowa']['linked'] = "Polecane (powiązane) produkty";
$aConfig['lang']['m_oferta_produktowa']['editions'] = "Powiązane wydania";
$aConfig['lang']['m_oferta_produktowa']['preview'] = "Podgląd";
$aConfig['lang']['m_oferta_produktowa']['attachments'] = "Załączniki do produktu";
$aConfig['lang']['m_oferta_produktowa']['change_category'] = "Przenieś do kategorii";
$aConfig['lang']['m_oferta_produktowa']['publish'] = "Zmień stan publikacji";
$aConfig['lang']['m_oferta_produktowa']['edit'] = "Edytuj produkt";
$aConfig['lang']['m_oferta_produktowa']['delete'] = "Usuń produkt";
$aConfig['lang']['m_oferta_produktowa']['delete_q'] = "Czy na pewno usunąć wybrany produkt?";
$aConfig['lang']['m_oferta_produktowa']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa']['delete_all_q'] = "Czy na pewno usunąć wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa']['delete_all_err'] = "Nie wybrano żadnego produktu do usunięcia!";
$aConfig['lang']['m_oferta_produktowa']['tag_all'] = "Otaguj zaznaczone";
$aConfig['lang']['m_oferta_produktowa']['tag_all_q'] = "Czy na pewno otagować wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa']['tag_all_err'] = "Nie wybrano żadnego produktu do otagowania!";
$aConfig['lang']['m_oferta_produktowa']['link_all'] = "Powiąż zaznaczone";
$aConfig['lang']['m_oferta_produktowa']['link_all_q'] = "Czy na pewno powiązać wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa']['link_all_err'] = "Nie wybrano żadnego produktu do powiązania!";
$aConfig['lang']['m_oferta_produktowa']['publish_all'] = "Zmień stan publikacji";
$aConfig['lang']['m_oferta_produktowa']['publish_all_q'] = "Czy na pewno zmienić stan publikacji wybranych produktów?";
$aConfig['lang']['m_oferta_produktowa']['publish_all_err'] = "Nie wybrano żadnego produktu do zmiany stanu publikacji!";
$aConfig['lang']['m_oferta_produktowa']['review_all'] = "Przejrzyj zaznaczone";
$aConfig['lang']['m_oferta_produktowa']['review_all_q'] = "Czy na pewno ustawić jako przejrzane wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa']['review_all_err'] = "Nie wybrano żadnego produktu do przejrzenia!";
$aConfig['lang']['m_oferta_produktowa']['change_status'] = "Zmień status dostępności zaznaczonych";
$aConfig['lang']['m_oferta_produktowa']['change_status_q'] = "Czy na pewno zmienić status dostępności wybranych produkty?";
$aConfig['lang']['m_oferta_produktowa']['change_status_err'] = "Nie wybrano żadnego produktu do zmiany statusu dostępności!";

$aConfig['lang']['m_oferta_produktowa']['pkwiu'] = "PKWiU";
$aConfig['lang']['m_oferta_produktowa']['add_packet'] = "Dodaj pakiet";
$aConfig['lang']['m_oferta_produktowa']['edit_book_list'] = "Modyfikuj listę książek";
$aConfig['lang']['m_oferta_produktowa']['add'] = "Dodaj produkt";
$aConfig['lang']['m_oferta_produktowa']['go_back'] = "Powrót";
$aConfig['lang']['m_oferta_produktowa']['main_category'] = "Kategoria główna";
$aConfig['lang']['m_oferta_produktowa']['extra_categories'] = "Kategorie dodatkowe";
$aConfig['lang']['m_oferta_produktowa']['current_extra_categories'] = "Obecne kategorie dodatkowe";
$aConfig['lang']['m_oferta_produktowa']['add_cat'] = "Dodaj ->";
$aConfig['lang']['m_oferta_produktowa']['del_cat'] = "Usuń";
$aConfig['lang']['m_oferta_produktowa']['review'] = "Ustaw jako przejrzany";



$aConfig['lang']['m_oferta_produktowa']['news_status'] = "Kategoria nowości";
$aConfig['lang']['m_oferta_produktowa']['previews_status'] = "Kategoria zapowiedzi";
$aConfig['lang']['m_oferta_produktowa']['not_news'] = "Nie jest nowością";
$aConfig['lang']['m_oferta_produktowa']['not_previews'] = "Nie jest zapowiedzią";
$aConfig['lang']['m_oferta_produktowa']['header_0'] = "Dodawanie produktu";
$aConfig['lang']['m_oferta_produktowa']['header_1'] = "Edycja produktu";
$aConfig['lang']['m_oferta_produktowa']['header_tags'] = "Dodaj tagi do produktów";
$aConfig['lang']['m_oferta_produktowa']['header_status'] = "Zmień status dostępności produktów";
$aConfig['lang']['m_oferta_produktowa']['for'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa']['attention'] = "<span class=\"red\">UWAGA!</span>";
$aConfig['lang']['m_oferta_produktowa']['attention_text'] = "<span class=\"red\">Produkt występuje w pakietach.<br>WSZELKIE ZMIANY W CENIE PRODUKTU POWINNY ZOSTAĆ UWZGLĘDNIONE I ZAKTUALIZOWANE W JEGO PAKIETACH.<br /><br />Jeżeli stan publikacji produktu zostanie ustawiony na 'nieopublikowany', we wszystkich pakietach w których skład wchodzi produkt stany publikacji również zostaną ustawione na 'nieopublikowany'!</span><br>";
$aConfig['lang']['m_oferta_produktowa']['name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa']['name2'] = "Podtytuł";
$aConfig['lang']['m_oferta_produktowa']['isbn'] = "Kod ISBN";
$aConfig['lang']['m_oferta_produktowa']['isbn_10'] = "Kod ISBN 10";
$aConfig['lang']['m_oferta_produktowa']['isbn_13'] = "Kod ISBN 13";
$aConfig['lang']['m_oferta_produktowa']['ean_13'] = "Kod EAN 13";
$aConfig['lang']['m_oferta_produktowa']['binding'] = "Oprawa";
$aConfig['lang']['m_oferta_produktowa']['dimension'] = "Format";
$aConfig['lang']['m_oferta_produktowa']['language'] = "Język";
$aConfig['lang']['m_oferta_produktowa']['translator'] = "Tłumacz";
$aConfig['lang']['m_oferta_produktowa']['exact_code'] = "Kod Exact";
$aConfig['lang']['m_oferta_produktowa']['publication_year'] = "Rok publikacji";
$aConfig['lang']['m_oferta_produktowa']['fake_publication_year'] = "Ukryty rok publikacji";
$aConfig['lang']['m_oferta_produktowa']['additional_discount'] = "Dodatkowy rabat";
$aConfig['lang']['m_oferta_produktowa']['show_packet_year'] = "Pokazuj ukryty rok publikacji";
$aConfig['lang']['m_oferta_produktowa']['pages'] = "Liczba stron";
$aConfig['lang']['m_oferta_produktowa']['edition'] = "Wydanie";
$aConfig['lang']['m_oferta_produktowa']['city'] = "Miejsce wydania";
$aConfig['lang']['m_oferta_produktowa']['original_language'] = "Język oryginalny";
$aConfig['lang']['m_oferta_produktowa']['original_title'] = "Tytuł oryginalny";
$aConfig['lang']['m_oferta_produktowa']['attachment'] = "Załącznik";
$aConfig['lang']['m_oferta_produktowa']['volume_nr'] = "Tom";
$aConfig['lang']['m_oferta_produktowa']['volumes'] = "Tomów";
$aConfig['lang']['m_oferta_produktowa']['volume_name'] = "Nazwa tomu";
$aConfig['lang']['m_oferta_produktowa']['short_description'] = "Krótki opis";
$aConfig['lang']['m_oferta_produktowa']['description'] = "Pełny opis";
$aConfig['lang']['m_oferta_produktowa']['too_long_short_description'] = "Zbyt długi krótki opis. Opis powinien mieć maksymalnie %d znaków długości!";
$aConfig['lang']['m_oferta_produktowa']['tags'] = "Tagi (oddzielone ',')";
$aConfig['lang']['m_oferta_produktowa']['new_tag'] = "Dodaj tag";
$aConfig['lang']['m_oferta_produktowa']['add_tag_button'] = "Dodaj";
$aConfig['lang']['m_oferta_produktowa']['status_invalid_err'] = "Produkt nie może być jednocześnie zapowiedzią i nowością. Popraw status produktu";
$aConfig['lang']['m_oferta_produktowa']['tags_invalid_err'] = "Nieprawidłowe znaki w polu \"Tagi\"!<br>Tagi moga zawierać tylko znaki alfanumeryczne i przecinek jako separator.";
$aConfig['lang']['m_oferta_produktowa']['price_section'] = "Cena produktu";
$aConfig['lang']['m_oferta_produktowa']['price_brutto'] = "Cena bazowa (brutto)";
$aConfig['lang']['m_oferta_produktowa']['price_netto'] = "Cena netto";
$aConfig['lang']['m_oferta_produktowa']['attachment_section'] = "Załącznik";
$aConfig['lang']['m_oferta_produktowa']['attachment_type'] = "Typ załącznika";
$aConfig['lang']['m_oferta_produktowa']['attachment_price_netto'] = "Cena netto załącznika";
$aConfig['lang']['m_oferta_produktowa']['attachment_vat'] = "VAT załącznika";
$aConfig['lang']['m_oferta_produktowa']['vat'] = "VAT";
$aConfig['lang']['m_oferta_produktowa']['no_price_text'] = "Tekst zamiast ceny";
$aConfig['lang']['m_oferta_produktowa']['discount'] = "&nbsp;&nbsp;&nbsp;&nbsp;Rabat<br />książki(%)";
$aConfig['lang']['m_oferta_produktowa']['discount2'] = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rabat<br />dodatkowy(%)";
$aConfig['lang']['m_oferta_produktowa']['discount3'] = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rabat<br />całkowity(%)";
$aConfig['lang']['m_oferta_produktowa']['exclude_discount'] = "Nie uwzględniaj w rabacie ogólnym";
$aConfig['lang']['m_oferta_produktowa']['users_discount'] = "Stosuj indywidualny rabat użytkownika";
$aConfig['lang']['m_oferta_produktowa']['total_value_discount'] = "Bierz pod uwagę przy obliczaniu całkowitej wartości zamówienia";
$aConfig['lang']['m_oferta_produktowa']['show_euro'] = "Cena w Euro";
$aConfig['lang']['m_oferta_produktowa']['status_section'] = "Status produktu";
$aConfig['lang']['m_oferta_produktowa']['prod_status'] = "Status";
$aConfig['lang']['m_oferta_produktowa']['show_english_flag'] = "Książka po angielsku";
$aConfig['lang']['m_oferta_produktowa']['status_0'] = "Niedostępny";
$aConfig['lang']['m_oferta_produktowa']['status_1'] = "Dostępny";
$aConfig['lang']['m_oferta_produktowa']['status_2'] = "Wyczerpany nakład";
$aConfig['lang']['m_oferta_produktowa']['status_3'] = "Zapowiedź";
$aConfig['lang']['m_oferta_produktowa']['shipment_time'] = "Wysyłamy";
$aConfig['lang']['m_oferta_produktowa']['shipment_date'] = "Wysyłka od";
$aConfig['lang']['m_oferta_produktowa']['shipment_0'] = "Od ręki";
$aConfig['lang']['m_oferta_produktowa']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['m_oferta_produktowa']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['m_oferta_produktowa']['shipment_3'] = "tylko na zamówienie";
$aConfig['lang']['m_oferta_produktowa']['n'] = "Minimalny narzut (%)";
$aConfig['lang']['m_oferta_produktowa']['m'] = "Kwota modyfikująca";
$aConfig['lang']['m_oferta_produktowa']['p'] = "Procent kwoty odejmowany od ceny katalogowej";
$aConfig['lang']['m_oferta_produktowa']['o'] = "Minimalna ilość opini sklepu";
$aConfig['lang']['m_oferta_produktowa']['k'] = "Ignorowane sklepy przy prównywaniu";
$aConfig['lang']['m_oferta_produktowa']['source'] = "Aktualne źródło";
$aConfig['lang']['m_oferta_produktowa']['source_0'] = "Profit J";
$aConfig['lang']['m_oferta_produktowa']['source_1'] = "Profit E";
$aConfig['lang']['m_oferta_produktowa']['source_10'] = "Stock";
$aConfig['lang']['m_oferta_produktowa']['source_11'] = "Platon";
$aConfig['lang']['m_oferta_produktowa']['source_12'] = "Azymut obcojęzyczny";
$aConfig['lang']['m_oferta_produktowa']['source_13'] = "Panda";
$aConfig['lang']['m_oferta_produktowa']['source_5'] = "Ateneum";
$aConfig['lang']['m_oferta_produktowa']['source_6'] = "Profit X";
$aConfig['lang']['m_oferta_produktowa']['source_7'] = "Dictum";
$aConfig['lang']['m_oferta_produktowa']['source_8'] = "Siódemka";
$aConfig['lang']['m_oferta_produktowa']['source_9'] = "Olesiejuk";
$aConfig['lang']['m_oferta_produktowa']['source_2'] = "Azymut";
$aConfig['lang']['m_oferta_produktowa']['source_3'] = "ABE";
$aConfig['lang']['m_oferta_produktowa']['source_4'] = "Helion";
$aConfig['lang']['m_oferta_produktowa']['last_import'] = "Data ostatniego importu";
$aConfig['lang']['m_oferta_produktowa']['too_long_prod_status_text'] = "Zbyt długi opis statusu. Opis powinien mieć maksymalnie %d znaków długości!";
$aConfig['lang']['m_oferta_produktowa']['bestseller'] = "Bestseller";
$aConfig['lang']['m_oferta_produktowa']['ready_statuses'] = "Wybierz zdefiniowany opis statusu";
$aConfig['lang']['m_oferta_produktowa']['required_attributes'] = "Obowiązkowe atrybuty";
$aConfig['lang']['m_oferta_produktowa']['weight'] = "Waga";
$aConfig['lang']['m_oferta_produktowa']['legal_status_date'] = "Stan prawny na";
$aConfig['lang']['m_oferta_produktowa']['weight_info'] = "Masa";
$aConfig['lang']['m_oferta_produktowa']['prod_status_text'] = "Lub wpisz nowy";
$aConfig['lang']['m_oferta_produktowa']['series_index_section'] = "Seria wydawnicza oraz indeks rzeczowy";
$aConfig['lang']['m_oferta_produktowa']['index'] = "Indeks";
$aConfig['lang']['m_oferta_produktowa']['publ_section'] = "Stan publikacji";
$aConfig['lang']['m_oferta_produktowa']['published'] = "Opublikowany";
$aConfig['lang']['m_oferta_produktowa']['packet_in_box'] = "W boksie na głównej stronie";
$aConfig['lang']['m_oferta_produktowa']['recipients_reviews_section'] = "Adresaci oraz recenzje";
$aConfig['lang']['m_oferta_produktowa']['recipients'] = "Adresaci";
$aConfig['lang']['m_oferta_produktowa']['reviews'] = "Recenzje";
$aConfig['lang']['m_oferta_produktowa']['perdix_section'] = "Dane dla Perdixa";
$aConfig['lang']['m_oferta_produktowa']['perdix_id'] = "Kod serwisu (ID)";
$aConfig['lang']['m_oferta_produktowa']['tarrif_id'] = "Cennik (ID)";
$aConfig['lang']['m_oferta_produktowa']['attributes_section'] = "Atrybuty produktu";
$aConfig['lang']['m_oferta_produktowa']['attributes_group'] = "Grupa atrybutów";
$aConfig['lang']['m_oferta_produktowa']['curr_file'] = "Obecny plik";
$aConfig['lang']['m_oferta_produktowa']['modified'] = "Data ostatniej modyfikacji";
$aConfig['lang']['m_oferta_produktowa']['modified_by'] = "Zmodyfikował";
$aConfig['lang']['m_oferta_produktowa']['product_author'] = "Autor";
$aConfig['lang']['m_oferta_produktowa']['product_publisher'] = "Wydawnictwo";


$aConfig['lang']['m_oferta_produktowa']['add_ok'] = "Dodano produkt \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa']['add_ok-1'] = "Dodano produkt \"%s / %s\", ale nie udało się przesłać danych do Navireo !";
$aConfig['lang']['m_oferta_produktowa']['book_added_to_packet'] = "Dodano produkt do pakietu";
$aConfig['lang']['m_oferta_produktowa']['book_is_in_packet'] = "Niektóre książki znajdowały się już w pakiecie. Nie można dodać ich ponownie.";
$aConfig['lang']['m_oferta_produktowa']['book_status_err'] = "Książka \"%s\" ma nieprawidłowy status";
$aConfig['lang']['m_oferta_produktowa']['book_notadded_to_packet'] = "Wystąpił błąd podczas próby dodania produktu do pakietu<br />Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['add_err'] = "Wystąpił błąd podczas próby dodania produktu \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['edit_ok'] = "Zmieniono produkt \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa']['edit_ok-1'] = "Zmieniono produkt \"%s / %s\", ale nie udało się przesłać danych do Navireo !";
$aConfig['lang']['m_oferta_produktowa']['edit_err'] = "Wystąpił błąd podczas próby zmiany produktu \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['published_0'] = "nieopublikowany";
$aConfig['lang']['m_oferta_produktowa']['published_1'] = "opublikowany";
$aConfig['lang']['m_oferta_produktowa']['publ_ok'] = "Zmieniono stan publikacji produktu \"%s / %s\" na \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['publ_err'] = "Wystąpił błąd podczas próby zmiany stanu publikacji produktu \"%s / %s\" na \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['publ_ok-1'] = "Zmieniono stan publikacji produktu \"%s / %s\" na \"%s\", ale nie udało się przesłać zmienionych danych do Navireo ! ";
$aConfig['lang']['m_oferta_produktowa']['publ_unsorted_err'] = "Nie mozna zmienić stanu publikacji produktu \"%s\" - Produkt w kategorii nieposortowanej!";
$aConfig['lang']['m_oferta_produktowa']['tags_ok'] = "Dodano tagi \"%s\" do produktów \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['tags_err'] = "Wystąpił błąd podczas próby dodania tagów \"%s\" do produktów \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['tags_empty'] = "Nie wybrano tagów do dodania";
$aConfig['lang']['m_oferta_produktowa']['not_selected'] = "Brak przypisania";
$aConfig['lang']['m_oferta_produktowa']['tags_products_empty'] = "Nie wybrano produktów do otagowania";

$aConfig['lang']['m_oferta_produktowa']['statuses_ok'] = "Zmieniono status na \"%s\" dla produktów \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['statuses_err'] = "Wystąpił błąd podczas próby zmiany statusu na \"%s\" dla produktów \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['statuses_empty'] = "Nie wybrano statusu";
$aConfig['lang']['m_oferta_produktowa']['statuses_products_empty'] = "Nie wybrano produktów do zmiany statusu";


$aConfig['lang']['m_oferta_produktowa']['review_ok'] = "Ustawiono produkt jako przejrzany \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa']['review_err'] = "Wystąpił błąd podczas próby zmiany stanu produktu na przejrzany \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['del_ok_0']		= "Usunięto produkt %s";
$aConfig['lang']['m_oferta_produktowa']['del_ok_00']		= "Usunięto produkt";
$aConfig['lang']['m_oferta_produktowa']['del_ok_1']		= "Usunięto produkty %s";
$aConfig['lang']['m_oferta_produktowa']['del_err_0']		= "Wystąpił błąd podczas próby usunięcia produktu %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['del_err_00']		= "Wystąpił błąd podczas próby usunięcia produktu!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['del_err_1']		= "Wystąpił błąd podczas próby usunięcia produktów %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['isbn_err']		= "Istnieje już książka o podanym ISBN";

$aConfig['lang']['m_oferta_produktowa']['header_change_category'] = "Przenoszenie produktu ";
$aConfig['lang']['m_oferta_produktowa']['current_category'] = "Aktualna kategoria";
$aConfig['lang']['m_oferta_produktowa']['parent_id'] = "Nowa kategoria";
$aConfig['lang']['m_oferta_produktowa']['new_series'] = "Nowa seria";
$aConfig['lang']['m_oferta_produktowa']['category_update_ok'] = "Przeniesiono produkt \"%s\" z kategorii \"%s\" do kategorii \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['category_update_err'] = "Nie udało się przenieść produktu \"%s\" ze kategorii \"%s\" do kategorii \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['category_update_err2'] = "Nie wybrano nowej kategorii produktu!";

$aConfig['lang']['m_oferta_produktowa']['tabs_list'] = "Zakładki dla produktu ";
$aConfig['lang']['m_oferta_produktowa']['tablist_title'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa']['images_header'] = "Zdjęcie produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['image'] = "Zdjęcie";
$aConfig['lang']['m_oferta_produktowa']['image_ok'] = "Wprowadzono zmiany w zdjęciu produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa']['image_no_changes'] = "Nie wprowadzono żadnych zmian w zdjęciu produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa']['image_err_0'] = "Wystąpił błąd podczas próby zmiany zdjęcia produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa']['image_err_-4'] = "Wystąpił błąd \"%s / %s\": Nieprawidłowe rozszerzenie zdjęcia";
$aConfig['lang']['m_oferta_produktowa']['image_err_-3'] = "Wystąpił błąd \"%s / %s\": Nieprawidłowa ścieżka do katalogu docelowego";
$aConfig['lang']['m_oferta_produktowa']['image_err_-2'] = "Wystąpił błąd \"%s / %s\": Katalog docelowy nie jest zapisywalny";
$aConfig['lang']['m_oferta_produktowa']['image_err_-1'] = "Wystąpił błąd \"%s / %s\": Nie mozna przenieść zdjęcia do katalogu docelowego";

$aConfig['lang']['m_oferta_produktowa']['product_name'] = "Id/Nazwa produktu";
$aConfig['lang']['m_oferta_produktowa']['list_price_brutto'] = "Cena bruttto";
$aConfig['lang']['m_oferta_produktowa']['list_base_price'] = "Cena bazowa";
$aConfig['lang']['m_oferta_produktowa']['product_isbn'] = "ISBN";
$aConfig['lang']['m_oferta_produktowa']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa']['packet_header_0'] = "Dodawanie pakietu";
$aConfig['lang']['m_oferta_produktowa']['packet_header_1'] = "Edycja pakietu \"%s\" kategorii \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['packet_products_section'] = "Produkty wchodzące w skład pakietu";
$aConfig['lang']['m_oferta_produktowa']['button_add_product'] = "Dodaj nowe pole na produkt";
$aConfig['lang']['m_oferta_produktowa']['category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa']['product'] = "Produkt";
$aConfig['lang']['m_oferta_produktowa']['packet_add_ok'] = "Dodano pakiet \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa']['packet_add_err'] = "Wystąpił błąd podczas próby dodania pakietu \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['packet_edit_ok'] = "Zmieniono pakiet \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa']['packet_edit_err'] = "Wystąpił błąd podczas próby zmiany pakietu \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa']['packet_edit_ok2'] = "Zmieniono rabaty w pakiecie";
$aConfig['lang']['m_oferta_produktowa']['packet_edit_err2'] = "Wystąpił błąd podczas próby zmiany rabatów!<br />Spróbuj ponownie";


$aConfig['lang']['m_oferta_produktowa']['files_header'] = "Pliki produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa']['file'] = "Plik załącznika";
$aConfig['lang']['m_oferta_produktowa']['filetype'] = "Typ pliku";
$aConfig['lang']['m_oferta_produktowa']['files_ok'] = "Wprowadzono zmiany w plikach produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['files_no_changes'] = "Nie wprowadzono żadnych zmian w plikach produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['files_err_0'] = "Wystąpił błąd podczas próby zmiany plikach produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['files_err_-5'] = "Wystąpił błąd \"%s\": Nieprawidłowe rozszerzenie pliku \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['files_err_-4'] = "Wystąpił błąd \"%s\": Nieprawidłowa ścieżka do katalogu docelowego";
$aConfig['lang']['m_oferta_produktowa']['files_err_-3'] = "Wystąpił błąd \"%s\": Katalog docelowy nie jest zapisywalny";
$aConfig['lang']['m_oferta_produktowa']['files_err_-2'] = "Wystąpił błąd \"%s\": Plik o podanej nazwie \"%s\" już istnieje";
$aConfig['lang']['m_oferta_produktowa']['files_err_-1'] = "Wystąpił błąd \"%s\": Nie mozna przenieść pliku \"%s\" do katalogu docelowego";
$aConfig['lang']['m_oferta_produktowa']['no_filetype_err'] = "Brak wybranego typu dla pliku %s";
$aConfig['lang']['m_oferta_produktowa']['files_err'] = "Wystąpił błąd podczas próby zmiany plikach produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa']['save_and_back'] = "Wprowadź zmiany i wróć";

/*--------------- cenniki */
$aConfig['lang']['m_oferta_produktowa_tarrifs']['list'] = "Cenniki dla produktu (%s) %s";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['list_price'] = "Cena brutto";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['list_start_date'] = "Obowiązuje od";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['list_end_date'] = "Obowiązuje do";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['list_discount'] = "Rabat";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['type'] = "Typ";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['no_items'] = "Brak zdefiniowanych cenników";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['add'] = "Dodaj cennik";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['delete'] = "Usuń cennik";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['delete_q'] = "Czy na pewno usunąć wybrany cennik?";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['delete_all_q'] = "Czy na pewno usunąć wybrane cenniki z polecanych?";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['delete_all_err'] = "Nie wybrano żadnego cennika do usunięcia!";

$aConfig['lang']['m_oferta_produktowa_tarrifs']['price'] = "Cena bazowa brutto";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['start_date'] = "Obowiązuje od";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['end_date'] = "Obowiązuje do";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['discount'] = "Rabat(%)";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['header'] = "Edytuj/dodaj cennik dla produktu %s";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['go_back'] = "Powrót";

$aConfig['lang']['m_oferta_produktowa_tarrifs']['error_publisher_limit'] = "Wartość rabatu przekracza limit dla wydawnictwa!";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['add_ok'] = "Dodano cennik produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['add_err'] = "Nie udało się dodać cennika produktu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['edit_ok'] = "Zmieniono cennik produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['edit_err'] = "Nie udało się zmienić cennika produktu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['del_ok_0']		= "Usunięto cennik";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['del_ok_1']		= "Usunięto cenniki";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['del_err_0']		= "Nie udało się usunąć cennika!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_tarrifs']['del_err_1']		= "Nie udało się usunąć cenników!<br>Spróbuj ponownie";

/*---------- polecane produkty */
$aConfig['lang']['m_oferta_produktowa_linked']['list'] = "Lista polecanych produktów produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_linked']['list_name'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_linked']['list_category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_linked']['no_items'] = "Brak zdefiniowanych polecanych produktów";
$aConfig['lang']['m_oferta_produktowa_linked']['delete'] = "Usuń z polecanych";
$aConfig['lang']['m_oferta_produktowa_linked']['delete_q'] = "Czy na pewno usunąć wybrany produkt z polecanych?";
$aConfig['lang']['m_oferta_produktowa_linked']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_linked']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_linked']['delete_all_q'] = "Czy na pewno usunąć wybrane produkty z polecanych?";
$aConfig['lang']['m_oferta_produktowa_linked']['delete_all_err'] = "Nie wybrano żadnego produktu do usunięcia z polecanych!";
$aConfig['lang']['m_oferta_produktowa_linked']['sort'] = "Sortuj polecane";
$aConfig['lang']['m_oferta_produktowa_linked']['sort_items'] = "Sortowanie polecanych";
$aConfig['lang']['m_oferta_produktowa_linked']['go_back'] = "Powrót do listy polecanych";
$aConfig['lang']['m_oferta_produktowa_linked']['go_back_products'] = "Powrót do produktów";
$aConfig['lang']['m_oferta_produktowa_linked']['add'] = "Dodaj do polecanych";

$aConfig['lang']['m_oferta_produktowa_linked']['product_name'] = "Id/Nazwa produktu";
$aConfig['lang']['m_oferta_produktowa_linked']['product_isbn'] = "ISBN";
$aConfig['lang']['m_oferta_produktowa_linked']['product_publisher'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_linked']['product_author'] = "Autor";
$aConfig['lang']['m_oferta_produktowa_linked']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa_linked']['choose_all'] = "Wszystkie";

$aConfig['lang']['m_oferta_produktowa_linked']['header'] = "Dodawanie produktu do polecanych produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_linked']['category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_linked']['product'] = "Produkty";
$aConfig['lang']['m_oferta_produktowa_linked']['add_ok'] = "Dodano produkt \"%s\" do polecanych produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_linked']['add_err'] = "Nie udało się dodać produktu \"%s\" do polecanych produktu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_linked']['add_already_linked'] = "Wybrany produkt \"%s\" jest już polecany!<br>Wybierz inny";
$aConfig['lang']['m_oferta_produktowa_linked']['del_ok_0']		= "Usunięto produkt %s z polecanych produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_linked']['del_ok_1']		= "Usunięto produkty %s z polecanych produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_linked']['del_err_0']		= "Nie udało się usunąć produktu %s z polecanych produktu \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_linked']['del_err_1']		= "Nie udało się usunąć produktów %s z polecanych produktu \"%s\"!<br>Spróbuj ponownie";

/*---------- wydania */
$aConfig['lang']['m_oferta_produktowa_editions']['list'] = "Lista powiązanych wydań produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_editions']['list_name'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_editions']['list_category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_editions']['no_items'] = "Brak zdefiniowanych wydań produktu";
$aConfig['lang']['m_oferta_produktowa_editions']['delete'] = "Usuń z powiązanych wydań";
$aConfig['lang']['m_oferta_produktowa_editions']['delete_q'] = "Czy na pewno usunąć wybrany produkt z powiązanych wydań?";
$aConfig['lang']['m_oferta_produktowa_editions']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_editions']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_editions']['delete_all_q'] = "Czy na pewno usunąć wybrane produkty z powiązanych wydań?";
$aConfig['lang']['m_oferta_produktowa_editions']['delete_all_err'] = "Nie wybrano żadnego produktu do usunięcia z powiązanych wydań!";
$aConfig['lang']['m_oferta_produktowa_editions']['sort'] = "Sortuj powiązane wydania";
$aConfig['lang']['m_oferta_produktowa_editions']['sort_items'] = "Sortowanie powiązanych wydań";
$aConfig['lang']['m_oferta_produktowa_editions']['go_back'] = "Powrót do listy powiązanych wydań";
$aConfig['lang']['m_oferta_produktowa_editions']['go_back_products'] = "Powrót do produktów";
$aConfig['lang']['m_oferta_produktowa_editions']['add'] = "Dodaj do powiązanych wydań";

$aConfig['lang']['m_oferta_produktowa_editions']['product_name'] = "Id/Nazwa produktu";
$aConfig['lang']['m_oferta_produktowa_editions']['product_isbn'] = "ISBN";
$aConfig['lang']['m_oferta_produktowa_editions']['product_publisher'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_editions']['product_author'] = "Autor";

$aConfig['lang']['m_oferta_produktowa_editions']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa_editions']['choose_all'] = "Wszystkie";

$aConfig['lang']['m_oferta_produktowa_editions']['header'] = "Dodawanie produktu do powiązanych wydań produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_editions']['category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_editions']['product'] = "Produkty";
$aConfig['lang']['m_oferta_produktowa_editions']['add_ok'] = "Dodano produkt \"%s\" do polecanych produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_editions']['add_err'] = "Nie udało się dodać produktu \"%s\" do polecanych produktu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_editions']['add_already_linked'] = "Wybrany produkt \"%s\" jest już powiązany!<br>Wybierz inny";
$aConfig['lang']['m_oferta_produktowa_editions']['del_ok_0']		= "Usunięto produkt %s z powiązanych wydań produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_editions']['del_ok_1']		= "Usunięto produkty %s z powiązanych wydań produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_editions']['del_err_0']		= "Nie udało się usunąć produktu %s z powiązanych wydań produktu \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_editions']['del_err_1']		= "Nie udało się usunąć produktów %s z powiązanych wydań produktu \"%s\"!<br>Spróbuj ponownie";

/*---------- autorzy produktu */
$aConfig['lang']['m_oferta_produktowa_authors']['list'] = "Lista autorów produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_authors']['list_surname'] = "Nazwisko";
$aConfig['lang']['m_oferta_produktowa_authors']['send_button_1'] = "Przypisz do autora";
$aConfig['lang']['m_oferta_produktowa_authors']['list_name'] = "Imię";
$aConfig['lang']['m_oferta_produktowa_authors']['list_role'] = "Rola autora";
$aConfig['lang']['m_oferta_produktowa_authors']['list_modified'] = "Zmodyfikowano";
$aConfig['lang']['m_oferta_produktowa_authors']['f_role'] = "Rola autora:";
$aConfig['lang']['m_oferta_produktowa_authors']['f_all_roles'] = "Wszystkie role";
$aConfig['lang']['m_oferta_produktowa_authors']['no_items'] = "Brak zdefiniowanych autorów produktu";
$aConfig['lang']['m_oferta_produktowa_authors']['delete'] = "Usuń autora";
$aConfig['lang']['m_oferta_produktowa_authors']['delete_q'] = "Czy na pewno usunąć wybranego autora produktu?";
$aConfig['lang']['m_oferta_produktowa_authors']['check_all'] = "Zaznacz / odznacz wszystkich";
$aConfig['lang']['m_oferta_produktowa_authors']['delete_all'] = "Usuń zaznaczonych";
$aConfig['lang']['m_oferta_produktowa_authors']['delete_all_q'] = "Czy na pewno usunąć wybranuch autorów produktu?";
$aConfig['lang']['m_oferta_produktowa_authors']['delete_all_err'] = "Nie wybrano żadnego autora produktu do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_authors']['sort'] = "Sortuj autorów w roli";
$aConfig['lang']['m_oferta_produktowa_authors']['sort_items'] = "Sortowanie autorów w roli \"%s\"";
$aConfig['lang']['m_oferta_produktowa_authors']['go_back'] = "Powrót do listy autorów";
$aConfig['lang']['m_oferta_produktowa_authors']['go_back_products'] = "Powrót do produktów";
$aConfig['lang']['m_oferta_produktowa_authors']['add'] = "Dodaj autora(ów)";

$aConfig['lang']['m_oferta_produktowa_authors']['add_new'] = "Lub dodaj nowego";
$aConfig['lang']['m_oferta_produktowa_authors']['name'] = "Imię";
$aConfig['lang']['m_oferta_produktowa_authors']['surname'] = "Nazwisko";

$aConfig['lang']['m_oferta_produktowa_authors']['header'] = "Dodawanie autora(ów) produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_authors']['edit_header'] = "Edycja autora \"%s\" produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_authors']['category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_authors']['authors_section'] = "Autorzy";
$aConfig['lang']['m_oferta_produktowa_authors']['role'] = "Rola";
$aConfig['lang']['m_oferta_produktowa_authors']['author'] = "Autor";
$aConfig['lang']['m_oferta_produktowa_authors']['author_name'] = "Imię, nazwisko";
$aConfig['lang']['m_oferta_produktowa_authors']['button_search'] = "Szukaj";
$aConfig['lang']['m_oferta_produktowa_authors']['button_add_product_author'] = "Dodaj nowe pole na autora";
$aConfig['lang']['m_oferta_produktowa_authors']['add_ok'] = "Dodano autora(ów) %s produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa_authors']['add_err'] = "Nie udało się dodać autora(ów) produktu \"%s / %s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_authors']['edit_ok'] = "Zmodyfikowano autora %s produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa_authors']['edit_err'] = "Nie udało się zmodyfikować autora produktu \"%s / %s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_authors']['del_ok_0']		= "Usunięto autora %s produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa_authors']['del_ok_1']		= "Usunięto autorów %s produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa_authors']['del_err_0']		= "Nie udało się usunąć autora %s produktu \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_authors']['del_err_1']		= "Nie udało się usunąć autorów %s produktu \"%s / %s\"!<br>Spróbuj ponownie";

/*---------- zalaczniki do produktu */
$aConfig['lang']['m_oferta_produktowa_attachments']['list'] = "Lista załączników do produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_attachments']['list_name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_attachments']['no_items'] = "Brak zdefiniowanych załączników do produktu";
$aConfig['lang']['m_oferta_produktowa_attachments']['edit'] = "Edytuj załącznik do produktu";
$aConfig['lang']['m_oferta_produktowa_attachments']['delete'] = "Usuń załącznik do produktu";
$aConfig['lang']['m_oferta_produktowa_attachments']['delete_q'] = "Czy na pewno usunąć wybrany załącznik do produktu?";
$aConfig['lang']['m_oferta_produktowa_attachments']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_attachments']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_attachments']['delete_all_q'] = "Czy na pewno usunąć wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa_attachments']['delete_all_err'] = "Nie wybrano żadnego załącznika do produktu do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_attachments']['add'] = "Dodaj załącznik do produktu";
$aConfig['lang']['m_oferta_produktowa_attachments']['go_back'] = "Powrót do produktów";

$aConfig['lang']['m_oferta_produktowa_attachments']['header_0'] = "Dodawanie załącznika do produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_attachments']['header_1'] = "Edycja załącznik do produktu";
$aConfig['lang']['m_oferta_produktowa_attachments']['for'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_attachments']['name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_attachments']['price_brutto'] = "Cena brutto";
$aConfig['lang']['m_oferta_produktowa_attachments']['vat'] = "VAT";

$aConfig['lang']['m_oferta_produktowa_attachments']['add_ok'] = "Dodano załącznik \"%s\" do produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa_attachments']['add_err'] = "Wystąpił błąd podczas próby dodania załącznika \"%s\" do produktu \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_attachments']['edit_ok'] = "Zmieniono załącznik \"%s\" do produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa_attachments']['edit_err'] = "Wystąpił błąd podczas próby zmiany załącznika \"%s\" do produktu \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_attachments']['del_ok_0']		= "Usunięto załącznik %s do produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa_attachments']['del_ok_1']		= "Usunięto załączniki %s do produktu \"%s / %s\"";
$aConfig['lang']['m_oferta_produktowa_attachments']['del_err_0']		= "Wystąpił błąd podczas próby usunięcia załącznika %s do produktu \"%s / %s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_attachments']['del_err_1']		= "Wystąpił błąd podczas próby usunięcia załączników do produktu \"%s / %s\"!<br>Spróbuj ponownie";

/*---------- serie wydawnicze */
$aConfig['lang']['m_oferta_produktowa_series']['header_categories'] = "Powiąż kategorię główną z serią \"%s\"";
$aConfig['lang']['m_oferta_produktowa_series']['main_category'] = "Kategoria główna";
$aConfig['lang']['m_oferta_produktowa_series']['news_category'] = "Kategoria nowości";
$aConfig['lang']['m_oferta_produktowa_series']['edit_linked_ok'] = "Zmieniono powiazania serii \"%s\"";
$aConfig['lang']['m_oferta_produktowa_series']['edit_linked_err'] = "Nie udało się zmienić powiazan serii \"%s\"!<br><br>Spróbuj ponownie";


$aConfig['lang']['m_oferta_produktowa_series']['header_linked'] = "Przypisywanie do wybranej serii - %s";
$aConfig['lang']['m_oferta_produktowa_series']['selected_none'] = "Nie wybrano żadnych serii do przypisania!";
$aConfig['lang']['m_oferta_produktowa_series']['series_none'] = "Nie wybrano serii do której przypisujemy!";
$aConfig['lang']['m_oferta_produktowa_series']['linking_failed'] = "Wystąpił błąd podczas zapisu mapowań. Spróbuj ponownie.";
$aConfig['lang']['m_oferta_produktowa_series']['linking_ok'] = "Zapisano mapowanie autorów. Serie wydawnicze zostały zaktualizowane.";

$aConfig['lang']['m_oferta_produktowa_series']['add_publ'] = "Dodaj ->";
$aConfig['lang']['m_oferta_produktowa_series']['del_publ'] = "Usuń";
$aConfig['lang']['m_oferta_produktowa_series']['found_publishers'] = "Znalezione serie";
$aConfig['lang']['m_oferta_produktowa_series']['publisher_mapping'] = "Zmapowane";
$aConfig['lang']['m_oferta_produktowa_series']['publisher_search'] = "Seria";
$aConfig['lang']['m_oferta_produktowa_series']['button_search'] = "Szukaj";
$aConfig['lang']['m_oferta_produktowa_series']['mappings_section'] = "Mapowania serii";
$aConfig['lang']['m_oferta_produktowa_series']['discount_limit'] = "Limit rabatu";
$aConfig['lang']['m_oferta_produktowa_series']['recount_limit'] = "Przelicz limit";

$aConfig['lang']['m_oferta_produktowa_series']['send_button_toseries'] = "Przypisz do serii";
$aConfig['lang']['m_oferta_produktowa_series']['move_to'] = "Przepisz książki do innej serii";
$aConfig['lang']['m_oferta_produktowa_series']['header_move_to'] = "Przepisz książki z serii \"%s\"";
$aConfig['lang']['m_oferta_produktowa_series']['move_to_item'] = "Przepisz książki do serii:";
$aConfig['lang']['m_oferta_produktowa_series']['button_send'] = "Przepisz";
$aConfig['lang']['m_oferta_produktowa_series']['copy_failed'] = "Przepisanie książek nie powiodło się. Spróbuj ponownie.";
$aConfig['lang']['m_oferta_produktowa_series']['copy_ok'] = "Przepisanie książek powiodło się.";

$aConfig['lang']['m_oferta_produktowa_series']['list'] = "Lista serii wydawniczych wydawnictwa \"%s\"";
$aConfig['lang']['m_oferta_produktowa_series']['list_name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_series']['list_publisher'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_series']['f_all_categories'] = "Wszystkie kategorie";
$aConfig['lang']['m_oferta_produktowa_series']['no_items'] = "Brak zdefiniowanych serii wydawniczych";
$aConfig['lang']['m_oferta_produktowa_series']['edit'] = "Edytuj serię wydawniczą";
$aConfig['lang']['m_oferta_produktowa_series']['linked'] = "Powiązane serie";
$aConfig['lang']['m_oferta_produktowa_series']['categories'] = "Powiąż książki tej serii z kategorią główną";
$aConfig['lang']['m_oferta_produktowa_series']['delete'] = "Usuń serię wydawniczą";
$aConfig['lang']['m_oferta_produktowa_series']['delete_q'] = "Czy na pewno usunąć wybraną serię wydawniczą?";
$aConfig['lang']['m_oferta_produktowa_series']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_series']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_series']['delete_all_q'] = "Czy na pewno usunąć wybrane serie wydawnicze?";
$aConfig['lang']['m_oferta_produktowa_series']['delete_all_err'] = "Nie wybrano żadnej serii wydawniczej do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_series']['sort'] = "Sortuj serie w boksie";
$aConfig['lang']['m_oferta_produktowa_series']['sort_items'] = "Sortowanie serii wydawniczych wyróżnionych w boksie";
$aConfig['lang']['m_oferta_produktowa_series']['go_back'] = "Powrót do listy wydawnictw";
$aConfig['lang']['m_oferta_produktowa_series']['add'] = "Dodaj serię wydawniczą";
$aConfig['lang']['m_oferta_produktowa_series']['f_all'] = "Wszyscy";
$aConfig['lang']['m_oferta_produktowa_series']['f_publisher'] = "Wydawnictwo";

$aConfig['lang']['m_oferta_produktowa_series']['header_0'] = "Dodawanie serii wydawniczej wydawnictwa \"%s\"";
$aConfig['lang']['m_oferta_produktowa_series']['header_1'] = "Edycja serii wydawniczej wydawnictwa \"%s\" ";
$aConfig['lang']['m_oferta_produktowa_series']['name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_series']['publisher'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_series']['description'] = "Opis";
$aConfig['lang']['m_oferta_produktowa_series']['add_ok'] = "Dodano serię wydawniczą \"%s\"";
$aConfig['lang']['m_oferta_produktowa_series']['add_err'] = "Nie udało się dodać serii wydawniczej \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_series']['edit_ok'] = "Zmieniono serię wydawniczą \"%s\"";
$aConfig['lang']['m_oferta_produktowa_series']['edit_err'] = "Nie udało się zmienić serii wydawniczej \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_series']['series_in_use_err'] = "Nie można usunąć serii dopóki jest używana w produktach!<br><br>Usuń najpierw przypisanie produktów do serii";
$aConfig['lang']['m_oferta_produktowa_series']['del_ok_0']		= "Usunięto serię wydawniczą %s";
$aConfig['lang']['m_oferta_produktowa_series']['del_ok_1']		= "Usunięto serie wydawnicze %s";
$aConfig['lang']['m_oferta_produktowa_series']['del_err_0']		= "Nie udało się usunąć serii wydawniczej %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_series']['del_err_1']		= "Nie udało się usunąć serii wydawniczych %s!<br>Spróbuj ponownie";

/*---------- wydawnictwa */
$aConfig['lang']['m_oferta_produktowa_publishers']['send_button_topublishers'] = "Powiąż wybrane z wydawnictwem";
$aConfig['lang']['m_oferta_produktowa_publishers']['header_linked2'] = "Powiązywanie z wydawnictwem - %s";
$aConfig['lang']['m_oferta_produktowa_publishers']['selected_none'] = "Nie wybrano żadnych autorów do przypisania!";
$aConfig['lang']['m_oferta_produktowa_publishers']['author_none'] = "Nie wybrano autora do którego przypisujemy!";
$aConfig['lang']['m_oferta_produktowa_publishers']['linking_failed'] = "Wystąpił błąd podczas zapisu mapowań. Spróbuj ponownie.";
$aConfig['lang']['m_oferta_produktowa_publishers']['linking_ok'] = "Zapisano mapowanie wydawnictw.";
$aConfig['lang']['m_oferta_produktowa_publishers']['author_search'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_publishers']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa_publishers']['button_send'] = "Zapisz";
$aConfig['lang']['m_oferta_produktowa_publishers']['border_time'] = "Godzina graniczna HH:MM:SS<br /> (dla auto-odznadzania źródeł)";

$aConfig['lang']['m_oferta_produktowa_publishers']['f_limited'] = "Z limitem";
$aConfig['lang']['m_oferta_produktowa_publishers']['f_not_limited'] = "Bez limitu";
$aConfig['lang']['m_oferta_produktowa_publishers']['f_all'] = "Wszystkie";

$aConfig['lang']['m_oferta_produktowa_publishers']['f_check_external_border_time'] = "Sprawdź godzinę graniczną";
$aConfig['lang']['m_oferta_produktowa_publishers']['check_external_border_time'] = "Sprawdź godzinę graniczną dla źródła zewnętrznego - najpierw sprawdzane są źródła zewnętrzne (dla auto-odznadzania źródeł)";

$aConfig['lang']['m_oferta_produktowa_publishers']['f_border_time'] = "Godzina graniczna";
$aConfig['lang']['m_oferta_produktowa_publishers']['f_null'] = "Brak";
$aConfig['lang']['m_oferta_produktowa_publishers']['f_set'] = "Ustawiona";

$aConfig['lang']['m_oferta_produktowa_publishers']['list'] = "Lista wydawnictw";
$aConfig['lang']['m_oferta_produktowa_publishers']['list_name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_publishers']['list_created'] = "Utworzono";
$aConfig['lang']['m_oferta_produktowa_publishers']['list_created_by'] = "Utworzył";
$aConfig['lang']['m_oferta_produktowa_publishers']['list_modified'] = "Zmodyfikowano";
$aConfig['lang']['m_oferta_produktowa_publishers']['list_modified_by'] = "Zmodyfikował";
$aConfig['lang']['m_oferta_produktowa_publishers']['no_items'] = "Brak zdefiniowanych wydawnctw";
$aConfig['lang']['m_oferta_produktowa_publishers']['edit'] = "Edytuj wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_publishers']['linked'] = "Powiązane wydawnictwa";
$aConfig['lang']['m_oferta_produktowa_publishers']['discount_limit'] = "Limit rabatu";
$aConfig['lang']['m_oferta_produktowa_publishers']['recount_limit'] = "Przelicz limit";
$aConfig['lang']['m_oferta_produktowa_publishers']['ignore_discount'] = "Ignoruj limit podczas aktualizowania produktów w ceneo";
$aConfig['lang']['m_oferta_produktowa_publishers']['user_code_ignore_discount_limit'] = "Ignoruj limit rabatu po wpisaniu kodu rabatowego";
$aConfig['lang']['m_oferta_produktowa_publishers']['series'] = "Serie wydawnicze";
$aConfig['lang']['m_oferta_produktowa_publishers']['delete'] = "Usuń wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_publishers']['change'] = "Przypisz książki tego wydawnictwa do innego";
$aConfig['lang']['m_oferta_produktowa_publishers']['categories'] = "Powiąż książki tego wydawnictwa z kategorią główną";
$aConfig['lang']['m_oferta_produktowa_publishers']['delete_q'] = "Czy na pewno usunąć wybrane wydawnictwo?";
$aConfig['lang']['m_oferta_produktowa_publishers']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_publishers']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_publishers']['delete_all_q'] = "Czy na pewno usunąć wybrane wydawnictwa?";
$aConfig['lang']['m_oferta_produktowa_publishers']['delete_all_err'] = "Nie wybrano żadnego wydawnictwa do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_publishers']['add'] = "Dodaj wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_publishers']['providers_mapping_list'] = "Przypisz książki tego wydawnictwa do dostawcy";

$aConfig['lang']['m_oferta_produktowa_publishers']['header_categories'] = "Powiąż kategorię główną z wydawnictwem ";
$aConfig['lang']['m_oferta_produktowa_publishers']['main_category'] = "Kategoria główna";
$aConfig['lang']['m_oferta_produktowa_publishers']['news_category'] = "Kategoria nowości";
$aConfig['lang']['m_oferta_produktowa_publishers']['header_change'] = "Przypisz do innego wydawnictwa książki wydawnictwa";
$aConfig['lang']['m_oferta_produktowa_publishers']['change_ok'] = "Przepisano książki wydawnictwa \"%s\" do wydawnictwa \"%s\", zmienionych produktów %d";
$aConfig['lang']['m_oferta_produktowa_publishers']['change_err'] = "Nie udało się przepisać książek wydawnictwa \"%s\" do wydawnictwa \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_publishers']['incorrect_publisher_err'] = "Nie istnieje wydawca \"%s\"";


$aConfig['lang']['m_oferta_produktowa_publishers']['add_publ'] = "Dodaj ->";
$aConfig['lang']['m_oferta_produktowa_publishers']['del_publ'] = "Usuń";
$aConfig['lang']['m_oferta_produktowa_publishers']['found_publishers'] = "Znalezieni wydawcy";
$aConfig['lang']['m_oferta_produktowa_publishers']['publisher_mapping'] = "Zmapowane";
$aConfig['lang']['m_oferta_produktowa_publishers']['publisher_search'] = "Wydawca";
$aConfig['lang']['m_oferta_produktowa_publishers']['button_search'] = "Szukaj";
$aConfig['lang']['m_oferta_produktowa_publishers']['mappings_section'] = "Mapowania wydawców";
//$aConfig['lang']['m_oferta_produktowa_publishers']['publisher_search'] = "Usuń";
$aConfig['lang']['m_oferta_produktowa_publishers']['edit_linked_ok'] = "Zmieniono powiazania wydawnictwa \"%s\"";
$aConfig['lang']['m_oferta_produktowa_publishers']['edit_linked_err'] = "Nie udało się zmienić powiazan wydawnictwa \"%s\"!<br><br>Spróbuj ponownie";

$aConfig['lang']['m_oferta_produktowa_publishers_groups']['linked'] = "Powiązane wydawnictwa";


$aConfig['lang']['m_oferta_produktowa_publishers']['header_0'] = "Dodawanie wydawnictwa";
$aConfig['lang']['m_oferta_produktowa_publishers']['header_1'] = "Edycja wydawnictwa";
$aConfig['lang']['m_oferta_produktowa_publishers']['header_linked'] = "Edycja powiazanych wydawnictw dla %s";
$aConfig['lang']['m_oferta_produktowa_publishers']['name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_publishers']['url'] = "Adres WWW";
$aConfig['lang']['m_oferta_produktowa_publishers']['show_series'] = "Wyświetlaj serie";
$aConfig['lang']['m_oferta_produktowa_publishers']['omit_internal_provider'] = 'Pomiń sprawdzanie źródeł wewnętrznych <br />(automatyzacja odznaczania zamówień do źródeł)';
$aConfig['lang']['m_oferta_produktowa_publishers']['logo'] = "Logotyp wydawnictwa";
$aConfig['lang']['m_oferta_produktowa_publishers']['add_ok'] = "Dodano wydawnictwo \"%s\"";
$aConfig['lang']['m_oferta_produktowa_publishers']['add_err'] = "Nie udało się dodać wydawnictwa \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_publishers']['edit_ok'] = "Zmieniono wydawnictwo \"%s\"";
$aConfig['lang']['m_oferta_produktowa_publishers']['edit_err'] = "Nie udało się zmienić wydawnictwa \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_publishers']['in_use_err'] = "Nie można usunąć wydawnictwa dopóki jest używane w produktach!<br><br>Usuń najpierw przypisanie produktów";
$aConfig['lang']['m_oferta_produktowa_publishers']['del_ok_0']		= "Usunięto wydawnictwo %s";
$aConfig['lang']['m_oferta_produktowa_publishers']['del_ok_1']		= "Usunięto wydawnictwa %s";
$aConfig['lang']['m_oferta_produktowa_publishers']['del_err_0']		= "Nie udało się usunąć wydawnictwa %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_publishers']['del_err_1']		= "Nie udało się usunąć wydawnictw %s!<br>Spróbuj ponownie";

/*---------- promocje */
$aConfig['lang']['m_oferta_produktowa_promotions']['list'] = "Lista promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['under_zero'] = "Rabat nie może być ujemny!";
$aConfig['lang']['m_oferta_produktowa_promotions']['go_back'] = "Powrót do listy promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['book_list2'] = "Lista książek w promocji \"%s\"";
$aConfig['lang']['m_oferta_produktowa_promotions']['title'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_promotions']['discount_stake'] = "Stawka rabatu (%)";
$aConfig['lang']['m_oferta_produktowa_promotions']['category_page'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_promotions']['discount'] = "Rabat (%)";
$aConfig['lang']['m_oferta_produktowa_promotions']['date_start'] = "Początek";
$aConfig['lang']['m_oferta_produktowa_promotions']['start_time'] = "Od godz.";
$aConfig['lang']['m_oferta_produktowa_promotions']['date_end'] = "Koniec";
$aConfig['lang']['m_oferta_produktowa_promotions']['end_time'] = "Do godz.";
$aConfig['lang']['m_oferta_produktowa_promotions']['price_range'] = "Zakres cen";
$aConfig['lang']['m_oferta_produktowa_promotions']['price_from'] = "Od";
$aConfig['lang']['m_oferta_produktowa_promotions']['price_to'] = "Do";
$aConfig['lang']['m_oferta_produktowa_promotions']['author'] = "Autor";
$aConfig['lang']['m_oferta_produktowa_promotions']['publisher'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_promotions']['publication_year'] = "Rok wyd.";
$aConfig['lang']['m_oferta_produktowa_promotions']['series'] = "Seria";
$aConfig['lang']['m_oferta_produktowa_promotions']['books'] = "Lista książek w promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['show'] = "Rozwiń";
$aConfig['lang']['m_oferta_produktowa_promotions']['hide'] = "Zwiń";
$aConfig['lang']['m_oferta_produktowa_promotions']['book_status_err'] = "Książka \"%s\" ma nieprawidłowy status";
$aConfig['lang']['m_oferta_produktowa_promotions']['info001'] = "Niektóre produkty były już dodane wcześniej i nie można dodać ich ponownie.";
$aConfig['lang']['m_oferta_produktowa_promotions']['info002'] = ". Ze względu na nałożone limity rabatów niektóre produkty zostały dodane do innej wartości rabatu.";
$aConfig['lang']['m_oferta_produktowa_promotions']['info003'] = " Dla niektórych produktów dodano nowe wartości rabatu.";

$aConfig['lang']['m_oferta_produktowa_promotions']['list_id'] = "Id";
$aConfig['lang']['m_oferta_produktowa_promotions']['list_name'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_promotions']['book_list'] = "Lista książek";
$aConfig['lang']['m_oferta_produktowa_promotions']['list_pname'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_promotions']['list_base_price'] = "Cena bazowa";
$aConfig['lang']['m_oferta_produktowa_promotions']['list_price_brutto'] = "Cena brutto";
$aConfig['lang']['m_oferta_produktowa_promotions']['no_books_to_show'] = "Brak pasujących pozycji";
$aConfig['lang']['m_oferta_produktowa_promotions']['new_discount_section'] = "Rabat";
$aConfig['lang']['m_oferta_produktowa_promotions']['start_date'] = "Obowiązuje od";
$aConfig['lang']['m_oferta_produktowa_promotions']['end_date'] = "Obowiązuje do";
$aConfig['lang']['m_oferta_produktowa_promotions']['del_discount'] = "Usuń wartość rabatu";
$aConfig['lang']['m_oferta_produktowa_promotions']['list_category'] = "Dział promocji";

$aConfig['lang']['m_oferta_produktowa_promotions']['sort'] = "Sortuj promocje";
$aConfig['lang']['m_oferta_produktowa_promotions']['sort_items2'] = "Sortuj pozycje w boksie";
$aConfig['lang']['m_oferta_produktowa_promotions']['sort_items'] = "Sortowanie promocji";

$aConfig['lang']['m_oferta_produktowa_promotions']['no_items'] = "Brak zdefiniowanych promocji";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['no_items'] = "Brak książek w wybranej promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['edit'] = "Edytuj promocję";
$aConfig['lang']['m_oferta_produktowa_promotions']['delete'] = "Usuń z promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['delete_q'] = "Czy na pewno usunąć wybrany produkt z promocji?";
$aConfig['lang']['m_oferta_produktowa_promotions']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_promotions']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_promotions']['delete_all_q'] = "Czy na pewno usunąć wybrane produkty z promocji?";
$aConfig['lang']['m_oferta_produktowa_promotions']['delete_all_err'] = "Nie wybrano żadnego produktu do usunięcia z promocji!";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['delete'] = "Usuń książkę z promocji";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['delete_q'] = "Czy na pewno usunąć wybrany produkt z promocji?";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['preview'] = "Podgląd";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['remove_all_q'] = "Czy na pewno usunąć wybrane produkty z promocji?";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['remove_all_err'] = "Nie wybrano żadnego produktu do usunięcia z promocji!";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_promotions_insertBooks']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_promotions_insertBooks']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_promotions_insertBooks']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_promotions_removeVal']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_promotions_removeVal']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_promotions_removeVal']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_promotions']['search_in'] = "Dodaj z pliku";
$aConfig['lang']['m_oferta_produktowa_promotions']['add_from_file'] = "Dodaj z pliku z rabatem ";
$aConfig['lang']['m_oferta_produktowa_promotions']['add_topromo_fileError'] = "Dodawanie z pliku nie powiodło się. Sprawdź poprawność pliku i spróbuj ponownie.";
//$aConfig['lang']['m_oferta_produktowa_promotions']['sort'] = "Sortuj produkty promocyjne";
//$aConfig['lang']['m_oferta_produktowa_promotions']['sort_items'] = "Sortowanie promocji";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['go_back'] = "Powrót do listy promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['add'] = "Dodaj promocję";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['add_books'] = "Dodaj stawkę rabatu";
$aConfig['lang']['m_oferta_produktowa_promotions']['add_books'] = "Dodaj stawkę rabatu";
$aConfig['lang']['m_oferta_produktowa_promotions']['show_in_box'] = "Pokazuj w boxie";
$aConfig['lang']['m_oferta_produktowa_promotions']['ignore_publisher_limit'] = "Ignoruj limit rabatu wydawnictwa i serii wydawniczej";

$aConfig['lang']['m_oferta_produktowa_promotions']['place_on_main'] = "Wyświetlaj zazn. na głównej";
$aConfig['lang']['m_oferta_produktowa_promotions']['place_on_main_q'] = "Czy na pewno pokazywać wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa_promotions']['place_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_promotions']['unplace_on_main'] = "Nie wyświetlaj zazn. na głównej";
$aConfig['lang']['m_oferta_produktowa_promotions']['unplace_on_main_q'] = "Czy na pewno nie pokazywać wybranych produktów?";
$aConfig['lang']['m_oferta_produktowa_promotions']['unplace_on_main_err'] = "Musisz wybrać produkty!";

$aConfig['lang']['m_oferta_produktowa_promotions_books']['place_on_main'] = "Wyświetlaj zaznaczone na głównej";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['place_on_main_q'] = "Czy na pewno pokazywać wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['place_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['unplace_on_main'] = "Nie wyświetlaj zaznaczonych na głównej";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['unplace_on_main_q'] = "Czy na pewno nie pokazywać wybranych produktów?";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['unplace_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['plc_ok'] = "Zmiany zostały zapisane.";
$aConfig['lang']['m_oferta_produktowa_promotions_books']['plc_err'] = "Wystąpił błąd podczas zapisu. Spróbuj ponownie.";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['place_on_main'] = "Wyświetlaj zaznaczone na głównej";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['place_on_main_q'] = "Czy na pewno pokazywać wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['place_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['unplace_on_main'] = "Nie wyświetlaj zaznaczonych na głównej";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['unplace_on_main_q'] = "Czy na pewno nie pokazywać wybranych produktów?";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['unplace_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['plc_ok'] = "Zmiany zostały zapisane.";
$aConfig['lang']['m_oferta_produktowa_promotions_remove']['plc_err'] = "Wystąpił błąd podczas zapisu. Spróbuj ponownie.";
$aConfig['lang']['m_oferta_produktowa_promotions']['placeOnMainLabel'] = "Wyświetlaj";
$aConfig['lang']['m_oferta_produktowa_news']['placeOnMainLabel'] = "Wyświetlaj";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['placeOnMainLabel'] = "Wyświetlaj";
$aConfig['lang']['m_oferta_produktowa_previews']['placeOnMainLabel'] = "Wyświetlaj";

$aConfig['lang']['m_oferta_produktowa_promotions']['header_0'] = "Dodawanie promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['header_1'] = "Edycja promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['header_add_books'] = "Dodawanie stawek rabatów do promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['header_edit'] = "Edycja rabatu produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_promotions']['category'] = "Dział promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['product'] = "Produkt";
$aConfig['lang']['m_oferta_produktowa_promotions']['current_price_brutto'] = "Aktualna cena brutto (zł)";
$aConfig['lang']['m_oferta_produktowa_promotions']['current_promo_price_brutto'] = "Aktualna promocyjna cena brutto (zł)";
$aConfig['lang']['m_oferta_produktowa_promotions']['discount'] = "Zmień wartość rabatu";
$aConfig['lang']['m_oferta_produktowa_promotions']['discount2'] = "Rabat";
$aConfig['lang']['m_oferta_produktowa_promotions']['discount3'] = "%";
$aConfig['lang']['m_oferta_produktowa_promotions']['name'] = "Nazwa promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['page_id'] = "Dział promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['boxOpt'] = "Pokazuj w boxie";
$aConfig['lang']['m_oferta_produktowa_promotions']['add_ok'] = "Dodano promocję  \"%s\"";
$aConfig['lang']['m_oferta_produktowa_promotions']['add_err'] = "Nie udało się utworzyć promocji!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_promotions']['add_topromo_ok'] = "Dodano produkty  \"%s\" do promocji \"%s\"";
$aConfig['lang']['m_oferta_produktowa_promotions']['add_discount_ok'] = "Dodano stawki rabatu  \"%s\" do promocji \"%s\"";
$aConfig['lang']['m_oferta_produktowa_promotions']['add_topromo_err'] = "Nie udało się dodać produktów do promocji \"%s\"!<br><br>Spróbuj ponownie";

$aConfig['lang']['m_oferta_produktowa_promotions']['edit_ok'] = "Wprowadzono zmianę w promocji \"%s\"";
$aConfig['lang']['m_oferta_produktowa_promotions']['edit_err'] = "Wystąpił błąd podczas próby zmiany w promocji \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_promotions']['discount_value_err'] = "Błędna wartość rabatu - wybierz wartośc z zakresu 1-99!";
$aConfig['lang']['m_oferta_produktowa_promotions']['date_value_err'] = "Podane daty nie są prawidłowe!";
$aConfig['lang']['m_oferta_produktowa_promotions']['del_ok']		= "Usunięto promocję";
$aConfig['lang']['m_oferta_produktowa_promotions']['del_err']		= "Nie udało się usunąć promocji!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_promotions']['del_prom_ok']		= "Usunięto produkt z promocji";
$aConfig['lang']['m_oferta_produktowa_promotions']['del_prom_err']		= "Nie udało się usunąć produktu z promocji!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_promotions']['del_val_ok']		= "Usunięto wartość rabatu";
$aConfig['lang']['m_oferta_produktowa_promotions']['del_val_err']		= "Nie udało się usunąć wartości rabatu z promocji!<br>Spróbuj ponownie";


$aConfig['lang']['m_oferta_produktowa_promotions']['product_name'] = "Nazwa produktu";
$aConfig['lang']['m_oferta_produktowa_promotions']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa_promotions']['choose_all'] = "Wszystkie";
$aConfig['lang']['m_oferta_produktowa_promotions']['add_product_w_disc'] = "Dodaj produkt z rabatem ";

/*---------- zapowiedzi */
$aConfig['lang']['m_oferta_produktowa_previews']['list'] = "Lista zapowiedzi";
$aConfig['lang']['m_oferta_produktowa_previews']['list_name'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_previews']['list_category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_previews']['no_items'] = "Brak zdefiniowanych zapowiedzi";
$aConfig['lang']['m_oferta_produktowa_previews']['delete'] = "Usuń z zapowiedzi";
$aConfig['lang']['m_oferta_produktowa_previews']['delete_q'] = "Czy na pewno usunąć wybrany produkt z zapowiedzi?";
$aConfig['lang']['m_oferta_produktowa_previews']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_previews']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_previews']['delete_all_q'] = "Czy na pewno usunąć wybrane produkty z zapowiedzi?";
$aConfig['lang']['m_oferta_produktowa_previews']['delete_all_err'] = "Nie wybrano żadnego produktu do usunięcia z zapowiedzi!";
$aConfig['lang']['m_oferta_produktowa_previews']['sort'] = "Sortuj zapowiedzi";
$aConfig['lang']['m_oferta_produktowa_previews']['sort_items'] = "Sortowanie zapowiedzi";
$aConfig['lang']['m_oferta_produktowa_previews']['go_back'] = "Powrót do listy zapowiedzi";
$aConfig['lang']['m_oferta_produktowa_previews']['add'] = "Dodaj do zapowiedzi";

$aConfig['lang']['m_oferta_produktowa_previews']['header'] = "Dodawanie produktu do zapowiedzi";
$aConfig['lang']['m_oferta_produktowa_previews']['category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_previews']['product'] = "Produkty";
$aConfig['lang']['m_oferta_produktowa_previews']['add_ok'] = "Dodano produkt \"%s / %s\" do zapowiedzi";
$aConfig['lang']['m_oferta_produktowa_previews']['add_err'] = "Nie udało się dodać produktu \"%s / %s\" do zapowiedzi!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_previews']['add_already_preview'] = "Wybrany produkt \"%s / %s\" jest już zapowiedzią!<br>Wybierz inny";
$aConfig['lang']['m_oferta_produktowa_previews']['del_ok_0']		= "Usunięto produkt %s z zapowiedzi";
$aConfig['lang']['m_oferta_produktowa_previews']['del_ok_1']		= "Usunięto produkty %s z zapowiedzi";
$aConfig['lang']['m_oferta_produktowa_previews']['del_err_0']		= "Nie udało się usunąć produktu %s z zapowiedzi!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_previews']['del_err_1']		= "Nie udało się usunąć produktów %s z zapowiedzi!<br>Spróbuj ponownie";

$aConfig['lang']['m_oferta_produktowa_previews']['product_name'] = "Id/Nazwa produktu";
$aConfig['lang']['m_oferta_produktowa_previews']['product_isbn'] = "ISBN";
$aConfig['lang']['m_oferta_produktowa_previews']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa_previews']['choose_all'] = "Wszystkie";

$aConfig['lang']['m_oferta_produktowa_previews']['settings'] = "Konfiguracja limitów";
$aConfig['lang']['m_oferta_produktowa_previews']['page_id'] = "Strona zapowiedzi";




$aConfig['lang']['m_oferta_produktowa_previews']['headerSet'] = "Konfiguracja limitów cen zapowiedzi (wartości minimalne)";
$aConfig['lang']['m_oferta_produktowa_previews']['limit_433_section'] = "Dział naukowy";
$aConfig['lang']['m_oferta_produktowa_previews']['limit_477_section'] = "Dział językowy";
$aConfig['lang']['m_oferta_produktowa_previews']['limit_478_section'] = "Dział ogólny";
$aConfig['lang']['m_oferta_produktowa_previews']['limit_value'] = "Wartość limitu";
$aConfig['lang']['m_oferta_produktowa_previews']['limit_valeOk'] = "Zmiany zostały zapisane";
$aConfig['lang']['m_oferta_produktowa_previews']['limit_valueErr'] = "Wystąpił błąd! Spróbuj ponownie.";



$aConfig['lang']['m_oferta_produktowa_previews']['place_on_main'] = "Wyświetlaj zazn. na głównej";
$aConfig['lang']['m_oferta_produktowa_previews']['place_on_main_q'] = "Czy na pewno pokazywać wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa_previews']['place_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_previews']['unplace_on_main'] = "Nie wyświetlaj zazn. na głównej";
$aConfig['lang']['m_oferta_produktowa_previews']['unplace_on_main_q'] = "Czy na pewno nie pokazywać wybranych produktów?";
$aConfig['lang']['m_oferta_produktowa_previews']['unplace_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_previews']['plc_ok'] = "Zmiany zostały zapisane.";
$aConfig['lang']['m_oferta_produktowa_previews']['plc_err'] = "Wystąpił błąd podczas zapisu. Spróbuj ponownie.";



/*---------- nowosci */
$aConfig['lang']['m_oferta_produktowa_news']['list'] = "Lista nowości";
$aConfig['lang']['m_oferta_produktowa_news']['list_name'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_news']['list_category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_news']['no_items'] = "Brak zdefiniowanych nowości";
$aConfig['lang']['m_oferta_produktowa_news']['delete'] = "Usuń z nowości";
$aConfig['lang']['m_oferta_produktowa_news']['delete_q'] = "Czy na pewno usunąć wybrany produkt z nowości?";
$aConfig['lang']['m_oferta_produktowa_news']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_news']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_news']['delete_all_q'] = "Czy na pewno usunąć wybrane produkty z nowości?";
$aConfig['lang']['m_oferta_produktowa_news']['delete_all_err'] = "Nie wybrano żadnego produktu do usunięcia z nowości!";
$aConfig['lang']['m_oferta_produktowa_news']['sort'] = "Sortuj nowości";
$aConfig['lang']['m_oferta_produktowa_news']['sort_items'] = "Sortowanie nowości";
$aConfig['lang']['m_oferta_produktowa_news']['manual_sort'] = "Uwzględniaj w sortowaniu ręcznym";
$aConfig['lang']['m_oferta_produktowa_news']['f_page'] = "Strona";
$aConfig['lang']['m_oferta_produktowa_news']['go_back'] = "Powrót do listy nowości";
$aConfig['lang']['m_oferta_produktowa_news']['add'] = "Dodaj do nowości";
$aConfig['lang']['m_oferta_produktowa_news']['edit'] = "Edytuj pozycję";
$aConfig['lang']['m_oferta_produktowa_news']['preview'] = "Podgląd pozycji";
$aConfig['lang']['m_oferta_produktowa_news']['settings'] = "Konfiguracja limitów";

$aConfig['lang']['m_oferta_produktowa_news']['place_on_main'] = "Wyświetlaj zazn. na głównej";
$aConfig['lang']['m_oferta_produktowa_news']['place_on_main_q'] = "Czy na pewno pokazywać wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa_news']['place_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_news']['unplace_on_main'] = "Nie wyświetlaj zazn. na głównej";
$aConfig['lang']['m_oferta_produktowa_news']['unplace_on_main_q'] = "Czy na pewno nie pokazywać wybranych produktów?";
$aConfig['lang']['m_oferta_produktowa_news']['unplace_on_main_err'] = "Musisz wybrać produkty!";

$aConfig['lang']['m_oferta_produktowa_news']['plc_ok'] = "Zmiany zostały zapisane.";
$aConfig['lang']['m_oferta_produktowa_news']['plc_err'] = "Wystąpił błąd podczas zapisu. Spróbuj ponownie.";


$aConfig['lang']['m_oferta_produktowa_news']['header'] = "Dodawanie produktu do nowości";
$aConfig['lang']['m_oferta_produktowa_news']['header_1'] = "Edycja pozycji \"%s\" w nowościach";
$aConfig['lang']['m_oferta_produktowa_news']['category'] = "Strona nowości";
$aConfig['lang']['m_oferta_produktowa_news']['product'] = "Produkt";
$aConfig['lang']['m_oferta_produktowa_news']['page_id'] = "Strona nowości";
$aConfig['lang']['m_oferta_produktowa_news']['add_ok'] = "Dodano produkt \"%s\" do nowości";
$aConfig['lang']['m_oferta_produktowa_news']['add_err'] = "Nie udało się dodać produktu \"%s\" do nowości!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_news']['edit_ok'] = "Zmodyfikowano pozycję \"%s\" nowości";
$aConfig['lang']['m_oferta_produktowa_news']['edit_err'] = "Nie udało się zmodfyfikować pozycji \"%s\" w nowościach!<br><br>Spróbuj ponownie";

$aConfig['lang']['m_oferta_produktowa_news']['add_already_news'] = "Wybrany produkt \%s\" jest już nowością!<br>Wybierz inny";
$aConfig['lang']['m_oferta_produktowa_news']['del_ok_0']		= "Usunięto produkt %s z nowości";
$aConfig['lang']['m_oferta_produktowa_news']['del_ok_1']		= "Usunięto produkty %s z nowości";
$aConfig['lang']['m_oferta_produktowa_news']['del_err_0']		= "Nie udało się usunąć produktu %s z nowości!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_news']['del_err_1']		= "Nie udało się usunąć produktów %s z nowości!<br>Spróbuj ponownie";

$aConfig['lang']['m_oferta_produktowa_news']['product_name'] = "Id/Nazwa produktu";
$aConfig['lang']['m_oferta_produktowa_news']['product_isbn'] = "ISBN";
$aConfig['lang']['m_oferta_produktowa_news']['product_author'] = "Autor";
$aConfig['lang']['m_oferta_produktowa_news']['product_publisher'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_news']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa_news']['choose_all'] = "Wszystkie";

$aConfig['lang']['m_oferta_produktowa_news']['headerSet'] = "Konfiguracja limitów cen bestselerów (wartości minimalne)";
$aConfig['lang']['m_oferta_produktowa_news']['limit_433_section'] = "Dział naukowy";
$aConfig['lang']['m_oferta_produktowa_news']['limit_477_section'] = "Dział językowy";
$aConfig['lang']['m_oferta_produktowa_news']['limit_478_section'] = "Dział ogólny";
$aConfig['lang']['m_oferta_produktowa_news']['limit_value'] = "Wartość limitu";
$aConfig['lang']['m_oferta_produktowa_news']['limit_valeOk'] = "Zmiany zostały zapisane";
$aConfig['lang']['m_oferta_produktowa_news']['limit_valueErr'] = "Wystąpił błąd! Spróbuj ponownie.";

/*---------- bestsellery */
$aConfig['lang']['m_oferta_produktowa_bestsellers']['list'] = "Lista bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['list_name'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['list_category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['no_items'] = "Brak zdefiniowanych bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['delete'] = "Usuń z bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['delete_q'] = "Czy na pewno usunąć wybrany produkt z bestsellerów?";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['delete_all'] = "Usuń zazn.";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['delete_all_q'] = "Czy na pewno usunąć wybrane produkty z bestsellerów?";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['delete_all_err'] = "Nie wybrano żadnego produktu do usunięcia z bestsellerów!";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['sort'] = "Sortuj bestsellery";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['sort_items'] = "Sortowanie bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['go_back'] = "Powrót do listy bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['add'] = "Dodaj do bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['settings'] = "Konfiguracja limitów";

$aConfig['lang']['m_oferta_produktowa_bestsellers']['headerSet'] = "Konfiguracja limitów cen bestselerów (wartości minimalne)";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['limit_433_section'] = "Dział naukowy";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['limit_477_section'] = "Dział językowy";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['limit_478_section'] = "Dział ogólny";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['limit_value'] = "Wartość limitu";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['limit_valeOk'] = "Zmiany zostały zapisane";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['limit_valueErr'] = "Wystąpił błąd! Spróbuj ponownie.";



$aConfig['lang']['m_oferta_produktowa_bestsellers']['place_on_main'] = "Wyświetlaj zazn. na głównej";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['place_on_main_q'] = "Czy na pewno pokazywać wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['place_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['unplace_on_main'] = "Nie wyświetlaj zazn. na głównej";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['unplace_on_main_q'] = "Czy na pewno nie pokazywać wybranych produktów?";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['unplace_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['plc_ok'] = "Zmiany zostały zapisane.";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['plc_err'] = "Wystąpił błąd podczas zapisu. Spróbuj ponownie.";

$aConfig['lang']['m_oferta_produktowa_bestsellers']['header'] = "Dodawanie produktu do bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['category'] = "Strona bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['product'] = "Produkty";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['page_id'] = "Strona bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['add_ok'] = "Dodano produkt \"%s / %s\" do bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['add_err'] = "Nie udało się dodać produktu \"%s / %s\" do bestsellerów!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['add_already_bestseller'] = "Wybrany produkt \"%s / %s\" jest już bestsellerem!<br>Wybierz inny";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['del_ok_0']		= "Usunięto produkt %s z bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['del_ok_1']		= "Usunięto produkty %s z bestsellerów";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['del_err_0']		= "Nie udało się usunąć produktu %s z bestsellerów!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['del_err_1']		= "Nie udało się usunąć produktów %s z bestsellerów!<br>Spróbuj ponownie";

$aConfig['lang']['m_oferta_produktowa_bestsellers']['product_name'] = "Id/Nazwa produktu";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['product_isbn'] = "ISBN";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['product_author'] = "Autor";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['product_publisher'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa_bestsellers']['choose_all'] = "Wszystkie";

/*---------- opisy statusow */
$aConfig['lang']['m_oferta_produktowa_statuses']['list'] = "Lista opisów statusów produktów";
$aConfig['lang']['m_oferta_produktowa_statuses']['list_status_text'] = "Opis";
$aConfig['lang']['m_oferta_produktowa_statuses']['no_items'] = "Brak zdefiniowanych opisów statusów produktów";
$aConfig['lang']['m_oferta_produktowa_statuses']['edit'] = "Edytuj opis statusu";
$aConfig['lang']['m_oferta_produktowa_statuses']['delete'] = "Usuń opis statusu";
$aConfig['lang']['m_oferta_produktowa_statuses']['delete_q'] = "Czy na pewno usunąć wybrany opis statusu?";
$aConfig['lang']['m_oferta_produktowa_statuses']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_statuses']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_statuses']['delete_all_q'] = "Czy na pewno usunąć wybrane opisy statusów?";
$aConfig['lang']['m_oferta_produktowa_statuses']['delete_all_err'] = "Nie wybrano żadnego opisu statusu do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_statuses']['add'] = "Dodaj opis statusu";

$aConfig['lang']['m_oferta_produktowa_statuses']['header_0'] = "Dodawanie opisu statusu produktów";
$aConfig['lang']['m_oferta_produktowa_statuses']['header_1'] = "Edycja opisu statusu produktów";
$aConfig['lang']['m_oferta_produktowa_statuses']['status_text'] = "Opis";
$aConfig['lang']['m_oferta_produktowa_statuses']['too_long_status_text'] = "Zbyt długi opis statusu. Opis powinien mieć maksymalnie %d znaków długości!";

$aConfig['lang']['m_oferta_produktowa_statuses']['add_ok'] = "Dodano opis statusu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_statuses']['add_err'] = "Nie udało się dodać opisu statusu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_statuses']['edit_ok'] = "Zmieniono opis statusu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_statuses']['edit_err'] = "Nie udało się zmienić opisu statusu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_statuses']['del_ok_0']		= "Usunięto opis statusu %s";
$aConfig['lang']['m_oferta_produktowa_statuses']['del_ok_1']		= "Usunięto opisy statusów %s";
$aConfig['lang']['m_oferta_produktowa_statuses']['del_err_0']		= "Nie udało się usunąć opisu statusu %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_statuses']['del_err_1']		= "Nie udało się usunąć opisów statusów %s!<br>Spróbuj ponownie";

/*---------- statusy dostepnosci produktow */
$aConfig['lang']['m_oferta_produktowa_availability']['list'] = "Lista statusów dostępności produktów";
$aConfig['lang']['m_oferta_produktowa_availability']['list_name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_availability']['list_symbol'] = "Symbol";
$aConfig['lang']['m_oferta_produktowa_availability']['list_description'] = "Opis";
$aConfig['lang']['m_oferta_produktowa_availability']['no_items'] = "Brak zdefiniowanych statusów dostępności produktów";

$aConfig['lang']['m_oferta_produktowa_availability']['edit'] = "Edytuj status dostępności";
$aConfig['lang']['m_oferta_produktowa_availability']['delete'] = "Usuń status dostępności";
$aConfig['lang']['m_oferta_produktowa_availability']['delete_q'] = "Czy na pewno usunąć wybrany status dostępności produktu?";
$aConfig['lang']['m_oferta_produktowa_availability']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_availability']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_availability']['delete_all_q'] = "Czy na pewno usunąć wybrane statusy dostępności produktów?";
$aConfig['lang']['m_oferta_produktowa_availability']['delete_all_err'] = "Nie wybrano żadnego statusu dostępności produktów do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_availability']['add'] = "Dodaj status";
$aConfig['lang']['m_oferta_produktowa_availability']['del_ok_0']		= "Status dostępności produktów %s został usunięty";
$aConfig['lang']['m_oferta_produktowa_availability']['del_ok_1']		= "Statusy dostępności produktów %s zostały usunięte";
$aConfig['lang']['m_oferta_produktowa_availability']['del_err_0']		= "Nie udało się usunąć statusu dostępności produktów %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_availability']['del_err_1']		= "Nie udało się usunąć statusów dostępności produktów %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_availability']['del_err2_0']		= "Istnieją produkty ze statusem dostępności produktów %s!<br>Usuń najpierw używające go produkty";
$aConfig['lang']['m_oferta_produktowa_availability']['del_err2_1']		= "Istnieją produkty ze statusami dostępności produktów %s!<br>Usuń najpierw używające ich produkty";

$aConfig['lang']['m_oferta_produktowa_availability']['header_0'] = "Dodawanie statusu dostępności produktów";
$aConfig['lang']['m_oferta_produktowa_availability']['header_1'] = "Edycja statusu dostępności produktów";
$aConfig['lang']['m_oferta_produktowa_availability']['name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_availability']['symbol'] = "Symbol ([a-z0-9_-] min. 3 znaki)";
$aConfig['lang']['m_oferta_produktowa_availability']['synchronize'] = "Synchronizuj z nazwą";
$aConfig['lang']['m_oferta_produktowa_availability']['synchronize_now'] = "Synchronizuj teraz";
$aConfig['lang']['m_oferta_produktowa_availability']['description'] = "Krótki opis";
$aConfig['lang']['m_oferta_produktowa_availability']['too_long_description'] = "Zbyt długi opis. Opis powinien mieć maksymalnie %d znaków długości!";

$aConfig['lang']['m_oferta_produktowa_availability']['add_ok'] = "Status dostępności produktów \"%s (%s)\" został dodany";
$aConfig['lang']['m_oferta_produktowa_availability']['add_err'] = "Nie udało się dodać statusu dostępności produktów \"%s (%s)\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_availability']['add_err2'] = "Statusu dostępności produktów o symbolu \"%s\" już istnieje!<br>Wybierz inny symbol";
$aConfig['lang']['m_oferta_produktowa_availability']['edit_ok'] = "Zmiany w ustawieniach statusu dostępności produktów \"%s (%s)\" zostały wprowadzone";
$aConfig['lang']['m_oferta_produktowa_availability']['edit_err'] = "Nie udało się wprowadzić zmian w statusie dostępności produktów \"%s (%s)\"!<br><br>Spróbuj ponownie";


/*---------- ustawienia oferta produktowa */
$aConfig['lang']['m_oferta_produktowa_config']['header'] = "Konfiguracja domyślna stron modułu \"".$aConfig['lang']['c_modules']['m_aktualnosci']."\"";
$aConfig['lang']['m_oferta_produktowa_config']['edit_err'] = "Wystąpił błąd podczas aktualizacji domyślnej konfiguracji modułu \"".$aConfig['lang']['c_modules']['m_aktualnosci']."\"";
$aConfig['lang']['m_oferta_produktowa_config']['edit_ok'] = "Zaktualizowano domyślną konfigurację modułu \"".$aConfig['lang']['c_modules']['m_oferta_produktowa']."\"";


/*---------- ustawienia oferty produktowej */
$aConfig['lang']['m_oferta_produktowa_settings']['settings'] = "Konfiguracja strony oferty produktowej";
$aConfig['lang']['m_oferta_produktowa_settings']['templates'] = "Szablony";
$aConfig['lang']['m_oferta_produktowa_settings']['list_template'] = "Szablon listy";
$aConfig['lang']['m_oferta_produktowa_settings']['item_template'] = "Szablon produktu";
$aConfig['lang']['m_oferta_produktowa_settings']['photos'] = "Zdjęcia";
$aConfig['lang']['m_oferta_produktowa_settings']['thumb_size'] = "Rozmiar miniaturki zdjęcia";
$aConfig['lang']['m_oferta_produktowa_settings']['small_size'] = "Rozmiar małego zdjęcia";
$aConfig['lang']['m_oferta_produktowa_settings']['medium_size'] = "Rozmiar średniego zdjęcia";
$aConfig['lang']['m_oferta_produktowa_settings']['big_size'] = "Rozmiar dużego zdjęcia";
$aConfig['lang']['m_oferta_produktowa_settings']['azymut_mappings'] = "Mapowanie kategorii Azymut";
$aConfig['lang']['m_oferta_produktowa_settings']['ateneum_mappings'] = "Mapowanie kategorii Ateneum";
$aConfig['lang']['m_oferta_produktowa_settings']['old_abe_mappings'] = "STARE - Mapowanie kategorii ABE";
$aConfig['lang']['m_oferta_produktowa_settings']['abe_mappings'] = "Mapowanie kategorii ABE";
$aConfig['lang']['m_oferta_produktowa_settings']['helion_mappings'] = "Mapowanie kategorii HELION";
$aConfig['lang']['m_oferta_produktowa_settings']['dictum_mappings'] = "Mapowanie kategorii Dictum";
$aConfig['lang']['m_oferta_produktowa_settings']['siodemka_mappings'] = "Mapowanie kategorii Siódemka";
$aConfig['lang']['m_oferta_produktowa_settings']['olesiejuk_mappings'] = "Mapowanie kategorii Olesiejuk";
$aConfig['lang']['m_oferta_produktowa_settings']['platon_mappings'] = "Mapowanie kategorii Platon";
$aConfig['lang']['m_oferta_produktowa_settings']['panda_mappings'] = "Mapowanie kategorii Panda";
$aConfig['lang']['m_oferta_produktowa_settings']['azymutobc_mappings'] = "Mapowanie kategorii Azymut obcojęzyczny";
//$aConfig['lang']['m_oferta_produktowa_settings']['helion_categories'] = "Mapowanie kategorii HELION";
$aConfig['lang']['m_oferta_produktowa_settings']['items_per_page'] = "Produktów na stronie";
$aConfig['lang']['m_oferta_produktowa_settings']['photo_desc_shortcut'] = "Liczba znaków w skrócie opisu zdjęć (0 - 255)";
$aConfig['lang']['m_oferta_produktowa_settings']['comments'] = "Komentarze";
$aConfig['lang']['m_oferta_produktowa_settings']['allow_comments'] = "Włącz komentowanie produktów";
$aConfig['lang']['m_oferta_produktowa_settings']['comments_per_page'] = "Komentarzy na stronie";
$aConfig['lang']['m_oferta_produktowa_settings']['comment_length'] = "Długość komentarza";
$aConfig['lang']['m_oferta_produktowa_settings']['publish_comment'] = "Komentarze bez zatwierdzania";
$aConfig['lang']['m_oferta_produktowa_settings']['email'] = "Informacja o nowym komentarzu na adres";

/*---------- ustawienia oferty produktowej - promocje, nowosci*/
$aConfig['lang']['m_oferta_produktowa_promocje_settings']['settings'] = "Konfiguracja strony oferty produktowej - Promocje";
$aConfig['lang']['m_oferta_produktowa_promocje_settings']['template'] = "Szablon listy";
$aConfig['lang']['m_oferta_produktowa_promocje_settings']['items_per_page'] = "Produktów na stronie";

/*---------- ustawienia boksow oferty produktowej */
$aConfig['lang']['m_oferta_produktowa_box_settings']['offer_menu'] = "Menu oferty";
$aConfig['lang']['m_oferta_produktowa_box_settings']['items_in_box'] = "Produktów w boksie";
$aConfig['lang']['m_oferta_produktowa_box_settings']['page'] = "Strona oferty";

/*---------- box promocje, aktualnosci */
$aConfig['lang']['m_oferta_produktowa_common_box_settings']['page_id'] = "Strona";
$aConfig['lang']['m_oferta_produktowa_common_box_settings']['items_in_box'] = "Produktów w boksie";

/*---------- autorzy */

$aConfig['lang']['m_oferta_produktowa_authors_list']['add_publ'] = "Dodaj ->";
$aConfig['lang']['m_oferta_produktowa_authors_list']['del_publ'] = "Usuń";
$aConfig['lang']['m_oferta_produktowa_authors_list']['found_publishers'] = "Znalezieni autorzy";
$aConfig['lang']['m_oferta_produktowa_authors_list']['publisher_mapping'] = "Zmapowani";
$aConfig['lang']['m_oferta_produktowa_authors_list']['publisher_search'] = "Autor";
$aConfig['lang']['m_oferta_produktowa_authors_list']['button_search'] = "Szukaj";
$aConfig['lang']['m_oferta_produktowa_authors_list']['mappings_section'] = "Mapowania autorów";


$aConfig['lang']['m_oferta_produktowa_authors_list']['list'] = "Lista autorów";
$aConfig['lang']['m_oferta_produktowa_authors_list']['list_title'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_authors_list']['list_surname'] = "Nazwisko";
$aConfig['lang']['m_oferta_produktowa_authors_list']['list_name'] = "Imię";
$aConfig['lang']['m_oferta_produktowa_authors_list']['helion'] = "Helion";
$aConfig['lang']['m_oferta_produktowa_authors_list']['no_items'] = "Brak zdefiniowanych autorów";
$aConfig['lang']['m_oferta_produktowa_authors_list']['preview'] = "Podgląd";
$aConfig['lang']['m_oferta_produktowa_authors_list']['edit'] = "Edytuj autora";
$aConfig['lang']['m_oferta_produktowa_authors_list']['delete'] = "Usuń autora";
$aConfig['lang']['m_oferta_produktowa_authors_list']['delete_q'] = "Czy na pewno usunąć wybranego autora?";
$aConfig['lang']['m_oferta_produktowa_authors_list']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_authors_list']['delete_all'] = "Usuń zaznaczonych";
$aConfig['lang']['m_oferta_produktowa_authors_list']['delete_all_q'] = "Czy na pewno usunąć wybranych autorów?";
$aConfig['lang']['m_oferta_produktowa_authors_list']['delete_all_err'] = "Nie wybrano żadnego autora do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_authors_list']['authors_in_use_err'] = "Nie można usunąć autorów dopóki są przypisani do produktów!<br><br>Usuń najpierw przypisanie autorów do produktów.";
$aConfig['lang']['m_oferta_produktowa_authors_list']['go_back'] = "powrót do listy stron";
$aConfig['lang']['m_oferta_produktowa_authors_list']['sort'] = "Sortuj autorów w boksie";
$aConfig['lang']['m_oferta_produktowa_authors_list']['sort_items'] = "Sortowanie autorów wyróżnionych w boksie";
$aConfig['lang']['m_oferta_produktowa_authors_list']['go_back'] = "Powrót do listy autorów";
$aConfig['lang']['m_oferta_produktowa_authors_list']['add'] = "Dodaj autora";
$aConfig['lang']['m_oferta_produktowa_authors_list']['send_button_toauthor'] = "Przypisz do  autora";

$aConfig['lang']['m_oferta_produktowa_authors_list']['header_linked'] = "Przypisywanie do wybranego autora - \"%s\"";
$aConfig['lang']['m_oferta_produktowa_authors_list']['selected_none'] = "Nie wybrano żadnych autorów do przypisania!";
$aConfig['lang']['m_oferta_produktowa_authors_list']['author_none'] = "Nie wybrano autora do którego przypisujemy!";
$aConfig['lang']['m_oferta_produktowa_authors_list']['author_search'] = "Autor";
$aConfig['lang']['m_oferta_produktowa_authors_list']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa_authors_list']['button_send'] = "Zapisz";
$aConfig['lang']['m_oferta_produktowa_authors_list']['linking_failed'] = "Wystąpił błąd podczas zapisu mapowań. Spróbuj ponownie.";
$aConfig['lang']['m_oferta_produktowa_authors_list']['linking_ok'] = "Zapisano mapowanie autorów. Autorzy produktów zostali zaktualizowani.";

$aConfig['lang']['m_oferta_produktowa_authors_list']['header_0'] = "Dodawanie autora";
$aConfig['lang']['m_oferta_produktowa_authors_list']['header_1'] = "Edycja autora";
$aConfig['lang']['m_oferta_produktowa_authors_list']['title'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_authors_list']['or'] = "lub nowy";
$aConfig['lang']['m_oferta_produktowa_authors_list']['name'] = "Imię";
$aConfig['lang']['m_oferta_produktowa_authors_list']['surname'] = "Nazwisko";
$aConfig['lang']['m_oferta_produktowa_authors_list']['add_ok'] = "Dodano autora \"%s\"";
$aConfig['lang']['m_oferta_produktowa_authors_list']['add_err'] = "Wystąpił błąd podczas dodawania autora \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_authors_list']['edit_ok'] = "Zmieniono autora \"%s\"";
$aConfig['lang']['m_oferta_produktowa_authors_list']['edit_err'] = "Wystąpił błąd podczas próby zmiany autora \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_authors_list']['del_ok_0']		= "Usunięto autora %s";
$aConfig['lang']['m_oferta_produktowa_authors_list']['del_ok_1']		= "Usunięto autorów: %s";
$aConfig['lang']['m_oferta_produktowa_authors_list']['del_err']		= "Wystąpił błąd podczas próby usunięcia autora \"%s\"!<br>Spróbuj ponownie";


/*---------- role autorow */
$aConfig['lang']['m_oferta_produktowa_authors_roles']['list'] = "Lista ról autorów";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['list_name'] = "Rola";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['no_items'] = "Brak zdefiniowanych ról autorów";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['edit'] = "Edytuj rolę autorów";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['delete'] = "Usuń rolę autorów";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['delete_q'] = "Czy na pewno usunąć wybraną rolę autorów?";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['delete_all_q'] = "Czy na pewno usunąć wybrane role autorów?";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['delete_all_err'] = "Nie wybrano żadnej roli autorów do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['roles_in_use_err'] = "Nie można usunąć ról dopóki są używane w produktach!<br><br>Usuń najpierw przypisanie autorów do ról w produktach.";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['sort'] = "Sortuj role autorów";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['sort_items'] = "Sortowanie ról autorów";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['go_back'] = "Powrót do listy ról autorów";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['add'] = "Dodaj rolę autorów";

$aConfig['lang']['m_oferta_produktowa_authors_roles']['header_0'] = "Dodawanie roli autorów";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['header_1'] = "Edycja roli autorów";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['name'] = "Nazwa roli";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['add_ok'] = "Dodano rolę autorów \"%s\"";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['add_err'] = "Wystąpił błąd podczas dodawania roli autorów \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['edit_ok'] = "Zmieniono rolę autorów \"%s\"";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['edit_err'] = "Wystąpił błąd podczas próby zmiany roli autorów \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['del_ok_0']		= "Usunięto rolę autorów %s";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['del_ok_1']		= "Usunięto role autorów: %s";
$aConfig['lang']['m_oferta_produktowa_authors_roles']['del_err']		= "Wystąpił błąd podczas próby usunięcia roli autorów \"%s\"!<br>Spróbuj ponownie";

$aConfig['lang']['m_oferta_produktowa_common_box_settings']['page_id'] = "Strona";
$aConfig['lang']['m_produktowa_common_box_settings']['items_in_box'] = "Produktów w boksie";

/*---------- powiadomienia */
$aConfig['lang']['m_oferta_produktowa_notifications']['header'] = "Powiadomienia o dostępności książek w sprzedaży";
$aConfig['lang']['m_oferta_produktowa_notifications']['notification'] = "Powiadomienia o dostępności książek w sprzedaży";
$aConfig['lang']['m_oferta_produktowa_notifications']['product'] = "Wybierz produkt";
$aConfig['lang']['m_oferta_produktowa_notifications']['no_product_chosen'] = "Musisz wybrać produkt!";
$aConfig['lang']['m_oferta_produktowa_notifications']['get_users_list'] = "Pobierz listę";
$aConfig['lang']['m_oferta_produktowa_notifications']['list_lp'] = "LP";
$aConfig['lang']['m_oferta_produktowa_notifications']['list_content'] = "ZAPYTANIE";
$aConfig['lang']['m_oferta_produktowa_notifications']['list_email'] = "EMAIL";

/*---------- jezyki */
$aConfig['lang']['m_oferta_produktowa_languages']['list'] = "Lista języków produktów";
$aConfig['lang']['m_oferta_produktowa_languages']['list_language'] = "Opis";
$aConfig['lang']['m_oferta_produktowa_languages']['no_items'] = "Brak zdefiniowanych języków produktów";
$aConfig['lang']['m_oferta_produktowa_languages']['edit'] = "Edytuj język";
$aConfig['lang']['m_oferta_produktowa_languages']['delete'] = "Usuń język";
$aConfig['lang']['m_oferta_produktowa_languages']['delete_q'] = "Czy na pewno usunąć wybrany język?";
$aConfig['lang']['m_oferta_produktowa_languages']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_languages']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_languages']['delete_all_q'] = "Czy na pewno usunąć wybrane języki?";
$aConfig['lang']['m_oferta_produktowa_languages']['delete_all_err'] = "Nie wybrano żadnego języka do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_languages']['add'] = "Dodaj język";

$aConfig['lang']['m_oferta_produktowa_languages']['change'] = "Przypisz książki tego języka do innego";
$aConfig['lang']['m_oferta_produktowa_languages']['header_0'] = "Dodawanie języka produktów";
$aConfig['lang']['m_oferta_produktowa_languages']['header_1'] = "Edycja języka produktów";
$aConfig['lang']['m_oferta_produktowa_languages']['language_search'] = "Język";
$aConfig['lang']['m_oferta_produktowa_languages']['language'] = "Język";
$aConfig['lang']['m_oferta_produktowa_languages']['logo'] = "Flaga";

$aConfig['lang']['m_oferta_produktowa_languages']['header_change'] = "Przypisz do innego języka książki języka";
$aConfig['lang']['m_oferta_produktowa_languages']['change_ok'] = "Przepisano książki języka \"%s\" do języka \"%s\", zmienionych produktów %d";
$aConfig['lang']['m_oferta_produktowa_languages']['change_err'] = "Nie udało się przepisać książek języka \"%s\" do języka \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_languages']['incorrect_language_err'] = "Wybrany język nie istnieje";


$aConfig['lang']['m_oferta_produktowa_languages']['add_ok'] = "Dodano język \"%s\"";
$aConfig['lang']['m_oferta_produktowa_languages']['add_err'] = "Nie udało się dodać języka \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_languages']['edit_ok'] = "Zmieniono język \"%s\"";
$aConfig['lang']['m_oferta_produktowa_languages']['edit_err'] = "Nie udało się zmienić języka \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_languages']['del_ok_0']		= "Usunięto język %s";
$aConfig['lang']['m_oferta_produktowa_languages']['del_ok_1']		= "Usunięto języki %s";
$aConfig['lang']['m_oferta_produktowa_languages']['del_err_0']		= "Nie udało się usunąć języka %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_languages']['del_err_1']		= "Nie udało się usunąć języków %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_languages']['del_err_exists']		= "Nie udało się usunąć języka %s!<br>Najpierw usuń produkt z tym językiem";

/*---------- oprawy */
$aConfig['lang']['m_oferta_produktowa_bindings']['list'] = "Lista opraw produktów";
$aConfig['lang']['m_oferta_produktowa_bindings']['list_binding'] = "Opis";
$aConfig['lang']['m_oferta_produktowa_bindings']['no_items'] = "Brak zdefiniowanych opraw produktów";
$aConfig['lang']['m_oferta_produktowa_bindings']['edit'] = "Edytuj oprawę";
$aConfig['lang']['m_oferta_produktowa_bindings']['delete'] = "Usuń oprawę";
$aConfig['lang']['m_oferta_produktowa_bindings']['delete_q'] = "Czy na pewno usunąć wybraną oprawę?";
$aConfig['lang']['m_oferta_produktowa_bindings']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_bindings']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_bindings']['delete_all_q'] = "Czy na pewno usunąć wybrane oprawy?";
$aConfig['lang']['m_oferta_produktowa_bindings']['delete_all_err'] = "Nie wybrano żadnej oprawy do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_bindings']['add'] = "Dodaj oprawę";

$aConfig['lang']['m_oferta_produktowa_bindings']['change'] = "Przypisz książki tej oprawy do innej";
$aConfig['lang']['m_oferta_produktowa_bindings']['header_0'] = "Dodawanie oprawy produktów";
$aConfig['lang']['m_oferta_produktowa_bindings']['header_1'] = "Edycja oprawy produktów";
$aConfig['lang']['m_oferta_produktowa_bindings']['binding'] = "Oprawa";
$aConfig['lang']['m_oferta_produktowa_bindings']['binding_search'] = "Oprawa";

$aConfig['lang']['m_oferta_produktowa_bindings']['header_change'] = "Przypisz do innej oprawy książki tej oprawy";
$aConfig['lang']['m_oferta_produktowa_bindings']['change_ok'] = "Przepisano książki oprawy \"%s\" do oprawy \"%s\", zmienionych produktów %d";
$aConfig['lang']['m_oferta_produktowa_bindings']['change_err'] = "Nie udało się przepisać książek oprawy \"%s\" do oprawy \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_bindings']['incorrect_binding_err'] = "Wybrana oprawa nie istnieje";


$aConfig['lang']['m_oferta_produktowa_bindings']['add_ok'] = "Dodano oprawę \"%s\"";
$aConfig['lang']['m_oferta_produktowa_bindings']['add_err'] = "Nie udało się dodać oprawy \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_bindings']['edit_ok'] = "Zmieniono oprawę \"%s\"";
$aConfig['lang']['m_oferta_produktowa_bindings']['edit_err'] = "Nie udało się zmienić oprawy \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_bindings']['del_ok_0']		= "Usunięto oprawę %s";
$aConfig['lang']['m_oferta_produktowa_bindings']['del_ok_1']		= "Usunięto oprawy %s";
$aConfig['lang']['m_oferta_produktowa_bindings']['del_err_0']		= "Nie udało się usunąć oprawy %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_bindings']['del_err_1']		= "Nie udało się usunąć opraw %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_bindings']['del_err_exists']		= "Nie udało się usunąć oprawy %s!<br>Najpierw usuń produkt";


/*---------- formaty */
$aConfig['lang']['m_oferta_produktowa_dimensions']['list'] = "Lista formatów produktów";
$aConfig['lang']['m_oferta_produktowa_dimensions']['list_dimension'] = "Format";
$aConfig['lang']['m_oferta_produktowa_dimensions']['no_items'] = "Brak zdefiniowanych formatów produktów";
$aConfig['lang']['m_oferta_produktowa_dimensions']['edit'] = "Edytuj format";
$aConfig['lang']['m_oferta_produktowa_dimensions']['delete'] = "Usuń format";
$aConfig['lang']['m_oferta_produktowa_dimensions']['delete_q'] = "Czy na pewno usunąć wybrany format?";
$aConfig['lang']['m_oferta_produktowa_dimensions']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_dimensions']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_dimensions']['delete_all_q'] = "Czy na pewno usunąć wybrane formaty?";
$aConfig['lang']['m_oferta_produktowa_dimensions']['delete_all_err'] = "Nie wybrano żadnego formatu do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_dimensions']['add'] = "Dodaj format";

$aConfig['lang']['m_oferta_produktowa_dimensions']['header_0'] = "Dodawanie formatu produktów";
$aConfig['lang']['m_oferta_produktowa_dimensions']['header_1'] = "Edycja formatu produktów";
$aConfig['lang']['m_oferta_produktowa_dimensions']['dimension'] = "Format";

$aConfig['lang']['m_oferta_produktowa_dimensions']['add_ok'] = "Dodano format \"%s\"";
$aConfig['lang']['m_oferta_produktowa_dimensions']['add_err'] = "Nie udało się dodać formatu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_dimensions']['edit_ok'] = "Zmieniono format \"%s\"";
$aConfig['lang']['m_oferta_produktowa_dimensions']['edit_err'] = "Nie udało się zmienić formatu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_dimensions']['del_ok_0']		= "Usunięto format %s";
$aConfig['lang']['m_oferta_produktowa_dimensions']['del_ok_1']		= "Usunięto formatów %s";
$aConfig['lang']['m_oferta_produktowa_dimensions']['del_err_0']		= "Nie udało się usunąć formatu %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_dimensions']['del_err_1']		= "Nie udało się usunąć formatów %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_dimensions']['del_err_exists']		= "Nie udało się usunąć formatu %s!<br>Najpierw usuń produkt";

/*---------- lata */
$aConfig['lang']['m_oferta_produktowa_years']['list'] = "Lista lat";
$aConfig['lang']['m_oferta_produktowa_years']['list_year'] = "Rok";
$aConfig['lang']['m_oferta_produktowa_years']['no_items'] = "Brak zdefiniowanych lat publikacji produktów";
$aConfig['lang']['m_oferta_produktowa_years']['edit'] = "Edytuj rok";
$aConfig['lang']['m_oferta_produktowa_years']['delete'] = "Usuń rok";
$aConfig['lang']['m_oferta_produktowa_years']['delete_q'] = "Czy na pewno usunąć wybrany rok?";
$aConfig['lang']['m_oferta_produktowa_years']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_years']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_years']['delete_all_q'] = "Czy na pewno usunąć wybrane lata?";
$aConfig['lang']['m_oferta_produktowa_years']['delete_all_err'] = "Nie wybrano żadnego roku do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_years']['add'] = "Dodaj rok";

$aConfig['lang']['m_oferta_produktowa_years']['header_0'] = "Dodawanie roku";
$aConfig['lang']['m_oferta_produktowa_years']['header_1'] = "Edycja roku";
$aConfig['lang']['m_oferta_produktowa_years']['year'] = "Rok";

$aConfig['lang']['m_oferta_produktowa_years']['add_ok'] = "Dodano rok \"%s\"";
$aConfig['lang']['m_oferta_produktowa_years']['add_err'] = "Nie udało się dodać roku \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_years']['edit_ok'] = "Zmieniono rok \"%s\"";
$aConfig['lang']['m_oferta_produktowa_years']['edit_err'] = "Nie udało się zmienić roku \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_years']['del_ok_0']		= "Usunięto rok %s";
$aConfig['lang']['m_oferta_produktowa_years']['del_ok_1']		= "Usunięto lata %s";
$aConfig['lang']['m_oferta_produktowa_years']['del_err_0']		= "Nie udało się usunąć roku %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_years']['del_err_1']		= "Nie udało się usunąć lat %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_years']['del_err_exists']		= "Nie udało się usunąć roku %s!<br>Najpierw usuń produkt";


/*---------- ttypy zalacznikow */
$aConfig['lang']['m_oferta_produktowa_attachment_types']['list'] = "Lista typów załączników produktów";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['list_name'] = "Typ";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['no_items'] = "Brak zdefiniowanych typów załączników produktów";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['edit'] = "Edytuj typ załącznika";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['delete'] = "Usuń typ załącznika";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['delete_q'] = "Czy na pewno usunąć wybrany typ załącznika?";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['delete_all_q'] = "Czy na pewno usunąć wybrane typy załącznika?";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['delete_all_err'] = "Nie wybrano żadnego typu załącznika do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['add'] = "Dodaj typ załącznika";

$aConfig['lang']['m_oferta_produktowa_attachment_types']['header_0'] = "Dodawanie typu załącznika produktów";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['header_1'] = "Edycja typu załącznika produktów";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['name'] = "Typ załącznika";

$aConfig['lang']['m_oferta_produktowa_attachment_types']['add_ok'] = "Dodano typ załącznika \"%s\"";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['add_err'] = "Nie udało się dodać typu załącznika \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['edit_ok'] = "Zmieniono typ załącznika \"%s\"";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['edit_err'] = "Nie udało się zmienić typu załącznika \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['del_ok_0']		= "Usunięto typ załącznika %s";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['del_ok_1']		= "Usunięto typy załącznika %s";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['del_err_0']		= "Nie udało się usunąć typu załącznika %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_attachment_types']['del_err_exists']		= "Nie udało się usunąć typów załącznika %s!<br>Najpierw usuń załącznik aby usunąć typ";

/*---------- ttypy plików */
$aConfig['lang']['m_oferta_produktowa_file_types']['list'] = "Lista typów plików";
$aConfig['lang']['m_oferta_produktowa_file_types']['list_name'] = "Typ";
$aConfig['lang']['m_oferta_produktowa_file_types']['no_items'] = "Brak zdefiniowanych typów plików produktów";
$aConfig['lang']['m_oferta_produktowa_file_types']['edit'] = "Edytuj typ pliku";
$aConfig['lang']['m_oferta_produktowa_file_types']['delete'] = "Usuń typ pliku";
$aConfig['lang']['m_oferta_produktowa_file_types']['delete_q'] = "Czy na pewno usunąć wybrany typ pliku?";
$aConfig['lang']['m_oferta_produktowa_file_types']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_file_types']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_file_types']['delete_all_q'] = "Czy na pewno usunąć wybrane typy plików?";
$aConfig['lang']['m_oferta_produktowa_file_types']['delete_all_err'] = "Nie wybrano żadnego typu plików do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_file_types']['add'] = "Dodaj typ plików";

$aConfig['lang']['m_oferta_produktowa_file_types']['header_0'] = "Dodawanie typu plików produktów";
$aConfig['lang']['m_oferta_produktowa_file_types']['header_1'] = "Edycja typu plików produktów";
$aConfig['lang']['m_oferta_produktowa_file_types']['name'] = "Typ plików";

$aConfig['lang']['m_oferta_produktowa_file_types']['add_ok'] = "Dodano typ plików \"%s\"";
$aConfig['lang']['m_oferta_produktowa_file_types']['add_err'] = "Nie udało się dodać typu plików \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_file_types']['edit_ok'] = "Zmieniono typ plików \"%s\"";
$aConfig['lang']['m_oferta_produktowa_file_types']['edit_err'] = "Nie udało się zmienić typu plików \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_file_types']['del_ok_0']		= "Usunięto typ plików %s";
$aConfig['lang']['m_oferta_produktowa_file_types']['del_ok_1']		= "Usunięto typy plików %s";
$aConfig['lang']['m_oferta_produktowa_file_types']['del_err_0']		= "Nie udało się usunąć typu plików %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_file_types']['del_err_exists']		= "Nie udało się usunąć typów plików %s!<br>Najpierw usuń plik aby usunąć typ";


/*---------- rabat ogólny */
$aConfig['lang']['m_oferta_produktowa_general_discount']['header'] = "Edycja rabatu ogólnego";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up'] = "Narzut";
$aConfig['lang']['m_oferta_produktowa_general_discount']['recount'] = "Przelicz cenniki";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_0'] = "Rabat ogólny - Profit J";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_1'] = "Rabat ogólny - Profit E";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_11'] = "Rabat ogólny - Platon";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_12'] = "Rabat ogólny - Azymut obcojęzyczny";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_13'] = "Rabat ogólny - Panda";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_10'] = "Rabat ogólny - Stock";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_2'] = "Rabat ogólny - Azymut";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_3'] = "Rabat ogólny - ABE";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_4'] = "Rabat ogólny - Helion";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_5'] = "Rabat ogólny - Ateneum";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_6'] = "Rabat ogólny - Profit X";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_7'] = "Rabat ogólny - Dictum";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_8'] = "Rabat ogólny - Siódemka";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_9'] = "Rabat ogólny - Olesiejuk";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_0_section'] = "Rabat ogólny - Profit J";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_1_section'] = "Rabat ogólny - Profit E";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_11_section'] = "Rabat ogólny - Platon";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_12_section'] = "Rabat ogólny - Azymut obcojęzyczny";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_13_section'] = "Rabat ogólny - Panda";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_10_section'] = "Rabat ogólny - Stock";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_2_section'] = "Rabat ogólny - Azymut";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_3_section'] = "Rabat ogólny - ABE";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_4_section'] = "Rabat ogólny - Helion";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_5_section'] = "Rabat ogólny - Ateneum";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_6_section'] = "Rabat ogólny - Profit X";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_7_section'] = "Rabat ogólny - Dictum";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_8_section'] = "Rabat ogólny - Siódemka";
$aConfig['lang']['m_oferta_produktowa_general_discount']['discount_9_section'] = "Rabat ogólny - Olesiejuk";

$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_0'] = "Narzut - Profit J";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_1'] = "Narzut - Profit E";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_10'] = "Narzut - Stock";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_11'] = "Narzut - Platon";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_12'] = "Narzut - Azymut obcojęzyczny";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_13'] = "Narzut - Panda";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_2'] = "Narzut - Azymut";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_3'] = "Narzut - ABE";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_4'] = "Narzut - Helion";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_5'] = "Narzut - Ateneum";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_6'] = "Narzut - Profit X";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_7'] = "Narzut - Dictum";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_8'] = "Narzut - Siódemka";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_9'] = "Narzut - Olesiejuk";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_0_section'] = "Narzut - Profit J";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_1_section'] = "Narzut - Profit E";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_10_section'] = "Narzut - Stock";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_11_section'] = "Narzut - Platon";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_12_section'] = "Narzut - Azymut obcojęzyczny";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_13_section'] = "Narzut - Panda";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_2_section'] = "Narzut - Azymut";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_3_section'] = "Narzut - ABE";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_4_section'] = "Narzut - Helion";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_5_section'] = "Narzut - Ateneum";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_6_section'] = "Narzut - Profit X";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_7_section'] = "Narzut - Dictum";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_8_section'] = "Narzut - Siódemka";
$aConfig['lang']['m_oferta_produktowa_general_discount']['mark_up_9_section'] = "Narzut - Olesiejuk";

$aConfig['lang']['m_oferta_produktowa_general_discount']['edit_ok'] = "Zmieniono rabat ogólny";
$aConfig['lang']['m_oferta_produktowa_general_discount']['edit_err'] = "Nie udało się zmienić rabatu ogólnego!<br><br>Spróbuj ponownie";


$aConfig['lang']['m_oferta_produktowa_not_mapped']['header'] = "Niezmapowane kategorie";
$aConfig['lang']['m_oferta_produktowa_not_mapped']['azymut_section'] = "Azymut";
$aConfig['lang']['m_oferta_produktowa_not_mapped']['abe_section'] = "STARE ABE";
$aConfig['lang']['m_oferta_produktowa_not_mapped']['helion_section'] = "HELION";
$aConfig['lang']['m_oferta_produktowa_not_mapped']['not_mapped'] = "Niezmapowane";

$aConfig['lang']['m_oferta_produktowa_offers']['no_filters'] = "Brak wybranych filtrów";
$aConfig['lang']['m_oferta_produktowa_offers']['list'] = "Lista produktów";
$aConfig['lang']['m_oferta_produktowa_offers']['f_published'] = "Stan publikacji";
$aConfig['lang']['m_oferta_produktowa_offers']['f_all'] = "Wszystkie";
$aConfig['lang']['m_oferta_produktowa_offers']['main_pool'] = 'Szukaj <br />(Tytuł, ISBN, Id)';
$aConfig['lang']['m_oferta_produktowa_offers']['f_tags'] = "LUB tag";
$aConfig['lang']['m_oferta_produktowa_offers']['f_published'] = "Opublikowane";
$aConfig['lang']['m_oferta_produktowa_offers']['f_not_published'] = "Nieopublikowane";
$aConfig['lang']['m_oferta_produktowa_offers']['f_type'] = "Typ produktu";
$aConfig['lang']['m_oferta_produktowa_offers']['f_author'] = "Autor";
$aConfig['lang']['m_oferta_produktowa_offers']['f_isbn'] = "ISBN";
$aConfig['lang']['m_oferta_produktowa_offers']['f_status'] = "Stan";
$aConfig['lang']['m_oferta_produktowa_offers']['no_attributes_groups'] = "Dla produktów tej kategorii nie ma ustanowionych żadnych grup atrybutów";
$aConfig['lang']['m_oferta_produktowa_offers']['f_category'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_offers']['f_publisher'] = "Wydawca";
$aConfig['lang']['m_oferta_produktowa_offers']['f_language'] = "Język";
$aConfig['lang']['m_oferta_produktowa_offers']['f_orig_language'] = "Język oryg.";
$aConfig['lang']['m_oferta_produktowa_offers']['f_binding'] = "Oprawa";
$aConfig['lang']['m_oferta_produktowa_offers']['f_reviewed'] = "Przejrzane";
$aConfig['lang']['m_oferta_produktowa_offers']['f_not_reviewed'] = "Nie przejrzane";
$aConfig['lang']['m_oferta_produktowa_offers']['f_category_btn'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source'] = "Akt.źr";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_internalj'] = "Profit J";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_internaln'] = "Profit E";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_internalg'] = "Stock";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_internalx'] = "Profit X";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_azymut'] = "Azymut";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_abe'] = "ABE";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_helion'] = "Helion";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_dictum'] = "Dictum";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_siodemka'] = "Siódemka";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_olesiejuk'] = "Olesiejuk";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_ateneum'] = "Ateneum";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_platon'] = "Platon";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_azymutobc'] = "Azymut obcojęzyczny";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_panda'] = "Panda";
$aConfig['lang']['m_oferta_produktowa_offers']['f_source_X'] = "X";
$aConfig['lang']['m_oferta_produktowa_offers']['f_image'] = "Z okładką";
$aConfig['lang']['m_oferta_produktowa_offers']['f_not_image'] = "Bez okładki";
$aConfig['lang']['m_oferta_produktowa_offers']['f_year'] = "Rok publikacji";
$aConfig['lang']['m_oferta_produktowa_offers']['f_start_date'] = "Dodano do oferty od";
$aConfig['lang']['m_oferta_produktowa_offers']['f_end_date'] = "Dodano do oferty do";
$aConfig['lang']['m_oferta_produktowa_offers']['button_search'] = "Pobierz pdf";
$aConfig['lang']['m_oferta_produktowa_offers']['header'] = "Generator ofert";
$aConfig['lang']['m_oferta_produktowa_offers']['config_section'] = "Widocznośc kolumn";
$aConfig['lang']['m_oferta_produktowa_offers']['edition'] = "Wydanie/rok wydania";
$aConfig['lang']['m_oferta_produktowa_offers']['price'] = "Cena katalogowa";
$aConfig['lang']['m_oferta_produktowa_offers']['tarrif'] = "Cena aktualna";
$aConfig['lang']['m_oferta_produktowa_offers']['search'] = "Szukaj";

$aConfig['lang']['m_oferta_produktowa_offers']['name_label'] = "Książka";
$aConfig['lang']['m_oferta_produktowa_offers']['price_brutto_label'] = "Cena katalog.";
$aConfig['lang']['m_oferta_produktowa_offers']['price_label'] = "Cena aktualna";
$aConfig['lang']['m_oferta_produktowa_offers']['authors_label'] = "Autorzy";
$aConfig['lang']['m_oferta_produktowa_offers']['publisher_label'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_offers']['valid_to'] = "obowiazuje do";
$aConfig['lang']['m_oferta_produktowa_offers']['title'] = "profit24 - Oferta";
$aConfig['lang']['m_oferta_produktowa_offers']['logo_title'] = "Księgarnia profit24 | www.profit24.com.pl";
$aConfig['lang']['m_oferta_produktowa_offers']['logo_string'] = "Profit M Spółka z ograniczoną odpowiedzialnością Al. Jerozolimskie 134, 02-305 Warszawa\nNIP 525-22-45-459, KRS: 0000756053 Sąd Rejonowy dla miasta stołecznego Warszawy w Warszawie, XII Wydział Gospodarczy KRS";

$aConfig['lang']['m_oferta_produktowa_offers']['maxed_out'] = "Przekroczno limit otrzymanych wyników! Zawęź warunki wyszukiwania. Znaleziono %d rekrodów dla limitu %d";

/*---------- stawki VAT*/
$aConfig['lang']['m_oferta_produktowa_vatstakes']['header'] = "Stawki VAT";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['list'] = "Lista stawek VAT";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['delete'] = "Usuń stawkę";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['delete_q'] = "Czy na pewno usunąć wybraną stawkę?";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['delete_all_q'] = "Czy na pewno usunąć wybrane stawki?";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['delete_all_err'] = "Nie wybrano żadnej stawki do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['no_items'] = "Brak zdefiniowanych stawek";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['edit'] = "Edytuj stawkę";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['add'] = "Dodaj stawkę";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['name'] = "Wartość stawki (%)";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['list_name'] = "Wartość stawki (%)";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['edit_ok'] = "Zmieniono stawkę \"%s\"";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['edit_err'] = "Nie udało się zmienić stawki \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['tags_invalid_err'] = "Wartość stawki zawiera nieprawidłowe znaki!";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['del_ok_0']		= "Usunięto stawkę %s";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['del_ok_1']		= "Usunięto stawki %s";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['del_err_0']		= "Nie udało się usunąć stawki %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_vatstakes']['del_err_1']		= "Nie udało się usunąć stawek %s!<br>Spróbuj ponownie";

/*---------- bledne stawki vat*/
$aConfig['lang']['m_oferta_produktowa_mismatchvat']['header'] = "Lista książek z błędnymi stawkami VAT";
$aConfig['lang']['m_oferta_produktowa_mismatchvat']['isbn'] = "ISBN";
$aConfig['lang']['m_oferta_produktowa_mismatchvat']['name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_mismatchvat']['value'] = "Wartość (%)";

/*---------- tagi */
$aConfig['lang']['m_oferta_produktowa_tags']['list'] = "Lista tagów";
$aConfig['lang']['m_oferta_produktowa_tags']['list_tag'] = "Tag";
$aConfig['lang']['m_oferta_produktowa_tags']['list_quantity'] = "Ilość";
$aConfig['lang']['m_oferta_produktowa_tags']['list_page'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_tags']['no_items'] = "Brak zdefiniowanych tagów";
$aConfig['lang']['m_oferta_produktowa_tags']['edit'] = "Edytuj tag";

$aConfig['lang']['m_oferta_produktowa_tags']['main_category'] = "Kategoria główna";

$aConfig['lang']['m_oferta_produktowa_tags']['delete'] = "Usuń tag";
$aConfig['lang']['m_oferta_produktowa_tags']['delete_q'] = "Czy na pewno usunąć wybrany tag?";
$aConfig['lang']['m_oferta_produktowa_tags']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_tags']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_tags']['delete_all_q'] = "Czy na pewno usunąć wybrane tagi?";
$aConfig['lang']['m_oferta_produktowa_tags']['delete_all_err'] = "Nie wybrano żadnego tagu do usunięcia!";


$aConfig['lang']['m_oferta_produktowa_tags']['header_0'] = "Dodawanie tagu";
$aConfig['lang']['m_oferta_produktowa_tags']['header_1'] = "Edycja tagu";
$aConfig['lang']['m_oferta_produktowa_tags']['tag'] = "Tag";
$aConfig['lang']['m_oferta_produktowa_tags']['edit_ok'] = "Zmieniono tag \"%s\"";
$aConfig['lang']['m_oferta_produktowa_tags']['edit_err'] = "Nie udało się zmienić tagu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_tags']['tags_invalid_err'] = "Tag zawiera nieprawidłowe znaki!";
$aConfig['lang']['m_oferta_produktowa_tags']['del_ok_0']		= "Usunięto tag %s";
$aConfig['lang']['m_oferta_produktowa_tags']['del_ok_1']		= "Usunięto tagi %s";
$aConfig['lang']['m_oferta_produktowa_tags']['del_err_0']		= "Nie udało się usunąć tagu %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_tags']['del_err_1']		= "Nie udało się usunąć tagów %s!<br>Spróbuj ponownie";


/*---------- Limity na źródła książek */
$aConfig['lang']['m_oferta_produktowa_sources']['header_linked2'] = "Powiązywanie z wydawnictwem - %s";
$aConfig['lang']['m_oferta_produktowa_sources']['selected_none'] = "Nie wybrano żadnych autorów do przypisania!";
$aConfig['lang']['m_oferta_produktowa_sources']['author_none'] = "Nie wybrano autora do którego przypisujemy!";
$aConfig['lang']['m_oferta_produktowa_sources']['linking_failed'] = "Wystąpił błąd podczas zapisu mapowań. Spróbuj ponownie.";
$aConfig['lang']['m_oferta_produktowa_sources']['linking_ok'] = "Zapisano mapowanie wydawnictw.";
$aConfig['lang']['m_oferta_produktowa_sources']['author_search'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_sources']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa_sources']['button_send'] = "Zapisz";


$aConfig['lang']['m_oferta_produktowa_sources']['f_limited'] = "Z limitem";
$aConfig['lang']['m_oferta_produktowa_sources']['f_not_limited'] = "Bez limitu";
$aConfig['lang']['m_oferta_produktowa_sources']['f_all'] = "Wszystkie";

$aConfig['lang']['m_oferta_produktowa_sources']['list'] = "Lista limitów rabatów na źródła pochodzenia";
$aConfig['lang']['m_oferta_produktowa_sources']['list_name'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_sources']['list_created'] = "Utworzono";
$aConfig['lang']['m_oferta_produktowa_sources']['list_modified'] = "Zmodyfikowano";
$aConfig['lang']['m_oferta_produktowa_sources']['no_items'] = "Brak zdefiniowanych limitów na źródła pochodzenia";
$aConfig['lang']['m_oferta_produktowa_sources']['edit'] = "Edytuj źródło";
$aConfig['lang']['m_oferta_produktowa_sources']['discount_limit'] = "Limit rabatu";
$aConfig['lang']['m_oferta_produktowa_sources']['recount_limit'] = "Przelicz limit";
$aConfig['lang']['m_oferta_produktowa_sources']['delete'] = "Usuń Limit";
$aConfig['lang']['m_oferta_produktowa_sources']['delete_q'] = "Czy na pewno usunąć wybrany limit?";
$aConfig['lang']['m_oferta_produktowa_sources']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_sources']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_sources']['delete_all_q'] = "Czy na pewno usunąć wybrane limity ?";
$aConfig['lang']['m_oferta_produktowa_sources']['delete_all_err'] = "Nie wybrano żadnego limitu do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_sources']['add'] = "Dodaj limit rabatu";

$aConfig['lang']['m_oferta_produktowa_sources']['header_0'] = "Dodawanie limitu na pierwotne źródło pochodzenia";
$aConfig['lang']['m_oferta_produktowa_sources']['header_1'] = "Edycja limitu na pierwotne źródło pochodzenia";
$aConfig['lang']['m_oferta_produktowa_sources']['first_source'] = "Pierwotne źródło - symbol";

$aConfig['lang']['m_oferta_produktowa_sources']['add_ok'] = "Dodano limit \"%s\"";
$aConfig['lang']['m_oferta_produktowa_sources']['add_err'] = "Nie udało się dodać limitu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_sources']['edit_ok'] = "Zmieniono limit \"%s\"";
$aConfig['lang']['m_oferta_produktowa_sources']['edit_err'] = "Nie udało się zmienić limitu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_sources']['del_ok_0']		= "Usunięto limit %s";
$aConfig['lang']['m_oferta_produktowa_sources']['del_ok_1']		= "Usunięto limity %s";
$aConfig['lang']['m_oferta_produktowa_sources']['del_err_0']		= "Nie udało się usunąć limitu %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_sources']['del_err_1']		= "Nie udało się usunąć limitów %s!<br>Spróbuj ponownie";





/*---------- promocje - darmowa dostawa */
$aConfig['lang']['m_oferta_produktowa_free_delivery']['list'] = "Lista pól - darmowa dostawa";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['go_back'] = "Powrót do listy promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['book_list2'] = "Lista książek w promocji \"%s\"";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['title'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['category_page'] = "Kategoria";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['date_start'] = "Początek";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['start_time'] = "Od godz.";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['date_end'] = "Koniec";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['end_time'] = "Do godz.";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['price_range'] = "Zakres cen";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['price_from'] = "Od";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['price_to'] = "Do";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['author'] = "Autor";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['publisher'] = "Wydawnictwo";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['publication_year'] = "Rok wyd.";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['series'] = "Seria";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['books'] = "Lista książek w promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['show'] = "Rozwiń";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['hide'] = "Zwiń";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['book_status_err'] = "Książka \"%s\" ma nieprawidłowy status";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['info001'] = "Niektóre produkty były już dodane wcześniej i nie można dodać ich ponownie.";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['info002'] = ". Ze względu na nałożone limity rabatów niektóre produkty zostały dodane do innej wartości rabatu.";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['info003'] = " Dla niektórych produktów dodano nowe wartości rabatu.";

$aConfig['lang']['m_oferta_produktowa_free_delivery']['list_id'] = "Id";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['list_name'] = "Tytuł";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['book_list'] = "Lista książek";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['list_pname'] = "Nazwa";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['list_base_price'] = "Cena bazowa";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['list_price_brutto'] = "Cena brutto";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['no_books_to_show'] = "Brak pasujących pozycji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['start_date'] = "Obowiązuje od";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['end_date'] = "Obowiązuje do";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['del_discount'] = "Usuń wartość rabatu";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['list_category'] = "Dział promocji";

$aConfig['lang']['m_oferta_produktowa_free_delivery']['sort'] = "Sortuj promocje";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['sort_items2'] = "Sortuj pozycje w boksie";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['sort_items'] = "Sortowanie promocji";

$aConfig['lang']['m_oferta_produktowa_free_delivery']['no_items'] = "Brak zdefiniowanych pól - darmowa dostawa";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['no_items'] = "Brak książek w wybranej promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['edit'] = "Edytuj rabat";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['delete'] = "Usuń z promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['delete_q'] = "Czy na pewno usunąć wybrany produkt z promocji?";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['delete_all_q'] = "Czy na pewno usunąć wybrane produkty z promocji?";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['delete_all_err'] = "Nie wybrano żadnego produktu do usunięcia z promocji!";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['delete'] = "Usuń książkę z promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['delete_q'] = "Czy na pewno usunąć wybrany produkt z promocji?";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['preview'] = "Podgląd";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['remove_all_q'] = "Czy na pewno usunąć wybrane produkty z promocji?";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['remove_all_err'] = "Nie wybrano żadnego produktu do usunięcia z promocji!";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_free_delivery_insertBooks']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_free_delivery_insertBooks']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_free_delivery_insertBooks']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_free_delivery_removeVal']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_free_delivery_removeVal']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_free_delivery_removeVal']['remove_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['search_in'] = "Dodaj z pliku";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['add_from_file'] = "Dodaj z pliku z rabatem ";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['add_topromo_fileError'] = "Dodawanie z pliku nie powiodło się. Sprawdź poprawność pliku i spróbuj ponownie.";
//$aConfig['lang']['m_oferta_produktowa_free_delivery']['sort'] = "Sortuj produkty promocyjne";
//$aConfig['lang']['m_oferta_produktowa_free_delivery']['sort_items'] = "Sortowanie promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['go_back'] = "Powrót do listy promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['add'] = "Dodaj";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['add_books'] = "Dodaj książki";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['add_books'] = "Dodaj książki";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['show_in_box'] = "Pokazuj w boxie";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['transport_methods'] = "Metoda transportu";

$aConfig['lang']['m_oferta_produktowa_free_delivery']['place_on_main'] = "Wyświetlaj zazn. na głównej";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['place_on_main_q'] = "Czy na pewno pokazywać wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['place_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['unplace_on_main'] = "Nie wyświetlaj zazn. na głównej";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['unplace_on_main_q'] = "Czy na pewno nie pokazywać wybranych produktów?";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['unplace_on_main_err'] = "Musisz wybrać produkty!";

$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['place_on_main'] = "Wyświetlaj zaznaczone na głównej";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['place_on_main_q'] = "Czy na pewno pokazywać wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['place_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['unplace_on_main'] = "Nie wyświetlaj zaznaczonych na głównej";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['unplace_on_main_q'] = "Czy na pewno nie pokazywać wybranych produktów?";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['unplace_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['plc_ok'] = "Zmiany zostały zapisane.";
$aConfig['lang']['m_oferta_produktowa_free_delivery_books']['plc_err'] = "Wystąpił błąd podczas zapisu. Spróbuj ponownie.";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['place_on_main'] = "Wyświetlaj zaznaczone na głównej";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['place_on_main_q'] = "Czy na pewno pokazywać wybrane produkty?";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['place_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['unplace_on_main'] = "Nie wyświetlaj zaznaczonych na głównej";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['unplace_on_main_q'] = "Czy na pewno nie pokazywać wybranych produktów?";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['unplace_on_main_err'] = "Musisz wybrać produkty!";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['plc_ok'] = "Zmiany zostały zapisane.";
$aConfig['lang']['m_oferta_produktowa_free_delivery_remove']['plc_err'] = "Wystąpił błąd podczas zapisu. Spróbuj ponownie.";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['placeOnMainLabel'] = "Wyświetlaj";

$aConfig['lang']['m_oferta_produktowa_free_delivery']['header_0'] = "Dodawanie promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['header_1'] = "Edycja promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['header_add_books'] = "Dodawanie stawek rabatów do promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['header_edit'] = "Edycja rabatu produktu \"%s\"";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['category'] = "Dział promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['product'] = "Produkt";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['current_price_brutto'] = "Aktualna cena brutto (zł)";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['current_promo_price_brutto'] = "Aktualna promocyjna cena brutto (zł)";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['discount'] = "Rabat (%)";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['discount2'] = "Rabat";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['discount3'] = "%";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['name'] = "Nazwa promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['page_id'] = "Dział promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['boxOpt'] = "Pokazuj w boxie";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['add_ok'] = "Dodano promocję  \"%s\"";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['add_err'] = "Nie udało się utworzyć promocji!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['add_topromo_ok'] = "Dodano produkty  \"%s\" do promocji \"%s\"";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['add_discount_ok'] = "Dodano stawki rabatu  \"%s\" do promocji \"%s\"";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['add_topromo_err'] = "Nie udało się dodać produktów do promocji \"%s\"!<br><br>Spróbuj ponownie";

$aConfig['lang']['m_oferta_produktowa_free_delivery']['edit_ok'] = "Wprowadzono zmianę w promocji \"%s\"";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['edit_err'] = "Wystąpił błąd podczas próby zmiany w promocji \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['discount_value_err'] = "Błędna wartość rabatu - wybierz wartośc z zakresu 1-99!";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['date_value_err'] = "Podane daty nie są prawidłowe!";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['del_ok']		= "Usunięto promocję";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['del_err']		= "Nie udało się usunąć promocji!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['del_prom_ok']		= "Usunięto produkt z promocji";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['del_prom_err']		= "Nie udało się usunąć produktu z promocji!<br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['del_val_ok']		= "Usunięto wartość rabatu";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['del_val_err']		= "Nie udało się usunąć wartości rabatu z promocji!<br>Spróbuj ponownie";


$aConfig['lang']['m_oferta_produktowa_free_delivery']['product_name'] = "Nazwa produktu";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['button_search'] = "Wyszukaj";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['choose_all'] = "Wszystkie";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['add_product_w_disc'] = "Dodaj produkt z rabatem ";
$aConfig['lang']['m_oferta_produktowa_free_delivery']['free_delivery'] = "Dodaj produkty do darmowa przesyłka \"%s\"";


/*---------- Mapowania stan w źródle -> stan w Profit24.pl */
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['list'] = "Lista mapowań stanów";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['list_source'] = "Źródło";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['list_input'] = "Oznaczenie w źródle";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['list_output'] = "Oznaczenie lokalne";

$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['no_items'] = "Brak danych";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['edit'] = "Edytuj";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['delete'] = "Usuń";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['delete_q'] = "Czy na pewno usunąć ?";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['delete_all_q'] = "Czy na pewno usunąć wybrane ?";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['delete_all_err'] = "Nie wybrano żadnego elementu do usunięcia!";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['add'] = "Dodaj nowe mapowanie";

$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['header_0'] = "Dodawanie nowego mapowania stanu";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['header_1'] = "Edycja mapowania stanu";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['source'] = "Źródło";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['input'] = "Oznaczenie w źródle";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['output'] = "Oznaczenie lokalne";

$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['add_ok'] = "Dodano mapowanie \"%s\"";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['add_err'] = "Nie udało się dodać mapowania \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['edit_ok'] = "Zmieniono mapowanie \"%s\"";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['edit_err'] = "Nie udało się zmienić typu plików \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['del_ok_0']		= "Usunięto typ plików ";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['del_ok_1']		= "Usunięto typy plików ";
$aConfig['lang']['m_oferta_produktowa_stock_source_mappings']['del_err_0']		= "Nie udało się usunąć!<br>Spróbuj ponownie";

/** Konfiguracja porównywarek **/
$aConfig['lang']['m_oferta_produktowa_comparison_sites']['header'] = 'Konfiguracja porównywarek';
$aConfig['lang']['m_oferta_produktowa_comparison_sites']['add_text'] = 'Wprowadź dodatkowy tekst po tytule.';
$aConfig['lang']['m_oferta_produktowa_comparison_sites']['shipment'] = array();
$aConfig['lang']['m_oferta_produktowa_comparison_sites']['shipment'][0] = $aConfig['lang']['m_oferta_produktowa']['shipment_0'];
$aConfig['lang']['m_oferta_produktowa_comparison_sites']['shipment'][1] = $aConfig['lang']['m_oferta_produktowa']['shipment_1'];
$aConfig['lang']['m_oferta_produktowa_comparison_sites']['shipment'][2] = $aConfig['lang']['m_oferta_produktowa']['shipment_2'];
$aConfig['lang']['m_oferta_produktowa_comparison_sites']['shipment'][3] = $aConfig['lang']['m_oferta_produktowa']['shipment_3'];
$aConfig['lang']['m_oferta_produktowa_comparison_sites']['shipment']['t'] = _('Ilośc najczęściej kupowanych produktów');
$aConfig['lang']['m_oferta_produktowa_comparison_sites']['shipment']['d'] = _('Ilość dni branych pod uwagę podczas ustalania najczęściej kupowanych produktów');

$aConfig['lang']['m_oferta_produktowa_comparison_sites']['edit_ok'] = 'Wprowadzono zmiany w konfiguracji porównywarek';
$aConfig['lang']['m_oferta_produktowa_comparison_sites']['edit_err'] = 'Wystąpił błąd podczas zapisywania konfiguracji porównywarek';
?>