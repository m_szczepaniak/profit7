<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - rabat ogolny
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		$this->Show($pSmarty); 
	} // end of Module() method



	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['header'],
			'refresh'	=> true,
			'search'	=> false,
			'checkboxes'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			
			array(
				'db_field'	=> 'isbn',
				'content'	=> $aConfig['lang'][$this->sModule]['isbn'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'vat',
				'content'	=> $aConfig['lang'][$this->sModule]['value'],
				'sortable'	=> true,
				'width'	=> '40'
			)
		);

		//pobranie stawek z bazy
	  $sSql = "SELECT value FROM ".$aConfig['tabls']['prefix']."products_vat_stakes WHERE 1";
		
		$aStakes = Common::GetAll($sSql);
		$stakesList='';
		foreach($aStakes as $aStake) {
			$stakesList.=(strlen($stakesList)?',':'').intval($aStake['value']);
		}
		
		
		// pobranie liczby wszystkich rekordow ktore posiadaja stawke vat nie znajdujaca sie na liscie
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products WHERE vat NOT IN ($stakesList)";
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products WHERE vat NOT IN ($stakesList)";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_mismatch', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow + sorotwanie
			$sSql = "SELECT id, isbn, CONCAT('<a href=\"?frame=main&module_id=26&module=m_oferta_produktowa&do=edit&id=', id, '\">', name, '</a>'), vat FROM ".$aConfig['tabls']['prefix']."products WHERE vat NOT IN ($stakesList)".
			' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').				 
			" LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				)
				/*,
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'), array('action'))
				)*/
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			//array('check_all', 'delete_all'),
			//array('add')
		);

		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
		
	} // end of AddEdit() function
	
	function getAzymutParentPath($iId) {
		global $aConfig;
		
		$sSql="SELECT name, parent_id
					FROM ".$aConfig['tabls']['prefix']."products_azymut_categories
					WHERE id = ".$iId;
		$aItem = Common::GetRow($sSql);
		return ((!empty($aItem['parent_id'])&&($aItem['parent_id']!=$iId))?$this->getAzymutParentPath($aItem['parent_id']):'').$aItem['name'].' / ';
	}
	
	function getABEParentPath($iId) {
		global $aConfig;
		
		$sSql="SELECT name, parent_id
					FROM ".$aConfig['tabls']['prefix']."products_azymut_categories
					WHERE id = ".$iId;
		$aItem = Common::GetRow($sSql);
		return $aItem['name'].' / ';
	}

} // end of Module Class
?>