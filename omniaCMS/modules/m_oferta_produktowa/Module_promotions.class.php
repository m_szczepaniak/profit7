<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - promoje
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
use Ceneo\CeneoDataProvider;
use Service\ProductsTariffsService;

include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {

		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'promocje');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (!empty($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}
	if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}

		// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
      case 'discount': $this->Discounts($pSmarty, $iId); break;
      case 'update_discounts': $this->UpdateDiscounts($pSmarty, $iId); break;
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'placeOnMain': $this->PlaceOnMain($pSmarty, $iPId, true); break;
			case 'unplaceOnMain': $this->PlaceOnMain($pSmarty, $iPId, false); break;
			case 'remove': $this->RemoveFromPromotion($pSmarty, $iPId,$iId); break;
			case 'removeVal': $this->RemoveVal($pSmarty, $iPId,$iId); break;
			case 'insert': $this->Insert($pSmarty); break;
			case 'add_to_promo': $this->AddVal($pSmarty, $iPId); break;
			case 'insertBooks': $this->AddBooks($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'edit': $this->AddEditForm($pSmarty,$iId); break;
			case 'add': $this->AddEditForm($pSmarty); break;
			case 'search_books_add': $this->AddBooksForm($pSmarty, $iPId, true); break;	
			case 'add_books': $this->AddBooksForm($pSmarty, $iPId); break;					
			case 'books': $this->ShowBooks($pSmarty, $iPId); break;						
			case 'addFromFile': $this->addFromFile($pSmarty, $iPId); break;			
			case 'sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				if($_GET['pid']>0) {
					//widok dla sortowania książek
					$pSmarty->assign('sContent',$oSort->Show('', $this->getBooksInPromotion($_GET['pid']),
 						phpSelf(array('do'=>'proceed_sort','pid'=>$_GET['pid']),
 				 		array(), false)));
					}
				else {
					//widok do sortowania listy promocji w dziale	
				$pSmarty->assign('sContent',
													$oSort->Show('',
																			 $this->getRecords($_GET['fpid']),
																			 phpSelf(array('do'=>'proceed_sort','fpid'=>$_GET['fpid']),
																			 				 array(), false)));
					}
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				if($_GET['pid']>0) {
					$oSort->Proceed($aConfig['tabls']['prefix']."products_tarrifs",
												$this->getBooksInPromotion($_GET['pid']),
												array('promotion_id' => $_GET['pid']),
											 	'order_by');							
					}
				else {
					$oSort->Proceed($aConfig['tabls']['prefix']."products_promotions",
												$this->getRecords($_GET['fpid']),
												array('page_id' => $_GET['fpid']),
											 	'order_by');
				}
			break;
      case 'export_csv': $this->doExportCSV($iPId); break;
      
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method
  
  
  /**
   * Metoda aktualizuje rabaty w promocji oraz w produktach promocji
   * 
   * @param object $pSmarty
   * @param int $iId
   */
  public function UpdateDiscounts($pSmarty, $iId) {
		$aPromo = $this->getPromotion($iId);
    
    Common::BeginTransaction();
    try {
      $aLogCount = $this->_doUpdateDiscounts($_POST['discount'], $aPromo, $iId);
    } catch (Exception $exc) {
      Common::RollbackTransaction();
      $this->sMsg = $exc->getTraceAsString();
      addLog($this->sMsg);
      $this->sMsg = GetMessage($this->sMsg);
      $this->Show($pSmarty);
      return;
   }
   
   Common::CommitTransaction();
   $this->sMsg = _('Zmieniono stawki rabatów w ilości odpowiednio: '.implode($aLogCount, ', '));
   addLog($this->sMsg, false);
   $this->sMsg = GetMessage($this->sMsg, false);
   return $this->Show($pSmarty);
  }// end of UpdateDiscounts() method
  
  
  /**
   * Metoda zmiania rabaty promocji
   * 
   * @param array $aDiscounts
   * @param array $aPromo
   * @param id $iId
   * @return array
   * @throws Exception
   */
  private function _doUpdateDiscounts($aDiscounts, $aPromo, $iId) {
    $aLogCount = array();
    
    foreach ($aDiscounts as $iPromnoValueId => $fDiscount) {
      $fDiscount = Common::formatPrice2($fDiscount);
      
      // pobieramy starą wartość promocji
      $aPromoValue = $this->_getPromotionValue($iPromnoValueId);
      
      if (floatval($fDiscount) != floatval($aPromoValue['value'])) {
        // promocje zmieniły swoje wartości

        $sSql = "SELECT * FROM products_tarrifs WHERE promotion_id = ".$iId.' AND discount = '.$aPromoValue['value'];
        $aTarrifs = Common::GetAll($sSql);
        $aLogCount[] = count($aTarrifs);

        $aValues = array(
            'value' => Common::formatPrice2($fDiscount)
        );
        if (Common::Update('products_promotions_values', $aValues, ' id='.$iPromnoValueId.' AND promotion_id = '.$iId) === false) {
          throw new Exception('Wystąpił błąd podczas zmiany wartości promocji');
        }
        
        // zmianiamy cenniki
        foreach ($aTarrifs as $aTarrif) {
          $bIgnorePublisherDiscountLimit = ($aPromo['ignore_publisher_limit'] == '1');
          if ($this->_changeTarrif($aTarrif['product_id'], $aTarrif['id'], $fDiscount, $bIgnorePublisherDiscountLimit) === false) {
            throw new Exception('Wystąpił błąd podczas zmiany cennika produktu');
          }
        }

      }
    }
    return $aLogCount;
  }// end of _doUpdateDiscounts method
  
  
  /**
   * Metoda zmienia cennik produktu o zadanych wartościach
   * 
   * @param int $iProductId
   * @param int $iPromotionId
   * @param float $fNewDiscount
   * @param boolean $bIgnorePublisherDiscountLimit
   * @return boolean
   */
  private function _changeTarrif($iProductId, $iTarrifId, $fNewDiscount, $bIgnorePublisherDiscountLimit) {
    $aProduct = $this->getProductData($iProductId, $bIgnorePublisherDiscountLimit);

    // jako wartosc rabatu przyjmujemy limit
    if(($aProduct['discount_limit'] > 0) && ($fNewDiscount > $aProduct['discount_limit'])){
      $fDiscount = $aProduct['discount_limit'];
    } else {
      $fDiscount = $fNewDiscount;
    }
    $aValuesTarrif = $this->calculateTarrif($aProduct['price_brutto'], $aProduct['vat'], $fDiscount);
    if (Common::Update('products_tarrifs', $aValuesTarrif, ' id = '.$iTarrifId) === false) {
      return false;
    }
    return true;
  }// end of _changeTarrif() method
  
  
  /**
   * Metoda pobiera dane na temat wartości promocji o podanym id
   * 
   * @param int $iId
   * @return array
   */
  private function _getPromotionValue($iId) {
    
    $sSql = "SELECT * 
             FROM products_promotions_values 
             WHERE id = ".$iId;
    return Common::GetRow($sSql);
  }// end of _getPromotionValue() method
  
  
  /**
   * Metoda wyświetla formularz zmiany wartości rabatów
   * 
   * @param object $pSmarty
   * @param int $iId
   * @return
   */
  public function Discounts($pSmarty, $iId) {
		global $aConfig;
    
		$aData = array();
		if ($iId > 0) {
			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT id, value
               FROM products_promotions_values
							 WHERE promotion_id = ".$iId;
			$aDiscounts =& Common::GetAll($sSql);
		}
		
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
    
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = _('Edycja wartości rabatów');
		$pForm = new FormTable('discounts', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_discounts');
    
    foreach ($aDiscounts as $aDiscount) {
      $pForm->AddText('discount['.$aDiscount['id'].']', _('Rabat'), Common::formatPrice($aDiscount['value']), array(), '', 'uint');
    }
    
    // przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		
 		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				ShowTable($pForm->ShowForm()));
  }// end of Discounts() method


	/**
	 * Metoda wyswietla liste zdefiniowanych promocji
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		
		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'category',
				'content'	=> $aConfig['lang'][$this->sModule]['list_category'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'start_date',
				'content'	=> $aConfig['lang'][$this->sModule]['date_start'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'end_date',
				'content'	=> $aConfig['lang'][$this->sModule]['date_end'],
				'sortable'	=> true
			),/*
			array(
				'db_field'	=> 'discount',
				'content'	=> $aConfig['lang'][$this->sModule]['discount'],
				'sortable'	=> true
			),*/
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(A.id)
						 FROM ".$aConfig['tabls']['prefix']."products_promotions A
						 WHERE 1 = 1".
						(isset($_POST['f_page']) && $_POST['f_page'] != '' ? ' AND A.page_id = '.$_POST['f_page'] : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_promotions";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_promotions', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// dodanie filtru grupy
		$pView->AddFilter('f_page', $aConfig['lang'][$this->sModule]['list_category'], array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),getModulePages('m_powiazane_oferta_produktowa',$this->iLangId,true,'','promocje')), $_POST['f_page']);
		
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT A.id, A.name, A.page_id category, A.start_date, A.end_date, A.discount, MI.symbol
								FROM ".$aConfig['tabls']['prefix']."products_promotions A
               JOIN menus_items AS MI
                ON A.page_id = MI.id
							 WHERE 1 = 1".
							(isset($_POST['f_page']) && $_POST['f_page'] != '' ? ' AND A.page_id = '.$_POST['f_page'] : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			foreach ($aRecords as $iKey => $aItem) {
				$aRecords[$iKey]['discount'] = Common::formatPrice($aItem['discount']);
				
				$aRecords[$iKey]['category'] = $this->getPageName($aItem['category']);
        $aRecords[$iKey]['action']['links']['preview'] = '/'.$aItem['symbol'].'/id'.$aItem['id'].','.link_encode($aItem['name']) . '.html?preview=' . md5($_SERVER['REMOTE_ADDR'] . $aConfig['common']['client_base_url_http']);
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'symbol'	=> array(
					'show'	=> false
				),
				'links'	=> array(
					'show'	=> false
				),
				'discount'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('preview', 'books', 'sort', 'edit', 'discount', 'delete'),
					'params' => array (
                        'preview'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}'),
												'edit'	=> array('id'=>'{id}'),
												'books'	=> array('pid'=>'{id}'),
                        'discount'	=> array('id'=>'{id}'),
												'sort'	=> array('pid'=>'{id}')
											),
            'show' => false,
					'lang'=> array('sort'=>$aConfig['lang'][$this->sModule]['sort_items2'])				
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('sort', 'add')
		);
	
		$aRecordsFooterParams = array(
			'sort' => array(array('fpid' => $_POST['f_page']))
		);
		if (!isset($_POST['f_page']) || $_POST['f_page'] == '') {
			unset($aRecordsFooter[1][0]);
			unset($aRecordsFooterParams['sort']);
		}
		$_GET['pid']='';
		$pView->AddRecordsFooter($aRecordsFooter,$aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function
	
	
	/**
	 * Metoda wyswietla liste ksiazek promocji
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function ShowBooks($pSmarty, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');
		include_once('Form/FormTable.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule.'_books');
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT name
						 FROM ".$aConfig['tabls']['prefix']."products_promotions
							WHERE id = ".$iId;
		$sPromoName=Common::GetOne($sSql);

		$aHeader = array(
			//'header'	=> sprintf($aConfig['lang'][$this->sModule]['book_list'],$sPromoName),
			'action' => phpSelf(array('action' => 'promotions', 'do' => 'books', 'id' => $iId)),
			'refresh_link' => phpSelf(array('action' => 'promotions', 'do' => 'books', 'id' => $iId, 'reset' => '1')),
			'refresh'	=> false,
			'search'	=> false,
			'action' => phpSelf(array('do'=>'remove'), array(), false),
			'second' => true,
			'per_page' => false		);

		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'B.name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'profit_g_status',
				'content'	=> _('Status G'),
				'sortable'	=> true,
				'width'	=> '45'
			),
			array(
				'db_field'	=> 'placeOnMain',
				'content'	=> $aConfig['lang'][$this->sModule]['placeOnMainLabel'],
				'sortable'	=> true,
				'width'	=> '45'
			),
			array(
				'db_field'	=> 'base_price',
				'content'	=> $aConfig['lang'][$this->sModule]['list_base_price'],
				'sortable'	=> true,
				'width'	=> '45'
			),
			array(
				'db_field'	=> 'A.price_brutto',
				'content'	=> $aConfig['lang'][$this->sModule]['list_price_brutto'],
				'sortable'	=> true,
				'width'	=> '55'
			),
			array(
				'db_field'	=> 'A.discount',
				'content'	=> $aConfig['lang'][$this->sModule]['discount'],
				'sortable'	=> true,
				'width'	=> '50'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);
		
		
		$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'order_by'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('delete','preview'),
					'params' => array (
												'delete'	=> array('do'=>'remove','id'=>'{id}')
											),
					'show' => false
				)
			);
			
		$aRecordsFooter = array(
			array('check_all', 'remove_all', 'place_on_main', 'unplace_on_main')
		);
		$aRecordsFooterParams = array(
			
		
												 );

		$pForm = new FormTable('books_discount_search', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'add_to_promo');
		
		//$pForm->AddText('discount',$aConfig['lang'][$this->sModule]['discount_stake'],$aData['s_product_name'],array('style'=>'width:300px;'),'','text',false);
		
			
		//$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'books', 'pid'=>$iPId)).'\');'), 'button'));
												 
		/*$pView2 = new View('promotions_books', $aHeader2, $aAttribs2);
		$pView2->AddRecordsHeader($aRecordsHeader2);
		
		*/
		$sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'products_promotions_values WHERE promotion_id='.$iId;
		$iRowCount = intval(Common::GetOne($sSql));
		$pForm->AddMergedRow(sprintf($aConfig['lang'][$this->sModule]['book_list2'], $sPromoName), array('class'=>'merged', 'style'=>'padding-left: 10px; background-color:#736f54; color:#fff; '));
					
		$iViewCounter=0;
		if($iRowCount>0) {
			$sSql='SELECT id, \'\' AS book_list, value FROM '.$aConfig['tabls']['prefix'].'products_promotions_values WHERE promotion_id='.$iId.' ORDER BY value';
				$aRecords =& Common::GetAll($sSql);
			foreach ($aRecords as $iKey0 => $aItem) {//wyswietlenie kazdej stawki promocji
					$sShowHideButton=$pForm->GetInputButtonHTML('btnShowHide'.$aItem['id'], $aConfig['lang'][$this->sModule]['show'], array('style'=>'margin-right:100px; width:70px;', 'onclick'=>'showHideRow('.$aItem['id'].');'), 'button').'&nbsp;';
					$pForm->AddMergedRow($sShowHideButton.$aConfig['lang'][$this->sModule]['discount2'].' '.Common::formatPrice($aItem['value']).$aConfig['lang'][$this->sModule]['discount3'], array('class'=>'merged', 'style'=>'padding-left: 10px; background-color:#736f54; color:#fff; '));
					
					
					//dla kazdej stawki pobieramy liste książek
					$pView = new View('promotions_books'.(++$iViewCounter), $aHeader, $aAttribs);
					$pView->AddRecordsHeader($aRecordsHeader);
					
					$sSql = "SELECT COUNT(A.id)
						 FROM ".$aConfig['tabls']['prefix']."products_tarrifs A
						 JOIN ".$aConfig['tabls']['prefix']."products B
						 ON B.id = A.product_id
						 WHERE A. discount=".$aItem['value']." AND  A.promotion_id = ".$iId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? (is_numeric($_POST['search']) ? ' AND B.id = '.(int) $_POST['search'] : ' AND B.name LIKE \'%'.$_POST['search'].'%\'') : '');
					$iRowCount2 = intval(Common::GetOne($sSql));
					$aBooks=array();
					if ($iRowCount2 > 0) {
						//jeżeli są jakieś ksiazki
						$sSql = "SELECT A.id, B.name, PS.profit_g_status, A.placeOnMain ,B.price_brutto base_price, A.price_brutto, A.discount, C.name AS publisher, A.product_id, B.publication_year, (SELECT order_by FROM ".$aConfig['tabls']['prefix']."products_shadow S WHERE S.id=B.id) AS order_by 
								FROM ".$aConfig['tabls']['prefix']."products_tarrifs A
							 JOIN ".$aConfig['tabls']['prefix']."products B
							 ON B.id = A.product_id
							 JOIN ".$aConfig['tabls']['prefix']."products_stock PS
							 ON PS.id = A.product_id
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers C
							 	 ON B.publisher_id = C.id
							 WHERE A. discount=".$aItem['value']." AND A.promotion_id = ".$iId.
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '');/*.
							 " LIMIT ".$iStartFrom.", ".$iPerPage;*/
						$aBooks =& Common::GetAll($sSql);
						
						foreach ($aBooks as $iKey => $aBook) {
              $aBooks[$iKey]['profit_g_status'] = ( $aBook['profit_g_status'] > 0 ? '<span style="font-size: 44px; color: #fff; background-color: black;">'.$aBook['profit_g_status'].'</span>' : $aBook['profit_g_status']);
							$aBooks[$iKey]['base_price'] = Common::formatPrice($aBook['base_price']);
							$aBooks[$iKey]['price_brutto'] = Common::formatPrice($aBook['price_brutto']);
							$aBooks[$iKey]['discount'] = Common::formatPrice($aBook['discount']);
							$aBooks[$iKey]['name'] = '<b><a href="'.createProductLink($aBook['product_id'], $aBook['name']).'" target="_blank">'.$aBook['name'].'</a></b>'.(!empty($aBook['publisher'])?'<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($aBook['publisher'],0,40,'UTF-8').'</span>':'');
							$aBooks[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aBook['product_id']),0,40,'UTF-8').' '.$aBook['publication_year'].'</span>';
							$aBooks[$iKey]['placeOnMain']='<span style="color:#'.($aBook['placeOnMain']?'0c0;">TAK':'c00;">NIE').'</span>';
							unset($aBooks[$iKey]['publisher']);
							//$aBooks[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aBook['product_id']),0,40,'UTF-8').' '.$aBook['publication_year'].'</span>';
							unset($aBooks[$iKey]['publication_year']);
							// link podgladu
							$aBooks[$iKey]['action']['links']['preview'] = (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'].'/' : '').(mb_strlen($aBook['name'], 'UTF-8')>210?mb_substr(link_encode($aBook['name']),0,210, 'UTF-8'):link_encode($aBook['name'])).',product'.$aBook['product_id'].'.html?preview='.md5($_SERVER['REMOTE_ADDR'].$aConfig['common']['client_base_url_http']);
							unset($aBooks[$iKey]['product_id']);	
						
						}		 
				}
					$pView->AddRecords($aBooks, $aColSettings);
					$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
					//dodanie tej formy jest konieczne ze względu na błąd interpretacjis truktury DOM w firefox
					$pForm->AddMergedRow(('<form name="NiewidocznaWDOM"></form>').$pView->Show(), array(), array( 'id'=>'listContainer'.$aItem['id'], 'style'=>' display:none;'));
					$pForm->AddMergedRow($pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['add_product_w_disc'].Common::formatPrice($aItem['value']).$aConfig['lang'][$this->sModule]['discount3'], array('onclick'=>'openSearchWindow('.$aItem['value'].'); return false;', 'style'=>'float:left; margin-left:10px;')).(!$iRowCount2?$pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['del_discount'], array('onclick'=>'window.location.href=\'?frame=main&module_id=26&module=m_oferta_produktowa&action=promotions&do=removeVal&pid='.$iId.'&id='.$aItem['id'].'\'; return false;','style'=>'margin-top:-2px; margin-left:20px; float:right;')):'&nbsp;').'<form style="margin-top:-3px; float:right;" action="" method="post" enctype="multipart/form-data"><input type="file" name="file" />&nbsp;&nbsp;<input type="submit" value="'.$aConfig['lang'][$this->sModule]['add_from_file'].Common::formatPrice($aItem['value']).$aConfig['lang'][$this->sModule]['discount3'].'" /><input type="hidden" name="do" value="addFromFile" /><input type="hidden" name="discount" value="'.$aItem['value'].'" /></form>'.($_COOKIE['displayedPromotionVal'.$aItem['id']]?'<script>showHideRow('.$aItem['id'].')</script>':''), array(), array( 'id'=>'listContainer2_'.$aItem['id'], 'style'=>' display:none;'));
					
			}
		}
    

		$pForm->AddMergedRow(
            '<div style="float: left;">'.
            $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['add_books'], array('onclick'=>'window.location.href=\'?frame=main&module_id=26&module=m_oferta_produktowa&action=promotions&do=add_books&pid='.$iId.'\'; return false;'))
            .'&nbsp;'.
            $pForm->GetInputButtonHTML('export_csv', _('Exportuj do CSV'), array('onclick'=>'window.location.href=\'?frame=main&module_id=26&module=m_oferta_produktowa&action=promotions&do=export_csv&pid='.$iId.'\'; return false;'))
            .'</div>'.
            $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['m_oferta_produktowa']['go_back'], array('style'=>'float:right;','onclick'=>'window.location.href=\''.phpSelf().'\'; return false;')));
		
					
		$sJs='<script>function openSearchWindow(disc) {
				window.open("ajax/SearchProduct.php?ppid='.$iPId.'&pid='.$iId.'&site=profit24&mode=2&disc="+disc,"","menubar=0,location=0,scrollbars=1,resizable=1,toolbar=0,width=700,height=500");
				};
				
				function createCookie(name,value, days) {
					if (days) {
						var date = new Date();
						date.setTime(date.getTime()+(days*24*60*60*1000));
						var expires = "; expires="+date.toGMTString();
					}
					else var expires = "";
					document.cookie = name+"="+value+expires+"; path=/";
				}
				
				function eraseCookie(name) {
					createCookie(name,"",-1);
				}
				
				function showHideRow(id) {
					var status = 		document.getElementById("listContainer"+id).style.display;
					var newStatus=	"none";
					var label=			"'.$aConfig['lang'][$this->sModule]['show'].'";
					eraseCookie("displayedPromotionVal"+id);
					if(status=="none") {
						newStatus="";
						label=			"'.$aConfig['lang'][$this->sModule]['hide'].'";
						createCookie("displayedPromotionVal"+id,1);
						}
					
					document.getElementById("listContainer"+id).style.display=newStatus;
					document.getElementById("listContainer2_"+id).style.display=newStatus;
					document.getElementById("btnShowHide"+id).value=label;
					}
				</script>';
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJs.
																				ShowTable($pForm->ShowForm()));
	} // end of Show() function


	/**
	 * Metoda ustawia status placeOnMain oznaczajacy obecnosc danego rpoduktu w boxie na stronie głównej
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id promocji
	 * @param bool $bStat - status pola
	 * @return 	void
	 */
	function placeOnMain(&$pSmarty, $iId,$bStat) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 1;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;
	 	$aValues=array('placeOnMain'=> ($bStat?'1':'0'));
		
		//dump($_POST['delete']);
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			
			foreach ($_POST['delete'] as $iItem) {
				// aktualizowanie
				$aValues=array('placeOnMain'=> ($bStat?'1':'0'));
				if ((Common::Update($aConfig['tabls']['prefix']."products_tarrifs",
						 $aValues,'id = '.$iItem,
						 false)) === false) {
							$bIsErr = true;
							break;
						}
				/*if (($iRecords = $this->deleteItem($iItem)) === false) {
					$bIsErr = true;
					break;
				}*/
			}
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg =$aConfig['lang']['m_oferta_produktowa_promotions_books']['plc_ok'];
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang']['m_oferta_produktowa_promotions_books']['plc_err'];
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$_POST['do']='books';
		$_GET['do']='books';
		$this->ShowBooks($pSmarty, $iId);
	} // end of Delete() funciton

	/**
	 * Metoda usuwa wybrane produkty z promocji
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego produktu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			foreach ($_POST['delete'] as $iItem) {
				// najpierw pobierzemy ksiażki w danej promocji
				$aProdIds = $this->getProductIdByPromotion($iItem);
				// usuwanie
				if (($iRecords = $this->deleteItem($iItem)) === false) {
					$bIsErr = true;
					break;
				}
				
				foreach ($aProdIds as $iProdId) {
					// przeliczmy teraz pakiety z tym produktem
					// jeśli produkt jest składową pakietu, aktualizacja tych pakietów
					$aPacketsProduct = $this->getPacketsProduct($iProdId);
					foreach ($aPacketsProduct as $iPacketProduct) {
						if ($this->updateProductPacketPrice($iProdId, $iPacketProduct) === false) {
							$bIsErr = true;
							break;
						}
						if ($this->updatePacketAuthorsPrices($iPacketProduct) === false) {
							$bIsErr = true;
							break;
						}
					}
				}
			}
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg =$aConfig['lang'][$this->sModule]['del_ok'];
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['del_err'];
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton
	
	
	/**
	 * Metoda pobiera idki książek które należą do pakietu na podstawie tabeli cenników
	 *
	 * @param integer $iPromotionId 
	 */
	function getProductIdByPromotion($iPromotionId) {
		global $aConfig;
		
		$sSql = "SELECT product_id FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						 WHERE promotion_id=".$iPromotionId;
		return Common::GetCol($sSql);
	}// end of getProductIdByPromotion() method
	
	
	/**
	 * Metoda usuwa wybrane produkty z promocji
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego produktu
	 * @return 	void
	 */
	function RemoveFromPromotion(&$pSmarty, $iPId,$iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			foreach ($_POST['delete'] as $iItem) {
				// usuwanie
				$iProdId = $this->getProductIdByTarrif($iItem);
				if (($iRecords = $this->deletePromotionItem($iItem)) === false) {
					$bIsErr = true;
					break;
				}
				
				// przeliczmy teraz pakiety z tym produktem
				// jeśli produkt jest składową pakietu, aktualizacja tych pakietów
				$aPacketsProduct = $this->getPacketsProduct($iProdId);
				foreach ($aPacketsProduct as $iPacketProduct) {
					if ($this->updateProductPacketPrice($iProdId, $iPacketProduct) === false) {
						$bIsErr = true;
						break;
					}
					if ($this->updatePacketAuthorsPrices($iPacketProduct) === false) {
						$bIsErr = true;
						break;
					}
				}
			}
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if (1) {
					$sMsg =$aConfig['lang'][$this->sModule]['del_prom_ok'];
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['del_prom_err'];
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$_GET['do']='books';
		$this->ShowBooks($pSmarty,$iPId);
	} // end of Delete() funciton
	
	/**
	 * Metoda usuwa wybrane produkty z promocji
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego produktu
	 * @return 	void
	 */
	function RemoveVal(&$pSmarty, $iPId,$iId) {
		global $aConfig;
		$bIsErr = false;
		
		Common::BeginTransaction();
		$bIsErr=!$this->deleteVal($iId);	
		if (!$bIsErr) {
					// usunieto
					Common::CommitTransaction();
					
						$sMsg =$aConfig['lang'][$this->sModule]['del_val_ok'];
						$this->sMsg = GetMessage($sMsg, false);
						// dodanie informacji do logow
						AddLog($sMsg, false);
					
				}
				else {
					// blad
					Common::RollbackTransaction();
					$sMsg = $aConfig['lang'][$this->sModule]['del_val_err'];
					$this->sMsg = GetMessage($sMsg);
					// dodanie informacji do logow
					AddLog($sMsg);
				}	
		/*$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			foreach ($_POST['delete'] as $iItem) {
				// usuwanie
				if (($iRecords = $this->deletePromotionItem($iItem)) === false) {
					$bIsErr = true;
					break;
				}
			}
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg =$aConfig['lang'][$this->sModule]['del_prom_ok'];
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = $aConfig['lang'][$this->sModule]['del_prom_err'];
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}*/
		$_GET['do']='books';
		$this->ShowBooks($pSmarty,$iPId);
	} // end of RemoveVal() funciton

	/**
	 * Metoda dodaje produkt do promocji
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST) || $oValidator->sError != '') {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEditForm($pSmarty);
			return;
		}
		

		$_POST['discount'] = Common::formatPrice2($_POST['discount']);
		if (strtotime($_POST['start_date'].' '.$_POST['start_time'])<strtotime($_POST['end_date'].' '.$_POST['end_time'])) {/*$_POST['discount'] > 0 && $_POST['discount'] < 100*/
			Common::BeginTransaction();
			
			$aValues=array(
				'name' => $_POST['name'],
				'start_date'	=>	FormatDateHour($_POST['start_date'], $_POST['start_time'] ),
				'end_date'	=>	FormatDateHour($_POST['end_date'], $_POST['end_time']),
				'discount' =>	Common::formatPrice2($_POST['discount']),
				'show_in_box' => isset($_POST['show_in_box']) ? '1' : '0',
        'ignore_publisher_limit' => isset($_POST['ignore_publisher_limit']) ? '1' : '0',
				'ignore_ceneo_tarrifs'	=>	$_POST['ignore_ceneo_tarrifs'] == 1 ? 1 : 0,
				'product_limit'	=>	(int)$_POST['product_limit'],
				'vip'	=>	$_POST['vip'] == 1 ? 1 : 0,
				'boxOpt' => $_POST['boxOpt'],
				'page_id' => $_POST['page_id']
			);
			if (($iPId=Common::Insert($aConfig['tabls']['prefix']."products_promotions",
											 $aValues)) === false) {
				$bIsErr = true;
			}
					
			if (!$bIsErr) {
				// dodano
				Common::CommitTransaction();

				if (isset($_POST['ignore_ceneo_tarrifs']) && $_POST['ignore_ceneo_tarrifs'] == 1) {

					$cDataProvier = new CeneoDataProvider();
					$cDataProvier->removeIgnoredPromotionsProducts();
				}

				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
												$_POST['name']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				// reset ustawien widoku
				$_POST['f_page']=$_POST['page_id'];
				$_GET['reset'] = 2;
				$this->Show($pSmarty);
			}
			else {
				// blad
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err']);
				$this->sMsg = GetMessage($sMsg);
	
				AddLog($sMsg);
				$this->AddForm($pSmarty);
			}

		}
		else {
			// wysokosc rabatu musi byc wieksza od 0
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['date_value_err']);
			$this->AddEditForm($pSmarty);
		}
	} // end of AddItem() funciton\
	
		/**
	 * Metoda dodaje wartość rabatu do promocji
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function AddVal(&$pSmarty,$iPId) {
		global $aConfig;
		$bIsErr = false;
		set_time_limit(3600);
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		$sAdded='';
		// odwrocenie tablicy z ID
		//$_POST['delete'] = array_keys($_POST['delete']);
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST) || $_POST['discount']<0) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			if($_POST['discount']<0)
				$this->sMsg =GetMessage($aConfig['lang'][$this->sModule]['under_zero'], true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddBooksForm($pSmarty,$iPId);
			return;
		}
		Common::BeginTransaction();
		$aPromo = $this->getPromotion($iPId);

		$aValues['promotion_id']=	$iPId;
		$aValues['value']=				str_replace(',', '.', $_POST['discount']);
		//dump($_POST);
		if($aValues['value']>=0 && $aValues['value']<100) {
				if (Common::Insert($aConfig['tabls']['prefix']."products_promotions_values", $aValues,'', false) === false) 
					$bIsErr=true;
					
				if (!$bIsErr) {
					// dodano
					Common::CommitTransaction();
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_discount_ok'],
													$_POST['discount'],
													$aPromo['name']);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
					// reset ustawien widoku
					$_GET['reset'] = 2;
					$_POST['do']=$_GET['do']='books';
					$this->ShowBooks($pSmarty, $iPId);
				}
				else {
					// blad
					// wyswietlenie komunikatu o niepowodzeniu
					// oraz ponowne wyswietlenie formularza dodawania
					Common::RollbackTransaction();
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_topromo_err'],
													$_POST['discount']);
					$this->sMsg = GetMessage($sMsg);
		
					AddLog($sMsg);
					$this->ShowBooks($pSmarty, $iPId);
				}
		}else {
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['discount_value_err']);
			$this->AddBooksForm($pSmarty, $iPId);
		}

	} // end of
	
	/**
	 * Metoda dodaje produkty do promocji z pliku csv
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPid	
	 * @return	void
	 */
	function addFromFile(&$pSmarty,$iPId) {
		global $aConfig;
		//jezeli plik został dodany i istnieje
		if(isset($_FILES['file']) && !empty($_FILES['file'])) {
			if($_FILES['file']['size']) {
				$aLista=file($_FILES['file']['tmp_name']);
				$_GET['discount']=$_POST['discount'];
				$aAId=array();
				foreach($aLista as $sIsbn) {
					$sIsbn=str_replace('-','', trim($sIsbn));
					$sIsbn=str_replace(' ','', $sIsbn);
					
					$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products WHERE isbn_plain='$sIsbn'";
					$iItemId=Common::GetOne($sSql);
					if($iItemId)
						$aAId[]=$iItemId;
					}
				$_GET['aid']=implode(',',$aAId);	
				//dump($aLista);
				$this->AddBooks($pSmarty, $iPId);
				}	
			else $bIsErr=true;	
			}
		else $bIsErr=true;
		
		if($bIsErr) {
			$this->sMsg=GetMessage($aConfig['lang'][$this->sModule]['add_topromo_fileError'], true);
			$this->ShowBooks($pSmarty, $iPId);
			}
	}	
	
	
	function AddBooks(&$pSmarty,$iPId) {
		global $aConfig;
		$bIsErr = false;
		set_time_limit(3600);
		//dump($_GET['discount']);
		//dump($_GET['aid']);
		
		$aSelItems=explode(',', $_GET['aid']);
		Common::BeginTransaction();
		$aPromo = $this->getPromotion($iPId);

		foreach ($aSelItems as $iBId) {
      $bIgnorePublisherDiscountLimit = ($aPromo['ignore_publisher_limit'] == '1');
			$aProduct=$this->getProductData((int)$iBId, $bIgnorePublisherDiscountLimit);
			$sSql='SELECT COUNT(id) FROM '.$aConfig['tabls']['prefix'].'products_tarrifs WHERE promotion_id='.$iPId.' AND product_id='.$iBId;
			$iBooksCountById=Common::GetOne($sSql);
			if($iBooksCountById>0) {$sMsg1=$aConfig['lang'][$this->sModule]['info001']; continue;}
			
			// nie można dodać do promocji produktu w statusie: 
			// nakład wyczerpany, niedostępna, lub nieopublikowana
      /*
			if ($this->checkProductStatus($iBId) === false) {
				$sMsg1 = sprintf($aConfig['lang'][$this->sModule]['book_status_err'], $this->getItemName($iBId));
				continue;
			}
			*/

			// jesli ustawiony limit dla wydawnictwa, i rabat jest od niego wiekszy,
			// jako wartosc rabatu przyjmujemy limit
			if(($aProduct['discount_limit'] > 0) && ($_GET['discount'] > $aProduct['discount_limit'])){
				$fDiscount = $aProduct['discount_limit'];
			} else {
				$fDiscount = $_GET['discount'];
			}
			$aValues=$this->calculateTarrif($aProduct['price_brutto'],$aProduct['vat'],$fDiscount);
			$aValues['start_date']=CalDateTimeToTimestamp(substr($aPromo['start_date'],0,16));
			$aValues['end_date']=CalDateTimeToTimestamp(substr($aPromo['end_date'],0,16));
			$aValues['product_id']= (int)$iBId;
			$aValues['promotion_id']= (int)$iPId;
      $aValues['placeOnMain'] = '0';
      
				//echo'| '.$aValues['product_id'].' '.$_GET['discount'].'=>'.$fDiscount;
			if($aValues['discount'] != $_GET['discount']) {
				$bDiffrentDiscount=1;
				$sSql="SELECT COUNT(id) FROM ".$aConfig['tabls']['prefix']."products_promotions_values WHERE promotion_id=$iPId AND value=".$aValues['discount'];
				if(Common::GetOne($sSql)<1) {
					$aValues2=array();
					$aValues2['promotion_id']=	$iPId;
					$aValues2['value']=				$aValues['discount'];
					if (Common::Insert($aConfig['tabls']['prefix']."products_promotions_values", $aValues2,'', false) === false) 
						$bIsErr+=1;
					}
					$sAdded2.=$aProduct['name'];
				}

      
			if (Common::Insert($aConfig['tabls']['prefix']."products_tarrifs",
												 $aValues,'',
												 false) === false) {
				$bIsErr = true;
			} else { $sAdded.=$aProduct['name'].", "; }

		}
			
		foreach ($aSelItems as $iBId) {
			// przeliczmy teraz pakiety z tym produktem
			// jeśli produkt jest składową pakietu, aktualizacja tych pakietów
			$aPacketsProduct = $this->getPacketsProduct($iBId);
			foreach ($aPacketsProduct as $iPacketProduct) {
				if ($this->updateProductPacketPrice($iBId, $iPacketProduct) === false) {
					$bIsErr = true;
					break;
				}
				if ($this->updatePacketAuthorsPrices($iPacketProduct) === false) {
					$bIsErr = true;
					break;
				}
			}
		}

		$sAdded=substr($sAdded,0,-2);

		$tariffsService = new ProductsTariffsService();


		$vipStatus = ProductsTariffsService::NON_VIP_STATUS;

		if ($aPromo['vip'] == 1) {

			$vipStatus = ProductsTariffsService::VIP_STATUS;
		}

		$tariffsService->changeTariffsVipStatus($iPId, $vipStatus);

		if (!$bIsErr) {
			// dodano
			Common::CommitTransaction();
			if(!empty($sAdded))
				$sMsg .= sprintf($aConfig['lang'][$this->sModule]['add_topromo_ok'],
											$sAdded,
											$aPromo['name']);
			if($bDiffrentDiscount)	
				$sMsg2=$aConfig['lang'][$this->sModule]['info002'];
			if($sAdded2!='')	
				$sMsg3=$aConfig['lang'][$this->sModule]['info003'];							

			if(!empty($sMsg)) $this->sMsg = GetMessage($sMsg, false);
			if(!empty($sMsg1)) $this->sMsg .= GetMessage($sMsg1, true);
			if(!empty($sMsg2)) $this->sMsg .= GetMessage($sMsg2, true);
			if(!empty($sMsg3)) $this->sMsg .= GetMessage($sMsg3, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			// blad
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_topromo_err'],
											$aPromo['name']);
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			$_GET['do']='books';
			$this->ShowBooks($pSmarty, $iPId);
		}
		$_GET['do']='books';
		$this->ShowBooks($pSmarty, $iPId);
	} // end of AddItem() funciton\
	
	/**
	 * Metoda dodaje produkt do promocji
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty,$iId) {
		global $aConfig;
		$bIsErr = false;
		set_time_limit(3600);

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		$sAdded='';
		
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddForm($pSmarty);
			return;
		}
		$_POST['discount'] = Common::formatPrice2($_POST['discount']);
		if (strtotime($_POST['start_date'].' '.$_POST['start_time'])<strtotime($_POST['end_date'].' '.$_POST['end_time'])) {//$_POST['discount'] > 0 && 
			Common::BeginTransaction();
			
			$aValues=array(
				'name' => $_POST['name'],
				'start_date'	=>	FormatDateHour($_POST['start_date'], $_POST['start_time']),
				'end_date'	=>	FormatDateHour($_POST['end_date'], $_POST['end_time']),
				'ignore_ceneo_tarrifs'	=>	$_POST['ignore_ceneo_tarrifs'] == 1 ? 1 : 0,
				'product_limit'	=>	(int)$_POST['product_limit'],
				'vip'	=>	$_POST['vip'] == 1 ? 1 : 0,
				'discount' =>	Common::formatPrice2($_POST['discount']),
				'show_in_box' => isset($_POST['show_in_box']) ? '1' : '0',
        'ignore_publisher_limit' => isset($_POST['ignore_publisher_limit']) ? '1' : '0',
				'boxOpt' => $_POST['boxOpt'],
				'page_id' => $_POST['page_id']
			);
			if ((Common::Update($aConfig['tabls']['prefix']."products_promotions",
											 $aValues,
											 'id = '.$iId)) === false) {
				$bIsErr = true;
			}
			
			
				$_POST['f_page']=$_POST['page_id'];
			// pobranie wszystkich rekordow
			$sSql = "SELECT A.id, B.name, B.vat, B.price_brutto base_price, A.price_brutto, A.discount, IFNULL(C.discount_limit,0) AS discount_limit
								FROM ".$aConfig['tabls']['prefix']."products_tarrifs A
							 JOIN ".$aConfig['tabls']['prefix']."products B
							  ON B.id = A.product_id
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers C
						 		ON C.id = B.publisher_id
							 WHERE A.promotion_id = ".$iId;
			$aTarrifs =& Common::GetAll($sSql);
			
			
			
			foreach ($aTarrifs as $aTarrif) {
				$aValues = array();
				/*// jesli ustawiony limit dla wydawnictwa, i rabat jest od niego wiekszy,
				// jako wartosc rabatu przyjmujemy limit
				if($_POST['discount'] > $aTarrif['discount_limit']){//($aTarrif['discount_limit'] > 0) && ()
					$fDiscount = $aTarrif['discount_limit'];
				} else {
					$fDiscount = $_POST['discount'];
				}
				if($fDiscount != $aTarrif['discount']){
					$aValues=$this->calculateTarrif($aTarrif['base_price'],$aTarrif['vat'],$fDiscount);
				}*/
				$aValues['start_date']=CalDateTimeToTimestamp($_POST['start_date'].' '.$_POST['start_time']);
				$aValues['end_date']=CalDateTimeToTimestamp($_POST['end_date'].' '.$_POST['end_time']);

				
					// update
				if (Common::Update($aConfig['tabls']['prefix']."products_tarrifs",
													 $aValues,'id = '.$aTarrif['id'],
													 false) === false) {
					$bIsErr = true;
				} 
				
			}

			$tariffsService = new ProductsTariffsService();


			$vipStatus = ProductsTariffsService::NON_VIP_STATUS;

			if (isset($_POST['vip']) && $_POST['vip'] == 1) {

				$vipStatus = ProductsTariffsService::VIP_STATUS;
			}

			$tariffsService->changeTariffsVipStatus($iId, $vipStatus);

					
				if (!$bIsErr) {
					// dodano
					Common::CommitTransaction();

					if (isset($_POST['ignore_ceneo_tarrifs']) && $_POST['ignore_ceneo_tarrifs'] == 1) {

						$cDataProvier = new CeneoDataProvider();
						$cDataProvier->removeIgnoredPromotionsProducts();
					}

					$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],$_POST['name']);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
					// reset ustawien widoku
					//$_GET['reset'] = 2;
					$this->Show($pSmarty);
				}
				else {
					// blad
					// wyswietlenie komunikatu o niepowodzeniu
					// oraz ponowne wyswietlenie formularza dodawania
					Common::RollbackTransaction();
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],$_POST['name']);
					$this->sMsg = GetMessage($sMsg);
		
					AddLog($sMsg);
					$this->AddForm($pSmarty);
				}

		}
		else {
			// wysokosc rabatu musi byc wieksza od 0
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['date_value_err']);
			$this->AddEditForm($pSmarty);
		}
	} // end of AddItem() funciton
	

	/**
	 * Metoda tworzy formularz wyboru produktu, ktory ma zostac dodany do listy
	 * promocji
	 *
	 * @param	object	$pSmarty
	 */
	function AddEditForm(&$pSmarty, $iId=0) {
		global $aConfig;
		if($_POST['id']>0)	$iId=intval($_POST['id']);		
		$aData = array();
		$sHtml = '';
		if ($iId > 0) {
			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT *,DATE_FORMAT(start_date, '".$aConfig['common']['cal_sql_date_format']."')
											AS start_date, 
											DATE_FORMAT(start_date, '".$aConfig['common']['sql_hour_format']."') AS start_time,
											DATE_FORMAT(end_date, '".$aConfig['common']['cal_sql_date_format']."')
											AS end_date,
											DATE_FORMAT(end_date, '".$aConfig['common']['sql_hour_format']."') AS end_time,
											ignore_ceneo_tarrifs, product_limit,
											vip
							 FROM ".$aConfig['tabls']['prefix']."products_promotions
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId>0?1:0)];
		if ($iId > 0) {
			$sHeader .= ' "'.$aData['name'].'"';
		}
				
		$pForm = new FormTable('books_discount', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		if ($iId > 0) {
			$pForm->AddHidden('do', 'update');
			$pForm->AddHidden('id', $iId);
		}
		else {
			$pForm->AddHidden('do', 'insert');
		}
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['new_discount_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		// nazwa promocji
		$pForm->AddText('name',$aConfig['lang'][$this->sModule]['name'],$aData['name'],array('style'=>'width:300px;'));
		// rabat w procentach
		//$pForm->AddText('discount', $aConfig['lang'][$this->sModule]['discount'], Common::formatPrice($aData['discount']), array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');
		// data początkowa
		//$pForm->AddText('start_date', $aConfig['lang'][$this->sModule]['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date');
		//$pForm->AddText('start_time', $aConfig['lang'][$this->sModule]['start_time'], !empty($aData['start_time'])?$aData['start_time']:'07:00:00', array('style'=>'width: 40px;', 'maxlength'=>'5'), '');
		$pForm->AddRow($aConfig['lang'][$this->sModule]['start_date'],
									 '<div style="float: left;">'.$pForm->GetTextHTML('start_date', $aConfig['lang'][$this->sModule]['start_date'], $aData['start_date'], array('style'=>'width: 75px;'), '', 'date', false).'</div><div style="float: left; padding-top: 3px; padding-left: 10px;">'.$pForm->GetLabelHTML('start_time', $aConfig['lang'][$this->sModule]['hour'], false).' '.$pForm->GetTextHTML('start_time', $aConfig['lang'][$this->sModule]['start_hour'], !empty($aData['start_time'])?$aData['start_time']:'07:00', array('style'=>'width: 50px;'), '', 'hour', false).'</div>');
		
		// data końcowa
		//$pForm->AddText('end_date', $aConfig['lang'][$this->sModule]['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date');
		//$pForm->AddText('end_time', $aConfig['lang'][$this->sModule]['end_time'], !empty($aData['end_time'])?$aData['end_time']:'23:59:59', array('style'=>'width: 40px;', 'maxlength'=>'5'), '');
		$pForm->AddRow($aConfig['lang'][$this->sModule]['end_date'],
									 '<div style="float: left;">'.$pForm->GetTextHTML('end_date', $aConfig['lang'][$this->sModule]['end_date'], $aData['end_date'], array('style'=>'width: 75px;'), '', 'date', false).'</div><div style="float: left; padding-top: 3px; padding-left: 10px;">'.$pForm->GetLabelHTML('end_time', $aConfig['lang'][$this->sModule]['hour'], false).' '.$pForm->GetTextHTML('end_time', $aConfig['lang'][$this->sModule]['end_hour'], !empty($aData['end_time'])?$aData['end_time']:'23:59', array('style'=>'width: 50px;'), '', 'hour', false).'</div>');
		
		// wyswietlaj w boxie
		$pForm->AddCheckBox('show_in_box', $aConfig['lang'][$this->sModule]['show_in_box'], array(), '', !empty($aData['show_in_box']), false);
		
		// ignoruj limit rabatu wydawnictwa
		$pForm->AddCheckBox('ignore_publisher_limit', $aConfig['lang'][$this->sModule]['ignore_publisher_limit'], array(), '', !empty($aData['ignore_publisher_limit']), false);
		$pForm->AddCheckBox('ignore_ceneo_tarrifs', _('Ignoruj cenniki ceneo'), array(), '', !empty($aData['ignore_ceneo_tarrifs']), false);
		$pForm->AddText('product_limit', _('Limit produktu na użytkownika'), empty($aData['product_limit']) ? 0 : $aData['product_limit']);
		$pForm->AddCheckBox('vip', _('Najważniejsza cena (Cena zostanie wyświetlona pomino cennika ceneo i wysokości rabatu)'), array(), '', !empty($aData['vip']), false);

		$aBoxOptions=array(array('value'=>0, 'label'=>'Wg ustawień boxu'));
		
		for($iI=1;$iI<=300;++$iI) {
			$aBoxOptions[]=array('value'=>$iI*3, 'label'=>$iI*3);
			}
		$pForm->AddSelect('boxOpt',$aConfig['lang'][$this->sModule]['boxOpt'],array(),$aBoxOptions,$aData['boxOpt'],'',  false);
		
		$pForm->AddSelect('page_id',$aConfig['lang'][$this->sModule]['page_id'],array(),array_merge(array(array('value'=>0,'label'=>$aConfig['lang']['common']['choose'])),getModulePages('m_powiazane_oferta_produktowa',$this->iLangId,true,'','promocje')),$aData['page_id']);

		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$sJS = '
		<script>

		$(document).ready(function(){

			$("#vip").on("change", function(){

				if ($(this).prop("checked")) {

					$("#ignore_ceneo_tarrifs").prop("checked", true)
				}
			})
		})
		</script>
		';

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	
	} // end of Add() function
	

	function GetBooksList(&$pSmarty, &$aBooks) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'form'	=>	false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'width'	=> '20'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['list_id'],
				'width' => '30'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['list_name']
			),

			array(
				'content'	=> $aConfig['lang'][$this->sModule]['list_base_price'],
				'width' => '100'
			)
		);

		$pView = new View('books_discount', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array();
		// dodanie rekordow do widoku
		foreach($aBooks as $iKey=>$aBook){
			$aBooks[$iKey]['name'] = '<b>'.$aBook['name'].'</b>'.(!empty($aBook['publisher'])?'<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($aBook['publisher'],0,40,'UTF-8').'</span>':'');
			unset($aBooks[$iKey]['publisher']);
			$aBooks[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">'.mb_substr($this->getAuthors($aBook['id']),0,40,'UTF-8').' '.$aBook['publication_year'].'</span>';
			unset($aBooks[$iKey]['publication_year']);
				
		}
		$pView->AddRecords($aBooks, $aColSettings);
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all')
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		// przepisanie langa - info o braku rekordow
		$aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_books_to_show'];
		return $pView->Show();
	} // end of GetUsersList() function
	
	

	function &GetBooksToDiscount($iCategoryId=0,$sTitle='',$sAuthor='',$iYear=0,$iPublisher=0,$iSeries=0,$fPriceFrom=0,$fPriceTo=0){
		global $aConfig;
		$aBooks=array();
		$sWhere='';
		if($iCategoryId>0){
			$sWhere.=' AND (A.page_id = '.$iCategoryId.' OR B.page_id = '.$iCategoryId.')';
		}
		if(!empty($iPublisher)){
			$sWhere.=' AND H.name LIKE \''.$iPublisher.'\'';
		}
		if($iSeries>0){
			$sWhere.=' AND D.series_id = '.$iSeries;
		}
		if(!empty($sTitle)){
			$sWhere.=" AND A.name LIKE '%".$sTitle."%'";
		}
		if(!empty($sAuthor)){
			$sWhere.=" AND CONCAT(G.name,' ',G.surname,' ',G.name) LIKE '%".$sAuthor."%'";
		}
		if($iYear>0){
			$sWhere.=' AND A.publication_year = '.$iYear;
		}
		
		if ($fPriceFrom > 0 && $fPriceTo > 0 && $fPriceTo > $fPriceFrom) {
			$sWhere.= " AND A.price_brutto >= ".$fPriceFrom." AND A.price_brutto <= ".$fPriceTo;
		}
		
		if(!empty($sWhere)){
			$sSQL="SELECT A.id, A.name, A.price_brutto, H.name AS publisher, A.publication_year
						 FROM ".$aConfig['tabls']['prefix']."products A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						 ON B.product_id=A.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series D
						 ON D.product_id=A.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_authors F
						 ON F.product_id=A.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_authors G
						 ON F.author_id=G.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers H
						 ON A.publisher_id=H.id
						 WHERE A.prod_status = '1'
						 ".$sWhere."
						 GROUP BY A.id
						 ORDER BY A.id";
			$aBooks =& Common::GetAll($sSQL);
				
			foreach ($aBooks as $iKey => $aBook) {
				$aBooks[$iKey]['price_brutto'] = Common::formatPrice($aBook['price_brutto']);
			}
		}
		return $aBooks;
	}
	
	function AddBooksForm($pSmarty, $iPId, $bSearch=false) {
		global $aConfig;
		
		$aPromo = $this->getPromotion($iPId);
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = $aConfig['lang'][$this->sModule]['header_add_books'];
			$sHeader .= ' "'.$aPromo['name'].'"';
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		// = new FormTable('books_discount', '', array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		//$pForm->AddHidden('do', 'add_to_promo');
		
		$pForm = new FormTable('books_discount_search', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'add_to_promo');
		
		$pForm->AddText('discount',$aConfig['lang'][$this->sModule]['discount_stake'],$aData['s_product_name'],array('style'=>'width:50px;', 'maxlength'=>10),'','text',false);
		
			
			$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'books', 'pid'=>$iPId)).'\');'), 'button'));

		$sJS = '
		<script type="text/javascript">
		
		$(document).ready(function(){
			$("#publisher_aci").autocomplete({
        source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetPublishers.php"
      });
			$("#publisher_aci").bind("blur", function(e){
     	 $.get("ajax/GetSeries.php", { publisher: e.target.value },
					  function(data){
					    var brokenstring=data.split(";");
					    var elSelSeries = document.getElementById(\'series_aci\');
					    elSelSeries.length = 0;
					    elSelSeries.options[elSelSeries.length] =  new Option("'.$aConfig['lang']['common']['choose'].'","0");
					    for (x in brokenstring){
								if(brokenstring[x]){
									var item=brokenstring[x].split("|");
		  						elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
	  						}
	  					}
					 });
    	});
		});
		</script>
		';
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
	
	}
			
	
	/**
	 * Metoda usuwa produkt z promocji
	 * 
	 * @param	integer	$iId	- Id produktu do usuniecia z promocji
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;

			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_promotions
						 	 WHERE id = ".$iId;
		return (Common::Query($sSql) != false);
	} // end of deleteItem() method
	
	/**
	 * Metoda usuwa wartosc promocji
	 * 
	 * @param	integer	$iId	- Id produktu do usuniecia z promocji
	 * @return	mixed	
	 */
	function deleteVal($iId) {
		global $aConfig;

			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_promotions_values
						 	 WHERE id = ".$iId;
		return (Common::Query($sSql) != false);
	} // end of deleteItem() method
	
	/**
	 * Metoda usuwa produkt z promocji
	 * 
	 * @param	integer	$iId	- Id produktu do usuniecia z promocji
	 * @return	mixed	
	 */
	function deletePromotionItem($iId) {
		global $aConfig;

			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_tarrifs
						 	 WHERE id = ".$iId;
		return (Common::Query($sSql) != false);
	} // end of deleteItem() method

		function &getProductPublishers(){
		global $aConfig;
		// pobranie wszystkich serii
			$sSql = "SELECT id as value, name as label
							 FROM ".$aConfig['tabls']['prefix']."products_publishers
							 ORDER BY name";
			$aItems= Common::GetAll($sSql);
			return array_merge(array(
		array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])
		),$aItems);
	}
	
	/**
	 * Metoda pobiera dane produktu
	 *
	 * @param	integer	$iId	- Id produktu
	 * @return	array ref	- lista rekordow
	 */
	function &getProductData($iId, $bIgnorePublisherDiscountLimit) {
		global $aConfig;

		// pobranie wszystkich promocji
		$sSql = "SELECT A.name, A.price_brutto, A.vat, 
										IFNULL(B.discount_limit,0) AS discount_limit, 
										IFNULL(PS.discount_limit,0) AS series_discount_limit, 
										IFNULL( C.discount_limit, 0 ) AS first_source_discount_limit
						 FROM ".$aConfig['tabls']['prefix']."products A
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers B
							ON B.id = A.publisher_id
						 LEFT JOIN products_source_discount AS C 
							ON C.first_source = A.created_by
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_to_series PTS
							ON A.id = PTS.product_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_series PS
							ON PTS.series_id = PS.id AND PS.publisher_id=A.publisher_id
						 WHERE A.id = ".$iId;
		$aItem = Common::GetRow($sSql);

    if ($bIgnorePublisherDiscountLimit === TRUE) {
      // zerujemy limit rabatu wydawnictwa, 
      // jeśli ustawiono ignorowanie limitu rabatu wydawnictwa
      $aItem['discount_limit'] = 0.00;
    }
		
		if ($bIgnorePublisherDiscountLimit === FALSE && 
            (($aItem['discount_limit'] <= 0.00) || (($aItem['series_discount_limit'] > 0) && $aItem['series_discount_limit'] < $aItem['discount_limit']))) {
			$aItem['discount_limit'] = $aItem['series_discount_limit'];
		}
		if (($aItem['discount_limit'] <= 0.00) || ($aItem['first_source_discount_limit'] > 0) && $aItem['first_source_discount_limit'] < $aItem['discount_limit']) {
			$aItem['discount_limit'] = $aItem['first_source_discount_limit'];
		}
		
		unset($aItem['series_discount_limit']);
		unset($aItem['first_source_discount_limit']);
						
		return $aItem;
	} // end of getProductData() method
	
	function &getPromotion($iId) {
		global $aConfig;
		
		$sSql = "SELECT *,DATE_FORMAT(start_date, '%d-%m-%Y %H:%i')
											AS start_date,
											DATE_FORMAT(end_date, '%d-%m-%Y %H:%i')
											AS end_date
						FROM ".$aConfig['tabls']['prefix']."products_promotions
						WHERE id = ".$iId;
		return Common::GetRow($sSql);	
	}
		/**
	 * Metoda pobiera liste rekordow
	 *
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($ifpid) {
		global $aConfig;

		// pobranie wszystkich nowosci
		$sSql = "SELECT A.id, A.name
						 FROM ".$aConfig['tabls']['prefix']."products_promotions A
						 WHERE A.page_id = ".$ifpid."
						 ORDER BY A.order_by ASC";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
			/**
	 * Metoda pobiera liste książek w danej promocji
	 *
	 * @return	array ref	- lista rekordow
	 */
	function &getBooksInPromotion($ipid) {
		global $aConfig;

		// pobranie wszystkich nowosci
		$sSql = "SELECT T.id, CONCAT(P.name, '<div style=\"width:60px; float:right;\">',REPLACE(T.discount, '.',','), '%</div>', '<div style=\"width:90px; float:right; text-align:left;\">', T.price_brutto,' zł</div>') AS row
						 FROM ".$aConfig['tabls']['prefix']."products_tarrifs T
						 JOIN ".$aConfig['tabls']['prefix']."products P
						 ON P.id=T.product_id
						 WHERE T.promotion_id = ".$ipid."
						 AND placeOnMain='1'
						 ORDER BY T.order_by ASC";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
	
	/**
	 * Metoda sprawdza status produktu, Nie akceptowane produkty nieopublikowane, niedostepne, nakład wyczerpany.
	 *
	 * @param type $iProdId 
	 */
	function checkProductStatus($iProdId) {
		global $aConfig;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."products
						 WHERE id=".$iProdId." AND published='1' AND prod_status<>'2'  AND prod_status<>'0' ";
		return Common::GetOne($sSql)>0;
	}// end of checkProductStatus() method
	

	/**
	 * Metoda zwraca nazwe produktu o podanym id
	 */
	function getItemName($iId) {
		global $aConfig;

		$sSql = "SELECT name FROM " . $aConfig['tabls']['prefix'] . "products
	 						WHERE id = " . $iId . "";

		return Common::GetOne($sSql);
	}
  
	/**
	 * Metoda eksportuje listę książek z wybranej promocji do pliku CSV
   * 
   * @param	integer	$iPId	- Id promocji
   * @return CSV
	 */
	function doExportCSV($iPId) {
      $aPromotionBooks = $this->getPromotionBooks($iPId);
     
      if ($aPromotionBooks[0]) {
        foreach ($aPromotionBooks[0] as $sKey => $sValue) {
          $aHeaders[] = $sKey;
        }
        
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=promocje.csv');
        $output = fopen('php://output', 'w');
        fwrite($output, "\xEF\xBB\xBF");
        fputcsv($output, $aHeaders, ';');
      
        foreach ($aPromotionBooks as $aPromotionBook) {
          fputcsv($output, $aPromotionBook, ';');
        }
        die;
      }  
  }// end of doExportCSV() funciton
  
  
	/**
	 * Metoda pobiera listę książek w danej promocji
   * 
 	 * @param	integer	$iPId	- Id promocji
   * @return array
	 */
	function getPromotionBooks($iPId) {
    global $pDbMgr;
    
		$sSql = "SELECT P.isbn AS Isbn, P.name AS Tytuł, PP.name AS Wydawnictwo
      FROM products_tarrifs AS PT
      JOIN products AS P
      ON PT.product_id = P.id
      JOIN products_publishers as PP
      ON P.publisher_id = PP.id
      WHERE PT.promotion_id = ".$iPId;
    
		return $pDbMgr->GetAll('profit24', $sSql);
	}// end of getPromotionBooks() funciton
  
} // end of Module Class
?>