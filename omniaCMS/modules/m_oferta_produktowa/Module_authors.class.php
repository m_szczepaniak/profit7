<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - autorzy
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;
	
	// domyslna liczba pol na autorow przy tworzeniu pakietu
	var $iDefaultAuthorsNo;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$this->iDefaultAuthorsNo = 3;

		$sDo = '';
		$iPPId = 0;
		$iPId = 0;
		$iId = 0;
		$iRId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['ppid'])) {
			$iPPId = intval($_GET['ppid']);
		}
		if (isset($_GET['pid'])) {
			$iPId = intval($_GET['pid']);
		}
		if (isset($_GET['rid'])) {
			$iRId = intval($_GET['rid']);
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iPPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId, $iPPId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iPPId, $iId); break;
			case 'add_product_author':
			case 'add': $this->Add($pSmarty, $iPId, $iPPId, $sDo == 'add_product_author'); break;
			case 'edit': $this->Edit($pSmarty, $iPId, $iPPId, $iId); break;
		
			case 'sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show($this->getRoleName($iRId),
																			 $this->getRecords($iPPId, $iRId),
																			 phpSelf(array('do'=>'proceed_sort', 'rid' => $iRId),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."products_to_authors",
												$this->getRecords($iPPId, $iRId),
												array('product_id' => $iPPId,
															 'role_id' => $iRId),
											 	'order_by');
			break;
			
			default: $this->Show($pSmarty, $iPId, $iPPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych autorow produktu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id kategorii
	 * @param	integer	$iId	- Id produktu
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header' => sprintf($aConfig['lang'][$this->sModule]['list'], trimString($this->getItemName($iId), 50), getPagePath($iPId, $this->iLangId, true, false, false)),
			'refresh' => true,
			'action' => phpSelf(array('ppid' => $iId)),
			'refresh_link' => phpSelf(array('ppid' => $iId, 'reset' => '1')),
			'search' => true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'B.name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_surname'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'A.role_id',
				'content'	=> $aConfig['lang'][$this->sModule]['list_role'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'A.created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '140'
			),
			array(
					'db_field'	=> 'A.modified',
					'content'	=> $aConfig['lang'][$this->sModule]['list_modified'],
					'sortable'	=> false,
					'width'	=> '140'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(A.author_id)
						 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
						 WHERE A.product_id = ".$iId.
									 (isset($_POST['f_role']) && !empty($_POST['f_role']) ? ' AND A.role_id = '.$_POST['f_role'] : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(A.author_id)
						 	 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
						 	 WHERE A.product_id = ".$iId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('product_authors', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// FILTRY
		// rola autora
		$pView->AddFilter('f_role', $aConfig['lang'][$this->sModule]['f_role'], $this->getAuthorsRoles($aConfig['lang'][$this->sModule]['f_all_roles']), $_POST['f_role']);
		
		if ($iRowCount > 0) {
			// pobranie wszystkich
			$sSql = "SELECT A.id, CONCAT(B.surname,' ',B.name) AS name, C.name role_name, A.created, A.created_by, A.modified, A.modified_by
							 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
							 JOIN ".$aConfig['tabls']['prefix']."products_authors B
							 ON B.id = A.author_id
							 JOIN ".$aConfig['tabls']['prefix']."products_authors_roles C
							 ON C.id = A.role_id
							 WHERE A.product_id = ".$iId.
										 (isset($_POST['f_role']) && !empty($_POST['f_role']) ? ' AND A.role_id = '.$_POST['f_role'] : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'C.order_by, A.order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '');
			$aRecords =& Common::GetAll($sSql);
			foreach ($aRecords as $iKey => $aItem) {
				$aRecords[$iKey]['created'] .= '<br />'.$aItem['created_by'];
				unset($aRecords[$iKey]['created_by']);
				
				$aRecords[$iKey]['modified'] .= '<br />'.$aItem['modified_by'];
				unset($aRecords[$iKey]['modified_by']);
			}
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('edit','delete'),
					'params' => array (
												'edit'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('sort', 'add')
		);
		// konfiguracja przyciskow stopki
		$aRecordsFooterParams = array(
			'go_back' => array(array(), array('action', 'pid', 'ppid')),
			'sort' => array(array('do' => 'sort', 'rid' => $_POST['f_role']))
		);

		if (!isset($_POST['f_role']) || empty($_POST['f_role']) ||
				(!empty($_POST['f_role']) && $iRowCount <= 1)) {
			// usuniecie mozliwosci filtrowania
			unset($aRecordsFooter[1][0]);
		}
		// zmiana w langu
		$aConfig['lang']['m_oferta_produktowa_authors']['go_back'] = $aConfig['lang']['m_oferta_produktowa_authors']['go_back_products'];
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa autorow z produktu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id kategorii
	 * @param	integer	$iPPId	- Id produktu
	 * @param	integer	$iId	- id autora
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iPPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($iPPId, $aItem['id'])) === false) {
				//	echo "deleteItem(".$iPPId.", ".$aItem['id'].") returned false<br>\n";
					$bIsErr = true;
					$sFailedToDel = $aItem['role'].': '.$aItem['name'];
					break;
				}
				elseif ($iRecords == 1) {
				//	echo "deleteItem(".$iPPId.", ".$aItem['id'].") returned true<br>\n";
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['role'].': '.$aItem['name'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if($this->updateShadowAuthors($iPPId)==false){
				//echo "updateShadowAuthors(".$iPPId.") returned false<br>\n";
				$bIsErr=true;
			}
			
			if (!$bIsErr) {
				// usunieto
				SetModification('products',$iPPId);
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel, $this->getItemName($iPPId), getPagePath($iPId, $this->iLangId, true, false, false));
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_'.($iDeleted > 1 ? '1' : '0')], $sFailedToDel, $this->getItemName($iPPId), getPagePath($iPId, $this->iLangId, true, false, false));
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId, $iPPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje autora(ow) produktu
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Add($pSmarty, $iPId, $iId);
			return;
		}
		Common::BeginTransaction();
		// dodanie do tabeli products_authors_import
		if(!empty($_POST['name']) && !empty($_POST['surname'])){
			$aValues = array(
					'index_letter' => strtoupper(mb_substr($_POST['surname'], 0, 1, 'utf8')),
					'name' => decodeString($_POST['name']),
					'surname' => decodeString($_POST['surname']),
					'created' => 'NOW()',
					'created_by ' => $_SESSION['user']['name']
			);
			if (($iIAId = Common::Insert($aConfig['tabls']['prefix']."products_authors_import",
																 $aValues)) === false) {
				$bIsErr = true;
			}
			// jeśli dodano do importów
			if ($iIAId > 0) {
				$aValues['id'] = $iIAId; // ID z tabeli products_authors_import to samo id ma przyjąć autor w products_authors
				if (($iNAId = Common::Insert($aConfig['tabls']['prefix']."products_authors",
																	 $aValues)) !== false) {
					$_POST['author_id'] = array($iNAId);
				} else {
					$bIsErr = true;
				}
			}
		}
		
		$aValues2 = array(
				'author_id'=>$iNAId, 	
				'author_import_id'=>$iIAId
		);
		if (Common::Insert($aConfig['tabls']['prefix']."products_authors_mappings",
									 $aValues2, '', false) === false)  {
			$bIsErr = true;
		}		
		
		// dodanie informacji do tabeli products_linked
		$sAdded = '';
		$bIsErr = ($iAdded = $this->addAuthors($iId, $_POST['role_id'], $_POST['author_id'], $sAdded)) == -1; 
		if($this->updateShadowAuthors($iId)==false) {
			$bIsErr=true;
		}
		
		if (!$bIsErr) {
			// dodano
			if ($iAdded > 0) {
				SetModification('products',$iId);
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
												$sAdded,
												getPagePath($iPId, $this->iLangId, true, false, false),
												$this->getItemName($iId));
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				// reset ustawien widoku
				$_GET['reset'] = 2;
			}
			$this->Show($pSmarty, $iPId, $iId);
		}
		else {
			// blad
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											getPagePath($iPId, $this->iLangId, true, false, false),
											$this->getItemName($iId));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->Add($pSmarty, $iPId, $iId);
		}
	} // end of Insert() funciton
	
	/**
	 * Metoda dodaje autora(ow) produktu
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iPPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId, $iPPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Edit($pSmarty, $iPId, $iPPId, $iId);
			return;
		}
		Common::BeginTransaction();

		$aValues = array(
			'role_id' => $_POST['role_id'],
			'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name']
		);
		
		if (Common::Update($aConfig['tabls']['prefix']."products_to_authors",
											 $aValues,
											 "id = ".$iId) === false) {
											 dump("error");
			$bIsErr = true;
		}
		
		//	if($this->updateShadowAuthors($iPPId)==false) {
		//		$bIsErr=true;
		//	}
		
		if (!$bIsErr) {
			// dodano
				SetModification('products',$iPPId);
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
												$sAdded,
												getPagePath($iPId, $this->iLangId, true, false, false),
												$this->getItemName($iPPId));
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				// reset ustawien widoku
				$_GET['reset'] = 2;
			$this->Show($pSmarty, $iPId, $iPPId);
		}
		else {
			// blad
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											getPagePath($iPId, $this->iLangId, true, false, false),
											$this->getItemName($iPPId));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->Edit($pSmarty, $iPId, $iPPId, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz wyboru dodawania autora produktu
	 *
	 * @param	object	$pSmarty
	 * @param	bool	- true: dodaj pole autora
	 */
	function Add(&$pSmarty, $iPId, $iId, $bAddAuthor=false) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		// poranie listy dostepnych w serwisie rol autorow
		$aAuthorsRoles =& $this->getAuthorsRoles($aConfig['lang']['common']['choose']);
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header'], trimString($this->getItemName($iId), 50));
				
		$pForm = new FormTable('search_author', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'add');
		
		// dla strony
		$pForm->AddRow($aConfig['lang'][$this->sModule]['category'], getPagePath($iPId, $this->iLangId, true), '', array(), array('class'=>'redText'));
		
		
		$pForm->AddText('author_name',$aConfig['lang'][$this->sModule]['author_name'],$aData['author_name'],array('style'=>'width:300px;'),'','text',false);
		// przycisk wyszukaj
		$pForm->AddRow('&nbsp;', '<div style="margin-left: 225px;">'.
									$pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_search'])
									.'</div>');
			
		$pForm2 = new FormTable('add_author', '', array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm2->AddHidden('do', 'insert');
		// autor
		$pForm2->AddSelect('author_id', $aConfig['lang'][$this->sModule]['author'], array('size'=>15,'multiple'=>1,'style'=>'width:300px;'), $this->getAuthors($_POST['author_name']),'','',false);
		
		// rola autora
		$pForm2->AddSelect('role_id', $aConfig['lang'][$this->sModule]['role'], array(), $aAuthorsRoles, '', '', true);
		
		$pForm2->AddMergedRow($aConfig['lang'][$this->sModule]['add_new'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		
		// imię
		$pForm2->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>60, 'style'=>'width: 200px;'),'','text',false);

		// nazwisko
		$pForm2->AddText('surname', $aConfig['lang'][$this->sModule]['surname'], $aData['surname'], array('maxlength'=>50, 'style'=>'width: 200px;'),'','text',false);
		

		// przyciski
		$pForm2->AddRow('&nbsp;', $pForm2->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm2->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm().$pForm2->ShowForm()));
	} // end of Add() function
	

	/**
	 * Metoda tworzy formularz wyboru dodawania autora produktu
	 *
	 * @param	object	$pSmarty
	 * @param	bool	- true: dodaj pole autora
	 */
	function Edit(&$pSmarty, $iPId, $iPPId, $iId) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
	if ($iId > 0) {
			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT A.role_id, CONCAT(B.surname,' ',B.name) AS name
							 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
							  JOIN ".$aConfig['tabls']['prefix']."products_authors B
							 ON B.id = A.author_id
							 WHERE A.id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		// poranie listy dostepnych w serwisie rol autorow
		$aAuthorsRoles =& $this->getAuthorsRoles($aConfig['lang']['common']['choose']);
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['edit_header'], $aData['name'], trimString($this->getItemName($iPPId), 50));
				

			
		$pForm = new FormTable('edit_author', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');
		// dla strony
		$pForm->AddRow($aConfig['lang'][$this->sModule]['category'], getPagePath($iPId, $this->iLangId, true), '', array(), array('class'=>'redText'));
		
		// rola autora
		$pForm->AddSelect('role_id', $aConfig['lang'][$this->sModule]['role'], array(), $aAuthorsRoles, $aData['role_id'], '', true);
		
			// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Add() function

	/**
	 * Metoda dodaje do bazy autorow produktu
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @param	array ref	$aRoles	- nowe role autorow
	 * @param	array ref	$aAuthors	- nowi autorzy
	 * @param	string	$sAdded	- zmienna na nazwiska dodanych autorow
	 * @return	integer
	 */
	function addAuthors($iId, $iRole, $aAuthors, &$sAdded) {
		global $aConfig;
		$iI = 0;
		foreach ($aAuthors as $iAuthor) {
			$aValues = array(
				'product_id' => $iId,
				'author_id' => (int) $iAuthor,
				'role_id' => (int) $iRole,
				'order_by' => '#order_by#',
				'created' => 'NOW()',
				'created_by' => $_SESSION['user']['name']
			);
			if (Common::Insert($aConfig['tabls']['prefix']."products_to_authors",
												 $aValues,
												 "product_id = ".$iId." AND
												 	role_id = ".$iRole,
												 	false) === false) {
				return -1;
			}
			else {
				$sAdded .= '"'.$this->getRoleName((int) $iRole).': '.$this->getAuthor((int) $iAuthor).'", ';
				$iI++;
			}
		}
		$sAdded = substr($sAdded, 0, -2);
		return $iI;
	} // end of addAuthors() method
	
	
	/**
	 * Metoda pobiera role autorow
	 * 
	 * @return	array
	 */
	function &getAuthorsRoles($sLabel) {
		global $aConfig;
		$aItems = array(
			array('value' => '', 'label' => $sLabel)
		);
		$sSql = "SELECT id value, name label
						 FROM ".$aConfig['tabls']['prefix']."products_authors_roles
						 ORDER BY order_by";
		$aItems = @array_merge($aItems, Common::GetAll($sSql));
		return $aItems;
	} // end of getAuthorsRoles() method
	
	
	/**
	 * Metoda pobiera autorow
	 * 
	 * @return	array
	 */
	function &getAuthors($sName='') {
		global $aConfig;
		$aItems=array();
		if(!empty($sName)){
			$sSql = "SELECT id value, CONCAT(surname, ' ',name) AS label
							 FROM ".$aConfig['tabls']['prefix']."products_authors
							 HAVING label LIKE '%".$sName."%'
							 ORDER BY label";
			$aItems =Common::GetAll($sSql);
		}
		return $aItems;
	} // end of getAuthors() method
	
	
	/**
	 * Metoda pobiera liste rekordow do sortowania
	 * 
	 * @param	integer	$iId	- Id produktu
	 * @param	integer	$iRId	- Id roli autorow
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($iId, $iRId) {
		global $aConfig;

		$sSql = "SELECT A.id, CONCAT(B.surname, ' ',B.name) as name
						 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
						 JOIN ".$aConfig['tabls']['prefix']."products_authors B
						 ON B.id = A.author_id
						 WHERE A.product_id = ".$iId." AND
						 			 A.role_id = ".$iRId."
						 ORDER BY A.order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
	
	/**
	 * Metoda pobiera i zwraca nazwe roli autorow
	 * 
	 * @param	integer	$iId	- Id roli
	 * @return	string
	 */
	function getRoleName($iId) {
		global $aConfig;
		
		$sSql = "SELECT name
						 FROM ".$aConfig['tabls']['prefix']."products_authors_roles
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getRoleName() method
	
	
	/**
	 * Metoda pobiera i zwraca nazwe autora
	 * 
	 * @param	integer	$iId	- Id autora
	 * @return	string
	 */
	function getAuthor($iId) {
		global $aConfig;
		
		$sSql = "SELECT CONCAT(surname, ' ',name)
						 FROM ".$aConfig['tabls']['prefix']."products_authors
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getAuthor() method
	
	
	/**
	 * Metoda pobiera liste autorow produktu do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id autorow
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT A.id, CONCAT(B.surname,' ',B.name) AS name, C.name role
					 		 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
					 		 JOIN ".$aConfig['tabls']['prefix']."products_authors B
					 		 ON B.id = A.author_id
					 		 JOIN ".$aConfig['tabls']['prefix']."products_authors_roles C
					 		 ON C.id = A.role_id
						 	 WHERE A.id IN  (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa autora produktu
	 * 
	 * @param	integer	$iProdId	- Id produktu
	 * @param	integer	$iId	- Id autora do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iProdId, $iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_to_authors WHERE product_id = ".$iProdId." AND id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method
	
} // end of Module Class
?>