<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - slownik oprawy
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

class Module extends Module_Common {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		//$iPId = getModulePageId('m_powiazane_oferta_produktowa', 'opisy_statusow');

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}

	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'insert': $this->Insert($pSmarty); break;
			case 'update': $this->Update($pSmarty, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iId); break;
			case 'change': $this->ChangeBooksForm($pSmarty, $iId); break;
			case 'change_books': $this->ChangeBooks($pSmarty, $iId); break;	
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych opraw
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'binding',
				'content'	=> $aConfig['lang'][$this->sModule]['list_binding'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."products_bindings
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND binding LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczby rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_bindings";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('products_bindings', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT id, binding, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."products_bindings
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND binding LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'binding' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('change', 'edit', 'delete'),
					'params' => array (
												'edit'	=> array('id'=>'{id}'),
												'change'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('add')
		);

		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane opawy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego opisu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				if($this->existProductWithBinding($aItem['id'])){
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_exists'], $aItem['binding']);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
					$this->Show($pSmarty);
					return;
				}
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['binding'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['binding'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowy oprawe
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}
		// dodanie
		$aValues = array(
			'binding' => $_POST['binding'],
			'created' => 'NOW()',
			'created_by' => $_SESSION['user']['name']
		);
		if (Common::Insert($aConfig['tabls']['prefix']."products_bindings",
						 					 $aValues,
											 "",
											 false) === false) {
			$bIsErr = true;
		}
				
		if (!$bIsErr) {
			// dodano
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											$_POST['binding']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
		else {
			// nie dodano
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$_POST['binding']);
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			$this->AddEdit($pSmarty);
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych oprawe
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id opisu
	 * @return	void
	 */
	function Update(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iId);
			return;
		}
		// aktualizacja
		$aValues = array(
			'binding' => $_POST['binding']
		);
		if (Common::Update($aConfig['tabls']['prefix']."products_bindings",
											 $aValues,
											 "id = ".$iId) === false) {
			$bIsErr = true;
		}
		if(!$bIsErr){
			$aShadows = $this->getShadowsWithBinding($iId);
			if(!empty($aShadows)) {
				foreach($aShadows as $iShadowId) {
					$aValues = array(
						'binding' => $_POST['binding']
					);
					Common::Update($aConfig['tabls']['prefix']."products_shadow", $aValues,'id = '.$iShadowId);
				}
			}
		}
		if (!$bIsErr) {
			// akapit zostal zaktualizowany
			SetModification('products_bindings',$iId);
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$_POST['binding']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// akapit nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$_POST['binding']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iId);
		}
	} // end of Update() funciton
	
	function &getShadowsWithBinding($iId) {
	global $aConfig;
		// wybiera grupy kategorii dla tego produktu
	  $sSql = "SELECT A.id
	  				 FROM ".$aConfig['tabls']['prefix']."products A
	  				 JOIN ".$aConfig['tabls']['prefix']."products_shadow B
	  				 ON B.id=A.id
	  				 WHERE A.binding =".$iId;
	  return Common::GetCol($sSql);
	} // end of getShadowsWithBinding()


	/**
	 * Metoda tworzy formularz dodawania / edycji oprawy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanego opisu statusu
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego rekordu
			$sSql = "SELECT binding
							 FROM ".$aConfig['tabls']['prefix']."products_bindings
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0) {
			$sHeader .= ' "'.$aData['binding'].'"';
		}

		$pForm = new FormTable('products_bindings', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
		
		// opis
		$pForm->AddText('binding', $aConfig['lang'][$this->sModule]['binding'], $aData['binding'], array('maxlength'=>'50', 'style'=>'width: 350px;'));
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda pobiera liste opraw do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id opisu
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, binding
					 		 FROM ".$aConfig['tabls']['prefix']."products_bindings
						 	 WHERE id IN  (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa oprawe
	 * 
	 * @param	integer	$iId	- Id opisu statusu do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."products_bindings
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method
	
	function existProductWithBinding($iId) {
		global $aConfig;
		
		$sSql = "SELECT count(id)
						 FROM ".$aConfig['tabls']['prefix']."products
						 WHERE binding = ".$iId;	
		return (Common::GetOne($sSql) > 0);
	} // end of existProductWithBinding() function

	
	/**
	 * Metoda tworzy formularz dodawania / edycji wydawnictwa
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id edytowanej serii
	 */
	function ChangeBooksForm(&$pSmarty, $iId) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

			// pobranie z bazy danych edytowanej serii
			$sSql = "SELECT binding
							 FROM ".$aConfig['tabls']['prefix']."products_bindings
							 WHERE id = ".$iId;
			$sName = Common::GetOne($sSql);

		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_change'];
		$sHeader .= ' "'.$sName.'"';


		$pForm = new FormTable('products_change', $sHeader, array('action'=>phpSelf(array('id'=>$iId)), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'change_books');
		$pForm->AddHidden('old_binding', $sName);
		
		$pForm->AddSelect('products_binding',$aConfig['lang'][$this->sModule]['binding_search'], array('style'=>'width:300px;'), $this->getBindings(),'');
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array()).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	

	function ChangeBooks(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->ChangeBooksForm($pSmarty, $iId);
			return;
		}
		
		$sSql = "SELECT id, binding
					 FROM ".$aConfig['tabls']['prefix']."products_bindings
					 WHERE id = ".intval($_POST['products_binding']); 
		$aNewBinding = Common::GetRow($sSql);
		if(!empty($aNewBinding)) {
			
			Common::BeginTransaction();
			
			$sSql = "SELECT id, name
							FROM ".$aConfig['tabls']['prefix']."products
							WHERE binding = ".$iId;
			$aProducts = Common::GetAll($sSql);
			if(!empty($aProducts)) {
				foreach($aProducts as $aProduct) {
					$aValues = array(
						'binding' => $aNewBinding['id']
					);
					if (Common::Update($aConfig['tabls']['prefix']."products",$aValues,'id = '.$aProduct['id']) === false) {
							$bIsErr = true;
					} 
				}
			}

			if (!$bIsErr) {
				// zaktualizowano
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['change_ok'],
												$_POST['old_binding'],
												$aNewBinding['binding'],
												count($aProducts));
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				$this->Show($pSmarty);
			}
			else {
				// nie zaktualizowano
				Common::RollbackTransaction();
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['change_err'],
												$_POST['old_binding'],
												$aNewBinding['binding']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->ChangeBooksForm($pSmarty, $iId);
			}
		} else {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['incorrect_binding_err']);
			$this->sMsg = GetMessage($sMsg, true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->ChangeBooksForm($pSmarty, $iId);
		}
	} // end of Update() funciton
	
} // end of Module Class
?>