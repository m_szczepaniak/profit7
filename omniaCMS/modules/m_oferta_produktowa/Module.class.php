<?php

use LIB\communicator\sources\Internal\streamsoft\getProduct;
use LIB\communicator\sources\Internal\streamsoft\manageProduct;
use LIB\communicator\sources\Internal\streamsoft\synchroProduct;
use omniaCMS\lib\Products\ProductsStockView;
use orders\Shipment\ShipmentTime;

/**
 * Klasa Module do obslugi modulu 'Oferta produktowa - produkty'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */

ini_set('upload_max_filesize', '10M');
// dolaczenie wspolnej klasy Module_Common
include_once('Module_Common.class.php');

ini_set('display_errors', 'On');

// Report simple running errors
error_reporting(E_ERROR | E_COMPILE_ERROR);

class Module extends Module_Common
{

    // komunikat
    var $sMsg;
    // nazwa modulu - do langow
    var $sModule;
    // ID wersji jezykowej
    var $iLangId;
    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;
    // ustawienia strony
    var $aSettings;
    // domyslna liczba pol na rodukty przy tworzeniu pakietu
    var $iDefaultProductsNo;

    public $reasonMsg = '';

    const ERR_CONNECT_PRODUCTS_PRICE = -1;
    const ERR_CONNECT_PRODUCTS_INDEKS = -2;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * Konstruktor klasy
     *
     * @return    void
     */
    function Module(&$pSmarty, &$oParent, $bInit = FALSE)
    {
        global $aConfig, $pDbMgr;

        if ($bInit === TRUE) {
            $this->pDbMgr = $pDbMgr;
            $this->sErrMsg = '';
            $this->iLangId = $_SESSION['lang']['id'];
            $this->sModule = $_GET['module'];
            $this->aPrivileges = &$_SESSION['user']['privileges'];
            $this->iDefaultProductsNo = 0;
            return true;
        }

        $this->pDbMgr = $pDbMgr;
        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];
        $this->aPrivileges = &$_SESSION['user']['privileges'];
        $this->iDefaultProductsNo = 0;

        $sDo = '';
        $iId = 0;


        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        if (isset($_GET['id'])) {
            $iId = $_GET['id'];
        }
        if (isset($_GET['pid'])) {
            $iPId = $_GET['pid'];
        }

        $this->sDir = $this->iLangId . '/' . $iPId . '/' . $iId;


        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }

        // pobranie ustawien modulu
        $this->aSettings = &$this->getGlobalSettings();

        if (isset($_POST['save_as'])) {
            $sDo = 'insert';
        }

//dump($_POST);die();
        switch ($sDo) {
            case 'delete_item_reservation':
                $this->doDeleteItemReservation($pSmarty, $iId);
                break;
            case 'delete_all':
            case 'delete':
                $this->Delete($pSmarty, $iId);
                break;
            case 'insert':
                $this->Insert($pSmarty);
                break;
            case 'update':
                $this->Update($pSmarty, $iId);
                break;
            case 'locations':
                $this->LocationHistory($pSmarty, $iId);
                break;
            case 'publish_all':
            case 'publish':
                $this->Publish($pSmarty, $iId);
                break;
            case 'review_all':
            case 'review':
                $this->Review($pSmarty, $iId);
                break;
            case 'tag_all':
                $this->TagAllForm($pSmarty);
                break;
            case 'change_status':
                $this->ChangeStatusForm($pSmarty);
                break;
            case 'update_status':
                $this->UpdateStatus($pSmarty);
                break;
            case 'update_tags':
                $this->UpdateTags($pSmarty);
                break;
            case 'change_product_type':
            case 'edit':
            case 'add':
                $this->AddEdit($pSmarty, $iId);
                break;
            case 'images':
                $this->AddEditImages($pSmarty, $iPId, $iId);
                break;
            case 'add_photo':
                $this->SaveAndAddImage($pSmarty, $iPId, $iId);
                break;
            case 'update_images':
                $this->UpdateImages($pSmarty, $iPId, $iId);
                break;
            case 'add_file':
                $this->AddEditFiles($pSmarty, $iPId, $iId, true);
                break;
            case 'files':
                $this->AddEditFiles($pSmarty, $iPId, $iId);
                break;
            case 'update_files':
                $this->ProceedFiles($pSmarty, $iPId, $iId);
                break;
            case 'edit_packet2':
                $this->EditPacketBookList($pSmarty, $iId);
                break;
            case 'books':
                $this->EditPacketBookList($pSmarty, $iId);
                break;
            case 'update_books_list':
                $this->UpdateBooksList($pSmarty, $iId);
                break;
            case 'removeFromPacket':
                $this->removeFromPacket($pSmarty, $iId, $iPId);
                break;
            case 'edit_packet':
            case 'add_packet':
            case 'add_product':
            case 'add_product_field':
            case 'change_packet_category':
                $this->AddEditPacket($pSmarty, $iId, $sDo == 'add_product_field');
                break;
            case 'insert_packet':
                $this->InsertPacket($pSmarty);
                break;
            case 'update_packet':
                $this->UpdatePacket($pSmarty, $iId);
                break;
            default:
                $this->Show($pSmarty);
                break;
            case 'prepare_connect':
                $this->prepareToConnect($pSmarty, $iId);
                break;
            case 'connect':
                $this->connectProducts($pSmarty, $iId);
                break;
        }
    } // end of Module() method

    /**
     *
     * @param Smarty $pSmarty
     * @param int $iId
     */
    public function doDeleteItemReservation(&$pSmarty, $iId)
    {

        $iItemReservation = intval($_GET['item_reservation']);
        if ($iItemReservation > 0) {
            $sSql = 'DELETE FROM products_stock_reservations WHERE id = ' . $iItemReservation . ' AND products_stock_id = ' . $iId;
            if (Common::Query($sSql) === false) {
                $sMsg = _('Wystąpił błąd podczas usuwania rezerwacji produktu: ' . $iId . ' - ' . $iItemReservation);
                addLog($sMsg, true);
                $this->sMsg .= GetMessage($sMsg, true);
            } else {
                $sMsg = _('Usunięto rezerwację produktu: ' . $iId . ' - ' . $iItemReservation);
                addLog($sMsg, false);
                $this->sMsg .= GetMessage($sMsg, false);
            }
        }
        $this->AddEdit($pSmarty, $iId);
    }


    /**
     * Metoda importuje produkt A do produktu B,
     *  jeśli cena brutto jest taka sama
     *  i jeśli zmieszczą się isbn'y
     *  i jeśli produkt nie występuje w żadnym otwartym zamówieniu
     *
     * @global DatabaseManager $pDbMgr
     * @param object $pSmarty
     * @param int $iId
     */
    private function connectProducts(&$pSmarty, $iId)
    {
        global $pDbMgr;

        if (!isset($_SESSION['user']['product_prepare_to_connect']) || $iId <= 0) {
            $this->sMsg = GetMessage(_('Wybierz ponownie produkty do połączenia'));
            return $this->Show($pSmarty);

        } else {
            $iDeleteProductId = $_SESSION['user']['product_prepare_to_connect'];
            $sDelProductName = $this->getProductAttrib($iDeleteProductId, ' name ');

            $reason = '';
            $result = $this->doConnectProducts($reason, $iId, $iDeleteProductId);
            if (self::ERR_CONNECT_PRODUCTS_PRICE === $result) {
                $this->sMsg = GetMessage(_('Produkty mają różną cenę brutto'));
                return $this->Show($pSmarty);
            } elseif (self::ERR_CONNECT_PRODUCTS_INDEKS === $result) {
                $this->sMsg = GetMessage(_('W produktach występuje zbyt wiele różnych indeksów, nie można połączyć takich produktów. Indeksy: '));
                return $this->Show($pSmarty);
            } elseif (false === $result) {
                $this->sMsg .= GetMessage(sprintf(_('Wystąpił błąd podczas łączenia produktów z powodu %s'), $reason));
                return $this->Show($pSmarty);
            } elseif (true === $result) {
                $sMsg = sprintf(_('Produkt "%s" został zaimportowany do produktu "%s"'), $sDelProductName, $this->getProductAttrib($iId, ' name '));
                unset($_SESSION['user']['product_prepare_to_connect']);
                AddLog($sMsg, false);
                $this->sMsg .= GetMessage($sMsg, false);
                SetModification('products', $iId);
            }
        }
        $this->Show($pSmarty);
    }// end of method


    /**
     * @param $validatePrice
     * @param $compareMainPriceBrutto
     * @param $compareDelPriceBrutto
     * @return bool
     */
    private function validateConnectProductPrice($validatePrice, $compareMainPriceBrutto, $compareDelPriceBrutto)
    {

        if (true === $validatePrice) {
            if (floatval($compareMainPriceBrutto) != floatval($compareDelPriceBrutto)) {
                return false;
            }
        }
        return true;
    }


    /**
     * @param $reason
     * @param $iId
     * @param $iDeleteProductId
     * @param bool $validatePrice
     * @return bool|int
     */
    public function doConnectProducts(&$reason, $iId, $iDeleteProductId, $validatePrice = true)
    {
        global $pDbMgr;
        $bIsErr = false;

        $aCompareCols = array('price_brutto', 'isbn_plain', 'ean_13', 'isbn_13', 'isbn_10', 'standard_code');

        $aCompareMain = $this->getProductAttribs($iId, $aCompareCols);
        $aCompareDel = $this->getProductAttribs($iDeleteProductId, $aCompareCols);


        if (false === $this->validateConnectProductPrice($validatePrice, $aCompareMain['price_brutto'], $aCompareDel['price_brutto'])) {
            return self::ERR_CONNECT_PRODUCTS_PRICE;
        } else {
            $sSql = 'SELECT code FROM websites WHERE type="shop" ';
            $shops = $pDbMgr->GetCol('profit24', $sSql);

            // mamy te same ceny, sprawdźmy czy nam się indeksy pomieszczą
            unset($aCompareMain['price_brutto']);
            unset($aCompareDel['price_brutto']);
            $aCompareDel = $this->clearEmpty($aCompareDel);
            $aCompareMain = $this->clearEmpty($aCompareMain);
            $aCompareMain = array_merge($aCompareMain, $aCompareDel);
            $aNewIsbn = array_unique($aCompareMain);
            if (count($aNewIsbn) > 4) {
                return self::ERR_CONNECT_PRODUCTS_INDEKS;
                // za dużo tych indeksów
            } else {


                $aUpdateIsbn = $this->matchKeys($aNewIsbn);
                // ok mamy isbn, teraz aktualizujemy indeksy dostawców
                $pDbMgr->BeginTransaction('profit24');

                foreach ($shops as $show) {
                    $pDbMgr->BeginTransaction($show);
                }


                $sDeleteAzymutIndex = $this->getProductAttrib($iDeleteProductId, ' azymut_index ');
                if ($sDeleteAzymutIndex != '') {
                    $sMainAzymutIndex = $this->getProductAttrib($iId, ' azymut_index ');
                    if ($sMainAzymutIndex != '') {
                        // dodajemy do tabeli z dodatkowymi
                        $aValuesBookindex = array(
                            'azymut_index' => $sDeleteAzymutIndex,
                            'product_id' => $iId
                        );
                        @$pDbMgr->Insert('profit24', 'products_azymut_bookindex', $aValuesBookindex);
                    } else {
                        // zmieniamy w głównej tabeli index azymut
                        $aUpdateIsbn['azymut_index'] = $sDeleteAzymutIndex;
                    }
                }

                // zmieniamy teraz indeksy dostawców
                $aValues = array('product_id' => $iId);
                @$pDbMgr->Update('profit24', 'products_to_sources', $aValues, ' product_id = ' . $iDeleteProductId);

                $sSql = 'SELECT * FROM products_azymut_bookindex WHERE product_id = ' . $iDeleteProductId;
                $aIndexRows = $pDbMgr->GetAll('profit24', $sSql);
                foreach ($aIndexRows as $aItem) {
                    // po jednym bo czasem możę się nie udać ze względu na unique
                    @$pDbMgr->Update('profit24', 'products_azymut_bookindex', $aValues, ' id = ' . $aItem['id']);
                }

                if ($pDbMgr->Update('profit24', 'products_available_notifications', $aValues, ' product_id = ' . $iDeleteProductId) === FALSE) {
                    $bIsErr = true;
                    $reason .= ' usuwanie komunikatów';
                }

                // zmieniamy ID w składowych zamówienia
                $sSql = "UPDATE orders_items AS OI, orders AS O
                        SET product_id = " . $iId . "
                        WHERE OI.order_id = O.id
                          AND product_id = " . $iDeleteProductId;
                if ($pDbMgr->Query('profit24', $sSql) === FALSE) {
                    $bIsErr = true;
                    $reason .= ' przepisywania ze składowych zamówień';
                }


                // zmieniamy id w składowych list
                $aValuesOILI = array(
                    'products_id' => $iId
                );
                if ($pDbMgr->Update('profit24', 'orders_items_lists_items', $aValuesOILI, ' products_id = ' . $iDeleteProductId) === FALSE) {
                    $bIsErr = true;
                    $reason .= ' przepisywania ze składowych list zbieranych';
                }


                // usuwamy produkt
                $sSql = 'DELETE FROM products WHERE id = ' . $iDeleteProductId . ' LIMIT 1';
                if ($pDbMgr->Query('profit24', $sSql) === FALSE) {
                    $bIsErr = true;
                    $reason .= ' usuwania z products w Profit24.pl ' . $sSql;
                    echo htmlspecialchars(mysqli_error($pDbMgr->aDBConn['profit24']->connection));
                    echo htmlspecialchars(mysql_error($pDbMgr->aDBConn['profit24']->connection));
                }

                // usuwamy produkt
                $sSql = 'DELETE FROM products_shadow WHERE id = ' . $iDeleteProductId . ' LIMIT 1';
                if ($pDbMgr->Query('profit24', $sSql) === FALSE) {
                    $bIsErr = true;
                    $reason .= ' usuwania z products_shadow w Profit24.pl ' . $sSql;
                }


                foreach ($shops as $show) {

                    if ($bIsErr === false) {
                        $sSql = 'SELECT id FROM products WHERE profit24_id = ' . $iDeleteProductId;
                        $iNpId = $pDbMgr->GetOne('np', $sSql);
                        if ($iNpId > 0) {
                            // usuwamy produkt
                            $sSql = 'DELETE FROM products_shadow WHERE id = ' . $iNpId . ' LIMIT 1';
                            if ($pDbMgr->Query($show, $sSql) === FALSE) {
                                $bIsErr = true;
                                $reason .= ' usuwania z products_shadow w ' . $show . ' ' . $sSql;
                            }
                            // usuwamy produkt
                            $sSql = 'DELETE FROM products WHERE id = ' . $iNpId . ' LIMIT 1';
                            if ($pDbMgr->Query($show, $sSql) === FALSE) {
                                $bIsErr = true;
                                $reason .= ' usuwania z products w ' . $show . ' ' . $sSql;
                            }
                        }
                    }
                }


                // zmieniamy isbn
                $aUpdateIsbn['modified'] = 'NOW()';
                $aUpdateIsbn['modified_by'] = $_SESSION['user']['name'] . ' ' . $_SESSION['user']['surname'];
                if ($pDbMgr->Update('profit24', 'products', $aUpdateIsbn, ' id = ' . $iId) === FALSE) {
                    $bIsErr = true;
                    $reason .= ' aktualizowania z products ';
                }

                if ($bIsErr === FALSE) {
                    $pDbMgr->CommitTransaction('profit24');
                    foreach ($shops as $show) {
                        $pDbMgr->CommitTransaction($show);
                    }
                } else {
                    $pDbMgr->RollbackTransaction('profit24');
                    foreach ($shops as $show) {
                        $pDbMgr->RollbackTransaction($show);
                    }
                }
                return !$bIsErr;
            }
        }
    }


    /**
     * Metoda sprawdza czy produkt występuje w otwartym zamówieniu
     *
     * @return bool
     */
    private function checkProductsIsInOpenOrder($iDeleteProductId)
    {
        global $pDbMgr;

        // sprawdźmy czy ten produkt jest teraz w jakimś otwartym zamówieniu !!
        $sSql = 'SELECT OI.id
             FROM orders_items AS OI
             JOIN orders AS O
              ON OI.order_id = O.id AND O.order_status <> "4" AND O.order_status <> "5"
             WHERE OI.product_id = ' . $iDeleteProductId;
        return ($pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
    }// end of checkProductsIsInOpenOrder


    /**
     * Dopasowujemy isbny
     *
     * @param array $aNewIsbn
     * @return array
     */
    private function matchKeys($aNewIsbn)
    {
        $aIsbn = array();
        foreach ($aNewIsbn AS $sKeyName => $sKeyValue) {
            if (strlen($sKeyValue) == 13) {
                if (!isset($aIsbn['EAN_13'])) {
                    $aIsbn['EAN_13'] = $sKeyValue;
                } elseif (!isset($aIsbn['ISBN_13'])) {
                    $aIsbn['ISBN_13'] = $sKeyValue;
                } elseif (!isset($aIsbn['isbn_plain'])) {
                    $aIsbn['isbn_plain'] = $sKeyValue;
                } else {
                    $aIsbn['isbn_10'] = $sKeyValue;
                }
            } else {
                if (!isset($aIsbn['isbn_plain'])) {
                    $aIsbn['isbn_plain'] = $sKeyValue;
                } elseif (!isset($aIsbn['isbn_10'])) {
                    $aIsbn['isbn_10'] = $sKeyValue;
                } elseif (!isset($aIsbn['EAN_13'])) {
                    $aIsbn['EAN_13'] = $sKeyValue;
                } else {
                    $aIsbn['ISBN_13'] = $sKeyValue;
                }
            }
        }
        return $aIsbn;
    }// end of matchKeys() method


    /**
     * Metoda czyści puste rekordy tablicy
     *
     * @param array $aCompareCols
     * @return array
     */
    private function clearEmpty($aCompareCols)
    {
        $aNewArr = array();

        $aCompareCols = array_unique($aCompareCols);
        foreach ($aCompareCols as $sColName => $sColValue) {
            if ($sColValue != '') {
                $aNewArr[] = $sColValue;
            }
        }
        return $aNewArr;
    }// end of clearEmpty() method


    /**
     * Metoda przygotowuje produkt do połączenia
     *
     * @param object $pSmarty
     * @param id $iId
     */
    private function prepareToConnect(&$pSmarty, $iId)
    {

        if (isset($_SESSION['user']['product_prepare_to_connect'])) {
            $this->sMsg .= GetMessage(sprintf(_('Przygotowałeś wcześniej do połączenia produkt "%s", ale zrezygnowałeś.'), $this->getProductAttrib($_SESSION['user']['product_prepare_to_connect'], ' name ')));
        }

        $_SESSION['user']['product_prepare_to_connect'] = $iId;
        $this->sMsg .= GetMessage(sprintf(_('Wybrałeś produkt do połączenia: "%s" <br /> Aby zaimportować ten produkt do innego przejdź do edycji kartoteki i wybierz "Zaimportuj"'), $this->getProductAttrib($iId, ' name ')), false);
        $this->Show($pSmarty);
    }// end of prepareToConnect() method


    /**
     * Metoda pobiera ustawienia dla modulu
     */
    function &getGlobalSettings()
    {
        global $aConfig;
        // pobranie konfiguracji dla modulu
        $sSql = "SELECT thumb_size, small_size, big_size
						 FROM " . $aConfig['tabls']['prefix'] . "products_config 
						 WHERE id = " . getModuleId('m_oferta_produktowa');
        return Common::GetRow($sSql);
    }

// end of setSettings() method

    /**
     * @return array
     */
    private function getVatStakes()
    {

        $sSql = 'SELECT value AS label, value AS value
               FROM products_vat_stakes';
        return addDefaultValue($this->pDbMgr->GetAll('profit24', $sSql));
    }

    /**
     * Metoda wyswietla liste produktow strony o id $iPId oferty produktowej
     *
     * @param    object $pSmarty
     * @param    integer $iPId - Id strony
     * @return    void
     */
    function Show(&$pSmarty)
    {
        global $aConfig, $oTimer, $pDbMgr;

        // $oTimer->setMarker($this->sModule.'_START_'.$aItem['id']);
        // dolaczenie klasy View
        include_once('View/View.class.php');

        $oProductsStockView = new ProductsStockView($pDbMgr, $pSmarty);
        $oProductsStockView->setHideOnStart();
        $aSourcesData = $this->_getSourcesData();

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header' => $aConfig['lang'][$this->sModule]['list'],
            'refresh' => true,
            'bigSearch' => true,
            'search' => true,
            'searchInfo' => 'W zapytaniu możesz użyć znaku * (gwiazdka), aby poszerzyć zakres wyników np. prawo*, *prawo lub *prawo*'
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'content' => '&nbsp;',
                'sortable' => false
            ),
            array(
                'content' => 'Pak.',
                'sortable' => false,
                'width' => '50'
            ),
            array(
                'db_field' => 'A.name',
                'content' => $aConfig['lang'][$this->sModule]['list_name'],
                'sortable' => true
            ),
            array(
                'db_field' => 'A.vat',
                'content' => $aConfig['lang'][$this->sModule]['list_vat'],
                'sortable' => true,
                'width' => '30'
            ),
            array(
                'db_field' => 'A.price_brutto',
                'content' => $aConfig['lang'][$this->sModule]['list_base'],
                'sortable' => true,
                'width' => '80'
            ),
            array(
                'db_field' => 'price',
                'content' => $aConfig['lang'][$this->sModule]['list_price'],
                'sortable' => false,
                'width' => '80'
            ),
            array(
                'db_field' => 'wholesale_price',
                'content' => $aConfig['lang'][$this->sModule]['list_wholesale_price'],
                'sortable' => true,
                'width' => '80'
            ),
            array(
                'db_field' => 'A.published',
                'content' => $aConfig['lang'][$this->sModule]['list_published'],
                'sortable' => true,
                'width' => '35'
            ),
            array(
                'db_field' => 'source',
                'content' => $aConfig['lang'][$this->sModule]['list_source'],
                'sortable' => true,
                'width' => '120'
            ),
            array(
                'db_field' => 'A.created',
                'content' => $aConfig['lang']['common']['created'],
                'sortable' => true,
                'width' => '140'
            ),
            array(
                'db_field' => 'A.modified',
                'content' => $aConfig['lang'][$this->sModule]['list_modified'],
                'sortable' => false,
                'width' => '140'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '180'
            )
        );


        if ($_POST['f_price_from'] < 0 || $_POST['f_price_to'] < 0 || $_POST['f_price_from'] > $_POST['f_price_to']) {
            $this->sMsg .= GetMessage($aConfig['lang']['m_oferta_produktowa']['invalid_price']);
            unset($_POST['f_price_from']);
            unset($_POST['f_price_to']);
        }

        $sSearch = '';
        if (isset($_POST['search']) && !empty($_POST['search'])) {
            $_POST['search'] = trim($_POST['search']);
            if ($this->SimpleCheckISBN($_POST['search'])) {
                $sSearch = ' AND ( 
                          A.id = ' . (int)$_POST['search'] . ' OR 
                          A.isbn_plain LIKE \'' . isbn2plain($_POST['search']) . '\' OR 
                          A.isbn_10 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                          A.isbn_13 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                          A.ean_13 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                          A.standard_code LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                          A.streamsoft_indeks LIKE \'' . isbn2plain($_POST['search']) . '\'
                          )';
            } else {
                $sSearchStr = $this->parseSearchString($_POST['search']);
                $sSearch = ' AND (
                          A.name LIKE \'' . $sSearchStr . '\'
                          )';
            }
        }
        $sInternalSourcePublisherWhereSQL = '';
        if (isset($_POST['f_hide_categories']) && !empty($_POST['f_hide_categories'])) {
            $aSourceArr = explode(';', $_POST['f_hide_categories']);
            $sInternalSource = $aSourceArr['1'];
            $aCategories = explode(',', $aSourceArr['0']);
            $sInternalSourcePublisherWhereSQL = $this->_getInternalSourcePublishers($sInternalSource, $aCategories);
        }

        $iNow = time();
//dump($_SESSION['_modified']);
        // pobranie liczby wszystkich rekordow
        $sSql = "SELECT COUNT(A.id)
						 FROM " . $aConfig['tabls']['prefix'] . "products A " .
            (!empty($sInternalSourcePublisherWhereSQL) ? ' JOIN products_stock AS PS ON A.id = PS.id ' : '') .
            (isset($_POST['f_category']) && $_POST['f_category'] != '' ?
                " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_extra_categories C
							 	 ON A.id = C.product_id" : "") .
            (isset($_POST['f_author']) && $_POST['f_author'] != '' ?
                " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_to_authors D
							 	 ON A.id = D.product_id
							 	 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_authors E
							 	 ON E.id = D.author_id" : "") .
            (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ?
                " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers F
							 	 ON A.publisher_id = F.id" : "") .
            (isset($_POST['f_image']) && $_POST['f_image'] != '' ? " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_images I
							 	 ON I.product_id = A.id " : '') .
            (isset($_POST['f_attachment']) && $_POST['f_attachment'] != '' ?
                " JOIN products_attachments PA
							 	 ON PA.product_id = A.id " : '') .
            (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ?
                " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_to_series G
							 	 ON G.product_id = A.id" : '') .
            ($_POST['f_price_from'] > 0 || $_POST['f_price_to'] > 0 ? ' LEFT JOIN ' . $aConfig['tabls']['prefix'] . 'products_tarrifs T ON T.product_id=A.id' : '') .
            "
						 WHERE 1=1" .
            $sInternalSourcePublisherWhereSQL .
            ($sSearch != '' ? $sSearch : '') .
            ($_POST['f_price_from'] > 0 ? ' AND T.price_brutto>=' . Common::FormatPrice2($_POST['f_price_from']) . ' AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' AND T.price_brutto=(SELECT price_brutto FROM ' . $aConfig['tabls']['prefix'] . 'products_tarrifs TT WHERE TT.product_id=A.id AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' ORDER BY price_brutto ASC LIMIT 1)' : '') .
            ($_POST['f_price_to'] > 0 ? ' AND T.price_brutto<=' . Common::FormatPrice2($_POST['f_price_to']) . ' AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' AND T.price_brutto=(SELECT price_brutto FROM ' . $aConfig['tabls']['prefix'] . 'products_tarrifs TT WHERE TT.product_id=A.id AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' ORDER BY price_brutto ASC LIMIT 1)' : '') .
            (isset($_POST['f_category']) && $_POST['f_category'] != '' ? ' AND (A.page_id = ' . $_POST['f_category'] . ' OR C.page_id = ' . $_POST['f_category'] . ')' : '') .
            (isset($_POST['f_published']) && $_POST['f_published'] != '' ? ' AND published = \'' . $_POST['f_published'] . '\'' : '') .
            (isset($_POST['f_language']) && $_POST['f_language'] != '' ? ' AND A.language = ' . $_POST['f_language'] : '') .
            (isset($_POST['f_orig_language']) && $_POST['f_orig_language'] != '' ? ' AND A.original_language = ' . $_POST['f_orig_language'] : '') .
            (isset($_POST['f_binding']) && $_POST['f_binding'] != '' ? ' AND A.binding = ' . $_POST['f_binding'] : '') .
            (isset($_POST['f_vat']) && $_POST['f_vat'] != '' ? ' AND A.vat >= ' . $_POST['f_vat'] : '') .
            (isset($_POST['f_ommit_auto_preview']) && $_POST['f_ommit_auto_preview'] != '' ? ' AND A.ommit_auto_preview = "' . $_POST['f_ommit_auto_preview'] . '"' : '') .
            (isset($_POST['f_year']) && $_POST['f_year'] != '' ? ' AND A.publication_year >= ' . $_POST['f_year'] : '') .
            (isset($_POST['f_yearto']) && $_POST['f_yearto'] != '' ? ' AND A.publication_year <= ' . $_POST['f_yearto'] : '') .
            (isset($_POST['f_created_to']) && $_POST['f_created_to'] != '' ? ' AND A.created <= "' . date('Y-m-d', strtotime($_POST['f_created_to'])) . ' 23:59:59"' : '') .
            (isset($_POST['f_created_from']) && $_POST['f_created_from'] != '' ? ' AND A.created > "' . date('Y-m-d', strtotime($_POST['f_created_from'])) . ' 00:00:00"' : '') .
            //	(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] != '' ? ' AND A.reviewed = \''.$_POST['f_reviewed'].'\'' : '').
            (isset($_POST['f_type']) && $_POST['f_type'] != '' ? ' AND A.packet = \'' . $_POST['f_type'] . '\'' : '') .
            (isset($_POST['f_image']) && $_POST['f_image'] != '' ? ($_POST['f_image'] == '0' ? ' AND I.id IS NULL' : ' AND I.id IS NOT NULL') : '') .
            (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ? ' AND F.name = \'' . $_POST['f_publisher'] . '\'' : '') .
            (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ? ' AND G.series_id = ' . $_POST['f_series'] . '' : '') .
            (isset($_POST['f_source']) && $_POST['f_source'] != '' ? ' AND A.source = \'' . $_POST['f_source'] . '\'' : '') .
            (isset($_POST['f_shipment']) && $_POST['f_shipment'] != '' ? ' AND A.shipment_time = \'' . $_POST['f_shipment'] . '\'' : '') .
            (isset($_POST['f_status']) && $_POST['f_status'] != '' ? ' AND A.prod_status = \'' . $_POST['f_status'] . '\'' : '') .
            (isset($_POST['f_exhausted_reason']) && $_POST['f_exhausted_reason'] != '' ? ' AND A.exhausted_reason = \'' . $_POST['f_exhausted_reason'] . '\'' : '') .
            (isset($_POST['f_author']) && $_POST['f_author'] != '' ? ' AND CONCAT(E.name,\' \',E.surname,\' \',E.name) LIKE \'%' . $_POST['f_author'] . '%\'' : '');
        $iRowCount = intval(Common::GetOne($sSql));

        if ($iRowCount == 0 && !isset($_GET['reset'])) {
            // resetowanie widoku
            resetViewState($this->sModule);
            // ponowne okreslenie liczny rekordow
            $sSql = "SELECT COUNT(id)
						 	 FROM " . $aConfig['tabls']['prefix'] . "products";
            $iRowCount = intval(Common::GetOne($sSql));
        }

        $pView = new View('products', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);

        // FILTRY
        // kategoria Oferty produktowej

        $aCats = array();
        if ($_POST['f_category'] != '' && $_POST['f_category'] > 0) {
            $sSql = "SELECT name FROM " . $aConfig['tabls']['prefix'] . "menus_items WHERE id=" . $_POST['f_category'];
            $aCats = array(
                array(
                    'label' => Common::GetOne($sSql),
                    'value' => $_POST['f_category']
                )
            );
        }
        $pView->AddFilter('f_category', $aConfig['lang'][$this->sModule]['f_category'], $aCats, $_POST['f_category']);
        $pView->AddFilter('f_category_btn', '', '<input type="button" name="nvtree" value="' . $aConfig['lang'][$this->sModule]['f_category_btn'] . '" id="show_navitree" \>&nbsp;', '', 'html');

        /*

		  // czy obrobiona
		  //				$aReviewed = array(
		  //				array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
		  //				array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_reviewed']),
		  //				array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_reviewed'])
		  //				);
		  //				$pView->AddFilter('f_reviewed', $aConfig['lang'][$this->sModule]['f_reviewed'], $aReviewed, $_POST['f_reviewed']);
		  //
		  // czy obrobiona

		 */


        // z okladka / bez okladki
        // typ produktu
        $aTypes = array(
            array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
            array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_packet']),
            array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_regular_product'])
        );
        $pView->AddFilter('f_type', $aConfig['lang'][$this->sModule]['f_type'], $aTypes, $_POST['f_type']);

        $pView->AddFilter('f_vat', _('VAT'), $this->getVatStakes(), $_POST['f_vat']);

        $aYesNo = [
            [
                'label' => _('Tak'),
                'value' => '1',
            ],
            [
                'label' => _('Nie'),
                'value' => '0',
            ]
        ];
        $pView->AddFilter('f_ommit_auto_preview', _('Pomiń zmianę auto-niedostępne zapowiedzi'), addDefaultValue($aYesNo), $_POST['f_ommit_auto_preview']);
        $pView->AddFilter('a', '', '</td></tr><tr><td>', '', 'html');

        $pView->AddFilter('f_year', $aConfig['lang'][$this->sModule]['f_year'], $this->getYears(), $_POST['f_year']);
        $pView->AddFilter('f_yearto', $aConfig['lang'][$this->sModule]['f_yearto'], $this->getYears(), $_POST['f_yearto']);

        $pView->AddFilter('a', '', '</td></tr><tr><td>', '', 'html');
        $pView->AddFilter('f_language', $aConfig['lang'][$this->sModule]['f_language'], $this->getLanguages(), $_POST['f_language']);
        $pView->AddFilter('f_orig_language', $aConfig['lang'][$this->sModule]['f_orig_language'], $this->getLanguages(), $_POST['f_orig_language']);
        $pView->AddFilter('f_binding', $aConfig['lang'][$this->sModule]['f_binding'], $this->getBindings(), $_POST['f_binding']);
        /*

		 */
        $pView->AddFilter('b', '', '</td></tr><tr><td>', '', 'html');

        // stan publikacji
        $aPublished = array(
            array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
            array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_published']),
            array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_published'])
        );
        $pView->AddFilter('f_published', $aConfig['lang'][$this->sModule]['f_publ_status'], $aPublished, $_POST['f_published']);

        $aSources = array(
            array('value' => '_', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
        );

        $aSourcesAll = $this->_getSources();
        $aSources = array_merge($aSources, $aSourcesAll);
        $pView->AddFilter('f_source', $aConfig['lang'][$this->sModule]['f_source'], $aSources, $_POST['f_source']);

        /*

		 */
        $aStatuses = array(
            array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
            array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1']),
            array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['status_0']),
            array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['status_2']),
            array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['status_3'])
        );
        $pView->AddFilter('f_status', $aConfig['lang'][$this->sModule]['f_status'], $aStatuses, $_POST['f_status']);


        $aExhaustedReason = [
            ['value' => '1', 'label' => 'burdello'],
            ['value' => '2', 'label' => 'stare wydanie'],
            ['value' => '3', 'label' => 'niechciane'],
            ['value' => '4', 'label' => 'aaa'],
        ];
        $pView->AddFilter('f_exhausted_reason', _('Powód wyczerpanego'), addDefaultValue($aExhaustedReason), $_POST['f_exhausted_reason']);

        $pShipmentTime = new ShipmentTime();

        $aShipments = array(
            array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
            array('value' => '0', 'label' => $pShipmentTime->getShipmentTime(null, 0)),
            array('value' => '1', 'label' => $pShipmentTime->getShipmentTime(null, 1)),
            array('value' => '2', 'label' => $pShipmentTime->getShipmentTime(null, 2)),
            array('value' => '3', 'label' => $pShipmentTime->getShipmentTime(null, 3)),
            array('value' => '4', 'label' => $pShipmentTime->getShipmentTime(null, 4)),
            array('value' => '5', 'label' => $pShipmentTime->getShipmentTime(null, 5)),
            array('value' => '6', 'label' => $pShipmentTime->getShipmentTime(null, 6)),
            array('value' => '7', 'label' => $pShipmentTime->getShipmentTime(null, 7)),
            array('value' => '8', 'label' => $pShipmentTime->getShipmentTime(null, 8)),
            array('value' => '9', 'label' => $pShipmentTime->getShipmentTime(null, 9)),
            array('value' => '10', 'label' => $pShipmentTime->getShipmentTime(null, 10)),
        );
        $pView->AddFilter('f_shipment', $aConfig['lang'][$this->sModule]['f_shipment'], $aShipments, $_POST['f_shipment']);

        $aImagesF = array(
            array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
            array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_not_image'])
        );
        $pView->AddFilter('f_image', $aConfig['lang'][$this->sModule]['f_image'], $aImagesF, $_POST['f_image']);

        $aAttachmentF = array(
            array('value' => '', 'label' => _('--')),
            array('value' => '1', 'label' => _('Z załącznikiem'))
        );
        $pView->AddFilter('f_attachment', _('Załącznik'), $aAttachmentF, $_POST['f_attachment']);

        $pView->AddFilter('c', '', '</td></tr><tr><td>', '', 'html');

        //filtr ceny
        $pView->AddFilter('f_price_from', '', '<label>' . $aConfig['lang']['m_oferta_produktowa']['f_price'] . '</label> <input type="text" name="f_price_from" value="' . $_POST['f_price_from'] . '" style="width:50px;" maxlength="7" />', $_POST['f_price_from'], 'html');
        $pView->AddFilter('f_price_to', '', '<label>' . $aConfig['lang']['m_oferta_produktowa']['f_price_to'] . '</label> <input type="text" name="f_price_to" value="' . $_POST['f_price_to'] . '" style="width:50px;" maxlength="7" /> ', $_POST['f_price_to'], 'html');

        $pView->AddFilter('f_author', $aConfig['lang'][$this->sModule]['f_author'], array(), $_POST['f_author'], 'text');
        $pView->AddFilter('f_publisher', $aConfig['lang'][$this->sModule]['f_publisher'], array(), $_POST['f_publisher'], 'text');

        $pView->AddFilter('f_publisher_btn', '', '<input type="hidden" name="f_hide_categories" id="f_hide_categories" value="' . $_POST['f_hide_categories'] . '" />'
            . '<input ' . (!empty($sInternalSourcePublisherWhereSQL) ? 'style="color: red; font-weight: bold; "' : '') . ' type="button" name="publisher_chooser" value="' . (!empty($sInternalSourcePublisherWhereSQL) ? '! ' : '') . $aConfig['lang'][$this->sModule]['f_publisher_btn'] . '" id="show_publishers_choose" />&nbsp;', '', 'html');

        $pView->AddFilter('f_series', $aConfig['lang'][$this->sModule]['f_series'], $this->getPublisherSeriesList($_POST['f_publisher']), $_POST['f_series']);

        $pView->AddFilter('f_created_from', $aConfig['lang'][$this->sModule]['f_created_from'], array('style' => 'float: right;'), (empty($_POST['f_created_from']) ? '00-00-0000' : $_POST['f_created_from']), 'date');
        $pView->AddFilter('f_created_to', $aConfig['lang'][$this->sModule]['f_created_to'], array('style' => 'float: right;'), (empty($_POST['f_created_to']) ? '00-00-0000' : $_POST['f_created_to']), 'date');

        if ($iRowCount > 0) {
            // dodanie Pagera do widoku
            $iCurrentPage = $pView->AddPager($iRowCount);
            $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
            $iStartFrom = ($iCurrentPage - 1) * $iPerPage;

            $aStocks = array();

            // pobranie wszystkich rekordow
            $sSql = "SELECT A.id, A.page_id, A.packet, A.name, A.vat, A.price_brutto,
							0	AS price, A.wholesale_price, F.name AS publisher, A.publication_year, 
							 A.published, A.source, A.created, A.created_by, A.modified, A.modified_by
						 	 FROM " . $aConfig['tabls']['prefix'] . "products A " .
                (!empty($sInternalSourcePublisherWhereSQL) ? ' JOIN products_stock AS PS ON A.id = PS.id ' : '') .
                (isset($_POST['f_category']) && $_POST['f_category'] != '' ?
                    " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_extra_categories C
							 	 ON A.id = C.product_id" : "") .
                (isset($_POST['f_author']) && $_POST['f_author'] != '' ?
                    " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_to_authors D
							 	 ON A.id = D.product_id
							 	 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_authors E
							 	 ON E.id = D.author_id" : "") .
                (isset($_POST['f_image']) && $_POST['f_image'] != '' ? " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_images I
							 	 ON I.product_id = A.id " : '') .
                (isset($_POST['f_attachment']) && $_POST['f_attachment'] != '' ?
                    " JOIN products_attachments PA
							 	 ON PA.product_id = A.id " : '') .
                (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ?
                    " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_to_series G
							 	 ON G.product_id = A.id" : '') .
                " LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers F
							 	 ON A.publisher_id = F.id" .
                ($_POST['f_price_from'] > 0 || $_POST['f_price_to'] > 0 ? ' LEFT JOIN ' . $aConfig['tabls']['prefix'] . 'products_tarrifs T ON T.product_id=A.id' : '') .
                "
						 	 WHERE 1=1" .
                $sInternalSourcePublisherWhereSQL .
                ($sSearch != '' ? $sSearch : '') .
                ($_POST['f_price_from'] > 0 ? ' AND T.price_brutto>=' . Common::FormatPrice2($_POST['f_price_from']) . ' AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' AND T.price_brutto=(SELECT price_brutto FROM ' . $aConfig['tabls']['prefix'] . 'products_tarrifs TT WHERE TT.product_id=A.id AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' ORDER BY price_brutto ASC LIMIT 1)' : '') .
                ($_POST['f_price_to'] > 0 ? ' AND T.price_brutto<=' . Common::FormatPrice2($_POST['f_price_to']) . ' AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' AND T.price_brutto=(SELECT price_brutto FROM ' . $aConfig['tabls']['prefix'] . 'products_tarrifs TT WHERE TT.product_id=A.id AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' ORDER BY price_brutto ASC LIMIT 1)' : '') .
                (isset($_POST['f_category']) && $_POST['f_category'] != '' ? ' AND (A.page_id = ' . $_POST['f_category'] . ' OR C.page_id = ' . $_POST['f_category'] . ')' : '') .
                (isset($_POST['f_published']) && $_POST['f_published'] != '' ? ' AND A.published = \'' . $_POST['f_published'] . '\'' : '') .
                (isset($_POST['f_language']) && $_POST['f_language'] != '' ? ' AND A.language = ' . $_POST['f_language'] : '') .
                (isset($_POST['f_orig_language']) && $_POST['f_orig_language'] != '' ? ' AND A.original_language = ' . $_POST['f_orig_language'] : '') .
                (isset($_POST['f_binding']) && $_POST['f_binding'] != '' ? ' AND A.binding = ' . $_POST['f_binding'] : '') .
                (isset($_POST['f_vat']) && $_POST['f_vat'] != '' ? ' AND A.vat = ' . $_POST['f_vat'] : '') .
                (isset($_POST['f_ommit_auto_preview']) && $_POST['f_ommit_auto_preview'] != '' ? ' AND A.ommit_auto_preview = "' . $_POST['f_ommit_auto_preview'] . '"' : '') .
                (isset($_POST['f_year']) && $_POST['f_year'] != '' ? ' AND A.publication_year >= ' . $_POST['f_year'] : '') .
                (isset($_POST['f_yearto']) && $_POST['f_yearto'] != '' ? ' AND A.publication_year <= ' . $_POST['f_yearto'] : '') .
                (isset($_POST['f_created_to']) && $_POST['f_created_to'] != '' ? ' AND A.created <= "' . date('Y-m-d', strtotime($_POST['f_created_to'])) . ' 23:59:59"' : '') .
                (isset($_POST['f_created_from']) && $_POST['f_created_from'] != '' ? ' AND A.created > "' . date('Y-m-d', strtotime($_POST['f_created_from'])) . ' 00:00:00"' : '') .
                //(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] != '' ? ' AND A.reviewed = \''.$_POST['f_reviewed'].'\'' : '').
                (isset($_POST['f_image']) && $_POST['f_image'] != '' ? ($_POST['f_image'] == '0' ? ' AND I.id IS NULL' : ' AND I.id IS NOT NULL') : '') .
                (isset($_POST['f_source']) && $_POST['f_source'] != '' && $_POST['f_source'] != '_' ? ' AND A.source = \'' . $_POST['f_source'] . '\'' : '') .
                (isset($_POST['f_shipment']) && $_POST['f_shipment'] != '' ? ' AND A.shipment_time = \'' . $_POST['f_shipment'] . '\'' : '') .
                (isset($_POST['f_status']) && $_POST['f_status'] != '' ? ' AND A.prod_status = \'' . $_POST['f_status'] . '\'' : '') .
                (isset($_POST['f_exhausted_reason']) && $_POST['f_exhausted_reason'] != '' ? ' AND A.exhausted_reason = \'' . $_POST['f_exhausted_reason'] . '\'' : '') .
                (isset($_POST['f_author']) && $_POST['f_author'] != '' ? ' AND CONCAT(E.name,\' \',E.surname,\' \',E.name) LIKE \'%' . $_POST['f_author'] . '%\'' : '') .
                (isset($_POST['f_type']) && $_POST['f_type'] != '' ? ' AND A.packet = \'' . $_POST['f_type'] . '\'' : '') .
                (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ? ' AND F.name = \'' . $_POST['f_publisher'] . '\'' : '') .
                (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ? ' AND G.series_id = ' . $_POST['f_series'] . '' : '') .
                ' GROUP BY A.id ORDER BY ' . ((isset($_GET['sort']) && !empty($_GET['sort']) && $_GET['sort'] != 'A.created') ? $_GET['sort'] : 'A.id') .
                (isset($_GET['order']) && !empty($_GET['order']) ? ' ' . $_GET['order'] : ' DESC') .
                " LIMIT " . $iStartFrom . ", " . $iPerPage;
            $aRecords = &Common::GetAll($sSql);
            foreach ($aRecords as $iKey => $aItem) {
                $aRecords[$iKey]['action']['links']['ceneo_history'] = phpSelf(array('action' => 'ceneo_history', 'id' => $aItem['id']));
                // pobranie stanów

                $aStocks[$aItem['id']] = $oProductsStockView->getProductsStockViewData($aItem['id']);

                // stan publikacji
                $aRecords[$iKey]['published'] = $aConfig['lang']['common'][$aItem['published'] == '1' ? 'yes' : 'no'];

                // link podgladu
                $aRecords[$iKey]['action']['links']['preview'] = (count($_SESSION['_settings']['lang']['versions']['symbols']) > 1 ? $_SESSION['lang']['symbol'] . '/' : '') . (mb_strlen($aItem['name'], 'UTF-8') > 210 ? mb_substr(link_encode($aItem['name']), 0, 210, 'UTF-8') : link_encode($aItem['name'])) . ',product' . $aItem['id'] . '.html?preview=' . md5($_SERVER['REMOTE_ADDR'] . $aConfig['common']['client_base_url_http']);

                $aRecords[$iKey]['created'] .= '<br />' . $aItem['created_by'];
                unset($aRecords[$iKey]['created_by']);

                $aRecords[$iKey]['modified'] .= '<br />' . $aItem['modified_by'];
                unset($aRecords[$iKey]['modified_by']);

                $aRecords[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">' . mb_substr($aItem['publisher'], 0, 40, 'UTF-8') . '</span>';
                unset($aRecords[$iKey]['publisher']);

                $aRecords[$iKey]['name'] .= '<br /><span style="font-weight: normal; font-style: italic;">' . mb_substr($this->getAuthors($aItem['id']), 0, 25, 'UTF-8') . ' ' . $aItem['publication_year'] . '</span>';
                unset($aRecords[$iKey]['publication_year']);

                $aRecords[$iKey]['source'] = '<a class="stockTooltip" href="#stockTooltip_' . $aItem['id'] . '" rel="#stockTooltip_' . $aItem['id'] . '">' . $this->_getSource($aItem['source'])['symbol'] . '</a>';

                // formatowanie ceny brutto
                $aTarrif = $this->getTarrif($aItem['id']);

                $aRecords[$iKey]['price_brutto'] = Common::formatPrice($aItem['price_brutto']);
                $aRecords[$iKey]['price'] = Common::formatPrice($aTarrif['price_brutto']);
                $aRecords[$iKey]['wholesale_price'] = Common::formatPrice($aItem['wholesale_price']);
//						if(!isset($_POST['f_reviewed']) || $_POST['f_reviewed'] != '0'){
//								$aRecords[$iKey]['disabled'][] = 'review';
//						}

                if ($this->checkUserPrivCommercial() === false) {
                    $aRecords[$iKey]['wholesale_price'] = null;
                }

                unset($aRecords[$iKey]['symbol']);

                if ($aItem['packet'] == '1') {
                    // wylaczona mozliwosc dodawania plikow i cenników
                    //$aRecords[$iKey]['disabled'][] = 'tarrifs';
                    $aRecords[$iKey]['disabled'][] = 'files';
                    $aRecords[$iKey]['disabled'][] = 'linked';

                    $aRecords[$iKey]['action']['links']['edit'] = phpSelf(array('do' => 'edit_packet', 'id' => $aItem['id']));
                    $aRecords[$iKey]['action']['links']['name'] = phpSelf(array('do' => 'edit_packet', 'id' => $aItem['id']));
                    $aRecords[$iKey]['packet'] = '[ ' . $aConfig['lang']['common']['yes'] . ' ]';
                } else {
                    $aRecords[$iKey]['packet'] = '&nbsp;';
                    $aRecords[$iKey]['disabled'][] = 'books';
                }
            }
            // $oTimer->setMarker($this->sModule.'_STOP_'.$aItem['id']);

            // ustawienia dla poszczegolnych kolumn rekordu
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'page_id' => array(
                    'show' => false
                ),
                'name' => array(
                    'link' => phpSelf(array('do' => 'edit', 'id' => '{id}'))
                ),
                'action' => array(
                    'actions' => array('locations', 'books', 'authors', 'linked', 'tarrifs', 'editions', 'images', 'files', 'publish', 'prepare_connect', 'mange_bookindex', 'preview', 'edit', 'ceneo_history'),//, 'delete'
                    'params' => array(
                        'locations' => array('id' => '{id}'),
                        'books' => array('pid' => '{page_id}', 'id' => '{id}'),
                        'authors' => array('action' => 'authors', 'pid' => '{page_id}', 'ppid' => '{id}'),
                        'linked' => array('action' => 'linked', 'pid' => '{page_id}', 'ppid' => '{id}'),
                        'tarrifs' => array('action' => 'tarrifs', 'pid' => '{id}'),
                        'editions' => array('action' => 'editions', 'pid' => '{page_id}', 'ppid' => '{id}'),
                        //'review' => array('pid' => '{page_id}', 'id' => '{id}'),
                        'images' => array('pid' => '{page_id}', 'id' => '{id}'),
                        'files' => array('pid' => '{page_id}', 'id' => '{id}'),
                        'publish' => array('pid' => '{page_id}', 'id' => '{id}'),
                        'prepare_connect' => array('pid' => '{page_id}', 'id' => '{id}'),
                        'mange_bookindex' => array('action' => 'additional_index', 'do' => '', 'pid' => '{id}', 'id' => '{id}'),
                        'edit' => array('pid' => '{page_id}', 'id' => '{id}'),
                        'delete' => array('pid' => '{page_id}', 'id' => '{id}')
                    ),
                    'show' => false,
                    'icon' => array('prepare_connect' => 'connect_to',
                        'mange_bookindex' => 'changes',
                        'locations' => 'comments'
                    ),
                )
            );
//					if(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] == '0'){
//						$aColSettings['action']['actions'][]= 'review';
//					}
            // dodanie rekordow do widoku
            $pView->AddRecords($aRecords, $aColSettings);

            $sStockHtml = $oProductsStockView->getMultipleStockView($aStocks, $aSourcesData);
            /*
			if (!empty($aStocks)) {
        $aStocks = $this->formateStockTemplateData($aStocks);
				$pSmarty->assign_by_ref('aStocks', $aStocks);
				$sStockHtml = $pSmarty->fetch('stocks.tpl');
				$pSmarty->clear_assign('aStocks', $aStocks);
			}
       */
        }
        // przyciski stopki stopki do widoku
        //	if(!isset($_POST['f_reviewed']) || $_POST['f_reviewed'] != '0'){
        $aRecordsFooter = array(
            array('check_all'),
            array('add_packet', 'add')
        );
//				} else {
//					$aRecordsFooter = array(
//						array('check_all', 'delete_all', 'publish_all', 'review_all'),
//						array('add_packet', 'add')
//					);
//				}
        $aRecordsFooterParams = array();
        $pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

        $aFooterGroup = array('tag_all', 'publish_all', 'delete_all', 'change_status');
//				if(isset($_POST['f_reviewed']) && $_POST['f_reviewed'] == '0'){
//					$aFooterGroup[]='review_all';
//				}
        $pView->AddFooterGroupActions($aFooterGroup);
        $sJS = '
					<script>
					
						$(document).ready(function(){
							$("#show_navitree").click(function() {
								window.open("ajax/NaviPopup.php",\'\',\'menubar=0,location=0,scrollbars=1,toolbar=0,width=500,height=300\');
							});
							$("#show_publishers_choose").click(function() {
                var sParams = $("#f_hide_categories").val();
								window.open("ajax/CategoriesChoosePopup.php?params=" + sParams,\'\',\'menubar=0,location=0,scrollbars=1,toolbar=0,width=800,height=900\');
							});
							$("a.stockTooltip").cluetip({
                width: "335px", 
                local:true, 
                showTitle: false, 
                sticky: true,
                arrows: true,
                closePosition: "top",
                closeText: "Zamknij",
                mouseOutClose: true
              });
              $("#f_publisher").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetPublishers.php"
              });
							$("#f_publisher").bind("blur", function(e){
						   	 var elSelSeries = document.getElementById("f_series");
						   	 if(e.target.value != "") {  
				     	    $.get("ajax/GetSeries.php", { publisher: e.target.value },
									  function(data){
									    var brokenstring=data.split(";");
								    elSelSeries.length = 0;
									    elSelSeries.options[elSelSeries.length] =  new Option("' . $aConfig['lang']['common']['choose'] . '","");
									    for (x in brokenstring){
												if(brokenstring[x]){
													var item=brokenstring[x].split("|");
						  						elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
					  						}
					  				}
									 });
						 			} else { elSelSeries.length = 0; elSelSeries.options[elSelSeries.length] =  new Option("' . $aConfig['lang']['common']['choose'] . '",""); }
	    					});
						});
					</script>';

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sStockHtml . $sJS . $pView->Show());
    }// end of Show() function


    /**
     *
     * @return boolean
     */
    private function checkUserPrivCommercial()
    {

        if (isset($_SESSION['user']['priv_commercial_data']) && $_SESSION['user']['priv_commercial_data'] == '1') {
            return true;
        }
        return false;
    }


    /**
     *
     * @return array
     */
    private function _getSourcesData()
    {
        $sSql = 'SELECT * FROM sources AS S
             ORDER BY order_by';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     *
     * @param array $aProductsStockData
     * @return array
     */
    private function formateStockTemplateData($aProductsStockData)
    {

        foreach ($aProductsStockData as &$aProductStockData) {
            $aSourcesToGetDiscount = array();
            $sWholesalePriceColName = '_wholesale_price';
            $sPriceBrutto = '_price_brutto';

            foreach ($aProductStockData as $sColName => $sValue) {
                if (strstr($sColName, $sWholesalePriceColName) !== FALSE) {
                    $aSourcesToGetDiscount[] = str_replace($sWholesalePriceColName, '', $sColName);
                }
            }

            foreach ($aSourcesToGetDiscount as $sSourceColSymbol) {
                $fWholesalePrice = $aProductStockData[$sSourceColSymbol . $sWholesalePriceColName];
                $fPriceBrutto = $aProductStockData[$sSourceColSymbol . $sPriceBrutto];
                if ($fWholesalePrice > 0) {
                    $aProductStockData[$sSourceColSymbol . '_our_discount'] = Common::formatPrice3(100 - (($fWholesalePrice * 100) / $fPriceBrutto));
                }
            }
        }

        return $aProductsStockData;
    }


    /**
     * Metoda zmienia string na potrzeby wyszukiwania
     *  ciąg *mowa* zmieniany jest na %mowa%
     *  ciąg *mowa ciała* zamieniany jest na %mowa ciała%
     *
     * @param string $sSearchStr
     * @return string
     */
    function parseSearchString($sSearchStr)
    {

        // preg_replace('/\s+/', '%', )
        if (mb_substr($sSearchStr, 0, 1) === '*') {
            $sSearchStr = '%' . mb_substr($sSearchStr, 1);
        }
        if (mb_substr($sSearchStr, -1, 1) === '*') {
            $sSearchStr = mb_substr($sSearchStr, 0, -1) . '%';
        }
        return $sSearchStr;
    } // end of parseSearchString() method


    /**
     * Metoda sprawdza czy podany string to ISBN
     *
     * @param string $sStr
     * @return boolean
     */
    function SimpleCheckISBN($sStr)
    {

        $aMatched = array();
        preg_match("/^([0-9X]{8,15})$/", $sStr, $aMatched);
        if (isset($aMatched[1]) || (is_numeric($sStr) && $sStr > 0 && $sStr < 2018688)) {
            return true;
        }
        return false;
    }// end of SimpleCheckISBN() method


    /**
     * Metoda usuwa wybrane rekordy
     *
     * @param    object $pSmarty
     * @param    integer $iId - Id usuwanego rekordu
     * @return    void
     */
    function Delete(&$pSmarty, $iId)
    {
        global $aConfig;
        $bIsErr = false;
        $aTmp = array();
        $sDel = '';
        $sFailedToDel = '';
        $iDeleted = 0;

        if ($_SESSION['user']['name'] != 'mchudy' && $_SESSION['user']['name'] != 'agolba') {
            $sMsg = _('Produkt może zostać usunięty wyłącznie przez Marcina ;) ');
            $this->sMsg = GetMessage($sMsg);
            return $this->Show($pSmarty);
        }
        if ($iId > 0) {
            $_POST['delete'][$iId] = '1';
        }
        $iI = 0;
        foreach ($_POST['delete'] as $sKey => $sVal) {
            $aTmp[$iI++] = $sKey;
        }
        $_POST['delete'] = &$aTmp;

        if (!empty($_POST['delete'])) {
            Common::BeginTransaction();
            $aItems = &$this->getItemsToDelete($_POST['delete']);
            foreach ($aItems as $aItem) {
                // usuwanie
                if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
                    $bIsErr = true;
                    $sFailedToDel = getPagePath((double)$aItem['page_id'], $this->iLangId, true) . ' / ' . $aItem['name'];
                    break;
                } elseif ($iRecords == 1) {
                    // usunieto
                    $iDeleted++;
                    $sDel .= '"' . getPagePath((double)$aItem['page_id'], $this->iLangId, true) . ' / ' . $aItem['name'] . '", ';
                }
            }
            $sDel = substr($sDel, 0, -2);

            if (!$bIsErr) {
                // usunieto
                Common::CommitTransaction();
                if ($iDeleted > 0) {
                    $sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_' . ($iDeleted > 1 ? '1' : '0')], $sDel);
                    $this->sMsg = GetMessage($sMsg, false);
                    // dodanie informacji do logow
                    AddLog($sMsg, false);
                }
            } else {
                // blad
                Common::RollbackTransaction();
                $sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
                $this->sMsg = GetMessage($sMsg);
                // dodanie informacji do logow
                AddLog($sMsg);
            }
        }
        $this->Show($pSmarty);
    }

// end of Delete() funciton


    /**
     * Metoda sprawdza czy produkt spełnia "ręczne" warunki dodania i aktualizacji produktu
     *
     * @param array $aData - dane produktu
     * @return bool
     */
    function checkInsertUpdateProducts($aData)
    {
        $bIsPreviews = false;
        $bIsNews = false;
        $bStatusSet = false;
        if (isset($aData['previews_status']) && intval($aData['previews_status']) > 0) {
            // mamy ustawioną zapowiedź
            $bIsPreviews = true;
        }

        if (isset($aData['prod_status']) && $aData['prod_status'] == '3') {
            // mamy ustawioną zapowiedź
            $bIsPreviews = true;
            $bStatusSet = true;
        }

        if (isset($aData['news_status']) && intval($aData['news_status']) > 0) {
            // mamy ustawioną zapowiedź
            $bIsNews = true;
        }
        if ($bIsPreviews === true && $bIsNews === true) {
            // nowosc i zapowiedz
            return -1;
        }
        if ($bStatusSet === false && $bIsPreviews === true) {
            //
            return -1;
        }

        return true;
    }// end of checkInsertUpdateProducts() method


    /**
     * Metoda dodaje do bazy danych nowy produkt
     *
     * @param    object $pSmarty
     * @param    integer $iPId - Id strony oferty produktowej
     * @return    void
     */
    function Insert(&$pSmarty)
    {
        global $aConfig;
        $bIsErr = false;

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty);
            return;
        }

        $oProductContext = new \LIB\EntityManager\Entites\ProductContext($this->pDbMgr);

        // jeśli pusty to przepisujemy z dowolnego wypełnionego
        $_POST['isbn'] = isbn2plain($_POST['isbn']);
        if ($_POST['isbn'] == '') {
            if ($_POST['isbn_10'] != '') {
                $_POST['isbn'] = $_POST['isbn_10'];
            } elseif ($_POST['isbn_13'] != '') {
                $_POST['isbn'] = $_POST['isbn_13'];
            } elseif ($_POST['ean_13'] != '') {
                $_POST['isbn'] = $_POST['ean_13'];
            }
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();

        $resultValidateStandard = $this->validateStandard($_POST);
        if ($resultValidateStandard !== true) {
            $oValidator->sError .= '<li>' . $resultValidateStandard . '</li>';
        }

        if ($this->checkInsertUpdateProducts($_POST) === false) {
            $oValidator->sError .= '<li>' . $aConfig['lang'][$this->sModule]['status_invalid_err'] . '</li>';
        }

        if (false === $this->checkExtraCategories($_POST)) {
            $oValidator->sError .= '<li>' . _('Niew wybrano kategorii drugiego poziomu') . '</li>';
        }
        if ($this->checkUniqueRef($_POST) === false) {
            $oValidator->sError .= '<li>' . _('Nie unikatowy isbn/ean') . '</li>';
        }
        $aSites = $oProductContext->getBookstores();
        $aValidationErrors = $this->validateBookstoresFields($_POST, $aSites, $oProductContext);
        if(!empty($aValidationErrors)) {
            foreach ($aValidationErrors as $sError) {
                $oValidator->sError .= '<li>' . $sError . '</li>';
            }
        }

        if (!$oValidator->Validate($_POST)) {
            $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEdit($pSmarty);
            return;
        }

        if ($this->existBookWithISBN(isbn2plain($_POST['isbn']))) {
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['isbn_err'], true);
            // istnieje juz ksiazka z ppodanym isbnem
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEdit($pSmarty);
            return;
        }

        // sprawdzenie tagow
        if (trim($_POST['tags']) != '' && !$this->checkTags($_POST['tags'])) {
            // nieprawidlowo wypelnione pole tagi
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
            $this->AddEdit($pSmarty);
            return;
        }

        if (isset($_POST['published']) && $_POST['page_id'] == $aConfig['import']['unsorted_category']) {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_unsorted_err'], $_POST['name']);
            $this->sMsg = GetMessage($sMsg);
            $this->AddEdit($pSmarty);
            return;
        }
        //wyczyszczenie opisu z linkow i script
        $sDesc = preg_replace('<a.*</a>', '', $_POST['profit24_description']);
        $sDesc = preg_replace('<script.*</script>', '', $sDesc);

        if (!empty($_POST['products_websites']) && !in_array(1, $_POST['products_websites'])) {
            unset($_POST['published']);
        }

        $bookstores = $oProductContext->getBookstores();
        foreach ($bookstores as $bookstore) {
            $this->pDbMgr->BeginTransaction($bookstore['code']);
        }

        $aValues = array(
            'page_id' => (double)$_POST['page_id'],
            'name' => $_POST['profit24_name'],
            'name2' => !empty($_POST['profit24_name2']) ? $_POST['profit24_name2'] : 'NULL',
            'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($_POST['profit24_name'], ' '))),
            'short_description' => strip_tags(br2nl(trimString($_POST['profit24_short_description'], 200), ' ')),
            'description' => $sDesc,
            'price_netto' => Common::formatPrice2($_POST['price_netto'] < 0 ? -$_POST['price_netto'] : $_POST['price_netto']),
            'price_brutto' => (Common::formatPrice2($_POST['price_netto'] < 0 ? -$_POST['price_netto'] : $_POST['price_netto']) * (1 + $_POST['vat'] / 100)),
            'vat' => $_POST['vat'],
            'pkwiu' => trim($_POST['pkwiu']),
            'shipment_time' => isset($_POST['shipment_time']) ? $_POST['shipment_time'] : '1',
            'weight' => (Common::formatWeight2($_POST['weight']) > 0) ? Common::formatWeight2($_POST['weight']) : 'NULL',
            'isbn' => $_POST['isbn'],
            'isbn_plain' => $_POST['isbn_plain'] != '' ? isbn2plain($_POST['isbn_plain']) : $_POST['isbn'],
            'isbn_10' => $_POST['isbn_10'] != '' ? $_POST['isbn_10'] : 'NULL',
            'isbn_13' => $_POST['isbn_13'] != '' ? $_POST['isbn_13'] : 'NULL',
            'ean_13' => $_POST['ean_13'] != '' ? $_POST['ean_13'] : 'NULL',
            'publication_year' => !empty($_POST['publication_year']) ? (int)$_POST['publication_year'] : 'NULL',
            'pages' => !empty($_POST['pages']) ? (int)$_POST['pages'] : 'NULL',
            'binding' => !empty($_POST['binding']) ? $_POST['binding'] : 'NULL',
            'dimension' => !empty($_POST['dimension']) ? $_POST['dimension'] : 'NULL',
            'language' => !empty($_POST['language']) ? $_POST['language'] : 'NULL',
            'translator' => $_POST['translator'],
            'volumes' => !empty($_POST['volumes']) ? (int)$_POST['volumes'] : 'NULL',
            'volume_nr' => !empty($_POST['volume_nr']) ? (int)$_POST['volume_nr'] : 'NULL',
            'edition' => !empty($_POST['edition']) ? (int)$_POST['edition'] : 'NULL',
            'volume_name' => !empty($_POST['volume_name']) ? $_POST['volume_name'] : 'NULL',
            'city' => !empty($_POST['city']) ? $_POST['city'] : 'NULL',
            'original_language' => !empty($_POST['original_language']) ? $_POST['original_language'] : 'NULL',
            'original_title' => !empty($_POST['original_title']) ? $_POST['original_title'] : 'NULL',
            'prod_status' => isset($_POST['prod_status']) ? $_POST['prod_status'] : '0',
            'ommit_auto_preview' => isset($_POST['ommit_auto_preview']) ? '1' : '0',
            'published' => isset($_POST['published']) ? '1' : '0',
            'legal_status_date' => (!empty($_POST['legal_status_date']) && ($_POST['legal_status_date'] != '00-00-0000')) ? FormatDate($_POST['legal_status_date']) : 'NULL',
            'shipment_date' => ($_POST['prod_status'] == '3' && !empty($_POST['shipment_date']) && ($_POST['shipment_date'] != '00-00-0000')) ? FormatDate($_POST['shipment_date']) : 'NULL',
            'created' => 'NOW()',
            'source' => isset($_POST['source']) ? $_POST['source'] : '1',
//				'type' => isset($_POST['type']) ? $_POST['type'] : '0',
            'created_by' => $_SESSION['user']['name'],
            'product_type' => $_POST['product_type'],
            'standard_code' => $_POST['standard_code'] != '' ? $_POST['standard_code'] : 'NULL',
            'standard_quantity' => $_POST['standard_quantity'] != '' ? $_POST['standard_quantity'] : 'NULL',
        );
        if ($aValues['pages'] < 0)
            $aValues['pages'] = -$aValues['pages'];

        $iPublished = $aValues['published'];
        if (!empty($_POST['attachment_type'])) {
            $aValues['price_brutto'] += (Common::formatPrice2($_POST['attachment_price_netto']) * (1 + $_POST['attachment_vat'] / 100));
        }
        if ($_POST['prod_status'] == '3') {
            $aValues['shipment_time'] = '0';
        }

        include_once($_SERVER['DOCUMENT_ROOT'] . 'import/CommonSynchro.class.php');
        $oCommonSynchro = new CommonSynchro();
        $sPublisher = $_POST['publisher'];
        $aValues['publisher_id'] = $oCommonSynchro->getPublisherId($sPublisher);

//dump($aValues);
        // dodanie
        if (($iId = Common::Insert($aConfig['tabls']['prefix'] . "products", $aValues)) === false) {
            $bIsErr = true;
        }

        if (!$bIsErr) {
            if ($this->proceedInsertUpdateGameAttributes($iId, true) === false) {
                $bIsErr = true;
            }
        }

        if (isset($_POST['products_websites'])) {
            if ($_POST['products_websites']) {
                foreach ($_POST['products_websites'] as $iWebsite) {
                    $aPVal = [
                        'website_id' => $iWebsite,
                        'product_id' => $iId
                    ];
                    $this->pDbMgr->Insert('profit24', 'products_websites', $aPVal);
                }
            }
        }

        // dodanie serii wydawniczych
        if (empty($_POST['series']) && !empty($_POST['series_name']) && !empty($aValues['publisher_id'])) {
            $aValues = array(
                'name' => decodeString($_POST['series_name']),
                'publisher_id' => (int)$aValues['publisher_id'],
                'created' => 'NOW()',
                'created_by' => $_SESSION['user']['name']
            );
            if (($iNewId = Common::Insert($aConfig['tabls']['prefix'] . "products_series", $aValues)) !== false) {
                $_POST['series'] = $iNewId;
            }
        }

        if (!empty($_POST['series'])) {
            $aValues = array(
                'product_id' => $iId,
                'series_id' => $_POST['series']
            );
            if ((Common::Insert($aConfig['tabls']['prefix'] . "products_to_series", $aValues, '', false)) === false) {
                $bIsErr = true;
            }
        }

        if (!empty($_POST['attachment_type'])) {
            $aValues = array(
                'attachment_type' => $_POST['attachment_type'],
                'product_id' => $iId,
                'price_netto' => Common::formatPrice2($_POST['attachment_price_netto']),
                'price_brutto' => (Common::formatPrice2($_POST['attachment_price_netto']) * (1 + $_POST['attachment_vat'] / 100)),
                'vat' => (int)$_POST['attachment_vat']
            );
            if ((Common::Insert($aConfig['tabls']['prefix'] . "products_attachments", $aValues, '', false)) === false) {
                $bIsErr = true;
            }
        }

        $_POST['extra_categories'][] = $_POST['page_id'];
        // dodanie dodatkopwych kategorii
        if (!$bIsErr && !empty($_POST['extra_categories'])) {
            foreach ($_POST['extra_categories'] as $iCatId) {
                if ($iCatId != $aConfig['import']['unsorted_category'] && !$this->existExtraCategory($iCatId, $iId)) {
                    $aValues = array(
                        'product_id' => $iId,
                        'page_id' => $iCatId
                    );
                    if ((Common::Insert($aConfig['tabls']['prefix'] . "products_extra_categories", $aValues, '', false)) === false) {
                        $bIsErr = true;
                    }
                    // pobranie kategorii - rodzicow wybranej kategorii
                    $aParents = $this->getPathIDs($iCatId, $_SESSION['lang']['id']);
                    // dodanie mapowan w gore drzewa lokalnego
                    if (!empty($aParents)) {
                        foreach ($aParents as $iParent) {
                            if (!$this->existExtraCategory($iParent, $iId)) {
                                $aValues = array(
                                    'product_id' => $iId,
                                    'page_id' => $iParent
                                );
                                if ((Common::Insert($aConfig['tabls']['prefix'] . "products_extra_categories", $aValues, '', false)) === false) {
                                    $bIsErr = true;
                                }
                            }
                        }
                    }
                }
            }
        }


        if (!$bIsErr) {
            if (trim($_POST['tags']) != '') {
                // dodanie tagow
                $bIsErr = !$this->addTags($iId, explode(',', $_POST['tags']));
            }
        }


        // status nowości
        if (!$bIsErr) {
            $aNews = $this->getNews($iId);
            if (empty($aNews)) {
                // produkt nie jest obecnie nowością
                if ($_POST['news_status'] > 0) {
                    // dodajemy do nowości
                    if ($this->addNews($iId, true) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }

        // status zapowiedzi
        if (!$bIsErr) {
            $aNews = $this->getPreviews($iId);
            if (empty($aNews)) {
                // produkt nie jest obecnie nowością
                if ($_POST['previews_status'] > 0) {
                    // dodajemy do nowości
                    if ($this->addPreview($iId, true) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }

        if ($iPublished == '1') {
            if ($this->addShadow($iId) == false) {
                $bIsErr = true;
            }
        }

        $oSynchro = new synchroProduct($this->pDbMgr);
        $oSynchro->trySynchronizeProduct($iId);

        if (!$bIsErr) {
            // rekord zostal dodany
            SetModification('products', $iId);
            $this->pDbMgr->CommitTransaction('profit24');

            $sAddErrWebsites = '';
            if(false === $oProductContext->insertProductToShopsWithTransactions($_POST, $iId) ) {
                $sAddErrWebsites = _(' <span style="color: red">Produkt <strong>nie został</strong> pomyślnie dodany do sklepów</span> ');
            }

            if ($aConfig['common']['status'] == 'productive') {
                $elasticIndex = new \Elasticsearch\Profit\ElasticIndex();
                $elasticIndex->createIndex([$iId], 1);
            }

            $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'].$sAddErrWebsites, getPagePath((double)$_POST['page_id'], $this->iLangId, true), $_POST['name']);
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            // reset ustawien widoku
            $_GET['reset'] = 2;
            $this->Show($pSmarty);
        } else {
            // rekord nie zostal dodany,
            // wyswietlenie komunikatu o niepowodzeniu
            // oraz ponowne wyswietlenie formularza dodawania
            foreach ($bookstores as $bookstore) {
                $this->pDbMgr->RollbackTransaction($bookstore['code']);
            }

            $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'], getPagePath((double)$_POST['page_id'], $this->iLangId, true), $_POST['name']);
            $this->sMsg = GetMessage($sMsg);
            AddLog($sMsg);
            $this->AddEdit($pSmarty);
        }
    }

// end of Insert() funciton

    /**
     * @return bool|string
     */
    private function validateStandard($aPost) {

        if (isset($aPost['standard_code']) && $aPost['standard_code'] != '' && (!isset($aPost['standard_quantity']) || $aPost['standard_quantity'] === '')) {
            return 'Wypełnij pole Ilość w standardzie';
        } elseif (isset($aPost['standard_quantity']) && $aPost['standard_quantity'] != '' && (!isset($aPost['standard_code']) || $aPost['standard_code'] === '')) {
            return 'Wypełnij pole Kod standardu';
        }
        return true;
    }


    /**
     * Metoda aktualizuje w bazie danych produkt
     *
     * @param        object $pSmarty
     * @param    integer $iPId - Id strony aktualizowanego rekordu
     * @param    integer $iId - Id aktualizowanego rekordu
     * @return    void
     */
    function Update(&$pSmarty, $iId)
    {
        global $aConfig, $pDbMgr, $pDB;
        $bIsErr = false;
        $iErrorCode = 0;

        $backUri = null;
        if (isset($_GET['back_uri']) && !empty($_GET['back_uri'])) {
            $backUri = $_GET['back_uri'];
        }

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty);
            return;
        }

        $oProductContext = new \LIB\EntityManager\Entites\ProductContext($this->pDbMgr);

        // jeśli pusty to przepisujemy z dowolnego wypełnionego
        $_POST['isbn'] = isbn2plain($_POST['isbn']);
        if ($_POST['isbn'] == '') {
            if ($_POST['isbn_10'] != '') {
                $_POST['isbn'] = $_POST['isbn_10'];
            } elseif ($_POST['isbn_13'] != '') {
                $_POST['isbn'] = $_POST['isbn_13'];
            } elseif ($_POST['ean_13'] != '') {
                $_POST['isbn'] = $_POST['ean_13'];
            }
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();

        $resultValidateStandard = $this->validateStandard($_POST);
        if ($resultValidateStandard !== true) {
            $oValidator->sError .= '<li>' . $resultValidateStandard . '</li>';
        }

        if (false === $this->checkExtraCategories($_POST)) {
            $oValidator->sError .= '<li>' . _('Niew wybrano kategorii drugiego poziomu') . '</li>';
        }

        if ($this->checkUniqueRef($_POST, $iId) === false) {
            $oValidator->sError .= '<li>' . _('Nie unikatowy ISBN/EAN lub kod standardu jest taki jak EAN/ISBN') . '</li>';
        }
        if ($this->checkInsertUpdateProducts($_POST) === -1) {
            $oValidator->sError .= '<li>' . $aConfig['lang'][$this->sModule]['status_invalid_err'] . '</li>';
        }
        $aSites = $oProductContext->getBookstores();
        $aValidationErrors = $this->validateBookstoresFields($_POST, $aSites, $oProductContext);
        if(!empty($aValidationErrors)) {
            foreach ($aValidationErrors as $sError) {
                $oValidator->sError .= '<li>' . $sError . '</li>';
            }
        }
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEdit($pSmarty, $iId);
            return;
        }

        if ($this->existBookWithISBN(isbn2plain($_POST['isbn']), $iId)) {
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['isbn_err'], true);
            // istnieje juz ksiazka z ppodanym isbnem
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEdit($pSmarty, $iId);
            return;
        }
        // sprawdzenie tagow
        if (trim($_POST['tags']) != '' && !$this->checkTags($_POST['tags'])) {
            // nieprawidlowo wypelnione pole tagi
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
            $this->AddEdit($pSmarty, $iId);
            return;
        }

        if (isset($_POST['published']) && $_POST['page_id'] == $aConfig['import']['unsorted_category']) {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_unsorted_err'], $_POST['name']);
            $this->sMsg = GetMessage($sMsg);
            $this->AddEdit($pSmarty, $iId);
            return;
        }

        // pobranie aktualnego stanu publikacji
        $aCurrent = &$this->getItem($iId);

        //wyczyszczenie opisu z linkow i script
        $sDesc = eregi_replace('<a.*</a>', '', $_POST['profit24_description']);
        $sDesc = eregi_replace('<script.*</script>', '', $sDesc);

        Common::BeginTransaction();

        if (!empty($_POST['products_websites']) && !in_array(1, $_POST['products_websites'])) {
            unset($_POST['published']);
        }

        $aValues = array(
            'page_id' => (double)$_POST['page_id'],
            'name' => $_POST['profit24_name'],
            'name2' => !empty($_POST['profit24_name2']) ? $_POST['profit24_name2'] : 'NULL',
            'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($_POST['profit24_name'], ' '))),
            'short_description' => strip_tags(br2nl(trimString($_POST['profit24_description'], 200), ' ')),
            'description' => $sDesc,
            'price_netto' => Common::formatPrice2($_POST['price_netto'] < 0 ? -$_POST['price_netto'] : $_POST['price_netto']),
            'price_brutto' => (Common::formatPrice2($_POST['price_netto'] < 0 ? -$_POST['price_netto'] : $_POST['price_netto']) * (1 + $_POST['vat'] / 100)),
            'vat' => $_POST['vat'],
            'pkwiu' => trim($_POST['pkwiu']),
            'shipment_time' => isset($_POST['shipment_time']) ? $_POST['shipment_time'] : '1',
            'weight' => (Common::formatWeight2($_POST['weight']) > 0) ? Common::formatWeight2($_POST['weight']) : 'NULL',
            'isbn' => $_POST['isbn'],
            'isbn_plain' => $_POST['isbn_plain'] != '' ? isbn2plain($_POST['isbn_plain']) : $_POST['isbn'],
            'isbn_10' => $_POST['isbn_10'] != '' ? $_POST['isbn_10'] : 'NULL',
            'isbn_13' => $_POST['isbn_13'] != '' ? $_POST['isbn_13'] : 'NULL',
            'ean_13' => $_POST['ean_13'] != '' ? $_POST['ean_13'] : 'NULL',
            'publication_year' => !empty($_POST['publication_year']) ? (int)$_POST['publication_year'] : 'NULL',
            'pages' => !empty($_POST['pages']) ? (int)$_POST['pages'] : 'NULL',
            'binding' => !empty($_POST['binding']) ? $_POST['binding'] : 'NULL',
            'dimension' => !empty($_POST['dimension']) ? $_POST['dimension'] : 'NULL',
            'language' => !empty($_POST['language']) ? $_POST['language'] : 'NULL',
            'translator' => $_POST['translator'],
            'volumes' => !empty($_POST['volumes']) ? (int)$_POST['volumes'] : 'NULL',
            'volume_nr' => !empty($_POST['volume_nr']) ? (int)$_POST['volume_nr'] : 'NULL',
            'edition' => !empty($_POST['edition']) ? (int)$_POST['edition'] : 'NULL',
            'volume_name' => !empty($_POST['volume_name']) ? $_POST['volume_name'] : 'NULL',
            'city' => !empty($_POST['city']) ? $_POST['city'] : 'NULL',
            'original_language' => !empty($_POST['original_language']) ? $_POST['original_language'] : 'NULL',
            'original_title' => !empty($_POST['original_title']) ? $_POST['original_title'] : 'NULL',
            'prod_status' => isset($_POST['prod_status']) ? $_POST['prod_status'] : '0',
            'published' => isset($_POST['published']) ? '1' : '0',
            'ommit_auto_preview' => isset($_POST['ommit_auto_preview']) ? '1' : '0',
            'legal_status_date' => (!empty($_POST['legal_status_date']) && ($_POST['legal_status_date'] != '00-00-0000')) ? FormatDate($_POST['legal_status_date']) : 'NULL',
            'shipment_date' => ($_POST['prod_status'] == '3' && !empty($_POST['shipment_date']) && ($_POST['shipment_date'] != '00-00-0000')) ? FormatDate($_POST['shipment_date']) : 'NULL',
//          'type' => isset($_POST['type']) ? $_POST['type'] : '0',
            'source' => isset($_POST['source']) ? $_POST['source'] : '1',
            'product_type' => $_POST['product_type'],
            'standard_code' => $_POST['standard_code'] != '' ? $_POST['standard_code'] : 'NULL',
            'standard_quantity' => $_POST['standard_quantity'] != '' ? $_POST['standard_quantity'] : 'NULL',
        );
        if (isset($_POST['streamsoft_indeks']) && $_POST['streamsoft_indeks'] != '') {
            $aValues['streamsoft_indeks'] = $_POST['streamsoft_indeks'];
        }
        if ($aValues['pages'] < 0)
            $aValues['pages'] = -$aValues['pages'];
        if (!empty($_POST['attachment_type'])) {
            $aValues['price_brutto'] += (Common::formatPrice2($_POST['attachment_price_netto']) * (1 + $_POST['attachment_vat'] / 100));
        }
        if ($_POST['prod_status'] == '3') {
            $aValues['shipment_time'] = '0';
        }

        if ($_POST['prod_status'] == '2') {
            $aValues['exhausted_reason'] = $_POST['exhausted_reason'];
        } else {
            $aValues['exhausted_reason'] = 'NULL';
        }

        if (!$aValues['published'] || $aValues['prod_status'] != 1) {
            /*
        $aVal = array('published' => '0');
        $aProdList = Common::GetAll("SELECT P.id
        FROM `products` P
        JOIN `products_packets_items` I ON I.packet_id = P.id
        WHERE I.product_id =" . $iId);
        $aListP = array(0);
        foreach ($aProdList as $sProd)
            $aListP[] = $sProd['id'];
        $bIsErr = Common::Update($aConfig['tabls']['prefix'] . "products", $aVal, "id IN (" . implode(',', $aListP) . ")") === false;
        */
            /**
             * TODO usuniecie z shadowa
             *
             */
        }

        if (isset($_POST['products_websites'])) {
            $sSql = 'DELETE FROM products_websites WHERE product_id = ' . $iId;
            $this->pDbMgr->Query('profit24', $sSql);
            if (!empty($_POST['products_websites'])) {
                foreach ($_POST['products_websites'] as $iWebsite) {
                    $aPVal = [
                        'website_id' => $iWebsite,
                        'product_id' => $iId
                    ];
                    $this->pDbMgr->Insert('profit24', 'products_websites', $aPVal);
                }
            }
        }


        if ($this->AffectUpdateInOrders($iId, $aValues) === false) {
            $bIsErr = true;
            $iErrorCode .= 1;
        }

        include_once($_SERVER['DOCUMENT_ROOT'] . 'import/CommonSynchro.class.php');
        $oCommonSynchro = new CommonSynchro();
        $sPublisher = $_POST['publisher'];
        $aValues['publisher_id'] = $oCommonSynchro->getPublisherId($sPublisher);

        $aValues['published'] = isset($_POST['published']) ? '1' : '0';

        // aktualizacja
        if (!$bIsErr) {
            if (Common::Update($aConfig['tabls']['prefix'] . "products", $aValues, "id = " . $iId) === false) {
                $bIsErr = true;
                $iErrorCode .= 2;
                $message = mysql_error($pDB->connection);
                $iErrorCode .= $message;
            }
        }
        if (!$bIsErr) {
            if ($aCurrent['published'] != $aValues['published']) {
                if ($aValues['published'] == '1') {
                    if ($this->publishTags($iId) == false) {
                        $bIsErr = true;
                        $iErrorCode .= 3;
                    }
                } else {
                    if ($this->unpublishTags($iId) == false) {
                        $bIsErr = true;
                        $iErrorCode .= 4;
                    }
                }
            }
        }

        if (!$bIsErr) {
            if ($this->proceedInsertUpdateGameAttributes($iId) === false) {
                $bIsErr = true;
                $iErrorCode .= 5;
            }
        }


        // jeśli produkt jest składową pakietu, aktualizacja tych pakietów
        $aPacketsProduct = $this->getPacketsProduct($iId);
        foreach ($aPacketsProduct as $iPacketProduct) {
            if ($this->updateProductPacketPrice($iId, $iPacketProduct) === false) {
                $bIsErr = true;
                $iErrorCode .= 6;
                break;
            }
            if ($this->updatePacketAuthorsPrices($iPacketProduct) === false) {
                $bIsErr = true;
                $iErrorCode .= 7;
                break;
            }
        }


        //usuniecie poprzednich serii wydawniczych
        if (!$bIsErr) {
            $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products_to_series
						WHERE product_id = " . $iId;
            if (Common::Query($sSql) === false) {
                $bIsErr = true;
                $iErrorCode .= 8;
            }
        }
        if (!$bIsErr) {
            // dodanie serii wydawniczych
            if (empty($_POST['series']) && !empty($_POST['series_name']) && !empty($aValues['publisher_id'])) {
                $aValues = array(
                    'name' => decodeString($_POST['series_name']),
                    'publisher_id' => (int)$aValues['publisher_id'],
                    'created' => 'NOW()',
                    'created_by' => $_SESSION['user']['name']
                );
                if (($iNewId = Common::Insert($aConfig['tabls']['prefix'] . "products_series", $aValues)) !== false) {
                    $_POST['series'] = $iNewId;
                } else {
                    $bIsErr = true;
                    $iErrorCode .= 9;
                }
            }
        }
        if (!$bIsErr) {
            if (!empty($_POST['series'])) {
                $aValues = array(
                    'product_id' => $iId,
                    'series_id' => $_POST['series']
                );
                if ((Common::Insert($aConfig['tabls']['prefix'] . "products_to_series", $aValues, '', false)) === false) {
                    $bIsErr = true;
                    $iErrorCode .= 10;
                }
            }
        }
        // załacznik
        $iCurrentAttach = $this->getCurrentAttachment($iId);
        if (!$bIsErr) {
            if (!empty($_POST['attachment_type'])) {
                $aValues = array(
                    'attachment_type' => $_POST['attachment_type'],
                    'price_netto' => Common::formatPrice2($_POST['attachment_price_netto']),
                    'price_brutto' => (Common::formatPrice2($_POST['attachment_price_netto']) * (1 + $_POST['attachment_vat'] / 100)),
                    'vat' => (int)$_POST['attachment_vat']
                );
                if ($iCurrentAttach > 0) {
                    if (Common::Update($aConfig['tabls']['prefix'] . "products_attachments", $aValues, "id = " . $iCurrentAttach) === false) {
                        $bIsErr = true;
                        $iErrorCode .= 11;
                    }
                } else {
                    $aValues['product_id'] = $iId;
                    if ((Common::Insert($aConfig['tabls']['prefix'] . "products_attachments", $aValues, '', false)) === false) {
                        $bIsErr = true;
                        $iErrorCode .= 12;
                    }
                }
            } else {
                if (false === $this->checkAttIsInOpenedOrder($iCurrentAttach)) {
                    if ($iCurrentAttach > 0) {
                        $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products_attachments
                                 WHERE product_id = " . $iId;
                        if (Common::Query($sSql) === false) {
                            $bIsErr = true;
                            $iErrorCode .= 13;
                        }
                    }
                } else {
                    $this->sMsg .= GetMessage('Załącznik nie zostal usunięty ponieważ znajduje sie w otwartym zamówieniu.');
                }
            }
        }

        sort($_POST['extra_categories'], SORT_NUMERIC);
        $aActCat = $this->getExtraCategories($iId);
        sort($aActCat, SORT_NUMERIC);
        if ($_POST['extra_categories'] != $aActCat) {
            //usuniecie poprzednich dodatkopwych kategorii
            if (!$bIsErr) {
                $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products_extra_categories
              WHERE product_id = " . $iId;
                if (Common::Query($sSql) === false) {
                    $bIsErr = true;
                    $iErrorCode .= 14;
                }
            }
            // dodanie dodatkopwych kategorii
            $_POST['extra_categories'][] = $_POST['page_id'];
            // dodanie dodatkopwych kategorii
            if (!$bIsErr && !empty($_POST['extra_categories'])) {
                foreach ($_POST['extra_categories'] as $iCatId) {
                    if ($iCatId != $aConfig['import']['unsorted_category'] && !$this->existExtraCategory($iCatId, $iId)) {
                        $aValues = array(
                            'product_id' => $iId,
                            'page_id' => $iCatId
                        );
                        if ((Common::Insert($aConfig['tabls']['prefix'] . "products_extra_categories", $aValues, '', false)) === false) {
                            $bIsErr = true;
                            $iErrorCode .= 14;
                        }
                        // pobranie kategorii - rodzicow wybranej kategorii
                        $aParents = $this->getPathIDs($iCatId, $_SESSION['lang']['id']);
                        // dodanie mapowan w gore drzewa lokalnego
                        if (!empty($aParents)) {
                            foreach ($aParents as $iParent) {
                                if (!$this->existExtraCategory($iParent, $iId)) {
                                    $aValues = array(
                                        'product_id' => $iId,
                                        'page_id' => $iParent
                                    );
                                    if ((Common::Insert($aConfig['tabls']['prefix'] . "products_extra_categories", $aValues, '', false)) === false) {
                                        $bIsErr = true;
                                        $iErrorCode .= 15;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!$bIsErr) {
            // aktualizacja tagow
            if (false === $this->addTags($iId, trim($_POST['tags']) != '' ? explode(',', $_POST['tags']) : array(), true)) {
                $bIsErr = true;
                $iErrorCode .= 16;
            }
        }

        // status nowości
        if (!$bIsErr) {
            $aNews = $this->getNews($iId);
            if (empty($aNews)) {
                // produkt nie jest obecnie nowością
                if ($_POST['news_status'] > 0) {
                    // dodajemy do nowości
                    if ($this->addNews($iId, true) === false) {
                        $bIsErr = true;
                        $iErrorCode .= 17;
                    }
                }
            } else {
                // produkt jest nowością
                if ($_POST['news_status'] > 0) {
                    if ($_POST['news_status'] != $aNews['page_id']) {
                        // mamy zmianę - aktualizujemy nowość
                        $aValues = array(
                            'page_id' => $_POST['news_status']
                        );
                        if ((Common::Update($aConfig['tabls']['prefix'] . "products_news", $aValues, 'id = ' . $aNews['id'])) === false) {
                            $bIsErr = true;
                            $iErrorCode .= 18;
                        }
                    }
                } else {
                    // usuwamy nowość
                    if ($this->deleteNews($iId) === false) {
                        $bIsErr = true;
                        $iErrorCode .= 19;
                    }
                }
            }
        }


        // status nowości
        if (!$bIsErr) {
            $aNews = $this->getPreviews($iId);
            if (empty($aNews)) {
                // produkt nie jest obecnie nowością
                if ($_POST['previews_status'] > 0) {
                    // dodajemy do nowości
                    if ($this->addPreview($iId, true) === false) {
                        $bIsErr = true;
                        $iErrorCode .= 20;
                    }
                }
            } else {
                // produkt jest nowością
                if ($_POST['previews_status'] > 0) {
                    if ($_POST['previews_status'] != $aNews['page_id']) {
                        // mamy zmianę - aktualizujemy nowość
                        $aValues = array(
                            'page_id' => $_POST['previews_status']
                        );
                        if ((Common::Update($aConfig['tabls']['prefix'] . "products_previews", $aValues, 'id = ' . $aNews['id'])) === false) {
                            $bIsErr = true;
                            $iErrorCode .= 21;
                        }
                    }
                } else {
                    // usuwamy nowość
                    if ($this->deletePreviews($iId) === false) {
                        $bIsErr = true;
                        $iErrorCode .= 22;
                    }
                }
            }
        }

        if (!$bIsErr) {
            // stan publikacji
            $_POST['published'] = (int)$_POST['published'];
            $_POST['old_published'] = (int)$_POST['old_published'];

            // tabela cieni
            // produkt obecnie nie jest opublikowany - zmiana na opublikowany - dodanie cieia
            if ($_POST['old_published'] == 0) {
                if ($_POST['published'] == 1) {
                    if ($this->existShadow($iId)) {
                        if ($this->updateShadow($iId) === false) {
                            $bIsErr = true;
                            $iErrorCode .= 23;
                        }
                    } else {
                        if ($this->addShadow($iId) === false) {
                            $message = mysql_error($pDB->connection);
                            $bIsErr = true;
                            $iErrorCode .= 24;
                            $iErrorCode .= $message;
                        }
                    }
                }
            } else {
                // produkt jest opublikowany - zmiana na nieopublikowany - usuniecie cienia
                if ($_POST['published'] == 0) {
                    if ($this->deleteShadow($iId) === false) {
                        $iErrorCode .= 25;
                        $bIsErr = true;
                    }
                } else {
                    // brak zmiany stany publikacji - aktualizacja cienia
                    if ($this->existShadow($iId)) {
                        if ($this->updateShadow($iId) === false) {
                            $iErrorCode .= 26;
                            $bIsErr = true;
                        }
                    } else {
                        if ($this->addShadow($iId) === false) {
                            $iErrorCode .= 27;
                            $bIsErr = true;
                        }
                    }
                }
            }
        }

//        // ok robimy maly fix na serie wydawnicze - wiem zły, można sie pobawić ze zmiennymi w SQL set @nazwa = zmienna;
//        $sSql = "UPDATE products SET price_brutto = price_brutto + 0.01 WHERE id = " . $iId . " LIMIT 1;";
//        Common::Query($sSql);
//        $sSql = "UPDATE products SET price_brutto = price_brutto - 0.01 WHERE id = " . $iId . " LIMIT 1;";
//        Common::Query($sSql);

        $productSql = "
        SELECT DISTINCT P.id, (
              SELECT MI2.id
              FROM products_extra_categories as PEC2
              JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
              JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
              WHERE PEC2.product_id = P.id
                  AND IF(P.product_type != 'K', MENUS.product_type = P.product_type, (MENUS.product_type = 'K'))
                  AND MI2.published = '1'
                  AND MI2.mtype = '0'
                  AND MI2.priority > 0
                  AND MI2.parent_id IS NOT NULL
              ORDER BY MI2.priority ASC LIMIT 1) as page_id
        FROM products AS P
        WHERE P.id = $iId
        ";

        $productMainCategory = $pDbMgr->GetRow('profit24', $productSql);
        $mCatId = $productMainCategory['page_id'];

        if ($mCatId > 0) {
            $sSql = "UPDATE products SET main_category_id = $mCatId WHERE id = " . $iId . " LIMIT 1;";
            Common::Query($sSql);
        }

        $sendAndBack = $_POST['send_and_back'];

        //$this->trySynchronizeProduct($iId);
        $oSynchro = new synchroProduct($this->pDbMgr);
        $oSynchro->trySynchronizeProduct($iId, true);

        if (!$bIsErr) {
            // rekord zostal zaktualizowany
            SetModification('products', $iId);

            $sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'], getPagePath((double)$_POST['page_id'], $this->iLangId, true), $_POST['name']);
            $this->sMsg .= GetMessage($sMsg, false);
            // dodanie informacji do logow
            Common::CommitTransaction();
            AddLog($sMsg, false);

            if (false === $oProductContext->updateShopsProductWithTransaction($_POST, $iId) ) {
                $bIsErr = true;
                $iErrorCode .= 88;
            }

            if ($aConfig['common']['status'] == 'productive') {
                $elasticIndex = new \Elasticsearch\Profit\ElasticIndex();
                $elasticIndex->createIndex([$iId], 1);
            }

            if (null !== $sendAndBack && null !== $backUri) {

                doRedirect('/omniaCMS/' . $backUri);
            }

            $this->Show($pSmarty);
        } else {
            // rekord nie zostal zmieniony,
            // wyswietlenie komunikatu o niepowodzeniu
            // oraz ponowne wyswietlenie formularza edycji
            Common::RollbackTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'], getPagePath((double)$_POST['page_id'], $this->iLangId, true), $_POST['name']) .
                ' KOD BŁĘDU: ' . $iErrorCode;
            $this->sMsg .= GetMessage($sMsg);
            Common::RollbackTransaction();
            AddLog($sMsg);
            $this->AddEdit($pSmarty, $iId);
        }
    }// end of Update() funciton

    /**
     * @param $iAttId
     * @return mixed
     */
    private function checkAttIsInOpenedOrder($iAttId)
    {

        $sSql = 'SELECT O.id
                 FROM orders_items AS OI
                  JOIN orders AS O
                  ON O.id = OI.order_id
                 WHERE OI.attachment_id = ' . $iAttId . ' AND O.order_status <> "4" AND O.order_status <> "5" 
                 ';
        return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
    }

    /**
     *
     * @param int $iProductId
     */
    private function trySynchronizeProduct($iProductId)
    {

        $oMP = new manageProduct();
        $oGP = new getProduct($this->pDbMgr);
        $oProduct = $oGP->getSingleProduct($iProductId, false);

        if ($this->checkStreamsoftProductExists($oMP, $oProduct->getEAN_13()) === true) {
            $oMP->setProductToUpdate($oProduct);
            $oMP->saveUpdate();
        } else {
            $oMP->addProduct($oProduct);
        }
    }

    /**
     *
     * @param manageProduct $oMP
     * @param string $sEAN_13
     * @return boolean
     */
    private function checkStreamsoftProductExists(manageProduct $oMP, $sEAN_13)
    {

        if ($sEAN_13 != '') {
            $oKProduct = $oMP->checkProduct($sEAN_13);
            if (isset($oKProduct) && !empty($oKProduct)) {
                return true;
            } else {
                return false;
            }
        } else {
            throw new Exception(_('Pusty EAN_13'));
        }
    }

    /**
     * Metoda aktualizuje wagę produktu w złożonych zamówieniach o statusie "W realizacji" i "Skompletowane"
     *
     * @param    integer $iId - Id produktu
     * @param    string $sWeight - Waga do aktualizacji
     * @return bool
     */
    private function doUpdateWeight($iId, $sWeight)
    {
        $sWeight = str_replace(',', '.', $sWeight);

        $sSql = "UPDATE orders_items AS OI
      JOIN orders AS O
      ON OI.order_id = O.id
      SET OI.weight = " . $sWeight . " 
      WHERE 
          O.order_status NOT IN ('4','5') 
      AND OI.product_id = " . $iId . " 
      AND OI.deleted = '0'";

        return (Common::Query($sSql) === false ? false : true);
    }


    /**
     * Metoda stosuje zmiany w zamówieniach
     *
     * @param    integer $iId - Id produktu
     * @param    array $aProduct - Tablica danych produktu
     * @return bool
     */
    private function AffectUpdateInOrders($iId, $aProduct)
    {
        if ($this->updateWeightOrders($iId, $aProduct) === false) {
            return false;
        }

        return true;
    }

    /**
     * Metoda aktualizuje wagę, jeśli spełniony został warunek
     *
     * @param    integer $iId - Id produktu
     * @param    array $aProduct - Tablica danych produktu
     * @return bool
     */
    public function updateWeightOrders($iId, $aProduct)
    {
        if ($this->checkUpdateWeightOrders($iId, $aProduct) === true) {
            if ($this->doUpdateWeight($iId, $aProduct['weight']) === false) {
                return false;
            }
            if ($this->doUpdateWeightOtherServices($iId, $aProduct['weight']) === false) {
                return false;
            }
            return true;
        } else {
            return true;
        }
    }


    /**
     * Zmiana Wagi
     *
     * @global DatabaseManager $pDbMgr
     * @param type $iProductId
     * @param type $fWeight
     * @return boolean
     */
    private function doUpdateWeightOtherServices($iProductId, $fWeight)
    {
        global $pDbMgr;

        $aValues = array(
            'weight' => $fWeight
        );
        if ($pDbMgr->Update('np', 'products', $aValues, 'profit24_id = ' . $iProductId) === false) {
            return false;
        }

        if ($pDbMgr->Update('smarkacz', 'products', $aValues, 'profit24_id = ' . $iProductId) === false) {
            return false;
        }
        if ($pDbMgr->Update('mestro', 'products', $aValues, 'profit24_id = ' . $iProductId) === false) {
            return false;
        }
        if ($pDbMgr->Update('it', 'products', $aValues, 'profit24_id = ' . $iProductId) === false) {
            return false;
        }
        return true;
    }


    /**
     * Metoda porównuje wagę produktu (wartość wyedytowaną przez użytkownika z wartością, która znajduje się w bazie.
     * Jeżeli wartości są różne, efektem będzie aktualizacja wagi w bazie danych przez metodę doUpdateWeight
     *
     * @param    integer $iId - Id produktu
     * @param    array $aProduct - Tablica danych produktu
     * @return bool
     */
    private function checkUpdateWeightOrders($iId, $aProduct)
    {
        $aProductDB = $this->getProductCols($iId, array('weight'));
        if (floatval($aProduct['weight']) != floatval($aProductDB['weight'])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metoda zmienia stan publikacji wybranego produktu
     *
     * @param    object $pSmarty
     * @param    integer $iPId - Id strony
     * @param    integer $iId - Id produktu
     * @return    void
     */
    function Publish(&$pSmarty, $iId)
    {
        global $aConfig;
        $bIsErr = false;
        $aTmp = array();

        if ($iId > 0) {
            $_POST['delete'][$iId] = '1';
        }
        $iI = 0;
        foreach ($_POST['delete'] as $sKey => $sVal) {
            $aTmp[$iI++] = $sKey;
        }
        $_POST['delete'] = &$aTmp;

        if (!empty($_POST['delete'])) {
            Common::BeginTransaction();
            foreach ($_POST['delete'] as $iItem) {

                // pobranie aktualnego stanu publikacji
                $aProduct = &$this->getItem($iItem);
                if ($aProduct['page_id'] == $aConfig['import']['unsorted_category']) {
                    Common::RollbackTransaction();
                    $sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_unsorted_err'], $aProduct['name']);
                    $this->sMsg = GetMessage($sMsg);
                    // dodanie informacji do logow
                    AddLog($sMsg);
                    $this->Show($pSmarty, $iPId);
                    return 0;
                }
                $iPublished = $aProduct['published'] == 1 ? 0 : 1;

                // zmiana stanu publikacji produktu
                $sSql = "UPDATE " . $aConfig['tabls']['prefix'] . "products
							 SET published = IF (published = '0', '1', '0')
							 WHERE id = " . $iItem;
                if (($iRes = Common::Query($sSql)) === false) {
                    $bIsErr = true;
                }

                if ($aProduct['published'] == '1') {
                    if ($this->unpublishTags($iItem) == false) {
                        $bIsErr = true;
                    }
                } else {
                    if ($this->publishTags($iItem) == false) {
                        $bIsErr = true;
                    }
                }


                if ($aProduct['published'] == '1') {
                    if ($this->deleteShadow($iItem) == false) {
                        $bIsErr = true;
                    }
                } else {
                    if (!$this->existShadow($iItem)) {
                        if ($this->addShadow($iItem) == false) {
                            $bIsErr = true;
                        }
                    }
                    if ($aConfig['common']['status'] == 'productive') {
                        $elasticIndex = new \Elasticsearch\Profit\ElasticIndex();
                        $elasticIndex->createIndex([$iId], 1);
                    }
                }

                $sDel .= $aProduct['name'] . ', ';
                SetModification('products', $iItem);
            }
        }
        $sDel = substr($sDel, 0, -2);

        if ($bIsErr) {
            Common::RollbackTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_err'], '', $sDel, $aConfig['lang'][$this->sModule]['published_' . $iPublished]);
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
        } else {
            Common::CommitTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_ok'], '', $sDel, $aConfig['lang'][$this->sModule]['published_' . $iPublished]);
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
        }
        $this->Show($pSmarty, $iPId);
    }

// end of Publish() funciton

    /**
     * Metoda zmienia stan obrobienia wybranego produktu
     *
     * @param    object $pSmarty
     * @param    integer $iPId - Id strony
     * @param    integer $iId - Id produktu
     * @return    void
     */
    function Review(&$pSmarty, $iId)
    {
        global $aConfig;
        $bIsErr = false;
        $aTmp = array();

        if ($iId > 0) {
            $_POST['delete'][$iId] = '1';
        }
        $iI = 0;
        foreach ($_POST['delete'] as $sKey => $sVal) {
            $aTmp[$iI++] = $sKey;
        }
        $_POST['delete'] = &$aTmp;

        if (!empty($_POST['delete'])) {
            Common::BeginTransaction();
            foreach ($_POST['delete'] as $iItem) {

                // pobranie aktualnego stanu publikacji
                $aProduct = &$this->getItem($iItem);

                // zmiana stanu publikacji produktu
                $sSql = "UPDATE " . $aConfig['tabls']['prefix'] . "products
								 SET reviewed = '1'
								 WHERE id = " . $iItem;
                if (Common::Query($sSql) === false) {
                    $bIsErr = true;
                }
                $sDel .= $aProduct['name'] . ', ';
                SetModification('products', $iItem);
            }
        }
        $sDel = substr($sDel, 0, -2);
        if ($bIsErr) {
            Common::RollbackTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['review_err'], '', $sDel);
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
        } else {
            Common::CommitTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['review_ok'], '', $sDel);
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
        }
        $this->Show($pSmarty, $iPId);
    }// end of Publish() funciton

    function UpdateTags(&$pSmarty)
    {
        global $aConfig;
        $bIsErr = false;
        $aTmp = array();
        $iI = 0;
        foreach ($_POST['products'] as $sKey => $sVal) {
            $aTmp[$iI++] = $sKey;
        }
        $_POST['products'] = &$aTmp;

        $aTags = explode(',', $_POST['tags']);
        // sprawdzenie tagow
        if (trim($_POST['tags']) != '' && !$this->checkTags($_POST['tags'])) {
            // nieprawidlowo wypelnione pole tagi
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
            $this->Show($pSmarty);
            return;
        }
        if (trim($_POST['tags']) == '') {
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_empty'], true);
        } else {

            if (!empty($_POST['products'])) {
                Common::BeginTransaction();
                foreach ($_POST['products'] as $iItem) {

                    if (!$bIsErr) {
                        if ($this->addTags($iItem, $aTags) === false) {
                            $bIsErr = true;
                        } else {
                            if ($this->updateShadowTags($iItem) === false) {
                                $bIsErr = true;
                            }
                        }
                    }
                    $sDel .= $this->getItemName($iItem) . ', ';
                }
            }
            $sDel = substr($sDel, 0, -2);

            if ($bIsErr) {
                Common::RollbackTransaction();
                $sMsg = sprintf($aConfig['lang'][$this->sModule]['tags_err'], $_POST['tags'], $sDel);
                $this->sMsg = GetMessage($sMsg);
                // dodanie informacji do logow
                AddLog($sMsg);
            } else {
                Common::CommitTransaction();
                foreach ($_POST['products'] as $iItem) {
                    SetModification('products', $iItem);
                }
                $sMsg = sprintf($aConfig['lang'][$this->sModule]['tags_ok'], $_POST['tags'], $sDel);
                $this->sMsg = GetMessage($sMsg, false);
                // dodanie informacji do logow
                AddLog($sMsg, false);
            }
        }
        $this->Show($pSmarty);
    }

// end of Publish() funciton

    function UpdateStatus(&$pSmarty)
    {
        global $aConfig;
        $bIsErr = false;
        $aTmp = array();
        $iI = 0;
        foreach ($_POST['products'] as $sKey => $sVal) {
            $aTmp[$iI++] = $sKey;
        }
        $_POST['products'] = &$aTmp;

        $aTags = explode(',', $_POST['tags']);
        if ($_POST['prod_status'] == '') {
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['statuses_empty'], true);
        } else {
            Common::BeginTransaction();
            if (!empty($_POST['products'])) {
                foreach ($_POST['products'] as $iItem) {
                    if (!$bIsErr) {
                        $aValues = array(
                            'prod_status' => isset($_POST['prod_status']) ? $_POST['prod_status'] : '0'
                        );
                        if (Common::Update($aConfig['tabls']['prefix'] . "products", $aValues, "id = " . $iItem) === false) {
                            $bIsErr = true;
                        }
                    }
                    if (!$bIsErr && $this->existShadow($iItem)) {
                        $aValues = array(
                            'prod_status' => isset($_POST['prod_status']) ? $_POST['prod_status'] : '0'
                        );
                        if (Common::Update($aConfig['tabls']['prefix'] . "products_shadow", $aValues, 'id = ' . $iItem) === false) {
                            $bIsErr = true;
                        }
                    }
                    $sDel .= $this->getItemName($iItem) . ', ';
                }
                $sDel = substr($sDel, 0, -2);
            }


            if ($bIsErr) {
                Common::RollbackTransaction();
                $sMsg = sprintf($aConfig['lang'][$this->sModule]['statuses_err'], $aConfig['lang'][$this->sModule]['status_' . $_POST['prod_status']], $sDel);
                $this->sMsg = GetMessage($sMsg);
                // dodanie informacji do logow
                AddLog($sMsg);
            } else {
                Common::CommitTransaction();
                foreach ($_POST['products'] as $iItem) {
                    SetModification('products', $iItem);
                }
                $sMsg = sprintf($aConfig['lang'][$this->sModule]['statuses_ok'], $aConfig['lang'][$this->sModule]['status_' . $_POST['prod_status']], $sDel);
                $this->sMsg = GetMessage($sMsg, false);
                // dodanie informacji do logow
                AddLog($sMsg, false);
            }
        }
        $this->Show($pSmarty);
    }

// end of UpdateStatus() funciton

    /**
     * @return array
     */
    private function getProductTypesList()
    {
        $aPTypes = \LIB\EntityManager\Entites\Product::getProductTypes();
        $aPTypesList = $this->mapKeyValuToList($aPTypes, true);
        return $aPTypesList;
    }


    /**
     * @return array
     */
    private function getSexOptionsList()
    {
        $aSTypes = \LIB\EntityManager\Entites\ProductGameAttributes::getSexOptions();
        $aSTypesList = $this->mapKeyValuToList($aSTypes, false);
        return $aSTypesList;
    }


    /**
     * @param $aPTypes
     * @return array
     */
    private function mapKeyValuToList($aPTypes, $pType = false)
    {

        $aPTypesList = [];
        $i = 0;
        foreach ($aPTypes as $name => $symbol) {
            $aPTypesList[$i] = [
                'label' => $name,
                'value' => $symbol,
            ];
            if ($pType = true) {
                $aPTypesList[$i]['onclick'] = (\LIB\EntityManager\Entites\Product::PRODUCT_TYPE_BOOK !== $symbol ? '$(\'.game_attr\').parent().show();' : '$(\'.game_attr\').parent().hide();');
            }
            $i++;
        }
        return $aPTypesList;
    }


    /**
     * @param $pForm
     * @return mixed
     */
    private function getGameAttributes(\FormTable $pForm, $aData)
    {


        $pForm->AddMergedRow(_('Atrybuty zabawek'), ['class' => 'game_attr merged']);

        $sexOptions = $this->getSexOptionsList();
        $pForm->AddRadioSet('sex', _('Płeć'), $sexOptions, $aData['sex'], '', false, true, ['class' => 'game_attr']);

        $pForm->AddText('age', _('Wiek w miesiącach'), $aData['age'], [], _('Pole w zakresie od do'), 'text', false, '/(\d*[\-\s]\d*)/', null, ['class' => 'game_attr']);//

        $pForm->AddText('game_time', _('Czas gry'), $aData['game_time'], [], _('Pole w zakresie od do'), 'text', false, '/(\d*\-\d*)/', null, ['class' => 'game_attr']);//

        $pForm->AddText('number_of_players', _('Ilość graczy'), $aData['number_of_players'], [], '', 'uint', false, '', null, ['class' => 'game_attr']);

        $pForm->AddText('count_elements', _('Ilość elementów'), $aData['count_elements'], [], '', 'text', false, '', null, ['class' => 'game_attr']);

        $pForm->AddText('lego_code', _('Kod produktu lego'), $aData['lego_code'], [], '', 'text', false, '', null, ['class' => 'game_attr']);

        $pForm->AddWYSIWYG('content_of_box', _('Zawartość pudełka'), $aData['content_of_box'], [], '', false, ['class' => 'game_attr']);


        return $pForm;
    }

    /**
     * @param $iId
     * @param bool $bForceInsert
     * @return bool
     */
    private function proceedInsertUpdateGameAttributes($iId, $bForceInsert = false)
    {

        $iPGAId = 0;
        if ($_POST['product_type'] !== \LIB\EntityManager\Entites\Product::PRODUCT_TYPE_BOOK) {
            $aValues = [
                'product_id' => $iId,
                'sex' => $_POST['sex'],
                'age' => $_POST['age'],
                'game_time' => $_POST['game_time'],
                'number_of_players' => $_POST['number_of_players'],
                'content_of_box' => $_POST['content_of_box'],
                'count_elements' => $_POST['count_elements'],
                'lego_code' => $_POST['lego_code'],
                'modified' => 'NOW()',
                'modified_by' => $_SESSION['user']['name'],
            ];

            if (false === $bForceInsert) {
                $sSql = 'SELECT id FROM products_game_attributes WHERE product_id =' . $iId;
                $iPGAId = Common::GetOne($sSql);
            }

            if ($iPGAId > 0) {
                if ((Common::Update("products_game_attributes", $aValues, ' id = ' . $iPGAId)) === false) {
                    return false;;
                }
            } else {
                if ((Common::Insert("products_game_attributes", $aValues)) === false) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Metoda pobera listę serwisow dla SELECT'a
     *
     * @global array $aConfig
     * @return array
     */
    private function getWebsitesList()
    {
        global $aConfig;

        $sSql = "SELECT name AS label, id AS value
						 FROM websites
						 WHERE url LIKE 'http%' ";
        return Common::GetAll($sSql);
    }// end of getWebsitesList() method

    private function getProductWebsites($iPId)
    {
        global $aConfig;

        $sSql = "SELECT website_id
						 FROM products_websites
						 WHERE product_id = " . $iPId;
        return Common::GetCol($sSql);
    }

    /**
     * @param $aData
     * @return mixed
     */
    function fillPriceIfEmpty($aData, $iId)
    {

        $aSources = ['azymut', 'ateneum', 'siodemka', 'platon', 'dictum'];

        if ($aData['price_brutto'] > 0.00) {
            return $aData;
        }

        $sSql = 'SELECT * FROM products_stock WHERE id = ' . $iId;
        $aStock = $this->pDbMgr->GetRow('profit24', $sSql);

        foreach ($aSources as $column) {
            $priceNetto = $column . '_price_netto';
            $priceBrutto = $column . '_price_brutto';
            $vat = $column . '_vat';

            if (isset($aStock[$priceNetto]) && $aStock[$priceNetto] > 0) {
                $aData['price_netto'] = $aStock[$priceNetto];
                $aData['price_brutto'] = $aStock[$priceBrutto];
                $aData['vat'] = $aStock[$vat];
            }

        }

        return $aData;
    }


    /**
     * @param $pSmarty
     * @param $iId
     */
    public function LocationHistory($pSmarty, $iId)
    {
        global $aConfig;

        $sSql = 'SELECT * 
                 FROM products_stock_locations AS PSL
                 WHERE PSL.products_stock_id = ' . $iId . '
                 ORDER BY id DESC
                 ';
        $productsStockLocations = $this->pDbMgr->GetAll('profit24', $sSql);

        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('products_stock_locations', _('Historia lokalizacji'));
        if (!empty($productsStockLocations)) {
            foreach ($productsStockLocations as $productsStockLocation) {
                if (
                    $productsStockLocation['new_g_location'] != '' ||
                    $productsStockLocation['old_g_location'] != ''
                ) {
                    $pForm->AddRow($productsStockLocation['last_update'], '<h2 class="location-style">' . $productsStockLocation['old_g_location'] . ' -> ' . $productsStockLocation['new_g_location']) . '</h2>';
                }

                if (
                    $productsStockLocation['new_x_location'] != '' ||
                    $productsStockLocation['old_x_location'] != ''
                ) {
                    $pForm->AddRow($productsStockLocation['last_update'], '<h2 class="location-style">' . $productsStockLocation['old_x_location'] . ' -> ' . $productsStockLocation['new_x_location']) . '</h2>';
                }

            }
        } else {
            $pForm->AddRow('', _('Brak danych'));
        }
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show'), array('id')) . '\');'), 'button'));
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
    }

    /**
     * Metoda tworzy formularz dodawania / edycji produktu
     *
     * @param    object $pSmarty
     * @param    integer $iId - ID edytowanego rekordu
     */
    function AddEdit(&$pSmarty, $iId = 0)
    {
        global $aConfig;

        $backUri = null;
        if (isset($_GET['back_uri']) && !empty($_GET['back_uri'])) {
            $backUri = $_GET['back_uri'];
        }

        $aData = array();
        $sHtml = '';

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
//		$pSmarty->assign('sBodyOnload', 'checkDiscounts(document.getElementById(\'discount\'), document.getElementById(\'exclude_discount\'), document.getElementById(\'users_discount\'), document.getElementById(\'total_value_discount\'));');

        $aExtraCat = $this->getProductsCategories(Common::getProductsMenu($this->iLangId), '');
        $aCats = array();
        $aData['legal_status_date'] = '00-00-0000';
        $aData['shipment_date'] = '00-00-0000';
//		$aData['type'] = '0';// domyślnie
        if ($iId > 0) {
            // pobranie z bazy danych edytowanego rekordu
            $sSql = "SELECT A.*, PGA.*, A.published old_published, A.page_id curr_page_id,
								DATE_FORMAT(legal_status_date, '" . $aConfig['common']['cal_sql_date_format'] . "')
											AS legal_status_date, 
								DATE_FORMAT(shipment_date, '" . $aConfig['common']['cal_sql_date_format'] . "')
											AS shipment_date,	B.name AS publisher, 
								GROUP_CONCAT(DISTINCT C.azymut_index SEPARATOR ', ') 
									AS old_azymut_indexes,
									PS.profit_g_location,
									PS.profit_x_location
							 FROM " . $aConfig['tabls']['prefix'] . "products A
							 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers B
							 ON A.publisher_id=B.id
							 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_stock PS
							    ON A.id=PS.id
							 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_azymut_bookindex C
								 ON A.id=C.product_id
							 LEFT JOIN products_game_attributes AS PGA
							  ON PGA.product_id = A.id
							 WHERE A.id = " . $iId;
            $aData = &Common::GetRow($sSql);

            $aData = $this->fillPriceIfEmpty($aData, $iId);

            if (empty($aData['legal_status_date'])) {
                $aData['legal_status_date'] = '00-00-0000';
            }
            if (empty($aData['shipment_date'])) {
                $aData['shipment_date'] = '00-00-0000';
            }
            $sName = $aData['plain_name'];
            $aData['series'] = $this->getProductSeries($iId);
            $aData['news_status'] = $this->getNewsStatus($iId);
            $aData['previews_status'] = $this->getPreviewsStatus($iId);
            $aCats = $this->getExtraCategories($iId);
            $aAttachData = $this->getAttachmentForProduct($iId);
            if (!empty($aAttachData)) {
                $aData = array_merge($aData, $aAttachData);
            }
            // tagi
            $aData['tags'] = $this->getProductTags($iId);
            $aData['news_status'] = $this->getNewsStatus($iId);
            $aData['products_websites'] = $this->getProductWebsites($iId);
        } else {
            if (!isset($aData['product_type'])) {
                $aData['product_type'] = \LIB\EntityManager\Entites\Product::PRODUCT_DEFAULT_TYPE;
            }
        }
        if (!empty($_POST)) {
            $aData = &$_POST;
            if (isset($aData['extra_categories'])) {
                $aCats = $aData['extra_categories'];
            }
        }
        $sCurrentExtraCats = '';
        foreach ($aExtraCat as $aCat) {
            if (in_array($aCat['value'], $aCats)) {
                $sCurrentExtraCats .= getPagePath($aCat['value'], $this->iLangId, true, false, false) . "<br />\n";
            }
        }

        $sHeader = $aConfig['lang'][$this->sModule]['header_' . ($iId > 0 ? 1 : 0)];
        if ($iId > 0 && isset($sName) && !empty($sName)) {
            $sHeader .= ' "' . trimString($sName, 50) . '"';
        }

        $formUriParams = array('id' => $iId);
        if ($backUri !== null) {
            $formUriParams['back_uri'] = urlencode($backUri);
        }

        $pForm = new \FormTable('products', $sHeader, array('action' => phpSelf($formUriParams)), array('col_width' => 155), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
        if ($iId > 0) {
            $pForm->AddHidden('old_published', $aData['old_published']);
            $pForm->AddHidden('curr_page_id', $aData['curr_page_id']);
        }


        if (isset($_SESSION['user']['product_prepare_to_connect']) && $_SESSION['user']['product_prepare_to_connect'] != $iId) {
            $pForm->AddRow(_('Przygotowałeś produkt do zaimportowania: '), '<span style="font-size: 12px; color: red; font-weight:bold"> Czy chcesz zaimportować: "' . $this->getProductAttrib($_SESSION['user']['product_prepare_to_connect'], ' name ') . '" do tego produktu ?  - jeśli tak kliknij <a href="' . phpSelf(array('do' => 'connect', 'id' => $iId)) . '">Zaimportuj</a></span> ');
        }

        if ($aData['streamsoft_indeks'] != '') {
            if ($_SESSION['user']['name'] == 'mchudy') {
                $pForm->AddText('streamsoft_indeks', _('Streamsoft indeks'), $aData['streamsoft_indeks']);
            } else {
                $pForm->AddRow(_('Streamsoft indeks'), $aData['streamsoft_indeks']);
            }
        }

        if ($aData['profit_g_location'] != '') {
            $pForm->AddRow(_('Lokalizacja Stock'), $aData['profit_g_location']);
        }
        if ($aData['profit_x_location'] != '') {
            $pForm->AddRow(_('Lokalizacja wysokiego składowania stock'), $aData['profit_x_location']);
        }

        $aProductTypesList = $this->getProductTypesList();
        $pForm->AddRadioSet('product_type', _('Typ produktu'), $aProductTypesList, $aData['product_type'], '', false, true);


        // dla strony
        //$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], getPagePath((double) $aData['curr_page_id'], $this->iLangId, true), '', array(), array('class'=>'redText'));
        // kategoria
        $pForm->AddSelect('page_id', $aConfig['lang'][$this->sModule]['main_category'], array('style' => 'width:300px;'), $this->getTopProductsCategories(), $aData['page_id']);

        // obecne dodatkowe kategorie
        if (!empty($sCurrentExtraCats)) {
            $pForm->AddRow($aConfig['lang'][$this->sModule]['current_extra_categories'], $sCurrentExtraCats);
        }

        // drzewo kategorii
        $pForm->AddRow($aConfig['lang'][$this->sModule]['extra_categories'], '<div style="height:300px; overflow:auto;">' .
            Form::GetCheckTree('extra_categories[]', array(), $this->getProductsCategoriesChTree(Common::getProductsMenu($this->iLangId)), $aCats)
            . '</div>'
        );


//		$pForm->AddRow($aConfig['lang'][$this->sModule]['extra_categories'],
//									$pForm->GetSelectHtml('avaible_categories',$aConfig['lang'][$this->sModule]['avaible_categories'], array('size'=>10,'style'=>'width:300px;'), $aExtraCat,'','',false).'&nbsp;'.
//									$pForm->GetInputButtonHTML('add_cat', $aConfig['lang'][$this->sModule]['add_cat'], array('onclick'=>'addExtraCategory();'), 'button').'&nbsp;'.
//									$pForm->GetInputButtonHTML('del_cat', $aConfig['lang'][$this->sModule]['del_cat'], array('onclick'=>'delExtraCategory();'), 'button').'&nbsp;'.
//									$pForm->GetSelectHtml('extra_categories',$aConfig['lang'][$this->sModule]['extra_categories'], array('size'=>10,'style'=>'width:300px;','multiple'=>1), $aData['extra_categories'],'','',false));
//

        $pForm->AddRadioSet('news_status', $aConfig['lang'][$this->sModule]['news_status'], array_merge(array(array('value' => 0, 'label' => $aConfig['lang'][$this->sModule]['not_news'])), getModulePages('m_powiazane_oferta_produktowa', $this->iLangId, false, '', 'nowosci')), $aData['news_status'], '', true, false);
        $pForm->AddRadioSet('previews_status', $aConfig['lang'][$this->sModule]['previews_status'], array_merge(array(array('value' => 0, 'label' => $aConfig['lang'][$this->sModule]['not_previews'])), getModulePages('m_powiazane_oferta_produktowa', $this->iLangId, false, '', 'zapowiedzi')), $aData['previews_status'], '', true, false);
//			$pForm2->AddSelect('page_id',$aConfig['lang'][$this->sModule]['page_id'],array(),,$aData['page_id']);
        // nazwa
        $aTypes = array(
            array('label' => $aConfig['lang'][$this->sModule]['type_0'],
                'value' => '0'),
            array('label' => $aConfig['lang'][$this->sModule]['type_1'],
                'value' => '1'),
        );

//		$pForm->AddRadioSet('type', $aConfig['lang'][$this->sModule]['type'], $aTypes, $aData['type'], '', true, false, ['style' => 'visibility: hidden'], ['style' => 'visibility: hidden']);



        $pForm = $this->getGameAttributes($pForm, $aData);


        // wydawnictwo
        //$pForm->AddSelect('publisher_id',$aConfig['lang'][$this->sModule]['publisher'], array('onchange'=>'document.getElementById(\'do\').value =\'add\'; selectExtraCategories(); this.form.submit();'), $this->getPublishers(), $aData['publisher_id']);
        $pForm->AddText('publisher', $aConfig['lang'][$this->sModule]['publisher'], $aData['publisher'], array('id' => 'publisher_aci', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', true);


        // seria
        $pForm->AddRow($aConfig['lang'][$this->sModule]['series'], $pForm->GetSelectHtml('series', $aConfig['lang'][$this->sModule]['series'], array('style' => 'width:300px;'), (!empty($aData['publisher_id']) ? $this->getSeriesList($aData['publisher_id']) : array()), (!empty($aData['publisher_id']) ? $this->getProductSeries($iId) : ''), '', false) .
            '&nbsp;&nbsp;&nbsp;' . $aConfig['lang'][$this->sModule]['add_series'] . '&nbsp;&nbsp;&nbsp;' .
            $pForm->GetTextHTML('series_name', $aConfig['lang'][$this->sModule]['series'], '', array('style' => 'width:200px;', 'maxlength' => 255), '', 'text', false));


        // isbn
        $pForm->AddText('isbn', $aConfig['lang'][$this->sModule]['isbn'], $aData['isbn'], array('maxlength' => 20), '', 'isbn', true);

        // isbn
        $pForm->AddText('isbn_plain', _('Kod ISBN plain'), $aData['isbn_plain'], array('maxlength' => 20), '', 'isbn', false);

        // isbn_10
        $pForm->AddText('isbn_10', $aConfig['lang'][$this->sModule]['isbn_10'], $aData['isbn_10'], array('maxlength' => 10), '', 'text', false, '/^([0-9a-zA-Z]){10}$/');

        // isbn_13
        $pForm->AddText('isbn_13', $aConfig['lang'][$this->sModule]['isbn_13'], $aData['isbn_13'], array('maxlength' => 13), '', 'text', false, '/^([0-9a-zA-Z]){13}$/');

        // ean_13
        $pForm->AddText('ean_13', $aConfig['lang'][$this->sModule]['ean_13'], $aData['ean_13'], array('maxlength' => 13), '', 'text', false, '/^([0-9a-zA-Z]){13}$/');


        // ean_13
        $pForm->AddText('standard_code', _('Kod standardu'), $aData['standard_code'], array('maxlength' => 20, 'style' => 'background-color: #eadce7'), '', 'text', false, '/^([0-9a-zA-Z]){6,20}$/');

        // ilośc w standardzie
        $pForm->AddText('standard_quantity', _('Ilość w standardzie'), $aData['standard_quantity'], array('maxlength' => 5, 'style' => 'width: 50px; background-color: #eadce7'), '', 'uint', false);

        //rok wydania
        //$pForm->AddText('publication_year',$aConfig['lang'][$this->sModule]['publication_year'], $aData['publication_year'],array('maxlength'=>4),'','uinteger' ,false);
        $pForm->AddSelect('publication_year', $aConfig['lang'][$this->sModule]['publication_year'], array(), $this->getYears(), $aData['publication_year'], '', false);

        // wydanie
        $pForm->AddSelect('edition', $aConfig['lang'][$this->sModule]['edition'], array(), $this->getNumbersList(90), $aData['edition'], '', false);
        // stan prawny na
        $pForm->AddText('legal_status_date', $aConfig['lang'][$this->sModule]['legal_status_date'], $aData['legal_status_date'], array(), '', 'date', false);
        // miejsce wydania
        $pForm->AddText('city', $aConfig['lang'][$this->sModule]['city'], $aData['city'], array('id' => 'city_aci', 'maxlength' => 45), '', 'text', false);
        // stron
        $pForm->AddText('pages', $aConfig['lang'][$this->sModule]['pages'], $aData['pages'], array('maxlength' => 6), '', 'text', false);
        //oprawa
        $pForm->AddSelect('binding', $aConfig['lang'][$this->sModule]['binding'], array(), $this->getBindings(), $aData['binding'], '', false);
        // wymiary
        $pForm->AddSelect('dimension', $aConfig['lang'][$this->sModule]['dimension'], array(), $this->getDimensions(), $aData['dimension'], '', false);
        // jezyk ksiazki
        $pForm->AddSelect('language', $aConfig['lang'][$this->sModule]['language'], array(), $this->getLanguages(), $aData['language'], '', false);
        // tłumacz
        $pForm->AddText('translator', $aConfig['lang'][$this->sModule]['translator'], $aData['translator'], array('maxlength' => 100), '', 'text', false);
        // jezyk oryginalny
        $pForm->AddSelect('original_language', $aConfig['lang'][$this->sModule]['original_language'], array(), $this->getLanguages(), $aData['original_language'], '', false);
        // tytul oryginalny
        $pForm->AddText('original_title', $aConfig['lang'][$this->sModule]['original_title'], $aData['original_title'], array('maxlength' => 500), '', 'text', false);
        // waga
        $pForm->AddText('weight', $aConfig['lang'][$this->sModule]['weight'], Common::formatWeight($aData['weight']), array('maxlength' => 12, 'style' => 'width: 75px;'), '', 'ufloat', false);
        // tomów
        $pForm->AddSelect('volumes', $aConfig['lang'][$this->sModule]['volumes'], array(), $this->getNumbersList(50), $aData['volumes'], '', false);
        // nr tomu
        $pForm->AddSelect('volume_nr', $aConfig['lang'][$this->sModule]['volume_nr'], array(), $this->getNumbersList(50), $aData['volume_nr'], '', false);
        // nazwa tomu
        $pForm->AddText('volume_name', $aConfig['lang'][$this->sModule]['volume_name'], $aData['volume_name'], array('maxlength' => 75), '', 'text', false);

        // tagi
        $pForm->AddRow($aConfig['lang'][$this->sModule]['new_tag'], $pForm->GetTextHtml('new_tag', $aConfig['lang'][$this->sModule]['new_tag'], $aData['new_tag'], array('id' => 'tags_aci', 'style' => 'width:300px;', 'maxlength' => 64), '', 'text', false) . '&nbsp;&nbsp;' .
            $pForm->GetInputButtonHTML('add_tag', $aConfig['lang'][$this->sModule]['add_tag_button'], array('id' => 'add_tag_button'), 'button'));
        $pForm->AddTextArea('tags', $aConfig['lang'][$this->sModule]['tags'], $aData['tags'], array('id' => 'tags_list', 'rows' => '8', 'style' => 'width: 450px;'), 512, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);

        $oProductContext = new \LIB\EntityManager\Entites\ProductContext($this->pDbMgr);
        $aWebsitesCodes = $oProductContext->getBookstores();
        $aProductData = [];
        if ( $iId === 0 ) {
            foreach ($aWebsitesCodes as $aSite) {
                $aProductData[$aSite['code']] = [
                    'name' => $_POST[$aSite['code'] . '_name'],
                    'name2' => $_POST[$aSite['code'] . '_name2'],
                    'description' => $_POST[$aSite['code'] . '_description'],
                    'short_description' => $_POST[$aSite['code'] . '_short_description'],
                ];
            }
        } else {
            $aProductData = $oProductContext->getData($iId);
        }

        $aTabs = [];
        $aTabsLabels = [];
        foreach ($aWebsitesCodes as $iKey => $aSite) {
            if ($iId > 0 && $aSite['code'] !== 'profit24' && $oProductContext->getPageId($aSite['code'], $iId) === null) {
                unset($aWebsitesCodes[$iKey]);
            } else {
                $aTabs[$aSite['code']] = '';
                $aTabsLabels[$aSite['code']] = $aSite['name'];
            }
        }

        $pForm->AddTabs('title_description', $aTabs, $aTabsLabels);

        foreach ($aWebsitesCodes as $aSite) {
            $aProductSiteData = $aProductData[$aSite['code']];
            $pForm->AddTabFields('title_description', $aSite['code'], [
                $pForm->GetTextHTML($aSite['code'] . '_name', $aConfig['lang'][$this->sModule]['name'], $aProductSiteData['name'], array('maxlength' => 500, 'style' => 'width: 450px;'), '', 'text', $aSite['code'] == 'profit24'),
                $pForm->GetTextHTML($aSite['code'] . '_name2', $aConfig['lang'][$this->sModule]['name2'], $aProductSiteData['name2'], array('maxlength' => 500, 'style' => 'width: 450px;'), '', 'text', false),
                $pForm->GetWYSIWYGHTML($aSite['code'] . '_description', $aConfig['lang'][$this->sModule]['description'], $aProductSiteData['description'], [], '', $aSite['code'] == 'profit24'),
                $pForm->GetTextAreaHTML($aSite['code'] . '_short_description', $aConfig['lang'][$this->sModule]['short_description'], $aProductSiteData['short_description'], array('rows'=>'8', 'style'=>'width: 450px;'), 255, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false)
            ], [
                $aConfig['lang'][$this->sModule]['name'],
                $aConfig['lang'][$this->sModule]['name2'],
                $aConfig['lang'][$this->sModule]['description'],
                $aConfig['lang'][$this->sModule]['short_description']
            ]);
        }

        // CENA
        $pForm->AddMergedRow($aConfig['lang'][$this->sModule]['price_section'], array('class' => 'merged', 'style' => 'padding-left: 160px;'));

//
//	// bazowa
        $pForm->AddText('price_brutto', $aConfig['lang'][$this->sModule]['price_brutto'], isset($aData['price_brutto']) && $aData['price_brutto'] != '' ? Common::formatPrice($aData['price_brutto']) : '', array('readonly' => 'readonly', 'maxlength' => 12, 'style' => 'width: 75px;'), '', 'ufloat', false);
//	// netto
        $pForm->AddText('price_netto', $aConfig['lang'][$this->sModule]['price_netto'], isset($aData['price_netto']) && $aData['price_netto'] != '' ? Common::formatPrice($aData['price_netto']) : '', array('maxlength' => 12, 'style' => 'width: 75px;', 'onchange' => 'calculateBasePrice();'), '', 'ufloat', true);

        // stawka VAT
        //$pForm->AddText('vat', $aConfig['lang'][$this->sModule]['vat'], $aData['vat'], array('maxlength'=>3, 'style'=>'width: 50px;'), '', 'range', true, '', array(0,100));
        $pForm->AddSelect('vat', $aConfig['lang'][$this->sModule]['vat'], array('onchange' => 'calculateBasePrice();'), $this->getVATList(), $aData['vat'], '', true);

        // pkwiu
        $pForm->AddText('pkwiu', $aConfig['lang'][$this->sModule]['pkwiu'], $aData['pkwiu'], array('id' => 'pkwiu_aci', 'style' => 'width:150px;', 'maxlength' => 255), '', 'text', false);

        // STATUS PRODUKTU
        $pForm->AddMergedRow($aConfig['lang'][$this->sModule]['attachment_section'], array('class' => 'merged', 'style' => 'padding-left: 160px;'));

        // załącznik
        $pForm->AddSelect('attachment_type', $aConfig['lang'][$this->sModule]['attachment_type'], array('onchange' => 'setAttachment(this); calculateBasePrice();'), $this->getAttachmentTypes(), $aData['attachment_type'], '', false);
        if ($aData['attachment_type'] > 0) {
            $aAttachmentAttribs = array();
        } else {
            $aAttachmentAttribs = array('disabled' => 'disabled');
        }
        // netto
        $pForm->AddText('attachment_price_netto', $aConfig['lang'][$this->sModule]['attachment_price_netto'], isset($aData['attachment_price_netto']) && $aData['attachment_price_netto'] != '' ? Common::formatPrice($aData['attachment_price_netto']) : '', array_merge($aAttachmentAttribs, array('maxlength' => 12, 'style' => 'width: 75px;', 'onchange' => 'calculateBasePrice();')), '', 'ufloat', false);

        // stawka VAT
        //$pForm->AddText('vat', $aConfig['lang'][$this->sModule]['vat'], $aData['vat'], array('maxlength'=>3, 'style'=>'width: 50px;'), '', 'range', true, '', array(0,100));
        $pForm->AddSelect('attachment_vat', $aConfig['lang'][$this->sModule]['attachment_vat'], array_merge($aAttachmentAttribs, array('onchange' => 'calculateBasePrice();')), $this->getVATList(), $aData['attachment_vat'], '', false);


        // STATUS PRODUKTU
        $pForm->AddMergedRow($aConfig['lang'][$this->sModule]['status_section'], array('class' => 'merged', 'style' => 'padding-left: 160px;'));

        // status
        $aStatuses = array(
            array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true; $(\'#exhausted_reason\').parent().hide(); '),
            array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['status_0'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true; $(\'#exhausted_reason\').parent().hide(); '),
            array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['status_2'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true; $(\'#exhausted_reason\').parent().show(); '),
            array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['status_3'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=false; $(\'#exhausted_reason\').parent().hide(); ')
        );
        $pForm->AddRadioSet('prod_status', $aConfig['lang'][$this->sModule]['prod_status'], $aStatuses, $aData['prod_status'], '', true, false);

        //Exhausted reason
        $aExhaustedReason = [
            ['value' => '1', 'label' => 'burdello'],
            ['value' => '2', 'label' => 'stare wydanie'],
            ['value' => '3', 'label' => 'niechciane'],
            ['value' => '4', 'label' => 'aaa'],
        ];
        $pForm->AddRadioSet('exhausted_reason', _('Powód statusu wyczerpanego'), $aExhaustedReason, $aData['exhausted_reason'], '', true, false, ['id' => 'exhausted_reason'], [], 'prod_status', '2');

        $pForm->AddCheckBox('ommit_auto_preview', _('Pomiń zmianę auto-niedostępne zapowiedzi'), array(), '', !empty($aData['ommit_auto_preview']), false);
        if ($aData['prod_status'] == '3') {
            $aShipmentDateAttribs = array('style' => 'width: 75px;');
        } else {
            $aShipmentDateAttribs = array('style' => 'width: 75px;', 'disabled' => 'disabled');
        }
        $pForm->AddText('shipment_date', $aConfig['lang'][$this->sModule]['shipment_date'], $aData['shipment_date'], $aShipmentDateAttribs, '', 'date', false);
        // zdefiniowane opisy statusu
        //$pForm->AddSelect('ready_statuses', $aConfig['lang'][$this->sModule]['ready_statuses'], array('onchange' => 'setStatus(this);'), $this->getStatusesDescriptions(), '', '', false);
        // opis statusu
        //$pForm->AddTextArea('prod_status_text', $aConfig['lang'][$this->sModule]['prod_status_text'], $aData['prod_status_text'], array('rows'=>'6', 'style'=>'width: 350px;'), 255, '', $aLang['too_long_prod_status_text'], false);
        // status

        $pShipmentTime = new ShipmentTime();

        $aShipments = array(
            array('value' => '0', 'label' => $pShipmentTime->getShipmentTime($iId, 0)),
            array('value' => '1', 'label' => $pShipmentTime->getShipmentTime($iId, 1)),
            array('value' => '2', 'label' => $pShipmentTime->getShipmentTime($iId, 2)),
            array('value' => '3', 'label' => $pShipmentTime->getShipmentTime($iId, 3)),
            array('value' => '4', 'label' => $pShipmentTime->getShipmentTime($iId, 4)),
            array('value' => '5', 'label' => $pShipmentTime->getShipmentTime($iId, 5)),
            array('value' => '6', 'label' => $pShipmentTime->getShipmentTime($iId, 6)),
            array('value' => '7', 'label' => $pShipmentTime->getShipmentTime($iId, 7)),
            array('value' => '8', 'label' => $pShipmentTime->getShipmentTime($iId, 8)),
            array('value' => '9', 'label' => $pShipmentTime->getShipmentTime($iId, 9)),
            array('value' => '10', 'label' => $pShipmentTime->getShipmentTime($iId, 10)),
        );
        $pForm->AddRadioSet('shipment_time', $aConfig['lang'][$this->sModule]['shipment_time'], $aShipments, $aData['shipment_time'], '', true, false);

        $aSources = $this->_getSources();
        $pForm->AddRadioSet('source', $aConfig['lang'][$this->sModule]['source'], $aSources, isset($aData['source']) ? $aData['source'] : '1', '', true, false);

        // STAN PUBLIKACJI
        $pForm->AddMergedRow($aConfig['lang'][$this->sModule]['publ_section'], array('class' => 'merged', 'style' => 'padding-left: 160px;'));

        // status publikacji

        $pForm->AddCheckBox('published', $aConfig['lang'][$this->sModule]['published'], array(), '', !empty($aData['published']), false);

        $aWebsites = $this->getWebsitesList();
        $sAddName = 'products_websites';
        $pForm->AddCheckBoxSet($sAddName, _('Dostępny w serwisach: <br />(jeśli nic nie zaznaczono dostępny we wszystkich)'), $aWebsites, $aData['products_websites'], '', false, true);
        if ($aData['published'] == '1' || !empty($aData['products_websites'])) {
            $sProductWebsitesJs = '
              $("input[name^=' . $sAddName . ']").attr("onclick", "return false");
            ';
        }

        // status
        if ($aData['source'] != '') {
            $pForm->AddRow($aConfig['lang'][$this->sModule]['source'], $aConfig['lang'][$this->sModule]['source_' . $aData['source']]);
        }
        if (!empty($aData['last_import'])) {
            $pForm->AddRow($aConfig['lang'][$this->sModule]['last_import'], $aData['last_import']);
        }

        if (!empty($aData['modified'])) {
            $pForm->AddRow($aConfig['lang'][$this->sModule]['modified'], $aData['modified']);
        }

        if (!empty($aData['modified_by'])) {
            $pForm->AddRow($aConfig['lang'][$this->sModule]['modified_by'], $aData['modified_by']);
        }

        $aProductSources = $this->getProductSources($iId);
        if (!empty($aData['azymut_index']) || !empty($aProductSources)) {
            $pForm->AddMergedRow("Powiązane źródła zawnętrzne", array('class' => 'merged', 'style' => 'padding-left: 160px;'));
        }
        if (!empty($aData['azymut_index'])) {
            $pForm->AddRow($aConfig['lang'][$this->sModule]['actual_azymut_index'], $aData['azymut_index']);
        }

        if (!empty($aData['old_azymut_indexes'])) {
            $pForm->AddRow($aConfig['lang'][$this->sModule]['old_azymut_index'], $aData['old_azymut_indexes']);
        }

        if (!empty($aProductSources)) {
            $sSourcesHTML = '<table style="margin-left: 90px;">';
            $sSourcesHTML .= '<tr>';
            $sSourcesHTML .= '<td><span style="font-weight: bold">Źródło</span></td><td>id w źródle</td>';
            $sSourcesHTML .= '</tr>';

            foreach ($aProductSources as $sSource => $sId) {
                $sSourcesHTML .= '<tr>';
                $sSourcesHTML .= '<td><span style="font-weight: bold">' . $sSource . '</span></td><td>' . implode(',', $sId) . '</td>';
                $sSourcesHTML .= '</tr>';
            }
            $sSourcesHTML .= '</table>';
            $pForm->AddMergedRow($sSourcesHTML);
        }

        $pForm = $this->getReservationsForm($pForm, $iId);


//		if($aData['reviewed'] =='1'){
//			$pForm->AddRow($aConfig['lang'][$this->sModule]['f_reviewed'],($aData['reviewed']=='1'?$aConfig['lang'][$this->sModule]['f_reviewed']:$aConfig['lang'][$this->sModule]['f_not_reviewed']));
//		}
        // przyciski
        $additional = '';

        if (null !== $backUri) {
            $additional .= $pForm->GetInputButtonHTML('send_and_back', _('Wprowadz zmiany i powróć do wcześniejszego widoku'), array('onclick' => 'selectExtraCategories();')) . '&nbsp;&nbsp;';
        }

        $inputts = $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_' . ($iId > 0 ? 1 : 0)], array('onclick' => 'selectExtraCategories();')) . '&nbsp;&nbsp;' . $additional .
            $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => strpos($_SERVER['HTTP_REFERER'], 'mismatchvat') ? 'window.location.href=\'?frame=main&module_id=26&module=m_oferta_produktowa&action=mismatchvat\'' : 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button');
        $pForm->AddRow('&nbsp;', $inputts);

        if ($aData['product_type'] === \LIB\EntityManager\Entites\Product::PRODUCT_TYPE_BOOK) {
            $sJSAtProductType = '$(".game_attr").parent().hide();';
        } else {
            $sJSAtProductType = '$(".game_attr").parent().show();';
        }

        if ($aData['prod_status'] == '2') {
            $sExhaustedReason = '$("#exhausted_reason").parent().show();';
        } else {
            $sExhaustedReason = '$("#exhausted_reason").parent().hide();';
        }
        // funkcja JS - setStatus
        $sJS = '
		<script type="text/javascript">
    
		$(document).ready(function(){
		    ' . $sJSAtProductType . '
		    ' . $sProductWebsitesJs . '
		    ' . $sExhaustedReason . '
		    
		    
        setInterval(function() {
            $.ajax({
              type: "GET",
              cache: false,
              async: false,
              url: "ajax/continueSession.php"
            });
         }, 60000);


            $("#products").submit( function(event) {
              if ($("#vat option:selected").val() != 23 && $("#pkwiu_aci").val() == "") {
                alert("Podaj PKWiU, wymagane jeśli stawka VAT jest różna od stawki podstawowej");
                event.preventDefault();
                return false;
              }
            });

			if ($("#isbn").val() == ""){
				$("#isbn_10, #isbn_13, #ean_13").keyup(function() {
					$("#isbn").val($(this).val());
				});		
			}

      $("#tags_aci").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetTags.php",
                select: function( event, ui ) {
                  $("#add_tag_button").focus();
                }
      });
      
      $("#publisher_aci").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetPublishers.php"
      });
      
      $("#pkwiu_aci").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetPkwiu.php",
                select: function( event, ui ) {
                  $("#pkwiu_aci").val(ui.item.value.replace(/ \(.*\)/, ""));
                  return false;
                }
      });
      
      $("#city_aci").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetCities.php"
      });
      
			$("#publisher_aci").bind("blur", function(e){
     	 $.get("ajax/GetSeries.php", { publisher: e.target.value },
					  function(data){
					    var brokenstring=data.split(";");
					    var elSelSeries = document.getElementById(\'series\');
					    elSelSeries.length = 0;
					    elSelSeries.options[elSelSeries.length] =  new Option("' . $aConfig['lang']['common']['choose'] . '","0");
					    for (x in brokenstring){
								if(brokenstring[x]){
									var item=brokenstring[x].split("|");
		  						elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
	  						}
	  					}
					 });
    	});
    	$("#add_tag_button").click(function () {
    		var sCurTags = $("#tags_list").val();
    		var sNewTag = $("#tags_aci").val();
    		if(sNewTag != ""){
    			if(sCurTags != ""){
    				sCurTags = sCurTags+",";
					}
      		$("#tags_list").val(sCurTags+sNewTag);
      		$("#tags_aci").val("");
      	}
   		});
    	
		});
		
		function setAttachment(oSel) {
				if (oSel.value > 0) {
					document.getElementById(\'attachment_price_netto\').disabled=false;
					document.getElementById(\'attachment_vat\').disabled=false;
				} else {
					document.getElementById(\'attachment_price_netto\').disabled=true;
					document.getElementById(\'attachment_price_netto\').value=\'\';
					document.getElementById(\'attachment_vat\').disabled=true;
					document.getElementById(\'attachment_vat\').value=\'\';
				}
			}
		
		function calculateBasePrice(){
			var fNetto = parseFloat(document.getElementById(\'price_netto\').value.replace(\',\',\'.\'));
			var iVat = parseFloat(document.getElementById(\'vat\').value);
			var iAttachment = document.getElementById(\'attachment_type\').value;
			var elBasePrice = document.getElementById(\'price_brutto\');
			var fPrice = fNetto * (1+iVat/100);
			if(iAttachment > 0){
				var fAttNetto = parseFloat(document.getElementById(\'attachment_price_netto\').value.replace(\',\',\'.\'));
				var iAttVat = parseFloat(document.getElementById(\'attachment_vat\').value);
				if(!isNaN(fAttNetto) && !isNaN(iAttVat)){
					fPrice=fPrice + fAttNetto * (1+iAttVat/100);
				}
			}
			fPrice = parseInt(fPrice*100)/100;
			elBasePrice.value = fPrice.toString().replace(\'.\',\',\');
		}
		
			function setStatus(oSel) {
				if (oSel.options.selectedIndex > 0) {
					document.getElementById(\'prod_status_text\').value = oSel.options[oSel.options.selectedIndex].value;
				}
			}
		</script>
		';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sJS . ShowTable($pForm->ShowForm()));
    }// end of AddEdit() function


    /**
     * Sprawdzanie czy extra catgegories ma zaznaczona kategorie drugiego poziomu
     *
     * @param $data
     * @return bool
     */
    private function checkExtraCategories($data)
    {
        if (isset($data['extra_categories'])) {
            global $aConfig;
            $sSql = "SELECT parent_id
					 FROM " . $aConfig['tabls']['prefix'] . "menus_items
					 WHERE id IN (" . implode(',', $data['extra_categories']) . ") AND parent_id IS NOT NULL";
            $results = Common::GetCol($sSql);

            if (empty($results)) {
                return false;
            }
        }
        return true;
    }

    /**
     *
     * @param array $aProductData
     * @param int $iNotId
     * @return bool
     */
    private function checkUniqueRef(&$aProductData, $iNotId = 0)
    {

        if (isset($aProductData['isbn_plain']) && $aProductData['isbn_plain'] != '') {
            if ($this->checkIndeksUnique($aProductData['isbn_plain'], $iNotId) === false) {
                return false;
            }
        }

        if (isset($aProductData['isbn_10']) && $aProductData['isbn_10'] != '') {
            if ($this->checkIndeksUnique($aProductData['isbn_10'], $iNotId) === false) {
                unset($aProductData['isbn_10']);
                return false;
            }
        }

        if (isset($aProductData['streamsoft_indeks']) && $aProductData['streamsoft_indeks'] != '') {
            if ($this->checkIndeksUnique($aProductData['streamsoft_indeks'], $iNotId) === false) {
                unset($aProductData['streamsoft_indeks']);
                return false;
            }
        }

        if (isset($aProductData['isbn_13']) && $aProductData['isbn_13'] != '') {
            if ($this->checkIndeksUnique($aProductData['isbn_13'], $iNotId) === false) {
                unset($aProductData['isbn_13']);
                return false;
            }
        }

        if (isset($aProductData['ean_13']) && $aProductData['ean_13'] != '') {
            if ($this->checkIndeksUnique($aProductData['ean_13'], $iNotId) === false) {
                unset($aProductData['ean_13']);
                return false;
            }
        }

        if (isset($aProductData['standard_code']) && $aProductData['standard_code'] != '') {
            if ($this->checkIndeksUnique($aProductData['standard_code'], $iNotId) === false) {
                unset($aProductData['standard_code']);
                return false;
            }

            if (
                $aProductData['standard_code'] == $aProductData['ean_13'] ||
                $aProductData['standard_code'] == $aProductData['isbn_13'] ||
                $aProductData['standard_code'] == $aProductData['streamsoft_indeks'] ||
                $aProductData['standard_code'] == $aProductData['isbn_10'] ||
                $aProductData['standard_code'] == $aProductData['isbn_plain']
            ) {
                // żaden identyfikator nie może powtarzać się z kodem standardu nawet w tym samym produkcie
               return false;
            }
        }

        return true;
    }

    /**
     *
     * @global DatabaseManager $pDbMgr
     * @param string $sIndeks
     * @param int $iNotId
     * @return int
     */
    private function checkIndeksUnique($sIndeks, $iNotId = 0)
    {
        global $pDbMgr;

        $sSqlSelect = 'SELECT id
               FROM products
               WHERE 
                (
                  isbn_plain = "%1$s" OR 
                  isbn_10 = "%1$s" OR 
                  isbn_13 = "%1$s" OR 
                  ean_13 = "%1$s" OR
                  streamsoft_indeks = "%1$s" OR
                  standard_code = "%1$s"
                )
                ';
        $sSqlSelect = sprintf($sSqlSelect, $sIndeks);
        $aIdks = $pDbMgr->GetCol('profit24', $sSqlSelect);
        if ($iNotId > 0) {
            if (($key = array_search($iNotId, $aIdks)) !== false) {
                unset($aIdks[$key]);
            }
        }
        if (!empty($aIdks)) {
            return false;
        }
        return true;
    }

    /**
     * Metoda pobiera formularz rezerwacji
     *
     * @global DatabaseManager $pDbMgr
     * @param FormTable $pForm
     * @param int $iId
     * @return FormTable
     */
    private function getReservationsForm($pForm, $iId)
    {
        global $pDbMgr;


        if ($_SESSION['user']['priv_order_status'] == '1' || $_SESSION['user']['type'] == '1') {
            $bPrivilagesRemoveReservations = true;
        } else {
            $bPrivilagesRemoveReservations = false;
        }

        $sSql = 'SELECT PSR.*, O.order_number 
             FROM products_stock_reservations AS PSR
             JOIN orders AS O
              ON O.id = PSR.order_id
             WHERE products_stock_id = ' . $iId;
        $aReservations = $pDbMgr->GetAll('profit24', $sSql);

        if (!empty($aReservations)) {
            $pForm->AddMergedRow(_('Rezerwacje na towar'), array('class' => 'merged'));
            foreach ($aReservations as $iKey => $aReservation) {
                if ($bPrivilagesRemoveReservations === true) {
                    $sButtonRemove = $pForm->GetInputButtonHTML('delete_item_reservation', _('Usuń rezerwację'),
                        array('onclick' =>
                            'MenuNavigate(\'' . phpSelf(array('id' => $iId, 'do' => 'delete_item_reservation', 'item_reservation' => $aReservation['id'])) . '\');'), 'button');
                }

                $pForm->AddRow('Rezerwacja ' . ($iKey + 1), ' Zamówienie: <strong>' . $aReservation['order_number'] . '</strong> na ilość: <strong>' . $aReservation['quantity'] . '</strong> &nbsp; &nbsp; ' . $sButtonRemove);
            }
        }

        return $pForm;
    }// end of getReservationsForm() method

    /**
     * Metoda pobiera zewnętrznych dostawców
     *
     * @return array
     */
    function _getSource($iId)
    {
        global $aConfig;

        // pobranie z bazy danych edytowanego rekordu
        $sSql = "SELECT id AS value, symbol, symbol AS label
             FROM " . $aConfig['tabls']['prefix'] . "sources
             WHERE id = " . $iId;
        $aSources = Common::GetRow($sSql);
        return $aSources;
    }// end of _getSources() method

    /**
     * Metoda pobiera zewnętrznych dostawców
     *
     * @return array
     */
    function _getSources()
    {
        global $aConfig;

        // pobranie z bazy danych edytowanego rekordu
        $sSql = "SELECT id AS value, symbol, symbol as label
             FROM " . $aConfig['tabls']['prefix'] . "sources";
        $aSources = Common::GetAll($sSql);
        return $aSources;
    }// end of _getSources() method


    /**
     * Metoda pobiera powiązane źródła produktu
     *
     * @global array $aConfig
     * @param integer $iId
     * @return array
     */
    function getProductSources($iId)
    {
        global $aConfig;

        $sSql = "SELECT B.symbol, A.source_index 
						 FROM " . $aConfig['tabls']['prefix'] . "products_to_sources AS A
						 JOIN " . $aConfig['tabls']['prefix'] . "sources AS B
							 ON A.source_id = B.id
						 WHERE A.product_id = " . $iId;
        return Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, true);
    }// end of getProductSources() method


    function TagAllForm(&$pSmarty)
    {
        global $aConfig;

        $aData = array();
        $sHtml = '';
        if (empty($_POST['delete'])) {
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_products_empty'], true);
            // dodanie informacji do logow
            AddLog($sMsg, true);
            $this->Show($pSmarty);
            return;
        }

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
//		$pSmarty->assign('sBodyOnload', 'checkDiscounts(document.getElementById(\'discount\'), document.getElementById(\'exclude_discount\'), document.getElementById(\'users_discount\'), document.getElementById(\'total_value_discount\'));');

        $sNames = '';
        foreach ($_POST['delete'] as $sKey => $sVal) {
            $sNames .= $this->getItemName($sKey) . ', ';
        }
        $sNames = substr($sNames, 0, -2);


        if (!empty($_POST)) {
            $aData = &$_POST;
        }

        $sHeader = $aConfig['lang'][$this->sModule]['header_tags'] . ' "' . $sNames . '"';


        $pForm = new FormTable('products_tags', $sHeader, array('action' => phpSelf(array('id' => $iId))), array('col_width' => 155), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', 'update_tags');
        if ($iId > 0) {
            $pForm->AddHidden('old_published', $aData['old_published']);
            $pForm->AddHidden('curr_page_id', $aData['curr_page_id']);
        }
        foreach ($_POST['delete'] as $sKey => $sVal) {
            $pForm->AddHidden('products[' . $sKey . ']', '1');
        }

        // tagi
        $pForm->AddRow($aConfig['lang'][$this->sModule]['new_tag'], $pForm->GetTextHtml('new_tag', $aConfig['lang'][$this->sModule]['new_tag'], $aData['new_tag'], array('id' => 'tags_aci', 'style' => 'width:300px;', 'maxlength' => 64), '', 'text', false) . '&nbsp;&nbsp;' .
            $pForm->GetInputButtonHTML('add_tag', $aConfig['lang'][$this->sModule]['add_tag_button'], array('id' => 'add_tag_button'), 'button'));
        $pForm->AddTextArea('tags', $aConfig['lang'][$this->sModule]['tags'], $aData['tags'], array('id' => 'tags_list', 'rows' => '8', 'style' => 'width: 450px;'), 512, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);

        $pForm->AddSelect('page_id', $aConfig['lang'][$this->sModule]['main_category'], array('style' => 'width:300px;'), $this->getTopProductsCategories(true), $aData['page_id']);

        // przyciski
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_' . ($iId > 0 ? 1 : 0)], array()) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));

        // funkcja JS - setStatus
        $sJS = '
		<script type="text/javascript">
		
		$(document).ready(function(){
      $("#tags_aci").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetTags.php",
                select: function( event, ui ) {
                  $("#add_tag_button").focus();
                }
      });
      
    	$("#add_tag_button").click(function () {
    		var sCurTags = $("#tags_list").val();
    		var sNewTag = $("#tags_aci").val();
    		if(sNewTag != ""){
    			if(sCurTags != ""){
    				sCurTags = sCurTags+",";
					}
      		$("#tags_list").val(sCurTags+sNewTag);
      		$("#tags_aci").val("");
      	}
   		});
    	
		});
		</script>
		';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sJS . ShowTable($pForm->ShowForm()));
    }

// end of TagAllForm() function

    function ChangeStatusForm(&$pSmarty)
    {
        global $aConfig;

        $aData = array();
        $sHtml = '';
        if (empty($_POST['delete'])) {
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['statuses_products_empty'], true);
            // dodanie informacji do logow
            AddLog($sMsg, true);
            $this->Show($pSmarty);
            return;
        }

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
//		$pSmarty->assign('sBodyOnload', 'checkDiscounts(document.getElementById(\'discount\'), document.getElementById(\'exclude_discount\'), document.getElementById(\'users_discount\'), document.getElementById(\'total_value_discount\'));');

        $sNames = '';
        foreach ($_POST['delete'] as $sKey => $sVal) {
            $sNames .= $this->getItemName($sKey) . ', ';
        }
        $sNames = substr($sNames, 0, -2);


        if (!empty($_POST)) {
            $aData = &$_POST;
        }

        $sHeader = $aConfig['lang'][$this->sModule]['header_status'] . ' "' . $sNames . '"';


        $pForm = new FormTable('products_status', $sHeader, array('action' => phpSelf(array('id' => $iId))), array('col_width' => 155), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', 'update_status');

        foreach ($_POST['delete'] as $sKey => $sVal) {
            $pForm->AddHidden('products[' . $sKey . ']', '1');
        }

        // status
        $aStatuses = array(
            array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1']),
            array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['status_0']),
            array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['status_2'])
        );
        $pForm->AddRadioSet('prod_status', $aConfig['lang'][$this->sModule]['prod_status'], $aStatuses, '', '', true, false);

        // przyciski
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_' . ($iId > 0 ? 1 : 0)], array()) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button'));

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
    }

// end of ChangeStatusForm() function
    /**
     * Metoda pobiera dostępne opisy statusów produktu
     * @return unknown_type
     */

    function &getStatusesDescriptions()
    {
        global $aConfig;
        $aStatuses = array();

        $sSql = "SELECT status_text
						 FROM " . $aConfig['tabls']['prefix'] . "products_statuses
						 ORDER BY id";
        $aItems = &Common::GetCol($sSql);
        if (empty($aItems))
            return $aStatuses;

        $aStatuses = array(
            array('value' => '', 'label' => $aConfig['lang']['common']['choose'])
        );
        foreach ($aItems as $sStatus) {
            $aStatuses[] = array(
                'value' => Common::encodeString(br2nl($sStatus)), 'label' => trimString(br2nl($sStatus, " "), 75)
            );
        }
        return $aStatuses;
    }

// end of getStatusesDescriptions() method

    /**
     * Metoda wprowadza zmiany w zdjeciach akapitu do bazy danych
     *
     * @param        object $pSmarty
     * @return    void
     */
    function UpdateImages(&$pSmarty, $iPId, $iId)
    {
        global $aConfig;

        if (!$this->ProceedImages($pSmarty, $iPId, $iId)) {
            // wyswietlenie komunikatu o niepowodzeniu
            // wycofanie transakcji
            // oraz ponowne wyswietlenie formularza edycji
            Common::RollbackTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['image_err_0'], getPagePath($iPId, $this->iLangId, true), $this->getItemName($iId));
            $this->sMsg = GetMessage($sMsg);
            AddLog($sMsg);
            $this->AddEditImages($pSmarty, $iPId, $iId);
        } else {
            SetModification('products', $iId);
            Common::CommitTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['image_ok'], getPagePath($iPId, $this->iLangId, true), $this->getItemName($iId));
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            $this->AddEditImages($pSmarty, $iPId, $iId);
        }
    }

// end of UpdateImages() funciton

    /**
     * Metoda wprowadza zmiany w zdjeciach produktu do bazy danych
     *
     * @param    object $pSmarty
     * @return    void
     */
    function SaveAndAddImage(&$pSmarty, $iPId, $iId)
    {
        global $aConfig;

        if (!$this->ProceedImages($pSmarty, $iPId, $iId)) {
            // wyswietlenie komunikatu o niepowodzeniu
            // wycofanie transakcji
            // oraz ponowne wyswietlenie formularza edycji
            Common::RollbackTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['image_err_0'], getPagePath($iPId, $this->iLangId, true), $this->getItemName($iId));
            $this->sMsg = GetMessage($sMsg);
            AddLog($sMsg);
            $this->AddEditImages($pSmarty, $iPId, $iId);
        } else {
            SetModification('products', $iId);
            Common::CommitTransaction();
            $this->AddEditImages($pSmarty, $iPId, $iId, true);
        }
    }

// end of SaveAndAddImage() funciton

    /**
     * Metoda wykonuje operacje na zdjeciach akapitu
     *
     * @param    object $pSmarty
     * @param    integer $iPId
     * @param    integer $iId
     *
     * @return    integer
     */
    function ProceedImages(&$pSmarty, $iPId, $iId)
    {

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty, $iPId);
            return;
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEditImages($pSmarty, $iPId, $iId);
            return;
        }

        Common::BeginTransaction();
        // dolaczenie klasy Images
        include_once('ProductImages.class.php');
        $oImages = new ProductImages($iPId,
            $iId,
            $this->aSettings,
            $this->sDir,
            'product_id',
            'products_images');
        return $oImages->proceed(true, false);
    }

// end of ProceedImages() method

    /**
     * Metoda pobiera i zwraca stan publikacji produktu
     *
     * @param    integer $iId - Id produktu
     * @return    integer
     */
    function getItemPublishState($iId)
    {
        global $aConfig;

        $sSql = "SELECT published
						 FROM " . $aConfig['tabls']['prefix'] . "products
						 WHERE id = " . $iId;
        return intval(Common::GetOne($sSql));
    }

// end of getItemPublishState() method

    /**
     * Metoda zwraca liste produktow o przekazanych do niej Id
     *
     * @param    array $aIds - Id rekordow dla ktorych zwrocone zostana nazwy
     * @return    string    - ciag z nazwami
     */
    function getItemsNames($aIds)
    {
        global $aConfig;
        $sList = '';

        // pobranie nazw usuwanych rekordow
        $sSql = "SELECT name
				 		 FROM " . $aConfig['tabls']['prefix'] . "products
					 	 WHERE id IN  (" . implode(',', $aIds) . ")";
        $aRecords = &Common::GetAll($sSql);
        foreach ($aRecords as $aItem) {
            $sList .= '"' . $aItem['name'] . '", ';
        }
        $sList = substr($sList, 0, -2);
        return $sList;
    }

// end of getItemsNames() method

    /**
     * Metoda zwraca nazwe produktu o podanym id
     */
    function getItemName($iId)
    {
        global $aConfig;

        $sSql = "SELECT name FROM " . $aConfig['tabls']['prefix'] . "products
	 						WHERE id = " . $iId . "";

        return Common::GetOne($sSql);
    }

    /**
     * Metoda zwraca liste produktow do usuniecia na podstawie
     * przekazanych do niej Id
     *
     * @param    array $aIds - Id produktow
     * @return    array ref
     */
    function getItemsToDelete($aIds)
    {
        global $aConfig;
        $sList = '';

        // pobranie nazw usuwanych rekordow
        $sSql = "SELECT id, page_id, name
				 		 FROM " . $aConfig['tabls']['prefix'] . "products
					 	 WHERE id IN  (" . implode(',', $aIds) . ")";
        return Common::GetAll($sSql);
    }

// end of getItemsToDelete() method

    /**
     * Metoda usuwa produkt o podanym Id
     *
     * @param    integer $iId - Id produktu do usuniecia
     * @return    mixed
     */
    function deleteItem($iId)
    {
        global $aConfig;
        // pobranie obrazkow
        $aImages = &$this->getImages($iId);
        // usuwanie produktu
        $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products
							 WHERE id = " . $iId;
        if (($iRes = Common::Query($sSql)) === false) {
            return false;
        } else {
            // usuwanie zdjec i plikow produktu
            $this->deleteImages($aImages);
            $this->deleteShadow($iId);

            $this->deleteProductWebsites($iId, ['profit24']);

            return 1;
        }
        //}*/
        return 1;
    }// end of deleteItem() method

    /**
     *
     * @param array $aOmmitWebsites
     * @return array
     */
    private function getWebsites($aOmmitWebsites)
    {
        $sSql = 'SELECT code 
             FROM websites
             WHERE code NOT IN ("' . implode('", "', $aOmmitWebsites) . '")';
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

    /**
     *
     * @param int $iId
     * @param  $sWebsite
     */
    private function deleteProductFromSingleWebsite($iId, $sWebsite)
    {

        $sSql = 'SELECT id FROM products WHERE profit24_id = ' . $iId;
        $iWProductId = $this->pDbMgr->GetOne($sWebsite, $sSql);
        if ($iWProductId > 0) {
            $sSql = "DELETE FROM products
               WHERE id = " . $iWProductId;
            $this->pDbMgr->Query($sWebsite, $sSql);

            $sSql = "DELETE FROM products_shadow
                 WHERE id = " . $iWProductId;
            $this->pDbMgr->Query($sWebsite, $sSql);
        }
    }


    /**
     *
     * @param int $iId
     * @param array $aOmmitWebsites
     */
    private function deleteProductWebsites($iId, $aOmmitWebsites)
    {

        $aWebsites = $this->getWebsites($aOmmitWebsites);
        foreach ($aWebsites as $sWebsite) {
            $this->deleteProductFromSingleWebsite($iId, $sWebsite);
        }
    }

    /**
     * Metoda usuwa zdjecia produktu
     *
     * @param    integer $iId - Id produktu
     * @param    string $sExt - rozszerzenie miniaturki, sredniego i duzego zdjecia
     * @return    void
     */
    function deleteImages($aImages)
    {
        global $aConfig;

        if (!empty($aImages)) {
            foreach ($aImages as $aImg) {
                if (file_exists($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/' . $aImg['photo'])) {
                    @unlink($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/' . $aImg['photo']);
                }
                if (file_exists($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/__t_' . $aImg['photo'])) {
                    @unlink($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/__t_' . $aImg['photo']);
                }
                if (file_exists($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/__b_' . $aImg['photo'])) {
                    @unlink($aConfig['common']['client_base_path'] . $aConfig['common']['photo_dir'] . '/' . $aImg['directory'] . '/__b_' . $aImg['photo']);
                }
            }
        }
    }

// end of deleteImage() method

    /**
     * Metoda tworzy formularz dodawania / edycji zdjec akapitu
     *
     * @param        object $pSmarty
     * @param    integer $iPId - I strony akapitu
     * @param        integer $iId ID edytowanego akapitu
     * @param        bool $bAddPhoto - true: zwieksz liczbe zdjec
     */
    function AddEditImages(&$pSmarty, $iPId, $iId, $bAddPhoto = false)
    {
        global $aConfig;

        $aData = array();
        $aOrderBy = array();
        $sHtml = '';

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        // pobranie zdjec
        $sSql = "SELECT id, directory, photo, photo AS name, description, order_by, author
						 FROM " . $aConfig['tabls']['prefix'] . "products_images
						 WHERE product_id = " . $iId . "
					 	 ORDER BY order_by, id";
        $aImgs = &Common::GetAll($sSql);

        $sHeader = sprintf($aConfig['lang'][$this->sModule]['images_header'], $this->getItemName($iId));

        $pForm = new FormTable('images', $sHeader, array('action' => phpSelf(array('pid' => $iPId, 'id' => $iId)), 'enctype' => 'multipart/form-data'), array('col_width' => 135), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', 'update_images');


        // maksymalna suma rozmiarow przesylanych zdjec
        $pForm->AddRow($aConfig['lang']['common']['max_img_upload_size'], getMaxUploadSize(), '', array(), array('class' => 'redText'));

        // dopuszczalne formaty zdjec
        $pForm->AddRow($aConfig['lang']['common']['img_available_exts'], implode(', ', $aConfig['common']['img_extensions']), '', array(), array('class' => 'redText'));

        $iImages = 2;
        /* if (!empty($_POST['desc_image'])) {
		  $iImages = count($_POST['desc_image']);
		  }
		  elseif (!empty($aImgs)) {
		  $iImages = count($aImgs) + 1;
		  }
		  else {
		  $iImages = $aConfig['default']['photos_no'];
		  }
		  if ($iImages < $aConfig['default']['photos_no']) {
		  $iImages = $aConfig['default']['photos_no'];
		  }
		  if ($bAddPhoto) {
		  // wybranmo dodanie kolejnego zdjecia
		  $iImages += $aConfig['default']['photos_no'];
		  } */
        // przygotwanie tablicy do ustalania kolejnosci wyswietlania zdjec
        // dolaczenie klasy Images
        include_once('ProductImages.class.php');
        $oImages = new ProductImages($iPId,
            $iId,
            $this->aSettings,
            $this->sDir,
            'product_id',
            'products_images');
        $aOrderBy = &$oImages->getOrderBy(count($aImgs));

        for ($i = 0; $i < $iImages; $i++) {
            if (isset($aImgs[$i])) {
                $pForm->AddImage('image[' . $i . '_' . $aImgs[$i]['id'] . ']', $aConfig['lang'][$this->sModule]['image'] . ' ' . ($i + 1), $aImgs[$i], $aOrderBy);
            } else {
                $pForm->AddImage('image[' . $i . '_0]', $aConfig['lang'][$this->sModule]['image'] . ' ' . ($i + 1));
            }
        }
        //$pForm->AddRow('&nbsp;', '<div class="existingPhoto">'.$pForm->GetInputButtonHTML('add_photo', $aConfig['lang']['common']['button_add_photo'], array('onclick'=>'GetObject(\'do\').value=\'add_photo\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'</div>');


        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show'), array('id')) . '\');'), 'button'));

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
    }

// end of AddEditImages() function


    /**
     * Metoda pobiera i zwraca produkt o podanym Id
     *
     * @param    integer $iId - Id produktu
     * @param array $aCols
     * @return    array ref
     */
    private function &getProductCols($iId, $aCols)
    {
        global $aConfig;

        $sSql = "SELECT " . implode($aCols, ', ') . "
						 FROM " . $aConfig['tabls']['prefix'] . "products
						 WHERE id = " . $iId;
        return Common::GetRow($sSql);
    }// end of getProductCols() method


    /**
     * Metoda pobiera i zwraca produkt o podanym Id
     *
     * @param    integer $iId - Id produktu
     * @return    array ref
     */
    function &getItem($iId)
    {
        global $aConfig;

        $sSql = "SELECT *
						 FROM " . $aConfig['tabls']['prefix'] . "products
						 WHERE id = " . $iId;
        return Common::GetRow($sSql);
    }

// end of getItem() method

    function &getPublishers()
    {
        global $aConfig;
        // pobranie wszystkich serii
        $sSql = "SELECT id AS value, name AS label
							 FROM " . $aConfig['tabls']['prefix'] . "products_publishers
							 ORDER BY name";
        $aItems = Common::GetAll($sSql);
        foreach ($aItems as $iKey => $aItem) {
            $aItems[$iKey]['label'] = substr($aItem['label'], 0, 36);
        }
        return array_merge(array(
            array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])
        ), $aItems);
    }

    function getImages($iId)
    {
        global $aConfig;

        $sSql = "SELECT *
						 FROM " . $aConfig['tabls']['prefix'] . "products_images
						 WHERE product_id = " . $iId;
        return Common::GetAll($sSql);
    }

    /**
     * Funkcja tworzaca na podstawie przekazanego do niej Id strony
     * jej pelna sciezke od korzenia do niej
     *
     * @param    integer $iId - ID strony do ktorej ma byc tworzona sciezka
     * @param    integer $iLangId - Id wersji jezykowej
     * @return    array    - uporzadkowana tablica ze sciezka do strony
     */
    function getPathIDs($iId, $iLangId)
    {
        global $aConfig;
        $aPath = array();

        $sSql = "SELECT A.id, IFNULL(A.parent_id, 0) AS parent_id
  					 FROM " . $aConfig['tabls']['prefix'] . "menus_items AS A
  					 WHERE A.id = " . $iId . " AND
  					 			 A.language_id = " . $iLangId;
        $aItem = &Common::GetRow($sSql);

        while (intval($aItem['parent_id']) > 0) {
            $sSql = "SELECT id, IFNULL(parent_id, 0) AS parent_id
  					 FROM " . $aConfig['tabls']['prefix'] . "menus_items
  					 WHERE id = " . $aItem['parent_id'] . " AND
  					 			 language_id = " . $iLangId;
            $aItem = &Common::GetRow($sSql);
            $aPath[] = $aItem['id'];
        }
        return array_reverse($aPath);
    }

// end getPathItems() function

    function existExtraCategory($iCategory, $iProduct)
    {
        global $aConfig;
        $sSql = "SELECT count(*)
					 FROM " . $aConfig['tabls']['prefix'] . "products_extra_categories
					 WHERE page_id = " . $iCategory . " AND product_id = " . $iProduct;
        return (Common::GetOne($sSql) > 0);
    }

    function existBookWithISBN($sISBN, $iId = 0)
    {
        global $aConfig;

        $sSql = "SELECT count(id)
						 FROM " . $aConfig['tabls']['prefix'] . "products
						 WHERE isbn_plain = '" . $sISBN . "'" .
            ($iId > 0 ? ' AND id <> ' . $iId : '');
        return (Common::GetOne($sSql) > 0);
    }

// end of existBookWithISBN() function

    function GetNaviTreeElem($sName, $aOptions = array())
    {
        global $aConfig;
        $sHtml = '';
        if (!empty($aOptions)) {
            foreach ($aOptions as $mKey => $aOAttribs) {
                $sHtml .= '<li><a href="javascript:void(0)" onclick="NaviSetFilterValue(' . $aOAttribs['value'] . ')">' . $aOAttribs['label'] . '</a>';

                if (isset($aOAttribs['items'])) {
                    $sHtml .= '<ul>';
                    $sHtml .= $this->GetNaviTreeElem($sName, $aOAttribs['items']);
                    $sHtml .= '</ul>';
                }
                $sHtml .= '</li>';
            }
        }
        return $sHtml;
    }

    function GetNaviTree()
    {
        global $aConfig;

        $sHtml = '
		<script>
		
			$(document).ready(function(){
				$("#show_navitree").click(function() {
					$("#navi").dialog(\'open\');
					window.open("ajax/NaviPopup.php",\'\',\'menubar=0,location=0,scrollbars=1,toolbar=0,width=500,height=300\');
				});
			});
		</script>';
//		$aOptions = $this->getProductsCategoriesChTree(Common::getProductsMenu($this->iLangId));
//		$sHtml .= '<div id="navi"><div style="width:378px; height:355px; overflow:auto;">';
//		$sHtml .= '<ul id="categories_navi">';
//		$sHtml .= $this->GetNaviTreeElem($sName, $aOptions, $mSelected);
//
//		$sHtml .= '</ul></div></div>';
        //	dump($sHtml);
        return $sHtml;
    }

// end of GetNaviTree() method

    /**
     * Metoda tworzy formularz tworzenia / edycji pakietu
     *
     * @param    object $pSmarty
     * @param    integer $iId - Id pakietu
     * @param    bool $bAddProduct - true: zwieksz liczbe pol
     */
    function AddEditPacket(&$pSmarty, $iId = 0, $bAddProduct = false)
    {
        global $aConfig;

        $aData = array();
        $aIdAttribs = array('maxlength' => 10, 'style' => 'width: 50px;');
        $aLang = &$aConfig['lang'][$this->sModule];
        $sHtml = '';

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        $aExtraCat = $this->getProductsCategories(Common::getProductsMenu($this->iLangId), '');
        $aCats = array();
        $aData['legal_status_date'] = '00-00-0000';
        $aData['shipment_date'] = '00-00-0000';
        if ($iId > 0) {
            // pobranie z bazy danych edytowanego rekordu
            $sSql = "SELECT A.*, A.name AS bookfullname, A.published old_published, A.page_id curr_page_id, 
								DATE_FORMAT(legal_status_date, '" . $aConfig['common']['cal_sql_date_format'] . "')
											AS legal_status_date, 
								DATE_FORMAT(shipment_date, '" . $aConfig['common']['cal_sql_date_format'] . "')
								AS shipment_date,	B.name AS publisher
							 FROM " . $aConfig['tabls']['prefix'] . "products A
							 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers B
							 ON A.publisher_id=B.id
							 WHERE A.id = " . $iId;
            $aData = &Common::GetRow($sSql);
            $sName = $aData['plain_name'];
            if (empty($aData['legal_status_date'])) {
                $aData['legal_status_date'] = '00-00-0000';
            }
            if (empty($aData['shipment_date'])) {
                $aData['shipment_date'] = '00-00-0000';
            }
            $sName = $aData['plain_name'];
            $aData['series'] = $this->getProductSeries($iId);
            $aData['news_status'] = $this->getNewsStatus($iId);
            $aData['previews_status'] = $this->getPreviewsStatus($iId);
            $aCats = $this->getExtraCategories($iId);
            // produkty pakietu
            $this->getPacketProducts($iId, $aData);
            // tagi
            $aData['tags'] = $this->getProductTags($iId);
        }
        if (!empty($_POST)) {
            $aData = &$_POST;
            if (isset($aData['extra_categories'])) {
                $aCats = $aData['extra_categories'];
            }
        }


        $sCurrentExtraCats = '';
        foreach ($aExtraCat as $aCat) {
            if (in_array($aCat['value'], $aCats)) {
                $sCurrentExtraCats .= getPagePath($aCat['value'], $this->iLangId, true, false, false) . "<br />\n";
            }
        }

        if ($iId > 0) {
            $sHeader = sprintf($aLang['packet_header_1'], trimString($sName, 50), getPagePath((double)$aData['curr_page_id'], $this->iLangId, true));
        } else {
            $sHeader = $aLang['packet_header_0'];
        }

        $pForm = new FormTable('products', $sHeader, array('action' => phpSelf(array('id' => $iId))), array('col_width' => 155), $aConfig['common']['js_validation']);
        if ($iId > 0) {
            $pForm->AddHidden('do', 'update_packet');
            $pForm->AddHidden('curr_page_id', $aData['curr_page_id']);
            $pForm->AddHidden('old_published', $aData['old_published']);
            // atrybuty pola Id - wylaczona mozliwosc edycji
            $aIdAttribs['readonly'] = 'readonly';
        } else {
            $pForm->AddHidden('do', 'insert_packet');
        }

        $pForm->AddSelect('page_id', $aConfig['lang'][$this->sModule]['main_category'], array('style' => 'width:300px;'), $this->getTopProductsCategories(), $aData['page_id']);

        // obecne dodatkowe kategorie
        if (!empty($sCurrentExtraCats)) {
            $pForm->AddRow($aConfig['lang'][$this->sModule]['current_extra_categories'], $sCurrentExtraCats);
        }

        // drzewo kategorii
        $pForm->AddRow($aConfig['lang'][$this->sModule]['extra_categories'], '<div style="height:300px; overflow:auto;">' .
            Form::GetCheckTree('extra_categories[]', array(), $this->getProductsCategoriesChTree(Common::getProductsMenu($this->iLangId)), $aCats)
            . '</div>'
        );
        $pForm->AddRadioSet('news_status', $aConfig['lang'][$this->sModule]['news_status'], array_merge(array(array('value' => 0, 'label' => $aConfig['lang'][$this->sModule]['not_news'])), getModulePages('m_powiazane_oferta_produktowa', $this->iLangId, false, '', 'nowosci')), $aData['news_status'], '', true, false);
        // nazwa
        $pForm->AddText('name', $aLang['name'], $aData['bookfullname'], array('maxlength' => 500, 'style' => 'width: 450px;'), '', 'text');
        $pForm->AddText('name2', $aConfig['lang'][$this->sModule]['name2'], $aData['name2'], array('maxlength' => 500, 'style' => 'width: 450px;'), '', 'text', false);
        // wydawnictwo
        //$pForm->AddSelect('publisher_id',$aConfig['lang'][$this->sModule]['publisher'], array('onchange'=>'document.getElementById(\'do\').value =\'add\'; selectExtraCategories(); this.form.submit();'), $this->getPublishers(), $aData['publisher_id']);
        $pForm->AddText('publisher', $aConfig['lang'][$this->sModule]['publisher'], $aData['publisher_id'] > 0 ? Common::GetOne('SELECT name FROM ' . $aConfig['tabls']['prefix'] . 'products_publishers WHERE id=' . $aData['publisher_id']) : '', array('id' => 'publisher_aci', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', false);


        // seria
        $pForm->AddRow($aConfig['lang'][$this->sModule]['series'], $pForm->GetSelectHtml('series', $aConfig['lang'][$this->sModule]['series'], array('style' => 'width:300px;'), (!empty($aData['publisher_id']) ? $this->getSeriesList($aData['publisher_id']) : array()), (!empty($aData['publisher_id']) ? $this->getProductSeries($iId) : ''), '', false) .
            '&nbsp;&nbsp;&nbsp;' . $aConfig['lang'][$this->sModule]['add_series'] . '&nbsp;&nbsp;&nbsp;' .
            $pForm->GetTextHTML('series_name', $aConfig['lang'][$this->sModule]['series'], '', array('style' => 'width:200px;', 'maxlength' => 255), '', 'text', false));


        //rok
        $pForm->AddSelect('publication_year', $aConfig['lang'][$this->sModule]['fake_publication_year'], array(), $this->getYears(), $aData['publication_year'], '', true);
        //pokazuj ukryty rok publikacji
        $pForm->AddCheckBox('show_packet_year', $aConfig['lang'][$this->sModule]['show_packet_year'], array(), '', $aData['show_packet_year'] ? true : false, false);

        // tagi
        //$pForm->AddTextArea('tags', $aConfig['lang'][$this->sModule]['tags'], $aData['tags'], array('rows'=>'8', 'style'=>'width: 450px;'), 1024, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);
        // tagi
        $pForm->AddRow($aConfig['lang'][$this->sModule]['new_tag'], $pForm->GetTextHtml('new_tag', $aConfig['lang'][$this->sModule]['new_tag'], $aData['new_tag'], array('id' => 'tags_aci', 'style' => 'width:300px;', 'maxlength' => 64), '', 'text', false) . '&nbsp;&nbsp;' .
            $pForm->GetInputButtonHTML('add_tag', $aConfig['lang'][$this->sModule]['add_tag_button'], array('id' => 'add_tag_button'), 'button'));
        $pForm->AddTextArea('tags', $aConfig['lang'][$this->sModule]['tags'], $aData['tags'], array('id' => 'tags_list', 'rows' => '8', 'style' => 'width: 450px;'), 512, '', $aConfig['lang'][$this->sModule]['too_long_short_description'], false);


        // PRODUKTY WCHODZACE W SKLAD PAKIETU
        //$pForm->AddMergedRow($aLang['packet_products_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));

        $iProducts = 0;
        if (!empty($_POST['product'])) {
            $iProducts = count($_POST['product']);
        } elseif (!empty($aData['product'])) {
            $iProducts = count($aData['product']);
        } else {
            $iProducts = $this->iDefaultProductsNo;
        }
        if ($iProducts < $this->iDefaultProductsNo) {
            $iProducts = $this->iDefaultProductsNo;
        }
        if ($bAddProduct) {
            // wybranmo dodanie kolejnego produktu
            $iProducts += 1;
        }

        // poranie listy dostepnych w serwisie kategorii
        $aCategories = &$this->getProductsCategories(Common::getProductsMenu($this->iLangId), $aConfig['lang']['common']['choose']);

        $sJS = '
					<script>
						$(document).ready(function(){
						{
            
      $("#tags_aci").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetTags.php",
                select: function( event, ui ) {
                  $("#add_tag_button").focus();
                }
      });
      
      $("#publisher_aci").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetPublishers.php"
      });
      
      $("#city_aci").autocomplete({
                source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetCities.php"
      });

			$("#publisher_aci").bind("blur", function(e){
     	 $.get("ajax/GetSeries.php", { publisher: e.target.value },
					  function(data){
					    var brokenstring=data.split(";");
					    var elSelSeries = document.getElementById(\'series\');
					    elSelSeries.length = 0;
					    elSelSeries.options[elSelSeries.length] =  new Option("' . $aConfig['lang']['common']['choose'] . '","0");
					    for (x in brokenstring){
								if(brokenstring[x]){
									var item=brokenstring[x].split("|");
		  						elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
	  						}
	  					}
					 });
    	});
    	$("#add_tag_button").click(function () {
    		var sCurTags = $("#tags_list").val();
    		var sNewTag = $("#tags_aci").val();
    		if(sNewTag != ""){
    			if(sCurTags != ""){
    				sCurTags = sCurTags+",";
					}
      		$("#tags_list").val(sCurTags+sNewTag);
      		$("#tags_aci").val("");
      	}
   		});
    	
		}
						';
        /* for ($i = 0; $i < $iProducts; $i++) {
		  $bRequired = $i <= 1;
		  $iNo = ' '.($i + 1);

		  $pForm->AddMergedRow($aLang['product'].' '.($iNo), array('class'=>'mergedLight', 'style'=>'padding-left: 160px'));

		  // produkt
		  $pForm->AddText('product_name['.$i.']',$aConfig['lang'][$this->sModule]['product_name'],$aData['product_name'][$i],array('style'=>'width:300px;'),'','text',false);

		  //$pForm->AddText('product_isbn['.$i.']',$aConfig['lang'][$this->sModule]['product_isbn'],$aData['product_isbn'][$i],array('style'=>'width:300px;'),'','text',false);
		  $pForm->AddText('product_author['.$i.']',$aConfig['lang'][$this->sModule]['product_author'], $aData['product_author'][$i],array('style'=>'width:300px;','maxlength'=>255),'','text' ,false);
		  $pForm->AddText('product_publisher['.$i.']',$aConfig['lang'][$this->sModule]['product_publisher'], $aData['product_publisher'][$i],array('id'=>'publisher_aci_'.$i,'style'=>'width:300px;','maxlength'=>255),'','text' ,false);
		  $sJS.='
		  $("#publisher_aci_'.$i.'").autocomplete("ajax/GetPublishers.php",{ minChars: 2, max: 100, matchContains: true, cacheLength: 1, matchSubset: false });
		  ';

		  // przycisk wyszukaj
		  $pForm->AddRow('&nbsp;', '<div style="margin-left: 225px;">'.
		  $pForm->GetInputButtonHTML('search'.$i, $aConfig['lang'][$this->sModule]['button_search'],array('onclick'=>'document.getElementById(\'do\').value=\'add_product\'; this.form.submit();'),'button')
		  .'</div>');

		  // produkt
		  $pForm->AddSelect('product['.$i.']', $aConfig['lang'][$this->sModule]['product'], array('style'=>'width:300px;','onchange' => 'document.getElementById(\'do\').value=\'add_product\'; this.form.submit();'), $this->getProducts('packet',(($iId>0 && !empty($aData['product'][$i]) && empty($aData['product_name'][$i]))?$aData['product'][$i]:$aData['product_name'][$i]),$aData['product_author'][$i],$aData['product_publisher'][$i],true),$aData['product'][$i],'',$bRequired);

		  if (isset($aData['product'][$i]) && !empty($aData['product'][$i])) {
		  // dotychczasowa cena produktu
		  $fPrice = $this->getProductAttrib($aData['product'][$i], 'price_brutto');
		  $fPriceN = $this->getProductAttrib($aData['product'][$i], 'price_netto');
		  // VAT
		  $iVAT = $this->getProductAttrib($aData['product'][$i], 'vat');
		  $pForm->AddRow($aLang['price_brutto'], Common::formatPrice($fPrice).' '.$aConfig['lang']['common']['currency'], '', array(), array('class'=>'redText'));
		  // ukryte pole z cena
		  $pForm->AddHidden('price_netto['.$i.']', $fPriceN);
		  $pForm->AddHidden('price_brutto['.$i.']', $fPrice);
		  $pForm->AddHidden('vat['.$i.']', $iVAT);

		  // rabat w procentach
		  $pForm->AddText('discount['.$i.']', $aLang['discount'], isset($aData['discount'][$i]) && is_numeric($aData['discount'][$i]) ? Common::formatPrice3($aData['discount'][$i]) : '', array('maxlength'=>5, 'style'=>'width: 50px;'), $aLang['discount'].$iNo, 'ufloat', true);
		  }



		  /*********************************** */

        //}
        //$pForm->AddRow('&nbsp;', '<div class="existingPhoto">'.$pForm->GetInputButtonHTML('add_product_field', $aLang['button_add_product'], array('onclick'=>'document.getElementById(\'do\').value=\'add_product_field\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'</div>');
        // opis produktu
        $pForm->AddWYSIWYG('description', $aLang['description'], $aData['description']);
        //dodatkowy rabat
        $pForm->AddText('additional_discount', $aConfig['lang'][$this->sModule]['additional_discount'], Common::FormatPrice($aData['additional_discount']), array('maxlength' => 7, 'style' => 'width: 50px;'), '', 'text', true);
        // STATUS PRODUKTU
        $pForm->AddMergedRow($aLang['status_section'], array('class' => 'merged', 'style' => 'padding-left: 160px;'));

        // status
        // status
        $aStatuses = array(
            array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true;'),
            array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['status_0'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true;'),
            array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['status_2'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=true;'),
            array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['status_3'], 'onclick' => 'document.getElementById(\'shipment_date\').disabled=false;')
        );
        $pForm->AddRadioSet('prod_status', $aConfig['lang'][$this->sModule]['prod_status'], $aStatuses, $aData['prod_status'], '', true, false);

        if ($aData['prod_status'] == '3') {
            $aShipmentDateAttribs = array('style' => 'width: 75px;');
        } else {
            $aShipmentDateAttribs = array('style' => 'width: 75px;', 'disabled' => 'disabled');
        }
        $pForm->AddText('shipment_date', $aConfig['lang'][$this->sModule]['shipment_date'], $aData['shipment_date'], $aShipmentDateAttribs, '', 'date', false);

        // status
        $pShipmentTime = new ShipmentTime();

        $aShipments = array(
            array('value' => '0', 'label' => $pShipmentTime->getShipmentTime($iId, 0)),
            array('value' => '1', 'label' => $pShipmentTime->getShipmentTime($iId, 1)),
            array('value' => '2', 'label' => $pShipmentTime->getShipmentTime($iId, 2)),
            array('value' => '3', 'label' => $pShipmentTime->getShipmentTime($iId, 3)),
            array('value' => '4', 'label' => $pShipmentTime->getShipmentTime($iId, 4)),
            array('value' => '5', 'label' => $pShipmentTime->getShipmentTime($iId, 5)),
            array('value' => '6', 'label' => $pShipmentTime->getShipmentTime($iId, 6)),
            array('value' => '7', 'label' => $pShipmentTime->getShipmentTime($iId, 7)),
            array('value' => '8', 'label' => $pShipmentTime->getShipmentTime($iId, 8)),
            array('value' => '9', 'label' => $pShipmentTime->getShipmentTime($iId, 9)),
            array('value' => '10', 'label' => $pShipmentTime->getShipmentTime($iId, 10)),
        );
        $pForm->AddRadioSet('shipment_time', $aConfig['lang'][$this->sModule]['shipment_time'], $aShipments, $aData['shipment_time'], '', true, false);


        // STAN PUBLIKACJI
        $pForm->AddMergedRow($aConfig['lang'][$this->sModule]['publ_section'], array('class' => 'merged', 'style' => 'padding-left: 160px;'));

        // status publikacji

        $pForm->AddCheckBox('published', $aConfig['lang'][$this->sModule]['published'], array(), '', !empty($aData['published']), false);

        // status


        if (!empty($aData['modified'])) {
            $pForm->AddRow($aConfig['lang'][$this->sModule]['modified'], $aData['modified']);
        }

        if (!empty($aData['modified_by'])) {
            $pForm->AddRow($aConfig['lang'][$this->sModule]['modified_by'], $aData['modified_by']);
        }

        // w boksie na glownej stronie
        //$pForm->AddCheckBox('packet_in_box', $aConfig['lang'][$this->sModule]['packet_in_box'], array(), '', !empty($aData['packet_in_box']), false);
        // przyciski
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_' . ($iId > 0 ? 1 : 0)]) . '&nbsp;&nbsp;' . ($iId > 0 ? $pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['edit_book_list'], array('onclick' => 'window.location.href=\'' . phpSelf(array('do' => 'edit_packet2', 'id' => $iId)) . '\''), 'button') : '') . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array()) . '\');'), 'button'));

        $sJS .= '
			$("#add_tag_button").click(function () {
    		var sCurTags = $("#tags_list").val();
    		var sNewTag = $("#tags_aci").val();
    		if(sNewTag != ""){
    			if(sCurTags != ""){
    				sCurTags = sCurTags+",";
					}
      		$("#tags_list").val(sCurTags+sNewTag);
      		$("#tags_aci").val("");
      	}
   		});
						});
					
		
		
		
		</script>
		';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sJS . ShowTable($pForm->ShowForm()));
    }

// end of AddEditPacket() function

    /**
     * Metoda tworzy formularz edycji listy książek w pakiecie
     *
     * @param    object $pSmarty
     * @param    integer $iId - Id pakietu
     */
    function EditPacketBookList(&$pSmarty, $iId = 0)
    {
        global $aConfig;
        if ($iId) {

            $aData = array();
            $aIdAttribs = array('maxlength' => 10, 'style' => 'width: 50px;');
            $aLang = &$aConfig['lang'][$this->sModule];
            $sHtml = '';

            // dolaczenie klasy FormTable
            include_once('Form/FormTable.class.php');

            // dolaczenie klasy View
            include_once('View/EditableView.class.php');


            $aAttribs = array(
                'width' => '100%',
                'border' => 0,
                'cellspacing' => 0,
                'cellpadding' => 0,
                'class' => 'viewHeaderTable',
            );
            $aRecordsHeader = array(
                /* array(
					  'content'	=> '&nbsp;',
					  'sortable'	=> false
					  ), */
                array(
                    'db_field' => '.name',
                    'content' => $aConfig['lang'][$this->sModule]['list_name'],
                    'sortable' => false
                ),
                array(
                    'db_field' => 'base_price',
                    'content' => $aConfig['lang'][$this->sModule]['list_base_price'],
                    'sortable' => false,
                    'width' => '45'
                ),
                array(
                    'db_field' => 'price_brutto',
                    'content' => $aConfig['lang'][$this->sModule]['list_price_brutto'],
                    'sortable' => false,
                    'width' => '55'
                ),
                array(
                    'db_field' => 'discount',
                    'content' => $aConfig['lang'][$this->sModule]['discount'],
                    'sortable' => false,
                    'width' => '50'
                ),
                array(
                    'db_field' => 'discount2',
                    'content' => $aConfig['lang'][$this->sModule]['discount2'],
                    'sortable' => false,
                    'width' => '50'
                ),
                array(
                    'db_field' => 'discount3',
                    'content' => $aConfig['lang'][$this->sModule]['discount3'],
                    'sortable' => false,
                    'width' => '50'
                ),
                array(
                    'content' => $aConfig['lang']['common']['action'],
                    'sortable' => false,
                    'width' => '45'
                )
            );
            //pobranie domyslnego rabatu
            $sSql = 'SELECT additional_discount FROM ' . $aConfig['tabls']['prefix'] . 'products WHERE id=' . $iId;
            $fDefDiscount = Common::GetOne($sSql);

            $aExtraCat = $this->getProductsCategories(Common::getProductsMenu($this->iLangId), '');
            $aCats = array();
            $aData['legal_status_date'] = '00-00-0000';
            $aData['shipment_date'] = '00-00-0000';
            if ($iId > 0) {
                //dodanie nowego produktu do pakietu (pojedynczego)
                if (isset($_GET['aid']) && $_GET['aid'] > 0)
                    $this->addProductToPacket($iId, $_GET['aid']);

                // pobranie z bazy danych edytowanego rekordu
                $sSql = "SELECT A.plain_name, A.published old_published, A.page_id curr_page_id,
									DATE_FORMAT(legal_status_date, '" . $aConfig['common']['cal_sql_date_format'] . "')
												AS legal_status_date, 
									DATE_FORMAT(shipment_date, '" . $aConfig['common']['cal_sql_date_format'] . "')
									AS shipment_date,	B.name AS publisher
								 FROM " . $aConfig['tabls']['prefix'] . "products A
								 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers B
								 ON A.publisher_id=B.id
								 WHERE A.id = " . $iId;
                $aData = &Common::GetRow($sSql);
                $sName = $aData['plain_name'];
                if (empty($aData['legal_status_date'])) {
                    $aData['legal_status_date'] = '00-00-0000';
                }
                if (empty($aData['shipment_date'])) {
                    $aData['shipment_date'] = '00-00-0000';
                }
                $sName = $aData['plain_name'];
                $aData['series'] = $this->getProductSeries($iId);
                $aCats = $this->getExtraCategories($iId);
                // produkty pakietu
                $this->getPacketProducts($iId, $aData);
                // tagi
                $aData['tags'] = $this->getProductTags($iId);
            }
            /* if (!empty($_POST)) {
			  $aData2 =& $_POST;
			  if(isset($aData2['extra_categories'])){
			  $aCats=$aData2['extra_categories'];
			  }
			  } */

            //dump($aData);

            $fSumBase = 0;
            $fSumBrut = 0;


            $sCurrentExtraCats = '';
            foreach ($aExtraCat as $aCat) {
                if (in_array($aCat['value'], $aCats)) {
                    $sCurrentExtraCats .= getPagePath($aCat['value'], $this->iLangId, true, false, false) . "<br />\n";
                }
            }

            if ($iId > 0) {
                $sHeader = sprintf($aLang['packet_header_1'], trimString($sName, 50), getPagePath((double)$aData['curr_page_id'], $this->iLangId, true));
            } else {
                $sHeader = $aLang['packet_header_0'];
            }
            $aHeader = array(
                'form' => false,
                //'action' => phpSelf(array('action' => 'promotions', 'do' => 'books', 'id' => $iId)),
                //'refresh_link' => phpSelf(array('action' => 'promotions', 'do' => 'books', 'id' => $iId, 'reset' => '1')),
                'refresh' => false,
                'search' => false,
                //'action' => phpSelf(array('do'=>'books'), array(), false),
                'editable' => false,
                'second' => true,
                'checkboxes' => false
            );
            $pView = new EditableView('promotions_books', $aHeader, $aAttribs);
            $pView->AddRecordsHeader($aRecordsHeader);

            $pForm = new FormTable('products', $sHeader, array('action' => phpSelf(array('do' => 'edit_packet2', 'id' => $iId))), array('col_width' => 155), $aConfig['common']['js_validation']);

            $pForm->AddHidden('do', 'update_books_list');
            $pForm->AddHidden('saveAndBack', '0');
            $pForm->AddHidden('curr_page_id', $aData['curr_page_id']);
            $pForm->AddHidden('old_published', $aData['old_published']);
            $pForm->AddHidden('def_discount', $fDefDiscount);
            // atrybuty pola Id - wylaczona mozliwosc edycji
            $aIdAttribs['readonly'] = 'readonly';


            // PRODUKTY WCHODZACE W SKLAD PAKIETU
            //$pForm->AddMergedRow($aLang['packet_products_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));

            $iProducts = 0;
            if (!empty($_POST['product'])) {
                $iProducts = count($_POST['product']);
            } elseif (!empty($aData['product'])) {
                $iProducts = count($aData['product']);
            } else {
                $iProducts = $this->iDefaultProductsNo;
            }
            if ($iProducts < $this->iDefaultProductsNo) {
                $iProducts = $this->iDefaultProductsNo;
            }
            if ($bAddProduct) {
                // wybranmo dodanie kolejnego produktu
                $iProducts += 1;
            }

            // poranie listy dostepnych w serwisie kategorii
            $aCategories = &$this->getProductsCategories(Common::getProductsMenu($this->iLangId), $aConfig['lang']['common']['choose']);

            $sJS = '
						<script>
							$(document).ready(function(){
							';
            //dump($aData2);
            //dump($aData);
            $aItems = array();
            for ($i = 0; $i < $iProducts; $i++) {
                $aItems[$i]['id'] = $aData['product'][$i];
                //$aTarrif = $this->getTarrif($aData['product'][$i]);
                //$aData['price_brutto'][$i]=$aTarrif['price_brutto'];
                $aItems[$i]['name'] = '<b><a href="' . createProductLink($aData['product'][$i], $aData['name'][$i]) . '" target="_blank">' . $aData['name'][$i] . '</a></b>' . (!empty($aData['publisher'][$i]) ? '<br /><span style="font-weight: normal; font-style: italic;">' . mb_substr($aData['publisher'][$i], 0, 40, 'UTF-8') . '</span>' : '') .
                    '<br /><span style="font-weight: normal; font-style: italic;">' . mb_substr($this->getAuthors($aData['product'][$i]), 0, 40, 'UTF-8') . ' ' . $aData['year'][$i] . '</span>'; //$aData['name'][$i];
                $aItems[$i]['base_price'] = Common::formatPrice($aData['price_brutto'][$i]);
                $aItems[$i]['price_brutto'] = Common::formatPrice($aData['promo_price_brutto'][$i]); //Common::formatPrice($aData['price_brutto'][$i]-$aData['price_brutto'][$i]*($aData['discount'][$i]/100.0));
                $aItems[$i]['discount'] = Common::formatPrice($aData['discount'][$i]);
                $aItems[$i]['discount2'] = Common::formatPrice($fDefDiscount);
                $aItems[$i]['discount3'] = Common::formatPrice($aData['discount'][$i] + $fDefDiscount);
                $fSumBase += Common::formatPrice2($aItems[$i]['base_price']);
                $fSumBrut += Common::formatPrice2($aItems[$i]['price_brutto']);
                /* $bRequired = $i <= 1;
				  $iNo = ' '.($i + 1);

				  $pForm->AddMergedRow($aLang['product'].' '.($iNo), array('class'=>'mergedLight', 'style'=>'padding-left: 160px'));

				  // produkt
				  $pForm->AddText('product_name['.$i.']',$aConfig['lang'][$this->sModule]['product_name'],$aData['product_name'][$i],array('style'=>'width:300px;'),'','text',false);

				  //$pForm->AddText('product_isbn['.$i.']',$aConfig['lang'][$this->sModule]['product_isbn'],$aData['product_isbn'][$i],array('style'=>'width:300px;'),'','text',false);
				  $pForm->AddText('product_author['.$i.']',$aConfig['lang'][$this->sModule]['product_author'], $aData['product_author'][$i],array('style'=>'width:300px;','maxlength'=>255),'','text' ,false);
				  $pForm->AddText('product_publisher['.$i.']',$aConfig['lang'][$this->sModule]['product_publisher'], $aData['product_publisher'][$i],array('id'=>'publisher_aci_'.$i,'style'=>'width:300px;','maxlength'=>255),'','text' ,false);
				  $sJS.='
				  $("#publisher_aci_'.$i.'").autocomplete("ajax/GetPublishers.php",{ minChars: 2, max: 100, matchContains: true, cacheLength: 1, matchSubset: false });
				  ';

				  // przycisk wyszukaj
				  $pForm->AddRow('&nbsp;', '<div style="margin-left: 225px;">'.
				  $pForm->GetInputButtonHTML('search'.$i, $aConfig['lang'][$this->sModule]['button_search'],array('onclick'=>'document.getElementById(\'do\').value=\'add_product\'; this.form.submit();'),'button')
				  .'</div>');

				  // produkt
				  $pForm->AddSelect('product['.$i.']', $aConfig['lang'][$this->sModule]['product'], array('style'=>'width:300px;','onchange' => 'document.getElementById(\'do\').value=\'add_product\'; this.form.submit();'), $this->getProducts('packet',(($iId>0 && !empty($aData['product'][$i]) && empty($aData['product_name'][$i]))?$aData['product'][$i]:$aData['product_name'][$i]),$aData['product_author'][$i],$aData['product_publisher'][$i],true),$aData['product'][$i],'',$bRequired);

				  if (isset($aData['product'][$i]) && !empty($aData['product'][$i])) {
				  // dotychczasowa cena produktu
				  $fPrice = $this->getProductAttrib($aData['product'][$i], 'price_brutto');
				  $fPriceN = $this->getProductAttrib($aData['product'][$i], 'price_netto');
				  // VAT
				  $iVAT = $this->getProductAttrib($aData['product'][$i], 'vat');
				  $pForm->AddRow($aLang['price_brutto'], Common::formatPrice($fPrice).' '.$aConfig['lang']['common']['currency'], '', array(), array('class'=>'redText'));
				  // ukryte pole z cena
				  $pForm->AddHidden('price_netto['.$i.']', $fPriceN);
				  $pForm->AddHidden('price_brutto['.$i.']', $fPrice);
				  $pForm->AddHidden('vat['.$i.']', $iVAT);

				  // rabat w procentach
				  $pForm->AddText('discount['.$i.']', $aLang['discount'], isset($aData['discount'][$i]) && is_numeric($aData['discount'][$i]) ? Common::formatPrice3($aData['discount'][$i]) : '', array('maxlength'=>5, 'style'=>'width: 50px;'), $aLang['discount'].$iNo, 'ufloat', true);
				  }

				 */

                /*				 * ********************************* */
            }

            $aItems[$i]['id'] = 0;
            $aItems[$i]['name'] = 'SUMA:';
            $aItems[$i]['base_price'] = Common::formatPrice($fSumBase);
            $aItems[$i]['price_brutto'] = Common::formatPrice($fSumBrut);
            $aItems[$i]['base_price'] = Common::formatPrice($fSumBase);
            $aItems[$i]['discount'] = '';
            $aItems[$i]['discount2'] = '';
            $aItems[$i]['discount3'] = '';
            $aItems[$i]['invisible'] = array('delete');

            //dump($aItems);
            //$pForm->AddRow('&nbsp;', '<div class="existingPhoto">'.$pForm->GetInputButtonHTML('add_product_field', $aLang['button_add_product'], array('onclick'=>'document.getElementById(\'do\').value=\'add_product_field\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'</div>');
            // w boksie na glownej stronie
            //$pForm->AddCheckBox('packet_in_box', $aConfig['lang'][$this->sModule]['packet_in_box'], array(), '', !empty($aData['packet_in_box']), false);
            // przyciski
            //$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['add'], array('id'=>'add_to_packet'), 'button').'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array()).'\');'), 'button'));
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'discount' => array(
                    'editable' => true
                ),
                'action' => array(
                    'actions' => array('delete'),
                    'params' => array(
                        'delete' => array('do' => 'removeFromPacket', 'id' => $iId, 'pid' => '{id}')
                    ),
                    'show' => false
                )
            );
            // ustawienia dla kolumn edytowalnych
            $aEditableSettings = array(
                'discount' => array(
                    'type' => 'text'
                )
            );
            // dodanie rekordow do widoku
            $pView->AddRecords($aItems, $aColSettings, $aEditableSettings);

            $sJS .= '

			$(document).ready(function(){
					$("#add_to_packet").click(function() {
					window.open("ajax/SearchProduct.php?ppid=' . $iPId . '&pid=' . $iId . '&site=profit24&mode=1","","menubar=0,location=0,scrollbars=1,resizable=1,toolbar=0,width=700,height=500");
					});

					$("#order_items").submit(function() {
					var sErr = "";
					$(".heditable_input, .editable_input").each(function(){
						regex = /^editable\[([A-Za-z0-9_]+)\]\[([A-Za-z0-9_]+)\]$/;
						matches = regex.exec(this.name);
						if(matches[2] == "discount"){
							$(this).val($(this).val().replace(/&nbsp;/, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
							if ($(this).val() != "" && (!$(this).val().match(/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) || $(this).val() < 0 || $(this).val() >= 100)) {
								sErr += "\t- Rabat\n";
								}
							}
						});
					if(sErr != ""){
						alert("' . $aConfig['lang']['form']['error_prefix'] . '\n"+sErr+"' . $aConfig['lang']['form']['error_postfix'] . '");
						return false;
						}
						});
					});

				$("#add_tag_button").click(function () {
					var sCurTags = $("#tags_list").val();
					var sNewTag = $("#tags_aci").val();
					if(sNewTag != ""){
						if(sCurTags != ""){
							sCurTags = sCurTags+",";
						}
						$("#tags_list").val(sCurTags+sNewTag);
						$("#tags_aci").val("");
					}
				});
							});

						</script>';
            $pForm->AddMergedRow($sJS . $pView->Show() . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'edit_packet', 'id' => $iId)) . '\');'), 'button') . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['add'], array('id' => 'add_to_packet'), 'button'));
            //$pForm->AddMergedRow($sJS . $pView->Show() . $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('button', $aConfig['lang'][$this->sModule]['save_and_back'], array('onclick' => 'document.getElementById(\'saveAndBack\').value=1;')) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'edit_packet', 'id' => $iId)) . '\');'), 'button') . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['add'], array('id' => 'add_to_packet'), 'button'));
            //$pForm->AddMergedRow($sJS . $pView->Show() . $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('button', $aConfig['lang'][$this->sModule]['save_and_back'], array('onclick' => 'document.getElementById(\'saveAndBack\').value=1;')) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'edit_packet', 'id' => $iId)) . '\');'), 'button') . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang'][$this->sModule]['add'], array('id' => 'add_to_packet'), 'button'));
            $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm())/* .ShowTable($pForm->ShowForm()) */);
        }
    }

// end of AddPacketBookList() function

    /**
     * Metoda dodaje do bazy danych nowy pakiet
     *
     * @param    object $pSmarty
     * @return    void
     */
    function InsertPacket(&$pSmarty)
    {
        global $aConfig;
        $bIsErr = false;

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty);
            return;
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEditPacket($pSmarty);
            return;
        }
        // sprawdzenie tagow
        if (trim($_POST['tags']) != '' && !$this->checkTags($_POST['tags'])) {
            // nieprawidlowo wypelnione pole tagi
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
            $this->AddEditPacket($pSmarty);
            return;
        }

        if (isset($_POST['published']) && $_POST['page_id'] == $aConfig['import']['unsorted_category']) {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_unsorted_err'], $_POST['name']);
            $this->sMsg = GetMessage($sMsg);
            $this->AddEditPacket($pSmarty);
            return;
        }

        $sSql = "SELECT id
					 FROM " . $aConfig['tabls']['prefix'] . "products_publishers 
					 WHERE name LIKE '" . $_POST['publisher'] . "'";
        $iPublisher = (int)Common::GetOne($sSql);
        // przeliczanie cen
        //$this->calculatePacketPrices();
        //wyczyszczenie opisu z linkow i script
        $sDesc = preg_replace('<a.*</a>', '', $_POST['description']);
        $sDesc = preg_replace('<script.*</script>', '', $sDesc);

        Common::BeginTransaction();

        $aValues = array(
            'page_id' => (double)$_POST['page_id'],
            'name' => $_POST['name'],
            'name2' => !empty($_POST['name2']) ? $_POST['name2'] : 'NULL',
            'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($_POST['name'], ' '))),
            'short_description' => strip_tags(br2nl(trimString($_POST['description'], 200), ' ')),
            'description' => $sDesc,
            //'price_netto' => $_POST['packet_price_netto'],
            //'price_brutto' => $_POST['packet_price_brutto'],
            'vat' => 0,
            'shipment_time' => isset($_POST['shipment_time']) ? $_POST['shipment_time'] : '1',
            'shipment_date' => ($_POST['prod_status'] == '3' && !empty($_POST['shipment_date']) && ($_POST['shipment_date'] != '00-00-0000')) ? FormatDate($_POST['shipment_date']) : 'NULL',
            'prod_status' => isset($_POST['prod_status']) ? $_POST['prod_status'] : '0',
            'published' => isset($_POST['published']) ? '1' : '0',
            'created' => 'NOW()',
            'source' => '0',
            'packet' => '1',
            'publication_year' => !empty($_POST['publication_year']) ? (int)$_POST['publication_year'] : 'NULL',
            'created_by' => $_SESSION['user']['name'],
            'additional_discount' => Common::FormatPrice2($_POST['additional_discount']),
            'show_packet_year' => $_POST['show_packet_year'] ? '1' : '0',
            'is_news' => $_POST['news_status'] ? '1' : '0',
            'is_previews' => $_POST['previews_status'] ? '1' : '0',
            'publisher_id' => $iPublisher,
            'isbn' => '',
            'isbn_plain' => ''
        );
        if ($_POST['prod_status'] == '2') {
            $aValues['exhausted_reason'] = $_POST['exhausted_reason'];
        } else {
            $aValues['exhausted_reason'] = 'NULL';
        }
        if ($aValues['additional_discount'] < 0)
            $aValues['additional_discount'] = -$aValues['additional_discount'];
        if ($aValues['publisher_id'] < 1)
            unset($aValues['publisher_id']);

        $iPublished = $aValues['published'];
        if ($_POST['prod_status'] == '3') {
            $aValues['shipment_time'] = '0';
        }
        // dodanie
        $bIsErr = ($iId = Common::Insert($aConfig['tabls']['prefix'] . "products", $aValues)) === false;
        if ($mId != '') {
            $iId = $mId;
        }

        if (!$bIsErr) {
            if (trim($_POST['tags']) != '') {
                // dodanie tagow
                $bIsErr = !$this->addTags($iId, explode(',', $_POST['tags']));
            }
        }

        if (!$bIsErr) {
            // dodanie produktow skladajacych sie na pakiet
            //$bIsErr = !$this->addPacketProducts($iId);
        }
        // dodawanie wydawcy
        $sSql = "SELECT id
					 FROM " . $aConfig['tabls']['prefix'] . "products_publishers 
					 WHERE name LIKE '" . $_POST['publisher'] . "'";
        $iPublisher = (int)Common::GetOne($sSql);
        if ($iPublisher > 0) {
            $aValues['publisher_id'] = $iPublisher;
        } elseif (!empty($_POST['publisher'])) {
            // dodajemy nowego wydawcę

            $aPValues = array(
                'index_letter' => strtoupper(mb_substr($_POST['publisher'], 0, 1, 'utf8')),
                'name' => decodeString($_POST['publisher']),
                'created' => 'NOW()',
                'created_by' => $_SESSION['user']['name']
            );
            if (($iNewId = Common::Insert($aConfig['tabls']['prefix'] . "products_publishers", $aPValues)) !== false) {
                $aValues['publisher_id'] = $iNewId;
            } else {
                $bIsErr = true;
            }
            if (!$bIsErr) {
                $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "products_publishers_import
					 WHERE name = " . Common::Quote(stripslashes($_POST['publisher']));
                $iIId = Common::GetOne($sSql);
                // wydawnictwo istnieje w tabeli ukrytej
                if ($iIId == false) {
                    $aPIValues = array(
                        'name' => decodeString($_POST['publisher'])
                    );
                    if (($iIId = Common::Insert($aConfig['tabls']['prefix'] . "products_publishers_import", $aPIValues)) === false) {
                        $bIsErr = true;
                    }
                }
                if (!$bIsErr) {
                    $aPMValues = array(
                        'publisher_id' => $iNewId,
                        'publisher_import_id' => $iIId
                    );
                    if (Common::Insert($aConfig['tabls']['prefix'] . "products_publishers_mappings", $aPMValues, '', false) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }
        $aValues = array(
            'product_id' => $iId,
            'discount' => 100,
            'price_brutto' => 0,
            'price_netto' => 0,
            'discount_value' => 0
        );
        if (Common::Insert($aConfig['tabls']['prefix'] . "products_tarrifs", $aValues, '', false) === false) {
            $bIsErr = true;
        }


        $_POST['extra_categories'][] = $_POST['page_id'];
        // dodanie dodatkopwych kategorii
        if (!$bIsErr && !empty($_POST['extra_categories'])) {
            foreach ($_POST['extra_categories'] as $iCatId) {
                if ($iCatId != $aConfig['import']['unsorted_category'] && !$this->existExtraCategory($iCatId, $iId)) {
                    $aValues = array(
                        'product_id' => $iId,
                        'page_id' => $iCatId
                    );
                    if ((Common::Insert($aConfig['tabls']['prefix'] . "products_extra_categories", $aValues, '', false)) === false) {
                        $bIsErr = true;
                    }
                    // pobranie kategorii - rodzicow wybranej kategorii
                    $aParents = $this->getPathIDs($iCatId, $_SESSION['lang']['id']);
                    // dodanie mapowan w gore drzewa lokalnego
                    if (!empty($aParents)) {
                        foreach ($aParents as $iParent) {
                            if (!$this->existExtraCategory($iParent, $iId)) {
                                $aValues = array(
                                    'product_id' => $iId,
                                    'page_id' => $iParent
                                );
                                if ((Common::Insert($aConfig['tabls']['prefix'] . "products_extra_categories", $aValues, '', false)) === false) {
                                    $bIsErr = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!$bIsErr) {
            // dodanie serii wydawniczych
            if (empty($_POST['series']) && !empty($_POST['series_name']) && !empty($aValues['publisher_id'])) {
                $aValues = array(
                    'name' => decodeString($_POST['series_name']),
                    'publisher_id' => (int)$aValues['publisher_id'],
                    'created' => 'NOW()',
                    'created_by' => $_SESSION['user']['name']
                );
                if (($iNewId = Common::Insert($aConfig['tabls']['prefix'] . "products_series", $aValues)) !== false) {
                    $_POST['series'] = $iNewId;
                } else {
                    $bIsErr = true;
                }
            }
        }
        if (!$bIsErr) {
            if (!empty($_POST['series'])) {
                $aValues = array(
                    'product_id' => $iId,
                    'series_id' => $_POST['series']
                );
                if ((Common::Insert($aConfig['tabls']['prefix'] . "products_to_series", $aValues, '', false)) === false) {
                    $bIsErr = true;
                }
            }
        }
        // status nowości
        if (!$bIsErr) {
            $aNews = $this->getNews($iId);
            if (empty($aNews)) {
                // produkt nie jest obecnie nowością
                if ($_POST['news_status'] > 0) {
                    // dodajemy do nowości
                    if ($this->addNews($iId, true) === false) {
                        $bIsErr = true;
                    }
                }
            } else {
                // produkt jest nowością
                if ($_POST['news_status'] > 0) {
                    if ($_POST['news_status'] != $aNews['page_id']) {
                        // mamy zmianę - aktualizujemy nowość
                        $aValues = array(
                            'page_id' => $_POST['news_status']
                        );
                        if ((Common::Update($aConfig['tabls']['prefix'] . "products_news", $aValues, 'id = ' . $aNews['id'])) === false) {
                            $bIsErr = true;
                        }
                    }
                } else {
                    // usuwamy nowość
                    if ($this->deleteNews($iId) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }


        // status zapowiedzi
        if (!$bIsErr) {
            $aNews = $this->getPreviews($iId);
            if (empty($aNews)) {
                // produkt nie jest obecnie nowością
                if ($_POST['previews_status'] > 0) {
                    // dodajemy do nowości
                    if ($this->addPreview($iId, true) === false) {
                        $bIsErr = true;
                    }
                }
            } else {
                // produkt jest nowością
                if ($_POST['previews_status'] > 0) {
                    if ($_POST['previews_status'] != $aNews['page_id']) {
                        // mamy zmianę - aktualizujemy nowość
                        $aValues = array(
                            'page_id' => $_POST['previews_status']
                        );
                        if ((Common::Update($aConfig['tabls']['prefix'] . "products_previews", $aValues, 'id = ' . $aNews['id'])) === false) {
                            $bIsErr = true;
                        }
                    }
                } else {
                    // usuwamy nowość
                    if ($this->deletePreviews($iId) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }

        if ($iPublished == '1') {
            if ($this->addShadow($iId) == false) {
                $bIsErr = true;
            }
        }

        if (!$bIsErr) {
            // rekord zostal dodany
            Common::CommitTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['packet_add_ok'], getPagePath((double)$_POST['page_id'], $this->iLangId, true), $_POST['name']);
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            // reset ustawien widoku
            $_GET['reset'] = 2;
            //$this->Show($pSmarty);
            $this->EditPacketBookList($pSmarty, $iId);
        } else {
            // rekord nie zostal dodany,
            // wyswietlenie komunikatu o niepowodzeniu
            // oraz ponowne wyswietlenie formularza dodawania
            Common::RollbackTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['packet_add_err'], getPagePath((double)$_POST['page_id'], $this->iLangId, true), $_POST['name']);
            $this->sMsg = GetMessage($sMsg);
            AddLog($sMsg);
            $this->AddEditPacket($pSmarty);
        }
    }

// end of InsertPacket() funciton
    /**
     * Metoda usowa produkt z pakietu
     *
     * @param    object $pSmarty
     * @param    integer $iPid - Id produktu
     * @param    integer $iId - Id pakietu
     * @return    void
     */

    function removeFromPacket(&$pSmarty, $iId, $iPid)
    {
        global $aConfig;
        $bIsErr = false;

        $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products_packets_items
							 WHERE packet_id = " . $iId . ' AND product_id=' . $iPid;

        $iRes = Common::Query($sSql);
        $iRes += $this->updatePacketAuthorsPrices($iId);

        if ($iRes === false) {
            $sMsg = $aConfig['lang'][$this->sModule]['del_err_00'];
            $this->sMsg = GetMessage($sMsg, true);
            //return false;
        } else {
            $sMsg = $aConfig['lang'][$this->sModule]['del_ok_00'];
            $this->sMsg = GetMessage($sMsg, false);
            //return true;
        }
        $this->EditPacketBookList($pSmarty, $iId);
    }

//end of removeFromPacket

    /**
     * Metoda sprawdxa czy w wybranym pakeicie istnieje juz ksiazka o danym id
     */
    function checkInPacket($iPacketId, $iBookId)
    {
        $sSql = 'SELECT COUNT(packet_id) FROM ' . $aConfig['tabls']['prefix'] . 'products_packets_items WHERE packet_id=' . $iPacketId . ' AND product_id=' . $iBookId;
        return (Common::GetOne($sSql));
    }

    /**
     * Metoda modyfikuje rabaty w pakiecie
     *
     * @param    object $pSmarty
     * @param    integer $iId - Id pakietu
     * @return    void
     */
    function UpdateBooksList(&$pSmarty, $iId)
    {
        global $aConfig;
        $bIsErr = false;

        if (!empty($_POST['editable'])) {
            Common::BeginTransaction();

            /*
			 aktualnie nie używane na prośbę Marcina Chudego okolo września
			foreach ($_POST['editable'] as $iKey => $aItem) {
				if ($this->updateProductPacketPrice($iKey, $iId) === false) {
					$bIsErr = true;
					break;
				}
			}
			 */
            if ($this->updatePacketAuthorsPrices($iId) === false) {
                $bIsErr = true;
            }

            if (!$bIsErr) {
                Common::CommitTransaction();
                $sMsg = $aConfig['lang'][$this->sModule]['packet_edit_ok2'];
                $this->sMsg = GetMessage($sMsg, false);
                // dodanie informacji do logow
                AddLog($sMsg, false);
            } else {
                Common::RollbackTransaction();
                $sMsg = $aConfig['lang'][$this->sModule]['packet_edit_err2'];
                $this->sMsg = GetMessage($sMsg);
                AddLog($sMsg);
            }
        }

        if ($_POST['saveAndBack'])
            $this->AddEditPacket($pSmarty, $iId, false);
        else
            $this->EditPacketBookList($pSmarty, $iId);
    }

//end of UpdateBooksList

    /**
     * Metoda aktualizuje w bazie danych pakiet
     *
     * @param    object $pSmarty
     * @param    integer $iId - Id pakietu
     * @return    void
     */
    function UpdatePacket(&$pSmarty, $iId)
    {
        global $aConfig;
        $bIsErr = false;

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty);
            return;
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEditPacket($pSmarty, $iId);
            return;
        }
        // sprawdzenie tagow
        if (trim($_POST['tags']) != '' && !$this->checkTags($_POST['tags'])) {
            // nieprawidlowo wypelnione pole tagi
            $this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['tags_invalid_err']);
            $this->AddEditPacket($pSmarty, $iId);
            return;
        }

        if (isset($_POST['published']) && $_POST['page_id'] == $aConfig['import']['unsorted_category']) {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_unsorted_err'], $_POST['name']);
            $this->sMsg = GetMessage($sMsg);
            $this->AddEditPacket($pSmarty, $iId);
            return;
        }

        // pobranie aktualnego stanu publikacji
        $aCurrent = &$this->getItem($iId);

        // przeliczanie cen
        //$this->calculatePacketPrices();
        //pobranie domyslnego rabatu
        $sSql = 'SELECT additional_discount FROM ' . $aConfig['tabls']['prefix'] . 'products WHERE id=' . $iId;
        $fDefDiscount = Common::GetOne($sSql);
        $fCurDiscount = Common::FormatPrice2($_POST['additional_discount']);
        Common::BeginTransaction();


        //wyczyszczenie opisu z linkow i script
        $sDesc = eregi_replace('<a.*</a>', '', $_POST['description']);
        $sDesc = eregi_replace('<script.*</script>', '', $sDesc);

        $aValues = array(
            'page_id' => (double)$_POST['page_id'],
            'name' => $_POST['name'],
            'name2' => !empty($_POST['name2']) ? $_POST['name2'] : 'NULL',
            'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($_POST['name'], ' '))),
            'short_description' => strip_tags(br2nl(trimString($_POST['description'], 200), ' ')),
            'description' => $sDesc,
            //'price_netto' => $_POST['packet_price_netto'],
            //'price_brutto' => $_POST['packet_price_brutto'],
            'vat' => 0,
            'shipment_time' => isset($_POST['shipment_time']) ? $_POST['shipment_time'] : '1',
            'prod_status' => isset($_POST['prod_status']) ? $_POST['prod_status'] : '0',
            'shipment_date' => ($_POST['prod_status'] == '3' && !empty($_POST['shipment_date']) && ($_POST['shipment_date'] != '00-00-0000')) ? FormatDate($_POST['shipment_date']) : 'NULL',
            'published' => isset($_POST['published']) ? '1' : '0',
            'packet' => '1',
            'additional_discount' => Common::FormatPrice2($_POST['additional_discount']),
            'publication_year' => !empty($_POST['publication_year']) ? (int)$_POST['publication_year'] : 'NULL',
            'show_packet_year' => $_POST['show_packet_year'] ? '1' : '0',
            'is_news' => $_POST['news_status'] ? '1' : '0',
            'is_previews' => $_POST['previews_status'] ? '1' : '0',
            'publisher_id' => $iPublisher
        );
        if ($aValues['additional_discount'] < 0)
            $aValues['additional_discount'] = -$aValues['additional_discount'];
        if ($_POST['prod_status'] == '3') {
            $aValues['shipment_time'] = '0';
        }
        if ($aValues['publisher_id'] < 1)
            unset($aValues['publisher_id']);
        // dodawanie wydawcy
        $sSql = "SELECT id
					 FROM " . $aConfig['tabls']['prefix'] . "products_publishers 
					 WHERE name LIKE '" . $_POST['publisher'] . "'";
        $iPublisher = (int)Common::GetOne($sSql);
        if ($iPublisher > 0) {
            $aValues['publisher_id'] = $iPublisher;
        } elseif (!empty($_POST['publisher'])) {
            // dodajemy nowego wydawcę

            $aPValues = array(
                'index_letter' => strtoupper(mb_substr($_POST['publisher'], 0, 1, 'utf8')),
                'name' => decodeString($_POST['publisher']),
                'created' => 'NOW()',
                'created_by' => $_SESSION['user']['name']
            );
            if (($iNewId = Common::Insert($aConfig['tabls']['prefix'] . "products_publishers", $aPValues)) !== false) {
                $aValues['publisher_id'] = $iNewId;
            } else {
                $bIsErr = true;
            }
            if (!$bIsErr) {
                $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "products_publishers_import
					 WHERE name = " . Common::Quote(stripslashes($_POST['publisher']));
                $iIId = Common::GetOne($sSql);
                // wydawnictwo istnieje w tabeli ukrytej
                if ($iIId == false) {
                    $aPIValues = array(
                        'name' => decodeString($_POST['publisher'])
                    );
                    if (($iIId = Common::Insert($aConfig['tabls']['prefix'] . "products_publishers_import", $aPIValues)) === false) {
                        $bIsErr = true;
                    }
                }
                if (!$bIsErr) {
                    $aPMValues = array(
                        'publisher_id' => $iNewId,
                        'publisher_import_id' => $iIId
                    );
                    if (Common::Insert($aConfig['tabls']['prefix'] . "products_publishers_mappings", $aPMValues, '', false) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }
        // aktualizacja
        $bIsErr = Common::Update($aConfig['tabls']['prefix'] . "products", $aValues, "id = " . $iId) === false;
        if (!$bIsErr) {
            // dodanie serii wydawniczych
            if (empty($_POST['series']) && !empty($_POST['series_name']) && !empty($aValues['publisher_id'])) {
                $aValues = array(
                    'name' => decodeString($_POST['series_name']),
                    'publisher_id' => (int)$aValues['publisher_id'],
                    'created' => 'NOW()',
                    'created_by' => $_SESSION['user']['name']
                );
                if (($iNewId = Common::Insert($aConfig['tabls']['prefix'] . "products_series", $aValues)) !== false) {
                    $_POST['series'] = $iNewId;
                } else {
                    $bIsErr = true;
                }
            }
        }
        if (!$bIsErr) {
            if (!empty($_POST['series'])) {
                $aValues = array(
                    'product_id' => $iId,
                    'series_id' => $_POST['series']
                );
                if ((Common::Insert($aConfig['tabls']['prefix'] . "products_to_series", $aValues, '', false)) === false) {
                    $bIsErr = true;
                }
            }
        }
        //zmodyfikowanie rabatów produktów należących do tego pakietu jeżeli zmienił się domyślny rabat
        if ($fDefDiscount != $fCurDiscount) {
            /* 'discount'=>Common::FormatPrice2($aItem['discount']),
			  'discount_currency'=>Common::FormatPrice2($aProd['price_brutto']*Common::FormatPrice2($aItem['discount'])/100.0),
			  'promo_price_netto'=>Common::FormatPrice2($aProd['price_brutto']-$aProd['price_brutto']*Common::FormatPrice2($aItem['discount'])/100.0),
			  'promo_price_brutto'=>Common::FormatPrice2($aProd['price_netto']-$aProd['price_netto']*Common::FormatPrice2($aItem['discount'])/100.0),
			 */
            /*
			  $sSql='UPDATE '.$aConfig['tabls']['prefix'].'products_packets_items SET
			  `discount`=`discount`+ '.($fCurDiscount-$fDefDiscount).'
			  WHERE packet_id='.$iId;
			 */
            //if(Common::Query($sSql)===false) $bIsErr+=1;

            $aBaseProductsPacketData = $this->getProductsDataPacket($iId);

            foreach ($aBaseProductsPacketData as $aBaseProductData) {

                $sSql = "SELECT discount FROM " . $aConfig['tabls']['prefix'] . "products_packets_items
								 WHERE packet_id=" . $iId . ' AND product_id=' . $aBaseProductData['id'];
                $fProdDiscount = Common::GetOne($sSql);
                $iCurProdPacketDiscount = $fProdDiscount + $fCurDiscount;

                $aValues = array(
                    'discount_currency' => Common::formatPrice2($aBaseProductData['price_brutto'] * $iCurProdPacketDiscount / 100),
                    'promo_price_netto' => Common::formatPrice2($aBaseProductData['price_netto'] - $aBaseProductData['price_netto'] * $iCurProdPacketDiscount / 100),
                    'promo_price_brutto' => Common::formatPrice2($aBaseProductData['price_brutto'] - $aBaseProductData['price_brutto'] * $iCurProdPacketDiscount / 100),
                );
                if (Common::Update($aConfig['tabls']['prefix'] . 'products_packets_items', $aValues, 'packet_id=' . $iId . ' AND product_id=' . $aBaseProductData['id']) === false) {
                    $bIsErr = true;
                    break;
                    // blad wyskakujemy
                }
                /*
				  // aktualizujemy cene dla skladowej pakietu
				  $sSql='UPDATE '.$aConfig['tabls']['prefix'].'products_packets_items SET
				  `discount_currency`= '..',
				  `promo_price_netto`= (`price_netto`-`price_netto`*(`discount`)/100),
				  `promo_price_brutto`= (`price_brutto`-`price_brutto`*(`discount`)/100),
				  `price_netto`= (`price_netto`),
				  `price_brutto`= (`price_brutto`)
				  WHERE packet_id='.$iId;
				  if(Common::Query($sSql) === false) {
				  $bIsErr = true;
				  break;
				  // blad wyskakujemy
				  }
				 */
            }
            /*

			  $sSql='UPDATE '.$aConfig['tabls']['prefix'].'products_packets_items SET
			  `discount_currency`=(`price_brutto`*(`discount`)/100),
			  `promo_price_netto`= (`price_netto`-`price_netto`*(`discount`)/100),
			  `promo_price_brutto`= (`price_brutto`-`price_brutto`*(`discount`)/100),
			  `price_netto`= (`price_netto`),
			  `price_brutto`= (`price_brutto`)
			  WHERE packet_id='.$iId;
			 */
            if ($bIsErr === false)
                $this->updatePacketAuthorsPrices($iId);
        }

        if ($aCurrent['published'] != $aValues['published']) {
            if ($aValues['published'] == '1') {
                if ($this->publishTags($iId) == false) {
                    $bIsErr = true;
                }
            } else {
                if ($this->unpublishTags($iId) == false) {
                    $bIsErr = true;
                }
            }
        }

        if (!$bIsErr) {
            // aktualizacja tagow
            $bIsErr = !$this->addTags($iId, trim($_POST['tags']) != '' ? explode(',', $_POST['tags']) : array(), true);
        }
        // status nowości
        if (!$bIsErr) {
            $aNews = $this->getNews($iId);
            if (empty($aNews)) {
                // produkt nie jest obecnie nowością
                if ($_POST['news_status'] > 0) {
                    // dodajemy do nowości
                    if ($this->addNews($iId, true) === false) {
                        $bIsErr = true;
                    }
                }
            } else {
                // produkt jest nowością
                if ($_POST['news_status'] > 0) {
                    if ($_POST['news_status'] != $aNews['page_id']) {
                        // mamy zmianę - aktualizujemy nowość
                        $aValues = array(
                            'page_id' => $_POST['news_status']
                        );
                        if ((Common::Update($aConfig['tabls']['prefix'] . "products_news", $aValues, 'id = ' . $aNews['id'])) === false) {
                            $bIsErr = true;
                        }
                    }
                } else {
                    // usuwamy nowość
                    if ($this->deleteNews($iId) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }

        // status zapowiedzi
        if (!$bIsErr) {
            $aNews = $this->getPreviews($iId);
            if (empty($aNews)) {
                // produkt nie jest obecnie nowością
                if ($_POST['previews_status'] > 0) {
                    // dodajemy do nowości
                    if ($this->addPreview($iId, true) === false) {
                        $bIsErr = true;
                    }
                }
            } else {
                // produkt jest nowością
                if ($_POST['previews_status'] > 0) {
                    if ($_POST['previews_status'] != $aNews['page_id']) {
                        // mamy zmianę - aktualizujemy nowość
                        $aValues = array(
                            'page_id' => $_POST['previews_status']
                        );
                        if ((Common::Update($aConfig['tabls']['prefix'] . "products_previews", $aValues, 'id = ' . $aNews['id'])) === false) {
                            $bIsErr = true;
                        }
                    }
                } else {
                    // usuwamy nowość
                    if ($this->deletePreviews($iId) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }

        /* if (!$bIsErr) {
		  // dodanie produktow skladajacych sie na pakiet
		  $bIsErr = !$this->addPacketProducts($iId, true);
		  } */

        /* $aValues=array(
		  'discount' => 100,
		  'price_brutto' => $_POST['packet_promo_price_brutto'],
		  'price_netto' => $_POST['packet_promo_price_netto'],
		  'discount_value' => $_POST['packet_discount_money']
		  );
		  if (Common::Update($aConfig['tabls']['prefix']."products_tarrifs",
		  $aValues,"isdefault = '0' AND product_id = ".$iId) === false) {
		  $bIsErr = true;
		  } */

        sort($_POST['extra_categories'], SORT_NUMERIC);
        $aActCat = $this->getExtraCategories($iId);
        sort($aActCat, SORT_NUMERIC);
        if ($_POST['extra_categories'] != $aActCat) {
            //usuniecie poprzednich dodatkopwych kategorii
            if (!$bIsErr) {
                $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products_extra_categories
              WHERE product_id = " . $iId;
                $bIsErr = (Common::Query($sSql) === false);
            }
            // dodanie dodatkopwych kategorii
            $_POST['extra_categories'][] = $_POST['page_id'];
            // dodanie dodatkopwych kategorii
            if (!$bIsErr && !empty($_POST['extra_categories'])) {
                foreach ($_POST['extra_categories'] as $iCatId) {
                    if ($iCatId != $aConfig['import']['unsorted_category'] && !$this->existExtraCategory($iCatId, $iId)) {
                        $aValues = array(
                            'product_id' => $iId,
                            'page_id' => $iCatId
                        );
                        if ((Common::Insert($aConfig['tabls']['prefix'] . "products_extra_categories", $aValues, '', false)) === false) {
                            $bIsErr = true;
                        }
                        // pobranie kategorii - rodzicow wybranej kategorii
                        $aParents = $this->getPathIDs($iCatId, $_SESSION['lang']['id']);
                        // dodanie mapowan w gore drzewa lokalnego
                        if (!empty($aParents)) {
                            foreach ($aParents as $iParent) {
                                if (!$this->existExtraCategory($iParent, $iId)) {
                                    $aValues = array(
                                        'product_id' => $iId,
                                        'page_id' => $iParent
                                    );
                                    if ((Common::Insert($aConfig['tabls']['prefix'] . "products_extra_categories", $aValues, '', false)) === false) {
                                        $bIsErr = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!$bIsErr) {
            // stan publikacji
            $_POST['published'] = (int)$_POST['published'];
            $_POST['old_published'] = (int)$_POST['old_published'];

            // tabela cieni
            // produkt obecnie nie jest opublikowany - zmiana na opublikowany - dodanie cieia
            if ($_POST['old_published'] == 0) {
                if ($_POST['published'] == 1) {
                    if ($this->existShadow($iId)) {
                        if ($this->updateShadow($iId) === false) {
                            $bIsErr = true;
                        }
                    } else {
                        if ($this->addShadow($iId) === false) {
                            $bIsErr = true;
                        }
                    }
                }
            } else {
                // produkt jest opublikowany - zmiana na nieopublikowany - usuniecie cienia
                if ($_POST['published'] == 0) {
                    if ($this->deleteShadow($iId) === false) {
                        $bIsErr = true;
                    }
                } else {
                    // brak zmiany stany publikacji - aktualizacja cienia
                    if ($this->existShadow($iId)) {
                        if ($this->updateShadow($iId) === false) {
                            $bIsErr = true;
                        }
                    } else {
                        if ($this->addShadow($iId) === false) {
                            $bIsErr = true;
                        }
                    }
                }
            }
        }

        if (!$bIsErr) {
            // rekord zostal zaktualizowany
            SetModification('products', $iId);
            Common::CommitTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['packet_edit_ok'], getPagePath((double)$_POST['page_id'], $this->iLangId, true), $_POST['name']);
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            $this->Show($pSmarty);
        } else {
            // rekord nie zostal zaktualizowany
            Common::RollbackTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['packet_edit_err'], getPagePath((double)$_POST['page_id'], $this->iLangId, true), $_POST['name']);
            $this->sMsg = GetMessage($sMsg);
            AddLog($sMsg);
            $this->AddEditPacket($pSmarty, $iId);
        }
    }

// end of UpdatePacket() funciton

    /**
     * Metoda pobiera dane produktów z pakietu
     *
     * @global type $aConfig
     * @param integer $iPacketId
     * @return array
     */
    function getProductsDataPacket($iPacketId)
    {
        global $aConfig;

        $sSql = "SELECT B.id, B.price_brutto, B.price_netto 
						 FROM " . $aConfig['tabls']['prefix'] . "products_packets_items AS A
						 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products AS B
							ON A.product_id=B.id
						 WHERE packet_id=" . $iPacketId;
        return Common::GetAll($sSql);
    }

// end of getProductsDataPacket() method

    /**
     * Metoda przelicza ceny pakietu
     *
     * @return    void
     */
    function calculatePacketPrices()
    {
        /* $_POST['packet_discount_money'] = 0;
		  $_POST['packet_price_brutto'] = 0;
		  $_POST['packet_price_netto'] = 0;
		  $_POST['packet_promo_price_brutto'] = 0;
		  $_POST['packet_promo_price_netto'] = 0;

		  foreach ($_POST['product'] as $iKey => $iProdId) {
		  $iProdId = (double) $iProdId;
		  if ($iProdId > 0) {
		  // cena brutto
		  $_POST['price_brutto'][$iKey] = Common::formatPrice2($_POST['price_brutto'][$iKey]);
		  // vat
		  $_POST['vat'][$iKey] = (int) $_POST['vat'][$iKey];

		  // okreslenie ceny netto
		  $_POST['price_netto'][$iKey] = $_POST['vat'][$iKey] > 0 ? Common::formatPrice2($_POST['price_brutto'][$iKey] / (1 + ($_POST['vat'][$iKey] / 100))) : $_POST['price_brutto'][$iKey];
		  // rabat
		  $_POST['discount'][$iKey] = Common::formatPrice2($_POST['discount'][$iKey]);
		  $_POST['discount_money'][$iKey] = 0;
		  if ($_POST['discount'][$iKey] > 0) {
		  // obliczenie wartosci brutto rabatu
		  $_POST['discount_money'][$iKey] = Common::formatPrice2($_POST['price_brutto'][$iKey] * ($_POST['discount'][$iKey] / 100));
		  // cena promocyjna brutto
		  $_POST['promo_price_brutto'][$iKey] = $_POST['price_brutto'][$iKey] - $_POST['discount_money'][$iKey];
		  // cena promocyjna netto
		  $_POST['promo_price_netto'][$iKey] = $_POST['vat'][$iKey] > 0 ? Common::formatPrice2($_POST['promo_price_brutto'][$iKey] / (1 + ($_POST['vat'][$iKey] / 100))) : $_POST['promo_price_brutto'][$iKey];

		  // cena promocyjna pakietu
		  $_POST['packet_promo_price_brutto'] += $_POST['promo_price_brutto'][$iKey];
		  $_POST['packet_promo_price_netto'] += $_POST['promo_price_netto'][$iKey];
		  $_POST['packet_discount_money'] += $_POST['discount_money'][$iKey];
		  }
		  else {
		  $_POST['packet_promo_price_brutto'] += $_POST['price_brutto'][$iKey];
		  $_POST['packet_promo_price_netto'] += $_POST['price_netto'][$iKey];
		  }
		  $_POST['packet_price_brutto'] += $_POST['price_brutto'][$iKey];
		  $_POST['packet_price_netto'] += $_POST['price_netto'][$iKey];
		  }
		  } */
    }

// end of calculatePacketPrices() method

    /**
     * Metoda dodaje skladowe produkty pakietu
     *
     * @param    integer $iId - id pakietu
     * @param    bool $bUpdate - true: usun dotychczasowe
     * @return    bool
     */
    function addPacketProducts($iId, $bUpdate = false)
    {
        global $aConfig;

        if ($bUpdate) {
            $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products_packets_items
							 WHERE packet_id = " . $iId;
            if (Common::Query($sSql) === false)
                return false;
        }
        // dodawanie produktow pakietu
        foreach ($_POST['product'] as $iKey => $iProdId) {
            $iProdId = (double)$iProdId;
            if ($iProdId > 0) {
                $aValues = array(
                    'packet_id' => $iId,
                    'product_id' => $iProdId,
                    'price_netto' => $_POST['price_netto'][$iKey],
                    'price_brutto' => $_POST['price_brutto'][$iKey],
                    'vat' => $_POST['vat'][$iKey],
                    'discount' => $_POST['discount'][$iKey],
                    'discount_currency' => $_POST['discount_money'][$iKey],
                    'promo_price_netto' => $_POST['promo_price_netto'][$iKey],
                    'promo_price_brutto' => $_POST['promo_price_brutto'][$iKey],
                    'order_by' => '#order_by#'
                );
                // dodanie
                if (Common::Insert($aConfig['tabls']['prefix'] . "products_packets_items", $aValues, "packet_id = " . $iId, false) === false)
                    return false;
            }
        }
        return true;
    }

// end of addPacketProducts() method

    /**
     * Metoda dodaje skladowe produkty pakietu
     *
     * @param    integer $iId - id pakietu
     * @param    bool $bUpdate - true: usun dotychczasowe
     * @return    bool
     */
    function addProductToPacket($iId, $sProdId)
    {
        global $aConfig;

        $bIsErr = 0;
        $aProdId = explode(',', $sProdId);

        //pobranie domyslnego rabatu
        $sSql = 'SELECT additional_discount FROM ' . $aConfig['tabls']['prefix'] . 'products WHERE id=' . $iId;
        $_POST['discount'] = Common::GetOne($sSql);

        Common::BeginTransaction();

        foreach ($aProdId as $iKey => $iProdId) {
            // dodawanie produktow pakietu
            $iProdId = (double)$iProdId;
            if ($iProdId > 0) {
                if ($this->checkInPacket($iId, $iProdId)) {
                    $sMsg2 = $aConfig['lang'][$this->sModule]['book_is_in_packet'];
                    continue;
                }

                // nie można dodać do pakietu produktu w statusie:
                // nakład wyczerpany, niedostępna, lub nieopublikowana
                if ($this->checkProductStatus($iProdId) === false) {
                    $sMsg2 = sprintf($aConfig['lang'][$this->sModule]['book_status_err'], $this->getItemName($iProdId));
                    continue;
                }

                $sSql = 'SELECT id, vat FROM ' . $aConfig['tabls']['prefix'] . 'products WHERE id=' . $iProdId . ' LIMIT 1';
                $aProduct = Common::GetRow($sSql);
                $iNow = time();
                $sSql = 'SELECT price_netto, price_brutto, discount FROM ' . $aConfig['tabls']['prefix'] . 'products_tarrifs WHERE product_id=' . $iProdId . ' AND start_date<' . $iNow . ' AND end_date>=' . $iNow . ' ORDER BY price_brutto ASC LIMIT 1';
                $aProduct2 = Common::GetRow($sSql);

                $sSql = 'SELECT price_netto, price_brutto FROM ' . $aConfig['tabls']['prefix'] . 'products WHERE id=' . $iProdId;
                $aProduct22 = Common::GetRow($sSql);

                Common::RollbackTransaction();
                $aValues = array(
                    'packet_id' => $iId,
                    'product_id' => $iProdId,
                    'price_netto' => $aProduct2['price_netto'],
                    'price_brutto' => $aProduct2['price_brutto'],
                    'vat' => $aProduct['vat'],
                    'discount' => $aProduct2['discount'],
                    'discount_currency' => round($aProduct2['price_netto'] * ($_POST['discount'] / 100.0), 2),
                    'promo_price_netto' => $aProduct22['price_netto'] - Common::FormatPrice2($aProduct22['price_netto'] * ($_POST['discount'] + $aProduct2['discount']) / 100.0),
                    'promo_price_brutto' => $aProduct22['price_brutto'] - Common::FormatPrice2($aProduct22['price_brutto'] * ($_POST['discount'] + $aProduct2['discount']) / 100.0),
                    'order_by' => '#order_by#'
                );
                // dodanie
                $bIsErr += !Common::Insert($aConfig['tabls']['prefix'] . "products_packets_items", $aValues, "packet_id = " . $iId, false);
            }
        }

        // aktualizacja cen pakietu
        $bIsErr += $this->updatePacketAuthorsPrices($iId);


        // pobranie tagów pakietu
        $sTagsPacket = $this->getProductTags($iId);

        // pobranie tagów produktu który powinien zostać do pakietu
        $sTagsNewProduct = $this->getProductTags($iProdId);

        if ($sTagsPacket != '') {
            if ($sTagsNewProduct != '') {
                $sTags = trim($sTagsPacket) . ',' . trim($sTagsNewProduct);
            } else {
                $sTags = $sTagsPacket;
            }
        } else {
            if ($sTagsNewProduct != '') {
                $sTags = $sTagsNewProduct;
            }
        }

        // sprawdzenie tagow
        if (trim($sTags)) {
            if (!$this->checkTags($sTags)) {
                // nieprawidłowo wypełnione tagi
                $bIsErr = true;
            }
        }

        $iPublished = $this->getProductAttrib($iId, 'published');

        if (!$bIsErr) {
            if ($iPublished == '1') {
                if ($this->publishTags($iId) == false) {
                    $bIsErr = true;
                }
            } else {
                if ($this->unpublishTags($iId) == false) {
                    $bIsErr = true;
                }
            }
        }

        if (!$bIsErr) {
            // aktualizacja tagow
            $bIsErr = !$this->addTags($iId, trim($sTags) != '' ? explode(',', $sTags) : array(), true);
        }

        if (!$bIsErr) {
            if ($this->updateShadowTags($iId) === false) {
                $bIsErr = true;
            }
        }


        if ($bIsErr) {
            Common::RollbackTransaction();
            $sMsg = $aConfig['lang'][$this->sModule]['book_notadded_to_packet'];
            $this->sMsg = GetMessage($sMsg, true);
        } else {
            Common::CommitTransaction();
            $sMsg = $aConfig['lang'][$this->sModule]['book_added_to_packet'];
            $this->sMsg = GetMessage($sMsg, false);
            if ($sMsg2 != '')
                $this->sMsg = GetMessage($sMsg2, true);
        }
        return true;
    }

// end of addPacketProducts() method

    /**
     * Metoda pobiera produkty pakietu o podanym Id
     *
     * @param    integer $iId - Id pakietu
     * @param    array ref    $aData    - tablica z danymi pakietu
     * @return    array
     */
    function &getPacketProducts($iId, &$aData)
    {
        global $aConfig;

        $sSql = "SELECT B.product_id, B.discount, A.name, A.price_brutto, C.name AS publisher, B.promo_price_brutto
						 FROM " . $aConfig['tabls']['prefix'] . "products_packets_items B
						 JOIN " . $aConfig['tabls']['prefix'] . "products A
						 ON A.id = B.product_id
						 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers C
						 ON A.publisher_id = C.id
						 WHERE B.packet_id = " . $iId . "
						 ORDER BY order_by";
        $aItems = &Common::GetAll($sSql);
        unset($aData['publisher']);
        foreach ($aItems as $iKey => $aItem) {
            $aData['product'][$iKey] = $aItem['product_id'];
            $aData['discount'][$iKey] = $aItem['discount'];
            $aData['name'][$iKey] = $aItem['name'];
            $aData['price_brutto'][$iKey] = $aItem['price_brutto'];
            $aData['publisher'][$iKey] = array();
            $aData['publisher'][$iKey] = $aItem['publisher'];
            $aData['promo_price_brutto'][$iKey] = $aItem['promo_price_brutto'];
        }
    }

// end of getPacketProducts() method

    /**
     * Metoda pobiera pakiety w ktorych wystepuje produkt o Id
     *
     * @param    integer $iId - Id produktu
     * @param    array ref    $aData    - tablica z pakietami produktu
     * @return    array
     */
    function &getProductPackets($iId)
    {
        global $aConfig;

        $sSql = "SELECT A.page_id, A.id, A.plain_name
						 FROM " . $aConfig['tabls']['prefix'] . "products_packets_items B
						 JOIN " . $aConfig['tabls']['prefix'] . "products A
						 ON A.id = B.packet_id
						 WHERE B.product_id = " . $iId . "
						 ORDER BY A.plain_name";
        return Common::GetAll($sSql);
    }

// end of getProductPackets() method

    /**
     * Zwraca id taga, w razie potrzeby tworzy wpis w tabeli products_tags
     * @param $sTag - tag
     * @return int id taga / false
     */
    function getIdForTag($sTag)
    {
        global $aConfig;
        $sTag = trim($sTag);
        if (!empty($sTag)) {
            $sSql = "SELECT id 
							FROM " . $aConfig['tabls']['prefix'] . "products_tags
							WHERE tag = " . Common::Quote($sTag);
            $iId = intval(Common::GetOne($sSql));
            if ($iId > 0) {
                return $iId;
            } else {
                $aValues = array(
                    'tag' => $sTag,
                    'quantity' => 0,
                    'page_id' => $_POST['page_id']
                );
                if (($iId = Common::Insert($aConfig['tabls']['prefix'] . "products_tags", $aValues)) === false) {
                    return false;
                } else {
                    return $iId;
                }
            }
        }
        return false;
    }

    /**
     * Metoda sprawdza poprawosc wypelnienia tagow
     *
     * @param    string $sTags - tagi
     * @return    bool
     */
    function checkTags($sStr = '')
    {
        if ($sStr == '') {
            $sStr = $_POST['tags'];
        }

        return preg_match('/^[^`~!@#\$%\^&\*\(\)\+=\[\]\{\};:\'"\,\.<\\>\?\/](\s*[^`~!@#\$%\^&\*\(\)\+=\[\]\{\};:\'"\,\.<\\>\?\/])+(,\s*[^`~!@#\$%\^&\*\(\)\+=\[\]\{\};:\'"\,\.<\\>\?\/](\s*[^`~!@#\$%\^&\*\(\)\+=\[\]\{\};:\'"\,\.<\\>\?\/])+)*$/', $sStr);
    }

// end of checkTags() method

    /**
     * Funkcja sprawdza czy do produktu jest przywiązany tag o podanym id
     * @param $iPId int - id produktu
     * @param $iTagId int - id tagu
     * @return bool
     */
    function existsProductTag($iPId, $iTagId)
    {
        global $aConfig;
        $sSql = "SELECT COUNT(1)
						FROM " . $aConfig['tabls']['prefix'] . "products_to_tags
						WHERE product_id = " . $iPId . "
						AND tag_id = " . $iTagId;
        return (Common::GetOne($sSql) > 0);
    }

    /**
     * Metoda dodaje tagi produktu
     *
     * @param    integer $iId - Id ksiazki
     * @param    array $aTags - tagi
     * @param    bool $bUpdate - true: update
     * @return    bool
     */
    function addTags($iId, $aTags, $bUpdate = false)
    {
        global $aConfig;

        if ($bUpdate && !$this->deleteTags($iId))
            return false;

        foreach ($aTags as $sTag) {
            if (trim($sTag) != '') {
                $aValues = array(
                    'product_id' => $iId,
                    'tag_id' => $this->getIdForTag($sTag)
                );
                if ($aValues['tag_id'] > 0) {
                    if ($this->existsProductTag($iId, $aValues['tag_id'])) {
                        return true;
                    }
                    if (Common::Insert($aConfig['tabls']['prefix'] . "products_to_tags", $aValues, "", false) === false) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        return true;
    }

// end of addTags() method

    /**
     * Metoda usuwa tagi produktu o podanym Id
     *
     * @param    integer $iId - Id produktu
     * @return    bool
     */
    function deleteTags($iId)
    {
        global $aConfig;

        // usuniecie poprzednich tagow
        $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products_to_tags
						 WHERE product_id = " . $iId;
        if (Common::Query($sSql) === false)
            return false;
        return true;
    }

// end deleteTags() method

    /**
     * Metoda publikuje tagi produktu o podanym Id
     * @param $iId - id produktu
     * @return bool
     */
    function publishTags($iId)
    {
        global $aConfig;

        $sSql = "SELECT tag_id
							FROM " . $aConfig['tabls']['prefix'] . "products_to_tags
							WHERE product_id = " . $iId;
        $bIsError = false;
        $aTags = Common::GetCol($sSql);
        //dump($aTags);
        if (!empty($aTags)) {
            foreach ($aTags as $iTag) {
                //tag juz istnieje - zwiekszenie liczebnosci
                $sSql = "UPDATE " . $aConfig['tabls']['prefix'] . "products_tags SET quantity = quantity + 1 WHERE id = " . $iTag;
                if (Common::Query($sSql) === false) {
                    $bIsError = true;
                }
            }
        }
        return !$bIsError;
    }

    /**
     * Metoda odpublikowuje tagi produktu o podanym id
     * @param $iId - id produktu
     * @return bool
     */
    function unpublishTags($iId)
    {
        global $aConfig;

        $sSql = "SELECT tag_id
							FROM " . $aConfig['tabls']['prefix'] . "products_to_tags
							WHERE product_id = " . $iId;
        $bIsError = false;
        $aTags = Common::GetCol($sSql);
        if (!empty($aTags)) {
            foreach ($aTags as $iTag) {
                $sSql = "UPDATE " . $aConfig['tabls']['prefix'] . "products_tags SET quantity = quantity - 1 WHERE id = " . $iTag;
                if (Common::Query($sSql) === false) {
                    $bIsError = true;
                }
            }
        }
        return !$bIsError;
    }

    /**
     * Metoda wykonuje operacje na plikach
     *
     * @param    object $pSmarty
     * @param    integer $iPId
     * @param    integer $iId
     * @return    integer
     */
    function ProceedFiles(&$pSmarty, $iPId, $iId, $bAddFile = false)
    {
        global $aConfig;

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->Show($pSmarty, $iPId);
            return;
        }
        $bIsErr = false;
        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $this->AddEditFiles($pSmarty, $iPId, $iId, $bAddFile);
            return;
        }
        // walidacja na podst plików

        foreach ($_FILES['file']['name'] as $key => $aFile) {
            if (!empty($_FILES['file']['name'][$key]) && empty($_POST['filetype'][$key])) {
                $this->sMsg = GetMessage(sprintf($aConfig['lang'][$this->sModule]['no_filetype_err'], $_FILES['file']['name'][$key]), true);
                $this->AddEditFiles($pSmarty, $iPId, $iId, $bAddFile);
                return;
            }
        }
        //dump($_POST);
        $sPathId = $iId . '/';

        //dump($_FILES);
        // dolaczenie klasy File
        include_once('File.class.php');
        $oFile = new MyFile();
        foreach ($_POST['filetype'] as $key => $iFiletype) {

            if (isset($_POST['old_file'][$key])) {
                $_POST['old_file'][$key] = substr($_POST['old_file'][$key], strlen($sPathId));
            }

            // przetwarzanie pliku
            if (!$bIsErr && isset($_POST['delete_file'][$key])) {
                // usuwanie poprzedniego piku
                if (!$this->deleteFile($iId, $key, $_POST['old_file'][$key])) {
                    $iResult = 0;
                    $sFileName = $_POST['old_file'][$key];
                    break;
                } else {
                    $iResult = 1;
                }
            }
            if (!$bIsErr && !empty($_FILES['file']['name'][$key])) {
                // przeslano plik - przetwarzanie
                // zamiana spacji i innych znakow na '_'
                $_FILES['file']['name'][$key] = preg_replace('/\s+/', '_', $_FILES['file']['name'][$key]);
                if (($iResult = $this->addFile($iId, $key, $_FILES['file'], $oFile, isset($_POST['old_file'][$key]) && $_POST['old_file'][$key] == $_FILES['file']['name'][$key])) != 1) {
                    $sFileName = $_FILES['file']['name'][$key];
                    $this->sMsg .= GetMessage(sprintf($aConfig['lang'][$this->sModule]['files_err_' . $iResult], $this->getItemName($iId), $_FILES['file']['name'][$key]), false);
                    $bIsErr = true;
                } else {
                    if (isset($_POST['old_file'][$key]) && !isset($_POST['delete_file'][$key]) && $_POST['old_file'][$key] != $_FILES['file']['name'][$key]) {
                        // poprzedni plik byl inny od nowego - usuniecie
                        @unlink($aConfig['common']['client_base_path'] . $aConfig['common']['product_file_dir'] . '/' . $iId . '/' . $_POST['old_file'][$key]);
                    }
                }
            }
        } //die();

        if (!$bIsErr) {
            // sukces
            Common::CommitTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['files_ok'], $this->getItemName($iId));
            $this->sMsg .= GetMessage($sMsg, false);
            AddLog($sMsg);
            $this->AddEditFiles($pSmarty, $iPId, $iId, $bAddFile);
        } else {
            // blad
            Common::RollbackTransaction();
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['files_err'], $this->getItemName($iId));
            $this->sMsg .= GetMessage($sMsg);
            AddLog($sMsg);
            $this->AddEditFiles($pSmarty, $iPId, $iId, $bAddFile);
        }
    }

// end of ProceedFiles() method

    function getFileTypes()
    {
        global $aConfig;
        $sSql = "SELECT id AS value, name AS label
						FROM " . $aConfig['tabls']['prefix'] . "products_file_types
						ORDER BY name";
        $aFileTypes = Common::GetAll($sSql);
        return array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])), $aFileTypes);
    }

    /**
     * Metoda tworzy formularz dodawania / edycji plikow produktow
     *
     * @param    object $pSmarty
     * @param    integer $iPId - Id strony
     * @param    integer $iId Id produktu
     */
    function AddEditFiles(&$pSmarty, $iPId, $iId, $bAdd = false)
    {
        global $aConfig;

        $aData = array();
        $sHtml = '';

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        // pobranie plikow produktu
        $aFiles = &$this->getFiles($iId);

        $iFiles = 0;
        if (!empty($_POST['desc_file'])) {
            $iFiles = count($_POST['desc_file']);
        } elseif (!empty($aFiles)) {
            $iFiles = count($aFiles) + 1;
        } else {
            $iFiles = $aConfig['default']['files_no'];
        }
        if ($iFiles < $aConfig['default']['files_no']) {
            $iFiles = $aConfig['default']['files_no'];
        }
        if ($bAdd) {
            // wybranmo dodanie kolejnego zdjecia
            $iFiles += $aConfig['default']['files_no'];
        }

        $sHeader = sprintf($aConfig['lang'][$this->sModule]['files_header'], getPagePath($iPId, $this->iLangId, true), $this->getItemName($iId));

        $pForm = new FormTable('files', $sHeader, array('action' => phpSelf(array('pid' => $iPId, 'id' => $iId)), 'enctype' => 'multipart/form-data'), array('col_width' => 135), $aConfig['common']['js_validation']);
        $pForm->AddHidden('do', 'update_files');

        // maksymalna suma rozmiarow przesylanych plikow
        $pForm->AddRow($aConfig['lang']['common']['max_files_upload_size'], getMaxUploadSize(), '', array(), array('class' => 'redText'));

        // dopuszczalne formaty plikow
        $pForm->AddRow($aConfig['lang']['common']['files_available_exts'], implode(', ', $aConfig['common']['product_file_extensions']), '', array(), array('class' => 'redText'));

        $aFileTypes = $this->getFileTypes();

        for ($i = 0; $i < $iFiles; $i++) {
            if (isset($aFiles[$i])) {
                $pForm->AddSelect('filetype[' . $i . '_' . $aFiles[$i]['id'] . ']', $aConfig['lang'][$this->sModule]['filetype'], array(), $aFileTypes, $aFiles[$i]['ftype'], '', false);
                $pForm->AddProductAttachment('file[' . $i . '_' . $aFiles[$i]['id'] . ']', $aConfig['lang'][$this->sModule]['file'], $iId . '/' . $aFiles[$i]['filename']);
            } else {
                $pForm->AddSelect('filetype[' . $i . '_0]', $aConfig['lang'][$this->sModule]['filetype'], array(), $aFileTypes, $aFiles[$i]['ftype'], '', false);
                $pForm->AddProductAttachment('file[' . $i . '_0]', $aConfig['lang'][$this->sModule]['file']);
            }
        }

        $pForm->AddRow('&nbsp;', '<div class="existingPhoto">' . $pForm->GetInputButtonHTML('add_file', $aConfig['lang']['common']['button_add_file'], array('onclick' => 'GetObject(\'do\').value=\'add_file\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button') . '</div>');

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array(), array('id', 'pid')) . '\');'), 'button'));

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
    }

// end of AddEditFiles() function

    /**
     * Metoda pobiera liste pliko produktu
     *
     * @param    integer $iId - id pliku
     * @return    array ref
     */
    function &getFiles($iId)
    {
        global $aConfig;

        // pobranie plikow produktu
        $sSql = "SELECT id, ftype, filename
						 FROM " . $aConfig['tabls']['prefix'] . "products_files
						 WHERE product_id = " . $iId . "
					 	 ORDER BY ftype";
        return Common::GetAll($sSql);
    }

// end of getFiles() method

    /**
     * Metoda usuwa plik z bazy i dysku
     *
     * @param    integer $iId - Id produktu
     * @param    integer $key - klucz
     * @param    string $sFile - nazwa pliku
     * @return    bool
     */
    function deleteFile($iId, $sKey, $sFile)
    {
        global $aConfig;
        $aKey = explode('_', $sKey, 2);
        if (!empty($aKey[1])) {
            $sSql = "DELETE FROM " . $aConfig['tabls']['prefix'] . "products_files
							 WHERE product_id = " . $iId . " AND
							 			 id = " . $aKey[1];
            if (Common::Query($sSql) !== false) {
                // usuwanie pliku z dysku
                unlink($aConfig['common']['client_base_path'] . $aConfig['common']['product_file_dir'] . '/' . $iId . '/' . $sFile);
                return true;
            }
        }
        return false;
    }

// end of deleteFile() method

    /**
     * Metoda przeosi przeslany plik do katalgu docelowego
     * oraz dodaje informacje do bazy danych
     *
     * @param    integer $iId - Id produktu
     * @param    integer $iKey - klucz w tablicy files
     * @param    array ref    $aFile    - dane pliku
     * @param    object ref    $oFile    - obiekt klasy MyFile
     * @return    integer    -5: nieprawidlowe rozszerzeie pliku
     *                                    -4: podana sciezka nie wskazuje na katalog
     *                  -3: katalog nie jest zapisywalny
     *                  -2: plik o podanej nazwie juz istnieje
     *                  -1: proba przeniesienia do katalogu docelowego nie powiodla sie
     *                   0: blad
     *                                     1: plik zostal przeniesiony do katalogu docelowego
     */
    function addFile($iId, $iKey, &$aFile, &$oFile, $bOverwrite = false)
    {
        global $aConfig;
        if (!is_dir($aConfig['common']['client_base_path'] . $aConfig['common']['product_file_dir'] . '/' . $iId)) {
            @mkdir($aConfig['common']['client_base_path'] . $aConfig['common']['product_file_dir'] . '/' . $iId);
        }
        $aConfig['common']['client_base_path'] . $aConfig['common']['product_file_dir'] . '/' . $iId;
        $oFile->SetFile($aConfig['common']['client_base_path'] . $aConfig['common']['product_file_dir'] . '/' . $iId, $aFile, $iKey);
        if (($iResult = $oFile->proceedFile($aConfig['common']['product_file_extensions'], false, $bOverwrite)) == 1) {
            // plik przeniesiono
            $aId = explode('_', $iKey, 2);
            if ($aId[1] == '0') {
                // dodanie informacji do bazy danych
                $aValues = array(
                    'product_id' => $iId,
                    'ftype' => $_POST['filetype'][$iKey],
                    'filename' => $aFile['name'][$iKey]
                );
                if (Common::Insert($aConfig['tabls']['prefix'] . "products_files", $aValues, "", false) === false) {
                    // jakis blad - usuniecie przeniesionego pliku
                    @unlink($aConfig['common']['client_base_path'] . $aConfig['common']['product_file_dir'] . '/' . $iId . '/' . $aFile['name'][$iKey]);
                    return 0;
                }
            } else {
                // dodanie informacji do bazy danych
                $aValues = array(
                    'ftype' => $_POST['filetype'][$iKey],
                    'filename' => $aFile['name'][$iKey]
                );
                if (Common::Update($aConfig['tabls']['prefix'] . "products_files", $aValues, "id = " . $aId[1]) === false) {
                    // jakis blad - usuniecie przeniesionego pliku
                    @unlink($aConfig['common']['client_base_path'] . $aConfig['common']['product_file_dir'] . '/' . $iId . '/' . $aFile['name'][$iKey]);
                    return 0;
                }
            }
        }
        return $iResult;
    }

// end of addFile() method
    /**
     * Metoda pobiera autorow produktu o podanym id
     *
     * @param    intger $iId - Id produktu
     * @return    array    - autorzy produktu
     */

    function getAuthors($iId)
    {
        global $aConfig;
        // pobranie autorow ksiazki
        $sSql = "SELECT B.name AS role_name, CONCAT(C.surname,' ',C.name) AS author
					FROM " . $aConfig['tabls']['prefix'] . "products_to_authors A
					JOIN " . $aConfig['tabls']['prefix'] . "products_authors_roles B
					ON B.id = A.role_id
					JOIN " . $aConfig['tabls']['prefix'] . "products_authors C
					ON C.id = A.author_id
					WHERE A.product_id = " . $iId . "
					ORDER BY B.order_by";

        $aResult = &Common::GetAll($sSql);
        $sAuthors = '';

        foreach ($aResult as $aAuthor) {
            $sAuthors .= $aAuthor['author'] . ', ';
        }
        return substr($sAuthors, 0, -2);
    }


    /**
     * Metoda sprawdza status produktu, Nie akceptowane produkty nieopublikowane, niedostepne, nakład wyczerpany.
     *
     * @param type $iProdId
     */
    function checkProductStatus($iProdId)
    {
        global $aConfig;

        $sSql = "SELECT id FROM " . $aConfig['tabls']['prefix'] . "products
						 WHERE id=" . $iProdId . " AND published='1' AND prod_status<>'2'  AND prod_status<>'0' ";
        return Common::GetOne($sSql) > 0;
    }// end of checkProductStatus() method

    function validateBookstoresFields($aData, $aSites, $oProductContext) {
        $aErrors = [];

        foreach ($aSites as $aSite) {
            $bEmpty = $oProductContext->isSiteEmpty($aSite, $aData);
            if ($bEmpty === false) {
                if ( $aData[$aSite['code'] . '_' . 'name'] == '' ) {
                    $aErrors[] = $aSite['name'] . ' ' . _('Nazwa');
                }
                if ( $aData[$aSite['code'] . '_' . 'description'] == '' ) {
                    $aErrors[] = $aSite['name'] . ' ' . _('Pełny opis');
                }
            }
        }
        return $aErrors;
    }
}

// end of Module Class
?>
