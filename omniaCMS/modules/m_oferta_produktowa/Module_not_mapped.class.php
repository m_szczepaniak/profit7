<?php
/**
 * Klasa Module do obslugi modulu 'Oferta produktowa' - rabat ogolny
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// dane konfiguracyjne
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		$this->Show($pSmarty); 
	} // end of Module() method



	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		$sAbeMappings= "<ul>";
		$sSql = "SELECT DISTINCT A.id, A.name, A.parent_id
							FROM products_abe_categories A
							LEFT JOIN products_abe_mapping B ON B.abe_category = A.id
							WHERE B.page_id IS NULL";
	
		$aCats = Common::GetAll($sSql);
		foreach($aCats as $aCat) {
			$sParent='';
			// znalezienie rodzica
			if(isset($aCat['parent_id']) && !empty($aCat['parent_id'])){
				$sParent = $this->getABEParentPath($aCat['parent_id']);
			}
			$sAbeMappings .= "<li>".$sParent.$aCat['name']."</li>";
		}
		$sAbeMappings .= "</ul>";
	

		$sAzymutMappings = "<ul>";
		$sSql = "SELECT DISTINCT A.id, A.name, A.parent_id
							FROM products_azymut_categories A
							LEFT JOIN products_azymut_mapping B ON B.azymut_category = A.id
							WHERE B.page_id IS NULL";
		$aCats = Common::GetAll($sSql);
		foreach($aCats as $aCat) {
			$sParent='';
			// znalezienie rodzica
			if(isset($aCat['parent_id']) && !empty($aCat['parent_id'])){
				$sParent = $this->getAzymutParentPath($aCat['parent_id']);
			}
			$sAzymutMappings .= "<li>".$sParent.$aCat['name']."</li>";
		}
		$sAzymutMappings .= "</ul>";
		
		
		
		$sHelionMappings = "<ul>";
		$sSql = "SELECT DISTINCT A.id, A.name, A.parent_id
							FROM products_helion_categories A
							LEFT JOIN products_helion_mapping B 
								ON B.helion_id = A.id AND A.source = B.source
							WHERE B.page_id IS NULL";
		$aCats = Common::GetAll($sSql);
		foreach($aCats as $aCat) {
			$sParent='';
			// znalezienie rodzica
			if(isset($aCat['parent_id']) && !empty($aCat['parent_id'])){
				$sParent = $this->getAzymutParentPath($aCat['parent_id']);
			}
			$sHelionMappings .= "<li>".$sParent.$aCat['name']."</li>";
		}
		$sHelionMappings .= "</ul>";
		

		$pForm = new FormTable('not_mapped', $aConfig['lang'][$this->sModule]['header'], array(), array('col_width'=>150),false);

		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['abe_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		$pForm->AddRow($aConfig['lang'][$this->sModule]['not_mapped'],$sAbeMappings);

		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['azymut_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		$pForm->AddRow($aConfig['lang'][$this->sModule]['not_mapped'],$sAzymutMappings);
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['helion_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		$pForm->AddRow($aConfig['lang'][$this->sModule]['not_mapped'],$sHelionMappings);

		
		$this->ShowSources($pForm);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm($pForm)));
	} // end of AddEdit() function
	
	function getAzymutParentPath($iId) {
		global $aConfig;
		
		$sSql="SELECT name, parent_id
					FROM ".$aConfig['tabls']['prefix']."products_azymut_categories
					WHERE id = ".$iId;
		$aItem = Common::GetRow($sSql);
		return ((!empty($aItem['parent_id'])&&($aItem['parent_id']!=$iId))?$this->getAzymutParentPath($aItem['parent_id']):'').$aItem['name'].' / ';
	}
	
	function getABEParentPath($iId) {
		global $aConfig;
		
		$sSql="SELECT name, parent_id
					FROM ".$aConfig['tabls']['prefix']."products_azymut_categories
					WHERE id = ".$iId;
		$aItem = Common::GetRow($sSql);
		return $aItem['name'].' / ';
	}
	
	
	function getHelionParentPath($iId) {
		global $aConfig;
		
		$sSql="SELECT name, parent_id
					FROM ".$aConfig['tabls']['prefix']."products_helion_categories
					WHERE id = ".$iId;
		$aItem = Common::GetRow($sSql);
		return $aItem['name'].' / ';
	}

	
	/**
	 * Metoda pobiera źródła
	 *
	 * @global array $aConfig
	 * @param object $pSmarty
	 */
	function ShowSources(&$pForm) {
		global $aConfig;
		
		// pobierz źródła
		$aSources = $this->getSources();
		foreach ($aSources as $aSource) {
			$sSMapingsMappings = $this->unMappedCategories($aSource['id']);
			
			$pForm->AddMergedRow($aSource['symbol'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
			$pForm->AddRow($aConfig['lang'][$this->sModule]['not_mapped'], $sSMapingsMappings);
		}
	} // end of AddEdit() function
	
	
	/**
	 * Metoda pobiera niezmapowane kategorie dla źródła
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $sSourceId
	 * @param type $sSymbol
	 * @return string 
	 */
	function unMappedCategories($sSourceId) {
		global $aConfig, $pDbMgr;
		$sStrAll = '';
		$aCats = array();
		
		$this->getCatsSource($aCats, $sSourceId);
		if (empty($aCats)) return '';
		
		$sSql = "SELECT A.source_category_id FROM ".$aConfig['tabls']['prefix']."menus_items_mappings AS A
						 JOIN menus_items_mappings_categories AS B
						 ON A.source_id = B.source_id AND A.source_category_id = B.source_category_id
						 WHERE A.source_id=".$sSourceId." AND A.source_category_id IN (".implode(', ', array_keys($aCats)).")
						 ORDER BY B.name ASC";
		$aComparedIds = $pDbMgr->GetCol('profit24', $sSql);

		// mamy wszystkie kategorie teraz należy porównać z kategoriami które istnieją w bazie danych i wybrać te których nie ma
		foreach ($aCats as $iKey => $aCat) {
			if (in_array($iKey, $aComparedIds) == false){
				// nie istnieje
				if ($sSourceId == '7') {
					$sStr = '('.$iKey.')'.$aCat['name'];
					$this->getPathArr($sStr, $aCats, $aCat['parent_id'], '');
					$sStrAll .= $sStr.' <br />';
				} else {
					$sStr = '('.$iKey.') / '.$aCat['name'];
					$this->getPathArr($sStr, $aCats, $aCat['parent_id'], ' / ');
					$aPaths = explode(' / ', $sStr);
					$aPaths = array_reverse($aPaths);
					$sStrAll .= implode(' / ', $aPaths).' <br />';
				}
			}
		}
		
		return $sStrAll;
	}// end of unMappedCategories() method
	
	
	/**
	 * Rekurencyjna metoda pobierająca path pliku
	 *
	 * @param string $sStr
	 * @param type $aArr
	 * @param type $iId
	 * @return string 
	 */
	function getPathArr(&$sStr, $aArr, $iId, $sSep) {
		
		if (isset($aArr[$iId]) && !empty($aArr[$iId]) && is_array($aArr[$iId])) {
			$sStr .= $sSep.$aArr[$iId]['name'];
			$this->getPathArr($sStr, $aArr, $aArr[$iId]['parent_id'], $sSep);
		}
		
		return $sStr;
	}// end of getPathArr() method
	
	
	/**
	 * Metoda pobiera wszystkie kategorie z Pr24
	 *
	 * @global type $aConfig
	 * @global type $pDbMgr
	 * @param type $aCats
	 * @param type $iParentId
	 * @return type 
	 */
	function getCatsSource(&$aCats, $sSourceId, $iParentId = null) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT source_category_id AS id_cat, source_category_id AS id, source_category_parent_id AS parent_id, name 
						 FROM ".$aConfig['tabls']['prefix']."menus_items_mappings_categories
						 WHERE source_id = ".$sSourceId." AND ".
									($iParentId !== null ? "source_category_parent_id=".$iParentId : ' (source_category_parent_id IS NULL OR source_category_parent_id = 0)')."
						 ORDER BY name ASC";
		$aItems = $pDbMgr->GetAssoc('profit24', $sSql, false, array(), DB_FETCHMODE_ASSOC, false);
		if (!empty($aItems) && is_array($aItems)) {
			$aCats = $aCats + $aItems;
			foreach ($aItems as $iKey => $aVal) {
				$this->getCatsSource($aCats, $sSourceId, $aVal['id']);
			}
		}
		return $aCats;
	}// end of getCatsSource() method
	
	
	/**
	 * Metoda pobiera dostępne źródła
	 *
	 * @global type $aConfig
	 * @return type 
	 */
	function getSources() {
		global $aConfig;
		
		$sSql = "SELECT id, symbol FROM ".$aConfig['tabls']['prefix']."sources
						 WHERE id <> 1 AND id <> 4";
		return Common::GetAll($sSql);
	}// end of getSources() method
	
	
	/**
	 * Metoda pobiera brakujące mapowania dla źródła
	 *
	 * @global type $aConfig
	 * @param type $iId
	 * @return type 
	 */
	function getSourceParentPath($iId) {
		global $aConfig;
		
		$sSql="SELECT page_id, source_id, source_category_id
					FROM ".$aConfig['tabls']['prefix']."menus_items_mappings
					WHERE id = ".$iId;
		$aItem = Common::GetRow($sSql);
		return ((!empty($aItem['parent_id'])&&($aItem['parent_id']!=$iId))?$this->getSourceParentPath($aItem['parent_id']):'').$aItem['name'].' / ';
	}
} // end of Module Class
?>