<?php
namespace omniaCMS\modules\m_oferta_produktowa;

use Admin;
use FormTable;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use View;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 26.09.16
 * Time: 08:47
 */
class Module__oferta_produktowa__publishers_to_groups implements ModuleEntity, SingleView
{

    /**
     *
     * @var string
     */
    private $sMsg;

    /**
     *
     * @var string
     */
    private $sModule;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    public function __construct(Admin $oClassRepository)
    {

        $this->oClassRepository = $oClassRepository;
        $this->em = $this->oClassRepository->entityManager;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function getMsg()
    {
        return $this->sMsg;
    }

    public function doDefault()
    {
        global $aConfig;
        $aData = array();

        if (!empty($_POST)) {
            $aData =& $_POST;
        }
        if ($_GET['ppid'] > 0) {
            $iId = $_GET['ppid'];
        }


        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aData = $this->pDbMgr->getTableRow('products_publishers_groups', $iId, ['name']);
        $aHeader = array(
            'header' => sprintf(_('Wydawnictwa w grupie %s'), $aData['name']),
            'refresh' => true,
            'search' => true,
            'items_per_page' => 20,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 2,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'name',
                'content' => _('Nazwa'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '40'
            )
        );

        $aColSettings = array(
            'id' => array(
                'show' => false
            ),
            'action' => array(
                'actions' => array('delete'),
                'params' => array(
                    'delete' => array('id' => '{id}'),
                ),
            )
        );


        include_once('View/View.class.php');
        $oView = new View(_('publishers_groups'), $aHeader, $aAttribs);

        $aCols = array('PPTG.id', 'PP.name'
        );

        $sSql = 'SELECT %cols
             FROM products_publishers_to_groups AS PPTG
             JOIN products_publishers AS PP
              ON PPTG.publisher_id = PP.id
             WHERE
              PPTG.group_id = '.$iId.'
              %filters
             ';
        $sGroupBySQL = 'GROUP BY PPTG.id';
        $aSearchCols = array('PP.name');
        $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, '', $sGroupBySQL, ' PPTG.id ASC ');
        $oView->AddRecordsHeader($aRecordsHeader);
        $oView->setActionColSettings($aColSettings);

        $aRecordsFooterParams = array(
            'go_back' => array(0 => ['action' => 'publishers_groups'], 1 => array('id', 'ppid')),
        );
        $aRecordsFooter = array(array('go_back'), array('add'));
        $oView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
        return ShowTable($oView->Show());
    }


    /**
     *
     * @return string
     */
    public function doDelete()
    {

        $iId = $_GET['id'];
        if ($iId > 0) {

            $sSql = 'DELETE FROM products_publishers_to_groups WHERE id = ' . $iId . ' LIMIT 1';
            if ($this->pDbMgr->Query('profit24', $sSql) === false) {
                $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania wydawnictwa z grupy'));
                return $this->doDefault();
            } else {
                $this->sMsg .= GetMessage(_('Wydawnictwo grupy zostało usunięte'), false);
                return $this->doDefault();
            }
        } else {
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania wydawnictwa z grupy'));
            return $this->doDefault();
        }
    }

    public function parseViewItem(array $aItem)
    {
        return $aItem;
    }

    public function resetViewState()
    {
        resetViewState();
    }

    public function doAdd()
    {
        global $aConfig;

        $aData = array();
        if (!empty($_POST)) {
            $aData = $_POST;
        }
        include_once('lib/Form/FormTable.class.php');
        $pForm = new FormTable('add_publisher_to_group', _('Dodaj wydawnictwo grupy'));
        $pForm->AddHidden('do', 'insert');
        $pForm->AddText('publisher', _('Wydawnictwo'), '', ['id' => 'publisher_aci']);

        $pForm->AddRow('&nbsp;',
            $pForm->GetInputButtonHTML('send', _('Zapisz')) . '&nbsp;&nbsp;' .
            $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'],
                array('onclick' => 'MenuNavigate(\'' . phpSelf() . '\');'), 'button'));

        $sJS = '
		<script type="text/javascript">
            $(document).ready(function(){
              $("#publisher_aci").autocomplete({
                        source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetPublishers.php"
              });
            });
		</script>
		';
        return $pForm->ShowForm().$sJS;
    }


    public function doInsert()
    {
        if (!empty($_POST)) {
            $aData = $_POST;
        } else {
            return $this->doDefault();
        }

        include_once($_SERVER['DOCUMENT_ROOT'].'import/CommonSynchro.class.php');
        $oCommonSynchro = new \CommonSynchro();

        $aValues = [
            'publisher_id' => $oCommonSynchro->getPublisherId($_POST['publisher']),
            'group_id' => $_GET['ppid']
        ];
        if ($this->pDbMgr->Insert('profit24', 'products_publishers_to_groups', $aValues) === false) {
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas dodawania wydawnictwa do grupy'));
            return $this->doAdd();
        } else {
            $this->sMsg .= GetMessage(_('Wydawnictwo zostało dodane do grupy'), false);
        }
        return $this->doDefault();
    }
}