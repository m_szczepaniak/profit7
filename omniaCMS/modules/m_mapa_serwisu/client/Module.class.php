<?php
/**
 * Klasa odule do obslugi stron modulu 'Mapa serwisu'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // Id strony
  var $iPageId;

  // symbol modulu
  var $sModule;

  // ustawienia strony aktualnosci
  var $aSettings;

  // katalog z szblonami
  var $sTemplatesDir;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;

		$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->aSettings =& $this->getSettings();
		$this->sTemplatesDir = 'modules/'.$aConfig['_tmp']['page']['module'];
	} // end Module() function


	/**
	 * Metoda pobiera konfiguracje dla strony opisowej
	 *
	 * @return	array
	 */
	function getSettings() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aCfg = unserialize($oCache->get(md5($aConfig['_tmp']['page']['symbol']), 'modules_settings')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT A.menu_id, A.template, B.name
							 FROM ".$aConfig['tabls']['prefix']."sitemap_settings AS A
							 			JOIN ".$aConfig['tabls']['prefix']."menus AS B
							 			ON B.id = A.menu_id
							 WHERE A.page_id = ".$this->iPageId."
							 ORDER BY A.id";
			$aCfg = Common::GetAll($sSql);
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aCfg), md5($aConfig['_tmp']['page']['symbol']), 'modules_settings');
		}
		return $aCfg;
	} // end of getSettings() function


	/**
	 * Metoda pobiera tresc strony o id $this->iPageId
	 * dowiazuje ja do Smartow oraz zwraca xHTML z trescia strony po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty, $oCache;
		$sHtml = '';
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['name'] =& $aConfig['_tmp']['page']['name'];
		
		
		if (!$aConfig['common']['caching'] || false === ($sHtml = $oCache->get(md5($aConfig['_tmp']['page']['symbol']), 'modules'))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie i przetworzenie przez szablon listy autorow
			foreach ($this->aSettings as $aMenu) {
				$aModule['sub_template'] = $this->sTemplatesDir.'/_'.$aMenu['template'];
				// pobranie stron menu
				$aModule['menu_name'] =& $aMenu['name'];
				$aModule['menu'] =& $this->getMenuTree($aMenu['menu_id']);
	
				if (!empty($aModule['menu'])) {
					$pSmarty->assign_by_ref('aModule', $aModule);
					$sHtml .= $pSmarty->fetch($this->sTemplatesDir.'/'.$aMenu['template']);
					$pSmarty->clear_assign('aModule');
				}
			}
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save($sHtml, md5($aConfig['_tmp']['page']['symbol']), 'modules');
		}
		if (trim($sHtml) == '') {
			setMessage($aModule['lang']['no_data']);
		}		
		return $sHtml;
	} // end of getContent()


	/**
	 * Metoda zwraca drzewo menu dla mapy strony
	 *
	 * @param	integer	$iMId	- Id menu
	 * @param	integer	$mRId	- Id korzenia
	 * @return	array ref	- drzewo menu
	 */
	function &getMenuTree($iMId, $mRId=0) {
		global $aConfig;

		// pobranie podstron
		$aItems =& $this->getMenuLeaf($iMId, $mRId);
		foreach ($aItems as $iKey => $aItem) {
			$aItems[$iKey]['link'] = getPageLink($aItem);
			// pobranie podstron
			$aItems[$iKey]['children'] =& $this->getMenuTree($iMId, $aItem['id']);
			if (empty($aItems[$iKey]['children'])) {
				unset($aItems[$iKey]['children']);
			}
		}
		return $aItems;
	} // end of getMenuTree() method


	/**
	 * Metoda pobiera podstrony jednej galezi menu
	 *
	 * @param	integer	$iMId	- Id menu
	 * @param	integer	$mRId	- Id korzenia
	 * @return	array ref	- galaz menu
	 */
	function &getMenuLeaf($iMId, $mRId) {
		global $aConfig;

		$sSql = "SELECT id, symbol, name, url, link_to_id, mtype, new_window
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$_GET['lang_id']." AND
						 			 menu_id = ".$iMId." AND
						 			 published = '1' AND
						 			 parent_id ".($mRId == 0 ? 'IS NULL' : '= '.$mRId)."
						 ORDER BY order_by";
		return Common::GetAll($sSql);
	} // end of getMenuLeaf() method
} // end of Module Class
?>