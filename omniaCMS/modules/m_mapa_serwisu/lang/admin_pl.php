<?php
/**
* Plik jezykowy dla interfejsu modulu 'Mapa serwisu' Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

/*---------- ustawienia mapy serwisu */
$aConfig['lang']['m_mapa_serwisu_settings']['template'] = "Szablon strony";
$aConfig['lang']['m_mapa_serwisu_settings']['menus'] = "Lista menu i ich szablonów mapy strony";
$aConfig['lang']['m_mapa_serwisu_settings']['exclude'] = "Nie uwzględniaj";
$aConfig['lang']['m_mapa_serwisu_settings']['include_invisible_items'] = 'Wyświetlaj strony niewidoczne w menu';
$aConfig['lang']['m_mapa_serwisu_settings']['templates'] = 'Szablony menu';
?>
