<?php
/**
* Plik jezykowy modulu 'MAPA SERWISU'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/

$aConfig['lang']['mod_m_mapa_serwisu']['no_data']	= 'Nie wybrano żadnego menu dla którego ma być wyświetlana mapa serwisu.';
?>