<?php
/**
 * Klasa Module do obslugi konfiguracji stron modulu 'Mapa serwisu'
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Settings {

  // nazwa modulu - do langow
	var $sModule;

	// Id strony dla ktorej edytowane sa ustawienia
	var $iId;

	// ID wersji jez.
	var $iLangId;

	// sciezka do katalogu zawierajacego szablony czesci klienckiej modulu
	var $sTemplatesDir;

	// lista szablonow czesci klienckiej modulu
	var $sTemplatesList;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$sModule, &$iId) {
		global $aConfig;
		$this->sModule = $sModule;
		$this->iId = $iId;
		$this->iLangId = intval($_SESSION['lang']['id']);
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/modules/'.$this->sModule;
		$this->sTemplatesList =& GetTemplatesList($this->sTemplatesDir);

		// pobranie konfiguracji dla modulu
		$this->setSettings();
	} // end Settings() function


	/**
	 * Metoda pobiera konfiguracje dla strony modulu
	 *
	 * @return	void
	 */
	function setSettings() {
		global $aConfig;
		$aCfg = array();

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."sitemap_settings
						 WHERE page_id = ".$this->iId."
						 ORDER BY id";
		$aCfg = Common::GetAll($sSql);

		$i = 1;
		foreach ($aCfg as $iKey => $aRow) {
			$aCfg['menus'][$i] = $aRow['menu_id'];
			$aCfg['templates'][$i++] = $aRow['template'];
			unset($aCfg[$iKey]);
		}

		// dolaczenie ustawien do glownej tablicy ustawien $aConfig
		$aConfig['settings'][$this->sModule] =& $aCfg;
	} // end of setSettings() method


	/**
	 * Metoda wprowadza zmiany w ustawieniach aktualnosci
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;

		// usuniecie starych ustawien
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."sitemap_settings
						 WHERE page_id = ".$this->iId;
		if (Common::Query($sSql) === false) {
			return false;
		}
		// dodanie ustawien
		foreach ($_POST['menus'] as $iKey => $iMenuId) {
			if ($iMenuId != 0) {
				$aValues = array(
					'page_id' => $this->iId,
					'menu_id' => $iMenuId,
					'template' => $_POST['templates'][$iKey]
				);

				if (Common::Insert($aConfig['tabls']['prefix']."sitemap_settings",
													 $aValues) === false) {
					return false;
				}
			}
		}
		return true;
	} // end of UpdateSettings() funciton


	/**
	 * Metoda tworzy formularz edycji konfiguracji aktualnosci
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;

		$aData = array();
		$sMenus = '';
		$sTemplates = '';

		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_mtype') {
			$aData =& $_POST;
		}

		// pobranie listy menu serwisu
		$sSql = "SELECT id AS value, name AS label
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id = ".$this->iLangId."
						 ORDER BY id";
		$aMenus =& Common::GetAll($sSql);

		$aSelectMenus = array_merge(array(array('value' => '0', 'label' => $aConfig['lang'][$this->sModule.'_settings']['exclude'])), $aMenus);

		$sMenus = '<ul class="list">';
		$sTemplates = '<ul class="list">';

		for ($i = 1; $i <= count($aMenus); $i++) {
			$sMenus .= '<li>'.$pForm->GetSelectHTML('menus['.$i.']', '', array(), $aSelectMenus, $aData['menus'][$i], '', false).'</li>';
			$sTemplates .= '<li>'.$pForm->GetSelectHTML('templates['.$i.']', '', array(), $this->sTemplatesList, $aData['templates'][$i], '', false).'</li>';
		}
		$sMenus .= '</ul>';
		$sTemplates .= '</ul>';

		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['settings'], array('class'=>'merged', 'style'=>'padding-left: 218px'));

		// menu oraz szablony mapy strony
		$pForm->AddRow($aConfig['lang'][$this->sModule.'_settings']['menus'], $sMenus.$sTemplates);
	} // end of SettingsForm() function
} // end of Settings Class
?>