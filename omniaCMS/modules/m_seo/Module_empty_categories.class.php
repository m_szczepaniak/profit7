<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 25.04.17
 * Time: 14:24
 */

use omniaCMS\modules\m_stats\CompletationItems;
use omniaCMS\modules\m_stats\StatsListsTypes;

class Module__seo__empty_categories
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    private $pDbMgr;

    function __construct($pSmarty)
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;


        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        if (isset($_GET['pid'])) {
            $iPId = intval($_GET['pid']);
        }
        $this->aPrivileges =& $_SESSION['user']['privileges'];
        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }
        $sHtml = $this->Show($pSmarty);
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sHtml);
    }

    private function Show($pSmarty)
    {
        global $aConfig;

        if (empty($_POST['f_website_id'])) {
            $_POST['f_website_id'] = '1';
        }

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Nieopisane kategorie')),
            'refresh' => true,
            'search' => false,
            'checkboxes' => false,
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'name',
                'content' => _('Kategoria'),
                'sortable' => true,
                'width' => '180'
            ),


        );

        $pView = new \View('description_dedicated', $aHeader, $aAttribs);
        // pobranie liczby wszystkich uzytkownikow
        $aRecords = $this->getRecords();
        $iRowCount = count($aRecords);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'name_base' => array(
                    'show' => false
                ),
                'name' => array(
                    'link' => phpSelf(array('do' => 'add', 'table_destination' => 'menus_items', 'name' => '{name_base}', 'website_id' => $_POST['f_website_id']), ['action'])
                ),
                'action' => array(
                    'show' => false,
                ),
            );

            foreach ($aRecords as $key => $record) {
                $name = $record['name'] . ' (' . $record['id'] . ')';
                $aRecords[$key]['name_base'] = urlencode(htmlentities($name));
            }
            $pView->AddRecords($aRecords, $aColSettings);
        }

        // filtr aktywosci konta
        $aWebsites = $this->getWebsites();
        $pView->AddFilter('f_website_id', _('Serwis'), $aWebsites, $_POST['f_website_id']);

        $options = [
            [
                'label' => 'Ukryj',
                'value' => ''
            ],
            [
                'label' => 'Pokaż',
                'value' => '1'
            ],
        ];
        $pView->AddFilter('f_empty_in_menus_items', _('Czy ukryć jeśli jest opis w struktura menu'), $options, $_POST['f_empty_in_menus_items']);

        $pView->AddRecordsHeader($aRecordsHeader);
        $aRecordsFooter = array(array(), array('add'));
        $pView->AddRecordsFooter($aRecordsFooter);
        return $pView->Show();
    }

    /**
     * @return array
     */
    private function getWebsites()
    {

        $sSql = 'SELECT id AS value, name AS label FROM websites';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @return array
     */
    private function getRecords()
    {
        $sSql = 'SELECT code FROM websites WHERE id = ' . $_POST['f_website_id'];
        $websiteCode = $this->pDbMgr->GetOne('profit24', $sSql);

        $sSql = 'SELECT DD.dest_id
                 FROM description_dedicated AS DD
                 WHERE 1=1
                 AND DD.table_destination = "menus_items"
                 ' . (isset($_POST['f_website_id']) ? ' AND DD.website_id = "' . $_POST['f_website_id'] . '"' : '') . '
                 ' . (isset($_POST['f_search']) ? ' AND name LIKE "%' . $_POST['f_search'] . '%"' : '') . '
                 ORDER BY DD.id DESC';
        $aDescriptionDedicated = $this->pDbMgr->GetCol('profit24', $sSql);

        if (empty($_POST['f_empty_in_menus_items'])) {
            $andWhere = ' AND (MI.description IS NULL OR MI.description = "") ';
        }

        $sSql = 'SELECT MI.id, CONCAT(IFNULL(MI_PARENT.name, " -- "), " > ", MI.name) as name 
                 FROM menus_items AS MI
                 LEFT JOIN menus_items AS MI_PARENT
                  ON MI_PARENT.id = MI.parent_id
                 JOIN modules AS M
                  ON M.id = MI.module_id AND M.symbol = "m_oferta_produktowa"
                  WHERE MI.id NOT IN ("' . implode('", "', $aDescriptionDedicated) . '")
                  ' . $andWhere . '
                  ORDER BY MI.name ASC
                 ';
        return $this->pDbMgr->GetAll($websiteCode, $sSql);
    }
}