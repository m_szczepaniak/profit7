<?php

/**
 * Skrypt statystyk serwisu.
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-10
 * @copyrights Marcin Chudy - Profit24.pl
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_USER_NOTICE);
ini_set('display_errors', 'On');

class Module
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    private $pDbMgr;

    /**
     *
     * @global DatabaseManager $pDbMgr
     * @param Smarty $pSmarty
     * @return void
     */
    function __construct($pSmarty)
    {
        global $pDbMgr;

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        $sDo = '';
        $iId = 0;
        $iPId = 0;


        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        $this->pDbMgr = $pDbMgr;


        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }


        if (isset($_GET['id'])) {
            $iId = $_GET['id'];
        }

        $this->aPrivileges =& $_SESSION['user']['privileges'];
        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }
        switch ($sDo) {
            case 'delete':
                $resultHTML = $this->Delete($pSmarty, $iId);
                break;
            case 'insert':
                $resultHTML = $this->Insert($pSmarty);
                break;
            case 'update':
                $resultHTML = $this->Update($pSmarty, $iId);
                break;
            case 'add':
                $resultHTML = $this->AddEdit($pSmarty);
                break;
            case 'edit':
                $resultHTML = $this->AddEdit($pSmarty, $iId);
                break;
            default:
                $resultHTML = $this->Show($pSmarty);
                break;
        }
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $resultHTML);
    }

    private function Show($pSmarty)
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Opisy')),
            'refresh' => true,
            'search' => false,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'id',
                'content' => _('Serwis'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'id_dest',
                'content' => _('Id docelowe'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'entity',
                'content' => _('Tabela'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'name',
                'content' => _('Nazwa'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'description',
                'content' => _('Opis'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'created_by',
                'content' => _('przez'),
                'sortable' => true,
                'width' => '180'
            ),

            array(
                'db_field' => 'modified',
                'content' => _('Zmodyfikowano'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'modified_by',
                'content' => _('przez'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '55'
            )

        );

        $pView = new \View('description_dedicated', $aHeader, $aAttribs);
        // pobranie liczby wszystkich uzytkownikow
        $aRecords = $this->getRecords();
        $iRowCount = count($aRecords);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'name' => array(
                    'link' => phpSelf(array('do' => 'edit', 'id' => '{id}'))
                ),
                'action' => array(
                    'actions' => array('edit', 'delete'),
                    'params' => array(
                        'edit' => array('id' => '{id}'),
                        'delete' => array('id' => '{id}')
                    ),
                    'show' => false,
                )
            );

            foreach ($aRecords as $key => $record) {
                $aRecords[$key]['description'] = trimString(strip_tags($record['description']), 120);
            }
            $pView->AddRecords($aRecords, $aColSettings);
        }
        $pView->AddRecordsHeader($aRecordsHeader);
        $aRecordsFooter = array(array(), array('add'));
        $pView->AddRecordsFooter($aRecordsFooter);
        return $pView->Show();
    }


    private function getWebsites()
    {

        $sSql = 'SELECT name AS label, id AS value
                 FROM websites';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @param $pSmarty
     * @param int $iId
     * @return string
     */
    private function AddEdit($pSmarty, $iId = 0)
    {
        global $aConfig;

        $aData = [];
        if (isset($_POST) && !empty($_POST)) {
            $aData = $_POST;
        }
        else if (isset($_GET) && !empty($_GET) && isset($_GET['name'])) {
            $aData = $_GET;
            if (isset($_GET['name']) && !empty($_GET['name'])) {
                $aData['name'] = urldecode(html_entity_decode($_GET['name']));
            }
        }
        if ($iId > 0 && empty($aData)) {
            $aData = $this->getRecord($iId);
        }

        include_once('Form/FormTable.class.php');
        $pForm = new \FormTable('description_dedicated', _('Dedykowany opis'), array('action' => phpSelf(array('id' => (isset($iId)) ? $iId : null))), array('col_width' => 150), $aConfig['common']['js_validation']);

        $pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
        if ($iId <= 0) {
            $pForm->AddSelect('website_id', _('Serwis'), ['id' => 'website_id'], addDefaultValue($this->getWebsites(), 'Wszystkie'), $aData['website_id'], '', false);
            $pForm->AddSelect('table_destination', _('Typ opisu'), ['id' => 'table_destination'], addDefaultValue($this->getTableDestination()), $aData['table_destination']);
            $pForm->AddText('name', _('Nazwa'), $aData['name'], array('id' => 'searchEntityName', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', true);
        }

        $pForm->AddWYSIWYG('description', _('Opis'), $aData['description']);

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz')) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'default')) . '\');'), 'button'));

        $js = '
		<script type="text/javascript">
          $(document).ready(function(){
                $("#searchEntityName").autocomplete({
                            source: function( request, response ) {
                                    $.ajax({
                                        url: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . '/ajax/GetDedicatedDescriptionOptions.php",
                                        dataType: "json",
                                        data: {
                                          entity: $("#table_destination").val(),
                                          website: $("#website_id").val(),
                                          term: request.term
                                        },
                                        success: function( data ) {
                                          response( data );
                                        }
                                      });
                            },
                            minLength: 2
              });
         });
          
            
            
              
        </script>';

        return ShowTable($pForm->ShowForm()) . $js;
    }

    /**
     * @param $pSmarty
     * @param $iId
     * @return string
     */
    private function Update($pSmarty, $iId)
    {

        $aValues = array(
//            'website_id' => (!empty($_POST['website_id']) ? $_POST['website_id'] : 'NULL'),
//            'table_destination' => $_POST['table_destination'],
//            'name' => $_POST['name'],
//            'dest_id' => $iDestId,
            'description' => $_POST['description'],
            'modified' => 'NOW()',
            'modified_by' => $_SESSION['user']['name']
        );

        if (Common::Update('description_dedicated', $aValues, ' id = ' . $iId)) {
            //rekord zostal zmieniony
            $sMsg = sprintf(_('Zmieniono opis'));
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            return $this->Show($pSmarty);
        } else {
            // rekord nie zostal zmieniony
            $sMsg = sprintf(_('Błąd zmiany dedykowanego opisu'));
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
            return $this->AddEdit($pSmarty, $iId);
        }
    }

    /**
     * @param $pSmarty
     * @return string
     */
    private function Insert($pSmarty)
    {
        global $aConfig;

        if (empty($_POST['website_id']) && $_POST['table_destination'] == 'menus_items') {
            // rekord nie zostal zmieniony
            $sMsg = sprintf(_('Nie można dodać opisu kategorii dla Wszystkich serwisów, ponieważ kategorie i ich ID mogą się różnić między serwisami.'));
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
            return $this->AddEdit($pSmarty);
        }


        preg_match('/\((\d+)\)/', $_POST['name'], $maches);
        $iDestId = $maches[1];
        if ($iDestId <= 0) {
            // rekord nie zostal zmieniony
            $sMsg = sprintf(_('Uwaga nie wybrano nazwy elementu !'));
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
            return $this->AddEdit($pSmarty);
        }

        $aValues = array(
            'website_id' => (!empty($_POST['website_id']) ? $_POST['website_id'] : 'NULL'),
            'table_destination' => $_POST['table_destination'],
            'name' => $_POST['name'],
            'dest_id' => $iDestId,
            'description' => $_POST['description'],
            'created' => 'NOW()',
            'created_by' => $_SESSION['user']['name']
        );

        if (Common::Insert("description_dedicated", $aValues)) {
            //rekord zostal zmieniony
            $sMsg = sprintf(_('Dodano dedykowany opis'));
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            return $this->Show($pSmarty);
        } else {
            // rekord nie zostal zmieniony
            $sMsg = sprintf(_('Błąd dodawania dedykowanego opis'));
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
            return $this->AddEdit($pSmarty);
        }
    }//end of Insert() function

    /**
     * @param $pSmarty
     * @param $iId
     * @return string
     */
    private function Delete($pSmarty, $iId)
    {
        $bIsErr = false;

        $sSql = 'DELETE FROM description_dedicated
            WHERE id = ' . $iId;

        if (($iResS = Common::Query($sSql)) === false) {
            $bIsErr = true;
            $sMsg = _('Błąd usuwania dedykowanego opisu');
            AddLog($sMsg);
        } else {
            $sMsg = _('Usunięto dedykowany opis');
        }

        $this->sMsg = GetMessage($sMsg, $bIsErr);
        return $this->Show($pSmarty);
    }

    /**
     * @return array
     */
    private function getRecords()
    {
        $sSql = 'SELECT * 
                 FROM description_dedicated
                 WHERE 1=1
                 ' . (isset($_POST['f_search']) ? ' AND name LIKE "%' . $_POST['f_search'] . '%"' : '') . '
                 ORDER BY id DESC';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @param $iId
     * @return array
     */
    private function getRecord($iId)
    {
        $sSql = 'SELECT * FROM description_dedicated AS DD WHERE id = ' . $iId;
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }

    /**
     * @return array
     */
    private function getTableDestination()
    {
        $options = [
            [
                'label' => 'Kategorie',
                'value' => 'menus_items'
            ],
            [
                'label' => 'Autorzy',
                'value' => 'products_authors'
            ],
            [
                'label' => 'Wydawnictwa',
                'value' => 'products_publishers'
            ],
        ];
        return $options;
    }
}