<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_B;

use Admin;
use DatabaseManager;
use LIB\EntityManager\Entites\Magazine;
use LIB\orders\listType\filters;
use LIB\orders\listType\filters\containerAvailableForEmployee;
use LIB\orders\listType\filters\containerAvailableForEmployeeThreatOrderAsAll;
use LIB\orders\listType\filters\ommitDouble;
use LIB\orders\listType\filters\onlyDouble;
use LIB\orders\listType\filters\typeFilter;
use LIB\orders\listType\manageGetOrdersItemsList;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj\Abstract_getListType;
use Smarty;

/**
 * Description of Module_part_linked_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_zbieranie_B__stock_full_double extends Abstract_getListType implements ModuleEntity
{

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected $sMsg;

  /**
   *
   * @var Smarty
   */
  public $pSmarty;

  /**
   *
   * @var string
   */
  public $sModule;

  /**
   *
   * @var DatabaseManager
   */
  protected $pDbMgr;

  /**
   *
   * @var manageGetOrdersItemsList
   */
  protected $oManageLists;

  public function __construct(Admin $oClassRepository)
  {
    global $aConfig;

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;

    $this->oManageLists = new manageGetOrdersItemsList($this->pDbMgr, Magazine::TYPE_LOW_STOCK_SUPPLIES);
    $this->oManageLists->setListType(array('SORTER_STOCK'));
    $this->oManageLists->setGetFromTrain(FALSE);
    $this->oManageLists->initOrderListType();
    $this->sListName = 'Pełne Zasoby';
    $this->cListType = '0';

    $_GET['magazine'] = Magazine::TYPE_LOW_STOCK_SUPPLIES;

    $oFilterDuplicates = new onlyDouble($this->pDbMgr);
    $this->oManageLists->addFilter($oFilterDuplicates);
    $this->oManageLists->addRequiredFilter($oFilterDuplicates);

    $oFilter = new typeFilter($this->pDbMgr);
    $oFilter->setFilterCollectingType($_GET['type']);

    $this->oManageLists->addFilter($oFilter);
    $this->oManageLists->addRequiredFilter($oFilter);

    $orderLimit = 30;
    $this->cListType = '6';

    $detectHighLocation = new filters\detectHighLocation($this->pDbMgr);
    $detectHighLocation->unsetIFNotSH(true);
    $detectHighLocation->unsetIFSH(false);
    $detectHighLocation->setMinQuantityStock(filters\detectHighLocation::MIN_QUANTITY_HIGH_LOCATION);

    $this->oManageLists->addFilter($detectHighLocation);
    $this->oManageLists->addRequiredFilter($detectHighLocation);

    $checkMagazine = new filters\checkMagazine($this->pDbMgr);
    $checkMagazine->setMagazine(Magazine::TYPE_HIGH_STOCK_SUPPLIES);
    $checkMagazine->unsetIFNotExists(true);
    $checkMagazine->unsetIFExists(false);
    $checkMagazine->reserveItem(true);
    $checkMagazine->addSellPredict(false);
    $this->oManageLists->addFilter($checkMagazine);
    $this->oManageLists->addRequiredFilter($checkMagazine);

    if (isset($_GET['is_available_employee']) && $_GET['is_available_employee'] != '') {
      $oFilter = new containerAvailableForEmployeeThreatOrderAsAll($this->pDbMgr);
      if ($_GET['is_available_employee'] == '1') {
        $oFilter->setAvailableForEmpoyee(true);
      } else {
        $oFilter->setAvailableForEmpoyee(false);
      }
      $this->oManageLists->addFilter($oFilter);
      $this->oManageLists->addRequiredFilter($oFilter);
    }


    $oFilter = new filters\ordersLimit($this->pDbMgr);
    $oFilter->setLimit($orderLimit);
    $this->oManageLists->addFilter($oFilter);
  }
}
