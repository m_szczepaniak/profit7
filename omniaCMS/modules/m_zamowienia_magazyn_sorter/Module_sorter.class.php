<?php

use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use magazine\Containers;
use omniaCMS\lib\Products\ProductsStockReservations;
use orders\getTransportList;
use orders\Shipment;

/**
 * Moduł sortownika
 * 1) wczytanie listy
 * 2)
 *
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-06-26
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module__zamowienia_magazyn_sorter__sorter
{

    // komunikat
    public $sMsg;

    // nazwa modulu - do langow
    public $sModule;

    // id wersji jezykowej
    public $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    private $aPrivileges;

    // id uzytkownika
    private $iUId;

    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    public $pDbMgr;

    /**
     *
     * @var Module
     */
    private $oModuleZamowienia;

    /**
     *
     * @var getTransportList
     */
    protected $oGetTransport;

    function __construct(&$pSmarty, $bInit = true)
    {
        global $pDbMgr, $aConfig;
        include_once($aConfig['common']['client_base_path'] . 'LIB/autoloader.php');

        $this->pDbMgr = $pDbMgr;

        $this->iLangId = $_SESSION['lang']['id'];
        $_GET['lang_id'] = $this->iLangId;
        $this->sModule = $_GET['module'];

        $sDo = '';

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        $this->sModule .= '_alert';

        $this->iUId = $_SESSION['user']['id'];

        $this->aPrivileges =& $_SESSION['user']['privileges'];
        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }

        include_once('modules/m_zamowienia/Module.class.php');
        $pSmartyyyy = $oParent = new stdClass();
        $this->oModuleZamowienia = new Module($pSmartyyyy, $oParent, true);

        $this->oGetTransport = new getTransportList($this->pDbMgr);

        if ($bInit == false) {
            return;
        }
        switch ($sDo) {
            case 'generate':
                $this->getList($pSmarty);
                break;

            case 'setPackage':
                $this->setPackage($pSmarty, $_POST['pack_number']);
                break;

            case 'save_big_list':
                // dodajemy info o tym ze lista jest duża
                $this->confirmList($pSmarty, true);
            break;

//      case 'save_order_shelf':
//        $this->saveOrderShelf($pSmarty);
//      break;

            case 'confirm_list':
                $this->confirmList($pSmarty);
                break;

            case 'get_invoice_shelf':
                $this->getInvoiceShelfView($pSmarty, $_GET['id'], $_POST['invoice_id']);
                break;

            case 'match_shelf_container':
                $this->MatchShelfToContainer($pSmarty, $_POST['pack_number']);
                break;

            case 'do_match_shelf_container':
                $this->DoMatchShelfToContainer($pSmarty, $_GET['id']);
                break;

            case 'do':
            default:
                $this->GetPackageNumber($pSmarty);
                break;
        }
    }// end of __construct


    /**
     *
     * @param Smarty $pSmarty
     * @param int $iPackNumber
     * @param int $iInvoiceId
     */
    private function getInvoiceShelfView(Smarty $pSmarty, $iPackNumber, $iInvoiceId)
    {

        $iShelfId = $this->getInvoiceShelfNumber($iInvoiceId);
        if ($iShelfId > 0) {
            $this->sMsg .= GetMessage(sprintf(_('Połóż FV na półkę numer <h2>%s</h2>'), $iShelfId), false);
            $iFVQuantity = $this->getFVToPrintQuantity($iPackNumber);
            $iLeftFVQuantity = $iFVQuantity - 1;
            $this->setFVToPrintQuantity($iPackNumber, $iLeftFVQuantity);
            $bFindInvoice = TRUE;
        } else {
            $this->sMsg .= GetMessage(sprintf(_('Nie znaleziono na tej liście FV o nr %s'), $iShelfId), true);
            $bFindInvoice = FALSE;
        }

        if ($iLeftFVQuantity > 0 || $bFindInvoice === false) {
            $this->GetScanFVView($pSmarty, $iPackNumber);
        } else {
            $this->sMsg .= GetMessage(_('Wszystkie FV zostały dopasowane'), false);
            $this->MatchShelfToContainer($pSmarty, $iPackNumber);
        }
    }

    /**
     *
     * @param int $iInvoiceId
     * @return int
     */
    private function getInvoiceShelfNumber($iInvoiceId)
    {

        $sSql = 'SELECT OILI.shelf_number
      FROM orders AS O
      JOIN orders_items_lists_items AS OILI
        ON O.id = orders_id AND OILI.shelf_number <> ""
      WHERE O.invoice_id = "' . $iInvoiceId . '" OR O.invoice2_id = "' . $iInvoiceId . '"
      ORDER BY OILI.id DESC';

        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * Metoda zapisuje zapisuje wybrane rozmiary paczek w zamówieniach i drukuje
     *  listy przewozowe w tych zamówieniach w których rozmiar został wybrany
     *
     * @param Smarty $pSmarty

    public function saveOrderShelf($pSmarty) {
     * $bIsErr = false;
     *
     * try {
     * if (isset($_POST['transport_packages']) && !empty($_POST['transport_packages'])) {
     * $aFedExPackages = $_POST['transport_packages'];
     * foreach ($aFedExPackages as $iOrderId => $iPackagesAmount) {
     * $aLinkedOrders = $this->oModuleZamowienia->getOrdersLinked($iOrderId);
     * $aOrder = $this->oModuleZamowienia->getOrderData($iOrderId);
     * $aOrderSelData = $this->getOrderSelData($iOrderId);
     * switch ($aOrderSelData['transport_symbol']) {
     * case 'opek-przesylka-kurierska':
     * case 'poczta_polska':
     * case 'tba':
     * $this->printoutOrder($aOrderSelData['transport_symbol'], $this->oModuleZamowienia, $this->oGetTransport, $iOrderId, $iPackagesAmount, $aOrder, $aLinkedOrders);
     * break;
     * }
     * }
     * }
     *
     * if (isset($_POST['transport_option_id']) && !empty($_POST['transport_option_id'])) {
     * $aPaczkomatyPackages = $_POST['transport_option_id'];
     * foreach ($aPaczkomatyPackages as $iOrderId => $iPackagesAmount) {
     * $aLinkedOrders = $this->oModuleZamowienia->getOrdersLinked($iOrderId);
     * $aOrder = $this->oModuleZamowienia->getOrderData($iOrderId);
     * $this->printoutOrder($aOrderSelData['transport_symbol'], $this->oModuleZamowienia, $this->oGetTransport, $iOrderId, $iPackagesAmount, $aOrder, $aLinkedOrders);
     * }
     * }
     *
     * } catch (Exception $ex) {
     * $bIsErr = true;
     * $this->sMsg .= GetMessage($ex->getMessage()).'<br />';
     * AddLog($this->sMsg);
     * }
     *
     * if ($bIsErr === FALSE) {
     * $sMsg = _('Lista została zatwierdzona');
     * $this->sMsg .= GetMessage($sMsg, false);
     * addLog($sMsg, false);
     * }
     *
     * $_POST = array();
     * $this->GetPackageNumber($pSmarty);
     * //$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : ''));
     * }
     */

    /**
     * Metoda potwierdza i drukuje etykietę
     *
     * @param string $sTransportSymbol
     * @param Module $oModuleZamowienia
     * @param getTransportList $oGetTransportList
     * @param int $iOrderId
     * @param int $iPackagesAmount
     * @param array $aOrder
     * @param array $aLinkedOrders
     */
    protected function printoutOrder($sTransportSymbol, Module $oModuleZamowienia, getTransportList $oGetTransportList, $iOrderId, $iPackagesAmount, $aOrder, $aLinkedOrders, $bConfirm = true)
    {
        global $aConfig, $pDbMgr;

        try {
            if ($aOrder['transport_number'] == '') {

                switch ($sTransportSymbol) {
                    case 'poczta_polska':
                    case 'poczta-polska-doreczenie-pod-adres':
                    case 'opek-przesylka-kurierska':
                    case 'tba':
                        if ($oGetTransportList->setTransportPackages($iOrderId, $iPackagesAmount, $aLinkedOrders) === false) {
                            throw new Exception("Wystąpił błąd podczas wprowadzania zmian w zamówieniu 5");
                        }
                        $aOrder['transport_packages'] = $iPackagesAmount;
                        break;
                    case 'paczkomaty_24_7':
                        $aOrder['transport_option_id'] = $iPackagesAmount;
                        $sTransportOptionSymbol = $oModuleZamowienia->getTransportOptionSymbol($iPackagesAmount);
                        $aOrder['transport_option_symbol'] = $sTransportOptionSymbol;
                        $oGetTransportList->setPaczkomatyPackages($iOrderId, $iPackagesAmount, $sTransportOptionSymbol, $aOrder['point_of_receipt'], $aLinkedOrders);
                        break;
                }

                if ($this->proceedConfirm($sTransportSymbol, $oModuleZamowienia, $oGetTransportList, $iOrderId, $aOrder, $aLinkedOrders) === false) {
                    return false;
                }
            } else {
                // tylko wyślij na drukarkę
                if ($bConfirm === TRUE) {
//          $this->confirmOrder($oModuleZamowienia, $iOrderId, $aOrder, false, $aLinkedOrders);
                    if ($this->proceedConfirm($sTransportSymbol, $oModuleZamowienia, $oGetTransportList, $iOrderId, $aOrder, $aLinkedOrders) === false) {
                        return false;
                    }
                }

                $oShipment = new Shipment($sTransportSymbol, $pDbMgr);
                if ($oGetTransportList->addToPrintQueue($oShipment->getTransportListFilename($aConfig, $aOrder['transport_number']), $_COOKIE['printer_labels']) === false) {
                    throw new Exception(_("Wystąpił błąd podczas dodawania pliku do kolejki drukowania"));
                }
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
        return true;
    }// end of printoutOrder() method

    /**
     * Metoda drukuje etykietę
     *
     * @param $sTransportSymbol
     * @param Module $oModuleZamowienia
     * @param getTransportList $oGetTransportList
     * @param int $iOrderId
     * @param array $aOrder
     * @param array $aLinkedOrders
     * @throws Exception
     */
    protected function proceedConfirm($sTransportSymbol, Module $oModuleZamowienia, getTransportList $oGetTransportList, $iOrderId, $aOrder, $aLinkedOrders)
    {

        try {
            $this->confirmOrder($oModuleZamowienia, $iOrderId, $aOrder, false, $aLinkedOrders);
            if ($oGetTransportList->proceedAddressLabel($aOrder['transport_id'], $iOrderId, $aOrder, $aLinkedOrders) === false) {
                throw new Exception("Wystąpił błąd podczas tworzenia listu przewozowego w poczta-polska");
            }
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }// end of proceedRuchConfirm() method


    /**
     * Potwierdzamy mamowienie
     *
     * @param Module $oModuleZamowienia
     * @param int $iOrderId
     * @param array $aOrder
     * @param bool $bPersonalReception
     * @param array $aLinkedOrders
     * @return bool
     */
    protected function confirmOrder(Module $oModuleZamowienia, $iOrderId, $aOrder, $bPersonalReception, $aLinkedOrders = array())
    {

        if ($this->tryDeactivateContainers($iOrderId) === false) {
            throw new Exception(_('Wystąpił błąd podczas usuwania zamówienia z zakłedek'), 1);
        }
        if ($this->remarksRead($iOrderId) === false) {
            throw new Exception(_('Wystąpił błąd podczas ustawiania uwag zamówienia jako przeczytane'), 1);
        }
        if ($oModuleZamowienia->setOrderConfirmed($aOrder, $iOrderId) === false) {
            throw new Exception(_('Wystąpił błąd podczas ustawiania zamówienia jako potwierdzone'), 2);
        }
        if ($oModuleZamowienia->proceedSetInvoice($iOrderId, $aOrder, $bPersonalReception) === false) {
            throw new Exception(_('Wystąpił błąd podczas ustawiania faktury jako gotowa do pobrania'), 3);
        }
        // potwierdzamy teraz powiązane
        foreach ($aLinkedOrders as $aOrderLinked) {
            if ($this->remarksRead($aOrderLinked['id']) === false) {
                throw new Exception(_('Wystąpił błąd podczas ustawiania uwag zamówienia jako przeczytane'), 1);
            }
            if ($oModuleZamowienia->setOrderConfirmed($aOrderLinked, $aOrderLinked['id']) === false) {
                throw new Exception(_('Wystąpił błąd podczas ustawiania zamówienia jako potwierdzone'), 2);
            }
            if ($oModuleZamowienia->proceedSetInvoice($aOrderLinked['id'], $aOrderLinked, $bPersonalReception) === false) {
                throw new Exception(_('Wystąpił błąd podczas ustawiania faktury jako gotowa do pobrania'), 3);
            }
        }
        return true;
    }// end of confirmOrder() method


    /**
     * Metoda deaktywuje zamówienia w zakładkach
     *
     * @param int $iOrderId
     * @return boolean
     */
    private function tryDeactivateContainers($iOrderId)
    {
        $aValues = array(
            'checked' => '1',
            'checked_by' => 'auto-confirm'
        );
        if ($this->pDbMgr->Update('profit24', 'orders_reasons_containers', $aValues, ' order_id = ' . $iOrderId . ' AND checked = "0" ') === false) {
            return false;
        }
        return true;
    }// end of tryDeactivateContainers() method


    /**
     * Metoda ustawia status przeczytania uwagi
     *
     * @param int $iOrderId
     * @return bool
     */
    protected function remarksRead($iOrderId)
    {

        $aValues = array('remarks_read' => '1');
        return $this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = ' . $iOrderId);
    }// end of remarksRead() method


    /**
     * Metoda potwierdza listę
     *
     * @param object $pSmarty
     */
    public function confirmList(&$pSmarty, $autoSortoutList = false)
    {
        global $aConfig;

        $aBooks = array();
        $bIsErr = false;

        if (class_exists('memcached')) {
            $oMemcache = new Memcached();
            $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
            include_once('../LIB/orders_semaphore/sorterSemaphore.class.php');
            $oLock = new sorterSemaphore($oMemcache);
            $sLockOrderId = 'S_' . intval($_POST['pack_number']);
            if (true === $oLock->isLocked($sLockOrderId, 1)) {
                exit('Podwójne przesłanie formularza odczekaj 40 sekund');
            }
            $oLock->lockOrder($sLockOrderId, 1);
        }

        if (!isset($_POST['token']) || !isset($_SESSION['token']) || $_POST['token'] != $_SESSION['token']) {
            $this->sMsg .= GetMessage(_('Jesteś za szybki, dwa razy wysłałeś formualarz, prawdopodobnie zamówienia zostały potwierdzone, ale sprawdź lepiej.'));
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            $this->GetPackageNumber($pSmarty);
            return;
        }
        unset($_SESSION['token']);


        if (!empty($_POST)) {
            if (isset($_POST['pack_number']) && intval($_POST['pack_number']) > 0) {
                $iPackNumber = intval($_POST['pack_number']);
                if ($_POST['books'] != '') {
                    $aBooks = json_decode($_POST['books']);
                }
                if (true === $autoSortoutList) {
                    $sSql = 'SELECT OILI.id, OILI.product_id, OILI.quantity, OILI.orders_id, OILI.orders_items_id, OI.parent_id
                             FROM orders_items_lists_items AS OILI
                             JOIN orders_items AS OI
                              ON OI.id = OILI.orders_items_id
                             WHERE OILI.orders_items_lists_id = '.$iPackNumber;
                    $rows = $this->pDbMgr->GetAll('profit24', $sSql);
                    if (!empty($rows)) {
                        foreach ($rows as $row) {
                            $oBook = new \stdClass();
                            $oBook->quantity = $row['quantity'];
                            $oBook->currquantity = $row['quantity'];
                            $oBook->checked = '1';
                            $oBook->product_id = $row['product_id'];
                            $oBook->id = $row['id'];
                            $oBook->orders_items_id = $row['orders_items_id'];
                            $oBook->order_id = $row['orders_id'];
                            $oBook->parent_id = $row['parent_id'];
                            $aBooks[] = $oBook;
                        }
                    }
                }

                if ($this->confirmSortedBooks($aBooks, $iPackNumber, $pSmarty, $autoSortoutList) === false) {
                    $bIsErr = true;
                }

            } else {
                $bIsErr = true;
            }
        } else {
            $bIsErr = true;
        }

        if ($bIsErr === FALSE) {
            try {
                $bPrint = false;
                $this->confirmOrders($iPackNumber);

                $oLock->unlockOrder($sLockOrderId, 1);
//        $this->showPrintConfirmOrderListView($pSmarty, $iPackNumber);

                // @todo pokazujemy FV do półek, jeśli jakieś są.
                // Jeśli nie to pokazujemy przypisz półkę do kuwety.
                $aSelColumn = array('GROUP_CONCAT(O.id SEPARATOR ",") AS orders_ids',
                    'COUNT(O.invoice_id) + COUNT(O.invoice2_id) as quantity');
                $aListPrintFV = $this->getListPrintFV($iPackNumber, $aSelColumn);
                $aPrintFVIds = explode(',', $aListPrintFV['orders_ids']);
                if (isset($aPrintFVIds[0]) && $aPrintFVIds[0] != '') {
                    $bPrint = true;
                    $this->PrintMultipleFV($aPrintFVIds);
                }
                $iPrintFVQuantity = $aListPrintFV['quantity'];
                $this->setFVToPrintQuantity($iPackNumber, $iPrintFVQuantity);


                if ($bPrint === true && $iPrintFVQuantity <= 0) {
                    $sContent = 'blad _ iPrintFVQuantity:' . $iPrintFVQuantity . ' | $iPackNumber:' . $iPackNumber . ' | $aPrintFVIds:' . print_r($aPrintFVIds, true) . ' | $aListPrintFV:' . print_r($aListPrintFV, true) . ' | $aBooks:' . print_r($aBooks, true);
                    \Common::sendTxtHtmlMail('kontakt@profit24.pl', 'kontakt@profit24.pl', $sContent, 'błąd sortownik', '', $sContent);
                }

                if ($iPrintFVQuantity > 0 || $bPrint === true) {
                    $this->AddInfoGetFV();
                    $this->GetScanFVView($pSmarty, $iPackNumber);
                } else {
                    $this->MatchShelfToContainer($pSmarty, $iPackNumber);
                }
            } catch (Exception $ex) {
                $this->sMsg .= GetMessage($ex->getMessage());
                $bIsErr = true;
            }
        }

        if ($bIsErr === FALSE) {
            $oLock->unlockOrder($sLockOrderId, 1);

            $sMsg = _('Lista numer: ' . $iPackNumber . ' została posortowana przez ' . $_SESSION['user']['author'] . '<br /> Możesz teraz wydrukować listy oraz faktury ');
            // rekord zostal zaktualizowany
            $this->sMsg .= GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            /*
            //$this->GetPackageNumber($pSmarty);
            setSessionMessage($sMsg);
            $_SESSION['f_send_pack_number'] = $_POST['pack_number'];
            $_SESSION['f_send_pack_user_id'] = $_SESSION['user']['id'];

            doRedirect($aConfig['common']['base_url_https'].phpSelf(array('module' => 'm_zamowienia', 'module_id' => '38', 'ppid' => '5', 'reset' => '1'), array('action')));
            exit(0);
             */
        } else {
            $oLock->unlockOrder($sLockOrderId, 1);

            $sMsg = _('Wystąpił błąd podczas sortowania listy numer: ' . $iPackNumber . ' osoba sortująca: ' . $_SESSION['user']['author'] . ' <br /> Proszę zgłosić ten błąd programiście.');
            // rekord zostal zaktualizowany
            $this->sMsg .= GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
            $this->GetPackageNumber($pSmarty);
        }
    }// end of confirmList() method

    /**
     *
     * @param array $aOrdersIds
     */
    protected function PrintMultipleFV($aOrdersIds)
    {
        global $aConfig;

        include_once('lib/Invoice.class.php');
        foreach ($aOrdersIds as $iOrderId) {
            $oInvoice = new Invoice();
            $oInvoice->sTemplatePath = $aConfig['common']['client_base_path'] . 'omniaCMS/smarty/templates/';
            $sFilePath = $oInvoice->getInvoicePDF($iOrderId, $this->getOrderData($iOrderId), 2);
            $this->oGetTransport->addToPrintQueue($sFilePath, $_COOKIE['printer_invoices']);
        }
    }

    /**
     * Pobiera dane zamówienia
     *
     * @param int $iOrderId - id zamowienia
     * @return array
     */
    private function getOrderData($iOrderId)
    {

        $sSql = "SELECT *
						FROM orders
						WHERE id = " . $iOrderId;
        return $this->pDbMgr->GetRow('profit24', $sSql);
    } // end of getOrderData() method

    /**
     *
     * @param int $iOILId
     * @return bool
     */
    private function confirmOrders($iOILId)
    {

        $aOrdersALL = $this->getOrdersOfList($iOILId);
//    $aOrders = $this->getOrdersUniqueLinked($this->oModuleZamowienia, $aOrdersALL);
        foreach ($aOrdersALL as $aOrderListData) {
//      if (!isset($aOrderListData['linked_orders'])) {
//        $aOrderListData['linked_orders'] = array();
//      }
            try {
                $this->confirmOrder($this->oModuleZamowienia, $aOrderListData['order_id'], $this->oModuleZamowienia->getOrderData($aOrderListData['order_id']), true);
            } catch (Exception $ex) {
                throw new Exception($ex->getMessage());
            }
        }
        return true;
    }

    /**
     *
     * @param int $iPackNumber
     * @return int $iPrintFVQuantity
     */
    protected function getFVToPrintQuantity($iPackNumber)
    {

        return $_SESSION['sort_' . $iPackNumber]['FV_to_print_quantity'];
    }

    /**
     *
     * @param int $iPackNumber
     * @param int $iPrintFVQuantity
     */
    protected function setFVToPrintQuantity($iPackNumber, $iPrintFVQuantity)
    {

        $_SESSION['sort_' . $iPackNumber]['FV_to_print_quantity'] = $iPrintFVQuantity;
    }

    /**
     *
     * @param Smarty $pSmarty
     * @param int $iOILId
     * @return void
     */
    private function MatchShelfToContainer(Smarty $pSmarty, $iOILId)
    {

        include_once('Form/FormTable.class.php');
        $oForm = new FormTable('match_shelf_container', _('Przypisz półkę do kuwety'), array('action' => phpSelf(array('id' => $iOILId, 'do' => 'match_shelf_container'))), array('col_width' => 55), false);
        $oForm->AddHidden('do', 'do_match_shelf_container');
        $oForm->AddMergedRow(_('Przypisz półkę do kuwety'), array('class' => 'merged'));
        $oForm->AddText('shelf_id', _('Półka'), '', array('style' => 'width: 400px; font-size: 25px;'), '', 'text');
        $oForm->AddText('container_id', _('Numer kuwety /<br> НОМЕР ЯЩИКА:<br>'), '', array('style' => 'width: 400px; font-size: 25px;'), '', 'text');
        $oForm->AddText('shelf_id2', _('Półka'), '', array('style' => 'width: 400px; font-size: 25px;'), '', 'text');
        $oForm->AddHidden('oilid_hidden', $iOILId);
        $oForm->AddInputButton('save', _('Zapisz'), array('style' => 'font-size: 25'));
        $sHTML = $oForm->ShowForm();
        $sJS = '<script type="text/javascript" src="js/matchShelfToContainer.js?time=26082014"></script>';
        $sJS .= '<script type="text/javascript" src="js/shelfAjaxLoader.js?time=26082014"></script>';

        $shelfContainer = '<div id="shelf-container"></div>';

        $pSmarty->assign('sContent', $shelfContainer . (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($sHTML) . $sJS);
    }


    public function getShelfProductsNotConfirmed($iOILId, $sShelfNumber){

        $sSql = 'SELECT OILI.id, OILI.isbn_plain, P.name, (OILI.quantity - OILI.confirmed_quantity) diff_quantity
                 FROM orders_items_lists_items AS OILI
                 LEFT JOIN products AS P 
                    ON P.id = OILI.products_id
                 WHERE
                  OILI.orders_items_lists_id = ' . $iOILId . ' AND
                  OILI.shelf_number = ' . $sShelfNumber.' AND
                  OILI.confirmed = "0"
                  ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }





    /**
     *
     * @param Smarty $pSmarty
     * @param int $iOILId
     * @return void
     */
    private function DoMatchShelfToContainer(Smarty $pSmarty, $iOILId)
    {

        $oContainers = new Containers($this->pDbMgr);
        if ($_POST['shelf_id'] == $_POST['shelf_id2']) {
            $sShelfNumber = $_POST['shelf_id'];
            if ($oContainers->checkContainer($_POST['container_id']) === 1) {
                $ordersNotConfirmed = $this->getShelfProductsNotConfirmed($iOILId, $sShelfNumber);
                if (!empty($ordersNotConfirmed)) {
                    $msg = '';
                    foreach ($ordersNotConfirmed as $orderProduct) {
                        $msg .= _('Na podanej półce nr "'.$sShelfNumber.'" w liście "'.$iOILId.'" wystąpił brak produktu "'.$orderProduct['name'].'" o EAN "'.$orderProduct['isbn_plain'].'", brakująca ilość '.$orderProduct['diff_quantity']);
                    }
                    AddLog($msg);
                    $this->sMsg .= GetMessage($msg);
                    return $this->GetPackageNumber($pSmarty);
                }

                $this->pDbMgr->BeginTransaction('profit24');
                $oContainers->lockContainer($_POST['container_id']);
                if ($this->UpdateShelfItemsLists($iOILId, $sShelfNumber, $_POST['container_id']) === false) {
                    $this->pDbMgr->RollbackTransaction('profit24');
                    $this->sMsg .= GetPopupMessage(_('Błąd przypisywania półki do kuwety'));
                } else {
                    $this->pDbMgr->CommitTransaction('profit24');
                    $this->sMsg .= GetMessage(sprintf(_('Przypisano półkę %s do kuwety %s // ПОЛКА...УСПЕШНО ПРИПИСАНА К ЯЩИКУ...'), $sShelfNumber, $_POST['container_id']), false);
                }
            } else {
                $this->pDbMgr->RollbackTransaction('profit24');
                $this->sMsg .= GetPopupMessage(_('Kuweta jest zablokowana /<br>ЛИБО ЯЩИК ЗАБЛОКИРОВАН'));
                unset($_POST);
                return $this->GetPackageNumber($pSmarty);
            }
        }
//        $this->addToTimeStats($iListNumber, $scQuantity, $oBook->id);
        if ($this->checkExistNextNotComparedShelf($iOILId)) {
            $this->MatchShelfToContainer($pSmarty, $iOILId);
        } else {
            $this->sMsg .= GetMessage(_('<span style="font-size:40px; color:blue">Przypisano wszystkie półki do kuwet / ВСЕ ЗАКАЗЫ УСПЕШНО ПРИПИСАНЫ</span>'), false);
            unset($_POST);
            $this->GetPackageNumber($pSmarty);
        }
    }

    /**
     *
     * @param int $iOILId
     * @return int
     */
    private function checkExistNextNotComparedShelf($iOILId)
    {

        $sSql = 'SELECT id 
             FROM orders_items_lists_items
             WHERE 
              orders_items_lists_id = ' . $iOILId . ' AND
              container_id IS NULL';
        return ($this->pDbMgr->GetOne('profit24', $sSql) > 0);
    }

    /**
     *
     * @param int $iOILId
     * @param string $sShelfNumber
     * @return bool
     */
    private function UpdateShelfItemsLists($iOILId, $sShelfNumber, $sContainerId)
    {

        if ($this->checkOILIExist($iOILId, $sShelfNumber) == TRUE) {

            $sWhere = '
              orders_items_lists_id = ' . $iOILId . ' AND
              shelf_number = ' . $sShelfNumber;

            $aValues = array(
                'container_id' => $sContainerId
            );
            if ($this->pDbMgr->Update('profit24', 'orders_items_lists_items', $aValues, $sWhere) === false) {
                return false;
            }

            $iOrderId = $this->getOrderIdByShelf($iOILId, $sShelfNumber);
            if ($iOrderId > 0) {
                if ($this->setContainerOrderId($sContainerId, $iOrderId) === false) {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     *
     * @param string $sContainerId
     * @param int $iOrderId
     * @return type
     */
    private function setContainerOrderId($sContainerId, $iOrderId)
    {

        $aValues = array(
            'item_id' => $iOrderId
        );
        return $this->pDbMgr->Update('profit24', 'containers', $aValues, ' id = "' . $sContainerId . '"');
    }


    /**
     *
     * @param int $iOILId
     * @param string $sShelfNumber
     * @return int
     */
    private function getOrderIdByShelf($iOILId, $sShelfNumber)
    {

        $sSql = 'SELECT orders_id
             FROM orders_items_lists_items
             WHERE
                  orders_items_lists_id = ' . $iOILId . ' AND
                  shelf_number = ' . $sShelfNumber;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * @param int $iOILId
     * @param string $sShelfNumber
     * @return bool
     */
    private function checkOILIExist($iOILId, $sShelfNumber)
    {

        $sSql = 'SELECT id 
             FROM orders_items_lists_items
             WHERE 
              orders_items_lists_id = "' . $iOILId . '" AND
              shelf_number = ' . $sShelfNumber;
        return ($this->pDbMgr->GetOne('profit24', $sSql) > 0);
    }

    /**
     *
     * @param Smarty $pSmarty
     * @param int $iOILId
     * @return void
     */
    protected function GetScanFVView(Smarty $pSmarty, $iOILId)
    {

        include_once('Form/FormTable.class.php');
        $oForm = new FormTable('scan_fv', _('Wczytaj FV / ВВЕДИТЕ КОД НАКЛАДНОЙ'), array('action' => phpSelf(array('id' => $iOILId, 'do' => 'get_invoice_shelf'))), array('col_width' => 155), false);
        $oForm->AddText('invoice_id', _('Faktura numer'), '', array('style' => 'width: 400px; font-size: 25px;', 'autofocus' => 'autofocus'), '', 'text');
        $oForm->AddInputButton('save', _('Zapisz'), array('style' => 'font-size: 25'));
        $sHTML = $oForm->ShowForm();
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($sHTML));
    }

    /**
     * @return void
     */
    protected function AddInfoGetFV()
    {

        $this->sMsg .= GetMessage('Pobierz FV z drukarki', false);
        return true;
    }

    /**
     *
     * @param int $iOILId
     * @return bool
     */
    protected function getListPrintFV($iOILId, array $aCols)
    {

        $sSql = 'SELECT ' . implode(', ', $aCols) . '
              FROM orders AS O
              WHERE id IN 
              (
               SELECT OILI.orders_id 
               FROM orders_items_lists_items AS OILI
               WHERE OILI.orders_items_lists_id = ' . $iOILId . '
              ) AND
              O.print_fv = "1" ';

        return $this->pDbMgr->GetRow('profit24', $sSql);
    }// end of getListPrintFV() method


    /**
     * Metoda otrzymuje wszystkie zamówienia, a ma pozostawić tylko gówne zamówienie i dowiazane inne pozostałe zamówienia
     *
     * @param Module $oModuleZamowienia
     * @param array $aOrdersALL
     * @return array
     */
    private function getOrdersUniqueLinked(Module $oModuleZamowienia, $aOrdersALL)
    {
        $aLinkedIds = array();
        $aOrders = array();

        foreach ($aOrdersALL as $iKey => $aOrderListData) {
            // jeśli nie istnieje ten element w już połaczonych
            if (!in_array($aOrderListData['order_id'], $aLinkedIds)) {
                $aLinkedOrders = $oModuleZamowienia->getOrdersLinked($aOrderListData['order_id']);
                // @todo, sprawdzamy czy wszystkie w $aOrdersALL znajdują się także $aLinkedOrders,
                // jesli nie to usuwamy, te wszystkie z $aOrdersAll

                if (!empty($aLinkedOrders)) {
                    if ($this->checkAllLinkedOrdersAreInConfirmed($aOrdersALL, $aLinkedOrders) === TRUE) {
                        $aOrderListData['linked_orders_shelfs'] = $this->getLinkedOrdersShelfs($aLinkedIds, $aOrdersALL, $aLinkedOrders);
                        $aOrders[$iKey] = $aOrderListData;
                        $aOrders[$iKey]['linked_orders'] = $aLinkedOrders;
                    }
                } else {
                    $aOrders[$iKey] = $aOrderListData;
                }
            }
        }
        return $aOrders;
    }// end of getOrdersUniqueLinked() method


    /**
     * Metoda sprawdza, czy wszystkie składowe połączonego zamówienia, zostały potiwerdzone
     *
     * @param array $aOrdersALL
     * @param array $aLinkedOrders
     * @return boolean
     */
    private function checkAllLinkedOrdersAreInConfirmed($aOrdersALL, $aLinkedOrders)
    {
        $aIDs = array();

        foreach ($aOrdersALL as $aOrder) {
            $aIDs[] = $aOrder['order_id'];
        }
        foreach ($aLinkedOrders as $aOrder) {
            if (!in_array($aOrder['id'], $aIDs)) {
                return false;
            }
        }
        return true;
    }// end of checkAllLinkedOrdersAreInConfirmed() method


    /**
     * Metoda zwraca tablicę powiązanych półek zamówień wystłanych razem
     *
     * @param array $aLinkedIds
     * @param array $aOrdersAll
     * @param array $aLinkedOrders
     * @return array
     */
    private function getLinkedOrdersShelfs(&$aLinkedIds, $aOrdersAll, $aLinkedOrders)
    {
        $aReturnLinkedShelfs = array();


        foreach ($aLinkedOrders as $aLinked) {
            $aLinkedIds[] = $aLinked['id'];
        }
        foreach ($aOrdersAll as $aOrder) {
            if (in_array($aOrder['order_id'], $aLinkedIds)) {
                $aReturnLinkedShelfs[] = $aOrder['shelf_number'];
            }
        }
        return $aReturnLinkedShelfs;
    }// end of getLinkedOrdersShelfs() method


    /**
     * Metoda pobiera id zamówień na podstawie id przekazanej listy
     *
     * @param int $iOILId
     * @return array
     */
    private function getOrdersOfList($iOILId)
    {

        $sSql = 'SELECT DISTINCT OILI.orders_id as order_id, OILI.shelf_number
             FROM orders_items_lists_items AS OILI
             WHERE 
             OILI.orders_items_lists_id = ' . $iOILId . '
               AND
              (SELECT OI.id 
               FROM orders_items AS OI
               WHERE OI.order_id = OILI.orders_id
                 AND deleted = "0"
                 AND item_type = "I"
                 AND status <> "4"
                 LIMIT 1
               ) IS NULL
               ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }// end of getOrdersOfList() method


    /**
     * Metoda pobiera pełną wagę zamówienia
     *
     * @param int $iOrderId
     * @return float
     */
    protected function getOrderSelData($iOrderId)
    {

        $sSql = 'SELECT W.code as bookstore, O.transport_id, OTM.symbol AS transport_symbol, O.magazine_remarks, O.remarks, O.transport_remarks, O.order_number
             FROM orders AS O
             JOIN websites AS W
              ON O.website_id = W.id
             JOIN orders_transport_means AS OTM
              ON OTM.id = O.transport_id
             WHERE O.id = ' . $iOrderId;

        return $this->pDbMgr->GetRow('profit24', $sSql);
    }// end of getOrderWeight() method


    /**
     * Metoda potwierdza książki, które zostaną potwierdzone po sortowniku
     *
     * @param array $aBooks
     * @param int $iListNumber
     * @param $pSmarty
     * @param bool $autoSortoutList
     * @return bool
     */
    public function confirmSortedBooks($aBooks, $iListNumber, $pSmarty, $autoSortoutList = false)
    {
        $bIsErr = false;

        $aOrdersToRecount = array();
        $aConfirmParents = array();

        $this->pDbMgr->BeginTransaction('profit24');

        if ($this->closeList($iListNumber) === false) {
            $bIsErr = true;
        }

        if (!empty($aBooks)) {
            foreach ($aBooks as $oBook) {
                if (!empty($oBook)) {
                    if (true === $autoSortoutList) {
                        // tak dla pewności ;) poziom wyżej jest to samo
                        $oBook->currquantity = $oBook->quantity;
                        $oBook->checked = '1';
                    }

                    // potwierdzamy w OILI
                    if ($this->updateOILItem($oBook->id, $oBook->checked, $oBook->currquantity) === FALSE) {
                        $bIsErr = true;
                    }
                    $scQuantity = $oBook->currquantity;
                    if ($oBook->currquantity > $oBook->quantity) {
                        $scQuantity = $oBook->quantity;
                    }

                    $this->addToTimeStats($iListNumber, $scQuantity, $oBook->id);


                    // potwierdzamy w OI
                    if ($oBook->checked == '1') {
                        if ($this->updateConfirmItem($oBook->orders_items_id) === false) {
                            $bIsErr = true;
                        }

                        if ($oBook->parent_id > 0 && !in_array($oBook->parent_id, $aConfirmParents)) {
                            $aConfirmParents[] = $oBook->parent_id;
                        }
                    }

                    // przeliczamy zamówienie
                    if (!in_array($oBook->order_id, $aOrdersToRecount)) {
                        $aOrdersToRecount[] = $oBook->order_id;
                    }
                }
            }
        } else {
            $this->pDbMgr->RollbackTransaction('profit24');
            $sMsg = _('Wystąpił błąd nr 2 podczas sortowania listy numer: ' . $iListNumber . ' osoba sortująca: ' . $_SESSION['user']['author'] . ' <br /> Proszę zgłosić ten błąd programiście.');
            $this->sMsg .= GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
            $this->GetPackageNumber($pSmarty);
            die;
        }

        // potwierdzamy rodziców
        if ($bIsErr === FALSE) {
            foreach ($aConfirmParents as $iParentId) {
                if ($bIsErr === FALSE && $this->confirmParents($iParentId) === FALSE) {
                    $bIsErr = true;
                }
            }
        }

        if ($bIsErr === false) {
            unset($_SESSION['sort_time_'.$iListNumber]);
            $this->pDbMgr->CommitTransaction('profit24');
        } else {
            $this->pDbMgr->RollbackTransaction('profit24');
        }


        require_once('OrderRecount.class.php');
        $oOrderRecount = new OrderRecount();
        if ($bIsErr === false) {
            // wycofujemy do '---' pozycje które nie zostały potwierdzone na tej liście
            $sSql = 'SELECT orders_items_id, orders_id
             FROM orders_items_lists_items
             WHERE orders_items_lists_id = ' . $iListNumber . '
                   AND confirmed = "0"';
            $aOrdersItemsIds = $this->pDbMgr->GetAll('profit24', $sSql);

            $sSql = 'SELECT orders_items_id, orders_id
             FROM orders_items_lists_items
             WHERE orders_items_lists_id = ' . $iListNumber . '
                   AND confirmed = "1"';
            $aOrdersItemsConfirmedIds = $this->pDbMgr->GetAll('profit24', $sSql);

            if (!empty($aOrdersItemsConfirmedIds)) {
                foreach ($aOrdersItemsIds as $aItemUselRow) {
                    // wycofujemy do nowych produkty, które nie zostały zeskanowane w obrębie tej listy
                    if ($this->backNewOrderItem($oOrderRecount, $aItemUselRow['orders_items_id'], $aItemUselRow['orders_id']) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }


        // jeśli brak błędu
        if ($bIsErr === FALSE) {
            $aOrdersToRecount = array_unique($aOrdersToRecount);
            foreach ($aOrdersToRecount as $iOrderId) {
                if (!$bIsErr) {
                    if ($oOrderRecount->recountOrder($iOrderId, true, true, true) === false) {
                        $bIsErr = true;
                    }
                }
            }
        }

        return !$bIsErr;
    }// end of confirmSortedBooks() method

    /**
     *
     * @param int $iOrderItemId
     * @return int
     */
    private function getSourceOrderItem($iOrderItemId)
    {

        $sSql = 'SELECT source 
             FROM orders_items 
             WHERE id = ' . $iOrderItemId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     *
     * @param int $iOrderId
     * @param int $iOrderItemId
     * @return bool
     */
    private function deleteReservation($iOrderId, $iOrderItemId)
    {

        $iSourceId = $this->getSourceOrderItem($iOrderItemId);
        $oPSReservations = new ProductsStockReservations($this->pDbMgr);
        return $oPSReservations->deleteReservation($iSourceId, $iOrderId, $iOrderItemId);
    }

    /**
     * Metoda wycofuje produkt do nowych, zamówienie moze zostać ponownie pobrane na listę do zebrania,
     * następnie zamówienie zostaje przeliczone
     *
     * @param OrderRecount $oOrderRecount
     * @param int $iOrderItemId
     * @param int $iOrderId
     * @return bool
     */
    protected function backNewOrderItem(OrderRecount $oOrderRecount, $iOrderItemId, $iOrderId)
    {
        $bIsErr = false;
        $reservationManager = new ReservationManager($this->pDbMgr);
        $beforeReservation = $reservationManager->getDataProvider()->getReservationsByOrderId($iOrderId);
        $aOIValue = array(
            '`status`' => '0',
            'get_ready_list' => '0'
        );
        $aOIOtherReadyValue = array(
            'get_ready_list' => '0'
        );

        if ($this->deleteReservation($iOrderId, $iOrderItemId) === false) {
            $bIsErr = true;
        }

        // cofamy status
        if ($this->pDbMgr->Update('profit24', 'orders_items', $aOIValue, ' id = ' . $iOrderItemId) === FALSE) {
            $bIsErr = true;
        }
        // pozostałe produkty tego zamówienia mogą ponownie pojawić się na liście od zebrania
        if ($this->pDbMgr->Update('profit24', 'orders_items', $aOIOtherReadyValue, ' order_id = ' . $iOrderId) === FALSE) {
            $bIsErr = true;
        }

        // pobieramy Id transportu w celu usunięcia etykiety adresowej
        $sTransportNumber = $this->getTransportNumber($iOrderId);

        if (!isset($this->oModuleZamowienia)) {
            // ok zatwierdzamy to zamówienie, aby przeszło do paczkomaty/paczki/odbior osobisty
            include_once('modules/m_zamowienia/Module.class.php');
            $pSmartyy = $oParent = new stdClass();
            $this->oModuleZamowienia = new Module($pSmartyy, $oParent, true);
        }
        // pobieramy zamówienia powiązane w celu usunięcia z nich etykiety adresowej
        $aLinkedOrders = $this->oModuleZamowienia->getOrdersLinked($iOrderId);

        $aOrdersToIterate[] = $iOrderId;

        if ($aLinkedOrders) {
            foreach ($aLinkedOrders as $aLinkedOrder) {
                $aOrdersToIterate[] = $aLinkedOrder['id'];
            }
        }

        foreach ($aOrdersToIterate as $iOrderId) {

            // zamówienie wycofujemy i moze być zmienione
            if ($this->pDbMgr->Update('profit24', 'orders', array('print_ready_list' => '0', 'transport_number' => 'NULL', 'transport_packages' => 'NULL'), ' id = ' . $iOrderId) === FALSE) {
                $bIsErr = true;
            } else {
                // usuwamy etykietę adresową
                $this->deleteSticker($sTransportNumber, $iOrderId);
            }

            // jeśli brak błędu
            if ($bIsErr === FALSE) {
                if (!$bIsErr) {
                    if ($oOrderRecount->recountOrder($iOrderId) === false) {
                        $bIsErr = true;
                    }
                }
            }

            // Streamsoft rezerwacje
            if (false == $bIsErr) {
                try {
                    $reservationResult = $reservationManager->createStreamsoftReservation($beforeReservation, $iOrderId);

                    // zamowienie czesciowe, wyswielamy info
                    if (false === $reservationResult) {
                        $this->sMsg .= GetMessage($reservationManager->getErrorMessage());
                    }

                } catch (\Exception $e) {
                    $bIsErr = true;
                    $this->sMsg .= GetMessage($e->getMessage());
                }
            }
        }

        return !$bIsErr;
    }// end of backNewOrderItem() method


    /**
     * Metoda zwraca Id transportu na podstawie Id zamówienia
     *
     * @param string $iOrderId
     * return string
     */
    private function getTransportNumber($iOrderId)
    {

        $sSql = 'SELECT transport_number 
             FROM orders
             WHERE id = ' . $iOrderId;

        return $this->pDbMgr->GetOne('profit24', $sSql);
    }// end of getTransportNumber() method


    /**
     * Metoda zwraca symbol transportu na podstawie Id zamówienia
     *
     * @param string $iOrderId
     * return string
     */
    private function getTransportSymbol($iOrderId)
    {

        $sSql = 'SELECT OTM.symbol as transport_symbol
             FROM orders AS O
             JOIN orders_transport_means AS OTM
              ON O.transport_id = OTM.id
              WHERE O.id = ' . $iOrderId;

        return $this->pDbMgr->GetOne('profit24', $sSql);
    }// end of getTransportSymbol() method


    /**
     * Metoda usuwa plik PDF z wygenerowaną etykietą adresową
     *
     * @param string $sTransportNumber
     */
    private function deleteSticker($sTransportNumber, $iOrderId)
    {
        global $aConfig, $pDbMgr;

        $sTransportSymbol = $this->getTransportSymbol($iOrderId);

        $oShipment = new Shipment($sTransportSymbol, $pDbMgr);
        $sFilename = $oShipment->getTransportListFilename($aConfig, $sTransportNumber);

        if (file_exists($sFilename)) {
            unlink($sFilename);
        }
    }// end of deleteSticker() method


    /**
     * Metoda zamyka listę
     *
     * @return boolean
     */
    protected function closeList($iListNumber)
    {

        // zamykamy listę
        $aValuesOIL = array(
            'closed' => '1',
            'user_id' => $_SESSION['user']['id']
        );
        if ($this->pDbMgr->Update('profit24', 'orders_items_lists', $aValuesOIL, ' id = ' . $iListNumber) === FALSE) {
            return false;
        }
        return true;
    }// end of closeList() method


    /**
     * Metoda ustawia zeskanowaną ilość na sortowniku
     *
     * @param int $iOILIId
     * @param bool $bConfirmed
     * @param int $iQuantity
     * @return boolean
     */
    protected function updateOILItem($iOILIId, $bConfirmed, $iQuantity)
    {

        $aOILIValues = array(
            'confirmed' => $bConfirmed,
            'confirmed_quantity' => $iQuantity
        );
        if ($this->pDbMgr->Update('profit24', 'orders_items_lists_items', $aOILIValues, ' id = ' . $iOILIId) === FALSE) {
            return false;
        }
        return true;
    }// end of updateOILItem() method


    /**
     * Potwierdzamy składową zamówienia w DB
     *
     * @param int $iOId
     * @return boolean
     */
    protected function updateConfirmItem($iOId)
    {

        $aOIValues = array(
            'status' => '4'
        );
        if ($this->pDbMgr->Update('profit24', 'orders_items', $aOIValues, 'id = ' . $iOId) === FALSE) {
            return false;
        }
        return true;
    }// end of updateConfirmItem() method


    /**
     * Ustawiamy dane do zatwierdzenia
     *
     * @param object $pSmarty
     * @param string $sPackageNumber
     */
    public function setPackage(&$pSmarty, $sPackageNumber)
    {

        if ($sPackageNumber != '') {
            $aListData = $this->getOrdersItemsListsByPackageIdent($sPackageNumber, array('*'), false);
            if (!empty($aListData)) {
                switch ($aListData['type']) {
                    case '2':
                        $this->sMsg .= GetMessage(_(' <span style="font-size: 2em; ">!!! UWAGA ZAMÓWIENIA ŁĄCZONE !!! </span> '), FALSE);
                        break;
                }
                switch ($aListData['closed']) {
                    case '0':
                        $oContainers = new Containers($this->pDbMgr);
                        $oContainers->unlockContainer($sPackageNumber);
                        $this->getConfirmBookList($pSmarty, $aListData, $aListData['id']);
                        break;
                    case '2':
                        $this->getConfirmBookList($pSmarty, $aListData, $aListData['id']);
                        break;

                    case '1':
                        // zamknięte, wyświetlenie numeru półki po wprowadzeniu nr listu przewozowego
                        $this->MatchShelfToContainer($pSmarty, $aListData['id']);
                        break;

                    case '3':
                        $this->packageWaiting($pSmarty, $aListData);
                        break;
                }
            } else {
                $this->sMsg = getMessage("Brak listy o podanym numerze");
                $this->GetPackageNumber($pSmarty);
            }
        } else {
            $this->sMsg = getMessage("Wprowadzono pusty numer listy");
            $this->GetPackageNumber($pSmarty);
        }
    }// end of setPackage() method


    /**
     * Wczytano kuwetę oczekującą na pobranie listu przewozowego w jednym z zamówienień
     *
     * @param Smarty $pSmarty
     * @param array $aListData
     * @return void
     */
    protected function packageWaiting(&$pSmarty, $aListData)
    {
        $sMsg = sprintf(_('Odłóż kuwetę o nr "%s" na "poczekalnię" i spróbuj wskanować ją później, jeśli kuweta przez dłuższy czas nie zostanie odblokowana zgłoś to przełożonemu.'), $aListData['package_number']);
        $this->sMsg = GetMessage($sMsg);
        AddLog($sMsg);
        unset($_POST);
        return $this->GetPackageNumber($pSmarty);
    }// end of packageWaiting() method


    /**
     * Metoda pobiera dane listy na podstawie przekazanego numeru paczki,
     *  UWAGA METODA POBIERA OSTATNIĄ ZAPISANĄ LISTĘ DANEJ KUWETY
     *
     * @param string $sPackageNumber
     * @param array $aCols
     * @param bool $bSingle
     * @param bool $bNotClosed - czy lista nie ma być zamknięta
     * @return array
     */
    protected function getOrdersItemsListsByPackageIdent($sPackageNumber, $aCols, $bSingle, $bNotClosed = false, $bDouble = false)
    {
        //$sPackageNumber = '3000004000';
        $sSql = 'SELECT ' . implode(',', $aCols) . ' 
             FROM orders_items_lists 
             WHERE package_number = "' . $sPackageNumber . '"
               ' . ($bSingle === true ? ' AND type = "1" ' : ' AND type <> "1" ') . '
               ' . ($bDouble === true ? ' AND type = "6" ' : ' AND type <> "6" ') . '
               ' . ($bNotClosed === true ? ' AND closed <> "1"' : '') . '
              AND type != "3"
             ORDER BY id DESC 
             LIMIT 1';
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }// end of getOrdersItemsListsByPackageIdent() method


    /**
     * Metoda pobiera książki do potwierdzenia, do przesortowania
     *
     * @param int $iListId
     * @return array
     */
    private function _getConfirmedBooks($iListId)
    {

        // ok zapisujemy jakie zamówienia mają być w których półkach
        $sSql = "SELECT 
                OILI.orders_id, OILI.orders_id as order_id, OILI.orders_items_id, OILI.id, OILI.quantity, '0' AS currquantity, OILI.isbn_plain, 
                OILI.isbn_10, OILI.isbn_13, OILI.ean_13, OILI.shelf_number, PI.directory, PI.photo,
                OI.name,
                OI.weight, OI.publisher, OI.authors, OI.publication_year, OI.edition,
                '0' AS checked,
                OI.parent_id
             FROM orders_items_lists_items AS OILI
             LEFT JOIN products_images AS PI
                ON PI.product_id = OILI.products_id
             LEFT JOIN orders_items AS OI
                ON OI.id = OILI.orders_items_id
             WHERE orders_items_lists_id = " . $iListId . "
             GROUP BY OILI.id";
        return $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);
    }// end of _getConfirmedBooks() method

  /**
   *
   * @param int $iOrderId
   * @return array
   */
  protected function getOrdersLinked($iOrderId) {

    $sSql = 'SELECT linked_order_id FROM orders_linked_transport WHERE main_order_id = '.$iOrderId;
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }

  /**
   * @param $aListItems
   * @return array
   */
  private function mergeLinkedOrders($aListItems) {
    $result = [];
    $usedIds = [];

    // na jedną półkę maja trafić te same zamówienia
    foreach ($aListItems as $iKey => $aOrderItems) {
      if (false === in_array($iKey, $usedIds)) {
        $result[$iKey] = $aOrderItems;
        $linkedRows = $this->getOrdersLinked($iKey);
        foreach ($linkedRows as $orderId) {
          $usedIds[] = $orderId;

          if (isset($aListItems[$orderId])) {
            $result[$iKey] = array_merge($result[$iKey], $aListItems[$orderId]);
          }
        }
      }
    }
    return $result;
  }


    /**
     * Metoda pobiera listę produktów do potwierdzenia
     *
     * @param object $pSmarty
     * @param array $aListData
     * @param string $sPackageIdent
     */
    protected function getConfirmBookList(&$pSmarty, $aListData, $sPackageIdent)
    {

        $aListItems = $this->_getConfirmedBooks($aListData['id']);

        if ('2' === $aListData['type']) {
          $aListItems = $this->mergeLinkedOrders($aListItems);
        }
        // merge linked orders
        if ($aListData['closed'] == '0') {
            // nadajemy numer półkom
            $iCounter = 1;
            foreach ($aListItems as $iKey => $aOrderItems) {
                $aValues = array(
                    'shelf_number' => $iCounter,
                );
                foreach ($aOrderItems as $iItemKey => $aItem) {
                    $this->pDbMgr->Update('profit24', 'orders_items_lists_items', $aValues, ' id = ' . $aItem['id']);
                    $aListItems[$iKey][$iItemKey]['shelf_number'] = $iCounter;
                }
                $iCounter++;
            }
            $this->pDbMgr->Update('profit24', 'orders_items_lists', array('closed' => '2'), ' id=' . $aListData['id']);
        }


        // ok zmienione zostały
        // wyświetlamy listę
        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('ordered_itm', _('Sortowanie książek do półek'), array('action' => phpSelf(array('id' => $aListItems['id']))), array('col_width' => 155), false);
        $pForm->AddHidden('do', 'confirm_list');
        $_SESSION['token'] = md5(session_id() . time());
        $_SESSION['sort_time_'.$aListData['id']] = time();
        $pForm->AddHidden('token', $_SESSION['token']);

        $pForm->AddHidden('pack_number', $sPackageIdent);
        $pForm->AddHidden('orders_items_lists_id', $aListData['id']);
        $pForm->AddHidden('books', '');
        $pForm->AddRow('Lista numer', str_pad($aListData['id'], 13, '0', STR_PAD_LEFT));
        $pForm->AddRow('Pobrana:', $aListData['created']);

        $aBooks = array();
        foreach ($aListItems as $aItems) {
            foreach ($aItems AS $aItem) {
                $aBooks[] = $aItem;
            }
        }
        $sHTML = $this->sorterView($pSmarty, $aBooks, $aListData);

        //$aBooks = $this->getBooksNew();
        //$this->GetBooksList($pSmarty, $aBooks)
        $pForm->AddMergedRow($sHTML);

        if (!empty($aListItems)) {
            $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz'), array('style' => 'font-size: 40px;', 'onclick' => "return checkIfNoMissing(this);")));
        }

        $pForm->AddRow('&nbsp;',
            $pForm->GetInputButtonHTML('show_not_scanned', _('Pokaż braki /<br> ПОКАЗАТЬ НЕПОДТВЕРЖДЁННОЕ'), array('style' => 'font-size: 40px; float:left ;', 'onclick' => 'veryfiList();'), 'button') . ' &nbsp; ' .
            $pForm->GetInputButtonHTML('cancel', _('Anuluj'), array('style' => 'font-size: 40px; float:right;', 'onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show')) . '\');'), 'button')
        );

        $sInput = '
    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" /></div>

    <script type="text/javascript" src="js/searchSorterISBN.js?date=20171201"></script>
    <script type="text/javascript">
      $(function() {
        $("#pack_number").focus();
      });
    </script>
    <script type="text/javascript">
      $(function() {
          $(parent.leftMenu.document).children().append("<div id=\"menuOverlay\"  style=\"background: rgba(0,0,0,.5); width:100%; height:100%; position:fixed; top:0; left:0; z-index:999; text-align: center\">");
          $(parent.document).find(".headerBg").find("td").first().css("position","relative").append("<div id=\"headerOverlay\" style=\"background: rgba(0,0,0,.3); width:100%; height:100%; text-align: center; top: 0; position: absolute;\">");
      });
      $( window ).on("unload", function() {
        $.LoadingOverlay("show", {
          color: "rgba(255, 255, 255, 0.4)"
        });
        $(parent.leftMenu.document).find("#menuOverlay").remove();
        $(parent.document).find(".headerBg").find("td").first().css("position","").find("#headerOverlay").remove();
      });
      $(window).bind(\'beforeunload\', function(e) {
          if ($("#do").val() != "save_big_list") {
              if(!checkIfNoMissing()) {
                  $.LoadingOverlay("show", {
                    color: "rgba(255, 255, 255, 0.4)"
                  });
                  $(".loadingoverlay").css("display", "none");
                  return "";
              }
          }
      });
      function checkIfNoMissing() {
          if ((quantityAllBooks - iScanned) != 0) {
              alert("NIe wszystkie produkty zostały zeskanowane");
          }
          return (quantityAllBooks - iScanned) == 0;
      }
    </script>';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $sInput . ShowTable($pForm->ShowForm()));
    }// end of getConfirmBookList() method


    /**
     * Metoda potwierdza rodzica
     *
     * @param type $iParentId
     * @return boolean
     */
    private function confirmParents($iParentId)
    {

        // sprawdzamy czy wszystkie inne tego parenta są też potwierdzone
        // wszystkie są potiwerdzone - nie usunite
        $sSql = 'SELECT COUNT(id) 
             FROM orders_items 
             WHERE parent_id = ' . $iParentId . ' 
               AND `status` <> "4" 
               AND `deleted` = "0" ';
        if ($this->pDbMgr->GetOne('profit24', $sSql) == 0) {

            $aValues = array(
                '`status`' => '4'
            );
            if ($this->pDbMgr->Update('profit24', 'orders_items', $aValues, ' id = ' . $iParentId) === FALSE) {
                return false;
            }
        }
        return true;
    }// end of confirmParents() method


    /**
     * Metoda wyświetla widok sortowania produktów
     *
     * @param object $pSmarty
     * @param array $aBooks
     * @param $aListData
     * @return string - html
     */
    private function sorterView(&$pSmarty, $aBooks, $aListData)
    {
        $sJS = json_encode($aBooks);
        $aSmartyData['js'] = $sJS;
        $aSmartyData['is_big'] = $aListData['big'];

        $pSmarty->assign('aData', $aSmartyData);
        $sHtml = $pSmarty->fetch('sorter.tpl');
        $pSmarty->clear_assign('aBooks');
        return $sHtml;
    }// end of sorterView() method


    /**
     * Metoda wyśw widok pobierania numeru paczki
     *
     * @param object $pSmarty
     */
    public function GetPackageNumber(&$pSmarty)
    {
        $aData = array();
        if (!empty($_POST)) {
            $aData = &$_POST;
        }
        // wybieramy numer paczki
        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('ordered_itm', _("Sortownik"), array('action' => phpSelf()), array('col_width' => 155), false);
        $pForm->AddHidden('do', 'setPackage');
        $pForm->AddText('pack_number', _('Numer kuwety /<br> НОМЕР ЯЩИКА:<br>'), $aData['pack_number'], array('style' => 'font-size: 25px;'));
        $pForm->AddInputButton('send', _('Dalej'), array('style' => 'font-size: 40px;'), 'submit');
        $sJS = '
    <script type="text/javascript">
      $(function() {
        $("#pack_number").focus();
      });
    </script>';
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()) . $sJS);
    }

    /**
     * @param int $iOrderItemListId
     * @param int $ordersItemsListsItemsId
     */
    private function addToTimeStats($iOrderItemListId, $scQuantity, $ordersItemsListsItemsId)
    {
        global $aConfig;

        if (isset($_SESSION['sort_time_'.$iOrderItemListId])) {
            $stop = time();
            $scTime = $stop - $_SESSION['sort_time_'.$iOrderItemListId];
            $scTime = $scTime * 1000;
            unset($_SESSION['sort_time_'.$iOrderItemListId]);
        } else {
            $scTime = 0;
        }
        $sContent = '$iOrderItemListId: '.$iOrderItemListId.' $scQuantity'.$scQuantity;
        file_put_contents("orders_items_lists_items_times.log", $sContent, FILE_APPEND | LOCK_EX);
        include_once($aConfig['common']['client_base_path'].'/omniaCMS/modules/m_zamowienia_magazyn_zbieranie_tramwaj/Common_get_ready_orders_books.class.php');
        $commonGetReadyOrders = new Common_get_ready_orders_books($this);
        $commonGetReadyOrders->AddOIILITimeItem($ordersItemsListsItemsId, $scTime , $scQuantity, '2');
    }// end of GetPackageNumber() method
}// end of Class
