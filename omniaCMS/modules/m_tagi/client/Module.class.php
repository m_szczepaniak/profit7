<?php
/**
 * Klasa odule do obslugi stron modulu 'Tagi'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */
 

class Module {

  // Id strony
  var $iPageId;
  
  // szablon strony
  var $sTemplate;
  
  // symbol modulu
  var $sModule;
  
  // link do strony
  var $sPageLink;
    
  // czesc nazwy pliku cache
  var $sCacheName;
  
  // ustawienia strony aktualnosci
  var $aSettings;
  
	function Module(){
  	global $aConfig;
   	
  	$this->sCacheName = md5($_SERVER['REQUEST_URI']);
   	$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->sTemplate = 'modules/'.$this->sModule.'/'.'domyslny.tpl';
		$this->sPageLink .= '/'.$aConfig['_tmp']['page']['symbol'];
		$this->aSettings =& $this->getSettings();
  }
  
  	/**
	 * Metoda pobiera konfiguracje dla strony aktualnosci
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aCfg = unserialize($oCache->get($this->sCacheName, 'modules_settings')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT * 
							 FROM ".$aConfig['tabls']['prefix']."products_settings 
							 WHERE page_id=".$this->iPageId;
							 
			$aCfg = Common::GetRow($sSql);
			$aCfg['items_per_page'] = (int) $aCfg['items_per_page'];
			$aCfg['category_id']=intval($aCfg['item_template']);
		
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aCfg), $this->sCacheName, 'modules_settings');
		}
		return $aCfg;
	} // end of getSettings() function
  
	/**
	 * Metoda pobiera liste podstron strony o id $this->iPageId
	 * dowiazuje ja do Smartow oraz zwraca xHTML z lista podstron po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty, $oCache;
		
		if (!$aConfig['common']['caching'] || false === ($sHtml = $oCache->get($this->sCacheName, 'modules'))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie i przetworzenie listy tagow
			$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		
			$aModule['items'] =& $this->getTags();
			
			if (!empty($aModule['items'])) {
				$pSmarty->assign_by_ref('aModule', $aModule);
				$sHtml = $pSmarty->fetch($this->sTemplate);
				$pSmarty->clear_assign('aModule', $aModule);
			}
			else {
				setMessage($aModule['lang']['no_data']);
			}
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save($sHtml, $this->sCacheName, 'modules');
		}
		return $sHtml;
	} // end of getContent()
	
	/**
	 * Metoda pobiera liste tagow oraz ich ilosc\
	 */
	function getTags() {
		global $aConfig;
		
		
		
	/*$sSql = "SELECT count(tag)
						 FROM ".$aConfig['tabls']['prefix']."products_tags
						 WHERE quantity > 0".
						 ($this->aSettings['category_id']>0?" AND page_id = ".$this->aSettings['category_id']:'');
		if (($iRecords = (int) Common::GetOne($sSql)) > 0) {*/
			// okreslenie liczby rekordow na stronie
			$iPerPage = (int) $this->aSettings['items_per_page'];
			$sSql = "SELECT tag,  quantity
						 FROM ".$aConfig['tabls']['prefix']."products_tags
						 WHERE quantity > 0".
						 ($this->aSettings['category_id']>0?" AND page_id = ".$this->aSettings['category_id']:'')."
						  ORDER BY quantity DESC".
						  ($iPerPage>0?" LIMIT 0, ".$iPerPage:'');
			/* stronicowanie
			  if ($iPerPage > 0) {
				// uzycie pagera do stronicowania,
				include_once('Pager/CPager.class.php');
				$aPagerParams = array();
				$aPagerParams['perPage'] = $iPerPage;
				$aPagerParams['path'] = $aConfig['common']['base_url_http_no_slash'].$this->sPageLink.'/';
				$aPagerParams['fileName'] = 'p%d.html';
				$aPagerParams['append'] = false;
	
				$oPager = new CPager($iRecords, $aPagerParams);
				$iStartFrom = $oPager->getStartFrom();
	
				// linki Pagera
				$aData['pager']['links'] =& $oPager->getLinks('all');
				$aData['pager']['total_pages'] =& $oPager->getLinks('total_pages');
				$aData['pager']['total_items'] =& $oPager->getLinks('total_items');
				$aData['pager']['current_page'] =& $oPager->getLinks('current_page');
	
				// dodanie wybrania zakresu do zapytania
				$sSql .= " LIMIT ".$iStartFrom.", ".$iPerPage;
			}
		*/
			$aData['tags'] =& Common::GetAll($sSql);	
			if(!empty($aData['tags'])){
				foreach($aData['tags'] as $iKey => $aValue){
					if($iKey < 6) $aData['tags'][$iKey]['tag'] = "&nbsp;<a href='".getTagLink($aValue['tag'])."' class='size_".($iKey+1)."'>".$aValue['tag']."</a>&nbsp;";
					else $aData['tags'][$iKey]['tag'] = "&nbsp;<a href='".getTagLink($aValue['tag'])."'>".$aValue['tag']."</a>&nbsp;";
				}		
				asort($aData['tags']);
			}
		//}
		return $aData;
	} // end of getContent()
} // end of Module Class
?>