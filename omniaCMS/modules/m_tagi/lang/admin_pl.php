<?php
/**
* Plik jezykowy dla interfejsu modulu 'Powiazane oferta produktowa' Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/


/*---------- ustawienia oferty produktowej */
$aConfig['lang']['m_tagi_settings']['settings'] = "Konfiguracja strony oferty produktowej";
$aConfig['lang']['m_tagi_settings']['templates'] = "Szablony";
$aConfig['lang']['m_tagi_settings']['list_template'] = "Szablon";
$aConfig['lang']['m_tagi_settings']['items_per_page'] = "Tagów na stronie";
$aConfig['lang']['m_tagi_settings']['show_page_id'] = "Strona";

/*---------- ustawienia boksow */
$aConfig['lang']['m_tagi_box_settings']['page_id'] = "Strona";
$aConfig['lang']['m_tagi_box_settings']['items_in_box'] = "Tagów w boksie";
$aConfig['lang']['m_tagi_box_settings']['category_id'] = "Kategoria";

?>