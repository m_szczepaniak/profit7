<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - serie wydawnicze
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_serie']['no_data'] = 'W chwili obecnej nie posiadamy zdefiniowanych serii wydawniczych.';
?>