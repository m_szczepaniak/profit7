<?php
namespace omniaCMS\modules\m_multilocalization;

use Admin;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\SellPredict;
use LIB\EntityManager\Entites\StockLocation;
use LIB\FixAutomats\stockLocationFixer;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use View;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 19.01.18
 * Time: 08:52
 */
class Module__multilocalization__missing implements ModuleEntity, SingleView
{

    /**
     *
     * @var string
     */
    private $sMsg;

    /**
     *
     * @var string
     */
    private $sModule;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;
    private $stockLocation;
    private $sellPredict;

    public function __construct(Admin $oClassRepository) {

        $this->oClassRepository = $oClassRepository;
        $this->em = $this->oClassRepository->entityManager;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
        $this->stockLocation = new StockLocation($this->pDbMgr);
        $this->sellPredict = new SellPredict($this->pDbMgr);
    }

    public function doDefault()
    {
        global $aConfig;
        $aData = array();


        if (!empty($_POST)) {
            $aData =& $_POST;
        }

        if (!isset($aData['f_active'])) {
            $aData['f_active'] = '1';
        }

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header'	=> _('Braki na zbieraniu'),
            'refresh'	=> true,
            'search'	=> true,
            'items_per_page' => 20,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 2,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'db_field'	=> 'name',
                'content'	=> _('Tytuł'),
                'sortable'	=> true,
                'width' => '300'
            ),
            array(
                'db_field'	=> 'ean_13',
                'content'	=> _('EAN 13'),
                'sortable'	=> true,
            ),
            array(
                'db_field'	=> 'active',
                'content'	=> _('Aktywne'),
                'sortable'	=> true,
            ),
            array(
                'db_field'	=> 'order_number',
                'content'	=> _('Numery zamówień'),
                'sortable'	=> true,
            ),
            array(
                'db_field'	=> 'required_quantity',
                'content'	=> _('Wymagana ilość'),
                'sortable'	=> true,
            ),
            array(
                'db_field'	=> 'available_quantity',
                'content'	=> _('Dostępna ilość - (na moment sprawdzania)'),
                'sortable'	=> true,
            ),
            array(
                'db_field'	=> 'created',
                'content'	=> _('Wykryto'),
                'sortable'	=> true,
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '40'
            )
        );

        $aColSettings = array(
            'id'	=> array(
                'show'	=> false
            ),
            'action' => array(
                'actions' => array('fixLocations'),
                'params' => array(
                    'fixLocations' => array('id' => '{id}'),
                ),
                'icon' => array(
                    'fixLocations' => 'publish',
                ),
            )
        );

        include_once('View/View.class.php');
        $oView = new View(_('bank_accounts'), $aHeader, $aAttribs);

        //MHI.*, MHO.id as missing_high_oili_id
        $aCols = array('P.id', 'P.name', 'P.ean_13', '"1" AS active', 'GROUP_CONCAT(O.order_number SEPARATOR ", ") as order_number', 'OI.quantity AS required_quantity', 'SLM.available_quantity', 'SLM.created');

        $sSearch = '';
        if (isset($_POST['search']) && $_POST['search'] != '') {
            $sSearch = ' AND ( 
                          P.id = ' . (int)$_POST['search'] . ' OR 
                          P.isbn_plain LIKE \'' . isbn2plain($_POST['search']) . '\' OR 
                          P.isbn_10 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                          P.isbn_13 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                          P.ean_13 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                          P.streamsoft_indeks LIKE \'' . isbn2plain($_POST['search']) . '\'
                       )';
        }



        $sSql = '
              SELECT %cols
              FROM `stock_location_missing` AS SLM 
              JOIN products AS P 
                ON P.id = SLM.product_id
              JOIN orders_items AS OI
                ON OI.id = SLM.order_item_id AND OI.status <> "4" AND OI.source = "51" AND OI.deleted = "0"
              JOIN orders AS O
                ON O.id = OI.order_id AND O.order_status <> "4" AND O.order_status <> "5" AND O.order_on_list_locked = "0"
             WHERE 
             1=1
              '.($aData['f_active'] != '1' ? '' : '
              AND 
              (
                SELECT IFNULL(SUM(available), 0) 
                FROM stock_location AS SL
                WHERE SL.products_stock_id = SLM.product_id
              )
              < OI.quantity
              AND 
              (SELECT id FROM sell_predict WHERE product_id = P.id AND status <> "'.SellPredict::STATUS_INACTIVE.'" AND status <> "'.SellPredict::STATUS_CLOSED.'" LIMIT 1 ) IS NULL
              
              ') .'             
              %filters
              '.$sSearch.'              
              
             ';
        //

        $sGroupBySQL = 'GROUP BY P.id';
        $aSearchCols = array();
        $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, '', $sGroupBySQL, ' SLM.created ASC ');
        $oView->AddRecordsHeader($aRecordsHeader);
        $oView->setActionColSettings($aColSettings);

        $aActive = [
            [
                'label' => 'Tak',
                'value' => '1'
            ]
        ];
        $oView->AddFilter('f_active', _('Aktywne'), addDefaultValue($aActive, _('Wszystkie')), $aData['f_active']);

        $aRecordsFooter = array(array(),array('add'));
        $oView->AddRecordsFooter($aRecordsFooter);
        return ShowTable($oView->Show());
    }

    /**
     *
     */
    public function doFixLocations()
    {
        if (isset($_GET['id'])) {
            $iProductId = $_GET['id'];
        }

        $stockLocationFixer = new stockLocationFixer($this->pDbMgr);
        $productStockLocation = $stockLocationFixer->fixProductStockLocation($iProductId);
        if (!empty($productStockLocation)) {
            $this->sMsg .= GetMessage('Naprawiono rezerwacje', false);
        } else {
            $this->sMsg .= GetMessage('Nic nie naprawiono');
        }
        return $this->doDefault();
    }

    /**
     * @param array $aItem
     * @return array
     */
    public function parseViewItem(array $aItem)
    {

        $available = $this->stockLocation->getProductMagazineAvailableQuantity($aItem['id'], Magazine::TYPE_HIGH_STOCK_SUPPLIES);
        $available += $this->stockLocation->getProductMagazineAvailableQuantity($aItem['id'], Magazine::TYPE_LOW_STOCK_SUPPLIES);
        $sellPredictId = $this->sellPredict->getActiveProductOnPredict($aItem['id']);

        if ($available >= $aItem['required_quantity']) {
            $aItem['active'] = 'nieaktywne';
        } else {
            $aItem['active'] = 'aktywne';
        }
        if ($sellPredictId > 0) {
            $aItem['active'] .= ' - jest już na liście doładowań';
        }
        $aItem['available_quantity'] = $available;
        return $aItem;
    }

    public function resetViewState()
    {
        resetViewState();
    }

    public function getMsg()
    {
        return $this->sMsg;
    }
}