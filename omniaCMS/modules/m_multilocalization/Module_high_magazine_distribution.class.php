<?php
namespace omniaCMS\modules\m_multilocalization;

use Admin;
use Exception;
use LIB\EntityManager\Entites\Containers;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\EntityManager\Entites\SellPredict;
use LIB\EntityManager\Entites\StockLocation;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use View\touchKeyboard;

class Module__multilocalization__high_magazine_distribution extends OrdersItemsLists implements ModuleEntity, SingleView  {

    /**
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    private $containers;

    protected $sellPredict;

    public function __construct(Admin $oClassRepository) {

        $this->oClassRepository = $oClassRepository;
        $this->em = $this->oClassRepository->entityManager;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
        $this->containers = new Containers();
        $this->sellPredict = new SellPredict($this->pDbMgr);
    }


    public function parseViewItem(array $aItem)
    {
        return $aItem;
    }

    public function resetViewState()
    {
        resetViewState();
    }

    public function getMsg()
    {

        $sMsg = strip_tags($this->sMsg);
        if (!empty($sMsg)) {
            AddLog($sMsg);
        }

        $sJS = '
        <script>
      $(function() {
        var komunikat = $(".message1").html();
        if (komunikat !== undefined && komunikat !== "") {
          new Messi(komunikat, {title: "Błąd", modal: true});
        }
      });
      </script>
      ';

        return $this->sMsg.$sJS;
    }


    /**
     *
     */
    public function doDefault()
    {
        include_once('Form/FormTable.class.php');
        $form = new \FormTable('scan_products', _('Skanuj produkt'));
        $form->AddHidden('do', 'saveProducts');
        $form->AddHidden('sc_date', date('YmdHis'));

        $form->AddMergedRow('', array('id' => 'sc_products', 'readonly' => 'readonly', 'style' => 'font-size:15px; width: 90%; height: 150px', 'class' => 'editable_input'));
        $sCheckbox = $form->GetCheckBoxHTML('ask_quantity', _('Zapytaj o ilość'), [], '', false, false);
        $sButton = $form->GetInputButtonHTML("submit", _('Potwierdź / ПОДТВЕРДИТЬ'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
        $form->AddMergedRow($sButton);

        $touchKeyboard = new touchKeyboard($this->pSmarty);
        $touchKeyboardHTML = $touchKeyboard->getKeyboardNumeric();


        $input = '
    <div id="scanner_pool" style="padding: 20px;">
        <div style="float: left">ISBN: <input id="product_isbn" type="text" value="" style="font-size: 26px;" /></div>
        <div style="float: left; margin-left:10px">Ilość: <input id="product_quantity" type="text" value="" style="font-size: 26px;" /></div>
        <div style="float: left; margin-left:10px">Lokalizacja: <input id="location" type="text" value="" style="font-size: 26px;" /></div>
    </div>
    <script type="text/javascript" src="js/high_magazine_distribution.js?date=07022018"></script>';
        $content = $input . ShowTable($form->ShowForm());
        $content .= $touchKeyboardHTML;

        $content .= $this->showLog();

        return $content;
    }

    /**
     * @return string
     */
    public function doSaveProducts()
    {
        $this->pDbMgr->BeginTransaction('profit24');

        $scannedProducts = $_POST['scanned_products'];

        try {
            if (empty($scannedProducts)) {
                throw new \Exception(_('Brak zeskanowanych produktów / ПРОДУКТА НЕ СУЩЕСТВУЕТ'));
            }
            foreach($scannedProducts as $keyIdLocation => $product) {
                $results = explode('_', $keyIdLocation);
                if (isset($results[0])) {
                    $productId = $results[0];
                } else {
                    $productId = $keyIdLocation;
                }

                $this->createHighLevelMiss($productId, $product['quantity'], $product['location']);
                $this->saveToLog($productId, $product['quantity'], $product['location']);
                $d = '';
            }


            $this->sMsg .= GetMessage(_('Zapisano rozkładanie'), false);
            $this->pDbMgr->CommitTransaction('profit24');
            return $this->doDefault();
        } catch(Exception $e) {
            $this->sMsg .= GetMessage($e->getMessage());
            $this->pDbMgr->RollbackTransaction('profit24');
            return ;
        }
    }

    /**
     * @param $productId
     * @param $productQuantity
     * @param $productLocation
     * @throws Exception
     */
    private function createHighLevelMiss($productId, $productQuantity, $productLocation)
    {
        //TODO odkomentowac, dla testow zakomentowane
//        $this->checkLocation($productLocation);

        $parsedLists = $this->proceedLists($productId, $productQuantity, $productLocation);
        $filteredList = array_filter($parsedLists, function($element){
            return $element['should_update'] == true;
        });

        $this->updateChangedLists($filteredList);
        $this->saveMissing($filteredList, $productId);
        $this->removeHighStockReservation($filteredList);
        $this->addLowStockReservation($filteredList, $productId, $productLocation);

        $d = '';
    }

    /**
     * @param $productId
     * @param $productQuantity
     */
    private function findCloseSellPredict($productId, $productQuantity) {

        $sSql = 'SELECT id, quantity
                 FROM sell_predict
                 WHERE product_id = "'.$productId.'"
                 AND status <> "'.SellPredict::STATUS_INACTIVE.'"
                 AND status <> "'.SellPredict::STATUS_CLOSED.'"
                 ORDER BY status DESC, id ASC';
        $sellPredict = $this->pDbMgr->GetAll('profit24', $sSql);

        foreach ($sellPredict as $row) {
            if ($productQuantity > 0) {
                if ($row['quantity'] <= $productQuantity) {
                    $productQuantity -= $row['quantity'];
                    $this->sellPredict->setStatus($row['id'], SellPredict::STATUS_INACTIVE);
                }
            } else {
                break;
            }
        }
    }

    /**
     * @param $location
     * @return bool
     * @throws Exception
     */
    private function checkLocation($location)
    {
        if (($container = $this->containers->findContainer($location)) === null) {
            throw new Exception("Nieprawidlowa kuweta");
        }

        return true;
    }

    /**
     * @param array $parsedLists
     * @param $productId
     * @param $productLocation
     * @throws Exception
     */
    public function addLowStockReservation(array $parsedLists, $productId, $productLocation)
    {
        $sql = "SELECT SLOILI.*, SL.id AS sl_id, SL.quantity AS sl_quantity, SL.available AS sl_available, SL.reservation AS sl_reservation
                FROM stock_location_orders_items_lists_items AS SLOILI
                JOIN stock_location AS SL ON SL.id = SLOILI.stock_location_id
                JOIN sell_predict AS SP
                  ON SLOILI.orders_items_lists_items_id = SP.orders_items_lists_items_id AND SP.status = '".SellPredict::STATUS_COMPLETATION."'
                WHERE SLOILI.orders_items_lists_items_id = %d
                AND SL.magazine_type = '%s'
                AND SLOILI.type = '%s'
                AND SLOILI.status = '1'
                AND SL.container_id = '%s'
                ORDER BY SLOILI.id DESC
                ";

        foreach($parsedLists as $parsedList) {
            $sSql = 'SELECT magazine_type FROM containers WHERE id = "'.$productLocation.'"';
            $magazineType = $this->pDbMgr->GetOne('profit24', $sSql);

            $existRow = $this->pDbMgr->GetRow('profit24', sprintf($sql, $parsedList['id'], $magazineType, StockLocationOrdersItemsListsItems::TYPE_MM, $productLocation));

            if (empty($existRow)) {
                // @TODO nie odejmuje z wysokiej lokalizacji ilości
                $stockLocation = new StockLocation($this->pDbMgr);
                $stockLocationId = $stockLocation->findAddSumLocation($productId, $productLocation, $magazineType, $parsedList['distributed'], 0, $parsedList['distributed']);
                if (false === $stockLocationId){
                    throw new Exception("Wystapil blad podczas zapisu danych");
                }

                /*
                $stockLocationValues = [
                    'magazine_type' => StockLocation::LOW_MAGAZINE_TYPE,
                    'container_id' => $productLocation,
                    'products_stock_id' => $productId,
                    'quantity' => $parsedList['confirmed_quantity'],
                    'reservation' => 0,
                    'available' => $parsedList['confirmed_quantity']
                ];

                if (false === $stockLocationId = $this->pDbMgr->Insert('profit24', 'stock_location', $stockLocationValues)) {
                    throw new Exception("Wystapil blad podczas zapisu danych");
                }
                */


                $childrenValues = [
                    'stock_location_id' => $stockLocationId,
                    'stock_location_magazine_type' => $magazineType,
                    'orders_items_lists_items_id' => $parsedList['id'],
                    'reserved_quantity' => $parsedList['distributed'],
                    'type' => StockLocationOrdersItemsListsItems::TYPE_MM_PLUS,
                    'created_by' => $_SESSION['user']['name'],
                    'created' => 'NOW()'
                ];

                if (false === $this->pDbMgr->Insert('profit24', 'stock_location_orders_items_lists_items', $childrenValues)) {
                    throw new Exception("Wystapil blad podczas zapisu danych");
                }
            }
            else {
                // istnieje sumujemy ilości

                $stockLocation = new StockLocation($this->pDbMgr);
                $stockLocationId = $stockLocation->findAddSumLocation($productId, $productLocation, $magazineType, $parsedList['distributed'], 0, $parsedList['distributed']);
                if (false === $stockLocationId){
                    throw new Exception("Wystapil blad podczas zapisu danych");
                }

                $updateValues = [
                    'reserved_quantity' => $existRow['reserved_quantity'] + $parsedList['distributed'],
                ];

                if (false === $this->pDbMgr->Update('profit24', 'stock_location_orders_items_lists_items', $updateValues, "id = ".$existRow['id'])) {
                    throw new Exception("Wystapil blad podczas zapisu danych");
                }
            }
            $this->findCloseSellPredict($productId, $parsedList['confirmed_quantity']);
        }
    }

    /**
     * @param array $parsedLists
     * @throws Exception
     */
    private function removeHighStockReservation(array $parsedLists)
    {
        $stockLocation = new StockLocation($this->pDbMgr);

        $sql = "SELECT SLOILI.*, SL.id AS sl_id, SL.quantity AS sl_quantity, SL.available AS sl_available, SL.reservation AS sl_reservation
                FROM stock_location_orders_items_lists_items AS SLOILI
                JOIN stock_location AS SL ON SL.id = SLOILI.stock_location_id
                WHERE SLOILI.orders_items_lists_items_id = %d
                AND SL.magazine_type = '%s'
                AND SLOILI.type = '%s'
                AND SLOILI.status = '1'
                ORDER BY SLOILI.id DESC
                ";

        foreach($parsedLists as &$parsedList) {
            $sSql = sprintf($sql, $parsedList['id'], StockLocation::HIGH_MAGAZINE_TYPE, StockLocationOrdersItemsListsItems::TYPE_MM);
            $aSupplies = $this->pDbMgr->GetAll('profit24', $sSql);

            $aSupplies = $this->getSuppliesToConfirm($aSupplies, $parsedList['distributed']);
            if (empty($aSupplies)) {
                throw new Exception("Rezerwacja na wysokim skladowaniu nie istnieje");
            }
            foreach ($aSupplies as $row) {
                $currentReservation = $row['sl_reservation'] - $row['_own_reserved'];
                $quantityFull =  $row['sl_quantity'] - $row['_own_reserved'];

                // zabezpieczenie przeciw ujemnym rezerwacjom
                if ($currentReservation < 0) {
                    $currentReservation = 0;
                }
                $availableQuantity = $quantityFull - $currentReservation;

                if (false === $stockLocation->changeStockLocationQuantity($row['stock_location_id'], $quantityFull, $currentReservation, $availableQuantity)) {
                    throw new Exception('Wystąpil blad podczas aktualizacji');
                }

                $childrenValues = [
                    'confirmed_quantity' => $row['confirmed_quantity'],
                    'status' => $row['status']
                ];

                if (false === $this->pDbMgr->Update('profit24', 'stock_location_orders_items_lists_items', $childrenValues, ' id=' . $row['id'])) {
                    throw new Exception('Wystąpil blad podczas aktualizacji');
                }
            }
        }
    }

    /**
     * @param array $parsedLists
     * @param $productId
     * @throws Exception
     */
    private function saveMissing(array $parsedLists, $productId)
    {
        $sql = "
              SELECT MHI.*, MHO.id as missing_high_oili_id FROM missing_high_items AS MHI
              JOIN missing_high_oili as MHO ON MHO.id = MHI.missing_high_oili_id
              WHERE MHO.oili_id = %d
              ";

        foreach($parsedLists as $parsedList) {

            $existMissing = $this->pDbMgr->GetRow('profit24', sprintf($sql, $parsedList['id']));

            //sprawdzamy czy nie rozwiazac braku (usunac)
            if (!empty($existMissing) && ($parsedList['confirmed'] == true)) {

                // Delete
                if (false === $this->pDbMgr->Query('profit24', "DELETE FROM missing_high_items WHERE id = ".$existMissing['id'])) {
                    throw new Exception('Wystapil blad podczas usuwania');
                }


                $missingHighOiliItems = $this->pDbMgr->GetOne('profit24', "SELECT COUNT(*) FROM missing_high_items WHERE missing_high_oili_id = ".$existMissing['missing_high_oili_id']);
                if (!is_numeric($missingHighOiliItems)) {
                    throw new Exception('Blad w zliczaniu pozostalych elementow');
                }

                if ($missingHighOiliItems == 0) {
                    if (false === $this->pDbMgr->Query('profit24', "DELETE FROM missing_high_oili WHERE id = ".$existMissing['missing_high_oili_id'])) {
                        throw new Exception('Wystapil blad podczas usuwania oili item');
                    }
                }

                continue;
            }

            if (empty($existMissing)) {

                // Insert
                $values = [
                    'oili_id' => $parsedList['id']
                ];

                if (false === $missingHightOiliId = $this->pDbMgr->Insert('profit24', 'missing_high_oili', $values)) {
                    throw new Exception('Wystapil blad podczas dodawania');
                }

                $childrenValues = [
                    'product_id' => $productId,
                    'missing_high_oili_id' => $missingHightOiliId,
                    'quantity' => $parsedList['distributed'],
                ];

                if (false === $this->pDbMgr->Insert('profit24', 'missing_high_items', $childrenValues)) {
                    throw new Exception('Wystapil blad podczas dodawania');
                }
            } else {

                // Update

                $updateValues = [
                    'quantity' => $parsedList['available_quantity']
                ];

                if (false === $this->pDbMgr->Update('profit24', 'missing_high_items', $updateValues, "id = ".$existMissing['id'])) {
                    throw new Exception('Wystapil blad podczas aktualizacji');
                }
            }
        }
    }

    /**
     * @param array $parsedLists
     * @throws Exception
     */
    private function updateChangedLists(array $parsedLists)
    {
        foreach($parsedLists as $parsedList) {

            $values = [
                'confirmed_quantity' => $parsedList['confirmed_quantity'],
                'confirmed' => ($parsedList['confirmed'] == true || $parsedList['confirmed'] == '1' ? '1' : '0'),
            ];

            $id = $parsedList['id'];

            if (false === $this->pDbMgr->Update('profit24', 'orders_items_lists_items', $values, "id = $id")) {
                throw new \Exception("Wystapil blad podczas aktualizowania OILI");
            }


            if ($parsedList['confirmed'] == true || $parsedList['confirmed'] == '1' ) {
                // zamknąliśmy tytuł spróbujmy zamknąć całą listę
                $sSql = 'SELECT id
                     FROM orders_items_lists_items
                     WHERE confirmed = "0" 
                     AND orders_items_lists_id = '.$parsedList['orders_items_lists_id'];
                $existsOpen = $this->pDbMgr->GetOne('profit24', $sSql);

                // jeśli nie ma żadnego
                if (intval($existsOpen) <= 0) {
                    $valueUp = [
                        'closed' => '1'
                    ] ;
                    $this->pDbMgr->Update('profit24', 'orders_items_lists', $valueUp, ' id='.$parsedList['orders_items_lists_id']);
                }
            }
        }
    }

    /**
     * @param $productId
     * @param $productQuantity
     * @param $productLocation
     * @return array
     * @throws Exception
     */
    private function proceedLists($productId, $productQuantity, $productLocation)
    {
        $stockLocation = new OrdersItemsLists($this->pDbMgr);
        $highLevelLocationLists = $stockLocation->findOpenHighStockLevelProductList($productId);

        if (empty($highLevelLocationLists)) {
            throw new Exception("<h1>Błąd brak elementów do rozłożenia na wysokim skladowaniu !! / НЕПРАВИЛЬНО ВВЕДЕНЫ ДАННЫЕ. ПОПРОБУЙТЕ ЕЩЁ РАЗ</h1>");
        }

        $parsedLists = [];

        $quantityToDistribute = $productQuantity;

        foreach($highLevelLocationLists as $highLevelLocationElement) {

            $confirmed = 0;

            // jak wszystko zostalo odznaczone to jedziemy z listami do konca oznaczajacz reszte jako braki
            if ($quantityToDistribute <= 0) {

                $parsedLists[] = [
                    'id' => $highLevelLocationElement['id'],
                    'orders_items_lists_id' => $highLevelLocationElement['orders_items_lists_id'],
                    'available_to_distribute' => 0,
                    'confirmed' => $confirmed,
                    'should_update' => false,
                    'confirmed_quantity' => 0,
                    'available_quantity' => $highLevelLocationElement['quantity']
                ];

                continue;
            }

            $availableQuantity = $highLevelLocationElement['quantity'] - $highLevelLocationElement['confirmed_quantity'];

            // czy możemy coś w tym zbieraniu odznaczyć
            if ($availableQuantity < 1) {
                continue;
            }

            $availableToDistribute = $quantityToDistribute;

            if (($availableQuantity - $productQuantity) < 1) {
                $availableToDistribute = $availableQuantity;
            }

            $quantityToDistribute -= $availableToDistribute;

            $confirmedQuantity = $highLevelLocationElement['confirmed_quantity'] + $availableToDistribute;
            $confirmed = $confirmedQuantity == $highLevelLocationElement['quantity'] ? true : false;

            $parsedLists[] = [
                'id' => $highLevelLocationElement['id'],
                'orders_items_lists_id' => $highLevelLocationElement['orders_items_lists_id'],
                'distributed' => $availableToDistribute,
                'confirmed' => $confirmed,
                'should_update' => true,
                'confirmed_quantity' => $confirmedQuantity,
                'available_quantity' => $highLevelLocationElement['quantity'] - $confirmedQuantity
            ];
        }

        if ($quantityToDistribute > 0) {
            throw new Exception("Ilości na wysokim skladowaniu i ilosc zeskanowana sie nie zgadza, produkt: $productId");
        }

        return $parsedLists;
    }


    /**
     * @param $aSupplies
     * @param $confirmedQuantity
     * @return array
     * @throws Exception
     */
    private function getSuppliesToConfirm($aSupplies, $confirmedQuantity)
    {
        $reservedSupplies = [];
        foreach ($aSupplies as $aSupply) {
            $supplyAvailableQuantity = $aSupply['reserved_quantity'] - $aSupply['confirmed_quantity'];
            if ($supplyAvailableQuantity >= $confirmedQuantity) {
                // ok mamy wystarczająco dużo żeby sobie potwierdzić
                $aSupply['confirmed_quantity'] += $confirmedQuantity;
                $aSupply['_own_reserved'] = $confirmedQuantity;
                if ($aSupply['reserved_quantity'] <= $aSupply['confirmed_quantity']) {
                    $aSupply['status'] = '0';
                }

                $reservedSupplies[] = $aSupply;
                return $reservedSupplies;
            } else {
                // w tej dostawie jest za mała ilość
                $confirmedQuantity -= $supplyAvailableQuantity;
                $aSupply['confirmed_quantity'] = $aSupply['reserved_quantity'];
                $aSupply['_own_reserved'] = $supplyAvailableQuantity;
                $aSupply['status'] = '0';
                $reservedSupplies[] = $aSupply;
            }
        }
        throw new Exception('Wystąpił błąd podczas dopasowywania zbieranych list z dostawami do rozłożenia ! Ilość : '.$confirmedQuantity.' <br /> Tablica danych: <br />'.print_r($aSupplies, true));
    }

    private function saveToLog($productId, $productQuantity, $productLocation) {
        $aValues = [
            'product_id' => $productId,
            'quantity' => $productQuantity,
            'location' => $productLocation,
            'user_id' => $_SESSION['user']['id']
        ];

        $this->pDbMgr->Insert('profit24', 'magazine_distribution_log', $aValues);
    }

    /**
     * @return string
     */
    function showLog()
    {
        global $aConfig;
        $aIDs = array();
        $sDateSql = '';

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header'	=> _("Historia zeskanowanych produktów"),
            'refresh'	=> false,
            'search'	=> false,
            'checkboxes'=> false,
            'per_page'  => false,
        );
        $aAttribs = array(
            'width'			=> '100%',
            'border'		=> 0,
            'cellspacing'	=> 0,
            'cellpadding'	=> 0,
            'class'			=> 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'content'	=> '&nbsp;',
                'sortable'	=> false,
                'width'	    => '20'
            ),
            array(
                'db_field'	=> 'image',
                'content'	=> _("Okładka"),
                'sortable'	=> false
            ),
            array(
                'db_field'	=> 'ean_13',
                'content'	=> _("EAN13"),
                'sortable'	=> false,
            ),
            array(
                'db_field'	=> 'quantity',
                'content'	=> _("Ilość"),
                'sortable'	=> false,
            ),
            array(
                'db_field'	=> 'location',
                'content'	=> _("Lokalizacja"),
                'sortable'	=> false,
            ),
            array(
                'db_field'	=> 'name',
                'content'	=> _("Tytuł"),
                'sortable'	=> false,
            ),
        );


        // pobranie liczby wszystkich aktualnosci strony
        $sSql = "SELECT COUNT(*) 
						 FROM magazine_distribution_log
						 WHERE user_id = " . $_SESSION['user']['id'];

        $iRowCount = intval($this->pDbMgr->GetOne('profit24', $sSql));

        $pView = new \View('phrases', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);


        if ($iRowCount > 0) {
            // pobranie wszystkich wyszukiwanych fraz dla danego serwisu
            $sSql = "SELECT PI.directory, PI.photo, P.ean_13, MDL.quantity, MDL.location, P.name 
                    FROM magazine_distribution_log MDL 
                    LEFT JOIN products P 
                    ON MDL.product_id = P.id
                    LEFT JOIN products_images PI 
                    ON P.id = PI.product_id
                    WHERE user_id = " . $_SESSION['user']['id'] . " 
                    ORDER BY MDL.id DESC
                    LIMIT 20";
            $aRecords =& $this->pDbMgr->GetAll('profit24', $sSql);

            $iId = 0;
            foreach ($aRecords as $iKey => $aRecord) {
                $atmp = [
                    '&nbsp;' => ++$iId,
                    'image' => '<img src="/images/photos/' . $aRecord['directory'] . '/' . $aRecord['photo'] . '">'
                ];
                $aRecords[$iKey] = ($atmp + $aRecord);
            }

            // ustawienia dla poszczegolnych kolumn rekordu
            $aColSettings = array(
                'directory' => array(
                    'show' => false
                ),
                'photo' => array(
                    'show' => false
                )
            );

            // dodanie rekordow do widoku
            $pView->AddRecords($aRecords, $aColSettings);
        }

        $pView->AddRecordsHeader($aRecordsHeader);
        $aRecordsFooter = array(
        );
        $aRecordsFooterParams = array(
        );
        $pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

        return $pView->Show();
    }
}
