<?php
namespace omniaCMS\modules\m_multilocalization;

use Admin;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\SellPredict;
use LIB\EntityManager\Entites\StockLocation;
use LIB\FixAutomats\stockLocationFixer;
use magazine\Containers;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use Validator;
use View;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 19.01.18
 * Time: 08:52
 */
class Module__multilocalization__diff_stock_location implements ModuleEntity, SingleView
{

    /**
     *
     * @var string
     */
    private $sMsg;

    /**
     *
     * @var string
     */
    private $sModule;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;
    private $stockLocation;
    private $sellPredict;
    private $container;

    public function __construct(Admin $oClassRepository)
    {

        $this->oClassRepository = $oClassRepository;
        $this->em = $this->oClassRepository->entityManager;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
        $this->stockLocation = new StockLocation($this->pDbMgr);
        $this->sellPredict = new SellPredict($this->pDbMgr);
        $this->container = new Containers($this->pDbMgr);
    }

    /**
     * @return string
     */
    public function doDefault()
    {
        global $aConfig;
        $aData = array();

        if (!empty($_POST)) {
            $aData =& $_POST;
        }

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header' => _('Różnice w stanach magazynowych'),
            'refresh' => true,
            'search' => true,
            'items_per_page' => 20,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 2,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'id',
                'content' => _('#id'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'name',
                'content' => _('nazwa'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'ean_13',
                'content' => _('EAN_13'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'isbn_plain',
                'content' => _('ISBN'),
                'sortable' => true,
                'width' => '300'
            ),
            array(
                'db_field' => 'sl_locations',
                'content' => _('Dane Lokalizacji'),
                'sortable' => true,
                'width' => '180'
            ),
            array(
                'db_field' => 'profit_g_act_stock',
                'content' => _('Stan w ERP'),
                'sortable' => true,
            ),
            array(
                'db_field' => 'sl_quantity',
                'content' => _('Stan na lokalizacjach'),
                'sortable' => true,
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '40'
            )
        );

        $aColSettings = array(
            'number' => array(
                'link' => phpSelf(array('do' => 'edit', 'id' => '{id}'))
            ),
            'action' => array(
                'actions' => array('edit'),
                'params' => array(
                    'edit' => array('id' => '{id}'),
                ),
            )
        );


        include_once('View/View.class.php');
        $oView = new View(_('bank_accounts'), $aHeader, $aAttribs);

        if (isset($_POST['search']) && !empty($_POST['search'])) {
            $sSearch = ' AND ( 
                          P.id = ' . (int)$_POST['search'] . ' OR 
                          P.isbn_plain LIKE \'' . isbn2plain($_POST['search']) . '\' OR 
                          P.isbn_10 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                          P.isbn_13 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                          P.ean_13 LIKE \'' . isbn2plain($_POST['search']) . '\' OR
                          P.streamsoft_indeks LIKE \'' . isbn2plain($_POST['search']) . '\'
                       )';
        }

        //MHI.*, MHO.id as missing_high_oili_id
        $aCols = array('PS.id', 'P.name', 'P.ean_13', 'P.isbn_plain',
            'GROUP_CONCAT(SL.container_id," - ",SL.quantity SEPARATOR \'<br>\') AS sl_locations',
            'PS.profit_g_act_stock',
            '( SELECT SUM(quantity) 
               FROM stock_location AS SL
               WHERE SL.products_stock_id = PS.id
               ) AS sl_quantity');

        $sSql = '
              SELECT %cols
              FROM products_stock AS PS
              LEFT JOIN stock_location AS SL
              ON SL.products_stock_id = PS.id
              JOIN products AS P
                ON P.id = PS.id
              WHERE
              (
               SELECT IFNULL(SUM(quantity), 0) 
               FROM stock_location AS SL
               WHERE SL.products_stock_id = PS.id
               )
               <> PS.profit_g_act_stock
              
              %filters
              
              '.$sSearch .'
             ';
        $sGroupBySQL = 'GROUP BY PS.id';
        $aSearchCols = array();
        $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, '', $sGroupBySQL, ' PS.id DESC');
        $oView->AddRecordsHeader($aRecordsHeader);
        $oView->setActionColSettings($aColSettings);

        $aRecordsFooter = array(array(), array(''));
        $oView->AddRecordsFooter($aRecordsFooter);
        return ShowTable($oView->Show());
    }

    public function doEdit($iProductId = 0)
    {
        if (isset($_GET['id'])) {
            $iProductId = $_GET['id'];
        }

        include_once('Form/FormTable.class.php');
        $pForm = new \FormTable(_('Edycja produktu na lokalizacji'), _('Edycja lokalizacji'), [], [], true);
        $highLocations = $this->stockLocation->getFindLocations($iProductId);
        foreach ($highLocations as $location) {
            $pForm->AddText('location['.$location['id'].']', _($location['container_id']), $location['quantity'], [], '', 'uinteger');
        }


        $sFields = ' lokalizacja: ' . $pForm->GetTextHTML('location_new', _('lokalizacja / НОМЕР ЛОКАЛИЗАЦИИ'), '', [], '', 'uinteger', false);
        $sFields .= ' ilość ' . $pForm->GetTextHTML('quantity_new', _('Ilość'), '', [], '', 'uinteger', false);

        $pForm->AddMergedRow('Nowa '.$sFields, ['class'=>'merged']);

        $pForm->AddHidden('productId', $iProductId);
        $pForm->AddHidden('do', 'save');
        $pForm->AddInputButton('save', _('Zapisz'), array('style' => 'font-size: 25'));

        return $pForm->ShowForm();
    }


    /**
     * @return string
     */
    public function doSave() {

        $data = $_POST;
        $productId = $data['productId'];

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        $result = $this->checkQuantity($data);
        if ($result !== true) {
            $oValidator->sError .= $result;
        }
        if (!$oValidator->Validate($data)) {
            $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            return $this->doEdit($productId);
        }

        $this->pDbMgr->BeginTransaction('profit24');
        $bIsFalse = false;
        foreach ($data['location'] as $stockLocation => $quantity) {
            if(false === $this->stockLocation->changeQuantity($stockLocation, $quantity)) {
                $bIsFalse = true;
                break;
            }
        }
        if (true === $bIsFalse) {
            $sMsg = _('Wystąpił błąd podczas ustawiania ilości na lokalizacjach');
            $this->sMsg .= GetMessage($sMsg);
            $this->pDbMgr->RollbackTransaction('profit24');
            AddLog($sMsg);
            return $this->doDefault();
        }

        if ($data['location_new'] != '' && intval($data['quantity_new']) > 0) {
            //$productId, $location, $magazineType, $quantity = 0, $reservation = 0, $available = 0
            $container = $this->pDbMgr->getTableRow('containers', $data['location_new'], ['magazine_type']);
            if (!empty($container)) {
                if( false === $this->stockLocation->findAddSumLocation($productId, $data['location_new'], $container['magazine_type'], $data['quantity_new'], 0, $data['quantity_new'])) {
                    $bIsFalse = true;
                }
            } else {
                $sMsg = _('Wprowadzony numer kontenera nie znajduje się w bazie');
                $this->sMsg .= GetMessage($sMsg);
                AddLog($sMsg);
                $this->pDbMgr->RollbackTransaction('profit24');
                return $this->doDefault();
            }

        }

        if (true === $bIsFalse) {
            $sMsg = _('Wystąpił błąd podczas ustawiania ilości na lokalizacjach');
            AddLog($sMsg);
            $this->sMsg .= GetMessage($sMsg);
            $this->pDbMgr->RollbackTransaction('profit24');
            return $this->doDefault();
        }


        $sMsg = _('Zapisano zmiany w dostawach produktu o id "'.$productId.'" <br /> DANE DEBUG : <pre>'.print_r($data['location'], true).'</pre>');
        $this->sMsg .= GetMessage($sMsg, false);
        AddLog($sMsg, false);
        $this->pDbMgr->CommitTransaction('profit24');
        return $this->doDefault();

    }

    /**
     * @param $data
     * @return bool|string
     */
    private function checkQuantity($data) {

        $result = true;
        foreach ($data['location'] as $stockLocation => $quantity) {
            $stockLocation = $this->pDbMgr->getTableRow('stock_location', $stockLocation, [' * ']);
            if ($quantity > 0 && $stockLocation['reservation'] > $quantity) {
                $result .= '<li>Ilość ('.$quantity.') musi być większa lub równa ilości na rezerwacji '.$stockLocation['reservation'].'</li>';
            }
        }
        return $result;
    }

    public function parseViewItem(array $aItem)
    {
        return $aItem;
    }

    public function resetViewState()
    {
        resetViewState();
    }

    public function getMsg()
    {
        return $this->sMsg;
    }
}