<?php
namespace omniaCMS\modules\m_multilocalization;

use Admin;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\StockLocation;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use View;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 19.01.18
 * Time: 08:52
 */
class Module__multilocalization__deficiency_details implements ModuleEntity, SingleView
{

    /**
     *
     * @var string
     */
    private $sMsg;

    /**
     *
     * @var string
     */
    private $sModule;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    public function __construct(Admin $oClassRepository) {

        $this->oClassRepository = $oClassRepository;
        $this->em = $this->oClassRepository->entityManager;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function doDefault()
    {
        global $aConfig;
        $aData = array();

        if (!empty($_POST)) {
            $aData =& $_POST;
        }

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header'	=> _('Braki na rozkładaniu z wysokiego składowania'),
            'refresh'	=> true,
            'search'	=> true,
            'items_per_page' => 20,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 2,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'db_field'	=> 'name',
                'content'	=> _('Tytuł'),
                'sortable'	=> true,
                'width' => '300'
            ),
            array(
                'db_field'	=> 'quantity',
                'content'	=> _('Ilość'),
                'sortable'	=> true,
                'width' => '150'
            ),
            array(
                'content'	=> $aConfig['lang']['common']['action'],
                'sortable'	=> false,
                'width'	=> '40'
            )
        );

        $aColSettings = array(
            'id'	=> array(
                'show'	=> false
            ),
//            'number' => array (
//                'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
//            ),
            'action' => array (
                'actions'	=> array ('publish'),
                'params' => array (
                    'publish'	=> array('id' => '{id}'),
                ),
            )
        );


        include_once('View/View.class.php');
        $oView = new View(_('deficiency_details'), $aHeader, $aAttribs);

        //MHI.*, MHO.id as missing_high_oili_id
        $aCols = array('MHI.id', 'P.name', 'MHI.quantity');

        $sSql = '
              SELECT %cols
              FROM orders_items_lists AS OIL 
              JOIN orders_items_lists_items AS OILI
                ON OIL.id = OILI.orders_items_lists_id
              JOIN missing_high_oili AS MHO
                ON MHO.oili_id = OILI.id
              JOIN missing_high_items AS MHI
                ON MHO.id = MHI.missing_high_oili_id
              JOIN products AS P
                ON P.id = MHI.product_id
             WHERE 
              1=1
             AND 
              OIL.closed = "0"
             AND 
              OIL.orders_send_history_id = "'.$_GET['pid'].'"
             ';
        $sGroupBySQL = 'GROUP BY MHI.id';
        $aSearchCols = array('MHI.id');
        $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, '', $sGroupBySQL, ' MHI.id ASC ');
        $oView->AddRecordsHeader($aRecordsHeader);
        $oView->setActionColSettings($aColSettings);

        $aRecordsFooter = array(array(),array());
        $oView->AddRecordsFooter($aRecordsFooter);
        return ShowTable($oView->Show());
    }

    public function doPublish() {
        $iMHIId = $_GET['id'];


        try {
            $this->pDbMgr->BeginTransaction('profit24');
            $this->deleteMissing($iMHIId);

            $this->pDbMgr->CommitTransaction('profit24');
            $sMsg = _('Usunięto brak produktu na rozkładaniu o id = '.$iMHIId);
            AddLog($sMsg, false);
            $this->sMsg .= GetMessage($sMsg, false);
        } catch (\Exception $ex) {
            $this->pDbMgr->RollbackTransaction('profit24');
            $sMsg = _('Wystąpił błąd - '.$ex->getMessage(). ' id = '.$iMHIId);
            AddLog($sMsg, false);
            $this->sMsg .= GetMessage($sMsg, false);
        }



        return $this->doDefault();
    }

    /**
     * @param $iMHIId
     */
    private function deleteMissing($iMHIId) {

        $sSql = 'SELECT OILI.id, OILI.orders_items_lists_id 
                 FROM orders_items_lists_items as OILI
                 JOIN missing_high_oili AS MHO
                  ON OILI.id = MHO.oili_id
                 JOIN missing_high_items AS MHI
                  ON MHO.id = MHI.missing_high_oili_id
                 WHERE MHI.id = '.$iMHIId.'
                 ';
        $ordersItemsListsData = $this->pDbMgr->GetRow('profit24', $sSql);

        $updateSQL = '
        UPDATE orders_items_lists_items
        SET 
          confirmed = "1"
        WHERE id = '.$ordersItemsListsData['id'].'
        LIMIT 1
        ';
        if (false === $this->pDbMgr->Query('profit24', $updateSQL)) {
            throw new Exception('Wystapil blad zmiany orders_items_lists_items o id = '.$ordersItemsListsData['id']);
        }

        $this->tryCloseAllList($ordersItemsListsData['orders_items_lists_id']);


        $sSql = 'SELECT missing_high_oili_id FROM missing_high_items WHERE id = '.$iMHIId;
        $missingOILIId = $this->pDbMgr->GetOne('profit24', $sSql);

        // Delete
        if (false === $this->pDbMgr->Query('profit24', "DELETE FROM missing_high_items WHERE id = ".$iMHIId)) {
            throw new Exception('Wystapil blad podczas usuwania');
        }

        if (false === $this->pDbMgr->Query('profit24', "DELETE FROM missing_high_oili WHERE id = ".$missingOILIId)) {
            throw new Exception('Wystapil blad podczas usuwania oili item');
        }
    }

    /**
     * @param $iOILId
     * @return bool
     */
    public function tryCloseAllList($iOILId) {

        // zamknąliśmy tytuł spróbujmy zamknąć całą listę
        $sSql = 'SELECT id
                     FROM orders_items_lists_items
                     WHERE confirmed = "0" 
                     AND orders_items_lists_id = '.$iOILId;
        $existsOpen = $this->pDbMgr->GetOne('profit24', $sSql);

        // jeśli nie ma żadnego
        if (intval($existsOpen) <= 0) {
            $valueUp = [
                'closed' => '1'
            ] ;
            if( false === $this->pDbMgr->Update('profit24', 'orders_items_lists', $valueUp, ' id='.$iOILId)) {
                throw new Exception('Wystapil blad podczas zamykania orders_items_lists id = '.$iOILId);
            }
        }
        return true;
    }

    public function parseViewItem(array $aItem)
    {
        return $aItem;
    }

    public function resetViewState()
    {
        resetViewState();
    }

    public function getMsg()
    {
        return $this->sMsg;
    }
}