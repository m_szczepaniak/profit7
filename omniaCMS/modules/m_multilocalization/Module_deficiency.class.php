<?php
namespace omniaCMS\modules\m_multilocalization;

use Admin;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use View;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 19.01.18
 * Time: 08:52
 */
class Module__multilocalization__deficiency implements ModuleEntity, SingleView
{

    /**
     *
     * @var string
     */
    private $sMsg;

    /**
     *
     * @var string
     */
    private $sModule;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    public function __construct(Admin $oClassRepository) {

        $this->oClassRepository = $oClassRepository;
        $this->em = $this->oClassRepository->entityManager;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function doDefault()
    {
        global $aConfig;
        $aData = array();

        if (!empty($_POST)) {
            $aData =& $_POST;
        }

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header'	=> _('Braki na rozkładaniu z wysokiego składowania'),
            'refresh'	=> true,
            'search'	=> true,
            'items_per_page' => 20,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 2,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'db_field'	=> 'name',
                'content'	=> _('Tytuł'),
                'sortable'	=> true,
                'width' => '300'
            ),
            array(
                'db_field'	=> 'quantity',
                'content'	=> _('Ilość'),
                'sortable'	=> true,
                'width' => '150'
            ),
            array(
                'content'	=> $aConfig['lang']['common']['action'],
                'sortable'	=> false,
                'width'	=> '40'
            )
        );

        $aColSettings = array(
            'id'	=> array(
                'show'	=> false
            ),
//            'number' => array (
//                'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
//            ),
//            'action' => array (
//                'actions'	=> array ('edit', 'delete'),
//                'params' => array (
//                    'edit'	=> array('id' => '{id}'),
//                    'delete' => array('id' => '{id}'),
//                ),
//            )
        );


        include_once('View/View.class.php');
        $oView = new View(_('bank_accounts'), $aHeader, $aAttribs);

        //MHI.*, MHO.id as missing_high_oili_id
        $aCols = array('P.name', 'MHI.quantity'
        );

        $sSql = '
              SELECT %cols
              FROM missing_high_items AS MHI
              JOIN products AS P
                ON P.id = MHI.product_id
              JOIN missing_high_oili as MHO 
                ON MHO.id = MHI.missing_high_oili_id
             WHERE 
             1=1
              %filters
             ';
        $sGroupBySQL = 'GROUP BY MHI.id';
        $aSearchCols = array('MHI.id');
        $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, '', $sGroupBySQL, ' MHI.id ASC ');
        $oView->AddRecordsHeader($aRecordsHeader);
        $oView->setActionColSettings($aColSettings);

        $aRecordsFooter = array(array(),array('add'));
        $oView->AddRecordsFooter($aRecordsFooter);
        return ShowTable($oView->Show());
    }

    public function parseViewItem(array $aItem)
    {
        return $aItem;
    }

    public function resetViewState()
    {
        resetViewState();
    }

    public function getMsg()
    {
        return $this->sMsg;
    }
}