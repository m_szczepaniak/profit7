<?php

namespace omniaCMS\modules\m_finanse;

use Admin;
use DatabaseManager;
use FormTable;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use Smarty;
use Validator;
use View;
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */

/**
 * Description of Module_bank_accounts
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__finanse__bank_accounts implements ModuleEntity, SingleView {

  /**
   *
   * @var string
   */
	private $sMsg;

  /**
   *
   * @var string
   */
	private $sModule;

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;
	
  public function __construct(Admin $oClassRepository) {
    
    $this->oClassRepository = $oClassRepository;
    $this->em = $this->oClassRepository->entityManager;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }
  
  /**
   * 
   * @return string
   */
  public function doDelete() {
    
    $iId = $_GET['id'];
    if ($iId > 0) {
      
      $sSql = 'DELETE FROM bank_accounts WHERE id = '.$iId.' LIMIT 1';
      if ($this->pDbMgr->Query('profit24', $sSql) === false) {
        $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania rachunku bankowego'));
        return $this->doDefault();
      } else {
        $this->sMsg .= GetMessage(_('Rachunek bankowy został usunięty'), false);
        return $this->doDefault();
      }
    } else {
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania rachunku bankowego'));
      return $this->doDefault();
    }
  }
  
  /**
   * 
   * @return string html
   */
  public function doAddBankAccount() {
    
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			return $this->doDefault();
		}
    
    $aData = $_POST;
    $aValues = array(
        'orders_payment_types_id' => $aData['orders_payment_types_id'],
        'websites_id' => $aData['websites_id'],
        'streamsoft_id' => $aData['streamsoft_id'],
        'number' => $aData['number'],
        'ident_statments_file' => $aData['ident_statments_file'],
        'postal_fee' => isset($aData['postal_fee']) ? '1' : '0',
        'created' => 'NOW()',
        'created_by' => $_SESSION['user']['name']
        
    );
    if ($this->pDbMgr->Insert('profit24', 'bank_accounts', $aValues) > 0) {
      $this->sMsg .= GetMessage(sprintf(_('Dodano rachunek bankowe nr "%s"'), $aData['number']), false);
    } else {
      $this->sMsg .= GetMessage(sprintf(_('Wystąpił błąd podczas dodawnia konta nr "%s"'), $aData['number']));
    }
    return $this->doDefault();
  }
  
  /**
   * 
   * @return string
   */
  public function doEditBankAccount() {
    
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
    if ($this->SprawdzNumerNRB($_POST['number']) === false) {
      $oValidator->sError .= '<li>' . _('Błędny numer konta bankowego') . '</li>';
    }
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			return $this->doDefault();
		}
    $iBankAccountId = $_POST['id'];
    if ($iBankAccountId > 0) {
      $aData = $_POST;
      $aValues = array(
          'orders_payment_types_id' => $aData['orders_payment_types_id'],
          'websites_id' => $aData['websites_id'],
          'streamsoft_id' => $aData['streamsoft_id'],
          'number' => $aData['number'],
          'ident_statments_file' => $aData['ident_statments_file'],
          'postal_fee' => isset($aData['postal_fee']) ? '1' : '0',
          'modified' => 'NOW()',
          'modified_by' => $_SESSION['user']['name']

      );
      if ($this->pDbMgr->Update('profit24', 'bank_accounts', $aValues, ' id='.$iBankAccountId) > 0) {
        $this->sMsg .= GetMessage(sprintf(_('Dodano rachunek bankowe nr "%s"'), $aData['number']), false);
      } else {
        $this->sMsg .= GetMessage(sprintf(_('Wystąpił błąd podczas dodawnia konta nr "%s"'), $aData['number']));
      }
    } else {
      $this->sMsg .= GetMessage(sprintf(_('Wystąpił błąd podczas dodawnia konta nr "%s"'), $aData['number']));
    }
    return $this->doDefault();
  }
  
  /**
   * 
   * @global array $aConfig
   * @return array
   */
  public function doAdd() {
    return $this->addEditForm();
  }
  
  /**
   * 
   * @global array $aConfig
   * @return array
   */
  public function doEdit() {
    $iBankAccountId = $_GET['id'];
    return $this->addEditForm($iBankAccountId);
  }
  
  /**
   * 
   * @global array $aConfig
   * @param int $iBankAccountId
   * @return string
   */
  private function addEditForm($iBankAccountId = 0) {
    global $aConfig;
    
    $aData = array();
    if (!empty($_POST)) {
      $aData = $_POST;
    } 
    elseif ($iBankAccountId > 0) {
      $aData = $this->pDbMgr->getTableRow('bank_accounts', $iBankAccountId, array('*'));
    }
    
    include_once('lib/Form/FormTable.class.php');
    $pForm = new FormTable('bank_account_addedit', _('Dodaj rachunek bankowe'));
    $aAttr = array('style' => 'font-size: 16px;');
    if ($iBankAccountId > 0) {
      $pForm->AddHidden('do', 'editBankAccount');
      $pForm->AddHidden('id', $iBankAccountId);
    } else {
      $pForm->AddHidden('do', 'addBankAccount');
    }
    $aWebsites = $this->getWebsites();
    $pForm->AddSelect('websites_id', _('Serwis'), $aAttr, addDefaultValue($aWebsites), $aData['websites_id'], '', true);
    $aPaymentTypesList = $this->getPaymentTypesList();
    $pForm->AddSelect('orders_payment_types_id', _('Płatność'), $aAttr, addDefaultValue($aPaymentTypesList), $aData['orders_payment_types_id'], '');
    $pForm->AddText('number', _('Numer konta'), $aData['number'], array('style' => 'font-size: 16px; width: 380px;'), '', 'text', true, '/^\d{26}$/');
    $pForm->AddText('ident_statments_file', _('Identyfikator rachunku w pliku XML'), $aData['ident_statments_file'], $aAttr, '', 'text');
    $pForm->AddText('streamsoft_id', _('Identyfikator rachunku w StreamSoft'), $aData['streamsoft_id'], $aAttr, '', 'text', false);
    $pForm->AddCheckBox('postal_fee', _('Rachunek płatności przy odbiorze'), $aAttr, '', isset($aData['postal_fee']) && $aData['postal_fee'] == '1', false);
    
    $pForm->AddRow('&nbsp;', 
            $pForm->GetInputButtonHTML('send', _('Zapisz')).'&nbsp;&nbsp;'.
            $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], 
                    array('onclick'=>'MenuNavigate(\''.phpSelf().'\');'), 'button'));
    
    return $pForm->ShowForm();
  }
  
  /**
   * 
   * @return array
   */
  private function getWebsites() {
    
    $sSql = 'SELECT id AS value, name AS label
             FROM websites
             ORDER BY id ASC';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @global array $aConfig
   * @return array
   */
  private function getPaymentTypesList() {
    global $aConfig;
    
    // fuck langi zamowien, do nazw metod płatności
    include_once('modules/m_zamowienia/lang/admin_pl.php');
    $aOrdersLang = $aConfig['lang']['m_zamowienia'];
    
    $sSql = 'SELECT OPT.ptype, OPT.id AS value, TM.name AS transport
             FROM orders_payment_types AS OPT
             JOIN orders_transport_means AS TM
              ON OPT.transport_mean = TM.id
             ORDER BY OPT.ptype DESC, TM.name';
    $aPaymentTypesList = $this->pDbMgr->GetAll('profit24', $sSql);
    foreach ($aPaymentTypesList as $iId => $aPaymentType) {
      $aPaymentTypesList[$iId]['label'] = $aOrdersLang['ptype_'.$aPaymentType['ptype']] .' // '. $aPaymentType['transport'];
      unset($aPaymentTypesList[$iId]['ptype']);
      unset($aPaymentTypesList[$iId]['transport']);
    }
    return $aPaymentTypesList;
  }

  /**
   * 
   * @global array $aConfig
   * @return string
   */
  public function doDefault() {
    global $aConfig;
    $aData = array();
    
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
    
		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> _('Rachunki bankowe'),
			'refresh'	=> true,
			'search'	=> true,
      'items_per_page' => 20,
      'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 2,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
    $aRecordsHeader = array(
      array(
				'db_field'	=> 'number',
				'content'	=> _('Numer'),
				'sortable'	=> true,
				'width' => '300'
			),
      array(
				'db_field'	=> 'website',
				'content'	=> _('Serwis'),
				'sortable'	=> true,
        'width' => '150'
			),
      array(
				'db_field'	=> 'transport',
				'content'	=> _('Transport'),
				'sortable'	=> true,
        'width' => '250'
			),
      array(
				'db_field'	=> 'payment',
				'content'	=> _('Płatność'),
				'sortable'	=> true,
        'width' => '350'
			),
      array(
				'db_field'	=> 'streamsoft_id',
				'content'	=> _('StreamSoft Id'),
				'sortable'	=> true,
				'width' => '30'
			),
			array(
				'db_field'	=> 'postal_fee',
				'content'	=> _('Rachunek płatności przy odbiorze'),
				'sortable'	=> true,
				'width' => '150'
			),
			array(
				'db_field'	=> 'created',
				'content'	=> _('Utworzono'),
				'sortable'	=> true,
        'width' => '150'
			),
      array(
				'db_field'	=> 'created_by',
				'content'	=> _('Utworzył'),
				'sortable'	=> true,
        'width' => '100'
			),
			array(
				'db_field'	=> 'modified',
				'content'	=> _('Zmodyfikowano'),
				'sortable'	=> true,
        'width' => '150'
			),
      array(
				'db_field'	=> 'modified_by',
				'content'	=> _('Zmodyfikował'),
				'sortable'	=> true,
        'width' => '100'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
    
    $aColSettings = array(
      'id'	=> array(
        'show'	=> false
      ),
      'number' => array (
        'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
      ),
      'action' => array (
        'actions'	=> array ('edit', 'delete'),
        'params' => array (
          'edit'	=> array('id' => '{id}'),
          'delete' => array('id' => '{id}'),
        ),
      ) 
    );
    
    
    include_once('View/View.class.php');
    $oView = new View(_('bank_accounts'), $aHeader, $aAttribs);
    
    $aCols = array('BA.id', 'BA.number', 'W.name as website', 'TM.name as transport', 'OPT.ptype as payment', 
        'BA.streamsoft_id', 'BA.postal_fee', 'BA.created', 'BA.created_by', 'BA.modified', 'BA.modified_by'
        );
    
    $sSql = 'SELECT %cols
             FROM bank_accounts AS BA
             JOIN websites AS W
              ON BA.websites_id = W.id
             JOIN orders_payment_types AS OPT
              ON OPT.id = BA.orders_payment_types_id
             JOIN orders_transport_means AS TM
              ON OPT.transport_mean = TM.id
             WHERE 
             1=1
              %filters
             ';
    $sGroupBySQL = 'GROUP BY BA.id';
    $aSearchCols = array('BA.number');
    $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, '', $sGroupBySQL, ' BA.id ASC ');
    $oView->AddRecordsHeader($aRecordsHeader);
    $oView->setActionColSettings($aColSettings);
    
    $aRecordsFooter = array(array(),array('add'));
		$oView->AddRecordsFooter($aRecordsFooter);
    return ShowTable($oView->Show());
  }

  /**
   * 
   * @return string
   */
  public function getMsg() {
    return $this->sMsg;
  }
  
  /**
   * 
   * @param string $p_iNRB
   * @return boolean
   */
  function SprawdzNumerNRB($p_iNRB)
  {
    // Usuniecie spacji
    $iNRB = str_replace(' ', '', $p_iNRB);
    // Sprawdzenie czy przekazany numer zawiera 26 znaków
    if(strlen($iNRB) != 26)
      return false;

    // Zdefiniowanie tablicy z wagami poszczególnych cyfr				
    $aWagiCyfr = array(1, 10, 3, 30, 9, 90, 27, 76, 81, 34, 49, 5, 50, 15, 53, 45, 62, 38, 89, 17, 73, 51, 25, 56, 75, 71, 31, 19, 93, 57);

    // Dodanie kodu kraju (w tym przypadku dodajemy kod PL)		
    $iNRB = $iNRB.'2521';
    $iNRB = substr($iNRB, 2).substr($iNRB, 0, 2); 

    // Wyzerowanie zmiennej
    $iSumaCyfr = 0;

    // Pćtla obliczająca sumć cyfr w numerze konta
    for($i = 0; $i < 30; $i++) 
      $iSumaCyfr += $iNRB[29-$i] * $aWagiCyfr[$i];

    // Sprawdzenie czy modulo z sumy wag poszczegolnych cyfr jest rowne 1
    return ($iSumaCyfr % 97 == 1);
  }

  /**
   * 
   * @global array $aConfig
   * @param array $aItem
   * @return array
   */
  public function parseViewItem(array $aItem) {
    global $aConfig;
    
    // fuck langi zamowien, do nazw metod płatności
    include_once('modules/m_zamowienia/lang/admin_pl.php');
    $aOrdersLang = $aConfig['lang']['m_zamowienia'];
    
    $aItem['payment'] = $aOrdersLang['ptype_'.$aItem['payment']];
    $aItem['postal_fee'] = $aConfig['lang']['common'][$aItem['postal_fee']];
    return $aItem;
  }

  public function resetViewState() {
    resetViewState($this->sModule);
  }
}