<?php
/**
 * Klasa do obsługi przelewów OPEK
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
ini_set('memory_limit','1G');
ini_set('post_max_size', '64M');
//ini_set('display_errors', 'On');
//error_reporting(E_ALL ^ E_NOTICE);
class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
  
  // tablica zamówień które można wybrać
  private $aOrdersToCompare;
  
  // tablica zamówień które zostały wybrane
  private $aOrdersUsed = array(-1);
  
  /**
   *
   * @var array
   */
  private $file_types;
  
  /**
   *
   * @var int
   */
  private $iTransportId;


  private static $aTransportMethodsMethodsIdsEqual = [8,4];
  
  public function __construct(&$pSmarty, $bInit = true) {
    global $aConfig;
//    ini_set('memory_limit','1G');
//    set_time_limit(440000);
//    ini_set('error_reporting', 'E_ALL');
//    ini_set('display_errors', '1');
//    var_dump(ini_get('display_errors'));
//    var_dump(ini_get('error_reporting'));
//    var_dump(ini_get('memory_limit'));
    
		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
    $this->setFileTypes();
    $this->setTransportIdByFileType();
      $this->iTransportId = $this->mergeTransportIfEqual($this->iTransportId);

		$sDo = '';
		$iId = 0;
		$iPId = 0;
		$iCId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
		if (isset($_GET['cid'])) {
			$iCId = $_GET['cid'];
		}
    
    // wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
    
    // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    if ($bInit === TRUE) {
      return true;
    }
		switch ($sDo) {
			case 'save': $this->saveOpek($pSmarty); break;
      case 'import': $this->Import($pSmarty); break;
      case 'details': $this->Details($pSmarty, $iId); break;
			case 'paczkomat_pdf_download': $this->importTransfersOrders($pSmarty); break;// nazwa niestety taka bo omnia View ma takie możliwosci jakie ma
			default: $this->Show($pSmarty); break;
		}
  }// end of __construct() method

    /**
     * Metoda łączy metody transportu jeśli jest to metoda równoważna
     *
     * @param $iTransportId
     * @return array
     */
    private function mergeTransportIfEqual($iTransportId) {

        if (in_array($iTransportId,  self::$aTransportMethodsMethodsIdsEqual)) {
            $iTransportId = implode(', ', self::$aTransportMethodsMethodsIdsEqual);
        }
        return $iTransportId;
    }
  
  /**
   * 
   */
  private function setTransportIdByFileType() {
    
    if (isset($_POST['file_type'])) {
      $this->iTransportId = $this->getTransportIdBySymbol($_POST['file_type']);
    } elseif (isset($_POST['transport_id'])) {
      $this->iTransportId = $_POST['transport_id'];
    } else {
      $this->iTransportId = 0;
    }
  }
  
  
  /**
   * 
   */
  private function setFileTypes() {
    
    $this->file_types = array(
          array(
          'label' => _('FedEx'),
          'value' => 'opek-przesylka-kurierska'
          ),
      array(
          'label' => _('TBA'),
          'value' => 'tba'
          ),
      array(
          'label' => _('Poczta Polska'),
          'value' => 'poczta_polska'
          ),
      array(
          'label' => _('Ruch - Format TBA'),
          'value' => 'ruch'
          ),
    );
  }
  
  
  /**
   * Metoda wyświeta szczegóły przelewów i zamówień
   * 
   * @global array $aConfig
   * @param type $pSmarty
   * @param type $iId
   */
  public function Details(&$pSmarty, $iId) {
    global $aConfig;
    
    $sSql = "SELECT A.*, O.order_number
             FROM orders_linking_transport_numbers AS A
             JOIN orders AS O
              ON A.order_id = O.id
             WHERE A.orders_bank_statement_id=".$iId." AND A.order_id > 0
             GROUP BY A.order_id
             ORDER BY A.transport_number";
    $aData = Common::GetAll($sSql);
    $aOrdersBankStatments = unserialize($aData[0]['orders_bank_statements']);
    if (empty($aOrdersBankStatments)) {
     $aOrdersBankStatments[] = $aData[0]['orders_bank_statement_id'];
    }
    $sSql = "SELECT GROUP_CONCAT(title SEPARATOR '<br /> ') as titles, SUM(paid_amount) as paid_amount FROM orders_bank_statements WHERE id IN(".implode(', ', $aOrdersBankStatments).")";
    $aBankTransfers = Common::GetRow($sSql);
    
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('transfers', _('Przelew OPEK'), array(), array('col_width'=>150));
    
    $pForm->AddRow(_('Przelewy'), $aBankTransfers['titles']);
    $pForm->AddRow(_('Wartość przelewów'), str_replace('.', ',', $aBankTransfers['paid_amount']));
    
    
    foreach ($aData as $aTransfer) {
      
      $pForm->AddMergedRow(_(sprintf('Zamówienie "%s" zostało opłacone na kwotę :', $aTransfer['order_number'])). ' '.
              '<b>'.str_replace('.', ',', $aTransfer['payed']).'</b>'.
              ' przelew za list "<b>'.$aTransfer['transport_number'].'</b>"'.
              ' wykonano na kwotę: <b>'.str_replace('.', ',', $aTransfer['topay']).'</b>', 
              array('style'=>'text-align: left; font-size: 12px; font-weight: normal;'));
    }
    
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
    
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
  }// end of Details() method
  
  
  /**
   * 
   */
  private function getFileTypes() {
    return $this->file_types;
  }
  
  
  private function _getLastestPaymentDate($aTransfers) {
    
    $sSql = "SELECT created 
             FROM orders_bank_statements
             WHERE id IN ('".implode("', '", $aTransfers)."')
             ORDER BY created DESC";
    return Common::GetOne($sSql);
  }
  
  /**
   * Metoda zapisuje przelewy do zamowień
   * 
   * @global array $aConfig
   * @param type $pSmarty
   * @return boolean
   */
  private function saveOpek(&$pSmarty) {
    global $aConfig;
    $bIsErr = false;
    
    $sLastestPaymentDate = '';
    $iFirstStatement = $_POST['bank_transfers'][0];
    
    $aTransferFile = unserialize(stripslashes($_POST['sTransferSerialized']));
    if ($this->_arrayOrdersHaveDuplicates($_POST['transfers_orders'])) {
      $this->sMsg = GetMessage(_("Wystąpił błąd podczas importowania przelewów. <br />Zamówienia powtarzają się !"));
      $this->importTransfersOrders($pSmarty);
      return false;
    } else {

      Common::BeginTransaction();
      // zatwierdzamy przelewy
      $aValues = array(
          'matched_order' => 1,
          'finished' => '1'
      );
      $sLastestPaymentDate = $this->_getLastestPaymentDate($_POST['bank_transfers']);
      
      $aBankTMP = array();
      foreach ($_POST['bank_transfers'] as $iBankTransferId) {
        if ($iBankTransferId > 0) {
          $aBankTMP[] = $iBankTransferId;
          if(Common::Update($aConfig['tabls']['prefix']."orders_bank_statements", $aValues, "id = ".$iBankTransferId) === false) {
            Common::RollbackTransaction();
            $this->sMsg = GetMessage(_(sprintf('Matchowania przelewu "%s" ', $iBankTransferId)));
            AddLog($this->sMsg);
            $this->Show($pSmarty);
            return false;
          }
        }
      }
      
      // sprawdzmy czy jakies przelewy bankowe zostały wybrane
      if (empty($aBankTMP)) {
        Common::RollbackTransaction();
        $this->sMsg = GetMessage(_(sprintf('Brak wybranych przelewów ')));
        AddLog($this->sMsg);
        $this->Show($pSmarty);
        return false;
      }
      
      
      
      // lecimy po zamówieniach
      if (!empty($_POST['transfers_orders']) && is_array($_POST['transfers_orders'])) {
        foreach ($_POST['transfers_orders'] as $iTKey => $aTransport) {
          // jest jedno zamowienie
          $aTransferInfo = $aTransferFile[$iTKey];
          foreach ($aTransport as $iOKey => $iOrderId) {
            if ($iOrderId > 0) {
              $sOrderNumber = $this->_getOrderNumber($iOrderId);
              
              // dodajemy powiązania
              foreach ($aBankTMP as $iStatment) {
                if ($this->_addOrdersToTransportToStatments($iStatment, $aBankTMP, $_POST['transport_number'][$iTKey], $iOrderId, $_POST['order_payed'][$iTKey][$iOKey], $aTransferInfo['5']) === false) {
                  Common::RollbackTransaction();
                  $this->sMsg = GetMessage(_(sprintf('Wystąpił błąd podczas dodawania powiazań do zamowienia "%s"', $sOrderNumber)));
                  AddLog($this->sMsg);
                  $this->Show($pSmarty);
                  return false;
                }
              }
              
              // opłacamy zamówienie
              $mRet = $this->payOrder($iOrderId, $_POST['order_payed'][$iTKey][$iOKey], $_POST['transport_number'][$iTKey], $sLastestPaymentDate);
              if ($mRet === -1) {
                Common::RollbackTransaction();
                $this->sMsg = GetMessage(_(sprintf('W zamówieniu nr "%s" znajduje się opłacona 2 metoda płatności', $sOrderNumber)));
                AddLog($this->sMsg);
                $this->Show($pSmarty);
                return false;
              } elseif ($mRet === -2) {
                Common::RollbackTransaction();
                $this->sMsg = GetMessage(_(sprintf('Wystąpił błąd podczas opłacania zamówienia "%s" ', $sOrderNumber)));
                AddLog($this->sMsg);
                $this->Show($pSmarty);
                return false;
              }

              // dodajemy płatności do zamłówienia
              if ($this->addPayment($iOrderId, 
                                     $sLastestPaymentDate, 
                                     $_POST['order_payed'][$iTKey][$iOKey], 
                                     $aTransferInfo[3].' '.$aTransferInfo[4], 
                                     $_POST['transport_number'][$iTKey],
                                     $this->iTransportId,
                                     $iFirstStatement ) === false) {
                Common::RollbackTransaction();
                $this->sMsg = GetMessage(_(sprintf('Wystąpił błąd podczas dodawania płatności zamówienia "%s" ', $sOrderNumber)));
                AddLog($this->sMsg);
                $this->Show($pSmarty);
                return false;
              }
            }
          }
        }
      } else {
        Common::RollbackTransaction();
        $this->sMsg = GetMessage(_(sprintf('Pusta tablica danych ')));
        AddLog($this->sMsg);
        $this->Show($pSmarty);
        return false;
      }
      
      Common::CommitTransaction();
      $this->sMsg = GetMessage(_(sprintf('Zamówienia zostały opłacone, przelewy zostały zatwierdzone')), false);
      AddLog($this->sMsg, false);
      $this->Show($pSmarty);
      return false;
    }
  }// end of saveOpek() method
  
  /**
   * 
   * @param string $sTransportSymbol
   * @return int
   */
  private function getTransportIdBySymbol($sTransportSymbol) {
    
    $sSql = 'SELECT id 
            FROM orders_transport_means 
            WHERE symbol = "'.$sTransportSymbol.'" 
            LIMIT 1';
    return Common::GetOne($sSql);
  }
  
  
  /**
   * Metoda powiązania zamówienia do listu przewozowego do przelewu
   * 
   * @global array $aConfig
   * @param int $iStatment
   * @param array $aStatments
   * @param string $sTransportNumber
   * @param int $iOrder
   * @param string $sPayed
   * @param string $sToPay
   * @return boolean
   */
  private function _addOrdersToTransportToStatments($iStatment, $aStatments, $sTransportNumber, $iOrderId, $sPayed, $sToPay) {
    global $aConfig;
    
    $aValues = array(
        'orders_bank_statement_id' => $iStatment,
        'transport_number' => $sTransportNumber,
        'order_id' => $iOrderId,
        'payed' => floatval(str_replace(',', '.', $sPayed)),
        'topay' => floatval(str_replace(',', '.', $sToPay)),
        'orders_bank_statements' => serialize($aStatments)
    );
    if (Common::Insert($aConfig['tabls']['prefix']."orders_linking_transport_numbers", $aValues) === false) {
      return false;
    }
    return true;
  }// end of _addOrdersToTransportToStatments() method
  
  
  /**
   * Metoda pobiera numer zamówienia
   * 
   * @global array $aConfig
   * @param int $iOrderId
   * @return string
   */
  private function _getOrderNumber($iOrderId) {
    
    $sSql = "
      SELECT order_number 
      FROM orders 
      WHERE id = ".$iOrderId;
    return Common::GetOne($sSql);
  }// end of _getOrderNumber() method
  
  
  /**
   * Metoda dodaje płatność do listy płatności zamówienia
   * 
   * @global array $aConfig
   * @param int $iOrderId
   * @param string $sTransferDate
   * @param string $sTransferPayedValue
   * @param string $sTransferTitle
   */
  public function addPayment($iOrderId, $sTransferDate, $sTransferPayedValue, $sTransferTitle, $sTransportNumber, $iTransportId, $iStatementId = 0) {
    global $aConfig;
    
    $aValues2 = array(
      'order_id' => $iOrderId,
      'statement_id' => ($iStatementId > 0 ? $iStatementId : 'NULL'),
      'date' => $sTransferDate,
      'ammount' => floatval(str_replace(',', '.', $sTransferPayedValue)),
      'type' => ($iTransportId == '1' ? 'opek' : $this->getTransportSymbol($iTransportId)),
      'iban' => '',
      'reference' => $sTransportNumber, // nr listu przewozowego
      'title' => $sTransferTitle,
      'matched_by' => $_SESSION['user']['name']
    );
      
    if(Common::Insert($aConfig['tabls']['prefix']."orders_payments_list", $aValues2,'', false) === false){
        return false;	
    }
    return true;
  }// end of _addPayment() method
  
  
  /**
   * 
   * @param int $iTransportId
   * @return string
   */
  private function getTransportSymbol($iTransportId) {
    
    $sSql = 'SELECT symbol 
      FROM orders_transport_means 
      WHERE id IN ( '.$iTransportId.' )
      LIMIT 1';
    return Common::GetOne($sSql);
  }
  
  
  /**
   * Metoda opłaca pojedyncze zamówienie
   * 
   * @param int $iOrderId
   * @param string $sPayed
   * @param string $sTransportNumber
   * @param array $sLastestPaymentDate
   * @return bool
   */
  public function payOrder($iOrderId, $sPayed, $sTransportNumber, $sLastestPaymentDate) {
    global $aConfig;
    
    // sprwadzamy czy jest to 1, czy 2 płatność
    $sGetPTypeSql = "SELECT second_payment_type, payment_status, second_payment_status, transport_id
                     FROM ".$aConfig['tabls']['prefix']."orders
                     WHERE id=".$iOrderId;
    $aOrderData = Common::GetRow($sGetPTypeSql);
    if ($aOrderData['second_payment_status'] == '1') {
      // jeśli druga metoda płatności jest opłacona to błąd,
      // nie ma 3 metody płatności
      return -1;
    }
    
    $sPoolPrefix = '';
    if ($aOrderData['payment_status'] == '1') {
      // jeśli druga metoda płatności to płatność przy odbiorze lub 
      // pierwsza metoda płatności jest już opłacona to opłacamy 2
      $sPoolPrefix = 'second_';
    }
    
    $iPaymentId = $this->_getBankTransferIdByTransportId('postal_fee', $aOrderData['transport_id']);
    $sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders
             SET paid_amount = (paid_amount + ".floatval(str_replace(',', '.', $sPayed))."), 
                 ".$sPoolPrefix."payment_date = '".$sLastestPaymentDate."',
                 ".$sPoolPrefix."payment_status = '1',
                 ".$sPoolPrefix."payment_type = 'postal_fee',
                 ".$sPoolPrefix."payment_id = ".$iPaymentId.",
                 ".($sPoolPrefix != '' ? ' second_payment_enabled = "1", ' : '')."
                 ".$sPoolPrefix."payment = 'płatność przy odbiorze'
             WHERE id = ".$iOrderId."
             LIMIT 1";
    if(Common::Query($sSql) === false) {
      return -2;
    }
    return true;
  }// end of payOrder() method
  
  
	/**
	 * Metoda pobiera id typu płatności
	 *
	 * @global type $aConfig
	 * @param type $sPType
	 * @param type $iTransportId
	 * @return type 
	 */
	private function _getBankTransferIdByTransportId($sPType, $iTransportId) {
		global $aConfig;
		
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders_payment_types
						 WHERE 	transport_mean=".$iTransportId." AND ptype='".$sPType."'";
		return Common::GetOne($sSql);
	}// end of getBankTransferIdByTransportId() method
  
  
	/**
	 * Metoda sprawdza czy wartości się powtarzaja
	 *
	 * @param array $aArr
	 * @return bool 
	 */
	private function _arrayOrdersHaveDuplicates($aArr) {
		
    $aNewOrder = array();
    foreach ($_POST['transfers_orders'] as $aTransferOrder) {
      foreach ($aTransferOrder as $iOrderId) {
        if ($iOrderId > 0) {
          if (isset($aNewOrder[$iOrderId])) {
            return true;
          } else {
            $aNewOrder[$iOrderId] = 1;
          }
        }
      }
    }
		// brak duplikatow
		return false;
	}// end of arrayHaveDuplicateKey() method
  
  
  /**
   * Metoda wyświetla listę przelewów do OPEK
   * 
   * @global array $aConfig
   * @param object $pSmarty
   */
  public function Show(&$pSmarty) {
    		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');
		include_once('Form/Form.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true,
			'editable' => true,
			'checkboxes'=>false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'paidamount',
				'content'	=> $aConfig['lang'][$this->sModule]['list_transfer_value'],
				'sortable'	=> false,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang'][$this->sModule]['list_import'],
				'sortable'	=> false,
			  'width'=>'150'
			),
			array(
				'db_field'	=> 'matched_order',
				'content'	=> $aConfig['lang'][$this->sModule]['list_title'],
				'sortable'	=> false,
        'width'	=> '100',
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
		
		
		$sFilter='';

		switch($_POST['f_status']) {
			default: $sFilter=' AND finished = "0" AND `deleted`=\'0\''; 	break;
			case'3': $sFilter=' AND finished = "1" AND `deleted`=\'0\''; 	break;
			case'1': $sFilter=' AND `deleted`=\'1\' '; 	break;
			case'2': $sFilter=''; 	break;
		}
		
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(DISTINCT A.id)
						 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements AS A
						 WHERE A.is_Opek='1' ".$sFilter.
		(isset($_POST['search']) && !empty($_POST['search']) ? (" AND (A.title LIKE '%".$_POST['search']."%' OR A.id LIKE '%".$_POST['search']."%') ") : '');
		$iRowCount = intval(Common::GetOne($sSql));

		$pView = new View('orders_bank_statements', $aHeader, $aAttribs, $iRowCount);
		$pView->AddRecordsHeader($aRecordsHeader);

		// FILTRY
		// wszystkie/usuniete/nierozliczone
		$aStatuses = array(
			array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_new']),
			array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['f_matched']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_deleted']),
			array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
		);
		$pView->AddFilter('f_status', $aConfig['lang'][$this->sModule]['f_status'], $aStatuses, $_POST['f_status']);
		
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT  DISTINCT A.id, A.paid_amount paidamount, A.created, A.title, OLTN.order_id as linking_id
						 	 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements AS A
               LEFT JOIN ".$aConfig['tabls']['prefix']."orders_linking_transport_numbers AS OLTN
                 ON OLTN.orders_bank_statement_id = A.id AND OLTN.order_id > 0
						 	 WHERE A.is_Opek='1' ".$sFilter.
			(isset($_POST['search']) && !empty($_POST['search']) ? (" AND (A.title LIKE '%".$_POST['search']."%' OR A.id LIKE '%".$_POST['search']."%') ") : '').
               " GROUP BY A.id ".
							 " ORDER BY ".(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							" LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			foreach($aRecords as $iKey => $aItem) {
				$aRecords[$iKey]['paidamount'] = Common::formatPrice($aItem['paidamount']);
				$aRecords[$iKey]['paidamount'] .= ' zł';
        if (intval($aItem['linking_id']) <= 0) {
          $aRecords[$iKey]['disabled'][] = 'details';
        }
			}
					
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
        'linking_id'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('details'),
					'params' => array (
												'details'	=> array('id' => '{id}'),
											)
				)	
			);
      
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}

    $aRecordsFooter = array(array(),array('paczkomat_pdf_download'));
    $aRecordsFooterParams = array('paczkomat_pdf_download');

		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.
																				$pView->Show());
  }// end of Show() method
  
  
  /**
   * Klasa importuje powiązania przelewy - zamowienia
   * 
   * @param type $pSmarty
   */
  public function importTransfersOrders(&$pSmarty) {
    global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_0'];

		$pForm = new FormTable('import_statements', $sHeader, array('action'=>phpSelf(), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'import');

    
		// nazwa
		$pForm->AddFile('transfers', $aConfig['lang'][$this->sModule]['file'],  array('style'=>'width: 350px;'));
    
    $pForm->AddCheckBox('only_compared', _('Wyszukaj zamówienia tylko dla zadanych listów'), array(), '', true, false);
    $pForm->AddSelect('file_type', _('Typ przelewu'), array(), addDefaultValue($this->getFileTypes()), $aData['file_type'], '', true);
    
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_0']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
  }// end of importTransfersOrders() method
  
  /**
   * Metoda importuje przelewy
   * 
   * @global array $aConfig
   * @param type $pSmarty
   */
  public function Import(&$pSmarty) {
    global $aConfig, $aTransfers;
    
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
    $sFileType = $_POST['file_type'];
    
    // dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Show($pSmarty);
			return;
		}
    
    $iTransfersCount = 0;
//		$aFile = file($_FILES['transfers']['tmp_name']);
    
    $aTransfers = array();
    $iCSVCountTransfers = 0;
    $fTransferSummary = 0.00;

    $iRow = 0;
    $handle = fopen($_FILES['transfers']['tmp_name'], "rb");
    while (($sStatment = fgets($handle, 4096)) !== false) {
      // zmiana kodowania pliku
      $sStatment = iconv('cp1250', 'UTF-8', trim($sStatment));
      $aStatment[$iRow] = explode(';', $sStatment);

      // uwaga tutaj
      $aItStat =& $aStatment[$iRow]; 
      if (!empty($aItStat) && is_array($aItStat)) {
        // lecimy po elementach przelewu
        foreach ($aItStat as $iSKey => $sSVal) {
          $aItStat[$iSKey] = preg_replace('/^"?\??(.*?)\??"?$/', '$1', trim($sSVal));
        }

        if ($sFileType == 'opek-przesylka-kurierska') {
          if ($this->validateFedEx($aItStat) === false) {
            $this->sMsg = GetMessage(_('Wystąpił błąd podczas importu danych z CSV. <br />Rozmiar tablicy danych jest nieprawidłowy "'.count($aItStat).nl2br(print_r($aItStat, true).'".')));
            $this->Show($pSmarty);
          }
          $this->getItemTransferFedExRef($aTransfers, $fTransferSummary, $iCSVCountTransfers, $aItStat);
        } elseif ($sFileType == 'tba') {
          if ($this->validateTBA($aItStat) === false) {
            $this->sMsg = GetMessage(_('Wystąpił błąd podczas importu danych z CSV. <br />Rozmiar tablicy danych jest nieprawidłowy "'.count($aItStat).nl2br(print_r($aItStat, true).'".')));
            $this->Show($pSmarty);
          }
          $this->getItemTransferTBARef($aTransfers, $aItStat);
        } elseif ($sFileType == 'ruch') {
          if ($this->validateTBA($aItStat) === false) {
            $this->sMsg = GetMessage(_('Wystąpił błąd podczas importu danych z CSV. <br />Rozmiar tablicy danych jest nieprawidłowy "'.count($aItStat).nl2br(print_r($aItStat, true).'".')));
            $this->Show($pSmarty);
          }
          $this->getItemTransferTBARef($aTransfers, $aItStat);
        }
        elseif ($sFileType == 'poczta_polska') {
          if ($iRow > 0) {
            if ($this->validatePP($aItStat) === false) {
              $this->sMsg = GetMessage(_('Wystąpił błąd podczas importu danych z CSV. <br />Rozmiar tablicy danych jest nieprawidłowy "'.count($aItStat).nl2br(print_r($aItStat, true).'".')));
              $this->Show($pSmarty);
            }
            $this->getItemTransferPPRef($aTransfers, $aItStat);
          }
        }
        elseif ($sFileType == 'poczta-polska-doreczenie-pod-adres')
        {
            if ($iRow > 0) {
                if ($this->validatePP($aItStat) === false) {
                    $this->sMsg = GetMessage(_('Wystąpił błąd podczas importu danych z CSV. <br />Rozmiar tablicy danych jest nieprawidłowy "'.count($aItStat).nl2br(print_r($aItStat, true).'".')));
                    $this->Show($pSmarty);
                }
                $this->getItemTransferPPRef($aTransfers, $aItStat);
            }
        }
        $iRow++;
      }
    }
    
    if (!feof($handle)) {
        echo "Error: unexpected fgets() fail\n";
    }
    fclose($handle);
    if ($sFileType == 'opek-przesylka-kurierska') {
      $mFedExReturn = $this->validateCountFedEx($aTransfers, $iCSVCountTransfers, $fTransferSummary);
      switch ($mFedExReturn) {
        case true:
          $this->sMsg = GetMessage(_('Plik z przelewami do zamówień został zaimportowany.<br /><span style="color: red"> Dopasuj listy przewozowe do zamówień oraz wybierz przelewy do rozliczenia.</span>'), false);
          // wyświetlamy formularz matchowania i przekazujemy dane z importu do formularza
          $this->getTransfersForm($pSmarty, $aTransfers, $iCSVCountTransfers, $fTransferSummary);
          return true;
        break;
      
        case '-1';
          $this->sMsg = GetMessage(_('Wystąpił błąd podczas importu danych z CSV. <br /> Ilość przelewów nie zgadza się z ilością podaną w podsumowaniu.'));
          $this->Show($pSmarty);
          return false;
        case '-2':
          $this->sMsg = GetMessage(_('Wystąpił błąd podczas importu danych z CSV. <br /> Dane są nieprawidłowe.'));
          $this->Show($pSmarty);
          return false;
        break;
      }
    } else {
      $this->sMsg = GetMessage(_('Plik z przelewami do zamówień został zaimportowany.<br /><span style="color: red"> Dopasuj listy przewozowe do zamówień oraz wybierz przelewy do rozliczenia.</span>'), false);
      // wyświetlamy formularz matchowania i przekazujemy dane z importu do formularza
      $this->getTransfersForm($pSmarty, $aTransfers, $iCSVCountTransfers, $fTransferSummary);
      return true;
    }
  }// end of Import() method
  
  /**
   * 
   * @param array $aTransfers
   * @param int $iCSVCountTransfers
   * @param float $fTransferSummary
   * @return boolean
   */
  private function validateCountFedEx($aTransfers, $iCSVCountTransfers, $fTransferSummary) {
    
    if (!empty($aTransfers) && $iCSVCountTransfers > 0 && $fTransferSummary > 0.00 && count($aTransfers) == $iCSVCountTransfers) {
      return 1;
    } elseif(count($aTransfers) != ($iCSVCountTransfers+1)) {
      return '-1';
    } else {
      return '-2';
    }
  }
  
  /**
   * 
   * @param array $aItStat
   * @return boolean
   */
  private function validatePP($aItStat) {
    if (count($aItStat) != 7) {
      return false;
    }
    return true;
  }
  
  /**
   * 
   * @param array $aItStat
   * @return boolean
   */
  private function validateTBA($aItStat) {
    if (count($aItStat) != 7) {
      return false;
    }
    return true;
  }
  
  /**
   * 
   * @param array $aTransfers
   * @param float $fTransferSummary
   * @param int $iCSVCountTransfers
   * @param array $aItStat
   * @return array
   */
  private function getItemTransferTBARef(array &$aTransfers, array &$aItStat) {
    $raMatchedOrdersNumbers = array();
    $aMatches = array();
    
    $aItStat['9'] = str_replace(' ', '', $aItStat['9']);
    preg_match('/(\d+([,\.]\d{1,2})?)/', $aItStat['9'], $aMatches);
    
    $aItStat['1'] = $aItStat['5'];//kompatybilnosc z widokiem
    $aItStat['5'] = isset($aMatches[1]) ? $aMatches[1] : 0.00;
    
    
    // czy przelew
    if ($this->_checkIsTBAPayment($aItStat, $raMatchedOrdersNumbers)) {
      if (!empty($raMatchedOrdersNumbers) && is_array($raMatchedOrdersNumbers)) {
        $aItStat['orders'] = $raMatchedOrdersNumbers;
      }
      $aItStat['orders'] = array();
      $aOrNumbers = $this->_getOrdersNumByTransportNumber($aItStat['1']);
      if ($aItStat['1'] != '') {
        $aItStat['orders'] = $aOrNumbers;
        $aTransfers[] = $aItStat;
      }
    }
  }
  
  /**
   * 
   * @param array $aTransfers
   * @param array $aItStat
   */
  private function getItemTransferPPRef(array &$aTransfers, array &$aItStat) {
    
    $raMatchedOrdersNumbers = array();
    $aMatches = array();
    $aItStat['5'] = $aItStat['2'];
    
    // czy przelew
    if ($this->_checkIsPPPayment($aItStat, $raMatchedOrdersNumbers)) {
      if (!empty($raMatchedOrdersNumbers) && is_array($raMatchedOrdersNumbers)) {
        $aItStat['orders'] = $raMatchedOrdersNumbers;
      }
      $aItStat['orders'] = array();
      $aOrNumbers = $this->_getOrdersNumByTransportNumber($aItStat['1']);
      if ($aItStat['1'] != '') {
        $aItStat['orders'] = $aOrNumbers;
        $aTransfers[] = $aItStat;
      }
    }
  }
  
  
  /**
   * 
   * @param array $aTransfers
   * @param float $fTransferSummary
   * @param int $iCSVCountTransfers
   * @param array $aItStat
   * @return array
   */
  private function getItemTransferFedExRef(array &$aTransfers, &$fTransferSummary, &$iCSVCountTransfers, array &$aItStat) {
    $raMatchedOrdersNumbers = array();

    // czy przelew
    if ($this->_checkIsFedExPayment($aItStat, $raMatchedOrdersNumbers)) {
      if (!empty($raMatchedOrdersNumbers) && is_array($raMatchedOrdersNumbers)) {
        $aItStat['orders'] = $raMatchedOrdersNumbers;
      }
      $aItStat['orders'] = array();
      $aOrNumbers = $this->_getOrdersNumByTransportNumber($aItStat['1']);
      $aItStat['orders'] = $aOrNumbers;
      $aTransfers[] = $aItStat;
    } else {
      // czy podsumowanie
      $mRet = $this->_checkIsSummary($aItStat);
      if ($mRet !== false) {
        $fTransferSummary = $mRet;
      } else {
        // czy ilość przelewów
        $mRet = $this->_checkIsTransfersCount($aItStat);
        if ($mRet > 0) {
          $iCSVCountTransfers = $mRet;
        }
      }
    }
  }
  
  
  private function validateFedEx($aItStat) {

    // rozmiar tablic to 5
    if (count($aItStat) != 6) {
      return false;
    }
  }
  
  
  /**
   * Metoda wyświetla formularz wyboru zamówień OPEK do listów przewozowych oraz przelewów do rozliczenia
   * 
   * @param array $aTransfers
   * @param int $iCountTransfers
   * @param float $fTransferSummary
   */
  public function getTransfersForm(&$pSmarty, $aTransfers, $iCountTransfers, $fTransferSummary) {
    $aSelfTransfers = $aTransfers;
    $aOrdersToPayed = array();
    $arefOrdersToPayedBalance = array();
    
    $aTransportNumbers = array();
    if ($_POST['only_compared'] == '1') {
      foreach ($aTransfers as $aTransfer) {
        $aTransportNumbers[] = $aTransfer[1];
      }
    }
    $aOrdersToPayed = $this->getOrdersToCompare($arefOrdersToPayedBalance, $aTransportNumbers);
    
    foreach ($aTransfers as $iTransferKey => $aTransfer) {
      
      if (!empty($aTransfer['orders'])) {
        // wygeneruj przyciski
        foreach ($aTransfer['orders'] as $iOrderKey => $sOrderNumber) {
          $aTransfers[$iTransferKey]['transfers_orders'][] = $this->_getSelectOrders($aOrdersToPayed, $iTransferKey, $iOrderKey, $aTransfer, $sOrderNumber);
        }
      } else {
        // wygeneruj jeden przycisk
        $aTransfers[$iTransferKey]['transfers_orders'][] = $this->_getSelectOrders($aOrdersToPayed, $iTransferKey, '0', $aTransfer);
      }
    }
	
    if (isset($_POST['bank_transfers']) && !empty($_POST['bank_transfers'])) {
      foreach ($_POST['bank_transfers'] as $iKey => $aBankTransfer) {
        $aBankTransfer[$iKey] = $this->_getSelectTransfers($iKey);
      }
    } else {
      $aBankTransfer[] = $this->_getSelectTransfers(0);
    }
    
    $sTransferSerialized = (! get_magic_quotes_gpc ()) ? addslashes(serialize($aSelfTransfers)) : serialize($aSelfTransfers);
    $pSmarty->assign_by_ref('sTransferSerialized', $sTransferSerialized);
    $pSmarty->assign_by_ref('aBankTransfer', $aBankTransfer);
    $pSmarty->assign_by_ref('sOrdersToPayedBalanceJS', json_encode($arefOrdersToPayedBalance));
    $pSmarty->assign_by_ref('aTransfers', $aTransfers);
    $pSmarty->assign_by_ref('iCountTransfers', $iCountTransfers);
    $pSmarty->assign_by_ref('fTransferSummary', $fTransferSummary);
    $pSmarty->assign_by_ref('iTransportId', $this->iTransportId);
    $sHTML = $pSmarty->fetch('finanse/opek_transfers.tpl');
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($sHTML));
  }// end of _getTransfersForm() method
  
  
  /**
   * Metoda pobiera zamówienia do wybrania
   * 
   * @param type $iTransferKey
   * @param type $iOrderKey
   * @param type $aTransfer
   * @param type $sOrderNumber
   * @return type
   */
  private function _getSelectOrders($aOrdersToPayed, $iTransferKey, $iOrderKey, $aTransfer, $sOrderNumber = '') {
    include_once('Form/Form.class.php');
    
    $sEleSelectKey = 'transfers_orders['.$iTransferKey.']['.$iOrderKey.']';
    if (!empty($_POST['transfers_orders'])) {
      $sValueSelect = $_POST[$sEleSelectKey];
    } else {
      $sValueSelect = $this->_getOrdersByTransportNumber($aTransfer['1'], $sOrderNumber, $aTransfer[5], $this->aOrdersUsed);
    }
    if ($sValueSelect > 0) {
      $this->aOrdersUsed[] = $sValueSelect;
    }

    return Form::GetSelect($sEleSelectKey, array('class' => 'transfers_orders'), $aOrdersToPayed, $sValueSelect);
  }// end of _getSelectOrders() method
  
  
  /**
   * Metoda pobiera przelewy które można połączyć z zamówieniami
   * 
   * @param type $iTKey
   * @return type
   */
  private function _getSelectTransfers($iTKey) {
    include_once('Form/Form.class.php');
    
    $sEleSelectKey = 'bank_transfers['.$iTKey.']';
    if (!empty($_POST['bank_transfers'])) {
      $sValueSelect = $_POST[$sEleSelectKey];
    }
    
    $aTransfers = $this->_getTransfersNotFinished();
    return Form::GetSelect('bank_transfers[]', array('class' => 'bank_transfers'), addDefaultValue($aTransfers), $sValueSelect);
  }// end of _getSelectTransfers() method
  
  
  /**
   * Metoda pobiera przelewey nie zakończone, które można powiązać z zamówieniami
   * 
   * @global array $aConfig
   * @return type
   */
  private function _getTransfersNotFinished() {
    global $aConfig;
    
			$sSql = "SELECT DISTINCT A.id as value, CONCAT(A.paid_amount, ' ', A.created, ' ', A.title) as label
						 	 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements AS A
						 	 WHERE A.is_Opek='1' AND A.finished='0' AND `deleted`='0'";
    return Common::GetAll($sSql);  
  }// end of getTransfersNotFinished() method
  
  
  /**
   * Metoda znajduje zamówienia które mogą być wybrane do opłacenia
   * 
   * @global array $aConfig
   * @param array $arefBalance - tablica referencyjna uzupełniana tablicą {{id, balance}}
   * @return array
   */
  private function getOrdersToCompare(&$arefBalance, $aTransportNumbers = array()) {
		global $aConfig;
    
		//oszczędzamy zasoby
		if(is_array($this->aOrdersToCompare))
			return $this->aOrdersToCompare;
		
		$sSql = "SELECT 
              id AS value, 
              CONCAT(order_number, ' ', IF(ISNULL(transport_number),' ',transport_number)) AS label,
              (to_pay-paid_amount) as balance
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE order_status = '4'
						AND (payment_status = '0' OR IF(second_payment_enabled='1', second_payment_status = '0', 1=2) OR paid_amount < to_pay )
						AND (payment_type = 'postal_fee' OR second_payment_type = 'postal_fee')
						AND correction != '1'
            ".
            (!empty($aTransportNumbers) ? " AND transport_number IN ('".implode("','", $aTransportNumbers)."') " : '');
            ($this->iTransportId > 0 ? " AND transport_id IN (" . $this->iTransportId . ") " : '');
		$aOrders = Common::GetAll($sSql);
    
    $arefBalance = array();
    foreach ($aOrders as $iOKey => $aOrder) {
      $arefBalance[$aOrder['value']] = str_replace('.', ',', $aOrder['balance']);
      unset($aOrders[$iOKey]['balance']);
    }
    
    if (!empty($aOrders)) {
      $this->aOrdersToCompare = array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),$aOrders); 
    }
		return $this->aOrdersToCompare;
  }// end of getOrdersToCompare() method
  

  /**
   * Metoda pobiera numer zamówienia na podstawie nr listu przewozowego
   * 
   * @global array $aConfig
   * @param string $sTransportNumber
   * @param string $sOrderNumber
   * @return int
   */
  private function _getOrdersByTransportNumber($sTransportNumber, $sOrderNumber, $sPayed, $aOmmit) {
    global $aConfig;
    
    // w pierwszej kolejności dopasowujemy jeśli pasuje order_number i transport number
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders A
						WHERE A.id NOT IN (".implode(',', $aOmmit).") 
             AND A.order_number = '".$sOrderNumber."' 
             AND A.transport_number = '".$sTransportNumber."'
             ".($this->iTransportId != '' ? " AND transport_id IN (" . $this->iTransportId . ") " : '');
		$iRetOrder = Common::GetOne($sSql);	
    if (intval($iRetOrder) > 0) {
      return $iRetOrder;
    }
    
    // w pierwszej kolejności dopasowujemy jeśli pasuje order_number i transport number
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders A
						WHERE  A.id NOT IN (".implode(',', $aOmmit).") 
             AND A.to_pay = '".str_replace(',', '.', $sPayed)."' 
             AND A.transport_number = '".$sTransportNumber."'
             ".($this->iTransportId != '' ? " AND transport_id IN (" . $this->iTransportId . ") " : '');
    
		$iRetOrder = Common::GetOne($sSql);	
    if (intval($iRetOrder) > 0) {
      return $iRetOrder;
    }
    
    // kolejno dopasowuje zamówienia z pasującym transport number
    $sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders A
						WHERE A.id NOT IN (".implode(',', $aOmmit).") 
             AND A.transport_number = '".$sTransportNumber."'
             ".($this->iTransportId != '' ? " AND transport_id IN (" . $this->iTransportId . ") " : '');
		return Common::GetOne($sSql);	
  }// end of _getOrdersByTransportNumber() method
  
  
  /**
   * Metoda pobiera numer zamówienia na podstawie nr listu przewozowego
   * 
   * @global array $aConfig
   * @param string $sTransportNumber
   * @param string $sOrderNumber
   * @return int
   */
  private function _getOrdersNumByTransportNumber($sTransportNumber) {
    global $aConfig;
    
    // kolejno dopasowuje zamówienia z pasującym transport number
    $sSql = "SELECT order_number FROM ".$aConfig['tabls']['prefix']."orders
						WHERE transport_number = '".$sTransportNumber."'
            ".($this->iTransportId != '' ? " AND transport_id IN (" . $this->iTransportId . ") " : '');
		return Common::GetCol($sSql);	
  }// end of _getOrdersNumByTransportNumber() method
  
  
  /**
   * Metoda sprawdza czy jest to ilosc pozycji
   * 
   * @param array $aData - przelew
   * @return boolean|int
   */
  private function _checkIsTransfersCount($aData) {
    
    if ($aData[4] === 'Ilość pozycji' && 
        intval($aData[5]) > 0 &&
        empty($aData[0]) &&
        empty($aData[1]) &&
        empty($aData[2]) &&
        empty($aData[3]) ) {
      
      return intval($aData[5]);
    }
    return false;
  }// end of _checkIsTransfersCount() method
  
  
  /**
   * Metoda sprawdza czy jest to podsumowanie raportu
   * 
   * @param array $aData - przelew
   * @return boolean|float
   */
  private function _checkIsSummary($aData) {
    
    if ($aData[4] === 'Razem' && 
        floatval(str_replace(',', '.', $aData[5])) > 0.00 &&
        empty($aData[0]) &&
        empty($aData[1]) &&
        empty($aData[2]) &&
        empty($aData[3]) ) {
      
      return floatval(str_replace(',', '.', $aData[5]));
    }
    return false;
  }// end of _checkIsSummary() method
  
  /**
   * 
   * @param array $aData
   * @param array ref $raMatchedOrdersNumbers
   */
  private function _checkIsPPPayment($aData, &$raMatchedOrdersNumbers) {
    
//    // sprawdzamy pola
//    // Data przekazania do banku
//    if (intval(preg_match("/^\d{4}-\d{2}-\d{2}$/", $aData['2'])) <= 0) {
//      return false;
//    }
    
    // kwota przelewu
    if (floatval(str_replace(',', '.', $aData['2'])) <= 0.00) {
      return false;
    }
    return true;
  }// end of _checkFedExIsPayment() method
  
  
  /**
   * 
   * @param array $aData
   * @param array ref $raMatchedOrdersNumbers
   */
  private function _checkIsTBAPayment($aData, &$raMatchedOrdersNumbers) {
    
    // sprawdzamy pola
    // Data przekazania do banku
    if (intval(preg_match("/^\d{4}-\d{2}-\d{2}$/", $aData['8'])) <= 0) {
      return false;
    }
    
    // kwota przelewu
    if (floatval(str_replace(',', '.', $aData['9'])) <= 0.00) {
      return false;
    }
    return true;
  }// end of _checkFedExIsPayment() method
  
  /**
   * Metoda sprawdza czy dane odpowiadają za przelew
   * 
   * @global array $aConfig
   * @param array $aData - przelew
   * @return boolean
   */
  private function _checkIsFedExPayment($aData, &$raMatchedOrdersNumbers) {
    global $aConfig;
    
    // sprawdzamy pola
    // Data przekazania do banku
    if (intval(preg_match("/^\d{4}-\d{2}-\d{2}$/", $aData['0'])) <= 0) {
      return false;
    }
    
    // Nr listu
    if (intval(preg_match("/^(\w+)$/", $aData['1'])) <= 0) {
      return false;
    }
    
    // Data nadania
    if (intval(preg_match("/^\d{4}-\d{2}-\d{2}$/", $aData['2'])) <= 0) {
      return false;
    }
    
    // z opisu wyciągnijmy nr zamowienia
    $aMatched = array();
    preg_match_all('/('.$aConfig['regexp_order_number'].')/', $aData['3'], $aMatched);
    if (!empty($aMatched[1])) {
      // @ref
      $raMatchedOrdersNumbers = $aMatched[1]; 
    }
    
    // kwota przelewu
    if (floatval(str_replace(',', '.', $aData['5'])) <= 0.00) {
      return false;
    }
    return true;
  }// end of _checkFedExIsPayment() method
}// end of class
