<?php

namespace omniaCMS\modules\m_finanse;

use Admin;
use BankParser\BankParser;
use DatabaseManager;
use FormTable;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use Smarty;
use Validator;
use View;

class Module__finanse__parse_bank_files implements ModuleEntity
{

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    public function __construct(Admin $oClassRepository) {
        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule.$this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function doDefault()
    {
        return $this->parseBankFilesAction();
    }

    public function getMsg() {
        return $this->sMsg;
    }

    private function getBankAccounts()
    {
        return $this->pDbMgr->GetAll('profit24', "SELECT ident_statments_file as label, ident_statments_file as value from bank_accounts group by ident_statments_file");
    }

    private function parseBankFilesAction()
    {

        include_once('lib/Form/FormTable.class.php');

        $form = new \FormTable('bank_file_parser', _('Parsuj plik do XML\'a'), ['action' => phpSelf(['do' => 'parseFile']), 'method' => 'POST', 'enctype' => 'multipart/form-data']);
        $form->AddFile('file', _('Plik TXT'));
        $form->AddSelect('bank_number', _('Wybierz nr. konta bankowego'), [], $this->getBankAccounts());
        $form->AddRow('&nbsp;', $form->GetInputButtonHTML('send', _('Generuj')));

        return $form->ShowForm();
    }

    public function doParseFile()
    {

        $bankParser = new BankParser();

        try {
            $filePath = $bankParser->parseBankFile($_FILES['file']['tmp_name'], $_POST['bank_number']);
        } catch (\Exception $e) {
            $this->sMsg .= getMessage($e->getMessage());
            return $this->doDefault();
        }

        $data = file_get_contents($filePath);
        unlink($filePath);

        header('Content-type: text/xml');
        header('Content-Disposition: attachment; filename="'.$_POST['bank_number'].'.xml"');
        echo $data;
        die;
    }
}