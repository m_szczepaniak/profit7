<?php

/** do "Przelewy Pobrania" **/
$aConfig['lang']['m_finanse_fedex_transfers']['f_status'] = "Status przelewu";
$aConfig['lang']['m_finanse_fedex_transfers']['f_all'] = "Wszystkie";
$aConfig['lang']['m_finanse_fedex_transfers']['f_deleted'] = "Usunięty";
$aConfig['lang']['m_finanse_fedex_transfers']['f_new'] = "Nierozliczony";
$aConfig['lang']['m_finanse_fedex_transfers']['f_matched'] = "Rozliczony";
$aConfig['lang']['m_finanse_fedex_transfers']['go_back'] = "Powrót na listy przelewów";
$aConfig['lang']['m_finanse_fedex_transfers']['no_linked_order'] = "<span style='color: red'>Poza zamówieniami</span>";
$aConfig['lang']['m_finanse_fedex_transfers']['no_linked'] = "<span style='color: red'>Nie powiązane</span>";

$aConfig['lang']['m_finanse_fedex_transfers']['list'] = 'Przelewy pobrań';
$aConfig['lang']['m_finanse_fedex_transfers']['list_import'] = "Dodano";
$aConfig['lang']['m_finanse_fedex_transfers']['list_title'] = "Tytuł";
$aConfig['lang']['m_finanse_fedex_transfers']['list_transfer_value'] = "Wartość przelewu";
$aConfig['lang']['m_finanse_fedex_transfers']['import_transfers'] = "Importuj przelewy";

$aConfig['lang']['m_finanse_fedex_transfers']['no_items'] = 'Brak przelewów';
$aConfig['lang']['m_finanse_fedex_transfers']['payment_from_day'] = 'Przelew z dnia ';
$aConfig['lang']['m_finanse_fedex_transfers']['for_ammount'] = ' na kwotę ';
$aConfig['lang']['m_finanse_fedex_transfers']['send_button_save'] = 'Zatwierdź';
$aConfig['lang']['m_finanse_fedex_transfers']['delete_from_opek'] = 'Usuń';
$aConfig['lang']['m_finanse_fedex_transfers']['del_err'] = 'Wystąpił błąd podczas usuwania przelewu. Spróbuj ponownie.';
$aConfig['lang']['m_finanse_fedex_transfers']['del_ok'] = 'Usunięto przelew';
$aConfig['lang']['m_finanse_fedex_transfers']['status_edit_err'] = 'Wystąpił bład podczas zatwierdzania przelewów. Spróbuj ponownie.';
$aConfig['lang']['m_finanse_fedex_transfers']['status_edit_ok'] = 'Przelewy zostały zatwierdzone';
$aConfig['lang']['m_finanse_fedex_transfers']['save_err_unique'] = 'Wystąpił błąd podczas edycji przelewu: <li>Użyto dwa razy tego samego nr zamówienia</li>';

$aConfig['lang']['m_finanse_fedex_transfers']['edit_err'] = 'Wystąpił błąd podczas edycji przelewu. Spróbuj ponownie.';
$aConfig['lang']['m_finanse_fedex_transfers']['edit_ok'] = 'Zmieniono przelew';
$aConfig['lang']['m_finanse_fedex_transfers']['paczkomat_pdf_download'] = 'Importuj plik specyfikacji do przelewu';
$aConfig['lang']['m_finanse_fedex_transfers']['header_0'] = 'Import pliku z przelewami do zamówień ';
$aConfig['lang']['m_finanse_fedex_transfers']['file'] = 'Wybierz plik CSV';



// przelewy bankowe
$aConfig['lang']['m_finanse_statements']['list'] = 'Przelewy bankowe';
$aConfig['lang']['m_finanse_statements']['no_items'] = 'Brak przelewów';
$aConfig['lang']['m_finanse_statements']['list_title'] = 'Tytułem';
$aConfig['lang']['m_finanse_statements']['list_order_nr'] = 'Nr zamówienia';
$aConfig['lang']['m_finanse_statements']['list_matched_order'] = 'Zamówienie';
$aConfig['lang']['m_finanse_statements']['list_paid_amount'] = 'Zapłacono';
$aConfig['lang']['m_finanse_statements']['list_to_pay_amount'] = 'Do zapłaty';
$aConfig['lang']['m_finanse_statements']['list_diff'] = 'Różnica';
$aConfig['lang']['m_finanse_statements']['list_created'] = 'Dodano';
$aConfig['lang']['m_finanse_statements']['add'] = 'Zaimportuj listę przelewów';
$aConfig['lang']['m_finanse_statements']['auto_match'] = 'Znajdź i rozlicz z zamówieniem - tak jak robi to automat';
$aConfig['lang']['m_finanse_statements']['details'] = 'Przypisz wpłatę do wielu zamówień';
$aConfig['lang']['m_finanse_statements']['header_0'] = 'Zaimportuj wyciąg z konta';
$aConfig['lang']['m_finanse_statements']['file'] = 'Plik wyciągu';
$aConfig['lang']['m_finanse_statements']['del_ok_0']		= "Usunięto przelew %s";
$aConfig['lang']['m_finanse_statements']['del_ok_1']		= "Usunięto przelewy %s";
$aConfig['lang']['m_finanse_statements']['del_err_0']		= "Nie udało się usunąć przelewu %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_finanse_statements']['del_err_1']		= "Nie udało się usunąć przelewów %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_finanse_statements']['delete_item'] = "Usuń przelew i zaktualizuj saldo rachunku";
$aConfig['lang']['m_finanse_statements']['delete_item_q'] = "Czy na pewno usunąć wybrany przelew?";
$aConfig['lang']['m_finanse_statements']['delete_item_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_finanse_statements']['delete_item_all_q'] = "Czy na pewno usunąć wybrane przelewy?";
$aConfig['lang']['m_finanse_statements']['delete_item_all_err'] = "Nie wybrano żadnego przelewu do usunięcia!";
$aConfig['lang']['m_finanse_statements']['del_balance_ok_0']		= "Usunięto przelew %s oraz zaktualizowano balans";
$aConfig['lang']['m_finanse_statements']['del_balance_ok_1']		= "Usunięto przelewy %s oraz zaktualizowano balans";
$aConfig['lang']['m_finanse_statements']['del_balance_err']		= "Nie udało się usunąć przelewu i zaktualizować balansu %s!<br>Spróbuj ponownie";


$aConfig['lang']['m_finanse_statements']['delete'] = "Usuń przelew";
$aConfig['lang']['m_finanse_statements']['delete_q'] = "Czy na pewno usunąć wybrany przelew?";
$aConfig['lang']['m_finanse_statements']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_finanse_statements']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_finanse_statements']['delete_all_q'] = "Czy na pewno usunąć wybrane przelewy?";
$aConfig['lang']['m_finanse_statements']['delete_all_err'] = "Nie wybrano żadnego przelewu do usunięcia!";
$aConfig['lang']['m_finanse_statements']['send_button_match'] = 'Zatwierdź przelewy';
$aConfig['lang']['m_finanse_statements']['send_button_toOpek'] = 'Przenieś do Pobrania';
$aConfig['lang']['m_finanse_statements']['send_button_toApproved'] = 'Zaksięguj wybrane';
$aConfig['lang']['m_finanse_statements']['match_ok'] = 'Ustawiono status "opłacone" dla zamówień "%s"';
$aConfig['lang']['m_finanse_statements']['match_empty'] = 'Nie wybrano zamówień do ustawienia jako opłacone';
$aConfig['lang']['m_finanse_statements']['match_err'] = 'Nie udało się ustawić statusu "opłacone" dla zamówień "%s"!<br>Spróbuj ponownie';
$aConfig['lang']['m_finanse_statements']['match_err_1'] = 'Nie udało się ustawić statusu "opłacone" dla zamówień "%s" - ujemna wartość przelewu!<br>Spróbuj ponownie';
$aConfig['lang']['m_finanse_statements']['wrong_file_type'] = 'Załadowany plik musi być plikiem XML!';
$aConfig['lang']['m_finanse_statements']['add_ok'] = "Lista przelewów została zaimportowana";
$aConfig['lang']['m_finanse_statements']['add_err'] = "Nie udało się zaimportować listy przelewów!";
$aConfig['lang']['m_finanse_statements']['add_err_1'] = "Nie udało się zaimportować listy przelewów, dane w pliku XML są nieprawidłowe !";
$aConfig['lang']['m_finanse_statements']['add_err_2'] = "Nie udało się zaimportować listy przelewów, jedna lub więcej transakcji została wcześniej zaimportowana!";
$aConfig['lang']['m_finanse_statements']['saved_items'] = "Wybrane przelewy zostały przeniesione do Przelewy Pobrania";
$aConfig['lang']['m_finanse_statements']['saved_error'] = "Wystąpił błąd podczas przenoszenia do Przelewy Pobrania. Spróbuj ponownie.";

$aConfig['lang']['m_finanse_statements']['f_status'] = "Status przelewu";
$aConfig['lang']['m_finanse_statements']['f_all'] = "Wszystkie";
$aConfig['lang']['m_finanse_statements']['f_deleted'] = "Usunięty";
$aConfig['lang']['m_finanse_statements']['f_new'] = "Nierozliczony";
$aConfig['lang']['m_finanse_statements']['f_matched'] = "Rozliczony";

$aConfig['lang']['m_finanse_bank_balances']['status_-1'] = "nowe - zapo";
$aConfig['lang']['m_finanse_bank_balances']['status_0'] = "nowe";
$aConfig['lang']['m_finanse_bank_balances']['status_1'] = "w realizacji";
$aConfig['lang']['m_finanse_bank_balances']['status_2'] = "skompletowane";
$aConfig['lang']['m_finanse_bank_balances']['status_3'] = "zatwierdzone";
$aConfig['lang']['m_finanse_bank_balances']['status_4'] = "zrealizowane";
$aConfig['lang']['m_finanse_bank_balances']['status_5'] = "anulowane";
$aConfig['lang']['m_finanse_bank_balances']['do_it'] = "Zleć przelew";
$aConfig['lang']['m_finanse_bank_balances']['payu'] = "Zwrot PayU";
$aConfig['lang']['m_finanse_bank_balances']['transfer'] = "Przeksięguj";


$aConfig['lang']['m_finanse_bank_balances']['details'] = "Pobierz przelewy";
$aConfig['lang']['m_finanse_bank_renouncement'] = $aConfig['lang']['m_finanse_bank_balances'];