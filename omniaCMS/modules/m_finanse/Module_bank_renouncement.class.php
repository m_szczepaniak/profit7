<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_finanse;

use Admin;
use Common;
use DatabaseManager;
use Exception;
use FormTable;
use LIB\EntityManager\Entites\OrdersPaymentReturns;
use LIB\orders\Payment\Payment;
use LIB\orders\Payment\TransferOverpayment;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use Smarty;
use Validator;
use View;

include_once('Module_bank_balances.class.php');

/**
 * Description of Module_bank_balances
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__finanse__bank_renouncement extends Module__finanse__bank_balances implements ModuleEntity, SingleView
{

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    protected $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    private $sModule;

    protected $type = OrdersPaymentReturns::TYPE_RENOUNCEMENT;

    /**
     *
     * @param Admin $oClassRepository
     */
    public function __construct(Admin $oClassRepository)
    {
        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
        parent::__construct($oClassRepository);
    }

    /**
     * @return string
     */
    final protected function getSQLViewList()
    {

        $sSql = 'SELECT %cols
             FROM orders_payment_returns AS OPR
             JOIN orders AS O
              ON OPR.order_id = O.id AND 
              O.paid_amount > 0 AND
              O.order_status = "4"
             WHERE 
             OPR.type = "' . $this->type . '" AND 
             OPR.active = "1"
              
              %filters
             ';

        return $sSql;
    }

    final function getGroupBy() {
        return '';
    }


    /**
     * @param $orderId
     * @return mixed
     */
    private function checkIsRenouncementActive($orderId) {

        $sSql = 'SELECT OPR.id 
                 FROM orders_payment_returns AS OPR 
                 WHERE OPR.active = "1"
                  AND OPR.order_id = '.$orderId.'
                  AND OPR.type = "'.$this->type.'"';
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }


    /**
     * @return string
     */
    public function doAddRenouncementByPayU() {

        $payUSessionId = $_GET['orders_platnoscipl_sessions_id'];
        $iOrderId = $_GET['order_id'];
        if ($this->checkIsRenouncementActive($iOrderId) > 0) {
            $this->sMsg .= 'To zamówienie już ma dodany zwrot lub odstąpienie.';
            return ;
        }

        $orderData = $this->pDbMgr->getTableRow('orders', $iOrderId, ['order_number', 'paid_amount']);
        $payUData = $this->pDbMgr->getTableRow('orders_platnoscipl_sessions', $payUSessionId, ['*']);

        $paymentAmmount = -($payUData['amount']/100);

        if ($orderData['paid_amount'] < $paymentAmmount) {
            $defaultAmount = Common::FormatPrice2($orderData['paid_amount']);
        } else {
            $defaultAmount = Common::FormatPrice2($paymentAmmount);
        }

        if (!empty($_POST)) {
            $aData = $_POST;
        }

        include_once('lib/Form/FormTable.class.php');
        $oForm = new FormTable('bank_renouncement', _('Dodaj odstąpienie do zamówienia nr "' . $orderData['order_number'] . '"'));
        $oForm->AddHidden('do', 'saveRenouncementPayU');
        $oForm->AddText('amount', _('Wartość zwrotu'), $aData['amount'] > 0 ? $aData['amount'] : $defaultAmount);
        $oForm->AddHidden('order_id', $iOrderId);
        $oForm->AddHidden('orders_platnoscipl_sessions_id', $payUSessionId);


        $oForm->AddInputButton('submit', _('Zapisz'));
        return $oForm->ShowForm();
    }


    /**
     * @return string|void
     */
    public function doSaveRenouncementPayU() {
        $orderId = $_POST['order_id'];
        $payUSessionId = $_POST['orders_platnoscipl_sessions_id'];
        if ($this->checkIsRenouncementActive($orderId) > 0) {
            $this->sMsg .= 'To zamówienie już ma dodany zwrot lub odstąpienie.';
            return ;
        }

        $payUData = $this->pDbMgr->getTableRow('orders_platnoscipl_sessions', $payUSessionId, ['*']);
        $orderData = $this->pDbMgr->getTableRow('orders', $orderId, ['order_number', 'paid_amount']);
        $amount = Common::FormatPrice2($_POST['amount']);
        $paymentAmmount = -($payUData['amount']/100);


        include_once('Form/Validator.class.php');
        $oValidator = new Validator();

        if ($amount < $paymentAmmount) {
            $oValidator->sError .= '<li>'._('Wartość zwrotu przewyższa wartość płatności - '.$paymentAmmount).'</li>';
        }

        if ($amount < 0) {
            $oValidator->sError .= '<li>'._('Wartość zwrotu powinna być dodatnia '.$amount).'</li>';
        }

        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $_GET['order_id'] = $_POST['order_id'];
            $_GET['orders_platnoscipl_sessions_id'] = $_POST['orders_platnoscipl_sessions_id'];
            return $this->doAddRenouncementByPayU();
        }

        $this->addOrdersPaymentReturns($orderId, $amount, '', $_SESSION['user']['name'], '', false);
        $this->sMsg .= GetMessage(_('Dodano odstąpienie do zamówienia - '.$orderData['order_number']), false);
        return $this->doDefault();
    }

    /**
     * @return string
     */
    public function doAddRenouncementByStatement()
    {
        $statementId = $_GET['statement_id'];
        $paymentId = $_GET['payment_id'];
        $orderId = $_GET['order_id'];
        if ($this->checkIsRenouncementActive($orderId) > 0) {
            $this->sMsg .= 'To zamówienie już ma dodany zwrot lub odstąpienie.';
            return ;
        }

        $orderData = $this->pDbMgr->getTableRow('orders', $orderId, ['order_number', 'paid_amount']);
        $paymentData = $this->pDbMgr->getTableRow('orders_payments_list', $paymentId, ['*']);
        $sSql = 'SELECT name, surname, company FROM orders_users_addresses WHERE order_id = "'.$orderId.'" AND address_type = "0"';
        $ordersUsersAddresss = $this->pDbMgr->GetRow('profit24', $sSql);
        $statementNameTo = '';
        if (!empty($ordersUsersAddresss)) {
            if ('' != $ordersUsersAddresss['company']) {
                $statementNameTo = $ordersUsersAddresss['company'];
            } else {
                $statementNameTo = $ordersUsersAddresss['name'].' '.$ordersUsersAddresss['surname'];
            }
        }

        $defaultTitle = $orderData['order_number'].' Odstąpienie';

        if ($orderData['paid_amount'] < $paymentData['ammount']) {
            $defaultAmount = Common::FormatPrice2($orderData['paid_amount']);
        } else {
            $defaultAmount = Common::FormatPrice2($paymentData['ammount']);
        }

        $defaultBankAccount = $this->getAccountNumberFromTransferTitle($paymentData['title']);

        if (!empty($_POST)) {
            $aData = $_POST;
        }


        include_once('lib/Form/FormTable.class.php');
        $oForm = new FormTable('bank_renouncement', _('Dodaj odstąpienie do zamówienia nr "' . $orderData['order_number'] . '"'));
//        $oForm->AddRow(_('Płatność'), ' wartość - <strong>' . Common::FormatPrice($paymentData['ammount']) . ' zł</strong> tytuł - <strong>' . $paymentData['title'] . '</strong>');
        $oForm->AddHidden('do', 'saveRenouncement');
        $oForm->AddHidden('order_id', $orderId);
        $oForm->AddHidden('payment_id', $paymentId);
        $oForm->AddHidden('statement_id', $statementId);
        $oForm->AddText('amount', _('Wartość zwrotu'));
        $oForm->AddText('title', _('Tytuł przelewu'), $aData['title'] != '' ? $aData['title'] : $defaultTitle, ['style' => 'width: 220px;']);
        $oForm->AddText('bank_account', _('Numer konta bankowego bez spacji'), $aData['bank_account'] != '' ? $aData['bank_account'] : $defaultBankAccount, array('style' => 'width: 220px;', 'maxlength' => 26), '', 'uint', true, '/^\d{26}$/');
        $oForm->AddText('statement_name_to', _('Dane odbiorcy'), $statementNameTo != '' ? $statementNameTo : '', array('style' => 'width: 220px;', 'maxlength' => 100), '', 'text', true, '');
        $oForm->AddInputButton('submit', _('Zapisz'));
        return $oForm->ShowForm();
    }

    /**
     * @return string
     */
    public function doSaveRenouncement() {
        $statementId = $_POST['statement_id'];
        $paymentId = $_POST['payment_id'];
        $orderId = $_POST['order_id'];
        if ($this->checkIsRenouncementActive($orderId) > 0) {
            $this->sMsg .= 'To zamówienie już ma dodany zwrot lub odstąpienie.';
            return ;
        }

        $amount = Common::FormatPrice2($_POST['amount']);
        $title = $_POST['title'];
        $bankAccount = $_POST['bank_account'];
        $statementNameTo = $_POST['statement_name_to'];
        $orderData = $this->pDbMgr->getTableRow('orders', $orderId, ['order_number', 'paid_amount']);
        $paymentData = $this->pDbMgr->getTableRow('orders_payments_list', $paymentId, ['*']);


        include_once('Form/Validator.class.php');
        $oValidator = new Validator();

        /*
        if ($amount > $orderData['paid_amount']) {
            $oValidator->sError .= '<li>'._('Wartość zwrotu przewyższa wartość płatności - '.$orderData['paid_amount']).'</li>';

        }
        */

        if ($amount < 0) {
            $oValidator->sError .= '<li>'._('Wartość zwrotu powinna być dodatnia '.$amount).'</li>';
        }

        if (!$oValidator->Validate($_POST)) {
            $this->sMsg = GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            $_GET['statement_id'] = $_POST['statement_id'];
            $_GET['payment_id'] = $_POST['payment_id'];
            $_GET['order_id'] = $_POST['order_id'];
            return $this->doAddRenouncementByStatement();
        }

        // dobra można dodawać
        if ($this->addOrdersPaymentReturns($orderId, (-$amount), $bankAccount, $_SESSION['user']['name'], $title, true, $statementNameTo) === false) {
            $this->sMsg = GetMessage(_('Wystąpił błąd podczas zlecania zwrotu należności do zamówienia '.$orderData['order_number']));
        } else {
            $this->sMsg = GetMessage(_('Zlecono zwrot nadpłaty '.$amount.' do zamówienia '.$orderData['order_number']), false);
        }

        return $this->doDefault();
    }


    /**
     * @return string
     */
    public function doPayu() {
        $bIsErr = false;
        $iOrderId = $_GET['id'];
        $oprId = $_GET['OPR_ID'];

        $orderPaymentReturns = $this->pDbMgr->getTableRow('orders_payment_returns', $oprId, ['balance', 'statement_title']);

        $aOrderData = $this->getOrderSelData($iOrderId, array('balance', 'transport_number', 'order_number'));
        $this->pDbMgr->BeginTransaction('profit24');
        $oPayment = new Payment($this->pDbMgr);

        $values = [
            'active' => '0'
        ];
        if (false === $this->pDbMgr->Update('profit24', 'orders_payment_returns', $values, ' id='.$oprId)){
            $bIsErr = true;
        }

//        $oPayment->doPayBalanceOrder($iOrderId, $fPayBalance);
        $statementTitle = ($orderPaymentReturns['statement_title'] != '' ? $orderPaymentReturns['statement_title'] : $aOrderData['order_number'].' Odstąpienie');

        if ($oPayment->addPayment($iOrderId, date("Y-m-d H:i:s"), $orderPaymentReturns['balance'], $statementTitle, $aOrderData['transport_number'], 'WEWN/PAYU_OUT') === false) {
            $bIsErr = true;
        }

        if (false === $bIsErr) {
            $this->pDbMgr->CommitTransaction('profit24');
            $this->sMsg .= GetMessage(_('Opłacono zamówienie o nr '.$aOrderData['order_number'] . ' balance: '.$orderPaymentReturns['balance']), false);
        } else {
            $this->pDbMgr->RollbackTransaction('profit24');
            $this->sMsg .= GetMessage(_('Błąd opłacania zamówienia id = '.$iOrderId . ' balance: '.$orderPaymentReturns['balance']));
        }

        return $this->doDefault();
    }

    /**
     * @return array
     */
    final protected function getSQLViewCols() {

        $aCols = array('O.id', ' O.order_number', 'O.payment_type as payment', 'O.second_payment_type as second_payment', 'O.order_status',
            '"" AS `bank_details`', "(SELECT COUNT(*) from orders AS O2 where O.id <> O2.id AND O2.email = O.email and (O2.order_status = '0' OR O2.order_status = '1' OR O2.order_status = '2') ) as bank_orders",
            ' IF( OPR.statement_name_to != "", statement_name_to, (SELECT OPL.title FROM orders_payments_list AS OPL WHERE OPL.order_id = O.id ORDER BY OPL.id DESC LIMIT 1)) AS title ',
            'OPR.balance as overpayment',
            'OPR.id as OPR_ID',
            'OPR.bank_account as bank_account',
            ' CONCAT(OPR.created, " - ", OPR.created_by) AS created ',
            'OPR.statement_title',
            'OPR.is_statement_return',
            'OPR.submitted'
        );
        return $aCols;
    }

    /**
     *
     * @global type $aConfig
     * @param array $aItem
     * @return type
     */
    final public function parseViewItem(array $aItem) {
        global $aConfig;

//        if ($aItem['order_status'] != '3' && $aItem['order_status'] != '4' && $aItem['order_status'] != '5') {
//            $aItem['disabled'][] = 'payu';
//            $aItem['disabled'][] = 'do_it';
//        }
        if ($aItem['submitted'] == '1') {
            $aItem['disabled'][] = 'payu';
            $aItem['disabled'][] = 'do_it';
            $aItem['disabled'][] = 'transfer';
            $aItem['disabled'][] = 'details';
        }

        if ('1' == $aItem['is_statement_return']) {
            $aItem['disabled'][] = 'payu';
        }
        unset($aItem['is_statement_return']);
        $aItem['disabled'][] = 'do_it';
        if (!empty($aItem['statement_title'])) {
            $aItem['bank_details'] = $aItem['statement_title'];
        } else {
            $aItem['bank_details'] = $aItem['order_number'] . ' ' . ($aItem['order_status'] == '5' ? _('Anulowane') : _('Odstąpienie'));
        }

        $aItem['order_status'] = $aConfig['lang'][$this->sModule]['status_'.$aItem['order_status']].($aItem['submitted'] == '1' ? '<br /><span style="color: green; font-weight: bold;"> Przelew zlecony - pobrany do pliku</span>' : '');
        $aItem['overpayment'] = Common::formatPrice($aItem['overpayment']);
        $aItem['payment'] = $aConfig['lang']['m_zamowienia']['ptype_'.$aItem['payment']];
        if ($aItem['second_payment'] != '') {
            $aItem['second_payment'] = $aConfig['lang']['m_zamowienia']['ptype_'.$aItem['second_payment']];
        }
        if ($aItem['title'] == '') {
            $aItem['disabled'][] = 'details';
        }
        unset($aItem['submitted']);
        unset($aItem['statement_title']);
        return $aItem;
    }

    /**
     *
     * @param int $iOrderId
     * @return string
     */
    private function getTransferTitleByStatement($iStatementId)
    {


        $sSql = 'SELECT OBS.title
                 FROM orders_bank_statements AS OBS
                 WHERE id = ' . $iStatementId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }
}
