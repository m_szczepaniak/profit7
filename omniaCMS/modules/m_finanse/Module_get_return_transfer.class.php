<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-18 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_finanse;

use Admin;
use DatabaseManager;
use FormTable;
use LIB\EntityManager\Entites\OrdersPaymentReturns;
use LIB\Helpers\StringHelper;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use Smarty;

/**
 * Description of Module_get_return_transfer
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__finanse__get_return_transfer implements ModuleEntity, SingleView {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var Smarty
   */
  private $pSmarty;
  
  /**
   *
   * @var string - komunikat
   */
  private $sMsg;
  
  /**
   * @var Admin
   */
  public $oClassRepository;
  
  private $sModule;
  
  const FILEPATH = '/../files/get_return_transfer/';
  
  /**
   * 
   * @param Admin $oClassRepository
   */
  public function __construct(Admin $oClassRepository) {
    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule.$this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }
  
  /**
   * 
   * @return string
   */
  public function doDefault() {
    return $this->Show();
  }
  
  /**
   * 
   * @global array $aConfig
   * @return string
   */
  private function Show() {
    global $aConfig;

    include_once('Form/FormTable.class.php');
    $oForm = new \FormTable('get_return_transfer', _('Pobierz pliki'), array('target' => '_blank'), array());
    $oForm->AddHidden('do', 'getReturnTransferFile');
    $oForm->AddHidden('type', $_GET['type']);
    $oForm->AddInputButton('submit', _('Pobierz plik z przelewami do zwrotu'), ['onclick' => '$.LoadingOverlay(\'hide\');']);
    $oForm = $this->getManageFiles($oForm, $aConfig['common']['client_base_path'].self::FILEPATH, 'txt');
    return $oForm->ShowForm().'<script>hideOverlay = true;</script>';
  }
  
  /**
   * 
   */
  public function doGetReturnTransferFile() {
    global $aConfig;

    if (!isset($_POST['type']) || empty($_POST['type'])) {
        $this->sMsg .= GetMessage(_('Brak przekazanego typu'));
        return $this->Show();
    }

    $aToReturnTransferData = $this->getReturnTransfers($_POST['type']);
    $this->pDbMgr->BeginTransaction('profit24');
    
    if ($this->markAsSubmitted($aToReturnTransferData) === false) {
      $this->pDbMgr->RollbackTransaction('profit24');
    }
    $sTransfers = $this->getTransfersString($aToReturnTransferData);


    // $this->pDbMgr->RollbackTransaction('profit24');


    $sFileName = 'zwrot_'.date('Ymd-His').'.txt';
    $sFilePathName = $aConfig['common']['client_base_path'].self::FILEPATH.$sFileName;
    
    echo $this->getFileSourceHeaders($sFileName, $sTransfers);
    file_put_contents($sFilePathName, $sTransfers);
    $this->pDbMgr->CommitTransaction('profit24');
    exit(0);
  }
  
  /**
   * 
   * @param string $sFileName
   * @param string $sTransfers
   * @return string
   */
  private function getFileSourceHeaders($sFileName, $sTransfers) {
    
    $_GET['hideHeader'] = 1;
    header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$sFileName.'"');
		header("Pragma: no-cache");
		header("Expires: 0");
    return $sTransfers;
  }
  
  
  /**
   * 
   * @param array $aToReturnTransferData
   * @return string
   */
  private function getTransfersString($aToReturnTransferData) {
    
    $sTransfers = '';
    foreach ($aToReturnTransferData as $aTransferData) {
      // rachunek bankowy odpowiedni do przelewów odpowiednio z księgarnii,
      // dane osoby odbierajacej z przelewu przychodzącego
      $sToName = $this->replaceBankAccount($aTransferData['title']);
      $fAmount = ($aTransferData['balance'] * -1);
      $sFromAccount = $this->getFromBankAccount($aTransferData['website_id']);
      $sTransfers .= $this->getTransferRow($sFromAccount, $aTransferData['bank_account'], $sToName, $fAmount, $aTransferData['order_status'], $aTransferData['order_number'], $aTransferData['statement_title']);
    }
    return $sTransfers;
  }
  
  /**
   * 
   * @param int $iWebsiteId
   * @return int
   */
  public function getFromBankAccount($iWebsiteId) {
    
    $sSql = 'SELECT BA.number
             FROM bank_accounts AS BA 
             JOIN orders_payment_types AS OPT
             ON BA.orders_payment_types_id = OPT.id AND 
                OPT.ptype = "bank_transfer" AND OPT.transport_mean = "1"
             WHERE BA.websites_id = '.$iWebsiteId.'
             LIMIT 1
             ';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
  
  /**
   * 
   * @param string $sTitle
   * @return string
   */
  public function replaceBankAccount($sTitle) {
    return preg_replace('/(\s)(\d{26}\s)/', '$1', $sTitle);
  }

  /**
   * 
   * @return array
   */
  private function getReturnTransfers($type) {

    $sSql = 'SELECT DISTINCT OPR.id, O.order_number, O.order_status, OPR.balance, OPR.bank_account, IF(OPR.statement_name_to != "", OPR.statement_name_to, OPL.title) as title, O.website_id, OPR.statement_title
             FROM orders_payment_returns AS OPR
             JOIN orders AS O
              ON O.id = OPR.order_id
             JOIN orders_payments_list AS OPL
              ON OPL.order_id = O.id
             WHERE OPR.submitted = "0" AND 
                   OPR.bank_account IS NOT NULL AND 
                   OPR.bank_account <> "" AND 
                   OPR.type = "'.$type.'" AND
                   OPR.is_statement_return  = "1" AND 
                   OPR.active = "1"
             GROUP BY OPR.id
             LIMIT 20
              ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  
  /**
   * 
   * @param array $aToReturnTransferData
   * @return bool
   */
  private function markAsSubmitted($aToReturnTransferData) {
    $aValues = array(
        'submitted' => '1'
    );
    foreach ($aToReturnTransferData as $aTransfer) {
      if ($this->pDbMgr->Update('profit24', 'orders_payment_returns', $aValues, ' id = '.$aTransfer['id']) === false) {
        return false;
      }
    }
    return true;
  }


    /**
     *
     * @param string $sFromAccount
     * @param string $sToAccount
     * @param string $sToName
     * @param float $fAmount
     * @param $iOrderStatus
     * @param string $sOrderNumber
     * @param string $statementTitle
     * @return string
     */
  public function getTransferRow($sFromAccount, $sToAccount, $sToName, $fAmount, $iOrderStatus, $sOrderNumber, $statementTitle = "") {
    
$sStr = '0
11
1
1
2
'.$this->getLast10FromAccount($sFromAccount).'
3
'.$this->getNumberOfBank3To10($sToAccount).'
4
'.$sToAccount.'
5

6
'.$this->toString($this->getNameTo35In3Line($sToName)).'
7
'.$fAmount.'
8
'.$this->getNowdmy().'
9
'.$this->GetPaymentDetails34In4Lines($iOrderStatus, $sOrderNumber, $statementTitle).'
-1
';
    return $sStr;
  }
  
  /**
   * 
   * @param string $sOrderNumber
   * @return string
   */
  private function GetPaymentDetails34In4Lines($iOrderStatus, $sOrderNumber, $statementTitle = "") {
    if ($statementTitle == '') {
        $statementTitle = $sOrderNumber . ' ' . ($iOrderStatus == '5' ? _('Anulowane') : _('Nadpłata'));
    }

    return $this->toString($this->getNameTo34In4Line($statementTitle));
  }


    /**
     * @test
     * @param string $sToName
     * @return string
     */
    public function getNameTo34In4Line($sToName) {
        // wywalamy wszystko poza literami i spacjami
        $sToName = $this->replaceNotWordNumericSpace($sToName);
        $iMax = 0;
        $aReturn = array();
        while($iMax < 4) {
            $iMax++;
            $aReturn[] = mb_substr($sToName, 0, 34, 'UTF-8');
            $sToName = mb_substr($sToName, 34, mb_strlen($sToName, 'UTF-8'), 'UTF-8');
        }
        return $aReturn;
    }

    /**
     *
     * @param string $sToName
     * @return string
     */
    private function replaceNotWordNumericSpace($sToName) {
        return  preg_replace('/[^a-zA-Z\s\d]/', '', $sToName);
    }

  /**
   * 
   * @return data
   */
  private function getNowdmy() {
    return date("d/m/y");
  }
  
  
  /**
   * 
   * @param array $aNames
   * @return string
   */
  private function toString($aNames) {
    return implode("\r\n", $aNames);
  }
  
  /**
   * 
   * @param string $sToName
   * @return string
   */
  private function replaceNotWordSpace($sToName) {
    $sToName = StringHelper::specialCharsReplace($sToName);
    return  preg_replace('/[^a-zA-Z\s]/', '', $sToName);
  }
  
  /**
   * @test
   * @param string $sToName
   * @return string
   */
  public function getNameTo35In3Line($sToName) {
    // wywalamy wszystko poza literami i spacjami
    $sToName = $this->replaceNotWordSpace($sToName);
    $iMax = 0;
    $aReturn = array();
    while($iMax < 3) {
      $iMax++;
      $aReturn[] = mb_substr($sToName, 0, 35, 'UTF-8');
      $sToName = mb_substr($sToName, 35, mb_strlen($sToName, 'UTF-8'), 'UTF-8');
    }
    return $aReturn;
  }
  
  
  /**
   * 
   * @param string $sToAccount
   * @return string
   */
  private function getNumberOfBank3To10($sToAccount) {
    return substr($sToAccount, 2, 8);
  }
  
  /**
   * 
   * @param string $sFromAccount
   * @return string
   */
  private function getLast10FromAccount($sFromAccount) {
    return substr($sFromAccount, -10);
  }
  
  
  /**
   * 
   * @global array $aConfig
   */
  public function doGetFile() {
    global $aConfig;
    
    $_GET['hideHeader'] = 1;
    $sFileName = rawurldecode($_GET['file_name']);
    
    header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="'.$sFileName.'"');
		header("Pragma: no-cache");
		header("Expires: 0");
    
    echo file_get_contents($aConfig['common']['client_base_path'].self::FILEPATH.$sFileName);
    exit(0);
  }
  
  /**
   * 
   * @return string
   */
  public function doDeleteFile() {
    global $aConfig;
    
    $_GET['hideHeader'] = 1;
    $sFileName = rawurldecode($_GET['file_name']);
    unlink($aConfig['common']['client_base_path'].self::FILEPATH.$sFileName);
    $this->sMsg .= GetMessage(sprintf(_('Plik %s został usunięty '), $sFileName));
    return $this->Show();
  }
  
  /**
   * 
   * @param FormTable $oForm
   * @param string $sFilePath
   * @param string $sExt
   * @return FormTable
   */
  private function getManageFiles($oForm, $sFilePath, $sExt) {
    
    $aFiles = glob($sFilePath.'/*.'.$sExt);
    $aFiles = array_reverse($aFiles);
    foreach ($aFiles as $sFilenamePath) {
      $sFileName = array_pop(explode('/', $sFilenamePath));

      $sURL = phpSelf(array('do'=>'getFile', 'file_name' => rawurldecode($sFileName)));
      $sDelete = phpSelf(array('do'=>'deleteFile', 'file_name' => rawurldecode($sFileName)));
      $oForm->AddRow(sprintf(_(' %s '), $sFileName), '<a href="'.$sURL.'">'._('Pobierz').'</a> &nbsp; &nbsp;'. '<a href="'.$sDelete.'">Usuń</a>');
    }
    return $oForm;
  }

  public function getMsg() {
    return $this->sMsg;
  }

  public function parseViewItem(array $aItem) {
  }

  public function resetViewState() {
  }
}
