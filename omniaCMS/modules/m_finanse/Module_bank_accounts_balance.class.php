<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-20 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_finanse;

use Admin;
use DatabaseManager;
use LIB\bank_accounts\Balance;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;

/**
 * Description of Module_bank_accounts_balance
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__finanse__bank_accounts_balance extends Balance implements ModuleEntity {

  /**
   *
   * @var string
   */
	private $sMsg;

  /**
   *
   * @var string
   */
	private $sModule;

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;
	
  public function __construct(Admin $oClassRepository) {
    
    $this->oClassRepository = $oClassRepository;
    $this->em = $this->oClassRepository->entityManager;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    parent::__construct($this->pDbMgr);
  }

  /**
   * 
   * @return string - HTML
   */
  public function doDefault() {
    
    $aBankBalancesDays = $this->getBankBalancesDays();
    return $this->parseViewHTML('bank_accounts_balances', $aBankBalancesDays);
  }
  
  /**
   * 
   * @param string $sTPLFileName
   * @param array $aBankBalancesDays
   * @return string - HTML
   */
  private function parseViewHTML($sTPLFileName, $aBankBalancesDays) {
    
    $this->pSmarty->assign('bankBalancesDays', $aBankBalancesDays);
    $sHtml = $this->pSmarty->fetch('finanse/'.$sTPLFileName.'.tpl');
    $this->pSmarty->clear_assign('bankBalancesDays');
    return $sHtml;
  }
  
  /**
   * 
   * @return array
   */
  private function getBankBalancesDays() {
    $aBankBalances = $this->getBankAccountsBalances();
    $aStatements = $this->getStatements(31);
    $aBankBalancesDays = $this->linkBankBalanceAndStatement($aBankBalances, $aStatements);
    return $this->getBalancePerDay($aBankBalancesDays);
  }
  
  /**
   * 
   * @param array $aBankBalancesDays
   * @return array
   */
  private function getBalancePerDay($aBankBalancesDays) {
    
    foreach ($aBankBalancesDays as $sIdent => $aBalance) {
      $fCurrentBalance = $aBalance['balance'];
      if (isset($aBalance['days_transfers'])) {
        foreach ($aBalance['days_transfers'] as $iKey => $aDayTransfer) {
          $aBankBalancesDays[$sIdent]['days_transfers'][$iKey]['end_balance'] = $fCurrentBalance;
          $fCurrentBalance = $fCurrentBalance - $aDayTransfer['paid_amount'];
          $aBankBalancesDays[$sIdent]['days_transfers'][$iKey]['start_balance'] = $fCurrentBalance;
        }
      }
    }
    return $aBankBalancesDays;
  }

  public function getMsg() {
    
  }

  public function resetViewState() {
    
  }

}
