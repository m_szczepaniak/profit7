<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_finanse;

use Admin;
use Common;
use DatabaseManager;
use Exception;
use FormTable;
use LIB\EntityManager\Entites\OrdersPaymentReturns;
use LIB\orders\Payment\Payment;
use LIB\orders\Payment\TransferOverpayment;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;
use Smarty;
use Validator;
use View;

/**
 * Description of Module_bank_balances
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__finanse__bank_balances implements ModuleEntity, SingleView {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var Smarty
   */
  private $pSmarty;
  
  /**
   *
   * @var string - komunikat
   */
    protected  $sMsg;
  
  /**
   * @var Admin
   */
  public $oClassRepository;
  
  private $sModule;

  protected $type = OrdersPaymentReturns::TYPE_RETURN;
  
  /**
   * 
   * @param Admin $oClassRepository
   */
  public function __construct(Admin $oClassRepository) {
    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule.$this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }
  
  /**
   * 
   * @return string
   */
  public function doDefault() {
    return $this->Show();
  }
  
  /**
   * 
   * @return html
   */
  public function doSaveTransfer() {
    
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Show();
			return;
		}
    $iOrderFromTransfer = $_POST['current_order_id'];
    $iOrderToTransferId = $this->getRegExpOrder($_POST['order']);
    
    if ($iOrderToTransferId > 0 && $iOrderFromTransfer > 0) {
      if ($iOrderFromTransfer != $iOrderToTransferId) {
        $this->pDbMgr->BeginTransaction('profit24');
        $oTransferOverpayment = new TransferOverpayment($this->pDbMgr);
        try {
          $sMsg = '';
          $oTransferOverpayment->doTransferOverpaymentSetMsg($sMsg, $iOrderFromTransfer, $iOrderToTransferId);
          $this->sMsg = GetMessage($sMsg, false);
          $this->pDbMgr->CommitTransaction('profit24');
        } catch (Exception $ex) {
          $this->pDbMgr->RollbackTransaction('profit24');
          $this->sMsg .= GetMessage($ex);
          return $this->Show();
        }
      } else {
        $this->sMsg .= GetMessage(_('Błąd wybrane zemówienia to te same zamówienia'));
      }
    } else {
      $this->sMsg .= GetMessage(_('Błąd wybierania zamówień 2, spróbuj ponownie.'));
    }
    return $this->Show();
  }
  
  
  /**
   * 
   * @param string $sOrderStr
   * @return boolean|int
   */
  private function getRegExpOrder($sOrderStr) {
    
    $aMatches = array();
    preg_match('/\((\d+)\)/', $sOrderStr, $aMatches);
    if (isset($aMatches[1])) {
      return $aMatches[1];
    }
    return false;
  }
  
  /**
   * 
   * @return html
   */
  public function doTransfer() {
    global $aConfig;
    
    $iOrderId = $_GET['id'];
    $aOrderData = $this->getOrderSelData($iOrderId, array('balance', 'email', 'order_number'));
    
    include_once('lib/Form/FormTable.class.php');
    $pForm = new FormTable('overpayment_transfer', _('Przeksięgowanie nadpłaty do zamówienia'));
    $pForm->AddRow(_('Przeksiegowanie nadpłaty z zamówienia: '), $aOrderData['order_number']);
    $pForm->AddRow(_('Email: '), $aOrderData['email']);
    $pForm->AddRow(_('Nadpłata: '), Common::formatPrice($aOrderData['balance']));
    $pForm->AddHidden('current_order_id', $iOrderId);
    
    $pForm->AddHidden('do', 'saveTransfer');
    $pForm->AddText('order', _('Wybierz zamówienie<br />wprowadź numer lub email lub wartość niedopłaty'), '', array('id' => 'order', 'style' => 'font-size: 14px; width: 400px;'), '', 'text', true, '/\((\d+)\)$/');
    $pForm->AddRow('&nbsp;', 
            $pForm->GetInputButtonHTML('send', _('Zapisz')).'&nbsp;&nbsp;'.
            $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], 
                    array('onclick'=>'MenuNavigate(\''.phpSelf().'\');'), 'button'));
    
    return ShowTable($pForm->ShowForm()).$this->getOrdersTransferJs();
  }
  
  /**
   * 
   * @return string
   */
  public function doDo_It() {
    $iOrderId = $_GET['id'];
    $aOrderData = $this->getOrderSelData($iOrderId, array('balance', 'transport_number', 'order_number'));
    $fBalance = -$aOrderData['balance'];
    $sAccount = $this->getAccountNumberFromTransferTitle($this->getTransferTitle($iOrderId));
    
    if ($this->addOrdersPaymentReturns($iOrderId, $fBalance, $sAccount, $_SESSION['user']['name'], '', true) === false) {
      $this->sMsg = GetMessage(_('Wystąpił błąd podczas zlecania zwrotu należności do zamówienia '.$aOrderData['order_number']));
    } else {
      $this->sMsg = GetMessage(_('Zlecono zwrot nadpłaty '.$fBalance.' do zamówienia '.$aOrderData['order_number']), false);
    }
    return $this->Show();
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return string
   */
  private function getTransferTitle($iOrderId) {
    
    $sSql = '
      SELECT OPL.title 
      FROM orders_payments_list AS OPL 
      WHERE OPL.order_id = '.$iOrderId.'
      ORDER BY OPL.id DESC LIMIT 1';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
  
  /**
   * 
   * @param string $sTransferTitle
   * @return string
   */
  protected function getAccountNumberFromTransferTitle($sTransferTitle) {
    
    preg_match('/\s(\d{26})\s/', $sTransferTitle, $aMatches);
    return $aMatches[1];
  }

    /**
     *
     * @param int $iOrderId
     * @param float $fBalance
     * @param string $sAccount
     * @param string $sCreatedBy
     * @param string $statementTitle
     * @param bool $isStatementReturn
     * @param string $statementNameTo
     * @return bool
     */
  protected function addOrdersPaymentReturns($iOrderId, $fBalance, $sAccount, $sCreatedBy, $statementTitle = '', $isStatementReturn = true, $statementNameTo = '') {
    
    $aValues = array(
        'order_id' => $iOrderId,
        'balance' => $fBalance,
        'submitted' => '0',
        'bank_account' => $sAccount,
        'created' => 'NOW()',
        'created_by' => $sCreatedBy,
        'type' => $this->type,
        'statement_title' => $statementTitle,
        'is_statement_return' => $isStatementReturn == true ? '1' : '0',
        'statement_name_to' => $statementNameTo
    );
    return $this->pDbMgr->Insert('profit24', 'orders_payment_returns', $aValues);
  }

  /**
   * 
   */
  public function doPayu() {
    
    $iOrderId = $_GET['id'];
    $aOrderData = $this->getOrderSelData($iOrderId, array('balance', 'transport_number', 'order_number'));
    $fPayBalance = -$aOrderData['balance'];
    $this->pDbMgr->BeginTransaction('profit24');
    $oPayment = new Payment($this->pDbMgr);
    $oPayment->doPayBalanceOrder($iOrderId, $fPayBalance);
    if ($oPayment->addPayment($iOrderId, date("Y-m-d H:i:s"), $fPayBalance, $aOrderData['order_number'].' Zwrot', $aOrderData['transport_number'], 'WEWN/PAYU_OUT') === false) {
      $this->pDbMgr->RollbackTransaction('profit24');
      $this->sMsg .= GetMessage(_('Błąd opłacania zamówienia id = '.$iOrderId . ' balance: '.$fPayBalance));
    } else {
      $this->pDbMgr->CommitTransaction('profit24');
      $this->sMsg .= GetMessage(_('Opłacono zamówienie o nr '.$aOrderData['order_number'] . ' balance: '.$fPayBalance), false);
    }
    
    return $this->doDefault();
  }
  
  /**
   * 
   * @param int $iId
   * @param array $aCols
   * @return array
   */
  protected function getOrderSelData($iId, array $aCols) {
    
    $sSql = 'SELECT '.implode(', ', $aCols).'
             FROM orders
             WHERE id = '.$iId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
  
  /**
   * 
   * @return string
   */
  public function getMsg() {
    return $this->sMsg;
  }
  
  /**
   * 
   */
  public function Show() {
    global $aConfig;
    $aData = array();
    
    // fuck langi zamowien, do nazw metod płatności
    include_once('modules/m_zamowienia/lang/admin_pl.php');
    
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
    
		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> _('Nadpłaty'),
			'refresh'	=> true,
			'search'	=> true,
      'items_per_page' => 20,
      'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
    $aRecordsHeader = array(
			array(
				'db_field'	=> 'number',
				'content'	=> _('Numer'),
				'sortable'	=> true,
				'width' => '100'
			),
      array(
				'db_field'	=> 'payment',
				'content'	=> _('Płatność'),
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'second_payment',
				'content'	=> _('Druga płatność'),
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'order_status',
				'content'	=> _('Status'),
				'sortable'	=> true,
				'width' => '150'
			),
			array(
				'db_field'	=> 'title',
				'content'	=> _('Tytuł przelewu'),
				'sortable'	=> true,
        'width' => '180'
			),
      array(
				'db_field'	=> 'bank_details',
				'content'	=> _('Dane do przelewu'),
				'sortable'	=> true,
			),
      array(
				'db_field'	=> 'overpayment',
				'content'	=> _('Nadpłata'),
				'sortable'	=> true,
				'width' => '50'
			),
      array(
				'db_field'	=> 'created ',
				'content'	=> _('Zlecono - Zlecił'),
				'sortable'	=> true,
				'width' => '150'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
    $aColSettings = array(
      'id'	=> array(
        'show'	=> false
      ),
      'OPR_ID'	=> array(
        'show'	=> false
      ),
      'bank_account' => array(
        'show'	=> false
      ),
      'bank_orders' => array(
        'show'	=> false
      ),
      'action' => array (
        'actions'	=> array ('do_it', 'payu', 'details'),
        'params' => array (
                      'do_it'	=> array('id' => '{id}', 'OPR_ID' => '{OPR_ID}'),
                      'payu' => array('id' => '{id}', 'OPR_ID' => '{OPR_ID}'),
//                      'transfer' => array('id' => '{id}', 'OPR_ID' => '{OPR_ID}'),
                      'details' => array('OPR_ID' => '{OPR_ID}', 'do' => 'EditAccount'),
                    ),
        'show' => false,
        'icon' => array(
            'payu' => 'tarrifs',
            'do_it' => 'connect_to',
            'transfer' => 'back'
        )
      )
    );
      
    include_once('View/View.class.php');
    $oView = new View(_('overpayments'), $aHeader, $aAttribs);
    
    $aCols = $this->getSQLViewCols();
    $sSql = $this->getSQLViewList();

    $aSearchCols = array('O.order_number');
    $oView->setSqlListView($this, $sSql, $aCols, $aSearchCols, '', $this->getGroupBy(), ' O.id DESC ');
    $oView->AddFilter('f_O|payment_type', _('Pierwsza pł.'), addDefaultValue($this->_getPaymentTypes()), isset($aData['f_O|payment_type']) ? $aData['f_O|payment_type'] : '');
    $oView->AddFilter('f_O|second_payment_type', _('Druga pł.'), addDefaultValue($this->_getPaymentTypes()), isset($aData['f_O|second_payment_type']) ? $aData['f_O|second_payment_type'] : '');

    foreach ($oView->aRecords as $iKey => $aRecord) {
      if ($aRecord['bank_account'] != '') {
        $oView->aRecords[$iKey]['title'] = 'WŁAŚCIWE KONTO : 
                                          <span style="font-weight:bold; color: red;">'.$aRecord['bank_account'].'</span>
                                            <br />'.$aRecord['title'];
      }

      if ($oView->aRecords[$iKey]['bank_orders'] > 0){
        $oView->aRecords[$iKey]['bank_details'] .= '<img src="gfx/msg_1.gif" />';
      }
    }

    $oView->AddRecordsHeader($aRecordsHeader);
    $oView->setActionColSettings($aColSettings);
    
    $aRecordsFooter = array(
      array(),
            array('details')
    );
    $aRecordsFooterParams = array(
        'details' => array(array(
            'action' => 'get_return_transfer',
            'do' => 'Default',
            'type' => $this->type
        ))
    );
		$oView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
    return ShowTable($oView->Show());
  }


  protected function getGroupBy() {
    return 'GROUP BY O.id';
  }

    /**
     * @return array
     */
  protected function getSQLViewCols() {

      $aCols = array('O.id', ' O.order_number', 'O.payment_type as payment', 'O.second_payment_type as second_payment', 'O.order_status',
          '"" AS `bank_details`', "(SELECT COUNT(*) from orders AS O2 where O.id <> O2.id AND O2.email = O.email and (O2.order_status = '0' OR O2.order_status = '1' OR O2.order_status = '2') ) as bank_orders",
          ' (SELECT OPL.title FROM orders_payments_list AS OPL WHERE OPL.order_id = O.id ORDER BY OPL.id DESC LIMIT 1) AS title ',
          'O.balance as overpayment',
          'OPR.id as OPR_ID',
          'OPR.bank_account as bank_account',
          ' CONCAT(OPR.created, " - ", OPR.created_by) AS created ');
      return $aCols;
  }

    /**
     * @return string
     */
  protected function getSQLViewList() {

      $sSql = 'SELECT %cols
             FROM orders AS O
             LEFT JOIN orders_payment_returns AS OPR
              ON OPR.order_id = O.id AND OPR.type = "'.$this->type.'" AND OPR.active = "1"
             WHERE 
              O.balance > 0
              %filters
             ';

      return $sSql;
  }
  
  /**
   * 
   * @global array $aConfig
   * @return array
   */
  private function _getPaymentTypes() {
    global $aConfig;
    $aOptions = array();
    
    $sSql = 'SELECT ptype FROM orders_payment_types GROUP BY ptype';
    $aPaymentTypes = $this->pDbMgr->GetCol('profit24', $sSql);
    foreach ($aPaymentTypes as $iKey => $sPType) {
      $aOptions[$iKey]['label'] = $aConfig['lang']['m_zamowienia']['ptype_'.$sPType];
      $aOptions[$iKey]['value'] = $sPType;
    }
    return $aOptions;
  }
  
  /**
   * 
   * @return string
   */
  public function doSaveAccount() {
    $iId = $_GET['OPR_ID'];
    $sAccount = $_POST['bank_account'];
    
    $aValues = array('bank_account' => $sAccount);
    
    if ($this->pDbMgr->Update('profit24', 'orders_payment_returns', $aValues, ' id = '.$iId. ' LIMIT 1') === false) {
      $this->sMsg = GetMessage(_('Wystąpił błąd podczas ustawiania konta bankowego do zwrotu id ').$iId.' '.print_r($aValues, true));
    } else {
      $this->sMsg = GetMessage(_('Ustawiono numer konta bankowego'), false);
    }
    return $this->doDefault();
  }
  
  
  /**
   * 
   * @return string
   */
  public function doEditAccount() {
    $iId = $_GET['OPR_ID'];
    $aData = $this->pDbMgr->getTableRow('orders_payment_returns', $iId, array('bank_account'));
            
    include_once('Form/FormTable.class.php');
    $oForm = new \FormTable('edit_account', _('Numer konta bankowego'), array('action' => phpSelf(array('OPR_ID' => $iId))));
    $oForm->AddHidden('do', 'saveAccount');
    $oForm->AddText('bank_account', _('Numer konta bankowego bez spacji'), $aData['bank_account'], array('style' => 'width: 220px;', 'maxlength' => 26), '', 'uint', true, '/^\d{26}$/');
    $oForm->AddInputButton('submit', _('Zapisz'));
    return $oForm->ShowForm();
  }

  /**
   * 
   * @global type $aConfig
   * @param array $aItem
   * @return type
   */
  public function parseViewItem(array $aItem) {
    global $aConfig;
    
    if ($aItem['order_status'] != '3' && $aItem['order_status'] != '4' && $aItem['order_status'] != '5') {
      $aItem['disabled'][] = 'payu';
      $aItem['disabled'][] = 'do_it';
    }
    if ($aItem['created'] != '') {
      $aItem['disabled'][] = 'payu';
      $aItem['disabled'][] = 'do_it';
      $aItem['disabled'][] = 'transfer';
    }
    $aItem['bank_details'] = $aItem['order_number'] . ' ' . ($aItem['order_status'] == '5' ? _('Anulowane') : _('Nadpłata'));
    $aItem['order_status'] = $aConfig['lang'][$this->sModule]['status_'.$aItem['order_status']];
    $aItem['overpayment'] = Common::formatPrice($aItem['overpayment']);
    $aItem['payment'] = $aConfig['lang']['m_zamowienia']['ptype_'.$aItem['payment']];
    if ($aItem['second_payment'] != '') {
      $aItem['second_payment'] = $aConfig['lang']['m_zamowienia']['ptype_'.$aItem['second_payment']];
    }
    if ($aItem['title'] == '') {
      $aItem['disabled'][] = 'details';
    }
    return $aItem;
  }
  
  /**
   * 
   */
  public function resetViewState() {
    //resetViewState($this->sModule);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @global array $aConfig
   * @return string
   */
  private function getOrdersTransferJs() {
    global $aConfig;
    
    $sJs = '
<script>
    var iCurrentOrderId = $("#current_order_id").val();
    $("#order").autocomplete({
      source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetOrders.php?current_order_id="+iCurrentOrderId+"&mode=default",
      select: function( event, ui ) {
        $("#order").attr("readonly", "readonly").css("background-color", "#CACACA");
      }
    });
</script>
';
    return $sJs;
  }
}
