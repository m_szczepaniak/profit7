<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-26 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_finanse;

use Admin;
use DatabaseManager;
use FormTable;
use LIB\orders\Payment\Payment;
use omniaCMS\lib\interfaces\ModuleEntity;
use Smarty;

/**
 * Description of Module_import_payments
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__finanse__import_payments implements ModuleEntity {

  /**
   *
   * @var string
   */
	private $sMsg;

  /**
   *
   * @var string
   */
	private $sModule;

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;
  
  /**
   *
   * @var string
   */
  private $sFileSavePath = 'LIB/communicator/sources/Internal/streamsoft/csv/';
	
  /**
   * 
   * @global array $aConfig
   * @param Admin $oClassRepository
   */
  public function __construct(Admin $oClassRepository) {
    global $aConfig;
    
    $this->oClassRepository = $oClassRepository;
    $this->em = $this->oClassRepository->entityManager;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    $this->sFullFileSavePath = $aConfig['common']['client_base_path'].'/'.$this->sFileSavePath;
  }

  /**
   * 
   * @global array $aConfig
   * @return string
   */
  public function doDefault() {
    global $aConfig;
    
    include_once('Form/FormTable.class.php');
    $oForm = new FormTable('get_payments_form', _('Pobierz plik płatności do StreamSoft'));
    $oForm->AddHidden('do', 'getPayments');
    $oForm->AddInputButton('get_payments', _('Generuj plik płatności do StreamSoft'));
    $oForm->AddText('date', _('Przelewy z dnia'), '00-00-0000', array('style' => 'width: 90px;'), '', 'date', FALSE);
    
    $oForm->AddMergedRow(_('Pliki płatności do StreamSoft'), array('class'=>'merged'));

    $aFiles = glob($this->sFullFileSavePath.'/*.csv');
    $aFiles = array_reverse($aFiles);
    foreach ($aFiles as $sFilenamePath) {
      $sFilename = str_replace($aConfig['common']['client_base_path'].'/', '', $sFilenamePath);
      $sURLFile = $aConfig['common']['client_base_url_https'].$sFilename;

      $oForm->AddRow(sprintf(_(' %s '), str_replace($this->sFileSavePath.'/', '', $sFilename)), '<a href="'.$sURLFile.'">'._('Pobierz').'</a>');
    }
    return ShowTable($oForm->ShowForm());
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return boolean
   */
  public function markAsExported($iOrderId) {
    $aValues = array(
        'erp_export' => "NOW()"
    );
    if ($this->pDbMgr->Update('profit24', 'orders_bank_statements', $aValues, ' id = '.$iOrderId) === false) {
      return false;
    } else {
      return true;
    }
  }
  
  /**
   * Wysyłamy dokument, zapisujemy odpowiednio w folderze
   * 
   */
  public function doGetPayments() {
    $bIsErr = false;
    $fData = NULL;

    if ($_POST['date'] != '') {
      $fData = formatDate($_POST['date'], 0);
    }
    
    $oPayment = new Payment($this->pDbMgr);
    $aPaymentData = $oPayment->getPaymentsToERP($fData);
    $aStatements = $this->getStatementsApproved($fData);
    
    $fp = fopen($this->sFullFileSavePath.date("Ymd_His").'.csv', 'w');
    
    $this->pDbMgr->BeginTransaction('profit24');
    
    foreach ($aPaymentData as $iKey => $aItem) {
      if ($oPayment->markAsExported($aItem['id']) === FALSE) {
        $bIsErr = true;
      }
      unset($aPaymentData[$iKey]['id']);
    }
    
    foreach ($aStatements as $iKey => $aItem) {
      if ($this->markAsExported($aItem['id']) === FALSE) {
        $bIsErr = true;
      }
      unset($aStatements[$iKey]['id']);
    }
    
    foreach ($aStatements as $fields) {
        fputcsv($fp, $fields, ';');
    }
    
    foreach ($aPaymentData as $fields) {
        fputcsv($fp, $fields, ';');
    }
    
    fclose($fp);
    if ($bIsErr === FALSE) {
      $this->pDbMgr->CommitTransaction('profit24');
      $this->sMsg = GetMessage(_('Plik został wygenerowany pomyślnie'), false);
    } else {
      $this->pDbMgr->RollbackTransaction('profit24');
      $this->sMsg = GetMessage(_('Błąd generowania pliku, spróbuj ponownie'), false);
    }
    
    return $this->doDefault();
  }
  
  /**
   * 
   * @param data $fData
   * @return array
   */
  public function getStatementsApproved($fData) {
    
    $iPayUStramsoftId = 6016;
    $sSql = '
      SELECT 
        BA.streamsoft_id, 
        DATE(OBS.created) as c_date, 
        IF(OBS.paid_amount > 0.00, "BP_KROZ", "BW_KROZ2") as `type`, 
        IF (BA.streamsoft_id = "03", "'.$iPayUStramsoftId.'", "0") AS kontrahent, 
        REPLACE(IF(OBS.paid_amount > 0, OBS.paid_amount, 0), ".", ",") as wplata, 
        REPLACE(IF(OBS.paid_amount <= 0, OBS.paid_amount, 0)*(-1), ".", ",") as wyplata,
        IF(OBS.order_nr <> "", OBS.order_nr, ".") as order_number,
        OBS.id
      FROM orders_bank_statements AS OBS
      JOIN bank_accounts AS BA
      ON BA.ident_statments_file = OBS.ident_statments_file
      WHERE 
        approved = "1" AND 
        erp_export IS NULL
        '
          .($fData != '' ? ' AND DATE(OBS.created) = "'.$fData.'"' : '').
        '
      GROUP BY OBS.id
    ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

  /**
   * 
   * @return string
   */
  public function getMsg() {
    return $this->sMsg;
  }
}
