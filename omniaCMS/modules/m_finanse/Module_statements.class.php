<?php

use LIB\bank_accounts\Balance;
use LIB\EntityManager\Entites\OrdersPaymentReturns;
use LIB\orders\Payment\MatchStatementToOrder;

/**
 * Klasa Module do obslugi modulu 'zamowienia - import wyciagow z kont'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */
class Module__finanse__statements {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// wartośc określa wersję importowane XML
	// 0 - nie zdefiniowano wersji
	// 1 - wersja podstawowa aktualnie importowana
	// 2 - wersja która jest z czasu kiedy mechanizm nie istniał
    // 3 - najnowsza wersja z 21.07.2017
	var $iVersionXML = 0;

    protected $ordersPayed;

    /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  private $pSmarty;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	public function __construct(&$pSmarty) {
		global $aConfig, $pDbMgr;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
    $this->pSmarty = $pSmarty;
    $this->pDbMgr = $pDbMgr;

		$sDo = '';
		$iId = 0;
		$iPId = 0;
		$iCId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
		if (isset($_GET['cid'])) {
			$iCId = $_GET['cid'];
		}
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];

	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iId); break;
            case 'delete_item': $this->DeleteStatementUpdateBalance($pSmarty, $iPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'add': $this->Add($pSmarty, $iPId, $iId); break;
			case 'match': $this->matchOrders($pSmarty); break;
      case 'match_multiple_orders': $this->matchMultipleOrders($pSmarty, $iId); break;
			case 'toOpek': $this->toOpek($pSmarty); break;
      case 'toApproved': $this->toApproved($pSmarty); break;
      case 'details': $this->Details($pSmarty, $iId); break;
            case 'auto_match': $this->tryAutoMatchStatement($pSmarty, $iId); break;
			default: $this->Show($pSmarty); break;
		}
	} // end of Module() method


  /**
   *
   * @return void
   */
  public function doDefault() {
    return $this->Show($this->pSmarty);
  }


  /**
   *
   * @param Smarty $pSmarty
   * @param int $iStatmentId
   */
  private function matchMultipleOrders(&$pSmarty, $iStatmentId) {
    $sNumbers = '';
    Common::BeginTransaction();
    $aStatement = $this->getStatement($iStatmentId);
    $fPaidAmount = $aStatement['paid_amount'];
    foreach ($_POST['orders'] as $iPKey => $iOrderId) {
        if (intval($iOrderId) > 0) {
          if ($fPaidAmount > 0) {
          $aOrder = $this->pDbMgr->getTableRow('orders', $iOrderId, array('to_pay', 'paid_amount', 'order_number'));
          $sNumbers .= $aOrder['order_number'].', ';
          $fToPay = ($aOrder['to_pay'] - $aOrder['paid_amount']);
          if ($fToPay > 0) {
            if (intval($_POST['orders'][$iPKey+1]) > 0) {
              if ($fPaidAmount > $fToPay) {
                // jeśli wystarczy hajsu, opłacamy tyle ile powinno być
                $fOrderPaydAmount = $fToPay;
              } else {
                // nie wystarczy hajsu, opłacamy tyle ile mamy
                $fOrderPaydAmount = $fPaidAmount;
              }
            } else {
              // ostatenie zamówienie, ma tu ma zostać opłacona cała reszta
              $fOrderPaydAmount = $fPaidAmount;
            }
            try {
              $this->proceedPayStatmentOrder($iStatmentId, $iOrderId, $fOrderPaydAmount);
              $fPaidAmount = $fPaidAmount - $fOrderPaydAmount;
            } catch (Exception $exc) {
              $sMsg = sprintf(_('Wystąpił błąd podczas matchowania przelewów do zamówień '.$iOrderId.' <br />'.$exc->getMessage()));
              $this->sMsg .= GetMessage($sMsg, true);
              // dodanie informacji do logow
              Common::RollbackTransaction();
              AddLog($sMsg, true);
              return $this->Show($pSmarty);
            }
          } else {
            $sMsg = sprintf(_('Zamówienie '.$aOrder['order_number'].' nie posiada niedopłaty, do opłacenia '.$fToPay));
            $this->sMsg .= GetMessage($sMsg, true);
            // dodanie informacji do logow
            Common::RollbackTransaction();
            AddLog($sMsg, true);
            return $this->Show($pSmarty);
          }
        } else {
          $sMsg = sprintf(_('Występuje zbyt duża niedopłata aby opłacić też '.$aOrder['order_number'].', do opłacenia '.$fToPay));
          $this->sMsg .= GetMessage($sMsg, true);
          // dodanie informacji do logow
          Common::RollbackTransaction();
          AddLog($sMsg, true);
          return $this->Show($pSmarty);
        }
      }
    }
    if(!empty($sNumbers)){
      $sNumbers = substr($sNumbers,0,-2);
    }

    $sMsg = sprintf(_('Zamówienia zostały opłacone "'.$sNumbers.'"'));
    $this->sMsg .= GetMessage($sMsg, false);
    // dodanie informacji do logow
    Common::CommitTransaction();
    AddLog($sMsg, false);
    $this->Show($pSmarty);
  }// end of matchMultipleOrders method


  /**
   * Metoda wyświetla szczegóły przelewu, można potwierdzić więcej niż jednen przelew do zamówienia
   *
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iStatmentId
   */
  public function Details(&$pSmarty, $iStatmentId) {
    global $aConfig;

    $aData = $this->getStatment($iStatmentId);
    include_once('lib/Form/FormTable.class.php');
    $oForm = new FormTable('match_orders', _('Lista zamówień'), array('action'=>phpSelf(array('id' => $iStatmentId)), 'enctype'=>'multipart/form-data'));
    $oForm->AddHidden('do', 'match_multiple_orders');

    $aOrders = $this->findOrders();
    for ($i = 0; $i < 20; $i++) {
      $oForm->AddRow('Zamówienie '.($i+1),
              $oForm->GetTextHTML('orders['.$i.']', 'Zamówienie '.($i+1), '', array('class' => 'choose_order'), 'text', '', false)
              .'<span class="to_pay_amount" style="padding-left: 20px; font-size: 16px;"></div>'//$oForm->GetTextHTML('orders_payed['.$i.']', _('Opłacono zamówienie '.($i+1)), '', array(), '', '', ($i === 0 ? true : false))
              , '', array(), array(), ($i === 0 ? true : false)
              );
    }

    $oForm->AddInputButton('add_statment', _('Dodaj przelew'), array('onclick' => 'addStatment();'), 'button');
    $oForm->AddRow(_('Wartość zamówień'), '<span id="sum_payed"></span> zł', array(), array('style' => 'font-weight: bold; font-size: 14px;'));
    $oForm->AddRow(_('Wartość przelewu'), '<span id="sum_statment_amount">'.str_replace('.', ',', $aData['paidamount']).'</span> zł', array(), array('style' => 'font-weight: bold; font-size: 14px;'));

    $oForm->AddRow('&nbsp;', $oForm->GetInputButtonHTML('send', _('Zatwierdź przelewy'), array('onclick' => 'doConfirm()'), 'button').'&nbsp;&nbsp;'.$oForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));


    //$sJs2 = $this->assignPHPVaribles($this->findOrdersJS());
    $sJs = $this->getDetailsJS();
    $sJs2 = $this->getOrdersTransferJs('recountSum(true);');
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJs.$sJs2.$oForm->ShowForm());
  }

  /**
   *
   * @param array $aOrdersJS
   * @return array
   */
  private function assignPHPVaribles(array $aOrdersJS) {

    $sJs2 = '';
    $sJs2 .= '<script type="text/javascript">
    var aOrderValues=new Array();
    aOrderValues[0]=0.0;';

    foreach($aOrdersJS as $aItem) {
      $sJs2 .= 'aOrderValues['.$aItem['id'].']='.$aItem['to_pay2'].'; ';
    }

    $sJs2 .= '
    </script>';
    return $sJs2;
  }

  /**
   *
   * @return string
   */
  private function getDetailsJS() {

    $sJs = '
<script type="text/javascript">
  function doConfirm() {
     if ($("#sum_payed").attr("style") == "color: green") {
        $("#match_orders").submit();
     } else {
      if (confirm(\'Wartość przelewu nie jest równa wartości zamówień. Czy na pewno chcez zatwierdzić ?\')) {
        $("#match_orders").submit();
      }
     }
  }
  
  function recountSum(bRecountValue) {
    var fSum = 0.00;
    $(".to_pay_amount:visible").each(function(index, value) {
      var fOrdId = $(this).html()+"";
      if (fOrdId !== "") {
        var fOrderPayed = parseFloat(fOrdId);
        fSum = fSum + fOrderPayed;
      }
    });

    fSum = fSum.toFixed(2)+"";
    $("#sum_payed").html(fSum.replace(".",","));
    var sAmountPayed = "-" + $("#sum_statment_amount").html();
    if (sAmountPayed == $("#sum_payed").html()) {
      $("#sum_payed").attr("style", "color: green");
    } else {
      $("#sum_payed").attr("style", "color: red");
    }
  }
  
  $(function(){

    function fillOrderPrice(oThis) {
      
      var iOrdId = $(oThis).val();
      var fOrderPayed = parseFloat(aOrderValues[iOrdId])+"";
      fOrderPayed = fOrderPayed.replace(".",",")
      var oInput = $(oThis).parent().children("input[type=\'text\']");
      oInput.val(fOrderPayed);
    }
    

    
    $("input").change( function () {
      recountSum();
    });

    $(\'input[name^=\"orders\"]\').parent().parent().hide();
    addStatment();
    addStatment();
  });
  
  function addStatment() {
    var iCount = $(\'input[name^=\"orders\"]:visible\').length; 
    $(\'input[name=\"orders\[\'+iCount+\'\]\"]\').parent().parent().show();
  }

</script>
';
    return $sJs;
  }


  /**
   * Metoda pobiera przelew
   *
   * @global type $aConfig
   * @param type $iStatmentId
   * @return type
   */
  private function getStatment($iStatmentId) {
    global $aConfig;

    $sSql = "SELECT DISTINCT A.id, A.title, A.order_nr,'&nbsp;' AS `to_pay_ammount`, A.matched_order, A.paid_amount paidamount, '&nbsp;' AS diff, A.created
						 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements AS A
             WHERE id = ".$iStatmentId;
    return Common::GetRow($sSql);
  }


	/**
	 * Metoda wyswietla liste subskrybentow newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/EditableView.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true,
			'editable' => true,
			'send_button' => 'match',
			'send_button2' => 'toOpek',
      'send_button3' => 'toApproved',
      'items_per_page' => 20
//      'per_page' => false,
		);

		if($_POST['f_status']==3) {
			unset($aHeader['send_button']);
			unset($aHeader['send_button2']);
      unset($aHeader['send_button3']);
		}
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'title',
				'content'	=> $aConfig['lang'][$this->sModule]['list_title'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'order_nr',
				'content'	=> _('Wyodrębniony nr zam'),
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'matched_order',
				'content'	=> $aConfig['lang'][$this->sModule]['list_matched_order'],
				'sortable'	=> true,
				'width'	=> '105'
			),
			array(
				'db_field'	=> 'to_pay_amount',
				'content'	=> $aConfig['lang'][$this->sModule]['list_to_pay_amount'],
				'sortable'	=> true,
				'width'	=> '85',
        'style' => 'font-size: 14px; font-weight: bold; '
			),
			array(
				'db_field'	=> 'paidamount',
				'content'	=> $aConfig['lang'][$this->sModule]['list_paid_amount'],
				'sortable'	=> true,
				'width'	=> '105',
        'style' => 'color: #c30500; font-size: 14px; font-weight: bold;'
			),
			array(
				'db_field'	=> 'diff',
				'content'	=> $aConfig['lang'][$this->sModule]['list_diff'],
				'sortable'	=> true,
				'width'	=> '75',
        'style' => 'font-size: 14px; font-weight: bold; '
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang'][$this->sModule]['list_created'],
				'sortable'	=> true,
				'width'	=> '105'
			),
      array(
				'db_field'	=> 'created_by',
				'content'	=> _('Zaimportował'),
				'sortable'	=> true,
				'width'	=> '105'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '15'
			)
		);

			$sFilter='';

    switch($_POST['f_status']) {
      case'1': $sFilter=' AND `deleted`=\'1\''; 	break;
      case'2': $sFilter=''; 	break;
      case'3': $sFilter=' AND `finished`=\'1\' AND `deleted`=\'0\''; 	break;
      default: $sFilter=' AND `finished`=\'0\' AND `deleted`=\'0\''; 	break;
    }

    if (!isset($_POST['f_approved']) || $_POST['f_approved'] == '0') {
      $sFilter .= ' AND approved = "0" ';
    } else {
      $sFilter .= ' AND approved = "1" ';
    }

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(DISTINCT A.id)
						 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements AS A
						 WHERE is_Opek='0'".
							$sFilter.
		(isset($_POST['search']) && !empty($_POST['search']) ? (" AND A.title LIKE '%".$_POST['search']."%'") : '');
		$iRowCount = intval(Common::GetOne($sSql));

		$pView = new EditableView('orders_bank_statements', $aHeader, $aAttribs, $iRowCount);
		$pView->AddRecordsHeader($aRecordsHeader);


    // FILTRY
    // wszystkie/usuniete/nierozliczone
    $aStatuses = array(
      array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_new']),
      array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['f_matched']),
      array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_deleted']),
      array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
    );
    $pView->AddFilter('f_status', $aConfig['lang'][$this->sModule]['f_status'], $aStatuses, $_POST['f_status']);

    $aApprovedStatus = [
        ['value' => '0', 'label' => 'NIE'],
        ['value' => '1', 'label' => 'TAK']
    ];

    $pView->AddFilter('f_approved', _('Zaksięgowane'), $aApprovedStatus, $_POST['f_approved']);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich subskrybentow
			$sSql = "SELECT DISTINCT A.id, A.title, A.order_nr, A.matched_order, '&nbsp;' AS `to_pay_ammount`, A.paid_amount paidamount, '&nbsp;' AS diff, A.created, A.created_by, A.finished
						 	 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements AS A
						 	 WHERE is_Opek='0'".
							$sFilter.
			(isset($_POST['search']) && !empty($_POST['search']) ? (" AND A.title LIKE '%".$_POST['search']."%'") : '').
							 " ORDER BY ".(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

      $aOrdersToCompare = $this->findOrders();
			foreach($aRecords as $iKey=>$aItem){
        $aRecords[$iKey] = $this->proceedViewItem($aItem, $aOrdersToCompare);
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('auto_match', 'details'),//, 'delete', 'delete_statement_update_balance'
					'params' => array (

					    'auto_match' => array('id'=>'{id}'),
						'activate'	=> array('id'=>'{id}'),
						'edit'	=> array('id'=>'{id}'),
            'details'	=> array('id'=>'{id}'),
						'delete'	=> array('id'=>'{id}'),
                        'delete_item'	=> array('id'=>'{id}')
					),
          'icon' => array(
              'details' => 'changes',
              'auto_match' => 'auto-source',
              'delete_item' => 'delete_sum',
          )
				)
			);
            if ($_SESSION['user']['name'] == 'mchudy' ||
                $_SESSION['user']['name'] == 'agolba' ||
                $_SESSION['user']['name'] == 'kmalz' ||
                $_SESSION['user']['name'] == 'nzienkiewicz') {
                if ($_SESSION['user']['name'] == 'mchudy' ||
                    $_SESSION['user']['name'] == 'agolba') {
                    $aColSettings['action']['actions'] = ['auto_match', 'details', 'delete', 'delete_item'];

                } else {
                    $aColSettings['action']['actions'] = ['auto_match', 'details', 'delete_item'];

                }
            }
			$aEditableSettings = array(
//        'paidamount' => array(
//          'type' => 'text'
//        )
      );
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings,$aEditableSettings);
		}

    $aRecordsFooter = array(
      array('check_all'),
      array(1 => 'add')
    );
    $aRecordsFooterParams = array(

    );

		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

    //$aOrdersJS = $this->findOrdersJS();
    //$sJs2 = $this->getFindOrderJS($aOrdersJS);
    $sJS = $this->getOrdersTransferJs();
    //$sJS = $this->getJsView();
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.$sJs2.
																				$pView->Show());
	} // end of Show() function

  /**
   *
   * @global array $aConfig
   * @return string
  private function getJSView(){
    global $aConfig;

    $sJS = '
		<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
			$("#orders_bank_statements").submit(function() {
			//alert("this");
				var sErr = "";
				$(".heditable_input, .editable_input").each(function(){
					regex = /^editable\[([A-Za-z0-9_]+)\]\[([A-Za-z0-9_]+)\]$/;
					matches = regex.exec(this.name);
					if(matches[2] == "paidamount"){
						$(this).val($(this).val().replace(/&nbsp;/g, "").replace(/^\s+/g, "").replace(/\s+$/g, ""));
						if ($(this).val() != "" && (!$(this).val().match(/^-?(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/) || $(this).val() < 0)) {
							sErr += "\t- Zapłacono\n";
						}
					}

				});
				if(sErr != ""){
					alert("'.$aConfig['lang']['form']['error_prefix'].'\n"+sErr+"'.$aConfig['lang']['form']['error_postfix'].'");
					return false;
				}
			});
		});
	// ]]>
</script>
		';
    return $sJS;
  }
  */

  /**
   *
   * @param array $aOrdersJS
   * @return string
   */
  private function getFindOrderJS(array $aOrdersJS) {

    $sJs2='';
    $sJs2.='<script>
    var aOrderValues=new Array();
    aOrderValues[0]=0.0;';

    foreach($aOrdersJS as $aItem) {
      $sJs2.='aOrderValues['.$aItem['id'].']='.$aItem['to_pay2'].'; ';
    }


    $sJs2.='
    function changeDiff(iId, iOrder, fMoney) {
      document.getElementById("orderDiff_"+iId).innerHTML=(fMoney-aOrderValues[iOrder]).toFixed(2);
      if((fMoney-aOrderValues[iOrder]).toFixed(2)!=0.00)
        document.getElementById("orderDiff_"+iId).style.fontWeight="bold";
      else	
        document.getElementById("orderDiff_"+iId).style.fontWeight="normal";
      document.getElementById("orderDiff_"+iId).innerHTML=document.getElementById("orderDiff_"+iId).innerHTML.replace(".", ",");

      if((fMoney-aOrderValues[iOrder]).toFixed(2)>0)
        document.getElementById("orderDiff_"+iId).style.color="#090";
      else if((fMoney-aOrderValues[iOrder]).toFixed(2)<0)
        document.getElementById("orderDiff_"+iId).style.color="#c00";
      } 
    </script>';
    return $sJs2;
  }

  /**
   *
   * @param int $iStatmentId
   * @return boolean
   */
  private function finishedOrderBankStatement($iStatmentId) {

    $aStateVal['finished']='1';
    $aStateVal['deleted']='0';
    if(Common::Update("orders_bank_statements", $aStateVal, "id = ".$iStatmentId) === false){
        return false;
    }
    return true;
  }

  /**
   *
   * @param array $aItem
   * @param array $aOrdersToCompare
   * @return string
   */
  private function proceedViewItem(array $aItem, array $aOrdersToCompare) {

    $aRecord = $aItem;
    if (strlen($aItem['order_nr']) > 5) {
      $aRecord['to_pay_ammount'] = '<span class="to_pay_amount"></span>';//$this->getOrderToPayAmount($aItem['order_nr']);
    }

    $fDiffr = $aItem['paidamount']-Common::FormatPrice2($aRecord['to_pay_ammount']);
    if ($aItem['matched_order'] > 0) {
      $aRecord['diff']='<span id="orderDiff_'.$aItem['id'].'" '.($fDiffr!=0.0?' style="font-weight:bold; font-size: 15px; color:#'.($fDiffr>0?'c00':'c00').'"':'').'>'.Common::FormatPrice($fDiffr).'</span>';
    } else {
      $aRecord['diff']='<span id="orderDiff_'.$aItem['id'].'"></span>';
    }

    //$aRecord['matched_order'] = Form::GetSelect('order['.$aItem['id'].']', array('style'=>'width:200px;', 'onchange'=>'changeDiff('.$aItem['id'].', this.value, '.($aItem['paidamount']+0).')'), $aOrdersToCompare, $aItem['matched_order']);
    $aRecord['matched_order'] = Form::GetText('order['.$aItem['id'].']', '', array('class' => 'choose_order', 'id' => 'order', 'style' => 'font-size: 14px; width: 400px;'));
    $aRecord['paidamount'] = '<span class="paidamount">'.$aItem['paidamount'].'</span>';

    if ($fDiffr == 0.00) {
      $aRecord['disabled'] = array('details');
    }
    unset($aRecord['finished']);
    return $aRecord;
  }

  /**
   *
   * @param int $iOrderId
   * @global array $aConfig
   * @return string
   */
  private function getOrdersTransferJs($sAddActionOnSelect = '') {
    global $aConfig;

    $sJs = '
<script>
    $( function () {
      $(".choose_order").autocomplete({
        source: "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetOrders.php?mode=statements",
        select: function( event, ui ) {
          $(this).attr("readonly", "readonly").css("background-color", "#CACACA");
          $(this).parent().parent().find(".to_pay_amount").html(ui.item.balance);
          var toPay = parseFloat(ui.item.balance);
          var paidAmount = parseFloat($(this).parent().parent().find(".paidamount").html());
          var diffPaid = parseFloat(toPay + paidAmount).toFixed(2);
          var changeItem = $(this).parent().parent().find("span[id^=\'orderDiff\']");
          if (diffPaid != 0.00) {
            changeItem.attr("style", "color: red;");
          } else {
            changeItem.attr("style", "color: green;");
          }
          changeItem.html(diffPaid);
          $(this).val(ui.item.value);
          '.$sAddActionOnSelect.'
        }
      });
    });
</script>
';
    return $sJs;
  }

  /**
   *
   * @param int $iOrderNumber
   * @return float
   */
  private function getOrderToPayAmount($iOrderNumber) {

    $sSql = 'SELECT (`to_pay`-`paid_amount`)
             FROM orders
             WHERE 
             order_number = "'.$iOrderNumber.'"';
    $fToPayAmount = $this->pDbMgr->GetOne('profit24', $sSql);
    return Common::formatPrice($fToPayAmount);
  }


	/**
	 * Metoda usuwa wybranych subskrybentow
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony newslettera
	 * @param	integer	$iId	- Id usuwanego subskrybenta
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;

		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = substr($aItem['title'],0,40);
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.substr($aItem['title'],0,40).'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);

			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
					$this->sMsg .= GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel);
				$this->sMsg .= GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton



    /**
     * Metoda usuwa wybranych subskrybentow
     *
     * @param	object	$pSmarty
     * @param	integer	$iPId	- Id strony newslettera
     * @param	integer	$iId	- Id usuwanego subskrybenta
     * @return 	void
     */
    function DeleteStatementUpdateBalance(&$pSmarty, $iPId, $iId) {
        global $aConfig;
        $bIsErr = false;
        $aTmp = array();
        $sDel = '';
        $sFailedToDel = '';
        $iDeleted = 0;

        if ($iId > 0) {
            $_POST['delete'][$iId] = '1';
        }
        if (!empty($_POST['delete'])) {
            Common::BeginTransaction();
            $aItems =& $this->getItemsToDelete($_POST['delete']);
            foreach ($aItems as $aItem) {
                // usuwanie
                if (($iRecords = $this->deleteItemUpdateBalance($aItem['id'])) === false) {
                    $bIsErr = true;
                    $sFailedToDel = substr($aItem['title'], 0, 40);
                    break;
                } elseif ($iRecords == 1) {
                    // usunieto
                    $iDeleted++;
                    $sDel .= '"' . substr($aItem['title'], 0, 40) . '", ';
                }
            }
            $sDel = substr($sDel, 0, -2);

            if (!$bIsErr) {
                // usunieto
                Common::CommitTransaction();
                if ($iDeleted > 0) {
                    $sMsg = sprintf($aConfig['lang'][$this->sModule]['del_balance_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel);
                    $this->sMsg .= GetMessage($sMsg, false);
                    // dodanie informacji do logow
                    AddLog($sMsg, false);
                }
            }
            else {
                // blad
                Common::RollbackTransaction();
                $sMsg = sprintf($aConfig['lang'][$this->sModule]['del_balance_err'], $sFailedToDel);
                $this->sMsg .= GetMessage($sMsg);
                // dodanie informacji do logow
                AddLog($sMsg);
            }
        }
        $this->Show($pSmarty, $iPId);
    } // end of Delete() funciton


  /**
   *
   * @param array $aTransactions
   */
  private function getUniqueNewTransactionsToProceeed(array $aTransactions, &$duplicatesQuantity) {

    $aUniqueReferenceNumbers = array();
    if (isset($aTransactions[0])) {
      foreach ($aTransactions as &$aItemTransaction) {
        try {
          $aItemTransaction = $this->getUniqueNewTransactionsToProceeedItem($aUniqueReferenceNumbers, $aItemTransaction);
        } catch (Exception $ex) {
          throw new $ex;
        }
      }
    } else {
      // mamy tylko jeden przelew, zmienia się wtedy tabela
      try {
        $aTransactions = $this->getUniqueNewTransactionsToProceeedItem($aUniqueReferenceNumbers, $aTransactions);
      } catch (Exception $ex) {
        throw new $ex;
      }
    }
    $duplicatesQuantity = $this->countDuplicatesTransactions($aUniqueReferenceNumbers);
    return $aTransactions;
  }

    /**
     * @param $aUniqueReferenceNumbers
     * @return int
     */
    private function countDuplicatesTransactions($aUniqueReferenceNumbers)
    {
        $quantityDuplicates = 0;
        foreach ($aUniqueReferenceNumbers as $quantity) {
            if ($quantity > 1) {
                $quantityDuplicates += ($quantity - 1);
            }
        }
        return $quantityDuplicates;
    }

  /**
   *
   * @param array $aUniqueReferenceNumbers
   * @param array $aItemTransaction
   * @return array
   * @throws Exception
   */
  private function getUniqueNewTransactionsToProceeedItem(&$aUniqueReferenceNumbers, $aItemTransaction) {
    if ($this->checkTransactionDataAddReferenceNumber($aItemTransaction) === false) {
      throw new Exception('Nie udało się zaimportować listy przelewów, dane w pliku XML są nieprawidłowe !');
    }
    $sReference = $aItemTransaction['reference_number'];
    if (!isset($aUniqueReferenceNumbers[$sReference])) {
      $aUniqueReferenceNumbers[$sReference] = 1;
    } else {
      $aUniqueReferenceNumbers[$sReference]++;// dodajemy wartość
      $sActValueCounter = $aUniqueReferenceNumbers[$sReference];
      $md5NewDuplicateReference = md5($sReference.$sActValueCounter);
      $aUniqueReferenceNumbers[$md5NewDuplicateReference] = 1;
      $aItemTransaction['reference_number'] = $md5NewDuplicateReference; // zmieniamy reference
    }
    return $aItemTransaction;
  }

	private function checkLock()
	{
		$bankLoad = $_SESSION['lock_bank_load'];

		if (!empty($bankLoad)) {
			$now = new DateTime();

			if ($now < $bankLoad) {
				$diff = $bankLoad->diff($now)->format('%i:%s');
				exit("Przelewy zostały zablokowanie, odblokowanie nastapi za: $diff");
			}
		}

		$toInsert =  new \DateTime();
		$toInsert->modify('+1 minutes');
		$_SESSION['lock_bank_load'] = $toInsert;
	}

  /**
   * Metoda dodaje do bazy danych nowa kategorie strony newslettera
   *
   * @param    object $pSmarty
   * @param    integer $iPId - Id strony
   * @return    void
   */
  function Insert(&$pSmarty, $iPId)
  {

    $lockStatements = new LIB\orders\Payment\lockStatements($this->pDbMgr);
    if ($lockStatements->lock() !== true) {
      exit("Przelewy zostały zablokowanie, import przelewów został przerwany.");
    }
//		$this->checkLock();

    global $aConfig;
    set_time_limit(1800);
    $bIsErr = false;
    $iFound = 0;
    $iAdded = 0;
    $aInsertResult = array();

    if (empty($_POST)) {
      // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
      $lockStatements->unlock();
      $this->Show($pSmarty, $iPId);
      return;
    }
    if (!isset($_POST['token']) || !isset($_SESSION['token']) || $_POST['token'] != $_SESSION['token']) {
      $this->sMsg .= GetMessage(_('Jesteś za szybki, dwa razy wysłałeś formualrz, prawdopodobnie przelewy zostały już zmatchowane, ale sprawdź lepiej.'));
      // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
      $lockStatements->unlock();
      $this->Show($pSmarty, $iPId);
      return;
    }
    unset($_SESSION['token']);

    // dolaczenie klasy walidatora
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    if (!$oValidator->Validate($_POST)) {
      $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
      // wystapil blad wypelnienia formularza
      // wyswietlenie formularza wraz z komunikatem bledu
      $lockStatements->unlock();
      $this->Add($pSmarty, $iPId);
      return;
    }

    require_once("XML/Unserializer.php");
    $aData = array();
    $aOptions = array(
        'complexType' => 'array'
    );
    $pUnser = new XML_Unserializer($aOptions);

    $oF = fopen($_FILES['statements']['tmp_name'], 'r');
    if (file_exists($_FILES['statements']['tmp_name']) && ($oF !== false)) {
      // czyszczenie i zapisa
      $sData = fread($oF, filesize($_FILES['statements']['tmp_name']));
      /*
      $sData = iconv('UTF-8', 'cp1250', $sData);
      $sData = str_ireplace('<?xml version="1.0" encoding="UTF-8" standalone="yes"?>','<?xml version="1.0" encoding="windows-1250" standalone="yes"?>',$sData);
      //dump($sData);
      //die;
      */
      $pResult = $pUnser->unserialize($sData, false);

      if (!PEAR::IsError($pResult)) {
        $aData =& $pUnser->getUnserializedData();

        foreach ($aData as $key => $row) {
            $newData[mb_strtolower($key, 'UTF-8')] = $row;
        }
        $aData = $newData;

        try {
          $iRefduplicatesQuantity = 0;
          $aData['transaction'] = $this->getUniqueNewTransactionsToProceeed($aData['transaction'], $iRefduplicatesQuantity);
          if ($iRefduplicatesQuantity > intval($_POST['max_duplicates_quantity'])) {
              throw new Exception(_("Przelewy nie zostały zaimportowane. Przekroczono ilość duplikatów. Ilość duplikatów ".$iRefduplicatesQuantity));
          }
        } catch (Exception $ex) {
          $sMsg = $ex->getMessage();
          AddLog($sMsg, false);
          $this->sMsg .= GetMessage($sMsg, true);
          $lockStatements->unlock();
          return $this->Show($pSmarty, $iPId);
        }

        if (!empty($aData)) {
          if (isset($aData['transaction'][0])) {
            foreach ($aData['transaction'] as $aItem) {
              Common::BeginTransaction();
              $mRet = $this->processItem($aItem);

              if ($mRet === -1) {
                Common::RollbackTransaction();
                $bIsErr = -1;
                break;
              } elseif ($mRet === -2) {
                Common::RollbackTransaction();
                $bIsErr = -2;
                break;
              } elseif ($mRet === false) {
                Common::RollbackTransaction();
                $bIsErr = true;
                break;
              } elseif (is_string($mRet)) {
                $aInsertResult[$mRet]++;
              }

              Common::CommitTransaction();
            }
          } else {
            Common::BeginTransaction();
            $mRet = $this->processItem($aData['transaction']);
            if ($mRet === -1) {
              Common::RollbackTransaction();
              $bIsErr = -1;
            } elseif ($mRet === -2) {
              Common::RollbackTransaction();
              $bIsErr = -2;
            } elseif ($mRet === false) {
              Common::RollbackTransaction();
              $bIsErr = true;
            } elseif (is_string($mRet)) {
              $aInsertResult[$mRet]++;
            }
            Common::CommitTransaction();
          }
        } else {
          // brak danych
          $bIsErr = true;
        }
      } else {
        $this->sMsg .= GetMessage($aConfig['lang'][$this->sModule]['wrong_file_type'], true);
        // wystapil blad wypelnienia formularza
        // wyswietlenie formularza wraz z komunikatem bledu
        $lockStatements->unlock();
        $this->Add($pSmarty, $iPId);
        return;
      }
      fclose($oF);
    } else {
      $bIsErr = true;
    }

    //echo memory_get_peak_usage()."<br />";
    //echo memory_get_peak_usage(true);
      if ($bIsErr === false) {
          if (!empty($this->ordersPayed)) {
              foreach ($this->ordersPayed as $iOId) {
                  $this->sendOrderPayedMail($iOId);
              }
          }

      // dodano adresy
      $sInsertAdditionalInfo = $this->getResultImportStatementsAdditionalInfo($aInsertResult);
      $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok']) . '<br />' . $sInsertAdditionalInfo;
      $this->sMsg .= GetMessage($sMsg, false);

      // dodanie informacji do logow
      $lockStatements->unlock();
      AddLog($sMsg, false);
      $this->Show($pSmarty, $iPId);
    } elseif ($bIsErr == -1) {
      // nie dodano adresow
      // wyswietlenie komunikatu o niepowodzeniu
      // oraz ponowne wyswietlenie formularza dodawania
      $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err_1']);
      $this->sMsg .= GetMessage($sMsg);
      $lockStatements->unlock();

      AddLog($sMsg);
      $this->Add($pSmarty, $iPId);
    } elseif ($bIsErr == -2) {
      // nie dodano adresow
      // wyswietlenie komunikatu o niepowodzeniu
      // oraz ponowne wyswietlenie formularza dodawania
      $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err_2']);
      $this->sMsg .= GetMessage($sMsg);
      $lockStatements->unlock();

      AddLog($sMsg);
      $this->Add($pSmarty, $iPId);
    } else {
      // nie dodano adresow
      // wyswietlenie komunikatu o niepowodzeniu
      // oraz ponowne wyswietlenie formularza dodawania
      $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err']);
      $this->sMsg .= GetMessage($sMsg);
      $lockStatements->unlock();

      AddLog($sMsg);
      $this->Add($pSmarty, $iPId);
    }
  } // end of Insert() funciton

  /**
   *
   * @param array $aInsertStatementsResult
   * @return string
   */
  private function getResultImportStatementsAdditionalInfo($aInsertStatementsResult) {

    $sMsg = _('%s w ilości: %s');
    $sMsgResult = '';
    foreach ($aInsertStatementsResult as $sResltMsg => $iCount) {
      $sMsgResult .= sprintf($sMsg, $sResltMsg, $iCount);
    }
    return $sMsgResult;
  }


	/**
	 * Metoda sprawdza wersję pliku XML który jest importowany
	 *
	 * @param ref array $aItem  - tablica elementu
	 * @return bool
	 */
	private function checkTransactionDataAddReferenceNumber(&$aItem) {

		// jesli nie określono wersji XML
		//if ($this->iVersionXML == 0) {
			// sprawdzamy dane XML

			// pusty reference
			if (!isset($aItem['reference_number']) || $aItem['reference_number'] == '') {
			    if ($aItem['referenceIdNumber'] != '' && $aItem['creditDebitIndicator'] != '')
			    {
                    $this->iVersionXML = 3;
                    $aItem = $this->convertToOldFormat($aItem);
                    if ($this->changeOldXMLData($aItem) === false) {
                        return false;
                    }
                    if (!empty($aItem) && is_array($aItem)) {
                        return true;
                    } else {
                        return false;
                    }
                }
                elseif ($aItem['date'] != '' && $aItem['description'] != '' && $aItem['amount'] != '') {
					// jest ok
					$this->iVersionXML = 2;
					if ($this->changeOldXMLData($aItem) === false) {
						return false;
					}

					return true;
				} else {
					// brak wymaganych pól w tym przelewie
					return false;
				}

			} else {
				$this->iVersionXML = 1;
			}
			/*
		} elseif ($this->iVersionXML == 2) {
			// znamy wersję od razu parsujemy
			if ($this->changeOldXMLData($aItem) === false) {
				return false;
			}
		}
			 */

		return true;
	}// end of checkTransactionData() method

    /*
        <displayAccountNumber>0330007969</displayAccountNumber>
        <transactionDate>2017-07-20</transactionDate>
        <transactionPostingDate>2017-07-20</transactionPostingDate>
        <transactionDescription>BILLBIRD SPÓŁKA AKCYJNA 26114010810000580482001025 060711091717 </transactionDescription>
        <transactionCode>607</transactionCode>
        <referenceIdNumber>BMRKA1707204453935</referenceIdNumber>
        <transactionAmount>228.669999999999987494447850622236728668212890625</transactionAmount>
        <currencyCode>PLN</currencyCode>
        <foreignTransactionAmount>228.67</foreignTransactionAmount>
        <transactionType>PRZELEW ZEWNĘTRZNY - WPŁATA</transactionType>
        <runningBalance>26377.02999999999883584678173065185546875</runningBalance>
        <creditDebitIndicator>CR</creditDebitIndicator>
        <transactionSequenceNumber>8</transactionSequenceNumber>
        <transactionDisplayDate>2017-07-20</transactionDisplayDate>
        <nfcEnrolled>false</nfcEnrolled>
        <principalAmountAfterPost>0.0</principalAmountAfterPost>
        <displayLoanDetails>false</displayLoanDetails>
    */

    /*
        <date>19/07/2017</date>
        <description>IWONA SAKOWSKA 31249000050000400442280004 zamówienie nr 160717039717 Artur Sa kowski</description>
        <amount>518,40</amount>
        <account_number>0330017059</account_number>
        <account_name/>
        <running_balance>1.630,98</running_balance>
        <transaction_type>PRZELEW ZEWNĘTRZNY - WPŁATA</transaction_type>
    */

    /**
     * @param $aItem
     * @return array
     */
    private function convertToOldFormat($aItem) {

        $convItem = [
            'date' => preg_replace('/(\d{4})-(\d{2})-(\d{2})/','$3/$2/$1', $aItem['transactionDate']),
            'description' => $aItem['transactionDescription'],
            'amount' => Common::formatPrice($aItem['transactionAmount'], '.'),
            'account_number' => $aItem['displayAccountNumber'],
            'running_balance' => Common::formatPrice($aItem['runningBalance'], '.'),
            'transaction_type' => $aItem['transactionType']
        ];

        return $convItem;
    }


    /**
     * @param $iban
     * @return mixed
     */
    public function getBankIdentifier($iban) {
        preg_match('/^\d{2}(\d{4})/', $iban, $matches);
        if (isset($matches[1])) {
            return $matches[1];
        }
        return false;
    }


    /**
     * @param $description
     * @param $bankIdentifier
     * @return bool
     */
    private function validateFirstNumber($description, $bankIdentifier) {
        // 00013200006000000408123 MAGDALENA KŁOSOWSKASZARYCH SZEREGÓW 85132015374061746630000001
        preg_match('/^(\d{3}'.$bankIdentifier.'\d{16} )/', $description, $matches);
        if (isset($matches[1])) {
            return true;
        }
        return false;
    }

    /**
     * @param $description
     * @param $bankIdentifier
     * @return mixed
     */
    private function removeFirstNumber($description, $bankIdentifier) {

        return preg_replace('/^(\d{3}'.$bankIdentifier.'\d{16} )/', '', $description);
    }

	/**
	 * Metoda zmienia dane dla starego XML'a
	 *
	 * @param ref array $aItem
	 * @return bool
	 */
	private function changeOldXMLData(&$aItem) {
			if ($aItem['transaction_amount'] != '') {
        return false;
      }

        preg_match('/ (\d{26})/', $aItem['description'], $aMatchesIBAN);
        if (isset($aMatchesIBAN) && !empty($aMatchesIBAN)) {
            unset($aMatchesIBAN[0]);
            foreach ($aMatchesIBAN as $iban) {
                if ($this->checkIBAN($iban) === true) {
                    $bankIdentifier = $this->getBankIdentifier($iban);
                    if ($bankIdentifier !== false && $bankIdentifier != '' ) {
                        if (true === $this->validateFirstNumber($aItem['description'], $bankIdentifier)) {
                            $aItem['description'] = $this->removeFirstNumber($aItem['description'], $bankIdentifier);
                        }
                    }
                }
            }
        }


      $aMatches = array();
      preg_match('/^(\d{26})/', $aItem['description'], $aMatches);
      $aItem['source_iban_account_number'] = $aMatches[1];

			$aItem['transaction_amount'] = $aItem['amount'];
			unset($aItem['amount']);

      $aItem['payment_description'] = $aItem['description'];
      unset($aItem['description']);

			// reference_number
			$sDate = preg_replace("/(\d{2})\/(\d{2})\/(\d{4})/", "$3$2$1", $aItem['date']);
			if ($sDate == '') return false;
			$aItem['transaction_value_date'] = $sDate;

			// końcówka Reference
			$sPostRef = md5($sDate.$aItem['transaction_amount'].$aItem['payment_description']);
			$aItem['reference_number'] = $sPostRef;
	}// end of changeOldXMLData() method


    /**
     * @param $iban
     * @return bool
     */
    function checkIBAN($iban)
    {
        //(c) Bartłomiej Zastawnik, "Rzast".
        //Użycie funkcji dozwolone przy zachowaniu komentarzy.
                $puste = array(' ', '-', '_', '.', ',','/', '|');//znaki do usuniącia
                $temp = strtoupper(str_replace($puste, '', $iban));//Zostają cyferki + duże litery
                if (($temp{0}<='9')&&($temp{1}<='9')){//Jeżeli na początku są cyfry, to dopisujemy PL, inne kraje muszć być jawnie wprowadzone
                    $temp ='PL'.$temp;
                }
                $temp=substr($temp,4).substr($temp, 0, 4);//przesuwanie cyfr kontrolnych na koniec
                $znaki=array('0'=>'0','1'=>'1','2'=>'2','3'=>'3','4'=>'4',
                    '5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9',
                    'A'=>'10','B'=>'11','C'=>'12','D'=>'13','E'=>'14','F'=>'15',
                    'G'=>'16','H'=>'17','I'=>'18','J'=>'19','K'=>'20',
                    'L'=>'21','M'=>'22','N'=>'23','O'=>'24','P'=>'25',
                    'Q'=>'26','R'=>'27','S'=>'28','T'=>'29','U'=>'30',
                    'V'=>'31','W'=>'32','X'=>'33','Y'=>'34','Z'=>'35');//Tablica zamienników, potrzebnych do wyliczenia sumy kontrolnej
                $ilosc=strlen($temp);//długość numeru
                $ciag='';
                for ($i=0;$i<$ilosc;$i++){
                    $ciag.=$znaki[$temp{$i}];
                }
                $mod = 0;
                $ilosc=strlen($ciag);//nowa długość numeru
                for($i=0;$i<$ilosc;$i=$i+6) {
        //oblicznie modulo, $ciag jest zbyt wielkć liczbę na format integer, wiąc dzielć go na kawaśki
                    $mod = (int)($mod.substr($ciag, $i, 6)) % 97;
                }
                $out=($mod==1)?true:false;
        return $out;
    }

	/**
	 * Metoda przetwarza Element zamówienia
	 *
	 * @global type $aConfig
	 * @param type $aItem
	 * @return mixed : false - błąd ogólny : true - poszło ok : -1 - poblem z wersją XML'a
	 */
	private function processItem(&$aItem){
	global $aConfig;

		if (!empty($aItem['payment_description']) && !$this->referenceNumberExists($aItem['reference_number'])) {
			// usun puste znaki z tytulu

      $sTitle = preg_replace("/\s/","",$aItem['payment_description']);
      /** Modyfikacja Tylko OPKI **/
      //if (!stristr($sTitle, 'OPEK')) {
      //	 return true;
      //}

      $sOrderNr = '';
      // dopasuj nr zamowienia profitu
      if(preg_match("/[0-9]{5}\/[0-9]{4}\/[0-9]{2}\/PR24/", $sTitle, $aMatch)){
       $sOrderNr = $aMatch[0];
       // id zamowienia
       $iNr = $this->matchOrderNumber($sOrderNr);
      } else {
        $oMatchStatementToOrder = new MatchStatementToOrder($this->pDbMgr);
        $sOrderNr = $oMatchStatementToOrder->tryShellOrderNumber($aItem['payment_description']);
        if($sOrderNr != ''){
         // id zamowienia
         $iNr = $this->matchOrderNumber($sOrderNr);
        }
      }

      // usun " i ' z ceny
      $sPrice = preg_replace("/[\"\\'\s\.]/","",$aItem['transaction_amount']);
      $aItem['payment_description'] = preg_replace("/[\"\\']/","",$aItem['payment_description']);

      $sDatabaseDate = substr($aItem['transaction_value_date'],0,4).'-'.substr($aItem['transaction_value_date'],4,2).'-'.substr($aItem['transaction_value_date'],6,2);

      // Jezeli format daty jest nieprawidlowy wtedy zamien ja na date aktualna.
      if(!preg_match('/(19|20)[0-9]{2}[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])/', $sDatabaseDate)){
        $sDatabaseDate = date('Y-m-d');
      }

      $fStatementAmount = Common::formatPrice2($sPrice);
      $aValues = array(
       'ident_statments_file' => $aItem['account_number'],
       'title' => trim(mb_substr($aItem['description'], 0, 40)).' '.$aItem['payment_description'],
       'order_nr' => $sOrderNr,
       'matched_order' => $iNr>0?$iNr:'NULL',
       'paid_amount' => $fStatementAmount,
       'created' => $sDatabaseDate.' '.date('H:i:s'),
       'created_by' => $_SESSION['user']['name'],
       'iban'=> $aItem['source_iban_account_number'],
       'reference'=>$aItem['reference_number']
      );

      $iStatmentId = Common::Insert($aConfig['tabls']['prefix']."orders_bank_statements", $aValues);
      if ($iStatmentId === false){
       return false;
      } else {
        $oBalance = new Balance($this->pDbMgr);
        if ($oBalance->newStatement($aItem['account_number'], $fStatementAmount) === false) {
          return false;
        }
      }

      // może uda się automatycznie to zaksięgować
      if ($this->detactIsPostalFee($aValues['ident_statments_file'])) {
        if ($fStatementAmount > 0.00) {
          if ($this->changeStatmentToPostalFee($iStatmentId) === false) {
             return false;
          } else {
            if ($this->tryMatchStatementToOrder($iStatmentId, true) === true) {
             return _('Automatycznie rozliczone - Odbiór osobisty');
            }
          }
        }
      } else {
        if ($this->tryMatchStatementToOrder($iStatmentId) === true) {
         return _('Automatycznie rozliczone');
        }
      }
      return true;
    } else {
      // duplikaty
      return _('Ponownie importowane');
    }
    return true;
	}// end of processItem() method


    /**
     * @param $pSmarty
     * @param $iId
     * @return bool|string
     */
    private function tryAutoMatchStatement($pSmarty, $iId)
    {
        $sMsg = '';
        $sSql = 'SELECT * FROM orders_bank_statements WHERE id = '.$iId.' AND finished = "0" AND deleted = "0" AND is_opek = "0" AND approved = "0" ';
        $aObs = $this->pDbMgr->GetRow('profit24', $sSql);

        if (empty($aObs)) {
            return false;
        }
        // może uda się automatycznie to zaksięgować
        if ($this->detactIsPostalFee($aObs['ident_statments_file'])) {
            if ($aObs['paid_amount'] > 0.00) {
                if ($this->changeStatmentToPostalFee($iId) === false) {
                    return false;
                } else {
                    if ($this->tryMatchStatementToOrder($iId, true) === true) {
                        $sMsg .= _('Automatycznie rozliczone - Odbiór osobisty');
                    } else {
                        $sMsg .= _('Nie udało dopasować zamówienia');
                    }
                }
            }
        } else {
            if ($this->tryMatchStatementToOrder($iId) === true) {
                $sMsg .= GetMessage(_('Automatycznie rozliczone'), false);
            } else {
                $sMsg .= GetMessage(_('Nie udało dopasować zamówienia'), true);
            }
        }
        $this->sMsg .= $sMsg;
        return $this->Show($pSmarty);
    }// end of getWebsiteId() method

  /**
   *
   * @param string $sIdentStatementsFile
   * @return boolean
   */
  private function detactIsPostalFee($sIdentStatementsFile) {

    $sSql = 'SELECT postal_fee
             FROM bank_accounts 
             WHERE ident_statments_file = "'.$sIdentStatementsFile.'"
             LIMIT 1';
    return ($this->pDbMgr->GetOne('profit24', $sSql) == '1');
  }

  /**
   *
   * @param int $iStatementId
   * @param bool $bIsPostalFee
   * @return string bool
   */
  private function tryMatchStatementToOrder($iStatementId, $bIsPostalFee = false) {

    $oStatement = (object)$this->pDbMgr->getTableRow('orders_bank_statements', $iStatementId, array('*'));
    $oMatchStatement = new MatchStatementToOrder($this->pDbMgr);
    $oMatchStatement->setStatement($oStatement);
    if ($oMatchStatement->matchOrder() === true) {
      $aOrderMatched = $oMatchStatement->getMatchedOrder();
      if (!empty($aOrderMatched)) {
        if ($bIsPostalFee === FALSE) {

          $this->ordersPayed[] = $aOrderMatched['id'];
          if ($this->proceedPayStatmentOrder($oStatement->id, $aOrderMatched['id'], $oStatement->paid_amount) === true) {
            return true;
          }
        } else {
          include_once('Module_fedex_transfers.class.php');
          $oModule = new Module($this->pSmarty, true);

          if ($oModule->payOrder($aOrderMatched['id'], $oStatement->paid_amount, '', $oStatement->created) === false) {
            return false;
          } else {
            if ($oModule->addPayment($aOrderMatched['id'], $oStatement->created, $oStatement->paid_amount, $oStatement->title, '', $aOrderMatched['transport_id'], $iStatementId) === false) {
              return false;
            } else {
              $this->finishedOrderBankStatement($iStatementId);
            }
          }
        }
      }
    }
    return false;
  }

	/**
	 * funckja sprawdza czy istnieje juz przelew o danym reference number
	 * sNumber - nr referencyjny danego przelewu
	 * */
	function referenceNumberExists($sNumber){
		global $aConfig;
		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders_bank_statements WHERE reference ='".$sNumber."'";
		return intval(Common::GetOne($sSql)) > 0;
		}//end of referenceNumberExists

	/*
	 function Insert(&$pSmarty, $iPId) {
		global $aConfig;
		set_time_limit(1800);
		$bIsErr = false;
		$iFound = 0;
		$iAdded = 0;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Add($pSmarty, $iPId);
			return;
		}
		///dump($_FILES['statements']);
		if($_FILES['statements']['type'] != 'text/csv' && $_FILES['statements']['type'] != 'text/comma-separated-values' && $_FILES['statements']['type'] != 'text/plain'){
			$this->sMsg .= GetMessage( $aConfig['lang'][$this->sModule]['wrong_file_type'], true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Add($pSmarty, $iPId);
			return;
		}
		// rozpoczecie transkacji
		Common::BeginTransaction();
		// otwarcie pliku i wyszukiwanie adresow e-mail w jego zawartosci
		if (@$rFile = fopen($_FILES['statements']['tmp_name'], 'r')) {
			while(!feof($rFile)) {
				$sBuffer = fgets($rFile);
				// rozdziel sekcje csv
				$aLine=preg_split("/,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/",$sBuffer);
				if(!empty($aLine[1])){
					// usun puste znaki z tytulu
	       	$sTitle = preg_replace("/\s/","",$aLine[1]);
	       	$sOrderNr = '';
	       	// dopasuj nr zamowienia profitu
	       	if(preg_match("/[0-9]{5}\/[0-9]{4}\/[0-9]{2}\/PR24/",$sTitle,$aMatch)){
	       		$sOrderNr = $aMatch[0];
	       		// id zamowienia
	       		$iNr = $this->matchOrderNumber($sOrderNr);
	       	}
	       	// usun " i ' z ceny
	       	$sPrice = preg_replace("/[\"\\'\s]/","",$aLine[2]);
	       	$fPrice = Common::formatPrice2($sPrice);
	       	$aLine[1] = preg_replace("/[\"\\']/","",$aLine[1]);

	       	$aValues = array(
	       		'title' => iconv("CP1250","UTF-8", $aLine[1]),
	       		'order_nr' => $sOrderNr,
	       		'matched_order' => $iNr>0?$iNr:'NULL',
	       		'paid_amount' => Common::formatPrice2($sPrice),
	       		'created' => 'NOW()',
	       		'created_by' => $_SESSION['user']['name']
	       	);
	       	if(Common::Insert($aConfig['tabls']['prefix']."orders_bank_statements",$aValues) === false){
	       		$bIsErr = true;
	       	}
	       }
			}
			fclose($rFile);
		}
		else {
			$bIsErr = true;
		}
		//echo memory_get_peak_usage()."<br />";
		//echo memory_get_peak_usage(true);
		if (!$bIsErr) {
			// dodano adresy
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok']);
			$this->sMsg .= GetMessage($sMsg, false);
			// dodanie informacji do logow

			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// nie dodano adresow
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err']);
			$this->sMsg .= GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->Add($pSmarty, $iPId);
		}
	} // end of Insert() funciton
	 */


	/**
	 * Metoda tworzy formularz dodawania subskrybentow
	 *
	 * @param		object	$pSmarty
	 * @param		string	$iPId	- ID strony
	 */
	function Add(&$pSmarty, $iPId) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_0'];

		$pForm = new FormTable('import_statements', $sHeader, array('action'=>phpSelf(), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
    $pForm->setTokenSession($_SESSION);
		$pForm->AddHidden('do', 'insert');
    $_SESSION['token'] = md5(session_id(). time());
    $pForm->AddHidden('token', $_SESSION['token']);


		// nazwa
		$pForm->AddFile('statements', $aConfig['lang'][$this->sModule]['file'],  array('style'=>'width: 350px;'));
        $pForm->AddText("max_duplicates_quantity", _('Maksymalna ilość zduplikowanych (ponowionych) przelewów'), !isset($aData['max_duplicates_quantity']) ? 3 : $aData['max_duplicates_quantity']);

		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_0']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Add() function


  /**
   * Metoda powodouje opłacenie zamówienia i powiązanie z podanym przelewem
   *
   * @param int $iStatmentId
   * @param int $iOrderId
   * @param float $fOrderPaydAmount
   * @return boolean
   */
  private function proceedPayStatmentOrder($iStatmentId, $iOrderId, $fOrderPaydAmount) {
    global $aConfig;

    // pobranie przelewu
    $aStatement = $this->getStatement($iStatmentId);

    if($aStatement['paid_amount'] != $fOrderPaydAmount){
      $aStatement['paid_amount'] = $fOrderPaydAmount;
    }

//    if ($aStatement['paid_amount'] <= 0.00) {
//      $sMsg = sprintf($aConfig['lang'][$this->sModule]['match_err_1'], $iOrderId);
//      throw new Exception($sMsg);
//    }


    $aOrder = $this->getOrderInternalStatus($iOrderId);
    $sSql = "SELECT created
             FROM ".$aConfig['tabls']['prefix']."orders_bank_statements
             WHERE id = ".$iStatmentId;


    $aValues = array(
      'paid_amount' => $aOrder['paid_amount']+$aStatement['paid_amount'],
    );

    // najpierw należy sprawdzic którą metodą płatności powinniśmy sie zajmować
    // domyślnie pierwszy
    $bFirst = true;

    if ( $aOrder['payment_type'] != 'bank_transfer' && $aOrder['payment_type'] != 'bank_14days' && ($aOrder['payment_status'] == '0' || $aOrder['payment_status'] == '2')) {
      // zmiana pierwszej metody płatnosci
      $bFirst = true;
      $aValues['payment_type'] = 'bank_transfer';
      $aValues['payment_id']=$this->getBankTransferIdByTransportId('bank_transfer', $aOrder['transport_id']); //$aOrder['transport_id']==2?'5':'1';
      $aValues['payment']=$aConfig['lang']['m_zamowienia']['ptype_bank_transfer'];

    }
    elseif (($aOrder['payment_type'] == 'bank_transfer' || $aOrder['payment_type'] == 'bank_14days')) {// && ($aOrder['payment_status'] == '0' || $aOrder['payment_status'] == '2')
      // aktualizacja jedynie dat, metoda płatności jest ok
      $bFirst = true;
    }
    elseif ($aOrder['payment_status'] == '1' && $aOrder['second_payment_type'] == '') {
      // dodawanie drugiej metody płatnosci
      $bFirst = false;
      $aValues['second_payment_type'] = 'bank_transfer';
      $aValues['second_payment_id']=$this->getBankTransferIdByTransportId('bank_transfer', $aOrder['transport_id']); //$aOrder['transport_id']==2?'5':'1';
      $aValues['second_payment']=$aConfig['lang']['m_zamowienia']['ptype_bank_transfer'];
    }
    elseif ($aOrder['payment_status'] == '1' && $aOrder['second_payment_type'] != '' && $aOrder['second_payment_type'] != 'bank_transfer' && $aOrder['payment_type'] != 'bank_14days') {
      // zmiana drugiej metody płatnosci
      $bFirst = false;
      $aValues['second_payment_type'] = 'bank_transfer';
      $aValues['second_payment_id']=$this->getBankTransferIdByTransportId('bank_transfer', $aOrder['transport_id']); //$aOrder['transport_id']==2?'5':'1';
      $aValues['second_payment']=$aConfig['lang']['m_zamowienia']['ptype_bank_transfer'];
    }
    elseif ($aOrder['payment_status'] == '1' && $aOrder['second_payment_type'] != '' && ($aOrder['second_payment_type'] == 'bank_transfer' || $aOrder['payment_type'] == 'bank_14days')) {
      // nie potrzeba zmieniać metody płatności ale płacimy 2 metodą płatności
      $bFirst = false;
    }


    if ($bFirst == true) {
      $aValues['payment_status'] = '1';
      $aValues['payment_date'] = Common::GetOne($sSql);
    } else {
      $aValues['second_payment_enabled'] = '1';
      $aValues['second_payment_status'] = '1';
      $aValues['second_payment_date'] = Common::GetOne($sSql);
    }
    // wg mnie to zawsze powinno sie wykonać
    $aValues['paid_amount'] = $aOrder['paid_amount'] + $aStatement['paid_amount'];

    //  TODO RESZTE SPRAWDZIĆ I PRZETESTOWAĆ
    $aStateVal['finished']='1';
    $aStateVal['deleted']='0';
    if(Common::Update($aConfig['tabls']['prefix']."orders_bank_statements", $aStateVal, "id = ".$iStatmentId) === false){
        throw new Exception(_('Wystąpił błąd podczas zmiany statusu przelewu ERR_CODE: 0-'.$iStatmentId));
    }

    /*
    if(!empty($aOrder)){
      if($aOrder['internal_status'] == '0'){
        $aValues['internal_status'] = '1'; // nowe
      }
      elseif($aOrder['internal_status'] == '4'){
        if($aValues['paid_amount'] >= $aOrder['to_pay']){
          if($aOrder['personal_reception'] == '1'){
            $aValues['internal_status'] = '6'; // odbiór osobisty
          } else {
            $aValues['internal_status'] = '5'; // wysyłka
          }
        }
      }
    }
    */

    // aktualizacja statusów także dla składowych zamówienia
    if ($aValues['second_payment_id'] > 0) {
      $aFirstPayment = array();
      $aFirstPayment['ptype'] = $aValues['second_payment_type'];
      $aFirstPayment['id'] = $aValues['second_payment_id'];
      if ($this->updatePaymentItems($aFirstPayment, $iOrderId, true) === false) {
        throw new Exception(_('Wystąpił błąd podczas zmiany płatności składowych zamówienia ERR_CODE: 1-'.$iOrderId));
      }
    }
    if ($aValues['payment_id'] > 0) {
      $aSecondPayment = array();
      $aSecondPayment['ptype'] = $aValues['payment_type'];
      $aSecondPayment['id'] = $aValues['payment_id'];
      if ($this->updatePaymentItems($aSecondPayment, $iOrderId, false) === false) {
        throw new Exception(_('Wystąpił błąd podczas zmiany płatności składowych zamówienia ERR_CODE: 2-'.$iOrderId));
      }
    }

    /***  /statusy wewnętrzne   ***/
    // aktualizacja zamowienia
    if(Common::Update($aConfig['tabls']['prefix']."orders", $aValues, "id = ".$iOrderId) === false){
      throw new Exception(_('Wystąpił błąd podczas zmiany płatności w zamówieniu ERR_CODE: 3-'.$iOrderId));
    }

    include_once('OrderRecount.class.php');
    $oOrderRecount = new OrderRecount();
    if($oOrderRecount->recountOrder($iOrderId, false, false) === false){
      throw new Exception(_('Wystąpił błąd podczas przeliczania zamówienia ERR_CODE: 4-'.$iOrderId));
    }

    $aOldStatement = $aStatement;
    $sSql = "SELECT *
      FROM ".$aConfig['tabls']['prefix']."orders_bank_statements
      WHERE id = ".$iStatmentId;
    $aNewStatement = Common::getRow($sSql);

    $sSql = "SELECT *
      FROM ".$aConfig['tabls']['prefix']."orders
      WHERE id = ".$iOrderId;
    $aNowOrder = Common::getRow($sSql);

    //dodanie informacji o platnosci do zamowienia
    $aPaymentValues=array(
      'order_id'=>$iOrderId,
      'statement_id' => $iStatmentId,
      'date'=>$aNewStatement['created'],
      'ammount'=>$aOldStatement['paid_amount'],
      'type' => ($bFirst == true ? $aNowOrder['payment_type'] : $aNowOrder['second_payment_type']),
      'iban'=>$aNewStatement['iban'],
      'reference'=>$aNewStatement['reference'],
      'title'=>$aNewStatement['title'],
      'matched_by' => $_SESSION['user']['name']
    );
    if(Common::Insert($aConfig['tabls']['prefix']."orders_payments_list", $aPaymentValues,'', false) === false){
        throw new Exception(_('Wystąpił zmiany podczas aktualizacji listy przelewów w zamówieniu ERR_CODE: 4-'.$iOrderId));
      }
  }// end of proceedPayStatmentOrder method


    /**
     * @param $orderId
     * @param $paidAmount
     * @return bool
     */
    private function matchReturnPayment($orderId, $paidAmount) {

        $sSql = 'SELECT id 
                 FROM orders_payment_returns 
                 WHERE 
                  order_id = "'.$orderId.'" AND 
                  balance = "'.$paidAmount.'" AND 
                  submitted = "1" AND 
                  type = "'.OrdersPaymentReturns::TYPE_RENOUNCEMENT.'" AND
                  is_statement_return = "1" AND
                  active = "1" ';
        $iPaymentReturnId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($iPaymentReturnId > 0) {
            $values = [
                'active' => '0'
            ];
            if (false === $this->pDbMgr->Update('profit24', 'orders_payment_returns', $values, ' id='.$iPaymentReturnId)){
                return false;
            }
        }
        return true;
    }


  /**
   * Metoda dopasowuje zamówienia
   *
   * @global array $aConfig
   * @param object $pSmarty
   * @return boolean
   */
	function matchOrders($pSmarty){
		global $aConfig;
		$bIsErr = false;
			if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Show($pSmarty);
			return;
		}
		if(!empty($_POST['editable'])){
			foreach($_POST['editable'] as $sKey=>&$aElement){
				if(isset($_POST['order'][$sKey]) && $_POST['order'][$sKey] > 0.00){
          $aElement['paidamount'] = Common::FormatPrice2(trim($aElement['paidamount']));
          if ($aElement['paidamount'] !== "" && (!preg_match('/^(?=\d|[\.,]\d)\d*([\.,])?(\d*)$/',$aElement['paidamount']) || $aElement['paidamount'] <= 0.00)) {
              $sErr .= "<li>Zapłacona kwota</li>";
          }
				}
			}
			if(!empty($sErr)){
				$sErr = $aConfig['lang']['form']['error_prefix'].
					'<ul class="formError">'.$sErr.'</ul>'.
					$aConfig['lang']['form']['error_postfix'];
				$this->sMsg .= GetMessage($sErr, true);
				// wystapil blad wypelnienia formularza
				// wyswietlenie formularza wraz z komunikatem bledu
				$this->Show($pSmarty);
				return;
			}
		}
		$aIdsToSend = array();
    // rozpoczecie transkacji
    Common::BeginTransaction();
		if(!empty($_POST['order'])){
			foreach($_POST['order'] as $iStatment => $iOrderId){
				if(!empty($iOrderId)){
					if(!$bIsErr){
            $aStatment = $this->getStatement($iStatment);
            $fOrderPaydAmount = $aStatment['paid_amount'];
//            $fOrderPaydAmount = Common::FormatPrice2($fStatmentPaidAmount);
            // pobranie numeru dobranego zamowienia
            $sNumbers .= $this->getOrderNumber($iOrderId).', ';
            // pobraenie wczesniejszej metody płatności
            $sOldPaymentMethod = $this->getOldPaymentMethod($iOrderId);
            try {
              $this->proceedPayStatmentOrder($iStatment, $iOrderId, $fOrderPaydAmount);
            } catch (Exception $exc) {
              Common::RollbackTransaction();
              $this->sMsg .= GetMessage($exc->getMessage());
              AddLog($exc->getMessage());
              $this->Show($pSmarty);
              return false;
            }
            if($sOldPaymentMethod != 'bank_14days'){
              $aIdsToSend[] = $iOrderId;
            }
          }
        }
      }
      if(!empty($sNumbers)){
        $sNumbers = substr($sNumbers,0,-2);
      }

      if (!$bIsErr) {
        // dodano adresy
        if(!empty($sNumbers)){
          $sMsg = sprintf($aConfig['lang'][$this->sModule]['match_ok'],$sNumbers);
        } else {
          $sMsg = $aConfig['lang'][$this->sModule]['match_empty'];
        }
        $this->sMsg .= GetMessage($sMsg, false);
        // dodanie informacji do logow
        Common::CommitTransaction();

        if(!empty($aIdsToSend)){
          foreach($aIdsToSend as $iId){
            $this->sendOrderPayedMail($iId);
          }
        }

        AddLog($sMsg, false);
        $this->Show($pSmarty);
      }
      else {
        // nie dodano adresow
        // wyswietlenie komunikatu o niepowodzeniu
        // oraz ponowne wyswietlenie formularza dodawania
        $sMsg = sprintf($aConfig['lang'][$this->sModule]['match_err'],$sNumbers);
        $this->sMsg .= GetMessage($sMsg);
        Common::RollbackTransaction();

        AddLog($sMsg);
        $this->Add($pSmarty);
      }
		} else {
			$this->Show($pSmarty);
		}
	} // end of () method

	/**
	 * Metoda pobiera id typu płatności
	 *
	 * @global type $aConfig
	 * @param type $sPType
	 * @param type $iTransportId
	 * @return type
	 */
	function getBankTransferIdByTransportId($sPType, $iTransportId) {
		global $aConfig;

		$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders_payment_types
						 WHERE 	transport_mean=".$iTransportId." AND ptype='".$sPType."'";
		return Common::GetOne($sSql);
	}// end of getBankTransferIdByTransportId() method


  /**
   * @param Smarty $pSmarty
	 */
	public function toApproved($pSmarty){
		global $aConfig;
		$bIsErr = false;
			if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Show($pSmarty);
			return;
		}
		Common::BeginTransaction();

		foreach($_POST['delete'] as $iKey => $iItem) {
			//dla kazdego zaznaczonego elementu
			if($iItem==1) {//jeżeli zaznaczone
                if ($this->changeStatmentToApproved($iKey) == false) {
                  $bIsErr = true;
                }
                if ($this->compareWithRenounecement($iKey) == false) {
                    $bIsErr = true;
                }
			}
		}

		if (!$bIsErr) {
			// dodano adresy
			$sMsg = _('Zaksięgowano wybrane przelewy');
			$this->sMsg .= GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();

			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// nie dodano adresow
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = _('Błąd księgowania przelewów');
			$this->sMsg .= GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->Show($pSmarty);
		}
	} // end of toOpek() method


    /**
     * @param $statementId
     * @return bool
     */
    private function compareWithRenounecement($statementId) {

        $sSql = 'SELECT OBS.paid_amount, OBS.title, OBS.order_nr
                 FROM orders_bank_statements AS OBS
                 WHERE id = '.$statementId;
        $ordersBankStatements = $this->pDbMgr->GetRow('profit24', $sSql);

        // niedopłata
        // nr zamówienia
        // valid z odstąpieniem
        if (!empty($ordersBankStatements) && $ordersBankStatements['paid_amount'] < 0.00 && '' != $ordersBankStatements['order_nr']) {
            $orderId = $this->getRennounecementOrderId($ordersBankStatements['order_nr']);

            if (false === $this->matchReturnPayment($orderId, $ordersBankStatements['paid_amount'])) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $orderNumber
     * @return mixed
     */
    private function getRennounecementOrderId($orderNumber) {
        $sSql = 'SELECT O.id 
                 FROM orders AS O
                 JOIN orders_payment_returns AS OPR
                  ON O.id = OPR.order_id
                 WHERE O.order_number = "'.$orderNumber.'"
                   AND OPR.submitted = "1"
                   AND OPR.active = "1" ';
        $orderId = $this->pDbMgr->GetOne('profit24', $sSql);
        return $orderId;
    }


	/**
	 * Metoda oznacza wybrane przelewy jako przelewy z opek co je przenosi do zakladki Przelewy Pobrania
	 */
	function toOpek($pSmarty){
		global $aConfig;
		$bIsErr = false;
			if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Show($pSmarty);
			return;
		}
		Common::BeginTransaction();

		foreach($_POST['delete'] as $iKey => $iItem) {
			//dla kazdego zaznaczonego elementu
			if($iItem==1) {//jeżeli zaznaczone
        if ($this->changeStatmentToPostalFee($iKey) == false) {
          $bIsErr = true;
        }
			}
		}

		if (!$bIsErr) {
			// dodano adresy
			$sMsg = $aConfig['lang'][$this->sModule]['saved_items'];
			$this->sMsg .= GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();

			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// nie dodano adresow
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = $aConfig['lang'][$this->sModule]['saved_error'];
			$this->sMsg .= GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->Show($pSmarty);
		}
	} // end of toOpek() method

  /**
   *
   * @param int $iStatementId
   */
  private function changeStatmentToApproved($iStatementId) {

    $aValues = array(
        'approved' => '1'
    );
    return $this->pDbMgr->Update('profit24', 'orders_bank_statements', $aValues, ' id = '.$iStatementId);
  }


  /**
   * @param int $iOId
   */
  private function changeStatmentToPostalFee($iOId) {
    global $aConfig;

    $aValues = array(
        'is_opek' => '1',
        'deleted' => '0'
    );
    if(Common::Update($aConfig['tabls']['prefix']."orders_bank_statements", $aValues, "id = ".$iOId) === false){
      return false;
    }
    // dodanie numerów listów przewozowych z tytułu przelewu opek
    if ($this->addLinkingTransportNumbers($iOId) === false) {
      return false;
    }
    return true;
  }// end of _changeStatmentToOpek() method


	/**
	 * Metoda dodaje do tabeli
	 *
	 * @global type $aConfig
	 * @param type $iId
	 * @return type
	 */
	function addLinkingTransportNumbers($iId) {
		global $aConfig;

		// pobranie tytułu przelewu
		$sSql = "SELECT title FROM ".$aConfig['tabls']['prefix']."orders_bank_statements
						 WHERE id=".$iId;
		$sTitle = Common::GetOne($sSql);
		$aTransportNumers = $this->getTransportNumbers($sTitle);

		foreach ($aTransportNumers as $sTransportNumber) {
			$aValues = array(
					'orders_bank_statement_id' => $iId,
					'transport_number' => $sTransportNumber
			);
			if (Common::Insert($aConfig['tabls']['prefix']."orders_linking_transport_numbers", $aValues) === false) {
				return false;
				// uciekamy reszta zostanie posprzątana w transakcji w metodzie nadrzędnej
			}
		}

		return true;
	} // end addLinkingTransportNumbers() method


	/**
	 * Metoda pobiera przyjmuje tytuł przelewu
	 *  intrepretuje i zwraca tablicę numerów transportu
	 *
	 * @param string $sTitle
	 * @return array
	 */
	function getTransportNumbers($sTitle) {
		$aTransportNumers = array();
		$iStart=0;

		$sListaTmp = preg_replace('[ ]', '', $sTitle);
		if(($iPos = strpos($sListaTmp, 'REF'))>0)
			$sListaTmp = substr($sListaTmp, 0, $iPos);

		$sListaTmp = preg_replace('[a-zA-Z]', '', $sListaTmp);
		$iCountL = floor(strlen($sListaTmp)/8);
		for($i=0; $i<$iCountL; ++$i){
			$sItem=mb_substr($sListaTmp, $iStart, 8, 'UTF-8');
			if(strlen($sItem)==8)
				$aTransportNumers[]=$sItem;
			else break;
			$iStart+=8;
		}

		return $aTransportNumers;
	}// end of getTransportNumbers() method


	/**
	 * Metoda pobiera aktualną metodę płatności
	 *
	 * @global type $aConfig
	 * @return string
	 */
	function getOldPaymentMethod($iId) {
		global $aConfig;

		$sSql = "SELECT IF(second_payment_enabled='1', second_payment_type, payment_type) AS payment_type
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	}// end of getOldPaymentMethod() method


	/**
	 * Pobiera dane dla statusu wewn
	 * @param $iId - id zamówienia
	 * @return array
	 */
	function getOrderInternalStatus($iId){
		global $aConfig;
		$sSql = "SELECT A.internal_status, A.payment_status, A.payment, A.payment_type, A.payment_id, A.value_brutto,
										A.transport_cost, B.personal_reception, A.to_pay, A.paid_amount, A.payment_date, A.transport_id, A.second_payment_type,
										A.second_payment_status, A.payment_status
						FROM ".$aConfig['tabls']['prefix']."orders A
						LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
						ON B.id=A.transport_id
						WHERE A.id = ".$iId;
		return Common::GetRow($sSql);
	} // end of getOrderInternalStatus() method


	/**
	 * Metoda pobiera liste subskrybentow do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 *
	 * @param	array ref	$aIds	- Id subskrybentow
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, title
				 		 FROM ".$aConfig['tabls']['prefix']."orders_bank_statements
					 	 WHERE id IN (".implode(',', array_keys($aIds)).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method


	/**
	 * Metoda usuwa odbiorce newslettera
	 *
	 * @param	integer	$iId	- Id odbiorcy do usuniecia
	 * @return	mixed
	 */
	function deleteItem($iId) {
		global $aConfig;
		$aValues['deleted']='1';
		return Common::Update($aConfig['tabls']['prefix']."orders_bank_statements", $aValues, "id = ".$iId);

	} // end of deleteItem() method

    /**
     * Metoda usuwa odbiorce newslettera
     *
     * @param	integer	$iId	- Id odbiorcy do usuniecia
     * @return	mixed
     */
    function deleteItemUpdateBalance($iId) {
        global $aConfig;

        $sSql = 'SELECT * FROM orders_bank_statements AS OBS WHERE id = '.$iId.' AND deleted = "0" ';
        $statementData = $this->pDbMgr->GetRow('profit24', $sSql);
        if (!empty($statementData)) {
            if (false === $this->updateAccountsByIdent($statementData['ident_statments_file'], $statementData['paid_amount']) ) {
                return false;
            }

            $aValues = ['deleted' => '1'];
            return Common::Update($aConfig['tabls']['prefix']."orders_bank_statements", $aValues, "id = ".$iId);
        } else {
            return false;
        }
    } // end of deleteItem() method


    /**
     * @param $bankIdent
     * @param $paidAmount
     * @return bool
     */
    public function updateAccountsByIdent($bankIdent, $paidAmount) {

        if ($paidAmount > 0) {
            $sSql = 'UPDATE bank_accounts
                 SET balance = balance - '.$paidAmount.'
                 WHERE ident_statments_file = "'.$bankIdent.'"';
        } else {
            $sSql = 'UPDATE bank_accounts
                 SET balance = balance + '.abs($paidAmount).'
                 WHERE ident_statments_file = "'.$bankIdent.'"';

        }


        return ($this->pDbMgr->Query('profit24', $sSql) === false ? false : true );
    }



	/**
	 * Pobiera id zamówienia dla danego numeru
	 * @param string $sNr - nr zamowienia
	 * @return in - id zamowienia
	 */
	function matchOrderNumber($sNr){
		global $aConfig;
		$sSql = "SELECT id
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE order_number = ".Common::Quote($sNr);
		return intval(Common::GetOne($sSql));
	}
	/*
	 Zapytanie z przelew 14 dni:

		SELECT id AS value, CONCAT(order_number, ' ', to_pay-paid_amount, 'zł') AS label
		FROM ".$aConfig['tabls']['prefix']."orders
		WHERE
			(
				payment_type = 'bank_transfer'
				OR payment_type = 'card'
				OR payment_type = 'bank_14days'
				OR payment_type = 'platnoscipl'
				OR
				IF (
					second_payment_enabled = '1',
					second_payment_type = 'bank_transfer'
					OR second_payment_type = 'card'
					OR second_payment_type = 'bank_14days'
					OR second_payment_type = 'platnoscipl',
					1=2
				)
			)
			AND (
				(
					order_status != '4'
					OR payment_type = 'bank_14days'
					OR second_payment_type = 'bank_14days'
				)
				AND (order_status != '5' OR DATEDIFF(NOW(), order_date) <= 7)
			)
			AND (
				payment_status = '0'
				OR payment_status = '2'
				OR
				IF (
					second_payment_enabled = '1',
					second_payment_status = '0' OR second_payment_status = '2',
					1=2
				)
				OR paid_amount < to_pay
			)
	 *
	 */
/**
	 * Pobiera zamówienia dla dopasowania
   *
   * @see 06.05.2014 - wyłączenie matchowania zamówień, które są anuliwane - AND (order_status != '5' OR DATEDIFF(NOW(), order_date) <= 7)
	 * @return array - zamowienia
	 */
	function findOrders(){
		global $aConfig;

		$sSql = "SELECT id AS value, CONCAT(order_number, ' ', to_pay-paid_amount, 'zł') AS label
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE 
							(
								payment_type != 'postal_fee' 
								OR 
								IF ( 
									second_payment_enabled = '1',
									second_payment_type != 'postal_fee',
									1=2
								)
							)
							AND (
								(
									order_status != '4' 
                  AND order_status != '5'
								)
							)
							AND (
								payment_status = '0' 
								OR payment_status = '2'
								OR
								IF (
									second_payment_enabled = '1',
									second_payment_status = '0' OR second_payment_status = '2',
									1=2
								)
								OR paid_amount < to_pay 
							)
							AND correction != '1'
              ORDER BY to_pay-paid_amount DESC
							";
		return array_merge(array(array('value'=>'','label'=>$aConfig['lang']['common']['choose'])),Common::GetAll($sSql));
	}

/**
	 * Pobiera zamówienia dla js
	 * @return array - zamowienia
	 */
	function findOrdersJS(){
		global $aConfig;
		$sSql = "SELECT id, to_pay-paid_amount AS to_pay2
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE 
							(
								payment_type != 'postal_fee' 
								OR 
								IF ( 
									second_payment_enabled = '1',
									second_payment_type != 'postal_fee',
									1=2
								)
							)
							AND (
									order_status != '4' 
                  AND order_status != '5'
							)
							AND (
								payment_status = '0' 
								OR payment_status = '2'
								OR
								IF (
									second_payment_enabled = '1',
									second_payment_status = '0' OR second_payment_status = '2',
									1=2
								)
								OR paid_amount < to_pay 
							)
							AND correction != '1'
							";
		return Common::GetAll($sSql);
	}
	/**
	 * Pobiera dane przelewu
	 * @param int $iId - id przelewu
	 * @return array - dane przelewu
	 */
	function getStatement($iId){
		global $aConfig;

		$sSql = "SELECT id, paid_amount
						FROM ".$aConfig['tabls']['prefix']."orders_bank_statements
						WHERE id = ".$iId;
		return Common::GetRow($sSql);
	}

	/**
	 * Pobiera numer zamówienia
	 * @param int $iId - id zamówienia
	 * @return string - nr zamówienia
	 */
	function getOrderNumber($iId){
		global $aConfig;
		$sSql = "SELECT order_number
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id = ".$iId;
		return Common::GetOne($sSql);
	}


	/**
	 * Pobiera typ płatności
	 * @param int $iId - id zamówienia
	 * @return string - nr zamówienia
	 */
	function getPaymentType($iId){
		global $aConfig;
		$sSql = "SELECT payment_type
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id = ".$iId;
		return Common::GetOne($sSql);
	}


	/**
	 * Pobiera typ płatności
	 * @param int $iId - id zamówienia
	 * @return string - nr zamówienia
	 */
	function getSecondPaymentType($iId){
		global $aConfig;
		$sSql = "SELECT second_payment_type
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id = ".$iId;
		return Common::GetOne($sSql);
	}


  /**
   * Metoda wysyła emaila potwierdzającego z informacja o opłaceniu zamówienia
   *
   * @global type $aConfig
   * @global type $pSmarty
   * @param type $iId
   * @return type
   */
	function sendOrderPayedMail($iId) {
		global $aConfig, $pSmarty;

		$sWebsite = $this->getWebsiteSymbol($this->getWebsiteId($iId, false));
		setWebsiteSettings(1, $sWebsite);

	 	$aModule['lang'] =& $aConfig['lang']['m_zamowienia_mail'];

	 	$_GET['lang_id']=1;
		$aSiteSettings = getSiteSettings($sWebsite);
	 	$sFrom = $aConfig['default']['website_name'];
		$sFromMail = $aSiteSettings['orders_email'];

		$sSql = "SELECT *, (transport_cost + value_brutto) as total_value_brutto, DATE_FORMAT(order_date, '".$aConfig['common']['sql_date_hour_format']."') as order_date, DATE_FORMAT(status_1_update, '".$aConfig['common']['sql_date_hour_format']."') as status_1_update, DATE_FORMAT(status_2_update, '".$aConfig['common']['sql_date_hour_format']."') as status_2_update, DATE_FORMAT(status_3_update, '".$aConfig['common']['sql_date_hour_format']."') as status_3_update
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE id = ".$iId;
		$aModule['order'] =& Common::GetRow($sSql);
		$aVars = array('{nr_zamowienia}','{data_statusu}','{kwota}');
		$aValues = array($aModule['order']['order_number'],$aModule['order']['payment_date'],Common::formatPrice($aModule['order']['paid_amount']));

		$aMail = Common::getWebsiteMail('status_oplacone', $sWebsite);
		return Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,$aModule['order']['email'],  $aMail['content'],$aVars,$aValues,array(), array(), $sWebsite);
	} // end of sendUserPasswordEmail() method


	/**
   * Jeśli w zamówieniu zmieniła się metoda płatności aktualizacja także w składowych zamówienia
   *
   * @global array $aConfig
   * @param array $aPayment
   * @param int $iOrderId
   * @param bool $bSecondPayment
   * @return boolean
   */
	function updatePaymentItems($aPayment, $iOrderId, $bSecondPayment=true) {
		global $aConfig;

		$aValues = array();
		if ($aPayment['ptype'] != '' && $aPayment['id'] != '') {
			$aValues['payment_type']=$aPayment['ptype'];
			$aValues['payment_id']=	$aPayment['id'];
		} else {
			// nic nie robimy
			return true;
		}

		return Common::Update($aConfig['tabls']['prefix']."orders_items", $aValues, ($bSecondPayment == true ? " second_payment='1'" : " second_payment='0'")." AND order_id=".$iOrderId);
	} // end of updatePaymentItems() method


	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type
	 */
	function getWebsiteSymbol($iWebsiteId) {
		global $aConfig;

		$sSql = "SELECT code FROM ".$aConfig['tabls']['prefix']."websites
						 WHERE id=".$iWebsiteId;
		return Common::GetOne($sSql);
	}// end of getWebsiteSymbol() method


	/**
	 * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
	 *
	 * @global type $aConfig
	 * @param type $iOId
	 * @return type
	 */
	function getWebsiteId($iOId) {
		global $aConfig;

		$sSql = "SELECT website_id FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id=".$iOId;
		return Common::GetOne($sSql);
	}
} // end of Module Class
