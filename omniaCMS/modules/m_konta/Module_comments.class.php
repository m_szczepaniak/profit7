<?php
/**
 * Klasa zarządzania komentarzami użytkowników
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */
include_once('Konta_common.class.php');
class Module extends Konta_common { //Module__konta__comments
  
  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// Id strony modulu
	var $iPageId;
  
  // id uzytkownika
  var $iUId;

  function __construct(&$pSmarty, $bUseAsSimpleClass = false) {
//    if ($bUseAsSimpleClass) return;
    
		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];
		$this->iPageId = getModulePageId('m_konta');
    
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
    $sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
    
		if (isset($_GET['pid'])) {
			$this->iUId = $_GET['pid'];
		}
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    switch ($sDo) {
      case 'edit':
        $this->Edit($pSmarty, $iId);
      break;
      case 'add':
        $this->Add($pSmarty, $iId);
      break;
      case 'insert':
        $this->Insert($pSmarty, $iId);
      break;
      default: $this->Show($pSmarty); break;
    }
  }// end of __construct() method
  
  
  /**
   * Metoda wyświetla listę komentarzy
   * 
   * @global type $aConfig
   * @param type $pSmarty
   */
  public function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');
    
    // zapamietanie opcji
		rememberViewState($this->sModule);
    
		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'], $this->getUserEmail($this->iUId)),
			'refresh'	=> true,
			'search'	=> false,
      'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
      array(
        'db_field'	=> 'comment',
        'content'	=> $aConfig['lang'][$this->sModule]['comment'],
        'sortable'	=> true,
      ),
      array(
        'db_field'	=> 'created',
        'content'	=> $aConfig['lang']['common']['created'],
        'sortable'	=> true,
        'width'	=> '180'
      ),
      array(
        'db_field'	=> 'created_by',
        'content'	=> $aConfig['lang']['common']['created_by'],
        'sortable'	=> true,
        'width'	=> '180'
      )
    );
    // pobranie liczby wszystkich uzytkownikow
    $sSql = "SELECT COUNT(id)
             FROM ".$aConfig['tabls']['prefix']."users_accounts_comments
             WHERE 1 = 1 AND 
                   user_id = ".$this->iUId;
    $iRowCount = intval(Common::GetOne($sSql));
    if ($iRowCount == 0 && !isset($_GET['reset'])) {
      // resetowanie widoku
      resetViewState($this->sModule);
      // ponowne okreslenie liczny rekordow
      $sSql = "SELECT COUNT(id)
               FROM ".$aConfig['tabls']['prefix']."users_accounts_comments
               WHERE user_id = ".$this->iUId;
      $iRowCount = intval(Common::GetOne($sSql));
    }
        
    $pView = new View('users', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
    
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
      
      // pobranie wszystkich rekordow
			$sSql = "SELECT comment, created, created_by
               FROM users_accounts_comments
               WHERE user_id = ".$this->iUId.
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
      foreach ($aRecords as $iKey => $aRecord) {
        $aRecords[$iKey]['comment'] = nl2br($aRecord['comment']);
      }
			// dodanie rekordow do widoku
      $aColSettings = array();
			$pView->AddRecords($aRecords, $aColSettings);
    }
    
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('go_back'),
			array('add')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		);

		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
  }// end of Show() method
  
  
  /**
   * Metoda tworzy formularz dodawania nowego komentarza
   * 
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iId
   */
  public function Add(&$pSmarty, $iId) {
    global $aConfig;
    
		$aData = array();
		$aLang = $aConfig['lang'][$this->sModule];
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
    
    if (!empty($_POST)) {
      $aData = $_POST;
    } else {
      if ($iId > 0) {
        $aData = $this->_getUserAddressData($this->iUId, $iId);
      }
    }
    
    $sHeader = sprintf($aConfig['lang'][$this->sModule]['header'], $this->getUserEmail($this->iUId));
		
		$pForm = new FormTable('user_comments', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), $aConfig['common']['js_validation']);
    $pForm->AddHidden('do', 'insert');
    
    $pForm->AddTextArea('comment', $aLang['comment'], $aData['comment'], array('style' => 'width: 600px; height: 120px;'), 65535);
    
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		
    
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$sJS.ShowTable($pForm->ShowForm()));
  } // end of Add() method
  
  
  /**
   * Metoda dodaje nowy komentarz
   * 
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iId
   */
  public function Insert($pSmarty, $iId) {
    global $aConfig;
    
    $sSql = "SELECT name, surname
							FROM ".$aConfig['tabls']['prefix']."users
							 WHERE id = ".intval($_SESSION['user']['id']);
		$aGeneratorName=Common::GetRow($sSql);
    
    $aValues = array(
        'user_id' => $this->iUId,
        'comment' => $_POST['comment'],
        'created' => 'NOW()',
        'created_by' => $aGeneratorName['name'].' '.$aGeneratorName['surname'],
    );
    
    if (Common::Insert("users_accounts_comments", $aValues)) {
      // rekord zostal zmieniony
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
                      $this->getUserEmail($this->iUId));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
    } else {
			// rekord nie zostal zmieniony
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
                      $this->getUserEmail($this->iUId));
			$this->sMsg = GetMessage($sMsg);
      // dodanie informacji do logow
			AddLog($sMsg);
			$this->Edit($pSmarty, $iId);
    }
  }// end of Insert() method
}// end of class
?>