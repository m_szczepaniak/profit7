<?php
/**
 * Klasa Module do obslugi modulu 'Konta uzytkownikow' - wersja zaadaptowana
 * na potrzeby projektu ksiegarnia.beck.pl
 * Dane uzytkownikow przechowywane w bazie $aConfig['db2']['name']
 * w tabeli $aConfig['tabls']['users_accounts'] - koniecznosc
 * polaczenia z druga baza danych
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2008
 * @version   1.0
 *
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// Id strony modulu
	var $iPageId;
	
	// katalog plikow log
	var $sLogsDir;


  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig, $pDbMgr;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];
		$this->iPageId = getModulePageId('m_konta');
		$this->sLogsDir = $aConfig['common']['base_path'].'logs';
        $this->pDbMgr = $pDbMgr;
		
		
		$sDo = '';
		$iId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}

		// dolaczenie klasy XML/Unserializer
		require_once ("XML/Unserializer.php");

		$aOptions = array(
			'complexType'       => 'array',
			'parseAttributes'		=> true,
			'attributesArray'		=> 'attribs',
			'forceEnum'	=> array('item')
		);
		

		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'details': $this->Details($pSmarty, $iId); break;
			case 'update_details': $this->UpdateUserDetails($pSmarty, $iId); break;
			case 'discount': $this->EditUserDiscount($pSmarty, $iId); break;
			case 'balance': $this->EditUserBalance($pSmarty, $iId); break;
			case 'update_user_discount': $this->UpdateUserDiscount($pSmarty, $iId); break;
			case 'update_user_balance': $this->UpdateUserBalance($pSmarty, $iId); break;
			case 'activate': $this->Activate($pSmarty, $iId); break;
			case 'send': $this->prepareUserMessage($pSmarty, $iId); break;
			case 'proceed_user_send': $this->sendUserMessage($pSmarty, $iId); break;
			case 'show_users': $this->EditGroupDiscount($pSmarty, true); break;
			case 'send_group_discount_info': $this->prepareGroupMessage($pSmarty); break;
			case 'proceed_group_send': $this->sendGroupMessage($pSmarty); break;
			case 'group_discount': $this->EditGroupDiscount($pSmarty); break;
			case 'update_group_discount': $this->UpdateGroupDiscount($pSmarty); break;
      case 'change_email': $this->changeUserEmail($pSmarty, $iId);break;
      case 'change_seller_id': $this->changeSellerId($pSmarty, $iId); break;
      case 'connect_accounts': $this->connectAccounts($pSmarty, $iId, $iPId); break;
      
			default: $this->Show($pSmarty); break;
		}
	} // end Module() function
  
  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param Smarty $pSmarty
   * @param int $iId
   * @return string html
   */
  private function changeSellerId($pSmarty, $iId) {
    global $pDbMgr;
    
    $sSellerStreamsoft = $_POST['seller_streamsoft_id'];
    $aValues = array(
        'seller_streamsoft_id' => $sSellerStreamsoft
    );
    if ($pDbMgr->Update('profit24', 'users_accounts', $aValues, ' id='.$iId) === false) {
      $this->sMsg = _('Błąd ustawiania danych handlowca');
      AddLog($this->sMsg, true);
      $this->sMsg = GetMessage($this->sMsg, true);
      return $this->Details($pSmarty, $iId);
    } else {
      $this->sMsg = _('Ustawiono dane id handlowca w streamsoft');
      AddLog($this->sMsg, false);
      $this->sMsg = GetMessage($this->sMsg, false);
      return $this->Details($pSmarty, $iId);
    }
  }
  
  /**
   * Metoda łączy konta użytkowników
   * 
   * @param object $pSmarty
   * @param int $iOldUserId
   * @param int $iNewUserId
   * @return string
   */
  public function connectAccounts($pSmarty, $iOldUserId, $iNewUserId) {

    if ($iOldUserId != $iNewUserId && $iOldUserId > 0 && $iNewUserId > 0) {
      $aOldUser = $this->getUserAttribs($iOldUserId, 'email');
      $aNewUser = $this->getUserAttribs($iNewUserId, 'email');
      
      $bIsErr = false;
      Common::BeginTransaction();
      
      // przepisujemy adresy
      $aValues = array('user_id' => $iNewUserId);
      if (Common::Update('users_accounts_address', $aValues, ' user_id = '.$iOldUserId) === FALSE) {
        $bIsErr = 1;
      }
      
      // zmieniamy adres email w zamówieniach
      $aValuesEmail = array(
          'email' => $aNewUser['email']
      );
      if (Common::Update('orders', $aValuesEmail, ' user_id = '.$iOldUserId.' AND order_status <> "4" AND order_status <> "5" ') === FALSE) {
        $bIsErr = 3;
      }
      
      // przepisujemy zamówienia
      if (Common::Update('orders', $aValues, ' user_id = '.$iOldUserId) === FALSE) {
        $bIsErr = 2;
      }
      
      // usuwamy stare konto
      $sSql = "DELETE FROM users_accounts WHERE id = ".$iOldUserId.' LIMIT 1';
      if (Common::Query($sSql) === FALSE) {
        $bIsErr = 4;
      }
      
      if ($bIsErr !== FALSE) {
        Common::RollbackTransaction();
        $this->sMsg = _('Wystąpił błąd podczas łączenia kont KOD: '.$bIsErr);
        AddLog($this->sMsg, true);
        $this->sMsg = GetMessage($this->sMsg, true);
        return $this->Show($pSmarty);
      } else {
        Common::CommitTransaction();
        $this->sMsg = _('Konto o adresie '.$aOldUser['email'].' zostało zaimportowane do '.$aNewUser['email']);
        AddLog($this->sMsg, false);
        $this->sMsg = GetMessage($this->sMsg, false);
        return $this->Show($pSmarty);
      }
    } else {
      $this->sMsg = _('Błąd weryfikacji danych kont użytkownika');
      AddLog($this->sMsg, true);
      $this->sMsg = GetMessage($this->sMsg, true);
      return $this->Show($pSmarty);
    }
  }// end of connectAccounts() method
  
  
  /**
   * Metoda wyświetla formularz zapytania, czy zamówienia mają zostać połączone
   * 
   * @global array $aConfig
   * @param object $pSmarty
   * @param int $iOldUserId
   * @param int $iNewUserId
   */
  public function connectAccountsForm($pSmarty, $iOldUserId, $iNewUserId) {
		global $aConfig;

		$aLang =& $aConfig['lang'][$this->sModule];

    $aOldUser = $this->getUserAttribs($iOldUserId, 'email');
    $aNewUser = $this->getUserAttribs($iNewUserId, 'email');
    
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
    
		$pForm = new FormTable('attributes', _('Łączenie kont użytkownika'), array('action'=>phpSelf(array('id' => $iOldUserId, 'pid' => $iNewUserId))), array('col_width'=>155), false);
		$pForm->AddHidden('do', 'connect_accounts');
    
    $pForm->AddMergedRow(_('Łączenie konta użytkownika '.$aOldUser['email'].' z '.$aNewUser['email'].' <br />
                            Zamówienia oraz adresy przypisane do konta '.$aOldUser['email'].' zostaną przeniesione do '.$aNewUser['email'].' w tych zamowieniach zmieniony zostanie adres email na nowy.<br />
                            <strong style="color: red">Konto '.$aOldUser['email'].' zostanie usunięte.</span>'), array('class' => 'merged'));
    $pForm->AddRow('',
      '<div style="float: left;">'.
        $pForm->GetInputButtonHTML('submit', _('Tak połącz !')).' &nbsp; &nbsp; '.
        $pForm->GetInputButtonHTML('cancel', _('Powrót'), array('onclick' => 'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button').
      '</div>',
      array('style', 'float: left;')
    );
    
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
  }// end of connectAccountsForm() method
  
  
  /**
   * Metoda zmienia adres email użytkownika
   * 
   * @global array $aConfig
   * @param object $pSmarty
   * @param int $iId
   * @return string - html
   */
  public function changeUserEmail($pSmarty, $iId) {
    global $aConfig;
    $aMatches = array();
    
    $sNewEmail = trim($_POST['email']);
    preg_match($aConfig['class']['form']['email']['pcre'], $sNewEmail, $aMatches);
    if (empty($aMatches)) {
      $this->sMsg = _('Wprowadzony adres email jest nieprawidłowy');
      AddLog($this->sMsg, true);
      $this->sMsg = GetMessage($this->sMsg, true);
      return $this->Details($pSmarty, $iId);
    } else {
      // sprawdźmy czy nowy adres email znajduje się w bazie w obrębie danego serwisu
      $iNewUserId = $this->_checkEmailEqualWebsite($sNewEmail, $iId);
      if ($iNewUserId > 0) {
        $this->sMsg = _('Wprowadzony adres email występuje już w naszej bazie użytkowników.<br /> Czy chcesz usunąć stare konto użytkownika, przenieść zamówienia oraz adresy użytkownika ze starego konta do nowego, oraz usunąć stare konto ?');
        AddLog($this->sMsg, false);
        $this->sMsg = GetMessage($this->sMsg, false);
        return $this->connectAccountsForm($pSmarty, $iId, $iNewUserId);
      }
      
      Common::BeginTransaction();
      // możemy aktualizować
      $aValues = array(
          'email' => $sNewEmail,
          'modified' => 'NOW()',
      );
      if (Common::Update('users_accounts', $aValues, ' id = '.$iId.' LIMIT 1') === FALSE) {
        // ERR
        Common::RollbackTransaction();
        $this->sMsg = _('Wystąpił błąd podczas zmiany adresu email użytkownika CODE: 0001');
        AddLog($this->sMsg, true);
        $this->sMsg = GetMessage($this->sMsg, true);
        return $this->Details($pSmarty, $iId);
      } else {
        
        // zmieniami email w zamówieniach, nie-zrealizowanych i nie-anulowanych
        $aValuesOrd = array(
            'email' => $sNewEmail,
        );
        if (Common::Update('orders', $aValuesOrd, ' user_id = '.$iId.' AND order_status <> "4" AND order_status <> "5" ') === FALSE) {
          // ERR
          Common::RollbackTransaction();
          $this->sMsg = _('Wystąpił błąd podczas zmiany adresu email użytkownika CODE: 0001');
          AddLog($this->sMsg, true);
          $this->sMsg = GetMessage($this->sMsg, true);
          return $this->Details($pSmarty, $iId);
        } else {
          // OKI
          Common::CommitTransaction();
          $this->sMsg = _('Adres email został zmieniony w koncie użytkownika a także w otwartych zamówieniach złożonych z Jego konta.');
          AddLog($this->sMsg, false);
          $this->sMsg = GetMessage($this->sMsg, false);
          return $this->Show($pSmarty);
        }
      } 
    }
  }// end of changeUserEmail() method
  
  
  /**
   * Metoda sprawdza czy adres email występuje w serwisie
   * 
   * @param string $sNewEmail
   */
  private function _checkEmailEqualWebsite($sNewEmail, $iId) {
    $aUser = $this->getUserAttribs($iId, 'website_id');
    
    $sSql = 'SELECT id FROM users_accounts WHERE email = "'.$sNewEmail.'" AND website_id = '.$aUser['website_id'];
    return Common::GetOne($sSql);
  }// end of _checkEmailEqualWebsite() method


	/**
	 * Metoda usuwa wybrane rekordy
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id usuwanego rekordu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		$sDel = '';
		$iDeleted = 0;

        $sSql = 'SELECT id 
                 FROM orders 
                 WHERE user_id = '.$iId.' 
                   AND order_status NOT IN ("5", "4")
                 LIMIT 1
                    ';
        $iOrderId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($iOrderId > 0) {
            $this->sMsg = GetMessage(_('Nie można usunąć zamówienia. Istnieje zamówienie tego użytkownika w realizacji.'));
            return $this->Show($pSmarty);
        }


		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI++] = $sKey;
		}
		if (!empty($_POST['delete'])) {
			if (count($_POST['delete']) == 1) {
				$sErrPostfix = '0';
			}
			else {
				$sErrPostfix = '1';
			}

			// pobranie nazw usuwanych rekordow
			$aDel = $this->getItemsIdsNames($_POST['delete']);
			
			foreach ($aDel as $aItem) {
				// usuwanie
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."users_accounts
								 WHERE id = ".$aItem['id'];
				if (($iRes = Common::Query($sSql)) === false) {
					$bIsErr = true;
					break;
				}
				elseif ($iRes == 1) {
					$sDel .= '"'.Common::convert($aItem['email']).'" ('.str_replace('\\', '', $aItem['nazwa']).'), ';
					$iDeleted++;
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				if ($iDeleted > 0) {
					Common::CommitTransaction();
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.$sErrPostfix],
													$sDel);
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_'.$sErrPostfix],
												$sDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton


	/**
	 * Metoda zmienia stan aktywacji konta uzytkownika
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id konta uzytkownika
	 * @return	void
	 */
	function Activate(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;
		
		// okreslenie aktualnego stanu aktywacji konta
		$sSql = "SELECT active
						 FROM ".$aConfig['tabls']['prefix']."users_accounts
						 WHERE id = ".$iId;
		$iActive = (int) Common::GetOne($sSql);
		
		// zmiana stanu aktywacji konta uzytkownika
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."users_accounts
						 SET active = IF (active = '0', '1', '0')
						 WHERE id = ".$iId;
		if (Common::Query($sSql) === false) {
			$bIsErr = true;
		}

		if ($bIsErr) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['account_activation_err'],
											$this->getItemsNames(array($iId)),
											$aConfig['lang'][$this->sModule]['account_activation_'.$iActive]);
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
		}
		else {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['account_activation_ok'],
											$this->getItemsNames(array($iId)),
											$aConfig['lang'][$this->sModule]['account_activation_'.$iActive]);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
		}
		$this->Show($pSmarty);
	} // end of Activate() funciton


	/**
	 * Metoda wyswietla liste kont uzytkownikow
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
    
    if (isset($_GET['search']) && $_GET['search'] != '') {
      $_POST['search'] = $_GET['search'];
    }
    
		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false,
				'width'	=> '20'
			),
			
			array(
				'db_field'	=> 'email',
				'content'	=> $aConfig['lang'][$this->sModule]['list_email'],
				'sortable'	=> true,
				'width'	=> '250'
			),
			array(
				'db_field'	=> 'im_naz',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name_surname'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang'][$this->sModule]['list_registered'],
				'sortable'	=> true,
				'width'	=> '150'
			),
			array(
				'db_field'	=> 'active',
				'content'	=> $aConfig['lang'][$this->sModule]['list_active'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '65'
			)
		);
    
		// pobranie liczby wszystkich uzytkownikow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."users_accounts
						 WHERE 1 = 1".
									 (isset($_POST['search']) && !empty($_POST['search']) ?
										' AND (name LIKE \'%'.$_POST['search'].'%\' OR
													 surname LIKE \'%'.$_POST['search'].'%\' OR
													 email LIKE \'%'.$_POST['search'].'%\')' : ''); 
									 (isset($_POST['f_active']) && $_POST['f_active'] != '' ? " AND active = '".$_POST['f_active']."'" : '');
		if($_POST['f_privliges']>0) {
			switch($_POST['f_privliges']) {
				case 1: $sSql.=' AND (`allow_14days`=\'1\' OR	`allow_personal_reception`=\'1\' OR 	`allow_free_transport`=\'1\' OR `discount`>0.00)';	break;
				case 2: $sSql.=' AND `allow_14days`=\'1\'';	break;
				case 3: $sSql.=' AND `allow_personal_reception`=\'1\'';	break;
				case 4: $sSql.=' AND `allow_free_transport`=\'1\'';	break;
				case 6: $sSql.=' AND `discount`>0.00';	break;
				case 5: $sSql.=' AND (`allow_14days`=\'0\' OR	`allow_personal_reception`=\'0\' OR 	`allow_free_transport`=\'0\' OR `discount`=0.00)';	break;
				}
			}
			if($_POST['f_users_email_denied']>0){
				switch($_POST['f_users_email_denied'])
				{
					case 1:	$sSql .= " AND `promotions_agreement` = '0'";	break;
					case 2:	$sSql .= " AND `promotions_agreement` = '1'";	break;
				}
			}
			if($_POST['f_users_deleted']>0){
				switch($_POST['f_users_deleted'])
				{
					case 1:	$sSql .= " AND `deleted` = '0'";	break;
					case 2:	$sSql .= " AND `deleted` = '1'";	break;
				}
			}
      
			if($_POST['f_registred'] != ''){
				switch($_POST['f_registred'])
				{
					case 0:	$sSql .= " AND `registred` = '0'";	break;
					case 1:	$sSql .= " AND `registred` = '1'";	break;
				}
			}
			
			if($_POST['f_website_id']>0){
					$sSql .= " AND `website_id` = '".$_POST['f_website_id']."'";
			}
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."users_accounts";
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('users', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// FILTRY

    // filtr rejestracji
		$aRegistred = array(
			array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_registred_0']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_registred_1'])
		);
		$pView->AddFilter('f_registred', $aConfig['lang'][$this->sModule]['f_registred'], $aRegistred, $_POST['f_registred']);
		
		
		// filtr aktywosci konta
		$aActivity = array(
			array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '1', 'label' => $aConfig['lang']['common']['yes']),
			array('value' => '0', 'label' => $aConfig['lang']['common']['no'])
		);
		$pView->AddFilter('f_active', _('Aktywne'), $aActivity, $_POST['f_active']);
		
		$aPrivliges = array(
			array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_privliges_any']),
			array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_privliges_1']),
			array('value' => '3', 'label' => $aConfig['lang'][$this->sModule]['f_privliges_2']),
			array('value' => '4', 'label' => $aConfig['lang'][$this->sModule]['f_privliges_3']),
			array('value' => '6', 'label' => $aConfig['lang'][$this->sModule]['f_privliges_4']),
			array('value' => '5', 'label' => $aConfig['lang'][$this->sModule]['f_privliges_no']),
			);
		$pView->AddFilter('f_privliges', $aConfig['lang'][$this->sModule]['f_privliges'], $aPrivliges, $_POST['f_privliges']);
		
		$aWithoutMailPermissions = array(
			array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_hidden_0']),
			array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_hidden_1']),
			//array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_users_email_denied']),
			);
		$pView->AddFilter('f_users_email_denied', $aConfig['lang'][$this->sModule]['f_users_email_denied'], $aWithoutMailPermissions, $_POST['f_users_email_denied']);
		
		$aDeletedAccounts = array(
			array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['f_hidden_0']),
			array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['f_hidden_1']),
		);
		$pView->AddFilter('f_users_deleted', $aConfig['lang'][$this->sModule]['f_users_deleted'], $aDeletedAccounts, $_POST['f_users_deleted']);
		
		$aWebsites = $this->getWebsitesList();
		$aDefault = array(array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']));
		$aWebsites = array_merge($aDefault, $aWebsites);
		$pView->AddFilter('f_website_id', $aConfig['lang'][$this->sModule]['f_website_id'], $aWebsites, $_POST['f_website_id']);
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
			
			$sSqlPart='';
			
			if($_POST['f_users_email_denied']>0){
				switch($_POST['f_users_email_denied'])
				{
					case 1:	$sSqlPart .= " AND `promotions_agreement` = '0'";	break;
					case 2:	$sSqlPart .= " AND `promotions_agreement` = '1'";	break;
				}
			}
			if($_POST['f_users_deleted']>0){
				switch($_POST['f_users_deleted'])
				{
					case 1:	$sSqlPart .= " AND `deleted` = '0'";	break;
					case 2:	$sSqlPart .= " AND `deleted` = '1'";	break;
				}
			}
			if($_POST['f_privliges']>0) {
				switch($_POST['f_privliges'])
				{
					case 1: $sSqlPart.=' AND (`allow_14days`=\'1\' OR	`allow_personal_reception`=\'1\' OR 	`allow_free_transport`=\'1\')';	break;
					case 2: $sSqlPart.=' AND `allow_14days`=\'1\'';	break;
					case 3: $sSqlPart.=' AND `allow_personal_reception`=\'1\'';	break;
					case 4: $sSqlPart.=' AND `allow_free_transport`=\'1\'';	break;
					case 6: $sSqlPart.=' AND `discount`>0.00';	break;
					case 5: $sSqlPart.=' AND (`allow_14days`=\'0\' OR	`allow_personal_reception`=\'0\' OR 	`allow_free_transport`=\'0\')';	break;
				}
			}
			if($_POST['f_website_id']>0){
					$sSqlPart .= " AND `website_id` = '".$_POST['f_website_id']."'";
			}
			// pobranie wszystkich rekordow
			$sSql = "SELECT DISTINCT A.id, A.email, CONCAT(A.surname, ' ', A.name) AS im_naz, A.created, A.active, A.deleted, A.website_id, B.id AS address_id, registred
							 FROM ".$aConfig['tabls']['prefix']."users_accounts AS A
               LEFT JOIN users_accounts_address AS B
                 ON A.id = B.user_id
							 WHERE 1 = 1".
										 (isset($_POST['search']) && !empty($_POST['search']) ?
											' AND (A.name LIKE \'%'.$_POST['search'].'%\' OR
														 A.surname LIKE \'%'.$_POST['search'].'%\' OR
														 A.email LIKE \'%'.$_POST['search'].'%\')' : '').
									 	 (isset($_POST['f_active']) && $_POST['f_active'] != '' ? " AND A.active = '".$_POST['f_active']."'" : '').
                     (isset($_POST['f_registred']) && $_POST['f_registred'] !== '' ? " AND A.registred = '".$_POST['f_registred']."'" : '').
									 $sSqlPart.
							 ' GROUP BY A.id 
                 ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			foreach ($aRecords as $iKey => $aItem) {
				$aRecords[$iKey]['active'] = $aConfig['lang']['common'][$aItem['active'] == '1' ? 'yes' : 'no'];
				$aRecords[$iKey]['im_naz'] = Common::convert($aItem['im_naz']);
				if($aItem['deleted'] == '1'){
					$aRecords[$iKey]['email'] = '<span style="color: #cccccc;">'.$aRecords[$iKey]['email'].' - USUNIĘTE</span>'; 
				}
				if ($aItem['registred'] == '0'){
					$aRecords[$iKey]['email'] = '<span style="color: blue;">'.$aRecords[$iKey]['email'].' - BEZ REJESTRACJI</span>'; 
//          $aRecords[$iKey]['disabled'][] = 'address';
//          $aRecords[$iKey]['disabled'][] = 'delete';
          $aRecords[$iKey]['disabled'][] = 'activate';
          $aRecords[$iKey]['disabled'][] = 'balance';
				}
				$aRecords[$iKey]['email'] = '<img src="gfx/'.$aConfig['invoice_website_symbol_'.$aItem['website_id']].'.gif" /> '.$aRecords[$iKey]['email'];
        
				if (intval($aItem['address_id']) <= 0) {
          $aRecords[$iKey]['disabled'][] = 'address';
        }
        
        unset($aRecords[$iKey]['registred']);
        unset($aRecords[$iKey]['address_id']);
				unset($aRecords[$iKey]['deleted']);
				unset($aRecords[$iKey]['website_id']);
				unset($aRecords[$iKey]['ukryte']);
			}
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id' => array (
					'show'	=> false
				),
				'email' => array (
					'link'	=> phpSelf(array('do' => 'details', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('comments', 'address', 'publishers_series_discount' , 'send', 'discount','balance', 'details', 'activate', 'delete'),
					'params' => array (
                        'comments'	=> array('action' => 'comments', 'pid' => '{id}'),
                        'address'	=> array('action' => 'user_address_list', 'pid' => '{id}'),
                        'publishers_series_discount' => array('action' => 'publishers_series_discount','pid' => '{id}', 'reset' => '1'),
												'send'	=> array('id' => '{id}'),
												'discount'	=> array('id' => '{id}'),
												'balance'	=> array('id' => '{id}'),
												'activate'	=> array('id' => '{id}'),
												'details'	=> array('id' => '{id}'),
												'delete'	=> array('id' => '{id}')
											),
					'show' => false,
          'icon' => array('address' => 'authors', 'publishers_series_discount' => 'linked')
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all'),
			array('group_discount', 'send_group_discount_info')
		);

		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda tworzy formularz z informacjami na temat uzytkownika
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID konta uzytkownika
	 * @return	void
	 */
	function Details(&$pSmarty, $iId) {
		global $aConfig;

		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych szczegolow konta uzytkownika
		$sSql = "SELECT id, passwd, CONCAT(name, ' ', surname) as nazwa, name, surname, 
										company, street, number, number2, postal, city, invoice_street, 
										invoice_number, invoice_number2, invoice_company, invoice_postal, 
										invoice_city, email, phone, invoice_nip, nip, 
										DATE_FORMAT(created, '".$aConfig['common']['sql_date_hour_format']."') data_rej,
										active, discount, invoice_data, allow_14days, allow_personal_reception, allow_free_transport,
										balance, promotions_agreement, seller_streamsoft_id
						 FROM ".$aConfig['tabls']['prefix']."users_accounts
						 WHERE id = ".$iId;
		$aData =& Common::GetRow($sSql);
		
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['account_details_header'],
											 $aData['email'],$aData['nazwa']);

		$pForm = new FormTable('attributes', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), false);
		$pForm->AddHidden('do', '');
		$pForm->AddMergedRow($aLang['details_login_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		$pForm->AddRow($aLang['details_id'], $aData['id']);
		$pForm->AddRow($aLang['details_email'], $aData['email']);
    $pForm->AddRow(_('Nowy adres email'), $pForm->GetTextHTML('email', _('Nowy adres email'), '', array('style' => 'width: 200px;', 'maxlength' => 100), '', 'phone', false).' <div style="font-weight: bold;"> Uwaga, jeśli prośba o zmianę adresu email odebrana została telefonicznie, <br />poproś o wysłanie na nasz adres email maila potwierdzającego.</div>');
    
    $pForm->AddInputButton('button', _('Zmień'), array('onclick' => '$(\'#do\').val(\'change_email\'); $(\'#attributes\').submit(); '), 'button');
    
    $pForm->AddMergedRow(_('Handlowiec'), array('class'=>'merged', 'style'=>'padding-left: 160px;'));
    $pForm->AddText('seller_streamsoft_id', _('Symbol Handlowca w Streamsoft'), $aData['seller_streamsoft_id'], array('style' => 'width:80px', 'maxlength' => '5'), '', 'text', false);
    $pForm->AddInputButton('button_2', _('Zmień'), array('onclick' => '$(\'#do\').val(\'change_seller_id\'); $(\'#attributes\').submit(); '), 'button');
		
//		if ($_SESSION['user']['priv_password'] == '1' || $_SESSION['user']['type'] == '1') {
//			$pForm->AddRow($aLang['details_passwd'], decodePasswd($aData['passwd']));
//		}
//
		$pForm->AddMergedRow($aLang['details_user_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
    if (isset($_SESSION['user']['priv_address_data']) && $_SESSION['user']['priv_address_data'] == '1') {
      $pForm->AddRow($aLang['details_name'], !empty($aData['name']) ? $aData['name'] : $aLang['details_no_data']);
      $pForm->AddRow($aLang['details_surname'], !empty($aData['surname']) ? $aData['surname'] : $aLang['details_no_data']);
			$pForm->AddRow($aLang['details_phone'], $aData['phone'] != '' ? $aData['phone'] : $aLang['details_no_data']);
    }
	//	$pForm->AddRow($aLang['details_street'], $aData['street'].' '.$aData['number'].(!empty($aData['number2'])?' /'.$aData['number2']:''));
	//	$pForm->AddRow($aLang['details_city'], $aData['postal'].' '.$aData['city']);
		
	//	$pForm->AddRow($aLang['details_name_company'], !empty($aData['company']) ? $aData['company'] : $aLang['details_no_data']);
	//	$pForm->AddRow($aLang['details_nip'], !empty($aData['nip']) ? $aData['nip'] : $aLang['details_no_data']);
		
	
		
		/*$pForm->AddMergedRow($aLang['details_invoice_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		$pForm->AddRow($aLang['details_name_company'], !empty($aData['invoice_company']) ? $aData['invoice_company'] : $aLang['details_no_data']);
		$pForm->AddRow($aLang['details_nip'], !empty($aData['invoice_nip']) ? $aData['invoice_nip'] : $aLang['details_no_data']);
		$pForm->AddRow($aLang['details_street'], !empty($aData['invoice_street'])?$aData['invoice_street'].' '.$aData['invoice_number'].(!empty($aData['invoice_number2'])?' /'.$aData['invoice_number2']:'') : $aLang['details_no_data']);
		$pForm->AddRow($aLang['details_city'], !empty($aData['invoice_postal'])&&!empty($aData['invoice_city'])?$aData['invoice_postal'].' '.$aData['invoice_city'] : $aLang['details_no_data']);
	*/	
		
		$pForm->AddMergedRow($aLang['details_other_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		$pForm->AddRow($aLang['details_registered'], !empty($aData['data_rej']) ? $aData['data_rej'] : $aLang['details_no_data']);
		//$pForm->AddRow($aLang['details_client_type'], $aLang['f_client_type_'.$aData['is_company']]);
		$pForm->AddRow($aLang['details_active'], $aConfig['lang']['common'][$aData['active'] == '1' ? 'yes' : 'no']);
		$pForm->AddRow($aLang['allow_14days'], $aConfig['lang']['common'][$aData['allow_14days'] == '1' ? 'yes' : 'no']);
		$pForm->AddRow($aLang['allow_personal_reception'], $aConfig['lang']['common'][$aData['allow_personal_reception'] == '1' ? 'yes' : 'no']);
		$pForm->AddRow($aLang['allow_free_transport'], $aConfig['lang']['common'][$aData['allow_free_transport'] == '1' ? 'yes' : 'no']);

		$pForm->AddMergedRow($aLang['details_discount_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		$pForm->AddRow($aLang['details_discount'], floatval($aData['discount']) > 0 ? Common::formatPrice3($aData['discount']) : $aLang['details_no_discount']);
		$pForm->AddRow($aLang['details_balance'], Common::formatPrice($aData['balance']));
    
    if (isset($_SESSION['user']['priv_address_data']) && $_SESSION['user']['priv_address_data'] == '1') {
      $aAddresses = $this->getUserAddresses($iId);
      foreach($aAddresses as $aAddress){
        $this->showUserAddress($pForm,$aAddress);
      }
    }
		// przyciski
		$pForm->AddRow('&nbsp;', '&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Details() function
	
	
	function prepareUserMessage(&$pSmarty, $iId) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		$aLang =& $aConfig['lang'][$this->sModule];
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		$sHeader = sprintf($aLang['send_user_header'], $this->getItemsNames(array($iId)));
		
		$pForm = new FormTable('send_to_customer', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>165), true);
		$pForm->AddHidden('do', 'proceed_user_send');
		
		// temat maila
		$pForm->AddText('subject', $aLang['subject'], $aData['subject'], array('style'=>'width: 350px;'));
		
		// dolaczaj informacje o aktualnym rabacie
		$pForm->AddCheckbox('include_discount', $aLang['include_discount'], array(), '', !empty($aData['include_discount']), false);
		// informacja na temat jakich znacznikow uzywac w wiadomosci aby
		
		// zostaly zamienione odpowiednio na rabat i date jego waznosci
		$pForm->AddRow('&nbsp;', $aLang['include_discount_info']);
		
		// tresc TXT
		$pForm->AddTextArea('body_txt', $aLang['body_txt'], $_POST['body_txt'], array('rows'=>20, 'style'=>'width: 100%;'), 0, '', '', false);
		
		// tresc HTML
		$pForm->AddWYSIWYG('body_html', $aConfig['lang'][$this->sModule]['body_html'], $_POST['body_html'], array('strip_urls' =>'0'), '', false);
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aLang['send_button']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of prepareUserMessage() function
	
	
	/**
	 * Metoda wysyla wiadomosc do uzytkownika
	 * 
	 * @param	integer	$iId	- Id uzytkownika
	 * @return	void
	 */
	function sendUserMessage(&$pSmarty, $iId) {
		global $aConfig;
		
		// nazwy atrybutow do pobrania
		$sAttribs = 'email, website_id';
		if (isset($_POST['include_discount'])) {
			// sprawdzenei poprawnosci wypelnienia tresci - czy jest znacznik {rabat}
			if (!($bTxt = !(strpos($_POST['body_txt'], '{rabat}') === false)) && !($bHtml = !(strpos($_POST['body_html'], '{rabat}') === false))) {
				// brak tresci wiadomosci
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_body_err'], true);
				$this->prepareUserMessage($pSmarty, $iId);
				return;
			}
			// dolaczenie do wybieranych atrybutow rabatu i jeo czasu obowiazywania
			$sAttribs .= ", discount";
		}
		else {
			if (!($bTxt = !(trim($_POST['body_txt']) == '')) && !($bHtml = !(trim($_POST['body_html']) == ''))) {
				// brak tresci wiadomosci
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_plain_body_err'], true);
				$this->prepareUserMessage($pSmarty, $iId);
				return;
			}
		}
		
		$aData =& $this->getUserAttribs($iId, $sAttribs);
		if (isset($aData['discount'])) {
			// zamiana {rabat} na wartosc rabatu w tresci wiadomosci
			$sContent = stripslashes(str_replace('{rabat}', Common::formatPrice3($aData['discount']).'%', $_POST['body_'.($bTxt ? 'txt' : 'html')]));
		}
		else {
			$sContent = stripslashes($_POST['body_'.($bTxt ? 'txt' : 'html')]);
		}
		$sWebsiteSymbol = $this->getWebsiteSymbol($aData['website_id']);
		$aSiteSettings = getSiteSettings($sWebsiteSymbol);
		$sFromEmail = $aSiteSettings['users_email'];
		if (Common::sendMail('', $sFromEmail, $aData['email'], changeCharcodes(stripslashes($_POST['subject']), 'utf8', 'no'), $sContent, !$bTxt)) {
			// wiadomosc wyslana
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['send_user_msg_ok'],
											$_POST['subject'],
											$this->getItemsNames(array($iId)));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// nie udalo sie wyslac wiadomosci
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['send_user_msg_err'],
											$_POST['subject'],
											$this->getItemsNames(array($iId)));
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
			$this->Show($pSmarty);
		}
	} // end of sendUserMessage() method

	/**
	 * Metoda tworzy formularz wysylki maila grupowego
	 *
	 * @param	object	$pSmarty
	 */
	function prepareGroupMessage(&$pSmarty) {
		global $aConfig;
				
		$sHtml = '';
		$aLang =& $aConfig['lang'][$this->sModule];
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
				
		$sHeader = $aLang['send_group_message_header'];
		
		$pForm = new FormTable('send_to_group', $sHeader, array('action'=>phpSelf()), array('col_width'=>165), true);
		$pForm->AddHidden('do', 'proceed_group_send');
		
		// log dla ktorego beda wysylane wiadomosci
		$pForm->AddSelect('log_file', $aConfig['lang'][$this->sModule]['log_file'], array(), $this->getLogs(), $_POST['log_file']);
		
		// temat maila
		$pForm->AddText('subject', $aLang['subject'], $_POST['subject'], array('style'=>'width: 350px;'));
		
		// informacja
		$pForm->AddRow('&nbsp;', $aLang['include_discount_info']);
		
		// tresc TXT
		$pForm->AddTextArea('body_txt', $aLang['body_txt'], $_POST['body_txt'], array('rows'=>20, 'style'=>'width: 100%;'), 0, '', '', false);
		
		// tresc HTML
		$pForm->AddWYSIWYG('body_html', $aConfig['lang'][$this->sModule]['body_html'], $_POST['body_html'], array('strip_urls' =>'0'), '', false);
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aLang['send_button']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of prepareGroupMessage() function
	
	
	/**
	 * Metoda wysyla wiadomosc do grupy uzytkownikow
	 * 
	 * @param	integer	$iId	- Id uzytkownika
	 * @return	void
	 */
	function sendGroupMessage(&$pSmarty) {
		global $aConfig;
		set_time_limit(3600);
		$iSent = 0;
		
		if (!($bTxt = !(strpos($_POST['body_txt'], '{rabat}') === false)) && !($bHtml = !(strpos($_POST['body_html'], '{rabat}') === false))) {
			// brak tresci wiadomosci
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_body_err'], true);
			$this->prepareGroupMessage($pSmarty);
			return;
		}
		
		// pobranie danych z pliku logu
		$sLog = implode('', @file($this->sLogsDir.'/'.$_POST['log_file']));
		preg_match_all('/\[ ID\:(\d+) \]/', $sLog, $aIDs);
		
		if (empty($aIDs[1])) {
			// nie znaleziono zadnych ID uzytkownikow w pliku
			$this->sMsg = GetMessage(sprintf($aConfig['lang'][$this->sModule]['send_group_no_ids'], $_POST['log_file']), true);
			$this->prepareGroupMessage($pSmarty);
			return;
		}
		
		// nazwy atrybutow do pobrania
		$sAttribs = 'id, email, CONCAT(name,\' \',surname) AS nazwa, discount, website_id ';
		// pobranie naglowka z pliku log
		$sLogHeader = mb_substr($sLog, 0, mb_strpos($sLog, 'ID', 0, 'utf8'), 'utf8');
		// nazwa pliku - otwarcie / utworzenie
		$sFileName = $_POST['log_file'].'__rozsylka_'.date('dmY_His');
		$sLogFile = $this->sLogsDir.'/'.$sFileName;
		// odwrocenie tablicy z ID
		if ($oF = @fopen($sLogFile, 'w')) {
			// zapis daty
			fwrite($oF, sprintf($aConfig['lang'][$this->sModule]['log_send_date'], date('d-m-Y H:i:s')));
			// zapis naglowka skopiowanego z pliku logu
			fwrite($oF, $sLogHeader);
			// naglowek rekordow
			fwrite($oF, $aConfig['lang'][$this->sModule]['log_users_header']);
		
			foreach ($aIDs[1] as $iId) {
				$aData =& $this->getUserAttribs($iId, $sAttribs);
				// zamiana {rabat} na wartosc rabatu w tresci wiadomosci
				$sContent = stripslashes(str_replace('{rabat}', Common::formatPrice3($aData['discount']).'%', $_POST[$bTxt ? 'body_txt' : 'body_html']));
				$aSiteSettings = getSiteSettings($this->getWebsiteSymbol($aData['website_id']));
				$sFromEmail = $aSiteSettings['users_email'];
				if (Common::sendMail('', $sFromEmail, $aData['email'], changeCharcodes(stripslashes($_POST['subject']), 'utf8', 'no'), $sContent, !$bTxt)) {
					// wiadomosc wyslana
					$iSent++;
					// dodanie informacji do pliku
					fwrite($oF, $this->getUserLogData($aData));
				}
			}
			fclose($oF);
		}
		if ($iSent > 0) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['send_group_msg_ok'],
											$iSent,
											$_POST['log_file'],
											$sFileName);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// nie udalo sie wyslac wiadomosci
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['send_group_msg_err'],
											$_POST['log_file']);
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
			$this->Show($pSmarty);
		}
	} // end of sendGroupMessage() method
		
	
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type 
	 */
	function getWebsiteSymbol($iWebsiteId) {
		global $aConfig;
		
		$sSql = "SELECT code FROM ".$aConfig['tabls']['prefix']."websites
						 WHERE id=".$iWebsiteId;
		return Common::GetOne($sSql);
	}// end of getWebsiteSymbol() method
	
	
	/**
	 * Metoda aktualizuje rabat uzytkownika o podanym Id
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateGroupDiscount(&$pSmarty) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->EditGroupAccount($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->EditGroupDiscount($pSmarty, true);
			return;
		}
		if (empty($_POST['delete'])) {
			// nie zaznaczono zadnego uzytkownika
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_users_checked_err'], true);
			$this->EditGroupDiscount($pSmarty, true);
			return;
		}
		
		// nazwa pliku - otwarcie / utworzenie
		$sFileName = 'rabat_'.date('dmY_His').'__'.preg_replace('/[\,\.]/', '_', $_POST['discount']).'_proc';
		$sLogFile = $this->sLogsDir.'/'.$sFileName;
		// odwrocenie tablicy z ID
		$_POST['delete'] = array_keys($_POST['delete']);
		// pobranie danych uzytkownikow
		$aUsers =& $this->getUsersData($_POST['delete']);

		if (!empty($aUsers) && $oF = @fopen($sLogFile, 'w')) {
			// zapis daty
			fwrite($oF, sprintf($aConfig['lang'][$this->sModule]['log_date'], date('d-m-Y H:i:s')));
			// zapis wysokosci rabatu
			fwrite($oF, sprintf($aConfig['lang'][$this->sModule]['log_discount'], Common::formatPrice3($_POST['discount'])));
			// zapis warunkow dla ktorych wybrano uzytkownikow do nadania rabatu
			fwrite($oF, $this->getConditionsString(trim($_POST['name_includes']), trim($_POST['email_includes']), Common::formatPrice2($_POST['value_from']), Common::formatPrice2($_POST['value_to']), $_POST['date_from'], $_POST['date_to'], Common::formatPrice2($_POST['current_discount'])));
			// naglowek rekordow
			fwrite($oF, $aConfig['lang'][$this->sModule]['log_users_header']);
			
			foreach ($_POST['delete'] as $iUId) {
				// aktualizacja rabatu
				$aValues = array(
					'discount' => Common::formatPrice2($_POST['discount']),
					'discount_set' => 'NOW()'
				);
				if (Common::Update($aConfig['tabls']['prefix']."users_accounts",
													 $aValues,
													 "id = ".$iUId) === false) {
					$bIsErr = true;
					// usuniecie pliku
					unlink($sLogFile);
					break;
				}
				// dodanie informacji do pliku
				fwrite($oF, $this->getUserLogData($aUsers[$iUId]));
			}
		}
		else {
			$bIsErr = true;
		}
		fclose($oF);
		if (!$bIsErr) {
			// rabat uzytkownika zostal zapisany
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['group_discount_edit_ok'],
											Common::formatPrice3($_POST['discount']),
											$sFileName);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// rabat nie sotal zapisany - blad
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['group_discount_edit_err'],
											Common::formatPrice3($_POST['discount']));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->EditGroupDiscount($pSmarty, true);
		}
	} // end of UpdateGroupDiscount() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji rabatu grupowego
	 * dla uzytkownikow, ktorzy spelniaja okreslone zalozenia:
	 * przedzial wartosci zlozoych dotychczas zamowien, wartosc aktualnego
	 * rabatu
	 *
	 * @param	object	$pSmarty
	 * @param	bool	$bSearch	- true: wyswietl uzytkownikow
	 */
	function EditGroupDiscount(&$pSmarty, $bSearch=false) {
		global $aConfig;
		$sHtml = '';
		$aUsers = array();
		
		if ($bSearch) {
			// wyszukiwanie uzytkownikow spelniajacych zadane kryteria
			$aUsers =& $this->getUsersToDiscount(trim($_POST['name_includes']), trim($_POST['email_includes']), Common::formatPrice2($_POST['value_from']), Common::formatPrice2($_POST['value_to']), $_POST['date_from'], $_POST['date_to'], Common::formatPrice2($_POST['current_discount']));
		}
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$sHeader = $aConfig['lang'][$this->sModule]['group_discount_header'];
		
		$pForm = new FormTable('group_discount', $sHeader, array('action'=>phpSelf()), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_group_discount');
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['conditions_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		
		// nazwa zawiera
		$pForm->AddText('name_includes', $aConfig['lang'][$this->sModule]['name_includes'], isset($_POST['name_includes']) && $_POST['name_includes'] != '' ? $_POST['name_includes'] : '', array('style'=>'width: 250px;'), '', 'text', false);
		
		// przedzial wartosci zlozonych do tej pory zamowien
		$pForm->AddRow($aConfig['lang'][$this->sModule]['value_range'], $pForm->GetTextHTML('value_from', '', isset($_POST['value_from']) && $_POST['value_from'] != '' ? Common::formatPrice3($_POST['value_from']) : '', array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'ufloat', false).'&nbsp;&nbsp;&nbsp;&nbsp;'.$aConfig['lang'][$this->sModule]['value_to'].'&nbsp;&nbsp;'.$pForm->GetTextHTML('value_to', '', isset($_POST['value_to']) && $_POST['value_to'] != '' ? Common::formatPrice3($_POST['value_to']) : '', array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'ufloat', false));
		
		// przedzial czasowy w ktorym zostaly zlozone zamowienia
		$pForm->AddRow($aConfig['lang'][$this->sModule]['date_range'], '<table border="0"><tr><td style="border: 0;">'.$pForm->GetTextHTML('date_from', '', isset($_POST['date_from']) && $_POST['date_from'] != '' ? $_POST['date_from'] : '00-00-0000', array('style' => 'width: 75px;'), '', 'date', false).'</td><td style="border: 0;">'.$aConfig['lang'][$this->sModule]['date_to'].'</td><td style="border: 0;">'.$pForm->GetTextHTML('date_to', '', isset($_POST['date_to']) && $_POST['date_to'] != '' ? Common::formatPrice3($_POST['date_to']) : '00-00-0000', array('style' => 'width: 75px;'), '', 'date', false).'</td></tr></table>');
		
		// lub
		$pForm->AddRow('&nbsp;', $aConfig['lang'][$this->sModule]['or']);
		
		// adres e-mail zawiera
		$pForm->AddText('email_includes', $aConfig['lang'][$this->sModule]['email_includes'], isset($_POST['email_includes']) && $_POST['email_includes'] != '' ? $_POST['email_includes'] : '', array('style'=>'width: 150px;'), '', 'text', false);
		
		// lub
		$pForm->AddRow('&nbsp;', $aConfig['lang'][$this->sModule]['or']);
		
		// aktualny rabat rabat
		$pForm->AddText('current_discount', $aConfig['lang'][$this->sModule]['current_discount'], isset($_POST['current_discount']) && floatval($_POST['current_discount']) > 0 ? Common::formatPrice3($_POST['current_discount']) : '', array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat', false);
		
		// przycisk wyszukaj
		$pForm->AddRow('&nbsp;', '<div style="margin-left: 215px;">'.$pForm->GetInputButtonHTML('show_users', $aConfig['lang'][$this->sModule]['button_show_users'], array('onclick'=>'document.getElementById(\'do\').value=\'show_users\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'</div>');
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['new_discount_section'], array('class'=>'merged', 'style'=>'padding-left: 155px;'));
		
		$pForm->AddText('discount', $aConfig['lang'][$this->sModule]['discount_amount'], isset($_POST['discount']) && floatval($_POST['discount']) > 0 ? Common::formatPrice3($_POST['discount']) : '', array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');
		
		if ($bSearch) {
			// wyswietlenie listy pasujacych uzytkownikow
			$pForm->AddMergedRow($this->GetUsersList($pSmarty, $aUsers));
		}

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang'][$this->sModule]['button_add_group_discount']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of EditGroupDiscount() function
	
	
	/**
	 * Metoda wyswietla liste uzytkownikow z tablicy $aUsers
	 *
	 * @param		object	$pSmarty
	 * @param	array ref	$aUsers	- lista uzytkownikow
	 * @return	void
	 */
	function GetUsersList(&$pSmarty, &$aUsers) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		$aHeader = array(
			'refresh'	=> false,
			'search'	=> false,
			'per_page'	=> false,
			'form'	=>	false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'width'	=> '20'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_id'],
				'width' => '30'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_name'],
				'width' => '150'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_email']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_discount']
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['users_list_orders_value']
			)
		);

		$pView = new View('group_discount', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// ustawienia dla poszczegolnych kolumn rekordu
		$aColSettings = array();
		// dodanie rekordow do widoku
		$pView->AddRecords($aUsers, $aColSettings);
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array('check_all')
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		// przepisanie langa - info o braku rekordow
		$aConfig['lang'][$this->sModule]['no_items'] = $aConfig['lang'][$this->sModule]['no_users_to_show'];
		return $pView->Show();
	} // end of GetUsersList() function
	
	
	/**
	 * Metoda zwraca liste uzytkownikow o przekazanych do niej Id
	 *
	 * @param	array	$aIds	- Id rekordow dla ktorych zwrocone zostana nazwy
	 * @return	string	- ciag z nazwami
	 */
	function getItemsNames($aIds) {
		global $aConfig;
		$sList = '';

		// pobranie nazw rekordow
		$sSql = "SELECT email, CONCAT(name, ' ', surname) as nazwa
				 		 FROM ".$aConfig['tabls']['prefix']."users_accounts
					 	 WHERE id IN  (".implode(',', $aIds).")";
		$aRecords =& Common::GetAll($sSql);
		foreach ($aRecords as $aItem) {
			$sList .= '"'.Common::convert($aItem['email']).'" ('.str_replace('\\', '', Common::convert($aItem['nazwa'])).'), ';
		}
		$sList = substr($sList, 0, -2);
		return $sList;
	} // end of getItemsNames() method
	
	
	/**
	 * Metoda zwraca liste uzytkownikow o przekazanych do niej Id
	 * wraz z ich Id
	 *
	 * @param	array	$aIds	- Id rekordow dla ktorych zwrocone zostana nazwy
	 * @return	string	- ciag z nazwami
	 */
	function getItemsIdsNames($aIds) {
		global $aConfig;
		$sList = '';

		// pobranie nazw rekordow
		$sSql = "SELECT id, email, CONCAT(name,' ',surname) AS nazwa
				 		 FROM ".$aConfig['tabls']['prefix']."users_accounts
					 	 WHERE id IN  (".implode(',', $aIds).")";
		return Common::GetAll($sSql);
	} // end of getItemsIdsNames() method
	
	
	/**
	 * Metoda pobiera i zwraca wartosci podanych atrybutow z konta uzytkownia
	 *
	 * @param	integer	$iId	- Id uzytkownika
	 * @param	string	$sAttribs	- nazwa atrybutow (jezeli wiecej niz jeden - 
	 * 														oddzielone przeinkami - jak do zapytania SQL
	 * @return	array
	 */
	function getUserAttribs($iId, $sAttribs) {
		global $aConfig;

		$sSql = "SELECT ".$sAttribs."
				 		 FROM ".$aConfig['tabls']['prefix']."users_accounts
					 	 WHERE id =  ".$iId;
		return Common::GetRow($sSql);
	} // end of getUserAttribs() method
	
	
	/**
	 * Metoda pobiera i zwraca dane uzytkownikow o Id z podanej tablicy
	 *
	 * @param	array ref	$aIDs	- Id uzytkownikow
	 * @return	array ref
	 */
	function &getUsersData(&$aIDs) {
		global $aConfig;

		$sSql = "SELECT id uid, id, CONCAT(name, ' ', surname) as nazwa, email,discount
				 		 FROM ".$aConfig['tabls']['prefix']."users_accounts
					 	 WHERE id IN (".implode(',', $aIDs).")";
		return Common::GetAssoc($sSql, true);
	} // end of getUsersData() method
	
	
	/**
	 * Metoda formatuje i zwraca dane uzytkownika na potrzeby logow
	 *
	 * @param	array ref	$aData	- dane uzytkownika
	 * @return	string
	 */
	function getUserLogData(&$aData) {
		global $aConfig;
			
		return sprintf($aConfig['lang'][$this->sModule]['log_user_data'], $aData['id'], $aData['nazwa'], $aData['email'], Common::formatPrice3($aData['discount']));
	} // end of getUserLogData() method
	
	
	/**
	 * Metoda pobiera liste uzytkownikow spelniajacych zadane kryteria
	 * 
	 * @param	string	$sNameIncludes	- nazwa uzytkownika zawiera
	 * @param	string	$sEmailIncludes	- email uzytkownika zawiera
	 * @param	float	$fValueFrom	- poczatkowy zakres wartosci zamowien
	 * @param	float	$fValueTo	- koncowy zakres wartosci zamowien
	 * @param	string	$sDateFrom	- poczatkowy zakres dat skladanych zamowien
	 * @param	string	$sDateTo	- koncowy zakres dat skladanych zamowien
	 * @param	float	$fCurrentDiscout	- aktualny rabat
	 * @return	array ref
	 */
	function &getUsersToDiscount($sNameIncludes, $sEmailIncludes, $fValueFrom, $fValueTo, $sDateFrom, $sDateTo, $fCurrentDiscount) {
		global $aConfig;
		$aUsers = array();
		
		if ($sNameIncludes != '' || ($fValueFrom > 0 && $fValueTo > 0 && $fValueTo > $fValueFrom) || ($sDateFrom != '' && $sDateTo != '')) {
			$_POST['current_discount'] = '';
			$_POST['email_includes'] = '';
			$sDateSql = '';
			$sIncludesSql = '';
			$sHavingSql = '';
			if ($sNameIncludes != '') {
				$sIncludesSql = " AND CONCAT(name,' ',surname,' ',name) LIKE '%".$sNameIncludes."%'";
			}
			if ($fValueFrom > 0 && $fValueTo > 0 && $fValueTo > $fValueFrom) {
				$sHavingSql = " HAVING ile >= ".$fValueFrom." AND ile <= ".$fValueTo;
			}
			if ($sDateFrom != '' && $sDateTo != '') {
				$sDateSql = " AND DATE_FORMAT(order_date, '%Y-%m-%d') >= '".FormatDate($sDateFrom)."' AND DATE_FORMAT(order_date, '%Y-%m-%d') <= '".FormatDate($sDateTo)."'";
			}
			// pobranie ID uzytkownikow spelniajacych zadane kryteria
			$sSql = "SELECT user_id, SUM(value_brutto) ile
							 FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE user_id IS NOT NULL AND user_id > 0".$sIncludesSql.$sDateSql."
							 GROUP BY user_id".$sHavingSql;
			$aOrders =& Common::GetAssoc($sSql);
			if (!empty($aOrders)) {
				// pobranie uzytkownikow spelniajacych zadane kryteria
				$sSql = "SELECT id, CONCAT(name, ' ', surname) as nazwa, email, discount
								 FROM ".$aConfig['tabls']['prefix']."users_accounts
								 WHERE id IN (".implode(',', array_keys($aOrders)).")";
				$aUsers =& Common::GetAll($sSql);
				
				foreach ($aUsers as $iKey => $aUser) {
					$aUsers[$iKey]['discount'] = Common::formatPrice3($aUser['discount']).' %';
					$aUsers[$iKey]['wartosc_zamowien'] = Common::formatPrice3($aOrders[$aUser['id']]).' '.$aConfig['lang']['common']['currency'];
				}
			}
		}
		elseif ($fCurrentDiscount > 0 || $sEmailIncludes != '') {
			$_POST['name_includes'] = '';
			$_POST['value_from'] = '';
			$_POST['value_to'] = '';
			$_POST['date_from'] = '00-00-0000';
			$_POST['date_to'] = '00-00-0000';
			
			if ($sEmailIncludes != '') {
				$_POST['current_discount'] = '';
				// pobranie uzytkownikow ktorych adres email zawiera okreslona fraze
				$sSql = "SELECT id uid, id, CONCAT(name, ' ', surname) as nazwa, email, discount
								 FROM ".$aConfig['tabls']['prefix']."users_accounts
								 WHERE email LIKE '%".$sEmailIncludes."%'";
				$aUsers =& Common::GetAssoc($sSql, true);
			}
			elseif ($fCurrentDiscount > 0) {
				$_POST['email_includes'] = '';
				// pobranie uzytkownikow posiadajacych okreslony rabat
				$sSql = "SELECT id uid, id, CONCAT(name, ' ', surname) as nazwa, email, discount
								 FROM ".$aConfig['tabls']['prefix']."users_accounts
								 WHERE discount = ".$fCurrentDiscount;
				$aUsers =& Common::GetAssoc($sSql, true);
			}
			// pobranie wartosci zamowien uzytkownikow
			$sSql = "SELECT user_id, SUM(value_brutto) ile
							 FROM ".$aConfig['tabls']['prefix']."orders
							 WHERE user_id IN (".(!empty($aUsers) ? implode(',', array_keys($aUsers)) : '0').")
							 GROUP BY user_id";
			$aOrders =& Common::GetAssoc($sSql);
			foreach ($aUsers as $iKey => $aUser) {
				$aUsers[$iKey]['discount'] = Common::formatPrice3($aUser['discount']).' %';
				$aUsers[$iKey]['wartosc_zamowien'] = Common::formatPrice3($aOrders[$aUser['id']]).' '.$aConfig['lang']['common']['currency'];
			}
		}
		return $aUsers;
	} // end of getUsersToDiscount() method
	
	
	/**
	 * Metoda zwraca string z informacja na temat kryteriow na podstawie ktorych
	 * przyznano rabat uzytkownikom
	 * 
	 * @param	string	$sNameIncludes	- nazwa uzytkownika zawiera
	 * @param	string	$sEmailIncludes	- email uzytkownika zawiera
	 * @param	float	$fValueFrom	- poczatkowy zakres wartosci zamowien
	 * @param	float	$fValueTo	- koncowy zakres wartosci zamowien
	 * @param	string	$sDateFrom	- poczatkowy zakres dat skladanych zamowien
	 * @param	string	$sDateTo	- koncowy zakres dat skladanych zamowien
	 * @param	float	$fCurrentDiscout	- aktualny rabat
	 * @return	string
	 */
	function &getConditionsString($sNameIncludes, $sEmailIncludes, $fValueFrom, $fValueTo, $sDateFrom, $sDateTo, $fCurrentDiscount) {
		global $aConfig;
		$sStr = $aConfig['lang'][$this->sModule]['log_conditions'];
		
		if ($sNameIncludes != '' || ($fValueFrom > 0 && $fValueTo > 0 && $fValueTo > $fValueFrom) || ($sDateFrom != '' && $sDateTo != '')) {
			if ($sNameIncludes != '') {
				$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_name_includes'], $sNameIncludes);
			}
			$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_value_range'], Common::formatPrice3($fValueFrom), Common::formatPrice3($fValueTo));
			if ($sDateFrom != '' && $sDateTo != '') {
				$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_date_range'], $sDateFrom, $sDateTo);
			}
		}
		elseif ($sEmailIncludes != '') {
			$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_email_includes'], $sEmailIncludes);
		}
		elseif ($fCurrentDiscount > 0) {
			$sStr .= sprintf($aConfig['lang'][$this->sModule]['log_current_discount'], $fCurrentDiscount);
		}
		return $sStr."\n";
	} // end of getConditionsString() method
	
	
	/**
	 * Metoda pobiera liste logow i zwraca je w postaci tablicy dla pola SELECT
	 * 
	 * @return	array ref
	 */
	function &getLogs() {
		global $aConfig;
		$aLogs	= array(array('value' => '', 'label' => $aConfig['lang']['common']['choose']));
		
		$pDir = opendir($this->sLogsDir);
  	while (false !== ($sFile = readdir($pDir))) {
    	if (preg_match('/^rabat_(\d{2})(\d{2})(\d{4})_(\d{2})(\d{2})(\d{2})__(\d+)(_(\d+))?_proc$/', $sFile)) {
    		$aLogs[] = array('value' => $sFile, 'label' => $sFile);
      }
    }
    closedir($pDir);
    sort($aLogs);

    return $aLogs;
	} // end of getLogs() method
	
	
	/**
	 * Metoda aktualizuje rabat uzytkownika o podanym Id
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id uzytkownika
	 * @return	void
	 */
	function UpdateUserDiscount(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->EditUserDiscount($pSmarty, $iId);
			return;
		}
		// aktualizacja
		$aValues = array(
			'discount' => Common::formatPrice2($_POST['discount']),
            'invoice_to_pay_days' => $_POST['invoice_to_pay_days'],
			'discount_set' => 'NOW()',
			'promotions_agreement' => $_POST['promotions_agreement']=='1'?'1':'0',
			'allow_14days' => isset($_POST['allow_14days']),
			'allow_personal_reception' => isset($_POST['allow_personal_reception']),
			'allow_free_transport' => isset($_POST['allow_free_transport']),
            'allow_add_unavailable' => isset($_POST['allow_add_unavailable']),
		);
		if (Common::Update($aConfig['tabls']['prefix']."users_accounts",
											 $aValues,
											 "id = ".$iId) === false) {
			$bIsErr = true;
		}
		
		if (!$bIsErr) {
			// rabat uzytkownika zostal zapisany
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['discount_edit_ok'],
											$this->getItemsNames(array($iId)));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// rabat nie sotal zapisany - blad
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['discount_edit_err'],
											$this->getItemsNames(array($iId)));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->EditUserDiscount($pSmarty, $iId);
		}
	} // end of UpdateUserDiscount() funciton
	
	/**
	 * Metoda aktualizuje saldo uzytkownika o podanym Id
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id uzytkownika
	 * @return	void
	 */
	function UpdateUserBalance(&$pSmarty, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->EditUserBalance($pSmarty, $iId);
			return;
		}
		// aktualizacja
		$aValues = array(
			'balance' => Common::formatPrice2($_POST['balance']),
		);
		if (Common::Update($aConfig['tabls']['prefix']."users_accounts",
											 $aValues,
											 "id = ".$iId) === false) {
			$bIsErr = true;
		}
		if(!$bIsErr){
			$aValues = array(
				'user_id' => $iId,
				'description' => $_POST['balance_description'],
				'balance' => Common::formatPrice2($_POST['balance'])
			);
			if(Common::Insert($aConfig['tabls']['prefix']."orders_balances",$aValues) === false){
				$bIsErr = true;
			}
		}
		
		if (!$bIsErr) {
			// rabat uzytkownika zostal zapisany
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['balance_edit_ok'],
											$this->getItemsNames(array($iId)),
											Common::formatPrice($_POST['balance']));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
		else {
			// rabat nie sotal zapisany - blad
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['balance_edit_err'],
											$this->getItemsNames(array($iId)));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->EditUserBalance($pSmarty, $iId);
		}
	} // end of UpdateUserBalance() funciton

  
	/**
	 * Metoda tworzy formularz edycji rabatu uzytkownika
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	ID konta uzytkownika
	 */
	function EditUserDiscount(&$pSmarty, $iId) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych uzytkownika
		$sSql = "SELECT email, discount, allow_14days, allow_personal_reception, allow_free_transport, promotions_agreement, invoice_to_pay_days,
                        allow_add_unavailable
						 FROM ".$aConfig['tabls']['prefix']."users_accounts
						 WHERE id = ".$iId;
		$aData =& Common::GetRow($sSql);

		if (!empty($_POST)) {
			$aData['discount'] =& $_POST['discount'];
			$aData['invoice_to_pay_days'] = $_POST['invoice_to_pay_days'];
		}

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['discount_header'],
											 $aData['email']);
		
		$pForm = new FormTable('user_discount', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_user_discount');
		
		// rabat
		$pForm->AddText('discount', $aConfig['lang'][$this->sModule]['discount_amount'], isset($aData['discount']) && floatval($aData['discount']) > 0 ? Common::formatPrice3($aData['discount']) : 0, array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');
        $pForm->AddText('invoice_to_pay_days', $aConfig['lang'][$this->sModule]['discount_amount'], isset($aData['discount']) && floatval($aData['discount']) > 0 ? Common::formatPrice3($aData['discount']) : 0, array('maxlength'=>5, 'style'=>'width: 50px;'), '', 'ufloat');
        $aOptions = [
            [
                'label' => '7',
                'value' => '7'
            ],
            [
                'label' => '10',
                'value' => '10'
            ],
            [
                'label' => '14',
                'value' => '14'
            ],
            [
                'label' => '21',
                'value' => '21'
            ],
            [
                'label' => '30',
                'value' => '30'
            ],
            [
                'label' => '45',
                'value' => '45'
            ],
            [
                'label' => '60',
                'value' => '60'
            ],
        ];
        $pForm->AddSelect('invoice_to_pay_days', _('Liczba dni na opłacenie FV przelewem przedpłatą'), [], addDefaultValue($aOptions), $aData['invoice_to_pay_days'], true);
		$pForm->AddCheckBox('allow_14days', $aConfig['lang'][$this->sModule]['allow_14days'],array(),'', $aData['allow_14days'],false);
		$pForm->AddCheckBox('allow_personal_reception', $aConfig['lang'][$this->sModule]['allow_personal_reception'],array(),'', $aData['allow_personal_reception'],false);
		$pForm->AddCheckBox('allow_free_transport', $aConfig['lang'][$this->sModule]['allow_free_transport'],array(),'', $aData['allow_free_transport'],false);
        $pForm->AddCheckBox('allow_add_unavailable', _('Pozwól na zamawianie niedostępnych produktów'),array(),'', $aData['allow_add_unavailable'],false);


        // Oznacznie "Nie wysyłaj mailingów okazjonalnych"
		if ($_POST['promotions_agreement'] != '') {
			$aData['promotions_agreement'] = $_POST['promotions_agreement'];
		}
		$pForm->AddCheckbox('promotions_agreement', $aConfig['lang'][$this->sModule]['promotions_agreement'], array(),'', $aData['promotions_agreement']=='1'?'1':'0', false);
		//$pForm->addHidden('do', 'update_details');
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of EditUserDiscount() function
  

	/**
	 * Metoda tworzy formularz edycji salda uzytkownika
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	ID konta uzytkownika
	 */
	function EditUserBalance(&$pSmarty, $iId) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		// pobranie z bazy danych uzytkownika
		$sSql = "SELECT email, balance
						 FROM ".$aConfig['tabls']['prefix']."users_accounts
						 WHERE id = ".$iId;
		$aData =& Common::GetRow($sSql);

		if (!empty($_POST)) {
			$aData['balance'] =& $_POST['balance'];
			$aData['balance_description'] =& $_POST['balance_description'];
		}

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['balance_header'],
											 $aData['email']);
		
		$pForm = new FormTable('user_balancet', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_user_balance');
		
		// rabat
		$pForm->AddText('balance', $aConfig['lang'][$this->sModule]['balance_value'], Common::formatPrice($aData['balance']), array('maxlength'=>8, 'style'=>'width: 50px;'), '', 'ufloat');
		$pForm->AddText('balance_description', $aConfig['lang'][$this->sModule]['balance_description'], $aData['balance_description'], array('maxlength'=>128, 'style'=>'width: 350px;'), '', 'text');
		
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of EditUserDiscount() function
	
	/**
	 * Wyświetla adres użytkownika
	 * @param array ref $pForm - formularz
	 * @param array ref $aData - dane adresu użytkownika
	 * @return void
	 */
	function showUserAddress(&$pForm,&$aData){
		global $aConfig;
		$aLang =& $aConfig['lang'][$this->sModule];
		$pForm->AddMergedRow(sprintf($aLang['details_user_address'],$aData['address_name']), array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		if($aData['is_invoice'] == '1'){
			//$pForm->AddRow($aLang['details_default'], $aLang['details_default_invoice']);
		}
		if($aData['is_transport'] == '1'){
			//$pForm->AddRow($aLang['details_default'], $aLang['details_default_transport']);
		}
		if($aData['is_company'] == '1'){
			$pForm->AddRow($aLang['details_type'], $aLang['details_type_company']);
			$pForm->AddRow($aLang['details_name_company'], !empty($aData['company']) ? $aData['company'] : $aLang['details_no_data']);
			$pForm->AddRow($aLang['details_nip'], !empty($aData['nip']) ? $aData['nip'] : $aLang['details_no_data']);
			$pForm->AddRow($aLang['details_street'], $aData['street'].' '.$aData['number'].(!empty($aData['number2'])?' /'.$aData['number2']:''));
			$pForm->AddRow($aLang['details_city'], !empty($aData['postal'])&&!empty($aData['city'])?$aData['postal'].' '.$aData['city'] : $aLang['details_no_data']);
			$pForm->AddRow($aLang['details_name'], !empty($aData['name']) ? $aData['name'] : $aLang['details_no_data']);
			$pForm->AddRow($aLang['details_surname'], !empty($aData['surname']) ? $aData['surname'] : $aLang['details_no_data']);
			$pForm->AddRow($aLang['details_phone'], $aData['phone'] != '' ? $aData['phone'] : $aLang['details_no_data']);
		} else {
			$pForm->AddRow($aLang['details_type'], $aLang['details_type_private']);
			$pForm->AddRow($aLang['details_name'], !empty($aData['name']) ? $aData['name'] : $aLang['details_no_data']);
			$pForm->AddRow($aLang['details_surname'], !empty($aData['surname']) ? $aData['surname'] : $aLang['details_no_data']);
			$pForm->AddRow($aLang['details_street'], $aData['street'].' '.$aData['number'].(!empty($aData['number2'])?' /'.$aData['number2']:''));
			$pForm->AddRow($aLang['details_city'], !empty($aData['postal'])&&!empty($aData['city'])?$aData['postal'].' '.$aData['city'] : $aLang['details_no_data']);
			$pForm->AddRow($aLang['details_phone'], $aData['phone'] != '' ? $aData['phone'] : $aLang['details_no_data']);
		}
		$pForm->AddRow($aLang['details_created'], $aData['created']);
		if(!empty($aData['modified'])){
			$pForm->AddRow($aLang['details_modified'], $aData['modified']);
		}
		/*$pForm->AddRow($aLang['details_city'], $aData['postal'].' '.$aData['city']);
		$pForm->AddRow($aLang['details_email'], $aData['email']);
		
		$pForm->AddRow($aLang['details_nip'], !empty($aData['nip']) ? $aData['nip'] : $aLang['details_no_data']);
		
		
		$pForm->AddRow($aLang['details_passwd'], decodePasswd($aData['passwd']));
		$pForm->AddMergedRow($aLang['details_invoice_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
		$pForm->AddRow($aLang['details_name_company'], !empty($aData['invoice_company']) ? $aData['invoice_company'] : $aLang['details_no_data']);
		
		$pForm->AddRow($aLang['details_street'], !empty($aData['invoice_street'])?$aData['invoice_street'].' '.$aData['invoice_number'].(!empty($aData['invoice_number2'])?' /'.$aData['invoice_number2']:'') : $aLang['details_no_data']);
		
		*/
	} // end of showUserAddress() method
	
	/**
	 * Pobiera listę adresów usera
	 * @param $int iUserId - id uzytkownika
	 * @return array ref - tablica z adresami
	 */
	function &getUserAddresses($iUserId){
		global $aConfig;
		$sSql = "SELECT *,
										DATE_FORMAT(created, '".$aConfig['common']['sql_date_hour_format']."') created,
										DATE_FORMAT(modified, '".$aConfig['common']['sql_date_hour_format']."') modified
						 FROM ".$aConfig['tabls']['prefix']."users_accounts_address
						 WHERE user_id = ".$iUserId;
		return Common::GetAll($sSql);
	} // end of getUserAddresses() method
	
	
	/**
	 * Metoda pobera listę serwisow dla SELECT'a
	 *
	 * @global array $aConfig
	 * @return array
	 */
	function getWebsitesList() { 
		global $aConfig;
		
		$sSql = "SELECT name AS label, id AS value
						 FROM ".$aConfig['tabls']['prefix']."websites";
		return Common::GetAll($sSql);
	}// end of getWebsitesList() method
} // end of Module Class

?>