<?php

/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-17
 * Time: 09:19
 */
class Module
{

    // symbol wersji jezykowej
    var $sLang;

    // link do strony logowania
    var $sPageLink;

    // sciezka do katalogu z szablonami
    var $sTemplatesPath;

    // szablon boksu
    var $sTemplate;

    // nazwa modulu
    var $sModule;

    // opcja modulu
    var $sOption;

    function Module() {
        global $aConfig;

        $this->sLang =& $_GET['lang'];
        $this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
        $this->sModule = 'm_konta';
        $this->sOption = 'login';
        $this->sTemplatesPath = 'modules/'.$this->sModule.'/registration_landing_page';
        $this->sTemplate = 'registration_landing_page.tpl';
    }

    /**
     * @return mixed|string|void
     */
    function getContent() {
        global $pSmarty, $aConfig;
        $sHtml = '';
        $aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule.'_'.$this->sOption];

        if (!empty($_POST)) {
            if ($this->proceedPost() === true) {
                setMessage(_('Dziękujemy'), false);
            }
        }


        include_once('Form/FormTable.class.php');
        $oForm = new FormTable('userAccountForm',
            '',
            array('action' => $this->sPageLink, 'enctype' => 'multipart/form-data'),
            array(),
            true);



        $aModule['form']['email']['label'] = $oForm->GetLabelHTML('email', _('Email'));
        $aModule['form']['email']['field'] = $oForm->GetTextHTML('email', _('Email'), '', ['style' => 'width: 80vw; height: 10vh; font-size: 33px; text-align:center;'], '', 'email');

        $sRegulations = _('Wyrażam dobrowolną zgodę na przetwarzanie moich danych osobowych w celach marketingowych przez Profit M Spółka z ograniczoną odpowiedzialnością z siedzibą w Warszawie (00-110) Plac Defilad 1, Metro Centrum, lok. 2002D, zgodnie z ustawą o ochronie danych osobowych z dnia 29 sierpnia 1997 r. (tekst jednolity: Dz. U. z 2002 r., Nr 101, poz. 926 z późn. zm.). Zgoda może być odwołana w każdym czasie.');
        $aModule['form']['promotions_agreement']['label'] = $oForm->GetLabelHTML('promotions_agreement', $sRegulations);
        $aModule['form']['promotions_agreement']['field'] = $oForm->GetCheckBoxHTML('promotions_agreement', 'Zgoda na przetwarzanie moich danych osobowych');


        $aModule['form']['header'] = $oForm->GetFormHeader();
        $aModule['form']['footer'] = $oForm->GetFormFooter();
        $aModule['form']['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
            $oForm->GetErrorPrefix(),
            array('id' => 'lF__ErrPrefix'));
        $aModule['form']['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
            $oForm->GetErrorPostfix(),
            array('id' => 'lF__ErrPostfix'));
        $aModule['form']['validator'] = $oForm->GetHiddenHTML('__Validator',
            $oForm->GetValidator(),
            array('id' => 'lF__Validator'));

        $pSmarty->assign_by_ref('aModule', $aModule);
        $sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
        $pSmarty->clear_assign('aModule');
        return $sHtml;
    }

    /**
     *
     */
    private function proceedPost() {
        global $pDbMgr;

        include_once('Form/Validator.class.php');
        $oValidator = new Validator();
        if (!$oValidator->Validate($_POST)) {
            setMessage($oValidator->GetErrorString(), true);
            return false;
        }

        if ($this->userExists($_POST['email']) === false) {
            $aUser = [
                'email' => $_POST['email'],
                'registred' => '0',
                'deleted' => '0',
                'created' => 'NOW()',
                'promotions_agreement' => '1',
                'active' => '1'
            ];
            if ($pDbMgr->Insert('profit24', 'users_accounts', $aUser) === false) {
                setMessage(_('Wystąpił błąd podczas dodawania użytkownika do bazy'), true);
            } else {
                return true;
            }
        }
        return true;
    }

    /**
     * @param $sEmail
     * @return mixed
     */
    private function userExists($sEmail) {
        global $aConfig, $pDbMgr;

        $sSql = 'SELECT id FROM users_accounts WHERE email = '.Common::Quote($sEmail).' AND website_id = "'.$aConfig['profit24_website_id'].'" ';
        return ($pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
    }
}