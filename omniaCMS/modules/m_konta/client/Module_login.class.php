<?php
/**
 * Klasa Module do obslugi logowania dla stron z ograniczonym dostepem
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Module {
  
  // symbol wersji jezykowej
  var $sLang;
  
  // link do strony logowania
  var $sPageLink;
    
	// sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu
  var $sModule;
  
  // opcja modulu
  var $sOption;
  
  /**
   * Konstruktor klasy
   *
   * @return	void
   */
	function Module() {
		$this->sLang =& $_GET['lang'];
		$this->sPageLink = $this->getMyAccountPageSymbol();
		$this->sModule = 'm_konta';
		$this->sOption = 'login';
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/login';
		$this->sTemplate = 'login.tpl';
	} // end Module() function
	
	
	/**
	 * Metoda pobiera Id strony modulu m_konta
	 * 
	 * @return	array
	 */
	function getMyAccountPageSymbol() {
		global $aConfig;
		
		$sSql = "SELECT A.symbol
						 FROM ".$aConfig['tabls']['menus_items']." AS A
						 JOIN ".$aConfig['tabls']['modules']." AS B 
						 ON A.module_id = B.id
						 WHERE B.symbol = 'm_konta'";
		return Common::GetOne($sSql).'/';
	} // end of getMyAccountPageSymbol() function
	
	
	/**
	 * Metoda okresla sciezke do strony Konta uzytkownikow,
	 * dowiazuje ja do Smartow oraz zwraca xHTML z boksem newslettera
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $pSmarty, $aConfig;
		$sHtml = '';
		
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule.'_'.$this->sOption];
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('loginRestrForm',
													 '',
													 array('action' => $this->sPageLink),
													 array(),
													 false);
													 
		$aAttribs = array(
			'class' => 'text'
		);

	 	$aModule['form']['fields']['ref']['input'] = $oForm->GetHiddenHTML('ref', isset($_POST['ref']) ? $_POST['ref'] : base64_encode(substr($_SERVER['REQUEST_URI'], 1)), array('id' => 'm_ref'));
	 	
	 	$aModule['form']['fields']['login']['label'] = $oForm->GetLabelHTML('m_login', $aConfig['lang']['common']['login'], false);
		$aModule['form']['fields']['login']['input'] = $oForm->GetTextHTML('login', $aConfig['lang']['common']['login'], '', array_merge($aAttribs, array('id' => 'm_login')), '', 'login', true, $aConfig['common']['login']['pcre']);
		
		$aModule['form']['fields']['passwd']['label'] = $oForm->GetLabelHTML('m_passwd', $aConfig['lang']['common']['passwd'], false);
		$aModule['form']['fields']['passwd']['input'] = $oForm->GetPasswordHTML('passwd', $aConfig['lang']['common']['passwd'], '', array_merge($aAttribs, array('id' => 'm_passwd')));
		
		$aModule['form']['fields']['remember']['label'] = $oForm->GetLabelHTML('m_remember', $aConfig['lang']['common']['remember'], false);
		$aModule['form']['fields']['remember']['input'] = $oForm->GetCheckboxHTML('remember', $aConfig['lang']['common']['remember'], array('id' => 'm_remember', 'class'=>'checkbox'), '', true, false);
		
		$aModule['form']['fields']['do_login']['input'] = $oForm->GetInputButtonHTML('send', $aConfig['lang']['common']['do_login']);
		
		$aModule['form']['header'] = $oForm->GetFormHeader();
		$aModule['form']['footer'] = $oForm->GetFormFooter();
		$aModule['form']['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																							 	 array('id' => 'lF__ErrPrefix'));
		$aModule['form']['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																							 	 array('id' => 'lF__ErrPostfix'));
		$aModule['form']['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																							 	 array('id' => 'lF__Validator'));

		// linki zaloz konto i przypomnij haslo
		$aModule['links']['register'] = createLink($this->sPageLink, 'register');
		$aModule['links']['change_passwd'] = createLink($this->sPageLink, 'chngpasswd', array('step' => 1));
		
		$pSmarty->assign_by_ref('aModule', $aModule);
    $sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
    $pSmarty->clear_assign('aModule');

		return $sHtml;
	} // end of getContent()
} // end of Box Class
?>