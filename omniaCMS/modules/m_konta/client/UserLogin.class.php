<?php
/**
* Klasa Logowania Uzytkownika serwisu
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version   1.0
*/

class UserLogin {
	
	// link do strony logowania
	var $aPageLink;
		
	// link do strony z ktorej nastepuje logowanie
	// na ta strone nastapi przekierowanie po pomyslnym logowaniu
	var $sReferer;
	
	/**
  * Konstruktor klasy UserLogin
	*/
	function UserLogin($sPageLink='', $sReferer='') {
		$this->sPageLink = $sPageLink;
		$this->sReferer =& $sReferer;
	} // end UserLogin()
	
	
	/**
	 * Metoda zwraca formularz logowania
	 *
	 * @return	array	- tablica z zawartoscia formularza
	 */
	function &getLoginForm($bOrder = false) {
	 	global $aConfig;
	 	$aForm = array();
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable(($bOrder ? 'order_loginForm': 'loginForm'),
													 '',
													 array('action' => $this->sPageLink),
													 array(),
													 false);
													 
		
	 	$aForm['pagelink'] = $this->sPageLink;
	 	$aForm['fields']['login']['label'] = $oForm->GetLabelHTML('email', $aConfig['lang']['common']['email'], true);
		$aForm['fields']['login']['input'] = $oForm->GetTextHTML('email', $aConfig['lang']['common']['email'], '', array('style' => ($bOrder ? 'width: 165px' : 'width: 225px'), 'class' => 'text','maxlength' => 100), '', 'email');
		
		$aForm['fields']['passwd']['label'] = $oForm->GetLabelHTML('passwd', $aConfig['lang']['common']['passwd'], true);
		$aForm['fields']['passwd']['input'] = $oForm->GetPasswordHTML('passwd', $aConfig['lang']['common']['passwd'], '', array('style' => ($bOrder ? 'width: 165px' : 'width: 225px'),'class' => 'text','maxlength' => 32));
		
		$aForm['fields']['remember']['label'] = $oForm->GetLabelHTML('remember', $aConfig['lang']['common']['remember'], false);
		$aForm['fields']['remember']['input'] = $oForm->GetCheckboxHTML('remember', $aConfig['lang']['common']['remember'], array('class'=>'checkbox'), '', true, false);
		
		if ($this->sReferer != '') {
			$aForm['fields']['ref']['input'] = $oForm->GetHiddenHTML('ref', base64_encode($this->sReferer));
		}
		
		
		$aForm['fields']['do_login']['input'] = $oForm->GetInputButtonHTML('send', $aConfig['lang']['common']['do_login']);
		
		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();
		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix());
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix());
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator());
	 	return $aForm;
	} // end of getLoginForm()
	
	
	/** 
	* Metoda wykonuje logowanie
	* Sprawdza czy istnieje taki uzytkownik czy podano dobre haslo
	* 
	* @param	string	$sLogin	- sprawdzany login
	* @param	string	$sPasswd	- sprawdzane haslo
	* @param int $iRemember - 1 - zapamietaj i loguj nastepnym razem automatycznie
	* @return	integer	- -1: konto nieaktywne
	* 									 0: zly login lub haslo
	* 									 1: logowanie poprawne
	*/
	function doLogin($sEmail, $sPasswd,$iRemember=0) {
		global $aConfig;
		
		if (($iID = $this->userExists($sEmail, $sPasswd)) > 0) {
  								
      return $this->doLoginById($iID,$iRemember);
		}
		
		return $iID;
	} // end of doLogin() method
	
	function doLoginFromCookie($sMD5) {
		global $aConfig;
		if (($iID = $this->getUserId($sMD5)) > 0) {

      return $this->doLoginById($iID,1);
		}
		
		return 0;
	} // end of doLoginFromCookie() method
	
	/**
	 * Metoda wykonuje logowanie
	 * @param int $iID
	 * @param int $iRemember - 1 - zapamietaj i loguj nastepnym razem automatycznie
	 * @return integer	-  0: zly login lub haslo
	 * 									 1: logowanie poprawne
	 */
	function doLoginById($iID,$iRemember=0){
		global $aConfig, $pDbMgr;
		$_SESSION['w_user']['id'] = $iID;
      
      $_SESSION['w_user']['ip'] = $_SERVER['REMOTE_ADDR'];
      $_SESSION['w_user']['host'] = gethostbyaddr($_SERVER['REMOTE_ADDR']);
      	
		$sSql = "SELECT CONCAT(A.name, ' ', A.surname) as name, A.discount, A.email
					FROM ".$aConfig['tabls']['prefix']."users_accounts A
					WHERE id = ".$iID."
						AND active = '1'
						AND website_id=".$aConfig['profit24_website_id'];
			
		$aData =& $pDbMgr->GetRow('profit24', $sSql);
		if(!empty($aData)){
			$_SESSION['w_user']['email'] = $aData['email'];
		
			$_SESSION['w_user']['user_name'] = $aData['name'];
		  	$_SESSION['w_user']['discounts'] = (float) $aData['discount'];
		  //  $_SESSION['w_user']['client_type'] = $aData['is_company'];
		    
	    	$_SESSION['w_user']['host'] = gethostbyaddr($_SERVER['REMOTE_ADDR']);
			$_SESSION['wu_check'] = md5($iID.':'.$aData['email']);
				
			AddCookie('wu_check', md5($iID.':'.$aData['email']));
			
			if ($iRemember == 1) {
				// wybrano opcje zapamietania uzytkownika 
				AddCookie('wu_remember', md5($iID.':'.$aData['email'].':'.$aConfig['common']['login_safekey']), time() + 10 * 365 * 24 * 60 * 60);
			}
			elseif (isset($_COOKIE['wu_remember'])) {
				DeleteCookie('wu_remember');
			}	
		
		
			// dolaczenie klasy Common modulu zamowienia
    	include_once('modules/m_zamowienia/client/Common.class.php');
      include_once('modules/m_zamowienia/lang/pl.php');
    	$oOrders = new Common_m_zamowienia('','','','');
    	
    	// kod rabatowy
		/*	if(!empty($_SESSION['wu_cart']['entered_discount_code'])){

				$aCode = $oOrders->checkDiscountCode($_SESSION['wu_cart']['entered_discount_code']);
				if($aCode === false) {
					$_SESSION['code_info'] = 'err';
				} else {
					$_SESSION['wu_cart']['discount_code'] = $aCode;
				}
				unset($_SESSION['wu_cart']['entered_discount_code']);
				}
    	*/
    	
    	// przeliczenie produktow
    	$oOrders->loggedInRecalculateProducts();

	    // zapamietanie repository w bazie
	    if(isset($_COOKIE['repository'])){
	    	$this->addRepositoryToBase();
	    }
	    
		  // zapamietanie aktualnych elementow
		  $aCurrCart=array();
		  if(!empty($_SESSION['wu_cart']['products'])){
				foreach($_SESSION['wu_cart']['products'] as $iKey=>$aProduct) {
					$aCurrCart[$iKey]['id'] = $aProduct['id'];
					$aCurrCart[$iKey]['quantity'] = $aProduct['quantity'];
				}
		  }
		  
			// wczytanie koszyka z bazy
		  restoreCartFromDatabase(true,true);
	
		  // dodanie aktualnych elementow do bazy
		  if(!empty($aCurrCart)) {
		  	foreach($aCurrCart as $iKey=>$aProduct) {
		  		storeAddToDB($aProduct['id'],$aProduct['quantity']);
		  	}
		  }
			
		  serializeCartToCookie();
			
			unset($_SESSION['order']['data']);
			return 1;
		}
		return 0;
	}
	
	
	/** 
	* Metoda wykonuje wylogowanie
	* Usuwa cookie i zamyka sesje
	*
	* @return boolean
	*/
	function doLogout() {
		global $aConfig;
    unset($_SESSION['wu_check']);
    unset($_SESSION['w_user']);
		DeleteCookie('wu_check');
		DeleteCookie('wu_remember');
		
		if (isset($_SESSION['wu_cart']['user_data'])) {
			unset($_SESSION['wu_cart']['user_data']);
		}
		if (isset($_SESSION['wu_cart']['discount_code'])){
			unset($_SESSION['wu_cart']['discount_code']);
		}
		//unset($_SESSION['wu_cart']);
		// dolaczenie klasy Common modulu zamowienia
		include_once('modules/m_zamowienia/client/Common.class.php');
    include_once('modules/m_zamowienia/lang/pl.php');
		$oOrders = new Common_m_zamowienia('','','','');
	//	 przeliczenie produktow
		$oOrders->loggedOutRecalculateProducts();
		//serializeCartToCookie();
	AddCookie("cart", "");
	    unset($_SESSION['order']['data']);
      
    include_once($aConfig['common']['base_path'].'LIB/fb_connector/fbConnector.class.php');
    $oFB = fbConnector::getInstance();
    $oFB->doLogout();
	} // end of doLogout()
	
  
	/**
	 * Metoda sprawdza czy dane sa poprawne
	 * Jezeli tak to zwraca ID uzytkownika
	 * 
	 * @return	integer									- 	0: bledne dane;
	 * 																		> 0: poprawne dane;
	 * 																		 -1: konto nieaktywne
	 * 																		 -2: grupa(y) uzytkownika sa niekatywne
	 */
	function userExists($sEmail, $sPasswd) {
		global $aConfig, $pDbMgr;
    $sEmail = trim($sEmail);
	 	
    $sSalt = $this->_getUserSalt($sEmail);
	 	// sprawdzamy czy o konto o podanym loginie istnieje
	 	$sSql = "SELECT id, active
             FROM ".$aConfig['tabls']['prefix']."users_accounts
             WHERE email=".Common::Quote($sEmail)." AND
									 website_id=".$aConfig['profit24_website_id']." AND
             			 passwd='".encodePasswd($sPasswd, $sSalt)."' AND 
             			 deleted ='0'";
      
	 	$aTemp = &$pDbMgr->GetRow('profit24', $sSql);
    if (empty($aTemp)) {
    	// nie ma takiego uzytkownika
    	return 0;
		}
		elseif($aTemp['active'] == '0') {
    	return -1;
    }
    
    return $aTemp['id'];
	} // end of userExists()
  
  
  /**
   * Metoda pobiera id użytkownika z tabeli kont, dla podanego adresu email,
   *  pobieranie na potrzeby procesu ponownego wysyłania linku aktywacyjnego
   * 
   * @global array $aConfig
   * @global object $pDbMgr
   * @param string $sEmail
   * @return int
   */
  public function getUserIdByEmail($sEmail) {
    global $aConfig, $pDbMgr;
    
    $sSql = "SELECT id, active, email, name, surname
             FROM users_accounts
             WHERE 
              email = ".Common::Quote($sEmail).'
              AND deleted = "0"
              AND website_id = '.$aConfig['profit24_website_id'];
    $aUserData = $pDbMgr->GetRow('profit24', $sSql);
    if ($aUserData['active'] == '0') {
      return $aUserData;
    } else {
      return -1;
    }
  }// end of getUserIdByEmail() method
	
  
  /**
   * Metoda pobiera sól dla zadanego użytkownika
   * 
   * @global array $aConfig
   * @global object $pDbMgr
   * @param int $iUId
   * @return string
   */
  private function _getUserSalt($sEmail) {
    global $aConfig, $pDbMgr;
    
    $sSql = 'SELECT salt 
             FROM users_accounts 
             WHERE email = '.Common::Quote($sEmail).'
                   AND website_id = '.$aConfig['profit24_website_id'].'
                   AND deleted = "0"
                   AND registred = "1"
             LIMIT 1';
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of _getUserSalt() method
  
  
	/**
	 * Metoda zwraca MD5 z ID:login uzytkownika o podanym loginie
	 *  
	 * @return	string
	 */
	function getActivationID($sEmail) {
		global $aConfig, $pDbMgr;
	 	
	 	$sSql = "SELECT MD5(CONCAT(id, ':', email))
             FROM ".$aConfig['tabls']['prefix']."users_accounts
             WHERE email='".$sEmail."'
						 AND website_id=".$aConfig['profit24_website_id']."
             AND deleted='0'";
      
		return $pDbMgr->GetOne('profit24', $sSql);
	} // end of getActivationID()
	
	
	/**
	 * Metoda przenosi produkty z przechowalni z cookies do bazy danych
	 *  
	 */
	function addRepositoryToBase() {
		global $aConfig, $pDbMgr;
	 	
	 	$sRepository = base64_decode($_COOKIE['repository']);
	 	
	 	foreach(explode(",",$sRepository) as $iKey => $iId){
	 		if ($this->productExists($iId)){
		 		$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_briefcase
		 							WHERE konto_id = ".$_SESSION['w_user']['id']."
		 							AND product_id = ".$iId.
		 							($iIt>0?" AND price_id = ".$iIt:'');
		 							
		 		if((int) $pDbMgr->GetRow('profit24', $sSql) != 1){
		 			$aValues = array(
						'konto_id' => $_SESSION['w_user']['id'],
						'product_id' => $iId
					);
					
					$pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."orders_briefcase",
														$aValues, "", false);
		 		}	
	 		}
	 	}
	 	
	 	DeleteCookie("repository");
	} // end of addRepositoryToBase()
	
		/**
	 * Metoda sprawdza czy produkt o podanym typie i Id istnieje
	 *
	 * @param	string	$sType	- typ produktu
	 * @param	integer	$iId	- Id produktu
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function productExists($iId) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT A.id
						 FROM ".$aConfig['tabls']['prefix']."products A
						 JOIN ".$aConfig['tabls']['prefix']."menus_items B
						 ON B.id = A.page_id
						 WHERE A.id = ".$iId." AND
						 			 B.published = '1' AND
						 			 A.published = '1' ";
		return  ((double) $pDbMgr->GetOne('profit24', $sSql)) > 0;
	} // end of productExists() function
	
	/**
	 * Metoda zwraca Id uzytkownika o podanym hashu
	 *  
	 * @return	string
	 */
	function getUserId($sMD5='') {
		global $aConfig, $pDbMgr;
	 	
	 	$sSql = "SELECT id
             FROM ".$aConfig['tabls']['prefix']."users_accounts
             WHERE 
							MD5(CONCAT(id, ':', email, ':".$aConfig['common']['login_safekey']."')) = ".Common::Quote($sMD5)."
							AND website_id=".$aConfig['profit24_website_id'];
      //dump($sSql);
		return $pDbMgr->GetOne('profit24', $sSql);
	} // end of getUserData()	
} // end of class UserLogin
?>