<?php

class UserConfirmation
{
	/**
	 * @var array
	 */
	private $websiteUser;

	/**
	 * UserConfirmation constructor.
	 * @param $websiteUserId
	 */
	public function __construct($websiteUserId)
	{
		global $pDbMgr;
		$this->websiteUser = $pDbMgr->GetRow('profit24', "SELECT id, user_confirmed, created, email FROM users_accounts WHERE id = $websiteUserId");
	}

	public function isUserConfirmed()
	{
		if (1 == $this->websiteUser['user_confirmed']) {
			return true;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function hasUserOldOrders()
	{
		global $pDbMgr, $aConfig;

		$websiteId = $aConfig['website_id'];

		$registerDate = $this->websiteUser['created'];
		$userId = $this->websiteUser['id'];

		$result = $pDbMgr->GetOne('profit24', "select count(*) from orders where order_date < '$registerDate' and user_id = $userId AND website_id = $websiteId");

		if ($result > 0) {
			return true;
		}

		return false;
	}

	/**
	 * @param $code
	 * @return array
	 */
	public function confirmAccount($code)
	{
		global $pDbMgr, $pDB;
		$code = $pDB->quoteSmart(stripslashes($code));

		$response = [
			'message' => 'NieprawidĹ‚owy kod lub wygasĹ‚a jego waĹĽnoĹ›Ä‡',
			'error' => true
		];

		$now = new \DateTime();
		$nowFormatted = $now->format('Y-m-d H:i:s');

		$codeInformation = $pDbMgr->GetRow('profit24', "SELECT id, activation_code, activation_code_sent_at, user_confirmed FROM users_accounts WHERE activation_code = $code");

		if (empty($codeInformation)) {
			return $response;
		}

		if ($codeInformation['activation_code_sent_at'] < $nowFormatted) {
			return $response;
		}

		if ($codeInformation['user_confirmed'] > 0) {
			$response['message'] = 'Konto zostaĹ‚o juĹĽ potwierdzone';
			return $response;
		}

		$values = [
			'activation_code' => null,
			'user_confirmed' => 1
		];

		$userId = $codeInformation['id'];

		$res = $pDbMgr->Update('profit24', 'users_accounts', $values, "id = $userId");

		if (false == $res) {
			$response['message'] = 'Nie udaĹ‚o siÄ™ potwierdziÄ‡ adresu, skontaktuj siÄ™ z obsĹ‚ugÄ…';
			return $response;
		}

		return [
			'message' => 'Twoje zamĂłwienia zostaĹ‚y przywrĂłcone',
			'error' => false
		];
	}

	/**
	 * @return bool
	 */
	public function sendConfirmationMail()
	{
		global $aConfig;

		$code = $this->generateConfirmationCode();

		if ($code === false) {
			return false;
		}

		global $pDbMgr;
		$now = new \DateTime();
		$now->modify('+ 6 hours');
		$userId = $this->websiteUser['id'];

		$values = [
			'activation_code' => $code,
			'activation_code_sent_at' => $now->format('Y-m-d H:i:s')
		];

		$res = $pDbMgr->Update('profit24', 'users_accounts', $values, "id = $userId");

		if (false === $res){
			return false;
		}

		$websiteSymbol = $pDbMgr->GetOne('profit24', "SELECT code from websites where id =".$aConfig['website_id']);

		if (empty($websiteSymbol)) {
			return false;
		}

		$aSiteSettings = getSiteSettings($websiteSymbol);
		$sFrom = $aConfig['default']['website_name'];
		$sFromMail = $aSiteSettings['users_email'];

		$activationLink = str_replace('http://', 'https://', $aConfig['common'][$websiteSymbol]['client_base_url_http_no_slash']).'/moje-konto/activate-mail.html?code='.$code;

		$aMail = Common::getWebsiteMail('user_confirmation', $websiteSymbol);

		return Common::sendWebsiteMail($aMail, $sFrom, $sFromMail, $this->websiteUser['email'],  $aMail['content'], ['{activation_link}', 'expiration_time'], [$activationLink, $now->format('Y-m-d H:i:s')], [], [], $websiteSymbol);
	}

	/**
	 * @return string
	 */
	private function generateConfirmationCode()
	{
		$length = 20;
		global $pDbMgr;

		for($i = 0; $i <= 10; $i++) {

			$code =  md5(substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length).microtime());

			$exist = $result = $pDbMgr->GetOne('profit24', "select count(*) from users_accounts where activation_code = '$code'");

			if ($exist < 1 && false !== $exist) {
				return $code;
			}
		}

		return false;
	}

	/**
	 * @return array
	 */
	public function getWebsiteUser()
	{
		return $this->websiteUser;
	}
}
