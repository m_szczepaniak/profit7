<?php
/**
* Klasa Sald Uzytkownika serwisu
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version   1.0
*/

class UserBalances {
	
	// link do strony modulu konta uzytkownikow
	var $sPageLink;
	
	// Id uzytkownika
	var $iUserId;
	
	// liczba artykulow na stronie
  var $iPerPage;

  // symbol modulu
  var $sModule;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
	
	/**
  * Konstruktor klasy UserBalances
	*/
	function UserBalances($sPageLink, $iUserId, $iPerPage) {
		global $aConfig;
		
		$this->sPageLink = $sPageLink;
		$this->iUserId = $iUserId;
		$this->iPerPage = $iPerPage;
		$this->sModule =& $aConfig['_tmp']['page']['module'];
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/';
	} // end User()
	
	function getUserBalances() {
		global $aConfig;
		$aBalances = array();
		$sSql = "SELECT balance
						FROM ".$aConfig['tabls']['prefix']."users_accounts
						WHERE id = ".$this->iUserId;
		$aBalances['balance'] = Common::formatPrice(Common::GetOne($sSql));
		return $aBalances;
	}
	
	
	/**
	 * Metoda pobiera zamowienia uzytkownika o Id $this->iUserId
	 * 
	 * @return	void
	 
	function &getUserBalances() {
		global $aConfig;
		$aBalances = array();
		
		
		// okreslenie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."orders_balances
						 WHERE user_id = ".$this->iUserId;
						 
		if (($iRecords = (int) Common::GetOne($sSql)) > 0) {
			// pobranie listy zamowien uzytkownika		
			$sSql = "SELECT A.*, B.payment, (B.value_brutto+B.transport_cost) AS order_value
							 FROM ".$aConfig['tabls']['prefix']."orders_balances A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."orders B
							 ON B.id=A.order_id
							 WHERE A.user_id = ".$this->iUserId."
							 ORDER BY A.id DESC";
							 
			if ($this->iPerPage > 0) {
				// uzycie pagera do stronicowania
				include_once('Pager/CPager.class.php');
				$aPagerParams = array();
				$aPagerParams['perPage'] = $this->iPerPage;
				$aPagerParams['path'] = $aConfig['common']['base_url_https_no_slash'].
																$this->sPageLink;
				$aPagerParams['fileName'] = 'balance,p%d.html';
				$aPagerParams['append'] = false;

				$oPager = new CPager($iRecords, $aPagerParams);
				$iStartFrom = $oPager->getStartFrom();

				// linki Pagera
				$aBalances['pager']['links'] =& $oPager->getLinks('all');
				$aBalances['pager']['total_pages'] =& $oPager->getLinks('total_pages');
				$aBalances['pager']['total_items'] =& $oPager->getLinks('total_items');
				$aBalances['pager']['current_page'] =& $oPager->getLinks('current_page');

				// dodanie wybrania zakresu do zapytania
				$sSql .= " LIMIT ".$iStartFrom.", ".$this->iPerPage;
			} 
			$aBalances['balances'] =& Common::GetAll($sSql);
			$iLp=$iRecords-$iStartFrom;
			foreach ($aBalances['balances'] as $iKey => $aItem) {
				$aBalances['balances'][$iKey]['lp'] = $iLp;
				$iLp--;
				$aBalances['balances'][$iKey]['paid_amount'] = Common::formatPrice($aItem['paid_amount']);
				$aBalances['balances'][$iKey]['balance'] = Common::formatPrice($aItem['balance']);
				$aBalances['balances'][$iKey]['order_value'] = Common::formatPrice($aItem['order_value']);
				
				
				//$aBalances['balances'][$iKey]['payment_date']
			
				if(!empty($aItem['order_id'])){
					$aBalances['balances'][$iKey]['order_link'] = createLink($this->sPageLink,'id'.$aItem['order_id'].',orders');
				}
				
			}
		}
		return $aBalances;
	} // end of getUserBalances() function
	*/


} // end of class UserOrders
?>