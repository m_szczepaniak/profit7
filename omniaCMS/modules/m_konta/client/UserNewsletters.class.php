<?php
/**
* Klasa Newsletterów Uzytkownika serwisu
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version   1.0
*/

class UserNewsletters {
	
	// link do strony modulu konta uzytkownikow
	var $sPageLink;
	
	// Id uzytkownika
	var $iUserId;
	
	// Email uzytkownika
	var $sUserEmail;
	
	// liczba artykulow na stronie
  var $iPerPage;

  // symbol modulu
  var $sModule;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
	
	/**
  * Konstruktor klasy UserNewsletters
	*/
	function UserNewsletters($sPageLink, $iUserId, $sUserEmail) {
		global $aConfig;
		
		$this->sPageLink = $sPageLink;
		$this->iUserId = $iUserId;
		$this->sUserEmail = $sUserEmail;
		$this->iPerPage = $iPerPage;
		$this->sModule =& $aConfig['_tmp']['page']['module'];
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/';
	} // end User()
	
	
	/**
	 * Metoda pobiera zamowienia uzytkownika o Id $this->iUserId
	 * 
	 * @return	void
	 */
	function &getNewsletters() {
		global $aConfig;
		
		
	include_once('modules/m_newsletter/client/Newsletter.class.php');
 	$oNewsletter = new Newsletter('m_newsletter', 0, array(), '');
 	$oNewsletter->setSubscriberDataByEmail($this->sUserEmail);

 	$aModule['categories_groups'] =& $oNewsletter->getCategoriesGroupsList();
	// pobranie kategorii newslettera
	$aModule['categories'] =& $oNewsletter->getCategoriesList();
	$aModule['n_categories'] = $oNewsletter->getCategories();

		return $aModule;
	} // end of getNewsletters() function
	
	
	/**
	 * Metoda aktualizuje ustawienia newslettera 
	 * @version Arkadiusz Golba mod - dodawanie użytkownika do newslettera jeśli nie istnieje
	 *
	 * @global array() $aConfig
	 * @param array() $aCategories
	 * @return int|boolean 
	 */
	function editNewsletters($aCategories) {
		
		include_once('modules/m_newsletter/client/Newsletter.class.php');
		$oNewsletter = new Newsletter('m_newsletter', 0, array(), '');
		if ($oNewsletter->recipientExists($this->sUserEmail) > 0) {
			$oNewsletter->setSubscriberDataByEmail($this->sUserEmail);
			return $oNewsletter->edit($this->sUserEmail,$aCategories);
		} else {
			if ($oNewsletter->add($this->sUserEmail, $aCategories, true) == -1) {
				// nie udalo sie dodac newslettera
				Common::RollbackTransaction();
				return 0;
			} else {
				return true;
			}
		}
		return false;
	}// end of editNewsletters() method
} // end of class UserNewsletters
?>