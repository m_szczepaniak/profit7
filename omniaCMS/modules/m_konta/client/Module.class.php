<?php

include_once('modules/m_konta/client/UserConfirmation.php');

/**
 * Klasa odule do obslugi Konta uzytkownika
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */

class Module {

  // Id strony
  var $iPageId;

  // szablon strony
  var $sTemplate;

  // sciezka do katalogu z szablonami
  var $sTemplatesPath;

  // symbol modulu
  var $sModule;

  // ustawienia strony modulu
  var $aSettings;

  // link do strony modulu
  var $sPageLink;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;

		$this->iPageId =& $aConfig['_tmp']['page']['id'];
		$this->sModule =& $aConfig['_tmp']['page']['module'];
		$this->sTemplate = 'modules/'.$this->sModule.'/domyslny.tpl';
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/';
		$this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
		$this->aSettings =& $this->getSettings();
	} // end Module() function


	/**
	 * Metoda pobiera tresc strony o id $this->iPageId
	 * dowiazuje ja do Smartow oraz zwraca xHTML z trescia strony po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$bParseTemplate = true;
		
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['page_link'] =& $this->sPageLink;
		$aModule['template'] = $this->sTemplatesPath;

		if ($_GET['action'] == 'activate-mail') {

			$userConfirmation = new UserConfirmation($_SESSION['w_user']['id']);
			$activationResult = $userConfirmation->confirmAccount($_GET['code']);

			setMessage($activationResult['message'], $activationResult['error'], true);

			doRedirect('/');
			die;
		}

		if (!$aModule['is_logged_in'] = isLoggedIn()) {
			// uzytkownik nie jest zalogowany
			// linki zaloz konto i przypomnij haslo
			$aModule['links']['register'] = createLink($this->sPageLink, 'register');
			$aModule['links']['register2'] = createLink($this->sPageLink, 'register_step2');
			$aModule['links']['remind_passwd'] = createLink($this->sPageLink, 'remind-passwd');

			switch ($_GET['action']) {
        case 'fb-logowanie':
          $sBaseURL = str_replace('https://', '', $aConfig['common']['client_base_url_http']);
          $sLastURL = '/moje-konto';
          if (stristr($_SERVER['HTTP_REFERER'], $sBaseURL) !== FALSE) {
            $sLastURL = $_SERVER['HTTP_REFERER'];
          }
          
          // połączenie kont
          include_once($aConfig['common']['base_path'].'LIB/fb_connector/fbConnector.class.php');
          $oFB = fbConnector::getInstance();
          $iFbUId = $oFB->getFBUId();
          $iUserId = $oFB->getUserIdByFBUId($iFbUId);
          $aUData = $oFB->getUserData();
          
          // czy przypadkiem juz nie został zalogowany
          if (!isLoggedIn()) {
            // czy jest id konta z facebooka, czy przesłane zostały dane konta, oraz moze juz ma konto
            if ($iFbUId > 0 && !empty($aUData)) {
              $mRetCon = $oFB->connectAccounts($iFbUId, $aUData);
              if ($mRetCon === 1) { // konta zostały połączone
                // konta zostały połączone
                setMessage(_('Zostałeś pomyślnie zalogowany'), false, true);
                doRedirect($sLastURL);
              } elseif ($mRetCon === 2) {
                // zarejestrowamy
                setMessage(_('Witamy w serwisie Profit24.pl<br /> Zostałeś pomyślnie zalogowany'), false, true);
                doRedirect($sLastURL);
              } elseif ($mRetCon === 3) {
                // zarejestrowamy i gitara
                setMessage(_('Witamy w serwisie Profit24.pl'), false, true);
                doRedirect($sLastURL);
              } else {
                setMessage(_('Wystąpił błąd podczas komunikacji z serwisem Facebook Kod: '.$mRetCon), true, true);
                doRedirect('/');
              }
            } else {
              setMessage(_('Wystąpił błąd podczas komunikacji z serwisem Facebook Kod: 102'), true, true);
              doRedirect('/');
            }
          } else {
            // zalogowani
            doRedirect($sLastURL);
          }
        break;
        
				case 'register':
				///	dump($this->sPageLink);
						$aModule['template'] .= '_registrationStep1Form.tpl';
						$aConfig['_tmp']['page']['name'] .= $aModule['lang']['registration_header'];
						// dolaczenie klasy User
						include_once('User.class.php');
					
						$oUser = new User($this->sModule,
														$this->iPageId,
														$this->aSettings,
														$aModule['links']['register']);
						if (!empty($_POST) && !isset($_POST['cart'])) {
              if (isset($_POST['email'])) {
                $_POST['email']= trim($_POST['email']);
              }
              if (isset($_POST['confirm_email'])) {
                $_POST['confirm_email']= trim($_POST['confirm_email']);
              }
							// dolaczenie klasy walidatora
							include_once('Form/Validator.class.php');
							$oValidator = new Validator();
							if (!$oValidator->Validate($_POST)) {
								setMessage($oValidator->GetErrorString(), true);
								// ponowne wyswietlenie formularz logowania
								$aModule['form'] =& $oUser->getRegistrationStep1Form($_POST['ref']);
							}
							elseif($_POST['email'] !== $_POST['confirm_email']) {
								setMessage($aModule['lang']['email_confirm_err'], true);
								$aModule['form'] =& $oUser->getRegistrationStep1Form($_POST['ref']);
							}
							elseif($oUser->emailExists($_POST['email'])) {
								setMessage(sprintf($aModule['lang']['email_exists'],
																		 $_POST['email']), true);
								$aModule['form'] =& $oUser->getRegistrationStep1Form($_POST['ref']);
							}
							else {
								// zarejestruj i wyslij linka
								$oUser->saveStep1Data();
								
								
								// tworzenie konta uzytkownika
								switch (($iUserID = $oUser->add(true, true))) {
									case -2:
										// konto o podanym adresie email juz istnieje
										setMessage(sprintf($aModule['lang']['email_exists'],
																			 $_SESSION['registration']['email']), true);
										$aModule['form'] =& $oUser->getRegistrationStep2Form($_SESSION['registration']['ref_link']);
									break;
	
									case 0:
										// wystapil blad
										setMessage(sprintf($aModule['lang']['add_err'],
																			 $aConfig['_settings']['site']['email'],
																			 $aConfig['_settings']['site']['email']), true);
										$aModule['form'] =& $oUser->getRegistrationStep2Form($_SESSION['registration']['ref_link']);
									break;
	
									default:
										// konto zostalo utworzone
										// link potwierdzajacy tworzenie konta
                    /*
										$sActivationLink = createLink($this->sPageLink,
																									'Login',
																									'',
																									array('u'=>md5($iUserID.':'.strtolower($_POST['email'])), 'action' => 'activate'),
																									true).($_SESSION['registration']['ref_link'] != '' ?
																												'?sed='.$_SESSION['registration']['ref_link'] : '');
                     */
										// konto zostalo utworzone
										$oUser->sendRegistrationMail($_SESSION['registration']['email'],
																							 $_SESSION['registration']['orig_passwd'],
																							 $_SESSION['registration']['name'],
																							 $_SESSION['registration']['surname']);
																							 
										/*
										// dolaczenie klasy UserLogin
										include_once('UserLogin.class.php');
										$oLogin = new UserLogin($this->sPageLink);
										if($oLogin->doLoginById($iUserID)){
											$_COOKIE['wu_check'] = $_SESSION['wu_check'];
										}
											*/												 
										// rejestracja powiodlo sie
										if (isset($_POST['ref'])) {
											// komunikat o powodzeniu
											setMessage(sprintf($aModule['lang']['add_ok'],
																				 $_SESSION['registration']['email'],
																				 $aConfig['_settings']['site']['email'],
																				 $aConfig['_settings']['site']['email']),false,true, 100000);
																				 
											
											// przeslano linke strony na ktora ma byc powrot po zarejestrowaniu
											$sRefLink = base64_decode($_SESSION['registration']['ref_link']);
//										dump($_SESSION['registration']);
//										dump($sRefLink); die();
											unset($_SESSION['registration']);
											doRedirectHttps($sRefLink);
										}
										else {
											// komunikat o powodzeniu
											setMessage(sprintf($aModule['lang']['add_ok'],
																				 $_SESSION['registration']['email'],
																				 $aConfig['_settings']['site']['email'],
																				 $aConfig['_settings']['site']['email']), false, true, 100000);
																				 
											unset($_SESSION['registration']);
                      doRedirect('/');
		
											// ustawienie nieparsowania szablonu
											$bParseTemplate = false;
										}
										
									break;
								}
								
								//doRedirectHttps(createLink($this->sPageLink,'register_step2'));
							}
						} else {
							$aModule['form'] =& $oUser->getRegistrationStep1Form($_POST['ref']);
						}
				break;	
				
				case 'activate':
					
					addToPath(array(createLink($this->sPageLink, 'edit'),$aModule['lang']['edit_header']));
					addToPath(array(createLink($this->sPageLink, 'edit_addresses'),$aModule['lang']['edit_addresses_header']));
					addToPath(array(createLink($this->sPageLink, 'add_address'),$aModule['lang']['add_addresses_header']));
					$aModule['edit_mainpage_link'] = createLink($this->sPageLink, 'edit');
					$aModule['edit_back_link'] = createLink($this->sPageLink, 'edit_addresses');
					$aModule['buttons']['edit']['selected'] = true;
					
					if ($_GET['sed'] != '') {
						$_SESSION['registration']['ref_link'] = $_GET['sed'];
						//dump($_SESSION['registration']);
						//die;
					}
					//dump($_SESSION['registration']['ref_link']);
					//die;
						
					// dolaczenie klasy User
					if (isset($_GET['u'])) {
						$_GET['parameters'][0] = $_GET['u'];
					}
					include_once('User.class.php');
					$oUser = new User($this->sModule,
														$this->iPageId,
														$this->aSettings,
														$aModule['links']['register'],
														$_GET['parameters'][0]);
					if ($oUser->userExists()) {
						switch ($oUser->activate()) {
							case -1:
								setMessage($aModule['lang']['already_active'], true);
								
								// wyswietlenie formularza logowania
								$aModule['template'] .= '_loginForm.tpl';
								// dolaczenie klasy UserLogin
								include_once('UserLogin.class.php');
								$oLogin = new UserLogin($this->sPageLink, strpos($_SERVER['HTTP_REFERER'], $aConfig['common']['client_base_url']) != false ? $_SERVER['HTTP_REFERER'] : '');
								$aModule['form'] =& $oLogin->getLoginForm();
							break;

							case 0:
								setMessage(sprintf(getWebsiteMessage('aktywacja_blad'),
																	 $aConfig['_settings']['site']['email'],
																	 $aConfig['_settings']['site']['email']), true);
								
								// wyswietlenie formularza logowania
								$aModule['template'] .= '_loginForm.tpl';
								// dolaczenie klasy UserLogin
								include_once('UserLogin.class.php');
								$oLogin = new UserLogin($this->sPageLink, strpos($_SERVER['HTTP_REFERER'], $aConfig['common']['client_base_url']) != false ? $_SERVER['HTTP_REFERER'] : '');
								$aModule['form'] =& $oLogin->getLoginForm();
					
							break;

							case 1:
								//unset($_SESSION['registration']['ref_link']);
								setMessage(getWebsiteMessage('aktywacja_ok'), false, true);
								// dolaczenie klasy UserLogin
								include_once('UserLogin.class.php');
								$oLogin = new UserLogin($this->sPageLink);
								if($oLogin->doLoginById($oUser->aUser['id'])){
									$_COOKIE['wu_check'] = $_SESSION['wu_check'];
								}
								/*
								// ponowne wyswietlenie formularz logowania
								$aModule['template'] .= '_registrationStep2Form.tpl';
								$aModule['form'] =& $oUser->getRegistrationStep2Form();
								*/
								doRedirectHttps($this->sPageLink);
							break;
						}
					} else {
						// wyswietlenie formularza logowania
						$aModule['template'] .= '_loginForm.tpl';
						// dolaczenie klasy UserLogin
						include_once('UserLogin.class.php');
						$oLogin = new UserLogin($this->sPageLink, strpos($_SERVER['HTTP_REFERER'], $aConfig['common']['client_base_url']) != false ? $_SERVER['HTTP_REFERER'] : '');
						$aModule['form'] =& $oLogin->getLoginForm();
					}
					
				break;

				case 'remind-passwd':
					// przypominanie hasla
					// dolaczenie klasy User
					include_once('User.class.php');
					$oUser = new User($this->sModule, $this->iPageId, $this->aSettings,$aModule['links']['remind_passwd']);
						
					// formularz przypominania hasla - adres e-mail uzytkownika
					$aModule['template'] .= '_remindPasswdForm.tpl';
					$aConfig['_tmp']['page']['name'] .= $aModule['lang']['remind_passwd_header'];
					
					if (isset($_POST['passwd-change-email'])) {
						// dolaczenie klasy walidatora
						include_once('Form/Validator.class.php');
						$oValidator = new Validator();
            if ($_COOKIE['captcha_sec'] !== base64_encode(md5($_POST['cap']))) {
              $oValidator->sError .= '<li>'._('Tekst z obrazka').'</li>';
            }
            
						if (!$oValidator->Validate($_POST)) {
							setMessage($oValidator->GetErrorString(), true);
							// ponowne wyswietlenie formularza
							$aModule['form'] = $oUser->getRemindPasswdForm();
						}
						else {
							if ($oUser->userEmailExists(strtolower($_POST['passwd-change-email']))) {
								// wyslanie maila z haslem uzytkownika
								$oUser->sendUserPasswordEmail($_POST['passwd-change-email'], $oUser->getUserPassword(strtolower($_POST['passwd-change-email'])));
								setMessage(sprintf($aModule['lang']['user_password_sent'], $_POST['passwd-change-email']));
								// wylaczenie parsowania szablonu
								$bParseTemplate = false;
							}
							else {
								// nie ma konta o podanym adresie e-mail
								setMessage(sprintf($aModule['lang']['email_doesnt_exist'], $_POST['passwd-change-email']), true);
								$aModule['form'] = $oUser->getRemindPasswdForm();
							}
						}
					}
					else {
						$aModule['form'] = $oUser->getRemindPasswdForm();
					}
				break;
        
        case 'resend-activation':
          $aModule['template'] .= '_loginForm.tpl';
          $aConfig['_tmp']['page']['name'] .= $aModule['lang']['login_header'];
          
          $sEmail = base64_decode($_GET['email']);
					include_once('UserLogin.class.php');
					$oLogin = new UserLogin($this->sPageLink);
          $aUserData = $oLogin->getUserIdByEmail($sEmail);
          if (!empty($aUserData) && is_array($aUserData) && $aUserData['id'] > 0) {
            // wyślij ponownie link aktywacyjny konto
            // link potwierdzajacy tworzenie konta
            $sActivationLink = createLink($this->sPageLink,
                                          'Login',
                                          '',
                                          array('u'=>md5($aUserData['id'].':'.strtolower($sEmail)), 'action' => 'activate'),
                                          true);
            include_once('User.class.php');
            $oUser = new User($this->sModule, $this->iPageId, $this->aSettings, $aModule['links']['remind_passwd']);
            // konto zostalo utworzone
            $oUser->sendActivationMail($aUserData['email'],
                                       $aUserData['name'],
                                       $aUserData['surname'],
                                       $sActivationLink);
            setMessage(sprintf($aModule['lang']['ok_send_account_active'], 
                               $sEmail, 
                               $aConfig['_settings']['site']['email'],
                               $aConfig['_settings']['site']['email']
                        ), false, false, 999999);
          } elseif ($aUserData == -1) {
            // błąd
            setMessage($aModule['lang']['err_account_active'], true, false, 999999);
          } else {
            // błąd
            setMessage($aModule['lang']['err_resend_activation'], true, false, 999999);
          }
          
          // ponowne wyswietlenie formularz logowania
          $aModule['form'] =& $oLogin->getLoginForm();
        break;

				default:
					$aModule['template'] .= '_loginForm.tpl';
					$aConfig['_tmp']['page']['name'] .= $aModule['lang']['login_header'];
					$sRef = base64_decode($_POST['ref']);
					// dolaczenie klasy UserLogin
					include_once('UserLogin.class.php');
					$oLogin = new UserLogin($this->sPageLink, strpos($_SERVER['HTTP_REFERER'], $aConfig['common']['client_base_url']) != false ? $_SERVER['HTTP_REFERER'] : '');
					if (!empty($_POST)) {
						// dolaczenie klasy walidatora
						include_once('Form/Validator.class.php');
						$oValidator = new Validator();
						if (!$oValidator->Validate($_POST)) { 
							if (isset($_POST['ref']) && ($sRef == '/koszyk/step2.html#cart')) {
								setMessage($oValidator->GetErrorString(), true, true);
								// przeslano linke strony na ktora ma byc powrot po zalogowaniu
								$_POST['ref'] = base64_decode($_POST['ref']);
								doRedirectHttps($_POST['ref']);
							}
							else {
								setMessage($oValidator->GetErrorString(), true);
	
								// ponowne wyswietlenie formularz logowania
								$aModule['form'] =& $oLogin->getLoginForm();
							}
						}
						else {
							// logowanie
							
							switch ($oLogin->doLogin($_POST['email'], $_POST['passwd'],intval($_POST['remember']))) {
								case -1:
                // konto nieaktywne
                $sActivationUrl = createLink($this->sPageLink, 'resend-activation').'?email='.base64_encode($_POST['email']);
								if (isset($_POST['ref']) && ($sRef == '/koszyk/step2.html#cart')) {
										setMessage(sprintf($aModule['lang']['not_activated_account'], $sActivationUrl), true, true, 999999);
										$_POST['ref'] = base64_decode($_POST['ref']);
										doRedirectHttps($_POST['ref']);
									}
									else {
										// zly login lub haslo
										setMessage(sprintf($aModule['lang']['not_activated_account'], $sActivationUrl), true, false, 999999);
	
								  	// ponowne wyswietlenie formularz logowania
										$aModule['form'] =& $oLogin->getLoginForm();
									}
								break;
								case 0:				
								if (isset($_POST['ref']) && ($sRef == '/koszyk/step2.html#cart')) {
										// zly login lub haslo
										setMessage($aModule['lang']['bad_login'], true, true, 999999);
										// przeslano linke strony na ktora ma byc powrot po zalogowaniu
										$_POST['ref'] = base64_decode($_POST['ref']);
										doRedirectHttps($_POST['ref']);
									}
									else {
										// zly login lub haslo
										setMessage($aModule['lang']['bad_login'], true, false, 999999);
	
								  	// ponowne wyswietlenie formularz logowania
										$aModule['form'] =& $oLogin->getLoginForm();
									}
								break;

								case 1:
								
									// logowanie powiodlo sie
									if (isset($_POST['ref'])) {
										// przeslano linke strony na ktora ma byc powrot po zalogowaniu
										$_POST['ref'] = base64_decode($_POST['ref']);
										doRedirectHttps($_POST['ref']);
									}
									else {
										// ponowne wywolanie strony typu Konta uzytkownikow
										doRedirectHttps($this->sPageLink);
									}
								break;
							}
						}
					}
					else {
						// wyswietlenie formularz logowania
						$aModule['form'] =& $oLogin->getLoginForm();
					}
				break;
			}
		}
		else {
			// uzytkownik jest zalogowany
			// zestaw przyciskow
			$aModule['buttons'] = array(
				'edit' => array(
					'label' => $aModule['lang']['edit'],
					'link' => createLink($this->sPageLink, 'edit'),
					'info' => $aModule['lang']['edit_info'],
					'selected' => false
				),
				//'balance' => array(
				//	'label' => $aModule['lang']['balance'],
				//	'link' => createLink($this->sPageLink, 'balance'),
				//	'info' => $aModule['lang']['balance_info'],
				//	'selected' => false
				//),
				'orders' => array(
					'label' => $aModule['lang']['orders'],
					'link' => createLink($this->sPageLink, 'orders'),
					'info' => $aModule['lang']['orders_info'],
					'selected' => false
				),
				'newsletters' => array(
					'label' => $aModule['lang']['newsletters_label'],
					'link' => createLink($this->sPageLink, 'newsletters'),
					'info' => $aModule['lang']['newsletters_info'],
					'selected' => false
				),
				'connections' => array(
					'label' => $aModule['lang']['connections_header'],
					'link' => createLink($this->sPageLink, 'connections'),
					'info' => $aModule['lang']['connections_info'],
					'selected' => false
				)
			);	
			//dump($_SESSION['w_user']['discounts']);
			$aUserPrivs = $this->getSpecialUserPrivileges();
			//if($_SESSION['w_user']['discounts'] > 0 || $aUserPrivs['allow_14days'] == '1' || $aUserPrivs['allow_personal_reception'] == '1' || $aUserPrivs['allow_free_transport'] == '1'){
				$aModule['buttons']['discount']= array(
					'label' => $aModule['lang']['discount'],
					'link' => createLink($this->sPageLink, 'discount'),
					'info' => $aModule['lang']['discount_info'],
					'selected' => false
				);	
			//}

			$aRemember = array('edit', 'orders', 'discount_codes', 'newsletters', 'edit_userdata', 'edit_addresses', 'password', 'change_email');
			
			if (!isset($_GET['action'])) {
				if (isset($_COOKIE['myaccacc'])) {
					$_GET['action'] = $_COOKIE['myaccacc'];
				}
				else {
					$_GET['action'] = 'orders';
				}
			}
			
			in_array($_GET['action'], $aRemember) ? AddCookie('myaccacc', $_GET['action']) : '';
			
			if (isset($aModule['buttons'][$_GET['action']])) {
				$aModule['buttons'][$_GET['action']]['selected'] = true;
			}
			
			switch ($_GET['action']) {
				case 'logout':
					// wylogowanie uzytkownika
					// dolaczenie klasy UserLogin
					include_once('UserLogin.class.php');
					$oLogin = new UserLogin($this->sPageLink, strpos($_SERVER['HTTP_REFERER'], $aConfig['common']['client_base_url']) != false ? $_SERVER['HTTP_REFERER'] : '');
					$oLogin->doLogout();
					doRedirectHttps($_SERVER['HTTP_REFERER']);
				break;
      
				case 'register':
          doRedirect('/');
          die;
          //XXXXXXXXXXXXXXXXXXX
					// adresy podstawowe
					// dolaczenie klasy User
					include_once('User.class.php');
					$oUser = new User($this->sModule,
														$this->iPageId,
														$this->aSettings,
														createLink($this->sPageLink, 'discount'),
														'','','',$_SESSION['w_user']['id']);
					$aAddresses = $oUser->getAddresses();
					if (!empty($aAddresses)) {
						doRedirect('/');
					}
					
					addToPath(array(createLink($this->sPageLink, 'edit'),$aModule['lang']['edit_header']));
					addToPath(array(createLink($this->sPageLink, 'edit_addresses'),$aModule['lang']['edit_addresses_header']));
					addToPath(array(createLink($this->sPageLink, 'add_address'),$aModule['lang']['add_addresses_header']));
					$aModule['edit_mainpage_link'] = createLink($this->sPageLink, 'edit');
					$aModule['edit_back_link'] = createLink($this->sPageLink, 'edit_addresses');
					$aModule['buttons']['edit']['selected'] = true;
					
					$aModule['template'] .= '_registrationStep2Form.tpl';
					
					//if(!empty($_SESSION['registration'])){


						// dolaczenie klasy User
						include_once('User.class.php');
						
						$oUser = new User($this->sModule,
															$this->iPageId,
															$this->aSettings,
															createLink($this->sPageLink, 'register'), '', '', '', $_SESSION['w_user']['id']);
						if (!empty($_POST)) {
							// dolaczenie klasy walidatora
							include_once('Form/Validator.class.php');
							$oValidator = new Validator();
							if (!$oValidator->Validate($_POST)) {
								setMessage($oValidator->GetErrorString(), true);
	
								// ponowne wyswietlenie formularz logowania
								$aModule['form'] =& $oUser->getRegistrationStep2Form($_SESSION['registration']['ref_link']);
	
							}
							else {
								if(isset($_SESSION['registration']['ref_link'])){
									$_POST['ref'] = $_SESSION['registration']['ref_link'];
								}
								// dodawania domyślnych danych przesyłki i faktury
								switch (($iUserID = $oUser->add(true, false, $oUser->aUser['id']))) {
									case -2:
										// konto o podanym adresie email juz istnieje
										setMessage(sprintf($aModule['lang']['email_exists'],
																			 $_SESSION['registration']['email']), true);
										$aModule['form'] =& $oUser->getRegistrationStep2Form($_SESSION['registration']['ref_link']);
									break;
	
									case 0:
										// wystapil blad
										setMessage(sprintf($aModule['lang']['add_err'],
																			 $aConfig['_settings']['site']['email'],
																			 $aConfig['_settings']['site']['email']), true);
										$aModule['form'] =& $oUser->getRegistrationStep2Form($_SESSION['registration']['ref_link']);
									break;
	
									default:
										// konto zostalo utworzone
																		
										/*
										$oUser->sendRegistrationMail($_SESSION['registration']['email'],
																							 $_SESSION['registration']['orig_passwd'],
																							 $_SESSION['registration']['name'],
																							 $_SESSION['registration']['surname'],
																							 $this->sPageLink);
																							 */
										// dolaczenie klasy UserLogin
										include_once('UserLogin.class.php');
										$oLogin = new UserLogin($this->sPageLink);
										if($oLogin->doLoginById($iUserID)){
											$_COOKIE['wu_check'] = $_SESSION['wu_check'];
										}
																							 
										// rejestracja powiodlo sie
										if (isset($_POST['ref'])) {
											// komunikat o powodzeniu
											setMessage(sprintf($aModule['lang']['address_add_ok']), false, true);
																				 
											
											// przeslano linke strony na ktora ma byc powrot po zarejestrowaniu
											$sRefLink = base64_decode($_SESSION['registration']['ref_link']);
//										dump($_SESSION['registration']);
//										dump($sRefLink); die();
											unset($_SESSION['registration']);
											doRedirectHttps($sRefLink);
										}
										else {
											// komunikat o powodzeniu
											setMessage(sprintf($aModule['lang']['address_add_ok']), false, true);
																				 
											unset($_SESSION['registration']);
		
											// ustawienie nieparsowania szablonu
											$bParseTemplate = false;
											doRedirectHttps($this->sPageLink);
										}
										
									break;
								}
							}
						}
						else {
							$aModule['form'] =& $oUser->getRegistrationStep2Form($_SESSION['registration']['ref_link']);
						}
						/*
					} else {
						doRedirectHttps(createLink($this->sPageLink,'register'));
					}
						 */
				break;
			
				case 'edit':
					$aModule['template'] .= '_edit_mainpage.tpl';
					//$aConfig['_tmp']['page']['name'] .= $aModule['lang']['discount_header'];
					addToPath(array(createLink($this->sPageLink, 'edit'),$aModule['lang']['edit_header']));
					
					$aModule['edit_sections'] = array(
						'edit_userdata' => array(
							'label' => $aModule['lang']['edit_userdata_label'],
							'link' => createLink($this->sPageLink, 'edit_userdata')
						),
						'edit_addresses' => array(
							'label' => $aModule['lang']['edit_addresses_label'],
							'link' => createLink($this->sPageLink, 'edit_addresses')
						),
						'password' => array(
							'label' => $aModule['lang']['chngpasswd'],
							'link' => createLink($this->sPageLink, 'password')
						),
						'change_email' => array(
							'label' => $aModule['lang']['change_email_label'],
							'link' => createLink($this->sPageLink, 'change_email')
						),
						'delete_account' => array(
							'label' => $aModule['lang']['delete_account_label'],
							'link' => createLink($this->sPageLink, 'delete_account')
						)
					);	
	
				break;
				
				case 'edit_userdata':
					$aModule['template'] .= '_profileEditForm.tpl';
					//$aConfig['_tmp']['page']['name'] .= $aModule['lang']['my_data_header'];
					addToPath(array(createLink($this->sPageLink, 'edit'),$aModule['lang']['edit_header']));
					addToPath(array(createLink($this->sPageLink, 'edit_userdata'),$aModule['lang']['my_data_header']));
					$aModule['edit_mainpage_link'] = createLink($this->sPageLink, 'edit');
					$aModule['buttons']['edit']['selected'] = true;
					// dolaczenie klasy User
					include_once('User.class.php');
					$oUser = new User($this->sModule,
														$this->iPageId,
														$this->aSettings,
														createLink($this->sPageLink, 'edit_userdata'),
														'','','',$_SESSION['w_user']['id']);
					if (!empty($_POST)) {
						// dolaczenie klasy walidatora
						include_once('Form/Validator.class.php');
						$oValidator = new Validator();
						if (!$oValidator->Validate($_POST)) {
							setMessage($oValidator->GetErrorString(), true);

							// ponowne wyswietlenie formularza edycji danych
							$aModule['form'] =& $oUser->getEditMainDataForm();
						}
						else {
							// aktualizacja danych uzytkownika
							if(!$oUser->updateMainUserData()) {
									// wystapil blad
									setMessage(sprintf($aModule['lang']['update_err'],
																	 $aConfig['_settings']['site']['email'],
																	 $aConfig['_settings']['site']['email']), true);
							} else {
									setMessage($aModule['lang']['update_ok']);
							}
							$aModule['form'] =& $oUser->getEditMainDataForm();
						}
					}
					else {
						$aModule['form'] =& $oUser->getEditMainDataForm();
					}
					
				break;

				case 'password':
					$aModule['template'] .= '_setNewPasswordForm.tpl';
					//$aConfig['_tmp']['page']['name'] .= $aModule['lang']['set_new_passwd_header'];
					addToPath(array(createLink($this->sPageLink, 'edit'),$aModule['lang']['edit_header']));
					addToPath(array(createLink($this->sPageLink, 'password'),$aModule['lang']['set_new_passwd_header']));
					$aModule['edit_mainpage_link'] = createLink($this->sPageLink, 'edit');
					$aModule['buttons']['edit']['selected'] = true;
					// dolaczenie klasy User
					include_once('User.class.php');
					$oUser = new User($this->sModule,
														$this->iPageId,
														$this->aSettings,
														createLink($this->sPageLink, 'password'),
														'','','',$_SESSION['w_user']['id']);
					if (!empty($_POST)) {
						// dolaczenie klasy walidatora
						include_once('Form/Validator.class.php');
						$oValidator = new Validator();
						if (!$oValidator->Validate($_POST)) {
							setMessage($oValidator->GetErrorString(), true);

							// ponowne wyswietlenie formularza zmiany hasla
							$aModule['form'] =& $oUser->getPasswordChangeForm();
						}
						else {
							// zmiana hasla uzytkownika
							switch ($oUser->changePassword()) {
								case -1:
									// podano bledne aktualne haslo
									setMessage($aModule['lang']['current_passwd_err'], true);
								break;

								case 0:
									// wystapil blad
									setMessage(sprintf($aModule['lang']['passwd_change_err'],
																	 $aConfig['_settings']['site']['email'],
																	 $aConfig['_settings']['site']['email']), true);
								break;

								default:
									setMessage($aModule['lang']['passwd_change_ok']);
								break;
							}
							$aModule['form'] =& $oUser->getPasswordChangeForm();
						}
					}
					else {
						$aModule['form'] =& $oUser->getPasswordChangeForm();
					}
				break;

				case 'change_email':
					$aModule['template'] .= '_changeEmailForm.tpl';
					addToPath(array(createLink($this->sPageLink, 'edit'),$aModule['lang']['edit_header']));
					addToPath(array(createLink($this->sPageLink, 'change_email'),$aModule['lang']['change_email_header']));
					$aModule['edit_mainpage_link'] = createLink($this->sPageLink, 'edit');
					$aModule['buttons']['edit']['selected'] = true;
					// dolaczenie klasy User
					include_once('User.class.php');
					$oUser = new User($this->sModule,
														$this->iPageId,
														$this->aSettings,
														createLink($this->sPageLink, 'change_email'),
														'','','',$_SESSION['w_user']['id']);
					if (!empty($_POST)) {
						// dolaczenie klasy walidatora
						include_once('Form/Validator.class.php');
						$oValidator = new Validator();
						if (!$oValidator->Validate($_POST)) {
							setMessage($oValidator->GetErrorString(), true);

							// ponowne wyswietlenie formularza zmiany emaila
							$aModule['form'] =& $oUser->getEmailChangeForm();
						}
						else {
							// zmiana emaila uzytkownika
							switch ($oUser->changeEmail()) {
								case -2:
										// konto o podanym adresie email juz istnieje
										setMessage(sprintf($aModule['lang']['email_exists'],
																			 $_POST['email']), true);
									break;

								case 0:
									// wystapil blad
									setMessage(sprintf($aModule['lang']['email_change_err'],
																	 $aConfig['_settings']['site']['email'],
																	 $aConfig['_settings']['site']['email']), true);
								break;

								default:
									$_SESSION['w_user']['email'] = filterInput($_POST['email']);
									setMessage($aModule['lang']['email_change_ok']);
								break;
							}
							$aModule['form'] =& $oUser->getEmailChangeForm();
						}
					}
					else {
						$aModule['form'] =& $oUser->getEmailChangeForm();
					}
				break;
				
				case 'edit_addresses':
					if(isset($_POST['ref'])){
						$_SESSION['edit_data']['ref'] = filterInput($_POST['ref']);
					}
					
					addToPath(array(createLink($this->sPageLink, 'edit'),$aModule['lang']['edit_header']));
					addToPath(array(createLink($this->sPageLink, 'edit_addresses'),$aModule['lang']['edit_addresses_header']));
					$aModule['edit_mainpage_link'] = createLink($this->sPageLink, 'edit');
					$aModule['edit_back_link'] = createLink($this->sPageLink, 'edit_addresses');
					$aModule['buttons']['edit']['selected'] = true;
					
					// dolaczenie klasy User
					include_once('User.class.php');
					$oUser = new User($this->sModule,
														$this->iPageId,
														$this->aSettings,
														$this->sPageLink,
														'','','',$_SESSION['w_user']['id']);
					$aModule['add_address_link'] = createLink($this->sPageLink, 'add_address');			
					$aModule['add_first_address_link'] = createLink($this->sPageLink, 'add_address');			
					if(isset($_GET['id'])){
						$aModule['template'] .= '_addressEdit.tpl';
						addToPath(array(createLink($this->sPageLink,'id'.$_GET['id'].',edit_addresses'),$oUser->getAddressName($_SESSION['w_user']['id'],$_GET['id'])));
						
						if (!empty($_POST)) {
							// dolaczenie klasy walidatora
							include_once('Form/Validator.class.php');
							$oValidator = new Validator();
							if (!$oValidator->Validate($_POST)) {
								setMessage($oValidator->GetErrorString(), true);
	
								// ponowne wyswietlenie formularza edycji adresu
								$aModule['form'] = $oUser->getAddressEditForm((int) $_GET['id'], true);
								if(empty($aModule['form'])){
									doRedirectHttps(createLink($this->sPageLink, 'edit_addresses'));
								}
							}
							else {
								// aktualizacja adresu uzytkownika
								if(!$oUser->updateUserAddress($_SESSION['w_user']['id'],$_GET['id'],0)) {
										// wystapil blad
										setMessage(sprintf($aModule['lang']['update_err'],
																		 $aConfig['_settings']['site']['email'],
																		 $aConfig['_settings']['site']['email']), true);
								} else {
									if(isset($_SESSION['edit_data']['ref'])){
										setMessage($aModule['lang']['update_ok'],false,true);
										// przeslano linke strony na ktora ma byc powrot po zarejestrowaniu
										$sRefLink = base64_decode($_SESSION['edit_data']['ref']);
										unset($_SESSION['edit_data']);
										doRedirectHttps($sRefLink);
									} else {
										setMessage($aModule['lang']['update_ok']);
									}
								}
								$aModule['form'] = $oUser->getAddressEditForm((int) $_GET['id'], true);
								if(empty($aModule['form'])){
									doRedirectHttps(createLink($this->sPageLink, 'edit_addresses'));
								}
							}
						}
						else {
							$aModule['form'] = $oUser->getAddressEditForm((int) $_GET['id'], true);
							if(empty($aModule['form'])){
								doRedirectHttps(createLink($this->sPageLink, 'edit_addresses'));
							}
							
						}
												
						
						$aModule['details']['id'] = $_GET['id'];
						//dump($aModule['details']);
					} else {
						$aModule['template'] .= '_addressesList.tpl';
	
						$aModule['addresses'] = $oUser->getAddresses($oUser->canDeleteUserAddress($_SESSION['w_user']['id']));
						
					}
				break;
				
				case 'add_address':
					if(isset($_POST['ref'])){
						$_SESSION['edit_data']['ref'] = filterInput($_POST['ref']);
					}
          
					addToPath(array(createLink($this->sPageLink, 'edit'),$aModule['lang']['edit_header']));
					addToPath(array(createLink($this->sPageLink, 'edit_addresses'),$aModule['lang']['edit_addresses_header']));
					addToPath(array(createLink($this->sPageLink, 'add_address'),$aModule['lang']['add_addresses_header']));
					$aModule['edit_mainpage_link'] = createLink($this->sPageLink, 'edit');
					$aModule['edit_back_link'] = isset($_SESSION['edit_data']['ref']) ? base64_decode($_SESSION['edit_data']['ref']) : createLink($this->sPageLink,'edit_addresses');//createLink($this->sPageLink, 'edit_addresses');
					$aModule['buttons']['edit']['selected'] = true;
					// dolaczenie klasy User
					include_once('User.class.php');
					$oUser = new User($this->sModule,
														$this->iPageId,
														$this->aSettings,
														$this->sPageLink,
														'','','',$_SESSION['w_user']['id']);
						$aModule['template'] .= '_addressEdit.tpl';
						if (!empty($_POST) && $_POST['cart'] != '1') {
							// dolaczenie klasy walidatora
							include_once('Form/Validator.class.php');
							$oValidator = new Validator();
							if (!$oValidator->Validate($_POST)) {
								setMessage($oValidator->GetErrorString(), true);
	
								// ponowne wyswietlenie formularza edycji adresu
								$aModule['form'] = $oUser->getAddressEditForm();
								if(empty($aModule['form'])){
									doRedirectHttps(createLink($this->sPageLink, 'edit_addresses'));
								}
							}
							else {
								// aktualizacja adresu uzytkownika
								if(!$oUser->addUserAddress($_SESSION['w_user']['id'],0)) {
										// wystapil blad
										setMessage(sprintf($aModule['lang']['address_add_err'],
																		 $aConfig['_settings']['site']['email'],
																		 $aConfig['_settings']['site']['email']), true);
									$aModule['form'] = $oUser->getAddressEditForm();
									if(empty($aModule['form'])){
										doRedirectHttps(createLink($this->sPageLink, 'edit_addresses'));
									}
								} else {
									if(isset($_SESSION['edit_data']['ref'])){
										setMessage($aModule['lang']['address_add_ok'],false,true);
										// przeslano linke strony na ktora ma byc powrot po zarejestrowaniu
										$sRefLink = base64_decode($_SESSION['edit_data']['ref']);
                    if (stristr($sRefLink, 'step3.html#cart') != false) {
                      // informujemy ze klient dodał właśnie nowy adres
                      $sRefLink = str_replace('step3.html#cart', 'step3.html?adress_added=1#cart', $sRefLink);
                    }
										unset($_SESSION['edit_data']);
										doRedirectHttps($sRefLink);
									} else {
										setMessage($aModule['lang']['address_add_ok'],false,true);
										doRedirectHttps(createLink($this->sPageLink, 'edit_addresses'));
									}
										
								}
								
							}
						}
						else {
							$aModule['form'] = $oUser->getAddressEditForm();
							if(empty($aModule['form'])){
								doRedirectHttps(createLink($this->sPageLink, 'edit_addresses'));
							}
							
						}
						
				break;
				
				case 'delete_address':
					if(isset($_GET['id'])){
						// dolaczenie klasy User
					include_once('User.class.php');
					$oUser = new User($this->sModule,
														$this->iPageId,
														$this->aSettings,
														$this->sPageLink,
														'','','',$_SESSION['w_user']['id']);
						$sAddressName = $oUser->getAddressName($_SESSION['w_user']['id'],$_GET['id']);
						switch($oUser->deleteUserAddress($_SESSION['w_user']['id'],$_GET['id'])) {
								case 0:
									// wystapil blad
									setMessage(sprintf($aModule['lang']['address_delete_err'],
																		$sAddressName,
																	 $aConfig['_settings']['site']['email'],
																	 $aConfig['_settings']['site']['email']), true,true);
								break;

								default:
									setMessage(sprintf($aModule['lang']['address_delete_ok'],$sAddressName),false,true);
								break;
							}
							doRedirectHttps(createLink($this->sPageLink, 'edit_addresses'));
					} else {
						doRedirectHttps(createLink($this->sPageLink, 'edit_addresses'));
					}
					break;
					
					case 'delete_account':
					
						// dolaczenie klasy User
						include_once('User.class.php');
						$oUser = new User($this->sModule,
															$this->iPageId,
															$this->aSettings,
															$this->sPageLink,
															'','','',$_SESSION['w_user']['id']);
						if(!$oUser->canDeleteUserAccount($_SESSION['w_user']['id'])){
										// wystapil blad
								setMessage($aModule['lang']['account_delete_orders_err'], true,true);
								
								doRedirectHttps(createLink($this->sPageLink, 'edit'));
						} else {
							if(isset($_POST['delete_confirmed'])){
								if(!$oUser->deleteUserAccount($_SESSION['w_user']['id'])) {
										// wystapil blad
										setMessage(sprintf($aModule['lang']['account_delete_err'],
																		 $aConfig['_settings']['site']['email'],
																		 $aConfig['_settings']['site']['email']), true,true);
										doRedirectHttps(createLink($this->sPageLink, 'delete_account'));
								} else {
										include_once('UserLogin.class.php');
										$oLogin = new UserLogin($this->sPageLink, strpos($_SERVER['HTTP_REFERER'], $aConfig['common']['client_base_url']) != false ? $_SERVER['HTTP_REFERER'] : '');
										$oLogin->doLogout();
										setMessage(sprintf($aModule['lang']['account_delete_ok'],$sAddressName),false,true);
										doRedirect('/');
								}
								
							} else {
								addToPath(array(createLink($this->sPageLink, 'edit'),$aModule['lang']['edit_header']));
								addToPath(array(createLink($this->sPageLink, 'edit_addresses'),$aModule['lang']['delete_account_header']));
								$aModule['buttons']['edit']['selected'] = true;
								$aModule['edit_mainpage_link'] = createLink($this->sPageLink, 'edit');
								$aModule['delete_account_link'] = createLink($this->sPageLink, 'delete_account');
								$aModule['template'] .= '_accountDelete.tpl';
							}
					}
					break;

				default:
					$aModule['buttons']['orders']['selected'] = true;
				case 'generate-activation-mail':
					$userConfirmation = new UserConfirmation($_SESSION['w_user']['id']);
					$sent = $userConfirmation->sendConfirmationMail();

					if (false == $sent) {
						setMessage('Nie udało się wysłać wiadomości, spróbuj jeszcze raz', true, true);
					} else {
						setMessage('Wiadomość z linkiem aktywacyjnym została wysłana na podany adres', false, true);
					}

					doRedirectHttps(createLink($this->sPageLink, 'orders'));
					die;

					break;
				case 'orders':

					$userConfirmation = new UserConfirmation($_SESSION['w_user']['id']);
					$isUserConfirmed = $userConfirmation->isUserConfirmed();
					$hasUserOldOrders = $userConfirmation->hasUserOldOrders();

					$aModule['confirm_account_message'] = null;

					if ($isUserConfirmed == false && $hasUserOldOrders == true) {
						$aModule['confirm_account_message'] = 'Posiadasz stare zamówienia, potwierdz konto mailowo żeby mieć do nich dostęp';
					}

					addToPath(array(createLink($this->sPageLink, 'orders'),$aModule['lang']['orders_header']));
	
					if(isset($_GET['id'])){
						$aModule['template'] .= '_ordersDetails.tpl';
						//$aConfig['_tmp']['page']['name'] .= $aModule['lang']['orders_header'];
						
						// pobranie zamowien uzytkownika
						// dolaczenie klasy UserOrders
						include_once('modules/m_konta/client/UserOrders.class.php');
						$oUserOrders = new UserOrders($this->sPageLink,
																					$_SESSION['w_user']['id'],
																					0,
																					$this->sTemplatesPath);

                        $sSql = 'SELECT id FROM orders WHERE order_number = '.intval($_GET['id']);
                        $iOId = Common::GetOne($sSql);

						$aModule['details'] = $oUserOrders->getUserOrderDetails((int) $iOId);
						if(empty($aModule['details'])){
							doRedirectHttps(createLink($this->sPageLink, 'orders'));
						}
						$aModule['details']['id'] = $_GET['id'];
						//dump($aModule['details']);
					} else {
						$aModule['template'] .= '_orders.tpl';
						//$aConfig['_tmp']['page']['name'] .= $aModule['lang']['orders_header'];
	
						// pobranie zamowien uzytkownika
						// dolaczenie klasy UserOrders
						include_once('modules/m_konta/client/UserOrders.class.php');
						
						$oUserOrders = new UserOrders($this->sPageLink,
																					$_SESSION['w_user']['id'],
																					10,
																					$this->sTemplatesPath);
						$aModule = array_merge($aModule, $oUserOrders->getUserOrders());
					}
				break;
				case 'proforma':
					addToPath(array(createLink($this->sPageLink, 'orders'),$aModule['lang']['orders_header']));


					if(isset($_GET['id'])){
                        $sSql = 'SELECT id FROM orders WHERE order_number = '.intval($_GET['id']);
                        $iOId = Common::GetOne($sSql);

						//$aConfig['_tmp']['page']['name'] .= $aModule['lang']['orders_header'];
						
						// pobranie zamowien uzytkownika
						// dolaczenie klasy UserOrders
						include_once('modules/m_konta/client/UserOrders.class.php');
						$oUserOrders = new UserOrders($this->sPageLink,
																					$_SESSION['w_user']['id'],
																					0,
																					$this->sTemplatesPath);
						if($oUserOrders->generateInvoice($iOId) == false){
							
						}
						exit();
						//dump($aModule['details']);
					} else {
						doRedirectHttps(createLink($this->sPageLink, 'orders'));
					}
				break;
                case 'proforma-new':
                    if(isset($_GET['id'])) {
                        $sSql = 'SELECT invoice_url FROM orders_merlin_invoices OMI
                                 LEFT JOIN orders O 
                                 ON O.order_number = OMI.order_number
                                 WHERE OMI.id = ' . intval($_GET['id']).' AND O.user_id = "'.$_SESSION['w_user']['id'].'" ';
                        $url = Common::GetOne($sSql);

                        if ($url !== NULL) {
                            $ch = curl_init($url);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                            curl_setopt($ch, CURLOPT_HEADER, FALSE);

                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                            $result = curl_exec($ch);
                            $info = curl_getinfo($ch);
                            curl_close($ch);

                            header("Content-type:application/pdf");
                            header("Content-Disposition:attachment;filename='Faktura.pdf'");
                            header('Content-Type: application/pdf');
                            header('Content-Length: ' . strlen($result));
                            header('Content-disposition: inline; filename="Faktura PDF"');
                            header('Cache-Control: public, must-revalidate, max-age=0');
                            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                            echo $result;
                            exit();
                        }
                        else
                        {
                            die('');
                        }
                    }
                break;
				/*
				case 'balance':
					addToPath(array(createLink($this->sPageLink, 'balance'),$aModule['lang']['balances_header']));
	
						$aModule['template'] .= '_balances.tpl';
						//$aConfig['_tmp']['page']['name'] .= $aModule['lang']['orders_header'];
	
						// pobranie zamowien uzytkownika
						// dolaczenie klasy UserBalances
						include_once('modules/m_konta/client/UserBalances.class.php');
						
						$oUserBalances = new UserBalances($this->sPageLink,
																					$_SESSION['w_user']['id'],
																					10,
																					$this->sTemplatesPath);
						$aModule = array_merge($aModule, $oUserBalances->getUserBalances());
					
				break;
				*/
				case 'discount':
						$aModule['template'] .= 'settings.tpl';
						//$aConfig['_tmp']['page']['name'] .= $aModule['lang']['discount_header'];
						addToPath(array(createLink($this->sPageLink, 'discount'),$aModule['lang']['discount_header']));
	
						// dolaczenie klasy User
						include_once('User.class.php');
					$oUser = new User($this->sModule,
														$this->iPageId,
														$this->aSettings,
														createLink($this->sPageLink, 'discount'),
														'','','',$_SESSION['w_user']['id']);
						$aModule = array_merge($aModule, $oUser->getUserData());
						$aModule['discount'] = Common::formatPrice3($aModule['discount'],',');
						if(!empty($aUserPrivs)){
							if($aUserPrivs['allow_14days'] == '1'){
								$aModule['user_privs'][] = $aModule['lang']['user_privs']['allow_14days'];
							}
							if($aUserPrivs['allow_personal_reception'] == '1'){
								$aModule['user_privs'][] = $aModule['lang']['user_privs']['allow_personal_reception'];
							}
							if($aUserPrivs['allow_free_transport'] == '1'){
								$aModule['user_privs'][] = $aModule['lang']['user_privs']['allow_free_transport'];
							}
						}
						
						
					if (!empty($_POST)) {
						// dolaczenie klasy walidatora
						include_once('Form/Validator.class.php');
						$oValidator = new Validator();
						if (!$oValidator->Validate($_POST)) {
							setMessage($oValidator->GetErrorString(), true);

							// ponowne wyswietlenie formularza edycji danych
							$aModule['form'] =& $oUser->getEditAccountSettingsForm();
						}
						else {
							// aktualizacja danych uzytkownika
							if(!$oUser->updateAccountSettings()) {
									// wystapil blad
									setMessage(sprintf($aModule['lang']['update_err'],
																	 $aConfig['_settings']['site']['email'],
																	 $aConfig['_settings']['site']['email']), true);
							} else {
									setMessage($aModule['lang']['update_ok']);
							}
							$aModule['form'] =& $oUser->getEditAccountSettingsForm();
						}
					}
					else {
						$aModule['form'] =& $oUser->getEditAccountSettingsForm();
					}
				break;
				case 'newsletters':
						$aModule['template'] .= '_newsletters.tpl';
						//$aConfig['_tmp']['page']['name'] .= $aModule['lang']['discount_header'];
						addToPath(array(createLink($this->sPageLink, 'newsletters'),$aModule['lang']['newsletters_header']));
						$aModule['page_link_news'] = createLink($this->sPageLink, 'newsletters');
						// dolaczenie klasy User
						include_once('modules/m_konta/client/UserNewsletters.class.php');
						$oUserNews = new UserNewsletters($this->sModule,
																					$_SESSION['w_user']['id'],
																					$_SESSION['w_user']['email']);
																					
						if(!empty($_POST['n_categories'])){
							
							if($oUserNews->editNewsletters($_POST['n_categories'])===false){
								setMessage(sprintf($aModule['lang']['newsletter_edit_err'],
																	 $aConfig['_settings']['site']['email'],
																	 $aConfig['_settings']['site']['email']), true);
							} else {
								setMessage($aModule['lang']['newsletter_edit_ok']);
							}
						}
						$aModule = array_merge($aModule, $oUserNews->getNewsletters());

				break;
        
        case 'connections':
          addToPath(array(createLink($this->sPageLink, 'connections'),$aModule['lang']['connections_header']));
          include_once('modules/m_konta/client/UserConnections.class.php');
          $oUserConnections = new UserConnections($_SESSION['w_user']['id'],
                                                  $this->sPageLink);
          
          $iFbUId = $oUserConnections->getAccountConnectedFB();
          if ($iFbUId > 0) {
            $aModule['template'] .= 'connections/disconnect.tpl';
            $aModule['disconnect'] = $oUserConnections->getDisconnectForm();
          } else {
            $aModule['template'] .= 'connections/connect.tpl';
            $aModule['connect'] = $oUserConnections->getConnectForm();
          }
        break;
        
        case 'disconnect':
          addToPath(array(createLink($this->sPageLink, 'connections'),$aModule['lang']['connections_header']));
          include_once('modules/m_konta/client/UserConnections.class.php');
          $oUserConnections = new UserConnections($_SESSION['w_user']['id'],
                                                  $this->sPageLink);
          $aModule['template'] .= 'connections/do_disconnect.tpl';
          $aModule['do_disconnect'] = $oUserConnections->doDisconnectForm(false);
        break;
          
        case 'do_disconnect':
          addToPath(array(createLink($this->sPageLink, 'connections'),$aModule['lang']['connections_header']));
          include_once('modules/m_konta/client/UserConnections.class.php');
          $oUserConnections = new UserConnections($_SESSION['w_user']['id'],
                                                  $this->sPageLink);
          $aModule['template'] .= 'connections/do_disconnect.tpl';
          $oUserConnections->doDisconnectForm(true);
        break;
      
        case 'fb-logowanie':
          $sBaseURL = str_replace('https://', '', $aConfig['common']['client_base_url_http']);
          $sLastURL = '/moje-konto';
          if (stristr($_SERVER['HTTP_REFERER'], $sBaseURL) !== FALSE) {
            $sLastURL = $_SERVER['HTTP_REFERER'];
          }

          // połączenie kont
          include_once($aConfig['common']['base_path'].'LIB/fb_connector/fbConnector.class.php');
          $oFB = fbConnector::getInstance();
          $iFbUId = $oFB->getFBUId();
          $iUserId = $oFB->getUserIdByFBUId($iFbUId);
          $aUData = $oFB->getUserData();

          // czy przypadkiem juz nie został zalogowany
            // czy jest id konta z facebooka, czy przesłane zostały dane konta, oraz moze juz ma konto
            if ($iFbUId > 0 && !empty($aUData)) {
              $mRetCon = $oFB->connectAccounts($iFbUId, $aUData, true);
              if ($mRetCon > 0) {
                setMessage(_('Konta zostały połączone'), false, true);
                doRedirect($sLastURL);
              } else {
                setMessage(_('Wystąpił błąd podczas komunikacji z serwisem Facebook Kod: '.$mRetCon), true, true);
                doRedirect($sLastURL);
              }
            } elseif ($iUserId > 0) {
              if ($oFB->doTryLogin() === TRUE) {
                // zalogowani z powodzeniem
                doRedirect($sLastURL);
              } else {
                setMessage(_('Wystąpił błąd podczas komunikacji z serwisem Facebook Kod: 101'), true, true);
                doRedirect($sLastURL);
              }
            } else {
              setMessage(_('Wystąpił błąd podczas komunikacji z serwisem Facebook Kod: 102'), true, true);
              doRedirect($sLastURL);
            }
        break;
      
				/*default:
					// wyswietlenie dostepnych w koncie uzytkownika akcji
					$this->sTemplate = $this->sTemplatesPath.'_myAccount.tpl';
				break;*/
			}
		}

		if ($bParseTemplate) {
			
			$aModule['text'] =& $this->aSettings['ftext'];
			$pSmarty->assign_by_ref('aModule', $aModule);
			$sHtml = $pSmarty->fetch($this->sTemplate);
			$pSmarty->clear_assign('aModule');
		}
		
		//dump($aModule);
		return $sHtml;
	} // end of getContent()
  
  
	/**
	 * Metoda pobiera konfiguracje dla strony
	 *
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."users_accounts_settings
						 WHERE page_id = ".$this->iPageId;
		$aCfg =& Common::GetRow($sSql);
		$aCfg['items_per_page'] = (int) $aCfg['items_per_page'];
		return $aCfg;
	} // end of getSettings() function
	
	/**
	 * pobiera informacje o specjanych uprawnieniach użytkownika - przelew 14 dni,
	 * odbór osobisty, zwolnienie z kosztów transportu 
	 * @return array
	 */
	function getSpecialUserPrivileges(){
  	global $aConfig;
  	if(!empty($_SESSION['w_user']) && $_SESSION['w_user']['id'] > 0){
	  	$sSql = "SELECT allow_14days, allow_personal_reception, allow_free_transport
		  					FROM ".$aConfig['tabls']['prefix']."users_accounts 
		  					WHERE id = ".$_SESSION['w_user']['id'];
  		return Common::GetRow($sSql);
  	} else {
  		return array();
  	}
  } // end of getSpecialUserPrivileges() method
	
} // end of Module Class
?>