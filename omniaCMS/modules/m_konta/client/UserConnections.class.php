<?php
/**
 * Klasa zarządzania powiązanymi kontami
 *  wymaga: 
 *    LIB/fb_connector/fbConnector.class.php
 *    objektu bazy - $pDbMgr
 *    funkcji - /inc/functions.ini.php
 * 
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-05-14 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class UserConnections {

  // Id uzytkownika
	private $iUserId;
  
  // link do konta użytkownika
  private $sPageLink;
  
  function __construct($iUserId, $sPageLink) {
    $this->iUserId = $iUserId;
    $this->sPageLink = $sPageLink;
  }
  
  
  /**
   * Metoda pobiera fb_uid dla uzytkownika
   * 
   * @global object $pDbMgr
   * @return int
   */
  public function getAccountConnectedFB() {
    global $pDbMgr;
    
    $sSql = "SELECT fb_uid 
             FROM users_accounts
             WHERE id=".$this->iUserId;
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of _getAccountConnectedFB() method
  

  /**
   * Metoda generuje formularz łączenia kont
   * 
   * @global array $aConfig
   * @return type
   */
  public function getConnectForm() {
    global $aConfig;
    $aModule = array();
    
    // dołączenie klasy integracji z facebook
    include_once($aConfig['common']['base_path'].'LIB/fb_connector/fbConnector.class.php');
    $oFB = fbConnector::getInstance();
    $aModule['links']['fb_login'] = $oFB->getLoginURL();
    return $aModule;
  }// end of getConnectForm() method
  
  
  /**
   * Metoda generuje formularz rozłączenia kont
   * 
   * @global array $aConfig
   */
  public function getDisconnectForm() {
    $aModule = array();
    
    // dołączenie klasy integracji z facebook
    $aModule['links']['fb_login'] = createLink($this->sPageLink, 'disconnect');
    return $aModule;
  }// end of getDiscountForm() method
  
  
  /**
   * Metoda wyświetla formularz rozłączania konta
   * 
   * @return array
   */
  public function doDisconnectForm($bActionDoDisconnect) {
    global $aConfig;
    $aModule = array();
    
    // czy użytkownik ma zdefiniowane hasło
    if ($this->checkEmptyUserPassword() === TRUE && $bActionDoDisconnect == FALSE) {
      // czy chcesz mieć wciąż dostęp do konta
      $aModule['links']['do_disconnect'] = createLink($this->sPageLink, 'do_disconnect');
      $aModule['links']['change_passwd'] = createLink($this->sPageLink, 'password');
    } else {
      // usuwamy połaczenie z FB
      include_once($aConfig['common']['base_path'].'LIB/fb_connector/fbConnector.class.php');
      $oFB = fbConnector::getInstance();
      if ($oFB->disconnectAccounts($this->iUserId) === TRUE) {
        if ($bActionDoDisconnect == TRUE) {
          // definitywne usunięcie konta
					include_once('User.class.php');
          $aTMP = array();
					$oUser = new User('',
														0,
														$aTMP,
														'',
														'','','',
                            $this->iUserId);
          $oUser->deleteUserAccount($this->iUserId);
          doRedirect(createLink($this->sPageLink, 'logout'));
        }
        // OKI
        setMessage(_('Konta zostały rozłączone'), FALSE, TRUE);
        doRedirect($this->sPageLink);
      } else {
        // ERR
        setMessage(_('Wystąpił błąd podczas rozłączania kont'), TRUE, TRUE);
        doRedirect(createLink($this->sPageLink, 'connections'));
      }
    }
    return $aModule;
  }// end of doDisconnectForm() method
  
  
  /**
   * Metoda sprawdza czy uzytkownik ma zdefiniowane hasło
   * 
   * @global type $pDbMgr
   * @return type
   */
  private function checkEmptyUserPassword() {
    global $pDbMgr;
    
    $sSql = "SELECT passwd 
             FROM users_accounts 
             WHERE id = ".$this->iUserId;
    return ($pDbMgr->GetOne('profit24', $sSql) != '') ? FALSE : TRUE;
  }// end of _checkEmptyUserPassword() method
}// end of class
?>