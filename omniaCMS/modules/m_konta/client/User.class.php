<?php
/**
* Klasa Uzytkownika serwisu
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version   1.0
*/

class User {

	// dane zalogowanego uzytkownika
	var $aUser;

  // Id strony
  var $iPageId;

  // symbol modulu
  var $sModule;

  // ustawienia strony modulu
  var $aSettings;

  // link do strony newslettera
  var $sPageLink;

	/**
  * Konstruktor klasy User
	*/
	function User($sSymbol, $iPageId, &$aSettings, $sPageLink, $sUser='', $sLogin='', $sEmail='', $iID='') {
		$this->iPageId = $iPageId;
		$this->sModule = $sSymbol;
		$this->aSettings = $aSettings;
		$this->aUser = null;
		$this->sPageLink = $sPageLink;
		if ($sUser != '') {
			$this->setUserData($sUser);
		}
		if ($sLogin != '' && $sEmail != '') {
			$this->setUserData2($sLogin, $sEmail);
		}
		if ($iID != '') {
			$this->setUserData3($iID);
		}
	} // end User()


	/**
	 * Metoda pobiera ustawienia uzytkownika o przekazanym MD5 z ID:login
	 *
	 * @param	string	$sUser	- MD5 z ID:login
	 * @return	void
	 */
	function setUserData($sUser) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."users_accounts
						 WHERE MD5(CONCAT(id, ':', email)) = '".$sUser."'
               AND deleted='0'
							 AND website_id=".$aConfig['profit24_website_id'];
						 
		$this->aUser =& $pDbMgr->GetRow('profit24', $sSql);
	} // end of setUserData() function


	/**
	 * Metoda pobiera ustawienia uzytkownika o przekazanym loginie
	 * i adresie e-mail
	 *
	 * @param	string	$sLogin	- login
	 * @param	string	$sEmail	- adres e-mail
	 * @return	void
	 */
	function setUserData2($sEmail) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."users_accounts
						 WHERE email = '".$sEmail."'
						 AND deleted='0'
						 AND website_id=".$aConfig['profit24_website_id'];
		$this->aUser =& $pDbMgr->GetRow('profit24', $sSql);
	} // end of setUserData2() function

	/**
	 * Metoda pobiera ustawienia uzytkownika o przekazanym id
	 * i adresie e-mail
	 *
	 * @param	string	$iID	- id
	 * @return	void
	 */
	function setUserData3($iID) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."users_accounts
						 WHERE id = ".$iID."
               AND deleted='0'
							 AND website_id=".$aConfig['profit24_website_id'];
		$this->aUser =& $pDbMgr->GetRow('profit24', $sSql);
	} // end of setUserData2() functio
	
	/**
	 * Metoda zwraca dane uzytkownika
	 */
	function getUserData() {
		return $this->aUser;
	} // end of getUserData() method


	/**
	 * Metoda ustawia dla uzytkownika klucz zmiany hasla
	 *
	 * @param	string	$sKey	- klucz zmiany hasla
	 * @return	bool	- true: pomyslnie; false: blad
	 */
	function setChangePasswdKey($sKey) {
		global $aConfig, $pDbMgr;

		$aValues = array(
			'passwd_change_key' => $sKey
		);
		return $pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts",
													$aValues,
													"id = ".$this->aUser['id']." AND website_id=".$aConfig['profit24_website_id']) == 1;
	} // end of setChangePasswdKey() function


	/**
	 * Metoda ustawia dla uzytkownika nowe haslo
	 *
	 * @return	bool	- true: pomyslnie; false: blad
	 */
	function setNewPassword($sEmail, $sPasswd) {
		global $aConfig, $pDbMgr;

    $sSalt = $this->getRandomString(255);
		$aValues = array(
			'passwd' => encodePasswd($sPasswd, $sSalt),
      'salt' => $sSalt,
//			'passwd_change_key' => 'NULL'
		);
		return $pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts",
													$aValues,
													" email = ".Common::Quote($sEmail)." AND website_id=".$aConfig['profit24_website_id'].' AND deleted = "0" ') == 1;
	} // end of setNewPassword() function


	/**
	 * Metoda zwraca ID uzytkownika
	 */
	function getID() {
		return $this->aUser['id'];
	} // end of getID() method


	/**
	 * Metoda zwraca adres email uzytkownika
	 */
	function getEmail() {
		return $this->aUser['email'];
	} // end of getEmail() method

	/**
	 * Metoda zwraca klucz zmiany hasla
	 */
	function getKey() {
		return $this->aUser['passwd_change_key'];
	} // end of getKey() method


	/**
	 * Metoda zwraca czy konto uzytkownika jest aktywne czy tez nie
	 */
	function isActive() {
		return $this->aUser['active'] == '1';
	} // end of isActive() method

	/**
	 * Metoda zwraca haslo użytkownika
	 */
	function getPasswd() {
		return $this->aUser['passwd'];
	} // end of getPasswd() method
	

	/**
	 * Metoda zwraca czy uzytkownik istnieje czy tez nie (udalo sie ustawic
	 * dane uzytkownika w tablicy $aUser
	 */
	function userExists() {
		return is_array($this->aUser) && !empty($this->aUser);
	} // end of userExists() method


	/**
	 * Metoda aktywuje konto uzytkownika
	 *
	 * @return	integer	- -1: konto juz aktywne
	 * 										 0: wystapil blad
	 * 										 1: konto zostalo aktywowane
	 */
	function activate() {
		global $aConfig, $pDbMgr;

		if ($this->aUser['active'] == '1') {
			return -1;
		}
		$aValues = array(
			'active' => '1'
		);
    $mRet = $pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts",
											 $aValues,
											 "id = ".$this->aUser['id']." AND website_id=".$aConfig['profit24_website_id']);
    if ($mRet === false) {
      return false;
    }
		if (!$mRet) {
			return 0;
		}
		return 1;
	} // end of activate() method


	/**
	 * Metoda zwraca formularz rejestracji / edycji uzytkownika
	 *
	 * @return	array	- tablica z zawartoscia formularza
	 */
	function &getRegistrationForm() {
	 	global $aConfig;
	 	$aForm = array();
	 	$aData = array();
	 	$aLoginAttribs = array();
	 	$bLoginDataRequired = true;
	 	$bShowAgreements = true;
	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];
	 	$sPasswdLabel =& $aLang['passwd'];
	 	$sAvatar = '';

	 	if (isLoggedIn()) {
	 		// uzytkownik jest zalogowany - edycja jego danych
	 		$aData =& $this->aUser;
	 		$bLoginDataRequired = false;
	 		$bShowAgreements = false;
	 	}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		if (!isset($aData['invoice_data'])) {
			$aData['invoice_data'] = '0';
		}

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('userAccountForm',
													 '',
													 array('action' => $this->sPageLink, 'enctype' => 'multipart/form-data'),
													 array(),
													 true);

		$aMiddleTextAttribs = array(
			'class' => 'middle_text'
		);
		$aTextAttribs = array(
			'class' => 'text'
		);
		$aShortTextAttribs = array(
			'class' => 'short_text'
		);
		$aSelectAttribs = array(
			'class' => 'select'
		);
		$aDisabledEmail = array(
			'class' => 'inputDisabled',
			'readonly' => 'readonly'
		);

	 	if ($bLoginDataRequired) {
			$aForm['fields']['login_section']['passwd']['label'] = $oForm->GetLabelHTML('passwd', $sPasswdLabel, $bLoginDataRequired);
			$aForm['fields']['login_section']['passwd']['input'] = $oForm->GetPasswordHTML('passwd', $sPasswdLabel, '', array_merge($aTextAttribs, array('maxlength' => 16,'tabindex' => 3)), '', $bLoginDataRequired);

			$aForm['fields']['login_section']['passwd2']['label'] = $oForm->GetLabelHTML('passwd2', $aLang['passwd2'], $bLoginDataRequired);
			$aForm['fields']['login_section']['passwd2']['input'] = $oForm->GetConfirmPasswordHTML('passwd2', $aLang['passwd2'], '', 'passwd', array_merge($aTextAttribs, array('tabindex' => 4)), $aLang['confirm_passwd_err']);
	 	}

		// adres email
		$aForm['fields']['name_section']['email']['label'] = $oForm->GetLabelHTML('email', $aLang['email'], $bLoginDataRequired);
		$aForm['fields']['name_section']['email']['input'] = $oForm->GetTextHTML('email', $aLang['email'], $aData['email'], array_merge(($bLoginDataRequired?$aTextAttribs:$aDisabledEmail), array('maxlength' => 64,'tabindex' => 1)), '', 'email',$bLoginDataRequired);
		if ($bLoginDataRequired) {
			// potwierdz adres email
			$aForm['fields']['name_section']['confirm_email']['label'] = $oForm->GetLabelHTML('confirm_email', $aLang['confirm_email'], true);
			$aForm['fields']['name_section']['confirm_email']['input'] = $oForm->GetTextHTML('confirm_email', $aLang['confirm_email'], $aData['confirm_email'], array_merge($aTextAttribs, array('maxlength' => 64,'tabindex' => 2)), '', 'email');
		}
		
 		$aForm['fields']['name_section']['name']['label'] = $oForm->GetLabelHTML('name', $aLang['name'], true);
		$aForm['fields']['name_section']['name']['input'] = $oForm->GetTextHTML('name', $aLang['name'], $aData['name'], array_merge($aTextAttribs, array('maxlength' => 32,'tabindex' => 5)), '', 'text', true);

		$aForm['fields']['name_section']['surname']['label'] = $oForm->GetLabelHTML('surname', $aLang['surname'], true);
		$aForm['fields']['name_section']['surname']['input'] = $oForm->GetTextHTML('surname', $aLang['surname'], $aData['surname'], array_merge($aTextAttribs, array('maxlength' => 32,'tabindex' => 6)), '', 'text', true);

 		$aForm['fields']['address_section']['street']['label'] = $oForm->GetLabelHTML('street', $aLang['street'], true);
		$aForm['fields']['address_section']['street']['input'] = $oForm->GetTextHTML('street', $aLang['street'], $aData['street'], array_merge($aTextAttribs, array('maxlength' => 64,'tabindex' => 7)), '', 'text', true);

		$aForm['fields']['address_section']['number']['label'] = $oForm->GetLabelHTML('number', $aLang['number'], true);
		$aForm['fields']['address_section']['number']['input'] = $oForm->GetTextHTML('number', $aLang['number'], $aData['number'], array_merge($aTextAttribs, array('maxlength' => 8,'tabindex' => 8)), '', 'text', true);
 
		$aForm['fields']['address_section']['number2']['label'] = $oForm->GetLabelHTML('number2', $aLang['number2'], false);
		$aForm['fields']['address_section']['number2']['input'] = $oForm->GetTextHTML('number2', $aLang['number2'], $aData['number2'], array_merge($aTextAttribs, array('maxlength' => 8,'tabindex' => 9)), '', 'text', false);
 
		
		$aForm['fields']['address_section']['postal']['label'] = $oForm->GetLabelHTML('postal', $aLang['postal'], true);
		$aForm['fields']['address_section']['postal']['input'] = $oForm->GetTextHTML('postal', $aLang['postal'], $aData['postal'], array_merge($aTextAttribs, array('maxlength' => 6,'tabindex' => 10)), '', 'postal', true);

		$aForm['fields']['address_section']['city']['label'] = $oForm->GetLabelHTML('city', $aLang['city'], true);
		$aForm['fields']['address_section']['city']['input'] = $oForm->GetTextHTML('city', $aLang['city'], $aData['city'], array_merge($aTextAttribs, array('maxlength' => 32,'tabindex' => 11)), '', 'text', true);
 

 		$aForm['fields']['phone_section']['phone']['label'] = $oForm->GetLabelHTML('phone', $aLang['phone'], true);
		$aForm['fields']['phone_section']['phone']['input'] = $oForm->GetTextHTML('phone', $aLang['phone'], $aData['phone'], array_merge($aTextAttribs, array('maxlength' => 16,'tabindex' => 12)), '', 'phone', true);

		$aForm['fields']['address_section']['company']['label'] = $oForm->GetLabelHTML('company', $aLang['company'], false);
		$aForm['fields']['address_section']['company']['input'] = $oForm->GetTextHTML('company', $aLang['company'], $aData['company'], array_merge($aTextAttribs, array('maxlength' => 255,'tabindex' => 13)), '', 'text', false, '', array());

		$aForm['fields']['address_section']['nip']['label'] = $oForm->GetLabelHTML('nip', $aLang['nip'], false);
		$aForm['fields']['address_section']['nip']['input'] = $oForm->GetTextHTML('nip', $aLang['nip'], $aData['nip'], array_merge($aTextAttribs, array('maxlength' => 13,'tabindex' => 14)), '', 'nip', false,$aConfig['class']['form']['nip']['pcre']);
		

	 	if (!isLoggedIn() && Common::moduleExists('m_newsletter')) {
	 		// dolaczenie klasy Newsletter
	 		include_once('modules/m_newsletter/client/Newsletter.class.php');
	 		$oNewsletter = new Newsletter('m_newsletter', 0, array(), '');

	 		$aForm['newsletter']['categories_groups'] =& $oNewsletter->getCategoriesGroupsList();
			// pobranie kategorii newslettera
			$aForm['newsletter']['categories'] =& $oNewsletter->getCategoriesList();
			if(isset($_POST['n_categories'])) {
				$aForm['newsletter']['n_categories'] =& $oNewsletter->getSelectedCatsFromPost();
			} else {
				$aForm['newsletter']['n_categories'] =& $oNewsletter->getDefaultSelectedCategories();
			}
	 		
			$aForm['fields']['newsletter']['label'] = $oForm->GetLabelHTML('newsletter', $aLang['newsletter'], false);
			$aForm['fields']['newsletter']['template'] ='modules/m_konta/_newsletter.tpl';
			
	 	}
	 	
	 	
	 	if ($aData['invoice_data'] == '0') {
			$aCompanyAttribs = array(
				'disabled' => 'disabled',
				'class' => 'inputDisabled'
			);
			$bInvoiceRequired = false;
			$aForm['fields']['company_section']['show'] ='0';
		}
		else {
			$aCompanyAttribs = array(
			'class' => 'text'
			);
			$bInvoiceRequired = true;
			$aForm['fields']['company_section']['show'] ='1';
		}

		$aForm['fields']['company_section']['invoice_data']['label'] = $oForm->GetLabelHTML('invoice_data', $aModule['lang']['invoice_data'],false);
 		$aForm['fields']['company_section']['invoice_data']['input'] = $oForm->GetCheckBoxHTML('invoice_data',$aModule['lang']['invoice_data'],array('onclick'=>'toggleActivity(this, \'ty_\', \'inputDisabled\', \'text\'); toggleInvoiceArea(this);','tabindex' => 15),'',$aData['invoice_data'],false);
		
 		$aForm['fields']['company_section']['company']['label'] = $oForm->GetLabelHTML('invoice_company', $aLang['company'], false);
		$aForm['fields']['company_section']['company']['input'] = $oForm->GetTextHTML('invoice_company', $aLang['company'], $aData['invoice_company'], array_merge($aTextAttribs, array('maxlength' => 255, 'id' => 'ty_company','tabindex' => 16)), '', 'text', false, '');

		$aForm['fields']['company_section']['nip']['label'] = $oForm->GetLabelHTML('invoice_nip', $aLang['nip'], false);
		$aForm['fields']['company_section']['nip']['input'] = $oForm->GetTextHTML('invoice_nip', $aLang['nip'], $aData['invoice_nip'], array_merge($aTextAttribs, array('maxlength' => 13, 'id' => 'ty_nip','tabindex' => 17)), '', 'nip', false,$aConfig['class']['form']['nip']['pcre']);

		$aForm['fields']['company_section']['street']['label'] = $oForm->GetLabelHTML('invoice_street', $aLang['street'], true);
		$aForm['fields']['company_section']['street']['input'] = $oForm->GetTextHTML('invoice_street', $aLang['street'], $aData['invoice_street'], array_merge($aTextAttribs, array('maxlength' => 64, 'id' => 'ty_street','tabindex' => 18)), '', 'text', $bInvoiceRequired, '', array(), 'invoice_data');
		
		$aForm['fields']['company_section']['number']['label'] = $oForm->GetLabelHTML('invoice_number', $aLang['number'], true);
		$aForm['fields']['company_section']['number']['input'] = $oForm->GetTextHTML('invoice_number', $aLang['number'], $aData['invoice_number'], array_merge($aTextAttribs, array('maxlength' => 8, 'id' => 'ty_number','tabindex' => 19)), '', 'text', $bInvoiceRequired, '', array(), 'invoice_data');
 
		$aForm['fields']['company_section']['number2']['label'] = $oForm->GetLabelHTML('invoice_number2', $aLang['number2'], false);
		$aForm['fields']['company_section']['number2']['input'] = $oForm->GetTextHTML('invoice_number2', $aLang['number2'], $aData['invoice_number2'], array_merge($aTextAttribs, array('maxlength' => 8, 'id' => 'ty_number2','tabindex' => 20)), '', 'text', false, '');
 
		
		$aForm['fields']['company_section']['postal']['label'] = $oForm->GetLabelHTML('invoice_postal', $aLang['postal'], true);
		$aForm['fields']['company_section']['postal']['input'] = $oForm->GetTextHTML('invoice_postal', $aLang['postal'], $aData['invoice_postal'], array_merge($aTextAttribs, array('maxlength' => 6,'id' => 'ty_postal','tabindex' => 21)), '', 'postal', $bInvoiceRequired, '', array(), 'invoice_data');

		$aForm['fields']['company_section']['city']['label'] = $oForm->GetLabelHTML('invoice_city', $aLang['city'], true);
		$aForm['fields']['company_section']['city']['input'] = $oForm->GetTextHTML('invoice_city', $aLang['city'], $aData['invoice_city'], array_merge($aTextAttribs, array('maxlength' => 32, 'id' => 'ty_city','tabindex' => 22)), '', 'text', $bInvoiceRequired, '', array(), 'invoice_data');
 
	 	
 		if (!empty($this->aSettings['regulations_page_id'])) {
 			// okreslenie linki strony z regulaminem
 			$aPage = array('mtype' => '1',
							 			 'link_to_id' => $this->aSettings['regulations_page_id']);
			$sRegulationsPageLink = getPageLink($aPage);

 			$aForm['fields']['regulations_agreement']['regulations']['label'] = $oForm->GetLabelHTML('regulations', sprintf($aLang['regulations'], $sRegulationsPageLink), true);
			$aForm['fields']['regulations_agreement']['regulations']['input'] = $oForm->GetCheckBoxHTML('regulations', $aLang['regulations'], array(), $aLang['regulations_err'], isset($aData['regulations']), true);
 		}

// 			if (!isLoggedIn()) {
// 				$aForm['fields']['privacy_agreement']['privacy']['label'] = $oForm->GetLabelHTML('privacy', $aLang['privacy_text'], true);
//				$aForm['fields']['privacy_agreement']['privacy']['input'] = $oForm->GetCheckBoxHTML('privacy', $aLang['privacy'], array(), '', isset($aData['privacy']), true);
// 			}

	 	$aForm['fields']['do_login']['input'] = $oForm->GetInputButtonHTML('send', $aConfig['lang']['common']['do_register']);

		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();
		
		$oForm->setAdditionalJS("function toggleInvoiceArea(oCheckbox){
		if(oCheckbox.checked) {
			$(\"#invoice_section_area\").show();
		} else {
				$(\"#invoice_section_area\").hide();
				}
		}
		");
		$aForm['JS'] = $oForm->GetJS();
		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));

	 	return $aForm;
	} // end of getRegistrationForm()
	
	/**
	 * Metoda zwraca formularz rejestracji / edycji uzytkownika
	 *
	 * @return	array	- tablica z zawartoscia formularza
	 */
	function &getRegistrationStep1Form($sReferer='') {
	 	global $aConfig;
	 	$aForm = array();
	 	$aData = array();

	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];

	 	if(!empty($_SESSION['registration'])){
	 		$aData =& $_SESSION['registration'];
	 	}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('registerForm',
													 '',
													 array('action' => $this->sPageLink),
													 array(),
													 false);

		$aTextAttribs = array(
			'class' => 'text'
		);

		
		/*
 		$aForm['fields']['name']['label'] = $oForm->GetLabelHTML('name', $aLang['name'], true);
		$aForm['fields']['name']['input'] = $oForm->GetTextHTML('name', $aLang['name'], $aData['name'], array_merge($aTextAttribs, array('maxlength' => 32,'tabindex' => 1)), $aLang['name_err'], 'text', true);

		$aForm['fields']['surname']['label'] = $oForm->GetLabelHTML('surname', $aLang['surname'], true);
		$aForm['fields']['surname']['input'] = $oForm->GetTextHTML('surname', $aLang['surname'], $aData['surname'], array_merge($aTextAttribs, array('maxlength' => 32,'tabindex' => 2)), $aLang['surname_err'], 'text', true);
		*/
		// adres email
		$aForm['fields']['email']['label'] = $oForm->GetLabelHTML('email', $aLang['email'], true);
		$aForm['fields']['email']['input'] = $oForm->GetTextHTML('email', $aLang['email'], $aData['email'], array_merge($aTextAttribs, array('maxlength' => 64,'tabindex' => 3)), $aLang['email_err'], 'email',true);
		
		// potwierdz adres email
		$aForm['fields']['confirm_email']['label'] = $oForm->GetLabelHTML('confirm_email', $aLang['confirm_email'], true);
		$aForm['fields']['confirm_email']['input'] = $oForm->GetTextHTML('confirm_email', $aLang['confirm_email'], $aData['confirm_email'], array_merge($aTextAttribs, array('maxlength' => 64,'tabindex' => 4)), $aLang['confirm_email_err'], 'email');
	
		$aForm['fields']['passwd']['label'] = $oForm->GetLabelHTML('passwd',  $aLang['passwd'], true);
		$aForm['fields']['passwd']['input'] = $oForm->GetPasswordHTML('passwd',  $aLang['passwd'], '', array_merge($aTextAttribs, array('maxlength' => 16,'tabindex' => 5)), $aLang['passwd_err'], true);

		$aForm['fields']['passwd2']['label'] = $oForm->GetLabelHTML('passwd2', $aLang['passwd2'], true);
		$aForm['fields']['passwd2']['input'] = $oForm->GetConfirmPasswordHTML('passwd2', $aLang['passwd2'], '', 'passwd', array_merge($aTextAttribs, array('tabindex' => 6)), $aLang['confirm_passwd_err']);
 	
		$aForm['fields']['phone']['label'] = $oForm->GetLabelHTML('phone', $aLang['phone'], true);
		$aForm['fields']['phone']['input'] = $oForm->GetTextHTML('phone', $aLang['phone'], (!empty($aData['phone'])?$aData['phone']:''), array_merge($aTextAttribs, array('maxlength' => 16,'tabindex' => 7)), $aLang['phone_err'], 'phone', true);

		$aForm['accept'] =& $this->getRegulationsAcceptForm($oForm, $aData);
		
	 	$aForm['fields']['do_login']['input'] = $oForm->GetInputButtonHTML('send', $aConfig['lang']['common']['do_register']);

		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();
		
		//$oForm->setAdditionalJS("");
		//$aForm['JS'] = $oForm->GetJS();
		$aOmmit= array('email','confirm_email','passwd2');
		$aForm['JS'] = '
	<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
			$("#registerForm input").blur(function() {
			  '.
		(!empty($aOmmit)?$this->getAjaxCondition($aOmmit).'
			performFieldAjaxValidation("registerForm",this.name,$("#registerForm :input").serialize());
		} else {
		':'').$oForm->GetBlurFieldValidatorJS("registerForm",$aOmmit).(!empty($aOmmit)?"\n} \n":'').'
			});
			$("#registerForm").submit(function() {
				//performAjaxValidation("registerForm",$("#registerForm input").serialize());
			  if($("#registerForm input:enabled").hasClass("validErr")){
			  	alert("'.$aLang['form_send_err'].'");
			 		return false;
			  } else 
			  	return true;
			});
		});
	// ]]>
</script>
		
		';
		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));
		$aForm['ajaxval'] = $oForm->GetHiddenHTML('__ajaxValidator', 'register1',
																								 array('id' => 'uaF__ajaxValidator'));
																								 
		if ($sReferer != '') {
			$aForm['ref_link'] = $oForm->GetHiddenHTML('ref', $sReferer);
		}

	 	return $aForm;
	} // end of getRegistrationStep1Form()
	
	/**
	 * Metoda zwraca formularz rejestracji / edycji uzytkownika
	 *
	 * @return	array	- tablica z zawartoscia formularza
	 */
	function &getUserDataForm($iNr,&$aForm, &$oForm, &$aData,$bEnabled=true,$sDependsOn='',$sDependsVal='', $bEditOnly=false) {
	 	global $aConfig;
	 	//$aForm = array();
	 	//$aData = array();
	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		$aMiddleTextAttribs = array(
			'class' => 'middle_text'
		);
		$aTextAttribs = array(
			'class' => 'text'
		);
		$aMainAttribs = array(
			'class' => 'text'
		);
		$aShortTextAttribs = array(
			'class' => 'short_text'
		);
		$aPrivateAttribs = array(
			'class' => 'text priv addr_'.$iNr
		);
		$aCorporateAttribs = array(
			'class' => 'text corp addr_'.$iNr
		);
		$aDisabledAttribs = array(
			'disabled' => 'disabled'
		);
		if(!$bEnabled){
			$aPrivateAttribs['disabled'] = 'disabled';
			$aCorporateAttribs['disabled'] = 'disabled';
			$aMainAttribs['disabled'] = 'disabled';
		} else {
			if($aData['is_company_'.$iNr] == '1'){
				$aPrivateAttribs['disabled'] = 'disabled';
			} else {
				$aCorporateAttribs['disabled'] = 'disabled';
			}
		}
		if(!isset($aData['is_company_'.$iNr])){
			$aData['is_company_'.$iNr] = '0';
		}
		$aType = array(
			array('value' => '0','label' => $aLang['is_company_0_'.$iNr],'id'=>'addr_'.$iNr.'_is_company_0','onclick'=>'toggleAddresType('.$iNr.',0)'),
			array('value' => '1','label' => $aLang['is_company_1_'.$iNr],'id'=>'addr_'.$iNr.'_is_company_1','onclick'=>'toggleAddresType('.$iNr.',1)')
		);
		if(!$bEnabled){
			$aType[0]['disabled']='disabled';
			$aType[1]['disabled']='disabled';
		}
		$aForm['addr_'.$iNr]['is_company']['label'] = $oForm->GetLabelHTML('is_company_'.$iNr, $aLang['is_company'], true);
		$aForm['addr_'.$iNr]['is_company']['input'] = $oForm->GetRadioSetHTML('is_company_'.$iNr, $aLang['is_company'], $aType, $aData['is_company_'.$iNr], '', true, false,$sDependsOn,$sDependsVal);
		$aForm['addr_'.$iNr]['is_company']['value'] = $aData['is_company_'.$iNr];
		

		$aForm['addr_'.$iNr]['name']['label'] = $oForm->GetLabelHTML('address_name_'.$iNr, $aLang['address_name'], true);
		$aForm['addr_'.$iNr]['name']['input'] = $oForm->GetTextHTML('address_name_'.$iNr, $aLang['address_name'], $aData['address_name_'.$iNr], array_merge($aMainAttribs, array('maxlength' => 45)), $aLang['address_name_err'], 'text', true,'',array(),$sDependsOn,$sDependsVal);
		
 		$aForm['addr_'.$iNr]['priv']['name']['label'] = $oForm->GetLabelHTML('priv_name_'.$iNr, $aLang['name'], true);
		$aForm['addr_'.$iNr]['priv']['name']['input'] = $oForm->GetTextHTML('priv_name_'.$iNr, $aLang['name'], $aData['priv_name_'.$iNr], array_merge($aPrivateAttribs, array('maxlength' => 45)), $aLang['name_err'], 'text', true,'',array(),'is_company_'.$iNr,'0');

		$aForm['addr_'.$iNr]['priv']['surname']['label'] = $oForm->GetLabelHTML('priv_surname_'.$iNr, $aLang['surname'], true);
		$aForm['addr_'.$iNr]['priv']['surname']['input'] = $oForm->GetTextHTML('priv_surname_'.$iNr, $aLang['surname'], $aData['priv_surname_'.$iNr], array_merge($aPrivateAttribs, array('maxlength' => 45)), $aLang['surname_err'], 'text', true,'',array(),'is_company_'.$iNr,'0');

//		$aForm['addr_'.$iNr]['priv']['nip']['label'] = $oForm->GetLabelHTML('priv_nip_'.$iNr, $aLang['ev_number'], false);
//		$aForm['addr_'.$iNr]['priv']['nip']['input'] = $oForm->GetTextHTML('priv_nip_'.$iNr, $aLang['ev_number'], $aData['priv_nip_'.$iNr], array_merge($aPrivateAttribs, array('maxlength' => 13)), '', '', false);
		
 		$aForm['addr_'.$iNr]['priv']['street']['label'] = $oForm->GetLabelHTML('priv_street_'.$iNr, $aLang['street'], true);
		$aForm['addr_'.$iNr]['priv']['street']['input'] = $oForm->GetTextHTML('priv_street_'.$iNr, $aLang['street'], $aData['priv_street_'.$iNr], array_merge($aPrivateAttribs, array('maxlength' => 150)), $aLang['street_err'], 'text', true,'',array(),'is_company_'.$iNr,'0');

		$aForm['addr_'.$iNr]['priv']['number']['label'] = $oForm->GetLabelHTML('priv_number_'.$iNr, $aLang['number'], true);
		$aForm['addr_'.$iNr]['priv']['number']['input'] = $oForm->GetTextHTML('priv_number_'.$iNr, $aLang['number'], $aData['priv_number_'.$iNr], array_merge($aPrivateAttribs, array('maxlength' => 8)), $aLang['number_err'], 'text', true,'',array(),'is_company_'.$iNr,'0');
 
		$aForm['addr_'.$iNr]['priv']['number2']['label'] = $oForm->GetLabelHTML('priv_number2_'.$iNr, $aLang['number2'], false);
		$aForm['addr_'.$iNr]['priv']['number2']['input'] = $oForm->GetTextHTML('priv_number2_'.$iNr, $aLang['number2'], $aData['priv_number2_'.$iNr], array_merge($aPrivateAttribs, array('maxlength' => 8)), $aLang['number2_err'], 'text', false);
 
		$aForm['addr_'.$iNr]['priv']['postal']['label'] = $oForm->GetLabelHTML('priv_postal_'.$iNr, $aLang['postal'], true);
		$aForm['addr_'.$iNr]['priv']['postal']['input'] = $oForm->GetTextHTML('priv_postal_'.$iNr, $aLang['postal'], $aData['priv_postal_'.$iNr], array_merge($aPrivateAttribs, array('maxlength' => 6)), $aLang['postal_err'], 'postal', true,'',array(),'is_company_'.$iNr,'0');

		$aForm['addr_'.$iNr]['priv']['city']['label'] = $oForm->GetLabelHTML('priv_city_'.$iNr, $aLang['city'], true);
		$aForm['addr_'.$iNr]['priv']['city']['input'] = $oForm->GetTextHTML('priv_city_'.$iNr, $aLang['city'], $aData['priv_city_'.$iNr], array_merge($aPrivateAttribs, array('maxlength' => 32)), $aLang['city_err'], 'text', true,'',array(),'is_company_'.$iNr,'0');
 /*
 		$aForm['addr_'.$iNr]['priv']['phone']['label'] = $oForm->GetLabelHTML('priv_phone_'.$iNr, $aLang['phone'], true);
		$aForm['addr_'.$iNr]['priv']['phone']['input'] = $oForm->GetTextHTML('priv_phone_'.$iNr, $aLang['phone'], (!empty($aData['priv_phone_'.$iNr])?$aData['priv_phone_'.$iNr]:''), array_merge($aPrivateAttribs, array('maxlength' => 16)), $aLang['phone_err'], 'phone', true,'',array(),'is_company_'.$iNr,'0');
*/
		$aForm['addr_'.$iNr]['corp']['company']['label'] = $oForm->GetLabelHTML('corp_company_'.$iNr, $aLang['company'], true);
		$aForm['addr_'.$iNr]['corp']['company']['input'] = $oForm->GetTextHTML('corp_company_'.$iNr, $aLang['company'], $aData['corp_company_'.$iNr], array_merge($aCorporateAttribs, array('maxlength' => 255)), $aLang['company_err'], 'text', true, '', array(),'is_company_'.$iNr,'1');
		
		if($iNr=='1' || $iNr=='0') {
			$aForm['addr_'.$iNr]['corp']['nip']['label'] = $oForm->GetLabelHTML('corp_nip_'.$iNr, $aLang['nip'], true);
			$aForm['addr_'.$iNr]['corp']['nip']['input'] = $oForm->GetTextHTML('corp_nip_'.$iNr, $aLang['nip'], $aData['corp_nip_'.$iNr], array_merge($aCorporateAttribs, array('maxlength' => 13)), $aLang['nip_err'], 'nip', true,$aConfig['class']['form']['nip']['pcre'], array(),'is_company_'.$iNr,'1');
		}
		
		/*
		$aForm['addr_'.$iNr]['corp']['name']['label'] = $oForm->GetLabelHTML('corp_name_'.$iNr, $aLang['name'], true);
		$aForm['addr_'.$iNr]['corp']['name']['input'] = $oForm->GetTextHTML('corp_name_'.$iNr, $aLang['name'], $aData['corp_name_'.$iNr], array_merge($aCorporateAttribs, array('maxlength' => 45)), $aLang['name_err'], 'text', true, '', array(),'is_company_'.$iNr,'1');

		$aForm['addr_'.$iNr]['corp']['surname']['label'] = $oForm->GetLabelHTML('corp_surname_'.$iNr, $aLang['surname'], true);
		$aForm['addr_'.$iNr]['corp']['surname']['input'] = $oForm->GetTextHTML('corp_surname_'.$iNr, $aLang['surname'], $aData['corp_surname_'.$iNr], array_merge($aCorporateAttribs, array('maxlength' => 45)), $aLang['surname_err'], 'text', true, '', array(),'is_company_'.$iNr,'1');
*/
 		$aForm['addr_'.$iNr]['corp']['street']['label'] = $oForm->GetLabelHTML('corp_street_'.$iNr, $aLang['street'], true);
		$aForm['addr_'.$iNr]['corp']['street']['input'] = $oForm->GetTextHTML('corp_street_'.$iNr, $aLang['street'], $aData['corp_street_'.$iNr], array_merge($aCorporateAttribs, array('maxlength' => 150)), $aLang['street_err'], 'text', true, '', array(),'is_company_'.$iNr,'1');

		$aForm['addr_'.$iNr]['corp']['number']['label'] = $oForm->GetLabelHTML('corp_number_'.$iNr, $aLang['number'], true);
		$aForm['addr_'.$iNr]['corp']['number']['input'] = $oForm->GetTextHTML('corp_number_'.$iNr, $aLang['number'], $aData['corp_number_'.$iNr], array_merge($aCorporateAttribs, array('maxlength' => 8)), $aLang['number_err'], 'text', true, '', array(),'is_company_'.$iNr,'1');
 
		$aForm['addr_'.$iNr]['corp']['number2']['label'] = $oForm->GetLabelHTML('corp_number2_'.$iNr, $aLang['number2'], false);
		$aForm['addr_'.$iNr]['corp']['number2']['input'] = $oForm->GetTextHTML('corp_number2_'.$iNr, $aLang['number2'], $aData['corp_number2_'.$iNr], array_merge($aCorporateAttribs, array('maxlength' => 8)), $aLang['number2_err'], 'text', false);
 
		$aForm['addr_'.$iNr]['corp']['postal']['label'] = $oForm->GetLabelHTML('corp_postal_'.$iNr, $aLang['postal'], true);
		$aForm['addr_'.$iNr]['corp']['postal']['input'] = $oForm->GetTextHTML('corp_postal_'.$iNr, $aLang['postal'], $aData['corp_postal_'.$iNr], array_merge($aCorporateAttribs, array('maxlength' => 6)), $aLang['postal_err'], 'postal', true, '', array(),'is_company_'.$iNr,'1');

		$aForm['addr_'.$iNr]['corp']['city']['label'] = $oForm->GetLabelHTML('corp_city_'.$iNr, $aLang['city'], true);
		$aForm['addr_'.$iNr]['corp']['city']['input'] = $oForm->GetTextHTML('corp_city_'.$iNr, $aLang['city'], $aData['corp_city_'.$iNr], array_merge($aCorporateAttribs, array('maxlength' => 32)), $aLang['city_err'], 'text', true, '', array(),'is_company_'.$iNr,'1');
 /*
 		$aForm['addr_'.$iNr]['corp']['phone']['label'] = $oForm->GetLabelHTML('corp_phone_'.$iNr, $aLang['phone'], true);
		$aForm['addr_'.$iNr]['corp']['phone']['input'] = $oForm->GetTextHTML('corp_phone_'.$iNr, $aLang['phone'], (!empty($aData['corp_phone_'.$iNr])?$aData['corp_phone_'.$iNr]:''), array_merge($aCorporateAttribs, array('maxlength' => 16)), $aLang['phone_err'], 'phone', true, '', array(),'is_company_'.$iNr,'1');
*/
		if($iNr=='1' || $iNr=='2') {
			/*
				$aForm['accept']['reg']['label'] = $oForm->GetLabelHTML('accept_reg', $aLang['accept_reg'], true);
				$aForm['accept']['reg']['input'] = $oForm->GetCheckBoxHTML('accept_reg', $aLang['accept_reg_err'], array(), '', (isset($_POST['accept_reg']) || !isset($_POST['sentForm']))?true:false, true);

				$aForm['accept']['promotions_agreement']['label'] = $oForm->GetLabelHTML('promotions_agreement', $aLang['promotions_agreement_text'], false);
				$aForm['accept']['promotions_agreement']['input'] = $oForm->GetCheckBoxHTML('promotions_agreement', $aLang['promotions_agreement'], array(), '', (!isset($_POST['sentForm'])?true:isset($aData['promotions_agreement'])?true:false), false);

				$aForm['accept']['priv']['label'] = $oForm->GetLabelHTML('accept_priv', $aLang['accept_priv'], true);
				$aForm['accept']['priv']['input'] = $oForm->GetCheckBoxHTML('accept_priv', $aLang['accept_priv_err'], array(), '', (isset($_POST['accept_priv']) || !isset($_POST['sentForm']))?true:false, true);
			*/
		}
		
//		if($bEditOnly) {
			//jesli jestesmy w edycji to pokazujemy pola do zmian domyslnych
 			$aForm['addr_'.$iNr]['default_invoice']['label'] = $oForm->GetLabelHTML('default_invoice_'.$iNr, $aLang['default_invoice'], false);
			$aForm['addr_'.$iNr]['default_invoice']['input']=  $oForm->GetCheckBoxHTML('default_invoice_'.$iNr, $aLang['default_invoice'], array(), '', $aData['default_invoice_'.$iNr]=='1'?true:false, false);
			$aForm['addr_'.$iNr]['default_transport']['label'] = $oForm->GetLabelHTML('default_transport_'.$iNr, $aLang['default_transport'], false);
			$aForm['addr_'.$iNr]['default_transport']['input']=  $oForm->GetCheckBoxHTML('default_transport_'.$iNr, $aLang['default_transport'], array(), '', $aData['default_transport_'.$iNr]=='1'?true:false, false);
//			}
	} // end of getRegistrationForm()
	
	
	/**
	 * Metoda pobiera formularz akceptacji regulaminów
	 *
	 * @global array $aConfig
	 * @param object ref $oForm
	 * @param array $aData
	 * @return array 
	 */
	function getRegulationsAcceptForm(&$oForm, &$aData, $bIsEdit=false) {
	 	global $aConfig;
	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		$aAttr = array();
		$bDefRequired = true;
		if ($bIsEdit === true) {
            // jeśli edycja, można edytować tylko zgodę na promocję
            $aAttr = array('readonly' => 'readonly', 'disabled' => 'disabled');
            $bDefRequired = false;
            $aData['accept_priv'] = 1;
            $aData['accept_reg'] = 1;
        }

		$aForm['reg']['label'] = $oForm->GetLabelHTML('accept_reg', $aLang['accept_reg'], true);
		$aForm['reg']['input'] = $oForm->GetCheckBoxHTML('accept_reg', $aLang['accept_reg_err'], $aAttr, '', (isset($aData['accept_reg']))?true:false, $bDefRequired);

		$aForm['promotions_agreement']['label'] = $oForm->GetLabelHTML('promotions_agreement', $aLang['promotions_agreement_text'], false);
		$aForm['promotions_agreement']['input'] = $oForm->GetCheckBoxHTML('promotions_agreement', $aLang['promotions_agreement'], array(), '', $aData['promotions_agreement'], false);

//		$aForm['priv']['label'] = $oForm->GetLabelHTML('accept_priv', $aLang['accept_priv'], true);
//		$aForm['priv']['input'] = $oForm->GetCheckBoxHTML('accept_priv', $aLang['accept_priv_err'], $aAttr, '', (isset($aData['accept_priv']))?true:false, $bDefRequired); //  || !isset($aData['sentForm'])
		
		return $aForm;
	}// end of getRegulationsAcceptForm() method
	
	
	/**
	 * Metoda generuje formularz zmiany ustawień konfiguracyjnych
	 *
	 * @return array 
	 */
	function getEditAccountSettingsForm() {
	 	$aForm = array();
	 	$aData = array();

	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		if (empty($aData)) {
			$aData = $this->getUserData();
		}

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('settingsForm',
													 '',
													 array('action' => $this->sPageLink),
													 array(),
													 false);

		$aTextAttribs = array(
			'class' => 'text'
		);
		
		
		$aForm['accept'] = $this->getRegulationsAcceptForm($oForm, $aData, true);
		
		$aForm['fields']['send']['input'] = $oForm->GetInputButtonHTML('send', $aConfig['lang']['common']['do_save']);

		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();

		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));
		return $aForm;
	}
	
	
	/**
	 * Metoda zwraca formularz rejestracji / edycji uzytkownika
	 *
	 * @return	array	- tablica z zawartoscia formularza
	 */
	function &getRegistrationStep2Form($sReferer='') {
	 	global $aConfig;
	 	$aForm = array();
	 	$aData = array();
	 	$aLoginAttribs = array();
	 	$bLoginDataRequired = true;
	 	$bShowAgreements = true;
	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];
	 	$sPasswdLabel =& $aLang['passwd'];
	 	$sAvatar = '';

		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		if(empty($aData)){
			$aUserData = $this->getUserData();
			$aData['priv_name_1'] = $aUserData['name'];
			$aData['priv_surname_1'] = $aUserData['surname'];
			$aData['corp_name_1'] = $aUserData['name'];
			$aData['corp_surname_1'] = $aUserData['surname'];
			$aData['corp_name_2'] = $aUserData['name'];
			$aData['corp_surname_2'] = $aUserData['surname'];
		}
		if(!isset($aData['transport_addres'])){
			$aData['transport_addres'] = '0';
		}
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('registerStep2',
													 '',
													 array('action' => $this->sPageLink, 'enctype' => 'multipart/form-data'),
													 array(),
													 false);

		$aMiddleTextAttribs = array(
			'class' => 'middle_text'
		);
		$aTextAttribs = array(
			'class' => 'text'
		);
		$aShortTextAttribs = array(
			'class' => 'short_text'
		);
		$aSelectAttribs = array(
			'class' => 'select'
		);
		$aDisabledEmail = array(
			'class' => 'inputDisabled',
			'readonly' => 'readonly'
		);
		
		//////// 
		$this->getUserDataForm(1,$aForm,$oForm,$aData);
		$this->getUserDataForm(2,$aForm,$oForm,$aData,($aData['transport_addres'] == '1'),'transport_addres','1');
		
		$aTransport = array(
			array('value' => '0','label' => $aLang['transport_addres_0'],'id'=>'transport_addres_0','onclick'=>'toggleTransportAddres(0)'),
			array('value' => '1','label' => $aLang['transport_addres_1'],'id'=>'transport_addres_1','onclick'=>'toggleTransportAddres(1)', 'checked'=>'checked')
		);
		$aForm['transport_addres']['label'] = $oForm->GetLabelHTML('transport_addres', $aLang['transport_addres'], true);
		$aForm['transport_addres']['input'] = $oForm->GetRadioSetHTML('transport_addres', $aLang['transport_addres'], $aTransport, $aData['transport_addres'], '', true, false);
		$aForm['transport_addres']['value'] = $aData['transport_addres'];

	 	if (isLoggedIn() && Common::moduleExists('m_newsletter')) {
	 		// dolaczenie klasy Newsletter
	 		include_once('modules/m_newsletter/client/Newsletter.class.php');
	 		$oNewsletter = new Newsletter('m_newsletter', 0, array(), '');

	 		$aForm['newsletter']['categories_groups'] =& $oNewsletter->getCategoriesGroupsList();
			// pobranie kategorii newslettera
			$aForm['newsletter']['categories'] =& $oNewsletter->getCategoriesList();
			if(isset($_POST['n_categories'])) {
				$aForm['newsletter']['n_categories'] =& $oNewsletter->getSelectedCatsFromPost();
			} else {
				$aForm['newsletter']['n_categories'] =& $oNewsletter->getDefaultSelectedCategories();
			}
	 		
			$aForm['fields']['newsletter']['label'] = $oForm->GetLabelHTML('newsletter', $aLang['newsletter'], false);
			$aForm['fields']['newsletter']['template'] ='modules/m_konta/_newsletter.tpl';
			
	 	}


 		if (!empty($this->aSettings['regulations_page_id'])) {
 			// okreslenie linki strony z regulaminem
 			$aPage = array('mtype' => '1',
							 			 'link_to_id' => $this->aSettings['regulations_page_id']);
			$sRegulationsPageLink = getPageLink($aPage);

 			$aForm['fields']['regulations_agreement']['regulations']['label'] = $oForm->GetLabelHTML('regulations', sprintf($aLang['regulations'], $sRegulationsPageLink), true);
			$aForm['fields']['regulations_agreement']['regulations']['input'] = $oForm->GetCheckBoxHTML('regulations', $aLang['regulations'], array(), $aLang['regulations_err'], isset($aData['regulations']), true);
 		}

	 	$aForm['fields']['do_login']['input'] = $oForm->GetInputButtonHTML('send', $aConfig['lang']['common']['do_register']);

		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();
		
		$oForm->setAdditionalJS("
		");
		$aOmmit = array('priv_city_1','priv_city_2','corp_city_1','corp_city_2');
		$aForm['JS'] = '
	<script type="text/javascript">
	// <![CDATA[
		function toggleAddresType(iAddrNr,iType){
			if(iType == 1){
				$("#addr"+iAddrNr+"_grp .corp").removeAttr("disabled");
				$("#addr"+iAddrNr+"_grp .priv").attr("disabled","disabled");
				$("#addr"+iAddrNr+"_grp .priv_grp").hide();
				$("#addr"+iAddrNr+"_grp .corp_grp").show();
			} else {
				$("#addr"+iAddrNr+"_grp .priv").removeAttr("disabled");
				$("#addr"+iAddrNr+"_grp .corp").attr("disabled","disabled");
				$("#addr"+iAddrNr+"_grp .corp_grp").hide();
				$("#addr"+iAddrNr+"_grp .priv_grp").show();
			}
		}
		function toggleTransportAddres(iType){
			if(iType == 1){
				$("#addr_2_is_company_0").attr("checked","checked");
				toggleAddresType(2,0);
				$("#addr2_grp .priv").removeAttr("disabled");
				$("#addr2_grp input[name=is_company_2]").removeAttr("disabled");
				$("#address_name_2").removeAttr("disabled");
				//alert($("#addr1_grp input:radio:checked").val());
				if($("#addr1_grp input:radio:checked").val() == "1"){
					$("#addr2_grp input[name=priv_name_2]").val($("#addr1_grp input[name=corp_name_1]").val());
					$("#addr2_grp input[name=priv_surname_2]").val($("#addr1_grp input[name=corp_surname_1]").val());
					$("#addr2_grp input[name=priv_phone_2]").val($("#addr1_grp input[name=corp_phone_1]").val());
				} else {
					$("#addr2_grp input[name=priv_name_2]").val($("#addr1_grp input[name=priv_name_1]").val());
					$("#addr2_grp input[name=priv_surname_2]").val($("#addr1_grp input[name=priv_surname_1]").val());
					$("#addr2_grp input[name=priv_phone_2]").val($("#addr1_grp input[name=priv_phone_1]").val());
				}
				$("#addr2_grp").show();
			} else {
				$("#addr2_grp input").attr("disabled","disabled");
				$("#addr2_grp").hide();
			}
		}
		$(document).ready(function(){
			toggleTransportAddres(1); // wyswietlenie wyboru adresu dostawy
			$("#registerStep2 input").blur(function() {
				'.
		(!empty($aOmmit)?$this->getAjaxCondition($aOmmit).'
			performFieldAjaxValidation("registerStep2",this.name,$("#registerStep2 :input").serialize());
		} else {
		':'').$oForm->GetBlurFieldValidatorJS("registerStep2",$aOmmit).(!empty($aOmmit)?"\n} \n":'').'
			});
			$("#registerStep2").submit(function() {
			//	performAjaxValidation("registerStep2",$("#registerStep2 input").serialize());
			  if($("#registerStep2 input:enabled").hasClass("validErr")){
			  	alert("'.$aLang['form_send_err'].'");
			 		return false;
			  } else 
			  	return true;
			});
			$("#priv_postal_1").autocomplete({ source : "/internals/GetPostals.php", minLength: 2});
			$("#priv_postal_2").autocomplete({ source : "/internals/GetPostals.php", minLength: 2});
			$("#corp_postal_1").autocomplete({ source : "/internals/GetPostals.php", minLength: 2});
			$("#corp_postal_2").autocomplete({ source : "/internals/GetPostals.php", minLength: 2});
      
			$("#priv_city_1").autocomplete({ 
        source: function( request, response ) {
        $.ajax({
            url: "/internals/GetPostalCities.php",
            dataType: "json",
            data: {
              postal: $("#priv_postal_1").val(),
              term: request.term
            },
            success: function( data ) {
              response( data );
            }
          });
        },
        minLength: 2
      });
      
			$("#priv_city_2").autocomplete({ 
        source: function( request, response ) {
        $.ajax({
            url: "/internals/GetPostalCities.php",
            dataType: "json",
            data: {
              postal: $("#priv_postal_2").val(),
              term: request.term
            },
            success: function( data ) {
              response( data );
            }
          });
        },
        minLength: 2
       });
       
			$("#corp_city_1").autocomplete({ 
        source: function( request, response ) {
        $.ajax({
            url: "/internals/GetPostalCities.php",
            dataType: "json",
            data: {
              postal: $("#corp_postal_1").val(),
              term: request.term
            },
            success: function( data ) {
              response( data );
            }
          });
        },
        minLength: 2
      });
      
			$("#corp_city_2").autocomplete({ 
        source: function( request, response ) {
        $.ajax({
            url: "/internals/GetPostalCities.php",
            dataType: "json",
            data: {
              postal: $("#corp_postal_2").val(),
              term: request.term
            },
            success: function( data ) {
              response( data );
            }
          });
        },
        minLength: 2
      });
		});
	// ]]>
</script>
		
		';
		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));
	  $aForm['ajaxval'] = $oForm->GetHiddenHTML('__ajaxValidator', 'register2',
																								 array('id' => 'uaF__ajaxValidator'));
		if ($sReferer != '') {
			$aForm['ref_link'] = $oForm->GetHiddenHTML('ref', $sReferer);
		}
//dump($aForm); die();
	 	return $aForm;
	} // end of getRegistrationForm()

	
	function getAjaxCondition($aOmmit){
		$sJS='';
		if(!empty($aOmmit)){
			$sJS='if(';
			foreach($aOmmit as $sOmmit){
				$sJS .= 'this.name == "'.$sOmmit.'" || ';
			}
			$sJS = substr($sJS,0,-4);
			$sJS .= ') {';
		}
		return $sJS;
	}

	/**
	 * Metoda zwraca formularz zmiany hasla uzytkownika
	 *
	 * @return	array	- tablica z zawartoscia formularza
	 */
	function &getPasswordChangeForm() {
	 	global $aConfig;
	 	$aForm = array();
	 	$aData =& $this->aUser;
	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];

	 	// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('changePasswordForm',
													 '',
													 array('action' => $this->sPageLink),
													 array(),
													 false);

		$aMiddleTextAttribs = array(
			/*'style' => 'width: '.$aConfig['default']['middle_form_field_width'],*/
			'style' => 'width: 200px',
			'class' => 'middle_text'
		);

    if ($aData['fb_uid'] <= 0 || !empty($aData['passwd'])) {
      $aForm['fields']['old_passwd']['label'] = $oForm->GetLabelHTML('old_passwd', $aLang['old_passwd'], true);
      $aForm['fields']['old_passwd']['input'] = $oForm->GetPasswordHTML('old_passwd', $aLang['old_passwd'], '', array(), $aLang['old_passwd_err']);
    }

		$aForm['fields']['passwd']['label'] = $oForm->GetLabelHTML('n_passwd', $aLang['new_passwd'], true);
		$aForm['fields']['passwd']['input'] = $oForm->GetPasswordHTML('n_passwd', $aLang['new_passwd'], '', array(), $aLang['n_passwd_err']);

		$aForm['fields']['passwd2']['label'] = $oForm->GetLabelHTML('n_passwd2', $aLang['new_passwd2']);
		$aForm['fields']['passwd2']['input'] = $oForm->GetConfirmPasswordHTML('passwd2', $aLang['new_passwd2'], '', 'n_passwd', array(), $aLang['confirm_passwd_err']);

		$aForm['fields']['do_change_passwd']['input'] = $oForm->GetInputButtonHTML('send', $aConfig['lang']['common']['do_change_passwd']);

		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();
		$aForm['JS'] = '
	<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
			$("#changePasswordForm input:enabled").blur(function() {
			  performFieldAjaxValidation("changePasswordForm",this.name,$("#changePasswordForm :input").serialize());
			});
			$("#changePasswordForm").submit(function() {
				//performAjaxValidation("changePasswordForm",$("#changePasswordForm input").serialize());
			  if($("#changePasswordForm input:enabled").hasClass("validErr")){
			  	alert("'.$aLang['form_send_err'].'");
			 		return false;
			  } else 
			  	return true;
			});
		});
	// ]]>
</script>
		
		';
		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));
		/*$aForm['ajaxval'] = $oForm->GetHiddenHTML('__ajaxValidator', 'passwordChange',
																								 array('id' => 'uaF__ajaxValidator'));*/
	 	return $aForm;
	} // end of getPasswordChangeForm() method
	
	/**
	 * Metoda zwraca formularz zmiany hasla uzytkownika
	 *
	 * @return	array	- tablica z zawartoscia formularza
	 */
	function &getEmailChangeForm() {
	 	global $aConfig;
	 	$aForm = array();
	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];

	 	// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('changeEmailForm',
													 '',
													 array('action' => $this->sPageLink),
													 array(),
													 false);


		// adres email
		$aForm['fields']['email']['label'] = $oForm->GetLabelHTML('email', $aLang['email'], true);
		$aForm['fields']['email']['input'] = $oForm->GetTextHTML('email', $aLang['email'], '', array('class'=>'text','maxlength' => 64,'tabindex' => 3), $aLang['email_err'], 'email',true);
		
		// potwierdz adres email
		$aForm['fields']['confirm_email']['label'] = $oForm->GetLabelHTML('confirm_email', $aLang['confirm_email'], true);
		$aForm['fields']['confirm_email']['input'] = $oForm->GetTextHTML('confirm_email', $aLang['confirm_email'], '',array('class'=>'text','maxlength' => 64,'tabindex' => 4), $aLang['confirm_email_err'], 'email');
	
		$aForm['fields']['send']['input'] = $oForm->GetInputButtonHTML('send', $aConfig['lang']['common']['do_change_email']);

		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();
		$aForm['JS'] = '
	<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
			$("#changeEmailForm input:enabled").blur(function() {
			  performFieldAjaxValidation("changeEmailForm",this.name,$("#changeEmailForm :input").serialize());
			});
			$("#changeEmailForm").submit(function() {
				//performAjaxValidation("changeEmailForm",$("#changeEmailForm input").serialize());
			  if($("#changeEmailForm input:enabled").hasClass("validErr")){
			  	alert("'.$aLang['form_send_err'].'");
			 		return false;
			  } else 
			  	return true;
			});
		});
	// ]]>
</script>
		
		';
		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));
		$aForm['ajaxval'] = $oForm->GetHiddenHTML('__ajaxValidator', 'emailChange',
																								 array('id' => 'uaF__ajaxValidator'));
	 	return $aForm;
	} // end of getEmailChangeForm() method



	/**
	 * Metoda tworzy nowe konto uzytkownika
	 *
	 * @return	integer	- -2: konto o podanym adresie email juz istnieje
	 * 										-1: konto o podanym loginie juz istnieje
	 * 										 0: wystapil blad
	 * 									  >0: konto zostalo utworzone - zwracane ID uzytkownika
	 */
	function add($bActive = true, $bAddUserData = true, $iUId=0) {
    global $pDbMgr;
    $bIsErr = false;
    
    $pDbMgr->BeginTransaction('profit24');
    if ($bAddUserData === TRUE) {
      // dodajemy użytkownika
      if (empty($_SESSION['registration']) 
          || !is_array($_SESSION['registration'])
          || empty($_SESSION['registration']['email'])) {
            setMessage(_('Wystąpił błąd. Zarejestruj się ponownie !'), true, true);
            doRedirect('/');
      }
    
      $mReturn = $this->doAddUser($_SESSION['registration'], $_POST, $bActive, $bAddUserData, $iUId);
      if ($mReturn > 0) {
        $pDbMgr->CommitTransaction('profit24');
      } else {
        $pDbMgr->RollbackTransaction('profit24');
      }
      return $mReturn;
      
    } else {
      // dodajemy adres
      
      if($_POST['transport_addres'] == '1'){
        if($this->addUserAddress($iUId,1,true,false) === false){
          $bIsErr = true;
        }
        if($this->addUserAddress($iUId,2,false,true) === false){
          $bIsErr = true;
        }
       } else {
        if($this->addUserAddress($iUId,1,true,true) === false){
          $bIsErr = true;
        }
  //      $_POST['address_name_1'] = 'adres 2';
  //      if($this->addUserAddress($iUId,1,false,true) === false){
  //        $bIsErr = true;
  //      }
       }
       if ($bIsErr === false) {
         $pDbMgr->CommitTransaction('profit24');
         return true;
       } else {
         $pDbMgr->RollbackTransaction('profit24');
         return false;
       }
      }
	} // end of add() method
  
  
  /**
   * Metoda dodaje użytkownika do bazy danych, na podstawie przekazanych danych
   * 
   * @global array $aConfig
   * @global type $pDbMgr
   * @param array $aUserData
   * @param array $aRegulationsData
   * @param bool $bActive
   * @param int $iUId
	 * @return	integer	- -2: konto o podanym adresie email juz istnieje
	 * 										-1: konto o podanym loginie juz istnieje
	 * 										 0: wystapil blad
	 * 									  >0: konto zostalo utworzone - zwracane ID uzytkownika
   */
  public function doAddUser($aUserData, $aRegulationsData, $bActive = true, $iUId=0) {
    global $aConfig, $pDbMgr;
		$bIsErr = false;
    
    $aUserData['email'] = trim($aUserData['email']);
    // sprawdzenie czy uzytkownik o podanym loginie lub adresie email
    // nie istnieje w bazie
    if (!$this->emailExists($aUserData['email'])) {

      
      // mozna utworzyc konto uzytkownika
    /*
     * 
      $aUserData['name'] = filterInput($_POST['name']);
      $aUserData['surname'] = filterInput($_POST['surname']);
      $aUserData['email'] = filterInput(strtolower($_POST['email']));
      $aUserData['confirm_email'] = filterInput(strtolower($_POST['confirm_email']));
      $aUserData['phone'] = filterInput($_POST['phone']);
      $aUserData['passwd'] = encodePasswd($_POST['passwd']);
     */
        $aValues = array(
          'website_id' => $aConfig['profit24_website_id'],
          'passwd' => $aUserData['passwd'],
          'salt' => $aUserData['salt'],
          'email' => $aUserData['email'],
//						'name' => $aUserData['name'],
//						'surname' => $aUserData['surname'],
          'phone' => $aUserData['phone'],
          'privacy_agreement' => isset($aRegulationsData['accept_reg']) ? '1' : '0', // isset($aRegulationsData['accept_priv']) ? '1' : '0',
          'regulations_agreement' => isset($aRegulationsData['accept_reg']) ? '1' : '0',
          'promotions_agreement' => isset($aRegulationsData['promotions_agreement']) ? '1' : '0',
          'created' => date('Y-m-d H:i:s'),
          'active' => $bActive == true ? '1' : '0',
          'registred' => isset($aRegulationsData['registred']) ? $aRegulationsData['registred'] : '1'
        );

        $iUpdatedId = $this->checkUpdateEmailAccount($aUserData['email']);
        if ($iUpdatedId > 0) {
//          unset($aValues['created']);
          if (($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts",
                                      $aValues, ' id='.$iUpdatedId)) === false) {
            $bIsErr = true;
          } else {
            $iUId = $iUpdatedId;
          }
        }
        else {
          if (($iUId = $pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."users_accounts",
                                      $aValues)) === false) {
            $bIsErr = true;
          }
        }
        if(!$bIsErr){
          if($this->addOldOrdersToAccount($aUserData['email'], $iUId) === false){
            $bIsErr = true;
          }
        }
      if ($bIsErr) {
        return 0;
      }
      return $iUId;
    }
    // uzytkownik o podanym adresie email istnieje
    return -2;
  }// end of doAddUser() method
  
  
  
  /**
	 * Dodaje dane użytkownika do tabeli adresów
   *  uzywane przy skladaniu zamowienia z rejestracją
   * 
	 * @param int $iUId - id użytkownika
	 * @param int $iFormAddresId - nr formularza
	 * @param bool $bIsDefaultInvoice - czy domyslny dla faktury
	 * @param bool $bIsDefaultTransport - czy domyslny dla transportu
	 * @return bool - success
	 */
	public function doAddUserAddressNew($iUId, $aData, $bIsDefaultInvoice=false, $bIsDefaultTransport=false){
		global $aConfig, $pDbMgr;
		
    
		if($aData['is_company'] == "1") {
			$aValues = array(
			  'user_id' => $iUId,
			  'address_name' => 'adres '.filterInput($aData['street']),
			  'is_company' => '1',
			  'company' => filterInput($aData['company']),
			  //'name' => isset($_POST['corp_name_'.$iFormAddresId]) && $_POST['corp_name_'.$iFormAddresId] != '' ? filterInput($_POST['corp_name_'.$iFormAddresId]) : '',
			  //'surname' => filterInput($_POST['corp_surname_'.$iFormAddresId]),
			  'street' => filterInput($aData['street']),
			  'number' => filterInput($aData['number']),
			  'number2' => filterInput($aData['number2']),
			  'postal' => filterInput($aData['postal']),
			  'city' => filterInput($aData['city']),
			  'nip' => filterInput($aData['nip']),
			  //'phone' => filterInput($_POST['corp_phone_'.$iFormAddresId]),
			  'is_invoice' => $bIsDefaultInvoice?'1':'0',
			  'is_transport' => $bIsDefaultTransport?'1':'0',
			  'default_invoice' => $bIsDefaultInvoice?'1':'0',
			  'default_transport' => $bIsDefaultTransport?'1':'0',
			  'created' => 'NOW()'
			);
		} else {
			$aValues = array(
			  'user_id' => $iUId,
			  'address_name' => 'adres '.filterInput($aData['street']),
			  'is_company' => '0',
			  'name' => filterInput($aData['name']),
			  'surname' => filterInput($aData['surname']),
			  'street' => filterInput($aData['street']),
			  'number' => filterInput($aData['number']),
			  'number2' => filterInput($aData['number2']),
			  'postal' => filterInput($aData['postal']),
			  'city' => filterInput($aData['city']),
			  //'phone' => filterInput($_POST['priv_phone_'.$iFormAddresId]),
//				'nip' => filterInput($_POST['priv_nip_'.$iFormAddresId]),
			  'is_invoice' => $bIsDefaultInvoice?'1':'0',
			  'is_transport' => $bIsDefaultTransport?'1':'0',
			  'default_invoice' => $bIsDefaultInvoice?'1':'0',
			  'default_transport' => $bIsDefaultTransport?'1':'0',
			  'created' => 'NOW()'
			);
		}
		if($bIsDefaultInvoice) {
			//jezeli ustawiamy adres jako domyslny musimy zresetowac to pole u wszystkich - faktura
			$aSecondValues['default_invoice']='0';
			if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts_address", $aSecondValues," user_id = ".$iUId) === false) {
				return false;
			}
		}
		if($bIsDefaultTransport) {
			//jezeli ustawiamy adres jako domyslny musimy zresetowac to pole u wszystkich - transport
			$aSecondValues['default_transport']='0';
			if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts_address", $aSecondValues," user_id = ".$iUId) === false) {
				return false;
			}
		}
    
		if ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."users_accounts_address", $aValues) === false) {
			return false;
		}
    
		return true;
	} // end of addUserAddress() method
  
	
	/**
	 * Dodaje dane użytkownika do tabeli adresów
	 * @param int $iUId - id użytkownika
	 * @param int $iFormAddresId - nr formularza
	 * @param bool $bIsDefaultInvoice - czy domyslny dla faktury
	 * @param bool $bIsDefaultTransport - czy domyslny dla transportu
	 * @return bool - success
	 */
	function addUserAddress($iUId, $iFormAddresId, $bIsDefaultInvoice=false, $bIsDefaultTransport=false){
		global $aConfig, $pDbMgr;
		
		if (empty($_POST) || filterInput($_POST['address_name_'.$iFormAddresId]) == '') {
			$sFrom = $aConfig['_settings']['site']['name'];
			$sFromMail = $aConfig['_settings']['site']['users_email'];
			//Common::sendMail($sFrom, $sFromMail, 'golba@omnia.pl', 'PUSTA TABLICA POST LUB adress_name pusty ', print_r($_POST, true).print_r($_SERVER["HTTP_USER_AGENT"], true));
			Common::RollbackTransaction();
			return false;
		}
		if($_POST['is_company_'.$iFormAddresId] == "1") {
			$aValues = array(
			  'user_id' => $iUId,
			  'address_name' => filterInput($_POST['address_name_'.$iFormAddresId]),
			  'is_company' => '1',
			  'company' => filterInput($_POST['corp_company_'.$iFormAddresId]),
			  //'name' => isset($_POST['corp_name_'.$iFormAddresId]) && $_POST['corp_name_'.$iFormAddresId] != '' ? filterInput($_POST['corp_name_'.$iFormAddresId]) : '',
			  //'surname' => filterInput($_POST['corp_surname_'.$iFormAddresId]),
			  'street' => filterInput($_POST['corp_street_'.$iFormAddresId]),
			  'number' => filterInput($_POST['corp_number_'.$iFormAddresId]),
			  'number2' => filterInput($_POST['corp_number2_'.$iFormAddresId]),
			  'postal' => filterInput($_POST['corp_postal_'.$iFormAddresId]),
			  'city' => filterInput($_POST['corp_city_'.$iFormAddresId]),
			  'nip' => filterInput($_POST['corp_nip_'.$iFormAddresId]),
			  //'phone' => filterInput($_POST['corp_phone_'.$iFormAddresId]),
			  'is_invoice' => $_POST['default_invoice_'.$iFormAddresId] == '1'?'1':'0',
			  'is_transport' => $_POST['default_transport_'.$iFormAddresId] == '1'?'1':'0',
			  'default_invoice' => $_POST['default_invoice_'.$iFormAddresId] == '1'?'1':'0',
			  'default_transport' => $_POST['default_transport_'.$iFormAddresId] == '1'?'1':'0',
			  'created' => 'NOW()'
			);
		} else {
			$aValues = array(
			  'user_id' => $iUId,
			  'address_name' => filterInput($_POST['address_name_'.$iFormAddresId]),
			  'is_company' => '0',
			  'name' => filterInput($_POST['priv_name_'.$iFormAddresId]),
			  'surname' => filterInput($_POST['priv_surname_'.$iFormAddresId]),
			  'street' => filterInput($_POST['priv_street_'.$iFormAddresId]),
			  'number' => filterInput($_POST['priv_number_'.$iFormAddresId]),
			  'number2' => filterInput($_POST['priv_number2_'.$iFormAddresId]),
			  'postal' => filterInput($_POST['priv_postal_'.$iFormAddresId]),
			  'city' => filterInput($_POST['priv_city_'.$iFormAddresId]),
			  //'phone' => filterInput($_POST['priv_phone_'.$iFormAddresId]),
//				'nip' => filterInput($_POST['priv_nip_'.$iFormAddresId]),
			  'is_invoice' => $_POST['default_invoice_'.$iFormAddresId] == '1'?'1':'0',
			  'is_transport' => $_POST['default_transport_'.$iFormAddresId] == '1'?'1':'0',
			  'default_invoice' => $_POST['default_invoice_'.$iFormAddresId] == '1'?'1':'0',
			  'default_transport' => $_POST['default_transport_'.$iFormAddresId] == '1'?'1':'0',
			  'created' => 'NOW()'
			);
		}
    
		if($_POST['default_invoice_'.$iFormAddresId] == '1') {
			//jezeli ustawiamy adres jako domyslny musimy zresetowac to pole u wszystkich - faktura
			$aSecondValues['default_invoice']='0';
			if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts_address", $aSecondValues," user_id = ".$iUId) === false) {
        Common::RollbackTransaction();
				return false;
			}
		}
		if($_POST['default_transport_'.$iFormAddresId] == '1') {
			//jezeli ustawiamy adres jako domyslny musimy zresetowac to pole u wszystkich - transport
			$aSecondValues['default_transport']='0';
			if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts_address", $aSecondValues," user_id = ".$iUId) === false) {
        Common::RollbackTransaction();
				return false;
			}
		}

		$additionalAddressValues = $this->determineAdditionalAddressValues(null, $aValues);
		$aValues = array_merge($aValues, $additionalAddressValues);
    
		if ($pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."users_accounts_address", $aValues) === false) {
			return false;
		}
		// zapis kategorii newslettera
		if (Common::moduleExists('m_newsletter') && !empty($_POST['n_categories'])) {
			// dolaczenie klasy Newsletter
			include_once('modules/m_newsletter/client/Newsletter.class.php');
			$oNewsletter = new Newsletter('m_newsletter', 0, array(), '');
			if($oNewsletter->add($_SESSION['w_user']['email'], $_POST['n_categories'], true) == -1){
				// nie udalo sie dodac newslettera
				Common::RollbackTransaction();
				return 0;
			}
		}
		return true;
	} // end of addUserAddress() method

	/**
	 * Aktualizuje dane użytkownika w tabeli adresów
	 * @param int $iUId - id użytkownika
	 * @param int $iAddressId - id adresu
	 * @param int $iFormAddresId - nr formularza
	 * @return bool - success
	 */
	function updateUserAddress($iUId, $iAddressId, $iFormAddresId){
		global $aConfig, $pDbMgr;
		
		if($_POST['is_company_'.$iFormAddresId] == "1"){
			$aValues = array(
			  'address_name' => filterInput($_POST['address_name_'.$iFormAddresId]),
			  'is_company' => '1',
			  'company' => filterInput($_POST['corp_company_'.$iFormAddresId]),
			  //'name' => filterInput($_POST['corp_name_'.$iFormAddresId]),
			  //'surname' => filterInput($_POST['corp_surname_'.$iFormAddresId]),
			  'street' => filterInput($_POST['corp_street_'.$iFormAddresId]),
			  'number' => filterInput($_POST['corp_number_'.$iFormAddresId]),
			  'number2' => filterInput($_POST['corp_number2_'.$iFormAddresId]),
			  'postal' => filterInput($_POST['corp_postal_'.$iFormAddresId]),
			  'city' => filterInput($_POST['corp_city_'.$iFormAddresId]),
			  'nip' => filterInput($_POST['corp_nip_'.$iFormAddresId]),
			  //'phone' => filterInput($_POST['corp_phone_'.$iFormAddresId]),
			  'default_invoice' => ($_POST['default_invoice_'.$iFormAddresId])?'1':'0',
			  'default_transport' => ($_POST['default_transport_'.$iFormAddresId])?'1':'0',
			  'modified' => 'NOW()'
			);
		} else {
			$aValues = array(
			  'address_name' => filterInput($_POST['address_name_'.$iFormAddresId]),
			  'is_company' => '0',
			  'name' => filterInput($_POST['priv_name_'.$iFormAddresId]),
			  'surname' => filterInput($_POST['priv_surname_'.$iFormAddresId]),
			  'street' => filterInput($_POST['priv_street_'.$iFormAddresId]),
			  'number' => filterInput($_POST['priv_number_'.$iFormAddresId]),
			  'number2' => filterInput($_POST['priv_number2_'.$iFormAddresId]),
			  'postal' => filterInput($_POST['priv_postal_'.$iFormAddresId]),
			  'city' => filterInput($_POST['priv_city_'.$iFormAddresId]),
//				'nip' => filterInput($_POST['priv_nip_'.$iFormAddresId]),
			  //'phone' => filterInput($_POST['priv_phone_'.$iFormAddresId]),
			  'default_invoice' => ($_POST['default_invoice_'.$iFormAddresId])?'1':'0',
			  'default_transport' => ($_POST['default_transport_'.$iFormAddresId])?'1':'0',
			  'modified' => 'NOW()'
			);
		}
		if($_POST['default_invoice_'.$iFormAddresId]) {
			//jezeli ustawiamy adres jako domyslny musimy zresetowac to pole u wszystkich - faktura
			$aSecondValues['default_invoice']='0';
			if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts_address", $aSecondValues," user_id = ".$iUId) === false) {
				return false;
				}
			}
		if($_POST['default_transport_'.$iFormAddresId]) {
			//jezeli ustawiamy adres jako domyslny musimy zresetowac to pole u wszystkich - transport
			$aSecondValues['default_transport']='0';
			if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts_address", $aSecondValues," user_id = ".$iUId) === false) {
				return false;
				}
			}

		$additionalAddressValues = $this->determineAdditionalAddressValues($iAddressId, $aValues);
		$aValues = array_merge($aValues, $additionalAddressValues);

		if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts_address", $aValues," user_id = ".$iUId." AND id = ".$iAddressId) === false) {
			return false;
		}
		return true;
	} // end of updateUserAddress() method

	/**
	 * @param null $iAddressId
	 * @param array $newValues
	 * @return array
	 */
	private function determineAdditionalAddressValues($iAddressId = null, array $newValues)
	{
		global $pDbMgr;

		$result = [];
		$iAddressId = mysql_real_escape_string($iAddressId);
		$md5 = null;
		$newMd5 = md5(json_encode($newValues));

		if (null != $iAddressId) {
			$md5 = $pDbMgr->GetOne('profit24', "SELECT address_checksum FROM users_accounts_address WHERE id = $iAddressId");
		}

		if ($newMd5 != $md5) {
			$result['address_checksum'] = $newMd5;
			$result['streamsoft_id'] = "NULL";
		}

		return $result;
	}

	/**
	 * Metoda aktualizuje główne dane uzytkownika
	 *
	 * @return	bool
	 */
	function updateMainUserData() {
		global $aConfig, $pDbMgr;
		$sAvatar = '';
		
			$aValues = array(
					'name' => ($_POST['name'] = trim($_POST['name'])) != '' ? filterInput($_POST['name']) : 'NULL',
					'surname' => ($_POST['surname'] = trim($_POST['surname'])) != '' ? filterInput($_POST['surname']) : 'NULL',
					'phone' => ($_POST['phone'] = trim($_POST['phone'])) != '' ? filterInput($_POST['phone']) : 'NULL',
					'modified' => 'NOW()'
			);

			if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts",
																	$aValues,
																	"id = ".$this->aUser['id']." AND website_id=".$aConfig['profit24_website_id']) === false) {	
				return false;
			}
//			if(isset($_SESSION['wu_cart']['user_data'])){
//				unset($_SESSION['wu_cart']['user_data']);
//			}
			
		return true;
	} // end of updateMainUserData() method
	
	
	/**
	 * Metoda aktualizuje ustawnienia konta
	 *
	 * @global array $aConfig
	 * @return bool
	 */
	function updateAccountSettings() {
		global $aConfig, $pDbMgr;

		$aValues = array(
				'promotions_agreement' => $_POST['promotions_agreement']=='1'?'1':'0',
		);
		
		if ($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts",
																$aValues,
																"id = ".$this->aUser['id']." AND website_id=".$aConfig['profit24_website_id']) === false) {	
			return false;
		}
		return true;
	}// end of updateAccountSettings() method
	
	
	/**
	 * Metoda zmienia haslo uzytkownika
	 *
	 * @return	integer	- -1: bledne aktualne haslo
	 * 										 0: wystapil blad
	 * 									  >0: dane zostaly zaktualizowane
	 */
	function changePassword() {
		global $aConfig, $pDbMgr;
			// sprawdzenie aktualnego hasla uzytkownika
		if ($_POST['old_passwd'] == '' || $this->passwordIsCorrect($_POST['old_passwd'], $this->aUser['id'])) {
			// zmiana hasla uzytkownika
      $sSalt = $this->getRandomString(255);
			$aValues = array(
				'passwd' => encodePasswd($_POST['n_passwd'], $sSalt),
        'salt' => $sSalt
			);
			return (int) $pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts",
																	$aValues,
																	"id = ".$this->aUser['id']." AND website_id=".$aConfig['profit24_website_id']);
		}
		// bledne aktualne haslo
		return -1;
		

	} // end of changePassword() method
  
  
  /**
   * Metoda usuwa zgodę na otrzymanie informacji o promocji
   * 
   * @global type $pDb
   * @global type $pDbMgr
   * @global array $aConfig
   * @param type $sToken
   * @return boolean
   */
  public function deletePromotionAgreement($sEmail) {
    global $pDbMgr, $aConfig;
    
    $sWhereSql = " email = ".Common::Quote($sEmail).' 
                   AND website_id = '.$aConfig['profit24_website_id'].'
                   LIMIT 1';
    $aValues = array(
        'promotions_agreement' => '0'
    );
    if ($pDbMgr->Update('profit24', 'users_accounts', $aValues, $sWhereSql) === false) {
      return false;
    }
    return true;
  }// end of deletePromotionAgreement() method
  
	
	/**
	 * Metoda zmienia email uzytkownika
	 *
	 * @return	integer	- -2: email istnieeje
	 * 										 0: wystapil blad
	 * 									  >0: dane zostaly zaktualizowane
	 */
	function changeEmail() {
		global $aConfig, $pDbMgr;
			// sprawdzenie czy eail juz nie występuje w bazie
		if (!$this->emailExists($_POST['email'],$this->aUser['id'])) {
			// zmiana emaila uzytkownika
			$aValues = array(
				'email' => filterInput(strtolower($_POST['email']))
			);
			return (int) $pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts",
																	$aValues,
																	"id = ".$this->aUser['id']." AND website_id=".$aConfig['profit24_website_id']);
		}
		// bledne aktualne haslo
		return -2;
		

	} // end of changePassword() method

		/**
	 * Metoda sprawdza czy podane aktualne haslo uzytkownika jest poprawne
	 *
	 * @param	string	$sLogin	- login
	 * @param	string	$sPasswd	- haslo
	 * @param	integer	$iUId	- Id uzytkownika
	 * @return	bool	- true: poprawne; false: niepoprawne
	 */
	function passwordIsCorrect($sPasswd, $iUId) {
		global $aConfig, $pDbMgr;
    
    $sSalt = $this->_getSalt($iUId);
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."users_accounts
						 WHERE id = ".$iUId." AND
						 			  passwd = '".encodePasswd($sPasswd, $sSalt)."'
										AND website_id=".$aConfig['profit24_website_id']."
						 			  AND deleted='0'";
		return is_numeric($pDbMgr->GetOne('profit24', $sSql));
	} // end of passwordIsCorrect() method
  
  
  /**
   * Metoda pobiera sól dla zadanego użytkownika
   * 
   * @global array $aConfig
   * @global object $pDbMgr
   * @param int $iUId
   * @return string
   */
  private function _getSalt($iUId) {
    global $aConfig, $pDbMgr;
    
    $sSql = 'SELECT salt 
             FROM users_accounts 
             WHERE id = '.$iUId.'
                   AND website_id = '.$aConfig['profit24_website_id'].'
             LIMIT 1';
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of _getSalt() method


	/**
	 * Metoda sprawdza czy uzytkownik o podanym adresie email istnieje
	 *
	 * @param	string	$sEmail	- sprawdzany adres email
	 * @param	integer	- Id uzytkownika dla ktorego ma zostac pominiete sprawdzanie
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function emailExists($sEmail, $iId=0) {
		global $aConfig, $pDbMgr;

		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."users_accounts
						 WHERE 
                deleted = '0' AND 
                registred = '1' AND
                email = ".Common::Quote(strtolower($sEmail)).
									" AND website_id=".$aConfig['profit24_website_id'].
						 			 ($iId > 0 ? " AND id <> ".$iId : '');
		return is_numeric($pDbMgr->GetOne('profit24', $sSql));
	} // end of emailExists() method
  
  
  /**
   * Metoda sprawdza czy konto ma zostać zaktualizowane czy utworzone
   * 
   * @global array $aConfig
   * @global object $pDbMgr
   * @param string $sEmail
   * @return int
   */
  private function checkUpdateEmailAccount($sEmail) {
    global $aConfig, $pDbMgr;
    
    $sSql = "SELECT id 
             FROM users_accounts 
             WHERE 
              email = ".Common::Quote(strtolower($sEmail))." AND
              website_id = ".$aConfig['profit24_website_id']." AND
              registred = '0' AND
              deleted = '0'
             ";
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of checkUpdateEmailAccount() method


	/**
	 * Metoda wysyla maila potwierdzajacego po zalozeniu nowego
	 * konta uzytkownika
	 *
	 * @return	bool	- true: mail wyslany; false: mail nie wyslany
	 */
	/*function sendRegistrationMail($sEmail, $sPasswd, $sName, $sSurname, $sLoginPageLink) {
		global $aConfig, $pSmarty;
		$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		$sFrom = $aConfig['_settings']['site']['name'];

		$sTo = $sEmail;
															 
		$sSubject = sprintf($aLang['registration_email_subject'],
																	 $aConfig['_settings']['site']['name']);															 
		
		$sFromMail = $aConfig['_settings']['site']['users_email'];
		// mail informujacy o utworzeniu konta zawierajacy link aktywujacy konto
		//$aModule['logo_link'] = $aConfig['common']['base_url_http']."images/gfx/logo_mail.gif";
		$aModule['header'] = sprintf($aLang['registration_email_body_header'],$sName);
		$aModule['content'] = $aLang['registration_email_body_content'];
		$aModule['login_site'] = $aLang['registration_email_body_login_site'];
		$aModule['login_data'] = $aLang['registration_email_body_login_data'];
		$aModule['end'] = $aLang['registration_email_body_end'];
		$aModule['email_text'] = $aLang['email'];
		$aModule['email'] = $sEmail;
		$aModule['passwd'] = $sPasswd;
		//$aModule['name'] = $sName;
		//$aModule['surname'] = $sSurname;
		$aModule['passwd_text'] = $aLang['passwd'];
		$aModule['login_page_link'] = $aConfig['common']['client_base_url_http_no_slash'].$sLoginPageLink;
		$aModule['site_name'] = $aConfig['_settings']['site']['name'];

		$aModule['footer'] = $aConfig['_settings']['site']['mail_footer'];

    $pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch('modules/m_konta/_registrationMail.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
	//return Common::sendMail($sFrom, $sFromMail, $sTo, $sSubject, $sHtml, true);
		$aHtmlImages = array();
		if(!empty($aConfig['common']['email_logo_file'])){
			$sImgF = $aConfig['common']['client_base_path'].$aConfig['common']['email_logo_file'];
			$aImage = getimagesize($sImgF);
			$aHtmlImages[] = array($sImgF, $aImage['mime']);
		}
		return Common::sendTxtHtmlMail($sFrom, $sFromMail, $sTo, $sSubject, '', $sHtml, '', '', $aHtmlImages);
	
	} // end of sendActivationLink() method 
	*/
	function sendRegistrationMail($sEmail, $sPasswd, $sName, $sSurname) {
		global $aConfig, $pSmarty;
		$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		$sFrom = $aConfig['_settings']['site']['name'];

		$sTo = $sEmail;
															 
		$sFromMail = $aConfig['_settings']['site']['users_email'];

		$aMail = Common::getWebsiteMail('rejestracja');
		return Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,$sTo,  $aMail['content'],array('{imie}','{nazwisko}','{email}','{haslo}'),
												array($sName, $sSurname, $sEmail, $sPasswd,),array(), array(), 'profit24');
				
	} // end of sendActivationLink() method

  /**
   * Metoda wysyła mail z linkiem aktywacyjnym
   * 
   * @global array $aConfig
   * @param string $sEmail
   * @param string $sName
   * @param string $sSurname
   * @param string $sActivationLink
   * @return bool
   */
	function sendActivationMail($sEmail, $sName, $sSurname, $sActivationLink) {
		global $aConfig;

		$sFrom = $aConfig['_settings']['site']['name'];

		$sTo = $sEmail;
															 
		$sFromMail = $aConfig['_settings']['site']['users_email'];

		$aMail = Common::getWebsiteMail('aktywacja');
		return Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,$sTo,  $aMail['content'],array('{imie}','{nazwisko}','{email}', '{link_aktywacyjny}'),
												array($sName,$sSurname,$sEmail, $aConfig['common']['client_base_url_http_no_slash'].$sActivationLink),array(), array(), 'profit24');
				
	} // end of sendActivationMail() method


	/**
	 * Metoda wysyla maila z linkiem do zmiany hasla
	 *
	 * @return	bool	- true: mail wyslany; false: mail nie wyslany
	 */
	function sendChangePasswd($sChangePasswd) {
		global $aConfig, $pSmarty;
		$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		$sFrom = $aConfig['_settings']['site']['name'];

		$sTo = $this->aUser['email'];


		$sSubject = sprintf($aLang['change_passwd_email_subject'],
																	 $aConfig['_settings']['site']['name']);

		$sFromMail = $aConfig['_settings']['site']['users_email'];

		
		//$aModule['logo_link'] = $aConfig['common']['base_url_http']."images/gfx/logo_mail.gif";
		// mail informujacy o utworzeniu konta zawierajacy link aktywujacy konto
		$aModule['content'] = sprintf($aLang['change_passwd_email_body'],
    												 $aConfig['_settings']['site']['name'],
    												 $aConfig['common']['client_base_url_http_no_slash'].$sChangePasswd);
    	$aModule['activation_link'] = $aConfig['common']['client_base_url_http_no_slash'].$sChangePasswd;
    	$aModule['header'] = $aLang['activation_email2_body_header'];
    	$aModule['site_name'] = $aConfig['_settings']['site']['name'];
	    $aModule['footer'] = $aConfig['_settings']['site']['mail_footer'];    													 
    	$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch('modules/m_konta/_resendActivationLink.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		
		//return Common::sendMail($sFrom, $sFromMail, $sTo, $sSubject, $sHtml, true);
		$aHtmlImages = array();
		if(!empty($aConfig['common']['email_logo_file'])){
			$sImgF = $aConfig['common']['client_base_path'].$aConfig['common']['email_logo_file'];
			$aImage = getimagesize($sImgF);
			$aHtmlImages[] = array($sImgF, $aImage['mime']);
		}
		return Common::sendTxtHtmlMail($sFrom, $sFromMail, $sTo, $sSubject, '', $sHtml, '', '', $aHtmlImages);
	
	} // end of sendChangePasswdLink() method
	

	/**
	 * Metoda zwraca formularz umowliwiajacy podanie adresu e-mail
	 * w celu wyslania do uzytkownika jego hasla
	 *
	 * @return	array	- tablica z zawartoscia formularza
	 */
	function &getRemindPasswdForm() {
	 	global $aConfig;
	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];
	 	$aForm = array();

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('loginForm',
													 '',
													 array('action' => $this->sPageLink),
													 array(),
													 false);

	 	$aForm['fields']['email']['label'] = $oForm->GetLabelHTML('passwd-change-email', $aLang['email'], true);
		$aForm['fields']['email']['input'] = $oForm->GetTextHTML('passwd-change-email', $aLang['email'], '', array('class' => 'text', 'maxlength' => 64), '', 'email');

		$aForm['fields']['do_send']['input'] = $oForm->GetInputButtonHTML('send', $aLang['sendButton']);

		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();
		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'lF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'lF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'lF__Validator'));
	 	return $aForm;
	} // end of getRemindPasswdForm() method
	
	/**
	 * Metoda zwraca formularz  edycji głównych danychuzytkownika
	 *
	 * @return	array	- tablica z zawartoscia formularza
	 */
	function &getEditMainDataForm() {
	 	global $aConfig;
	 	$aForm = array();
	 	$aData = array();

	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		if (isLoggedIn()) {
	 		// uzytkownik jest zalogowany - edycja jego danych
	 		$aData =& $this->aUser;
	 	}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('editMainDataForm',
													 '',
													 array('action' => $this->sPageLink),
													 array(),
													 false);

		$aTextAttribs = array(
			'class' => 'text'
		);
		
 		$aForm['fields']['name']['label'] = $oForm->GetLabelHTML('name', $aLang['name'], true);
		$aForm['fields']['name']['input'] = $oForm->GetTextHTML('name', $aLang['name'], $aData['name'], array_merge($aTextAttribs, array('maxlength' => 32,'tabindex' => 1)), $aLang['name_err'], 'text', true);

		$aForm['fields']['surname']['label'] = $oForm->GetLabelHTML('surname', $aLang['surname'], true);
		$aForm['fields']['surname']['input'] = $oForm->GetTextHTML('surname', $aLang['surname'], $aData['surname'], array_merge($aTextAttribs, array('maxlength' => 32,'tabindex' => 2)), $aLang['surname_err'], 'text', true);
		
		$aForm['fields']['phone']['label'] = $oForm->GetLabelHTML('phone', $aLang['phone'], true);
		$aForm['fields']['phone']['input'] = $oForm->GetTextHTML('phone', $aLang['phone'], (!empty($aData['phone'])?$aData['phone']:'+48 '), array_merge($aTextAttribs, array('maxlength' => 16,'tabindex' => 7)), $aLang['phone_err'], 'phone', true);

	 	$aForm['fields']['send']['input'] = $oForm->GetInputButtonHTML('send', $aConfig['lang']['common']['do_register']);

		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();
		
		$aForm['JS'] = '
	<script type="text/javascript">
	// <![CDATA[
		$(document).ready(function(){
			$("#editMainDataForm input:enabled").blur(function() {
			 // performFieldAjaxValidation("editMainDataForm",this.name,$("#editMainDataForm :input").serialize());
			  '.$oForm->GetBlurFieldValidatorJS("editMainDataForm").'
			});
			$("#editMainDataForm").submit(function() {
				//performAjaxValidation("editMainDataForm",$("#editMainDataForm input").serialize());
			  if($("#editMainDataForm input:enabled").hasClass("validErr")){
			  	alert("'.$aLang['form_send_err'].'");
			 		return false;
			  } else 
			  	return true;
			});
		});
	// ]]>
</script>
		
		';
		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));

	 	return $aForm;
	} // end of getEditMainDataForm()
	
	function saveStep1Data(){
		global $aConfig;
		
		$_SESSION['registration']['name'] = filterInput($_POST['name']);
		$_SESSION['registration']['surname'] = filterInput($_POST['surname']);
		$_SESSION['registration']['email'] = filterInput(strtolower($_POST['email']));
		$_SESSION['registration']['confirm_email'] = filterInput(strtolower($_POST['confirm_email']));
		$_SESSION['registration']['phone'] = filterInput($_POST['phone']);
    $_SESSION['registration']['salt'] = $this->getRandomString(255);
		$_SESSION['registration']['passwd'] = encodePasswd($_POST['passwd'], $_SESSION['registration']['salt']);
		$_SESSION['registration']['orig_passwd'] = $_POST['passwd'];
		$_SESSION['registration']['ref_link'] = $_POST['ref'];
	}
	
	/**
	 * Metoda sprawdza czy istnieje konto uzytkownika o podanym adresie e-mail
	 * 
	 * @param	string	$sEmail	- adres e-mail
	 * @return	bool
	 */
	function userEmailExists($sEmail) {
		global $aConfig, $pDbMgr;
	 	
	 	$sSql = "SELECT id
             FROM ".$aConfig['tabls']['prefix']."users_accounts
             WHERE email = '".$sEmail."'
							AND website_id=".$aConfig['profit24_website_id']."
              AND registred = '1'
             AND deleted = '0'";
		return ((double) $pDbMgr->GetOne('profit24', $sSql)) > 0;
	} // end of userEmailExists()
	
  
  /**
	 * Metoda pobiera haslo uzytkownika o podanym adresie e-mail
	 * 
	 * @param	string	$sEmail	- adres e-mail
	 * @return	bool
	 */
	function getUserPassword($sEmail) {
		global $aConfig, $pDbMgr;
    
    $sPasswd = $this->_getRandomUserPasswd();
    $this->setNewPassword($sEmail, $sPasswd);
    return $sPasswd;
	} // end of getUserPassword()
  
	
  /**
   * Metoda pobiera losowe hasło
   * 
   * @return type
   */
  private function _getRandomUserPasswd() {
    $iLength = 12;
    
    return $this->getRandomString($iLength);
  }// end of _getRandomPasswd() method
  
  
  /**
   * Metoda generuje losowy łańcuch znaków
   * 
   * @param int $iLength  - długość łańcucha
   * @return string - łańcuch
   */
  public function getRandomString($iLength) {
    
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < $iLength; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass);
  }// end of getRandomString() method
  
  
	/**
	 * Metoda pobiera haslo uzytkownika o podanym adresie e-mail
	 * 
	 * @param	string	$sEmail	- adres e-mail
	 * @return	bool
	 */
	function getUserPassword_OLD($sEmail) {
		global $aConfig, $pDbMgr;
	 	
	 	$sSql = "SELECT passwd
             FROM ".$aConfig['tabls']['prefix']."users_accounts
             WHERE email = '".$sEmail."'
						 AND website_id=".$aConfig['profit24_website_id']."
             AND deleted='0'";
		return decodePasswd($pDbMgr->GetOne('profit24', $sSql));
	} // end of getUserPassword()
	
	function getAddressName($iUserId, $iAddressId) {
		global $aConfig, $pDbMgr;
	 	
	 	$sSql = "SELECT address_name
             FROM ".$aConfig['tabls']['prefix']."users_accounts_address
             WHERE user_id = ".$iUserId.
							" AND id = ".$iAddressId;
		return $pDbMgr->GetOne('profit24', $sSql);
	} // end of getAddressName()
	
	/**
	 * Usuwa adres z konta użytkownika
	 * @param int $iUserId - id użytkownika
	 * @param int $iAddressId - id adresu
	 * @return bool
	 */
	function deleteUserAddress($iUserId, $iAddressId){
		global $aConfig, $pDbMgr;
		$sSql="SELECT is_invoice, is_transport
						FROM ".$aConfig['tabls']['prefix']."users_accounts_address
							WHERE user_id = ".$iUserId.
							" AND id = ".$iAddressId;
		$aAddr = $pDbMgr->GetRow('profit24', $sSql);
		if($aAddr['is_invoice'] == '1' || $aAddr['is_transport'] == '1'){
			$sSql="UPDATE ".$aConfig['tabls']['prefix']."users_accounts_address
							SET ".($aAddr['is_invoice'] == '1'?" is_invoice='1'".($aAddr['is_transport'] == '1'?',':''):'').
							($aAddr['is_transport'] == '1'?" is_transport='1'":'')."
							WHERE user_id = ".$iUserId.
							" AND id <> ".$iAddressId."
							LIMIT 1";
			if($pDbMgr->Query('profit24', $sSql) === false){
				return false;
			}
		}
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."users_accounts_address
							WHERE user_id = ".$iUserId.
							" AND id = ".$iAddressId;
		return $pDbMgr->Query('profit24', $sSql);
	} // end of deleteUserAddress() method
	
	/**
	 * Sprawdza czy możliwe jest usuniecie adresu użytkownika
	 * @param $iUserId - id uzytkownika
	 * @return bool
	 */
	function canDeleteUserAddress($iUserId){
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT COUNT(1)
						FROM ".$aConfig['tabls']['prefix']."users_accounts_address
							WHERE user_id = ".$iUserId;
		return intval($pDbMgr->GetOne('profit24', $sSql)) > 1;
	} // end of canDeleteUserAddress() method
	
	/**
	 * Całkowicie usuwa konto użytkownika z serwisu
	 * @param $iUserId - id konta
	 * @return bool - success
	 */
	function deleteUserAccount($iUserId){
		global $aConfig, $pDbMgr;
		$bIsErr = false;

		$pDbMgr->BeginTransaction('profit24');
		$sSql = "SELECT email 
							FROM ".$aConfig['tabls']['prefix']."users_accounts
							WHERE id = ".$iUserId."
								AND website_id=".$aConfig['profit24_website_id'];
		$sEmail = $pDbMgr->GetOne('profit24', $sSql);
		
		$aValues = array(
			'passwd' => '',
			'phone' => '',
			'regulations_agreement' => '0',
      'promotions_agreement' => '0',
			'privacy_agreement' => '0',
			'active' => '0',
			'discount' => '0',
			'deleted' => '1'
		);
		if($pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."users_accounts",$aValues,'id = '.$iUserId." AND website_id=".$aConfig['profit24_website_id'])===false){
			$bIsErr = true;
		}
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."users_accounts_address
							WHERE user_id = ".$iUserId;
		if($pDbMgr->Query('profit24', $sSql) === false){
			$bIsErr = true;
		}
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."orders_cart
							WHERE user_id = ".$iUserId;
		if(Common::Query($sSql) === false){
			$bIsErr = true;
		}
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."orders_briefcase
							WHERE konto_id = ".$iUserId;
		if(Common::Query($sSql) === false){
			$bIsErr = true;
		}
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
							WHERE email = ".Common::Quote($sEmail);
		if(Common::Query($sSql) === false){
		
			$bIsErr = true;
		} 
		if($bIsErr){
			$pDbMgr->RollbackTransaction('profit24');
			return false;
		} else {
			$pDbMgr->CommitTransaction('profit24');
			return true;
		}
	} // end of deleteUserAccount() method
	
	/**
	 * Sprawdza czy mozliwe jest usunięcie konta użytkownika
	 * (brak zamówień w trakcie realizacji)
	 * @param $iUserId- id uzytkownika
	 * @return bool
	 */
	function canDeleteUserAccount($iUserId){
		global $aConfig, $pDbMgr;
		$sSql = "SELECT COUNT(1)
						FROM ".$aConfig['tabls']['prefix']."orders
							WHERE user_id = ".$iUserId."
							AND website_id=".$aConfig['profit24_website_id']."
							AND order_status <> '4'
							AND order_status <> '5'";
		return intval($pDbMgr->GetOne('profit24', $sSql)) == 0;
	} // end of canDeleteUserAccount() method
	
	/**
	 * Metoda wysyla maila z haslem uzytkownika
	 *
	 * @param	string	$sEmail	- e-mail uzytkownika
	 * @param	string	$sPasswd	- haslo uzytkownika
	 * @return	bool	- true: mail wyslany; false: mail nie wyslany
	 */
	function sendUserPasswordEmail($sEmail, $sPasswd) {
		global $aConfig, $pSmarty;

		$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		$sFrom = $aConfig['_settings']['site']['name'];

		$sTo = $sEmail;
															 
		$sFromMail = $aConfig['_settings']['site']['users_email'];

		$aMail = Common::getWebsiteMail('przypomnienie_hasla');
		return Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,$sTo,  $aMail['content'],array('{email}','{haslo}'),
												array($sEmail,$sPasswd),array(), array(), 'profit24');
	} // end of sendUserPasswordEmail() method
	
	/**
	 * Pobiera listę adresów użytkownika
	 * @return array ref - lista adresów
	 */
	function &getAddresses($bCanDelete=false){
		global $aConfig, $pDbMgr;
		
		$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."users_accounts_address
							 WHERE user_id = ".$this->aUser['id']."
							 ORDER BY id ASC";
		$aAddresses =& $pDbMgr->GetAll('profit24', $sSql);
		$bWasInvoice=false;
		foreach($aAddresses as $iKey=>$aAddress){
			$aAddresses[$iKey]['edit_link'] = createLink($this->sPageLink,'id'.$aAddress['id'].',edit_addresses');
			$aAddresses[$iKey]['default_status']=$aAddress['default_invoice']*2+$aAddress['default_transport'];
			if($aAddress['default_invoice']>0) $bWasInvoice=true;
			if(count($aAddresses)==1) $aAddresses[$iKey]['default_status']=-1;	//potrzebne przy wyswietlaniu listy adresow
			 
			//if($aAddress['is_invoice'] == '0' && $aAddress['is_transport'] == '0'){
			if($bCanDelete){
				$aAddresses[$iKey]['delete_link'] = createLink($this->sPageLink,'id'.$aAddress['id'].',delete_address');
			}
		}
		if(!$bWasInvoice && isset($aAddresses[0])) $aAddresses[0]['default_status']+=2;
		return $aAddresses;
	} // end of getAddresses() method
	
	/**
	 * Dodaje zamówienia złożone bez rejestracji do konta
	 * @param $sEmail - adres email
	 * @param $iAccountId - id konta
	 * @return bool success
	 */
	function addOldOrdersToAccount($sEmail, $iAccountId){
		global $aConfig, $pDbMgr;
		
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders
						SET user_id = ".$iAccountId."
						WHERE user_id IS NULL
						AND website_id=".$aConfig['profit24_website_id']."
						AND email = ".Common::Quote($sEmail);
		if($pDbMgr->Query('profit24', $sSql) === false){
			return false;
		}
		return true;
	} // end of addOldOrdersToAccount() method 
	
	/**
	 * Pobiera zbazy dane konkretnego adresu użytkownika i przekształca na
	 *  formę strawną dla formularza edycji
	 * @param int $iAddressId - id adresu
	 * @return array ref - dane dla formularza
	 */
	function &getAddressDataForForm($iAddressId){
		global $aConfig, $pDbMgr;
			$aItem = array();
			$sSql = "SELECT *
							 FROM ".$aConfig['tabls']['prefix']."users_accounts_address
							 WHERE user_id = ".$this->aUser['id']."
							 AND id = ".$iAddressId;
			$aAddress = $pDbMgr->GetRow('profit24', $sSql);
			$aItem['is_company_0'] = $aAddress['is_company'];
			$aItem['address_name_0'] = $aAddress['address_name'];
			$aItem['default_invoice_0'] = $aAddress['default_invoice'];
			$aItem['default_transport_0'] = $aAddress['default_transport'];
			if($aAddress['is_company'] == '1'){
				$aItem['corp_company_0'] = $aAddress['company'];
				$aItem['corp_nip_0'] = $aAddress['nip'];
				$aItem['corp_name_0'] = $aAddress['name'];
				$aItem['corp_surname_0'] = $aAddress['surname'];
				$aItem['corp_street_0'] = $aAddress['street'];
				$aItem['corp_number_0'] = $aAddress['number'];
				$aItem['corp_number2_0'] = $aAddress['number2'];
				$aItem['corp_postal_0'] = $aAddress['postal'];
				$aItem['corp_city_0'] = $aAddress['city'];
				$aItem['corp_phone_0'] = $aAddress['phone'];
			} else {
				$aItem['priv_name_0'] = $aAddress['name'];
				$aItem['priv_surname_0'] = $aAddress['surname'];
				$aItem['priv_street_0'] = $aAddress['street'];
				$aItem['priv_nip_0'] = $aAddress['nip'];
				$aItem['priv_number_0'] = $aAddress['number'];
				$aItem['priv_number2_0'] = $aAddress['number2'];
				$aItem['priv_postal_0'] = $aAddress['postal'];
				$aItem['priv_city_0'] = $aAddress['city'];
				$aItem['priv_phone_0'] = $aAddress['phone'];
			}
			return $aItem;
	} // end of getAddressDataForForm() method
	
	/**
	 * Tworzy formularz edycji danych użytkownika
	 * @param int $iAddressId - id adresu
	 * @return array ref - formularz
	 */
	function &getAddressEditForm($iAddressId=0, $bEditOnly=false){
		global $aConfig;
		$aForm = array();
	 	$aData = array();

	 	$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		if (isLoggedIn()) {
	 		// uzytkownik jest zalogowany - edycja jego danych
	 		//$aData =& $this->aUser;
	 		if($iAddressId>0){
	 			$aData = $this->getAddressDataForForm($iAddressId);
	 		} else {
	 		/*	$aData['priv_name_0'] = $this->aUser['name'];
				$aData['priv_surname_0'] = $this->aUser['surname'];
				$aData['priv_phone_0'] = $this->aUser['phone'];
				$aData['corp_name_1'] = $this->aUser['name'];
				$aData['corp_surname_1'] = $this->aUser['surname'];
				$aData['corp_phone_1'] = $this->aUser['phone'];*/
	 		}
	 	}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('editAddress',
													 '',
													 array('action' => ($iAddressId>0?createLink($this->sPageLink,'id'.$iAddressId.',edit_addresses'):
													 																	createLink($this->sPageLink,'add_address'))),
													 array(),
													 false);
													 
		$this->getUserDataForm(0,$aForm,$oForm,$aData, true, '','', $bEditOnly);

		
	 	$aForm['fields']['send']['input'] = $oForm->GetInputButtonHTML('send', $aConfig['lang']['common']['do_register']);

		$aForm['header'] = $oForm->GetFormHeader();
		$aForm['footer'] = $oForm->GetFormFooter();
		$aOmmit = array('priv_city_0','corp_city_0');
		$aForm['JS'] = '
	<script type="text/javascript">
	// <![CDATA[
	function toggleAddresType(iAddrNr,iType){
			if(iType == 1){
				$("#addr"+iAddrNr+"_grp .corp").removeAttr("disabled");
				$("#addr"+iAddrNr+"_grp .priv").attr("disabled","disabled");
				$("#addr"+iAddrNr+"_grp .priv_grp").hide();
				$("#addr"+iAddrNr+"_grp .corp_grp").show();
			} else {
				$("#addr"+iAddrNr+"_grp .priv").removeAttr("disabled");
				$("#addr"+iAddrNr+"_grp .corp").attr("disabled","disabled");
				$("#addr"+iAddrNr+"_grp .corp_grp").hide();
				$("#addr"+iAddrNr+"_grp .priv_grp").show();
			}
		}
		$(document).ready(function(){
			$("#editAddress input").blur(function() {
				'.
		(!empty($aOmmit)?$this->getAjaxCondition($aOmmit).'
			performFieldAjaxValidation("editAddress",this.name,$("#editAddress :input").serialize());
		} else {
		':'').$oForm->GetBlurFieldValidatorJS("editAddress",$aOmmit).(!empty($aOmmit)?"\n} \n":'').'
			});
			$("#editAddress").submit(function() {
			  if($("#editAddress input:enabled").hasClass("validErr")){
			  	alert("'.$aLang['form_send_err'].'");
			 		return false;
			  } else 
			  	return true;
			});
      if ($("#priv_postal_0").length > 0){
        $("#priv_postal_0").autocomplete({ source : "/internals/GetPostals.php", minLength: 2});
        
      }
      if ($("#corp_postal_0").length > 0){
        $("#corp_postal_0").autocomplete({ source : "/internals/GetPostals.php", minLength: 2});
      }
      if ($("#priv_city_0").length > 0){
        $("#priv_city_0").autocomplete({ 
          source: function( request, response ) {
          $.ajax({
              url: "/internals/GetPostalCities.php",
              dataType: "json",
              data: {
                postal: $("#priv_postal_0").val(),
                term: request.term
              },
              success: function( data ) {
                response( data );
              }
            });
          },
          minLength: 2
        });
      }
      
      if ($("#corp_city_0").length > 0){
        $("#corp_city_0").autocomplete({ 
          source: function( request, response ) {
          $.ajax({
              url: "/internals/GetPostalCities.php",
              dataType: "json",
              data: {
                postal: $("#corp_postal_0").val(),
                term: request.term
              },
              success: function( data ) {
                response( data );
              }
            });
          },
          minLength: 2
        });
      }
		});
	// ]]>
</script>
		';
		$aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));
		$aForm['ajaxval'] = $oForm->GetHiddenHTML('__ajaxValidator', 'editAddress',
																								 array('id' => 'uaF__ajaxValidator'));

	 	return $aForm;
	}// end of getAddressEditForm() method
} // end of class User
?>