<?php
/**
 * Klasa obslugi sprawdzenia, statusu zamówienia dla osoby niezalogowanej,
 * jeśli użytkownik zalogowany, przeskakuje do kont użytkownika i zakładki zamówień
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-05-24 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Module { // Module_sprawdzanie
    
  // Id wersji jezykowej
  private  $iLangId;
  
  // symbol wersji jezykowej
  private  $sLang;
  
  // Id strony
  private  $iPageId;
  
  // szablon strony
  private  $sTemplate;
  
  // symbol modulu
  private  $sModule;
  
  // ustawienia strony aktualnosci
  private  $aSettings;
    
  // link do strony
  private  $sPageLink;
  
    // czesc nazwy pliku cache - na podstawie sciezki URI
//  private  $sCacheName;
  
  // czy cache'owac czy tez nie
//  private  $bDoCache;
	
  public function __construct() {
		global $aConfig;

    $aConfig['_tmp']['robots'] = 'noindex,nofollow';
    $this->sOption = 'sprawdzanie';
		$this->iPageId =& $aConfig['_tmp']['page']['id'];
		$this->sModule =& $aConfig['_tmp']['page']['module'];
		$this->sTemplate = 'modules/'.$this->sModule.'/'.$this->sOption.'/sprawdzanie.tpl';
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/';
		$this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
    
		$this->aSettings =& $this->getSettings();
  }
  
  /**
   * Domyślna metoda routera
   * 
   * @global array $aConfig
   * @global object $pSmarty
   * @return string - HTML
   */
  public function getContent() {
    global $aConfig;
    
    $sHtml = '';

    switch ($_GET['action']) {
      case 'details':
          $sSql = 'SELECT id FROM orders WHERE order_number = '.intval($_GET['id']);
          $iOId = Common::GetOne($sSql);

        if ($iOId > 0 && !empty($_SESSION['logged_out']['authorized_orders']) && in_array($iOId, $_SESSION['logged_out']['authorized_orders'])) {
          // mamy autoryzację i wchodzimy do zamówienia
          $aOrderData = $this->_getSelOrderDate($iOId, array('user_id'));

          // pobranie zamowien uzytkownika
          // dolaczenie klasy UserOrders
          include_once('modules/m_konta/client/UserOrders.class.php');
          $oUserOrders = new UserOrders($this->sPageLink,
                                        $aOrderData['user_id'],
                                        0,
                                        $this->sTemplatesPath);
          $oUserOrders->iUserId = $aOrderData['user_id'];
          $aDetails = $oUserOrders->getUserOrderDetails((int) $iOId);
          if(empty($aDetails)){
            setMessage(_('Wystąpił błąd podczas wyświetlania szczegółów zamówienia!'), true, true, 4000);
            doRedirectHttps($this->sPageLink);
          }
          
          if ($this->_checkOrderActivate($iOId) === false) {
            setMessage(getWebsiteMessage('zam_nowe_nieaktywne'), false, false, 9000);
          }

          $aDetails['id'] = $_GET['id'];
          if (isset($_SESSION['logged_out']['header_message'])) {
            $aDetails['header_message'] = $_SESSION['logged_out']['header_message'];
            unset($_SESSION['logged_out']['header_message']);
          }
          if (isset($_SESSION['logged_out']['js'])) {
            $aDetails['js'] = $_SESSION['logged_out']['js'];
            unset($_SESSION['logged_out']['js']);
          }
          $sHtml = $this->_showDetails($aDetails);
        } else {
          doRedirect('/');
        }
      break;
      case 'proforma':

          $sSql = 'SELECT id FROM orders WHERE order_number = '.intval($_GET['id']);
          $iOId = Common::GetOne($sSql);

        if ($iOId > 0 && !empty($_SESSION['logged_out']['authorized_orders']) && in_array($iOId, $_SESSION['logged_out']['authorized_orders'])) {
          $aOrderData = $this->_getSelOrderDate($iOId, array('user_id'));
          
          // pobranie zamowien uzytkownika
          // dolaczenie klasy UserOrders
          include_once('modules/m_konta/client/UserOrders.class.php');
          $oUserOrders = new UserOrders($this->sPageLink,
                                        $aOrderData['user_id'],
                                        0,
                                        $this->sTemplatesPath);
          if($oUserOrders->generateInvoice((int) $iOId) == false){

          }
          exit();
        } else {
          doRedirect('/');
        }
      break;
      case 'proforma-new':
          if (isset($_GET['id']))
          {
              $sSql = 'SELECT O.id FROM orders_merlin_invoices OMI
                                     LEFT JOIN orders O 
                                     ON O.order_number = OMI.order_number
                                     WHERE OMI.id = "' . intval($_GET['id']) . '" ';

              $oId = Common::GetOne($sSql);

              if (!empty($_SESSION['logged_out']['authorized_orders']) && in_array($oId, $_SESSION['logged_out']['authorized_orders']))
              {
                      $sSql = 'SELECT invoice_url FROM orders_merlin_invoices OMI
                                     LEFT JOIN orders O 
                                     ON O.order_number = OMI.order_number
                                     WHERE OMI.id = "' . intval($_GET['id']) . '" ';
                      $url = Common::GetOne($sSql);

                      if ($url !== NULL) {
                          $ch = curl_init($url);
                          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                          curl_setopt($ch, CURLOPT_HEADER, FALSE);

                          curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

                          curl_setopt($ch, CURLOPT_URL, $url);
                          curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
                          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                          curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
                          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                          $result = curl_exec($ch);
                          $info = curl_getinfo($ch);
                          curl_close($ch);

                          header("Content-type:application/pdf");
                          header("Content-Disposition:attachment;filename='Faktura.pdf'");
                          header('Content-Type: application/pdf');
                          header('Content-Length: ' . strlen($result));
                          header('Content-disposition: inline; filename="Faktura PDF"');
                          header('Cache-Control: public, must-revalidate, max-age=0');
                          header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                          echo $result;
                          exit();
                      }
                      else
                      {
                          doRedirect('/');
                      }
              }
          }
      break;
      case 'invoice-new':
          $sKey = $_GET['key'];
          $iOId = intval($_GET['id']);
          if ($sKey != '' && $iOId > 0) {
              $aOrderData = $this->_getAuthorizedSelOrderDate($iOId, $sKey, array(' * '));
              if (!empty($aOrderData)) {
                  // pobranie zamowien uzytkownika
                  // dolaczenie klasy UserOrders
                  $sSql = 'SELECT invoice_url FROM orders_merlin_invoices OMI
                                     LEFT JOIN orders O 
                                     ON O.order_number = OMI.order_number
                                     WHERE O.id = "' . intval($_GET['id']) . '" ';
                  $url = Common::GetOne($sSql);

                  if ($url !== NULL) {
                      $ch = curl_init($url);
                      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
                      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                      curl_setopt($ch, CURLOPT_HEADER, FALSE);

                      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

                      curl_setopt($ch, CURLOPT_URL, $url);
                      curl_setopt($ch, CURLOPT_TIMEOUT, 1000);
                      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0');
                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                      $result = curl_exec($ch);
                      $info = curl_getinfo($ch);
                      curl_close($ch);

                      header("Content-type:application/pdf");
                      header("Content-Disposition:attachment;filename='Faktura.pdf'");
                      header('Content-Type: application/pdf');
                      header('Content-Length: ' . strlen($result));
                      header('Content-disposition: inline; filename="Faktura PDF"');
                      header('Cache-Control: public, must-revalidate, max-age=0');
                      header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                      echo $result;
                      exit();
                  }
                  else
                  {
                      doRedirect('/');
                  }
              } else {
                  setMessage(_('Wystąpił błąd podczas autoryzacji !'), true);
              }
          } else {
              setMessage(_('Wystąpił błąd podczas autoryzacji !'), true);
              doRedirect('/');
          }
          break;
      case 'invoice':

       $sKey = $_GET['key'];
       $iOId = intval($_GET['id']);
        if ($sKey != '' && $iOId > 0) {
          $aOrderData = $this->_getAuthorizedSelOrderDate($iOId, $sKey, array(' * '));
          if (!empty($aOrderData)) {
            // pobranie zamowien uzytkownika
            // dolaczenie klasy UserOrders
            include_once('lib/Invoice.class.php');
            $oInvoice = new Invoice();
            $oInvoice->sTemplatePath = $aConfig['common']['client_base_path'].'omniaCMS/smarty/templates/';

            $oInvoice->getInvoicePDF($iOId, $aOrderData);
            exit();
          } else {
            setMessage(_('Wystąpił błąd podczas autoryzacji !'), true);
          }
        } else {
          setMessage(_('Wystąpił błąd podczas autoryzacji !'), true);
          doRedirect('/');
        }
      break;
      default:
        $aData = $this->_getGlobalData();
        if (!empty($aData) && is_array($aData)) {
          if ($this->_validateForm($aData) === false) {
            $aData = array();
            $sHtml = $this->_showDefault($aData);
          } else {
            // poszło, sprawdzamy, czy prawidłowe dane podał
            $aOrderDataCur = $this->_getOrderId($aData);
            $iOId = $aOrderDataCur['id'];
            $orderNumber = $aOrderDataCur['order_number'];
            if ($iOId > 0) {
              $_SESSION['logged_out']['authorized_orders'][] = $iOId;
              $_SESSION['logged_out']['email'] = $aData['email'];
//              $_SESSION['logged_out']['order_number'] = $aData['order_number'];
              $_SESSION['logged_out']['check_status_key'] = $aData['check_status_key'];
              doRedirectHttps(createLink($this->sPageLink, 'id'.$orderNumber, 'details' ));
            } else {
              setMessage(_('Wystąpił błąd podczas autoryzacji !'), true);
              $sHtml = $this->_showDefault($aData);
            }
          }
        } else {
          $sHtml = $this->_showDefault($aData);
        }
      break;
    }

    
    return $sHtml;
  }// end of getContent() method
  
  
  /**
   * Metoda pobiera wybrane kolumny dla zamówienia
   * 
   * @global object $pDbMgr
   * @param int $iOId
   * @param string $sKey
   * @param array $aSelCols
   * @return array
   */
  private function _getAuthorizedSelOrderDate($iOId, $sKey, $aSelCols = array('id')) {
    global $pDbMgr, $pDB;
    
    $sSql = "SELECT ".implode(', ', $aSelCols).' 
             FROM orders
             WHERE id = '.$iOId. ' AND MD5(CONCAT(check_status_key, order_number)) = '.$pDB->quoteSmart($sKey);
    return $pDbMgr->GetRow('profit24', $sSql);
  }// end of _getSelOrderDate() method
  
  
  /**
   * Metoda sprawdza czy zamówienie zostało aktywowane
   * 
   * @global type $pDbMgr
   * @param type $iOId
   * @return type
   */
  private function _checkOrderActivate($iOId) {
    global $pDbMgr;
    
    $sSql = "
      SELECT id 
      FROM orders 
      WHERE id = ".$iOId."
      AND active = '1'";
    return  $pDbMgr->GetOne('profit24', $sSql) > 0 ? TRUE : FALSE;
  }// end of _checkOrderActivate() method
  
  
  /**
   * Metoda pobiera wybrane kolumny dla zamówienia
   * 
   * @global object $pDbMgr
   * @param int $iOId
   * @param array $aSelCols
   * @return array
   */
  private function _getSelOrderDate($iOId, $aSelCols = array('id')) {
    global $pDbMgr;
    
    $sSql = "SELECT ".implode(', ', $aSelCols).' 
             FROM orders
             WHERE id = '.$iOId;
    return $pDbMgr->GetRow('profit24', $sSql);
  }// end of _getSelOrderDate() method
  
  
  
  /**
   * Metoda zwraca kod html podstawowego widoku
   * 
   * @global object $pSmarty
   * @param type $aData
   * @return type
   */
  private function _showDetails($aDetails) {
    global $pSmarty, $aConfig;
    
    include_once('modules/m_konta/lang/pl.php');
    
    $aModule['lang'] =& $aConfig['lang']['mod_m_konta']; //$aConfig['lang']['mod_'.$this->sModule];
		$aModule['template'] = $this->sTemplatesPath;
    $aModule['details'] = $aDetails;
            
    $pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch('modules/m_konta/_ordersDetails.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
    return $sHtml;
  }// end of showDefault() method
  
  
  /**
   * Metoda sprawdza przesłane dane
   * 
   * @global array $aConfig
   * @global object $pDbMgr
   * @global object $pDB
   * @param array $aData
   * @return type
   */
  private function _getOrderId($aData) {
    global $pDbMgr, $pDB;
    
    $sSql = "SELECT id, order_number
             FROM orders
             WHERE 
              (check_status_key = ".$pDB->quoteSmart($aData['check_status_key']).")
                 AND email = ".$pDB->quoteSmart($aData['email']);
    return $pDbMgr->GetRow('profit24', $sSql);
  }// end of _getOrderId() method
  
  
  /**
   * Metoda zwraca kod html podstawowego widoku
   * 
   * @global object $pSmarty
   * @param type $aData
   * @return type
   */
  private function _showDefault($aData) {
    global $pSmarty, $aConfig;
    
    $aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['page_link'] =& $this->sPageLink;
		$aModule['template'] = $this->sTemplatesPath;

    $aModule['form'] = $this->_showDefaultForm($aData);
    $pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplate);
		$pSmarty->clear_assign('aModule', $aModule);
    return $sHtml;
  }// end of showDefault() method
  
  
  /**
   * Metoda validuje formularz
   * 
   * @param array $aData
   * @return boolean
   */
  private function _validateForm($aData) {
    
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    if ($aData['check_status_key'] <= 0) {
      $oValidator->sError .= '<li>'._('Hasło zamówienia').'</li>';
    }
    if (!$oValidator->Validate($aData)) {
      setMessage($oValidator->GetErrorString(), true);
      return false;
    }
    return true;
  }// end of _validateForm() method
  
  
  /**
   * Metoda pobiera globalne dane
   * 
   * @return array
   */
  private function _getGlobalData() {
    
    $aData = array();
    if (isset($_POST) && !empty($_POST) && isset($_POST['check_status_key']) && $_POST['check_status_key'] > 0 ) {
			$aData =& $_POST;
		} elseif (isset($_GET) && !empty($_GET) && isset($_GET['check_status_key']) && $_GET['check_status_key'] > 0 && isset($_GET['email'])) {
			$aData =& $_GET;
		}
    return $aData;
  }// end of getGlobalData() method

  
  /**
   * Metoda generuje domyślny formularz weryfikacji danych zamówienia
   * 
   * @return array
   */
  private function _showDefaultForm($aData) {

    /*
    if (isset($aData['check_status_key'])) {
      $aData['order_number'] = $aData['check_status_key'];
    }
     */

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		$oForm = new FormTable('get_order',
													 '',
													 array('action' => $this->sPageLink, 'enctype' => 'multipart/form-data'),
													 array(),
													 false);
    $aForm['check_status_key'] = $oForm->GetTextHTML('check_status_key', _('Hasło'), $aData['check_status_key'], array('class' => 'RedText'), _('Nieprawidłowe hasło'), 'uint', true);
    $aForm['email'] = $oForm->GetTextHTML('email', _('Adres email'), $aData['email'], array('class' => 'RedText'), _('Nieprawidłowy adres email'), 'email', true);
    $aForm['validator'] = $this->_getValidation($oForm, 'get_order');
    return $aForm;
  }// end of _showDefaultForm() method
  
  
  /**
   * Metoda generuja pola validacji
   * 
   * @param type $oForm
   * @param type $aData
   */
  private function _getValidation(&$oForm, $sAjaxValidationName) {
    
    $aForm['header'] = $oForm->GetFormHeader();
    $aForm['footer'] = $oForm->GetFormFooter();
    
    $aForm['JS'] = $oForm->GetBlurFieldValidatorJS($sAjaxValidationName);
    
    $aForm['err_prefix'] = $oForm->GetHiddenHTML('__ErrPrefix',
																								 $oForm->GetErrorPrefix(),
																								 array('id' => 'uaF__ErrPrefix'));
		$aForm['err_postfix'] = $oForm->GetHiddenHTML('__ErrPostfix',
																								 $oForm->GetErrorPostfix(),
																								 array('id' => 'uaF__ErrPostfix'));
		$aForm['validator'] = $oForm->GetHiddenHTML('__Validator',
																								 $oForm->GetValidator(),
																								 array('id' => 'uaF__Validator'));
	  $aForm['ajaxval'] = $oForm->GetHiddenHTML('__ajaxValidator', $sAjaxValidationName,
																								 array('id' => 'uaF__ajaxValidator'));
    return $aForm;
  }// end of _getValidation() method
  
  
	/**
	 * Metoda pobiera konfiguracje dla strony
	 *
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig;

		return $aCfg;
	} // end of getSettings() function
}// end of Class
?>