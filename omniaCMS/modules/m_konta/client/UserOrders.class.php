<?php
/**
* Klasa Zamowien Uzytkownika serwisu
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version   1.0
*/

class UserOrders {
	
	// link do strony modulu konta uzytkownikow
	var $sPageLink;
	
	// Id uzytkownika
	var $iUserId;
	
	// liczba artykulow na stronie
  var $iPerPage;

  // symbol modulu
  var $sModule;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
	
	/**
  * Konstruktor klasy UserOrders
	*/
	function UserOrders($sPageLink, $iUserId, $iPerPage) {
		global $aConfig;
		
		$this->sPageLink = $sPageLink;
		$this->iUserId = $iUserId;
		$this->iPerPage = $iPerPage;
		$this->sModule =& $aConfig['_tmp']['page']['module'];
		$this->sTemplatesPath = 'modules/'.$this->sModule.'/';
	} // end User()
	
	
	/**
	 * Metoda pobiera zamowienia uzytkownika o Id $this->iUserId
	 * 
	 * @return	void
	 */
	function &getUserOrders() {
		global $aConfig;
		$aOrders = array();

		$userConfirmation = new UserConfirmation($_SESSION['w_user']['id']);
		$isUserConfirmed = $userConfirmation->isUserConfirmed();
		$userRegistrationDate = $userConfirmation->getWebsiteUser()['created'];

		// okreslenie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."orders
						 WHERE user_id = ".$this->iUserId;

		if (false == $isUserConfirmed) {
			$sSql .= " AND order_date >= '$userRegistrationDate'";
		}
						 
		if (($iRecords = (int) Common::GetOne($sSql)) > 0) {
			// pobranie listy zamowien uzytkownika		
			$sSql = "SELECT A.*, B.personal_reception
							 FROM ".$aConfig['tabls']['prefix']."orders A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
							 ON B.id=A.transport_id
							 WHERE A.user_id = ".$this->iUserId;
			if (false == $isUserConfirmed) {
				$sSql .= " AND order_date >= '$userRegistrationDate'";
			}
			 $sSql .= " ORDER BY A.id DESC";
							 
			if ($this->iPerPage > 0) {
				// uzycie pagera do stronicowania
				include_once('Pager/CPager.class.php');
				$aPagerParams = array();
				$aPagerParams['perPage'] = $this->iPerPage;
				$aPagerParams['path'] = $aConfig['common']['base_url_https_no_slash'].
																$this->sPageLink;
				$aPagerParams['fileName'] = 'orders,p%d.html';
				$aPagerParams['append'] = false;

				$oPager = new CPager($iRecords, $aPagerParams);
				$iStartFrom = $oPager->getStartFrom();

				// linki Pagera
				$aOrders['pager']['links'] =& $oPager->getLinks('all');
				$aOrders['pager']['total_pages'] =& $oPager->getLinks('total_pages');
				$aOrders['pager']['total_items'] =& $oPager->getLinks('total_items');
				$aOrders['pager']['current_page'] =& $oPager->getLinks('current_page');

				// dodanie wybrania zakresu do zapytania
				$sSql .= " LIMIT ".$iStartFrom.", ".$this->iPerPage;
			} 
			$aOrders['orders'] =& Common::GetAll($sSql);
			$iLp=$iStartFrom+1;
			
			foreach ($aOrders['orders'] as $iKey => $aItem) {
				$aOrders['orders'][$iKey]['lp'] = $iLp;
				$iLp++;
				$aOrders['orders'][$iKey]['price'] = Common::formatPrice($aItem['value_brutto'] + $aItem['transport_cost']);
				$aOrders['orders'][$iKey]['order_status_text'] = $this->getOrderStatus($aItem['order_status'],($aItem['second_payment_enabled'] && $aItem['second_payment_type'])?$aItem['second_payment_type']:$aItem['payment_type'],($aItem['second_payment_enabled'] && $aItem['second_payment_type'])?$aItem['second_payment_status']:$aItem['payment_status'],($aItem['personal_reception']=='1'), Common::formatPrice2($aItem['balance']));
				$aOrders['orders'][$iKey]['order_link'] = createLink($this->sPageLink,'id'.$aItem['order_number'].',orders');
				if(($aItem['payment_type'] == 'platnoscipl' || $aItem['payment_type'] == 'payu_bank_transfer' || $aItem['payment_type'] == 'card') && $aItem['payment_status'] != '1' && $aItem['order_status'] != '5'){
					$aOrders['orders'][$iKey]['order_pay_link'] = $aOrders['orders'][$iKey]['order_link'].'#platnosci';
				}
				if(($aItem['second_payment_type'] == 'platnoscipl' || $aItem['second_payment_type'] == 'payu_bank_transfer' || $aItem['second_payment_type'] == 'card') && $aItem['second_payment_status'] != '1' && $aItem['second_payment_enabled']=='1' && $aItem['order_status'] != '5'){
					$aOrders['orders'][$iKey]['order_pay2_link'] = $aOrders['orders'][$iKey]['order_link'].'#platnosci';
				}
				if($aOrders['orders'][$iKey]['order_pay_link'] == '' && ($aItem['payment_type'] == 'platnoscipl' || $aItem['payment_type'] == 'card' || $aItem['payment_type'] == 'payu_bank_transfer') && ($aItem['payment_status'] != '1' || $aItem['paid_amount'] < $aItem['to_pay']) && $aItem['order_status'] != '5'){
					$aOrders['orders'][$iKey]['order_pay2_link'] = $aOrders['orders'][$iKey]['order_link'].'#platnosci';
				}
				//$aOrders['orders'][$iKey]['details'] =& $this->getUserOrderDetails($aItem['id']);
			}
		}
		return $aOrders;
	} // end of getUserOrders() function
	

	/**
	 * Metoda zwraca szczegoly zamowienia o odpowiednim id
	 */
	function getUserOrderDetails($iId){
		global $aConfig, $pDbMgr;
		$aData=array();
		if($iId > 0) {

			$sSql = "SELECT A.*, (A.transport_cost + A.value_brutto) as total_value_brutto, 
							B.personal_reception, A.transport_cost AS transport_cost_ , B.symbol as transport_symbol
							FROM ".$aConfig['tabls']['prefix']."orders A
							LEFT JOIN ".$aConfig['tabls']['prefix']."orders_transport_means B
							ON B.id=A.transport_id
							WHERE A.id = ".$iId." AND A.user_id = ".$this->iUserId."";

			$aData =& Common::GetRow($sSql);
			if(!empty($aData)){
				$aData['price'] = Common::formatPrice($aData['value_brutto'] + $aData['transport_cost']);
				$aData['order_status_text'] = $this->getOrderStatus($aData['order_status'],($aData['second_payment_enabled'] && $aData['second_payment_type'])?$aData['second_payment_type']:$aData['payment_type'],($aData['second_payment_enabled'] && $aData['second_payment_type'])?$aData['second_payment_status']:$aData['payment_status'],($aData['personal_reception']=='1'), Common::formatPrice2($aData['balance']));
				$bEnabledSecondInvoice = ($aData['second_invoice'] == '1');
				
				$aData['total_value_brutto'] = Common::formatPrice($aData['total_value_brutto']);
				$aData['nopromo_value_brutto'] = Common::formatPrice($aData['nopromo_value_brutto']);
				$aData['paid_amount'] = Common::formatPrice2($aData['paid_amount']);
				$aData['balance'] = Common::formatPrice2($aData['balance']);
				$aData['transport_cost_'] = $aData['transport_cost'];
				$aData['transport_cost'] = Common::formatPrice($aData['transport_cost']);
				$aData['value_brutto'] = Common::formatPrice($aData['value_brutto']);
				$aData['value_brutto_invoice1'] = 0;
				$aData['value_brutto_invoice2'] = 0;
				if($aData['second_payment_enabled'] && $aData['second_payment_type']!='')
				$aData['payment'] .=', '.$aData['second_payment'] ;
				
				$aData['addresses'] = $this->getOrderAddresses($iId);
				//$aData['cost']['transport'] = $aConfig['lang']['mod_m_konta']['kurier'];
				
			}
		}
		if(!empty($aData)){
			$aData['items'] = $this->getInvoiceItems($iId,$bEnabledSecondInvoice,false);
		// dodanie kuriera do pozycji zamówienia
			if($aData['personal_reception'] == '0'){
				$aItem = array();
				$aItem['name'] = $aData['transport'];
				$aItem['price_brutto'] = Common::formatPrice($aData['transport_cost_']);
				$aItem['promo_price_brutto'] = 0;
				$aItem['nopromo_value_brutto'] = Common::formatPrice($aData['transport_cost_']);
				$aItem['value_brutto_'] = $aData['transport_cost_'];
				$aItem['value_brutto'] = Common::formatPrice($aData['transport_cost_']);
				$aItem['status_text'] = '';
				$aItem['quantity'] = 1;
				$aItem['deleted'] = '0';
				$aData['items'][] = $aItem;
			}
			if(!empty($aData['items'])){
				foreach($aData['items'] as $iKey=>$aItem){
					// jeśli elementy pakietu lub jeśli przesyłka
					if($aItem['deleted'] == '0' && ($aItem['packet']=='0' || !isset($aItem['packet'])) && ($aItem['item_type'] != 'A')){
						$aData['value_brutto_invoice1'] += $aItem['value_brutto_'];
					}
				}
			}
			if($aData['second_invoice'] == '1'){
				$aData['items2'] = $this->getInvoiceItems($iId,$bEnabledSecondInvoice,true);
				if(!empty($aData['items2'])){
					foreach($aData['items2'] as $iKey=>$aItem){
						// jeśli elementy pakietu lub jeśli przesyłka
						if($aItem['deleted'] == '0' && ($aItem['packet']=='0' || !isset($aItem['packet']))){
							$aData['value_brutto_invoice2'] += $aItem['value_brutto_'];
						}
					}
				}
			}
			$aData['value_brutto_invoice1'] = Common::formatPrice($aData['value_brutto_invoice1']);
			$aData['value_brutto_invoice2'] = Common::formatPrice($aData['value_brutto_invoice2']);
			if(($aData['payment_type'] == 'platnoscipl' || $aData['payment_type'] == 'payu_bank_transfer' || $aData['payment_type'] == 'card') && ($aItem['payment_status'] != '1' || $aItem['paid_amount'] < $aItem['to_pay']) && $aData['order_status'] != '5'){
				$aData['payment_form'] = $this->getPlatnosciForm($iId, Common::formatPrice2($aData['total_value_brutto']-$aData['paid_amount']), ($aData['payment_type'] == 'card'));
			}
		if(($aData['second_payment_type'] == 'platnoscipl' || $aData['payment_type'] == 'payu_bank_transfer' || $aData['second_payment_type'] == 'card') && $aData['second_payment_status'] != '1' && $aData['second_payment_enabled'] == '1' && $aData['order_status'] != '5'){
				$aData['payment_form'] = $this->getPlatnosciForm($iId, Common::formatPrice2(-$aData['balance']), ($aData['payment_type'] == 'card'));
			}

			$sSql = 'SELECT * FROM '.$aConfig['tabls']['prefix'].'orders_merlin_invoices
					 WHERE order_number = "'.$aData['order_number'] .'" ';

			$proformas = Common::GetAll($sSql);

			foreach($proformas as $id => $proform)
            {
                $proformas[$id]['link'] = createLink($this->sPageLink,'id'.$proform['id'].',proforma-new');
            }

			$aData['proformas'] = $proformas;

            if (!empty($aData['invoice_date']) && $aData['invoice_date'] < '2019-01-25 00:00:00' && empty($aData['proformas']) )
            {
                $aData['proforma_link'] = createLink($this->sPageLink,'id'.$aData['order_number'].',proforma');
            }
		}
		$aData['to_pay']=Common::formatPrice($aData['to_pay']-$aData['paid_amount']);
		
    switch ($aData['transport_symbol']) {
      case 'paczkomaty_24_7':
      case 'ruch':
      case 'orlen':
      case 'poczta_polska':
        include_once('LIB/autoloader.php');
        $oShipment = new \orders\Shipment($aData['transport_symbol'], $pDbMgr);
        $aData['point_of_receipt'] = $oShipment->getPopupDetails('PointsOfReceipt', $aData['point_of_receipt']);
      break;
    }
		return $aData;	
	}

	/**
	 * Metoda zwraca formularz dla platosci DotPay
	 * 
	 * @param	array ref	$aOrder	- dane zamowienia
	 * @return	string	- formularz platnosci DotPay
	 */
	function &getDotPayForm(&$aOrder) {
		global $aConfig, $pSmarty;
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];

		$sOrdersPageLink = getModulePageLink('m_zamowienia');


		$aModule['dotpay'] = array(
			'id'	=> getDotPayId(),
			'amount'	=> Common::formatPrice2($aOrder['total_value_brutto']),
			'currency'	=>	'PLN',
			'description'	=>	sprintf($aModule['lang']['order_number'], $aOrder['order_number'], $aOrder['email']),
			'lang'	=>	'pl',
			'URL'	=> $aConfig['common']['base_url_https_no_slash'].createLink($sOrdersPageLink, '', 'dotpay'),
			'URLC'	=> $aConfig['common']['base_url_https_no_slash'].createLink($sOrdersPageLink, '', 'dotpay_c'),
			'type'	=>	3,
			'control'	=> base64_encode($aOrder['id'].':'.md5($aOrder['email'])),
			'firstname'	=> $aOrder['name'],
			'lastname'	=> $aOrder['surname'],
			'email'	=>	$aOrder['email'],
			'street'	=>	$aOrder['street'],
			'street_n1'	=>	$aOrder['number'],
			'street_n2'	=>	$aOrder['number2'],
			'city'	=>	$aOrder['city'],
			'postcode'	=>	$aOrder['postal'],
			'country'	=> 'POL',
			'onlinetransfer'	=> '0',
			'podatek'	=> '1'
		);
		
		$aModule['dotpay_url'] = 'https://ssl.dotpay.pl/';
		
		$pSmarty->assign_by_ref('aModule', $aModule);
		$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/_dotpay_form.tpl');
		$pSmarty->clear_assign('aModule', $aModule);
		return $sHtml;
	} // end of getDotPayForm() method
	
	/**
	 * Pobiera opis stanu zamóweienia
	 * @param int $iStatus - status zamowienia
	 * @param string $sPayment - metoda platnosci
	 * @param int $iPaymentStatus - status platnosci
	 * @param bool $bPersonal - odbior osobisty
	 * @param	float	$fBalance	- slado dla zamowienia
	 * @return string - opis stanu
	 */
	function getOrderStatus($iStatus, $sPayment, $iPaymentStatus,$bPersonal,$fBalance) {
		global $aConfig;
		//echo$iStatus.' '.$sPayment.' '.$iPaymentStatus.' '.$bPersonal.' '.$fBalance.'<br />';
		switch($iStatus){
			// nowe
			case '0':
				switch($sPayment){
					case 'bank_transfer':
						switch($iPaymentStatus){
							case '0':
								return 'nowe – oczekuje na wpłatę';
							break;
							case '1':
								if ($fBalance < 0) {
									// niedoplata
									return 'nowe - opłacone - niedopłata';
								}
								else {
									// w pelni oplacone
									return 'nowe - opłacone';
								}
							break;
						}
					break;
					case 'dotpay':
					case 'platnoscipl':
					case 'card':
						switch($iPaymentStatus){
							case '0':
								return 'nowe – oczekuje na wpłatę';
							break;
							case '1':
								if ($fBalance < 0) {
									// niedoplata
									return 'nowe - opłacone - niedopłata';
								}
								else {
									// w pelni oplacone
									return 'nowe - opłacone';
								}
							break;
							case '2':
								return 'nowe – transakcja odmowna';
							break;
						}
					break;
					case 'bank_14days':
					case 'postal_fee':
						return 'nowe';
					break;
				}
			break;
			// w realizacji
			case '1':
				switch($sPayment){
					case 'postal_fee':
						switch($iPaymentStatus){
							case '0':
								return 'w realizacji';
							break;
						}
					break;
					case 'bank_transfer':
						switch($iPaymentStatus){
							case '0':
								return 'w realizacji – oczekuje na wpłatę';
							break;
							case '1':
								if ($fBalance < 0) {
									// niedoplata
									return 'w realizacji - opłacone - niedopłata';
								}
								else {
									// w pelni oplacone
									return 'w realizacji - opłacone';
								}
							break;
						}
					break;
					case 'dotpay':
					case 'platnoscipl':
					case 'card':
						switch($iPaymentStatus){
							case '0':
                  return 'w realizacji – oczekuje na wpłatę';
              break;
							case '1':	
								if ($fBalance < 0) {
									// niedoplata
									return 'w realizacji - opłacone - niedopłata';
								}
								else {
									// w pelni oplacone
									return 'w realizacji - opłacone';
								}
							break;
						}
					break;
					case 'bank_14days':
						switch($iPaymentStatus){
							case '0':
								return 'w realizacji';
							break;
							case '1':
								if ($fBalance < 0) {
									// niedoplata
									return 'w realizacji - opłacone - niedopłata';
								}
								else {
									// w pelni oplacone
									return 'w realizacji - opłacone';
								}
							break;
						}
					break;
				}
			break;
			// skompletowane
			case '2':
			case '3':
				switch($sPayment){
					case 'postal_fee':
						switch($iPaymentStatus){
							case '0':
								return 'skompletowane';
							break;
						}
					break;
					case 'bank_transfer':
						switch($iPaymentStatus){
							case '0':
								return 'skompletowane – oczekuje na wpłatę';
							break;
							case '1':
								if($bPersonal){
									return 'gotowe do odbioru';
								}
								else {
									if ($fBalance < 0) {
										// niedoplata
										return 'skompletowane - opłacone - niedopłata';
									}
									else {
										// wpelni oplacone
										return 'skompletowane - opłacone';
									}
								}
							break;
						}
					break;
					case 'dotpay':
					case 'platnoscipl':
					case 'card':
						switch($iPaymentStatus){
							case '1':
								if($bPersonal){
									return 'gotowe do odbioru';
								}
								else {
									if ($fBalance < 0) {
										// niedoplata
										return 'skompletowane - opłacone - niedopłata';
									}
									else {
										// wpelni oplacone
										return 'skompletowane - opłacone';
									}
								}
							break;
						}
					break;
					case 'bank_14days':
						switch($iPaymentStatus){
							case '0':
								return 'skompletowane';
							break;
							case '1':
								if ($fBalance < 0) {
									// niedoplata
									return 'skompletowane - opłacone - niedopłata';
								}
								else {
									// wpelni oplacone
									return 'skompletowane - opłacone';
								}
							break;
						}
					break;
				}
			break;
			// zrealizowane
			case '4':
				return 'zrealizowane';
			break;
			// anulowane
			case '5':
				return 'anulowane';
			break;
		}
		return '';
	} // end of getOrderStatus() method
	
	
		/**
	 * Pobiera dane adresowe z zamówienai z bazy
	 * @param int $iId - id adresu
	 * @return array ref
	 */
	function &getOrderAddresses($iId){
		global $aConfig;

		$sSql = "SELECT address_type, is_company, company, name, surname, street, number, number2, postal, city, nip, phone
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
						 " ORDER BY address_type"; 
		return Common::GetAssoc($sSql);
	} // end of getOrderAddresses() method
	
	
	/**
	 * Metoda pobiera elementy zamowienia
	 *
	 * @global array $aConfig
	 * @param integer $iId
	 * @param bool $bEnabledSecondInvoice
	 * @param bool $bSecondInvoice
	 * @return array
	 */
	function getInvoiceItems($iId,$bEnabledSecondInvoice,$bSecondInvoice=false){
	global $aConfig;
		$aData = array();
		$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_items
						WHERE order_id = ".$iId.
						($bEnabledSecondInvoice?($bSecondInvoice?" AND second_invoice='1'":" AND second_invoice='0'"):'');
		
		$aData =& Common::GetAll($sSql);
		if(!empty($aData)){
			foreach ($aData as $iKey => $aValue){
				$aData[$iKey]['price_brutto'] = Common::formatPrice($aValue['price_brutto']);
				$aData[$iKey]['promo_price_brutto'] = Common::formatPrice($aValue['promo_price_brutto']);
				$aData[$iKey]['nopromo_value_brutto'] = Common::formatPrice($aValue['value_brutto']+$aValue['discount_currency']);
				$aData[$iKey]['link'] = createProductLink($aValue['product_id'], $aValue['name']);
				$aData[$iKey]['status_text'] = $aConfig['lang']['mod_'.$this->sModule]['shipment_'.$aValue['shipment_time']];
				if($aValue['deleted']=='1'){
					$aData[$iKey]['status_text'] = $aConfig['lang']['mod_'.$this->sModule]['item_deleted'];
				}
				if($aValue['packet']>0) {
					$aData[$iKey]['items']=Common::GetAll('SELECT P.name FROM products_packets_items I JOIN products P ON P.id=I.product_id WHERE packet_id='.$aValue['product_id']);
				}
				if ($aValue['attachments']>0) {
					$aData[$iKey]['value_brutto_'] = $aValue['bundle_value_brutto'];
					$aData[$iKey]['value_brutto'] = Common::formatPrice($aValue['bundle_value_brutto']);
				} else {
					$aData[$iKey]['value_brutto_'] = $aValue['value_brutto'];
					$aData[$iKey]['value_brutto'] = Common::formatPrice($aValue['value_brutto']);
				}
					
			}
		}
		// usunmy elementy pakietu
		/*
		foreach ($aData as $iKey => $aItemOrder) {
			if ($aItemOrder['parent_id'] > 0 && $aItemOrder['item_type'] == 'P') {
				unset($aData[$iKey]);
			}
		}
		*/
		return $aData;
	}// end of getInvoiceItems() method
	
	
	function getPlatnosciForm($iId,$fCost,$bCardPayment) {
		global $aConfig;
		
		$aForm = array();
		$sCartPageLink = getModulePageLink('m_zamowienia');
		$aForm['action'] = createLink($sCartPageLink, 'platnosci_pay','',array('id'=>$iId));
		$aForm['reflink'] = $this->sPageLink;
		$aForm['card'] = $bCardPayment?'1':'0';
		if(!$bCardPayment) {
			include_once ('Platnoscipl.class.php');
			$pPlatnosci = new Platnoscipl();
			$aForm['payments'] = $pPlatnosci->getPaymentSelection($fCost, false, $aConfig['common']['platosci_pl_show_test_mode']);
		}
		return $aForm;
	}
	
	
	
	/**
	 * Generuje fakture w pdf
	 * @param int $iOrderId
	 * @return void
	 */
	function generateInvoice($iOrderId, $sTemplatePath = ''){
		global $pSmarty,$aConfig;
		
		// ---------------------------------------------------------
		require_once('Invoice.class.php');
		include_once('modules/m_zamowienia/lang/invoice_'.$aConfig['default']['language'].'.php');
		$oInvoice = new Invoice();
		$oInvoice->sTemplatePath = $aConfig['common']['client_base_path'].'omniaCMS/smarty/templates/';
    
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id = ".$iOrderId."
						AND user_id = ".$this->iUserId;
		$aOrder = Common::GetRow($sSql);
		if(!empty($aOrder)) {

			$_GET['hideHeader'] = '1';
			$fName = 'faktura_proforma'.str_replace("/","_",$aOrder['order_number']).'.pdf';
			$fName2 = 'faktura'.str_replace("/","_",$aOrder['invoice_id']).'.pdf';
			$sFile = $aConfig['common']['base_path'].$aConfig['common']['pro_forma_invoice_dir'].$fName;
			$sFile2 = $aConfig['common']['base_path'].$aConfig['common']['cms_dir'].$aConfig['common']['invoice_dir'].$fName2;

			// Jezeli uzytkownik o ID 3 = korecki@omnia.pl
			//if (((int) $_SESSION['w_user']['id']) === 3) {
			//	UserOrders::sendInvoice($sFile2);
			//}
			//jesli zamówienie jest zrealizowane i istnieje faktura to zwracamy fakture
			if($aOrder['order_status'] == '4') {
				// wyslanie faktury do uzytkownika
        if ($oInvoice->invoiceExists($iOrderId, false)) {
          UserOrders::sendInvoice($oInvoice, $sFile2, $fName2, $iOrderId, false);
        } else {
          // ok generujemy fakturę
          $oInvoice->getInvoicePDF($iOrderId, $aOrder);
        }
			}
			else {
				// faktura nie istnieje - sprawdzamy czy istnieje ProForma
				/* 
				 *  MODYFIKACJA 05.04.2012 - za każdym razem faktura PROFORMA jest generowana
				if ($oInvoice->invoiceExists($iOrderId, true)) {
					// wyslanie faktury ProForma do uzytkownika
					UserOrders::sendInvoice($oInvoice, $sFile, $fName, $iOrderId, true);
				}
				// ProForma nie istnieje, musimy wygenerować PDF z faktura ProForma
				else {
					*/
					$aLang =& $aConfig['lang'][$this->sModule];

					require_once('OLD_tcpdf/config/lang/pl.php');
					require_once('OLD_tcpdf/tcpdf.php');

					// create new PDF document
					$pdf = new TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

					// set document information
					$pdf->SetCreator(PDF_CREATOR);
					$pdf->SetAuthor('profit24');
					$pdf->SetTitle($aLang['title']);
					$pdf->SetSubject($aLang['title']);
					$pdf->SetKeywords('');
					$pdf->setPrintHeader(false);

					// set default header data
					//$pdf->SetHeaderData('logo.gif', PDF_HEADER_LOGO_WIDTH, $aLang['logo_title'], $aLang['logo_string']);

					// set header and footer fonts
					//$pdf->setHeaderFont(Array('freesans', '', PDF_FONT_SIZE_MAIN));
					$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));

					// set default monospaced font
					$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

					//set margins
					$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
					//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
					$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

					//set auto page breaks
					$pdf->SetAutoPageBreak(TRUE, 10);

					//set image scale factor
					$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

					//set some language-dependent strings
					$pdf->setLanguageArray($l);

          if (empty($sTemplatePath)) {
            $oInvoice->sTemplatePath = $this->sTemplatesPath.'/';
          } else {
            $oInvoice->sTemplatePath = $sTemplatePath;
          }
					$sHtml =$oInvoice->getInvoiceHtml($iOrderId,false,true);

					// set font
					$pdf->SetFont('freesans', '', 10);

					// add a page
					$pdf->AddPage();
					$pdf->writeHTML($sHtml, true, false, false, false, '');
					// druga faktura
					if($aOrder['second_invoice'] == '1'){
						$sHtml2 =$oInvoice->getInvoiceHtml($iOrderId,true,true);
						if(!empty($sHtml2)){
							// add a page
							$pdf->AddPage();
							$pdf->writeHTML($sHtml2, true, false, false, false, '');
						}
					}
					//Close and output PDF document
					//$pdf->Output($sFile, 'F');
					//$pdf->Output($fName, 'D');
					$oInvoice->saveInvoiceData($iOrderId, $fName, $pdf->Output($sFile, 'S'), true, 'NOW()');
					$pdf->Output($fName, 'D');
				//}
			}
			return true;
		}
		else {
			return false;
		}
	} // end of generateInvoice() method
	
	
	function sendInvoice(&$oInvoice, $sFile, $fName, $iOrderId, $bProforma = false) {
		
		$sContent =& $oInvoice->getInvoiceData($iOrderId, $bProforma);
		if ($sContent != '') {
				header('Content-Type: application/pdf');
				header('Content-Disposition: attachment; filename="'.$fName.'"');
				header('Content-Transfer-Encoding: binary');
				//header('Content-Length: '.$iSize);
				
				echo $sContent;
		}
		exit(0);
	} // end of sendInvoice() method
} // end of class UserOrders
?>