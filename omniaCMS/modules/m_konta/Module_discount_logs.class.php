<?php
/**
 * Klasa Module do obslugi modulu 'Konta uzytkownikow' - logi nadania rabatu
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2008
 * @version   1.0
 *
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// Id strony modulu
	var $iPageId;
	
	// katalog plikow log
	var $sLogsDir;


  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig, $pDB2;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_konta');
		$this->sModule = $_GET['module'];
		$this->sLogsDir = $aConfig['common']['base_path'].'logs';
		
		$sDo = '';
		$iId = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'details': $this->Details($pSmarty, $iId); break;
			default: $this->Show($pSmarty); break;
		}
	} // end Module() function


	/**
	 * Metoda usuwa wybrane logi rabatow
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$sFile	- nazwa usuwanego logu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $sFile) {
		global $aConfig;
		$bIsErr = false;

		if ($sFile != '' && file_exists($this->sLogsDir.'/'.$sFile)) {
			// usuwanie
			if (!@unlink($this->sLogsDir.'/'.$sFile)) {
				$bIsErr = true;
				break;
			}
			
			if (!$bIsErr) {
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok'],
												$sFile);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
			else {
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'],
												$sFile);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton


	/**
	 * Metoda wyswietla liste logow rabatow
	 *
	 * @param	object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true,
			'checkboxes'	=> false,
			'per_page' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['list_file'],
				'sortable'	=> false
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['list_date'],
				'sortable'	=> false,
				'width'	=> '150'
			),
			array(
				'content'	=> $aConfig['lang'][$this->sModule]['list_discount'],
				'sortable'	=> false,
				'width'	=> '125'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '60'
			)
		);

		// pobranie logow
		$aLogs =& $this->getLogs();
		
		$pView = new View('discount_logs', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

			
		if (count($aLogs) > 0) {
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'name' => array (
					'link'	=> phpSelf(array('do' => 'details', 'id' => '{name}'))
				),
				'action' => array (
					'actions'	=> array ('details', 'delete'),
					'params' => array (
												'details'	=> array('id' => '{name}'),
												'delete'	=> array('id' => '{name}')
											),
					'show' => false
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aLogs, $aColSettings);
		}
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda tworzy formularz ze szczegolami logu rabatow
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$sFile	- nazwa pliko logow
	 * @return	void
	 */
	function Details(&$pSmarty, $sFile) {
		global $aConfig;

		$aData = array();
		$aLang =& $aConfig['lang'][$this->sModule];
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['discount_log_details_header'], $sFile);

		$pForm = new FormTable('discount_log_details', $sHeader, array('action'=>phpSelf()), array('col_width'=>1), false);
		
		$pForm->AddMergedRow('<pre>'.implode('', file($this->sLogsDir.'/'.$sFile)).'</pre>', array('style'=>'text-align: left; font-weight: normal;'));
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Details() function
	
	
	function &getLogs() {
		$aLogs	= array();
		
		$pDir = opendir($this->sLogsDir);
  	while (false !== ($sFile = readdir($pDir))) {
    	if (preg_match('/^rabat_(\d{2})(\d{2})(\d{4})_(\d{2})(\d{2})(\d{2})__(\d+)(_(\d+))?_proc$/', $sFile, $aMatches)) {
    		$iTime = mktime($aMatches[4], $aMatches[5], $aMatches[6], $aMatches[2], $aMatches[1], $aMatches[3]);
    		$aLogs[$iTime] = array(
    			'name' => $sFile,
    			'date' => $aMatches[1].'-'.$aMatches[2].'-'.$aMatches[3].' '.$aMatches[4].':'.$aMatches[5].':'.$aMatches[6],
    			'discount' => $aMatches[7].(isset($aMatches[9]) ? ','.$aMatches[9] : '')
    		);
      }
    }
    closedir($pDir);
    krsort($aLogs);
    
    return $aLogs;
	} // end of getLogs() method
	
	
	
} // end of Module Class

?>