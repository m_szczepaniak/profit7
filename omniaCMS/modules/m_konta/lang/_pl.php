<?php
/**
* Plik jezykowy modulu 'Konta uzytkownikow'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*/

$aConfig['lang']['mod_m_konta']['header_login']	= '<strong>Dane twojego konta Profit24</strong> - potrzebne do uzyskania dostępu do księgarnii';
$aConfig['lang']['mod_m_konta']['header_vat']	= '<strong>Dane do faktury VAT</strong> - do zamówienia w Profit24 <u>zawsze</u> dołączamy fakturę:';
$aConfig['lang']['mod_m_konta']['header_transport']	= '<strong>Adres dostarczenia przesyłki:</strong>';
$aConfig['lang']['mod_m_konta']['header_send']	= 'Dane adresowe dla dostawy i wystawienia faktury VAT';
$aConfig['lang']['mod_m_konta']['header_invoice']	= 'Faktura VAT na inne dane';
$aConfig['lang']['mod_m_konta']['header_user_data']	= '<strong>Edycja danych Twojego konta (imię, nazwisko, telefon)</strong>';
$aConfig['lang']['mod_m_konta']['header_data']	= '<strong>Wybierz, które dane chcesz zmienić:</strong>';

$aConfig['lang']['mod_m_konta']['edit_header']	= 'Twoje dane';
$aConfig['lang']['mod_m_konta']['change_data']	= 'Zmień dane';
$aConfig['lang']['mod_m_konta']['header_your_data']	= 'Twoje dane:';
$aConfig['lang']['mod_m_konta']['edit_addresses_header']	= 'Edycja Twoich adresów';
$aConfig['lang']['mod_m_konta']['add_addresses_header'] = 'Dodaj adres';
$aConfig['lang']['mod_m_konta']['change_email_header'] = 'Zmiana Twojego adresu e-mail';
$aConfig['lang']['mod_m_konta']['delete_account_header'] = 'Całkowite usunięcie konta';
$aConfig['lang']['mod_m_konta']['type_corp'] = 'Firma lub instytucja';
$aConfig['lang']['mod_m_konta']['type_private'] = 'Osoba prywatna';
$aConfig['lang']['mod_m_konta']['namesurname'] = 'Imię i nazwisko';
$aConfig['lang']['mod_m_konta']['address'] = 'Adres';

$aConfig['lang']['mod_m_konta']['is_transport'] = 'Domyślny adres do dostarczenia przesyłki';
$aConfig['lang']['mod_m_konta']['is_invoice'] = 'Domyślny adres do faktury VAT';
$aConfig['lang']['mod_m_konta']['is_transport_extra'] = 'Adres dodatkowy';
$aConfig['lang']['mod_m_konta']['newsletter_info'] = '<strong>W tym miejscu możesz wybrać kategorie newsletter-a</strong> (newsletter to wiadmość e-mail, przesyłana na Twój adres e-mail podany przy rejestracji iinformacjami o nowościach w naszej księgarnii), <strong>z których chesz otrzymywać informacje o nowościach w Profit24.</strong><br /> 
<br />
Będziesz go otrzymywać JEDEN RAZ w tygodniu. W każdej chwili będziesz miał dostęp do edycji wybranych kategorii.';

$aConfig['lang']['mod_m_konta']['bad_login']	= 'Zły login lub hasło!';
$aConfig['lang']['mod_m_konta']['bad_login_email']	= 'Zły login lub adres e-mail!';
$aConfig['lang']['mod_m_konta']['profile_edit_header']	= 'Edycja danych';
$aConfig['lang']['mod_m_konta']['edit_profile']	= 'Edycja danych';
$aConfig['lang']['mod_m_konta']['money']	= 'Saldo konta';
$aConfig['lang']['mod_m_konta']['buy_packet']	= 'Doładuj saldo';
$aConfig['lang']['mod_m_konta']['invoice_data']	= 'Faktura VAT na inny adres';
$aConfig['lang']['mod_m_konta']['articles']	= 'Artykuły';
$aConfig['lang']['mod_m_konta']['advices']	= 'Porady';
$aConfig['lang']['mod_m_konta']['order_advice']	= 'Zamów poradę';
$aConfig['lang']['mod_m_konta']['orders']	= 'Zamówienia';
$aConfig['lang']['mod_m_konta']['chngpasswd'] = 'Zmiana hasła';
$aConfig['lang']['mod_m_konta']['discount'] = 'Twoje przywileje';
$aConfig['lang']['mod_m_konta']['discount_header'] = 'Twoje przywileje';
$aConfig['lang']['mod_m_konta']['discount_info'] = 'Przywileje przypisane do Twojego konta';
$aConfig['lang']['mod_m_konta']['your_discount'] = 'Wysokość rabatu przypisanego do Twojego konta wynosi:';
$aConfig['lang']['mod_m_konta']['edit'] = 'Twoje dane';
$aConfig['lang']['mod_m_konta']['do_edit']	= 'aktualizuj';
$aConfig['lang']['mod_m_konta']['logout']	= 'Wyloguj';
$aConfig['lang']['mod_m_konta']['remind_passwd_header']	= ' - przypominanie hasła';
$aConfig['lang']['mod_m_konta']['blog_header']	= ' - Twój blog';
$aConfig['lang']['mod_m_konta']['change_passwd_1_header'] = 'Zmiana hasła';
$aConfig['lang']['mod_m_konta']['registration_header']	= ' - rejestracja w serwisie';
$aConfig['lang']['mod_m_konta']['my_data_header']	= 'Edycja danych';
$aConfig['lang']['mod_m_konta']['discount_codes_header']	= ' - kody rabatowe';
$aConfig['lang']['mod_m_konta']['set_new_passwd_header']	= 'Zmiana hasła';
$aConfig['lang']['mod_m_konta']['account_balance_header']	= ' - saldo konta';
$aConfig['lang']['mod_m_konta']['buy_packet_header']	= ' - doładuj saldo';
$aConfig['lang']['mod_m_konta']['articles_header']	= ' - wykupione artykuły';
$aConfig['lang']['mod_m_konta']['advices_header']	= ' - wykupione porady';
$aConfig['lang']['mod_m_konta']['order_advice_header']	= ' - zamawianie porady';
$aConfig['lang']['mod_m_konta']['activate_header']	= ' - aktywacja konta';
$aConfig['lang']['mod_m_konta']['orders_header']	= 'Historia zamówień';
$aConfig['lang']['mod_m_konta']['balances_header']	= 'Twoje saldo';
$aConfig['lang']['mod_m_konta']['login_header']	= ' - logowanie';
$aConfig['lang']['mod_m_konta']['change_passwd_header']	= 'Zmiana hasła';
$aConfig['lang']['mod_m_konta']['newsletters_header']	= 'Newslettery';
$aConfig['lang']['mod_m_konta']['newsletters_label']	= 'Newslettery';
$aConfig['lang']['mod_m_konta']['newsletters_info']	= 'Edycja subskrypcji newsletterów';
$aConfig['lang']['mod_m_konta']['balance']	= 'Twoje saldo';
$aConfig['lang']['mod_m_konta']['balance_info']	= 'Historia zmian salda';

$aConfig['lang']['mod_m_konta']['edit_userdata_label']	= 'Edycja danych Twojego konta (imię,nazwisko,telefon)';
$aConfig['lang']['mod_m_konta']['edit_addresses_label']	= 'Edycja Twoich adresów';
$aConfig['lang']['mod_m_konta']['change_email_label']	= 'Zmiana Twojego adresu e-mail';
$aConfig['lang']['mod_m_konta']['delete_account_label']	= 'Całkowite usunięcie konta';

$aConfig['lang']['mod_m_konta']['account_delete_orders_err']	= 'Nie można usunąć konta użytkownika, jeśli istnieją złożone zamówienia w trakcie realizacji!';

$aConfig['lang']['mod_m_konta']['remind_passwd_path']	= 'Przypominanie hasła';

$aConfig['lang']['mod_m_konta']['description']	= 'Opis';
$aConfig['lang']['mod_m_konta']['no_user_balances']	= 'Brak pozycji w historii salda';

$aConfig['lang']['mod_m_konta']['login']	= 'Login:';
$aConfig['lang']['mod_m_konta']['login_info']	= 'Uniwersalny identyfikator użytkownika, min. 6 - max. 32 znaki, może zawierać małe i duże litery (bez liter narodowych), cyfry oraz znaki @, - i . (kropka)';
$aConfig['lang']['mod_m_konta']['passwd']	= 'Hasło:';
$aConfig['lang']['mod_m_konta']['passwd_err']	= 'Uzupełnij poprawnie "Hasło"';
$aConfig['lang']['mod_m_konta']['passwd_info']	= 'Min. 6 - max. 32 znaki, NIE MOŻE zawierać znaków \, \' i "';
$aConfig['lang']['mod_m_konta']['old_passwd']	= 'Aktualne hasło';
$aConfig['lang']['mod_m_konta']['new_passwd']	= 'Nowe hasło:';
$aConfig['lang']['mod_m_konta']['do_change_passwd']	= 'zmień hasło';
$aConfig['lang']['mod_m_konta']['sendButton']	= 'wyślij';
$aConfig['lang']['mod_m_konta']['passwd2']	= 'Powtórz hasło:';
$aConfig['lang']['mod_m_konta']['passwd2_err']	= 'Uzupełnij poprawnie "Powtórz hasło"';
$aConfig['lang']['mod_m_konta']['new_passwd2']	= 'Powtórz nowe hasło:';
$aConfig['lang']['mod_m_konta']['old_passwd_err']	= 'Uzupełnij poprawnie "Aktualne hasło"';
$aConfig['lang']['mod_m_konta']['n_passwd_err']	= 'Uzupełnij poprawnie "Nowe hasło"';
$aConfig['lang']['mod_m_konta']['confirm_passwd_err']	= 'Podane hasła są różne';
$aConfig['lang']['mod_m_konta']['email']	= 'Adres e-mail:';
$aConfig['lang']['mod_m_konta']['email_err']	= 'Uzupełnij poprawnie "Adres e-mail"';
$aConfig['lang']['mod_m_konta']['confirm_email']	= 'Powtórz e-mail:';
$aConfig['lang']['mod_m_konta']['confirm_email_err']	= 'Uzupełnij poprawnie "Powtórz e-mail"';
$aConfig['lang']['mod_m_konta']['name']	= 'Imię:';
$aConfig['lang']['mod_m_konta']['name_err']	= 'Uzupełnij "Imię"';
$aConfig['lang']['mod_m_konta']['surname']	= 'Nazwisko:';
$aConfig['lang']['mod_m_konta']['surname_err']	= 'Uzupełnij "Nazwisko"';
$aConfig['lang']['mod_m_konta']['country']	= 'Kraj';
$aConfig['lang']['mod_m_konta']['choose_country']	= '-- wybierz kraj';
$aConfig['lang']['mod_m_konta']['street']	= 'Ulica (nazwa wsi):';
$aConfig['lang']['mod_m_konta']['street_err']	= 'Uzupełnij pole "Ulica (nazwa wsi)"';
$aConfig['lang']['mod_m_konta']['number']	= 'Nr domu:';
$aConfig['lang']['mod_m_konta']['number_err']	= 'Uzupełnij pole "Nr domu"';
$aConfig['lang']['mod_m_konta']['number2']	= 'Nr lokalu';
$aConfig['lang']['mod_m_konta']['number2_err']	= 'Uzupełnij pole "Nr lokalu"';
$aConfig['lang']['mod_m_konta']['number2_info']	= 'Nr lokalu - pole nieobowiązkowe';
$aConfig['lang']['mod_m_konta']['postal']	= 'Kod pocztowy:';
$aConfig['lang']['mod_m_konta']['postal_err']	= 'Uzupełnij pole "Kod pocztowy"';
$aConfig['lang']['mod_m_konta']['postal_info']	= 'W formacie: 12-345';
$aConfig['lang']['mod_m_konta']['city']	= 'Miejscowość (poczta):';
$aConfig['lang']['mod_m_konta']['phone']	= 'Telefon:';
$aConfig['lang']['mod_m_konta']['phone_err']	= 'Uzupełnij poprawnie "Telefon"';
$aConfig['lang']['mod_m_konta']['phone_info']	= 'W formacie: 12 3456789 (stacjonarny) lub 123456789 (komórkowy)';
$aConfig['lang']['mod_m_konta']['cellular_phone']	= 'Tel. komórkowy:';
$aConfig['lang']['mod_m_konta']['private_client'] = 'Osoba prywatna';
$aConfig['lang']['mod_m_konta']['company_client'] = 'Firma lub instytucja';
$aConfig['lang']['mod_m_konta']['company']	= 'Nazwa:';
$aConfig['lang']['mod_m_konta']['company_err']	= 'Uzupełnij pole "Nazwa"';
$aConfig['lang']['mod_m_konta']['nip']	= 'NIP:';
$aConfig['lang']['mod_m_konta']['nip_err']	= 'Uzupełnij pole "NIP"';
$aConfig['lang']['mod_m_konta']['faktura']	= 'Dane do faktury:';
$aConfig['lang']['mod_m_konta']['faktura_vat']	= 'Faktura VAT';
$aConfig['lang']['mod_m_konta']['firm']	= 'Imię, nazwisko / firma';
$aConfig['lang']['mod_m_konta']['payment_data']	= 'Jeśli dane płatnika są identyczne zaznacz (w przypadku firmy uzupełnij poniższe pole NIP)';
$aConfig['lang']['mod_m_konta']['newsletter'] = 'Newsletter';
$aConfig['lang']['mod_m_konta']['avatar'] = 'Avatar forum';
$aConfig['lang']['mod_m_konta']['privacy_text'] = 'Wyrażam zgodę na przetwarzanie moich danych osobowych do celów marketingowych';
$aConfig['lang']['mod_m_konta']['avatar_info'] = 'Dopuszczalne formaty: <strong>jpg, gif, png</strong>; maks. rozmiar <strong>100 x 100 pikseli</strong>';
$aConfig['lang']['mod_m_konta']['current_avatar'] = 'Twój avatar';
$aConfig['lang']['mod_m_konta']['delete_avatar'] = 'Usuń ten avatar';
$aConfig['lang']['mod_m_konta']['add_to_newsletter'] = 'Chcę otrzymywać newsletter';
$aConfig['lang']['mod_m_konta']['newsletter_choose_category'] = 'Chcę otrzymywać newsletter z kategorii:';
$aConfig['lang']['mod_m_konta']['privacy']	= 'Zgoda na przetwarzanie danych osobowych';
$aConfig['lang']['mod_m_konta']['regulations']	= 'Zapoznałem się z <a href="%s" target="_blank">regulaminem</a> i akceptuję go';
$aConfig['lang']['mod_m_konta']['regulations_err']	= 'Akceptacja regulaminu';
$aConfig['lang']['mod_m_konta']['do_register']	= 'utwórz konto';
$aConfig['lang']['mod_m_konta']['do_login']	= 'zaloguj';
$aConfig['lang']['mod_m_konta']['discount_code'] = 'Wpisz swój kod rabatowy';
$aConfig['lang']['mod_m_konta']['codes_history'] = 'Użyte przez ciebie kody rabatowe';
$aConfig['lang']['mod_m_konta']['order_id'] = 'Numer zamówienia';
$aConfig['lang']['mod_m_konta']['order_id_mini'] = 'Nr zamówienia';
$aConfig['lang']['mod_m_konta']['order_date'] = 'Data zamówienia';
$aConfig['lang']['mod_m_konta']['order_value'] = 'Wartość zamówienia';
$aConfig['lang']['mod_m_konta']['payment_form'] = 'Forma wpłaty';
$aConfig['lang']['mod_m_konta']['payment_form2'] = 'Forma<br />wpłaty';
$aConfig['lang']['mod_m_konta']['order_realization_date'] = ' Data realizacji';
$aConfig['lang']['mod_m_konta']['order_status'] = 'Status zamówienia';
$aConfig['lang']['mod_m_konta']['order_payment_type'] = 'Sposób płatności';
$aConfig['lang']['mod_m_konta']['payment_date'] = 'Data wpłaty';
$aConfig['lang']['mod_m_konta']['paid_amount'] = 'Kwota wpłaty';
$aConfig['lang']['mod_m_konta']['balance'] = 'Saldo';
$aConfig['lang']['mod_m_konta']['order_transport_type'] = 'Sposób wysyłki';
$aConfig['lang']['mod_m_konta']['transport_number'] = 'Nr listu przewozowego';
$aConfig['lang']['mod_m_konta']['invoice_number'] = 'Nr faktury VAT';
$aConfig['lang']['mod_m_konta']['invoice_number2'] = 'Nr drugiej faktury VAT';
$aConfig['lang']['mod_m_konta']['discount_code2'] = 'Kod rabatowy';
$aConfig['lang']['mod_m_konta']['kurier']	= 'Transport za pośrednictwem firmy spedycyjnej';
$aConfig['lang']['mod_m_konta']['wlasny']	= 'Transport firmowy (do 250km)';
$aConfig['lang']['mod_m_konta']['is_company']	= 'Odbiorca';
$aConfig['lang']['mod_m_konta']['is_company_0']	= 'Osoba prywatna';
$aConfig['lang']['mod_m_konta']['is_company_1']	= 'Firma lub instytucja';
$aConfig['lang']['mod_m_konta']['transport_addres']	= 'Wybierz adres';
$aConfig['lang']['mod_m_konta']['transport_addres_0']	= 'Taki sam jak na fakturze';
$aConfig['lang']['mod_m_konta']['transport_addres_1']	= 'Przesyłka na inny adres';
$aConfig['lang']['mod_m_konta']['address_name'] = 'Nazwa tego adresu:';
$aConfig['lang']['mod_m_konta']['delete_address'] = 'Usuń adres';
$aConfig['lang']['mod_m_konta']['edit_address'] = 'Edytuj adres';
$aConfig['lang']['mod_m_konta']['add_address'] = 'Dodaj kolejny adres';

$aConfig['lang']['mod_m_konta']['default_invoice'] = 'Domyślny adres do faktury VAT';
$aConfig['lang']['mod_m_konta']['default_transport'] = 'Domyślny adres do wysyłki';

$aConfig['lang']['mod_m_konta']['default_2'] = 'Domyślny adres do faktury VAT';
$aConfig['lang']['mod_m_konta']['default_1'] = 'Domyślny adres do wysyłki';
$aConfig['lang']['mod_m_konta']['default_3'] = 'Domyślny adres do faktury VAT oraz wysyłki';

$aConfig['lang']['mod_m_konta']['form_send_err']	= 'Uzupełnij poprawnie formularz!';

$aConfig['lang']['mod_m_konta']['edit_info']	= 'Edycja Twoich Danych';
$aConfig['lang']['mod_m_konta']['orders_info']	= 'Przeglądanie Twoich zamówień';
$aConfig['lang']['mod_m_konta']['password_info']	= 'Możliwość zmiany Twojego dotychczasowego hasła';

$aConfig['lang']['mod_m_konta']['add_ok']	= 'Twoje konto zostało utworzone<br />Na adres <strong>%s</strong> został wysłany e-mail z potwierdzeniem rejestracji.';
$aConfig['lang']['mod_m_konta']['add_err']	= 'Wystąpił błąd podczas tworzenia Twojego konta.<br /><br />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';
$aConfig['lang']['mod_m_konta']['update_ok']	= 'Twoje dane zostały zaktualizowane.';
$aConfig['lang']['mod_m_konta']['update_err']	= 'Wystąpił błąd podczas aktualizacji Twoich danych.<br /><br />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';
$aConfig['lang']['mod_m_konta']['user_err']	= 'Użytkownik nie istnieje!';
$aConfig['lang']['mod_m_konta']['address_add_ok']	= 'Adres został dodany do Twojego konta';
$aConfig['lang']['mod_m_konta']['address_add_err']	= 'Wystąpił błąd podczas dodawania adresu do Twojego konta.<br /><br />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';
$aConfig['lang']['mod_m_konta']['address_delete_ok']	= 'Adres "%s" został usunięty z Twojego konta';
$aConfig['lang']['mod_m_konta']['address_delete_err']	= 'Wystąpił błąd podczas usuwania adresu "%s" z Twojego konta.<br /><br />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';

$aConfig['lang']['mod_m_konta']['account_delete_err']	= 'Wystąpił błąd podczas Twojego konta.<br /><br />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';
$aConfig['lang']['mod_m_konta']['account_delete_ok']	= 'Twoje konto zostalo całkowicie usunięte z serwisu';

$aConfig['lang']['mod_m_konta']['login_exists'] = 'Istnieje już Konto Użytkownika o podanym loginie <strong>%s</strong>!';
$aConfig['lang']['mod_m_konta']['email_exists'] = 'Istnieje już Konto Użytkownika o podanym adresie e-mail <strong>%s</strong>!';

$aConfig['lang']['mod_m_konta']['passwd_change_ok']	= 'Twoje hasło zostało zmienione.';
$aConfig['lang']['mod_m_konta']['current_passwd_err']	= 'Podałeś niepoprawne aktualne hasło!<br /><br />Spróbuj jeszcze raz.';
$aConfig['lang']['mod_m_konta']['passwd_change_err']	= 'Wystąpił błąd podczas zmiany Twojego hasła.<br /><br />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';
$aConfig['lang']['mod_m_konta']['check_discount_code'] = 'Podany przez ciebie kod rabatowy jest nieprawidłowy. Rabat nie został przyznany.';
$aConfig['lang']['mod_m_konta']['check_discount_code_ok'] = 'Twój kod rabatowy jest prawidłowy. Rabat został przyznany.';

$aConfig['lang']['mod_m_konta']['newsletter_edit_ok']	= 'Twoje subskrypcje newslettera zostały zmienione.';
$aConfig['lang']['mod_m_konta']['newsletter_edit_err']	= 'Wystąpił błąd podczas zmiany Twoich subskrypcji newslettera.<br /><br />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';

$aConfig['lang']['mod_m_konta']['newsletters_change_button']	= 'Zmień';

$aConfig['lang']['mod_m_konta']['email_change_ok']	= 'Twój email został zmieniony.';
$aConfig['lang']['mod_m_konta']['email_change_err']	= 'Wystąpił błąd podczas zmiany Twojego emaila.<br /><br />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';


$aConfig['lang']['mod_m_konta']['registration_email_subject'] = 'Potwierdzenie rejestracji w serwisie %s';
$aConfig['lang']['mod_m_konta']['registration_email_body_header'] = 'Witaj %s,<br />Witamy wśród użytkowników serwisu';
$aConfig['lang']['mod_m_konta']['registration_email_body_content'] = 'Twoje konto zostało utworzone.';
$aConfig['lang']['mod_m_konta']['registration_email_body_login_site'] = 'Strona logowania:';
$aConfig['lang']['mod_m_konta']['registration_email_body_login_data'] = 'Dane logowania Twojego konta.';
$aConfig['lang']['mod_m_konta']['registration_email_body_end'] = ''; 

$aConfig['lang']['mod_m_konta']['registration_email2_subject'] = 'Aktywacja konta w serwisie %s';
$aConfig['lang']['mod_m_konta']['registration_email2_body_header'] = 'Witamy wśród użytkowników serwisu ';
$aConfig['lang']['mod_m_konta']['registration_email2_body_content'] = 'Twoje konto zostało utworzone ale nie jest jeszcze aktywne, aby je aktywować kliknij lub skopiuj do paska adresu przeglądarki poniższy link:';

$aConfig['lang']['mod_m_konta']['user_passwd_email_subject'] = 'Przypomnienie hasła do serwisu %s';
$aConfig['lang']['mod_m_konta']['user_passwd_email_body'] = 'To jest przypomnienie Twojego hasła w serwisie %s.<br /></br />
Twoje dane logowania to<br /><br />
login (e-mail): <strong>%s</strong><br />
hasło: <strong>%s</strong>';

$aConfig['lang']['mod_m_konta']['email_exists'] = 'Istnieje już Konto Użytkownika o podanym adresie e-mail <strong>%s</strong>!';
$aConfig['lang']['mod_m_konta']['email_confirm_err'] = 'Podane aresy email różnią się od siebie!';
$aConfig['lang']['mod_m_konta']['email_doesnt_exist']	= 'Konto o podanym adresie e-mail <strong>%s</strong> nie istnieje!';
$aConfig['lang']['mod_m_konta']['user_password_sent']	= 'Twoje hasło zostało wysłane na podany adres e-mail <strong>%s</strong>.';


$aConfig['lang']['mod_m_konta']['discount_code_email_body'] = 'Jako nowy użytkownik otrzymałeś kod rabatowy uprawniający do %d %% zniżki. Aby aktywować swój kod oraz dowiedzieć się więcej informacji zaloguj się na swoje nowo utworzone konto oraz w zakładce "Kody rabatowe" wpisz następujący kod: <br />%s<br /><br />Twój kod jest ważny do: %s, możesz go użyć %s razy.';
$aConfig['lang']['mod_m_konta']['already_active'] = 'Konto jest już aktywne!';
$aConfig['lang']['mod_m_konta']['activation_err'] = 'Wystąpił błąd podczas aktywacji konta!<br /><br  />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';
$aConfig['lang']['mod_m_konta']['activation_ok'] = 'Konto zostało aktywowane.<br />Możesz się zalogować.';
$aConfig['lang']['mod_m_konta']['not_active']	= 'Twoje konto nie zostało jeszcze aktywowane.<br /><br />Podczas rejestracji na adres <strong>%s</strong> został wysłany e-mail z prośbą o potwierdzenie - kliknij link, który się w nim znajduje.<br /><br />Jeżeli nie otrzymałeś od nas żadnego maila - <a href="%s">kliknij, aby wysłać go ponownie</a> lub skontaktuj się z nami <a href="mailto:%s">%s</a>.';
$aConfig['lang']['mod_m_konta']['activation_resent']	= 'Na adres <strong>%s</strong> został wysłany e-mail z prośbą o potwierdzenie - odbierz go i kliknij link, który się w nim znajduje.<br /><br />Jeżeli nie otrzymałeś od nas żadnego maila - <a href="%s">kliknij, aby wysłać go ponownie</a> lub skontaktuj się z nami <a href="mailto:%s">%s</a>.';

$aConfig['lang']['mod_m_konta']['do_change_passwd_1']	= 'dalej';
$aConfig['lang']['mod_m_konta']['do_change_passwd_2']	= 'zmień hasło';
$aConfig['lang']['mod_m_konta']['passwd_change_key_err']	= 'Wystąpił błąd!<br /><br  />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';
$aConfig['lang']['mod_m_konta']['change_passwd_email_subject'] = 'Twoje hasło w serwisie %s';
$aConfig['lang']['mod_m_konta']['change_passwd_email_body'] = 'Twoje hasło w serwisie %s to: %s';
$aConfig['lang']['mod_m_konta']['passwd_change_key_sent']	= 'Mail z twoim hasłem został wysłany na adres <strong>%s</strong>.<br /><br />Jeżeli nie otrzymałeś od nas żadnego maila - <a href="%s">kliknij, aby wysłać go ponownie</a> lub skontaktuj się z nami <a href="mailto:%s">%s</a>.';
$aConfig['lang']['mod_m_konta']['do_change_passwd'] = 'Zmień hasło';
$aConfig['lang']['mod_m_konta']['do_change_email'] = 'Zmień email';
$aConfig['lang']['mod_m_konta']['bad_change_passwd_key'] = 'Zły lub nieaktualny klucz zmiany hasła!<br /><br /><a href="%s">Kliknij</a> i wygeneruj nowy klucz zmiany hasła.';
$aConfig['lang']['mod_m_konta']['passwd_changed']	= 'Twoje hasło zostało zmienione.<br />Możesz się zalogować.';
$aConfig['lang']['mod_m_konta']['passwd_change_err']	= 'Wystąpił błąd podczas próby zmiany hasła!<br /><br  />Spróbuj ponowanie, jeżeli błąd będzie się powtarzał prosimy o kontakt <a href="mailto:%s">%s</a>.';

// artykuly
$aConfig['lang']['mod_m_konta']['no_user_articles']	= 'Nie wykupiłeś jeszcze żadnych artykułów.';

// porady
$aConfig['lang']['mod_m_konta']['no_user_advices']	= 'Nie wykupiłeś jeszcze żadnych porad.';

// slado konta
$aConfig['lang']['mod_m_konta']['account_balance']	= 'Aktualne saldo Twojego konta to: <span><strong>%s PLN</strong></span>';
$aConfig['lang']['mod_m_konta']['buy_info']	= 'Aby zasilić Twoje konto Pakietem Wartościowym dodaj interesujący Cie Pakiet z poniższej listy do koszyka';
$aConfig['lang']['mod_m_konta']['header_name']	= 'Nazwa Pakietu';
$aConfig['lang']['mod_m_konta']['header_price']	= 'Cena';
$aConfig['lang']['mod_m_konta']['header_value']	= 'Wartość';
$aConfig['lang']['mod_m_konta']['buy_now']	= 'Kup teraz';
$aConfig['lang']['mod_m_konta']['current_balance']	= 'Twoje bieżące saldo w rozrachunkach z Księgarnią Profit24 to: ';

$aConfig['lang']['mod_m_konta']['balance_history']	= 'Historia operacji';
$aConfig['lang']['mod_m_konta']['no_user_balance_history']	= 'Brak dotychczasowych operacji na saldzie.';
$aConfig['lang']['mod_m_konta']['operation_type']	= 'Typ';
$aConfig['lang']['mod_m_konta']['operation_types']['0']	= 'Obciążenie';
$aConfig['lang']['mod_m_konta']['operation_types']['1']	= 'Uznanie';
$aConfig['lang']['mod_m_konta']['operation_name']	= 'Operacja';
$aConfig['lang']['mod_m_konta']['operation_amount']	= 'Kwota';
$aConfig['lang']['mod_m_konta']['operation_balance']	= 'Saldo&nbsp;po&nbsp;/&nbsp;przed';
$aConfig['lang']['mod_m_konta']['bought_article']	= 'Opłacono artykuł';
$aConfig['lang']['mod_m_konta']['bought_packet']	= 'Wykupiono pakiet';
$aConfig['lang']['mod_m_konta']['bought_advice']	= 'Opłacono poradę';
$aConfig['lang']['mod_m_konta']['order_no']	= 'Zamówienie nr';
$aConfig['lang']['mod_m_konta']['pager_total_items']	= 'wszystkich operacji';
$aConfig['lang']['mod_m_konta']['pager_total_articles']	= 'wszystkich artykułów';
$aConfig['lang']['mod_m_konta']['pager_total_advices']	= 'wszystkich porad';
$aConfig['lang']['mod_m_konta']['pager_total_orders']	= 'wszystkich zamówień';

// orders
$aConfig['lang']['mod_m_konta']['order_number'] = 'Zamówienie nr: %s - %s';

$aConfig['lang']['mod_m_konta']['platnosci_pl_order_id']	= 'Zamowienie nr:';
$aConfig['lang']['mod_m_konta']['my_orders_header']	= ' - moje zamówienia';
$aConfig['lang']['mod_m_konta']['no_user_orders']	= 'Nie składałeś jeszcze żadnych zamówień.';
$aConfig['lang']['mod_m_konta']['cart_info']	= 'Trochę informacji na temat co i jak można robić z koszykiem';
$aConfig['lang']['mod_m_konta']['list_name']	= 'Nazwa';
$aConfig['lang']['mod_m_konta']['list_position']	= 'Pozycja';
$aConfig['lang']['mod_m_konta']['list_quantity']	= 'Ilość';
$aConfig['lang']['mod_m_konta']['list_price_brutto']	= 'Cena brutto';
$aConfig['lang']['mod_m_konta']['list_price_netto']	= 'Cena netto';
$aConfig['lang']['mod_m_konta']['total']	= 'Suma: ';
$aConfig['lang']['mod_m_konta']['transport_type']	= 'Sposób wysyłki:';
$aConfig['lang']['mod_m_konta']['total_cost']	= 'Całkowity koszt twojego zamówienia:';
$aConfig['lang']['mod_m_konta']['total_cost2']	= 'Łączna kwota do zapłaty:';
$aConfig['lang']['mod_m_konta']['back_to_list']	= '&laquo; Powrót do listy zamówień';
$aConfig['lang']['mod_m_konta']['order_details']	= 'Szczegóły zamówienia numer';
$aConfig['lang']['mod_m_konta']['total_products_netto']	= 'Wartość towarów netto:';
$aConfig['lang']['mod_m_konta']['total_products_brutto']	= 'Wartość brutto';
$aConfig['lang']['mod_m_konta']['total_price']	= 'Wartość';
$aConfig['lang']['mod_m_konta']['total_cost_netto']	= 'SUMA NETTO:';
$aConfig['lang']['mod_m_konta']['total_cost_brutto']	= 'Suma zamówienia:';
$aConfig['lang']['mod_m_konta']['total_transport_cost']	= 'Koszt transportu:';
$aConfig['lang']['mod_m_konta']['total_postal_fee_cost']	= 'Koszt pobrania:';
$aConfig['lang']['mod_m_konta']['transport']	= 'Transport:';
$aConfig['lang']['mod_m_konta']['installment_cost']	= 'Opłata manipulacyjna:';
$aConfig['lang']['mod_m_konta']['details']	= 'Szczegóły';

//$aConfig['lang']['mod_m_konta']['transport_invoice_item'] = '';
$aConfig['lang']['mod_m_konta']['orders_order_id']	= 'Numer zamówienia: ';
$aConfig['lang']['mod_m_konta']['orders_order_date']	= 'Data zamówienia: ';
$aConfig['lang']['mod_m_konta']['orders_order_status']	= 'Status zamówienia: ';
$aConfig['lang']['mod_m_konta']['orders_price']	= 'Wartość zamówienia ';
$aConfig['lang']['mod_m_konta']['pey_order']	= 'Opłać zamówienie';
$aConfig['lang']['mod_m_konta']['orders_status']	= 'Status zamówienia';
$aConfig['lang']['mod_m_konta']['status']	= 'Status';
$aConfig['lang']['mod_m_konta']['order_status_0'] = "nowe";
$aConfig['lang']['mod_m_konta']['order_status_1'] = "opłacone";
$aConfig['lang']['mod_m_konta']['order_status_2'] = "zrealizowane / wysłane";
$aConfig['lang']['mod_m_konta']['order_status_3'] = "transakcja odmowna";
$aConfig['lang']['mod_m_konta']['order_status_4'] = "anulowane";
$aConfig['lang']['mod_m_konta']['orders_payment']	= 'Sposób płatności: ';
$aConfig['lang']['mod_m_konta']['orders_payment_types']['bank_transfer']	= 'przelew bankowy';
$aConfig['lang']['mod_m_konta']['orders_payment_types']['allpay']	= 'allpay';
$aConfig['lang']['mod_m_konta']['payment']	= 'Metoda płatności';

$aConfig['lang']['mod_m_konta']['user_privileges'] = "Masz przyznane następujace przywileje:";
$aConfig['lang']['mod_m_konta']['user_privs']['allow_14days'] = 'Przelew 14 dni';
$aConfig['lang']['mod_m_konta']['user_privs']['allow_personal_reception'] = 'Odbiór osobisty';
$aConfig['lang']['mod_m_konta']['user_privs']['allow_free_transport'] = 'Zwolnienie z kosztów transportu';

$aConfig['lang']['mod_m_konta']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_konta']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_konta']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_konta']['shipment_3'] = "tylko na zamówienie";

$aConfig['lang']['mod_m_konta']['info_register_box_title'] = "tylko na zamówienie";
$aConfig['lang']['mod_m_konta']['info_register_box_content_1'] = "
Szanowny Kliencie,<br />
Uprzejmie prosimy o wypełnienie formularza zgodnie z etykietami.
Na następnym kroku będzie możliwość zdefiniowania adresów do wystawienia faktury VAT i dostawy.
";
$aConfig['lang']['mod_m_konta']['info_register_box_content_2'] = "
W tym miejscu zdefiniuj adres na który wystawimy fakturę lub faktury. Adresów możesz mieć wiele na swoim koncie i każdy nazwać inaczej, <strong>np.: dom, praca, babcia.</strong> <br />
<br />
Jeżeli adres dostawy książek jest inny niż dane na fakturze, wypełnij formularz znajdujący się poniżej.<br />
Wpisz numer telefonu, tak by kurier mógł się z Tobą skontaktować i ewentualnie doprecyzować szczegóły dostawy. Pamiętaj, że kurierzy pracują w godzinach 10 – 17, więc wskazane jest podanie adresu pod którym przebywasz w tych godzinach.<br />
<br />
Po rejestracji w księgarni, zawsze masz dostęp do swoich danych i adresów, które możesz dowolnie modyfikować i dodawać kolejne.
";


$aConfig['lang']['mod_m_konta']['countries'][1] = "ALBANIA";
$aConfig['lang']['mod_m_konta']['countries'][2] = "ALGIERIA";
$aConfig['lang']['mod_m_konta']['countries'][4] = "ARGENTYNA";
$aConfig['lang']['mod_m_konta']['countries'][5] = "AUSTRALIA";
$aConfig['lang']['mod_m_konta']['countries'][6] = "AUSTRIA";
$aConfig['lang']['mod_m_konta']['countries'][7] = "BAHRAJN";
$aConfig['lang']['mod_m_konta']['countries'][8] = "BELGIA";
$aConfig['lang']['mod_m_konta']['countries'][10] = "BIRMA";
$aConfig['lang']['mod_m_konta']['countries'][12] = "BRAZYLIA";
$aConfig['lang']['mod_m_konta']['countries'][14] = "CHILE";
$aConfig['lang']['mod_m_konta']['countries'][15] = "CHINY";
$aConfig['lang']['mod_m_konta']['countries'][16] = "CHORWACJA";
$aConfig['lang']['mod_m_konta']['countries'][17] = "CYPR";
$aConfig['lang']['mod_m_konta']['countries'][18] = "CZECHY";
$aConfig['lang']['mod_m_konta']['countries'][19] = "DANIA";
$aConfig['lang']['mod_m_konta']['countries'][20] = "EGIPT";
$aConfig['lang']['mod_m_konta']['countries'][21] = "ERYTREA";
$aConfig['lang']['mod_m_konta']['countries'][22] = "ESTONIA";
$aConfig['lang']['mod_m_konta']['countries'][23] = "FINLANDIA";
$aConfig['lang']['mod_m_konta']['countries'][24] = "FRANCJA";
$aConfig['lang']['mod_m_konta']['countries'][25] = "GRECJA";
$aConfig['lang']['mod_m_konta']['countries'][26] = "GWATEMALA";
$aConfig['lang']['mod_m_konta']['countries'][27] = "HISZPANIA";
$aConfig['lang']['mod_m_konta']['countries'][28] = "HOLANDIA";
$aConfig['lang']['mod_m_konta']['countries'][29] = "HONGKONG";
$aConfig['lang']['mod_m_konta']['countries'][30] = "INDIE";
$aConfig['lang']['mod_m_konta']['countries'][31] = "INDONEZJA";
$aConfig['lang']['mod_m_konta']['countries'][32] = "IRLANDIA";
$aConfig['lang']['mod_m_konta']['countries'][33] = "ISLANDIA";
$aConfig['lang']['mod_m_konta']['countries'][34] = "IZRAEL";
$aConfig['lang']['mod_m_konta']['countries'][35] = "JAPONIA";
$aConfig['lang']['mod_m_konta']['countries'][36] = "JEMEN";
$aConfig['lang']['mod_m_konta']['countries'][37] = "JORDANIA";
$aConfig['lang']['mod_m_konta']['countries'][38] = "KANADA";
$aConfig['lang']['mod_m_konta']['countries'][39] = "KENIA";
$aConfig['lang']['mod_m_konta']['countries'][40] = "KOLUMBIA";
$aConfig['lang']['mod_m_konta']['countries'][42] = "KUWEJT";
$aConfig['lang']['mod_m_konta']['countries'][43] = "LIECHTENSTEIN";
$aConfig['lang']['mod_m_konta']['countries'][44] = "LITWA";
$aConfig['lang']['mod_m_konta']['countries'][45] = "LUKSEMBURG";
$aConfig['lang']['mod_m_konta']['countries'][47] = "MACEDONIA";
$aConfig['lang']['mod_m_konta']['countries'][48] = "MAJORKA";
$aConfig['lang']['mod_m_konta']['countries'][49] = "MALAYSIA";
$aConfig['lang']['mod_m_konta']['countries'][50] = "MALTA";
$aConfig['lang']['mod_m_konta']['countries'][51] = "MAROKO";
$aConfig['lang']['mod_m_konta']['countries'][52] = "MEKSYK";
$aConfig['lang']['mod_m_konta']['countries'][53] = "MONAKO";
$aConfig['lang']['mod_m_konta']['countries'][54] = "NIEMCY";
$aConfig['lang']['mod_m_konta']['countries'][55] = "NORWEGIA";
$aConfig['lang']['mod_m_konta']['countries'][57] = "OMAN";
$aConfig['lang']['mod_m_konta']['countries'][59] = "POLSKA";
$aConfig['lang']['mod_m_konta']['countries'][60] = "PORTUGALIA";
$aConfig['lang']['mod_m_konta']['countries'][62] = "ROSJA";
$aConfig['lang']['mod_m_konta']['countries'][63] = "RPA";
$aConfig['lang']['mod_m_konta']['countries'][64] = "RUMUNIA";
$aConfig['lang']['mod_m_konta']['countries'][67] = "SINGAPUR";
$aConfig['lang']['mod_m_konta']['countries'][70] = "SZWAJCARIA";
$aConfig['lang']['mod_m_konta']['countries'][71] = "SZWECJA";
$aConfig['lang']['mod_m_konta']['countries'][72] = "TAJLANDIA";
$aConfig['lang']['mod_m_konta']['countries'][73] = "TAJWAN";
$aConfig['lang']['mod_m_konta']['countries'][74] = "TANZANIA";
$aConfig['lang']['mod_m_konta']['countries'][75] = "TUNEZJA";
$aConfig['lang']['mod_m_konta']['countries'][76] = "TURCJA";
$aConfig['lang']['mod_m_konta']['countries'][77] = "UKRAINA";
$aConfig['lang']['mod_m_konta']['countries'][78] = "URUGWAJ";
$aConfig['lang']['mod_m_konta']['countries'][79] = "USA";
$aConfig['lang']['mod_m_konta']['countries'][80] = "WATYKAN";
$aConfig['lang']['mod_m_konta']['countries'][81] = "WENEZUELA";
?>