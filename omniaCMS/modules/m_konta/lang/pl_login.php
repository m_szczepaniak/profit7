<?php
/**
* Plik jezykowy modulu 'Konta uzytkownikow'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_konta_login']['logged_as'] = "ZALOGOWANY:";
$aConfig['lang']['mod_m_konta_login']['welcome'] = "Witaj,";
$aConfig['lang']['mod_m_konta_login']['logout'] = "&nbsp;WYLOGUJ&nbsp;";
$aConfig['lang']['mod_m_konta_login']['edit']	= '&nbsp;MOJE KONTO&nbsp;';
$aConfig['lang']['mod_m_konta_login']['dostep']	= 'Dostęp tylko dla zalogowanych użytkowników.';
$aConfig['lang']['mod_m_konta_login']['brak_konta']	= 'Jeśli nie masz konta kliknij ';
$aConfig['lang']['mod_m_konta_login']['tutaj']	= 'TUTAJ';
$aConfig['lang']['mod_m_konta_login']['zapomnialem'] = 'Zapomniałem hasła';
?>