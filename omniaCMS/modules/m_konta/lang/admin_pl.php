<?php
/**
* Plik jezykowy dla interfejsu modulu 'Konta uzytkownikow' Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/
/*---------- lista kont uzytkownikow */
$aConfig['lang']['m_konta']['f_active'] = "Konta";
$aConfig['lang']['m_konta']['f_all'] = "Wszystkie";
$aConfig['lang']['m_konta']['f_active_0'] = "Nieaktywne";
$aConfig['lang']['m_konta']['f_active_1'] = "Aktywne";
$aConfig['lang']['m_konta']['f_registred'] = 'Zarejestrowany';
$aConfig['lang']['m_konta']['f_registred_0'] = 'Nie';
$aConfig['lang']['m_konta']['f_registred_1'] = 'Tak';

$aConfig['lang']['m_konta']['f_privliges'] = "Uprawnienia";
$aConfig['lang']['m_konta']['f_privliges_any'] = "Z uprawnieniami";
$aConfig['lang']['m_konta']['f_privliges_1'] = "Z \"Przelew 14 dni\"";
$aConfig['lang']['m_konta']['f_privliges_2'] = "Z \"Odbiór osobisty\"";
$aConfig['lang']['m_konta']['f_privliges_3'] = "Z \"Zwolnienie z k. transportu\"";
$aConfig['lang']['m_konta']['f_privliges_4'] = "Z indywidualnym rabatem";
$aConfig['lang']['m_konta']['f_privliges_no'] = "Bez uprawnień";

$aConfig['lang']['m_konta']['f_users_deleted'] = "Konta usunięte";
$aConfig['lang']['m_konta']['f_users_email_denied'] = "Zgoda na mailing";
$aConfig['lang']['m_konta']['f_website_id'] = "Serwis";

$aConfig['lang']['m_konta']['f_type'] = "Typ";
$aConfig['lang']['m_konta']['f_type_0'] = "Zwykłe";
$aConfig['lang']['m_konta']['f_type_1'] = "KAM";
$aConfig['lang']['m_konta']['f_type_2'] = "HURT";

$aConfig['lang']['m_konta']['f_hidden'] = "Ukryte";
$aConfig['lang']['m_konta']['f_hidden_0'] = "Nie";
$aConfig['lang']['m_konta']['f_hidden_1'] = "Tak";

$aConfig['lang']['m_konta']['f_client_type'] = "Typ klienta";
$aConfig['lang']['m_konta']['f_client_type_0'] = "Osoba prywatna";
$aConfig['lang']['m_konta']['f_client_type_1'] = "Firma lub instytucja";

$aConfig['lang']['m_konta']['list'] = "Lista kont użytkowników";
$aConfig['lang']['m_konta']['list_login'] = "Login";
$aConfig['lang']['m_konta']['list_name'] = "Nazwa (firma)";
$aConfig['lang']['m_konta']['list_name_surname'] = "Nazwisko i Imię";
$aConfig['lang']['m_konta']['list_email'] = "Email";
$aConfig['lang']['m_konta']['list_registered'] = "Utworzone";
$aConfig['lang']['m_konta']['list_active'] = "Aktywne";

$aConfig['lang']['m_konta']['address'] = "Adresy użytkownika";
$aConfig['lang']['m_konta']['publishers_series_discount'] = "Rabat dla wydawcy i serii";
$aConfig['lang']['m_konta']['comments'] = "Komentarze użytkownika";
$aConfig['lang']['m_konta']['no_items'] = "Brak zdefiniowanych kont użytkowników";
$aConfig['lang']['m_konta']['send'] = "Wyślij wiadomość / powiadomienie";
$aConfig['lang']['m_konta']['send_group_discount_info'] = "Wyślij powiadomienie grupowo";
$aConfig['lang']['m_konta']['discount'] = "Ustawienia użytkownika";
$aConfig['lang']['m_konta']['activate'] = "Zmień stan aktywacji konta";
$aConfig['lang']['m_konta']['details'] = "Pokaż szczegóły";
$aConfig['lang']['m_konta']['group_discount'] = "Rabat grupowy";
$aConfig['lang']['m_konta']['delete'] = "Usuń użytkownika";
$aConfig['lang']['m_konta']['balance'] = "Zmień saldo";
$aConfig['lang']['m_konta']['delete_q'] = "Czy na pewno usunąć wybrane konto użytkownika?";
$aConfig['lang']['m_konta']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_konta']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_konta']['delete_all_q'] = "Czy na pewno usunąć wybrane konta użytkowników?";
$aConfig['lang']['m_konta']['delete_all_err'] = "Nie wybrano żadnego konta użytkownika do usunięcia!";
$aConfig['lang']['m_konta']['account_activation_ok']		= "Zmieniono stan aktywności konta użytkownika %s na \"%s\"";
$aConfig['lang']['m_konta']['account_activation_err']		= "Wystąpił błąd podczas próby zmiany stanu aktywacji konta użytkownika %s \"%s\"";
$aConfig['lang']['m_konta']['account_activation_0']		= "aktywne";
$aConfig['lang']['m_konta']['account_activation_1']		= "nieaktywne";
$aConfig['lang']['m_konta']['del_ok_0']		= "Usunięto konto użytkownika %s";
$aConfig['lang']['m_konta']['del_ok_1']		= "Usunięto konta użytkowników %s";
$aConfig['lang']['m_konta']['del_err_0']		= "Błąd przy próbie usunięcia konta użytkownika %s";
$aConfig['lang']['m_konta']['del_err_1']		= "Błąd przy próbie usunięcia kont użytkowników %s";

// szczegoly konta uzytkownika
$aConfig['lang']['m_konta']['account_details_header'] = 'Szczegóły konta użytkownika "%s" (%s)';
$aConfig['lang']['m_konta']['details_id'] = 'ID:';
$aConfig['lang']['m_konta']['details_login_section'] = 'Dane logowania';
$aConfig['lang']['m_konta']['details_login'] = 'Login:';
$aConfig['lang']['m_konta']['details_passwd'] = 'Hasło:';
$aConfig['lang']['m_konta']['details_user_section'] = 'Dane użytkownika';
$aConfig['lang']['m_konta']['details_invoice_section'] = 'Dane do faktury';
$aConfig['lang']['m_konta']['details_name_company'] = 'Nazwa (firma):';
$aConfig['lang']['m_konta']['details_email'] = 'Adres e-mail:';
$aConfig['lang']['m_konta']['details_name'] = 'Imię:';
$aConfig['lang']['m_konta']['details_surname'] = 'Nazwisko:';
$aConfig['lang']['m_konta']['details_country'] = 'Kraj:';
$aConfig['lang']['m_konta']['details_street'] = 'Ulica, nr:';
$aConfig['lang']['m_konta']['details_postal'] = 'Kod pocztowy:';
$aConfig['lang']['m_konta']['details_city'] = 'Miasto:';
$aConfig['lang']['m_konta']['details_phone'] = 'Telefon:';
$aConfig['lang']['m_konta']['details_cellular_phone'] = 'Telefon komórkowy:';
$aConfig['lang']['m_konta']['details_nip'] = 'NIP:';
$aConfig['lang']['m_konta']['details_regulations_agreement'] = 'Zgoda z regulaminem:';
$aConfig['lang']['m_konta']['details_privacy_agreement'] = 'Zgoda na przetwarzanie danych:';
$aConfig['lang']['m_konta']['details_registered'] = 'Utworzono:';
$aConfig['lang']['m_konta']['details_active'] = 'Konto aktywne:';
$aConfig['lang']['m_konta']['details_company_section'] = 'Dane firmowe (płatnika)';
$aConfig['lang']['m_konta']['details_company'] = 'Firma:';
$aConfig['lang']['m_konta']['details_other_section'] = 'Data rejestracji, typ konta';
$aConfig['lang']['m_konta']['details_type'] = 'Typ konta:';
$aConfig['lang']['m_konta']['details_type_company'] = 'Firma lub instytucja';
$aConfig['lang']['m_konta']['details_type_private'] = 'Osoba prywatna';
$aConfig['lang']['m_konta']['details_client_type'] = 'Typ klienta:';
$aConfig['lang']['m_konta']['details_discount_section'] = 'Indywidualny rabat';
$aConfig['lang']['m_konta']['details_discount'] = 'Przyznany rabat';
$aConfig['lang']['m_konta']['details_discount_untill'] = 'Rabat do';
$aConfig['lang']['m_konta']['details_discount_to'] = 'Rabat nadano';
$aConfig['lang']['m_konta']['details_no_data'] = '-- brak danych --';
$aConfig['lang']['m_konta']['details_no_discount'] = '-- nie przyznano --';
$aConfig['lang']['m_konta']['details_not_set'] = '-- nie ustawiono --';
$aConfig['lang']['m_konta']['details_discount_code'] = 'Kod rabatowy';
$aConfig['lang']['m_konta']['details_discount_code_value'] = 'Zniżka kodu rabatowego';
$aConfig['lang']['m_konta']['allow_14days'] = 'Przelew 14 dni';
$aConfig['lang']['m_konta']['allow_personal_reception'] = 'Odbiór osobisty';
$aConfig['lang']['m_konta']['allow_free_transport'] = 'Zwolnienie z kosztów transportu';
$aConfig['lang']['m_konta']['details_user_address'] = 'Adres: "%s"';
$aConfig['lang']['m_konta']['details_default'] = 'domyślny:';
$aConfig['lang']['m_konta']['details_default_invoice'] = 'dla faktury';
$aConfig['lang']['m_konta']['details_default_transport'] = 'dla transportu';
$aConfig['lang']['m_konta']['details_created'] = 'Utworzono:';
$aConfig['lang']['m_konta']['details_modified'] = 'Zmodyfikowano:';
$aConfig['lang']['m_konta']['details_balance'] = 'Saldo konta:';
$aConfig['lang']['m_konta']['promotions_agreement'] = "Wysyłaj mailingi okazjonalne";
$aConfig['lang']['m_konta']['details_edit_ok'] = 'Zmieniono ustawienia użytkownika %s ';
$aConfig['lang']['m_konta']['details_edit_err'] = 'Wystąpił błąd podczas próby zmiany ustawień użytkownika %s!<br>Spróbuj ponownie';

$aConfig['lang']['m_konta']['countries'][1] = "ALBANIA";
$aConfig['lang']['m_konta']['countries'][2] = "ALGIERIA";
$aConfig['lang']['m_konta']['countries'][4] = "ARGENTYNA";
$aConfig['lang']['m_konta']['countries'][5] = "AUSTRALIA";
$aConfig['lang']['m_konta']['countries'][6] = "AUSTRIA";
$aConfig['lang']['m_konta']['countries'][7] = "BAHRAJN";
$aConfig['lang']['m_konta']['countries'][8] = "BELGIA";
$aConfig['lang']['m_konta']['countries'][10] = "BIRMA";
$aConfig['lang']['m_konta']['countries'][12] = "BRAZYLIA";
$aConfig['lang']['m_konta']['countries'][14] = "CHILE";
$aConfig['lang']['m_konta']['countries'][15] = "CHINY";
$aConfig['lang']['m_konta']['countries'][16] = "CHORWACJA";
$aConfig['lang']['m_konta']['countries'][17] = "CYPR";
$aConfig['lang']['m_konta']['countries'][18] = "CZECHY";
$aConfig['lang']['m_konta']['countries'][19] = "DANIA";
$aConfig['lang']['m_konta']['countries'][20] = "EGIPT";
$aConfig['lang']['m_konta']['countries'][21] = "ERYTREA";
$aConfig['lang']['m_konta']['countries'][22] = "ESTONIA";
$aConfig['lang']['m_konta']['countries'][23] = "FINLANDIA";
$aConfig['lang']['m_konta']['countries'][24] = "FRANCJA";
$aConfig['lang']['m_konta']['countries'][25] = "GRECJA";
$aConfig['lang']['m_konta']['countries'][26] = "GWATEMALA";
$aConfig['lang']['m_konta']['countries'][27] = "HISZPANIA";
$aConfig['lang']['m_konta']['countries'][28] = "HOLANDIA";
$aConfig['lang']['m_konta']['countries'][29] = "HONGKONG";
$aConfig['lang']['m_konta']['countries'][30] = "INDIE";
$aConfig['lang']['m_konta']['countries'][31] = "INDONEZJA";
$aConfig['lang']['m_konta']['countries'][32] = "IRLANDIA";
$aConfig['lang']['m_konta']['countries'][33] = "ISLANDIA";
$aConfig['lang']['m_konta']['countries'][34] = "IZRAEL";
$aConfig['lang']['m_konta']['countries'][35] = "JAPONIA";
$aConfig['lang']['m_konta']['countries'][36] = "JEMEN";
$aConfig['lang']['m_konta']['countries'][37] = "JORDANIA";
$aConfig['lang']['m_konta']['countries'][38] = "KANADA";
$aConfig['lang']['m_konta']['countries'][39] = "KENIA";
$aConfig['lang']['m_konta']['countries'][40] = "KOLUMBIA";
$aConfig['lang']['m_konta']['countries'][42] = "KUWEJT";
$aConfig['lang']['m_konta']['countries'][43] = "LIECHTENSTEIN";
$aConfig['lang']['m_konta']['countries'][44] = "LITWA";
$aConfig['lang']['m_konta']['countries'][45] = "LUKSEMBURG";
$aConfig['lang']['m_konta']['countries'][47] = "MACEDONIA";
$aConfig['lang']['m_konta']['countries'][48] = "MAJORKA";
$aConfig['lang']['m_konta']['countries'][49] = "MALAYSIA";
$aConfig['lang']['m_konta']['countries'][50] = "MALTA";
$aConfig['lang']['m_konta']['countries'][51] = "MAROKO";
$aConfig['lang']['m_konta']['countries'][52] = "MEKSYK";
$aConfig['lang']['m_konta']['countries'][53] = "MONAKO";
$aConfig['lang']['m_konta']['countries'][54] = "NIEMCY";
$aConfig['lang']['m_konta']['countries'][55] = "NORWEGIA";
$aConfig['lang']['m_konta']['countries'][57] = "OMAN";
$aConfig['lang']['m_konta']['countries'][59] = "POLSKA";
$aConfig['lang']['m_konta']['countries'][60] = "PORTUGALIA";
$aConfig['lang']['m_konta']['countries'][62] = "ROSJA";
$aConfig['lang']['m_konta']['countries'][63] = "RPA";
$aConfig['lang']['m_konta']['countries'][64] = "RUMUNIA";
$aConfig['lang']['m_konta']['countries'][67] = "SINGAPUR";
$aConfig['lang']['m_konta']['countries'][70] = "SZWAJCARIA";
$aConfig['lang']['m_konta']['countries'][71] = "SZWECJA";
$aConfig['lang']['m_konta']['countries'][72] = "TAJLANDIA";
$aConfig['lang']['m_konta']['countries'][73] = "TAJWAN";
$aConfig['lang']['m_konta']['countries'][74] = "TANZANIA";
$aConfig['lang']['m_konta']['countries'][75] = "TUNEZJA";
$aConfig['lang']['m_konta']['countries'][76] = "TURCJA";
$aConfig['lang']['m_konta']['countries'][77] = "UKRAINA";
$aConfig['lang']['m_konta']['countries'][78] = "URUGWAJ";
$aConfig['lang']['m_konta']['countries'][79] = "USA";
$aConfig['lang']['m_konta']['countries'][80] = "WATYKAN";
$aConfig['lang']['m_konta']['countries'][81] = "WENEZUELA";

// rabat dla wydawnictw i serii wyd.

$aConfig['lang']['m_konta_publishers_series_discount']['page_header'] = 'Rabaty przynane dla użytkownika < %s >';
$aConfig['lang']['m_konta_publishers_series_discount']['linkedDiscount_header'] = 'Rabat dla wydawcy i serii';
$aConfig['lang']['m_konta_publishers_series_discount']['publisher'] = 'Wydawnictwo';
$aConfig['lang']['m_konta_publishers_series_discount']['series'] = 'Seria wydawnicza';
$aConfig['lang']['m_konta_publishers_series_discount']['series_discount'] = 'Rabat (%)';
$aConfig['lang']['m_konta_publishers_series_discount']['publisher_discount'] = 'Rabat (%)';
$aConfig['lang']['m_konta_publishers_series_discount']['add_series'] = 'Dodaj nową serię';
$aConfig['lang']['m_konta_publishers_series_discount']['edit_publisher'] = 'Modifikuj rabat wydawnictwa';
$aConfig['lang']['m_konta_publishers_series_discount']['add'] = 'Dodaj nowe wydawnictwo';
$aConfig['lang']['m_konta_publishers_series_discount']['discount'] = 'Kopiuj ustawienia';
$aConfig['lang']['m_konta_publishers_series_discount']['edit_series'] = 'Modifikuj rabat na serie';
$aConfig['lang']['m_konta_publishers_series_discount']['delete_series'] = 'Usuń rabat na serię';
$aConfig['lang']['m_konta_publishers_series_discount']['delete_publisher'] = 'Usuń rabat na wydawnictwo i serie';
$aConfig['lang']['m_konta_publishers_series_discount']['publisher_discount_edit_ok'] = 'Pomyślnie zmodyfikowano rabat dla wydawnictwa: %s';
$aConfig['lang']['m_konta_publishers_series_discount']['publisher_discount_edit_err'] = 'Nie udało się zmofidykować rabatu dla wydawnictwa: %s';
$aConfig['lang']['m_konta_publishers_series_discount']['series_discount_edit_ok'] = 'Pomyślnie zmodyfikowano rabat dla serii: %s';
$aConfig['lang']['m_konta_publishers_series_discount']['series_discount_edit_err'] = 'Nie udało się zmofidykować rabatu dla serii: %s';

$aConfig['lang']['m_konta_publishers_series_discount']['publisher_discount_edit_seller_ok'] = 'Pomyślnie zmodyfikowano rabat dla wydawnictwa: %s dla wszystkich użytkowników handlowca %s';
$aConfig['lang']['m_konta_publishers_series_discount']['publisher_discount_edit_seller_err'] = 'Nie udało się zmofidykować rabatu dla wydawnictwa: %s w użytkownikach handlowca %s';
$aConfig['lang']['m_konta_publishers_series_discount']['series_discount_edit_seller_ok'] = 'Pomyślnie zmodyfikowano rabat dla serii: %s  dla wszystkich użytkowników handlowca %s';
$aConfig['lang']['m_konta_publishers_series_discount']['series_discount_edit_seller_err'] = 'Nie udało się zmofidykować rabatu dla serii: %s  dla wszystkich użytkowników handlowca %s';

$aConfig['lang']['m_konta_publishers_series_discount']['del_err_publisher'] = 'Nie udało sie usunąć wydawnictwa';
$aConfig['lang']['m_konta_publishers_series_discount']['del_err_series'] = 'Nie udało sie usunąć serii';
$aConfig['lang']['m_konta_publishers_series_discount']['del_err_both'] = 'Nie udało sie usunąć wydawnictwa oraz serii';
$aConfig['lang']['m_konta_publishers_series_discount']['del_success_both'] = 'Pomyślnie usunięto wydawnictwo oraz serie';
$aConfig['lang']['m_konta_publishers_series_discount']['del_success_series'] = 'Pomyślnie usunięto serię: %s';
$aConfig['lang']['m_konta_publishers_series_discount']['del_success_publisher'] = 'Pomyślnie usunięto wydawnictwo';
$aConfig['lang']['m_konta_publishers_series_discount']['publisher_exists'] = 'Podane wydawnictwo jest już na liście';
$aConfig['lang']['m_konta_publishers_series_discount']['publisher_invalid_name'] = 'Wprowadzono nieprawidłową nazwę wydawnictwa';
$aConfig['lang']['m_konta_publishers_series_discount']['series_exists'] = 'Podana seria jest już na lisćie';
$aConfig['lang']['m_konta_publishers_series_discount']['f_adress_name'] = 'Dane do faktury';
$aConfig['lang']['m_konta_publishers_series_discount']['no_user_adress'] = 'Brak zdefiniowanych danych do faktury dla danego użytkownika';


$aConfig['lang']['m_konta_publishers_series_discount']['publisher_series'] = 'Wydawnictwo / seria wyd.';
$aConfig['lang']['m_konta_publishers_series_discount']['user_publisher_series_discount'] = 'Rabat';
$aConfig['lang']['m_konta_publishers_series_discount']['created_date'] = 'Utworzono';
$aConfig['lang']['m_konta_publishers_series_discount']['created_by'] = 'Zmodyfikowane przez';
$aConfig['lang']['m_konta_publishers_series_discount']['action'] = 'Akcje';
$aConfig['lang']['m_konta_publishers_series_discount']['add_publisher_series_discount'] = 'Modyfikuj serie: ';
$aConfig['lang']['m_konta_publishers_series_discount']['add_publisher_ok'] = 'Pomyślnie przypisano rabat na wydawnictwo';
$aConfig['lang']['m_konta_publishers_series_discount']['add_publisher_err'] = 'Nie udało się przypisać rabatu na wydawnictwo';
$aConfig['lang']['m_konta_publishers_series_discount']['add_series_ok'] = 'Pomyślnie przypisano rabat na serie wydawnicze';
$aConfig['lang']['m_konta_publishers_series_discount']['add_series_err'] = 'Nie udało się przypisać rabatu na serie wydawnicze';
$aConfig['lang']['m_konta_publishers_series_discount']['publisher_add_header'] = 'Dodaj nowe wydawnictwo';
$aConfig['lang']['m_konta_publishers_series_discount']['publisher_edit_header'] = 'Modyfikuj rabat wydawnictwa';

// rabat uzytkownika
$aConfig['lang']['m_konta']['discount_header'] = 'Ustawienia użytkownika "%s"';
$aConfig['lang']['m_konta']['discount_amount'] = 'Rabat (%)';
$aConfig['lang']['m_konta']['discount_untill'] = 'Obowiązuje do (włącznie)';
$aConfig['lang']['m_konta']['discount_edit_ok'] = "Zmieniono ustawienia użytkownika %s";
$aConfig['lang']['m_konta']['discount_edit_err'] = "Wystąpił błąd podczas próby zmiany ustawień użytkownika %s!<br>Spróbuj ponownie";

// saldo uzytkownika
$aConfig['lang']['m_konta']['balance_header'] = 'Saldo użytkownika "%s"';
$aConfig['lang']['m_konta']['balance_value'] = 'Saldo';
$aConfig['lang']['m_konta']['balance_description'] = 'Opis zmiany';
$aConfig['lang']['m_konta']['balance_edit_ok'] = "Ustawiono saldo użytkownika %s w wysokości %s";
$aConfig['lang']['m_konta']['balance_edit_err'] = "Wystąpił błąd podczas próby ustawienia salda użytkownika %s!<br>Spróbuj ponownie";

// rabat grupowy
$aConfig['lang']['m_konta']['group_discount_header'] = 'Rabat grupowy użytkowników';
$aConfig['lang']['m_konta']['conditions_section'] = 'Warunki dla użytkowników';
$aConfig['lang']['m_konta']['name_includes'] = 'Nazwa zawiera';
$aConfig['lang']['m_konta']['email_includes'] = 'Email zawiera';
$aConfig['lang']['m_konta']['value_range'] = 'Wartość złożonych zamówień';
$aConfig['lang']['m_konta']['value_to'] = 'Do';
$aConfig['lang']['m_konta']['date_range'] = 'Przedział czasowy składanych zamówień';
$aConfig['lang']['m_konta']['date_to'] = 'Do';
$aConfig['lang']['m_konta']['or'] = 'lub';
$aConfig['lang']['m_konta']['button_show_users'] = "wyszukaj";
$aConfig['lang']['m_konta']['current_discount'] = 'Aktualny rabat';
$aConfig['lang']['m_konta']['new_discount_section'] = 'Przyznany rabat';
$aConfig['lang']['m_konta']['button_add_group_discount'] = 'Nadaj rabat';
$aConfig['lang']['m_konta']['users_list_id'] = 'ID';
$aConfig['lang']['m_konta']['users_list_name'] = 'Nazwa';
$aConfig['lang']['m_konta']['users_list_login'] = 'Login';
$aConfig['lang']['m_konta']['users_list_email'] = 'Email';
$aConfig['lang']['m_konta']['users_list_discount'] = 'Rabat';
$aConfig['lang']['m_konta']['users_list_orders_value'] = 'Wartość zamówień';
$aConfig['lang']['m_konta']['no_users_to_show'] = "Brak użytkowników spełniających zadane kryteria";
$aConfig['lang']['m_konta']['no_users_checked_err'] = 'Nie wybrano żadnego użytkownika dla którego ma zostać ustawiony rabat!';
$aConfig['lang']['m_konta']['log_date'] = "Data nadania rabatu: %s\n";
$aConfig['lang']['m_konta']['log_discount'] = "Nadany rabat: %s%%\n\n";
$aConfig['lang']['m_konta']['log_conditions'] = "WARUNKI NADANIA RABATU\n";
$aConfig['lang']['m_konta']['log_name_includes'] = "Nazwa zawiera: \"%s\"\n";
$aConfig['lang']['m_konta']['log_value_range'] = "Wartość zamówień z przedziału: od %s zł do %s zł\n";
$aConfig['lang']['m_konta']['log_date_range'] = "Zamówienia złożone w okresie: od %s do %s\n";
$aConfig['lang']['m_konta']['log_email_includes'] = "Adres email zawiera: \"%s\"\n";
$aConfig['lang']['m_konta']['log_current_discount'] = "Posiada dotychczasowy rabat: %s%%\n";
$aConfig['lang']['m_konta']['log_users_header'] = "ID\tNAZWA\tLOGIN\tEMAIL\tDOTYCHCZASOWY RABAT\n";
$aConfig['lang']['m_konta']['log_user_data'] = "[ ID:%d ]\t%s\t%s\t%s\t%s%%\n";

$aConfig['lang']['m_konta']['group_discount_edit_ok'] = 'Ustawiono rabat grupowy w wysokosci %s%%.<br>Szczegółowe informacje w piku log: %s';
$aConfig['lang']['m_konta']['group_discount_edit_err'] = 'Wystąpił błąd podczas próby ustawienia rabatu grupowego w wysokosci %s%%!<br>Spróbuj ponownie';

// wysylka wiadomosci email
$aConfig['lang']['m_konta']['send_user_header'] = "Tworzenie wiadomości do użytkownika %s";
$aConfig['lang']['m_konta']['subject'] = "Temat";
$aConfig['lang']['m_konta']['include_discount'] = "Załącz informację o rabacie";
$aConfig['lang']['m_konta']['include_discount_info'] = "<strong>Uwaga!</strong><br />Aby w wybranym miejscu wiadomości pojawiła się wartość rabatu użytkownika wstaw znacznik {rabat}.<br>Zostanie on automatycznie zamieniony na odpowiednią wartość.";
$aConfig['lang']['m_konta']['body_txt'] = "Treść TXT";
$aConfig['lang']['m_konta']['body_html'] = "Treść HTML";
$aConfig['lang']['m_konta']['send_button'] = "Wyślij wiadomość";
$aConfig['lang']['m_konta']['no_body_err'] = 'Nie wprowadzono treści wiadomości lub w jej treści nie znaleziono znacznika {rabat}!';
$aConfig['lang']['m_konta']['no_plain_body_err'] = 'Nie wprowadzono treści wiadomości!';
$aConfig['lang']['m_konta']['log_send_date'] = "Data rozsyłki powiadomienia o rabacie: %s\n";
$aConfig['lang']['m_konta']['send_user_msg_ok'] = "Wysłano wiadomość \"%s\" do użytkownika %s";
$aConfig['lang']['m_konta']['send_user_msg_err'] = "Nie udało się wysłać wiadomości \"%s\" do użytkownika %s!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_konta']['send_group_message_header'] = "Tworzenie wiadomości do grupy użytkowników";
$aConfig['lang']['m_konta']['log_file'] = "Plik logu";
$aConfig['lang']['m_konta']['send_group_no_ids'] = "Nie znaleziono informacji o użytkownikach w pliku log \"%s\"!";
$aConfig['lang']['m_konta']['send_group_msg_ok'] = "Wysłano wiadomość do %d użytkowników z pliku log \"%s\". Informacje zapisano do pliku log \"%s\"";
$aConfig['lang']['m_konta']['send_group_msg_err'] = "Nie udało się wysłać wiadomości do żadnego użytkownika z pliku log \"%s\"!<br><br>Spróbuj ponownie";

/*---------- ustawienia boksu konta uzytkownikow */
$aConfig['lang']['m_konta_box_settings']['page'] = "Strona modułu";

/*---------- logi rabatow */
$aConfig['lang']['m_konta_discount_logs']['list'] = "Lista logów rabatów grupowych";
$aConfig['lang']['m_konta_discount_logs']['list_file'] = "Nazwa logu";
$aConfig['lang']['m_konta_discount_logs']['list_date'] = "Data nadania";
$aConfig['lang']['m_konta_discount_logs']['list_discount'] = "Nadany rabat (%)";

$aConfig['lang']['m_konta_discount_logs']['no_items'] = "Brak wygenerowanych logów rabatów grupowych";
$aConfig['lang']['m_konta_discount_logs']['details'] = "Pokaż log rabatów";
$aConfig['lang']['m_konta_discount_logs']['delete'] = "Usuń log rabatów";
$aConfig['lang']['m_konta_discount_logs']['delete_q'] = "Czy na pewno usunąć wybrany log?";
$aConfig['lang']['m_konta_discount_logs']['del_ok']		= "Usunięto log rabatów grupowych \"%s\"";
$aConfig['lang']['m_konta_discount_logs']['del_err']		= "Błąd przy próbie usunięcia logu rabatów grupowych %s";

$aConfig['lang']['m_konta_discount_logs']['discount_log_details_header']		= "Szczegóły logu rabatów \"%s\"";

/*---------- logi rozsylki powiadomien o rabacie */
$aConfig['lang']['m_konta_email_logs']['list'] = "Lista logów rozsyłki grupowej powiadomiań o rabacie";
$aConfig['lang']['m_konta_email_logs']['list_file'] = "Nazwa logu";
$aConfig['lang']['m_konta_email_logs']['list_date'] = "Data rozsyłki";
$aConfig['lang']['m_konta_email_logs']['list_discount'] = "Nadany rabat (%)";

$aConfig['lang']['m_konta_email_logs']['no_items'] = "Brak wygenerowanych logów rozsyłki grupowej powiadomień o rabacie";
$aConfig['lang']['m_konta_email_logs']['details'] = "Pokaż log rozsyłki";
$aConfig['lang']['m_konta_email_logs']['delete'] = "Usuń log rozsyłki";
$aConfig['lang']['m_konta_email_logs']['delete_q'] = "Czy na pewno usunąć wybrany log?";
$aConfig['lang']['m_konta_email_logs']['del_ok']		= "Usunięto log rozsyłki grupowej powiadomień o rabacie \"%s\"";
$aConfig['lang']['m_konta_email_logs']['del_err']		= "Błąd przy próbie usunięcia logu rozsyłki grupowej powiadomień o rabacie %s";

$aConfig['lang']['m_konta_email_logs']['discount_log_details_header']		= "Szczegóły logu rozsyłki \"%s\"";

/*---------- ustawienia kont uzytkownikow */
$aConfig['lang']['m_konta_settings']['template'] = "Szablon";
$aConfig['lang']['m_konta_settings']['ftext'] = "Tekst w panelu użytkownika";
$aConfig['lang']['m_konta_settings']['form_settings'] = "Konfiguracja formularza rejestracji użytkownika";
$aConfig['lang']['m_konta_settings']['radio_dont_show'] = "Nie wyświetlaj";
$aConfig['lang']['m_konta_settings']['radio_show'] = "Wyświetlaj";
$aConfig['lang']['m_konta_settings']['radio_required'] = "Pola wymagane";
$aConfig['lang']['m_konta_settings']['radio_not_required'] = "Pola niewymagane";
$aConfig['lang']['m_konta_settings']['radio_required2'] = "Zgoda wymagana";
$aConfig['lang']['m_konta_settings']['radio_not_required2'] = "Zgoda niewymagana";
$aConfig['lang']['m_konta_settings']['radio_required3'] = "Pole wymagane";
$aConfig['lang']['m_konta_settings']['radio_not_required3'] = "Pole niewymagane";
$aConfig['lang']['m_konta_settings']['radio_depends_on'] = "Zależne od wyboru firma / osoba prywatna";
$aConfig['lang']['m_konta_settings']['address_data'] = "Dane adresowe";
$aConfig['lang']['m_konta_settings']['name_data'] = "Dane osobowe";
$aConfig['lang']['m_konta_settings']['country_data'] = "Kraj";
$aConfig['lang']['m_konta_settings']['phone_data'] = "Telefon";
$aConfig['lang']['m_konta_settings']['fax_data'] = "Fax";
$aConfig['lang']['m_konta_settings']['company_data'] = "Dane firmowe";
$aConfig['lang']['m_konta_settings']['regulations_agreement'] = "Warunki regulaminu";
$aConfig['lang']['m_konta_settings']['regulations_page'] = "Strona z treścią regulaminu";
$aConfig['lang']['m_konta_settings']['privacy_agreement'] = "Zgoda na przetwarzanie danych";
$aConfig['lang']['m_konta_settings']['privacy_text'] = "Treść zgody";
$aConfig['lang']['m_konta_settings']['newsletter'] = "Newsletter";
$aConfig['lang']['m_konta_settings']['avatar'] = "Avatar forum";
$aConfig['lang']['m_konta_settings']['items_per_page'] = "Elementów na stronie";

$aConfig['lang']['m_konta_settings']['header'] = "Edycja ustawień modułu \"".$aConfig['lang']['m_konta']['users']."\"";
$aConfig['lang']['m_konta_settings']['require_confirmation'] = "Konto aktywne po potwierdzeniu przez użytkownika";
$aConfig['lang']['m_konta_settings']['user_articles_per_page'] = "Liczba artykułów na stronie użytkownika";
$aConfig['lang']['m_konta_settings']['edit_ok'] = "Zmiany w ustawieniach modułu \"".$aConfig['lang']['m_konta']['users']."\" zostały wprowadzone do bazy";
$aConfig['lang']['m_konta_settings']['edit_err'] = "Nie udało się wprowadzić zmian w ustawieniach modułu \"".$aConfig['lang']['m_konta']['users']."\"!<br><br>Spróbuj ponownie";

/** ADRESY UZYTKOWNIKA **/
$aConfig['lang']['m_konta_user_address_list']['list'] = 'Adresy użytkownika "%s"';
$aConfig['lang']['m_konta_user_address_list']['list_address_name'] = 'Nazwa adresu';
$aConfig['lang']['m_konta_user_address_list']['list_is_company'] = 'Firma/Osoba prywatna';
$aConfig['lang']['m_konta_user_address_list']['list_default_invoice'] = 'Domyślne dane do faktury';
$aConfig['lang']['m_konta_user_address_list']['list_default_transport'] = 'Domyślne dane do wysyłki';
$aConfig['lang']['m_konta_user_address_list']['list_created'] = 'Utworzono';
$aConfig['lang']['m_konta_user_address_list']['list_modified'] = 'Zmodyfikowano';
$aConfig['lang']['m_konta_user_address_list']['is_company_1'] = 'Firma';
$aConfig['lang']['m_konta_user_address_list']['is_company_0'] = 'Osoba prywatna';
$aConfig['lang']['m_konta_user_address_list']['go_back'] = 'Powrót na listę użytkowników';
$aConfig['lang']['m_konta_user_address_list']['edit'] = 'Edytuj adres użytkownika';

$aConfig['lang']['m_konta_user_address_list']['header'] = 'Edycja adresu "%s" użytkownika "%s"';
$aConfig['lang']['m_konta_user_address_list']['is_company'] = 'Firma';
$aConfig['lang']['m_konta_user_address_list']['company'] = 'Nazwa firmy';
$aConfig['lang']['m_konta_user_address_list']['name'] = 'Imię';
$aConfig['lang']['m_konta_user_address_list']['surname'] = 'Nazwisko';
$aConfig['lang']['m_konta_user_address_list']['street'] = 'Ulica';
$aConfig['lang']['m_konta_user_address_list']['number'] = 'Nr domu';
$aConfig['lang']['m_konta_user_address_list']['number2'] = 'Nr mieszkania';
$aConfig['lang']['m_konta_user_address_list']['postal'] = 'Kod pocztowy';
$aConfig['lang']['m_konta_user_address_list']['city'] = 'Miejscowość';
$aConfig['lang']['m_konta_user_address_list']['nip'] = 'NIP';
$aConfig['lang']['m_konta_user_address_list']['phone'] = 'Telefon';
$aConfig['lang']['m_konta_user_address_list']['created'] = 'Utworzono';
$aConfig['lang']['m_konta_user_address_list']['modified'] = 'Zmodyfikowano';
$aConfig['lang']['m_konta_user_address_list']['default_invoice'] = 'Domyślne dane do faktury';
$aConfig['lang']['m_konta_user_address_list']['default_transport'] = 'Domyślne dane do wysyłki';
$aConfig['lang']['m_konta_user_address_list']['edit_ok'] = 'Zmieniono dane adresowe "%s" użytkownika "%s"';
$aConfig['lang']['m_konta_user_address_list']['edit_err'] = 'Wystąpił błąd podczas zmiany danych adresowych "%s" użytkownika "%s"';

/** KOMENTARZE UZYTKOWNIKA **/
$aConfig['lang']['m_konta_comments']['list'] = 'Komentarze użytkownika "%s"';
$aConfig['lang']['m_konta_comments']['add'] = 'Dodaj nowy komentarz';
$aConfig['lang']['m_konta_comments']['header'] = 'Dodawanie komentarza użytkownika "%s"';
$aConfig['lang']['m_konta_comments']['comment'] = 'Komentarz';
$aConfig['lang']['m_konta_comments']['add_ok'] = 'Dodano komentarz do użytkownika "%s"';
$aConfig['lang']['m_konta_comments']['add_err'] = 'Wystąpił błąd podczas dodawania komentarz do użytkownika "%s"';
$aConfig['lang']['m_konta_comments']['go_back'] = $aConfig['lang']['common']['go_back'];
$aConfig['lang']['m_konta_comments']['no_items'] = $aConfig['lang']['common']['no_items'];

$aConfig['lang']['m_konta']['user_name']			= "Nazwa użytkownika";
$aConfig['lang']['m_konta']['surname']				= "Nazwisko";
$aConfig['lang']['m_konta']['name']						= "Imię";
$aConfig['lang']['m_konta']['group']						= "Grupa";
?>
