<?php

/*
 * Klasa zarządzająca rabatami na wydawnictwa i serie wyd.
 * 
 * @author Marcin Janowski <janowski@windowslive.com>
 * @created 2015-05-07 
 * @copyrights Marcin Chudy - Profit24.pl
 */

include_once('Konta_common.class.php');

class Module extends Konta_common {

  // komunikat
  public $sMsg;
  // nazwa modulu - do langow
  public $sModule;
  // ID wersji jezykowej
  public $iLangId;
  // uprawnienia uzytkownika dla wersji jez.
  public $aPrivileges;
  // Id strony modulu
  public $iPageId;
  // id uzytkownika
  public $iUId;
  // id adresu użytkownika
  private $iUserAdressId;

    private $pDbMgr;

  function __construct(&$pSmarty) {
      global $pDbMgr;
      $this->pDbMgr = $pDbMgr;

    $this->iLangId = $_SESSION['lang']['id'];
    $_GET['lang_id'] = $this->iLangId;
    $this->sModule = $_GET['module'] . '_' . $_GET['action'];
    $this->iPageId = getModulePageId('m_konta');
    
    if($_POST['f_adress_name']!=''){
      $this->iUserAdressId = $_POST['f_adress_name'];
    }
    elseif (isset($_GET['user_adress_id'])) {
      $this->iUserAdressId = $_GET['user_adress_id'];
    }
    if ($this->iUserAdressId <= 0) {
        // workaround, jeśli nie wybrano FV
        $this->iUserAdressId = 262404;
    }

    $sDo = '';

    if (isset($_GET['do'])) {
      $sDo = $_GET['do'];
    }
    if (isset($_POST['do'])) {
      $sDo = $_POST['do'];
    }

    if (isset($_GET['pid'])) {
      $this->iUId = $_GET['pid'];
    }

    if (isset($_GET['id'])) {
      $iId = $_GET['id'];
    }

    if (isset($_GET['selected'])) {
      $iSelectedId = $_GET['selected'];
    }
    

    $this->aPrivileges = & $_SESSION['user']['privileges'];
    // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
    if ($_SESSION['user']['type'] === 0) {
      if (!hasModulePrivileges($_GET['module_id'])) {
        showPrivsAlert($pSmarty);
        return;
      }
    }

    switch ($sDo) {
      case 'edit_publisher':
        $this->AddEditPublisher($pSmarty, $iId, true);
        break;
      case 'add':
        $this->AddEditPublisher($pSmarty, $iId, false);
        break;
      case 'edit_series':
        $this->AddEditSeries($pSmarty, $iId, true, $iSelectedId);
        break;
      case 'add_series':
        $this->AddEditSeries($pSmarty, $iId, false);
        break;
      case 'delete_publisher':
        $this->DeletePublisher($pSmarty, $iId);
        break;
      case 'delete_series':
        $this->DeleteSeries($pSmarty, $iId, $iSelectedId);
        break;
      case 'insert_publisher':
        $this->InsertPublisher($pSmarty, $iId);
        break;
      case 'insert_series':
        $this->InsertSeries($pSmarty, $iId);
        break;
      case 'update_publisher':
        $this->UpdatePublisher($pSmarty, $iId);
        break;
      case 'update_series':
        $this->UpdateSeries($pSmarty, $iId);
        break;
        case 'discount':
            $this->copySettingsForm($pSmarty, $iId);
        break;
        case 'do_copy_settings':
            $this->doCopySettings($pSmarty, $iId);
        break;
      default: $this->Show($pSmarty);
        break;
    }
  }
// end of __construct() method

  /**
   * Metoda wyświetla listę przynanych rabatów 
   * 
   * @global type $aConfig
   * @param type $pSmarty
   */
  public function Show(&$pSmarty) {
    global $aConfig;
    // dolaczenie klasy View
    include_once('View/View.class.php');
    // zapamietanie opcji
    rememberViewState($this->sModule);
    if($_POST['f_adress_name']!='') {
      $this->iUserAdressId = $_POST['f_adress_name'];
    }
    $sSql = 'SELECT email 
            FROM users_accounts 
            WHERE id = ' . $this->iUId;
    $sUName = Common::getOne($sSql);
    $sHeader = sprintf($aConfig['lang'][$this->sModule]['page_header'], $sUName);

    $aHeader = array(
        'header' => $sHeader,
        'refresh' => false,
        'search' => false,
        'checkboxes' => false
    );

    $aAttribs = array(
        'width' => '100%',
        'border' => 0,
        'cellspacing' => 0,
        'cellpadding' => 0,
        'class' => 'viewHeaderTable'
    );
    $aRecordsHeader = array(
        array(
            'content' => $aConfig['lang'][$this->sModule]['publisher_series'],
            'width' => '250'
        ),
        array(
            'content' => $aConfig['lang'][$this->sModule]['user_publisher_series_discount']
        ),
        array(
            'content' => $aConfig['lang'][$this->sModule]['created_date']
        ),
        array(
            'content' => $aConfig['lang'][$this->sModule]['created_by']
        ),
        array(
            'content' => $aConfig['lang'][$this->sModule]['action'],
            'width' => '250'
        )
    );

    $aColSettingsPublisher = array(
        'id' => array(
            'show' => false
        ),
        'products_publishers_id' => array(
            'show' => false
        ),
        'user_adress_id' => array(
            'show' => false
        ),
        'utp_id' => array(
          'show' => false  
        ),
        'action' => array(
            'actions' => array('add_series', 'edit_publisher', 'delete_publisher'),
            'params' => array(
                'add_series' => array('id' => '{utp_id}', 'user_adress_id' => $this->iUserAdressId),
                'edit_publisher' => array('id' => '{id}', 'user_adress_id' => $this->iUserAdressId),
                'delete_publisher' => array('id' => '{utp_id}')
            ),
            'icon' => array('add_series' => 'add', 'edit_publisher' => 'edit', 'delete_publisher' => 'delete')
        )
    );

    $aColSettingsSeries = array(
        'id' => array(
            'show' => false
        ),
        'products_publishers_id' => array(
            'show' => false
        ),
        'action' => array(
            'actions' => array('edit_series', 'delete_series'),
            'params' => array(
                'edit_series' => array('id' => '{users_to_products_publishers_id}', 'selected' => '{id}', 'user_adress_id' => $this->iUserAdressId),
                'delete_series' => array('id' => '{id}', 'selected' => '{id}')
            ),
            'icon' => array('edit_series' => 'edit', 'delete_series' => 'delete')
        )
    );

    $pView = new View('group_discount', $aHeader, $aAttribs);
    $pView->AddRecordsHeader($aRecordsHeader);

    //FILTRY 

    $aAdressName = $this->getAdressNameArray($this->iUID);

    $pView->AddFilter('f_adress_name', $aConfig['lang'][$this->sModule]['f_adress_name'], $aAdressName, $_POST['f_adress_name']);

    if ($this->iUserAdressId == '' && !empty($aAdressName)) {
      $this->iUserAdressId = $aAdressName[0]['value'];
    }

    if ($this->iUserAdressId != '') {
      $sSql = 'SELECT COUNT(p.id) 
             FROM users_to_products_publishers AS utp
             JOIN products_publishers AS p
             ON utp.products_publishers_id = p.id 
             WHERE utp.users_accounts_id = ' . $this->iUId . '
             AND utp.user_adress_id = ' . $this->iUserAdressId;

      $iRowCount = intval(Common::GetOne($sSql));
      if ($iRowCount > 0) {
        // dodanie Pagera do widoku
        $iCurrentPage = $pView->AddPager($iRowCount);
        $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
        $iStartFrom = ($iCurrentPage - 1) * $iPerPage;

        $sSql = "SELECT p.id, p.name, utp.discount, utp.products_publishers_id, utp.created_date, utp.created_by, utp.user_adress_id, utp.id AS utp_id
              FROM users_to_products_publishers AS utp 
              JOIN products_publishers AS p 
              ON utp.products_publishers_id = p.id 
              WHERE utp.users_accounts_id = " . $this->iUId . '
              AND utp.user_adress_id = ' . $this->iUserAdressId . '
              LIMIT ' . $iStartFrom . ',' . $iPerPage;

        $aDiscounts = & Common::GetAll($sSql);

        $pView->AddRecords($aDiscounts, $aColSettingsPublisher);
        $aSubRecords = array();
        foreach ($aDiscounts as $iKey => $aPublisher) {

          $sSql = "SELECT utps.id, s.name, utps.created_by, utps.created_date, utps.discount, utps.users_to_products_publishers_id 
                FROM users_to_prod_series_publishers AS utps 
                JOIN products_series AS s ON utps.products_series_id = s.id 
                WHERE utps.users_to_products_publishers_id = " . $aPublisher['utp_id'] . "
                AND utps.users_accounts_id = " . $this->iUId;
          $aSubRecords[$aPublisher['id']] = Common::GetAll($sSql);
        }
        $pView->AddSubRecords($aSubRecords, $aColSettingsSeries);
      }
    }
    $aRecordsFooter = array(
        array('go_back'),
        array('discount', 'add')
    );

    $aRecordsFooterParams = array(
        'go_back' => array(1 => array('action', 'id', 'pid')),
        'add' => array(0 => array('user_adress_id' => $this->iUserAdressId)),
        'discount' => array(0 => array('id' => $this->iUId))
    );

    $pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $pView->Show());
  }

    /**
     * @param $pSmarty
     * @param $iId
     * @return bool
     */
  public function copySettingsForm($pSmarty, $iId) {
      global $aConfig;
      $aData = array();
      include_once('Form/FormTable.class.php');

      $pForm = new FormTable('copy_settings', _('Kopiuj konfigurację'), array('action' => phpSelf(array('id' => $iId, 'do' => 'do_copy_settings'))), array('col_width' => 150), $aConfig['common']['js_validation']);
      $sSql = 'SELECT name as label, id as value
       FROM websites
       WHERE url <> ""
      ';
      $websites  = $this->pDbMgr->GetAll('profit24', $sSql);
      $pForm->AddSelect('website_id', _('Serwis'), [], addDefaultValue($websites));
      $pForm->AddText('email', _('Email'));

      $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_0']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show'), array('id')) . '\');'), 'button'));
      $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $pForm->ShowForm());
      return true;
  }


    /**
     * @param $pSmarty
     * @param $iId
     */
    private function doCopySettings($pSmarty, $iId) {
        global $pDB;

        $sSql = 'SELECT id FROM users_accounts WHERE email = '.$pDB->quoteSmart($_POST['email']) .' AND website_id = '.$_POST['website_id'];
        $userId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($userId <= 0 && $iId > 0) {
            $this->sMsg .= GetMessage('Nie znaleziono uzytkownika o podanych danych');
            return $this->copySettingsForm($pSmarty, $iId);
        } else {

            $sSql = 'SELECT * FROM users_to_products_publishers WHERE users_accounts_id = '.$iId;
            $publishers = $this->pDbMgr->GetAll('profit24', $sSql);

            foreach ($publishers as $publisher) {
                $pUPId = $publisher['id'];
                unset($publisher['id']);
                unset($publishers['user_adress_id']);
                // insert
                $publisher['users_accounts_id'] = $userId;
                $insertedPublisherId = $this->pDbMgr->Insert('profit24', 'users_to_products_publishers', $publisher);

                $sSql = 'SELECT * FROM users_to_prod_series_publishers WHERE users_accounts_id = '.$iId.' AND users_to_products_publishers_id = '.$pUPId;
                $seriesPublishers = $this->pDbMgr->GetAll('profit24', $sSql);

                foreach ($seriesPublishers as $seriesPublisher) {
                    // podmiana
                    unset($seriesPublisher['id']);
                    $seriesPublisher['users_to_products_publishers_id'] = $insertedPublisherId;
                    $seriesPublisher['users_accounts_id'] = $userId;
                    $this->pDbMgr->Insert('profit24', 'users_to_prod_series_publishers', $seriesPublisher);
                }
            }
            $this->sMsg .= GetMessage('Przekiopiowano ustawienia z użytkownika o id '.$iId.' na użytkownika o email '.$_POST['email'], false);
            return $this->Show($pSmarty);
        }
    }


//end show() function

  private function getsJS() {
    return '
    <script>
      $(document).ready(function(){
        $("#publisher_aci").autocomplete({
                  source: "' . $aConfig['common']['client_base_url_https'] . $aConfig['common']['cms_dir'] . 'ajax/GetPublishers.php"
        });

        $("#publisher_aci").bind("blur", function(e){
         $.get("ajax/GetSeries.php", { publisher: e.target.value },
              function(data){
                var brokenstring=data.split(";");
                var elSelSeries = document.getElementById(\'series\');
                elSelSeries.length = 0;
                elSelSeries.options[elSelSeries.length] =  new Option("' . $aConfig['lang']['common']['choose'] . '","0");
                for (x in brokenstring){
                  if(brokenstring[x]){
                    var item=brokenstring[x].split("|");
                    elSelSeries.options[elSelSeries.length] = new Option(item[0],item[1]);
                  }
                }
             });
        });
        
      });
      </script> ';
  }

  
  /**
   * Metoda usuwa daną serię wyd. z listy rabatów 
   * @global type $aConfig
   * @param type $pSmarty
   * @param integer $iId -
   * @param integer $iSelectedId - 
   */
  public function deleteSeries($pSmarty, $iId, $iSelectedId) {
    global $aConfig;
    $bIsErr = false;

    $sSql = 'SELECT ps.name 
           FROM users_to_prod_series_publishers AS utps JOIN products_series AS ps 
           ON utps.products_series_id = ps.id
           WHERE utps.id = ' . $iId;
    $sSeriesName = Common::getOne($sSql);


    $sSql = 'DELETE FROM users_to_prod_series_publishers
            WHERE id = ' . $iSelectedId . '
            AND users_accounts_id = ' . $this->iUId;

    if (($iResS = Common::Query($sSql)) === false) {
      $bIsErr = true;
      $sMsg = $aConfig['lang'][$this->sModule]['del_err_series'];
      AddLog($sMsg);
    } else {
      $sMsg = sprintf($aConfig['lang'][$this->sModule]['del_success_series'], $sSeriesName);
    }

    $this->sMsg = GetMessage($sMsg, $bIsErr);
    $this->Show($pSmarty);
  }
//end deleteSeries() function 

  /**
   * Metoda sprawdza czy wydawnictwo o podanej nazwie istnieje
   * @param string $sPublisherName - nazwa wydawnictwa
   * @return boolean 
   */
  public function checkPublisherExists($sPublisherName) {

    $sSql = 'SELECT COUNT(*) 
           FROM products_publishers
           WHERE name = \'' . $sPublisherName . '\'';
    $bExists = Common::getOne($sSql);
    return $bExists;
  }

  private function getAdressNameArray($iUID) {

    $aAdressName = array();

    $sSql = 'SELECT CONCAT(name, " ",surname) as name, 
              is_company, company, id 
             FROM users_accounts_address
             WHERE user_id = ' . $this->iUId;

    $aData = Common::GetAll($sSql);
    foreach ($aData as $iKey => $aValue) {
      if ($aValue['is_company'] == 1) {
        $aAdressName[] = array('value' => $aValue['id'], 'label' => $aValue['company']);
      } else {
        $aAdressName[] = array('value' => $aValue['id'], 'label' => $aValue['name']);
      }
    }
    return $aAdressName;
  }

  /**
   * Metoda usuwa dane wydawnictwo z listy rabatów
   * @global type $aConfig
   * @param type $pSmarty
   * @param type $iId - id rekordu 
   */
  public function deletePublisher($pSmarty, $iId) {
    global $aConfig;
    $bIsErr = false;

    $sSql = 'DELETE FROM users_to_products_publishers
            WHERE id = ' . $iId;


    if (($iResP = Common::Query($sSql)) === false) {
      $bIsErr = true;
      $sMsg = $aConfig['lang'][$this->sModule]['del_err_publisher'];
      AddLog($sMsg);
    } else {
      $sMsg = $aConfig['lang'][$this->sModule]['del_success_both'];
    }


    $this->sMsg = GetMessage($sMsg, $bIsErr);
    $this->Show($pSmarty);
  }
//end deletePublisher() function 

  /**
   * Metoda dodaje/usuwa rabat na serię wydawniczą
   * @global type $aConfig
   * @param type $pSmarty
   * @param integer $iUserToProdPublisherId - id rekordu w tabeli users_to_products_publishers
   * @param boolean $bEdit - edycja
   * @param integer $iSelectedId - id rekordu
   */
  public function AddEditSeries($pSmarty, $iUserToProdPublisherId, $bEdit, $iSelectedId = null) {

    global $aConfig;
    $aData = array();
    include_once('Form/FormTable.class.php');

    $sSql = 'SELECT PP.id, PP.name  
           FROM products_publishers AS PP 
           JOIN users_to_products_publishers AS UTP
           ON UTP.products_publishers_id = PP.id
           WHERE UTP.id = ' . $iUserToProdPublisherId;
    $aPublisherRow = Common::GetRow($sSql);

    $sPublisherName = $aPublisherRow['name'];
    $aPublisherId = $aPublisherRow['id'];
    
    
    $sSql = 'SELECT name AS name, id AS value
           FROM products_series
           WHERE publisher_id = ' . $aPublisherId;

    $aData = Common::GetAll($sSql);

    $aAtribs = array('style' => 'width:300px;');
    if ($bEdit === TRUE) {

      $sSql = 'SELECT ps.id, utps.discount 
             FROM users_to_prod_series_publishers AS utps 
             JOIN products_series AS ps
             ON utps.products_series_id = ps.id 
             WHERE utps.id = ' . $iSelectedId . '
             AND utps.users_accounts_id = ' . $this->iUId;
      $aSelectedSeries = Common::GetRow($sSql);
      $sAction = 'update_series';
      $aAtribs['DISABLED'] = 'DISABLED';
    } else {
      $sAction = 'insert_series';
    }


    $sHeader = sprintf($aConfig['lang'][$this->sModule]['add_publisher_series_discount'] . $sPublisherName);
    $pForm = new FormTable('add_series', $sHeader, array('action' => phpSelf(array('id' => (isset($iSelectedId)) ? $iSelectedId : $iUserToProdPublisherId, 'do' => $sAction, 'user_adress_id' => $this->iUserAdressId))), array('col_width' => 150), $aConfig['common']['js_validation']);
    // seria
    $pForm->AddRow($aConfig['lang'][$this->sModule]['series'], $pForm->GetSelectHtml('series', $aConfig['lang'][$this->sModule]['series'], $aAtribs, addDefaultValue($aData), $aSelectedSeries['id'], '', true) .
            '&nbsp;&nbsp;&nbsp;' . $aConfig['lang'][$this->sModule]['series_discount'] . '&nbsp;&nbsp;&nbsp;' .
            $pForm->GetTextHTML('series_discount', $aConfig['lang'][$this->sModule]['series_discount'], $aSelectedSeries['discount'], array('id' => 'series_discount', 'style' => 'width:30px;', 'maxlength' => 255), '', 'text', true, '', array(0, 100)));

    $sSql = 'SELECT DISTINCT seller_streamsoft_id AS label, seller_streamsoft_id AS value 
           FROM users_accounts 
           WHERE seller_streamsoft_id <> ""
           GROUP BY seller_streamsoft_id
           ';
    $sellersStreamsoft = $this->pDbMgr->GetAll('profit24', $sSql);
    $pForm->AddSelect('apply_user_of_seller_id', _('Zastosuj do wszystkich użytkowników wybranego Handlowca : '), [], addDefaultValue($sellersStreamsoft, 'NIE'), '', '', false);

    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_' . ($iUserToProdPublisherId > 0 ? 1 : 0)]) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show'), array('id')) . '\');'), 'button'));
    $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()) . ((!$bEdit) ? $this->getsJS() : ''));
  }
//end AddEditSeries() function  

  /**
   * Metoda dodaje/usuwa rabat na wydawnictwo
   * @global array $aConfig
   * @param Smarty $pSmarty
   * @param int $iId - id wydawnictwa
   * @param boolean $bEdit - edycja
   */
  private function AddEditPublisher($pSmarty, $iId, $bEdit) {
    global $aConfig;

    if ($this->iUserAdressId != '') {
      include_once('Form/FormTable.class.php');

      $sSql = 'SELECT pp.name, utpp.discount 
           FROM users_to_products_publishers AS utpp 
           JOIN products_publishers AS pp 
           ON utpp.products_publishers_id = pp.id
           WHERE utpp.products_publishers_id = ' . $iId . '
           AND users_accounts_id = ' . $this->iUId;
      $aPublisher = (isset($iId)) ? Common::GetRow($sSql) : array();

      $aAtribs = array('id' => 'publisher_aci', 'style' => 'width:300px;', 'maxlength' => 255);
      if ($bEdit === TRUE) {
        $aAtribs['DISABLED'] = 'DISABLED';
        $sAction = 'update_publisher';
        $sTitle = 'publisher_edit_header';
      } else {
        $sAction = 'insert_publisher';
        $sTitle = 'publisher_add_header';
      }

      $sHeader = sprintf($aConfig['lang'][$this->sModule][$sTitle]);
      $pForm = new FormTable('add_publisher', $sHeader, array('action' => phpSelf(array('id' => $iId, 'do' => $sAction, 'user_adress_id' => $this->iUserAdressId))), array('col_width' => 150), $aConfig['common']['js_validation']);

      //Wydawnictwo
      $pForm->AddRow($aConfig['lang'][$this->sModule]['publisher'], $pForm->GetTextHTML('publisher', $aConfig['lang'][$this->sModule]['publisher'], $aPublisher['name'], $aAtribs, '', 'text', true) .
              '&nbsp;&nbsp;&nbsp;' . $aConfig['lang'][$this->sModule]['publisher_discount'] . '&nbsp;&nbsp;&nbsp;' .
              $pForm->GetTextHTML('publisher_discount', $aConfig['lang'][$this->sModule]['publisher_discount'], $aPublisher['discount'], array('style' => 'width:30px;', 'maxlength' => 255), '', 'text', true, '', array(0, 100)));


        $sSql = 'SELECT DISTINCT seller_streamsoft_id AS label, seller_streamsoft_id AS value 
                   FROM users_accounts 
                   WHERE seller_streamsoft_id <> ""
                   GROUP BY seller_streamsoft_id
                   ';
        $sellersStreamsoft = $this->pDbMgr->GetAll('profit24', $sSql);
        $pForm->AddSelect('apply_user_of_seller_id', _('Zastosuj do wszystkich użytkowników wybranego Handlowca : '), [], addDefaultValue($sellersStreamsoft, 'NIE'), '', '', false);

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_' . ($iId > 0 ? 1 : 0)]) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'show'), array('id')) . '\');'), 'button'));



      $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()) . $this->getsJS());
    }
    else {
      $sMsg = $aConfig['lang'][$this->sModule]['no_user_adress'];
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
    }
  }
//end of AddEditPublisher() function 

  /**
   * Metoda wrzuca do bazy danych nowy rabat na wydawnictwo
   * @global type $aConfig
   * @param type $pSmarty
   * @param integer $iId - id wydawnictwa
   */
  private function InsertPublisher($pSmarty, $iId) {
    global $aConfig;
      $bIsErr = false;

      $discountPercent = \Common::formatPrice2($_POST['publisher_discount']);

    $sSql = 'SELECT COUNT(*) 
           FROM users_to_products_publishers AS utpp 
           JOIN products_publishers AS pp
           ON utpp.products_publishers_id = pp.id
           WHERE pp.name = \'' . $_POST['publisher'] . '\'
           AND utpp.users_accounts_id = ' . $this->iUId . ' 
           AND utpp.user_adress_id = ' . $this->iUserAdressId;

    $bCheckNotDoubled = Common::getOne($sSql);
    $bCheckPublisherExists = $this->checkPublisherExists($_POST['publisher']);

    if (!$bCheckNotDoubled && $bCheckPublisherExists) {
      $sCreatedBy = $_SESSION['user']['name'];


      $sSql = 'SELECT id 
             FROM products_publishers 
             WHERE name LIKE \'' . $_POST['publisher'] . '\'';

      $iPublisherId = Common::GetOne($sSql);

        if (isset($_POST['apply_user_of_seller_id']) && $_POST['apply_user_of_seller_id'] != '') {
            $sellerStreamsoftId = $_POST['apply_user_of_seller_id'];

            $sSql = 'SELECT UA.id, UTPP.id AS UTPP_ID  
                     FROM users_accounts AS UA
                     LEFT JOIN users_to_products_publishers AS UTPP
                      ON UA.id = UTPP.users_accounts_id AND UTPP.products_publishers_id = "'.$iPublisherId.'"
                     WHERE UA.seller_streamsoft_id = "'.$sellerStreamsoftId.'"
                     AND UA.id <> "'.$this->iUId.'"
                     ';
            $usersSellerIds = $this->pDbMgr->GetAll('profit24', $sSql);
            foreach ($usersSellerIds as $userSellerRow) {
                $iUTTPId = $userSellerRow['UTPP_ID'];
                if ($iUTTPId > 0) {
                    $sUpdateSQL = 'UPDATE users_to_products_publishers 
                                   SET discount = '.$discountPercent.'
                                   WHERE id = '.$iUTTPId;
                    if (false === $this->pDbMgr->Query('profit24', $sUpdateSQL)) {
                        $bIsErr = true;
                    }
                } else {
                    $aValues = array(
                        'products_publishers_id' => $iPublisherId,
                        'users_accounts_id' => $userSellerRow['id'],
                        'user_adress_id' => $this->iUserAdressId,
                        'discount' => $discountPercent,
                        'created_date' => 'NOW()',
                        'created_by' => $sCreatedBy
                    );
                    if (false === $this->pDbMgr->Insert('profit24', "users_to_products_publishers", $aValues)) {
                        $bIsErr = true;
                    }
                }
            }
        }

      $aValues = array(
          'products_publishers_id' => $iPublisherId,
          'users_accounts_id' => $this->iUId,
          'user_adress_id' => $this->iUserAdressId,
          'discount' => $discountPercent,
          'created_date' => 'NOW()',
          'created_by' => $sCreatedBy
      );
      if (false === $this->pDbMgr->Insert('profit24', "users_to_products_publishers", $aValues)) {
          $bIsErr = true;
      }

      if ($bIsErr === false) {
          $this->pDbMgr->CommitTransaction('profit24');
        //rekord zostal zmieniony
        $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_publisher_ok'], $this->getUserEmail($this->iUId));
        $this->sMsg = GetMessage($sMsg, false);
        // dodanie informacji do logow
        AddLog($sMsg, false);
        $this->Show($pSmarty);
      } else {
          $this->pDbMgr->RollbackTransaction('profit24');
        // rekord nie zostal zmieniony
        $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_publisher_err'], $this->getUserEmail($this->iUId));
        $this->sMsg = GetMessage($sMsg);
        // dodanie informacji do logow
        AddLog($sMsg);
        $this->AddEditPublisher($pSmarty, $iId, false);
      }
    } elseif ($bCheckNotDoubled) {
      $sMsg = $aConfig['lang'][$this->sModule]['publisher_exists'];
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
    } else {
      $sMsg = $aConfig['lang'][$this->sModule]['publisher_invalid_name'];
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
    }
  }
//end of InsertPublisher() function

  /**
   * Metoda wrzuca do bazy danych nowy rabat na serie wyd.
   * @global type $aConfig
   * @param type $pSmarty
   * @param integer $iId - id wydawnictwa
   */
  private function InsertSeries($pSmarty, $iId) {
    global $aConfig;
      $bIsErr = false;
      $discountPercent = \Common::formatPrice2($_POST['series_discount']);

    $sSql = 'SELECT COUNT(*) 
           FROM users_to_prod_series_publishers AS utps 
           JOIN products_series AS ps 
           ON utps.products_series_id = ps.id
            JOIN users_to_products_publishers AS utpp
            ON utps.users_to_products_publishers_id = utpp.id
           WHERE utps.products_series_id = ' . $_POST['series'] . '
           AND utps.users_accounts_id = ' . $this->iUId . '
           AND utpp.user_adress_id = ' .$this->iUserAdressId;
    

    $iCheck = Common::getOne($sSql);

    if (!$iCheck) {
      $sCreatedBy = $_SESSION['user']['name'];


        if (isset($_POST['apply_user_of_seller_id']) && $_POST['apply_user_of_seller_id'] != '') {
            $sellerStreamsoftId = $_POST['apply_user_of_seller_id'];

            $sSql = 'SELECT UA.id, UTPSP.id AS UTPSP_ID  
                     FROM users_accounts AS UA
                     LEFT JOIN users_to_prod_series_publishers AS UTPSP
                      ON UA.id = UTPSP.users_accounts_id AND UTPSP.products_series_id = "'.$_POST['series'].'"
                     WHERE UA.seller_streamsoft_id = "'.$sellerStreamsoftId.'"
                     AND UA.id <> "'.$this->iUId.'"
                     ';
            $usersSellerIds = $this->pDbMgr->GetAll('profit24', $sSql);
            foreach ($usersSellerIds as $userSellerRow) {
                $iUTTPId = $userSellerRow['UTPSP_ID'];
                if ($iUTTPId > 0) {
                    $sUpdateSQL = 'UPDATE users_to_prod_series_publishers 
                                   SET discount = '.$discountPercent.'
                                   WHERE id = '.$iUTTPId;
                    if (false === $this->pDbMgr->Query('profit24', $sUpdateSQL)) {
                        $bIsErr = true;
                    }
                } else {

                    $sSql = 'SELECT products_publishers_id
                             FROM users_to_products_publishers
                             WHERE id = '.$iId;
                    $productsPublishersId = $this->pDbMgr->GetOne('profit24', $sSql);
                    if ($productsPublishersId > 0) {
                        $sSql = 'SELECT id 
                                 FROM users_to_products_publishers 
                                 WHERE products_publishers_id = "'.$productsPublishersId.'"
                                   AND users_accounts_id = '.$userSellerRow['id'];
                        $usersToProductsPublishersId = $this->pDbMgr->GetOne('profit24', $sSql);
                        if ($usersToProductsPublishersId > 0) {
                            $aValues = array(
                                'users_to_products_publishers_id' => $usersToProductsPublishersId,
                                'users_accounts_id' => $userSellerRow['id'],
                                'products_series_id' => $_POST['series'],
                                'discount' => $discountPercent,
                                'created_date' => 'NOW()',
                                'created_by' => $sCreatedBy
                            );
                            if (false === $this->pDbMgr->Insert('profit24', "users_to_prod_series_publishers", $aValues)) {
                                $bIsErr = true;
                            }
                        }
                    }
                }
            }
        }

      $aValues = array(
          'users_to_products_publishers_id' => $iId,
          'users_accounts_id' => $this->iUId,
          'products_series_id' => $_POST['series'],
          'discount' => $discountPercent,
          'created_date' => 'NOW()',
          'created_by' => $sCreatedBy
      );
      if (false === Common::Insert("users_to_prod_series_publishers", $aValues)) {
        $bIsErr = true;
      }


      if ($bIsErr === false) {
        //rekord zostal zmieniony
        $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_series_ok'], $this->getUserEmail($this->iUId));
        $this->sMsg = GetMessage($sMsg, false);
        // dodanie informacji do logow
        AddLog($sMsg, false);
        $this->Show($pSmarty);
      } else {
        // rekord nie zostal zmieniony
        $sMsg = sprintf($aConfig['lang'][$this->sModule]['add_series_err'], $this->getUserEmail($this->iUId));
        $this->sMsg = GetMessage($sMsg);
        // dodanie informacji do logow
        AddLog($sMsg);
        $this->AddEditSeries($pSmarty, $iId, false);
      }
    } else {
      $sMsg = $aConfig['lang'][$this->sModule]['series_exists'];
      $this->sMsg = GetMessage($sMsg);
      $this->Show($pSmarty);
    }
  }
//end InsertSeries() function 

    /**
     * Metoda aktualizuje rabat na wydawnictwo
     *
     * @param $pSmarty
     * @param $iId
     * @return bool
     */
  private function UpdatePublisher($pSmarty, $iId) {
    global $aConfig;

      if (empty($_POST)) {
          $sMsg = _('Brak przesłanych danych');
          $this->sMsg = GetMessage($sMsg);
          AddLog($sMsg);
          $this->AddEditPublisher($pSmarty, $iId, true);
          return false;
      }
    $dDiscount = $_POST['publisher_discount'];

    // pobieramy handlowca i filtrujemy update po handlowcu
    if (intval($this->iUId) <= 0) {
        $sMsg = _('Brak id użytkownika');
        $this->sMsg = GetMessage($sMsg);
        AddLog($sMsg);
        $this->AddEditPublisher($pSmarty, $iId, true);
        return false;
    }

    if (isset($_POST['apply_user_of_seller_id']) && $_POST['apply_user_of_seller_id'] != '') {
        $sellerStreamsoftId = $_POST['apply_user_of_seller_id'];
    }

    $sSql = 'SELECT name 
           FROM products_publishers 
           WHERE id = ' . $iId;
    $sPublisherName = Common::getOne($sSql);

    $sSql = 'UPDATE users_to_products_publishers AS UTPP
             JOIN users_accounts AS UA
              ON UA.id = UTPP.users_accounts_id
             SET 
              UTPP.discount = "'.$dDiscount.'",
              UTPP.created_date = NOW(),
              UTPP.created_by = "'.$_SESSION['user']['name'].'"
             WHERE  
             products_publishers_id = '.$iId.' AND
             '.($sellerStreamsoftId != '' ? ' UA.seller_streamsoft_id = "'.$sellerStreamsoftId.'" ' : ' UA.id = "'.$this->iUId.'" ');
    if ($this->pDbMgr->Query('profit24', $sSql) === false) {
      $bIsErr = true;
    }

    if (!$bIsErr) {
      // rabat uzytkownika zostal zmodyfikowany
        if ($sellerStreamsoftId != '') {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['publisher_discount_edit_seller_ok'], $sPublisherName, $sellerStreamsoftId);
        } else {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['publisher_discount_edit_ok'], $sPublisherName);
        }

      $this->sMsg = GetMessage($sMsg, false);
      // dodanie informacji do logow
      AddLog($sMsg, false);
      $this->Show($pSmarty);
    } else {
      // rabat nie sotal zapisany - blad
        if ($sellerStreamsoftId != '') {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['publisher_discount_edit_seller_err'], $sPublisherName, $sellerStreamsoftId);
        } else {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['publisher_discount_edit_err'], $sPublisherName);
        }

      $this->sMsg = GetMessage($sMsg);
      AddLog($sMsg);
      $this->AddEditPublisher($pSmarty, $iId, true);
    }
  }//end UpdatePublishers() function

    /**
     * @param $userId
     * @return mixed
     */
    public function getSellerId($userId) {

        $sSql = 'SELECT seller_streamsoft_id
                 FROM users_accounts
                 WHERE id = '.$userId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

  /**
   * Metoda aktualizuje rabat na serię wyd.
   * @global type $aConfig
   * @param type $pSmarty
   * @param integer $iId - id rekordu
   */
  private function UpdateSeries($pSmarty, $iId) {
    global $aConfig;

    if (empty($_POST)) {
      $sMsg = _('Brak przesłanych danych');
      $this->sMsg = GetMessage($sMsg);
      AddLog($sMsg);
      $this->AddEditPublisher($pSmarty, $iId, true);
      return false;
    }

    $sDiscount = $_POST['series_discount'];

    // pobieramy handlowca i filtrujemy update po handlowcu
    if (intval($this->iUId) <= 0) {
      $sMsg = _('Brak id użytkownika');
      $this->sMsg = GetMessage($sMsg);
      AddLog($sMsg);
      $this->AddEditPublisher($pSmarty, $iId, true);
      return false;
    }

    if (isset($_POST['apply_user_of_seller_id']) && $_POST['apply_user_of_seller_id'] != '') {
      $sellerStreamsoftId = $_POST['apply_user_of_seller_id'];
    }

      $sSql = 'SELECT ps.name, ps.id
             FROM users_to_prod_series_publishers AS utps JOIN products_series AS ps 
             ON utps.products_series_id = ps.id
             WHERE utps.id = ' . $iId;
      $usersSeriesPublishers = Common::GetRow($sSql);
      $sSeriesName = $usersSeriesPublishers['name'];


      $sSql = 'UPDATE users_to_prod_series_publishers AS UTPP
             JOIN users_accounts AS UA
              ON UA.id = UTPP.users_accounts_id
             SET 
              UTPP.discount = "'.$sDiscount.'",
              UTPP.created_date = NOW(),
              UTPP.created_by = "'.$_SESSION['user']['name'].'"
             WHERE
             products_series_id = '.$usersSeriesPublishers['id'].' AND
             '.($sellerStreamsoftId != '' ? ' UA.seller_streamsoft_id = "'.$sellerStreamsoftId.'" ' : ' UA.id = "'.$this->iUId.'" ');
      if ($this->pDbMgr->Query('profit24', $sSql) === false) {
          $bIsErr = true;
      }


    if (!$bIsErr) {
      // rabat uzytkownika zostal zmodyfikowany
        if ($sellerStreamsoftId != '') {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['series_discount_edit_seller_ok'], $sSeriesName, $sellerStreamsoftId);
        } else {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['series_discount_edit_ok'], $sSeriesName);
        }

      $this->sMsg = GetMessage($sMsg, false);
      // dodanie informacji do logow
      AddLog($sMsg, false);
      $this->Show($pSmarty);
    } else {
      // rabat nie sotal zapisany - blad
        if ($sellerStreamsoftId != '') {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['series_discount_edit_seller_err'], $sSeriesName, $sellerStreamsoftId);
        } else {
            $sMsg = sprintf($aConfig['lang'][$this->sModule]['series_discount_edit_err'], $sSeriesName);
        }
      $this->sMsg = GetMessage($sMsg);
      AddLog($sMsg);
      $this->AddEditSeries($pSmarty, $iId, true);
    }
  }
//end UpdateSeries() function
}
