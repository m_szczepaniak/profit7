<?php

use LIB\EntityManager\Entites\UsersGroups;

class Module
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    /**
     *
     * @var DatabaseManager $pDbMgr
     */
    private $pDbMgr;

    private $pUsersGroups;

    /**
     *
     * @global DatabaseManager $pDbMgr
     * @param Smarty $pSmarty
     * @return void
     */
    function __construct($pSmarty)
    {
        global $pDbMgr;

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        $sDo = '';
        $iId = 0;
        $iPId = 0;


        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do']) && $_POST['do'] !== "") {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        $this->pDbMgr = $pDbMgr;
        $this->pUsersGroups = new UsersGroups($pDbMgr);

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->sModule = $_GET['module'];

        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }


        if (isset($_GET['id'])) {
            $iId = $_GET['id'];
        }

        $this->aPrivileges =& $_SESSION['user']['privileges'];
        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if ('set_as_read' !== $sDo && !hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }
        switch ($sDo) {
            case 'delete':
                $resultHTML = $this->Delete($pSmarty, $iId);
                break;
            case 'insert':
                $resultHTML = $this->Insert($pSmarty);
                break;
            case 'update':
                $resultHTML = $this->Update($pSmarty, $iId);
                break;
            case 'add':
                $resultHTML = $this->AddEdit($pSmarty);
                break;
            case 'edit':
                $resultHTML = $this->AddEdit($pSmarty, $iId);
                break;
            case 'details':
                $resultHTML = $this->Details($pSmarty, $iId);
                break;
            case 'addToGroup':
                $resultHTML = $this->AddToGroup($pSmarty);
                break;
            case 'insert_to_group':
                $resultHTML = $this->InsertToGroup($pSmarty);
                break;
            case 'remove_from_group':
                $resultHTML = $this->RemoveFromGroup($pSmarty, $iId);
                break;
            case 'set_as_read':
                $this->SetAsRead($pSmarty, $iId);
                break;
            default:
                $resultHTML = $this->Show($pSmarty);
                break;
        }
        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . $resultHTML);
    }

    private function Show($pSmarty)
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Grupy')),
            'refresh' => true,
            'search' => false,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field' => 'id',
                'content' => _('id'),
                'sortable' => true,
                'width' => '380'
            ),
            array(
                'db_field' => 'name',
                'content' => _('Nazwa'),
                'sortable' => true,
                'width' => '380'
            ),
            array(
                'db_field' => 'created',
                'content' => _('Utworzono'),
                'sortable' => true,
                'width' => '380'
            ),
            array(
                'db_field' => 'created_by',
                'content' => _('przez'),
                'sortable' => true,
                'width' => '380'
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '25'
            )
        );

        $pView = new \View('users_groups', $aHeader, $aAttribs);
        // pobranie liczby wszystkich uzytkownikow
        $aRecords = $this->getRecords();
        $iRowCount = count($aRecords);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'name' => array(
                    'link' => phpSelf(array('do' => 'edit', 'id' => '{id}'))
                ),
                'modified' => array(
                    'show' => false,
                ),
                'modified_by' => array(
                    'show' => false,
                ),
                'action' => array(
                    'actions' => array('details','edit', 'delete'),
                    'params' => array(
                        'details' => array('id' => '{id}'),
                        'edit' => array('id' => '{id}'),
                        'delete' => array('id' => '{id}')
                    ),
                    'show' => false,
                )
            );

            $pView->AddRecords($aRecords, $aColSettings);
        }
        $pView->AddRecordsHeader($aRecordsHeader);
        $aRecordsFooter = array(array(), array('add'));
        $pView->AddRecordsFooter($aRecordsFooter);
        return $pView->Show();
    }

    /**
     * @param $pSmarty
     * @param int $iId
     * @return string
     */
    private function AddEdit($pSmarty, $iId = 0)
    {
        global $aConfig;

        $aData = [];
        if (isset($_POST) && !empty($_POST)) {
            $aData = $_POST;
        }
        else if (isset($_GET) && !empty($_GET) && isset($_GET['name'])) {
            $aData = $_GET;
            if (isset($_GET['name']) && !empty($_GET['name'])) {
                $aData['name'] = urldecode(html_entity_decode($_GET['name']));
            }
        }
        if ($iId > 0 && empty($aData)) {
            $aData = $this->getRecord($iId);
        }

        include_once('Form/FormTable.class.php');
        $pForm = new \FormTable('user_groups', _('Grupy'), array('action' => phpSelf(array('id' => (isset($iId)) ? $iId : null))), array('col_width' => 150), $aConfig['common']['js_validation']);

        $pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');

        $pForm->AddText('name', _('Nazwa'), $aData['name'], array('id' => 'name', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', true);

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz')) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'default')) . '\');'), 'button'));

        return ShowTable($pForm->ShowForm());
    }

    /**
     * @param $pSmarty
     * @param $iId
     * @return string
     */
    private function Update($pSmarty, $iId)
    {

        $aValues = array(
            'name' => $_POST['name'],
            'modified' => 'NOW()',
            'modified_by' => $_SESSION['user']['name']
        );

        if ($this->pDbMgr->Update('profit24', 'users_groups', $aValues, ' id = ' . $iId)) {
            //rekord zostal zmieniony
            $sMsg = sprintf(_('Zmieniono grupę użytkowników'));
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            return $this->Show($pSmarty);
        } else {
            // rekord nie zostal zmieniony
            $sMsg = sprintf(_('Błąd zmiany grupy użytkowników'));
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
            return $this->AddEdit($pSmarty, $iId);
        }
    }

    /**
     * @param $pSmarty
     * @return string
     */
    private function Insert($pSmarty)
    {
        global $aConfig;

        preg_match('/\((\d+)\)/', $_POST['name'], $maches);

        $aValues = array(
            'name' => $_POST['name'],
            'created' => 'NOW()',
            'created_by' => $_SESSION['user']['name'],
            'modified' => 'NOW()',
            'modified_by' => $_SESSION['user']['name']
        );

        if ($this->pDbMgr->Insert('profit24', 'users_groups', $aValues)) {
            //rekord zostal zmieniony
            $sMsg = sprintf(_('Dodano grupę użytkowników'));
            $this->sMsg = GetMessage($sMsg, false);
            // dodanie informacji do logow
            AddLog($sMsg, false);
            return $this->Show($pSmarty);
        } else {
            // rekord nie zostal zmieniony
            $sMsg = sprintf(_('Błąd dodawania grupy użytkowników'));
            $this->sMsg = GetMessage($sMsg);
            // dodanie informacji do logow
            AddLog($sMsg);
            return $this->AddEdit($pSmarty);
        }
    }//end of Insert() function

    /**
     * @param $pSmarty
     * @param $iId
     * @return string
     */
    private function Delete($pSmarty, $iId)
    {
        $bIsErr = false;

        if (($iResS = $this->pUsersGroups->removeGroup($iId)) === false) {
            $bIsErr = true;
            $sMsg = _('Błąd usuwania grupy użytkowników');
            AddLog($sMsg);
        } else {
            $sMsg = _('Usunięto grupę użytkowników');
        }

        $this->sMsg = GetMessage($sMsg, $bIsErr);
        return $this->Show($pSmarty);
    }

    /**
     * @return array
     */
    private function getRecords()
    {
        $sSql = 'SELECT * 
                 FROM users_groups
                 WHERE 1=1
                 ORDER BY id DESC';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @param $iId
     * @return array
     */
    private function getRecord($iId)
    {
        $sSql = 'SELECT * FROM users_groups WHERE id = ' . $iId;
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }


    function AddToGroup($pSmarty) {
        global $aConfig;

        $aData = array();
        $sHtml = '';

        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');

        if (!empty($_POST)) {
            $aData =& $_POST;
        }

        $sHeader = $aConfig['lang'][$_GET['module']]['header_'];

        $pForm = new FormTable('c_users', $sHeader, array('action'=>phpSelf(array('id' => $_POST['delete']))), array('col_width'=>160), false);

        $pForm->AddHidden('do', 'insert_to_group');
        $pForm->AddHidden('add', serialize(array_keys($_POST['delete'])));

        $pForm->AddMergedRow($aConfig['lang'][$_GET['module']]['group'], array('class'=>'merged'));

        $aGroups = $this->pUsersGroups->getGroups();
        $aGroupsData = [];

        foreach ($aGroups as $aGroup) {
            $aGroupsData[] = [
                'value' =>  $aGroup['id'],
                'name'  =>  $aGroup['name']
            ];
        }

        $pForm->AddSelect('group', $aConfig['lang'][$_GET['module']]['group'], [], $aGroupsData, $aData['utype'], '');

        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_0']).
            '&nbsp;&nbsp;'.
            $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

        return ShowTable($pForm->ShowForm());
    }

    function InsertToGroup($pSmarty) {
        $aUsersId = unserialize($_POST['add']);
        $iGroupId = $_POST['group'];

        foreach ( $aUsersId as $iUserId ) {
            if ( $this->pUsersGroups->userInGroup($iUserId, $iGroupId) === false ) {
                $this->pUsersGroups->addUserToGroup($iUserId, $iGroupId);
            }
        }

        return $this->Show($pSmarty);
    }

    private function Details($pSmarty, $iId)
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        $aHeader = array(
            'header' => sprintf(_('Użytkownicy w grupie')),
            'refresh' => true,
            'search' => false,
            'checkboxes' => false
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable'
        );
        $aRecordsHeader = array(
            array(
                'db_field'	=> 'login',
                'content'	=> $aConfig['lang'][$_GET['module']]['user_name'],
                'sortable'	=> true
            ),
            array(
                'db_field'	=> 'surname',
                'content'	=> $aConfig['lang'][$_GET['module']]['surname'],
                'sortable'	=> true
            ),
            array(
                'db_field'	=> 'name',
                'content'	=> $aConfig['lang'][$_GET['module']]['name'],
                'sortable'	=> true
            ),
            array(
                'content' => $aConfig['lang']['common']['action'],
                'sortable' => false,
                'width' => '25'
            )
        );

        $pView = new \View('users_groups', $aHeader, $aAttribs);
        // pobranie liczby wszystkich uzytkownikow
        $aRecords = $this->pUsersGroups->getUsersInGroup($iId);
        $iRowCount = count($aRecords);
        if ($iRowCount > 0) {
            $aColSettings = array(
                'user_id' => array(
                    'show' => false,
                ),
                'group_id' => array(
                    'show' => false,
                ),
                'action' => array(
                    'actions' => array('delete'),
                    'params' => array(
                        'delete' => array('id' => '{user_id}', 'group_id' => '{group_id}', 'do' => 'remove_from_group')
                    ),
                    'show' => false,
                )
            );

            $pView->AddRecords($aRecords, $aColSettings);
        }
        $pView->AddRecordsHeader($aRecordsHeader);
        $aRecordsFooter = array(
            array('go_back')
        );
        $aRecordsFooterParams = array(
            'go_back' => array(1 => array('action', 'id', 'pid'))
        );
        $pView->AddRecordsFooter($aRecordsFooter);
        return $pView->Show();
    }


    function removeFromGroup($pSmarty, $iId) {
        $iGroupId = $_GET['group_id'];

        $this->pUsersGroups->removeUserFromGroup($iId, $iGroupId);

        return $this->Details($pSmarty, $iGroupId);
    }


    function SetAsRead($pSmarty, $iId) {
        $this->pDbMgr->BeginTransaction('profit24');

        $aValues = [
            'user_id' => $_SESSION['user']['id'],
            'alert_id' => $iId,
            '`read`' => 1
        ];

        $pResult = $this->pDbMgr->Insert('profit24', 'users_groups_alert_read', $aValues);

        $sSql = 'SELECT count(C.id) = count(B.id) AS all_read
                 FROM orders_to_review A
                 LEFT JOIN users_to_groups B
                 ON B.group_id = A.group_id
                 LEFT JOIN users_groups_alert_read C
                 ON C.alert_id = A.id
                 WHERE A.id = ' . $iId . ' FOR UPDATE';
        $aRow = $this->pDbMgr->GetRow('profit24', $sSql);

        if ( $aRow['all_read'] === "1" && $pResult !== false ) {
            $aValues = [
                'disabled' => '1'
            ];

            $pResult = $this->pDbMgr->Update('profit24', 'orders_to_review', $aValues, 'id = '.$iId);
        }

        if (false === $pResult) {
            $this->pDbMgr->RollbackTransaction('profit24');
            $this->sMsg .= GetMessage(_('Wystąpił błąd podczas oznaczania jako wiadomość przeczytana'));
            return;
        } else {
            $this->pDbMgr->CommitTransaction('profit24');
            $this->sMsg .= GetMessage(_('Wiadomość została przeczytana'), false);
        }
    }
}