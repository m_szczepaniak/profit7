<?php
/**
 * Klasa wyświetlania listy adresów użytkowników
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-14 
 * @copyrights Marcin Chudy - Profit24.pl
 */
include_once("Konta_common.class.php");
class Module extends Konta_common { //Module__Konta__User_address_list

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// Id strony modulu
	var $iPageId;
  
  // id uzytkownika
  var $iUId;
	
  function __construct(&$pSmarty) {

		$this->iLangId = $_SESSION['lang']['id'];
		$_GET['lang_id'] = $this->iLangId;
		$this->sModule = $_GET['module'];
		$this->iPageId = getModulePageId('m_konta');
    
		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
    
		$sDo = '';

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
    
		if (isset($_GET['pid'])) {
			$this->iUId = $_GET['pid'];
		}
    
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0 || $_SESSION['user']['priv_address_data'] != '1') {
				if (!hasModulePrivileges($_GET['module_id']) || ($_SESSION['user']['priv_address_data'] != '1')){
					showPrivsAlert($pSmarty);
				return;
				}
		}
    
    switch ($sDo) {
      case 'edit':
        $this->Edit($pSmarty, $iId);
      break;
      case 'update':
        $this->Update($pSmarty, $iId);
      break;
      default: $this->Show($pSmarty); break;
    }
  }// end of __construct() method
  
  
  /**
   * Metoda wyświetla listę adresów
   * 
   * @global array $aConfig
   * @param type $pSmarty
   */
  public function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);
    
		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'], $this->getUserEmail($this->iUId)),
			'refresh'	=> true,
			'search'	=> false,
      'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'address_name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_address_name'],
				'sortable'	=> true,
			),
			array(
				'db_field'	=> 'is_company',
				'content'	=> $aConfig['lang'][$this->sModule]['list_is_company'],
				'sortable'	=> true,
        'width'	=> '180'
			),
			array(
				'db_field'	=> 'default_invoice',
				'content'	=> $aConfig['lang'][$this->sModule]['list_default_invoice'],
				'sortable'	=> true,
				'width'	=> '180'
			),
			array(
				'db_field'	=> 'default_transport',
				'content'	=> $aConfig['lang'][$this->sModule]['list_default_transport'],
				'sortable'	=> true,
				'width'	=> '180'
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang'][$this->sModule]['list_created'],
				'sortable'	=> true,
				'width'	=> '180'
			),
			array(
				'db_field'	=> 'modified',
				'content'	=> $aConfig['lang'][$this->sModule]['list_modified'],
				'sortable'	=> true,
				'width'	=> '180'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '65'
			)
    );
    
    // pobranie liczby wszystkich uzytkownikow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."users_accounts_address
						 WHERE 1 = 1 AND 
                   user_id = ".$this->iUId;
    $iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."users_accounts_address 
               WHERE user_id = ".$this->iUId;
			$iRowCount = intval(Common::GetOne($sSql));
		}

		$pView = new View('users', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
    
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;
      
      // pobranie wszystkich rekordow
			$sSql = "SELECT id, address_name, is_company, default_invoice, default_transport, created, modified
               FROM users_accounts_address
               WHERE user_id = ".$this->iUId.
							 ' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
      foreach ($aRecords as $iKey => $aRecord) {
        $aRecords[$iKey]['is_company'] = $aConfig['lang'][$this->sModule]['is_company_'.$aRecord['is_company']];
        $aRecords[$iKey]['default_invoice'] = $aConfig['lang']['common'][$aRecord['default_invoice']];
        $aRecords[$iKey]['default_transport'] = $aConfig['lang']['common'][$aRecord['default_transport']];
        
      }
      
			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id' => array (
					'show'	=> false
				),
        'address_name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit'),
					'params' => array (
												'edit'	=> array('id' => '{id}'),
											),
					'show' => false,
				)
			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
    }
    
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('go_back'),
			array()
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		);

		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
  }// end of Show() method
  
  
  /**
   * Metoda wyswietla formularz edycji danych użytkownika
   * 
   * @param type $pSmarty
   */
  public function Edit(&$pSmarty, $iId) {
    global $aConfig;
    
		$aData = array();
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

    if (!empty($_POST)) {
      $aData = $_POST;
    } else {
      if ($iId > 0) {
        $aData = $this->_getUserAddressData($this->iUId, $iId);
      }
    }
    
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header'], $this->_getUserAddressName($iId), $this->getUserEmail($this->iUId));
		
		$pForm = new FormTable('products', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>155), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
    
		$pForm->AddCheckBox('is_company', $aConfig['lang'][$this->sModule]['is_company'], array('onclick' => 'ChangeObjValue(\'do\', \'edit\'); this.form.submit();'), '', !empty($aData['is_company']), false);
    
    if ($aData['is_company'] == '1') {
      $pForm->AddText('company', $aConfig['lang'][$this->sModule]['company'], $aData['company'], array('style'=>'width: 120px;'), '', 'text', '', false);
    } else {
      $pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('style'=>'width: 120px;'), '', 'text', '', false);
      $pForm->AddText('surname', $aConfig['lang'][$this->sModule]['surname'], $aData['surname'], array('style'=>'width: 120px;'), '', 'text', '', false);
    }
    $pForm->AddText('street', $aConfig['lang'][$this->sModule]['street'], $aData['street'], array('style'=>'width: 120px;'), '', 'text', '', false);
    $pForm->AddText('number', $aConfig['lang'][$this->sModule]['number'], $aData['number'], array('style'=>'width: 30px;'), '', 'text', '', false);
    $pForm->AddText('number2', $aConfig['lang'][$this->sModule]['number2'], $aData['number2'], array('style'=>'width: 40px;'), '', 'text', '', false);
    $pForm->AddText('postal', $aConfig['lang'][$this->sModule]['postal'], $aData['postal'], array('style'=>'width: 50px;'), '', 'text', '', false);
    $pForm->AddText('city', $aConfig['lang'][$this->sModule]['city'], $aData['city'], array('style'=>'width: 100px;'), '', 'text', '', false);
    $pForm->AddText('nip', $aConfig['lang'][$this->sModule]['nip'], $aData['nip'], array('style'=>'width: 80px;'), '', 'text', '', false);
    $pForm->AddText('phone', $aConfig['lang'][$this->sModule]['phone'], $aData['phone'], array('style'=>'width: 80px;'), '', 'text', '', false);
    
    $pForm->AddCheckBox('default_invoice', $aConfig['lang'][$this->sModule]['default_invoice'], array(), '', !empty($aData['default_invoice']), false);
    $pForm->AddCheckBox('default_transport', $aConfig['lang'][$this->sModule]['default_transport'], array(), '', !empty($aData['default_transport']), false);
    
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
  }// end of Edit() method
  
  
  /**
   * Metoda aktualizuje dane adresowe
   * 
   * @global array $aConfig
   * @param type $pSmarty
   * @param type $iId
   */
  public function Update($pSmarty, $iId) {
    global $aConfig;
    $bIsErr = false;
    
    Common::BeginTransaction();
    
    $aValues  = array(
        'is_company' => $_POST['is_company'] === '1' ? '1' : '0',
        'is_invoice' => $_POST['default_invoice'] === '1' ? '1' : '0',
        'default_invoice' => $_POST['default_invoice'] === '1' ? '1' : '0',
        'is_transport' => $_POST['default_transport'] === '1' ? '1' : '0',
        'default_transport' => $_POST['default_transport'] === '1' ? '1' : '0',
        'company' => isset($_POST['company']) ? $_POST['company'] : 'NULL',
        'name' => isset($_POST['name']) ? $_POST['name'] : 'NULL',
        'surname' => isset($_POST['surname']) ? $_POST['surname'] : 'NULL',
        'street' => $_POST['street'],
        'number' => $_POST['number'],
        'number2' => $_POST['number2'],
        'postal' => $_POST['postal'],
        'city' => $_POST['city'],
        'nip' => $_POST['nip'],
        'phone' => $_POST['phone'],
        'modified' => 'NOW()'
    );
    if ($_POST['default_invoice'] == '1') {
      $sSql = "UPDATE users_accounts_address 
               SET default_invoice = '0', is_invoice = '0'
               WHERE user_id = ".$this->iUId;
      if (Common::Query($sSql) === false) {
        $bIsErr = true;
      }
    }
    
    if ($_POST['default_transport'] == '1') {
      $sSql = "UPDATE users_accounts_address 
               SET default_transport = '0', is_transport = '0'
               WHERE user_id = ".$this->iUId;
      if (Common::Query($sSql) === false) {
        $bIsErr = true;
      }
    }
    
    
    if (Common::Update('users_accounts_address', $aValues, ' id='.$iId) === false) {
      $bIsErr = true;
    }
    
    if ($bIsErr === false) {
			Common::CommitTransaction();
      // rekord zostal zmieniony
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$this->_getUserAddressName($iId),
                      $this->getUserEmail($this->iUId));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
    } else {
      Common::RollbackTransaction();
			// rekord nie zostal zmieniony
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$this->_getUserAddressName($iId),
                      $this->getUserEmail($this->iUId));
			$this->sMsg = GetMessage($sMsg);
      // dodanie informacji do logow
			AddLog($sMsg);
			$this->Edit($pSmarty, $iId);
    }
  }// end of Update() method
  
  
  /**
   * Metoda pobiera dane adresowe użytkownika
   * 
   * @global array $aConfig
   * @param int $iUId
   * @param int $iId
   * @return array
   */
  private function _getUserAddressData($iUId, $iId) {
    
    $sSql = "SELECT * FROM users_accounts_address WHERE user_id = ".$iUId." AND id = ".$iId;
    return Common::GetRow($sSql);
  }// end of _getUserAddressData() method
  
  
  /**
   * Metoda pobiera email użytkownika
   * 
   * @param int $iUId
   * @return string - email
   */
  private function _getUserAddressName($iId) {
    
    $sSql = "SELECT address_name
             FROM users_accounts_address 
             WHERE id = ".$iId;
    return Common::GetOne($sSql);
  }// end of _getUserAddressName() method
}// end of class
?>