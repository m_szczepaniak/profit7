<?php

/*
 * @author Marcin Janowski <janowski@windowslive.com>
 * @created 2015-06-05 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_sort_out;

use LIB\orders\counter\OrdersCounter;
use orders\getTransportList;
use Admin;
use DatabaseManager;
use FormTable;
use Module;
use omniaCMS\lib\interfaces\ModuleEntity;
use orders\Shipment;
use Smarty;
use stdClass;

class Module__zamowienia_magazyn_sort_out__get_label implements ModuleEntity
{

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;
    private $sModule;

    /**
     *
     * @var Module
     */
    private $oModuleZamowienia;

    function __construct(Admin $oClassRepository)
    {
        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;

        include_once('modules/m_zamowienia/Module.class.php');
        $pSmartyyyy = $oParent = new stdClass();
        $this->oModuleZamowienia = new Module($pSmartyyyy, $oParent, true);
    }


    public function doDefault()
    {
        return $this->getCheckTransportNumberView();
    }

    /**
     * @return string
     */
    public function getMsg()
    {

        $sJS = '<script>
      $(function() {
        var komunikat = $(".message1").html();
        if (komunikat !== undefined && komunikat !== "") {
          new Messi(komunikat, {title: "Błąd", modal: true});
        }
      });
      </script>';
        return $this->sMsg . $sJS;
    }

    private function setFontWeight($sStr)
    {
        $sStr = '<span style="font-size: 40px!important;">' . $sStr . '</span>';
        return $sStr;
    }

    /**
     * Metoda wyświetla formularz oraz loga kurierów
     * @return type
     */
    private function getCheckTransportNumberView()
    {
        global $aConfig, $pDbMgr;

        $bIsErr = false;

        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('get_transport_number', _('Zeskanuj nr listu przewozowego'));
        $pForm->AddText('transport_number', _('Nr listu'), '', array('style' => 'font-size: 26px; width: 400px;', 'autofocus' => 'autofocus'));
        $pForm->AddHidden('do', 'default');

        $sTransportNumber = $_POST['transport_number'];
        if ($sTransportNumber != '') {

            $this->pDbMgr->BeginTransaction('profit24');
            $aSingleOrderData = $this->getOrderData($sTransportNumber);
            if (empty($aSingleOrderData)) {
                $this->sMsg .= GetMessage($this->setFontWeight(_('Podany numer listu jest nieprawidłowy lub jest już nadany list przewozowy od przewoźnika!')));
                $bIsErr = true;
            } else {
                if (false === in_array($aSingleOrderData['order_status'], ['2', '3'])) {
                    $sMsg = _('!!! Status zamówienia "' . $aSingleOrderData['order_number'] . '" jest nieprawidłowy !!!');
                    $this->sMsg .= GetMessage($this->setFontWeight($sMsg));
                    AddLog($sMsg);
                    $this->pDbMgr->RollbackTransaction('profit24');
                    return '';
                }
            }

            if ($bIsErr === TRUE) {
                $this->pDbMgr->RollbackTransaction('profit24');
            } else {

                $orderLinkedOrders = $this->getOrdersLinked($aSingleOrderData['id']);
                if (!empty($orderLinkedOrders)) {
                    $aSingleOrderData['linked_orders'] = $orderLinkedOrders;
                }

                try {
                    // pobieramy etykietę
                    $oGetTransportList = new \orders\getTransportList($pDbMgr);
                    $oGetTransportList->getSimpleTransportAddressLabel($aSingleOrderData, $aSingleOrderData['linked_orders'], false, true);

                    $aSingleOrderData = $this->getOnlyOrderData($aSingleOrderData['id']);
                    $oShipment = new Shipment($aSingleOrderData['transport_id'], $pDbMgr);
                    $sFilePathName = $oShipment->getTransportListFilename($aConfig, $aSingleOrderData['transport_number']);
                    if (file_exists($sFilePathName)) {
                        // pobianie PDF'a
                        $this->pDbMgr->CommitTransaction('profit24');
                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment; filename="' . $this->getFileNameByFilePath($sFilePathName) . '"');
                        header('Content-Transfer-Encoding: binary');
                        echo file_get_contents($sFilePathName);
                        exit();
                    } else {
                        throw new \Exception("Brak pliku");
                    }
                } catch (\Exception $ex) {
                    $sMsg = $ex->getMessage();
                    $this->sMsg .= GetMessage($this->setFontWeight($sMsg));
                    AddLog($sMsg);
                    $this->pDbMgr->RollbackTransaction('profit24');
                    return '';
                }
            }
        }
        $this->pDbMgr->RollbackTransaction('profit24');

        $pForm->AddInputButton('save', _('Dalej &raquo;'), array('style' => 'font-size: 25'));
        return $pForm->ShowForm();
    }//end of getCheckTransportNumberView()

    /**
     * Pobieramy nazwę pliku ze ścieżki
     *
     * @param string $sFilePathName
     * @return string
     */
    private function getFileNameByFilePath($sFilePathName)
    {

        $aData = explode(DIRECTORY_SEPARATOR, $sFilePathName);
        return array_pop($aData);
    }


    /**
     * Pobiera dane zamówienia
     * @external
     *
     * @param int $iOrderId - id zamowienia
     * @return array
     */
    public function getOnlyOrderData($iOrderId)
    {
        global $aConfig;

        $sSql = "SELECT *
						FROM " . $aConfig['tabls']['prefix'] . "orders
						WHERE id = " . $iOrderId;
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }



    /**
     * Metoda pobiera powiązane zamówienia z podanym,
     * @external
     *
     * @global DatabaseManager $pDbMgr
     * @param int $iOrderItemId
     * @return array
     */
    public function getOrdersLinked($iOrderItemId)
    {
        global $pDbMgr;

        $sSql = 'SELECT O.*
             FROM orders_linked_transport AS OLT
             JOIN orders AS O
              ON O.id = OLT.linked_order_id
             WHERE OLT.main_order_id = ' . $iOrderItemId;
        return $pDbMgr->GetAll('profit24', $sSql);
    }
// end of getOrdersLinked() method


    /**
     * Metoda zwraca id zamówienia oraz transport_id
     * @param type $sTransportNumber
     * @return type
     */
    private function getOrderData($sTransportNumber)
    {

        $sSql = 'SELECT O.*
            FROM orders AS O
            JOIN orders_transport_means AS OTM
              ON OTM.id = O.transport_id 
            WHERE O.transport_number = "' . $sTransportNumber . '"
              AND O.tmp_transport_number = "1"
            ORDER BY O.id DESC
            LIMIT 1
            ';

        return $this->pDbMgr->GetRow('profit24', $sSql);
    }
}// end of Module_zamowienia_magazyn_pakowanie_sorting()
