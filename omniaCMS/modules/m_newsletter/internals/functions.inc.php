<?php
/**
 * Biblioteka funkcji modulu Newsletter
 */


/**
 * Metoda wyszukuje linki w tresci TXT oraz HTML newslettera
 * dodaje je do bazy danych oraz zamienia na linki sledzace
 * 
 * @param	integer	$iNId	- Id newslettera
 * @param	string	$sTxt	- tresc TXT
 * @param	string	$sHtml	- tresc HTML
 * @return	void
 */
function extractLinks($iNId, &$sTxt, &$sHtml, $bAuto=false) {
	global $aConfig;
	$aLinks = array();
	
	if ($sTxt != '') {
		// newsletter tekstowy
		// dodanie linek z tresci do bazy danych i zwrocenie odpowiednio spreparowanego adresu
		$sTxt = preg_replace('/(http:\/\/(www\.)?|www\.)\S+/e', 'addNewsletterLink('.$iNId.', "\\0", false, $aLinks)', $sTxt);
	}
	if ($sHtml != '') {
		// newsletter HTML
		// dodanie linek z tresci do bazy danych i zwrocenie odpowiednio spreparowanego adresu
		$sHtml = preg_replace('/href[\s]*=[\s]*"((http:\/\/(www\.)?|www\.)\S+)"/e', 'addNewsletterLink('.$iNId.', "\\1", true, $aLinks)', $sHtml);
	}
	
	if (!empty($aLinks) && !$bAuto) {
		// usuniecie duplikatow linek
		$aLinks = array_unique($aLinks);
		// usuniecie juz nieistniejacych w newsletterze linkow, ktore sa ciagle w bazie
		// np. w wyiku zmian w newsletterze zostaly z niego usuniete
		deleteOldLinks($iNId, implode(',', $aLinks));
	}
	//return $sString;
} // end of extractLinks() method
	
	
/**
 * Metoda zapisuje do bazy link oraz zwraca odpowiednio spreparowany
 * linki sledzacy
 * 
 * @param	integer	$iNId	- Id newslettera
 * @param	string $sLink	- link
 * @param	bool	$bHtml	- true: html; false: txt
 * @param	array ref	$aLinks	- tablica z informacjami o Id znalezionych linkow
 * @return string
 */
function addNewsletterLink($iNId, $sLink, $bHtml=false, &$aLinks) {
	global $aConfig;
	
	// sprawdzenie czy taki link juz istnieje w bazie
	$sSql = "SELECT id
					 FROM ".$aConfig['tabls']['prefix']."newsletters_links
					 WHERE newsletter_id = ".$iNId." AND
					 			 link = '".$sLink."'";
	$iId = (double) Common::GetOne($sSql);
	if ($iId == 0) {
		// linku jeszcze nie ma zapisanego - dodanie
		$aValues = array(
			'newsletter_id' => $iNId,
			'link' => $sLink
		);
		$iId = Common::Insert($aConfig['tabls']['prefix']."newsletters_links", $aValues);
	}
	if ($iId == 0) {
		// cos poszlo nie tak - nie udalo sie odnalezc ani dodac linku
		return $sLink;
	}
	// dodanie informacji o uzywanym linku
	$aLinks[] = $iId;
	// formatowanie linku sledzacego
	$sLink = $aConfig['common']['client_base_url_http'].'ns_go,'.$iNId.','.$iId.'.html';
	if ($bHtml) {
		return 'href="'.$sLink.'"';
	}
	return $sLink;
} // end of addNewsletterLink() method


/**
 * Metoda usuwa dla newslettera o Id $iNId te linki ktorych Id nie znajduje
 * sie na liscie $siIds
 * 
 * @param	integer	$iNId	- Id newslettera
 * @param	string	$sIds	- lista Id linkow ktore maja pozostac
 * @return void
 */
function deleteOldLinks($iNId, $sIds) {
	global $aConfig;
	
	$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_links
					 WHERE newsletter_id = ".$iNId." AND
					 			 id NOT IN (".$sIds.")";
	Common::Query($sSql);
} // end of deleteOldLinks() method


/**
 * Funkcja przygotowuje tresc newslettera do wyslania go z jego obrazkami w formie
 * zalacznikow do maila. Zwraca tablice wszystkich obrazkow z maila, zamienia ich wystapienia
 * w tresci z URLi na ich nazwy
 * 
 * @param	array string	$sHtml	- tresc HTML newslettera
 * @return 	array
 */
function extractHTMLImages(&$sHtml) {
	global $aConfig;
	$aHTMLImages = array();
	$aProcessedImages = array();
	
	// ekstrakcja linkow do obrazkow
	preg_match_all('/<img[^>]*src="([^"]+)"[^>]*>/', $sHtml, $aMatches);
	
	// okresleie samego hosta - bez http i bez www
	$sHost = str_replace('/', '', str_replace('.', '\.', str_replace('www.', '', $aConfig['common']['client_base_url'])));
	foreach ($aMatches[1] as $iKey => $sMatch) {
		// usuniecie http:// lub https://
		$sMatchNoHttp = preg_replace('/^https?:\/\//', '', $sMatch);
		if ($sMatch != '' && preg_match('/^(www\.)?'.$sHost.'/', $sMatchNoHttp) && !in_array($sMatch, $aProcessedImages)) {
			// wyciagniecie samej nazwy
			$sName = array_pop(explode('/', $sMatch));
			// zamiana URL na nazwe pliku w ciagu
			$sHtml = str_replace($sMatch, $sName, $sHtml);
			
			// zamiana URL na sciezke do pliku na dysku
			$sPath = preg_replace('/^(www\.)?'.$sHost.'\//', $aConfig['common']['client_base_path'], $sMatchNoHttp);
			// okreslenie typu obrazka
			$aImage = getimagesize($sPath);
			$sType = $aImage['mime'];
			$aHTMLImages[] = array($sPath, $sType);
			// dodanie do tablicy tymczasowej
			$aProcessedImages[] = $sMatch;
		}
	}
	return $aHTMLImages;
} // end of extractHTMLImages() function


/**
 * Metoda dodaje do kazdego linku sledzacego adres email uzytkownika
 * 
 * @global array $aConfig
 * @param string $sEmail
 * @param string $sInTxt - wejściowy tekstowy mail
 * @param string $sInHtml - wejściowy html mail
 * @param string $sOutTxt - wyjście testowe REFERENCJA
 * @param string $sOutHtml - wyjście htmlowe REFERENCJA
 */
function addEmailToLinks($sEmail, $sInTxt, $sInHtml, &$sOutTxt, &$sOutHtml) {
	global $aConfig;
	
	// zamiana linek sledzacych na linki sledzace + adres email
	$sEmail = base64_encode($sEmail);
	if ($sInTxt != '') {
		$sOutTxt = preg_replace('/('.str_replace('/', '\/', $aConfig['common']['client_base_url_http']).'ns_go\,\d+,\d+)\.html/', "\\1,".$sEmail.".html", $sInTxt);
	}
	if ($sInHtml != '') {
		$sOutHtml = preg_replace('/('.str_replace('/', '\/', $aConfig['common']['client_base_url_http']).'ns_go\,\d+,\d+)\.html/', "\\1,".$sEmail.".html", $sInHtml);
	}
} // end of addEmailToLinks() method

function addHTMLContainer(&$sHtml, $sUnsubscribe = '') {
	global $aConfig, $pSmarty;
	
	if ($sHtml != '') {
		$pSmarty->assign('sContent', $sHtml);
    $pSmarty->assign('sUnsubscribe', $sUnsubscribe);
		$sHtml = $pSmarty->fetch($aConfig['common']['client_base_path'].$aConfig['smarty']['template_dir'].'modules/m_newsletter/newsletters/html_template.tpl');
		$pSmarty->clear_assign('sContent');
    $pSmarty->clear_assign('sUnsubscribe');
	}
} // end of addHTMLContainer() function
?>