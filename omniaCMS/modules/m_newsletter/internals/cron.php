<?php
// dolaczenie biblioteki funkcji modulu
include_once('functions.inc.php');

// liczba maili w wysylanym pakiecie
$iMails = 500;
// czas w sekundach przerwy pomiedzy pakietami
$iSleep = 5;
// ukoczone wysylanie
$bFinished = true;
// katalog dla plikow 

/**
 * Funkcja zapisuje logi rozsyłki newslettera, i zwiększa licznik auto newslettera
 * 
 * @global array $aConfig
 * @param integer $iNId - id auto newslettera
 * @param string $sEmail
 */
function writeEmail($iNId, $sEmail) {
	global $aConfig;
	
	$sLogFile = $aConfig['common']['client_base_path'].'omniaCMS/logs/newsletter_'.$iNId.'.txt';
	if ($rHandle = fopen($sLogFile, "a")) {
		fwrite($rHandle, "[ ".date('Y-m-d H:i:s')." ]\t".$sEmail."\t".memory_get_usage()."\t".memory_get_usage(true)."\t".memory_get_peak_usage()."\t".memory_get_peak_usage(true)."\t".getRemainingTime()."\r\n");
		fclose($rHandle);
	}
	//sleep(1);
} // end of writeEmail() function


/**
 * Metoda pobiera konfigurację serwisu
 * 
 * @return array
 */
function _getWebsiteSettings() {
  
  $sSql = "SELECT * 
           FROM website_settings";
  return Common::GetRow($sSql);
}// end of _getWebsiteSettings() method

/**
 * Metoda aktualizuje status
 * 
 * @global array $aConfig
 * @param int $iPId
 * @param int $iId
 * @param int $iStatus
 * @param bool $bUpdateDate
 */
function updateStatus($iPId, $iId, $iStatus, $bUpdateDate=false) {
	global $aConfig;
	// aktualizacja newslettera
	$aValues = array(
		'sending_status' => $iStatus
	);
	if ($bUpdateDate) {
		$aValues['sent'] = 'NOW()';
	}
	Common::Update($aConfig['tabls']['prefix']."newsletters", $aValues, "id = ".$iId." AND page_id = ".$iPId);
}// end of updateStatus() method


/**
 * Metoda pobiera aktualnych odbiorców newslettera, do których należy wysłać mail
 * 
 * @global array $aConfig
 * @param int $iId
 * @return object
 */
function getRecipients($iId) {
	global $aConfig;
	
	$sSql = "SELECT recipient_id AS id, email
					 FROM ".$aConfig['tabls']['prefix']."newsletters_jobs
					 WHERE newsletter_id = ".$iId;
	return Common::PlainQuery($sSql);
}// end of getRecipients() method


/**
 * Metoda usuwa odbiorców newslettera z listy do wysłania
 * 
 * @global array $aConfig
 * @param int $iNId
 * @param int $iId
 */
function deleteRecipient($iNId, $iId) {
	global $aConfig;
	
	$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_jobs
					 WHERE recipient_id = ".$iId." AND
					 			 newsletter_id = ".$iNId;
	Common::Query($sSql);
}// end of deleteRecipient() method

$aWebsiteSettings = _getWebsiteSettings();


// usuwanie starych niepotwierdzonych adresow email z bazy subskrybentow
// pobranie liczby dni przeznaczonych na aktywacje
$sSql = "SELECT A.page_id, A.days_to_confirm, B.symbol
				 FROM ".$aConfig['tabls']['prefix']."newsletters_settings AS A
				 JOIN ".$aConfig['tabls']['prefix']."menus_items AS B
				 ON B.id = A.page_id";
$aCfg = Common::GetAssoc($sSql);
foreach ($aCfg as $iPId => $aSettings) {
  
  include_once('MailNewsletter.class.php');
  $oMailsNewsletter = new MailNewsletter();
  
  /**
   * WYWALAMY
  
	// czyszczenie adresow email
	$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
					 WHERE page_id = ".$iPId." AND
            confirmed = '0' AND
            DATE_ADD(added, INTERVAL ".$aSettings['days_to_confirm']." DAY) < NOW()";
	Common::Query($sSql);
	*/
  
	// pobranie tematu, numeru i tresci newslettera do wyslania
	// pobierany jest tylko newsletter ktory nie zostal jeszcze wyslany
	// oraz nie jest przetwarzany przez inne wywolanie Crona (sending_status = 1)
	$sSql = "SELECT A.id, A.category_id AS cid, A.subject, A.content_txt, A.content_html, A.html_images, A.sending_status, B.name AS category_name, B.sender, B.sender_email, A.unsubscribe_button
					 FROM ".$aConfig['tabls']['prefix']."newsletters AS A
					 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories AS B
					 ON B.id = A.category_id AND
					 B.page_id = A.page_id
					 WHERE A.page_id = ".$iPId." AND
					 			 A.start_sending IS NOT NULL AND
					 			 A.start_sending <= NOW() AND
					 			 A.sending_status <> '1' AND A.sending_status <> '3'
					 ORDER BY A.sending_status DESC
					 LIMIT 0, 1";
	$aNewsletter =& Common::GetRow($sSql);
	
	if (!empty($aNewsletter)) {
		// usuniecie polskich znakow z tytulu
		$aNewsletter['subject'] = changeCharcodes($aNewsletter['subject'], 'utf8', 'no');
    
    
		
		if (((int) $aNewsletter['sending_status']) == 0) {
			// usuniecie aktualnego wpisu w zadaniach
			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_jobs
							 WHERE newsletter_id = ".$aNewsletter['id'];
			if (Common::Query($sSql) !== false) {
				// poczatek rozsylki tego newslettera - dodanie subskrybentow
				// do tablicy newsletters_jobs
				$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."newsletters_jobs
								 (SELECT ".$aNewsletter['id'].", B.id, B.email
								 	FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat AS A
								 	JOIN ".$aConfig['tabls']['prefix']."newsletters_recipients AS B
								 	ON B.id = A.recipient_id
								 	WHERE A.category_id = ".$aNewsletter['cid']." AND
								 				B.confirmed = '1')";
				if (Common::Query($sSql) === false) {
					$bIsErr = false;
					break;
				}
			}
			else {
				break;
			}
		}
		// POCZATEK ROZSYLKI
		// ustawienie statusu na 1 - w trakcie rozsylki
		updateStatus($iPId, $aNewsletter['id'], 1);

    
    
		// wybranie odbiorcow
		$oRecipients =& getRecipients($aNewsletter['id']);
		// dodanie do wszystkich linkow zrodla przejscia do sklepu - link sledzacy
		// wraz z Id newslettera
		extractLinks($aNewsletter['id'], $aNewsletter['content_txt'], $aNewsletter['content_html']);
		if ($aNewsletter['html_images'] == '1') {
			// ekstrakcja obrazkow HTML
			$aHTMLImages = extractHTMLImages($aNewsletter['content_html']);
		}
    if ($aNewsletter['unsubscribe_button'] == '1') {
      // dodanie kontenera HTMLowe i stopki wypisania
      addHTMLContainer($aNewsletter['content_html'], $aWebsiteSettings['mail_footer_unsubscribe']);
    } else {
      // dodanie kontenera HTMLowe bez stopki wypisania
      addHTMLContainer($aNewsletter['content_html']);
    }
		
		
		
		$iCounter = 0;
    
    if ($aNewsletter['html_images'] == '1' && !empty($aHTMLImages)) {
      $oMailsNewsletter->addHTMLImage($aHTMLImages);
    }
    $oMailsNewsletter->setBackend();

		while ($aRecipient =& $oRecipients->fetchRow(DB_FETCHMODE_ASSOC)) {
			if (getRemainingTime() < 10) {
				// pozostalo mniej niz 10 sekund do przekroczenia maksymalnego czasu
				// wykonania skryptu
				// ustawienie statusu na 2 i przerwanie rozsylki
				updateStatus($iPId, $aNewsletter['id'], 2);
				// nie oznaczaj jako wyslany
				$bFinished = false;
				break;
			}
			else {
				$sTxt = '';
				$sHtml = '';

				// link edycji ustawie newslettera
				$sEditLink = $aConfig['common']['base_url_http'].$aSettings['symbol'].'/edit,'.md5($aRecipient['id'].':'.$aRecipient['email']).'.html';
				// link usuwaj�cy
				$sDeleteLink = $aConfig['common']['base_url_http'].$aSettings['symbol'].'/delete,'.md5($aRecipient['id'].':'.$aRecipient['email']).'.html';
        
        // link usuniecia zgody na rozsyłkę kodów rabatowych
        $sDeleteCodeSubscribe = $aConfig['common']['base_url_http'].$aSettings['symbol'].'/delete_promotion_agreement,'.md5($aRecipient['id'].':'.$aRecipient['email']).'.html';
        
				// dodanie adresu email do linek sledzacych
				addEmailToLinks($aRecipient['email'], $aNewsletter['content_txt'], $aNewsletter['content_html'], $sTxt, $sHtml);
				// zamiana znacznikow na linki w TXT
//				$sTxt = str_replace('{delete_link}', $sDeleteLink, str_replace('{edit_link}', $sEditLink, $sTxt));
				// zamiana znacznikow na linki w HTML
        $sHtml = str_replace('{delete_link}', $sDeleteLink, str_replace('{edit_link}', $sEditLink, $sHtml));
        $sHtml = str_replace('{link_zrezygnuj}', $sDeleteCodeSubscribe, $sHtml);
        
				// wysylka maila
        $oMailsNewsletter->setHTMLBody($sHtml);
        $oMailsNewsletter->setGetParams();
        $oMailsNewsletter->setHeaderData($aNewsletter['sender'],$aNewsletter['sender_email'], $aNewsletter['subject']);
        
        if ($aConfig['common']['status'] == 'development') {
          dump($aRecipient);
//          dump($aRecipient['email']);
          //$oMailsNewsletter->sendNewsletterMail($aRecipient['email']);
//          $oMailsNewsletter->sendNewsletterMail('arekgolba@gmail.com');
        } else {
          $oMailsNewsletter->sendNewsletterMail($aRecipient['email']);
        }

        
        
//				Common::sendTxtHtmlMail($aNewsletter['sender'], $aNewsletter['sender_email'], $aRecipient['email'], $aNewsletter['subject'], $sTxt, $sHtml, '', '', $aHTMLImages);
				// zapis do pliku
				writeEmail($aNewsletter['id'], $aRecipient['email']);
				// usuwanie maila z zadan
				deleteRecipient($aNewsletter['id'], $aRecipient['id']);
				// jezeli wyslano 500 - 5 sekund przerwy
				if ((++$iCounter) % $iMails == 0) {
					sleep($iSleep);
				}
			}
		}
		
		if ($bFinished) {
			// nie ma juz do kogo wysylac, ustawienie statusu na 3 i czasu wysylki
			updateStatus($iPId, $aNewsletter['id'], 3, true);
		}
		unset($oRecipients);
		unset($aRecipient);
		unset($aNewsletter);
	}
}
?>