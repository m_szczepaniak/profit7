<?php
/**
 * Klasa Module do obslugi modulu 'strony newslettera - kategorie'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {

			case'opinion_clicks': $this->OpinionClicks($pSmarty, $iPId); break;
			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych dla strony kategorii newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> false,
			'checkboxes'=>false										 
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'send_date',
				'content'	=> $aConfig['lang'][$this->sModule]['list_send_date'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'type',
				'content'	=> $aConfig['lang'][$this->sModule]['type'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'sent_count',
				'content'	=> $aConfig['lang'][$this->sModule]['list_sent_count'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'clicked',
				'content'	=> $aConfig['lang'][$this->sModule]['list_clicks'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);

		// pobranie liczby wszystkich kategorii strony
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."orders_opinions_sent A
						 WHERE 1=1 ".
									(isset($_POST['f_type']) && $_POST['f_type'] != '' ? (' AND A.type = \''.$_POST['f_type'].'\'') : '').
									(isset($_POST['f_date_from']) && $_POST['f_date_from'] != '' ? (' AND A.send_date >= \''.date('Y-m-d', strtotime($_POST['f_date_from'])).'\'') : '').
										 (isset($_POST['f_date_to']) && $_POST['f_date_to'] != '' ? (' AND A.send_date <= \''.date('Y-m-d', strtotime($_POST['f_date_to'])).'\'') : '');
							 
		$sSql.=' GROUP BY send_date, type';		
		$iRowCount=0;
		$aCountRow=Common::GetAll($sSql);	
		foreach($aCountRow as $aRow)
			++$iRowCount;
		
		
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(A.id)
							 FROM ".$aConfig['tabls']['prefix']."orders_opinions_sent A GROUP BY send_date, type";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('categories', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		$sDateOfFirstOrder=Common::GetOne('SELECT send_date FROM `orders_opinions_sent` ORDER BY send_date ASC LIMIT 1');
		
		//filtr daty
		$pView->AddFilter('f_date_from', $aConfig['lang'][$this->sModule]['f_date_from'], '',  empty($_POST['f_date_from'])?date('d-m-Y',strtotime($sDateOfFirstOrder)):$_POST['f_date_from'], 'date');
		$pView->AddFilter('f_date_to', $aConfig['lang'][$this->sModule]['f_date_to'], '', $_POST['f_date_to'], 'date');
		
		$aFType=array(array('label'=>$aConfig['lang'][$this->sModule]['f_type_All'], 'value'=>''),array('label'=>$aConfig['lang'][$this->sModule]['f_type_C'], 'value'=>'C'), array('label'=>$aConfig['lang'][$this->sModule]['f_type_O'], 'value'=>'O'));
		
		$pView->AddFilter('f_type', $aConfig['lang'][$this->sModule]['f_type'], $aFType, $_POST['f_type']);
			
		
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich paragrafow strony
			$sSql = "SELECT id, A.send_date, `type`, COUNT(A.id) AS sent_count,  SUM(A.clicked)-COUNT(A.id) AS clicked
							 FROM ".$aConfig['tabls']['prefix']."orders_opinions_sent A
							 WHERE 1=1".
										 (isset($_POST['f_type']) && $_POST['f_type'] != '' ? (' AND A.type = \''.$_POST['f_type'].'\'') : '').
										 (isset($_POST['f_date_from']) && $_POST['f_date_from'] != '' ? (' AND A.send_date >= \''.date('Y-m-d', strtotime($_POST['f_date_from'])).'\'') : '').
										 (isset($_POST['f_date_to']) && $_POST['f_date_to'] != '' ? (' AND A.send_date <= \''.date('Y-m-d', strtotime($_POST['f_date_to'])).'\'') : '').
							
							' GROUP BY send_date, type
							ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.send_date').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : ' DESC').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			foreach($aRecords as $iKey=>$aRecord){
				$aRecords[$iKey]['type'] = $aConfig['lang'][$this->sModule]['type_'.$aRecord['type']];
				//unset($aRecords[$iKey]['newsletter_id']);
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'newsletter_id'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('clicks'),
					'params' => array (
												'clicks'	=> array('pid' => '{id}', 'action'=>'opinions_clicks')
											),
					'icon' => array(
												'clicks' => 'bars'
											)
				)
				
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		);
		
		
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show());
	} // end of Show() function

//wysweitla liste adresow na jakie zostaly wyslane prosby
	
function OpinionClicks(&$pSmarty, $iId) {
	global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);
		
		$aHeader = array(
			'header' => $aConfig['lang'][$this->sModule]['list_header'].' - '.$_GET['pd'].' - '.$_GET['pt'],
			'refresh' => true,
			'search' => true,
			'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'link',
				'content'	=> $aConfig['lang'][$this->sModule]['list_email'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'clicked',
				'content'	=> $aConfig['lang'][$this->sModule]['list_clicked'],
				'sortable'	=> true,
				'width'	=> '75'
			),
			array(
				'db_field'	=> 'clicked',
				'content'	=> $aConfig['lang'][$this->sModule]['list_clicked_date'],
				'sortable'	=> true,
				'width'	=> '140'
			)
		);
		$_GET['pt']=substr($_GET['pt'],0,1);
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."orders_opinions_sent
						 WHERE send_date = '".$_GET['pd']."' AND `type`='".$_GET['pt']."'";
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."orders_opinions_sent
						 WHERE send_date = '".$_GET['pd']."' AND `type`='".$_GET['pt']."'";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('newsletters_links', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT email, clicked, clicked_date
							 FROM ".$aConfig['tabls']['prefix']."orders_opinions_sent
							 WHERE send_date = '".$_GET['pd']."' AND `type`='".$_GET['pt']."'
							 ORDER BY ".(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id ').
							 (isset($_GET['order']) && !empty($_GET['order']) ? " ".$_GET['order'] : '  DESC').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			foreach ($aRecords as $iKey => $aRecord) {
				$aRecords[$iKey]['clicked']=$aRecords[$iKey]['clicked']==0?'NIE':'TAK';
				$aRecords[$iKey]['clicked_date']=$aRecords[$iKey]['clicked_date']=='0000-00-00 00:00:00'?'':$aRecords[$iKey]['clicked_date'];
				// przycicecie linku
				//$aRecords[$iKey]['link'] = substr($aRecord['link'],0, 100);
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('go_back')
		);
		{
			$aRecordsFooterParams = array(
				'go_back' => array(array('action'=>'opinions_sent'), array())
		
			);
		}
		
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
}	//end of func
	
} // end of Module Class
?>