<?php
/**
 * Klasa Module do obslugi modulu 'Newsletter - newslettery'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// sciezka do katalogu zawierajacego szablony czesci klienckiej modulu
	var $sTemplatesDir;

	// lista szablonow czesci klienckiej modulu
	var $sTemplatesList;

	// konfiguracja modulu
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/modules/'.$this->sModule.'/newsletters';
		$this->sTemplatesList =& GetTemplatesList($this->sTemplatesDir);

		$sDo = '';
		$iId = 0;
		$iPId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
		// sprawdzenie czy newsletter moze zostac wyslany
		// (czy nie zostal wyslany juz wczesniej)
		if (!$this->canBeSent($sDo, $iPId, $iId)) {
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['already_sent']);
			$this->Show($pSmarty, $iPId);
			return;
		}

		// pobranie konfiguracji strony modulu
		$this->aSettings =& $this->getSettings($iPId);

		if (empty($this->aSettings['preview_emails']) || !$this->hasCategories($iPId)) {
			// nie ustawiono nadawcy lub adresu nadawcy mailingu
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_settings']);
			$this->Show($pSmarty, $iPId);
			return;
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iPId, $iId); break;
			case 'save_copy': $this->Insert($pSmarty, $iPId, true); break;
			case 'send_later': $this->PrepareToSend($pSmarty, $iPId, $iId); break;
			case 'send': $this->SendNewsletter($pSmarty, $iPId, $iId); break;
			case 'update_send': $this->UpdateSend($pSmarty, $iPId, $iId); break;
			case 'send_preview': $this->SendPreview($pSmarty, $iPId, $iId); break;
			case 'clicks': $this->ShowLinksClicks($pSmarty, $iPId, $iId); break;

			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda pobiera konfiguracje strony modulu
	 *
	 * @return	array ref	- konfiguracja strony modulu
	 */
	function &getSettings($iPId) {
		global $aConfig;

		// pobranie ustawien newslettera
		$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."newsletters_settings
						 WHERE page_id = ".$iPId;
		return Common::GetRow($sSql);
	} // end of getSettings() method


	/**
	 * Metoda wyswietla liste zdefiniowanych dla strony kategorii newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iPId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'subject',
				'content'	=> $aConfig['lang'][$this->sModule]['list_subject'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'recipients',
				'content'	=> $aConfig['lang'][$this->sModule]['list_recipients'],
				'sortable'	=> false,
				'width'	=> '75'
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'db_field'	=> 'sent',
				'content'	=> $aConfig['lang'][$this->sModule]['sent'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'sent_by',
				'content'	=> $aConfig['lang'][$this->sModule]['sent_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '125'
			)
		);
		$sDeletedSql = '';
		if ($_SESSION['user']['type'] === 0) {
			$sDeletedSql = " AND deleted = '0' ";
		}
		
		// pobranie liczby wszystkich newsletterow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."newsletters
						 WHERE auto_id IS NULL
						 AND page_id = ".$iPId.$sDeletedSql.
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND subject LIKE \'%'.$_POST['search'].'%\'' : '').
									 (isset($_POST['f_category_id']) && $_POST['f_category_id'] != '0' ? ' AND category_id = '.$_POST['f_category_id'] : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."newsletters
							 WHERE auto_id IS NULL
						 		AND page_id = ".$iPId.$sDeletedSql;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('newsletters', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		// filtr wyslane / niewyslane
		$aSent = array(
			array('value' => '-1', 'label' => $aConfig['lang'][$this->sModule]['all']),
			array('value' => '1', 'label' => $aConfig['lang']['common']['yes']),
			array('value' => '0', 'label' => $aConfig['lang']['common']['no'])
		);
		$pView->AddFilter('f_sent', $aConfig['lang'][$this->sModule]['f_sent'], $aSent, $_POST['f_sent']);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich newsletterow
			$sSql = "SELECT A.id, A.subject, CONCAT((SELECT COUNT(B.recipient_id) FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat AS B WHERE B.category_id = A.category_id), ' (', (SELECT COUNT(B.recipient_id) FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat AS B JOIN ".$aConfig['tabls']['prefix']."newsletters_recipients AS C ON C.id = B.recipient_id WHERE B.category_id = A.category_id AND C.confirmed = '1'), ')') AS recipients, A.created, A.created_by, A.sent, A.sent_by, A.sending_status, A.deleted, IFNULL((SELECT SUM(clicked) FROM ".$aConfig['tabls']['prefix']."newsletters_links WHERE newsletter_id = A.id), 0) AS clicks 
							 FROM ".$aConfig['tabls']['prefix']."newsletters AS A
							 WHERE auto_id IS NULL
						 		AND A.page_id = ".$iPId.$sDeletedSql.
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND A.subject LIKE \'%'.$_POST['search'].'%\'' : '').
										 (isset($_POST['f_sent']) && $_POST['f_sent'] != '-1' ? ' AND A.sent '.($_POST['f_sent'] == '1' ? ' IS NOT NULL' : 'IS NULL') : '').
							' ORDER BY A.'.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			foreach ($aRecords as $iKey => $aRecord) {
				//$aRecords[$iKey]['html'] = $aConfig['lang']['common'][$aRecord['html'] == '1' ? 'yes' : 'no'];
				if ($aRecord['clicks'] == '0') {
					// wylaczenie mozliwosci podgladu klikniec w linki
					$aRecords[$iKey]['disabled'][] = 'clicks';
				}
				unset($aRecords[$iKey]['clicks']);
				if ($aRecord['sending_status'] != '0') {
					// wylaczenie mozliwosci ponownego wyslania
					$aRecords[$iKey]['disabled'][] = 'send';
					$aRecords[$iKey]['disabled'][] = 'send_preview';
					$aRecords[$iKey]['disabled'][] = 'send_later';
				}
				if ($aRecord['deleted'] == '1') {
					$aRecords[$iKey]['subject'] = '<span style="color: red;">'.$aRecord['subject'].'</span>';
					$aRecords[$iKey]['disabled'][] = 'send';
					$aRecords[$iKey]['disabled'][] = 'send_preview';
					$aRecords[$iKey]['disabled'][] = 'send_later';
					$aRecords[$iKey]['disabled'][] = 'delete';
				}
				unset($aRecords[$iKey]['deleted']);
				unset($aRecords[$iKey]['sending_status']);
				
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'subject' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'pid' => $iPId, 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('clicks', 'send_preview', 'send_later', 'edit', 'delete'), // 'send'
					'params' => array (
												'clicks'	=> array('pid' => $iPId, 'ppid'=>'{id}', 'action'=>'clicks', 'do'=>''),
												'send_preview'	=> array('pid' => $iPId, 'id'=>'{id}'),
												'send_later'	=> array('pid' => $iPId, 'id'=>'{id}'),
//												'send'	=> array('pid' => $iPId, 'id'=>'{id}'),
												'edit'	=> array('pid' => $iPId, 'id'=>'{id}'),
												'delete'	=> array('pid' => $iPId, 'id'=>'{id}')
											),
					'icon' => array(
												'clicks' => 'bars'
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('add')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		);
		
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane mailingi newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony newslettera
	 * @param	integer	$iId	- Id usuwanego mailingu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['subject'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['subject'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel, getPagePath($iPId, $this->iLangId, true));
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel, getPagePath($iPId, $this->iLangId, true));
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowy newsletter
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	bool	$bCopy	- czy zapisac jako kopie
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId, $bCopy=false) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
		if (trim($_POST['content_txt']) == '' && trim($_POST['content_html']) == '') {
			// brak tresci wiadomosci
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_body_err'], true);
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
	// dodanie newslettera
		$aValues = array(
			'page_id' => $iPId,
			'category_id' => (int) $_POST['category_id'],
			'subject' => ($bCopy ? 'KOPIA ' : '').$_POST['subject'],
			'content_txt' => $_POST['content_txt'] != '' ? $_POST['content_txt'] : 'NULL',
			'content_html' => $_POST['content_html'] != '' ? $_POST['content_html'] : 'NULL',
			'html_images' => isset($_POST['html_images']) ? '1' : '0',
      'unsubscribe_button' => isset($_POST['unsubscribe_button']) ? '1' : '0',
			'created' => 'NOW()',
			'created_by' => $_SESSION['user']['name']
		);
//dump($_POST);
//dump($_FILES);
//		if (!empty($_FILES['attachment']['name'])) {
//			include_once('File.class.php');
//			$oFile = new MyFile();
//			// przeslano plik - przetwarzanie
//				// zamiana spacji i innych znakow na '_'
//				$_FILES['attachment']['name'] = preg_replace('/\s+/', '_', $_FILES['attachment']['name']);
//				//dump($_FILES);
//				$oFile->SetFile($aConfig['common']['client_base_path'].$aConfig['common']['file_dir'], $_FILES['attachment']);
//				$iResult = $oFile->proceedFile($aConfig['common']['file_extensions'], false, isset($_POST['old_attachment']) && $_POST['old_attachment'] == $_FILES['attachment']['name']);
//				if ($iResult != 1) {
//					$sFileName = $_FILES['attachment']['name'];
//					//break;
//				}
//				else {
//					if (isset($_POST['old_attachment']) && !isset($_POST['del_attachment']) && $_POST['old_attachment'] != $_FILES['attachment']['name']) {
//						// poprzedni plik byl inny od nowego - usuniecie
//						@unlink($aConfig['common']['client_base_path'].$aConfig['common']['file_dir'].'/'.$_POST['old_attachment']);
//					}
//					$aValues['attachment']=$_FILES['attachment']['name'];
//	
//				}
//		}
			
		if (($iNId = Common::Insert($aConfig['tabls']['prefix']."newsletters", $aValues)) === false) {
			$bIsErr = true;
		}

		if (!$bIsErr) {
			// newsletter zostal dodany
			$sMsg = sprintf($aConfig['lang'][$this->sModule][($bCopy ? 'copy_' : '').'add_ok'],
											$_POST['subject'],
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// newsletter nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$_POST['subject'],
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			if ($bCopy) {
				$this->AddEdit($pSmarty, $iPId, (double) $_POST['id']);
			}
			else {
				$this->AddEdit($pSmarty, $iPId);
			}
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie newsletter
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id newslettera
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iId);
			return;
		}
		if (trim($_POST['content_txt']) == '' && trim($_POST['content_html']) == '') {
			// brak tresci wiadomosci
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['no_body_err'], true);
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
		
		// aktualizacja newslettera
		$aValues = array(
			'category_id' => (int) $_POST['category_id'],
			'subject' => $_POST['subject'],
			'content_txt' => $_POST['content_txt'] != '' ? $_POST['content_txt'] : 'NULL',
			'content_html' => $_POST['content_html'] != '' ? $_POST['content_html'] : 'NULL',
			'html_images' => isset($_POST['html_images']) ? '1' : '0',
      'unsubscribe_button' => isset($_POST['unsubscribe_button']) ? '1' : '0',
			'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name']
		);
		
//			if (!empty($_FILES['attachment']['name'])) {
//			include_once('File.class.php');
//			$oFile = new MyFile();
//			// przeslano plik - przetwarzanie
//				// zamiana spacji i innych znakow na '_'
//				$_FILES['attachment']['name'] = preg_replace('/\s+/', '_', $_FILES['attachment']['name']);
//				//dump($_FILES);
//				$oFile->SetFile($aConfig['common']['client_base_path'].$aConfig['common']['file_dir'], $_FILES['attachment']);
//				$iResult = $oFile->proceedFile($aConfig['common']['file_extensions'], false, isset($_POST['old_attachment']) && $_POST['old_attachment'] == $_FILES['attachment']['name']);
//				if ($iResult != 1) {
//					$sFileName = $_FILES['attachment']['name'];
//				//	break;
//				}
//				else {
//					if (isset($_POST['old_attachment']) && !isset($_POST['del_attachment']) && $_POST['old_attachment'] != $_FILES['attachment']['name']) {
//						// poprzedni plik byl inny od nowego - usuniecie
//						@unlink($aConfig['common']['client_base_path'].$aConfig['common']['file_dir'].'/'.$_POST['old_attachment']);
//					}
//					$aValues['attachment']=$_FILES['attachment']['name'];
//	
//				}
//		}
//		if (isset($_POST['old_attachment']) && isset($_POST['del_attachment'])) {
//						// poprzedni plik byl inny od nowego - usuniecie
//						@unlink($aConfig['common']['client_base_path'].$aConfig['common']['file_dir'].'/'.$_POST['old_attachment']);
//						$aValues['attachment']='NULL';
//		}
		if (Common::Update($aConfig['tabls']['prefix']."newsletters",
											 $aValues,
											 "id = ".$iId) === false) {
			$bIsErr = true;
		}
		
		if (!$bIsErr) {
			// newsletter zostal dodany
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$_POST['subject'],
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// newsletter nie zostal dodany,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$_POST['subject'],
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda przygotowuje newsletter do wyslania
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iPId	- Id strony newslettera
	 * @param	integer	$iId	- Id newslettera
	 * @return	void
	 */
	function UpdateSend(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->PrepareToSend($pSmarty, $iPId, $iId);
			return;
		}
		// pobranie Id kategorii newslettera
		$iCId = $this->getCategoryId($iPId, $iId);

		// poczatek transakji
		Common::BeginTransaction();

		// aktualizacja daty rozsylki newslettera
		$aValues = array(
			'start_sending' => FormatDateHour($_POST['start_sending_date'], $_POST['start_sending_hour']),
			'sent_by' => $_SESSION['user']['name']
		);
		if (Common::Update($aConfig['tabls']['prefix']."newsletters",
											 $aValues,
											 "id = ".$iId." AND
											  page_id = ".$iPId) === false) {
			$bIsErr = true;
		}

		if (!$bIsErr) {
			// zaktualizowano
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['send_update_ok'],
											getPagePath($iPId, $this->iLangId, true),
											$this->getNewsletterSubject($iPId, $iId));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// nie zaktualizowano,
			Common::RollbackTransaction();
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['send_update_err'],
											getPagePath($iPId, $this->iLangId, true),
											$this->getNewsletterSubject($iPId, $iId));
			$this->sMsg = GetMessage($sMsg);

			AddLog($sMsg);
			$this->PrepareToSend($pSmarty, $iPId, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji kategorii
	 *
	 * @param		object	$pSmarty
	 * @param		string	$sId	- ID strony
	 * @param		integer	$iId	- ID edytowanego akapitu
	 */
	function AddEdit(&$pSmarty, $iPId, $iId=0) {
		global $aConfig;

		$aData = array();
		$aReadOnlyAttr = array();
		$aDisabledAttr = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego newslettera
			$sSql = "SELECT category_id, subject, content_txt, content_html, html_images, sent, sending_status, deleted, attachment, unsubscribe_button
							 FROM ".$aConfig['tabls']['prefix']."newsletters
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
			if ($aData['deleted'] == '1' || $aData['sending_status'] != '0') {
				// zablokowanie mozliwosci edycji
				$aReadOnlyAttr['readonly'] = "readonly";
				$aDisabledAttr['disabled'] = "disabled";
			}
		}
		if (!empty($aData) && ($aData['deleted'] == '1' || $aData['sending_status'] != '0')) {
			// tylko podglad - bez mozliwosci edycji
			$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_2'], $aData['subject'], getPagePath($iPId, $this->iLangId, true));
			
			$pForm = new FormTable('newsletter', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>115), $aConfig['common']['js_validation']);
			$pForm->AddHidden('do', 'save_copy');
			$pForm->AddHidden('category_id', $aData['category_id']);
			$pForm->AddHidden('id', $iId);
			// tytul
			$pForm->AddRow('&nbsp;', getNewsletter($iId));
			// pole TEXT
			$pForm->AddText('subject', $aConfig['lang'][$this->sModule]['subject'], $aData['subject'], array('maxlength'=>255, 'style'=>'width: 450px;', 'readonly' => 'readonly'), '', 'text', false);
						
			// tresc TXT
			$pForm->AddTextarea('content_txt', $aConfig['lang'][$this->sModule]['body_txt'], $aData['content_txt'], array('readonly' => 'readonly', 'rows'=>10, 'style'=>'width: 100%;'), '', '', 0, false);
			
			// tresc HTML
			$pForm->AddTextarea('content_html', $aConfig['lang'][$this->sModule]['body_html'], $aData['content_html'], array('readonly' => 'readonly', 'rows'=>20, 'style'=>'width: 100%;'), '', '', 0, false);
			
			// przyciski
			$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['save_as']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		}
		else {
			if (!empty($_POST)) {
				$aData =& $_POST;
			}
	
			if ($iId > 0) {
				$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_1'], $aData['subject'], getPagePath($iPId, $this->iLangId, true));
			}
			else {
				$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_0'], getPagePath($iPId, $this->iLangId, true));
			}
	
			$pForm = new FormTable('newsletter', $sHeader, array('action'=>phpSelf(array('id'=>$iId)),'enctype'=>'multipart/form-data'), array('col_width'=>115), $aConfig['common']['js_validation']);
			$pForm->AddHidden('do', $iId > 0 ? 'update' : 'insert');
	
			// kategorie newslettera
			$aCategories =& $this->getCategories($iPId);
			if (count($aCategories) > 2) {
				$pForm->AddSelect('category_id', $aConfig['lang'][$this->sModule]['category_id'], array_merge(array(), $aDisabledAttr), $aCategories, $aData['category_id']);
			}
			else {
				$pForm->AddHidden('category_id', $aCategories[1]['value']);
			}
	
			// tytul
			$pForm->AddText('subject', $aConfig['lang'][$this->sModule]['subject'], $aData['subject'], array_merge(array('maxlength'=>255, 'style'=>'width: 350px;'), $aReadOnlyAttr));
	
			// tresc TXT
			$pForm->AddTextArea('content_txt', $aConfig['lang'][$this->sModule]['body_txt'], $aData['content_txt'], array_merge(array('rows'=>20, 'style'=>'width: 100%;'), $aReadOnlyAttr), 0, '', '', false);
			
			// tresc HTML
			$pForm->AddWYSIWYG('content_html', $aConfig['lang'][$this->sModule]['body_html'], $aData['content_html'], array('strip_urls' =>'0'), '', false);
	
			// dolaczaj obrazki do maila
			$pForm->AddCheckBox('html_images',$aConfig['lang'][$this->sModule]['html_images'], array(), '', $aData['html_images'] == '1', false, false);
		
			// dolaczaj obrazki do maila
			$pForm->AddCheckBox('unsubscribe_button',$aConfig['lang'][$this->sModule]['unsubscribe_button'], array(), '', $aData['unsubscribe_button'] == '1', false, false);
		
			// przyciski
			$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? '1' : '0')]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));
		}
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function


	/**
	 * Metoda wprowadza informacje o wyslaniu newslettera
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function SetSentInfo(&$pSmarty, $iId) {
		global $aConfig;

		// aktualizacja newslettera
		$aValues = array(
			'sending_status' => '3',
			'sent' => 'NOW()',
			'sent_by ' => $_SESSION['user']['name']
		);
		Common::Update($aConfig['tabls']['prefix']."newsletters",
									 $aValues,
									 "id = ".$iId);
	} // end of SetSentInfo() funciton


	/**
	 * Metoda wysyla newsletter do wszystkich jego odbiorcow
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony newslettera
	 * @param	integer	$iId	- Id newslettera do wyslania
	 * @return	void
	 */
	function SendNewsletter(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		// pobranie Id kategorii newslettera
		$iCId = $this->getCategoryId($iPId, $iId);

		// pbranie wszystkich odbiorcow newslettera
		$oRes =& $this->getRecipients($iPId, $iCId);
		if ($oRes !== false) {
			// wyslanie newslettera
			if ($this->Send($pSmarty, $iPId, $iId, $oRes)) {
				// aktualizacja w newsletterze iformacji o wyslaniu
				$this->SetSentInfo($pSmarty, $iId);
			}
		}
		else {
			$this->sMsg = GetMessage(sprintf($aConfig['lang'][$this->sModule]['no_recipients_err'], $this->getCategoryName($iPId, $iCId)));
		}
		$this->Show($pSmarty, $iPId);
	} // end of SendNewsletter() function


	/**
	 * Metoda wysyla testowy newsletter do osob zdefiniowanych w konfiguracji strony
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony newslettera
	 * @param	integer	$iId	- Id newslettera do wyslania
	 * @return	void
	 */
	function SendPreview(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		// rozdzielenie testowych odbiorcow newslettera
		$aTmp = explode(';', $this->aSettings['preview_emails']);
		foreach($aTmp as $iKey => $sRecipient) {
			$aRecipients[$iKey]['id'] = 0;
			$aRecipients[$iKey]['email'] = $sRecipient;
		}
		if (!empty($aRecipients)) {
			// wyslanie newslettera
			$this->Send($pSmarty, $iPId, $iId, $aRecipients);
		}
		$this->Show($pSmarty, $iPId);
	} // end of SendPreview() function


	/**
	 * Metoda wysyla newsletter
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony newslettera
	 * @param	integer	$iId	- Id newslettera do wyslania
	 * @param	mixed	$aRecipients	- lista odbiorcow newslettera (tablica lub wynik zapytania SQL)
	 * @return	void
	 */
	function Send(&$pSmarty, $iPId, $iId, &$mRecipients) {
		global $aConfig;
		set_time_limit(600);
		$iI = 0;
		
		include('internals/functions.inc.php');
		// link do strony newslettera
		$aPagesLinks =& getModulePagesLinks($_GET['module_id'], $this->iLangId);
		$sPageLink = $aPagesLinks[$iPId]['label'];

		// pobranie tematu, numeru, tresci newslettera oraz nadawcy
		$aNewsletter =& $this->getNewsletterData($iId);
		if (!empty($aNewsletter) && ((is_array($mRecipients) && !empty($mRecipients)) || is_object($mRecipients))) {
			// dodanie do wszystkich linkow zrodla przejscia do sklepu - link sledzacy
			// wraz z Id newslettera
			extractLinks($iId, $aNewsletter['content_txt'], $aNewsletter['content_html']);
			if ($aNewsletter['html_images'] == '1') {
				// ekstrakcja obrazkow HTML
				$aHTMLImages = extractHTMLImages($aNewsletter['content_html']);
			}
			// dodanie kontenera HTMLowe
			addHTMLContainer($aNewsletter['content_html']);
			// wysylanie Newslettera - odbiorcy z tablicy
			if (is_array($mRecipients)) {
				foreach ($mRecipients as $aRecipient) {
					// link potwierdzajacy
					$sEditLink = $aConfig['common']['client_base_url_http'].
											 $sPageLink.
											 '/edit,'.md5($aRecipient['id'].':'.$aRecipient['email']).'.html';
					// link usuwający
					$sDeleteLink = $aConfig['common']['client_base_url_http'].
												 $sPageLink.
												 '/delete,'.md5($aRecipient['id'].':'.$aRecipient['email']).'.html';
					// dodanie adresu email do linek sledzacych
					addEmailToLinks($aRecipient['email'], $aNewsletter['content_txt'], $aNewsletter['content_html'], $sTxt, $sHtml);
					// zamiana linkow delete i edit w tresci TXT
					$sTxt = str_replace('{delete_link}', $sDeleteLink, str_replace('{edit_link}', $sEditLink, $sTxt));
					// zamiana linkow delete i edit w tresci HTML
					$sHtml = str_replace('{delete_link}', $sDeleteLink, str_replace('{edit_link}', $sEditLink, $sHtml));
					if (Common::sendTxtHtmlMail($aNewsletter['sender'], $aNewsletter['sender_email'], $aRecipient['email'], changeCharcodes($aNewsletter['subject'], 'utf8', 'no'), $sTxt, $sHtml, '', '', $aHTMLImages)) {
						$iI++;
					}
				}
			}
			// wysylanie Newslettera - odbiorcy z zapytania
			if (is_object($mRecipients)) {
				while ($aRecipient =& $mRecipients->fetchRow(DB_FETCHMODE_ASSOC)) {
					// link potwierdzajacy
					$sEditLink = $aConfig['common']['client_base_url_http'].
											 $sPageLink.
											 '/edit,'.md5($aRecipient['id'].':'.$aRecipient['email']).'.html';
					// link usuwający
					$sDeleteLink = $aConfig['common']['client_base_url_http'].
												 $sPageLink.
												 '/delete,'.md5($aRecipient['id'].':'.$aRecipient['email']).'.html';
					// dodanie adresu email do linek sledzacych
					addEmailToLinks($aRecipient['email'], $aNewsletter['content_txt'], $aNewsletter['content_html'], $sTxt, $sHtml);
					// zamiana linkow delete i edit w tresci TXT
					$sTxt = str_replace('{delete_link}', $sDeleteLink, str_replace('{edit_link}', $sEditLink, $sTxt));
					// zamiana linkow delete i edit w tresci HTML
					$sHtml = str_replace('{delete_link}', $sDeleteLink, str_replace('{edit_link}', $sEditLink, $sHtml));
					if (Common::sendTxtHtmlMail($aNewsletter['sender'], $aNewsletter['sender_email'], $aRecipient['email'], changeCharcodes($aNewsletter['subject'], 'utf8', 'no'), $sTxt, $sHtml, '', '', $aHTMLImages)) {
						$iI++;
					}
				}
			}
			
			$this->sMsg = GetMessage(sprintf($aConfig['lang'][$this->sModule]['sent_to'], $aNewsletter['subject'], getPagePath($iPId, $this->iLangId, true), $iI), false);
			return true;
		}
		else {
			// albo wystapil blad podczas pobierania danych newslettera albo nie zawiera
			// on zadnych danych
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['send_err']);
			return false;
		}
	} // end of Send() function


	/**
	 * Metoda tworzy formularz ustawien rozsylki newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- ID strony
	 * @param	integer	$iId	- Id wysylanego newslettera
	 */
	function PrepareToSend(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$aData = array();

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
;
		// okreslenie tytulu newslettera
		$sSql = "SELECT subject, DATE_FORMAT(start_sending, '".$aConfig['common']['cal_sql_date_format']."') AS start_sending_date, DATE_FORMAT(start_sending, '%H:%i') AS start_sending_hour, sending_status
						 FROM ".$aConfig['tabls']['prefix']."newsletters
						 WHERE page_id = ".$iPId." AND
						 			 id = ".$iId;
		$aNewsletter =& Common::GetRow($sSql);

		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		else {
			if (!empty($aNewsletter['start_sending_date'])) {
				$aData['start_sending_date'] = $aNewsletter['start_sending_date'];
			}
			if (!empty($aNewsletter['start_sending_hour'])) {
				$aData['start_sending_hour'] = $aNewsletter['start_sending_hour'];
			}
			else {
				$aData['start_sending_hour'] = date('H:i');
			}
		}

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['choose_category_header'], $aNewsletter['subject'], getPagePath($iPId, $this->iLangId, true));

		$pForm = new FormTable('newsletter', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_send');

		// data rozpoczecia wysylki
		$pForm->AddText('start_sending_date', $aConfig['lang'][$this->sModule]['start_sending_date'], $aData['start_sending_date'], array('style'=>'width: 75px;'), '', 'date');

		// godzina rozpoczecia wysylki
		$pForm->AddSelect('start_sending_hour', $aConfig['lang'][$this->sModule]['start_sending_hour'], array(), $this->getHours(), $aData['start_sending_hour']);

		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of PrepareToSend() function


	/**
	 * Meetoda pobiera i zwraca tablice subskrybentow newslettera
	 * dla podanej kategorii
	 *
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iCId	- Id kategorii
	 * @return	object	- wynik zapytania
	 */
	function &getRecipients($iPId, $iCId) {
		global $aConfig;

		$sSql = "SELECT DISTINCT A.id, A.email
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients AS A
						 JOIN ".$aConfig['tabls']['prefix']."newsletters_rec2cat AS B
						 			ON A.id = B.recipient_id
						 WHERE B.category_id = ".$iCId." AND
						 			 A.page_id = ".$iPId." AND
						 			 A.confirmed = '1'";
		return Common::PlainQuery($sSql);
	} // end of getRecipients() method


	function getCategories($iPId) {
		global $aConfig;

		// pobranie kategorii
		$sSql = "SELECT B.name, A.id AS value, A.name AS label
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories A
						 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories_groups B
						 ON B.id=A.group_id
						 WHERE A.page_id = ".$iPId."
						 ORDER BY A.name";
		$aItems = Common::GetAssoc($sSql,false,array(),DB_FETCHMODE_ASSOC,true);
		$aCats = array();
		foreach($aItems as $sGroup=>$aList) {
			$aCats[]=array('label'=>$sGroup,'value'=>'OPTGROUP');
			$aCats=array_merge($aCats,$aList);
			$aCats[]=array('value'=>'/OPTGROUP');
		}
		return array_merge(array(array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])), $aCats);
	} // end of getCategories() method


	/**
	 * Metoda pobiera Id kategorii newslettera
	 *
	 * @param	integer	$iPId	- Id strony newslettera
	 * @param	integer	$iId	- Id newslettera
	 * @return	integer	- Id kategorii newslettera
	 */
	function getCategoryId($iPId, $iId) {
		global $aConfig;

		// pobranie Id kategorii
		$sSql = "SELECT category_id
						 FROM ".$aConfig['tabls']['prefix']."newsletters
						 WHERE id = ".$iId." AND
						 			 page_id = ".$iPId;
		return (int) Common::GetOne($sSql);
	} // end of getCategoryId() method


	/**
	 * Metoda pobiera nazwe kategorii newslettera
	 *
	 * @param	integer	$iPId	- Id strony newslettera
	 * @param	integer	$iId	- Id newslettera
	 * @return	string	- nazwa kategorii newslettera
	 */
	function getCategoryName($iPId, $iId) {
		global $aConfig;

		// pobranie nazwy kategorii
		$sSql = "SELECT name
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
						 WHERE id = ".$iId." AND
						 			 page_id = ".$iPId;
		return Common::GetOne($sSql);
	} // end of getCategoryName() method


	/**
	 * Metoda pobiera temat newslettera
	 *
	 * @param	integer	$iPId	- Id strony newslettera
	 * @param	integer	$iId	- Id newslettera
	 * @return	string	- tytul newslettera
	 */
	function getNewsletterSubject($iPId, $iId) {
		global $aConfig;

		// pobranie nazwy kategorii
		$sSql = "SELECT subject
						 FROM ".$aConfig['tabls']['prefix']."newsletters
						 WHERE id = ".$iId." AND
						 			 page_id = ".$iPId;
		return Common::GetOne($sSql);
	} // end of getNewsletterSubject() method


	/**
	 * Metoda tworzy tablice z godzinami dla pola SELECT
	 *
	 * @return	array ref	- lista godzin
	 */
	function &getHours() {
		global $aConfig;
		$aItems = array(array('value' => '0',
													'label' => $aConfig['lang']['common']['choose']));
		for ($i = 0; $i < 24; $i++) {
			$aItems[] = array('value' => ($i <= 9 ? '0' : '').$i.':00',
												'label' => ($i <= 9 ? '0' : '').$i.':00');
			$aItems[] = array('value' => ($i <= 9 ? '0' : '').$i.':30',
												'label' => ($i <= 9 ? '0' : '').$i.':30');
		}
		return $aItems;
	} // end of etHours()


	/**
	 * Metoda w zaleznosci od wybranej akcji sprawdza czy newsletter
	 * moze zostac wyslany - czy nie zostal wyslany juz wczesniej
	 *
	 * @param	string	$sDo	- akcja
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id newsletter
	 * @return	bool	- true: moze; false: nie moze (juz wyslany)
	 */
	function canBeSent($sDo, $iPId, $iId) {
		global $aConfig;

		switch ($sDo) {
			case 'send_later':
			case 'send':
			case 'update_send':
			case 'send_preview':
				return !$this->isSent($iPId, $iId);
			break;
		}
		return true;
	} // end of canBeSent() method


	/**
	 * Metoda sprawdza czy newsletter zostal juz wyslany
	 *
	 * @param	integer	$iPId	- Id strony newslettera
	 * @param	integer	$iId	- Id newslettera
	 * @return	array ref - informacje o stanie wyslania
	 */
	function &isSent($iPId, $iId) {
		global $aConfig;

		$sSql = "SELECT sending_status
						 FROM ".$aConfig['tabls']['prefix']."newsletters
						 WHERE id = ".$iId." AND
						 			 page_id = ".$iPId;
		return (int) Common::GetOne($sSql) != 0;
	} // end of isSent() method
	
	
	/**
	 * Metoda sprawdza czy newsletter posiada kategorie
	 *
	 * @param	integer	$iPId	- Id strony newslettera
	 * @return	bool
	 */
	function hasCategories($iPId) {
		global $aConfig;

		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
						 WHERE page_id = ".$iPId;
		return intval(Common::GetOne($sSql)) > 0;
	} // end of hasCategories() method
	
	
	/**
	 * Metoda pobiera dane newslettera
	 *
	 * @param	integer	$iId	- Id newslettera
	 * @return	bool
	 */
	function &getNewsletterData($iId) {
		global $aConfig;

		$sSql = "SELECT A.id, A.subject, A.content_txt, A.content_html, A.attachment, A.html_images, B.sender, B.sender_email
						 FROM ".$aConfig['tabls']['prefix']."newsletters A
						 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories B
						 ON B.id = A.category_id
						 WHERE A.id = ".$iId;
		return Common::GetRow($sSql);
	} // end of hasCategories() method
	

	/**
	 * Metoda pobiera liste mailingow newslettera do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id mailingow
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, subject
				 		 FROM ".$aConfig['tabls']['prefix']."newsletters
					 	 WHERE id IN (".implode(',', $_POST['delete']).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa mailingi newslettera - oznacza jako usuniete
	 * 
	 * @param	integer	$iId	- Id newslettera do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."newsletters SET deleted = '1' WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method
	
	
	/**
	 * Metooda pobiera nazwe uzytkownika ktory utworzyl newsletter o podanym Id
	 * 
	 * @param	integer	$iId	 - Id newslettera
	 * @return	string
	 */
	function getCreatorName($iId) {
		global $aConfig;
		
		$sSql = "SELECT created_by
						 FROM ".$aConfig['tabls']['prefix']."newsletters
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getCreatorName() method


	

	/**
	 * Metoda przeosi przeslany plik do katalgu docelowego
	 * oraz dodaje informacje do bazy danych
	 * 
	 * @param	integer	$iId	- Id newslettera
	 * @param	array ref	$aFile	- dane pliku
	 * @param	object ref	$oFile	- obiekt klasy MyFile
	 * @return	integer	-5: nieprawidlowe rozszerzeie pliku 
	 * 									-4: podana sciezka nie wskazuje na katalog
   *                  -3: katalog nie jest zapisywalny
   *                  -2: plik o podanej nazwie juz istnieje
   *                  -1: proba przeniesienia do katalogu docelowego nie powiodla sie
   *                   0: blad
   * 									 1: plik zostal przeniesiony do katalogu docelowego
	 */
	function addFile($iId, $iType, &$aFile, &$oFile, $bOverwrite=false) {
		global $aConfig;
		
		$oFile->SetFile($aConfig['common']['client_base_path'].$aConfig['common']['product_file_dir'], $aFile);
		$iResult = $oFile->proceedFile($aConfig['common']['product_file_extensions'], false, $bOverwrite);
		return $iResult;
	} // end of addFile() method	
	
} // end of Module Class
?>