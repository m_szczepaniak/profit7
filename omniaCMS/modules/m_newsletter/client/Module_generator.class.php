<?php
/**
 * Klasa odule do obslugi Newslettera
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie klasy newslettera
include_once('Newsletter.class.php');

class Module {

  // Id strony
  var $iPageId;

  // szablon strony
  var $sTemplate;

  // symbol modulu
  var $sModule;

  // ustawienia strony modulu
  var $aSettings;

  // link do strony newslettera
 	var $sPageLink; 

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;
		
		$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->sTemplate = 'modules/'.$this->sModule.'/'.$this->aSettings['template'];
		$this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
	} // end Module() function
	
	
	/**
	 * Metoda pobiera tresc strony o id $this->iPageId
	 * dowiazuje ja do Smartow oraz zwraca xHTML z trescia strony po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';

		return $sHtml;
	} // end of getContent()
} // end of Module Class
?>