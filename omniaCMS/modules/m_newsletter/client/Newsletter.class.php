<?php
/**
* Klasa do obslugi Newslettera
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*/


class Newsletter {

  // Id strony
  var $iPageId;

  // symbol modulu
  var $sModule;

  // ustawienia strony modulu
  var $aSettings;

  // katalog z szblonami
  var $sTemplatesDir;

  // szablon
  var $sTemplate;

  // dane subskrybenta
  var $aSubscriber;

  // link do strony newslettera
  var $sPageLink;

	/**
	* Konstruktor klasy ClientCommon
	*
	*/
	function Newsletter($sSymbol, $iPageId, $aSettings, $sPageLink, $sSubscriber='') {
		$this->sModule = $sSymbol;
		$this->iPageId = $iPageId > 0 ? $iPageId : $this->getPageId();
		$this->aSettings = $aSettings;
		$this->sTemplate = 'modules/'.$sSymbol.'/'.$this->aSettings['template'];
		$this->aSubscriber = null;
		$this->sPageLink = $sPageLink;
		if ($sSubscriber != '') {
			$this->setSubscriberData($sSubscriber);
		}
	} // end Newsletter()


	/**
	 * Metoda pobiera dane subskrybenta na podstawie przekazanego
	 * skrotu md5 z ID i adresu email subskrybenta - md5(ID:email)
	 *
	 * @param	string	$sSubscriber	- md5 subskrybenta
	 * @return void
	 */
	function setSubscriberData($sSubscriber) {
		global $aConfig;

		// pobranie danych subskrybenta
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE MD5(CONCAT(id, ':', email)) = ".Common::Quote($sSubscriber)." AND
						 			 page_id = ".$this->iPageId;
		$aSubscriber =& Common::GetRow($sSql);
		if (!empty($aSubscriber)) {
			// pobranie kategorii subskrybenta
			$sSql = "SELECT category_id, 1
						 	 FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat
						 	 WHERE recipient_id = ".$aSubscriber['id'];
			$aSubscriber['categories'] =& Common::GetAssoc($sSql);
			$this->aSubscriber =& $aSubscriber;
		}
	} // end of setSubscriberData() method
	
	/**
	 * Metoda pobiera dane subskrybenta na podstawie przekazanego emaila
	 *
	 * @param	string	$sEmail	- email subskrybenta
	 * @return void
	 */
	function setSubscriberDataByEmail($sEmail) {
		global $aConfig;

		// pobranie danych subskrybenta
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE email = ".Common::Quote($sEmail)." AND
						 			 page_id = ".$this->iPageId;
		$aSubscriber =& Common::GetRow($sSql);
		if (!empty($aSubscriber)) {
			// pobranie kategorii subskrybenta
			$sSql = "SELECT category_id, 1
						 	 FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat
						 	 WHERE recipient_id = ".$aSubscriber['id'];
			$aSubscriber['categories'] =& Common::GetAssoc($sSql);
			$this->aSubscriber =& $aSubscriber;
		}
	} // end of setSubscriberData() method


	/**
	 * Metoda dodaje subskrybenta do bazy danych
	 *
	 * @param	string	$sEmail	- dodawany adres email
	 * @param	array	$aCategories	- wybrane kategorie newslettera
	 * @param	bool	$bConfirmed	- true: od razu potwierdzony; false: nie potwierdzony
	 * @param	bool	$bUnregistred	- true: niezarejestrowany; false: zarejestrowany
	 * @return	integer	-1: wystapil blad
	 * 									 0: adres zostal dodany
	 * 									 1: istnieje ale niepotwierdzony
	 * 									 2: istnieje
	 */
	function add($sEmail, $aCategories, $bConfirmed=false, $bUnregistred=false) {

		$iResult = $this->recipientExists($sEmail);
		if ($iResult == 0) {
			// adres nie istnieje - dodanie
			if (!is_numeric($iRId = $this->addRecipient($sEmail, $aCategories, $bConfirmed, $bUnregistred))) {
				// wystapil blad
				return -1;
			}
			// adres zostal dodany
			if (!$bConfirmed) {
				$this->sendConfirmationEmail($iRId, $sEmail, $aCategories);
			}
			return 0;
		}
		elseif ($bConfirmed && $iResult == 1) {
			// adres istnieje ale jest niepotwierdzony - potwierdzenie
			$this->aSubscriber['id'] = $this->getSubscriberId($sEmail);
			$this->aSubscriber['email'] = $sEmail;
			$this->aSubscriber['confirmed'] = '0';
			if($bUnregistred == false){
				$this->removeUnregistredCategory($sEmail);
			}
			return $this->confirm();
		}
		if($bUnregistred == false){
			$this->removeUnregistredCategory($sEmail);
		}
		// zwrocenie wartosci $iResult
		return $iResult;
	} // end of add() method


	/**
	 * Metoda aktywuje subskrybenta
	 *
	 * @return	integer	-1: wystapil blad
	 * 									 0: adres zostal potwierdzony
	 * 									 1: istnieje ale juz potwierdzony
	 * 									 2: nie istnieje
	 */
	function confirm() {
		global $aConfig;

		if (empty($this->aSubscriber)) {
			return 2;
		}
		if ($this->aSubscriber['confirmed'] == '1') {
			return 1;
		}

		// potwierdzanie
		$aValues = array(
			'confirmed' => '1'
		);
		if (Common::Update($aConfig['tabls']['prefix']."newsletters_recipients",
											 $aValues,
											 "id = ".$this->aSubscriber['id'].
											 " AND page_id = ".$this->iPageId.
											 " AND email = ".Common::Quote($this->aSubscriber['email'])) !== false) {
			return 0;
		}
		return -1;
	} // end of confirm() method

	function removeUnregistredCategory($sEmail){
		global $aConfig;
		$sSql="DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat 
						WHERE recipient_id = (SELECT id 
																	FROM ".$aConfig['tabls']['prefix']."newsletters_recipients 
																	WHERE email = ".Common::Quote($sEmail)." 
																	LIMIT 1)
							AND category_id = (SELECT id 
																	FROM ".$aConfig['tabls']['prefix']."newsletters_categories 
																	WHERE unregistred_cat = '1' 
																	LIMIT 1)";
		return (Common::Query($sSql)!==false);
	}

	/**
	 * Metoda wprowadza zmiany w ustawieniach subskrybenta
	 *
	 * @param	string	$sEmail	- dodawany adres email
	 * @param	array	$aCategories	- wybrane kategorie newslettera
	 * @return	bool	- true: wprowadzono; false - nie udalo sie wprowadzic zmian
	 */
	function add_tmp($sEmail, $aCategories) {
		global $aConfig;

		if ($aCategories != $this->aSubscriber['categories']) {
			// zmienily sie kategorie - aktualizacja
			// usuniecie starych kategorii
			Common::BeginTransaction();
			$iRId=$this->getSubscriberId($sEmail);
			
			$sSql = "DELETE
					 FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat_tmp
					 WHERE recipient_id = ".$iRId;
			if (Common::Query($sSql) === false) {
				Common::RollbackTransaction();
				return false;
			}

			// dodawanie kategorii newslettera
			foreach ($aCategories as $iCId => $mVal) {
				$aValues = array(
					'category_id' => $iCId,
					'recipient_id' => $iRId
				);
				if (!Common::Insert($aConfig['tabls']['prefix']."newsletters_rec2cat_tmp",
														$aValues,
														'',
														false)) {
					// wystapil blad, wycofanie transakcji
					Common::RollbackTransaction();
					return false;
				}
			}
			
			$sCCode=md5($iRId.':'.$sEmail);
				
				$this->sendEditConfirmationEmail($iRId, $sEmail, $aCategories, $sCCode);
			
			Common::CommitTransaction();
		}
		return true;
	} // end of add_tmp() method


	/**
	 * Metoda wprowadza zmiany w ustawieniach subskrybenta
	 *
	 * @param	string	$sEmail	- dodawany adres email
	 * @param	array	$aCategories	- wybrane kategorie newslettera
	 * @return	bool	- true: wprowadzono; false - nie udalo sie wprowadzic zmian
	 */
	function edit($sEmail, $aCategories) {
		global $aConfig;

		if ($aCategories != $this->aSubscriber['categories']) {
			// zmienily sie kategorie - aktualizacja
			// usuniecie starych kategorii
			Common::BeginTransaction();

			$sSql = "DELETE
							 FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat
							 WHERE recipient_id = ".$this->aSubscriber['id'];
			if (Common::Query($sSql) === false) {
				Common::RollbackTransaction();
				return false;
			}

			// dodawanie kategorii newslettera
			foreach ($aCategories as $iCId => $mVal) {
				$aValues = array(
					'category_id' => $iCId,
					'recipient_id' => $this->aSubscriber['id']
				);
				if (!Common::Insert($aConfig['tabls']['prefix']."newsletters_rec2cat",
														$aValues,
														'',
														false)) {
					// wystapil blad, wycofanie transakcji
					Common::RollbackTransaction();
					return false;
				}
			}
			Common::CommitTransaction();
		}
		return true;
	} // end of edit() method
	
		/**
	 * Metoda wprowadza zmiany w ustawieniach subskrybenta
	 *
	 * @param	string	$sEmail	- dodawany adres email
	 * @param	array	$aCategories	- wybrane kategorie newslettera
	 * @return	bool	- true: wprowadzono; false - nie udalo sie wprowadzic zmian
	 */
	function edit_confirm() {
		global $aConfig;
		if (empty($this->aSubscriber)) {
			return false;
		}
//dump($this->aSubscriber);
		// zmienily sie kategorie - aktualizacja
		// usuniecie starych kategorii
		Common::BeginTransaction();

		$sSql = "DELETE
				 FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat
				 WHERE recipient_id = ".$this->aSubscriber['id'];
		if (Common::Query($sSql) === false) {
			Common::RollbackTransaction();
			return false;
		}
		$sSql = "SELECT category_id 
				 FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat_tmp
				 WHERE recipient_id = ".$this->aSubscriber['id'];
		$aCategories=Common::GetCol($sSql);
		if(!empty($aCategories)){
			// dodawanie kategorii newslettera
			foreach ($aCategories as $iCId) {
				$aValues = array(
					'category_id' => $iCId,
					'recipient_id' => $this->aSubscriber['id']
				);
				if (!Common::Insert($aConfig['tabls']['prefix']."newsletters_rec2cat",
														$aValues,
														'',
														false)) {
					// wystapil blad, wycofanie transakcji
					Common::RollbackTransaction();
					return false;
				}
			}
			
			$sSql = "DELETE
					 FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat_tmp
					 WHERE recipient_id = ".$this->aSubscriber['id'];
			if (Common::Query($sSql) === false) {
				Common::RollbackTransaction();
				return false;
			}
		}
		Common::CommitTransaction();
		return true;
	} // end of edit() method
	
	/**
	 * Metoda usuwa subskrybenta
	 *
	 * @return	bool	- true: usunieto; false - nie udalo sie usunac
	 */
	function delete() {
		global $aConfig;

		$sSql = "DELETE
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE id = ".$this->aSubscriber['id']." AND
						 			 page_id = ".$this->iPageId." AND
						 			 email = ".Common::Quote($this->aSubscriber['email']);
		if (Common::Query($sSql) === false) {
			return false;
		}
		return true;
	} // end of delete() method
  
  
  /**
   * Metoda usuwa odbiorce newslettera ze wszystkich kategorii
   * 
   * @global array $aConfig
   * @return boolean
   */
  public function deleteAllCategories() {
    global $aConfig;

		$sSql = "DELETE
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE id = ".$this->aSubscriber['id']." AND
						 			 email = ".Common::Quote($this->aSubscriber['email']);
		if (Common::Query($sSql) === false) {
			return false;
		}
		return true;
  }// end of deleteAllCategories() method


	/**
	 * Metoda sprawdza czy podany adres email juz istnieje
	 * w bazie subskrybentow newslettera
	 *
	 * @param	string	$sEmail	- sprawdzany adres e-mail
	 * @return	integer	-1: wystapil blad
	 * 									 0: nie istnieje
	 * 									 1: istnieje ale niepotwierdzony
	 * 									 2: istnieje
	 */
	function recipientExists($sEmail) {
		global $aConfig;

		$sSql = "SELECT confirmed
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE page_id = ".$this->iPageId." AND
						 			 email = ".Common::Quote($sEmail);
		if (($iConfirmed =& Common::GetOne($sSql)) !== false) {
			if (!is_numeric($iConfirmed)) {
				// adres nie istnieje
				return 0;
			}
			return (int) $iConfirmed + 1;
		}
		return -1;
	} // end of recipientExists() method


	/**
	 * Metoda dodaje do bazy danych subskrybenta newslettera
	 *
	 * @param	string	$sEmail	- dodawany adres email
	 * @param	array	$aCategories	- wybrane kategorie newslettera
	 * @param	bool	$bConfirmed	- true: od razu potwierdzony; false: nie potwierdzony
	 * @param	bool	$bUnregistred	- true: niezarejestrowany; false: zarejestrowany
	 * @return	mixed	- integer > 0: dodano; false - nie udalo sie dodac
	 */
	function addRecipient($sEmail, $aCategories, $bConfirmed=false, $bUnregistred=false) {
		global $aConfig;

		$aValues = array(
			'page_id' => $this->iPageId,
			'email' => $sEmail,
			'added'	=> 'NOW()'
		);
		if ($bConfirmed) {
			$aValues['confirmed'] = '1';
		}

		Common::BeginTransaction();
		// dodatkowa kategoria niezarejestrowana
		$iUnregistredCId = $this->getUnregistredCategoryId();
		if($bUnregistred && $iUnregistredCId>0){
			$aCategories[$iUnregistredCId] = '1';
		}

		if (is_numeric($iRId = Common::Insert($aConfig['tabls']['prefix']."newsletters_recipients", $aValues))) {
			// dodawanie kategorii newslettera
			foreach ($aCategories as $iCId => $mVal) {
				$aValues = array(
					'category_id' => $iCId,
					'recipient_id' => $iRId
				);
				if (!Common::Insert($aConfig['tabls']['prefix']."newsletters_rec2cat",
														$aValues,
														'',
														false)) {
					// wystapil blad, wycofanie transakcji
					Common::RollbackTransaction();
					return false;
				}
			}
			Common::CommitTransaction();
			return $iRId;
		}
		Common::RollbackTransaction();
		return false;
	} // end of addRecipient() method


	/**
	 * Metoda wysyla newsletter
	 *
	 * @param	integer	$iRId	- Id subskrybenta
	 * @param	string	$sEmail	- adres email subskrybenta
	 * @param	array	$aCategories	- kategorie newslettera
	 * @return	void
	 */
	function sendConfirmationEmail($iRId, $sEmail, &$aCategories) {
		global $aConfig;
		$sCategories = '';
		
		//dump($this->aSettings);
		$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		// link potwierdzajacy
		$sConfirmationLink = $aConfig['common']['base_url_http'].
													 substr($this->sPageLink, 1).
													 '/confirm,'.md5($iRId.':'.$sEmail).'.html';
		// link usuwający
		$sDeleteLink = $aConfig['common']['base_url_http'].
													 substr($this->sPageLink, 1).
													 '/delete,'.md5($iRId.':'.$sEmail).'.html';

		// wysylanie potwierdzenia
		$aGetParams = array('head_charset'=>$aConfig['default']['charset'],
												'text_charset'=>$aConfig['default']['charset']);
		$aHeaders['From'] = '"'.$this->aSettings['newsletter_name'].'"<'.$this->aSettings['main_email'].'>';
    $aHeaders['Reply-To'] = 'kontakt@profit24.pl'; // @ticket Marcin Chudy 02.10.2013  - $aHeaders['Reply-To'] = $this->aSettings['main_email'];
		$aHeaders['Subject'] = $this->aSettings['conf_email_subject'];
		//if (count($aCategories) > 1) {
			// wersja z wiecej niz jedna kategoria
			$sCategories = $this->getCategoriesNames($aCategories);
			$aBodyTXT['body'] = sprintf($aLang['confirmation_body_categories'],
																	$sEmail,
																	$this->aSettings['newsletter_name'],
																	$sCategories,
																	$sConfirmationLink,
																	$this->aSettings['days_to_confirm'],
																	//$sDeleteLink,
	  												 			$aConfig['_settings']['site']['mail_footer']);
		//}
		//else {
			// wersja z tylko jedna kateoria
		//	$aBodyTXT['body'] = sprintf($aLang['confirmation_body'],
		//															$sEmail,
		//															$this->aSettings['newsletter_name'],
		//															$sConfirmationLink,
		//															$this->aSettings['days_to_confirm'],
		//															//$sDeleteLink,
	  //												 			$aConfig['_settings']['site']['mail_footer']);
		//}

		return Common::SendMimeEmail($sEmail,
																 $aHeaders,
																 $aBodyTXT,
																 array(),
																 $aGetParams);
	} // end of sendConfirmationEmail() function

	
		/**
	 * Metoda wysyla newsletter
	 *
	 * @param	integer	$iRId	- Id subskrybenta
	 * @param	string	$sEmail	- adres email subskrybenta
	 * @param	array	$aCategories	- kategorie newslettera
	 * @return	void
	 */
	function sendEditConfirmationEmail($iRId, $sEmail, &$aCategories,$ConfirmCode) {
		global $aConfig;
		$sCategories = '';
		
		//dump($this->aSettings);
		$aLang =& $aConfig['lang']['mod_'.$this->sModule];

		// link potwierdzajacy
		$sConfirmationLink = $aConfig['common']['base_url_http'].
													 substr($this->sPageLink, 1).
													 '/edit_confirm,'.$ConfirmCode.'.html';


		// wysylanie potwierdzenia
		$aGetParams = array('head_charset'=>$aConfig['default']['charset'],
												'text_charset'=>$aConfig['default']['charset']);
		$aHeaders['From'] = '"'.$this->aSettings['newsletter_name'].'"<'.$this->aSettings['main_email'].'>';
		$aHeaders['Reply-To'] = $this->aSettings['main_email'];
		$aHeaders['Subject'] = $aLang['edit_confirmation_subject'];
		//if (count($aCategories) > 1) {
			// wersja z wiecej niz jedna kategoria
			$sCategories = $this->getCategoriesNames($aCategories);
			$aBodyTXT['body'] = sprintf($aLang['edit_confirmation_body_categories'],
																	$sEmail,
																	$this->aSettings['newsletter_name'],
																	$sCategories,
																	$sConfirmationLink,
	  												 			$aConfig['_settings']['site']['mail_footer']);


		return Common::SendMimeEmail($sEmail,
																 $aHeaders,
																 $aBodyTXT,
																 array(),
																 $aGetParams);
	} // end of sendEditConfirmationEmail() function
	
	
	/**
	 * Metoda pobiera i zwraca w postaci listy wszystkie
	 * grupy kategorii newslettera
	 *
	 * @return	array ref	- tablica z kategoriami newslettera
	 */
	function &getCategoriesGroupsList() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aItems = unserialize($oCache->get('newsletter_cats_groups_'.md5($this->iPageId), 'queries')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT id, name, show_name
							 FROM ".$aConfig['tabls']['prefix']."newsletters_categories_groups
							 WHERE page_id = ".$this->iPageId."
							 ORDER BY order_by";
			$aItems =& Common::GetAssoc($sSql, true);
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aItems), 'newsletter_cats_groups_'.md5($this->iPageId), 'queries');
		}
		return $aItems;
	} // end of getCategoriesGroupsList() method
	
	
	/**
	 * Metoda pobiera i zwraca w postaci listy grupy kategorii
	 * newslettera dla wybranych przez uzytkownika kategorii (maslo maslane :))
	 *
	 * @param	array ref	$aCIds	- Id wybranych kategorii
	 * @return	array ref	- tablica z kategoriami newslettera
	 */
	function &getChosenCategoriesGroupsList(&$aCIds) {
		global $aConfig, $oCache;
		
		$sSql = "SELECT A.id, A.name, A.show_name
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories_groups A
						 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories B
						 ON A.id = B.group_id
						 WHERE B.page_id = ".$this->iPageId." AND
						 			 B.id IN (".implode(',', $aCIds).")
						 ORDER BY A.order_by";
		return Common::GetAssoc($sSql, true);
	} // end of getChosenCategoriesGroupsList() method
	

	/**
	 * Metoda pobiera i zwraca w postaci listy wszystkie
	 * kategorie newslettera
	 *
	 * @return	array ref	- tablica z kategoriami newslettera
	 */
	function &getCategoriesList() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aItems = unserialize($oCache->get('newsletter_cats_'.md5($this->iPageId), 'queries')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT B.id gid, A.id AS value, A.name AS label, A.forced
							 FROM ".$aConfig['tabls']['prefix']."newsletters_categories A
							 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories_groups B
							 ON B.id = A.group_id
							 WHERE A.public = '1' AND
								 		 A.page_id = ".$this->iPageId."
								 		 AND unregistred_cat = '0'
							 ORDER BY B.order_by, A.order_by";
			$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aItems), 'newsletter_cats_'.md5($this->iPageId), 'queries');
		}
		return $aItems;
	} // end of getCategoriesList() method
	
	
	/**
	 * Metoda pobiera i zwraca w postaci tablicy kategorie ktore maja byc domyslnie zaznaczone
	 *
	 * @return	array ref	- tablica z kategoriami newslettera
	 */
	function &getDefaultSelectedCategories() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aItems = unserialize($oCache->get('newsletter_cats_def_sel'.md5($this->iPageId), 'queries')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT id, ' checked=\"checked\"'
							 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
							 WHERE page_id = ".$this->iPageId." AND
							 			 public = '1' AND
							 			 default_selected = '1'";
			$aItems =& Common::GetAssoc($sSql);
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aItems), 'newsletter_cats_def_sel'.md5($this->iPageId), 'queries');
		}
		return $aItems;
	} // end of getDefaultSelectedCategories() method
	
	function getSelectedCatsFromPost() {
		global $aConfig;
		$aItems=array();
		foreach($_POST['n_categories'] as $iKey=>$iVal) {
			if($iVal==1) {
				$aItems[$iKey] = ' checked="checked"';
			}
		}
		return $aItems;
	}


	/**
	 * Metoda pobiera i zwraca w postaci listy wybrane przez uzytkownika
	 * kategorie newslettera
	 *
	 * @param	array	$aCategories	- tablica z kategoriami newslettera
	 * @return	string	- lista zamowionych kategorii newslettera
	 */
	function getCategoriesNames(&$aCategories) {
		global $aConfig;
		$sCategories = '';
		
		$aChosenCats = array_keys($aCategories);
		// pobranie grup kategorii newslettera
		$aGroups =& $this->getChosenCategoriesGroupsList($aChosenCats);
		// pobranie kategorii newslettera
		$aCategories =& $this->getCategoriesList();
		
		foreach ($aGroups as $iGId => $aGroup) {
			if ($aGroup['show_name'] == '1') {
				$sCategories .= "\t".$aGroup['name'].":\n";
			}
			// wypisanie wybranych z tej grupy kategorii
			foreach ($aCategories[$iGId] as $aCategory) {
				if (in_array($aCategory['value'], $aChosenCats)) {
					$sCategories .= "\t\t- ".$aCategory['label']."\n";
				}
			}
		}
		return $sCategories;
	} // end of getCategoriesNames() method


	/**
	 * Metoda zwraca adres email subskrybenta
	 *
	 * @return	string	- email
	 */
	function getEmail() {
		return isset($this->aSubscriber['email']) ? $this->aSubscriber['email'] : '';
	} // end of getEmail() method

	/**
	 * Metoda zwraca kategorie subskrybenta
	 *
	 * @return	string	- email
	 */
	function getCategories() {
		return isset($this->aSubscriber['categories']) ? $this->aSubscriber['categories'] : array();
	} // end of getCategories() method


	/**
	 * Metoda pobiera i zwraca Id strony newslettera
	 *
	 * @return	integer
	 */
	function getPageId() {
		global $aConfig;

		$sSql = "SELECT A.id
						 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
						 JOIN ".$aConfig['tabls']['prefix']."modules AS B
						 ON A.module_id = B.id
						 WHERE B.symbol = '".$this->sModule."' AND
						 			 A.moption IS NULL";
		return (int) Common::GetOne($sSql);
	} // end of getPageId() method


	/**
	 * Metoda pobiera i zwraca Id subskrybenta o podanym adresie email
	 *
	 * @return	integer
	 */
	function getSubscriberId($sEmail) {
		global $aConfig;

		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE page_id = ".$this->iPageId." AND
									 email = ".Common::Quote($sEmail);
		return (int) Common::GetOne($sSql);
	} // end of getSubscriberId() method
	
	/**
	 * Zwraca id kategorii dla niezarejestrowanych uzytkownikow
	 * @return int
	 */
	function getUnregistredCategoryId(){
		global $aConfig;

		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
						 WHERE unregistred_cat='1' LIMIT 1";
		return (int) Common::GetOne($sSql);
	} // end of getUnregistredCategoryId()
} // end of Newsletter class
?>