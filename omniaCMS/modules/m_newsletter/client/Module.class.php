<?php
/**
 * Klasa Module do obslugi strony modulu 'Newslettera'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

// dolaczenie klasy newslettera
include_once('Newsletter.class.php');

class Module {
  
  // Id strony
  var $iPageId;
    
  // szablon strony
  var $sTemplate;
  
  // symbol modulu
  var $sModule;
  
  // ustawienia strony modulu
  var $aSettings;
  
  // link do strony newslettera
 	var $sPageLink; 
    
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module() {
		global $aConfig;
		
		$this->iPageId = $aConfig['_tmp']['page']['id'];
		$this->sModule = $aConfig['_tmp']['page']['module'];
		$this->aSettings =& $this->getSettings();
		$this->sTemplate = 'modules/'.$this->sModule.'/'.$this->aSettings['template'];
		$this->sPageLink = '/'.$aConfig['_tmp']['page']['symbol'];
	} // end Module() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla strony
	 * 
	 * @return	array
	 */
	function getSettings() {
		global $aConfig, $oCache;
		
		if (!$aConfig['common']['caching'] || !is_array($aCfg = unserialize($oCache->get(md5($aConfig['_tmp']['page']['symbol']), 'modules_settings')))) {
			// wylaczone cache'owanie lub danych nie ma w cache'u
			// pobranie ustawien strony modulu
			$sSql = "SELECT * 
							 FROM ".$aConfig['tabls']['prefix']."newsletters_settings 
							 WHERE page_id = ".$this->iPageId;
			$aCfg = Common::GetRow($sSql);
			// zapis do cache'a
			if ($aConfig['common']['caching'])
				$oCache->save(serialize($aCfg), md5($aConfig['_tmp']['page']['symbol']), 'modules_settings');
		}
		return $aCfg;
	} // end of getSettings() function
	
	
	/**
	 * Metoda pobiera tresc strony o id $this->iPageId
	 * dowiazuje ja do Smartow oraz zwraca xHTML z trescia strony po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$bShowForm = true;
		$aModule['lang'] =& $aConfig['lang']['mod_'.$this->sModule];
		$aModule['page_link'] =& $this->sPageLink;
		
		$oNewsletter = new Newsletter($this->sModule,
										$this->iPageId,
										$this->aSettings,
										$this->sPageLink,
										$_GET['parameters'][0]);

		if (!empty($_POST)) {
			// sprawdzenie czy formularz zostal poprawnie wypelniony
			$iRecpId = $this->findRecipient();
			$bHasMail=($iRecpId != false);
			
			if ($this->validateForm($bHasMail && !isset($_POST['n_frombox']))) {
				switch ($_GET['action']) {
					case 'edit':
						// edycja
						$aModule['data']['n_email'] = $oNewsletter->getEmail();
						if (empty($aModule['data']['n_email'])) {
							// adres nie istnieje
							setMessage($aConfig['lang']['mod_'.$this->sModule]['doesnt_exists'], true);
							$bShowForm = false;
						}
						else {
							$this->sPageLink = $_SERVER['REQUEST_URI'];
							$aModule['action'] = 'edit';
							
							if ($oNewsletter->edit($_POST['n_email'], $_POST['n_categories'])) {
								// zaktualizowano
								setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['recipient_edited'],
																	 $aModule['data']['n_email']), false);
							}
							else {
								// wystapil blad
								setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['error'],
																	 $aConfig['_settings']['site']['email'],
																	 $aConfig['_settings']['site']['email']), true);
							}
						}
					break;
					
					default:
						// dodawanie
						switch ($oNewsletter->add($_POST['n_email'], $_POST['n_categories'])) {
							case -1:
								// wystapil blad
								setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['error'],
																	 $aConfig['_settings']['site']['email'],
																	 $aConfig['_settings']['site']['email']), true);
							break;
							
							case 0:
								// adres zostal dodany
								setMessage(printWebsiteMessage(getWebsiteMessage('newsletter_nowy_uzytkownik'),
																		array('email','nazwa_newslettera','dni_na_potwierdzenie'),
																	 array($_POST['n_email'],
																	 $this->aSettings['newsletter_name'],
																	 $this->aSettings['days_to_confirm'])));
								$bShowForm = false;
							break;
							
							case 1:
								// adres istnieje ale jest jeszcze nie potwierdzony
								setMessage(printWebsiteMessage(getWebsiteMessage('newsletter_niepotwierdzony'),array('email','nazwa_newslettera'), array($_POST['n_email'], $this->aSettings['newsletter_name'])), true);
								$bShowForm = false;
							break;
							
							case 2:
								
								// adres istnieje i jest potwierdzony
								$oNewsletter->add_tmp($_POST['n_email'], $_POST['n_categories']);
								setMessage(printWebsiteMessage(getWebsiteMessage('newsletter_uzytkownik_istnieje'), array('email','nazwa_newslettera'),array($_POST['n_email'], $this->aSettings['newsletter_name'])), false);
								$bShowForm = false;
							break;
						}
					break;
				}
			}
			if ($bShowForm) {
				if($bHasMail){
					$aConfig['_tmp']['page']['name'] .= $aModule['lang']['edit_subscriptions'];
				//$aModule['subname']=$aModule['lang']['edit_subscriptions'];
					$aModule['sendbutton'] = $aModule['lang']['edit_send'];
				} else {
					$aModule['sendbutton'] = $aModule['lang']['add_send'];
				}
				// ustawienie wartosci formularza
				$aModule['data']['n_email'] =& $_POST['n_email'];
				if($bHasMail){
					$aModule['data']['n_categories'] = $this->getCategories($iRecpId);
				} else {
					$aModule['data']['n_categories'] = isset($_POST['n_categories']) ? $_POST['n_categories'] : array();
				
				}
				foreach ($aModule['data']['n_categories'] as $iCId => $mVal) {
					if ($mVal == '1') {
						$aModule['data']['n_categories'][$iCId] = ' checked="checked"';
					}
				}
				if (isset($_GET['action']) && $_GET['action'] == 'edit') {
					$this->sPageLink = $_SERVER['REQUEST_URI'];
					$aModule['action'] = 'edit';
				}
			}
		}
		else {
			switch ($_GET['action']) {
				case 'edit':
					// edycja
					$aModule['data']['n_email'] = $oNewsletter->getEmail();
					if (empty($aModule['data']['n_email'])) {
						// adres nie istnieje
						setMessage($aConfig['lang']['mod_'.$this->sModule]['doesnt_exists'], true);
						$bShowForm = false;
					}
					else {
						$this->sPageLink = $_SERVER['REQUEST_URI'];
						$aModule['action'] = 'edit';
						
						$aModule['data']['n_categories'] = $oNewsletter->getCategories();
						foreach ($aModule['data']['n_categories'] as $iCId => $mVal) {
							if ($mVal == '1') {
								$aModule['data']['n_categories'][$iCId] = ' checked="checked"';
							}
						}
					}
				break;
				
				case 'delete':
					$aModule['data']['n_email'] = $oNewsletter->getEmail();
					if (empty($aModule['data']['n_email'])) {
						// adres nie istnieje
						setMessage($aConfig['lang']['mod_'.$this->sModule]['doesnt_exists'], true);
					}
					else {
						if ($oNewsletter->delete()) {
								// usunieto
								setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['recipient_deleted'],
																	 $aModule['data']['n_email']));
							}
							else {
								// wystapil blad
								setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['error'],
																	 $aConfig['_settings']['site']['email'],
																	 $aConfig['_settings']['site']['email']), true);
							}
					}
					$bShowForm = false;
				break;
				
				case 'confirm':
					// wybrano potwierdzenie subskrypcji
					switch ($oNewsletter->confirm()) {
						case -1:
							// wystapil blad
							setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['error'],
																 $aConfig['_settings']['site']['email'],
																 $aConfig['_settings']['site']['email']), true);
						break;
						
						case 0:
							// adres zostal potwierdzony
							setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['recipient_confirmed'],
																 $oNewsletter->getEmail()));
						break;
						
						case 1:
							// adres jest juz potwierdzony
							setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['already_confirmed'],
																 $oNewsletter->getEmail()), true);
						break;
						
						case 2:
							// adres nie istnieje
							setMessage($aConfig['lang']['mod_'.$this->sModule]['doesnt_exists'], true);
						break;
					}
					$bShowForm = false;
				break;
				case 'edit_confirm':
					// wybrano potwierdzenie subskrypcji
					if($oNewsletter->edit_confirm()) {
						// adres zostal potwierdzony
						setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['edit_confirmed'],
															 $oNewsletter->getEmail()));
					} else {
						// wystapil blad
						setMessage(sprintf($aConfig['lang']['mod_'.$this->sModule]['error'],
															 $aConfig['_settings']['site']['email'],
															 $aConfig['_settings']['site']['email']), true);
					}
					$bShowForm = false;
				break;
        
        // usunięcie subskrypcji newslettera
        case 'delete_promotion_agreement':
          $this->_deletePromotionAgreement($oNewsletter);
        break;
			}
		}
				
		if ($bShowForm) {
			// pobranie grup kategorii newslettera
			$aModule['categories_groups'] =& $oNewsletter->getCategoriesGroupsList();
			// pobranie kategorii newslettera
			$aModule['categories'] =& $oNewsletter->getCategoriesList();
			
			if (!isset($_POST['n_categories']) && empty($aModule['data']['n_categories'])) {
				// nie byly przeslane kategorie - oznaczenei kategorii domyslnie zaznaczonych
				$aModule['data']['n_categories'] =& $oNewsletter->getDefaultSelectedCategories();
			}
			if (!empty($aModule['categories'])) {
				$pSmarty->assign_by_ref('sFText', $this->aSettings['ftext']);
				$aModule['name'] = $aConfig['_tmp']['page']['name'];
				$pSmarty->assign_by_ref('aModule', $aModule);
				$sHtml = $pSmarty->fetch($this->sTemplate);
				$pSmarty->clear_assign('aModule');
			}
		}		
		return $sHtml;
	} // end of getContent()
	
  
  
  /**
   * Metoda usuwa zgodę na promocje
   * 
   * @param string $sToken
   */
  private function _deletePromotionAgreement(&$oNewsletter) {
    
    // usuwamy zgodę na otrzymywanie kodów rabatowych
    if ($oNewsletter->aSubscriber['email'] != '') {// md5(id:email)
      // sprawdzamy token
      
      include_once('modules/m_konta/client/User.class.php');
      $aTMP = array();
      $oUser = new User('',0,$aTMP,'');
      if ($oUser->deletePromotionAgreement($oNewsletter->aSubscriber['email']) === false) {
          setMessage(_('Wystąpił błąd podczas rezygnacji z otrzymywania informacji handlowych <b>w tym kodów rabatowych CODE: 201</b>'), true, true, 8000);
          doRedirectHttps('/');
      } else {
        setMessage(_('Rezygnacja z otrzymywania informacji handlowych <b>w tym kodów rabatowych</b> została przeprowadzona pomyśnie.'), false, true, 8000);
        doRedirectHttps('/');
        /*
        if ($oNewsletter->deleteAllCategories() === false) {
          setMessage(_('Wystąpił błąd podczas rezygnacji z otrzymywania informacji handlowych <b>w tym kodów rabatowych CODE: 202</b>'), true, true, 8000);
          doRedirectHttps('/');
        } else {
          setMessage(_('Rezygnacja z otrzymywania informacji handlowych <b>w tym kodów rabatowych</b> została przeprowadzona pomyśnie.'), false, true, 8000);
          doRedirectHttps('/');
        }
         */
      }
    } else {
      setMessage(_('Twój e-mail nie znajduje się na liście newslettera, prawdopodobnie został wcześniej usunięty.'), false, true, 8000);
      doRedirect('/');
    }
    die;
  }// end of _deletePromotionAgreement() method
  
  
	function findRecipient(){
		global $aConfig;
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE page_id = ".$this->iPageId." AND
									 email = ".Common::Quote($_POST['n_email']);
		return (int) Common::GetOne($sSql);
	}
	
		/**
	 * Metoda zwraca kategorie subskrybenta
	 *
	 * @return	string	- email
	 */
	function &getCategories($iId) {
		global $aConfig;
		// pobranie kategorii subskrybenta
			$sSql = "SELECT category_id, 1
						 	 FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat
						 	 WHERE recipient_id = ".$iId;
			return Common::GetAssoc($sSql);
	} // end of getCategories() method
	/**
	 * Metoda sprawdza czy formularz zostal poprawnie wypelniony
	 * 
	 * @return	bool	- true: poprawnie; false: niepoprawnie
	 */
	function validateForm($bHasMail) {
		global $aConfig;
		
    if (!isset($_POST['n_regulations'])) {
			// brak zgody na przetważanie danych osobowych
			if(!isset($_POST['n_frombox'])) setMessage($aConfig['lang']['mod_'.$this->sModule]['regulations_err'], true);
			return false;
    }
    
		if (!preg_match($aConfig['class']['form']['email']['pcre'], $_POST['n_email'])) {
			// bledny adres email
			setMessage($aConfig['lang']['mod_'.$this->sModule]['email_err'], true);
			return false;
		}
		
		if (!$bHasMail && empty($_POST['n_categories'])) {
			// nie wybrano zadnej kategorii
			if(!isset($_POST['n_frombox'])) setMessage($aConfig['lang']['mod_'.$this->sModule]['category_err'], true);
			return false;
		}
    
		return true;
	} // end of validateForm() method
} // end of Module Class
?>