<?php
/**
 * Klasa Module do obslugi modulu 'strony newslettera - kategorie'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {


			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych dla strony kategorii newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'send_date',
				'content'	=> $aConfig['lang'][$this->sModule]['list_send_date'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'status',
				'content'	=> $aConfig['lang'][$this->sModule]['list_status'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'sent_count',
				'content'	=> $aConfig['lang'][$this->sModule]['list_sent_count'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'clicks',
				'content'	=> $aConfig['lang'][$this->sModule]['list_clicks'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);

		// pobranie liczby wszystkich kategorii strony
		$sSql = "SELECT COUNT(A.id)
						 FROM ".$aConfig['tabls']['prefix']."newsletters_auto A
						 WHERE 1=1 ".
									 (isset($_POST['f_status']) && $_POST['f_status'] != '' ? ' AND A.status = '.$_POST['f_status'] : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(A.id)
							 FROM ".$aConfig['tabls']['prefix']."newsletters_auto A";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('categories', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// dodanie filtru grupy
		//
		$aStatuses=array(
			array('value' => '', 'label' => $aConfig['lang'][$this->sModule]['f_all']),
			array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['status_0']),
			array('value' => '1', 'label' => $aConfig['lang'][$this->sModule]['status_1']),
			array('value' => '2', 'label' => $aConfig['lang'][$this->sModule]['status_2']),
			array('value' => '4', 'label' => $aConfig['lang'][$this->sModule]['status_4'])
		);
		$pView->AddFilter('f_status', $aConfig['lang'][$this->sModule]['f_status'], $aStatuses, $_POST['f_status']);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich paragrafow strony
			$sSql = "SELECT A.id, A.send_date, A.status, A.sent_count, B.id AS newsletter_id,
											IFNULL((SELECT SUM(clicked) FROM ".$aConfig['tabls']['prefix']."newsletters_links WHERE newsletter_id = B.id), 0) AS clicks
							 FROM ".$aConfig['tabls']['prefix']."newsletters_auto A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."newsletters B
							 ON A.id=B.auto_id
							 WHERE 1=1".
										 (isset($_POST['f_group']) && $_POST['f_group'] != '' ? ' AND A.group_id = '.$_POST['f_group'] : '').
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND A.name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.send_date').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : ' DESC').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			foreach($aRecords as $iKey=>$aRecord){
				$aRecords[$iKey]['status'] = $aConfig['lang'][$this->sModule]['status_'.$aRecord['status']];
				//unset($aRecords[$iKey]['newsletter_id']);
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'newsletter_id'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('clicks'),
					'params' => array (
												'clicks'	=> array('pid' => $iPId, 'ppid'=>'{newsletter_id}', 'action'=>'clicks', 'do'=>'')
											),
					'icon' => array(
												'clicks' => 'bars'
											)
				)
				
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('go_back')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		);
		
		
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show());
	} // end of Show() function


} // end of Module Class
?>