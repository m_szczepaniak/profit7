<?php
/**
 * Klasa Module do obslugi konfiguracji stron modulu 'Newsletter'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Settings {

  // nazwa modulu - do langow
	var $sModule;

	// Id strony dla ktorej edytowane sa ustawienia
	var $iId;

	// sciezka do katalogu zawierajacego szablony czesci klienckiej modulu
	var $sTemplatesDir;

	// lista szablonow czesci klienckiej modulu
	var $sTemplatesList;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$sModule, &$iId) {
		global $aConfig;
		$this->sModule = $sModule;
		$this->iId = $iId;
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/modules/'.$this->sModule;
		$this->sTemplatesList =& GetTemplatesList($this->sTemplatesDir);

		// pobranie konfiguracji dla modulu
		$this->setSettings();
	} // end Settings() function
	

	/**
	 * Metoda pobiera konfiguracje dla strony modulu lub w przypadku
	 * 
	 * @return	void
	 */
	function setSettings() {
		global $aConfig;
		$aCfg = array();

		// pobranie konfiguracji dla strony
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."newsletters_settings
						 WHERE page_id = ".$this->iId;
		$aCfg = Common::GetRow($sSql);
		if (!isset($aCfg['days_to_confirm']) || empty($aCfg['days_to_confirm'])) {
			$aCfg['days_to_confirm'] = $aConfig['default']['days_to_confirm'];
		}
		// dolaczenie ustawien do glownej tablicy ustawien $aConfig
		$aConfig['settings'][$this->sModule] =& $aCfg;
	} // end of setSettings() method


	/**
	 * Metoda wprowadza zmiany w ustawieniach strony newslettera
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;

		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."newsletters_settings
						 WHERE page_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);
		
		$aValues = array(
			'template' => $_POST['template'],
	 		'newsletter_name' => trim($_POST['newsletter_name']),
	 		'main_email' => $_POST['main_email'],
	 		'conf_email_subject' => trim($_POST['conf_email_subject']),
 			'days_to_confirm' => (int) $_POST['days_to_confirm'],
	 		'preview_emails' => $_POST['preview_emails'],
	 		'ftext' => trim($_POST['ftext']) != '' ? $_POST['ftext'] : 'NULL',
			'auto_send_subject' => trim($_POST['auto_send_subject']),
			'auto_send_number' => (int) $_POST['auto_send_number'],
			'auto_sending_day' => (int) $_POST['auto_sending_day'],
			'auto_sender' => trim($_POST['auto_sender']),
			'auto_sender_email' => trim($_POST['auto_sender_email'])
		);
		if (!empty($aSettings)) {
			// update
			return Common::Update($aConfig['tabls']['prefix']."newsletters_settings",
														$aValues,
														"page_id = ".$this->iId) !== false;
		}
		else {
			// insert
			$aValues['page_id'] = $this->iId;

			return Common::Insert($aConfig['tabls']['prefix']."newsletters_settings",
														$aValues,
														'',
														false) !== false;
		}
	} // end of UpdateSettings() funciton


	/**
	 * Metoda tworzy formularz edycji konfiguracji strony
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_mtype') {
			$aData =& $_POST;
		}

		// szablon strony
		$pForm->AddSelect('template', $aConfig['lang'][$this->sModule.'_settings']['template'], array(), $this->sTemplatesList, $aData['template'], '', false);
		
		// nazwa newslettera
		$pForm->AddText('newsletter_name', $aConfig['lang'][$this->sModule.'_settings']['newsletter_name'], $aData['newsletter_name'], array('maxlength'=>255, 'style'=>'width: 350px;'));
		
		// glowny adres email newslettera (do wysylki aktywacji)
		$pForm->AddText('main_email', $aConfig['lang'][$this->sModule.'_settings']['main_email'], $aData['main_email'], array('maxlength'=>64, 'style'=>'width: 250px;'), '', 'email');
		
		// temat maila aktywacyjnego
		$pForm->AddText('conf_email_subject', $aConfig['lang'][$this->sModule.'_settings']['conf_email_subject'], $aData['conf_email_subject'], array('maxlength'=>255, 'style'=>'width: 350px;'));
		
		// adresy email do wysylki podgladu
		$pForm->AddText('preview_emails', $aConfig['lang'][$this->sModule.'_settings']['preview_emails'], $aData['preview_emails'], array('maxlength'=>255, 'style'=>'width: 350px;'), '', 'emails');

		// liczba dni na potwierdzenie
		$pForm->AddText('days_to_confirm', $aConfig['lang'][$this->sModule.'_settings']['days_to_confirm'], $aData['days_to_confirm'], array('maxlength'=>3, 'style'=>'width: 50px;'), '', 'uinteger', true);
		
		// dzien rozsylki newsletterow nowosci
		$pForm->AddSelect('auto_sending_day',$aConfig['lang'][$this->sModule.'_settings']['auto_sending_day'], array(), $this->getDays(),$aData['auto_sending_day'],'',true);
		// temat maila nowosci
		$pForm->AddText('auto_send_subject', $aConfig['lang'][$this->sModule.'_settings']['auto_send_subject'], $aData['auto_send_subject'], array('maxlength'=>255, 'style'=>'width: 350px;'));
		// ilosc popzycji w newsletterze nowosci
		$pForm->AddText('auto_send_number', $aConfig['lang'][$this->sModule.'_settings']['auto_send_number'], $aData['auto_send_number'], array('style'=>'width: 75px;'), '', 'uinteger');
		// nadawca
		$pForm->AddText('auto_sender', $aConfig['lang'][$this->sModule.'_settings']['auto_sender'], $aData['auto_sender'], array('maxlength'=>255, 'style'=>'width: 350px;'), '', 'text');
		// email
		$pForm->AddText('auto_sender_email', $aConfig['lang'][$this->sModule.'_settings']['auto_sender_email'], $aData['auto_sender_email'], array('maxlength'=>64, 'style'=>'width: 250px;'), '', 'email');
		
		// opis do formularza dodawania do newslettera
		$pForm->AddWYSIWYG('ftext', $aConfig['lang'][$this->sModule.'_settings']['ftext'], $aData['ftext'], array('theme' => 'simple'), '', false);
	} // end of SettingsForm() function
	
	function &getDays(){
		global $aConfig;
		$aDays=array(array('value' => '',
													'label' => $aConfig['lang']['common']['choose']));
		foreach($aConfig['lang']['days'] as $iKey => $sDay){
			$aDays[]=array('value'=>$iKey,'label'=>$sDay);
		}
		return $aDays;
	}
} // end of Settings Class
?>