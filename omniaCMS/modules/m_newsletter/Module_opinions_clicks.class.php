<?php
/**
 * Klasa Module do obslugi modulu 'strony newslettera - kategorie'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {

			case'opinion_clicks': $this->OpinionClicks($pSmarty, $iPId); break;
			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych dla strony kategorii newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony
	 * @return	void
	 */
function Show(&$pSmarty, $iId) {
	global $aConfig;
		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);
		
		$aData=Common::GetRow('SELECT send_date, type FROM `orders_opinions_sent` WHERE id='.$iId);
		$_GET['pd']=$aData['send_date'];
		$_GET['pt']=$aConfig['lang'][$this->sModule]['type_'.$aData['type']];
		
		$aHeader = array(
			'header' => $aConfig['lang'][$this->sModule]['list_header'].' - '.$_GET['pd'].' - '.$_GET['pt'],
			'refresh' => true,
			'search' => true,
			'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'email',
				'content'	=> $aConfig['lang'][$this->sModule]['list_email'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'clicked',
				'content'	=> $aConfig['lang'][$this->sModule]['list_clicked'],
				'sortable'	=> true,
				'width'	=> '75'
			),
			array(
				'db_field'	=> 'clicked',
				'content'	=> $aConfig['lang'][$this->sModule]['list_clicked_date'],
				'sortable'	=> true,
				'width'	=> '140'
			)
		);
		$_GET['pt']=substr($_GET['pt'],0,1);
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."orders_opinions_sent
						 WHERE send_date = '".$_GET['pd']."' AND `type`='".$_GET['pt']."'";
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."orders_opinions_sent
						 WHERE send_date = '".$_GET['pd']."' AND `type`='".$_GET['pt']."'";
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('newsletters_links', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT email, clicked, clicked_date
							 FROM ".$aConfig['tabls']['prefix']."orders_opinions_sent
							 WHERE send_date = '".$_GET['pd']."' AND `type`='".$_GET['pt']."'
							 ".
								($_POST['search']!=''?' AND email LIKE \'%'.$_POST['search'].'%\'':'')
								."
							 ORDER BY ".(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id ').
							 (isset($_GET['order']) && !empty($_GET['order']) ? " ".$_GET['order'] : '  DESC').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			foreach ($aRecords as $iKey => $aRecord) {
				$aRecords[$iKey]['clicked']=$aRecords[$iKey]['clicked']==0?'NIE':'TAK';
				$aRecords[$iKey]['clicked_date']=$aRecords[$iKey]['clicked_date']=='0000-00-00 00:00:00'?'':$aRecords[$iKey]['clicked_date'];
				// przycicecie linku
				//$aRecords[$iKey]['link'] = substr($aRecord['link'],0, 100);
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('go_back')
		);
		{
			$aRecordsFooterParams = array(
				'go_back' => array(array('action'=>'opinions_sent'), array())
		
			);
		}
		
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
}	//end of func
	
} // end of Module Class
?>