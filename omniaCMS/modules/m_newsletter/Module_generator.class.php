<?php

use Newsletter\QueryGenerator;

class Module
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID strony zamowien
    var $iPageId;

    // ID wersji jezykowej
    var $iLangId;

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * Konstruktor klasy
     *
     * @return    void
     */
    function Module(&$pSmarty)
    {
        global $aConfig, $pDbMgr;

        $this->sErrMsg = '';
        $this->iPageId = getModulePageId('m_zamowienia');
        $this->sModule = $_GET['module'];
        $this->iLangId = $_SESSION['lang']['id'];
        $this->pDbMgr = $pDbMgr;

        $sDo = '';

        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        switch ($sDo) {
            case 'genenrate':
                $this->Generate($pSmarty);
                break;
            default:
                $this->Edit($pSmarty);
                break;
        }
    } // end of Module() method

    function getProductsCategoriesChTree($aMId, $bAllowOther=false) {
        global $aConfig;
        $aItems = array();
        if (empty($aMId)) return $aItems;

        $sSql = "SELECT IFNULL(parent_id, 0), id, name, mtype, module_id,
										IFNULL(link_to_id, 0) AS link_to_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE menu_id IN (".implode(',',$aMId).") AND
						 			 language_id = ".$_SESSION['lang']['id']."
						 			 ORDER BY parent_id, order_by";
        $aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
        // utworzenie drzewa menu

        $aMenuItems =& getMenuTree($aItems);

        if($bAllowOther){
            $iAllowedModule=0;
        } else {
            $iAllowedModule=getModuleId('m_oferta_produktowa');
        }


        $aMenuTree2=getCheckboxMenuTree($aMenuItems, 0, 0, 0, $iAllowedModule);
        return $aMenuTree2;
    }

    private function getFields()
    {
        return [
            [
                'label' => 'Imię',
                'value' => 'UI.name'
            ],
            [
                'label' => 'Nazwisko',
                'value' => 'UI.surname'
            ],
            [
                'label' => 'Kod pocztowy',
                'value' => 'UI.postal'
            ]
        ];
    }

    /**
     * Metoda tworzy formularz edycji danych sklepu
     *
     * @param        object $pSmarty
     */
    function Edit(&$pSmarty, $bAddPhoto = false)
    {
        global $aConfig;
        include_once('Form/FormTable.class.php');

        $data = $_POST['query'];

        $sHeader = $aConfig['lang'][$this->sModule]['header'];
        $aExtraCat =  $this->getProductsCategoriesChTree(Common::getProductsMenu($this->iLangId));

        $pForm = new FormTable('generator', _('Generator maili'), array('action' => phpSelf(), 'enctype' => 'multipart/form-data'), array('col_width' => 175), $aConfig['common']['js_validation']);
        $pForm->AddCheckBoxSet('query[fields]', 'Dodatkowe kolumny do pobrania', $this->getFields(), $data['fields'], false, false);
        $pForm->AddHidden('do', 'genenrate');
        $pForm->AddSelect('query[website_id]', _('Wybierz sklep'), [], addDefaultValue($this->getWebsites()), $data['website_id'], '');
        $pForm->AddSelect('query[strategy]', _('Wybierz strategie generowania'), ['onchange' => 'changeSelect(this)'], $this->getStrategies(), 'default', '', false);
        $pForm->AddText('query[date_from]', _('Zarejestrowani od'), '', [], [], 'date', false);
        $pForm->AddText('query[date_to]', _('Zarejestrowani do'), '', [], [], 'date', false);
        $pForm->AddText('query[year]', _('Rok wydania'), $data['year'], [], [], 'text', false);
        $pForm->AddText('query[publisher]', _("Wydawca"), $data['publisher'], array('id' => 'publisher_aci', 'style' => 'width:300px;', 'maxlength' => 255), '', 'text', false, false, false);

//        $pForm->AddCheckBox('query[promotions_agreement]', _('Użytkownicy którzy wyraźili zgodę'), '', [], true, false);
//        $pForm->AddCheckBox('query[deleted]', _('Użytkownicy którzy dezaktywowali konto'), '', [], false, false);
        $pForm->AddSelect('query[order_statuses]', _('Statusy zamówień'), ['multiple' => true], $this->getOrderStatuses(), $data['order_statuses'], '', false);
        $pForm->AddText('query[created_from]', _('Zamówienia utworzone od'), '', [], [], 'date', false);
        $pForm->AddText('query[created_to]', _('Zamówienia utworzone do'), '', [], [], 'date', false);
        $pForm->AddTextArea('query[products]', _('Id produktow (z profit24.pl) podawane po przecinku, spacji lub nowej lini'), '', ['style' => 'width: 100%; height: 300px', 'class' => 'products-inpt'], '', '', '', false);
        $pForm->AddRow('Produkty które znajdują sie w conajmniej jednej z wybranych kategori (NIE MYLIC Z KATEGORIĄ GLÓWNĄ)', '<div id="cat-tree" style="height:300px; overflow:auto;">' .
            Form::GetCheckTree('query[extra_categories][]', array('free_choice' => true), $aExtraCat, [])
            . '</div>'
        );

        $js = $this->js();

        // przyciski
        $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Generuj')).$js);

        $pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '') . ShowTable($pForm->ShowForm()));
    }

    private function js()
    {
        $js = '<script type="text/javascript">

                $(document).ready(function(){

                    var productContainer = $(".products-inpt").parent().parent();
                    var catTree = $("#cat-tree").parent().parent();

                    productContainer.hide();
                    catTree.hide();

                      $("#publisher_aci").autocomplete({
                        source: "ajax/GetPublishers.php"
                        });


                  $("ul.tree").checkboxTree({
                      initializeChecked: "expanded",
                      initializeUnchecked: "collapsed",
                      onCheck: {
                          descendants: "uncheck"
                      },
                      onUncheck: {
                          descendants: "uncheck"
                      }
                  });
                });

                function changeSelect(elem)
                {
                    var productContainer = $(".products-inpt").parent().parent();
                    var catTree = $("#cat-tree").parent().parent();

                        if (elem.value == "product") {
                            catTree.hide();
                            productContainer.show();
                        } else if(elem.value == "category") {
                            catTree.show();
                            productContainer.hide();
                        } else {
                            catTree.hide();
                            productContainer.hide();
                        };
                }

                </script>';

        return $js;
    }

    private function getOrderStatuses()
    {
        return [
            [
                'name' => 'Nowe',
                'value' => '0',
            ],
            [
                'name' => 'Zrealizowane',
                'value' => '4',
            ],
            [
                'name' => 'Anulowane',
                'value' => '5',
            ],
            [
                'name' => 'W realizacji',
                'value' => '1',
            ],
            [
                'name' => 'Skompletowane',
                'value' => '2',
            ],
            [
                'name' => 'Zatwierdzone',
                'value' => '3',
            ],
        ];
    }

    function Generate(&$pSmarty, $bAddPhoto = false)
    {
        if (isset($_POST['query']['products']) && !empty($_POST['query']['products'])) {

            $products = $_POST['query']['products'];
            $products = str_replace(["\r\n", "\r", "\n", '.', ' '], ',', $products);

            $_POST['query']['products'] = $products;
        }

        $queryGenerator = new QueryGenerator($_POST['query']);
        $query = $queryGenerator->generateQuery();

//        print_r($query);
//        return $this->Edit($pSmarty);


        $rows = $this->pDbMgr->GetAll('profit24', $query);

        header('Content-Encoding: UTF-8');
        header("Content-type: text/html; charset=UTF-8");
        header("Content-Disposition:attachment;filename='export_maile.csv'");

        $f = fopen('php://output', 'w');
        foreach ($rows as $line) {
            fputcsv($f, $line);

        }
        die;
    }

    private function getStrategies()
    {
        return [
            [
                'name' => 'Szukanie bez informacji o produktach',
                'value' => 'default',
            ],
            [
                'name' => 'Szukanie poprzez podanie id produktów',
                'value' => 'product',
            ],
            [
                'name' => 'Szukanie poprzez kategorie produktu',
                'value' => 'category',
            ],
        ];
    }

    private function getWebsites()
    {
        return $this->pDbMgr->GetAll('profit24', "SELECT id, name, id as value FROM websites");
    }

    /**
     * @param $aMId
     * @param $sLabel
     * @param bool|false $bAllowOther
     * @return array
     */
    function getProductsCategories($aMId, $sLabel,$bAllowOther=false) {
        global $aConfig;
        $aItems = array();
        if (empty($aMId)) return $aItems;

        $sSql = "SELECT IFNULL(parent_id, 0), id, name, mtype, module_id,
										IFNULL(link_to_id, 0) AS link_to_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE menu_id IN (".implode(',',$aMId).") AND
						 			 language_id = ".$_SESSION['lang']['id']."
						 			 ORDER BY parent_id, order_by";
        $aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
        // utworzenie drzewa menu
        $aMenuItems =& getMenuTree($aItems);

        if($bAllowOther){
            $iAllowedModule=0;
        } else {
            $iAllowedModule=getModuleId('m_oferta_produktowa');
        }

        // strona do ktorej linkuje
        if($sLabel != ''){
            $aMenuTree2 = array_merge(
                array(array('value' => '', 'label' => $sLabel)),
                getComboMenuTree($aMenuItems, 0, 0, 0, $iAllowedModule)
            );
        }
        else {
            $aMenuTree2=getComboMenuTree($aMenuItems,0, 0, 0, $iAllowedModule);
        }
        return $aMenuTree2;
    }
}
