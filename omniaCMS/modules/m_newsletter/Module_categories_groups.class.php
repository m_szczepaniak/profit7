<?php
/**
 * Klasa Module do obslugi modulu 'Newsletter - grupy kategorii'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iId); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iPId, $iId); break;
			case 'sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show(getPagePath($iPId, $this->iLangId, true),
																			 $this->getRecords($iPId),
																			 phpSelf(array('do'=>'proceed_sort'),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."newsletters_categories_groups",
												$this->getRecords($iPId),
												array(
													'page_id' => $iPId
											 	));
			break;

			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych dla strony grup kategorii newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich kategorii strony
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories_groups
						 WHERE page_id = ".$iId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."newsletters_categories_groups
							 WHERE page_id = ".$iId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('categories_groups', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT id, name, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."newsletters_categories_groups
							 WHERE page_id = ".$iId.
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
		

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'pid' => $iId, 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('pid' => $iId, 'id'=>'{id}'),
												'delete'	=> array('pid' => $iId, 'id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('sort', 'add')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid'))
		);
		
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane grupy kategorie
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony newslettera
	 * @param	integer	$iId	- Id usuwanej grupy kategorii
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			if (!$this->groupsAreInUse($_POST['delete'])) {
				// grupy nie sa uzywane w kategoriach - mozna je usunac
				Common::BeginTransaction();
				$aItems =& $this->getItemsToDelete($_POST['delete']);
				foreach ($aItems as $aItem) {
					// usuwanie
					if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
						$bIsErr = true;
						$sFailedToDel = $aItem['name'];
						break;
					}
					elseif ($iRecords == 1) {
						// usunieto
						$iDeleted++;
						$sDel .= '"'.$aItem['name'].'", ';
					}
				}
				$sDel = substr($sDel, 0, -2);
				
				if (!$bIsErr) {
					// usunieto
					Common::CommitTransaction();
					if ($iDeleted > 0) {
						$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel, getPagePath($iPId, $this->iLangId, true));
						$this->sMsg = GetMessage($sMsg, false);
						// dodanie informacji do logow
						AddLog($sMsg, false);
					}
				}
				else {
					// blad
					Common::RollbackTransaction();
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel, getPagePath($iPId, $this->iLangId, true));
					$this->sMsg = GetMessage($sMsg);
					// dodanie informacji do logow
					AddLog($sMsg);
				}
			}
			else {
				// nie mozna usunac - jedna lub wiecej grup jest uzywana w kategoriach
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['groups_in_use_err']);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowa grupe kategorii strony newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
		// dodanie kategorii
		$aValues = array(
			'page_id' => $iPId,
			'name' => $_POST['name'],
			'show_name' => isset($_POST['show_name']) ? '1' : '0',
			'order_by' => '#order_by#',
			'created' => 'NOW()',
			'created_by' => $_SESSION['user']['name']
		);
		$bIsErr = Common::Insert($aConfig['tabls']['prefix']."newsletters_categories_groups", $aValues, "page_id = ".$iPId, false) === false;

		if (!$bIsErr) {
			// kategoria zostala dodana
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'], $_POST['name'], getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// kategoria nie zostala dodana,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'], $_POST['name'], getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId);
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych grupe kategorii strony newslettera
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id aktualizowanej grupy
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iId);
			return;
		}
		// aktualizacja kategorii
		$aValues = array(
			'name' => $_POST['name'],
			'show_name' => isset($_POST['show_name']) ? '1' : '0',
			'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name']
		);
		$bIsErr = Common::Update($aConfig['tabls']['prefix']."newsletters_categories_groups", $aValues, "id = ".$iId) === false;

		if (!$bIsErr) {
			// grupa kategorii zostala zaktualizowana
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'], $_POST['name'], getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// grupa kategorii nie zostala zmieniona,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'], $_POST['name'], getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji grupy kategorii
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iPId	- ID strony
	 * @param		integer	$iId	- ID edytowanej grupy kategorii
	 */
	function AddEdit(&$pSmarty, $iPId, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanej grupy kategorii
			$sSql = "SELECT name, show_name
							 FROM ".$aConfig['tabls']['prefix']."newsletters_categories_groups
							 WHERE id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		if (!isset($aData['show_name'])) {
			$aData['show_name'] = '1';
		}

		// okreslenie nazwy strony dla ktorej dodawana / edytowana jest grupa kategorii
		$sName = getPagePath($iPId, $this->iLangId, true);

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0) {
			$sHeader .= ' "'.$aData['name'].'"';
		}

		$pForm = new FormTable('categories_groups', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>115), $aConfig['common']['js_validation']);
		$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], $sName, '', array(), array('class'=>'redText'));
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));

		// nazwa
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>128, 'style'=>'width: 250px;'), '', 'text');
		
		// wyswietlaj nazwe grupy na liscie
		$pForm->AddCheckBox('show_name', $aConfig['lang'][$this->sModule]['show_name'], array(), '', !empty($aData['show_name']), false);
		
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda sprawdza czy grupy o przekazanych do niej Id sa uzywane
	 * w kategoriach
	 * 
	 * @param	array ref	$aIds	- Id grup kategorii
	 * @return	bool	- sa uzywane
	 */
	function groupsAreInUse(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id
				 		 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
					 	 WHERE group_id IN (".implode(',', $_POST['delete']).")";
		$aIts =& Common::GetCol($sSql);
		return is_array($aIts) && !empty($aIts);
	} // end of groupsAreInUse() method
	
	
	/**
	 * Metoda pobiera liste rekordow
	 *
	 * @param	integer	$iPId	- Id strony
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($iPId) {
		global $aConfig;

		// pobranie wszystkich grup kategorii dla danej strony
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories_groups
						 WHERE page_id = ".$iPId."
						 ORDER BY order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
	
	/**
	 * Metoda pobiera liste kategorii do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id grup kategorii
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, name
				 		 FROM ".$aConfig['tabls']['prefix']."newsletters_categories_groups
					 	 WHERE id IN (".implode(',', $_POST['delete']).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa grupe kategorii newslettera
	 * 
	 * @param	integer	$iId	- Id kategorii do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_categories_groups
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method


	/**
	 * Metooda pobiera nazwe uzytkownika ktory utworzyl kategorie o podanym Id
	 * 
	 * @param	integer	$iId	 - Id kategorii
	 * @return	string
	 */
	function getCreatorName($iId) {
		global $aConfig;
		
		$sSql = "SELECT created_by
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getCreatorName() method


} // end of Module Class
?>