<?php
/**
 * Klasa Module do obslugi modulu 'strony newslettera - kategorie'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iId); break;
			case 'edit':
			case 'change_type':
			case 'add': $this->AddEdit($pSmarty, $iPId, $iId); break;
			case 'sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show(getPagePath($iPId, $this->iLangId, true),
																			 $this->getRecords($iPId, $_GET['gid']),
																			 phpSelf(array('do'=>'proceed_sort', 'gid'=>$_GET['gid']), array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."newsletters_categories",
												$this->getRecords($iPId, $_GET['gid']),
												array(
													'page_id' => $iPId,
													'group_id' => $_GET['gid']
											 	));
			break;

			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste zdefiniowanych dla strony kategorii newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'A.name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'B.name',
				'content'	=> $aConfig['lang'][$this->sModule]['list_group_name'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'A.created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'A.created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '45'
			)
		);

		// pobranie liczby wszystkich kategorii strony
		$sSql = "SELECT COUNT(A.id)
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories A
						 WHERE A.page_id = ".$iId.
									 (isset($_POST['f_group']) && $_POST['f_group'] != '' ? ' AND A.group_id = '.$_POST['f_group'] : '').
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND A.name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(A.id)
							 FROM ".$aConfig['tabls']['prefix']."newsletters_categories A
							 WHERE A.page_id = ".$iId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('categories', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);
		
		// dodanie filtru grupy
		$pView->AddFilter('f_group', $aConfig['lang'][$this->sModule]['f_group'], $this->getGroups($iId, $aConfig['lang'][$this->sModule]['f_all_groups']), $_POST['f_group']);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich paragrafow strony
			$sSql = "SELECT A.id, A.name, B.name gname, A.created, A.created_by
							 FROM ".$aConfig['tabls']['prefix']."newsletters_categories A
							 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories_groups B
							 ON B.id = A.group_id
							 WHERE A.page_id = ".$iId.
										 (isset($_POST['f_group']) && $_POST['f_group'] != '' ? ' AND A.group_id = '.$_POST['f_group'] : '').
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND A.name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'B.order_by, A.order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do' => 'edit', 'pid' => $iId, 'id' => '{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('pid' => $iId, 'id'=>'{id}'),
												'delete'	=> array('pid' => $iId, 'id'=>'{id}')
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('check_all', 'delete_all', 'go_back'),
			array('sort', 'add')
		);
		$aRecordsFooterParams = array(
			'go_back' => array(1 => array('action', 'id', 'pid')),
			'sort' => array(array('gid' => $_POST['f_group']))
		);
		if (!isset($_POST['f_group']) || $_POST['f_group'] == '') {
			unset($aRecordsFooter[1][0]);
			unset($aRecordsFooterParams['sort']);
		}
		
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybrane kategorie
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony usuwanej kategorii
	 * @param	integer	$iId	- Id usuwanej kategorii
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['name'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['name'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel, getPagePath($iPId, $this->iLangId, true));
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel, getPagePath($iPId, $this->iLangId, true));
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowa kategorie strony newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId);
			return;
		}
		// dodanie kategorii
		$aValues = array(
			'page_id' => $iPId,
			'group_id' => (int) $_POST['group_id'],
			'name' => $_POST['name'],
			'sender' => $_POST['sender'],
			'sender_email' => $_POST['sender_email'],
			'public' => isset($_POST['public']) ? '1' : '0',
			'default_selected' => isset($_POST['default_selected']) ? '1' : '0',
			'forced' => isset($_POST['forced']) ? '1' : '0',
			'unregistred_cat' => isset($_POST['unregistred_cat']) ? '1' : '0',
			'order_by' => '#order_by#',
			'auto_send' => isset($_POST['auto_send']) ? '1' : '0',
			'created' => 'NOW()',
			'created_by' => $_SESSION['user']['name']
		);
		
		if(isset($_POST['auto_send'])){
			$aValues['send_categories'] = (int) $_POST['send_categories'];
			$aValues['send_number'] = (int) $_POST['send_number'];
			$aValues['start_sending_hour'] = $_POST['start_sending_hour'];
			$aValues['start_sending_day'] = (int) $_POST['start_sending_day'];
		}
		
		$bIsErr = Common::Insert($aConfig['tabls']['prefix']."newsletters_categories", $aValues, "page_id = ".$iPId." AND group_id = ".(int) $_POST['group_id'], false) === false;

		if (!$bIsErr) {
			// kategoria zostala dodana
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
											$_POST['name'],
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// kategoria nie zostala dodana,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
											$_POST['name'],
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId);
		}
	} // end of Insert() funciton


	/**
	 * Metoda aktualizuje w bazie danych kategorie strony newslettera
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id aktualizowanej strony
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iPId, $iId);
			return;
		}
		// aktualizacja kategorii
		$aValues = array(
			'group_id' => (int) $_POST['group_id'],
			'name' => $_POST['name'],
			'public' => isset($_POST['public']) ? '1' : '0',
			'default_selected' => isset($_POST['default_selected']) ? '1' : '0',
			'forced' => isset($_POST['forced']) ? '1' : '0',
			'unregistred_cat' => isset($_POST['unregistred_cat']) ? '1' : '0',
			'sender' => $_POST['sender'],
			'sender_email' => $_POST['sender_email'],
			'auto_send' => isset($_POST['auto_send']) ? '1' : '0',
			'modified' => 'NOW()',
			'modified_by' => $_SESSION['user']['name']
		);
		
		if(isset($_POST['auto_send'])){
			$aValues['send_categories'] = (int) $_POST['send_categories'];
			$aValues['send_number'] = (int) $_POST['send_number'];
			$aValues['start_sending_hour'] = $_POST['start_sending_hour'];
			$aValues['start_sending_day'] = (int) $_POST['start_sending_day'];
		}
		$bIsErr = Common::Update($aConfig['tabls']['prefix']."newsletters_categories", $aValues, "id = ".$iId) === false;

		if (!$bIsErr) {
			// kategoria zostala zaktualizowana
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
											$_POST['name'],
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// kategoria nie zostala zmieniona,
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
											$_POST['name'],
											getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $iPId, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji kategorii
	 *
	 * @param		object	$pSmarty
	 * @param		string	$sId	- ID strony
	 * @param		integer	$iId	- ID edytowanego akapitu
	 */
	function AddEdit(&$pSmarty, $iPId, $iId=0) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanej kategorii
			$sSql = "SELECT *, TIME_FORMAT(start_sending_hour,'%H:%i') AS start_sending_hour
							 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
							 WHERE page_id = ".$iPId." AND
							 			 id = ".$iId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		if ($iId == 0 && empty($_POST)) {
			$aData['public'] = '1';
		}

		// okreslenie nazwy strony dla ktorej dodawana / edytowana jest kategoria
		$sName = getPagePath($iPId, $this->iLangId, true);

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['name']) && !empty($aData['name'])) {
			$sHeader .= ' "'.trimString($aData['name'], 50).'"';
		}

		$pForm = new FormTable('categories', $sHeader, array('action'=>phpSelf(array('id'=>$iId))), array('col_width'=>115), $aConfig['common']['js_validation']);
		$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], $sName, '', array(), array('class'=>'redText'));
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));
		
		// grupa kategorii pol
		$aGroups = $this->getGroups($iPId, $aConfig['lang']['common']['choose']);
		if (count($aGroups) == 2) {
			$pForm->AddHidden('group_id', $aGroups[1]['value']);
		}
		else {
			$pForm->AddSelect('group_id', $aConfig['lang'][$this->sModule]['group'], array(), $aGroups, $aData['group_id']);
		}
		
		// nazwa
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('maxlength'=>128, 'style'=>'width: 250px;'), '', 'text');
		
		// nadawca
		$pForm->AddText('sender', $aConfig['lang'][$this->sModule]['sender'], $aData['sender'], array('maxlength'=>255, 'style'=>'width: 350px;'), '', 'text');

		// email
		$pForm->AddText('sender_email', $aConfig['lang'][$this->sModule]['email'], $aData['sender_email'], array('maxlength'=>64, 'style'=>'width: 250px;'), '', 'email');
		
		// typ kategorii - publiczna czy nie
		$pForm->AddCheckBox('public', $aConfig['lang'][$this->sModule]['public'], array(), '', !empty($aData['public']), false);
		
		// domyslnie zaznaczona
		$pForm->AddCheckBox('default_selected', $aConfig['lang'][$this->sModule]['default_selected'], array(), '', !empty($aData['default_selected']), false);
		
		
		$pForm->AddCheckBox('forced', $aConfig['lang'][$this->sModule]['forced'], array(), '', !empty($aData['forced']), false);
		
		$pForm->AddCheckBox('unregistred_cat', $aConfig['lang'][$this->sModule]['unregistred_cat'], array(), '', !empty($aData['unregistred_cat']), false);
		
		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['auto_send_section'],array('class'=>'merged', 'style'=>'padding-left: 210px'));
		// auomatyczna rozsyłka
		$pForm->AddCheckBox('auto_send', $aConfig['lang'][$this->sModule]['auto_send'], array('onchange'=>'ChangeObjValue(\'do\', \'change_type\'); this.form.submit();'), '', !empty($aData['auto_send']), false);
		
		if(!empty($aData['auto_send'])){
/**********************************************/
			$aExtraCat=$this->getProductsCategories(Common::getProductsMenu($this->iLangId),$aConfig['lang']['common']['choose']);
			$pForm->AddSelect('send_categories',$aConfig['lang'][$this->sModule]['send_categories'], array('style'=>'width:300px;'), $aExtraCat,$aData['send_categories'],'',true);
		//	$pForm->AddText('send_number', $aConfig['lang'][$this->sModule]['send_number'], $aData['send_number'], array('style'=>'width: 75px;'), '', 'uinteger');
		// data rozpoczecia wysylki
		//$pForm->AddText('start_sending_date', $aConfig['lang'][$this->sModule]['start_sending_date'], $aData['start_sending_date'], array('style'=>'width: 75px;'), '', 'date');
		//$pForm->AddSelect('start_sending_day',$aConfig['lang'][$this->sModule]['start_sending_day'], array(), $this->getDays(),$aData['start_sending_day'],'',true);
		// godzina rozpoczecia wysylki
		//$pForm->AddSelect('start_sending_hour', $aConfig['lang'][$this->sModule]['start_sending_hour'], array(), $this->getHours(), $aData['start_sending_hour']);
		
/**********************************************/		
		}
		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
 	/** Metoda pobiera liste grup
	 *
	 * @param	integer	$iPId	- Id strony
	 * @param	string	$sLabel	- etykieta pierwszej opcji
	 * @return	array ref	- lista grup
	 */
	function getGroups($iPId, $sLabel) {
		global $aConfig;

		// pobranie wszystkich akapitów dla danej strony
		$sSql = "SELECT id AS value, name AS label
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories_groups
						 WHERE page_id = ".$iPId."
						 ORDER BY order_by";
		return @array_merge(
			array(array('value' => '', 'label' => $sLabel)),
			Common::GetAll($sSql)
		);
	} // end of getGroups() method
	
	
	/**
	 * Metoda pobiera liste kategorii do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id kategorii
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, name
				 		 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
					 	 WHERE id IN (".implode(',', $_POST['delete']).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda usuwa kategorie newslettera
	 * 
	 * @param	integer	$iId	- Id kategorii do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_categories
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method
	
	
	/**
	 * Metoda pobiera liste rekordow
	 *
	 * @param	integer	$iPId	- Id strony
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($iPId, $iGId) {
		global $aConfig;

		// pobranie wszystkich kategorii dla danej strony i grupy
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
						 WHERE page_id = ".$iPId." AND
						 	   group_id = ".$iGId."
						 ORDER BY order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method


	/**
	 * Metooda pobiera nazwe uzytkownika ktory utworzyl kategorie o podanym Id
	 * 
	 * @param	integer	$iId	 - Id kategorii
	 * @return	string
	 */
	function getCreatorName($iId) {
		global $aConfig;
		
		$sSql = "SELECT created_by
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getCreatorName() method

		/**
	 * Metoda tworzy tablice z godzinami dla pola SELECT
	 *
	 * @return	array ref	- lista godzin
	 */
	function &getHours() {
		global $aConfig;
		$aItems = array(array('value' => '0',
													'label' => $aConfig['lang']['common']['choose']));
		for ($i = 0; $i < 24; $i++) {
			$aItems[] = array('value' => ($i <= 9 ? '0' : '').$i.':00',
												'label' => ($i <= 9 ? '0' : '').$i.':00');
			$aItems[] = array('value' => ($i <= 9 ? '0' : '').$i.':30',
												'label' => ($i <= 9 ? '0' : '').$i.':30');
		}
		return $aItems;
	} // end of etHours()
	
	function &getDays(){
		global $aConfig;
		$aDays=array(array('value' => '',
													'label' => $aConfig['lang']['common']['choose']));
		foreach($aConfig['lang']['days'] as $iKey => $sDay){
			$aDays[]=array('value'=>$iKey,'label'=>$sDay);
		}
		return $aDays;
	}
	
	/**
	 * 
	 * @param	integer	$iMId	- Id menu oferty produktowej
	 */
	function getProductsCategories($aMId, $sLabel,$bAllowOther=false) {
		global $aConfig;
		$aItems = array();
		if (empty($aMId)) return $aItems;
		
		$sSql = "SELECT IFNULL(parent_id, 0), id, name, mtype, module_id,
										IFNULL(link_to_id, 0) AS link_to_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE menu_id IN (".implode(',',$aMId).") AND
						 			 language_id = ".$this->iLangId."
						 			 ORDER BY parent_id, order_by";
		$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
		// utworzenie drzewa menu
		
		$aMenuItems =& getMenuTree($aItems);

		if($bAllowOther){
			$iAllowedModule=0;
		} else {
			$iAllowedModule=getModuleId('m_oferta_produktowa');
		}
		
		// strona do ktorej linkuje
		if($sLabel != ''){
		$aMenuTree2 = array_merge(
			array(array('value' => '', 'label' => $sLabel)),
			getComboMenuTree($aMenuItems, 0, 0, 0, $iAllowedModule)
		);
		}
		else {
			$aMenuTree2=getComboMenuTree($aMenuItems, 0, 0, 0, $iAllowedModule);
		}
		return $aMenuTree2;
	} // end of getProductsCategories method
} // end of Module Class
?>