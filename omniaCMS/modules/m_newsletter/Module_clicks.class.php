<?php
/**
 * Klasa Module do obslugi modulu 'Newsletter - klikniecia w linki'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;
		$iPPId = 0;


		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
		if (isset($_GET['ppid'])) {
			$iPPId = $_GET['ppid'];
		}
		
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
		
		switch ($sDo) {
			case 'get_report': $this->getLinkReport($iPId, $iPPId, $iId); break;
			case 'get_total_report': $this->getTotalReport($iPId, $iPPId); break;
			default: $this->Show($pSmarty, $iPId, $iPPId); break;
		}
	} // end of Module() method

	
	/**
	 * Metoda wyswietla liste linkow maila wraz z informacjami
	 * o kliknieciach w nie
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id newslettera
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header' => ($iPId>0?sprintf($aConfig['lang'][$this->sModule]['list'], getPagePath($iPId, $this->iLangId, true), $this->getNewsletterSubject($iId)):sprintf($aConfig['lang'][$this->sModule]['list2'], $this->getNewsletterSubject($iId))),
			'refresh' => true,
			'search' => true,
			'checkboxes' => false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'db_field'	=> 'link',
				'content'	=> $aConfig['lang'][$this->sModule]['list_link'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'clicked',
				'content'	=> $aConfig['lang'][$this->sModule]['list_clicks'],
				'sortable'	=> true,
				'width'	=> '75'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '125'
			)
		);
		
		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."newsletters_links
						 WHERE newsletter_id = ".$iId;
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."newsletters_links
							 WHERE newsletter_id = ".$iId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('newsletters_links', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich rekordow
			$sSql = "SELECT id, link, clicked
							 FROM ".$aConfig['tabls']['prefix']."newsletters_links
							 WHERE newsletter_id = ".$iId."
							 ORDER BY ".(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'clicked').
							 (isset($_GET['order']) && !empty($_GET['order']) ? " ".$_GET['order'] : '  DESC').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			foreach ($aRecords as $iKey => $aRecord) {
				if ($aRecord['clicked'] == '0') {
					$aRecords[$iKey]['disabled'][] = 'get_report';
				}
				// przycicecie linku
				$aRecords[$iKey]['link'] = substr($aRecord['link'],0, 100);
			}

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'action' => array (
					'actions'	=> array ('get_report'),
					'params' => array (
												'get_report' => array('pid' => $iPId, 'ppid'=>$iId, 'id'=>'{id}')
											),
					'icon' => array(
												'get_report' => 'download'
											)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		$aRecordsFooter = array(
			array('go_back'),
			array('get_total_report')
		);
		if($iPId>0){
			$aRecordsFooterParams = array(
				'go_back' => array(array('action'=>'newsletters'), array('id', 'ppid')),
				'get_total_report' => array(2 => 'download')
			);
		} else {
			$aRecordsFooterParams = array(
				'go_back' => array(array('action'=>'auto_newsletters'), array()),
				'get_total_report' => array(2 => 'download')
			);
		}
		
		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function
	
	
	/**
	 * Metoda zwraca raport klikniec dla linku o Id $iId w postaci
	 * pliku CSV (\t) o rozszerzeniu .xls
	 * 
	 * @param	integer $iPId	- Id strony
	 * @param	integer $iNId	- Id newslettera
	 * @param	integer $iId	- Id linku
	 * @return void
	 */
	function getLinkReport($iPId, $iNId, $iId) {
		
		$aItems =& $this->getLinkClickers($iId);
		
		$_GET['hideHeader'] = '1';
		
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="klikniecia_'.$iNId.'_'.$iId.'.xls"');
		header("Pragma: no-cache");
		header("Expires: 0");
		// zapis nazwy strony newslettera
		echo "\t\t\t\t\t".getPagePath($iPId, $this->iLangId, true)."\r\n";
		// zapis nazwy newslettera
		echo "\t\t\t\t\t".$this->getNewsletterSubject($iNId)."\r\n";
		// zapis linku
		echo "\t\t\t\t\t".$this->getLink($iId)."\r\n\r\n";
		// wyslij naglowek w pierwszym wierszu
		echo "LP\tDATA KLIKNIECIA\tADRES E-MAIL\tID KONTA\r\n";
		// zapis rekordow
		foreach ($aItems as $iKey => $aItem) {
			echo ($iKey + 1)."\t".$aItem['date_click']."\t\"".$aItem['source_email']."\"\t".$aItem['source_konto_id']."\r\n";
		}
	} // end of getLinkReport() method
	
	
	/**
	 * Metoda pobiera i zwraca liste maili uzytkownikow ktorzy klikneli
	 * w link newslettera
	 * 
	 * @param	integer $iId	- Id linku
	 * @return array ref
	 */
	function &getLinkClickers($iId) {
		global $aConfig;
		
		$sSql = "SELECT DATE_FORMAT(click_date, '%Y-%m-%d %H:%i:%s') AS date_click, source_email, source_konto_id
						 FROM ".$aConfig['tabls']['prefix']."newsletters_clicked_links
						 WHERE link_id = ".$iId."
						 ORDER BY id DESC";
		return Common::GetAll($sSql);
	} // end of getLinkClickers() method
	
	
	/**
	 * Metoda pobiera link o podanym Id
	 * 
	 * @param	integer	$iId	- Id linku
	 * @return	string
	 */
	function getLink($iId) {
		global $aConfig;
		
		$sSql = "SELECT link
						 FROM ".$aConfig['tabls']['prefix']."newsletters_links
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getLink() method
	
	
	/**
	 * Metoda zwraca raport liczby klikniec dla wszystkich linkow
	 * newslettera o Id $iNId w postaci pliku CSV (\t) o rozszerzeniu .xls
	 * 
	 * @param	integer $iPId	- Id strony
	 * @param	integer $iNId	- Id newslettera
	 * @return void
	 */
	function getTotalReport($iPId, $iNId) {
		
		$aItems =& $this->getLinks($iNId);
		
		$_GET['hideHeader'] = '1';
		
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="linki_'.$iNId.'.xls"');
		header("Pragma: no-cache");
		header("Expires: 0");
		// zapis nazwy strony newslettera
		echo "\t\t\t".getPagePath($iPId, $this->iLangId, true)."\r\n";
		// zapis nazwy newslettera
		echo "\t\t\t".$this->getNewsletterSubject($iNId)."\r\n\r\n";
		// wyslij naglowek w pierwszym wierszu
		echo "LP\tLINK\tKLIKNIECIA\r\n";
		// zapis rekordow
		foreach ($aItems as $iKey => $aItem) {
			echo ($iKey + 1)."\t".$aItem['link']."\t".$aItem['clicked']."\r\n";
		}
	} // end of getTotalReport() method
	
	
	function &getLinks($iNId) {
		global $aConfig;
		
		// pobranie wszystkich rekordow
		$sSql = "SELECT link, clicked
						 FROM ".$aConfig['tabls']['prefix']."newsletters_links
						 WHERE newsletter_id = ".$iNId."
						 ORDER BY clicked  DESC";
		return Common::GetAll($sSql);
	} // end of getLinks() method
	
	
	/**
	 * Metoda pobiera temat newslettera
	 *
	 * @param	integer	$iId	- Id newslettera
	 * @return	string	- tytul newslettera
	 */
	function getNewsletterSubject($iId) {
		global $aConfig;

		// pobranie nazwy kategorii
		$sSql = "SELECT subject
						 FROM ".$aConfig['tabls']['prefix']."newsletters
						 WHERE id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getNewsletterSubject() method


	
} // end of Module Class
?>