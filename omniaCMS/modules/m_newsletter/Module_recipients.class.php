<?php
/**
 * Klasa Module do obslugi modulu 'Newsletter - subskrybecni'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->sModule = $_GET['module'];

		$sDo = '';
		$iId = 0;
		$iPId = 0;
		$iCId = 0;

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}
		if (isset($_GET['pid'])) {
			$iPId = $_GET['pid'];
		}
		if (isset($_GET['cid'])) {
			$iCId = $_GET['cid'];
		}
	// wydzielenie uprawnien
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		
	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iPId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iPId); break;
			case 'add': $this->Add($pSmarty, $iPId, $iId); break;
			case 'update': $this->Update($pSmarty, $iPId, $iId); break;
			case 'edit': $this->Edit($pSmarty, $iPId, $iId); break;
			case 'activate': $this->Activate($pSmarty, $iPId, $iId); break;
			case 'export_recipients': $this->getCategorySubscribers($iPId, $iCId); break;
			default: $this->Show($pSmarty, $iPId); break;
		}
	} // end of Module() method


	/**
	 * Metoda wyswietla liste subskrybentow newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Show(&$pSmarty, $iPId) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> sprintf($aConfig['lang'][$this->sModule]['list'],
													 getPagePath($iPId, $this->iLangId, true)),
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable',
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'email',
				'content'	=> $aConfig['lang'][$this->sModule]['list_email'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'added',
				'content'	=> $aConfig['lang'][$this->sModule]['list_added'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'confirmed',
				'content'	=> $aConfig['lang'][$this->sModule]['list_confirmed'],
				'sortable'	=> true,
				'width'	=> '105'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '85'
			)
		);

		// pobranie liczby wszystkich rekordow
		$sSql = "SELECT COUNT(DISTINCT A.id)
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients AS A
						 JOIN ".$aConfig['tabls']['prefix']."newsletters_rec2cat AS B
									ON A.id = B.recipient_id
						 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories AS C
									ON B.category_id = C.id
						 WHERE A.test = '0' AND C.page_id = ".$iPId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND A.email LIKE \'%'.$_POST['search'].'%\'' : '').
									 (isset($_POST['f_category_id']) && !empty($_POST['f_category_id']) ? ' AND C.id = '.$_POST['f_category_id'] : '').
									 (isset($_POST['f_confirmed']) && $_POST['f_confirmed'] != '-1' ? ' AND A.confirmed = '.$_POST['f_confirmed'] : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(DISTINCT A.id)
						 	 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients AS A
						 	 JOIN ".$aConfig['tabls']['prefix']."newsletters_rec2cat AS B
										ON A.id = B.recipient_id
						 	 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories AS C
										ON B.category_id = C.id
						 	 WHERE A.test = '0' AND C.page_id = ".$iPId;
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('recipients', $aHeader, $aAttribs, $iRowCount);
		$pView->AddRecordsHeader($aRecordsHeader);

		// filtr przynaleznosci do kategorii
		// pobranie kategorii
		$sSql = "SELECT id AS value, name AS label
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
						 WHERE page_id = ".$iPId;
		$aCategories =& Common::GetAll($sSql);
		$aCategories = array_merge(
			array(array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['all_categories'])),
			$aCategories
		);
		$pView->AddFilter('f_category_id', $aConfig['lang'][$this->sModule]['category_id'], $aCategories, $_POST['f_category_id']);

		// potwierdzony / niepotwierdzony
		$aConfirmed = array(
			array('value' => '-1', 'label' => $aConfig['lang'][$this->sModule]['all']),
			array('value' => '1', 'label' => $aConfig['lang']['common']['yes']),
			array('value' => '0', 'label' => $aConfig['lang']['common']['no'])
		);
		$pView->AddFilter('f_confirmed', $aConfig['lang'][$this->sModule]['confirmed'], $aConfirmed, $_POST['f_confirmed']);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich subskrybentow
			$sSql = "SELECT DISTINCT A.id, A.email, A.added, IF (A.confirmed = '0', '".$aConfig['lang']['common']['no']."', '".$aConfig['lang']['common']['yes']."') AS confirmed
						 	 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients AS A
						 	 JOIN ".$aConfig['tabls']['prefix']."newsletters_rec2cat AS B
										ON A.id = B.recipient_id
						 	 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories AS C
										ON B.category_id = C.id
						 	 WHERE A.test = '0' AND C.page_id = ".$iPId.
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND A.email LIKE \'%'.$_POST['search'].'%\'' : '').
										 (isset($_POST['f_category_id']) && !empty($_POST['f_category_id']) ? ' AND C.id = '.$_POST['f_category_id'] : '').
										 (isset($_POST['f_confirmed']) && $_POST['f_confirmed'] != '-1' ? ' AND A.confirmed = '.$_POST['f_confirmed'] : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'A.id DESC').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);
			
			

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'email' => array (
					'link'	=> phpSelf(array('pid' => $iPId, 'id'=>'{id}', 'do'=>'edit'))
				),
				'action' => array (
					'actions'	=> array ('activate', 'edit', 'delete'),
					'params' => array (
						'activate'	=> array('pid' => $iPId, 'id'=>'{id}'),
						'edit'	=> array('pid' => $iPId, 'id'=>'{id}'),
						'delete'	=> array('pid' => $iPId, 'id'=>'{id}')
					)
				)
			);

			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku
		if ($iRowCount > 0 && isset($_POST['f_category_id']) && !empty($_POST['f_category_id'])) {
			$aRecordsFooter = array(
				array('check_all', 'delete_all', 'go_back'),
				array('export_recipients', 'add')
			);
			$aRecordsFooterParams = array(
				'go_back' => array(1 => array('action', 'id', 'pid')),
				'export_recipients' => array(0 => array('cid' => $_POST['f_category_id']), 2 => 'download')
			);
		}
		else {
			$aRecordsFooter = array(
				array('check_all', 'delete_all', 'go_back'),
				array(1 => 'add')
			);
			$aRecordsFooterParams = array(
				'go_back' => array(1 => array('action', 'id', 'pid'))
			);
		}

		$pView->AddRecordsFooter($aRecordsFooter, $aRecordsFooterParams);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function


	/**
	 * Metoda usuwa wybranych subskrybentow
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony newslettera
	 * @param	integer	$iId	- Id usuwanego subskrybenta
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;
		$aTmp = array();
		$sDel = '';
		$sFailedToDel = '';
		$iDeleted = 0;
		
		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			$aTmp[$iI++] = $sKey;
		}
		$_POST['delete'] =& $aTmp;

		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();
			$aItems =& $this->getItemsToDelete($_POST['delete']);
			foreach ($aItems as $aItem) {
				// usuwanie
				if (($iRecords = $this->deleteItem($aItem['id'])) === false) {
					$bIsErr = true;
					$sFailedToDel = $aItem['email'];
					break;
				}
				elseif ($iRecords == 1) {
					// usunieto
					$iDeleted++;
					$sDel .= '"'.$aItem['email'].'", ';
				}
			}
			$sDel = substr($sDel, 0, -2);
			
			if (!$bIsErr) {
				// usunieto
				Common::CommitTransaction();
				if ($iDeleted > 0) {
					$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.($iDeleted > 1 ? '1' : '0')], $sDel, getPagePath($iPId, $this->iLangId, true));
					$this->sMsg = GetMessage($sMsg, false);
					// dodanie informacji do logow
					AddLog($sMsg, false);
				}
			}
			else {
				// blad
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'], $sFailedToDel, getPagePath($iPId, $this->iLangId, true));
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty, $iPId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowa kategorie strony newslettera
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @return	void
	 */
	function Insert(&$pSmarty, $iPId) {
		global $aConfig;
		set_time_limit(1800);
		$bIsErr = false;
		$iFound = 0;
		$iAdded = 0;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Add($pSmarty, $iPId);
			return;
		}
		// rozpoczecie transkacji
		Common::BeginTransaction();
		// otwarcie pliku i wyszukiwanie adresow e-mail w jego zawartosci
		if (@$rFile = fopen($_FILES['subscribers']['tmp_name'], 'r')) {
			$aDelims = array(',' => ',', ';' => ';', '\t' => "\t", '\n' => "\n");
			while(!feof($rFile)) {
				$sEmail = $this->getDelimitedFileContents($rFile, $aDelims[Common::strip_slashes($_POST['separator'])], 512);
				// usuwanie ' oraz "
				$sEmail = str_replace("'", '', str_replace('"', '', $sEmail));
				if ($sEmail !== false && preg_match($aConfig['class']['form']['email']['pcre'], ($sEmail = preg_replace('/\s+/', '', $sEmail)))) {
					$iFound++;
					if (!$this->recipientExists($sEmail)) {
						// adres email nie istnieje jeszcze w bazie - insert
						$aValues = array(
							'page_id' => $iPId,
							'email' => $sEmail,
							'confirmed' => '1',
							'added' => 'NOW()'
						);
						if (($iRId = Common::Insert($aConfig['tabls']['prefix']."newsletters_recipients", $aValues)) === false) {
							$bIsErr = true;
						}
						else {
							// dodawanie kategorii
							foreach ($_POST['categories'] as $iCId) {
								if (!$bIsErr) {
									$aValues = array(
										'category_id' => $iCId,
										'recipient_id' => $iRId
									);
									if (Common::Insert($aConfig['tabls']['prefix']."newsletters_rec2cat",
																		 $aValues,
																		 '',
																		 false) === false) {
										$bIsErr = true;
									}
								}
							}
							$iAdded++;
						}
					}
					elseif (($iRId = $this->getRecipientId($sEmail)) > 0) {
						// dodanie tylko tych kategorii w ktorych jeszcze nie jest
						$bAdded = false;
						foreach ($_POST['categories'] as $iCId) {
							if (!$bIsErr && !$this->isAssignedToCategory($iRId, $iCId)) {
								$aValues = array(
									'category_id' => $iCId,
									'recipient_id' => $iRId
								);
								if (Common::Insert($aConfig['tabls']['prefix']."newsletters_rec2cat",
																	 $aValues,
																	 '',
																	 false) === false) {
									$bIsErr = true;
								}
								else $bAdded = true;
							}
						}
						if ($bAdded) $iAdded++;
					}
				}
			}
			fclose($rFile);
			if ($iFound == 0) {
				// nie odnaleziono zadnego adresu e-mail - byc moze wybrano nieprawidlowy
				// separator
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['file_err'], true);
				$this->Add($pSmarty, $iPId);
				return;
			}
		}
		else {
			$bIsErr = true;
		}
		//echo memory_get_peak_usage()."<br />";
		//echo memory_get_peak_usage(true);
		$sName = getPagePath($iPId, $this->iLangId, true);
		if (!$bIsErr) {
			// dodano adresy
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'], $iAdded, $sName);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();

			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// nie dodano adresow
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'], $sName);
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();

			AddLog($sMsg);
			$this->Add($pSmarty, $iPId);
		}
	} // end of Insert() funciton
	
	
	/**
	 * Metoda zwraca pojedynczy ciag znakow z pliku $rFile z zawartoscia
	 * rozdzielona przez separator $sDelim
	 * Metoda znaleziona w komentarzach do funkcji fscanf
	 * na: http://pl.php.net/manual/pl/function.fscanf.php
	 * 
	 * @param	resource	$rFile	- obiekt (uchwyt) pliku
	 * @param	string	$sDelim	- separator
	 * @param	integer	$iBuffer	- liczba odczytywanych jednorazowo bajtow
	 * @return string
	 */
	function getDelimitedFileContents(&$rFile, $sDelim, $iBuffer=1024) {	
	  $sRecord = '';
	
	  while(!feof($rFile)) {
	    $iPos = strpos($sRecord, $sDelim);
	    if ($iPos === false) {
	        $sRecord .= fread($rFile, $iBuffer);
	    }
	    else {
	        fseek($rFile, 0-strlen($sRecord)+$iPos+strlen($sDelim), SEEK_CUR);
	        return substr($sRecord, 0, $iPos);
	    }
	  }
	
	  // Last read got some more data before hitting EOF?
	  if ($sRecord != '') {
	    if (($iPos = strpos($sRecord, $sDelim)) !== false) {
	      fseek($rFile, 0-strlen($sRecord)+$iPos+strlen($sDelim), SEEK_CUR);
	      return substr($sRecord, 0, $iPos);
	    }
	    else {
	      return $sRecord;
	    }
	  }
	  else {
	    return false;
	  }
	} // end of getDelimitedFileContents() method
	
	
	/**
	 * Metoda wprowadza zmiany w przypisaniu subskrybenta newslettera
	 * do kategorii
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id subskrybenta
	 * @return	void
	 */
	function Update(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Show($pSmarty, $iPId);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Edit($pSmarty, $iPId, $iId);
			return;
		}

		// rozpoczecie transakcji
		Common::BeginTransaction();

		// usuniecie poprzednich powiazan
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat WHERE recipient_id = ".$iId;
		$bIsErr = Common::Query($sSql) === false;
		if (!$bIsErr) {
			// dodawanie przypisan
			foreach ($_POST['categories'] as $iCId) {
				if (!$bIsErr) {
					$aValues = array(
						'category_id' => $iCId,
						'recipient_id' => $iId
					);
					if (Common::Insert($aConfig['tabls']['prefix']."newsletters_rec2cat",
														 $aValues,
														 '',
														 false) === false) {
						$bIsErr = true;
						break;
					}
				}
			}
		}
		if (!$bIsErr) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'], $this->getSubscriber($iPId, $iId), getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			Common::CommitTransaction();
			AddLog($sMsg, false);
			$this->Show($pSmarty, $iPId);
		}
		else {
			// blad
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'], $this->getSubscriber($iPId, $iId), getPagePath($iPId, $this->iLangId, true));
			$this->sMsg = GetMessage($sMsg);
			Common::RollbackTransaction();
			AddLog($sMsg);
			$this->Edit($pSmarty, $iPId, $iId);
		}
	} // end of Update() funciton


	/**
	 * Metoda zmienia stan potwierdzenia wybranego subskrybenta
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iPId	- ID strony
	 * @param		integer	$iId	- ID subskrybenta
	 * @return 	void
	 */
	function Activate(&$pSmarty, $iPId, $iId) {
		global $aConfig;
		$bIsErr = false;

		// pobranie stanu potwierdzenia
		$sSql = "SELECT IF (confirmed = '0', '1', '0') AS confirmed
				 		 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
					 	 WHERE id = ".$iId." AND
					 	 			 page_id = ".$iPId;
		if (($iConfirmed =& Common::GetOne($sSql)) === false) {
			$bIsErr = true;
		}

		if (!$bIsErr) {
			// zmiana stanu potwierdzenia
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."newsletters_recipients
							 SET confirmed = IF (confirmed = '0', '1', '0')
							 WHERE page_id = ".$iPId." AND
										 id = ".$iId;
			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
		}
		if ($bIsErr) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['confirm_err'],
											$this->getSubscriber($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true),
											$aConfig['lang'][$this->sModule]['confirmed_'.$iConfirmed]);
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
		}
		else {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['confirm_ok'],
											$this->getSubscriber($iPId, $iId),
											getPagePath($iPId, $this->iLangId, true),
											$aConfig['lang'][$this->sModule]['confirmed_'.$iConfirmed]);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
		}
		$this->Show($pSmarty, $iPId);
	} // end of Activate() funciton


	/**
	 * Metoda tworzy formularz dodawania subskrybentow
	 *
	 * @param		object	$pSmarty
	 * @param		string	$iPId	- ID strony
	 */
	function Add(&$pSmarty, $iPId) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		// okreslenie nazwy strony dla ktorej dodawani sa subskrybenci
		$sName = getPagePath($iPId, $this->iLangId, true);

		$sHeader = $aConfig['lang'][$this->sModule]['header_0'];

		$pForm = new FormTable('categories', $sHeader, array('action'=>phpSelf(), 'enctype'=>'multipart/form-data'), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddRow($aConfig['lang'][$this->sModule]['for'], $sName, '', array(), array('class'=>'redText'));
		$pForm->AddHidden('do', 'insert');

		// nazwa
		$pForm->AddFile('subscribers', $aConfig['lang'][$this->sModule]['file'],  array('style'=>'width: 350px;'));

		// separator adresow w pliku
		$aSeparators = array(
			array('value' => '0', 'label' => $aConfig['lang']['common']['choose']),
			array('value' => ';', 'label' => $aConfig['lang'][$this->sModule]['semicolon']),
			array('value' => ',', 'label' => $aConfig['lang'][$this->sModule]['comma']),
			array('value' => '\t', 'label' => $aConfig['lang'][$this->sModule]['tab']),
			array('value' => '\n', 'label' => $aConfig['lang'][$this->sModule]['new_line']),
		);
		$pForm->AddSelect('separator', $aConfig['lang'][$this->sModule]['separator'], array(), $aSeparators, stripslashes($aData['separator']));

		// kategorie do ktorych maja zostac przypisani subskrybenci
		$aCategories =& $this->getCategories($iPId);

		$pForm->AddCheckboxSet('categories', $aConfig['lang'][$this->sModule]['categories'], $aCategories, $aData['categories'], '', true, false);

		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_0']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Add() function
	
	
	/**
	 * Metoda tworzy formularz edycji przypisania subskrybenta do kategorii
	 *
	 * @param		object	$pSmarty
	 * @param		string	$iPId	- ID strony
	 * @param		string	$iId	- ID subskrybenta
	 */
	function Edit(&$pSmarty, $iPId, $iId) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		// okreslenie nazwy strony newsletera
		$sName = getPagePath($iPId, $this->iLangId, true);
		// adres email subskrybenta
		$sEmail = $this->getSubscriber($iPId, $iId);
		// kategorie subskrybenta
		$aData['categories'] =& $this->getSubscriberCategories($iPId, $iId);
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_1'], $sEmail, $sName);
		$pForm = new FormTable('subscriber', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');

		// adres subskrybenta
		$pForm->AddText('email', $aConfig['lang'][$this->sModule]['subscriber_email'], $sEmail,  array('style'=>'width: 350px;', 'readonly' => 'readonly'), '', 'text', false);

		// kategorie do ktorych przypisany jest subskrybent
		$aCategories =& $this->getCategories($iPId);

		$pForm->AddCheckboxSet('categories', $aConfig['lang'][$this->sModule]['subscriber_categories'], $aCategories, $aData['categories'], '', true, false);

		// przyciski
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of Edit() function


	/**
	 * Metoda sprawdza czy podany adres email istnieje juz w bazie
	 * subskrybentow czy tez nie
	 *
	 * @param	string	$sEmail	- sprawdzany email
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function recipientExists($sEmail) {
		global $aConfig;

		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE email = '".$sEmail."'";
		return ((double) Common::GetOne($sSql)) > 0;
	} // end of recipientExists() method
	
	
	/**
	 * Metoda pobiera Id odbiorcy o podanym adresie email
	 *
	 * @param	string	$sEmail	- sprawdzany email
	 * @return	integer
	 */
	function getRecipientId($sEmail) {
		global $aConfig;

		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE email = '".$sEmail."'";
		return (double) Common::GetOne($sSql);
	} // end of getRecipientId() method
	
	
	/**
	 * Metoda sprawdza czy odbiorca o podanym Id jest juz przypisany
	 * do kategorii o podanym Id
	 *
	 * @param	integer	$iRId	- Id odiorcy
	 * @param	integer	$iCId	- Id kategorii
	 * @return	bool
	 */
	function isAssignedToCategory($iRId, $iCId) {
		global $aConfig;

		$sSql = "SELECT recipient_id
						 FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat
						 WHERE recipient_id = ".$iRId." AND
						 			 category_id = ".$iCId;
		return ((double) Common::GetOne($sSql)) > 0;
	} // end of isAssignedToCategory() method
	
	
	/**
	 * Metoda pobiera i zwraca liste kategorii newslettera
	 * 
	 * @param	integer	$iPId	- Id strony newslettera
	 * @return	array ref
	 */
	function &getCategories($iPId) {
		global $aConfig;
		
//		$sSql = "SELECT id AS value, name AS label
//						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
//						 WHERE page_id = ".$iPId."
//						 ORDER BY name";
//		return Common::GetAll($sSql);
		
		$sSql = "SELECT B.name, A.id AS value, A.name AS label
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories A
						 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories_groups B
						 ON B.id=A.group_id
						 WHERE A.page_id = ".$iPId."
						 ORDER BY A.name";
		$aItems = Common::GetAssoc($sSql,false,array(),DB_FETCHMODE_ASSOC,true);
		$aCats = array();
		foreach($aItems as $sGroup=>$aList) {
			$aCats[]=array('label'=>$sGroup,'value'=>'GROUPHEADER');
			$aCats=array_merge($aCats,$aList);
		}
		return $aCats;
	} // end of getCategories() method


	/**
	 * Metoda pbiera i zwraca adres email subskrybenta
	 *
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id subskrybenta
	 * @return	string	- email subskrybenta
	 */
	function getSubscriber($iPId, $iId) {
		global $aConfig;

		$sSql = "SELECT email
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE page_id = ".$iPId." AND
						 			 id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getSubscriber() method
	
	
	/**
	 * Metoda pbiera i zwraca Id kategorii subskrybenta
	 *
	 * @param	integer	$iPId	- Id strony
	 * @param	integer	$iId	- Id subskrybenta
	 * @return	array ref
	 */
	function &getSubscriberCategories($iPId, $iId) {
		global $aConfig;

		$sSql = "SELECT A.category_id
						 FROM ".$aConfig['tabls']['prefix']."newsletters_rec2cat A
						 JOIN ".$aConfig['tabls']['prefix']."newsletters_categories B
						 ON B.id = A.category_id
						 WHERE B.page_id = ".$iPId." AND
						 			 A.recipient_id = ".$iId;
		return Common::GetCol($sSql);
	} // end of getSubscriberCategories() method
	
	
	/**
	 * Metoda pobiera liste subskrybentow do usuniecia na podstawie przekazanej
	 * tablicy z ich Id
	 * 
	 * @param	array ref	$aIds	- Id subskrybentow
	 * @return	array ref
	 */
	function &getItemsToDelete(&$aIds) {
		global $aConfig;
		$sSql = "SELECT id, email
				 		 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
					 	 WHERE id IN (".implode(',', $_POST['delete']).")";
		return Common::GetAll($sSql);
	} // end of getItemsToDelete() method
	
	
	/**
	 * Metoda zwraca liste subskrybentow kategorii newslettera o Id $iCId
	 * plik XLS o rozszerzeniu .xls
	 * 
	 * @param	integer $iPId	- Id strony
	 * @param	integer $iCId	- Id kategorii newslettera
	 * @return void
	 */
	function getCategorySubscribers($iPId, $iCId) {
		global $aConfig;
		
		$_GET['hideHeader'] = '1';
		
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="subskrybenci_'.$this->getCategoryName($iPId, $iCId).'_'.date('Ymd').'.xls"');
		header("Pragma: no-cache");
		header("Expires: 0");
		echo $aConfig['lang'][$this->sModule]['list_email']."\t".$aConfig['lang'][$this->sModule]['confirmed2']."\n\n";
		
		$sSql = "SELECT A.email, A.confirmed
						 FROM ".$aConfig['tabls']['prefix']."newsletters_recipients A
						 JOIN ".$aConfig['tabls']['prefix']."newsletters_rec2cat B
						 ON A.id = B.recipient_id
						 WHERE A.test = '0' AND A.page_id = ".$iPId." AND
						 			 B.category_id = ".$iCId."
						 ORDER BY A.confirmed DESC, A.id DESC";
		if (($oRes =& Common::PlainQuery($sSql)) !== false) {
			while ($aRow =& $oRes->fetchRow()) {
		    echo $aRow[0]."\t".$aConfig['lang']['common'][$aRow[1] == '1' ? 'yes' : 'no']."\n";
			}
		}
	} // end of getCategorySubscribers() method
	
	
	/**
	 * Metoda zwraca nazwe kategorii bez polskich liter i bialych znakow
	 * 
	 * @param	integer $iPId	- Id strony
	 * @param	integer $iCId	- Id kategorii newslettera
	 * @return string
	 */
	function getCategoryName($iPId, $iId) {
		$sSql = "SELECT name
						 FROM ".$aConfig['tabls']['prefix']."newsletters_categories
						 WHERE id = ".$iId." AND
						 			 page_id = ".$iPId;
		return preg_replace('/\s+/', '_', changeCharcodes(Common::GetOne($sSql), 'utf8', 'no'));
	} // end of getCategoryName() method
	
	
	/**
	 * Metoda usuwa odbiorce newslettera
	 * 
	 * @param	integer	$iId	- Id odbiorcy do usuniecia
	 * @return	mixed	
	 */
	function deleteItem($iId) {
		global $aConfig;
		$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."newsletters_recipients
						 WHERE id = ".$iId;
		return Common::Query($sSql);
	} // end of deleteItem() method


	
} // end of Module Class
?>