<?php
/**
 * Klasa Module do obslugi modulu 'Newsletter' - Opcje automatycznego newslettera
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Module {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID strony zamowien
	var $iPageId;

	// ID wersji jezykowej
	var $iLangId;
  
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;
		
		$this->sErrMsg = '';
		$this->iPageId = getModulePageId('m_zamowienia');
		$this->sModule = $_GET['module'];
		$this->iLangId = $_SESSION['lang']['id'];
		
		$sDo = '';		
		
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		
		// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu i akcji
		if ($_SESSION['user']['type'] === 0) {
			// brak uprawnien do modulu, menu, strony
			showPrivsAlert($pSmarty);
			return;
		}
		
		switch ($sDo) {
			case 'add_photo': $this->Update($pSmarty, true); break;
			case 'update': $this->Update($pSmarty); break;
			default: $this->Edit($pSmarty); break;
		}
	} // end of Module() method
	
	
	/**
	 * Metoda aktualizuje w bazie danych krekord
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iId	- Id aktualizowanego rekordu
	 * @return	void
	 */
	function Update(&$pSmarty, $bAddPhoto=false) {
		global $aConfig;
		$bIsErr = false;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Edit($pSmarty);
			return; 
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Edit($pSmarty, $bAddPhoto);
			return;
		}
		Common::BeginTransaction();
		$aValues = array(
			'auto_description' => $_POST['auto_description']
		);
		// update
		if (Common::Update($aConfig['tabls']['prefix']."newsletters_settings",
												$aValues) === false) {
			$bIsErr = true;
		}
		if ($bIsErr == false) {
			$this->ProceedImages($pSmarty);
		}
		if (!$bIsErr) {
			Common::CommitTransaction();
			// rekord zostal zaktualizowany
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_ok'], false);
			// dodanie informacji do logow
			AddLog($aConfig['lang'][$this->sModule]['edit_ok'], false);
		}
		else {
			Common::RollbackTransaction();
			// rekord nie zostal zmieniony, 
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['edit_err']);
			AddLog($aConfig['lang'][$this->sModule]['edit_err']);
		}
		$this->Edit($pSmarty, $bAddPhoto);
	} // end of Update() funciton

	
	/**
	 * Metoda tworzy formularz edycji danych sklepu
	 *
	 * @param		object	$pSmarty
	 */
	function Edit(&$pSmarty, $bAddPhoto=false) {
		global $aConfig;
				
		$aData = array();
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		// pobranie z bazy danych edytowanego rekordu
		$aData =& $this->getData();
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		$sHeader = $aConfig['lang'][$this->sModule]['header'];
		
		$pForm = new FormTable('seller_data', $sHeader, array('action'=>phpSelf(), 'enctype'=>'multipart/form-data'), array('col_width'=>175), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update');

		// pole opisowe
		$pForm->AddWYSIWYG('auto_description', $aConfig['lang'][$this->sModule]['description'], $aData['auto_description'],array(), '', false);
		// zdjecia
		// bez zmiany rozmiaru
		
		// pobranie zdjec
		$sSql = "SELECT id, directory, photo, photo AS name, description, order_by
						 FROM " . $aConfig['tabls']['prefix'] . "newsletters_images
					 	 ORDER BY order_by, id";
		$aImgs = & Common::GetAll($sSql);
		
				$iImages = 0;
		if (!empty($_POST['desc_image'])) {
    	$iImages = count($_POST['desc_image']);
		}
		elseif (!empty($aImgs)) {
    	$iImages = count($aImgs) + 1;
		}
		else {
			$iImages = $aConfig['default']['photos_no'];
		}
		if ($iImages < $aConfig['default']['photos_no']) {
			$iImages = $aConfig['default']['photos_no'];
		}
		if ($bAddPhoto) {
			// wybranmo dodanie kolejnego zdjecia
			$iImages += $aConfig['default']['photos_no'];
		}
		$this->aSettings = array();
		// przygotwanie tablicy do ustalania kolejnosci wyswietlania zdjec
		// dolaczenie klasy Images
		include_once('Images.class.php');
		$oImages = new Images($iPId,
													$iId,
													$this->aSettings,
													$this->sDir,
													'',
													'newsletters_images');
		$aOrderBy =& $oImages->getOrderBy(count($aImgs));

		for ($i = 0; $i < $iImages; $i++) {
			if (isset($aImgs[$i])) {
				$pForm->AddImage('image['.$i.'_'.$aImgs[$i]['id'].']', $aConfig['lang'][$this->sModule]['image'].' '.($i+1), $aImgs[$i], $aOrderBy, array(), '', false, false, false);
			}
			else {
				$pForm->AddImage('image['.$i.'_0]', $aConfig['lang'][$this->sModule]['image'].' '.($i+1), array(), array(), array(), '', false, false, false);
			}
		}
		$pForm->AddRow('&nbsp;', '<div class="existingPhoto">'.$pForm->GetInputButtonHTML('add_photo', $aConfig['lang']['common']['button_add_photo'], array('onclick'=>'GetObject(\'do\').value=\'add_photo\'; this.form.submit();', 'style' => 'margin-bottom: 10px;'), 'button').'</div>');

		
		// przyciski		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda pobiera dane sprzedawcy
	 * 
	 * @return	array ref	- tablica z danymi
	 */
	function &getData() {
		global $aConfig;
		
		$sSql = "SELECT auto_description 
						 FROM ".$aConfig['tabls']['prefix']."newsletters_settings";
		return Common::GetRow($sSql);
	} // end of getData method


	/**
	 * Metoda wykonuje operacje na zdjeciach akapitu
	 *
	 * @param	object	$pSmarty
	 * @param	integer	$iPId
	 * @param	integer	$iId
	 *
	 * @return	integer
	 */
	function ProceedImages(&$pSmarty) {
		global $aConfig;
		
		if (empty($_POST)) {
			// zabezpieczenie przed wywolaniem akcji bez przeslania formularza
			$this->Edit($pSmarty);
			return;
		}

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->Edit($pSmarty);
			return;
		}
		$this->sDir = $aConfig['common']['newsletter_dir'].$this->iLangId;

		// dolaczenie klasy Images
		include_once('Images.class.php');
		$oImages = new Images('',
													'',
													$this->aSettings,
													$this->sDir,
													'',
													'newsletters_images',
													'');
		return $oImages->proceed();
	} // end of () method
} // end of Module Class
?>