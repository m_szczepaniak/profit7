<?php
/**
* Plik jezykowy dla interfejsu modulu 'Newsletter' Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['m_newsletter']['list'] = "Lista stron newslettera";
$aConfig['lang']['m_newsletter']['preview'] = "Podgląd";
$aConfig['lang']['m_newsletter']['no_items'] = "Brak zdefiniowanych stron newslettera";

$aConfig['lang']['m_newsletter']['categories_groups'] = "Grupy kategorii strony newslettera";
$aConfig['lang']['m_newsletter']['categories'] = "Kategorie strony newslettera";
$aConfig['lang']['m_newsletter']['recipients'] = "Odbiorcy newslettera";

/*---------- ustawienia stron newslettera */
$aConfig['lang']['m_newsletter_settings']['template'] = "Szablon strony";
$aConfig['lang']['m_newsletter_settings']['newsletter_name'] = "Nazwa newslettera";
$aConfig['lang']['m_newsletter_settings']['main_email'] = "Główny adres email (do wysyłki aktywacji)";
$aConfig['lang']['m_newsletter_settings']['conf_email_subject'] = "Temat maila aktywacyjnego)";
$aConfig['lang']['m_newsletter_settings']['preview_emails'] = "Maile testowe (oddzielone średnikami)";
$aConfig['lang']['m_newsletter_settings']['days_to_confirm'] = "Liczba dni na potwierdzenie";
$aConfig['lang']['m_newsletter_settings']['ftext'] = "Opis do formularza";
$aConfig['lang']['m_newsletter_settings']['auto_sending_day'] = "Dzień rozsylki nowości";
$aConfig['lang']['m_newsletter_settings']['auto_send_subject'] = "Temat maila rozsyłki nowości";
$aConfig['lang']['m_newsletter_settings']['auto_send_number'] = "Ilosc nowości w newsletterze";
$aConfig['lang']['m_newsletter_settings']['auto_sender'] = "Nadawca maila nowości";
$aConfig['lang']['m_newsletter_settings']['auto_sender_email'] = "Email do rozsyłki nowości";

/*---------- grupy kategorii stron newslettera */
$aConfig['lang']['m_newsletter_categories_groups']['list'] = "Lista grup kategorii strony: \"%s\"";
$aConfig['lang']['m_newsletter_categories_groups']['list_name'] = "Nazwa";
$aConfig['lang']['m_newsletter_categories_groups']['no_items'] = "Brak zdefiniowanych grup kategorii strony newslettera";

$aConfig['lang']['m_newsletter_categories_groups']['edit'] = "Edytuj grupę kategorii";
$aConfig['lang']['m_newsletter_categories_groups']['delete'] = "Usuń grupę kategorii";
$aConfig['lang']['m_newsletter_categories_groups']['delete_q'] = "Czy na pewno usunąć wybraną grupę kategorii?";
$aConfig['lang']['m_newsletter_categories_groups']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_newsletter_categories_groups']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_newsletter_categories_groups']['delete_all_q'] = "Czy na pewno usunąć wybrane grupy kategorii?";
$aConfig['lang']['m_newsletter_categories_groups']['delete_all_err'] = "Nie wybrano żadnej grupy kategorii do usunięcia!";
$aConfig['lang']['m_newsletter_categories_groups']['sort'] = "Sortuj grupy kategorii";
$aConfig['lang']['m_newsletter_categories_groups']['sort_items'] = "Sortowanie grup kategorii strony: \"%s\"";
$aConfig['lang']['m_newsletter_categories_groups']['add'] = "Dodaj grupę kategorii";
$aConfig['lang']['m_newsletter_categories_groups']['del_ok_0'] = "Usunięto grupę kategorii %s newslettera \"%s\"";
$aConfig['lang']['m_newsletter_categories_groups']['del_ok_1'] = "Usunięto grupy kategorii %s newslettera \"%s\"";
$aConfig['lang']['m_newsletter_categories_groups']['del_err'] = "Wystąpił błąd podczas próby usunięcia grupy kategorii \"%s\" newslettera \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_newsletter_categories_groups']['groups_in_use_err'] = "Nie można usunąć grup kategorii dopóki istnieją przypisane do nich kategorie!<br>Usuń lub zmień najpierw przypisania kategorii do tych grup.";

$aConfig['lang']['m_newsletter_categories_groups']['go_back'] = "Powrót do listy stron";

$aConfig['lang']['m_newsletter_categories_groups']['header_0'] = "Dodawanie grupy kategorii";
$aConfig['lang']['m_newsletter_categories_groups']['header_1'] = "Edycja grupy kategorii";
$aConfig['lang']['m_newsletter_categories_groups']['for'] = "Dla strony";
$aConfig['lang']['m_newsletter_categories_groups']['name'] = "Nazwa";
$aConfig['lang']['m_newsletter_categories_groups']['show_name'] = "Wyświetlaj nazwę na liście";

$aConfig['lang']['m_newsletter_categories_groups']['add_ok'] = "Dodano grupę kategorii \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_categories_groups']['add_err'] = "Wystąpił błąd podczas próby dodania grupy kategorii \"%s\" newslettera \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_newsletter_categories_groups']['edit_ok'] = "Wprowadzono zmiany w grupie kategorii \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_categories_groups']['edit_err'] = "Wystąpił błąd podczas próby wprowadzenia zmian w grupie kategorii \"%s\" newslettera \"%s\"!<br><br>Spróbuj ponownie";

/*---------- kategorie stron newslettera */
$aConfig['lang']['m_newsletter_categories']['list'] = "Lista kategorii strony: \"%s\"";
$aConfig['lang']['m_newsletter_categories']['f_group'] = "Grupa kategorii:";
$aConfig['lang']['m_newsletter_categories']['f_all_groups'] = "Wszystkie grupy";
$aConfig['lang']['m_newsletter_categories']['list_name'] = "Nazwa";
$aConfig['lang']['m_newsletter_categories']['list_group_name'] = "Grupa kategorii";
$aConfig['lang']['m_newsletter_categories']['sender'] = "Wysyłka od";
$aConfig['lang']['m_newsletter_categories']['email'] = "Wysyłka z adresu";
$aConfig['lang']['m_newsletter_categories']['public'] = "Publiczna";
$aConfig['lang']['m_newsletter_categories']['no_items'] = "Brak zdefiniowanych kategorii strony newslettera";

$aConfig['lang']['m_newsletter_categories']['edit'] = "Edytuj kategorię";
$aConfig['lang']['m_newsletter_categories']['delete'] = "Usuń kategorię";
$aConfig['lang']['m_newsletter_categories']['delete_q'] = "Czy na pewno usunąć wybraną kategorię?";
$aConfig['lang']['m_newsletter_categories']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_newsletter_categories']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_newsletter_categories']['delete_all_q'] = "Czy na pewno usunąć wybrane kategorie?";
$aConfig['lang']['m_newsletter_categories']['delete_all_err'] = "Nie wybrano żadnej kategorii do usunięcia!";
$aConfig['lang']['m_newsletter_categories']['add'] = "Dodaj kategorię";
$aConfig['lang']['m_newsletter_categories']['sort'] = "Sortuj kategorie w grupie";
$aConfig['lang']['m_newsletter_categories']['sort_items'] = "Sortowanie kategorii strony: \"%s\"";
$aConfig['lang']['m_newsletter_categories']['del_ok_0'] = "Usunięto kategorię %s newslettera \"%s\"";
$aConfig['lang']['m_newsletter_categories']['del_ok_1'] = "Usunięto kategorie %s newslettera \"%s\"";
$aConfig['lang']['m_newsletter_categories']['del_err'] = "Wystąpił błąd podczas próby usunięcia kategorii \"%s\" newslettera \"%s\"!<br>Spróbuj ponownie";

$aConfig['lang']['m_newsletter_categories']['go_back'] = "Powrót do listy stron";

$aConfig['lang']['m_newsletter_categories']['header_0'] = "Dodawanie kategorii";
$aConfig['lang']['m_newsletter_categories']['header_1'] = "Edycja kategorii";
$aConfig['lang']['m_newsletter_categories']['for'] = "Dla strony";
$aConfig['lang']['m_newsletter_categories']['group'] = "Grupa";
$aConfig['lang']['m_newsletter_categories']['name'] = "Nazwa";
$aConfig['lang']['m_newsletter_categories']['default_selected'] = 'Domyślnie zaznaczona';
$aConfig['lang']['m_newsletter_categories']['forced'] = "Nieodznaczalne";
$aConfig['lang']['m_newsletter_categories']['unregistred_cat'] = "Kategoria dla zakupów bez rejestracji";

$aConfig['lang']['m_newsletter_categories']['auto_send_section'] = "Automatyczna rozsyłka";
$aConfig['lang']['m_newsletter_categories']['auto_send'] = "Automatycznie rozsyłany";
$aConfig['lang']['m_newsletter_categories']['send_categories'] = "Wyślij nowości z kategorii";
$aConfig['lang']['m_newsletter_categories']['send_number'] = "Ilośc najnowszych pozycji";
$aConfig['lang']['m_newsletter_categories']['start_sending_day'] = "Wyślij dnia";
$aConfig['lang']['m_newsletter_categories']['start_sending_hour'] = "Wyślij o godzinie";

$aConfig['lang']['m_newsletter_categories']['add_ok'] = "Dodano kategorię \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_categories']['add_err'] = "Wystąpił błąd podczas próby dodania kategorii \"%s\" newslettera \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_newsletter_categories']['edit_ok'] = "Wprowadzono zmiany w ustawieniach kategorii \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_categories']['edit_err'] = "Wystąpił błąd podczas próby wprowadzenia zmian w kategorii \"%s\" newslettera \"%s\"!<br><br>Spróbuj ponownie";

/*---------- newslettery */
$aConfig['lang']['m_newsletter_newsletters']['list'] = "Lista mailingów newslettera: \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['all'] = "Wszystkie";
$aConfig['lang']['m_newsletter_newsletters']['f_sent'] = "Wysłane";
$aConfig['lang']['m_newsletter_newsletters']['list_subject'] = "Tytuł";
$aConfig['lang']['m_newsletter_newsletters']['list_html'] = "HTML";
$aConfig['lang']['m_newsletter_newsletters']['list_recipients'] = "Odbiorcy";
$aConfig['lang']['m_newsletter_newsletters']['sent'] = "Wysłano";
$aConfig['lang']['m_newsletter_newsletters']['sent_by'] = "Wysłał";
$aConfig['lang']['m_newsletter_newsletters']['no_items'] = "Brak zdefiniowanych mailingów newslettera";
$aConfig['lang']['m_newsletter_newsletters']['no_settings'] = "Brak zdefiniowanych ustawień lub kategorii newslettera";
$aConfig['lang']['m_newsletter_newsletters']['already_sent'] = "Mailing został już wysłany lub jest w trakcie rozsyłki!";

$aConfig['lang']['m_newsletter_newsletters']['clicks'] = "Statystyki kliknięć linek mailingu";
$aConfig['lang']['m_newsletter_newsletters']['send_preview'] = "Wyślij próbny mailing";
$aConfig['lang']['m_newsletter_newsletters']['send'] = "Wyślij mailing teraz";
$aConfig['lang']['m_newsletter_newsletters']['send_later'] = "Przygotuj mailing do wysłania";
$aConfig['lang']['m_newsletter_newsletters']['edit'] = "Edytuj mailing";
$aConfig['lang']['m_newsletter_newsletters']['delete'] = "Usuń mailing";
$aConfig['lang']['m_newsletter_newsletters']['delete_q'] = "Czy na pewno usunąć wybrany mailing?";
$aConfig['lang']['m_newsletter_newsletters']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_newsletter_newsletters']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_newsletter_newsletters']['delete_all_q'] = "Czy na pewno usunąć wybrane mailingi newslettera?";
$aConfig['lang']['m_newsletter_newsletters']['delete_all_err'] = "Nie wybrano żadnego mailingu newslettera do usunięcia!";
$aConfig['lang']['m_newsletter_newsletters']['add'] = "Dodaj mailing";
$aConfig['lang']['m_newsletter_newsletters']['del_ok_0'] = "Usunięto mailing %s newslettera \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['del_ok_1'] = "Usunięto mailingi %s newslettera \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['del_err'] = "Wystąpił błąd podczas próby usunięcia mailingu \"%s\" newslettera \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_newsletter_newsletters']['send_err'] = "Wystąpił bład podczas wysyłania mailingu newslettera!<br /><br />Spróbuj ponownie";
$aConfig['lang']['m_newsletter_newsletters']['sent_to'] = "Mailing \"%s\" newslettera \"%s\" został wysłany do %d osób";
$aConfig['lang']['m_newsletter_newsletters']['no_recipients_err'] = "Brak potwierdzonych subskrybentów kategorii \"%s\"!";

$aConfig['lang']['m_newsletter_newsletters']['choose_category_header'] = "Przygotowanie do wysłania mailingu \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['categories'] = "Kategoria";
$aConfig['lang']['m_newsletter_newsletters']['start_sending_date'] = "Wyślij dnia";
$aConfig['lang']['m_newsletter_newsletters']['start_sending_hour'] = "Wyślij o godzinie";

$aConfig['lang']['m_newsletter_newsletters']['go_back'] = "Powrót do listy stron";

$aConfig['lang']['m_newsletter_newsletters']['header_0'] = "Dodawanie mailingu newslettera \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['header_1'] = "Edycja mailingu \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['header_2'] = "Podgląd wysłanego lub usuniętego mailingu \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['for'] = "Dla strony";
$aConfig['lang']['m_newsletter_newsletters']['subject'] = "Tytuł";
$aConfig['lang']['m_newsletter_newsletters']['attachment'] = "Załącznik";
$aConfig['lang']['m_newsletter_newsletters']['old_attachment'] = "Obecny załącznik";
$aConfig['lang']['m_newsletter_newsletters']['del_attachment'] = "Usuń załącznik";
$aConfig['lang']['m_newsletter_newsletters']['get_attachment'] = "Pobierz załącznik";
$aConfig['lang']['m_newsletter_newsletters']['body_txt'] = "Treść TXT";
$aConfig['lang']['m_newsletter_newsletters']['unsubscribe_button'] = "Dołącz tekst oraz link umożliwiający automatyczne wypisanie się z otrzymywania informacji handlowych w tym kodów rabatowych";
$aConfig['lang']['m_newsletter_newsletters']['no_body_err'] = "Nie wprowadzono treści wiadomości!";
$aConfig['lang']['m_newsletter_newsletters']['body_html'] = "Treść HTML";
$aConfig['lang']['m_newsletter_newsletters']['html_images'] = "Dołącz obrazki do treści";
$aConfig['lang']['m_newsletter_newsletters']['category_id'] = "Kategoria";

$aConfig['lang']['m_newsletter_newsletters']['add_ok'] = "Dodano mailing \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['copy_add_ok'] = "Utowrzono kopię mailingu \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['add_err'] = "Wystąpił błąd podczas próby dodania mailingu \"%s\" newslettera!<br>Spróbuj ponownie";
$aConfig['lang']['m_newsletter_newsletters']['edit_ok'] = "Wprowadzono zmiany w ustawieniach mailingu \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['edit_err'] = "Wystąpił błąd podczas próby wprowadzenia zmian w ustawieniach mailingu \"%s\" newslettera \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_newsletter_newsletters']['send_update_ok'] = "Zmieniono ustawienia rozsyłki mailingu \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_newsletters']['send_update_err'] = "Wystąpił błąd podczas próby zmiany ustawień rozsyłki mailingu \"%s\" newslettera \"%s\"!<br><br>Spróbuj ponownie";

/*---------- linki newslettera */
$aConfig['lang']['m_newsletter_clicks']['list'] = "Lista linków mailingu: \"%s / %s\"";
$aConfig['lang']['m_newsletter_clicks']['list2'] = "Lista linków mailingu: \"%s\"";
$aConfig['lang']['m_newsletter_clicks']['list_link'] = "Link";
$aConfig['lang']['m_newsletter_clicks']['list_clicks'] = "Kliknięć";
$aConfig['lang']['m_newsletter_clicks']['get_total_report'] = "pobierz listę kliknięć";
$aConfig['lang']['m_newsletter_clicks']['get_report'] = "Pobierz listę klikających";
$aConfig['lang']['m_newsletter_clicks']['go_back'] = "powrót";

/*---------- subskrybenci newslettera */
$aConfig['lang']['m_newsletter_recipients']['list'] = "Lista subskrybentów newslettera strony: \"%s\"";
$aConfig['lang']['m_newsletter_recipients']['all_categories'] = "Wszystkie kategorie";
$aConfig['lang']['m_newsletter_recipients']['category_id'] = "Kategoria";
$aConfig['lang']['m_newsletter_recipients']['all'] = "Wszystkie";
$aConfig['lang']['m_newsletter_recipients']['confirmed'] = "Potwierdzone";
$aConfig['lang']['m_newsletter_recipients']['confirmed2'] = "Potwierdzony";
$aConfig['lang']['m_newsletter_recipients']['list_email'] = "Adres email";
$aConfig['lang']['m_newsletter_recipients']['list_added'] = "Dodany";
$aConfig['lang']['m_newsletter_recipients']['list_confirmed'] = "Potwierdzony";
$aConfig['lang']['m_newsletter_recipients']['no_items'] = "Brak zdefiniowanych subskrybentów newslettera";
$aConfig['lang']['m_newsletter_recipients']['activate'] = "Zmień stan potwierdzenia";
$aConfig['lang']['m_newsletter_recipients']['delete'] = "Usuń subskrybenta";
$aConfig['lang']['m_newsletter_recipients']['delete_q'] = "Czy na pewno usunąć wybranego subskrybenta?";
$aConfig['lang']['m_newsletter_recipients']['check_all'] = "Zaznacz / odznacz wszystkich";
$aConfig['lang']['m_newsletter_recipients']['delete_all'] = "Usuń zaznaczonych";
$aConfig['lang']['m_newsletter_recipients']['delete_all_q'] = "Czy na pewno usunąć wybranych subskrybentów?";
$aConfig['lang']['m_newsletter_recipients']['delete_all_err'] = "Nie wybrano żadnego subskrybenta do usunięcia!";
$aConfig['lang']['m_newsletter_recipients']['add'] = "Dodaj subskrybentów";
$aConfig['lang']['m_newsletter_recipients']['export_recipients'] = "Pobierz z kategorii";
$aConfig['lang']['m_newsletter_recipients']['del_ok_0'] = "Usunięto subskrybenta %s newslettera \"%s\"";
$aConfig['lang']['m_newsletter_recipients']['del_ok_1'] = "Usunięto subskrybentów %s newslettera \"%s\"";
$aConfig['lang']['m_newsletter_recipients']['del_err'] = "Wystąpił błąd podczas próby usunięcia subskrybenta \"%s\" newslettera \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_newsletter_recipients']['confirmed_0'] = "niepotwierdzony";
$aConfig['lang']['m_newsletter_recipients']['confirmed_1'] = "potwierdzony";
$aConfig['lang']['m_newsletter_recipients']['confirm_ok'] = "Zmieniono stan potwierdzenia subskrybenta \"%s\" newslettera \"%s\" na \"%s\"";
$aConfig['lang']['m_newsletter_recipients']['confirm_err'] = "Wystąpił błąd podczas próby zmiany stanu potwierdzenia subskrybenta \"%s\" newslettera \"%s\" na \"%s\"!<br>Spróbuj ponownie";

$aConfig['lang']['m_newsletter_recipients']['go_back'] = "Powrót do listy stron";

$aConfig['lang']['m_newsletter_recipients']['header_0'] = "Dodawanie subskrybentów";
$aConfig['lang']['m_newsletter_recipients']['header_1'] = "Edycja przypisania do kategorii subskrybenta \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_recipients']['for'] = "Dla strony";
$aConfig['lang']['m_newsletter_recipients']['file'] = "Plik z adresami";
$aConfig['lang']['m_newsletter_recipients']['separator'] = "Separator adresów";
$aConfig['lang']['m_newsletter_recipients']['semicolon'] = "; - średnik";
$aConfig['lang']['m_newsletter_recipients']['comma'] = ", - przecinek";
$aConfig['lang']['m_newsletter_recipients']['tab'] = "\\t - tabulator";
$aConfig['lang']['m_newsletter_recipients']['new_line'] = "\\n - znak nowej linii";
$aConfig['lang']['m_newsletter_recipients']['categories'] = "Przypisz do kategorii";
$aConfig['lang']['m_newsletter_recipients']['subscriber_email'] = "Adres email";
$aConfig['lang']['m_newsletter_recipients']['subscriber_categories'] = "Kategorie";

$aConfig['lang']['m_newsletter_recipients']['file_err'] = "W przesłanym pliku nie odnaleziono żadnego adresu e-mail (sprawdź czy adresy w pliku oddzielone są wybranym separatorem)!";
$aConfig['lang']['m_newsletter_recipients']['add_ok'] = "Dodano %d subskrybentów newslettera \"%s\"";
$aConfig['lang']['m_newsletter_recipients']['add_err'] = "Wystąpił błąd podczas próby dodania subskrybentów newslettera \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_newsletter_recipients']['edit_ok'] = "Zmieniono przypisania do kategorii subskrybenta \"%s\" newslettera \"%s\"";
$aConfig['lang']['m_newsletter_recipients']['edit_err'] = "Wystąpił błąd podczas próby zmiany przypisania do kategorii subskrybenta \"%s\" newslettera \"%s\"!<br>Spróbuj ponownie";

/*---------- ustawienia boksu newslettera */
$aConfig['lang']['m_newsletter_box_settings']['page'] = "Strona newslettera";
$aConfig['lang']['m_newsletter_box_settings']['show_categories'] = "Wyświetlaj kategorie w boksie";
$aConfig['lang']['m_newsletter_box_settings']['info'] = "Krótka informacja";
$aConfig['lang']['m_newsletter_box_settings']['too_long_info'] = "Strona newslettera";

/*---------- linki newslettera */
$aConfig['lang']['m_newsletter_auto_newsletters']['list'] = "Lista automatycznych newsletterów nowości";
$aConfig['lang']['m_newsletter_auto_newsletters']['list_send_date'] = "Data rozsyłki";
$aConfig['lang']['m_newsletter_auto_newsletters']['list_status'] = "Status";
$aConfig['lang']['m_newsletter_auto_newsletters']['list_clicks'] = "Kliknięć";
$aConfig['lang']['m_newsletter_auto_newsletters']['list_sent_count'] = "Ilość wysłanych";
$aConfig['lang']['m_newsletter_auto_newsletters']['f_all'] = "Wszystkie";
$aConfig['lang']['m_newsletter_auto_newsletters']['f_status'] = "Status";
$aConfig['lang']['m_newsletter_auto_newsletters']['status_0'] = "Utworzony";
$aConfig['lang']['m_newsletter_auto_newsletters']['status_1'] = "W trakcie rozsyłki";
$aConfig['lang']['m_newsletter_auto_newsletters']['status_2'] = "Przerwany";
$aConfig['lang']['m_newsletter_auto_newsletters']['status_4'] = "Zakończony";
$aConfig['lang']['m_newsletter_auto_newsletters']['go_back'] = "powrót";

/*---------- prosby o opinie */

$aConfig['lang']['m_newsletter_opinions_sent']['list'] = "Lista rozesłanych próśb o opinię";
$aConfig['lang']['m_newsletter_opinions_sent']['list_send_date'] = "Data rozsyłki";
$aConfig['lang']['m_newsletter_opinions_sent']['list_clicks'] = "Kliknięć";
$aConfig['lang']['m_newsletter_opinions_sent']['list_sent_count'] = "Ilość wysłanych";
$aConfig['lang']['m_newsletter_opinions_sent']['f_date_from'] = "Data od";
$aConfig['lang']['m_newsletter_opinions_sent']['f_date_to'] = "do";
$aConfig['lang']['m_newsletter_opinions_sent']['f_type'] = "Typ";
$aConfig['lang']['m_newsletter_opinions_sent']['f_type_O'] = "Opineo";
$aConfig['lang']['m_newsletter_opinions_sent']['f_type_C'] = "Ceneo";
$aConfig['lang']['m_newsletter_opinions_sent']['f_type_All'] = "Dowolny";
$aConfig['lang']['m_newsletter_opinions_sent']['type'] = "Typ";
$aConfig['lang']['m_newsletter_opinions_sent']['type_O'] = "Opineo";
$aConfig['lang']['m_newsletter_opinions_sent']['type_C'] = "Ceneo";
$aConfig['lang']['m_newsletter_opinions_sent']['no_items'] = "Brak danych spełniających wybrane kryteria";

$aConfig['lang']['m_newsletter_opinions_clicks']['list_header'] = "Lista osób, do których została wysłana prośba o opinię";
$aConfig['lang']['m_newsletter_opinions_clicks']['list_email'] = "E-mail";
$aConfig['lang']['m_newsletter_opinions_clicks']['list_clicked'] = "Kliknięty";
$aConfig['lang']['m_newsletter_opinions_clicks']['list_clicked_date'] = "Data kliknięcia";
$aConfig['lang']['m_newsletter_opinions_clicks']['go_back'] = "powrót";
$aConfig['lang']['m_newsletter_opinions_clicks']['type_O'] = "Opineo";
$aConfig['lang']['m_newsletter_opinions_clicks']['type_C'] = "Ceneo";
$aConfig['lang']['m_newsletter_opinions_clicks']['no_items'] = "Brak danych spełniających wybrane kryteria";

/*----------- Konfiguracja automatycznego newslettera */
$aConfig['lang']['m_newsletter_auto_options']['image'] = "Zdjęcie";
$aConfig['lang']['m_newsletter_auto_options']['description'] = "Opis";
$aConfig['lang']['m_newsletter_auto_options']['header'] = "Konfiguracja ustawień automatycznie rozsyłanego newslettera";
$aConfig['lang']['m_newsletter_auto_options']['edit_ok'] = "Wprowadzono zmiany w konfiguracji ustawień automatycznej rozsylki newslettera";
$aConfig['lang']['m_newsletter_auto_options']['edit_err'] = "Wystąpił błąd podczas wprowadzania zmian w konfiguracji ustawień autmatycznej rozsyłki newslettera";
?>
