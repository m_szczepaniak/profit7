<?phphttps://www.stride.com/downloads
/**
* Plik jezykowy modulu 'Newsletter'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/

$aConfig['lang']['mod_m_newsletter']['edit_send']	= 'Wprowadź zmiany';
$aConfig['lang']['mod_m_newsletter']['add_send']	= 'Dodaj adres';
$aConfig['lang']['mod_m_newsletter']['email_err']	= 'Podany adres e-mail jest nieprawidłowy!';
$aConfig['lang']['mod_m_newsletter']['category_err']	= 'Musisz wybrać przynajmniej jedną kategorię newslettera!';
$aConfig['lang']['mod_m_newsletter']['email']	= 'Twój adres e-mail:';
$aConfig['lang']['mod_m_newsletter']['categories']	= 'Kategorie newslettera:';
$aConfig['lang']['mod_m_newsletter']['edit_subscriptions']	= ' - edycja subskrypcji';
$aConfig['lang']['mod_m_newsletter']['recipient_added']	= 'Adres e-mail <strong>%s</strong> został dodany do naszej bazy subskrybentów, ale nie jest jeszcze potwierdzony.<br />Kliknij na przesłany pod podany adres link aktywujący.<br />Jeżeli nie zrobisz tego w przeciągu <strong>%d</strong> dni od momentu jego dodania, zostanie on usunięty!<br /><br />Jeżeli nie otrzymałeś maila z linkiem potwierdzającym:
<ul>' .
		'<li>Odczekaj chwilę i sprawdź swoją pocztę ponownie;</li>' .
		'<li>Sprawdź lub poproś swojego administratora o sprawdzenie ustawień antyspamowych Twojego konta pocztowego;</li>' .
		'<li>Sprawdź czy nasz mail nie został potraktowany jako spam i nie został automatycznie przeniesiony do folderu SPAM/JUNK/BULK w Twojej skrzynce pocztowej;</li>' .
		'<li>Spróbuj dodać ponownie swój adres e-mail do naszego newslettera, być może pomyliłeś się za pierwszym razem.</li>' .
		'</ul><br />' .
		'Jeżeli żadna z powyższych porad nie okazała się pomocna prosimy o kontakt z nami <a href="mailto:%s">%s</a>.';
$aConfig['lang']['mod_m_newsletter']['recipient_exists'] = 'Podany adres e-mail <strong>%s</strong> istnieje już w bazie subskrybentów naszego newslettera.';
$aConfig['lang']['mod_m_newsletter']['recipient_exists_unconfirmed'] = 'Podany adres e-mail <strong>%s</strong> istnieje już w bazie subskrybentów naszego newslettera ale <strong>NIE ZOSTAŁ</strong> jeszcze potwierdzony!';
$aConfig['lang']['mod_m_newsletter']['error'] = 'Wystąpił błąd!<br /><br />Spróbuj ponownie jeżeli problem będzie się powtarzał skontaktuj się z nami <a href="mailto:%s">%s</a>.';

$aConfig['lang']['mod_m_newsletter']['confirmation_subject']	= 'Prosba o potwierdzenie subskrypcji newslettera'; 
$aConfig['lang']['mod_m_newsletter']['edit_confirmation_subject']	= 'Prosba o potwierdzenie zmian w subskrypcji newslettera';
$aConfig['lang']['mod_m_newsletter']['confirmation_body_categories'] = 'Witaj,
Twój adres email %s został dodany do listy subskrybentów newslettera \'%s\' w następujących kategoriach:
%s
Prosimy o potwierdzenie chęci otrzymywania drogą elektroniczną informacji o nowościach w naszym serwisie. W tym celu kliknij tutaj: %s
W przypadku braku potwierdzenia Twój adres zostanie usunięty z naszej bazy po %d dniach.

--
%s';
$aConfig['lang']['mod_m_newsletter']['edit_confirmation_body_categories'] = 'Witaj %s,
zostały zmienione ustawienia subskrypcji newslettera \'%s\'. Wybrano następujące kategorie:
%s
Prosimy o potwierdzenie zmian w subskrypcji newslettera w naszym serwisie. W tym celu kliknij tutaj: %s

--
%s';
$aConfig['lang']['mod_m_newsletter']['confirmation_body'] = 'Witaj,
Twój adres email %s został dodany do listy subskrybentów newslettera \'%s\'.

Prosimy o potwierdzenie chęci otrzymywania drogą elektroniczną informacji o nowościach w naszym serwisie. W tym celu kliknij tutaj: %s
W przypadku braku potwierdzenia Twój adres zostanie usunięty z naszej bazy po %d dniach.

--
%s';
$aConfig['lang']['mod_m_newsletter']['edit_confirmed']	= 'Zmiany w kategoriach newslettera dla <strong>%s</strong> zostały zatwierdzone.';

$aConfig['lang']['mod_m_newsletter']['recipient_confirmed']	= 'Adres e-mail <strong>%s</strong> został potwierdzony.';
$aConfig['lang']['mod_m_newsletter']['doesnt_exists'] = 'Taki adres e-mail nie istnieje w bazie subskrybentów naszego newslettera!';
$aConfig['lang']['mod_m_newsletter']['already_confirmed'] = 'Podany adres e-mail <strong>%s</strong> jest już potwierdzony.';
$aConfig['lang']['mod_m_newsletter']['regulations_err'] = 'Brak zgody na przetwarzanie danych osobowych.';

$aConfig['lang']['mod_m_newsletter']['recipient_edited']	= 'Zmiany w ustawieniach dla adresu e-mail <strong>%s</strong> zostały wprowadzone do naszej bazy subskrybentów.';
$aConfig['lang']['mod_m_newsletter']['recipient_deleted']	= 'Adres e-mail <strong>%s</strong> został usunięty z naszej bazy subskrybentów.';
$aConfig['lang']['mod_m_newsletter']['regulations']	= 'Wyrażam dobrowolną zgodę na przetwarzanie moich danych osobowych w celach marketingowych przez Profit M Spółka z ograniczoną odpowiedzialnością w Warszawie(02-305) Al. Jerozolimskie 134, zgodnie z ustawą o ochronie danych osobowych z dnia 29 sierpnia 1997 r. (tekst jednolity: Dz. U. z 2002 r., Nr 101, poz. 926 z późn. zm.). Zgoda może być odwołana w każdym czasie.';
?>