<?php

$aConfig['lang']['mod_m_newsletter_nowosci']['no_data'] = 'W chwili obecnej nie posiadamy żadnych nowości.';

$aConfig['lang']['mod_m_newsletter_nowosci']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['mod_m_newsletter_nowosci']['language'] = "Język książki";


$aConfig['lang']['mod_m_newsletter_nowosci']['shipment'] = "Dostępność";
$aConfig['lang']['mod_m_newsletter_nowosci']['isbn']= 'ISBN: ';
$aConfig['lang']['mod_m_newsletter_nowosci']['shipment_0'] = "Od ręki";
$aConfig['lang']['mod_m_newsletter_nowosci']['shipment_1'] = "1 - 2 dni";
$aConfig['lang']['mod_m_newsletter_nowosci']['shipment_2'] = "7 - 9 dni";
$aConfig['lang']['mod_m_newsletter_nowosci']['shipment_3'] = "na zamówienie";
$aConfig['lang']['mod_m_newsletter_nowosci']['publisher']	= 'Wydawnictwo';
$aConfig['lang']['mod_m_newsletter_nowosci']['authors']	= 'Autor';
$aConfig['lang']['mod_m_newsletter_nowosci']['edition']	= 'Wydanie';
$aConfig['lang']['mod_m_newsletter_nowosci']['avalaibility']	= 'Dostępny: ';
$aConfig['lang']['mod_m_newsletter_nowosci']['currency']	= ' zł';

$aConfig['lang']['mod_m_newsletter_nowosci']['cena_w_ksiegarni'] = "Cena Profit24: ";
$aConfig['lang']['mod_m_newsletter_nowosci']['cena_uzytkownika'] = "Twoja cena: ";
$aConfig['lang']['mod_m_newsletter_nowosci']['price_brutto'] = "Twoja cena";
$aConfig['lang']['mod_m_newsletter_nowosci']['old_price_brutto'] = "Cena katalogowa";

?>