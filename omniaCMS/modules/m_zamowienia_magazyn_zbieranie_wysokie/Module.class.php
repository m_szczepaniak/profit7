<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-30
 * Time: 13:11
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_wysokie;

use Admin;
use Common_m_oferta_produktowa;
use DatabaseManager;
use LIB\EntityManager\Entites\Magazine;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;

class Module__zamowienia_magazyn_zbieranie_wysokie implements ModuleEntity, SingleView
{
  protected $destinationMagazine;

  /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     *
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var string - komunikat
     */
    private $sMsg;

    /**
     * @var Admin
     */
    public $oClassRepository;

    private $sModule;

    /**
     *
     * @param Admin $oClassRepository
     */
    public function __construct(Admin $oClassRepository)
    {
        global $aConfig;
        if (isset($_GET['magazine'])) {
          $this->destinationMagazine = $_GET['magazine'];
        } else {
          $this->destinationMagazine = Magazine::TYPE_HIGH_STOCK_SUPPLIES;
        }


        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function getMsg()
    {
        return $this->sMsg;
    }

    public function doDefault()
    {
        return $this->Show();
    }

    public function parseViewItem(array $aItem)
    {
        return $aItem;
    }

    public function resetViewState()
    {
        $this->resetViewState();
    }

    private function Show() {
        global $pSmarty;
        include_once($_SERVER['DOCUMENT_ROOT'].'omniaCMS/modules/m_oferta_produktowa/client/Common.class.php');
        include_once($_SERVER['DOCUMENT_ROOT'].'omniaCMS/modules/m_zamowienia_magazyn_zbieranie_tramwaj/Common_get_ready_orders_books.class.php');
        $common = new Common_m_oferta_produktowa();

        $products = $this->getRecords();
        usort($products, sortItems);
        foreach ($products as $key => $product) {
            $images = $common->getImages($product['product_id']);
            $products[$key]['image'] = $images[0];

            if ('' != $product['ean_13']) {
                $products[$key]['idents'][$product['ean_13']] = 1;
            }

            if ('' != $product['streamsoft_index']) {
                $products[$key]['idents'][$product['streamsoft_index']] = 1;
            }

            if ('' != $product['isbn_plain']) {
                $products[$key]['idents'][$product['isbn_plain']] = 1;
            }

            if ('' != $product['isbn_10']) {
                $products[$key]['idents'][$product['isbn_10']] = 1;
            }

            if ('' != $product['isbn_13']) {
                $products[$key]['idents'][$product['isbn_13']] = 1;
            }
        }

        $this->pSmarty->assign('products_encoded', json_encode($products));
        $this->pSmarty->assign('products', $products);
        $sHtml = $this->pSmarty->fetch($this->sModule.'/products_collecting.tpl');
        $this->pSmarty->clear_assign('products');
        $this->pSmarty->clear_assign('products_encoded');
        return $sHtml;
    }

    private function getRecords() {

      $sCol = ' PS.profit_x_location ';
      if ($this->destinationMagazine == Magazine::TYPE_LOW_STOCK_SUPPLIES) {
        $sCol = ' PS.profit_g_location ';
      }

        $sSql = 'SELECT DISTINCT CHL.id, CHL.product_id, P.name, PS.profit_g_reservations as quantity, P.ean_13, PS.profit_x_location, PS.profit_g_act_stock, CHL.active, 
                  CONCAT(OIL.package_number, " - ", CHLL.quantity) as packages_numbers, 
                  CHL.created, CHL.created_by, P.streamsoft_indeks, P.isbn_plain, P.isbn_10, P.isbn_13,
                  CONCAT((366 - DAYOFYEAR( CURDATE( ) )), '.$sCol.') AS pack_number,
                  '.$sCol.' AS location
                 FROM collecting_high_level AS CHL
                 JOIN products AS P
                  ON P.id = CHL.product_id
                 JOIN products_stock AS PS
                  ON PS.id = CHL.product_id
                 JOIN collecting_high_level_lists AS CHLL
                  ON CHL.id = CHLL.collecting_high_level_id
                 JOIN orders_items_lists AS OIL
                  ON OIL.id = CHLL.orders_items_lists_id
                 WHERE 1=1
                   AND active = "1"
                   AND CHL.destination_magazine = "'.$this->destinationMagazine.'"
                 GROUP BY CHL.id
                 ORDER BY CHL.id ASC
                 ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }
}