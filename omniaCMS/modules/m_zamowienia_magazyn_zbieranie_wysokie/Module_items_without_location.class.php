<?php
namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_wysokie;

use Common;
use FormTable;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use omniaCMS\lib\interfaces\ModuleEntity;
use View;

class Module__zamowienia_magazyn_zbieranie_wysokie__items_without_location implements ModuleEntity
{

    /**
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;


    public function __construct(\Admin $oClassRepository)
    {
        global $aConfig;

        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;
    }

    public function doDefault()
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header' => _('Lista produktów bez lokalizacji'),
//            'refresh'	=> true,
//            'search'	=> true
        );
        $aAttribs = array(
            'width' => '100%',
            'border' => 0,
            'cellspacing' => 0,
            'cellpadding' => 0,
            'class' => 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'content' => '&nbsp;',
            ),
            array(
                'content' => 'Źródło',
            ),
            array(
                'content' => 'Numer',
            ),
            array(
                'content' => 'FV',
            ),
            array(
                'content' => 'Data'
            ),
            array(
                'content' => 'Utworzył'
            ),
            array(
                'content' => 'Akcja',
            ),

        );

        $type = \LIB\EntityManager\Entites\OrdersItemsLists::INDEPENDENT_SUPPLY_DOCUMENT_TYPE;

        // pobranie liczby wszystkich rekordow
        $sSql = "SELECT count(DISTINCT OSH.id)
							 FROM orders_items_lists_items AS OILI
							 JOIN orders_items_lists AS OIL ON OIL.id = OILI.orders_items_lists_id AND OIL.closed = '0'
                             JOIN orders_send_history AS OSH
							  ON OSH.id = OIL.orders_send_history_id
							 WHERE OILI.confirmed = '0'
                             AND OIL.type = '$type'
							 ";
        $iRowCount = intval(Common::GetOne($sSql));
        if ($iRowCount == 0 && !isset($_GET['reset'])) {
            // resetowanie widoku
            resetViewState($this->sModule);
            // ponowne okreslenie liczby rekordow
            $sSql = "SELECT COUNT(id)
							 FROM " . $aConfig['tabls']['prefix'] . "products_bindings";
            $iRowCount = intval(Common::GetOne($sSql));
        }
        $pView = new View('products_bindings', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);

        if ($iRowCount > 0) {
            // dodanie Pagera do widoku
            $iCurrentPage = $pView->AddPager($iRowCount);
            $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
            $iStartFrom = ($iCurrentPage - 1) * $iPerPage;

            // pobranie wszystkich rekordow
            $sSql = "SELECT DISTINCT OSH.id, OSH.source, OSH.number, OSHA.fv_nr, OSH.date_send, OSH.send_by
							 FROM orders_items_lists_items AS OILI
							 JOIN products AS P ON P.id = OILI.product_id
							 JOIN orders_items_lists AS OIL ON OIL.id = OILI.orders_items_lists_id AND OIL.closed = '0'
							 JOIN orders_send_history AS OSH
							  ON OSH.id = OIL.orders_send_history_id
							 LEFT JOIN orders_send_history_attributes AS OSHA
							  ON OSH.id = OSHA.orders_send_history_id
							 WHERE OILI.confirmed = '0' AND OIL.type = '$type' " .
                ' ORDER BY OILI.' . (isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : ' id ') .
                (isset($_GET['order']) && !empty($_GET['order']) ? ' ' . $_GET['order'] : ' DESC ') .
                " LIMIT " . $iStartFrom . ", " . $iPerPage;
            $aRecords =& Common::GetAll($sSql);
            foreach ($aRecords as &$aItem) {
                $aItem['source'] = in_array($aItem['source'], array(0, 1, 5, 51, 52)) ? ($aConfig['lang'][$this->sModule]['source_' . intval($aItem['source'])]) : \Common::GetOne('SELECT name FROM `external_providers` WHERE id=' . $aItem['source']);
            }


            // ustawienia dla poszczegolnych kolumn rekordu
            $aColSettings = array(
                'id' => array(
                    'show' => false
                ),
                'action' => array(
                    'actions' => array('details'),
                    'params' => array(
                        'details' => array('pid' => '{id}', 'action' => 'items_without_location_details', 'do' => 'default'),
                    )
                )
            );

            // dodanie rekordow do widoku
            $pView->AddRecords($aRecords, $aColSettings);
        }
        // przyciski stopki stopki do widoku
        $aRecordsFooter = array(
            array(),
            array('add')
        );

        $pView->AddRecordsFooter($aRecordsFooter);

        return $pView->Show();
    }

    /**
     * @return array
     */
    private function getSources()
    {

        $sSql = 'SELECT
        name AS label, id AS value
        FROM external_providers
        ORDER BY name
        ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     *
     */
    public function doAdd()
    {
        global $aConfig;

        $aData = [];
        if (isset($_POST) && !empty($_POST)) {
            $aData = $_POST;
        }
        // dolaczenie klasy FormTable
        include_once('Form/FormTable.class.php');
        $pForm = new FormTable('ordered_itm', _('Dodaj defekty'), ['action' => phpSelf(['do' => 'add'])], array('col_width' => 155), true);
        $source = $this->getSources();

        $pForm->AddSelect('source', _('Źródło'), ['onchange' => '$(\'#do\').remove(); this.form.submit();'], addDefaultValue($source), $aData['source']);// źródło
        if ($_POST['source'] != '') {
            $pForm->AddHidden('do', 'saveDefect');
            $invoices = $this->getInvoicesBySource($aData['source']);
            $pForm->AddSelect('osh_id', _('Numer faktury'), [], addDefaultValue($invoices), $aData['osh_id']); // wybierz FV
            $pForm->AddTextArea('defect_ean', _('Defekty'), $aData['defect_ean'], array('class' => 'important', 'style' => 'width: 180px; height: 400px;'));
            $pForm->GetInputButtonHTML("submit", _('Zapisz'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
            $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']) . '&nbsp;&nbsp;' . $pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick' => 'MenuNavigate(\'' . phpSelf(array('do' => 'default')) . '\');'), 'button'));
        }
        return $pForm->ShowForm();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function doSaveDefect() {

        if (empty($_POST)) {
            // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
            return $this->doDefault();
        }

        // dolaczenie klasy walidatora
        include_once('Form/Validator.class.php');
        $oValidator = new \Validator();
        if (!$oValidator->Validate($_POST)) {
            $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
            // wystapil blad wypelnienia formularza
            // wyswietlenie formularza wraz z komunikatem bledu
            return $this->doAdd();
        }
        $this->pDbMgr->BeginTransaction('profit24');
        $sDefectEans = $_POST['defect_ean'];
        $oshId = $_POST['osh_id'];

        include_once('modules/m_zamowienia/Module_sc_confirm_items.class.php');
        // sprawdzamy czy w danym dokumencie znajduje się ten produkt
        $scConfirmItems = new \Module__zamowienia__sc_confirm_items($this->pSmarty, true);
        $scConfirmItems->proceedAdditionalProducts($sDefectEans, $oshId, true);
        // zapisano

        $aAdditionalIndeks = explode("\n", $sDefectEans);
        if (!empty($aAdditionalIndeks)) {
            foreach ($aAdditionalIndeks as $sIndeks) {
                $sIndeks = trim($sIndeks);
                if ($sIndeks != '') {
                    $productId = $scConfirmItems->searchProductByIndexGetId($sIndeks);
                    if ($productId > 0) {
                        // odejmujemy 1 szt z rozliczeń
                        try {
                            if (true === $this->subtractProductDefectSupply($productId, $oshId)) {
                                $log = _('Dodano defekt o ean '.$sIndeks.' do dokumentu o id '.$oshId.' ORAZ zdjęto ten produkt z ilości nierozłożonych z dostawy.');
                            } else {
                                $log = _('Dodano defekt o ean '.$sIndeks.' do dokumentu o id '.$oshId.' towar już został rozłożony, lub nie znaleziono odpowiednich danych.');
                            }

                            $this->sMsg .= GetMessage($log, false);
                            AddLog($log, false);
                        } catch (\Exception $ex) {
                            $this->pDbMgr->RollbackTransaction('profit24');
                            $log = _('Wystąpił błąd podczas zdejmowania defektu - komunikat systemowy : '.$ex->getMessage());
                            $this->sMsg .= GetMessage($log);
                            AddLog($log);
                            return $this->doAdd();
                        }
                    }
                }
            }
        }

        $this->pDbMgr->CommitTransaction('profit24');
        return $this->doDefault();
    }


    /**
     * @param $productId
     * @param $oshId
     * @throws \Exception
     */
    public function subtractProductDefectSupply($productId, $oshId) {
        
        $sSql = 'SELECT id FROM orders_items_lists 
                 WHERE orders_send_history_id = '.$oshId.' AND 
                    type="'.OrdersItemsLists::INDEPENDENT_SUPPLY_DOCUMENT_TYPE.'"
                    AND closed = "0"';
        $listId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($listId > 0) {
            $sSql = 'SELECT id
                     FROM orders_items_lists_items AS OILI 
                     WHERE OILI.orders_items_lists_id = '.$listId.'
                     AND OILI.confirmed = "0"
                     AND OILI.product_id = '.$productId;
            $iOILId = $this->pDbMgr->GetOne('profit24', $sSql);
            if ($iOILId > 0) {
                $stockLocationOrdersItemsListsItems = new StockLocationOrdersItemsListsItems();
                if (false === $stockLocationOrdersItemsListsItems->confirmQuantityOILI($iOILId, 1)) {
                    throw new Exception('Wystąpił błąd podczas potwierdzania listy OILI ' . $iOILId);
                }

                if (false === $stockLocationOrdersItemsListsItems->confirmCloseOILI($listId)) {
                    throw new \Exception('Wystąpił błąd podczas potwierdzania listy Defektów OIL ' . $listId);
                }
                return true;
            }
        }
        return false;
    }

    public function getMsg()
    {
        return $this->sMsg;
    }

    private function getInvoicesBySource($iSource)
    {
        $sSql = 'SELECT 
DISTINCT OSHA.fv_nr AS label, OSH.id AS value
FROM orders_send_history AS OSH
JOIN orders_send_history_attributes AS OSHA
  ON OSH.id = OSHA.orders_send_history_id AND OSHA.fv_nr <> "" AND OSHA.fv_nr IS NOT NULL
WHERE OSH.source = "' . $iSource . '"
ORDER BY OSHA.fv_nr DESC';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }
}
