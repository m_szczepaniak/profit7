<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_wysokie;

use Admin;
use DatabaseManager;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\OrdersItemsLists;
use LIB\EntityManager\Entites\SellPredict;
use LIB\orders\listType\filters;
use LIB\orders\listType\filters\containerAvailableForEmployee;
use LIB\orders\listType\filters\ordersItemsRecordsLimit;
use LIB\orders\listType\getOrderDataSellPredict;
use LIB\orders\listType\manageGetOrdersItemsList;
use LIB\orders\listType\manageGetOrdersItemsListMultilocalize;
use LIB\orders_semaphore\magazineSemaphore;
use Memcached;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj\Abstract_multilocalizeListType;
use Smarty;

/**
 * Description of Module_part_linked_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_zbieranie_wysokie__stock_SH_predict extends Abstract_multilocalizeListType  implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected  $sMsg;
  
  /**
   *
   * @var Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  /**
   *
   * @var DatabaseManager
   */
  protected  $pDbMgr;
  
  /**
   *
   * @var manageGetOrdersItemsList 
   */
  protected $oManageLists;

    protected $sellPredict;

    public function __construct(Admin $oClassRepository) {

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    $this->sellPredict = new SellPredict($this->pDbMgr);

    $oGetOrderData = new getOrderDataSellPredict($this->pDbMgr);
    $this->oManageLists = new manageGetOrdersItemsListMultilocalize($this->pDbMgr, $oGetOrderData);

    $this->oManageLists->setListType(array('SORTER_STOCK'));
    $this->oManageLists->setGetFromTrain(FALSE);
    $this->oManageLists->initOrderListType();
    $this->sListName = 'Pełne Zasoby';
    $this->cListType = OrdersItemsLists::HIGH_STOCK_LEVEL_DOCUMENT_TYPE;

    $checkMagazine = new filters\checkMagazine($this->pDbMgr);
    $checkMagazine->setMagazine(Magazine::TYPE_HIGH_STOCK_SUPPLIES);
    $checkMagazine->reserveItem(true);
    $checkMagazine->forceReserveItems(true);
    $checkMagazine->addSellPredict(false);
    $this->oManageLists->addFilter($checkMagazine);
    $this->oManageLists->addRequiredFilter($checkMagazine);

    if ($_GET['type'] == 'employee') {
        $oFilter = new containerAvailableForEmployee($this->pDbMgr);
        $oFilter->setAvailableForEmpoyee(true);
        $this->oManageLists->addFilter($oFilter);
        $this->oManageLists->addRequiredFilter($oFilter);
    } else {
        $oFilter = new containerAvailableForEmployee($this->pDbMgr);
        $oFilter->setAvailableForEmpoyee(false);
        $this->oManageLists->addFilter($oFilter);
        $this->oManageLists->addRequiredFilter($oFilter);
    }
    $oFilter = new ordersItemsRecordsLimit($this->pDbMgr);
    $oFilter->setLimit(20);
    $this->oManageLists->addFilter($oFilter);

  }

    /**
     *
     */
  public function __destruct()
  {
      $this->unlock();
  }


    /**
     *
     */
    private function unlock() {
        global $aConfig;

        if (class_exists('memcached')) {
            $oMemcache = new Memcached();
            $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
            $magazineSemaphore = new magazineSemaphore($oMemcache);
            $magazineSemaphore->unlock(Magazine::TYPE_LOW_STOCK_SUPPLIES);
        }
    }

    /**
     * @param $iILId
     * @param $aItem
     * @return bool
     */
    public function doInsertOrderListItems($iILId, $aItem) {

        $aValuesOILI = array(
            'orders_items_lists_id' => $iILId,
            'orders_items_id' => 'NULL',
            'orders_id' => 'NULL',
            'products_id' => $aItem['product_id'],
            'product_id' => $aItem['product_id'],
            'quantity' => $aItem['quantity'],
            'is_g' => $aItem['is_g'],
            'isbn_plain' => $aItem['isbn_plain'],
            'isbn_10' => $aItem['isbn_10'],
            'isbn_13' => $aItem['isbn_13'],
            'ean_13' => $aItem['ean_13'],
            'confirmed' => '0'
        );
        $iOIlId = $this->pDbMgr->Insert('profit24', 'orders_items_lists_items', $aValuesOILI);
        if ($iOIlId === FALSE) {
            return false;
        }
        return $iOIlId;
    }

  /**
   * @param array $aList
   * @param \char $cListType
   * @param bool $bGetFromTrain
   * @param string $sPackageNumber
   * @param mixed $mTransportId
   * @param string $sDebug
   * @return bool|int
   */
    public function doCreateList($aList, $cListType, $bGetFromTrain, $sPackageNumber, $mTransportId, $sDebug = '') {
      global $aConfig;
      $bIsErr = false;

      include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
      // dodajemy teraz dane listy
      $aValuesOIL = array(
          'user_id' => $_SESSION['user']['id'],
          'document_number' => ($this->pDbMgr->GetOne('profit24', 'SELECT MAX( id ) FROM orders_items_lists') + 1),
          'package_number' => ($sPackageNumber == '' ? 'TMP' : $sPackageNumber),
          'closed' => '0',
          'type' => $cListType,
          'get_from_train' => $bGetFromTrain,
          'created' => 'NOW()',
          'debug' => $sDebug
      );
      if (is_array($mTransportId)) {
        $aValuesOIL['transport_id'] = 'NULL';
      } elseif ($mTransportId <= 0) {
        $aValuesOIL['transport_id'] = 'NULL';
      } else {
        $aValuesOIL['transport_id'] = $mTransportId;
      }
      $iILId = $this->pDbMgr->Insert('profit24', 'orders_items_lists', $aValuesOIL);

      if ($iILId > 0) {
        // dodajemy składowe
        foreach ($aList as $aItem) {
          // uwaga to jest nadpisane..
          $iOILIId = $this->doInsertOrderListItems($iILId, $aItem);
          if ($iOILIId === false) {
            $bIsErr = true;
          }

            $this->sellPredict->setStatus($aItem['SP_id'], SellPredict::STATUS_COMPLETATION);
            $this->sellPredict->setOrderItemListItem($aItem['SP_id'], $iOILIId);


            // bzdura rezerwujemy jeśli nasz magazyn Stock - rozróżniamy tylko wysoką i niską lokalizację
//          if ($cListType == \LIB\EntityManager\Entites\OrdersItemsLists::HIGH_STOCK_LEVEL_DOCUMENT_TYPE) {
            // rezerwujemy bo mamy tutaj składowe
            if (false === $this->reserveOnMagazine($iOILIId, $aItem)) {
              $bIsErr = true;
            }


//          }

          $aOrderItemValues = array('get_ready_list' => '1');
          if ($this->pDbMgr->Update('profit24', 'orders_items', $aOrderItemValues, 'id = '. $aItem['id']) === FALSE) {
            $bIsErr = true;
          }
        }
      }
      if ($bIsErr === TRUE) {
        return false;
      } else {
        return $iILId;
      }
    }
}
