<?php

class Module
{

    /**
     * @var Smarty
     */
    private $pSmarty;

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;


    public function __construct(&$pSmarty)
    {
        global $pDbMgr;

        $this->pSmarty = $pSmarty;
        $this->pDbMgr = $pDbMgr;

        if ($_GET['do'] == 'publish') {
            return $this->doPublish();
        } else {
            return $this->showList();
        }
    }

    private function showList()
    {
        global $aConfig;

        // dolaczenie klasy View
        include_once('View/View.class.php');

        // zapamietanie opcji
        rememberViewState($this->sModule);

        $aHeader = array(
            'header'	=> _('Lista produktów bez lokalizacji'),
//            'refresh'	=> true,
//            'search'	=> true
        );
        $aAttribs = array(
            'width'				=> '100%',
            'border'			=> 0,
            'cellspacing'	=> 0,
            'cellpadding'	=> 0,
            'class'				=> 'viewHeaderTable',
        );
        $aRecordsHeader = array(
            array(
                'content'	=> '&nbsp;',
            ),
            array(
                'content'	=> 'EAN',
            ),
            array(
                'content'	=> 'Główny indeks',
            ),
            array(
                'content'	=> 'Nazwa produktu',
            ),
            array(
                'content'   => 'Brakująca ilość'
            ),
            array(
                'content'	=> 'Id produktu',
            ),
            array(
                'content'	=> 'Id dostawy niezależnej',
            ),
            array(
                'content'	=> 'Akcja',
            ),

        );

        $type = \LIB\EntityManager\Entites\OrdersItemsLists::INDEPENDENT_SUPPLY_DOCUMENT_TYPE;

        // pobranie liczby wszystkich rekordow
        $sSql = "SELECT count(OILI.id)
							 FROM orders_items_lists_items AS OILI
							 JOIN orders_items_lists AS OIL ON OIL.id = OILI.orders_items_lists_id AND OIL.closed = '0'
							 JOIN orders_send_history AS OSH
							  ON OSH.id = OIL.orders_send_history_id
							 WHERE OILI.confirmed = '0'
							 AND OSH.id = ".$_GET['pid']."
                             AND OIL.type = '$type'
							 ";
        $iRowCount = intval(Common::GetOne($sSql));
        if ($iRowCount == 0 && !isset($_GET['reset'])) {
            // resetowanie widoku
            resetViewState($this->sModule);
            // ponowne okreslenie liczby rekordow
            $sSql = "SELECT COUNT(id)
							 FROM ".$aConfig['tabls']['prefix']."products_bindings";
            $iRowCount = intval(Common::GetOne($sSql));
        }
        $pView = new View('products_bindings', $aHeader, $aAttribs);
        $pView->AddRecordsHeader($aRecordsHeader);

        if ($iRowCount > 0) {
            // dodanie Pagera do widoku
            $iCurrentPage = $pView->AddPager($iRowCount);
            $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
            $iStartFrom = ($iCurrentPage - 1) * $iPerPage;

            // pobranie wszystkich rekordow
            $sSql = "SELECT OILI.id, OILI.ean_13, OILI.isbn_plain, P.name, 
                            (OILI.quantity - OILI.confirmed_quantity) AS def_quantity,
                            OILI.product_id, OIL.document_number
							 FROM orders_items_lists_items AS OILI
							 JOIN products AS P ON P.id = OILI.product_id
							 JOIN orders_items_lists AS OIL ON OIL.id = OILI.orders_items_lists_id AND OIL.closed = '0'
							 JOIN orders_send_history AS OSH
							  ON OSH.id = OIL.orders_send_history_id
							 WHERE 
							 OSH.id = ".$_GET['pid']."
							 AND OILI.confirmed = '0' 
							 AND OIL.type = '$type' ".
                ' ORDER BY OILI.'.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'id').
                (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
                " LIMIT ".$iStartFrom.", ".$iPerPage;
            $aRecords =& Common::GetAll($sSql);


            // ustawienia dla poszczegolnych kolumn rekordu
            $aColSettings = array(
                'id'	=> array(
                    'show'	=> false
                ),
                'action' => array (
                    'actions'	=> array ('publish'),
                    'params' => array (
                        'publish'	=> array('id' => '{id}'),
                    ),
                )
            );

            // dodanie rekordow do widoku
            $pView->AddRecords($aRecords, $aColSettings);
        }
        // przyciski stopki stopki do widoku
        $aRecordsFooter = array(
//            array('check_all', 'delete_all'),
//            array('add')
        );

        $pView->AddRecordsFooter($aRecordsFooter);

        $this->pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
            $pView->Show());
    }


    /**
     * @param $iOILId
     * @return bool
     * @throws Exception
     */
    private function deleteMissing($iOILId) {

        $sSql = 'SELECT OILI.orders_items_lists_id 
                 FROM orders_items_lists_items as OILI
                 WHERE OILI.id = '.$iOILId.'
                 ';
        $ordersItemsListsData = $this->pDbMgr->GetRow('profit24', $sSql);

        $updateSQL = '
        UPDATE orders_items_lists_items
        SET 
          confirmed = "1"
        WHERE id = '.$iOILId.'
        LIMIT 1
        ';
        if (false === $this->pDbMgr->Query('profit24', $updateSQL)) {
            throw new Exception('Wystapil blad zmiany orders_items_lists_items o id = '.$iOILId);
        }

        $this->tryCloseAllList($ordersItemsListsData['orders_items_lists_id']);

        return true;
    }

    /**
     * @param $iOILId
     * @return bool
     * @throws Exception
     */
    public function tryCloseAllList($iOILId) {

        // zamknąliśmy tytuł spróbujmy zamknąć całą listę
        $sSql = 'SELECT id
                     FROM orders_items_lists_items
                     WHERE confirmed = "0" 
                     AND orders_items_lists_id = '.$iOILId;
        $existsOpen = $this->pDbMgr->GetOne('profit24', $sSql);

        // jeśli nie ma żadnego
        if (intval($existsOpen) <= 0) {
            $valueUp = [
                'closed' => '1'
            ] ;
            if( false === $this->pDbMgr->Update('profit24', 'orders_items_lists', $valueUp, ' id='.$iOILId)) {
                throw new Exception('Wystapil blad podczas zamykania orders_items_lists id = '.$iOILId);
            }
        }
        return true;
    }


    /**
     * @return string
     */
    public function doPublish() {
        $iOILId = $_GET['id'];

        try {
            $this->pDbMgr->BeginTransaction('profit24');
            $this->deleteMissing($iOILId);

            $this->pDbMgr->CommitTransaction('profit24');
            $sMsg = _('Usunięto brak produktu na rozkładaniu o id = '.$iOILId);
            AddLog($sMsg, false);
            $this->sMsg .= GetMessage($sMsg, false);
        } catch (\Exception $ex) {
            $this->pDbMgr->RollbackTransaction('profit24');
            $sMsg = _('Wystąpił błąd - '.$ex->getMessage(). ' id = '.$iOILId);
            AddLog($sMsg, false);
            $this->sMsg .= GetMessage($sMsg, false);
        }

        return $this->showList();
    }
}
