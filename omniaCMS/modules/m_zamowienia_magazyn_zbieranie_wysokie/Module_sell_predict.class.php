<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-30
 * Time: 13:11
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_wysokie;

use Admin;
use DatabaseManager;
use FormTable;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\SellPredict;
use LIB\EntityManager\Entites\StockLocation;
use magazine\Containers;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\lib\View\SingleView;

class Module__zamowienia_magazyn_zbieranie_wysokie__sell_predict implements ModuleEntity, SingleView
{

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var Smarty
   */
  private $pSmarty;

  /**
   *
   * @var string - komunikat
   */
  private $sMsg;

  /**
   * @var Admin
   */
  public $oClassRepository;

  private $sModule;

  /**
   *
   * @param Admin $oClassRepository
   */
  public function __construct(Admin $oClassRepository)
  {
    global $aConfig;

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule . $this->oClassRepository->sAction;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }


  public function getMsg()
  {
    return $this->sMsg;
  }

  public function parseViewItem(array $aItem)
  {
  }

  public function resetViewState()
  {
    $this->resetViewState();
  }

  public function doDefault()
  {
    return $this->View();
  }

  /**
   *
   * @return string
   */
  public function doDelete() {

    $iId = $_GET['id'];
    if ($iId > 0) {

      $sSql = 'DELETE FROM sell_predict WHERE id = '.$iId.' LIMIT 1';
      if ($this->pDbMgr->Query('profit24', $sSql) === false) {
        $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania '));
        return $this->doDefault();
      } else {
        $this->sMsg .= GetMessage(_('Usunięto pomyślnie'), false);
        return $this->doDefault();
      }
    } else {
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas usuwania'));
      return $this->doDefault();
    }
  }

  /**
   * @return string
   */
  private function View()
  {
    global $aConfig;

    // dolaczenie klasy View
    include_once('View/View.class.php');

    $aHeader = array(
        'header' => sprintf(_('Towar do przesunięcia')),
        'refresh' => true,
        'search' => true,
        'checkboxes' => false,
    );
    $aAttribs = array(
        'width' => '100%',
        'border' => 0,
        'cellspacing' => 0,
        'cellpadding' => 0,
        'class' => 'viewHeaderTable'
    );
    // DISTINCT CHL.id, P.name, P.ean_13, PS.profit_x_location, PS.profit_g_act_stock, CHL.active, GROUP_CONCAT(DISTINCT CONCAT(OIL.package_number, " - ", CHLL.quantity) SEPARATOR ", ") as packages_numbers, CHL.created, CHL.created_by
    $aRecordsHeader = array(
        array(
            'db_field' => 'name',
            'content' => _('Tytuł'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'db_field' => 'ean_13',
            'content' => _('EAN 13'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'db_field' => 'quantity',
            'content' => _('Ilość'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'db_field' => 'login',
            'content' => _('Zbierane przez'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'db_field' => 'confirmed_quantity',
            'content' => _('Rozł. ilość'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'db_field' => 'OILI_id',
            'content' => _('Dane zbierania/rozkładania'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'db_field' => 'type',
            'content' => _('Typ'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'db_field' => 'status',
            'content' => _('Status'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'db_field' => 'order_by',
            'content' => _('Kolejność'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'db_field' => 'created',
            'content' => _('Utworzono'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'db_field' => 'created_by',
            'content' => _('przez'),
            'sortable' => true,
            'width' => '180'
        ),
        array(
            'content' => $aConfig['lang']['common']['action'],
            'sortable' => false,
            'width' => '25'
        )
    );

    $pView = new \View('sell_predict', $aHeader, $aAttribs);
      $aYesNo = [
          [
              'label' => _('Wszystkie'),
              'value' => '1',
          ],
      ];
    $pView->AddFilter('f_status', _('Status: '), addDefaultValue($aYesNo, 'Aktywne', 'test'), $_POST['f_status']);

      // pobranie liczby wszystkich uzytkownikow
    $aRecords = $this->getRecords();
    $iRowCount = count($aRecords);
    if ($iRowCount > 0) {
      $aColSettings = array(
          'id' => array(
              'show' => false
          ),
          'action' => array(
              'actions' => array('publish','delete'),
              'params' => array(
                  'publish' => ['id' => '{id}'],
                  'delete' => array('id' => '{id}')
              )
          )
      );
      foreach ($aRecords as &$aItem) {
          if ($aItem['OILI_id'] > 0) {
              $sSql = 'SELECT SL.container_id, SLO.type, SLO.stock_location_magazine_type, SLO.reserved_quantity, SLO.confirmed_quantity, SLO.created, SLO.created_by
                       FROM stock_location_orders_items_lists_items AS SLO
                       JOIN stock_location AS SL
                        ON SLO.stock_location_id = SL.id AND SLO.stock_location_magazine_type = SL.magazine_type
                       WHERE SLO.orders_items_lists_items_id = '.$aItem['OILI_id'];
              $dataRows = $this->pDbMgr->GetAll('profit24', $sSql);
              $sHTMLData = '<table>';
              $sHTMLData .= '<tr><th>Lokalizacja</th><th>Typ</th><th>Ilość zarezerwowana</th><th>Ilość zdjęta/Ilość rozłożona</th><th>Użytkownik</th></tr>';
              foreach ($dataRows as $dataRow) {
                  $sHTMLData .= '<tr>';
                  $sHTMLData .='<td><strong>'.
                      $dataRow['container_id'].
                      '</strong></td>';
                  $sDocType = '';
                  if ($dataRow['stock_location_magazine_type'] == Magazine::TYPE_HIGH_STOCK_SUPPLIES) {
                      $sDocType .= '<span style="color: #94100e">'.$dataRow['type'].'-';
                  } else {
                      $sDocType .= '<span style="color: #00bb00">'.($dataRow['type'] == 'MM' ? 'MM+' : $dataRow['type']);
                  }
                  $sHTMLData .='<td>'.
                      $sDocType.
                      '</span></td>';


                  if ($dataRow['stock_location_magazine_type'] == Magazine::TYPE_LOW_STOCK_SUPPLIES) {
                      $sHTMLData .='<td> -- </td>';

                      // tutaj oszukujemy bo nie potiwerdzam tej ilości, a może powinienem...
                      $sHTMLData .= '<td>' .
                          $dataRow['reserved_quantity'] .
                          '</td>';
                  } else {
                      $sHTMLData .='<td>'.
                          $dataRow['reserved_quantity'].
                          '</td>';
                      $sHTMLData .= '<td>' .
                          $dataRow['confirmed_quantity'] .
                          '</td>';
                  }
                  $sHTMLData .='<td>'.
                      $dataRow['created_by'].' o '.$dataRow['created'];
                      '</td>';

                  $sHTMLData .= '</tr>';
              }
              $sHTMLData .= '</table>';
              $aItem['OILI_id'] = $sHTMLData;
          }

          if ($aItem['status'] != '1') {
              $aItem['disabled'][] = 'delete';
          }

          if ($_SESSION['user']['name'] != 'mchudy' && $_SESSION['user']['name'] != 'agolba') {
              $aItem['disabled'][] = 'publish';
          }

        /**
         * const TYPE_ORDER_PREDICT = 1;// sprzedaż i przewidywanie sprzedaży
         * const TYPE_MANUAL = 2;// ręcznie dodane do listy
         * const TYPE_SELL_PREDICT = 3;// przewidywanie sprzedaży
         */
        switch ($aItem['type']) {
          case SellPredict::TYPE_SELL_PREDICT:
            $type = 'PRZEWIDYWANIE SPRZEDAŻY';
            break;
          case SellPredict::TYPE_MANUAL:
            $type = 'RĘCZNIE DODANE';
            break;
          case SellPredict::TYPE_ORDER_PREDICT:
            $type = 'OCZEKUJĄCE ZAMÓWIENIE';
            break;
        }
        $aItem['type'] = $type;


          switch ($aItem['status']) {
              case SellPredict::STATUS_ACTIVE:
                  $status = 'DO ZEBRANIA';
                  break;
              case SellPredict::STATUS_COMPLETATION:
                  $status = 'KOMPLETOWANE';
                  break;
              case SellPredict::STATUS_CLOSED:
                  $status = 'ZAMKNIĘTE';
                  break;
              case SellPredict::STATUS_INACTIVE:
                  $status = 'ROZŁOŻONE';
                  break;
          }
          $aItem['status'] = $status;
      }
      $pView->AddRecords($aRecords, $aColSettings);
    }

    $pView->AddRecordsHeader($aRecordsHeader);

    // dodanie stopki do widoku
    $aRecordsFooter = array(
        array(),
        array('add')
    );
    $pView->AddRecordsFooter($aRecordsFooter);

    return $pView->Show();
  }


    /**
     *
     */
  public function doPublish() {
      $id = $_GET['id'];
      // unreserve
      $stockLocation = new StockLocation($this->pDbMgr);
      try {
          $stockLocation->unreserveBySellPredict($id);
          $this->sMsg .= GetMessage(_('Zamknięto zbieranie o id '.$id), false);
          $sellPredict = new SellPredict($this->pDbMgr);
          $sellPredict->setStatus($id, SellPredict::STATUS_CLOSED);
          return $this->doDefault();
      } catch (\Exception $ex) {
          $this->sMsg .= GetMessage(_('Wystąpił błąd podczas zamykania zbierania o id '.$id));
          return $this->doDefault();
      }
  }

  /**
   *
   */
  public function doAdd() {
    global $aConfig, $pDbMgr;

    $_SESSION['sc_confirm_lists_independent'] = time();

    $aData = array();
    $aLang =& $aConfig['lang'][$this->sModule];

    // dolaczenie klasy FormTable
    include_once('Form/FormTable.class.php');

    // pobranie z bazy danych szczegolow konta uzytkownika
    $sHeader = sprintf($aConfig['lang'][$this->sModule]['details_header']);

    $pForm = new FormTable('ordered_itm', $sHeader, [], array('col_width'=>155), true);
    $pForm->AddHidden('do', 'saveManualCollecting');
    $pForm->AddHidden('sc_date', date('YmdHis'));
//    $pForm->AddSelect('source', _('Źródło'), array('style' => 'font-size: 20'), addDefaultValue($this->_getSources()), $aData['source'], '', true);


    $pForm->AddMergedRow($aLang['details_content_section'], array('class'=>'merged', 'style'=>'padding-left: 160px;'));
    $pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', _('Zapisz'), array('style' => 'font-size: 40px; font-weight: bold; color: red;')));

    $pForm->AddMergedRow('', array('id' => 'sc_products', 'readonly' => 'readonly', 'style' => 'font-size:15px; width: 90%; height: 300px', 'class' => 'editable_input'));

    $sCheckbox = $pForm->GetCheckBoxHTML('ask_quantity', _('Zapytaj o ilość'), [], '', true, false);
    $sButton = $pForm->GetInputButtonHTML("submit", _('Zapisz'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
    $pForm->AddMergedRow(_('Zapytaj o ilość:'). $sCheckbox.' &nbsp; &nbsp; &nbsp;'.$sButton);


    $input = '
    <script type="text/javascript">
      $(function() {
        $("#fv_nr").autocomplete({
          source: function( request, response ) {
            $.getJSON( "'.$aConfig['common']['client_base_url_https'].$aConfig['common']['cms_dir'].'/ajax/GetSourceFile.php?source=" + $("#source").val(), {
              term: request.term
            }, response );
          }
        });
      });
    </script>

    <div style="float: left">ISBN: <input id="search_isbn" type="text" value="" /></div>
    <script type="text/javascript" src="js/independent_supply.js?date=20170314"></script>';
    return (!empty($this->sMsg) ? $this->sMsg : '').$input.ShowTable($pForm->ShowForm());
  }


  /**
   *
   * @param \Smarty $pSmarty
   * @return string
   */
  public function DoSaveManualCollecting() {

    if (empty($_POST)) {
      // zabezpieczenie przed wywolaniem akcji bez przeslania formularza
      $this->doDefault();
      return;
    }

    // dolaczenie klasy walidatora
    include_once('Form/Validator.class.php');
    $oValidator = new \Validator();
    if (!$oValidator->Validate($_POST)) {
      $this->sMsg .= GetMessage($oValidator->GetErrorString(), true);
      // wystapil blad wypelnienia formularza
      // wyswietlenie formularza wraz z komunikatem bledu
      $this->doAdd();
      return;
    }

    $aProductsScanned = $_SESSION['inventory'][$_POST['sc_date']];
    if (empty($aProductsScanned) || !is_array($aProductsScanned)) {
      $this->sMsg .= GetMessage(_('Brak rozpoznanych produktów EAN: '.$aProductsScanned));
      return $this->doDefault();
    }

    $this->pDbMgr->BeginTransaction('profit24');


    $sellPredict = new SellPredict($this->pDbMgr);
    foreach ($aProductsScanned as $productId => $aProduct) {
        $sSql = 'SELECT id FROM sell_predict WHERE product_id = '.$productId.' AND status <> "0" ';
        $iSellPredict = $this->pDbMgr->GetOne('profit24', $sSql);
        if (intval($iSellPredict) <= 0) {
            if (false === $sellPredict->insert($productId, $aProduct['quantity'], SellPredict::TYPE_MANUAL, $_SESSION['user']['name'])) {
                $bIsErr = true;
            }
        } else {
            $this->sMsg .= GetMessage(_('Produkt występuje już na doładowaniu'));
            $this->pDbMgr->RollbackTransaction('profit24');
            return $this->doAdd();
        }
    }


    if ($bIsErr === true) {
      $this->sMsg .= GetMessage(_('Wystąpił błąd podczas zapisywania listy produktów które należy ręcznie zebrać'));
      $this->pDbMgr->RollbackTransaction('profit24');
      return $this->doAdd();
    } else {
      $this->sMsg .= GetMessage(_('Wprowadzono produkty do ręcznego zebrania'), false);
      $this->pDbMgr->CommitTransaction('profit24');
      return $this->doDefault();
    }
  }


  /**
   * @return array
   */
  private function getRecords()
  {
    $sSql = '
        SELECT SP.id, P.name, P.ean_13, SP.quantity, U.login, OILI.confirmed_quantity, OILI.id AS OILI_id, SP.type, SP.status, SP.order_by ,SP.created, SP.created_by
        FROM sell_predict AS SP
        JOIN products AS P
          ON P.id = SP.product_id
        LEFT JOIN orders_items_lists_items AS OILI
          ON SP.orders_items_lists_items_id = OILI.id
        LEFT JOIN orders_items_lists AS OIL
          ON OILI.orders_items_lists_id = OIL.id
        LEFT JOIN users AS U
          ON U.id = OIL.user_id
        WHERE 1=1 ' .
        (isset($_POST['search']) && $_POST['search'] != ''? ' AND P.ean_13 = "' . $_POST['search'] . '" ' : '') .
        (isset($_POST['f_status']) && $_POST['f_status'] == '1' ? '  ' : ' AND SP.status IN ("1","2") ') .
        '
         
         ';

    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
}