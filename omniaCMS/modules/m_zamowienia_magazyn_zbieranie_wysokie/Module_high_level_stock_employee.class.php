<?php

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_wysokie;

use Admin;
use LIB\EntityManager\Entites\Magazine;
use LIB\orders\listType\filters\containerAvailableForEmployee;
use LIB\orders\listType\filters\ordersItemsRecordsLimit;
use LIB\orders\listType\filters\singleLinkedOrder;
use LIB\orders\listType\filters\websiteFilter;
use LIB\orders\listType\manageGetOrdersItemsList;
use LIB\orders\listType\manageGetOrdersItemsListsHighLevelStock;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj\Abstract_highLevelStockGetListType;

class Module__zamowienia_magazyn_zbieranie_wysokie__high_level_stock_employee extends Abstract_highLevelStockGetListType implements ModuleEntity
{

    /**
     * @var Admin
     */
    public $oClassRepository;

    /**
     *
     * @var string - komunikat
     */
    protected  $sMsg;

    /**
     *
     * @var Smarty
     */
    public $pSmarty;

    /**
     *
     * @var string
     */
    public $sModule;

    /**
     *
     * @var DatabaseManager
     */
    protected  $pDbMgr;

    /**
     *
     * @var manageGetOrdersItemsList
     */
    protected $oManageLists;

    public function __construct(Admin $oClassRepository) {
        global $aConfig;
//        parent::__construct($oClassRepository);


        $this->oClassRepository = $oClassRepository;
        $this->pSmarty = $this->oClassRepository->pSmarty;
        $this->sModule = $this->oClassRepository->sModule;
        $this->pDbMgr = $this->oClassRepository->pDbMgr;

        $this->destinationMagazineDeficiencies = Magazine::TYPE_HIGH_STOCK_SUPPLIES;
        $this->oManageLists = new manageGetOrdersItemsListsHighLevelStock($this->pDbMgr, $this->destinationMagazineDeficiencies);
        $this->oManageLists->setListType(array('SORTER_STOCK'));
        $this->oManageLists->setGetFromTrain(FALSE);
        $this->oManageLists->initOrderListType();
        $this->sListName = 'Pełne Zasoby';
        $this->cListType = '5';


        $oFilter = new containerAvailableForEmployee($this->pDbMgr);
        $oFilter->setAvailableForEmpoyee(true);
        $this->oManageLists->addFilter($oFilter);
        $this->oManageLists->addRequiredFilter($oFilter);

        $oFilter = new ordersItemsRecordsLimit($this->pDbMgr);
        $oFilter->setLimit(10);
        $this->oManageLists->addFilter($oFilter);
    }
}
