<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-11-18 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_wspolne;

use Admin;
use Common_get_ready_orders_books;
use DatabaseManager;
use Exception;
use FormTable;
use LIB\EntityManager\Entites\StockLocation;
use LIB\orders\listType\getOrderData;
use LIB\orders\listType\manageGetOrdersItemsList;
use omniaCMS\lib\interfaces\ModuleEntity;

/**
 * Description of Module_common_continue_list
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
require_once('modules/m_zamowienia_magazyn_zbieranie_tramwaj/Common_get_ready_orders_books.class.php');
class Module__zamowienia_magazyn_zbieranie_wspolne__common_continue_list extends Common_get_ready_orders_books implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected  $sMsg;
  
  /**
   *
   * @var Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  /**
   *
   * @var DatabaseManager
   */
  protected  $pDbMgr;
  
  /**
   *
   * @var manageGetOrdersItemsList 
   */
  protected $oManageLists;
  
  public function __construct(Admin $oClassRepository) {
    
    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
  }

  public function doDefault() {
    
    include_once('Form/FormTable.class.php');
    $pForm = new FormTable('continue_list', _('Kontynuuj zbieranie listy'), ['target' => '_blank']);
    $pForm->AddText('pack_number', _('Wczytaj Kuwetę'), '', ['autofocus' => 'autofocus', 'style' => 'font-size: 40px;'], '', 'number', true);
    $pForm->AddHidden('do', 'continueList');
    $pForm->AddInputButton("submit", _('Kontynuuj'), array('style' => 'font-size: 40px; font-weight: bold; color: red;'), 'submit');
    return $pForm->ShowForm();
  }
  
  /**
   * 
   */
  public function doContinueList() {
    
    $sPackNumber = $_POST['pack_number'];
    $iOILId = $this->getOIListId($sPackNumber);
    if ($iOILId > 0) {
      return $this->showContinueList($iOILId);
    } else {
      $this->sMsg .= GetMessage(sprintf(_('Brak kompletowanej listy o wprowadzonym numerze kuwety "%s"'), $sPackNumber));
      return $this->doDefault();
    }
  }

    /**
     *
     */
    public function doSave_list_data() {
        $this->saveListData($this->pSmarty);
    }
    
  /**
   * 
   * @param int $iILId
   * @return string HTML
   */
  private function showContinueList($iILId) {
    $sSql = 'SELECT package_number, get_from_train, destination_magazine FROM orders_items_lists WHERE id = '.$iILId;
    $aOIL = $this->pDbMgr->GetRow('profit24', $sSql);
    $_GET['magazine'] = $aOIL['destination_magazine'];
    $destinationMagazine = $_GET['magazine'];
    $sPackageNumber = $aOIL['package_number'];
    $bGetFromTrain = ($aOIL['get_from_train'] == '1' ? TRUE : FALSE);
    
    /*
    $sSql = "SELECT orders_id 
             FROM orders_items_lists_items
             WHERE orders_items_lists_id = ".$iILId;
    $aItems = Common::GetCol($sSql);
    
    foreach ($aItems as $iItemId) {
      $this->aOrdersAccepted[$iItemId] = '1';
    }
    
    $aList = $this->getRecordsListLimited(0, null, 0, 0, '', TRUE, $this->aOrdersAccepted, true);
    */
    $oGetOrderData = new getOrderData($this->pDbMgr, $destinationMagazine);
    $aList = $oGetOrderData->getOrdersItemsListDataByOILId($iILId, $bGetFromTrain);

      $this->stockLocation = new StockLocation($this->pDbMgr); //addOrderListReservation();
      $aList = $this->stockLocation->getStockLocationReservation($aList);

    usort($aList, 'sortItems');
    
    if (!empty($aList) && !empty($sPackageNumber)) {
      $sReturn = $this->getProductsScanner($this->pSmarty, $iILId, $aList, $sPackageNumber);
      return $sReturn;
    } else {
      throw Exception("Błąd brak listy do zebrania");
    }
  }
  
  /**
   * 
   * @param int $iPackNumber
   * @return int
   */
  private function getOIListId($sPackNumber) {
    
    $sSql = 'SELECT id
             FROM orders_items_lists
             WHERE package_number = "'.$sPackNumber.'" AND
                   closed = "0" 
             ORDER BY id DESC
             LIMIT 1';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }

  public function getMsg() {
    return $this->sMsg;
  }

}
