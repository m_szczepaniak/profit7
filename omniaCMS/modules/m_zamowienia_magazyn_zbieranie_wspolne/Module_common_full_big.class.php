<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_wspolne;

use Admin;
use DatabaseManager;
use LIB\orders\listType\filters;
use LIB\orders\listType\filters\typeFilter;
use LIB\orders\listType\manageGetOrdersItemsList;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj\Abstract_getListType;
use Smarty;

/**
 * Description of Module_part_linked_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_zbieranie_wspolne__common_full_big extends Abstract_getListType implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected  $sMsg;

  /**
   *
   * @var Smarty
   */
  public $pSmarty;

  /**
   *
   * @var string
   */
  public $sModule;

  /**
   *
   * @var DatabaseManager
   */
  protected  $pDbMgr;

  /**
   *
   * @var manageGetOrdersItemsList
   */
  protected $oManageLists;

  public function __construct(Admin $oClassRepository) {
      global $aConfig;

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    $this->oManageLists = new manageGetOrdersItemsList($this->pDbMgr);
    $this->oManageLists->setListType(array('SORTER_COMMON'));
    $this->oManageLists->setGetFromTrain(TRUE);
    $this->oManageLists->initOrderListType();
    $this->sListName = 'Zbieranie duże';

      if ($_GET['type'] == typeFilter::TYPE_MIXED) {
          $iMinQuantity = 21;
          $oFilterMax = new filters\zero_quantity_min($this->pDbMgr);
          $oFilterMax->setMinQuantity($iMinQuantity);

//          $iMinWeight = 6.00;
//          $oFilterMax = new filters\zero_weight_min($this->pDbMgr);
//          $oFilterMax->setMinWeight($iMinWeight);


      } else {
          $iMinWeight = 20.00;

          $oFilterMax = new filters\zero_weight_min($this->pDbMgr);
          $oFilterMax->setMinWeight($iMinWeight);
      }

      $oFilter = new typeFilter($this->pDbMgr);
      $oFilter->setFilterCollectingType($_GET['type']);

      $this->oManageLists->addFilter($oFilter);
      $this->oManageLists->addRequiredFilter($oFilter);



    $this->oManageLists->addFilter($oFilterMax);
    $this->oManageLists->addRequiredFilter($oFilterMax);

    $oFilter = new filters\singleOrder($this->pDbMgr);
    $this->oManageLists->addFilter($oFilter);
    $this->cListType = '0';


  }
}
