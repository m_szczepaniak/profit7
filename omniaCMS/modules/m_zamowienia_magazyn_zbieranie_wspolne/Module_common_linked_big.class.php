<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_wspolne;

use Admin;
use DatabaseManager;
use LIB\orders\listType\filters\linkedWeightLimit;
use LIB\orders\listType\filters\onlyLinkedOrder;
use LIB\orders\listType\filters\singleLinkedOrder;
use LIB\orders\listType\manageGetOrdersItemsList;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj\Abstract_getListType;
use Smarty;

/**
 * Description of Module_part_linked_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_zbieranie_wspolne__common_linked_big extends Abstract_getListType implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected  $sMsg;
  
  /**
   *
   * @var Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  /**
   *
   * @var DatabaseManager
   */
  protected  $pDbMgr;
  
  /**
   *
   * @var manageGetOrdersItemsList 
   */
  protected $oManageLists;
  
  public function __construct(Admin $oClassRepository) {
    global $aConfig;

    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    $this->oManageLists = new manageGetOrdersItemsList($this->pDbMgr);
    $this->oManageLists->setListType(array('LINKED_COMMON'));
    $this->oManageLists->setGetFromTrain(TRUE);
    $this->oManageLists->initOrderListType();
    $this->sListName = 'Łączone';
    $this->cListType = '2';


    $oFilter = new onlyLinkedOrder($this->pDbMgr);
    $this->oManageLists->addFilter($oFilter);
    $this->oManageLists->addRequiredFilter($oFilter);

    $iMaxWeight = 20;
    $oFilter = new linkedWeightLimit($this->pDbMgr);
    $oFilter->setLimit($iMaxWeight, false);
    $this->oManageLists->addFilter($oFilter);
    $this->oManageLists->addRequiredFilter($oFilter);

    $oFilter = new singleLinkedOrder($this->pDbMgr);
    $this->oManageLists->addFilter($oFilter);

    $oFilter = new onlyLinkedOrder($this->pDbMgr);
    $oFilter->type_filter = 2;
    $this->oManageLists->addFilter($oFilter);
    $this->oManageLists->addRequiredFilter($oFilter);

//    $oFilter = new linkedOrders($this->pDbMgr);
//    $oFilter->setMaxLinkedOrders(15);
//    $this->oManageLists->addFilter($oFilter);
//    $this->oManageLists->addRequiredFilter($oFilter);





    /*
      $oFilter = new typeFilter($this->pDbMgr);
      $oFilter->setFilterCollectingType($_GET['type']);

      $this->oManageLists->addFilter($oFilter);
      $this->oManageLists->addRequiredFilter($oFilter);
    */
  }
}
