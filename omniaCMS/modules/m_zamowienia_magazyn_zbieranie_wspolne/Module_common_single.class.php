<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace omniaCMS\modules\m_zamowienia_magazyn_zbieranie_wspolne;

use Admin;
use DatabaseManager;
use LIB\orders\listType\filters\ordersItemsLimit45;
use LIB\orders\listType\manageGetOrdersItemsList;
use omniaCMS\lib\interfaces\ModuleEntity;
use omniaCMS\modules\m_zamowienia_magazyn_zbieranie_tramwaj\Abstract_getListType;
use Smarty;

/**
 * Description of Module_stock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Module__zamowienia_magazyn_zbieranie_wspolne__common_single extends Abstract_getListType implements ModuleEntity {

  /**
   * @var Admin
   */
  public $oClassRepository;

  /**
   *
   * @var string - komunikat
   */
  protected  $sMsg;
  
  /**
   *
   * @var Smarty
   */
  public $pSmarty;
  
  /**
   *
   * @var string
   */
  public $sModule;
  
  /**
   *
   * @var DatabaseManager
   */
  protected  $pDbMgr;
  
  /**
   *
   * @var manageGetOrdersItemsList 
   */
  protected $oManageLists;
  
  public function __construct(Admin $oClassRepository) {
    
    $this->oClassRepository = $oClassRepository;
    $this->pSmarty = $this->oClassRepository->pSmarty;
    $this->sModule = $this->oClassRepository->sModule;
    $this->pDbMgr = $this->oClassRepository->pDbMgr;
    $this->oManageLists = new manageGetOrdersItemsList($this->pDbMgr);
    $this->oManageLists->setListType('SINGLE_SORTER_COMMON');
    $this->oManageLists->setGetFromTrain(TRUE);
    $this->oManageLists->initOrderListType();
    $this->sListName = 'Stock Single';
    $this->cListType = '1';
    $oFilter = new ordersItemsLimit45($this->pDbMgr);
    $this->oManageLists->addFilter($oFilter);
  }
}
