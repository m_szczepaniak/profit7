<?php

use LIB\EntityManager\Entites\Product;

/**
 * Klasa Module do obslugi modulu 'Struktura menu'
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {

  // komunikat
	var $sMsg;

	// nazwa modulu - do langow
	var $sModule;

	// ID wersji jez.
	var $iLangId;

	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;

	// sciezka do katalogu zawierajacego pliki XML z elementami menu
	var $sXMLDir;

	/** Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->aPrivileges =& $_SESSION['user']['privileges'];
		$this->sXMLDir = $aConfig['common']['client_base_path'].'images/xml';

		$sDo = '';
		$iId = 0;

		$this->sModule = $_GET['module'];

		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}

		if (isset($_GET['id'])) {
			$iId = $_GET['id'];
		}



	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}

		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iId); break;
			case 'insert': $this->Insert($pSmarty); break;
			case 'update': $this->Update($pSmarty); break;
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iId); break;
			
			case 'sort': 
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show('Sortowanie menu',
																			 $this->getRecords(),
																			 phpSelf(array('do' => 'proceed_sort'),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."menus",
												$this->getRecords(),
												array(
													'language_id' => $this->iLangId
											 	));
			break;
			default: $this->Show($pSmarty); break;
		}
	} // end Module() function


	/**
	 * Metoda usuwa wybrane menu
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	- ID usuwanego menu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iId=0) {
		global $aConfig;
		$bIsErr = false;

		if ($iId > 0) {
			$_POST['delete'][$iId] = '1';
		}
		$iI = 0;
		foreach($_POST['delete'] as $sKey => $sVal) {
			unset($_POST['delete'][$sKey]);
			$_POST['delete'][$iI] = $sKey;
			$iI++;
		}
		if (!empty($_POST['delete'])) {
			Common::BeginTransaction();

			// pobranie nazw usuwanych menu
			$sSql = "SELECT name
					 		 FROM ".$aConfig['tabls']['prefix']."menus
						 	 WHERE id IN (".implode(',', $_POST['delete']).")";
			$aRecords =& Common::GetCol($sSql);
			if ($aRecords === false) {
				$bIsErr = true;
			}
			else {
				foreach ($aRecords as $sSymbol) {
					if (isset($aConfig['lang'][$_GET['file']][$sSymbol])) {
						$sSymbol = $aConfig['lang'][$_GET['file']][$sSymbol];
					}
					$sDel .= '"'.$sSymbol.'", ';
				}
				$sDel = substr($sDel, 0, -2);
			}

			if (!$bIsErr) {
			// usuwanie
				$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."menus
								 WHERE id IN (".implode(',', $_POST['delete']).") AND
											 language_id=".$this->iLangId;
				if (count($_POST['delete']) == 1) {
					$sErrPostfix = '0';
				}
				else {
					$sErrPostfix = '1';
				}
				if (Common::Query($sSql) === false) {
					$bIsErr = true;
				}
			}
			if (!$bIsErr) {
				// usuniecie plikow XML z elementami menu
				foreach ($_POST['delete'] as $iMId) {
					@unlink($this->sXMLDir.'/'.$_SESSION['lang']['symbol'].'/menu_'.$iMId.'.xml');
				}
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			}
			else {
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err_'.$sErrPostfix], $sDel);
				$this->sMsg = GetMessage($sMsg);
				// dodanie informacji do logow
				AddLog($sMsg);
			}
		}
		$this->Show($pSmarty);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowe menu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Insert(&$pSmarty) {
		global $aConfig;

		$bIsErr = false;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty);
			return;
		}

		// dodanie menu do bazy danych
		$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."menus (
							 language_id,
							 name,
							 max_levels,
							 products,
							 product_type,
							 created,
							 created_by
						  )
						  VALUES (
						   ".$this->iLangId.",
							 '".decodeString($_POST['name'])."',
							 '".$_POST['max_levels']."',
							 '".(isset($_POST['products']) ? '1' : '0')."',
							 ".(isset($_POST['product_type']) ? '"'.$_POST['product_type'].'"' : 'NULL').",
							 NOW(),
							 '".$_SESSION['user']['name']."'
						  )";
		$mResult = Common::Query($sSql);
		if ($mResult === false) {
			$bIsErr = true;
		}

		if ($bIsErr) {
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			// wycofanie transakcji
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'], $_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty);
		}
		else {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'], $_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			// reset ustawien widoku
			$_GET['reset'] = 2;
			$this->Show($pSmarty);
		}
	} // end of Insert() funciton


	/**
	 * Metoda wprowadza zmiany w menu do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Update(&$pSmarty) {
		global $aConfig;

		$bIsErr = false;

		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $_GET['id']);
			return;
		}

		// aktualizacja menu w bazie danych
		$sSql = "UPDATE ".$aConfig['tabls']['prefix']."menus SET
							 name = '".decodeString($_POST['name'])."',
							 ".(isset($_POST['max_levels']) ? " max_levels = '".$_POST['max_levels']."'," : '')."
							  products = '".(isset($_POST['products']) ? '1' : '0')."',
							  product_type = ".(isset($_POST['product_type']) ? '"'.$_POST['product_type'].'"' : 'NULL').",
							 modified = NOW(),
							 modified_by = '".$_SESSION['user']['name']."'
						 WHERE
						 	 language_id = ".$this->iLangId." AND
						 	 id = ".$_GET['id'];
		$mResult = Common::Query($sSql);
		if ($mResult === false) {
			$bIsErr = true;
		}

		if ($bIsErr) {
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza edycji
			// wycofanie transakcji
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'], $_POST['name']);
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
			$this->AddEdit($pSmarty, $_GET['id']);
		}
		else {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'], $_POST['name']);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
			$this->Show($pSmarty);
		}
	} // end of Update() funciton


	/**
	 * Metoda tworzy formularz dodawania / edycji menu
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iId	ID edytowanego menu
	 */
	function AddEdit(&$pSmarty, $iId=0) {
		global $aConfig;

		$aData = array();
		$aTemp = array();
		$sHtml = '';

		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');

		if ($iId > 0) {
			// pobranie z bazy danych edytowanego menu
			$sSql = "SELECT name, max_levels, products, product_type
							 FROM ".$aConfig['tabls']['prefix']."menus
							 WHERE id = ".$iId." AND
										 language_id = ".$this->iLangId;
			$aData =& Common::GetRow($sSql);
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
		}

		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($iId > 0 ? 1 : 0)];
		if ($iId > 0 && isset($aData['name']) && !empty($aData['name'])) {
			$sHeader .= ' "'.trimString($aData['name'], 50).'"';
		}

		$pForm = new FormTable('menus', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>150), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', ($iId > 0 ? 'update' : 'insert'));

		// nazwa
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('style'=>'width: 350px;', 'maxlength'=>255));

		//if ($_SESSION['user']['type'] === 1) {
			// jezeli uzytkownik jest superadminem dodanie
			// mozliwosci zdefiniowania maksymalnej liczby poziomow zaglebienia dla menu
			for ($i = 1; $i <= 5; $i++) {
				$aMenuLevels[] = array('value' => $i, 'label'	=> $i);
			}
			$pForm->AddSelect('max_levels', $aConfig['lang'][$this->sModule]['max_levels'], array(), $aMenuLevels, $aData['max_levels'], '', false);
		//}
		
		$pForm->AddCheckBox('products', $aConfig['lang'][$this->sModule]['products'], array(), '', !empty($aData['products']), false);


        $aProductTypesList = $this->getProductTypesList();
        $pForm->AddSelect('product_type', _('Typ produktu'), [], addDefaultValue($aProductTypesList), (isset($aData['product_type']) ? $aData['product_type'] : ''), '', false, []);

		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function


    /**
     * @return array
     */
    private function getProductTypesList() {
        $aPTypes = Product::getProductTypes();
        $aPTypesList = $this->mapKeyValuToList($aPTypes, true);
        return $aPTypesList;
    }

    /**
     * @param $aPTypes
     * @return array
     */
    private function mapKeyValuToList($aPTypes, $pType = false) {

        $aPTypesList = [];
        $i = 0;
        foreach ($aPTypes as $name => $symbol) {
            $aPTypesList[$i] = [
                'label' => $name,
                'value' => $symbol,
            ];
            $i++;
        }
        return $aPTypesList;
    }



	/**
	 * Metoda wyswietla liste zdefiniowanych dla serwisu menu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;

		// dolaczenie klasy View
		include_once('View/View.class.php');

		// zapamietanie opcji
		rememberViewState($this->sModule);

		$aHeader = array(
			'header'	=> $aConfig['lang'][$this->sModule]['list'],
			'refresh'	=> true,
			'search'	=> true
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
		$aRecordsHeader = array(
			array(
				'content'	=> '&nbsp;',
				'sortable'	=> false
			),
			array(
				'db_field'	=> 'name',
				'content'	=> $aConfig['lang'][$this->sModule]['name'],
				'sortable'	=> true
			),
			array(
				'db_field'	=> 'created',
				'content'	=> $aConfig['lang']['common']['created'],
				'sortable'	=> true,
				'width'	=> '145'
			),
			array(
				'db_field'	=> 'created_by',
				'content'	=> $aConfig['lang']['common']['created_by'],
				'sortable'	=> true,
				'width'	=> '85'
			),
			array(
				'content'	=> $aConfig['lang']['common']['action'],
				'sortable'	=> false,
				'width'	=> '40'
			)
		);
	//if ($_SESSION['user']['type'] != 1) {
			// checkboxy wyswietlane tylko dla superadmina i admina
			$aHeader['checkboxes'] = false;
			$aRecordsHeader[4]['width'] = 18;
			unset ($aRecordsHeader[0]);
	//	}

		// pobranie liczby wszystkich menu aktualnego serwisu
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id=".$this->iLangId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
		$iRowCount = intval(Common::GetOne($sSql));
		if ($iRowCount == 0 && !isset($_GET['reset'])) {
			// resetowanie widoku
			resetViewState($this->sModule);
			// ponowne okreslenie liczny rekordow
			$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id=".$this->iLangId.
									 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '');
			$iRowCount = intval(Common::GetOne($sSql));
		}
		$pView = new View('menus', $aHeader, $aAttribs);
		$pView->AddRecordsHeader($aRecordsHeader);

		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
			$iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
			$iStartFrom = ($iCurrentPage - 1) * $iPerPage;

			// pobranie wszystkich menu dla danego serwisu
			$sSql = "SELECT id, name, created, created_by
							 FROM ".$aConfig['tabls']['prefix']."menus
							 WHERE language_id = ".$this->iLangId.
										 (isset($_POST['search']) && !empty($_POST['search']) ? ' AND name LIKE \'%'.$_POST['search'].'%\'' : '').
							' ORDER BY '.(isset($_GET['sort']) && !empty($_GET['sort']) ? $_GET['sort'] : 'order_by').
							 (isset($_GET['order']) && !empty($_GET['order']) ? ' '.$_GET['order'] : '').
							 " LIMIT ".$iStartFrom.", ".$iPerPage;
			$aRecords =& Common::GetAll($sSql);

			// ustawienia dla poszczegolnych kolumn rekordu
			$aColSettings = array(
				'id'	=> array(
					'show'	=> false
				),
				'name' => array (
					'link'	=> phpSelf(array('do'=>'edit', 'id'=>'{id}'))
				),
				'action' => array (
					'actions'	=> array ('edit', 'delete'),
					'params' => array (
												'edit'	=> array('id'=>'{id}'),
												'delete'	=> array('id'=>'{id}')
											)
				)
			);
			//if ($_SESSION['user']['type'] != 1) {
				// uzytkownik nie jest supeadminem - usuniecie ikon edycji i usuwania
				unset($aColSettings['action']['actions'][1]);
				unset($aColSettings['action']['actions'][2]);

				for ($i = 0; $i < count($aRecords); $i++) {
					$aRecords[$i]['disabled'] = array('delete', 'edit', 'name');
				}
			//}
			
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
		}
		// przyciski stopki stopki do widoku (superadmin i admin)
		//if ($_SESSION['user']['type'] === 1) {
			$aRecordsFooter = array(
				array('check_all', 'delete_all'),
				array('sort','add')
			);
		//}
		//else {
			// brak przyciskow stopki
			//$aRecordsFooter = array();
		//}
		$pView->AddRecordsFooter($aRecordsFooter);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pView->Show());
	} // end of Show() function
	
	
		/**
	 * Metoda pobiera liste rekordow
	 */
	function &getRecords() {
		global $aConfig;
		
		// pobranie listy wszystkich stron do posortowania
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id = ".$this->iLangId."
						 ORDER BY order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
} // end of Module Class
?>