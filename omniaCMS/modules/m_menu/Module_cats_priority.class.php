<?php
/**
 * Klasa Module do obslugi modulu 'Autorzy' - role autorow
 */

class Module
{

    // komunikat
    var $sMsg;

    // nazwa modulu - do langow
    var $sModule;

    // ID wersji jezykowej
    var $iLangId;

    // Id strony
    var $iPageId;

    // uprawnienia uzytkownika dla wersji jez.
    var $aPrivileges;

    // dane konfiguracyjne
    var $aSettings;

    /**
     * Konstruktor klasy
     *
     * @return    void
     */
    function Module(&$pSmarty)
    {
        global $aConfig;

        $this->sErrMsg = '';
        $this->iLangId = $_SESSION['lang']['id'];
        $this->iPageId = getModulePageId('m_autorzy');
        $this->sModule = $_GET['module'];

        $sDo = '';
        $iId = 0;


        if (isset($_GET['do'])) {
            $sDo = $_GET['do'];
        }
        if (isset($_POST['do'])) {
            $sDo = $_POST['do'];
        }
        if (isset($_GET['action'])) {
            $this->sModule .= '_' . $_GET['action'];
        }

        if (isset($_GET['id'])) {
            $iId = intval($_GET['id']);
        }

        // wydzielenie uprawnien
        $this->aPrivileges =& $_SESSION['user']['privileges'];

        // sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu
        if ($_SESSION['user']['type'] === 0) {
            if (!hasModulePrivileges($_GET['module_id'])) {
                showPrivsAlert($pSmarty);
                return;
            }
        }

        switch ($sDo) {
            case 'proceed_sort':
                // dolaczenie pliku klasy MySort
                include_once('Sort.class.php');
                // obiekt klasy MySort
                $oSort = new MySort();
                // sortowanie rekordow
                $oSort->Proceed($aConfig['tabls']['prefix'] . "menus_items",
                    $this->getRecords(),
                    array(),
                    'priority');
                break;

            default: // dolaczenie pliku klasy MySort
                include_once('Sort.class.php');
                // obiekt klasy MySort
                $oSort = new MySort();
                // utwozenie widoku do sortowania
                $pSmarty->assign('sContent',
                    $oSort->Show('',
                        $this->getRecords(),
                        phpSelf(array('do' => 'proceed_sort'),
                            array(), false)));
                break;
        }
    } // end of Module() method

    /**
     * Metoda pobiera liste rekordow
     *
     * @return    array ref    - lista rekordow
     */
    function &getRecords()
    {
        global $aConfig;

        $types = array_flip(\LIB\EntityManager\Entites\Product::getProductTypes());

        $aTopCats = $this->getTopProductsCategories();
        $sSql = "SELECT id,name
						FROM " . $aConfig['tabls']['prefix'] . "menus_items
					 WHERE module_id = " . getModuleId('m_oferta_produktowa') . "
					 AND mtype = '0'
					 AND parent_id IN (" . implode(',', $aTopCats) . ")
					 ORDER BY priority";

        $result = Common::GetAssoc($sSql);

        foreach ($result as $id => &$element) {

            $sql2 = "select mi2.name, m.product_type
				from menus_items as mi
				left join menus_items as mi2 on mi.parent_id = mi2.id
				join menus as m on mi.menu_id = m.id
				where mi.id = $id";

            $parent = Common::GetRow($sql2);


            if (!empty($parent)) {


                $element .= ' - ' . '<span style=color:blue>' . $parent['name'] . '</span> <span style="color: grey; "> : (' . $types[$parent['product_type']] . ') </span> ';
            }
        }

        return $result;
    } // end of getRecords() method

    function getTopProductsCategories()
    {
        global $aConfig;
        $sSql = "SELECT id
					 FROM " . $aConfig['tabls']['prefix'] . "menus_items
					 WHERE module_id = " . getModuleId('m_oferta_produktowa') . "
					 AND mtype = '0'
					 AND parent_id IS NULL
					 AND published = '1'";
        return Common::GetCol($sSql);
    }

} // end of Module Class
?>