<?php
/**
 * Klasa Module do obslugi modulu 'Struktura menu - elementy menu'
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Module {
  
  // komunikat
	var $sMsg;
	
	// nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jez.
	var $iLangId;
	
	// sciezka do katalogu zawierajacego pliki XML z elementami menu
	var $sXMLDir;
	
	// uprawnienia uzytkownika dla wersji jez.
	var $aPrivileges;
	
	// domyslna konfiguracja modulu
	var $aSettings;
	
	// plik XML z konfiguracja domyslna
	var $sXMLFile;
	
	// katalog z szablonami glownymi stron
	var $sTemplatesDir;
	
	/** Konstruktor klasy
   *
	 * @return	void
   */
	function Module(&$pSmarty) {
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = intval($_SESSION['lang']['id']);
		$this->sXMLDir = $aConfig['common']['client_base_path'].'images/xml';
		
		$this->sXMLFile = 'config/'.$_GET['module'].'/default_config.xml';
		$this->sTemplatesDir = $aConfig['common']['client_base_path'].'smarty/templates/pages';
		
		$sDo = '';
		$iType = '';
		$iMId = 0; // Id menu
		$iId = 0;
		$iPId = 0;
		
		$this->sModule = $_GET['module'];
		
		if (isset($_GET['action'])) {
			$this->sModule .= '_'.$_GET['action'];
		}
		
		if (isset($_GET['do'])) {
			$sDo = $_GET['do'];
		}
		if (isset($_POST['do'])) {
			$sDo = $_POST['do'];
		}
		if (isset($_GET['mtype'])) {
			$iType = intval($_GET['mtype']);
		}
		elseif (isset($_POST['mtype'])) {
			$iType = intval($_POST['mtype']);
		}
		
		if (isset($_GET['mid'])) {
			$iMId = intval($_GET['mid']);
		}
		if (isset($_GET['id'])) {
			$iId = intval($_GET['id']);
		}
		if (isset($_GET['parent_id'])) {
			$iPId = intval($_GET['parent_id']);
		}
		


	// sprawdzenie czy uzytkownik ma wystarczajace uprawnienia do modulu 
		if ($_SESSION['user']['type'] === 0) {
				if (!hasModulePrivileges($_GET['module_id'])){
					showPrivsAlert($pSmarty);
				return;
				}
		}
		
		// pobranie domyslnej konfiguracji modulu
		$this->aSettings =& getModuleConfig($this->sXMLFile);
		//echo $sDo;
		switch ($sDo) {
			case 'delete': $this->Delete($pSmarty, $iMId, $iId); break;
			case 'insert': $this->Insert($pSmarty, $iMId, $iPId); break;
			case 'update': $this->Update($pSmarty, $iMId, $iId); break;
			case 'publish': $this->Publish($pSmarty, $iMId, $iId); break;
			case 'change_menu':
			case 'move_to': $this->MoveTo($pSmarty, $iMId, $iId); break;
			case 'update_move_to': $this->UpdateMoveTo($pSmarty, $iMId, $iId); break;
			case 'change_mtype':
			case 'edit':
			case 'add': $this->AddEdit($pSmarty, $iMId, $iId, $iType, $iPId); break;
			
			case 'sort': 
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// utwozenie widoku do sortowania
				$pSmarty->assign('sContent',
													$oSort->Show($iId > 0 ? getPagePath($iId,
																													 $this->iLangId, true) :
																									$this->getMenuName($iMId),
																			 $this->getRecords($iMId, $iId),
																			 phpSelf(array('do' => 'proceed_sort',
																			 							 'id' => $iId),
																			 				 array(), false)));
			break;
			case 'proceed_sort':
				// dolaczenie pliku klasy MySort
				include_once('Sort.class.php');
				// obiekt klasy MySort
				$oSort = new MySort();
				// sortowanie rekordow
				$oSort->Proceed($aConfig['tabls']['prefix']."menus_items",
												$this->getRecords($iMId, $iId),
												array(
													'language_id' => $this->iLangId,
													'menu_id' => $iMId,
													'parent_id' => ($iId > 0 ? $iId : null)
											 	));
			break;
			
			case 'boxes': $this->BoxesSettings($pSmarty, $iMId, $iId); break;
			case 'update_boxes': $this->UpdateBoxes($pSmarty, $iMId, $iId); break;
			
			default: 
				$this->Show($pSmarty);
			break;
		}
		
		$_SESSION['_settings'][$this->sModule]['synchronize'] = $_POST['synchronize'];
	} // end Module() function
	
	
	/**
	 * Metoda usuwa wybrana strone menu
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iMId	- Id menu
	 * @param		integer	$iId	- ID usuwanej strony menu
	 * @return 	void
	 */
	function Delete(&$pSmarty, $iMId, $iId) {
		global $aConfig;
		$bIsErr = false;
		
		if ($iMId === 0 || $iId === 0) {
			$this->Show($pSmarty, $iMId);
			return;
		}
		
		Common::BeginTransaction();
		// pobranie sciezki do strony
		$sPagePath = $this->getLinkToPath($iId, true);
		
		// pobranie symbolu, numeru porzadkowego strony i Id rodzica
		$sSql = "SELECT symbol, order_by, parent_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE id = ".$iId." AND
						 			 menu_id = ".$iMId." AND
						 			 language_id = ".$this->iLangId;
		$aTemp =& Common::GetRow($sSql);
		if ($aTemp === false) {
			$bIsErr = true;
		}
		$sSymbol = $aTemp['symbol'];
		$iOrderBy = $aTemp['order_by'];
		$iParentId = $aTemp['parent_id'];
		
		if (!$bIsErr) {
			// pobranie nazwy menu
			$sSql = "SELECT name
							 FROM ".$aConfig['tabls']['prefix']."menus
							 WHERE id = ".$iMId." AND
							 			 language_id = ".$this->iLangId;
			$sMenuName = Common::GetOne($sSql);
			if ($sMenuName === false) {
				$bIsErr = true;
			}
		}
		
		if (!$bIsErr) {
			// usuwanie
			$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."menus_items
							 WHERE id = ".$iId." AND
							 			 menu_id = ".$iMId." AND
							 			 parent_id ".($iParentId == '' ? 'IS NULL' : '= '.$iParentId)." AND
										 language_id=".$this->iLangId;
			if (($mResult = Common::Query($sSql)) === false) {
				$bIsErr = true;
			}
		}
		
		if (!$bIsErr && $mResult > 0) {
			// zmiana kolejnosci wszystkich stron o order_by wiekszym od $oiOrderBy
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."menus_items
							 SET order_by = order_by - 1
							 WHERE order_by > ".$iOrderBy." AND
							 			 parent_id ".(empty($iParentId) ? "IS NULL" : "= ".$iParentId)." AND
							 			 menu_id = ".$iMId." AND
										 language_id = ".$this->iLangId;
			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
		}
		
		if (!$bIsErr) {
			// zapisanie stron menu do pliku XML
			if (!$this->writeMenuToXML($iMId)) {
				$bIsErr = true;
			}
		}
		
		if (!$bIsErr) {
			Common::CommitTransaction();
			// jezeli $mResult == 0 oznacza to np. proobe usuniecia strony
			// ktorej nie ma juz w bazie danych (odswiezenie przegladarki)
			if ($mResult > 0) {
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_ok'],
												$sPagePath,
												$sSymbol,
												$sMenuName);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
				
				// usuniecie katalogow plikow i zdjec strony
				deleteDir($this->iLangId.'/'.$iId);
			}
		}
		else {
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['del_err'],
											$sPagePath,
											$sSymbol,
											$sMenuName);
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
		}
		$this->Show($pSmarty, $iMId);
	} // end of Delete() funciton


	/**
	 * Metoda dodaje do bazy danych nowa strone menu
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iMId	- Id menu
	 * @param	integer	$iId	- Id elementu rodzica menu
	 * @return	void
	 */
	function Insert(&$pSmarty, $iMId, $iPId) {
		global $aConfig;
		
		$bIsErr = false;
		
		if (empty($_POST)) {
			$this->Show($pSmarty, $iMId);
			return;
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iMId, 0, $_POST['mtype'], $iPId);
			return;
		}
		
		// sprawdzenie czy uzytkownik moze dodawac / edytowac strony
		// w obrebie wybranej strony 'rodzica'
		// sprawdzenie czy strona o podanym symbolu nie istnieje w bazie danych
		if (!$this->itemExists($_POST['symbol'])) {
			// jezeli dodawana strona, jest strona modulu, a modul ten
			// moze posiadac tylko jedna strone na wersje jezykowa, sprawdzenie
			// czy strona tego modulu w danej wersji jezykowej juz nie istnieje
			if ($_POST['mtype'] == '0' &&
					$this->canBeOnlyOne($_POST['module_id']) &&
					$this->modulePageExists($_POST['module_id'], isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'] : '')) {
				// wyswietlenie komunikatu o istnieniu strony modulu
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err4'], $this->getModuleName($_POST['module_id']));
				$this->sMsg = GetMessage($sMsg);
				$this->AddEdit($pSmarty, $iMId, 0, $_POST['mtype'], $iPId);
				return;
			}
			
			Common::BeginTransaction();
			// okreslenie kolejnego numeru porzadkowego strony
			$sSql = "SELECT IFNULL(MAX(order_by), 0) + 1
							 FROM ".$aConfig['tabls']['prefix']."menus_items
							 WHERE language_id = ".$this->iLangId." AND
							 			 menu_id = ".$iMId." AND
							 			 parent_id ".($iPId == 0 ? "IS NULL" : "= ".$iPId);
			$iOrderBy = Common::GetOne($sSql);
			if ($iOrderBy === false) {
				$bIsErr = true;
			}
			elseif ($iOrderBy > 65535) {
				$iOrderBy = 65535;
			}
	
			if (!$bIsErr) {
				$aValues = array(
					'id' => '#id#',
					'menu_id' => $iMId,
					'language_id' => $this->iLangId,
					'parent_id' => $iPId == 0 ? 'NULL' : $iPId,
					'module_id' => !isset($_POST['module_id']) || $_POST['module_id'] == '0' ? 'NULL' : $_POST['module_id'],
					'link_to_id' => !isset($_POST['link_to_id']) || $_POST['link_to_id'] == '0' ? 'NULL' : $_POST['link_to_id'],
					'symbol' => $_POST['symbol'],
					'name' => decodeString($_POST['name']),
					'mtype' => $_POST['mtype'],
					'menu' => !isset($_POST['menu']) ? '0' : '1',
					'template' => isset($_POST['page_template']) && $_POST['page_template'] != '' ? $_POST['page_template'] : 'NULL',
					'description' => isset($_POST['description']) && trim($_POST['description']) != '' ? nl2br($_POST['description']) : 'NULL',
					'seo_title' => isset($_POST['seo_title']) && !empty($_POST['seo_title']) ? $_POST['seo_title'] : 'NULL',
					'seo_keywords' => isset($_POST['seo_keywords']) && !empty($_POST['seo_keywords']) ? $_POST['seo_keywords'] : 'NULL',
					'url' => !isset($_POST['url']) ? 'NULL' : $_POST['url_prefix'].$_POST['url'],
					'new_window' => !isset($_POST['new_window']) ? '0' : '1',
					'restricted' => !isset($_POST['restricted']) ? '0' : '1',
					'order_by' => $iOrderBy,
					'moption' => isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'] : 'NULL',
					'published' => !isset($_POST['published']) ? '0' : '1',
					'created' => 'NOW()',
					'created_by' => $_SESSION['user']['name'] 
				);
				if (($iNewId = Common::Insert($aConfig['tabls']['prefix']."menus_items",
																		 $aValues,
																		 "language_id = ".$this->iLangId)) === false) {
					$bIsErr = true;
				}
				$iNewId = intval($iNewId);
			}
			
			//if (!$bIsErr) {
				// dodanie zdjecia
			//	$bIsErr = !$this->proceedImage($iNewId);
			//}

			
			// dodanie ustawien boksow jezeli rodzic takie ustawienia posiada
			if ($iPId != 0) {
				$sSql = "SELECT DISTINCT box_id, show_mode
								 FROM ".$aConfig['tabls']['prefix']."boxes_settings
								 WHERE language_id = ".$this->iLangId." AND 
								 			 page_id = ".$iPId;
				if (($aBoxesSettings =& Common::GetAll($sSql)) === false) {
					$bIsErr = true;
				}
				else {
					foreach ($aBoxesSettings as $aBSettings) {
						if (!$bIsErr) {
							$aValues = array(
								'page_id' => $iNewId,
								'language_id' => $this->iLangId,
								'box_id' => intval($aBSettings['box_id']),
								'show_mode' => $aBSettings['show_mode']
							);
							if (Common::Insert($aConfig['tabls']['prefix']."boxes_settings",
															 	 $aValues,
															 	 '',
															 	 false) === false) {
								$bIsErr = true;
							}
						}
					}
				}
			}
			// ustawienia strony modulu
			if (isset($_POST['module']) &&
					file_exists('modules/'.$_POST['module'].'/Module_'.(isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'].'_' : '').'settings.class.php')) {
				// dolaczenie pliku zarzadzajacego konfiguracja kategorii modulu
				include_once('modules/'.$_POST['module'].'/Module_'.(isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'].'_' : '').'settings.class.php');
				$pSettings = new Settings($_POST['module'], $iNewId);
				if ($pSettings->UpdateSettings() === false) {
					$bIsErr = true;
				}
			}
			
			if (!$bIsErr) {
				// zapisanie stron menu do pliku XML
				if (!$this->writeMenuToXML($iMId)) {
					$bIsErr = true;
				}
			}
			if ($bIsErr) {
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				// wycofanie transakcji
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err'],
												$_POST['name'],
												$_POST['symbol'],
												($iPId > 0 ? $this->getLinkToPath($iPId, true) :
																		$this->getMenuName($iMId)));
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iMId, 0, $_POST['mtype'], $iPId);
			}
			else {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_ok'],
												$this->getLinkToPath($iNewId, true),
												$_POST['symbol']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			
				$this->Show($pSmarty, $iMId);
			}
		}
		else {
			// strona o podanym symbolu juz istnieje
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err2'], $_POST['symbol']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEdit($pSmarty, $iMId, 0, $_POST['mtype'], $iPId);
		}
	} // end of Insert() funciton
	

	/**
	 * Metoda wprowadza zmiany w stronie menu do bazy danych
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iMId	- Id menu
	 * @param	integer	$iId	- Id strony
	 * @return	void
	 */
	function Update(&$pSmarty, $iMId, $iId) {
		global $aConfig;
		
		$bIsErr = false;
		
		if (empty($_POST)) {
			$this->Show($pSmarty, $iMId);
			return;
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->AddEdit($pSmarty, $iMId, $iId, $_POST['mtype']);
			return;
		}
		
		// sprawdzenie czy strona o podanym symbolu nie istnieje w bazie danych
		if (!$this->itemExists($_POST['symbol'], $iId)) {
			// jezeli wybrany typ strony to strona modulu, a modul ten
			// moze posiadac tylko jedna strone na wersje jezykowa, sprawdzenie
			// czy strona tego modulu w danej wersji jezykowej juz nie istnieje
			if ($_POST['mtype'] == '0' &&
					$this->canBeOnlyOne($_POST['module_id']) &&
					$this->modulePageExists($_POST['module_id'], isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'] : '', $iId)) {
				// wyswietlenie komunikatu o istnieniu strony modulu
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err4'], $this->getModuleName($_POST['module_id']));
				$this->sMsg = GetMessage($sMsg);
				$this->AddEdit($pSmarty, $iMId, $iId, $_POST['mtype']);
				return;
			}
			
			if ($_POST['mtype'] == '1' && $_POST['link_to_id'] == $iId) {
				// sprawdzenie czy strona nie linkuje do samej siebie
				$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['add_err5']);
				$this->AddEdit($pSmarty, $iMId, $iId, $_POST['old_mtype']);
				return;
			}
			
			if ($_POST['mtype'] != $_POST['old_mtype']) {
				// zmieniono typ strony, sprawdzenie czy nie istnieja
				// boksy odwolujace sie do aktualizowanej strony
				// jezeli istnieja zwrocenie komunikatu o bledzie
				if ($this->linkingBoxesExist($iId)) {
					$this->sMsg = GetMessage($aConfig['lang'][$this->sModule]['add_err3']);
					$this->AddEdit($pSmarty, $iMId, $iId, $_POST['mtype']);
					return;
				}
			}
			
			Common::BeginTransaction();
			
			// aktualizacja strony w menu
			$aValues = array(
				'module_id' => !isset($_POST['module_id']) || $_POST['module_id'] == '0' ? 'NULL' : $_POST['module_id'],
				'link_to_id' => !isset($_POST['link_to_id']) || $_POST['link_to_id'] == '0' ? 'NULL' : $_POST['link_to_id'],
				'symbol' => $_POST['symbol'],
				'name' => decodeString($_POST['name']),
				'mtype' => $_POST['mtype'],
				'menu' => !isset($_POST['menu']) ? '0' : '1',
				'template' => isset($_POST['page_template']) && $_POST['page_template'] != '' ? $_POST['page_template'] : 'NULL',
				'description' => isset($_POST['description']) && trim($_POST['description']) != '' ? nl2br($_POST['description']) : 'NULL',
				'seo_title' => isset($_POST['seo_title']) && !empty($_POST['seo_title']) ? $_POST['seo_title'] : 'NULL',
				'seo_keywords' => isset($_POST['seo_keywords']) && !empty($_POST['seo_keywords']) ? $_POST['seo_keywords'] : 'NULL',
				'url' => !isset($_POST['url']) ? 'NULL' : $_POST['url_prefix'].$_POST['url'],
				'new_window' => !isset($_POST['new_window']) ? '0' : '1',
				'restricted' => !isset($_POST['restricted']) ? '0' : '1',
				'moption' => isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'] : 'NULL',
				'modified' => 'NOW()',
				'modified_by' => $_SESSION['user']['name']
			);
			
				$aValues['published'] = !isset($_POST['published']) ? '0' : '1';

			if (Common::Update($aConfig['tabls']['prefix']."menus_items",
												 $aValues,
												 "id = ".$iId." AND
							 			 			language_id = ".$this->iLangId) === false) {
				$bIsErr = true;
			}
			
			// ustawienia modulu
			if (isset($_POST['module']) &&
					file_exists('modules/'.$_POST['module'].'/Module_'.(isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'].'_' : '').'settings.class.php')) {
				// dolaczenie pliku zarzadzajacego konfiguracja kategorii modulu
				include_once('modules/'.$_POST['module'].'/Module_'.(isset($_POST['moption']) && !empty($_POST['moption']) ? $_POST['moption'].'_' : '').'settings.class.php');
				$pSettings = new Settings($_POST['module'], $iId);
				if ($pSettings->UpdateSettings() === false) {
					$bIsErr = true;
				}
			}
			
			
			//if (!$bIsErr) {
				// dodanie zdjecia
			//	$bIsErr = !$this->proceedImage($iId);
			//}
			

			if (!$bIsErr) {
				// zapisanie stron menu do pliku XML
				if (!$this->writeMenuToXML($iMId)) {
					$bIsErr = true;
				}
			}
			
			if ($bIsErr) {
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza dodawania
				// wycofanie transakcji
				Common::RollbackTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_err'],
												$this->getLinkToPath($iId, true),
												$_POST['symbol']);
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->AddEdit($pSmarty, $iMId, $iId, $_POST['mtype']);
			}
			else {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['edit_ok'],
												$this->getLinkToPath($iId, true),
												$_POST['symbol']);
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			
				$this->Show($pSmarty, $iMId);
			}
		}
		else {
			// strona o podanym symbolu juz istnieje
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['add_err2'], $_POST['symbol']);
			$this->sMsg = GetMessage($sMsg);
			$this->AddEdit($pSmarty, $iMId, $iId, $_POST['mtype']);
		}
	} // end of Update() funciton


	/**
	 * Metoda zmienia stan publikacji wybranej strony
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iMId	- ID menu strony
	 * @param		integer	$iId	- ID strony
	 * @return 	void
	 */
	function Publish(&$pSmarty, $iMId, $iId) {
		global $aConfig;
		$bIsErr = false;
		
		// pobranie aktualnego stanu publikacji strony
		$sSql = "SELECT IF (published = '0', '1', '0') AS published
				 		 FROM ".$aConfig['tabls']['prefix']."menus_items
					 	 WHERE id = ".$iId." AND
					 	 			 menu_id = ".$iMId." AND
					 	 			 language_id = ".$this->iLangId;
		if (($iPublished =& Common::GetOne($sSql)) === false) {
			$bIsErr = true;
		}
		
		if (!$bIsErr) {
			// zmiana stanu publikacji
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."menus_items
							 SET published = IF (published = '0', '1', '0')
							 WHERE language_id = ".$this->iLangId." AND
							 			 menu_id = ".$iMId." AND
										 id = ".$iId;
			if (Common::Query($sSql) === false) {
				$bIsErr = true;
			}
		}
		if ($bIsErr) {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_err'],
											$this->getLinkToPath($iId, true),
											$this->getItemSymbol($iMId, $iId),
											$aConfig['lang'][$this->sModule]['published_'.$iPublished]);
			$this->sMsg = GetMessage($sMsg);
			// dodanie informacji do logow
			AddLog($sMsg);
		}
		else {
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['publ_ok'],
											$this->getLinkToPath($iId, true),
											$this->getItemSymbol($iMId, $iId),
											$aConfig['lang'][$this->sModule]['published_'.$iPublished]);
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
		}
		$this->Show($pSmarty, $iMId);
	} // end of Publish() funciton
	
	
	/**
	 * Metoda tworzy formularz dodawania / edycji strony menu
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iMId	ID menu do ktorego nalezy dodawana / edytowana strona
	 * @param		integer	$iId	ID edytowanej strony menu
	 * @param		string	$sType	- typ menu:
	 * 																		 0: - strona modułu
	 * 																		 1: - link wewnetrzny (do innej strony)
	 * 																		 2: - link zewnetrzny
	 */
	function AddEdit($pSmarty, $iMId, $iId=0, $iType=0, $iPId=0) {
		global $aConfig;
		
		$aData = array();
		$aTemp = array();
		$aSOMIds = array();
		$sHtml = '';
		$iMenuId = $iMId;
		$iOldMType = 0;
		$aCfg = array();
		$bEdit = $iId > 0;
				
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		// okreslenie nazwy, liczby poziomow zaglebienia oraz typu menu
		$sSql = "SELECT name, max_levels, products
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE id = ".$iMId." AND
						 			 language_id = ".$this->iLangId;
		$aMenu =& Common::GetRow($sSql);
		if (!empty($aMenu)) {
			$iLevels = intval($aMenu['max_levels']);
			$sMenu = $aMenu['name'];
			$bProductsMenu = ($aMenu['products'] == '1');
		} 
		unset($aMenu);
		// okreslenie Id modulow ktore moga byc tworzone w menu 'Oferty produktowej'
		$aSOMIds = GetShopOfferModulesIds();
		
		
		if ($bEdit) {
			// pobranie z bazy danych edytowanej strony
			$sSql = "SELECT A.*, A.module_id curr_mod_id, B.menu_id AS link_to_menu_id 
							 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
							 			LEFT JOIN ".$aConfig['tabls']['prefix']."menus_items AS B
							 			ON B.id = A.link_to_id AND
							 				 B.language_id = A.language_id
							 WHERE A.menu_id = ".$iMId." AND
							 			 A.id = ".$iId." AND
										 A.language_id = ".$this->iLangId;
			$aData =& Common::GetRow($sSql);
			$iPId = intval($aData['parent_id']);
			$iOldMTypeId = $aData['mtype'];
			if (!empty($aData['description'])) {
				$aData['description'] = br2nl($aData['description']);
			}
			
			if (!empty($aData['url'])) {
				// okreslenie prefixu adresu
				if (preg_match('/^(http:\/\/|https:\/\/|mailto:)/', $aData['url'], $aMatches)) {
					$aData['url_prefix'] = $aMatches[1];
					$aData['url'] = substr($aData['url'], strlen($aMatches[1]));
				}
			}
			if ($aData !== false) {
				// pobranie zdjecia strony
				$sSql = "SELECT id, directory, photo, description 
								 FROM ".$aConfig['tabls']['prefix']."pages_images 
								 WHERE page_id = ".$iId;
				$aImg =& Common::GetRow($sSql);
			}
		}
		else {
			$aData['mtype'] = $iType;
		}
		if (!empty($_POST)) {
			$aData =& $_POST;
			$_SESSION['_settings'][$this->sModule]['synchronize'] = !empty($aData['synchronize']);
		}
		else {
			if (!isset($aData['menu'])) {
				$aData['menu'] = '1';
			}
		}
		if (!isset($aData['synchronize'])) {
			if (isset($_SESSION['_settings'][$this->sModule]['synchronize'])) {
				$aData['synchronize'] = intval($_SESSION['_settings'][$this->sModule]['synchronize']);
			}
			else {
				$aData['synchronize'] = '1';
			}
		}
		
		$sHeader = $aConfig['lang'][$this->sModule]['header_'.($bEdit ? 1 : 0)];
		
		$pForm = new FormTable('menus_items', $sHeader, array('action'=>phpSelf(array('mid' => $iMId, 'id' => $iId, 'parent_id' => $iPId)), 'enctype'=>'multipart/form-data'), array('col_width'=>200), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', ($bEdit ? 'update' : 'insert'));
		if ($bEdit) {
			if (isset($aData['module_id']) && !empty($aData['module_id'])) {
				$pForm->AddHidden('old_module_id', $aData['module_id']);
			}
			$pForm->AddHidden('old_mtype', $iOldMTypeId);
			// informacja o module aktualnie edytowanej strony
			$pForm->AddHidden('curr_mod_id', $aData['curr_mod_id']);
		}
		
		$pForm->AddRow($aConfig['lang'][$this->sModule]['parent'], ($iPId > 0 ? $this->getLinkToPath($iPId, true) : $sMenu).' /', '', array(), array('class'=>'redText'));
						
		// nazwa
		$pForm->AddText('name', $aConfig['lang'][$this->sModule]['name'], $aData['name'], array('style'=>'width: 350px;', 'maxlength'=>255, 'onkeyup'=>'synchronizeSymbol(document.getElementById(\'synchronize\').checked);'));
		
		// symbol
		$sLabel = $pForm->GetLabelHTML('symbol', $aConfig['lang'][$this->sModule]['symbol']);
		$sInput = $pForm->GetTextHTML('symbol', $aConfig['lang'][$this->sModule]['symbol'], $aData['symbol'], array('style'=>'width: 350px;', 'maxlength'=>255), '', 'text', true, '/^[a-z0-9_-]{3,128}$/').
							'&nbsp;&nbsp;'.$aConfig['lang'][$this->sModule]['synchronize'].'&nbsp;&nbsp;'.
							$pForm->GetCheckBoxHTML('synchronize', '', array(), '', !empty($aData['synchronize']), false).
							'&nbsp;&nbsp;<a href="javascript: void(0);" onclick="synchronizeSymbol(true);" title="'.$aConfig['lang'][$this->sModule]['synchronize_now'].'"><img src="gfx/icons/synchronize_ico.gif" alt="'.$aConfig['lang'][$this->sModule]['synchronize_now'].'" border="0" align="absmiddle" /></a>';
		$pForm->AddRow($sLabel, $sInput);
				
		// strona widoczna w menu
		$pForm->AddCheckBox('menu', $aConfig['lang'][$this->sModule]['menu'], array(), '', !empty($aData['menu']), false);
				
		// strona opublikowana
		
			$pForm->AddCheckBox('published', $aConfig['lang'][$this->sModule]['published'], array(), '', !empty($aData['published']), false);
	
		

		$pForm->AddMergedRow($aConfig['lang'][$this->sModule]['mtype'], array('class'=>'merged', 'style'=>'padding-left: 210px'));
		
		// typ elementu menu
		$aType = array(
			array('value' => 0, 'label'	=> $aConfig['lang'][$this->sModule]['type_0']),
			array('value' => 1, 'label'	=> $aConfig['lang'][$this->sModule]['type_1']),
			array('value' => 2, 'label'	=> $aConfig['lang'][$this->sModule]['type_2'])
		);
	if ($bProductsMenu) {
			// menu Oferty Produktowej - brak mozliwosci stworzenia
			// strony 'Link zewnetrzny' i 'Link wewnetrzny'
			unset($aType[1]);
			unset($aType[2]);
		}
			

		$pForm->AddSelect('mtype', $aConfig['lang'][$this->sModule]['mtype'], array('onchange'=>'ChangeObjValue(\'do\', \'change_mtype\'); this.form.submit();'), $aType, $aData['mtype'], '', false);
		
		switch ($iType) {
			case 0:
				$sModule = '';
				$sModsSql = '';
				$iSOModId = getModuleId('m_oferta_produktowa');
				/*
				if (!empty($aSOMIds)) {
					$sModsSql = "id IN(".implode(',', $aSOMIds).")";
				}
				else {
					$sModsSql = "menu = '1'".($iSOModId > 0 ? " AND id <> ".$iSOModId : '');
				}*/
				//$sModsSql = "menu = '1'";
				
				$aModules =& $this->getAvailModulesList($aSOMIds, isset($aData['curr_mod_id']) ? (int) $aData['curr_mod_id'] : 0, isset($aData['module_id']) ? (int) $aData['module_id'] : 0, $bProductsMenu, $pForm);
				if (isset($aModules['module_id'])) {
					$aData['module_id'] = $aModules['module_id'];
				}
				
//				// pobranie modulow ktorych typem moze byc strona
//				$sSql = "SELECT id, id AS value, symbol AS label, default_module
//								 FROM ".$aConfig['tabls']['prefix']."modules
//								 WHERE ".$sModsSql."
//								 ORDER by symbol";
//				$aModules =& Common::GetAssoc($sSql);
//				if ($aModules !== false && !empty($aModules)){
//					foreach ($aModules as $iKey => $aModule) {
//						$aModulesSymbols[$iKey] = $aModules[$iKey]['label'];
//						$aModules[$iKey]['label'] = $aConfig['lang']['c_modules'][$aModule['label']];
//						if (!isset($aData['module_id']) && $aModule['default_module'] == '1') {
//							$aData['module_id'] = $aModule['value'];
//						}
//						unset($aModules[$iKey]['default_module']);
//					}
//					$sModule = $aModulesSymbols[$aData['module_id']];
//					$pForm->AddHidden('module', $sModule);
//				}
//				else {
//					$aModules = array();
//				}
//
//				$aModules = array_merge(
//					array(array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])),
//					$aModules
//				);

				$pForm->AddSelect('module_id', $aConfig['lang'][$this->sModule]['module_id'], array('onchange'=>'ChangeObjValue(\'do\', \'change_mtype\'); this.form.submit();'), $aModules['modules'], $aData['module_id']);
				$sModule=$aModules['module'];
				// pobranie mozliwych opcji modulu
				$aModuleOptions =& $this->getModuleOptions($sModule);
			
				if (!empty($aModuleOptions)) {
					$pForm->AddSelect('moption', $aConfig['lang'][$this->sModule]['moption'], array('onchange'=>'ChangeObjValue(\'do\', \'change_mtype\'); this.form.submit();'), $aModuleOptions, $aData['moption'], '', false);
				}
				
				// ustawienia modulu
				if ($sModule != '' &&
						file_exists('modules/'.$sModule.'/Module_'.(isset($aData['moption']) && !empty($aData['moption']) ? $aData['moption'].'_' : '').'settings.class.php')) {
					// dolaczenie pliku jezykowego
					include_once('modules/'.$sModule.'/lang/admin_'.
											 $aConfig['default']['language'].'.php');
					// dolaczenie pliku zarzadzajacego konfiguracja strony modulu
					include_once('modules/'.$sModule.'/Module_'.(isset($aData['moption']) && !empty($aData['moption']) ? $aData['moption'].'_' : '').'settings.class.php');
					$pSettings = new Settings($sModule, $iId);
					$pSettings->SettingsForm($pForm);
				}
			break;
			
			case 1:
				// menu strony do ktorej linkuje
				if (!isset($aData['link_to_menu_id'])) {
					$aData['link_to_menu_id'] = $iMId;
				}
				$sSql = "SELECT id AS value, name AS label
								 FROM ".$aConfig['tabls']['prefix']."menus
								 WHERE language_id = ".$this->iLangId."
								 ORDER BY id";
				$aMenus =& Common::GetAll($sSql);
				$pForm->AddSelect('link_to_menu_id', $aConfig['lang'][$this->sModule]['link_to_menu_id'], array('onchange'=>'ChangeObjValue(\'do\', \'change_mtype\'); this.form.submit();'), $aMenus, $aData['link_to_menu_id'], '', false);
				
				$sSql = "SELECT IFNULL(parent_id, 0), id, name, mtype,
												IFNULL(link_to_id, 0) AS link_to_id
								 FROM ".$aConfig['tabls']['prefix']."menus_items
								 WHERE menu_id = ".$aData['link_to_menu_id']." AND
								 			 language_id = ".$this->iLangId."
								 			 ORDER BY parent_id, order_by";
				$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
				// utworzenie drzewa menu
				$aMenuItems =& getMenuTree($aItems);

				// strona do ktorej linkuje
				$aMenuTree2 = array_merge(
					array(array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])),
					getComboMenuTree($aMenuItems)
				);
				$pForm->AddSelect('link_to_id', $aConfig['lang'][$this->sModule]['link_to_id'], array(), $aMenuTree2, $aData['link_to_id']);
			break;
			
			case 2:
				// symbol
				$aPrefixes = array(
					array('value' => 'http://', 'label' => 'http://'),
					array('value' => 'https://', 'label' => 'https://'),
					array('value' => 'mailto:', 'label' => 'mailto:')
				);
				// prefix
				$sPrefix = $pForm->GetSelectHTML('url_prefix', '', array(), $aPrefixes, $aData['url_prefix'], '', false);
				// URL
				$sURL = $pForm->GetTextHTML('url', $aConfig['lang'][$this->sModule]['url'], 				$aData['url'], array('style'=>'width: 350px;', 'maxlength'=>255));
				// prefix + URL
				$pForm->AddRow($aConfig['lang'][$this->sModule]['url'], $sPrefix.'&nbsp;'.$sURL);
			break;
		}
		
		$pForm->AddMergedRow('', array());
		
		// otwierana w nowym oknie
		$pForm->AddCheckBox('new_window', $aConfig['lang'][$this->sModule]['new_window'], array(), '', !empty($aData['new_window']), false);
		
		// dostep tylko dla zalogowanych uzytkownikow
		$pForm->AddCheckBox('restricted', $aConfig['lang'][$this->sModule]['restricted'], array(), '', !empty($aData['restricted']), false);
		
		// tytuł strony
		$pForm->AddText('seo_title', $aConfig['lang'][$this->sModule]['seo_title'], $aData['seo_title'], array('style'=>'width: 350px;', 'maxlength'=>255),'','text',false);
		// słowa kluczowe
		$pForm->AddTextarea('seo_keywords', $aConfig['lang'][$this->sModule]['seo_keywords'], $aData['seo_keywords'], array('style'=>'width: 350px;', 'rows'=>8), 0, '', '', false);
		// opis strony
		$pForm->AddTextarea('description', $aConfig['lang'][$this->sModule]['description'], $aData['description'], array('style'=>'width: 350px;', 'rows'=>8), 0, '', '', false);
		
		
		// zdjecie strony
		
		if (isset($aImg)) {
			$pForm->AddImage('image[0_'.$aImg['id'].']', $aConfig['lang'][$this->sModule]['image'], $aImg);
		}
		else {
			$pForm->AddImage('image[0_0]', $aConfig['lang'][$this->sModule]['image'], array(), array(), array(), '', false, false, false);
		}
		
		
		if ($_SESSION['user']['type'] == 1) {
			// glowny szablon strony
			$pForm->AddSelect('page_template', $aConfig['lang'][$this->sModule]['page_template'], array(), array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])), GetTemplatesList($this->sTemplatesDir)), $aData['template'], '', false);
		}
				
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)],array('onclick'=>'sendMuliselects();')).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of AddEdit() function
	
	
	/**
	 * Metoda wyswietla liste zdefiniowanych dla serwisu menu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function Show(&$pSmarty) {
		global $aConfig;
		$aRecords = array();
		$sIds = '';
		
		// dolaczenie klasy TreeView
		include_once('TreeView/TreeView.class.php');
		
		// zapamietanie opcji
		rememberTreeViewState($this->sModule);
		
		// pobranie listy menu
		// tylko te menu do ktorych uzytkownik posiada uprawnienia
		if (isset($this->aPrivileges['menus']) && !empty($this->aPrivileges['menus'])) {
			foreach ($this->aPrivileges['menus'] as $iId => $iPrivs) {
				if (canCreate(intval($iPrivs, 2))) {
					$sIds .= $iId.',';
				}
			}
			$sIds = substr($sIds, 0, -1);
		}
		$sSql = "SELECT id, name, max_levels
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id = ".$this->iLangId.
						 			 (!empty($sIds) ? " AND id IN (".$sIds.")" : '')."
						 			 ORDER BY order_by";
		$aMenus =& Common::GetAll($sSql);

		foreach ($aMenus as $iMKey => $aMenu) {
			// pobranie z bazy danych listy wszystkich elementow menu
			$sSql = "SELECT IFNULL(A.parent_id, 0), A.id, A.name, A.mtype, A.menu_id AS mid,
											A.symbol, IFNULL(A.link_to_id, 0) AS link_to_id, A.url, A.menu,
											A.created_by, A.published,
											B.symbol AS m_symbol
							 FROM ".$aConfig['tabls']['prefix']."menus_items AS A
							 			LEFT JOIN ".$aConfig['tabls']['prefix']."modules AS B
							 			ON B.id = A.module_id  
							 WHERE menu_id = ".$aMenu['id']." AND
							 			 language_id = ".$this->iLangId."
							 			 ORDER BY A.parent_id, A.order_by";
			$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);

			foreach ($aItems as $iParent => $aPages) {
				foreach ($aPages as $iKey => $aItem) {
					$aItems[$iParent][$iKey]['disabled'] = array();
					$aItems[$iParent][$iKey]['name'] .= ' - '.$aItem['id'];
					// aktualny stan publikacji
					$aItems[$iParent][$iKey]['published'] = $aItem['published'] == '1' ? '<span class="green">'.$aConfig['lang'][$this->sModule]['publ_yes'].'</span>' : '<span class="red">'.$aConfig['lang'][$this->sModule]['publ_no'].'</span>';
					
					if (!empty($aItem['m_symbol'])) {
						$aItems[$iParent][$iKey]['type'] = $aConfig['lang']['c_modules'][$aItem['m_symbol']];
					}
					elseif (!empty($aItem['url'])) {
						$aItems[$iParent][$iKey]['type'] = '<span class="externalLink">'.$aConfig['lang'][$this->sModule]['link'].'</span>'.$aItem['url'];
					}
					elseif (!empty($aItem['link_to_id'])) {
						$aItems[$iParent][$iKey]['type'] = $this->getLinkToPath($aItem['link_to_id']);
					}
					// sprawdzenie dostepnosci sortowania stron
					if (!$this->hasSubpagesToSort($aItems, $aItem['id'])) {
						// strona nie posiada podstron lub brak uprawnien - wylaczone sortowanie
						$aItems[$iParent][$iKey]['disabled'][] = 'sort';
					}
					if ($aItem['mtype'] == '2') {
						// wylaczony podglad
						$aItems[$iParent][$iKey]['disabled'][] = 'preview';
					}
					if (!$this->boxesExist() || $_SESSION['user']['type'] === 2 ||
							$aItem['mtype'] == '2') {
						// brak mozliwosci edycji boksow
						$aItems[$iParent][$iKey]['disabled'][] = 'boxes';
					}
					else {
						if (!$this->canHaveChildren($aItem['id'], $aMenu['max_levels'])) {
							// uzytkownik nie posiada uprawnien do tworzenia ston
							$aItems[$iParent][$iKey]['disabled'][] = 'add';
						}
					}
				}
			}
			
			// utworzenie drzewa menu
			$aMenu['menu'] = '1';
			$aMenu['class'] = 'menu';
			$aMenu['mid'] = $aMenu['id'];
			$aMenu['id'] = 'm'.$aMenu['id'];
			$aMenu['published'] = '';
			$aMenu['disabled'] = array();
			// okreslenie ktore przyciski sa dostepne
			
			if (!in_array('sort', $aMenu['disabled']) && count($aItems[0]) <= 1) {
				// wylaczona mozliwosc sortowania - brak wystarczajacych podstron
				$aMenu['disabled'][] = 'sort';
			}
			
			$aRecords[$iMKey][0] = $aMenu; 
			$aRecords[$iMKey][0]['children'] = getMenuTree($aItems);
		}
		// ustawienia dostepnych akcji dla menu
		$aMenuActions = array (
			'actions'	=> array ('sort', 'add'),
			'params' => array (
					'sort'	=> array('mid' => '{mid}'),
					'add'	=> array('mid' => '{mid}')
			),
			'lang' => array (
					'sort' => $aConfig['lang'][$this->sModule]['sort'],
					'add'	=> $aConfig['lang'][$this->sModule]['add']
			)
		);

		// ustawienia dostepnych akcji dla rekordow
		if ($_SESSION['user']['type'] === 0) {
			$aActions = array (
				'actions'	=> array ('publish', 'sort', 'preview',
														'add', 'edit', 'delete'),
				'params' => array (
						'publish'	=> array('mid' => '{mid}', 'id' => '{id}'),
						'sort'	=> array('mid' => '{mid}', 'id' => '{id}'),
						'add'	=> array('mid' => '{mid}', 'parent_id' => '{id}'),
						'edit'	=> array('mid' => '{mid}', 'id' => '{id}', 'mtype' => '{mtype}'),
						'delete'	=> array('mid' => '{mid}', 'id'=>'{id}')
				),
				'lang' => array (
						'publish' => $aConfig['lang'][$this->sModule]['publish'],
						'sort' => $aConfig['lang'][$this->sModule]['sort'],
						'preview' => $aConfig['lang'][$this->sModule]['preview'],
						'add'	=> $aConfig['lang'][$this->sModule]['add'],
						'edit'	=> $aConfig['lang'][$this->sModule]['edit'],
						'delete'	=> $aConfig['lang'][$this->sModule]['delete'],
						'delete_q' => $aConfig['lang'][$this->sModule]['delete_q']
				),
				'links'	=> array(
					'name' => phpSelf(array('do'=>'edit', 'mid' => '{mid}', 'id'=>'{id}', 'mtype' => '{mtype}'))
				)
			);
		}
		else {
			$aActions = array (
				'actions'	=> array ('boxes', 'publish', 'sort', 'preview', 'move_to',
														'add', 'edit', 'delete'),
				'params' => array (
						'boxes'	=> array('mid' => '{mid}', 'id' => '{id}'),
						'publish'	=> array('mid' => '{mid}', 'id' => '{id}'),
						'sort'	=> array('mid' => '{mid}', 'id' => '{id}'),
						'move_to'	=> array('mid' => '{mid}', 'id' => '{id}'),
						'add'	=> array('mid' => '{mid}', 'parent_id' => '{id}'),
						'edit'	=> array('mid' => '{mid}', 'id' => '{id}', 'mtype' => '{mtype}'),
						'delete'	=> array('mid' => '{mid}', 'id'=>'{id}')
				),
				'lang' => array (
						'boxes' => $aConfig['lang'][$this->sModule]['boxes'],
						'publish' => $aConfig['lang'][$this->sModule]['publish'],
						'sort' => $aConfig['lang'][$this->sModule]['sort'],
						'preview' => $aConfig['lang'][$this->sModule]['preview'],
						'move_to' => $aConfig['lang'][$this->sModule]['move_to'],
						'add'	=> $aConfig['lang'][$this->sModule]['add'],
						'edit'	=> $aConfig['lang'][$this->sModule]['edit'],
						'delete'	=> $aConfig['lang'][$this->sModule]['delete'],
						'delete_q' => $aConfig['lang'][$this->sModule]['delete_q']
				),
				'links'	=> array(
					'name' => phpSelf(array('do'=>'edit', 'mid' => '{mid}', 'id'=>'{id}', 'mtype' => '{mtype}'))
				)
			);
		}
		$pTreeView = new TreeView($aConfig['lang'][$this->sModule]['list'],
															$aConfig['lang'][$this->sModule]['no_items'],
															$aConfig['common']['mod_rewrite']);
		
		// dodanie rekordow do widoku
		$pTreeView->AddRecords($aRecords,
													 $aActions,
													 $_SESSION['_treeViewState'][$this->sModule]['expanded'],
													 true,
													 $aMenuActions);

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').
																				$pTreeView->Show());
	} // end of Show() function
	
	
	/**
	 * Metoda tworzy formularz przenoszenia strony w strukturze menu
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iMId	ID menu do ktorego nalezy dodawana / edytowana strona
	 * @param		integer	$iId	ID edytowanej strony menu
	 */
	function MoveTo(&$pSmarty, $iMId, $iId) {
		global $aConfig;
		
		$aData = array();
		$bProductsMenu = $this->isProductsMenu($iMId);
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		if (empty($aData['menu_id'])) {
			$aData['menu_id'] = $iMId;
		}
		if (!isset($aData['parent_id'])) {
			$aData['old_parent_id'] = $aData['parent_id'] = $this->getItemParentId($iMId, $iId);
		}
				
		if ((int) $aData['menu_id'] > 0) {
			// okreslenie liczby poziomow zaglebienia wybranego menu
			$sSql = "SELECT max_levels
							 FROM ".$aConfig['tabls']['prefix']."menus
							 WHERE id = ".$aData['menu_id']." AND
							 			 language_id = ".$this->iLangId;
			$iLevels =& Common::GetOne($sSql);
		}
		// pobranie z bazy danych listy wszystkich elementow menu
		$sSql = "SELECT IFNULL(parent_id, 0), id, name, mtype,
										IFNULL(link_to_id, 0) AS link_to_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE menu_id = ".$aData['menu_id']." AND
						 			 language_id = ".$this->iLangId.
						 			 ($iId > 0 ? " AND id != ".$iId : '')."
						 			 ORDER BY parent_id, order_by";
		$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
		
		// utworzenie drzewa menu
		$aMenuItems =& getMenuTree($aItems);
		
		$sHeader = sprintf($aConfig['lang'][$this->sModule]['header_move_to'],
											 getPagePath($iId, $this->iLangId, true));
		
		$pForm = new FormTable('menus_items', $sHeader, array('action'=>phpSelf(array('id' => $iId))), array('col_width'=>120), $aConfig['common']['js_validation']);
		$pForm->AddHidden('do', 'update_move_to');
		$pForm->AddHidden('old_menu_id', $iMId);
		$pForm->AddHidden('old_parent_id', $aData['old_parent_id']);
				
		// menu strony
		$sSql = "SELECT id AS value, name AS label
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id = ".$this->iLangId.
						 			 ($bProductsMenu ? " AND products = '1'" : " AND products = '0'")."
						 ORDER BY id";
		$aMenus =& Common::GetAll($sSql);
		// usuniecie tych menu do ktorych uzytkownik nie posiada dostepu
		if (count($aMenus) > 1) {
			$aMenus = array_merge(
				array(array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])),
				$aMenus
			);
			$pForm->AddSelect('menu_id', $aConfig['lang'][$this->sModule]['menu_id'], array('onchange'=>'if (this.selectedIndex > 0) {ChangeObjValue(\'do\', \'change_menu\'); this.form.submit();}'), $aMenus, $aData['menu_id']);
		}
		else {
			$pForm->AddHidden('menu_id', $iMId);
		}
				
		$aMenuTree = array_merge(
			array(array('value' => '0', 'label' => $aConfig['lang'][$this->sModule]['main_menu'])),
			getComboMenuTree($aMenuItems, $iLevels)
		);
		// strona rodzica
		$pForm->AddSelect('parent_id', $aConfig['lang'][$this->sModule]['parent_id'], array('size'=>15), $aMenuTree, $aData['parent_id'], '', false);
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_'.($iId > 0 ? 1 : 0)]).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array('do'=>'show'), array('id')).'\');'), 'button'));

		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of MoveTo() function
	
	
	/**
	 * Metoda wprowadza zmiany w stronie menu - przeosi ja w strukturze
	 *
	 * @param		object	$pSmarty
	 * @param	integer	$iMId	- Id menu
	 * @param	integer	$iId	- Id strony
	 * @return	void
	 */
	function UpdateMoveTo(&$pSmarty, $iMId, $iId) {
		global $aConfig;
		
		$bIsErr = false;
		
		if (empty($_POST)) {
			$this->Show($pSmarty, $iMId);
			return;
		}
		
		// dolaczenie klasy walidatora
		include_once('Form/Validator.class.php');
		$oValidator = new Validator();
		if (!$oValidator->Validate($_POST)) {
			$this->sMsg = GetMessage($oValidator->GetErrorString(), true);
			// wystapil blad wypelnienia formularza
			// wyswietlenie formularza wraz z komunikatem bledu
			$this->MoveTo($pSmarty, $iMId, $iId);
			return;
		}
		
		if ($_POST['menu_id'] != $_POST['old_menu_id'] ||
				$_POST['parent_id'] != $_POST['old_parent_id']) {
			// zmienil sie rodzic - aktualizacja
			$aValues = array(
				'menu_id' => (int) $_POST['menu_id'],
				'parent_id' => empty($_POST['parent_id']) ? 'NULL' : (int) $_POST['parent_id'],
				'order_by' => $this->getNextOrderBy((int) $_POST['menu_id'], (int) $_POST['parent_id']),
				'modified' => 'NOW()',
				'modified_by' => $_SESSION['user']['name']
			);
			if (Common::Update($aConfig['tabls']['prefix']."menus_items",
												 $aValues,
												 "id = ".$iId." AND
							 			 			language_id = ".$this->iLangId) === false) {
				$bIsErr = true;
			}
			
			if(!$bIsErr){
				$aChildrens=$this->getChildrens($iId);
				if(!empty($aChildrens)){
					foreach($aChildrens as $iChild){
						$aValues = array(
							'menu_id' => (int) $_POST['menu_id'],
							'modified' => 'NOW()',
							'modified_by' => $_SESSION['user']['name']
						);
						if (Common::Update($aConfig['tabls']['prefix']."menus_items",
												 $aValues,
												 "id = ".$iChild." AND
							 			 			language_id = ".$this->iLangId) === false) {
							$bIsErr = true;
						}
					}
				}
			}
		
		
			if ($bIsErr) {
				// wyswietlenie komunikatu o niepowodzeniu
				// oraz ponowne wyswietlenie formularza
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['move_to_err'],
												$this->getLinkToPath($iId, true),
												$this->getItemSymbol($iMId, $iId));
				$this->sMsg = GetMessage($sMsg);
				AddLog($sMsg);
				$this->MoveTo($pSmarty, $iMId, $iId);
			}
			else {
				Common::CommitTransaction();
				$sMsg = sprintf($aConfig['lang'][$this->sModule]['move_to_ok'],
												$this->getLinkToPath($iId, true),
												$this->getItemSymbol($_POST['menu_id'], $iId));
				$this->sMsg = GetMessage($sMsg, false);
				// dodanie informacji do logow
				AddLog($sMsg, false);
			
				$this->Show($pSmarty);
			}
		}
		else {
			$this->Show($pSmarty);
		}
	} // end of UpdateMoveTo() funciton
	
	
	/**
	 * Metoda pobiera i zwraca pelna sciezke do strony o podanym Id
	 * 
	 * @param	integer	$iLinkToId	- Id strony dla ktorej tworzona jest sciezka
	 * @param	bool	$bFullPath	- true: pelna sciezka; false: pierwszy i ostatni element 
	 */
	function getLinkToPath($iLinkToId, $bFullPath=false) {
		return getPagePath($iLinkToId, $this->iLangId, $bFullPath);
	} // end of getLinkToPath
	
	
	/**
	 * Metoda pobiera liste rekordow
	 * 
	 * @param	integer	$iPId	- Id strony
	 * @return	array ref	- lista rekordow
	 */
	function &getRecords($iMId, $iPId) {
		global $aConfig;
		
		// pobranie listy wszystkich stron do posortowania
		$sSql = "SELECT id, name
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE menu_id = ".$iMId." AND
						 			 language_id = ".$this->iLangId." AND
						 			 parent_id ".($iPId > 0 ? '= '.$iPId : 'IS NULL')."
						 ORDER BY order_by";
		return Common::GetAssoc($sSql);
	} // end of getRecords() method
	
	
	/**
	 * Metoda wprowadza zmiany w konfiguracji boxow
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iMId	ID menu do ktorego nalezy strona
	 * @param		integer	$iId	ID strony menu
	 * @return	void
	 */
	function UpdateBoxes(&$pSmarty, $iMId, $iId) {
		global $aConfig;
		
		$bIsErr = false;
		$aPages = array();
		
		Common::BeginTransaction();
				
		if (isset($_POST['recursive'])) {
			// pobranie Id wszystkich podstron
			$sSql = "SELECT IFNULL(parent_id, 0), id, mtype
							 FROM ".$aConfig['tabls']['prefix']."menus_items
							 WHERE menu_id = ".$iMId." AND
							 			 language_id = ".$this->iLangId."
							 ORDER BY parent_id, order_by";
			$aMenus =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
			if ($aMenus === false) {
				$bIsErr = true;
			}
			else {
				$aPages =& getSubpages($aMenus, $iId, '0');
			}
		}
		// dodanie na poczatku Id strony dla ktorej edytowane sa ustawienia boksow
		$aPages = array_merge(array($iId), $aPages);
		
		foreach ($aPages as $iPId) {
			foreach ($_POST['box'] as $iBoxId => $sMode) {
				if (!$bIsErr && ($iPId == $iId || isset($_POST['recursive'][$iBoxId]))) {
					// usuniecie starej konfiguracji dla strony i boksu
					$sSql = "DELETE FROM ".$aConfig['tabls']['prefix']."boxes_settings
									 WHERE page_id = ".$iPId." AND
									 			 language_id = ".$this->iLangId." AND
									 			 box_id = ".$iBoxId;
					if (Common::Query($sSql) === false) {
						$bIsErr = true;
					}
					if (!$bIsErr && $sMode != '-1') {
						// dodanie nowej informacji o ustawieniach boksu
						// tylko wtedy gdy nie jest wybrane ustawienie domyslne
						$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."boxes_settings
										 (
										 		page_id,
										 		language_id,
										 		box_id,
										 		show_mode
										 )
										 VALUES(
										 		".$iPId.",
										 		".$this->iLangId.",
										 		".$iBoxId.",
										 		'".$sMode."'
										 )";
						if (Common::Query($sSql) === false) {
							$bIsErr = true;
						}
					}
				}
			}
		}

		if ($bIsErr) {
			// wyswietlenie komunikatu o niepowodzeniu
			// oraz ponowne wyswietlenie formularza dodawania
			// wycofanie transakcji
			Common::RollbackTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['boxes_edit_err'],
											$this->getLinkToPath($iId, true));
			$this->sMsg = GetMessage($sMsg);
			AddLog($sMsg);
		}
		else {
			Common::CommitTransaction();
			$sMsg = sprintf($aConfig['lang'][$this->sModule]['boxes_edit_ok'],
											$this->getLinkToPath($iId, true));
			$this->sMsg = GetMessage($sMsg, false);
			// dodanie informacji do logow
			AddLog($sMsg, false);
		}
		$this->Show($pSmarty, $iMId);
	} // end of UpdateBoxes() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji ustawien boksow strony i jej
	 * ewentualnych podstron
	 *
	 * @param		object	$pSmarty
	 * @param		integer	$iMId	ID menu do ktorego nalezy strona
	 * @param		integer	$iId	ID strony menu
	 * 
	 */
	function BoxesSettings(&$pSmarty, $iMId, $iId) {
		global $aConfig;
		$aData = array();
		$sHtml = '';
		
		if (!empty($_POST)) {
			$aData =& $_POST;
		}
		
		// dolaczenie klasy FormTable
		include_once('Form/FormTable.class.php');
		
		// pobranie obszarow serwisu bez obszaru strony glownej
		$sSql = "SELECT id, symbol
						 FROM ".$aConfig['tabls']['prefix']."areas
						 ORDER BY id";
		$aAreas =& Common::GetAll($sSql);
		
		// pobranie domyslnych ustawien boksow
		$sSql = "SELECT area_id, id, name, subpage
						 FROM ".$aConfig['tabls']['prefix']."boxes
						 WHERE language_id = ".$this->iLangId." AND
						 			 subpage != '0'
						 			 ORDER BY area_id, order_by";
		$aBoxes =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
	
		// pobranie ustawien zdefiniowanych dla strony
		$sSql = "SELECT box_id, show_mode
						 FROM ".$aConfig['tabls']['prefix']."boxes_settings
						 WHERE language_id = ".$this->iLangId." AND
						 			 page_id = ".$iId;
		$aBoxesSettings =& Common::GetAssoc($sSql);
		
		$sHeader = $aConfig['lang'][$this->sModule]['boxes_header'].
							 ' "'.getPagePath($iId, $this->iLangId).'"';
		
		$pForm = new FormTable('boxes_settings', $sHeader, array('action'=>phpSelf(array('mid' => $iMId, 'id' => $iId))), array('col_width'=>175), $aConfig['common']['js_validation']);
		
		$pForm->AddHidden('do', 'update_boxes');
		
		// wyswietlanie blokow
		$aModes = array(
			array('value' => '-1', 'label'	=> $aConfig['lang'][$this->sModule]['show_mode_-1']),
			array('value' => '0', 'label'	=> $aConfig['lang'][$this->sModule]['show_mode_0']),
			array('value' => '1', 'label'	=> $aConfig['lang'][$this->sModule]['show_mode_1']),
			array('value' => '2', 'label'	=> $aConfig['lang'][$this->sModule]['show_mode_2']),
			array('value' => '3', 'label'	=> $aConfig['lang'][$this->sModule]['show_mode_3'])
		);
		
		foreach ($aAreas as $aArea) {
			if (isset($aBoxes[$aArea['id']])) {
				if (isset($aConfig['lang']['c_areas'][$aArea['symbol']])) {
					$aArea['symbol'] = $aConfig['lang']['c_areas'][$aArea['symbol']];
				}
				$pForm->AddMergedRow($aArea['symbol'], array('class'=>'merged', 'style'=>'padding-left: 190px'));
				foreach ($aBoxes[$aArea['id']] as $aBox) {
					// dodanie informacji jakie jest domyslne ustawienie
					// boksu
					$aModes[0]['label'] = $aConfig['lang'][$this->sModule]['show_mode_-1'].' - '.
																$aConfig['lang'][$this->sModule]['show_mode_'.
																																	$aBox['subpage']];
					if (!isset($aData['box'][$aBox['id']])) {
						if (isset($aBoxesSettings[$aBox['id']])) {
							$aData['box'][$aBox['id']] = $aBoxesSettings[$aBox['id']];
						}
						else {
							$aData['box'][$aBox['id']] = '-1';
						}
					}
					$sLabel = $pForm->GetLabelHTML('box['.$aBox['id'].']', $aBox['name'], false);
					$sSelect = $pForm->GetSelectHTML('box['.$aBox['id'].']', '', array(), $aModes, $aData['box'][$aBox['id']], '', false);
					
					// jezeli strona posiada podstrony wyswietlenie checkboxow
					// z mozliwoscia zastosowania zmian do podstron
					if ($this->hasChildren($iMId, $iId)) {
						$sCheckboxLabel = $pForm->GetLabelHTML('recursive['.$aBox['id'].']', $aConfig['lang'][$this->sModule]['recursive'], false);
						$sCheckbox = $pForm->GetCheckBoxHTML('recursive['.$aBox['id'].']', '', array(), '', isset($aData['recursive'][$aBox['id']]), false);
					}
					else {
						$sCheckboxLabel = '';
						$sCheckbox = '';
					}
					
					$pForm->AddRow($sLabel, $sSelect.'&nbsp;&nbsp;'.$sCheckboxLabel.'&nbsp;'.$sCheckbox);
				}
			}
		}
		
		$pForm->AddRow('&nbsp;', $pForm->GetInputButtonHTML('send', $aConfig['lang']['common']['button_1']).'&nbsp;&nbsp;'.$pForm->GetInputButtonHTML('cancel', $aConfig['lang']['common']['cancel'], array('onclick'=>'MenuNavigate(\''.phpSelf(array(), array('id')).'\');'), 'button'));
		
		$pSmarty->assign('sContent', (!empty($this->sMsg) ? $this->sMsg : '').ShowTable($pForm->ShowForm()));
	} // end of BoxesSettings() function
	
	
	/**
	 * Metoda pobiera i zwraca w postaci tablicy identyfikatory
	 * linkujacych stron do strony o podanym Id
	 * 
	 * @return	array
	 */
	function getLinkingPages($iId) {
		global $aConfig;
		
		$sSql = "SELECT id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE link_to_id = ".$iId." AND
						 			 language_id = ".$this->iLangId;
		return Common::GetAll($sSql);
	} // end of getLinkingPages() method
	
	
	/**
	 * Metoda zapisuje strukture drzewa stron wybranego menu
	 * do pliku XML
	 * 
	 * @param	integer	$iMId	- Id menu
	 * @return	bool	- true: zapisano; false: wystapil blad
	 */
	function writeMenuToXML($iMId) {
		global $aConfig;
		
		// pobranie z bazy danych listy wszystkich elementow menu
		$sSql = "SELECT IFNULL(parent_id, 0), id, name, symbol, mtype, description,
										url, new_window, link_to_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE menu = '1' AND
						 			 menu_id = ".$iMId." AND
						 			 language_id = ".$this->iLangId."
						 			 ORDER BY parent_id, order_by";
		$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
		
		// utworzenie drzewa menu
		if ($_SESSION['lang']['default'] === '0') {
			$aMenuItems =& getMenuTreeForXML($aItems, 0, $_SESSION['lang']['symbol']);
		}
		else {
			$aMenuItems =& getMenuTreeForXML($aItems);
		}
		
		// dolaczenie klasy XML/Serializer
		require_once ("XML/Serializer.php");
		// Serializer options
		$aOptions = array(
			'addDecl'	=> true,
			'encoding'	=> 'utf-8',
			'indent'	=>	"\t",
			'rootName'	=>	'menu',
			'defaultTagName'	=>	'item'
		);
		
		$oSerializer = new XML_Serializer($aOptions);
		$oResult = $oSerializer->serialize($aMenuItems);
		
		if (PEAR::isError($oResult)) {
			if ($aConfig['common']['status'] == 'development') {
				TriggerError($status->getMessage().'<br />'.$pResult->getDebugInfo()." ".$_SERVER['SCRIPT_FILENAME'], E_USER_NOTICE);
			}
			return false;
		}
		
		if (!file_exists($this->sXMLDir)) {
			// utworzenie katalogu
			if (!mkdir($this->sXMLDir, 0755)) {
				return false;
			}
		}
		// katalog wersji jezykowej
		$this->sXMLDir .= '/'.$_SESSION['lang']['symbol'];
		if (!file_exists($this->sXMLDir)) {
			// utworzenie katalogu
			if (!mkdir($this->sXMLDir, 0755)) {
				return false;
			}
		}
		
//		// zapis do pliku XML
//		if ($oFile = fopen($this->sXMLDir.'/menu_'.$iMId.'.xml', 'w')) {
//			flock($oFile, 2);
//			if (fwrite($oFile, $oSerializer->getSerializedData())) {
//				flock($oFile, 3);
//				fclose($oFile);
//				return true;
//			}
//		}
		return true;
	} // end of writeMenuToXML() method
	
	
	/**
	 * Metoda sprawdza czy strona o podanym symbolu istnieje dla danej wersji jez.
	 *
	 * @param		string	$sSymbol	- symbol modulu
	 * @param		integer	$iId	- ID strony dla ktorej pomijane jest sprawdzanie
	 * @return	bool	- true: istnieje; false: nie istnieje
	 */
	function itemExists($sSymbol, $iId=0) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE symbol='".$sSymbol."' AND
						 			 language_id = ".$this->iLangId.
									 ($iId > 0 ? " AND id!=".$iId : '');
		
		return intval(Common::GetOne($sSql)) == 1;
	} // end of ItemExists() function


	/**
	 * Metoda sprawdza czy istnieja zdefiniowane w serwisie boksy
	 * odwolujace sie do strony o Id $iId
	 *
	 * @param		integer	$iId	- ID strony
	 * @return	bool	- true: istnieja; false: nie istnieja
	 */
	function linkingBoxesExist($iId) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."boxes
						 WHERE page_id = ".$iId." AND
						 			 language_id = ".$this->iLangId;
		
		return intval(Common::GetOne($sSql)) == 1;
	} // end of linkingBoxesExist() function
	
	
	/**
	 * Metoda sprawdza czy strona o podanym id posiada podstrony
	 *
	 * @param		integer	$iMId	- ID menu strony
	 * @param		integer	$iId	- ID strony
	 * @return	bool	- true: posiada; false: nie posiada
	 */
	function hasChildren($iMId, $iId) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE parent_id = ".$iId." AND
						 			 language_id = ".$this->iLangId." AND
									 menu_id = ".$iMId;
		
		return intval(Common::GetOne($sSql)) >= 1;
	} // end of hasChildren() function
	
	
	/**
	 * Metoda sprawdza czy menu o podanym Id jest menu Oferty Produktowej
	 * 
	 * @param	integer	$iId	- Id menu
	 * @return	bool
	 */
	function isProductsMenu($iId) {
		global $aConfig;
		
		$sSql = "SELECT products
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id = ".$this->iLangId." AND
						 			 id = ".$iId;
		return Common::GetOne($sSql) == '1';
	} // end of isProductsMenu() method
	
	
	
	/**
	 * Metoda sprawdza czy istnieja zdefiniowane boksy
	 *
	 * @return	bool	- true: istnieja; false: nie istnieja
	 */
	function boxesExist() {
		global $aConfig;
		
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."boxes
						 WHERE language_id = ".$this->iLangId;
		
		return intval(Common::GetOne($sSql)) > 0;
	} // end of boxesExist() function
	
	
	/**
	 * Metoda pobiera liste opcji modulu na podstawie plikow php z klasami
	 * z katalogu client modulu o przekazanym do metody symbolu
	 * 
	 * @param	string	$sModule	- symbol modulu
	 */
	function &getModuleOptions($sModule) {
		global $aConfig;
		$aOptions = array();
		$sDir = $aConfig['common']['base_path'].'modules/'.$sModule.'/client';
		if (is_dir($sDir)) {
			$pDir = opendir($sDir);
			$i = 0;
	  	while (false !== ($sFile = readdir($pDir))) {
	    	if (preg_match('/^Module_\w+\.class.php$/i', $sFile)) {
	    		$sFile = substr($sFile,
	    										strpos($sFile, '_') + 1,
	    										strpos($sFile, '.') - strpos($sFile, '_') - 1);
	    		
	    		$aOptions[$i] = array('value' => $sFile,
	    													'label' => $sFile
	    												 );
	      	if (isset($aConfig['lang']['c_modules'][$sModule.'_'.$sFile])) {
						$aOptions[$i]['label'] = $aConfig['lang']['c_modules'][$sModule.'_'.$sFile]; 
					}
					$i++;
	      }
	    }
	    closedir($pDir);
	    
	    if (!empty($aOptions)) {
	    	$aOptions = array_merge(
	    		array(array('value' => '', 'label' => $aConfig['lang']['c_modules']['default'])),
	    		$aOptions
	    	);
	    }
		}
    return $aOptions;
	} // end of getModuleOptions() method
	
	
	/**
	 * Metoda pobiera Id modulow ktorych strony moze tworzyc
	 * 
	 * @param	integer	$iSOModId	- Id odulu Oferty produktowej do wykluczenia
	 * @return	array ref	- lista Id modulow
	 */
	function &getUserModules($iSOModId=0) {
		global $aConfig;
		
		$sSql = "SELECT module_id
						 FROM ".$aConfig['tabls']['prefix']."umodules
						 WHERE user_id = ".$_SESSION['user']['id'].
						 			 ($iSOModId > 0 ? " AND module_id <> ".$iSOModId : '');
		return Common::GetCol($sSql);
	} // end of getUserModules() method
	
	

	
	
	/**
	 * Metoda sprawdza czy strona o podanym Id posiada podstrony do posortowania > 1
	 * 
	 * @param	array ref	$aItems	- talica z elementami
	 * @param	integer	$iId	- Id strony
	 * @return	bool	- true: posiada; false: nieposiada
	 */
	function hasSubpagesToSort(&$aItems, $iId) {
		return isset($aItems[$iId]) && count($aItems[$iId]) > 1;
	} // end of hasSubpagesToSort() method
	
	
	/**
	 * Rekurencyjna metoda sprawdzajaca czy uzytkownik posiada prawa usuniecia
	 * calej galezi strony
	 * 
	 * @param	array ref	$aItems	- tablica zawierajaca wszystkie strony menu
	 * @param	array ref	$aBranch	- podstrony sprawdzanej strony
	 * @return	bool	- true: mozna usunac; false: nie mozna usunac
	 */
	function canDeleteBranch(&$aItems, &$aBranch) {
		foreach ($aBranch as $aItem) {
			if (!canCreate($this->aPrivileges['pages'][$aItem['id']])) {
				return false;
			}
			if (isset($aItems[$aItem['id']])) {
				return $this->canDeleteBranch($aItems, $aItems[$aItem['id']]);
			}
		}
		return true;
	} // end of canDeleteBranch() method
	
	
	/**
	 * Metoda sprawdza czy strona o podanym Id moze posiadac jeszcze podstrony
	 * (czy jej poziom zaglebienia nie osiagnal poziomu maksymalnego)
	 * 
	 * @param	integer	$iid	- Id strony
	 * @param	integer	$iMaxLevels	
	 */
	function canHaveChildren($iId, $iMaxLevels) {
		return count(getPathItems($iId, $this->iLangId)) <= $iMaxLevels;
	} // end of canHaveChildren() method 
	
	
	/**
	 * Metoda pbiera i zwraca nazwe menu o podanym Id
	 * 
	 * @param	integer	$iId	- Id menu
	 * @return	string	- nazwa menu
	 */
	function getMenuName($iId) {
		global $aConfig;
		
		$sSql = "SELECT name
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE language_id = ".$this->iLangId." AND
						 			 id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getMenuName() method
	
	
	/**
	 * Metoda pbiera i zwraca nazwe uzytkownika tworzacego rekord
	 * 
	 * @param	integer	$iId	- Id strony
	 * @return	string	- nazwa uzytkownika
	 */
	function getCreatorName($iId) {
		global $aConfig;
		
		$sSql = "SELECT created_by
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$this->iLangId." AND
						 			 id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getCreatorName() method
	
	
	/**
	 * Metoda pbiera i zwraca nazwe lub symbol modulu
	 * 
	 * @param	integer	$iId	- Id modulu
	 * @return	string	- nazwa lub symbol modulu
	 */
	function getModuleName($iId) {
		global $aConfig;
		
		$sSql = "SELECT symbol
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE id = ".$iId;
		$sSymbol = Common::GetOne($sSql);
		
		return isset($aConfig['lang']['c_modules'][$sSymbol]) ? $aConfig['lang']['c_modules'][$sSymbol] : $sSymbol;
	} // end of getModuleName() method
	
	
	/**
	 * Metoda pbiera i zwraca sybmbol strony
	 * 
	 * @param	integer	$iMId	- Id menu strony
	 * @param	integer	$iId	- Id strony
	 * @return	string	- nazwa strony
	 */
	function getItemSymbol($iMId, $iId) {
		global $aConfig;
		
		$sSql = "SELECT symbol
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$this->iLangId." AND
						 			 menu_id = ".$iMId." AND
						 			 id = ".$iId;
		return Common::GetOne($sSql);
	} // end of getItemSymbol() method
	
	
	/**
	 * Metoda pbiera i zwraca Id rodzica strony
	 * 
	 * @param	integer	$iMId	- Id menu strony
	 * @param	integer	$iId	- Id strony
	 * @return	integer Id rodzica
	 */
	function getItemParentId($iMId, $iId) {
		global $aConfig;
		
		$sSql = "SELECT parent_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$this->iLangId." AND
						 			 menu_id = ".$iMId." AND
						 			 id = ".$iId;
		return (int) Common::GetOne($sSql);
	} // end of getItemParentId() method
	
	
	/**
	 * Metoda pbiera i zwraca kolejny numer porzadkowy dla menu i rodzica
	 * 
	 * @param	integer	$iMId	- Id menu strony
	 * @param	integer	$iId	- Id strony rodzica
	 * @return	string	- nazwa strony
	 */
	function getNextOrderBy($iMId, $iId) {
		global $aConfig;
		
		$sSql = "SELECT IFNULL(MAX(order_by), 0) + 1
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$this->iLangId." AND
						 			 menu_id = ".$iMId." AND
						 			 parent_id ".(empty($iId) ? "IS NULL" : "= ".$iId);
		return (int) Common::GetOne($sSql);
	} // end of getNextOrderBy() method
	

	/**
	 * Metoda sprawdza czy dla jednej wersji jezykowej moze istniec tylko jedna
	 * strona modulu o podanym Id $iId
	 * 
	 * @param	integer	$iId	- Id modulu
	 * @return	- true: moze byc tylko jedna; false: moze byc wiele
	 */
	function canBeOnlyOne($iId) {
		global $aConfig;
		
		$sSql = "SELECT one_per_lang
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE id = ".$iId;
		return Common::GetOne($sSql) == '1';
	} // end of canbeOnlyOne() method

	
	/**
	 * Metoda sprawdza czy dla danej wersji jezykowej istnieje juz strona
	 * modulu o ID $iId
	 * 
	 * @param	integer	$iId	- Id modulu
	 * @param	string	$sOption	- opcja modulu
	 * @param	integer	$iPId	- Id strony dla ktorej pomijane jest sprawdzanie
	 * @return	- true: strona modulu istnieje; false: strona modulu nie istnieje
	 */
	function modulePageExists($iId, $sOption='', $iPId=0) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(*)
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$this->iLangId." AND
						 			 module_id IS NOT NULL AND
						 			 module_id = ".$iId." AND
						 			 moption ".($sOption != '' ? "= '".$sOption."'": ' IS NULL').
						 			 ($iPId > 0 ? " AND id <> ".$iPId : '');
		return intval(Common::GetOne($sSql)) > 0;
	} // end of modulePageExists() method
	
	/**
	 * Metoda pobiera liste modulow ktorych strony moze utworzyc
	 * zalogowany uzytkownik
	 * 
	 * @param	array ref	$aSOMIds	- Id modulow ktorych strony moga byc
	 * 															tworzone w menu Oferty produktowej
	 * @param	integer	$iCurrModId	- Id modulu aktualnie edytowanej strony
	 * @param	integer	$iModId	- Id wybranego aktualnie modulu
	 * @param	bool	$bProductsMenu	- czy menu Oferty produktowej
	 * @param	object ref	$pForm	- obiekt formularza
	 * @return	array ref
	 */
	function &getAvailModulesList(&$aSOMIds, $iCurrModId, $iModId, $bProductsMenu, &$pForm) {
		global $aConfig;
		$sModsSql = '';
		$sUserAccessSql = '';
		$iSOModId = getModuleId('m_oferta_produktowa');
		$aData = array();
		
		if ($_SESSION['user']['type'] === 0) {
			// zwykly uzytkownik - okreslenie jakich modulow strony moze tworzyc
			$aMIDs = $this->getUserModules();
			if ($iCurrModId > 0) {
				// jezeli edycja strony - dodanie do mozliwych modulow modulu
				// edytownej strony - na wypadek gdyby uzytkownik nie mial
				// praw tworzenia stron danego modulu
				$aMIDs[] = $iCurrModId;
			}
			if ($bProductsMenu) {
				$aMods = array_intersect($aSOMIds, $aMIDs);
			}
			else {
				$aMods = array_diff($aMIDs, $aSOMIds);
			}
			$sModsSql = " AND id IN(".(!empty($aMods) ? implode(',', $aMods) : 0).")";
		}
		else {
			if ($bProductsMenu) {
				if (!empty($aSOMIds)) {
					$sModsSql = " AND id IN(".implode(',', $aSOMIds).")";
				}
			}
			else {
				if (!empty($aSOMIds)) {
					$sModsSql = " AND id NOT IN(".implode(',', $aSOMIds).")";
				}
			}
		}
		
		// pobranie modulow ktorych typem moze byc strona
		$sSql = "SELECT id, id AS value, symbol AS label, default_module
						 FROM ".$aConfig['tabls']['prefix']."modules
						 WHERE menu = '1'".$sModsSql."
						 ORDER by symbol";
		$aData['modules'] =& Common::GetAssoc($sSql);
		if ($aData['modules'] !== false && !empty($aData['modules'])) {
			foreach ($aData['modules'] as $iKey => $aModule) {
				$aModulesSymbols[$iKey] = $aData['modules'][$iKey]['label'];
				$aData['modules'][$iKey]['label'] = $aConfig['lang']['c_modules'][$aModule['label']];
				if ($iModId == 0 && $aModule['default_module'] == '1') {
					$aData['module_id'] = $aModule['value'];
				}
				unset($aData['modules'][$iKey]['default_module']);
			}
			if (isset($aData['module_id']) || $iModId > 0) {
				$aData['module'] = $aModulesSymbols[isset($aData['module_id']) ? $aData['module_id'] : $iModId];
				$pForm->AddHidden('module', $aData['module']);
			}
		}
		else {
			$aData['modules'] = array();
		}
		if (!empty($aSOMIds) && $iModId == 0 && !isset($aData['module_id'])) {
			// ustawienie jako zaznaczonego modulu Oferty produktowej
			$aData['module_id'] = $iSOModId;
			$aData['module'] = 'm_oferta_produktowa';
			$pForm->AddHidden('module', $aData['module']);
		}
		$aData['modules'] = array_merge(array(array('value' => '', 'label' => $aConfig['lang']['common']['choose'])), $aData['modules']);
		return $aData;
	} // end getAvailModulesList() method
	
	
	/**
	 * Metoda wykonuje operacje na zdjeciach akapitu
	 * 
	 * @param	object	$pSmarty
	 * @param	integer	$iPId
	 * @param	integer	$iId
	 * 
	 * @return	integer
	 */
	function proceedImage($iPId) {
		// dolaczenie klasy Images
		include_once('Images.class.php');
		$oImages = new Images(						0,
													$iPId,
													$this->aSettings,
													$this->iLangId.'/'.$iPId,
													'page_id',
													'pages_images');
		return $oImages->proceed(true, false);
	} // end of proceedImage() method
	
	
	
		/**
	 * Metoda sprawdza czy strona o podanym id posiada podstrony
	 *
	 * @param		integer	$iMId	- ID menu strony
	 * @param		integer	$iId	- ID strony
	 * @return	bool	- true: posiada; false: nie posiada
	 */
	function hasOldChildren($iId) {
		global $aConfig;
		
		$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE parent_id = ".$iId." AND
						 			 language_id = ".$this->iLangId;
		
		return intval(Common::GetOne($sSql)) >= 1;
	} // end of hasChildren() function
	
	/**
	 * pobiera listę id dzieci elementu menu
	 * @param $iCatId
	 * @return unknown_type
	 */
	function getChildrens($iCatId) {
		global $aConfig;
		$aCategories = array();
		
		// pobranie kategorii
		$sSql = "SELECT id
				 FROM ".$aConfig['tabls']['prefix']."menus_items
				 WHERE parent_id = ".$iCatId." AND
						 			 language_id = ".$this->iLangId."
				 ORDER BY id";
		$aCats =& Common::GetCol($sSql);

		if(!empty($aCats)){
			foreach ($aCats as $iCat) {
				if ($this->hasOldChildren($iCat)) {
					$aCategories[] = $iCat;
					$aSubCategories = $this->getChildrens($iCat);
					if (!empty($aSubCategories)) {
						$aCategories = array_merge($aCategories, $aSubCategories);
					}
				}
				else {
					$aCategories[] = $iCat;
				}
			}
		}
		return $aCategories;
	}
	
	
	
	
	
} // end of Module Class
?>