<?php
/**
* Plik jezykowy dla interfejsu modulu 'Struktura menu' Panelu Administracyjnego
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['m_menu']['list'] = "Lista menu";
$aConfig['lang']['m_menu']['name'] = "Nazwaasfas";
$aConfig['lang']['m_menu']['max_levels_list'] = "Zagłębienia";
$aConfig['lang']['m_menu']['created']	= "Utworzono";
$aConfig['lang']['m_menu']['created_by']	= "Utworzył";
$aConfig['lang']['m_menu']['no_items'] = "Brak zdefiniowanych menu";
$aConfig['lang']['m_menu']['menu_items'] = "Pokaż strony menu";
$aConfig['lang']['m_menu']['edit'] = "Edytuj menu";
$aConfig['lang']['m_menu']['delete'] = "Usuń menu";
$aConfig['lang']['m_menu']['delete_q'] = "Czy na pewno usunąć wybrane menu?";
$aConfig['lang']['m_menu']['check_all'] = "Zaznacz / odznacz wszystkie";
$aConfig['lang']['m_menu']['delete_all'] = "Usuń zaznaczone";
$aConfig['lang']['m_menu']['delete_all_q'] = "Czy na pewno usunąć wybrane menu?";
$aConfig['lang']['m_menu']['delete_all_err'] = "Nie wybrano żadnego menu do usunięcia!";
$aConfig['lang']['m_menu']['add'] = "Dodaj menu";
$aConfig['lang']['m_menu']['add_ok'] = "Menu \"%s\" zostało dodane do bazy danych";
$aConfig['lang']['m_menu']['add_err'] = "Nie udało się dodać menu do bazy danych!<br>Spróbuj ponownie";
$aConfig['lang']['m_menu']['edit_ok'] = "Zmiany w ustawieniach wybranego menu \"%s\" zostały wprowadzone do bazy danych";
$aConfig['lang']['m_menu']['edit_err'] = "Nie udało się wprowadzić zmian w ustawieniach wybranego menu \"%s\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_menu']['del_ok_0']		= "Menu %s zostało usunięty";
$aConfig['lang']['m_menu']['del_ok_1']		= "Menu %s zostały usunięte";
$aConfig['lang']['m_menu']['del_err_0']		= "Nie udało się usunąć menu %s!<br>Spróbuj ponownie";
$aConfig['lang']['m_menu']['del_err_1']		= "Nie udało się usunąć menu %s!<br>Spróbuj ponownie";

$aConfig['lang']['m_menu']['header_0'] = "Dodawanie menu";
$aConfig['lang']['m_menu']['header_1'] = "Edycja menu";
$aConfig['lang']['m_menu']['name'] = "Nazwa";
$aConfig['lang']['m_menu']['max_levels'] = "Maksymalny poziom zagłębienia menu";
$aConfig['lang']['m_menu']['products'] = "Menu oferty produktowej";

$aConfig['lang']['m_menu']['sort'] = "Sortuj";
$aConfig['lang']['m_menu']['sort_items'] = "Sortowanie menu";


/*---------- ustawienia */
$aConfig['lang']['m_menu_config']['header'] = "Konfiguracja domyślna stron modułu \"".$aConfig['lang']['c_modules']['m_menu']."\"";
$aConfig['lang']['m_menu_config']['edit_err'] = "Wystąpił błąd podczas aktualizacji domyślnej konfiguracji modułu \"".$aConfig['lang']['c_modules']['m_menu']."\"";
$aConfig['lang']['m_menu_config']['edit_ok'] = "Zaktualizowano domyślną konfigurację modułu \"".$aConfig['lang']['c_modules']['m_menu']."\"";

$aConfig['lang']['m_menu_settings']['photo_size'] = "Rozmiar zdjęcia strony";


/************ konfiguracja boxu menu */
$aConfig['lang']['m_menu_box_settings']['expand_levels'] = "Rozwiń do poziomu";
$aConfig['lang']['m_menu_box_settings']['expand_from_level'] = "Zacznij od poziomu";
$aConfig['lang']['m_menu_box_settings']['expand_first_level'] = "Rozwiń pierwszy poziom";
$aConfig['lang']['m_menu_box_settings']['menu'] = "Menu";

/************ elementy menu */
$aConfig['lang']['m_menu_items']['menu_list'] = "Lista menu";
$aConfig['lang']['m_menu_items']['menu_name'] = "Nazwa menu";

$aConfig['lang']['m_menu_items']['list'] = "Lista stron menu";
$aConfig['lang']['m_menu_items']['name'] = "Nazwa";
$aConfig['lang']['m_menu_items']['symbol'] = "Symbol";

$aConfig['lang']['m_menu_items']['menus_go_back'] = "Powrót do listy menu";
$aConfig['lang']['m_menu_items']['link'] = "Link: ";
$aConfig['lang']['m_menu_items']['no_items'] = "Brak zdefiniowanych stron menu";
$aConfig['lang']['m_menu_items']['boxes'] = "ustawienia boksów";
$aConfig['lang']['m_menu_items']['publish'] = "Zmień stan publikacji strony";
$aConfig['lang']['m_menu_items']['sort'] = "sortuj podstrony";
$aConfig['lang']['m_menu_items']['preview'] = "Podgląd strony";
$aConfig['lang']['m_menu_items']['edit'] = "Edytuj stronę";
$aConfig['lang']['m_menu_items']['delete'] = "Usuń stronę";
$aConfig['lang']['m_menu_items']['delete_q'] = "Czy na pewno usunąć wybraną stronę menu?";
$aConfig['lang']['m_menu_items']['move_to'] = "Przenieś stronę";
$aConfig['lang']['m_menu_items']['add'] = "Dodaj stronę menu";
$aConfig['lang']['m_menu_items']['add_ok'] = "Dodano stronę \"%s (%s)\"";
$aConfig['lang']['m_menu_items']['add_err'] = "Wystąpił błąd podczas próby dodania strony \"%s (%s)\" do \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_menu_items']['add_err2'] = "Strona o symbolu \"%s\" już istnieje!<br>Wybierz inny symbol";
$aConfig['lang']['m_menu_items']['add_err3'] = "Nie można zmienić typu strony dla której istnieją zdefiniowane boksy!<br><br>Usuń odwołujące się do strony boksy a następnie zmień typ strony.";
$aConfig['lang']['m_menu_items']['add_err4'] = "Strona tego modułu już istnieje!<br>Może istnieć tylko jedna strona modułu \"%s\" w wersji językowej.";
$aConfig['lang']['m_menu_items']['add_err5'] = "Strona nie może linkować do samej siebie!";
$aConfig['lang']['m_menu_items']['edit_ok'] = "Zmieniono ustawienia strony \"%s (%s)\"";
$aConfig['lang']['m_menu_items']['edit_err'] = "Wystąpił błąd podczas próby zmiany ustawień strony \"%s (%s)\"!<br><br>Spróbuj ponownie";
$aConfig['lang']['m_menu_items']['del_ok']		= "Usunieto stronę \"%s (%s)\"";
$aConfig['lang']['m_menu_items']['del_err']		= "Wystąpił błąd podczas próby usunięcia strony \"%s (%s)\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_menu_items']['published_0'] = "nieopublikowana";
$aConfig['lang']['m_menu_items']['published_1'] = "opublikowana";
$aConfig['lang']['m_menu_items']['publ_ok']		= "Zmieniono stan publikacji strony \"%s (%s)\" na \"%s\"";
$aConfig['lang']['m_menu_items']['publ_err']		= "Wystąpił błąd podczas próby zmiany stanu publikacji strony \"%s (%s)\" na \"%s\"!<br>Spróbuj ponownie";
$aConfig['lang']['m_menu_items']['publ_no'] = "Nieopubl.";
$aConfig['lang']['m_menu_items']['publ_yes'] = "Opubl.";
$aConfig['lang']['m_menu_items']['move_to_ok'] = "Przeniesiono stronę \"%s (%s)\"";
$aConfig['lang']['m_menu_items']['move_to_err'] = "Wystąpił błąd podczas próby przeniesienia strony \"%s (%s)\"!<br><br>Spróbuj ponownie";

$aConfig['lang']['m_menu_items']['header_0'] = "Dodawanie strony menu";
$aConfig['lang']['m_menu_items']['header_1'] = "Edycja strony menu";
$aConfig['lang']['m_menu_items']['header_move_to'] = "Przenoszenie strony %s";
$aConfig['lang']['m_menu_items']['parent'] = "Położenie strony";
$aConfig['lang']['m_menu_items']['mtype'] = "Typ strony";
$aConfig['lang']['m_menu_items']['type_0'] = "Strona modułu";
$aConfig['lang']['m_menu_items']['type_1'] = "Link wewnętrzny";
$aConfig['lang']['m_menu_items']['type_2'] = "Link zewnętrzny";
$aConfig['lang']['m_menu_items']['name'] = "Nazwa";
$aConfig['lang']['m_menu_items']['symbol'] = "Symbol ([a-z0-9_-] min. 3 znaki)";
$aConfig['lang']['m_menu_items']['synchronize'] = "Synchronizuj z nazwą";
$aConfig['lang']['m_menu_items']['synchronize_now'] = "Synchronizuj teraz";
$aConfig['lang']['m_menu_items']['description'] = "Krótki opis strony";
$aConfig['lang']['m_menu_items']['seo_title'] = "Tytuł strony";
$aConfig['lang']['m_menu_items']['seo_keywords'] = "Słowa kluczowe";
$aConfig['lang']['m_menu_items']['image'] = "Zdjęcie strony";
$aConfig['lang']['m_menu_items']['menu_id'] = "W menu";
$aConfig['lang']['m_menu_items']['parent_id'] = "Jako podstrona";
$aConfig['lang']['m_menu_items']['main_menu'] = "-- Główna strona --";
$aConfig['lang']['m_menu_items']['module_id'] = "Moduł strony";
$aConfig['lang']['m_menu_items']['moption'] = "Opcja modułu";
$aConfig['lang']['m_menu_items']['menu'] = "Widoczna w menu";
$aConfig['lang']['m_menu_items']['link_to_menu_id'] = "Wskazuje w menu";
$aConfig['lang']['m_menu_items']['link_to_id'] = "Na stronę";
$aConfig['lang']['m_menu_items']['url'] = "Adres URL";
$aConfig['lang']['m_menu_items']['new_window'] = "Otwieraj w nowym oknie";
$aConfig['lang']['m_menu_items']['restricted'] = "Tylko dla zalogowanych";
$aConfig['lang']['m_menu_items']['published'] = "Opublikowana";
$aConfig['lang']['m_menu_items']['page_template'] = "Główny szablon strony";

$aConfig['lang']['m_menu_items']['module_change_warning'] = "Jeżeli zmienisz moduł strony, wszystkie jej dotychczasowe dane (aktualności, strony opisowe, itp.) zostaną BEZPOWROTNIE usunięte!";


$aConfig['lang']['m_menu_items']['sort_items'] = "Sortowanie stron: \"%s\"";
$aConfig['lang']['m_menu_items']['go_back'] = "Powrót do listy stron";

/************ konfiguracja boksow */
$aConfig['lang']['m_menu_items']['boxes_header'] = "Edycja ustawień boksów strony";
$aConfig['lang']['m_menu_items']['show_mode_-1'] = "Domyślnie";
$aConfig['lang']['m_menu_items']['show_mode_0'] = "Nie wyświetlaj";
$aConfig['lang']['m_menu_items']['show_mode_4'] = "Nie wyświetlaj - listuj w podstronach";
$aConfig['lang']['m_menu_items']['show_mode_1'] = "Wyświetlaj";
$aConfig['lang']['m_menu_items']['show_mode_2'] = "Nie wyświetlaj gdy ID";
$aConfig['lang']['m_menu_items']['show_mode_3'] = "Wyświetlaj gdy ID";
$aConfig['lang']['m_menu_items']['recursive'] = "zastosuj do podstron";
$aConfig['lang']['m_menu_items']['boxes_edit_ok'] = "Ustawienia boksów strony \"%s\" zostały zmienione";
$aConfig['lang']['m_menu_items']['boxes_edit_err'] = "Nie udało się zmienić ustawień boksów strony \"%s\"!<br><br>Spróbuj ponownie";


/***************** priorytety kategorii ***************************/
$aConfig['lang']['m_menu_cats_priority']['sort'] = "Sortuj priorytety kategorii";
$aConfig['lang']['m_menu_cats_priority']['sort_items'] = "Sortowanie priorytetów kategorii";
$aConfig['lang']['m_menu_cats_priority']['go_back'] = "Powrót";
$aConfig['lang']['m_menu_cats_priority']['name'] = "Kategoria";

?>