<?php
/**
 * Klasa Module do obslugi modulu 'Autorzy' - role autorow
 */

class Module
{
	private $pSmarty;

	/**
	 * Konstruktor klasy
	 *
	 * @return    void
	 */
	function Module(&$pSmarty)
	{
		global $aConfig;

		$this->sErrMsg = '';
		$this->iLangId = $_SESSION['lang']['id'];
		$this->iPageId = getModulePageId('m_autorzy');
		$this->sModule = $_GET['module'];
		$this->pSmarty = $pSmarty;

		$sDo = $_GET['do'];

		switch ($sDo) {

			default:
				$this->rebuildButtonAction();
				break;
		}
	}

	public function rebuildButtonAction()
	{
		$info = '';

		if (isset($_POST['save'])) {

			$info .= '<div
				style="
				    text-align: left;
					color: green;
					margin: 12px 0 12px 204px;
				"
			>Odbudowa została uruchomiona - proces odbywa się w tle</div>';

			exec('/opt/php-5.4.45/bin/php -f /var/www/vhosts/profit24.pl/httpdocs/LIB/PriceUpdater/ProductMainCategoryUpdater.php >> /var/www/vhosts/profit24.pl/import_logi/ProductMainCategoryUpdater.txt &');
		}

		include_once('Form/FormTable.class.php');
		$form = new FormTable('form');
		$form->AddInputButton('save', 'Odbuduj główne kategorie ceneo', [
			'style' => 'font-size:24px'
		]);

		$info .= '<div
				style="
				    text-align: left;
					color: blue;
					margin: 12px 0 12px 204px;
				"
			>Po wcisnięciu przycisku rozpocznie się proces odbudowy, trwa kilka minut i odbywa się w tle</div>';

		$this->pSmarty->assign('sContent', $info.$form->ShowForm());
	}
}