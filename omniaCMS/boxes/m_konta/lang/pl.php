<?php
/**
* Plik jezykowy boksu modulu 'Konta uzytkownikow'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['box_m_konta']['logged_as'] = "Zalogowany:";
$aConfig['lang']['box_m_konta']['welcome'] = "Jesteś zalogowany jako:";
$aConfig['lang']['box_m_konta']['logout'] = "wyloguj";
$aConfig['lang']['box_m_konta']['password']	= 'zmień hasło';
$aConfig['lang']['box_m_konta']['edit']	= 'moje konto';
?>