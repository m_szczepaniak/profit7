<?php
/**
 * Klasa Box do obslugi boksu Konta uzytkownikow
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
 
class Box_m_konta {
  
  // Id boksu
  var $iId;
  
  // Id strony boksu
  var $iPageId;
  
  // nazwa boksu
  var $sName;
  
	// sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // konfiguracja boksu
  var $aSettings;
  
  // link do strony
  var $sPageLink;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_konta(&$aBox) {
		$this->iId =& $aBox['id'];
		$this->iPageId = $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sModule = $aBox['module'];
		$this->sTemplatesPath = 'boxes/'.$aBox['module'];
		$this->sTemplate = $aBox['template'];
		$this->sPageLink = '/'.$aBox['page_symbol'];
	} // end Box() function


	/**
	 * Metoda okresla sciezke do strony Konta uzytkownikow,
	 * dowiazuje ja do Smartow oraz zwraca xHTML z boksem newslettera
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $pSmarty, $aConfig, $pDB2;
		$sHtml = '';
		
		$aBox['page_link'] =& $this->sPageLink;
		$aBox['lang'] =& $aConfig['lang']['box_'.$this->sModule];
		
    
    if (!$aBox['is_logged_in'] = isLoggedIn()) {
      include_once($aConfig['common']['base_path'].'LIB/fb_connector/fbConnector.class.php');
      $oFB = fbConnector::getInstance();
      // zaloguj się
      $aBox['links']['fb_register'] = $oFB->getLoginURL($aConfig['common']['client_base_url_https'].'moje-konto/fb-logowanie');//.'/Pozainstancyjne-srodki-ochrony-prawnej,product650955.html'
    }
    
		if (!$aBox['is_logged_in'] = isLoggedIn()) {
      
      $aBox['links']['fb_register'] = $oFB->getLoginURL();
			// linki zaloz konto i przypomnij haslo
			$aBox['links']['register'] = createLink($this->sPageLink, 'register');
			$aBox['links']['login'] = $this->sPageLink;
		}
		else {
			$aBox['logged_as'] = $_SESSION['w_user']['email'];

			$aBox['user_name'] = $_SESSION['w_user']['user_name'];
			
			// linki
			// Edycja danych
			$aBox['links']['edit'] = $this->sPageLink;
			// Zmiana hasla
			//$aBox['links']['password'] = createLink($this->sPageLink, 'password');
			// Wyloguj
			$aBox['links']['logout'] = createLink($this->sPageLink, 'logout');
		}
		
		$aBox['settings'] =& $this->aSettings;
		$aBox['name'] =& $this->sName;
		
		$pSmarty->assign_by_ref('aBox', $aBox);
    $sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
    $pSmarty->clear_assign('aBox');
		
		//dump($aBox);
		return $sHtml;
	} // end of getContent()
} // end of Box Class
?>