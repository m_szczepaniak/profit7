<?php
/**
 * Klasa Box do obslugi boksu modulu 'Strony opisowe'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2007
 * @version   1.0
 */

// dolaczene wspolnej klasy modulu Aktualnosci
include_once('modules/m_opisy/client/Common.class.php');

class Box_m_opisy extends Common_m_opisy {
	
  // Id boksu
  var $iId;
  
  // nazwa boksu
  var $sName;
  
  // konfiguracja boksu
  var $aSettings;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_opisy(&$aBox) {
		// konstruktor klas Common
		parent::Common_m_opisy($aBox['page_id'],
													 '/'.$aBox['page_symbol'],
													 $aBox['module'],
													 'boxes/'.$aBox['module'],
													 $aBox['template']);
		$this->iId =& $aBox['id'];
		$this->sName = $aBox['name'];
	} // end Box() function
	
	
	/**
	 * Metoda pobiera tresc strony (wszystkie akapity)
	 * dowiazuje ja do Smartow oraz zwraca xHTML z trescia strony po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $pSmarty, $aConfig;
		$sHtml = '';
		
		$aBox =& $this->getItems();

		if (!empty($aBox['items'])) {
			$aBox['name'] =& $this->sName;
			$aBox['page_link'] =& $this->sPageLink;
			$aBox['lang'] =& $aConfig['lang']['box_'.$this->sModule];
			
			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
	    $pSmarty->clear_assign('aBox');
		}
		return $sHtml;
	} // end of getContent()
} // end of Box_m_opisy Class
?>