<?php
/**
 * Klasa Module do obslugi konfiguracji boksu modulu 'System bannerowy'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2007
 * @version   1.0
 */
 
class Settings {
  
  // nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;
	
	// Id kategorii dla ktorej edytowane sa ustawienia
	var $iId;
	
	// Id modulu aktualnosci
	var $iModuleId;
		
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$iModuleId, &$sModule, &$iLangId, &$iId) {
		global $aConfig;
		$this->iModuleId = $iModuleId;
		$this->sModule = $sModule;
		$this->iLangId = $iLangId;
		$this->iId = $iId;
				
		// pobranie konfiguracji dla boksu modulu
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."banners_box_settings 
						 WHERE box_id = ".$this->iId;
		$aCfg = Common::GetRow($sSql);
				
		// dolaczenie ustawien do glownej tablicy ustawien $aConfig
		$aConfig['settings'][$this->sModule] =& $aCfg;
	} // end Settings() function
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach boksu modulu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;
		
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."banners_box_settings
						 WHERE box_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);
		
		$aValues = array(
			'items_in_box' => intval($_POST['items_in_box'])
		);
		if (!empty($aSettings)) {
			// update
			if (Common::Update($aConfig['tabls']['prefix']."banners_box_settings",
												 $aValues,
												 "box_id = ".$this->iId) === false) {
				return false;
			}
		}
		else {
			// insert
			$aValues['box_id'] = $this->iId;
			if (Common::Insert($aConfig['tabls']['prefix']."banners_box_settings",
												 $aValues,
												 '',
												 false) === false) {
				return false;
			}
		}
		return true;
	} // end of UpdateSettings() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji konfiguracji boksu modulu
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;
		
		$aData = array();
		$sHtml = '';
		
		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_type') {
			$aData =& $_POST;
		}
		
		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['settings'], array('class'=>'merged', 'style'=>'padding-left: 173px'));
		
		// cache'owanie
		$pForm->AddHidden('cacheable', '0');
		
		// cache'owanie dla calego serwisu czy dla kazdej strony
		$pForm->AddHidden('cache_per_page', '0');
		
		// strony systemu bannerowego
		$aPages =& getModulePages($this->iModuleId, $this->iLangId);
		$aPages = array_merge(
			array(array('value'=>'0', 'label'=>$aConfig['lang']['common']['choose'])),
			$aPages
		);
		$pForm->AddSelect('page_id', $aConfig['lang'][$this->sModule.'_box_settings']['page_id'], array(), $aPages, $aData['page_id']);
		
		// liczba bannerow w boksie
		$pForm->AddText('items_in_box', $aConfig['lang'][$this->sModule.'_box_settings']['items_in_box'], $aData['items_in_box'], array('maxlength'=>3, 'style'=>'width: 30px;'), '', 'uinteger');
	} // end of SettingsForm() function
} // end of Settings Class
?>