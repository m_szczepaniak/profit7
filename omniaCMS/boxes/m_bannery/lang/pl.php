<?php
/**
* Plik jezykowy boksu 'Galeria'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/

$aConfig['lang']['box_m_galeria']['more'] = "więcej";
$aConfig['lang']['box_m_galeria']['see_all'] = "zobacz wszystkie";
$aConfig['lang']['box_m_galeria']['all'] = "wszystkich:";
?>