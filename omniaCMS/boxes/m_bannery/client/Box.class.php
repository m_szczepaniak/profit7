<?php
/**
 * Klasa Box do obslugi boksu modulu System bannerowy
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2007
 * @version   1.0
 */
 
class Box_m_bannery {
  
  // Id boksu
  var $iId;
  
  // Id wersji jezykowej
  var $iLangId; 
  
  // symbol wersji jezykowej
  var $sLang;
  
  // Id strony
  var $iPageId;
  
  // nazwa boksu
  var $sName;
  
	// sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // konfiguracja boksu
  var $aSettings;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_bannery(&$aBox) {
		$this->iId =& $aBox['id'];
		$this->iLangId =& $_GET['lang_id'];
		$this->sLang =& $_GET['lang'];
		$this->iPageId = $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sModule = $aBox['module'];
		$this->sTemplatesPath = 'boxes/'.$aBox['module'];
		$this->sTemplate = $aBox['template'];
		
		// pobranie konfiguracji boksu
		$this->aSettings =& $this->getSettings();
	} // end Box() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	array
	 */
	function getSettings() {
		global $aConfig;
		
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."banners_box_settings 
						 WHERE box_id=".$this->iId;
		$aCfg = Common::GetRow($sSql);
		return $aCfg;
	} // end of getSettings() function
	
	
	/**
	 * Metoda pobiera liste aktualnosci
	 * dowiazuje ja do Smartow oraz zwraca xHTML z boksem aktualnosci z
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $pSmarty, $aConfig;
		$sHtml = '';
		$aBox = array();

		$aBox['items'] =& $this->getItems();
		if (!empty($aBox['items'])) {
			$aBox['name'] =& $this->sName;
			$aBox['lang'] =& $aConfig['lang']['box_'.$this->sModule];
			$aBox['base_dir'] =& $aConfig['common']['file_dir'];
			$aBox['page_id'] =& $this->iPageId;
			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
	    $pSmarty->clear_assign('aBox');
		}
		return $sHtml;
	} // end of getContent()
	
	
	/**
	 * Metoda pobiera liste bannerow
	 * 
	 * @return	array	- tablica z lista bannerow
	 */
	function &getItems() {
		global $aConfig;
		$aItems = array();
		//dump($aConfig['_tmp']['path']);
		// liczba wyswietlanych w boksie bannerow
		$iItemsInBox = intval($this->aSettings['items_in_box']);
		$iPageId = 0;
		if(!empty($aConfig['_tmp']['path'][1]['id'])){
			$iPageId = $aConfig['_tmp']['path'][1]['id'];
			$iLevel = 1;
		} else {
			$iPageId = $aConfig['_tmp']['path'][0]['id'];
			$iLevel = 0;
		}
		if(empty($aConfig['_tmp']['path'])){
			$iPageId = $aConfig['_tmp']['page']['id'];
			$iLevel = 0;
		}
		
		$aExcludeBanners = array();
		if(!empty($_SESSION['banners']['excluded'])){
			foreach($_SESSION['banners']['excluded'] as $iKey=>$iVal){
				$aExcludeBanners[] = $iKey;
			}
		}
    
    if (intval($iPageId) <= 0 && intval($_GET['page_id']) > 0) {
      // leci z AJAX
      $iPageId = intval($_GET['page_id']);
      $iLevel = NULL;
    }
    if (empty($iPageId)) {
      // fix - czasem zdarza się że brak jest PAGE_ID
      $iPageId = '-1';
    }
		
		// okreslenie calkowitej liczby bannerow
		if (($iQuantity = $this->getItemsQuantity($iLevel,$iPageId)) > 0) {
			$sSql = "SELECT DISTINCT A.id, A.src, A.url, A.new_window, A.width, A.height, A.flash, A.capping
							 FROM ".$aConfig['tabls']['prefix']."banners A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."banners_to_pages B
							 ON B.banner_id=A.id ". ($iLevel !== null ? "AND B.level = ".$iLevel : '')."
							 WHERE A.page_id = ".$this->iPageId." AND
							 			 (A.start_date IS NULL OR A.start_date <= NOW()) AND
							 			 (A.end_date IS NULL OR A.end_date >= NOW()) AND
							 			 IF(A.max_views > 0, A.current_views < A.max_views, 1=1) AND
							 			 IF(A.logged_only = '1',".(isLoggedIn()?'1=1':'1=0').",1=1) AND
							 			 IF(A.all_pages = '1',1=1,B.page_id = ".$iPageId.")". 
							 			 (!empty($aExcludeBanners)?" AND IF(A.capping > 0, A.id NOT IN (".implode(',',$aExcludeBanners)."), 1=1)":'').
							 " ORDER BY ".
							 ($iItemsInBox > 0 && $iQuantity > $iItemsInBox ? " RAND()" : "A.order_by").
							 ($iItemsInBox > 0 ? " LIMIT 0, ".$iItemsInBox : '');
			$aItems =& Common::GetAll($sSql);
	
			// utwrzenie odpowiednich adresow URL dla bannerow
			$this->proceedItems($aItems);
	
			// zwiekszenie liczby wyswietlen na wybranych bannerach
			$this->increaseCurrentViews($aItems);
		}
		
		return $aItems;
	} // end of getItemsQuantity() method
	
	
	/**
	 * Metoda tworzy link dla kazdego bannera - w celu zliczania klikniec
	 * na poszczegolnych bannerach
	 * 
	 * @param	ref array	$aItems	- lista bannerow
	 * @return	void
	 */
	function proceedItems(&$aItems) {
		foreach ($aItems as $iKey => $aItem) {
			if (!empty($aItem['url'])) {
				$aItems[$iKey]['url'] = '/ads,'.
																base64_encode($this->iLangId.':'.
																							$this->iPageId.':'.
																							$aItem['id'].':'.
																							str_replace(':', '{_;_}', $aItem['url'])).
																							'.html';
			}
		}
	} // end of proceedItems() method
	
	
	/**
	 * Metoda zwieksza liczbe wyswietlen wyswietlanych bannerow
	 * 
	 * @param	ref array	$aItems	- lista bannerow
	 * @return	void
	 */
	function increaseCurrentViews(&$aItems) {
		global $aConfig;
		
		if (!empty($aItems)) {
			/*
			// wyłącznie uzgodnione z M. Korecki i M. Chudy 07.03.2012 w związku z limitem połączeń.
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."banners
							 SET current_views = current_views + 1
							 WHERE page_id = ".$this->iPageId." AND
						 			 	 id IN (".$this->getItemsIds($aItems).")";
			Common::Query($sSql);
			*/
			foreach($aItems as $aBanner){
				if($aBanner['capping'] > 0){
					$_SESSION['banners']['capping'][$aBanner['id']]++;
					if($_SESSION['banners']['capping'][$aBanner['id']] >= $aBanner['capping']){
						$_SESSION['banners']['excluded'][$aBanner['id']] = 1;
					}
				}
			}
		}
	} // end of increaseCurrentViews() method
	
	
	/**
	 * Metoda tworzy liste oddzielonych przecinkami Id bannerow
	 * do zapytania zwiekszajacego liczbe wyswietlen bannerow
	 * 
	 * @param	ref array	$aItems	- lista bannerow
	 * @return	string	- lista Id bannerow
	 */
	function getItemsIds(&$aItems) {
		$sIds = '';
		foreach ($aItems as $iKey => $aItem) {
			$sIds .= $aItem['id'].',';
		}
		return substr($sIds, 0, -1);
	} // end of getItemsIds() method
	
	
	/**
	 * Metoda pobiera i zwraca liczbe bannerow w danym boksie / stronie
	 * 
	 * @return	integer	- liczba wszystkich bannerow
	 */
	function getItemsQuantity($iLevel,$iPageId,$aExcludeBanners='') {
		global $aConfig;
		$sSql = "SELECT COUNT(DISTINCT id)
							 FROM ".$aConfig['tabls']['prefix']."banners A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."banners_to_pages B
							 ON B.banner_id=A.id ". ($iLevel !== null ? "AND B.level = ".$iLevel : '')."
							 WHERE A.page_id = ".$this->iPageId." AND
							 			 (A.start_date IS NULL OR A.start_date <= NOW()) AND
							 			 (A.end_date IS NULL OR A.end_date >= NOW()) AND
							 			 IF(A.max_views > 0, A.current_views < A.max_views, 1=1) AND
							 			 IF(A.logged_only = '1',".(isLoggedIn()?'1=1':'1=0').",1=1) AND
							 			 IF(A.all_pages = '1',1=1,B.page_id = ".$iPageId.")".
										(!empty($aExcludeBanners)?" AND IF(A.capping > 0, A.id NOT IN (".implode(',',$aExcludeBanners)."), 1=1)":'');
		/*$sSql = "SELECT COUNT(id)
						 FROM ".$aConfig['tabls']['prefix']."banners
						 WHERE page_id = ".$this->iPageId." AND
						 			 (start_date IS NULL OR start_date <= NOW()) AND
						 			 (end_date IS NULL OR end_date >= NOW()) AND
						 			 IF(max_views > 0, current_views < max_views, 1=1)";*/

		return intval(Common::GetOne($sSql));
	} // end of getItemsQuantity() method
} // end of Box Class
?>