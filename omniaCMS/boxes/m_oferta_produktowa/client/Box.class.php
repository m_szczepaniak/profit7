<?php
/**
 * Klasa Box_m_oferta_produktowa_nowowsci do obslugi boksu Oferty produktowej - bestsellery
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
include_once('modules/m_oferta_produktowa/client/Common.class.php');

class Box_m_oferta_produktowa extends Common_m_oferta_produktowa {
  
  // Id boksu
  var $iId;
  
  // Id wersji jezykowej
  var $iLangId;
  
  // symbol wersji jezykowej
  var $sLang;
  
  // Id menu
  var $iMenuId;
  
  // nazwa boksu
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // konfiguracja boksu
  var $aSettings;
  
  // opcja boksu
  var $sOption;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_oferta_produktowa(&$aBox) {
		
		$this->iId =& $aBox['id'];
		$this->iMenuId = $aBox['menu_id'];
		$this->iPageId = $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sModule = $aBox['module'];
		$this->sTemplatesPath = 'boxes/'.$aBox['module'].'/'.$this->sOption;
		$this->sTemplate = $aBox['template'];
		
		// pobranie konfiguracji boksu
		$this->aSettings =& $this->getSettings();
	} // end Box_m_oferta_produktowa_bestsellery() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig;

		$sSql = "SELECT A.items_in_box, B.page_id
						 FROM ".$aConfig['tabls']['prefix']."products_box_settings A
						 JOIN ".$aConfig['tabls']['prefix']."boxes B
						 ON B.id = A.box_id
						 WHERE A.box_id = ".$this->iId;
		return Common::GetRow($sSql);
	} // end of getSettings() function
	

	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aBox = array();
		
		$aBox['items'] = $this->getItems();
		if (!empty($aBox['items'])) {
			$aBox['name'] =& $this->sName;
			//$aBox['link'] =& $this->sOption;
			$aBox['link'] = $aBox['items'][0]['symbol'];
			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
			$pSmarty->clear_assign('aBox');
		}
		
		//dump($aBox);
		return $sHtml;
	} // end of getContent()
	
	

	
	/**
	 * Metoda pobiera i zwraca liste bestsellerow
	 * 
	 * @return	array	- lista produktow
	 */
	function &getItems() {
		global $aConfig;
		
		if (isLoggedIn()){
			$fUserDiscount = $_SESSION['w_user']['discounts'];
		}
		else {
			$fUserDiscount = 0;	
		}
		
		$sSql = "SELECT A.id as product_id, A.name as product_name, A.plain_name, A.price_brutto, A.discount_currency, A.promo_price_brutto, A.prod_status, A.prod_status_text, A.bestseller, A.short_description, A.exclude_discount, A.user_discount, IF(A.user_discount = '1',".($fUserDiscount > 0 ? "FORMAT(A.price_brutto - (A.price_brutto * (".$fUserDiscount."/100)), 2)" : "0").",'') as user_price
				,B.symbol
						 FROM ".$aConfig['tabls']['prefix']."products A
						 JOIN ".$aConfig['tabls']['prefix']."menus_items B
						 ON B.id = A.page_id					
						 WHERE A.page_id = '".$this->aSettings['page_id']."'"
						 			 .(!isPreviewMode() ? " AND A.published = '1'" : '')."
						 ".
						 ((int) $this->aSettings['items_in_box'] > 0 ? " LIMIT ".$this->aSettings['items_in_box'] : '');
		
		
		$aData = Common::GetAll($sSql);
		
		
		foreach($aData as $iKey => $aValue){
			
			// zdjecie produktu
			$aData[$iKey]['image'] = $this->getImages($aValue['product_id'], true);
			
			// link do produktu
			$aData[$iKey]['link'] = createLink($aValue['symbol'], 'id'.$aValue['product_id'], $aValue['product_name']);
			if($aData[$iKey]['promo_price_brutto'] != "0.00")
				$aData[$iKey]['promo_price_brutto'] = Common::formatPrice($aData[$iKey]['promo_price_brutto']);
			$aData[$iKey]['price_brutto'] = Common::formatPrice($aData[$iKey]['price_brutto']);
			$aData[$iKey]['promocyjna'] = Common::formatPrice($aData[$iKey]['promocyjna']);
			$aData[$iKey]['discount_currency'] = Common::formatPrice($aData[$iKey]['discount_currency']);
			$aData[$iKey]['user_price'] = Common::formatPrice($aData[$iKey]['user_price']);
		}

		return $aData;
		
	} // end of getItems() method
	
} // end of Box_m_oferta_produktowa_bestsellery
?>