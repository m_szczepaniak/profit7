<?php
/**
* Plik jezykowy boksu 'Zamowienia - Koszyk'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/

$aConfig['lang']['box_m_zamowienia']['total_quantity'] = "Przedmiotów:";
$aConfig['lang']['box_m_zamowienia']['items'] = " szt.";
$aConfig['lang']['box_m_zamowienia']['items_long'] = "Sztuk: ";
$aConfig['lang']['box_m_zamowienia']['total_value'] = "o wartości:";
$aConfig['lang']['box_m_zamowienia']['repository'] = "Przechowalnia:";
$aConfig['lang']['box_m_zamowienia']['last_added'] = "Ostatnio dodano";
$aConfig['lang']['box_m_zamowienia']['toCart'] = "&raquo; Przejdź do koszyka";
$aConfig['lang']['box_m_zamowienia']['empty_msg'] = "Twój koszyk jest obecnie pusty";

?>