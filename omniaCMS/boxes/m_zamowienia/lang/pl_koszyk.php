<?php
/**
* Plik jezykowy boksu 'Zamowienia - Koszyk'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/

$aConfig['lang']['box_m_zamowienia_koszyk']['products_no'] = "Przedmiotów: ";
$aConfig['lang']['box_m_zamowienia_koszyk']['cart_value'] = "Na sumę: ";
$aConfig['lang']['box_m_zamowienia_koszyk']['repository'] = "Przechowalnia:";
$aConfig['lang']['box_m_zamowienia_koszyk']['item'] = "szt.";
$aConfig['lang']['box_m_zamowienia_koszyk']['value'] = "PLN";


?>