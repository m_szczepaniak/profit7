<?php
/**
 * Klasa Box_m_amowienia_koszyk do obslugi boksu Zamowienia - koszyk
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

include_once('modules/m_zamowienia/client/Common.class.php');

class Box_m_zamowienia extends Common_m_zamowienia {

  // Id boksu
  var $iId;

  // nazwa boksu
  var $sName;

  // sciezka do katalogu z szablonami
  var $sTemplatesPath;

  // szablon boksu
  var $sTemplate;

  // nazwa modulu boksu
  var $sModule;

  // link do strony koszyka
  var$sPageLink;

  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_zamowienia(&$aBox) {
		$this->iId =& $aBox['id'];
		$this->iPageId = (int) $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sModule = $aBox['module'];
		$this->sTemplatesPath = 'boxes/'.$aBox['module'];
		$this->sTemplate = $aBox['template'];
		$this->sPageLink = '/'.$aBox['page_symbol'];
	} // end Box_m_zamowienia() function


	/**
	 * Metoda zwraca kod HTML koszyka po renderowaniu szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aBox = array();
		
		$aBox['page_link'] =& $this->sPageLink;
		$aBox['name'] =& $this->sName;
		$aBox['lang'] =& $aConfig['lang']['box_'.$this->sModule];
		$aBox['cart']['total_quantity'] = 0;
		$aBox['cart']['total_price_netto'] = 0;
		$aBox['cart']['total_price_brutto'] = 0;		
		
		$aBox['repository_link'] =$aConfig['_tmp']['repository_symbol'];
		$aBox['repository_items'] = 0;	
		$aBox['repository_items'] = $this->getRepositoryItems();
		
		if (!$this->cartIsEmpty()) {
			// koszyk nie jest pusty
			$aBox['cart'] =& $_SESSION['wu_cart'];
		///	dump($_SESSION['wu_cart']);
		}
		$pSmarty->assign_by_ref('aBox', $aBox);
		$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
		$pSmarty->clear_assign('aBox');
		return $sHtml;
	} // end of getContent()		


	/**
	 * Metoda zwraca ilosc przedmiotow w przechowalni
	 */
	function getRepositoryItems() {
		global $aConfig, $pSmarty;
		
		if(isLoggedIn()){
			$sSql = "SELECT COUNT(*) FROM ".$aConfig['tabls']['prefix']."orders_briefcase
								WHERE konto_id = ".$_SESSION['w_user']['id']."";
						
			$iResult =& Common::GetOne($sSql);
		} else {
			if(isset($_COOKIE['repository'])){
				$iResult = count(explode(",", base64_decode($_COOKIE['repository'])));
			} else {
				$iResult = 0;
			}
		}
		
		return $iResult;
	} // end of getContent()

} // end of Box_m_oferta_produktowa_promocje Class
?>