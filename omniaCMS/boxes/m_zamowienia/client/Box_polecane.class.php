<?php

include_once('modules/m_oferta_produktowa/client/Common.class.php');

class Box_m_zamowienia_polecane extends Common_m_oferta_produktowa
{

    // Id boksu
    var $iId;

    // Id menu
    var $iMenuId;

    // nazwa boksu
    var $sName;

    // sciezka do katalogu z szablonami
    var $sTemplatesPath;

    // szablon boksu
    var $sTemplate;

    // nazwa modulu boksu
    var $sModule;

    // konfiguracja boksu
    var $aSettings;

    // opcja boksu
    var $sOption;

    var $sPageLink;

    /**
     * Konstruktor klasy
     *
     * @param    array $aBox - dane boksu
     * @return    void
     */
    function Box_m_zamowienia_polecane(&$aBox)
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;
        $this->iId =& $aBox['id'];
        $this->iMenuId = $aBox['menu_id'];
        $this->iPageId = $aBox['page_id'];
        $this->sName = $aBox['name'];
        $this->sModule = $aBox['module'];
        $this->sOption = 'polecane';
        $this->sTemplatesPath = 'boxes/' . $aBox['module'] . '/' . $this->sOption;
        $this->sTemplate = $aBox['template'];
        $this->sPageLink = '/' . $aBox['page_symbol'];
        // pobranie konfiguracji boksu
        $this->aSettings =& $this->getSettings();

    }


    /**
     * Metoda pobiera konfiguracje dla boksu
     *
     * @return    array
     */
    function &getSettings()
    {
        global $aConfig;

        $sSql = "SELECT items_in_box, MI.bestsLimit
						 FROM " . $aConfig['tabls']['prefix'] . "products_box_settings 
             LEFT JOIN menus_items AS MI
              ON id = " . $this->iPageId . "
						 WHERE box_id = " . $this->iId;
        return Common::GetRow($sSql);
    } // end of getSettings() function


    function getContent()
    {
        global $aConfig, $pSmarty;
        $sHtml = '';
        $aBox = array();
        /*
        if (!pageIsPublished($this->iPageId)) {
            // strona nie jest opublikowana - boks nie moze zostac wyswietlony
            return $sHtml;
        }
     */

        $aBox['items'] = $this->getItems();
        if (!empty($aBox['items'])) {
            $aBox['name'] =& $this->sName;
            $aBox['link'] =& $this->sPageLink;
            $aBox['lang'] =& $aConfig['lang']['box_' . $this->sModule . '_' . $this->sOption];
            $pSmarty->assign_by_ref('aBox', $aBox);
            $pSmarty->assign('bKoszyk', $_GET['path'] === 'koszyk');
            $sHtml = $pSmarty->fetch($this->sTemplatesPath . '/' . $this->sTemplate);
            $pSmarty->clear_assign('bKoszyk');
            $pSmarty->clear_assign('aBox');
        }
        return $sHtml;
    } // end of getContent()


    /**d
     * Metoda pobiera i zwraca liste bestsellerow
     *
     * @return    array    - lista produktow
     */
    function &getItems()
    {
        global $aConfig;


        if (($_GET['path'] === 'koszyk' && isset($_SESSION['wu_cart']) && isset($_SESSION['wu_cart']['products']) && false === empty($_SESSION['wu_cart']['products'])) || (isset($_GET['product']) && $_GET['product'] !== "")) {
            $aProductsInCart = [];

            if($_GET['path'] === 'koszyk' && false === empty($_SESSION['wu_cart']['products'])) {
                foreach ($_SESSION['wu_cart']['products'] as $aProduct) {
                    $aProductsInCart[] = $aProduct['id'];
                }
            } else if (isset($_GET['product']) && $_GET['product'] !== "") {
                $aProductsInCart[] = $_GET['product'];
            }
            $aRecommendedAndCategory = [];

            $aProductsInCartProfit24Id = $aProductsInCart;
            if (!empty($aProductsInCartProfit24Id)) {
                $sSql = 'SELECT A.recommended_product_id  
                  FROM recommend_products AS A
                  JOIN products AS B
                    ON A.recommended_product_id = B.id
                  WHERE A.product_id IN (' . implode(',', $aProductsInCartProfit24Id) . ')
                    AND (B.prod_status = "1" OR B.prod_status = "3")
                    AND A.recommended_product_id NOT IN (' . implode(',', $aProductsInCartProfit24Id) . ')
                  ORDER BY A.priority DESC ' .
                    ((int)$this->aSettings['items_in_box'] > 0 ? " LIMIT " . $this->aSettings['items_in_box'] : '');

                $aRecommendedAndCategoryProfit24Id = $this->pDbMgr->GetCol('profit24', $sSql);
                if (!empty($aRecommendedAndCategoryProfit24Id)) {
                    $sSqlPR = 'SELECT id FROM products WHERE id IN (' . implode(', ', $aRecommendedAndCategoryProfit24Id) . ')';
                    $aRecommendedAndCategory = Common::GetCol($sSqlPR);
                }

            }

            $recommendedAmount = count($aRecommendedAndCategory);

            if ($recommendedAmount < $this->aSettings['items_in_box']) {
                $itemsLimit = ceil(($this->aSettings['items_in_box'] - $recommendedAmount) / count($aProductsInCart));
                $itemsAmount = $recommendedAmount;
                foreach ($aProductsInCart as $aProduct) {
                    $categories = $this->getLowestCategory($aProduct);
//                    if (!empty($categories)) {
                        // interesuje nas ostatnia kategoria
//                        $categories = [end($categories)];

//                    }
                    if (!empty($categories) && $itemsAmount < $this->aSettings['items_in_box']) {
                       $recommendedCategoryProducts = $this->getRecommendedProducts($itemsLimit, $aRecommendedAndCategory, $categories);
                        foreach ($recommendedCategoryProducts as $product) {
                            $aRecommendedAndCategory[] = $product;
                            $itemsAmount++;
                        }
                    }
                }
            }

            if ($itemsAmount < $this->aSettings['items_in_box']) {
                $diffItemsQuantity = $this->aSettings['items_in_box'] - $itemsAmount;
                $bestSellProductsIds = $this->getRecommendedProducts($diffItemsQuantity, array_merge($aProductsInCart, $aRecommendedAndCategory));
                if (!empty($bestSellProductsIds)) {
                    $aRecommendedAndCategory = array_merge($aRecommendedAndCategory, $bestSellProductsIds);
                }
            }


            $sSql = 'SELECT DISTINCT A.id, A.title AS name, A.price, A.price_brutto, A.edition, A.shipment_time, 
											A.isbn, A.isbn_plain, A.publication_year, 
											A.prod_status, A.publisher AS publisher_name, A.series, A.is_news, A.is_bestseller, A.is_previews, A.shipment_date, A.type, A.binding
							 FROM products_shadow AS A
					 		 WHERE 
               (A.prod_status = "1" OR A.prod_status = "3") '
                . (!empty($aRecommendedAndCategory) ? ' AND A.id IN (' . implode(',', $aRecommendedAndCategory) . ')' : '')
                . ' AND A.id NOT IN (' . implode(',', $aProductsInCart) . ')
                ORDER BY FIELD(A.id, ' . implode(',', $aRecommendedAndCategory) . ')
               LIMIT ' . $this->aSettings['items_in_box'];


            $aData = Common::GetAll($sSql);

            if ($aData === false)
                $aData = [];        // No recommended products

            foreach ($aData as $iKey => $aValue) {
                // pobranie autorow itp
                $aData[$iKey]['authors'] = $this->getAuthors($aValue['id'], 1);

                //pobranie cennika
                $aData[$iKey]['tarrif'] = $this->getTarrif($aValue['id']);
                // przeliczenie i formatowanie cen
                $aData[$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aData[$iKey]['price_brutto'], $aData[$iKey]['tarrif'], $aData[$iKey]['promo_text'], NULL, $this->getSourceDiscountLimit($aValue['id']), NULL, NULL, NULL));
                $aData[$iKey]['price_brutto'] = Common::formatPrice($aValue['price_brutto']);

                // okladka
//				$aData[$iKey]['image'] =getItemImage('products_images',array('product_id'=> $aValue['id']),'__b_');
                $aData[$iKey]['image'] = $this->getImages($aValue['id'], false);
                $aData[$iKey]['shipment_date'] = FormatDate($aValue['shipment_date']);
                $pShipmentTime = new \orders\Shipment\ShipmentTime();
                $aData[$iKey]['shipment'] = $pShipmentTime->getShipmentTime($aValue['id'], $aValue['shipment_time']);
                // link do produktu
                $aData[$iKey]['link'] = $aConfig['common']['base_url_http_no_slash'] . createProductLink($aValue['id'], $aValue['name']);
                $aData[$iKey]['cart_link'] = createLink('/koszyk', 'id' . $aValue['id'], 'add');
                $aData[$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'], 'id' . $aValue['id'], 'add_to_repository,');

//                $aData[$iKey]['book_rating'] = $rate->getRatingValue($aValue['book_rating']);
            }
            return $aData;
        }
    } // end of getItems() method


    private function getLowestCategory($iId)
    {
        global $aConfig;

        $aLowestCategory = [];

        $sCSql = "SELECT A.page_id
                          FROM " . $aConfig['tabls']['prefix'] . "products_extra_categories AS A
						  JOIN " . $aConfig['tabls']['prefix'] . "menus_items AS B
						  ON B.id = A.page_id
						  WHERE A.product_id = " . $iId . "
						  AND B.parent_id IS NOT NULL
                          ORDER BY B.priority ASC";
//                          LIMIT 1
//                            ";
        $aCats = Common::GetCol($sCSql);
        if (!empty($aCats)) {
            $aLowestCategory = $aCats;
            $aCats = $this->findLowestCategory($iId, $aCats);
            if (!empty($aCats)) {
                $aLowestCategory = $aCats;
            }
        } else {
            $sCSql = "SELECT A.page_id
                              FROM " . $aConfig['tabls']['prefix'] . "products_extra_categories AS A
                              JOIN " . $aConfig['tabls']['prefix'] . "menus_items AS B
                              ON B.id = A.page_id
                              WHERE A.product_id = " . $iId . "
                              AND B.parent_id IS NOT NULL
                                ";
            $aCats = Common::GetCol($sCSql);
            if (!empty($aCats)) {
                $aLowestCategory = $aCats;
            }
        }

        return $aLowestCategory;
    }

    /**
     *
     * @param int $iId
     * @param array $aParentCats
     * @return boolean
     */
    private function findLowestCategory($iId, $aParentCats)
    {
        $sCSql = "SELECT A.page_id
                FROM products_extra_categories AS A
                  JOIN menus_items AS B
                   ON B.id = A.page_id
                   WHERE A.product_id = " . $iId . "
                    AND B.parent_id IN (" . implode(',', $aParentCats) . ")
                    ";
        $aCats = Common::GetCol($sCSql);
        if (!empty($aCats)) {
            $aLowerCat = $this->findLowestCategory($iId, $aCats);
            if (!empty($aLowerCat)) {
                return $aLowerCat;
            } else {
                return $aCats;
            }
        }
        return false;
    }

    /**
     * @param $limit
     * @param array $aRecommended
     * @param array $categories
     * @return array
     */
    private function getRecommendedProducts($limit, $aRecommended = [], $categories = [])
    {
        if ($limit < 1) {
            $limit = 1;
        }
        if ($limit > 100) {
            $limit = 100;
        }
        $sSql = 'SELECT A.id
                FROM products_shadow AS A
                
                ' .(!empty($categories) ? '
                JOIN products_extra_categories AS B
                ON A.id = B.product_id
                ' : '' ).'
                WHERE 
                (A.prod_status = "1" OR A.prod_status = "3") 
                
                ' . (!empty($aRecommended) ? ' AND A.id NOT IN (' . implode(',', $aRecommended) . ')' : '') . '
                ' . (!empty($categories) ? ' AND B.page_id IN (' . implode(',', $categories).')' : '') . '
                ORDER BY SUBSTR(A.order_by, 1, 3) DESC 
                LIMIT ' . $limit;

        return \Common::GetCol($sSql);
    }
} // end of Box_m_oferta_produktowa_bestsellery
?>