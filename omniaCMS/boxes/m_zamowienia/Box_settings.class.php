<?php
/**
 * Klasa Module do obslugi konfiguracji boksu 'Zamowienia - koszyk'
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2008
 * @version   1.0
 */

class Settings {

  // nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// Id kategorii dla ktorej edytowane sa ustawienia
	var $iId;

	// Id modulu
	var $iModuleId;
	
	// Id strony
	var $iPageId;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$iModuleId, &$sModule, &$iLangId, &$iId) {
		global $aConfig;
		$this->iModuleId = $iModuleId;
		$this->sModule = $sModule;
		$this->iLangId = $iLangId;
		$this->iId = $iId;
		$this->iPageId = $this->getPageId();
	} // end Settings() function


	/**
	 * Metoda pobiera Id strony modulu zamowienia boksu
	 *
	 * @return	void
	 */
	function getPageId() {
		global $aConfig;
		if ($this->iId == 0) return 0;
		
		// pobranie Id strony
		$sSql = "SELECT page_id
						 FROM ".$aConfig['tabls']['prefix']."boxes
						 WHERE id = ".$this->iId;
		return (int) Common::GetOne($sSql);
	} // end of getPageId() method
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach boksu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		return true;
	} // end of UpdateSettings() funciton


	/**
	 * Metoda tworzy formularz edycji konfiguracji aktualnosci
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		$aData =& $this->aSettings;
		if (!empty($_POST) && $_POST['do'] != 'change_type') {
			$aData =& $_POST;
		}

		// strony modulu
		$aPages =& getModulePages($this->iModuleId, $this->iLangId);
		$aPages = array_merge(
			array(array('value'=>'0', 'label'=>$aConfig['lang']['common']['choose'])),
			$aPages
		);
		$pForm->AddSelect('page_id', $aConfig['lang'][$this->sModule.'_box_settings']['page'], array(), $aPages, $this->iPageId);
	} // end of SettingsForm() function
} // end of Settings Class
?>