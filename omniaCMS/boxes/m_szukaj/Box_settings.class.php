<?php
/**
 * Klasa Module do obslugi konfiguracji boksu 'Wyszukiwarki'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Settings {

  // nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// Id strony dla ktorej edytowane sa ustawienia
	var $iId;

	// Id modulu aktualnosci
	var $iModuleId;

	// konfiguracja boksu
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$iModuleId, &$sModule, &$iLangId, &$iId) {
		global $aConfig;
		$this->iModuleId = $iModuleId;
		$this->sModule = $sModule;
		$this->iLangId = $iLangId;
		$this->iId = $iId;
		$this->aSettings =& $this->getSettings();
	} // end Settings() function


	/**
	 * Metoda pobiera konfiguracje dla boksu
	 *
	 * @return	void
	 */
	function &getSettings() {
		global $aConfig;

		// pobranie konfiguracji dla boksu
		$sSql = "SELECT page_id
						 FROM ".$aConfig['tabls']['prefix']."boxes
						 WHERE id = ".$this->iId;
		return Common::GetRow($sSql);
	} // end of getSettings() method


	/**
	 * Metoda wprowadza zmiany w ustawieniach boksu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		return true;
	} // end of UpdateSettings() funciton


	/**
	 * Metoda tworzy formularz edycji konfiguracji boksu
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		$aData =& $this->aSettings;
		if (!empty($_POST) && $_POST['do'] != 'change_type') {
			$aData =& $_POST;
		}

		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['settings'], array('class'=>'merged', 'style'=>'padding-left: 175px'));

		// strony wyszukiwarki
		$aPages =& getModulePages($this->iModuleId, $this->iLangId);
		$aPages = array_merge(
			array(array('value'=>'0', 'label'=>$aConfig['lang']['common']['choose'])),
			$aPages
		);
		$pForm->AddSelect('page_id', $aConfig['lang'][$this->sModule.'_box_settings']['page'], array(), $aPages, $aData['page_id']);
	} // end of SettingsForm() function
} // end of Settings Class
?>