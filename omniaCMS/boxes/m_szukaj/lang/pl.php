<?php
/**
* Plik jezykowy boksu modulu 'WYSZUKIWARKA'
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @copyright 2006
* @version 1.0
*
*/

$aConfig['lang']['box_m_szukaj']['search_txt'] = "Wyszukaj w serwisie: ";
$aConfig['lang']['box_m_szukaj']['search'] = "słowo kluczowe";
$aConfig['lang']['box_m_szukaj']['all_categories'] = "Wszystkie działy";
?>