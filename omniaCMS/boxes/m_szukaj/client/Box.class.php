<?php
/**
 * Klasa Box do obslugi boksu wyszukiwarki
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Box_m_szukaj {
  
  // Id boksu
  var $iId;
  
  // Id strony wyszukiwarki
  var $iPageId;
  
  // nazwa boksu
  var $sName;
  
	// sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // link do strony
  var $sPageLink;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_szukaj(&$aBox) {
		$this->iId =& $aBox['id'];
		$this->iPageId = $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sModule = $aBox['module'];
		$this->sTemplatesPath = 'boxes/'.$aBox['module'];
		$this->sTemplate = $aBox['template'];
		$this->sPageLink = '/'.$aBox['page_symbol'];
	} // end Box() function


	/**
	 * Metoda okresla sciezke do strony wyszukiwarki,
	 * dowiazuje ja do Smartow oraz zwraca xHTML z boksem wyszukiwarki
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $pSmarty, $aConfig;
		$sHtml = '';
		
		$aBox['name'] = $this->sName;
		$aBox['page_link'] =& $this->sPageLink;
		$aBox['lang'] =& $aConfig['lang']['box_'.$this->sModule];
		
		$aBox['top_cats'] = $this->getTopPages();

		$js = "
			 $(document).ready(function(){

				$(document).click(function(){
					$('#qibox_results').html('');
				});

				$('#wyszukiwarkaSrodek').click(function(e){
					e.stopPropagation();
				});

				$(document).keydown(function(e) {

					if ($('#qibox').is(':focus')) {

						if((e.which == 40 || e.which == 38)) {

								var start = $('.s-highlight-selected')
								$('.s-highlight-li').removeClass('s-highlight-selected');

								if (start.length < 1) {

									var start = $('.s-highlight-li:first')
									start.addClass('s-highlight-selected');
								} else {

									if (e.which == 40) {
										var target = start.next('.s-highlight-li');
									}

									if (e.which == 38) {
										var target = start.prev('.s-highlight-li');
									}

									if (target.length < 1) {
										start.addClass('s-highlight-selected');
									} else {
										target.addClass('s-highlight-selected');
									}
								}

								var txt = $('.s-highlight-selected a').data('text');
								$('#qibox').val(txt);
						}
					}
				});

				var timer = null;
				var currentAjax = null;
				$('#qibox').keydown(function(e){

						if((e.which == 40 || e.which == 38 || e.which == 37 || e.which == 36)) {
							return;
						}

						url = '/internals/GetPhrases.php'
					   	clearTimeout(timer);

						var beforeVal = $(this).val();

					   	timer = setTimeout(function(){

				        var termValue = $('#qibox').val();
						wordLength = parseInt(termValue.length);

						if (wordLength >= 2 && $('#qibox').val() != beforeVal) {

						if (null != currentAjax) {
						   currentAjax.abort();
						}

						var notAval = 0;

						if($('.searchBoxFormBottom input#aval').is(':checked')) {
							notAval = 1
						}

						var fullUrl = url+'?term='+termValue+'&aval='+notAval;

						var selectedCat = $('#wyszukiwarka select#top_cat').val();

						if (selectedCat != undefined && selectedCat != '') {

						   fullUrl += '&top_cat='+selectedCat;
						}

						  currentAjax = $.ajax(fullUrl, {
						      beforeSend: function(){

						      var htmls2 = '<ul id=result-ui><li style=overflow:hidden;height:65px><a class=search-element><span class=search-element-img><img width=44 height=44 src=images/gfx/start-loader.gif></span><span class=search-element-cnt-container><span class=book-search-result>Trwa wyszukiwanie...</span></span></a></li><ul>';

						      	  $('#qibox_results').html(htmls2);
						      },
							  success: function(data) {
								$('#qibox_results').html(data);
							  },
							  error: function() {
							  	$('#qibox_results').html('');
							  }
						   });

						 }

					   	}, 1000)
				});
			 });
		";

		$aConfig['header_js'][]= $js;

		$js2 = "
		<script type=text/javascript>

		function submit_row(e)
		{
			var elementValue = $(e).data('text');
			$('#qibox').val(elementValue);

			$(document).find('#szukaj').trigger('click');
		}

		</script>
		";

		$pSmarty->assign_by_ref('aBox', $aBox);
    $sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
    $pSmarty->clear_assign('aBox');
		return $sHtml.$js2;
	} // end of getContent()
	
	function getTopPages(){
		global $aConfig;
		$sSql = "SELECT menu1_id, menu2_id, menu3_id 
						 FROM ".$aConfig['tabls']['prefix']."menus_box_glowna_settings";
		$aMenus = Common::GetRow($sSql);
		
		$sSql="SELECT menu_id, id AS value, name AS label
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".getModuleId('m_oferta_produktowa')."
					 AND mtype = '0'
					 AND parent_id IS NULL
					 AND published = '1'";
		$aTopCats = Common::GetAssoc($sSql);
		$aTopPages[] = array('value'=>'','label'=>$aConfig['lang']['box_'.$this->sModule]['all_categories']);
		$aTopPages[] = $aTopCats[$aMenus['menu1_id']];
		$aTopPages[] = $aTopCats[$aMenus['menu2_id']];
		$aTopPages[] = $aTopCats[$aMenus['menu3_id']];
		return $aTopPages;
	}
	

} // end of Box Class
?>