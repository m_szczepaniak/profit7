<?php
/**
 * Klasa Settings do obslugi konfiguracji boksu zapowiedzi 'Oferty produktowej'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Settings {
  
  // nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;
	
	// Id kategorii dla ktorej edytowane sa ustawienia
	var $iId;
	
	// Id modulu aktualnosci
	var $iModuleId;
	
	// opcja boksu
	var $sOption;
		
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$iModuleId, &$sModule, &$iLangId, &$iId, $sOption='') {
		global $aConfig;
		$this->iModuleId = $iModuleId;
		$this->sModule = $sModule;
		$this->iLangId = $iLangId;
		$this->iId = $iId;
		$this->sOption = $sOption;
				
		// pobranie konfiguracji dla modulu
		$sSql = "SELECT A.*, B.page_id 
						 FROM ".$aConfig['tabls']['prefix']."tags_box_settings A
						 JOIN ".$aConfig['tabls']['prefix']."boxes B
						 ON B.id = A.box_id 
						 WHERE A.box_id = ".$this->iId;
		$aCfg = Common::GetRow($sSql);
		
		// dolaczenie ustawien do glownej tablicy ustawien $aConfig
		$aConfig['settings'][$this->sModule] =& $aCfg;
	} // end Settings() function
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach boksu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;
		
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."tags_box_settings
						 WHERE box_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);

		$aValues = array(
			'items_in_box' => (int) $_POST['items_in_box'],
			'category_id' => ($_POST['category_id'] <= 0) ? 'NULL' : (int)$_POST['category_id']
		);
		if (!empty($aSettings)) {
			// Update
			return Common::Update($aConfig['tabls']['prefix']."tags_box_settings",
														$aValues,
														"box_id = ".$this->iId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('box_id' => $this->iId), $aValues);

			return Common::Insert($aConfig['tabls']['prefix']."tags_box_settings",
														$aValues, "", false) !== false;
		}
	} // end of UpdateSettings() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji konfiguracji boksu
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_type') {
			$aData =& $_POST;
		}
		$aLang =& $aConfig['lang'][$this->sModule.'_box_settings'];
		
		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['settings'], array('class'=>'merged', 'style'=>'padding-left: 175px'));
				

		$pForm->AddSelect('page_id', $aLang['page_id'], array(), array_merge(array(array('value'=>0,'label'=>$aConfig['lang']['common']['choose'])),getModulePages('m_tagi',$this->iLangId,true,'','')), $aData['page_id'], '', false);
		
		// liczba produktow w boksie
		$pForm->AddText('items_in_box', $aLang['items_in_box'], $aData['items_in_box'], array('maxlength'=>3, 'style'=>'width: 30px;'), '', 'uinteger');
		$aPages = $this->getProductsCategories(Common::getProductsMenu($_SESSION['lang']['id']), $aConfig['lang']['common']['choose']);
		$pForm->AddSelect('category_id', $aLang['category_id'], array(), $aPages, $aData['category_id'], '', false);
	
	} // end of SettingsForm() function
	
	/**
	 * 
	 * @param	integer	$iMId	- Id menu oferty produktowej
	 */
	function getProductsCategories($aMId, $sLabel) {
		global $aConfig;
		$aItems = array();
		if (empty($aMId)) return $aItems;
		
		$sSql = "SELECT IFNULL(parent_id, 0), id, name, mtype, module_id,
										IFNULL(link_to_id, 0) AS link_to_id
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE menu_id IN (".implode(',',$aMId).") AND
						 			 language_id = ".$_SESSION['lang']['id']."
						 			 ORDER BY parent_id, order_by";
		$aItems =& Common::GetAssoc($sSql, true, array(), DB_FETCHMODE_ASSOC, true);
		// utworzenie drzewa menu
		
		$aMenuItems =& getMenuTree($aItems);

		// strona do ktorej linkuje
		if($sLabel != ''){
		$aMenuTree2 = array_merge(
			array(array('value' => '0', 'label' => $sLabel)),
			getComboMenuTree($aMenuItems, 0, 0, 0, getModuleId('m_oferta_produktowa'))
		);
		}
		else {
			$aMenuTree2=getComboMenuTree($aMenuItems, 0, 0, 0, getModuleId('m_oferta_produktowa'));
		}
		return $aMenuTree2;
	} // end of getProductsCategories method
} // end of Settings Class
?>