<?php
/**
 * Klasa Module do obslugi boksu 'Tagi'
 *
 * @author	Marcin Korecki <m.korecki@omnia.pl>
 * @copyright	2008
 * @version	1.0
 */

class Box_m_tagi {
  
  // Id boksu
  var $iId;
  
  // Id strony modulu
  var $iPageId;
  
   // konfiguracja boksu
  var $aSettings;
  
  // szablon strony
  var $sTemplate;
  
  // nazwa boksu
  var $sName;
  
   // link do strony
  var $sPageLink;
  
	function Box_m_tagi(&$aBox) {
		$this->iId =& $aBox['id'];
		$aIds = getModulePagesIds('m_tagi');
		$this->iPageId = (double) $aIds[0];
		$this->sName = $aBox['name'];
		$this->sTemplate = 'boxes/'.$aBox['module'].'/'.'domyslny.tpl';
		// pobranie konfiguracji boksu
		$this->sPageLink = '/'.$aBox['page_symbol'];
		$this->aSettings =& $this->getSettings();
  }
  
  	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig;
		
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."tags_box_settings 
						 WHERE box_id = ".$this->iId;
		return Common::GetRow($sSql);
	} // end of getSettings() function
	
  
  
  
	/**
	 * Metoda pobiera liste tagow oraz dowiazuje ja do szablonu
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		
		if (!pageIsPublished($this->iPageId)) {
			// strona nie jest opublikowana - boks nie moze zostac wyswietlony
			return $sHtml;
		}
		
		$aBox['items'] =& $this->getTags();
		if (!empty($aBox['items'])) {
			$aBox['name'] =& $this->sName;
			$aBox['link'] = $this->sPageLink;
			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplate);
			$pSmarty->clear_assign('aBox', $aBox);
		}		
		return $sHtml;
	} // end of getContent()
	
	/**
	 * Metoda pobiera liste tagow oraz ich ilosc
	 */
	function getTags() {
		global $aConfig;
		
		$sSql = "SELECT tag,  quantity
						 FROM ".$aConfig['tabls']['prefix']."products_tags
						 WHERE quantity > 0".
						 ($this->aSettings['category_id']>0?" AND page_id = ".$this->aSettings['category_id']:'')."
						  ORDER BY quantity DESC".
						 ((int) $this->aSettings['items_in_box'] > 0 ? " LIMIT ".$this->aSettings['items_in_box'] : ' LIMIT 10');
		$aData =& Common::GetAll($sSql);		
		foreach($aData as $iKey => $aValue){
			if($iKey < 6) $aData[$iKey]['tag'] = "&nbsp;<a href='".getTagLink($aValue['tag'])."' class='size_".($iKey+1)."'>".$aValue['tag']."</a>&nbsp;";
			else $aData[$iKey]['tag'] = "&nbsp;<a href='".getTagLink($aValue['tag'])."'>".$aValue['tag']."</a>&nbsp;";
		}
		asort($aData);		
		return $aData;
	} // end of getContent()
} // end of Module Class
?>