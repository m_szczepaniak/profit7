<?php
/**
 * Klasa Box_m_menu do obslugi boksu menu - pierwszy poziom
 *
 * @author    Marcin Korecki <korecki@omnia.pl>
 * @copyright 2007
 * @version   1.0
 */

class Box_m_menu_pierwszy_poziom {

  // Id boksu
  var $iId;

  // Id menu
  var $iMenuId;

  // nazwa boksu
  var $sName;

  // sciezka do katalogu z szablonami
  var $sTemplatesPath;

  // szablon boksu
  var $sTemplate;

  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_menu_pierwszy_poziom(&$aBox) {
		$this->iId =& $aBox['id'];
		$this->iMenuId = $aBox['menu_id'];
		$this->sName = $aBox['name'];
		$this->sTemplatesPath = 'boxes/'.$aBox['module'];
		$this->sTemplate = $aBox['template'];
	} // end Box_m_menu_pierwszy_poziom() function


	/**
	 * Metoda pobiera z pliku XML strukture menu o Id $this->iMenuId,
	 * dowiazuje ja do Smartow oraz zwraca xHTML z elementami menu po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aSelected = array();
		$aBox = array();

		// pobranie pierwszego poziomu menu
		$aBox['menu'] =& $this->getMenuFirstLevel();
		if (!empty($aBox['menu'])) {
    	$aBox['name'] =& $this->sName;

    	$pSmarty->assign_by_ref('aBox', $aBox);
    	$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
    	$pSmarty->clear_assign('aBox');
    }
    	//dump($aBox);
		return $sHtml;
	} // end of getContent()


	/**
	 * Metoda pobiera i zwraca pierwszy poziom menu
	 *
	 * @return	array ref	- pierwszy poziom menu
	 */
	function &getMenuFirstLevel() {
		global $aConfig, $oTimer;

		$sSql = "SELECT id, symbol, name, url, link_to_id, mtype, new_window
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$_GET['lang_id']." AND
						 			 menu_id = ".$this->iMenuId." AND
						 			 published = '1' AND
						 			 menu = '1' AND
						 			 parent_id IS NULL
						 ORDER BY order_by";
		$aItems =& Common::GetAll($sSql);

		foreach ($aItems as $iKey => $aItem) {
			$aItems[$iKey]['name'] = htmlentities($aItem['name'], ENT_QUOTES, 'UTF-8');
			$aItems[$iKey]['link'] = getPageLink($aItem);
			$aItems[$iKey]['selected'] = in_array($aItem['id'], $aConfig['_tmp']['selected']);
		}
		return $aItems;
	} // end of getMenuFirstLevel() method
} // end of Box_m_menu_pierwszy_poziom Class
?>