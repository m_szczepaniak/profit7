<?php
/**
 * Klasa do obsługi "produkt znajduje się w pakietach"
 * powiazane
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
include_once('modules/m_oferta_produktowa/client/Common.class.php');

class Box_m_powiazane_oferta_produktowa_wpakiecie extends Common_m_oferta_produktowa {
  
  // Id boksu
  var $iId;
  
  // nazwa boksu
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // konfiguracja boksu
  var $aSettings;
  
  // opcja boksu
  var $sOption;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_powiazane_oferta_produktowa_wpakiecie(&$aBox) {
		$this->iId = (double) $aBox['id'];
		$this->iPageId = (double) $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sModule =& $aBox['module'];
		$this->sOption = 'wpakiecie';
		$this->sTemplatesPath = 'boxes/'.$aBox['module'].'/'.$this->sOption;
		$this->sTemplate = $aBox['template'];
		
		// pobranie konfiguracji boksu
		$this->aSettings =& $this->getSettings();
	} // end Box_m_powiazane_oferta_produktowa_wpakiecie() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig;

		$sSql = "SELECT items_in_box
						 FROM ".$aConfig['tabls']['prefix']."products_box_settings 
						 WHERE box_id = ".$this->iId;
		return Common::GetRow($sSql);
	} // end of getSettings() function
	

	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aBox = array();
		$aBox['lang'] =& $aConfig['lang']['box_'.$this->sModule.'_'.$this->sOption];
		$aBox['name'] = $this->sName;
            
		if(($aConfig['_tmp']['page']['module']  == 'm_oferta_produktowa') && $_GET['id'] > 0) {
			$aBox['items'] = $this->getItems($_GET['id'],$aBox['name']);
			if (!empty($aBox['items'])) {
			
				$pSmarty->assign_by_ref('aBox', $aBox);
				$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
				$pSmarty->clear_assign('aBox');
			}
		}
		return $sHtml;
	} // end of getContent()	

	
	/**
	 * Metoda pobiera i zwraca liste powiazanych produktow
	 * 
	 * @return	array	- lista produktow
	 */
	function &getItems($iId,&$sBoxName) {
		global $aConfig;

    
    $sSql = "SELECT packet
             FROM products_shadow
             WHERE id = ".$iId;
    $iPacket = Common::GetOne($sSql);
    if ($iPacket == '0') { // nie jest to pakiet
      $sSql = "SELECT A.id, A.title AS name, A.price, A.price_brutto, A.edition, A.shipment_time, 
                        A.isbn, A.isbn_plain, A.publication_year, 
                        A.prod_status, A.publisher AS publisher_name, A.series, A.is_news, A.is_bestseller, A.is_previews
                 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
                 JOIN ".$aConfig['tabls']['prefix']."products_packets_items AS B
                   ON A.id = B.packet_id
                 JOIN ".$aConfig['tabls']['prefix']."products_extra_categories AS C
                   ON C.product_id = B.product_id
                 LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
                   ON I.product_id = A.id 
                 WHERE 
                  (A.prod_status = '1' OR A.prod_status = '3')
                  AND B.product_id = ".$iId."
                  AND I.id IS NOT NULL 
                 GROUP BY A.id
                 ORDER BY A.id DESC".
                 ((int) $this->aSettings['items_in_box'] > 0 ? " LIMIT ".$this->aSettings['items_in_box'] : '');
      $aRecords = Common::GetAll($sSql);
      foreach($aRecords as $iKey => $aValue){
        // pobranie autorow itp
        $aRecords[$iKey]['authors'] =$this->getAuthors($aValue['id'], 1);

        //pobranie cennika
        $aRecords[$iKey]['tarrif'] = $this->getTarrif($aValue['id']);
        // przeliczenie i formatowanie cen
        $aRecords[$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aRecords[$iKey]['price_brutto'], $aRecords[$iKey]['tarrif'], $aRecords[$iKey]['promo_text'], NULL, $this->getSourceDiscountLimit($aValue['id'])));
        $aRecords[$iKey]['price_brutto'] = Common::formatPrice($aValue['price_brutto']);

        // okladka
        $aRecords[$iKey]['image'] =getItemImage('products_images',array('product_id'=> $aValue['id']),'__t_');
        // link do produktu
        $aRecords[$iKey]['link'] = $aConfig['common']['base_url_http_no_slash'].createProductLink($aValue['id'],$aValue['name']);
        $aRecords[$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aValue['id'], 'add');
        $aRecords[$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aValue['id'],'add_to_repository,');
      }
    }
		return $aRecords;
	} // end of getItems() method
} // end of Box_m_powiazane_oferta_produktowa_wpakiecie
?>