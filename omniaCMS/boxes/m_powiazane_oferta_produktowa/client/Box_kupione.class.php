<?php
/**
 * Klasa Box_m_oferta_produktowa_nowowsci do obslugi boksu Oferty produktowej
 * powiazane
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
include_once('modules/m_oferta_produktowa/client/Common.class.php');

class Box_m_powiazane_oferta_produktowa_kupione extends Common_m_oferta_produktowa {
  
  // Id boksu
  var $iId;
  
  // nazwa boksu
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // konfiguracja boksu
  var $aSettings;
  
  // opcja boksu
  var $sOption;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_powiazane_oferta_produktowa_kupione(&$aBox) {
		$this->iId = (double) $aBox['id'];
		$this->iPageId = (double) $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sModule =& $aBox['module'];
		$this->sOption = 'kupione';
		$this->sTemplatesPath = 'boxes/'.$aBox['module'].'/'.$this->sOption;
		$this->sTemplate = $aBox['template'];
		
		// pobranie konfiguracji boksu
		$this->aSettings =& $this->getSettings();
	} // end Box_m_oferta_produktowa_bestsellery() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig;

		$sSql = "SELECT items_in_box
						 FROM ".$aConfig['tabls']['prefix']."products_box_settings 
						 WHERE box_id = ".$this->iId;
		return Common::GetRow($sSql);
	} // end of getSettings() function
	

	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aSelected = array();
		$aBox = array();
		$aBox['lang'] =& $aConfig['lang']['box_'.$this->sModule.'_'.$this->sOption];
		
		if(($aConfig['_tmp']['page']['module']  == 'm_oferta_produktowa') && $_GET['id'] > 0) {
			$aBox['items'] = $this->getItems($_GET['id'],$aBox['name']);
			//dump($aBox['items']);
			if (!empty($aBox['items'])) {
			
				$pSmarty->assign_by_ref('aBox', $aBox);
				$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
				$pSmarty->clear_assign('aBox');
			}
		}
		return $sHtml;
	} // end of getContent()	

	
	/**
	 * Metoda pobiera i zwraca liste powiazanych produktow
	 * 
	 * @return	array	- lista produktow
	 */
	function &getItems($iId,&$sBoxName) {
		global $aConfig, $pSmarty;

		$sSql = "SELECT A.*, B.published
						FROM ".$aConfig['tabls']['prefix']."products_to_bought A
						JOIN ".$aConfig['tabls']['prefix']."products B
						ON A.bought_id = B.id
						WHERE A.product_id = ".$iId."
						AND B.published = '1'
						AND (B.prod_status = '1' OR B.prod_status = '3')
						LIMIT 0,".$this->aSettings['items_in_box'];
		$aResult =& Common::GetAll($sSql);
		
	if(!empty($aResult)){
		$sBoxName = $this->sName;
		foreach($aResult as $iKey => $aValue){
			$sSql = "SELECT A.id, A.title AS name, A.price, A.price_brutto, A.edition, A.shipment_time, 
											A.isbn, A.isbn_plain, A.publication_year, 
											A.prod_status, A.publisher AS publisher_name, A.series, A.is_news, A.is_bestseller, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							 	WHERE A.id = ".$aValue['bought_id'];
				$aLinked[$iKey] =& Common::GetRow($sSql);
				if(!empty($aLinked[$iKey])){
	
				$aLinked[$iKey]['authors'] = $this->getAuthors($aLinked[$iKey]['id'], 1);
	
				//pobranie cennika
				$aLinked[$iKey]['tarrif'] = $this->getTarrif($aLinked['id']);
				// przeliczenie i formatowanie cen
				$aLinked[$iKey]['promo_price'] = Common::formatPrice($this->getPromoPrice($aLinked[$iKey]['price_brutto'], $aLinked[$iKey]['tarrif'], $aLinked[$iKey]['promo_text'], NULL, $this->getSourceDiscountLimit($aValue['id'])));
				$aLinked[$iKey]['price_brutto'] = Common::formatPrice($aLinked['price_brutto']);
					
				// okladka
				$aLinked[$iKey]['image'] = getItemImage('products_images',array('product_id'=> $aLinked[$iKey]['id']),'__t_');
				// link do produktu
				$aLinked[$iKey]['link'] = $aConfig['common']['base_url_http'].createProductLink($aLinked[$iKey]['id'], $aLinked[$iKey]['plain_name']);
				$aLinked[$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aLinked[$iKey]['id'], 'add');
				$aLinked[$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aValue['id'],'add_to_repository,');
				} else {
					unset($aLinked[$iKey]);
				}
			}
	} else {
    $aLowestCategory = [];
		$sBoxName = $aConfig['lang']['box_'.$this->sModule.'_'.$this->sOption]['alternate_name'];

        $aLowestCategory = $this->getLowestCategory($iId);

		if(!empty($aLowestCategory)) {
		$sSql = "SELECT A.id, A.title AS name, A.price, A.price_brutto, A.edition, A.shipment_time, 
											A.isbn, A.isbn_plain, A.publication_year, 
											A.prod_status, A.publisher AS publisher_name, A.series, A.is_news, A.is_bestseller, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							  JOIN ".$aConfig['tabls']['prefix']."products_news AS B
								 ON A.id = B.product_id
								 JOIN ".$aConfig['tabls']['prefix']."products_extra_categories AS C
								 ON C.product_id = B.product_id
								 JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id 
						 	 WHERE (A.prod_status = '1' OR A.prod_status = '3')
						 	 AND A.id <> ".$iId."
						 	 AND C.page_id IN (".implode(',',$aLowestCategory).")
							 ORDER BY RAND()".
							 ((int) $this->aSettings['items_in_box'] > 0 ? " LIMIT ".$this->aSettings['items_in_box'] : '');
			$aLinked = Common::GetAll($sSql);
			foreach($aLinked as $iKey => $aValue){
				// pobranie autorow itp
				$aLinked[$iKey]['authors'] =$this->getAuthors($aValue['id'], 1);
				
				//pobranie cennika
				$aLinked[$iKey]['tarrif'] = $this->getTarrif($aValue['id']);
				// przeliczenie i formatowanie cen
				$aLinked[$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aLinked[$iKey]['price_brutto'], $aLinked[$iKey]['tarrif'], $aLinked[$iKey]['promo_text'], NULL, $this->getSourceDiscountLimit($aValue['id'])));
				$aLinked[$iKey]['price_brutto'] = Common::formatPrice($aValue['price_brutto']);
				
				// okladka
				$aLinked[$iKey]['image'] =getItemImage('products_images',array('product_id'=> $aValue['id']),'__t_');
				// link do produktu
				$aLinked[$iKey]['link'] = $aConfig['common']['base_url_http_no_slash'].createProductLink($aValue['id'],$aValue['name']);
				$aLinked[$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aValue['id'], 'add');
				$aLinked[$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aValue['id'],'add_to_repository,');
			}
		}
	}
		return $aLinked;
	} // end of getItems() method


    /**
     * @param $iId
     * @return array|bool
     */
    private function getLowestCategory($iId)
    {
        global $aConfig;

        $aLowestCategory = [];

        $sCSql = "SELECT A.page_id
                          FROM " . $aConfig['tabls']['prefix'] . "products_extra_categories AS A
						  JOIN " . $aConfig['tabls']['prefix'] . "menus_items AS B
						  ON B.id = A.page_id
						  WHERE A.product_id = " . $iId . "
						  AND B.parent_id IS NOT NULL
                          ORDER BY B.priority ASC";
//                          LIMIT 1
//                            ";
        $aCats = Common::GetCol($sCSql);
        if (!empty($aCats)) {
            $aLowestCategory = $aCats;
            $aCats = $this->findLowestCategory($iId, $aCats);
            if (!empty($aCats)) {
                $aLowestCategory = $aCats;
            }
        } else {
            $sCSql = "SELECT A.page_id
                              FROM " . $aConfig['tabls']['prefix'] . "products_extra_categories AS A
                              JOIN " . $aConfig['tabls']['prefix'] . "menus_items AS B
                              ON B.id = A.page_id
                              WHERE A.product_id = " . $iId . "
                              AND B.parent_id IS NOT NULL
                                ";
            $aCats = Common::GetCol($sCSql);
            if (!empty($aCats)) {
                $aLowestCategory = $aCats;
            }
        }

        return $aLowestCategory;
    }

    /**
     *
     * @param int $iId
     * @param array $aParentCats
     * @return boolean
     */
    private function findLowestCategory($iId, $aParentCats)
    {
        $sCSql = "SELECT A.page_id
                FROM products_extra_categories AS A
                  JOIN menus_items AS B
                   ON B.id = A.page_id
                   WHERE A.product_id = " . $iId . "
                    AND B.parent_id IN (" . implode(',', $aParentCats) . ")
                    ";
        $aCats = Common::GetCol($sCSql);
        if (!empty($aCats)) {
            $aLowerCat = $this->findLowestCategory($iId, $aCats);
            if (!empty($aLowerCat)) {
                return $aLowerCat;
            } else {
                return $aCats;
            }
        }
        return false;
    }
} // end of Box_m_oferta_produktowa_powiazane
?>