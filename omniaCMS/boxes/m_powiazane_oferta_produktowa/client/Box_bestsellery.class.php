<?php
/**
 * Klasa Box_m_oferta_produktowa_nowowsci do obslugi boksu Oferty produktowej - bestsellery
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

include_once('modules/m_oferta_produktowa/client/Common.class.php');

class Box_m_powiazane_oferta_produktowa_bestsellery extends Common_m_oferta_produktowa {
  
  // Id boksu
  var $iId;
  
  // Id menu
  var $iMenuId;
  
  // nazwa boksu
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // konfiguracja boksu
  var $aSettings;
  
  // opcja boksu
  var $sOption;
  
  var $sPageLink;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_powiazane_oferta_produktowa_bestsellery(&$aBox) {
		
		$this->iId =& $aBox['id'];
		$this->iMenuId = $aBox['menu_id'];
		$this->iPageId = $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sModule = $aBox['module'];
		$this->sOption = 'bestsellery';
		$this->sTemplatesPath = 'boxes/'.$aBox['module'].'/'.$this->sOption;
		$this->sTemplate = $aBox['template'];
		$this->sPageLink = '/'.$aBox['page_symbol'];
		// pobranie konfiguracji boksu
		$this->aSettings =& $this->getSettings();
	} // end Box_m_oferta_produktowa_bestsellery() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig;

		$sSql = "SELECT items_in_box
						 FROM ".$aConfig['tabls']['prefix']."products_box_settings 
						 WHERE box_id = ".$this->iId;
		return Common::GetRow($sSql);
	} // end of getSettings() function
	

	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aBox = array();
		
		if (!pageIsPublished($this->iPageId)) {
			// strona nie jest opublikowana - boks nie moze zostac wyswietlony
			return $sHtml;
		}
		
		$aBox['items'] = $this->getItems();
		if (!empty($aBox['items'])) {
			$aBox['name'] =& $this->sName;
			$aBox['link'] =& $this->sPageLink;
			$aBox['lang'] =& $aConfig['lang']['box_'.$this->sModule.'_'.$this->sOption];
			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
			$pSmarty->clear_assign('aBox');
		}	    
		return $sHtml;
	} // end of getContent()
	
	

	
	/**
	 * Metoda pobiera i zwraca liste bestsellerow
	 * 
	 * @return	array	- lista produktow
	 */
	function &getItems() {
		global $aConfig;

		$sSql = "SELECT A.id, A.title AS name, A.price, A.price_brutto, A.edition, A.shipment_time, 
											A.isbn, A.isbn_plain, A.publication_year, 
											A.prod_status, A.publisher AS publisher_name, A.series, A.is_news, A.is_bestseller, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
						 JOIN ".$aConfig['tabls']['prefix']."products_bestsellers B
						 ON A.id = B.product_id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id 
					 		 WHERE (A.prod_status = '1' OR A.prod_status = '3') AND I.id IS NOT NULL ".
									(!empty($this->iPageId)?" AND B.page_id = ".$this->iPageId:'')."
					 ORDER BY rand()".
					 ((int) $this->aSettings['items_in_box'] > 0 ? " LIMIT ".$this->aSettings['items_in_box'] : '');
		$aData = Common::GetAll($sSql);
		
		foreach($aData as $iKey => $aValue){
				// pobranie autorow itp
				$aData[$iKey]['authors'] =$this->getAuthors($aValue['id'], 1);
				
				//pobranie cennika
				$aData[$iKey]['tarrif'] = $this->getTarrif($aValue['id']);
				// przeliczenie i formatowanie cen
				$aData[$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aData[$iKey]['price_brutto'], $aData[$iKey]['tarrif'], $aData[$iKey]['promo_text'], NULL, $this->getSourceDiscountLimit($aValue['id'])));
				$aData[$iKey]['price_brutto'] = Common::formatPrice($aValue['price_brutto']);
				
				// okladka
				$aData[$iKey]['image'] =getItemImage('products_images',array('product_id'=> $aValue['id']),'__t_');
				// link do produktu
				$aData[$iKey]['link'] = $aConfig['common']['base_url_http_no_slash'].createProductLink($aValue['id'],$aValue['name']);
				$aData[$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aValue['id'], 'add');
				$aData[$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aValue['id'],'add_to_repository,');
			}
		return $aData;
	} // end of getItems() method
} // end of Box_m_oferta_produktowa_bestsellery
?>