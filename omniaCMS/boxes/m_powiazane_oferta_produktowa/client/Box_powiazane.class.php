<?php
/**
 * Klasa Box_m_oferta_produktowa_nowowsci do obslugi boksu Oferty produktowej
 * powiazane
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
include_once('modules/m_oferta_produktowa/client/Common.class.php');

class Box_m_powiazane_oferta_produktowa_powiazane extends Common_m_oferta_produktowa {
  
  // Id boksu
  var $iId;
  
  // nazwa boksu
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // konfiguracja boksu
  var $aSettings;
  
  // opcja boksu
  var $sOption;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_powiazane_oferta_produktowa_powiazane(&$aBox) {
		$this->iId = (double) $aBox['id'];
		$this->iPageId = (double) $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sOption = 'powiazane';
		$this->sTemplatesPath = 'boxes/'.$aBox['module'].'/'.$this->sOption;
		$this->sTemplate = $aBox['template'];
		
		// pobranie konfiguracji boksu
		$this->aSettings =& $this->getSettings();
	} // end Box_m_oferta_produktowa_bestsellery() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig;

		$sSql = "SELECT items_in_box
						 FROM ".$aConfig['tabls']['prefix']."products_box_settings 
						 WHERE box_id = ".$this->iId;
		return Common::GetRow($sSql);
	} // end of getSettings() function
	

	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aSelected = array();
		$aBox = array();
		
		if(($aConfig['_tmp']['page']['module']  == 'm_oferta_produktowa') && $_GET['id'] > 0) {
			$aBox['items'] = $this->getItems($_GET['id']);
			//dump($aBox['items']);
			if (!empty($aBox['items'])) {
				$aBox['name'] =& $this->sName;
				$pSmarty->assign_by_ref('aBox', $aBox);
				$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
				$pSmarty->clear_assign('aBox');
			}
		}
		return $sHtml;
	} // end of getContent()	
	

	
	/**
	 * Metoda pobiera i zwraca liste powiazanych produktow
	 * 
	 * @return	array	- lista produktow
	 */
	function &getItems($iId) {
		global $aConfig, $pSmarty;
		
		$sSql = "SELECT A.*
						FROM ".$aConfig['tabls']['prefix']."products_linked A
						JOIN ".$aConfig['tabls']['prefix']."products_shadow B
						ON A.linked_id = B.id
						 LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
						ON I.product_id = A.linked_id 
						WHERE A.product_id = ".$iId."
						AND (B.prod_status = '1' OR B.prod_status = '3') 
						AND I.id IS NOT NULL
						ORDER BY A.order_by
						LIMIT 0,".$this->aSettings['items_in_box'];
		$aResult =& Common::GetAll($sSql);
		
	if(!empty($aResult)){
	foreach($aResult as $iKey => $aValue){
		$sSql = "SELECT A.id, A.title AS name, A.price, A.price_brutto, A.edition, A.shipment_time, 
											A.isbn, A.isbn_plain, A.publication_year, 
											A.prod_status, A.publisher AS publisher_name, A.series, A.is_news, A.is_bestseller, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
						 	WHERE A.id = ".$aValue['linked_id'];
			$aLinked[$iKey] =& Common::GetRow($sSql);
			
			$aLinked[$iKey]['authors'] = $this->getAuthors($aLinked[$iKey]['id'], 1);
				
			//pobranie cennika
			$aLinked[$iKey]['tarrif'] = $this->getTarrif($aLinked[$iKey]['id']);
			// przeliczenie i formatowanie cen
			$aLinked[$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aLinked[$iKey]['price_brutto'], $aLinked[$iKey]['tarrif'], $aLinked[$iKey]['promo_text'], NULL, $this->getSourceDiscountLimit($aValue['id'])));
			$aLinked[$iKey]['price_brutto'] = Common::formatPrice($aLinked[$iKey]['price_brutto']);
				
			// okladka
			$aLinked[$iKey]['image'] = getItemImage('products_images',array('product_id'=> $aLinked[$iKey]['id']),'__t_');
			// link do produktu				
			$aLinked[$iKey]['link'] = $aConfig['common']['base_url_http_no_slash'].createProductLink($aLinked[$iKey]['id'], $aLinked[$iKey]['name']);
			$aLinked[$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aLinked[$iKey]['id'], 'add');
			$aLinked[$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aValue['id'],'add_to_repository,');
			
		}
	}
		return $aLinked;
	} // end of getItems() method
} // end of Box_m_oferta_produktowa_powiazane
?>