<?php
/**
 * Klasa Box_m_oferta_produktowa_nowowsci do obslugi boksu Oferty produktowej - promocje
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
include_once('modules/m_oferta_produktowa/client/Common.class.php');

class Box_m_powiazane_oferta_produktowa_selectedpromotions extends Common_m_oferta_produktowa {
  
  // Id boksu
  var $iId;
  
  // Id strony
  var $iPageId;
  
  // nazwa boksu
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // konfiguracja boksu
  var $aSettings;
  
  // opcja boksu
  var $sOption;
  
   var $sPageLink;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_powiazane_oferta_produktowa_selectedpromotions(&$aBox) {
		$this->iId =& $aBox['id'];
		$this->iPageId = (double) $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sModule = $aBox['module'];
		$this->sOption = 'selectedpromotions';
		$this->sTemplatesPath = 'boxes/'.$aBox['module'].'/'.$this->sOption;
		$this->sTemplate = $aBox['template'];
		$this->sPageLink = '/'.$aBox['page_symbol'];
		// pobranie konfiguracji boksu
		$this->aSettings =& $this->getSettings();
	} // end Box_m_oferta_produktowa_zapowiedzi() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig;
		
		$sSql = "SELECT items_in_box
						 FROM ".$aConfig['tabls']['prefix']."products_box_settings 
						 WHERE box_id = ".$this->iId;
		return Common::GetRow($sSql);
	} // end of getSettings() function
	
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aSelected = array();
		$aBox = array();
		
		if (!pageIsPublished($this->iPageId)) {
			// strona nie jest opublikowana - boks nie moze zostac wyswietlony
			return $sHtml;
		}
		
		$aBox['items'] = $this->getItems();
		if (!empty($aBox['items'])) {
			$aBox['name'] =& $this->sName;
			$aBox['link'] =& $this->sPageLink;
			$aBox['lang'] =& $aConfig['lang']['box_'.$this->sModule.'_'.$this->sOption];
			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
			$pSmarty->clear_assign('aBox');
		}
		//dump($aBox);
		return $sHtml;
	} // end of getContent()
	
	

	
	/**
	 * Metoda pobiera i zwraca liste produktow promocyjnych
	 * 
	 * @return	array	- lista produktow
	 */
	function &getItems() {
		global $aConfig;
		
		$sSql="SELECT A.id,A.name
					FROM ".$aConfig['tabls']['prefix']."products_promotions A
					WHERE A.show_in_box='1' ".
						 (!empty($this->iPageId)?" AND A.page_id = ".$this->iPageId:'')."
						 ORDER BY A.order_by";
		$aPromotions=Common::GetAll($sSql);
		
		foreach($aPromotions as $iPKey => $aPromo){
			$iPromoSettingNum=Common::GetOne('SELECT boxOpt FROM '.$aConfig['tabls']['prefix'].'products_promotions WHERE id='.$aPromo['id']);
			if($iPromoSettingNum>0)
				$this->aSettings['items_in_box']=$iPromoSettingNum;
			$sSql = "SELECT A.id, A.title AS name, A.price, A.price_brutto, A.edition, A.shipment_time, 
											A.isbn, A.isbn_plain, A.publication_year, 
											A.prod_status, A.publisher AS publisher_name, A.series, A.is_news, A.is_bestseller, A.is_previews
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
								 JOIN ".$aConfig['tabls']['prefix']."products_tarrifs B
								 ON B.product_id=A.id
								 LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id 
								 WHERE B.start_date <= UNIX_TIMESTAMP() AND B.end_date >= UNIX_TIMESTAMP() AND B.placeOnMain='1'
								 AND B.promotion_id = ".$aPromo['id']."
								 AND (A.prod_status = '1' OR A.prod_status = '3')
								 AND I.id IS NOT NULL 
								  GROUP BY A.id
								 ORDER BY A.order_by DESC".
							 ((int) $this->aSettings['items_in_box'] > 0 ? " LIMIT ".$this->aSettings['items_in_box'] : '');
			$aPromotions[$iPKey]['items'] = Common::GetAll($sSql);
			if(empty($aPromotions[$iPKey]['items'])) {
				unset($aPromotions[$iPKey]);
			} else {
				$aPromotions[$iPKey]['link'] = createLink($this->sPageLink, 'id'.$aPromo['id'], $aPromo['name']);

				foreach($aPromotions[$iPKey]['items'] as $iKey => $aValue){
					// pobranie autorow itp
					$aPromotions[$iPKey]['items'][$iKey]['authors'] = $this->getAuthors($aValue['id'], 1);

					//pobranie cennika
					$aPromotions[$iPKey]['items'][$iKey]['tarrif'] = $this->getTarrif($aValue['id']);
					// przeliczenie i formatowanie cen
					$aPromotions[$iPKey]['items'][$iKey]['promo_price'] = Common::formatPrice2($this->getPromoPrice($aPromotions[$iPKey]['items'][$iKey]['price_brutto'], $aPromotions[$iPKey]['items'][$iKey]['tarrif'], $aPromotions[$iPKey]['items'][$iKey]['promo_text'], NULL, $this->getSourceDiscountLimit($aValue['id'])));
					$aPromotions[$iPKey]['items'][$iKey]['price_brutto'] = Common::formatPrice($aValue['price_brutto']);
						
					// okladka
					//$aData[$iKey]['image'] =& getProductThumb($aValue['id']);
					$aPromotions[$iPKey]['items'][$iKey]['image'] =getItemImage('products_images',array('product_id'=> $aValue['id']),'__t_');
					// link do produktu
					$aPromotions[$iPKey]['items'][$iKey]['link'] = $aConfig['common']['base_url_http_no_slash'].createProductLink($aValue['id'],$aValue['name']);
					$aPromotions[$iPKey]['items'][$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aValue['id'], 'add');
					$aPromotions[$iPKey]['items'][$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aValue['id'],'add_to_repository,');
				
				}	
			}
		}
		//dump($aPromotions);
		return $aPromotions;		
		
	} // end of getItems() method	
} // end of Box_m_oferta_produktowa_promocje Class
?>