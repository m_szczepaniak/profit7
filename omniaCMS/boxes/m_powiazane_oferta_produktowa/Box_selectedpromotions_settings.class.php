<?php
/**
 * Klasa Settings do obslugi konfiguracji boksu zapowiedzi 'Oferty produktowej'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Settings {
  
  // nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;
	
	// Id kategorii dla ktorej edytowane sa ustawienia
	var $iId;
	
	// Id modulu aktualnosci
	var $iModuleId;
	
	// opcja boksu
	var $sOption;
		
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$iModuleId, &$sModule, &$iLangId, &$iId, $sOption='') {
		global $aConfig;
		$this->iModuleId = $iModuleId;
		$this->sModule = $sModule;
		$this->iLangId = $iLangId;
		$this->iId = $iId;
		$this->sOption = $sOption;
				
		// pobranie konfiguracji dla modulu
		$sSql = "SELECT A.*, B.page_id 
						 FROM ".$aConfig['tabls']['prefix']."products_box_settings A
						 JOIN ".$aConfig['tabls']['prefix']."boxes B
						 ON B.id = A.box_id 
						 WHERE A.box_id = ".$this->iId;
		$aCfg = Common::GetRow($sSql);
		
		// dolaczenie ustawien do glownej tablicy ustawien $aConfig
		$aConfig['settings'][$this->sModule] =& $aCfg;
	} // end Settings() function
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach boksu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;
		
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products_box_settings
						 WHERE box_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);

		$aValues = array(
			'items_in_box' => (int) $_POST['items_in_box']
		);
		if (!empty($aSettings)) {
			// Update
			return Common::Update($aConfig['tabls']['prefix']."products_box_settings",
														$aValues,
														"box_id = ".$this->iId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('box_id' => $this->iId), $aValues);

			return Common::Insert($aConfig['tabls']['prefix']."products_box_settings",
														$aValues, "", false) !== false;
		}
	} // end of UpdateSettings() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji konfiguracji boksu
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;
				
		$aData = array();
		$sHtml = '';
		
		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_type') {
			$aData =& $_POST;
		}
		$aLang =& $aConfig['lang'][$this->sModule.'_common_box_settings'];
		
		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['settings'], array('class'=>'merged', 'style'=>'padding-left: 175px'));
				
		/* strony
		$aPages =& getModulePages($this->iModuleId, $this->iLangId, true, '', $this->sOption);
		$aPages = array_merge(
			array(array('value'=>'0', 'label'=>$aConfig['lang']['common']['choose'])),
			$aPages
		);*/
		
		//$aPages = $this->getProductsCategories(Common::getProductsMenu($this->iLangId), $aConfig['lang']['common']['choose']);
		
		$pForm->AddSelect('page_id', $aLang['page_id'], array(), array_merge(array(array('value'=>0,'label'=>$aConfig['lang']['common']['choose'])),getModulePages('m_powiazane_oferta_produktowa',$this->iLangId,true,'','promocje')), $aData['page_id'], '', false);
		
		// liczba produktow w boksie
		$pForm->AddText('items_in_box', $aLang['items_in_box'], $aData['items_in_box'], array('maxlength'=>3, 'style'=>'width: 30px;'), '', 'uinteger');
	} // end of SettingsForm() function
	

} // end of Settings Class
?>