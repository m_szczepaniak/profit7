<?php
/**
 * Klasa Settings do obslugi konfiguracji boksu domyslnego 'Oferty produktowej'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Settings {
  
  // nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;
	
	// Id kategorii dla ktorej edytowane sa ustawienia
	var $iId;
	
	// Id modulu aktualnosci
	var $iModuleId;
		
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$iModuleId, &$sModule, &$iLangId, &$iId) {
		global $aConfig;
		$this->iModuleId = $iModuleId;
		$this->sModule = $sModule;
		$this->iLangId = $iLangId;
		$this->iId = $iId;
				
		// pobranie konfiguracji dla modulu
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."products_box_settings 
						 WHERE box_id = ".$this->iId;
		$aCfg = Common::GetRow($sSql);
		if (!isset($aCfg['subcategories']) || intval($aCfg['subcategories']) < 0) {
			$aCfg['subcategories'] = 5;
		}
		
		// dolaczenie ustawien do glownej tablicy ustawien $aConfig
		$aConfig['settings'][$this->sModule] =& $aCfg;
	} // end Settings() function
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach boksu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;
		
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."products_box_settings
						 WHERE box_id = ".$this->iId."";
		$aSettings =& Common::GetRow($sSql);
		
		if (!empty($aSettings)) {
			// update
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."products_box_settings SET
							 			menu_id = ".$_POST['menu_id'].",
							 			subcategories = ".$_POST['subcategories']."
						 WHERE box_id = ".$this->iId;
			if (($mResult = Common::Query($sSql)) === false) {
				return false;
			}
		}
		else {
			// nie zaktualizowano ustawien - nie ma ich jeszcze w bazie danych
			// insert
			$sSql = "INSERT INTO ".$aConfig['tabls']['prefix']."products_box_settings (
											box_id,
							 				language_id,
							 				menu_id,
							 				subcategories
							 )
							 VALUES (
							 				".$this->iId.",
							 				".$this->iLangId.",
							 				".$_POST['menu_id'].",
							 				".$_POST['subcategories']."
							 )";
			if (Common::Query($sSql) === false) {
				return false;
			}
		}
		return true;
	} // end of UpdateSettings() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji konfiguracji boksu
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;
		
		$aData = array();
		$sHtml = '';
		
		$aData =& $aConfig['settings'][$this->sModule];
		if (!empty($_POST) && $_POST['do'] != 'change_type') {
			$aData =& $_POST;
		}
		
		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['settings'], array('class'=>'merged', 'style'=>'padding-left: 175px'));
		
		// pobranie listy menu
		$sSql = "SELECT id AS value, name AS label
						 FROM ".$aConfig['tabls']['prefix']."menus
						 ORDER BY id DESC";
		$aMenus = array_merge(array(array('value' => '0', 'label' => $aConfig['lang']['common']['choose'])), Common::GetAll($sSql));
		$pForm->AddSelect('menu_id', $aConfig['lang'][$this->sModule.'_box_settings']['offer_menu'], array(), $aMenus, $aData['menu_id'], '', false);
		
		// liczba podkategorii w kategorii glownej
		$pForm->AddText('subcategories', $aConfig['lang'][$this->sModule.'_box_settings']['subcategories'], $aData['subcategories'], array('maxlength'=>3, 'style'=>'width: 30px;'), '', 'uinteger');
	} // end of SettingsForm() function
} // end of Settings Class
?>