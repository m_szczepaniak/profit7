<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - bestsellery
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['box_m_powiazane_oferta_produktowa_bestsellery']['no_data'] = 'W chwili obecnej nie posiadamy żadnych bestsellerów.';
$aConfig['lang']['box_m_powiazane_oferta_produktowa_bestsellery']['price_brutto'] = "Twoja cena";
$aConfig['lang']['box_m_powiazane_oferta_produktowa_bestsellery']['old_price_brutto'] = "Cena katalogowa";
?>