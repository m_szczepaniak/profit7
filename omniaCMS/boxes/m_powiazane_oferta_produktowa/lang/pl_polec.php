<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - polec znajomemu
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['s_mail']	= 'E-mail nadawcy';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['s_name']	= 'Imię i nazwisko nadawcy';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['r_mail']	= 'E-mail odbiorcy';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['description']	= 'Komentarz';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['mail_title'] = 'Twój znajomy poleca Ci książkę C.H. Beck';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['pages']	= 'Ilość stron: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['isbn']	= 'ISBN: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['series']	= 'Seria wydawnicza: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['issue']	= 'Wydanie ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['index']	= 'Indeks rzeczowy: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['notify_friend']	= 'Powiadom znajomego nt.: ';
$aConfig['lang']['mod_m_powiazane_oferta_produktowa']['send']	= 'Powiadomienie zostało wysłane';
?>