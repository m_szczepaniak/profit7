<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - bestsellery
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['box_m_powiazane_oferta_produktowa_wpakiecie']['no_data'] = 'W chwili obecnej nie posiadamy żadnych nowości.';

$aConfig['lang']['box_m_powiazane_oferta_produktowa_wpakiecie']['publication_year']	= 'Rok wydania: ';
$aConfig['lang']['box_m_powiazane_oferta_produktowa_wpakiecie']['language'] = "Język książki";
$aConfig['lang']['box_m_powiazane_oferta_produktowa_wpakiecie']['price_brutto'] = "Twoja cena";
$aConfig['lang']['box_m_powiazane_oferta_produktowa_wpakiecie']['old_price_brutto'] = "Cena katalogowa";
$aConfig['lang']['box_m_powiazane_oferta_produktowa_wpakiecie']['przechowalnia'] = 'dodaj do przechowalni';
?>