<?php
/**
* Plik jezykowy modulu 'Powiazane oferta produktowa' - promocje
* jezyk polski
*
* @author Marcin Korecki <m.korecki@omnia.pl>
* @version 1.0
*
*/

$aConfig['lang']['mod_m_powiazane_oferta_produktowa_promocje']['no_data'] = 'W chwili obecnej nie posiadamy żadnych produktów w promocji.';
?>