<?php
/**
 * Klasa pobiera komunikaty z sesji i dynamicznie je wyrzuca
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-05-13 
 * @copyrights Marcin Chudy - Profit24.pl
 */
 

class Box_e_komunikaty {
  
  // Id boksu
  var $iId;
  
  // Id wersji jezykowej
  var $iLangId;
  
  // symbol wersji jezykowej
  var $sLang;
  
  // Id menu
  var $iMenuId;
  
  // nazwa boksu
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // konfiguracja boksu
  var $aSettings;
  
  // opcja boksu
  var $sOption;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_e_koszyk(&$aBox) {
		$this->iId =& $aBox['id'];
		$this->iLangId =& $_GET['lang_id'];
		$this->sLang =& $_GET['lang'];
		$this->sName = $aBox['name'];
		$this->sSymbol = $aBox['symbol'];
		$this->sTemplatesPath = 'boxes/empty';
		$this->sTemplate = $aBox['template'];
	} // end Box_m_oferta_produktowa_promocje() function
	
		
	/**
	 * Metoda pobiera z pliku XML strukture menu o Id $this->iMenuId,
	 * dowiazuje ja do Smartow oraz zwraca xHTML z elementami menu po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aSelected = array();
		$aBox = array();
		$aBox['lang'] =& $aConfig['lang']['box_koszyk_info'];	
		
    // KOMUNIKAT
    if(isset($_SESSION['_tmp']['message'])){
      $aConfig['_tmp']['message'] = $_SESSION['_tmp']['message'];
      unset($_SESSION['_tmp']['message']);
    }
    if (isset($aConfig['_tmp']['message'])) {
      $pSmarty->assign_by_ref('sMsgText', $aConfig['_tmp']['message']['text']);
      $pSmarty->assign_by_ref('iCloseAfterSeconds', $aConfig['_tmp']['message']['close_afer_seconds']);
      if (isset($aConfig['_tmp']['message']['error']) &&
          $aConfig['_tmp']['message']['error']) {
        $sHtml = $pSmarty->fetch('error.tpl');
      }
      else {
        $sHtml = $pSmarty->fetch('message.tpl');
      }
      $pSmarty->clear_assign('sMsgText');
    }
		return $sHtml;
	} // end of getContent()
} // end of Box_e_koszyk Class
?>