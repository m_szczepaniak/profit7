<?php
/**
 * Klasa Box_m_oferta_produktowa_ukcje do obslugi boksu Oferty produktowej - aukje
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 

class Box_e_koszyk {
  
  // Id boksu
  var $iId;
  
  // Id wersji jezykowej
  var $iLangId;
  
  // symbol wersji jezykowej
  var $sLang;
  
  // Id menu
  var $iMenuId;
  
  // nazwa boksu
  var $sName;
  
  // sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // konfiguracja boksu
  var $aSettings;
  
  // opcja boksu
  var $sOption;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_e_koszyk(&$aBox) {
		$this->iId =& $aBox['id'];
		$this->iLangId =& $_GET['lang_id'];
		$this->sLang =& $_GET['lang'];
		$this->sName = $aBox['name'];
		$this->sSymbol = $aBox['symbol'];
		$this->sTemplatesPath = 'boxes/empty';
		$this->sTemplate = $aBox['template'];
	} // end Box_m_oferta_produktowa_promocje() function
	
		
	/**
	 * Metoda pobiera z pliku XML strukture menu o Id $this->iMenuId,
	 * dowiazuje ja do Smartow oraz zwraca xHTML z elementami menu po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aSelected = array();
		$aBox = array();
		$aBox['lang'] =& $aConfig['lang']['box_koszyk_info'];	
		
		if($_SESSION['wu_cart']['just_added'] == 1){
			$aBox['type'] = 1;
			$aBox = array_merge($aBox, $this->getLastItemName());
			$_SESSION['wu_cart']['just_added'] = 0;
			
			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
			$pSmarty->clear_assign('aBox');
		}
		elseif ($_SESSION['repository']['just_added'] == 1) {
			$aBox['type'] = 2;
			$aBox = array_merge($aBox, $this->getLastItemNameRepository());
			$_SESSION['repository']['just_added'] = 0;
			
			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
			$pSmarty->clear_assign('aBox');
		}
		elseif (isset($_SESSION['special_user_priv_notif'])) {
			$aBox['type'] = 5;
			$aBox['name'] =$aBox['lang']['special_user_priv_notif'];
			foreach($_SESSION['special_user_priv_notif'] as $iKey=>$sNotif){
				$aBox['notifs'][$iKey]=$aBox['lang']['user_priv_notif'][$sNotif];
			}
			$aBox['footer'] = sprintf($aBox['lang']['special_user_priv_notif_footer'],$aConfig['_tmp']['cart_symbol']);
			// usuniecie zmiennej
			unset($_SESSION['special_user_priv_notif']);

			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
			$pSmarty->clear_assign('aBox');
		}
		elseif (isset($_SESSION['notification'])) {
			$aBox['type'] = 3;
			$aBox['name'] = $aBox['lang']['notification_'.$_SESSION['notification']];
			// usuniecie zmiennej
			unset($_SESSION['notification']);
			
			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
			$pSmarty->clear_assign('aBox');
		}
		elseif (isset($_SESSION['logout_info'])) {
			$aBox['type'] = 4;
			$aBox['name'] = $aBox['lang']['logout_info'];
			unset($_SESSION['logout_info']);

			$pSmarty->assign_by_ref('aBox', $aBox);
			$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
			$pSmarty->clear_assign('aBox');
		}
		
		return $sHtml;
	} // end of getContent()
	
 	/**
	 * Metoda zwraca nazwe ostatnio dodanego przedmiotu
	 */
	function getLastItemName() {
		
		$aLast['name'] = $_SESSION['wu_cart']['last_item_name'];
		$aLast['link'] = $_SESSION['wu_cart']['last_item_link'];
		
		return $aLast;
	}
	
	/**
	 * Metoda zwraca nazwe oraz link do ostatnio dodanego do przechowalni produktu
	 */
	 function getLastItemNameRepository(){
	 	global $aConfig;
	 	list($iId,$iIt)=split("_", $_SESSION['repository']['last_item'], 2);
	 	$sSql = "SELECT A.name, A.id, B.symbol
	 					FROM ".$aConfig['tabls']['prefix']."products A
	 					JOIN ".$aConfig['tabls']['prefix']."menus_items B
	 					ON A.page_id = B.id
	 					WHERE A.id = ".$iId;
	 					
	 	$aData =& Common::GetRow($sSql);	 	
	 	$aData['link'] = createLink('/'.$aData['symbol'], 'id'.$aData['id'], $aData['name']);
	 	return $aData;	
	 }
} // end of Box_e_koszyk Class
?>