<?php
$aConfig['lang']['box_koszyk_info']['just_added'] = 'Dodano do <a href="/koszyk">koszyka</a>: ';
$aConfig['lang']['box_koszyk_info']['just_added_repository'] = 'Dodano do <a href="/schowek">schowka</a>: ';
$aConfig['lang']['box_koszyk_info']['notification_4'] = "Musisz podać numer telefonu lub adres e-mail!";
$aConfig['lang']['box_koszyk_info']['notification_5'] = "Podany numer telefonu jest niepoprawny! Powinien być formatu: +48 123 456 789";
$aConfig['lang']['box_koszyk_info']['notification_6'] = "Podany adres e-mail jest niepoprawny!";
$aConfig['lang']['box_koszyk_info']['notification_3'] = "Twój adres e-mail został dodany do bazy.<br />Powiadomimy Cię gdy książka będzie dostępna w sprzedaży";
$aConfig['lang']['box_koszyk_info']['notification_7'] = "Nie udało się dodać danych do bazy danych!<br />Spróbuj ponownie";
$aConfig['lang']['box_koszyk_info']['notification_9'] = 'Uwaga, w Twoim koszyku znajdują się także książki, które dodałeś podczas poprzedniej wizyty. Jeśli chcesz zmienić jego zawartość - <a href="/koszyk">kliknij tutaj</a>';
$aConfig['lang']['box_koszyk_info']['notification_8'] = "Kod z obrazka jest nieprawidłowy!";
$aConfig['lang']['box_koszyk_info']['logout_info'] = "Zostałeś wylogowany";
$aConfig['lang']['box_koszyk_info']['special_user_priv_notif'] = "Uwaga, masz możliwość skorzystania z:";
$aConfig['lang']['box_koszyk_info']['special_user_priv_notif_footer'] = 'Jeśli chesz wybrać którąś z opcji <a href="%s">kliknij tutaj</a>';
$aConfig['lang']['box_koszyk_info']['user_priv_notif']['14days'] = "formy płatności \"Przelew 14 dni\"";
$aConfig['lang']['box_koszyk_info']['user_priv_notif']['personal_reception'] = "metody transportu \"Odbiór osobisty\"";

?>