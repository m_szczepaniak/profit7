<?php
/**
 * Klasa Module do obslugi konfiguracji boksu menu
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Settings {
  
  // nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;
	
	// Id kategorii dla ktorej edytowane sa ustawienia
	var $iId;
	
	// konfiguracja boksu
	var $aSettings;
	
		
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings($sModule, &$iLangId, &$iId) {
		global $aConfig;
		$this->sModule = $sModule;
		$this->iLangId = $iLangId;
		$this->iId = $iId;
		$this->aSettings =& $this->getSettings();
	} // end Settings() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	void
	 */
	function &getSettings() {
		global $aConfig;
		
		// pobranie konfiguracji dla modulu
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."menus_box_settings
						 WHERE box_id = ".$this->iId;
		return Common::GetRow($sSql);
	} // end of getSettings() method
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach boksu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;
		
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."menus_box_settings
						 WHERE box_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);
		
		$aValues = array(
			'expand_levels' => $_POST['expand_levels'],
			'expand_from_level' => $_POST['expand_from_level'],
			'expand_first_level' => $_POST['expand_first_level']
		);
		if (!empty($aSettings)) {
			// Update
			return Common::Update($aConfig['tabls']['prefix']."menus_box_settings",
														$aValues,
														"box_id = ".$this->iId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('box_id' => $this->iId), $aValues);
			
			return Common::Insert($aConfig['tabls']['prefix']."menus_box_settings",
														$aValues) !== false;
		}
	} // end of UpdateSettings() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji konfiguracji boksu
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;
		
		$aData = array();
		$sHtml = '';
		
		$aData =& $this->aSettings;
		if (!empty($_POST) && $_POST['do'] != 'change_type') {
			$aData =& $_POST;
		}
		
		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['settings'], array('class'=>'merged', 'style'=>'padding-left: 165px'));
		
		// liczba poziomow rozwiniecia menu
		$aMenuLevels = array(
			array('value' => 1, 'label'	=> 1),
			array('value' => 2, 'label'	=> 2),
			array('value' => 3, 'label'	=> 3),
			array('value' => 4, 'label'	=> 4),
			array('value' => 5, 'label'	=> 5)
		);
		$pForm->AddSelect('expand_levels', $aConfig['lang'][$this->sModule.'_box_settings']['expand_levels'], array(), $aMenuLevels, $aData['expand_levels'], '', false);
		
		// liczba poziomow rozwiniecia menu
		$aExpandFromLevel = array(
			array('value' => 1, 'label'	=> 1),
			array('value' => 2, 'label'	=> 2),
			array('value' => 3, 'label'	=> 3),
			array('value' => 4, 'label'	=> 4)
		);
		$pForm->AddSelect('expand_from_level', $aConfig['lang'][$this->sModule.'_box_settings']['expand_from_level'], array(), $aExpandFromLevel, $aData['expand_from_level'], '', false);
		
		// wyswietla pierwszy poziom menu rozwiniety - stosowane w polaczeniu z rozwijanym / zwijanym menu
		// gdy opcja "liczba poziomow rozwiniecia menu" jest wykorzystywana do wygenerowania
		// listy wszystkich stron i podstron dla zwijaneg / rozwijanego menu
		$pForm->AddCheckBox('expand_first_level', $aConfig['lang'][$this->sModule.'_box_settings']['expand_first_level'], array(), '', !empty($aData['expand_first_level']), false);
	} // end of SettingsForm() function
} // end of Settings Class
?>