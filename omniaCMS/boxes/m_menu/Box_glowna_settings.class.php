<?php
/**
 * Klasa Module do obslugi konfiguracji boksu menu
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Settings {
  
  // nazwa modulu - do langow
	var $sModule;
	
	// ID wersji jezykowej
	var $iLangId;
	
	// Id kategorii dla ktorej edytowane sa ustawienia
	var $iId;
	
	// konfiguracja boksu
	var $aSettings;
	
		
  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$iModuleId, &$sModule, &$iLangId, &$iId) {
		global $aConfig;
		$this->iModuleId = $iModuleId;
		$this->sModule = $sModule;
		$this->iLangId = $iLangId;
		$this->iId = $iId;
		$this->aSettings =& $this->getSettings();
	} // end Settings() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	void
	 */
	function &getSettings() {
		global $aConfig;
		
		// pobranie konfiguracji dla modulu
		$sSql = "SELECT * 
						 FROM ".$aConfig['tabls']['prefix']."menus_box_glowna_settings
						 WHERE box_id = ".$this->iId;
		return Common::GetRow($sSql);
	} // end of getSettings() method
	
	
	/**
	 * Metoda wprowadza zmiany w ustawieniach boksu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;
		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."menus_box_glowna_settings
						 WHERE box_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);
		
		$aValues = array(
			'menu1_id' => $_POST['menu1_id']
		);

		if (!empty($aSettings)) {
			// Update
			return Common::Update($aConfig['tabls']['prefix']."menus_box_glowna_settings",
														$aValues,
														"box_id = ".$this->iId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('box_id' => $this->iId), $aValues);
			
			return Common::Insert($aConfig['tabls']['prefix']."menus_box_glowna_settings",
														$aValues,'',false) !== false;
		}
	} // end of UpdateSettings() funciton
	
	
	/**
	 * Metoda tworzy formularz edycji konfiguracji boksu
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;
		
		$aData = array();
		$sHtml = '';
		
		$aData =& $this->aSettings;
		if (!empty($_POST) && $_POST['do'] != 'change_type') {
			$aData =& $_POST;
		}
		
		// naglowek konfiguracji ustawien
		//$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['settings'], array('class'=>'merged', 'style'=>'padding-left: 165px'));

		$pForm->AddSelect('menu1_id', $aConfig['lang'][$this->sModule.'_box_settings']['menu'], array(), $this->getProductMenus(), $aData['menu1_id'], '', true);
		
		// wyswietla pierwszy poziom menu rozwiniety - stosowane w polaczeniu z rozwijanym / zwijanym menu
		// gdy opcja "liczba poziomow rozwiniecia menu" jest wykorzystywana do wygenerowania
		// listy wszystkich stron i podstron dla zwijaneg / rozwijanego menu
		//$pForm->AddCheckBox('expand_first_level', $aConfig['lang'][$this->sModule.'_box_settings']['expand_first_level'], array(), '', !empty($aData['expand_first_level']), false);
	} // end of SettingsForm() function
	
	function getProductMenus(){
		global $aConfig;
		$sSql = "SELECT id AS value, name AS label
						 FROM ".$aConfig['tabls']['prefix']."menus
						 WHERE products = '1'";
		return Common::GetAll($sSql);
	}
} // end of Settings Class
?>