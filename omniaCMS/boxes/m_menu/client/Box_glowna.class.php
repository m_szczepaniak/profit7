<?php
/**
 * Klasa Box_m_menu do obslugi boksu menu
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Box_m_menu_glowna {

  // Id boksu
  var $iId;

  // Id menu
  var $iMenuId;

  // nazwa boksu
  var $sName;

  // sciezka do katalogu z szablonami
  var $sTemplatesPath;

  // szablon boksu
  var $sTemplate;

  // konfiguracja boksu
  var $aSettings;

  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_menu_glowna(&$aBox) {
				global $aConfig;
		
		$this->iId =& $aBox['id'];
		$this->sName = $aBox['name'];
		$this->sTemplatesPath = 'boxes/'.$aBox['module'].'/'.$aBox['moption'];
		$this->sTemplate = $aBox['template'];

		// pobranie konfiguracji boksu
		$this->aSettings = $this->getSettings();


	} // end Box_m_menu() function


	/**
	 * Metoda pobiera konfiguracje dla boksu
	 *
	 * @return	array
	 */
	function getSettings() {
		global $aConfig;

		$aSetttings=array();
		
		$sSql = "SELECT A.menu1_id, A.box_id, C.id AS top_cat 
						 FROM ".$aConfig['tabls']['prefix']."menus_box_glowna_settings A
						 JOIN ".$aConfig['tabls']['prefix']."boxes B
						 ON B.id=A.box_id
						 JOIN ".$aConfig['tabls']['prefix']."menus_items C
						 ON C.menu_id=A.menu1_id
						 WHERE  C.parent_id IS NULL
						 ORDER BY B.order_by";
		$aSetttings['menu'] = Common::GetAll($sSql);

		foreach($aSetttings['menu'] as $iKey=>$aItem) {
			if($aItem['box_id'] == $this->iId) {
				$aSetttings['nr'] = $iKey;
			}
		}
		
		if($aConfig['_tmp']['path'][0]['menu_id'] == $aSetttings['menu'][$aSetttings['nr']]['menu1_id']){
			AddCookie('box_categories',strval($aSetttings['nr']));
		}

		if(!isset($_COOKIE['box_categories'])){
			AddCookie('box_categories','0');
		}
		return $aSetttings;
	} // end of getSettings() function


	/**
	 * Metoda pobiera z pliku XML strukture menu o Id $this->iMenuId,
	 * dowiazuje ja do Smartow oraz zwraca xHTML z elementami menu po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aBox = array();

		$aLevels[0] = 0;
		$aLevels[1] = 0;
		$aLevels[2] = 0;

		$aBox['selected']=0;
		if(($aConfig['_tmp']['path'][0]['menu_id'] == $this->aSettings['menu'][$this->aSettings['nr']]['menu1_id'])
		||
		(($aConfig['_tmp']['path'][0]['module'] != 'm_oferta_produktowa') && (intval($_COOKIE['box_categories']) == $this->aSettings['nr']))){

			$aBox['selected']=$this->aSettings['nr'];
			$aLevels[$this->aSettings['nr']] = 1;

			// pobranie menu
			$aMenu1 =& $this->getMenuTree($this->aSettings['menu'][0]['menu1_id'],0, $aLevels[0]);
			$aMenu2 =& $this->getMenuTree($this->aSettings['menu'][1]['menu1_id'], 0, $aLevels[1]);
			$aMenu3 =& $this->getMenuTree($this->aSettings['menu'][2]['menu1_id'], 0, $aLevels[2]);
	
			// sprawdź poziom drugi zagłębienia
			$iLevel = 2;
			
			if(!empty($aConfig['_tmp']['path'][$iLevel])){
				if($aConfig['_tmp']['path'][1]['menu_id'] == $this->aSettings['menu'][0]['menu1_id']){
					
					$aSubMenu=$this->getMenuTree($this->aSettings['menu'][0]['menu1_id'], $aConfig['_tmp']['path'][$iLevel]['id'], 2);
					if(!empty($aSubMenu)){
						$aMenu1[0]['children']=$aSubMenu;
						$aMenu1[0]['returnlink']=$aMenu1[0]['link'];
					} else {
						$aSubMenu=$this->getMenuTree($this->aSettings['menu'][0]['menu1_id'], $aConfig['_tmp']['path'][$iLevel-1]['id'], 2);
						if(!empty($aSubMenu)){
							$aMenu1[0]['children']=$aSubMenu;
							$aMenu1[0]['returnlink']=$aMenu1[0]['link'];
						}
					}
				}
				elseif($aConfig['_tmp']['path'][1]['menu_id'] == $this->aSettings['menu'][1]['menu1_id']){
					$aSubMenu=$this->getMenuTree($this->aSettings['menu'][1]['menu1_id'], $aConfig['_tmp']['path'][$iLevel]['id'], 2);
					if(!empty($aSubMenu)){
						$aMenu2[0]['children']=$aSubMenu;
						$aMenu2[0]['returnlink']=$aMenu2[0]['link'];
					} else {
						$aSubMenu=$this->getMenuTree($this->aSettings['menu'][1]['menu1_id'], $aConfig['_tmp']['path'][$iLevel-1]['id'], 2);
						if(!empty($aSubMenu)){
							$aMenu2[0]['children']=$aSubMenu;
							$aMenu2[0]['returnlink']=$aMenu2[0]['link'];
						}
					}
				}
				elseif($aConfig['_tmp']['path'][1]['menu_id'] == $this->aSettings['menu'][2]['menu1_id']){
					$aSubMenu=$this->getMenuTree($this->aSettings['menu'][2]['menu1_id'], $aConfig['_tmp']['path'][$iLevel]['id'], 2);
					if(!empty($aSubMenu)){
						$aMenu3[0]['children']=$aSubMenu;
						$aMenu3[0]['returnlink']=$aMenu3[0]['link'];
					} else {
						$aSubMenu=$this->getMenuTree($this->aSettings['menu'][2]['menu1_id'], $aConfig['_tmp']['path'][$iLevel-1]['id'], 2);
						if(!empty($aSubMenu)){
							$aMenu3[0]['children']=$aSubMenu;
							$aMenu3[0]['returnlink']=$aMenu3[0]['link'];
						}
					}
				}
			} else {
				// sprawdź poziom pierwszy zagłębienia
				$iLevel = 1;
				if (!empty($aConfig['_tmp']['path'][$iLevel])) {
					if($aConfig['_tmp']['path'][$iLevel]['menu_id'] == $this->aSettings['menu'][0]['menu1_id']){
						$aSubMenu=$this->getMenuTree($this->aSettings['menu'][0]['menu1_id'], $aConfig['_tmp']['path'][$iLevel]['id'], 2);
						if(!empty($aSubMenu)){
							$aMenu1[0]['children']=$aSubMenu;
							$aMenu1[0]['returnlink']=$aMenu1[0]['link'];
						}
					}
					elseif($aConfig['_tmp']['path'][1]['menu_id'] == $this->aSettings['menu'][1]['menu1_id']){
						$aSubMenu=$this->getMenuTree($this->aSettings['menu'][1]['menu1_id'], $aConfig['_tmp']['path'][$iLevel]['id'], 2);
						if(!empty($aSubMenu)){
							$aMenu2[0]['children']=$aSubMenu;
							$aMenu2[0]['returnlink']=$aMenu2[0]['link'];
						}
					}
					elseif($aConfig['_tmp']['path'][1]['menu_id'] == $this->aSettings['menu'][2]['menu1_id']){
						$aSubMenu=$this->getMenuTree($this->aSettings['menu'][2]['menu1_id'], $aConfig['_tmp']['path'][$iLevel]['id'], 2);
						if(!empty($aSubMenu)){
							$aMenu3[0]['children']=$aSubMenu;
							$aMenu3[0]['returnlink']=$aMenu3[0]['link'];
						}
					}
				}
			}
			
			$aBox['menu'] = array_merge($aMenu1,$aMenu2,$aMenu3);
			if (!empty($aBox['menu'])) {
				// nazwa boksu
	    	$aBox['name'] =& $this->sName;
	    	// dane 'rodzica' podmenu
	    	if (isset($aConfig['_tmp']['path'])) {
	    		if (count($aConfig['_tmp']['path']) >= 3) {
	    			if ($aCfg['expand_from_level'] > 2) {
	    				$aBox['parent'] =& $aConfig['_tmp']['path'][$aCfg['expand_from_level']-3];
	    			}
	    			else {
	    				$aBox['parent'] =& $aConfig['_tmp']['path'][count($aConfig['_tmp']['path'])-3];
	    			}
					}
					elseif (count($aConfig['_tmp']['path']) >= 2) {
	    			if ($aCfg['expand_from_level'] > 1) {
	    				$aBox['parent'] =& $aConfig['_tmp']['path'][$aCfg['expand_from_level']-2];
	    			}
	    			else {
	    				$aBox['parent'] =& $aConfig['_tmp']['path'][count($aConfig['_tmp']['path'])-2];
	    			}
	    		}
	    		else {
	    			$aBox['parent'] =& $aConfig['_tmp']['path'][count($aConfig['_tmp']['path'])-1];
	    		}
	    	}
	    	// okreslenie nazwy subtemplate'a dla menu
	    	$aBox['sub_template'] = $this->sTemplatesPath.'/_'.$this->sTemplate;
	    	$pSmarty->assign_by_ref('aBox', $aBox);
	    	$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
	    	$pSmarty->clear_assign('aBox');
	    }
			
		}

		return $sHtml;
	} // end of getContent()


	/**
	 * Metoda zwraca drzewo menu rozpoczynajaca sie od korzenia $iRId
	 * wraz z jej podstronami
	 *
	 * @param	integer	$iRId	- korzen galezi - 0 - od pierwszego poziomu
	 * @param	integer	$iExpLevels	- liczba poziomow do ktorych ma byc rozwieniete menu
	 * @param	integer	$iLevel	- aktualny poziom zaglebienia
	 * @return	array ref	- drzewo menu
	 */
	function &getMenuTree($iMenuId, $iRId, $iExpLevels=0, $iLevel=1) {
		global $aConfig;
		
		// pobranie podstron
		$aItems =& $this->getMenuLeaf($iMenuId, $iRId);

		foreach ($aItems as $iKey => $aItem) {
			//$aItems[$iKey]['name'] = htmlentities($aItem['name'], ENT_QUOTES, 'UTF-8');
			$aItems[$iKey]['link'] = getPageLink($aItem);
			$aItems[$iKey]['selected'] = in_array($aItem['id'], $aConfig['_tmp']['selected']);
			if ($aItems[$iKey]['selected'] || $iLevel <= $iExpLevels) {
				// pobranie podstron
				$aItems[$iKey]['children'] =& $this->getMenuTree($iMenuId, 
																												 $aItem['id'],
																												 $iExpLevels,
																												 $iLevel+1);
				if (empty($aItems[$iKey]['children'])) {
					unset($aItems[$iKey]['children']);
				}
			}
		}
		return $aItems;
	} // end of getMenuTree() method


	/**
	 * Metoda pobiera podstrony jednej galezi menu
	 *
	 * @param	integer	$iRId	- korzen galezi - 0 - od pierwszego poziomu
	 * @return	array ref	- galaz menu
	 */
	function &getMenuLeaf($iMenuId, $iRId) {
		global $aConfig;

		$sSql = "SELECT id, symbol, name, url, link_to_id, mtype, new_window
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$_GET['lang_id']." AND
						 			 menu_id = ".$iMenuId." AND
						 			 published = '1' AND
						 			 menu = '1' AND
						 			 parent_id ".($iRId == 0 ? 'IS NULL' : '= '.$iRId)."
						 ORDER BY order_by";
		return Common::GetAll($sSql);
	} // end of getMenuLeaf() method
	
	function &getWebsiteMapSymbols(){
		global $aConfig;
		$sSql="SELECT A.menu_id , B.symbol
						FROM ".$aConfig['tabls']['prefix']."sitemap_settings A
						JOIN ".$aConfig['tabls']['prefix']."menus_items B 
						ON A.page_id = B.id";
		return Common::GetAssoc($sSql);
	}

} // end of Box_m_menu Class
?>