<?php
/**
 * Klasa Box_m_menu do obslugi boksu menu
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Box_m_menu {

  // Id boksu
  var $iId;

  // Id menu
  var $iMenuId;

  // nazwa boksu
  var $sName;

  // sciezka do katalogu z szablonami
  var $sTemplatesPath;

  // szablon boksu
  var $sTemplate;

  // konfiguracja boksu
  var $aSettings;

  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_menu(&$aBox) {
		$this->iId =& $aBox['id'];
		$this->iMenuId = $aBox['menu_id'];
		$this->sName = $aBox['name'];
		$this->sTemplatesPath = 'boxes/'.$aBox['module'];
		$this->sTemplate = $aBox['template'];

		// pobranie konfiguracji boksu
		$this->aSettings =& $this->getSettings();
	} // end Box_m_menu() function


	/**
	 * Metoda pobiera konfiguracje dla boksu
	 *
	 * @return	array
	 */
	function &getSettings() {
		global $aConfig;

		$sSql = "SELECT expand_levels, expand_from_level, expand_first_level
						 FROM ".$aConfig['tabls']['prefix']."menus_box_settings
						 WHERE box_id = ".$this->iId;
		$aCfg = Common::GetRow($sSql);
		$aCfg['expand_levels'] = (int) $aCfg['expand_levels'];
		$aCfg['expand_from_level'] = (int) $aCfg['expand_from_level'];
		
		return $aCfg;
	} // end of getSettings() function


	/**
	 * Metoda pobiera z pliku XML strukture menu o Id $this->iMenuId,
	 * dowiazuje ja do Smartow oraz zwraca xHTML z elementami menu po renderowaniu
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $aConfig, $pSmarty;
		$sHtml = '';
		$aBox = array();
		$aCfg =& $this->aSettings;

		// menu jest renderowane wtedy gdy poziom od ktorego jest rozpoczynane
		// renderowanie jest rowny 1 lub gdy jest wiekszy od 1 i jest wybrana podstrona
		// podstrona nalezy do renderowanego menu i poziom zaglebienia przegladanej strony
		// jest przynajmniej o 1 mniejszy od poziomu od ktorego bedzie rozpoczynane
		// renderowanie menu
		if ($aCfg['expand_from_level'] == 1 ||
				($aCfg['expand_from_level'] > 1 && isset($aConfig['_tmp']['page']) &&
				 $aConfig['_tmp']['page']['menu_id'] == $this->iMenuId &&
				 count($aConfig['_tmp']['path']) >= ($aCfg['expand_from_level'] - 1))) {
			
			if (isset($_COOKIE['it-sel-menus']) && !empty($_COOKIE['it-sel-menus'])) {
				// istnieja jakies rozwiniete przez uzytkownika podstrony
				$aConfig['_tmp']['selected'] = array_merge($aConfig['_tmp']['selected'], explode('_', $_COOKIE['it-sel-menus']));
			}
			// Id strony - 'korzenia' dla ktorej renderowane bedzie menu
			$iRId = $aCfg['expand_from_level'] > 1 ? $aConfig['_tmp']['path'][$aCfg['expand_from_level']-2]['id'] : 0;
			// pobranie menu
			$aBox['menu'] =& $this->getMenuTree($iRId, $aCfg['expand_levels'] > $aCfg['expand_from_level'] ? $aCfg['expand_levels'] - $aCfg['expand_from_level'] : 0);
			//dump($aBox['menu']);
			if (!empty($aBox['menu'])) {
				// nazwa boksu
	    	$aBox['name'] =& $this->sName;
	    	// dane 'rodzica' podmenu
	    	if (isset($aConfig['_tmp']['path'])) {
	    		if (count($aConfig['_tmp']['path']) >= 2) {
	    			if ($aCfg['expand_from_level'] > 1) {
	    				$aBox['parent'] =& $aConfig['_tmp']['path'][$aCfg['expand_from_level']-2];
	    			}
	    			else {
	    				$aBox['parent'] =& $aConfig['_tmp']['path'][count($aConfig['_tmp']['path'])-2];
	    			}
	    		}
	    		else {
	    			$aBox['parent'] =& $aConfig['_tmp']['path'][count($aConfig['_tmp']['path'])-1];
	    		}
	    	}
	    	// okreslenie nazwy subtemplate'a dla menu
	    	$aBox['sub_template'] = $this->sTemplatesPath.'/_'.$this->sTemplate;
	    	//dump($aBox);
	    	$pSmarty->assign_by_ref('aBox', $aBox);
	    	$sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
	    	$pSmarty->clear_assign('aBox');
	    }
		}
		return $sHtml;
	} // end of getContent()


	/**
	 * Metoda zwraca drzewo menu rozpoczynajaca sie od korzenia $iRId
	 * wraz z jej podstronami
	 *
	 * @param	integer	$iRId	- korzen galezi - 0 - od pierwszego poziomu
	 * @param	integer	$iExpLevels	- liczba poziomow do ktorych ma byc rozwieniete menu
	 * @param	integer	$iLevel	- aktualny poziom zaglebienia
	 * @return	array ref	- drzewo menu
	 */
	function &getMenuTree($iRId, $iExpLevels=0, $iLevel=1) {
		global $aConfig;
		
		// pobranie podstron
		$aItems =& $this->getMenuLeaf($iRId);
		if ($this->aSettings['expand_first_level'] == '1' && $iLevel == 1) {
			// oznaczenie pierwszego poziomu jako domyslnie rozwinietego
			if (isset($_COOKIE['it-desel-menus']) && !empty($_COOKIE['it-desel-menus'])) {
				// istnieje ciasteczko ze zwinietymi przez uzytkownika menu
				$aDeselMenus = explode("_", $_COOKIE['it-desel-menus']);
				if (!in_array($aItems[0]['id'], $aDeselMenus)) {
					// boks ma ustawione rozwijanie pierwszego menu - a uzytkownik go nie zwinal
					// wyswietlenie rozwinietego
					$aConfig['_tmp']['selected'][] = $aItems[0]['id'];
				}
			}
			else {
				$aConfig['_tmp']['selected'][] = $aItems[0]['id'];
			}
		}
		foreach ($aItems as $iKey => $aItem) {
			$aItems[$iKey]['name'] = htmlentities($aItem['name'], ENT_QUOTES, 'UTF-8');
			$aItems[$iKey]['link'] = getPageLink($aItem);
			$aItems[$iKey]['selected'] = in_array($aItem['id'], $aConfig['_tmp']['selected']);
			if ($aItems[$iKey]['selected'] || $iLevel <= $iExpLevels) {
				// pobranie podstron
				$aItems[$iKey]['children'] =& $this->getMenuTree($aItem['id'],
																												 $iExpLevels,
																												 $iLevel+1);
				if (empty($aItems[$iKey]['children'])) {
					unset($aItems[$iKey]['children']);
				}
			}
		}
		return $aItems;
	} // end of getMenuTree() method


	/**
	 * Metoda pobiera podstrony jednej galezi menu
	 *
	 * @param	integer	$iRId	- korzen galezi - 0 - od pierwszego poziomu
	 * @return	array ref	- galaz menu
	 */
	function &getMenuLeaf($iRId) {
		global $aConfig;

		$sSql = "SELECT id, symbol, name, url, link_to_id, mtype, new_window
						 FROM ".$aConfig['tabls']['prefix']."menus_items
						 WHERE language_id = ".$_GET['lang_id']." AND
						 			 menu_id = ".$this->iMenuId." AND
						 			 published = '1' AND
						 			 menu = '1' AND
						 			 parent_id ".($iRId == 0 ? 'IS NULL' : '= '.$iRId)."
						 ORDER BY order_by";
		return Common::GetAll($sSql);
	} // end of getMenuLeaf() method
} // end of Box_m_menu Class
?>