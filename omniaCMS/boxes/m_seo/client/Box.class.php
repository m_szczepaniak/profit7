<?php

/**
 * Klasa Box do obslugi boksu Konta uzytkownikow
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
class Box_m_seo
{

    // Id boksu
    var $iId;

    // Id strony boksu
    var $iPageId;

    // nazwa boksu
    var $sName;

    // sciezka do katalogu z szablonami
    var $sTemplatesPath;

    // szablon boksu
    var $sTemplate;

    // nazwa modulu boksu
    var $sModule;

    // konfiguracja boksu
    var $aSettings;

    // link do strony
    var $sPageLink;

    public $pDbMgr;

    /**
     * Konstruktor klasy
     *
     * @param    array $aBox - dane boksu
     */
    function __construct(&$aBox)
    {
        global $pDbMgr;
        $this->pDbMgr = $pDbMgr;

        $this->sTemplatesPath = 'boxes/' . $aBox['module'];
        $this->sTemplate = $aBox['template'];

    } // end Box() function


    /**
     * Metoda okresla sciezke do strony Konta uzytkownikow,
     * dowiazuje ja do Smartow oraz zwraca xHTML z boksem newslettera
     * szablonu $this->sTemplate
     */
    function getContent()
    {
        global $pSmarty, $aConfig, $pDB2;
        $sHtml = '';


        $dedicatedDescription = $this->getDedicatedDescription();
        if (null === $dedicatedDescription || false === $dedicatedDescription) {
            return;
        }

        $filteredDescription = trimString(trim(strip_tags($dedicatedDescription['description'])), 160);
        $aConfig['_tmp']['page']['description'] = $filteredDescription;
        $aConfig['_tmp']['description'] = $filteredDescription;
        $aBox['data'] = $dedicatedDescription;

        $pSmarty->assign_by_ref('aBox', $aBox);
        $sHtml = $pSmarty->fetch($this->sTemplatesPath . '/' . $this->sTemplate);
        $pSmarty->clear_assign('aBox');

        return $sHtml;
    } // end of getContent()


    /**
     * @return bool
     */
    private function getDedicatedDescription()
    {
        global $aConfig;

        $currentModule = $aConfig['_tmp']['page']['module'];
        $currentWebsite = $aConfig['website_id'];

        if (isset($_GET['p']) && intval($_GET['p']) > 1) {
            return false;
        }

        switch ($currentModule) {
            case 'm_oferta_produktowa':
                $sTable = 'menus_items';
                $currentId = $aConfig['_tmp']['page']['id'];
                if ($_GET['id'] > 0) {
                    return false;
                }
                break;
            case 'm_szukaj':
                if ('autor' == $_GET['path']) {
                    $sTable = 'products_authors';
                    $autor = $_GET['autor'];
                    $sSql = 'SELECT id FROM products_authors WHERE CONCAT(surname, " ", name) = "' . $autor . '"';
                    $currentId = Common::GetOne($sSql);

                } elseif ('wydawnictwo' == $_GET['path']) {

                    if ($_GET['seria'] != '') {
                        return false;
                    }
                    $sTable = 'products_publishers';
                    $publisher = $_GET['publisher'];
                    $sSql = 'SELECT id FROM products_publishers WHERE name = "' . $publisher . '"';
                    $currentId = Common::GetOne($sSql);
                } else {
                    return false;
                }
                break;
            default:
                return false;
        }
        if ($currentId <= 0) {
            return false;
        }

        $sSql = 'SELECT * 
                 FROM description_dedicated 
                 WHERE 
                    dest_id = ' . $currentId . ' 
                    AND table_destination = "' . $sTable . '"
                    AND (website_id = "' . $currentWebsite . '" OR website_id IS NULL)';
        $data = $this->pDbMgr->GetRow('profit24', $sSql);
        if (empty($data)) {
            if ($sTable == 'menus_items' && '' != $aConfig['_tmp']['page']['description']) {
                return ['description' => $aConfig['_tmp']['page']['description']];
            }
        }
        return $data;
    }
} // end of Box Class
?>