<?php

/**
 * Klasa Box do obslugi boksu Konta uzytkownikow
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @version   1.0
 */
class Box_m_seo_modern
{

    // Id boksu
    var $iId;

    // Id strony boksu
    var $iPageId;

    // nazwa boksu
    var $sName;

    // sciezka do katalogu z szablonami
    var $sTemplatesPath;

    // szablon boksu
    var $sTemplate;

    // nazwa modulu boksu
    var $sModule;

    // konfiguracja boksu
    var $aSettings;

    // link do strony
    var $sPageLink;

    public $pDbMgr;

    /**
     * Konstruktor klasy
     *
     * @param    array $aBox - dane boksu
     */
    function __construct(&$aBox)
    {
        global $pDbMgr;
        $this->pDbMgr = $pDbMgr;

        $this->sTemplatesPath = 'boxes/' . $aBox['module'].'/' . $aBox['moption'];
        $this->sTemplate = $aBox['template'];
        $this->iId = $aBox['id'];
    } // end Box() function


    /**
     * Metoda okresla sciezke do strony Konta uzytkownikow,
     * dowiazuje ja do Smartow oraz zwraca xHTML z boksem newslettera
     * szablonu $this->sTemplate
     */
    function getContent()
    {
        global $pSmarty, $aConfig, $pDB2;
        $sHtml = '';

        $dedicatedDescription = $this->getDedicatedDescription();
        if (null === $dedicatedDescription || false === $dedicatedDescription) {
            return;
        }
        $aBox['data'] = $dedicatedDescription;

        $pSmarty->assign_by_ref('aBox', $aBox);
        $sHtml = $pSmarty->fetch($this->sTemplatesPath . '/' . $this->sTemplate);
        $pSmarty->clear_assign('aBox');

        return $sHtml;
    } // end of getContent()


    /**
     * @return bool
     */
    private function getDedicatedDescription()
    {
        global $aConfig, $pDB;

        $requestURI = $_SERVER['REQUEST_URI'];
        if (false !== strstr($requestURI, '?')) {
            $requestURI = preg_replace('(\?.*)', '', $requestURI);
        }
        // pobierz mi boks pod główny obszar

        if ($this->iId > 0) {


            $sSql = 'SELECT * 
                 FROM seo
                 WHERE box_id = ' . $this->iId . ' 
                 ';
            if (mb_substr($requestURI, -1, null, 'UTF-8') !== '/') {
                $requestURISlash = $requestURI . '/';
                $sSql .= ' AND (symbol = ' . $pDB->quoteSmart($requestURI) . ' OR symbol = ' . $pDB->quoteSmart($requestURISlash) . ') ';
            } else {
                $requestURISlash = mb_substr($requestURI, 0, -1, 'UTF-8');
                $sSql .= ' AND (symbol = ' . $pDB->quoteSmart($requestURI) . ' OR symbol = ' . $pDB->quoteSmart($requestURISlash) . ') ';
            }


            return $this->pDbMgr->GetRow($aConfig['common']['bookstore_code'], $sSql);
        }
        return null;
    }
} // end of Box Class
?>