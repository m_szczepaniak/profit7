<?php
/**
 * Klasa Module do obslugi konfiguracji boksu stron opisowych
 *
 * @author    Marcin Korecki <korecki@pnet.pl>
 * @copyright 2006
 * @version   1.0
 */

class Settings {

  // nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// Id kategorii dla ktorej edytowane sa ustawienia
	var $iId;

	// Id modulu aktualnosci
	var $iModuleId;

	// konfiguracja boksu
	var $aSettings;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$iModuleId, &$sModule, &$iLangId, &$iId) {
		global $aConfig;
		$this->iModuleId = $iModuleId;
		$this->sModule = $sModule;
		$this->iLangId = $iLangId;
		$this->iId = $iId;
		$this->aSettings =& $this->getSettings();
	} // end Settings() function


	/**
	 * Metoda pobiera konfiguracje dla boksu
	 *
	 * @return	void
	 */
	function &getSettings() {
        global $aConfig;

        // sprawdzenie czy ustawienia sa juz w bazie
        $sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."menus_box_settings
						 WHERE box_id = ".$this->iId;
        $aSettings =& Common::GetRow($sSql);

        $aValues = array(
            'expand_levels' => $_POST['expand_levels'],
            'expand_from_level' => $_POST['expand_from_level'],
            'expand_first_level' => $_POST['expand_first_level']
        );
        if (!empty($aSettings)) {
            // Update
            return Common::Update($aConfig['tabls']['prefix']."menus_box_settings",
                    $aValues,
                    "box_id = ".$this->iId) !== false;
        }
        else {
            // Insert
            $aValues = array_merge(array('box_id' => $this->iId), $aValues);

            return Common::Insert($aConfig['tabls']['prefix']."menus_box_settings",
                    $aValues) !== false;
        }
	} // end of getSettings() method


    /**
     * Metoda wprowadza zmiany w ustawieniach boksu
     *
     * @param		object	$pSmarty
     * @return	void
     */
    function UpdateSettings() {
            global $aConfig;

        // sprawdzenie czy ustawienia sa juz w bazie
        $sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."seo_box_settings
						 WHERE box_id = ".$this->iId;
        $aSettings =& Common::GetRow($sSql);

        $aValues = array(
            'main_box_area' => $_POST['main_box_area']
        );

        if (!empty($aSettings)) {
            // Update
            return Common::Update($aConfig['tabls']['prefix']."seo_box_settings",
                    $aValues,
                    "box_id = ".$this->iId) !== false;
        }
        else {
            // Insert
            $aValues = array_merge(array('box_id' => $this->iId), $aValues);

            return Common::Insert($aConfig['tabls']['prefix']."seo_box_settings",
                    $aValues) !== false;
        }
    } // end of UpdateSettings() funciton


	/**
	 * Metoda tworzy formularz edycji konfiguracji boksu
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		$aData =& $this->aSettings;
		if (!empty($_POST) && $_POST['do'] != 'change_type') {
			$aData =& $_POST;
		}

		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['settings'], array('class'=>'merged', 'style'=>'padding-left: 218px'));

        $pForm->AddCheckBox('main_box_area', "Główny obszar modułu", array(), '', !empty($aData['main_box_area']), false);
	} // end of SettingsForm() function
} // end of Settings Class
?>