<?php
/**
 * Klasa Module do obslugi konfiguracji boksu 'Newslettera'
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */

class Settings {

  // nazwa modulu - do langow
	var $sModule;

	// ID wersji jezykowej
	var $iLangId;

	// Id strony dla ktorej edytowane sa ustawienia
	var $iId;

	// Id modulu aktualnosci
	var $iModuleId;

  /**
   * Konstruktor klasy
   *
	 * @return	void
   */
	function Settings(&$iModuleId, &$sModule, &$iLangId, &$iId) {
		global $aConfig;
		$this->iModuleId = $iModuleId;
		$this->sModule = $sModule;
		$this->iLangId = $iLangId;
		$this->iId = $iId;
		$this->aSettings =& $this->getSettings();
	} // end Settings() function
	

	/**
	 * Metoda pobiera konfiguracje dla boksu
	 *
	 * @return	void
	 */
	function &getSettings() {
		global $aConfig;

		// pobranie konfiguracji dla boksu
		$sSql = "SELECT A.*, B.page_id
						 FROM ".$aConfig['tabls']['prefix']."newsletters_box_settings AS A
						 JOIN ".$aConfig['tabls']['prefix']."boxes AS B
						 ON B.id = A.box_id
						 WHERE A.box_id = ".$this->iId;
		$aCfg =& Common::GetRow($sSql);
		if ($this->iId == 0 && empty($_POST)) {
			$aCfg['show_categories'] = '1';
		}
		if (!empty($aCfg['info'])) {
			$aCfg['info'] = br2nl($aCfg['info']);
		}
		return $aCfg;
	} // end of getSettings() method


	/**
	 * Metoda wprowadza zmiany w ustawieniach boksu
	 *
	 * @param		object	$pSmarty
	 * @return	void
	 */
	function UpdateSettings() {
		global $aConfig;

		// sprawdzenie czy ustawienia sa juz w bazie
		$sSql = "SELECT *
						 FROM ".$aConfig['tabls']['prefix']."newsletters_box_settings
						 WHERE box_id = ".$this->iId;
		$aSettings =& Common::GetRow($sSql);
		
		$aValues = array(
			'info' => trim($_POST['info']) != '' ? nl2br($_POST['info']) : 'NULL',
			'show_categories' => isset($_POST['show_categories']) ? '1' : '0'
		);
		if (!empty($aSettings)) {
			// Update
			return Common::Update($aConfig['tabls']['prefix']."newsletters_box_settings",
														$aValues,
														"box_id = ".$this->iId) !== false;
		}
		else {
			// Insert
			$aValues = array_merge(array('box_id' => $this->iId), $aValues);

			return Common::Insert($aConfig['tabls']['prefix']."newsletters_box_settings",
														$aValues,
														"",
														false) !== false;
		}
	} // end of UpdateSettings() funciton


	/**
	 * Metoda tworzy formularz edycji konfiguracji boksu
	 *
	 * @param	object	$pForm	- obiekt klasy formularza
	 * @return	void
	 */
	function SettingsForm(&$pForm) {
		global $aConfig;

		$aData = array();
		$sHtml = '';

		$aData =& $this->aSettings;
		if (!empty($_POST) && $_POST['do'] != 'change_type') {
			$aData =& $_POST;
		}

		// naglowek konfiguracji ustawien
		$pForm->AddMergedRow($aConfig['lang'][$_GET['file']]['settings'], array('class'=>'merged', 'style'=>'padding-left: 175px'));

		// strony newslettera
		$aPages =& getModulePages($this->iModuleId, $this->iLangId);
		$aPages = array_merge(
			array(array('value'=>'0', 'label'=>$aConfig['lang']['common']['choose'])),
			$aPages
		);
		$pForm->AddSelect('page_id', $aConfig['lang'][$this->sModule.'_box_settings']['page'], array(), $aPages, $aData['page_id']);

		// wyswietlaj liste kategorii w boksie
		$pForm->AddCheckBox('show_categories', $aConfig['lang'][$this->sModule.'_box_settings']['show_categories'], array(), '', !empty($aData['show_categories']), false);

		// informacja dla newslettera
		$pForm->AddTextArea('info', $aConfig['lang'][$this->sModule.'_box_settings']['info'], $aData['info'], array('rows'=>'8', 'style'=>'width: 350px;'), 255, '', $aConfig['lang'][$this->sModule.'_box_settings']['too_long_info'], false);
	} // end of SettingsForm() function
} // end of Settings Class
?>