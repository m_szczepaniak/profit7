<?php
/**
 * Klasa Box do obslugi boksu newslettera
 *
 * @author    Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2006
 * @version   1.0
 */
 
class Box_m_newsletter {
  
  // Id boksu
  var $iId;
  
  // Id strony boksu
  var $iPageId;
  
  // nazwa boksu
  var $sName;
  
	// sciezka do katalogu z szablonami
  var $sTemplatesPath;
  
  // szablon boksu
  var $sTemplate;
  
  // nazwa modulu boksu
  var $sModule;
  
  // konfiguracja boksu
  var $aSettings;
  
  // link dos trony newslettera
  var $sPageLink;
  
  /**
   * Konstruktor klasy
   *
   * @param	array	$aBox	- dane boksu
	 * @return	void
   */
	function Box_m_newsletter(&$aBox) {
		$this->iId =& $aBox['id'];
		$this->iPageId = $aBox['page_id'];
		$this->sName = $aBox['name'];
		$this->sModule = $aBox['module'];
		$this->sTemplatesPath = 'boxes/'.$aBox['module'];
		$this->sTemplate = $aBox['template'];
		$this->sPageLink = '/'.$aBox['page_symbol'];
		
		// pobranie konfiguracji boksu
		$this->aSettings =& $this->getSettings();
	} // end Box() function
	
	
	/**
	 * Metoda pobiera konfiguracje dla boksu
	 * 
	 * @return	array
	 */
	function getSettings() {
		global $aConfig;
		
		$sSql = "SELECT A.*
						 FROM ".$aConfig['tabls']['prefix']."newsletters_box_settings A
						 JOIN ".$aConfig['tabls']['prefix']."menus_items B
						 ON B.id = ".$this->iPageId." 
						 WHERE A.box_id = ".$this->iId.
						 			 (!isPreviewMode() ? " AND B.published = '1'" : '');
		$aCfg = Common::GetRow($sSql);
		return $aCfg;
	} // end of getSettings() function
	
	
	/**
	 * Metoda okresla sciezke do strony newslettera,
	 * dowiazuje ja do Smartow oraz zwraca xHTML z boksem newslettera
	 * szablonu $this->sTemplate
	 */
	function getContent() {
		global $pSmarty, $aConfig;
		$sHtml = '';

		if (empty($this->aSettings)) {
			// nie ma ustawie strony - boks nie moze zostac wyswietlony
			return $sHtml;
		}
		
		// pobranie kategorii newslettera
		// dolaczenie klasy Newsletter
 		include_once('modules/m_newsletter/client/Newsletter.class.php');
 		$oNewsletter = new Newsletter('m_newsletter', $this->iPageId, array(), '');
 		$aBox['categories'] =& $oNewsletter->getCategoriesList();
		
		if (!empty($aBox['categories'])) {			
			// dodanie kodu JS dla boksu
			// tekst domyslnie wyswietlany w okienku newslettera
			$aBox['js'][] = 'var sNewsletterTxt = "'.
											 $aConfig['lang']['box_'.$this->sModule]['email'].'";';
			$aBox['page_link'] =& $this->sPageLink;
			
			$aBox['settings'] =& $this->aSettings;
			$aBox['name'] =& $this->sName;
		
		$pSmarty->assign_by_ref('aBox', $aBox);
	    $sHtml = $pSmarty->fetch($this->sTemplatesPath.'/'.$this->sTemplate);
	    $pSmarty->clear_assign('aBox');
		}
		return $sHtml;
	} // end of getContent()
} // end of Box Class
?>