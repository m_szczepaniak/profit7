<?php

/**
 * Plik jezykowy dla interfejsu panelu administracyjnego
 * jezyk polski
 *
 * @author Marcin Korecki <m.korecki@omnia.pl>
 * @copyright 2005 - 2006
 * @version 1.0
 *
 */
$aConfig['lang']['common']['choose'] = '-- wybierz --';
$aConfig['lang']['common']['website'] = 'Serwis';
$aConfig['lang']['common']['language'] = 'Wersja językowa';
$aConfig['lang']['common']['module'] = 'Moduł';
$aConfig['lang']['common']['action'] = 'Akcja';
$aConfig['lang']['common']['search'] = 'Szukaj';
$aConfig['lang']['common']['refresh'] = 'Odśwież';
$aConfig['lang']['common']['no_privileges'] = 'Brak wystarczających uprawnień!';
$aConfig['lang']['common']['no_privileges2'] = 'Brak uprawnień!';
$aConfig['lang']['common']['yes'] = 'Tak';
$aConfig['lang']['common']['no'] = 'Nie';
$aConfig['lang']['common']['login'] = 'Login';
$aConfig['lang']['common']['passwd'] = 'Hasło';
$aConfig['lang']['common']['passwd2'] = 'Powtórz hasło';
$aConfig['lang']['common']['passwd2_err'] = 'Podane hasła są różne';
$aConfig['lang']['common']['email'] = 'Adres email';
$aConfig['lang']['common']['no_access'] = 'Brak dostępu';
$aConfig['lang']['common']['created'] = "Utworzono";
$aConfig['lang']['common']['created_by'] = "Utworzył";
$aConfig['lang']['common']['modified'] = "Zmodyfikowano przez";
$aConfig['lang']['common']['modified_by'] = "Zmodyfikował";
$aConfig['lang']['common']['currency'] = "zł";
$aConfig['lang']['common']['currency_fraction'] = "gr.";
$aConfig['lang']['common']['show_image'] = "pokaż zdjęcie";
$aConfig['lang']['common']['hide_image'] = "ukryj zdjęcie";
$aConfig['lang']['common']['show_file'] = "pokaż plik";
$aConfig['lang']['common']['delete_image'] = "usuń zdjęcie";
$aConfig['lang']['common']['delete_file'] = "usuń plik";
$aConfig['lang']['common']['order_by'] = "kolejność wyświetlania";
$aConfig['lang']['common']['max_img_upload_size'] = 'Maksymalna całkowita wielkość zdjęć';
$aConfig['lang']['common']['max_files_upload_size'] = 'Maksymalna całkowita wielkość plików';
$aConfig['lang']['common']['img_available_exts'] = 'Dopuszczalne formaty zdjęć';
$aConfig['lang']['common']['files_available_exts'] = 'Dopuszczalne formaty plików';

$aConfig['lang']['common']['1'] = 'Tak';
$aConfig['lang']['common']['0'] = 'Nie';

$aConfig['lang']['common']['lang'] = "Wersja językowa";
$aConfig['lang']['common']['lang_pl'] = "polska";
$aConfig['lang']['common']['lang_en'] = "angielska";
$aConfig['lang']['common']['lang_de'] = "niemiecka";

// przyciski
$aConfig['lang']['common']['button_0'] = "Dodaj";
$aConfig['lang']['common']['button_1'] = "Wprowadź zmiany";
$aConfig['lang']['common']['button_add_photo'] = "Zapisz i dodaj nowe pola";
$aConfig['lang']['common']['button_add_file'] = "Zapisz i dodaj nowe pola";
$aConfig['lang']['common']['button_add_option'] = "Dodaj opcję";
$aConfig['lang']['common']['button_add_author'] = "Dodaj pola";
$aConfig['lang']['common']['button_send'] = "Wyślij";
$aConfig['lang']['common']['cancel'] = "Powrót";
$aConfig['lang']['common']['print'] = "Drukuj";
$aConfig['lang']['common']['send_print'] = "Wyślij na drukarkę";
$aConfig['lang']['common']['save_as'] = "Zapisz jako kopię";
$aConfig['lang']['common']['save_version'] = "Zapisz jako nową wersję";
$aConfig['lang']['common']['copy'] = "Kopia: ";
$aConfig['lang']['common']['download'] = "Pobierz";

// zdjecia
$aConfig['lang']['common']['photo_type'] = "Typ miniaturki";
$aConfig['lang']['common']['thumb'] = "Miniaturka";
$aConfig['lang']['common']['small'] = "Małe";

$aConfig['lang']['common']['go_back'] = "Powrót";
$aConfig['lang']['common']['no_items'] = "Brak elementów do wyświetlenia";

$aConfig['lang']['common']['add']	= 'Dodaj';
$aConfig['lang']['common']['edit']	= 'Edytuj';
$aConfig['lang']['common']['delete']	= 'Usuń';

$aConfig['lang']['common']['delete_q']	= 'Czy na pewno chcesz usunąć ?';



$aConfig['lang']['pager']['page'] = 'Strona';
$aConfig['lang']['pager']['pages'] = 'Strony: ';
$aConfig['lang']['pager']['first_page'] = 'Pierwsza strona';
$aConfig['lang']['pager']['previous_page'] = 'Poprzednia strona';
$aConfig['lang']['pager']['next_page'] = 'Następna strona';
$aConfig['lang']['pager']['last_page'] = 'Ostatnia strona';
$aConfig['lang']['pager']['per_page'] = 'Rekordów na stronie:';

$aConfig['lang']['filters']['submit'] = "Filtruj";


$aConfig['lang']['months'][1] = "styczeń";
$aConfig['lang']['months'][2] = "luty";
$aConfig['lang']['months'][3] = "marzec";
$aConfig['lang']['months'][4] = "kwiecień";
$aConfig['lang']['months'][5] = "maj";
$aConfig['lang']['months'][6] = "czerwiec";
$aConfig['lang']['months'][7] = "lipiec";
$aConfig['lang']['months'][8] = "sierpień";
$aConfig['lang']['months'][9] = "wrzesień";
$aConfig['lang']['months'][10] = "październik";
$aConfig['lang']['months'][11] = "listopad";
$aConfig['lang']['months'][12] = "grudzień";

$aConfig['lang']['days'][0] = 'Niedziela';
$aConfig['lang']['days'][1] = 'Poniedziałek';
$aConfig['lang']['days'][2] = 'Wtorek';
$aConfig['lang']['days'][3] = 'Środa';
$aConfig['lang']['days'][4] = 'Czwartek';
$aConfig['lang']['days'][5] = 'Piątek';
$aConfig['lang']['days'][6] = 'Sobota';

$aConfig['lang']['form']['error_prefix'] = 'Podane pola nie zostały wypełnione lub wypełniono je niepoprawnie';
$aConfig['lang']['form']['error_postfix'] = 'Popraw podane pola!';

$aConfig['lang']['image']['open_image_dialog'] = 'Wybierz zdjęcie';
$aConfig['lang']['image']['clear_image'] = 'Wyczyść pole zdjęcia';
$aConfig['lang']['image']['description'] = 'Opis';
$aConfig['lang']['image']['author'] = 'Autor';
$aConfig['lang']['image']['insert_image'] = 'Wybór zdjęcia';
$aConfig['lang']['image']['open_image_window'] = 'Otwórz okno obrazka';
$aConfig['lang']['image']['upload_err'] = 'Wystąpił błąd podczas przesyłania pliku na serwer!\nSprawdź czy rozmiar pliku nie przekracza dopuszczalnego rozmiaru: %s.';
$aConfig['lang']['image']['bad_ext_err'] = 'Obrazek posiada nieprawidłowe rozszerzenie!\nPrawidłowe rozszerzenie(a) to: %s.';
$aConfig['lang']['image']['bad_name_err'] = 'Nazwa pliku zawiera niedozwolone znaki!\nDozwolone znaki to litery bez znaków narodowych, cyfry i znak podkreślenia.';
$aConfig['lang']['image']['dir_not_writable'] = 'Brak wystarczających uprawnień do zapisu w katalogu \'%s\'!';
$aConfig['lang']['image']['not_dir'] = 'Podana ścieżka nie wskazuje na katalog!';
$aConfig['lang']['image']['too_long_name'] = 'Nazwa pliku przekracza dopuszczalną długość 64 znaków!';

$aConfig['lang']['file']['open_file_dialog'] = 'Wybierz plik';
$aConfig['lang']['file']['clear_file'] = 'Wyczyść pole pliku';
$aConfig['lang']['file']['description'] = 'Opis';
$aConfig['lang']['file']['insert_file'] = 'Wybór pliku';
$aConfig['lang']['file']['open_file_window'] = 'Otwórz okno pliku';
$aConfig['lang']['file']['file_exists_err'] = 'Plik o podanej nazwie już istnieje!\nJeżeli chcesz aby został nadpisany zaznacz checkbox \'Nadpisz istniejący\'';
$aConfig['lang']['file']['upload_err'] = 'Wystąpił błąd podczas przesyłania pliku na serwer!\nSprawdź czy rozmiar pliku nie przekracza dopuszczalnego rozmiaru: %s.';
$aConfig['lang']['file']['bad_ext_err'] = 'Plik posiada nieprawidłowe rozszerzenie!\nPrawidłowe rozszerzenie(a) to: %s.';
$aConfig['lang']['file']['bad_name_err'] = 'Nazwa pliku zawiera niedozwolone znaki!\nDozwolone znaki to litery bez znaków narodowych, cyfry i znak podkreślenia.';
$aConfig['lang']['file']['dir_not_writable'] = 'Brak wystarczających uprawnień do zapisu w katalogu \'%s\'!';
$aConfig['lang']['file']['not_dir'] = 'Podana ścieżka nie wskazuje na katalog!';
$aConfig['lang']['file']['too_long_name'] = 'Nazwa pliku przekracza dopuszczalną długość 64 znaków!';
$aConfig['lang']['file']['files_list'] = 'lub plik';

$aConfig['lang']['login']['header'] = 'Logowanie';
$aConfig['lang']['login']['login'] = 'Login:';
$aConfig['lang']['login']['passwd'] = 'Hasło:';
$aConfig['lang']['login']['website'] = 'Serwis:';
$aConfig['lang']['login']['language'] = 'Wersja jęz.:';
$aConfig['lang']['login']['do_login'] = ' Zaloguj ';
$aConfig['lang']['login']['incompatible'] = 'Aby zalogować się do systemu musisz używać przeglądarki Microsoft Internet Explorer 5.5+, Firefox 1.0+, Mozilla 1.3+ lub Netscape 7+!';
$aConfig['lang']['login']['bad_login'] = 'Zły login lub hasło!';
$aConfig['lang']['login']['inactive'] = 'Konto podanego użytkownika jest niekatywne';
$aConfig['lang']['login']['cookie_error'] = 'Wystąpił błąd!<br />Sprawdź czy Twoja przeglądarka akceptuje ciasteczka!';
$aConfig['lang']['login']['no_site_access'] = 'Brak wystarczających uprawnień aby zalogować się do wybranego serwisu!';

$aConfig['lang']['lay_top']['logged'] = 'Zalogowany';
$aConfig['lang']['lay_top']['from_addr'] = 'z adresu';

$aConfig['lang']['lay_header']['module'] = 'Moduł';
$aConfig['lang']['lay_header']['start'] = 'Strona główna';

$aConfig['lang']['menu']['start'] = "Strona główna";
$aConfig['lang']['menu']['clear_cache'] = "Wyczyść cache";
$aConfig['lang']['menu']['my_account'] = "Moje konto";
$aConfig['lang']['menu']['settings'] = "Zarządzanie";
$aConfig['lang']['menu']['modules'] = "Moduły";
$aConfig['lang']['menu']['areas'] = "Obszary";
$aConfig['lang']['menu']['boxes'] = "Boksy";
$aConfig['lang']['menu']['crons'] = "Zadania";
$aConfig['lang']['menu']['printers'] = "Drukarki";
$aConfig['lang']['menu']['languages'] = "Wersje językowe";
$aConfig['lang']['menu']['websites'] = "Serwisy";
$aConfig['lang']['menu']['users'] = "Konta użytkowników";
$aConfig['lang']['menu']['groups_alerts'] = "Dodaj alert";
$aConfig['lang']['menu']['main_settings'] = "Konfiguracja serwisu";
$aConfig['lang']['menu']['messages'] = "Komunikaty";
$aConfig['lang']['menu']['emails'] = "Edycja emaili";
$aConfig['lang']['menu']['logout'] = "Wyloguj";
$aConfig['lang']['menu']['history'] = "Historia";
$aConfig['lang']['menu']['login_history'] = "Logowania";
$aConfig['lang']['menu']['activities_history'] = "Zdarzeń";
$aConfig['lang']['menu']['postals'] = "Kody pocztowe";

//sortowanie
$aConfig['lang']['sort']['sort_info'] = "Kliknij i przeciągnij rekord w dół / górę aby zmienić jego kolejność";
$aConfig['lang']['sort']['sorting_items'] = "Trwa sortowanie rekordów...";
$aConfig['lang']['sort']['sort_ok'] = "Rekordy zostały posortowane";
$aConfig['lang']['sort']['sort_err'] = "Wystąpił błąd podczas sortowania rekordów!";


/* -------------- KALENDARZ -------------- */
$aConfig['lang']['calendar']['Warning'] = 'Niepoprawny format daty!';
$aConfig['lang']['calendar']['AltPrevYear'] = 'poprzedni rok';
$aConfig['lang']['calendar']['AltNextYear'] = 'następny rok';
$aConfig['lang']['calendar']['AltPrevMonth'] = 'poprzedni miesiąc';
$aConfig['lang']['calendar']['AltNextMonth'] = 'następny miesiąc';
$aConfig['lang']['calendar']['weekdays'][1] = 'Nd';
$aConfig['lang']['calendar']['weekdays'][2] = 'Po';
$aConfig['lang']['calendar']['weekdays'][3] = 'Wt';
$aConfig['lang']['calendar']['weekdays'][4] = 'Śr';
$aConfig['lang']['calendar']['weekdays'][5] = 'Czw';
$aConfig['lang']['calendar']['weekdays'][6] = 'Pi';
$aConfig['lang']['calendar']['weekdays'][7] = 'So';
$aConfig['lang']['calendar']['clear_date'] = 'Wyczyść pole daty';


/* NAZWY MODULOW */
$aConfig['lang']['c_modules']['default'] = "Domyślna";
$aConfig['lang']['c_modules']['m_oferta_produktowa'] = "Oferta produktowa";
$aConfig['lang']['c_modules']['m_finanse'] = "Finanse";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa'] = "Powiązane z ofertą produktową";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_polec'] = "Poleć znajomemu";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_nowosci'] = "Nowości";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_bestsellery'] = "Bestsellery";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_promocje'] = "Promocje";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_zapowiedzi'] = "Zapowiedzi";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_pakiety'] = "Pakiety";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_powiazane'] = "Produkty powiązane";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_index'] = "Indeks rzeczowy";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_serie'] = "Indeks serii";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_recent'] = "Ostatnio odwiedzane";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_autorzy'] = "Indeks autorów";
$aConfig['lang']['c_modules']['m_powiazane_oferta_produktowa_wydawcy'] = "Indeks wydawnictw";
$aConfig['lang']['c_modules']['m_aktualnosci'] = "Aktualności";
$aConfig['lang']['c_modules']['m_autorzy'] = "Autorzy";
$aConfig['lang']['c_modules']['m_opisy'] = "Strony opisowe";
$aConfig['lang']['c_modules']['m_menu'] = "Struktura menu";
$aConfig['lang']['c_modules']['m_szukaj'] = "Wyszukiwarka";
$aConfig['lang']['c_modules']['m_newsletter'] = "Newsletter";
$aConfig['lang']['c_modules']['m_mapa_serwisu'] = "Mapa serwisu";
$aConfig['lang']['c_modules']['m_lista_podstron'] = "Lista podstron";
$aConfig['lang']['c_modules']['m_lista_podkategorii'] = "Lista podkategorii";
$aConfig['lang']['c_modules']['m_formularz_kontaktu'] = "Formularz kontaktu";
$aConfig['lang']['c_modules']['m_formularz_zapytania'] = "Formularz zapytania";
$aConfig['lang']['c_modules']['m_bannery'] = "System bannerowy";
$aConfig['lang']['c_modules']['m_konta'] = "Konta użytkowników";
$aConfig['lang']['c_modules']['m_zamowienia'] = "Konfiguracja zamówień";
$aConfig['lang']['c_modules']['m_raporty'] = "Raporty";
$aConfig['lang']['c_modules']['m_zamowienia_listy'] = "Zamówienia listy";
$aConfig['lang']['c_modules']['m_tagi'] = "Tagi";
$aConfig['lang']['c_modules']['m_promocje'] = "Promocje";
$aConfig['lang']['c_modules']['m_promocje_hh'] = "Happy Hours";
$aConfig['lang']['c_modules']['m_promocje_codes'] = "Kody rabatowe";
$aConfig['lang']['c_modules']['m_promocje_orders_discount'] = "Rabat w zamówieniach";
$aConfig['lang']['c_modules']['m_ankiety'] = "Ankiety";
$aConfig['lang']['c_modules']['m_ksiegarnie'] = "Księgarnie";
$aConfig['lang']['c_modules']['m_pracownicy'] = "Pracownicy";
$aConfig['lang']['c_modules']['m_realizacje'] = "Realizacje";
$aConfig['lang']['c_modules']['m_galeria'] = "Galerie";
$aConfig['lang']['c_modules']['m_artykuly'] = "Artykuły";
$aConfig['lang']['c_modules']['m_sondy'] = "Sondy";
$aConfig['lang']['c_modules']['m_rss'] = "RSS";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn'] = "Magazyn";
$aConfig['lang']['c_modules']['m_stats'] = "Statystyki";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_sorter'] = "Sorter";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_zbieranie'] = "Zbieranie A";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_pakowanie'] = "Pakowanie";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_przyjecia'] = "Przyjęcia";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_sort_out'] = "Wydawka";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['invalid_weight'] = "Produkty bez wagi";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_zbieranie_wspolne'] = "Zbieranie A";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_zbieranie_smarkacz'] = "Zbieranie A mix - sm";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_zbieranie_mixed'] = "Zbieranie A mix";
$aConfig['lang']['c_modules']['m_dostepy'] = "Dostępy";
$aConfig['lang']['c_modules']['m_seo'] = "Seo";
$aConfig['lang']['c_modules']['m_multilocalization'] = "Wielolokalizacyjność";

$aConfig['lang']['c_modules']['m_zamowienia_magazyn_zbieranie_tramwaj'] = "Tramwaj";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_zbieranie_zasoby'] = "Stock";
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_zbieranie_wysokie'] = 'Doładowanie do A';
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_zbieranie_niskie'] = 'Doładowanie do B';
$aConfig['lang']['c_modules']['m_zamowienia_magazyn_zbieranie_B'] = 'Zbieranie B';

$aConfig['lang']['c_modules']['m_recenzje'] = 'Recenzje';
$aConfig['lang']['c_modules']['m_burdello'] = 'Burdello';



/* NAZWY OBSZAROW */
$aConfig['lang']['c_areas']['left_column'] = "Lewa kolumna";
$aConfig['lang']['c_areas']['right_column'] = "Strona główna - prawa kolumna";
$aConfig['lang']['c_areas']['main_top'] = "Główny obszar - nagłówek";
$aConfig['lang']['c_areas']['main_top_menu'] = "Główny obszar - nagłówek menu";
$aConfig['lang']['c_areas']['main_content'] = "Główny obszar - podstrony";
$aConfig['lang']['c_areas']['main_page_content'] = "Strona główna - lewa kolumna";
$aConfig['lang']['c_areas']['main_page_content2'] = "Główna obszar 2 - strona główna";
$aConfig['lang']['c_areas']['main_bottom'] = "Główny obszar - stopka";
$aConfig['lang']['c_areas']['header'] = "Nagłówek strony";
$aConfig['lang']['c_areas']['footer'] = "Stopka strony";
$aConfig['lang']['c_areas']['header2'] = "Nagłówek strony - boksy";
$aConfig['lang']['c_areas']['main_newsletter'] = "Główny obszar - newsletter";
$aConfig['lang']['c_areas']['right_newsletter'] = "Prawa kolumna - newsletter";
$aConfig['lang']['c_areas']['header2'] = "Nagłówek strony 2";
$aConfig['lang']['c_areas']['header_login'] = "Nagłówek - logowanie";
$aConfig['lang']['c_areas']['main_news'] = "Strona główna - nowości";


$aConfig['lang']['m_burdello']['diff_stock_location'] = 'Różnice w stanach magazynowych';
$aConfig['lang']['m_burdello']['list_paused_collection'] = 'Wstrzymane zbieranie';
$aConfig['lang']['m_burdello']['list_paused_collection_missing'] = 'Wstrzymane zbieranie - braki na lokalizacjach';
$aConfig['lang']['m_burdello']['invalid_weight'] = "Produkty bez wagi";
$aConfig['lang']['m_burdello']['sell_predict'] = "Do zebrania z B";
$aConfig['lang']['m_burdello']['items_without_location'] = 'Produkty bez lokalizacji - przyjęte ale nie rozłożone !';



/* -------------- MODUL 'AKTUALNOSCI' -------------- */
$aConfig['lang']['m_aktualnosci']['default'] = "Strony aktualności";
$aConfig['lang']['m_aktualnosci']['comments'] = "Komentarze";
$aConfig['lang']['m_aktualnosci']['settings'] = "Konfiguracja strony aktualności";
$aConfig['lang']['m_aktualnosci']['config'] = "Konfiguracja domyślna";

/* -------------- MODUL 'AUTORZY' -------------- */
$aConfig['lang']['m_autorzy']['default'] = "Lista autorów";
$aConfig['lang']['m_autorzy']['roles'] = "Role autorów";

/* -------------- MODUL 'STRONY OPISOWE' -------------- */
$aConfig['lang']['m_opisy']['default'] = "Strony opisowe";
$aConfig['lang']['m_opisy']['settings'] = "Konfiguracja strony opisowej";
$aConfig['lang']['m_opisy']['config'] = "Konfiguracja domyślna";

/* -------------- MODUL 'PRACOWNICY' -------------- */
$aConfig['lang']['m_pracownicy']['default'] = "Lista pracowników";

/* -------------- MODUL 'STRUKTURA MENU' -------------- */
$aConfig['lang']['m_menu']['default'] = "Menu";
$aConfig['lang']['m_menu']['items'] = "Strony menu";
$aConfig['lang']['m_menu']['config'] = "Konfiguracja domyślna";
$aConfig['lang']['m_menu']['cats_priority'] = "Priorytety kategorii";
$aConfig['lang']['m_menu']['ceneo_cats_rebuild'] = "Odbudowa kategorii głównych ceneo";

/* -------------- MODUL 'FORMULARZ KONTAKTU' -------------- */
$aConfig['lang']['m_formularz_kontaktu']['default'] = "Strony formularza";
$aConfig['lang']['m_formularz_kontaktu']['settings'] = "Konfiguracja strony formularza kontaktu";

/* -------------- MODUL 'FORMULARZ ZAPYTANIA' -------------- */
$aConfig['lang']['m_formularz_zapytania']['default'] = "Strony formularza";
$aConfig['lang']['m_formularz_zapytania']['settings'] = "Konfiguracja strony formularza zapytania";


/* -------------- MODUL 'LISTA PODSTRON' -------------- */
$aConfig['lang']['m_lista_podstron']['settings'] = "Konfiguracja strony lista podstron";

/* -------------- MODUL 'MAPA SERWISU' -------------- */
$aConfig['lang']['m_mapa_serwisu']['settings'] = "Konfiguracja strony mapa serwisu";

/* -------------- MODUL 'NEWSLETTER' -------------- */
$aConfig['lang']['m_newsletter']['default'] = "Strony newsletter";
$aConfig['lang']['m_newsletter']['generator'] = "Generator maili newsletter'a";
$aConfig['lang']['m_newsletter']['auto_newsletters'] = "Mailingi nowości";
$aConfig['lang']['m_newsletter']['auto_options'] = "Konfiguracja nowości";
$aConfig['lang']['m_newsletter']['opinions_sent'] = "Prośby o opinie";
$aConfig['lang']['m_newsletter']['settings'] = "Konfiguracja strony newslettera";

/* -------------- MODUL 'SYSTEM BANNEROWY' -------------- */
$aConfig['lang']['m_bannery']['default'] = "Strony bannerów";
$aConfig['lang']['m_bannery']['config'] = "Konfiguracja domyślna";

/* -------------- MODUL 'WYSZUKIWARKA' -------------- */
$aConfig['lang']['m_szukaj']['default'] = "Wyszukiwane frazy";

/* -------------- MODUL 'KONTA UZYTKOWNIKOW' -------------- */
$aConfig['lang']['m_konta']['default'] = "Konta użytkowników";
$aConfig['lang']['m_konta']['discount_logs'] = "Logi rabatów";
$aConfig['lang']['m_konta']['email_logs'] = "Logi rozsyłki";

/* -------------- MODUL 'ZAMOWIENIA' -------------- */
$aConfig['lang']['m_zamowienia']['default'] = "Zamówienia";
$aConfig['lang']['m_zamowienia']['seller_data'] = "Dane sprzedającego";
$aConfig['lang']['m_zamowienia']['transport_means'] = "Środki transportu";
$aConfig['lang']['m_zamowienia']['payment_types'] = "Metody płatności";
$aConfig['lang']['m_zamowienia']['statistics'] = "Statystyki handlowe";

$aConfig['lang']['m_zamowienia']['free_days'] = "Dni wolne";
$aConfig['lang']['m_zamowienia']['shipment_times'] = "Czasy wysyłki";
$aConfig['lang']['m_zamowienia']['statements'] = "Import wyciagów";
$aConfig['lang']['m_zamowienia']['package_weight'] = "Waga opakowania";
$aConfig['lang']['m_zamowienia']['mail_times'] = "Ustawienia powiadomień";
$aConfig['lang']['m_zamowienia']['codes'] = "Kody rabatowe";
$aConfig['lang']['m_zamowienia']['conf_magazine'] = "Konfiguracja magazynu";

/* -------------- MODUL 'RAPORTY' -------------- */
$aConfig['lang']['m_raporty']['move_to_erp'] = "Przesunięcie z G na BAZA";
$aConfig['lang']['m_raporty']['stock_report'] = "Raport mag. PDF";
$aConfig['lang']['m_raporty']['stock_DBF_report'] = "Raport mag. DBF";
$aConfig['lang']['m_raporty']['vat_report'] = "Raport VAT";
$aConfig['lang']['m_raporty']['summary_vat_report'] = "Raport zbiorczy VAT";
$aConfig['lang']['m_raporty']['exports'] = "Eksporty";
$aConfig['lang']['m_raporty']['diff_report'] = "Remanent Tramwaj";
$aConfig['lang']['m_raporty']['diff_dbf'] = "Porównaj DBF";
$aConfig['lang']['m_raporty']['inventory'] = "Remanent Ekonomiczna";
$aConfig['lang']['m_raporty']['inventory_stock'] = "Remanent Stocki";
$aConfig['lang']['m_raporty']['inventory_shelf_not_equal'] = "Remanent półki";

/* -------------- MODUL 'ZAMOWIENIA LISTY' -------------- */
$aConfig['lang']['m_zamowienia_listy']['default'] = "Przegląd";
$aConfig['lang']['m_zamowienia_listy']['list_search'] = "Wyszukaj zamówienia";
$aConfig['lang']['m_zamowienia_listy']['list_new'] = "Nowe";
$aConfig['lang']['m_zamowienia_listy']['list_new_manual'] = "Nowe - ręczne";
$aConfig['lang']['m_zamowienia_listy']['list_prev'] = "Zapowiedzi";
$aConfig['lang']['m_zamowienia_listy']['list_partial'] = "Częściowe";
$aConfig['lang']['m_zamowienia_listy']['list_realize'] = "Do realizacji";
$aConfig['lang']['m_zamowienia_listy']['list_completed'] = "Skompletowane";
$aConfig['lang']['m_zamowienia_listy']['list_shipment'] = "Wysyłka";
$aConfig['lang']['m_zamowienia_listy']['list_personal'] = "Odbiór osobisty";
$aConfig['lang']['m_zamowienia_listy']['list_invoices'] = "Faktury";
//$aConfig['lang']['m_zamowienia_listy']['list_shipments'] = "FedEx";
//$aConfig['lang']['m_zamowienia_listy']['list_pack'] = "Paczkomaty";
//$aConfig['lang']['m_zamowienia_listy']['list_pocztapolska'] = 'Poczta-Polska';
//$aConfig['lang']['m_zamowienia_listy']['list_ruch'] = 'RUCH';
//$aConfig['lang']['m_zamowienia_listy']['list_orlen'] = 'Orlen';
//$aConfig['lang']['m_zamowienia_listy']['list_tba'] = 'TBA';
$aConfig['lang']['m_zamowienia_listy']['list_confirmed'] = "Zatwierdzone";
$aConfig['lang']['m_zamowienia_listy']['list_to_review'] = "Alerty";
$aConfig['lang']['m_zamowienia_listy']['list_duplicates'] = "Duplikaty";
$aConfig['lang']['m_zamowienia_listy']['list_fraud'] = "Fraud";
$aConfig['lang']['m_zamowienia_listy']['list_paused_collection'] = 'Wstrzymane zbieranie';
$aConfig['lang']['m_zamowienia_listy']['list_paused_collection_missing'] = 'Wstrzymane zbieranie - braki na lokalizacjach';
$aConfig['lang']['m_zamowienia_listy']['list_deadline'] = "Przeterminowane";
$aConfig['lang']['m_zamowienia_listy']['list_err_transport'] = "Błędny transport";
$aConfig['lang']['m_zamowienia_listy']['get_deliver'] = "Potwierdzenie nadania";
$aConfig['lang']['m_zamowienia_listy']['invoice_streamsoft_id'] = "Id kontrahentów do FV";
/* -------------- MODUL 'ZAMOWIENIA MAGAZYN' -------------- */
$aConfig['lang']['m_zamowienia_magazyn']['confirm_items'] = "Potwierdź Zamówione";
$aConfig['lang']['m_zamowienia_magazyn']['confirm_hasnc_items'] = "Potwierdź Jest-niepotwierdzone";
$aConfig['lang']['m_zamowienia_magazyn']['sc_confirm_items'] = "Skaner - potwierdź Zamówione";
$aConfig['lang']['m_zamowienia_magazyn']['sc_confirm_hasnc_items'] = "Skaner - potwierdź Jest-niepotwierdzone";
$aConfig['lang']['m_zamowienia_magazyn']['sc_validate_order'] = "Skaner - weryfikacja zamówienia";
$aConfig['lang']['m_zamowienia_magazyn']['ordered_items'] = "Zamówione pozycje";
$aConfig['lang']['m_zamowienia_magazyn']['ordered_items_OLD'] = "STARE zamówione pozycje";
$aConfig['lang']['m_zamowienia_magazyn']['send_history'] = "Wysłane zamówienia";
$aConfig['lang']['m_zamowienia_magazyn']['external_providers'] = "Dostawcy zewnętrzni";
//$aConfig['lang']['m_zamowienia_magazyn']['migration'] = "MIGRUJ na tramwaj";
$aConfig['lang']['m_zamowienia_magazyn']['go_to_train'] = "Przypisz kuwetę na tramwaj";
$aConfig['lang']['m_zamowienia_magazyn']['move_products_to'] = "Przeniesienie towaru do Merlina";
$aConfig['lang']['m_zamowienia_magazyn']['move_products_to_after'] = "CZĘŚĆ II - Przeniesienie towaru do Merlina";
$aConfig['lang']['m_zamowienia_magazyn']['containers'] = "Numeracja";
$aConfig['lang']['m_zamowienia_magazyn']['defect'] = "Defekty";
$aConfig['lang']['m_zamowienia_magazyn']['gratis'] = "Gratis";
$aConfig['lang']['m_zamowienia_magazyn']['packages'] = "Opakowania";
$aConfig['lang']['m_zamowienia_magazyn']['location_generator'] = "Generator lokalizacji";
$aConfig['lang']['m_zamowienia_magazyn']['collecting_high_level'] = "STARE - Braki na zbieraniu";
$aConfig['lang']['m_zamowienia_magazyn']['return'] = "Zwroty magazynowe";
$aConfig['lang']['m_zamowienia_magazyn']['fast_track'] = "Fast track";
$aConfig['lang']['m_zamowienia_magazyn']['localization'] = "Lokalizator";
$aConfig['lang']['m_zamowienia_magazyn']['product_stock_location_history'] = "Historia ruchów magazynowych produktu";

$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['localization'] = "Lokalizator";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['localization'] = "Lokalizator";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['collecting_high_level'] = "STARE - Braki na zbieraniu";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['collecting_high_level'] = "STARE - Braki na zbieraniu";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['collecting_high_level'] = 'STARE - Braki na zbieraniu';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['high_level_stock'] = 'Doładowanie do A - widlak';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['high_level_stock_employee'] = 'Doładowanie do A - ręczne';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['deficiency'] = 'Braki z rozkładania';


$aConfig['lang']['m_zamowienia_magazyn_zbieranie_B']['stock_full_double_employee'] = 'Zbieranie double MIX - B ręczne';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_B']['stock_full_double_forklift'] = 'Zbieranie double MIX - B widlak';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_B']['stock_single_SH_employee'] = "Zbieranie single MIX - B ręczne";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_B']['stock_single_SH_forklift'] = "Zbieranie single MIX - B widlak";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_B']['stock_full_double'] = 'Zbieranie double MIX - B';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_B']['stock_single_SH'] = "Zbieranie single MIX - B";

$aConfig['lang']['m_zamowienia_magazyn_zbieranie_B']['common_continue_list'] = "Kontynuuj zbieranie";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_B']['collecting_high_level'] = "STARE - Braki na zbieraniu";



$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['items_without_location'] = '&bull; Produkty bez lokalizacji - przyjęte ale nie rozłożone !';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['high_level_stock_employee'] = 'Zbieranie wysokie składowanie - ręczne';

$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['stock_single_b6'] = '&bull; Single B6';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['stock_double_b6'] = '&bull; Double B6';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['stock_SH_predictt_employee'] = '&bull; Zbieranie z B na A ręczne';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['stock_SH_predictt_forklift'] = '&bull; Zbieranie z B na A widlak';

$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['move_product_localization'] = 'Przenoszenie produktu';

$aConfig['lang']['m_zamowienia_magazyn_sort_out']['sorting'] = 'Wydawka';
$aConfig['lang']['m_zamowienia_magazyn_sort_out']['get_label'] = 'Pobierz etykietę';
$aConfig['lang']['m_zamowienia_magazyn_sort_out']['location_generator'] = "Generator lokalizacji";
$aConfig['lang']['m_zamowienia_magazyn_sort_out']['default'] = "Przegląd";
$aConfig['lang']['m_zamowienia_magazyn_sort_out']['list_personal'] = "Odbiór osobisty";
$aConfig['lang']['m_zamowienia_magazyn_sort_out']['get_deliver'] = "Potwierdzenie nadania";
$aConfig['lang']['m_zamowienia_magazyn_sort_out']['packed_send'] = "Statystyka spakowane/wysłane";

$aConfig['lang']['m_dostepy']['default'] = "Dostępy";

/* -------------- MODUL 'ZAMOWIENIA SORTER' -------------- */
$aConfig['lang']['m_zamowienia_magazyn_sorter']['sorter'] = "Sorter";
$aConfig['lang']['m_zamowienia_magazyn_pakowanie']['sorter'] = "Sorter dużych";
$aConfig['lang']['m_zamowienia_magazyn_pakowanie']['sorter_single'] = "Pakowanie singli/double";
$aConfig['lang']['m_zamowienia_magazyn_sorter']['back_lack_train'] = 'Cofnij brak na tramwaj';
$aConfig['lang']['m_zamowienia_magazyn_sorter']['list_history'] = 'Historia zbieranych list';


$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj']['train_single'] = "Single";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj']['train'] = "Sorter";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj']['train_big'] = "Sorter Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj']['train_linked'] = "Łączone";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj']['canceled_collector'] = "Anulowane na Stock";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_tramwaj']['list_history'] = 'Historia zbieranych list';

$aConfig['lang']['m_zamowienia_magazyn_zbieranie_zasoby']['stock_full'] = "Sorter";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_zasoby']['stock_full_big'] = "Sorter Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_zasoby']['stock_part_linked'] = "Tramwaj";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_zasoby']['stock_single'] = "Single";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_zasoby']['list_history'] = 'Historia zbieranych list';

$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['common_full'] = "Sorter";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['common_full_big'] = "Sorter Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['common_linked'] = "Łączone";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['common_linked_big'] = "Łączone Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['common_single'] = "Single";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['common_double'] = "Double";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['list_history'] = 'Historia';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['common_continue_list'] = 'Kontynuuj zbieranie';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['stock_full'] = "Sorter S";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['stock_double'] = "Double S";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['stock_full_big'] = "Sorter S Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['stock_single'] = "Single S";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['train_single'] = "Single T";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wspolne']['canceled_collector'] = "Anulowane na Stock";

$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['common_full'] = "Sorter";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['common_full_big'] = "Sorter Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['common_linked'] = "Łączone";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['common_linked_big'] = "Łączone Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['common_single'] = "Single";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['list_history'] = 'Historia';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['common_continue_list'] = 'Kontynuuj zbieranie';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['stock_full'] = "Sorter S";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['stock_full_big'] = "Sorter S Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['stock_single'] = "Single S";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['train_single'] = "Single T";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['canceled_collector'] = "Anulowane na Stock";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['common_double'] = "Double";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_smarkacz']['stock_double'] = "Double S";

$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['common_full'] = "Sorter";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['common_full_big'] = "Sorter Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['common_linked'] = "Łączone";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['common_linked_big'] = "Łączone Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['common_single'] = "Single";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['list_history'] = 'Historia';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['common_continue_list'] = 'Kontynuuj zbieranie';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['stock_full'] = "Sorter S";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['common_double'] = "Double";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['stock_double'] = "Double S";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['stock_full_big'] = "Sorter S Duże";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['stock_single'] = "Single S";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['train_single'] = "Single T";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['canceled_collector'] = "Anulowane na Stock";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_mixed']['collecting_high_level'] = "Braki na zbieraniu";

/* -------------- MODUL 'ZAMOWIENIA PRZYJECIA' -------------- */
$aConfig['lang']['m_zamowienia_magazyn_zbieranie']['get_ready_to_send'] = "Pobierz listę";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie']['get_ready_to_send_single'] = "Pobierz single";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie']['list_history'] = 'Historia zbieranych list';

/* -------------- MODUL 'ZAMOWIENIA PRZYJECIA' -------------- */
$aConfig['lang']['m_zamowienia_magazyn_pakowanie']['packing'] = "Pakowanie";
$aConfig['lang']['m_zamowienia_magazyn_pakowanie']['packing_double'] = "Pakowanie double";
$aConfig['lang']['m_zamowienia_magazyn_pakowanie']['sorting'] = "Sortowanie";
$aConfig['lang']['m_zamowienia_magazyn_sorter']['sorting'] = "Wydawka";

/**
 * 
 */
//$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['default'] = "Przypisz produkt<br /> na Stock";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['put_on_magazine_location'] = "&bull; Przypisz produkt<br /> na lokalizację";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['empty_location'] = "Puste lokalizacje";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['localization'] = "Lokalizator";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['change_shelf_localization'] = "Przenoszenie wszystkich produktów na lokalizację";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['change_product_localization'] = "Przenoszenie wybranego produktu na lokalizację";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['group_change_localization'] = "Grupowa zmiana lokalizacji produktów";
$aConfig['lang']['m_zamowienia_magazyn_przyjecia']['products_stock_locations_log'] = "Produkty przyjęte przez użytkowników";

/* -------------- MODUL 'OFERTA PRODUKTOWA' -------------- */
$aConfig['lang']['m_oferta_produktowa']['default'] = "Produkty oferty";
$aConfig['lang']['m_oferta_produktowa']['series'] = "Serie wydawnicze";
$aConfig['lang']['m_oferta_produktowa']['index'] = "Indeks rzeczowy";
$aConfig['lang']['m_oferta_produktowa']['top15'] = "TOP 15";
$aConfig['lang']['m_oferta_produktowa']['news'] = "Nowości";
$aConfig['lang']['m_oferta_produktowa']['promotions'] = "Promocje";
$aConfig['lang']['m_oferta_produktowa']['free_delivery'] = "Darmowa dostawa";
$aConfig['lang']['m_oferta_produktowa']['bestsellers'] = "Bestsellery";
$aConfig['lang']['m_oferta_produktowa']['previews'] = "Zapowiedzi";
$aConfig['lang']['m_oferta_produktowa']['statuses'] = "Opisy statusów";
$aConfig['lang']['m_oferta_produktowa']['config'] = "Konfiguracja domyślna";
$aConfig['lang']['m_oferta_produktowa']['categories'] = "Konfiguracja kategorii";
$aConfig['lang']['m_oferta_produktowa']['attributes_groups'] = "Atrybuty produktów";
$aConfig['lang']['m_oferta_produktowa']['general_discount'] = "Rabat ogólny";
$aConfig['lang']['m_oferta_produktowa']['sources'] = "Rabat wg źródła";
$aConfig['lang']['m_oferta_produktowa']['authors_list'] = "Autorzy";
$aConfig['lang']['m_oferta_produktowa']['authors_roles'] = "Role autorów";
$aConfig['lang']['m_oferta_produktowa']['ceneo_history'] = "Historia ceneo";
$aConfig['lang']['m_oferta_produktowa']['publishers'] = "Wydawnictwa i serie";
$aConfig['lang']['m_oferta_produktowa']['publishers_groups'] = "Grupy wydawnicze";
$aConfig['lang']['m_oferta_produktowa']['notifications'] = "Powiadomienia";
$aConfig['lang']['m_oferta_produktowa']['languages'] = "Języki";
$aConfig['lang']['m_oferta_produktowa']['dimensions'] = "Format";
$aConfig['lang']['m_oferta_produktowa']['bindings'] = "Oprawy";
$aConfig['lang']['m_oferta_produktowa']['offers'] = "Generator ofert";
$aConfig['lang']['m_oferta_produktowa']['category_settings'] = "Ustawienia kategorii";
$aConfig['lang']['m_oferta_produktowa']['ceneo_tabs'] = "Zakładki ceneo (In, Out, VIP)";
$aConfig['lang']['m_oferta_produktowa']['invalid_wholesale_price'] = "Produkty z wyższą ceną zakupu niż katalogową";
$aConfig['lang']['m_oferta_produktowa']['product_amount'] = "Ilość produktów:";
$aConfig['lang']['m_oferta_produktowa']['years'] = "Słownik lat";
$aConfig['lang']['m_oferta_produktowa']['negative_discounts'] = "Produkty ujemnym rabatem";
$aConfig['lang']['m_oferta_produktowa']['not_mapped'] = "Niezmapowane kategorie";
$aConfig['lang']['m_oferta_produktowa']['negative_discounts'] = "Produkty ujemnym rabatem";
$aConfig['lang']['m_oferta_produktowa']['attachment_types'] = "Typy załączników";
$aConfig['lang']['m_oferta_produktowa']['vatstakes'] = "Stawki VAT";
$aConfig['lang']['m_oferta_produktowa']['mismatchvat'] = "Błędne stawki VAT";
$aConfig['lang']['m_oferta_produktowa']['tags'] = "Tagi";
$aConfig['lang']['m_oferta_produktowa']['file_types'] = "Typy plików";
$aConfig['lang']['m_oferta_produktowa']['stock_source_mappings'] = "Mapowania stanów";
$aConfig['lang']['m_oferta_produktowa']['comparison_sites'] = 'Konfiguracja porównywarek';
$aConfig['lang']['m_oferta_produktowa']['mark_up_cookie'] = 'Narzut na ofertę';
$aConfig['lang']['m_oferta_produktowa']['mark_up_cookie_buybox'] = 'Narzut na zejścia z BuyBox';
$aConfig['lang']['m_oferta_produktowa']['categories_types_mapping'] = 'Mapowanie głównych kategorii do typów produktów';

/* -------------- MODUŁ 'FINANSE'  --------------- */
$aConfig['lang']['m_finanse']['statements'] = "Import wyciągów";
$aConfig['lang']['m_finanse']['list_opek'] = "do Przelewy Pobrania";
$aConfig['lang']['m_finanse']['new_list_opek'] = "Przelewy Pobranie";
$aConfig['lang']['m_finanse']['bank_balances'] = "Nadpłaty";
$aConfig['lang']['m_finanse']['bank_renouncement'] = "Odstąpienia";
$aConfig['lang']['m_finanse']['bank_accounts'] = "Rachunki bankowe";
$aConfig['lang']['m_finanse']['bank_accounts_balances'] = "Salda bankowe";
$aConfig['lang']['m_finanse']['import_payments'] = "Pobierz płatności";

/* -------------- MODUL 'PROMOCJE' -------------- */
$aConfig['lang']['m_promocje']['default'] = "Strony promocji";
$aConfig['lang']['m_promocje']['codes'] = "Kody rabatowe";

/* -------------- MODUL 'ANKIETY' -------------- */
$aConfig['lang']['m_ankiety']['default'] = "Strony ankiet";
$aConfig['lang']['m_ankiety']['email_logs'] = "Logi rozsyłki";

/* -------------- MODUL 'KSIEGARNIE' -------------- */
$aConfig['lang']['m_ksiegarnie']['default'] = "Strony księgarni";

/* -------------- MODUL 'REALIZACJE' -------------- */
$aConfig['lang']['m_realizacje']['default'] = "Realizacje";
$aConfig['lang']['m_realizacje']['config'] = "Konfiguracja";

/* -------------- MODUL 'GALERIA' -------------- */
$aConfig['lang']['m_galeria']['default'] = "Strony galerii";
$aConfig['lang']['m_galeria']['settings'] = "Konfiguracja strony galerii";
$aConfig['lang']['m_galeria']['config'] = "Konfiguracja domyślna";

/* -------------- MODUL 'ARTYKULY' -------------- */
$aConfig['lang']['m_artykuly']['default'] = "Strony artykułów";
$aConfig['lang']['m_artykuly']['comments'] = "Komentarze";
$aConfig['lang']['m_artykuly']['settings'] = "Konfiguracja strony artykułów";
$aConfig['lang']['m_artykuly']['config'] = "Konfiguracja domyślna";

/* -------------- MODUL 'SONDY' -------------- */
$aConfig['lang']['m_sondy']['default'] = "Strony sond";
$aConfig['lang']['m_sondy']['settings'] = "Konfiguracja strony sond";
$aConfig['lang']['m_sondy']['config'] = "Konfiguracja domyślna";

$aConfig['lang']['m_stats']['default'] = "Statystki";
$aConfig['lang']['m_stats']['diff_stock_location'] = "Różnice w stanach magazynowych";
$aConfig['lang']['m_stats']['stat_ordered_items'] = "Najczęściej zamawiane pozycje";
$aConfig['lang']['m_stats']['stat_generated_items'] = "Przygotowane raporty";
$aConfig['lang']['m_stats']['external_products'] = "Produkty z wyzszą ceną";
$aConfig['lang']['m_stats']['packing'] = "Pakowanie";
$aConfig['lang']['m_stats']['completing'] = "Kompletowanie";
$aConfig['lang']['m_stats']['confirm_items'] = "Przyjmowanie dostaw";
$aConfig['lang']['m_stats']['sorting'] = "Sortowanie";
$aConfig['lang']['m_stats']['exhausted_products'] = "Produkty wyczerpane";
$aConfig['lang']['m_stats']['packed_send'] = "Statystyka spakowane/wysłane";
$aConfig['lang']['m_stats']['buffer_erp'] = "Buffor ERP";

$aConfig['lang']['m_stats']['v2'] = "v2 ALL";
$aConfig['lang']['m_stats']['v3'] = "v3 TODAY";


/* -------------- MODUL 'SEO' -------------- */
$aConfig['lang']['m_seo']['default'] = "Dedykowany opis";
$aConfig['lang']['m_seo']['empty_categories'] = "Puste opisy kategorii";

/* -------------- MODUL 'ZBIERANIE WYSOKIE' -------------- */
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['default'] = 'Doładowanie do A';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['default_przyjecia'] = "Przypisz produkt<br /> na Stock";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['unpacking_SH'] = "Rozkladanie z B";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['sell_predict'] = "&bull; Do zebrania z B";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_wysokie']['unpacking_SL'] = "&bull; Rozkladanie";

//$aConfig['lang']['m_zamowienia_magazyn_zbieranie_niskie']['default'] = 'Doładowanie do B';
//$aConfig['lang']['m_zamowienia_magazyn_zbieranie_niskie']['high_level_all'] = 'Doładowanie do B';
//$aConfig['lang']['m_zamowienia_magazyn_zbieranie_niskie']['default_przyjecia'] = 'Przypisz produkt<br /> na Stock';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_niskie']['unpacking_SL'] = "&bull;	Rozkladanie";
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_niskie']['localization'] = 'Lokalizator';
$aConfig['lang']['m_zamowienia_magazyn_zbieranie_niskie']['sell_predict'] = "&bull;	INFO Do zebrania z B";


$aConfig['lang']['m_multilocalization']['high_magazine_distribution'] = "Rozkładanie wysokie";

$aConfig['lang']['m_recenzje']['reviews_list'] = 'Lista';
$aConfig['lang']['m_recenzje']['reviews_users'] = 'Użytkownicy';
