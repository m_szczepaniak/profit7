<?php

require_once( 'class.magic-min.php' );

$vars = array(
    'echo' => false,
    'encode' => false,
    'timer' => true,
    'gzip' => false,
    'closure' => false,
    'remove_comments' => true,
    'force_rebuild' => false, //USE THIS SPARINGLY!
    'hashed_filenames' => false,
);

$minified = new Minifier( $vars );

$included_styles = array(
    'css/slimbox.css',
    'js/jqueryui/custom-theme/jquery-ui.min.css',
    'js/jqueryui/custom-theme/jquery-ui-mod.css',
    'css/styles.css',
    'css/slick.css'
);

?>