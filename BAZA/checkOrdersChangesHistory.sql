BEGIN

	IF (NEW.profit_g_last_import != OLD.profit_g_last_import) THEN
	 IF (NEW.profit_g_status != (NEW.profit_g_act_stock - NEW.profit_g_reservations)) THEN
	  SET NEW.profit_g_status = (NEW.profit_g_act_stock - NEW.profit_g_reservations);
	 END IF;
	END IF;

	IF (NEW.profit_j_last_import != OLD.profit_j_last_import) THEN
	 IF (NEW.profit_j_status != (NEW.profit_j_act_stock - NEW.profit_j_reservations)) THEN
	  SET NEW.profit_j_status = (NEW.profit_j_act_stock - NEW.profit_j_reservations);
	 END IF;
	END IF;

	IF (NEW.profit_e_last_import != OLD.profit_e_last_import) THEN
	 IF (NEW.profit_e_status != (NEW.profit_e_act_stock - NEW.profit_e_reservations)) THEN
	  SET NEW.profit_e_status = (NEW.profit_e_act_stock - NEW.profit_e_reservations);
	 END IF;
	END IF;

    IF (
        NEW.abe_price_brutto <> OLD.abe_price_brutto OR 
        NEW.abe_price_netto <> OLD.abe_price_netto OR 
        NEW.abe_shipment_date <> OLD.abe_shipment_date OR 
        NEW.abe_shipment_time <> OLD.abe_shipment_time OR 
        NEW.abe_status <> OLD.abe_status OR 
        NEW.abe_vat <> OLD.abe_vat OR 
        NEW.ateneum_price_brutto <> OLD.ateneum_price_brutto OR 
        NEW.ateneum_price_netto <> OLD.ateneum_price_netto OR 
        NEW.ateneum_shipment_date <> OLD.ateneum_shipment_date OR 
        NEW.ateneum_shipment_time <> OLD.ateneum_shipment_time OR 
        NEW.ateneum_status <> OLD.ateneum_status OR 
        NEW.ateneum_stock <> OLD.ateneum_stock OR 
        NEW.ateneum_vat <> OLD.ateneum_vat OR 
        NEW.ateneum_wholesale_price <> OLD.ateneum_wholesale_price OR 
/*
        NEW.azymutobc_price_brutto <> OLD.azymutobc_price_brutto OR 
        NEW.azymutobc_price_netto <> OLD.azymutobc_price_netto OR 
        NEW.azymutobc_shipment_date <> OLD.azymutobc_shipment_date OR 
        NEW.azymutobc_shipment_time <> OLD.azymutobc_shipment_time OR 
        NEW.azymutobc_status <> OLD.azymutobc_status OR 
        NEW.azymutobc_stock <> OLD.azymutobc_stock OR 
        NEW.azymutobc_vat <> OLD.azymutobc_vat OR 
        NEW.azymutobc_wholesale_price <> OLD.azymutobc_wholesale_price OR 
*/
        NEW.azymut_price_brutto <> OLD.azymut_price_brutto OR 
        NEW.azymut_price_netto <> OLD.azymut_price_netto OR 
        NEW.azymut_shipment_date <> OLD.azymut_shipment_date OR 
        NEW.azymut_shipment_time <> OLD.azymut_shipment_time OR 
        NEW.azymut_status <> OLD.azymut_status OR 
        NEW.azymut_stock <> OLD.azymut_stock OR 
        NEW.azymut_vat <> OLD.azymut_vat OR 
        NEW.azymut_wholesale_price <> OLD.azymut_wholesale_price OR 
        NEW.dictum_price_brutto <> OLD.dictum_price_brutto OR 
        NEW.dictum_price_netto <> OLD.dictum_price_netto OR 
        NEW.dictum_shipment_date <> OLD.dictum_shipment_date OR 
        NEW.dictum_shipment_time <> OLD.dictum_shipment_time OR 
        NEW.dictum_status <> OLD.dictum_status OR 
        NEW.dictum_stock <> OLD.dictum_stock OR 
        NEW.dictum_vat <> OLD.dictum_vat OR 
        NEW.dictum_wholesale_price <> OLD.dictum_wholesale_price OR 
        NEW.helion_price_brutto <> OLD.helion_price_brutto OR 
        NEW.helion_price_netto <> OLD.helion_price_netto OR 
        NEW.helion_shipment_date <> OLD.helion_shipment_date OR 
        NEW.helion_shipment_time <> OLD.helion_shipment_time OR 
        NEW.helion_status <> OLD.helion_status OR 
        NEW.helion_stock <> OLD.helion_stock OR 
        NEW.helion_vat <> OLD.helion_vat OR 
        NEW.helion_wholesale_price <> OLD.helion_wholesale_price OR 
        NEW.olesiejuk_price_brutto <> OLD.olesiejuk_price_brutto OR 
        NEW.olesiejuk_price_netto <> OLD.olesiejuk_price_netto OR 
        NEW.olesiejuk_shipment_date <> OLD.olesiejuk_shipment_date OR 
        NEW.olesiejuk_shipment_time <> OLD.olesiejuk_shipment_time OR 
        NEW.olesiejuk_status <> OLD.olesiejuk_status OR 
        NEW.olesiejuk_stock <> OLD.olesiejuk_stock OR 
        NEW.olesiejuk_vat <> OLD.olesiejuk_vat OR 
        NEW.olesiejuk_wholesale_price <> OLD.olesiejuk_wholesale_price OR 
        NEW.platon_price_brutto <> OLD.platon_price_brutto OR 
        NEW.platon_price_netto <> OLD.platon_price_netto OR 
        NEW.platon_shipment_date <> OLD.platon_shipment_date OR 
        NEW.platon_shipment_time <> OLD.platon_shipment_time OR 
        NEW.platon_status <> OLD.platon_status OR 
        NEW.platon_stock <> OLD.platon_stock OR 
        NEW.platon_vat <> OLD.platon_vat OR 
        NEW.platon_wholesale_price <> OLD.platon_wholesale_price OR 
        NEW.profit_e_act_stock <> OLD.profit_e_act_stock OR 
        NEW.profit_e_location <> OLD.profit_e_location OR 
        NEW.profit_e_price_brutto <> OLD.profit_e_price_brutto OR 
        NEW.profit_e_price_netto <> OLD.profit_e_price_netto OR 
        NEW.profit_e_reservations <> OLD.profit_e_reservations OR 
        NEW.profit_e_shipment_date <> OLD.profit_e_shipment_date OR 
        NEW.profit_e_shipment_time <> OLD.profit_e_shipment_time OR 
        NEW.profit_e_status <> OLD.profit_e_status OR 
        NEW.profit_e_vat <> OLD.profit_e_vat OR 
        NEW.profit_e_wholesale_price <> OLD.profit_e_wholesale_price OR 
        NEW.profit_g_act_stock <> OLD.profit_g_act_stock OR 
        NEW.profit_g_location <> OLD.profit_g_location OR 
        NEW.profit_g_price_brutto <> OLD.profit_g_price_brutto OR 
        NEW.profit_g_price_netto <> OLD.profit_g_price_netto OR 
        NEW.profit_g_reservations <> OLD.profit_g_reservations OR 
        NEW.profit_g_shipment_date <> OLD.profit_g_shipment_date OR 
        NEW.profit_g_shipment_time <> OLD.profit_g_shipment_time OR 
        NEW.profit_g_status <> OLD.profit_g_status OR 
        NEW.profit_g_vat <> OLD.profit_g_vat OR 
        NEW.profit_g_wholesale_price <> OLD.profit_g_wholesale_price OR 
        NEW.profit_j_act_stock <> OLD.profit_j_act_stock OR 
        NEW.profit_j_location <> OLD.profit_j_location OR 
        NEW.profit_j_price_brutto <> OLD.profit_j_price_brutto OR 
        NEW.profit_j_price_netto <> OLD.profit_j_price_netto OR 
        NEW.profit_j_reservations <> OLD.profit_j_reservations OR 
        NEW.profit_j_shipment_date <> OLD.profit_j_shipment_date OR 
        NEW.profit_j_shipment_time <> OLD.profit_j_shipment_time OR 
        NEW.profit_j_status <> OLD.profit_j_status OR 
        NEW.profit_j_vat <> OLD.profit_j_vat OR 
        NEW.profit_j_wholesale_price <> OLD.profit_j_wholesale_price OR 
        NEW.profit_m_location <> OLD.profit_m_location OR 
        NEW.profit_m_price_brutto <> OLD.profit_m_price_brutto OR 
        NEW.profit_m_price_netto <> OLD.profit_m_price_netto OR 
        NEW.profit_m_shipment_date <> OLD.profit_m_shipment_date OR 
        NEW.profit_m_shipment_time <> OLD.profit_m_shipment_time OR 
        NEW.profit_m_status <> OLD.profit_m_status OR 
        NEW.profit_m_vat <> OLD.profit_m_vat OR 
        NEW.profit_m_wholesale_price <> OLD.profit_m_wholesale_price OR 
        NEW.profit_x_location <> OLD.profit_x_location OR 
        NEW.profit_x_price_brutto <> OLD.profit_x_price_brutto OR 
        NEW.profit_x_price_netto <> OLD.profit_x_price_netto OR 
        NEW.profit_x_shipment_date <> OLD.profit_x_shipment_date OR 
        NEW.profit_x_shipment_time <> OLD.profit_x_shipment_time OR 
        NEW.profit_x_status <> OLD.profit_x_status OR 
        NEW.profit_x_vat <> OLD.profit_x_vat OR 
        NEW.profit_x_wholesale_price <> OLD.profit_x_wholesale_price OR 
        NEW.siodemka_price_brutto <> OLD.siodemka_price_brutto OR 
        NEW.siodemka_price_netto <> OLD.siodemka_price_netto OR 
        NEW.siodemka_shipment_date <> OLD.siodemka_shipment_date OR 
        NEW.siodemka_shipment_time <> OLD.siodemka_shipment_time OR 
        NEW.siodemka_status <> OLD.siodemka_status OR 
        NEW.siodemka_stock <> OLD.siodemka_stock OR 
        NEW.siodemka_vat <> OLD.siodemka_vat OR 
        NEW.siodemka_wholesale_price <> OLD.siodemka_wholesale_price
    ) THEN
        IF ((SELECT count(1) FROM products_stock_watch WHERE id = OLD.id) <= 0) THEN
            INSERT INTO products_stock_watch SET id = OLD.id;
        END IF;
    END IF;

END