
delimiter // 
DROP PROCEDURE IF EXISTS `check_linked_orders`//
CREATE PROCEDURE check_linked_orders (in __order_id INT, out __return CHAR(1)) 
BEGIN  


DECLARE done INT DEFAULT 0;
DECLARE __linked_order_id INTEGER DEFAULT 0;
DECLARE __ready_linked_order_id INTEGER DEFAULT 0;

DECLARE linked_cursor CURSOR FOR 
        SELECT linked_order_id
        FROM orders_linked_transport
        WHERE main_order_id = __order_id;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

MAIN_BLOCK: begin 
SET __return = "1";

OPEN linked_cursor;
  read_loop: LOOP

  FETCH linked_cursor INTO __linked_order_id;
    IF done THEN
      LEAVE read_loop;
	END IF;

	IF (__linked_order_id > 0) THEN

		SET __ready_linked_order_id = 0;

		SELECT DISTINCT O.id INTO __ready_linked_order_id
		FROM orders_items AS OI
			JOIN orders AS O
			ON 
				O.id = __linked_order_id
				AND OI.order_id = O.id
        AND IF (OI.source = '51', (OI.status = '3' OR OI.status = '4'), OI.status = '4')
        AND OI.get_ready_list = '0'
        AND OI.deleted = '0'
        AND (OI.item_type = 'I' OR OI.item_type = 'P')
        AND OI.packet = '0'
			-- uwaga warunki muszą dotyczyć wszystkich składowych, a nie tylko jednej
			LEFT JOIN orders_items AS OI_TEST
			ON OI_TEST.order_id = O.id
        AND IF (OI_TEST.source = '51', OI_TEST.status <> '3' AND OI_TEST.status <> '4', OI_TEST.status <> '4')
        AND OI_TEST.get_ready_list = '0'
        AND OI_TEST.deleted = '0'
        AND (OI_TEST.item_type = 'I' OR OI_TEST.item_type = 'P')
        AND OI_TEST.packet = '0'
			WHERE 
			  OI_TEST.id IS NULL

			  AND
			  (
          (
            (
            O.payment_type = 'bank_transfer' OR
            O.payment_type = 'platnoscipl' OR
            O.payment_type = 'card'
            )
            AND O.to_pay <= O.paid_amount
          )
          OR
          (
            O.payment_type = 'postal_fee' OR
            O.payment_type = 'bank_14days'
          )
          OR
          (
            O.second_payment_type = 'postal_fee' OR
            O.second_payment_type = 'bank_14days'
          )
			  )


			  AND 
			  (
				O.order_status = '1' OR
				O.order_status = '2' 
			  )

			  AND 
			  ( O.send_date = '0000-00-00' OR (O.send_date <> '0000-00-00' AND O.send_date < NOW()) );



			 IF (__ready_linked_order_id <= 0) THEN
				/*WYSTAPIL NIE GOTOWY WIEC TU MOZNA SKONCZYC*/
				SET __return = "0";
				leave MAIN_BLOCK; 
			 ELSE
				/*WYSTAPIL PRAWIDŁOWY*/
				SET __return = "1";
			 END IF;

		ELSE
			/*BRAK REKORDU*/
			SET __return = "1";
		END IF;

	END LOOP;
CLOSE linked_cursor;

END MAIN_BLOCK; 
END//