UPDATE
products
SET
name = REPLACE(name, "(ot)", "")
WHERE 
name LIKE '%(ot)%'

UPDATE
products
SET
name = REPLACE(name, "(om)", "")
WHERE 
name LIKE '%(om)%'

UPDATE
products
SET
name = REPLACE(name, "(bpz)", "")
WHERE 
name LIKE '%(bpz)%'



UPDATE
products_azymut_buffer_copy
SET
description = REPLACE(description, "<h2>Zawartość audiobooka:</h2></div>", "</div>")
WHERE
description LIKE '%<h2>Zawartość audiobooka:</h2></div>%'