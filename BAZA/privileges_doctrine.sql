GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`magazines` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`magazine_localizations` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`magazine_orders_items_remove` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`magazine_products_receipts` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`magazine_products_recipients_containers` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`magazine_products_to_localizations` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`magazine_receipts` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`magazine_remove_products` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`products_isbn` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`external_providers` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`orders_send_history` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`orders_send_history_items` TO  'doctrine'@'%';

GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`orders_items_lists` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`orders_items_lists_items` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`products` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`users` TO  'doctrine'@'%';
GRANT SELECT , INSERT , UPDATE , REFERENCES ON  `main_profit24_old`.`users_accounts` TO  'doctrine'@'%';

