SELECT  P.name, PP.name AS wydawnictwo, P.isbn_plain FROM `products` AS P
JOIN products_publishers AS PP
  ON PP.id = P.publisher_id
JOIN products_stock AS PS
  ON P.id = PS.id
WHERE `published` = '1' AND `prod_status` = '2' AND
(
  PS.azymut_status  = '1' OR
  PS.abe_status = '1' OR
  PS.helion_status = '1' OR
  PS.dictum_status = '1' OR
  PS.siodemka_status = '1' OR
  PS.olesiejuk_status = '1' OR
  PS.ateneum_status = '1' OR
  PS.profit_g_status > 0 OR
  PS.profit_j_status > 0 OR
  PS.profit_m_status > 0
)
