
SELECT invoice2_id, COUNT( invoice_id ) , COUNT( invoice2_id ) , (
COUNT( invoice_id ) + COUNT( invoice2_id )
) AS SUM, DATE( invoice_date ) , website_id, SUM(to_pay)
FROM  `orders` 
WHERE invoice_id IS NOT NULL 
AND DATE( invoice_date ) >=  '2015-05-11'
AND order_status IN ( "3", "4" )
GROUP BY DATE( invoice_date ) , website_id
ORDER BY invoice2_id DESC


SELECT invoice_id
FROM  `orders` 
WHERE DATE( invoice_date ) >=  '2015-05-11'
AND invoice_id IS NOT NULL
ORDER BY invoice_id DESC;
