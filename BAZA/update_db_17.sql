ALTER TABLE products_publishers
ADD `ignore_discount` ENUM('0','1') NOT NULL DEFAULT '0';

INSERT INTO `products_comparison_sites` (shipment_time, `text`)
VALUES
('n','1.00'),
('m','0.05'),
('p','1.00'),
('o','1000');


ALTER TABLE `websites`
ADD `ceneo` VARCHAR(150) NOT NULL;

UPDATE `websites`
SET ceneo='profit24.pl'
WHERE id=1;

UPDATE `websites`
SET ceneo='nieprzeczytane.pl'
WHERE id=3;

ALTER TABLE products_tarrifs
ADD `type` TINYINT(1) DEFAULT NULL;

ALTER TABLE products
ADD `main_category_id` INT(10) UNSIGNED NULL DEFAULT NULL;

CREATE TABLE `website_ceneo_categories` (
	`category_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`website_id` INT(11) UNSIGNED NULL DEFAULT NULL,
	INDEX `fk_website_ceneo_categories_category_id` (`category_id`),
	INDEX `fk_website_ceneo_categories_website_id` (`website_id`),
	CONSTRAINT `fk_website_ceneo_categories_category_id` FOREIGN KEY (`category_id`) REFERENCES `menus_items` (`id`),
	CONSTRAINT `fk_website_ceneo_categories_website_id` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

 insert into website_ceneo_categories( SELECT id AS category_id, null
	FROM menus_items
 WHERE module_id = 26
 AND mtype = '0'
 AND parent_id
 IN
 (
 SELECT id
 FROM menus_items
 WHERE module_id = 26
 AND mtype = '0'
 AND parent_id IS NULL
 AND published = '1'
 )
 ORDER BY priority);

CREATE TABLE `cyclic_buffor` (
	`product_id` INT(11) NOT NULL,
	`priority` CHAR(1) NOT NULL DEFAULT '0',
	PRIMARY KEY (`product_id`),
	INDEX `priority` (`priority`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


COLLATE='utf8_general_ci'
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `tarrifs_buffor` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`product_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`discount` DECIMAL(5,2) NOT NULL DEFAULT '0.00',
	`discount_value` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`price_netto` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`price_brutto` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	PRIMARY KEY (`id`),
	INDEX `product_id` (`product_id`),
	CONSTRAINT `FK1_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;

CREATE TABLE `update_now_buffor` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`product_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`discount` DECIMAL(5,2) NOT NULL DEFAULT '0.00',
	`discount_value` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`price_netto` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`price_brutto` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`minimum` DECIMAL(8,2) NULL DEFAULT NULL,
	`change_type` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `product_id` (`product_id`),
	CONSTRAINT `FK1_update_now_product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB


CREATE TABLE `ceneo_locks` (
	`name` VARCHAR(50) NULL DEFAULT NULL,
	`value` CHAR(1) NULL DEFAULT NULL
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;


INSERT INTO `ceneo_locks` (`name`, `value`) VALUES ('update', '0');
INSERT INTO `ceneo_locks` (`name`, `value`) VALUES ('compare', '0');


CREATE TABLE IF NOT EXISTS `main_profit24_old`.`users_to_products_publishers` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_publishers_id` INT(10) UNSIGNED NOT NULL,
  `users_accounts_id` INT(10) UNSIGNED NOT NULL,
  `user_adress_id` INT(10) UNSIGNED NOT NULL,
  `discount` DOUBLE(6,2) NOT NULL DEFAULT 0,
  `created_date` DATE NOT NULL,
  `created_by` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_to_products_publishers_products_publishers_idx` (`products_publishers_id` ASC),
  INDEX `fk_users_to_products_publishers_users_accounts1_idx` (`users_accounts_id` ASC),
  INDEX `fk_user_adress_idx` (`user_adress_id` ASC),
  CONSTRAINT `fk_users_to_products_publishers_products_publishers`
    FOREIGN KEY (`products_publishers_id`)
    REFERENCES `main_profit24_old`.`products_publishers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_to_products_publishers_users_accounts1`
    FOREIGN KEY (`users_accounts_id`)
    REFERENCES `main_profit24_old`.`users_accounts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_adress`
    FOREIGN KEY (`user_adress_id`)
    REFERENCES `main_profit24_old`.`users_accounts_address` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE IF NOT EXISTS `main_profit24_old`.`users_to_prod_series_publishers` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `users_to_products_publishers_id` INT(10) UNSIGNED NOT NULL,
  `users_accounts_id` INT(10) UNSIGNED NOT NULL,
  `products_series_id` INT(10) UNSIGNED NOT NULL,
  `discount` DOUBLE(8,2) NOT NULL DEFAULT 0,
  `created_date` DATE NOT NULL,
  `created_by` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_to_prod_series_publishers_users_accounts1_idx` (`users_accounts_id` ASC),
  INDEX `fk_users_to_prod_series_publishers_products_series1_idx` (`products_series_id` ASC),
  INDEX `fk_users_to_prod_series_publishers_products_publishers1_idx` (`users_to_products_publishers_id` ASC),
  CONSTRAINT `fk_users_to_prod_series_publishers_products_publishers1`
    FOREIGN KEY (`users_to_products_publishers_id`)
    REFERENCES `main_profit24_old`.`users_to_products_publishers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_to_prod_series_publishers_users_accounts1`
    FOREIGN KEY (`users_accounts_id`)
    REFERENCES `main_profit24_old`.`users_accounts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_to_prod_series_publishers_products_series1`
    FOREIGN KEY (`products_series_id`)
    REFERENCES `main_profit24_old`.`products_series` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)

ALTER TABLE `orders_users_addresses` ADD `user_adress_id` INT(16) NULL DEFAULT NULL ;

ALTER TABLE `orders_items_lists_items` ADD `container_id` INT UNSIGNED ZEROFILL NOT NULL AFTER `orders_id`;
ALTER TABLE `orders_items_lists_items` ADD INDEX(`container_id`);

ALTER TABLE `orders_items_lists_items` 
ADD CONSTRAINT `fk_orders_items_lists_items_orders_items1cionter` 
FOREIGN KEY (`container_id`) REFERENCES `main_profit24_old`.`containers`(`id`) 
ON DELETE CASCADE ON UPDATE CASCADE;
