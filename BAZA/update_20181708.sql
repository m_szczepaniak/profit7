CREATE TABLE `recommend_products` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT(10) UNSIGNED NOT NULL,
  `recommended_product_id` INT(10) UNSIGNED NOT NULL,
  `priority` INT,
  PRIMARY KEY (`id`),
  INDEX `fk_recommend_products_products1_idx` (`product_id` ASC),
  CONSTRAINT `fk_recommend_products_products1`
  FOREIGN KEY (`product_id`)
  REFERENCES `products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  INDEX `fk_recommend_products_products2_idx` (`product_id` ASC),
  CONSTRAINT `fk_recommend_products_products2`
  FOREIGN KEY (`recommended_product_id`)
  REFERENCES `products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);