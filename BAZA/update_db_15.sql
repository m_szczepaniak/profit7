CREATE TABLE IF NOT EXISTS `main_profit24`.`orders_items_lists_tranport` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `orders_items_lists_id` INT UNSIGNED NOT NULL,
  `orders_transport_means_id` TINYINT(3) UNSIGNED NOT NULL,
  INDEX `fk_orders_items_lists_tranport_orders_items_lists1_idx` (`orders_items_lists_id` ASC),
  INDEX `fk_orders_items_lists_tranport_orders_transport_means1_idx` (`orders_transport_means_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_orders_items_lists_tranport_orders_items_lists1`
    FOREIGN KEY (`orders_items_lists_id`)
    REFERENCES `main_profit24`.`orders_items_lists` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_items_lists_tranport_orders_transport_means1`
    FOREIGN KEY (`orders_transport_means_id`)
    REFERENCES `main_profit24`.`orders_transport_means` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;