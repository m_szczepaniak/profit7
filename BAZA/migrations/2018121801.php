<?php

use Migrations\Migration;

class Version2018121801 extends Migration {
    function migrate() {
        $this->pDbMgr->BeginTransaction('profit24');
        try {
            $this->pDbMgr->Query("profit24", "ALTER TABLE products_stock ADD merlin_wholesale_price decimal(8,2), ADD merlin_price_netto decimal(8,2), ADD merlin_price_brutto decimal(8,2), ADD merlin_shipment_time char(1), ADD merlin_shipment_date date, ADD merlin_vat tinyint(4), ADD merlin_last_import datetime, ADD merlin_status char(1)");        } catch (\Exception $e) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return true;
        }
        $this->pDbMgr->CommitTransaction('profit24');
        return false;
    }
}