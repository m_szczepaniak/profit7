<?php

use Migrations\Migration;

class Version2018121701 extends Migration {
    function migrate() {
        $this->pDbMgr->BeginTransaction('profit24');
        try {
            $this->pDbMgr->Query('profit24', 'CREATE TABLE migration_versions (version varchar(255) PRIMARY KEY)');
        } catch (\Exception $e) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return true;
        }
        $this->pDbMgr->CommitTransaction('profit24');
        return false;

    }
}