<?php

use Migrations\Migration;

class Version2018122001 extends Migration {
    function migrate() {
        $this->pDbMgr->BeginTransaction('profit24');
        try {
            $this->pDbMgr->PlainQuery('profit24', 'ALTER TABLE website_emails ADD merlin_statuses varchar(255);');
        } catch (\Exception $e) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return true;
        }
        $this->pDbMgr->CommitTransaction('profit24');
        return false;
    }
}