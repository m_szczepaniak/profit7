<?php

use Migrations\Migration;

class Version2018122102 extends Migration {
    function migrate() {
        $this->pDbMgr->BeginTransaction('profit24');
        try {
            // Tutaj wykonaj operacje na bazie ..
        } catch (\Exception $e) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return true;
        }
        $this->pDbMgr->CommitTransaction('profit24');
        return false;
    }
}