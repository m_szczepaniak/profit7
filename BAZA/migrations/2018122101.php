<?php

use Migrations\Migration;

class Version2018122101 extends Migration {
    function migrate() {
        $this->pDbMgr->BeginTransaction('profit24');
        try {
            $this->pDbMgr->Query('profit24', 'INSERT INTO `profit2`.`sources` (`provider_id`, `symbol`, `column_prefix_products_stock`, `order_by`, `select_privileged`) VALUES (102, \'merlin\', \'merlin\', 14, \'0\')');
        } catch (\Exception $e) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return true;
        }
        $this->pDbMgr->CommitTransaction('profit24');
        return false;
    }
}