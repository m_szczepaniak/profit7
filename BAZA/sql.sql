INSERT INTO `main_nieprzeczytane`.`orders_platnosci_payment_types` (`id`, `ptype`, `image`, `vmin`, `vmax`, `name`) VALUES (NULL, 'nlx', 'https://www.platnosci.pl/static/images/paytype/on-nlx.gif', '0.50', '999999.99', 'Noble Bank');
INSERT INTO `main_nieprzeczytane`.`orders_platnosci_payment_types` (`id`, `ptype`, `image`, `vmin`, `vmax`, `name`) VALUES (NULL, 'ib', 'https://www.platnosci.pl/static/images/paytype/on-ib.gif', '0.50', '999999.99', 'Paylink Idea - IdeaBank');
UPDATE `main_nieprzeczytane`.`orders_platnosci_payment_types` SET `name` = 'Credit Agricole' WHERE `orders_platnosci_payment_types`.`id` = 76;
INSERT INTO `main_nieprzeczytane`.`orders_platnosci_payment_types` (`id`, `ptype`, `image`, `vmin`, `vmax`, `name`) VALUES (NULL, 'ps', 'https://www.platnosci.pl/static/images/paytype/on-ps.gif', '0.50', '999999.99', 'PBS');
INSERT INTO `main_nieprzeczytane`.`orders_platnosci_payment_types` (`id`, `ptype`, `image`, `vmin`, `vmax`, `name`) VALUES (NULL, 'bo', 'https://www.platnosci.pl/static/images/paytype/on-bo.gif', '0.50', '999999.99', 'BOŚ');
INSERT INTO `main_nieprzeczytane`.`orders_platnosci_payment_types` (`id`, `ptype`, `image`, `vmin`, `vmax`, `name`) VALUES (NULL, 'bnx', 'https://www.platnosci.pl/static/images/paytype/on-bnx.gif', '0.50', '999999.99', 'BNP Paribas');
INSERT INTO `main_nieprzeczytane`.`orders_platnosci_payment_types` (`id`, `ptype`, `image`, `vmin`, `vmax`, `name`) VALUES (NULL, 'orx', 'https://www.platnosci.pl/static/images/paytype/on-orx.gif', '0.50', '999999.99', 'Płacę z Orange');

EXPLAIN UPDATE products_stock A
                                         SET
            A.azymut_status = '0',
            A.azymut_last_import = '2016-07-05 06:30:15',
            A.azymut_stock = NULL
                                         WHERE A.azymut_status = '1'
                                         AND A.azymut_last_import != '2016-07-05';









BEGIN

DECLARE curr_prod_status ENUM('0','1','2','3');
DECLARE curr_published ENUM('0','1');
DECLARE curr_ommit_auto_preview CHAR(1);
DECLARE komunikat VARCHAR(255);

DECLARE NEW_prod_status ENUM('0','1','2','3');
DECLARE NEW_prod_location VARCHAR(45);
DECLARE NEW_prod_price_brutto DECIMAL(8,2);
DECLARE NEW_prod_price_netto DECIMAL(8,2);
DECLARE MIN_wholesale_price DECIMAL(8,2);
DECLARE CMP_wholesale_price DECIMAL(8,2);
DECLARE NEW_prod_vat TINYINT(4);
DECLARE NEW_prod_source CHAR(2);
DECLARE NEW_prod_shipment_time ENUM('0', '1', '2', '3');
DECLARE NEW_prod_shipment_date DATE;
DECLARE att_price_netto DECIMAL(8,2);
DECLARE changed_prod_status INT;
DECLARE original_prod_status INT;


SELECT '0.00' INTO NEW_prod_price_brutto;
SELECT 0.00 INTO MIN_wholesale_price;
SELECT 0.00 INTO CMP_wholesale_price;
SELECT '' INTO NEW_prod_location;
SELECT 'TEST' INTO komunikat;

SELECT price_netto INTO att_price_netto FROM products_attachments WHERE product_id = NEW.id;
SELECT prod_status,published,ommit_auto_preview INTO curr_prod_status,curr_published,curr_ommit_auto_preview FROM products WHERE id = NEW.id;

IF(curr_prod_status <> '2') THEN
      SELECT '1' INTO NEW_prod_status;

   IF (NEW.profit_g_status > 0 AND NEW.profit_g_price_brutto > 0.00) THEN
      SELECT '10' INTO NEW_prod_source;
      SELECT NEW.profit_g_wholesale_price INTO MIN_wholesale_price;
      SELECT NEW.profit_g_price_brutto INTO NEW_prod_price_brutto;
      SELECT NEW.profit_g_price_netto INTO NEW_prod_price_netto;
      SELECT NEW.profit_g_vat INTO NEW_prod_vat;
      SELECT '0' INTO NEW_prod_shipment_time;
      SELECT NEW.profit_g_location INTO NEW_prod_location;
      SELECT NEW.profit_g_shipment_date INTO NEW_prod_shipment_date;
      SELECT 'G' INTO komunikat;
   ELSEIF (NEW.profit_e_status > 0 AND NEW.profit_e_price_brutto > 0.00) THEN
      SELECT '1' INTO NEW_prod_source;
      SELECT NEW.profit_e_wholesale_price INTO MIN_wholesale_price;
      SELECT NEW.profit_e_price_brutto INTO NEW_prod_price_brutto;
      SELECT NEW.profit_e_price_netto INTO NEW_prod_price_netto;
      SELECT NEW.profit_e_vat INTO NEW_prod_vat;
      SELECT '0' INTO NEW_prod_shipment_time;
      SELECT NEW.profit_e_location INTO NEW_prod_location;
      SELECT NEW.profit_e_shipment_date INTO NEW_prod_shipment_date;
      SELECT 'E' INTO komunikat;
   ELSEIF(NEW.profit_j_status > 0 AND NEW.profit_j_price_brutto > 0.00) THEN
      SELECT '0' INTO NEW_prod_source;
      SELECT NEW.profit_j_wholesale_price INTO MIN_wholesale_price;
      SELECT NEW.profit_j_price_brutto INTO NEW_prod_price_brutto;
      SELECT NEW.profit_j_price_netto INTO NEW_prod_price_netto;
      SELECT NEW.profit_j_vat INTO NEW_prod_vat;
      SELECT '0' INTO NEW_prod_shipment_time;
      SELECT NEW.profit_j_location INTO NEW_prod_location;
      SELECT NEW.profit_j_shipment_date INTO NEW_prod_shipment_date;
      SELECT 'J' INTO komunikat;
   ELSEIF (NEW.profit_m_status > 0 AND NEW.profit_m_price_brutto > 0.00) THEN
      SELECT '1' INTO NEW_prod_source;
      SELECT NEW.profit_m_wholesale_price INTO MIN_wholesale_price;
      SELECT NEW.profit_m_price_brutto INTO NEW_prod_price_brutto;
      SELECT NEW.profit_m_price_netto INTO NEW_prod_price_netto;
      SELECT NEW.profit_m_vat INTO NEW_prod_vat;
      SELECT '0' INTO NEW_prod_shipment_time;
      SELECT NEW.profit_m_location INTO NEW_prod_location;
      SELECT NEW.profit_m_shipment_date INTO NEW_prod_shipment_date;
      SELECT 'M' INTO komunikat;
   ELSEIF (NEW.profit_x_status > 0 AND NEW.profit_x_price_brutto > 0.00) THEN
      SELECT '1' INTO NEW_prod_source;
      SELECT NEW.profit_x_wholesale_price INTO MIN_wholesale_price;
      SELECT NEW.profit_x_price_brutto INTO NEW_prod_price_brutto;
      SELECT NEW.profit_x_price_netto INTO NEW_prod_price_netto;
      SELECT NEW.profit_x_vat INTO NEW_prod_vat;
      SELECT '0' INTO NEW_prod_shipment_time;
      SELECT NEW.profit_x_location INTO NEW_prod_location;
      SELECT NEW.profit_x_shipment_date INTO NEW_prod_shipment_date;
      SELECT 'X' INTO komunikat;
   ELSE


      IF (NEW.abe_status > 0 AND NEW.abe_price_brutto > 0.00 AND (NEW_prod_price_brutto = 0.00 OR (NEW_prod_price_brutto > 0.00 AND NEW_prod_price_brutto > (NEW.abe_status * NEW.abe_price_brutto)))) THEN
          SELECT CAST(NEW.abe_status AS CHAR) INTO NEW_prod_status;
          SELECT NEW.abe_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.abe_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.abe_vat INTO NEW_prod_vat;
          SELECT NEW.abe_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.abe_shipment_date INTO NEW_prod_shipment_date;
          SELECT '3' INTO NEW_prod_source;
          SELECT 'abe' INTO komunikat;
      END IF;

      IF (NEW.azymut_status > 0 AND NEW.azymut_price_brutto > 0.00 AND NEW.azymut_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.azymut_status * NEW.azymut_wholesale_price)))) THEN
          SELECT CAST(NEW.azymut_status AS CHAR) INTO NEW_prod_status;
          SELECT (NEW.azymut_status * NEW.azymut_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.azymut_wholesale_price INTO MIN_wholesale_price;
          SELECT NEW.azymut_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.azymut_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.azymut_vat INTO NEW_prod_vat;
          SELECT NEW.azymut_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.azymut_shipment_date INTO NEW_prod_shipment_date;
          SELECT '2' INTO NEW_prod_source;
          SELECT 'azymut' INTO komunikat;
      END IF;


      IF (NEW.helion_status > 0 AND NEW.helion_price_brutto > 0.00 AND NEW.helion_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.helion_status * NEW.helion_wholesale_price)))) THEN
          SELECT CAST(NEW.helion_status AS CHAR) INTO NEW_prod_status;
          SELECT NEW.helion_wholesale_price INTO MIN_wholesale_price;
          SELECT (NEW.helion_status * NEW.helion_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.helion_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.helion_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.helion_vat INTO NEW_prod_vat;
          SELECT NEW.helion_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.helion_shipment_date INTO NEW_prod_shipment_date;
          SELECT '4' INTO NEW_prod_source;
          SELECT 'helion' INTO komunikat;
      END IF;

      IF (NEW.dictum_status > 0 AND NEW.dictum_price_brutto > 0.00 AND NEW.dictum_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.dictum_status * NEW.dictum_wholesale_price)))) THEN
          SELECT CAST(NEW.dictum_status AS CHAR) INTO NEW_prod_status;
          SELECT NEW.dictum_wholesale_price INTO MIN_wholesale_price;
          SELECT (NEW.dictum_status * NEW.dictum_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.dictum_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.dictum_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.dictum_vat INTO NEW_prod_vat;
          SELECT NEW.dictum_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.dictum_shipment_date INTO NEW_prod_shipment_date;
          SELECT '7' INTO NEW_prod_source;
          SELECT 'dictum' INTO komunikat;
      END IF;

      IF (NEW.olesiejuk_status > 0 AND NEW.olesiejuk_price_brutto > 0.00 AND NEW.olesiejuk_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.olesiejuk_status * NEW.olesiejuk_wholesale_price)))) THEN
          SELECT CAST(NEW.olesiejuk_status AS CHAR) INTO NEW_prod_status;
          SELECT NEW.olesiejuk_wholesale_price INTO MIN_wholesale_price;
          SELECT (NEW.olesiejuk_status * NEW.olesiejuk_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.olesiejuk_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.olesiejuk_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.olesiejuk_vat INTO NEW_prod_vat;
          SELECT NEW.olesiejuk_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.olesiejuk_shipment_date INTO NEW_prod_shipment_date;
          SELECT '9' INTO NEW_prod_source;
          SELECT 'olesiejuk' INTO komunikat;
      END IF;



      IF (NEW.siodemka_status > 0 AND NEW.siodemka_price_brutto > 0.00 AND NEW.siodemka_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.siodemka_status * NEW.siodemka_wholesale_price)))) THEN
          SELECT CAST(NEW.siodemka_status AS CHAR) INTO NEW_prod_status;
          SELECT NEW.siodemka_wholesale_price INTO MIN_wholesale_price;
          SELECT (NEW.siodemka_status * NEW.siodemka_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.siodemka_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.siodemka_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.siodemka_vat INTO NEW_prod_vat;
          SELECT NEW.siodemka_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.siodemka_shipment_date INTO NEW_prod_shipment_date;
          SELECT '8' INTO NEW_prod_source;
          SELECT 'siodemka' INTO komunikat;
      END IF;

      IF (NEW.ateneum_status > 0 AND NEW.ateneum_price_brutto > 0.00 AND NEW.ateneum_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.ateneum_status * NEW.ateneum_wholesale_price)))) THEN
          SELECT CAST(NEW.ateneum_status AS CHAR) INTO NEW_prod_status;
          SELECT (NEW.ateneum_status * NEW.ateneum_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.ateneum_wholesale_price INTO MIN_wholesale_price;
          SELECT NEW.ateneum_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.ateneum_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.ateneum_vat INTO NEW_prod_vat;
          SELECT NEW.ateneum_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.ateneum_shipment_date INTO NEW_prod_shipment_date;
          SELECT '5' INTO NEW_prod_source;
          SELECT 'ateneum' INTO komunikat;
      END IF;

      IF (NEW.platon_status > 0 AND NEW.platon_price_brutto > 0.00 AND NEW.platon_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.platon_status * NEW.platon_wholesale_price)))) THEN
          SELECT CAST(NEW.platon_status AS CHAR) INTO NEW_prod_status;
          SELECT (NEW.platon_status * NEW.platon_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.platon_wholesale_price INTO MIN_wholesale_price;
          SELECT NEW.platon_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.platon_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.platon_vat INTO NEW_prod_vat;
          SELECT NEW.platon_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.platon_shipment_date INTO NEW_prod_shipment_date;
          SELECT '11' INTO NEW_prod_source;
          SELECT 'platon' INTO komunikat;
      END IF;

      IF (NEW.azymut_status > 0
		  AND att_price_netto IS NOT NULL
          AND att_price_netto > 0.00
          AND NEW.azymut_price_brutto IS NOT NULL
          AND NEW.azymut_price_brutto > 0.00) THEN

		SELECT CAST(NEW.azymut_status AS CHAR) INTO NEW_prod_status;
        SELECT (NEW.azymut_status * NEW.azymut_wholesale_price) INTO CMP_wholesale_price;
        SELECT NEW.azymut_wholesale_price INTO MIN_wholesale_price;
        SELECT NEW.azymut_price_brutto INTO NEW_prod_price_brutto;
        SELECT NEW.azymut_price_netto INTO NEW_prod_price_netto;
        SELECT NEW.azymut_vat INTO NEW_prod_vat;
		SELECT NEW.azymut_shipment_time INTO NEW_prod_shipment_time;
		SELECT NEW.azymut_shipment_date INTO NEW_prod_shipment_date;
        SELECT '2' INTO NEW_prod_source;
        SELECT 'azymut' INTO komunikat;
      END IF;
   END IF;

  IF (curr_ommit_auto_preview = '0' AND NEW_prod_source <> '' AND NEW_prod_price_brutto > 0.00) THEN
      UPDATE products
          SET
              price_brutto = NEW_prod_price_brutto,
              price_netto = NEW_prod_price_netto,
              vat = NEW_prod_vat,
              wholesale_price = MIN_wholesale_price,
              source = NEW_prod_source,
              shipment_time = NEW_prod_shipment_time,
              shipment_date = NEW_prod_shipment_date,
              last_import = NOW(),
              prod_status = NEW_prod_status,
              location = NEW_prod_location
      WHERE id = NEW.id;
  ELSE
      IF(curr_prod_status = '3') THEN
          SELECT '3' INTO NEW_prod_status;
      ELSE
          SELECT '0' INTO NEW_prod_status;
      END IF;

      UPDATE products
      SET last_import = NOw(),
          prod_status = NEW_prod_status
      WHERE id = NEW.id;
  END IF;

 IF(curr_published = '1') THEN
     UPDATE products_shadow A
     JOIN products B
     ON B.id=A.id
     SET A.price_brutto = B.price_brutto,
         A.prod_status = B.prod_status,
         A.shipment_time = B.shipment_time,
                   A.shipment_date = B.shipment_date,
         A.price = (SELECT price_brutto
                                 FROM products_tarrifs
                                 WHERE product_id = A.id AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
                                 ORDER BY `type` DESC, discount DESC LIMIT 1)
     WHERE A.id = NEW.id;
 END IF;

END IF;


END;
















BEGIN

DECLARE new_general_discount DECIMAL(8,2);
DECLARE new_mark_up DECIMAL(8,2);
DECLARE publisher_discount_limt DECIMAL(8,2);
DECLARE series_discount_limt DECIMAL(8,2);
DECLARE source_discount_limt DECIMAL(8,2);
DECLARE id_pakietu INT(10) unsigned;
DECLARE att_price_netto DECIMAL(8,2);
DECLARE old_products_tarrifs_general_discount DECIMAL(8,2);
DECLARE minimal_sell_price DECIMAL(8,2);
DECLARE ceneo_new_sell_price DECIMAL(8,2);
DECLARE ceneo_discount DECIMAL(8,2);
DECLARE cyclic_buffor_product_id INT DEFAULT NULL;
DECLARE ceneo_tab INT UNSIGNED DEFAULT 0;
DECLARE temp_prod_status CHAR(1) DEFAULT NULL;
DECLARE temp_shipment_time CHAR(1) DEFAULT NULL;
DECLARE temp_is_ceneo_tabs INT DEFAULT 0;
DECLARE has_ceneo_tarrif DECIMAL (8,2) DEFAULT NULL;
DECLARE publisher_ceneo INT DEFAULT 0;
DECLARE changed_prod_status INT;
DECLARE min_price DECIMAL(8,2);
DECLARE min_over DECIMAL(8,2);
DECLARE shop_main_category INT(10);

DECLARE done INT DEFAULT 0;

DECLARE pakiety CURSOR FOR    SELECT DISTINCT packet_id  FROM products_packets_items WHERE product_id=NEW.id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

SELECT price_netto INTO att_price_netto FROM products_attachments WHERE product_id = NEW.id;
SELECT discount, mark_up INTO new_general_discount, new_mark_up FROM products_general_discount WHERE source = NEW.source LIMIT 0,1;

IF ((att_price_netto IS NULL OR att_price_netto = 0) AND new_mark_up > 0 AND NEW.wholesale_price > 0) THEN

    SELECT (100 - ((((NEW.wholesale_price * (new_mark_up / 100)) + NEW.wholesale_price) / NEW.price_brutto) * 100)) INTO new_general_discount;
END IF;


IF (NEW.source <> OLD.source) OR (NEW.publisher_id <> OLD.publisher_id) OR (NEW.price_brutto <> OLD.price_brutto) OR (NEW.wholesale_price <> OLD.wholesale_price) THEN
  IF (NEW.packet='0') THEN
      SELECT IFNULL(discount_limit,0) INTO publisher_discount_limt FROM products_publishers WHERE id = NEW.publisher_id;
          SELECT IFNULL(PS.discount_limit,0) INTO series_discount_limt FROM products_series AS PS
          JOIN products_to_series AS PTS ON
                 PS.id = PTS.series_id
          WHERE PS.publisher_id = NEW.publisher_id AND PTS.product_id = NEW.id
          LIMIT 1;
      SELECT IFNULL(discount_limit,0) INTO source_discount_limt FROM products_source_discount WHERE first_source = NEW.created_by;

      IF (publisher_discount_limt > 0) AND (new_general_discount > publisher_discount_limt) THEN
          SELECT publisher_discount_limt INTO new_general_discount;
      END IF;

      IF (source_discount_limt > 0) AND (new_general_discount > source_discount_limt) THEN
          SELECT source_discount_limt INTO new_general_discount;
      END IF;

      IF (series_discount_limt > 0) AND (new_general_discount > series_discount_limt) THEN
          SELECT series_discount_limt INTO new_general_discount;
      END IF;

      SELECT discount INTO old_products_tarrifs_general_discount FROM products_tarrifs WHERE product_id = NEW.id AND general_discount = '1' LIMIT 0,1;
      IF (old_products_tarrifs_general_discount <> new_general_discount) OR (NEW.price_brutto <> OLD.price_brutto) THEN
        UPDATE products_tarrifs
        SET discount = new_general_discount,
         discount_value = ROUND(NEW.price_brutto*(discount/100), 2),
         price_brutto = NEW.price_brutto-discount_value,
         price_netto = ROUND(price_brutto / (1 + NEW.vat/100), 2)
        WHERE product_id = NEW.id AND general_discount = '1';
      END IF;
  END IF;
END IF;

IF NEW.price_brutto <> OLD.price_brutto THEN
  UPDATE products_tarrifs
  SET discount_value = ROUND(NEW.price_brutto*(discount/100), 2),
   price_brutto = NEW.price_brutto-discount_value,
   price_netto = ROUND(price_brutto / (1 + NEW.vat/100), 2)
  WHERE product_id = NEW.id;
END IF;



IF NEW.prod_status <> OLD.prod_status OR NEW.published <> OLD.published THEN
  IF NEW.prod_status = '0' OR NEW.prod_status = '2' OR NEW.published = '0' THEN
     UPDATE products_packets_items SET upd_packet_navailable = '0' WHERE NEW.id=product_id;
  END IF;
END IF;



IF NEW.prod_status <> OLD.prod_status OR NEW.published <> OLD.published THEN
  IF NEW.prod_status = '1' AND NEW.published = '1' THEN
     UPDATE products_packets_items SET upd_packet_navailable = '2' WHERE NEW.id=product_id;
  END IF;
END IF;



IF (NEW.published = '1' AND NEW.published <> OLD.published AND NEW.publication_year = YEAR(NOW())) THEN
 IF ((SELECT id FROM products_news WHERE product_id = NEW.id) > 0 ) THEN
     UPDATE products_news SET news_from = NOW() WHERE product_id = NEW.id;
 ELSE
     INSERT INTO products_news_become SET product_id = NEW.id;
 END IF;
END IF;


IF (
   NEW.prod_status <> OLD.prod_status OR
   NEW.shipment_time <> OLD.shipment_time OR
   NEW.publication_year <> OLD.publication_year OR
   NEW.is_news <> OLD.is_news OR
   NEW.is_previews <> OLD.is_previews)
THEN
   UPDATE products_shadow
   SET
   order_by =
       CONCAT(
         IF( NEW.prod_status = '1' OR NEW.prod_status = '3',
           IF( NEW.shipment_time = '0' OR NEW.shipment_time = '1',
             '2',
             IF (NEW.shipment_time = '2' OR NEW.shipment_time = '3',
               '1',
               '0'
             )
           )
         ,'0'
         )
         ,
         IF (NEW.is_news = '1' OR NEW.is_previews = '1',
           '1',
           '0'
         )
         ,
         LPAD(IFNULL(NEW.publication_year, 0), 4, '0'),
         LPAD(NEW.id, 9, '0')
       )
   WHERE id = NEW.id;
END IF;


IF (
  NEW.`name` <> OLD.`name` OR
  NEW.`isbn_plain` <> OLD.`isbn_plain` OR
  NEW.`isbn_10` <> OLD.`isbn_10` OR
  NEW.`isbn_13` <> OLD.`isbn_13` OR
  NEW.`ean_13` <> OLD.`ean_13` OR
  NEW.`publisher_id` <> OLD.`publisher_id` OR
  NEW.`publication_year` <> OLD.`publication_year` OR
  NEW.`edition` <> OLD.`edition` OR
  NEW.`legal_status_date` <> OLD.`legal_status_date` OR
  NEW.`city` <> OLD.`city` OR
  NEW.`pages` <> OLD.`pages` OR
  NEW.`binding` <> OLD.`binding` OR
  NEW.`dimension` <> OLD.`dimension` OR
  NEW.`language` <> OLD.`language` OR
  NEW.`translator` <> OLD.`translator` OR
  NEW.`original_language` <> OLD.`original_language` OR
  NEW.`original_title` <> OLD.`original_title` OR
  NEW.`weight` <> OLD.`weight` OR
  NEW.`volumes` <> OLD.`volumes` OR
  NEW.`volume_nr` <> OLD.`volume_nr` OR
  NEW.`volume_name` <> OLD.`volume_name` OR
  NEW.`id` <> OLD.`id`
) THEN
  UPDATE products_last_update SET last_update = NOW() WHERE product_id = NEW.id;
END IF;

SELECT product_id INTO ceneo_tab FROM main_profit24.ceneo_tabs_items WHERE product_id = NEW.id AND (`type`= 0 OR `type`= 2);
SELECT include_publishers_products INTO publisher_ceneo FROM main_profit24.products_publishers WHERE id = NEW.publisher_id;

SET temp_shipment_time = NEW.shipment_time;
SET temp_prod_status = NEW.prod_status;

IF (0 != ceneo_tab || 0 != publisher_ceneo) THEN

	SET temp_is_ceneo_tabs = 1;

	IF (temp_prod_status = '3') THEN
		SET temp_prod_status = '1';
	END IF;

	IF (temp_shipment_time = '1') THEN
		SET temp_shipment_time = '0';
	END IF;

END IF;

SELECT price_brutto INTO has_ceneo_tarrif FROM products_tarrifs WHERE product_id = NEW.id AND `type`= 1 LIMIT 1;


IF (has_ceneo_tarrif IS NOT NULL AND NEW.wholesale_price > 0) THEN

	SELECT minimal_overhead INTO min_over FROM main_profit24.website_ceneo_settings WHERE website_id = 1 LIMIT 1;
	SELECT WCC.website_id INTO shop_main_category FROM main_profit24.products AS P JOIN main_profit24.website_ceneo_categories AS WCC ON WCC.category_id = P.main_category_id where P.id = NEW.id;

	SET min_price = ROUND( NEW.wholesale_price + ((NEW.wholesale_price * min_over) / 100), 2 );

        IF(has_ceneo_tarrif < NEW.wholesale_price AND NEW.wholesale_price > 0) THEN

   	     DELETE FROM products_tarrifs WHERE product_id = NEW.id AND type = 1;
        END IF;


	IF(has_ceneo_tarrif < min_price AND shop_main_category = 1 AND NEW.wholesale_price != NEW.price_brutto) THEN



		DELETE FROM products_tarrifs WHERE product_id = NEW.id AND type = 1;


		DELETE PT FROM main_nieprzeczytane.products_tarrifs AS PT JOIN main_nieprzeczytane.products AS P ON P.id = PT.product_id WHERE P.profit24_id = NEW.id AND PT.type = 1;
		DELETE PT FROM main_mestro.products_tarrifs AS PT JOIN main_mestro.products AS P ON P.id = PT.product_id WHERE P.profit24_id = NEW.id AND PT.type = 1;


		DELETE FROM main_profit24.tarrifs_buffor WHERE shop_product_id = NEW.id AND website_id = 1;

	END IF;

END IF;

IF(OLD.name != NEW.name OR OLD.publication_year != NEW.publication_year OR OLD.publisher_id != NEW.publisher_id OR OLD.isbn != NEW.isbn OR OLD.isbn_10 != NEW.isbn_10 OR OLD.isbn_13 != NEW.isbn_13 OR OLD.ean_13 != NEW.ean_13 OR OLD.prod_status != NEW.prod_status OR OLD.published != NEW.published OR OLD.is_news != NEW.is_news OR OLD.is_bestseller != NEW.is_bestseller OR OLD.is_previews != NEW.is_previews OR OLD.wholesale_price != NEW.wholesale_price OR OLD.price_brutto != NEW.price_brutto) THEN

INSERT INTO product_changes_data (product_id, has_changed, elastic_has_changed) VALUES (NEW.id, 1, 0) ON DUPLICATE KEY UPDATE product_id = NEW.id, has_changed = 1, elastic_has_changed = 0, updated = NOW();

END IF;
END




