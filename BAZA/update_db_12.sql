-- chwile potrwa aktualizacja products_stock:
ALTER TABLE  `products_stock` 
ADD  `profit_e_act_stock` SMALLINT NULL DEFAULT  '0' AFTER  `profit_e_status` ,
ADD  `profit_e_reservations` SMALLINT NULL DEFAULT  '0' AFTER  `profit_e_act_stock`,
ADD  `profit_j_act_stock` SMALLINT NULL DEFAULT  '0' AFTER  `profit_j_status` ,
ADD  `profit_j_reservations` SMALLINT NULL DEFAULT  '0' AFTER  `profit_j_act_stock`;

-- uwaga zmieniamy triggery związane z rezerwacją, rezerwacja teraz odbywa się na plikach PHP

ALTER TABLE products_stock_reservations ADD
  `source_id` INT(11) UNSIGNED NOT NULL
AFTER `id`;
ALTER TABLE  `products_stock_reservations` ADD INDEX (  `source_id` );



-- TU odpalić skrypt "import/fillExternalProvidersReservations", który uzupełni provider_id i założyć relację
ALTER TABLE  `products_stock_reservations` ADD FOREIGN KEY (  `source_id` ) REFERENCES  `sources` (
`id`
) ON DELETE RESTRICT ON UPDATE CASCADE ;


-- dodać też column_prefix_products_stock

ALTER TABLE sources ADD
`provider_id` INT(11) UNSIGNED NOT NULL AFTER `id`,
`order_by` INT(11) UNSIGNED NOT NULL AFTER `column_prefix_products_stock`,
`select_privileged` CHAR(1) NOT NULL DEFAULT '0' AFTER `order_by`;
UPDATE `sources` SET `select_privileged` = '1' WHERE `sources`.`id` = 10;


ALTER TABLE  `sources` ADD INDEX (  `provider_id` );

UPDATE `sources` SET `id` = 0,`provider_id` = 0,`symbol` = 'Profit J',`column_prefix_products_stock` = 'profit_j',`order_by` = 2 WHERE `sources`.`id` = 0;
UPDATE `sources` SET `id` = 1,`provider_id` = 1,`symbol` = 'Profit E',`column_prefix_products_stock` = 'profit_e',`order_by` = 1 WHERE `sources`.`id` = 1;
UPDATE `sources` SET `id` = 2,`provider_id` = 7,`symbol` = 'azymut',`column_prefix_products_stock` = 'azymut',`order_by` = 4 WHERE `sources`.`id` = 2;
UPDATE `sources` SET `id` = 3,`provider_id` = 8,`symbol` = 'abe',`column_prefix_products_stock` = 'abe',`order_by` = 10 WHERE `sources`.`id` = 3;
UPDATE `sources` SET `id` = 4,`provider_id` = 6,`symbol` = 'helion',`column_prefix_products_stock` = 'helion',`order_by` = 9 WHERE `sources`.`id` = 4;
UPDATE `sources` SET `id` = 5,`provider_id` = 31,`symbol` = 'ateneum',`column_prefix_products_stock` = 'ateneum',`order_by` = 6 WHERE `sources`.`id` = 5;
UPDATE `sources` SET `id` = 7,`provider_id` = 20,`symbol` = 'dictum',`column_prefix_products_stock` = 'dictum',`order_by` = 7 WHERE `sources`.`id` = 7;
UPDATE `sources` SET `id` = 8,`provider_id` = 26,`symbol` = 'siodemka',`column_prefix_products_stock` = 'siodemka',`order_by` = 5 WHERE `sources`.`id` = 8;
UPDATE `sources` SET `id` = 9,`provider_id` = 27,`symbol` = 'olesiejuk',`column_prefix_products_stock` = 'olesiejuk',`order_by` = 8 WHERE `sources`.`id` = 9;
UPDATE `sources` SET `id` = 10,`provider_id` = 51,`symbol` = 'Stock',`column_prefix_products_stock` = 'profit_g',`order_by` = 3 WHERE `sources`.`id` = 10;



ALTER TABLE `orders_payments_list` ENGINE = InnoDB;
ALTER TABLE `orders_payments_list` CHANGE `order_id` `order_id` INT(11) UNSIGNED NOT NULL;

-- usunięcie rekordów które nie są związane z zamówieniami
DELETE FROM orders_payments_list
WHERE id IN (
  SELECT id FROM (
    SELECT OPL.id
    FROM orders_payments_list AS OPL
    LEFT JOIN orders AS O
    ON O.id = OPL.order_id
    WHERE O.id IS NULL
  ) AS TMP
);


ALTER TABLE `orders_payments_list` ADD  FOREIGN KEY (`order_id`) REFERENCES `orders`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `orders_payments_list` ADD `statement_id` INT UNSIGNED NULL DEFAULT NULL AFTER `order_id`, ADD INDEX (`statement_id`) ;
ALTER TABLE `orders_payments_list` CHANGE `statement_id` `statement_id` INT(10) NULL DEFAULT NULL;
ALTER TABLE `orders_payments_list` ADD FOREIGN KEY (`statement_id`) REFERENCES `orders_bank_statements`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;



CREATE TABLE IF NOT EXISTS `orders_payment_returns` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `balance` DECIMAL(8,2) NULL,
  `created` DATETIME NULL,
  `created_by` VARCHAR(16) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_payment_returns_orders1_idx` (`order_id` ASC),
  CONSTRAINT `fk_orders_payment_returns_orders1`
    FOREIGN KEY (`order_id`)
    REFERENCES `orders` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `bank_accounts` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `orders_payment_types_id` INT(10) UNSIGNED NULL,
  `websites_id` INT(10) UNSIGNED NOT NULL,
  `streamsoft_id` VARCHAR(12) NOT NULL,
  `number` VARCHAR(26) NOT NULL,
  `ident_statments_file` VARCHAR(26) NOT NULL,
  `postal_fee` CHAR(1) NOT NULL DEFAULT 0 COMMENT '1 - pobranie, wtedy zmieniamy oznaczenie postal_fee w statments dla danego przelewu z danego konta',
  `balance` DECIMAL(8,2) NOT NULL DEFAULT 0.00,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(16) NOT NULL,
  `modified` DATETIME NULL,
  `modified_by` VARCHAR(16) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_bank_accounts_orders_payment_types1_idx` (`orders_payment_types_id` ASC),
  INDEX `fk_bank_accounts_websites1_idx` (`websites_id` ASC),
  UNIQUE INDEX `UQ` (`orders_payment_types_id` ASC, `websites_id` ASC),
  CONSTRAINT `fk_bank_accounts_orders_payment_types1`
    FOREIGN KEY (`orders_payment_types_id`)
    REFERENCES `orders_payment_types` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_bank_accounts_websites1`
    FOREIGN KEY (`websites_id`)
    REFERENCES `websites` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;

ALTER TABLE `orders_bank_statements` ADD `ident_statments_file` VARCHAR(26) NULL DEFAULT NULL AFTER `id`, ADD INDEX (`ident_statments_file`) ;

ALTER TABLE `orders_bank_statements` ADD `approved` CHAR(1) NOT NULL DEFAULT '0' AFTER `finished`;

ALTER TABLE `orders_payments_list` ADD `erp_export` DATETIME NULL DEFAULT NULL ;
ALTER TABLE `orders_bank_statements` ADD `erp_export` DATETIME NULL DEFAULT NULL ;