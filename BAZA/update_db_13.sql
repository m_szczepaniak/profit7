-- na wszystkich serwisach
--ALTER TABLE `products_images` ADD `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;


ALTER TABLE `orders` ADD `erp_export` DATETIME NULL DEFAULT NULL;
ALTER TABLE `orders_payment_types` ADD `own_cost` DECIMAL(8,2) NOT NULL DEFAULT '0.00' ;
ALTER TABLE `orders_users_addresses` ADD `streamsoft_id` INT NULL DEFAULT NULL ;




ALTER TABLE 
`orders_send_history_items` 
ADD `price_netto` DECIMAL(8,2) NULL DEFAULT NULL , 
ADD `discount` DECIMAL(4,2) NULL DEFAULT NULL , 
ADD `vat` INT NULL DEFAULT NULL ;


ALTER TABLE `external_providers` ADD `streamsoft_id` INT NULL DEFAULT NULL AFTER `mapping_order_by`;

ALTER TABLE `orders_send_history` ADD `in_document_type` CHAR(1) NOT NULL DEFAULT '0' ;

ALTER TABLE `orders_transport_means` ADD `streamsoft_name` VARCHAR(128) NOT NULL AFTER `point_of_giving`;




-- oznaczenie adresow id startch FV14 dni
UPDATE orders_users_addresses AS OUA, orders AS O
SET streamsoft_id = -1
WHERE 
O.order_status = "4" AND
O.id = OUA.order_id;

-- stworzenie rezerwacji, nowe triggery





--- TMP dodanie nowej kolumny zmiany
--- zmiana triggera
AND ommit_auto_preview = '0'



SELECT streamsoft_indeks, ean_13 AS tt
FROM `products` 
WHERE ean_13 IS NOT NULL
    AND prod_status = "1"
    AND published = "1"
    AND packet = "0"
    AND shipment_time = "0"
union 
SELECT streamsoft_indeks, isbn_13 AS tt
FROM `products` 
WHERE isbn_13 IS NOT NULL
    AND prod_status = "1"
    AND published = "1"
    AND packet = "0"
    AND shipment_time = "0"
union 
SELECT streamsoft_indeks, isbn_plain AS tt
FROM `products` 
WHERE isbn_plain IS NOT NULL
     AND prod_status = "1"
     AND published = "1"
     AND packet = "0"
     AND shipment_time = "0"
union 
SELECT streamsoft_indeks, isbn_10 AS tt
FROM `products` 
WHERE isbn_10 IS NOT NULL
     AND prod_status = "1"
     AND published = "1"
     AND packet = "0"
     AND shipment_time = "0"
union
SELECT streamsoft_indeks, isbn_plain AS tt
FROM products WHERE isbn_plain IN (
  SELECT indeks
  FROM `tmp` 
)
union 
SELECT streamsoft_indeks, ean_13 AS tt
FROM products WHERE ean_13 IN (
  SELECT indeks
  FROM `tmp` 
)
union 
SELECT streamsoft_indeks, isbn_13 AS tt
FROM products WHERE isbn_13 IN (
  SELECT indeks
  FROM `tmp` 
)
union 
SELECT streamsoft_indeks, isbn_10 AS tt
FROM products WHERE isbn_10 IN (
  SELECT indeks
  FROM `tmp` 
)
GROUP BY tt



SELECT order_number, order_date, transport, (to_pay - paid_amount) as diff
FROM orders
WHERE (
  (
    payment_type =  'postal_fee'
    AND payment_status =  '0'
  )
  OR (
    second_payment_type =  'postal_fee'
    AND second_payment_status =  '0'
  )
)
AND correction <>  '1'
AND order_status =  '4'
AND to_pay <> paid_amount




SELECT W.name, O.payment_date, O.order_number, O.paid_amount
FROM orders AS O
JOIN websites AS W
ON W.id = O.website_id
WHERE 
order_status <> '4' 
AND order_status <> '5'
AND payment_type = 'bank_transfer'
AND payment_status = '1';




DELETE FROM products_stock_reservations WHERE 
orders_items_id IN (
  SELECT orders_items_id FROM (
    SELECT orders_items_id
    FROM `products_stock_reservations` 
    GROUP BY `orders_items_id`
    HAVING COUNT(`orders_items_id`) >1
  ) AS TMP
)
AND source_id = "0";