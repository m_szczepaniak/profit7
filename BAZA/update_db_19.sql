
ALTER TABLE `orders_transport_buffer` ADD `deliver_downloaded` CHAR(3) NOT NULL AFTER `transport_id`;

ALTER TABLE `login_history` ADD `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ;

INSERT INTO `modules` (`id`, `symbol`, `menu`, `box`, `default_module`, `cacheable`, `exclude_from_cache`, `one_per_lang`) VALUES
(46, 'm_zamowienia_magazyn_przyjecia', '0', '0', '0', '0', NULL, '0'),
(45, 'm_zamowienia_magazyn_pakowanie', '1', '0', '0', '0', NULL, '0');


ALTER TABLE `orders` ADD `order_on_list_locked` CHAR(1) NOT NULL DEFAULT '0' AFTER `magazine_get_ready_list_VIP`;

ALTER TABLE `orders` 
ADD COLUMN `linked_order` CHAR(1) NOT NULL DEFAULT '0' AFTER `order_on_list_locked`;



ALTER TABLE `orders_items_lists` 
ADD COLUMN `get_from_train` CHAR(1) NOT NULL DEFAULT '1' AFTER `type`;







-- Dodanie kolumny do zamówienia oznaczającej single/zwykłe/połączone

-- Dodanie kolumny do zamówienia oznaczające częściowo z Zasoby i Tramwaj
-- Zasoby to G, Tramwaj to E,J i zewnętrzne źródła

