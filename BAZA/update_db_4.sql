-- wprowadzic zmianę w CRONTAB, zmieniła się nazwa pliku który odpada automat alertów

ALTER TABLE  `orders_items_lists` ADD INDEX (  `document_number` );

ALTER TABLE  `orders_seller_data` ADD  `fedex_id` VARCHAR( 10 ) NULL DEFAULT NULL AFTER  `bank_account`;
ALTER TABLE  `orders_seller_data` ADD  `fedex_key` VARCHAR( 32 ) NULL DEFAULT NULL AFTER  `fedex_id`;
ALTER TABLE  `orders_seller_data` ADD  `fedex_courier_id` INT( 6 ) NULL DEFAULT NULL AFTER  `fedex_key`;

UPDATE orders_seller_data SET 
fedex_id = '5242665',
fedex_key = 'A68A7E19B771B03BC484436301D62590',
fedex_courier_id = '3100';-- ten numer trzeba tutaj zdobyć jaki mamy teraz w Pudełku jest gdzies zapisany


ALTER TABLE  `orders` ADD  `fedex_packages` TINYINT NULL DEFAULT NULL AFTER  `ceneo_agreement`;

ALTER TABLE  `orders` ADD  `print_fedex` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `print_paczkomaty`;




-- statusy fedex
CREATE TABLE IF NOT EXISTS `orders_fedex` (
  `id` INT(10) UNSIGNED NOT NULL,
  `created` DATETIME NOT NULL,
  `status` CHAR(1) NOT NULL DEFAULT 0,
  `last_update` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_orders_fedex_orders1`
    FOREIGN KEY (`id`)
    REFERENCES `orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `print_queue` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `filetype` CHAR(4) NOT NULL COMMENT 'typ pliku np. pdf',
  `filepath` VARCHAR(1024) NOT NULL COMMENT 'sciezka pliku',
  `dest_printer` VARCHAR(255) NOT NULL COMMENT 'drukarka na której będą drukowane dokumenty',
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

-- single
ALTER TABLE  `orders_items_lists` ADD  `type` CHAR( 1 ) NOT NULL DEFAULT  '0' COMMENT  '''0'' - zwykłe ''1'' - single ''2'' - wysyłka razem' AFTER  `closed`;

CREATE TABLE IF NOT EXISTS `containers` (
  `id` INT(10) UNSIGNED ZEROFILL NOT NULL,
  `locked` CHAR(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;
-- zrobić teraz skrypt z insert_containers.sql

-- to raczej nie będzie potrzebne
ALTER TABLE  `orders_send_history` ADD  `magazine_status` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `status`;

-- ustawiamy wszystkie dostawy jako przyjęte na tramwaj
UPDATE orders_send_history SET magazine_status = '5';

ALTER TABLE  `orders_send_history_items` 
ADD  `in_confirmed_quantity` SMALLINT( 5 ) NOT NULL DEFAULT  '0' AFTER  `item_id`,
ADD  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST; 


ALTER TABLE  `orders_mails_send` DROP FOREIGN KEY  `fk_orders_mails_send_orders1` ,
ADD FOREIGN KEY (  `order_id` ) REFERENCES  `orders` (
`id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE `orders_transport_means` ADD `border_time_deadline` tinyint(4) AFTER `order_by`;

ALTER TABLE  `users` ADD  `manage_get_ready_send` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `continue_session`;
UPDATE  `users` SET  `manage_get_ready_send` =  '1' WHERE  `users`.`id` =17;


CREATE TABLE IF NOT EXISTS `orders_items_lists_items_times` (
  `id` INT UNSIGNED NOT NULL,
  `time` INT UNSIGNED NOT NULL,
  `sc_quantity` SMALLINT UNSIGNED NOT NULL,
  INDEX `fk_orders_items_lists_items_times_orders_items_lists_items1_idx` (`id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_orders_items_lists_items_times_orders_items_lists_items1`
    FOREIGN KEY (`id`)
    REFERENCES `orders_items_lists_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

ALTER TABLE  `orders_items_lists_items_times` CHANGE  `time`  `time` INT( 10 ) UNSIGNED NULL DEFAULT NULL;


UPDATE  `orders_transport_means` SET  `max_weight` =  '20' WHERE  `orders_transport_means`.`id` = 3;


CREATE TABLE IF NOT EXISTS `orders_reasons` (
  `type` CHAR(1) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`type`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `orders_reasons_containers` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `orders_reasons_type` CHAR(1) NOT NULL,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `checked` CHAR(1) NOT NULL DEFAULT '0',
  `checked_by` VARCHAR( 16 ) NULL DEFAULT NULL,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_reasons_containters_orders1_idx` (`order_id` ASC),
  INDEX `fk_orders_reasons_containters_orders_reasons1_idx` (`orders_reasons_type` ASC),
  CONSTRAINT `fk_orders_reasons_containters_orders1`
    FOREIGN KEY (`order_id`)
    REFERENCES `orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_reasons_containters_orders_reasons1`
    FOREIGN KEY (`orders_reasons_type`)
    REFERENCES `orders_reasons` (`type`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

INSERT INTO `orders_reasons` (`type`, `name`) VALUES ('D', 'Duplikaty');
INSERT INTO `orders_reasons` (`type`, `name`) VALUES ('P', 'Przeterminowane');

-- tutaj trzeba wiedzieć jakie są ostateczne godziny graniczne, dla metod transportu, 
--  o tej godzinie pojawią się alerty o przeterminowanych zamówieniach
ALTER TABLE  `orders_transport_means` ADD  `border_time` TINYINT NULL DEFAULT NULL AFTER  `order_by`;

ALTER TABLE  `orders_reasons_containers` ADD UNIQUE (
`orders_reasons_type` ,
`order_id`,
`checked`
);


/*
Uwaga dodać wpis do crontaba o:
\orders\autoChangeStatus_12_19 - co godzinę od 12 do 19
\orders_containers\autoClearContainers.php 1 raz dziennie 

*/ 

ALTER TABLE  `products_stock_reservations` 
ADD  `move_to_erp` CHAR( 1 ) NOT NULL DEFAULT  '0',
MODIFY `quantity` smallint(5) unsigned NOT NULL
;

-- zmieniamy triggera
DROP TRIGGER IF EXISTS `trig_orders_upd`//
CREATE TRIGGER `trig_orders_upd` BEFORE UPDATE ON `orders`
 FOR EACH ROW BEGIN
DECLARE total_value DECIMAL(8,2);
DECLARE new_to_pay DECIMAL(8,2);
DECLARE order_balance DECIMAL(8,2);
IF (NEW.value_brutto <> OLD.value_brutto) OR (NEW.transport_cost <> OLD.transport_cost) OR (NEW.paid_amount <> OLD.paid_amount) THEN
  SELECT (NEW.value_brutto + NEW.transport_cost) INTO total_value;
  SELECT total_value INTO new_to_pay;
  SET NEW.to_pay = total_value;
  IF (NEW.paid_amount > 0) THEN
    SELECT NEW.paid_amount - total_value INTO order_balance;
    SET NEW.balance = order_balance;
  ELSEIF (OLD.paid_amount > 0) THEN
    SET NEW.balance = 0;
  END IF;
ELSEIF ((NEW.order_status <> OLD.order_status AND NEW.order_status = '5') OR (NEW.order_status = '5' AND NEW.paid_amount <> OLD.paid_amount)) THEN
  SET NEW.balance = NEW.paid_amount;
END IF;


	IF (NEW.order_status <> OLD.order_status) THEN
    IF (NEW.order_status = '3') THEN 
      -- jeśli zmienia status w zatwierdzono przenosimy do ERP
      UPDATE products_stock_reservations 
        SET move_to_erp = '1'
      WHERE products_stock_reservations.order_id = NEW.id;
    ELSEIF (NEW.order_status = '5') THEN
      -- jeśli anulujemy, to usuwamy rezerwacje
      DELETE FROM products_stock_reservations WHERE products_stock_reservations.order_id = NEW.id;
    END IF;
	END IF;

END
//

ALTER TABLE  `orders_items_lists` ADD  `transport_id` INT UNSIGNED NULL DEFAULT NULL AFTER  `type`;
ALTER TABLE `products_stock_reservations` DROP INDEX `orders_items_id_UNIQUE`;