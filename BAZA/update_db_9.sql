RENAME TABLE  `main_profit24`.`orders_paczkomaty_confirm` TO  `main_profit24`.`orders_shipment_confirm` ;
ALTER TABLE  `orders_shipment_confirm` COMMENT =  'dokumenty, potwierdzenia nadania przesylek';

-- UWAGA to chwilkę potrwa
ALTER TABLE  `orders` CHANGE  `confirm_printout_paczkomaty`  `confirm_printout_speditor` CHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL DEFAULT  '0';


INSERT INTO `key_value` (`id`, `table_name`, `id_dest`, `key`, `value`) VALUES
(52, 'orders_transport_means', 3, 'class_symbol', 'Paczkomaty');
INSERT INTO `key_value` (`id`, `table_name`, `id_dest`, `key`, `value`) VALUES
(53, 'orders_transport_means', 4, 'tracking_url', 'https://profit24.pl/LIB/communicator/sources/PocztaPolska/sledzenie.wsdl'),
(54, 'orders_transport_means', 4, 'tracking_login', 'sledzeniepp'),
(55, 'orders_transport_means', 4, 'tracking_password', 'PPSA');


-- w shipping_points_orlen dodano kolumnę sequence
ALTER TABLE  `shipping_points_orlen` ADD  `sequence` INT UNSIGNED NOT NULL FIRST;


INSERT INTO `key_value` (`id`, `table_name`, `id_dest`, `key`, `value`) VALUES
(68, 'orders_transport_means', 4, 'password_recipients', 'a.golba@profit24.pl'),
(67, 'orders_transport_means', 4, 'type', 'reception_at_point'),
(66, 'orders_transport_means', 3, 'type', 'reception_at_point'),
(65, 'orders_transport_means', 2, 'type', 'personal_reception'),
(63, 'orders_transport_means', 7, 'type', 'delivery_to_address'),
(62, 'orders_transport_means', 6, 'type', 'reception_at_point'),
(61, 'orders_transport_means', 5, 'type', 'reception_at_point'),
(60, 'orders_transport_means', 1, 'type', 'delivery_to_address');


INSERT INTO `key_value` (`id`, `table_name`, `id_dest`, `key`, `value`) VALUES
(57, 'orders_transport_means', 1, 'class_symbol', 'FedEx');
