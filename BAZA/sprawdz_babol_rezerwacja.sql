SELECT DISTINCT O.order_number
FROM orders AS O
JOIN orders_items AS OI
ON OI.order_id = O.id
    AND OI.deleted = "0"
JOIN products_stock AS PS
    ON PS.id = OI.product_id
    AND PS.profit_g_act_stock > 0
WHERE O.order_status IN (0,1)
AND O.erp_export IS NULL






DELETE FROM products_stock_reservations
WHERE products_stock_reservations.id IN (
  SELECT id FROM (
    SELECT PSR.id
    FROM `products_stock_reservations` AS PSR
    JOIN orders AS O
        ON O.id = PSR.order_id
        AND DATE(O.order_date) >= '2017-12-11'
        AND O.order_status IN ("0","1")

    GROUP BY CONCAT(products_stock_id, '_', orders_items_id)
    HAVING count(PSR.id) > 1
  ) AS TMP
)

DELETE FROM products_stock_reservations
WHERE products_stock_reservations.id IN (
  SELECT id FROM (
    SELECT PSR.id, O.order_date
    FROM  `products_stock_reservations` AS PSR
    JOIN orders_items AS OI 
      ON OI.id =  `orders_items_id` 
      AND OI.source <>  '51'
    JOIN orders AS O
      ON O.id = OI.order_id AND O.order_status IN ("0","1")
    WHERE PSR.source_id = 10
  ) AS TMP
)


    SELECT PSR.*
    FROM  `products_stock_reservations` AS PSR
    JOIN orders AS O ON O.id =  PSR.order_id
    AND O.order_status = '4';

SELECT PPP.* FROM products_stock AS PPP WHERE id IN (
  SELECT id FROM (
    SELECT  PS.id, SUM( PSR.quantity ), PS.profit_j_reservations
    FROM  `products_stock_reservations` AS PSR
    JOIN products_stock AS PS 
      ON PS.id = PSR.products_stock_id
    WHERE PSR.source_id = '0'
    GROUP BY products_stock_id
    HAVING SUM( PSR.quantity ) <> PS.profit_j_reservations
  ) AS TMP
);

UPDATE products_stock 
SET profit_g_reservations = 0
WHERE id IN (
  SELECT id FROM (
    SELECT products_stock.id FROM products_stock AS products_stock
    LEFT JOIN  products_stock_reservations AS PSR
    ON products_stock.id = PSR.products_stock_id
    WHERE PSR.id IS NULL 
    AND products_stock.profit_g_reservations > 0
  ) AS TMP
);

UPDATE products_stock 
SET profit_e_reservations = 0
WHERE id IN (
  SELECT id FROM (
    SELECT products_stock.id FROM products_stock AS products_stock
    LEFT JOIN  products_stock_reservations AS PSR
    ON products_stock.id = PSR.products_stock_id
    WHERE PSR.id IS NULL 
    AND products_stock.profit_e_reservations > 0
  ) AS TMP
);

UPDATE products_stock 
SET profit_j_reservations = 0
WHERE id IN (
  SELECT id FROM (
    SELECT products_stock.* FROM products_stock AS products_stock
    LEFT JOIN  products_stock_reservations AS PSR
    ON products_stock.id = PSR.products_stock_id
    WHERE PSR.id IS NULL 
    AND products_stock.profit_j_reservations > 0
  ) AS TMP
);




///---
DELETE FROM products_stock_reservations 
WHERE id IN (
  SELECT id FROM (
    SELECT  PSR.id, SUM( PSR.quantity ), PS.profit_g_reservations
    FROM  `products_stock_reservations` AS PSR
    JOIN products_stock AS PS 
      ON PS.id = PSR.products_stock_id
    WHERE PSR.source_id = '10'
    GROUP BY products_stock_id
    HAVING SUM( PSR.quantity ) <> PS.profit_g_reservations
  ) AS TMP
);


    SELECT  PSR.id, SUM( PSR.quantity ), PS.profit_j_reservations
    FROM  `products_stock_reservations` AS PSR
    JOIN products_stock AS PS 
      ON PS.id = PSR.products_stock_id
    JOIN orders_items AS OI
      ON OI.id = PSR.order_id AND source <> "0"
    WHERE PSR.source_id = '0'
    GROUP BY products_stock_id
    HAVING SUM( PSR.quantity ) <> PS.profit_j_reservations


    SELECT  PSR.id, SUM( PSR.quantity ), PS.profit_j_reservations
    FROM  `products_stock_reservations` AS PSR
    JOIN products_stock AS PS 
      ON PS.id = PSR.products_stock_id
    JOIN orders_items AS OI
      ON OI.id = PSR.order_id AND source <> "0"
    WHERE PSR.source_id = '0'


SELECT PSR.id, PSR.source_id, PS.profit_j_reservations, PSR . *, O.order_date, OI.source, O.order_number
FROM  `products_stock_reservations` AS PSR
JOIN products_stock AS PS ON PS.id = PSR.products_stock_id
JOIN orders_items AS OI ON OI.id = PSR.orders_items_id AND OI.source <>  "0"
JOIN orders AS O ON O.id = PSR.order_id
WHERE PSR.source_id =  '0'
