CREATE TABLE IF NOT EXISTS `main_profit24`.`users_to_prod_series_publishers` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `products_publishers_id` INT(10) UNSIGNED NOT NULL,
  `users_accounts_id` INT(10) UNSIGNED NOT NULL,
  `products_series_id` INT(10) UNSIGNED NOT NULL,
  `discount` DOUBLE(8,2) NOT NULL DEFAULT 0,
  `created_date` DATE NOT NULL,
  `created_by` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_to_prod_series_publishers_products_publishers1_idx` (`products_publishers_id` ASC),
  INDEX `fk_users_to_prod_series_publishers_users_accounts1_idx` (`users_accounts_id` ASC),
  INDEX `fk_users_to_prod_series_publishers_products_series1_idx` (`products_series_id` ASC),
  CONSTRAINT `fk_users_to_prod_series_publishers_products_publishers1`
    FOREIGN KEY (`products_publishers_id`)
    REFERENCES `main_profit24`.`products_publishers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_to_prod_series_publishers_users_accounts1`
    FOREIGN KEY (`users_accounts_id`)
    REFERENCES `main_profit24`.`users_accounts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_to_prod_series_publishers_products_series1`
    FOREIGN KEY (`products_series_id`)
    REFERENCES `main_profit24`.`products_series` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);



CREATE TABLE IF NOT EXISTS `main_profit24`.`users_to_products_publishers` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `products_publishers_id` INT(10) UNSIGNED NOT NULL,
  `users_accounts_id` INT(10) UNSIGNED NOT NULL,
  `discount` DOUBLE(6,2) NOT NULL DEFAULT 0,
  `created_date` DATE NOT NULL,
  `created_by` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_to_products_publishers_products_publishers_idx` (`products_publishers_id` ASC),
  INDEX `fk_users_to_products_publishers_users_accounts1_idx` (`users_accounts_id` ASC),
  CONSTRAINT `fk_users_to_products_publishers_products_publishers`
    FOREIGN KEY (`products_publishers_id`)
    REFERENCES `main_profit24`.`products_publishers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_to_products_publishers_users_accounts1`
    FOREIGN KEY (`users_accounts_id`)
    REFERENCES `main_profit24`.`users_accounts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);