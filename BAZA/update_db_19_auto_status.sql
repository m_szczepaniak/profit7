
UPDATE orders AS O, orders_linked_transport AS OLT
SET linked_order = "1"
WHERE 
OLT.main_order_id = O.id
AND O.order_status <> '4' 
AND O.order_status <> '5'


-- fix duplicate lists
SELECT OILI.id
FROM orders_items_lists_items AS OILI
JOIN orders_items_lists AS OIL
  ON OILI.orders_items_lists_id = OIL.id
JOIN orders AS O
  ON O.id = OILI.orders_id
WHERE DATE(OIL.created) = '2015-08-07' AND (
  O.order_status = '4' OR O.order_status = '3'
) AND
(

)




SELECT O.order_number, OI.name, GROUP_CONCAT(OIL.package_number SEPARATOR ", "), COUNT(orders_items_id), GROUP_CONCAT(OILI.id SEPARATOR ", "), GROUP_CONCAT(OIL.closed SEPARATOR ", ")
FROM orders_items_lists_items AS OILI
JOIN orders_items_lists AS OIL
  ON OILI.orders_items_lists_id = OIL.id
JOIN orders_items AS OI
  ON OI.id = OILI.orders_items_id
JOIN orders AS O
  ON O.id = OILI.orders_id
WHERE (DATE(OIL.created) = '2015-08-07' OR DATE(OIL.created) = '2015-08-06')  AND OIL.type = '0' AND OIL.closed = '0' AND get_from_train = '1'
GROUP BY orders_items_id
HAVING COUNT(orders_items_id) > 1
ORDER BY O.order_number DESC


-- kolumna oznacza: 
/*
0 - nie przyznany nr listy
1 - Single, Pełna, Tramwaj
2 - Single, Pełna, Zasoby
3 - Pełna, Tramwaj
4 - Pełna, Zasoby
5 - Częściowa, Zasoby i Tramwaj
6 - Łączone, Pełna Tramwaj
7 - Łączone, Pełna Zasoby
8 - Łączone, Częściowa


Tramwaj
OI.status = "1" OR (OI.status = "3" AND (OI.source = "1" OR OI.source = "0"))
Prostsze:
(OI.source <> "51")

-- 1 - Single, Pełna, Tramwaj
SELECT O.id 
FROM orders AS O
JOIN orders_items AS OI 
  ON OI.order_id = O.id AND 
     OI.source <> "51" AND 
     OI.status <> '0' AND 
     OI.status <> '-1' AND 
     OI.deleted = "0" 
WHERE
  (OI.item_type = 'I' OR OI.item_type = 'P') 
  AND O.order_status <> "5" 
  AND O.order_status <> "4"
  AND (
    (
     SELECT sum(quantity) 
     FROM orders_items AS OI_TEST
     WHERE 
        OI_TEST.order_id = O.id
        AND OI_TEST.deleted = "0"
        AND (OI_TEST.item_type = 'I' OR OI_TEST.item_type = 'P')
    ) = 1
  ) 
  AND O.linked_order <> "1"

-- 2 - Single, Pełna, Zasoby
SELECT O.id 
FROM orders AS O
JOIN orders_items AS OI 
  ON OI.order_id = O.id AND 
     OI.source = "51" AND 
     OI.status <> '0' AND 
     OI.status <> '-1' AND 
     OI.deleted = "0"
WHERE
  (OI.item_type = 'I' OR OI.item_type = 'P') 
  AND O.order_status <> "5" 
  AND O.order_status <> "4"
  AND (
    (
     SELECT sum(quantity) 
     FROM orders_items AS OI_TEST
     WHERE 
        OI_TEST.order_id = O.id
        AND OI_TEST.deleted = "0"
        AND (OI_TEST.item_type = 'I' OR OI_TEST.item_type = 'P')
    ) = 1
  )
  AND O.linked_order <> "1"


-- 3 - Pełna, Tramwaj
SELECT O.id 
FROM orders AS O
WHERE
  O.order_status <> "5" AND O.order_status <> "4"
  AND (
    (
     SELECT OI.id
     FROM orders_items AS OI
      ON OI.order_id = O.id AND 
         OI.source <> "51" AND 
         OI.status <> '0' AND 
         OI.status <> '-1' 
     LIMIT 1
    ) IS NULL
  )
  AND O.linked_order <> "1"
  AND O.list_type = "0"; -- nie są to wcześniejsze, np. single

*/