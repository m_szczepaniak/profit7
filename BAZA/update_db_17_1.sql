ALTER TABLE `users_accounts` ADD `seller_streamsoft_id` CHAR(5) NOT NULL AFTER `registred`;
ALTER TABLE `orders` ADD `seller_streamsoft_id` CHAR(5) NOT NULL ;

ALTER TABLE `orders_payment_returns` ADD `submitted` CHAR(1) NOT NULL DEFAULT '1' AFTER `balance`;
ALTER TABLE `orders_payment_returns` ADD `bank_account` CHAR(26) NULL DEFAULT NULL AFTER `submitted`;