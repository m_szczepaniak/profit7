CREATE TABLE `email_attachment` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `path` varchar(255) COLLATE utf8_polish_ci NOT NULL,
 `template` varchar(50) COLLATE utf8_polish_ci NOT NULL,
 `website_id` int(11) unsigned NOT NULL,
 PRIMARY KEY (`id`),
 KEY `FK_email_attachment_websites` (`website_id`),
 CONSTRAINT `FK_email_attachment_websites` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci