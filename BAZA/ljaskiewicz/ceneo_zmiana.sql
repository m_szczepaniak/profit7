ALTER TABLE website_ceneo_categories ADD `np_percentage_modifier` DECIMAL(8,2) NOT NULL DEFAULT '0.00';
ALTER TABLE website_ceneo_categories ADD `profit24_percentage_modifier` DECIMAL(8,2) NOT NULL DEFAULT '0.00';
ALTER TABLE website_ceneo_categories ADD `mestro_percentage_modifier` DECIMAL(8,2) NOT NULL DEFAULT '0.00';

CREATE TABLE `cyclic_buffor` (
	`product_id` INT(11) NOT NULL,
	`priority` CHAR(1) NOT NULL DEFAULT '0',
	`status` CHAR(1) NOT NULL DEFAULT '1',
	`updatet_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`product_id`),
	INDEX `priority` (`priority`)
);

CREATE TABLE `website_ceneo_settings` (
	`website_id` INT(11) NOT NULL,
	`minimal_overhead` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`price_modifier` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`catalogue_price_substractor` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`ignored_shops` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_polish_ci',
	PRIMARY KEY (`website_id`)
)
COLLATE='utf8_polish_ci';