	ALTER TABLE products_stock_reservations ADD `streamsoft_lp` CHAR(3) NULL DEFAULT NULL;
	ALTER TABLE orders ADD `order_number_iteration` TINYINT(4) NULL DEFAULT NULL;
	ALTER TABLE products_stock_supplies_to_reservations ADD `reservated_quantity` INT(11) NULL DEFAULT NULL;
	ALTER TABLE products_stock_supplies_to_reservations ADD `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP

	CREATE TABLE `reservations_debug` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`data` TEXT NOT NULL,
	`created` DATETIME NOT NULL,
	PRIMARY KEY (`id`)
	)
)