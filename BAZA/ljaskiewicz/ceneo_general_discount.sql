	ALTER TABLE products_general_discount ADD `minimal_overhead` DECIMAL(8,2) NOT NULL DEFAULT '0.00';
	ALTER TABLE products_general_discount ADD `catalogue_price_substractor` DECIMAL(8,2) NOT NULL DEFAULT '0.00';

CREATE TABLE `products_general_discount_common` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`source` CHAR(2) NOT NULL COLLATE 'utf8_polish_ci',
	`discount` DECIMAL(5,2) NOT NULL DEFAULT '0.00',
	`minimal_overhead` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`catalogue_price_substractor` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`mark_up` DECIMAL(8,2) NOT NULL DEFAULT '0.00',
	`recount` ENUM('0','1') NOT NULL DEFAULT '0' COLLATE 'utf8_polish_ci',
	`website_id` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_polish_ci'
ENGINE=InnoDB
;
