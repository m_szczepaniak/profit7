ALTER TABLE `profit2`.`orders_items_lists` 
ADD COLUMN `books_serialized` TEXT NULL DEFAULT NULL AFTER `debug`,
ADD COLUMN `books_to_send_serialized` TEXT NULL DEFAULT NULL AFTER `books_serialized`,

ALTER TABLE `profit2`.`orders_items_lists` 
ADD COLUMN `scanned_books_serialized` TEXT NULL DEFAULT NULL AFTER `books_to_send_serialized`;
