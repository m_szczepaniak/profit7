ALTER TABLE `users` 
ADD COLUMN `priv_address_data` CHAR(1) NOT NULL DEFAULT '0' AFTER `active`,
ADD COLUMN `priv_commercial_data` CHAR(1) NOT NULL DEFAULT '0' AFTER `priv_address_data`;
