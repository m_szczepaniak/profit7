CREATE TABLE IF NOT EXISTS `products_stock_reservations_debug` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `reservation_id` INT NOT NULL,
  `data` TEXT NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(48) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;



SELECT PSS.product_id, (reservation - reservation_to_reduce) as reservation, (

SELECT SUM( reservated_quantity ) 
FROM `products_stock_supplies_to_reservations` AS PSSTR
WHERE PSSTR.products_stock_supplies_id = PSS.id
GROUP BY products_stock_supplies_id
)
FROM `products_stock_supplies` AS PSS
WHERE (reservation - reservation_to_reduce) <> ( 
SELECT SUM( reservated_quantity ) 
FROM `products_stock_supplies_to_reservations` AS PSSTR
WHERE PSSTR.products_stock_supplies_id = PSS.id
GROUP BY products_stock_supplies_id ) 
ORDER BY PSS.id DESC



SELECT PSR.order_id, quantity,
(
	SELECT SUM( reservated_quantity )
	FROM `products_stock_supplies_to_reservations` AS PSSTR
	WHERE PSSTR.products_stock_reservations_id = PSR.id
	GROUP BY products_stock_reservations_id
) AS sum
FROM `products_stock_reservations` AS PSR
WHERE quantity <> 
(
	SELECT SUM( reservated_quantity )
	FROM `products_stock_supplies_to_reservations` AS PSSTR
	WHERE PSSTR.products_stock_reservations_id = PSR.id
	GROUP BY products_stock_reservations_id
)
AND move_to_erp = '0'
ORDER BY PSR.id DESC 