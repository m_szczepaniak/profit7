CREATE TABLE `menus_items_books` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `page_id` int(10) unsigned NOT NULL,
 PRIMARY KEY (`id`),
 KEY `menus_items_books` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8