<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 21.08.17
 * Time: 11:02
 */

$aConfig['config']['project_dir'] = 'import/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;

include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
set_time_limit(10800);

global $pDbMgr;

$productId = 1;
$sSql = "
SELECT DISTINCT CUR_P.id
FROM products AS CUR_P
JOIN products_extra_categories AS CUR_PEC
  ON CUR_P.id = CUR_PEC.product_id
WHERE
CUR_P.product_type <>
(
    SELECT MENUS.product_type
        FROM products_extra_categories AS PEC2
        JOIN products AS P ON PEC2.product_id = P.id
        JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
        JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
        WHERE PEC2.product_id = CUR_P.id
    AND MI2.published =  '1'
    AND MI2.mtype =  '0'
    AND MI2.priority >0
    AND MI2.parent_id IS NOT NULL
        ORDER BY MI2.priority ASC
        LIMIT 1
)
AND CUR_P.published = '1'
AND CUR_P.product_type <> 'A'
AND CUR_P.modiefied_by_service <> '1'
AND IF(CUR_P.product_type = 'G' AND CUR_P.azymut_index LIKE '%GR', 1=0, 1=1)
AND CUR_P.product_type = 'K'
LIMIT 500;

";
//$ids = $pDbMgr->GetCol('profit24', $sSql);
//foreach ($ids as $id) {
//    $sSql = "SELECT MENUS.product_type
//        FROM products_extra_categories AS PEC2
//        JOIN products AS P ON PEC2.product_id = P.id
//        JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
//        JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
//        WHERE PEC2.product_id = ".$id."
//    AND MI2.published =  '1'
//    AND MI2.mtype =  '0'
//    AND MI2.priority >0
//    AND MI2.parent_id IS NOT NULL
//        ORDER BY MI2.priority ASC
//        LIMIT 1";
//    $productType = $pDbMgr->GetOne('profit24', $sSql);
//
//    // mamy typ produktu
//    echo 'id:' . $id . ' TYP: ' . $productType . "\r\n";
//
//    if ($productType != '') {
//        $values = [
//            'product_type' => $productType
//        ];
//
//        $sSql = "
//    SELECT MI2.id
//    FROM products_extra_categories AS PEC2
//    JOIN products AS P ON PEC2.product_id = P.id
//    JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
//    JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
//    WHERE PEC2.product_id = " . $id . "
//    AND IF(P.product_type != 'K', MENUS.product_type = P.product_type, (MENUS.product_type = 'K'))
//    AND MI2.published = '1'
//    AND MI2.mtype = '0'
//    AND MI2.priority > 0
//    AND MI2.parent_id IS NOT NULL
//    ORDER BY MI2.priority ASC
//    LIMIT 1;
//    ";
//        $menuId = $pDbMgr->GetOne('profit24', $sSql);
//        if ($menuId > 0) {
//            $values['main_category_id'] = $menuId;
//        }
//        var_dump($values);
//        if (false === $pDbMgr->Update('profit24', 'products', $values, ' id=' . $id.' LIMIT 1')) {
//            echo 'błąd id: '.$id.' - '.print_r($values);
//        } else {
//
//            $sUpdateSQL = '
//               UPDATE products_last_update
//               SET last_update = NOW()
//               WHERE product_id = ' . $id.'
//               LIMIT 1';
//            $pDbMgr->Query('profit24', $sUpdateSQL);
//            echo 'poszło - '.$id."\r\n";
//        }
//    }
//}
//

$sSql = 'SELECT PS.id
         FROM products_shadow AS PS
         JOIN products AS P
          ON P.id = PS.id AND P.product_type NOT IN ("' . implode('", "', $aConfig['common']['allowed_product_type']) . '")';
$ps = Common::GetCol($sSql);

foreach ($ps as $id) {
    $sSql = 'DELETE FROM products_shadow WHERE id = '.$id;
    Common::Query($sSql);
    dump($sSql);
}