SELECT DISTINCT SL.id, SL.container_id, OI.name, OI.isbn, OI.status, OI.source, OI.deleted, O.order_number, SLO.reserved_quantity,
  SLO.reservation_subtracted, SL.reservation, SL.available, PS.profit_g_status, PS.profit_g_act_stock,
  (SELECT SUM(SL2.quantity)
   FROM stock_location AS SL2
   WHERE  SL2.products_stock_id = SL.products_stock_id
  ),
  PS.profit_g_reservations ,
  (SELECT SUM(SL2.reservation)
   FROM stock_location AS SL2
   WHERE  SL2.products_stock_id = SL.products_stock_id
  )
FROM stock_location AS SL
  JOIN stock_location_orders_items_lists_items AS SLO
    ON SL.id = SLO.stock_location_id AND SL.magazine_type = SLO.stock_location_magazine_type
  JOIN orders_items_lists_items AS OILI
    ON SLO.orders_items_lists_items_id = OILI.id
  JOIN orders_items AS OI
    ON OILI.orders_items_id = OI.id
  JOIN orders AS O
    ON O.id = OI.order_id
       AND order_status = "4" AND O.erp_export IS NOT NULL
  JOIN products_stock AS PS
    ON SL.products_stock_id = PS.id
WHERE SL.reservation > 0
      AND (SLO.reservation_subtracted = 0 OR SLO.reservation_subtracted IS NULL)
      AND (OI.source = "51" AND OI.deleted = "0")
      AND DATE(O.status_4_update) > "2018-07-01"
  AND PS.profit_g_reservations = "0"
ORDER BY SL.products_stock_id DESC