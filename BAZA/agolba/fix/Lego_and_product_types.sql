
SELECT *
FROM main_profit24.`products` AS MP
JOIN backup_profit24.products AS BP
  ON MP.id = BP.id
WHERE
MP.description LIKE "% LEGO® %"
AND (DATE(MP.modified) < DATE(NOW()) OR MP.modified IS NULL)
AND DATE(MP.created) < DATE(NOW())
AND MP.description <> BP.description


UPDATE main_profit24.`products` AS MP
JOIN backup_profit24.products AS BP
  ON MP.id = BP.id
SET MP.description = BP.description
WHERE
MP.description LIKE "% LEGO® %"
AND (DATE(MP.modified) < DATE(NOW()) OR MP.modified IS NULL)
AND DATE(MP.created) < DATE(NOW())
AND MP.description <> BP.description


SELECT MP.description
FROM main_profit24.`products` AS MP
JOIN backup_profit24.products AS BP
  ON MP.id = BP.id
WHERE
MP.description LIKE "% LEGO® %"
AND MP.description <> BP.description


SELECT *
FROM main_mestro.products AS MP
JOIN main_profit24.products AS PP
ON MP.profit24_id = PP.id
WHERE MP.product_type <> PP.product_type

UPDATE main_mestro.products AS MP
JOIN main_profit24.products AS PP
ON MP.profit24_id = PP.id
SET MP.product_type = PP.product_type
WHERE MP.product_type <> PP.product_type