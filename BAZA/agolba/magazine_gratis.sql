CREATE TABLE IF NOT EXISTS `magazine_gratis` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(64) NOT NULL,
  `code` VARCHAR(16) NOT NULL,
  `minimum_price` DECIMAL(8,2) NOT NULL,
  `unique_on_email` CHAR(1) NOT NULL DEFAULT 0,
  `circle` CHAR(1) NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(64) NOT NULL,
  `modified` DATETIME NULL,
  `modified_by` VARCHAR(64) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

ALTER TABLE magazine_gratis ADD `code` VARCHAR(16) NOT NULL;


CREATE TABLE IF NOT EXISTS `magazine_gratis_categories` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `magazine_gratis_id` INT UNSIGNED NOT NULL,
  `page_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_magazine_gratis_categories_magazine_gratis1_idx` (`magazine_gratis_id` ASC),
  INDEX `fk_magazine_gratis_categories_menus_items1_idx` (`page_id` ASC),
  CONSTRAINT `fk_magazine_gratis_categories_magazine_gratis1`
    FOREIGN KEY (`magazine_gratis_id`)
    REFERENCES `magazine_gratis` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_magazine_gratis_categories_menus_items1`
    FOREIGN KEY (`page_id`)
    REFERENCES `menus_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `magazine_gratis_publisher` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `magazine_gratis_id` INT UNSIGNED NOT NULL,
  `products_publishers_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_magazine_gratis_publisher_magazine_gratis1_idx` (`magazine_gratis_id` ASC),
  INDEX `fk_magazine_gratis_publisher_products_publishers1_idx` (`products_publishers_id` ASC),
  CONSTRAINT `fk_magazine_gratis_publisher_magazine_gratis1`
    FOREIGN KEY (`magazine_gratis_id`)
    REFERENCES `magazine_gratis` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_magazine_gratis_publisher_products_publishers1`
    FOREIGN KEY (`products_publishers_id`)
    REFERENCES `products_publishers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS `magazine_gratis_website` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `magazine_gratis_id` INT UNSIGNED NOT NULL,
  `website_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_magazine_gratis_website_magazine_gratis1_idx` (`magazine_gratis_id` ASC),
  INDEX `fk_magazine_gratis_website_websites1_idx` (`website_id` ASC),
  CONSTRAINT `fk_magazine_gratis_website_magazine_gratis1`
    FOREIGN KEY (`magazine_gratis_id`)
    REFERENCES `magazine_gratis` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_magazine_gratis_website_websites1`
    FOREIGN KEY (`website_id`)
    REFERENCES `websites` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `magazine_gratis_usages` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `magazine_gratis_id` INT UNSIGNED NOT NULL,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_magazine_gratis_usages_magazine_gratis1_idx` (`magazine_gratis_id` ASC),
  INDEX `fk_magazine_gratis_usages_orders1_idx` (`order_id` ASC),
  UNIQUE INDEX `a` (`magazine_gratis_id` ASC, `order_id` ASC),
  CONSTRAINT `fk_magazine_gratis_usages_magazine_gratis1`
  FOREIGN KEY (`magazine_gratis_id`)
  REFERENCES `magazine_gratis` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_magazine_gratis_usages_orders1`
  FOREIGN KEY (`order_id`)
  REFERENCES `orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB


DELETE FROM products_shadow WHERE id IN ( SELECT id FROM ( SELECT PS.id FROM products_shadow AS PS LEFT JOIN products AS P ON PS.id = P.id WHERE P.id IS NULL ) AS TMP )


DELETE FROM products_shadow WHERE id IN ( SELECT id FROM ( SELECT PS.id FROM products_shadow AS PS LEFT JOIN products AS P ON PS.id = P.id WHERE P.id IS NULL LIMIT 1000 ) AS TMP )