ALTER TABLE `banners_to_pages` ADD INDEX(`level`);
ALTER TABLE `banners` ADD INDEX(`start_date`);
ALTER TABLE `banners` ADD INDEX(`end_date`);
ALTER TABLE `banners` ADD INDEX(`max_views`);
ALTER TABLE `banners` ADD INDEX(`current_views`);


ALTER TABLE `languages` ADD INDEX(`published`);
ALTER TABLE `areas` ADD INDEX(`number`);
ALTER TABLE `areas` ADD INDEX(`subpage_area`);
ALTER TABLE `boxes` ADD INDEX(`moption`);
ALTER TABLE `products_tarrifs` ADD INDEX(`start_date`);
ALTER TABLE `products_tarrifs` ADD INDEX(`end_date`);




ALTER TABLE orders_transport_means ADD INDEX( personal_reception );
ALTER TABLE orders_payment_types ADD INDEX( published );
ALTER TABLE orders_payment_types ADD INDEX( cost );
ALTER TABLE orders_transport_means ADD INDEX( date_no_costs_from );
ALTER TABLE orders_transport_means ADD INDEX( date_start_date );
ALTER TABLE orders_transport_means ADD INDEX( date_end_date );
ALTER TABLE products_free_delivery ADD INDEX( start_date );
ALTER TABLE products_free_delivery ADD INDEX( end_date );
ALTER TABLE products_shadow ADD INDEX( prod_status );

w products_free_delivery_transport_method są podwójne indeksy


ALTER TABLE menus_items ADD INDEX( priority );
ALTER TABLE products_tarrifs ADD INDEX( general_discount );
ALTER TABLE products_tarrifs ADD INDEX( type );;

ceneo_tabs_items.type + (2 takie same indeksy)
products_last_update (2 takie same indeksy)
products_tarrifs.general_discount

main_mestro.products_tarrifs.type
main_nieprzeczytane.products_tarrifs.type



ALTER TABLE `orders_transport_means` ADD INDEX( `symbol`);
ALTER TABLE menus  ADD INDEX( products );
ALTER TABLE products_publishers ADD INDEX( name );
ALTER TABLE products_recommended ADD INDEX( order_by );
ALTER TABLE boxes ADD INDEX( omit_memcached );
ALTER TABLE boxes_settings ADD INDEX( show_mode );
ALTER TABLE search_phrases ADD INDEX( phrase );