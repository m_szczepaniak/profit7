CREATE TABLE IF NOT EXISTS `fast_track` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) UNSIGNED NULL,
  `container_id` BIGINT(10) UNSIGNED ZEROFILL NULL,
  `open_datetime` DATETIME NOT NULL,
  `closed` CHAR(1) NOT NULL DEFAULT 0,
  `orders_items_lists_id` INT UNSIGNED NULL,
  `close_datetime` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_fast_track_users1_idx` (`user_id` ASC),
  INDEX `fk_fast_track_containers1_idx` (`container_id` ASC),
  INDEX `fk_fast_track_orders_items_lists1_idx` (`orders_items_lists_id` ASC),
  CONSTRAINT `fk_fast_track_users1`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_fast_track_containers1`
  FOREIGN KEY (`container_id`)
  REFERENCES `containers` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_fast_track_orders_items_lists1`
  FOREIGN KEY (`orders_items_lists_id`)
  REFERENCES `orders_items_lists` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE )
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `fast_track_items` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `fast_track_id` INT UNSIGNED NOT NULL,
  `orders_send_history_items_id` INT(10) UNSIGNED NOT NULL,
  `container_id` BIGINT(10) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_fast_track_items_fast_track1_idx` (`fast_track_id` ASC),
  INDEX `fk_fast_track_items_orders_send_history_items1_idx` (`orders_send_history_items_id` ASC),
  INDEX `fk_fast_track_items_containers1_idx` (`container_id` ASC),
  CONSTRAINT `fk_fast_track_items_fast_track1`
  FOREIGN KEY (`fast_track_id`)
  REFERENCES `fast_track` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_fast_track_items_orders_send_history_items1`
  FOREIGN KEY (`orders_send_history_items_id`)
  REFERENCES `orders_send_history_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_fast_track_items_containers1`
  FOREIGN KEY (`container_id`)
  REFERENCES `containers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;
