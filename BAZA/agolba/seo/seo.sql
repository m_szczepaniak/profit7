CREATE TABLE IF NOT EXISTS `seo` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `box_id` INT UNSIGNED NOT NULL,
  `symbol` VARCHAR(255) NOT NULL,
  `area_symbol` VARCHAR(255) NOT NULL,
  `content` TEXT NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(64) NOT NULL,
  `modified` DATETIME NULL,
  `modified_by` VARCHAR(64) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_seo_1_idx` (`box_id` ASC),
  INDEX `index3` (`symbol` ASC),
  CONSTRAINT `fk_seo_1`
  FOREIGN KEY (`box_id`)
  REFERENCES `boxes` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;