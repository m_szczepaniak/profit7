INSERT INTO `sources`
  (`id`, `provider_id`, `symbol`, `column_prefix_products_stock`, `order_by`, `select_privileged`) VALUES
  (NULL, '114', 'panda', 'panda', '13', '0');

ALTER TABLE  `products_general_discount` ADD  `markup_wholesale_price` DECIMAL( 8, 2 ) NULL DEFAULT NULL ;
ALTER TABLE  `products_general_discount` ADD  `markup_wholesale_price_common` DECIMAL( 8, 2 ) NULL DEFAULT NULL ;


ALTER TABLE products_stock
  ADD  `panda_stock` varchar(20) COLLATE utf8_polish_ci DEFAULT NULL,
  ADD  `panda_status` char(1) COLLATE utf8_polish_ci NOT NULL,
  ADD  `panda_price_brutto` decimal(8,2) NOT NULL,
  ADD  `panda_price_netto` decimal(8,2) NOT NULL,
  ADD  `panda_vat` tinyint(4) NOT NULL,
  ADD  `panda_shipment_time` char(1) COLLATE utf8_polish_ci NOT NULL,
  ADD  `panda_last_import` datetime DEFAULT NULL,
  ADD  `panda_wholesale_price` decimal(8,2) NOT NULL,
  ADD  `panda_shipment_date` date DEFAULT NULL;


INSERT INTO `products_stock_source_mapping` (`source_id`, `input`, `output`) VALUES
  (13, '0-100000', '%s');