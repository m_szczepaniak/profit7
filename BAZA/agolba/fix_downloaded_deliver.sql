
DELETE FROM orders_transport_buffer WHERE id IN (
  SELECT id FROM (
    SELECT OTB.id
    FROM `orders_transport_buffer` AS OTB
    JOIN orders AS O
    ON O.id = OTB.order_id AND confirm_printout_speditor = "1"
    ORDER BY `OTB`.`created` DESC
  ) AS TMP
);
