
CREATE TABLE `products_ateneum_linked` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `mainId` int(10) unsigned NOT NULL,
 `linkedId` int(10) unsigned NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;