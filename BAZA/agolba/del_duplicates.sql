SELECT * FROM orders_cart
WHERE CONCAT(user_id, '-', product_id)
      IN (
        SELECT value FROM (
          SELECT CONCAT(user_id, '-', product_id) AS value
          FROM `orders_cart` AS OC
          GROUP BY CONCAT(user_id, product_id)
          HAVING count(user_id) > 1
        ) AS TMP

      );



DELETE FROM orders_cart
WHERE CONCAT(user_id, '-', product_id)
      IN (
        SELECT value FROM (
                            SELECT CONCAT(user_id, '-', product_id) AS value
                            FROM `orders_cart` AS OC
                            GROUP BY CONCAT(user_id, product_id)
                            HAVING count(user_id) > 1
                          ) AS TMP

      )

LIMIT 1;
