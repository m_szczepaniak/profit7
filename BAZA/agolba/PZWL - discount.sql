SELECT P.name, P.streamsoft_indeks, (100 - (((PS.ateneum_wholesale_price*100) / PS.ateneum_price_brutto))) AS discount
FROM `products` AS P
JOIN products_stock AS PS
    ON P.id = PS.id
WHERE P.publisher_id = 5931 AND
 ROUND (100 - (((PS.ateneum_wholesale_price*100) / PS.ateneum_price_brutto))) >= 38
 AND PS.ateneum_status = '1'