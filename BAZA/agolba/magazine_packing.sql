--ALTER TABLE `containers` ADD `type` CHAR(1) NOT NULL DEFAULT '0' ;
ALTER TABLE `modules` CHANGE `symbol` `symbol` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL;
INSERT INTO `modules` (`id`, `symbol`, `menu`, `box`, `default_module`, `cacheable`, `exclude_from_cache`, `one_per_lang`) VALUES
(48, 'm_zamowienia_magazyn_zbieranie_tramwaj', '0', '0', '0', '0', NULL, '0'),
(47, 'm_zamowienia_magazyn_zbieranie_zasoby', '0', '0', '0', '0', NULL, '0');


ALTER TABLE `containers` ADD `item_id` INT(10) UNSIGNED NULL DEFAULT NULL ;

ALTER TABLE `main_profit24_old`.`orders_items_lists_items_times` 
ADD COLUMN `user_id` INT(10) UNSIGNED NOT NULL AFTER `sc_quantity`,
ADD COLUMN `created` DATETIME NOT NULL AFTER `user_id`;
