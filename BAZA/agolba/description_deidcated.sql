CREATE TABLE IF NOT EXISTS `description_dedicated` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `website_id` INT(10) UNSIGNED,
  `table_destination` VARCHAR(255) NOT NULL,
  'dest_id' INT(10) UNSIGNED,
  `name` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(64) NOT NULL,
  `modified` DATETIME NULL,
  `modified_by` VARCHAR(64) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_description_dedicated_websites1_idx` (`website_id` ASC),
  CONSTRAINT `fk_description_dedicated_websites1`
    FOREIGN KEY (`website_id`)
    REFERENCES `websites` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB;


ALTER TABLE  `description_dedicated` ADD UNIQUE (
`website_id` ,
`dest_id` ,
`table_destination`
);
