use main_profit24;
/* Pierwszy wybor - "Szybki przelew lub BLIK” i logo PayU */
UPDATE orders_payment_types
SET order_by = '1'
WHERE ptype = 'platnoscipl';

/* Drugi wybór - “Płatność kartą” i logo PayU */
UPDATE orders_payment_types
SET order_by = '2'
WHERE ptype = 'card';

/* Trzeci wybór - “Zwykły przelew” - bez loga PayU */
UPDATE orders_payment_types
SET order_by = '3'
WHERE ptype = 'payu_bank_transfer';

/* Czwarty wybór - “Płatność przy odbiorze” */
UPDATE orders_payment_types
SET order_by = '4'
WHERE ptype = 'postal_fee';

/* Żeby kolejność się nie mieszała */
UPDATE orders_payment_types
SET order_by = '5'
WHERE ptype = 'bank_14days';


use main_nieprzeczytane;
/* Pierwszy wybor - "Szybki przelew lub BLIK” i logo PayU */
UPDATE orders_payment_types
SET order_by = '1'
WHERE ptype = 'platnoscipl';

/* Drugi wybór - “Płatność kartą” i logo PayU */
UPDATE orders_payment_types
SET order_by = '2'
WHERE ptype = 'card';

/* Trzeci wybór - “Zwykły przelew” - bez loga PayU */
UPDATE orders_payment_types
SET order_by = '3'
WHERE ptype = 'payu_bank_transfer';

/* Czwarty wybór - “Płatność przy odbiorze” */
UPDATE orders_payment_types
SET order_by = '4'
WHERE ptype = 'postal_fee';

/* Żeby kolejność się nie mieszała */
UPDATE orders_payment_types
SET order_by = '5'
WHERE ptype = 'bank_14days';


use main_mestro;
/* Pierwszy wybor - "Szybki przelew lub BLIK” i logo PayU */
UPDATE orders_payment_types
SET order_by = '1'
WHERE ptype = 'platnoscipl';

/* Drugi wybór - “Płatność kartą” i logo PayU */
UPDATE orders_payment_types
SET order_by = '2'
WHERE ptype = 'card';

/* Trzeci wybór - “Zwykły przelew” - bez loga PayU */
UPDATE orders_payment_types
SET order_by = '3'
WHERE ptype = 'payu_bank_transfer';

/* Czwarty wybór - “Płatność przy odbiorze” */
UPDATE orders_payment_types
SET order_by = '4'
WHERE ptype = 'postal_fee';

/* Żeby kolejność się nie mieszała */
UPDATE orders_payment_types
SET order_by = '5'
WHERE ptype = 'bank_14days';


use main_smarkacz;
/* Pierwszy wybor - "Szybki przelew lub BLIK” i logo PayU */
UPDATE orders_payment_types
SET order_by = '1'
WHERE ptype = 'platnoscipl';

/* Drugi wybór - “Płatność kartą” i logo PayU */
UPDATE orders_payment_types
SET order_by = '2'
WHERE ptype = 'card';

/* Trzeci wybór - “Zwykły przelew” - bez loga PayU */
UPDATE orders_payment_types
SET order_by = '3'
WHERE ptype = 'payu_bank_transfer';

/* Czwarty wybór - “Płatność przy odbiorze” */
UPDATE orders_payment_types
SET order_by = '4'
WHERE ptype = 'postal_fee';

/* Żeby kolejność się nie mieszała */
UPDATE orders_payment_types
SET order_by = '5'
WHERE ptype = 'bank_14days';


use main_naszabiblioteka;
/* Pierwszy wybor - "Szybki przelew lub BLIK” i logo PayU */
UPDATE orders_payment_types
SET order_by = '1'
WHERE ptype = 'platnoscipl';

/* Drugi wybór - “Płatność kartą” i logo PayU */
UPDATE orders_payment_types
SET order_by = '2'
WHERE ptype = 'card';

/* Trzeci wybór - “Zwykły przelew” - bez loga PayU */
UPDATE orders_payment_types
SET order_by = '3'
WHERE ptype = 'payu_bank_transfer';

/* Czwarty wybór - “Płatność przy odbiorze” */
UPDATE orders_payment_types
SET order_by = '4'
WHERE ptype = 'postal_fee';

/* Żeby kolejność się nie mieszała */
UPDATE orders_payment_types
SET order_by = '5'
WHERE ptype = 'bank_14days';