CREATE TABLE IF NOT EXISTS `products_stock_supplies_debug` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `supplies_id` INT UNSIGNED NOT NULL,
  `data` TEXT NOT NULL,
  `order_id` INT UNSIGNED NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;
