SELECT O.id, OI.quantity
FROM orders_items AS OI
JOIN orders AS O
WHERE O.order_status = '4' AND DATE(O.order_date) >= "2017-01-01"
GROUP BY MONTH(O.order_status)