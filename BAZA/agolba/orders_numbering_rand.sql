CREATE TABLE `orders_numbering_rand` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `date` int(11) NOT NULL,
 `rand` int(11) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
