SELECT order_number, value_netto
FROM orders
WHERE
  seller_streamsoft_id = 'PH08'
  AND order_status = "4"
  AND date(status_4_update) >= "2019-01-25"
  AND date(status_4_update) <= "2019-02-28";