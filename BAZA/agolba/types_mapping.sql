CREATE TABLE IF NOT EXISTS `products_types_mapping` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `menus_items_id` INT(10) UNSIGNED NOT NULL,
  `product_type` CHAR(1) NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_types_mapping_menus_items1_idx` (`menus_items_id` ASC),
  CONSTRAINT `fk_products_types_mapping_menus_items1`
    FOREIGN KEY (`menus_items_id`)
    REFERENCES `menus_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

ALTER TABLE `products_types_mapping` CHANGE `product_type` `product_type` CHAR(1) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL;