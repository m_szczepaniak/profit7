CREATE TABLE IF NOT EXISTS `products_reviews_users` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `users_accounts_id` INT(10) UNSIGNED NULL,
  `email` VARCHAR(64) NOT NULL COMMENT 'Uzupełniać pomimo że jest użytkownik zarejestrowany',
  `regulations_agreement` CHAR(1) NOT NULL,
  `promotions_agreement` CHAR(1) NOT NULL,
  `reviews_count` INT NOT NULL,
  vote_sum INT NOT NULL,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_reviews_users_users_accounts1_idx` (`users_accounts_id` ASC),
  CONSTRAINT `fk_products_reviews_users_users_accounts1`
    FOREIGN KEY (`users_accounts_id`)
    REFERENCES `users_accounts` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE `products_reviews` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `website_id` int(10) unsigned DEFAULT NULL,
 `product_id` int(10) unsigned DEFAULT NULL,
 `products_reviews_users_id` int(10) unsigned DEFAULT NULL,
 `review` text NOT NULL,
 `active` char(1) NOT NULL,
 `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `accepted` datetime DEFAULT NULL,
 `accepted_by` varchar(64) DEFAULT NULL,
 `alias` varchar(64) NOT NULL,
 `vote_like` int(10) unsigned DEFAULT '0',
 `vote_unlike` int(10) unsigned DEFAULT '0',
 PRIMARY KEY (`id`),
 KEY `fk_products_reviews_products1_idx` (`product_id`),
 KEY `fk_products_reviews_products_reviews_users1_idx` (`products_reviews_users_id`),
 KEY `fk_products_reviews_websites1_idx` (`website_id`),
 CONSTRAINT `fk_products_reviews_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
 CONSTRAINT `fk_products_reviews_products_reviews_users1` FOREIGN KEY (`products_reviews_users_id`) REFERENCES `products_reviews_users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;




CREATE TABLE `products_rating` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `products_reviews_id` int(10) unsigned NOT NULL,
 `book_rating` tinyint(4) NOT NULL,
 `product_id` int(10) unsigned DEFAULT NULL,
 `website_id` int(10) unsigned DEFAULT NULL,
 `active` char(1) NOT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_products_rating_products1_idx` (`product_id`),
 KEY `fk_products_rating_websites1_idx` (`website_id`),
 KEY `fk_products_rating_products_reviews1_idx` (`products_reviews_id`),
 CONSTRAINT `fk_products_rating_products1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
 CONSTRAINT `fk_products_rating_products_reviews1` FOREIGN KEY (`products_reviews_id`) REFERENCES `products_reviews` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `fk_products_rating_websites1` FOREIGN KEY (`website_id`) REFERENCES `websites` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;





INSERT INTO `modules` (`id`, `symbol`, `menu`, `box`, `default_module`, `cacheable`, `exclude_from_cache`, `one_per_lang`) VALUES
(42, 'm_recenzje', '1', '1', '0', '0', NULL, '0');


-- TO ODPALAMY W NIEPRZECZYTANE
CREATE TABLE IF NOT EXISTS `products_rating_avg` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT(10) UNSIGNED NULL,
  `average` DECIMAL(8,6) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_rating_avg_products1_idx` (`product_id` ASC),
  CONSTRAINT `fk_products_rating_avg_products1`
    FOREIGN KEY (`product_id`)
    REFERENCES `products` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE
    )
ENGINE = InnoDB;



INSERT INTO `boxes` (`id`, `language_id`, `area_id`, `menu_id`, `module_id`, `page_id`, `template`, `name`, `btype`, `start_page`, `subpage`, `symbol`, `order_by`, `cacheable`, `omit_memcached`, `moption`, `cache_per_page`) VALUES
(50, 1, 25, NULL, 42, NULL, 'domyslny.tpl', 'Recenzje produktu', '2', '0', '3', NULL, 4, '0', '0', NULL, '0');

INSERT INTO `boxes` (`language_id`, `area_id`, `menu_id`, `module_id`, `page_id`, `template`, `name`, `btype`, `start_page`, `subpage`, `symbol`, `order_by`, `cacheable`, `omit_memcached`, `moption`, `cache_per_page`) VALUES
(1, 25, NULL, 42, NULL, 'lista.tpl', 'Lista recenzji produktu', '2', '0', '3', NULL, 5, '0', '0', 'lista', '0');


-- trzeba dodać stronę w strukturze menu w nieprzeczytane - recenzje