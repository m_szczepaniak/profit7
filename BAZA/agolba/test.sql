      SELECT
        BA.streamsoft_id,
        DATE(OPL.date) as c_date,
        IF(OPL.ammount > 0.00, "BP_KROZ", "BW_KROZ2") as `type`,
        IF(OPL.type = "bank_14days", OUA.streamsoft_id,
          IF(OPL.type = "opek", "1825",
            IF(OPL.type = "tba", "9006",
              IF(OPL.type = "ruch", "6690",
                IF(OPL.type = "poczta_polska" OR OPL.type = "poczta-polska-doreczenie-pod-adres", "9267", "0")
              )
            )
          )
        ) AS kontrahent,
        ABS(OPL.ammount) AS kwota,
        REPLACE(IF(OPL.ammount > 0, OPL.ammount, 0), ".", ",") as wplata,
        REPLACE(IF(OPL.ammount <= 0, OPL.ammount, 0)*(-1), ".", ",") as wyplata,
        O.order_number,
        OPL.id,
        OPL.title
      FROM orders_payments_list AS OPL
      JOIN orders_bank_statements AS OBS
       ON OPL.statement_id = OBS.id
      JOIN bank_accounts AS BA
       ON BA.ident_statments_file = OBS.ident_statments_file
      JOIN orders AS O
       ON O.id = OPL.order_id
      LEFT JOIN orders_users_addresses AS OUA
        ON OUA.order_id = O.id AND OUA.streamsoft_id IS NOT NULL
      WHERE
       OPL.statement_id > 0 AND
       IF(OPL.type = "bank_14days", (OUA.streamsoft_id > 0 AND OUA.streamsoft_id IS NOT NULL), 1=1) AND
       DATE(OPL.date) = "2017-01-18"
      GROUP BY OPL.id






      SELECT
        BA.streamsoft_id,
        DATE(OBS.created) as c_date,
        IF(OBS.paid_amount > 0.00, "BP_KROZ", "BW_KROZ2") as `type`,
        IF (BA.streamsoft_id = "03", "6016", "0") AS kontrahent,
        ABS(OBS.paid_amount) as kwota,
        REPLACE(IF(OBS.paid_amount > 0, OBS.paid_amount, 0), ".", ",") as wplata,
        REPLACE(IF(OBS.paid_amount <= 0, OBS.paid_amount, 0)*(-1), ".", ",") as wyplata,
        IF(OBS.order_nr <> "", OBS.order_nr, OBS.title) as order_number,
        OBS.id
      FROM orders_bank_statements AS OBS
      JOIN bank_accounts AS BA
      ON BA.ident_statments_file = OBS.ident_statments_file
      AND OBS.ident_statments_file = "3300019815"
      WHERE
        approved = "1" AND
        DATE(OBS.created) = "2017-01-18"
      GROUP BY OBS.id