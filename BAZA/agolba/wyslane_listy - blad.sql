      UPDATE orders_items AS OI
      JOIN orders AS O
        ON O.id = OI.order_id AND order_status <> "4" AND order_status <> "5"
      JOIN orders_send_history_items AS OSHI
        ON OI.id = OSHI.item_id
      JOIN orders_send_history AS OSH
        ON OSH.id = OSHI.send_history_id AND OSH.source = OI.source
      SET OI.status = "2"
      WHERE OI.status = "1"
    
    
SELECT O.order_number, OI.isbn, OI.name, (
  SELECT CONCAT(OSH.source, ",", OSH.number)
  FROM orders_send_history_items AS OSHI
  JOIN orders_send_history AS OSH
    ON OSH.id = OSHI.send_history_id
  WHERE OSHI.item_id = OI.id
  ORDER BY OSH.id DESC
  LIMIT 1
),
OAI.value
FROM orders_items AS OI
JOIN orders AS O
  ON OI.order_id = O.id AND order_status <> "4" AND order_status <> "5"
JOIN orders_additional_info AS OAI
  ON OAI.order_id = O.id
WHERE OAI.order_id IS NOT NULL
 AND 
 CONCAT(OI.source, "|", "2", "|", "20") = (
  SELECT CONCAT(OSH.source, "|", magazine_status, "|", SUBSTR(pack_number, 1, 2)) 
  FROM orders_send_history_items AS OSHI
  JOIN orders_send_history AS OSH
    ON OSH.id = OSHI.send_history_id
  WHERE OSHI.item_id = OI.id
  ORDER BY OSH.id DESC
  LIMIT 1
)
AND
OI.status = "2"
AND OI.deleted = "0"
AND DATE(O.order_date) > "2014-01-01"
ORDER BY O.id DESC


UPDATE orders_items
SET status = "1"
WHERE id IN (
  SELECT id FROM (
    SELECT OI.id
    FROM orders_items AS OI
    JOIN orders AS O
      ON OI.order_id = O.id AND order_status <> "4" AND order_status <> "5"
    LEFT JOIN orders_additional_info AS OAI
      ON OAI.order_id = O.id
    WHERE OAI.order_id IS NULL
     AND 
     CONCAT(OI.source, "|", "2", "|", "20") = (
      SELECT CONCAT(OSH.source, "|", magazine_status, "|", SUBSTR(pack_number, 1, 2)) 
      FROM orders_send_history_items AS OSHI
      JOIN orders_send_history AS OSH
        ON OSH.id = OSHI.send_history_id
      WHERE OSHI.item_id = OI.id
      ORDER BY OSH.id DESC
      LIMIT 1
    )
    AND
    OI.status = "2"
    AND OI.deleted = "0"
    AND DATE(O.order_date) > "2014-01-01"
    ORDER BY O.id DESC
  ) AS TMP
)
    
    
SELECT OI.name, OI.source, O.order_number 
FROM orders_items AS OI
JOIN orders AS O
  ON O.id = OI.order_id AND order_status <> "4" AND order_status <> "5"
LEFT JOIN orders_send_history_items AS OSHI
  ON OI.id = OSHI.item_id
WHERE OI.status = "2"
AND OSHI.id IS NULL
ORDER BY OI.id DESC

UPDATE orders
SET transport_number = ''
WHERE id IN (
    SELECT id FROM (
        SELECT O.id FROM orders_items_lists_items AS OILI
        JOIN orders AS O
            ON O.id = OILI.orders_id AND
            O.order_status <> '3'
        WHERE O.transport_number <> '' AND OILI.orders_items_lists_id = 21337
    ) as TMP
)
LIMIT 34;