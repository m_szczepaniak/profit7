SELECT
  T.name AS "Nazwa",
  FORMAT(SUM(FORMAT(O.transport_cost / (1 + (O.transport_vat / 100)), 2)), 2) AS "Wartość NETTO",
  DATE(O.status_4_update) AS "Data realizacji"
FROM orders AS O
  JOIN orders_transport_means	AS T
    ON T.id = O.transport_id
WHERE
  O.transport_cost > 0
  AND O.order_status = "4"
  AND DATE(O.status_4_update) > "2017-01-01"
GROUP BY O.transport_id, DATE(O.status_4_update)
ORDER BY "Data realizacji" ASC


-- stock
SELECT
  SUM(OI.value_netto) AS "Wartość NETTO",
  SUM(OI.quantity) AS "ILOŚĆ EGZ",
  DATE(O.status_4_update) AS "Data realizacji"
FROM orders_items	AS OI
JOIN orders AS O
  ON OI.order_id = O.id
WHERE
  OI.source IN ("51", "1", "0")
  AND OI.deleted = "0"
  AND O.order_status = "4"
  AND DATE(O.status_4_update) > "2017-01-01"
  AND O.website_id = "5"
GROUP BY DATE(O.status_4_update)
ORDER BY "Data realizacji" ASC;


-- baza
SELECT
  SUM(OI.value_netto) AS "Wartość NETTO",
  SUM(OI.quantity) AS "ILOŚĆ EGZ",
  DATE(O.status_4_update) AS "Data realizacji"
FROM orders_items	AS OI
  JOIN orders AS O
    ON OI.order_id = O.id
WHERE
  OI.source NOT IN ("51", "0", "1")
  AND OI.deleted = "0"
  AND O.order_status = "4"
  AND DATE(O.status_4_update) > "2017-01-01"
  AND O.website_id = "5"
GROUP BY DATE(O.status_4_update)
ORDER BY "Data realizacji" ASC
