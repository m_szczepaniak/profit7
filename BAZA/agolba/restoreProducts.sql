SELECT P.* FROM `main_profit24`.`products` AS P
JOIN `backup_profit24`.`products` AS PP
ON P.id = PP.id
WHERE
P.modified_by = 'ateneum'


SELECT P.publisher_id, PP.publisher_id FROM `main_profit24`.`products` AS P
JOIN `backup_profit24`.`products` AS PP
ON P.id = PP.id
WHERE P.publisher_id <> PP.publisher_id
AND P.modified_by = 'ateneum'


UPDATE main_profit24.products AS P
JOIN `backup_profit24`.`products` AS PP
    ON P.id = PP.id
SET
    P.publisher_id = PP.publisher_id,
    P.binding = PP.binding,
    P.dimension = PP.dimension,
    P.language = PP.language,
    P.original_language = PP.original_language,
    P.name = PP.name,
    P.plain_name = PP.plain_name,
    P.name2 = PP.name2,
    P.short_description = PP.short_description,
    P.description = PP.description,
    P.publication_year = PP.publication_year,
    P.pages = PP.pages,
    P.translator = PP.translator,
    P.volumes = PP.volumes,
    P.volume_nr = PP.volume_nr,
    P.edition = PP.edition,
    P.volume_name = PP.volume_name ,
    P.city = PP.city,
    P.legal_status_date = PP.legal_status_date,
    P.original_title = PP.original_title
WHERE
    P.modified_by = 'ateneum'


SELECT * FROM main_profit24.products_to_authors AS PTA1
JOIN `backup_profit24`.`products_to_authors` AS PTA2
    ON PTA1.id = PTA2.id
WHERE PTA1.author_id <> PTA2.author_id


INSERT INTO main_profit24.products_authors
SELECT P.* FROM `backup_profit24`.`products_authors` AS P
LEFT JOIN main_profit24.products_authors AS PP
    ON P.id = PP.id
WHERE PP.id IS NULL


INSERT INTO main_profit24.products_to_authors
SELECT P.* FROM `backup_profit24`.`products_to_authors` AS P
JOIN `main_profit24`.products AS PR ON PR.id = P.product_id
LEFT JOIN main_profit24.products_to_authors AS PP
    ON P.id = PP.id
WHERE PP.id IS NULL



INSERT INTO main_profit24.products
SELECT P.* FROM `backup_profit24`.`products` AS P

LEFT JOIN main_profit24.products AS PP
    ON P.id = PP.id
WHERE PP.id IS NULL




UPDATE main_profit24.`products_shadow` AS P
JOIN `main_profit24`.products AS PP
    ON P.id = PP.id
SET
    P.publisher_id = PP.publisher_id,
    P.publication_year = PP.publication_year,
    P.edition = PP.edition,
    P.title = PP.name,
    P.name2 = PP.name2,



    P.binding = PP.binding,
    P.dimension = PP.dimension,
    P.language = PP.language,
    P.original_language = PP.original_language,
    P.name = PP.name,
    P.plain_name = PP.plain_name,
    P.name2 = PP.name2,
    P.short_description = PP.short_description,
    P.description = PP.description,

    P.pages = PP.pages,
    P.translator = PP.translator,
    P.volumes = PP.volumes,
    P.volume_nr = PP.volume_nr,

    P.volume_name = PP.volume_name ,
    P.city = PP.city,
    P.legal_status_date = PP.legal_status_date,
    P.original_title = PP.original_title
WHERE
    P.modified_by = 'ateneum'