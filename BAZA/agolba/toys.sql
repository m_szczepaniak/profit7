ALTER TABLE  `products` ADD  `product_type` CHAR( 1 ) NOT NULL DEFAULT  'K' AFTER  `main_category_id` ;

CREATE TABLE IF NOT EXISTS `products_game_attributes` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT(10) UNSIGNED NOT NULL,
  `sex` CHAR(1) NULL DEFAULT 0 COMMENT 'F - kobieta, M - mezczyzna, 0 - brak oznaczenia',
  `age` VARCHAR(32) NULL COMMENT 'Wiek',
  `game_time` VARCHAR(128) NULL COMMENT 'Czas gry',
  `number_of_players` VARCHAR(32) NULL COMMENT 'Ilość graczy',
  `content_of_box` VARCHAR(1024) NULL COMMENT 'Zawartość pudełka',
  `count_elements` INT(10) NULL COMMENT 'Ilość elementów',
  `lego_code` VARCHAR(16) NULL COMMENT 'Kod produktu',
  `modified` DATETIME NULL,
  `modified_by` VARCHAR(64) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `fk_products_game_attributes_products1_idx` (`product_id` ASC),
  CONSTRAINT `fk_products_game_attributes_products1`
  FOREIGN KEY (`product_id`)
  REFERENCES `products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;