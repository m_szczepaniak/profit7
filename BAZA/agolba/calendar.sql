ALTER TABLE  `orders_payment_types` ADD  `cart_calendar_legend` TEXT NOT NULL ;


UPDATE  `orders_payment_types` 
SET  `cart_calendar_legend` =  '<table border="0" class="calendar legend">
	<tbody>
		<tr>
			<td class="today">{czas_nadania}</td>
			<td>Data złożenia zamówienia</td>
		</tr>
		<tr>
			<td class="shipment">{czas_wysylki}</td>
			<td>Planowana data wysyłki (przy zaksięgowaniu wpłaty do godziny 14)</td>
		</tr>
		<tr>
			<td class="send">{czas_transportu}</td>
			<td>Planowana data doręczenia przesyłki</td>
		</tr>
	</tbody>
</table>
'
WHERE ptype = 'bank_transfer';


UPDATE  `orders_payment_types` 
SET  `cart_calendar_legend` =  '<table border="0" class="calendar legend">
	<tbody>
		<tr>
			<td class="today">{czas_nadania}</td>
			<td>Data złożenia zamówienia</td>
		</tr>
		<tr>
			<td class="shipment">{czas_wysylki}</td>
			<td>Planowana data wysyłki</td>
		</tr>
		<tr>
			<td class="send">{czas_transportu}</td>
			<td>Planowana data doręczenia przesyłki</td>
		</tr>
	</tbody>
</table>
'
WHERE ptype <> 'bank_transfer';

UPDATE  `orders_payment_types` 
SET  `message_before` = REPLACE(message_before, '16', '14')
WHERE message_before LIKE '%16%';

UPDATE  `orders_payment_types` 
SET  `message_after` = REPLACE(message_after, '16', '14')
WHERE message_after LIKE '%16%';