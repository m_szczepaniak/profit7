CREATE TABLE IF NOT EXISTS `products_websites` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT(10) UNSIGNED NOT NULL,
  `website_id` INT(10) UNSIGNED NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_websites_products1_idx` (`product_id` ASC),
  INDEX `fk_products_websites_websites1_idx` (`website_id` ASC),
  CONSTRAINT `fk_products_websites_products1`
    FOREIGN KEY (`product_id`)
    REFERENCES `products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_products_websites_websites1`
    FOREIGN KEY (`website_id`)
    REFERENCES `websites` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB