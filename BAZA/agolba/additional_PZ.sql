CREATE TABLE IF NOT EXISTS `main_profit24_old`.`orders_send_history_additional_items` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `orders_send_history_id` INT(10) UNSIGNED NOT NULL,
  `indeks` VARCHAR(13) NOT NULL,
  `streamsoft_id` VARCHAR(13) NULL,
  `created` DATE NOT NULL,
  `created_by` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_send_history_additional_items_orders_send_history_idx` (`orders_send_history_id` ASC),
  CONSTRAINT `fk_orders_send_history_additional_items_orders_send_history1`
    FOREIGN KEY (`orders_send_history_id`)
    REFERENCES `main_profit24_old`.`orders_send_history` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;