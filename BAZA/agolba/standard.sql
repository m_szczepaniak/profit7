
-- przed odpaleniem sprawdzić czas wykonywania
ALTER TABLE  `products` ADD  `standard_code` VARCHAR( 20 ) NULL DEFAULT NULL ,
  ADD  `standard_quantity` SMALLINT NULL DEFAULT NULL ,
  ADD INDEX (  `standard_code` ) ;
