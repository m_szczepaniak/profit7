CREATE TABLE IF NOT EXISTS `inventory` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `container_id` BIGINT(10) UNSIGNED ZEROFILL NOT NULL,
  `status` CHAR(1) NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_inventory_containers1_idx` (`container_id` ASC),
  CONSTRAINT `fk_inventory_containers1`
    FOREIGN KEY (`container_id`)
    REFERENCES `containers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `inventory_items` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT(10) UNSIGNED NOT NULL,
  `inventory_id` INT UNSIGNED NOT NULL,
  `quantity` INT NOT NULL,
  `orginally_scanned` INT NOT NULL,
  `scanned_by` VARCHAR(128) NOT NULL,
  `scanned` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_inventory_items_products1_idx` (`product_id` ASC),
  INDEX `fk_inventory_items_inventory1_idx` (`inventory_id` ASC),
  CONSTRAINT `fk_inventory_items_products1`
    FOREIGN KEY (`product_id`)
    REFERENCES `products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_inventory_items_inventory1`
    FOREIGN KEY (`inventory_id`)
    REFERENCES `inventory` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;