CREATE TABLE IF NOT EXISTS `orders_mails_sended` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `mail_type` CHAR(1) NOT NULL,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_mails_sended_orders1_idx` (`order_id` ASC),
  CONSTRAINT `fk_orders_mails_sended_orders1`
    FOREIGN KEY (`order_id`)
    REFERENCES `orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB