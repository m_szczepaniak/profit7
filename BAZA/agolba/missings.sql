ALTER TABLE orders_items_lists ADD orders_send_history_id INT(10) UNSIGNED NULL;
ALTER TABLE orders_items_lists
ADD CONSTRAINT orders_items_lists_orders_send_history_id_fk
FOREIGN KEY (orders_send_history_id) REFERENCES orders_send_history (id);