ALTER TABLE  `products_stock_supplies` ADD  `reservation_erp` MEDIUMINT( 8 ) UNSIGNED NOT NULL AFTER  `reservation` ;



SELECT P.EAN_13 FROM `products_stock_supplies` AS PSS
LEFT JOIN products_stock_supplies_to_reservations AS PSSTR
    ON PSSTR.products_stock_supplies_id = PSS.id
LEFT JOIN products AS P
    ON P.id = PSS.product_id
WHERE
PSSTR.id IS NULL AND
PSS.reservation > 0 AND
PSS.source = 10
AND
(
    SELECT OI.id
    FROM orders_items AS OI
    JOIN orders AS O
        ON O.id = OI.order_id
    WHERE
    OI.product_id = PSS.product_id
    AND OI.deleted = "0"
    AND OI.source = 51
    AND O.order_status <> "5"
    AND
    (
    O.order_status <> "4"
    OR O.erp_export IS NULL
    OR O.erp_send_ouz IS NULL
    )
    LIMIT 1
)
IS NULL



SELECT PSS.id, P.ean_13
FROM `products_stock_supplies` AS PSS
LEFT JOIN products AS P
    ON P.id = PSS.product_id
WHERE
PSS.reservation > 0 AND
PSS.source = 10
AND
(
    SELECT OI.id
    FROM orders_items AS OI
    JOIN orders AS O
        ON O.id = OI.order_id
    WHERE
    OI.product_id = PSS.product_id
    AND OI.deleted = "0"
    AND OI.source = 51
    AND O.erp_export IS NULL
    LIMIT 1
)
IS NULL


SELECT P.EAN_13, PSS.reservation, PSS.reservation_erp
FROM `products_stock_supplies` AS PSS
JOIN products_stock_supplies_to_reservations AS PSSTR
    ON PSSTR.products_stock_supplies_id = PSS.id
LEFT JOIN products AS P
    ON P.id = PSS.product_id
WHERE
PSS.reservation > 0 AND
PSS.source = 10
AND
(
    SELECT OI.id
    FROM orders_items AS OI
    JOIN orders AS O
        ON O.id = OI.order_id
    WHERE
    OI.product_id = PSS.product_id
    AND OI.deleted = "0"
    AND OI.source = 51
    AND O.order_status <> "5"
    AND
    (
    O.order_status <> "4"
    OR O.erp_export IS NULL
    OR O.erp_send_ouz IS NULL
    )
    LIMIT 1
)
IS NULL






AND
(
SELECT SUM(quantity)
FROM products_stock_reservations AS PSR
WHERE  PSR.products_stock_id = PSS.product_id AND PSR.source_id = PSS.source
) <> PSS.reservation



SELECT OI.id
FROM orders_items AS OI
JOIN orders AS O
    ON O.id = OI.order_id
WHERE
OI.product_id = PSS.product_id
OI.deleted = "0"
AND OI.source = 51
AND O.order_status <> "5"
AND O.order_status <> "4"
AND O.erp_export IS NULL
AND O.erp_send_ouz IS NULL




SELECT * FROM orders_items AS OI
JOIN orders AS O
ON O.id = OI.order_id
LEFT JOIN products_stock_reservations AS PSR
ON PSR.orders_items_id = OI.id
LEFT JOIN products_stock_supplies_to_reservations AS PSSTR
    ON PSSTR.products_stock_reservations_id = PSR.id
WHERE
PSSTR.id IS NULL AND
O.erp_export IS NULL AND
OI.source = 51 AND
OI.deleted = "0" AND
O.order_status <> "5" and
O.order_status <> "4" AND
(OI.status = "3" OR OI.status = "4")