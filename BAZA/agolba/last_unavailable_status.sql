ALTER TABLE  `products_stock` ADD  `profit_g_act_stock_unavailable_last_update` DATETIME NULL AFTER  `profit_g_last_import` ;
ALTER TABLE  `products_stock` ADD INDEX (  `profit_g_act_stock_unavailable_last_update` ) ;


CREATE TABLE IF NOT EXISTS `products_stock_locations` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_stock_id` INT(10) UNSIGNED NOT NULL,
  `new_x_location` VARCHAR(45) NULL,
  `old_x_location` VARCHAR(45) NULL,
  `new_g_location` VARCHAR(45) NULL,
  `old_g_location` VARCHAR(45) NULL,
  `last_update` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_stock_locations_products_stock1_idx` (`products_stock_id` ASC),
  CONSTRAINT `fk_products_stock_locations_products_stock1`
    FOREIGN KEY (`products_stock_id`)
    REFERENCES `products_stock` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB;
