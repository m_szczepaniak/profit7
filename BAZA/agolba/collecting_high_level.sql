CREATE TABLE IF NOT EXISTS `collecting_high_level` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT(10) UNSIGNED NOT NULL,
  `active` CHAR(1) NOT NULL DEFAULT '0',
  `completed` CHAR(1) NOT NULL DEFAULT '0',
  `quantity` INT NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_collecting_high_level_products1_idx` (`product_id` ASC),
  CONSTRAINT `fk_collecting_high_level_products1`
    FOREIGN KEY (`product_id`)
    REFERENCES `products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `collecting_high_level_lists` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `collecting_high_level_id` INT UNSIGNED NOT NULL,
  `orders_items_lists_id` INT UNSIGNED NOT NULL,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_collecting_high_level_lists_collecting_high_level1_idx` (`collecting_high_level_id` ASC),
  INDEX `fk_collecting_high_level_lists_orders_items_lists1_idx` (`orders_items_lists_id` ASC),
  CONSTRAINT `fk_collecting_high_level_lists_collecting_high_level1`
    FOREIGN KEY (`collecting_high_level_id`)
    REFERENCES `collecting_high_level` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_collecting_high_level_lists_orders_items_lists1`
    FOREIGN KEY (`orders_items_lists_id`)
    REFERENCES `orders_items_lists` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

ALTER TABLE  `collecting_high_level` ADD  `collecting` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `completed` ,
ADD INDEX (  `collecting` ) ;


CREATE TABLE IF NOT EXISTS `collecting_high_level_orders_items` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `collecting_high_level_id` INT UNSIGNED NOT NULL,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `orders_items_id` INT(10) UNSIGNED NOT NULL,
  `quantity` INT UNSIGNED NOT NULL,
  `created` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_table1_collecting_high_level1_idx` (`collecting_high_level_id` ASC),
  INDEX `fk_table1_orders1_idx` (`order_id` ASC),
  INDEX `fk_collecting_high_level_orders_items_orders_items1_idx` (`orders_items_id` ASC),
  CONSTRAINT `fk_table1_collecting_high_level1`
    FOREIGN KEY (`collecting_high_level_id`)
    REFERENCES `collecting_high_level` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_table1_orders1`
    FOREIGN KEY (`order_id`)
    REFERENCES `orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_collecting_high_level_orders_items_orders_items1`
    FOREIGN KEY (`orders_items_id`)
    REFERENCES `orders_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

ALTER TABLE  `collecting_high_level_orders_items` ADD UNIQUE (
    `collecting_high_level_id` ,
    `order_id` ,
    `orders_items_id`
);
