SELECT P.isbn_plain, P2.isbn_plain, P.created_by, P2.created_by FROM `products` AS P
JOIN products AS P2
ON P.id <> P2.id AND CONCAT('0', P.isbn_plain) = P2.isbn_plain
WHERE LENGTH(P.isbn_plain) = 12

SELECT P.isbn_plain, P2.isbn_plain, P.created_by, P2.created_by FROM `products` AS P
JOIN products AS P2
ON P.id <> P2.id AND CONCAT('00', P.isbn_plain) = P2.isbn_plain
WHERE LENGTH(P.isbn_plain) = 11
AND P.isbn_plain <> "00000000000"
AND P.packet <> '1';