CREATE TABLE IF NOT EXISTS `orders_counter` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `send_sortout_quantity` INT UNSIGNED NOT NULL,
  `created` DATE NOT NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB;
ALTER TABLE  `orders_counter` ADD UNIQUE (
  `created`
);


INSERT INTO orders_counter SET send_sortout_quantity = 695, created = "2018-08-07";

INSERT INTO orders_counter
(created, send_sortout_quantity) values (CURDATE(), 1)
ON DUPLICATE KEY UPDATE created = CURDATE(), send_sortout_quantity = send_sortout_quantity + 1
