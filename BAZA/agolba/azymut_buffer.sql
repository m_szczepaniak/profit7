ALTER TABLE  `products_azymut_buffer`
ADD  `age` VARCHAR( 32 ) NULL DEFAULT NULL AFTER  `foreign_language_book` ,
ADD  `sex` CHAR( 1 ) NULL DEFAULT  '0' AFTER  `age` ;


ALTER TABLE  `products_azymut_buffer_copy`
ADD  `age` VARCHAR( 32 ) NULL DEFAULT NULL AFTER  `foreign_language_book` ,
ADD  `sex` CHAR( 1 ) NULL DEFAULT  '0' AFTER  `age` ;




ALTER TABLE  `products_azymut_buffer` ADD  `attributes` TEXT NULL DEFAULT NULL ;
ALTER TABLE  `products_azymut_buffer_copy` ADD  `attributes` TEXT NULL DEFAULT NULL ;
