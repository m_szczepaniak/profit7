SELECT O.order_number
 FROM `stock_location_orders_items_lists_items` AS SL
 join orders_items_lists_items AS OILI
    ON SL.orders_items_lists_items_id = OILI.id
 JOIN orders AS O
    ON O.id = OILI.orders_id
WHERE `stock_location_id` = 63346
ORDER BY O.id  DESC;


SELECT DISTINCT SL.id, SL.*
FROM stock_location AS SL
JOIN stock_location_orders_items_lists_items AS SLO
    ON SLO.stock_location_id = SL.id AND type = "MM"
LEFT JOIN magazine_return_items AS MRI
    ON MRI.product_id = SL.products_stock_id
LEFT JOIN magazine_return AS MR
    ON MR.id = MRI.magazine_return_id AND MR.status = "1"

WHERE
SL.magazine_type = "SL" AND
SL.reservation > 0 AND
MR.id IS NULL
AND

(
SELECT O.id FROM orders_items_lists_items AS OILI
LEFT JOIN orders_items AS OI
    ON OI.id = OILI.orders_items_id
LEFT JOIN orders AS O
    ON O.id = OI.order_id AND O.erp_export IS NULL AND O.order_status <> '5'
WHERE
    OILI.id = SLO.orders_items_lists_items_id
)
IS NULL
ORDER BY SL.products_stock_id DESC;



AND SL.products_stock_id = "896375"

AND
(
    SELECT O.id
    FROM orders_items AS OI
     JOIN orders AS O
        ON O.id = OI.order_id AND (O.erp_export IS NULL OR O.order_status <> '5')
    WHERE OI.product_id = SL.products_stock_id AND OI.deleted = "0"

    LIMIT 1
)
IS NULL
