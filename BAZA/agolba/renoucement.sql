ALTER TABLE `orders_payment_returns`
ADD `type` char(1) DEFAULT NULL COMMENT 'Z - Zwrot D - Odstąpienie',
ADD `is_statement_return` char(1) DEFAULT '1',
ADD statement_title VARCHAR(128) DEFAULT "" NULL,
ADD active CHAR(1) DEFAULT "1" NULL;


UPDATE orders_payment_returns SET `type` = "Z";

ALTER TABLE orders_payment_returns ADD statement_name_to VARCHAR(100) DEFAULT "" NULL;