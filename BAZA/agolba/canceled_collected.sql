ALTER TABLE `profit2`.`orders_items` 
ADD COLUMN `canceled_collected` SMALLINT(5) UNSIGNED NOT NULL DEFAULT 0 AFTER `bundle_value_brutto`;

UPDATE orders_items AS OI
JOIN orders AS O
  ON O.id = OI.order_id
JOIN products AS P
  ON P.id = OI.product_id
SET 
    OI.canceled_collected = OI.quantity
WHERE 
   (O.order_status = "5" OR (OI.deleted = "1" AND (O.order_status = "4" OR O.order_status = "3"))) 
   AND (OI.item_type = "I" OR OI.item_type = "P")
   AND OI.packet = "0"
   AND OI.quantity > OI.canceled_collected
   AND OI.status = "4"




SELECT DISTINCT O.id, O.* 
             FROM orders AS O
             WHERE 
             ( 
               O.order_status = "0" OR 
               O.order_status = "1"
             )
              AND 
              (
               SELECT O_TEST.id 
               FROM orders AS O_TEST
               WHERE 
                 O_TEST.id <> O.id 
                 AND (
                    O_TEST.client_ip = O.client_ip OR 
                    O_TEST.email = O.email
                 )
                 AND (
                   O_TEST.order_status = "0" OR 
                   O_TEST.order_status = "1")
                 AND DATE(O_TEST.order_date) = CURDATE()
                 LIMIT 1
               ) IS NOT NULL
                AND (
                 SELECT ORC.id 
                 FROM orders_reasons_containers AS ORC
                 WHERE 
                  ORC.order_id = O.id AND
                  ORC.orders_reasons_type = "D"
                 LIMIT 1
                ) IS NULL
                AND DATE(O.order_date) = CURDATE()
