CREATE TABLE IF NOT EXISTS `main_profit24_old`.`products_stock_supplies` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT(10) UNSIGNED NOT NULL,
  `source` INT UNSIGNED NOT NULL DEFAULT 10 COMMENT 'źródło',
  `quantity` INT UNSIGNED NOT NULL,
  `type` CHAR(4) NOT NULL COMMENT 'Typ dokumentu MM|FV',
  `wholesale_price` DECIMAL(8,2) UNSIGNED NOT NULL,
  `price_brutto` DECIMAL(8,2) NOT NULL,
  `price_netto` DECIMAL(8,2) NOT NULL,
  `vat` INT NOT NULL,
  `reservation` MEDIUMINT UNSIGNED NOT NULL,
  `reservation_to_reduce` MEDIUMINT UNSIGNED NOT NULL,
  `last_update` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_stock_supplies_products1_idx` (`product_id` ASC),
  CONSTRAINT `fk_products_stock_supplies_products1`
    FOREIGN KEY (`product_id`)
    REFERENCES `main_profit24_old`.`products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `main_profit24_old`.`products_stock_supplies_to_reservations` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_stock_reservations_id` INT UNSIGNED NOT NULL,
  `products_stock_supplies_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_stock_supplies_to_reservations_products_stock_r_idx` (`products_stock_reservations_id` ASC),
  INDEX `fk_products_stock_supplies_to_reservations_products_stock_s_idx` (`products_stock_supplies_id` ASC),
  CONSTRAINT `fk_products_stock_supplies_to_reservations_products_stock_res1`
    FOREIGN KEY (`products_stock_reservations_id`)
    REFERENCES `main_profit24_old`.`products_stock_reservations` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_products_stock_supplies_to_reservations_products_stock_sup1`
    FOREIGN KEY (`products_stock_supplies_id`)
    REFERENCES `main_profit24_old`.`products_stock_supplies` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;