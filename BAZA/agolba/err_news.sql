
UPDATE `products` AS P
LEFT JOIN products_news AS PN
ON PN.product_id = P.id
SET P.is_news = '0'
WHERE P.is_news = '1' AND P.is_previews = '1';


UPDATE
products, products_shadow
SET
products_shadow.is_news = products.is_news
WHERE
products_shadow.id=products.id;