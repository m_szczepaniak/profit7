CREATE TABLE `products_publishers_groups` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `name` varchar(128) NOT NULL,
 `created` datetime NOT NULL,
 `created_by` varchar(128) NOT NULL,
 `modified` datetime NOT NULL,
 `modified_by` varchar(128) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `products_publishers_to_groups` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `group_id` int(10) unsigned NOT NULL,
 `publisher_id` int(10) unsigned NOT NULL,
 PRIMARY KEY (`id`),
 KEY `publisher_id` (`publisher_id`),
 KEY `group_id` (`group_id`),
 CONSTRAINT `products_publishers_to_groups_ibfk_2` FOREIGN KEY (`publisher_id`) REFERENCES `products_publishers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
 CONSTRAINT `products_publishers_to_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `products_publishers_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;