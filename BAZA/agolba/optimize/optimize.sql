EXPLAIN
SELECT DISTINCT OSH.id, OSH.number, OSH.pack_number,  OSH.date_send, OSH.source, OSH.saved_type, OSH.status,
(
    SELECT OSHA.fv_nr
    FROM orders_send_history_attributes AS OSHA
    WHERE OSH.id = OSHA.orders_send_history_id
    LIMIT 1
) AS fv_nr
, OSH.in_document_type, OSH.omit_create_doc_streamsoft, OSH.send_by, (SELECT name FROM external_providers AS EP WHERE EP.id=OSH.source) AS source_name,
OSH.independent_stock
FROM orders_send_history AS OSH
JOIN orders_send_history_items AS OSHI
 ON OSH.id = OSHI.send_history_id
JOIN orders_items AS OI
 ON OI.id = OSHI.item_id
 AND OI.status = '2'
 AND OI.deleted = '0'
 AND OSH.source = OI.source
JOIN orders AS O
 ON O.id = OI.order_id
 AND O.order_status <> '5'
 AND O.order_status <> '4'
 AND O.order_status <> '3'
WHERE OSH.status = '1'
AND OSH.magazine_status <> '5'

UNION SELECT DISTINCT OSH.id, OSH.number, OSH.pack_number,  OSH.date_send, OSH.source, OSH.saved_type, OSH.status,
(
    SELECT OSHA.fv_nr
    FROM orders_send_history_attributes AS OSHA
    WHERE OSH.id = OSHA.orders_send_history_id
    LIMIT 1
) AS fv_nr
, OSH.in_document_type, OSH.omit_create_doc_streamsoft, OSH.send_by, (SELECT name FROM external_providers AS EP WHERE EP.id=OSH.source) AS source_name,
OSH.independent_stock
FROM orders_send_history AS OSH
JOIN orders_send_history_items AS OSHI
 ON OSH.id = OSHI.send_history_id
WHERE OSH.status = '1'
AND OSH.magazine_status <> '5'
AND OSH.independent_stock = '1'
ORDER BY date_send













EXPLAIN
SELECT DISTINCT OSH.id, OSH.number, OSH.pack_number,  OSH.date_send, OSH.source, OSH.saved_type, OSH.status,
(
SELECT OSHA.fv_nr
FROM orders_send_history_attributes AS OSHA
WHERE OSH.id = OSHA.orders_send_history_id
LIMIT 1
) AS fv_nr
, OSH.in_document_type, OSH.omit_create_doc_streamsoft, OSH.send_by, (SELECT name FROM external_providers AS EP WHERE EP.id=OSH.source) AS source_name,
OSH.independent_stock
FROM orders_send_history AS OSH
JOIN orders_send_history_items AS OSHI
 ON OSH.id = OSHI.send_history_id
LEFT JOIN orders_items AS OI
 ON OI.id = OSHI.item_id
 AND OI.status = '2'
 AND OI.deleted = '0'
 AND OSH.source = OI.source
LEFT JOIN orders AS O
 ON O.id = OI.order_id
 AND O.order_status <> '5'
 AND O.order_status <> '4'
 AND O.order_status <> '3'
WHERE OSH.status = '1'
AND OSH.magazine_status <> '5'
AND (O.id IS NOT NULL OR OSH.independent_stock = '1')
 ORDER BY  OSH.date_send

