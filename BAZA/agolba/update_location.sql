UPDATE products_stock AS PS
JOIN inventory_items AS II
  ON PS.id = II.product_id
JOIN inventory AS I
  ON II.inventory_id = I.id AND I.container_id <> PS.profit_g_location
  AND SUBSTR(I.container_id, 1, 2) = 15
SET
  PS.profit_g_location = I.container_id
WHERE
  PS.id NOT IN
(
    SELECT product_id FROM (
        SELECT II.product_id
        FROM `inventory_items` AS II
        JOIN inventory AS I
            ON I.id = II.inventory_id
        WHERE
        (
            SELECT II2.id
            FROM inventory_items AS II2
            JOIn inventory AS I2
                ON I2.id = II2.inventory_id AND SUBSTR(I2.container_id, 1, 2) = 15
            WHERE II2.id <> II.id  AND II.product_id = II2.product_id AND I.container_id <> I2.container_id AND II2.quantity > 0
            LIMIT 1
        ) IS NOT NULL
        AND SUBSTR(I.container_id, 1, 2) = 15
    GROUP BY II.product_id) AS TMP
)
AND II.quantity > 0;

->

SELECT DISTINCT P.id, P.streamsoft_indeks, PS.profit_g_location, I.container_id 
FROM inventory_items AS II
JOIN products_stock AS PS
  ON PS.id = II.product_id
JOIN inventory AS I
  ON II.inventory_id = I.id AND I.container_id <> PS.profit_g_location
  AND SUBSTR(I.container_id, 1, 2) = 15
JOIN products AS P
    ON PS.id = P.id
WHERE
  PS.id NOT IN
(
    SELECT product_id FROM (
    SELECT II.product_id
    FROM `inventory_items` AS II
    JOIN inventory AS I
        ON I.id = II.inventory_id
    WHERE
    (
        SELECT II2.id
        FROM inventory_items AS II2
        JOIn inventory AS I2
            ON I2.id = II2.inventory_id AND SUBSTR(I2.container_id, 1, 2) = 15
        WHERE II2.id <> II.id  AND II.product_id = II2.product_id AND I.container_id <> I2.container_id AND II2.quantity > 0
        LIMIT 1
    ) IS NOT NULL
    AND SUBSTR(I.container_id, 1, 2) = 15
    GROUP BY II.product_id) AS TMP
)
AND II.quantity > 0;






UPDATE products_stock AS PS
JOIN inventory_items AS II
  ON PS.id = II.product_id
JOIN inventory AS I
  ON II.inventory_id = I.id AND I.container_id <> PS.profit_x_location
  AND SUBSTR(I.container_id, 1, 2) = 11
SET
  PS.profit_x_location = I.container_id
WHERE
  PS.id NOT IN
(
    SELECT product_id FROM (
        SELECT II.product_id
        FROM `inventory_items` AS II
        JOIN inventory AS I
            ON I.id = II.inventory_id
        WHERE
        (
            SELECT II2.id
            FROM inventory_items AS II2
            JOIN inventory AS I2
                ON I2.id = II2.inventory_id AND SUBSTR(I2.container_id, 1, 2) = 11
            WHERE II2.id <> II.id  AND II.product_id = II2.product_id AND I.container_id <> I2.container_id AND II2.quantity > 0
            LIMIT 1
        ) IS NOT NULL
        AND SUBSTR(I.container_id, 1, 2) = 11
        GROUP BY II.product_id
    ) AS TMP
)
AND II.quantity > 0;




-> 
SELECT  PS.profit_x_location, I.container_id 
FROM products_stock AS PS
JOIN inventory_items AS II
  ON PS.id = II.product_id
JOIN inventory AS I
  ON II.inventory_id = I.id AND I.container_id <> PS.profit_x_location
  AND SUBSTR(I.container_id, 1, 2) = 11
WHERE
  PS.id NOT IN
(
    SELECT product_id FROM (
        SELECT II.product_id
        FROM `inventory_items` AS II
        JOIN inventory AS I
            ON I.id = II.inventory_id
        WHERE
        (
            SELECT II2.id
            FROM inventory_items AS II2
            JOIN inventory AS I2
                ON I2.id = II2.inventory_id AND SUBSTR(I2.container_id, 1, 2) = 11
            WHERE II2.id <> II.id  AND II.product_id = II2.product_id AND I.container_id <> I2.container_id AND II2.quantity > 0
            LIMIT 1
        ) IS NOT NULL
        AND SUBSTR(I.container_id, 1, 2) = 11
        GROUP BY II.product_id
    ) AS TMP
)
AND II.quantity > 0;