EXPLAIN
SELECT OI.name, PP.name as publisher, OI.publication_year, COUNT(OI.id) AS orders_count, SUM(OI.quantity),
          PS.profit_g_status, PS.profit_g_reservations,
          PS.profit_e_status,
          PS.profit_j_status,
            PS.platon_wholesale_price,
            P.id as product_id
      FROM orders_items AS OI
      JOIN orders AS O
        ON OI.order_id = O.id AND O.order_status <> "5"
                    AND (O.order_date BETWEEN "2017-08-23 00:00:00" AND O.order_date <= "2017-08-23 23:59:59")
      JOIN products AS P
        ON P.id = OI.product_id
      JOIN products_publishers AS PP
        ON P.publisher_id = PP.id
      JOIN products_stock AS PS
        ON PS.id = P.id
      WHERE 1 = 1 AND
            OI.deleted = "0"
      LIMIT 100



EXPLAIN SELECT "0" as lp, IF(P.ean_13 <> "", P.ean_13, OI.isbn), OI.name, PP.name as publisher, OI.publication_year, COUNT(OI.id) AS orders_count, SUM(OI.quantity),
          PS.profit_g_status, PS.profit_g_reservations,
          PS.profit_e_status,
          PS.profit_j_status,
          IF(PS.azymut_status = "0", "0", PS.azymut_stock) AS azymut_stock,
            PS.azymut_wholesale_price,
          IF(PS.ateneum_status = "0", "0", PS.ateneum_stock) AS ateneum_stock,
            PS.ateneum_wholesale_price,
          IF(PS.siodemka_status = "0", "0", PS.siodemka_stock) AS siodemka_stock,
            PS.siodemka_wholesale_price,
          IF(PS.olesiejuk_status = "0", "0", PS.olesiejuk_stock) AS olesiejuk_stock,
            PS.olesiejuk_wholesale_price,
          IF(PS.dictum_status = "0", "0", PS.dictum_stock) AS dictum_stock,
            PS.dictum_wholesale_price,
          IF(PS.platon_status = "0", "0", PS.platon_stock) AS platon_stock,
            PS.platon_wholesale_price,
            P.id as product_id
      FROM orders_items AS OI
      JOIN orders AS O
        ON OI.order_id = O.id AND O.order_status <> "5"
        AND (O.order_date BETWEEN "2017-08-23 00:00:00" AND O.order_date <= "2017-08-23 23:59:59")

      JOIN products_stock AS PS
        ON PS.id = OI.product_id
      JOIN products AS P
        ON P.id = OI.product_id
      JOIN products_publishers AS PP
        ON P.publisher_id = PP.id

      WHERE 1 = 1 AND
            OI.deleted = "0"
      LIMIT 100