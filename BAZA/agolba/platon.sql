ALTER TABLE `products_stock` ADD `platon_status` CHAR(1) NOT NULL , 
ADD `platon_price_brutto` DECIMAL(8,2) NOT NULL , 
ADD `platon_price_netto` DECIMAL(8,2) NOT NULL , 
ADD `platon_vat` TINYINT NOT NULL , 
ADD `platon_shipment_time` CHAR(1) NOT NULL , 
ADD `platon_last_import` DATETIME NULL DEFAULT NULL , 
ADD `platon_wholesale_price` DECIMAL(8,2) NOT NULL , 
ADD `platon_shipment_date` DATE NULL DEFAULT NULL ;

ALTER TABLE  `key_value` CHANGE  `value`  `value` VARCHAR( 1028 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;

-- taki fixtures, sztucznie uzupełniający mapowania osoby.
INSERT INTO menus_items_mappings
SELECT  "147" AS page_id,  "11" AS source_id, source_category_id
FROM  `menus_items_mappings_categories` 
WHERE source_id =11;


