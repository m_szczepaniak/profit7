CREATE TABLE IF NOT EXISTS `magazine_packages` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(64) NOT NULL,
  `code` VARCHAR(16) NOT NULL,
  `count` INT NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(64) NOT NULL,
  `modified` DATETIME NOT NULL,
  `modified_by` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `magazine_packages_supplies` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `magazine_packages_id` INT UNSIGNED NOT NULL,
  `count` INT UNSIGNED NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_magazine_packages_supplies_magazine_packages1_idx` (`magazine_packages_id` ASC),
  CONSTRAINT `fk_magazine_packages_supplies_magazine_packages1`
    FOREIGN KEY (`magazine_packages_id`)
    REFERENCES `magazine_packages` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
