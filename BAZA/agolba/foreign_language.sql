ALTER TABLE  `products_azymut_buffer` ADD  `foreign_language_book` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `komplet` ;
ALTER TABLE  `products_azymut_buffer_copy` ADD  `foreign_language_book` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `komplet` ;
ALTER TABLE  `products` ADD  `azymut_foreign_language_book` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `azymut_index` ;


-- UWAGA TU id external_providers może isę różnić
INSERT INTO `external_providers` (`id`, `created`, `created_by`, `name`, `symbol`, `icon`, `stock_col`, `border_time`, `mapping_order_by`, `streamsoft_id`, `modified`, `modified_by`) VALUES
(74, '2015-12-02 13:32:01', 'agolba', 'Azymutobc', 'azymutobc', 'gfx/sources/azymut1.png', 'azymut_stock', '17:00:00', 0, 3, '2015-12-02 13:32:31', 'agolba');
INSERT INTO `sources` (`id`, `provider_id`, `symbol`, `column_prefix_products_stock`, `order_by`, `select_privileged`) VALUES (NULL, '74', 'azymutobc', 'azymut', '12', '0');

-- we wszystkich serwisach
INSERT INTO `products_general_discount` (`source`, `discount`, `mark_up`, `recount`, `website_id`) VALUES ('12', '0.01', '99', '0', '0');
INSERT INTO `products_general_discount` (`source`, `discount`, `mark_up`, `recount`) VALUES ('12', '0.01', '99', '0');
INSERT INTO `products_general_discount` (`source`, `discount`, `mark_up`, `recount`) VALUES ('12', '0.01', '99', '0');

ALTER TABLE `products_stock` 
ADD COLUMN `azymutobc_stock` VARCHAR(20) NOT NULL AFTER `platon_shipment_date`,
ADD COLUMN `azymutobc_status` CHAR(1) NOT NULL AFTER `azymutobc_stock`,
ADD COLUMN `azymutobc_price_brutto` DECIMAL(8,2) NOT NULL AFTER `azymutobc_status`,
ADD COLUMN `azymutobc_price_netto` DECIMAL(8,2) NOT NULL AFTER `azymutobc_price_brutto`,
ADD COLUMN `azymutobc_vat` TINYINT(4) NOT NULL AFTER `azymutobc_price_netto`,
ADD COLUMN `azymutobc_shipment_time` CHAR(1) NOT NULL AFTER `azymutobc_vat`,
ADD COLUMN `azymutobc_last_import` DATETIME NULL AFTER `azymutobc_shipment_time`,
ADD COLUMN `azymutobc_wholesale_price` DECIMAL(8,2) NOT NULL AFTER `azymutobc_last_import`,
ADD COLUMN `azymutobc_shipment_date` DATE NULL AFTER `azymutobc_wholesale_price`;


UPDATE `orders_items_lists`
SET
debug = ''
WHERE `debug` != '' AND DATE(created) < "2017-09-01"
LIMIT 100;