ALTER TABLE  `orders_items_lists_items_times` ADD  `type` CHAR( 1 ) NOT NULL DEFAULT  '1' AFTER  `id` ;

// -- usunięcie primary key z id
// ALTER TABLE  `orders_items_lists_items_times` ADD PRIMARY KEY (  `id` ,  `type` ) ;

ALTER TABLE  `orders_items_lists_items_times` ADD  `sc_weight` DECIMAL( 8, 2 ) NOT NULL AFTER  `sc_quantity` ;


CREATE TABLE IF NOT EXISTS `orders_send_history_times` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `orders_send_history_id` INT(10) UNSIGNED NOT NULL,
  `sc_quantity` INT UNSIGNED NOT NULL,
  `sc_weight` DECIMAL(8,2) UNSIGNED NOT NULL,
  `time` BIGINT UNSIGNED NOT NULL,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_send_history_items_times_orders_send_history1_idx` (`orders_send_history_id` ASC),
  INDEX `fk_orders_send_history_items_times_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_orders_send_history_items_times_orders_send_history1`
    FOREIGN KEY (`orders_send_history_id`)
    REFERENCES `orders_send_history` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_send_history_items_times_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

??
-- ALTER TABLE  `orders_send_history` ADD  `date_pz` DATETIME NOT NULL AFTER  `id` ;
