SELECT DISTINCT A.id, website_id, transport_id, A.order_number, email AS user, A.name, surname, (A.value_brutto+transport_cost) AS total_cost, order_status, order_date, activation_date,  payment, dotpay_t_id, CONCAT('dotpay_', dotpay_t_status) dotpay_t_status,
  invoice_id, transport_number, invoice_ready, status_1_update,status_1_update_by,status_2_update,status_2_update_by,status_3_update,status_3_update_by,status_4_update,status_4_update_by,status_5_update,status_5_update_by,user_id,payment_status,payment_date,A.payment_type, paid_amount,to_pay, balance,
  second_payment_enabled, A.second_payment,       second_payment_type, second_payment_id, second_payment_date, second_payment_status, correction, confirm_printout_speditor, internal_status, active,
                                                                GROUP_CONCAT(OD.order_number SEPARATOR ',') AS orders_connected_numbers, GROUP_CONCAT(OD.id SEPARATOR ',') AS orders_connected_ids,
                                                                (SELECT count(id) FROM orders_linked_transport WHERE main_order_id = A.id ) AS is_linked_transport,
  print_ready_list, order_on_list_locked
FROM orders AS A
  LEFT JOIN orders_deleted AS OD
    ON OD.order_id = A.id


WHERE A.order_number <> ''
      AND
      (A.order_number LIKE 'akrzystolik@poczta.onet.pl'
       OR email LIKE 'akrzystolik@poczta.onet.pl'
      ) AND DATE(order_date) >= '2009-11-07' AND DATE(order_date) <= '2018-09-05'
GROUP BY A.id
ORDER BY A.id DESC

LIMIT 0, 20