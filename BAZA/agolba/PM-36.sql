ALTER TABLE sources ADD merlin_source_code VARCHAR(255) NULL;

-- dodać wartości
UPDATE sources SET merlin_source_code = 'ABE01' WHERE symbol = 'abe';
UPDATE sources SET merlin_source_code = 'ATE01' WHERE symbol = 'ateneum';
UPDATE sources SET merlin_source_code = 'AZZ01' WHERE symbol = 'azymut';
UPDATE sources SET merlin_source_code = 'DIC01' WHERE symbol = 'dictum';
UPDATE sources SET merlin_source_code = 'HEL01' WHERE symbol = 'helion';
UPDATE sources SET merlin_source_code = 'PND01' WHERE symbol = 'panda';
UPDATE sources SET merlin_source_code = 'PLA01' WHERE symbol = 'platon';
UPDATE sources SET merlin_source_code = 'SS01' WHERE symbol = 'siodemka';
UPDATE sources SET merlin_source_code = 'KPF01' WHERE symbol = 'Profit E';
