SELECT O.order_number, OI.name
FROM 
`orders_send_history_items` AS OSHI
JOIN orders_send_history AS OSH
  ON OSH.id = OSHI.send_history_id AND OSH.magazine_status = '2'
JOIN orders_send_history_attributes AS OSHA
  ON OSHA.orders_send_history_id = OSHI.send_history_id AND fv_nr <> ''
JOIN orders_items AS OI
  ON OI.id = OSHI.item_id AND OI.status = "2" AND OI.deleted = '0'
JOIN orders AS O
  ON O.id = OI.order_id AND O.order_status <> '4' AND O.order_status <> '5'


