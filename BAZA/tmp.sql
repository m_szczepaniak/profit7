SELECT O.id as oid, O.id AS order_id, O.website_id,  CONCAT(OI.order_id, ",", OI.id) AS ident , OI.id, OI.get_ready_list, OI.quantity, OI.product_id, OI.quantity, OI.deleted, OI.packet, OI.item_type, OI.status, OI.source, OI.name, OI.weight, 
(
  IF (
    OI.source = "51" AND OI.status = "3",
    (SELECT IF(profit_g_location != "" AND profit_x_location != "", CONCAT(profit_x_location, " ", profit_g_location), IF (profit_g_location != "", profit_g_location, profit_x_location)) FROM products_stock AS PS WHERE PS.id = OI.product_id)
    ,
    IF (
      OI.source = "51" AND OI.status = "4", 
      IF (
        (
          SELECT OSH.pack_number
          FROM orders_send_history AS OSH
          JOIN orders_send_history_items AS OSHI
            ON OSH.id = OSHI.send_history_id
          WHERE OSHI.item_id = OI.id AND
                OSH.source IN ("33", "34")
          ORDER BY OSH.id DESC
          LIMIT 1
         ) IS NOT NULL
        ,
        (
          SELECT OSH.pack_number
          FROM orders_send_history AS OSH
          JOIN orders_send_history_items AS OSHI
            ON OSH.id = OSHI.send_history_id
          WHERE OSHI.item_id = OI.id AND
                OSH.source IN ("33", "34")
          ORDER BY OSH.id DESC
          LIMIT 1
         ),
        (
          SELECT package_number
          FROM orders_items_lists AS OIL
          JOIN orders_items_lists_items AS OILI
            ON OILI.orders_items_lists_id = OIL.id
          WHERE OILI.orders_items_id = OI.id AND package_number LIKE "20%"
          ORDER BY OIL.id DESC
          LIMIT 1
        )
       )
    ,
      (
        SELECT OSH.pack_number
        FROM orders_send_history AS OSH
        JOIN orders_send_history_items AS OSHI
          ON OSH.id = OSHI.send_history_id
        WHERE OSHI.item_id = OI.id 
        ORDER BY OSH.id DESC
        LIMIT 1
      )
    )
  )
) AS pack_number, O.order_status, O.linked_order, O.order_number, O.transport_id, O.print_ready_list,  O.point_of_receipt, O.order_on_list_locked, P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13
             FROM orders_items AS OI
             JOIN orders AS O
              ON OI.order_id = O.id
             LEFT JOIN products AS P
              ON P.id = OI.product_id
             WHERE 
              OI.deleted = "0"
              AND
              (
               SELECT id 
               FROM orders_items AS MOI 
               WHERE 
               MOI.order_id = O.id
               AND (MOI.item_type = "I" OR MOI.item_type = "P")
               AND (MOI.weight IS NULL OR MOI.weight = "0.00")
               AND MOI.deleted = "0"
               LIMIT 1
              ) IS NULL
              AND (OI.item_type = "I" OR OI.item_type = "P")
              AND OI.packet = "0"
              AND 
              (
                (
                  (
                    O.payment_type = "bank_transfer" OR
                    O.payment_type = "platnoscipl" OR
                    O.payment_type = "card"
                  )
                  AND O.to_pay <= O.paid_amount
                )
                OR
                (
                  O.payment_type = "postal_fee" OR
                  O.payment_type = "bank_14days"
                )
                OR
                (
                  O.second_payment_type = "postal_fee" OR
                  O.second_payment_type = "bank_14days"
                )
              )
              AND 
              (
                O.order_status = "1" OR
                O.order_status = "2" 
              )
              AND 
              (
                O.remarks = ""
                OR O.remarks IS NULL 
                OR O.remarks_read_1 IS NOT NULL
              )
              AND O.print_ready_list = "0"
              AND 
              ( O.send_date = "0000-00-00" OR (O.send_date <> "0000-00-00" AND O.send_date < NOW()) )
              
             ORDER BY O.magazine_get_ready_list_VIP DESC, O.shipment_date_expected ASC, IF(O.payment_type = "postal_fee", "1", "0") DESC, O.id ASC

