/* solve problem with duplicate of products_merlin_mappings */

DELETE FROM products_merlin_mappings
WHERE id IN (
  SELECT id
  FROM (
         SELECT PMM.id
         FROM products_merlin_mappings AS PMM
         GROUP BY CONCAT(PMM.product_id, '-', PMM.merlin_sku)
         HAVING COUNT(CONCAT(PMM.product_id, '-', PMM.merlin_sku)) > 1
       ) AS TMP
);