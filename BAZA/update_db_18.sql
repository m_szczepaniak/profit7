CREATE TABLE `orders_transport_buffer` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `transport_id` TINYINT(3) UNSIGNED NOT NULL,
  `website_id` INT(10) UNSIGNED NOT NULL,
  `get_deliver` CHAR(1) NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(45) NOT NULL,
  INDEX `fk_orders_transport_buffer_order_id_idx` (`order_id` ASC),
  INDEX `fk_orders_transport_buffer_transport_id_idx` (`transport_id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_orders_transport_buffer_websites1_idx` (`website_id` ASC),
  CONSTRAINT `fk_orders_transport_buffer_order_id`
    FOREIGN KEY (`order_id`)
    REFERENCES `orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_transport_buffer_transport_id`
    FOREIGN KEY (`transport_id`)
    REFERENCES `orders_transport_means` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_transport_buffer_websites1`
    FOREIGN KEY (`website_id`)
    REFERENCES `websites` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE TABLE `orders_packing_times` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `user_id` INT(10) UNSIGNED NOT NULL,
  `time` INT UNSIGNED NOT NULL,
  `sc_confirm` TINYINT UNSIGNED NOT NULL,
  `status` CHAR(1) NOT NULL DEFAULT 0 COMMENT '0 - start skanowania\n9 - zakonczono pakowanie',
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_packing_times_orders1_idx` (`order_id` ASC),
  INDEX `fk_orders_packing_times_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_orders_packing_times_orders1`
    FOREIGN KEY (`order_id`)
    REFERENCES `orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_packing_times_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
