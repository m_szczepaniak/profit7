CREATE TABLE users_groups (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL UNIQUE,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(32) NOT NULL,
  `modified` DATETIME NULL,
  `modified_by` VARCHAR(32) NULL,
  PRIMARY KEY (`id`)
);