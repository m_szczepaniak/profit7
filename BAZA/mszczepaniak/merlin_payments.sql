CREATE TABLE merlin_payments
(
  id int unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
  payment_id int(11),
  bank_statements_id int(11),
  platnoscipl_sessions_id int(11),
  order_id int(10) unsigned,
  sync_status char(1) NOT NULL,
  error_message text,
  CONSTRAINT merlin_payments_orders_payments_list_id_fk FOREIGN KEY (payment_id) REFERENCES orders_payments_list (id) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT merlin_payments_orders_bank_statements_id_fk FOREIGN KEY (bank_statements_id) REFERENCES orders_bank_statements (id) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT merlin_payments_orders_platnoscipl_sessions_id_fk FOREIGN KEY (platnoscipl_sessions_id) REFERENCES orders_platnoscipl_sessions (id) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT merlin_payments_orders_id_fk FOREIGN KEY (order_id) REFERENCES orders (id) ON DELETE SET NULL ON UPDATE SET NULL
);
CREATE UNIQUE INDEX merlin_payments_id_uindex ON merlin_payments (id);

ALTER TABLE orders_payments_list ADD COLUMN merlin_export int unsigned;
ALTER TABLE orders_payments_list ADD CONSTRAINT orders_payments_list_merlin_payments_id_fk FOREIGN KEY (merlin_export) REFERENCES merlin_payments (id);
ALTER TABLE orders_platnoscipl_sessions ADD COLUMN merlin_export int unsigned;
ALTER TABLE orders_platnoscipl_sessions ADD CONSTRAINT orders_platnoscipl_sessions_merlin_payments_id_fk FOREIGN KEY (merlin_export) REFERENCES merlin_payments (id);