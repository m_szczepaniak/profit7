ALTER TABLE orders_to_review
  ADD group_id int(10) UNSIGNED NULL DEFAULT NULL
  AFTER user_id,
  ADD FOREIGN KEY (`group_id`)
REFERENCES `users_groups` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;