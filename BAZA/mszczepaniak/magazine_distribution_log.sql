CREATE TABLE magazine_distribution_log
(
  id int unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
  product_id int(10) unsigned NOT NULL,
  quantity int unsigned NOT NULL,
  location int(10) unsigned NOT NULL,
  user_id int(10) unsigned NOT NULL,
  CONSTRAINT magazine_distribution_log_products_id_fk FOREIGN KEY (product_id) REFERENCES products (id),
  CONSTRAINT magazine_distribution_log_users_id_fk FOREIGN KEY (user_id) REFERENCES users (id)
);
CREATE UNIQUE INDEX magazine_distribution_log_id_uindex ON magazine_distribution_log (id);