ALTER TABLE websites ADD COLUMN merlin_frontend_id int unsigned;

UPDATE websites SET websites.merlin_frontend_id = 100200 WHERE code = 'profit24';
UPDATE websites SET websites.merlin_frontend_id = 100300 WHERE code = 'np';
UPDATE websites SET websites.merlin_frontend_id = 100400 WHERE code = 'mestro';
UPDATE websites SET websites.merlin_frontend_id = 100500 WHERE code = 'smarkacz';
UPDATE websites SET websites.merlin_frontend_id = 100600 WHERE code = 'nb';