CREATE TABLE products_stock_locations_log (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  user_id INT UNSIGNED NOT NULL,
  product_id int(10) UNSIGNED NOT NULL,
  container_id bigint(10) unsigned zerofill,
  orders_items_lists_item_id int(10) unsigned,
  quantity INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_stock_locations_log_user_idx` (`user_id` ASC),
  INDEX `fk_products_stock_locations_log_product_idx` (`product_id` ASC),
  INDEX `fk_products_stock_locations_log_container_idx` (`container_id` ASC),
  INDEX `fk_products_stock_locations_log_orders_items_lists_item_idx` (`orders_items_lists_item_id` ASC),
  CONSTRAINT `fk_products_stock_locations_log_user_idx`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_products_stock_locations_log_product_idx`
  FOREIGN KEY (`product_id`)
  REFERENCES `products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_products_stock_locations_log_container_idx`
  FOREIGN KEY (`container_id`)
  REFERENCES `containers` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_products_stock_locations_log_orders_items_lists_item_idx`
  FOREIGN KEY (`orders_items_lists_item_id`)
  REFERENCES `orders_items_lists_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)