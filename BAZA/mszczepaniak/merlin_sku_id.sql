DROP TABLE products_merlin_mappings;
CREATE TABLE products_merlin_mappings
(
  id int unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
  product_id int(10) unsigned NOT NULL,
  merlin_id int unsigned NOT NULL,
  merlin_sku int unsigned NOT NULL,
  updated_at datetime NOT NULL,
  CONSTRAINT products_merlin_mappings_products_id_fk FOREIGN KEY (product_id) REFERENCES products (id)
);
CREATE UNIQUE INDEX products_merlin_mappings_id_uindex ON products_merlin_mappings (id);