CREATE TABLE users_groups_alert_read (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `alert_id` int(10) UNSIGNED NOT NULL,
  `read` int(1) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_groups_alert_read_user_idx` (`user_id` ASC),
  INDEX `fk_users_groups_alert_read_group_idx` (`alert_id` ASC),
  CONSTRAINT `fk_users_groups_alert_read_user_idx`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_groups_alert_read_group_idx`
  FOREIGN KEY (`alert_id`)
  REFERENCES `orders_to_review` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)