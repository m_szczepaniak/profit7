CREATE TABLE users_to_groups (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  user_id INT UNSIGNED NOT NULL,
  group_id int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_to_groups_user_idx` (`user_id` ASC),
  INDEX `fk_users_to_groups_group_idx` (`group_id` ASC),
  CONSTRAINT `fk_users_to_groups_user_idx`
  FOREIGN KEY (`user_id`)
  REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_users_to_groups_group_idx`
  FOREIGN KEY (`group_id`)
  REFERENCES `users_groups` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
