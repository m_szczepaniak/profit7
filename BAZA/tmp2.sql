INSERT INTO fix_reservation_buffer
(order_number, type)
SELECT O.order_number, "3" AS type
FROM orders AS O
INNER JOIN orders_items AS OI
    ON O.id = OI.order_id
WHERE
 O.order_status IN ("0", "1") AND
 OI.deleted = "0" AND
 OI.source = "51" AND
OI.product_id IN (
    SELECT product_id FROM products_stock_supplies WHERE quantity < reservation
)
GROUP BY OI.product_id



(1612132, 1808957, 356693, 1, '2017-12-14 14:31:10'),
(1615777, 1812632, 356693, 1, '2017-12-14 14:33:57'),
(1617114, 1813974, 356693, 1, '2017-12-14 14:34:25'),
(1621866, 1818735, 356693, 1, '2017-12-14 19:41:01'),
(1623952, 1820813, 356693, 1, '2017-12-15 05:49:01'),
(1630623, 1827558, 356693, 1, '2017-12-15 23:21:01'),
(1634553, 1831476, 407733, 1, '2017-12-17 10:31:02'),
(1636387, 1833308, 407733, 1, '2017-12-17 18:14:02'),
(1639950, 1836865, 407733, 1, '2017-12-18 07:55:02'),
(1641772, 1838688, 356693, 1, '2017-12-18 08:33:04');
