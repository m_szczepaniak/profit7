CREATE TABLE IF NOT EXISTS `orders_deliver_attributes` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `transport_id` TINYINT(3) UNSIGNED NOT NULL,
  `guid` VARCHAR(64) NULL,
  `bufer_id` VARCHAR(64) NULL,
  INDEX `fk_orders_deliver_attributes_orders1_idx` (`order_id` ASC),
  INDEX `fk_orders_deliver_attributes_orders_transport_means1_idx` (`transport_id` ASC),
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_orders_deliver_attributes_orders1`
    FOREIGN KEY (`order_id`)
    REFERENCES `orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_deliver_attributes_orders_transport_means1`
    FOREIGN KEY (`transport_id`)
    REFERENCES `orders_transport_means` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


--
-- Dodać okładkę 
INSERT INTO `orders_transport_means` (`id`, `name`, `symbol`, `logo`, `personal_reception`, `max_weight`, `no_costs_from`, `mail_info`, `date_no_costs_from`, `date_start_date`, `date_end_date`, `order_by`, `border_time_deadline`, `point_of_giving`) VALUES
(4, 'Poczta Polska Odbiór w punkcie', 'poczta_polska', '1c7132d6034a715168b3c18c76e36057.jpg', '0', 999.99, NULL, 'Zamówione książki odbierzesz osobiście w wybranej przez siebie placówce pocztowej', NULL, '2014-11-27 07:00:00', '2014-11-27 23:59:00', 4, 11, '259173');

-- UWAGA W PROFITCIE, NIEPRZECZYTANE i w IT, następnie ustawić loginy i hasla 
CREATE TABLE IF NOT EXISTS `key_value` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `table_name` VARCHAR(255) NOT NULL,
  `id_dest` INT UNSIGNED NOT NULL,
  `key` VARCHAR(128) NOT NULL,
  `value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id` DESC),
  UNIQUE INDEX `security` (`table_name` DESC, `id_dest` DESC, `key` DESC),
  INDEX `get` (`table_name` DESC, `id_dest` DESC))
ENGINE = InnoDB;


--
-- Dodać skrypt uruchamiany codziennie i aktualizujący listę punktów
CREATE TABLE IF NOT EXISTS `shipping_points_poczta` (
  `id` int(10) unsigned NOT NULL,
  `desc` varchar(100) COLLATE utf8_polish_ci NOT NULL,
  `city` varchar(30) COLLATE utf8_polish_ci NOT NULL,
  `street` varchar(50) COLLATE utf8_polish_ci NOT NULL,
  `postcode` int(6) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `details` text COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

