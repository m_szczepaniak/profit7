
-- wykonać dla ksiegarnia it, nieprzeczytane i profit
//////

UPDATE  `sources` SET  `symbol` =  'Profit N' WHERE  `sources`.`id` = 1;
-- dziwne zachowanie
INSERT INTO  `sources` (`id` ,`symbol`) VALUES ('0',  'Profit J'); 
UPDATE  `sources` SET  `id` =  '0' WHERE  `sources`.`id` =10;
INSERT INTO `main_profit24_old`.`sources` (`id`, `symbol`) VALUES ('10', 'Profit G');

-- długi update
ALTER TABLE  `products` CHANGE  `source`  `source` CHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL DEFAULT  '0';
-- przenosimy triggera trig_profit_products_stock_upd

ALTER TABLE  `products_general_discount` CHANGE  `source`  `source` CHAR( 2 ) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL;

INSERT INTO `main_profit24`.`products_general_discount` (`source`, `discount`, `mark_up`, `recount`, `website_id`) VALUES ('10', '21.90', '0.00', '1', '');
INSERT INTO `main_nieprzeczytane`.`products_general_discount` (`source`, `discount`, `mark_up`, `recount`, `website_id`) VALUES ('10', '15.10', '0.00', '1', '');

//////


-- nie potrzebne już
DROP TABLE `products_internal_buffer`;


ALTER TABLE  `products_stock` 
ADD  `profit_g_wholesale_price` DECIMAL( 8, 2 ) NOT NULL AFTER  `profit_g_price_netto`,
ADD  `profit_m_wholesale_price` DECIMAL( 8, 2 ) NOT NULL AFTER  `profit_m_price_netto`,
ADD  `profit_x_wholesale_price` DECIMAL( 8, 2 ) NOT NULL AFTER  `profit_x_price_netto`,
ADD  `profit_e_wholesale_price` DECIMAL( 8, 2 ) NOT NULL AFTER  `profit_e_price_netto`,
ADD  `profit_j_wholesale_price` DECIMAL( 8, 2 ) NOT NULL AFTER  `profit_j_price_netto`;



-- szybkie przeliczenie oferty
UPDATE `products_stock` 
SET id=id
WHERE `profit_g_status` > 0 and profit_g_wholesale_price > 0;



