CREATE TABLE `streamsoft_stock` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`product_id` INT(10) UNSIGNED NOT NULL,
	`purchase_price` DECIMAL(10,2) NULL DEFAULT NULL,
	`quantity` INT(11) NULL DEFAULT NULL,
	`warehouse` INT(11) NULL DEFAULT NULL,
	`quantity_taken` INT(11) NULL DEFAULT NULL,
	`supplier` VARCHAR(50) NULL DEFAULT NULL,
	`supplier_id` INT(11) NULL DEFAULT NULL,
	`document_number` VARCHAR(50) NULL DEFAULT NULL,
	`document_date` DATETIME NULL DEFAULT NULL,
	`document_type` VARCHAR(2) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_streamsoft_stock_products` (`product_id`),
	CONSTRAINT `FK_streamsoft_stock_products` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
