CREATE TABLE `orders_merlin` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`order_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`send_status` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `FK__orders` (`order_id`),
	CONSTRAINT `FK__orders` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
)
ENGINE=InnoDB
;


ALTER TABLE `websites`
	ADD COLUMN `merlin_frontend_id` VARCHAR(6) NULL AFTER `bookstore`;

UPDATE `websites` SET `merlin_frontend_id`='400' WHERE `code`='profit24';
UPDATE `websites` SET `merlin_frontend_id`='403' WHERE `code`='np';
UPDATE `websites` SET `merlin_frontend_id`='402' WHERE `code`='mestro';
UPDATE `websites` SET `merlin_frontend_id`='401' WHERE `code`='smarkacz';
UPDATE `websites` SET `merlin_frontend_id`='404' WHERE `code`='nb';
