CREATE TABLE `orders_transport_means_products` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`orders_transport_means_id` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`product_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	CONSTRAINT `FK_orders_transport_means_products_orders_transport_means`
    FOREIGN KEY (`orders_transport_means_id`)
    REFERENCES `orders_transport_means` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_orders_transport_means_products_products`
    FOREIGN KEY (`product_id`)
    REFERENCES `products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE=InnoDB
AUTO_INCREMENT=1
;