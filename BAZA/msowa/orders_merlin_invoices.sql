CREATE TABLE `orders_merlin_invoices` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`order_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`invoice_url` VARCHAR(100) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `FK__orders__` (`order_id`),
	CONSTRAINT `FK__orders__` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
)
ENGINE=InnoDB
;