UPDATE website_emails SET content = '<p>
<font color="#c30500"><strong><font size="3">Zamówienie wysłane</font> </strong></font>
</p>
<p>
W dniu {data_statusu} Twoje zamówienie nr {nr_zamowienia} zostało wysłane.<br>
{faktura_vat}
</p>
<p>
Szacowany termin dostarczenia, wynosi 1-2 dni robocze od otrzymania tej wiadomości. 
</p>'
WHERE symbol = 'status_zrealizowane';