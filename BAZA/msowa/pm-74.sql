ALTER TABLE `sources`
	ADD COLUMN `name` VARCHAR(40) NOT NULL AFTER `symbol`;

UPDATE sources SET name = symbol;
UPDATE sources SET name = "Merlin Magazyn" WHERE symbol = 'Profit J';
UPDATE sources SET name = "merlin inne" WHERE symbol = 'merlin';