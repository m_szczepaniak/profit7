CREATE TABLE `orders_items_lists_data` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`orders_items_lists_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`orders_items_lists_items_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`stock_location_orders_items_lists_items_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`user_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `id` (`id`),
	CONSTRAINT `FK_orders_items_lists_data_orders_items_lists`
    FOREIGN KEY (`orders_items_lists_id`)
    REFERENCES `orders_items_lists` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_orders_items_lists_data_orders_items_lists_items`
    FOREIGN KEY (`orders_items_lists_items_id`)
    REFERENCES `orders_items_lists_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_orders_items_lists_data_stock_location`
    FOREIGN KEY (`stock_location_orders_items_lists_items_id`)
    REFERENCES `stock_location_orders_items_lists_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_orders_items_lists_data_users`
    FOREIGN KEY (`user_id`)
    REFERENCES `users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)
ENGINE=InnoDB
AUTO_INCREMENT=1
;

ALTER TABLE `containers`
	ADD COLUMN `area` INT NULL AFTER `magazine_type`;

/* Strefa 1 */
UPDATE containers
SET area = 1
WHERE id >= 2000001010 AND id <= 2000028070;
/* Strefa 2 */
UPDATE containers
SET area = 2
WHERE id >= 1501001010 AND id <= 1502032070;
/* Strefa 3 */
UPDATE containers
SET area = 3
WHERE id >= 1503001010 AND id <= 1504034070;
/* Strefa 4 */
UPDATE containers
SET area = 4
WHERE id >= 1505001010 AND id <= 1506038070;
/* Strefa 5 */
UPDATE containers
SET area = 5
WHERE id >= 1507001010 AND id <= 1509032055;

/* Palety */
INSERT INTO containers
SET
locked = 0,
type = 2,
available_for_employee = 1,
magazine_type = 'SL',
area = 2,
id = 1501001009;

INSERT INTO containers
SET
locked = 0,
type = 2,
available_for_employee = 1,
magazine_type = 'SL',
area = 3,
id = 1503001009;

INSERT INTO containers
SET
locked = 0,
type = 2,
available_for_employee = 1,
magazine_type = 'SL',
area = 4,
id = 1505001009;

INSERT INTO containers
SET
locked = 0,
type = 2,
available_for_employee = 1,
magazine_type = 'SL',
area = 5,
id = 1507001009;