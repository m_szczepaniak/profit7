-- INSERT INTO `external_providers` (`id`, `created`, `created_by`, `name`, `symbol`, `icon`, `stock_col`, `border_time`, `mapping_order_by`, `modified`, `modified_by`) 
-- VALUES ('51', '2015-02-11 00:00:00', 'agolba', 'Profit G', 'profit_g', '', 'profit_g', NULL, '9', '', '');
-- 
-- 
-- 
-- INSERT INTO `external_providers` (`id`, `created`, `created_by`, `name`, `symbol`, `icon`, `stock_col`, `border_time`, `mapping_order_by`, `modified`, `modified_by`) 
-- VALUES ('0', '2015-02-11 00:00:00', 'agolba', 'Profit J', 'profit_j', '', 'profit_j', NULL, '9', '', '');
-- 
-- -- uwaga pewnie profit J nieprawidłowo się doda
-- UPDATE  `external_providers` SET  `id` =  '0' WHERE  `external_providers`.`id` =52;
-- 
-- INSERT INTO `external_providers` (`id`, `created`, `created_by`, `name`, `symbol`, `icon`, `stock_col`, `border_time`, `mapping_order_by`, `modified`, `modified_by`) 
-- VALUES ('1', '2015-02-11 00:00:00', 'agolba', 'Profit N', 'profit_e', '', 'profit_e', NULL, '9', '', '');
-- 


CREATE TABLE IF NOT EXISTS `orders_change_history` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` INT UNSIGNED NOT NULL,
  `user_id` TINYINT NOT NULL,
  `created` INT NOT NULL,
  `content` TEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `order` (`order_id` ASC))
ENGINE = InnoDB;
