1!!!BACKUP )

-- porządki
DELETE FROM `events_logs_orders` WHERE `created` <= '2014-05-01 00:00:00';
OPTIMIZE TABLE  `events_logs_orders`;

/* uwaga tu może być inna liczba */
DELETE FROM orders_debug WHERE oid <= 131055; 

OPTIMIZE TABLE  `orders_debug`;


DROP TABLE  `products_shadow_05102012`;

set foreign_key_checks = 0;
DROP TABLE `products_stock_02102012`;
DROP TABLE `products_extra_categories_copy`;
set foreign_key_checks = 1;

set foreign_key_checks = 0;
UPDATE orders_send_history SET content = '' WHERE `date_send` <= '2014-05-01 00:00:00';
OPTIMIZE TABLE orders_send_history;
set foreign_key_checks = 1;

set foreign_key_checks = 0;
DROP TABLE `abc_baza_ks`;
set foreign_key_checks = 1;

set foreign_key_checks = 0;
DROP TABLE `beck_products`, `beck_products_attachments`, `beck_products_authors`, `beck_products_authors_roles`, `beck_products_images`, `beck_products_series`, `beck_products_to_authors`;
set foreign_key_checks = 1;

DELETE FROM  `search_phrases` WHERE  `counter` <3;
OPTIMIZE TABLE search_phrases;
OPTIMIZE TABLE products_shadow;



-- odpublikowanie "na zamówienie ABE"
UPDATE products
SET 
published = '0',
modified_by = 'abe_auto_unp'
WHERE 
published = '1' AND
shipment_time = '3' AND
source = '3';

DELETE FROM products_shadow
WHERE 
id IN (
  SELECT id from products
  WHERE
  published = '0' AND
  shipment_time = '3' AND
  source = '3'
);




/*
 -- to już zrobione
UPDATE orders
SET shipment_date_expected = shipment_date
WHERE 
order_status <> '4' 
AND order_status <> '5'
AND shipment_date_expected IS NULL;

UPDATE orders
SET shipment_date_expected = send_date
WHERE 
order_status <> '4' 
AND order_status <> '5'
AND send_date <> '0000-00-00' 
AND send_date IS NOT NULL;
*/


INSERT INTO `external_providers` (`id`, `created`, `created_by`, `name`, `symbol`, `icon`, `stock_col`, `border_time`, `mapping_order_by`, `modified`, `modified_by`) VALUES
(33, '2014-07-04 14:43:15', 'agolba', 'Tramwaj', 'tramwaj', '', NULL, NULL, 0, '0000-00-00 00:00:00', '');





-- ok lecimy


ALTER TABLE  `orders_send_history` ADD INDEX (  `status` );
ALTER TABLE  `orders_items` ADD INDEX  `source` (  `status` ,  `deleted` ,  `source` );

-- powinno zadziałać
ALTER TABLE  `orders_send_history` 
ADD  `post_array` TEXT NOT NULL AFTER  `send_by`,
ADD  `pack_number` BIGINT NULL DEFAULT NULL AFTER  `number`,
ADD  `post_status` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `post_array`;



ALTER TABLE  `orders_items` ADD  `get_ready_list` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `sent_hasnc`;
ALTER TABLE  `orders` ADD  `print_ready_list` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `locked_auto_order`;


ALTER TABLE  `orders` ADD INDEX (  `payment_type` ,  `order_status` ,  `paid_amount` ,  `to_pay` ,  `send_date` );


ALTER TABLE  `products_stock` ADD PRIMARY KEY (  `id` );



 -- warto będzie to przenieść do NP i IT
ALTER TABLE  `products_stock` ADD  `profit_g_act_stock` MEDIUMINT NOT NULL DEFAULT  '0' COMMENT  'aktualny stan w WF-MAG' AFTER  `profit_g_status` ,
ADD  `profit_g_reservations` SMALLINT NOT NULL DEFAULT  '0' COMMENT  'rezerwacje na G' AFTER  `profit_g_act_stock`;


CREATE TABLE IF NOT EXISTS `main_profit24`.`orders_linked_transport` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `main_order_id` INT(10) UNSIGNED NOT NULL,
  `linked_order_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_linked_transport_orders1_idx` (`main_order_id` ASC),
  INDEX `fk_orders_linked_transport_orders2_idx` (`linked_order_id` ASC),
  CONSTRAINT `fk_orders_linked_transport_orders1`
    FOREIGN KEY (`main_order_id`)
    REFERENCES `main_profit24`.`orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_linked_transport_orders2`
    FOREIGN KEY (`linked_order_id`)
    REFERENCES `main_profit24`.`orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

ALTER TABLE  `orders_linked_transport` ADD UNIQUE (
`main_order_id` ,
`linked_order_id`
);



CREATE TABLE IF NOT EXISTS `main_profit24`.`products_stock_reservations` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_stock_id` INT(10) UNSIGNED NOT NULL,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `orders_items_id` INT(10) UNSIGNED NOT NULL,
  `quantity` MEDIUMINT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_stock_reservations_products_stock1_idx` (`products_stock_id` ASC),
  INDEX `fk_products_stock_reservations_orders_items1_idx` (`orders_items_id` ASC),
  INDEX `fk_products_stock_reservations_orders1_idx` (`order_id` ASC),
  UNIQUE INDEX `orders_items_id_UNIQUE` (`orders_items_id` ASC),
  CONSTRAINT `fk_products_stock_reservations_products_stock1`
    FOREIGN KEY (`products_stock_id`)
    REFERENCES `main_profit24`.`products_stock` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_products_stock_reservations_orders_items1`
    FOREIGN KEY (`orders_items_id`)
    REFERENCES `main_profit24`.`orders_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_products_stock_reservations_orders1`
    FOREIGN KEY (`order_id`)
    REFERENCES `main_profit24`.`orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `main_profit24`.`orders_items_lists` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT(10) UNSIGNED NULL,
  `document_number` VARCHAR(32) NOT NULL COMMENT 'numer dokumentu',
  `package_number` VARCHAR(32) NOT NULL DEFAULT 0,
  `closed` CHAR(1) NOT NULL DEFAULT 0,
  `is_g` CHAR(1) NOT NULL DEFAULT 0,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_items_lists_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_orders_items_lists_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `main_profit24`.`users` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `main_profit24`.`orders_items_lists_items` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `orders_items_lists_id` INT UNSIGNED NOT NULL,
  `orders_items_id` INT(10) UNSIGNED NOT NULL,
  `orders_id` INT(10) UNSIGNED NOT NULL,
  `product_id` INT(10) UNSIGNED NOT NULL,
  `quantity` INT NOT NULL,
  `shelf_number` VARCHAR(32) NOT NULL COMMENT 'numer półki na sortowniku',
  `confirmed_quantity` INT NOT NULL,
  `confirmed` CHAR(1) NOT NULL DEFAULT 0,
  `isbn_plain` CHAR(20) NOT NULL,
  `isbn_10` CHAR(10) NULL,
  `isbn_13` CHAR(45) NULL,
  `ean_13` CHAR(45) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_items_lists_items_orders_items_lists1_idx` (`orders_items_lists_id` ASC),
  INDEX `fk_orders_items_lists_items_orders_items1_idx` (`orders_items_id` ASC),
  INDEX `fk_orders_items_lists_items_orders1_idx` (`orders_id` ASC),
  INDEX `isbn` (`isbn_plain` ASC),
  INDEX `isbn_10` (`isbn_10` ASC),
  INDEX `isbn_13` (`isbn_13` ASC),
  INDEX `ena_13` (`ean_13` ASC),
  CONSTRAINT `fk_orders_items_lists_items_orders_items_lists1`
    FOREIGN KEY (`orders_items_lists_id`)
    REFERENCES `main_profit24`.`orders_items_lists` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_items_lists_items_orders_items1`
    FOREIGN KEY (`orders_items_id`)
    REFERENCES `main_profit24`.`orders_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_orders_items_lists_items_orders1`
    FOREIGN KEY (`orders_id`)
    REFERENCES `main_profit24`.`orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;



-- TODO REZERWACJE

-- REZERWACJE
  INSERT INTO products_stock_reservations 
  SELECT 0 as id, OI.product_id, OI.order_id, OI.id AS orders_items_id, OI.quantity
  FROM orders_items AS OI
  JOIN orders AS O
    ON O.id = OI.order_id
  WHERE 
    OI.source = "51" AND
    OI.status = "3" AND
    OI.deleted = "0" AND
    OI.get_ready_list = "0" AND
    OI.deleted = "0" AND
    (OI.item_type = "I" OR OI.item_type = "P") AND
    OI.packet = "0" AND
    O.order_status <> "4" AND 
    O.order_status <> "5"