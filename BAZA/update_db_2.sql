ALTER TABLE  `orders` ADD  `dont_cancel` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `opineo_sent`;
ALTER TABLE  `orders` ADD  `invoice_remarks` VARCHAR( 512 ) NOT NULL DEFAULT  '' AFTER  `remarks`;

ALTER TABLE  `users` ADD  
`continue_session` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `priv_order_5_status`,
ADD  `ip_address` VARCHAR( 140 ) NOT NULL DEFAULT  '' AFTER  `continue_session`;