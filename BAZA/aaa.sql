INSERT INTO fix_reservation_buffer
(order_number, type)
    SELECT O.order_number, "2" AS type
    FROM `products_stock_supplies` AS PSS
    JOIN products_stock_supplies_to_reservations AS PSSTR
        ON PSS.id = PSSTR.products_stock_supplies_id
    JOIN products_stock_reservations AS PSR
        ON PSR.id = PSSTR.products_stock_reservations_id
    JOIN orders_items AS OI
        ON OI.id = PSR.order_id
    JOIN orders AS O
        ON O.id = PSR.order_id AND O.order_status IN ("0", "1", "2")
    WHERE PSS.reservation > PSS.quantity
    GROUP BY PSS.id


/*
INSERT INTO fix_reservation_buffer
(order_number, type)
SELECT   DISTINCT O.order_number, PS.profit_g_reservations AS type
FROM  `products_stock_reservations` AS PSR
JOIN products_stock AS PS
  ON PS.id = PSR.products_stock_id
JOIN products AS P
    ON P.id = PS.id
JOIN orders AS O
    ON O.id = PSR.order_id AND O.order_status IN ("0", "1", "2")
WHERE PSR.source_id = '10'
GROUP BY products_stock_id
HAVING SUM( PSR.quantity ) <> PS.profit_g_reservations
*/




INSERT INTO fix_reservation_buffer
(order_number, type)
SELECT O.order_number, "1" AS type
FROM `products_stock_supplies` AS PSS
JOIN products_stock_supplies_to_reservations AS PSSTR
    ON PSS.id = PSSTR.products_stock_supplies_id
JOIN products_stock_reservations AS PSR
    ON PSR.id = PSSTR.products_stock_reservations_id
JOIN orders_items AS OI
    ON OI.id = PSR.order_id
JOIN orders AS O
    ON O.id = PSR.order_id AND O.order_status IN ("0", "1", "2")
WHERE PSS.reservation <>
(
  SELECT SUM(reservated_quantity) FROM
  products_stock_supplies_to_reservations AS PSSTR
  WHERE PSS.id = PSSTR.products_stock_supplies_id
)
GROUP BY PSS.id
















{"before":{"id":"267287","product_id":"985256","source":"10","quantity":"40","type":"FA","wholesale_price":"40.95","price_brutto":"63.00","price_netto":"60.00","vat":"5","reservation":"40","reservation_erp":"40","reservation_to_reduce":"0","last_update":"2017-03-22 15:02:19"},"update":{"delete_supplies":"delete","data":{"id":"267287","product_id":"985256","source":"10","quantity":"40","type":"FA","wholesale_price":"40.95","price_brutto":"63.00","price_netto":"60.00","vat":"5","reservation":"40","reservation_erp":"40","reservation_to_reduce":"0","last_update":"2017-03-22 15:02:19"}},"0":[{"file":"/var/www/vhosts/profit24.pl/httpdocs/import/elements/internal/statusSupplyUpdater.class.php","line":159,"function":"insertProductsStockSuppliesDebug","class":"LIB\communicator\sources\Internal\streamsoft\DataProvider","type":"->"},{"file":"/var/www/vhosts/profit24.pl/httpdocs/import/elements/internal/statusSupplyUpdater.class.php","line":71,"function":"prepareProductsToDelete","class":"statusSupplyUpdater","type":"->"},{"file":"/var/www/vhosts/profit24.pl/httpdocs/import/elements/internal/statusElements.class.php","line":556,"function":"doRecountProductStockSupply","class":"statusSupplyUpdater","type":"->"},{"file":"/var/www/vhosts/profit24.pl/httpdocs/import/internalImportStatus.class.php","line":73,"function":"doRecountProductStockSupply","class":"statusElements","type":"->"}]}

241011846517
stany - 161215961317

SELECT * FROM products_stock WHERE profit_g_status <> profit_g_act_stock - profit_g_reservations;

INSERT INTO fix_reservation_buffer
(order_number, type)
SELECT
(
  SELECT DISTINCT O.order_number
  FROM products_stock_supplies_to_reservations AS PSSTR
  JOIN products_stock_reservations AS PSR
    ON PSR.id = PSSTR.products_stock_reservations_id
  JOIN orders AS O
    ON O.id = PSR.order_id
  WHERE PSSTR.products_stock_supplies_id = PSS.id
  ORDER BY O.id DESC
  LIMIT 1
)
, "2" AS type
FROM `products_stock_supplies` AS PSS
JOIN products_stock_supplies_to_reservations AS PSSTR
  ON PSSTR.products_stock_supplies_id = PSS.id
JOIN products_stock_reservations AS PSR
  ON PSR.id = PSSTR.products_stock_reservations_id
JOIN orders AS O
  ON O.id = PSR.order_id
WHERE PSS.reservation > PSS.quantity




SELECT
O.order_number
, "2" AS type
FROM `products_stock_supplies` AS PSS
JOIN products_stock_supplies_to_reservations AS PSSTR
  ON PSSTR.products_stock_supplies_id = PSS.id
JOIN products_stock_reservations AS PSR
  ON PSR.id = PSSTR.products_stock_reservations_id
JOIN orders AS O
  ON O.id = PSR.order_id
WHERE PSS.reservation > PSS.quantity


SELECT  O.id, P.name, O.order_number, PS.id AS PS_ID, SUM( PSR.quantity ), PS.profit_g_reservations
FROM  `products_stock_reservations` AS PSR
JOIN products_stock AS PS
  ON PS.id = PSR.products_stock_id
JOIN products AS P
    ON P.id = PS.id
JOIN orders AS O
    ON O.id = PSR.order_id
WHERE PSR.source_id = '10'
GROUP BY products_stock_id
HAVING SUM( PSR.quantity ) <> PS.profit_g_reservations



SELECT *,
(
  SELECT SUM(reservated_quantity) FROM
  products_stock_supplies_to_reservations AS PSSTR
  WHERE PSS.id = PSSTR.products_stock_supplies_id
)

FROM `products_stock_supplies` AS PSS
WHERE PSS.reservation <>
(
  SELECT SUM(reservated_quantity) FROM
  products_stock_supplies_to_reservations AS PSSTR
  WHERE PSS.id = PSSTR.products_stock_supplies_id
)

ORDER BY `source`  DESC





SELECT PS.*, O.id AS order_id, OI.quantity, OI.promo_price_brutto, O.order_number, O.shipment_date, OI.product_id, P.name, P.isbn_plain, P.publisher_id, PP.check_external_border_time, PP.omit_internal_provider, OI.id AS id,
    IF (
      ((ADDDATE('2017-12-13', INTERVAL 1 DAY) = O.shipment_date) AND '2017-12-13' = CURDATE()),
      '1',
      IF (
        (ADDDATE('2017-12-13', INTERVAL 1 DAY) < O.shipment_date OR (ADDDATE('2017-12-13', INTERVAL 1 DAY) <= O.shipment_date AND 2017-12-13 <> CURDATE())),
        '2',
        '0'
      )
    ) AS have_additional_day
             FROM orders_items AS OI
             JOIN orders AS O
              ON OI.order_id = O.id
             JOIN products AS P
              ON P.id = OI.product_id AND P.prod_status = '1'
             JOIN products_stock AS PS
              ON PS.id = P.id
             LEFT JOIN products_publishers AS PP
              ON PP.id = P.publisher_id
             WHERE
              O.order_status = '0' AND
              O.internal_status = '1' AND
               O.locked_auto_order = '0' AND

              OI.status = '0' AND
              OI.deleted <> '1' AND
              (
                (OI.item_type = 'I' AND OI.packet = '0')
                  OR
                 OI.item_type = 'P'
               )
               AND O.id = 1138265


              AND
              (
                O.payment_status = '1'
                OR
                (
                 SELECT O_TEST.id
                 FROM orders AS O_TEST
                 WHERE
                   O_TEST.id <> O.id
                   AND (
                      O_TEST.client_ip = O.client_ip OR
                      O_TEST.email = O.email
                   )
                   AND (
                     O_TEST.order_status = '0' OR
                     O_TEST.order_status = '1')
                   LIMIT 1
                 ) IS NULL
              )