
SELECT *  
FROM `products` AS P
JOIN orders_items AS OI
ON OI.product_id = P.id
WHERE 
P.azymut_index <> ''
AND P.azymut_index NOT LIKE '%KS' 
AND P.azymut_index NOT LIKE '%CD' 
AND P.azymut_index NOT LIKE '%KL'  
AND P.azymut_index NOT LIKE '%AK'  
AND P.azymut_index NOT LIKE '%AT'  
AND P.azymut_index NOT LIKE '%MP'  
AND P.azymut_index NOT LIKE '%DV'  
AND P.azymut_index NOT LIKE '%00'
AND P.azymut_index NOT LIKE '%GR'
AND P.azymut_index NOT LIKE '%PD'
AND P.azymut_index NOT LIKE '%CZ'
AND P.azymut_index NOT LIKE '%NT'
AND P.azymut_index NOT LIKE '%KA'
AND P.azymut_index NOT LIKE '%AL'
AND P.azymut_index NOT LIKE '%ME'
AND P.azymut_index NOT LIKE '%DS'
AND P.azymut_index NOT LIKE '%MC'
AND P.azymut_index NOT LIKE '%AP'
AND P.azymut_index NOT LIKE '%MR'


AND P.azymut_index NOT LIKE '%KV'

AND P.id NOT IN (
628068,
690619,
684641,
605393,
605388
);


SELECT * 
FROM products AS P
LEFT JOIN products_extra_categories AS EC
ON EC.product_id = P.id
 AND EC.page_id <> '1'
 AND EC.page_id <> '147'
 AND EC.page_id <> '261'
WHERE EC.page_id  IS NULL