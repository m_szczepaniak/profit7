ALTER TABLE  `promotions_codes` ADD  `website_id` INT UNSIGNED NULL DEFAULT NULL AFTER  `id`, ADD INDEX (  `website_id` );
ALTER TABLE  `promotions_codes` ADD FOREIGN KEY (  `website_id` ) REFERENCES  `websites` (
`id`
) ON DELETE CASCADE ON UPDATE RESTRICT ;

ALTER TABLE  `products` ADD  `description_on_create` MEDIUMTEXT NULL AFTER  `description` ;


ALTER TABLE  `orders_send_history_additional_items` ADD  `defect` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `streamsoft_id`;
ALTER TABLE  `orders_send_history_additional_items` ADD  `taken` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `defect`;


ALTER TABLE  `orders_send_history_items` ADD  `product_id` INT UNSIGNED NULL DEFAULT NULL AFTER  `vat` ,
ADD INDEX (  `product_id` );
ALTER TABLE  `orders_send_history_items` CHANGE  `item_id`  `item_id` INT( 11 ) NULL DEFAULT NULL ;
ALTER TABLE  `orders_send_history` ADD  `independent_stock` CHAR( 1 ) NOT NULL DEFAULT  '0' AFTER  `omit_create_doc_streamsoft` ;

alter table users_accounts
add `user_confirmed` TINYINT(1) NOT NULL DEFAULT '0',
add `activation_code` VARCHAR(32) NOT NULL,
add `activation_code_sent_at` DATETIME NOT NULL;