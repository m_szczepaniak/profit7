SELECT * 
FROM  `orders_send_history_items` AS OSHI
JOIN orders_items AS OI ON OI.id = OSHI.item_id
JOIN orders_send_history AS OSH ON OSH.id = OSHI.send_history_id
WHERE OI.status =  '2'
AND OSH.pack_number <>  ''
AND OI.deleted =  '0'
ORDER BY OSHI.id DESC 
LIMIT 20
