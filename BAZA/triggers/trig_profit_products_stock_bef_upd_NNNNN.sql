BEGIN

	IF (NEW.profit_g_last_import != OLD.profit_g_last_import) THEN
	 IF (NEW.profit_g_status != (NEW.profit_g_act_stock - NEW.profit_g_reservations)) THEN
	  SET NEW.profit_g_status = (NEW.profit_g_act_stock - NEW.profit_g_reservations);
	 END IF;
	END IF;

	IF (NEW.profit_j_last_import != OLD.profit_j_last_import) THEN
	 IF (NEW.profit_j_status != (NEW.profit_j_act_stock - NEW.profit_j_reservations)) THEN
	  SET NEW.profit_j_status = (NEW.profit_j_act_stock - NEW.profit_j_reservations);
	 END IF;
	END IF;

	IF (NEW.profit_e_last_import != OLD.profit_e_last_import) THEN
	 IF (NEW.profit_e_status != (NEW.profit_e_act_stock - NEW.profit_e_reservations)) THEN
	  SET NEW.profit_e_status = (NEW.profit_e_act_stock - NEW.profit_e_reservations);
	 END IF;
	END IF;


    IF ( NEW.profit_g_location <> OLD.profit_g_location ) THEN
        INSERT INTO products_stock_locations
        SET
            products_stock_id = NEW.id,
            new_g_location = NEW.profit_g_location,
            old_g_location = OLD.profit_g_location,
            last_update = NOW();
    END IF;

    IF ( NEW.profit_x_location <> OLD.profit_x_location ) THEN
        INSERT INTO products_stock_locations
        SET
            products_stock_id = NEW.id,
            new_x_location = NEW.profit_x_location,
            old_x_location = OLD.profit_x_location,
            last_update = NOW();
    END IF;
END