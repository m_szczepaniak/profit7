trig_orders_upd	UPDATE	orders	"BEGIN
DECLARE total_value DECIMAL(8,2);
DECLARE new_to_pay DECIMAL(8,2);
DECLARE order_balance DECIMAL(8,2);
IF (NEW.value_brutto <> OLD.value_brutto) OR (NEW.transport_cost <> OLD.transport_cost) OR (NEW.paid_amount <> OLD.paid_amount) OR (NEW.order_status <> OLD.order_status) THEN
  SELECT (NEW.value_brutto + NEW.transport_cost) INTO total_value;
  SELECT total_value INTO new_to_pay;
  SET NEW.to_pay = total_value;
  IF (NEW.paid_amount > 0) THEN
    SELECT NEW.paid_amount - total_value INTO order_balance;
    SET NEW.balance = order_balance;
  ELSEIF (OLD.paid_amount > 0) THEN
    SET NEW.balance = 0;
  END IF;
END IF;
IF ((NEW.order_status <> OLD.order_status AND NEW.order_status = '5') OR (NEW.order_status = '5' AND NEW.paid_amount <> OLD.paid_amount)) THEN
  SET NEW.balance = NEW.paid_amount;
END IF;


IF (NEW.order_status <> OLD.order_status) THEN
IF (NEW.order_status = '5') THEN

DELETE FROM products_stock_reservations WHERE products_stock_reservations.order_id = NEW.id;
END IF;
END IF;

END"	BEFORE	2019-02-06 14:35:18.75		admin58727_prof2@%	utf8	utf8_general_ci	utf8_general_ci
trig_profit_products_bef_ins	INSERT	products	"BEGIN

  SET NEW.description_on_create = NEW.description;

  IF (NEW.prod_status = '0' OR NEW.prod_status = '2') THEN
   SET NEW.last_change_unavailable = CURDATE();
  END IF;

  SET NEW.streamsoft_indeks = CASE WHEN NEW.ean_13 IS NOT NULL THEN NEW.ean_13
                  WHEN NEW.isbn_13 IS NOT NULL THEN NEW.isbn_13
                  WHEN NEW.isbn_plain IS NOT NULL AND NEW.isbn_plain <> '' AND NEW.isbn_plain <> '0000000000000' THEN NEW.isbn_plain
  END;

    IF (NEW.`type` = '1') THEN
        SET NEW.product_type = 'A';
    END IF;
    IF (NEW.`product_type` = 'A') THEN
        SET NEW.`type` = '1';
    END IF;
END"	BEFORE	2019-02-06 14:57:44.38		admin58727_prof2@%	utf8	utf8_unicode_ci	utf8_general_ci
trig_profit_products_aft_ins	INSERT	products	"BEGIN


  DECLARE new_general_discount DECIMAL(8,2);
  DECLARE new_mark_up DECIMAL(8,2);
  DECLARE new_price_netto DECIMAL(8,2);
  DECLARE publisher_discount_limt DECIMAL(8,2);
  DECLARE series_discount_limt DECIMAL(8,2);
  DECLARE source_discount_limt DECIMAL(8,2);
  DECLARE att_price_netto DECIMAL(8,2);

  SELECT discount, mark_up INTO new_general_discount, new_mark_up FROM products_general_discount WHERE source = NEW.source LIMIT 0,1;

  SELECT price_netto INTO att_price_netto FROM products_attachments WHERE product_id = NEW.id;
  IF ((att_price_netto IS NULL OR att_price_netto = 0) AND new_mark_up > 0 AND NEW.wholesale_price > 0) THEN

    SELECT (100 - ((((NEW.wholesale_price * (new_mark_up / 100)) + NEW.wholesale_price) / NEW.price_brutto) * 100)) INTO new_general_discount;
  END IF;

  IF NEW.price_brutto IS NOT NULL THEN

  IF NEW.vat > 0 THEN
  SELECT ROUND(NEW.price_brutto / (1 + NEW.vat/100), 2) INTO new_price_netto;
  ELSE
  SELECT NEW.price_brutto INTO new_price_netto;
  END IF;



  IF (NEW.packet='0') THEN

   SELECT IFNULL(discount_limit,0) INTO publisher_discount_limt FROM products_publishers WHERE id = NEW.publisher_id;
   SELECT IFNULL(PS.discount_limit,0) INTO series_discount_limt FROM products_series AS PS
     JOIN products_to_series AS PTS ON
         PS.id = PTS.series_id
     WHERE PS.publisher_id = NEW.publisher_id AND PTS.product_id = NEW.id
     LIMIT 1;
   SELECT IFNULL(discount_limit,0) INTO source_discount_limt FROM products_source_discount WHERE first_source = NEW.created_by;


   IF (publisher_discount_limt > 0) AND (new_general_discount > publisher_discount_limt) THEN
   SELECT publisher_discount_limt INTO new_general_discount;
   END IF;

   IF (source_discount_limt > 0) AND (new_general_discount > source_discount_limt) THEN
   SELECT source_discount_limt INTO new_general_discount;
   END IF;

   IF (series_discount_limt > 0) AND (new_general_discount > series_discount_limt) THEN
   SELECT series_discount_limt INTO new_general_discount;
   END IF;

  END IF;


  INSERT INTO products_tarrifs (product_id, price_brutto, price_netto, isdefault)
  VALUES (NEW.id, NEW.price_brutto, new_price_netto, '1');

  INSERT INTO products_tarrifs (product_id, discount, discount_value, price_brutto, price_netto, general_discount)
  VALUES (NEW.id,
         new_general_discount,
         ROUND(NEW.price_brutto*(new_general_discount/100), 2),
         NEW.price_brutto-ROUND(NEW.price_brutto*(new_general_discount/100), 2),
         ROUND((NEW.price_brutto-ROUND(NEW.price_brutto*(new_general_discount/100), 2)) / (1 + NEW.vat/100), 2),
         '1');
  ELSE

  INSERT INTO products_tarrifs (product_id, price_brutto, price_netto, isdefault) VALUES (NEW.id, 0.0, 0.0, '1');

  IF (NEW.packet='0') THEN

    INSERT INTO products_tarrifs (product_id, discount, discount_value, price_brutto, price_netto, general_discount)
           VALUES (NEW.id,
               new_general_discount,
               0,
               0,
               0,
               '1');
  ELSE

    INSERT INTO products_tarrifs (product_id, discount, discount_value, price_brutto, price_netto, general_discount)
           VALUES (NEW.id,
               0,
               0,
               0,
               0,
               '1');
  END IF;

  END IF;

  INSERT INTO products_stock (id) VALUES (NEW.id);
  INSERT INTO products_last_update (product_id) VALUES (NEW.id);
          INSERT INTO product_changes_data (product_id, has_changed, elastic_has_changed) VALUES (NEW.id, 1, 0) ON DUPLICATE KEY UPDATE product_id = NEW.id, has_changed = 1, elastic_has_changed = 0, updated = NOW();
END"	AFTER	2019-02-06 14:57:44.38		admin58727_prof2@%	utf8mb4	utf8mb4_general_ci	utf8_general_ci
trig_profit_products_bef_upd	UPDATE	products	"BEGIN

    DECLARE NP_P_ID DECIMAL(8,2);
    DECLARE ME_P_ID DECIMAL(8,2);
    DECLARE SM_P_ID DECIMAL(8,2);
    DECLARE NB_P_ID DECIMAL(8,2);

    DECLARE def_product_type CHAR(1) DEFAULT 'K';

    IF (NEW.source <> OLD.source) OR (NEW.publisher_id <> OLD.publisher_id) THEN
    SET NEW.last_modified_price = '1';
    END IF;


    IF (NEW.price_brutto <> OLD.price_brutto OR NEW.wholesale_price <> OLD.wholesale_price) THEN
    SET NEW.last_modified_price = '1';
    END IF;


    IF ((NEW.prod_status = '0' OR NEW.prod_status = '2')
       AND (OLD.prod_status = '1' OR OLD.prod_status = '3')) THEN
     SET NEW.last_change_unavailable = CURDATE();
    END IF;

    IF (NEW.prod_status = '3' AND (DATEDIFF(NEW.shipment_date, CURDATE()) < 0 OR NEW.shipment_date IS NULL)) THEN
     SET NEW.prod_status = '0';
    END IF;

    IF (NEW.ommit_auto_preview = '0' AND NEW.shipment_date > DATE_ADD(CURDATE(), INTERVAL 14 DAY) AND NEW.prod_status = '3' AND NEW.shipment_time <> '3' AND NEW.shipment_date <>  '' AND NEW.shipment_date IS NOT NULL) THEN
     SET NEW.shipment_time = '3';
    END IF;

    IF (NEW.shipment_date <= DATE_ADD(CURDATE(), INTERVAL 14 DAY) AND NEW.prod_status = '3' AND NEW.shipment_time = '3' AND NEW.shipment_date <>  '' AND NEW.shipment_date IS NOT NULL) THEN
     SET NEW.shipment_time = '0';
    END IF;

    IF (NEW.ommit_auto_preview = '1') THEN
     SET NEW.shipment_time = '1';
    END IF;

    IF ((NEW.prod_status = '0' OR NEW.prod_status = '2') AND OLD.prod_status = '1') THEN
        SELECT id
        INTO NP_P_ID
        FROM main_nieprzeczytane.products AS NP_P
        WHERE NP_P.profit24_id = NEW.id
        LIMIT 1;
        IF (NP_P_ID > 0) THEN
            UPDATE main_nieprzeczytane.products SET prod_status = NEW.prod_status WHERE main_nieprzeczytane.products.id = NP_P_ID;
        END IF;

        SELECT id
        INTO ME_P_ID
        FROM main_mestro.products AS ME_P
        WHERE ME_P.profit24_id = NEW.id
        LIMIT 1;
        IF (ME_P_ID > 0) THEN
            UPDATE main_mestro.products SET prod_status = NEW.prod_status WHERE main_mestro.products.id = ME_P_ID;
        END IF;

        SELECT id
        INTO SM_P_ID
        FROM main_smarkacz.products AS SM_P
        WHERE SM_P.profit24_id = NEW.id
        LIMIT 1;
        IF (SM_P_ID > 0) THEN
            UPDATE main_smarkacz.products SET prod_status = NEW.prod_status WHERE main_smarkacz.products.id = SM_P_ID;
        END IF;


        SELECT id
        INTO NB_P_ID
        FROM main_naszabiblioteka.products AS SM_P
        WHERE SM_P.profit24_id = NEW.id
        LIMIT 1;
        IF (NB_P_ID > 0) THEN
            UPDATE main_naszabiblioteka.products SET prod_status = NEW.prod_status WHERE main_naszabiblioteka.products.id = NB_P_ID;
        END IF;
    END IF;

    IF ( NEW.product_type = 'K') THEN

        SELECT MENUS.product_type INTO def_product_type
        FROM products_extra_categories AS PEC2
        JOIN products AS P ON PEC2.product_id = P.id
        JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
        JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
        WHERE PEC2.product_id = NEW.id
        AND MI2.published =  '1'
        AND MI2.mtype =  '0'
        AND MI2.priority >0
        AND MI2.parent_id IS NOT NULL
        ORDER BY MI2.priority ASC
        LIMIT 1;

     IF (def_product_type <> 'K') THEN
       SET NEW.product_type = def_product_type;
     END IF;
    END IF;

    IF (NEW.`type` <> OLD.`type` AND NEW.`type` = '1') THEN
        SET NEW.product_type = 'A';
    END IF;
    IF (NEW.`product_type` <> OLD.`product_type`) THEN
        IF (NEW.`product_type` = 'A') THEN
            SET NEW.`type` = '1';
        ELSE
            SET NEW.`type` = '0';
        END IF;

    END IF;
END"	BEFORE	2019-02-06 14:57:44.38	NO_ENGINE_SUBSTITUTION	admin58727_prof2@%	utf8	utf8_general_ci	utf8_general_ci
trig_profit_products_aft_upd	UPDATE	products	"BEGIN

DECLARE new_general_discount DECIMAL(8,2);
DECLARE new_mark_up DECIMAL(8,2);
DECLARE publisher_discount_limt DECIMAL(8,2);
DECLARE series_discount_limt DECIMAL(8,2);
DECLARE source_discount_limt DECIMAL(8,2);
DECLARE id_pakietu INT(10) unsigned;
DECLARE att_price_netto DECIMAL(8,2);
DECLARE old_products_tarrifs_general_discount DECIMAL(8,2);
DECLARE minimal_sell_price DECIMAL(8,2);
DECLARE ceneo_new_sell_price DECIMAL(8,2);
DECLARE ceneo_discount DECIMAL(8,2);
DECLARE cyclic_buffor_product_id INT DEFAULT NULL;
DECLARE ceneo_tab INT UNSIGNED DEFAULT 0;
DECLARE temp_prod_status CHAR(1) DEFAULT NULL;
DECLARE temp_shipment_time CHAR(1) DEFAULT NULL;
DECLARE temp_is_ceneo_tabs INT DEFAULT 0;
DECLARE has_ceneo_tarrif DECIMAL (8,2) DEFAULT NULL;
DECLARE publisher_ceneo INT DEFAULT 0;
DECLARE changed_prod_status INT;
DECLARE min_price DECIMAL(8,2);
DECLARE min_over DECIMAL(8,2);
DECLARE shop_main_category INT(10);
DECLARE source_min_over DECIMAL(8,2);

DECLARE done INT DEFAULT 0;

DECLARE pakiety CURSOR FOR    SELECT DISTINCT packet_id  FROM products_packets_items WHERE product_id=NEW.id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

SELECT price_netto INTO att_price_netto FROM products_attachments WHERE product_id = NEW.id;
SELECT discount, mark_up INTO new_general_discount, new_mark_up FROM products_general_discount WHERE source = NEW.source LIMIT 0,1;

IF ((att_price_netto IS NULL OR att_price_netto = 0) AND new_mark_up > 0 AND NEW.wholesale_price > 0) THEN

    SELECT (100 - ((((NEW.wholesale_price * (new_mark_up / 100)) + NEW.wholesale_price) / NEW.price_brutto) * 100)) INTO new_general_discount;
END IF;


IF (NEW.source <> OLD.source) OR (NEW.publisher_id <> OLD.publisher_id) OR (NEW.price_brutto <> OLD.price_brutto) OR (NEW.wholesale_price <> OLD.wholesale_price) THEN
  IF (NEW.packet='0') THEN
      SELECT IFNULL(discount_limit,0) INTO publisher_discount_limt FROM products_publishers WHERE id = NEW.publisher_id;
          SELECT IFNULL(PS.discount_limit,0) INTO series_discount_limt FROM products_series AS PS
          JOIN products_to_series AS PTS ON
                 PS.id = PTS.series_id
          WHERE PS.publisher_id = NEW.publisher_id AND PTS.product_id = NEW.id
          LIMIT 1;
      SELECT IFNULL(discount_limit,0) INTO source_discount_limt FROM products_source_discount WHERE first_source = NEW.created_by;

      IF (publisher_discount_limt > 0) AND (new_general_discount > publisher_discount_limt) THEN
          SELECT publisher_discount_limt INTO new_general_discount;
      END IF;

      IF (source_discount_limt > 0) AND (new_general_discount > source_discount_limt) THEN
          SELECT source_discount_limt INTO new_general_discount;
      END IF;

      IF (series_discount_limt > 0) AND (new_general_discount > series_discount_limt) THEN
          SELECT series_discount_limt INTO new_general_discount;
      END IF;

      SELECT discount INTO old_products_tarrifs_general_discount FROM products_tarrifs WHERE product_id = NEW.id AND general_discount = '1' LIMIT 0,1;
      IF (old_products_tarrifs_general_discount <> new_general_discount) OR (NEW.price_brutto <> OLD.price_brutto) THEN
        UPDATE products_tarrifs
        SET discount = new_general_discount,
         discount_value = ROUND(NEW.price_brutto*(discount/100), 2),
         price_brutto = NEW.price_brutto-discount_value,
         price_netto = ROUND(price_brutto / (1 + NEW.vat/100), 2)
        WHERE product_id = NEW.id AND general_discount = '1';
      END IF;
  END IF;
END IF;

IF NEW.price_brutto <> OLD.price_brutto THEN
  UPDATE products_tarrifs
  SET discount_value = ROUND(NEW.price_brutto*(discount/100), 2),
   price_brutto = NEW.price_brutto-discount_value,
   price_netto = ROUND(price_brutto / (1 + NEW.vat/100), 2)
  WHERE product_id = NEW.id;
END IF;



IF NEW.prod_status <> OLD.prod_status OR NEW.published <> OLD.published THEN
  IF NEW.prod_status = '0' OR NEW.prod_status = '2' OR NEW.published = '0' THEN
     UPDATE products_packets_items SET upd_packet_navailable = '0' WHERE NEW.id=product_id;
  END IF;
END IF;



IF NEW.prod_status <> OLD.prod_status OR NEW.published <> OLD.published THEN
  IF NEW.prod_status = '1' AND NEW.published = '1' THEN
     UPDATE products_packets_items SET upd_packet_navailable = '2' WHERE NEW.id=product_id;
  END IF;
END IF;



IF (NEW.published = '1' AND NEW.published <> OLD.published AND NEW.publication_year = YEAR(NOW())) THEN
 IF ((SELECT id FROM products_news WHERE product_id = NEW.id) > 0 ) THEN
     UPDATE products_news SET news_from = NOW() WHERE product_id = NEW.id;
 ELSE
     INSERT INTO products_news_become SET product_id = NEW.id ON DUPLICATE KEY UPDATE product_id = NEW.id;
 END IF;
END IF;



IF (
  NEW.`name` <> OLD.`name` OR
  NEW.`isbn_plain` <> OLD.`isbn_plain` OR
  NEW.`isbn_10` <> OLD.`isbn_10` OR
  NEW.`isbn_13` <> OLD.`isbn_13` OR
  NEW.`ean_13` <> OLD.`ean_13` OR
  NEW.`publisher_id` <> OLD.`publisher_id` OR
  NEW.`publication_year` <> OLD.`publication_year` OR
  NEW.`edition` <> OLD.`edition` OR
  NEW.`legal_status_date` <> OLD.`legal_status_date` OR
  NEW.`city` <> OLD.`city` OR
  NEW.`pages` <> OLD.`pages` OR
  NEW.`binding` <> OLD.`binding` OR
  NEW.`dimension` <> OLD.`dimension` OR
  NEW.`language` <> OLD.`language` OR
  NEW.`translator` <> OLD.`translator` OR
  NEW.`original_language` <> OLD.`original_language` OR
  NEW.`original_title` <> OLD.`original_title` OR
  NEW.`weight` <> OLD.`weight` OR
  NEW.`volumes` <> OLD.`volumes` OR
  NEW.`volume_nr` <> OLD.`volume_nr` OR
  NEW.`volume_name` <> OLD.`volume_name` OR
  NEW.`id` <> OLD.`id`
) THEN
  UPDATE products_last_update SET last_update = NOW() WHERE product_id = NEW.id;
END IF;

SELECT product_id INTO ceneo_tab FROM main_profit24.ceneo_tabs_items WHERE product_id = NEW.id AND (`type`= 0 OR `type`= 2);
SELECT include_publishers_products INTO publisher_ceneo FROM main_profit24.products_publishers WHERE id = NEW.publisher_id;

SET temp_shipment_time = NEW.shipment_time;
SET temp_prod_status = NEW.prod_status;

IF (0 != ceneo_tab || 0 != publisher_ceneo) THEN

	SET temp_is_ceneo_tabs = 1;

	IF (temp_prod_status = '3') THEN
		SET temp_prod_status = '1';
	END IF;

	IF (temp_shipment_time = '1') THEN
		SET temp_shipment_time = '0';
	END IF;

END IF;

SELECT price_brutto INTO has_ceneo_tarrif FROM products_tarrifs WHERE product_id = NEW.id AND `type`= 1 LIMIT 1;


IF (has_ceneo_tarrif IS NOT NULL AND NEW.wholesale_price > 0) THEN

	SELECT minimal_overhead INTO min_over FROM main_profit24.website_ceneo_settings WHERE website_id = 1 LIMIT 1;
	SELECT minimal_overhead INTO source_min_over FROM main_profit24.products_general_discount_common WHERE source = NEW.source AND website_id = 1;

	IF(source_min_over > 0) THEN
		SET min_over = source_min_over;
	END IF;

	SELECT WCC.website_id INTO shop_main_category FROM main_profit24.products AS P JOIN main_profit24.website_ceneo_categories AS WCC ON WCC.category_id = P.main_category_id where P.id = NEW.id;

	SET min_price = ROUND( NEW.wholesale_price + ((NEW.wholesale_price * min_over) / 100), 2 );

        IF( (has_ceneo_tarrif < NEW.wholesale_price AND NEW.wholesale_price > 0) OR NEW.price_brutto != OLD.price_brutto ) THEN

   	     DELETE FROM products_tarrifs WHERE product_id = NEW.id AND type = 1;
        END IF;


	IF(has_ceneo_tarrif < min_price AND shop_main_category = 1 AND NEW.wholesale_price != NEW.price_brutto) THEN



		DELETE FROM products_tarrifs WHERE product_id = NEW.id AND type = 1;


		DELETE PT FROM main_nieprzeczytane.products_tarrifs AS PT JOIN main_nieprzeczytane.products AS P ON P.id = PT.product_id WHERE P.profit24_id = NEW.id AND PT.type = 1;
		DELETE PT FROM main_mestro.products_tarrifs AS PT JOIN main_mestro.products AS P ON P.id = PT.product_id WHERE P.profit24_id = NEW.id AND PT.type = 1;


		DELETE FROM main_profit24.tarrifs_buffor WHERE shop_product_id = NEW.id AND website_id = 1;

	END IF;

END IF;

IF(OLD.name != NEW.name OR OLD.publication_year != NEW.publication_year OR OLD.publisher_id != NEW.publisher_id OR OLD.isbn != NEW.isbn OR OLD.isbn_10 != NEW.isbn_10 OR OLD.isbn_13 != NEW.isbn_13 OR OLD.ean_13 != NEW.ean_13 OR OLD.prod_status != NEW.prod_status OR OLD.published != NEW.published OR OLD.is_news != NEW.is_news OR OLD.is_bestseller != NEW.is_bestseller OR OLD.is_previews != NEW.is_previews OR OLD.wholesale_price != NEW.wholesale_price OR OLD.price_brutto != NEW.price_brutto) THEN

INSERT INTO product_changes_data (product_id, has_changed, elastic_has_changed) VALUES (NEW.id, 1, 0) ON DUPLICATE KEY UPDATE product_id = NEW.id, has_changed = 1, elastic_has_changed = 0, updated = NOW();

END IF;
END"	AFTER	2019-02-06 14:57:44.38		admin58727_prof2@%	utf8	utf8_unicode_ci	utf8_general_ci
trig_products_extra_categories_aft_ins	INSERT	products_extra_categories	"BEGIN
   DECLARE def_product_type CHAR(1) DEFAULT 'K';
   DECLARE main_cat_id INT(10) DEFAULT NULL;

   UPDATE products_last_update
   SET last_update = NOW()
   WHERE product_id = NEW.product_id;

    SELECT MENUS.product_type INTO def_product_type
    FROM products_extra_categories AS PEC2
    JOIN products AS P ON PEC2.product_id = P.id
    JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
    JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
    WHERE PEC2.product_id = NEW.product_id
    AND MI2.published =  '1'
    AND MI2.mtype =  '0'
    AND MI2.priority >0
    AND MI2.parent_id IS NOT NULL
    ORDER BY MI2.priority ASC
    LIMIT 1;

   IF (def_product_type <> 'K')
   THEN
     UPDATE products
     SET product_type = def_product_type
     WHERE id = NEW.product_id;

   END IF;

    SELECT MI2.id INTO main_cat_id
    FROM products_extra_categories as PEC2
    JOIN products AS P ON PEC2.product_id = P.id
    JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
    JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
    WHERE PEC2.product_id = NEW.product_id
    AND IF(P.product_type != 'K', MENUS.product_type = P.product_type, (MENUS.product_type = 'K'))
    AND MI2.published = '1'
    AND MI2.mtype = '0'
    AND MI2.priority > 0
    AND MI2.parent_id IS NOT NULL
    ORDER BY MI2.priority ASC LIMIT 1;

    UPDATE products SET main_category_id = main_cat_id WHERE id = NEW.product_id LIMIT 1;

 END"	AFTER	2019-02-06 14:59:12.04		admin58727_prof2@%	utf8	utf8_unicode_ci	utf8_general_ci
trig_products_extra_categories_aft_delete	DELETE	products_extra_categories	"BEGIN
    DECLARE main_cat_id INT(10) DEFAULT NULL;

    SELECT MI2.id INTO main_cat_id
    FROM products_extra_categories as PEC2
    JOIN products AS P ON PEC2.product_id = P.id
    JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
    JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
    WHERE PEC2.product_id = OLD.product_id
    AND IF(P.product_type != 'K', MENUS.product_type = P.product_type, (MENUS.product_type = 'K'))
    AND MI2.published = '1'
    AND MI2.mtype = '0'
    AND MI2.priority > 0
    AND MI2.parent_id IS NOT NULL
    ORDER BY MI2.priority ASC LIMIT 1;

    UPDATE products SET main_category_id = main_cat_id WHERE id = OLD.product_id LIMIT 1;

END"	AFTER	2019-02-06 14:59:12.04		admin58727_prof2@%	utf8	utf8_unicode_ci	utf8_general_ci
trig_products_shadow_aft_upd	UPDATE	products_shadow	"BEGIN

  IF (NEW.`authors` <> OLD.`authors` OR NEW.`series` <> OLD.`series`) THEN
    UPDATE products_last_update SET last_update = NOW() WHERE product_id = NEW.id;
  END IF;

END"	AFTER	2019-02-06 14:59:55.77		admin58727_prof2@%	utf8	utf8_general_ci	utf8_general_ci
trig_profit_products_stock_bef_upd	UPDATE	products_stock	"BEGIN

    IF (NEW.profit_g_reservations <> OLD.profit_g_reservations) THEN
        IF (NEW.stock_reservations > NEW.stock_act_stock) THEN
            SET NEW.stock_reservations = NEW.stock_act_stock;
            SET NEW.stock_status = 0;
        ELSE
            SET NEW.stock_reservations = NEW.profit_g_reservations;
            SET NEW.stock_status = NEW.stock_act_stock - NEW.profit_g_reservations;
        END IF;
    END IF;


  IF(
    (NEW.profit_g_act_stock = 0 OR NEW.profit_g_act_stock = NULL)
    AND
    (OLD.profit_g_act_stock > 0)
  ) THEN
    SET NEW.profit_g_act_stock_unavailable_last_update = NOW();
  END IF;

  IF (NEW.profit_g_act_stock > 0) THEN
    SET NEW.profit_g_act_stock_unavailable_last_update = NULL;
  END IF;

        IF (NEW.profit_g_last_import != OLD.profit_g_last_import) THEN
         IF (NEW.profit_g_status != (NEW.profit_g_act_stock - NEW.profit_g_reservations)) THEN
          SET NEW.profit_g_status = (NEW.profit_g_act_stock - NEW.profit_g_reservations);
         END IF;
        END IF;

        IF (NEW.profit_j_last_import != OLD.profit_j_last_import) THEN
         IF (NEW.profit_j_status != (NEW.profit_j_act_stock - NEW.profit_j_reservations)) THEN
          SET NEW.profit_j_status = (NEW.profit_j_act_stock - NEW.profit_j_reservations);
         END IF;
        END IF;

        IF (NEW.profit_e_last_import != OLD.profit_e_last_import) THEN
         IF (NEW.profit_e_status != (NEW.profit_e_act_stock - NEW.profit_e_reservations)) THEN
          SET NEW.profit_e_status = (NEW.profit_e_act_stock - NEW.profit_e_reservations);
         END IF;
        END IF;


    IF ( NEW.profit_g_location <> OLD.profit_g_location ) THEN
        INSERT INTO products_stock_locations
        SET
            products_stock_id = NEW.id,
            new_g_location = NEW.profit_g_location,
            old_g_location = OLD.profit_g_location,
            last_update = NOW();
    END IF;

    IF ( NEW.profit_x_location <> OLD.profit_x_location ) THEN
        INSERT INTO products_stock_locations
        SET
            products_stock_id = NEW.id,
            new_x_location = NEW.profit_x_location,
            old_x_location = OLD.profit_x_location,
            last_update = NOW();
    END IF;
END"	BEFORE	2019-02-06 15:00:32.00		admin58727_prof2@%	utf8	utf8_unicode_ci	utf8_general_ci
trig_profit_products_stock_upd	UPDATE	products_stock	"BEGIN

    DECLARE curr_prod_status ENUM('0','1','2','3');
    DECLARE curr_published ENUM('0','1');
    DECLARE curr_ommit_auto_preview CHAR(1);
    DECLARE komunikat VARCHAR(255);

    DECLARE NEW_prod_status ENUM('0','1','2','3');
    DECLARE NEW_prod_location VARCHAR(45);
    DECLARE NEW_prod_price_brutto DECIMAL(8,2);
    DECLARE NEW_prod_price_netto DECIMAL(8,2);
    DECLARE MIN_wholesale_price DECIMAL(8,2);
    DECLARE CMP_wholesale_price DECIMAL(8,2);
    DECLARE NEW_prod_vat TINYINT(4);
    DECLARE NEW_prod_source CHAR(2);
    DECLARE NEW_prod_shipment_time ENUM('0','1','2','3','4','5','6','7','8','9','10','11');
    DECLARE NEW_prod_shipment_date DATE;
    DECLARE att_price_netto DECIMAL(8,2);
    DECLARE changed_prod_status INT;
    DECLARE original_prod_status INT;
    DECLARE price_brutto_current_tarrif DECIMAL(8,2);


    SELECT '0.00' INTO NEW_prod_price_brutto;
    SELECT 0.00 INTO MIN_wholesale_price;
    SELECT 0.00 INTO CMP_wholesale_price;
    SELECT '' INTO NEW_prod_location;
    SELECT 'TEST' INTO komunikat;

    SELECT price_netto INTO att_price_netto FROM products_attachments WHERE product_id = NEW.id;
    SELECT prod_status,published,ommit_auto_preview INTO curr_prod_status,curr_published,curr_ommit_auto_preview FROM products WHERE id = NEW.id;

    IF(curr_prod_status <> '2') THEN
      SELECT '1' INTO NEW_prod_status;
      IF (NEW.merlin_status > 0 AND NEW.merlin_price_brutto > 0.00 AND NEW.merlin_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.merlin_status * NEW.merlin_wholesale_price)))) THEN
        SELECT CAST(NEW.merlin_status AS CHAR) INTO NEW_prod_status;
        SELECT (NEW.merlin_status * NEW.merlin_wholesale_price) INTO CMP_wholesale_price;
        SELECT NEW.merlin_wholesale_price INTO MIN_wholesale_price;
        SELECT NEW.merlin_price_brutto INTO NEW_prod_price_brutto;
        SELECT NEW.merlin_price_netto INTO NEW_prod_price_netto;
        SELECT NEW.merlin_vat INTO NEW_prod_vat;
        SELECT NEW.merlin_shipment_time INTO NEW_prod_shipment_time;
        SELECT NEW.merlin_shipment_date INTO NEW_prod_shipment_date;
        SELECT '14' INTO NEW_prod_source;
        SELECT 'merlin' INTO komunikat;
      END IF;

      IF (curr_ommit_auto_preview = '0' AND NEW_prod_source <> '' AND NEW_prod_price_brutto > 0.00) THEN
        UPDATE products
        SET
          price_brutto = NEW_prod_price_brutto,
          price_netto = NEW_prod_price_netto,
          vat = NEW_prod_vat,
          wholesale_price = MIN_wholesale_price,
          source = NEW_prod_source,
          shipment_time = NEW_prod_shipment_time,
          shipment_date = NEW_prod_shipment_date,
          last_import = NOW(),
          prod_status = NEW_prod_status,
          location = NEW_prod_location
        WHERE id = NEW.id;
      ELSE
        UPDATE products
        SET last_import = NOw(),
          prod_status = '0'
        WHERE id = NEW.id;
      END IF;

      IF(curr_published = '1') THEN
        SELECT price_brutto INTO price_brutto_current_tarrif
        FROM products_tarrifs
        WHERE product_id = NEW.id AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
        ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1;
        UPDATE products_shadow A
          JOIN products B
            ON B.id=A.id
        SET A.price_brutto = B.price_brutto,
          A.prod_status = B.prod_status,
          A.shipment_time = B.shipment_time,
          A.shipment_date = B.shipment_date,
          A.price = price_brutto_current_tarrif
        WHERE A.id = NEW.id;
      END IF;

    END IF;

  END"	AFTER	2019-02-11 14:39:24.73	NO_ENGINE_SUBSTITUTION	admin58727_prof2@%	utf8mb4	utf8mb4_general_ci	utf8_general_ci
after_insert	INSERT	products_tarrifs	"BEGIN

INSERT INTO product_changes_data (product_id, has_changed, elastic_has_changed) VALUES (NEW.product_id, 1, 0) ON DUPLICATE KEY UPDATE product_id = NEW.product_id, has_changed = 1, elastic_has_changed = 0, updated = NOW();

END"	AFTER	2019-02-06 15:04:58.16		admin@localhost	utf8	utf8_unicode_ci	utf8_general_ci
after_update	UPDATE	products_tarrifs	"BEGIN

INSERT INTO product_changes_data (product_id, has_changed, elastic_has_changed) VALUES (NEW.product_id, 1, 0) ON DUPLICATE KEY UPDATE product_id = NEW.product_id, has_changed = 1, elastic_has_changed = 0, updated = NOW();

END"	AFTER	2019-02-06 15:04:58.17		admin@localhost	utf8	utf8_unicode_ci	utf8_general_ci
after_delete	DELETE	products_tarrifs	"BEGIN

INSERT INTO product_changes_data (product_id, has_changed, elastic_has_changed) VALUES (OLD.product_id, 1, 0) ON DUPLICATE KEY UPDATE product_id = OLD.product_id, has_changed = 1, elastic_has_changed = 0, updated = NOW();


	IF ( OLD.`type` = 1) THEN
			INSERT INTO deleted_tarrifs (product_id, tarrif_id, price) VALUES (OLD.product_id, OLD.id, OLD.price_brutto) ON DUPLICATE KEY UPDATE product_id = OLD.product_id, tarrif_id = OLD.id, updatet_at = NOW(), price = OLD.price_brutto;
	END IF;



END"	AFTER	2019-02-06 15:04:58.17		admin@localhost	utf8	utf8_unicode_ci	utf8_general_ci
trig_products_to_tags_ins	INSERT	products_to_tags	"BEGIN
   DECLARE tag_quantity INT;
   DECLARE product_published ENUM('0','1');
	   SELECT quantity INTO tag_quantity FROM products_tags WHERE id = NEW.tag_id;
     SELECT published INTO product_published FROM products WHERE id = NEW.product_id;

	IF product_published = '1' AND tag_quantity IS NOT NULL THEN
				UPDATE products_tags SET quantity = quantity + 1 WHERE id = NEW.tag_id;
	END IF;
 END"	AFTER	2019-02-06 15:05:24.60	NO_AUTO_VALUE_ON_ZERO	admin58727_prof2@%	utf8	utf8_general_ci	utf8_general_ci
trig_products_to_tags_del	DELETE	products_to_tags	"BEGIN
   DECLARE tag_quantity INT;
   DECLARE product_published ENUM('0','1');
	   SELECT IFNULL(quantity, 0) INTO tag_quantity FROM products_tags WHERE id = OLD.tag_id;
       SELECT published INTO product_published FROM products WHERE id = OLD.product_id;

  IF product_published = '1' THEN
	 IF tag_quantity >= 1 THEN
	  	   UPDATE products_tags SET quantity = quantity - 1 WHERE id = OLD.tag_id;
	 END IF;
  END IF;
END"	AFTER	2019-02-06 15:05:24.60	NO_AUTO_VALUE_ON_ZERO	admin58727_prof2@%	utf8	utf8_general_ci	utf8_general_ci
