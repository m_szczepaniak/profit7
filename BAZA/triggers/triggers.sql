

USE `main_profit24`;
DELIMITER $$

CREATE TRIGGER `trig_profit_products_stock_bef_upd` BEFORE UPDATE ON `products_stock` FOR EACH ROW
BEGIN
/*
	IF (NEW.profit_g_reservations > OLD.profit_g_reservations) THEN
	 SET NEW.profit_g_status = (NEW.profit_g_act_stock - NEW.profit_g_reservations);
	END IF;
*/
	IF (NEW.profit_g_last_import != OLD.profit_g_last_import) THEN
	 IF (NEW.profit_g_status != (NEW.profit_g_act_stock - NEW.profit_g_reservations)) THEN
	  SET NEW.profit_g_status = (NEW.profit_g_act_stock - NEW.profit_g_reservations);
	 END IF;
	END IF;

	IF (NEW.profit_j_last_import != OLD.profit_j_last_import) THEN
	 IF (NEW.profit_j_status != (NEW.profit_j_act_stock - NEW.profit_j_reservations)) THEN
	  SET NEW.profit_j_status = (NEW.profit_j_act_stock - NEW.profit_j_reservations);
	 END IF;
	END IF;

	IF (NEW.profit_e_last_import != OLD.profit_e_last_import) THEN
	 IF (NEW.profit_e_status != (NEW.profit_e_act_stock - NEW.profit_e_reservations)) THEN
	  SET NEW.profit_e_status = (NEW.profit_e_act_stock - NEW.profit_e_reservations);
	 END IF;
	END IF;

END



USE `main_profit24_old`;
DELIMITER $$
CREATE TRIGGER `trig_products_stock_reservations_bef_del` BEFORE DELETE ON `products_stock_reservations` FOR EACH ROW
BEGIN

  IF (OLD.source_id = "10") THEN
    UPDATE products_stock 
    SET profit_g_reservations = profit_g_reservations - OLD.quantity
    WHERE OLD.products_stock_id = products_stock.id;
  ELSEIF (OLD.source_id = "1") THEN
    UPDATE products_stock 
    SET profit_e_reservations = profit_e_reservations - OLD.quantity
    WHERE OLD.products_stock_id = products_stock.id;
  ELSEIF (OLD.source_id = "0") THEN
    UPDATE products_stock 
    SET profit_j_reservations = profit_j_reservations - OLD.quantity
    WHERE OLD.products_stock_id = products_stock.id;
  END IF;

END




USE `main_profit24`;
DELIMITER $$
CREATE TRIGGER `trig_products_stock_reservations_aft_ins` AFTER INSERT ON `products_stock_reservations` FOR EACH ROW
BEGIN

	UPDATE products_stock 
	SET profit_g_reservations = profit_g_reservations + NEW.quantity
	WHERE NEW.products_stock_id = products_stock.id;

END



USE `main_profit24`;
DELIMITER $$
DROP TRIGGER IF EXISTS trig_products_stock_reservations_bef_upd$$
CREATE TRIGGER `trig_products_stock_reservations_bef_upd` BEFORE UPDATE ON `products_stock_reservations` FOR EACH ROW
BEGIN
	IF (NEW.quantity != OLD.quantity) THEN
    IF (NEW.quantity > OLD.quantity) THEN
      UPDATE products_stock 
        SET profit_g_reservations = profit_g_reservations + (NEW.quantity - OLD.quantity)
      WHERE OLD.products_stock_id = products_stock.id;
    ELSE 
      UPDATE products_stock 
        SET profit_g_reservations = profit_g_reservations - (OLD.quantity - NEW.quantity)
      WHERE OLD.products_stock_id = products_stock.id;
    END IF;
	END IF;
END



USE `main_profit24`;
DELIMITER $$
DROP TRIGGER IF EXISTS `orders_items_bef_upd` $$
CREATE TRIGGER `orders_items_bef_upd` BEFORE UPDATE ON `orders_items` FOR EACH ROW
BEGIN

IF ((NEW.item_type = "I" OR NEW.item_type = "P") AND NEW.packet = '0') THEN
	-- jeśli usuwamy
	IF (NEW.deleted = '1' && OLD.deleted = '0') THEN
		DELETE FROM products_stock_reservations 
		WHERE products_stock_reservations.orders_items_id = NEW.id;
	END IF;
	-- jeśli zmieniamy źródło
	IF (OLD.`source` = '51' && NEW.`source` <> '51') THEN
		DELETE FROM products_stock_reservations 
		WHERE products_stock_reservations.orders_items_id = NEW.id;
	END IF;

	-- jeśli zmieniamy ilość
	IF (NEW.quantity != OLD.quantity) THEN
		UPDATE products_stock_reservations 
		SET quantity = NEW.quantity
		WHERE products_stock_reservations.orders_items_id = NEW.id;
	END IF;

	-- dodajemy nowy rekord
	IF (NEW.`source` = '51' && ((NEW.`status` = '3' && OLD.`status` != '3') || (NEW.`source` = '51' && OLD.`source` != '51') || (NEW.deleted = '0' && OLD.deleted = '1'))) THEN
		INSERT INTO products_stock_reservations 
		SET
			products_stock_id = NEW.product_id,
			order_id = NEW.order_id,
			orders_items_id = NEW.id,
			quantity = NEW.quantity;
	END IF;

END IF;
END




USE `main_profit24`;
DELIMITER $$
CREATE
TRIGGER `main_profit24`.`trig_orders_upd`
BEFORE UPDATE ON `main_profit24`.`orders`
FOR EACH ROW
BEGIN
DECLARE total_value DECIMAL(8,2);
DECLARE new_to_pay DECIMAL(8,2);
DECLARE order_balance DECIMAL(8,2);
IF (NEW.value_brutto <> OLD.value_brutto) OR (NEW.transport_cost <> OLD.transport_cost) OR (NEW.paid_amount <> OLD.paid_amount) OR (NEW.order_status <> OLD.order_status) THEN
  SELECT (NEW.value_brutto + NEW.transport_cost) INTO total_value;
  SELECT total_value INTO new_to_pay;
  SET NEW.to_pay = total_value;
  IF (NEW.paid_amount > 0) THEN
    SELECT NEW.paid_amount - total_value INTO order_balance;
    SET NEW.balance = order_balance;
  ELSEIF (OLD.paid_amount > 0) THEN
    SET NEW.balance = 0;
  END IF;
END IF;
IF ((NEW.order_status <> OLD.order_status AND NEW.order_status = '5') OR (NEW.order_status = '5' AND NEW.paid_amount <> OLD.paid_amount)) THEN
  SET NEW.balance = NEW.paid_amount;
END IF;


	IF (NEW.order_status <> OLD.order_status) THEN
    IF (NEW.order_status = '3') THEN 
      -- jeśli zmienia status w zatwierdzono przenosimy do ERP
      UPDATE products_stock_reservations 
        SET move_to_erp = '1'
      WHERE products_stock_reservations.order_id = NEW.id;
    ELSEIF (NEW.order_status = '5') THEN
      -- jeśli anulujemy, to usuwamy rezerwacje
      DELETE FROM products_stock_reservations WHERE products_stock_reservations.order_id = NEW.id;
    END IF;
	END IF;

END
