BEGIN
DECLARE total_value DECIMAL(8,2);
DECLARE new_to_pay DECIMAL(8,2);
DECLARE order_balance DECIMAL(8,2);
IF (NEW.value_brutto <> OLD.value_brutto) OR (NEW.transport_cost <> OLD.transport_cost) OR (NEW.paid_amount <> OLD.paid_amount) OR (NEW.order_status <> OLD.order_status) THEN
  SELECT (NEW.value_brutto + NEW.transport_cost) INTO total_value;
  SELECT total_value INTO new_to_pay;
  SET NEW.to_pay = total_value;
  IF (NEW.paid_amount > 0) THEN
    SELECT NEW.paid_amount - total_value INTO order_balance;
    SET NEW.balance = order_balance;
  ELSEIF (OLD.paid_amount > 0) THEN
    SET NEW.balance = 0;
  END IF;
END IF;
IF ((NEW.order_status <> OLD.order_status AND NEW.order_status = '5') OR (NEW.order_status = '5' AND NEW.paid_amount <> OLD.paid_amount)) THEN
  SET NEW.balance = NEW.paid_amount;
END IF;


	IF (NEW.order_status <> OLD.order_status) THEN
    IF (NEW.order_status = '3') THEN 
      -- jeśli zmienia status w zatwierdzono przenosimy do ERP
      UPDATE products_stock_reservations 
        SET move_to_erp = '1'
      WHERE products_stock_reservations.order_id = NEW.id;
    ELSEIF (NEW.order_status = '5') THEN
      -- jeśli anulujemy, to usuwamy rezerwacje
      DELETE FROM products_stock_reservations WHERE products_stock_reservations.order_id = NEW.id;
    END IF;
	END IF;

END