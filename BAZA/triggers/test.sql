SELECT DISTINCT P.id, P.plain_name AS name, P.publication_year, PP.name as publisher, P.isbn, P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13, P.prod_status, P.published,
      (
      SELECT  price_brutto
      FROM products_tarrifs
      WHERE product_id = P.id AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
      ORDER BY type DESC, discount DESC LIMIT 0,1 LIMIT 1
   ) as price_brutto,
    (
   SELECT GROUP_CONCAT(PTG.tag)
      FROM products_to_tags AS PTTG
      JOIN products_tags AS PTG ON PTTG.tag_id = PTG.id
      WHERE PTTG.product_id = P.id
 ) as tags,
    (
   SELECT GROUP_CONCAT(PS.name)
      FROM products_to_series AS PTS
      JOIN products_series AS PS ON PTS.series_id = PS.id
      WHERE PTS.product_id = P.id
 ) as series,
 (
   SELECT GROUP_CONCAT(CONCAT (PA.name, ' ',PA.surname))
      FROM products_to_authors AS PTA
      JOIN products_authors AS PA ON PTA.author_id = PA.id
      WHERE PTA.product_id = P.id
 ) as authors,
 (SELECT GROUP_CONCAT(PEC.page_id) FROM products_extra_categories AS PEC WHERE product_id = P.id) as cats
    FROM products AS P
    LEFT JOIN products_publishers AS PP ON P.publisher_id = PP.id
    WHERE P.published = '1'
    GROUP BY P.id
    ORDER BY P.id