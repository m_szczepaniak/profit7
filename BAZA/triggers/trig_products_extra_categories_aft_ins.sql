BEGIN
   DECLARE def_product_type CHAR(1) DEFAULT 'K';
   DECLARE main_cat_id INT(10) DEFAULT NULL;

   UPDATE products_last_update
   SET last_update = NOW()
   WHERE product_id = NEW.product_id;

    SELECT MENUS.product_type INTO def_product_type
    FROM products_extra_categories AS PEC2
    JOIN products AS P ON PEC2.product_id = P.id
    JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
    JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
    WHERE PEC2.product_id = NEW.product_id
    AND MI2.published =  '1'
    AND MI2.mtype =  '0'
    AND MI2.priority >0
    AND MI2.parent_id IS NOT NULL
    ORDER BY MI2.priority ASC
    LIMIT 1;

   IF (def_product_type <> 'K')
   THEN
     UPDATE products
     SET product_type = def_product_type
     WHERE id = NEW.product_id;

   END IF;

    SELECT MI2.id INTO main_cat_id
    FROM products_extra_categories as PEC2
    JOIN products AS P ON PEC2.product_id = P.id
    JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
    JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
    WHERE PEC2.product_id = NEW.product_id
    AND IF(P.product_type != 'K', MENUS.product_type = P.product_type, (MENUS.product_type = 'K'))
    AND MI2.published = '1'
    AND MI2.mtype = '0'
    AND MI2.priority > 0
    AND MI2.parent_id IS NOT NULL
    ORDER BY MI2.priority ASC LIMIT 1;

    UPDATE products SET main_category_id = main_cat_id WHERE id = NEW.product_id LIMIT 1;

 END


