BEGIN

DECLARE curr_prod_status ENUM('0','1','2','3');
DECLARE curr_published ENUM('0','1');
DECLARE curr_ommit_auto_preview CHAR(1);
DECLARE komunikat VARCHAR(255);

DECLARE NEW_prod_status ENUM('0','1','2','3');
DECLARE NEW_prod_location VARCHAR(45);
DECLARE NEW_prod_price_brutto DECIMAL(8,2);
DECLARE NEW_prod_price_netto DECIMAL(8,2);
DECLARE MIN_wholesale_price DECIMAL(8,2);
DECLARE CMP_wholesale_price DECIMAL(8,2);
DECLARE NEW_prod_vat TINYINT(4);
DECLARE NEW_prod_source CHAR(2);
DECLARE NEW_prod_shipment_time ENUM('0','1','2','3','4','5','6','7','8','9','10','11');
DECLARE NEW_prod_shipment_date DATE;
DECLARE att_price_netto DECIMAL(8,2);
DECLARE changed_prod_status INT;
DECLARE original_prod_status INT;
DECLARE price_brutto_current_tarrif DECIMAL(8,2);


SELECT '0.00' INTO NEW_prod_price_brutto;
SELECT 0.00 INTO MIN_wholesale_price;
SELECT 0.00 INTO CMP_wholesale_price;
SELECT '' INTO NEW_prod_location;
SELECT 'TEST' INTO komunikat;

SELECT price_netto INTO att_price_netto FROM products_attachments WHERE product_id = NEW.id;
SELECT prod_status,published,ommit_auto_preview INTO curr_prod_status,curr_published,curr_ommit_auto_preview FROM products WHERE id = NEW.id;

IF(curr_prod_status <> '2') THEN
SELECT '1' INTO NEW_prod_status;
IF (NEW.merlin_status > 0 AND NEW.merlin_price_brutto > 0.00 AND NEW.merlin_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.merlin_status * NEW.merlin_wholesale_price)))) THEN
SELECT CAST(NEW.merlin_status AS CHAR) INTO NEW_prod_status;
SELECT (NEW.merlin_status * NEW.merlin_wholesale_price) INTO CMP_wholesale_price;
SELECT NEW.merlin_wholesale_price INTO MIN_wholesale_price;
SELECT NEW.merlin_price_brutto INTO NEW_prod_price_brutto;
SELECT NEW.merlin_price_netto INTO NEW_prod_price_netto;
SELECT NEW.merlin_vat INTO NEW_prod_vat;
SELECT NEW.merlin_shipment_time INTO NEW_prod_shipment_time;
SELECT NEW.merlin_shipment_date INTO NEW_prod_shipment_date;
SELECT '14' INTO NEW_prod_source;
SELECT 'merlin' INTO komunikat;
END IF;

IF (curr_ommit_auto_preview = '0' AND NEW_prod_source <> '' AND NEW_prod_price_brutto > 0.00) THEN
UPDATE products
SET
  price_brutto = NEW_prod_price_brutto,
  price_netto = NEW_prod_price_netto,
  vat = NEW_prod_vat,
  wholesale_price = MIN_wholesale_price,
  source = NEW_prod_source,
  shipment_time = NEW_prod_shipment_time,
  shipment_date = NEW_prod_shipment_date,
  last_import = NOW(),
  prod_status = NEW_prod_status,
  location = NEW_prod_location
WHERE id = NEW.id;
ELSE
UPDATE products
SET last_import = NOw(),
  prod_status = '0'
WHERE id = NEW.id;
END IF;

IF(curr_published = '1') THEN
SELECT price_brutto INTO price_brutto_current_tarrif
FROM products_tarrifs
WHERE product_id = NEW.id AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
ORDER BY vip DESC, type DESC, discount DESC LIMIT 0,1;
UPDATE products_shadow A
  JOIN products B
    ON B.id=A.id
SET A.price_brutto = B.price_brutto,
  A.prod_status = B.prod_status,
  A.shipment_time = B.shipment_time,
  A.shipment_date = B.shipment_date,
  A.price = price_brutto_current_tarrif
WHERE A.id = NEW.id;
END IF;

END IF;

END