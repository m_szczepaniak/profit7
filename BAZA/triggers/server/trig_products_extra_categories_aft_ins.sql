BEGIN
    DECLARE def_game CHAR(1) DEFAULT '0';
    DECLARE main_cat_id INT(10) DEFAULT NULL;

    UPDATE products_last_update
    SET last_update = NOW()
    WHERE product_id = NEW.product_id;

    SELECT game
    INTO def_game
    FROM menus_items AS MI
      JOIN menus AS M
        ON MI.menu_id = M.id AND M.game = "1"
    WHERE MI.id = NEW.page_id;

    IF (def_game = '1')
    THEN
      UPDATE products
      SET product_type = IF(product_type = 'K', 'G', 'K')
      WHERE id = NEW.product_id;

    END IF;

	SELECT MI2.id INTO main_cat_id
	FROM products_extra_categories as PEC2
	JOIN products AS P ON PEC2.product_id = P.id
	JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
	JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
	WHERE PEC2.product_id = NEW.product_id
	AND IF(P.product_type != 'K', MENUS.game = '1', (MENUS.game = '1' OR MENUS.game = '0'))
	AND MI2.published = '1'
	AND MI2.mtype = '0'
	AND MI2.priority > 0
	AND MI2.parent_id IS NOT NULL
	ORDER BY MI2.priority ASC LIMIT 1;

	UPDATE products SET main_category_id = main_cat_id WHERE id = NEW.product_id LIMIT 1;

  END