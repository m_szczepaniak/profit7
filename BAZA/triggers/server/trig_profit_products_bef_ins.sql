BEGIN

  SET NEW.description_on_create = NEW.description;

  IF (NEW.prod_status = '0' OR NEW.prod_status = '2') THEN
   SET NEW.last_change_unavailable = CURDATE();
  END IF;

  SET NEW.streamsoft_indeks = CASE WHEN NEW.ean_13 IS NOT NULL THEN NEW.ean_13
                  WHEN NEW.isbn_13 IS NOT NULL THEN NEW.isbn_13
                  WHEN NEW.isbn_plain IS NOT NULL AND NEW.isbn_plain <> '' AND NEW.isbn_plain <> '0000000000000' THEN NEW.isbn_plain
  END;
END