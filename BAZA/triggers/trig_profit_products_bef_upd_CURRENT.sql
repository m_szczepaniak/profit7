BEGIN

DECLARE NP_P_ID DECIMAL(8,2);
DECLARE ME_P_ID DECIMAL(8,2);
DECLARE SM_P_ID DECIMAL(8,2);
DECLARE NB_P_ID DECIMAL(8,2);

DECLARE def_product_type CHAR(1) DEFAULT 'K';

IF (NEW.source <> OLD.source) OR (NEW.publisher_id <> OLD.publisher_id) THEN
SET NEW.last_modified_price = '1';
END IF;


IF (NEW.price_brutto <> OLD.price_brutto OR NEW.wholesale_price <> OLD.wholesale_price) THEN
SET NEW.last_modified_price = '1';
END IF;


IF ((NEW.prod_status = '0' OR NEW.prod_status = '2')
AND (OLD.prod_status = '1' OR OLD.prod_status = '3')) THEN
SET NEW.last_change_unavailable = CURDATE();
END IF;

IF (NEW.prod_status = '3' AND (DATEDIFF(NEW.shipment_date, CURDATE()) < 0 OR NEW.shipment_date IS NULL)) THEN
SET NEW.prod_status = '0';
END IF;

IF (NEW.ommit_auto_preview = '0' AND NEW.shipment_date > DATE_ADD(CURDATE(), INTERVAL 14 DAY) AND NEW.prod_status = '3' AND NEW.shipment_time <> '3' AND NEW.shipment_date <>  '' AND NEW.shipment_date IS NOT NULL) THEN
SET NEW.shipment_time = '3';
END IF;

IF (NEW.shipment_date <= DATE_ADD(CURDATE(), INTERVAL 14 DAY) AND NEW.prod_status = '3' AND NEW.shipment_time = '3' AND NEW.shipment_date <>  '' AND NEW.shipment_date IS NOT NULL) THEN
SET NEW.shipment_time = '0';
END IF;

IF (NEW.ommit_auto_preview = '1') THEN
SET NEW.shipment_time = '1';
END IF;

IF ((NEW.prod_status = '0' OR NEW.prod_status = '2') AND OLD.prod_status = '1') THEN
SELECT id
INTO NP_P_ID
FROM main_nieprzeczytane.products AS NP_P
WHERE NP_P.profit24_id = NEW.id
LIMIT 1;
IF (NP_P_ID > 0) THEN
UPDATE main_nieprzeczytane.products SET prod_status = NEW.prod_status WHERE main_nieprzeczytane.products.id = NP_P_ID;
END IF;


SELECT id
INTO ME_P_ID
FROM main_mestro.products AS ME_P
WHERE ME_P.profit24_id = NEW.id
LIMIT 1;
IF (ME_P_ID > 0) THEN
UPDATE main_mestro.products SET prod_status = NEW.prod_status WHERE main_mestro.products.id = ME_P_ID;
END IF;


SELECT id
INTO SM_P_ID
FROM main_smarkacz.products AS SM_P
WHERE SM_P.profit24_id = NEW.id
LIMIT 1;
IF (SM_P_ID > 0) THEN
UPDATE main_smarkacz.products SET prod_status = NEW.prod_status WHERE main_smarkacz.products.id = SM_P_ID;
END IF;


SELECT id
INTO NB_P_ID
FROM main_naszabiblioteka.products AS SM_P
WHERE SM_P.profit24_id = NEW.id
LIMIT 1;
IF (NB_P_ID > 0) THEN
UPDATE main_naszabiblioteka.products SET prod_status = NEW.prod_status WHERE main_naszabiblioteka.products.id = NB_P_ID;
END IF;
END IF;

IF ( NEW.product_type = 'K') THEN

SELECT MENUS.product_type INTO def_product_type
FROM products_extra_categories AS PEC2
  JOIN products AS P ON PEC2.product_id = P.id
  JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
  JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
WHERE PEC2.product_id = NEW.id
      AND MI2.published =  '1'
      AND MI2.mtype =  '0'
      AND MI2.priority >0
      AND MI2.parent_id IS NOT NULL
ORDER BY MI2.priority ASC
LIMIT 1;

IF (def_product_type <> 'K') THEN
SET NEW.product_type = def_product_type;
END IF;
END IF;

IF (NEW.`type` <> OLD.`type` AND NEW.`type` = '1') THEN
SET NEW.product_type = 'A';
END IF;
IF (NEW.`product_type` <> OLD.`product_type`) THEN
IF (NEW.`product_type` = 'A') THEN
SET NEW.`type` = '1';
ELSE
SET NEW.`type` = '0';
END IF;

END IF;
END