BEGIN

DECLARE new_general_discount DECIMAL(8,2);
DECLARE new_mark_up DECIMAL(8,2);
DECLARE publisher_discount_limt DECIMAL(8,2);
DECLARE series_discount_limt DECIMAL(8,2);
DECLARE source_discount_limt DECIMAL(8,2);
DECLARE id_pakietu INT(10) unsigned;
DECLARE att_price_netto DECIMAL(8,2);
DECLARE old_products_tarrifs_general_discount DECIMAL(8,2);
DECLARE minimal_sell_price DECIMAL(8,2);
DECLARE ceneo_new_sell_price DECIMAL(8,2);
DECLARE ceneo_discount DECIMAL(8,2);
DECLARE cyclic_buffor_product_id INT DEFAULT NULL;
DECLARE ceneo_tab INT UNSIGNED DEFAULT 0;
DECLARE temp_prod_status CHAR(1) DEFAULT NULL;
DECLARE temp_shipment_time CHAR(1) DEFAULT NULL;
DECLARE temp_is_ceneo_tabs INT DEFAULT 0;
DECLARE has_ceneo_tarrif DECIMAL (8,2) DEFAULT NULL;
DECLARE publisher_ceneo INT DEFAULT 0;
DECLARE changed_prod_status INT;
DECLARE min_price DECIMAL(8,2);
DECLARE min_over DECIMAL(8,2);
DECLARE shop_main_category INT(10);
DECLARE source_min_over DECIMAL(8,2);

DECLARE done INT DEFAULT 0;

DECLARE pakiety CURSOR FOR    SELECT DISTINCT packet_id  FROM products_packets_items WHERE product_id=NEW.id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

SELECT price_netto INTO att_price_netto FROM products_attachments WHERE product_id = NEW.id;
SELECT discount, mark_up INTO new_general_discount, new_mark_up FROM products_general_discount WHERE source = NEW.source LIMIT 0,1;

IF ((att_price_netto IS NULL OR att_price_netto = 0) AND new_mark_up > 0 AND NEW.wholesale_price > 0) THEN

    SELECT (100 - ((((NEW.wholesale_price * (new_mark_up / 100)) + NEW.wholesale_price) / NEW.price_brutto) * 100)) INTO new_general_discount;
END IF;


IF (NEW.source <> OLD.source) OR (NEW.publisher_id <> OLD.publisher_id) OR (NEW.price_brutto <> OLD.price_brutto) OR (NEW.wholesale_price <> OLD.wholesale_price) THEN
  IF (NEW.packet='0') THEN
      SELECT IFNULL(discount_limit,0) INTO publisher_discount_limt FROM products_publishers WHERE id = NEW.publisher_id;
          SELECT IFNULL(PS.discount_limit,0) INTO series_discount_limt FROM products_series AS PS
          JOIN products_to_series AS PTS ON
                 PS.id = PTS.series_id
          WHERE PS.publisher_id = NEW.publisher_id AND PTS.product_id = NEW.id
          LIMIT 1;
      SELECT IFNULL(discount_limit,0) INTO source_discount_limt FROM products_source_discount WHERE first_source = NEW.created_by;

      IF (publisher_discount_limt > 0) AND (new_general_discount > publisher_discount_limt) THEN
          SELECT publisher_discount_limt INTO new_general_discount;
      END IF;

      IF (source_discount_limt > 0) AND (new_general_discount > source_discount_limt) THEN
          SELECT source_discount_limt INTO new_general_discount;
      END IF;

      IF (series_discount_limt > 0) AND (new_general_discount > series_discount_limt) THEN
          SELECT series_discount_limt INTO new_general_discount;
      END IF;

      SELECT discount INTO old_products_tarrifs_general_discount FROM products_tarrifs WHERE product_id = NEW.id AND general_discount = '1' LIMIT 0,1;
      IF (old_products_tarrifs_general_discount <> new_general_discount) OR (NEW.price_brutto <> OLD.price_brutto) THEN
        UPDATE products_tarrifs
        SET discount = new_general_discount,
         discount_value = ROUND(NEW.price_brutto*(discount/100), 2),
         price_brutto = NEW.price_brutto-discount_value,
         price_netto = ROUND(price_brutto / (1 + NEW.vat/100), 2)
        WHERE product_id = NEW.id AND general_discount = '1';
      END IF;
  END IF;
END IF;

IF NEW.price_brutto <> OLD.price_brutto THEN
  UPDATE products_tarrifs
  SET discount_value = ROUND(NEW.price_brutto*(discount/100), 2),
   price_brutto = NEW.price_brutto-discount_value,
   price_netto = ROUND(price_brutto / (1 + NEW.vat/100), 2)
  WHERE product_id = NEW.id;
END IF;



IF NEW.prod_status <> OLD.prod_status OR NEW.published <> OLD.published THEN
  IF NEW.prod_status = '0' OR NEW.prod_status = '2' OR NEW.published = '0' THEN
     UPDATE products_packets_items SET upd_packet_navailable = '0' WHERE NEW.id=product_id;
  END IF;
END IF;



IF NEW.prod_status <> OLD.prod_status OR NEW.published <> OLD.published THEN
  IF NEW.prod_status = '1' AND NEW.published = '1' THEN
     UPDATE products_packets_items SET upd_packet_navailable = '2' WHERE NEW.id=product_id;
  END IF;
END IF;



IF (NEW.published = '1' AND NEW.published <> OLD.published AND NEW.publication_year = YEAR(NOW())) THEN
 IF ((SELECT id FROM products_news WHERE product_id = NEW.id) > 0 ) THEN
     UPDATE products_news SET news_from = NOW() WHERE product_id = NEW.id;
 ELSE
     INSERT INTO products_news_become SET product_id = NEW.id ON DUPLICATE KEY UPDATE product_id = NEW.id;
 END IF;
END IF;



IF (
  NEW.`name` <> OLD.`name` OR
  NEW.`isbn_plain` <> OLD.`isbn_plain` OR
  NEW.`isbn_10` <> OLD.`isbn_10` OR
  NEW.`isbn_13` <> OLD.`isbn_13` OR
  NEW.`ean_13` <> OLD.`ean_13` OR
  NEW.`publisher_id` <> OLD.`publisher_id` OR
  NEW.`publication_year` <> OLD.`publication_year` OR
  NEW.`edition` <> OLD.`edition` OR
  NEW.`legal_status_date` <> OLD.`legal_status_date` OR
  NEW.`city` <> OLD.`city` OR
  NEW.`pages` <> OLD.`pages` OR
  NEW.`binding` <> OLD.`binding` OR
  NEW.`dimension` <> OLD.`dimension` OR
  NEW.`language` <> OLD.`language` OR
  NEW.`translator` <> OLD.`translator` OR
  NEW.`original_language` <> OLD.`original_language` OR
  NEW.`original_title` <> OLD.`original_title` OR
  NEW.`weight` <> OLD.`weight` OR
  NEW.`volumes` <> OLD.`volumes` OR
  NEW.`volume_nr` <> OLD.`volume_nr` OR
  NEW.`volume_name` <> OLD.`volume_name` OR
  NEW.`id` <> OLD.`id`
) THEN
  UPDATE products_last_update SET last_update = NOW() WHERE product_id = NEW.id;
END IF;

SELECT product_id INTO ceneo_tab FROM main_profit24.ceneo_tabs_items WHERE product_id = NEW.id AND (`type`= 0 OR `type`= 2);
SELECT include_publishers_products INTO publisher_ceneo FROM main_profit24.products_publishers WHERE id = NEW.publisher_id;

SET temp_shipment_time = NEW.shipment_time;
SET temp_prod_status = NEW.prod_status;

IF (0 != ceneo_tab || 0 != publisher_ceneo) THEN

	SET temp_is_ceneo_tabs = 1;

	IF (temp_prod_status = '3') THEN
		SET temp_prod_status = '1';
	END IF;

	IF (temp_shipment_time = '1') THEN
		SET temp_shipment_time = '0';
	END IF;

END IF;

SELECT price_brutto INTO has_ceneo_tarrif FROM products_tarrifs WHERE product_id = NEW.id AND `type`= 1 LIMIT 1;


IF (has_ceneo_tarrif IS NOT NULL AND NEW.wholesale_price > 0) THEN

	SELECT minimal_overhead INTO min_over FROM main_profit24.website_ceneo_settings WHERE website_id = 1 LIMIT 1;
	SELECT minimal_overhead INTO source_min_over FROM main_profit24.products_general_discount_common WHERE source = NEW.source AND website_id = 1;

	IF(source_min_over > 0) THEN
		SET min_over = source_min_over;
	END IF;

	SELECT WCC.website_id INTO shop_main_category FROM main_profit24.products AS P JOIN main_profit24.website_ceneo_categories AS WCC ON WCC.category_id = P.main_category_id where P.id = NEW.id;

	SET min_price = ROUND( NEW.wholesale_price + ((NEW.wholesale_price * min_over) / 100), 2 );

        IF( (has_ceneo_tarrif < NEW.wholesale_price AND NEW.wholesale_price > 0) OR NEW.price_brutto != OLD.price_brutto ) THEN

   	     DELETE FROM products_tarrifs WHERE product_id = NEW.id AND type = 1;
        END IF;


	IF(has_ceneo_tarrif < min_price AND shop_main_category = 1 AND NEW.wholesale_price != NEW.price_brutto) THEN



		DELETE FROM products_tarrifs WHERE product_id = NEW.id AND type = 1;


		DELETE PT FROM main_nieprzeczytane.products_tarrifs AS PT JOIN main_nieprzeczytane.products AS P ON P.id = PT.product_id WHERE P.profit24_id = NEW.id AND PT.type = 1;
		DELETE PT FROM main_mestro.products_tarrifs AS PT JOIN main_mestro.products AS P ON P.id = PT.product_id WHERE P.profit24_id = NEW.id AND PT.type = 1;


		DELETE FROM main_profit24.tarrifs_buffor WHERE shop_product_id = NEW.id AND website_id = 1;

	END IF;

END IF;

IF(OLD.name != NEW.name OR OLD.publication_year != NEW.publication_year OR OLD.publisher_id != NEW.publisher_id OR OLD.isbn != NEW.isbn OR OLD.isbn_10 != NEW.isbn_10 OR OLD.isbn_13 != NEW.isbn_13 OR OLD.ean_13 != NEW.ean_13 OR OLD.prod_status != NEW.prod_status OR OLD.published != NEW.published OR OLD.is_news != NEW.is_news OR OLD.is_bestseller != NEW.is_bestseller OR OLD.is_previews != NEW.is_previews OR OLD.wholesale_price != NEW.wholesale_price OR OLD.price_brutto != NEW.price_brutto) THEN

INSERT INTO product_changes_data (product_id, has_changed, elastic_has_changed) VALUES (NEW.id, 1, 0) ON DUPLICATE KEY UPDATE product_id = NEW.id, has_changed = 1, elastic_has_changed = 0, updated = NOW();

END IF;
END