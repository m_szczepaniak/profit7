CREATE TABLE sell_predict
(
    id INT(10) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
    quantity INT(10) unsigned NOT NULL,
    product_id INT(10) unsigned NOT NULL,
    type CHAR(1) NOT NULL,
    status CHAR(1) COMMENT '1-aktywne
2-kompletowane
0-nieaktywne',
    order_by INT(11) NOT NULL,
    created DATETIME NOT NULL,
    created_by VARCHAR(32) NOT NULL,
    CONSTRAINT fk_sell_predict_products1 FOREIGN KEY (product_id) REFERENCES products (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX fk_sell_predict_products1_idx ON sell_predict (product_id);