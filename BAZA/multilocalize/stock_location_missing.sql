CREATE TABLE IF NOT EXISTS `stock_location_missing` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `products_id` INT(10) UNSIGNED NOT NULL,
  `required_quantity` INT NOT NULL,
  `available_quantity` INT NOT NULL,
  `created` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_table1_products1_idx` (`products_id` ASC),
  CONSTRAINT `fk_table1_products1`
  FOREIGN KEY (`products_id`)
  REFERENCES `products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;