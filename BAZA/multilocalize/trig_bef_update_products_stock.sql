BEGIN

DECLARE curr_prod_status ENUM('0','1','2','3');
DECLARE curr_published ENUM('0','1');
DECLARE curr_ommit_auto_preview CHAR(1);
DECLARE komunikat VARCHAR(255);

DECLARE NEW_prod_status ENUM('0','1','2','3');
DECLARE NEW_prod_location VARCHAR(45);
DECLARE NEW_prod_price_brutto DECIMAL(8,2);
DECLARE NEW_prod_price_netto DECIMAL(8,2);
DECLARE MIN_wholesale_price DECIMAL(8,2);
DECLARE CMP_wholesale_price DECIMAL(8,2);
DECLARE NEW_prod_vat TINYINT(4);
DECLARE NEW_prod_source CHAR(2);
DECLARE NEW_prod_shipment_time ENUM('0', '1', '2', '3');
DECLARE NEW_prod_shipment_date DATE;
DECLARE att_price_netto DECIMAL(8,2);
DECLARE changed_prod_status INT;
DECLARE original_prod_status INT;
DECLARE price_brutto_current_tarrif DECIMAL(8,2);


SELECT '0.00' INTO NEW_prod_price_brutto;
SELECT 0.00 INTO MIN_wholesale_price;
SELECT 0.00 INTO CMP_wholesale_price;
SELECT '' INTO NEW_prod_location;
SELECT 'TEST' INTO komunikat;

SELECT price_netto INTO att_price_netto FROM products_attachments WHERE product_id = NEW.id;
SELECT prod_status,published,ommit_auto_preview INTO curr_prod_status,curr_published,curr_ommit_auto_preview FROM products WHERE id = NEW.id;

IF(curr_prod_status <> '2') THEN
      SELECT '1' INTO NEW_prod_status;

   IF (NEW.profit_g_status > 0 AND NEW.profit_g_price_brutto > 0.00) THEN
      SELECT '10' INTO NEW_prod_source;
      SELECT NEW.profit_g_wholesale_price INTO MIN_wholesale_price;
      SELECT NEW.profit_g_price_brutto INTO NEW_prod_price_brutto;
      SELECT NEW.profit_g_price_netto INTO NEW_prod_price_netto;
      SELECT NEW.profit_g_vat INTO NEW_prod_vat;
      SELECT '0' INTO NEW_prod_shipment_time;
      SELECT NEW.profit_g_location INTO NEW_prod_location;
      SELECT NEW.profit_g_shipment_date INTO NEW_prod_shipment_date;
      IF (NEW.stock_)
      SELECT 'G' INTO komunikat;
   ELSEIF (NEW.profit_e_status > 0 AND NEW.profit_e_price_brutto > 0.00) THEN
      SELECT '1' INTO NEW_prod_source;
      SELECT NEW.profit_e_wholesale_price INTO MIN_wholesale_price;
      SELECT NEW.profit_e_price_brutto INTO NEW_prod_price_brutto;
      SELECT NEW.profit_e_price_netto INTO NEW_prod_price_netto;
      SELECT NEW.profit_e_vat INTO NEW_prod_vat;
      SELECT '0' INTO NEW_prod_shipment_time;
      SELECT NEW.profit_e_location INTO NEW_prod_location;
      SELECT NEW.profit_e_shipment_date INTO NEW_prod_shipment_date;
      SELECT 'E' INTO komunikat;
   ELSEIF(NEW.profit_j_status > 0 AND NEW.profit_j_price_brutto > 0.00) THEN
      SELECT '0' INTO NEW_prod_source;
      SELECT NEW.profit_j_wholesale_price INTO MIN_wholesale_price;
      SELECT NEW.profit_j_price_brutto INTO NEW_prod_price_brutto;
      SELECT NEW.profit_j_price_netto INTO NEW_prod_price_netto;
      SELECT NEW.profit_j_vat INTO NEW_prod_vat;
      SELECT '0' INTO NEW_prod_shipment_time;
      SELECT NEW.profit_j_location INTO NEW_prod_location;
      SELECT NEW.profit_j_shipment_date INTO NEW_prod_shipment_date;
      SELECT 'J' INTO komunikat;
   ELSEIF (NEW.profit_m_status > 0 AND NEW.profit_m_price_brutto > 0.00) THEN
      SELECT '1' INTO NEW_prod_source;
      SELECT NEW.profit_m_wholesale_price INTO MIN_wholesale_price;
      SELECT NEW.profit_m_price_brutto INTO NEW_prod_price_brutto;
      SELECT NEW.profit_m_price_netto INTO NEW_prod_price_netto;
      SELECT NEW.profit_m_vat INTO NEW_prod_vat;
      SELECT '0' INTO NEW_prod_shipment_time;
      SELECT NEW.profit_m_location INTO NEW_prod_location;
      SELECT NEW.profit_m_shipment_date INTO NEW_prod_shipment_date;
      SELECT 'M' INTO komunikat;
   ELSEIF (NEW.profit_x_status > 0 AND NEW.profit_x_price_brutto > 0.00) THEN
      SELECT '1' INTO NEW_prod_source;
      SELECT NEW.profit_x_wholesale_price INTO MIN_wholesale_price;
      SELECT NEW.profit_x_price_brutto INTO NEW_prod_price_brutto;
      SELECT NEW.profit_x_price_netto INTO NEW_prod_price_netto;
      SELECT NEW.profit_x_vat INTO NEW_prod_vat;
      SELECT '0' INTO NEW_prod_shipment_time;
      SELECT NEW.profit_x_location INTO NEW_prod_location;
      SELECT NEW.profit_x_shipment_date INTO NEW_prod_shipment_date;
      SELECT 'X' INTO komunikat;
   ELSE


      IF (NEW.abe_status > 0 AND NEW.abe_price_brutto > 0.00 AND (NEW_prod_price_brutto = 0.00 OR (NEW_prod_price_brutto > 0.00 AND NEW_prod_price_brutto > (NEW.abe_status * NEW.abe_price_brutto)))) THEN
          SELECT CAST(NEW.abe_status AS CHAR) INTO NEW_prod_status;
          SELECT NEW.abe_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.abe_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.abe_vat INTO NEW_prod_vat;
          SELECT NEW.abe_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.abe_shipment_date INTO NEW_prod_shipment_date;
          SELECT '3' INTO NEW_prod_source;
          SELECT 'abe' INTO komunikat;
      END IF;

      IF (NEW.azymut_status > 0 AND NEW.azymut_price_brutto > 0.00 AND NEW.azymut_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.azymut_status * NEW.azymut_wholesale_price)))) THEN
          SELECT CAST(NEW.azymut_status AS CHAR) INTO NEW_prod_status;
          SELECT (NEW.azymut_status * NEW.azymut_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.azymut_wholesale_price INTO MIN_wholesale_price;
          SELECT NEW.azymut_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.azymut_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.azymut_vat INTO NEW_prod_vat;
          SELECT NEW.azymut_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.azymut_shipment_date INTO NEW_prod_shipment_date;
          SELECT '2' INTO NEW_prod_source;
          SELECT 'azymut' INTO komunikat;
      END IF;


      IF (NEW.helion_status > 0 AND NEW.helion_price_brutto > 0.00 AND NEW.helion_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.helion_status * NEW.helion_wholesale_price)))) THEN
          SELECT CAST(NEW.helion_status AS CHAR) INTO NEW_prod_status;
          SELECT NEW.helion_wholesale_price INTO MIN_wholesale_price;
          SELECT (NEW.helion_status * NEW.helion_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.helion_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.helion_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.helion_vat INTO NEW_prod_vat;
          SELECT NEW.helion_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.helion_shipment_date INTO NEW_prod_shipment_date;
          SELECT '4' INTO NEW_prod_source;
          SELECT 'helion' INTO komunikat;
      END IF;

      IF (NEW.dictum_status > 0 AND NEW.dictum_price_brutto > 0.00 AND NEW.dictum_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.dictum_status * NEW.dictum_wholesale_price)))) THEN
          SELECT CAST(NEW.dictum_status AS CHAR) INTO NEW_prod_status;
          SELECT NEW.dictum_wholesale_price INTO MIN_wholesale_price;
          SELECT (NEW.dictum_status * NEW.dictum_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.dictum_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.dictum_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.dictum_vat INTO NEW_prod_vat;
          SELECT NEW.dictum_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.dictum_shipment_date INTO NEW_prod_shipment_date;
          SELECT '7' INTO NEW_prod_source;
          SELECT 'dictum' INTO komunikat;
      END IF;

      IF (NEW.olesiejuk_status > 0 AND NEW.olesiejuk_price_brutto > 0.00 AND NEW.olesiejuk_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.olesiejuk_status * NEW.olesiejuk_wholesale_price)))) THEN
          SELECT CAST(NEW.olesiejuk_status AS CHAR) INTO NEW_prod_status;
          SELECT NEW.olesiejuk_wholesale_price INTO MIN_wholesale_price;
          SELECT (NEW.olesiejuk_status * NEW.olesiejuk_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.olesiejuk_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.olesiejuk_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.olesiejuk_vat INTO NEW_prod_vat;
          SELECT NEW.olesiejuk_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.olesiejuk_shipment_date INTO NEW_prod_shipment_date;
          SELECT '9' INTO NEW_prod_source;
          SELECT 'olesiejuk' INTO komunikat;
      END IF;



      IF (NEW.siodemka_status > 0 AND NEW.siodemka_price_brutto > 0.00 AND NEW.siodemka_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.siodemka_status * NEW.siodemka_wholesale_price)))) THEN
          SELECT CAST(NEW.siodemka_status AS CHAR) INTO NEW_prod_status;
          SELECT NEW.siodemka_wholesale_price INTO MIN_wholesale_price;
          SELECT (NEW.siodemka_status * NEW.siodemka_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.siodemka_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.siodemka_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.siodemka_vat INTO NEW_prod_vat;
          SELECT NEW.siodemka_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.siodemka_shipment_date INTO NEW_prod_shipment_date;
          SELECT '8' INTO NEW_prod_source;
          SELECT 'siodemka' INTO komunikat;
      END IF;

      IF (NEW.ateneum_status > 0 AND NEW.ateneum_price_brutto > 0.00 AND NEW.ateneum_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.ateneum_status * NEW.ateneum_wholesale_price)))) THEN
          SELECT CAST(NEW.ateneum_status AS CHAR) INTO NEW_prod_status;
          SELECT (NEW.ateneum_status * NEW.ateneum_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.ateneum_wholesale_price INTO MIN_wholesale_price;
          SELECT NEW.ateneum_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.ateneum_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.ateneum_vat INTO NEW_prod_vat;
          SELECT NEW.ateneum_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.ateneum_shipment_date INTO NEW_prod_shipment_date;
          SELECT '5' INTO NEW_prod_source;
          SELECT 'ateneum' INTO komunikat;
      END IF;

      IF (NEW.platon_status > 0 AND NEW.platon_price_brutto > 0.00 AND NEW.platon_wholesale_price > 0.00 AND (CMP_wholesale_price = 0.00 OR (CMP_wholesale_price > 0.00 AND CMP_wholesale_price > (NEW.platon_status * NEW.platon_wholesale_price)))) THEN
          SELECT CAST(NEW.platon_status AS CHAR) INTO NEW_prod_status;
          SELECT (NEW.platon_status * NEW.platon_wholesale_price) INTO CMP_wholesale_price;
          SELECT NEW.platon_wholesale_price INTO MIN_wholesale_price;
          SELECT NEW.platon_price_brutto INTO NEW_prod_price_brutto;
          SELECT NEW.platon_price_netto INTO NEW_prod_price_netto;
          SELECT NEW.platon_vat INTO NEW_prod_vat;
          SELECT NEW.platon_shipment_time INTO NEW_prod_shipment_time;
          SELECT NEW.platon_shipment_date INTO NEW_prod_shipment_date;
          SELECT '11' INTO NEW_prod_source;
          SELECT 'platon' INTO komunikat;
      END IF;

      IF (NEW.azymut_status > 0
		  AND att_price_netto IS NOT NULL
          AND att_price_netto > 0.00
          AND NEW.azymut_price_brutto IS NOT NULL
          AND NEW.azymut_price_brutto > 0.00) THEN

		SELECT CAST(NEW.azymut_status AS CHAR) INTO NEW_prod_status;
        SELECT (NEW.azymut_status * NEW.azymut_wholesale_price) INTO CMP_wholesale_price;
        SELECT NEW.azymut_wholesale_price INTO MIN_wholesale_price;
        SELECT NEW.azymut_price_brutto INTO NEW_prod_price_brutto;
        SELECT NEW.azymut_price_netto INTO NEW_prod_price_netto;
        SELECT NEW.azymut_vat INTO NEW_prod_vat;
		SELECT NEW.azymut_shipment_time INTO NEW_prod_shipment_time;
		SELECT NEW.azymut_shipment_date INTO NEW_prod_shipment_date;
        SELECT '2' INTO NEW_prod_source;
        SELECT 'azymut' INTO komunikat;
      END IF;
   END IF;

  IF (curr_ommit_auto_preview = '0' AND NEW_prod_source <> '' AND NEW_prod_price_brutto > 0.00) THEN
      UPDATE products
          SET
              price_brutto = NEW_prod_price_brutto,
              price_netto = NEW_prod_price_netto,
              vat = NEW_prod_vat,
              wholesale_price = MIN_wholesale_price,
              source = NEW_prod_source,
              shipment_time = NEW_prod_shipment_time,
              shipment_date = NEW_prod_shipment_date,
              last_import = NOW(),
              prod_status = NEW_prod_status,
              location = NEW_prod_location
      WHERE id = NEW.id;
  ELSE
      IF(curr_prod_status = '3') THEN
          SELECT '3' INTO NEW_prod_status;
      ELSE
          SELECT '0' INTO NEW_prod_status;
      END IF;

      UPDATE products
      SET last_import = NOw(),
          prod_status = NEW_prod_status
      WHERE id = NEW.id;
  END IF;

 IF(curr_published = '1') THEN
      SELECT price_brutto INTO price_brutto_current_tarrif
                                 FROM products_tarrifs
                                 WHERE product_id = NEW.id AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
                                 ORDER BY type DESC, discount DESC LIMIT 0,1;
     UPDATE products_shadow A
     JOIN products B
     ON B.id=A.id
     SET A.price_brutto = B.price_brutto,
         A.prod_status = B.prod_status,
         A.shipment_time = B.shipment_time,
                   A.shipment_date = B.shipment_date,
         A.price = price_brutto_current_tarrif
     WHERE A.id = NEW.id;
 END IF;

END IF;


END