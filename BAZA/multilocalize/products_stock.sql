ALTER TABLE `products_stock`
  ADD `stock_status` INT UNSIGNED NOT NULL DEFAULT '0'
  AFTER `profit_g_reservations`,
  ADD `stock_act_stock` MEDIUMINT NOT NULL DEFAULT '0'
  AFTER `stock_status`,
  ADD `stock_reservations` INT NOT NULL DEFAULT '0'
  AFTER `stock_act_stock`,
  ADD `stock_last_import` DATETIME NOT NULL
  AFTER `stock_reservations`;
