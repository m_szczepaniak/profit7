CREATE TABLE magazine
(
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(45) NOT NULL,
    type CHAR(4) NOT NULL
);
CREATE UNIQUE INDEX type_UNIQUE ON magazine (type);

INSERT INTO `magazine` (`id`, `name`, `type`) VALUES (NULL, 'B', 'SH'), (NULL, 'A', 'SL');



CREATE TABLE stock_location
(
    id INT(10) unsigned NOT NULL AUTO_INCREMENT,
    magazine_type CHAR(4) NOT NULL,
    products_stock_id INT(10) unsigned NOT NULL,
    container_id BIGINT(10) unsigned zerofill NOT NULL,
    quantity MEDIUMINT(9) unsigned NOT NULL,
    reservation MEDIUMINT(9) unsigned NOT NULL,
    available MEDIUMINT(9) unsigned NOT NULL,
    CONSTRAINT `PRIMARY` PRIMARY KEY (id, magazine_type),
    CONSTRAINT fk_stock_location_magazine1 FOREIGN KEY (magazine_type) REFERENCES magazine (type) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_stock_location_products_stock1 FOREIGN KEY (products_stock_id) REFERENCES products_stock (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_stock_location_containers1 FOREIGN KEY (container_id) REFERENCES containers (id) ON DELETE CASCADE ON UPDATE CASCADE
);
CREATE INDEX fk_stock_location_containers1_idx ON stock_location (container_id);
CREATE INDEX fk_stock_location_magazine1_idx ON stock_location (magazine_type);
CREATE INDEX fk_stock_location_products_stock1_idx ON stock_location (products_stock_id);



    CREATE TABLE stock_location_orders_items_lists_items
    (
        id INT(10) unsigned PRIMARY KEY NOT NULL AUTO_INCREMENT,
        stock_location_id INT(10) unsigned NOT NULL,
        stock_location_magazine_type CHAR(4) NOT NULL,
        orders_items_lists_items_id INT(10) unsigned NOT NULL,
        reserved_quantity MEDIUMINT(9) unsigned NOT NULL,
        type CHAR(4) NOT NULL,
        created_by VARCHAR(64) NOT NULL,
        created DATETIME NOT NULL,
        confirmed_quantity MEDIUMINT(9) unsigned DEFAULT '0',
        status CHAR(1) DEFAULT '1',
        reservation_subtracted CHAR(1) DEFAULT '0',
        CONSTRAINT fk_stock_location_orders_items_lists_items_stock_location1 FOREIGN KEY (stock_location_id, stock_location_magazine_type) REFERENCES stock_location (id, magazine_type) ON DELETE CASCADE ON UPDATE CASCADE,
        CONSTRAINT fk_stock_location_orders_items_lists_items_orders_items_lists1 FOREIGN KEY (orders_items_lists_items_id) REFERENCES orders_items_lists_items (id) ON DELETE CASCADE ON UPDATE CASCADE
    );
    CREATE INDEX fk_stock_location_orders_items_lists_items_orders_items_lis_idx ON stock_location_orders_items_lists_items (orders_items_lists_items_id);
    CREATE INDEX fk_stock_location_orders_items_lists_items_stock_location1_idx ON stock_location_orders_items_lists_items (stock_location_id, stock_location_magazine_type);

