CREATE TABLE IF NOT EXISTS `missing_high_oili` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `oili_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_missing_high_oili_orders_items_lists_items1_idx` (`oili_id` ASC),
  CONSTRAINT `fk_missing_high_oili_orders_items_lists_items1`
    FOREIGN KEY (`oili_id`)
    REFERENCES `orders_items_lists_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;



CREATE TABLE IF NOT EXISTS `missing_high_items` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` INT(10) UNSIGNED NOT NULL,
  `missing_high_oili_id` INT UNSIGNED NOT NULL,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_missing_high_items_products1_idx` (`product_id` ASC),
  INDEX `fk_missing_high_items_missing_high_oili1_idx` (`missing_high_oili_id` ASC),
  CONSTRAINT `fk_missing_high_items_products1`
    FOREIGN KEY (`product_id`)
    REFERENCES `products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_missing_high_items_missing_high_oili1`
    FOREIGN KEY (`missing_high_oili_id`)
    REFERENCES `missing_high_oili` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;