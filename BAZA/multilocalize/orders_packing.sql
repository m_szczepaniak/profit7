CREATE TABLE IF NOT EXISTS `orders_packing` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` INT(10) UNSIGNED NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_orders_packing_orders1_idx` (`order_id` ASC),
  CONSTRAINT `fk_orders_packing_orders1`
    FOREIGN KEY (`order_id`)
    REFERENCES `orders` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;