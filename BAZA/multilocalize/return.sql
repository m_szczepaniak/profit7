CREATE TABLE IF NOT EXISTS `magazine_return` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(45) NULL,
  `contractor_name` VARCHAR(128) NOT NULL,
  `magazine_type` INT NOT NULL,
  `status` CHAR(1) NOT NULL,
  `created` DATETIME NOT NULL,
  `created_by` VARCHAR(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `number_UNIQUE` (`number` ASC),
  INDEX `fk_magazine_return_magazine1_idx` (`magazine_type` ASC),
  CONSTRAINT `fk_magazine_return_magazine1`
  FOREIGN KEY (`magazine_type`)
  REFERENCES `magazine` (`type`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

CREATE TABLE magazine_return_items
(
    id INT(10) unsigned NOT NULL AUTO_INCREMENT,
    magazine_return_id INT(10) unsigned NOT NULL,
    quantity INT(10) unsigned,
    confirmed_quantity INT(10) unsigned NOT NULL,
    stock_location_orders_items_lists_items_id INT(10) unsigned NOT NULL,
    orders_items_lists_items_id INT(10) unsigned NOT NULL,
    product_id INT(10) unsigned
);
CREATE INDEX fk_magazine_ers_items_lists_items1_idx ON magazine_return_items (orders_items_lists_items_id);
CREATE INDEX fk_magazine_return_items_magazine_return1_idx ON magazine_return_items (magazine_return_id);
CREATE INDEX fk_magazine__orders_items_lists_items1_idx ON magazine_return_items (stock_location_orders_items_lists_items_id);
CREATE UNIQUE INDEX magazine_return_items_product_id_pk ON magazine_return_items (product_id);