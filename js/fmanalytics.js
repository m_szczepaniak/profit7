/**
 * FreshMail marketing automation
 * preparing userId for Google Analytics
 * @author Grzegorz Gorczyca
 * @version 1.0.0
 */
var FMGA = function() {
    var userId = null;
    var userIdRegEx = /FmUserId=([a-zA-Z0-9]+)/;
    var cookieLifeTime = 2592000000; //30 days

    var _getUserId = function() {
        _getUserIdParamFromUrl();
        if(userId === null) {
            _getUserIdParamFromCookie();
        }
        _setCookie();
    };

    var _getUserIdParamFromUrl = function() {
        if (userId === null && window.location.href.indexOf('FmUserId') > -1) {
            _setUserId(userIdRegEx.exec(window.location.href));
        }
    };

    var _getUserIdParamFromCookie = function() {
        if (userId === null && document.cookie.indexOf('FmUserId') > -1) {
            _setUserId(userIdRegEx.exec(document.cookie));
        }
    };

    var _setUserId = function(param) {
        if (param !== null) {
            userId = param[1];
        }
    };

    var _setCookie = function() {
        if(userId !== null) {
            var expire = new Date();
            expire.setTime(new Date().getTime() + cookieLifeTime);
            document.cookie = "FmUserId="+escape(userId) + ";expires="+expire.toGMTString() + ';path=/';
        }
    };

    return {
        getUserIdObject: function() {
            _getUserId();
            if (userId === null) {
                return "auto";
            } else {
                return {
                    'userId' : userId
                };
            }
        },
        getUserId: function() {
            _getUserId();
            return userId;
        },
    };
}();
var FMUserId = FMGA.getUserId();
