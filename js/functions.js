function ShowMessageBox(sPrefix, sMsg, sPostfix, bError) {
	alert(sPrefix + "\n" + sMsg + sPostfix);
	return !bError;
}

function SetCookie(sName, mValue, iDays) {
  if (iDays) {
    var iDate = new Date();
    iDate.setTime(iDate.getTime()+(iDays*24*60*60*1000));
    var sExpires = "; expires="+iDate.toGMTString();
  }
  else var sExpires = "";
  document.cookie = sName+"="+mValue+sExpires+"; path=/";
}

function GetCookie(sName) {
	var nameEQ = sName + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}

function DeleteCookie(sName) {
	SetCookie(name, "", -1);
}

function deobfuscate(sStr) {
	var aChars = sStr.split(/-/);
	var sOut = '';
	for (var i = 0; i < aChars.length; i++) {
		sOut += String.fromCharCode(aChars[i]);
	}
	return sOut;
}

function toggleActivity(oCheckbox, fieldPrefix, disabledClass, enabledClass) {
	var oForm = oCheckbox.form;

	for (var i = 0; i < oForm.elements.length; i++) {
		if (oForm.elements[i].id.indexOf(fieldPrefix) == 0) {
			oForm.elements[i].disabled = !oCheckbox.checked;
			oForm.elements[i].className = (oCheckbox.checked ? enabledClass : disabledClass);
			if ((oLabel = document.getElementById(oForm.elements[i].name + '_label'))) {
				//oLabel.style.display = (oCheckbox.checked ? 'inline' : 'none');
			}
		}
	}
}

function validateCartForm(oForm, bPro, sMsg) {
	var isTransport = false;
	
	if (bPro) {
		var sPCRE = /^itm_Prom_[0-9]+$/;
	}
	else {
		var sPCRE = /^itm_[0-9]+$/;
	}
	for (var i = 0; i < oForm.elements.length; i++) {
		if(oForm.elements[i].type == "text"){
			if ((oForm.elements[i].id.match(sPCRE) && isNaN(parseInt(oForm.elements[i].value)) || oForm.elements[i].value < 0)) {
				oForm.elements[i].select();
				alert(sMsg);
				return false;
			}
			oForm.elements[i].value = parseInt(oForm.elements[i].value);
		}
	}
	
	return true;
}

function togglePollVisibility(oObj, iId, iQId) {
	if (aVisPoll[iQId]) {
		oCurrVis = document.getElementById('dep_' + aVisPoll[iQId]);
		oCurrVis.style.display = 'none';
	}
	oBlock = document.getElementById('dep_' + iId);
	oBlock.style.display = 'block';
	aVisPoll[iQId] = iId;
}

function clearRadios(oObj, sName)  {
	if (oObj.value.replace(/^\s+/, '').replace(/\s+$/, '') != '') {
		// czyszczenie przyciskow
		aItems = document.getElementsByName(sName);
		for (var i = 0; i < aItems.length; i++) {
			aItems[i].checked = false;
		}
	}
}

function clearText(iId)  {
	document.getElementById('qo_' + iId).value = '';
}

function hidePoll(iQId) {
	if (aVisPoll[iQId]) {
		oCurrVis = document.getElementById('dep_' + aVisPoll[iQId]);
		oCurrVis.style.display = 'none';
	}
}

function toggleVisibility(oA, sId) {
	oObj = document.getElementById(sId);
	oObj.style.display = oA.checked ? 'block' : 'none';
}

function toggleFormGroup(oObj) {
	
	if (aVisPoll[oObj.name]) {
		oCurrVis = document.getElementById('dep_' + aVisPoll[oObj.name]);
		oCurrVis.style.display = 'none';

	}
	
	iId=oObj.options[oObj.selectedIndex].value;

	oBlock = document.getElementById('dep_' + iId);
	if(oBlock){
		oBlock.style.display = 'block';
		aVisPoll[oObj.name] = iId;
	}
}

function RadioToggleFormGroup(oObj) {
	
	if (aVisPoll[oObj.name]) {
		oCurrVis = document.getElementById('dep_' + aVisPoll[oObj.name]);
		oCurrVis.style.display = 'none';

	}
	
	iId=oObj.value;

	oBlock = document.getElementById('dep_' + iId);
	if(oBlock){
		oBlock.style.display = 'block';
		aVisPoll[oObj.name] = iId;
	}
}

function hideFormGroup(iField) {

	if (aVisPoll[iField]) {

		oCurrVis = document.getElementById('dep_' + aVisPoll[iField]);

		oCurrVis.style.display = 'none';

	}

}
function toggleVisibility(oA, sId) {
	oObj = document.getElementById(sId);
	oObj.style.display = oA.checked ? 'block' : 'none';
}

function toggleVisibility2(oA, sId, sShowTxt, sHideTxt) {
	oDesc = document.getElementById(sId);
	if (oA.innerHTML == sShowTxt) {
		// pokazanie opisu
		oDesc.style.display = 'block';
		oA.innerHTML = sHideTxt;
	}
	else {
		// ukrycie opisu
		oDesc.style.display = 'none';
		oA.innerHTML = sShowTxt;
	}
}

function toggleSubMenuVisibility(iMId, bWriteCookie) {
	oMenuItem = document.getElementById('menu_item_' + iMId);
	oSubMenu = document.getElementById('sub_menu_' + iMId);
	oMenuItem.className = oSubMenu.style.display == 'none' ? 'selected' : '';
	oSubMenu.style.display = oSubMenu.style.display == 'none' ? 'block' : 'none';
	if (bWriteCookie) {
		var sSelMenusVal = '';
		var sDeselMenusVal = '';
		// zapisanie info o rozwinietym / zwinietym menu do ciasteczka
		// sprawdzenie czy juz jakies wartosci sa w ciasteczkach
		var sSelMenus = GetCookie("it-sel-menus");
		var sDeselMenus = GetCookie("it-desel-menus");
		// ciasteczko z rozwinietymi przez uzytkownika menu
		if (sSelMenus != null) {
			// rozdzielenie
			aSelMenus = sSelMenus.split("_");
			for (var i = 0; i < aSelMenus.length; i++) {
				if (aSelMenus[i].match(/[0-9]+/) && aSelMenus[i] != iMId.toString()) {
					sSelMenusVal += aSelMenus[i] + "_";
				}
			}
			if (oSubMenu.style.display == 'block') {
				// dodanie rozwinietego menu do ciasteczka
				sSelMenusVal += iMId + "_";
			}
			sSelMenusVal = sSelMenusVal.substr(0, sSelMenusVal.length - 1);
			if (sSelMenusVal == '') {
				// usuniecie ciasteczka
				DeleteCookie("it-sel-menus");
			}
		}
		else if (oSubMenu.style.display == 'block') {
			sSelMenusVal = iMId;
		}
		SetCookie("it-sel-menus", sSelMenusVal, 0);
		
		// ciasteczko ze zwinietymi przez uzytkownika menu
		if (sDeselMenus != null) {
			// rozdzielenie
			aDeselMenus = sDeselMenus.split("_");
			for (var i = 0; i < aDeselMenus.length; i++) {
				if (aDeselMenus[i].match(/[0-9]+/) && aDeselMenus[i] != iMId.toString()) {
					sDeselMenusVal += aDeselMenus[i] + "_";
				}
			}
			if (oSubMenu.style.display == 'none') {
				// dodanie zwinietego menu do ciasteczka
				sDeselMenusVal += iMId + "_";
			}
			sDeselMenusVal = sDeselMenusVal.substr(0, sDeselMenusVal.length - 1);
			if (sDeselMenusVal == '') {
				// usuniecie ciasteczka
				DeleteCookie("it-desel-menus");
			}
		}
		else if (oSubMenu.style.display == 'none') {
			sDeselMenusVal = iMId;
		}
		SetCookie("it-desel-menus", sDeselMenusVal, 0);
	}
}

function activeTab(tab){
	var iNumber = tab.id.substring(3,2);
	var sPrefix = "boxTop_";
	var sCookieName = "beck_actTab";
	
	for(var i = 1; i < 5; i++){
		document.getElementById(sPrefix+i).style.display = "none";
		document.getElementById('l_'+i).className = "";
	}
	
	document.getElementById(sPrefix + iNumber).style.display = "block";
	document.getElementById("l_" + iNumber).className = "selected";
	
	document.cookie = sCookieName + "=" + iNumber + ";path=/";
}

function height_pol(els){
	var height = new Array();
	var max = 0;
	for( i = 0; i < els.length; i++){
		height = document.getElementById('pol_'+i).offsetHeight;
		if(height > max) max = height;
	}
	return max;
}



function changeItmValueUp(iId) {
		oInp = document.getElementById("itm_"+iId);
		oInp.value = (isNaN(oInp.value) ? 0 : parseInt(oInp.value)) + 1;
}
function changeItmValueDown(iId) {
	var oInp = document.getElementById("itm_"+iId);
	oInp.value = (isNaN(oInp.value) ? 0 : parseInt(oInp.value)) - 1;
	if( oInp.value < 0 ) {
		oInp.value = 0;
	}
}

function showAdv(){
	document.getElementById("advanced").style.display = document.getElementById("advanced").style.display == "block" ? "none" : "block";
	document.getElementById("adv").value = document.getElementById("adv").value == "1" ? "0" : "1";
}

function changeDest(oForm){
	dest = oForm.value;

	if ( dest == "1" ){
		document.getElementById("adv").value = "0";
		document.getElementById("showAdv").style.display = "block";
	} else {
		document.getElementById("adv").value = "0";
		document.getElementById("showAdv").style.display = "none";	
		document.getElementById("advanced").style.display = "none";
	}
}

function checkPayment(oForm){
	var bSelected = false;
	
	if(oForm.payment_type){
		for ( var i = 0; i < oForm.payment_type.length ; i++ ){
			if (oForm.payment_type[i].checked) bSelected = true;
		}
		if (bSelected == false && oForm.payment_type.length > 1){
			alert('Musisz wybrać formę płatności');
			return false;
		}
	}
	return true;
}



function cart_transport(fTransportPrice){
	var fPrice = document.getElementById("total_price_brutto").innerHTML;
	fPrice = fPrice.replace(',','.');
	fPrice = parseFloat(fPrice);
	var fTotal = fTransportPrice + fPrice;
	fTotal = String(fTotal);
	fTotal = fTotal.replace('.',',');
	if(fTotal.indexOf(",") == -1){
		fTotal = fTotal + ",00";
	} else {
		aTab = fTotal.split(",");
		if(aTab[1].length == 1){
			fTotal = fTotal + "0";
		}
	}

	document.getElementById("total_cost").innerHTML = fTotal;
}

function blink(id) {
	if(document.getElementById(id).style.visibility == "visible"){
		document.getElementById(id).style.visibility = "hidden";
	} else {
		document.getElementById(id).style.visibility = "visible";	
	}
	setTimeout("blink('" + id + "');", 1000);
}

function checkQuantity(iId, bPro) {
		oInp = document.getElementById((bPro ? "itm_Prom_" : "itm_") + iId);
		oInp.value = (isNaN(oInp.value) ? 0 : parseInt(oInp.value) == 0 ? 1 : parseInt(oInp.value));
}

function changePhoto(big, small) {
	var oImgCnt = document.getElementById('photoToBig');
	var oImgCnt2 = document.getElementById('photoToBigLink');
	var oImg = new Image();
	oImgCnt.src = '/omniaCMS/gfx/loading.gif';
	oImg.onload = function() {showImg(oImg, oImgCnt, oImgCnt2, big);}
	oImg.src = small;
}

function showImg(oImg, oImgCnt, oImgCnt2, big) {
	oImgCnt.src = oImg.src;
	oImgCnt2.href = big;
}
function pokaz (oA, id) {
	obj = document.getElementById(id);
	if(obj.style.display == "none") {
		obj.style.display="block";
		oA.className = 'rozwinieta';
	}
	else {
		obj.style.display="none";
		oA.className = 'zwinieta';
	}
}


function performAjaxValidation(sFormName,sData){
	$.ajax({
		   type: "POST",
		   url: "/internals/ajaxValidator.php",
		   data: sData,
		   async: false,
		   success: function(msg){
				var smsg = msg+"";
				$("#"+sFormName+" input:enabled").removeClass("validOK validErr validWarn");
				$(".validMsg").remove();
				var aFields = smsg.split(',');
				for(sfield in aFields){
					var aParams = aFields[sfield].split(':');
					if(aParams[0].length > 0){
						
						if(aParams[1] == "OK"){
							$("#"+sFormName+" input:enabled[name="+aParams[0]+"]").addClass("validOK");
						}
						else if(aParams[1] == "ERR"){
							$("#"+sFormName+" input:enabled[name="+aParams[0]+"]").addClass("validErr");
							if(aParams[2].length > 0){
								$("#"+sFormName+" .fRow:has(input[name="+aParams[0]+"])").append('<div id="'+aParams[0]+'_validMsg" class="validMsg">'+aParams[2]+'</div>');
							}
						}
						else if(aParams[1] == "WARN"){
							$("#"+sFormName+" input:enabled[name="+aParams[0]+"]").addClass("validWarn");
							if(aParams[2].length > 0){
								$("#"+sFormName+" .fRow:has(input[name="+aParams[0]+"])").append('<div id="'+aParams[0]+'_validMsg" class="validMsg">'+aParams[2]+'</div>');
							}
						}
					}
				}
			 },
		   error: function(msg){
		     //alert( "Błąd odpowiedzi serwera! Spróbuj ponownie" );
		   }
	});
}
function performFieldAjaxValidation(sFormName,sFieldName,sData){
	$.ajax({
		   type: "POST",
		   url: "/internals/ajaxOneFieldValidator.php?__field="+sFieldName,
		   data: sData,
		   success: function(msg){
				var smsg = msg+"";
				$("#"+sFieldName+"_validMsg").remove();
				$("#"+sFormName+" input:enabled[name="+sFieldName+"]").removeClass("validOK validErr validWarn");
				if(smsg != ""){
					var aFields = smsg.split(':');
					if(aFields[1].length > 0){
						if(aFields[1] == "OK"){
							$("#"+sFormName+" input:enabled[name="+sFieldName+"]").addClass("validOK");
						}
						else if(aFields[1] == "ERR"){
							$("#"+sFormName+" input:enabled[name="+sFieldName+"]").addClass("validErr");
							if(aFields[2].length > 0){
								$("#"+sFormName+" .fRow:has(input[name="+sFieldName+"])").append('<div id="'+sFieldName+'_validMsg" class="validMsg">'+aFields[2]+'</div>');
							}
						}
						else if(aFields[1] == "WARN"){
							$("#"+sFormName+" input:enabled[name="+sFieldName+"]").addClass("validWarn");
							if(aFields[2].length > 0){
								$("#"+sFormName+" .fRow:has(input[name="+sFieldName+"])").append('<div id="'+sFieldName+'_validMsg" class="validMsg">'+aFields[2]+'</div>');
							}
						}
					}
				}
			 },
		   error: function(msg){
		     //alert("Błąd odpowiedzi serwera! Spróbuj ponownie" );
		   }
	});
}