window.onload = setBehaviours;

function inputOnFocus (oObj, sTxt) {
	if (oObj.value == sTxt) oObj.value = '';
	//oObj.value = oObj.value.replace(sTxt, '');
}
function inputOnBlur (oObj, sTxt) {
	if (oObj.value == '') oObj.value = sTxt;
}

function sendSearchBoxForm(oForm) {
	if(oForm.top_cat.selectedIndex > 0) {
		SetCookie('box_categories',oForm.top_cat.selectedIndex-1,7);
	}
}

function sendSearchForm(oForm) {
	if(oForm.top_cat.selectedIndex > 0) {
		SetCookie('box_categories',oForm.top_cat.selectedIndex-1,7);
	}
}

function sendNewsletterBoxForm(oForm, sTxt) {
	var sEmail = oForm.n_email.value;
	sEmail = sEmail.replace(/^\s+/, '').replace(/\s+$/, '');
	
	if (sEmail == '' || sEmail == sTxt) {
		return false;
	}
}


function sendSorterForm(oForm) {
	/*var sLocation = (navigator.userAgent.indexOf('MSIE') != -1 ? ''  : '') + oForm.action;*/

	var sLocation = oForm.action;
	sLocation += '/sort:' + oForm.sort.options[oForm.sort.options.selectedIndex].value + ',filter:' + oForm.filter.options[oForm.filter.options.selectedIndex].value;
	if(oForm.current_page) {
		sLocation += ',p' + oForm.current_page.value;
	}
	window.location.href = sLocation + '.html';
	
}

function sendSearchSorterForm(oForm) {
	var sLocation = window.location.href;
	var sNewLoc = "";
	
	if(sLocation.indexOf(".html") != -1){
		if(sLocation.indexOf("sort") != -1){
			sLocation = sLocation.split(",sort");
			sNewLoc=sLocation[0];
		} else {
			sLocation= sLocation.split(".html");
			sNewLoc =sLocation[0];
		}
		sLocation = sNewLoc + ',sort:' + oForm.sort.options[oForm.sort.options.selectedIndex].value+'.html';
		
	} else {
		if(sLocation.indexOf("sort") != -1){
			sLocation = sLocation.split("&sort");
			sNewLoc=sLocation[0];
		} else {
			sNewLoc = sLocation;
		}
		sLocation = sNewLoc + '&sort=' + oForm.sort.options[oForm.sort.options.selectedIndex].value;
		
	}
	window.location.href = sLocation ;
}

function sendSearchSorterFilterForm(oForm) {
	var sLocation = window.location.href;
	var sNewLoc = "";
	
	if(sLocation.indexOf(".html") != -1){
		if(sLocation.indexOf("sort") != -1){
			sLocation = sLocation.split(",sort");
			sNewLoc=sLocation[0];
		} else {
			sLocation= sLocation.split(".html");
			sNewLoc =sLocation[0];
		}
		sLocation = sNewLoc + ',sort:' + oForm.sort.options[oForm.sort.options.selectedIndex].value
		+ ',filter:' + oForm.filter.options[oForm.filter.options.selectedIndex].value+'.html';
		
	} else {
		if(sLocation.indexOf("sort") != -1){
			sLocation = sLocation.split("&sort");
			sNewLoc=sLocation[0];
		} else {
			sNewLoc = sLocation;
		}
		sLocation = sNewLoc + '&sort=' + oForm.sort.options[oForm.sort.options.selectedIndex].value
		+ '&filter=' + oForm.filter.options[oForm.filter.options.selectedIndex].value;
		
	}
	window.location.href = sLocation ;
}

function sendSearchFilterForm(oForm) {
	var sLocation = window.location.href;
	var sNewLoc = "";
	
	if(sLocation.indexOf(".html") != -1){
		if(sLocation.indexOf("filter") != -1){
			sLocation = sLocation.split(",filter");
			sNewLoc=sLocation[0];
		} else {
			sLocation= sLocation.split(".html");
			sNewLoc =sLocation[0];
		}
		sLocation = sNewLoc + ',filter:' + oForm.filter.options[oForm.filter.options.selectedIndex].value+'.html';
		
	} else {
		if(sLocation.indexOf("filter") != -1){
			sLocation = sLocation.split("&filter");
			sNewLoc=sLocation[0];
		} else {
			sNewLoc = sLocation;
		}
		sLocation = sNewLoc + '&filter=' + oForm.filter.options[oForm.filter.options.selectedIndex].value;
		
	}
	window.location.href = sLocation ;
}

function hoverMenuButton(oLink, bHover) {
	oLink.style.backgroundPosition = bHover ? "19px 0" : "0 0";
}

function autoSelect(oElem){
	if(!SearchBoxInputHasFocus){
		oElem.select(); 
		oElem.focus();
	}
}
function setBehaviours() {
	
	if (document.getElementById) {
		// WYSZUKIWARKA
		// onsubmit
		if (oSearchBoxForm = document.getElementById('searchBoxForm')) {
			oSearchBoxForm.onsubmit = function() {return sendSearchBoxForm(oSearchBoxForm);};
		}
		if (oSearchForm = document.getElementById('searchForm')) {
			oSearchForm.onsubmit = function() {return sendSearchForm(oSearchForm);};
		}
		
		// NEWSLETTER
		// onfocus, onblur
		if (oNewsletterInp = document.getElementById('n_email')) {
			oNewsletterInp.value = sNewsletterTxt;
			oNewsletterInp.onfocus = function() {inputOnFocus(oNewsletterInp, sNewsletterTxt);};
			oNewsletterInp.onblur = function() {inputOnBlur(oNewsletterInp, sNewsletterTxt);};
		}
		// logowanie
		// onfocus, onblur
		if (oLoginInp = document.getElementById('b_login')) {
			oLoginInp.value = 'login';
			oLoginInp.onfocus = function() {inputOnFocus(oLoginInp, 'login');};
			oLoginInp.onblur = function() {inputOnBlur(oLoginInp, 'login');};
		}
		// onfocus, onblur
		if (oPasswdInp = document.getElementById('b_passwd')) {
			oPasswdInp.value = 'hasło';
			oPasswdInp.onfocus = function() {inputOnFocus(oPasswdInp, 'hasło');};
			oPasswdInp.onblur = function() {inputOnBlur(oPasswdInp, 'hasło');};
		}
		/*if (oSearchInp = document.getElementById('qibox')) {
			//if (oSearchInp.value == '') oSearchInp.value = 'wpisz tytuł, autora lub ISBN';
			oSearchInp.onfocus = function() {inputOnFocus(oSearchInp, 'wpisz tytuł, autora lub ISBN');};
			oSearchInp.onblur = function() {inputOnBlur(oSearchInp, 'wpisz tytuł, autora lub ISBN');};
			//oSearchInp.onkeyup = function() {inputOnFocus(oSearchInp, 'wpisz tytuł, autora lub ISBN');};
		}*/
		// onsubmit
		if (oNewsletterBoxForm = document.getElementById('newsletterBoxForm')) {
			oNewsletterBoxForm.onsubmit = function() {return sendNewsletterBoxForm(oNewsletterBoxForm, sNewsletterTxt);};
		}
		
		// FORMULARZ SORTOWANIA
		if (oSorterForm = document.getElementById('sorterForm')) {
			oSorterForm.sort.onchange = function() {sendSorterForm(oSorterForm)};
			oSorterForm.filter.onchange = function() {sendSorterForm(oSorterForm)};
		}
		
		// FORMULARZ SORTOWANIA W WYSZUKIWARCE
		if (oSorterSearcherForm = document.getElementById('sorterSearchForm')) {
			oSorterSearcherForm.sort.onchange = function() {sendSearchSorterForm(oSorterSearcherForm)};
		}
		
		// FORMULARZ SORTOWANIA W WYSZUKIWARCE po wydawcy, autorze, serii
		if (oSorterFilterSearcherForm = document.getElementById('sorterFilterSearchForm')) {
			oSorterFilterSearcherForm.sort.onchange = function() {sendSearchSorterFilterForm(oSorterFilterSearcherForm)};
			oSorterFilterSearcherForm.filter.onchange = function() {sendSearchSorterFilterForm(oSorterFilterSearcherForm)};
		}
		
		// FORMULARZ SORTOWANIA W WYSZUKIWARCE po wydawcy, autorze, serii
		if (oFilterSearcherForm = document.getElementById('FilterSearchForm')) {
			oFilterSearcherForm.filter.onchange = function() {sendSearchFilterForm(oFilterSearcherForm)};
		}
		
	}
}

