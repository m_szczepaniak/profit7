$(".produktWlasciwyOpis").ready(function(){
    function fixFontSize(element) {

        var fontSizeString = element.css("font-size");
        if (fontSizeString != '') {
            console.log(fontSizeString);
            regexpSize = /(\d+)(.+)/i;
            var patternFontSize = regexpSize.exec(fontSizeString);
            if (undefined !== patternFontSize[1] && undefined !== patternFontSize[2]) {
                var fontSize = parseInt(patternFontSize[1]);
                var fontType = patternFontSize[2];

                if (fontType === "pt") {
                    var defaultPTSize = 9;
                    var newDefaultPTSize = 12;
                    var diffSize = fontSize - defaultPTSize;
                    var newPTSize = newDefaultPTSize + diffSize;
                    element.css("font-size", newPTSize + "pt");
                }
                else if (fontType === "px") {
                    var defaultPTSize = 12;
                    var newDefaultPTSize = 16;
                    var diffSize = fontSize - defaultPTSize;
                    var newPTSize = newDefaultPTSize + diffSize;
                    element.css("font-size", newPTSize + "px");
                }
            }
        }
    }

    function checkAndFixFontSize(element) {
        var regexpFontSize = /size/;
        var style = $(element).attr("style");
        var size = $(element).attr("size");

        if ((size > 0 || true === regexpFontSize.test(style)) &&
            undefined !== $(element).css("font-size") &&
            '' != $(element).css("font-size")) {

            fixFontSize($(element));
        }
    }

    $(".produktWlasciwyOpis").children().each(function (index, element) {
        $(element).children().each(function (index, element) {
            $(element).children().each(function (index, element) {
                $(element).children().each(function (index, element) {
                    checkAndFixFontSize(element);
                });
                checkAndFixFontSize(element);
            });
            checkAndFixFontSize(element);
        });
        checkAndFixFontSize(element);
    });
});