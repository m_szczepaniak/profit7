## Tworzenie RAID 1
megacli -CfgLdAdd -r1 [252:2,252:3] -a0

- formatowanie partycji pod EXT4
mke4fs -t ext4 /dev/sdb

- mount lub wpis w fstab:
/dev/sdb1       /data    ext4    defaults,usrquota,grpquota,relatime     1       2


## Instalacja memcached

apt-get install memcached
apt-get install libmemcached-dev
apt-get install plesk-php54-dev
pecl install memcached-2.2.0
plesk bin php_handler --reread
- do memcached należy dorzucić konfigurację

## Konfiguracja poczty na serwerze profitu, problem z połączeniami
vi /etc/dovecot/conf.d/99-imap-login.conf
service imap-login {
        process_limit = 400
        process_min_avail = 16
}

## Duża ilośc połączeń i dropowanie pakietów
/etc/sysctl.conf:
dodaj linię:
net.netfilter.nf_conntrack_max = 655360
