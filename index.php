<?php

/*
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
gc_enable();
*/

error_reporting(E_ERROR | E_WARNING | E_PARSE | E_USER_NOTICE);
ini_set('display_errors','On');

global $aConfig;
/**        MAINTENANCE MODE                      */
if(file_exists('przerwa/przerwa_konfiguracja.php')){
	include_once('przerwa/przerwa_konfiguracja.php');
	if (!isset($_COOKIE['maintance_mode']) || $_COOKIE['maintance_mode'] != $przerwa_klucz) {
		if (isset($_GET['przerwa']) && $_GET['przerwa'] == $przerwa_klucz) {
			setcookie('maintance_mode', $przerwa_klucz, time()+60*60, '/', '.'.$_SERVER['SERVER_NAME'], 0);
		}
		else {
			// przekierowanie na index.html
			//include('przerwa.php');
			header("location:http://".$_SERVER['SERVER_NAME']."/przerwa.php");
			exit(0);
		}
	}
}

/**      / MAINTENANCE MODE                      */
$aPagePathURLSymbol = $_GET;
$sPagePathURLSymbol = md5(implode('/', $aPagePathURLSymbol).intval($_COOKIE['box_categories']));

$sFirstPageURLSymbol = array_pop(explode('/', preg_replace('/\/$/', '', $_GET['path'])));

include_once ('omniaCMS/config/common.inc.php');
$iBookstoreId = $aConfig[$aConfig['common']['bookstore_code'].'_website_id'];

if ($aConfig['common']['status'] !== 'development' && $_SERVER['HTTP_HOST'] != 'www.profit24.pl') {
    header("HTTP/1.1 301 Moved Permanently");
    header('Location: https://www.profit24.pl'.$_SERVER['REQUEST_URI']);
    exit();
}

if ($aConfig['common']['use_session'] && !isset($_SESSION)) {
  // musimy juz tu uruchomić sesję
  include_once($aConfig['common']['base_path'].$aConfig['common']['cms_dir']."config/session.inc.php");
}
include_once ('omniaCMS/config/ini.inc.php');
/*
if (!isLoggedIn()) {
  include_once($aConfig['common']['base_path'].'LIB/fb_connector/fbConnector.class.php');
  $oFB = fbConnector::getInstance();
  $oFB->doTryLogin();
}
*/

$passedSearchParametersAmount = \LIB\Helpers\SearchHelper::getPassedSearchParametersAmount();

if (isset($_GET['publisher']) && $_GET['publisher'] != '' && strstr($_GET['publisher'], ';amp;') == true) {
	$_GET['publisher'] = str_replace(';amp;', '&', $_GET['publisher']);
}
if ($_GET['q'] == '' && isset($_GET['path']) && ($_GET['path'] == 'szukaj' || $_GET['path'] == 'szukaj/') ) {
	if (isset($_GET['publisher']) && $_GET['publisher'] != '' && isset($_GET['series']) && $_GET['series'] != '') {
		doRedirect(getSeriesLink($_GET['publisher'], $_GET['series'], $_GET['p']), '301');
	}
	elseif (isset($_GET['publisher']) && $_GET['publisher'] != '' && $passedSearchParametersAmount < 2) {
		doRedirect(getPublisherLink($_GET['publisher'], $_GET['p']), '301');
	} 
	elseif (isset($_GET['tag']) && $_GET['tag'] != '') {
		doRedirect(getTagLink($_GET['tag'], $_GET['p']), '301');
	}
	elseif (strstr($_GET['action'], 'tag:')) {
		doRedirect(getTagLink(str_replace('tag:', '', $_GET['action']), $_GET['p']), '301');
	}
	elseif (isset($_GET['autor']) && $_GET['autor'] != '' && $passedSearchParametersAmount < 2) {
		doRedirect(getAuthorLink($_GET['autor'], $_GET['p']), '301');
	} 
}
include_once ('inc/header.inc.php');
// dolaczenie glownego pliku jezykowego
if (file_exists('lang/'.$_GET['lang'].'.php')) {
	include_once ('lang/'.$_GET['lang'].'.php');
}


// katalog szablonow
$sTplsDir = 'pages';

if (substr($_SERVER['HTTP_HOST'], 0, 3) !== 'www' &&
		strpos($_SERVER['DOCUMENT_ROOT'], 'projects') === false) {
	header("HTTP/1.1 301 Moved Permanently");
	header("location:https://www.".
				 substr($aConfig['common']['base_url'], 0, -1).
				 $_SERVER['REQUEST_URI']);
	header("Connection: close");
	exit;
}

if (isset($_COOKIE['wu_remember']) && !isLoggedIn()) {
	// automatyczne logowanie z ciasteczka
	doLogin($_COOKIE['wu_remember']);
}

$markupCookie = new \orders\MarkUpCookie();
$markupCookie->setAdditionalMarkupCookie();

if (isPreviewMode() || !class_exists('memcached')) {
  // tryb podgladu - wylaczenie cache'owania, i nie powinno być zapisywania
	$aConfig['common']['caching'] = false;
  define(S_MEMCACHED, false);
} else {
    if (
    (
        (isset($_SESSION['w_user']['discounts']) && intval($_SESSION['w_user']['discounts']) > 0.00) ||
        $markupCookie->isAnyMarkUpActiveForCache() === true
    )
  ) {
    $aConfig['common']['caching']	= false;
    define(S_MEMCACHED, false);
  } else {
    // XXX TODO CACHOWANIE TUUUU WŁĄCZYĆ
    $aConfig['common']['caching']	= false;
//    $aConfig['common']['caching']	= false;
    if ( empty($_POST) && intval($_GET['product']) <= 0 && 
            $sFirstPageURLSymbol != 'moje-konto' &&
            $sFirstPageURLSymbol != 'koszyk' &&
            $sFirstPageURLSymbol != 'szukaj' &&
            $sFirstPageURLSymbol != 'sprawdz-status-zamowienia' &&
            $sFirstPageURLSymbol != 'schowek' && 
            $sFirstPageURLSymbol != 'newsletter' && 
            (!isset($_SESSION['w_user']['discounts']) || intval($_SESSION['w_user']['discounts']) <= 0.00)
            ) {
      // XXX TODO CACHOWANIE TUUUU WŁĄCZYĆ
      define(S_MEMCACHED, false);
    }
  }
}

// adapter cacha, uĹĽywany w modułach i w Factory()
include_once ('omniaCMS/config/cache.inc.php');
if (S_MEMCACHED === true) {
  $oMemcache = new Memcached();
  $oMemcache->addServer($aConfig['memcached']['host'], $aConfig['memcached']['port'], 1);
}

if ($oCache != null && doClearCache()) {
	// czyszczenie cache'u
	$oCache->clean();
}

if (S_MEMCACHED === true) {
  // memcached
  $sHTML = $oMemcache->get('url_'.$iBookstoreId.'_'.$sPagePathURLSymbol);
  //$sHTML = '';
  if (!empty($sHTML)) {
    header('Content-Type: text/html; charset='.$aConfig['default']['charset']);
//    dump('odczyt z cache');
    echo $sHTML;
  } else {
    // obiekt glownej klasy
    $oFactory = new Factory();
    // pobranie tresci strony
    if (!isset($_GET['no_output'])) {
      if ($aConfig['common']['use_zlib']) {
        ob_start("ob_gzhandler");
      }
      if (!isset($aConfig['_tmp']['page'])) {
        // strona startowa
        if (isset($_GET['print'])) {
          $sPage = $pSmarty->fetch($sTplsDir.'/print.tpl');
        }
        else {
          $sPage = $pSmarty->fetch($sTplsDir.'/index.tpl');
        }
      }
      else {
        if (isset($_GET['print'])) {
          $sPage = $pSmarty->fetch($sTplsDir.'/print.tpl');
        }
        else {
          $sPage = $pSmarty->fetch($sTplsDir.'/'.$aConfig['_tmp']['page']['template']);
        }
      }
      
      header('Content-Type: text/html; charset='.$aConfig['default']['charset']);
      if (S_MEMCACHED === true) {
        // zapis do cache
        $sHTML = obfuscateOutput($sPage);
        if (!isPreviewMode()) {
          // nie zapisujemy do cacha danych z podglÄ…du
          $oMemcache->set('url_'.$iBookstoreId.'_'.$sPagePathURLSymbol, $sHTML);
        }
//        dump('zapis do cache');
        echo $sHTML;
      } else {
//        dump('zwykĹ‚e wczytanie');
        echo obfuscateOutput($sPage);
      }
    }
  }
} else {
//  dump('zwykĹ‚e wczytanie');
  // obiekt glownej klasy
  $oFactory = new Factory();
  // pobranie tresci strony
  if (!isset($_GET['no_output'])) {
    if ($aConfig['common']['use_zlib']) {
      ob_start("ob_gzhandler");
    }
    if (!isset($aConfig['_tmp']['page'])) {
      // strona startowa
      if (isset($_GET['print'])) {
        $sPage = $pSmarty->fetch($sTplsDir.'/print.tpl');
      }
      else {
        $sPage = $pSmarty->fetch($sTplsDir.'/index.tpl');
      }
    }
    else {
      if (isset($_GET['print'])) {
        $sPage = $pSmarty->fetch($sTplsDir.'/print.tpl');
      }
      else {
        $sPage = $pSmarty->fetch($sTplsDir.'/'.$aConfig['_tmp']['page']['template']);
      }
    }
//    dump('zwykĹ‚e wczytanie');
    echo obfuscateOutput($sPage);
  }
}

if (isset($_GET['benchmark']) && $_GET['benchmark'] == '1') {
  // Timer
  $oTimer->stop();
  $oTimer->display();
}