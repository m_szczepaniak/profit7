<?php
/**
 * Pobieranie listy punktów odbioru przesyłek w sieci RUCH (Paczka w Ruchu)
 * 
 * @author Paweł Bromka
 * @created 2014-12-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
header("Content-Type:text/html; charset=utf-8");
$aConfig['common']['use_session'] = false;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

$sTerm = trim($_GET['term']);

if(mb_strlen($sTerm, 'UTF8') >= 1){
  $sTerm = str_replace($aMatches[0], '', $sTerm);
  
  if ($sTerm != '') {
    $sWhere = " WHERE `desc` LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." AND status = 'T'";
  }
  $sWhere .= " ORDER BY `desc` ASC";


    if (mb_strlen($sTerm, 'UTF-8') <= 2) {
        $sWhere .= ' LIMIT 20';
    }
  
  $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
  $oPointOfReceipt = new \orders\Shipment('ruch', $pDbMgr, $bTestMode);
  $aPointsOfReceipt = $oPointOfReceipt->getPointsOfReceiptAutocomplete('PointsOfReceipt', array('DISTINCT `desc` AS value', 'id', 'details as data'), $sWhere);
  echo json_encode($aPointsOfReceipt);
}