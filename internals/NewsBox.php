<?php
$aConfig['_tmp']['location_prefix']='../';
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once('modules/m_oferta_produktowa/client/Common.class.php');
include_once('boxes/m_powiazane_oferta_produktowa/lang/'.(isset($_GET['lang'])?$_GET['lang']:'pl').'_nowosci.php');
include_once ('../lang/'.(isset($_GET['lang'])?$_GET['lang']:'pl').'.php');

$oCommon = new Common_m_oferta_produktowa();


$iMaxPages = 6;


	$sSql = "SELECT A.id, A.page_id,A.template, A.name, A.moption, B.items_in_box, C.symbol AS page_symbol
					 FROM ".$aConfig['tabls']['prefix']."boxes A
					 JOIN ".$aConfig['tabls']['prefix']."products_box_settings B
					 ON B.box_id=A.id
					 JOIN ".$aConfig['tabls']['prefix']."menus_items C
					 ON C.id=A.page_id
					 WHERE A.id = ".intval($_GET['boxid']);
	$aSettings = Common::GetRow($sSql);
	if(!empty($aSettings)){
		
		$iId = $aSettings['id'];
		$iPageId = (double) $aSettings['page_id'];
		$sName = $aSettings['name'];
		//$sModule = $aBox['module'];
		$sOption = 'nowosci';
		$sTemplatesPath = 'boxes/m_powiazane_oferta_produktowa/nowosci';
		$sTemplate = $aSettings['template'];
		$sPageLink = '/'.$aSettings['page_symbol'];
		$_GET['lang_id'] = 1;
		$aConfig['_tmp']['repository_symbol'] = getModulePageLink('m_zamowienia', 'przechowalnia');

	
		$aData = array();
		$aPages=array();
		
		$sCSql = "SELECT count(A.id)
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							  JOIN ".$aConfig['tabls']['prefix']."products_news AS B
								 ON A.id = B.product_id
								 LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id 
						 	 WHERE (A.prod_status = '1' OR A.prod_status = '3') AND I.id IS NOT NULL AND A.shipment_time<>'3' AND B.page_id = ".$iPageId;
		$iCount = Common::GetOne($sCSql);
		$aBox['total_items'] = floor($iCount/$aSettings['items_in_box']);
		if ($aBox['total_items'] > $iMaxPages) {
			$aBox['total_items'] = $iMaxPages;
		}
		if($iCount>30){
			$iCount=30;
		}
		
		preg_match('/(\d+)$/', $_GET['p'], $aMatches);
		$_GET['p'] = (int) $aMatches[0];
		

		$sSql = "SELECT A.id, A.title AS name, A.price, A.price_brutto, A.edition, A.shipment_time, 
											A.isbn, A.isbn_plain, A.publication_year, 
											A.prod_status, A.publisher AS publisher_name, A.series, A.is_news, A.is_bestseller, A.discount_limit
							 FROM ".$aConfig['tabls']['prefix']."products_shadow AS A
							  JOIN ".$aConfig['tabls']['prefix']."products_news AS B
								 ON A.id = B.product_id
								 LEFT JOIN ".$aConfig['tabls']['prefix']."products_images I
							 	 ON I.product_id = A.id 
						 	 WHERE (A.prod_status = '1' OR A.prod_status = '3') AND I.id IS NOT NULL AND A.shipment_time<>'3' AND B.page_id = ".$iPageId."
							 ORDER BY B.order_by ASC, A.order_by DESC".
							 ((int) $aSettings['items_in_box'] > 0 ? " LIMIT ".($_GET['p']*$aSettings['items_in_box']).",".$aSettings['items_in_box'] : '');
			$aData = Common::GetAll($sSql);
			
			foreach($aData as $iKey => $aValue){
				// pobranie autorow itp
				$aData[$iKey]['authors'] =$oCommon->getAuthors($aValue['id']);
				//pobranie cennika
				$aData[$iKey]['tarrif'] = $oCommon->getTarrif($aValue['id']);
				// przeliczenie i formatowanie cen
				$aData[$iKey]['promo_price'] = Common::formatPrice2($oCommon->getPromoPrice($aData[$iKey]['price_brutto'], $aData[$iKey]['tarrif'], $aData[$iKey]['promo_text'], $aData[$iKey]['discount_limit'], $oCommon->getSourceDiscountLimit($aValue['id'])));
				$aData[$iKey]['price_brutto'] = Common::formatPrice($aValue['price_brutto']);
				
				// okladka
				$aData[$iKey]['image'] =getItemImage('products_images',array('product_id'=> $aValue['id']),'__t_');
				// link do produktu
				$aData[$iKey]['link'] = $aConfig['common']['base_url_http_no_slash'].createProductLink($aValue['id'],$aValue['name']);
				$aData[$iKey]['cart_link'] = createLink('/koszyk', 'id'.$aValue['id'], 'add');
				$aData[$iKey]['repository_link'] = createLink($aConfig['_tmp']['repository_symbol'],'id'.$aValue['id'],'add_to_repository,');
			}
		
			$aBox['lang'] =& $aConfig['lang']['box_m_powiazane_oferta_produktowa_nowosci'];
			$aBox['items'] =& $aData;
			$aBox['boxid'] = $iId;
			$aBox['link'] = $sPageLink;

			$pSmarty->assign_by_ref('aLang', $aConfig['lang']);
			$pSmarty->assign_by_ref('aBox', $aBox);
			$pSmarty->display($sTemplatesPath.'/'.$sTemplate);
			$pSmarty->clear_assign('aBox');	   
	}
//		return $aData;
		
?>