<?php
/*
 * SERWER
 * 
 */
$aConfig=array(
	'base1'=>array(//baza aktualniejsza
		'host'=>'192.168.1.145',
		'user'=>'root',
		'pass'=>'root',
		'base'=>'profit2'
		),
		'base2'=>array(//baza do aktualizacji
		'host'=>'192.168.1.145',
		'user'=>'root',
		'pass'=>'root',
		'base'=>'main_profit24'
		)
	);


class baseDif {
	function baseDif() {
		global $aConfig;
		
		$this->oCon1=mysql_connect($aConfig['base1']['host'],$aConfig['base1']['user'],$aConfig['base1']['pass']);
		$this->oCon2=mysql_connect($aConfig['base2']['host'],$aConfig['base2']['user'],$aConfig['base2']['pass']);
		if(!$this->oCon1 || !$this->oCon2) {
			echo'Cannot connect to databases! Errors: Base1:'.!$this->oCon1.' Base2:'.!$this->oCon2; die();
			}
		if(!mysql_select_db('information_schema',$this->oCon1) || !mysql_select_db('information_schema',$this->oCon2)) {
			echo'Cannot select information_schema!';die();
			}
			
		$sSql1='SELECT TABLE_NAME, ENGINE FROM `TABLES` WHERE `TABLE_SCHEMA`=\''.$aConfig['base1']['base'].'\'';
		$sSql2='SELECT TABLE_NAME, ENGINE FROM `TABLES` WHERE `TABLE_SCHEMA`=\''.$aConfig['base2']['base'].'\'';
		$oResp1=mysql_query($sSql1, $this->oCon1);
		$oResp2=mysql_query($sSql2, $this->oCon2);
		$aTables1=array();
		$aTables2=array();	
		while($aRow=mysql_fetch_array($oResp1))
			$aTables1[]=$aRow;	
			
		while($aRow=mysql_fetch_array($oResp2))
			$aTables2[]=$aRow;	
		
		/*echo 'baza1';	
		var_dump($aTables1);
		echo 'baza2';	
		var_dump($aTables2);*/
		$this->showTablesListDifs($aTables1, $aTables2);
	}	
//--------------------------------------------------------------------------------------------
	
	function showTablesListDifs($aBase1, $aBase2) {
		echo'
		<style>
		table {font-size:10px;}
		td { max-width:170px;}
		.red td{background-color:#fbb;}
		.green td{background-color:#bfb;}
		.blue td{background-color:#bbf;}
		.red2 td{background-color:#fdd;}
		.green2 td{background-color:#dfd;}
		</style><table>
		<tr class="blue">
			<td colspan="7">Base1</td>
			<td colspan="7">Base2</td>
		</tr>
		<tr class="blue">
			<td>Field</td>
			<td>Type</td>
			<td>Encoding</td>
			<td>Attribs</td>
			<td>Null</td>
			<td>Default</td>
			<td>Additional</td>
			<td>Field</td>
			<td>Type</td>
			<td>Encoding</td>
			<td>Attribs</td>
			<td>Null</td>
			<td>Default</td>
			<td>Additional</td>
		</tr>';
		$aDrops=array();//tabele istniejace w base2 a nieistniejace w base1
		$iCnt1=$iCnt2=0;
		foreach($aBase1 as $iKey =>$aTable) {
			//pobranie każdej z tabel z base1 z base 2
			$sColor='green';
			$sTableName=$aBase2[$iCnt2]['TABLE_NAME'];
			if($aBase1[$iCnt1]['TABLE_NAME']!=$aBase2[$iCnt2]['TABLE_NAME']) {
				if($iInd=$this->findInArray($aBase1[$iCnt1]['TABLE_NAME'], $aBase2)) {
					//istnieje jednak - w base1 brakuje tabeli
					$sTableName=$aBase1[$iCnt1]['TABLE_NAME'];
					}else {
						$sColor='red';
						$sTableName='NOT EXIST';
						$iCnt2-=1;
					}
				}
			echo'<tr class="'.$sColor.'">
					<td colspan="7"><strong>'.$aBase1[$iCnt1]['TABLE_NAME'].'</strong></td>
					<td colspan="7"><strong>';
			if($sTableName=='NOT EXIST')//kod odpowiedzialny za wygenerowanie kodu utworzenia tabeli
				echo $this->showCreateTable($aBase1[$iCnt1]['TABLE_NAME']);
			else 
			 echo $sTableName;
			 	
			echo'</strong></td>
				</tr>';
			if($sTableName!='NOT EXIST')
			$this->showFields($aBase1[$iCnt1]['TABLE_NAME'], $sTableName);
			++$iCnt1;
			++$iCnt2;
			}
		
		echo'
		</table>';
		$this->showTriggers();
		$this->showCOnstraints();
		
		}
//--------------------------------------------------------------------------------------------	
	
	function showFields($sTab1, $sTab2) {
		global $aConfig;
		$sSql1='SELECT * FROM `COLUMNS` WHERE `TABLE_SCHEMA`=\''.$aConfig['base1']['base'].'\' AND `TABLE_NAME`=\''.$sTab1.'\'';
		$sSql2='SELECT * FROM `COLUMNS` WHERE `TABLE_SCHEMA`=\''.$aConfig['base2']['base'].'\' AND `TABLE_NAME`=\''.$sTab2.'\'';
		$oResp1=mysql_query($sSql1, $this->oCon1);
		$oResp2=mysql_query($sSql2, $this->oCon2);
		
		$aColumns1=array();
		$aColumns2=array();
		while($aRow=mysql_fetch_array($oResp1))
			$aColumns1[]=$aRow;
		while($aRow=mysql_fetch_array($oResp2))
			$aColumns2[]=$aRow;
			
		$iCnt1=$iCnt2=0;	
		foreach($aColumns1 as $iKey =>$aColumn) {
			$aColumn=$aColumns1[$iCnt1];
			$aColumn2=$aColumns2[$iCnt2];
			$sColor='green2';
			
			if($aColumn['COLUMN_NAME'] !=$aColumn2['COLUMN_NAME']) {
				if($iInd=$this->findInArray2($aColumn['COLUMN_NAME'], $aColumns2)) {
					//jednak istnieje pole
					$aColumn2=$aColumns2[$iInd];
					}
				else {
					//nie istnieje pole
					$sColor='red2';
					$aColumn2['COLUMN_NAME']='NOT EXIST';
					$iCnt2-=1;
					$aColumn2['COLUMN_NAME']=$this->showInsertField($sTab1,$aColumn, $aColumns1[$iKey-1]['COLUMN_NAME']);
					}
				}
			elseif(
				$aColumn['COLUMN_TYPE']!=$aColumn2['COLUMN_TYPE'] ||
				$aColumn['COLUMN_TYPE']!=$aColumn2['COLUMN_TYPE'] ||
				$aColumn['COLLATION_NAME']!=$aColumn2['COLLATION_NAME'] ||
				$aColumn['COLUMN_KEY']!=$aColumn2['COLUMN_KEY'] ||
				$aColumn['IS_NULLABLE']!=$aColumn2['IS_NULLABLE'] ||
				$aColumn['COLUMN_DEFAULT']!=$aColumn2['COLUMN_DEFAULT'] ||
				$aColumn['EXTRA']!=$aColumn2['EXTRA']
				) {//sprawdzamy czy aprametry pola są identyczne
				$sColor='red2';
				if(strpos($aColumn2['COLUMN_NAME'],'EXIST')<1)
					$aColumn2['COLUMN_NAME']=''.$this->showAlterField($sTab1,$aColumn,$aColumn2);
				}
			
			echo'<tr class="'.$sColor.'">
				<td>'.$aColumn['COLUMN_NAME'].'</td>
				<td>'.$aColumn['COLUMN_TYPE'].'</td>
				<td>'.$aColumn['COLLATION_NAME'].'</td>
				<td>'.$aColumn['COLUMN_KEY'].'</td>
				<td>'.$aColumn['IS_NULLABLE'].'</td>
				<td>'.$aColumn['COLUMN_DEFAULT'].'</td>
				<td>'.$aColumn['EXTRA'].'</td>';
			
			
			echo'<td>'.$aColumn2['COLUMN_NAME'].'</td>
				<td>'.$aColumn2['COLUMN_TYPE'].'</td>
				<td>'.$aColumn2['COLLATION_NAME'].'</td>
				<td>'.$aColumn2['COLUMN_KEY'].'</td>
				<td>'.$aColumn2['IS_NULLABLE'].'</td>
				<td>'.$aColumn2['COLUMN_DEFAULT'].'</td>
				<td>'.$aColumn2['EXTRA'].'</td>
				</tr>';
			
			
			++$iCnt1;
			++$iCnt2;
			}	
		}	
//--------------------------------------------------------------------------------------------	
	
		
		function findInArray($sNeedle, $aHaystack) {
			global $aConfig;
			//znajduje index po nazwie tabeli
			foreach($aHaystack as $iKey =>$aElem) {
				if($aElem['TABLE_NAME']==$sNeedle) return $iKey;
				} 
			}
//--------------------------------------------------------------------------------------------	
	
		
		function findInArray2($sNeedle, $aHaystack) {
			global $aConfig;
			//znajduje index po nazwie tabeli
			foreach($aHaystack as $iKey =>$aElem) {
				if($aElem['COLUMN_NAME']==$sNeedle) return $iKey;
				} 
			}
//--------------------------------------------------------------------------------------------	

		function showCreateTable($sName) {
			global $aConfig;
			$sBuf='CREATE TABLE IF NOT EXISTS `'.$sName.'` (';
			
			$sSql1='SELECT * FROM `COLUMNS` WHERE `TABLE_SCHEMA`=\''.$aConfig['base1']['base'].'\' AND `TABLE_NAME`=\''.$sName.'\'';
			$oResp1=mysql_query($sSql1, $this->oCon1);
			$aColumns1=array();
			
			while($aRow=mysql_fetch_array($oResp1)) {
				$sBuf.='<br />`'.$aRow['COLUMN_NAME'].'` '.$aRow['COLUMN_TYPE'].' '.(strlen($aRow['COLLATION_NAME'])?'COLLATE '.$aRow['COLLATION_NAME']:'').' '.($aRow['IS_NULLABLE']=='NO'?'NOT NULL':'').' '.(strlen($aRow['COLUMN_DEFAULT'])?'DEFAULT '.(in_array($aRow['DATA_TYPE'], array('varchar','date', 'datetime','enum'))?'\'':'').$aRow['COLUMN_DEFAULT'].(in_array($aRow['DATA_TYPE'], array('varchar','date', 'datetime','enum'))?'\'':''):'').' '.$aRow['EXTRA'];
				switch($aRow['COLUMN_KEY']) {
					case'PRI': $sBuf.=' PRIMARY KEY';	break;
					case'UNI': $sBuf.=' UNIQUE';	break;
					//case'MUL': $sBuf.='UNIQUE';	break;
					}
				$sBuf.=',';
				}
				//$aColumns1[]=$aRow;
			
			return substr($sBuf,0,-1).')';	
			}
//--------------------------------------------------------------------------------------------

		function showInsertField($sTab, $aRow, $sPrevNameColumn) {
			global $aConfig;
			
			$sBuf='ALTER TABLE `'.$sTab.'` ADD `'.$aRow['COLUMN_NAME'].'` '.$aRow['COLUMN_TYPE'].' '.(strlen($aRow['COLLATION_NAME'])?'COLLATE '.$aRow['COLLATION_NAME']:'').' '.($aRow['IS_NULLABLE']=='NO'?'NOT NULL':'').' '.(strlen($aRow['COLUMN_DEFAULT'])?'DEFAULT '.(in_array($aRow['DATA_TYPE'], array('varchar','date', 'datetime','enum'))?'\'':'').$aRow['COLUMN_DEFAULT'].(in_array($aRow['DATA_TYPE'], array('varchar','date', 'datetime','enum'))?'\'':''):'').' '.$aRow['EXTRA'].($sPrevNameColumn!=''?' AFTER `'.$sPrevNameColumn.'`':'');
			
			return $sBuf;
		}
//--------------------------------------------------------------------------------------------

		function showAlterField($sTab, $aRow, $aRow2) {
			global $aConfig;
			
			$sBuf='ALTER TABLE `'.$sTab.'` MODIFY `'.$aRow['COLUMN_NAME'].'` '.$aRow['COLUMN_TYPE'].' '.(strlen($aRow['COLLATION_NAME'])?'COLLATE '.$aRow['COLLATION_NAME']:'').' '.($aRow['IS_NULLABLE']=='NO'?'NOT NULL':'').' '.(strlen($aRow['COLUMN_DEFAULT'])?'DEFAULT '.(in_array($aRow['DATA_TYPE'], array('varchar','date', 'datetime','enum'))?'\'':'').$aRow['COLUMN_DEFAULT'].(in_array($aRow['DATA_TYPE'], array('varchar','date', 'datetime','enum'))?'\'':''):'').' '.$aRow['EXTRA'];;
			
			return $sBuf;
		}
//--------------------------------------------------------------------------------------------

		function showTriggers() {
			global $aConfig;
			
		echo'<table>';
		
			echo'<tr class="blue">
			<td colspan="13">Base1 Triggers</td>
			<td colspan="13">Base2 Triggers</td>
			</tr><tr class="blue">
					<td>Name</td>
					<td>Manipul</td>
					<td>Obj</td>
					<td>Act ord</td>
					<td>Act cond</td>
					<td>Act orient</td>
					<td>Act tim</td>
					<td>Act ref o tab</td>
					<td>Act ref n tab</td>
					<td>Act ref o row</td>
					<td>Act ref n row</td>
					<td>Mode</td>
					<td>Collation</td>
					<td>Name</td>
					<td>Manipul</td>
					<td>Obj</td>
					<td>Act ord</td>
					<td>Act cond</td>
					<td>Act orient</td>
					<td>Act tim</td>
					<td>Act ref o tab</td>
					<td>Act ref n tab</td>
					<td>Act ref o row</td>
					<td>Act ref n row</td>
					<td>Mode</td>
					<td>Collation</td>
					</tr>';
			$sColor='green2';
			
			$sSql1='SELECT * FROM TRIGGERS WHERE TRIGGER_SCHEMA = \''.$aConfig['base1']['base'].'\'';
			$sSql2='SELECT * FROM TRIGGERS WHERE TRIGGER_SCHEMA = \''.$aConfig['base2']['base'].'\'';
			$oResp1=mysql_query($sSql1, $this->oCon1);
			$oResp2=mysql_query($sSql2, $this->oCon2);
			
			$aColumns1=array();
			$aColumns2=array();
			$iCnt2=0;
			$aNames2=array();
			while($aRow=mysql_fetch_array($oResp1)) {
				$aColumns1[]=$aRow;
				}
			while($aRow=mysql_fetch_array($oResp2)) {
				$aColumns2[$iCnt2]=$aRow;
				$aNames2[$aRow['TRIGGER_NAME']]=$iCnt2;
				++$iCnt2;
				}
			
				
			foreach($aColumns1 as $iKey =>$aTrigger) {
				$aTrigger2=$aColumns2[$aNames2[$aTrigger['TRIGGER_NAME']]];
				$sColor='green2';
				
				if(!isset($aTrigger2)) { //brak triggera o tej nazwie
					$sColor='red2';
					$aTrigger2['TRIGGER_NAME']='NOT EXIST';
					}
				else if (//sprawdzamy czy jest taki sam
					$aTrigger['TRIGGER_NAME']!=$aTrigger['TRIGGER_NAME'] ||
					$aTrigger['EVENT_MANIPULATION']!= $aTrigger['EVENT_MANIPULATION']||
					$aTrigger['EVENT_OBJECT_TABLE']!= $aTrigger['EVENT_OBJECT_TABLE']||
					$aTrigger['ACTION_ORDER']!= $aTrigger['ACTION_ORDER']||
					$aTrigger['ACTION_CONDITION']!= $aTrigger['ACTION_CONDITION']||
					$aTrigger['ACTION_ORIENTATION']!=$aTrigger['ACTION_ORIENTATION'] ||
					$aTrigger['ACTION_TIMING']!=$aTrigger['ACTION_TIMING'] ||
					$aTrigger['ACTION_REFERENCE_OLD_TABLE']!=$aTrigger['ACTION_REFERENCE_OLD_TABLE'] ||
					$aTrigger['ACTION_REFERENCE_NEW_TABLE']!=$aTrigger['ACTION_REFERENCE_NEW_TABLE'] ||
					$aTrigger['ACTION_REFERENCE_OLD_ROW']!=$aTrigger['ACTION_REFERENCE_OLD_ROW'] ||
					$aTrigger['ACTION_REFERENCE_NEW_ROW']!=$aTrigger['ACTION_REFERENCE_NEW_ROW'] ||
					$aTrigger['SQL_MODE']!=$aTrigger['SQL_MODE'] ||
					$aTrigger['COLLATION_CONNECTION']!=$aTrigger['COLLATION_CONNECTION']
					)	
					$sColor='red2';
					
				echo'<tr class="'.$sColor.'" style="font-weight:bold;">
					<td>'.$aTrigger['TRIGGER_NAME'].'</td>
					<td>'.$aTrigger['EVENT_MANIPULATION'].'</td>
					<td>'.$aTrigger['EVENT_OBJECT_TABLE'].'</td>
					<td>'.$aTrigger['ACTION_ORDER'].'</td>
					<td>'.$aTrigger['ACTION_CONDITION'].'</td>
					<td>'.$aTrigger['ACTION_ORIENTATION'].'</td>
					<td>'.$aTrigger['ACTION_TIMING'].'</td>
					<td>'.$aTrigger['ACTION_REFERENCE_OLD_TABLE'].'</td>
					<td>'.$aTrigger['ACTION_REFERENCE_NEW_TABLE'].'</td>
					<td>'.$aTrigger['ACTION_REFERENCE_OLD_ROW'].'</td>
					<td>'.$aTrigger['ACTION_REFERENCE_NEW_ROW'].'</td>
					<td>'.$aTrigger['SQL_MODE'].'</td>
					<td>'.$aTrigger['COLLATION_CONNECTION'].'</td>';
				
				
				echo'<td>'.$aTrigger2['TRIGGER_NAME'].'</td>
					<td>'.$aTrigger2['EVENT_MANIPULATION'].'</td>
					<td>'.$aTrigger2['EVENT_OBJECT_TABLE'].'</td>
					<td>'.$aTrigger2['ACTION_ORDER'].'</td>
					<td>'.$aTrigger2['ACTION_CONDITION'].'</td>
					<td>'.$aTrigger2['ACTION_ORIENTATION'].'</td>
					<td>'.$aTrigger2['ACTION_TIMING'].'</td>
					<td>'.$aTrigger2['ACTION_REFERENCE_OLD_TABLE'].'</td>
					<td>'.$aTrigger2['ACTION_REFERENCE_NEW_TABLE'].'</td>
					<td>'.$aTrigger2['ACTION_REFERENCE_OLD_ROW'].'</td>
					<td>'.$aTrigger2['ACTION_REFERENCE_NEW_ROW'].'</td>
					<td>'.$aTrigger2['SQL_MODE'].'</td>
					<td>'.$aTrigger2['COLLATION_CONNECTION'].'</td>';
        

        $sCMP1 = preg_replace("/^\t*--.+$/m", "", trim($aTrigger['ACTION_STATEMENT']));
        $sCMP2 = preg_replace("/^\t*--.+$/m", "", trim($aTrigger2['ACTION_STATEMENT']));
        
        $sCMP1 = preg_replace("/\s/", "", $sCMP1);
        $sCMP2 = preg_replace("/\s/", "", $sCMP2);
        
				$sCMP1 = md5(str_replace(array("\n", "\r", "t"), '', $sCMP1));
        $sCMP2 = md5(str_replace(array("\n", "\r", "t"), '', $sCMP2));
        
				if($sCMP1 !== $sCMP2)	$sColor='red2';
				echo'</tr>
				<tr class="'.$sColor.'">
					<td colspan="13">'.nl2br($aTrigger['ACTION_STATEMENT']).'</td>
					<td colspan="13">'.nl2br($aTrigger2['ACTION_STATEMENT']).'</td></tr>';
				}	
		echo'
		</table>';
		}
//--------------------------------------------------------------------------------------------

		function showConstraints() {
			global $aConfig;
			
			echo'<table>
			<tr class="blue">
				<td colspan="5">Base 1 Constraints</td>
				<td colspan="5">Base 2 Constraints</td>
			</tr><tr class="blue">';
					echo'<td>Name</td>
					<td>Table</td>
					<td>Column</td>
					<td>Ref. table</td>
					<td>Ref. column</td><td>Name</td>
					<td>Table</td>
					<td>Column</td>
					<td>Ref. table</td>
					<td>Ref. column</td></tr>';
			
			$sSql1='SELECT * FROM `KEY_COLUMN_USAGE` WHERE `CONSTRAINT_SCHEMA`=\''.$aConfig['base1']['base'].'\' ORDER BY TABLE_NAME ASC';
			$sSql2='SELECT * FROM `KEY_COLUMN_USAGE` WHERE `CONSTRAINT_SCHEMA`=\''.$aConfig['base2']['base'].'\' ORDER BY TABLE_NAME ASC';
			$oResp1=mysql_query($sSql1, $this->oCon1);
			$oResp2=mysql_query($sSql2, $this->oCon2);
			
			$aColumns1=array();
			$aColumns2=array();
			$iCnt2=0;
			$aNames2=array();
			while($aRow=mysql_fetch_array($oResp1)) {
				$aColumns1[]=$aRow;
				}
			while($aRow=mysql_fetch_array($oResp2)) {
				$aColumns2[$iCnt2]=$aRow;
				$aNames2[$aRow['TRIGGER_NAME']]=$iCnt2;
				++$iCnt2;
				}
				//var_dump($aColumns2);
				$sColor='green';
				foreach($aColumns1 as $iKey =>$aConstraint) {	
					$aFound=$this->findConstraints($aConstraint, $aColumns2);
					if(empty($aFound)) $sColor='red';
					else 				 $sColor='green';
					echo'<tr class="'.$sColor.'">';
					echo'<td>'.$aConstraint['CONSTRAINT_NAME'].'</td>
					<td>'.$aConstraint['TABLE_NAME'].'</td>
					<td>'.$aConstraint['COLUMN_NAME'].'</td>
					<td>'.$aConstraint['REFERENCED_TABLE_NAME'].'</td>
					<td>'.$aConstraint['REFERENCED_COLUMN_NAME'].'</td>';
					if(!empty($aFound)) {
						echo'<td>'.$aFound['CONSTRAINT_NAME'].'</td>
						<td>'.$aFound['TABLE_NAME'].'</td>
						<td>'.$aFound['COLUMN_NAME'].'</td>
						<td>'.$aFound['REFERENCED_TABLE_NAME'].'</td>
						<td>'.$aFound['REFERENCED_COLUMN_NAME'].'</td>';
						
						}
					else {	
						echo'<td colspan="5">';
							$this->showConstraintSql($aConstraint);
						echo'</td>';
					}
					echo'</tr>';
					}
			//pokazac to co zostało w tabeli	
		
					
			foreach($aColumns2 as $iKey =>$aConstraint) {
				echo'<tr class="red"><td colspan="5">';
				$this->showConstraintSqlDrop($aConstraint);
				echo'</td><td>'.$aConstraint['CONSTRAINT_NAME'].'</td>
					<td>'.$aConstraint['TABLE_NAME'].'</td>
					<td>'.$aConstraint['COLUMN_NAME'].'</td>
					<td>'.$aConstraint['REFERENCED_TABLE_NAME'].'</td>
					<td>'.$aConstraint['REFERENCED_COLUMN_NAME'].'</td>
					</tr>';
				}	
			echo'</table>';
		}
//--------------------------------------------------------------------------------------------
function findConstraints($aNeedle, &$aHaystack) {
			global $aConfig;
			foreach($aHaystack as $iKey =>$aItem)
				if(
				$aItem['CONSTRAINT_NAME']==$aNeedle['CONSTRAINT_NAME'] &&
				$aItem['TABLE_NAME']==$aNeedle['TABLE_NAME'] &&
				$aItem['COLUMN_NAME']==$aNeedle['COLUMN_NAME'] &&
				$aItem['REFERENCED_TABLE_NAME']==$aNeedle['REFERENCED_TABLE_NAME'] &&
				$aItem['REFERENCED_COLUMN_NAME']==$aNeedle['REFERENCED_COLUMN_NAME']
				) {unset($aHaystack[$iKey]); return $aItem; break;}
			
}
//--------------------------------------------------------------------------------------------

function showConstraintSql($aConstraint) {
	switch($aConstraint['CONSTRAINT_NAME']) {
			case'PRIMARY':echo'ALTER TABLE `'.$aConstraint['TABLE_NAME'].'` ADD PRIMARY KEY ('.$aConstraint['COLUMN_NAME'].')';break;
			default: 			{
						if($aConstraint['REFERENCED_COLUMN_NAME']!='')
						echo'ALTER TABLE `'.$aConstraint['TABLE_NAME'].'` ADD CONSTRAINT `'.$aConstraint['CONSTRAINT_NAME'].'` FOREIGN KEY('.$aConstraint['COLUMN_NAME'].') REFERENCES `'.$aConstraint['REFERENCED_TABLE_NAME'].'`('.$aConstraint['REFERENCED_COLUMN_NAME'].') ON DELETE CASCADE ON UPDATE CASCADE;';}
						break;
			}
	
	}
//--------------------------------------------------------------------------------------------

function showConstraintSqlDrop($aConstraint) {
	switch($aConstraint['CONSTRAINT_NAME']) {
			case'PRIMARY':echo'ALTER TABLE `'.$aConstraint['TABLE_NAME'].'` DROP PRIMARY KEY';break;
			default: 			{
						if($aConstraint['REFERENCED_COLUMN_NAME']!='')
						echo'ALTER TABLE `'.$aConstraint['TABLE_NAME'].'`DROP FOREIGN KEY `'.$aConstraint['CONSTRAINT_NAME'].'`';}
						break;
			}
	
	}
};
	






	$jebloSie=new baseDif();
