<?php
/**
 * Pobieranie listy placówek pocztowych z Odbiorem w punkcie
 * 
 * @author Paweł Bromka
 * @created 2014-12-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
header("Content-Type:text/html; charset=utf-8");
$aConfig['common']['use_session'] = false;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';


$sTerm = trim($_GET['term']);
preg_match('(\d{2}-?\d{3})', $sTerm, $aMatches);
$iIntPostal = intval(str_replace('-', '', $aMatches[0]));


if(mb_strlen($sTerm, 'UTF8') > 2){
  $sTerm = str_replace($aMatches[0], '', $sTerm);
  $sTerm = str_replace(' ', '%', $sTerm);
  
  
  if ($sTerm != '') {
    $sAddSQL = " WHERE `desc` LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%')." AND status = '1' ";
  }
  
  if ($iIntPostal > 0) {
    if (empty($sAddSQL)) {
      $sAddSQL = " WHERE status = '1' ";
    } 
      
    $sAddSQL .= ' ORDER BY ABS( postcode -  "'.$iIntPostal.'" ) ASC
    LIMIT 6';    
  } else {
    $sAddSQL .= ' ORDER BY sequence '; 
  }  
  
  $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
  $oPointOfReceipt = new \orders\Shipment('orlen', $pDbMgr, $bTestMode);
  $aPointsOfReceipt = $oPointOfReceipt->getPointsOfReceiptAutocomplete('PointsOfReceipt', array('DISTINCT `desc` AS value', 'id', 'details as data'), $sAddSQL);
  echo json_encode($aPointsOfReceipt);
}
