<?php

$aConfig['common']['use_session'] = true;
$aConfig['_tmp']['location_prefix']='../';

include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');

if(isset($_SESSION['wu_cart']['products'][$_POST['product_id']]))
{
    echo json_encode(1);
}
else
{
    echo json_encode(0);
}

?>