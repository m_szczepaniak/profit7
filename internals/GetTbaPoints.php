<?php
/**
 * Pobieranie listy placówek pocztowych z Odbiorem w punkcie
 * 
 * @author Paweł Bromka
 * @created 2014-11-26 
 * @copyrights Marcin Chudy - Profit24.pl
 */

$aConfig['common']['use_session'] = false;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');

header("Content-Type:text/html; charset=utf-8");
$sTerm = trim($_GET['term']);
preg_match('(\d{2}-?\d{3})', $sTerm, $aMatches);
$iIntPostal = intval(str_replace('-', '', $aMatches[0]));


if(mb_strlen($sTerm, 'UTF8') > 2){
  $sTerm = str_replace($aMatches[0], '', $sTerm);
  $sTerm = str_replace(' ', '%', $sTerm);
  
  if ($iIntPostal > 0) {
    $sPostalSQL = ' ORDER BY ABS( postcode -  "'.$iIntPostal.'" ) ASC
     LIMIT 6';
  }

  if ($sTerm != '') {
    $sWhere = " WHERE `desc` LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%');
  }
  
	$sSql = "SELECT DISTINCT `desc` AS value
					 FROM shipping_points_tba 
            ".$sWhere
           .$sPostalSQL;
	$aPostals =& Common::GetAll($sSql);
  
  echo json_encode($aPostals);
}
?>