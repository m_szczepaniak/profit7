<?php
/**
 * Pobiera paczkomaty do wybrania
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-26 
 * @copyrights Marcin Chudy - Profit24.pl
 */

header("Content-Type:text/html; charset=utf-8");
$aConfig['common']['use_session'] = false;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';


$sTerm = trim($_GET['term']);
preg_match('(\d{2}-?\d{3})', $sTerm, $aMatches);
$iIntPostal = intval(str_replace('-', '', $aMatches[0]));

if(mb_strlen($sTerm, 'UTF8') >= 1){
  $sTerm = str_replace($aMatches[0], '', $sTerm);
  
  if ($iIntPostal > 0) {
    $sPostalSQL = ' ORDER BY ABS( postal -  "'.$iIntPostal.'" ) ASC
     LIMIT 6';
  }
  if ($sTerm != '') {
   $sWhere = " WHERE `desc` LIKE ".$pDB->quoteSmart('%'.stripslashes($sTerm).'%').' ';
    if (mb_strlen($sTerm, 'UTF-8') <= 2 && $iIntPostal <= 0) {
      $sWhere .= ' LIMIT 20';
    }
  }
  $sAddSQL = $sWhere . $sPostalSQL;


  $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
  $oPointOfReceipt = new \orders\Shipment('paczkomaty_24_7', $pDbMgr, $bTestMode);
  $aPointsOfReceipt = $oPointOfReceipt->getPointsOfReceiptAutocomplete('PointsOfReceipt', array('DISTINCT `desc` AS value', 'code AS id', 'data'), $sAddSQL);
  echo json_encode($aPointsOfReceipt);
}