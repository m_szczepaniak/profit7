<?php
$path = $_GET['path'];
$filename = $_GET['filename'];
$document_root = $_SERVER['DOCUMENT_ROOT'];
$path = $document_root.'/images/photos/'.$path;



function resize($newWidth, $targetFile, $originalFile) {

    $info = getimagesize($originalFile);
    $mime = $info['mime'];

    switch ($mime) {
        case 'image/jpeg':
            $image_create_func = 'imagecreatefromjpeg';
            $image_save_func = 'imagejpeg';
            $new_image_ext = 'jpg';
            break;

        case 'image/png':
            $image_create_func = 'imagecreatefrompng';
            $image_save_func = 'imagepng';
            $new_image_ext = 'png';
            break;

        case 'image/gif':
            $image_create_func = 'imagecreatefromgif';
            $image_save_func = 'imagegif';
            $new_image_ext = 'gif';
            break;

        default:
            throw new Exception('Unknown image type.');
    }

    $img = $image_create_func($originalFile);
    list($width, $height) = getimagesize($originalFile);

    $newHeight = ($height / $width) * $newWidth;
    $tmp = imagecreatetruecolor($newWidth, $newHeight);
    imagecopyresampled($tmp, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

    if (file_exists($targetFile)) {
        unlink($targetFile);
    }
    $image_save_func($tmp, "$targetFile");
}

function compress($entry) {
    if (false !== stristr($entry, '.png')) {
        @exec('optipng '.$entry);
        @exec('pngout '.$entry);
    } elseif (false !== stristr($entry, '.jpg') || false !== stristr($entry, '.jpeg')) {
        @exec('jpegoptim '.$entry.' --strip-all');
    }
}


if (is_dir($path)) {
    if (file_exists($path.'/__m_'.$filename)) {
        $imginfo = getimagesize($path.'/__m_'.$filename);
        compress($path.'/__m_'.$filename);
        header("Content-type: {$imginfo['mime']}");
        echo file_get_contents($path.'/__m_'.$filename);
        exit();
    }
    else if (file_exists($path.'/__b_'.$filename)) {
        resize(356, $path.'/__m_'.$filename, $path.'/__b_'.$filename);
        compress($path.'/__m_'.$filename);
        $imginfo = getimagesize($path.'/__m_'.$filename);
        header("Content-type: {$imginfo['mime']}");
        echo file_get_contents($path.'/__m_'.$filename);
        exit();
    }
    elseif (file_exists($path.'/'.$filename)) {
        resize(356, $path.'/__m_'.$filename, $path.'/'.$filename);
        compress($path.'/__m_'.$filename);
        $imginfo = getimagesize($path.'/__m_'.$filename);
        header("Content-type: {$imginfo['mime']}");
        echo file_get_contents($path.'/__m_'.$filename);
        exit();
    } else {
        $imginfo = getimagesize('/images/gfx/brak.png');
        header("Content-type: {$imginfo['mime']}");
        echo file_get_contents($_SERVER['DOCUMENT_ROOT'].'/images/gfx/brak.png');
        exit();
    }
} else {
    $imginfo = getimagesize('/images/gfx/brak.png');
    header("Content-type: {$imginfo['mime']}");
    echo file_get_contents($_SERVER['DOCUMENT_ROOT'].'/images/gfx/brak.png');
    exit();
}
