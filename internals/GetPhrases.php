<?php
use Elasticsearch\Profit\ElasticIndex;
use Elasticsearch\Profit\ElasticSearchBuilder;
use Elasticsearch\Profit\ElasticSearchStrategy;
use LIB\Helpers\ArrayHelper;

$aConfig['common']['use_session'] = false;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once('../LIB/autoloader.php');

$disable = false;

if ($disable == true) {
	exit;
}


if(strlen($_GET['term'])>=3){
	if(empty($_GET['limit'])){
		$_GET['limit']=15;
	}

	$field = 'id';

	if ('profit24' != $aConfig['common']['bookstore_code']) {
		$field = 'profit24_id';
	}

	$db = $aConfig['common']['bookstore_code'];

	$_GET['term'] = str_replace('+', ' ', $_GET['term']);

	$elasticIndex = new ElasticSearchStrategy();

	global $pDbMgr;

	$elasticResult = $elasticIndex->autocompleteSearch(15, true, false);

	if (empty($elasticResult)) {
		exit;
	}

	$products = '<ul id="result-ui">'.prepareHtml($elasticResult).'</ul>';

	$html .= $products;

	echo $html;die;

//	echo json_encode($aPhrases);
}


function prepareHtml(array $products)
{
	$html = '';

	foreach($products as $product) {

		if (empty($product['highlight'])) {

			$product['highlight'] = $product['name'];
		}

		$product['highlight'] = ucfirst(strtolower($product['highlight']));
		$product['name'] = ucfirst(strtolower($product['name']));

		$row = '<li class="s-highlight-li">
					<a onclick="submit_row(this)" data-text="'.trim($product['name']).'" class="s-highlight">'.trim($product['highlight']).'</a>
				</li>';

		$html .= $row;
	}

	return $html;
}
