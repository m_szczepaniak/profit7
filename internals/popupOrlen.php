<?php

$aConfig['common']['use_session'] = false;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

//wyczyśćmy code
$aMatches = array();
preg_match('/^\w+$/', $_GET['code'], $aMatches);
if ($aMatches[0] != '') {
  $sPunktOdbioruCode = $aMatches[0];
  $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
  $oOrlen = new \communicator\sources\Orlen\OrlenPointsOfReceipt($pDbMgr, $bTestMode);
  $aFormatedPointOfReciptDetails = $oOrlen->getSinglePointDetails($sPunktOdbioruCode);
  
  $sDetails = "Wybrana stacja Orlen: <b><br />"
          . $aFormatedPointOfReciptDetails['desc'] . '</b><br /><br />'.$aFormatedPointOfReciptDetails['Name'].'<br />'.$aFormatedPointOfReciptDetails['openHours'].'<br />';
  

  if (!empty($aFormatedPointOfReciptDetails) && is_array($aFormatedPointOfReciptDetails)) {
    $sSrc = 'https://maps.google.pl/maps?f=q&source=s_q&hl=pl&geocode=&q='
            .$aFormatedPointOfReciptDetails['Latitude'].','.$aFormatedPointOfReciptDetails['Longitude']
            .'('.$aFormatedPointOfReciptDetails['PostalCode'].' '.$aFormatedPointOfReciptDetails['City'].' '.$aFormatedPointOfReciptDetails['AddressLine']
            .')&aq=&vpsrc=0&ie=UTF8&t=m&z=14&'
            .$aFormatedPointOfReciptDetails['Latitude'].','.$aFormatedPointOfReciptDetails['Longitude']
            .'&output=embed';
  }
} else {
  die;
}
?><!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Lokalizacja stacji Orlen</title>
    <style>
      body {
        margin: 0;
      }
    </style>
  </head>
  <body>
    <div style="padding: 10px; border: solid 1px black; background: #da3522; color: #fff; font-family: Tahoma, Arial, Verdana, Helvetica;">
      <div style='position: absolute; right: 0; width: 151px; height: 72px; margin-right: 6px; margin-top: -10px;'></div>
      <?php echo $sDetails . $sHourDetails; ?>
    </div>
    <iframe 
  width="790" 
  height="620" 
  frameborder="0" 
  scrolling="no" 
  marginheight="0" 
  marginwidth="0" 
  src="<?php echo $sSrc; ?>"></iframe>
  </body>
</html>