<?php
$aConfig['common']['use_session'] = false;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');

$elastic = new \Elasticsearch\Profit\ElasticSearchBuilder();

$keyword = $_POST['q'];

$search = $elastic->search($keyword);

$string = '
<div>
<div>Znaleziono'.$search['hits']['total'].'</div>
<div>Najbardziej pasujacy'.$search['hits']['max_score'].'</div>
<table class="table-bordered">
<th>Wynik</th>
<th>Name</th>
%s
</table>
</div>
';

$results = '';

foreach($search['hits']['hits'] as $result) {
	$tr = '<tr>';
	$tr .= '<td>'.$result['_score'].'</td>';
	$tr .= '<td>'.$result['_source']['id'].'</td>';
	$tr .= '<td>'.$result['_source']['name'].'</td>';
	$tr .= '</tr>';

	$results .= $tr;
}

$string = sprintf($string, $results);

echo $string;
