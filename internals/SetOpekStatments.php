<?php

/**
 * Skrypt JEDNORAZOWEGO użytku nadaje do każdego przelewu 
 *  powiązanie przelew -> zamówienia w tabeli orders_linking_transport_numbers 
 * 
 */
$aConfig['_tmp']['location_prefix']='../';
$aConfig['common']['use_session'] = true;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');

/**
 * Funkcja pobiera dane przelewu na podstawie nr listu przewozowego
 *
 * @global array $aConfig
 * @param type $sRef
 * @return type 
 */
function getStatmentIdByTitle($sTitle) {
	global $aConfig;
	
	$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders_bank_statements
					 WHERE title LIKE '%".$sTitle."%'";
	return Common::GetOne($sSql);
}

/**
 * Metoda dodaje do tabeli 
 *
 * @global type $aConfig
 * @param type $iId
 * @return type 
 */
function addLinkingTransportNumbers($iId) {
	global $aConfig;

	// pobranie tytułu przelewu
	$sSql = "SELECT title FROM ".$aConfig['tabls']['prefix']."orders_bank_statements
					 WHERE id=".$iId;
	$sTitle = Common::GetOne($sSql);
	$aTransportNumers = getTransportNumbers($sTitle);

	foreach ($aTransportNumers as $sTransportNumber) {
		$aValues = array(
				'orders_bank_statement_id' => $iId,
				'transport_number' => $sTransportNumber
		);
		if (Common::Insert($aConfig['tabls']['prefix']."orders_linking_transport_numbers", $aValues) === false) {
			return false;
			// uciekamy reszta zostanie posprzątana w transakcji w metodzie nadrzędnej
		}
	}

	return true;
} // end addLinkingTransportNumbers() method


/**
 * Metoda pobiera przyjmuje tytuł przelewu 
 *  intrepretuje i zwraca tablicę numerów transportu
 *
 * @param string $sTitle
 * @return array 
 */
function getTransportNumbers($sTitle) {
	$aTransportNumers = array();
	$iStart=0;

	$sListaTmp = preg_replace('[ ]', '', $sTitle);
	if(($iPos = strpos($sListaTmp, 'REF'))>0)
		$sListaTmp = substr($sListaTmp, 0, $iPos);

	$sListaTmp = preg_replace('[a-zA-Z]', '', $sListaTmp);
	$iCountL = floor(strlen($sListaTmp)/8);
	for($i=0; $i<$iCountL; ++$i){
		$sItem=mb_substr($sListaTmp, $iStart, 8, 'UTF-8');
		if(strlen($sItem)==8)
			$aTransportNumers[]=$sItem;
		else break;
		$iStart+=8;
	}

	return $aTransportNumers;
}// end of getTransportNumbers() method


/**
 * Funkcja pobiera dane przelewu na podstawie nr referyncyjnego
 *
 * @global array $aConfig
 * @param type $sRef
 * @return type 
 */
function getOrdersLinkingTransportNumbers() {
	global $aConfig;
	
	$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_linking_transport_numbers";
	return Common::GetAll($sSql);
}


/**
 * Funkcja pobiera numer listu przewozowego na podstawie id zamówienia
 *
 * @global array $aConfig
 * @param type $iOId
 * @return type 
 */
function getTransportNumber($iOId) {
	global $aConfig;
	
	$sSql = "SELECT transport_number FROM ".$aConfig['tabls']['prefix']."orders
					 WHERE id='".$iOId."'";
	return Common::GetOne($sSql);
}// end of getTransportNumber() method

$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_payments_list
				 WHERE type='opek'";
$aOrdersPaymentsList = Common::GetAll($sSql);

// pobranie powiazań przelew -> zamowienie
$aLinks = getOrdersLinkingTransportNumbers();
Common::BeginTransaction();
foreach ($aLinks as $iKey => $aLink) {

	// pobierz statment id na podstawie tytułu przelewów
	$iStatment = getStatmentIdByTitle($aLink['transport_number']);
	if ($iStatment > 0) {
		$aValues = array(
				'orders_bank_statement_id' => $iStatment,
				//'transport_number' => getTransportNumber($aOrdersPayment['order_id']),
				//'order_id' => $aOrdersPayment['order_id'],
				//'topay' => $aOrdersPayment['ammount'],
				//'payed' => $aOrdersPayment['ammount'],
		);
		//. " AND transport_number=".getTransportNumber($aOrdersPayment['order_id'])
		if (Common::Update($aConfig['tabls']['prefix']."orders_linking_transport_numbers", $aValues, ' id='.$aLink['id']) === false) {
			echo 'Wystąpił blad podczas dodawania powiazania przelewu z zamowieniem<br />';
			dump($aValues);
			Common::RollbackTransaction();
			die;
		}
		echo 'Dodano powiązanie zamówienia z przelewem '.$iKey.'<br />';
		dump($aValues);
	} else {
		Common::RollbackTransaction();
		echo 'Błąd  - brak id nr przelewu<br />';
		dump($aLink);
		die;
	}
}

// jeśli przelewy są opek i są nizrealizowane

$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_bank_statements 
				 WHERE matched_order IS NULL AND is_opek='1'";
$aOrdBankStatments = Common::GetAll($sSql);
foreach ($aOrdBankStatments as $iKey => $aBankStatment) {
	// na podstawie titla inserty do tabeli orders_linking_transport_numbers
	if (addLinkingTransportNumbers($aBankStatment['id']) === false) {
		Common::RollbackTransaction();
		echo 'Wystąpił błąd podczas podawania powiązania <br />';
		dump($aBankStatment);
		die;
	}else {
		echo 'Dodano powiązanie zamówienie -> przelew <br />';
		dump($aBankStatment['id']);
	}
}

Common::CommitTransaction();
?>
