<?php
/**
 * Skrypt przenoszący informacje o załącznikach książki z tabeli orders_items_attachments 
 * do tabeli orders_items, oraz przeliczający ceny książki, załaczników
 * 
 */
$aConfig['_tmp']['location_prefix']='../';
$aConfig['common']['use_session'] = true;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');

/**
 * Metoda sprawdza czy element zamówienia posiada załącznik
 *
 * @global array $aConfig
 * @param integer $iIOId
 * @return integer
 */
function checkAttachments($iIOId) {
	global $aConfig;
	
	$sSql = "SELECT id FROM ".$aConfig['tabls']['prefix']."orders_items
					 WHERE parent_id = ".$iIOId." AND item_type='A' ";
	return Common::GetOne($sSql);
}// end of chceckAttachments() method

/**
 * Metoda pobiera dane o załączniku książki
 *
 * @param integer $iIOId
 * @return integer
 */
function getAttachmentsData($iIOId) {
	$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_items
					 WHERE parent_id = ".$iIOId." AND item_type='A' ";
	return Common::GetRow($sSql);
}// end of getAttachmentsData() method

// pobranie zamówień z załącznikami
$sSql = "SELECT A.*, 
							B.quantity AS product_quantity, 
							B.discount AS product_discount, 
							B.price_netto AS product_price_netto, 
							B.price_brutto AS product_price_brutto, 
							B.value_netto AS product_value_netto, 
							B.value_brutto AS product_value_brutto,
							B.shipment_time AS product_shipment_time,
							B.preview AS product_preview,
							B.shipment_date AS product_shipment_date,
							B.status AS product_status,
							B.deleted AS product_deleted,
							B.second_invoice AS product_second_invoice
				 FROM ".$aConfig['tabls']['prefix']."orders_items_attachments AS A
				 LEFT JOIN orders_items AS B
					ON A.order_item_id = B.id
				 GROUP BY A.id
				 ORDER BY id DESC
				 ";
$oAttachments = Common::PlainQuery($sSql);
if (!empty($oAttachments)) {
	while ($aItem =& $oAttachments->fetchRow(DB_FETCHMODE_ASSOC)) {
		$bIsErr = false;
		
		$aValues = array();
		// wyliczenie cen dla nowego załącznika w orders_items
		// ceny po promocji
		$aValues['promo_price_brutto'] =  Common::formatPrice2($aItem['price_brutto'] - $aItem['price_brutto']*($aItem['product_discount']/100));
		$aValues['promo_price_netto'] =  Common::formatPrice2(($aValues['promo_price_brutto'] / (1 + $aItem['vat']/100)));
		// wartości zamówienia - cena przemnożona przez ilość
		$aValues['value_brutto'] =  Common::formatPrice2($aItem['product_quantity'] * $aValues['promo_price_brutto']);
		$aValues['value_netto'] =  Common::formatPrice2($aItem['product_quantity'] * ($aValues['promo_price_brutto'] / (1 + $aItem['vat']/100)));
		
		// vat_currency nie ma uwzględniać ilości
		$aValues['vat_currency'] = Common::formatPrice2($aValues['promo_price_brutto'] - $aValues['promo_price_netto']); 
		// total_vat_currency uwzględnia ilość
		$aValues['total_vat_currency'] = Common::formatPrice2($aValues['value_brutto'] - $aValues['value_netto']); 
		$aValues['vat'] = $aItem['vat'];
		
		// rabat, ilość
		$aValues['discount'] = $aItem['product_discount'];
		// discount_currency Zdeprecjonowana Nie uwzglednia ilosci zamowionych pozycji
		$aValues['discount_currency'] = Common::formatPrice2($aItem['price_brutto'] * (Common::formatPrice2($aItem['product_discount']) / 100));
		// total_discount_currency uwzglednia ilosć zamowionych pozycji
		$aValues['total_discount_currency'] = Common::formatPrice2($aValues['discount_currency'] * $aItem['product_quantity']);
		$aValues['quantity'] = $aItem['product_quantity'];
		
		// cena netto i brutto pozostaje taka sama
		$aValues['price_brutto'] = Common::formatPrice2($aItem['price_brutto']);
		$aValues['price_netto'] = Common::formatPrice2(($aItem['price_brutto'] / (1 + $aItem['vat']/100)));
		// inne
		$aValues['order_id'] = $aItem['order_id'];
		$aValues['product_id'] = 'NULL';
		$aValues['attachment_id'] = $aItem['attachment_id']>0 ?$aItem['attachment_id']:'NULL' ;
		$aValues['name'] = $aItem['name'];
		$aValues['item_type'] = 'A';
		$aValues['parent_id'] = $aItem['order_item_id'];
		$aValues['shipment_time'] = $aItem['product_shipment_time'];
		$aValues['preview'] = $aItem['product_preview'];
		$aValues['shipment_date'] = $aItem['product_shipment_date'];
		$aValues['weight'] = '0.00';
		$aValues['status'] = $aItem['product_status'];
		$aValues['deleted'] = $aItem['product_deleted'];
		$aValues['sent_hasnc'] = '0';
		$aValues['added_by_employee'] = '0';
		$aValues['bundle_price_netto'] = 'NULL';
		$aValues['bundle_price_brutto'] = 'NULL';
		$aValues['bundle_value_netto'] = 'NULL';
		$aValues['bundle_value_brutto'] = 'NULL';
		
		Common::BeginTransaction();
		// dodanie
		$iOId = Common::Insert($aConfig['tabls']['prefix']."orders_items", $aValues);
		if (intval($iOId) <= 0) {
			// $bIsErr = true;
			// wystąpił błąd
			Common::RollbackTransaction();
			echo ' Wystąpił błąd podczas dodawania rekordu do orders_items <br />';
			dump($aValues);
			die;
		}
		echo 'Dodano załącznik o id: '.$iOId.'<br />';
		dump($aValues);
		Common::CommitTransaction();
	}
}


// pobranie wszystkich zamówień
$sSql = "SELECT * FROM ".$aConfig['tabls']['prefix']."orders_items
				 ORDER BY id DESC
";

$oAttachments = Common::PlainQuery($sSql);
if (!empty($oAttachments)) {
	while ($aItem =& $oAttachments->fetchRow(DB_FETCHMODE_ASSOC)) {
		// czy ma załaczniki
		$aValues = array();
		
		$iAId = checkAttachments($aItem['id']);
		if ($iAId > 0) {
			// wyliczenie cen dla książki z załącznikiem, przepisanie starych cen do kolumn bundle...
			//$aItem['quantity'] = 2;
			$aAData = getAttachmentsData($aItem['id']);
			if (empty($aAData)) {continue;}
			
			// do bundle lecą ceny w sumie pakietu
			$aValues['bundle_price_netto'] = $aItem['price_netto'];
			$aValues['bundle_price_brutto'] = $aItem['price_brutto'];
			$aValues['bundle_value_netto'] = $aItem['value_netto'];
			$aValues['bundle_value_brutto'] = $aItem['value_brutto'];

			$aAData['promo_price_brutto'] =  Common::formatPrice2($aAData['price_brutto'] - $aAData['price_brutto']*($aItem['discount']/100));
			$aValues['promo_price_brutto'] = Common::formatPrice2($aItem['promo_price_brutto'] - $aAData['promo_price_brutto']);
			$aValues['promo_price_netto'] = Common::formatPrice2(($aValues['promo_price_brutto'] / (1 + $aItem['vat']/100)));
			$aValues['value_netto'] = Common::formatPrice2($aItem['quantity'] * ($aValues['promo_price_brutto'] / (1 + $aItem['vat']/100)));// = Common::formatPrice2($aItem['quantity'] * ($aItem['promo_price_brutto'] / (1 + $aItem['vat']/100)));
			$aValues['value_brutto'] =  Common::formatPrice2($aItem['quantity'] * $aValues['promo_price_brutto']);
			

			$aValues['price_netto'] = Common::formatPrice2($aValues['bundle_price_netto'] - $aAData['price_netto']);
			$aValues['price_brutto'] = Common::formatPrice2($aValues['bundle_price_brutto'] - $aAData['price_brutto']);
			
			/*
			$aValues['value_netto'] = Common::formatPrice2($aValues['bundle_value_netto'] - $aAData['value_netto']);
			$aValues['value_brutto'] = Common::formatPrice2($aValues['bundle_value_brutto'] - $aAData['value_brutto']);
			*/
			
			// nie uwzględnia ilości
			$aValues['discount_currency'] = Common::formatPrice2($aValues['price_brutto'] * (Common::formatPrice2($aItem['discount']) / 100));
			
			// nie uwzględnia ilości
			$aValues['vat_currency'] = Common::formatPrice2($aValues['price_brutto'] - $aValues['price_netto']);
			// uwzględnia ilość
			$aValues['total_vat_currency'] = Common::formatPrice2($aValues['value_brutto'] - $aValues['value_netto']);

			// uwzględnia ilość
			$aValues['total_discount_currency'] = Common::formatPrice2($aValues['discount_currency'] * $aItem['quantity']);		
			
		} else {
			// zwykły element zamowienia - nie posiadający załącznika
			// uzupełnienie nowych kolumn
			
			if ($aItem['vat_currency'] == '') {
				// nie uwzględnia ilości
				$aValues['vat_currency'] = Common::formatPrice2($aItem['price_brutto'] - $aItem['price_netto']);
			}
			
			if ($aItem['total_vat_currency'] == '') {
				// uwzględnia ilość
				$aValues['total_vat_currency'] = Common::formatPrice2($aItem['value_brutto'] - $aItem['value_netto']);
			}

			if ($aItem['total_discount_currency'] == '') {
				// uwzględnia ilość
				$aValues['total_discount_currency'] = Common::formatPrice2($aItem['discount_currency'] * $aItem['quantity']);		
			}
		}
		// aktualizacja cen ksiązki
		if (!empty($aValues)) {
			Common::BeginTransaction();
			if (Common::Update($aConfig['tabls']['prefix']."orders_items", $aValues, " id=".$aItem['id']) === false) {
				Common::RollbackTransaction();
				echo ' Wystąpił błąd podczas dodawania rekordu do orders_items <br />';
				dump($aValues);
				die;
			}
			Common::CommitTransaction();
			//Common::RollbackTransaction();
			echo 'Zmodyfikowano ceny w zamówieniu: '.$aItem['id'].'<br />';
			dump($aValues);
			
		}
	}
}
?>