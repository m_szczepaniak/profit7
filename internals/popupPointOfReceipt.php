<?php
$aConfig['common']['use_session'] = true;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

$aMatches = array();
preg_match('/^\w+$/', $_GET['code'], $aMatches);
if ($aMatches[0] != '') {
  
  $iTransportId = $_SESSION['wu_cart']['payment']['transport_id'];
  $sPunktOdbioruCode = $aMatches[0];
  
  $oShipment = new \orders\Shipment($iTransportId, $pDbMgr);
  $aPopupDetails = $oShipment->getPopupDetails('PointsOfReceipt', $sPunktOdbioruCode);

} else {
  die;
}
?><!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Lokalizacja Punktu Odbioru Przesyłki</title>
    <style>
      body {
        margin: 0;
      }
    </style>
  </head>
  <body>
    <div style="padding: 10px; border: solid 1px black; background: <?php echo $aPopupDetails['backgroundColor']; ?>; color: <?php echo $aPopupDetails['fontColor']; ?>; font-family: Tahoma, Arial, Verdana, Helvetica;">
      <div style='position: absolute; right: 0; width: 151px; height: 72px; margin-right: 6px; margin-top: -10px;'></div>
      <?php echo $aPopupDetails['details']; ?>
    </div>
    <iframe 
  width="790" 
  height="620" 
  frameborder="0" 
  scrolling="no" 
  marginheight="0" 
  marginwidth="0" 
  src="<?php echo $aPopupDetails['source']; ?>"></iframe>
  </body>
</html>