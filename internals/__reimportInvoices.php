<?php
/**
 * Skrypt przenoszący informacje o fakturach zamówienia z bazy danych do folderu invoices 
 * 
 * 
 */
$aConfig['_tmp']['location_prefix']='../';
$aConfig['common']['use_session'] = true;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
set_time_limit(20800);

$sPathInv =  '../../invoices/1';
//$sPathInv = '../omniaCMS/invoices/1';
// pobierz faktury z bazy danych
$sPathPro =  '../../proforma/1';
//$sPathPro = '../omniaCMS/proforma/1';
// pobierz faktury proforma

/**
 * Funkcja zwaraca liczbe rekordów
 *
 * @return type 
 */
function getCountResults() {
	// pobierz wszystkie faktury z bazy danych
	$sSql = "SELECT count(id)
					FROM invoices
					WHERE id>27697
					";
	return Common::GetOne($sSql);
}// end of getCountResults() function

/**
	* Funkcja tworzy katalog docelowy
	* 
	* @return	bool	- true: utworzoo; false: wystapil blad
	*/
function createDirectory($sBaseDir, $sAPath) {
	global $aConfig;

	$aDirs = explode('/', $sAPath);
	foreach ($aDirs as $sDir) {
		$sPath .= '/'.$sDir;
		if (!is_dir($sBaseDir.$sPath)) {
			if (!mkdir($sBaseDir.$sPath,
									$aConfig['default']['new_dir_mode'])) {
				return false;
			};
		}
	}
	return true;
} // end of createDirectory() function


$iResults = intval(getCountResults());
$iResults = 9800;

$iCount = 600;
$iIteration = intval(ceil($iResults / $iCount));

$i=0;
while ($iIteration >= $i) {

	// pobierz wszystkie faktury z bazy danych
	$sSql = "SELECT id, order_id, invoice_name, data, proforma, content
					FROM invoices
					WHERE id>27697";
	echo $sSql;
	$oProducts = Common::PlainQuery($sSql);
	while ($aItem =& $oProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
		// zapisz do pliku z prawidłowym katalogiem
		// określ katalog
		if ($aItem['proforma'] == '1') {
			$aMatches = array();
			$sPath = $sPathPro;
			// faktura_proforma00365_2011_12_PR24.pdf
			preg_match("/faktura_proforma\d+_(\d+)_(\d+)_PR24\.pdf/", $aItem['invoice_name'], $aMatches);
			if (!empty($aMatches)) {
				$sAddPath = '/'.$aMatches['1'].'/'.$aMatches['2'];
			} else {
				echo 'Błąd importu proforma o id='.$aItem['id'];
				continue;
			}
		} else {
			$aMatches = array();
			$sPath = $sPathInv;
			// 	faktura00377_00086_11PR24.pdf
			// faktury zawsze posiadają wypełnione data
			preg_match("/(\d+)-(\d+)-\d+/", $aItem['data'], $aMatches);
			$sAddPath = '/'.$aMatches['1'].'/'.$aMatches['2'];
		}
		createDirectory($sPath, $sAddPath);

		if (!file_exists($sPath.'/'.$sAddPath.'/'.$aItem['invoice_name'])) {
			dump(file_put_contents($sPath.'/'.$sAddPath.'/'.$aItem['invoice_name'], $aItem['content']));
			dump($sPath.'/'.$sAddPath.'/'.$aItem['invoice_name']);
		} else {
			dump($sPath.'/'.$sAddPath.'/'.$aItem['invoice_name']);
			echo 'Plik istnieje'.$aItem['id'];
		}
	}
	
		$oProducts->free();
		unset($oProducts);
		$i++;
		die;
}
?>
