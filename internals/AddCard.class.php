<?php
$aConfig['_tmp']['location_prefix']='../';
$aConfig['common']['use_session'] = true;
$_GET['lang'] = 'pl';
$_GET['lang_id'] = '1';
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once('modules/m_zamowienia/lang/'.(isset($_GET['lang'])?$_GET['lang']:'pl').'.php');


/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class AddCard {
  private $iProductId;
  private $iCount;

  function __construct($iProductId, $iCount) {
    $this->iProductId = intval($iProductId);
    $this->iCount = intval($iCount);
  }
  
  /**
   * Metoda sprawdza czy liczba książek która jest zamawiana mieści się w przedziale
   * od 1 do 10 000
   * 
   * @return boolean
   */
  private function validCount() {
    if ($this->iCount > 10000 || $this->iCount <= 0) {
      return false;
    }
    return true;
  }
  
  /**
   * Metoda sprawdza czy id jest dodatnie
   * 
   * @return boolean
   */
  private function validProductId() {
    if ($this->iProductId <= 0) {
      return false;
    }
    return true;
  }
  
  public function addProductsCard() {
    global $aConfig;
    
    if ($this->validCount() === false) {
      return -1;
    }
    
    if ($this->validProductId() === false) {
      return -2;
    }

    /*
    if (isset($_SESSION['wu_cart']) && isset($_SESSION['wu_cart']['products']) && count(array_keys($_SESSION['wu_cart']['products'])) >= 99) {
      return -3;
    }
    */

    //remove cart items limit
    if (isset($_SESSION['wu_cart']) && isset($_SESSION['wu_cart']['products']) && count(array_keys($_SESSION['wu_cart']['products'])) >= ($aConfig['common']['max_cart_items'] )) {
      return -3;
    }
    
    // dołączamy
    include_once('modules/m_zamowienia/client/Module.class.php');
    $oModuleOrders = new Module('m_zamowienia', null, 'zamowienia');
    // dodanie produktu do koszyka
    $oModuleOrders->addProduct($this->iProductId, true, $this->iCount);

    if (!empty($_SESSION['wu_cart']['products'])) {

      $limiter = new \Basket\Limit\Limiter();
      $checkedData = $limiter->determineMaximumAmount($_SESSION['wu_cart']['products']);
      $limitResult = $limiter->checkLimits($checkedData);
      if (true !== $limitResult) {
        $oModuleOrders->getStepOneObj();
        $oModuleOrders->RecountAll(TRUE, TRUE);
        $_SESSION['wu_cart']['just_added'] = 0;
        return $limitResult;
      }
    }

    $oModuleOrders->getStepOneObj();
    $oModuleOrders->RecountAll(FALSE, FALSE);
    $_SESSION['wu_cart']['just_added'] = 0;
    return 1;
  }
}

if (isset($_POST['product_id'])) {
    $oAddCard = new AddCard($_POST['product_id'], $_POST['count']);
    echo $oAddCard->addProductsCard();
} else if ($_GET['product_id']) {
    $oAddCard = new AddCard($_GET['product_id'], $_GET['count']);
    echo $oAddCard->addProductsCard();
    doRedirect(createProductLink($_GET['product_id'], 'd'));
}