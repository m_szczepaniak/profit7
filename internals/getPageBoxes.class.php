<?php
$oMemcache = NULL;

header('Content-Type: text/html; charset=UTF-8');
$aConfig['config']['project_dir'] = 'internals/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['_tmp']['location_prefix']='../';
$aConfig['common']['use_session'] = true;
$aConfig['use_db_manager'] = true;
//define('S_MEMCACHED', true);
//define('GET_SOURCE', true);
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10); // maksymalny czas wykonywania 3 godziny - 
$_GET['lang'] = 'pl';

// załadowanie boksu ajaxowego
class getBox{
  private $iBoxId; // id boksu
  private $iPageId; // id aktualnej strony - kiedyś powinno się przydać
  private $iLangId; // id wersji językowej
  
  public function __construct($iBoxId, $iPageId, $iLangId) {
    
    $this->iPageId = $iPageId; // kiedyś powinno się przydać
    $this->iBoxId = $iBoxId;
    $this->iLangId = $iLangId;
  }// end of __construct
  
  
  /**
   * Metoda pobirea dane boksu z bazy danych
   * 
   * @global array $aConfig
   * @param int $iBoxId
   * @param int $iPId
   * @param int $iLangId
   * @return array
   */
  private function _setBoxDataDB($iBoxId, $iPId, $iLangId) {
    global $aConfig;
    
    $sSql =" SELECT DISTINCT A.area_id AS aid, A.area_id, A.id, IFNULL(A.menu_id, 0) as menu_id,
                    IFNULL(A.module_id, 0) AS module_id,
                    IFNULL(A.page_id, 0) AS page_id, A.name,
                    A.template, A.btype, A.symbol, A.cacheable, A.moption, A.subpage,
                    A.cache_per_page,
                    C.symbol AS module,
                    D.name AS page_name, D.symbol AS page_symbol
              FROM ".$aConfig['tabls']['prefix']."boxes AS A
              LEFT JOIN ".$aConfig['tabls']['prefix']."boxes_settings AS B
                ON B.page_id = ".$iPId." AND
                   B.language_id = A.language_id AND
                   B.box_id = A.id
              LEFT JOIN ".$aConfig['tabls']['prefix']."modules AS C
                ON C.id = A.module_id
              LEFT JOIN ".$aConfig['tabls']['prefix']."menus_items AS D
                ON D.id = A.page_id AND
                   D.language_id = A.language_id
              WHERE A.language_id = ".$iLangId." AND A.id = ".$iBoxId;
    return Common::GetRow($sSql);
   }// end of setBoxDataDB() method
   
   
   /**
    * Metoda pobiera zawartość boksu
    * 
    * @return string
    */
   public function getBoxContent() {
     global $aConfig, $pSmarty;
     
     
    $aBox = $this->_setBoxDataDB($this->iBoxId, $this->iPageId, $this->iLangId);
    $aConfig['_tmp']['selected'][] = $this->iPageId;
    if (!empty($aBox)) {
      // dowiazanie tablicy z wersja jezykowa do Smartow
      include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');
      $pSmarty->assign_by_ref('aLang', $aConfig['lang']);

      return $this->getBoxClass($aBox);
      /*
      $sFile = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].'boxes/'.$aBox['module'].'/client/Box.class.php';
      $sLangFile = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].
                   'boxes/'.$aBox['module'].'/lang/pl.php';
      $sClass = 'Box_'.$aBox['module'];

      if (file_exists($sFile)) {
        if (!class_exists($sClass)) {
          include_once($sFile);
        }
        if (file_exists($sLangFile)) {
          include_once($sLangFile);
        }
        $oBox = new $sClass($aBox);
        return $oBox->getContent();
      }
       */
     }// end of getBoxContent() method
   }
   

   /**
    * Metoda wykonuje klase boksu i wykonuje metodę getContents
    * 
    * @global array $aConfig
    * @param type $aBox
    * @return boolean
    */
    private function getBoxClass($aBox){
      global $aConfig;

      switch($aBox['btype']) {
         case '1':
           // boks menu
           $aBox['module'] = 'm_menu';

           $sFile = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].'boxes/'.$aBox['module'].'/client/Box.class.php';
           $sLangFile = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].
                        'boxes/'.$aBox['module'].'/lang/'.$_GET['lang'].'.php';
           $sClass = 'Box_'.$aBox['module'];
           if (file_exists($sFile)) {
             if (!class_exists($sClass)) {
               include_once($sFile);
             }
             if (file_exists($sLangFile)) {
               include_once($sLangFile);
             }
             $oBox = new $sClass($aBox);
             return $oBox->getContent();
           }
         break;

         case '4':
           // boks menu - pierwszy poziom
           $aBox['module'] = 'm_menu_pierwszy_poziom';

           $sFile = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].'boxes/'.$aBox['module'].'/client/Box.class.php';
           $sLangFile = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].
                        'boxes/'.$aBox['module'].'/lang/'.$_GET['lang'].'.php';
           $sClass = 'Box_'.$aBox['module'];
           if (file_exists($sFile)) {
             if (!class_exists($sClass)) {
               include_once($sFile);
             }
             if (file_exists($sLangFile)) {
               include_once($sLangFile);
             }
             $oBox = new $sClass($aBox);
             return $oBox->getContent();
           }
         break;

         case '2':
           // boks modulu
           $sFile = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].
                    'boxes/'.$aBox['module'].'/client/Box'.(!empty($aBox['moption']) ? '_'.$aBox['moption'] : '').'.class.php';
           $sLangFile = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].
                    'boxes/'.$aBox['module'].'/lang/'.$_GET['lang'].(!empty($aBox['moption']) ? '_'.$aBox['moption'] : '').'.php';
           $sClass = 'Box_'.$aBox['module'].
                     (!empty($aBox['moption']) ? '_'.$aBox['moption'] : '');
           if (file_exists($sFile)) {
             if (!class_exists($sClass)) {
               include_once($sFile);
             }
             if (file_exists($sLangFile)) {
               include_once($sLangFile);
             }
             $oBox = new $sClass($aBox);
             return $oBox->getContent();
           }
         break;

         case '3':
           // boks pusty
           $sFile = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].
                    'boxes/e_'.$aBox['symbol'].'/client/Box.class.php';
           $sLangFile = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].
                    'boxes/e_'.$aBox['symbol'].'/lang/'.$_GET['lang'].'.php';
           $sClass = 'Box_e_'.$aBox['symbol'];

           if (file_exists($sFile)) {
             if (!class_exists($sClass)) {
               include_once($sFile);
             }
             if (file_exists($sLangFile)) {
               include_once($sLangFile);
             }
             $oBox = new $sClass($aBox);
             return $oBox->getContent();
           }
         break;
       }
       return false;
    }// end of getBoxClass() method
}

$oBox = new getBox(intval($_GET['box_id']), intval($_GET['page_id']), intval($_GET['lang_id']));
$sBoxCont = $oBox->getBoxContent();
echo $sBoxCont;
?>
