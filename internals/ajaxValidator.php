<?php
$aConfig['common']['use_session'] = true;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once('Form/Validator.class.php');
$oValidator = new Validator();

//print_r(unserialize(base64_decode($_POST['__Validator'])));
//print_r($_POST);die();
	$sOKFields = '';
	$sError = '';
	$sErrFields = '';
	$aOKFields = array();
	$aErrFields = array();
	$aValidator = unserialize(base64_decode($_POST['__Validator']));
	foreach ($aValidator as $sName => $aData) {
    $mValue = $oValidator->getPoolData($sName);
    if (!isset($aData['depends_on']) || 
       ($oValidator->checkIsRequired($aData) === TRUE)) {
      
			$bOK = true;
			switch ($aData['type']) {
				case 'confirm_password':
					if (isset($aData['required']) && $mValue == '') {
//						if(empty($sError)){
//							$sError = $aData['err_msg'];
//						}
					//	$sErrFields .= '#'.$sName.',';
						$aErrFields[$sName] = $aData['err_msg'];
						$bOK = false;
					}	else {
						if (strpos($aData['passwd_field'], '[') != false) {
						
							$sPasswdName = substr($aData['passwd_field'], 0, strpos($aData['passwd_field'], '['));
							$sPasswdTableKeys = str_replace('[]', ' ', substr($aData['passwd_field'],
																								strpos($aData['passwd_field'], '[')));
						}
						else {
							$sPasswdName = $aData['passwd_field'];
							$sPasswdTableKeys = '';
						}
						eval('$sPasswdValue =& $_POST["'.$sPasswdName.'"]'.$sPasswdTableKeys.';');
						//if (!empty($sPasswdValue)) {
							if ($mValue != $sPasswdValue) {
//								if(empty($sError)){
//									$sError = $aData['err_msg'];
//								}
							//	$sErrFields .= '#'.$sName.',';
								$aErrFields[$sName] = $aData['err_msg'];
								$bOK = false;
							}
						//}
					}
				break;
				
				case 'file':
					if (!isset($_FILES[$sVarName]['name']) || empty($_FILES[$sVarName]['name'])) {
//						if(empty($sError)){
//							$sError = $aData['err_msg'];
//						}
					//	$sErrFields .= '#'.$sName.',';
						$aErrFields[$sName] = $aData['err_msg'];
						$bOK = false;
					}
				break;
				
				case 'radio_set':
					if ($mValue == '') {
//						if(empty($sError)){
//							$sError = $aData['err_msg'];
//						}
					//	$sErrFields .= '#'.$sName.',';
						$aErrFields[$sName] = $aData['err_msg'];
						$bOK = false;
					}
				break;
				
				default:
					if (isset($aData['pcre']) && !empty($aData['pcre'])) {
						if (isset($aData['required']) && !$aData['required']) {
							// jezeli pole jest nieobowiazkowe, ale zostalo wypelnione i istnieje pcre
							// sprawdzenie zgodnosci z pcre
							if ($mValue != '' && !preg_match($aData['pcre'], $mValue)) {
//								if(empty($sError)){
//									$sError = $aData['err_msg'];
//								}
							//	$sErrFields .= '#'.$sName.',';
								$aErrFields[$sName] = $aData['err_msg'];
								$bOK = false;
							}
						}
						elseif (!preg_match($aData['pcre'], $mValue)) {
//							if(empty($sError)){
//								$sError = $aData['err_msg'];
//							}
						//	$sErrFields .= '#'.$sName.',';
						$aErrFields[$sName] = $aData['err_msg'];
							$bOK = false;
						}
					}
					elseif (isset($aData['range']) && !empty($aData['range'])) {
						$mValue = floatval(str_replace(',', '.', $mValue));
						if ($mValue < $aData['range'][0] || $mValue > $aData['range'][1]) {
//							if(empty($sError)){
//								$sError = $aData['err_msg'];
//							}
						//	$sErrFields .= '#'.$sName.',';
							$aErrFields[$sName] = $aData['err_msg'];
							$bOK = false;
						}
					}
					elseif ($mValue == '') {
//						if(empty($sError)){
//							$sError = $aData['err_msg'];
//						}
					//	$sErrFields .= '#'.$sName.',';
						$aErrFields[$sName] = $aData['err_msg'];
						$bOK = false;
					}
				break;
			}
			if($bOK){
			//	$sOKFields .= '#'.$sName.',';
				$aOKFields[] = $sName;
			}
		}
	}
	// dodatkowe elementy walidacji
	if(!empty($_POST['__ajaxValidator'])){
		//$aConfig['common']['use_session'] = false;
		include_once('../omniaCMS/config/common.inc.php');
		include_once('../omniaCMS/config/ini.inc.php');
		
		// formularz rejestracji - pierwszy krok
		if($_POST['__ajaxValidator'] === 'register1'){
			// czy email nie powtarza sie w bazie
			if(!empty($_POST['email'])){
				$sSql = "SELECT count(id)
							 FROM ".$aConfig['tabls']['prefix']."users_accounts
							 WHERE 
                deleted ='0' AND 
                registred = '1' AND
                website_id=".$aConfig['profit24_website_id']." AND
                email = ".Common::Quote(strtolower($_POST['email']));
				if(intval(Common::GetOne($sSql)) > 0){
					if(empty($aErrFields['email'])){
						//$sErrFields .= '#'.$sName.',';
						$aErrFields['email'] = 'Istnieje już Konto Użytkownika o podanym adresie e-mail!';
						//if(empty($sError)){
						//	$sError = 'Istnieje już Konto Użytkownika o podanym adresie e-mail!';
						//}
						if(($mKey = array_search('email',$aOKFields)) !== false){
							unset($aOKFields[$mKey]);
						}
					}
				}
			}
			// czy pole powtórz email jest zgodne z email
			if(!empty($_POST['confirm_email'])){
				if($_POST['confirm_email'] !== $_POST['email']){
					if(empty($aErrFields['confirm_email'])){
						$aErrFields['confirm_email'] = 'Podane aresy email różnią się od siebie!';
						if(($mKey = array_search('confirm_email',$aOKFields)) !== false){
							unset($aOKFields[$mKey]);
						}
					}
				}
			}
		}
    
    if($_POST['__ajaxValidator'] === 'order_without_register'){
			// czy email nie powtarza sie w bazie
			if(!empty($_POST['email'])){
				$sSql = "SELECT count(id)
							 FROM ".$aConfig['tabls']['prefix']."users_accounts
							 WHERE 
                deleted = '0' AND 
                registred = '1' AND
                website_id=".$aConfig['profit24_website_id']." AND
                email = ".Common::Quote(strtolower($_POST['email']));
				if(intval($pDbMgr->GetOne('profit24', $sSql)) > 0){
					if(empty($aErrFields['email'])){
						//$sErrFields .= '#'.$sName.',';
						$aErrFields['email'] = 'Istnieje już Konto Użytkownika o podanym adresie e-mail!';
						//if(empty($sError)){
						//	$sError = 'Istnieje już Konto Użytkownika o podanym adresie e-mail!';
						//}
						if(($mKey = array_search('email',$aOKFields)) !== false){
							unset($aOKFields[$mKey]);
						}
					}
				}
			}
      
			// czy pole powtórz email jest zgodne z email
			if(!empty($_POST['confirm_email'])){
				if($_POST['confirm_email'] !== $_POST['email']){
					if(empty($aErrFields['confirm_email'])){
						$aErrFields['confirm_email'] = 'Podane aresy email różnią się od siebie!';
						if(($mKey = array_search('confirm_email',$aOKFields)) !== false){
							unset($aOKFields[$mKey]);
						}
					}
				}
			}
    }
    
		// formularz rejestracji - drugi krok
		if($_POST['__ajaxValidator'] === 'register2'){
			// czy pole powtórz email jest zgodne z email
			if(!empty($_POST['priv_city_1'])){
				if(!checkCityPostal($_POST['priv_city_1'],$_POST['priv_postal_1'])){
					$aWarnFields['priv_city_1'] = 'Miejscowość nie jest zgodna z kodem pocztowym!';
					if(($mKey = array_search('priv_city_1',$aOKFields)) !== false){
						unset($aOKFields[$mKey]);
					}
				} 
			}
			if(!empty($_POST['priv_city_2'])){
				if(!checkCityPostal($_POST['priv_city_2'],$_POST['priv_postal_2'])){
					$aWarnFields['priv_city_2'] = 'Miejscowość nie jest zgodna z kodem pocztowym!';
					if(($mKey = array_search('priv_city_2',$aOKFields)) !== false){
						unset($aOKFields[$mKey]);
					}
				} 
			}
			if(!empty($_POST['corp_city_1'])){
				if(!checkCityPostal($_POST['corp_city_1'],$_POST['corp_postal_1'])){
					$aWarnFields['priv_city_1'] = 'Miejscowość nie jest zgodna z kodem pocztowym!';
					if(($mKey = array_search('priv_city_1',$aOKFields)) !== false){
						unset($aOKFields[$mKey]);
					}
				} 
			}
			if(!empty($_POST['corp_city_2'])){
				if(!checkCityPostal($_POST['corp_city_2'],$_POST['corp_postal_2'])){
					$aWarnFields['priv_city_1'] = 'Miejscowość nie jest zgodna z kodem pocztowym!';
					if(($mKey = array_search('priv_city_1',$aOKFields)) !== false){
						unset($aOKFields[$mKey]);
					}
				} 
			}
		}
	// formularz zmiany email
		if($_POST['__ajaxValidator'] === 'emailChange'){
			// czy email nie powtarza sie w bazie
			if(!empty($_POST['email'])){
				$sSql = "SELECT count(id)
							 FROM ".$aConfig['tabls']['prefix']."users_accounts
							 WHERE 
               deleted = '0' AND
               registred = '1' AND
               website_id=".$aConfig['profit24_website_id']." AND 
               email = ".Common::Quote(strtolower($_POST['email'])).
								($_SESSION['w_user']['id'] > 0 ? " AND id <> ".$_SESSION['w_user']['id'] : '');
				if(intval($pDbMgr->GetOne('profit24', $sSql)) > 0){
					$aErrFields['email'] = 'Istnieje już Konto Użytkownika o podanym adresie e-mail!';
					if(($mKey = array_search('email',$aOKFields)) !== false){
						unset($aOKFields[$mKey]);
					}
				}
			}
			// czy pole powtórz email jest zgodne z email
			if(!empty($_POST['confirm_email'])){
				if($_POST['confirm_email'] !== $_POST['email']){
					$aErrFields['confirm_email'] = 'Podane aresy email różnią się od siebie!';
					if(($mKey = array_search('confirm_email',$aOKFields)) !== false){
						unset($aOKFields[$mKey]);
					}
				}
			}
		}
    
    
	// formularz sprawdzania statusu zamówienia bez rejestracji
	if($_POST['__ajaxValidator'] === 'get_order'){
    // czy email znajduje się w bazie zamówień
    $mKEmail = array_search('email',$aOKFields);
    if($mKEmail !== false && !empty($_POST['email'])){
      $sSql = "SELECT id
             FROM ".$aConfig['tabls']['prefix']."orders
             WHERE 
              email = ".Common::Quote(strtolower($_POST['email']));
      if(intval(Common::GetOne($sSql)) <= 0){
        $sError = 'Nie złożono zamówienia na podany adres e-mail!';
        $aErrFields['email'] = $sError;
        unset($aOKFields[$mKEmail]);
      }
    }

    // czy numer zamówienia znajduje się w bazie, niezależnie od statusu
    $mKOrderNumber = array_search('order_number',$aOKFields);
    if($mKOrderNumber !== false && !empty($_POST['order_number'])){
      $sSql = "SELECT id
             FROM ".$aConfig['tabls']['prefix']."orders
             WHERE 
              ( order_number = ".Common::Quote($_POST['order_number'])." OR check_status_key = ".Common::Quote($_POST['order_number'])." )";
      if(intval(Common::GetOne($sSql)) <= 0){
        $sError = 'Nie znaleziono zamówienia o podanym haśle!';
        $aErrFields['order_number'] = $sError;
        unset($aOKFields[$mKOrderNumber]);
      }
    }
    
    $mKOrderNumber = array_search('order_number',$aOKFields);
    $mKEmail = array_search('email',$aOKFields);
    // czy numer zamówienia znajduje się w bazie, niezależnie od statusu
    if($mKEmail !== false && $mKOrderNumber !== false && !empty($_POST['order_number']) && !empty($_POST['email'])){
      $sSql = "SELECT id
             FROM ".$aConfig['tabls']['prefix']."orders
             WHERE 
                  ( order_number = ".Common::Quote($_POST['order_number'])." OR check_status_key = ".Common::Quote($_POST['order_number'])." )
              AND email = ".Common::Quote($_POST['email']);
      if(intval(Common::GetOne($sSql)) <= 0){
        $sError = 'Zamówienie nie zostało złożone na podany adres e-mail!';
        $aErrFields['order_number'] = $sError;
        unset($aOKFields[$mKOrderNumber]);
        unset($aOKFields[$mKEmail]);
      }
    }
    
  }
    
	// formularz zmiany danych
		if($_POST['__ajaxValidator'] === 'editAddress'){
			// czy pole miejscowosc jest zgodne z kodem pocztowym
			if(!empty($_POST['priv_city_0'])){
				if(!checkCityPostal($_POST['priv_city_0'],$_POST['priv_postal_0'])){
					$aWarnFields['priv_city_0'] = 'Miejscowość nie jest zgodna z kodem pocztowym!';
					if(($mKey = array_search('priv_city_0',$aOKFields)) !== false){
						unset($aOKFields[$mKey]);
					}
				} 
			}
			if(!empty($_POST['corp_city_0'])){
				if(!checkCityPostal($_POST['corp_city_0'],$_POST['corp_postal_0'])){
					$aWarnFields['priv_city_0'] = 'Miejscowość nie jest zgodna z kodem pocztowym!';
					if(($mKey = array_search('priv_city_0',$aOKFields)) !== false){
						unset($aOKFields[$mKey]);
					}
				} 
			}
		}
	}
	
	
/*	$sOKFields = '';
	foreach($aOKFields as $sField){
		$sOKFields.='#'.$sField.',';
	}
	if(!empty($sOKFields)){
		$sOKFields = substr($sOKFields,0,-1);
	}
	foreach($aErrFields as $sField){
		$sErrFields.='#'.$sField.',';
	}
	if(!empty($sErrFields)){
		$sErrFields = substr($sErrFields,0,-1);
	}
	echo $sOKFields.':'.$sErrFields.':'.$sError;
	*/
	$sResult = '';
	foreach($aOKFields as $sField){
		$sResult.=$sField.':OK:,';
	}
	foreach($aErrFields as $sField=>$sMsg){
		$sResult.=$sField.':ERR:'.$sMsg.',';
	}
  if (isset($aWarnFields) && !empty($aWarnFields)) {
    foreach($aWarnFields as $sField=>$sMsg){
      $sResult.=$sField.':WARN:'.$sMsg.',';
    }
  }
	if(!empty($sResult)){
		$sResult=substr($sResult,0,-1);
	}
	echo $sResult;
	//echo $sOKFields."\n";
	//echo $sErrFields."\n";
	//echo $sError."\n";
?>
