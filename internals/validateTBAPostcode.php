<?php
/**
 * Pobieranie listy wyboru punktów odbioru w zależności od wybranego rodzaju transportu
 * 
 * @author Paweł Bromka
 * @created 2014-12-05 
 * @copyrights Marcin Chudy - Profit24.pl
 */
header("Content-Type:text/html; charset=utf-8");
$aConfig['common']['use_session'] = false;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';
global $aConfig;

$sPostcode = $_POST['postcode'];

$bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
$oShipment = new \orders\Shipment('tba', $pDbMgr, $bTestMode);

$bValidatePostcode = $oShipment->validatePostcode('PointsOfReceipt', $sPostcode);
$sResponse['status'] = $bValidatePostcode;

echo json_encode($sResponse);
