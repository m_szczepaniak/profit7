<?php
$aConfig['common']['use_session'] = true;
include_once('../omniaCMS/config/common.inc.php');
include_once('../omniaCMS/config/ini.inc.php');
          
// funkcja dla kodów pocztowych
function checkCityPostal($sCity,$sPostal){
	global $aConfig, $pDbMgr;
	$sSql = "SELECT count(1)
					FROM ".$aConfig['tabls']['prefix']."users_accounts_postals
					WHERE city = ".Common::Quote(stripslashes($sCity)).
					 " AND postal = ".Common::Quote(stripslashes($sPostal));
	return (intval($pDbMgr->GetOne('profit24', $sSql)) > 0);
}

// czy wysłano nawę pola do walidacji
	if(!empty($_GET['__field'])){
    include_once('Form/Validator.class.php');
    $oValidator = new Validator();
    $mValue = $oValidator->getPoolData($_GET['__field']);
    
		// czy pole nie jest puste - nie walidujemy pustego
		if($mValue != ''){
			$aValidator = unserialize(base64_decode($_POST['__Validator']));
			// czy istnieje definicja walidatora dla pola
			if(!empty($aValidator[$_GET['__field']])){
			$aData =& $aValidator[$_GET['__field']];
      
			if (!isset($aData['depends_on']) || 
         ($oValidator->checkIsRequired($aData) === TRUE)) {
				
				$bOK = true;
				$bErr = false;
				$bWarn = false;
				switch ($aData['type']) {
					case 'confirm_password':
						if (isset($aData['required']) && $mValue == '') {
							$sError = $aData['err_msg'];
							$bErr = true;
							$bOK = false;
						}	else {
							if (strpos($aData['passwd_field'], '[') != false) {
							
								$sPasswdName = substr($aData['passwd_field'], 0, strpos($aData['passwd_field'], '['));
								$sPasswdTableKeys = str_replace('[]', ' ', substr($aData['passwd_field'],
																									strpos($aData['passwd_field'], '[')));
							}
							else {
								$sPasswdName = $aData['passwd_field'];
								$sPasswdTableKeys = '';
							}
							eval('$sPasswdValue =& $_POST["'.$sPasswdName.'"]'.$sPasswdTableKeys.';');
							//if (!empty($sPasswdValue)) {
								if ($mValue != $sPasswdValue) {
									$sError = $aData['err_msg'];
									$bErr = true;
									$bOK = false;
								}
							//}
						}
					break;
					
					case 'file':
						if (!isset($_FILES[$sVarName]['name']) || empty($_FILES[$sVarName]['name'])) {
							$sError = $aData['err_msg'];
							$bErr = true;
							$bOK = false;
						}
					break;
					
					case 'radio_set':
						if ($mValue == '') {
							$sError = $aData['err_msg'];
							$bErr = true;
							$bOK = false;
						}
					break;
					
					default:
						if (isset($aData['pcre']) && !empty($aData['pcre'])) {
							if (isset($aData['required']) && !$aData['required']) {
								// jezeli pole jest nieobowiazkowe, ale zostalo wypelnione i istnieje pcre
								// sprawdzenie zgodnosci z pcre
								if ($mValue != '' && !preg_match($aData['pcre'], $mValue)) {
									$sError = $aData['err_msg'];
									$bErr = true;
									$bOK = false;
								}
							}
							elseif (!preg_match($aData['pcre'], $mValue)) {
								$sError = $aData['err_msg'];
								$bErr = true;
								$bOK = false;
							}
						}
						elseif (isset($aData['range']) && !empty($aData['range'])) {
							$mValue = floatval(str_replace(',', '.', $mValue));
							if ($mValue < $aData['range'][0] || $mValue > $aData['range'][1]) {
								$sError = $aData['err_msg'];
								$bErr = true;
								$bOK = false;
							}
						}
						elseif ($mValue == '') {
							$sError = $aData['err_msg'];
							$bErr = true;
							$bOK = false;
						}
					break;
				}
				// dodatkowe elementy walidacji
				if(!empty($_POST['__ajaxValidator'])){
					
					// formularz rejestracji - pierwszy krok
					if($_POST['__ajaxValidator'] === 'register1'){
						// czy email nie powtarza sie w bazie
						if($_GET['__field'] == 'email' && !empty($_POST['email'])){
							$sSql = "SELECT count(id)
										 FROM ".$aConfig['tabls']['prefix']."users_accounts
										 WHERE deleted = '0' AND
                     registred = '1' AND
										 website_id = ".$aConfig['profit24_website_id']." AND
										 email = ".Common::Quote(strtolower($_POST['email']));
							if(intval($pDbMgr->GetOne('profit24', $sSql)) > 0){
								$bErr = true;
								$bOK = false;
								$sError = 'Istnieje już Konto Użytkownika o podanym adresie e-mail!';
							}
						}
						// czy pole powtórz email jest zgodne z email
						if($_GET['__field'] == 'confirm_email' && !empty($_POST['confirm_email'])){
							if($_POST['confirm_email'] !== $_POST['email']){
								$bErr = true;
								$bOK = false;
								$sError = 'Podane aresy email różnią się od siebie!';
							}
						}
					}
					// formularz rejestracji - drugi krok
					if($_POST['__ajaxValidator'] === 'register2'){
						// czy pole miejscowosc jest zgodne z kodem pocztowym
						if($_GET['__field'] == 'priv_city_1' && !empty($_POST['priv_city_1'])){
							if(!checkCityPostal($_POST['priv_city_1'],$_POST['priv_postal_1'])){
								$bErr = false;
								$bOK = false;
								$bWarn = true;
								$sError = 'Miejscowość nie jest zgodna z kodem pocztowym!';
							} 
						}
						if($_GET['__field'] == 'priv_city_2' && !empty($_POST['priv_city_2'])){
							if(!checkCityPostal($_POST['priv_city_2'],$_POST['priv_postal_2'])){
								$bErr = false;
								$bOK = false;
								$bWarn = true;
								$sError = 'Miejscowość nie jest zgodna z kodem pocztowym!';
							} 
						}
						if($_GET['__field'] == 'corp_city_1' && !empty($_POST['corp_city_1'])){
							if(!checkCityPostal($_POST['corp_city_1'],$_POST['corp_postal_1'])){
								$bErr = false;
								$bOK = false;
								$bWarn = true;
								$sError = 'Miejscowość nie jest zgodna z kodem pocztowym!';
							} 
						}
						if($_GET['__field'] == 'corp_city_2' && !empty($_POST['corp_city_2'])){
							if(!checkCityPostal($_POST['corp_city_2'],$_POST['corp_postal_2'])){
								$bErr = false;
								$bOK = false;
								$bWarn = true;
								$sError = 'Miejscowość nie jest zgodna z kodem pocztowym!';
							} 
						}
					}
				// formularz zmiany email
					if($_POST['__ajaxValidator'] === 'emailChange'){
						// czy email nie powtarza sie w bazie
						if($_GET['__field'] == 'email' && !empty($_POST['email'])){
							$sSql = "SELECT count(id)
										 FROM ".$aConfig['tabls']['prefix']."users_accounts
										 WHERE deleted = '0' 
                     registred = '1' AND
										 website_id=".$aConfig['profit24_website_id']." AND 
										 email = ".Common::Quote(strtolower($_POST['email'])).
											($_SESSION['w_user']['id'] > 0 ? " AND id <> ".$_SESSION['w_user']['id'] : '');
							if(intval($pDbMgr->GetOne('profit24', $sSql)) > 0){
								$bErr = true;
								$bOK = false;
								$sError = 'Istnieje już Konto Użytkownika o podanym adresie e-mail!';
							}
						}
						// czy pole powtórz email jest zgodne z email
						if($_GET['__field'] == 'confirm_email' && !empty($_POST['confirm_email'])){
							if($_POST['confirm_email'] !== $_POST['email']){
								$bErr = true;
								$bOK = false;
								$sError = 'Podane aresy email różnią się od siebie!';
							}
						}
					}
          
          // formularz zmiany danych
					if($_POST['__ajaxValidator'] === 'editAddress'){
						// czy pole miejscowosc jest zgodne z kodem pocztowym
						if($_GET['__field'] == 'priv_city_0' && !empty($_POST['priv_city_0'])){
							if(!checkCityPostal($_POST['priv_city_0'],$_POST['priv_postal_0'])){
								$bErr = false;
								$bOK = false;
								$bWarn = true;
								$sError = 'Miejscowość nie jest zgodna z kodem pocztowym!';
							} 
						}
						if($_GET['__field'] == 'corp_city_0' && !empty($_POST['corp_city_0'])){
							if(!checkCityPostal($_POST['corp_city_0'],$_POST['corp_postal_0'])){
								$bErr = false;
								$bOK = false;
								$bWarn = true;
								$sError = 'Miejscowość nie jest zgodna z kodem pocztowym!';
							} 
						}
					}
          
          // formularz zamowienia bez rejestracji
          if($_POST['__ajaxValidator'] === 'order_without_register'){

            // czy email nie powtarza sie w bazie
            if($_GET['__field'] == 'email' && !empty($_POST['email'])){
              $sSql = "SELECT count(id)
                     FROM ".$aConfig['tabls']['prefix']."users_accounts
                     WHERE 
                      deleted = '0' AND 
                      registred = '1' AND
                      website_id = ".$aConfig['profit24_website_id']." AND
                      email = ".Common::Quote(strtolower($_POST['email']));
              if(intval(Common::GetOne($sSql)) > 0){
								$bErr = true;
								$bOK = false;
								$sError = 'Istnieje już Konto Użytkownika o podanym adresie e-mail!';
              }
            }
            // czy pole powtórz email jest zgodne z email
						if($_GET['__field'] == 'confirm_email' && !empty($_POST['confirm_email'])){
							if($_POST['confirm_email'] !== $_POST['email']){
								$bErr = true;
								$bOK = false;
								$sError = 'Podane aresy email różnią się od siebie!';
							}
						}

           // czy pole miejscowosc jest zgodne z kodem pocztowym
            if (strstr($_GET['__field'], '[city]')) {
              $sPrefixName = str_replace('[city]', '', $_GET['__field']);
              if(!checkCityPostal($_POST[$sPrefixName]['city'],$_POST[$sPrefixName]['postal'])){
								$bErr = false;
								$bOK = false;
								$bWarn = true;
								$sError = 'Miejscowość nie jest zgodna z kodem pocztowym!';
							} 
            }
          }
          
          // formularz sprawdzania statusu zamówienia bez rejestracji
          if($_POST['__ajaxValidator'] === 'get_order'){
            // czy email znajduje się w bazie zamówień
            if($_GET['__field'] == 'email' && !empty($_POST['email'])){
              $sSql = "SELECT id
                     FROM ".$aConfig['tabls']['prefix']."orders
                     WHERE 
                      email = ".Common::Quote(strtolower($_POST['email']));
              if(intval(Common::GetOne($sSql)) <= 0){
								$bErr = true;
								$bOK = false;
								$sError = 'Nie złożono zamówienia na podany adres e-mail!';
              }
            }
            
            // czy numer zamówienia znajduje się w bazie, niezależnie od statusu
            if($_GET['__field'] == 'order_number' && !empty($_POST['order_number'])){
              $sSql = "SELECT id
                     FROM ".$aConfig['tabls']['prefix']."orders
                     WHERE 
                      order_number = ".Common::Quote($_POST['order_number'])." OR check_status_key = ".Common::Quote($_POST['order_number']);
              if(intval(Common::GetOne($sSql)) <= 0){
								$bErr = true;
								$bOK = false;
								$sError = 'Nie znaleziono zamówienia o podanym haśle!';
              }
            }
          }
				}
				//
				echo $_GET['__field'].':'.($bWarn?'WARN':($bOK?'OK':'ERR')).':'.$sError;
			}
			}
		}
	}
?>
