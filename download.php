<?php
include_once('omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ('omniaCMS/config/ini.inc.php');

if(!empty($_GET['file'])) {
	// wydzielenie danych pliku
	$aFile = explode('#', base64_decode($_GET['file']));
	/*
		$aFile[0]	- nazwa tabeli
		$aFile[1]	- serializowana tablica z lista pol i ich wartosci
		$aFile[2]	- Id pliku
	*/
	if (count($aFile) == 3 && is_array($aFields = unserialize($aFile[1]))) {
		// pobranie pliku
		$sSql = "SELECT id, directory, filename, description, name, mime, size
						 FROM ".$aFile[0]."
					 	 WHERE";
		$sWhere = '';
		foreach ($aFields as $sKey => $iValue) {
			$sWhere .= " ".$sKey." = ".$iValue." AND";
		}
		$sWhere .= " id = ".$aFile[2];
		$aItem =& Common::GetRow($sSql.$sWhere);
		if (!empty($aItem)) {
			$sDir = $aConfig['common']['file_dir'];
			if (!empty($aItem['directory'])) {
				$sDir .= '/'.$aItem['directory'];
			}
			if (($iSize = filesize($aConfig['common']['base_path'].$sDir.'/'.$aItem['filename'])) !== false && $oF = fopen($aConfig['common']['base_path'].$sDir.'/'.$aItem['filename'], 'rb')) {			
				header('Content-Type: '.$aItem['mime']);
				header('Content-Disposition: attachment; filename="'.$aItem['name'].'"');
				header('Content-Transfer-Encoding: binary');
				header('Content-Length: '.$iSize);
	
				// pobieranie pliku
				while(!feof($oF)) {
					set_time_limit(0);
	
					// wysylanie zawartosci pliku do klienta
					echo fread($oF, $iSize);
				}
	
				fclose($oF);
				// zwiekszenie informacji o liczbie sciagniec
				$sSql = "UPDATE ".$aFile[0]." SET downloads = downloads + 1 WHERE".$sWhere;
				Common::Query($sSql);
			}
		}
	}
}
?>