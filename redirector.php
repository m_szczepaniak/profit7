<?php
include_once('omniaCMS/config/common.inc.php');
// nie wlaczaj sesji
$aConfig['common']['use_session'] = false;
include_once ('omniaCMS/config/ini.inc.php');

/**
 * Funkcja odpowiada za ustawienie klikniecia dla wyslanej wiadomosci opineo ceneo
 * 
 * @param	integer	$iId	- Id wiadomosci
 */
function hit($iId) {
	global $aConfig;
	
	$sSql = "UPDATE ".$aConfig['tabls']['prefix'].'orders_opinions_sent'."
					 SET clicked = '1', clicked_date=NOW()
					 WHERE id = ".$iId;
	Common::Query($sSql);
} // end of increaseHits() function

/**
 * Funkcja przekierowuje przegladarke uzytkownika na adres
 * zdefiniowany dla bannera
 * 
 * @param	string	$sUrl	- adres na ktory nastapi przekierowanie
 */
function redirect($sUrl) {
	header("Location: ".str_replace('{_;_}', ':', $sUrl));
} // end of redirect() function


if($_GET['id']!='' && $_GET['type']!='') {
	hit(intval($_GET['id']));
	
	redirect($_GET['type']=='C'?
	'http://www.ceneo.pl/3822_sklep_dodaj_opinie#scroll=add-review':
	'http://www.okazje.info.pl/sklep-internetowy/profit24-pl/#dodaj-opinie'
	);
}


?>