<?php
/**
 * Interfejs Raporty
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace reports\auto;

interface interface_report {
  function __construct();
  function getName();
  function getReport();
  function saveReport();
}

