<?php
/**
 * Generowanie odchylenia w cenach zakupu w źródłach, gdzie towar jest dostępny
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace reports\auto;

class Report_STD_products implements \reports\auto\interface_report {

  const REPORTS_FOLDER = '/LIB/reports/files/Report_STD_products';
  private $sName;
  private $aReport;
  private $pDbMgr;

  
  function __construct() {

    global $pDbMgr;
    
    $this->sName = 'Raport odchylenia w cenach zakupu';
    $this->pDbMgr = $pDbMgr;
  }
 
  
  /*
   *  Metoda zwraca nazwę raportu
   */  
  function getName() {
    return $this->sName;
  }
 
  
  /*
   *  Metoda pobiera dane do wygenerowania raportu
   */
  function getReport() {
    
    $sSql = '
      SELECT 
      "0" as lp, 
      IF(P.ean_13 <> "", P.ean_13, P.isbn_plain) AS ISBN, 
      P.name AS Nazwa, 
      PP.name as Wydawca, 
      P.publication_year AS Wydanie,
          PS.profit_g_status AS "Stock Status", PS.profit_g_reservations AS "Stock Rezerwacje",
          PS.profit_e_status AS "Profit E Status",
          PS.profit_j_status AS "Profit E Status",
          IF(PS.azymut_status = "0", "0", PS.azymut_stock) AS "Stan Azymut", 
            PS.azymut_wholesale_price AS "Cena Z Azymut",
          IF(PS.ateneum_status = "0", "0", PS.ateneum_stock) AS "Stan Ateneum", 
            PS.ateneum_wholesale_price AS "Cena Z Ateneum",
          IF(PS.siodemka_status = "0", "0", PS.siodemka_stock) AS "Stan Siodemka", 
            PS.siodemka_wholesale_price AS "Cena Z Siodemka",
          IF(PS.olesiejuk_status = "0", "0", PS.olesiejuk_stock) AS "Stan Olesiejuk", 
            PS.olesiejuk_wholesale_price AS "Cena Z Olesiejuk",
          IF(PS.dictum_status = "0", "0", PS.dictum_stock) AS "Stan Dictum", 
            PS.dictum_wholesale_price AS "Cena Z Dictum",
          IF(PS.helion_status = "0", "0", PS.helion_stock) AS "Stan Helion", 
            PS.helion_wholesale_price AS "Cena Z Helion",
      @diff_price:=
      IF (`azymut_wholesale_price` > 0 && PS.azymut_status = "1", `azymut_wholesale_price`, 
          IF (`helion_wholesale_price` > 0 && PS.helion_status = "1", `helion_wholesale_price`, 
              IF (`dictum_wholesale_price` > 0 && PS.dictum_status = "1", `dictum_wholesale_price`, 
                  IF (`siodemka_wholesale_price` > 0 && PS.siodemka_status = "1", `siodemka_wholesale_price`, 
                      IF (`olesiejuk_wholesale_price` > 0 && PS.olesiejuk_status = "1", `olesiejuk_wholesale_price`, 
                          IF (`ateneum_wholesale_price` > 0 && PS.ateneum_status = "1", `ateneum_wholesale_price`, 
                              0
                          )
                      )
                  )
              )
          )
      ) AS diff_price
      ,
      ABS
      ((
      (    (
              (IF (azymut_wholesale_price > 0 && PS.azymut_status = "1", azymut_wholesale_price - @diff_price, 0)) +
              (IF (helion_wholesale_price > 0 && PS.helion_status = "1", helion_wholesale_price - @diff_price, 0)) +
              (IF (dictum_wholesale_price > 0 && PS.dictum_status = "1", dictum_wholesale_price - @diff_price, 0)) +
              (IF (siodemka_wholesale_price > 0 && PS.siodemka_status = "1", siodemka_wholesale_price - @diff_price, 0)) +
              (IF (olesiejuk_wholesale_price > 0 && PS.olesiejuk_status = "1", olesiejuk_wholesale_price - @diff_price, 0)) +
              (IF (ateneum_wholesale_price > 0 && PS.ateneum_status = "1", ateneum_wholesale_price - @diff_price, 0))
          ) * 100 ) / @diff_price
      ) ) AS Odchylenie

      FROM `products_stock` AS PS 
      JOIN products AS P
        ON P.id = PS.id
      JOIN products_publishers AS PP 
        ON P.publisher_id = PP.id
      WHERE 
       P.prod_status = "1"
      AND
      IF (`azymut_wholesale_price` > 0 && PS.azymut_status = "1", `azymut_wholesale_price`, 
          IF (`helion_wholesale_price` > 0 && PS.helion_status = "1", `helion_wholesale_price`, 
              IF (`dictum_wholesale_price` > 0 && PS.dictum_status = "1", `dictum_wholesale_price`, 
                  IF (`siodemka_wholesale_price` > 0 && PS.siodemka_status = "1", `siodemka_wholesale_price`, 
                      IF (`olesiejuk_wholesale_price` > 0 && PS.olesiejuk_status = "1", `olesiejuk_wholesale_price`, 
                          IF (`ateneum_wholesale_price` > 0 && PS.ateneum_status = "1", `ateneum_wholesale_price`, 
                              0
                          )
                      )
                  )
              )
          )
      ) > 0
      
      GROUP BY P.id
      ORDER BY Odchylenie DESC
      LIMIT 300';
    
      $this->aReport = $this->pDbMgr->GetAll('profit24', $sSql);
  }

  
  /*
   *  Metoda zapisuje raport do pliku CSV
   */    
  function saveReport() {
  
    $sCsv = "\xEF\xBB\xBF";

    if ($this->aReport[0]) {
      unset($this->aReport[0]['diff_price']);

      foreach ($this->aReport[0] as $sKey => $sValue) {
        $aHeaders[] = $sKey;
      }

      $sHeaders = implode(';', $aHeaders);
      $sCsv .= $sHeaders."\n";

      foreach ($this->aReport as $iKey => $aRecord) {
        unset($aRecord['diff_price']);
        $aRecord['lp'] = $iKey+1;

        $aRecord['Odchylenie'] = str_replace('.', ',', $aRecord['Odchylenie']);
        $aRecord['Cena Z Azymut'] = str_replace('.', ',', $aRecord['Cena Z Azymut']);
        $aRecord['Cena Z Ateneum'] = str_replace('.', ',', $aRecord['Cena Z Ateneum']);
        $aRecord['Cena Z Siodemka'] = str_replace('.', ',', $aRecord['Cena Z Siodemka']);
        $aRecord['Cena Z Olesiejuk'] = str_replace('.', ',', $aRecord['Cena Z Olesiejuk']);
        $aRecord['Cena Z Dictum'] = str_replace('.', ',', $aRecord['Cena Z Dictum']);
        $aRecord['Cena Z Helion'] = str_replace('.', ',', $aRecord['Cena Z Helion']);

        $sCsv .= implode(";", $aRecord)."\n";
      }

      $sFileName = date('Y-m-d').'.csv';
      file_put_contents($_SERVER['DOCUMENT_ROOT'].self::REPORTS_FOLDER.'/'.$sFileName, $sCsv);
    }
  }    
}