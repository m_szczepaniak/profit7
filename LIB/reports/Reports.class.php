<?php
/**
 * Klasa podstawowa generowania raportów
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace reports;
class Reports {

  function __construct() {
    
  }
  
  
  public function getReports() {
    $aClasses = glob($_SERVER['DOCUMENT_ROOT'].'/LIB/reports/auto/Report*.*');
    foreach ($aClasses as $sClassName) {
      $sClassName = str_replace('.class.php', '', $sClassName);
      $sClassName = str_replace($_SERVER['DOCUMENT_ROOT'].'/LIB/reports/auto/', '', $sClassName);
      
      $sFullClassName = '\reports\auto\\'.$sClassName;
      $oReport = new $sFullClassName;

      if ($oReport instanceof \reports\auto\interface_report) {
        echo $oReport->getName() . '<br />';
        echo  'jest instancją report_interface <br />';
        
        $oReport->getReport();
        $oReport->saveReport();
        
      } 
    }
  }
}