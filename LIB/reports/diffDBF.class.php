<?php
/*
$sFileName1 = DIRECTORY_SEPARATOR.'DBF'.DIRECTORY_SEPARATOR.'artykuly_1.dbf';
$sFileName2 = DIRECTORY_SEPARATOR.'DBF'.DIRECTORY_SEPARATOR.'artykuly_2.dbf';


error_reporting(E_ERROR | E_WARNING | E_PARSE); 
ini_set('display_errors','On');
header('Content-Type: text/html; charset=UTF-8');
header('Content-Disposition: attachment; filename="dbf_compare.xls"');
*/

/**
 * Klasa służy do porównywania plików DBF
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace reports;
class diffDBF {
  public $pDBF1;
  public $pDBF2;
  
  public static $aCols = array(
      'CERTYFOPIS', // isbn
      'NAZWA_ART', // tytul
      'CENA_AX', // cena brutto
      'CENA_A', // cena netto
      'STAWKA_VAT', // vat
      'CERTYFDATA', // data publikacji
      'LOKACJA', // lokalizacja
      'STAN_FAKT', // stan magazynowy
  );
  public $aAllRecords;

  function __construct($sFilename1, $sFilename2) {
    $this->sCurrPath = preg_replace('/(.*)\\'.DIRECTORY_SEPARATOR.'.*/', '$1', __FILE__);
    
    $this->pDBF1 = dbase_open($sFilename1, '0');
    if ($this->pDBF1 <= 0) {
      throw new Exception("Błąd nawiązywania połączenia z bazą 1");
    }

    $this->pDBF2 = dbase_open($sFilename2, '0');
    if ($this->pDBF2 <= 0) {
      throw new Exception("Błąd nawiązywania połączenia z bazą 2");
    }
  }// end of __construct()
  
  
  /**
   * Metoda dokonuje porównania DBF
   * 
   * @param int $pDBF plik DBF
   * @param string $sSource porównywane źródło
   */
  public function GetRecords($pDBF, $sSource) {
    $record_numbers = dbase_numrecords($pDBF);

    // lecimy po rekordach dbf'a
    for ($i = 1; $i <= $record_numbers; $i++) {
      $sErr = '';
      $aRecordTMP = dbase_get_record_with_names($pDBF, $i);
      foreach ($aRecordTMP as &$sCol) {
        // trimujemy wartości komorek tabeli
        $sCol = trim($sCol);
      }
      
      if (empty($aRecordTMP['CERTYFOPIS']) || empty($aRecordTMP['NAZWA_ART'])) {
        // puste elementy
        continue;
      }

      // sprawdzamy czy już istnieje dany element w tablicy
      if (!isset($this->aAllRecords[$sSource][$aRecordTMP['CERTYFOPIS']])) {
        $this->aAllRecords[$sSource][$aRecordTMP['CERTYFOPIS']]['source'] ++;
        $this->aAllRecords[$sSource][$aRecordTMP['CERTYFOPIS']] = $aRecordTMP;
      } else {
        $this->aAllRecords[$sSource][$aRecordTMP['CERTYFOPIS']]['source'] ++;
        $this->aAllRecords[$sSource][$aRecordTMP['CERTYFOPIS']]['STAN_FAKT'] = intval($this->aAllRecords[$sSource][$aRecordTMP['CERTYFOPIS']]['STAN_FAKT']) + intval($aRecordTMP['STAN_FAKT']);
      }
    }
  }// end of GetRecords() method
  
  
  /**
   * Metoda porównuje DBF'y
   * 
   * @param type $sSource1
   * @param type $sSource2
   */
  public function compare($sSource1, $sSource2) {
    $aRecords1 =& $this->aAllRecords[$sSource1];
    $aRecords2 =& $this->aAllRecords[$sSource2];
    $sLineHeader = "ISBN \t TYTUŁ \t BARKOD \t Stan 1 \t Stan 2"."\n";
    $sDiffDate = date('YmdHis');
    $sFilenamePath = $this->sCurrPath.'/files_diff_dbf/diff_'.$sDiffDate.'.xls';
    file_put_contents($sFilenamePath, $sLineHeader);
    
    // stany identyczne
    // stan większy
    // stan mniejszy
    // brak elementu w 1
    // brak elementu w 2
    foreach ($aRecords1 as $sCertyfopis => $aRecord) {
      $sLine = '';
      if (isset($aRecords2[$sCertyfopis]) && intval($aRecords2[$sCertyfopis]['STAN_FAKT']) == intval($aRecord['STAN_FAKT'])) {
        // stany identyczne - to nie wyświetlamy
        unset($aRecords1[$sCertyfopis]);
        unset($aRecords2[$sCertyfopis]);
      } elseif (isset($aRecords2[$sCertyfopis]) && intval($aRecords2[$sCertyfopis]['STAN_FAKT']) != intval($aRecord['STAN_FAKT'])) {
        // różne ilości, ale w obu źródłach istnieje
        $sLine .= "".$sCertyfopis."\t".$aRecord['NAZWA_ART']."\t".$aRecord['BARKOD']."\t".$aRecord['STAN_FAKT']."\t".$aRecords2[$sCertyfopis]['STAN_FAKT']."\n";
		unset($aRecords1[$sCertyfopis]);
		unset($aRecords2[$sCertyfopis]);
      } elseif (!isset($aRecords2[$sCertyfopis]) && intval($aRecord['STAN_FAKT']) == 0) {
        // brak elementu w 2, a w głównym ilość to 0
        unset($aRecords1[$sCertyfopis]);
        unset($aRecords2[$sCertyfopis]);
      } elseif (!isset($aRecords2[$sCertyfopis])) {
        // nie istnieje w źródle 2
        unset($aRecords1[$sCertyfopis]);
        $sLine .= "".$sCertyfopis."\t".$aRecord['NAZWA_ART']."\t".$aRecord['BARKOD']."\t".$aRecord['STAN_FAKT']."\t BRAK \n";
      } else {
        $sLine .= 'tu nie powinno nigdy wejść';
        die;
      }
      // zapis do pliku
      file_put_contents($sFilenamePath, $sLine, FILE_APPEND);
    }
    
    // to co pozostało w tabeli Records2 to co nie istnieje w Records1
    foreach ($aRecords2 as $sCertyfopis => $aRecord) {
      $sLine = '';
      if (intval($aRecord['STAN_FAKT']) > 0) {
        // jeśli jest więcej niż jeden
        $sLine .= "".$sCertyfopis."\t".$aRecord['NAZWA_ART']."\t".$aRecord['BARKOD']."\t"." BRAK \t".$aRecord['STAN_FAKT']." \n";
        
        // zapis do pliku
        file_put_contents($sFilenamePath, $sLine, FILE_APPEND);
        
        unset($aRecords2[$sCertyfopis]);
      }
    }
    return file_get_contents($sFilenamePath);
  }


  /**
   * Metoda magiczna destruktora
   */
  public function __destruct() {
    dbase_close($this->pDBF1);
    dbase_close($this->pDBF2);
  }
}


//try {
//  $oCompareDBF = new compareDBF($sFileName1,$sFileName2);  
//  $oCompareDBF->GetRecords($oCompareDBF->pDBF1, '1');
//  $oCompareDBF->GetRecords($oCompareDBF->pDBF2, '2');
//  $oCompareDBF->compare('1', '2');
//} catch (Exception $exc) {
//  echo $exc;
//}


