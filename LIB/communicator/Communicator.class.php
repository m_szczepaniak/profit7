<?php
/**
 * Klasa przeprowadza komunikację, za pomocą poleceń ze źródłami zewnętrznymi
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator;
class Communicator {
  
  private $sSource;
  
  private $sCMD;
  
  private $aParams;
  
  private $pObj;

  function __construct($sSource, $iOption, $pDbMgr = null, $bTestMode = true ) {
    
    include_once('sources/CommonSources.class.php');
    $this->sSource = $sSource;
    include_once('sources/'.$sSource.'/'.$sSource.'.class.php');
    
    switch($iOption) {
      case 1:
        $sClassName = 'communicator\sources\\'.$sSource.'\\'.$sSource;
        $this->pObj = new $sClassName($pDbMgr, $bTestMode);
      break;
      case 2: 
        $this->pObj = new $sSource();
      break;
    }
  }

  public function sendCMD($sCMD, $aParams) {
    
    $this->sCMD = $sCMD;
    $this->aParams = $aParams;
    $sMethod = '__'.$sCMD;
    return $this->pObj->$sMethod($aParams);
  }
}

?>