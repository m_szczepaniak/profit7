<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-20 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace communicator\sources;

use DatabaseManager;
use Module__zamowienia__sc_confirm_items;

/**
 * Description of matchFVtoPZ
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class matchFVtoPZ {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  const MATCH_PERCENT = 30;
  
  /**
   *
   * @var string
   */
  private $sPath;
  
  
  /**
   *
   * @var array
   */
  private $aEAN13;
  
  /**
   *
   * @var Module__zamowienia__sc_confirm_items 
   */
  private $oConfirmItems;
  
  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function __construct($pDbMgr) {
    global $aConfig;
    
    $this->pDbMgr = $pDbMgr;
    include_once($aConfig['common']['client_base_path'].'/'.$aConfig['common']['cms_dir'].'modules/m_zamowienia/Module_sc_confirm_items.class.php');
    $tmp = new \stdClass();
    $this->oConfirmItems = new Module__zamowienia__sc_confirm_items($tmp, true);
    $this->oConfirmItems->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   */
  public function proceedMatch($sPath, $iSourceId) {
    $this->sPath = $sPath;
    $oOrdersMagazineData = new \orders\OrderMagazineData($this->pDbMgr);
    
    $this->loadItemsOSHI($iSourceId);
    if (!empty($this->aEAN13)) {
      foreach ($this->aEAN13 as $iOSHId => $aEAN) {
        $sMatchedFile = $this->searchFile($aEAN);
        if ($sMatchedFile !== false && is_string($sMatchedFile)) {
          $sClassName = 'communicator\sources\\'.$this->sPath.'\FV_XML';
          $oFV = new $sClassName($sMatchedFile);

          $this->oConfirmItems->validateUpdateItems($iOSHId, $oFV->getProductsToStreamsoft());
          $sFVNumber = $oFV->getFVNumber();
          if ($sFVNumber != '' && $this->checkUnique($sFVNumber) == TRUE) {
            $oOrdersMagazineData->addSetHistorySendAttr($iOSHId, 'fv_nr', $sFVNumber);
          }
          @unlink($sMatchedFile);
        }
      }
    }
    return true;
  }
  
  /**
   * 
   * @param string $sFVUniqe
   * @return bool
   */
  private function checkUnique($sFVUniqe) {
    $sSql = 'SELECT fv_nr FROM orders_send_history_attributes WHERE fv_nr = "'.$sFVUniqe.'"';
    
    return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? FALSE : TRUE);
  }
  
  /**
   * 
   * @param array $aEAN
   */
  private function searchFile($aEAN) {
    
    $aItems = $this->getFiles();
    if (!empty($aItems) && is_array($aItems)) {
      foreach ($aItems as $sFileName) {
        $sClassName = 'communicator\sources\\'.$this->sPath.'\FV_XML';
        $oFV = new $sClassName($sFileName);
        $aXMLEan = $oFV->getEANCol();

        if ($this->matchItems($aXMLEan, $aEAN) === TRUE) {
          return $sFileName;
        }
      }
    }
    return false;
  }
  
  /**
   * 
   * @param array $aXMLEAN
   * @param array $aDBEAN
   */
  private function matchItems($aXMLEAN, $aDBEAN) {
    $iMatched = 0;
    foreach ($aDBEAN as $sEan) {
      if (array_search($sEan, $aXMLEAN) !== FALSE) {
        $iMatched++;
      }
    }
    $iCountXML = count($aXMLEAN);
    $iCountDB = count($aDBEAN);
    
    if ($iMatched > 0) {
      $iPercent = abs((($iCountXML*100) / $iMatched) - 100);
      $iPercentCount = abs((($iCountXML*100) / $iCountDB) - 100);
    } else {
      return false;
    }
    
    if ($iPercent <= self::MATCH_PERCENT && $iPercentCount <= self::MATCH_PERCENT ) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   */
  private function loadItemsOSHI($iSource) {
    $aOSHIds = $this->loadFileItem($iSource);
    foreach ($aOSHIds as $iOSHId) {
      $this->loadOSHI($iOSHId);
    }
  }
  
  /**
   * 
   * @param int $iOSHId
   */
  private function loadOSHI($iOSHId) {
    $this->aEAN13[$iOSHId] = $this->getIsbnItems($iOSHId);
  }
  
  /**
   * 
   * @param int $iOSHIdItem
   * @return array
   */
  private function getIsbnItems($iOSHIdItem) {
    
    $sSql = 'SELECT P.ean_13 
             FROM orders_send_history_items AS OSHI
             JOIN orders_items AS OI
              ON OI.id = OSHI.item_id
             JOIN products AS P
              ON OI.product_id = P.id
             WHERE send_history_id = '.$iOSHIdItem;
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }
  
  /**
   * 
   * @return array
   */
  private function loadFileItem($iSource) {
    
    $sSql = 'SELECT OSH.id
             FROM orders_send_history AS OSH
             LEFT JOIN orders_send_history_attributes AS OSHA
              ON OSHA.orders_send_history_id = OSH.id
             WHERE 
              OSH.magazine_status <> "5" AND
              OSH.source = "'.$iSource.'" AND
              (OSHA.fv_nr IS NULL OR OSH.independent_stock = "1") AND
              DATE(OSH.date_send) > SUBDATE(CURDATE(), INTERVAL 20 DAY)
             ';
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }

  /**
   * 
   * @return array
   */
  private function getFiles() {
    
    $sFilenamePath = __DIR__.'/'.$this->sPath.'/attachments/';
    $handle = opendir($sFilenamePath);
    while ($filename = readdir($handle)) {
      if (stristr($filename, '.xml') !== false) {
        $aMatched = array();
        preg_match('/(Fwz|FV)/', $filename, $aMatched);
        if ($aMatched[1] != '') {
          $aData[] = $sFilenamePath.$filename;
        }
      }
    }
    return $aData;
  }
}
