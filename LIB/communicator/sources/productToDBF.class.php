<?php
/**
 * Klasa pojedynczego towaru do utworzenia DBF'a
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources;
class productToDBF {
  public $indexDostawcy; // ind
  public $isbn;
  public $ean_13;//kod
  public $tytul;
  public $pkwiu;
  public $vat;
  public $cena_net;
  public $cena_det;
  public $ilosc;
}