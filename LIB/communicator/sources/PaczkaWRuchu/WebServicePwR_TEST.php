<?php
namespace communicator\sources\PaczkaWRuchu;
class GiveMeAllRUCHZipcode {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GiveMeAllRUCHZipcodeResponse {
  public $GiveMeAllRUCHZipcodeResult; // GiveMeAllRUCHZipcodeResult
}

class GiveMeAllRUCHZipcodeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMeAllRUCHLocation {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GiveMeAllRUCHLocationResponse {
  public $GiveMeAllRUCHLocationResult; // GiveMeAllRUCHLocationResult
}

class GiveMeAllRUCHLocationResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMeAllPSDLocation {
}

class GiveMeAllPSDLocationResponse {
  public $GiveMeAllPSDLocationResult; // GiveMeAllPSDLocationResult
}

class GiveMeAllPSDLocationResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMePSDProvince {
  public $Province; // string
}

class GiveMePSDProvinceResponse {
  public $GiveMePSDProvinceResult; // GiveMePSDProvinceResult
}

class GiveMePSDProvinceResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMePSDZipcode {
}

class GiveMePSDZipcodeResponse {
  public $GiveMePSDZipcodeResult; // GiveMePSDZipcodeResult
}

class GiveMePSDZipcodeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMeCityPSDLocation {
  public $City; // string
}

class GiveMeCityPSDLocationResponse {
  public $GiveMeCityPSDLocationResult; // GiveMeCityPSDLocationResult
}

class GiveMeCityPSDLocationResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMePackStatus {
  public $PhoneNumber; // string
  public $PackCode; // string
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GiveMePackStatusResponse {
  public $GiveMePackStatusResult; // GiveMePackStatusResult
}

class GiveMePackStatusResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMePackStatusRenamed {
  public $PhoneNumber; // string
  public $PackCode; // string
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GiveMePackStatusRenamedResponse {
  public $GiveMePackStatusRenamedResult; // GiveMePackStatusRenamedResult
}

class GiveMePackStatusRenamedResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateBusinessPack {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
}

class GenerateBusinessPackResponse {
  public $GenerateBusinessPackResult; // GenerateBusinessPackResult
}

class GenerateBusinessPackResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateBusinessPackTwo {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $AutoDestinationChange; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $CofirmMailing; // string
}

class GenerateBusinessPackTwoResponse {
  public $GenerateBusinessPackTwoResult; // GenerateBusinessPackTwoResult
}

class GenerateBusinessPackTwoResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class PutCustomerPackCanceled {
  public $PackCode; // string
  public $PartnerID; // string
  public $PartnerKey; // string
}

class PutCustomerPackCanceledResponse {
  public $PutCustomerPackCanceledResult; // PutCustomerPackCanceledResult
}

class PutCustomerPackCanceledResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class PutCustomerPack {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PackCode; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
}

class PutCustomerPackResponse {
  public $PutCustomerPackResult; // PutCustomerPackResult
}

class PutCustomerPackResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class PutCustomerPackTwo {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PackCode; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $AutoDestinationChange; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $CofirmMailing; // string
}

class PutCustomerPackTwoResponse {
  public $PutCustomerPackTwoResult; // PutCustomerPackTwoResult
}

class PutCustomerPackTwoResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateNumberWaybills {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $NumberOfWaybills; // string
}

class GenerateNumberWaybillsResponse {
  public $GenerateNumberWaybillsResult; // GenerateNumberWaybillsResult
}

class GenerateNumberWaybillsResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class CreateCustomer {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $Nip; // string
  public $City; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $EMail; // string
}

class CreateCustomerResponse {
  public $CreateCustomerResult; // CreateCustomerResult
}

class CreateCustomerResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GivePaymentType {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GivePaymentTypeResponse {
  public $GivePaymentTypeResult; // GivePaymentTypeResult
}

class GivePaymentTypeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GivePartnerStatus {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GivePartnerStatusResponse {
  public $GivePartnerStatusResult; // GivePartnerStatusResult
}

class GivePartnerStatusResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GivePartnerStatusTwo {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GivePartnerStatusTwoResponse {
  public $GivePartnerStatusTwoResult; // GivePartnerStatusTwoResult
}

class GivePartnerStatusTwoResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPackList {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $Format; // FormatPliku
  public $BusinessPackList; // ArrayOfBusinessPack
}

class FormatPliku {
  const PDF = 'PDF';
  const EPL = 'EPL';
}

class BusinessPack {
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $CofirmMailing; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
}

class GenerateLabelBusinessPackListResponse {
  public $GenerateLabelBusinessPackListResult; // GenerateLabelBusinessPackListResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackListResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPackListTwo {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $AutoDestinationChange; // string
  public $Format; // FormatPliku3
  public $BusinessPackList; // ArrayOfBusinessPack
}

class FormatPliku3 {
  const PDF = 'PDF';
  const EPL = 'EPL';
  const PDF10 = 'PDF10';
}

class GenerateLabelBusinessPackListTwoResponse {
  public $GenerateLabelBusinessPackListTwoResult; // GenerateLabelBusinessPackListTwoResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackListTwoResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPackListThree {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $AutoDestinationChange; // string
  public $Format; // FormatPliku3
  public $BusinessPackList; // ArrayOfBusinessPack
}

class GenerateLabelBusinessPackListThreeResponse {
  public $GenerateLabelBusinessPackListThreeResult; // GenerateLabelBusinessPackListThreeResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackListThreeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPack {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
}

class GenerateLabelBusinessPackResponse {
  public $GenerateLabelBusinessPackResult; // GenerateLabelBusinessPackResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPackTwo {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
  public $AutoDestinationChange; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $CofirmMailing; // string
}

class GenerateLabelBusinessPackTwoResponse {
  public $GenerateLabelBusinessPackTwoResult; // GenerateLabelBusinessPackTwoResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackTwoResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GetGeneratedParcels {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $dateFrom; // dateTime
  public $dateTo; // dateTime
}

class GetGeneratedParcelsResponse {
  public $GetGeneratedParcelsResult; // GetGeneratedParcelsResult
}

class GetGeneratedParcelsResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateProtocol {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $parcels; // ArrayOfUnsignedLong
}

class GenerateProtocolResponse {
  public $GenerateProtocolResult; // GenerateProtocolResult
  public $LabelData; // base64Binary
}

class GenerateProtocolResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class LabelPrintDuplicateList {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $Format; // FormatPliku
  public $PackCodeList; // ArrayOfString
}

class LabelPrintDuplicateListResponse {
  public $LabelPrintDuplicateListResult; // LabelPrintDuplicateListResult
  public $LabelData; // base64Binary
}

class LabelPrintDuplicateListResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class LabelPrintDuplicate {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PackCode; // string
}

class LabelPrintDuplicateResponse {
  public $LabelPrintDuplicateResult; // LabelBundle
}

class LabelBundle {
  public $Err; // string
  public $ErrDes; // string
  public $Label; // base64Binary
}

class GenerateShippingCode {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $AutoDestinationChange; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $CofirmMailing; // string
}

class GenerateShippingCodeResponse {
  public $GenerateShippingCodeResult; // GenerateShippingCodeResult
}

class GenerateShippingCodeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveShippingCodeStatus {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $Codes; // ArrayOfString
}

class GiveShippingCodeStatusResponse {
  public $GiveShippingCodeStatusResult; // GiveShippingCodeStatusResult
}

class GiveShippingCodeStatusResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateShippingCodeRuch2Home {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PhoneNumber; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $ConfirmMailing; // string
}

class GenerateShippingCodeRuch2HomeResponse {
  public $GenerateShippingCodeRuch2HomeResult; // GenerateShippingCodeRuch2HomeResult
}

class GenerateShippingCodeRuch2HomeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPackRuch2Home {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $ConfirmMailing; // string
}

class GenerateLabelBusinessPackRuch2HomeResponse {
  public $GenerateLabelBusinessPackRuch2HomeResult; // GenerateLabelBusinessPackRuch2HomeResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackRuch2HomeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateBusinessPackSignature {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
  public $Signature; // boolean
  public $SignatureInstrA1; // string
  public $SignatureInstrA2; // string
  public $SignatureB; // boolean
  public $SignatureInstrB1; // string
  public $SignatureInstrB2; // string
  public $SignatureC; // boolean
  public $SignatureInstrC1; // string
  public $SignatureInstrC2; // string
  public $SignatureD; // boolean
  public $SignatureInstrD1; // string
  public $SignatureInstrD2; // string
  public $SignaturePDFNumber; // string
  public $Signaturequality; // boolean
  public $WindowType; // boolean
  public $WindowText; // string
  public $AutoDestinationChange; // string
}

class GenerateBusinessPackSignatureResponse {
  public $GenerateBusinessPackSignatureResult; // GenerateBusinessPackSignatureResult
  public $LabelData; // base64Binary
}

class GenerateBusinessPackSignatureResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateBusinessPackSignatureV2 {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
  public $Signature; // boolean
  public $SignatureInstrA1; // string
  public $SignatureInstrA2; // string
  public $SignatureB; // boolean
  public $SignatureInstrB1; // string
  public $SignatureInstrB2; // string
  public $SignatureC; // boolean
  public $SignatureInstrC1; // string
  public $SignatureInstrC2; // string
  public $SignatureD; // boolean
  public $SignatureInstrD1; // string
  public $SignatureInstrD2; // string
  public $SignatureInstrD3; // string
  public $SignatureInstrD4; // string
  public $SignaturePDFNumber; // string
  public $Signaturequality; // boolean
  public $WindowType; // boolean
  public $WindowText; // string
  public $WindowTextVerification; // string
  public $AutoDestinationChange; // string
}

class GenerateBusinessPackSignatureV2Response {
  public $GenerateBusinessPackSignatureV2Result; // GenerateBusinessPackSignatureV2Result
  public $LabelData; // base64Binary
}

class GenerateBusinessPackSignatureV2Result {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class ManualSignature {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $SignaturePDFNumber; // string
  public $SignaturePDF; // base64Binary
}

class ManualSignatureResponse {
  public $ManualSignatureResult; // ManualSignatureResult
}

class ManualSignatureResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class SimOperatorsActive {
}

class SimOperatorsActiveResponse {
  public $SimOperatorsActiveResult; // SimOperatorsActiveResult
}

class SimOperatorsActiveResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateNumberWaybillsSignature {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $NumberOfWaybills; // string
}

class GenerateNumberWaybillsSignatureResponse {
  public $GenerateNumberWaybillsSignatureResult; // GenerateNumberWaybillsSignatureResult
}

class GenerateNumberWaybillsSignatureResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class PutCustomerPackSignature {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PackCode; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
  public $Signature; // boolean
  public $SignatureInstrA1; // string
  public $SignatureInstrA2; // string
  public $SignatureB; // boolean
  public $SignatureInstrB1; // string
  public $SignatureInstrB2; // string
  public $SignatureC; // boolean
  public $SignatureInstrC1; // string
  public $SignatureInstrC2; // string
  public $SignatureD; // boolean
  public $SignatureInstrD1; // string
  public $SignatureInstrD2; // string
  public $SignatureInstrD3; // string
  public $SignatureInstrD4; // string
  public $SignaturePDFNumber; // string
  public $Signaturequality; // boolean
  public $WindowType; // boolean
  public $WindowText; // string
  public $WindowTextVerification; // string
  public $AutoDestinationChange; // string
}

class PutCustomerPackSignatureResponse {
  public $PutCustomerPackSignatureResult; // PutCustomerPackSignatureResult
}

class PutCustomerPackSignatureResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}


/**
 * WebServicePwRTest class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class WebServicePwR_TEST extends \SoapClient {

  private static $classmap = array(
                                    'GiveMeAllRUCHZipcode' => 'GiveMeAllRUCHZipcode',
                                    'GiveMeAllRUCHZipcodeResponse' => 'GiveMeAllRUCHZipcodeResponse',
                                    'GiveMeAllRUCHZipcodeResult' => 'GiveMeAllRUCHZipcodeResult',
                                    'GiveMeAllRUCHLocation' => 'GiveMeAllRUCHLocation',
                                    'GiveMeAllRUCHLocationResponse' => 'GiveMeAllRUCHLocationResponse',
                                    'GiveMeAllRUCHLocationResult' => 'GiveMeAllRUCHLocationResult',
                                    'GiveMeAllPSDLocation' => 'GiveMeAllPSDLocation',
                                    'GiveMeAllPSDLocationResponse' => 'GiveMeAllPSDLocationResponse',
                                    'GiveMeAllPSDLocationResult' => 'GiveMeAllPSDLocationResult',
                                    'GiveMePSDProvince' => 'GiveMePSDProvince',
                                    'GiveMePSDProvinceResponse' => 'GiveMePSDProvinceResponse',
                                    'GiveMePSDProvinceResult' => 'GiveMePSDProvinceResult',
                                    'GiveMePSDZipcode' => 'GiveMePSDZipcode',
                                    'GiveMePSDZipcodeResponse' => 'GiveMePSDZipcodeResponse',
                                    'GiveMePSDZipcodeResult' => 'GiveMePSDZipcodeResult',
                                    'GiveMeCityPSDLocation' => 'GiveMeCityPSDLocation',
                                    'GiveMeCityPSDLocationResponse' => 'GiveMeCityPSDLocationResponse',
                                    'GiveMeCityPSDLocationResult' => 'GiveMeCityPSDLocationResult',
                                    'GiveMePackStatus' => 'GiveMePackStatus',
                                    'GiveMePackStatusResponse' => 'GiveMePackStatusResponse',
                                    'GiveMePackStatusResult' => 'GiveMePackStatusResult',
                                    'GiveMePackStatusRenamed' => 'GiveMePackStatusRenamed',
                                    'GiveMePackStatusRenamedResponse' => 'GiveMePackStatusRenamedResponse',
                                    'GiveMePackStatusRenamedResult' => 'GiveMePackStatusRenamedResult',
                                    'GenerateBusinessPack' => 'GenerateBusinessPack',
                                    'GenerateBusinessPackResponse' => 'GenerateBusinessPackResponse',
                                    'GenerateBusinessPackResult' => 'GenerateBusinessPackResult',
                                    'GenerateBusinessPackTwo' => 'GenerateBusinessPackTwo',
                                    'GenerateBusinessPackTwoResponse' => 'GenerateBusinessPackTwoResponse',
                                    'GenerateBusinessPackTwoResult' => 'GenerateBusinessPackTwoResult',
                                    'PutCustomerPackCanceled' => 'PutCustomerPackCanceled',
                                    'PutCustomerPackCanceledResponse' => 'PutCustomerPackCanceledResponse',
                                    'PutCustomerPackCanceledResult' => 'PutCustomerPackCanceledResult',
                                    'PutCustomerPack' => 'PutCustomerPack',
                                    'PutCustomerPackResponse' => 'PutCustomerPackResponse',
                                    'PutCustomerPackResult' => 'PutCustomerPackResult',
                                    'PutCustomerPackTwo' => 'PutCustomerPackTwo',
                                    'PutCustomerPackTwoResponse' => 'PutCustomerPackTwoResponse',
                                    'PutCustomerPackTwoResult' => 'PutCustomerPackTwoResult',
                                    'GenerateNumberWaybills' => 'GenerateNumberWaybills',
                                    'GenerateNumberWaybillsResponse' => 'GenerateNumberWaybillsResponse',
                                    'GenerateNumberWaybillsResult' => 'GenerateNumberWaybillsResult',
                                    'CreateCustomer' => 'CreateCustomer',
                                    'CreateCustomerResponse' => 'CreateCustomerResponse',
                                    'CreateCustomerResult' => 'CreateCustomerResult',
                                    'GivePaymentType' => 'GivePaymentType',
                                    'GivePaymentTypeResponse' => 'GivePaymentTypeResponse',
                                    'GivePaymentTypeResult' => 'GivePaymentTypeResult',
                                    'GivePartnerStatus' => 'GivePartnerStatus',
                                    'GivePartnerStatusResponse' => 'GivePartnerStatusResponse',
                                    'GivePartnerStatusResult' => 'GivePartnerStatusResult',
                                    'GivePartnerStatusTwo' => 'GivePartnerStatusTwo',
                                    'GivePartnerStatusTwoResponse' => 'GivePartnerStatusTwoResponse',
                                    'GivePartnerStatusTwoResult' => 'GivePartnerStatusTwoResult',
                                    'GenerateLabelBusinessPackList' => 'GenerateLabelBusinessPackList',
                                    'FormatPliku' => 'FormatPliku',
                                    'BusinessPack' => 'BusinessPack',
                                    'GenerateLabelBusinessPackListResponse' => 'GenerateLabelBusinessPackListResponse',
                                    'GenerateLabelBusinessPackListResult' => 'GenerateLabelBusinessPackListResult',
                                    'GenerateLabelBusinessPackListTwo' => 'GenerateLabelBusinessPackListTwo',
                                    'FormatPliku3' => 'FormatPliku3',
                                    'GenerateLabelBusinessPackListTwoResponse' => 'GenerateLabelBusinessPackListTwoResponse',
                                    'GenerateLabelBusinessPackListTwoResult' => 'GenerateLabelBusinessPackListTwoResult',
                                    'GenerateLabelBusinessPackListThree' => 'GenerateLabelBusinessPackListThree',
                                    'GenerateLabelBusinessPackListThreeResponse' => 'GenerateLabelBusinessPackListThreeResponse',
                                    'GenerateLabelBusinessPackListThreeResult' => 'GenerateLabelBusinessPackListThreeResult',
                                    'GenerateLabelBusinessPack' => 'GenerateLabelBusinessPack',
                                    'GenerateLabelBusinessPackResponse' => 'GenerateLabelBusinessPackResponse',
                                    'GenerateLabelBusinessPackResult' => 'GenerateLabelBusinessPackResult',
                                    'GenerateLabelBusinessPackTwo' => 'GenerateLabelBusinessPackTwo',
                                    'GenerateLabelBusinessPackTwoResponse' => 'GenerateLabelBusinessPackTwoResponse',
                                    'GenerateLabelBusinessPackTwoResult' => 'GenerateLabelBusinessPackTwoResult',
                                    'GetGeneratedParcels' => 'GetGeneratedParcels',
                                    'GetGeneratedParcelsResponse' => 'GetGeneratedParcelsResponse',
                                    'GetGeneratedParcelsResult' => 'GetGeneratedParcelsResult',
                                    'GenerateProtocol' => 'GenerateProtocol',
                                    'GenerateProtocolResponse' => 'GenerateProtocolResponse',
                                    'GenerateProtocolResult' => 'GenerateProtocolResult',
                                    'LabelPrintDuplicateList' => 'LabelPrintDuplicateList',
                                    'LabelPrintDuplicateListResponse' => 'LabelPrintDuplicateListResponse',
                                    'LabelPrintDuplicateListResult' => 'LabelPrintDuplicateListResult',
                                    'LabelPrintDuplicate' => 'LabelPrintDuplicate',
                                    'LabelPrintDuplicateResponse' => 'LabelPrintDuplicateResponse',
                                    'LabelBundle' => 'LabelBundle',
                                    'GenerateShippingCode' => 'GenerateShippingCode',
                                    'GenerateShippingCodeResponse' => 'GenerateShippingCodeResponse',
                                    'GenerateShippingCodeResult' => 'GenerateShippingCodeResult',
                                    'GiveShippingCodeStatus' => 'GiveShippingCodeStatus',
                                    'GiveShippingCodeStatusResponse' => 'GiveShippingCodeStatusResponse',
                                    'GiveShippingCodeStatusResult' => 'GiveShippingCodeStatusResult',
                                    'GenerateShippingCodeRuch2Home' => 'GenerateShippingCodeRuch2Home',
                                    'GenerateShippingCodeRuch2HomeResponse' => 'GenerateShippingCodeRuch2HomeResponse',
                                    'GenerateShippingCodeRuch2HomeResult' => 'GenerateShippingCodeRuch2HomeResult',
                                    'GenerateLabelBusinessPackRuch2Home' => 'GenerateLabelBusinessPackRuch2Home',
                                    'GenerateLabelBusinessPackRuch2HomeResponse' => 'GenerateLabelBusinessPackRuch2HomeResponse',
                                    'GenerateLabelBusinessPackRuch2HomeResult' => 'GenerateLabelBusinessPackRuch2HomeResult',
                                    'GenerateBusinessPackSignature' => 'GenerateBusinessPackSignature',
                                    'GenerateBusinessPackSignatureResponse' => 'GenerateBusinessPackSignatureResponse',
                                    'GenerateBusinessPackSignatureResult' => 'GenerateBusinessPackSignatureResult',
                                    'GenerateBusinessPackSignatureV2' => 'GenerateBusinessPackSignatureV2',
                                    'GenerateBusinessPackSignatureV2Response' => 'GenerateBusinessPackSignatureV2Response',
                                    'GenerateBusinessPackSignatureV2Result' => 'GenerateBusinessPackSignatureV2Result',
                                    'ManualSignature' => 'ManualSignature',
                                    'ManualSignatureResponse' => 'ManualSignatureResponse',
                                    'ManualSignatureResult' => 'ManualSignatureResult',
                                    'SimOperatorsActive' => 'SimOperatorsActive',
                                    'SimOperatorsActiveResponse' => 'SimOperatorsActiveResponse',
                                    'SimOperatorsActiveResult' => 'SimOperatorsActiveResult',
                                    'GenerateNumberWaybillsSignature' => 'GenerateNumberWaybillsSignature',
                                    'GenerateNumberWaybillsSignatureResponse' => 'GenerateNumberWaybillsSignatureResponse',
                                    'GenerateNumberWaybillsSignatureResult' => 'GenerateNumberWaybillsSignatureResult',
                                    'PutCustomerPackSignature' => 'PutCustomerPackSignature',
                                    'PutCustomerPackSignatureResponse' => 'PutCustomerPackSignatureResponse',
                                    'PutCustomerPackSignatureResult' => 'PutCustomerPackSignatureResult',
                                   );

  public function WebServicePwRTest($wsdl = "https://91.242.220.103/WebServicePwR/WebServicePwRTest.asmx?wsdl", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *  
   *
   * @param GiveMeAllRUCHZipcode $parameters
   * @return GiveMeAllRUCHZipcodeResponse
   */
  public function GiveMeAllRUCHZipcode(GiveMeAllRUCHZipcode $parameters) {
    return $this->__soapCall('GiveMeAllRUCHZipcode', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMeAllRUCHLocation $parameters
   * @return GiveMeAllRUCHLocationResponse
   */
  public function GiveMeAllRUCHLocation(GiveMeAllRUCHLocation $parameters) {
    return $this->__soapCall('GiveMeAllRUCHLocation', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMeAllPSDLocation $parameters
   * @return GiveMeAllPSDLocationResponse
   */
  public function GiveMeAllPSDLocation(GiveMeAllPSDLocation $parameters) {
    return $this->__soapCall('GiveMeAllPSDLocation', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMePSDProvince $parameters
   * @return GiveMePSDProvinceResponse
   */
  public function GiveMePSDProvince(GiveMePSDProvince $parameters) {
    return $this->__soapCall('GiveMePSDProvince', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMePSDZipcode $parameters
   * @return GiveMePSDZipcodeResponse
   */
  public function GiveMePSDZipcode(GiveMePSDZipcode $parameters) {
    return $this->__soapCall('GiveMePSDZipcode', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMeCityPSDLocation $parameters
   * @return GiveMeCityPSDLocationResponse
   */
  public function GiveMeCityPSDLocation(GiveMeCityPSDLocation $parameters) {
    return $this->__soapCall('GiveMeCityPSDLocation', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMePackStatus $parameters
   * @return GiveMePackStatusResponse
   */
  public function GiveMePackStatus(GiveMePackStatus $parameters) {
    return $this->__soapCall('GiveMePackStatus', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMePackStatusRenamed $parameters
   * @return GiveMePackStatusRenamedResponse
   */
  public function GiveMePackStatusRenamed(GiveMePackStatusRenamed $parameters) {
    return $this->__soapCall('GiveMePackStatusRenamed', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateBusinessPack $parameters
   * @return GenerateBusinessPackResponse
   */
  public function GenerateBusinessPack(GenerateBusinessPack $parameters) {
    return $this->__soapCall('GenerateBusinessPack', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateBusinessPackTwo $parameters
   * @return GenerateBusinessPackTwoResponse
   */
  public function GenerateBusinessPackTwo(GenerateBusinessPackTwo $parameters) {
    return $this->__soapCall('GenerateBusinessPackTwo', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param PutCustomerPackCanceled $parameters
   * @return PutCustomerPackCanceledResponse
   */
  public function PutCustomerPackCanceled(PutCustomerPackCanceled $parameters) {
    return $this->__soapCall('PutCustomerPackCanceled', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param PutCustomerPack $parameters
   * @return PutCustomerPackResponse
   */
  public function PutCustomerPack(PutCustomerPack $parameters) {
    return $this->__soapCall('PutCustomerPack', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param PutCustomerPackTwo $parameters
   * @return PutCustomerPackTwoResponse
   */
  public function PutCustomerPackTwo(PutCustomerPackTwo $parameters) {
    return $this->__soapCall('PutCustomerPackTwo', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateNumberWaybills $parameters
   * @return GenerateNumberWaybillsResponse
   */
  public function GenerateNumberWaybills(GenerateNumberWaybills $parameters) {
    return $this->__soapCall('GenerateNumberWaybills', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CreateCustomer $parameters
   * @return CreateCustomerResponse
   */
  public function CreateCustomer(CreateCustomer $parameters) {
    return $this->__soapCall('CreateCustomer', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GivePaymentType $parameters
   * @return GivePaymentTypeResponse
   */
  public function GivePaymentType(GivePaymentType $parameters) {
    return $this->__soapCall('GivePaymentType', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GivePartnerStatus $parameters
   * @return GivePartnerStatusResponse
   */
  public function GivePartnerStatus(GivePartnerStatus $parameters) {
    return $this->__soapCall('GivePartnerStatus', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GivePartnerStatusTwo $parameters
   * @return GivePartnerStatusTwoResponse
   */
  public function GivePartnerStatusTwo(GivePartnerStatusTwo $parameters) {
    return $this->__soapCall('GivePartnerStatusTwo', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPackList $parameters
   * @return GenerateLabelBusinessPackListResponse
   */
  public function GenerateLabelBusinessPackList(GenerateLabelBusinessPackList $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPackList', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPackListTwo $parameters
   * @return GenerateLabelBusinessPackListTwoResponse
   */
  public function GenerateLabelBusinessPackListTwo(GenerateLabelBusinessPackListTwo $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPackListTwo', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPackListThree $parameters
   * @return GenerateLabelBusinessPackListThreeResponse
   */
  public function GenerateLabelBusinessPackListThree(GenerateLabelBusinessPackListThree $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPackListThree', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPack $parameters
   * @return GenerateLabelBusinessPackResponse
   */
  public function GenerateLabelBusinessPack(GenerateLabelBusinessPack $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPack', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPackTwo $parameters
   * @return GenerateLabelBusinessPackTwoResponse
   */
  public function GenerateLabelBusinessPackTwo(GenerateLabelBusinessPackTwo $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPackTwo', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetGeneratedParcels $parameters
   * @return GetGeneratedParcelsResponse
   */
  public function GetGeneratedParcels(GetGeneratedParcels $parameters) {
    return $this->__soapCall('GetGeneratedParcels', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateProtocol $parameters
   * @return GenerateProtocolResponse
   */
  public function GenerateProtocol(GenerateProtocol $parameters) {
    return $this->__soapCall('GenerateProtocol', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param LabelPrintDuplicateList $parameters
   * @return LabelPrintDuplicateListResponse
   */
  public function LabelPrintDuplicateList(LabelPrintDuplicateList $parameters) {
    return $this->__soapCall('LabelPrintDuplicateList', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param LabelPrintDuplicate $parameters
   * @return LabelPrintDuplicateResponse
   */
  public function LabelPrintDuplicate(LabelPrintDuplicate $parameters) {
    return $this->__soapCall('LabelPrintDuplicate', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateShippingCode $parameters
   * @return GenerateShippingCodeResponse
   */
  public function GenerateShippingCode(GenerateShippingCode $parameters) {
    return $this->__soapCall('GenerateShippingCode', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveShippingCodeStatus $parameters
   * @return GiveShippingCodeStatusResponse
   */
  public function GiveShippingCodeStatus(GiveShippingCodeStatus $parameters) {
    return $this->__soapCall('GiveShippingCodeStatus', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateShippingCodeRuch2Home $parameters
   * @return GenerateShippingCodeRuch2HomeResponse
   */
  public function GenerateShippingCodeRuch2Home(GenerateShippingCodeRuch2Home $parameters) {
    return $this->__soapCall('GenerateShippingCodeRuch2Home', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPackRuch2Home $parameters
   * @return GenerateLabelBusinessPackRuch2HomeResponse
   */
  public function GenerateLabelBusinessPackRuch2Home(GenerateLabelBusinessPackRuch2Home $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPackRuch2Home', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateBusinessPackSignature $parameters
   * @return GenerateBusinessPackSignatureResponse
   */
  public function GenerateBusinessPackSignature(GenerateBusinessPackSignature $parameters) {
    return $this->__soapCall('GenerateBusinessPackSignature', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateBusinessPackSignatureV2 $parameters
   * @return GenerateBusinessPackSignatureV2Response
   */
  public function GenerateBusinessPackSignatureV2(GenerateBusinessPackSignatureV2 $parameters) {
    return $this->__soapCall('GenerateBusinessPackSignatureV2', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param ManualSignature $parameters
   * @return ManualSignatureResponse
   */
  public function ManualSignature(ManualSignature $parameters) {
    return $this->__soapCall('ManualSignature', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param SimOperatorsActive $parameters
   * @return SimOperatorsActiveResponse
   */
  public function SimOperatorsActive(SimOperatorsActive $parameters) {
    return $this->__soapCall('SimOperatorsActive', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateNumberWaybillsSignature $parameters
   * @return GenerateNumberWaybillsSignatureResponse
   */
  public function GenerateNumberWaybillsSignature(GenerateNumberWaybillsSignature $parameters) {
    return $this->__soapCall('GenerateNumberWaybillsSignature', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param PutCustomerPackSignature $parameters
   * @return PutCustomerPackSignatureResponse
   */
  public function PutCustomerPackSignature(PutCustomerPackSignature $parameters) {
    return $this->__soapCall('PutCustomerPackSignature', array($parameters),       array(
            'uri' => 'https://api-test.paczkawruchu.pl/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

}

?>
