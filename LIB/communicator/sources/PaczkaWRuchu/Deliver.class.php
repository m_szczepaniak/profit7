<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\PaczkaWRuchu;
class Deliver extends \orders\Shipment\CommonDeliver implements \communicator\sources\iDeliver {
  
  public $aTransportSettings;
  public $bTestMode;
  public $pDbMgr;
  
  /**
   *
   * @var ElektronicznyNadawca $oEN
   */
  public $en;
  
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    parent::__construct($aTransportSettings, $pDbMgr, $bTestMode);
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }
  
  public function getShipmentList($aTransportNumbers, $aOrdersIds = array()) {
    $sPDF = $this->getPDFDeliver($aTransportNumbers);
    
    if ($sPDF != '' && is_array($sPDF) === false) {
      $sDeliverFilename = $this->saveDeliverGetFileNamePathTRANSACTION($sPDF, $aOrdersIds);
    } else {
      throw new \Exception(_('Wystąpił błąd podczas pobierania delivera z '.$this->aTransportSettings['name']));
    }
    return $sDeliverFilename;
  }
  
  /**
   * 
   * @param array $aTransportNumbers
   * @return string
   */
  private function getHTMLDeliver($aTransportNumbers) {
    $sDateFull = date('d.m.Y H:i:s');
    $sHTML ='
      <table style="width:100%; height:100%;">
        <tr>
          <td>Potwierdzenie nadania paczek: Paczka w Ruchu<br />
          Profit M Spółka z ograniczoną odpowiedzialnością<br />
          Pl. Defilad 1, Metro Centrum, lok.2002D<br />
          00-110 Warszawa 1 <br />
          525-22-45-459 <br />
          </td>
          <td>Warszawa<br />Dnia: '.$sDateFull.'</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td style="font-size: 60px;">Ilość paczek: '.count($aTransportNumbers).'</td>
          <td></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Podpis kuriera</td>
          <td>Podpis nadawcy</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>..............................</td>
          <td>'.$_SESSION['user']['author'].'</td>
        </tr>
        <tr>
          <td></td>
          <td>..............................</td>
        </tr>
      </table>
      <br /><br /><br /><br />
      
';
    $sHTML .= implode(',  ', $aTransportNumbers);
    return $sHTML;
  }
  
  /**
   * 
   * @param array $aTransportNumbers
   * @return string
   */
  private function getPDFDeliver($aTransportNumbers) {
    $sDateFull = date('YmdHis');
    $sHTMLDeliver = $this->getHTMLDeliver($aTransportNumbers);
    
    require_once('tcpdf/config/lang/pl.php');
		require_once('tcpdf/tcpdf.php');		

		// create new PDF document
		$pdf = new \TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('');
		$pdf->SetTitle(_('Deliver '.$sDateFull));
		$pdf->SetSubject(_('Deliver '.$sDateFull));
		$pdf->SetKeywords('');
		$pdf->setPrintHeader(false);
		$pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));
    

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
		//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 10);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO); 
		
		//set some language-dependent strings
		$pdf->setLanguageArray($l); 
		
		// set font
		$pdf->SetFont('freesans', '', 8);
		
		$pdf->AddPage();
		$pdf->writeHTML($sHTMLDeliver, true, false, false, false, '');
		
		// nazwa pliku - uwzgledniajaca daty
		$sFileName = 'deliver_ruch_'.$sDateFull.'.pdf';
		return $pdf->Output($sFileName, 'S');
  }
}
