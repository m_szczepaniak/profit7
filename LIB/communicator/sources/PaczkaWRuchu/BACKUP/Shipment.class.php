<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\PaczkaWRuchu;
/**
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Shipment implements \communicator\sources\iShipment {

  private $bTestMode;

  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var WebServicePwRTest
   */
  protected $PwR;
  private $aTransportSettings;

  CONST WARTOSC_UBEZPIECZENIA_PRZESYLKI = 30000;

  /**
   *
   * @param array $aTransportSettings
   * @param \DatabaseManager $pDbMgr
   * @param bool $bTestMode
   */
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;

    $this->init();
  }

  /**
   *
   * @param int $iWebsiteId
   */
  private function init() {

    if (is_null($this->PwR)) {
      $options = array();
      $options["login"] = $this->aTransportSettings['login'];
      $options["password"] = $this->aTransportSettings['password'];
      $options["trace"] = 1;
      $options['connection_timeout'] = 10;

      if ($this->bTestMode === TRUE) {
        $this->PwR = new WebServicePwR_TEST($this->aTransportSettings['api_url'], $options);
      } else {
        $this->PwR = new WebServicePwR($this->aTransportSettings['api_url'], $options);
      }
    }
  }

  /**
   *
   * @param array $aOrder
   * @param array $aDeliverAddress
   * @param array $aWebsiteSettings
   * @return string transport number
   * @throws \Exception
   */
  public function addShipment($aOrder, $aDeliverAddress, $aWebsiteSettings) {
    global $aConfig;
    $aOrder['weight'] = round($aOrder['weight']*1000);

    $GenerateLabelBusinessPack = new GenerateLabelBusinessPack();
    $GenerateLabelBusinessPack->PartnerID = $this->aTransportSettings['login'];
    $GenerateLabelBusinessPack->PartnerKey = $this->aTransportSettings['password'];
    $GenerateLabelBusinessPack->DestinationCode = $this->getPOPeR($aOrder['point_of_receipt']);
    $GenerateLabelBusinessPack->PackValue = self::WARTOSC_UBEZPIECZENIA_PRZESYLKI;
    $GenerateLabelBusinessPack->BoxSize = 'MINI';

    if ($this->checkIsPostalFee($aOrder)) {
      $GenerateLabelBusinessPack->CashOnDelivery = true;
      $GenerateLabelBusinessPack->AmountCashOnDelivery = round($this->getPobranie($aOrder) * 100);// kwota w groszach
      $GenerateLabelBusinessPack->TransferDescription = $aOrder['order_number'];
    } else {
      $GenerateLabelBusinessPack->CashOnDelivery = false;
    }

    $GenerateLabelBusinessPack->Insurance = true;// Ubezpieczenie

    $GenerateLabelBusinessPack->EMail = $aOrder['email'];
    $GenerateLabelBusinessPack->PhoneNumber = $aDeliverAddress['phone'];
    $GenerateLabelBusinessPack->FirstName = $aDeliverAddress['name'];
    $GenerateLabelBusinessPack->LastName = $aDeliverAddress['surname'];
    $GenerateLabelBusinessPack->CompanyName = $aDeliverAddress['company'];
    $GenerateLabelBusinessPack->StreetName = $aDeliverAddress['street'];
    $GenerateLabelBusinessPack->BuildingNumber = $aDeliverAddress['number'];
    $GenerateLabelBusinessPack->FlatNumber = $aDeliverAddress['number2'];
    $GenerateLabelBusinessPack->City = $aDeliverAddress['city'];
    $GenerateLabelBusinessPack->PostCode = $aDeliverAddress['postal'];


//    $GenerateLabelBusinessPack->SenderEMail = $aWebsiteSettings['email'];
    $GenerateLabelBusinessPack->SenderEMail = $this->detectSenderMail($aOrder);
    $GenerateLabelBusinessPack->SenderCompanyName = $aWebsiteSettings['paczkomaty_name'];
    $GenerateLabelBusinessPack->SenderStreetName = $this->aTransportSettings['street'];
    $GenerateLabelBusinessPack->SenderBuildingNumber = $this->aTransportSettings['number'];
    $GenerateLabelBusinessPack->SenderFlatNumber = $this->aTransportSettings['number2'];
    $GenerateLabelBusinessPack->SenderCity = $this->aTransportSettings['city'];
    $GenerateLabelBusinessPack->SenderPostCode = $this->aTransportSettings['postal'];
    $GenerateLabelBusinessPack->SenderPhoneNumber = $aWebsiteSettings['phone'];
    $GenerateLabelBusinessPack->SenderOrders = (isset($aOrder['orders_numbers']) && $aOrder['orders_numbers'] != '' ? $aOrder['orders_numbers'] : $aOrder['order_number']);
    $GenerateLabelBusinessPack->PrintAdress = '1';
    $GenerateLabelBusinessPack->PrintType = '1';


    $GenerateLabelBusinessPackResponse = new GenerateLabelBusinessPackResponse();
    $GenerateLabelBusinessPackResponse->GenerateLabelBusinessPackResult = new GenerateLabelBusinessPackResult();
    $GenerateLabelBusinessPackResponse->GenerateLabelBusinessPackResult->any = new GenerateLabelBusinessPack();

    $GenerateLabelBusinessPackResponse = $this->PwR->GenerateLabelBusinessPack($GenerateLabelBusinessPack);
    $sTransportNumber = $this->getPaczkaWRuchuTransportNumber($GenerateLabelBusinessPackResponse->GenerateLabelBusinessPackResult->any);
    if ($sTransportNumber != '') {
      $sFilePatch = $this->getTransportListFilename($aConfig, $sTransportNumber);
      file_put_contents($sFilePatch, $GenerateLabelBusinessPackResponse->LabelData);
      return $sTransportNumber;
    }
    else {
      throw new \Exception($this->getErrorDescription($GenerateLabelBusinessPackResponse->GenerateLabelBusinessPackResult->any));
    }
  }

  private function detectSenderMail($aOrder)
  {
    $default = 'kontakt@profit24.pl';

    $sSql = 'SELECT code FROM websites WHERE id = '.$aOrder['website_id'];
    $sCode = $this->pDbMgr->GetOne('profit24', $sSql);

    $sSql = 'SELECT email FROM website_settings LIMIT 1';
    $sEmail = $this->pDbMgr->GetOne($sCode, $sSql);
    if ($sEmail != '') {
      return $sEmail;
    }
    return $default;
  }

  /**
   *
   * @param array $aOrder
   * @return boolean
   */
  private function checkIsPostalFee($aOrder) {

    if ( ($aOrder['payment_type'] == 'postal_fee' || $aOrder['second_payment_type'] == 'postal_fee') &&
        ($aOrder['to_pay'] - $aOrder['paid_amount']) > 0) {
      return true;
    } else {
      return false;
    }
  }// end of checkIsPostalFee() method

  /**
   *
   * @return float
   */
  private function getPobranie($aOrder) {

    return $aOrder['to_pay']-$aOrder['paid_amount'];
  }

  /**
   *
   * @param string $sXMLData
   * @return string
   */
  private function getErrorDescription($sXMLData) {

    $aMatches = array();
    preg_match('/\<ErrDes\>([^\<]+)\<\/ErrDes\>/', $sXMLData, $aMatches);
    return $aMatches[1];
  }// end of getPaczkaWRuchuTransportNumber() method

  public function doCancelShipment() {

  }

  /**
   *
   * @global \DatabaseManager $pDbMgr
   * @param string $sFilePath
   * @param string $sPrinter
   * @return boolean
   */
  public function doPrintAddressLabel($sFilePath, $sPrinter) {
    global $pDbMgr;

    $aValues = array(
        'filetype' => 'PDF',
        'filepath' => $sFilePath,
        'dest_printer' => $sPrinter,
        'created' => 'NOW()'
    );
    if ($pDbMgr->Insert('profit24', 'print_queue', $aValues) === false) {
      return false;
    }
    return true;
  }

  /**
   *
   * @param string $sFilePath
   * @param int $iOrderId
   * @return string
   */
  public function getAddressLabel($sFilePath, $iOrderId) {
    return $sFilePath; // ;-) [:->
  }

  /**
   *
   * @param int $iId
   * @param string $sTransportNumber
   * @return array
   */
  public function getDeliveryStatus($iId, $sTransportNumber) {

    $GiveMePackStatus = new GiveMePackStatus();
    $GiveMePackStatus->PartnerID = $this->aTransportSettings['login'];
    $GiveMePackStatus->PartnerKey = $this->aTransportSettings['password'];
    $GiveMePackStatus->PackCode = $sTransportNumber;
    $GiveMePackStatusResponse = new GiveMePackStatusResponse();
    $GiveMePackStatusResponse->GiveMePackStatusResult = new GiveMePackStatusResult();
    $GiveMePackStatusResponse = $this->PwR->GiveMePackStatus($GiveMePackStatus);
    $GiveMePackStatusResult = simplexml_load_string($GiveMePackStatusResponse->GiveMePackStatusResult->any);

    $aStatusesCollection = array();
    foreach ($GiveMePackStatusResult->NewDataSet->PackStatus as $oOrderStatus) {

      if (isset($oOrderStatus->Trans_Des) && isset($oOrderStatus->Data)) {
        $oDeliveryShipment = new \orders\Shipment\DeliveryShipment();
        $oDeliveryShipment->sStatusName = $oOrderStatus->Trans_Des;
        $oDeliveryShipment->sStatusDate = $oOrderStatus->Data;
        $aStatusesCollection[] = $oDeliveryShipment;
      }
    }

    return $aStatusesCollection;
  }

  /**
   *
   * @param array $agConfig
   * @param string $sTransportNumber
   * @return string
   */
  public function getTransportListFilename($agConfig, $sTransportNumber) {

    $sFilename = 'ListPrzewozowyPwR_'.$sTransportNumber.'.pdf';

    return $agConfig['common']['client_base_path'].'/'.$agConfig['common']['cms_dir'].$agConfig['common']['ruch_dir'].$sFilename;
  }// end of getTransportListFilename() method


  /**
   *
   * @param int $iPointOfReceipt
   * @return string
   * @throws Exception
   */
  private function getPOPeR($iPointOfReceipt) {

    $sSql = 'SELECT details
             FROM shipping_points_ruch
             WHERE id = '.$iPointOfReceipt;
    $aPoint = $this->pDbMgr->GetRow('profit24', $sSql);
    if (!empty($aPoint)) {
      $aPointDetails = unserialize(base64_decode($aPoint['details']));
      $sPOPeR = $aPointDetails['destination'];
      return $sPOPeR;
      if ($aPointDetails['status'] == 'T') {
        $sPOPeR = $aPointDetails['destination'];
        return $sPOPeR;
      } else {
        throw new \Exception('Wybrany punkt odbioru przesyłki nie jest już dostępny: '.$iPointOfReceipt);
      }
    } else {
      throw new \Exception('Wybrany punkt odbioru przesyłki nie jest już dostępny: '.$iPointOfReceipt);
    }
  }// end of getPOPeR() method


  /**
   *
   * @param string $sXMLData
   * @return string
   */
  private function getPaczkaWRuchuTransportNumber($sXMLData) {

    $aMatches = array();
    preg_match('/\<PackCode_RUCH\>([^\<]+)\<\/PackCode_RUCH\>/', $sXMLData, $aMatches);
    return $aMatches[1];
  }
// end of getPaczkaWRuchuTransportNumber() method
}
