<?php
/**
 * Spedycja - Paczka w Ruchu
 * 
 * @author Paweł Bromka
 * @created 2014-11-24
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\PaczkaWRuchu;

class PaczkaWRuchu implements \communicator\sources\iShipment {

  private $bTestMode;
  
  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var WebServicePwRTest
   */
  private $PwR;
  private $iTransportId = 5;// symbol 'ruch'
  private $aTransportSettings;
  
  CONST WARTOSC_UBEZPIECZENIA_PRZESYLKI = 30000;

  public function __construct($bTestMode, $pDbMgr) {

    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }

  public function addShipment($aOrder, $aDeliverAddress, $aSellerData) {
    global $aConfig;
    
    $this->initPwR($aOrder['website_id']);
    
  }
  
  

  
  
  /**
   * 
   * @param int $iWebsiteId
   * @return string
   */
  private function getWebsiteEmail($iWebsiteId) {
    
    
    
    $sSql = 'SELECT email
             FROM website_settings';
    return $this->pDbMgr->GetOne($this->getWebsiteSymbol($iWebsiteId), $sSql);
  }

  public function doCancelShipment() {
    
  }
  
  
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type 
	 */
	private function getWebsiteSymbol($iWebsiteId) {
		global $pDbMgr;
		
		$sSql = "SELECT code 
             FROM websites
						 WHERE id=".$iWebsiteId;
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getWebsiteSymbol() method
  
  
  /**
   * 
   * @param int $iWebsiteId
   */
  private function initPwR($iWebsiteId) {
    
    if (is_null($this->PwR)) {
      $this->aTransportSettings = $this->getTransportSettings($iWebsiteId);
      $options = array();
      $options["login"] = $this->aTransportSettings['login'];//"it@profit24.pl";
      $options["password"] = $this->aTransportSettings['password'];//"test#pocztapolska$21A";
      $options["trace"] = 1;
      if ($this->bTestMode === TRUE) {
        $this->PwR = new WebServicePwR_TEST($this->aTransportSettings['api_url'], $options);
      } else {
        $this->PwR = new WebServicePwR($this->aTransportSettings['api_url'], $options);
      }
    }
  }
  
  
  /**
   * 
   * @param int $iWebsiteId
   * @return array
   */
  private function getTransportSettings($iWebsiteId) {
    $oKeyValue = new \table\keyValue('orders_transport_means', $this->getWebsiteSymbol($iWebsiteId));
    return $oKeyValue->getAllByIdDest($this->iTransportId);
  }// end of getTransportSettings() method

  public function doPrintAddressLabel($sFilePath, $sPrinter) {

  }

  public function getAddressLabel($sFilePath, $iOrderId) {

  }

  public function getDeliveryStatus($iId, $sTransportNumber) {

  }

  public function getDestinationPointsList() {

  }

    
  /**
   * 
   * @param string $sPointOfReceipt
   * @return bool
   */
  public function checkDestinationPointAvailable($sPointOfReceipt) {
    

  }
  
  /**
   * 
   * @param array $aTransportNumbers
   * @param array $aOrdersIds
   * @return string
   */
  public function getShipmentList($aTransportNumbers, $aOrdersIds = array()) {
    return $this->getPDFDeliver($aOrdersIds);
  }
  
  /**
   * 
   * @param int $iShippingPoint
   * @return array
   */
  public function getShippingPoint($iShippingPoint) {
    
    $this->initPwR(1);
    $oPwRPointsOfReceipt = new PaczkaWRuchuPointsOfReceipt($this->bTestMode, $this->pDbMgr);
    $oPwRPointsOfReceipt->PwR = $this->PwR;
    return $oPwRPointsOfReceipt->getShippingPoint($iShippingPoint);
  }
  



  
  /**
   * 
   * @param string $sXMLData
   * @return string
   */
  private function getErrorDescription($sXMLData) {
    
    $aMatches = array();
    preg_match('/\<ErrDes\>([^\<]+)\<\/ErrDes\>/', $sXMLData, $aMatches);
    return $aMatches[1];
  }// end of getPaczkaWRuchuTransportNumber() method
  

  private function getPaczkaWRuchuTransportNumber($sXMLData) {

  }
}
