<?php

namespace communicator\sources\PaczkaWRuchu;

class PaczkaWRuchuPointsOfReceipt
{

  private $bTest;
  
  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var WebServicePwRTest
   */
  public $PwR;
  

	public function __construct($pDbMgr, $bTest) {
    global $pDbMgr;
    
    $this->pDbMgr = $pDbMgr;
    $this->bTest = $bTest;
    if (is_null($this->pDbMgr)) {
      $this->pDbMgr = $pDbMgr;
    }
  }

  
  
  

  
  /**
   * Metoda sprawdza dostępność punktu odbioru
   *
   * @param type $sPointOfReceipt
   * @return bool
   */
  public function checkDestinationPointAvailable($sPointOfReceipt) {
  }
  
  
  /**
   * 
   * @global \DatabaseManager $pDbMgr
   * @param int $iShippingPoint
   * @return array
   */
  public function getShippingPoint($iShippingPoint) {

  }

}