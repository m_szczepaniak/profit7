<?php

namespace communicator\sources\PaczkaWRuchu;

class GiveMePSDProvince {
  public $Province; // string
}

class GiveMePSDProvinceResponse {
  public $GiveMePSDProvinceResult; // GiveMePSDProvinceResult
}

class GiveMePSDProvinceResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMeAllPSDLocation {
}

class GiveMeAllPSDLocationResponse {
  public $GiveMeAllPSDLocationResult; // GiveMeAllPSDLocationResult
}

class GiveMeAllPSDLocationResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMePSDZipcode {
}

class GiveMePSDZipcodeResponse {
  public $GiveMePSDZipcodeResult; // GiveMePSDZipcodeResult
}

class GiveMePSDZipcodeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMeCityPSDLocation {
  public $City; // string
}

class GiveMeCityPSDLocationResponse {
  public $GiveMeCityPSDLocationResult; // GiveMeCityPSDLocationResult
}

class GiveMeCityPSDLocationResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMePackStatus {
  public $PhoneNumber; // string
  public $PackCode; // string
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GiveMePackStatusResponse {
  public $GiveMePackStatusResult; // GiveMePackStatusResult
}

class GiveMePackStatusResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateBusinessPack {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
}

class GenerateBusinessPackResponse {
  public $GenerateBusinessPackResult; // GenerateBusinessPackResult
}

class GenerateBusinessPackResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class PutCustomerPackCanceled {
  public $PackCode; // string
  public $PartnerID; // string
  public $PartnerKey; // string
}

class PutCustomerPackCanceledResponse {
  public $PutCustomerPackCanceledResult; // PutCustomerPackCanceledResult
}

class PutCustomerPackCanceledResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class PutCustomerPack {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PackCode; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
}

class PutCustomerPackResponse {
  public $PutCustomerPackResult; // PutCustomerPackResult
}

class PutCustomerPackResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateNumberWaybills {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $NumberOfWaybills; // string
}

class GenerateNumberWaybillsResponse {
  public $GenerateNumberWaybillsResult; // GenerateNumberWaybillsResult
}

class GenerateNumberWaybillsResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class CreateCustomer {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $Nip; // string
  public $City; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $EMail; // string
}

class CreateCustomerResponse {
  public $CreateCustomerResult; // CreateCustomerResult
}

class CreateCustomerResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GivePaymentType {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GivePaymentTypeResponse {
  public $GivePaymentTypeResult; // GivePaymentTypeResult
}

class GivePaymentTypeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GivePartnerStatus {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GivePartnerStatusResponse {
  public $GivePartnerStatusResult; // GivePartnerStatusResult
}

class GivePartnerStatusResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPackList {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $Format; // FormatPliku
  public $BusinessPackList; // ArrayOfBusinessPack
}

class FormatPliku {
  const PDF = 'PDF';
  const EPL = 'EPL';
}

class BusinessPack {
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
}

class GenerateLabelBusinessPackListResponse {
  public $GenerateLabelBusinessPackListResult; // GenerateLabelBusinessPackListResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackListResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPack {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
}

class GenerateLabelBusinessPackResponse {
  public $GenerateLabelBusinessPackResult; // GenerateLabelBusinessPackResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateBusinessPackSignature {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
  public $Signature; // string
  public $SignatureInstr1; // string
  public $SignatureInstr2; // string
}

class GenerateBusinessPackSignatureResponse {
  public $GenerateBusinessPackSignatureResult; // GenerateBusinessPackSignatureResult
  public $LabelData; // base64Binary
}

class GenerateBusinessPackSignatureResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}


/**
 * WebServicePwRTest class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class WebServicePwR_TEST extends \SoapClient {

  private static $classmap = array(
                                    'GiveMePSDProvince' => 'GiveMePSDProvince',
                                    'GiveMePSDProvinceResponse' => 'GiveMePSDProvinceResponse',
                                    'GiveMePSDProvinceResult' => 'GiveMePSDProvinceResult',
                                    'GiveMeAllPSDLocation' => 'GiveMeAllPSDLocation',
                                    'GiveMeAllPSDLocationResponse' => 'GiveMeAllPSDLocationResponse',
                                    'GiveMeAllPSDLocationResult' => 'GiveMeAllPSDLocationResult',
                                    'GiveMePSDZipcode' => 'GiveMePSDZipcode',
                                    'GiveMePSDZipcodeResponse' => 'GiveMePSDZipcodeResponse',
                                    'GiveMePSDZipcodeResult' => 'GiveMePSDZipcodeResult',
                                    'GiveMeCityPSDLocation' => 'GiveMeCityPSDLocation',
                                    'GiveMeCityPSDLocationResponse' => 'GiveMeCityPSDLocationResponse',
                                    'GiveMeCityPSDLocationResult' => 'GiveMeCityPSDLocationResult',
                                    'GiveMePackStatus' => 'GiveMePackStatus',
                                    'GiveMePackStatusResponse' => 'GiveMePackStatusResponse',
                                    'GiveMePackStatusResult' => 'GiveMePackStatusResult',
                                    'GenerateBusinessPack' => 'GenerateBusinessPack',
                                    'GenerateBusinessPackResponse' => 'GenerateBusinessPackResponse',
                                    'GenerateBusinessPackResult' => 'GenerateBusinessPackResult',
                                    'PutCustomerPackCanceled' => 'PutCustomerPackCanceled',
                                    'PutCustomerPackCanceledResponse' => 'PutCustomerPackCanceledResponse',
                                    'PutCustomerPackCanceledResult' => 'PutCustomerPackCanceledResult',
                                    'PutCustomerPack' => 'PutCustomerPack',
                                    'PutCustomerPackResponse' => 'PutCustomerPackResponse',
                                    'PutCustomerPackResult' => 'PutCustomerPackResult',
                                    'GenerateNumberWaybills' => 'GenerateNumberWaybills',
                                    'GenerateNumberWaybillsResponse' => 'GenerateNumberWaybillsResponse',
                                    'GenerateNumberWaybillsResult' => 'GenerateNumberWaybillsResult',
                                    'CreateCustomer' => 'CreateCustomer',
                                    'CreateCustomerResponse' => 'CreateCustomerResponse',
                                    'CreateCustomerResult' => 'CreateCustomerResult',
                                    'GivePaymentType' => 'GivePaymentType',
                                    'GivePaymentTypeResponse' => 'GivePaymentTypeResponse',
                                    'GivePaymentTypeResult' => 'GivePaymentTypeResult',
                                    'GivePartnerStatus' => 'GivePartnerStatus',
                                    'GivePartnerStatusResponse' => 'GivePartnerStatusResponse',
                                    'GivePartnerStatusResult' => 'GivePartnerStatusResult',
                                    'GenerateLabelBusinessPackList' => 'GenerateLabelBusinessPackList',
                                    'FormatPliku' => 'FormatPliku',
                                    'BusinessPack' => 'BusinessPack',
                                    'GenerateLabelBusinessPackListResponse' => 'GenerateLabelBusinessPackListResponse',
                                    'GenerateLabelBusinessPackListResult' => 'GenerateLabelBusinessPackListResult',
                                    'GenerateLabelBusinessPack' => 'GenerateLabelBusinessPack',
                                    'GenerateLabelBusinessPackResponse' => 'GenerateLabelBusinessPackResponse',
                                    'GenerateLabelBusinessPackResult' => 'GenerateLabelBusinessPackResult',
                                    'GenerateBusinessPackSignature' => 'GenerateBusinessPackSignature',
                                    'GenerateBusinessPackSignatureResponse' => 'GenerateBusinessPackSignatureResponse',
                                    'GenerateBusinessPackSignatureResult' => 'GenerateBusinessPackSignatureResult',
                                   );

  public function WebServicePwRTest($wsdl = "https://91.242.220.103/WebServicePwR/WebServicePwRTest.asmx?wsdl", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *  
   *
   * @param GiveMePSDProvince $parameters
   * @return GiveMePSDProvinceResponse
   */
  public function GiveMePSDProvince(GiveMePSDProvince $parameters) {
    return $this->__soapCall('GiveMePSDProvince', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMeAllPSDLocation $parameters
   * @return GiveMeAllPSDLocationResponse
   */
  public function GiveMeAllPSDLocation(GiveMeAllPSDLocation $parameters) {
    return $this->__soapCall('GiveMeAllPSDLocation', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMePSDZipcode $parameters
   * @return GiveMePSDZipcodeResponse
   */
  public function GiveMePSDZipcode(GiveMePSDZipcode $parameters) {
    return $this->__soapCall('GiveMePSDZipcode', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMeCityPSDLocation $parameters
   * @return GiveMeCityPSDLocationResponse
   */
  public function GiveMeCityPSDLocation(GiveMeCityPSDLocation $parameters) {
    return $this->__soapCall('GiveMeCityPSDLocation', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMePackStatus $parameters
   * @return GiveMePackStatusResponse
   */
  public function GiveMePackStatus(GiveMePackStatus $parameters) {
    return $this->__soapCall('GiveMePackStatus', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateBusinessPack $parameters
   * @return GenerateBusinessPackResponse
   */
  public function GenerateBusinessPack(GenerateBusinessPack $parameters) {
    return $this->__soapCall('GenerateBusinessPack', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param PutCustomerPackCanceled $parameters
   * @return PutCustomerPackCanceledResponse
   */
  public function PutCustomerPackCanceled(PutCustomerPackCanceled $parameters) {
    return $this->__soapCall('PutCustomerPackCanceled', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param PutCustomerPack $parameters
   * @return PutCustomerPackResponse
   */
  public function PutCustomerPack(PutCustomerPack $parameters) {
    return $this->__soapCall('PutCustomerPack', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateNumberWaybills $parameters
   * @return GenerateNumberWaybillsResponse
   */
  public function GenerateNumberWaybills(GenerateNumberWaybills $parameters) {
    return $this->__soapCall('GenerateNumberWaybills', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CreateCustomer $parameters
   * @return CreateCustomerResponse
   */
  public function CreateCustomer(CreateCustomer $parameters) {
    return $this->__soapCall('CreateCustomer', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GivePaymentType $parameters
   * @return GivePaymentTypeResponse
   */
  public function GivePaymentType(GivePaymentType $parameters) {
    return $this->__soapCall('GivePaymentType', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GivePartnerStatus $parameters
   * @return GivePartnerStatusResponse
   */
  public function GivePartnerStatus(GivePartnerStatus $parameters) {
    return $this->__soapCall('GivePartnerStatus', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPackList $parameters
   * @return GenerateLabelBusinessPackListResponse
   */
  public function GenerateLabelBusinessPackList(GenerateLabelBusinessPackList $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPackList', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPack $parameters
   * @return GenerateLabelBusinessPackResponse
   */
  public function GenerateLabelBusinessPack(GenerateLabelBusinessPack $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPack', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateBusinessPackSignature $parameters
   * @return GenerateBusinessPackSignatureResponse
   */
  public function GenerateBusinessPackSignature(GenerateBusinessPackSignature $parameters) {
    return $this->__soapCall('GenerateBusinessPackSignature', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwRTest',
            'soapaction' => ''
           )
      );
  }

}

?>
