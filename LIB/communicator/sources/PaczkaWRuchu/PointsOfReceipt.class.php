<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\PaczkaWRuchu;

/**
 * Description of PointsOfReceipt
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class PointsOfReceipt extends Shipment implements \communicator\sources\iPointsOfReceipt {

    public $aTransportSettings;
    public $bTestMode;
    public $pDbMgr;

    /**
     *
     * @var ElektronicznyNadawca $oEN
     */
    public $en;

    function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
        parent::__construct($aTransportSettings, $pDbMgr, $bTestMode);
        $this->aTransportSettings = $aTransportSettings;
        $this->pDbMgr = $pDbMgr;
        $this->bTestMode = $bTestMode;
    }

    public function checkDestinationPointAvailable($sPointOfReceipt) {

        $sSql = "SELECT details
             FROM shipping_points_ruch 
             WHERE id = '".$sPointOfReceipt."'";

        $aPointDetails = unserialize(base64_decode($this->pDbMgr->GetOne('profit24', $sSql)));
        return $aPointDetails['status'] == 'T' ? true : false;
    }

    public function checkDestinationPointAvailablePayCashOnDelivery($sPointOfReceipt) {
        $sSql = "SELECT details
             FROM shipping_points_ruch 
             WHERE id = '".$sPointOfReceipt."'";

        $aPointDetails = unserialize(base64_decode($this->pDbMgr->GetOne('profit24', $sSql)));
        return $aPointDetails['status'] == 'T' && $aPointDetails['cashondelivery'] == 'true' ? true : false;
    }

    /**
     * Metoda zwraca pobraną z WebApi listę punktów odbioru
     *
     * @return array
     */
    public function getDestinationPointsList() {

        try {
            $g = new GiveMeAllRUCHLocation();
            $g->PartnerID = $this->aTransportSettings['login'];
            $g->PartnerKey = $this->aTransportSettings['password'];
            $result = $this->PwR->GiveMeAllRUCHLocation($g);

            $xml = simplexml_load_string($result->GiveMeAllRUCHLocationResult->any);
            foreach ($xml->NewDataSet->AllRUCHLocation as $psd) {
                $response[] = $psd;
            }

            return $response;
        }
        catch (SoapFault $e)
        {
            echo $e->faultcode.'<br />';
            echo $e->faultstring;
        }
    }

    /**
     *
     * @see getShippingPoint
     * @global \DatabaseManager $pDbMgr
     * @param string $sPointOfReceipt
     * @return array
     */
    public function getSinglePointDetails($sPointOfReceipt) {
        global $pDbMgr;

        $sSql = 'SELECT details FROM  shipping_points_ruch WHERE id = '.$sPointOfReceipt;
        return unserialize(base64_decode($pDbMgr->GetOne('profit24', $sSql)));
    }


    /**
     * Metoda zwracająca tablicę z informacjami o punktach sieci Ruch
     *
     * @return array
     */
    public function getDestinationPointsDropdown() {

        $sAddSQL = " WHERE status = 'T'
             ORDER BY `desc` ASC";
        $data = $this->getPointsOfReceipt(array('`desc` AS label', '`id` AS value, details'), $sAddSQL);
        return $data;
    }


    /**
     * Metoda zwraca listę punktów odbioru w postaci tablicy
     *
     * @return array
     */
    public function getDestinationPointsArray($data) {
        $aDataItems = array();

        $date = new \DateTime();
        $timestamp = $date->format('Y-m-d H:i:s');


        foreach ($data as $key => $row) {

            $id = (string) $row->PSD;
            $numerPlacowki = (string) $row->DestinationCode;
            $x = (string) $row->Latitude;
            $y = (string) $row->Longitude;
            $wojewodztwo = (string) $row->Province;
            $powiat = (string) $row->District;
            $miejscowosc = (string) $row->City;
            $ulica = (string) $row->StreetName;
            $budynek = (string) $row->BuildingNumber;
            $platnePrzyOdbiorze = (string) $row->CashOnDelivery;
            $lokalizacja = (string) $row->Location;
            $godzinyOtwarcia = (string) $row->OpeningHours;
            $stanPlacowki = (string) $row->Available;
            $pointType = (string) $row->PointType;

            $aDetails = array(
                'id' => $id,
                'destination' => $numerPlacowki,
                'x' => $x,
                'y' => $y,
                'province' => $wojewodztwo,
                'district' => $powiat,
                'city' => $miejscowosc,
                'street' => $ulica.' '.$budynek,
                'cashondelivery' => $platnePrzyOdbiorze,
                'locale' => $lokalizacja,
                'hours' => $godzinyOtwarcia,
                'status' => $stanPlacowki,
                'pointType' => $pointType
            );

            $aDataItems[] = array(
                'id' => $id,
                '`desc`' => $miejscowosc.' '.$ulica.' '.$budynek,
                'city' => $miejscowosc,
                'status' => $stanPlacowki,
                'created' => $timestamp,
                'details' => base64_encode(serialize($aDetails)),
            );
        }

        return $aDataItems;
    }


    /**
     * Metoda aktualizuje w bazie listę punktów odbioru
     *
     * @return bool
     */
    public function doInsertDestinationPointsList($aDataItems) {
        global $pDbMgr;

        if (!$aDataItems) {
            return false;
        }

        if ($this->bTest === true) {
            $time_start = microtime(true); // pomiar czasu
        }

        $sSql = "DELETE FROM shipping_points_ruch WHERE id > 0";
        $pDbMgr->Query('profit24', $sSql);


        foreach ($aDataItems as $aDataItem) {

            if ($this->pDbMgr->Insert('profit24', 'shipping_points_ruch', $aDataItem) === false) {
                //return false;
            }
        }

        if ($this->bTest === true) {
            $time_end = microtime(true);
            $execution_time = ($time_end - $time_start)/60;
            echo '<b>Czas wykonania insertu:</b> '.$execution_time.' Mins'; // pomiar czasu
        }
    }


    /**
     *
     * @param array $aPontsOfRecipt
     * @return array
     */
    private function doFormatPointsOfReceiptAutocomplete($aPontsOfRecipt) {

        foreach($aPontsOfRecipt as $iKey => $aValue) {
            $aPontsOfRecipt[$iKey]['data'] = json_encode(unserialize(base64_decode($aValue['data'])));
        }

        return $aPontsOfRecipt;
    }// end of doFormatPointsOfReceipt() method


    /**
     *
     * @param array $aCols
     * @param string $sAddSQL
     * @return array
     */
    private function getPointsOfReceipt($aCols, $sAddSQL) {

        $sSql = "SELECT ".implode(',', $aCols)."
             FROM shipping_points_ruch 
             ".$sAddSQL;
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     *
     * @param array $aCols
     * @param string $sAddSQL
     * @return array
     */
    public function getPointsOfReceiptAutocomplete($aCols, $sAddSQL) {
        $aPointsOfReceipt = $this->getPointsOfReceipt($aCols, $sAddSQL);
        return $this->doFormatPointsOfReceiptAutocomplete($aPointsOfReceipt);
    }


    /**
     *
     *
     * @param int $sPointOfReceipt
     * @return array
     */
    public function getPopupDetails($sPointOfReceipt) {

        $sFontColor = '#fff';
        $sBackgroundColor = '#504747';
        $aFormatedPointOfReciptDetails = $this->getSinglePointDetails($sPointOfReceipt);

        $sDetails = "Wybrany punkt sieci RUCH: <b><br />"
            . $aFormatedPointOfReciptDetails['city'] . ', ' . $aFormatedPointOfReciptDetails['street'] . '</b><br /><br />'.$aFormatedPointOfReciptDetails['locale'].'<br />'.$aFormatedPointOfReciptDetails['hours'];

        if ($aFormatedPointOfReciptDetails['cashondelivery'] == 'false') {
            $sDetails .= '<br /><strong>Brak płatności przy odbiorze !</strong>';
        }

        if (!empty($aFormatedPointOfReciptDetails) && is_array($aFormatedPointOfReciptDetails)) {
            $sSrc = 'https://maps.google.pl/maps?f=q&source=s_q&hl=pl&geocode=&q='
                .$aFormatedPointOfReciptDetails['x'].','.$aFormatedPointOfReciptDetails['y']
                .'('.$aFormatedPointOfReciptDetails['city'].' '.$aFormatedPointOfReciptDetails['street']
                .')&aq=&vpsrc=0&ie=UTF8&t=m&z=14&'
                .$aFormatedPointOfReciptDetails['x'].','.$aFormatedPointOfReciptDetails['y']
                .'&output=embed';
        }

        return array('fontColor' => $sFontColor, 'backgroundColor' => $sBackgroundColor, 'details' => $sDetails, 'source' => $sSrc);
    }
}
