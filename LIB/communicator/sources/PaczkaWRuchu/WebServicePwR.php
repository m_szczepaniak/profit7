<?php
namespace communicator\sources\PaczkaWRuchu;
class GiveMeAllRUCHZipcode {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GiveMeAllRUCHZipcodeResponse {
  public $GiveMeAllRUCHZipcodeResult; // GiveMeAllRUCHZipcodeResult
}

class GiveMeAllRUCHZipcodeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMeAllRUCHLocation {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GiveMeAllRUCHLocationResponse {
  public $GiveMeAllRUCHLocationResult; // GiveMeAllRUCHLocationResult
}

class GiveMeAllRUCHLocationResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMeAllPSDLocation {
}

class GiveMeAllPSDLocationResponse {
  public $GiveMeAllPSDLocationResult; // GiveMeAllPSDLocationResult
}

class GiveMeAllPSDLocationResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMePSDProvince {
  public $Province; // string
}

class GiveMePSDProvinceResponse {
  public $GiveMePSDProvinceResult; // GiveMePSDProvinceResult
}

class GiveMePSDProvinceResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMePSDZipcode {
}

class GiveMePSDZipcodeResponse {
  public $GiveMePSDZipcodeResult; // GiveMePSDZipcodeResult
}

class GiveMePSDZipcodeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMeCityPSDLocation {
  public $City; // string
}

class GiveMeCityPSDLocationResponse {
  public $GiveMeCityPSDLocationResult; // GiveMeCityPSDLocationResult
}

class GiveMeCityPSDLocationResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveMePackStatus {
  public $PhoneNumber; // string
  public $PackCode; // string
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GiveMePackStatusResponse {
  public $GiveMePackStatusResult; // GiveMePackStatusResult
}

class GiveMePackStatusResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateBusinessPack {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
}

class GenerateBusinessPackResponse {
  public $GenerateBusinessPackResult; // GenerateBusinessPackResult
}

class GenerateBusinessPackResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateBusinessPackTwo {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $AutoDestinationChange; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $CofirmMailing; // string
}

class GenerateBusinessPackTwoResponse {
  public $GenerateBusinessPackTwoResult; // GenerateBusinessPackTwoResult
}

class GenerateBusinessPackTwoResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class PutCustomerPackCanceled {
  public $PackCode; // string
  public $PartnerID; // string
  public $PartnerKey; // string
}

class PutCustomerPackCanceledResponse {
  public $PutCustomerPackCanceledResult; // PutCustomerPackCanceledResult
}

class PutCustomerPackCanceledResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class PutCustomerPack {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PackCode; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
}

class PutCustomerPackResponse {
  public $PutCustomerPackResult; // PutCustomerPackResult
}

class PutCustomerPackResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateNumberWaybills {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $NumberOfWaybills; // string
}

class GenerateNumberWaybillsResponse {
  public $GenerateNumberWaybillsResult; // GenerateNumberWaybillsResult
}

class GenerateNumberWaybillsResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class CreateCustomer {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $Nip; // string
  public $City; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $EMail; // string
}

class CreateCustomerResponse {
  public $CreateCustomerResult; // CreateCustomerResult
}

class CreateCustomerResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GivePaymentType {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GivePaymentTypeResponse {
  public $GivePaymentTypeResult; // GivePaymentTypeResult
}

class GivePaymentTypeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GivePartnerStatus {
  public $PartnerID; // string
  public $PartnerKey; // string
}

class GivePartnerStatusResponse {
  public $GivePartnerStatusResult; // GivePartnerStatusResult
}

class GivePartnerStatusResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPackList {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $Format; // FormatPliku
  public $BusinessPackList; // ArrayOfBusinessPack
}

class FormatPliku {
  const PDF = 'PDF';
  const EPL = 'EPL';
}

class BusinessPack {
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $CofirmMailing; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
}

class GenerateLabelBusinessPackListResponse {
  public $GenerateLabelBusinessPackListResult; // GenerateLabelBusinessPackListResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackListResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPackListTwo {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $AutoDestinationChange; // string
  public $Format; // FormatPliku3
  public $BusinessPackList; // ArrayOfBusinessPack
}

class FormatPliku3 {
  const PDF = 'PDF';
  const EPL = 'EPL';
  const PDF10 = 'PDF10';
}

class GenerateLabelBusinessPackListTwoResponse {
  public $GenerateLabelBusinessPackListTwoResult; // GenerateLabelBusinessPackListTwoResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackListTwoResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPack {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
}

class GenerateLabelBusinessPackResponse {
  public $GenerateLabelBusinessPackResult; // GenerateLabelBusinessPackResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPackTwo {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $PrintType; // string
  public $AutoDestinationChange; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $CofirmMailing; // string
}

class GenerateLabelBusinessPackTwoResponse {
  public $GenerateLabelBusinessPackTwoResult; // GenerateLabelBusinessPackTwoResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackTwoResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GetGeneratedParcels {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $dateFrom; // dateTime
  public $dateTo; // dateTime
}

class GetGeneratedParcelsResponse {
  public $GetGeneratedParcelsResult; // GetGeneratedParcelsResult
}

class GetGeneratedParcelsResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateProtocol {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $parcels; // ArrayOfUnsignedLong
}

class GenerateProtocolResponse {
  public $GenerateProtocolResult; // GenerateProtocolResult
  public $LabelData; // base64Binary
}

class GenerateProtocolResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class LabelPrintDuplicateList {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $Format; // FormatPliku
  public $PackCodeList; // ArrayOfString
}

class LabelPrintDuplicateListResponse {
  public $LabelPrintDuplicateListResult; // LabelPrintDuplicateListResult
  public $LabelData; // base64Binary
}

class LabelPrintDuplicateListResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class LabelPrintDuplicate {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PackCode; // string
}

class LabelPrintDuplicateResponse {
  public $LabelPrintDuplicateResult; // LabelBundle
}

class LabelBundle {
  public $Err; // string
  public $ErrDes; // string
  public $Label; // base64Binary
}

class GenerateShippingCode {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PhoneNumber; // string
  public $DestinationCode; // string
  public $AlternativeDestinationCode; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $AutoDestinationChange; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $CofirmMailing; // string
}

class GenerateShippingCodeResponse {
  public $GenerateShippingCodeResult; // GenerateShippingCodeResult
}

class GenerateShippingCodeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GiveShippingCodeStatus {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $Codes; // ArrayOfString
}

class GiveShippingCodeStatusResponse {
  public $GiveShippingCodeStatusResult; // GiveShippingCodeStatusResult
}

class GiveShippingCodeStatusResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateShippingCodeRuch2Home {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $PhoneNumber; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $ConfirmMailing; // string
}

class GenerateShippingCodeRuch2HomeResponse {
  public $GenerateShippingCodeRuch2HomeResult; // GenerateShippingCodeRuch2HomeResult
}

class GenerateShippingCodeRuch2HomeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}

class GenerateLabelBusinessPackRuch2Home {
  public $PartnerID; // string
  public $PartnerKey; // string
  public $BoxSize; // string
  public $PackValue; // string
  public $CashOnDelivery; // string
  public $AmountCashOnDelivery; // string
  public $Insurance; // string
  public $EMail; // string
  public $FirstName; // string
  public $LastName; // string
  public $CompanyName; // string
  public $StreetName; // string
  public $BuildingNumber; // string
  public $FlatNumber; // string
  public $City; // string
  public $PostCode; // string
  public $PhoneNumber; // string
  public $SenderEMail; // string
  public $SenderFirstName; // string
  public $SenderLastName; // string
  public $SenderCompanyName; // string
  public $SenderStreetName; // string
  public $SenderBuildingNumber; // string
  public $SenderFlatNumber; // string
  public $SenderCity; // string
  public $SenderPostCode; // string
  public $SenderPhoneNumber; // string
  public $SenderOrders; // string
  public $ReturnDestinationCode; // string
  public $ReturnEMail; // string
  public $ReturnFirstName; // string
  public $ReturnLastName; // string
  public $ReturnCompanyName; // string
  public $ReturnStreetName; // string
  public $ReturnBuildingNumber; // string
  public $ReturnFlatNumber; // string
  public $ReturnCity; // string
  public $ReturnPostCode; // string
  public $ReturnPhoneNumber; // string
  public $ReturnPack; // string
  public $TransferDescription; // string
  public $PrintAdress; // string
  public $ReturnAvailable; // string
  public $ReturnQuantity; // string
  public $ConfirmTermsOfUse; // string
  public $ConfirmMarketing; // string
  public $ConfirmMailing; // string
}

class GenerateLabelBusinessPackRuch2HomeResponse {
  public $GenerateLabelBusinessPackRuch2HomeResult; // GenerateLabelBusinessPackRuch2HomeResult
  public $LabelData; // base64Binary
}

class GenerateLabelBusinessPackRuch2HomeResult {
  public $schema; // <anyXML>
  public $any; // <anyXML>
}


/**
 * WebServicePwR class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class WebServicePwR extends \SoapClient {

  private static $classmap = array(
                                    'GiveMeAllRUCHZipcode' => 'GiveMeAllRUCHZipcode',
                                    'GiveMeAllRUCHZipcodeResponse' => 'GiveMeAllRUCHZipcodeResponse',
                                    'GiveMeAllRUCHZipcodeResult' => 'GiveMeAllRUCHZipcodeResult',
                                    'GiveMeAllRUCHLocation' => 'GiveMeAllRUCHLocation',
                                    'GiveMeAllRUCHLocationResponse' => 'GiveMeAllRUCHLocationResponse',
                                    'GiveMeAllRUCHLocationResult' => 'GiveMeAllRUCHLocationResult',
                                    'GiveMeAllPSDLocation' => 'GiveMeAllPSDLocation',
                                    'GiveMeAllPSDLocationResponse' => 'GiveMeAllPSDLocationResponse',
                                    'GiveMeAllPSDLocationResult' => 'GiveMeAllPSDLocationResult',
                                    'GiveMePSDProvince' => 'GiveMePSDProvince',
                                    'GiveMePSDProvinceResponse' => 'GiveMePSDProvinceResponse',
                                    'GiveMePSDProvinceResult' => 'GiveMePSDProvinceResult',
                                    'GiveMePSDZipcode' => 'GiveMePSDZipcode',
                                    'GiveMePSDZipcodeResponse' => 'GiveMePSDZipcodeResponse',
                                    'GiveMePSDZipcodeResult' => 'GiveMePSDZipcodeResult',
                                    'GiveMeCityPSDLocation' => 'GiveMeCityPSDLocation',
                                    'GiveMeCityPSDLocationResponse' => 'GiveMeCityPSDLocationResponse',
                                    'GiveMeCityPSDLocationResult' => 'GiveMeCityPSDLocationResult',
                                    'GiveMePackStatus' => 'GiveMePackStatus',
                                    'GiveMePackStatusResponse' => 'GiveMePackStatusResponse',
                                    'GiveMePackStatusResult' => 'GiveMePackStatusResult',
                                    'GenerateBusinessPack' => 'GenerateBusinessPack',
                                    'GenerateBusinessPackResponse' => 'GenerateBusinessPackResponse',
                                    'GenerateBusinessPackResult' => 'GenerateBusinessPackResult',
                                    'GenerateBusinessPackTwo' => 'GenerateBusinessPackTwo',
                                    'GenerateBusinessPackTwoResponse' => 'GenerateBusinessPackTwoResponse',
                                    'GenerateBusinessPackTwoResult' => 'GenerateBusinessPackTwoResult',
                                    'PutCustomerPackCanceled' => 'PutCustomerPackCanceled',
                                    'PutCustomerPackCanceledResponse' => 'PutCustomerPackCanceledResponse',
                                    'PutCustomerPackCanceledResult' => 'PutCustomerPackCanceledResult',
                                    'PutCustomerPack' => 'PutCustomerPack',
                                    'PutCustomerPackResponse' => 'PutCustomerPackResponse',
                                    'PutCustomerPackResult' => 'PutCustomerPackResult',
                                    'GenerateNumberWaybills' => 'GenerateNumberWaybills',
                                    'GenerateNumberWaybillsResponse' => 'GenerateNumberWaybillsResponse',
                                    'GenerateNumberWaybillsResult' => 'GenerateNumberWaybillsResult',
                                    'CreateCustomer' => 'CreateCustomer',
                                    'CreateCustomerResponse' => 'CreateCustomerResponse',
                                    'CreateCustomerResult' => 'CreateCustomerResult',
                                    'GivePaymentType' => 'GivePaymentType',
                                    'GivePaymentTypeResponse' => 'GivePaymentTypeResponse',
                                    'GivePaymentTypeResult' => 'GivePaymentTypeResult',
                                    'GivePartnerStatus' => 'GivePartnerStatus',
                                    'GivePartnerStatusResponse' => 'GivePartnerStatusResponse',
                                    'GivePartnerStatusResult' => 'GivePartnerStatusResult',
                                    'GenerateLabelBusinessPackList' => 'GenerateLabelBusinessPackList',
                                    'FormatPliku' => 'FormatPliku',
                                    'BusinessPack' => 'BusinessPack',
                                    'GenerateLabelBusinessPackListResponse' => 'GenerateLabelBusinessPackListResponse',
                                    'GenerateLabelBusinessPackListResult' => 'GenerateLabelBusinessPackListResult',
                                    'GenerateLabelBusinessPackListTwo' => 'GenerateLabelBusinessPackListTwo',
                                    'FormatPliku3' => 'FormatPliku3',
                                    'GenerateLabelBusinessPackListTwoResponse' => 'GenerateLabelBusinessPackListTwoResponse',
                                    'GenerateLabelBusinessPackListTwoResult' => 'GenerateLabelBusinessPackListTwoResult',
                                    'GenerateLabelBusinessPack' => 'GenerateLabelBusinessPack',
                                    'GenerateLabelBusinessPackResponse' => 'GenerateLabelBusinessPackResponse',
                                    'GenerateLabelBusinessPackResult' => 'GenerateLabelBusinessPackResult',
                                    'GenerateLabelBusinessPackTwo' => 'GenerateLabelBusinessPackTwo',
                                    'GenerateLabelBusinessPackTwoResponse' => 'GenerateLabelBusinessPackTwoResponse',
                                    'GenerateLabelBusinessPackTwoResult' => 'GenerateLabelBusinessPackTwoResult',
                                    'GetGeneratedParcels' => 'GetGeneratedParcels',
                                    'GetGeneratedParcelsResponse' => 'GetGeneratedParcelsResponse',
                                    'GetGeneratedParcelsResult' => 'GetGeneratedParcelsResult',
                                    'GenerateProtocol' => 'GenerateProtocol',
                                    'GenerateProtocolResponse' => 'GenerateProtocolResponse',
                                    'GenerateProtocolResult' => 'GenerateProtocolResult',
                                    'LabelPrintDuplicateList' => 'LabelPrintDuplicateList',
                                    'LabelPrintDuplicateListResponse' => 'LabelPrintDuplicateListResponse',
                                    'LabelPrintDuplicateListResult' => 'LabelPrintDuplicateListResult',
                                    'LabelPrintDuplicate' => 'LabelPrintDuplicate',
                                    'LabelPrintDuplicateResponse' => 'LabelPrintDuplicateResponse',
                                    'LabelBundle' => 'LabelBundle',
                                    'GenerateShippingCode' => 'GenerateShippingCode',
                                    'GenerateShippingCodeResponse' => 'GenerateShippingCodeResponse',
                                    'GenerateShippingCodeResult' => 'GenerateShippingCodeResult',
                                    'GiveShippingCodeStatus' => 'GiveShippingCodeStatus',
                                    'GiveShippingCodeStatusResponse' => 'GiveShippingCodeStatusResponse',
                                    'GiveShippingCodeStatusResult' => 'GiveShippingCodeStatusResult',
                                    'GenerateShippingCodeRuch2Home' => 'GenerateShippingCodeRuch2Home',
                                    'GenerateShippingCodeRuch2HomeResponse' => 'GenerateShippingCodeRuch2HomeResponse',
                                    'GenerateShippingCodeRuch2HomeResult' => 'GenerateShippingCodeRuch2HomeResult',
                                    'GenerateLabelBusinessPackRuch2Home' => 'GenerateLabelBusinessPackRuch2Home',
                                    'GenerateLabelBusinessPackRuch2HomeResponse' => 'GenerateLabelBusinessPackRuch2HomeResponse',
                                    'GenerateLabelBusinessPackRuch2HomeResult' => 'GenerateLabelBusinessPackRuch2HomeResult',
                                   );

  public function WebServicePwR($wsdl = "https://91.242.220.103/WebServicePwRProd/WebServicePwR.asmx?wsdl", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *  
   *
   * @param GiveMeAllRUCHZipcode $parameters
   * @return GiveMeAllRUCHZipcodeResponse
   */
  public function GiveMeAllRUCHZipcode(GiveMeAllRUCHZipcode $parameters) {
    return $this->__soapCall('GiveMeAllRUCHZipcode', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMeAllRUCHLocation $parameters
   * @return GiveMeAllRUCHLocationResponse
   */
  public function GiveMeAllRUCHLocation(GiveMeAllRUCHLocation $parameters) {
    return $this->__soapCall('GiveMeAllRUCHLocation', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMeAllPSDLocation $parameters
   * @return GiveMeAllPSDLocationResponse
   */
  public function GiveMeAllPSDLocation(GiveMeAllPSDLocation $parameters) {
    return $this->__soapCall('GiveMeAllPSDLocation', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMePSDProvince $parameters
   * @return GiveMePSDProvinceResponse
   */
  public function GiveMePSDProvince(GiveMePSDProvince $parameters) {
    return $this->__soapCall('GiveMePSDProvince', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMePSDZipcode $parameters
   * @return GiveMePSDZipcodeResponse
   */
  public function GiveMePSDZipcode(GiveMePSDZipcode $parameters) {
    return $this->__soapCall('GiveMePSDZipcode', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMeCityPSDLocation $parameters
   * @return GiveMeCityPSDLocationResponse
   */
  public function GiveMeCityPSDLocation(GiveMeCityPSDLocation $parameters) {
    return $this->__soapCall('GiveMeCityPSDLocation', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveMePackStatus $parameters
   * @return GiveMePackStatusResponse
   */
  public function GiveMePackStatus(GiveMePackStatus $parameters) {
    return $this->__soapCall('GiveMePackStatus', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateBusinessPack $parameters
   * @return GenerateBusinessPackResponse
   */
  public function GenerateBusinessPack(GenerateBusinessPack $parameters) {
    return $this->__soapCall('GenerateBusinessPack', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateBusinessPackTwo $parameters
   * @return GenerateBusinessPackTwoResponse
   */
  public function GenerateBusinessPackTwo(GenerateBusinessPackTwo $parameters) {
    return $this->__soapCall('GenerateBusinessPackTwo', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param PutCustomerPackCanceled $parameters
   * @return PutCustomerPackCanceledResponse
   */
  public function PutCustomerPackCanceled(PutCustomerPackCanceled $parameters) {
    return $this->__soapCall('PutCustomerPackCanceled', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param PutCustomerPack $parameters
   * @return PutCustomerPackResponse
   */
  public function PutCustomerPack(PutCustomerPack $parameters) {
    return $this->__soapCall('PutCustomerPack', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateNumberWaybills $parameters
   * @return GenerateNumberWaybillsResponse
   */
  public function GenerateNumberWaybills(GenerateNumberWaybills $parameters) {
    return $this->__soapCall('GenerateNumberWaybills', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CreateCustomer $parameters
   * @return CreateCustomerResponse
   */
  public function CreateCustomer(CreateCustomer $parameters) {
    return $this->__soapCall('CreateCustomer', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GivePaymentType $parameters
   * @return GivePaymentTypeResponse
   */
  public function GivePaymentType(GivePaymentType $parameters) {
    return $this->__soapCall('GivePaymentType', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GivePartnerStatus $parameters
   * @return GivePartnerStatusResponse
   */
  public function GivePartnerStatus(GivePartnerStatus $parameters) {
    return $this->__soapCall('GivePartnerStatus', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPackList $parameters
   * @return GenerateLabelBusinessPackListResponse
   */
  public function GenerateLabelBusinessPackList(GenerateLabelBusinessPackList $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPackList', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPackListTwo $parameters
   * @return GenerateLabelBusinessPackListTwoResponse
   */
  public function GenerateLabelBusinessPackListTwo(GenerateLabelBusinessPackListTwo $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPackListTwo', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPack $parameters
   * @return GenerateLabelBusinessPackResponse
   */
  public function GenerateLabelBusinessPack(GenerateLabelBusinessPack $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPack', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPackTwo $parameters
   * @return GenerateLabelBusinessPackTwoResponse
   */
  public function GenerateLabelBusinessPackTwo(GenerateLabelBusinessPackTwo $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPackTwo', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetGeneratedParcels $parameters
   * @return GetGeneratedParcelsResponse
   */
  public function GetGeneratedParcels(GetGeneratedParcels $parameters) {
    return $this->__soapCall('GetGeneratedParcels', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateProtocol $parameters
   * @return GenerateProtocolResponse
   */
  public function GenerateProtocol(GenerateProtocol $parameters) {
    return $this->__soapCall('GenerateProtocol', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param LabelPrintDuplicateList $parameters
   * @return LabelPrintDuplicateListResponse
   */
  public function LabelPrintDuplicateList(LabelPrintDuplicateList $parameters) {
    return $this->__soapCall('LabelPrintDuplicateList', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param LabelPrintDuplicate $parameters
   * @return LabelPrintDuplicateResponse
   */
  public function LabelPrintDuplicate(LabelPrintDuplicate $parameters) {
    return $this->__soapCall('LabelPrintDuplicate', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateShippingCode $parameters
   * @return GenerateShippingCodeResponse
   */
  public function GenerateShippingCode(GenerateShippingCode $parameters) {
    return $this->__soapCall('GenerateShippingCode', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GiveShippingCodeStatus $parameters
   * @return GiveShippingCodeStatusResponse
   */
  public function GiveShippingCodeStatus(GiveShippingCodeStatus $parameters) {
    return $this->__soapCall('GiveShippingCodeStatus', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateShippingCodeRuch2Home $parameters
   * @return GenerateShippingCodeRuch2HomeResponse
   */
  public function GenerateShippingCodeRuch2Home(GenerateShippingCodeRuch2Home $parameters) {
    return $this->__soapCall('GenerateShippingCodeRuch2Home', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GenerateLabelBusinessPackRuch2Home $parameters
   * @return GenerateLabelBusinessPackRuch2HomeResponse
   */
  public function GenerateLabelBusinessPackRuch2Home(GenerateLabelBusinessPackRuch2Home $parameters) {
    return $this->__soapCall('GenerateLabelBusinessPackRuch2Home', array($parameters),       array(
            'uri' => 'https://91.242.220.103/WebServicePwR',
            'soapaction' => ''
           )
      );
  }

}

?>
