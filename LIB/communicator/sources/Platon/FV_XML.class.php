<?php
/**
 * Klasa obsługuje fakture od Ateneum
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Platon;

use Exception;
class FV_XML {

  /**
   * @var simplexml_load_file $aXML
   */
  private $aXML;
  
  /**
   * $sXMLFilePath
   * 
   * @param string $sXMLFilePath
   * @throws Exception
   */
  function __construct($sXMLFilePath) {
    if (file_exists($sXMLFilePath)) {
      $this->aXML = (array)\simplexml_load_file($sXMLFilePath, "SimpleXMLElement");
      
    } else {
      throw new Exception(_('Brak pliku XML do zaimportowania'));
    }
  }
  
  /**
   * Metoda wybiera numer faktury z dokumentu XML
   * 
   * @return string
   */
  public function getFVNumber() {
    $tmp = (array)$this->aXML;
    return (string)$tmp['Invoice-Header']->InvoiceNumber;
  }// end of getFVNumber() method
  
  
  /**
   * Metoda wybiera numer faktury z dokumentu XML
   * 
   * @return string
   */
  public function getOrderNumberNumber() {
    $tmp = (array)$this->aXML;
    $str = $tmp['Invoice-Header'];
    return (string)$str->Order->BuyerOrderNumber;
  }// end of getFVNumber() method
  
  
  /**
   * 
   * @return string
   */
  public function getOrderNumber() {
    
    $tmp = (array)$this->aXML;
    return (string)$tmp['Invoice-Header']->Order->BuyerOrderNumber;
  }
  
  /**
   * Metoda pobiera produkty w formacie zgodnym z eksportem DBF
   * 
   * @return array
   */
  public function getProducts() {
    $aDoc = (array)$this->aXML;
    
    foreach ($aDoc['Invoice-Lines']->Line as $oLine) {
      $aLineTMP = (array)$oLine;
      $aLineItem = (array)$aLineTMP['Line-Item'];
      
      
      $spNetto = (string)$aLineItem['InvoiceUnitNetPrice'];
      $aItem = array(
          'ean_13' => (string)$aLineItem['EAN'],
          'price_netto' => $spNetto,
          'discount' => 0,
          'vat' => (string)$aLineItem['TaxRate'],
          'source_item_id' => (string)$aLineItem['SupplierItemCode'],
          'quantity' => (string)$aLineItem['InvoiceQuantity'],
          
      );
      $aItems[] = $aItem;
    }
    return $aItems;
  }// end of getProducts() method
  
  /**
   * 
   * @return array
   */
  public function getEANCol() {
    $aDoc = (array)$this->aXML;
    
    $aItem = array();
    foreach ($aDoc['Invoice-Lines']->Line as $oLine) {
      $aLineTMP = (array)$oLine;
      $aLineItem = $aLineTMP['Line-Item'];
      
      $aItem[] = (string)$aLineItem['EAN'];
    }
    return $aItem;
  }
}
