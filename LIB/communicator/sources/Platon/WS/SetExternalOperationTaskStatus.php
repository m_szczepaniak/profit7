<?php

namespace communicator\sources\Platon\WS;

class SetExternalOperationTaskStatus
{

    /**
     * @var ConnectorInfo $ConnectorInfo
     */
    protected $ConnectorInfo = null;

    /**
     * @var ExternalOperationStatus $NewStatus
     */
    protected $NewStatus = null;

    /**
     * @var ExternalOperationStatus $OldStatus
     */
    protected $OldStatus = null;

    /**
     * @param ConnectorInfo $ConnectorInfo
     * @param ExternalOperationStatus $NewStatus
     * @param ExternalOperationStatus $OldStatus
     */
    public function __construct($ConnectorInfo, $NewStatus, $OldStatus)
    {
      $this->ConnectorInfo = $ConnectorInfo;
      $this->NewStatus = $NewStatus;
      $this->OldStatus = $OldStatus;
    }

    /**
     * @return ConnectorInfo
     */
    public function getConnectorInfo()
    {
      return $this->ConnectorInfo;
    }

    /**
     * @param ConnectorInfo $ConnectorInfo
     * @return \communicator\sources\Platon\WS\SetExternalOperationTaskStatus
     */
    public function setConnectorInfo($ConnectorInfo)
    {
      $this->ConnectorInfo = $ConnectorInfo;
      return $this;
    }

    /**
     * @return ExternalOperationStatus
     */
    public function getNewStatus()
    {
      return $this->NewStatus;
    }

    /**
     * @param ExternalOperationStatus $NewStatus
     * @return \communicator\sources\Platon\WS\SetExternalOperationTaskStatus
     */
    public function setNewStatus($NewStatus)
    {
      $this->NewStatus = $NewStatus;
      return $this;
    }

    /**
     * @return ExternalOperationStatus
     */
    public function getOldStatus()
    {
      return $this->OldStatus;
    }

    /**
     * @param ExternalOperationStatus $OldStatus
     * @return \communicator\sources\Platon\WS\SetExternalOperationTaskStatus
     */
    public function setOldStatus($OldStatus)
    {
      $this->OldStatus = $OldStatus;
      return $this;
    }

}
