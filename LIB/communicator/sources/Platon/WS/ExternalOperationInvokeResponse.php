<?php

namespace communicator\sources\Platon\WS;

class ExternalOperationInvokeResponse
{

    /**
     * @var string $ExternalOperationInvokeResult
     */
    protected $ExternalOperationInvokeResult = null;

    /**
     * @param string $ExternalOperationInvokeResult
     */
    public function __construct($ExternalOperationInvokeResult)
    {
      $this->ExternalOperationInvokeResult = $ExternalOperationInvokeResult;
    }

    /**
     * @return string
     */
    public function getExternalOperationInvokeResult()
    {
      return $this->ExternalOperationInvokeResult;
    }

    /**
     * @param string $ExternalOperationInvokeResult
     * @return \communicator\sources\Platon\WS\ExternalOperationInvokeResponse
     */
    public function setExternalOperationInvokeResult($ExternalOperationInvokeResult)
    {
      $this->ExternalOperationInvokeResult = $ExternalOperationInvokeResult;
      return $this;
    }

}
