<?php

namespace communicator\sources\Platon\WS;

class SendConnectorResult
{

    /**
     * @var ConnectorResult $ConnectorResult
     */
    protected $ConnectorResult = null;

    /**
     * @param ConnectorResult $ConnectorResult
     */
    public function __construct($ConnectorResult)
    {
      $this->ConnectorResult = $ConnectorResult;
    }

    /**
     * @return ConnectorResult
     */
    public function getConnectorResult()
    {
      return $this->ConnectorResult;
    }

    /**
     * @param ConnectorResult $ConnectorResult
     * @return \communicator\sources\Platon\WS\SendConnectorResult
     */
    public function setConnectorResult($ConnectorResult)
    {
      $this->ConnectorResult = $ConnectorResult;
      return $this;
    }

}
