<?php


 function autoload_87fcc3b8aaf52c1e180558cee052f078($class)
{
    $classes = array(
        'communicator\sources\Platon\WS\CommS_WCF_TransferService' => __DIR__ .'/CommS_WCF_TransferService.php',
        'communicator\sources\Platon\WS\SVLLoginInfo' => __DIR__ .'/SVLLoginInfo.php',
        'communicator\sources\Platon\WS\RemoteFileInfo' => __DIR__ .'/RemoteFileInfo.php',
        'communicator\sources\Platon\WS\csStructureInfo' => __DIR__ .'/csStructureInfo.php',
        'communicator\sources\Platon\WS\ConnectorInfo' => __DIR__ .'/ConnectorInfo.php',
        'communicator\sources\Platon\WS\ExternalOperationStatus' => __DIR__ .'/ExternalOperationStatus.php',
        'communicator\sources\Platon\WS\ConnectorTaskInfo' => __DIR__ .'/ConnectorTaskInfo.php',
        'communicator\sources\Platon\WS\ConnectorTaskKind' => __DIR__ .'/ConnectorTaskKind.php',
        'communicator\sources\Platon\WS\ConnectorResult' => __DIR__ .'/ConnectorResult.php',
        'communicator\sources\Platon\WS\UploadFile' => __DIR__ .'/UploadFile.php',
        'communicator\sources\Platon\WS\UploadFileResponse' => __DIR__ .'/UploadFileResponse.php',
        'communicator\sources\Platon\WS\GetFolderStructure' => __DIR__ .'/GetFolderStructure.php',
        'communicator\sources\Platon\WS\GetFolderStructureResponse' => __DIR__ .'/GetFolderStructureResponse.php',
        'communicator\sources\Platon\WS\ExternalOperationInvoke' => __DIR__ .'/ExternalOperationInvoke.php',
        'communicator\sources\Platon\WS\ExternalOperationInvokeResponse' => __DIR__ .'/ExternalOperationInvokeResponse.php',
        'communicator\sources\Platon\WS\GetConnectorTask' => __DIR__ .'/GetConnectorTask.php',
        'communicator\sources\Platon\WS\GetConnectorTaskResponse' => __DIR__ .'/GetConnectorTaskResponse.php',
        'communicator\sources\Platon\WS\SetExternalOperationTaskStatus' => __DIR__ .'/SetExternalOperationTaskStatus.php',
        'communicator\sources\Platon\WS\SetExternalOperationTaskStatusResponse' => __DIR__ .'/SetExternalOperationTaskStatusResponse.php',
        'communicator\sources\Platon\WS\SendConnectorResult' => __DIR__ .'/SendConnectorResult.php',
        'communicator\sources\Platon\WS\SendConnectorResultResponse' => __DIR__ .'/SendConnectorResultResponse.php',
        'communicator\sources\Platon\WS\RegisterConnector' => __DIR__ .'/RegisterConnector.php',
        'communicator\sources\Platon\WS\RegisterConnectorResponse' => __DIR__ .'/RegisterConnectorResponse.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_87fcc3b8aaf52c1e180558cee052f078');

// Do nothing. The rest is just leftovers from the code generation.
{
}
