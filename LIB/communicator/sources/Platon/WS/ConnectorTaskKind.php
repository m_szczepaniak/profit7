<?php

namespace communicator\sources\Platon\WS;

class ConnectorTaskKind
{
    const __default = 'ExecSqlStatement';
    const ExecSqlStatement = 'ExecSqlStatement';
    const SaveFile = 'SaveFile';
    const ReportGenerate = 'ReportGenerate';
    const DownloadFiles = 'DownloadFiles';
    const TransformFile = 'TransformFile';
    const MergeImages = 'MergeImages';
    const Fiscalization = 'Fiscalization';
    const PerformImageActions = 'PerformImageActions';
    const SignDocument = 'SignDocument';
    const SignDocumentFeedback = 'SignDocumentFeedback';


}
