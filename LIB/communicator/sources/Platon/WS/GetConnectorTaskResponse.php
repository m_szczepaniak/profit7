<?php

namespace communicator\sources\Platon\WS;

class GetConnectorTaskResponse
{

    /**
     * @var ConnectorTaskInfo $GetConnectorTaskResult
     */
    protected $GetConnectorTaskResult = null;

    /**
     * @param ConnectorTaskInfo $GetConnectorTaskResult
     */
    public function __construct($GetConnectorTaskResult)
    {
      $this->GetConnectorTaskResult = $GetConnectorTaskResult;
    }

    /**
     * @return ConnectorTaskInfo
     */
    public function getGetConnectorTaskResult()
    {
      return $this->GetConnectorTaskResult;
    }

    /**
     * @param ConnectorTaskInfo $GetConnectorTaskResult
     * @return \communicator\sources\Platon\WS\GetConnectorTaskResponse
     */
    public function setGetConnectorTaskResult($GetConnectorTaskResult)
    {
      $this->GetConnectorTaskResult = $GetConnectorTaskResult;
      return $this;
    }

}
