<?php

namespace communicator\sources\Platon\WS;

class GetConnectorTask
{

    /**
     * @var ConnectorInfo $ConnectorInfo
     */
    protected $ConnectorInfo = null;

    /**
     * @param ConnectorInfo $ConnectorInfo
     */
    public function __construct($ConnectorInfo)
    {
      $this->ConnectorInfo = $ConnectorInfo;
    }

    /**
     * @return ConnectorInfo
     */
    public function getConnectorInfo()
    {
      return $this->ConnectorInfo;
    }

    /**
     * @param ConnectorInfo $ConnectorInfo
     * @return \communicator\sources\Platon\WS\GetConnectorTask
     */
    public function setConnectorInfo($ConnectorInfo)
    {
      $this->ConnectorInfo = $ConnectorInfo;
      return $this;
    }

}
