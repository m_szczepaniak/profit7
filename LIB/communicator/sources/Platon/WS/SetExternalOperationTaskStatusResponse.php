<?php

namespace communicator\sources\Platon\WS;

class SetExternalOperationTaskStatusResponse
{

    /**
     * @var boolean $SetExternalOperationTaskStatusResult
     */
    protected $SetExternalOperationTaskStatusResult = null;

    /**
     * @param boolean $SetExternalOperationTaskStatusResult
     */
    public function __construct($SetExternalOperationTaskStatusResult)
    {
      $this->SetExternalOperationTaskStatusResult = $SetExternalOperationTaskStatusResult;
    }

    /**
     * @return boolean
     */
    public function getSetExternalOperationTaskStatusResult()
    {
      return $this->SetExternalOperationTaskStatusResult;
    }

    /**
     * @param boolean $SetExternalOperationTaskStatusResult
     * @return \communicator\sources\Platon\WS\SetExternalOperationTaskStatusResponse
     */
    public function setSetExternalOperationTaskStatusResult($SetExternalOperationTaskStatusResult)
    {
      $this->SetExternalOperationTaskStatusResult = $SetExternalOperationTaskStatusResult;
      return $this;
    }

}
