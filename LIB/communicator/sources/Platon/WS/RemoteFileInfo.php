<?php

namespace communicator\sources\Platon\WS;

class RemoteFileInfo
{

    /**
     * @var string $CompanyIdent
     */
    protected $CompanyIdent = null;

    /**
     * @var string $DirecotoryName
     */
    protected $DirecotoryName = null;

    /**
     * @var base64Binary $FileByteStream
     */
    protected $FileByteStream = null;

    /**
     * @var string $FileIdent
     */
    protected $FileIdent = null;

    /**
     * @var string $FileName
     */
    protected $FileName = null;

    /**
     * @var string $FolderId
     */
    protected $FolderId = null;

    /**
     * @var string $InvokerUUID
     */
    protected $InvokerUUID = null;

    /**
     * @var boolean $IsRemote
     */
    protected $IsRemote = null;

    /**
     * @var int $Length
     */
    protected $Length = null;

    /**
     * @var string $RemoteFileNamePath
     */
    protected $RemoteFileNamePath = null;

    /**
     * @param boolean $IsRemote
     * @param int $Length
     */
    public function __construct($IsRemote, $Length)
    {
      $this->IsRemote = $IsRemote;
      $this->Length = $Length;
    }

    /**
     * @return string
     */
    public function getCompanyIdent()
    {
      return $this->CompanyIdent;
    }

    /**
     * @param string $CompanyIdent
     * @return \communicator\sources\Platon\WS\RemoteFileInfo
     */
    public function setCompanyIdent($CompanyIdent)
    {
      $this->CompanyIdent = $CompanyIdent;
      return $this;
    }

    /**
     * @return string
     */
    public function getDirecotoryName()
    {
      return $this->DirecotoryName;
    }

    /**
     * @param string $DirecotoryName
     * @return \communicator\sources\Platon\WS\RemoteFileInfo
     */
    public function setDirecotoryName($DirecotoryName)
    {
      $this->DirecotoryName = $DirecotoryName;
      return $this;
    }

    /**
     * @return base64Binary
     */
    public function getFileByteStream()
    {
      return $this->FileByteStream;
    }

    /**
     * @param base64Binary $FileByteStream
     * @return \communicator\sources\Platon\WS\RemoteFileInfo
     */
    public function setFileByteStream($FileByteStream)
    {
      $this->FileByteStream = $FileByteStream;
      return $this;
    }

    /**
     * @return string
     */
    public function getFileIdent()
    {
      return $this->FileIdent;
    }

    /**
     * @param string $FileIdent
     * @return \communicator\sources\Platon\WS\RemoteFileInfo
     */
    public function setFileIdent($FileIdent)
    {
      $this->FileIdent = $FileIdent;
      return $this;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
      return $this->FileName;
    }

    /**
     * @param string $FileName
     * @return \communicator\sources\Platon\WS\RemoteFileInfo
     */
    public function setFileName($FileName)
    {
      $this->FileName = $FileName;
      return $this;
    }

    /**
     * @return string
     */
    public function getFolderId()
    {
      return $this->FolderId;
    }

    /**
     * @param string $FolderId
     * @return \communicator\sources\Platon\WS\RemoteFileInfo
     */
    public function setFolderId($FolderId)
    {
      $this->FolderId = $FolderId;
      return $this;
    }

    /**
     * @return string
     */
    public function getInvokerUUID()
    {
      return $this->InvokerUUID;
    }

    /**
     * @param string $InvokerUUID
     * @return \communicator\sources\Platon\WS\RemoteFileInfo
     */
    public function setInvokerUUID($InvokerUUID)
    {
      $this->InvokerUUID = $InvokerUUID;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getIsRemote()
    {
      return $this->IsRemote;
    }

    /**
     * @param boolean $IsRemote
     * @return \communicator\sources\Platon\WS\RemoteFileInfo
     */
    public function setIsRemote($IsRemote)
    {
      $this->IsRemote = $IsRemote;
      return $this;
    }

    /**
     * @return int
     */
    public function getLength()
    {
      return $this->Length;
    }

    /**
     * @param int $Length
     * @return \communicator\sources\Platon\WS\RemoteFileInfo
     */
    public function setLength($Length)
    {
      $this->Length = $Length;
      return $this;
    }

    /**
     * @return string
     */
    public function getRemoteFileNamePath()
    {
      return $this->RemoteFileNamePath;
    }

    /**
     * @param string $RemoteFileNamePath
     * @return \communicator\sources\Platon\WS\RemoteFileInfo
     */
    public function setRemoteFileNamePath($RemoteFileNamePath)
    {
      $this->RemoteFileNamePath = $RemoteFileNamePath;
      return $this;
    }

}
