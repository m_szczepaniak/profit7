<?php

namespace communicator\sources\Platon\WS\Data;

use DateTime;

/**
 * Class representing DocumentOrder
 */
class DocumentOrder
{

  private $sNewOrderNumber;
  private $aAccessData;
  private $aSellerData;
  private $aBooksList;

  public function __construct($aBooksList, $aSellerData, $aAccessData, $sNewOrderNumber) {
    
    
    $this->aBooksList = $aBooksList;
    $this->aSellerData = $aSellerData;
    $this->aAccessData = $aAccessData;
    $this->sNewOrderNumber = $sNewOrderNumber;
  }
  
  public function getDocumentOrder() {
    $aDO['Order-Header'] = $this->getOrderHeader();
    $aDO['Order-Parties'] = $this->getOrderParties();
    $aDO['Order-Lines'] = $this->getOrderLines();
    return $aDO;
  }
  
  
  /**
   * 
   * @return string
   */
  public function getXMLInfo() {
    
    $s = '<ExternalOperationInfo>
        <OperationIdent>ORDER</OperationIdent>
        <CompanyGuid>10a60cd5-8850-4dc4-b9cf-1938eb42d1a8</CompanyGuid>
        <TransactionIdent>4536d117-fc70-4c0e-a507-c8cd5bb5b999</TransactionIdent>
        <UserName>'.$this->aAccessData['login'].'</UserName>
        <Password>'.$this->aAccessData['passw'].'</Password>
      </ExternalOperationInfo>';
    return $s;
  }
  
  /**
   * 
   * @param string $sOrderBase64
   * @return string
   */
  public function getXMLParams($sOrderBase64) {
    $s = '<ExternalOperationParams>
        <Params>
          '.base64_encode($sOrderBase64).'
        </Params>
      </ExternalOperationParams>';
    return $s;
  }
  
  private function getOrderHeader() {
    $data = new DateTime();
    
    $aHeaders = [];
    $aHeaders['OrderNumber'] = $this->sNewOrderNumber;
    $aHeaders['OrderDate'] = (string)$data->format("Y-m-d");
    $aHeaders['DocumentFunctionCode'] = 'O';
    $aHeaders['csOrderPriceType'] = 'N';
    $aHeaders['Remarks'] = '';
    $aHeaders['OrderCurrency'] = 'PLN';
    $aHeaders['Payment'] = $this->getPayment();
    return $aHeaders;
  }
  
  private function getPayment() {
    
    $aPayment['PaymentTerms'] = $this->PaymentTerms();
    return $aPayment;
  }
  
  private function PaymentTerms() {
    $data = new DateTime();
    
    $aPaymentTerms = [
        'PaymentDescription' => 'Przelew',
        'PaymentDate' => (string)$data->format("Y-m-d")
    ];
    return $aPaymentTerms;
  }
  
  private function getOrderParties() {
    $aOP = [
        'Buyer' => $this->getBuyer(),
        'Invoicee' => $this->getInvoicee()
    ];
    return $aOP;
  }
  
  private function getBuyer() {
    
    $aBuyer = [
        'TaxID' => $this->aSellerData['nip'],
        'AccountNumber' => $this->aAccessData['AccountNumber'],
        'Name' => $this->aSellerData['invoice_name'],
        'StreetAndNumber' => $this->aSellerData['street'],
        'CityName' => $this->aSellerData['city'],
        'PostalCode' => $this->aSellerData['postal'],
        'Country' => 'PL',
    ];
    return $aBuyer;
  }
  
  private function getInvoicee() {
    
    $aInvoicee = [
        'TaxID' => $this->aSellerData['nip'],
        'AccountNumber' => $this->aAccessData['AccountNumber'],
        'Name' => $this->aSellerData['invoice_name'],
        'StreetAndNumber' => $this->aSellerData['street'],
        'CityName' => $this->aSellerData['city'],
        'PostalCode' => $this->aSellerData['postal'],
        'Country' => 'PL',
    ];
    return $aInvoicee;
  }
  
  private function getOrderLines() {
    return  $this->getLine();
  }
  
  private function getLine() {
    $a['Line'] = $this->getLineItem();
    return $a;
  }
  
  /**
   * 
   * @return array
   */
  private function getLineItem() {
    
    $aBooks = [];
    foreach ($this->aBooksList as $iKey => $aBook) {
      $aBooks[]['Line-Item'] = [
          'LineNumber' => ($iKey+1),
          'SupplierItemCode' => $aBook['source_item_id'],
          'OrderedQuantity' => $aBook['quantity']
      ];
    }
    return $aBooks;
  }
}

