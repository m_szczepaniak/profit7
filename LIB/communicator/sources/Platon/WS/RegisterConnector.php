<?php

namespace communicator\sources\Platon\WS;

class RegisterConnector
{

    /**
     * @var ConnectorInfo $ConnectorInfo
     */
    protected $ConnectorInfo = null;

    /**
     * @var string $ConnectorHardwareInformation
     */
    protected $ConnectorHardwareInformation = null;

    /**
     * @param ConnectorInfo $ConnectorInfo
     * @param string $ConnectorHardwareInformation
     */
    public function __construct($ConnectorInfo, $ConnectorHardwareInformation)
    {
      $this->ConnectorInfo = $ConnectorInfo;
      $this->ConnectorHardwareInformation = $ConnectorHardwareInformation;
    }

    /**
     * @return ConnectorInfo
     */
    public function getConnectorInfo()
    {
      return $this->ConnectorInfo;
    }

    /**
     * @param ConnectorInfo $ConnectorInfo
     * @return \communicator\sources\Platon\WS\RegisterConnector
     */
    public function setConnectorInfo($ConnectorInfo)
    {
      $this->ConnectorInfo = $ConnectorInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getConnectorHardwareInformation()
    {
      return $this->ConnectorHardwareInformation;
    }

    /**
     * @param string $ConnectorHardwareInformation
     * @return \communicator\sources\Platon\WS\RegisterConnector
     */
    public function setConnectorHardwareInformation($ConnectorHardwareInformation)
    {
      $this->ConnectorHardwareInformation = $ConnectorHardwareInformation;
      return $this;
    }

}
