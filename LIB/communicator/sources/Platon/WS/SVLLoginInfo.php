<?php

namespace communicator\sources\Platon\WS;

class SVLLoginInfo
{

    /**
     * @var string $CSIdent
     */
    protected $CSIdent = null;

    /**
     * @var string $CompanyGuid
     */
    protected $CompanyGuid = null;

    /**
     * @var int $IsPE
     */
    protected $IsPE = null;

    /**
     * @var string $NP
     */
    protected $NP = null;

    /**
     * @var string $Password
     */
    protected $Password = null;

    /**
     * @var string $RP
     */
    protected $RP = null;

    /**
     * @var string $SecurityModelResponse
     */
    protected $SecurityModelResponse = null;

    /**
     * @var string $UserName
     */
    protected $UserName = null;

    /**
     * @param int $IsPE
     */
    public function __construct($IsPE)
    {
      $this->IsPE = $IsPE;
    }

    /**
     * @return string
     */
    public function getCSIdent()
    {
      return $this->CSIdent;
    }

    /**
     * @param string $CSIdent
     * @return \communicator\sources\Platon\WS\SVLLoginInfo
     */
    public function setCSIdent($CSIdent)
    {
      $this->CSIdent = $CSIdent;
      return $this;
    }

    /**
     * @return string
     */
    public function getCompanyGuid()
    {
      return $this->CompanyGuid;
    }

    /**
     * @param string $CompanyGuid
     * @return \communicator\sources\Platon\WS\SVLLoginInfo
     */
    public function setCompanyGuid($CompanyGuid)
    {
      $this->CompanyGuid = $CompanyGuid;
      return $this;
    }

    /**
     * @return int
     */
    public function getIsPE()
    {
      return $this->IsPE;
    }

    /**
     * @param int $IsPE
     * @return \communicator\sources\Platon\WS\SVLLoginInfo
     */
    public function setIsPE($IsPE)
    {
      $this->IsPE = $IsPE;
      return $this;
    }

    /**
     * @return string
     */
    public function getNP()
    {
      return $this->NP;
    }

    /**
     * @param string $NP
     * @return \communicator\sources\Platon\WS\SVLLoginInfo
     */
    public function setNP($NP)
    {
      $this->NP = $NP;
      return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
      return $this->Password;
    }

    /**
     * @param string $Password
     * @return \communicator\sources\Platon\WS\SVLLoginInfo
     */
    public function setPassword($Password)
    {
      $this->Password = $Password;
      return $this;
    }

    /**
     * @return string
     */
    public function getRP()
    {
      return $this->RP;
    }

    /**
     * @param string $RP
     * @return \communicator\sources\Platon\WS\SVLLoginInfo
     */
    public function setRP($RP)
    {
      $this->RP = $RP;
      return $this;
    }

    /**
     * @return string
     */
    public function getSecurityModelResponse()
    {
      return $this->SecurityModelResponse;
    }

    /**
     * @param string $SecurityModelResponse
     * @return \communicator\sources\Platon\WS\SVLLoginInfo
     */
    public function setSecurityModelResponse($SecurityModelResponse)
    {
      $this->SecurityModelResponse = $SecurityModelResponse;
      return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
      return $this->UserName;
    }

    /**
     * @param string $UserName
     * @return \communicator\sources\Platon\WS\SVLLoginInfo
     */
    public function setUserName($UserName)
    {
      $this->UserName = $UserName;
      return $this;
    }

}
