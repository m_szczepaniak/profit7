<?php

namespace communicator\sources\Platon\WS;

class CommS_WCF_TransferService extends \SoapClient
{

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
  'SVLLoginInfo' => 'communicator\\sources\\Platon\\WS\\SVLLoginInfo',
  'RemoteFileInfo' => 'communicator\\sources\\Platon\\WS\\RemoteFileInfo',
  'csStructureInfo' => 'communicator\\sources\\Platon\\WS\\csStructureInfo',
  'ConnectorInfo' => 'communicator\\sources\\Platon\\WS\\ConnectorInfo',
  'ConnectorTaskInfo' => 'communicator\\sources\\Platon\\WS\\ConnectorTaskInfo',
  'ConnectorResult' => 'communicator\\sources\\Platon\\WS\\ConnectorResult',
  'UploadFile' => 'communicator\\sources\\Platon\\WS\\UploadFile',
  'UploadFileResponse' => 'communicator\\sources\\Platon\\WS\\UploadFileResponse',
  'GetFolderStructure' => 'communicator\\sources\\Platon\\WS\\GetFolderStructure',
  'GetFolderStructureResponse' => 'communicator\\sources\\Platon\\WS\\GetFolderStructureResponse',
  'ExternalOperationInvoke' => 'communicator\\sources\\Platon\\WS\\ExternalOperationInvoke',
  'ExternalOperationInvokeResponse' => 'communicator\\sources\\Platon\\WS\\ExternalOperationInvokeResponse',
  'GetConnectorTask' => 'communicator\\sources\\Platon\\WS\\GetConnectorTask',
  'GetConnectorTaskResponse' => 'communicator\\sources\\Platon\\WS\\GetConnectorTaskResponse',
  'SetExternalOperationTaskStatus' => 'communicator\\sources\\Platon\\WS\\SetExternalOperationTaskStatus',
  'SetExternalOperationTaskStatusResponse' => 'communicator\\sources\\Platon\\WS\\SetExternalOperationTaskStatusResponse',
  'SendConnectorResult' => 'communicator\\sources\\Platon\\WS\\SendConnectorResult',
  'SendConnectorResultResponse' => 'communicator\\sources\\Platon\\WS\\SendConnectorResultResponse',
  'RegisterConnector' => 'communicator\\sources\\Platon\\WS\\RegisterConnector',
  'RegisterConnectorResponse' => 'communicator\\sources\\Platon\\WS\\RegisterConnectorResponse',
);

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = 'https://platon.com.pl/uPlatonWebApiOrders/CommS_WCF_TransferService.svc?wsdl')
    {
      foreach (self::$classmap as $key => $value) {
        if (!isset($options['classmap'][$key])) {
          $options['classmap'][$key] = $value;
        }
      }
      $options = array_merge(array (
          'trace' => 1, 'soap_version' => SOAP_1_1,
  'features' => 1,
), $options);
      parent::__construct($wsdl, $options);
    }

    /**
     * @param UploadFile $parameters
     * @return UploadFileResponse
     */
    public function UploadFile(UploadFile $parameters)
    {
      return $this->__soapCall('UploadFile', array($parameters));
    }

    /**
     * @param GetFolderStructure $parameters
     * @return GetFolderStructureResponse
     */
    public function GetFolderStructure(GetFolderStructure $parameters)
    {
      return $this->__soapCall('GetFolderStructure', array($parameters));
    }

    /**
     * @param ExternalOperationInvoke $parameters
     * @return ExternalOperationInvokeResponse
     */
    public function ExternalOperationInvoke(ExternalOperationInvoke $parameters)
    {
      return $this->__soapCall('ExternalOperationInvoke', array($parameters));
    }

    /**
     * @param GetConnectorTask $parameters
     * @return GetConnectorTaskResponse
     */
    public function GetConnectorTask(GetConnectorTask $parameters)
    {
      return $this->__soapCall('GetConnectorTask', array($parameters));
    }

    /**
     * @param SetExternalOperationTaskStatus $parameters
     * @return SetExternalOperationTaskStatusResponse
     */
    public function SetExternalOperationTaskStatus(SetExternalOperationTaskStatus $parameters)
    {
      return $this->__soapCall('SetExternalOperationTaskStatus', array($parameters));
    }

    /**
     * @param SendConnectorResult $parameters
     * @return SendConnectorResultResponse
     */
    public function SendConnectorResult(SendConnectorResult $parameters)
    {
      return $this->__soapCall('SendConnectorResult', array($parameters));
    }

    /**
     * @param RegisterConnector $parameters
     * @return RegisterConnectorResponse
     */
    public function RegisterConnector(RegisterConnector $parameters)
    {
      return $this->__soapCall('RegisterConnector', array($parameters));
    }

}
