<?php

namespace communicator\sources\Platon\WS;

class GetFolderStructure
{

    /**
     * @var SVLLoginInfo $LoginInfo
     */
    protected $LoginInfo = null;

    /**
     * @param SVLLoginInfo $LoginInfo
     */
    public function __construct($LoginInfo)
    {
      $this->LoginInfo = $LoginInfo;
    }

    /**
     * @return SVLLoginInfo
     */
    public function getLoginInfo()
    {
      return $this->LoginInfo;
    }

    /**
     * @param SVLLoginInfo $LoginInfo
     * @return \communicator\sources\Platon\WS\GetFolderStructure
     */
    public function setLoginInfo($LoginInfo)
    {
      $this->LoginInfo = $LoginInfo;
      return $this;
    }

}
