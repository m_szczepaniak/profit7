<?php

namespace communicator\sources\Platon\WS;

class RegisterConnectorResponse
{

    /**
     * @var ConnectorTaskInfo $RegisterConnectorResult
     */
    protected $RegisterConnectorResult = null;

    /**
     * @param ConnectorTaskInfo $RegisterConnectorResult
     */
    public function __construct($RegisterConnectorResult)
    {
      $this->RegisterConnectorResult = $RegisterConnectorResult;
    }

    /**
     * @return ConnectorTaskInfo
     */
    public function getRegisterConnectorResult()
    {
      return $this->RegisterConnectorResult;
    }

    /**
     * @param ConnectorTaskInfo $RegisterConnectorResult
     * @return \communicator\sources\Platon\WS\RegisterConnectorResponse
     */
    public function setRegisterConnectorResult($RegisterConnectorResult)
    {
      $this->RegisterConnectorResult = $RegisterConnectorResult;
      return $this;
    }

}
