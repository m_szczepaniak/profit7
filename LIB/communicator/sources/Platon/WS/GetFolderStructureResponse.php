<?php

namespace communicator\sources\Platon\WS;

class GetFolderStructureResponse
{

    /**
     * @var csStructureInfo $GetFolderStructureResult
     */
    protected $GetFolderStructureResult = null;

    /**
     * @param csStructureInfo $GetFolderStructureResult
     */
    public function __construct($GetFolderStructureResult)
    {
      $this->GetFolderStructureResult = $GetFolderStructureResult;
    }

    /**
     * @return csStructureInfo
     */
    public function getGetFolderStructureResult()
    {
      return $this->GetFolderStructureResult;
    }

    /**
     * @param csStructureInfo $GetFolderStructureResult
     * @return \communicator\sources\Platon\WS\GetFolderStructureResponse
     */
    public function setGetFolderStructureResult($GetFolderStructureResult)
    {
      $this->GetFolderStructureResult = $GetFolderStructureResult;
      return $this;
    }

}
