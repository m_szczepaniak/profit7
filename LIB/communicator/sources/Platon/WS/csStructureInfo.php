<?php

namespace communicator\sources\Platon\WS;

class csStructureInfo
{

    /**
     * @var string $Base64XML
     */
    protected $Base64XML = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string
     */
    public function getBase64XML()
    {
      return $this->Base64XML;
    }

    /**
     * @param string $Base64XML
     * @return \communicator\sources\Platon\WS\csStructureInfo
     */
    public function setBase64XML($Base64XML)
    {
      $this->Base64XML = $Base64XML;
      return $this;
    }

}
