<?php

namespace communicator\sources\Platon\WS;

class UploadFile
{

    /**
     * @var SVLLoginInfo $aLoginInfo
     */
    protected $aLoginInfo = null;

    /**
     * @var RemoteFileInfo $Request
     */
    protected $Request = null;

    /**
     * @param SVLLoginInfo $aLoginInfo
     * @param RemoteFileInfo $Request
     */
    public function __construct($aLoginInfo, $Request)
    {
      $this->aLoginInfo = $aLoginInfo;
      $this->Request = $Request;
    }

    /**
     * @return SVLLoginInfo
     */
    public function getALoginInfo()
    {
      return $this->aLoginInfo;
    }

    /**
     * @param SVLLoginInfo $aLoginInfo
     * @return \communicator\sources\Platon\WS\UploadFile
     */
    public function setALoginInfo($aLoginInfo)
    {
      $this->aLoginInfo = $aLoginInfo;
      return $this;
    }

    /**
     * @return RemoteFileInfo
     */
    public function getRequest()
    {
      return $this->Request;
    }

    /**
     * @param RemoteFileInfo $Request
     * @return \communicator\sources\Platon\WS\UploadFile
     */
    public function setRequest($Request)
    {
      $this->Request = $Request;
      return $this;
    }

}
