<?php

namespace communicator\sources\Platon\WS;

class ConnectorInfo
{

    /**
     * @var guid $ConnectorIdent
     */
    protected $ConnectorIdent = null;

    /**
     * @var ExternalOperationStatus $ConnectorTaskStatus
     */
    protected $ConnectorTaskStatus = null;

    /**
     * @var int $ConnectorTimeOut
     */
    protected $ConnectorTimeOut = null;

    /**
     * @var guid $TaskGuid
     */
    protected $TaskGuid = null;

    /**
     * @param guid $ConnectorIdent
     * @param ExternalOperationStatus $ConnectorTaskStatus
     * @param int $ConnectorTimeOut
     * @param guid $TaskGuid
     */
    public function __construct($ConnectorIdent, $ConnectorTaskStatus, $ConnectorTimeOut, $TaskGuid)
    {
      $this->ConnectorIdent = $ConnectorIdent;
      $this->ConnectorTaskStatus = $ConnectorTaskStatus;
      $this->ConnectorTimeOut = $ConnectorTimeOut;
      $this->TaskGuid = $TaskGuid;
    }

    /**
     * @return guid
     */
    public function getConnectorIdent()
    {
      return $this->ConnectorIdent;
    }

    /**
     * @param guid $ConnectorIdent
     * @return \communicator\sources\Platon\WS\ConnectorInfo
     */
    public function setConnectorIdent($ConnectorIdent)
    {
      $this->ConnectorIdent = $ConnectorIdent;
      return $this;
    }

    /**
     * @return ExternalOperationStatus
     */
    public function getConnectorTaskStatus()
    {
      return $this->ConnectorTaskStatus;
    }

    /**
     * @param ExternalOperationStatus $ConnectorTaskStatus
     * @return \communicator\sources\Platon\WS\ConnectorInfo
     */
    public function setConnectorTaskStatus($ConnectorTaskStatus)
    {
      $this->ConnectorTaskStatus = $ConnectorTaskStatus;
      return $this;
    }

    /**
     * @return int
     */
    public function getConnectorTimeOut()
    {
      return $this->ConnectorTimeOut;
    }

    /**
     * @param int $ConnectorTimeOut
     * @return \communicator\sources\Platon\WS\ConnectorInfo
     */
    public function setConnectorTimeOut($ConnectorTimeOut)
    {
      $this->ConnectorTimeOut = $ConnectorTimeOut;
      return $this;
    }

    /**
     * @return guid
     */
    public function getTaskGuid()
    {
      return $this->TaskGuid;
    }

    /**
     * @param guid $TaskGuid
     * @return \communicator\sources\Platon\WS\ConnectorInfo
     */
    public function setTaskGuid($TaskGuid)
    {
      $this->TaskGuid = $TaskGuid;
      return $this;
    }

}
