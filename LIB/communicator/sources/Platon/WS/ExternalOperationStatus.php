<?php

namespace communicator\sources\Platon\WS;

class ExternalOperationStatus
{
    const __default = 'ConnectorIsWaitingForTask';
    const ConnectorIsWaitingForTask = 'ConnectorIsWaitingForTask';
    const PreparingTaskForConnector = 'PreparingTaskForConnector';
    const TaskIsWaitingForConnector = 'TaskIsWaitingForConnector';
    const TaskIsAssignedToConnector = 'TaskIsAssignedToConnector';
    const ConnectorIsGettingTask = 'ConnectorIsGettingTask';
    const ConnectorIsProcessingTask = 'ConnectorIsProcessingTask';
    const ConnectorCompletedTask = 'ConnectorCompletedTask';
    const ConnectorResultTransformed = 'ConnectorResultTransformed';
    const OperationCompleted = 'OperationCompleted';
    const ErrorOccured = 'ErrorOccured';


}
