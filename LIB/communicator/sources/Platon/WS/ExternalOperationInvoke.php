<?php

namespace communicator\sources\Platon\WS;

class ExternalOperationInvoke
{

    /**
     * @var string $OperationInfo
     */
    protected $OperationInfo = null;

    /**
     * @var string $OperationParams
     */
    protected $OperationParams = null;

    /**
     * @param string $OperationInfo
     * @param string $OperationParams
     */
    public function __construct($OperationInfo, $OperationParams)
    {
      $this->OperationInfo = $OperationInfo;
      $this->OperationParams = $OperationParams;
    }

    /**
     * @return string
     */
    public function getOperationInfo()
    {
      return $this->OperationInfo;
    }

    /**
     * @param string $OperationInfo
     * @return \communicator\sources\Platon\WS\ExternalOperationInvoke
     */
    public function setOperationInfo($OperationInfo)
    {
      $this->OperationInfo = $OperationInfo;
      return $this;
    }

    /**
     * @return string
     */
    public function getOperationParams()
    {
      return $this->OperationParams;
    }

    /**
     * @param string $OperationParams
     * @return \communicator\sources\Platon\WS\ExternalOperationInvoke
     */
    public function setOperationParams($OperationParams)
    {
      $this->OperationParams = $OperationParams;
      return $this;
    }

}
