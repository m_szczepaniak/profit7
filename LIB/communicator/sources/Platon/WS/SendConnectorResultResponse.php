<?php

namespace communicator\sources\Platon\WS;

class SendConnectorResultResponse
{

    /**
     * @var boolean $SendConnectorResultResult
     */
    protected $SendConnectorResultResult = null;

    /**
     * @param boolean $SendConnectorResultResult
     */
    public function __construct($SendConnectorResultResult)
    {
      $this->SendConnectorResultResult = $SendConnectorResultResult;
    }

    /**
     * @return boolean
     */
    public function getSendConnectorResultResult()
    {
      return $this->SendConnectorResultResult;
    }

    /**
     * @param boolean $SendConnectorResultResult
     * @return \communicator\sources\Platon\WS\SendConnectorResultResponse
     */
    public function setSendConnectorResultResult($SendConnectorResultResult)
    {
      $this->SendConnectorResultResult = $SendConnectorResultResult;
      return $this;
    }

}
