<?php

namespace communicator\sources\Platon\WS;

class ConnectorResult
{

    /**
     * @var guid $ConnectorIdent
     */
    protected $ConnectorIdent = null;

    /**
     * @var string $ErrorMessage
     */
    protected $ErrorMessage = null;

    /**
     * @var string $Result
     */
    protected $Result = null;

    /**
     * @var guid $TaskGuid
     */
    protected $TaskGuid = null;

    /**
     * @param guid $ConnectorIdent
     * @param guid $TaskGuid
     */
    public function __construct($ConnectorIdent, $TaskGuid)
    {
      $this->ConnectorIdent = $ConnectorIdent;
      $this->TaskGuid = $TaskGuid;
    }

    /**
     * @return guid
     */
    public function getConnectorIdent()
    {
      return $this->ConnectorIdent;
    }

    /**
     * @param guid $ConnectorIdent
     * @return \communicator\sources\Platon\WS\ConnectorResult
     */
    public function setConnectorIdent($ConnectorIdent)
    {
      $this->ConnectorIdent = $ConnectorIdent;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->ErrorMessage;
    }

    /**
     * @param string $ErrorMessage
     * @return \communicator\sources\Platon\WS\ConnectorResult
     */
    public function setErrorMessage($ErrorMessage)
    {
      $this->ErrorMessage = $ErrorMessage;
      return $this;
    }

    /**
     * @return string
     */
    public function getResult()
    {
      return $this->Result;
    }

    /**
     * @param string $Result
     * @return \communicator\sources\Platon\WS\ConnectorResult
     */
    public function setResult($Result)
    {
      $this->Result = $Result;
      return $this;
    }

    /**
     * @return guid
     */
    public function getTaskGuid()
    {
      return $this->TaskGuid;
    }

    /**
     * @param guid $TaskGuid
     * @return \communicator\sources\Platon\WS\ConnectorResult
     */
    public function setTaskGuid($TaskGuid)
    {
      $this->TaskGuid = $TaskGuid;
      return $this;
    }

}
