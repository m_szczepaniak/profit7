<?php

namespace communicator\sources\Platon\WS;

class ConnectorTaskInfo
{

    /**
     * @var guid $ConnectorIdent
     */
    protected $ConnectorIdent = null;

    /**
     * @var string $ConnectorResult
     */
    protected $ConnectorResult = null;

    /**
     * @var string $ConnectorTask
     */
    protected $ConnectorTask = null;

    /**
     * @var base64Binary $ConnectorTaskData
     */
    protected $ConnectorTaskData = null;

    /**
     * @var int $ConnectorTaskGettingTimeOut
     */
    protected $ConnectorTaskGettingTimeOut = null;

    /**
     * @var ConnectorTaskKind $ConnectorTaskKind
     */
    protected $ConnectorTaskKind = null;

    /**
     * @var int $ConnectorTaskLargeTaskSizeLimit
     */
    protected $ConnectorTaskLargeTaskSizeLimit = null;

    /**
     * @var int $ConnectorTaskProcessingTimeOut
     */
    protected $ConnectorTaskProcessingTimeOut = null;

    /**
     * @var int $ConnectorTaskResultSendingTimeOut
     */
    protected $ConnectorTaskResultSendingTimeOut = null;

    /**
     * @var guid $TaskGuid
     */
    protected $TaskGuid = null;

    /**
     * @var ExternalOperationStatus $TaskStatus
     */
    protected $TaskStatus = null;

    /**
     * @var int $TaskWaitingForConnectorTimeOut
     */
    protected $TaskWaitingForConnectorTimeOut = null;

    /**
     * @var int $TransformConnectorResultTimeOut
     */
    protected $TransformConnectorResultTimeOut = null;

    /**
     * @param guid $ConnectorIdent
     * @param int $ConnectorTaskGettingTimeOut
     * @param ConnectorTaskKind $ConnectorTaskKind
     * @param int $ConnectorTaskLargeTaskSizeLimit
     * @param int $ConnectorTaskProcessingTimeOut
     * @param int $ConnectorTaskResultSendingTimeOut
     * @param guid $TaskGuid
     * @param ExternalOperationStatus $TaskStatus
     * @param int $TaskWaitingForConnectorTimeOut
     * @param int $TransformConnectorResultTimeOut
     */
    public function __construct($ConnectorIdent, $ConnectorTaskGettingTimeOut, $ConnectorTaskKind, $ConnectorTaskLargeTaskSizeLimit, $ConnectorTaskProcessingTimeOut, $ConnectorTaskResultSendingTimeOut, $TaskGuid, $TaskStatus, $TaskWaitingForConnectorTimeOut, $TransformConnectorResultTimeOut)
    {
      $this->ConnectorIdent = $ConnectorIdent;
      $this->ConnectorTaskGettingTimeOut = $ConnectorTaskGettingTimeOut;
      $this->ConnectorTaskKind = $ConnectorTaskKind;
      $this->ConnectorTaskLargeTaskSizeLimit = $ConnectorTaskLargeTaskSizeLimit;
      $this->ConnectorTaskProcessingTimeOut = $ConnectorTaskProcessingTimeOut;
      $this->ConnectorTaskResultSendingTimeOut = $ConnectorTaskResultSendingTimeOut;
      $this->TaskGuid = $TaskGuid;
      $this->TaskStatus = $TaskStatus;
      $this->TaskWaitingForConnectorTimeOut = $TaskWaitingForConnectorTimeOut;
      $this->TransformConnectorResultTimeOut = $TransformConnectorResultTimeOut;
    }

    /**
     * @return guid
     */
    public function getConnectorIdent()
    {
      return $this->ConnectorIdent;
    }

    /**
     * @param guid $ConnectorIdent
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setConnectorIdent($ConnectorIdent)
    {
      $this->ConnectorIdent = $ConnectorIdent;
      return $this;
    }

    /**
     * @return string
     */
    public function getConnectorResult()
    {
      return $this->ConnectorResult;
    }

    /**
     * @param string $ConnectorResult
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setConnectorResult($ConnectorResult)
    {
      $this->ConnectorResult = $ConnectorResult;
      return $this;
    }

    /**
     * @return string
     */
    public function getConnectorTask()
    {
      return $this->ConnectorTask;
    }

    /**
     * @param string $ConnectorTask
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setConnectorTask($ConnectorTask)
    {
      $this->ConnectorTask = $ConnectorTask;
      return $this;
    }

    /**
     * @return base64Binary
     */
    public function getConnectorTaskData()
    {
      return $this->ConnectorTaskData;
    }

    /**
     * @param base64Binary $ConnectorTaskData
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setConnectorTaskData($ConnectorTaskData)
    {
      $this->ConnectorTaskData = $ConnectorTaskData;
      return $this;
    }

    /**
     * @return int
     */
    public function getConnectorTaskGettingTimeOut()
    {
      return $this->ConnectorTaskGettingTimeOut;
    }

    /**
     * @param int $ConnectorTaskGettingTimeOut
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setConnectorTaskGettingTimeOut($ConnectorTaskGettingTimeOut)
    {
      $this->ConnectorTaskGettingTimeOut = $ConnectorTaskGettingTimeOut;
      return $this;
    }

    /**
     * @return ConnectorTaskKind
     */
    public function getConnectorTaskKind()
    {
      return $this->ConnectorTaskKind;
    }

    /**
     * @param ConnectorTaskKind $ConnectorTaskKind
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setConnectorTaskKind($ConnectorTaskKind)
    {
      $this->ConnectorTaskKind = $ConnectorTaskKind;
      return $this;
    }

    /**
     * @return int
     */
    public function getConnectorTaskLargeTaskSizeLimit()
    {
      return $this->ConnectorTaskLargeTaskSizeLimit;
    }

    /**
     * @param int $ConnectorTaskLargeTaskSizeLimit
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setConnectorTaskLargeTaskSizeLimit($ConnectorTaskLargeTaskSizeLimit)
    {
      $this->ConnectorTaskLargeTaskSizeLimit = $ConnectorTaskLargeTaskSizeLimit;
      return $this;
    }

    /**
     * @return int
     */
    public function getConnectorTaskProcessingTimeOut()
    {
      return $this->ConnectorTaskProcessingTimeOut;
    }

    /**
     * @param int $ConnectorTaskProcessingTimeOut
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setConnectorTaskProcessingTimeOut($ConnectorTaskProcessingTimeOut)
    {
      $this->ConnectorTaskProcessingTimeOut = $ConnectorTaskProcessingTimeOut;
      return $this;
    }

    /**
     * @return int
     */
    public function getConnectorTaskResultSendingTimeOut()
    {
      return $this->ConnectorTaskResultSendingTimeOut;
    }

    /**
     * @param int $ConnectorTaskResultSendingTimeOut
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setConnectorTaskResultSendingTimeOut($ConnectorTaskResultSendingTimeOut)
    {
      $this->ConnectorTaskResultSendingTimeOut = $ConnectorTaskResultSendingTimeOut;
      return $this;
    }

    /**
     * @return guid
     */
    public function getTaskGuid()
    {
      return $this->TaskGuid;
    }

    /**
     * @param guid $TaskGuid
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setTaskGuid($TaskGuid)
    {
      $this->TaskGuid = $TaskGuid;
      return $this;
    }

    /**
     * @return ExternalOperationStatus
     */
    public function getTaskStatus()
    {
      return $this->TaskStatus;
    }

    /**
     * @param ExternalOperationStatus $TaskStatus
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setTaskStatus($TaskStatus)
    {
      $this->TaskStatus = $TaskStatus;
      return $this;
    }

    /**
     * @return int
     */
    public function getTaskWaitingForConnectorTimeOut()
    {
      return $this->TaskWaitingForConnectorTimeOut;
    }

    /**
     * @param int $TaskWaitingForConnectorTimeOut
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setTaskWaitingForConnectorTimeOut($TaskWaitingForConnectorTimeOut)
    {
      $this->TaskWaitingForConnectorTimeOut = $TaskWaitingForConnectorTimeOut;
      return $this;
    }

    /**
     * @return int
     */
    public function getTransformConnectorResultTimeOut()
    {
      return $this->TransformConnectorResultTimeOut;
    }

    /**
     * @param int $TransformConnectorResultTimeOut
     * @return \communicator\sources\Platon\WS\ConnectorTaskInfo
     */
    public function setTransformConnectorResultTimeOut($TransformConnectorResultTimeOut)
    {
      $this->TransformConnectorResultTimeOut = $TransformConnectorResultTimeOut;
      return $this;
    }

}
