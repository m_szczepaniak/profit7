<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-09-22 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace communicator\sources\Platon;

use DatabaseManager;
use orders\OrderMagazineData;

/**
 * Description of DataProvider
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class dataProvider {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  public function __construct(DatabaseManager $pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * Metoda wykonuje pobranie zamówień do wysłania
   * 
   * @return array - lista produktów
   */
  public function getOrdersToSend($vat = null, $notVat = null, $ommitLimit = false) {
    
    $oOrderMagazine = new OrderMagazineData($this->pDbMgr);
    $aSelectCols = array('CONCAT(O.id, \'-\', OI.product_id)',
        'OI.product_id',
        'GROUP_CONCAT(OI.id SEPARATOR \',\') as items',
        'OI.order_id',
        'SUM(OI.quantity) as quantity',
        'OI.shipment_date',
        '(OI.quantity * PS.platon_wholesale_price) AS platon_wholesale_price'
    );
    return $oOrderMagazine->getOrdersItemsSendToSource(
        Platon::SOURCE_ID,
        Platon::NEW_INTERNAL_STATUS,
        Platon::SOURCE_SYMBOL,
        $aSelectCols,
        $ommitLimit == false ? $this->getOrdersLimit() : 0.00,
        $ommitLimit == false ? 0 : 9999999,
        null,
        $vat,
        $notVat);
//    if (empty($aParams['items'])) {
//      return array();
//    }
//    $aParams['platon_order_id'] = $oOrderMagazine->getNewListNumber(Order::NEW_INTERNAL_STATUS);
//    if ($aParams['platon_order_id'] == '') {
//      throw new Exception('Błąd generowania numeru listy');
//    }
//    return $aParams;
  }// end of getOrdersToSend() method
  
  
  /**
   * Metoda pobiera limit zamówień
   * 
   * @global object $pDbMgr
   * @return string
   */
  private function getOrdersLimit() {
    global $pDbMgr;

    $sSql = 'SELECT platon_limit_price
             FROM orders_magazine_settings 
             WHERE id = 1';
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of getOrdersLimit() method
  
  
  /**
   * Metoda pobiera atrybuty produktu
   * 
   * @param int $iPId
   * @param array $aCols
   * TABELE: s - sources
   *         pts - products_to_sources
   *         p - products
   * @return array
   */
  public function _getProductAttr($iPId, $aCols) {
    
    $sSql = 'SELECT '.implode(',', $aCols).' 
      FROM sources AS s
      JOIN products_to_sources AS pts 
      ON s.id = pts.source_id
      JOIN products AS p 
      ON pts.product_id = p.id
      WHERE p.id = '.$iPId.' 
        AND s.provider_id = '.Platon::SOURCE_ID.'
      ORDER BY pts.main_index DESC '; 
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of _getProductAttr() method
  
  
  /**
   * 
   * @return array
   */
  public function getSellerData() {
    
    $sSql = 'SELECT * FROM orders_seller_data';
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
}
