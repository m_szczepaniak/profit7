<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-10-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Platon;

require_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/communicator/sources/CommonSources.class.php');

use Common;
use CommonSources;
use Exception;
use orders\OrderMagazineData;

/**
 * Description of Invoice
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Invoice extends CommonSources {

  private $bTestMode;
  private $pDbMgr;
  
  private $sInvoicePath;
  public $oModuleZamowienia;
  
  public function __construct($pDbMgr, $bTestMode) {
    
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    include_once($_SERVER['DOCUMENT_ROOT'] . 'omniaCMS/modules/m_zamowienia/Module.class.php');
    $oNull = new \stdClass();
    $this->oModuleZamowienia = new \Module($oNull, $oNull, TRUE);
    
    if ($bTestMode === true) {
      $this->sInvoicePath = __DIR__.'/../../../../omniaCMS/files/'.Platon::SOURCE_SYMBOL;
    } else {
      $this->sInvoicePath = __DIR__.'/../../../../../files/'.Platon::SOURCE_SYMBOL;
    }
    parent::__construct();
  }
  
  
  /**
   * 
   */
  public function proceedInvoices() {

    $aXMLFiles = $this->getXMLFVFiles($this->sInvoicePath);
    foreach ($aXMLFiles as $sXMLFile) {
      if ($this->proceedInvoiceFile($sXMLFile) !== false);
      rename($this->sInvoicePath.'/'.$sXMLFile, $this->sInvoicePath.'/old/'.$sXMLFile);
    }
  }
  
  /**
   * 
   * @param string $sXMLFile
   */
  private function proceedInvoiceFile($sXMLFile) {
    return $this->proceedGetDataXML($sXMLFile);
  }
  
  /**
   * 
   * @param FV_XML $oFV
   * @return type
   */
  private function getOrderSHIId(FV_XML $oFV) {
    $sOrderIdent = $oFV->getOrderNumberNumber();
    return $this->getOSHIdByOrderNumber($sOrderIdent);
  }
  
  /**
   * Metoda pobiera zamówione pozycje w azymut, które
   * 
   * @global object $pDbMgr
   */
  private function getOSHIdByOrderNumber($sOrderIdent) {
    global $pDbMgr;
    
    $sSql = "SELECT OSH.id
             FROM orders_send_history AS OSH
             JOIN orders_send_history_attributes AS OSHA
              ON orders_send_history_id = OSH.id
             JOIN orders_send_history_items AS OSHI
              ON OSHI.send_history_id = OSH.id
             WHERE OSH.source = '".Platon::SOURCE_ID."'
                   AND OSHA.ident_nr = '".$sOrderIdent."'
                   AND OSHA.fv_nr IS NULL
                   AND OSH.status = '".Platon::NEW_INTERNAL_STATUS."'
             ";
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of getRRAzymutTransactions() method
  
  /**
   * 
   * @param string $sXMLFile
   * @return array
   */
  private function proceedGetDataXML($sXMLFile) {

    $oFV = new FV_XML($this->sInvoicePath.'/'.$sXMLFile);
    $oOrdersMagazineData = new OrderMagazineData($this->pDbMgr);
    $iOSHId = $this->getOrderSHIId($oFV);
    if ($iOSHId <= 0) {
      // brak takiego nr u nas, może zamówiono telefonicznie
      return true;
    }
    
    $aItems = $oFV->getProducts();
    if ($this->validateUpdateItems($iOSHId, $aItems) === false) {
      return false;
    }
    $sFVNumber = $oFV->getFVNumber();
    if ($sFVNumber != '' && $this->checkFVUnique($sFVNumber) == TRUE) {
      $oOrdersMagazineData->addSetHistorySendAttr($iOSHId, 'fv_nr', $sFVNumber);
    }
    return $this->proceedConformItemsFV($aItems, $iOSHId);
  }
  
  
  /**
   * 
   * @param int $iOSHId
   * @param array $aXMLItems
   * @param array $aPostData
   */
  public function validateUpdateItems($iOSHId, $aXMLItems) {
    
    foreach ($aXMLItems as $aItem) {
      if ($this->updateOSHItemPrices($iOSHId, $aItem) === false) {
        return false;
      }
    }
    return true;
  }
 
  
  /**
   * 
   * @param int $iOSHId
   * @param array $aItem
   * @return array
   */
  private function updateOSHItemPrices($iOSHId, $aItem) {
    
    $sSql = 'SELECT OSHI.id 
             FROM orders_send_history_items AS OSHI
             JOIN orders_items AS OI
              ON OSHI.item_id = OI.id
             JOIN products AS P
              ON P.id = OI.product_id
             WHERE
             OSHI.send_history_id = '.$iOSHId.' AND
             OSHI.item_source_id = "'.$aItem['source_item_id'].'" ';
    $aItems = $this->pDbMgr->GetCol('profit24', $sSql);
    $aToUpdate = [
        'discount' => Common::formatPrice2($aItem['discount']),
        'price_netto' => Common::formatPrice2($aItem['price_netto']),
        'vat' => $aItem['vat']
    ];
    foreach ($aItems as $iItemId) {
      if ($this->pDbMgr->Update('profit24', 'orders_send_history_items', $aToUpdate, ' id='.$iItemId) === false) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * 
   * @param array $aItems
   * @param int $iOSHId
   * @throws Exception
   */
  private function proceedConformItemsFV($aItems, $iOSHId) {
    
    foreach ($aItems as $aOrderedItem) {
      if ($this->proceedOrderedItemsResponse($iOSHId, $aOrderedItem['source_item_id'], $aOrderedItem['quantity'], 6, Platon::SOURCE_ID, Platon::NEW_INTERNAL_STATUS) === FALSE) {
        $sErr = 'Wystąpił błąd podczas zmiany statusu zamówienia'
                .' send_history_id: '.$iOSHId
                .' bookindeks: '.$aOrderedItem['SupplierItemCode'];
        $this->aErr[] = $sErr;
        throw new Exception($sErr);
      }
    }
    return true;
  }
  
  /**
   * 
   * @param string $sFVUniqe
   * @return bool
   */
  private function checkFVUnique($sFVUniqe) {
    $sSql = 'SELECT fv_nr FROM orders_send_history_attributes WHERE fv_nr = "'.$sFVUniqe.'"';
    
    return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? FALSE : TRUE);
  }
  
  /**
   * 
   * @param string $sPath
   * @return array
   */
  private function getXMLFVFiles($sPath) {
    
    $items = [];
    if ($handle = opendir($sPath)) {
      while (false !== ($entry = readdir($handle))) {
          if ($entry != "." && $entry != ".." && stristr($entry, '.xml')) {
            $items[] = $entry;
          }
      }
      closedir($handle);
    }
    return $items;
  }
}
