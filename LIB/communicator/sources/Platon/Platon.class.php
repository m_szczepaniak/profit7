<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-09-22 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace communicator\sources\Platon;

use communicator\sources\AutoOrders;
use DatabaseManager;
use Exception;

/**
 * Description of Platon
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Platon implements AutoOrders {

  /**
   *
   * @var DatabaseManager
   */
  public $pDbMgr;

  CONST SOURCE_ID = 41;
  CONST SOURCE_SYMBOL = 'platon';
  CONST NEW_INTERNAL_STATUS = 1;

  public function __construct($pDbMgr, $bTestMode = TRUE) {
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }

  /**
   * 
   * @param array $aBooksList
   * @return boolean
   */
  public function __putOrder($aBooksList = NULL) {

    $order = new Order($this->pDbMgr, $this->bTestMode);
    if ((isset($aBooksList['vat']) && $aBooksList['vat'] != '') || (isset($aBooksList['notVat']) && $aBooksList['notVat'] != '')) {
      $vat = $aBooksList['vat'];
      $notVat = $aBooksList['notVat'];
      $ommitLimit = false;
      if (isset($aBooksList['ommitLimit']) && $aBooksList['ommitLimit'] == '1') {
          $ommitLimit = true;
      }
      unset($aBooksList['vat']);
      unset($aBooksList['notVat']);
      unset($aBooksList);
    }

    // pobieramy listę
    if (empty($aBooksList) || $vat > 0 || $notVat > 0) {
      // pobierzmy listę książek
      $aBooksList = $order->getBooksList([], $vat, $notVat, $ommitLimit);
    }
    // doOrder
    if (!empty($aBooksList)) {
      try {
        $this->pDbMgr->BeginTransaction('profit24');
        $order->sendOrder($aBooksList);
        $this->pDbMgr->CommitTransaction('profit24');
      } catch (Exception $ex) {
        $this->pDbMgr->RollbackTransaction('profit24');
        $this->aErr[] = $ex;
        $this->sendInfoMail(self::SOURCE_SYMBOL . ' - ' . $ex->getMessage(), implode('<br /><br />', $this->aErr));
        echo self::SOURCE_SYMBOL . ' - ' . $ex;
        throw new Exception($ex);
      }
    }
  }

  public function __getInv() {
    $order = new Invoice($this->pDbMgr, $this->bTestMode);

    try {
      $this->pDbMgr->BeginTransaction('profit24');
      $order->proceedInvoices();
      $this->pDbMgr->CommitTransaction('profit24');
    } catch (Exception $ex) {
      $this->pDbMgr->RollbackTransaction('profit24');
      $this->aErr[] = $ex;
      $this->sendInfoMail(self::SOURCE_SYMBOL . ' - ' . $ex->getMessage(), implode('<br /><br />', $this->aErr));
      echo self::SOURCE_SYMBOL . ' - ' . $ex;
      throw new Exception($ex);
    }
  }

  /**
   * 
   * @param string $sLogin
   * @param string $sPassword
   * @param bool $bLogout
   * @return string
   */
  public function _doLogin($sLogin, $sPassword, $bLogout) {

    return 'Zalogowany jako MARCIN JANOWSKI !!!!';
  }// end of _addSourceIndeks() method


  /**
   * Wysyła maila z logiem importu
   * @param $sTopic - temat maila
   * @param $sContent - treść maila
   * @return void
   */
  public function sendInfoMail($sTopic, $sContent){
    global $aConfig;
    $emails = [
        'a.golba@profit24.pl',
        'milena.sadowska@platon.com.pl',
        'wydanie@platon.com.pl',
        $aConfig['common']['import_send_to']
    ];

    if(!\Common::sendMail('', $aConfig['common']['import_sender_email'], $emails, $sTopic, $sContent))
      dump('error sending mail');
  } // end of sendInfoMail() function
}
