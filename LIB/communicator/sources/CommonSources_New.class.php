<?php
/**
 * Klasa wspólna pomagająca do obsługi komunikacji z serwisami zewnętrznymi
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources;

class CommonSources_New {

  function __construct() {
    
  }

  /**
	 * Metoda wysyła dane za pomocą POST lub GET
   * @author Arkadiusz Golba <arekgl0@op.pl>
   * 
   * @param $sGETURL - adres który ma być pobrany, moze zawierać parametry GET
   * @param $bPost - czy wysyłamy POST'em
   * @param $aPostData - dane do POST'a
   * @param $iTimeOutInSecounds - czas po którym połączenie jest porzucane
	 * @return boolean|response
	 */
	public function CURL_file_get_contents($sGETURL, $bPost = TRUE, $aPostData = array(), $iTimeOutInSecounds = 10) {
    
    $oCURL = curl_init();
    curl_setopt($oCURL, CURLOPT_URL, $sGETURL);
    curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
    curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);
    
    if ($bPost === TRUE) {
      curl_setopt($oCURL, CURLOPT_POST, TRUE);
      curl_setopt($oCURL, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
      curl_setopt($oCURL, CURLOPT_POSTFIELDS, $aPostData);
    }
    
    curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($oCURL, CURLOPT_CONNECTTIMEOUT, $iTimeOutInSecounds);
    curl_setopt($oCURL, CURLOPT_TIMEOUT, $iTimeOutInSecounds);

    $sReturnStr = curl_exec($oCURL);
    $info = curl_getinfo($oCURL);
    if ($sReturnStr === false) {
      curl_close($oCURL);
      return false;
    }
    curl_close($oCURL);
    return $sReturnStr;
	}// end of CURL_file_get_contents() function
  
  
  /**
  * Wyłuskuje błędy walidacji pliku wygenerowanego do eksportu
  *
  * @return string
  */
 public function libxml_display_error($error)
 {
     $return = "<br/>\n";
     switch ($error->level) {
         case LIBXML_ERR_WARNING:
             $return .= "<b>Warning $error->code</b>: ";
             break;
         case LIBXML_ERR_ERROR:
             $return .= "<b>Error $error->code</b>: ";
             break;
         case LIBXML_ERR_FATAL:
             $return .= "<b>Fatal Error $error->code</b>: ";
             break;
     }
     $return .= trim($error->message);
     if ($error->file) {
         $return .=    " in <b>$error->file</b>";
     }
     $return .= " on line <b>$error->line</b>\n";

     return $return;
 }
 
 
  /**
   * Wyłuskuje błędy walidacji pliku wygenerowanego do eksportu
   *
   * @return string
   */
  public function libxml_display_errors() {
      $sErr = '';
      $errors = libxml_get_errors();
      foreach ($errors as $error) {
          $sErr .= $this->libxml_display_error($error);
      }
      libxml_clear_errors();
      return $sErr;
  }// end of libxml_display_errors() function
  
  
	/**
	* Wysyła maila z logiem importu
	* @param $sTopic - temat maila
	* @param $sContent - treść maila
	* @return void
	*/
	public function sendInfoMail($sTopic, $sContent){
		global $aConfig;

		if(!\Common::sendMail('', $aConfig['common']['import_sender_email'], $aConfig['common']['import_send_to'], $sTopic, $sContent))
			dump('error sending mail');
	} // end of sendInfoMail() function
  
  
  /**
   * Metoda zapisuje do bazy danych informacje na temat zmienionego statusu zamówień
   * 
   * @param int $iOldStatus
   * @param int $iNewStatus
   * @param array $aInfoData
   * @return bool
   */
  public function informAboutStatusChanged($aInfoData) {
    global $aConfig, $pDbMgr;
    
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
    if (isset($aInfoData) && !empty($aInfoData) && is_array($aInfoData)) {
      foreach ($aInfoData as $aOrderItem) {

        $aValues = array(
          'created' => 'NOW()',
          'created_by' => 'job_auto_status',
          'content' => serialize($aOrderItem)
        );
        if ($pDbMgr->Insert('profit24', 'events_logs_orders', $aValues) === false) {
          return false;
        }
      }
    }
    return true;
  }// end of informAboutStatusChanged() method
  
  
  /**
   * Metoda przetwarza ilość którą zwróciło źródło
   * 
   * @param int $iOrderProviderId
   * @param string $sSourceIndex
   * @param int $iCurrentQuantity
   * @param int $iStatus 1 - otwarty normalnie w zamówieniu do dostawcy, 3 - rezerwacja, 6 - FV, 9 - tytuł potwierdzony
   * @return bool
   */
  public function proceedOrderedItemsResponse($iOrderProviderId, $sSourceIndex, $iCurrentQuantity, $iStatus, $sCreatedBy = 'auto') {
    
    $bIsErr = FALSE;
    $iSumOrderedQuantity = 0;
    $aInformData = array();
    
    // odnalezienie pozycji z listy
    $aOItems = $this->oOrdersToProviders->getSourceItemOrderToProvider($iOrderProviderId, $sSourceIndex);
//    $aOItems = $this->_getOrderItemBy($iSendHistoryId, $sSourceIndex);
        
    if ($iOrderProviderId > 0 && !empty($aOItems)) {
      // zmiana 
      $this->oOrdersToProviders->changeOrdersToProvidersStatus($iOrderProviderId, $iStatus, $sCreatedBy);
      foreach ($aOItems as $aOItem) {
  //      $iOrderedQuantity = $aOItem['quantity'];

        // sumujemy każdą ze składowych
        // aby sprawdzić czy wystarczająco zostało zatwierdzonych
        $iSumOrderedQuantity += $aOItem['to_order_quantity'];

        if (!empty($aOItem) && is_array($aOItem)) {
          if (intval($iCurrentQuantity) < intval($iSumOrderedQuantity)) {
            $bNotEnought = TRUE;
            $cItemStatus = 'B';
            $iConfirmedQuantity = intval($iCurrentQuantity) - intval($iSumOrderedQuantity-$aOItem['to_order_quantity']); // odejmujemy to ile chcemy od tego ile sumarycznie przyszło być moze uda sie potwierdzić np. 1 z zamówionych 2
          } else {
            $bNotEnought = FALSE;
            $cItemStatus = $iStatus;
            $iConfirmedQuantity = $aOItem['to_order_quantity'];// wystarczy więc potwierdzamy tyle ile zamówiliśmy
          }
          if ($this->oOrdersToProviders->updateCurrentQuantity($aOItem['id'], intval($iConfirmedQuantity), $cItemStatus) !== FALSE) {

            // sprawdzenie, czy zarezerwowano, taką ilość, jaką zamówiono
            if ($bNotEnought) {

              // jeśli zrealizowano mniej, niż zamówiono, to zmiana statusu: '---'
              if ($this->_setProductNewRecount($aOItem, $aOItem['order_id'], $aOItem['order_item_id'], $aOItem['payment_id']) === FALSE) {
                $bIsErr = TRUE;
              }
            }
          } else {
            $bIsErr = TRUE;
          }
        } else {
          $bIsErr = TRUE;
        }
      }

      if ($bIsErr != TRUE && !empty($aInformData)) {
        $this->informAboutStatusChanged($aInformData);
      }
    }
    return $bIsErr === TRUE ? FALSE : TRUE;
  }// end of proceedOrderedItemsResponse() method
  
  
  /**
   * Metoda ustwia produkt w zamówieniu na '---' i przelicza zamówienie
   * 
   * @param array $aOItem
   * @param int $iOrderId
   * @param int $iOrderItemId
   * @param int $iPaymentId
   * @return boolean
   */
  public function _setProductNewRecount($aOItem, $iOrderId, $iOrderItemId, $iPaymentId) {
    
    if ($this->_setOrderItemStatusNew($iOrderItemId) !== FALSE) {
      // przeliczenie zamówienia
      if ($this->oModuleZamowienia->recountOrder($iOrderId, $iPaymentId) === FALSE) {
        return FALSE;
      }
      $aOrderTMP = $aOItem;
      $aOrderTMP['id'] = $iOrderId;
      // zmiana statusu
      $aInformData[] = array(
          'new_status' => '0',
          'old_status' => 1,
          'order' => $aOrderTMP
      );
    } else {
     return FALSE;
    }
    return TRUE;
  }// end of _setRecountProductNew() method
  
  
  /**
   * Metoda ustawia zamówienie jako nowe
   * 
   * @global object $pDbMgr
   * @param int $iOItem
   * @return boolean
   */
  private function _setOrderItemStatusNew($iOItem) {
    global $pDbMgr;
    
    if ($iOItem > 0) {
      $aValues = array(
          'sent_hasnc' => '0',
          'status' => '0'
      );
      return $pDbMgr->Update('profit24', 'orders_items', $aValues, ' id='.$iOItem);
    } else {
      return FALSE;
    }
  }// end of _setOrderItemStatusNew method
  
  
  /**
   * Metoda dodaje zamówienie do dostawcy
   * 
   * @param string $sOrderNumber
   * @param array $aBooksList
   * @param int $iSourceId
   * @param string $sFilePath
   * @param string $sExOrderNumber
   * @param string $sCreatedBy = 'auto'
   * @throws \Exception
   * @return int
   */
  public function addOrdersList($sOrderNumber, $aBooksList, $iSourceId, $sFilePath, $sExOrderNumber = 'NULL', $sCreatedBy = 'auto') {
    try {
      include_once($_SERVER['DOCUMENT_ROOT'].'LIB/orders/magazine/OrderMagazineData.class.php');
      $oOrderMagazine = new \orders\magazine\OrderMagazineData();
      //XXX TODO - odkomentować      $oOrderMagazine->setOrderedItemsByStatus(self::NEW_INTERNAL_STATUS, $this->_getOrdersItemsList($aParams));
      $iSendOrderToProviderId = $oOrderMagazine->addOrdersSendHistory($sOrderNumber, $aBooksList, $iSourceId, $sFilePath, $sExOrderNumber, $sCreatedBy);
      
      /*
      $aSendHistoryLog = array(array(
          'type' => 'new_azymut_transaction',
          'number' => $aParams['number'],
          'orders_to_providers' => $iSendOrderToProviderId,
          'id_transaction' => $sAzymutIdTransakcji
      ));
      $this->informAboutStatusChanged($aSendHistoryLog);
       */
      return $iSendOrderToProviderId;
    } catch (\Exception $exc) {
      throw new \Exception($exc->getMessage());
    }
  }// end of addOrdersList() method
}
