<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-17 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources;
interface iPointsOfReceipt {

  /**
   * 
   * @param string $sPointOfReceipt
   * @return \communicator\sources\Localizations @TODO do zmiany
   */
  public function getSinglePointDetails($sPointOfReceipt);
  
  
  /**
   * 
   * @param string $sPointOfReceipt
   * @return bool
   */
  public function checkDestinationPointAvailable($sPointOfReceipt);
  

  /**
   * 
   * @return array
   */
  public function getDestinationPointsList();
  
  /**
   * @return mixed string - HTML select, array(array(label, value))
   */
  public function getDestinationPointsDropdown();
}

