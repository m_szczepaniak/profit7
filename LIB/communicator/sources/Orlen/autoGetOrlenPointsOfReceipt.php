<?php

namespace communicator\sources\Orlen;
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Orlen/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

$bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
$oOrlen = new \orders\Shipment('orlen', $pDbMgr, $bTestMode);

$aPoints = $oOrlen->getDestinationPointsList();
if (!empty($aPoints)) {
  $oOrlen->doDeletePoints('PointsOfReceipt');
  $oOrlen->doInsertPoints('PointsOfReceipt', $aPoints);
}