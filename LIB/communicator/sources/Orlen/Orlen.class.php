<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Orlen;
class Orlen implements \communicator\sources\iShipment {

  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;
  private $bTestMode;
  
  /**
   *
   * @var WebService
   */
  public $Orlen;
  
  const PRZESYLKA_POBRANIOWA = '0';
  
  CONST UBEZPIECZENIE = 32000;

  public $iTransportId = 6;
          
  function __construct($bTestMode, $pDbMgr) {
    $this->bTestMode = $bTestMode;
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @param int $iWebsiteId
   */
  protected function init($iWebsiteId) {
    
    if (is_null($this->Orlen)) {
      $this->aTransportSettings = $this->getTransportSettings($iWebsiteId);
      if ($this->bTestMode === TRUE) {
        $this->aTransportSettings['login'] = 'demo';
        $this->aTransportSettings['password'] = 'demo';
      }
      $options = array();
      $options["login"] = $this->aTransportSettings['login'];
      $options["password"] = $this->aTransportSettings['password'];
      $options["trace"] = 1;
      $options['soap_version'] = SOAP_1_2;
      $this->Orlen = new WebService($this->aTransportSettings['api_url'], $options);
    }
  }

  
  public function addShipment($aOrder, $aDeliverAddress, $aWebsiteSettings) {
  
  }  
  

  private function getRecipientAddress($aDeliverAddress, $sPointOfReceipt) {
    
  }


  public function doCancelShipment() {
    
  }

  
  /**
   * 
   * @param int $iShippingPoint
   * @return array
   */
  public function getShippingPoint($iShippingPoint) {
    
    $this->init(1);
    $oOrlenPointsOfReceipt = new PointsOfReceipt($this->bTestMode, $this->pDbMgr);
    $oOrlenPointsOfReceipt->iTransportId = $this->iTransportId;
    $oOrlenPointsOfReceipt->Orlen = $this->Orlen;
    return $oOrlenPointsOfReceipt->getSinglePointDetails($iShippingPoint);
  }

  
  /**
   * 
   * @param string $sPointOfReceipt
   * @return bool
   */
  public function checkDestinationPointAvailable($sPointOfReceipt) {

    $this->initOrlen(1);
    $oOrlenPointsOfReceipt = new PointsOfReceipt($this->bTestMode, $this->pDbMgr);
    $oOrlenPointsOfReceipt->iTransportId = $this->iTransportId;
    $oOrlenPointsOfReceipt->Orlen = $this->Orlen;
    return $oOrlenPointsOfReceipt->checkDestinationPointAvailable($sPointOfReceipt);
  }

  
  /**
   * 
   * @return array
   */
  public function getDestinationPointsList() {
    
    $this->initOrlen(1);
    $oOrlenPointsOfReceipt = new OrlenPointsOfReceipt($this->bTestMode, $this->pDbMgr);
    $oOrlenPointsOfReceipt->iTransportId = $this->iTransportId;
    $oOrlenPointsOfReceipt->Orlen = $this->Orlen;
    return $oOrlenPointsOfReceipt->getDestinationPointsList();
  }

  
  /**
   * 
   * @param int $iWebsiteId
   * @return array
   */
  private function getTransportSettings($iWebsiteId) {
    $oKeyValue = new \table\keyValue('orders_transport_means', $this->getWebsiteSymbol($iWebsiteId));
    return $oKeyValue->getAllByIdDest($this->iTransportId);
  }// end of getTransportSettings() method
  
  
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type 
	 */
	private function getWebsiteSymbol($iWebsiteId) {
		global $pDbMgr;
		
		$sSql = "SELECT code 
             FROM websites
						 WHERE id=".$iWebsiteId;
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getWebsiteSymbol() method
  

  public function getDeliveryStatus($iId, $sTransportNumber) {

  }  
  
  
   public function doPrintAddressLabel($sFilePath, $sPrinter) {

  }
  
  
  public function getAddressLabel($sFilePath, $iOrderId) {

  }  
  
  
  public function getTransportListFilename($agConfig, $sTransportNumber) {
    
  }  
}
