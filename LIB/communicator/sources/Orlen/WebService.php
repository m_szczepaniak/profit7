<?php
namespace communicator\sources\Orlen\Shipping;

class Login {
  public $token; // Token
}

class Token {
  public $Username; // string
  public $Password; // string
}

class LoginResponse {
  public $LoginResult; // boolean
}

class ChangePassword {
  public $token; // Token
  public $newPassword; // string
}

class ChangePasswordResponse {
  public $ChangePasswordResult; // boolean
}

class getPointsList {
  public $hash; // string
}

class getPointsListResponse {
  public $getPointsListResult; // PointListResponse
}

class PointListResponse {
  public $Hash; // string
  public $Date; // dateTime
  public $Point; // ArrayOfPoint
}

class Point {
  public $Id; // int
  public $PostalCode; // string
  public $City; // string
  public $AddressLine; // string
  public $Name; // string
  public $Province; // string
  public $ProvinceId; // int
  public $District; // string
  public $DistrictId; // int
  public $Availability; // int
  public $Longitude; // string
  public $Latitude; // string
  public $PointTypeId; // int
  public $openHoursMoStart; // string
  public $openHoursMoEnd; // string
  public $openHoursTuStart; // string
  public $openHoursTuEnd; // string
  public $openHoursWeStart; // string
  public $openHoursWeEnd; // string
  public $openHoursThStart; // string
  public $openHoursThEnd; // string
  public $openHoursFrStart; // string
  public $openHoursFrEnd; // string
  public $openHoursSaStart; // string
  public $openHoursSaEnd; // string
  public $openHoursSuStart; // string
  public $openHoursSuEnd; // string
}

class getAllPointsList {
}

class getAllPointsListResponse {
  public $getAllPointsListResult; // ArrayOfPointEx
}

class PointEx {
  public $Active; // boolean
}

class getPointsDetail {
  public $Id; // int
}

class getPointsDetailResponse {
  public $getPointsDetailResult; // PointListResponse
}

class getPointsProvinces {
}

class getPointsProvincesResponse {
  public $getPointsProvincesResult; // ArrayOfProvince
}

class Province {
  public $Id; // int
  public $Province1; // string
}

class getPointsDistricts {
  public $ProvinceId; // int
}

class getPointsDistrictsResponse {
  public $getPointsDistrictsResult; // ArrayOfDistrict
}

class District {
  public $Id; // int
  public $DistrictD; // string
}

class getCities {
  public $Province; // string
}

class getCitiesResponse {
  public $getCitiesResult; // ArrayOfString
}

class getPointsCity {
  public $City; // string
}

class getPointsCityResponse {
  public $getPointsCityResult; // PointListResponse
}

class getDistrictPointsList {
  public $DistrictId; // string
}

class getDistrictPointsListResponse {
  public $getDistrictPointsListResult; // PointListResponse
}

class getPointsNearAddress {
  public $Address; // string
  public $Distance; // int
}

class getPointsNearAddressResponse {
  public $getPointsNearAddressResult; // PointListResponse
}

class shipment {
  public $token; // Token
  public $shipment; // Shipment
}

class ShipmentObj {
  public $ShipTo; // ShipTo
  public $ShipFrom; // ShipFrom
  public $LabelImageFormat; // string
  public $CODAmountFLAG; // int
  public $CODAmount; // int
  public $PaymentFlag; // int
  public $PickupDate; // int
  public $SelfDeliverToSorting; // int
  public $ServiceType; // int
  public $CourierId; // int
  public $CurierPackageNumber; // string
  public $Description; // string
  public $PackageWeight; // int
  public $Reference1; // string
  public $Reference2; // string
  public $InsuranceAmount; // int
  public $VitayNo; // string
}

class ShipTo {
  public $FirstName; // string
  public $LastName; // string
  public $PhoneNumber; // string
  public $PointId; // int
}

class ShipFrom {
  public $Name; // string
  public $AttentionName; // string
  public $AddressLine; // string
  public $City; // string
  public $PostalCode; // string
  public $CountryCode; // string
  public $PhoneNumber; // string
  public $Login; // string
}

//class shipmentResponse {
//  public $shipmentResult; // ShipmentResponse
//}

class ShipmentResponse {
  public $PackageNumber; // string
  public $CurierPackageNumber; // string
  public $DazumiNumber; // string
  public $LabeLimageFormat; // string
  public $Label; // string
  public $Point; // Point
  public $Date; // dateTime
  public $Delay; // int
  public $ShipmentData; // string
}

//class shipmentFrom {
//  public $token; // Token
//  public $shipment; // ShipmentFrom
//}

class ShipmentFrom {
  public $ShipTo; // Location
  public $ShipFrom; // ShipFromPoint
  public $LabelImageFormat; // string
  public $PaymentFlag; // int
  public $PickupDate; // int
  public $CurierPackageNumber; // string
  public $Description; // string
  public $PackageWeight; // int
  public $Reference1; // string
  public $Reference2; // string
  public $InsuranceAmount; // int
  public $VitayNo; // string
}

class Location {
  public $ClientTaxId; // string
  public $Name; // string
  public $Address; // string
  public $City; // string
  public $PostCode; // string
  public $CountryCode; // string
  public $Person; // string
  public $Contact; // string
  public $EMail; // string
  public $IsPrivatePerson; // boolean
}

class ShipFromPoint {
  public $PointId; // int
}

class shipmentFromResponse {
  public $shipmentFromResult; // ShipmentResponse
}


class MultipackLabel {
  public $token; // Token
  public $request; // MultipackRequest
}

class MultipackRequest {
  public $ShipFrom; // Location
  public $Type; // ParcelType
  public $TotalWeight; // decimal
}

class ParcelType {
  const Package = 'Package';
  const Palette = 'Palette';
}

class MultipackLabelResponse {
  public $MultipackLabelResult; // MultipackResponse
}

class MultipackResponse {
  public $PackageNumber; // string
  public $Label; // string
}



class updateShipmentResponse {
  public $updateShipmentResult; // string
}

class checkBalance {
  public $token; // Token
  public $login; // string
}

class checkBalanceResponse {
  public $checkBalanceResult; // BalanceStatus
}

class BalanceStatus {
  public $Balance; // int
  public $ModificationDate; // string
}

class getRates {
  public $token; // Token
  public $login; // string
}

class getRatesResponse {
  public $getRatesResult; // Rates
}

class Rates {
  public $NetRatePerPrepaidShipping; // int
  public $GrossRatePerPrepaidShipping; // int
  public $NetRatePerPrepaidCOD; // int
  public $GrossRatePerPrepaidCOD; // int
  public $NetRatePerPostpaidShipping; // int
  public $GrossRatePerPostpaidShipping; // int
  public $NetRatePerPostpaidCOD; // int
  public $GrossRatePerPostpaidCOD; // int
  public $PostpaidLimit; // int
  public $NetOwnCourier; // int
  public $GrossOwnCourier; // int
}

class rechargeAccount {
  public $token; // Token
  public $recharge; // Recharge
}

class Recharge {
  public $Login; // string
  public $Amount; // int
  public $TransferTitle; // string
}

class rechargeAccountResponse {
  public $rechargeAccountResult; // BalanceStatus
}

class createLogin {
  public $token; // Token
}

class createLoginResponse {
  public $createLoginResult; // string
}

class cancelShipment {
  public $token; // Token
  public $packageNumber; // string
}

class cancelShipmentResponse {
  public $cancelShipmentResult; // string
}

class getMassPaymentsAccount {
  public $token; // Token
  public $login; // string
}

class getMassPaymentsAccountResponse {
  public $getMassPaymentsAccountResult; // string
}

class getShipments {
  public $token; // Token
  public $shipmentsListRequest; // ShipmentsListRequest
}

class ShipmentsListRequest {
  public $Login; // string
  public $Offset; // int
  public $RowCount; // int
  public $PackageNumber; // string
  public $CurierPackageNumber; // string
  public $DispatchDateStart; // dateTime
  public $DispatchDateEnd; // dateTime
  public $DeliveryDateStart; // dateTime
  public $DeliveryDateEnd; // dateTime
  public $RecipientName; // string
  public $SenderName; // string
  public $PackStatusCode; // int
  public $CODStatusCode; // int
  public $InvoiceStatus; // int
  public $InvoiceNumber; // string
}

class getShipmentsResponse {
  public $getShipmentsResult; // ArrayOfPackage
}

class Package {
  public $PackageNumber; // string
  public $CurierPackageNumber; // string
  public $RecipientName; // string
  public $SenderName; // string
  public $PackStatusCode; // int
  public $PackStatusDescription; // string
  public $DispatchDate; // dateTime
  public $DeliveryDate; // dateTime
  public $DateOfCompletion; // string
  public $CODAmount; // int
  public $Point; // Point
  public $Label; // string
  public $CODStatusCode; // string
  public $CODStatusDecription; // string
  public $DazumiNumber; // string
  public $RSCurierPackageNumber; // string
  public $Reference1; // string
  public $Reference2; // string
}

class getPaymentOperations {
  public $token; // Token
  public $paymentOperationRequest; // PaymentOperationRequest
}

class PaymentOperationRequest {
  public $Login; // string
  public $Offset; // int
  public $RowCount; // int
  public $OperationDateStart; // string
  public $OperationDateEnd; // string
  public $Plus; // int
  public $AmountMin; // int
  public $AmountMax; // int
}

class getPaymentOperationsResponse {
  public $getPaymentOperationsResult; // ArrayOfPaymentOperation
}

class PaymentOperation {
  public $OperationDate; // string
  public $TransferTitle; // string
  public $Amount; // int
  public $Balance; // int
  public $Plus; // int
  public $PackageNumber; // string
}

class picup {
  public $token; // Token
  public $pickupRequest; // PickupRequest
}

class PickupRequest {
  public $Login; // string
  public $CloseTime; // string
  public $ReadyTime; // string
  public $PickupDate; // string
  public $CompanyName; // string
  public $ContactName; // string
  public $AddressLine; // string
  public $City; // string
  public $PostalCode; // string
  public $Number; // string
  public $CountryCode; // string
  public $Quantity; // int
  public $Weight; // int
  public $ConfirmationEmailAddress; // string
  public $UndeliverableEmailAddress; // string
}

class picupResponse {
  public $picupResult; // string
}

class shopUpdate {
  public $token; // Token
  public $shop; // Shop
}

class Shop {
  public $Login; // string
  public $VATinvoiceCompanyName; // string
  public $VATinvoiceTaxIdNo; // string
  public $VATinvoiceStreetName; // string
  public $VATinvoiceStreetNo; // string
  public $VATinvoiceFlatNo; // string
  public $VATinvoiceCity; // string
  public $VATinvoiceZipCode; // string
  public $VATinvoiceContactFirstName; // string
  public $VATinvoiceContactLastName; // string
  public $VATinvoiceContactTel; // string
  public $VATinvoiceContactEmail; // string
  public $ReturnTheFundsCompanyName; // string
  public $ReturnTheFundsTaxIdNo; // string
  public $ReturnTheFundsStreetName; // string
  public $ReturnTheFundsStreetNo; // string
  public $ReturnTheFundsFlatNo; // string
  public $ReturnTheFundsZipCode; // string
  public $ReturnTheFundsCity; // string
  public $ReturnTheFundsAccountNo; // string
}

class shopUpdateResponse {
  public $shopUpdateResult; // string
}

class GetAvailableServices {
  public $token; // Token
  public $pickupLocation; // Location
  public $deliveryLocation; // Location
  public $ReadyDate; // dateTime
  public $Parcels; // ArrayOfParcel
  public $cod; // COD
  public $InsuranceAmount; // decimal
  public $rebateCoupon; // string
}

class Parcel {
  public $Weight; // decimal
  public $D; // decimal
  public $W; // decimal
  public $S; // decimal
}

class COD {
  public $Amount; // decimal
  public $RetAccountNo; // string
}

class GetAvailableServicesResponse {
  public $GetAvailableServicesResult; // GetAvailableServicesResponse
}
//
//class GetAvailableServicesResponse {
//  public $AvailableServices; // ArrayOfService
//}

class Service {
  public $ID; // int
  public $Name; // string
  public $MaxPickupTime; // dateTime
  public $MaxDeliveryTime; // dateTime
}

class CreateOrder {
  public $token; // Token
  public $ServiceID; // int
  public $pickupLocation; // Location
  public $deliveryLocation; // Location
  public $ReadyDate; // dateTime
  public $Parcels; // ArrayOfParcel
  public $cod; // COD
  public $ROD; // boolean
  public $InsuranceAmount; // decimal
  public $addidtionalInfo; // string
  public $PackageContentDescr; // string
  public $rebateCoupon; // string
  public $RequestLabelFormat; // int
}

class CreateOrderResponse {
  public $CreateOrderResult; // Order
}

class Order {
  public $LetterNo; // string
  public $MimeData; // base64Binary
  public $ReadyDate; // dateTime
}

class GetOrderStatus {
  public $PackageNo; // ArrayOfString
}

class GetOrderStatusResponse {
  public $GetOrderStatusResult; // ArrayOfOrderStatus
}

class OrderStatus {
  public $PackageNo; // string
  public $CreateDate; // dateTime
  public $PickupDate; // dateTime
  public $DeliveryDate; // dateTime
  public $CurrentStatusId; // string
  public $CurrentStatus; // string
  public $DeliveredToPerson; // string
  public $LastStatusDate; // dateTime
}

class CreateReturnOrder {
  public $PackageNo; // string
}

class CreateReturnOrderResponse {
  public $CreateReturnOrderResult; // boolean
}


/**
 * WebService class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class WebService extends \SoapClient {

  private static $classmap = array(
                                    'Login' => 'Login',
                                    'Token' => 'Token',
                                    'LoginResponse' => 'LoginResponse',
                                    'ChangePassword' => 'ChangePassword',
                                    'ChangePasswordResponse' => 'ChangePasswordResponse',
                                    'getPointsList' => 'getPointsList',
                                    'getPointsListResponse' => 'getPointsListResponse',
                                    'PointListResponse' => 'PointListResponse',
                                    'Point' => 'Point',
                                    'getAllPointsList' => 'getAllPointsList',
                                    'getAllPointsListResponse' => 'getAllPointsListResponse',
                                    'PointEx' => 'PointEx',
                                    'getPointsDetail' => 'getPointsDetail',
                                    'getPointsDetailResponse' => 'getPointsDetailResponse',
                                    'getPointsProvinces' => 'getPointsProvinces',
                                    'getPointsProvincesResponse' => 'getPointsProvincesResponse',
                                    'Province' => 'Province',
                                    'getPointsDistricts' => 'getPointsDistricts',
                                    'getPointsDistrictsResponse' => 'getPointsDistrictsResponse',
                                    'District' => 'District',
                                    'getCities' => 'getCities',
                                    'getCitiesResponse' => 'getCitiesResponse',
                                    'getPointsCity' => 'getPointsCity',
                                    'getPointsCityResponse' => 'getPointsCityResponse',
                                    'getDistrictPointsList' => 'getDistrictPointsList',
                                    'getDistrictPointsListResponse' => 'getDistrictPointsListResponse',
                                    'getPointsNearAddress' => 'getPointsNearAddress',
                                    'getPointsNearAddressResponse' => 'getPointsNearAddressResponse',
                                    'shipment' => 'shipment',
                                    'Shipment' => 'Shipment',
                                    'ShipTo' => 'ShipTo',
                                    'ShipFrom' => 'ShipFrom',
                                    'shipmentResponse' => 'shipmentResponse',
                                    'ShipmentResponse' => 'ShipmentResponse',
                                    'shipmentFrom' => 'shipmentFrom',
                                    'ShipmentFrom' => 'ShipmentFrom',
                                    'Location' => 'Location',
                                    'ShipFromPoint' => 'ShipFromPoint',
                                    'shipmentFromResponse' => 'shipmentFromResponse',
                                    'shipmentExt' => 'shipmentExt',
                                    'ShipmentExt' => 'ShipmentExt',
                                    'shipmentExtResponse' => 'shipmentExtResponse',
                                    'MultipackLabel' => 'MultipackLabel',
                                    'MultipackRequest' => 'MultipackRequest',
                                    'ParcelType' => 'ParcelType',
                                    'MultipackLabelResponse' => 'MultipackLabelResponse',
                                    'MultipackResponse' => 'MultipackResponse',
                                    'updateShipment' => 'updateShipment',
                                    'UpdateShipment' => 'UpdateShipment',
                                    'updateShipmentResponse' => 'updateShipmentResponse',
                                    'checkBalance' => 'checkBalance',
                                    'checkBalanceResponse' => 'checkBalanceResponse',
                                    'BalanceStatus' => 'BalanceStatus',
                                    'getRates' => 'getRates',
                                    'getRatesResponse' => 'getRatesResponse',
                                    'Rates' => 'Rates',
                                    'rechargeAccount' => 'rechargeAccount',
                                    'Recharge' => 'Recharge',
                                    'rechargeAccountResponse' => 'rechargeAccountResponse',
                                    'createLogin' => 'createLogin',
                                    'createLoginResponse' => 'createLoginResponse',
                                    'cancelShipment' => 'cancelShipment',
                                    'cancelShipmentResponse' => 'cancelShipmentResponse',
                                    'getMassPaymentsAccount' => 'getMassPaymentsAccount',
                                    'getMassPaymentsAccountResponse' => 'getMassPaymentsAccountResponse',
                                    'getShipments' => 'getShipments',
                                    'ShipmentsListRequest' => 'ShipmentsListRequest',
                                    'getShipmentsResponse' => 'getShipmentsResponse',
                                    'Package' => 'Package',
                                    'getPaymentOperations' => 'getPaymentOperations',
                                    'PaymentOperationRequest' => 'PaymentOperationRequest',
                                    'getPaymentOperationsResponse' => 'getPaymentOperationsResponse',
                                    'PaymentOperation' => 'PaymentOperation',
                                    'picup' => 'picup',
                                    'PickupRequest' => 'PickupRequest',
                                    'picupResponse' => 'picupResponse',
                                    'shopUpdate' => 'shopUpdate',
                                    'Shop' => 'Shop',
                                    'shopUpdateResponse' => 'shopUpdateResponse',
                                    'GetAvailableServices' => 'GetAvailableServices',
                                    'Parcel' => 'Parcel',
                                    'COD' => 'COD',
                                    'GetAvailableServicesResponse' => 'GetAvailableServicesResponse',
                                    'GetAvailableServicesResponse' => 'GetAvailableServicesResponse',
                                    'Service' => 'Service',
                                    'CreateOrder' => 'CreateOrder',
                                    'CreateOrderResponse' => 'CreateOrderResponse',
                                    'Order' => 'Order',
                                    'GetOrderStatus' => 'GetOrderStatus',
                                    'GetOrderStatusResponse' => 'GetOrderStatusResponse',
                                    'OrderStatus' => 'OrderStatus',
                                    'CreateReturnOrder' => 'CreateReturnOrder',
                                    'CreateReturnOrderResponse' => 'CreateReturnOrderResponse',
                                   );

  public function WebService($wsdl = "http://api.stacjazpaczka.pl/WebService.asmx?WSDL", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *  
   *
   * @param Login $parameters
   * @return LoginResponse
   */
  public function Login(Login $parameters) {
    return $this->__soapCall('Login', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param ChangePassword $parameters
   * @return ChangePasswordResponse
   */
  public function ChangePassword(ChangePassword $parameters) {
    return $this->__soapCall('ChangePassword', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getPointsList $parameters
   * @return getPointsListResponse
   */
  public function getPointsList(getPointsList $parameters) {
    return $this->__soapCall('getPointsList', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getAllPointsList $parameters
   * @return getAllPointsListResponse
   */
  public function getAllPointsList(getAllPointsList $parameters) {
    return $this->__soapCall('getAllPointsList', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getPointsDetail $parameters
   * @return getPointsDetailResponse
   */
  public function getPointsDetail(getPointsDetail $parameters) {
    return $this->__soapCall('getPointsDetail', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getPointsProvinces $parameters
   * @return getPointsProvincesResponse
   */
  public function getPointsProvinces(getPointsProvinces $parameters) {
    return $this->__soapCall('getPointsProvinces', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getPointsDistricts $parameters
   * @return getPointsDistrictsResponse
   */
  public function getPointsDistricts(getPointsDistricts $parameters) {
    return $this->__soapCall('getPointsDistricts', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getCities $parameters
   * @return getCitiesResponse
   */
  public function getCities(getCities $parameters) {
    return $this->__soapCall('getCities', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getPointsCity $parameters
   * @return getPointsCityResponse
   */
  public function getPointsCity(getPointsCity $parameters) {
    return $this->__soapCall('getPointsCity', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getDistrictPointsList $parameters
   * @return getDistrictPointsListResponse
   */
  public function getDistrictPointsList(getDistrictPointsList $parameters) {
    return $this->__soapCall('getDistrictPointsList', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getPointsNearAddress $parameters
   * @return getPointsNearAddressResponse
   */
  public function getPointsNearAddress(getPointsNearAddress $parameters) {
    return $this->__soapCall('getPointsNearAddress', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param shipment $parameters
   * @return shipmentResponse
   */
  public function shipment(shipment $parameters) {
    return $this->__soapCall('shipment', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param shipmentFrom $parameters
   * @return shipmentFromResponse
   */
  public function shipmentFrom(shipmentFrom $parameters) {
    return $this->__soapCall('shipmentFrom', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param shipmentExt $parameters
   * @return shipmentExtResponse
   */
  public function shipmentExt(shipmentExt $parameters) {
    return $this->__soapCall('shipmentExt', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param MultipackLabel $parameters
   * @return MultipackLabelResponse
   */
  public function MultipackLabel(MultipackLabel $parameters) {
    return $this->__soapCall('MultipackLabel', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param updateShipment $parameters
   * @return updateShipmentResponse
   */
  public function updateShipment(updateShipment $parameters) {
    return $this->__soapCall('updateShipment', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param checkBalance $parameters
   * @return checkBalanceResponse
   */
  public function checkBalance(checkBalance $parameters) {
    return $this->__soapCall('checkBalance', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getRates $parameters
   * @return getRatesResponse
   */
  public function getRates(getRates $parameters) {
    return $this->__soapCall('getRates', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param rechargeAccount $parameters
   * @return rechargeAccountResponse
   */
  public function rechargeAccount(rechargeAccount $parameters) {
    return $this->__soapCall('rechargeAccount', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param createLogin $parameters
   * @return createLoginResponse
   */
  public function createLogin(createLogin $parameters) {
    return $this->__soapCall('createLogin', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param cancelShipment $parameters
   * @return cancelShipmentResponse
   */
  public function cancelShipment(cancelShipment $parameters) {
    return $this->__soapCall('cancelShipment', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getMassPaymentsAccount $parameters
   * @return getMassPaymentsAccountResponse
   */
  public function getMassPaymentsAccount(getMassPaymentsAccount $parameters) {
    return $this->__soapCall('getMassPaymentsAccount', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getShipments $parameters
   * @return getShipmentsResponse
   */
  public function getShipments(getShipments $parameters) {
    return $this->__soapCall('getShipments', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param getPaymentOperations $parameters
   * @return getPaymentOperationsResponse
   */
  public function getPaymentOperations(getPaymentOperations $parameters) {
    return $this->__soapCall('getPaymentOperations', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param picup $parameters
   * @return picupResponse
   */
  public function picup(picup $parameters) {
    return $this->__soapCall('picup', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param shopUpdate $parameters
   * @return shopUpdateResponse
   */
  public function shopUpdate(shopUpdate $parameters) {
    return $this->__soapCall('shopUpdate', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetAvailableServices $parameters
   * @return GetAvailableServicesResponse
   */
  public function GetAvailableServices(GetAvailableServices $parameters) {
    return $this->__soapCall('GetAvailableServices', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CreateOrder $parameters
   * @return CreateOrderResponse
   */
  public function CreateOrder(CreateOrder $parameters) {
    return $this->__soapCall('CreateOrder', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetOrderStatus $parameters
   * @return GetOrderStatusResponse
   */
  public function GetOrderStatus(GetOrderStatus $parameters) {
    return $this->__soapCall('GetOrderStatus', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CreateReturnOrder $parameters
   * @return CreateReturnOrderResponse
   */
  public function CreateReturnOrder(CreateReturnOrder $parameters) {
    return $this->__soapCall('CreateReturnOrder', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

}

