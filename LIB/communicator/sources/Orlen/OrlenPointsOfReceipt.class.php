<?php

namespace communicator\sources\Orlen;

class OrlenPointsOfReceipt
{

  private $bTest;
  
  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var WebServiceOrlenTest
   */
  public $Orlen;
  

	public function __construct($pDbMgr, $bTest) {
    global $pDbMgr;
    
    $this->pDbMgr = $pDbMgr;
    $this->bTest = $bTest;
    if (is_null($this->pDbMgr)) {
      $this->pDbMgr = $pDbMgr;
    }
  }

  
  /**
  * Metoda zwraca pobraną z WebApi listę punktów odbioru
  * 
  * @return array
  */    
  public function getDestinationPointsList() {
    
    try 
    {
      $response = $this->Orlen->getAllPointsList(new getAllPointsList())->getAllPointsListResult->PointEx;
      
      return $response;
    } 
    catch (SoapFault $e) 
    {
      echo $e->faultcode.'<br />';
      echo $e->faultstring;
    } 
  }  
  
  
  /**
  * Metoda formatuje dane o punktach odbioru (dla potrzeb insertu do bazy danych)
  * 
  * @param array $aDataItem 
  * @return array
  */    
  public function doFormatInsertPoint($aDataItem) {
    
    $aResponse = array(
          'id' => $aDataItem->Id,
          '`desc`' => $aDataItem->PostalCode.' '.$aDataItem->City.' '.$aDataItem->AddressLine,
          'postcode' => str_replace('-', '', $aDataItem->PostalCode),
          'status' => $aDataItem->Active == true ? '1' : '0',
          'details' => base64_encode(serialize((array)$aDataItem)),
      );   
    return $aResponse;
  } 
  
  /**
   * 
   */
  public function doDeletePoints() {
    
    $sSql = "DELETE FROM shipping_points_orlen WHERE id > 0";
    $this->pDbMgr->Query('profit24', $sSql);  
  }
  
  
   /**
   * Metoda dodaje w bazie listę punktów odbioru
   * 
   * @param array $aPoints
   * @return bool
   */  
	public function doInsertPoints($aPoints) {
    
    if ($this->bTest === true) {
      $time_start = microtime(true); // pomiar czasu  
    } 

    foreach ($aPoints as $aDataItem) {
      $aFormatedDataItem = $this->doFormatInsertPoint($aDataItem);
      if ($this->pDbMgr->Insert('profit24', 'shipping_points_orlen', $aFormatedDataItem) === false) {
        return false;
      }
    }
    
    if ($this->bTest === true) {
      $time_end = microtime(true);
      $execution_time = ($time_end - $time_start)/60;
      echo '<b>Czas wykonania insertu:</b> '.$execution_time.' Mins'; // pomiar czasu  
    }
  }
  
  
   /**
   * Metoda zwracająca tablicę z informacjami o punktach Orlen dla listy wyboru
   * 
   * @return array
   */  
	public function getPointsOfReceiptList() {
    
    $sSql = "SELECT `desc` AS label, `id` AS value
             FROM shipping_points_orlen 
             WHERE status = '1'
             ORDER BY `desc` ASC";
    
    $aResponse = $this->pDbMgr->GetAll('profit24', $sSql);
    
    return $aResponse;
  }
  

  /**
   * Metoda sprawdza dostępność punktu odbioru
   *
   * @param type $sPointOfReceipt
   * @return bool
   */
  public function checkDestinationPointAvailable($sPointOfReceipt) {
  
    $sSql = "SELECT status
             FROM shipping_points_orlen
             WHERE id = '".$sPointOfReceipt."'";
    $cPointStatus = $this->pDbMgr->GetOne('profit24', $sSql);
    return ($cPointStatus == '1' ? true : false);
  }
  
  
   /**
   * Metoda zwracająca sformatowany string ze dot. godzin otwarcia wybranej stacji
   * 
   * @param int $sPointOfReceipt
   * @return array
   */  
	public function getSinglePointDetails($sPointOfReceipt) {
    
    $sAddSQL = ' WHERE id = "'.$sPointOfReceipt.'" LIMIT 1';
    $aPointsOfReceipt = $this->getPointsOfReceipt(array('details'), $sAddSQL);
    if (!empty($aPointsOfReceipt) && !empty($aPointsOfReceipt[0])) {
      $aPointOfReceipt = $aPointsOfReceipt[0];
      $aPointDetails = unserialize(base64_decode($aPointOfReceipt['details']));
      $aPointDetails['openHours'] = $this->getOpenHours($aPointDetails);
      $aPointDetails['desc'] = $this->getDescriptionPointOfReceipt($aPointDetails);
      return $aPointDetails;
    }
  }
  
  /**
   * 
   * @param array $aPointDetails
   * @return string
   */
  private function getDescriptionPointOfReceipt($aPointDetails) {
    return $aPointDetails['PostalCode'] . ' '. $aPointDetails['City'] . ', ' . $aPointDetails['AddressLine'];
  }
  
  
  /**
   * 
   * @param array $aPontsOfRecipt
   * @return array
   */
  private function doFormatPointsOfReceiptAutocomplete($aPontsOfRecipt) {
    
    foreach ($aPontsOfRecipt as $iKey => $aPointOfReceipt) {
      $aUserializedData = unserialize(base64_decode($aPointOfReceipt['data']));
      $aUserializedData['FormatedHours'] = $this->getOpenHours($aUserializedData);
      $aPontsOfRecipt[$iKey]['data'] = json_encode($aUserializedData);
    }
    return $aPontsOfRecipt;
  }// end of doFormatPointsOfReceipt() method

  
  /**
   * 
   * @param array $aPointDetails
   * @return string
   */
  public function getOpenHours($aPointDetails) {
    $sHTML .= _('Godziny otwarcia: ');
    $sHTML .= 'Pn: '.$this->getStartHour($aPointDetails['openHoursMoStart']).'-'.$this->getEndHour($aPointDetails['openHoursMoEnd']).', ';
    $sHTML .= 'Wt: '.$this->getStartHour($aPointDetails['openHoursTuStart']).'-'.$this->getEndHour($aPointDetails['openHoursTuEnd']).', ';
    $sHTML .= 'Śr: '.$this->getStartHour($aPointDetails['openHoursWeStart']).'-'.$this->getEndHour($aPointDetails['openHoursWeEnd']).', ';
    $sHTML .= 'Cz: '.$this->getStartHour($aPointDetails['openHoursThStart']).'-'.$this->getEndHour($aPointDetails['openHoursThEnd']).', ';
    $sHTML .= 'Pt: '.$this->getStartHour($aPointDetails['openHoursFrStart']).'-'.$this->getEndHour($aPointDetails['openHoursFrEnd']).', ';
    $sHTML .= 'So: '.$this->getStartHour($aPointDetails['openHoursSaStart']).'-'.$this->getEndHour($aPointDetails['openHoursSaEnd']).', ';
    $sHTML .= 'Nd: '.$this->getStartHour($aPointDetails['openHoursSuStart']).'-'.$this->getEndHour($aPointDetails['openHoursSuEnd']);
    
    return $sHTML;
  }
  
  
  /**
   * 
   * @param string $sHour
   * @return string
   */
  private  function getStartHour($sHour) {
    $sFormatedHour = strlen($sHour) == '1' ? $sHour = '0'.$sHour : $sHour;

    return $sFormatedHour;
  }

  /**
   * 
   * @param string $sHour
   * @return string
   */
  private  function getEndHour($sHour) {
    $sFormatedHour = strlen($sHour) == '1' ? $sHour = '0'.$sHour : $sHour;
    $sFormatedHour == '00' ? $sFormatedHour = '24' : $sFormatedHour;

    return $sFormatedHour;
  }
    
    
  /**
   * 
   * @param array $aCols
   * @param string $sAddSQL
   * @return array
   */
  private function getPointsOfReceipt($aCols, $sAddSQL) {
    
    $sSql = "SELECT ".implode(',', $aCols)."
             FROM shipping_points_orlen 
             ".$sAddSQL;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param array $aCols
   * @param string $sAddSQL
   * @return array
   */
  public function getPointsOfReceiptAutocomplete($aCols, $sAddSQL) {
    $aPointsOfReceipt = $this->getPointsOfReceipt($aCols, $sAddSQL);
    return $this->doFormatPointsOfReceiptAutocomplete($aPointsOfReceipt);
  }
}