<?php

namespace communicator\sources\Orlen;

class PointsOfReceipt extends \communicator\sources\Orlen\Shipment implements \communicator\sources\iPointsOfReceipt
{
 
  public $aTransportSettings;
  public $bTestMode;
  public $pDbMgr;

  
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    parent::__construct($aTransportSettings, $pDbMgr, $bTestMode);
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }

  
  /**
  * Metoda zwraca przechowywany w bazie hash dot. listy punktów
  * 
  * @return string
  */    
  private function getDestinationPointsHash() {
    $oKeyValue = new \table\keyValue('orders_transport_means');
    $sHash = $oKeyValue->getByIdDestKey($this->aTransportSettings['transport_id'], 'hash');
    
    return $sHash;
  } 

  
  /**
  * Metoda zapisuje w bazie hash dot. listy punktów
  * 
  * param string $sHash
  * @return bool
  */    
  private function updateDestinationPointsHash($sHash) {
    $oKeyValue = new \table\keyValue('orders_transport_means');
    return ($oKeyValue->update($this->aTransportSettings['transport_id'], 'hash', $sHash) == true ? true : false);
  }  
  
  
  /**
  * Metoda zwraca pobraną z WebApi listę punktów odbioru
  * 
  * @return array
  */    
  public function getDestinationPointsList() {
    
    try 
    {
      $sHash = $this->getDestinationPointsHash();
      
      $oPointsList = new \communicator\sources\Orlen\Shipping\getPointsList();
      $oPointsList->hash = $sHash;

      $sRemoteHash = $this->Orlen->getPointsList($oPointsList)->getPointsListResult->Hash;
      
      if ($sHash != $sRemoteHash) {
        echo 'Uruchomiono procedurę aktualizacji punktów odbiorów w bazie danych';
        $aResponse = $this->Orlen->getAllPointsList(new \communicator\sources\Orlen\Shipping\getAllPointsList())->getAllPointsListResult->PointEx;
        $this->updateDestinationPointsHash($sRemoteHash);
        
        return $aResponse;
      } else {
        echo 'Lista punktów odbiorów w bazie danych jest aktualna';
      }  
    } 
    catch (SoapFault $e) 
    {
      echo $e->faultcode.'<br />';
      echo $e->faultstring;
    } 
  }  
  
  
  /**
  * Metoda formatuje dane o punktach odbioru (dla potrzeb insertu do bazy danych)
  * 
  * @param array $aDataItem 
  * @return array
  */    
  public function doFormatInsertPoint($iKey, $aDataItem) {
    
    $aResponse = array(
          'sequence' => $iKey+1,
          'id' => $aDataItem->Id,
          '`desc`' => $aDataItem->PostalCode.' '.$aDataItem->City.' '.$aDataItem->AddressLine,
          'postcode' => str_replace('-', '', $aDataItem->PostalCode),
          'status' => $aDataItem->Active == true ? '1' : '0',
          'details' => base64_encode(serialize((array)$aDataItem)),
      );   
    return $aResponse;
  } 
  
  /**
   * 
   */
  public function doDeletePoints() {
    
    $sSql = "DELETE FROM shipping_points_orlen WHERE id > 0";
    $this->pDbMgr->Query('profit24', $sSql);  
  }
  
 
  /**
   * Metoda sortuje punkty odbioru wg miasta i ulicy dla potrzeb insertu do bazy danych
   * 
   * @param array $aPoints
   * @return array
   */  
	public function doSortPoints($aPointsOfRecipt) {
    
    $aSort = array();    
    foreach ($aPointsOfRecipt as $iKey => $aPointOfReceipt) {
      $aSort['City'][$iKey] = $aPointOfReceipt->City;
      $aSort['AddressLine'][$iKey] = $aPointOfReceipt->AddressLine;
    }
    
    setlocale(LC_COLLATE,'pl_PL.utf8');
    array_multisort($aSort['City'], SORT_LOCALE_STRING, SORT_ASC, $aSort['AddressLine'], SORT_LOCALE_STRING, SORT_ASC, $aPointsOfRecipt);    

    return $aPointsOfRecipt;
  }
  
  
   /**
   * Metoda dodaje w bazie listę punktów odbioru
   * 
   * @param array $aPoints
   * @return bool
   */  
	public function doInsertPoints($aPoints) {
    
    if ($this->bTest === true) {
      $time_start = microtime(true); // pomiar czasu  
    } 

    $aPoints = $this->doSortPoints($aPoints);
    
    foreach ($aPoints as $iKey => $aDataItem) {
      $aFormatedDataItem = $this->doFormatInsertPoint($iKey, $aDataItem);
      if ($this->pDbMgr->Insert('profit24', 'shipping_points_orlen', $aFormatedDataItem) === false) {
        return false;
      }
    }
    
    if ($this->bTest === true) {
      $time_end = microtime(true);
      $execution_time = ($time_end - $time_start)/60;
      echo '<b>Czas wykonania insertu:</b> '.$execution_time.' Mins'; // pomiar czasu  
    }
  }
  
  
  /**
   * 
   * @param array $aPointDetails
   * @return string
   */
  private function getDescriptionPointOfReceipt($aPointDetails) {
    return $aPointDetails['PostalCode'] . ' '. $aPointDetails['City'] . ', ' . $aPointDetails['AddressLine'];
  }
  
  
  /**
   * 
   * @param array $aPointsOfRecipt
   * @return array
   */
  private function doFormatPointsOfReceiptAutocomplete($aPointsOfRecipt) {

    foreach ($aPointsOfRecipt as $iKey => $aPointOfReceipt) {
      $aUserializedData = unserialize(base64_decode($aPointOfReceipt['data']));
      $aUserializedData['FormatedHours'] = $this->getOpenHours($aUserializedData);
      $aPointsOfRecipt[$iKey]['data'] = json_encode($aUserializedData);
    }

    return $aPointsOfRecipt;
  }// end of doFormatPointsOfReceipt() method

  
  /**
   * 
   * @param array $aPointDetails
   * @return string
   */
  public function getOpenHours($aPointDetails) {
    $sHTML .= _('Godziny otwarcia: ');
    $sHTML .= 'Pn: '.$this->getStartHour($aPointDetails['openHoursMoStart']).'-'.$this->getEndHour($aPointDetails['openHoursMoEnd']).', ';
    $sHTML .= 'Wt: '.$this->getStartHour($aPointDetails['openHoursTuStart']).'-'.$this->getEndHour($aPointDetails['openHoursTuEnd']).', ';
    $sHTML .= 'Śr: '.$this->getStartHour($aPointDetails['openHoursWeStart']).'-'.$this->getEndHour($aPointDetails['openHoursWeEnd']).', ';
    $sHTML .= 'Cz: '.$this->getStartHour($aPointDetails['openHoursThStart']).'-'.$this->getEndHour($aPointDetails['openHoursThEnd']).', ';
    $sHTML .= 'Pt: '.$this->getStartHour($aPointDetails['openHoursFrStart']).'-'.$this->getEndHour($aPointDetails['openHoursFrEnd']).', ';
    $sHTML .= 'So: '.$this->getStartHour($aPointDetails['openHoursSaStart']).'-'.$this->getEndHour($aPointDetails['openHoursSaEnd']).', ';
    $sHTML .= 'Nd: '.$this->getStartHour($aPointDetails['openHoursSuStart']).'-'.$this->getEndHour($aPointDetails['openHoursSuEnd']);
    
    return $sHTML;
  }
  
  
  /**
   * 
   * @param string $sHour
   * @return string
   */
  private  function getStartHour($sHour) {
    $sFormatedHour = strlen($sHour) == '1' ? $sHour = '0'.$sHour : $sHour;

    return $sFormatedHour;
  }

  /**
   * 
   * @param string $sHour
   * @return string
   */
  private  function getEndHour($sHour) {
    $sFormatedHour = strlen($sHour) == '1' ? $sHour = '0'.$sHour : $sHour;
    $sFormatedHour == '00' ? $sFormatedHour = '24' : $sFormatedHour;

    return $sFormatedHour;
  }

  
  /**
   * Metoda sprawdza dostępność punktu odbioru
   *
   * @param type $sPointOfReceipt
   * @return bool
   */
  public function checkDestinationPointAvailable($sPointOfReceipt) {

    $sSql = "SELECT status
             FROM shipping_points_orlen
             WHERE id = '".$sPointOfReceipt."'";
    $cPointStatus = $this->pDbMgr->GetOne('profit24', $sSql);
    
    return ($cPointStatus == '1' ? true : false);
  }  

  
  /**
   * 
   * @param array $aCols
   * @param string $sAddSQL
   * @return array
   */
  public function getPointsOfReceipt($aCols, $sAddSQL) {
    
    $sSql = "SELECT ".implode(',', $aCols)."
             FROM shipping_points_orlen 
             ".$sAddSQL;
    
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }    
  
  
  /**
   * 
   * @param array $aCols
   * @param string $sAddSQL
   * @return array
   */
  public function getPointsOfReceiptAutocomplete($aCols, $sAddSQL) {
    $aPointsOfReceipt = $this->getPointsOfReceipt($aCols, $sAddSQL);    
    return $this->doFormatPointsOfReceiptAutocomplete($aPointsOfReceipt);
  }  
  
  
   /**
   * Metoda zwracająca tablicę z informacjami o punktach Orlen dla listy wyboru
   * 
   * @return array
   */  
	public function getDestinationPointsDropdown() {
    
    return $this->getPointsOfReceipt(array('`desc` AS label', '`id` AS value'), ' WHERE status = "1" ORDER BY sequence ASC');
  }  
  
  
   /**
   * Metoda zwracająca sformatowany string ze dot. godzin otwarcia wybranej stacji
   * 
   * @param int $sPointOfReceipt
   * @return array
   */  
	public function getSinglePointDetails($sPointOfReceipt) {

    $sAddSQL = ' WHERE id = "'.$sPointOfReceipt.'" LIMIT 1';
    $aPointsOfReceipt = $this->getPointsOfReceipt(array('details'), $sAddSQL);
    if (!empty($aPointsOfReceipt) && !empty($aPointsOfReceipt[0])) {
      $aPointOfReceipt = $aPointsOfReceipt[0];
      $aPointDetails = unserialize(base64_decode($aPointOfReceipt['details']));
      $aPointDetails['openHours'] = $this->getOpenHours($aPointDetails);
      $aPointDetails['desc'] = $this->getDescriptionPointOfReceipt($aPointDetails);
      
      return $aPointDetails;
    }
  }

  
   /**
   * 
   * 
   * @param int $sPointOfReceipt
   * @return array
   */  
	public function getPopupDetails($sPointOfReceipt) {

    $sFontColor = '#fff';
    $sBackgroundColor = '#da3522';
    $aFormatedPointOfReciptDetails = $this->getSinglePointDetails($sPointOfReceipt); 
    
    $sDetails = "Wybrana stacja Orlen: <b><br />"
            . $aFormatedPointOfReciptDetails['desc'] . '</b><br /><br />'.$aFormatedPointOfReciptDetails['Name'].'<br />'.$aFormatedPointOfReciptDetails['openHours'].'<br />';

    if (!empty($aFormatedPointOfReciptDetails) && is_array($aFormatedPointOfReciptDetails)) {
      $sSrc = 'https://maps.google.pl/maps?f=q&source=s_q&hl=pl&geocode=&q='
              .$aFormatedPointOfReciptDetails['Latitude'].','.$aFormatedPointOfReciptDetails['Longitude']
              .'('.$aFormatedPointOfReciptDetails['PostalCode'].' '.$aFormatedPointOfReciptDetails['City'].' '.$aFormatedPointOfReciptDetails['AddressLine']
              .')&aq=&vpsrc=0&ie=UTF8&t=m&z=17&'
              .$aFormatedPointOfReciptDetails['Latitude'].','.$aFormatedPointOfReciptDetails['Longitude']
              .'&output=embed';
    } 

    return array('fontColor' => $sFontColor, 'backgroundColor' => $sBackgroundColor, 'details' => $sDetails, 'source' => $sSrc);  
    }
    
}
