<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-22 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Orlen;

class Shipment implements \communicator\sources\iShipment  {



  /**
   *
   * @var \communicator\sources\Orlen\Shipping\WebService
   */
  protected $Orlen;
  private $bTestMode;
  private $pDbMgr;
  private $aTransportSettings;
  
  CONST PRZESYLKA_POBRANIOWA = '0';
  CONST UBEZPIECZENIE = 32000;  
  
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {

    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    
    $this->init();
  }

  
  /**
   * 
   */
  protected function init() {
     
    if (is_null($this->Orlen)) { 
      if ($this->bTestMode === TRUE) {
        $this->aTransportSettings['login'] = 'demo';
        $this->aTransportSettings['password'] = 'demo';
      }
   
      $options = array();
      $options["login"] = $this->aTransportSettings['login'];
      $options["password"] = $this->aTransportSettings['password'];
      $options["trace"] = 1;
      $options['soap_version'] = SOAP_1_2;
      $options['connection_timeout'] = 10;
      
      include_once('WebService.php');
      $this->Orlen = new \communicator\sources\Orlen\Shipping\WebService($this->aTransportSettings['api_url'], $options);
    }
  }  
  
  
  /**
   * 
   * @param array $aOrder
   * @param array $aDeliverAddress
   * @param array $aSellerData
   * @return string transport number
   */
  public function addShipment($aOrder, $aDeliverAddress, $aSellerData) {
    global $aConfig;
    
    $shipment = new \communicator\sources\Orlen\Shipping\shipment();
    $shipment->token = new \communicator\sources\Orlen\Shipping\Token();
    $shipment->token->Username = $this->aTransportSettings['login'];
    $shipment->token->Password = $this->aTransportSettings['password'];
    
    $shipment->shipment = new \communicator\sources\Orlen\Shipping\ShipmentObj();
    $shipment->shipment->ShipFrom = $this->getSenderAddress($aSellerData, $aOrder['website_id']);
    $shipment->shipment->ShipTo = $this->getRecipientAddress($aDeliverAddress, $aOrder['point_of_receipt']);
    
    $shipment->shipment->LabelImageFormat = 'PDF2';
    $shipment->shipment->CODAmountFLAG = self::PRZESYLKA_POBRANIOWA;
    $shipment->shipment->CODAmount = 0;
    $shipment->shipment->PaymentFlag = '0';
    $shipment->shipment->SelfDeliverToSorting = '0';
    $shipment->shipment->ServiceType = '1';// ich kurier
    $shipment->shipment->Description = (isset($aOrder['orders_numbers']) && $aOrder['orders_numbers'] != '' ? $aOrder['orders_numbers'] : $aOrder['order_number']);
    $shipment->shipment->Reference1 = (isset($aOrder['orders_numbers']) && $aOrder['orders_numbers'] != '' ? $aOrder['orders_numbers'] : $aOrder['order_number']);
    $shipment->shipment->PackageWeight = $aOrder['weight'];
    $shipment->shipment->InsuranceAmount = self::UBEZPIECZENIE;
    
    $shipment->shipment->PickupDate = '0';
    $shipment->shipment->CourierId = '0';
    
    $shipmentReponse = new \stdClass();
    $shipmentReponse->shipmentResult = new \communicator\sources\Orlen\Shipping\ShipmentResponse();
    try{
      $shipmentReponse = $this->Orlen->shipment($shipment);
      $sTransportNumber = $shipmentReponse->shipmentResult->DazumiNumber;
      if (!empty($sTransportNumber)) {
        $sFilePatch = $this->getTransportListFilename($aConfig, $sTransportNumber);
        file_put_contents($sFilePatch, base64_decode($shipmentReponse->shipmentResult->Label));
        return $sTransportNumber;
      } else {
        throw new Exception(_('Brak nr listu przewozowego'));
      }
      return $sTransportNumber;
    } catch (\Exception $ex) {
      throw new \Exception(_(' Wystąpił błąd podczas pobierania listu przewozowego z Orlenu: ').$ex->getMessage());
    }
    return false;
  }
  

  public function doCancelShipment() {
    
  }

  
  public function doPrintAddressLabel($sFilePath, $sPrinter) {
    global $pDbMgr;
    
    $aValues = array(
        'filetype' => 'PDF',
        'filepath' => $sFilePath,
        'dest_printer' => $sPrinter,
        'created' => 'NOW()'
    );
    if ($pDbMgr->Insert('profit24', 'print_queue', $aValues) === false) {
      return false;
    }
    return true;
  }

  
  public function getAddressLabel($sFilePath, $iOrderId) {
    return $sFilePath; // ;-) [:->
  }
  

  public function getDeliveryStatus($iId, $sTransportNumber) {

    preg_match("/^\d{4}(\d{13})/", $sTransportNumber, $aMatches);   
    $sTransportNumber = $aMatches[1];    
    
    $oGetOrderStatus = new \communicator\sources\Orlen\Shipping\GetOrderStatus();
    $oGetOrderStatus->PackageNo[] = $sTransportNumber;
    $oGetOrderStatusResponse = new \communicator\sources\Orlen\Shipping\GetOrderStatusResponse();
    $oGetOrderStatusResponse->GetOrderStatusResult = new \communicator\sources\Orlen\Shipping\GetOrderStatusResponse();
    $oResult = $this->Orlen->GetOrderStatus($oGetOrderStatus);    

    $aStatusesCollection = array();  
    foreach ($oResult->GetOrderStatusResult as $oOrderStatus) {

      if (isset($oOrderStatus->CurrentStatus) && isset($oOrderStatus->LastStatusDate)) {
        $oDeliveryShipment = new \orders\Shipment\DeliveryShipment();
        $oDeliveryShipment->sStatusName = $oOrderStatus->CurrentStatus;  
        $oDeliveryShipment->sStatusDate = $oOrderStatus->LastStatusDate;
        $aStatusesCollection[] = $oDeliveryShipment; 
      }
    }  
    
    return $aStatusesCollection;
  }

  
  public function getTransportListFilename($agConfig, $sTransportNumber) {
    
    $sFilename = 'ListPrzewozowyOrlen_'.$sTransportNumber.'.pdf';
    return $agConfig['common']['client_base_path'].'/'.$agConfig['common']['cms_dir'].$agConfig['common']['orlen_dir'].$sFilename;
  }// end of getTransportListFilename() method

  
  /**
   * 
   * @param array $aDeliverAddress
   * @param string $sPointOfReceipt
   * @return \communicator\sources\Orlen\ShipTo
   */
  private function getRecipientAddress($aDeliverAddress, $sPointOfReceipt) {
    
    $ShipTo = new \communicator\sources\Orlen\Shipping\ShipTo();
    $ShipTo->FirstName = $aDeliverAddress['name'];
    $ShipTo->LastName = $aDeliverAddress['surname'];
    $ShipTo->PhoneNumber = $aDeliverAddress['phone'];
    $ShipTo->PointId = $sPointOfReceipt;
    return $ShipTo;
  }  
 
  
  /**
   * 
   * @param array $aSellerData
   * @return \communicator\sources\Orlen\ShipFrom
   */
  private function getSenderAddress($aSellerData) {
    
    $ShipFrom = new \communicator\sources\Orlen\Shipping\ShipFrom();
    $ShipFrom->Name = $aSellerData['paczkomaty_name'];
    $ShipFrom->AddressLine = $this->aTransportSettings['street'].' '.$this->aTransportSettings['number'] . '/' . $this->aTransportSettings['number2'];
    $ShipFrom->City = $this->aTransportSettings['city'];
    $ShipFrom->CountryCode = 'PL';
    $ShipFrom->Login = $this->aTransportSettings['login'];
    $ShipFrom->PhoneNumber = $aSellerData['phone'];
    $ShipFrom->PostalCode = $this->aTransportSettings['postal'];
    return $ShipFrom;
  }  

}

