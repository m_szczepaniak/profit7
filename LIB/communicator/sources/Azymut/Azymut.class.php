<?php
/**
 * Klasa obsługi komunikacji z Azymut
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Azymut extends CommonSources {

  public $aErr = array();
  public $aErrMsg;
  private $sCurrPath;
  private $sLogin;
  private $sPasswd;
  private $oModuleZamowienia;
  private $bTestMode;
  private $pDbMgr;
  CONST SOURCE_ID = 7;
  CONST SOURCE_SYMBOL = 'azymut';
  CONST NEW_INTERNAL_STATUS = 1;

  function __construct($bTestMode = false) {
    global $aConfig, $pDbMgr;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    $this->sCurrPath = preg_replace('/(.*)\\' . DIRECTORY_SEPARATOR . '.*/', '$1', __FILE__);

    $this->sLogin = $aConfig['import']['AUTO_azymut_login'];
    $this->sPasswd = $aConfig['import']['AUTO_azymut_password'];

    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/Module.class.php');
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
    $oNull = new \stdClass();
    $this->oModuleZamowienia = new \Module($oNull, $oNull, TRUE);
  }

  /**
   * Metoda pobiera z Azymut: Raport kompletacji zamówienia w formie faktury
   * 
   * @param void $null
   * @return boolean|string
   */
  public function __getInv($null = NULL) {
    
    $aAzymutTransactions = $this->getInvAzymutTransactons();
    
    foreach ($aAzymutTransactions as $iCount => $sTransactionId) {
        $sResponseFilePath = $this->sCurrPath . DIRECTORY_SEPARATOR . 'files/' . DIRECTORY_SEPARATOR . date('YmdHis') . '_'.$iCount.'_getInv__response.HTML';
        $sGETURL = 'http://www.azymut.pl/autoMw/main.olo?_cmd=getInv&l=' . $this->sLogin . '&p=' . urlencode($this->sPasswd) . '&orderId=' . $sTransactionId;
        
      try {
        $sXML = $this->CURL_file_get_contents($sGETURL, FALSE, array(), 500);
        $this->saveXML($sResponseFilePath, $sXML);
        
        $this->parseXMLgetInv($sResponseFilePath, $sTransactionId);
        
      } catch (Exception $e) {
        $this->sendInfoMail('Azymut - ' . $e, implode('<br /><br />', $this->aErr));
        echo 'Azymut - ' . $e;
        return FALSE;
      }
    }
    return TRUE;
  } // end of __getInv() method
  
  
  /**
   * Metoda pobiera z Azymut: Raport realizacji zamówienia
   * 
   * @param array $aParams
   * @return boolean|string
   */
  public function __postRR($null = NULL) {
    
    $aAzymutTransactions = $this->getRRAzymutTransactons();
    
    foreach ($aAzymutTransactions as $iCount => $sTransactionId) {
      $sResponseFilePath = $this->sCurrPath . DIRECTORY_SEPARATOR . 'files/' . DIRECTORY_SEPARATOR . date('YmdHis') . '_'.$iCount.'_postRR__response.HTML';
      $sGETURL = 'http://www.azymut.pl/autoMw/main.olo?_cmd=postRR&orderId=' . $sTransactionId . '&_mode=auto';

      try {
        $sXML = $this->CURL_file_get_contents($sGETURL, FALSE, array(), 500);
        $this->saveXML($sResponseFilePath, $sXML);

        // interpretacja pliku
        // odznaczanie produktów, które zostały zamówione, a nie zostały zarezerwowane
        $this->parseXMLpostRR($sResponseFilePath);

      } catch (Exception $e) {
        $this->sendInfoMail('Azymut - ' . $e, implode('<br /><br />', $this->aErr));
        echo 'Azymut - ' . $e;
        return FALSE;
      }
    }
    return TRUE;
  } // end of __postRR() method
  
  
  /**
   * Metoda wysyłania zamówienia na serwer Azymut, generuje przy tym 1 XML'a z 
   *  produktami do zamówienia, oraz otrzymuje zwrotnie XML'a z numerem zamówienia w Azymut (lub błędem)
   * 
   * @global array $aConfig
   * @param array $aParams
   * @return return FALSE
   */
  public function __putOrder($null = NULL) {

    try {
      // pobieramy listę
      $aParams = $this->getAzymutOrdersToSend();
      if (!empty($aParams)) {
        $aFormatedParams = $this->_formatAzymutData($aParams);
        // dodajemy listę do bazy i pobieramy jej numer
        $sFilePath = $this->xmlOrderBuilder($aFormatedParams);
        if ($this->bTestMode === TRUE) {
          $sAzymutIdTransakcji = '2013-09-23_14-49-88_0_03_03831';
        } else {
          $sAzymutIdTransakcji = $this->sendXMLOrder($sFilePath);
        }
        $this->setOrdersListStatus($aParams, $sFilePath, $sAzymutIdTransakcji);
      }
      return TRUE;
    } catch (Exception $e) {
      $this->aErr[] = $e;
      $this->sendInfoMail('Azymut - ' . $e->getMessage(), implode('<br /><br />', $this->aErr));
      echo 'Azymut - ' . $e;
      return FALSE;
    }
  }// end of __putOrder() method
  
  
  /**
   * Metoda pobiera zamówione pozycje w azymut, które
   * 
   * @global object $pDbMgr
   */
  private function getInvAzymutTransactons() {
    global $pDbMgr;
    
    $sSql = "SELECT OSHA.ident_nr 
             FROM orders_send_history AS OSH
             JOIN orders_send_history_attributes AS OSHA
              ON orders_send_history_id = OSH.id
             JOIN orders_send_history_items AS OSHI
              ON OSHI.send_history_id = OSH.id
             WHERE source = '".self::SOURCE_ID."'
                   AND OSHA.ident_nr IS NOT NULL
                   AND OSHA.ident_nr <> ''
                   AND OSHA.fv_nr IS NULL
                   AND (OSHI.status = '1' OR OSHI.status = '0' OR OSHI.status = '3')
                   AND OSH.status = '".self::NEW_INTERNAL_STATUS."'
                   AND DATE_ADD( OSH.date_send, INTERVAL 30 MINUTE ) <= NOW( )
             GROUP BY OSHA.ident_nr
               ";
    return $pDbMgr->GetCol('profit24', $sSql);
  }// end of getRRAzymutTransactions() method
  
  
  /**
   * Metoda pobiera zamówione pozycje w azymut, które
   * 
   * @global object $pDbMgr
   */
  private function getRRAzymutTransactons() {
    global $pDbMgr;
    
    $sSql = "SELECT OSHA.ident_nr 
             FROM orders_send_history AS OSH
             JOIN orders_send_history_attributes AS OSHA
              ON orders_send_history_id = OSH.id
             WHERE source = '".self::SOURCE_ID."'
                   AND OSHA.ident_nr IS NOT NULL
                   AND OSHA.ident_nr <> ''
                   AND OSHA.fv_nr IS NULL
                   AND DATE_ADD( OSH.date_send, INTERVAL 15 MINUTE ) <= NOW( )
             GROUP BY OSHA.ident_nr
               ";
    return $pDbMgr->GetCol('profit24', $sSql);
  }// end of getRRAzymutTransactions() method
  
  
  /**
   * Metoda grupuje rekordy na fakturze po azymut bookindeksie i zwraca tablice
   *  
   * @param object $xml
   * @return array (
   *  'index' => sumaryczna ilosc,
   *  ..
   *  )
   */
  private function _getInvoiceGroupByArray(&$xml) {
    $aInvoiceArray = array();

      foreach ($xml->linie->linia as $oItemInvoice) {
        $sSourceBookindeks = (string)$oItemInvoice->ind;
        $iCurrentQuantity = intval($oItemInvoice->ilosc);
        if (isset($aInvoiceArray[$sSourceBookindeks])) {
          $aInvoiceArray[$sSourceBookindeks]['quantity'] += $iCurrentQuantity;
        } else {
          $aInvoiceArray[$sSourceBookindeks]['quantity'] = $iCurrentQuantity;
        }
        $aInvoiceArray[$sSourceBookindeks]['vat'] = (int)$oItemInvoice->vat;
        $aInvoiceArray[$sSourceBookindeks]['cena_net'] = (float)$oItemInvoice->cena_net;
        $aInvoiceArray[$sSourceBookindeks]['cena_det'] = (float)$oItemInvoice->cena_det;
        $aInvoiceArray[$sSourceBookindeks]['rabat'] = (float)$oItemInvoice->rabat;
      }
      return $aInvoiceArray;
  }// end of _getInvoiceGroupByArray() method
  
  
  /**
   * Metoda obsługuje XML'a z fakturą z Azymutu
   * 
   * @param string $sResponseFilePath
   * @param int $sAzymutTransaction id traksakcji azymut
   * @throws Exception
   */
  public function parseXMLgetInv($sResponseFilePath, $sAzymutTransaction) {
    $oOrderMagazine = new orders\OrderMagazineData($this->pDbMgr);
    
    $iCount = 0;
    
    $xml = simplexml_load_file($sResponseFilePath);
    $sNrInvoice = (string)$xml->nr_dok_full;
    if ($sNrInvoice != '') {
      $iSendHistoryId = $this->_getSendHistoryId($sAzymutTransaction);

      $oOrderMagazine->addSetHistorySendAttr($iSendHistoryId, 'fv_nr', $sNrInvoice);

      $aInvoiceArray = $this->_getInvoiceGroupByArray($xml);
      if (!empty($aInvoiceArray)) {
        foreach ($aInvoiceArray as $sSourceBookindeks => $aOrderedItem) {
          if ($this->proceedOrderedItemsResponse($iSendHistoryId, $sSourceBookindeks, $aOrderedItem['quantity'], 6, $aOrderedItem['cena_net'], $aOrderedItem['vat'], $aOrderedItem['rabat']) === FALSE) {
          $sErr = 'Wystąpił błąd podczas zmiany statusu zamówienia'
              .' send_history_id: '.$iSendHistoryId
              .' bookindeks: '.$sSourceBookindeks;
          $this->aErr[] = $sErr;
          throw new Exception($sErr);
          } else {
          $iCount++;
          }
        }
      }



      if ($iCount > 0) {
        // jeśli jakiś element nie wystąpił na FV z tej listy, oznacza to że nie znajduje się na fakturze
        $aOrdersItemsList = $this->getListItemNotOnInvoice($iSendHistoryId);
        if (!empty($aOrdersItemsList) && is_array($aOrdersItemsList)) {
          foreach ($aOrdersItemsList as $aOItem) {
            // jeśli zrealizowano mniej, niż zamówiono, to zmiana statusu: '---'
            if ($this->_setOrderItemStatusNew($aOItem['id']) !== FALSE) {

            // przeliczenie zamówienia
            if ($this->oModuleZamowienia->recountOrder($aOItem['order_id'], $aOItem['payment_id']) === FALSE) {
              $bIsErr = TRUE;
            }
            } else {
             $bIsErr = TRUE;
            }
          }
        }
      }
    }
    
    return TRUE;
  }// end of parseXMLgetInv() method
  
  
  /**
   * Metoda pobiera składowe listy, które nie znalazły się na fakturze, ponieważ nie zostały zakupione
   * 
   * @global object $pDbMgr
   * @param int $iSendHistoryId
   * @return array(int)
   */
  private function getListItemNotOnInvoice($iSendHistoryId) {
    global $pDbMgr;
    
    $sSql = "SELECT OSH.item_id as id, O.payment_id, O.id as order_id
             FROM orders_send_history_items AS OSH
             JOIN orders_items AS OI
              ON OI.id = OSH.item_id
             JOIN orders AS O
              ON O.id = OI.order_id
             WHERE OSH.send_history_id = ".$iSendHistoryId. 
                 " AND OSH.status <> '6'";
    return $pDbMgr->GetAll('profit24', $sSql);
  }// end of getListItemNotOnInvoice() method
  
  
  /**
   * Metoda obsługuje odpowiedź z Azymut na temat staniu realizacji zamówienia
   * 
   * @param string $sResponseFilePath
   * @return string
   * @throws Exception
   */
  public function parseXMLpostRR($sResponseFilePath) {
    
    $xml = simplexml_load_file($sResponseFilePath);
    // dopasowanie listy
    $aOrderList = $xml->attributes();
    $iSendHistoryId = $this->_getSendHistoryId($aOrderList['orderId']);
    if ($iSendHistoryId > 0) {
      foreach ($xml->linieZam->liniaZ as $oItem) {
        $aOrderedItem = $oItem->attributes();

        //, $fPriceBrutto, $fPriceNetto, $fVat, $fDiscoun
        if ($this->proceedOrderedItemsResponse($iSendHistoryId, $aOrderedItem['indeks'], $aOrderedItem['iloscZreal'], 3) === FALSE) {
          $sErr = 'Wystąpił błąd podczas zmiany statusu zamówienia'
                  .' send_history_id: '.$iSendHistoryId
                  .' bookindeks: '.$aOrderedItem['indeks'];
          $this->aErr[] = $sErr;
          throw new Exception($sErr);
        }
      }
    } else {
      return FALSE;
    }
    
    // ustawienie statusu listy
    // bardziej pasuje to do potwierdzenia faktury
    // $this->setListStatus();
    
    return true;
  }// end of parseXMLpostRR() method
  
  
  /**
   * Metoda przetwarza ilość którą zwróciło źródło
   * 
   * @param int $iSendHistoryId
   * @param string $sSourceIndex
   * @param int $iOrderedQuantity
   * @param int $iCurrentQuantity
   * @param int $iStatus 0 - nowe wysłanem, 3 - aktualizacja, raport realizacji, 6 - zatwierdznie z fv
   * @return bool
   */
  public function proceedOrderedItemsResponse($iSendHistoryId, $sSourceIndex, $iCurrentQuantity, $iStatus, $fPriceNetto = NULL, $fVat = NULL, $fDiscount = NULL) {
    $bIsErr = FALSE;
    $iSumOrderedQuantity = 0;
    $aInformData = array();
    
    // odnalezienie pozycji z listy
    $aOItems = $this->_getOrderItemBy($iSendHistoryId, $sSourceIndex);
    foreach ($aOItems as $aOItem) {
//      $iOrderedQuantity = $aOItem['quantity'];
      
      // sumujemy każdą ze składowych
      // aby sprawdzić czy wystarczająco zostało zatwierdzonych
      $iSumOrderedQuantity += $aOItem['quantity'];
      if (!empty($aOItem) && is_array($aOItem)) {
        if ($this->_updateCurrentQuantity($iSendHistoryId, $aOItem['id'], intval($iCurrentQuantity), $iStatus, $fPriceNetto, $fVat, $fDiscount) !== FALSE) {
          // sprawdzenie, czy zarezerwowano, taką ilość, jaką zamówiono
          
          //  jeśli Azymut FV potwierdza mniejszą ilość niż zamówiono, wtedy należy,
          //   o ile jest to ilość > 0 to należy dodać nową pozycję do zamówienia
          if (intval($iCurrentQuantity) < intval($iSumOrderedQuantity)) {
            //mark unavailable for all websites for source
            $this->markAsUnavailableEverywhere(false, $sSourceIndex,$aOItem['product_id']);
            $iToConfirmInItem = $iCurrentQuantity - (intval($iSumOrderedQuantity) - $aOItem['quantity']);
            $iDefQuantity = $aOItem['quantity'] - $iToConfirmInItem;
            if ($aOItem['quantity'] > 1 && $iToConfirmInItem > 0 && $iDefQuantity > 0) {
            
              //@@ dodajemy duplikat tego produktu, na ilość jaka brakuje
              if ($this->addNewDuplicateItem($aOItem['id'], $iDefQuantity) === FALSE) {
                $bIsErr = TRUE;
              }
            
              // zamówiono więcej niż 1, oraz jest cokolwiek do potwierdzenia
              //@@ zmianiamy ilośc produktu aktualnego i potwierdzamy ilosć tylko taką jaka przyszła
              if ($this->changeOrderQuantity($aOItem['id'], $iToConfirmInItem) === FALSE) {
                $bIsErr = TRUE;
              }
            } else {
              // jeśli zrealizowano mniej, niż zamówiono, to zmiana statusu: '---'
              if ($this->_setOrderItemStatusNew($aOItem['id']) === FALSE) {
                $bIsErr = TRUE;
              }
            }
            
            // przeliczenie zamówienia
            if ($this->oModuleZamowienia->recountOrder($aOItem['order_id'], $aOItem['payment_id']) === FALSE) {
              $bIsErr = TRUE;
            }
            $aOrderTMP = $aOItem;
            $aOrderTMP['id'] = $aOrderTMP['order_id'];
            // zmiana statusu
            $aInformData[] = array(
                'new_status' => '0',
                'old_status' => self::NEW_INTERNAL_STATUS,
                'order' => $aOrderTMP
            );
          }
        } else {
          $bIsErr = TRUE;
        }
      } else {
        $bIsErr = TRUE;
      }
    }

    if ($bIsErr != TRUE && !empty($aInformData)) {
      $this->informAboutStatusChanged($aInformData);
    }
    return $bIsErr === TRUE ? FALSE : TRUE;
  }// end of proceedOrderedItemsResponse() method
  
  
  /**
   * Metoda zmienia aktualną ilość zamówionych pozycji
   * 
   * @global object $pDbMgr
   * @param int $iSendHistoryId
   * @param int $iOrderItemId
   * @param int $iCurrentQuantity
   * @param int $iStatus 0 - nowe wysłanem, 3 - aktualizacja, raport realizacji, 6 - zatwierdznie z fv
   * @return bool
   */
  public function _updateCurrentQuantity($iSendHistoryId, $iOrderItemId, $iCurrentQuantity, $iStatus, $fPriceNetto, $fVat, $fDiscount) {
    global $pDbMgr;
    
    $aValues = array(
        'current_quantity' => $iCurrentQuantity,
        'status' => $iStatus,
    );
    if ($fPriceNetto != NULL) {
      $aValues['price_netto'] = $fPriceNetto;
    }
    if ($fVat != NULL) {
      $aValues['vat'] = $fVat;
    }
    if ($fDiscount != NULL) {
      $aValues['discount'] = $fDiscount;
    }
    return $pDbMgr->Update('profit24', 'orders_send_history_items', $aValues, ' send_history_id = '.$iSendHistoryId.' AND item_id = '.$iOrderItemId);
  }// end of _updateCurrentQuantity() method
  
  
  /**
   * Metoda pobiera id wysłanej listy do zamówienia na podstawie jej numeru
   * 
   * @global object $pDbMgr
   * @param string $sAzymutTransaction Numer transakcji azymut
   * @return int
   */
  private function _getSendHistoryId($sAzymutTransaction) {
    global $pDbMgr;
    
    $sSql = "SELECT orders_send_history_id 
             FROM orders_send_history_attributes 
             WHERE ident_nr = '".$sAzymutTransaction."' LIMIT 1";
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of _getSendHistoryId method
  
  
  /**
   * Metoda pobiera składowe zamówień po id w źródle
   * 
   * @global object $pDbMgr
   * @param int $iSendHistoryId
   * @param string $sSourceBookindeks
   * @return int
   */
  private function _getOrderItemBy($iSendHistoryId, $sSourceBookindeks) {
    global $pDbMgr;
    
    $sSql = "SELECT OI.id, OI.order_id, O.payment_id, OI.quantity, O.order_number, OI.name, OI.isbn AS isbn_plain
             FROM orders_items AS OI
             JOIN orders AS O
              ON O.id = OI.order_id
             JOIN orders_send_history_items AS OSHI
              ON OI.id = OSHI.item_id
             WHERE 
              OI.source = '".self::SOURCE_ID."' AND
              OI.status <> '0' AND 
              OI.status <> '-1' AND
              OSHI.send_history_id = ".$iSendHistoryId." AND 
              OSHI.item_source_id = '".$sSourceBookindeks."'";
    return $pDbMgr->GetAll('profit24', $sSql);
  }// end of getOrderItemBy() method

  
  /**
   * Metoda pobiera składową zamówienia
   * 
   * @global object $pDbMgr
   * @param int $iOIId
   * @return array
   */
  private function getOrderItemAllData($iOIId) {
    global $pDbMgr;
    
    $sSql = 'SELECT * FROM orders_items WHERE id = '.$iOIId;
    return $pDbMgr->GetRow('profit24', $sSql);
  }// end of getOrderItemAllData() method
  
  
  /**
   * Metoda nową składową zamówienia, która jest duplikatem
   * 
   * @todo bundle getOrderItemAttachments
   * @param int $iOIId
   * @param int $iDefQuantity - ilość jaka nam brakuje
   * @return bool
   */
  public function addNewDuplicateItem($iOIId, $iDefQuantity) {
    global $pDbMgr;
    
    $aOrderItem = $this->getOrderItemAllData($iOIId);
    foreach ( $aOrderItem as $sCol => $mVal) {
      if ($mVal === NULL) {
        $aOrderItem[$sCol] = 'NULL';
      }
    }
    unset($aOrderItem['id']);
    
    $aOrderItem['sent_hasnc'] = '0';
    $aOrderItem['status'] = '0';
    $aOrderItem['quantity'] = $iDefQuantity;
    
    $aOrderItem = $this->changeOrderValueByNewQuantity($aOrderItem, $iDefQuantity, $aOrderItem);
    // @todo bundle getOrderItemAttachments
    return $pDbMgr->Insert('profit24', 'orders_items', $aOrderItem);
  }// end of addNewDuplicateItem() method

  
  /**
   * Metoda zmiania zamówioną przez klienta ilość, aktualnej pozycji
   * 
   * @param int $iOIId
   * @param int $iToConfirmInItem
   */
  public function changeOrderQuantity($iOIId, $iToConfirmInItem) {
    global $pDbMgr;
    
    
    $aOrderItem = $this->getOrderItemAllData($iOIId);
    $aValues = $this->changeOrderValueByNewQuantity($aOrderItem, $iToConfirmInItem);
    $aValues['quantity'] = $iToConfirmInItem;
    
    return $pDbMgr->Update('profit24', 'orders_items', $aValues, ' id = '.$iOIId);
  }// end of changeOrderQuantity() method
  
  
  /**
   * 
   * @param array $aOrderItem
   * @param int $iNewQuantity
   * @return array
   */
  private function changeOrderValueByNewQuantity($aOrderItem, $iNewQuantity, $aToUpdateItem = array()) {
    
    $aToUpdateItem['total_discount_currency'] = Common::formatPrice2($aOrderItem['discount_currency']*$iNewQuantity);
    $aToUpdateItem['value_netto'] = $iNewQuantity * $aOrderItem['promo_price_netto'];
    $aToUpdateItem['value_brutto'] = $iNewQuantity * $aOrderItem['promo_price_brutto'];
    $aToUpdateItem['total_vat_currency'] = Common::FormatPrice2($aToUpdateItem['value_brutto'] - $aToUpdateItem['value_netto']);
    return $aToUpdateItem;
  }// end of changeOrderValueByNewQuantity() method
  

  /**
   * Metoda ustawia zamówienie jako nowe
   * 
   * @global object $pDbMgr
   * @param int $iOItem
   * @return boolean
   */
  public function _setOrderItemStatusNew($iOItem) {
    global $pDbMgr;
    
    if ($iOItem > 0) {
      $aValues = array(
          'sent_hasnc' => '0',
          'status' => '0'
      );
      return $pDbMgr->Update('profit24', 'orders_items', $aValues, ' id='.$iOItem." AND source = '".self::SOURCE_ID."' AND status <> '4' ");
    } else {
      return FALSE;
    }
  }// end of _setOrderItemStatusNew method
  
  
  /**
   * Metoda dostosowuje tablicę danych do wysłania do Azymutu
   * 
   * @param array $aParams
   * @return array
   */
  private function _formatAzymutData($aParams) {
    foreach ($aParams['items'] as $iPKey => $aParam) {
      $aParams['items'][$iPKey]['bookindeks'] = $aParam['azymut_index'];
    }
    return $aParams;
  }// end of _formatAzymutData method
  
  
  /**
   * Metoda ustawia status listy do zamówienia i składowych
   * 
   * @param array $aParams
   * @param string $sFilePath
   * @param string $sAzymutIdTransakcji
   * @throws Exception
   */
  private function setOrdersListStatus($aParams, $sFilePath, $sAzymutIdTransakcji) {
    
    try {
      $oOrderMagazine = new orders\OrderMagazineData($this->pDbMgr);
      $oOrderMagazine->setOrderedItemsByStatus(self::NEW_INTERNAL_STATUS, $this->_getOrdersItemsList($aParams));
      $iSendHistoryId = $oOrderMagazine->addSendHistory($aParams, $sFilePath, self::NEW_INTERNAL_STATUS, self::SOURCE_ID, 'azymut');
      $oOrderMagazine->addSetHistorySendAttr($iSendHistoryId, 'ident_nr', $sAzymutIdTransakcji);
      
      $aSendHistoryLog = array(array(
          'type' => 'new_azymut_transaction',
          'number' => $aParams['azymut_order_id'],
          'send_history_id' => $iSendHistoryId,
          'id_transaction' => $sAzymutIdTransakcji
      ));
      $this->informAboutStatusChanged($aSendHistoryLog);
    } catch (Exception $exc) {
      throw new Exception($exc->getMessage());
    }
  }// end of setOrdersListStatus method
  
  
  /**
   * Metoda pobiera listę z id orders_items na podstawie tablicy do eksportu Azymut
   * 
   * @param array $aParams
   * @return string
   */
  private function _getOrdersItemsList($aParams) {
    
    $sItems = '';
    if (isset($aParams['items']) && !empty($aParams['items'])) {
      foreach ($aParams['items'] as $aItem) {
        $sItems .= $aItem['items'].',';
      }
    }
    return  substr($sItems, 0, -1);
  }// end of _getOrdersItemsList() method
  
  
  /**
   * Metoda zapisuje XML'a
   * 
   * @param string $sResponseFilePath
   * @param string $sXML
   * @throws Exception
   */
  private function saveXML($sResponseFilePath, $sXML) {

    if ($sXML !== FALSE && $sXML != '') {
      $mSave = file_put_contents($sResponseFilePath, $sXML);
      if ($mSave === FALSE) {
        $this->aErr[] = 'Wystąpił błąd podczas zapisu odpowiedzi z Azmymut do XML';
        throw new Exception('Wystąpił błąd podczas zapisu odpowiedzi z Azmymut do XML');
      }
    } else {
      $this->aErr[] = 'Azymut zwrócił pusty XML w odpowiedzi';
      throw new Exception('Azymut zwrócił pusty XML w odpowiedzi');
    }
    return TRUE;
  } // end of saveXML() method
  
  
  /**
   * Metoda wysyła XML'a do Azymutu
   * 
   * @global array $aConfig
   * @param string $sFilePath
   * @return int - id transakcji
   * @throws Exception
   */
  private function sendXMLOrder($sFilePath) {

    $sGETURL = 'http://www.azymut.pl/autoMw/main.olo';
//    $sGETURL = 'http://testprofit24.serwer/TEST/getData.php';
    $sResponseFilePath = $this->sCurrPath . DIRECTORY_SEPARATOR . 'files/' . DIRECTORY_SEPARATOR . date('YmdHis') . 'putOrder__response.XML';


    $aPOSTData = array(
        '_cmd' => 'putOrder',
        '_mode' => 'auto',
        'id' => $this->sLogin, // XXX TODO DOCELOWO usunac
        'passwd' => $this->sPasswd,
        'file1' => '@' . $sFilePath
    );
    $sXML = $this->CURL_file_get_contents($sGETURL, TRUE, $aPOSTData, 500);
    if ($sXML !== FALSE && $sXML != '') {
      $mSave = file_put_contents($sResponseFilePath, $sXML);
      if ($mSave === FALSE) {
        $this->aErr[] = 'Wystąpił błąd podczas zapisu odpowiedzi z Azmymut do XML';
        throw new Exception('Wystąpił błąd podczas zapisu odpowiedzi z Azmymut do XML');
      }
    } else {
      $this->aErr[] = 'Azymut zwrócił pusty XML w odpowiedzi';
      throw new Exception('Azymut zwrócił pusty XML w odpowiedzi');
    }

//    $sResponseFilePath = $this->sCurrPath.DIRECTORY_SEPARATOR.'files/'.DIRECTORY_SEPARATOR.'20130318123939putOrder__response.XML';
//    $sResponseFilePath = $this->sCurrPath.DIRECTORY_SEPARATOR.'files/'.DIRECTORY_SEPARATOR.'test_putOrder__response.XML';
    $xml = simplexml_load_file($sResponseFilePath);
    $aAttr = $xml->message->attributes();
    if ((string) $aAttr->code === '100.1') {
      $sIdTransakcji = trim((string) $xml->message->text);
    } else {
      $sErr = 'Wystąpił błąd podczas składania zamówienia' .
              ' KOD błędu: ' . (string) $aAttr->code .
              ' Komunikat błędu: ' . (string) $xml->message->text;
      $this->aErr[] = $sErr;
      throw new Exception($sErr);
    }
    return $sIdTransakcji;
  } // end of sendXMLOrder() method

  
  /**
   * Metoda buduje plik XML który wyśle zamówienie
   * 
      $aData = array(
        'azymut_order_id' => '2',
        'items' => array(
            array(
              'bookindeks' => '60919201622KS',
              'quantity' => '2'
            )
        )
      );
   * 
   * @param array $aData
   * @return boolean|string
   */
  public function xmlOrderBuilder($aData) {

    // specyfikacja najlepie opisana w http://www.azymut.pl/autoMw/DTD/zamowienie_2007-02.dtd
    if (!empty($aData['items'])) {

      $sXML = '<?xml version="1.0" encoding="iso-8859-2"?>
<!DOCTYPE zamowienie SYSTEM "http://www.azymut.pl/autoMw/DTD/zamowienie_2007-02.dtd"[]>
	<zamowienie idKlienta="' . $this->sLogin . '">
	<nrobcy>' . $aData['azymut_order_id'] . '</nrobcy>
  ';
      if ($aData['retry'] === TRUE) {
        // <!-- OPCJA: wystąpienie tego znacznika wymusi złożenie tego zamówienia, nawet jeśli istniało już jakieś z podanym wyżej numerem obcym -->
        $sXML .= '<ponow />';
      }

      // <!-- OPCJA: jesli pojawi sie ta flaga to zamowienia nie zostana dobite do obecnych oczekujacych do kompletacji zamowien-->
      $sXML .= '<nieDoklejac />';

      /*
        <!--	typ płatnosci
        PP - przelew
        PG - gotówka
        //-->
       */
      $sXML .= '<platnosc	typ="PP" trybLimitKredytowy="3" />';

      /*
        <!--
        sposób dostawy:
        2 - odbiór własny z Magazynu w Strykowie
        4 - dostawa Kurierem
        //-->
       */
      $sXML .= '
		<odbior typ="4" />
		<komentarz />
		<linieZam>';
      foreach ($aData['items'] as $aItem) {
        $sXML .= '<liniaZ indeks="' . $aItem['bookindeks'] . '" ilosc="' . $aItem['quantity'] . '" />';
      }
      $sXML .= '
		</linieZam>
	</zamowienie>      
';


      $sFilePath = $this->sCurrPath . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . date('YmdHis') . 'putOrder' . '.XML';

      $mSave = file_put_contents($sFilePath, $sXML);
      if ($mSave === FALSE) {
        $this->aErr[] = 'Wystąpił błąd podczas zapisu zamawianych pozycji w Azmymut do XML';
        throw new Exception('Wystąpił błąd podczas zapisu zamawianych pozycji w Azmymut do XML');
      }

      // Enable user error handling
      libxml_use_internal_errors(TRUE);

      $dom = new DOMDocument;
      $dom->Load($sFilePath);
      if (!$dom->validate()) {
        $this->aErr[] = $this->libxml_display_errors();
        throw new Exception("Wygenerowano nieprawidłowy plik XML");
      }

      return $sFilePath;
    }

    $this->aErr[] = 'Brak składowych do wysłania do Azymut';
    throw new Exception("Brak składowych do wysłania do Azymut");
  }// end of xmlOrderBuilder() method


  /**
   * Metoda wykonuje pobranie zamówień do wysłania
   * 
   * @return array - lista produktów
   */
  public function getAzymutOrdersToSend() {
    $aParams = array();
    
    $oOrderMagazine = new orders\OrderMagazineData($this->pDbMgr);
    $aSelectCols = array('CONCAT(O.id, \'-\', OI.product_id)',
        'OI.product_id',
        'GROUP_CONCAT(OI.id SEPARATOR \',\') as items',
        'OI.order_id',
        'SUM(OI.quantity) as quantity',
        'OI.shipment_date',
        '(OI.quantity * PS.azymut_wholesale_price) AS azymut_wholesale_price'
    );
    $aParams['items'] = $oOrderMagazine->getOrdersItemsSendToSource(self::SOURCE_ID, self::NEW_INTERNAL_STATUS, self::SOURCE_SYMBOL, $aSelectCols, $this->_getAzymutOrdersLimit());
    if (empty($aParams['items'])) {
//      throw new Exception('Niewystarczająca ilość produktów do wysłania');
      return array();
    }
    $aParams['azymut_order_id'] = $oOrderMagazine->getNewListNumber(self::NEW_INTERNAL_STATUS);
    if ($aParams['azymut_order_id'] == '') {
      throw new Exception('Błąd generowania numeru listy');
    }
    return $aParams;
  }// end of getAzymutOrdersToSend() method


  /**
   * Metoda pobiera limit zamówień dla azymutu
   * 
   * @global object $pDbMgr
   * @return string
   */
  private function _getAzymutOrdersLimit() {
    global $pDbMgr;

    $sSql = 'SELECT azymut_limit_price
             FROM orders_magazine_settings 
             WHERE id = 1';
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of getAzymutOrdersLimit() method
} // end of Class
