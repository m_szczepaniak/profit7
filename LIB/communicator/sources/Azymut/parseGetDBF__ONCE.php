<?php
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Azymut';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';
error_reporting(E_ALL);


/**
 * Klasa parsuje plik XML, a następnie towrzy i zapisuje plik DBF, jako wynikowy
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-12-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class ParseXMLOrdersToDBF {
  CONST FILE_PATH_DBF = '/files_DBF/';
  CONST FILE_PATH = '/files/';
  CONST FILE_OLD_PATH = '/files_old/';
  CONST DBF_BAZA_DANYCH_PATH = 'import/DBF/';
  
  private $aCols = array (
      0 => 
      array ('TOW_USL', 'N', 1, 0
      ),      1 => 
      array ('NAZWA_ART', 'C', 35, 0
      ),      2 => 
      array (
        'OPIS_ART','C',35,0,      
      ),      3 => 
      array (
        'NR_HANDL','C',20,0,      
      ),      4 => 
      array (
        'NR_KATAL','C',20,0,      
      ),  5 => 
      array (
        'PLU','N',6,0,
      ),      6 => 
      array (
        'BARKOD','C',20,0,
      ),      7 => 
      array (
        'IDENT_KAT','N',8,0,
      ),      8 => 
      array (
        'STAN','N',11,3,
      ),      9 => 
      array (
        'STAN_FAKT','N',11,3,
      ),      10 => 
      array (
        'STAN_MIN','N',11,3,
      ),      11 => 
      array (
        'STAN_MAX','N',11,3,
      ),      12 => 
      array (
        'CENA','N',11,2,
      ),      13 => 
      array (
        'CENAX','N',11,2,
      ),      14 => 
      array (
        'NARZUT_A','N',8,4,
      ),      15 => 
      array (
        'CENA_A','N',11,2,
      ),      16 => 
      array (
        'CENA_AX','N',11,2,
      ),      17 => 
      array (
        'UPUST_B','N',6,2,
      ),      18 => 
      array (
        'CENA_B','N',11,2,
      ),      19 => 
      array (
        'CENA_BX','N',11,2,
      ),      20 => 
      array (
        'UPUST_C','N',6,2,
      ),      21 => 
      array (
        'CENA_C','N',11,2,
      ),      22 => 
      array (
        'CENA_CX','N',11,2,
      ),      23 => 
      array (
        'CENAEWID','N',11,2,
      ),      24 => 
      array (
        'ZAMOWIENIA','N',11,3,
      ),      25 => 
      array (
        'ZAMREZER','N',11,3,
      ),      26 => 
      array (
        'ZAMOWDOST','N',11,3,
      ),      27 => 
      array (
        'CENAWALWE','N',15,4,
      ),      28 => 
      array (
        'NARZUTWAL','N',8,4,
      ),      29 => 
      array (
        'CENAWALWY','N',15,4,
      ),      30 => 
      array (
        'JEDN_M','C',7,0,
      ),      31 => 
      array (
        'JM_PODZIEL','L',1,0,
      ),      32 => 
      array (
        'STAWKA_VAT','N',5,2,
      ),      33 => 
      array (
        'ST_VAT_ZAK','N',5,2,
      ),      34 => 
      array (
        'SWW_KU','C',10,0,
      ),      35 => 
      array (
        'PKWIU','C',14,0,
      ),      36 => 
      array (
        'PCN','C',9,0,
      ),      37 => 
      array (
        'WAL_KOD','C',3,0,
      ),      38 => 
      array (
        'WAL_KURS','N',12,6,
      ),      39 => 
      array (
        'WAL_KURSS','N',12,6,
      ),      40 => 
      array (
        'WAL_DATA','D',8,0,
      ),      41 => 
      array (
        'CLO','N',6,2,      
      ),      42 => 
      array (
        'TRANSPORT','N',6,2,      
      ),      43 => 
      array (
        'AKCYZA_PR','N',6,2,
      ),      44 => 
      array (
        'AKCYZA','N',9,2,
      ),      45 => 
      array (
        'PODATEK','N',6,2,
      ),      46 => 
      array (
        'KOSZT_PROC','N',6,2,
      ),      47 => 
      array (
        'CERTYFIKAT','L',1,0,
      ),      48 => 
      array (
        'CERTYFOPIS','C',35,0,
      ),      49 => 
      array (
        'CERTYFDATA','D',8,0,
      ),      50 => 
      array (
        'PROD_MNOZ','N',11,3,
      ),      51 => 
      array (
        'LOKACJA','C',15,0,
      ),      52 => 
      array (
        'WAGA_JM_KG','N',10,4,
      ),      53 => 
      array (
        'ID_KAT','N',4,0,
      ),      54 => 
      array (
        'NAZWA_KAT','C',25,0,
      ),      55 => 
      array (
        'KSTAWKAVAT','N',6,2,
      ),      56 => 
      array (
        'CENAWE','N',13,2,
      ),      57 => 
      array (
        'CENAWEX','N',13,2,
      ),      58 => 
      array (
        'DATA_WAZN','D',8,0,
      ),      59 => 
      array (
        'NR_SERII','C',10,0,
      ),      60 => 
      array (
        'STAWKAVATP','N',5,2,
      ),      61 => 
      array (
        'DATA','D',8,0,
      ),      62 => 
      array (
        'ILOSC','N',11,3,      
      ),      63 => 
      array (
        'deleted','L',1,0
      )
    );

  function __construct() {
    // nazwa pliku to nazwainvoice_nrlisty.DBF
    $aFilesToGenerate = $this->_getListsToGenerate();
    $this->_parseFiles($aFilesToGenerate);
  }
  
  private function _parseFiles($aFilesToGenerate) {
    foreach ($aFilesToGenerate as $sFileToGenerate) {
     $this->_parseFile($sFileToGenerate);
     $this->_copyToOldFiles($sFileToGenerate); // TODO XXX
    }
  }
  
  private function _copyToOldFiles($sFileToGenerate) {
    rename(__DIR__.self::FILE_PATH.$sFileToGenerate, __DIR__.self::FILE_OLD_PATH.$sFileToGenerate);
  }
  
  
  private function _parseFile($sFileToGenerate) {
    // wyekstrachujemy szczegóły tego zamówienia
    $xml = simplexml_load_file(__DIR__.self::FILE_PATH.$sFileToGenerate);
    $sNrInvoice = (string)$xml->nr_dok_full;
    if ($sNrInvoice != '') {
      $this->_parseFileItems($xml, $sNrInvoice, $sFileToGenerate);
    }
  }
  
  private function _parseFileItems($xml, $sNrInvoice, $sFileToGenerate) {
    $aInvoiceArray = array();
    
    foreach ($xml->linie->linia as $oItemInvoice) {
      $sIsbn = str_replace('-', '', (string)$oItemInvoice->isbn);
      if (empty($sIsbn)) {
        $sIsbn = (string)$oItemInvoice->kod;
      }
      $aInvoiceArray[$sIsbn]['bookindex'] = (string)$oItemInvoice->ind;
      $aInvoiceArray[$sIsbn]['isbn'] = $sIsbn;
      $aInvoiceArray[$sIsbn]['ean'] = (string)$oItemInvoice->kod;
      $aInvoiceArray[$sIsbn]['tytul'] = (string)$oItemInvoice->tytul;
      $aInvoiceArray[$sIsbn]['pkwiu'] = (string)$oItemInvoice->pkwiu;
      $aInvoiceArray[$sIsbn]['vat'] = (int)$oItemInvoice->vat;
      $aInvoiceArray[$sIsbn]['cena_net'] = (float)$oItemInvoice->cena_net;
      $aInvoiceArray[$sIsbn]['cena_det'] = (float)$oItemInvoice->cena_det;
      $aInvoiceArray[$sIsbn]['rabat'] = (float)$oItemInvoice->rabat;
      $aInvoiceArray[$sIsbn]['wart'] = (float)$oItemInvoice->wart;
      
      $iCurrentQuantity = intval($oItemInvoice->ilosc);
      if (isset($aInvoiceArray[$sIsbn]['ilosc'])) {
        $aInvoiceArray[$sIsbn]['ilosc'] += $iCurrentQuantity;
      } else {
        $aInvoiceArray[$sIsbn]['ilosc'] = $iCurrentQuantity;
      }
    }

    $aInvoiceArray = $this->_getArrRecordsDBF($aInvoiceArray, array_keys($aInvoiceArray));
    $aInvoiceArray = $this->_defineEmptyProducts($aInvoiceArray);
    if (!empty($aInvoiceArray)) {
      $sNewDBFName = substr(str_replace('/', '', $sNrInvoice), 0, 7).'.DBF';//.'_'.str_replace('getInv__response.HTML', '', $sFileToGenerate).'
      $iDB = dbase_create(__DIR__.self::FILE_PATH_DBF.$sNewDBFName, $this->aCols);
      foreach ($aInvoiceArray as $aRecord) {
        dbase_add_record($iDB, array_values($aRecord));
      }
      dbase_close($iDB);
      echo 'Wygenerowano: <a href="https://www.profit24.pl/LIB/communicator/sources/Azymut/'.self::FILE_PATH_DBF.$sNewDBFName.'">'.$sNewDBFName.'</a><br />';
    }
  }
  
  
  private function _defineEmptyProducts($aInvoiceItems) {
    foreach ($aInvoiceItems as $sIsbn => $aItem) {
      if (!isset($aItem['CENA'])) {
        $aInvoiceItems[$sIsbn] = $this->_defineProductData($aItem);
      }
    }
    return $aInvoiceItems;
  }
  
  private function _defineProductData($aInvoiceData) {
    $aRecordTMP = $this->_getEmptyArrColsDBF($this->aCols);
    
    $aRecordTMP['CENA'] = $aInvoiceData['cena_net'];
    $aRecordTMP['CENAX'] = Common::formatPrice2($aInvoiceData['cena_net'] * (1 + $aInvoiceData['vat']/100));
    $aRecordTMP['CENA_A'] = $aInvoiceData['cena_det'];
    $aRecordTMP['CENA_AX'] = Common::formatPrice2($aInvoiceData['cena_det'] * (1 + $aInvoiceData['vat']/100));

//    $aRecordTMP['STAWKA_VAT'] = $aInvoiceData['vat'];
    
    
    $aRecordTMP['TOW_USL'] = 1;
    $aRecordTMP['NAZWA_ART'] = iconv("UTF-8", "ISO-8859-1//TRANSLIT", trimString($aInvoiceData['tytul'], 27));
    $aRecordTMP['OPIS_ART'] = $aInvoiceData['bookindex'];
    $aRecordTMP['NR_HANDL'] = 1002;
    $aRecordTMP['NR_KATAL'] = '98'.$aInvoiceData['bookindex'];
    $aRecordTMP['PLU'] = '0';
    $aRecordTMP['BARKOD'] = $aInvoiceData['ean'];
    $aRecordTMP['IDENT_KAT'] = 1002;
//    $aRecordTMP['STAN'] = 0;
//    $aRecordTMP['STAN'] = 0;
//    $aRecordTMP['STAN_FAKT'] = 0;
//    $aRecordTMP['STAN_MIN'] = 0;
//    $aRecordTMP['STAN_MAX'] = 9999999.990;
//    $aRecordTMP['NARZUT_A'] = 0;
//    $aRecordTMP['UPUST_B'] = 0;
//    $aRecordTMP['CENA_B'] = 0;
//    $aRecordTMP['CENA_BX'] = 0;
//    $aRecordTMP['UPUST_C'] = 0;
//    $aRecordTMP['CENA_C'] = 0;
//    $aRecordTMP['CENA_CX'] = 0;
//    $aRecordTMP['CENAEWID'] = 0;
//    $aRecordTMP['ZAMOWIENIA'] = 0;
//    $aRecordTMP['ZAMREZER'] = 0;
//    $aRecordTMP['ZAMOWDOST'] = 0;
//    $aRecordTMP['CENAWALWE'] = 0;
//    $aRecordTMP['NARZUTWAL'] = 0;
//    $aRecordTMP['CENAWALWY'] = 0;
    $aRecordTMP['JEDN_M'] = 'egz.';
//    $aRecordTMP['JM_PODZIEL'] = FALSE;
    $aRecordTMP['STAWKA_VAT'] = $aInvoiceData['vat'];
    $aRecordTMP['ST_VAT_ZAK'] = $aInvoiceData['vat'];
//    $aRecordTMP['SWW_KU'] = '';
    $aRecordTMP['PKWIU'] = $aInvoiceData['pkwiu'];
//    $aRecordTMP['PCN'] = '';
//    $aRecordTMP['WAL_KOD'] = '';
//    $aRecordTMP['WAL_KURS'] = 0;
//    $aRecordTMP['WAL_KURSS'] = 0;
//    $aRecordTMP['WAL_DATA'] = '';
//    $aRecordTMP['CLO'] = 0;
//    $aRecordTMP['TRANSPORT'] = 0;
//    $aRecordTMP['AKCYZA_PR'] = 0;
//    $aRecordTMP['AKCYZA'] = 0;
//    $aRecordTMP['PODATEK'] = 0;
//    $aRecordTMP['KOSZT_PROC'] = 0;
//    $aRecordTMP['CERTYFIKAT'] = TRUE;
//    $aRecordTMP['CERTYFDATA'] = $aInvoiceData['isbn'];
//    $aRecordTMP['PROD_MNOZ'] = 1;
    
    $aRecordTMP['CERTYFIKAT'] = 'T';
    $aRecordTMP['CERTYFOPIS'] = $aInvoiceData['isbn'];
    $aRecordTMP['CERTYFDATA'] = date('Ymd');
    $aRecordTMP['ID_KAT'] = 1002;
    $aRecordTMP['NAZWA_KAT'] = 'PROFIT24,COM,PL';
    $aRecordTMP['KSTAWKAVAT'] = $aInvoiceData['vat'];
    $aRecordTMP['CENAWE'] = $aRecordTMP['CENA'];
    $aRecordTMP['CENAWEX'] = $aRecordTMP['CENAX'];
    $aRecordTMP['STAWKAVATP'] = $aRecordTMP['STAWKA_VAT'];
    $aRecordTMP['DATA'] = date('Ymd');
    $aRecordTMP['ILOSC'] = $aInvoiceData['ilosc'];
    $aRecordTMP['deleted'] = 0;
    return $aRecordTMP;
  }
  
  
  /**
   * Metoda generuje tablicę na postawie schematu kolumn tabeli
   * 
   * @param array $aColsArra
   * @return string
   */
  private function _getEmptyArrColsDBF($aColsArra) {
    
    $aRow = array();
    foreach ($aColsArra as $aCol) {
      $aRow[$aCol[0]] = '';
    }
    return $aRow;
  }// end of _getEmptyArrColsDBF() method
  
  
  /**
   * Metoda przeszukuje DBF'a w poszukiwaniu rekordów o przekazanych isbn
   * 
   * @global array $aConfig
   * @param array $aProductsItems
   * @param array $aISBN tablica ISBN
   * @return array
   */
  private function _getArrRecordsDBF($aProductsItems, $aISBN) {
    global $aConfig;
    
    $iDBF = dbase_open($_SERVER['DOCUMENT_ROOT'].'/'.self::DBF_BAZA_DANYCH_PATH.'ARTYKULY_BAZA_DANYCH.DBF','0');
    $iCountRecords = dbase_numrecords($iDBF);
    for ($i = 1; $i <= $iCountRecords; $i++) {
      $aRecordTMP = dbase_get_record_with_names($iDBF, $i);
      foreach ($aRecordTMP as &$sCol) {
        $sCol = trim($sCol);
      }
      
      $aMatchedKeys = array_keys($aISBN, $aRecordTMP['CERTYFOPIS']);
//      if (!empty($aMatchedKeys)) {
//        var_dump($aRecordTMP['CERTYFOPIS']);
//        var_dump($aISBN);
//        var_dump($aMatchedKeys);die;
//      }
      foreach ($aMatchedKeys as $iMatchedProductsItemKey) {
        if ($iMatchedProductsItemKey !== FALSE) {
          $sKeyIsbn = $aISBN[$iMatchedProductsItemKey];
          // jeśli już zdefiniowany, a ilość większa od 1 to nadpisujemy
          
          if (
                  isset($aProductsItems[$sKeyIsbn]) && 
                  isset($aProductsItems[$sKeyIsbn]['STAN']) && 
                  isset($aRecordTMP['STAN']) &&
                  ($aRecordTMP['STAN'] > $aProductsItems[$sKeyIsbn]['STAN'])) {
            // nadpisujemy
            $aProductsItems[$sKeyIsbn] = $this->_overwriteParamsWHChange($aProductsItems[$sKeyIsbn], $aRecordTMP);
          } elseif (!isset($aProductsItems[$sKeyIsbn]['STAN'])) {
            // definiujemy
            $aProductsItems[$sKeyIsbn] = $this->_overwriteParams($aProductsItems[$sKeyIsbn], $aRecordTMP);
          }
          
          /*
          if (!isset($aProductsItems[$sKeyIsbn]['STAN'])) {
            // definiujemy
            $aProductsItems[$sKeyIsbn] = $this->_overwriteParams($aProductsItems[$sKeyIsbn], $aRecordTMP);
          }
            */
        }
      }
    }
    return $aProductsItems;
  }// end of _getArrRecordsDBF() method
  
  
  /**
   * Metoda nadpisuje wszystkie pola z tablicy źródłowej do docelowej, poza wybranymi
   * 
   * @param array $aSourceArr
   * @param array $aDestArr
   * @return array
   */
  private function _overwriteParamsWHChange($aSourceArr, $aDestArr) {
    $aMergedArray = array();
    $aColsToMoveFromSource = array(
        'CENA', 'CENAX', 'CENA_A', 'CENA_AX', 'CENAWE', 'CENAWEX', 'STAWKA_VAT', 'STAWKAVATP', 'KSTAWKAVAT', 'ILOSC', 'DATA', 'deleted', 'CERTYFIKAT'
    );
    
    foreach ($aSourceArr as $sColName => $sValue) {
      if (in_array($sColName, $aColsToMoveFromSource)) {
        // jeśli, jest w grupie kolumn do przeniesienia z tabli źródłowej
        $aMergedArray[$sColName] = $sValue;
      } else {
        // wszystkie inne pobierz z tablicy docelowej
        $aMergedArray[$sColName] = $aDestArr[$sColName];
      }
    }
    return $aMergedArray;
  }// end of _overwriteParamsWHChange() method
  
  
  /**
   * Nadpisujemy pola, jeśli pozycja istnieje w DBF
   * 
   * @param array $aInvoiceData
   * @param array $aRecordTMP
   * @return array
   */
  private function _overwriteParams($aInvoiceData, $aRecordTMP) {
    $aRecordTMPNEW = $this->_getEmptyArrColsDBF($this->aCols);
    
    $aRecordTMP['CENA'] = $aInvoiceData['cena_net'];
    $aRecordTMP['CENAX'] = Common::formatPrice2($aInvoiceData['cena_net'] * (1 + $aInvoiceData['vat']/100));
    $aRecordTMP['CENA_A'] = $aInvoiceData['cena_det'];
    $aRecordTMP['CENA_AX'] = Common::formatPrice2($aInvoiceData['cena_det'] * (1 + $aInvoiceData['vat']/100));
    $aRecordTMP['CENAWE'] = $aRecordTMP['CENA'];
    $aRecordTMP['CENAWEX'] = $aRecordTMP['CENAX'];
    $aRecordTMP['STAWKA_VAT'] = $aInvoiceData['vat'];
    $aRecordTMP['STAWKAVATP'] = $aRecordTMP['STAWKA_VAT'];
    $aRecordTMP['KSTAWKAVAT'] = $aInvoiceData['vat'];
    $aRecordTMP['ILOSC'] = $aInvoiceData['ilosc'];
    $aRecordTMP['ID_KAT'] = 1002;
    $aRecordTMP['NAZWA_KAT'] = 'PROFIT24,COM,PL';
    $aRecordTMP['DATA'] = date('Y-m-d');
    $aRecordTMP['NR_SERII'] = "";
    $aRecordTMP['DATA_WAZN'] = "";
    $aRecordTMP['deleted'] = 0;
    $aRecordTMP['CERTYFIKAT'] = 'T';
    
    $aRecordTMP = array_merge($aRecordTMPNEW, $aRecordTMP);
    
    return $aRecordTMP;
  }// end of _overwriteParams() method
  
  
  /**
   * Metoda pobiera listę plików do wygenerowania DBF
   * 
   * @return type
   */
  private function _getListsToGenerate() {
    $aInvoices = array();
    
    $aParsedInvoices = scandir(__DIR__.self::FILE_PATH_DBF);
    if ($handle = opendir(__DIR__.self::FILE_PATH)) {
      while (false !== ($sFileName = readdir($handle))) {
        if ($sFileName != "." && $sFileName != ".." && $this->_testIsInvoice($sFileName, $aParsedInvoices)) {
          $aInvoices[] = $sFileName;
        }
      }
      closedir($handle);
    }
    return $aInvoices;
  }// end of _getListsToGenerate() method
  
  
  /**
   * Metoda sprawdza czy plik jest niepustą fakturą
   * 
   * @param string $sFileName
   * @param array $aParsedInvoices
   * @return bool
   */
  private function _testIsInvoice($sFileName, $aParsedInvoices) {
    
    if (stristr($sFileName, 'getInv__response') && filesize(__DIR__.self::FILE_PATH.$sFileName) != 166) {
      foreach ($aParsedInvoices as $sParsedFileName) {
        if (strstr($sParsedFileName, str_replace('getInv__response.HTML', '', $sFileName))) {
          return false;
        }
      }
      // już tu doszedł to znaczy że należy sparsować
      return true; 
    }
    return false;
  }// end of _testIsInvoice() method
}

$oParseXMLOrdersToDBF = new ParseXMLOrdersToDBF();
