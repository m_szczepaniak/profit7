<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Azymut';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

$aOmmitOIds = [];
/**
 * 
 * @global DatabaseManager $pDbMgr
 * @param string $sAzymutBookindex
 * @return boolean
 */
function getOrderId($sAzymutBookindex) {
  global $pDbMgr, $aOmmitOIds;
  
  $sSql = 'SELECT id
           FROM products
           WHERE azymut_index = "' . $sAzymutBookindex . '"
           ORDER BY id DESC
           LIMIT 1
           ';
  $iOrderId = $pDbMgr->GetOne('profit24', $sSql);
  if ($iOrderId > 0) {
    $aOmmitOIds[$iOrderId] = true;
    return $iOrderId;
  }
  
  $sSql = 'SELECT product_id
           FROM products_azymut_bookindex
           WHERE azymut_index = "'.$sAzymutBookindex.'"
           ORDER BY product_id DESC
           LIMIT 1';
  $iOrderId = $pDbMgr->GetOne('profit24', $sSql);
  if ($iOrderId > 0) {
    if (isset($aOmmitOIds[$iOrderId])) {
      return false;
    } else {
      return $iOrderId;
    }
  }
  return false;
}

/**
 * 
 * @global DatabaseManager $pDbMgr
 * @param string $sWeight
 * @param int $iOrderId
 * @return bool
 */
function updateWeight($sWeight, $iOrderId) {
  global $pDbMgr;
  
  $aValues = [
      'weight' => $sWeight
  ];
  return $pDbMgr->Update('profit24', 'products', $aValues, ' id = '.$iOrderId.' LIMIT 1');
}


$aItems = file(__DIR__.'/files/WBD_waga.csv');

foreach ($aItems as $aItem) {
  $aItem = str_getcsv($aItem, "\t", '');
  $fWeight = str_replace(',', '.', $aItem[1]);
  if ($fWeight > 0.00) {
    $iOrderId = getOrderId($aItem[0]);
    if ($iOrderId > 0) {
      updateWeight($fWeight, $iOrderId);
    }
  }
}
