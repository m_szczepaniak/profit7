<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\TBA;
class Shipment implements \communicator\sources\iShipment {

  public $aTransportSettings;
  public $bTestMode;
  public $pDbMgr;
  
  /**
   *
   * @var api $oEN
   */
  public $tba;
  
  CONST KWOTWA_UBEZPIECZENIA = 5000.00;// ustalone mailowo
  
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    
    $this->init();
  }
  
  /**
   * 
   */
  private function init() {
    
    //$this->aTransportSettings = $this->getTransportSettings($iWebsiteId);
            
    $options = array();
    $options["login"] = $this->aTransportSettings['login'];
    $options["password"] = $this->aTransportSettings['password'];
    $options["trace"] = 1;
    $options['connection_timeout'] = 10;
    
    include_once('api.class.php');
    $this->tba = new api($this->aTransportSettings['api_url'], $options);
  }

  /**
   * 
   * @param array $aOrder
   * @param array $aDeliverAddress
   * @param array $aSellerData
   */
  public function addShipment($aOrder, $aDeliverAddress, $aSellerData) {
    global $aConfig;
    
    $CreateOrder = new CreateOrder();
    $CreateOrder->addidtionalInfo = $aOrder['transport_remarks'];
    $CreateOrder->InsuranceAmount = self::KWOTWA_UBEZPIECZENIA;
    $CreateOrder->ServiceType = 1; 
    $CreateOrder->Parcels = $this->getParcels($aOrder['weight'], $aOrder['transport_packages']);
    $CreateOrder->Payer = $this->getPayerData($aSellerData);
    $CreateOrder->cod = $this->GetCOD($aOrder);
    $CreateOrder->delivery = $this->getDelivery($aDeliverAddress);
    $CreateOrder->pickup = $this->getPickup($aSellerData);
    $CreateOrder->login = $this->aTransportSettings['login'];
    $CreateOrder->password = $this->aTransportSettings['password'];
    $CreateOrder->ROD = FALSE;
    $CreateOrder->ReadyDate = date('c');
    
    try{
      $CreateOrder->ServiceID = 36; ////TO i tak na sztywno nam podali, więc nie ma sensu $this->getServiceId($CreateOrder);
      $oReturn = new CreateOrderResponse();
      $oReturn->CreateOrderResult = new Order();
      $oReturn = $this->tba->CreateOrder($CreateOrder);
      $sTransportNumber = $oReturn->CreateOrderResult->LetterNo;
      if (!empty($sTransportNumber)) {
        $sFilePatch = $this->getTransportListFilename($aConfig, $sTransportNumber);
        file_put_contents($sFilePatch, $oReturn->CreateOrderResult->MimeData);
        return $sTransportNumber;
      } else {
        throw new Exception(_('Brak nr listu przewozowego'));
      }
      return $sTransportNumber;
    } catch (\Exception $ex) {
      throw new \Exception(_(' Wystąpił błąd podczas pobierania listu przewozowego z TBA: ').$ex->getMessage());
    }
    return false;
  }
  
  
  /**
   * 
   * @param type $aSellerData
   * @return \communicator\sources\TBA\Location
   */
  private function getPickup($aSellerData) {
    
    $location = new Location();
    $location->Name = $aSellerData['paczkomaty_name'];
    $location->Address = $this->aTransportSettings['street'] . ' ' . $this->aTransportSettings['number'] . ' / ' . $this->aTransportSettings['number2'];
    $location->City = $this->aTransportSettings['city'];
    $location->PostCode = $this->aTransportSettings['postal'];
    $location->CountryCode = 'PL';
    return $location;
  }
  
  /**
   * 
   * @param array $aDeliverAddresss
   * @return \communicator\sources\TBA\Location
   */
  private function getDelivery($aDeliverAddresss) {
    
    $sName = $aDeliverAddresss['name']. ' '.$aDeliverAddresss['surname'];
    $location =  new Location();
    $location->Person = $sName;
    $location->Name = ($aDeliverAddresss['company'] != '' ? $aDeliverAddresss['company'] : $sName);
    $location->Address = $aDeliverAddresss['street'] . ' ' . $aDeliverAddresss['number'] . ' / ' . $aDeliverAddresss['number2'];
    $location->City = $aDeliverAddresss['city'];
    $location->PostCode = $aDeliverAddresss['postal'];
    $location->CountryCode = 'PL';
    $location->Contact = $aDeliverAddresss['phone'];
    return $location;
  }
  
  /**
   * 
   * @param array $aOrder
   */
  private function GetCOD($aOrder) {
    
    if ($this->checkOrderToPay($aOrder)) {
      $cod = new COD();
      $cod->Amount = $this->getOrderToPay($aOrder['to_pay'], $aOrder['paid_amount']);
      $cod->RetAccountNo = $this->aTransportSettings['COD_RetAccount'];
      return $cod;
    } else {
      $cod = new COD();
      $cod->Amount = 0;
      return $cod;
    }
  }
  
  
  /**
   * 
   * @param array $aOrder
   * @return float
   */
  private function getOrderToPay($fToPay, $fPayAmount) {
    return ($fToPay - $fPayAmount);
  }
  
  /**
   * 
   * @param array $aSellerData
   * @return \communicator\sources\TBA\Location
   */
  private function getPayerData($aSellerData) {
    
    $location = new Location();
    $location->ClientNo = $this->aTransportSettings['client_id'];
    $location->Name = $aSellerData['invoice_name'];
    $location->Address = $aSellerData['street'] . ' ' . $aSellerData['number'] . ' / ' . $aSellerData['number2'];
    $location->City = $aSellerData['city'];
    $location->PostCode = $aSellerData['postal'];
    $location->CountryCode = 'PL';
    return $location;
  }// end of getPayerData() method
  
  /**
   * 
   * @param float $fOrderWeight
   * @param int $iCountPackages
   * @return \communicator\sources\TBA\Parcel
   */
  private function getParcels($fOrderWeight, $iCountPackages) {
    $iCountPackages = ($iCountPackages == 0 ? 1 : $iCountPackages);
    $aParcels = array();
    for ($i=0; $i < $iCountPackages; $i++) {
      $tmpParcel = new Parcel();
      $tmpParcel->Weight = ($fOrderWeight / $iCountPackages);
      $tmpParcel->Type = ParcelType::Package;
      $tmpParcel->D =0;
      $tmpParcel->S =0;
      $tmpParcel->W =0;
      $aParcels[] = $tmpParcel;
    }
    return $aParcels;
  }
  
  
  /**
   * Metoda sprawdza, czy zamówienie jest do opłacenia, przy odbiorze
   * 
   * @param array $aOrder
   * @return boolean
   */
  private function checkOrderToPay($aOrder) {
    
    if ( ($aOrder['payment_type'] == 'postal_fee' || $aOrder['second_payment_type'] == 'postal_fee') && 
              ($aOrder['to_pay'] - $aOrder['paid_amount']) > 0) {
      return true;
    } else {
      return false;
    }
  }// end of checkOrderToPay() method
  
  

  public function doCancelShipment() {
    
  }

  public function doPrintAddressLabel($sFilePath, $sPrinter) {
    global $pDbMgr;
    
    $aValues = array(
        'filetype' => 'PDF',
        'filepath' => $sFilePath,
        'dest_printer' => $sPrinter,
        'created' => 'NOW()'
    );
    if ($pDbMgr->Insert('profit24', 'print_queue', $aValues) === false) {
      return false;
    }
    return true;
  }

  public function getAddressLabel($sFilePath, $iOrderId) {
    
  }

  public function getDeliveryStatus($iId, $sTransportNumber) {

    $GetOrderStatus = new \communicator\sources\TBA\GetOrderStatus();
    $GetOrderStatus->referenceNo = $sTransportNumber;

    $GetOrderStatusResponse = new GetOrderStatusResponse();
    $GetOrderStatusResponse->GetOrderStatusResult = new GetOrderStatusResponse();
    $GetOrderStatusResponse = $this->tba->GetOrderStatus($GetOrderStatus);    

    $aStatusesCollection = array();  
    foreach ($GetOrderStatusResponse as $oOrderStatus) {
    
      if (isset($oOrderStatus->CurrentStatus) && isset($oOrderStatus->CreateDate)) {
        $oDeliveryShipment = new \orders\Shipment\DeliveryShipment();
        $oDeliveryShipment->sStatusName = $oOrderStatus->CurrentStatus;  
        $oDeliveryShipment->sStatusDate = $oOrderStatus->CreateDate;
        $aStatusesCollection[] = $oDeliveryShipment;
      }
    }  
    
    return $aStatusesCollection;
  }

  /**
   * 
   * @param array $agConfig
   * @param string $sTransportNumber
   * @return string
   */
  public function getTransportListFilename($agConfig, $sTransportNumber) {
    
    $sFilename = 'ListPrzewozowyTBA_'.$sTransportNumber.'.pdf';
    return $agConfig['common']['client_base_path'].'/'.$agConfig['common']['cms_dir'].$agConfig['common']['tba_dir'].$sFilename;
  }// end of getTransportListFilename() method

}

