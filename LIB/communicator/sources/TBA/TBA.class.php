<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\TBA;
class TBA implements \communicator\sources\iShipment {

  private $pDbMgr;
  private $bTestMode;
  private $aTransportSettings;
  
  /**
   *
   * @var api $oEN
   */
  public $tba;
  
  private $iTransportId = 7;// symbol 'tba'
  CONST KWOTWA_UBEZPIECZENIA = 5000.00;// ustalone mailowo
  
  function __construct($bTestMode = false, $pDbMgr = null) {
    $this->bTestMode = $bTestMode;
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @param int $iWebsiteId
   */
  private function init($iWebsiteId) {
    
    $this->aTransportSettings = $this->getTransportSettings($iWebsiteId);
            
    $options = array();
    $options["login"] = $this->aTransportSettings['login'];
    $options["password"] = $this->aTransportSettings['password'];
    $options["trace"] = 1;
    $options["connection_timeout"] = 30;
    include_once('api.class.php');
    $this->tba = new api($this->aTransportSettings['api_url'], $options);
  }
  
  /**
   * 
   * @param int $iWebsiteId
   * @return array
   */
  private function getTransportSettings($iWebsiteId) {
    $oKeyValue = new \table\keyValue('orders_transport_means', $this->getWebsiteSymbol($iWebsiteId));
    return $oKeyValue->getAllByIdDest($this->iTransportId);
  }// end of getTransportSettings() method
  
  
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @global type $aConfig
	 * @param type $iWebsiteId
	 * @return type 
	 */
	private function getWebsiteSymbol($iWebsiteId) {
		global $pDbMgr;
		
		$sSql = "SELECT code 
             FROM websites
						 WHERE id=".$iWebsiteId;
		return $pDbMgr->GetOne('profit24', $sSql);
	}// end of getWebsiteSymbol() method

  
  

  
  
  /**
   * 
   * @param CreateOrder $CreateOrder
   */
  private function getServiceId($CreateOrder) {
    $GetAvailableServices = new GetAvailableServices();
    $GetAvailableServices->InsuranceAmount = $CreateOrder->InsuranceAmount;
    $GetAvailableServices->Parcels = $CreateOrder->Parcels;
    $GetAvailableServices->Payer = $CreateOrder->Payer;
    $GetAvailableServices->ReadyDate = date('c');
    $GetAvailableServices->ServiceType = $CreateOrder->ServiceType;
    $GetAvailableServices->cod = $CreateOrder->cod;
    $GetAvailableServices->delivery = $CreateOrder->delivery;
    $GetAvailableServices->login = $CreateOrder->login;
    $GetAvailableServices->password = $CreateOrder->password;
    $GetAvailableServices->pickup = $CreateOrder->pickup;
    $GetAvailableServices->ROD = $CreateOrder->ROD;
    
    $Services = new GetAvailableServicesResponse();
    $Services->GetAvailableServicesResult = new Service();
    try {
      $Services = $this->tba->GetAvailableServices($GetAvailableServices);
    } catch (\Exception $ex) {
       dump($this->tba->__getLastRequest());
    }
    foreach ($Services->GetAvailableServicesResult->Service as $Service) {
      return $Service->ID;
    }
  }


  public function doCancelShipment() {
    
  }

  public function doPrintAddressLabel($sFilePath, $sPrinter) {
  }

  public function getAddressLabel($sFilePath, $iOrderId) {
    
  }

  

  public function getDestinationPointsList() {
  }

  public function addShipment($aOrder, $aDeliverAddress, $aWebsiteSettings) {
    
  }

  public function getDeliveryStatus($iId, $sTransportNumber) {
    
  }

  public function getTransportListFilename($agConfig, $sTransportNumber) {
    
  }

}
