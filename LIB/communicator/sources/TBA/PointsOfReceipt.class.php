<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\TBA;
class PointsOfReceipt extends TBA implements \communicator\sources\iPointsOfReceipt
{

  private $bTest;
  
  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var WebServiceTBATest
   */
  public $TBA;
  

	public function __construct($pDbMgr, $bTest) {
    global $pDbMgr;
    
    parent::__construct($bTest);
    $this->pDbMgr = $pDbMgr;
    $this->bTest = $bTest;
    if (is_null($this->pDbMgr)) {
      $this->pDbMgr = $pDbMgr;
    }
  }


  /**
   * Metoda sprawdza, czy przekazany kod pocztowy znajduje się w obszarze doręczeń
   * 
   * param string $sPostcode
   * @return bool
   */
  public function validatePostcode($sPostcode) {
    
    $sSql = "SELECT `desc`
             FROM shipping_points_tba 
             WHERE `desc` = '".$sPostcode."'";

    return ($this->pDbMgr->GetOne('profit24', $sSql) == true ? true : false); 
  }

  public function checkDestinationPointAvailable($sPointOfReceipt) {
    return $this->validatePostcode($sPointOfReceipt);
  }

  public function getDestinationPointsList() {}

  public function getSinglePointDetails($sPointOfReceipt) {}

  public function getDestinationPointsDropdown() {
    
    $sSql = "SELECT postcode AS value, `desc` AS label
             FROM shipping_points_tba 
             ORDER BY `desc` ";
    return $this->pDbMgr->GetAll('profit24', $sSql); 
  }
}
