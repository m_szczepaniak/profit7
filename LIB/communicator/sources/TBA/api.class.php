<?php
namespace communicator\sources\TBA;
class GetOrderStatus {
  public $referenceNo; // string
}

class GetOrderStatusResponse {
  public $GetOrderStatusResult; // OrderStatus
}

class OrderStatus {
  public $LetterNo; // string
  public $OrderNo; // string
  public $CreateDate; // dateTime
  public $PickupDate; // dateTime
  public $DeliveryDate; // dateTime
  public $CurrentStatus; // string
  public $DeliveredToPerson; // string
  public $ExtLetterNo; // string
}

class GetAvailableServices {
  public $login; // string
  public $password; // string
  public $ServiceType; // int
  public $Payer; // Location
  public $pickup; // Location
  public $delivery; // Location
  public $ReadyDate; // dateTime
  public $Parcels; // ArrayOfParcel
  public $cod; // COD
  public $ROD; // boolean
  public $InsuranceAmount; // decimal
  public $rebateCoupon; // string
}

class Location {
  public $ClientNo; // string
  public $ClientTaxId; // string
  public $Name; // string
  public $Address; // string
  public $City; // string
  public $PostCode; // string
  public $CountryCode; // string
  public $Person; // string
  public $Contact; // string
  public $Email; // string
}

class Parcel {
  public $Type; // ParcelType
  public $Weight; // decimal
  public $D; // decimal
  public $W; // decimal
  public $S; // decimal
}

class ParcelType {
  const Envelope = 'Envelope';
  const Package = 'Package';
  const Palette = 'Palette';
}

class COD {
  public $Amount; // decimal
  public $RetAccountNo; // string
}

class GetAvailableServicesResponse {
  public $GetAvailableServicesResult; // ArrayOfService
}

class Service {
  public $ID; // int
  public $Name; // string
  public $MaxDeliveryTime; // dateTime
  public $PriceWithoutTax; // decimal
  public $PriceWithTax; // decimal
  public $Tax; // decimal
  public $PriceElements; // ArrayOfPrice
}

class Price {
  public $PriceItem; // string
  public $ItemPrice; // decimal
}

class GetManifestPDF {
  public $login; // string
  public $password; // string
  public $LetterNumbers; // ArrayOfString
}

class GetManifestPDFResponse {
  public $GetManifestPDFResult; // base64Binary
}

class CreateOrder {
  public $login; // string
  public $password; // string
  public $Payer; // Location
  public $ServiceType; // int
  public $ServiceID; // int
  public $pickup; // Location
  public $delivery; // Location
  public $ReadyDate; // dateTime
  public $Parcels; // ArrayOfParcel
  public $cod; // COD
  public $ROD; // boolean
  public $InsuranceAmount; // decimal
  public $MPK; // string
  public $addidtionalInfo; // string
  public $rebateCoupon; // string
}

class CreateOrderResponse {
  public $CreateOrderResult; // Order
}

class Order {
  public $No; // string
  public $LetterNo; // string
  public $MimeData; // base64Binary
  public $PriceElements; // ArrayOfPrice
  public $OrderPrice; // decimal
}

class CreateInvoiceForOrder {
  public $login; // string
  public $password; // string
  public $LetterNo; // string
  public $Payer; // Location
}

class CreateInvoiceForOrderResponse {
  public $CreateInvoiceForOrderResult; // Invoice
}

class Invoice {
  public $InvoiceNo; // string
  public $SalesTime; // dateTime
  public $CreateTime; // dateTime
  public $PaymentDeadline; // dateTime
  public $PaymentAccountNo; // string
  public $PaymentFormDescription; // string
  public $SumWithouTax; // decimal
  public $SumWithTax; // decimal
  public $Tax; // decimal
  public $MimeData; // base64Binary
}


/**
 * api class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class api extends \SoapClient {

  private static $classmap = array(
                                    'GetOrderStatus' => 'GetOrderStatus',
                                    'GetOrderStatusResponse' => 'GetOrderStatusResponse',
                                    'OrderStatus' => 'OrderStatus',
                                    'GetAvailableServices' => 'GetAvailableServices',
                                    'Location' => 'Location',
                                    'Parcel' => 'Parcel',
                                    'ParcelType' => 'ParcelType',
                                    'COD' => 'COD',
                                    'GetAvailableServicesResponse' => 'GetAvailableServicesResponse',
                                    'Service' => 'Service',
                                    'Price' => 'Price',
                                    'GetManifestPDF' => 'GetManifestPDF',
                                    'GetManifestPDFResponse' => 'GetManifestPDFResponse',
                                    'CreateOrder' => 'CreateOrder',
                                    'CreateOrderResponse' => 'CreateOrderResponse',
                                    'Order' => 'Order',
                                    'CreateInvoiceForOrder' => 'CreateInvoiceForOrder',
                                    'CreateInvoiceForOrderResponse' => 'CreateInvoiceForOrderResponse',
                                    'Invoice' => 'Invoice',
                                   );

  public function api($wsdl = "http://system.tba.pl/api.asmx?WSDL", $options = array()) {
    foreach(self::$classmap as $key => $value) {
      if(!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    parent::__construct($wsdl, $options);
  }

  /**
   *  
   *
   * @param GetOrderStatus $parameters
   * @return GetOrderStatusResponse
   */
  public function GetOrderStatus(GetOrderStatus $parameters) {
    return $this->__soapCall('GetOrderStatus', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetAvailableServices $parameters
   * @return GetAvailableServicesResponse
   */
  public function GetAvailableServices(GetAvailableServices $parameters) {
    return $this->__soapCall('GetAvailableServices', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param GetManifestPDF $parameters
   * @return GetManifestPDFResponse
   */
  public function GetManifestPDF(GetManifestPDF $parameters) {
    return $this->__soapCall('GetManifestPDF', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CreateOrder $parameters
   * @return CreateOrderResponse
   */
  public function CreateOrder(CreateOrder $parameters) {
    return $this->__soapCall('CreateOrder', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

  /**
   *  
   *
   * @param CreateInvoiceForOrder $parameters
   * @return CreateInvoiceForOrderResponse
   */
  public function CreateInvoiceForOrder(CreateInvoiceForOrder $parameters) {
    return $this->__soapCall('CreateInvoiceForOrder', array($parameters),       array(
            'uri' => 'http://tempuri.org/',
            'soapaction' => ''
           )
      );
  }

}
