<?php

namespace communicator\sources\TBA;

class TBAPointsOfReceipt
{

  private $bTest;
  
  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var WebServiceTBATest
   */
  public $TBA;
  

	public function __construct($pDbMgr, $bTest) {
    global $pDbMgr;
    
    $this->pDbMgr = $pDbMgr;
    $this->bTest = $bTest;
    if (is_null($this->pDbMgr)) {
      $this->pDbMgr = $pDbMgr;
    }
  }


  /**
   * Metoda sprawdza, czy przekazany kod pocztowy znajduje się w obszarze doręczeń
   * 
   * param string $sPostcode
   * @return bool
   */
  public function validatePostcode($sPostcode) {
    
    $sSql = "SELECT `desc`
             FROM shipping_points_tba 
             WHERE `desc` = '".$sPostcode."'";

    return ($this->pDbMgr->GetOne('profit24', $sSql) == true ? true : false); 
  }  

}
