<?php

/**
 * Interfejs Automatyczne zamówienia
 * 
 * @author Marcin Janowski
 * @created 2015-05-20
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources;

interface AutoOrders {
  
  /**
   * Metoda wysyłania zamówienia do dostawcy
   * 
   * @global array $aConfig
   * @param array $aParams
   * @return return FALSE
   */
  function __putOrder($aBooksList = NULL);
  
  /**
   * Metoda loguje się u dostawcy
   * @param object $oCURL
   * @param string $sLogin
   * @param string $sPassword
   * @return string
   */
  function _doLogin($sLogin, $sPassword, $bLogout);
  
} 