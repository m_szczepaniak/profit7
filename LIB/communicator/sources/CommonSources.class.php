<?php
/**
 * Klasa wspólna pomagająca do obsługi komunikacji z serwisami zewnętrznymi
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */

use LIB\Helpers\ArrayHelper;

class CommonSources
{

    function __construct()
    {

    }

    /**
     * Metoda wysyła dane za pomocą POST lub GET
     * @author Arkadiusz Golba <arekgl0@op.pl>
     *
     * @param $sGETURL - adres który ma być pobrany, moze zawierać parametry GET
     * @param $bPost - czy wysyłamy POST'em
     * @param $aPostData - dane do POST'a
     * @param $iTimeOutInSecounds - czas po którym połączenie jest porzucane
     * @return boolean|response
     */
    public function CURL_file_get_contents($sGETURL, $bPost = TRUE, $aPostData = array(), $iTimeOutInSecounds = 10)
    {

        $oCURL = curl_init();
        curl_setopt($oCURL, CURLOPT_URL, $sGETURL);
        curl_setopt($oCURL, CURLOPT_HEADER, FALSE);
        curl_setopt($oCURL, CURLOPT_SSL_VERIFYPEER, FALSE);

        if ($bPost === TRUE) {
            curl_setopt($oCURL, CURLOPT_POST, TRUE);
            curl_setopt($oCURL, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
            curl_setopt($oCURL, CURLOPT_POSTFIELDS, $aPostData);
        }

        curl_setopt($oCURL, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($oCURL, CURLOPT_CONNECTTIMEOUT, $iTimeOutInSecounds);
        curl_setopt($oCURL, CURLOPT_TIMEOUT, $iTimeOutInSecounds);

        $sReturnStr = curl_exec($oCURL);
        $info = curl_getinfo($oCURL);
        if ($sReturnStr === false) {
            curl_close($oCURL);
            return false;
        }
        curl_close($oCURL);
        return $sReturnStr;
    }// end of CURL_file_get_contents() function


    /**
     * Wyłuskuje błędy walidacji pliku wygenerowanego do eksportu
     *
     * @return string
     */
    public function libxml_display_error($error)
    {
        $return = "<br/>\n";
        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "<b>Warning $error->code</b>: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "<b>Error $error->code</b>: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "<b>Fatal Error $error->code</b>: ";
                break;
        }
        $return .= trim($error->message);
        if ($error->file) {
            $return .= " in <b>$error->file</b>";
        }
        $return .= " on line <b>$error->line</b>\n";

        return $return;
    }


    /**
     * Wyłuskuje błędy walidacji pliku wygenerowanego do eksportu
     *
     * @return string
     */
    public function libxml_display_errors()
    {
        $sErr = '';
        $errors = libxml_get_errors();
        foreach ($errors as $error) {
            $sErr .= $this->libxml_display_error($error);
        }
        libxml_clear_errors();
        return $sErr;
    }// end of libxml_display_errors() function


    /**
     * Wysyła maila z logiem importu
     * @param $sTopic - temat maila
     * @param $sContent - treść maila
     * @return void
     */
    public function sendInfoMail($sTopic, $sContent)
    {
        global $aConfig;

        if (!Common::sendMail('', $aConfig['common']['import_sender_email'], $aConfig['common']['import_send_to'], $sTopic, $sContent))
            dump('error sending mail');
    } // end of sendInfoMail() function


    /**
     * Metoda zapisuje do bazy danych informacje na temat zmienionego statusu zamówień
     *
     * @param int $iOldStatus
     * @param int $iNewStatus
     * @param array $aInfoData
     * @return bool
     */
    public function informAboutStatusChanged($aInfoData)
    {
        global $aConfig, $pDbMgr;

        include_once($_SERVER['DOCUMENT_ROOT'] . 'omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
        if (isset($aInfoData) && !empty($aInfoData) && is_array($aInfoData)) {
            foreach ($aInfoData as $aOrderItem) {

                $aValues = array(
                    'created' => 'NOW()',
                    'created_by' => 'job_auto_status',
                    'content' => serialize($aOrderItem)
                );
                if ($pDbMgr->Insert('profit24', 'events_logs_orders', $aValues) === false) {
                    return false;
                }
            }
        }
        return true;
    }// end of informAboutStatusChanged() method


    /**
     *
     * @param type $iSendHistoryId
     * @param type $sSourceIndex
     * @param type $iCurrentQuantity
     * @param type $iStatus
     * @param type $fPriceNetto
     * @param type $fVat
     * @param type $fDiscount
     * @return type
     */
    public function proceedOrderedItemsResponse($iSendHistoryId, $sSourceIndex, $iCurrentQuantity, $iStatus, $iSourceId, $iNewInternalStatus)
    {
        $bIsErr = FALSE;
        $iSumOrderedQuantity = 0;
        $aInformData = array();

        // odnalezienie pozycji z listy
        $aOItems = $this->_getOrderItemBy($iSendHistoryId, $sSourceIndex, $iSourceId);
        foreach ($aOItems as $aOItem) {
//      $iOrderedQuantity = $aOItem['quantity'];
            // sumujemy każdą ze składowych
            // aby sprawdzić czy wystarczająco zostało zatwierdzonych
            $iSumOrderedQuantity += $aOItem['quantity'];
            if (!empty($aOItem) && is_array($aOItem)) {
                if ($this->_updateCurrentQuantity($iSendHistoryId, $aOItem['id'], intval($iCurrentQuantity), $iStatus) !== FALSE) {
                    // sprawdzenie, czy zarezerwowano, taką ilość, jaką zamówiono

                    //  jeśli Azymut FV potwierdza mniejszą ilość niż zamówiono, wtedy należy,
                    //   o ile jest to ilość > 0 to należy dodać nową pozycję do zamówienia
                    if (intval($iCurrentQuantity) < intval($iSumOrderedQuantity)) {
                        //mark as unavailable on all services
                        $this->markAsUnavailableEverywhere(false, $iSourceId,$aOItem['product_id']);

                        $iToConfirmInItem = $iCurrentQuantity - (intval($iSumOrderedQuantity) - $aOItem['quantity']);
                        $iDefQuantity = $aOItem['quantity'] - $iToConfirmInItem;
                        if ($aOItem['quantity'] > 1 && $iToConfirmInItem > 0 && $iDefQuantity > 0) {

                            //@@ dodajemy duplikat tego produktu, na ilość jaka brakuje
                            if ($this->addNewDuplicateItem($aOItem['id'], $iDefQuantity) === FALSE) {
                                $bIsErr = TRUE;
                            }

                            // zamówiono więcej niż 1, oraz jest cokolwiek do potwierdzenia
                            //@@ zmianiamy ilośc produktu aktualnego i potwierdzamy ilosć tylko taką jaka przyszła
                            if ($this->changeOrderQuantity($aOItem['id'], $iToConfirmInItem) === FALSE) {
                                $bIsErr = TRUE;
                            }
                        } else {
                            // jeśli zrealizowano mniej, niż zamówiono, to zmiana statusu: '---'
                            if ($this->_setOrderItemStatusNew($aOItem['id'], $iSourceId) === FALSE) {
                                $bIsErr = TRUE;
                            }
                        }

                        // przeliczenie zamówienia
                        if ($this->oModuleZamowienia->recountOrder($aOItem['order_id'], $aOItem['payment_id']) === FALSE) {
                            $bIsErr = TRUE;
                        }
                        $aOrderTMP = $aOItem;
                        $aOrderTMP['id'] = $aOrderTMP['order_id'];
                        // zmiana statusu
                        $aInformData[] = array(
                            'new_status' => '0',
                            'old_status' => $iNewInternalStatus,
                            'order' => $aOrderTMP
                        );
                    }
                } else {
                    $bIsErr = TRUE;
                }
            } else {
                $bIsErr = TRUE;
            }
        }

        if ($bIsErr != TRUE && !empty($aInformData)) {
            $this->informAboutStatusChanged($aInformData);
        }
        return $bIsErr === TRUE ? FALSE : TRUE;
    }// end of proceedOrderedItemsResponse() method


    /**
     * Metoda zmienia aktualną ilość zamówionych pozycji
     *
     * @global object $pDbMgr
     * @param int $iSendHistoryId
     * @param int $iOrderItemId
     * @param int $iCurrentQuantity
     * @param int $iStatus 0 - nowe wysłanem, 3 - aktualizacja, raport realizacji, 6 - zatwierdznie z fv
     * @return bool
     */
    public function _updateCurrentQuantity($iSendHistoryId, $iOrderItemId, $iCurrentQuantity, $iStatus)
    {
        global $pDbMgr;

        $aValues = array(
            'current_quantity' => $iCurrentQuantity,
            'status' => $iStatus,
        );
        return $pDbMgr->Update('profit24', 'orders_send_history_items', $aValues, ' send_history_id = ' . $iSendHistoryId . ' AND item_id = ' . $iOrderItemId);
    }// end of _updateCurrentQuantity() method


    private function _getOrderItemBy($iSendHistoryId, $sSourceBookindeks, $iSourceId)
    {
        global $pDbMgr;
        $sSql = "SELECT OI.id, OI.order_id, O.payment_id, OI.quantity, O.order_number, OI.name, OI.isbn AS isbn_plain , OI.product_id
             FROM orders_items AS OI
             JOIN orders AS O
              ON O.id = OI.order_id
             JOIN orders_send_history_items AS OSHI
              ON OI.id = OSHI.item_id
             WHERE 
              OI.source = '".$iSourceId."' AND
              OI.status <> '0' AND 
              OI.status <> '-1' AND
              OSHI.send_history_id = ".$iSendHistoryId." AND 
              OSHI.item_source_id = '".$sSourceBookindeks."'";
    return $pDbMgr->GetAll('profit24', $sSql);
  }// end of getOrderItemBy() method


    /**
     * Metoda pobiera składową zamówienia
     *
     * @global object $pDbMgr
     * @param int $iOIId
     * @return array
     */
    private function getOrderItemAllData($iOIId)
    {
        global $pDbMgr;

        $sSql = 'SELECT * FROM orders_items WHERE id = ' . $iOIId;
        return $pDbMgr->GetRow('profit24', $sSql);
    }// end of getOrderItemAllData() method


    /**
     * Metoda nową składową zamówienia, która jest duplikatem
     *
     * @todo bundle getOrderItemAttachments
     * @param int $iOIId
     * @param int $iDefQuantity - ilość jaka nam brakuje
     * @return bool
     */
    public function addNewDuplicateItem($iOIId, $iDefQuantity)
    {
        global $pDbMgr;

        $aOrderItem = $this->getOrderItemAllData($iOIId);
        foreach ($aOrderItem as $sCol => $mVal) {
            if ($mVal === NULL) {
                $aOrderItem[$sCol] = 'NULL';
            }
        }
        unset($aOrderItem['id']);

        $aOrderItem['sent_hasnc'] = '0';
        $aOrderItem['status'] = '0';
        $aOrderItem['quantity'] = $iDefQuantity;

        $aOrderItem = $this->changeOrderValueByNewQuantity($aOrderItem, $iDefQuantity, $aOrderItem);
        // @todo bundle getOrderItemAttachments
        return $pDbMgr->Insert('profit24', 'orders_items', $aOrderItem);
    }// end of addNewDuplicateItem() method


    /**
     * Metoda zmiania zamówioną przez klienta ilość, aktualnej pozycji
     *
     * @param int $iOIId
     * @param int $iToConfirmInItem
     * @param null $status
     * @return mixed
     */
    public function changeOrderQuantity($iOIId, $iToConfirmInItem, $status = null)
    {
        global $pDbMgr;

        $aOrderItem = $this->getOrderItemAllData($iOIId);
        $aValues = $this->changeOrderValueByNewQuantity($aOrderItem, $iToConfirmInItem);
        $aValues['quantity'] = $iToConfirmInItem;

        if (null != $status) {
            $aValues['status'] = $status;
        }

        return $pDbMgr->Update('profit24', 'orders_items', $aValues, ' id = ' . $iOIId);
    }// end of changeOrderQuantity() method

    public function changeOrderItemStatus($iOIId, $status)
    {
        global $pDbMgr;

        return $pDbMgr->Update('profit24', 'orders_items', ['status' => $status], ' id = ' . $iOIId);
    }

    /**
     *
     * @param array $aOrderItem
     * @param int $iNewQuantity
     * @return array
     */
    private function changeOrderValueByNewQuantity($aOrderItem, $iNewQuantity, $aToUpdateItem = array())
    {

        $aToUpdateItem['total_discount_currency'] = Common::formatPrice2($aOrderItem['discount_currency'] * $iNewQuantity);
        $aToUpdateItem['value_netto'] = $iNewQuantity * $aOrderItem['promo_price_netto'];
        $aToUpdateItem['value_brutto'] = $iNewQuantity * $aOrderItem['promo_price_brutto'];
        $aToUpdateItem['total_vat_currency'] = Common::FormatPrice2($aToUpdateItem['value_brutto'] - $aToUpdateItem['value_netto']);
        return $aToUpdateItem;
    }// end of changeOrderValueByNewQuantity() method


    /**
     * Metoda ustawia zamówienie jako nowe
     *
     * @global object $pDbMgr
     * @param int $iOItem
     * @return boolean
     */
    public function _setOrderItemStatusNew($iOItem, $iSourceId)
    {
        global $pDbMgr;


        if ($iOItem > 0) {
            $aValues = array(
                'sent_hasnc' => '0',
                'status' => '0'
            );
            return $pDbMgr->Update('profit24', 'orders_items', $aValues, ' id=' . $iOItem . " AND source = '" . $iSourceId . "' AND status <> '4' ");
        } else {
            return FALSE;
        }
    }


    /**
     * Metoda pobiera id wysłanej listy do zamówienia na podstawie jej numeru
     *
     * @global object $pDbMgr
     * @param string $sAzymutTransaction Numer transakcji azymut
     * @return int
     */
    public function getSourceSendHistoryId($sSourceIdent)
    {
        global $pDbMgr;

        $sSql = "SELECT orders_send_history_id 
             FROM orders_send_history_attributes 
             WHERE ident_nr = '" . $sSourceIdent . "' LIMIT 1";
        return $pDbMgr->GetOne('profit24', $sSql);
    }// end of _getSendHistoryId method

    /**
     * Metoda oznacza dany produkt jako niedostępny we wszystkich serwisach dla danego źródła
     * @param $product_ean
     * @param $source_id
     * @throws Exception
     */
    public function markAsUnavailableEverywhere($product_ean, $source_id,$product_id = false)
    {
        global $pDbMgr;

        $websites = $this->getWebsites();

        $column_to_change = $pDbMgr->GetOne('profit24', 'SELECT stock_col FROM external_providers WHERE id="' . (int)$source_id . '"');

        if ($product_id === FALSE)
        {
            $product_id = $pDbMgr->GetOne('profit24', "SELECT id FROM products WHERE isbn_plain = '" . $product_ean . "' OR isbn_10 = '" . $product_ean . "' OR isbn_13 = '" . $product_ean . "' OR ean_13 = '" . $product_ean . "'");
        }

        // zmieniamy prod_status na 0 we wszystkich serwisach dla danego EAN'u
        $aValues[$column_to_change] = 0;

        foreach ($websites as $website) {
            $dbCode = $website['code'];

            $column = 'profit24_id';

            if ($dbCode == 'profit24') {
                $column = 'id';
            }

            $pDbMgr->Update($dbCode, 'products_stock', $aValues, ' ' . $column . ' = ' . (int)$product_id.' LIMIT 1');
        }
    }

    public function getWebsites()
    {
        global $pDbMgr;

        $result = $pDbMgr->GetAll('profit24', "SELECT * FROM websites WHERE bookstore = 1");

        if (false === $result) {

            throw new \Exception('Błąd w pobieraniu listy stron');
        }

        if (!empty($result)) {
            $result = ArrayHelper::toKeyValues('id', $result);
        }

        return $result;
    }

}



?>