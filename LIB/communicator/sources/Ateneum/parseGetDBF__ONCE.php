<?php
/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-09-20 
 * @copyrights Marcin Chudy - Profit24.pl
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Ateneum';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/communicator/sources/CommonSources_New.class.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/communicator/sources/Ateneum/ProceedFV.class.php');



class AteneumMailbox_Test {
  function __construct() {}
  
  public function test_read() {
    global $pDbMgr;
    $bTestMode = TRUE;
    
    $oAutoProviderToOrders = new \communicator\sources\Ateneum\ProceedFV($pDbMgr, $bTestMode);
    
  }
}
global $pDbMgr;
$oAteneum_Test = new AteneumMailbox_Test();
$oAteneum_Test->test_read();