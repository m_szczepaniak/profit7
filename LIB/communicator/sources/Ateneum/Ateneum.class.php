<?php
/**
 * Klasa obsługi komunikacji ze źródłem Ateneum
 * 
 * @version 0.01
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-02-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace communicator\sources\Ateneum;

require_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/communicator/sources/CommonSources.class.php');

use communicator\sources\AutoOrders;
use communicator\sources\productToDBF;
use Curl_HTTP_Client;
use DatabaseManager;
use Exception;
use orders\magazine\providers\ordersToProviders;
use table\keyValue;

class Ateneum extends \CommonSources implements AutoOrders {

  CONST SOURCE_ID = 31;
  CONST SOURCE_SYMBOL = 'ateneum';
  CONST NEW_INTERNAL_STATUS = 1;

  private $sCurrPath;
  private $iCardId;

  /**
   * @var Curl_HTTP_Client $oCurl
   */
  private $oCURL;
  private $sNow;
  public $aErr = array();
  public $aErrMsg;
  public $oModuleZamowienia;

  /**
   *
   * @var ordersToProviders $oOrdersToProviders
   */
  public $oOrdersToProviders;

  /**
   * @var DatabaseManager $pDbMgr
   */
  public $pDbMgr;
  public $iTestMode;

  function __construct($pDbMgr, $iTestMode = 1) {
    $this->pDbMgr = $pDbMgr;
    $this->oKeyValue = new keyValue('external_providers');


    $this->iTestMode = $iTestMode;

    $this->sCurrPath = preg_replace('/(.*)\\' . DIRECTORY_SEPARATOR . '.*/', '$1', __FILE__);


//    include_once($_SERVER['DOCUMENT_ROOT'].'/LIB/orders/magazine/providers/ordersToProviders.class.php');
//    $this->oOrdersToProviders = new \orders\magazine\providers\ordersToProviders($pDbMgr);
    $this->initCURL();
    $this->sNow = date('Ymd_His');
  }

  /**
   * Metoda pobiera id zamówienia u dostawcy na podstawie numeru faktury
   * 
   * @param string $sFvNumber
   * @return string
   */
  public function __getSourceOrderIdByFvNumber($sFvNumber) {
    $aMatches = array();

    $sLogin = 'profit';
    $sPassword = 'g8gMKtgp';
    $sURL = 'http://' . $sLogin . ':' . $sPassword . '@ateneum.net.pl/profit/invtoorder.php?fanumer=FS+' . urldecode($sFvNumber);
    $sHTMLFvNumber = $this->oCURL->fetch_url($sURL, null, 60);
    preg_match('/zamid=(\d+)/', $sHTMLFvNumber, $aMatches);
    if (intval($aMatches[1]) > 0) {
      return intval($aMatches[1]);
    }
    throw new Exception(_('#2 Wystąpił błąd podczas pobierania id zamówienia w Ateneum dla FV nr:"' . $sFvNumber . '" - ' . "\n" . $sURL . ' - ' . "\n odpowiedź:'" . $sHTMLFvNumber . '"'));
  }
// end of __getOrderIdByFvNumber() method

  /**
   * Metoda obsługuje XML'a z fakturą z Azymutu
   * 
   * @param string $sResponseFilePath
   * @param int $sAzymutTransaction id traksakcji azymut
   * @throws Exception
   */
  public function __parseXMLgetInv($aFvData) {

    $iCount = 0;
    $sNrInvoice = $aFvData['add_number'];
    $sExIdentNumber = $aFvData['ex_ident_number'];

    if ($sNrInvoice != '' && $sExIdentNumber != '') {
      $iOrderProviderId = $this->oOrdersToProviders->getOrderProviderId($sExIdentNumber);
      // TO JUŻ NIE POWINNO BYĆ POTRZEBNE
      //$iSendHistoryId = $this->_getSendHistoryId($sAzymutTransaction);
      //$oOrderMagazine->addSetHistorySendAttr($iSendHistoryId, 'fv_nr', $sNrInvoice);
      if ($iOrderProviderId > 0) {
        $aInvoiceArray = $this->_getInvoiceGroupByArray($aFvData['items']);
        if (!empty($aInvoiceArray)) {
          foreach ($aInvoiceArray as $sSourceBookindeks => $iCurrentQuantity) {

            if ($this->proceedOrderedItemsResponse($iOrderProviderId, $sSourceBookindeks, $iCurrentQuantity, 6, 'auto-ateneum') === FALSE) {
              $sErr = 'Wystąpił błąd podczas zmiany statusu zamówienia'
                      . ' send_history_id: ' . $iOrderProviderId
                      . ' bookindeks: ' . $sSourceBookindeks;
              $this->aErr[] = $sErr;
              throw new Exception($sErr);
            } else {
              $iCount++;
            }
          }
        }

        if ($iCount > 0) {
          // jeśli jakiś element nie wystąpił na FV z tej listy, oznacza to że nie znajduje się na fakturze
          $aOrdersItemsList = $this->oOrdersToProviders->getItemsNotConfirmed($iOrderProviderId);
          if (!empty($aOrdersItemsList) && is_array($aOrdersItemsList)) {
            foreach ($aOrdersItemsList as $aOItem) {
              // jeśli zrealizowano mniej, niż zamówiono, to zmiana statusu: '---'
              if ($this->oOrdersToProviders->changeOrderToProvidersItemsStatus($aOItem['id'], 'B') !== FALSE) {
                // zmiana statusu: '---'
                if ($this->_setProductNewRecount($aOItem, $aOItem['order_id'], $aOItem['order_item_id'], $aOItem['payment_id']) === FALSE) {
                  return FALSE;
                }
              } else {
                return FALSE;
              }
            }
          }
        }
      }
    }
    return TRUE;
  }
// end of parseXMLgetInv() method

  /**
   * Metoda grupuje rekordy na fakturze po azymut bookindeksie i zwraca tablice
   * 
   * @param productToDBF $aProductsDBFsArray
   * @return array 
   */
  private function _getInvoiceGroupByArray($aProductsDBFsArray) {
    $aInvoiceArray = array();

    foreach ($aProductsDBFsArray as $aProductDBF) {
      $sSourceBookindeks = $aProductDBF->indexDostawcy;
      $iCurrentQuantity = intval($aProductDBF->ilosc);
      if (isset($aInvoiceArray[$sSourceBookindeks])) {
        $aInvoiceArray[$sSourceBookindeks] += $iCurrentQuantity;
      } else {
        $aInvoiceArray[$sSourceBookindeks] = $iCurrentQuantity;
      }
    }
    return $aInvoiceArray;
  }
// end of _getInvoiceGroupByArray() method

  /**
   * Metoda wysyłania zamówienia do dostawcy
   * 
   * @global array $aConfig
   * @param array $aParams
   * @return return FALSE
   */
  public function __putOrder($aBooksList = NULL) {

    include_once($_SERVER['DOCUMENT_ROOT'] . 'omniaCMS/modules/m_zamowienia/Module.class.php');
    $oNull = new \stdClass();
    $this->oModuleZamowienia = new \Module($oNull, $oNull, TRUE);
    $sFileName = 'order_' . $this->sNow . '.csv';
    $this->pDbMgr->BeginTransaction('profit24');

    try {
      // pobieramy listę
      if (empty($aBooksList) || isset($aBooksList['vat']) ||  isset($aBooksList['notVat'])) {
        // pobierzmy listę książek
        $aBooksList = $this->getAteneumOrdersToSend($aBooksList['vat'], $aBooksList['notVat']);
      }

      if (!empty($aBooksList)) {
         file_put_contents(__DIR__ . '/reports/CLEAR_BOOKSLIST.csv', print_r($aBooksList, true));
        $aBooksList = $this->_addAteneumSourceIndeks($aBooksList);
        file_put_contents(__DIR__ . '/reports/CLEAR_WITH_INDEX_BOOKSLIST.csv', print_r($aBooksList, true));
        $sFilePath = $this->_saveBooksToCSV($aBooksList, $sFileName);
        //$aFormatedParams = $this->formatAteneumData($aParams);
        $this->iCardId = $this->_doLoginGetCardId();
        if ($this->iCardId > 0) {
          // dodajemy nowe zamówienie do listy zamówień
          try {
            $this->SendOrderedBooks($sFilePath, $aBooksList);
          } catch (Exception $ex) {
            // wywalmy koszyk, wywalił błąd
            $this->_clearCard($this->iCardId);
            $this->pDbMgr->RollbackTransaction('profit24');
            throw $ex;
          }
        } else {
          $this->pDbMgr->RollbackTransaction('profit24');
          throw new Exception("Wystąpił błąd podczas próby dodawania/pobierania id koszyka");
        }
      } else {
        $this->pDbMgr->RollbackTransaction('profit24');
        return false;
      }
      $this->pDbMgr->CommitTransaction('profit24');
      return TRUE;
    } catch (Exception $e) {
      $this->pDbMgr->RollbackTransaction('profit24');
      $this->aErr[] = $e;
      $this->sendInfoMail('Ateneum - ' . $e->getMessage(), implode('<br /><br />', $this->aErr));
      echo 'Ateneum - ' . $e;
      throw new Exception($e);
    }
  }
// end of __putOrder() method

  /**
   * Metoda zbiera i wysyła zamówienie
   * @param string $sFilePath
   * @param array $aBooksList
   * @return boolean
   */
  private function SendOrderedBooks($sFilePath, &$aBooksList) {

    //Czyścimy koszyk dla pewności, że nic w nim nie ma 
    $this->_clearCard($this->iCardId);
    $this->iCardId = $this->_addNewCard();

    $aRaportArray = $this->ImportGetRaport($sFilePath, $aBooksList);
    if ($this->ChangeQuantities($aBooksList, $aRaportArray, $this->iCardId) === TRUE) {
//      Puszczamy całość jeszcze raz 
      //$aNextBooksList = $this->getAteneumOrdersToSend();
      //$aNextBooksList = $this->_addAteneumSourceIndeks($aNextBooksList);
      //$sFileName = 'order_' . $this->sNow . '.csv';
      //$sFilePath = $this->_saveBooksToCSV($aNextBooksList, $sFileName);
      //$aRaportArray = $this->ImportGetRaport($this->iCardId, $sFilePath, $aNextBooksList);
      return true;
    }

    // sprawdźmy minimum logistyczne..., może nie będziemy tego wogole wysyłać
    // jesli nie osiągnięte, wywalamy wszystko z koszyka i idziemy spać...
    if ($this->_checkMinimumLogistic($this->iCardId) === TRUE) {
      $oOrderMagazine = new \orders\OrderMagazineData($this->pDbMgr);
      try {
        $oOrderMagazine->setOrderedItemsByStatus(self::NEW_INTERNAL_STATUS, $this->_getOrdersItemsList($aBooksList));
      } catch (\Exception $ex) {
        $this->_clearCard($this->iCardId);
        throw $ex;
      }
      if ($this->iTestMode === 0) {
        $iSourceOrderId = $this->_doOrder($this->iCardId);
      } else {
        $iSourceOrderId = 'NULL';
      }
      $iSendHistoryId = $this->ChangeStatusesAddHistory($oOrderMagazine, $aBooksList, $sFilePath, $iSourceOrderId);
      $this->_proceedParseResponse($iSendHistoryId, $aRaportArray);
    } else {
      // wywalamy koszyk, brak minimum logistycznego
      $this->_clearCard($this->iCardId);
    }
  }

  /**
   * Metoda zwraca raport z dostępnymi w hurtowni pozycjami 
   * 
   * @param string $sFilePath
   * @param array $aBooksList
   * @return array
   */
  public function ImportGetRaport($sFilePath, &$aBooksList) {

    $aImportedProducts = $this->_importCart($this->iCardId, $sFilePath);
    file_put_contents(__DIR__ . '/reports/raport_START_'.time().'.csv', print_r($aImportedProducts, true));

    $iMaxAnalizeIteration = (count($aBooksList) * 4);
//        $aRaportProducts = $aImportedProducts;
    $aRaportProducts = $this->AnalyseUnavailable($aImportedProducts, $aBooksList, $iMaxAnalizeIteration);
    file_put_contents(__DIR__ . '/reports/raport_END_'.time().'.csv', print_r($aRaportProducts, true));
    return $aRaportProducts;
  }

  /**
   * Metoda zmienia ilości zamówionych pozycji w przypadku, kiedy są niedostępne
   * @param array $aBooksList
   * @param array $aRaportArray
   * @param int $iCardId
   * @return boolean
   */
  private function ChangeQuantities($aBooksList, $aRaportArray, $iCardId) {
    if ($this->proceedResponse($aBooksList, $aRaportArray) === TRUE) {
      print 'Zmieniono ilość pozycji';
      //$this->pDbMgr->CommitTransaction('profit24');
      $this->_clearCard($iCardId);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Metoda zmienia statusy zamówionych pozycji oraz dodaje historie wyslanego zamówienia
   * @param array $aBooksList
   * @param string $sFilePath
   * @param int $iSourceOrderId
   * @return int
   */
  private function ChangeStatusesAddHistory($oOrderMagazine, $aBooksList, $sFilePath, $iSourceOrderId) {
    $sOrderNumber = $oOrderMagazine->getNewListNumber(self::NEW_INTERNAL_STATUS);
    //$iOrderToProviderId = $this->oOrdersToProviders->InsertOrderToProvider($sListNumber, self::SOURCE_ID, FALSE, $aBooksList, 'auto-ateneum', $iSourceOrderId);
    $aOrderData = array(
        'items' => $aBooksList,
        'ateneum_order_id' => $sOrderNumber
    );
    $iSendHistoryId = $oOrderMagazine->addSendHistory($aOrderData, $sFilePath, self::NEW_INTERNAL_STATUS, self::SOURCE_ID, 'ateneum');
    $oOrderMagazine->addSetHistorySendAttr($iSendHistoryId, 'ident_nr', $iSourceOrderId);

    return $iSendHistoryId;
  }

  /**
   * 
   * @param type $iOrderId
   * @param type $iProductId
   * @return type
   */
  private function getOIArray($iOrderId, $iProductId) {
    $sSql = 'SELECT OI.id, OI.quantity, OI.order_id, OI.product_id, O.payment_id, OI.name 
         FROM orders_items AS OI
         JOIN orders AS O
         ON OI.order_id = O.id
         WHERE OI.order_id = ' . $iOrderId . '
         AND OI.product_id = ' . $iProductId . '
         AND O.order_status <> "5"
         AND OI.status = "1"
         AND OI.deleted = "0"
         AND OI.source = "' . self::SOURCE_ID . '"';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

  /**
   * 
   * @param type $aBooksList
   * @param type $aRaportProducts
   * @return boolean
   */
  private function proceedResponse($aBooksList, $aRaportProducts) {
    require_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/orders_alert/OrdersAlert.class.php');
    $oAlert = new \orders_alert\OrdersAlert();
    $bQuantitiesChanged = FALSE;
    foreach ($aBooksList as $iKey => $sValue) {
      if ($aRaportProducts[$iKey][1] < $aRaportProducts[$iKey][3]) {
        $iToConfirm = $aRaportProducts[$iKey][1];

        $aOI = $this->getOIArray($aBooksList[$iKey]['order_id'], $aBooksList[$iKey]['id']);
        $iSumQuantity = 0;
        foreach ($aOI as $iKeys => $sValues) {
          $iSumQuantity += $aOI[$iKeys]['quantity'];
          $iMissing = $iSumQuantity - $iToConfirm;
          $iNewQuantity = $aOI[$iKeys]['quantity'] - $iMissing;
          if ($iNewQuantity > 0 && $iMissing > 0) {
            $this->addNewDuplicateItem($aOI[$iKeys]['id'], $iMissing);
            $this->changeOrderQuantity($aOI[$iKeys]['id'], $iNewQuantity);
            $sMsg = _('Zmienono ilosc produktu: %s z %s na %s, dodano nowy rekord na ilosc %s');
            $oAlert->createAlert($aBooksList[$iKey]['order_id'], 'auto-ateneum', sprintf($sMsg, $aOI[$iKeys]['name'], $aOI[$iKeys]['quantity'], $iNewQuantity, $iMissing), date("Y/m/d H:i:s"), $aOI[$iKeys]['id']);
            $bQuantitiesChanged = TRUE;
          } elseif ($iNewQuantity <= 0) {
            $this->_setOrderItemStatusNew($aOI[$iKeys]['id'], self::SOURCE_ID);
            $sMsg = _('Zmieniono status produktu: %s na ---');
            $oAlert->createAlert($aBooksList[$iKey]['order_id'], 'auto-ateneum', sprintf($sMsg, $aOI[$iKeys]['name']), date("Y/m/d H:i:s"), $aOI[$iKeys]['id']);
            $bQuantitiesChanged = TRUE;
          }

          // przeliczenie zamówienia
          if ($this->oModuleZamowienia->recountOrder($aOI[$iKeys]['order_id'], $aOI[$iKeys]['payment_id']) === FALSE) {
            $bIsErr = TRUE;
          }
          $aOrderTMP = $aOI[$iKeys];
          $aOrderTMP['id'] = $aOI[$iKeys]['order_id'];
          // zmiana statusu
          $aInformData[] = array(
              'new_status' => '0',
              'old_status' => self::NEW_INTERNAL_STATUS,
              'order' => $aOrderTMP
          );

          if ($bIsErr != TRUE && !empty($aInformData)) {
            $this->informAboutStatusChanged($aInformData);
          }
        }
      }
    }
    $oAlert->addAlerts();
    return $bQuantitiesChanged;
  }

  /**
   * Metoda usuwa koszyk o zadanym id
   * 
   * @param int $iCardId
   * @return HTML
   */
  private function _clearCard($iCardId) {
    $sClearURL = 'http://www.ateneum.net.pl/koszyki.php?action=3&id=' . $iCardId;
    if ($this->iTestMode !== 2) {
      $this->oCURL->fetch_url($sClearURL);
      $this->oCURL->fetch_url($sClearURL);
    }
    return true;
  }
// end of _clearCard() method

  /**
   * Metoda pobiera listę z id orders_items na podstawie tablicy do eksportu
   * 
   * @param array $aParams
   * @return string
   */
  private function _getOrdersItemsList($aParams) {

    $sItems = '';
    if (!empty($aParams)) {
      foreach ($aParams as $aItem) {
        $sItems .= $aItem['items'] . ',';
      }
    }
    return substr($sItems, 0, -1);
  }
// end of _getOrdersItemsList() method

  /**
   * Metoda składa zamówienie u dostawcy
   * 
   * @param int $iCardId
   * @return boolean
   * @throws Exception
   */
  private function _doOrder($iCardId) {
    $aURLToSendOrder = array();
    $aOtParam = array();
    $aAwParam = array();

//   $aWysylkaIdParam = array();

    curl_setopt($this->oCURL->ch, CURLOPT_CONNECTTIMEOUT, 1800);
    curl_setopt($this->oCURL->ch, CURLOPT_TIMEOUT, 1800); //timeout in seconds

    $sURL = 'http://www.ateneum.net.pl/zamowienia.php?action=2&kid=' . $iCardId;
    $sOrderFormHTML = $this->oCURL->fetch_url($sURL, null, 1800);

    preg_match('/action="(zamowienia\.php\?action=4\&zamid=(\d+))"/', $sOrderFormHTML, $aURLToSendOrder);
    if (!isset($aURLToSendOrder[1]) || empty($aURLToSendOrder[1])) {
      throw new Exception(_('Nie odnalezionu w formularzu składania zamówienia id zamówienia 1 <br /> '."\n".$sURL."\n".$sOrderFormHTML));
    }
    if (isset($aURLToSendOrder[2]) && !empty($aURLToSendOrder[2])) {
      $iNewOrderId = intval($aURLToSendOrder[2]);
    } else {
      throw new Exception(_('Nie odnalezionu w formularzu składania zamówienia id zamówienia 2'));
    }


    preg_match('/value="(\d+)"\s?name="aw"/', $sOrderFormHTML, $aAwParam);
    if (!isset($aAwParam[1]) || empty($aAwParam[1])) {
      throw new Exception(_('Nie odnalezionu w formularzu składania zamówienia aw zamówienia'));
    }

    /*
     * XXX pole wywalone 02.03.2014 r.
      preg_match('/value="(\d+)"\s?name="wysylka_id"/', $sOrderFormHTML, $aWysylkaIdParam);
      if (!isset($aWysylkaIdParam[1]) || empty($aWysylkaIdParam[1])) {
      throw new \Exception(_('Nie odnalezionu w formularzu składania zamówienia wysylka id zamówienia'));
      }
     */


    $sUrlToSendOrder = 'http://www.ateneum.net.pl/zamowienia.php?action=4&zamid=' . $iNewOrderId;
    $aPostArray = array(
        'uwagi' => '',
        'otra' => 6,
        'ot' => 6,
        'aw' => $aAwParam[1],
        'wyslij' => 'Wyslij zamowienie'
    );
    $sAfterOrderInfoHTML = $this->oCURL->send_post_data($sUrlToSendOrder, $aPostArray);
    $sFilePath = __DIR__ . '/files/orders_csv/after_order_' . $this->sNow . '.csv';
    file_put_contents($sFilePath, $sAfterOrderInfoHTML);
    preg_match('/(Zam.wienie zosta.o z.o.one)/', $sAfterOrderInfoHTML, $aAfterOrderInfo);
    if ($aAfterOrderInfo[1]) {
      return $iNewOrderId;
    } else {
      return FALSE;
    }
  }
// end of _doOrder() method

  /**
   * Metoda sprawdza czy minumu logistyczne zostało przekroczone
   * 
   * @param int $iCardId
   * @throws Exception
   * @return boolean
   */
  private function _checkMinimumLogistic($iCardId) {
    $aMatches = array();

    $sURL = 'http://www.ateneum.net.pl/koszyki.php?action=2&id=' . $iCardId;
    $sCardHTML = $this->oCURL->fetch_url($sURL);
    preg_match('/(OP.ATA LOGISTYCZNA!)/', $sCardHTML, $aMatches);
    if (isset($aMatches[1]) && !empty($aMatches[1])) {
      $this->_clearCard($iCardId);
      throw new Exception(_('Brak minumum logistycznego w koszyku Ateneum !'));
    } else {
      return TRUE;
    }
  }
// end of _checkMinimumLogistic() method

  /**
   * Metoda przetwarza odpowiedź o realizacji zamówienia
   * 
   * @param int $iOrderProviderId
   * @param array $aRaportProducts
   * @throws Exception
   */
  private function _proceedParseResponse($iSendHistoryId, $aRaportProducts) {
    $oAlert = new \orders_alert\OrdersAlert();
    foreach ($aRaportProducts as $aProduct) {
      $iCurrentQuantity = intval($aProduct['1']);
      $sSourceIndex = trim($aProduct['2']);

      try {
        if ($this->proceedOrderedItemsResponse($iSendHistoryId, $sSourceIndex, $iCurrentQuantity, 3, self::SOURCE_ID, self::NEW_INTERNAL_STATUS) === FALSE) {
          throw new Exception('n');
        }
      } catch (Exception $ex) {
        $sErr = 'Wystąpił błąd podczas zmiany statusu zamówienia'
                . ' orders_send_history : ' . $iSendHistoryId
                . ' source_item_id: ' . $sSourceIndex;
        $oAlert->createAlert('NULL', 'auto-ateneum', $sErr);
      }
    }
    $oAlert->sendAlertsMail();
    $oAlert->removeDuplicates();
    $oAlert->addAlerts();
  }
// end of _proceedParseResponse() method

  /**
   * Metoda wykonuje pobranie zamówień do wysłania
   *
   * @param null $vat
   * @return array - lista produktów
   */
  public function getAteneumOrdersToSend($vat = null, $notVat = null) {

    $oOrderMagazine = new \orders\OrderMagazineData($this->pDbMgr);
    $aSelectCols = array('CONCAT(O.id, \'-\', OI.product_id)',
        'OI.product_id',
        'GROUP_CONCAT(OI.id SEPARATOR \',\') as items',
        'GROUP_CONCAT(OI.quantity SEPARATOR \',\') as items_quantity',
        'OI.order_id',
        'SUM(OI.quantity) as quantity',
        'OI.shipment_date',
        '(OI.quantity * PS.ateneum_wholesale_price) AS ateneum_wholesale_price'
    );
    $aBooksList = $oOrderMagazine->getOrdersItemsSendToSource(
        self::SOURCE_ID,
        self::NEW_INTERNAL_STATUS,
        self::SOURCE_SYMBOL,
        $aSelectCols,
        $this->_getSourceOrdersLimit(),
        0,
        null,
        $vat,
        $notVat
    );
    return $this->ChangeKeys($aBooksList);
  }
// end of getAteneumOrdersToSend() method

  /**
   * Metoda zmienia klucze tablicy
   * @param type $aBooksList
   * @return array
   */
  private function ChangeKeys($aBooksList) {
    
    if(empty($aBooksList)) {
      return $aBooksList;
    }
    $aNewKeysBooksList = array();
    $iNewKey = 0;
    foreach($aBooksList as $iKey => $aValue) {
      $aNewKeysBooksList[$iNewKey] = $aValue;
      $iNewKey++;
    }
    return $aNewKeysBooksList;
  }
  
  /**
   * Metoda pobiera limit zamówień dla Ateneumu
   * 
   * @global object $pDbMgr
   * @return string
   */
  private function _getSourceOrdersLimit() {

    $sSql = 'SELECT ateneum_limit_price
             FROM orders_magazine_settings
             WHERE id = 1';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
// end of getAteneumOrdersLimit() method

  /**
   * Metoda zapisuje książki do CSV
   * 
   * @param array $aBookList
   * @param array $sFileName
   * @return string
   */
  public function _saveBooksToCSV($aBooksList, $sFileName) {

    $sFilePath = __DIR__ . '/files/orders_csv/' . $sFileName;
    foreach ($aBooksList as $aBook) {
      $sLinia = $aBook['source_item_id'] . ';' . $aBook['quantity'] . ";" . preg_replace("/\n/", ' ', $aBook['name']) . "\n";
      file_put_contents($sFilePath, $sLinia, FILE_APPEND | LOCK_EX);
    }
    return $sFilePath;
  }
// end of _saveBooksToCSV() method

  /**
   * Metoda pobiera atrybuty produktu
   * 
   * @param int $iPId
   * @param array $aCols
   * @return array
   */
  private function _getProductAttr($iPId, $aCols) {

    $sSql = 'SELECT ' . implode(',', $aCols) . ' FROM products WHERE id = ' . $iPId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
// end of _getProductAttr() method

  /**
   * Metoda loguje się u dostawcy
   * @param object $oCURL
   * @param string $sLogin
   * @param string $sPassword
   * @return string
   */
  public function _doLogin($sLogin, $sPassword, $bLogout = false) {
    $aPostdata = array(
        'user' => $sLogin,
        'pass' => $sPassword
    );

    $this->oCURL->send_post_data('http://www.ateneum.net.pl/loguj.php', $aPostdata, NULL, 20);
    $sCardHTML = $this->oCURL->fetch_url("http://www.ateneum.net.pl/koszyki.php");
    if ($bLogout) {
      $this->oCURL->send_post_data('http://www.ateneum.net.pl/wyloguj.php', '', NULL, 20);
    }
    return $sCardHTML;
  }

  /**
   * Metoda loguje i pobiera id koszyka
   * 
   * @return int
   * @throws Exception
   */
  public function _doLoginGetCardId() {

    if ($this->iTestMode === 2) {
      return 12;
    }

    $sLogin = $this->oKeyValue->getByIdDestKey(self::SOURCE_ID, 'login');
    $sPassword = $this->oKeyValue->getByIdDestKey(self::SOURCE_ID, 'passw');
    
    $sCardHTML = $this->_doLogin($sLogin, $sPassword);
    if ($this->_checkIsLoggedIn($sCardHTML) === TRUE) {
      $this->clearAllCards($sCardHTML);
      return $this->_addNewCard();
    } else {
      throw new Exception("Wystąpił błąd podczas próby zalogowania na konto użytkownika");
    }
  }
// end of _doLoginGetCardId() method

  /**
   * Metoda sprawdza czy użytkownik został poprawnie zalogowany
   * 
   * @param string $sCardHTML
   * @return boolean
   */
  private function _checkIsLoggedIn($sCardHTML) {
    $aMatches = array();
    preg_match('/Zalogowany jako\: /', $sCardHTML, $aMatches);
    if (!empty($aMatches)) {
      return TRUE;
    }
    return FALSE;
  }
// end of _checkIsLoggedIn() method
  
  /**
   * 
   * @param string $sCardHTML
   */
  public function clearAllCards($sCardHTML) {
    
    $aMatches = $this->getAllCardsIds($sCardHTML);
    foreach ($aMatches[1] as $iCardId) {
      if ($iCardId > 0) {
        $this->_clearCard($iCardId);
      }
    }
    return true;
  }
  
  /**
   * 
   * @param string $sCardHTML
   * @return array
   */
  private function getAllCardsIds($sCardHTML) {
    
    preg_match_all("/koszyki\.php\?action=2\&id=(\d+)/", $sCardHTML, $aMatches);
    return $aMatches;
  }

  /**
   * Metoda pobiera id koszyka
   * 
   * @param string $sCardHTML
   * @throws Exception
   * @return int
   */
  private function _getCardId($sCardHTML) {

    if ($this->iTestMode == 2) {
      return 12;
    }
    $iCardId = $this->findCardId($sCardHTML);
    if ($iCardId > 0) {
      return $iCardId;
    } else {
      throw new Exception(_('Wystąpił błąd podczas tworzenia koszyka'));
    }
  }
// end of _getCardId() method

  /**
   * Metoda dodaje nowy koszyk
   * 
   * @return int
   */
  private function _addNewCard() {
    $sCardHTML = $this->oCURL->fetch_url("http://www.ateneum.net.pl/koszyki.php?action=5");
    return $this->_getCardId($sCardHTML);
  }
// end of _addNewCard() method
  
  /**
   * 
   * @param string $sCardHTML
   * @return int
   */
  private function findCardId($sCardHTML) {
    
   preg_match("/koszyki\.php\?action=2\&id=(\d+)/", $sCardHTML, $aMatches);
   return $aMatches[1]; 
  }

  /**
   * Metoda inicjuje klase CURL
   * 
   */
  private function initCURL() {
    include_once($_SERVER['DOCUMENT_ROOT'] . 'omniaCMS/lib/curl_http_client/curl_http_client.php');
    $this->oCURL = new Curl_HTTP_Client(false);
    $this->oCURL->set_user_agent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.117 Safari/537.36");
    $this->oCURL->store_cookies(__DIR__ . '/files/cookie.txt');
  }
// end of initCURL() method

  /**
   * Metoda importuje koszyk i zwraca raport realizacji zamówienia
   * 
   * @param int $iCardId
   * @param string $sFilename
   * @return array|bool
   */
  private function _importCart($iCardId, $sFilename, $iIteration = false) {

    if ($this->iTestMode === 2) {
      return $this->testModeResponseFile($iIteration);
    }

    $sURL = "http://www.ateneum.net.pl/import_koszyka.php?action=2";
    $aFilePost = array(
        'plik' => $sFilename
    );
    $aPostArray = array(
        'MAX_FILE_SIZE' => '100000',
        'koszid' => $iCardId,
    );
    $sImportHTML = $this->oCURL->send_multipart_post_data($sURL, $aPostArray, $aFilePost, NULL, 10);
    $sRaportCSV = $this->_getRaportFile($sImportHTML);
    if (!empty($sRaportCSV)) {
      $this->_saveRaportFile($sRaportCSV, $iIteration);
      // parsujemy wyniki raportu
      $aRaportArray = $this->_proceedImportRaport($sRaportCSV);

      return $aRaportArray;
    }
  }
// end of _importCart() method

  /**
   * 
   * @param int $iIteration
   * @return array
   */
  private function testModeResponseFile($iIteration) {

    $sPath = __DIR__ . '/files/orders_csv/test_raports';
    if ($iIteration === FALSE) {
      $aResponseArrray = file_get_contents($sPath . '/response_START.csv');
    } else {
      $aResponseArrray = file_get_contents($sPath . '/response_' . $iIteration . '.csv');
    }
    $aRows = explode("\n", $aResponseArrray);

    foreach ($aRows as $iKey => $aValue) {
      if ($aRows[$iKey] != '') {
        $aData = explode(';', $aRows[$iKey]);
        $aRaportArray[] = $aData;
      }
    }
    return $aRaportArray;
  }

  /**
   * Metoda analizuje niedostępne pozycje a następnie podmienia ISBN/EAN i wykonuje ponowny import
   * @param array $aRaportArray
   * @param string $sFilename
   * @param array &$aBooksLists
   * @param int $iCardId\
   * @param int $iMaxAnalizeIteration
   * @return array
   */
  private function AnalyseUnavailable($aRaportArray, &$aBooksLists, &$iMaxAnalizeIteration) {
    if ($iMaxAnalizeIteration == 0) {
      return FALSE;
    }
    $aUnavailable = $this->getUnavailableArray($aRaportArray);
    file_put_contents(__DIR__ . '/reports/books_lists' . $iMaxAnalizeIteration . '.csv', print_r($aBooksLists, true));
    file_put_contents(__DIR__ . '/reports/unavailable_array_iteration' . $iMaxAnalizeIteration . '.csv', print_r($aUnavailable, true));
    //Sprawdzenie czy w raporcie istnieją niedostępne pozycje
    if (!empty($aUnavailable)) {
      $aUnavailableBooksLists = $this->getUnavailableInBooksList($aBooksLists, $aUnavailable);
      $aUnavailableForSure = $this->getUnavailableForSure($aBooksLists, $aUnavailable);

      if (!empty($aUnavailableForSure))
      {
          foreach($aUnavailableForSure as $unv)
          {
              $this->markAsUnavailableEverywhere($unv['isbn'],self::SOURCE_ID);
          }
      }

      file_put_contents(__DIR__ . '/reports/unavailable_bookslist_iteration' . $iMaxAnalizeIteration . '.csv', print_r($aUnavailableBooksLists, true));
      if (!empty($aUnavailableBooksLists)) {
        $aUnavailableWithIndexesLists = $this->_addAteneumSourceIndeks($aUnavailableBooksLists);
        $aNewBooksLists = $this->MergeChangesWithBooksList($aUnavailableWithIndexesLists, $aBooksLists);
        $sNewFilename = 'order_ISBN_EAN_CHANGED' . time() . '_iteration_' . $iMaxAnalizeIteration . '.csv';
        $sNewFilePath = $this->_saveBooksToCSV($aNewBooksLists, $sNewFilename);

        //Czyścimy koszyk 
        $this->_clearCard($this->iCardId);
        $this->iCardId = $this->_addNewCard();

        //Importujemy listę ponownie
        $aNewRaportArray = $this->_importCart($this->iCardId, $sNewFilePath, $iMaxAnalizeIteration);
        $iMaxAnalizeIteration--;
        return $this->AnalyseUnavailable($aNewRaportArray, $aNewBooksLists, $iMaxAnalizeIteration);
      }
    }
    return $aRaportArray;
  }

  /**
   * Metoda zwraca tablicę niedostępnych pozycji z raportu
   * @param array $aRaportArray
   * @return array 
   */
  private function getUnavailableArray($aRaportArray) {

    $aUnavailable = array();
    if (!empty($aRaportArray)) {
      foreach ($aRaportArray as $iKey => $aOrderPosition) {
        if ($aOrderPosition[1] == 0 || $aOrderPosition[0] == 'ND') {
          $aUnavailable[$iKey] = $aOrderPosition;
        }
      }
    }
    return $aUnavailable;
  }

  /**
   * Metoda zwraca niedostępne pozycje z wyłączeniem pozycji oznaczonych jako niedostępne na 100%
   * @param array $aUnavailable
   * @param array $aBooksLists
   * @return array
   */
  private function getUnavailableInBooksList(&$aBooksLists, $aUnavailable) {
    $aUnavailableBooksLists = array();
    foreach ($aUnavailable as $iKey => $aOrderPosition) {
      if ($aBooksLists[$iKey]['try_again'] !== FALSE) {
//        array_push($aUnavailableBooksLists, $aBooksLists[$iKey]);
        $aBooksLists[$iKey]['try_again'] = TRUE;
        $aUnavailableBooksLists[$iKey] = $aBooksLists[$iKey];
      }
    }
    return $aUnavailableBooksLists;
  }

    /**
     * Metoda zwraca niedostępne na 100%
     * @param array $aUnavailable
     * @param array $aBooksLists
     * @return array
     */
    private function getUnavailableForSure($aBooksLists, $aUnavailable) {
        $aUnavailableForSure = array();
        foreach ($aUnavailable as $iKey => $aOrderPosition) {
            if ($aBooksLists[$iKey]['try_again'] !== TRUE) {
                $aUnavailableForSure[$iKey] = $aBooksLists[$iKey];
            }
        }
        return $aUnavailableForSure;
    }

  /**
   * Metoda dodaje indeks źródła Ateneum
   * 
   * @param array $aUnavailableBooksLists
   * @return array
   */
  public function _addAteneumSourceIndeks($aUnavailableBooksLists) {

    foreach ($aUnavailableBooksLists as $iKey => &$aBook) {
      $aProductAttr = $this->_getProductAttr($aBook['id'], array('ean_13', 'isbn_plain', 'isbn_13', 'isbn_10'));
      if (isset($aUnavailableBooksLists[$iKey]['try_again']) && $aUnavailableBooksLists[$iKey]['try_again'] === TRUE) {
        $aUniqueIdent = $this->getUniqueIdent($aProductAttr);
        $aBook['try_again'] = $this->checkLastElement($aUniqueIdent, $aUnavailableBooksLists[$iKey]['last_ident_col']);
        $mNextCheckIdent = $this->getNextIndex($aUniqueIdent, $aBook);
        if ($mNextCheckIdent !== FALSE) {
          $aUnavailableBooksLists[$iKey]['last_ident_col'] = $mNextCheckIdent['last_ident_col'];
          $aUnavailableBooksLists[$iKey]['source_item_id'] = $mNextCheckIdent['source_item_id'];
        } else {
          // @TODO do trzech kresek
          $aUnavailableBooksLists[$iKey]['try_again'] = FALSE;
        }
      } else if (!isset($aBook['try_again'])) {
        $aUnavailableBooksLists[$iKey]['source_item_id'] = ($aProductAttr['ean_13'] != '' ? $aProductAttr['ean_13'] : $aProductAttr['isbn_plain']);
        $aUnavailableBooksLists[$iKey]['last_ident_col'] = ($aProductAttr['ean_13'] != '' ? 'ean_13' : 'isbn_plain');
      } else {
        // @TODO nic nie robimy wycofujemy do ---
      }
    }
    return $aUnavailableBooksLists;
  }
// end of _addAteneumSourceIndeks() method

  /**
   * 
   * @param array $aUniqueIdent
   * @param array $aBook
   * @return boolean
   */
  private function getNextIndex($aUniqueIdent, &$aBook) {
    $bSetNext = false;

    foreach ($aUniqueIdent as $sIdent => $sValue) {
      if ($bSetNext) {
        $aBook['source_item_id'] = $sValue;
        $bSetNext = FALSE;
        $aCurrRow = array('last_ident_col' => $sIdent, 'source_item_id' => $sValue);
        return $aCurrRow;
      }

      if ($sIdent == $aBook['last_ident_col']) {
        $bSetNext = TRUE;
      }
    }
    return false;
  }

  /**
   * 
   * @param array $aUniqueIdent
   * @param array $sNowCol
   * @return boolean
   */
  private function checkLastElement($aUniqueIdent, $sNowCol) {
    $sLastIdent = array_pop(array_keys($aUniqueIdent));
    if ($sNowCol == $sLastIdent) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Metoda zwraca tablicę unikatowych indeksów produktu
   * @param array $aProductAttr
   */
  private function getUniqueIdent($aProductAttr) {

    foreach ($aProductAttr as $sKey => $sVal) {
      if ($sVal == '') {
        unset($aProductAttr[$sKey]);
      }
    }
    return array_unique($aProductAttr);
  }

  /**
   * Metoda zwraca tablicę książek z zamienionymi indeksami
   * @param array $aUnavailableWithIndexesLists
   * @param array $aBooksLists
   * @return array
   */
  private function MergeChangesWithBooksList($aUnavailableWithIndexesLists, $aBooksLists) {

    foreach ($aUnavailableWithIndexesLists as $iKey => $aOrderPosition) {
      $aBooksLists[$iKey] = $aUnavailableWithIndexesLists[$iKey];
    }
    return $aBooksLists;
  }

  /**
   * Metoda importuje raport w CSV i zwraca tablicę
   * 
   * @param string $sRaportCSV
   * @return array
   */
  private function _proceedImportRaport($sRaportCSV) {

    return $this->_getOrderItemsFromRaportCSV($sRaportCSV);
  }
// end of _proceedImportRaport() method

  /**
   * Metoda na podstawie pliku CSV zwraca listę produktów
   * 
   * @param string $sRaportCSV
   * @return array
   */
  private function _getOrderItemsFromRaportCSV($sRaportCSV) {

    $aRows = explode("\n", $sRaportCSV);
    foreach ($aRows as $iKey => $sRow) {
      $sRow = trim($sRow);
      if ($sRow != '') {
        $aResponse = explode(';', $sRow);
        if (isset($aResponse[1])) {
          $aRows[$iKey] = $aResponse;
        } else {
          unset($aRows[$iKey]);
        }
      } else {
        unset($aRows[$iKey]);
      }
    }
    return $aRows;
  }
// end of _getOrderItemsFromRaportCSV() method

  /**
   * Metoda pobiera plik raportu
   * 
   * @param string $sImportHTML
   * @return boolean
   */
  private function _getRaportFile($sImportHTML) {
    $sRaportURL = $this->_getRaportURL($sImportHTML);
    if (!empty($sRaportURL)) {
      return $this->oCURL->fetch_url($sRaportURL);
    } else {
      return FALSE;
    }
  }
// end of _getRaportFile() method

  /**
   * Metoda zapisuje plik raportu
   * 
   * @param string $sRaportCSV
   * @return boolean
   */
  private function _saveRaportFile($sRaportCSV, $iIteration) {
    $sFilePath = __DIR__ . '/files/orders_csv/response_' . $this->sNow . '_' . $iIteration . '.csv';
    return file_put_contents($sFilePath, $sRaportCSV);
  }
// end of _saveRaportFile() method

  /**
   * Metoda wyłuskuje URL raportu
   * 
   * @param type $sImportHTML
   * @return array
   */
  private function _getRaportURL($sImportHTML) {
    $aMatches = array();
    // ...<button onclick="location.href='raporty_importu.php?fid=7293'"...
    preg_match('/(raporty_importu\.php\?fid\=\d+)/', $sImportHTML, $aMatches);
    return "http://www.ateneum.net.pl/" . $aMatches[1];
  }
// end of _getRaportURL() method
}

// end of Class()

