<?php
/**
 * Klasa obsługuje fakture od Ateneum
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Ateneum;

use communicator\sources\productToDBF;
use Exception;
class FV_XML {

  /**
   * @var simplexml_load_file $oXML
   */
  private $oXML;
  
  /**
   * $sXMLFilePath
   * 
   * @param string $sXMLFilePath
   * @throws Exception
   */
  function __construct($sXMLFilePath) {
    if (file_exists($sXMLFilePath)) {
      $sXML = str_replace('&', '&amp;', file_get_contents($sXMLFilePath));
      $this->oXML = \simplexml_load_string($sXML, "SimpleXMLElement");
      
    } else {
      throw new Exception(_('Brak pliku XML do zaimportowania'));
    }
  }
  
  /**
   * Metoda wybiera numer faktury z dokumentu XML
   * 
   * @return string
   */
  public function getFVNumber() {
      $aAttribs = $this->oXML->dokument->attributes();
      preg_match('/(\d{4})/', $aAttribs->dataDok, $matches);
      return (string)$aAttribs->nrDok.'/'.$matches[1];
  }// end of getFVNumber() method
  
  
  /**
   * Metoda wybiera numer faktury z dokumentu XML
   * 
   * @return string
   */
  public function getDocType() {
    $aAttribs = $this->oXML->dokument->attributes();
    return (string)$aAttribs->typ;
  }// end of getFVNumber() method
  
  
  /**
   * Metoda pobiera produkty w formacie zgodnym z eksportem DBF
   * 
   * @return productToDBF[]
   */
  public function getProductsToDBF() {
    $aOProductsToDBF = array();
    //$oProductToDFB->cena_det = 
    foreach ($this->oXML->dokument->pozycja as $oPozycja) {
      $oProductToDFB = new productToDBF();
      $oProductToDFB->indexDostawcy = (string)$oPozycja->indeksDostawcy;
      $oProductToDFB->tytul = (string)$oPozycja->towar->nazwa;
      $oProductToDFB->isbn = (string)$oPozycja->towar->ean;
      $oProductToDFB->ean_13 = (string)$oPozycja->towar->ean;
      $oProductToDFB->pkwiu = (string)$oPozycja->towar->pkwiu;
      $oProductToDFB->cena_det = (string)$oPozycja->towar->cenaSpNetto;
      $oProductToDFB->cena_net = (string)$oPozycja->cenaNetto;
      $oProductToDFB->vat = (string)$oPozycja->vat;
      $oProductToDFB->ilosc = (string)$oPozycja->ilosc;
      $aOProductsToDBF[] = $oProductToDFB;
    }
    return $aOProductsToDBF;
  }// end of getProductsToDBF() method
  
  
  /**
   * Metoda pobiera produkty w formacie zgodnym z eksportem DBF
   * 
   * @return productToDBF[]
   */
  public function getProductsToStreamsoft() {
    //$oProductToDFB->cena_det = 
    foreach ($this->oXML->dokument->pozycja as $oPozycja) {
      $spNetto = (string)$oPozycja->cenaNetto;
      $aItem = array(
          'ean_13' => (string)$oPozycja->towar->ean,
          'price_netto' => $spNetto,
          'discount' => 0,
          'vat' => (string)$oPozycja->towar->vat,
          'quantity' => (string)$oPozycja->ilosc

      );
      $aItems[] = $aItem;
    }
    return $aItems;
  }// end of getProductsToDBF() method
  
  /**
   * 
   * @return array
   */
  public function getEANCol() {
    $aItem = array();
    foreach ($this->oXML->dokument->pozycja as $oPozycja) {
      $aItem[] = (string)$oPozycja->towar->ean;
    }
    return $aItem;
  }
}
