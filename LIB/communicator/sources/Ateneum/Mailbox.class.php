<?php
/**
 * Klasa odpowiedzialna, za czytanie maili ze skrzynki odbiorczej konta ateneum
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Ateneum;
class Mailbox {

  const SERVER = '{imap.profit24.pl:143/imap/novalidate-cert}INBOX';
  const LOGIN = 'ateneum@profit24.pl';
  const PASSWORD = 'i_1E0g5q';
  
  /**
   *
   * @var \ImapMailbox $this->oImapMailbox
   */
  private $oImapMailbox;
  
  private $aAttachments;
  
  function __construct() {
    
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/IMAP/ImapMailbox.class.php');
    $this->oImapMailbox = new \ImapMailbox(self::SERVER, self::LOGIN, self::PASSWORD, __DIR__.'/attachments/', 'utf-8');
    $this->oImapMailbox->checkMailbox();
    // pobieramy nieprzeczytane wiadomości
    $aUnseenMailsIds = $this->oImapMailbox->searchMailbox('UNSEEN');
    // sprawdzamy czy mają załącznik
    if (!empty($aUnseenMailsIds)) {
      $this->_proceedMails($aUnseenMailsIds);
    }
  }// end of __construct() method
  
  
  /**
   * Metoda zwraca nowo dodane załączniki
   * 
   * @return array
   */
  public function getNewAttachments() {
    return $this->aAttachments;
  }// end of getNewAttachments() method
  
  
  /**
   * Metoda prztważa nie przeczytane maile, najpierw dodaje załączniki, następnie oznacza jako przeczytany
   * 
   * @param array $aUnseenMailsIds
   */
  private function _proceedMails($aUnseenMailsIds) {
    foreach ($aUnseenMailsIds as $iMailId) {
      $this->_proceedMail($iMailId);
    }
  }// end of _proceedMails() method
  
  
  /**
   * Metoda przetważa pojedyńczego maila, dodaje do tablicy załączników maila
   * 
   * @param int $iMailId
   * @return void
   */
  private function _proceedMail($iMailId) {
    // pobieramy maila
    $oEmail = $this->oImapMailbox->getMail($iMailId);
    $aAttachments = $oEmail->getAttachments();
    foreach ($aAttachments as $iAtId => $oAttachment) {
      $this->aAttachments[$iMailId][$iAtId]['filePath'] = $oAttachment->filePath;
      $this->aAttachments[$iMailId][$iAtId]['name'] = $oAttachment->name;
    }
  }// end of _proceedMail() method
}