<?php
/**
 * Klasa przetwarza fakturę zakupu
 * 1) Pobiera XML z poczty ateneum@profit24.pl \Mailbox
 * 2) Wyciąga nr faktury z XML \FV_XML
 * 3) Pobiera za pomocą HTTP z serwera Ateneum  id zamówienia na podstawie przekazanego nr Faktury z kroku 2) \Ateneum
 * 4) Generuje DBF'a klasa \FV_XML przygotowuje dane dla \ParseDBF
 * 5) \ParseDBF generuje DBF'a
 * 6) \orders\magazine\sources\OrdersToProviders.class.php dodaje dokumenty do zamówienia do dostawcy
 * 
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-06 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Ateneum;
class ProceedFV {
  
  public $pDbMgr;
  public $bTestMode;

  function __construct($pDbMgr, $bTestMode) {
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    
    //zal[mail_id][zal_id][filePath]
    $aMailAttachments = $this->_getXMLFiles();
    //$aMailAttachments = $this->_proceedMailAttachments($aMailAttachments);
  }
  
  /**
   * Metoda przetważa załączniki maili
   * 
   * @param array $aMailAttachments
   * @return array
   */
  private function _proceedMailAttachments($aMailAttachments) {
    $oAteneum = $this->_getAteneumObj();
    
    if (!empty($aMailAttachments)) {
      foreach ($aMailAttachments as $iMailId => $aAttachments) {
        $aMailAttachments[$iMailId] = $this->_proceedAttachments($oAteneum, $aAttachments);
      }
      return $aMailAttachments;
    }
  }// end of _proceedMailAttachments() method
  
  
  /**
   * Metoda przygotowuje dane załączników, dodaje numer faktury i id zamówienia w źródle
   * 
   * @param Ateneum $oAteneum
   * @param array $aAttachments
   * @return array
   */
  private function _proceedAttachments($oAteneum, $aAttachments) {
    
    foreach ($aAttachments as $iAttId => $aAttachment) {
      if (stristr($aAttachment['filePath'], '.XML')) {
        $oFV_XML = $this->_getFV_XMLObj($aAttachment['filePath']);
        // dodajmy nr zamówienia do XML
        $aAttachments[$iAttId]['add_number'] = $this->_getFvNumer($oFV_XML);
        $aAttachments[$iAttId]['ex_ident_number'] = 0;// wyłączone tymczasowo $this->_getSourceOrderId($oAteneum, $aAttachments[$iAttId]['add_number']);
        $aAttachments[$iAttId]['items'] = $this->_getProductsToDBF($oFV_XML);
//        $oAteneum->__parseXMLgetInv($aAttachments[$iAttId]);
        $oParseProductsToDBF = $this->_getParseProductsToDBFObj($aAttachments[$iAttId]);
        unset($aAttachments[$iAttId]['items']);
        $aAttachments['DBF'] = array(
            'filePath' => $oParseProductsToDBF->doParse(),
            'ex_ident_number' => $aAttachments[$iAttId]['ex_ident_number'],
            'add_number' => $aAttachments[$iAttId]['add_number']
        );
//        $iOrderToProvider = $oAteneum->oOrdersToProviders->getOrderProviderId($aAttachments[$iAttId]['ex_ident_number']);
        $sNewOrderNumber = $aAttachments[$iAttId]['add_number'];
        $sAddNumber = $aAttachments[$iAttId]['ex_ident_number'];
        $sFilename = $aAttachments[$iAttId]['filePath'];
        dump($aAttachments[$iAttId]['ex_ident_number']);
        dump($iOrderToProvider);
        if ($iOrderToProvider > 0 && $sNewOrderNumber != '') {
          // XML
//          $oAteneum->oOrdersToProviders->InsertOrderToProviderDocument($iOrderToProvider, '1', $sNewOrderNumber, $sFilename, $sAddNumber);
          // DBF
          $sFilename = $aAttachments['DBF']['filePath'];
//          $oAteneum->oOrdersToProviders->InsertOrderToProviderDocument($iOrderToProvider, '3', $sNewOrderNumber, $sFilename, $sAddNumber);
        }
      }
    }
    return $aAttachments;
  }// end of _proceedAttachments() method
  
  
  /**
   * Metoda tworzy obiekt parsowania produktów do DBF
   * 
   * @param array $aFVData
   * @return \communicator\sources\parseProductsToDBF
   */
  private function _getParseProductsToDBFObj($aFVData) {
    return new \communicator\sources\parseProductsToDBF($aFVData, __DIR__.'/files_DBF/');
  }// end of _getParseProductsToDBFObj() method
  
  
  /**
   * Metoda pobiera dane do wygenerowania DBF
   * 
   * @param FV_XML $oFV_XML
   * @return \communicator\sources\productToDBF[]
   * @throws \communicator\sources\Ateneum\Exception
   */
  private function _getProductsToDBF(FV_XML $oFV_XML) {
    try {
      return $oFV_XML->getProductsToDBF();
    } catch (\Exception $ex) {
      throw $ex;
    }
  }// end of _getFvNumer() method
  
  
  /**
   * Metoda pobiera id zamówienia u dostawcy na podstawie id faktury
   * 
   * @param string $sFvNumber
   * @return string
   */
  private function _getSourceOrderId(Ateneum $oAteneumObj, $sFvNumber) {

    return $oAteneumObj->__getSourceOrderIdByFvNumber($sFvNumber);
  }// end of _getSourceOrderId() method
  
  
  
  /**
   * Metoda pobiera objekt głownej komunikacji z Ateneum
   * 
   * @return Ateneum
   */
  private function _getAteneumObj() {
    return new Ateneum($this->pDbMgr, $this->bTestMode);
  }// end of _getFV_XMLObj() method
  
  
  /**
   * Metoda pobiera objekt XML Faktury z Ateneum
   * 
   * @param string $sXMLFilePath
   * @return \communicator\sources\Ateneum\FV_XML
   */
  private function _getFV_XMLObj($sXMLFilePath) {
    return new FV_XML($sXMLFilePath);
  }// end of _getFV_XMLObj() method
  
  
  /**
   * Metoda pobiera nr faktury
   * 
   * @param FV_XML $oFV_XML
   * @return string
   * @throws \communicator\sources\Ateneum\Exception
   */
  private function _getFvNumer(FV_XML $oFV_XML) {
    try {
      return $oFV_XML->getFVNumber();
    } catch (\Exception $ex) {
      throw $ex;
    }
  }// end of _getFvNumer() method
  
  
  /**
   * Metoda pobiera nowe załączniki
   * 
   * @return type
   */
  private function _getXMLFiles() {
     $oMailbox = new Mailbox();
     return $oMailbox->getNewAttachments();
  }// end of _getXMLFiles() method

}

