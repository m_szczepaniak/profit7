<?php
/**
 * Klasa parsuje plik XML, a następnie towrzy i zapisuje plik DBF, jako wynikowy
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-12-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources;
class parseProductsToDBF {
  CONST DBF_BAZA_DANYCH_PATH = 'import/DBF/';
  CONST FILE_PATH_DBF = 'files_DBF/';
  
  private $aCols = array (
      0 => 
      array ('TOW_USL', 'N', 1, 0
      ),      1 => 
      array ('NAZWA_ART', 'C', 35, 0
      ),      2 => 
      array (
        'OPIS_ART','C',35,0,      
      ),      3 => 
      array (
        'NR_HANDL','C',20,0,      
      ),      4 => 
      array (
        'NR_KATAL','C',20,0,      
      ),  5 => 
      array (
        'PLU','N',6,0,
      ),      6 => 
      array (
        'BARKOD','C',20,0,
      ),      7 => 
      array (
        'IDENT_KAT','N',8,0,
      ),      8 => 
      array (
        'STAN','N',11,3,
      ),      9 => 
      array (
        'STAN_FAKT','N',11,3,
      ),      10 => 
      array (
        'STAN_MIN','N',11,3,
      ),      11 => 
      array (
        'STAN_MAX','N',11,3,
      ),      12 => 
      array (
        'CENA','N',11,2,
      ),      13 => 
      array (
        'CENAX','N',11,2,
      ),      14 => 
      array (
        'NARZUT_A','N',8,4,
      ),      15 => 
      array (
        'CENA_A','N',11,2,
      ),      16 => 
      array (
        'CENA_AX','N',11,2,
      ),      17 => 
      array (
        'UPUST_B','N',6,2,
      ),      18 => 
      array (
        'CENA_B','N',11,2,
      ),      19 => 
      array (
        'CENA_BX','N',11,2,
      ),      20 => 
      array (
        'UPUST_C','N',6,2,
      ),      21 => 
      array (
        'CENA_C','N',11,2,
      ),      22 => 
      array (
        'CENA_CX','N',11,2,
      ),      23 => 
      array (
        'CENAEWID','N',11,2,
      ),      24 => 
      array (
        'ZAMOWIENIA','N',11,3,
      ),      25 => 
      array (
        'ZAMREZER','N',11,3,
      ),      26 => 
      array (
        'ZAMOWDOST','N',11,3,
      ),      27 => 
      array (
        'CENAWALWE','N',15,4,
      ),      28 => 
      array (
        'NARZUTWAL','N',8,4,
      ),      29 => 
      array (
        'CENAWALWY','N',15,4,
      ),      30 => 
      array (
        'JEDN_M','C',7,0,
      ),      31 => 
      array (
        'JM_PODZIEL','L',1,0,
      ),      32 => 
      array (
        'STAWKA_VAT','N',5,2,
      ),      33 => 
      array (
        'ST_VAT_ZAK','N',5,2,
      ),      34 => 
      array (
        'SWW_KU','C',10,0,
      ),      35 => 
      array (
        'PKWIU','C',14,0,
      ),      36 => 
      array (
        'PCN','C',9,0,
      ),      37 => 
      array (
        'WAL_KOD','C',3,0,
      ),      38 => 
      array (
        'WAL_KURS','N',12,6,
      ),      39 => 
      array (
        'WAL_KURSS','N',12,6,
      ),      40 => 
      array (
        'WAL_DATA','D',8,0,
      ),      41 => 
      array (
        'CLO','N',6,2,      
      ),      42 => 
      array (
        'TRANSPORT','N',6,2,      
      ),      43 => 
      array (
        'AKCYZA_PR','N',6,2,
      ),      44 => 
      array (
        'AKCYZA','N',9,2,
      ),      45 => 
      array (
        'PODATEK','N',6,2,
      ),      46 => 
      array (
        'KOSZT_PROC','N',6,2,
      ),      47 => 
      array (
        'CERTYFIKAT','L',1,0,
      ),      48 => 
      array (
        'CERTYFOPIS','C',35,0,
      ),      49 => 
      array (
        'CERTYFDATA','D',8,0,
      ),      50 => 
      array (
        'PROD_MNOZ','N',11,3,
      ),      51 => 
      array (
        'LOKACJA','C',15,0,
      ),      52 => 
      array (
        'WAGA_JM_KG','N',10,4,
      ),      53 => 
      array (
        'ID_KAT','N',4,0,
      ),      54 => 
      array (
        'NAZWA_KAT','C',25,0,
      ),      55 => 
      array (
        'KSTAWKAVAT','N',6,2,
      ),      56 => 
      array (
        'CENAWE','N',13,2,
      ),      57 => 
      array (
        'CENAWEX','N',13,2,
      ),      58 => 
      array (
        'DATA_WAZN','D',8,0,
      ),      59 => 
      array (
        'NR_SERII','C',10,0,
      ),      60 => 
      array (
        'STAWKAVATP','N',5,2,
      ),      61 => 
      array (
        'DATA','D',8,0,
      ),      62 => 
      array (
        'ILOSC','N',11,3,      
      ),      63 => 
      array (
        'deleted','L',1,0
      )
    );
  
  /**
   * @var \communicator\sources\productToDBF $aFVData
   */
  private $aFVData;

  private $sNewFilePath;
  
  private $bOmmitExistsRecords;
  
  /**
   * Konstruktor
   * 
   * @param \communicator\sources\productToDBF $aFVData
   * @param string $sNewFilePath
   */
  function __construct($aFVData, $sNewFilePath) {
    $this->aFVData = $aFVData;
    $this->sNewFilePath = $sNewFilePath;
  }// end of __construct()
  
  
  /**
   * Metoda parsuje dodatkowe numery
   * 
   * @param string $sNewFilename
   * @return string
   */
  public function doParse($sNewFilename = '', $bOmmitExistsRecords = false) {
    if ($sNewFilename == '') {
      $sNewFilename = $this->aFVData['add_number'];
    }
    $this->bOmmitExistsRecords = $bOmmitExistsRecords;
    
    // wyekstrachujemy szczegóły tego zamówienia
    return $this->_parseFileItems($this->aFVData['items'], $this->aFVData['add_number']);
  }// end of doParse() method
  
  
  /**
   * Metoda przetwarza tablicę danych
   * 
   * @param \communicator\sources\productToDBF $aProductsToDBF
   * @param string $sNrInvoice
   * @return string
   */
  private function _parseFileItems($aProductsToDBF, $sNrInvoice) {
    $aInvoiceArray = array();
    
    foreach ($aProductsToDBF as $oProductToDBF) {
      $sIsbn = str_replace('-', '', (string)$oProductToDBF->isbn);
      if (empty($sIsbn)) {
        $sIsbn = (string)$oProductToDBF->ean_13;
      }
      $aInvoiceArray[$sIsbn]['bookindex'] = (string)$oProductToDBF->indexDostawcy;
      $aInvoiceArray[$sIsbn]['isbn'] = $sIsbn;
      $aInvoiceArray[$sIsbn]['ean'] = (string)$oProductToDBF->ean_13;
      $aInvoiceArray[$sIsbn]['tytul'] = (string)$oProductToDBF->tytul;
      $aInvoiceArray[$sIsbn]['pkwiu'] = (string)$oProductToDBF->pkwiu;
      $aInvoiceArray[$sIsbn]['vat'] = (int)$oProductToDBF->vat;
      $aInvoiceArray[$sIsbn]['cena_net'] = (float)$oProductToDBF->cena_net;
      $aInvoiceArray[$sIsbn]['cena_det'] = (float)$oProductToDBF->cena_det;
      
      $iCurrentQuantity = intval($oProductToDBF->ilosc);
      if (isset($aInvoiceArray[$sIsbn]['ilosc'])) {
        $aInvoiceArray[$sIsbn]['ilosc'] += $iCurrentQuantity;
      } else {
        $aInvoiceArray[$sIsbn]['ilosc'] = $iCurrentQuantity;
      }
    }
    $aEANs = array();
    foreach ($aInvoiceArray as $aItem) {
      if (!empty($aItem['ean'])) {
        $aEANs[] = (string) $aItem['ean'];
      } else {
        $aEANs[] = (string) $aItem['isbn'];
      }
    }
    
    $aInvoiceArray = $this->_getArrRecordsDBF($aInvoiceArray, array_keys($aInvoiceArray), $aEANs);
    $aInvoiceArray = $this->_defineEmptyProducts($aInvoiceArray);
    if (!empty($aInvoiceArray)) {
      $sNewDBFName = substr(str_replace('/', '', $sNrInvoice), 0, 7).'.DBF';//.'_'.str_replace('getInv__response.HTML', '', $sFileToGenerate).'
      $iDB = dbase_create($this->sNewFilePath.$sNewDBFName, $this->aCols);
      foreach ($aInvoiceArray as $aRecord) {
        dbase_add_record($iDB, array_values($aRecord));
      }
      dbase_close($iDB);
      return $this->sNewFilePath.$sNewDBFName;
    }
    return false;
  }
  
  
  private function _defineEmptyProducts($aInvoiceItems) {
    foreach ($aInvoiceItems as $sIsbn => $aItem) {
      if (!isset($aItem['CENA'])) {
        $aInvoiceItems[$sIsbn] = $this->_defineProductData($aItem);
      }
    }
    return $aInvoiceItems;
  }
  
  private function _defineProductData($aInvoiceData) {
    $aRecordTMP = $this->_getEmptyArrColsDBF($this->aCols);
    
    $aRecordTMP['CENA'] = $aInvoiceData['cena_net'];
    $aRecordTMP['CENAX'] = \Common::formatPrice2($aInvoiceData['cena_net'] * (1 + $aInvoiceData['vat']/100));
    $aRecordTMP['CENA_A'] = $aInvoiceData['cena_det'];
    $aRecordTMP['CENA_AX'] = \Common::formatPrice2($aInvoiceData['cena_det'] * (1 + $aInvoiceData['vat']/100));

//    $aRecordTMP['STAWKA_VAT'] = $aInvoiceData['vat'];
    
    //$alias = , $alias)
    $aRecordTMP['TOW_USL'] = 1;
    $aRecordTMP['NAZWA_ART'] = str_replace(array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ź', 'ż', 'Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ź', 'Ż'), array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z', 'A', 'C', 'E', 'L', 'N', 'O', 'S', 'Z', 'Z'), trimString($aInvoiceData['tytul'], 27));
    $aRecordTMP['OPIS_ART'] = $aInvoiceData['bookindex'];
    $aRecordTMP['NR_HANDL'] = 1002;
    $aRecordTMP['NR_KATAL'] = '98'.$aInvoiceData['bookindex'];
    $aRecordTMP['PLU'] = '0';
    $aRecordTMP['BARKOD'] = $aInvoiceData['ean'];
    $aRecordTMP['IDENT_KAT'] = 1002;
//    $aRecordTMP['STAN'] = 0;
//    $aRecordTMP['STAN'] = 0;
//    $aRecordTMP['STAN_FAKT'] = 0;
//    $aRecordTMP['STAN_MIN'] = 0;
//    $aRecordTMP['STAN_MAX'] = 9999999.990;
//    $aRecordTMP['NARZUT_A'] = 0;
//    $aRecordTMP['UPUST_B'] = 0;
//    $aRecordTMP['CENA_B'] = 0;
//    $aRecordTMP['CENA_BX'] = 0;
//    $aRecordTMP['UPUST_C'] = 0;
//    $aRecordTMP['CENA_C'] = 0;
//    $aRecordTMP['CENA_CX'] = 0;
//    $aRecordTMP['CENAEWID'] = 0;
//    $aRecordTMP['ZAMOWIENIA'] = 0;
//    $aRecordTMP['ZAMREZER'] = 0;
//    $aRecordTMP['ZAMOWDOST'] = 0;
//    $aRecordTMP['CENAWALWE'] = 0;
//    $aRecordTMP['NARZUTWAL'] = 0;
//    $aRecordTMP['CENAWALWY'] = 0;
    $aRecordTMP['JEDN_M'] = 'egz.';
//    $aRecordTMP['JM_PODZIEL'] = FALSE;
    $aRecordTMP['STAWKA_VAT'] = $aInvoiceData['vat'];
    $aRecordTMP['ST_VAT_ZAK'] = $aInvoiceData['vat'];
//    $aRecordTMP['SWW_KU'] = '';
    $aRecordTMP['PKWIU'] = $aInvoiceData['pkwiu'];
//    $aRecordTMP['PCN'] = '';
//    $aRecordTMP['WAL_KOD'] = '';
//    $aRecordTMP['WAL_KURS'] = 0;
//    $aRecordTMP['WAL_KURSS'] = 0;
//    $aRecordTMP['WAL_DATA'] = '';
//    $aRecordTMP['CLO'] = 0;
//    $aRecordTMP['TRANSPORT'] = 0;
//    $aRecordTMP['AKCYZA_PR'] = 0;
//    $aRecordTMP['AKCYZA'] = 0;
//    $aRecordTMP['PODATEK'] = 0;
//    $aRecordTMP['KOSZT_PROC'] = 0;
//    $aRecordTMP['CERTYFIKAT'] = TRUE;
//    $aRecordTMP['CERTYFDATA'] = $aInvoiceData['isbn'];
//    $aRecordTMP['PROD_MNOZ'] = 1;
    
    $aRecordTMP['CERTYFIKAT'] = 'T';
    $aRecordTMP['CERTYFOPIS'] = $aInvoiceData['isbn'];
    $aRecordTMP['CERTYFDATA'] = date('Ymd');
    $aRecordTMP['ID_KAT'] = 1002;
    $aRecordTMP['NAZWA_KAT'] = 'PROFIT24,COM,PL';
    $aRecordTMP['KSTAWKAVAT'] = $aInvoiceData['vat'];
    $aRecordTMP['CENAWE'] = $aRecordTMP['CENA'];
    $aRecordTMP['CENAWEX'] = $aRecordTMP['CENAX'];
    $aRecordTMP['STAWKAVATP'] = $aRecordTMP['STAWKA_VAT'];
    $aRecordTMP['DATA'] = date('Ymd');
    $aRecordTMP['ILOSC'] = $aInvoiceData['ilosc'];
    $aRecordTMP['deleted'] = 0;
    return $aRecordTMP;
  }
  
  
  /**
   * Metoda generuje tablicę na postawie schematu kolumn tabeli
   * 
   * @param array $aColsArra
   * @return string
   */
  private function _getEmptyArrColsDBF($aColsArra) {
    
    $aRow = array();
    foreach ($aColsArra as $aCol) {
      $aRow[$aCol[0]] = '';
    }
    return $aRow;
  }// end of _getEmptyArrColsDBF() method
  
  
  /**
   * Metoda przeszukuje DBF'a w poszukiwaniu rekordów o przekazanych isbn
   * 
   * @global array $aConfig
   * @param array $aProductsItems
   * @param array $aISBN tablica ISBN
   * @param array $aEANs tablica EANow
   * @return array
   */
  private function _getArrRecordsDBF($aProductsItems, $aISBN, $aEANs) {
    global $aConfig;
    
    $iDBF = dbase_open($_SERVER['DOCUMENT_ROOT'].'/'.self::DBF_BAZA_DANYCH_PATH.'ARTYKULY_BAZA_DANYCH.DBF','0');
    $iCountRecords = dbase_numrecords($iDBF);
    for ($i = 1; $i <= $iCountRecords; $i++) {
      $aRecordTMP = dbase_get_record_with_names($iDBF, $i);
      foreach ($aRecordTMP as &$sCol) {
        $sCol = trim($sCol);
      }
      if ($aRecordTMP['deleted'] == '0') {
      
        $aMatchedKeys = array_keys($aISBN, $aRecordTMP['CERTYFOPIS']);
        $aMatchedKeysEan = array_keys($aEANs, $aRecordTMP['CERTYFOPIS']);
        if (!empty($aMatchedKeys) && !empty($aMatchedKeysEan)) {
          $aMatchedKeys = array_merge($aMatchedKeys, $aMatchedKeysEan);
        } elseif (!empty($aMatchedKeysEan)) {
          $aMatchedKeys = $aMatchedKeysEan;
        }

        $aMatchedKeysBARKOD = array_keys($aISBN, $aRecordTMP['BARKOD']);
        if (!empty($aMatchedKeys) && !empty($aMatchedKeysBARKOD)) {
          $aMatchedKeys = array_merge($aMatchedKeys, $aMatchedKeysBARKOD);
        } elseif (!empty($aMatchedKeysBARKOD)) {
          $aMatchedKeys = $aMatchedKeysBARKOD;
        }

        $aMatchedKeysBARKODEan = array_keys($aEANs, $aRecordTMP['BARKOD']);
        if (!empty($aMatchedKeys) && !empty($aMatchedKeysBARKODEan)) {
          $aMatchedKeys = array_merge($aMatchedKeys, $aMatchedKeysBARKODEan);
        } elseif (!empty($aMatchedKeysBARKODEan)) {
          $aMatchedKeys = $aMatchedKeysBARKODEan;
        }

        $aMatchedKeys = array_unique($aMatchedKeys);
  //      if (!empty($aMatchedKeys)) {
  //        var_dump($aRecordTMP['CERTYFOPIS']);
  //        var_dump($aISBN);
  //        var_dump($aMatchedKeys);die;
  //      }
        foreach ($aMatchedKeys as $iMatchedProductsItemKey) {
          if ($iMatchedProductsItemKey !== FALSE) {
            $sKeyIsbn = $aISBN[$iMatchedProductsItemKey];
            // jeśli już zdefiniowany, a ilość większa od 1 to nadpisujemy
            if ($this->bOmmitExistsRecords === TRUE) {
              unset($aProductsItems[$sKeyIsbn]);
              unset($aISBN[$iMatchedProductsItemKey]);
            } else {

              if (
                      isset($aProductsItems[$sKeyIsbn]) && 
                      isset($aProductsItems[$sKeyIsbn]['STAN']) && 
                      isset($aRecordTMP['STAN']) &&
                      ($aRecordTMP['STAN'] > $aProductsItems[$sKeyIsbn]['STAN'])) {
                // nadpisujemy
                $aProductsItems[$sKeyIsbn] = $this->_overwriteParamsWHChange($aProductsItems[$sKeyIsbn], $aRecordTMP);
              } elseif (!isset($aProductsItems[$sKeyIsbn]['STAN'])) {
                // definiujemy
                $aProductsItems[$sKeyIsbn] = $this->_overwriteParams($aProductsItems[$sKeyIsbn], $aRecordTMP);
              }

              /*
              if (!isset($aProductsItems[$sKeyIsbn]['STAN'])) {
                // definiujemy
                $aProductsItems[$sKeyIsbn] = $this->_overwriteParams($aProductsItems[$sKeyIsbn], $aRecordTMP);
              }
                */
            }
          }
        }
      }
    }
    return $aProductsItems;
  }// end of _getArrRecordsDBF() method
  
  
  /**
   * Metoda nadpisuje wszystkie pola z tablicy źródłowej do docelowej, poza wybranymi
   * 
   * @param array $aSourceArr
   * @param array $aDestArr
   * @return array
   */
  private function _overwriteParamsWHChange($aSourceArr, $aDestArr) {
    $aMergedArray = array();
    $aColsToMoveFromSource = array(
        'CENA', 'CENAX', 'CENA_A', 'CENA_AX', 'CENAWE', 'CENAWEX', 'STAWKA_VAT', 'STAWKAVATP', 'KSTAWKAVAT', 'ILOSC', 'DATA', 'deleted', 'CERTYFIKAT'
    );
    
    foreach ($aSourceArr as $sColName => $sValue) {
      if (in_array($sColName, $aColsToMoveFromSource)) {
        // jeśli, jest w grupie kolumn do przeniesienia z tabli źródłowej
        $aMergedArray[$sColName] = $sValue;
      } else {
        // wszystkie inne pobierz z tablicy docelowej
        $aMergedArray[$sColName] = $aDestArr[$sColName];
      }
    }
    return $aMergedArray;
  }// end of _overwriteParamsWHChange() method
  
  
  /**
   * Nadpisujemy pola, jeśli pozycja istnieje w DBF
   * 
   * @param array $aInvoiceData
   * @param array $aRecordTMP
   * @return array
   */
  private function _overwriteParams($aInvoiceData, $aRecordTMP) {
    $aRecordTMPNEW = $this->_getEmptyArrColsDBF($this->aCols);
    
    $aRecordTMP['CENA'] = $aInvoiceData['cena_net'];
    $aRecordTMP['CENAX'] = \Common::formatPrice2($aInvoiceData['cena_net'] * (1 + $aInvoiceData['vat']/100));
    $aRecordTMP['CENA_A'] = $aInvoiceData['cena_det'];
    $aRecordTMP['CENA_AX'] = \Common::formatPrice2($aInvoiceData['cena_det'] * (1 + $aInvoiceData['vat']/100));
    $aRecordTMP['CENAWE'] = $aRecordTMP['CENA'];
    $aRecordTMP['CENAWEX'] = $aRecordTMP['CENAX'];
    $aRecordTMP['STAWKA_VAT'] = $aInvoiceData['vat'];
    $aRecordTMP['STAWKAVATP'] = $aRecordTMP['STAWKA_VAT'];
    $aRecordTMP['KSTAWKAVAT'] = $aInvoiceData['vat'];
    $aRecordTMP['ILOSC'] = $aInvoiceData['ilosc'];
    $aRecordTMP['ID_KAT'] = 1002;
    $aRecordTMP['NAZWA_KAT'] = 'PROFIT24,COM,PL';
    $aRecordTMP['DATA'] = date('Y-m-d');
    $aRecordTMP['NR_SERII'] = "";
    $aRecordTMP['DATA_WAZN'] = "";
    $aRecordTMP['deleted'] = 0;
    $aRecordTMP['CERTYFIKAT'] = 'T';
    
    $aRecordTMP = array_merge($aRecordTMPNEW, $aRecordTMP);
    
    return $aRecordTMP;
  }// end of _overwriteParams() method
}