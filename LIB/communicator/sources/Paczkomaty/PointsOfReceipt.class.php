<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-13 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Paczkomaty;
include_once('Paczkomaty.class.php');
class PointsOfReceipt extends Paczkomaty implements \communicator\sources\iPointsOfReceipt {
  
  public $aTransportSettings;
  public $bTestMode;
  public $pDbMgr;
  
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    parent::__construct($bTestMode);
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }

  /**
   * 
   * @param string $sPointOfReceipt
   * @return bool
   */
  public function checkDestinationPointAvailable($sPointOfReceipt) {
    return $this->checkInpostMachnesStatus($sPointOfReceipt);
  }
  
  /**
   * 
   * @param string $sPointOfReceipt
   * @return bool
   */
  public function checkDestinationPointAvailableDB($sPointOfReceipt) {
    
    $sSql = 'SELECT code FROM paczkomaty_machines WHERE code = "'.$sPointOfReceipt.'"';
    return ($this->pDbMgr->GetOne('profit24', $sSql) != '' ? true : false);
  }

  /**
   * 
   * @return array
   */
  public function getDestinationPointsList() {
    return inpost_get_machine_list();
  }
  
  /**
   * 
   * @return array
   */
  public function getDestinationPointsDropdown() {
    return $this->getPointsOfReceipt(array('`desc` AS label', '`code` AS value'), ' ORDER BY `desc` ASC');
  }

  /**
   * 
   * @param string $sPointOfReceipt
   * @return string
   */
  public function getSinglePointDetails($sPointOfReceipt) {
		include_once('paczkomaty/inpost.php');
		$machines_all = inpost_get_machine_list();
		$machines_with_names_as_keys = array();

		if (count($machines_all)) {
			foreach ($machines_all as $machine) {
				$machines_with_names_as_keys[$machine['name']] = $machine;
			}
		}

		if (!empty($machines_with_names_as_keys)) {
			foreach ($machines_with_names_as_keys as $machine) {
				if ($sPointOfReceipt == $machine['name']) {
					return $machine;
				}
			}
		}
    return false;
  }
  
  
  /**
   * 
   * @param array $aPontsOfRecipt
   * @return array
   */
  private function doFormatPointsOfReceiptAutocomplete($aPontsOfRecipt) {
    
    // sortowanie listy paczkomatów wg miasta i ulicy
    $aSort = array();
    foreach($aPontsOfRecipt as $iKey => $aValue) {
        $aValue['data'] = unserialize($aValue['data']);
        $aSort['city'][$iKey] = $aValue['data'][4];
        $aSort['street'][$iKey] = $aValue['data'][1];
        $aPontsOfRecipt[$iKey]['data'] = json_encode($aValue['data']);
    }

    setlocale(LC_COLLATE,'pl_PL.utf8');
    array_multisort($aSort['city'], SORT_LOCALE_STRING, SORT_ASC, $aSort['street'], SORT_LOCALE_STRING, SORT_ASC, $aPontsOfRecipt);
    return $aPontsOfRecipt;
  }// end of doFormatPointsOfReceipt() method
  
  
  /**
   * 
   * @param array $aCols
   * @param string $sAddSQL
   * @return array
   */
  private function getPointsOfReceipt($aCols, $sAddSQL) {
    
    $sSql = "SELECT ".implode(',', $aCols)."
             FROM paczkomaty_machines 
             ".$sAddSQL;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param array $aCols
   * @param string $sAddSQL
   * @return array
   */
  public function getPointsOfReceiptAutocomplete($aCols, $sAddSQL) {
    $aPointsOfReceipt = $this->getPointsOfReceipt($aCols, $sAddSQL);
    return $this->doFormatPointsOfReceiptAutocomplete($aPointsOfReceipt);
  }
  
  
   /**
   * 
   * 
   * @param int $sPointOfReceipt
   * @return array
   */  
	public function getPopupDetails($sPointOfReceipt) {

    $sFontColor = '#000';
    $sBackgroundColor = '#FFD000';
    $aFormatedPointOfReciptDetails = $this->getSinglePointDetails($sPointOfReceipt); 
    
    $sDetails = "Wybrany paczkomat: <b><br />"
            . $aFormatedPointOfReciptDetails['postcode'] . ' '. $aFormatedPointOfReciptDetails['town'] . ', ' . $aFormatedPointOfReciptDetails['street'] .' '. $aFormatedPointOfReciptDetails['buildingnumber'] . '</b><br /><br />'.$aFormatedPointOfReciptDetails['locationdescription'];

    if (!empty($aFormatedPointOfReciptDetails) && is_array($aFormatedPointOfReciptDetails)) {
      $sSrc = 'https://maps.google.pl/maps?f=q&source=s_q&hl=pl&geocode=&q='
              .$aFormatedPointOfReciptDetails['latitude'].','.$aFormatedPointOfReciptDetails['longitude']
              .'('.$aFormatedPointOfReciptDetails['postcode'].' '.$aFormatedPointOfReciptDetails['town'].' '.$aFormatedPointOfReciptDetails['street'].' '.$aFormatedPointOfReciptDetails['buildingnumber']
              .')&aq=&vpsrc=0&ie=UTF8&t=m&z=14&'
              .$aFormatedPointOfReciptDetails['latitude'].','.$aFormatedPointOfReciptDetails['longitude']
              .'&output=embed';
    }

    return array('fontColor' => $sFontColor, 'backgroundColor' => $sBackgroundColor, 'details' => $sDetails, 'source' => $sSrc);  
    }  
}
