<?php
/**
 * Klasa obsługi paczkomaty, części tylko niestety
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-10-24 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Paczkomaty;
include_once('paczkomaty/inpost.php');
class Paczkomaty {

  /**
   *
   * @var array
   */
  private $aTransportSettings;

  /**
   *
   * @var bool test mode
   */
  private $bTestMode;
  
  /**
   *
   * @var array
   */
  private $aMachines;

  /**
   *
   * @var \DatabaseManager baza 
   */
  public $pDbMgr;
  
  /**
   * 
   * @global \DatabaseManager $pDbMgr
   * @param bool $bTestMode
   */
  function __construct($aTransportSettings, $bTestMode = true) {
    global $pDbMgr, $aConfig, $inpost_api_url;

    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    $this->aTransportSettings = $aTransportSettings;

      if (true === $this->bTestMode) {
          $inpost_api_url = $aConfig['common']['paczkomaty']['api'];
      }
  }// end of __construct() method
  
  
  /**
   * Metoda sprawdza, czy przekazany punkt odbioru jest aktywny
   * 
   * @param array $sVerificationMachine
   * @return array
   */
  public function checkInpostMachnesStatus($sVerificationMachine){ 

    if (empty($this->aMachines)) {
      $this->aMachines = $this->getMachinesArr();
    }
      
    if (!empty($this->aMachines)) {
      if (in_array($sVerificationMachine, $this->aMachines)) {
        // nie usuwamy z tablicy tego
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
    return false;
  }// end of disabledInpostMachnesStatus() method
  
  
  /**
   * Metoda ustawia Paczkomaty
   * 
   * @global string $inpost_data_dir
   * @return array
   */
  private function getMachinesArr() {
    global $inpost_data_dir;
    
    if (inpost_cache_is_valid(1) == 0) {
        inpost_download_machines();
    }
    $cache = @file_get_contents($inpost_data_dir."/cache1.dat");
    if ($cache) {
      $aMachineListData = unserialize($cache);
      return $this->getMachines($aMachineListData);
    }
    return false;
  }// end of setMachines() method
  
  
  /**
   * Metoda pobiera listę paczkomatów w jednej tablicy
   * 
   * @param array $aMachineList
   * @return array
   */
  private function getMachines($aMachineList) {
    $aMachines = array();
    foreach ($aMachineList as $aMachine) {
      if (isset($aMachine[0]) && $aMachine[0] != '') {
        $aMachines[] = $aMachine[0];
      }
    }
    return $aMachines;
  }// end of getMachines() method
  
  
  /**
   * Metoda zapisuje listę i zwraca numer listu przewozowego
   * 
   * @param array $aOrder
   * @param string $sWebsiteEmail
   * @param array $aSellerData
   * @return boolean
   * @throws \Exception
   */
  public function doUtworzList($aOrder, $sWebsiteEmail, $aSellerData) {
    global $aConfig;
    
    $aPacksData = array();
		if ($this->bTestMode === true) {
			// tryb test
			$aPacksData[0]['adreseeEmail'] = $aOrder['email'];
			$aPacksData[0]['senderEmail'] = $this->aTransportSettings['login'];
			$aPacksData[0]['phoneNum'] = '669709928'; // test telefon
			$aPacksData[0]['boxMachineName'] = $aOrder['point_of_receipt'];
            /** Zlecił p.sedziak 31.05.2017 */
			$aPacksData[0]['packType'] = 'C';//$aOrder['transport_option_symbol'];
			$aPacksData[0]['insuranceAmount'] = '';
			$aPacksData[0]['onDeliveryAmount'] = '';
			$aPacksData[0]['customerRef'] = $aOrder['order_number']; // opis od ksiegarni
		} else {
			// dane oficjalne
			$aPacksData[0]['adreseeEmail'] = $aOrder['email'];
			$aPacksData[0]['senderEmail'] = $this->aTransportSettings['login'];
			$aPacksData[0]['phoneNum'] = empty($aOrder['phone_paczkomaty']) ? $aOrder['phone'] : $aOrder['phone_paczkomaty']; // jeśli telefon paczkomaty jest pusty, oznacza to zmianę metody transportu, pobieramy telefon z głównego pola
			$aPacksData[0]['boxMachineName'] = $aOrder['point_of_receipt'];
			/** Zlecił p.sedziak 31.05.2017 */
			$aPacksData[0]['packType'] = 'C';//$aOrder['transport_option_symbol'];
			$aPacksData[0]['insuranceAmount'] = '';
			$aPacksData[0]['onDeliveryAmount'] = '';
			$aPacksData[0]['customerRef'] = $aOrder['order_number']; // opis od ksiegarni
		}
		// dane nadawcy widoczne na etykiecie
		$aPacksData[0]['senderAddress'] = array(
				'name' => $aSellerData['paczkomaty_name'],
				'email' => $sWebsiteEmail,
				'phoneNum' => $aSellerData['phone'],
				'zipCode' => $aSellerData['postal']
				/*
 				 * Wyłączenie z dnia 05.06.2012 - modyfikacja ustalona z Marcinem Chudy
					'street' => $aSellerAddresses['street'],
					'buildingNo' => $aSellerAddresses['number'],
					'flatNo' => $aSellerAddresses['number2'],
					'town' => $aSellerAddresses['city'],
					'zipCode' => $aSellerAddresses['postal'],
				 */
		);
		// UWAGA - lokalnie uzywamy konta testowe:
		$aReturn = inpost_send_packs($this->aTransportSettings['login'], 
											$this->aTransportSettings['password'], 
											$aPacksData, 
											0, 
											0);
    if ($aReturn['0']['error_key'] == 'BoxDoesNotExist') {
      throw new \Exception('Wystąpił błąd podczas tworzenia numeru listu przewozowego w zamówieniu "'.$aOrder['order_number'].'" w Paczkomatach '.
              "\n".'<br /> Brak aktywnego paczkomatu. Komunikat błedu z systemu Paczkomaty: '.print_r($aReturn, true));
		} elseif ($aReturn['0']['packcode'] == '' && $aReturn['0']['error_key'] != '') {
      throw new \Exception('Wystąpił błąd podczas tworzenia numeru listu przewozowego w zamówieniu "'.$aOrder['order_number'].'" w Paczkomatach '.
              "\n".'<br /> Komunikat błedu z systemu Paczkomaty: '.print_r($aReturn, true));
		} elseif ($aReturn['0']['packcode'] == '') {
      throw new \Exception('Wystąpił błąd podczas tworzenia numeru listu przewozowego w zamówieniu "'.$aOrder['order_number'].'" w Paczkomatach '.
              "\n".'<br /> Komunikat błedu z systemu Paczkomaty: '.print_r($aReturn, true));
		} else {
      return $aReturn['0']['packcode'];
    }
  }// end of doUtworzList() method
  
  
  /**
   * Metoda zapisuje numer listu przewozowego
   * 
   * @todo Tutaj przewudzidzieć także wiele listów do jednego zamowienia, pewnie zostanie to opracowane poziom wyżej
   * 
   * @global \DatabaseManager $pDbMgr
   * @param string $sTransportNumber
   * @param int $iOrderId
   * @param string $sLogin login
   * @param string $sPassword haslo
   * @param bool $bPrint
   * @return boolean
   */
  public function doZapiszList($sTransportNumber, $iOrderId, $bPrint = true) {
    global $aConfig;
    
    try {
      $this->setTransportList($iOrderId, $sTransportNumber);
      
      $sPDF = $this->getSticker($sTransportNumber, $this->aTransportSettings['login'], $this->aTransportSettings['password']);
      
      $sFilePath = $this->getTransportListFilename($aConfig, $sTransportNumber);
      if (file_put_contents($sFilePath, $sPDF, FILE_APPEND | LOCK_EX) === false) {
        throw new \Exception('Wystąpił błąd podczas zapisu pliku PDF');
      }
      
      if ($bPrint == true) {
        // drukujemy sobie
        if ($this->addToPrintQueue($sFilePath, $_COOKIE['printer_labels']) === false) {
          return false;
        }
      }
      
    } catch (\Exception $ex) {
      throw new \Exception($ex->getMessage());
    }
  }// end of doZapiszList() method
  
  /**
   * 
   * @param type $agConfig
   * @param type $sTransportNumber
   * @return type
   */
  public function getTransportListFilename($agConfig, $sTransportNumber) {
    
    $sFilename = 'Paczka_'.$sTransportNumber.'.pdf';
    return $agConfig['common']['client_base_path'].'/'.$agConfig['common']['cms_dir'].$agConfig['common']['paczkomaty_dir'].$sFilename;
  }// end of getTransportListFilename() method


  /**
   * Metoda pobiera etykiete z paczkomatow
   *
   * @throws Exception
   * @param string $sTransportNumber
   * @param string $sLogin
   * @param string $sPassword
   * @return string
   */
  private function getSticker($sTransportNumber, $sLogin, $sPassword) {

    $sPDF = inpost_get_sticker($sLogin, 
																$sPassword,
																$sTransportNumber,
																'A6P');
    if (!empty($sPDF) && is_array($sPDF) === false) {
      // OK
      return $sPDF;
    } else {
      throw new \Exception(_('Wystąpił błąd podczas pobierania etykiety <br /> Komunikat z Paczkomaty : '. print_r($sPDF, true)));
    }
  }// end of getSticker() method


  /**
   * Metoda zapisuje nr listu przewozowego
   *
   * @param type $iOrderId
   * @param type $sTransportNumber
   * @return boolean
   * @throws Exception
   */
  private function setTransportList($iOrderId, $sTransportNumber) {
    
    
    $aValues = array(
        'transport_number' => $sTransportNumber,
    );
    if ($this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOrderId) === false) {
      throw new \Exception(_('Wystąpił błąd podczas zapisywania numeru listu przewozowego'));
    }
    return true;
  }// end of setTransportList() method
  
  
  /**
   * Dodajemy dokument do kolejki drukowania 
   * 
   * @global \DatabaseManager $pDbMgr
   * @param string $sFilePath
   * @param string $sPrinter
   * @return boolean
   */
  public function addToPrintQueue($sFilePath, $sPrinter) {
    
    $aValues = array(
        'filetype' => 'PDF',
        'filepath' => $sFilePath,
        'dest_printer' => $sPrinter,
        'created' => 'NOW()'
    );
    if ($this->pDbMgr->Insert('profit24', 'print_queue', $aValues) === false) {
      return false;
    }
    return true;
  }// end of addToPrintQueue() method
  
  
  public function pobierzStatusyPrzesylki($sTransportNumber) {
    return inpost_get_pack_status($sTransportNumber);
  }
}

