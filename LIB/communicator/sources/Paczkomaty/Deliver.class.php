<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-13 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Paczkomaty;
include_once('Paczkomaty.class.php');
class Deliver extends \orders\Shipment\CommonDeliver implements \communicator\sources\iDeliver {

  public $aTransportSettings;
  public $bTestMode;
  public $pDbMgr;
  
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    parent::__construct($aTransportSettings, $pDbMgr, $bTestMode);
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }

  /**
   * 
   * @param array $aTransportNumbers
   * @param array $aOrdersIds
   * @return string path file
   */
  public function getShipmentList($aTransportNumbers, $aOrdersIds = array()) {
    
    $sPDF = inpost_get_confirm_printout($this->aTransportSettings['login'], 
                                        $this->aTransportSettings['password'],  
                                        $aTransportNumbers, 
                                        $this->bTestMode );
    
    if ($sPDF != '' && is_array($sPDF) === false) {
      $sDeliverFilename = $this->saveDeliverGetFileNamePathTRANSACTION($sPDF, $aOrdersIds);
    } else {
      throw new \Exception(_('Wystąpił błąd podczas pobierania delivera z '.$this->aTransportSettings['class_symbol'].' Komunikat odpowiedzi: '.print_r($sPDF, true)));
    }
    return $sDeliverFilename;
  }
}
