<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-13 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Paczkomaty;
class Shipment extends Paczkomaty implements \communicator\sources\iShipment  {

  public $bTestMode;
  public $pDbMgr;
  public $aTransportSettings;

  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    parent::__construct($aTransportSettings, $bTestMode);
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }

  /**
   * 
   * @param array $aOrder
   * @param array $aDeliverAddress
   * @param array $aWebsiteSettings
   * @return string - transport number
   */
  public function addShipment($aOrder, $aDeliverAddress, $aWebsiteSettings) {
    
    try {
      $sTransportNumber = $this->doUtworzList($aOrder, $aWebsiteSettings['email'], $aWebsiteSettings);
      if (!empty($sTransportNumber)) {
        $this->doZapiszList($sTransportNumber, $aOrder['id'], false);
        return $sTransportNumber;
      } else {
        throw new Exception(_('Brak nr listu przewozowego'));
      }
    } catch (\Exception $ex) {
      throw new \Exception($ex->getMessage());
    }
  }
  
  

  public function doCancelShipment() {
    
  }

  /**
   * 
   * @global \DatabaseManager $pDbMgr
   * @param string $sFilePath
   * @param string $sPrinter
   * @return boolean
   */
  public function doPrintAddressLabel($sFilePath, $sPrinter) {
    global $pDbMgr;
    
    $aValues = array(
        'filetype' => 'PDF',
        'filepath' => $sFilePath,
        'dest_printer' => $sPrinter,
        'created' => 'NOW()'
    );
    if ($pDbMgr->Insert('profit24', 'print_queue', $aValues) === false) {
      return false;
    }
    return true;
  }

  /**
   * 
   * @param string $sFilePath
   * @param int $iOrderId
   * @return string
   */
  public function getAddressLabel($sFilePath, $iOrderId) {
    return $sFilePath; // ;-) [:->
  }

  /**
   * 
   * @param int $iId
   * @param string $sTransportNumber
   * @return array
   */
  public function getDeliveryStatus($iId, $sTransportNumber) {
   
    $aStatus = $this->pobierzStatusyPrzesylki($sTransportNumber);
    
    $aStatusesCollection = array();
    if (isset($aStatus['status']) && isset($aStatus['statusDate'])) {
      $oDeliveryShipment = new \orders\Shipment\DeliveryShipment();
      $oDeliveryShipment->sStatusName = $aStatus['status'];  
      $oDeliveryShipment->sStatusDate = $aStatus['statusDate'];
      $aStatusesCollection[] = $oDeliveryShipment;
    }
 
    return $aStatusesCollection;      
  }

  /**
   * 
   * @param array $agConfig
   * @param string $sTransportNumber
   * @return string
   */
  public function getTransportListFilename($agConfig, $sTransportNumber) {
    return $agConfig['common']['client_base_path'].'/'.$agConfig['common']['cms_dir'].$agConfig['common']['paczkomaty_dir'].'Paczka_'.$sTransportNumber.'.pdf';
  }// end of getTransportListFilename() method
}

