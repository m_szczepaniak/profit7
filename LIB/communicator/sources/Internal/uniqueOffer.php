<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-20 
 * @copyrights Marcin Chudy - Profit24.pl
 * @global $pDbMgr \DatabaseManager
 */

header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(180); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '1G');

global $pDbMgr;


array(
    'EAN_13',
    'ISBN_13',
    'ISBN_10',
);

$sSql = file_get_contents('notUniqueEAN_13.sql');
$oGetProducts = $pDbMgr->PlainQuery('profit24', $sSql);
$xml = new SimpleXMLElement('<products/>');


while ($aItem =& $oGetProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
  $product = $xml->addChild('product');
  foreach ($aItem as $sKey => $sValue) {
    $product->addChild($sKey, str_replace('&', '&amp;', $sValue));
  }
}
$xml->saveXML('notUniqueEAN_13.xml');



/*
  SELECT 
  DISTINCT P.id,
  P.name, 
  P.name2, 
  P.price_netto,
  P.price_brutto,
  P.vat,
  P.pkwiu,
  P.weight,
  P.isbn_plain,
  P.isbn_plain,
  P.isbn_13,
  P.isbn_10,
  P.ean_13,
  P.publication_year,
  P.pages,
  P.volumes,
  P.volume_nr,
  P.edition,
  P.legal_status_date,
  P.type,
  MI.name AS category,
  PP.name AS publisher,
  PS.name AS publisher_series,
  (
    SELECT GROUP_CONCAT(CONCAT(name, " ", surname) SEPARATOR ";; ")
    FROM products_authors AS PA
    JOIN products_to_authors AS PTA
      ON PTA.author_id = PA.id
    WHERE PTA.product_id = P.id
    LIMIT 1
  ) AS authors,
  PB.binding,
  PD.dimension,
  PL.language
  FROM products AS P
  JOIN products_extra_categories AS PEC
    ON PEC.product_id = P.id
  JOIN menus_items AS MI 
    ON PEC.page_id = MI.id
  JOIN products_publishers AS PP
    ON PP.id = P.publisher_id 
  LEFT JOIN products_to_series AS PTS
    ON PTS.product_id = P.id
  LEFT JOIN products_series AS PS
    ON PTS.series_id = PS.id
  LEFT JOIN products_bindings AS PB
    ON PB.id = P.binding
  LEFT JOIN products_dimensions AS PD
    ON PD.id = P.dimension
  LEFT JOIN products_languages AS PL
    ON PL.id = P.language
    

  WHERE P.id < 733518
  

  ORDER BY P.id DESC
  LIMIT 20;
  */
