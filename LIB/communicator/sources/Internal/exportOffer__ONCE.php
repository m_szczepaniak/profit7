<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-20 
 * @copyrights Marcin Chudy - Profit24.pl
 * @global $pDbMgr \DatabaseManager
 */

header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(180); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '1G');

$sSql = file_get_contents('getOffer.sql');
global $pDbMgr;
$oGetProducts = $pDbMgr->PlainQuery('profit24', $sSql);
$xml = new SimpleXMLElement('<products/>');


function getImage($iProductId) {
  global $aConfig;
  
  if ($iProductId > 0) {

    $sSql = "SELECT *
            FROM products_images
            WHERE product_id = ".$iProductId." 
            LIMIT 1";
    $aValue = Common::GetRow($sSql);
    if(!empty($aValue)){
      if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/__b_'.$aValue['photo'])){
        $sImage = 'images/photos/'.$aValue['directory'].'/__b_'.$aValue['photo'];
      }
      else if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/'.$aValue['photo'])){
        $sImage = 'images/photos/'.$aValue['directory'].'/'.$aValue['photo'];
      }
      else if(file_exists($aConfig['common']['client_base_path'].'images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'])){
        $sImage = 'images/photos/'.$aValue['directory'].'/__t_'.$aValue['photo'];
      }
    }
    if ($sImage != '') {
      $aImage = explode('.', $sImage);
      $dimage = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/'.$sImage);
      if (!empty($dimage)) {
        file_put_contents('images/'.$iProductId.'.'.array_pop($aImage), $dimage);
      }
    }
  }
}
function getProductCategorySingleBranch($iProductId, $sSeparator) {
  global $pDbMgr;
  $sCategories = '';
  $sCategoriesIds = '';
  
  $sSql = 'SELECT MI.name, MI.id FROM menus_items AS MI
          JOIN products_extra_categories AS PEC ON PEC.page_id = MI.id
          WHERE PEC.product_id = '.$iProductId.'
          AND MI.parent_id IS NULL 
          ORDER BY MI.order_by DESC ';
  $aRow1 = $pDbMgr->GetRow('profit24', $sSql);
  $sCategories = filterNameGroup($aRow1['name']);
  $sCategoriesIds = $aRow1['id'];
  
  //drugi poziom
  if (!empty($aRow1)) {
    $sSql = 'SELECT MI.name, MI.id FROM menus_items AS MI
            JOIN products_extra_categories AS PEC ON PEC.page_id = MI.id
            WHERE PEC.product_id = '.$iProductId.'
            AND MI.parent_id = '.$aRow1['id'].' 
            ORDER BY MI.order_by DESC ';
    $aRow2 = $pDbMgr->GetRow('profit24', $sSql);
    if (!empty($aRow2)) {
      $sCategories .= $sSeparator . filterNameGroup($aRow2['name']);
      $sCategoriesIds .= $sSeparator . $aRow2['id'];
    }
  }
  
  //trzeci poziom
  if (!empty($aRow2)) {
    $sSql = 'SELECT MI.name, MI.id FROM menus_items AS MI
            JOIN products_extra_categories AS PEC ON PEC.page_id = MI.id
            WHERE PEC.product_id = '.$iProductId.'
            AND MI.parent_id = '.$aRow2['id'].' 
            ORDER BY MI.order_by DESC ';
    $aRow3 = $pDbMgr->GetRow('profit24', $sSql);
    if (!empty($aRow3)) {
      $sCategories .= $sSeparator . filterNameGroup($aRow3['name']);
      $sCategoriesIds .= $sSeparator . $aRow3['id'];
    }
  }
  
  //czwarty poziom
  if (!empty($aRow3)) {
    $sSql = 'SELECT MI.name, MI.id FROM menus_items AS MI
            JOIN products_extra_categories AS PEC ON PEC.page_id = MI.id
            WHERE PEC.product_id = '.$iProductId.'
            AND MI.parent_id = '.$aRow3['id'].' 
            ORDER BY MI.order_by DESC ';
    $aRow4 = $pDbMgr->GetRow('profit24', $sSql);
    if (!empty($aRow4)) {
      $sCategories .= $sSeparator . filterNameGroup($aRow4['name']);
      $sCategoriesIds .= $sSeparator . $aRow4['id'];
    }
  }
  $aCategories = array(
      'names' => $sCategories,
      'ids' => $sCategoriesIds
  );
  return $aCategories;
}

/**
 * 
 * @param string $sStr
 * @param int $iMaxLength
 * @param int $iItemId
 * @return string
 */
function trimIf($sStr, $iMaxLength, $iItemId) {
  $iStrLen = mb_strlen($sStr, 'UTF-8');
  if ($iStrLen > $iMaxLength) {
    $sAdditionalStr = '-'.$iItemId;
    $iLenAdditionalStr = mb_strlen($sAdditionalStr, 'UTF-8');
    $sStr = mb_substr($sStr, 0, ($iMaxLength - $iLenAdditionalStr), 'UTF8') . $sAdditionalStr;
    return $sStr;
  }
  return $sStr;
}

function filterNameGroup($sName) {
  return str_replace('-', ' ', $sName);
}


while ($aItem =& $oGetProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
  $product = $xml->addChild('product');
  $aCategoriesData = getProductCategorySingleBranch($aItem['id'],' - ');
  $aItem['category'] = $aCategoriesData['names'];
  $aItem['ID_category'] = $aCategoriesData['ids'];
  $aSeries = explode(' ;; ', $aItem['series']);
  $aPublishers = explode(' ;; ', $aItem['publisher']);
  
  $aItem['publisher'] = trimIf($aPublishers[0], 49, $aPublishers[1]);
  $aItem['ID_publisher'] = $aPublishers[1];
  $aItem['series'] = trimIf($aSeries[0], 49, $aSeries[1]);
  $aItem['ID_series'] = $aSeries[1];
  //getImage($aItem['id']);
  
  foreach ($aItem as $sKey => $sValue) {
    $product->addChild($sKey, str_replace('&', '&amp;', $sValue));
  }
}
$xml->saveXML('Offer/Offer.xml');



/*
  SELECT 
  DISTINCT P.id,
  P.name, 
  P.name2, 
  P.price_netto,
  P.price_brutto,
  P.vat,
  P.pkwiu,
  P.weight,
  P.isbn_plain,
  P.isbn_plain,
  P.isbn_13,
  P.isbn_10,
  P.ean_13,
  P.publication_year,
  P.pages,
  P.volumes,
  P.volume_nr,
  P.edition,
  P.legal_status_date,
  P.type,
  MI.name AS category,
  PP.name AS publisher,
  PS.name AS publisher_series,
  (
    SELECT GROUP_CONCAT(CONCAT(name, " ", surname) SEPARATOR ";; ")
    FROM products_authors AS PA
    JOIN products_to_authors AS PTA
      ON PTA.author_id = PA.id
    WHERE PTA.product_id = P.id
    LIMIT 1
  ) AS authors,
  PB.binding,
  PD.dimension,
  PL.language
  FROM products AS P
  JOIN products_extra_categories AS PEC
    ON PEC.product_id = P.id
  JOIN menus_items AS MI 
    ON PEC.page_id = MI.id
  JOIN products_publishers AS PP
    ON PP.id = P.publisher_id 
  LEFT JOIN products_to_series AS PTS
    ON PTS.product_id = P.id
  LEFT JOIN products_series AS PS
    ON PTS.series_id = PS.id
  LEFT JOIN products_bindings AS PB
    ON PB.id = P.binding
  LEFT JOIN products_dimensions AS PD
    ON PD.id = P.dimension
  LEFT JOIN products_languages AS PL
    ON PL.id = P.language
    

  WHERE P.id < 733518
  

  ORDER BY P.id DESC
  LIMIT 20;
  */
