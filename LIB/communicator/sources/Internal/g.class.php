<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');

/**
 * Klasa generuje DBF z produktami, które zostały zamówione po raz pierwszy.
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-04 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class generateNewProductsDBF {
  private $pDbMgr;
  
  function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  
    $this->getNewProducts();
  }
  
  private function getNewProducts() {

      $items = [];
      $file = fopen('targi.csv', 'r');
      while (($item = fgetcsv($file, 0, ',', '"', '"')) !== false){
          $items[] = $item;
      }

    // porownujemy z DBF
    $aOProductDBFItems = $this->getProductsToDBF($items);
    $this->doParse($aOProductDBFItems);
  }// end of getNewProducts() method
  
  
  /**
   * Metoda pobiera produkty w formacie zgodnym z eksportem DBF
   * 
   * @param array $aOrdersItems
   * @return \communicator\sources\productToDBF[]
   */
  public function getProductsToDBF($aOrdersItems) {
    $aOProductsToDBF = array();
    
    foreach ($aOrdersItems as $aItem) {
        $vat = intval(str_replace('%', '', $aItem[5]));
      $oProductToDFB = new \communicator\sources\productToDBF();
//      $oProductToDFB->indexDostawcy = (string)($aItem['isbn_plain'].$aItem['id']);
      $oProductToDFB->tytul = (string)$aItem[2];
      $oProductToDFB->isbn = (string)$aItem[1];
      $oProductToDFB->ean_13 = (string)$aItem[0];
      $oProductToDFB->pkwiu = (string)'58.11.1'; //(string)$oPozycja->towar->pkwiu;
      $oProductToDFB->cena_det = (string)Common::formatPrice2(Common::formatPrice2($aItem[4])/(1+$vat/100));

      $oProductToDFB->cena_net = (string)'0.00';
      $oProductToDFB->vat = (string)$vat;
      $oProductToDFB->ilosc = (string)$aItem[3];
      $aOProductsToDBF[] = $oProductToDFB;
    }
    return $aOProductsToDBF;
  }// end of getProductsToDBF() method
  
  
  /**
   * Metoda porównuje nowo zamówione produkty z plikiem DBF
   * 
   * @param array $aOProductDBFItems
   */
  private function doParse($aOProductDBFItems) {
    $aNewArr['items'] = $aOProductDBFItems;
    $aNewArr['add_number'] = date("ymd");
    $oParseProductsToDBF = new \communicator\sources\parseProductsToDBF($aNewArr, __DIR__.'/files_DBF/');
    echo $oParseProductsToDBF->doParse('', true);
  }
}
global $pDbMgr;
$oGetNewProducts = new generateNewProductsDBF($pDbMgr);