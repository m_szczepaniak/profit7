SELECT 
  P.id,
  P.name, 
  P.name2, 

    (SELECT CONCAT(name, " ;; ", id)
    FROM products_publishers AS PP 
    WHERE PP.id = P.publisher_id 
    LIMIT 1) AS publisher,

    (SELECT CONCAT(name, " ;; ", id)
    FROM products_series AS PS
    JOIN products_to_series AS PTS
      ON PTS.series_id = PS.id
    WHERE PTS.product_id = P.id
    LIMIT 1) AS series,

  (
    SELECT GROUP_CONCAT(CONCAT(IFNULL(name, ""), " ", IFNULL(surname, "")) SEPARATOR " ;; ")
    FROM products_authors AS PA
    JOIN products_to_authors AS PTA
      ON PTA.author_id = PA.id
    WHERE PTA.product_id = P.id
    LIMIT 1
  ) AS authors,
  (
    SELECT binding
    FROM products_bindings AS PB
    WHERE PB.id = P.binding
    LIMIT 1
  ) AS binding,
  (
    SELECT dimension
    FROM products_dimensions AS PB
    WHERE PB.id = P.dimension
    LIMIT 1
  ) AS dimension,
  (
    SELECT language
    FROM products_languages AS PB
    WHERE PB.id = P.language
    LIMIT 1
  ) AS language,
  P.price_netto,
  P.price_brutto,
  P.vat,
  P.pkwiu,
  P.weight,
  P.isbn_plain,
  P.isbn_13,
  P.isbn_10,
  IF (P.ean_13 <> '' AND P.ean_13 IS NOT NULL, P.ean_13, IF(LENGTH(P.isbn_plain) = 10, CONCAT('978', P.isbn_plain), P.isbn_plain)) AS ean_13,
  P.publication_year,
  P.pages,
  P.volumes,
  P.volume_nr,
  P.edition,
  P.legal_status_date,
  P.type
  
  FROM products AS P
  WHERE 
    P.prod_status = "1" AND 
    P.published = "1" AND
    P.packet = "0" AND 
    P.shipment_time = "0";