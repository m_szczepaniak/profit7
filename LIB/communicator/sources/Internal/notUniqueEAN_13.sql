UPDATE products_test20150305
SET ean_13 = NULL
WHERE id IN (
  SELECT id FROM (
    SELECT P1.id
    FROM products AS P1
    JOIN products AS P2
    ON (P1.id <> P2.id) AND
    (P1.ean_13 = P2.isbn_plain)
     WHERE 
    P1.isbn_plain <> ''
  ) AS TMP
);


UPDATE products_test20150305
SET isbn_13 = NULL
WHERE id IN (
  SELECT id FROM (
    SELECT P1.id
    FROM products_test20150305 AS P1
    JOIN products_test20150305 AS P2
    ON (P1.id <> P2.id) AND
    (P1.isbn_13 = P2.isbn_plain)
     WHERE 
    P1.isbn_plain <> ''
  ) AS TMP
);


UPDATE products_test20150305
SET ean_13 = NULL
WHERE id IN (
  SELECT id FROM (
    SELECT P1.id
    FROM products_test20150305 AS P1
    JOIN products_test20150305 AS P2
    ON (P1.id <> P2.id) AND
    (P1.ean_13 = P2.isbn_13)
     WHERE 
    P1.isbn_plain <> ''
  ) AS TMP
);


ALTER TABLE  `products_test20150305` ADD  `streamsoft_indeks` VARCHAR( 20 ) NULL DEFAULT NULL,
ADD UNIQUE (
`streamsoft_indeks`
);

UPDATE products_test20150305
SET streamsoft_indeks = CASE WHEN ean_13 IS NOT NULL THEN ean_13
                  WHEN isbn_13 IS NOT NULL THEN isbn_13
                  WHEN isbn_plain IS NOT NULL THEN isbn_plain
END
WHERE streamsoft_indeks IS NULL
AND isbn_plain <> '0000000000000'
AND isbn_plain <> '';

/*
UPDATE products_test20150305
SET ean_13 = NULL
WHERE ean_13 = isbn_plain;

UPDATE products_test20150305
SET isbn_13 = NULL
WHERE isbn_13 = isbn_plain;

*/
UPDATE products_test20150305
SET ean_13 = CASE WHEN isbn_13 IS NOT NULL THEN isbn_13
                  WHEN isbn_plain IS NOT NULL THEN isbn_plain
                  ELSE ean_13
END
WHERE ean_13 IS NULL;




trig_profit_products_bef_ins
BEGIN
  
  IF (NEW.prod_status = '0' OR NEW.prod_status = '2') THEN
   SET NEW.last_change_unavailable = CURDATE();
  END IF;
  
  SET NEW.streamsoft_indeks = CASE WHEN NEW.ean_13 IS NOT NULL THEN NEW.ean_13
                  WHEN NEW.isbn_13 IS NOT NULL THEN NEW.isbn_13
                  WHEN NEW.isbn_plain IS NOT NULL AND NEW.isbn_plain <> '' AND NEW.isbn_plain <> '0000000000000' THEN NEW.isbn_plain
  END;
END










-- zgrupowane
SELECT * , GROUP_CONCAT( id SEPARATOR  ', ' ), GROUP_CONCAT( isbn_plain SEPARATOR  ', ' ), GROUP_CONCAT( prod_status SEPARATOR  ', ' )
FROM  `products_test20150305` 
GROUP BY ean_13
HAVING COUNT( id ) >1





SELECT P1.*, P2.*
FROM products_test20150305 AS P1
JOIN products_test20150305 AS P2
ON (P1.id <> P2.id) AND
(P1.ean_13 IS NOT NULL  AND P1.ean_13 <> '' AND P1.ean_13 = P2.isbn_10)
 WHERE 
P1.isbn_plain <> '' AND
P1.prod_status = '1';



SELECT COUNT(P1.id)
FROM products_test20150305 AS P1
JOIN products_test20150305 AS P2
ON (P1.id <> P2.id) AND
( P1.isbn_plain = P2.ean_13 OR
P1.isbn_plain = P2.isbn_10 OR
P1.isbn_plain = P2.isbn_13 OR
(P1.isbn_10 IS NOT NULL AND P1.isbn_10 <> '' AND  P1.isbn_10 = P2.isbn_13  ) OR
(P1.isbn_10 IS NOT NULL AND P1.isbn_10 <> '' AND P1.isbn_10 = P2.ean_13 ) OR
(P1.ean_13 IS NOT NULL AND P1.ean_13 <> '' AND P1.ean_13 = P2.isbn_13) OR
(P1.ean_13 IS NOT NULL  AND P1.ean_13 <> '' AND P1.ean_13 = P2.isbn_10) OR
(P1.isbn_13 IS NOT NULL AND P1.isbn_13 <> '' AND P1.isbn_13 = P2.isbn_10) OR
(P1.isbn_13 IS NOT NULL AND P1.isbn_13 <> ''  AND P1.isbn_13 = P2.ean_13)
) WHERE 
P1.isbn_plain <> '';







INSERT INTO products_test20150305
SELECT id, name, prod_status, shipment_time, isbn_plain, isbn_10, isbn_13 , ean_13
FROM products;