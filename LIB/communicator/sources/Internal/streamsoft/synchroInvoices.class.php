<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use LIB\communicator\sources\Internal\streamsoft\WS\wsNagl;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPozArray;
use LIB\Supplies\StockSuppliceService;
use orders_alert\OrdersAlert;

/**
 * Description of synchroInvices
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class synchroInvoices extends ProfitServerImplService {

  /**
   *
   * @var bool
   */
  private $bTestMode;

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var getInvoice
   */
  private $oGI;
  
  /**
   *
   * @var integer
   */
  private $sesja;

  /**
   *
   * @var ProfitServerImplService
   */
  private $WS;
  
  /**
   *
   * @var OrdersAlert
   */
  private $oAlert;

  /**
   * @var
   */
  private $dataProvider;

  /**
   * 
   * @param DatabaseManager $pDbMgr
   * @param bool $bTestMode
   */
  public function __construct(DatabaseManager $pDbMgr, $bTestMode = false) {
    global $aConfig;
    
    $this->pDbMgr = $pDbMgr;
    $this->oGI = new getInvoice($pDbMgr);
    $this->bTestMode = $bTestMode;
    $this->oAlert = new OrdersAlert();
    $this->pDbMgr = $pDbMgr;
    $this->stockSuppliceService = new StockSuppliceService($this->pDbMgr);
    if ($aConfig['common']['status'] == 'development') {
      parent::__construct(array('force_socket_timeout' => '1600'), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
      $this->WS = new ProfitServerImplService(array('force_socket_timeout' => '1600'), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
    } else {
      parent::__construct(array('force_socket_timeout' => '1600'));
      $this->WS = new ProfitServerImplService(array('force_socket_timeout' => '1600'));
    }
    $this->sesja = $this->WS->utworzSesje('webservice', 'profwebservice')->getValue();
  }

  /**
   *
   */
  public function doSynchronizeAllInvoices($bSmall = true) {
    $bIsErr = false;
      $i = 0;

      echo 'Iteracja '.$i;
      if ($bSmall == true) {
          while (null != ($aOrdersInvoices = $this->oGI->getAllInvoicesSmall())) {
              $i++;
              echo 'Iteracja '.$i;
              if (!empty($aOrdersInvoices)) {
                  foreach ($aOrdersInvoices as $iOrderId) {
                      $this->doSynchronizeSingleInvoice($iOrderId);
                  }
              }
          }
      } else {
          while (null != ($aOrdersInvoices = $this->oGI->getAllInvoicesBig())) {
              $i++;
              echo 'Iteracja '.$i;
              if (!empty($aOrdersInvoices)) {
                  foreach ($aOrdersInvoices as $iOrderId) {
                      $this->doSynchronizeSingleInvoice($iOrderId);
                  }
              }
          }
      }



    return !$bIsErr;
  }

  /**
   * 
   * @param int $iOrderId
   * @return boolean
   */
  public function doSynchronizeSingleInvoice($iOrderId) {
    $bIsErr = false;
    
    $aInvoices = $this->oGI->getOrderInvoices($iOrderId);

    if (!empty($aInvoices) && is_array($aInvoices)) {
      foreach ($aInvoices as $aInvoice) {
        $pozycje = $aInvoice['poz'];
        $naglowek = $aInvoice['nagl'];
        $kod = $this->getKodUrzZewn($this->oGI->oInvoice->website_id);
        try {
          if ($this->addDokument($naglowek, $pozycje, $kod) <= 0) {
            $bIsErr = true;
          }
        } catch (Exception $ex) {
            if (false === stristr($ex->getMessage(), 'Error Fetching http headers')) {
                print_r(' Error Fetching http headers ');
                $bIsErr = true;
            }
          $this->oAlert->createAlert($this->oGI->oInvoice->id, 'err-inv-add-product', 'Błąd FV : '.print_r(nl2br($ex->getMessage()), true));
          print_r($ex->getMessage());
        }
      }
      $this->oAlert->removeDuplicates();
      $this->oAlert->addAlerts();
    } else {
        print_r(' PUSTE DANE FV ?????? ');
      $bIsErr = true;
    }
    
    if ($bIsErr === false) {
      if ($this->bTestMode === false) {
        $this->markOrderAsExported($iOrderId);
      }
      return true;
    } else {
        echo 'błąd danych id - '.$iOrderId.' ';
        return false;
    }
  }
  
  /**
   * 
   * @param int $iWebsiteId
   * @return string
   */
  private function getKodUrzZewn($iWebsiteId) {
    
    switch ($iWebsiteId) {
      case '1':
        return '02';
      case '3':
        return '03';
      case '2':
        return '04';
      case '4':
        return '10';
      case '5':
        return '11';
      case '8':
        return '15';
    }
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return boolean
   */
  private function markOrderAsExported($iOrderId) {
    $aValues = array(
        'erp_send_ouz' => "NOW()"
    );

//    if ($this->pDbMgr->Update('profit24', 'products_stock_reservations', ['move_to_erp' => '1'], ' order_id = '.$iOrderId) === false) {
//      return false;
//    }

    if ($this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOrderId) === false) {
      return false;
    } else {
      return true;
    }
  }
  
  
//  private function createInvoiceByCMS(stdClass $oInvoiceWithItems) {
//    
//    $cechy = new wsCechaDok($cecha1, $cecha2, $cecha3, $cecha4, $cecha5);
//    $naglowek = new wsNagl($kontrahent, $skrDefDok, $numer_dokumentu, $data_dokumentu, $sposob_dostawy, $sposob_platnosci, $termin_platnosci, $uwagi, $cechy);
//    $pozycje[] = new wsPoz($indeks, $ilosc, $cena_sprzedazy_netto, $cena_sprzedazy_brutto);
//    
//    $this->addDokument($naglowek, $pozycje, $kod_urzzew);
//    //$oInvoiceWithItems->items
//    //$oWebService = new ProfitServerImplService();
//    //$oWebService->utworzDokument($sesja, $naglowek, $pozycje, $kod_urzzew);
//  }
//  
  
  /**
   * 
   * @param wsNagl $naglowek
   * @param wsPozArray $pozycje
   * @param string $kod_urzzew
   * @return integer
   */
  private function addDokument(wsNagl $naglowek, wsPozArray $pozycje, $kod_urzzew) {
      var_dump($naglowek);
      var_dump($pozycje);
      var_dump($kod_urzzew);
    $oDok = $this->utworzDokument($this->sesja, $naglowek, $pozycje, $kod_urzzew);

      var_dump($oDok);
    if ($oDok->getErrorCode() != 0) {
      throw new Exception($oDok->getErrorMessage().print_r($naglowek, true).print_r($pozycje, true), $oDok->getErrorCode());
    }
    return $oDok->getValue();
  }
}
