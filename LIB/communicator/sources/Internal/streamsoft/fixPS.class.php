<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 17.08.17
 * Time: 10:54
 */

echo "\r\n".date('Y-m-d H:i:s')."\r\n";

use LIB\communicator\sources\Internal\streamsoft\validateInvoice;

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-07
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal/streamsoft';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');

global $pDbMgr;

// pobieramy produkty z różna ilością rezerwacji

$sql = "
SELECT PSS.id, PSS.product_id, (reservation - reservation_to_reduce) as reservation, 
    (
        SELECT SUM( reservated_quantity ) 
        FROM products_stock_supplies_to_reservations AS PSSTR
        WHERE PSSTR.products_stock_supplies_id = PSS.id
        GROUP BY products_stock_supplies_id
    ) as sum,
    reservation_erp,
    S.column_prefix_products_stock
FROM `products_stock_supplies` AS PSS
JOIN sources AS S
  ON PSS.source = S.id
WHERE 

(reservation - reservation_to_reduce) <> 
( 
    SELECT SUM( reservated_quantity ) 
    FROM products_stock_supplies_to_reservations AS PSSTR
    WHERE PSSTR.products_stock_supplies_id = PSS.id
    GROUP BY products_stock_supplies_id 
)
AND reservation_to_reduce = 0
ORDER BY PSS.id DESC
";

$productStockSupplies = $pDbMgr->GetAll('profit24', $sql);

$productStock = [
    'profit_g_reservations' => 0,
    'profit_e_reservations' => 0,
    'profit_j_reservations' => 0
];

foreach ($productStockSupplies as $stockSupply) {
    if ($stockSupply['sum'] == $stockSupply['reservation_erp']) {
        // zgadza nam się ilość rezerwacji w tabelce relacyjnej i streamsoft
        // - wynika z tego że trzeba naprostować product_stock i product_stock_supplies
        // UWAGA wykluczyliśmy reservation_to_reduce - dzięki czemu nie musimy przewidywać spadających rezerwacji
        $correctReservationProductSupplies = $stockSupply['sum'];
        $correctReservationProductStock = $stockSupply['reservation'] - $correctReservationProductSupplies;
        if ($correctReservationProductStock != 0) {

            $valuesSupplies = [
                'reservation' => $correctReservationProductSupplies
            ];
            $pDbMgr->Update('profit24', 'products_stock_supplies', $valuesSupplies, ' id = ' . $stockSupply['id'] . ' LIMIT 1');

            $sSql = 'SELECT * FROM products_stock WHERE id = ' . $stockSupply['product_id'];
            $productStock = $pDbMgr->GetRow('profit24', $sSql);

            $prefixCol = $stockSupply['column_prefix_products_stock'];
            // wykminić prefix
            $currentProductStockReservation = $productStock[$prefixCol . '_reservations'] - $correctReservationProductStock;
            $valuesStock = [
                $prefixCol . '_reservations' => $currentProductStockReservation,
                $prefixCol . '_status' => $productStock['profit_g_act_stock'] - $currentProductStockReservation
            ];

            echo "\n\n\nDO POPRAWY:\n";
            print_r($stockSupply);
            print_r($valuesSupplies);
            print_r($valuesStock);
            $pDbMgr->Update('profit24', 'products_stock', $valuesStock, ' id = ' . $stockSupply['product_id'] . ' LIMIT 1');

        }

    }

}