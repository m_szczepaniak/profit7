<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-23 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\communicator\sources\Internal\streamsoft;

use Exception;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use LIB\communicator\sources\Internal\streamsoft\WS\stringArray;
use LIB\communicator\sources\Internal\streamsoft\WS\wsIntegerResult;
use LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka;
use LIB\communicator\sources\Internal\streamsoft\WS\wsKartotekaArray;

include_once('WS/autoload.php');
/**
 * Description of streamsoft
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class manageProduct extends ProfitServerImplService {
  
  /**
   *
   * @var void 
   */
  private $sesja;
  
  /**
   *
   * @var wsKartoteka
   */
  private $oProduct;
  
  public function __construct($sesja = NULL) {
    global $aConfig;
    
    if ($aConfig['common']['status'] == 'development') {
      parent::__construct(array(), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
    } else {
      parent::__construct();
    }
    
    if ($sesja === NULL || empty($sesja)) {
      $this->sesja = $this->utworzSesje('webservice', 'profwebservice')->getValue();
    } else {
      $this->sesja = $sesja;
    }
  }
  
  /**
   * 
   * @param wsKartoteka $oKartoteka
   * @return integer
   */
  public function addProduct(wsKartoteka $oKartoteka) {

    $oWSKartoteka = $this->utworzKartoteke($this->sesja, $oKartoteka);
    if ($oWSKartoteka->getErrorCode() != 0) {

      var_dump($oWSKartoteka->getErrorMessage());

      throw new Exception(nl2br(print_r($oKartoteka, true).$oWSKartoteka->getErrorMessage()), $oWSKartoteka->getErrorCode());
    }
    return $oWSKartoteka->getValue();
  }
  
  /**
   * 
   * @param wsKartotekaArray $oWieleKartotek
   * @return integer
   */
  public function addMultiplyProducts(wsKartotekaArray $oWieleKartotek) {
//    global $oTimer;
//    $oTimer->setMarker('Start dodajWieleKartotek');
    $oWSKartoteka = $this->dodajWieleKartotek($this->sesja, $oWieleKartotek);
//    $oTimer->setMarker('Stop dodajWieleKartotek');
    if ($oWSKartoteka->getErrorCode() != 0) {
      throw new Exception(nl2br(print_r($oWieleKartotek, true).$oWSKartoteka->getErrorMessage()), $oWSKartoteka->getErrorCode());
    }
    return $oWSKartoteka->getValue();
  }
  
  /**
   * 
   * @param string $sStreamsoft_indeks
   * @return wsKartoteka
   */
  public function checkProduct($sStreamsoft_indeks) {
    $oWSKartoteka = $this->sprawdzKartoteke($this->sesja, $sStreamsoft_indeks);
    if ($oWSKartoteka->getErrorCode() == -1) {
      return false;
    } elseif ($oWSKartoteka->getErrorCode() != 0) {
      throw new Exception($oWSKartoteka->getErrorMessage(), $oWSKartoteka->getErrorCode());
    }
    return $oWSKartoteka->getValue();
  }
  
  /**
   * @var $return wsStringTableResult
   * @param array $aProductsStraemsoftIds
   * @return array
   */
  public function groupCheckProducts(array $aProductsStraemsoftIds) {
//    global $oTimer;
    
    $oArray = new stringArray();
    $oArray->setItem($aProductsStraemsoftIds);
    try {
      
//      $oTimer->setMarker('Start sprawdzGrupeKartotek');
      $result = $this->sprawdzGrupeKartotek($this->sesja, $oArray);
//      $oTimer->setMarker('Stop sprawdzGrupeKartotek');
      
      if ($result->getErrorCode() != 0) {
        throw new Exception($result->getErrorMessage(), $result->getErrorCode());
      } else {
        return $result->getValue();
      }
    } catch (Exception $ex) {
      throw new Exception($ex->getMessage());
    }
  }
  
  /**
   * 
   * @param string $sStreamsoft_indeks
   * @return wsKartoteka
   */
  public function updateProduct($sStreamsoft_indeks) {
    $oProduct = $this->checkProduct($sStreamsoft_indeks);
    
    if (!empty($oProduct)) {
      $this->oProduct = $oProduct;
      return $this->oProduct;
    } else {
      throw new Exception(_('Brak produktu o podanym EAN'));
    }
  }
  
  /**
   * 
   * @param wsKartoteka $oKartoteka
   */
  public function setProductToUpdate(wsKartoteka $oKartoteka) {
    $this->oProduct = $oKartoteka;
  }
  
  /**
   * 
   * @return wsIntegerResult
   */
  public function saveUpdate() {
    $oReturnInteger = $this->aktualizujKartoteke($this->sesja, $this->oProduct);
    if ($oReturnInteger->getErrorCode() != 0) {
      throw new Exception($oReturnInteger->getErrorMessage(), $oReturnInteger->getErrorCode());
    }
    
    return $oReturnInteger;
  }
}