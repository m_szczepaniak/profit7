<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\communicator\sources\Internal\streamsoft;

use Common;
use DatabaseManager;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use LIB\communicator\sources\Internal\streamsoft\WS\wsZestVat;
use LIB\Supplies\StockSuppliceService;
use OrderItemRecount;
use orders_alert\OrdersAlert;

/**
 * Description of validateInvoice
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class validateInvoice extends ProfitServerImplService {

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var integer
   */
  private $sesja;
  
  /**
   *
   * @var OrderItemRecount
   */
  private $oOrderItemRecount;
  
  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function __construct(DatabaseManager $pDbMgr) {
    global $aConfig;
    
    $this->pDbMgr = $pDbMgr;
    if ($aConfig['common']['status'] == 'development') {
      //parent::__construct(array(), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
      parent::__construct(array(), 'http://83.144.105.248:8080/WebServiceProTest/ServerProfit?wsdl');
    } else {
      parent::__construct();
    }
    
    $this->sesja = $this->utworzSesje('webservice', 'profwebservice')->getValue();
    
		include_once('OrderItemRecount.class.php');
		$this->oOrderItemRecount = new OrderItemRecount();
    $this->stockSuppliceService = new StockSuppliceService($this->pDbMgr);
  }
  
  /**
 *
 * @return array
 */
  private function getOrdersSynchroToday() {

    $sSql = 'SELECT id, order_number, invoice_id, invoice2_id,
                value_netto, value_brutto, transport_cost, transport_vat,
                to_pay, second_invoice
             FROM orders AS O
             WHERE
              O.invoice_id IS NOT NULL
              AND O.order_status IN("4", "3")
              AND O.erp_export IS NULL
              AND O.erp_send_ouz IS NOT NULL
              AND O.order_date > "2015-09-25"
             ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

  /**
   *
   * @return array
   */
  private function getOrdersSynchroByDate($dDate) {

    $sSql = 'SELECT id, order_number, invoice_id, invoice2_id,
                value_netto, value_brutto, transport_cost, transport_vat,
                to_pay, second_invoice
             FROM orders AS O
             WHERE
              O.invoice_id IS NOT NULL
              AND O.order_status IN("4", "3")
              AND O.erp_send_ouz IS NOT NULL
              AND DATE(O.invoice_date) = "'.$dDate.'"
             ';
    echo $sSql;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param int $iIdentCMS
   * @var $wsZestVatResult WS\wsZestVatResult
   * @var @value WS\wsZestVat
   * @throws Exception
   * @return wsZestVat
   */
  private function getZestWS($iOrderId, $iIdentCMS) {
    
    $wsZestVatResult = $this->pobierzZestVat($this->sesja, $iIdentCMS);
    if ($wsZestVatResult->getErrorCode() !== 0) {
//      $this->setNotExportedErp($iOrderId);
      $oAlerts = new OrdersAlert();
      $oAlerts->createAlert($iOrderId, 'auto-validate-invoices', $wsZestVatResult->getErrorMessage());
      $oAlerts->removeDuplicates();
      $oAlerts->addAlerts();
      throw new Exception($wsZestVatResult->getErrorMessage());
    } else {
      return $wsZestVatResult->getValue();
    }
  }
  
  /**
   * 
   * @param int $iOrderId
   */
  private function setExportedErp($iOrderId) {
    $this->pDbMgr->BeginTransaction('profit24');
    try {
      $this->stockSuppliceService->updateReservationsToReduceByOrderId($iOrderId);
    } catch(\Exception $e) {
      $this->pDbMgr->RollbackTransaction('profit24');
      $this->oAlert->createAlert($this->oGI->oInvoice->id, 'err-inv-add-product', 'Blad podczas aktualizacji reservation_to_reduce '." \n".$e->getMessage(), true);
      return false;
    }
    
    if ($this->pDbMgr->Update('profit24', 'products_stock_reservations', ['move_to_erp' => '1'], ' order_id = '.$iOrderId) === false) {
      $this->pDbMgr->RollbackTransaction('profit24');
      return false;
    }
    
    $aValues = [
        'erp_export' => 'NOW()'
    ];
    if ($this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOrderId) === false) {
      $this->pDbMgr->RollbackTransaction('profit24');
      return false;
    }
    $this->pDbMgr->CommitTransaction('profit24');
  }
  
  /**
   * 
   * @return bool
   */
  public function proceedValidateInvoicesAddAlert() {
    $bIsErr = false;
    $aOrders = $this->getOrdersSynchroToday();
    
    $oAlerts = new OrdersAlert();
    foreach ($aOrders as $aOrder) {
      try {
        $this->proceedValidateSingleInvoice($aOrder);
      } catch (Exception $ex) {
        $bIsErr = true;
        $oAlerts->createAlert($aOrder['id'], 'auto-export-invoices', $ex->getMessage());
      }
    }
    $oAlerts->removeDuplicates();
    $oAlerts->addAlerts();
    return !$bIsErr;
  }

  public function proceedValidateInvoicesByDay($dInvoiceDate) {
    $bIsErr = false;
    $aOrders = $this->getOrdersSynchroByDate($dInvoiceDate);

    foreach ($aOrders as $aOrder) {
      try {
        $this->proceedValidateSingleInvoice($aOrder);
      } catch (Exception $ex) {
        print_r('rózne wartości '.$ex->getMessage());
        echo "\r\n";
      }
    }

  }
  
  /**
   * 
   * @param array $aOrder
   */
  public function proceedValidateSingleInvoice(array $aOrder) {
    $bIsErr = false;
    
    if ($aOrder['second_invoice'] == '1' && $aOrder['invoice2_id'] != '') {
      $wsZestVat = $this->getZestWS($aOrder['id'], $aOrder['invoice2_id']);
      if ($this->validateInvoice($wsZestVat, $aOrder, TRUE) === FALSE) {
        $bIsErr = true;
      }
    }
    
    $wsZestVat = $this->getZestWS($aOrder['id'], $aOrder['invoice_id']);
    if ($this->validateInvoice($wsZestVat, $aOrder, FALSE) === FALSE) {
      $bIsErr = true;
    }
    
    return $bIsErr === TRUE ? FALSE : TRUE;
  }

  /**
   * @param wsZestVat $wsZestVat
   * @param array $aOrder
   * @param $bSecondInvoice
   * @return bool
   * @throws Exception
   */
  private function validateInvoice(wsZestVat $wsZestVat, array $aOrder, $bSecondInvoice) {
    
    $aOrderData = $this->oOrderItemRecount->getOrderPricesDetail($aOrder['id'], $bSecondInvoice, '', true);
    $fPaidAmount = Common::formatPrice2($aOrderData['order']['to_pay']);
    if (floatval($wsZestVat->getWart_brutto()) === floatval($fPaidAmount)) {
      $this->setExportedErp($aOrder['id']);
      return true;
    } else {
      if ($aOrder['second_invoice'] == '1') {
        $this->setExportedErp($aOrder['id']);
        return true;
      }
      $sMsg = sprintf(_('Rożne wartości FV brutto %s - %s'), 
//              $wsZestVat->getWart_netto(), $aOrder['value_netto'], 
              $wsZestVat->getWart_brutto(), $fPaidAmount
              );
//      file_put_contents('log_err_1.log', $aOrder['id']."\n".$sMsg."\n"."\n", FILE_APPEND);
      throw new Exception($sMsg);
    }
  }
}
