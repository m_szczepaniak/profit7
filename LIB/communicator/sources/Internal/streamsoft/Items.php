<?php

namespace LIB\communicator\sources\Internal\streamsoft;

use DateTime;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPoz;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPozArray;

class Items
{
    protected $dataProvider;

    function __construct()
    {
        global $pDbMgr;
        $this->dataProvider = new DataProvider($pDbMgr);
    }


    public function prepareItems($aOSHItems, $pDbMgr, $session, $itemSoruce = false)
    {
        $pozycje = new wsPozArray();
        $aItems = array();
        $aStreamsoftIDs = [];

        $oSynchronizeProduct = new synchroProduct($pDbMgr, $session);

        foreach ($aOSHItems as $aItem) {
            if ($aItem['supplies'] != '') {

            } else {
                if ($aItem['in_confirmed_quantity'] > 0 && false === $this->dataProvider->checkProductInStreamsoft($aItem['product_id'])) {
                    $aStreamsoftIDs[] = $aItem['streamsoft_indeks'];
                }
            }
        }
        if (!empty($aStreamsoftIDs)) {
            $oSynchronizeProduct->groupCheckProductsAndAddNew($aStreamsoftIDs);
        }

        foreach ($aOSHItems as $aItem) {
            if ($aItem['in_confirmed_quantity'] > 0) {

                $aItems[] = new wsPoz(
                    strtoupper($aItem['streamsoft_indeks']),
                    $aItem['in_confirmed_quantity'],
                    false,
                    $aItem['price_netto'],
                    ($aItem['price_netto'] > 0.00 ? true : false),
                    0,
                    $itemSoruce === true ? ProfitServerImplService::getMagazineCodeBySource($aItem['source_id']) : '01',
                    '',
                    $itemSoruce === true ? ProfitServerImplService::getMagazineCodeBySource($aItem['source_id']) : $aItem['item_source_id'],
                    '',
                    '',
                    new DateTime(),
                    '',
                    '',
                    ''
                );
            }
        }

        $pozycje->setItem($aItems);

        return $pozycje;
    }
}