<?php

use LIB\communicator\sources\Internal\streamsoft\synchroInvoices;

// zabezpieczenie przed wielokrotnym odpaleniem skryptu
//$sCMD = "ps -Af|grep ".basename(__FILE__);
//$d = exec($sCMD, $op);
//if (count($op) > 3) {
//    echo 'Już działam...';
//    die;
//}

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-07 
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal/streamsoft';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');



$bSmall = true;
global $pDbMgr;
$oValidateInvoice = new synchroInvoices($pDbMgr, false);
$oValidateInvoice->doSynchronizeAllInvoices($bSmall);
echo 'koniec';