<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use stdClass;

/**
 * Description of getInvoiceCMS
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class getInvoiceCMS {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  public function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }

  /**
   * 
   * @return array
   */
  public function getInvoiceItems($iOrderId, $bIsFirstInvoice) {
    $aOrderItems = array();
    $aItems = $this->getInvoiceItemsDB($iOrderId, $bIsFirstInvoice);
    
    foreach ($aItems as $aItem) {
      $aOrderItems[] = (object)$aItem;
    }
    return $aOrderItems;
  }
  
  /**
   * @TODO uwaga sprawdzić jak to się zachowuje jak są załączniki item_type i pakiety
   * 
   * @param type $iOrderId
   * @return array
   */
  private function getInvoiceItemsDB($iOrderId, $bIsFirstInvoice) {
    
    $sSql = 'SELECT OI.id, OI.name, OI.quantity, OI.discount, OI.source, 
      (SELECT PSR.streamsoft_lp FROM products_stock_reservations AS PSR WHERE PSR.orders_items_id = OI.id ORDER BY PSR.id DESC LIMIT 1) AS streamsoft_lp,
      OI.promo_price_netto, OI.promo_price_brutto, OI.vat, P.streamsoft_indeks,
      OI.product_id, OI.price_netto, OI.price_brutto, OI.quantity, OI.value_brutto,
      OI.item_type, OI.parent_id, OI.attachment_id
             FROM orders_items AS OI
             LEFT JOIN products AS P
              ON P.id = OI.product_id
             WHERE 
                OI.order_id = '.$iOrderId .' AND 
                OI.deleted = "0" AND
                OI.second_invoice = "' . ($bIsFirstInvoice === TRUE ? "0" : "1") . '" AND
                OI.packet = "0"
     GROUP BY OI.id DESC
                ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @return array
   */
  public function getAllInvoices() {
    $sSql = 'SELECT O.id, O.order_date
            FROM orders AS O
            WHERE O.erp_send_ouz IS NULL 
              AND O.invoice_id IS NOT NULL 
              AND O.order_status IN("4", "3")
              AND DATE(O.invoice_date) >= "2015-05-04"
            ORDER BY O.invoice_date ASC';
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }

    /**
     *
     * @return array
     */
    public function getAllInvoicesBig() {
        $sSql = '
            SELECT O.id, O.order_date, O.order_number,
            IFNULL((SELECT SUM(OI.quantity) FROM orders_items AS OI WHERE OI.order_id = O.id AND OI.deleted = "0" AND OI.source = "51"), 0) as D
            FROM orders AS O
            WHERE O.erp_send_ouz IS NULL
            AND O.invoice_id IS NOT NULL
            AND O.order_status IN("4", "3")
            AND DATE(O.invoice_date) >= "2015-05-04"
            HAVING D > 2
            ORDER BY O.invoice_date ASC
    ';
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

    /**
     *
     * @return array
     */
    public function getAllInvoicesSmall() {
        $sSql = '
            SELECT O.id, O.order_date, O.order_number,
            IFNULL((SELECT SUM(OI.quantity) FROM orders_items AS OI WHERE OI.order_id = O.id AND OI.deleted = "0" AND OI.source = "51"), 0) as D
            FROM orders AS O
            WHERE O.erp_send_ouz IS NULL
            AND O.invoice_id IS NOT NULL
            AND O.order_status IN("4", "3")
            AND DATE(O.invoice_date) >= "2015-05-04"
            HAVING D <= 2
            ORDER BY O.invoice_date ASC
    ';
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

  /**
   * 
   * @param int $iId
   * @return stdClass
   */
  public function getSingleInvoice($iId) {
    
    $sSql = 'SELECT O.website_id, O.id, O.order_number, O.order_number AS oryginal_order_number, O.to_pay, O.paid_amount, 
                    O.second_payment_enabled, O.payment_type, O.second_payment_type, 
                    O.invoice_date, O.invoice_date_pay, O.second_invoice, O.invoice_id, O.invoice2_id,
                    O.transport_id, O.transport_vat, O.transport_cost, TM.streamsoft_name, 
                    O.seller_streamsoft_id
             FROM orders AS O
             JOIN orders_transport_means AS TM
              ON TM.id = O.transport_id
             JOIN orders_payment_types AS OPT 
              ON O.payment_id = OPT.id
             WHERE O.id = "'.$iId.'" AND
                   O.erp_send_ouz IS NULL AND 
                   O.invoice_id IS NOT NULL AND
                   O.order_status IN("4", "3") ';
    $oInvoice = (object)$this->pDbMgr->GetRow('profit24', $sSql);
    $oInvoice->payu_to_pay = $this->getPaidAmountPayU($oInvoice);
    $oInvoice->postal_fee_to_pay = $this->getToPayPostalFee($oInvoice);
    return $oInvoice;
  }
  
  /**
   * 
   * @param stdClass $oOrder
   * @return float
   */
  public function getPaidAmountPayU($oOrder) {
    
    // zawsze jak mamy już FV, to PayU, coś powinno być opłacone
    if ($oOrder->paid_amount > 0) {
      if ($this->checkIsSelectedPaymentType($oOrder, 'platnoscipl') === true || 
          $this->checkIsSelectedPaymentType($oOrder, 'card') === true) {
        return $this->getPaidAmountPayUDB($oOrder->id, $oOrder->to_pay);
      }
    } else {
      return 0.00;
    }
  }
  
  /**
   * 
   * @param stdClass $oOrder
   * @retrun float|false
   */
  public function getToPayPostalFee($oOrder) {
    
    if ($oOrder->to_pay > $oOrder->paid_amount) {
      // mamy coś do opłacenia
      if ($this->checkIsSelectedPaymentType($oOrder, 'postal_fee')) {
        return ($oOrder->to_pay - $oOrder->paid_amount);
      } else {
        return false;
      }
    } else {
      return 0.00;
    }
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return float
   */
  private function getPaidAmountPayUDB($iOrderId, $fToPay) {
    
    $sSql = 'SELECT SUM(amount)/100
             FROM orders_platnoscipl_sessions 
             WHERE order_id = '.$iOrderId.' 
               AND status = "99"
             GROUP BY order_id';
    $fPayUPaidAmount = $this->pDbMgr->GetOne('profit24', $sSql);
    if ($fPayUPaidAmount > $fToPay) {
      return $fToPay;
    } else {
      return $fPayUPaidAmount;
    }
  }
  
  /**
   * 
   * @param stdClass $oOrder
   * @param string $sPType
   * @return boolean
   */
  private function checkIsSelectedPaymentType($oOrder, $sPType) {
    
    if ($oOrder->payment_type == $sPType) {
      return true;
    } 
    elseif ($oOrder->second_payment_enabled == '1' && $oOrder->second_payment_type == $sPType) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param bool $bIsFirstInvoice
   * @param bool $bIsInvoice14Days
   * @return stdClass
   */
  protected function getUserAddress($iOrderId, $bIsFirstInvoice, $bIsInvoice14Days) {
    
    $sSql = 'SELECT * 
             FROM orders_users_addresses AS OUA
             WHERE order_id = '.$iOrderId.' AND
                   address_type = "'.($bIsFirstInvoice === TRUE ? '0' : '2').'" 
                   ' . ($bIsInvoice14Days === true ? 'AND streamsoft_id IS NOT NULL AND streamsoft_id > 0 ' : '') .'
             LIMIT 1 ';
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
}
