<?php

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\getProduct;
use LIB\communicator\sources\Internal\streamsoft\manageProduct;
use LIB\communicator\sources\Internal\streamsoft\WS\wsKartotekaArray;
use LIB\communicator\sources\Internal\streamsoft\WS\wsStringTableResult;

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */

/**
 * Description of synchroProduct
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class synchroProduct {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var getProduct 
   */
  private $oGP;
  
  /**
   *
   * @var manageProduct 
   */
  private $oMP;
  
  /**
   *
   * @var sesja
   */
  private $sesja;
  
  /**
   * 
   * @param DatabaseManager $pDbMgr
   * @param sesja $sesja
   */
  public function __construct($pDbMgr, $sesja = NULL) {
    $this->pDbMgr = $pDbMgr;
    
    $this->sesja = $sesja;
    $this->oMP = new manageProduct($sesja);
    $this->oGP = new getProduct($this->pDbMgr);
  }
  
  /**
   * 
   * @param int $iProductId
   * @param bool $bForce
   * @param bool $bDontUpdate
   * @param bool $bDontInsert
   */
  public function trySynchronizeProduct($iProductId, $bForce = false, $bDontUpdate = FALSE, $bDontInsert = FALSE) {
    
    /*
    $oProduct = $this->oGP->getSingleProduct($iProductId);
    $oProductAttachment = $this->oGP->getProductAttachment();
    
    $this->oMP2 = new manageProduct();
    if ($oProductAttachment !== false && $oProductAttachment->getIndeks_kartoteki_powiazanej() != $oProduct->getCechy()->getId_cms()) {
      $oProductAttachment->setIndeks_kartoteki_powiazanej($oProduct->getstreamsoft_indeks());
      nl2br(var_export($oProductAttachment));
      $this->oMP2->setProductToUpdate($oProductAttachment);
    }

    */
    $oProduct = $this->oGP->getSingleProduct($iProductId, $bForce);
    if ($oProduct !== false) {
      $oProductAttachment = $this->oGP->getProductAttachmentN();
      if ($oProductAttachment !== false) {
        if ($this->checkStreamsoftProductExists($this->oMP, $oProductAttachment->getIndeks()) === true) {
          $this->oMP->setProductToUpdate($oProductAttachment);
          $this->oMP->saveUpdate();
        } else {
          if ($bDontInsert === FALSE) {
            $this->oMP->addProduct($oProductAttachment);
          }
        }
        $oProduct = $this->oGP->assignAttachmentToProduct($oProduct, $oProductAttachment);
      }

      
      if ($this->checkStreamsoftProductExists($this->oMP, $oProduct->getIndeks()) === true) {
        if ($bDontUpdate === FALSE) {
          $this->oMP->setProductToUpdate($oProduct);
          $this->oMP->saveUpdate();
        }
      } else {
        if ($bDontInsert === FALSE) {
          $this->oMP->addProduct($oProduct);
        }
      }

      $this->oMP2 = new manageProduct($this->sesja);
      if ($oProductAttachment !== false && $oProductAttachment->getIndeks_kartoteki_powiazanej() != $oProduct->getCechy()->getId_cms()) {
        $oProductAttachment->setIndeks_kartoteki_powiazanej($oProduct->getIndeks());
        $this->oMP2->setProductToUpdate($oProductAttachment);
        $this->oMP2->saveUpdate();
      }
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param array $aNewProducts
   */
  private function prepareAddNewProducts($aNewProducts, $bForce) {
//    global $oTimer;
    
    $oMultiple = new wsKartotekaArray();
    $aToAdd = array();
    $aAttachmentToAdd = array();
    
//    $oTimer->setMarker('Start getMultipleProducts');
    foreach ($aNewProducts as $sProductsStreamsoftId) {
      
      $oProduct = $this->oGP->getMultipleProducts($sProductsStreamsoftId, $bForce);
      
      if ($oProduct !== false) {
        
        $aToAdd[] = $oProduct;
        $oProductAttachment = $this->oGP->getProductAttachmentN();
        if ($oProductAttachment !== false) {
          $aAttachmentToAdd[]['att'] = $oProductAttachment;
          $aAttachmentToAdd[]['prod'] = $oProduct;
          $aToAdd[] = $oProductAttachment;
        }
      }
    }
//    $oTimer->setMarker('Stop getMultipleProducts');
    
    if (!empty($aToAdd)) {
      $oMultiple->setItem($aToAdd);
      $this->oMP->addMultiplyProducts($oMultiple);
    }
    // załączniki
    foreach ($aAttachmentToAdd as $aoProductAtt) {
      $oProduct = $this->oGP->assignAttachmentToProduct($aoProductAtt['prod'], $aoProductAtt['att']);
      $this->oMP->setProductToUpdate($oProduct);
      $this->oMP->saveUpdate();
      $oProduct = $this->oGP->assignAttachmentToProduct($aoProductAtt['att'], $aoProductAtt['prod']);
      $this->oMP->setProductToUpdate($oProduct);
      $this->oMP->saveUpdate();
    }
  }

  /**
   * 
   * @param array $aProductsStraemsoftIds
   * @return wsStringTableResult
   */
  public function groupCheckProductsAndAddNew(array $aProductsStraemsoftIds) {
    try {
      $aNewProducts = $this->oMP->groupCheckProducts($aProductsStraemsoftIds);
      if (!empty($aNewProducts)) {
        $this->prepareAddNewProducts($aNewProducts, TRUE);
      }
    } catch (Exception $ex) {
      throw new Exception($ex->getMessage());
    }
  }
  
  /**
   * 
   * @return bool
   */
  public function synchroAllProducts() {
    $bIsErr = false;
    
    $aProductsIds = $this->getAllProducts();
    foreach ($aProductsIds as $iProductId) {
      try {
        if ($this->trySynchronizeProduct($iProductId, TRUE, FALSE, TRUE) === false) {
          $bIsErr = true;
        }
      } catch (Exception $ex) {
        echo $ex->getMessage();
        $bIsErr = true;
      }
    }
    
    return !$bIsErr;
  }
  
  /**
   * 
   * @return array
   */
  private function getAllProducts() {
    $sSql = 'SELECT PLU.product_id
            FROM products_last_update AS PLU
            JOIN products AS P
              ON P.id = PLU.product_id AND
                 P.published = "1" AND
                 P.packet = "0"
            WHERE 
              DATE_SUB(CURDATE(), INTERVAL 15 DAY) < DATE(last_update)
            GROUP BY PLU.product_id';
    // P.shipment_time = "0" AND
    // P.prod_status = "1" AND
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }
  
  /**
   *
   * @param manageProduct $oMP
   * @param string $sStreamsoft_indeks
   * @return boolean
   */
  private function checkStreamsoftProductExists(manageProduct $oMP, $sStreamsoft_indeks) {
    
    if ($sStreamsoft_indeks != '') {
      $oKProduct = $oMP->checkProduct($sStreamsoft_indeks);
      if (isset($oKProduct) && !empty($oKProduct)) {
        return true;
      } else {
        return false;
      }
    } else {
      throw new Exception(_('Pusty streamsoft_indeks'));
    }
  }
}
