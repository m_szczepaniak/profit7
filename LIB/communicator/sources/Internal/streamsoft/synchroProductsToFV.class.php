<?php

use LIB\communicator\sources\Internal\streamsoft\synchroProduct;

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-07 
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal/streamsoft';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');

global $pDbMgr;
$oSynchroProducts = new synchroProduct($pDbMgr);

$sSql = '
SELECT OI.product_id AS id
FROM `orders_send_history_items` AS OSHI
JOIN orders_send_history AS OSH
  ON OSH.id = OSHI.send_history_id 
  AND OSH.date_send > DATE_SUB( CURDATE( ) , INTERVAL 1 DAY )
JOIN orders_items AS OI 
  ON OI.id = OSHI.item_id AND OI.status <> "4"
GROUP BY OI.product_id
ORDER BY OSHI.id DESC
    ';
$aProductsIds = $pDbMgr->GetCol('profit24', $sSql);
echo count($aProductsIds);
$i = 0;
foreach ($aProductsIds as $iProductId) {
//  $sStr =  $iProductId.'-'.$i."\n";
//  file_put_contents('log_1.log', $sStr, FILE_APPEND);
  try {
    $oSynchroProducts->trySynchronizeProduct($iProductId, true, true);
  } catch (Exception $ex) {
//    file_put_contents('log_1.log', $ex->getMessage(), FILE_APPEND);
//    echo $ex->getMessage();
  }
  $i++;
}