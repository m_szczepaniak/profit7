<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\communicator\sources\Internal\streamsoft;

use Common;
use DatabaseManager;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use LIB\communicator\sources\Internal\streamsoft\WS\wsZestVat;
use LIB\EntityManager\Entites\StockLocation;
use LIB\Supplies\StockSuppliceService;
use omniaCMS\lib\Products\ProductsStockReservations;
use OrderItemRecount;
use orders_alert\OrdersAlert;
use statusSupplyUpdater;

/**
 * Description of validateInvoice
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class validateInvoice extends ProfitServerImplService {

    protected $dataProvider;

    /**
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var integer
   */
  private $sesja;
  
  /**
   *
   * @var OrderItemRecount
   */
  private $oOrderItemRecount;

    /**
     * @var ProductsStockReservations
     */
  private $productStockReservation;

  private $bIsFix = false;

  private $oStatusSupplyUpdater;

    private $stockLocation;

  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function __construct(DatabaseManager $pDbMgr) {
    global $aConfig;
    
    $this->pDbMgr = $pDbMgr;
    if ($aConfig['common']['status'] == 'development') {
      //parent::__construct(array(), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
      parent::__construct(array(), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
    } else {
      parent::__construct();
    }

    $this->stockLocation = new StockLocation($this->pDbMgr);
    $this->sesja = $this->utworzSesje('webservice', 'profwebservice')->getValue();
    
		include_once('OrderItemRecount.class.php');
		$this->oOrderItemRecount = new OrderItemRecount();
    $this->stockSuppliceService = new StockSuppliceService($this->pDbMgr);
    $this->productStockReservation = new ProductsStockReservations($this->pDbMgr);
    $this->dataProvider = new DataProvider($this->pDbMgr);

    include_once($_SERVER['DOCUMENT_ROOT'].'import/elements/internal/statusSupplyUpdater.class.php');
    $this->oStatusSupplyUpdater = new statusSupplyUpdater($pDbMgr, date("Y-m-d H:i:s"));
  }
  
  /**
 *
 * @return array
 */
  private function getOrdersSynchroToday() {

    $sSql = 'SELECT id, order_number, invoice_id, invoice2_id,
                value_netto, value_brutto, transport_cost, transport_vat,
                to_pay, second_invoice
             FROM orders AS O
             WHERE
              O.invoice_id IS NOT NULL
              AND O.order_status IN("4", "3")
              AND O.erp_export IS NULL
              AND O.erp_send_ouz IS NOT NULL
              AND O.order_date > "2015-09-25"
             ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }


    private function getOrdersSynchroByRealized() {

        $sSql = 'SELECT DISTINCT O.id, O.order_number, O.invoice_id, O.invoice2_id,
                    O.value_netto, O.value_brutto, O.transport_cost, O.transport_vat,
                    O.to_pay, O.second_invoice, O.erp_send_ouz, O.erp_export
                 FROM orders AS O
                 JOIN orders_items AS OI
                  ON OI.order_id = O.id
                 JOIN products_stock_reservations PSR
                    ON PSR.orders_items_id = OI.id
                 WHERE
                  O.invoice_id IS NOT NULL
                  AND O.erp_export IS NOT NULL
                  AND DATE_SUB(DATE(NOW()), INTERVAL 1 DAY) >  O.status_3_update
                 ';
        echo $sSql;
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


  /**
   *
   * @return array
   */
  private function getOrdersSynchroByDate($dDate) {

    $sSql = 'SELECT id, order_number, invoice_id, invoice2_id,
                value_netto, value_brutto, transport_cost, transport_vat,
                to_pay, second_invoice
             FROM orders AS O
             WHERE
              O.invoice_id IS NOT NULL
              AND O.order_status IN("4", "3")
              AND O.erp_send_ouz IS NOT NULL
              AND DATE(O.invoice_date) = "'.$dDate.'"
             ';
    echo $sSql;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param int $iIdentCMS
   * @var $wsZestVatResult WS\wsZestVatResult
   * @var @value WS\wsZestVat
   * @throws Exception
   * @return wsZestVat
   */
  private function getZestWS($iOrderId, $iIdentCMS, $bAddAlert = true) {
    
    $wsZestVatResult = $this->pobierzZestVat($this->sesja, $iIdentCMS);
    if ($wsZestVatResult->getErrorCode() !== 0) {
//      $this->setNotExportedErp($iOrderId);
        if (true === $bAddAlert) {
            $oAlerts = new OrdersAlert();
            $oAlerts->createAlert($iOrderId, 'auto-validate-invoices', $wsZestVatResult->getErrorMessage());
            $oAlerts->removeDuplicates();
            $oAlerts->addAlerts();
            throw new Exception($wsZestVatResult->getErrorMessage());
        } else {
            return false;
        }
    } else {
      return $wsZestVatResult->getValue();
    }
  }

    /**
     *
     * @param int $iOrderId
     * @param $aOrder
     * @return bool
     */
  private function setExportedErp($iOrderId, $aOrder) {
        $this->pDbMgr->BeginTransaction('profit24');
        if (false === $this->bIsFix) {
            try {
                $this->stockSuppliceService->updateReservationsToReduceByOrderId($iOrderId);
            } catch (\Exception $e) {
                $this->pDbMgr->RollbackTransaction('profit24');
                $this->oAlert->createAlert($this->oGI->oInvoice->id, 'err-inv-add-product', 'Blad podczas aktualizacji reservation_to_reduce ' . " \n" . $e->getMessage(), true);
                return false;
            }

            if ($this->pDbMgr->Update('profit24', 'products_stock_reservations', ['move_to_erp' => '1'], ' order_id = ' . $iOrderId) === false) {
                $this->pDbMgr->RollbackTransaction('profit24');
                return false;
            }
        }

        if (empty($aOrder['erp_export'])) {
            $aValues = [
                'erp_export' => 'NOW()'
            ];
            if ($this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOrderId) === false) {
                $this->pDbMgr->RollbackTransaction('profit24');
                return false;
            }
        }
        $this->pDbMgr->CommitTransaction('profit24');
  }
  
  /**
   * 
   * @return bool
   */
  public function proceedValidateInvoicesAddAlert() {
    $bIsErr = false;
    $aOrders = $this->getOrdersSynchroToday();
    
    $oAlerts = new OrdersAlert();
    foreach ($aOrders as $aOrder) {
      try {
        $this->proceedValidateSingleInvoice($aOrder);
      } catch (Exception $ex) {
        $bIsErr = true;
        $oAlerts->createAlert($aOrder['id'], 'auto-export-invoices', $ex->getMessage());
      }
    }
    $oAlerts->removeDuplicates();
    $oAlerts->addAlerts();
    return !$bIsErr;
  }

  public function proceedValidateInvoicesByDay($dInvoiceDate) {
    $bIsErr = false;
    $aOrders = $this->getOrdersSynchroByDate($dInvoiceDate);

    foreach ($aOrders as $aOrder) {
      try {
        $this->proceedValidateSingleInvoice($aOrder);
      } catch (Exception $ex) {
        print_r('rózne wartości '.$ex->getMessage());
        echo "\r\n";
      }
    }

  }

    /**
     *  proceedValidateInvoicesByRealized
     *
     *
     */
    public function proceedValidateInvoicesByRealized() {
        $this->bIsFix = false;
        $bIsErr = false;
        $aOrders = $this->getOrdersSynchroByRealized();
        print_r($aOrders);
        foreach ($aOrders as $aOrder) {

            try {
                $this->proceedValidateSingleInvoiceSendOuz($aOrder);
                $aProductsIds = $this->getProductsOrdersItems($aOrder['id']);
                $this->oStatusSupplyUpdater->substractReservationOnStockSupplies($aProductsIds);
            } catch (Exception $ex) {
                print_r('rózne wartości '.$ex->getMessage());
                echo "\r\n";
            }
        }

    }

    /**
     * @param $iOrderId
     * @return array
     */
    private function getProductsOrdersItems($iOrderId) {

        $sSql = 'SELECT product_id 
                 FROM orders_items WHERE order_id = '.$iOrderId.' AND product_id IS NOT NULL ';
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }


    private function deleteOrderReservation($iOrderId) {

        $reservations = $this->dataProvider->getReservationsOrdersItemsByOrderId($iOrderId);
        foreach ($reservations as $reservation) {
            $this->productStockReservation->deleteReservationBySourceId($reservation['source_id'], $iOrderId, $reservation['order_item_id']);
            $this->stockLocation->unreserveStockLocation($iOrderId, $reservation['order_item_id']);
        }
    }

    /**
     *
     * @param array $aOrder
     */
    public function proceedValidateSingleInvoiceSendOuz(array $aOrder)
    {
        $bIsErr = false;

        if ($aOrder['second_invoice'] == '1' && $aOrder['invoice2_id'] != '') {
            $wsZestVat = $this->getZestWS($aOrder['id'], $aOrder['invoice2_id'], false);
            if (false !== $wsZestVat) {
                $invoiceExists = $this->validateInvoice($wsZestVat, $aOrder, TRUE);
                if (true === $invoiceExists) {
                    $this->deleteOrderReservation($aOrder['id']);
                    if (empty($aOrder['erp_send_ouz'])) {
                        $this->setSendOuz($aOrder['id']);
                    }
                } else if (false === $invoiceExists) {
                    $bIsErr = true;
                }
            }
        }


        $wsZestVat = $this->getZestWS($aOrder['id'], $aOrder['invoice_id'], false);
        if (false !== $wsZestVat) {
            $invoiceExists = $this->validateInvoice($wsZestVat, $aOrder, FALSE);
            if (true === $invoiceExists) {
                $this->deleteOrderReservation($aOrder['id']);
                if (empty($aOrder['erp_send_ouz'])) {
                    $this->setSendOuz($aOrder['id']);
                }
            } else if (false === $invoiceExists) {
                $bIsErr = true;
            }
        }

        return $bIsErr === TRUE ? FALSE : TRUE;
    }


    /**
     * @param $iOrderId
     * @return bool
     */
    private function setSendOuz($iOrderId) {

        $aValues = [
            'erp_send_ouz' => 'NOW()'
        ];
        if ($this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOrderId) === false) {
            return false;
        }
    }
  
  /**
   * 
   * @param array $aOrder
   */
  public function proceedValidateSingleInvoice(array $aOrder) {
    $bIsErr = false;
    
    if ($aOrder['second_invoice'] == '1' && $aOrder['invoice2_id'] != '') {
      $wsZestVat = $this->getZestWS($aOrder['id'], $aOrder['invoice2_id']);
      if ($this->validateInvoice($wsZestVat, $aOrder, TRUE) === FALSE) {
        $bIsErr = true;
      }
    }
    
    $wsZestVat = $this->getZestWS($aOrder['id'], $aOrder['invoice_id']);
    if ($this->validateInvoice($wsZestVat, $aOrder, FALSE) === FALSE) {
      $bIsErr = true;
    }
    
    return $bIsErr === TRUE ? FALSE : TRUE;
  }

  /**
   * @param wsZestVat $wsZestVat
   * @param array $aOrder
   * @param $bSecondInvoice
   * @return bool
   * @throws Exception
   */
  private function validateInvoice(wsZestVat $wsZestVat, array $aOrder, $bSecondInvoice) {
    
    $aOrderData = $this->oOrderItemRecount->getOrderPricesDetail($aOrder['id'], $bSecondInvoice, '', true);
    $fPaidAmount = Common::formatPrice2($aOrderData['order']['to_pay']);
    if (floatval($wsZestVat->getWart_brutto()) === floatval($fPaidAmount)) {
      $this->setExportedErp($aOrder['id'], $aOrder);
      return true;
    } else {
      if ($aOrder['second_invoice'] == '1') {
        $this->setExportedErp($aOrder['id'], $aOrder);
        return true;
      }
      $sMsg = sprintf(_('Rożne wartości FV brutto %s - %s'), 
//              $wsZestVat->getWart_netto(), $aOrder['value_netto'], 
              $wsZestVat->getWart_brutto(), $fPaidAmount
              );
//      file_put_contents('log_err_1.log', $aOrder['id']."\n".$sMsg."\n"."\n", FILE_APPEND);
      throw new Exception($sMsg);
    }
  }
}
