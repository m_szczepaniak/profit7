<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use stdClass;

include_once('WS/autoload.php');
/**
 * Description of synchroProductImage
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class synchroProductImage extends ProfitServerImplService{
  
  /**
   *
   * @var void 
   */
  private $sesja;
  
  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  public function __construct($pDbMgr) {
    global $aConfig;
    
    $this->pDbMgr = $pDbMgr;
    if ($aConfig['common']['status'] == 'development') {
      parent::__construct(array(), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
    } else {
      parent::__construct();
    }
    $this->sesja = $this->utworzSesje('webservice', 'profwebservice')->getValue();
  }
  
  /**
   * 
   * @return bool
   */
  public function synchroAllImages() {
    $bIsErr = false;
    $aImages = $this->getUpdateImages();
    
    foreach ($aImages as $aImage) {
      sleep(1);
      $oImage = (object)$aImage;
      $sBase64Image = $this->getBase64Image($oImage);
      if ($sBase64Image !== false && $sBase64Image != '') {
        try {
          if ($this->synchroImage($oImage, $sBase64Image) !== true ) {
//            $bIsErr = true;
          }
        } catch (Exception $ex) {
//          $bIsErr = true;
          echo $ex->getMessage();
        }
      }
    }
    
    if ($bIsErr === false) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param stdClass $oImage
   * @return base64 - image
   */
  private function getBase64Image($oImage) {
    global $aConfig;
    
    $sFullPathName = $aConfig['common']['client_base_path'] . '/' . $aConfig['common']['photo_dir'] . '/' . $oImage->directory . '/' . $oImage->photo;
    if (($sFileSource = file_get_contents($sFullPathName)) != '') {
      return base64_encode($sFileSource);
    } else {
      return false;
    }
  }
  
  
  /**
   * 
   * @param stdClass $oImage
   * @param string  $sBase64Image
   * @return base64 - image
   */
  private function synchroImage(stdClass $oImage, $sBase64Image) {
    $oImageReturn = $this->wprowadzOkladke($this->sesja, $oImage->streamsoft_indeks, $oImage->name, $sBase64Image);
    
    if ($oImageReturn->getErrorCode() == -1) {
      return false;
    } elseif ($oImageReturn->getErrorCode() != 0) {
      throw new Exception($oImageReturn->getErrorMessage(), $oImageReturn->getErrorCode());
    } else {
      echo 'Dodano okładkę kartoteki: '.$oImage->streamsoft_indeks."\n";
      return true;
    }
  }
  
  
  /**
   * 
   * @return array
   */
  private function getUpdateImages() {
    
    $sSql = 'SELECT PI.*, P.streamsoft_indeks
            FROM products_images AS PI
            JOIN products AS P
             ON P.id = PI.product_id 
             AND P.prod_status = "1" 
             AND P.published = "1"
            WHERE DATE( PI.created ) >= DATE(SUBDATE(NOW(), 1))
            ORDER BY PI.created ASC ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
}
