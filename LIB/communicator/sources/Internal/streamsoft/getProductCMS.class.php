<?php

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use stdClass;
class getProductCMS {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  public function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @param int $iProductId
   * @param bool $bForce
   * @return stdClass
   */
  public function getSingleProduct($iProductId, $bForce) {
    
    $sSql = 'SELECT 
  P.id,
  P.name, 
  P.name2, 

    (SELECT CONCAT(name, " ;; ", id)
    FROM products_publishers AS PP 
    WHERE PP.id = P.publisher_id 
    LIMIT 1) AS publisher,

    (SELECT CONCAT(name, " ;; ", id)
    FROM products_series AS PS
    JOIN products_to_series AS PTS
      ON PTS.series_id = PS.id
    WHERE PTS.product_id = P.id
    LIMIT 1) AS series,

  (
    SELECT GROUP_CONCAT(CONCAT(IFNULL(name, ""), " ", IFNULL(surname, "")) SEPARATOR " ;; ")
    FROM products_authors AS PA
    JOIN products_to_authors AS PTA
      ON PTA.author_id = PA.id
    WHERE PTA.product_id = P.id
    LIMIT 1
  ) AS authors,
  (
    SELECT binding
    FROM products_bindings AS PB
    WHERE PB.id = P.binding
    LIMIT 1
  ) AS binding,
  (
    SELECT dimension
    FROM products_dimensions AS PB
    WHERE PB.id = P.dimension
    LIMIT 1
  ) AS dimension,
  (
    SELECT language
    FROM products_languages AS PB
    WHERE PB.id = P.language
    LIMIT 1
  ) AS language,
  P.price_netto,
  P.price_brutto,
  P.vat,
  P.pkwiu,
  P.weight,
  P.isbn_plain,
  P.isbn_13,
  P.isbn_10,
  P.ean_13,
  P.streamsoft_indeks,
  P.publication_year,
  P.pages,
  P.volumes,
  P.volume_nr,
  P.edition,
  P.legal_status_date,
  P.type
  
  FROM products AS P
  WHERE 
    P.id = '.$iProductId;
    if ($bForce !== TRUE) {
      $sSql .= '
       AND
      P.prod_status = "1" AND 
      P.published = "1" AND
      P.packet = "0" AND 
      P.shipment_time = "0"';
    }
    $aProduct = $this->pDbMgr->GetRow('profit24', $sSql);
    $oProduct = new stdClass();
    if (!empty($aProduct)) {
      $oProduct = (object)$this->parseProductItem($aProduct);
      $oProduct->attachment = (object)$this->getProductAttachment($iProductId);
        if (!empty($oProduct->attachment)) {
            $oProduct = $this->substractProductPriceByAttachment($oProduct);
        }
    }
    return $oProduct;
  }


    /**
     * @param stdClass $product
     * @return stdClass
     */
    private function substractProductPriceByAttachment(stdClass $product) {
      $att = $product->attachment;
      $product->price_brutto -= $att->price_brutto;
      $product->price_netto = \Common::formatPrice2($product->price_brutto / (1 + ($product->vat / 100)));

      return $product;
    }
  
  
  /**
   * 
   * @param int $iProductId
   * @param bool $bForce
   * @return stdClass
   */
  public function getSingleProductByStramsoftId($sStraemsoftIndeks, $bForce) {
    
    $sSql = 'SELECT 
  P.id,
  P.name, 
  P.name2, 

    (SELECT CONCAT(name, " ;; ", id)
    FROM products_publishers AS PP 
    WHERE PP.id = P.publisher_id 
    LIMIT 1) AS publisher,

    (SELECT CONCAT(name, " ;; ", id)
    FROM products_series AS PS
    JOIN products_to_series AS PTS
      ON PTS.series_id = PS.id
    WHERE PTS.product_id = P.id
    LIMIT 1) AS series,

  (
    SELECT GROUP_CONCAT(CONCAT(IFNULL(name, ""), " ", IFNULL(surname, "")) SEPARATOR " ;; ")
    FROM products_authors AS PA
    JOIN products_to_authors AS PTA
      ON PTA.author_id = PA.id
    WHERE PTA.product_id = P.id
    LIMIT 1
  ) AS authors,
  (
    SELECT binding
    FROM products_bindings AS PB
    WHERE PB.id = P.binding
    LIMIT 1
  ) AS binding,
  (
    SELECT dimension
    FROM products_dimensions AS PB
    WHERE PB.id = P.dimension
    LIMIT 1
  ) AS dimension,
  (
    SELECT language
    FROM products_languages AS PB
    WHERE PB.id = P.language
    LIMIT 1
  ) AS language,
  P.price_netto,
  P.price_brutto,
  P.vat,
  P.pkwiu,
  P.weight,
  P.isbn_plain,
  P.isbn_13,
  P.isbn_10,
  P.ean_13,
  P.streamsoft_indeks,
  P.publication_year,
  P.pages,
  P.volumes,
  P.volume_nr,
  P.edition,
  P.legal_status_date,
  P.type
  
  FROM products AS P
  WHERE 
    P.streamsoft_indeks = "'.$sStraemsoftIndeks.'"';
    if ($bForce !== TRUE) {
      $sSql .= '
       AND
      P.prod_status = "1" AND 
      P.published = "1" AND
      P.packet = "0" AND 
      P.shipment_time = "0"';
    }
    
    $aProduct = $this->pDbMgr->GetRow('profit24', $sSql);
    $oProduct = new stdClass();
    if (!empty($aProduct)) {
      $oProduct = (object)$this->parseProductItem($aProduct);
      $oProduct->attachment = (object)$this->getProductAttachment($aProduct['id']);
    }
    return $oProduct;
  }
  
  /**
   * 
   * @param int $iProductId
   * @return array
   */
  private function getProductAttachment($iProductId) {
    
    $sSql = 'SELECT PA.id, PAT.name, PA.price_netto, PA.price_brutto, PA.vat
      FROM products_attachments AS PA
      JOIN products_attachment_types AS PAT
        ON PA.attachment_type = PAT.id
      WHERE PA.product_id = '.$iProductId;
    
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }

    private function getTopProductsCategories(){
        global $aConfig;

        $sSql="SELECT id
					 FROM ".$aConfig['tabls']['prefix']."menus_items
					 WHERE module_id = ".getModuleId('m_oferta_produktowa')."
					 AND mtype = '0'
					 AND parent_id IS NULL
					 AND published = '1'";
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

  /**
   * 
   * @global DatabaseManager $pDbMgr
   * @param int $iProductId
   * @param string $sSeparator
   * @return string
   */
  private function getProductCategorySingleBranch($iProductId, $sSeparator) {
    $sCategories = '';
    $sCategoriesIds = '';





    //drugi poziom
      $aTopCats = $this->getTopProductsCategories();
      $sSql = 'SELECT MI.name, MI.id, MI.parent_id FROM menus_items AS MI
              JOIN products_extra_categories AS PEC ON PEC.page_id = MI.id
              WHERE PEC.product_id = '.$iProductId.'
              AND MI.parent_id IN ('.implode(',', $aTopCats).') 
              ORDER BY MI.priority ';
      $aRow2 = $this->pDbMgr->GetRow('profit24', $sSql);

      if (!empty($aRow2)) {
          $sSql = 'SELECT MI.name, MI.id FROM menus_items AS MI
            JOIN products_extra_categories AS PEC ON PEC.page_id = MI.id
            WHERE MI.id = ('.$aRow2['parent_id'].') 
            ORDER BY MI.priority ';
          $aRow1 = $this->pDbMgr->GetRow('profit24', $sSql);
          $sCategories = $this->filterNameGroup($aRow1['name']);
          $sCategoriesIds = $aRow1['id'];
      }

      if (!empty($aRow2)) {
          $sCategories .= $sSeparator . $this->filterNameGroup($aRow2['name']);
          $sCategoriesIds .= $sSeparator . $aRow2['id'];
      }

    //trzeci poziom
    if (!empty($aRow2)) {
      $sSql = 'SELECT MI.name, MI.id FROM menus_items AS MI
              JOIN products_extra_categories AS PEC ON PEC.page_id = MI.id
              WHERE PEC.product_id = '.$iProductId.'
              AND MI.parent_id = '.$aRow2['id'].' 
              ORDER BY MI.priority ';
      $aRow3 = $this->pDbMgr->GetRow('profit24', $sSql);
      if (!empty($aRow3)) {
        $sCategories .= $sSeparator . $this->filterNameGroup($aRow3['name']);
        $sCategoriesIds .= $sSeparator . $aRow3['id'];
      }
    }

    //czwarty poziom
    if (!empty($aRow3)) {
      $sSql = 'SELECT MI.name, MI.id FROM menus_items AS MI
              JOIN products_extra_categories AS PEC ON PEC.page_id = MI.id
              WHERE PEC.product_id = '.$iProductId.'
              AND MI.parent_id = '.$aRow3['id'].' 
              ORDER BY MI.priority ';
      $aRow4 = $this->pDbMgr->GetRow('profit24', $sSql);
      if (!empty($aRow4)) {
        $sCategories .= $sSeparator . $this->filterNameGroup($aRow4['name']);
        $sCategoriesIds .= $sSeparator . $aRow4['id'];
      }
    }
    $aCategories = array(
        'names' => $sCategories,
        'ids' => $sCategoriesIds
    );
    return $aCategories;
  }

  /**
   * 
   * @param string $sName
   * @return string
   */
  private function filterNameGroup($sName) {
    return str_replace('-', ' ', $sName);
  }
  
  /**
   * 
   * @param array $aItem
   * @return stdClass
   */
  private function parseProductItem($aItem) {
    
    $aCategoriesData = $this->getProductCategorySingleBranch($aItem['id'],' - ');
    $aItem['name'] = mb_substr($aItem['name'], 0, 360, 'UTF-8');
    $aItem['name2'] = mb_substr($aItem['name2'], 0, 60, 'UTF-8');
    $aItem['authors'] = mb_substr($aItem['authors'], 0, 60, 'UTF-8');
    $aItem['category'] = mb_substr($aCategoriesData['names'], 0, 60, 'UTF-8');
    $aItem['full_categories'] = $aCategoriesData['names'];
    $aItem['ID_category'] = $aCategoriesData['ids'];
    $aSeries = explode(' ;; ', $aItem['series']);
    $aPublishers = explode(' ;; ', $aItem['publisher']);

    $aItem['publisher'] = $this->trimIf($aPublishers[0], 49, $aPublishers[1]);
    $aItem['ID_publisher'] = $aPublishers[1];
    $aItem['series'] = $this->trimIf($aSeries[0], 49, $aSeries[1]);
    $aItem['ID_series'] = $aSeries[1];
    return (object)$aItem;
  }
  
  /**
   * 
   * @param string $sStr
   * @param int $iMaxLength
   * @param int $iItemId
   * @return string
   */
  private function trimIf($sStr, $iMaxLength, $iItemId) {
    $iStrLen = mb_strlen($sStr, 'UTF-8');
    if ($iStrLen > $iMaxLength) {
      $sAdditionalStr = '-'.$iItemId;
      $iLenAdditionalStr = mb_strlen($sAdditionalStr, 'UTF-8');
      $sStr = mb_substr($sStr, 0, ($iMaxLength - $iLenAdditionalStr), 'UTF8') . $sAdditionalStr;
      return $sStr;
    }
    return $sStr;
  }
}
