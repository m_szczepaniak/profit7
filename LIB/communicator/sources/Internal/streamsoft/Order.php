<?php

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use DateTime;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok;
use LIB\communicator\sources\Internal\streamsoft\WS\wsNagl;

class Order extends ProfitServerImplService
{
    /**
     * @var DatabaseManager
     */
    private $pDbMgr;


    /**
     * @var void
     */
    private $sesja;

    /**
     * @param DatabaseManager $pDbMgr
     */
    public function __construct(DatabaseManager $pDbMgr)
    {
        global $aConfig;//, $oTimer
        $this->pDbMgr = $pDbMgr;
        if ($aConfig['common']['status'] == 'development') {
            parent::__construct([], 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
        } else {
            parent::__construct();
        }

//        $this->sesja = $this->utworzSesje('webservice', 'profwebservice')->getValue();
    }

    private function createHeader($externalNumber, $orderNumber)
    {

        $cechy = new wsCechaDok($orderNumber, '', '', '', '');

        $kontrahent = new WS\wsKontrah('', '', '', '', '', '', '', '', '', '', '', '', '');
        $naglowek = new wsNagl($kontrahent, 'Zamodb', 'ZA', $externalNumber, new DateTime(), '', '', new DateTime(), '', '', $cechy);

        return $naglowek;
    }

    /**
     * @param order
     * @param array $positions
     *
     * @return int
     *
     * @throws Exception
     */
    public function createOrUpdateOrder($order, array $positions)
    {
        $externalCode = $order['streamsoft_order_number'];
        $orderNumber = substr($order['streamsoft_order_number'], 0, 12);

        $header = $this->createHeader($externalCode, $orderNumber);
        $positions = (new Items())->prepareItems($positions, $this->pDbMgr, $this->sesja, true);

        $kod = $this->getKodUrzZewnKS($order['website_id']);

        $result = $this->dodajLubAktualizujZamowienie($this->sesja, $header, $positions, $kod);

        if ($result->getErrorCode() != 0) {
            throw new Exception($result->getErrorMessage(), $result->getErrorCode());
        }
        return $result->getValue();
    }

    public function cancelOrder($externalNumber)
    {
        $orderResult = $this->anulujZamowienie($this->sesja, $externalNumber);

//        if ($orderResult->getErrorCode() == -1){
//            throw new \Exception("Wystapil blad podczas anulowania zamowienia");
//        }
        return $orderResult;
    }
}