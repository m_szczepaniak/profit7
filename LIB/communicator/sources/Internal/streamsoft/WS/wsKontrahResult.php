<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsKontrahResult extends wsResult
{

    /**
     * @var wsKontrah $value
     */
    protected $value = null;

    /**
     * @param int $errorCode
     * @param string $errorMessage
     * @param wsKontrah $value
     */
    public function __construct($errorCode, $errorMessage, $value)
    {
      parent::__construct($errorCode, $errorMessage);
      $this->value = $value;
    }

    /**
     * @return wsKontrah
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param wsKontrah $value
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrahResult
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
