<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsPoz
{

    /**
     * @var string $indeks
     */
    protected $indeks = null;

    /**
     * @var int $ilosc
     */
    protected $ilosc = null;

    /**
     * @var boolean $czyBrutto
     */
    protected $czyBrutto = null;

    /**
     * @var float $cena_sprzedazy
     */
    protected $cena_sprzedazy = null;

    /**
     * @var boolean $czyCenaUzgodniona
     */
    protected $czyCenaUzgodniona = null;

    /**
     * @var float $procent_upustu
     */
    protected $procent_upustu = null;

    /**
     * @var string $mag_oznnrwydr
     */
    protected $mag_oznnrwydr = null;

    /**
     * @var string $uwagi
     */
    protected $uwagi = null;

    /**
     * @var string $cecha1
     */
    protected $cecha1 = null;

    /**
     * @var string $cecha2
     */
    protected $cecha2 = null;

    /**
     * @var string $cecha3
     */
    protected $cecha3 = null;

    /**
     * @var \DateTime $dotyczy_daty
     */
    protected $dotyczy_daty = null;

    /**
     * @var int $dotyczy_lp
     */
    protected $dotyczy_lp = null;

    /**
     * @var string $dotyczy_nazwa_dok
     */
    protected $dotyczy_nazwa_dok = null;

    /**
     * @var string $dotyczy_numer_dok
     */
    protected $dotyczy_numer_dok = null;

    /**
     * @param string $indeks
     * @param int $ilosc
     * @param boolean $czyBrutto
     * @param float $cena_sprzedazy
     * @param boolean $czyCenaUzgodniona
     * @param float $procent_upustu
     * @param string $mag_oznnrwydr
     * @param string $uwagi
     * @param string $cecha1
     * @param string $cecha2
     * @param string $cecha3
     * @param \DateTime $dotyczy_daty
     * @param int $dotyczy_lp
     * @param string $dotyczy_nazwa_dok
     * @param string $dotyczy_numer_dok
     */
    public function __construct($indeks, $ilosc, $czyBrutto, $cena_sprzedazy, $czyCenaUzgodniona, $procent_upustu, $mag_oznnrwydr, $uwagi, $cecha1, $cecha2, $cecha3, \DateTime $dotyczy_daty, $dotyczy_lp, $dotyczy_nazwa_dok, $dotyczy_numer_dok)
    {
      $this->indeks = $indeks;
      $this->ilosc = $ilosc;
      $this->czyBrutto = $czyBrutto;
      $this->cena_sprzedazy = $cena_sprzedazy;
      $this->czyCenaUzgodniona = $czyCenaUzgodniona;
      $this->procent_upustu = $procent_upustu;
      $this->mag_oznnrwydr = $mag_oznnrwydr;
      $this->uwagi = $uwagi;
      $this->cecha1 = $cecha1;
      $this->cecha2 = $cecha2;
      $this->cecha3 = $cecha3;
      $this->dotyczy_daty = $dotyczy_daty->format(\DateTime::ATOM);
      $this->dotyczy_lp = $dotyczy_lp;
      $this->dotyczy_nazwa_dok = $dotyczy_nazwa_dok;
      $this->dotyczy_numer_dok = $dotyczy_numer_dok;
    }

    /**
     * @return string
     */
    public function getIndeks()
    {
      return $this->indeks;
    }

    /**
     * @param string $indeks
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setIndeks($indeks)
    {
      $this->indeks = $indeks;
      return $this;
    }

    /**
     * @return int
     */
    public function getIlosc()
    {
      return $this->ilosc;
    }

    /**
     * @param int $ilosc
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setIlosc($ilosc)
    {
      $this->ilosc = $ilosc;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCzyBrutto()
    {
      return $this->czyBrutto;
    }

    /**
     * @param boolean $czyBrutto
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setCzyBrutto($czyBrutto)
    {
      $this->czyBrutto = $czyBrutto;
      return $this;
    }

    /**
     * @return float
     */
    public function getCena_sprzedazy()
    {
      return $this->cena_sprzedazy;
    }

    /**
     * @param float $cena_sprzedazy
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setCena_sprzedazy($cena_sprzedazy)
    {
      $this->cena_sprzedazy = $cena_sprzedazy;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getCzyCenaUzgodniona()
    {
      return $this->czyCenaUzgodniona;
    }

    /**
     * @param boolean $czyCenaUzgodniona
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setCzyCenaUzgodniona($czyCenaUzgodniona)
    {
      $this->czyCenaUzgodniona = $czyCenaUzgodniona;
      return $this;
    }

    /**
     * @return float
     */
    public function getProcent_upustu()
    {
      return $this->procent_upustu;
    }

    /**
     * @param float $procent_upustu
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setProcent_upustu($procent_upustu)
    {
      $this->procent_upustu = $procent_upustu;
      return $this;
    }

    /**
     * @return string
     */
    public function getMag_oznnrwydr()
    {
      return $this->mag_oznnrwydr;
    }

    /**
     * @param string $mag_oznnrwydr
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setMag_oznnrwydr($mag_oznnrwydr)
    {
      $this->mag_oznnrwydr = $mag_oznnrwydr;
      return $this;
    }

    /**
     * @return string
     */
    public function getUwagi()
    {
      return $this->uwagi;
    }

    /**
     * @param string $uwagi
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setUwagi($uwagi)
    {
      $this->uwagi = $uwagi;
      return $this;
    }

    /**
     * @return string
     */
    public function getCecha1()
    {
      return $this->cecha1;
    }

    /**
     * @param string $cecha1
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setCecha1($cecha1)
    {
      $this->cecha1 = $cecha1;
      return $this;
    }

    /**
     * @return string
     */
    public function getCecha2()
    {
      return $this->cecha2;
    }

    /**
     * @param string $cecha2
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setCecha2($cecha2)
    {
      $this->cecha2 = $cecha2;
      return $this;
    }

    /**
     * @return string
     */
    public function getCecha3()
    {
      return $this->cecha3;
    }

    /**
     * @param string $cecha3
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setCecha3($cecha3)
    {
      $this->cecha3 = $cecha3;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDotyczy_daty()
    {
      if ($this->dotyczy_daty == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->dotyczy_daty);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $dotyczy_daty
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setDotyczy_daty(\DateTime $dotyczy_daty)
    {
      $this->dotyczy_daty = $dotyczy_daty->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return int
     */
    public function getDotyczy_lp()
    {
      return $this->dotyczy_lp;
    }

    /**
     * @param int $dotyczy_lp
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setDotyczy_lp($dotyczy_lp)
    {
      $this->dotyczy_lp = $dotyczy_lp;
      return $this;
    }

    /**
     * @return string
     */
    public function getDotyczy_nazwa_dok()
    {
      return $this->dotyczy_nazwa_dok;
    }

    /**
     * @param string $dotyczy_nazwa_dok
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setDotyczy_nazwa_dok($dotyczy_nazwa_dok)
    {
      $this->dotyczy_nazwa_dok = $dotyczy_nazwa_dok;
      return $this;
    }

    /**
     * @return string
     */
    public function getDotyczy_numer_dok()
    {
      return $this->dotyczy_numer_dok;
    }

    /**
     * @param string $dotyczy_numer_dok
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPoz
     */
    public function setDotyczy_numer_dok($dotyczy_numer_dok)
    {
      $this->dotyczy_numer_dok = $dotyczy_numer_dok;
      return $this;
    }

}
