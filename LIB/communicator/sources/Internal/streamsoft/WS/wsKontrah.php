<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsKontrah
{

    /**
     * @var int $nrkontrah
     */
    protected $nrkontrah = null;

    /**
     * @var string $nazwaskr
     */
    protected $nazwaskr = null;

    /**
     * @var string $nazwadl
     */
    protected $nazwadl = null;

    /**
     * @var string $nip
     */
    protected $nip = null;

    /**
     * @var string $miejscowosc
     */
    protected $miejscowosc = null;

    /**
     * @var string $kodpoczta
     */
    protected $kodpoczta = null;

    /**
     * @var string $poczta
     */
    protected $poczta = null;

    /**
     * @var string $ulica
     */
    protected $ulica = null;

    /**
     * @var string $nrdomu
     */
    protected $nrdomu = null;

    /**
     * @var string $nrlokalu
     */
    protected $nrlokalu = null;

    /**
     * @var string $kodkraju
     */
    protected $kodkraju = null;

    /**
     * @var string $email
     */
    protected $email = null;

    /**
     * @var string $telefon
     */
    protected $telefon = null;

    /**
     * @param int $nrkontrah
     * @param string $nazwaskr
     * @param string $nazwadl
     * @param string $nip
     * @param string $miejscowosc
     * @param string $kodpoczta
     * @param string $poczta
     * @param string $ulica
     * @param string $nrdomu
     * @param string $nrlokalu
     * @param string $kodkraju
     * @param string $email
     * @param string $telefon
     */
    public function __construct($nrkontrah, $nazwaskr, $nazwadl, $nip, $miejscowosc, $kodpoczta, $poczta, $ulica, $nrdomu, $nrlokalu, $kodkraju, $email, $telefon)
    {
      $this->nrkontrah = $nrkontrah;
      $this->nazwaskr = $nazwaskr;
      $this->nazwadl = $nazwadl;
      $this->nip = $nip;
      $this->miejscowosc = $miejscowosc;
      $this->kodpoczta = $kodpoczta;
      $this->poczta = $poczta;
      $this->ulica = $ulica;
      $this->nrdomu = $nrdomu;
      $this->nrlokalu = $nrlokalu;
      $this->kodkraju = $kodkraju;
      $this->email = $email;
      $this->telefon = $telefon;
    }

    /**
     * @return int
     */
    public function getNrkontrah()
    {
      return $this->nrkontrah;
    }

    /**
     * @param int $nrkontrah
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setNrkontrah($nrkontrah)
    {
      $this->nrkontrah = $nrkontrah;
      return $this;
    }

    /**
     * @return string
     */
    public function getNazwaskr()
    {
      return $this->nazwaskr;
    }

    /**
     * @param string $nazwaskr
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setNazwaskr($nazwaskr)
    {
      $this->nazwaskr = $nazwaskr;
      return $this;
    }

    /**
     * @return string
     */
    public function getNazwadl()
    {
      return $this->nazwadl;
    }

    /**
     * @param string $nazwadl
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setNazwadl($nazwadl)
    {
      $this->nazwadl = $nazwadl;
      return $this;
    }

    /**
     * @return string
     */
    public function getNip()
    {
      return $this->nip;
    }

    /**
     * @param string $nip
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setNip($nip)
    {
      $this->nip = $nip;
      return $this;
    }

    /**
     * @return string
     */
    public function getMiejscowosc()
    {
      return $this->miejscowosc;
    }

    /**
     * @param string $miejscowosc
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setMiejscowosc($miejscowosc)
    {
      $this->miejscowosc = $miejscowosc;
      return $this;
    }

    /**
     * @return string
     */
    public function getKodpoczta()
    {
      return $this->kodpoczta;
    }

    /**
     * @param string $kodpoczta
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setKodpoczta($kodpoczta)
    {
      $this->kodpoczta = $kodpoczta;
      return $this;
    }

    /**
     * @return string
     */
    public function getPoczta()
    {
      return $this->poczta;
    }

    /**
     * @param string $poczta
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setPoczta($poczta)
    {
      $this->poczta = $poczta;
      return $this;
    }

    /**
     * @return string
     */
    public function getUlica()
    {
      return $this->ulica;
    }

    /**
     * @param string $ulica
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setUlica($ulica)
    {
      $this->ulica = $ulica;
      return $this;
    }

    /**
     * @return string
     */
    public function getNrdomu()
    {
      return $this->nrdomu;
    }

    /**
     * @param string $nrdomu
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setNrdomu($nrdomu)
    {
      $this->nrdomu = $nrdomu;
      return $this;
    }

    /**
     * @return string
     */
    public function getNrlokalu()
    {
      return $this->nrlokalu;
    }

    /**
     * @param string $nrlokalu
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setNrlokalu($nrlokalu)
    {
      $this->nrlokalu = $nrlokalu;
      return $this;
    }

    /**
     * @return string
     */
    public function getKodkraju()
    {
      return $this->kodkraju;
    }

    /**
     * @param string $kodkraju
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setKodkraju($kodkraju)
    {
      $this->kodkraju = $kodkraju;
      return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
      return $this->email;
    }

    /**
     * @param string $email
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setEmail($email)
    {
      $this->email = $email;
      return $this;
    }

    /**
     * @return string
     */
    public function getTelefon()
    {
      return $this->telefon;
    }

    /**
     * @param string $telefon
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah
     */
    public function setTelefon($telefon)
    {
      $this->telefon = $telefon;
      return $this;
    }

}
