<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsDokBank
{

    /**
     * @var string[] $value
     */
    protected $data_dokumentu = null;

    protected $kod_banku = null;

    protected $kod_dokumentu = null;

    protected $kwota = null;

    protected $numer_kontrahenta = null;

    protected $opis = null;

    /**
     * @param $data_dokumentu
     * @param $kod_banku
     * @param $kod_dokumentu
     * @param $kwota
     * @param $numer_kontrahenta
     * @param $opis
     */
    public function __construct($data_dokumentu = '', $kod_banku = '', $kod_dokumentu = '', $kwota = '', $numer_kontrahenta = '', $opis = '')
    {

        $this->data_dokumentu = $data_dokumentu;
        $this->kod_banku = $kod_banku;
        $this->kod_dokumentu = $kod_dokumentu;
        $this->kwota = $kwota;
        $this->numer_kontrahenta = $numer_kontrahenta;
        $this->opis = $opis;
    }

    public function getData_dokumentu()
    {
      return $this->data_dokumentu;
    }

    public function setData_dokumentu($data_dokumentu)
    {
      $this->data_dokumentu = $data_dokumentu;
      return $this;
    }

    public function getKod_banku() {
        return $this->kod_banku;
    }

    public function setKod_banku($kod_banku) {
        $this->kod_banku = $kod_banku;
        return $this;
    }

    public function getKod_dokumentu() {
        return $this->kod_dokumentu;
    }

    public function setKod_dokumentu($kod_dokumentu) {
        $this->kod_dokumentu = $kod_dokumentu;
        return $this;
    }

    public function getKwota() {
        return $this->kwota;
    }

    public function setKwota($kwota) {
        $this->kwota = $kwota;
        return $this;
    }

    public function getNumer_kontrahenta() {
        return $this->kwota;
    }

    public function setNumer_kontrahenta($numer_kontrahenta) {
        $this->numer_kontrahenta = $numer_kontrahenta;
        return $this;
    }

    public function setOpis($opis) {
        $this->opis = $opis;
        return $this;
    }
}
