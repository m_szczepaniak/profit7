<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsKartotekaArray
{

    /**
     * @var wsKartoteka[] $item
     */
    protected $item = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return wsKartoteka[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param wsKartoteka[] $item
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartotekaArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
