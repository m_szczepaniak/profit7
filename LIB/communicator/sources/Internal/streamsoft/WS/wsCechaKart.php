<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsCechaKart
{

    /**
     * @var int $id_cms
     */
    protected $id_cms = null;

    /**
     * @var string $oprawa
     */
    protected $oprawa = null;

    /**
     * @var string $format
     */
    protected $format = null;

    /**
     * @var string $jezyk
     */
    protected $jezyk = null;

    /**
     * @var string $waga
     */
    protected $waga = null;

    /**
     * @var int $rok_wydania
     */
    protected $rok_wydania = null;

    /**
     * @var int $liczba_stron
     */
    protected $liczba_stron = null;

    /**
     * @var int $liczba_tomow
     */
    protected $liczba_tomow = null;

    /**
     * @var int $numer_tomu
     */
    protected $numer_tomu = null;

    /**
     * @var int $wydanie
     */
    protected $wydanie = null;

    /**
     * @var string $stan_prawny_na_dzien
     */
    protected $stan_prawny_na_dzien = null;

    /**
     * @var string $typ
     */
    protected $typ = null;

    /**
     * @var boolean $posiada_zalacznik
     */
    protected $posiada_zalacznik = null;

    /**
     * @var string $typ_zalacznika
     */
    protected $typ_zalacznika = null;

    /**
     * @var string $isbn
     */
    protected $isbn = null;

    /**
     * @param int $id_cms
     * @param string $oprawa
     * @param string $format
     * @param string $jezyk
     * @param string $waga
     * @param int $rok_wydania
     * @param int $liczba_stron
     * @param int $liczba_tomow
     * @param int $numer_tomu
     * @param int $wydanie
     * @param string $stan_prawny_na_dzien
     * @param string $typ
     * @param boolean $posiada_zalacznik
     * @param string $typ_zalacznika
     * @param string $isbn
     */
    public function __construct($id_cms, $oprawa, $format, $jezyk, $waga, $rok_wydania, $liczba_stron, $liczba_tomow, $numer_tomu, $wydanie, $stan_prawny_na_dzien, $typ, $posiada_zalacznik, $typ_zalacznika, $isbn)
    {
      $this->id_cms = $id_cms;
      $this->oprawa = $oprawa;
      $this->format = $format;
      $this->jezyk = $jezyk;
      $this->waga = $waga;
      $this->rok_wydania = $rok_wydania;
      $this->liczba_stron = $liczba_stron;
      $this->liczba_tomow = $liczba_tomow;
      $this->numer_tomu = $numer_tomu;
      $this->wydanie = $wydanie;
      $this->stan_prawny_na_dzien = $stan_prawny_na_dzien;
      $this->typ = $typ;
      $this->posiada_zalacznik = $posiada_zalacznik;
      $this->typ_zalacznika = $typ_zalacznika;
      $this->isbn = $isbn;
    }

    /**
     * @return int
     */
    public function getId_cms()
    {
      return $this->id_cms;
    }

    /**
     * @param int $id_cms
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setId_cms($id_cms)
    {
      $this->id_cms = $id_cms;
      return $this;
    }

    /**
     * @return string
     */
    public function getOprawa()
    {
      return $this->oprawa;
    }

    /**
     * @param string $oprawa
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setOprawa($oprawa)
    {
      $this->oprawa = $oprawa;
      return $this;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
      return $this->format;
    }

    /**
     * @param string $format
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setFormat($format)
    {
      $this->format = $format;
      return $this;
    }

    /**
     * @return string
     */
    public function getJezyk()
    {
      return $this->jezyk;
    }

    /**
     * @param string $jezyk
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setJezyk($jezyk)
    {
      $this->jezyk = $jezyk;
      return $this;
    }

    /**
     * @return string
     */
    public function getWaga()
    {
      return $this->waga;
    }

    /**
     * @param string $waga
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setWaga($waga)
    {
      $this->waga = $waga;
      return $this;
    }

    /**
     * @return int
     */
    public function getRok_wydania()
    {
      return $this->rok_wydania;
    }

    /**
     * @param int $rok_wydania
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setRok_wydania($rok_wydania)
    {
      $this->rok_wydania = $rok_wydania;
      return $this;
    }

    /**
     * @return int
     */
    public function getLiczba_stron()
    {
      return $this->liczba_stron;
    }

    /**
     * @param int $liczba_stron
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setLiczba_stron($liczba_stron)
    {
      $this->liczba_stron = $liczba_stron;
      return $this;
    }

    /**
     * @return int
     */
    public function getLiczba_tomow()
    {
      return $this->liczba_tomow;
    }

    /**
     * @param int $liczba_tomow
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setLiczba_tomow($liczba_tomow)
    {
      $this->liczba_tomow = $liczba_tomow;
      return $this;
    }

    /**
     * @return int
     */
    public function getNumer_tomu()
    {
      return $this->numer_tomu;
    }

    /**
     * @param int $numer_tomu
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setNumer_tomu($numer_tomu)
    {
      $this->numer_tomu = $numer_tomu;
      return $this;
    }

    /**
     * @return int
     */
    public function getWydanie()
    {
      return $this->wydanie;
    }

    /**
     * @param int $wydanie
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setWydanie($wydanie)
    {
      $this->wydanie = $wydanie;
      return $this;
    }

    /**
     * @return string
     */
    public function getStan_prawny_na_dzien()
    {
      return $this->stan_prawny_na_dzien;
    }

    /**
     * @param string $stan_prawny_na_dzien
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setStan_prawny_na_dzien($stan_prawny_na_dzien)
    {
      $this->stan_prawny_na_dzien = $stan_prawny_na_dzien;
      return $this;
    }

    /**
     * @return string
     */
    public function getTyp()
    {
      return $this->typ;
    }

    /**
     * @param string $typ
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setTyp($typ)
    {
      $this->typ = $typ;
      return $this;
    }

    /**
     * @return boolean
     */
    public function getPosiada_zalacznik()
    {
      return $this->posiada_zalacznik;
    }

    /**
     * @param boolean $posiada_zalacznik
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setPosiada_zalacznik($posiada_zalacznik)
    {
      $this->posiada_zalacznik = $posiada_zalacznik;
      return $this;
    }

    /**
     * @return string
     */
    public function getTyp_zalacznika()
    {
      return $this->typ_zalacznika;
    }

    /**
     * @param string $typ_zalacznika
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setTyp_zalacznika($typ_zalacznika)
    {
      $this->typ_zalacznika = $typ_zalacznika;
      return $this;
    }

    /**
     * @return string
     */
    public function getIsbn()
    {
      return $this->isbn;
    }

    /**
     * @param string $isbn
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart
     */
    public function setIsbn($isbn)
    {
      $this->isbn = $isbn;
      return $this;
    }

}
