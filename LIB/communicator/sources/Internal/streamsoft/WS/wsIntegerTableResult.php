<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsIntegerTableResult extends wsResult
{

    /**
     * @var int[] $value
     */
    protected $value = null;

    /**
     * @param int $errorCode
     * @param string $errorMessage
     */
    public function __construct($errorCode, $errorMessage)
    {
      parent::__construct($errorCode, $errorMessage);
    }

    /**
     * @return int[]
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param int[] $value
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsIntegerTableResult
     */
    public function setValue(array $value)
    {
      $this->value = $value;
      return $this;
    }

}
