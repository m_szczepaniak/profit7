<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsPozArray
{

    /**
     * @var wsPoz[] $item
     */
    protected $item = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return wsPoz[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param wsPoz[] $item
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsPozArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
