<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsKartoteka
{

    /**
     * @var wsCechaKart $cechy
     */
    protected $cechy = null;

    /**
     * @var string $indeks_kartoteki_powiazanej
     */
    protected $indeks_kartoteki_powiazanej = null;

    /**
     * @var string $tytul
     */
    protected $tytul = null;

    /**
     * @var string $podtytul
     */
    protected $podtytul = null;

    /**
     * @var wsGroup $grupa_kategoria
     */
    protected $grupa_kategoria = null;

    /**
     * @var wsGroup $grupa_wydawca
     */
    protected $grupa_wydawca = null;

    /**
     * @var string $autorzy
     */
    protected $autorzy = null;

    /**
     * @var string $kategorie
     */
    protected $kategorie = null;

    /**
     * @var string $jm
     */
    protected $jm = null;

    /**
     * @var float $cena_detaliczna_netto
     */
    protected $cena_detaliczna_netto = null;

    /**
     * @var float $cena_detaliczna_brutto
     */
    protected $cena_detaliczna_brutto = null;

    /**
     * @var string $vat
     */
    protected $vat = null;

    /**
     * @var string $PKWiU
     */
    protected $PKWiU = null;

    /**
     * @var string $glowny_ISBN
     */
    protected $glowny_ISBN = null;

    /**
     * @var string $ISBN_13
     */
    protected $ISBN_13 = null;

    /**
     * @var string $ISBN_10
     */
    protected $ISBN_10 = null;

    /**
     * @var string $EAN_13
     */
    protected $EAN_13 = null;

    /**
     * @var string $indeks
     */
    protected $indeks = null;

    /**
     * @param wsCechaKart $cechy
     * @param string $indeks_kartoteki_powiazanej
     * @param string $tytul
     * @param string $podtytul
     * @param wsGroup $grupa_kategoria
     * @param wsGroup $grupa_wydawca
     * @param string $autorzy
     * @param string $kategorie
     * @param string $jm
     * @param float $cena_detaliczna_netto
     * @param float $cena_detaliczna_brutto
     * @param string $vat
     * @param string $PKWiU
     * @param string $glowny_ISBN
     * @param string $ISBN_13
     * @param string $ISBN_10
     * @param string $EAN_13
     * @param string $indeks
     */
    public function __construct($cechy, $indeks_kartoteki_powiazanej, $tytul, $podtytul, $grupa_kategoria, $grupa_wydawca, $autorzy, $kategorie, $jm, $cena_detaliczna_netto, $cena_detaliczna_brutto, $vat, $PKWiU, $glowny_ISBN, $ISBN_13, $ISBN_10, $EAN_13, $indeks)
    {
      $this->cechy = $cechy;
      $this->indeks_kartoteki_powiazanej = $indeks_kartoteki_powiazanej;
      $this->tytul = $tytul;
      $this->podtytul = $podtytul;
      $this->grupa_kategoria = $grupa_kategoria;
      $this->grupa_wydawca = $grupa_wydawca;
      $this->autorzy = $autorzy;
      $this->kategorie = $kategorie;
      $this->jm = $jm;
      $this->cena_detaliczna_netto = $cena_detaliczna_netto;
      $this->cena_detaliczna_brutto = $cena_detaliczna_brutto;
      $this->vat = $vat;
      $this->PKWiU = $PKWiU;
      $this->glowny_ISBN = $glowny_ISBN;
      $this->ISBN_13 = $ISBN_13;
      $this->ISBN_10 = $ISBN_10;
      $this->EAN_13 = $EAN_13;
      $this->indeks = $indeks;
    }

    /**
     * @return wsCechaKart
     */
    public function getCechy()
    {
      return $this->cechy;
    }

    /**
     * @param wsCechaKart $cechy
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setCechy($cechy)
    {
      $this->cechy = $cechy;
      return $this;
    }

    /**
     * @return string
     */
    public function getIndeks_kartoteki_powiazanej()
    {
      return $this->indeks_kartoteki_powiazanej;
    }

    /**
     * @param string $indeks_kartoteki_powiazanej
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setIndeks_kartoteki_powiazanej($indeks_kartoteki_powiazanej)
    {
      $this->indeks_kartoteki_powiazanej = $indeks_kartoteki_powiazanej;
      return $this;
    }

    /**
     * @return string
     */
    public function getTytul()
    {
      return $this->tytul;
    }

    /**
     * @param string $tytul
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setTytul($tytul)
    {
      $this->tytul = $tytul;
      return $this;
    }

    /**
     * @return string
     */
    public function getPodtytul()
    {
      return $this->podtytul;
    }

    /**
     * @param string $podtytul
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setPodtytul($podtytul)
    {
      $this->podtytul = $podtytul;
      return $this;
    }

    /**
     * @return wsGroup
     */
    public function getGrupa_kategoria()
    {
      return $this->grupa_kategoria;
    }

    /**
     * @param wsGroup $grupa_kategoria
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setGrupa_kategoria($grupa_kategoria)
    {
      $this->grupa_kategoria = $grupa_kategoria;
      return $this;
    }

    /**
     * @return wsGroup
     */
    public function getGrupa_wydawca()
    {
      return $this->grupa_wydawca;
    }

    /**
     * @param wsGroup $grupa_wydawca
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setGrupa_wydawca($grupa_wydawca)
    {
      $this->grupa_wydawca = $grupa_wydawca;
      return $this;
    }

    /**
     * @return string
     */
    public function getAutorzy()
    {
      return $this->autorzy;
    }

    /**
     * @param string $autorzy
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setAutorzy($autorzy)
    {
      $this->autorzy = $autorzy;
      return $this;
    }

    /**
     * @return string
     */
    public function getKategorie()
    {
      return $this->kategorie;
    }

    /**
     * @param string $kategorie
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setKategorie($kategorie)
    {
      $this->kategorie = $kategorie;
      return $this;
    }

    /**
     * @return string
     */
    public function getJm()
    {
      return $this->jm;
    }

    /**
     * @param string $jm
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setJm($jm)
    {
      $this->jm = $jm;
      return $this;
    }

    /**
     * @return float
     */
    public function getCena_detaliczna_netto()
    {
      return $this->cena_detaliczna_netto;
    }

    /**
     * @param float $cena_detaliczna_netto
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setCena_detaliczna_netto($cena_detaliczna_netto)
    {
      $this->cena_detaliczna_netto = $cena_detaliczna_netto;
      return $this;
    }

    /**
     * @return float
     */
    public function getCena_detaliczna_brutto()
    {
      return $this->cena_detaliczna_brutto;
    }

    /**
     * @param float $cena_detaliczna_brutto
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setCena_detaliczna_brutto($cena_detaliczna_brutto)
    {
      $this->cena_detaliczna_brutto = $cena_detaliczna_brutto;
      return $this;
    }

    /**
     * @return string
     */
    public function getVat()
    {
      return $this->vat;
    }

    /**
     * @param string $vat
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setVat($vat)
    {
      $this->vat = $vat;
      return $this;
    }

    /**
     * @return string
     */
    public function getPKWiU()
    {
      return $this->PKWiU;
    }

    /**
     * @param string $PKWiU
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setPKWiU($PKWiU)
    {
      $this->PKWiU = $PKWiU;
      return $this;
    }

    /**
     * @return string
     */
    public function getGlowny_ISBN()
    {
      return $this->glowny_ISBN;
    }

    /**
     * @param string $glowny_ISBN
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setGlowny_ISBN($glowny_ISBN)
    {
      $this->glowny_ISBN = $glowny_ISBN;
      return $this;
    }

    /**
     * @return string
     */
    public function getISBN_13()
    {
      return $this->ISBN_13;
    }

    /**
     * @param string $ISBN_13
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setISBN_13($ISBN_13)
    {
      $this->ISBN_13 = $ISBN_13;
      return $this;
    }

    /**
     * @return string
     */
    public function getISBN_10()
    {
      return $this->ISBN_10;
    }

    /**
     * @param string $ISBN_10
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setISBN_10($ISBN_10)
    {
      $this->ISBN_10 = $ISBN_10;
      return $this;
    }

    /**
     * @return string
     */
    public function getEAN_13()
    {
      return $this->EAN_13;
    }

    /**
     * @param string $EAN_13
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setEAN_13($EAN_13)
    {
      $this->EAN_13 = $EAN_13;
      return $this;
    }

    /**
     * @return string
     */
    public function getIndeks()
    {
      return $this->indeks;
    }

    /**
     * @param string $indeks
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka
     */
    public function setIndeks($indeks)
    {
      $this->indeks = $indeks;
      return $this;
    }

}
