<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class stringArray
{

    /**
     * @var string[] $item
     */
    protected $item = null;

    
    public function __construct()
    {
    
    }

    /**
     * @return string[]
     */
    public function getItem()
    {
      return $this->item;
    }

    /**
     * @param string[] $item
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\stringArray
     */
    public function setItem(array $item)
    {
      $this->item = $item;
      return $this;
    }

}
