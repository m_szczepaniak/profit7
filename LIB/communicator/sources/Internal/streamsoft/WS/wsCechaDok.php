<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsCechaDok
{

    /**
     * @var string $cecha1
     */
    protected $cecha1 = null;

    /**
     * @var string $cecha2
     */
    protected $cecha2 = null;

    /**
     * @var string $cecha3
     */
    protected $cecha3 = null;

    /**
     * @var string $cecha4
     */
    protected $cecha4 = null;

    /**
     * @var string $cecha5
     */
    protected $cecha5 = null;

    /**
     * @param string $cecha1
     * @param string $cecha2
     * @param string $cecha3
     * @param string $cecha4
     * @param string $cecha5
     */
    public function __construct($cecha1, $cecha2, $cecha3, $cecha4, $cecha5)
    {
      $this->cecha1 = $cecha1;
      $this->cecha2 = $cecha2;
      $this->cecha3 = $cecha3;
      $this->cecha4 = $cecha4;
      $this->cecha5 = $cecha5;
    }

    /**
     * @return string
     */
    public function getCecha1()
    {
      return $this->cecha1;
    }

    /**
     * @param string $cecha1
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok
     */
    public function setCecha1($cecha1)
    {
      $this->cecha1 = $cecha1;
      return $this;
    }

    /**
     * @return string
     */
    public function getCecha2()
    {
      return $this->cecha2;
    }

    /**
     * @param string $cecha2
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok
     */
    public function setCecha2($cecha2)
    {
      $this->cecha2 = $cecha2;
      return $this;
    }

    /**
     * @return string
     */
    public function getCecha3()
    {
      return $this->cecha3;
    }

    /**
     * @param string $cecha3
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok
     */
    public function setCecha3($cecha3)
    {
      $this->cecha3 = $cecha3;
      return $this;
    }

    /**
     * @return string
     */
    public function getCecha4()
    {
      return $this->cecha4;
    }

    /**
     * @param string $cecha4
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok
     */
    public function setCecha4($cecha4)
    {
      $this->cecha4 = $cecha4;
      return $this;
    }

    /**
     * @return string
     */
    public function getCecha5()
    {
      return $this->cecha5;
    }

    /**
     * @param string $cecha5
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok
     */
    public function setCecha5($cecha5)
    {
      $this->cecha5 = $cecha5;
      return $this;
    }

}
