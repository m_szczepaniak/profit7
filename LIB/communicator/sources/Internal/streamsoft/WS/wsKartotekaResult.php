<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsKartotekaResult extends wsResult
{

    /**
     * @var wsKartoteka $value
     */
    protected $value = null;

    /**
     * @param int $errorCode
     * @param string $errorMessage
     * @param wsKartoteka $value
     */
    public function __construct($errorCode, $errorMessage, $value)
    {
      parent::__construct($errorCode, $errorMessage);
      $this->value = $value;
    }

    /**
     * @return wsKartoteka
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param wsKartoteka $value
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsKartotekaResult
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
