<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsGroup
{

    /**
     * @var string $kod_prosty
     */
    protected $kod_prosty = null;

    /**
     * @var string $nazwa_prosta
     */
    protected $nazwa_prosta = null;

    /**
     * @var wsGroup $dziecko
     */
    protected $dziecko = null;

    /**
     * @param string $kod_prosty
     * @param string $nazwa_prosta
     * @param wsGroup $dziecko
     */
    public function __construct($kod_prosty, $nazwa_prosta, $dziecko)
    {
      $this->kod_prosty = $kod_prosty;
      $this->nazwa_prosta = $nazwa_prosta;
      $this->dziecko = $dziecko;
    }

    /**
     * @return string
     */
    public function getKod_prosty()
    {
      return $this->kod_prosty;
    }

    /**
     * @param string $kod_prosty
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsGroup
     */
    public function setKod_prosty($kod_prosty)
    {
      $this->kod_prosty = $kod_prosty;
      return $this;
    }

    /**
     * @return string
     */
    public function getNazwa_prosta()
    {
      return $this->nazwa_prosta;
    }

    /**
     * @param string $nazwa_prosta
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsGroup
     */
    public function setNazwa_prosta($nazwa_prosta)
    {
      $this->nazwa_prosta = $nazwa_prosta;
      return $this;
    }

    /**
     * @return wsGroup
     */
    public function getDziecko()
    {
      return $this->dziecko;
    }

    /**
     * @param wsGroup $dziecko
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsGroup
     */
    public function setDziecko($dziecko)
    {
      $this->dziecko = $dziecko;
      return $this;
    }

}
