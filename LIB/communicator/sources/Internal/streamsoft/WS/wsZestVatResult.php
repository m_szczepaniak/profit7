<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsZestVatResult extends wsResult
{

    /**
     * @var wsZestVat $value
     */
    protected $value = null;

    /**
     * @param int $errorCode
     * @param string $errorMessage
     * @param wsZestVat $value
     */
    public function __construct($errorCode, $errorMessage, $value)
    {
      parent::__construct($errorCode, $errorMessage);
      $this->value = $value;
    }

    /**
     * @return wsZestVat
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param wsZestVat $value
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsZestVatResult
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
