<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsResult
{

    /**
     * @var int $errorCode
     */
    protected $errorCode = null;

    /**
     * @var string $errorMessage
     */
    protected $errorMessage = null;

    /**
     * @param int $errorCode
     * @param string $errorMessage
     */
    public function __construct($errorCode, $errorMessage)
    {
      $this->errorCode = $errorCode;
      $this->errorMessage = $errorMessage;
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
      return $this->errorCode;
    }

    /**
     * @param int $errorCode
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsResult
     */
    public function setErrorCode($errorCode)
    {
      $this->errorCode = $errorCode;
      return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
      return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsResult
     */
    public function setErrorMessage($errorMessage)
    {
      $this->errorMessage = $errorMessage;
      return $this;
    }

}
