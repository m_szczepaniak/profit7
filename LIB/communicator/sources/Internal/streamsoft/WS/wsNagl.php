<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsNagl
{

    /**
     * @var wsKontrah $kontrahent
     */
    protected $kontrahent = null;

    /**
     * @var string $skrDefDok
     */
    protected $skrDefDok = null;

    /**
     * @var string $rodzajDok
     */
    protected $rodzajDok = null;

    /**
     * @var string $numer_dokumentu
     */
    protected $numer_dokumentu = null;

    /**
     * @var \DateTime $data_dokumentu
     */
    protected $data_dokumentu = null;

    /**
     * @var string $sposob_dostawy
     */
    protected $sposob_dostawy = null;

    /**
     * @var string $sposob_platnosci
     */
    protected $sposob_platnosci = null;

    /**
     * @var \DateTime $termin_platnosci
     */
    protected $termin_platnosci = null;

    /**
     * @var string $uwagi
     */
    protected $uwagi = null;

    /**
     * @var string $magazyn
     */
    protected $magazyn = null;

    /**
     * @var wsCechaDok $cechy
     */
    protected $cechy = null;

    /**
     * @param wsKontrah $kontrahent
     * @param string $skrDefDok
     * @param string $rodzajDok
     * @param string $numer_dokumentu
     * @param \DateTime $data_dokumentu
     * @param string $sposob_dostawy
     * @param string $sposob_platnosci
     * @param \DateTime $termin_platnosci
     * @param string $uwagi
     * @param string $magazyn
     * @param wsCechaDok $cechy
     */
    public function __construct($kontrahent, $skrDefDok, $rodzajDok, $numer_dokumentu, \DateTime $data_dokumentu, $sposob_dostawy, $sposob_platnosci, \DateTime $termin_platnosci, $uwagi, $magazyn, $cechy)
    {
      $this->kontrahent = $kontrahent;
      $this->skrDefDok = $skrDefDok;
      $this->rodzajDok = $rodzajDok;
      $this->numer_dokumentu = $numer_dokumentu;
      $this->data_dokumentu = $data_dokumentu->format(\DateTime::ATOM);
      $this->sposob_dostawy = $sposob_dostawy;
      $this->sposob_platnosci = $sposob_platnosci;
      $this->termin_platnosci = $termin_platnosci->format(\DateTime::ATOM);
      $this->uwagi = $uwagi;
      $this->magazyn = $magazyn;
      $this->cechy = $cechy;
    }

    /**
     * @return wsKontrah
     */
    public function getKontrahent()
    {
      return $this->kontrahent;
    }

    /**
     * @param wsKontrah $kontrahent
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setKontrahent($kontrahent)
    {
      $this->kontrahent = $kontrahent;
      return $this;
    }

    /**
     * @return string
     */
    public function getSkrDefDok()
    {
      return $this->skrDefDok;
    }

    /**
     * @param string $skrDefDok
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setSkrDefDok($skrDefDok)
    {
      $this->skrDefDok = $skrDefDok;
      return $this;
    }

    /**
     * @return string
     */
    public function getRodzajDok()
    {
      return $this->rodzajDok;
    }

    /**
     * @param string $rodzajDok
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setRodzajDok($rodzajDok)
    {
      $this->rodzajDok = $rodzajDok;
      return $this;
    }

    /**
     * @return string
     */
    public function getNumer_dokumentu()
    {
      return $this->numer_dokumentu;
    }

    /**
     * @param string $numer_dokumentu
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setNumer_dokumentu($numer_dokumentu)
    {
      $this->numer_dokumentu = $numer_dokumentu;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getData_dokumentu()
    {
      if ($this->data_dokumentu == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->data_dokumentu);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $data_dokumentu
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setData_dokumentu(\DateTime $data_dokumentu)
    {
      $this->data_dokumentu = $data_dokumentu->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getSposob_dostawy()
    {
      return $this->sposob_dostawy;
    }

    /**
     * @param string $sposob_dostawy
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setSposob_dostawy($sposob_dostawy)
    {
      $this->sposob_dostawy = $sposob_dostawy;
      return $this;
    }

    /**
     * @return string
     */
    public function getSposob_platnosci()
    {
      return $this->sposob_platnosci;
    }

    /**
     * @param string $sposob_platnosci
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setSposob_platnosci($sposob_platnosci)
    {
      $this->sposob_platnosci = $sposob_platnosci;
      return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTermin_platnosci()
    {
      if ($this->termin_platnosci == null) {
        return null;
      } else {
        try {
          return new \DateTime($this->termin_platnosci);
        } catch (\Exception $e) {
          return false;
        }
      }
    }

    /**
     * @param \DateTime $termin_platnosci
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setTermin_platnosci(\DateTime $termin_platnosci)
    {
      $this->termin_platnosci = $termin_platnosci->format(\DateTime::ATOM);
      return $this;
    }

    /**
     * @return string
     */
    public function getUwagi()
    {
      return $this->uwagi;
    }

    /**
     * @param string $uwagi
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setUwagi($uwagi)
    {
      $this->uwagi = $uwagi;
      return $this;
    }

    /**
     * @return string
     */
    public function getMagazyn()
    {
      return $this->magazyn;
    }

    /**
     * @param string $magazyn
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setMagazyn($magazyn)
    {
      $this->magazyn = $magazyn;
      return $this;
    }

    /**
     * @return wsCechaDok
     */
    public function getCechy()
    {
      return $this->cechy;
    }

    /**
     * @param wsCechaDok $cechy
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsNagl
     */
    public function setCechy($cechy)
    {
      $this->cechy = $cechy;
      return $this;
    }

}
