<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsRezerwacjeResult extends wsResult
{

    /**
     * @var wsRezerwacja[] $value
     */
    protected $value = null;

    /**
     * @param int $errorCode
     * @param string $errorMessage
     */
    public function __construct($errorCode, $errorMessage)
    {
      parent::__construct($errorCode, $errorMessage);
    }

    /**
     * @return wsRezerwacja[]
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param wsRezerwacja[] $value
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacjeResult
     */
    public function setValue(array $value)
    {
      $this->value = $value;
      return $this;
    }

}
