<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsZestVat
{

    /**
     * @var float $wart_netto
     */
    protected $wart_netto = null;

    /**
     * @var float $wart_brutto
     */
    protected $wart_brutto = null;

    /**
     * @var float $wart_vat
     */
    protected $wart_vat = null;

    /**
     * @param float $wart_netto
     * @param float $wart_brutto
     * @param float $wart_vat
     */
    public function __construct($wart_netto, $wart_brutto, $wart_vat)
    {
      $this->wart_netto = $wart_netto;
      $this->wart_brutto = $wart_brutto;
      $this->wart_vat = $wart_vat;
    }

    /**
     * @return float
     */
    public function getWart_netto()
    {
      return $this->wart_netto;
    }

    /**
     * @param float $wart_netto
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsZestVat
     */
    public function setWart_netto($wart_netto)
    {
      $this->wart_netto = $wart_netto;
      return $this;
    }

    /**
     * @return float
     */
    public function getWart_brutto()
    {
      return $this->wart_brutto;
    }

    /**
     * @param float $wart_brutto
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsZestVat
     */
    public function setWart_brutto($wart_brutto)
    {
      $this->wart_brutto = $wart_brutto;
      return $this;
    }

    /**
     * @return float
     */
    public function getWart_vat()
    {
      return $this->wart_vat;
    }

    /**
     * @param float $wart_vat
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsZestVat
     */
    public function setWart_vat($wart_vat)
    {
      $this->wart_vat = $wart_vat;
      return $this;
    }

}
