<?php


 function autoload_b70d4fc578542a980b42e32c4ffef9c7($class)
{
    $classes = array(
        'LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService' => __DIR__ .'/ProfitServerImplService.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsResult' => __DIR__ .'/wsResult.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacjeResult' => __DIR__ .'/wsRezerwacjeResult.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja' => __DIR__ .'/wsRezerwacja.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka' => __DIR__ .'/wsKartoteka.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart' => __DIR__ .'/wsCechaKart.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsGroup' => __DIR__ .'/wsGroup.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsIntegerTableResult' => __DIR__ .'/wsIntegerTableResult.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsStringResult' => __DIR__ .'/wsStringResult.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsIntegerResult' => __DIR__ .'/wsIntegerResult.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsStringTableResult' => __DIR__ .'/wsStringTableResult.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsZestVatResult' => __DIR__ .'/wsZestVatResult.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsZestVat' => __DIR__ .'/wsZestVat.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsNagl' => __DIR__ .'/wsNagl.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah' => __DIR__ .'/wsKontrah.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok' => __DIR__ .'/wsCechaDok.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsPoz' => __DIR__ .'/wsPoz.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsKontrahResult' => __DIR__ .'/wsKontrahResult.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsKartotekaResult' => __DIR__ .'/wsKartotekaResult.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsKartotekaArray' => __DIR__ .'/wsKartotekaArray.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\wsPozArray' => __DIR__ .'/wsPozArray.php',
        'LIB\communicator\sources\Internal\streamsoft\WS\stringArray' => __DIR__ .'/stringArray.php'
    );
    if (!empty($classes[$class])) {
        include $classes[$class];
    };
}

spl_autoload_register('autoload_b70d4fc578542a980b42e32c4ffef9c7');

// Do nothing. The rest is just leftovers from the code generation.
{
}
