<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsIntegerResult extends wsResult
{

    /**
     * @var int $value
     */
    protected $value = null;

    /**
     * @param int $errorCode
     * @param string $errorMessage
     * @param int $value
     */
    public function __construct($errorCode, $errorMessage, $value)
    {
      parent::__construct($errorCode, $errorMessage);
      $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param int $value
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsIntegerResult
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
