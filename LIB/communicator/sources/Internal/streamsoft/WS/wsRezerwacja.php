<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsRezerwacja
{

    /**
     * @var string $pozNazwa
     */
    protected $pozNazwa = null;

    /**
     * @var string $indeks
     */
    protected $indeks = null;

    /**
     * @var int $lp
     */
    protected $lp = null;

    /**
     * @var int $ilosc
     */
    protected $ilosc = null;

    /**
     * @var int $iloscrez
     */
    protected $iloscrez = null;

    /**
     * @param string $pozNazwa
     * @param string $indeks
     * @param int $lp
     * @param int $ilosc
     * @param int $iloscrez
     */
    public function __construct($pozNazwa, $indeks, $lp, $ilosc, $iloscrez)
    {
        $this->pozNazwa = $pozNazwa;
        $this->indeks = $indeks;
        $this->lp = $lp;
        $this->ilosc = $ilosc;
        $this->iloscrez = $iloscrez;
    }

    /**
     * @return string
     */
    public function getPozNazwa()
    {
        return $this->pozNazwa;
    }

    /**
     * @param string $pozNazwa
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja
     */
    public function setPozNazwa($pozNazwa)
    {
        $this->pozNazwa = $pozNazwa;
        return $this;
    }

    /**
     * @return string
     */
    public function getIndeks()
    {
        return $this->indeks;
    }

    /**
     * @param string $indeks
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja
     */
    public function setIndeks($indeks)
    {
        $this->indeks = $indeks;
        return $this;
    }

    /**
     * @return int
     */
    public function getLp()
    {
        return $this->lp;
    }

    /**
     * @param int $lp
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja
     */
    public function setLp($lp)
    {
        $this->lp = $lp;
        return $this;
    }

    /**
     * @return int
     */
    public function getIlosc()
    {
        return $this->ilosc;
    }

    /**
     * @param int $ilosc
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja
     */
    public function setIlosc($ilosc)
    {
        $this->ilosc = $ilosc;
        return $this;
    }

    /**
     * @return int
     */
    public function getIloscrez()
    {
        return $this->iloscrez;
    }

    /**
     * @param int $iloscrez
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja
     */
    public function setIloscrez($iloscrez)
    {
        $this->iloscrez = $iloscrez;
        return $this;
    }

}
