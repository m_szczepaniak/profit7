<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsStringTableResult extends wsResult
{

    /**
     * @var string[] $value
     */
    protected $value = null;

    /**
     * @param int $errorCode
     * @param string $errorMessage
     */
    public function __construct($errorCode, $errorMessage)
    {
      parent::__construct($errorCode, $errorMessage);
    }

    /**
     * @return string[]
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param string[] $value
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsStringTableResult
     */
    public function setValue(array $value)
    {
      $this->value = $value;
      return $this;
    }

}
