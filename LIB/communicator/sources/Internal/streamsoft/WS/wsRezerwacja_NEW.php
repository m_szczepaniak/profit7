<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsRezerwacja
{

    /**
     * @var string $pozNazwa
     */
    protected $pozNazwa = null;


    /**
     * @var int $lp
     */
    protected $lp = null;

    /**
     * @var int $ilosc
     */
    protected $ilosc = null;

    /**
     * @var int $iloscrez
     */
    protected $iloscrez = null;

    protected $id_dostawa = null;

    protected $indeks = null;

    /**
     * @param string $pozNazwa
     * @param string $indeks
     * @param int $lp
     * @param int $ilosc
     * @param int $iloscrez
     */
    public function __construct($pozNazwa, $indeks, $lp, $ilosc, $iloscrez)
    {
      $this->pozNazwa = $pozNazwa;
      $this->indeks = $indeks;
      $this->lp = $lp;
      $this->ilosc = $ilosc;
      $this->iloscrez = $iloscrez;
    }

    /**
     * @return null|string
     */
    public function getIndeks() {
        return $this->indeks;
    }

    /**
     * @return string
     */
    public function getPozNazwa()
    {
      return $this->pozNazwa;
    }

    /**
     * @param string $pozNazwa
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja
     */
    public function setPozNazwa($pozNazwa)
    {
      $this->pozNazwa = $pozNazwa;
      return $this;
    }


    /**
     * @return int
     */
    public function getLp()
    {
      return $this->lp;
    }

    /**
     * @param int $lp
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja
     */
    public function setLp($lp)
    {
      $this->lp = $lp;
      return $this;
    }

    /**
     * @return int
     */
    public function getIlosc()
    {
      return $this->ilosc;
    }

    /**
     * @param int $ilosc
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja
     */
    public function setIlosc($ilosc)
    {
      $this->ilosc = $ilosc;
      return $this;
    }

    /**
     * @return int
     */
    public function getIloscrez()
    {
      return $this->iloscrez;
    }

    /**
     * @param int $iloscrez
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja
     */
    public function setIloscrez($iloscrez)
    {
      $this->iloscrez = $iloscrez;
      return $this;
    }

    /**
     * @return null
     */
    public function getIdDostawa()
    {
        return $this->id_dostawa;
    }

    /**
     * @param null $id_dostawa
     */
    public function setIdDostawa($id_dostawa)
    {
        $this->id_dostawa = $id_dostawa;
    }

}
