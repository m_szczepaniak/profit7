<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class wsStringResult extends wsResult
{

    /**
     * @var string $value
     */
    protected $value = null;

    /**
     * @param int $errorCode
     * @param string $errorMessage
     * @param string $value
     */
    public function __construct($errorCode, $errorMessage, $value)
    {
      parent::__construct($errorCode, $errorMessage);
      $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue()
    {
      return $this->value;
    }

    /**
     * @param string $value
     * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsStringResult
     */
    public function setValue($value)
    {
      $this->value = $value;
      return $this;
    }

}
