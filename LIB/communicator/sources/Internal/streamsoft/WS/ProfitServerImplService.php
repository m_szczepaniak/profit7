<?php

namespace LIB\communicator\sources\Internal\streamsoft\WS;

class ProfitServerImplService extends \SoapClient
{

    private static $allMagazinesCodes = [
        1 => '02',
        0 => '03',
        10 => '07'
    ];

    /**
     * @var array $classmap The defined classes
     */
    private static $classmap = array (
  'wsResult' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsResult',
  'wsRezerwacjeResult' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsRezerwacjeResult',
  'wsRezerwacja' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsRezerwacja',
  'wsKartoteka' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsKartoteka',
  'wsCechaKart' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsCechaKart',
  'wsGroup' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsGroup',
  'wsIntegerTableResult' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsIntegerTableResult',
  'wsStringResult' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsStringResult',
  'wsIntegerResult' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsIntegerResult',
  'wsStringTableResult' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsStringTableResult',
  'wsZestVatResult' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsZestVatResult',
  'wsZestVat' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsZestVat',
  'wsNagl' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsNagl',
  'wsKontrah' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsKontrah',
  'wsCechaDok' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsCechaDok',
  'wsPoz' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsPoz',
  'wsKontrahResult' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsKontrahResult',
  'wsKartotekaResult' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsKartotekaResult',
  'wsKartotekaArray' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsKartotekaArray',
  'wsPozArray' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\wsPozArray',
  'stringArray' => 'LIB\\communicator\\sources\\Internal\\streamsoft\\WS\\stringArray',
);

    public static function getMagazineCodeBySource($source)
    {
        $magazineCode = self::$allMagazinesCodes[$source];

        if(null === $magazineCode){
            return 01;
        }

        return $magazineCode;
    }

    /**
     * @param array $options A array of config values
     * @param string $wsdl The wsdl file to use
     */
    public function __construct(array $options = array(), $wsdl = null)
    {
        global $aConfig;
        if (null === $wsdl) {
            if ($aConfig['common']['status'] != 'development') {
                $wsdl = 'http://213.216.85.50:3030/WebServicePro/ServerProfit?wsdl';
            } else {
                $wsdl = 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl';
            }
        }
        foreach (self::$classmap as $key => $value) {
            if (!isset($options['classmap'][$key])) {
                $options['classmap'][$key] = $value;
            }
        }

        if (isset($options['force_socket_timeout'])) {
            ini_set("default_socket_timeout", $options['force_socket_timeout']);
            $options['TIMEOUT'] = $options['force_socket_timeout'];
            $options['timeout'] = $options['force_socket_timeout'];
            $options['connection_timeout'] = $options['force_socket_timeout'];
            unset($options['force_socket_timeout']);
        } else {
            ini_set("default_socket_timeout", 200);
            $options['TIMEOUT'] = '200';
            $options['timeout'] = '200';
            $options['connection_timeout'] = 200;
        }
        $options = array_merge(array(
            'features' => 1,
        ), $options);

        parent::__construct($wsdl, $options);
    }

    /**
     *
     * @param int $iWebsiteId
     * @return string
     */
    public function getKodUrzZewnKS($iWebsiteId) {

        switch ($iWebsiteId) {
            case '1':
                return '02';
            case '3':
                return '03';
            case '2':
                return '04';
            case '4':
                return '10';
            case '5':
                return '11';
            case '8':
                return '15';
        }
    }

    /**
     * @return string
     */
    public function getmicrotime()
    {
        $microtime = explode(' ', microtime());
        return $microtime[1] . substr($microtime[0], 1);
    }

    /**
     * @param string $function_name
     * @param array $arguments
     * @param array|null $options
     * @param null $input_headers
     * @param array|null $output_headers
     * @return mixed
     */
    public function __soapCall($function_name, array $arguments, array $options = null, $input_headers = null, array &$output_headers = null) {

//      ini_set("default_socket_timeout", 30);

      $sBuff = print_r($function_name, true)." \n\n ".
              print_r($arguments, true)." \n\n ".
              print_r($options, true)." \n\n ".
              print_r($input_headers, true)." \n\n ".
              print_r($output_headers, true);

        $sLogFilenameee = __DIR__.'/logs/no_response_'.date('YmdHisu').'_'.$function_name.'.log';
//        file_put_contents($sLogFilenameee, $sBuff, FILE_APPEND | LOCK_EX);

        $time_start = $this->getmicrotime();
      $response = parent::__soapCall($function_name, $arguments, $options, $input_headers, $output_headers);
        $time_stop = $this->getmicrotime();
		
        $exTime = round($time_stop - $time_start, 5);

      $sLogFilename = __DIR__.'/logs/'. $exTime .'_'.date('YmdHisu').'_'.$function_name.'.log';

//      file_put_contents($sLogFilename, $sBuff.print_r($response, true), FILE_APPEND | LOCK_EX);
      @unlink($sLogFilenameee);
      return $response;
    }

    /**
     * @param string $sesja
     * @param wsKartoteka $kart
     * @return wsIntegerResult
     */
    public function aktualizujKartoteke($sesja, wsKartoteka $kart)
    {
      return $this->__soapCall('aktualizujKartoteke', array($sesja, $kart));
    }

    /**
     * @param string $sesja
     * @param string $indeks
     * @param string $nazwa_pliku_z_rozszerzeniem
     * @param string $okladkaB64
     * @return wsResult
     */
    public function wprowadzOkladke($sesja, $indeks, $nazwa_pliku_z_rozszerzeniem, $okladkaB64)
    {
      return $this->__soapCall('wprowadzOkladke', array($sesja, $indeks, $nazwa_pliku_z_rozszerzeniem, $okladkaB64));
    }

    /**
     * @param string $sesja
     * @param wsNagl $naglowek
     * @param wsPozArray $pozycje
     * @param string $kod_urzzew
     * @return wsIntegerResult
     */
    public function utworzDokument($sesja, wsNagl $naglowek, wsPozArray $pozycje, $kod_urzzew)
    {
      return $this->__soapCall('utworzDokument', array($sesja, $naglowek, $pozycje, $kod_urzzew));
    }

    /**
     * @param string $sesja
     * @param string $numer_zew_dok_cms
     * @return wsIntegerResult
     */
    public function akceptujDokument($sesja, $numer_zew_dok_cms)
    {
      return $this->__soapCall('akceptujDokument', array($sesja, $numer_zew_dok_cms));
    }

    /**
     * @param string $sesja
     * @param string $indeks
     * @return wsKartotekaResult
     */
    public function sprawdzKartoteke($sesja, $indeks)
    {
      return $this->__soapCall('sprawdzKartoteke', array($sesja, $indeks));
    }

    /**
     * @param string $sesja
     * @param string $numer_zew_dok_cms
     * @return wsZestVatResult
     */
    public function pobierzZestVat($sesja, $numer_zew_dok_cms)
    {
      return $this->__soapCall('pobierzZestVat', array($sesja, $numer_zew_dok_cms));
    }

    /**
     * @param string $sesja
     * @param wsKontrah $kontrah
     * @return wsKontrahResult
     */
    public function sprawdzDodajKontrah($sesja, wsKontrah $kontrah)
    {
      return $this->__soapCall('sprawdzDodajKontrah', array($sesja, $kontrah));
    }

    /**
     * @param string $sesja
     * @param stringArray $tablica_indeksow
     * @return wsStringTableResult
     */
    public function sprawdzGrupeKartotek($sesja, stringArray $tablica_indeksow)
    {
      return $this->__soapCall('sprawdzGrupeKartotek', array($sesja, $tablica_indeksow));
    }

    /**
     * @param string $sesja
     * @param wsKartotekaArray $kart
     * @return wsIntegerTableResult
     */
    public function dodajWieleKartotek($sesja, wsKartotekaArray $kart)
    {
      return $this->__soapCall('dodajWieleKartotek', array($sesja, $kart));
    }

    /**
     * @param string $sesja
     * @param wsNagl $naglowek
     * @param wsPozArray $pozycje
     * @param string $kod_urzzew
     * @return wsIntegerResult
     */
    public function dodajLubAktualizujZamowienie($sesja, wsNagl $naglowek, wsPozArray $pozycje, $kod_urzzew)
    {
        ini_set("default_socket_timeout", 10);
      return $this->__soapCall('dodajLubAktualizujZamowienie', array($sesja, $naglowek, $pozycje, $kod_urzzew));
    }

    /**
     * @param string $sesja
     * @param string $numer_zew_dok_cms
     * @return wsRezerwacjeResult
     */
    public function pokazRezerwacje($sesja, $numer_zew_dok_cms)
    {
        ini_set("default_socket_timeout", 20);
      return $this->__soapCall('pokazRezerwacje', array($sesja, $numer_zew_dok_cms));
    }

    /**
     * @param string $sesja
     * @param wsKartoteka $kart
     * @return wsIntegerResult
     */
    public function utworzKartoteke($sesja, wsKartoteka $kart)
    {
      return $this->__soapCall('utworzKartoteke', array($sesja, $kart));
    }

    /**
     * @param string $login
     * @param string $haslo
     * @return wsStringResult
     */
    public function utworzSesje($login, $haslo)
    {
      return $this->__soapCall('utworzSesje', array($login, $haslo));
    }

    /**
     * @param string $sesja
     * @return wsResult
     */
    public function zamknijSesje($sesja)
    {
      return $this->__soapCall('zamknijSesje', array($sesja));
    }

    /**
     * @param string $sesja
     * @param string $numer_zew_zam_cms
     * @return wsIntegerResult
     */
    public function zalozRezerwacje($sesja, $numer_zew_zam_cms)
    {
      return $this->__soapCall('zalozRezerwacje', array($sesja, $numer_zew_zam_cms));
    }

    /**
     * @param string $sesja
     * @param string $numer_dok_urzzewnagl
     * @return wsResult
     */
    public function anulujZamowienie($sesja, $numer_dok_urzzewnagl)
    {
      return $this->__soapCall('anulujZamowienie', array($sesja, $numer_dok_urzzewnagl));
    }

    /**
     * @param $sesja
     * @param wsDokBank[] $WsDokBank
     * @return mixed
     */
    public function dodajDokumentyBankowe($sesja, $wsDokBank = array())
    {
		ini_set("default_socket_timeout", 1000);
        return $this->__soapCall('dodajDokumentyBankowe', array($sesja, $wsDokBank));
    }

}
