<?php

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use LIB\Helpers\ArrayHelper;
use LIB\Helpers\DbHelper;

class DataProvider
{

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;

  public function __construct(DatabaseManager $pDbMgr)
  {
    $this->pDbMgr = $pDbMgr;
  }


    /**
     * @param $iProductId
     * @return bool
     */
  public function checkProductInStreamsoft($iProductId) {

      if ($iProductId > 0) {
          $sSql = 'SELECT id 
                   FROM products_stock
                   WHERE id = '.$iProductId.'
                   AND 
                   (
                   profit_g_last_import IS NOT NULL OR
                   profit_e_last_import IS NOT NULL OR
                   profit_j_last_import IS NOT NULL
                   )
                   ';
          if ($this->pDbMgr->GetOne('profit24', $sSql) > 0) {
              return true;
          } else {
              return false;
          }
      }
      return false;
  }
  /**
   * @param $iOSHId
   * @return array
   */
  public function getItems($iOSHId) {

    $sSql = 'SELECT OSHI.*,
              SUM(OSHI.in_confirmed_quantity) AS in_confirmed_quantity,
              P.id as product_id,
              P.streamsoft_indeks
             FROM orders_send_history_items AS OSHI
             JOIN orders_items AS OI
              ON OI.id = OSHI.item_id
             JOIN products AS P
              ON P.id = OI.product_id
             WHERE OSHI.send_history_id = '.$iOSHId.'
             GROUP BY P.id
             ORDER BY OI.name ASC
             ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

  /**
   * 
   * @param int $iOSHId
   * @return array
   */
  public function getItemsByProductId($iOSHId) {

    $sSql = 'SELECT OSHI.*,
              SUM(OSHI.in_confirmed_quantity) AS in_confirmed_quantity,
              P.id as product_id,
              P.streamsoft_indeks
             FROM orders_send_history_items AS OSHI
             JOIN products AS P
              ON P.id = OSHI.product_id
             WHERE OSHI.send_history_id = '.$iOSHId.'
             GROUP BY P.id
             ORDER BY P.name ASC
             ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
     * @param $iOid
     * @param bool $onlyStock
     * @param bool $onlyReservations
     * @return array
     */
  public function getOrderItems($iOid, $onlyStock = false, $onlyReservations = false) {

    $extraSql = '';
    if (true == $onlyStock){
        $extraSql = ' AND OI.source = 51 ';
    } elseif(true == $onlyReservations) {
        $extraSql = " AND(((OI.source = 1 OR OI.source = 0) AND OI.`status` != '4') OR OI.source = 51 ) ";
    }

    $sSql = "
              SELECT OI.*, P.id as product_id, P.streamsoft_indeks
             FROM orders_items AS OI
             JOIN products AS P
              ON P.id = OI.product_id
             WHERE OI.order_id = $iOid $extraSql
             AND OI.packet = '0'
             AND OI.deleted = '0'

             ORDER BY OI.name ASC
             ";

      //GROUP BY P.id
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

  /**
   *
   * @param int $iOSHId
   * @return array
   */
  public function getOSHI($iOSHId) {

    $sSql = 'SELECT OSH.*, OSHA.fv_nr, EP.streamsoft_id
              FROM orders_send_history AS OSH
              JOIN orders_send_history_attributes AS OSHA
                      ON OSHA.orders_send_history_id = OSH.id
              JOIN external_providers AS EP
                ON EP.id = OSH.source
              WHERE OSH.id = '.$iOSHId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }

    public function getStreamsoftOrderNumber($orderId)
    {
        $sSql = "SELECT id, streamsoft_order_number FROM orders WHERE id = $orderId";

        return $this->pDbMgr->GetRow('profit24', $sSql);
    }

  /**
   *
   * @param int $iOSHId
   * @return array
   */
  public function getOrder($iOid, $columns, $onlyOrderData = false, $streamsoftOrder = false) {

    $implodedColumns = implode(',', $columns);

      $sSql =  'SELECT '.$implodedColumns.'
             FROM orders AS O
             JOIN orders_items AS OI
              ON OI.order_id = O.id
             JOIN products AS P
              ON P.id = OI.product_id
             WHERE O.id = '.$iOid;

      if (true == $onlyOrderData) {
          $sSql =  'SELECT '.$implodedColumns.'
             FROM orders AS O
             WHERE O.id = '.$iOid;
      }

      $result = $this->pDbMgr->GetRow('profit24', $sSql);

      if ($streamsoftOrder == true) {
          $orderNumber = $result['order_number'];
          if (null != $result['order_number_iteration']){
              $orderNumber = $orderNumber.''.$result['order_number_iteration'];
          }
          $result['streamsoft_order_number'] = $orderNumber;
      }

      return $result;
  }

  public function getAllOrdersIdsToRecreateReservations()
  {
      $sSql = "
            SELECT
            DISTINCT O.id, O.order_status
            FROM orders as O
            JOIN orders_items AS OI
            ON OI.order_id = O.id
            AND (OI.status = '3' OR OI.status = '4')
            AND (OI.source = '51' OR OI.source = '0' OR OI.source = '1')
            AND OI.deleted = '0'
            AND (OI.item_type = 'I' OR OI.item_type = 'P')
            AND OI.packet = '0'
            WHERE O.order_status != '5'
            AND O.order_status != '4'
            AND O.erp_export IS NULL
      ";

//    $sSql = "SELECT id, order_status from orders WHERE order_status = '3'";
    //$sSql = "SELECT id, order_status from orders WHERE order_status != '5' and order_status != '4' AND erp_export IS NULL";

    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

    public function getReservationsOrdersItemsByOrderId($orderId, $source = false)
    {
//    $sSql = '
//              SELECT
//                PSR.*, PSR.quantity AS in_confirmed_quantity, P.id as product_id, P.streamsoft_indeks, OI.id AS order_item_id
//              FROM products_stock_reservations PSR
//              JOIN orders_items AS OI ON PSR.orders_items_id = OI.id
//              JOIN products AS P ON OI.product_id = P.id
//              WHERE PSR.order_id = '.$orderId.'
//             ';

        $sSql = '
            SELECT
             OI.id AS order_item_id, PSR.source_id           
            FROM products_stock_reservations PSR
            JOIN orders_items AS OI ON PSR.orders_items_id = OI.id
            JOIN products AS P ON OI.product_id = P.id
            WHERE PSR.order_id = '.$orderId.'
             ';

        if ($source != false){
            $sSql .= " AND PSR.source_id = $source";
        }

        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

  public function getReservationsByOrderId($orderId, $source = false)
  {
//    $sSql = '
//              SELECT
//                PSR.*, PSR.quantity AS in_confirmed_quantity, P.id as product_id, P.streamsoft_indeks, OI.id AS order_item_id
//              FROM products_stock_reservations PSR
//              JOIN orders_items AS OI ON PSR.orders_items_id = OI.id
//              JOIN products AS P ON OI.product_id = P.id
//              WHERE PSR.order_id = '.$orderId.'
//             ';

      $sSql = '
            SELECT
             PSR.*, PSR.quantity AS in_confirmed_quantity, P.id as product_id, P.streamsoft_indeks, OI.id AS order_item_id,
             (SELECT group_concat(concat(PSSTR.products_stock_supplies_id,\':\',PSSTR.reservated_quantity) separator \',\') FROM products_stock_supplies_to_reservations AS PSSTR WHERE PSR.id = PSSTR.products_stock_reservations_id) AS supplies
            FROM products_stock_reservations PSR
            JOIN orders_items AS OI ON PSR.orders_items_id = OI.id
            JOIN products AS P ON OI.product_id = P.id
            WHERE PSR.order_id = '.$orderId.'
             ';

    if ($source != false){
        $sSql .= " AND PSR.source_id = $source";
    }

    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

    public function moveOrdersPositionToLines(array $positions)
    {
        foreach($positions as $id => $position) {
            $this->pDbMgr->Update('profit24', "orders_items", ['status' => 0], 'id = '.$id);
        }
    }

    public function removeOrderItems($orderId)
    {
        return $this->pDbMgr->Query('profit24', "DELETE FROM orders_items WHERE order_id = ".$orderId);
    }


    public function getSuppliesByProductIdAndSource($productId, $source, $supplyIds = null)
    {

        if (null != $supplyIds){
            $implodedIds = implode(',', $supplyIds);
            $sql = "SELECT * FROM products_stock_supplies WHERE id IN($implodedIds) ORDER BY wholesale_price ASC";
            $result = $this->pDbMgr->GetAll('profit24', $sql);
        } else {
            $sql = "SELECT * FROM products_stock_supplies WHERE source = %s AND product_id = %d ORDER BY wholesale_price ASC";
            $result = $this->pDbMgr->GetAll('profit24', sprintf($sql, $source, $productId));
        }

        if (empty($result)){
            return [];
        }

        $newSupplies = [];

        foreach($result as $supply){
            if(($supply['quantity'] - $supply['reservation']) < 0){
                $supply['available_quantity'] = 0;
            } else {
                $supply['available_quantity'] = $supply['quantity'] - $supply['reservation'];
            }

            $newSupplies[] = $supply;
        }

        return $newSupplies;
    }

    public function updateProductStockSupply($productStockSupply, $columns)
    {
        $values = [];

        foreach($columns as $column) {
            $values[$column] = $productStockSupply[$column];
        }

        $this->pDbMgr->Update('profit24', "products_stock_supplies", $values, 'id = '.$productStockSupply['id']);
    }

    public function getProductStockDataReservationByResId($reservationId, $supplyId)
    {
        $sql = "SELECT * FROM products_stock_supplies_to_reservations WHERE products_stock_reservations_id = %d AND products_stock_supplies_id = $supplyId";

        return $this->pDbMgr->GetRow('profit24', sprintf($sql, $reservationId));
    }



    public function getReservationsToSupplies($productId)
    {
        $sql = "
          SELECT PSR.*
          FROM products_stock_reservations AS PSR
          WHERE PSR.products_stock_id = $productId
        ";

        return $this->pDbMgr->GetAll('profit24', $sql);
    }

    public function getProductStockDataResByResId($orderId)
    {
        $sql = "
          SELECT PSSTR.*
          FROM products_stock_supplies_to_reservations AS PSSTR
          JOIN products_stock_reservations AS PSR on PSR.id = PSSTR.products_stock_reservations_id
          WHERE PSR.order_id = $orderId
        ";

        return $this->pDbMgr->GetAll('profit24', $sql);
    }

    public function findOrderByOshi($oshi)
    {
        $sql = "
          SELECT O.id
          FROM orders_send_history_items AS OSHI
          JOIN orders_items AS OI ON OI.id = OSHI.item_id
          JOIN orders AS O ON OI.order_id = O.id
          WHERE OSHI.send_history_id = $oshi
        ";

        $result = $this->pDbMgr->GetAll('profit24', $sql);

        return $result;
    }

    public function findOrdersByOshi($oshiId)
    {
        $sql = "
            SELECT DISTINCT O.id
            FROM orders_send_history_items AS OSHI
            JOIN orders_items AS OI ON OI.id = OSHI.item_id
            JOIN orders AS O ON OI.order_id = O.id
            WHERE OSHI.send_history_id = $oshiId
        ";

        return $this->pDbMgr->GetCol('profit24', $sql);
    }

    public function insertProdutStockSupplyReservation($reservationId, $supplyId, $reservated)
    {
        $values = [
            'products_stock_reservations_id' => $reservationId,
            'products_stock_supplies_id' => $supplyId,
            'reservated_quantity' => $reservated,
        ];

        return $this->pDbMgr->Insert('profit24', 'products_stock_supplies_to_reservations', $values);
    }

    /**
     * @param string $productIds
     *
     * @return array
     */
    public function getProductsWholesalePrice($productIds)
    {
        $sql = "SELECT id, wholesale_price FROM products WHERE id IN(%s)";

        return $this->pDbMgr->GetAssoc('profit24', sprintf($sql, $productIds));
    }

    public function getProductStockReservationById($id)
    {
        $sql = "SELECT * FROM products_stock_reservations WHERE id = %d";

        return $this->pDbMgr->GetRow('profit24', sprintf($sql, $id));
    }

    /**
     * @param $id
     * @return array
     */
    public function getProductStockById($id)
    {
        $sql = "SELECT * FROM products_stock WHERE id = %d";

        return $this->pDbMgr->GetRow('profit24', sprintf($sql, $id));
    }

    /**
     * @return array
     */
    public function getMappedSources()
    {
        $sql = "SELECT provider_id, column_prefix_products_stock FROM sources";

        return $this->pDbMgr->GetAssoc('profit24', $sql);
    }
    
    /**
     * @return array
     */
    public function getMappedSourceByStockPrefix()
    {
        $sql = "SELECT column_prefix_products_stock, id FROM sources";

        return $this->pDbMgr->GetAssoc('profit24', $sql);
    }

    public function reserveAllProductsSourceSupply($source, $productId) {

        $sql = 'UPDATE products_stock_supplies
                 SET reservation = quantity
                 WHERE product_id = '.$productId. ' AND source = "'.$source.'"';
        return $this->pDbMgr->Query('profit24', $sql);
    }

    public function updateProductStock($productStock, $id)
    {
        $this->pDbMgr->Update('profit24', "products_stock", $productStock, 'id = '.$id);
    }

    public function deleteProductStockSuppliesToResByResId($reservationId)
    {
        $sql = "DELETE FROM products_stock_supplies_to_reservations WHERE products_stock_reservations_id = %d";

        return $this->pDbMgr->Query('profit24', sprintf($sql, $reservationId));
    }

    public function deleteReservationByOrderId($orderId)
    {
        return $this->pDbMgr->Query('profit24', "DELETE FROM products_stock_reservations WHERE order_id = ".$orderId);
    }

    public function deleteReservationByOrderIdAndItems($orderId, $resItems)
    {
        $items = implode(',', $resItems);

        return $this->pDbMgr->Query('profit24', "DELETE FROM products_stock_reservations WHERE order_id = $orderId AND id IN($items)");
    }

    public function deleteReservationByOrderIds(array $orderIds)
    {
        implode(',',$orderIds);

        return $this->pDbMgr->Query('profit24', "DELETE FROM products_stock_reservations WHERE order_id IN()");
    }

    public function deleteReservationByProductIdIn($productsIds)
    {
        return $this->pDbMgr->Query('profit24', "DELETE FROM products_stock_reservations WHERE products_stock_id IN ($productsIds)");
    }

    public function deleteAllReservation()
    {
        return $this->pDbMgr->Query('profit24', "DELETE FROM products_stock_reservations");
    }

    public function resetProductStockByProductsIdsIn($productsIds)
    {
        $sql = "update products_stock set profit_g_status = 0, profit_g_act_stock = 0, profit_g_reservations = 0, profit_e_status = 0, profit_e_act_stock = 0, profit_e_reservations = 0, profit_j_status = 0, profit_j_act_stock = 0, profit_j_reservations = 0 WHERE id IN($productsIds)";

        return $this->pDbMgr->Query('profit24', $sql);
    }

    public function updateRservationLp($resId, $lp)
    {
        $sql = "UPDATE products_stock_reservations set streamsoft_lp = '$lp' WHERE id = ".$resId;

        return $this->pDbMgr->Query('profit24', $sql);
    }

    public function clearOrderReservationsLp($orderId)
    {
        $sql = "UPDATE products_stock_reservations set streamsoft_lp = null WHERE order_id = ".$orderId;

        return $this->pDbMgr->Query('profit24', $sql);
    }

    public function getProductStockSuppliesByreservation($resId)
    {
        $sql = "SELECT PSS.id, PSS.reservation_to_reduce
            FROM products_stock_supplies_to_reservations AS PSSTR
            JOIN products_stock_supplies AS PSS ON PSSTR.products_stock_supplies_id = PSS.id
            WHERE PSSTR.products_stock_reservations_id = %d
            ";

        return $this->pDbMgr->GetRow('profit24', sprintf($sql, $resId));
    }
    
    /**
     * 
     * @param int $iSupplyId
     * @param array $aData
     * @param int $iOrderId
     */
    public function insertProductsStockSuppliesDebug($iSupplyId, $aData, $iOrderId = 0) {

      $aData[] = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
      $aValues = [
          'supplies_id' => $iSupplyId,
          'data' => json_encode($aData),
          'order_id' => $iOrderId,
          'created' => 'NOW()',
          'created_by' => isset($_SESSION) ? $_SESSION['user']['name'] : 'NULL'
      ];
      $this->pDbMgr->Insert('profit24', 'products_stock_supplies_debug', $aValues);
    }

    public function updateProductStockResSupplyToReduce($id, $newResToReduce)
    {
        return $this->pDbMgr->Query('profit24', "UPDATE products_stock_supplies set reservation_to_reduce = $newResToReduce WHERE id = $id LIMIT 1");
    }

    public function iterateStreamsoftNumber($orderId)
    {
        $orderIteration = $this->pDbMgr->GetRow('profit24', "SELECT order_number_iteration FROM orders WHERE id = $orderId LIMIT 1");
        $newOrderIteration = $orderIteration['order_number_iteration'] + 1;

        $result = $this->pDbMgr->Query('profit24', "UPDATE orders set order_number_iteration = $newOrderIteration WHERE id = $orderId LIMIT 1");

        if (false == $result || false == $orderIteration){
            throw new \Exception('Blad poczas aktualizacji numeru zamowienia (iteracja)');
        }
    }

    public function resetReservationToReduce($psstrId)
    {
        return $this->pDbMgr->Query('profit24', "UPDATE products_stock_supplies_to_reservations set reservated_quantity = 0 WHERE id = $psstrId LIMIT 1");
    }

    public function getProductsIdsByOrderId($orderId)
    {
        return $this->pDbMgr->GetCol('profit24', "SELECT DISTINCT product_id FROM orders_items where order_id = $orderId");
    }

    public function deleteReservationsByOrderId($orderId)
    {
        return $this->pDbMgr->Query('profit24', "DELETE FROM products_stock_reservations WHERE order_id = $orderId");
    }
    
    public function insertProductsStockReservationDebug($iReservationId, $aData) {
      
      $aValues = array(
          'reservation_id' => $iReservationId,
          'data' => json_encode($aData),
          'created' => 'NOW()',
          'created_by' => $_SESSION['user']['name']
      );
      return $this->pDbMgr->Insert('profit24', 'products_stock_reservations_debug', $aValues);
    }

    public function deleteReservationsForRepair($orderId)
    {
        $sql = "
        DELETE
        FROM products_stock_reservations AS PSR
        JOIN orders_items AS OI ON OI.id = PSR.orders_items_id
        WHERE PSR.order_id = $orderId
        AND (PSR.source_id = 10 OR (PSR.source_id = 0 OR PSR.source_id = 1 AND (OI.status = 3)))
        ";

        return $this->pDbMgr->Query('profit24', $sql);
    }

    public function getOldReservationsToRepair($orderId)
    {
//        $sql = "
//        SELECT PSR.*
//        FROM products_stock_reservations AS PSR
//        JOIN orders_items AS OI ON OI.id = PSR.orders_items_id
//        WHERE PSR.order_id = $orderId
//        AND (PSR.source_id = 10 OR (PSR.source_id = 0 OR PSR.source_id = 1 AND (OI.status = 3)))
//        AND OI.deleted = '0'
//        AND OI.packet = '0'
//        ";
        $sql = "
        SELECT PSR.*, PSR.products_stock_id as product_id, PSR.quantity AS in_confirmed_quantity, P.streamsoft_indeks, OI.id AS order_item_id
        FROM products_stock_reservations AS PSR
        JOIN orders_items AS OI ON OI.id = PSR.orders_items_id
        JOIN products AS P ON OI.product_id = P.id
        LEFT JOIN orders_send_history_items AS OSHI ON OSHI.item_id = OI.id and OI.quantity = OSHI.in_confirmed_quantity
        LEFT JOIN orders_send_history AS OSH ON OSH.id = OSHI.send_history_id
        WHERE PSR.order_id = $orderId
        AND OI.deleted = '0'
        AND OI.packet = '0'
        AND (OI.source = 51 OR (OI.source = 0 OR OI.source = 1 AND (OI.status = 3) AND (OSH.mm_doc = '0' OR OSH.mm_doc IS NULL)))
        ";

        return $this->pDbMgr->GetAll('profit24', $sql);
    }

    public function getOrdersItemsForReparation($orderId)
    {
        $sql = "
        SELECT OI.*
        FROM orders_items AS OI
        LEFT JOIN orders_send_history_items AS OSHI ON OSHI.item_id = OI.id and OI.quantity = OSHI.in_confirmed_quantity
        LEFT JOIN orders_send_history AS OSH ON OSH.id = OSHI.send_history_id
        WHERE OI.order_id = $orderId
        AND (OI.source = 51 OR (OI.source = 0 OR OI.source = 1 AND (OI.status = 3) AND (OSH.mm_doc = '0' OR OSH.mm_doc IS NULL)))AND OI.deleted = '0'
        AND OI.packet = '0'
        ";

        return $this->pDbMgr->GetAll('profit24', $sql);
    }

    public function getReservationsSumBySourceAndProductId($productId, $source)
    {
        $sql = "
        SELECT sum(quantity) as sum FROM products_stock_reservations WHERE products_stock_id = $productId AND source_id = $source
        ";

        return $this->pDbMgr->GetRow('profit24', $sql);
    }

    public function getSuppliesByIdIn($supplyIds)
    {
        $imploded = implode(',', $supplyIds);
        return $this->pDbMgr->GetAll('profit24', "SELECT * FROM products_stock_supplies WHERE id IN($imploded)");
    }

    public function getSupplyByProductIdSourceAndWhPrice($productId, $whPrice, $source)
    {
        $sql = "SELECT * FROM products_stock_supplies WHERE product_id = $productId AND wholesale_price = $whPrice AND source = $source";

        return $this->pDbMgr->GetRow('profit24', $sql);
    }
}
