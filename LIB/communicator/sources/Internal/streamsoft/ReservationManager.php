<?php

namespace LIB\communicator\sources\Internal\streamsoft;

use Items\Confirmer;
use LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacjeResult;
use LIB\Helpers\ArrayHelper;
use LIB\Supplies\ProductStockService;
use LIB\Supplies\StockSuppliceService;
use omniaCMS\lib\Orders\orderItemReservation;
use omniaCMS\lib\Products\ProductsStockReservations;

class ReservationManager extends AbstractErpManager
{
    const NEW_RESERVATION = 1;

    const RECREATE_RESERVATION = 2;

    const SAME_RESERVATION = 3;

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    private $acceptableSources = [10, 0, 1];

    private $reservationResponse = null;

    private $unReservedProducts = [];

    private $isPartial = false;

    private $errorMessage = null;

    private $stockSupplyService;

    private $orderRecount;

    private $currentStreamsoftReservation = null;

    private $oldReservations;

    private $orderData;

    private $creationHasBeenTried;

    public function __construct($pDbMgr)
    {
        include_once('OrderRecount.class.php');
        $this->orderRecount = new \OrderRecount();
        $this->pDbMgr = $pDbMgr;
        $this->productStockReservation = new ProductsStockReservations($this->pDbMgr);
        $this->dataProvider = new DataProvider($this->pDbMgr);
        $this->order = new Order($this->pDbMgr);
        $this->reservation = new Reservation($this->pDbMgr);
        $this->stockSupplyService = new StockSuppliceService($this->pDbMgr);
        $this->productStockService = new ProductStockService($this->pDbMgr);
    }

    /**
     * @param array $oldReservations
     * @param $orderId
     * @param bool $forceCreate
     *
     * @throws \Exception
     * @return bool
     *
     */
    public function createStreamsoftReservation(array $oldReservations, $orderId, $forceCreate = false, $updatedItems = false)
    {
        $this->resetObjectData();
        $order = $this->dataProvider->getOrder($orderId, ['O.id', 'O.website_id', 'P.id as product_id', 'O.order_number', 'O.order_number_iteration'], false, true);

        $oldReservations = ArrayHelper::toKeyValues('id', $oldReservations);
        $this->oldReservations = $oldReservations;
        $this->orderData = $order;

        //$updatedReservations = $this->dataProvider->getReservationsByOrderId($orderId);
        $updatedReservations = $this->getOrderReservations($orderId, $updatedItems);

        // wszystkie idki z oldReservation i updated
        $mergedItems = array_unique(ArrayHelper::arrayColumn(array_merge($updatedReservations, $oldReservations), 'product_id'));

        $updatedReservations = ArrayHelper::toKeyValues('id', $this->filterProducts($updatedReservations));

//        $this->getFromStreamsoftCurrentReservation($order['streamsoft_order_number']);

        // jezeli ustawimy flage na true to wymusi utworzenie rezerwacji
        if ($forceCreate === false) {
            $reservationStatus = $this->reservationStatus($oldReservations, $updatedReservations);
        } else {
            $reservationStatus = self::RECREATE_RESERVATION;
        }

        // ustawia flage, ze proba utworzenia zamowienia sie odbyla
        $this->creationHasBeenTried = true;

        // jesli nic sie nie zmienilo to nic nie robimy
        if ($reservationStatus == self::SAME_RESERVATION) {
            $this->updateProductStock($mergedItems);
            return true;
        }

//        $this->createErpOrderWithReservation($order ,$updatedReservations);

        // jezel iejst czesciowe
        if (true === $this->isPartial()) {
            $this->proceedPartial($order);
        }

        $updatedReservations = ArrayHelper::toKeyValues('id', $this->filterProducts($this->getOrderReservations($orderId, $updatedItems)));

        $this->stockSupplyService->updateStockReservationsByReservationsItems($oldReservations, $updatedReservations);

        $this->updateProductStock($mergedItems);

        if (true === $this->isPartial()) {
            return false;
        }

        return true;

    }

    private function getOrderReservations($orderId, $updatedItems = false)
    {
        if (true == $updatedItems){
            return $this->dataProvider->getOldReservationsToRepair($orderId);
        }

        return $this->dataProvider->getReservationsByOrderId($orderId);
    }

    private function updateProductStock($productsIds)
    {
        $this->productStockService->updateStockByProductsIds($productsIds);
    }

    /**
     * @param $order
     */
    private function proceedPartial($order)
    {
        // rozbite skladowe zamowienia na niezamowione i zamowione
        $this->explodeOrdersItems($order);

        $this->updateOrderReservationsQuantity();

        // usuwamy zamowienie wtedy kiedy wszystkie pozycje poszly do kresek, w innym przypadku wczesniejsza aktualizacja poprawila zmaowienie
        $this->removeOrderIfNessesary($order);

        $toExteption = ArrayHelper::toKeyValue($this->unReservedProducts, 'id', 'name');
        $this->errorMessage = _(sprintf('Uwaga, produkty: <br> %s <br> nie zostały zrealizowane w całości i zostały przeniesione do kresek', implode('<br>', $toExteption).' '.$order['id']));
    }

    /**
     * Pobiera rezerwacje ze streamsoftu
     *
     * @param $externalNumber
     */
    private function getFromStreamsoftCurrentReservation($externalNumber)
    {
        $reservation = $this->reservation->showReservation($externalNumber);

        if ($reservation->getErrorCode() != -1) {
            $this->currentStreamsoftReservation = $reservation;
        }
    }

    /**
     * @param array $updatedReservations
     *
     * @return array
     */
    public function filterProducts(array $updatedReservations)
    {
        $result = [];

        foreach($updatedReservations as $reservationItem) {
            if(false == in_array($reservationItem['source_id'], $this->acceptableSources)) {
                continue;
            }

            $result[] = $reservationItem;
        }

        return $result;
    }

    /**
     * Rozklada czesciowe zamowienia na skladowe lub nie
     *
     * @param $order
     * @return array
     */
    private function explodeOrdersItems($order)
    {
        $orderItems = $this->dataProvider->getOrderItems($order['id'], true);

        $confirmer = new Confirmer();

        foreach($orderItems as $orderItem) {
            $unreservedItem = $this->unReservedProducts[$orderItem['id']];


            if (null == $unreservedItem){
                continue;
            }

            $oldItemNewStatus = 0;

            if ($unreservedItem['reserved'] > 0){
                $oldItemNewStatus = 3;
            }

            $confirmer->divideOrderItem($order['id'], $orderItem, $unreservedItem['reserved'], $unreservedItem['unreserved'], $oldItemNewStatus);
        }
    }

    /**
     * @param $order
     * @return bool
     */
    private function removeOrderIfNessesary($order)
    {
        if (false == $this->reservationResponse instanceof wsRezerwacjeResult){
            return false;
        }

        foreach($this->reservationResponse->getValue() as $item){
            if (null == $item){
                continue;
            }

            if ($item->getIloscrez() > 0){
                return false;
            }
        }

        $this->removeOrder($order);

        return true;

        /**w razie problemow zamienic
        $orderItems = $this->dataProvider->getOrderItems($order['id'], true);

        if (true == empty($orderItems)) {
            $result = $this->removeOrder($order);
            return true;
        }

        return false; **/
    }

    private function updateOrderReservationsQuantity()
    {
        foreach($this->unReservedProducts as $position) {

            if ($position['reserved'] < 1) {
                $this->pDbMgr->Query('profit24', "DELETE FROM products_stock_reservations WHERE id =".$position['id']);
                continue;
            }

            $this->pDbMgr->Update('profit24', "products_stock_reservations", ['quantity' => $position['reserved']], 'id = '.$position['id']);
        }
    }

    /**
     * @param array $oldReservations
     * @param array $updatedReservations
     *
     * @return bool
     */
    private function reservationStatus(array $oldReservations, array $updatedReservations)
    {
        if (true === empty($oldReservations) && true === empty($updatedReservations)) {
            return self::SAME_RESERVATION;
        }

        if (null === $this->currentStreamsoftReservation){
            return self::NEW_RESERVATION;
        }

        $reservationStatus = self::RECREATE_RESERVATION;

        $oldCounted = count($oldReservations);
        $newCounted = count($updatedReservations);
        $sameReservations = 0;

        // sprawdzamy zmiany jezeli jest taka sama ilsoc rezerwacji
        if ($oldCounted == $newCounted){

            foreach($oldReservations as $id => $values) {

                $reservationItem = $updatedReservations[$id];

                if(null != $reservationItem && $reservationItem['quantity'] == $values['quantity']) {
                    $sameReservations++;
                }
            }

            if ($oldCounted == $sameReservations) {
                $reservationStatus = self::SAME_RESERVATION;
            }
        }

        // przenioslem wyzej do testow
//        if ($oldCounted == $sameReservations) {
//            $reservationStatus = self::SAME_RESERVATION;
//        }

        return $reservationStatus;
    }

    /**
     * Tworzy zamowienie i rezerwacje w ERP
     *
     * @param $order
     * @param $positions
     *
     * @throws \Exception
     * @return bool
     */
    private function createErpOrderWithReservation($order, $positions)
    {
        $newPos = [];

        // bierzemy tylko ze stocka do streamsoftu
        foreach($positions as $key => $pos){
            if($pos['source_id'] == 10){
                $newPos[$key] = $pos;
            }
        }

        if (null == $this->currentStreamsoftReservation && empty($newPos)) {
            return true;
        }

        try {

            $orderRes = $this->order->createOrUpdateOrder($order, $newPos);

            if ($_SESSION['user']['name'] == 'agolba') {
                var_dump($order['streamsoft_order_number']);
                echo '<pre>';
                var_dump($orderRes);
                echo '</pre>';
            }

        } catch(\Exception $e)
        {

            if ($_SESSION['user']['name'] == 'agolba') {
                var_dump($order['streamsoft_order_number']);
                echo '<pre>';
                var_dump($e->getMessage());
                echo '</pre>';
            }

            throw $e;

        }


        $reservationResult = $this->reservation->createReservation($order['streamsoft_order_number']);

        // TODO jak przypal to obsluzyc - zamowienie puste itp
        $reservation = $this->reservation->showReservation($order['streamsoft_order_number']);
        $this->reservationResponse = $reservation;

        if ($reservationResult->getValue() == 2) {
            $this->isPartial = true;
            $this->prepareUnreservedProducts($reservation, $newPos);
        }


        if (false === ($this->dataProvider->clearOrderReservationsLp($order['id']))){
            throw new \Exception('blad podczas aktualziacji kolumny LP');
        }

        $this->updateReservationLp($newPos, $reservation);

        return true;
    }

    private function updateReservationLp($newPos, $reservation)
    {
        $test = $reservation->getValue();
        $isInstanceof = $reservation instanceof wsRezerwacjeResult;
        if (false == $isInstanceof || empty($test)){
            return;
        }

        foreach($reservation->getValue() as $reservationItem){

            if (null == $reservationItem){
                continue;
            }

            //sprawdzamy czy pozycja zostala zarezerwowana
           if($reservationItem->getIloscrez() > 0) {
               $currentOurlement = array_values($newPos)[0];

               if (null == $currentOurlement){
                   break;
               }

               if(false === $this->dataProvider->updateRservationLp($currentOurlement['id'], $reservationItem->getLp())){
                   throw new \Exception('blad podczas aktualziacji kolumny LP');
               }

               unset($newPos[$currentOurlement['id']]);
           }
        }
    }

    private function prepareUnreservedProducts(wsRezerwacjeResult $reservation, $positions)
    {
        foreach($reservation->getValue() as $reservationItem) {

            if (null == $reservationItem) {
                continue;
            }

            if ($reservationItem->getIloscrez() < $reservationItem->getIlosc()) {
                foreach($positions as $position) {
                    if ($position['streamsoft_indeks'] == $reservationItem->getIndeks() && $position['quantity'] == $reservationItem->getIlosc() && $reservationItem) {
                        $this->unReservedProducts[$position['order_item_id']] = [
                            'unreserved' => $reservationItem->getIlosc() - $reservationItem->getIloscrez(),
                            'name' => '-- '.$reservationItem->getPozNazwa(),
                            'id' => $position['id'],
                            'reserved' => $reservationItem->getIloscrez()
                        ];
                    }
                }
            }

            foreach($positions as $key => $pos){
                if($pos['streamsoft_indeks'] == $reservationItem->getIndeks()){
                    unset($positions[$key]);
                    break;
                }
            }
        }
    }

    /**
     * @param $orderId
     *
     * @return bool
     */
    public function createLocalReservationsByOrderId($orderId)
    {
        $changedOrderItems = $this->dataProvider->getOrderItems($orderId, false, true);

        // jezeli niema rezerwacji do stworzenia do nic nie robimy
        if(true === empty($changedOrderItems)){
            return false;
        }

        foreach($changedOrderItems as $item) {

            // ustawiamy status rezerwazcjom na 0, nie mozemy podac pustej tablicy, skrypt sprawdza status starej rezerwacji
            $nonChangedOrderItem = $item;
            $nonChangedOrderItem['status'] = 0;

            if ($item['status'] == 4){
                $item['status'] = 3;
            }

            $orderItemReservation = new orderItemReservation($this->productStockReservation);
            $orderItemReservation->updateValues($item, $nonChangedOrderItem);
        }

        return true;
    }


    public function rollbackChanges()
    {
        if (false == $this->creationHasBeenTried){
            return true;
        }

        //TODO cofnac wszystkie zmiany
        $this->removeOrder($this->orderData);

        if (false == empty($this->oldReservations)){
            $order = $this->dataProvider->getOrder($this->orderData['id'], ['O.id', 'O.order_number', 'O.website_id', 'P.id as product_id', 'O.order_number_iteration'], false, true);
            $this->orderData = $order;
//            $this->createErpOrderWithReservation($this->orderData, $this->oldReservations);
        }

        return true;
    }

    public function isPartial()
    {
      return $this->isPartial;
    }

    /**
     * @param $order
     * @param null $oldReservations
     *
     * @param bool $iterate
     * @return WS\wsResult
     * @throws \Exception
     */
    public function removeOrder($order, $oldReservations = null, $iterate = true)
    {
        if (true == $iterate){
            $orderData = null == $this->orderData ? $order : $this->orderData;
            $order = $this->dataProvider->getOrder($orderData['id'], ['O.id', 'O.order_number', 'O.website_id', 'P.id as product_id', 'O.order_number_iteration'], false, true);
        }

        if (false == isset($order['streamsoft_order_number'])){
            $order['streamsoft_order_number'] = $order['order_number'];
        }

        if (null !== $oldReservations) {
            $this->stockSupplyService->updateStockReservationsByReservationsItems($oldReservations, []);
        }

//        $andrzej = $this->order->cancelOrder($order['streamsoft_order_number']);


        if (true == $iterate){
            $this->dataProvider->iterateStreamsoftNumber($order['id']);
        }

//        return $andrzej;
    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    private function resetObjectData()
    {
        $this->reservationResponse = null;
        $this->unReservedProducts = [];
        $this->isPartial = false;
        $this->errorMessage = null;
        $this->currentStreamsoftReservation = null;
        $this->oldReservations = null;
        $this->orderData = null;
        $this->creationHasBeenTried = false;
    }

    public static function getPozByLp($reservationResult, $lp)
    {
        if (null == $reservationResult){
            return null;
        }

        foreach ($reservationResult->getValue() as $pos){
            if (null == $pos){
                continue;
            }

            if ($pos->getLp() == $lp){
                return $pos;
            }
        }

        return null;
    }

    /**
     * @param mixed $oldReservations
     *
     * @return $this
     */
    public function setOldReservations($oldReservations)
    {
        $this->oldReservations = $oldReservations;

        return $this;
    }

    /**
     * @param mixed $orderData
     *
     * @return $this
     */
    public function setOrderData($orderData)
    {
        $this->orderData = $orderData;

        return $this;
    }
}