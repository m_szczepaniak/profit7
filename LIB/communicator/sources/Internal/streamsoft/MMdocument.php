<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use DateTime;
use Exception;
use Benchmark_Timer;
use LIB\communicator\sources\Internal\streamsoft\getPZCMS;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok;
use LIB\communicator\sources\Internal\streamsoft\WS\wsIntegerResult;
use LIB\communicator\sources\Internal\streamsoft\WS\wsNagl;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPoz;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPozArray;
use LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah;

/**
 * Description of getPZ
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class MMdocument extends ProfitServerImplService
{

  private $magazinesCodes = [
      1 => '03',
      0 => '98'
  ];

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;


  /**
   *
   * @var void
   */
  private $sesja;

  /**
   *
   * @param \LIB\communicator\sources\Internal\streamsoft\DatabaseManager $pDbMgr
   * @global Benchmark_Timer $oTimer
   */
  public function __construct(DatabaseManager $pDbMgr)
  {
    global $aConfig;//, $oTimer
    $this->pDbMgr = $pDbMgr;
    if ($aConfig['common']['status'] == 'development') {
      parent::__construct(array(), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
    } else {
      parent::__construct();
    }
//    $oTimer->setMarker('Start utworzSesje');
    $this->sesja = $this->utworzSesje('webservice', 'profwebservice')->getValue();
//    $oTimer->setMarker('Stop utworzSesje');
  }

  /**
   * @param $iOSHI
   * @return int
   *
   * @throws Exception
   */
  public function doSendMM($oshi)
  {
    $oGetPZCMS = new getPZCMS($this->pDbMgr);

    $magazineCode = $this->magazinesCodes[$oshi['source']];

    $naglowek = $this->getNaglowek($oshi);
    $pozycje = $this->getPozycjeArray($oGetPZCMS, $oshi['id'], $magazineCode);
    $result = $this->utworzDokument($this->sesja, $naglowek, $pozycje, 'M'.$magazineCode);

    if ($result->getErrorCode() != 0) {
      throw new Exception($result->getErrorMessage(), $result->getErrorCode());
    }

    return $result;
  }

  public function doSendMM_custom($location,$products_to_mm,$magazineStream = '02')
  {
      $oGetPZCMS = new getPZCMS($this->pDbMgr);

      $magazineCode = $magazineStream; //stock

      $unikalny_identyfikator = '';//$this->incrementalHash();

      $naglowek = $this->getNaglowekCustom($location,'16',$unikalny_identyfikator); //16 newest Merlin magazine
      $pozycje = $this->getPozycjeArrayCustom($oGetPZCMS, $location, $magazineCode,$products_to_mm);
      $result = $this->utworzDokument($this->sesja, $naglowek, $pozycje, 'M'.$magazineCode);

      if ($result->getErrorCode() != 0) {
          throw new Exception($result->getErrorMessage(), $result->getErrorCode());
      }

      $this->saveDocumentHeader($location,$location);

      return $result;
  }

  private function incrementalHash($len = 5)
  {
      $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
      $base = strlen($charset);
      $result = '';

      $now = explode(' ', microtime())[1];
      while ($now >= $base){
        $i = $now % $base;
        $result = $charset[$i] . $result;
        $now /= $base;
      }
    return substr($result, -5);
  }

  private function saveDocumentHeader($header,$location)
  {
      $aValues = array(
          'streamsoft_header' => $header,
          'location' => $location,
          'status' => 'NOWY'
      );
      if ($this->pDbMgr->Insert('profit24', 'merlin_sent_documents', $aValues) === false) {
          return false;
      }
      return true;
  }

  /**
   *
   * @param getPZCMS $oGetPZCMS
   * @param int $iOSHI
   * @return wsPozArray
   */
  private function getPozycjeArray($oGetPZCMS, $iOSHI, $magazineCode)
  {
    $aOSHItems = $oGetPZCMS->getItems($iOSHI);

    $pozycje = new wsPozArray();
    $aItems = array();

    foreach ($aOSHItems as $aItem) {
      if ($aItem['in_confirmed_quantity'] > 0) {

        $aItems[] = new wsPoz(
            $aItem['streamsoft_indeks'],
            $aItem['in_confirmed_quantity'],
            FALSE,
            $aItem['price_netto'],
            ($aItem['price_netto'] > 0.00 ? TRUE : FALSE),
            0,
            $magazineCode,
            '',
            $aItem['item_source_id'],
            '',
            '',
            new DateTime(),
            '',
            '',
            ''
        );
      }
    }
    $pozycje->setItem($aItems);
    return $pozycje;
  }

    private function getPozycjeArrayCustom($oGetPZCMS, $location, $magazineCode,$products_to_mm)
    {
        $pozycje = new wsPozArray();
        $aItems = array();

        foreach ($products_to_mm as $aItem) {
            if ($aItem['quantity'] > 0) {

                $aItems[] = new wsPoz(
                    $aItem['streamsoft_indeks'],
                    $aItem['quantity'],
                    FALSE,
                    '',
                    FALSE,
                    0,
                    $magazineCode,
                    '',
                    '',
                    '',
                    '',
                    new DateTime(),
                    '',
                    '',
                    ''
                );
            }
        }
        $pozycje->setItem($aItems);
        return $pozycje;
    }

  /**
   * @return wsNagl
   */
  private function getNaglowek($oSHI, $moveToMagazine = '01')// zrodlowy kod urzadzenia zewnętrznego
  {
    $cechy = new wsCechaDok($oSHI['number'], '', '', '', '');
    $kontrahent = new wsKontrah('', '', '', '', '', '', '', '', '', '', '', '', '');
    $naglowek = new wsNagl($kontrahent, 'MMA-', 'MM-', $oSHI['number'], new DateTime(), '', '', new DateTime(), '','','');
    $naglowek->setMagazyn($moveToMagazine);// docelowy kod magazynu
    $naglowek->setCechy($cechy);

    return $naglowek;
  }

  private function getNaglowekCustom($location, $moveToMagazine = '01',$unikalny_identyfikator)// zrodlowy kod urzadzenia zewnętrznego
  {
    $cechy = new wsCechaDok($location, '', '', '', '');
    $kontrahent = new wsKontrah('', '', '', '', '', '', '', '', '', '', '', '', '');
    $naglowek = new wsNagl($kontrahent, 'MMA-', 'MM-', $location, new DateTime(), '', '', new DateTime(), '','','');
    $naglowek->setMagazyn($moveToMagazine);// docelowy kod magazynu
    $naglowek->setCechy($cechy);

    return $naglowek;
  }

  /**
   * @param $mmDoc
   * @param $iOSHI
   *
   * @return mixed
   */
  public function saveMMtoOrder($mmDoc, $iOSHI)
  {
    global $aConfig, $pDbMgr;

    return $pDbMgr->Update($aConfig['common']['bookstore_code'], $aConfig['tabls']['prefix'].'orders_send_history', ['mm_doc' => $mmDoc], "id = ".$iOSHI);
  }

  /**
   *
   * @param int $iOSHId
   * @return array
   */
  public function getOSHI($iOSHId)
  {
    global $aConfig;

    $sSql = 'SELECT OSH.*
              FROM orders_send_history AS OSH
              WHERE OSH.id = '.$iOSHId;

    return $this->pDbMgr->GetRow($aConfig['common']['bookstore_code'], $sSql);
  }

  /**
   * @param $oshi
   * @return bool
   *
   * @throws Exception
   */
  public function acceptMMdoc($oshi)
  {
    $res = $this->akceptujDokument($this->sesja, $oshi['number']);

    if ($res->getErrorCode() == -1) {
      throw new \Exception($res->getErrorMessage());
    }

    $this->saveMMtoOrder(2, $oshi['id']);

     return true;
  }

  /**
   * @param $oshi
   *
   * @return bool
   */
  public function canBeCreated($oshi)
  {
      if(true === in_array($oshi['source'], array_keys($this->magazinesCodes))) {
          return true;
      }

      return false;
  }
}
