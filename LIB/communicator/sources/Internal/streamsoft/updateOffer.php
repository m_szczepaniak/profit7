<?php

namespace LIB\communicator\sources\Internal\streamsoft;

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-07
 * @copyrights Marcin Chudy - Profit24.pl
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal/streamsoft';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');
global $pDbMgr;

$sql = '
SELECT PS.id 
FROM products_stock AS PS
JOIN products AS P
ON P.id = PS.id
WHERE PS.profit_g_status > 0
AND P.modified > DATE_SUB(DATE(NOW()), INTERVAL 1 DAY)
ORDER BY P.last_import DESC

';
echo date('Y-m-d H:i:s')."\n";
$rows = $pDbMgr->GetCol('profit24', $sql);
$synchroProduct = new synchroProduct($pDbMgr);
foreach($rows as $key => $productId) {
    echo 'i:'.$key.' p: '.$productId."\n";
    try {
        $synchroProduct->trySynchronizeProduct($productId, true);
    } catch (\Exception $ex) {
        echo $ex->getMessage();
    }
    usleep(200);
}
echo 'koniec !'."\n";