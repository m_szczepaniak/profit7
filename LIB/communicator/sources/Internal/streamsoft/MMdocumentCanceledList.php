<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace LIB\communicator\sources\Internal\streamsoft;

use Benchmark_Timer;
use DatabaseManager;
use DateTime;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\getPZCMS;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok;
use LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah;
use LIB\communicator\sources\Internal\streamsoft\WS\wsNagl;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPoz;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPozArray;

/**
 * Description of getPZ
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class MMdocumentCanceledList extends ProfitServerImplService
{
  
  /**
   * @var DatabaseManager
   */
  private $pDbMgr;


  /**
   *
   * @var void
   */
  private $sesja;

  /**
   *
   * @param \LIB\communicator\sources\Internal\streamsoft\DatabaseManager $pDbMgr
   * @global Benchmark_Timer $oTimer
   */
  public function __construct(DatabaseManager $pDbMgr)
  {
    global $aConfig;
    $this->pDbMgr = $pDbMgr;
    if ($aConfig['common']['status'] == 'development') {
      parent::__construct(array(), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
    } else {
      parent::__construct();
    }
    $this->sesja = $this->utworzSesje('webservice', 'profwebservice')->getValue();
  }

  /**
   * @param $iOSHI
   * @return int
   *
   * @throws Exception
   */
  public function doSendMMByOrdersItemsList($iOILId)
  {
    $magazineCodeFrom = '01';
    $aOILData = $this->getOrdersListsCanceled($iOILId);
    $naglowek = $this->getNaglowek($aOILData);
    $pozycje = $this->getPozycjeArray($aOILData['id'], $magazineCodeFrom);
    $result = $this->utworzDokument($this->sesja, $naglowek, $pozycje, 'M'.$magazineCodeFrom);

    if ($result->getErrorCode() != 0) {
      throw new Exception($result->getErrorMessage(), $result->getErrorCode());
    }

    return $result;
  }
  
  /**
   * 
   * @param int $iOrdersListsId
   * @return array
   */
  private function getOrdersListsCanceled($iOrdersListsId) {
    
    $sSql = 'SELECT id
             FROM orders_items_lists
             WHERE id = '.$iOrdersListsId .' AND type = "4" ';
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }

  /**
   * 
   * @param int $iOSHI
   * @return array items
   */
  private function getItems($iOSHI) {
    
    $sSql = 'SELECT streamsoft_indeks, SUM(confirmed_quantity) AS confirmed_quantity
             FROM orders_items_lists_items  AS OILI
             JOIN orders_items AS OI
              ON OI.id = OILI.orders_items_id
             JOIN products AS P
              ON P.id = OI.product_id
             WHERE orders_items_lists_id = ' . $iOSHI.' AND confirmed_quantity > 0
             GROUP BY streamsoft_indeks
             ORDER BY OI.name ASC';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   *
   * @param getPZCMS $oGetPZCMS
   * @param int $iOSHI
   * @return wsPozArray
   */
  private function getPozycjeArray($iOSHI, $magazineCode)
  {
    $aOSHItems = $this->getItems($iOSHI);

    $pozycje = new wsPozArray();
    $aItems = array();

    foreach ($aOSHItems as $aItem) {
      if ($aItem['confirmed_quantity'] > 0) {

        $aItems[] = new wsPoz(
            $aItem['streamsoft_indeks'],
            $aItem['confirmed_quantity'],
            FALSE,
            0,
            0,
            0,
            $magazineCode,
            '',
            '',
            '',
            '',
            new DateTime(),
            '',
            '',
            ''
        );
      }
    }
    $pozycje->setItem($aItems);
    return $pozycje;
  }

  /**
   * @return wsNagl
   */
  private function getNaglowek($aOILData, $moveToMagazine = '07')// zrodlowy kod urzadzenia zewnętrznego
  {
    $docPrefix = 'A_';
    $sDocNumber = $docPrefix.$aOILData['id'];
    $cechy = new wsCechaDok($sDocNumber, '', '', '', '');
    $kontrahent = new wsKontrah('', '', '', '', '', '', '', '', '', '', '', '', '');
    $naglowek = new wsNagl($kontrahent, 'MMA-', 'MM-', $sDocNumber, new DateTime(), '', '', new DateTime(), '', $moveToMagazine, $cechy);

    return $naglowek;
  }

  /**
   * @param $mmDoc
   * @param $iOSHI
   *
   * @return mixed
   */
  public function saveMMtoOrder($mmDoc, $iOSHI)
  {

    return $this->pDbMgr->Update('profit24', 'orders_send_history', ['mm_doc' => $mmDoc], "id = ".$iOSHI);
  }

  /**
   *
   * @param int $iOSHId
   * @return array
   */
  public function getOSHI($iOSHId)
  {

    $sSql = 'SELECT OSH.*
              FROM orders_send_history AS OSH
              WHERE OSH.id = '.$iOSHId;

    return $this->pDbMgr->GetRow('profit24', $sSql);
  }

  /**
   * @param $oshi
   * @return bool
   *
   * @throws Exception
   */
  public function acceptMMdoc($oshi)
  {
    $res = $this->akceptujDokument($this->sesja, $oshi['number']);

    if ($res->getErrorCode() == -1) {
      throw new \Exception($res->getErrorMessage());
    }

    $this->saveMMtoOrder(2, $oshi['id']);

     return true;
  }

  /**
   * @param $oshi
   *
   * @return bool
   */
  public function canBeCreated($oshi)
  {
      if(true === in_array($oshi['source'], array_keys($this->magazinesCodes))) {
          return true;
      }

      return false;
  }
}
