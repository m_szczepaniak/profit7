<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-01 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use DateTime;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok;
use LIB\communicator\sources\Internal\streamsoft\WS\wsKontrah;
use LIB\communicator\sources\Internal\streamsoft\WS\wsNagl;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPoz;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPozArray;
use orders_alert\OrdersAlert;
use stdClass;

/**
 * Description of getInvoice
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class getInvoice extends getInvoiceCMS {

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   *
   * @var stdClass
   */
  public $oInvoice;
  
  /**
   *
   * @var OrdersAlert
   */
  private $oAlert;

  /**
   * @var
   */
  private $reservationData;

  public function __construct(DatabaseManager $pDbMgr) {
    $this->pDbMgr = $pDbMgr;
    $this->oAlert = new OrdersAlert();
    parent::__construct($pDbMgr);
  }
  
  /**
   * 
   * @param int $iId
   * @return array
   */
  public function getOrderInvoices($iId) {
    $this->oInvoice = $this->getSingleInvoice($iId);
    $aInvoices = array();
    $this->dataProvider = new DataProvider($this->pDbMgr);
    $this->orderDetails = $this->dataProvider->getOrder($iId, ['id', 'order_number', 'order_date', 'order_number_iteration'], true, true);
//    $this->reservationData = (new Reservation($this->pDbMgr))->showReservation($this->orderDetails['streamsoft_order_number']);

    $this->oInvoice->order_number = $this->orderDetails['streamsoft_order_number'];

    if ($this->oInvoice->second_invoice == '1') {
      $oNgl2 = $this->getWSNaglDok($iId, false);
      if ($oNgl2 !== false) {
        $aInvoices['1']['nagl'] = $oNgl2;
        $aInvoices['1']['poz'] = $this->getWSPozycje($iId, false);
      } else {
        return false;
      }
    }
    $oNgl1 = $this->getWSNaglDok($iId, true);
    if ($oNgl1 !== false) {
      $aInvoices['0']['nagl'] = $oNgl1;
      $aInvoices['0']['poz'] = $this->getWSPozycje($iId, true);
      return $aInvoices;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param bool $bIsFirstInvoice
   * @return wsPozArray
   */
  private function getWSPozycje($iOrderId, $bIsFirstInvoice) {
    $aInvoiceItems = $this->getInvoiceItems($iOrderId, $bIsFirstInvoice);
    return $this->getPozycjeArray($aInvoiceItems, $bIsFirstInvoice);
  }

  /**
   * 
   * @param array $aInvoiceItems
   * @param bool $bIsFirstInvoice
   * @return wsPozArray
   */
  private function getPozycjeArray(array $aInvoiceItems, $bIsFirstInvoice) {
    $aPozArray = new wsPozArray();
    $aItems = array();
    foreach ($aInvoiceItems as $oItem) {
      $sMagazine = $this->getMagazineBySource($oItem->source);
      
      if ($oItem->product_id > 0) {
        $this->synchroProduct($oItem->product_id);
      } elseif ($oItem->item_type == 'A') {
        // załącznik
        $oAttachmentParent = $this->getAttachmentParentData($oItem->parent_id);
        // = $this->getOrderItemProductId($iParentId);
        $oItem->streamsoft_indeks = $this->getAttachmentStreamsoftIndeks($oAttachmentParent->product_id, $oItem->attachment_id);
        $sMagazine = $this->getMagazineBySource($oAttachmentParent->source);
      }

      $newPos = new wsPoz(strtoupper($oItem->streamsoft_indeks), $oItem->quantity, TRUE, ($oItem->value_brutto/$oItem->quantity),
          TRUE, 0/*$oItem->discount*/, $sMagazine, '', '', '', '', new DateTime(), '', '', '');

      if (null != $oItem->streamsoft_lp){
//        $resPos = ReservationManager::getPozByLp($this->reservationData, $oItem->streamsoft_lp);
//
//        if (null != $resPos){
//          $newPos->setDotyczy_daty(new \DateTime($this->orderDetails['order_date']));
//          $newPos->setDotyczy_lp($oItem->streamsoft_lp);
//          $newPos->setDotyczy_numer_dok($this->oInvoice->order_number);
//          $newPos->setDotyczy_nazwa_dok("ZA");
//        }
      } else {
          if($oItem->source == 10) {
            $encoded = json_encode($oItem);
            $this->pDbMgr->Insert('profit24', 'reservations_debug', ['data' => $encoded]);
          }
      }

      $aItems[] = $newPos;
    }
    $this->oAlert->removeDuplicates();
    $this->oAlert->addAlerts();
    $mReturn = $this->getTransportItem($bIsFirstInvoice);
    if ($mReturn instanceof wsPoz) {
      $aItems[] = $mReturn;
    }
    $aPozArray->setItem($aItems);
    return $aPozArray;
  }
  
  private function getMagazineBySource($sSource) {
    if ($sSource != '51') { // ??51
      return '01';// Pokorna, tramwaj- niewidoczny w CMS
    } else {
      return '07';// Zasoby - widoczny w CMS
    }
  }
  
  /**
   * 
   * @param int $iProductId
   * @param int $iAttachmentId
   * @return string
   */
  private function getAttachmentStreamsoftIndeks($iProductId, $iAttachmentId) {
    
    $sIDT = $iProductId.'0'.$iAttachmentId;
    $sId = trim($sIDT);
    return $sId;
  }
  
  /**
   * 
   * @param int $iParentId
   * @return int
   */
  private function getAttachmentParentData($iParentId) {
    
    $sSql = 'SELECT product_id, source
             FROM orders_items
             WHERE id = '.$iParentId;
    return (object)$this->pDbMgr->GetRow('profit24', $sSql);
  }
  
  /**
   * 
   * @param array $aInvoiceOrders
   * @param bool $bFirstMethod
   * @return wsPoz
   */
  public function getTransportItem($bFirstMethod) {
    
    if ($bFirstMethod === true) {
      $iTMPVAT = $this->oInvoice->transport_vat;
      if ($this->oInvoice->transport_vat < 10) {
        $iTMPVAT = ($this->oInvoice->transport_vat * 10);
      }
      $indeks = sprintf('1%1$02d%2$010d', $iTMPVAT, $this->oInvoice->transport_id);
      $oPoz = new wsPoz($indeks, 1, TRUE, floatval($this->oInvoice->transport_cost), TRUE, 
              0, '01', '', '', '', '', new DateTime(), '', '', '');
      return $oPoz;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param int $iProductId
   */
  private function synchroProduct($iProductId) {
    $oProduct = new synchroProduct($this->pDbMgr);
    try {
      $oProduct->trySynchronizeProduct($iProductId, true, true);
    } catch (Exception $ex) {
      $this->oAlert->createAlert($this->oInvoice->id, 'err-inv-add-product', 'Błąd dodawania produktu do oferty produktowej: '.print_r(nl2br($ex->getMessage()), true));
    }
  }
  
  /**
   * @param string $sDocIdent
   * @param bool $bIsFirstInvoice
   * @return 
   */
  private function getWSCechaDok($sDocIdent, $bIsFirstInvoice) {
    return new wsCechaDok($sDocIdent, $this->oInvoice->oryginal_order_number,
            floatval($this->oInvoice->payu_to_pay), floatval($this->oInvoice->postal_fee_to_pay), $this->oInvoice->seller_streamsoft_id);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param bool $bIsFirstInvoice
   * @return wsKontrah
   */
  private function getWSKontrah($iOrderId, $bIsFirstInvoice) {
    $ak = $this->getUserAddress($iOrderId, $bIsFirstInvoice, $this->checkIsPayment14Days());
    if (!empty($ak)) {
      $k = (object)$ak;
      $sNameLong = ($k->company != '' ? $k->company . ' ' : ''). $k->name.' '.$k->surname;
      // trzeba sprawdzić czy F14 dni ???
      $iKontrahId = $this->getKontrahId($k);//$iKontrahId
      return new wsKontrah($iKontrahId, 'K'.$k->id, $sNameLong, str_replace('-', '', $k->nip).'', substr($k->city, 0, 256), $k->postal, substr($k->city, 0, 29), substr($k->street, 0 , 59), substr($k->number, 0, 10), substr($k->number2, 0, 10), 'PL', $k->email, $k->phone);
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param stdClass $k
   * @return int
   */
  private function getKontrahId(stdClass $k) {
    
    if ($this->checkIsPayment14Days() === TRUE) {
      return $k->streamsoft_id;
    }
    return 0;
  }
  
  /**
   * 
   * @return boolean
   */
  private function checkIsPayment14Days() {
    
    if ($this->oInvoice->payment_type == 'bank_14days' || $this->oInvoice->second_payment_type == 'bank_14days') {
      return true;
    } else {
      return false;
    }
  }

  /**
   * @param int $iId
   * @param bool $bIsFirstInvoice
   * @return wsNagl
   */
  private function getWSNaglDok($iId, $bIsFirstInvoice) {
    
    $kontrahent = $this->getWSKontrah($iId, $bIsFirstInvoice);
    if ($kontrahent !== false) {
      $sInvoiceNumber = $this->getIdentOrderInvoiceNumber($bIsFirstInvoice);
      $cechy = $this->getWSCechaDok($sInvoiceNumber, $bIsFirstInvoice);//02

      return new wsNagl($kontrahent, 'FvatCMS', 'FA', 
              $sInvoiceNumber, 
              new DateTime($this->oInvoice->invoice_date), 
              /*$this->oInvoice->transport*/$this->oInvoice->streamsoft_name, $this->getPaymentString($this->oInvoice->payment_type), new DateTime($this->oInvoice->invoice_date_pay), '', '', $cechy);
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param string $sDBPayment
   * @return string
   */
  private function getPaymentString($sDBPayment) {
    switch ($sDBPayment) {
      case 'bank_transfer':
        return 'Przedpłata';
      break;
    
      case 'bank_14days':
        return 'Przelew';
      break;
    
      case 'card':
      case 'platnoscipl':
        return 'PayU';
      break;
    
      case 'postal_fee':
        return 'Pobranie';
      break;
    }
  }
  
  /**
   * 
   * @param bool $bIsFirstInvoice
   * @return string
   */
  private function getIdentOrderInvoiceNumber($bIsFirstInvoice) {
    if ($bIsFirstInvoice == true) {
      return $this->oInvoice->invoice_id;//.'00'.RAND(0,129);
    } else {
      return $this->oInvoice->invoice2_id;//.'00'.RAND(0,129);
    }
  }
}
