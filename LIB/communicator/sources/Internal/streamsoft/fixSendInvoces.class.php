<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 17.08.17
 * Time: 10:54
 */


use LIB\communicator\sources\Internal\streamsoft\validateInvoice;

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-07
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal/streamsoft';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');

global $pDbMgr;
$oValidateInvoice = new validateInvoice($pDbMgr);
$oValidateInvoice->proceedValidateInvoicesByRealized();
echo 'koniec';

