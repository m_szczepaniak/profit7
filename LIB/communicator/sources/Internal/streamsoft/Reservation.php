<?php

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use Benchmark_Timer;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;

/**
 * Description of getPZ
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Reservation extends ProfitServerImplService
{

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;


    /**
     * @var void
     */
    private $sesja;

    /**
     * @param DatabaseManager $pDbMgr
     */
    public function __construct(DatabaseManager $pDbMgr)
    {
        global $aConfig;//, $oTimer
        $this->pDbMgr = $pDbMgr;
        if ($aConfig['common']['status'] == 'development') {
            parent::__construct(array(), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
        } else {
            parent::__construct();
        }

//        $this->sesja = $this->utworzSesje('webservice', 'profwebservice')->getValue();
    }

    /**
     * @param $externalNumber
     * @return WS\wsIntegerResult
     */
    public function createReservation($externalNumber)
    {
        return $this->zalozRezerwacje($this->sesja, $externalNumber);
    }

    /**
     * @param $externalNumber
     * @return WS\wsRezerwacjeResult
     */
    public function showReservation($externalNumber)
    {
        return $this->pokazRezerwacje($this->sesja, $externalNumber);
    }
}
