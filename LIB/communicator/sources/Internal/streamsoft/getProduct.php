<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-24 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use LIB\communicator\sources\Internal\streamsoft\WS\wsCechaKart;
use LIB\communicator\sources\Internal\streamsoft\WS\wsGroup;
use LIB\communicator\sources\Internal\streamsoft\WS\wsKartoteka;
use stdClass;

/**
 * Description of getProduct
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class getProduct extends getProductCMS {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var stdClass
   */
  public $oProduct;

  public function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
    parent::__construct($pDbMgr);
  }
  
  /**
   * 
   * @param int $iProductId
   * @param bool $bForce
   * @return wsKartoteka
   */
  public function getSingleProduct($iProductId, $bForce) {
    $this->oProduct = parent::getSingleProduct($iProductId, $bForce);
    
    if ($this->oProduct->id > 0) {
      return $this->createProductByProductCMS($this->oProduct);
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param string $sStreamsoftIndeks
   * @param bool $bForce
   * @return wsKartoteka
   */
  public function getMultipleProducts($sStreamsoftIndeks, $bForce) {
    
    $this->oProduct = parent::getSingleProductByStramsoftId($sStreamsoftIndeks, $bForce);
    
    if ($this->oProduct->id > 0) {
      return $this->createProductByProductCMS($this->oProduct);
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @return wsKartoteka
   */
  public function getProductAttachmentN() {
    return $this->createProductAttachmentByProductCMS($this->oProduct);
  }
  
  /**
   * 
   * @param stdClass $oProductStd
   * @return wsKartoteka
   */
  private function createProductAttachmentByProductCMS(stdClass $oProductStd) {
    
    $oAttachment = $oProductStd->attachment;
    $aAtt = (array)$oAttachment;
    if (!empty($aAtt)) {
      $sTitle = $oProductStd->name . ' +' . $oAttachment->name;
      $grupa_wydawca = $this->getProductPublisherSeries($oProductStd->ID_publisher, $oProductStd->publisher, $oProductStd->ID_series, $oProductStd->series);
      $grupa_kategoria = $this->getProductCategories($oProductStd->ID_category, $oProductStd->full_categories);
      $sIDT = $oProductStd->id.'0'.$oAttachment->id;
      $sId = trim($sIDT);

      $cechy = new wsCechaKart($sId, '', '', '', 0.00, '', '', '', '', '', '', '', '', $oAttachment->name, '');
      $oProduct = new wsKartoteka($cechy, '', $sTitle, '', $grupa_kategoria, 
              $grupa_wydawca, '', '', 'egz', $oAttachment->price_netto, 
              $oAttachment->price_brutto, $oAttachment->vat.'%', '0',
              mb_strtoupper($sId, 'UTF-8'), $oProductStd->isbn_13, $oProductStd->isbn_10, $oProductStd->ean_13, $sId);
      return $oProduct;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param wsKartoteka $oProduct
   * @param wsKartoteka $oProductAttachment
   * @return $oProduct
   */
  public function assignAttachmentToProduct(wsKartoteka $oProduct, wsKartoteka $oProductAttachment) {
    $oProduct->setIndeks_kartoteki_powiazanej($oProductAttachment->getIndeks());
    return $oProduct;
  }
  
  /**
   * 
   * @param stdClass $oProduct
   * @return wsKartoteka
   */
  private function createProductByProductCMS($oProduct) {
    $grupa_wydawca = $this->getProductPublisherSeries($oProduct->ID_publisher, $oProduct->publisher, $oProduct->ID_series, $oProduct->series);
    $grupa_kategoria = $this->getProductCategories($oProduct->ID_category, $oProduct->full_categories);

    $cechy = new wsCechaKart($oProduct->id, $oProduct->binding, $oProduct->dimension, $oProduct->language, $oProduct->weight, $oProduct->publication_year, 
            $oProduct->pages, $oProduct->volumes, $oProduct->volume_nr, $oProduct->edition, $oProduct->legal_status_date, 
            $oProduct->type, (!empty($oProduct->attachment) ? TRUE : FALSE), (!empty($oProduct->attachment->type) ? $oProduct->attachment->type : ''), $oProduct->isbn_plain);

    $oProduct = $this->clearUniqueIndeks($oProduct);

    $oProductSS = new wsKartoteka($cechy, '', $oProduct->name, $oProduct->name2, $grupa_kategoria, $grupa_wydawca, 
            $oProduct->authors, $oProduct->category, 'egz', $oProduct->price_netto, 
            $oProduct->price_brutto, $oProduct->vat.'%', $oProduct->pkwiu, 
            $oProduct->isbn_plain,
            $oProduct->isbn_13,
            $oProduct->isbn_10,
            $oProduct->ean_13,
            $oProduct->streamsoft_indeks
    );
    return $oProductSS;
  }

  private function clearUniqueIndeks($oProduct) {
      /**
      $this->glowny_ISBN = $glowny_ISBN;
      $this->ISBN_13 = $ISBN_13;
      $this->ISBN_10 = $ISBN_10;
      $this->EAN_13 = $EAN_13;
      $this->indeks = $indeks;
       */

      if ($oProduct->ean_13 == $oProduct->isbn_10) {
          unset($oProduct->isbn_10);
      }
      if ($oProduct->ean_13 == $oProduct->isbn_13) {
          unset($oProduct->isbn_13);
      }
      if ($oProduct->ean_13 == $oProduct->isbn_plain) {
          unset($oProduct->ean_13);
      }

      if ($oProduct->isbn_13 == $oProduct->isbn_10) {
          unset($oProduct->isbn_10);
      }

      if ($oProduct->isbn_plain == $oProduct->isbn_13) {
          unset($oProduct->isbn_13);
      }

      if ($oProduct->isbn_plain == $oProduct->isbn_10) {
          unset($oProduct->isbn_10);
      }
      return $oProduct;
  }
  
  /**
   * 
   * @param int $iPublisherId
   * @param string $sPublisher
   * @param int $iSeriesId
   * @param string $sSeries
   * @return wsGroup
   */
  private function getProductPublisherSeries($iPublisherId, $sPublisher, $iSeriesId, $sSeries) {
    
    if ($iSeriesId > 0) {
      $seria_wydawnicza = new wsGroup($iSeriesId, $sSeries, NULL);
    }
    $grupa_wydawca = new wsGroup($iPublisherId, $sPublisher, $seria_wydawnicza);
    return $grupa_wydawca;
  }
  
  /**
   * 
   * @param string $sCateogiryIDs
   * @param string $sCategory
   * @return wsGroup
   */
  private function getProductCategories($sCateogiryIDs, $sCategory) {
    
    $sSeparator = ' - ';
    $aCategoriesIds = array_reverse(explode($sSeparator, $sCateogiryIDs));
    $aCategories = array_reverse(explode($sSeparator, $sCategory));
    foreach ($aCategoriesIds as $iKey => $iCategoryId) {
      $grupa_kategoria = new wsGroup($iCategoryId, $aCategories[$iKey], $grupa_kategoria);
    }
    if ($grupa_kategoria->getKod_prosty() == '') {
      $grupa_kategoria = new wsGroup('1', 'Dział ogólny', null);
    }
    return $grupa_kategoria;
  }
}