<?php

namespace LIB\communicator\sources\Internal\streamsoft;

abstract class AbstractErpManager
{
  /**
   * @var Order
   */
  protected $order;

  /**
   * @var Reservation
   */
  protected $reservation;

  /**
   * @var DataProvider
   */
  protected $dataProvider;

  private $stockSupplyService;

    public function getOrder()
  {
    return $this->order;
  }

  public function getReservation()
  {
    return $this->reservation;
  }

  public function getDataProvider()
  {
    return $this->dataProvider;
  }

  public function getStockSupplyService()
  {
    return $this->stockSupplyService;
  }
}