<?php

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use Benchmark_Timer;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use LIB\communicator\sources\Internal\streamsoft\WS\wsIntegerResult;
use LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja;
use LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacjeResult;

/**
 * Description of getPZ
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Reservation extends ProfitServerImplService
{

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;


    /**
     * @var void
     */
    private $sesja;

    /**
     * @param DatabaseManager $pDbMgr
     */
    public function __construct(DatabaseManager $pDbMgr)
    {
        global $aConfig;//, $oTimer
        $this->pDbMgr = $pDbMgr;
        if ($aConfig['common']['status'] == 'development') {
            parent::__construct(array(), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
        } else {
            parent::__construct();
        }

        $this->sesja = $this->utworzSesje('webservice', 'profwebservice')->getValue();
    }

    /**
     * @param $externalNumber
     * @return WS\wsIntegerResult
     */
    public function createReservation($externalNumber)
    {

        $rez = $this->zalozRezerwacje($this->sesja, $externalNumber);
        $resultInteger = new wsIntegerResult($rez->getErrorCode(), $rez->getErrorMessage(), $rez->getErrorCode());
        return $resultInteger;
    }

    /**
     * @param $externalNumber
     * @return WS\wsRezerwacjeResult
     */
    public function showReservation($externalNumber)
    {
        $rez = $this->pokazRezerwacje($this->sesja, $externalNumber);
        return $this->groupByIndeks($rez);
    }

    /**
     * @param wsRezerwacjeResult $rez
     * @return array
     */
    private function groupByIndeks($rez) {
        /** @var wsRezerwacja[] $rezPositions */
        $rezPositions = $rez->getValue();
        /** @var wsRezerwacja $rezervation */
        $retRez = [];
        if (null !== $rezPositions) {
            foreach ($rezPositions as $rezervation) {
                $indeksLP = $rezervation->getIndeks().'-'.$rezervation->getLp();
                if (isset($retRez[$indeksLP])) {
                    /** @var wsRezerwacja $current */
                    $current = $retRez[$indeksLP];
                    $current->setIloscrez($current->getIloscrez() + $rezervation->getIloscrez());
                    $retRez[$indeksLP] = $current;
                } else {
                    $retRez[$indeksLP] = $rezervation;
                }
            }
            return $rez->setValue($retRez);
        }
        else {
            return $rez;
        }

    }
}
