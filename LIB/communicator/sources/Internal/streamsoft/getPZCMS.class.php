<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;

/**
 * Description of getPZCMS
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class getPZCMS {

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   * @var DataProvider
   */
  private $dataProvider;

  //put your code here
  // 01
  // PZ
  // 
  // wsPos - ineks kontrahenta,
  // 
  
  public function __construct(DatabaseManager $pDbMgr) {

    $this->dataProvider = new DataProvider($pDbMgr);
    $this->pDbMgr = $pDbMgr;
  }


  /**
   *
   * @param int $iSHId
   * @return array
   */
  public function getItems($iOSHId, $bIsIndependentStock = false) {
    if ($bIsIndependentStock === false) {
      return $this->dataProvider->getItems($iOSHId);
    } else {
      return $this->dataProvider->getItemsByProductId($iOSHId);
    }
  }


  /**
   *
   * @param int $iOSHId
   * @return array
   */
  public function getOSHI($iOSHId) {
    return $this->dataProvider->getOSHI($iOSHId);
  }
  
  /**
   * 
   * @param int $iOSHId
   * @return array
   */
  public function getOSHAI($iOSHId) {
    
    $sSql = 'SELECT streamsoft_id s_index, streamsoft_id as streamsoft_indeks, COUNT(1) as in_confirmed_quantity
             FROM orders_send_history_additional_items
             WHERE orders_send_history_id = '.$iOSHId .' 
             GROUP BY streamsoft_id ';
             ;
    return $this->pDbMgr->GetAssoc('profit24', $sSql, false, array(), DB_FETCHMODE_ASSOC, false);
  }
}
