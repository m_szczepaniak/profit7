<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2016-07-20
 * @copyrights Marcin Chudy - Profit24.pl
 */
use LIB\orders\Payment\Payment;

ini_set('display_errors', 'On');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal/streamsoft';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');


/**
 *
 * @return array
 */
function getPaymentsToERP() {
    global $pDbMgr;

    // type opek
    // type tba
    // type payu
    // pobieramy najpierw standardowe płatności
    $iFedExId = 1825;
    $iTBAId = 9006;
    $iRUCHId = 6690;
    $iPPId = 9267;
    $sSql = '
      SELECT
        BA.streamsoft_id,
        DATE(OPL.date) as c_date,
        IF(OPL.ammount > 0.00, "BP_KROZ", "BW_KROZ2") as `type`,
        IF(OPL.type = "bank_14days", OUA.streamsoft_id,
          IF(OPL.type = "opek", "'.$iFedExId.'",
            IF(OPL.type = "tba", "'.$iTBAId.'",
              IF(OPL.type = "ruch", "'.$iRUCHId.'",
                IF(OPL.type = "poczta_polska" OR OPL.type = "poczta-polska-doreczenie-pod-adres", "'.$iPPId.'", "0")
              )
            )
          )
        ) AS kontrahent,
        ABS(OPL.ammount) AS kwota,
        REPLACE(IF(OPL.ammount > 0, OPL.ammount, 0), ".", ",") as wplata,
        REPLACE(IF(OPL.ammount <= 0, OPL.ammount, 0)*(-1), ".", ",") as wyplata,
        O.order_number,
        OPL.id,
        OPL.title
      FROM orders_payments_list AS OPL
      JOIN orders_bank_statements AS OBS
       ON OPL.statement_id = OBS.id
      JOIN bank_accounts AS BA
       ON BA.ident_statments_file = OBS.ident_statments_file
      JOIN orders AS O
       ON O.id = OPL.order_id
      LEFT JOIN orders_users_addresses AS OUA
        ON OUA.order_id = O.id AND OUA.streamsoft_id IS NOT NULL AND OUA.address_type = "0"
      WHERE
       OPL.statement_id > 0 AND
       OPL.erp_export IS NULL AND
       IF(OPL.type = "bank_14days", (OUA.streamsoft_id > 0 AND OUA.streamsoft_id IS NOT NULL), 1=1) AND
       DATE(OPL.date) >=  DATE(NOW()) - INTERVAL 60 DAY
      GROUP BY OPL.id
              ';
    echo $sSql;
    $aStatements = $pDbMgr->GetAll('profit24', $sSql);
    foreach ($aStatements as $iKey => $aStatment) {
        if ($aStatment['wyplata'] > 0) {
            preg_match('/\d{12} (.*)/', $aStatment['title'], $aMatches);
            $aStatements[$iKey]['order_number'] .= ' '.$aMatches[1];
        }

    }
    return $aStatements;
}


function markAsExportedOBS($iOBSId) {
    global $pDbMgr;

    $aValues = array(
        'erp_export' => "NOW()"
    );
    if ($pDbMgr->Update('profit24', 'orders_bank_statements', $aValues, ' id = '.$iOBSId) === false) {
        return false;
    } else {
        return true;
    }
}


/**
 *
 * @param int $iOrderId
 * @return boolean
 */
function markAsExportedOPL($iOrderId) {
    global $pDbMgr;

    $aValues = array(
        'erp_export' => "NOW()"
    );
    if ($pDbMgr->Update('profit24', 'orders_payments_list', $aValues, ' id = '.$iOrderId) === false) {
        return false;
    } else {
        return true;
    }
}

/**
 *
 * @return array
 */
function getStatementsApproved() {
    global $pDbMgr;

    $iPayUStramsoftId = 6016;
    $sSql = '
      SELECT
        BA.streamsoft_id,
        DATE(OBS.created) as c_date,
        IF(OBS.paid_amount > 0.00, "BP_KROZ", "BW_KROZ2") as `type`,
        IF (BA.streamsoft_id = "03", "'.$iPayUStramsoftId.'", "0") AS kontrahent,
        ABS(OBS.paid_amount) as kwota,
        REPLACE(IF(OBS.paid_amount > 0, OBS.paid_amount, 0), ".", ",") as wplata,
        REPLACE(IF(OBS.paid_amount <= 0, OBS.paid_amount, 0)*(-1), ".", ",") as wyplata,
        IF(OBS.order_nr <> "", OBS.order_nr, OBS.title) as order_number,
        OBS.id
      FROM orders_bank_statements AS OBS
      JOIN bank_accounts AS BA
      ON BA.ident_statments_file = OBS.ident_statments_file
      WHERE
        approved = "1" AND
        erp_export IS NULL AND
        DATE(OBS.created) >=  DATE(NOW()) - INTERVAL 60 DAY
      GROUP BY OBS.id
    ';
    echo $sSql;
    return $pDbMgr->GetAll('profit24', $sSql);
}

/**
 * @param $aPayment
 * @return \LIB\communicator\sources\Internal\streamsoft\WS\wsDokBank
 */
function addBankDoc($aPayment) {

    $oWsBankDoc = new \LIB\communicator\sources\Internal\streamsoft\WS\wsDokBank();
    $oWsBankDoc->setData_dokumentu($aPayment['c_date']);
    $oWsBankDoc->setKod_banku($aPayment['streamsoft_id']);
    $oWsBankDoc->setKod_dokumentu($aPayment['type']);
    $oWsBankDoc->setKwota($aPayment['kwota']);
    $oWsBankDoc->setNumer_kontrahenta($aPayment['kontrahent']);
    $oWsBankDoc->setOpis($aPayment['order_number']);
    return $oWsBankDoc;
}

$oPayment = new Payment($pDbMgr);


$aPaymentData = getPaymentsToERP();
$aStatments = getStatementsApproved();
dump(count($aPaymentData ) + count($aStatments));
$wsBankDoc = [];

$iCountStatements = 0;
$iMaxCount = 40;

$limitedPaymentData = [];
if (!empty($aPaymentData)) {
    foreach ($aPaymentData as $aPayment) {
        if ($iCountStatements > $iMaxCount) {
            break;
        }
        $limitedPaymentData[] = $aPayment;
        $wsBankDoc[] = addBankDoc($aPayment);
        $iCountStatements++;
    }
}
$aPaymentData = $limitedPaymentData;

$iMaxCount = 30;
$iCountStatements = 0;

$limitedStatments = [];
if (!empty($aStatments)) {
    foreach ($aStatments as $aPayment) {
        if ($iCountStatements > $iMaxCount) {
            break;
        }
        $limitedStatments[] = $aPayment;
        $wsBankDoc[] = addBankDoc($aPayment);
        $iCountStatements++;
    }
}
$aStatments = $limitedStatments;

/*
 * DODAC do crontaba co 30 min
 *
 // TOTO ODKOMENTOWAC !!!!!!!!!!!!!
*/
if ($aConfig['common']['status'] == 'development') {
    $oWS = new \LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService(array('force_socket_timeout' => '10000'), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
} else {
    $oWS = new \LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService(array('force_socket_timeout' => '10000'));
}

$sesja = $oWS->utworzSesje('webservice', 'profwebservice')->getValue();
$result = $oWS->dodajDokumentyBankowe($sesja, $wsBankDoc);

//file_put_contents($_SERVER['DOCUMENT_ROOT'].'/../import_logi/statements_auto_'.date('YmdHis').'.log', var_export($wsBankDoc, true) . " \n STATMENTS: ".var_export($wsBankDoc, true)." \n PAYMENTS: ".var_export($aPaymentData, true));
if ($result->getErrorCode() != 0) {

    \Common::sendMail('raporty@profit24.pl', 'raporty@profit24.pl', 'a.golba@profit24.pl;l.jaskiewicz@profit24.pl;raporty@profit24.pl;k.malz@profit24.pl', 'Błąd auto przelewy', var_export($wsBankDoc, true) . " \n STATMENTS: ".var_export($wsBankDoc, true)." \n PAYMENTS: ".var_export($aPaymentData, true));
    throw new Exception($result->getErrorMessage(), $result->getErrorCode());
} else if (!empty($aPaymentData) || !empty($aStatments)) {

    // TOTO USUNAC TRANSAKCJE !!!!!!!!!!!!!
    //\Common::BeginTransaction();
    if (!empty($aPaymentData)) {
        foreach ($aPaymentData as $aPayment) {
            markAsExportedOPL($aPayment['id']);
        }
    }

    if (!empty($aStatments)) {
        foreach ($aStatments as $aStatement) {
            markAsExportedOBS($aStatement['id']);
        }
    }
    // TOTO USUNAC TRANSAKCJE !!!!!!!!!!!!!
    //\Common::RollbackTransaction();
}