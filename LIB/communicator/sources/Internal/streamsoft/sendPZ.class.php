<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use DateTime;
use Exception;
use Benchmark_Timer;
use LIB\communicator\sources\Internal\streamsoft\getPZCMS;
use LIB\communicator\sources\Internal\streamsoft\WS\ProfitServerImplService;
use LIB\communicator\sources\Internal\streamsoft\WS\wsCechaDok;
use LIB\communicator\sources\Internal\streamsoft\WS\wsIntegerResult;
use LIB\communicator\sources\Internal\streamsoft\WS\wsNagl;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPoz;
use LIB\communicator\sources\Internal\streamsoft\WS\wsPozArray;

/**
 * Description of getPZ
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class sendPZ extends ProfitServerImplService {

    /**
     * @var DataProvider
     */
  protected $dataProvider;

    /**
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  
  /**
   *
   * @var void 
   */
  private $sesja;
  
  /**
   * 
   * @param \LIB\communicator\sources\Internal\streamsoft\DatabaseManager $pDbMgr
   * @global Benchmark_Timer $oTimer
   */
  public function __construct(DatabaseManager $pDbMgr) {
    global $aConfig;//, $oTimer
    $this->pDbMgr = $pDbMgr;
    if ($aConfig['common']['status'] == 'development') {
      parent::__construct(array(), 'http://213.216.85.50:3030/WebServiceProTest/ServerProfit?wsdl');
    } else {
      parent::__construct();
    }
//    $oTimer->setMarker('Start utworzSesje');
    $this->sesja = $this->utworzSesje('webservice', 'profwebservice')->getValue();
    $this->dataProvider = new DataProvider($pDbMgr);
//    $oTimer->setMarker('Stop utworzSesje');
  }
  
  /**
   * 
   * @param int $iOSHI
   * @var wsIntegerResult $result
   */
  public function doSendPZ($iOSHI) {
    
    $oGetPZCMS = new getPZCMS($this->pDbMgr);

    
    $aOSHIData = $oGetPZCMS->getOSHI($iOSHI);
    $bIsIndependentStock = $this->isIndependentStock($aOSHIData);
    $naglowek = $this->getNaglowek($aOSHIData);
    $pozycje = $this->getPozycjeArray($oGetPZCMS, $iOSHI, $bIsIndependentStock);
    $result = $this->utworzDokument($this->sesja, $naglowek, $pozycje, ($bIsIndependentStock === true ? 'M02' : 'M01'));
    if ($result->getErrorCode() != 0) {
      throw new Exception($result->getErrorMessage(), $result->getErrorCode());
    }
    return $result->getValue();
  }
  
  /**
   * 
   * @param array $aOSHIData
   * @return bool
   */
  private function isIndependentStock($aOSHIData) {
    
    return $aOSHIData['independent_stock'] == '1' ? true : false;
  }
  
  /**
   * 
   * @param getPZCMS $oGetPZCMS
   * @param int $iOSHI
   * @return wsPozArray
   */
  private function getPozycjeArray($oGetPZCMS, $iOSHI, $bIsIndependentStock) {
    $aOSHItems = $oGetPZCMS->getItems($iOSHI, $bIsIndependentStock);
    $pozycje = new wsPozArray();
    $aItems = array();
    $aStreamsoftIDs = array();
    
    $oSynchronizeProduct = new synchroProduct($this->pDbMgr, $this->sesja);
    
    foreach ($aOSHItems as $aItem) {
        if ($aItem['supplies'] != '') {

        } else {
            if ($aItem['in_confirmed_quantity'] > 0 && false === $this->dataProvider->checkProductInStreamsoft($aItem['product_id'])) {
                $aStreamsoftIDs[] = $aItem['streamsoft_indeks'];
            }
        }
    }
    if (!empty($aStreamsoftIDs)) {
      $oSynchronizeProduct->groupCheckProductsAndAddNew($aStreamsoftIDs);
    }
    
    $aOSHItems = $this->addAdditionalIndeksOSH($oGetPZCMS, $aOSHItems, $iOSHI);
    
    foreach ($aOSHItems as $aItem) {
      if ($aItem['in_confirmed_quantity'] > 0) {
        $iProductId = $aItem['product_id'];
//        $oSynchronizeProduct->trySynchronizeProduct($iProductId, true, true);
        $aItems[] = new wsPoz(strtoupper($aItem['streamsoft_indeks']), $aItem['in_confirmed_quantity'], FALSE, 
                $aItem['price_netto'], 
                ($aItem['price_netto'] > 0.00 ? TRUE : FALSE), 
                0, 
                '01', '', $aItem['item_source_id'], '', '', 
                new DateTime(), '', '', '');
      }
    }
    $pozycje->setItem($aItems);
    return $pozycje;
  }
  
  
  /**
   * 
   * @param getPZCMS $oGetPZCMS
   * @param array $aOSHItems
   * @param int $iOSHId
   * @return array
   */
  private function addAdditionalIndeksOSH(getPZCMS $oGetPZCMS, $aOSHItems, $iOSHId) {
    $aAdditionalIndex = $oGetPZCMS->getOSHAI($iOSHId);
    if (!empty($aAdditionalIndex)) {
      foreach ($aOSHItems as $iKey => $aItem) {
        $iSIndex = $aItem['streamsoft_indeks'];
        if(isset($aAdditionalIndex[$iSIndex])) {
          // ten sam tytuł
          $aOSHItems[$iKey]['in_confirmed_quantity'] += $aAdditionalIndex[$iSIndex]['in_confirmed_quantity'];
          unset($aAdditionalIndex[$iSIndex]);
        }
      }
    
      if (!empty($aOSHItems)) {
        $aOSHItems = array_merge($aOSHItems, $aAdditionalIndex);
      } else {
        $aOSHItems = $aAdditionalIndex;
      }
    }
    return $aOSHItems;
  }
  
  /**
   * 
   * @param getPZCMS $oGetPZCMS
   * @param int $iOSHI
   * @return wsNagl
   */
  private function getNaglowek($aOSHIData) {
    
    $cechy = new wsCechaDok($aOSHIData['number'], '', '', '', '');
    $docType = $this->getDocType($aOSHIData['in_document_type']);
    $kontrahent = new WS\wsKontrah($aOSHIData['streamsoft_id'], '', '', '', '', '', '', '', '', '', '', '', '');
    $dateDoc = $aOSHIData['date_pz'] != '' ? new DateTime($aOSHIData['date_pz']) : new DateTime() ;
    $naglowek = new wsNagl($kontrahent, $docType, 'PZ', $aOSHIData['fv_nr'],
          $dateDoc,
          '', '', new DateTime(),
            $aOSHIData['number'], '', $cechy);
    return $naglowek;
  }
  
  /**
   * 
   * @param string $cDocType
   * @return string
   */
  private function getDocType($cDocType) {
    
    if ($cDocType == '2') {
      return 'PZK';
    } else {
      return 'PZ';
    }
  }
}
