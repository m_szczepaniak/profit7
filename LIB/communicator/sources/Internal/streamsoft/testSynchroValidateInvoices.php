<?php

use LIB\communicator\sources\Internal\streamsoft\validateInvoice;

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-07
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal/streamsoft';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');

/**
 *
 * @return array
 */
function getOrdersSynchroTodayTEST() {
    global $pDbMgr;

    $sSql = 'SELECT id, order_number, invoice_id, invoice2_id,
                value_netto, value_brutto, transport_cost, transport_vat,
                to_pay, second_invoice
             FROM orders AS O
             WHERE
              O.invoice_id IS NOT NULL
              AND O.order_status IN("4", "3")
              AND O.erp_export IS NULL
              AND O.order_date > "2015-09-25"
             ';
    //               AND O.erp_send_ouz IS NOT NULL
    return $pDbMgr->GetAll('profit24', $sSql);
}

global $pDbMgr;
$oValidateInvoice = new validateInvoice($pDbMgr);
$i=0;
$aOrders = getOrdersSynchroTodayTEST();
foreach ($aOrders as $aOrder) {

    echo $i.':' .$aOrder['invoice_id']."\n";

    $wsZestVatResult = $oValidateInvoice->pobierzZestVat($oValidateInvoice->sesja, $aOrder['invoice_id']);
    var_dump($wsZestVatResult);
    $i++;
}
echo 'koniec';