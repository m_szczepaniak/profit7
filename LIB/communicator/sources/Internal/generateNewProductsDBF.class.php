<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Internal';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');

/**
 * Klasa generuje DBF z produktami, które zostały zamówione po raz pierwszy.
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-07-04 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class generateNewProductsDBF {
  private $pDbMgr;
  
  function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  
    $this->getNewProducts();
  }
  
  private function getNewProducts() {
    $sSql = '
SELECT OI.*, P.ean_13, P.isbn_plain, P.pkwiu
FROM orders_items AS OI
JOIN orders AS O
  ON OI.order_id = O.id
JOIN products AS P
  ON OI.product_id = P.id
WHERE 
  O.order_date >= DATE_SUB(DATE(NOW()), INTERVAL 1 DAY) AND
  OI.source <> "1" AND
  OI.source <> "0" AND
  OI.source <> "51" AND 
  OI.source <> "52" AND 
  P.prod_status <> "3"
';
    /*
 AND
(
  SELECT O2.id FROM orders_items AS OI2
  LEFT JOIN orders AS O2 
    ON OI2.order_id = O2.id 
    AND O2.order_date < DATE_SUB(DATE(NOW()), INTERVAL 1 DAY)
    AND O2.order_status = "4"
  WHERE OI.product_id = OI2.product_id
  LIMIT 1
) IS NULL
     */
    $aOrdersItems = $this->pDbMgr->GetAll('profit24', $sSql);
    // porownujemy z DBF
    $aOProductDBFItems = $this->getProductsToDBF($aOrdersItems);
    $this->doParse($aOProductDBFItems);
  }// end of getNewProducts() method
  
  
  /**
   * Metoda pobiera produkty w formacie zgodnym z eksportem DBF
   * 
   * @param array $aOrdersItems
   * @return \communicator\sources\productToDBF[]
   */
  public function getProductsToDBF($aOrdersItems) {
    $aOProductsToDBF = array();
    
    foreach ($aOrdersItems as $aItem) {
      $oProductToDFB = new \communicator\sources\productToDBF();
      $oProductToDFB->indexDostawcy = (string)($aItem['isbn_plain'].$aItem['id']);
      $oProductToDFB->tytul = (string)$aItem['name'];
      $oProductToDFB->isbn = (string)$aItem['isbn_plain'];
      $oProductToDFB->ean_13 = (string)$aItem['ean_13'];
      $oProductToDFB->pkwiu = (string)($aItem['pkwiu'] != '' ? $aItem['pkwiu'] : '58.11.1'); //(string)$oPozycja->towar->pkwiu;
      $oProductToDFB->cena_det = (string)$aItem['price_netto'];
      $oProductToDFB->cena_net = (string)'0.00';
      $oProductToDFB->vat = (string)$aItem['vat'];
      $oProductToDFB->ilosc = (string)0;
      $aOProductsToDBF[] = $oProductToDFB;
    }
    return $aOProductsToDBF;
  }// end of getProductsToDBF() method
  
  
  /**
   * Metoda porównuje nowo zamówione produkty z plikiem DBF
   * 
   * @param array $aOProductDBFItems
   */
  private function doParse($aOProductDBFItems) {
    $aNewArr['items'] = $aOProductDBFItems;
    $aNewArr['add_number'] = date("ymd");
    $oParseProductsToDBF = new \communicator\sources\parseProductsToDBF($aNewArr, __DIR__.'/files_DBF/');
    echo $oParseProductsToDBF->doParse('', true);
  }
}
global $pDbMgr;
$oGetNewProducts = new generateNewProductsDBF($pDbMgr);