<?php
namespace communicator\sources\Internal;

class Shipment {

    /**
     * Wygenerowanie naszej etykiety
     *
     * @param $orderNumber int Numer zamówienia
     * @param $errorMessage
     */
    public function getProfitLabel($orderNumber, $errorMessage) {
        global $aConfig;

        $aLang =& $aConfig['lang'][$this->sModule];

        require_once('tcpdf/config/lang/pl.php');
        require_once('tcpdf/tcpdf.php');

        // create new PDF document
        $pdf = new \TCPDF ('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('profit24');
        $pdf->SetTitle($aLang['title']);
        $pdf->SetSubject($aLang['title']);
        $pdf->SetKeywords('');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf->SetAutoPageBreak(FALSE);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray(array());

        // set font
        $pdf->SetFont('freesans', '', 80);

        // add a page
        $pdf->AddPage();

        $this->getBarcode($orderNumber);

        $pdf->Image($_SERVER['DOCUMENT_ROOT'].'/tmp/barcode_'.$orderNumber.'.png', 0, 0, 297, 80, 'PNG', '', '', true, 150, '', false, false, 1, false, false, false);

        $pdf->Text(15, 100, $orderNumber, false, false, true, 0, 0, 'C', false,'',0,false,'M','M');

        // set font
        $pdf->SetFont('freesans', '', 18);

        $pdf->SetY(115);

        $html = "<table><tr><td style=\"text-align: center; line-height: 150%\">" . $errorMessage . "</td></tr></table>";

        $pdf->writeHTML($html);
//        $pdf->Text(15, 130, $errorMessage, false, false, true, 0, 0, 'C', false,'',0,false,'M','M');

        $sFilename = "etykieta_".$orderNumber.".pdf";

        $sFilePath = $aConfig['common']['client_base_path'].'/'.$aConfig['common']['cms_dir'].$aConfig['common']['internal_label_dir'];

        if(false === file_exists($sFilePath)) {
            @mkdir ($sFilePath, 0755);
            @chmod($sFilePath, 0755);
        }
        $pdf->Output($sFilePath.$sFilename, 'F');
        @unlink($_SERVER['DOCUMENT_ROOT'].'/tmp/barcode_'.$orderNumber.'.png');
    }

    /**
     * Wygenerowanie kodu kreskowego używając numeru zamówienia
     *
     * @param $orderNumber int Numer zamówienia
     */
    function getBarcode($orderNumber) {
        //kod kreskowy
        include_once 'barcode/barcode.php';

        $sCode = str_pad($orderNumber, 13, '0', STR_PAD_LEFT);
        $im     = imagecreatetruecolor(300, 100);
        $black  = ImageColorAllocate($im,0x00,0x00,0x00);
        $white  = ImageColorAllocate($im,0xff,0xff,0xff);
        imagefilledrectangle($im, 0, 0, 300, 100, $white);
        $data = \Barcode::gd($im, $black, 150, 50, 0, "code128", $sCode, 2, 50);

        ob_start();
        imagepng($im);
        imagedestroy( $im );
        $imagedata = ob_get_clean();

        $sBarCodePath = $_SERVER['DOCUMENT_ROOT'].'/tmp/barcode_'.$orderNumber.'.png';
        file_put_contents($sBarCodePath,$imagedata);
    }

    /**
     * Metoda ustawia numer listu przewozowego w zamóweniu
     *
     * @param \communicator\sources\PocztaPolska\type $iOrderId
     * @param \communicator\sources\PocztaPolska\type $sTransportNumber
     * @param bool $isTmp
     * @return bool
     * @throws \Exception
     * @global \DatabaseManager $pDbMgr
     */
    public function updateOrderTransportNumber($iOrderId, $sTransportNumber, $isTmp = false) {
        global $pDbMgr;

        $aValues = array(
            'transport_number' => $sTransportNumber,
        );

        if (true === $isTmp) {
            $aValues['tmp_transport_number'] = '1';
        } else {
            $aValues['tmp_transport_number'] = '0';
        }

        if ($pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOrderId) === false) {
            throw new \Exception(_('Wystąpił błąd podczas zapisywania numeru listu przewozowego'));
        }
        return true;
    }// end of _updateOrderTransportNumber() method

    /**
     * @param $agConfig
     * @param $sTransportNumber
     * @return string
     */
    public function getTransportListFilename($agConfig, $sTransportNumber) {
        global $aConfig;

        return $aConfig['common']['client_base_path'].'/'.$aConfig['common']['cms_dir'].$aConfig['common']['internal_label_dir'] . "etykieta_".$sTransportNumber.".pdf";
    }
}