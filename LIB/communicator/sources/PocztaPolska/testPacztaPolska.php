<?php

//ini_set('display_errors', 'On');
//error_reporting(E_ALL);
/*
 * Testy poczty-polskiej
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-24 
 * @copyrights Marcin Chudy - Profit24.pl
 */
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/orders';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';
if ($aConfig['common']['status']	!= 'development') {
  $bTestMode = false;
} else {
  $bTestMode = true;
}
$sSql = 'SELECT * FROM orders_seller_data';
$aSellerData = $pDbMgr->GetRow('profit24', $sSql);

$bTestMode = true;// XXX bo na produkcyjnej testuję
$oPocztaPolska = new communicator\sources\PocztaPolska\PocztaPolska($bTestMode);
try {
  
  $aOrder = array(
      'id' => 132691,
      'weight' => 22.1,
      'point_of_receipt' => '255445'
  );
  $aDeliverAddress = array(
      'name' => 'Arkadiusz',
      'surname' => 'Golba',
      'phone' => '690892692',
      'email' => 'arekgolba@gmail.com',
      
      'street' => 'Pulaskiego',
      'number' => '8',
      'number2' => '18',
      'postal' => '37-500',
      'city' => 'WARSZAWA',

  );
//  $sTransprotNumber = $oPocztaPolska->addShipment($aOrder, $aDeliverAddress, $aSellerData);
//  // @TODO dodać pobranie nr bufora
  //$oPocztaPolska->sendEnvelope($bufor);
  
  $oPocztaPolska->getAddressLabel('./', 132691);
//  $oPocztaPolska->testDodaniaPrzesylki();
} catch (\Exception $ex) {
  echo $ex->getMessage();
}
//echo "REQUEST:\n" . $oPocztaPolska->en->__getLastRequestHeaders() ."\n\n". $oPocztaPolska->en->__getLastRequest() . "\n";

//echo "Response:\n" . $oPocztaPolska->en->__getLastRequestHeaders(). "\n\n". $oPocztaPolska->en->__getLastResponse() . "\n";
