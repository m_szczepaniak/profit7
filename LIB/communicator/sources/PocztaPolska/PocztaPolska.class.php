<?php
/**
 * Spedycja - Poczta Polska
 * 
 * @author Paweł Bromka
 * @created 2014-11-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\PocztaPolska;
class PocztaPolska implements \communicator\sources\iShipment {

  private $pDbMgr;
  private $bTestMode;
  
  /**
   *
   * @var ElektronicznyNadawca $oEN
   */
  public $en;
  
  private $guid;
  
  private $sAutoBufferName = "auto-buffor %s";
  private $iTransportId = 4;// symbol 'poczta_polska'
  CONST KWOTA_UBEZPIECZENIA = 100000;
  
  function __construct($bTestMode = false, $pDbMgr = null) {
    $this->bTestMode = $bTestMode;
    $this->pDbMgr = $pDbMgr;
  }
  
  public function hello() {
    $oHello = new \hello();
    $oHello->in='Arek';
    var_dump($this->en->hello($oHello));
  }

  public function doCancelShipment() {
    
  }


  
  /**
   * Sprawdzenie statusu przesyłki
   * 
   * @param int $iId
   * @param string $sTransportNumber
   * @return array
   */
  public function getDeliveryStatus($iId, $sTransportNumber) {
  }

  
  public function getDestinationPointsList() {
    
  }


  
  
  
  

  
  
  public function testPobraniaNalepki()
  {

  }


  



  
}
