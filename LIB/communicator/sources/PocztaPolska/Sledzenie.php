<?php
class maksymalnaLiczbaPrzesylekResponse {
	/** @var int */	public $return;
}

class sprawdzPrzesylke {
	/** @var string */	public $numer;
}

class sprawdzPrzesylkeResponse {
	/** @var Przesylka */	public $return;
}

class sprawdzPrzesylkePl {
	/** @var string */	public $numer;
}

class sprawdzPrzesylkePlResponse {
	/** @var Przesylka */	public $return;
}

class sprawdzPrzesylki {
	/** @var string */	public $numer;
}

class sprawdzPrzesylkiResponse {
	/** @var Komunikat */	public $return;
}

class sprawdzPrzesylkiPl {
	/** @var string */	public $numer;
}

class sprawdzPrzesylkiPlResponse {
	/** @var Komunikat */	public $return;
}

class sprawdzPrzesylkiOdDo {
	/** @var string */	public $numer;
	/** @var string */	public $odDnia;
	/** @var string */	public $doDnia;
}

class sprawdzPrzesylkiOdDoResponse {
	/** @var Komunikat */	public $return;
}

class sprawdzPrzesylkiOdDoPl {
	/** @var string */	public $numer;
	/** @var string */	public $odDnia;
	/** @var string */	public $doDnia;
}

class sprawdzPrzesylkiOdDoPlResponse {
	/** @var Komunikat */	public $return;
}

class wersjaResponse {
	/** @var string */	public $return;
}

class witaj {
	/** @var string */	public $imie;
}

class witajResponse {
	/** @var string */	public $return;
}

class Przesylka {
	/** @var DanePrzesylki */	public $danePrzesylki;
	/** @var string */	public $numer;
	/** @var int */	public $status;
}

class DanePrzesylki {
	/** @var date */	public $dataNadania;
	/** @var string */	public $kodKrajuNadania;
	/** @var string */	public $kodKrajuPrzezn;
	/** @var string */	public $kodRodzPrzes;
	/** @var string */	public $krajNadania;
	/** @var string */	public $krajPrzezn;
	/** @var float */	public $masa;
	/** @var string */	public $numer;
	/** @var string */	public $rodzPrzes;
	/** @var Jednostka */	public $urzadNadania;
	/** @var Jednostka */	public $urzadPrzezn;
	/** @var boolean */	public $zakonczonoObsluge;
	/** @var ListaZdarzen */	public $zdarzenia;
}

class Jednostka {
	/** @var SzczDaneJednostki */	public $daneSzczegolowe;
	/** @var string */	public $nazwa;
}

class SzczDaneJednostki {
	/** @var float */	public $dlGeogr;
	/** @var GodzinyPracy */	public $godzinyPracy;
	/** @var string */	public $miejscowosc;
	/** @var string */	public $nrDomu;
	/** @var string */	public $nrLokalu;
	/** @var string */	public $pna;
	/** @var float */	public $szerGeogr;
	/** @var string */	public $ulica;
}

class GodzinyPracy {
	/** @var GodzinyZUwagami */	public $dniRobocze;
	/** @var GodzinyZUwagami */	public $niedzISw;
	/** @var GodzinyZUwagami */	public $soboty;
}

class GodzinyZUwagami {
	/** @var string */	public $godziny;
	/** @var string */	public $uwagi;
}

class ListaZdarzen {
	/** @var Zdarzenie */	public $zdarzenie;
}

class Zdarzenie {
	/** @var string */	public $czas;
	/** @var Jednostka */	public $jednostka;
	/** @var string */	public $kod;
	/** @var boolean */	public $konczace;
	/** @var string */	public $nazwa;
	/** @var Przyczyna */	public $przyczyna;
}

class Przyczyna {
	/** @var string */	public $kod;
	/** @var string */	public $nazwa;
}

class Komunikat {
	/** @var ListaPrzesylek */	public $przesylki;
	/** @var int */	public $status;
}

class ListaPrzesylek {
	/** @var Przesylka */	public $przesylka;
}


/**
 * Sledzenie class
 * 
 *  
 * 
 * @author    {author}
 * @copyright {copyright}
 * @package   {package}
 */
class Sledzenie extends \SoapClient {

	const WSDL_FILE = "sledzenie.wsdl";
	private $classmap = array(
			'maksymalnaLiczbaPrzesylekResponse' => 'maksymalnaLiczbaPrzesylekResponse',
			'sprawdzPrzesylke' => 'sprawdzPrzesylke',
			'sprawdzPrzesylkeResponse' => 'sprawdzPrzesylkeResponse',
			'sprawdzPrzesylkePl' => 'sprawdzPrzesylkePl',
			'sprawdzPrzesylkePlResponse' => 'sprawdzPrzesylkePlResponse',
			'sprawdzPrzesylki' => 'sprawdzPrzesylki',
			'sprawdzPrzesylkiResponse' => 'sprawdzPrzesylkiResponse',
			'sprawdzPrzesylkiPl' => 'sprawdzPrzesylkiPl',
			'sprawdzPrzesylkiPlResponse' => 'sprawdzPrzesylkiPlResponse',
			'sprawdzPrzesylkiOdDo' => 'sprawdzPrzesylkiOdDo',
			'sprawdzPrzesylkiOdDoResponse' => 'sprawdzPrzesylkiOdDoResponse',
			'sprawdzPrzesylkiOdDoPl' => 'sprawdzPrzesylkiOdDoPl',
			'sprawdzPrzesylkiOdDoPlResponse' => 'sprawdzPrzesylkiOdDoPlResponse',
			'wersjaResponse' => 'wersjaResponse',
			'witaj' => 'witaj',
			'witajResponse' => 'witajResponse',
			'Przesylka' => 'Przesylka',
			'DanePrzesylki' => 'DanePrzesylki',
			'Jednostka' => 'Jednostka',
			'SzczDaneJednostki' => 'SzczDaneJednostki',
			'GodzinyPracy' => 'GodzinyPracy',
			'GodzinyZUwagami' => 'GodzinyZUwagami',
			'ListaZdarzen' => 'ListaZdarzen',
			'Zdarzenie' => 'Zdarzenie',
			'Przyczyna' => 'Przyczyna',
			'Komunikat' => 'Komunikat',
			'ListaPrzesylek' => 'ListaPrzesylek',
			);

	public function __construct($wsdl = null, $options = array()) {
		foreach($this->classmap as $key => $value) {
			if(!isset($options['classmap'][$key])) {
				$options['classmap'][$key] = $value;
			}
		}
		if(isset($options['headers'])) {
			$this->__setSoapHeaders($options['headers']);
		}
		parent::__construct($wsdl ?: self::WSDL_FILE, $options);
	}

	/**
	 *  
	 *
	 * @param 
	 * @return wersjaResponse
	 */
	public function wersja() {
		return $this->__soapCall('wersja', array(), array(
						'uri' => 'http://sledzenie.pocztapolska.pl',
						'soapaction' => ''
					)
			);
	}

	/**
	 *  
	 *
	 * @param sprawdzPrzesylkiPl $parameters
	 * @return sprawdzPrzesylkiPlResponse
	 */
	public function sprawdzPrzesylkiPl(sprawdzPrzesylkiPl $parameters) {
		return $this->__soapCall('sprawdzPrzesylkiPl', array($parameters), array(
						'uri' => 'http://sledzenie.pocztapolska.pl',
						'soapaction' => ''
					)
			);
	}

	/**
	 *  
	 *
	 * @param sprawdzPrzesylkePl $parameters
	 * @return sprawdzPrzesylkePlResponse
	 */
	public function sprawdzPrzesylkePl(sprawdzPrzesylkePl $parameters) {
		return $this->__soapCall('sprawdzPrzesylkePl', array($parameters), array(
						'uri' => 'http://sledzenie.pocztapolska.pl',
						'soapaction' => ''
					)
			);
	}

	/**
	 *  
	 *
	 * @param sprawdzPrzesylkiOdDo $parameters
	 * @return sprawdzPrzesylkiOdDoResponse
	 */
	public function sprawdzPrzesylkiOdDo(sprawdzPrzesylkiOdDo $parameters) {
		return $this->__soapCall('sprawdzPrzesylkiOdDo', array($parameters), array(
						'uri' => 'http://sledzenie.pocztapolska.pl',
						'soapaction' => ''
					)
			);
	}

	/**
	 *  
	 *
	 * @param 
	 * @return maksymalnaLiczbaPrzesylekResponse
	 */
	public function maksymalnaLiczbaPrzesylek() {
		return $this->__soapCall('maksymalnaLiczbaPrzesylek', array(), array(
						'uri' => 'http://sledzenie.pocztapolska.pl',
						'soapaction' => ''
					)
			);
	}

	/**
	 *  
	 *
	 * @param sprawdzPrzesylke $parameters
	 * @return sprawdzPrzesylkeResponse
	 */
	public function sprawdzPrzesylke(sprawdzPrzesylke $parameters) {
		return $this->__soapCall('sprawdzPrzesylke', array($parameters), array(
						'uri' => 'http://sledzenie.pocztapolska.pl',
						'soapaction' => ''
					)
			);
	}

	/**
	 *  
	 *
	 * @param sprawdzPrzesylkiOdDoPl $parameters
	 * @return sprawdzPrzesylkiOdDoPlResponse
	 */
	public function sprawdzPrzesylkiOdDoPl(sprawdzPrzesylkiOdDoPl $parameters) {
		return $this->__soapCall('sprawdzPrzesylkiOdDoPl', array($parameters), array(
						'uri' => 'http://sledzenie.pocztapolska.pl',
						'soapaction' => ''
					)
			);
	}

	/**
	 *  
	 *
	 * @param witaj $parameters
	 * @return witajResponse
	 */
	public function witaj(witaj $parameters) {
		return $this->__soapCall('witaj', array($parameters), array(
						'uri' => 'http://sledzenie.pocztapolska.pl',
						'soapaction' => ''
					)
			);
	}

	/**
	 *  
	 *
	 * @param sprawdzPrzesylki $parameters
	 * @return sprawdzPrzesylkiResponse
	 */
	public function sprawdzPrzesylki(sprawdzPrzesylki $parameters) {
		return $this->__soapCall('sprawdzPrzesylki', array($parameters), array(
						'uri' => 'http://sledzenie.pocztapolska.pl',
						'soapaction' => ''
					)
			);
	}

}


