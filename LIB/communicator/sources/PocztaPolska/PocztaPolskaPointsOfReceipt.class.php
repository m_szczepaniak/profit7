<?php

namespace communicator\sources\PocztaPolska;

class PocztaPolskaPointsOfReceipt
{

  private $bTest;
  
  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;

  private $CSV_PATH;

	public function __construct($pDbMgr, $bTest) {

    $this->pDbMgr = $pDbMgr;
    $this->bTest = $bTest;
    $this->CSV_PATH = __DIR__.'/csv/';
  }


  /**
   * Metoda nieużywana - pobiera z WebApi i zwraca listę urzędów wydających e-przesyłki
   * Odeszliśmy od tego sposobu na rzecz parsowania pliku csv - mamy tam więcej danych
   * 
   * @return array
   */  
	public function getUrzedyWydajaceEPrzesylki() {
    $response = $this->en->getUrzedyWydajaceEPrzesylki(new getUrzedyWydajaceEPrzesylki());
    
    return $response->urzadWydaniaEPrzesylki;
	}
  
  
  /**
   * Metoda nieużywana - zwraca pobraną z WebApi listę urzędów wydających e-przesyłki w formie tablicy. 
   * Odeszliśmy od tego sposobu na rzecz parsowania pliku csv - mamy tam więcej danych
   * 
   * @return array
   */  
	public function getUrzedyWydajaceEPrzesylkiArray() {
    $aDataItems = array();
    
    $response = $this->en->getUrzedyWydajaceEPrzesylki(new getUrzedyWydajaceEPrzesylki());
    $response = $response->urzadWydaniaEPrzesylki;

    foreach ($response as $row) {
      
      $id = $row->id;
      $wojewodztwo = $row->wojewodztwo;
      $powiat = $row->powiat;
      $miejsce = $row->miejsce;
      $kodPocztowy = $row->kodPocztowy;
      $miejscowosc = $row->miejscowosc;        
      $ulica = $row->ulica;
      $numerDomu = $row->numerDomu;
      
      $aDataItems[] = array(
          'id' => $id,
          'province' => $wojewodztwo,
          'district' => $powiat,
          'place' => $miejsce,
          'postcode' => $kodPocztowy,
          'city' => $miejscowosc,
          'street' => $ulica,
          'street_number' => $numerDomu,
      );
    }
    
    return $aDataItems;
  }
  
  
   /**
   * Metoda nadpisuje w bazie aktualną listę urzędów wydających e-przesyłki - dane o urzędach pochodzą z pobranego pliku csv
   * 
   * @param array - $aDataItems - tablica danych o urzędach pocztowych
   * @return bool
   */  
	public function doInsertUrzedyWydajaceEPrzesylki($aDataItems) {
    
    if (!$aDataItems) {
      return false;
    }

    if ($this->bTest === true) {
      $time_start = microtime(true); // pomiar czasu  
    }

    $sSql = "DELETE FROM shipping_points_poczta WHERE id > 0";
    $this->pDbMgr->Query('profit24', $sSql);  
    
    foreach ($aDataItems as $aDataItem) {
      
      if ($this->pDbMgr->Insert('profit24', 'shipping_points_poczta', $aDataItem) === false) {
        return false;
      }
    }
    
    if ($this->bTest === true) {
      $time_end = microtime(true);
      $execution_time = ($time_end - $time_start)/60;
      echo '<b>Czas wykonania insertu:</b> '.$execution_time.' Mins'; // pomiar czasu  
    }
  }  
  
  
  /**
   * Metoda pobiera plik csv zip z serwera Poczty Polskiej - http://odbiorwpunkcie.poczta-polska.pl/pliki.php?t=csv
   * 
   * @return bool
   */  
	public function doDownloadUrzedyWydajaceEPrzesylkiCsvZip() {

    $remoteFile = 'http://odbiorwpunkcie.poczta-polska.pl/pliki.php?t=csv';
    $downloadedFile = $this->CSV_PATH.'UrzedyWydajaceEPrzesylki.zip';

    $sStr = file_get_contents($remoteFile);
    if ($sStr !== false && $sStr != '') {
      if (file_put_contents($downloadedFile, $sStr) === false) {
        return false;
      }
    } else {
      return false;
    }

    return true;
  }
  
  
   /**
   * Metoda rozpakowuje plik csv z archiwum zip
   * 
   */  
	public function doExtractUrzedyWydajaceEPrzesylkiCsvZip() {

    $zip = zip_open($this->CSV_PATH.'UrzedyWydajaceEPrzesylki.zip');
    
    if ($zip) {
      while ($zip_entry = zip_read($zip)) {
        $fp = fopen($this->CSV_PATH."UrzedyWydajaceEPrzesylki.csv", "w");
        if (zip_entry_open($zip, $zip_entry, "r")) {
          $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
          fwrite($fp, $buf);
          zip_entry_close($zip_entry);
          fclose($fp);
        }
      }
      zip_close($zip);
    }
  }
 
  
  /**
   * Metoda konwertuje plik csv z ANSI (WINDOWS-1250) na UTF-8
   * 
   */  
	public function doConvertUrzedyWydajaceEPrzesylkiAnsiToUTF8() {

    $data = file_get_contents($this->CSV_PATH.'UrzedyWydajaceEPrzesylki.csv'); 
    $data = iconv("WINDOWS-1250", "UTF-8", $data); 
    file_put_contents($this->CSV_PATH.'UrzedyWydajaceEPrzesylki_utf8.csv', $data);  
  }
  
  
  /**
   * Metoda zwraca tablicę danych o urzędach wydających e-przesyłki - tablica utworzona jest na podstawie pobranego i przekonwertowanego pliku UrzedyWydajaceEPrzesylki_utf8.csv
   * 
   * @return array
   */  
	public function getUrzedyWydajaceEPrzesylkiFromCsv() {
    $aDataItems = array();
   
    $date = new \DateTime();
    $timestamp = $date->format('Y-m-d H:i:s');

    if (!file_exists($this->CSV_PATH.'UrzedyWydajaceEPrzesylki_utf8.csv')) {
      return false;
    }    
    
    $fileCsv = file($this->CSV_PATH.'UrzedyWydajaceEPrzesylki_utf8.csv');

    foreach ($fileCsv as $key => $row) {
      
      if ($key != 0) {
        $rowDetails = explode(';', $row);
        
        $id = $rowDetails[0];
        $x = $rowDetails[1];
        $y = $rowDetails[2];
        $wojewodztwo = $rowDetails[3];        
        $powiat = $rowDetails[4];        
        $gmina = $rowDetails[5];
        $nazwaPlacowki = $rowDetails[6];
        $kodPocztowy = $rowDetails[7];          
        $miejscowosc = $rowDetails[8];
        $ulica = $rowDetails[9];     
        $telefon = $rowDetails[10]; 
        $godzinyPracy = $rowDetails[11];
        $stanPlacowki = $rowDetails[12];

        $aDetails = array(
            'id' => $id,
            'x' => $x,
            'y' => $y,
            'province' => $wojewodztwo,
            'district' => $powiat,
            'community' => $gmina,
            'name' => $nazwaPlacowki,
            'postcode' => $kodPocztowy,
            'city' => $miejscowosc,
            'street' => $ulica,
            'hours' => str_replace('#', '<br />', $godzinyPracy),
            'status' => $stanPlacowki
        );
        
        $aDataItems[] = array(
            'id' => $id,
            '`desc`' => $kodPocztowy.' '.$miejscowosc.' '.$ulica,
            'city' => $miejscowosc,
            'street' => $ulica,
             'postcode' => str_replace('-', '', $kodPocztowy),
            'created' => $timestamp,
            'details' => base64_encode(serialize($aDetails)),
        );
      }
    }
  
    return $aDataItems;
  }
  
    
   /**
   * Metoda zwracająca tablicę z informacjami o konkretnym urzędzie
   * 
   * @param int - $iId - Id urzędu
   * @return array
   */  
	public function getUrzadWydajacyEPrzesylki($iId) {
    
    $sSql = "SELECT * 
            FROM shipping_points_poczta 
            WHERE id = '".$iId."'";
    
    $response = $this->pDbMgr->GetRow('profit24', $sSql);
    $response['details'] = unserialize(base64_decode($response['details']));
    
    return $response;
  }

  
   /**
   * Metoda zwracająca tablicę z informacjami o urzędach
   * 
   * @return array
   */  
	public function getUrzedyWydajaceEPrzesylkiList() {
    
    $sSql = "SELECT `desc` AS label, `id` AS value
             FROM shipping_points_poczta 
             ORDER BY postcode ASC";
    
    $response = $this->pDbMgr->GetAll('profit24', $sSql);
    
    return $response;
  }
  
  
  /**
   * Metoda sprawdza obecność punktu odbioru w bazie
   *
   * @param type $sPointOfReceipt
   * @return bool
   */
  public function checkDestinationPointAvailable($sPointOfReceipt) {
  
    $sSql = "SELECT details
             FROM shipping_points_poczta 
             WHERE id = '".$sPointOfReceipt."'";
    
    $aPointDetails = unserialize(base64_decode($this->pDbMgr->GetOne('profit24', $sSql)));

    return $aPointDetails;
  }  
}