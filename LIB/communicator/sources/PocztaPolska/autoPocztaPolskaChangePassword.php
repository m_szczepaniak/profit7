<?php

namespace communicator\sources\PocztaPolska;
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 'On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/PocztaPolska/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '1G');
global $pDbMgr;

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

$bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
$oShipment = new \orders\Shipment('poczta_polska', $pDbMgr, $bTestMode);

$oCurrentDate = new \DateTime('now');
$sPasswordExpiredDate = $oShipment->getPasswordExpiredDate('Shipment');
$oPasswordExpiredDate = new \DateTime($sPasswordExpiredDate);

$iPasswordValidity = $oCurrentDate->diff($oPasswordExpiredDate)->days;

if ($iPasswordValidity <= 60) {
  echo 'Uruchomiono automatyczną zmianę hasła do WebApi Poczty Polskiej<br />';

  $sPassword = $oShipment->getRandomPassword('Shipment');
  
  $bErrConfig = true;
  $bErrApi = true;
  $sErrApiMsg = '';

  $aChangePasswordWebApiRaport = $oShipment->changePassword('Shipment', $sPassword);
  
  if ($aChangePasswordWebApiRaport['status'] == true ) {
    $bErrApi = false;
    
    if ($oShipment->updatePasswordConfig('Shipment', $sPassword)) {
      $bErrConfig = false;
    }      
  } else {
    $sErrApiMsg = $aChangePasswordWebApiRaport['message'];
  }
  
  $aPasswordChangeErrorRaport = array('config' => $bErrConfig, 'api' => $bErrApi, 'message' => $sErrApiMsg);
  $oShipment->sendPasswordNotice('Shipment', $aPasswordChangeErrorRaport, $sPassword);
} else {
  echo 'Hasło do WebApi Poczty Polskiej jest ważne jeszcze przez dni: '.$iPasswordValidity;
}
