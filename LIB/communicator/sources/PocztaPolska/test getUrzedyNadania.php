<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<?php
include "../PocztaPolska/ElektronicznyNadawca_TEST.php";
class client
{
	protected $guid = null;
	public $urzedyNadania = null;
	protected $karty = null;
	protected $przesylki = array();
	protected $przesylkiDodane = array();
	protected $login = 'it@profit24.pl';
	protected $location = 'https://en-testwebapi.poczta-polska.pl/websrv/labs.php';
	protected $password = 'test#pocztapolska$21A';
	protected $en = null;
	// -------------------------------------------------------------------------
	public function __construct()
	{
		$this->en = new ElektronicznyNadawca("https://en-testwebapi.poczta-polska.pl/websrv/labs.wsdl", array(
																'login' => $this->login,
																'password' => $this->password,
																'location' => $this->location ));
	}
	// -------------------------------------------------------------------------
	public function testGetKarty()
	{
		echo "========<b>testGetKarty</b>====================<BR>";
		$parameters = new getKarty();
		$response = $this->en->getKarty($parameters);
		var_dump($response);
		$response->karta = $this->convertToArray($response->karta);
		$this->karty = $response->karta;
	}
	// -------------------------------------------------------------------------
	public function testSetAktywnaKarta()
	{
		echo "========<b>setAktywnaKarta</b>====================<BR>";
		$parameters = new setAktywnaKarta();
		$parameters->idKarta = reset($this->karty)->idKarta;
		$response = $this->en->setAktywnaKarta($parameters);
		var_dump($parameters);
		$response = new setAktywnaKartaResponse();
		if(!is_null($response->error))
		{
			$this->printErrors($response->error);
			throw new Exception("B��d ustawiania karty.");
		}
	}
	// -------------------------------------------------------------------------
	public function testCreateEnvelopeBufor()
	{
		echo "========<b>testCreateEnvelopeBufor</b>====================<BR>";
		$parameters = new createEnvelopeBufor();
		$parameters->bufor = new buforType();
		$date = new DateTime(Date("Y-m-d"));
		$date->add(new DateInterval('P3D'));
		$parameters->bufor->dataNadania = $date->format('Y-m-d');
		$parameters->bufor->opis = "Moj nowy bufor z dat� " . $parameters->bufor->dataNadania;
		$response = $this->en->createEnvelopeBufor($parameters);
		var_dump($response);
		$response = new createEnvelopeBuforResponse();
		$response->createdBufor = new buforType();
		if(!is_null($response->error))
		{
			$this->printErrors($response->error);
			throw new Exception("B��d towrzenia bufora.");
		}
		return $response->createdBufor;
	}
	// -------------------------------------------------------------------------
	public function testPaczkaPocztowaType()
	{
		echo "========<b>testPaczkaPocztowaType</b>====================<BR>";
		$paczka = new paczkaPocztowaType();
		$paczka->adres = new adresType();
		$paczka->adres->nazwa = "Jan Kowalski";
		$paczka->adres->ulica = "Kowalska";
		$paczka->adres->numerDomu = "666";
		$paczka->adres->numerLokalu = "666";
		$paczka->adres->kodPocztowy = "66-666";
		$paczka->adres->miejscowosc = "Warszawa";
		$paczka->kategoria = kategoriaType::PRIORYTETOWA;
		$paczka->gabaryt = gabarytType::GABARYT_B;
		$paczka->wartosc = 20000;
		
		$paczka->guid = $this->getGuid();
		var_dump($paczka);
		$this->przesylki[] = $paczka;
	}
	// -------------------------------------------------------------------------
	public function testAddShipment(buforType $bufor)
	{
		echo "========<b>testAddShipment</b>====================<BR>";
		$parameters = new addShipment();
		$parameters->idBufor = $bufor->idBufor;
		$parameters->przesylki = $this->przesylki;
		$response = $this->en->addShipment($parameters);
		var_dump($response);
		$response->retval = $this->convertToArray($response->retval);
		foreach($response->retval as $przesylka)
		{
			if(is_null($przesylka->error))
				$this->przesylkiDodane[] = $przesylka;
			else
			{
				echo "<B>Nie uda�o si� doda� przesy�ki </B>" . $przesylka->guid . " z powodu b��d�w.<BR>";
				$this->printErrors($przesylka->error);
			}
		}
	}
	// -------------------------------------------------------------------------
	public function testSendEnvelope(buforType $bufor)
	{
		echo "========<b>testSendEnvelope</b>====================<BR>";
		$parameters = new sendEnvelope();
		$parameters->urzadNadania = reset($this->urzedyNadania)->urzadNadania;
		$parameters->idBufor = $bufor->idBufor;
		$response = $this->en->sendEnvelope($parameters);
		var_dump($response);
		if(!is_null($response->error))
		{
			$this->printErrors($response->error);
			throw new Exception("B��d wysy�ania zbioru");
		}
		else
		{
			echo "<B>Status wys�anego kontenera przesy�ek: </B>" . $response->envelopeStatus . "<BR>";
			echo "<B>Identyfikator kontenera przesy�ek: </B>" . $response->idEnvelope . "<BR>";
			return $response->idEnvelope;
		}
	}
	// -------------------------------------------------------------------------
	public function testGetUrzadNadania()
	{
		echo "========<b>testGetUrzadNadania</b>====================<BR>";
		$response = $this->en->getUrzedyNadania(new getUrzedyNadania());
		var_dump($response->urzedyNadania);
		$response->urzedyNadania = $this->convertToArray($response->urzedyNadania);
		if(count($response->urzedyNadania) == 0)
			throw new Exception("Brak urz�d�w nadania.");
		$this->urzedyNadania = $response->urzedyNadania;
	}
	// -------------------------------------------------------------------------
	function getGuid()
	{
		mt_srand((double)microtime() * 10000);
		$charid = strtoupper(md5(uniqid(rand(), true)));
		$retval = substr($charid, 0, 32);
		return $retval;
	}
	// -------------------------------------------------------------------------
	function convertToArray($data)
	{
		if(!is_array($data))
			$data = array(
						$data );
		return $data;
	}
	// -------------------------------------------------------------------------
	function printErrors($errors)
	{
		$errors = $this->convertToArray($errors);
		foreach($errors as $error)
		{
			echo "[" . $error->errorNumber . "] " . $error->errorDesc . " " . $error->guid . "<BR>";
		}
	}
}

$c = new client();
//$c->testGetKarty();
//$c->testSetAktywnaKarta();
//$bufor = $c->testCreateEnvelopeBufor();
//$c->testPaczkaPocztowaType();
//$c->testAddShipment($bufor);
$c->testGetUrzadNadania();
//$c->testSendEnvelope($bufor);
?>
	</body>
</html>