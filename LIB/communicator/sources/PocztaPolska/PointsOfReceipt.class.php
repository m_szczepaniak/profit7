<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace communicator\sources\PocztaPolska;

/**
 * Description of PointsOfReceipt
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class PointsOfReceipt extends Shipment implements \communicator\sources\iPointsOfReceipt {
  
  public $aTransportSettings;
  public $bTestMode;
  public $pDbMgr;
  private $CSV_PATH;
  
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    parent::__construct($aTransportSettings, $pDbMgr, $bTestMode);
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    $this->CSV_PATH = __DIR__.'/csv/';
  }
  
  
  public function checkDestinationPointAvailable($sPointOfReceipt) {

    $sSql = "SELECT `desc`
             FROM shipping_points_poczta 
             WHERE id = '".$sPointOfReceipt."'";

    return ($this->pDbMgr->GetOne('profit24', $sSql) == true ? true : false); 
  }

  
  public function getDestinationPointsList() {
    
  }

  
  public function getSinglePointDetails($sPointOfReceipt) {
    global $pDbMgr;
    
    $sSql = 'SELECT details FROM  shipping_points_poczta WHERE id = '.$sPointOfReceipt;
    return unserialize(base64_decode($pDbMgr->GetOne('profit24', $sSql)));
  }

  
  /**
   * 
   * @param array $aCols
   * @param string $sAddSQL
   * @return array
   */
  private function getPointsOfReceipt($aCols, $sAddSQL) {
    
    $sSql = "SELECT ".implode(',', $aCols)."
             FROM shipping_points_poczta 
             ".$sAddSQL;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  
  /**
   * 
   * @param array $aCols
   * @param string $sAddSQL
   * @return array
   */
  public function getPointsOfReceiptAutocomplete($aCols, $sAddSQL) {
    $aPointsOfReceipt = $this->getPointsOfReceipt($aCols, $sAddSQL);
    return $this->doFormatPointsOfReceiptAutocomplete($aPointsOfReceipt);
  }
  
  
  /**
   * 
   * @param array $aPontsOfRecipt
   * @return array
   */
  private function doFormatPointsOfReceiptAutocomplete($aPontsOfRecipt) {
    
    foreach ($aPontsOfRecipt as $iKey => $aPointOfReceipt) {
      $aPontsOfRecipt[$iKey]['data'] = json_encode(unserialize(base64_decode($aPointOfReceipt['data'])));
    }
    return $aPontsOfRecipt;
  }// end of doFormatPointsOfReceipt() method
  
  
   /**
   * 
   * 
   * @param int $sPointOfReceipt
   * @return array
   */  
	public function getPopupDetails($sPointOfReceipt) {

    $sFontColor = '#fff';
    $sBackgroundColor = '#e6392b';
    $aFormatedPointOfReciptDetails = $this->getSinglePointDetails($sPointOfReceipt); 
    
    $sDetails = "Wybrana placówka pocztowa: <b><br />"
            . $aFormatedPointOfReciptDetails['postcode'] . ' '. $aFormatedPointOfReciptDetails['city'] . ', ' . $aFormatedPointOfReciptDetails['street'] . '</b><br /><br />'.$aFormatedPointOfReciptDetails['hours'];

    if (!empty($aFormatedPointOfReciptDetails) && is_array($aFormatedPointOfReciptDetails)) {
      $sSrc = 'https://maps.google.pl/maps?f=q&source=s_q&hl=pl&geocode=&q='
              .$aFormatedPointOfReciptDetails['x'].','.$aFormatedPointOfReciptDetails['y']
              .'('.$aFormatedPointOfReciptDetails['postcode'].' '.$aFormatedPointOfReciptDetails['city'].' '.$aFormatedPointOfReciptDetails['street']
              .')&aq=&vpsrc=0&ie=UTF8&t=m&z=14&'
              .$aFormatedPointOfReciptDetails['x'].','.$aFormatedPointOfReciptDetails['y']
              .'&output=embed';
    }

    return array('fontColor' => $sFontColor, 'backgroundColor' => $sBackgroundColor, 'details' => $sDetails, 'source' => $sSrc);  
    }

  public function getDestinationPointsDropdown() {
    return $this->getPointsOfReceipt(array('`desc` AS label', '`id` AS value'), ' ORDER BY `desc` ASC');
  }

  
  
  
  
  /**
   * Metoda nieużywana - pobiera z WebApi i zwraca listę urzędów wydających e-przesyłki
   * Odeszliśmy od tego sposobu na rzecz parsowania pliku csv - mamy tam więcej danych
   * 
   * @return array
   */  
	public function getUrzedyWydajaceEPrzesylki() {
    $response = $this->en->getUrzedyWydajaceEPrzesylki(new getUrzedyWydajaceEPrzesylki());
    
    return $response->urzadWydaniaEPrzesylki;
	}
  
  
  /**
   * Metoda nieużywana - zwraca pobraną z WebApi listę urzędów wydających e-przesyłki w formie tablicy. 
   * Odeszliśmy od tego sposobu na rzecz parsowania pliku csv - mamy tam więcej danych
   * 
   * @return array
   */  
	public function getUrzedyWydajaceEPrzesylkiArray() {
    $aDataItems = array();
    
    $response = $this->en->getUrzedyWydajaceEPrzesylki(new getUrzedyWydajaceEPrzesylki());
    $response = $response->urzadWydaniaEPrzesylki;

    foreach ($response as $row) {
      
      $id = $row->id;
      $wojewodztwo = $row->wojewodztwo;
      $powiat = $row->powiat;
      $miejsce = $row->miejsce;
      $kodPocztowy = $row->kodPocztowy;
      $miejscowosc = $row->miejscowosc;        
      $ulica = $row->ulica;
      $numerDomu = $row->numerDomu;
      
      $aDataItems[] = array(
          'id' => $id,
          'province' => $wojewodztwo,
          'district' => $powiat,
          'place' => $miejsce,
          'postcode' => $kodPocztowy,
          'city' => $miejscowosc,
          'street' => $ulica,
          'street_number' => $numerDomu,
      );
    }
    
    return $aDataItems;
  }
  
  
   /**
   * Metoda nadpisuje w bazie aktualną listę urzędów wydających e-przesyłki - dane o urzędach pochodzą z pobranego pliku csv
   * 
   * @param array - $aDataItems - tablica danych o urzędach pocztowych
   * @return bool
   */  
	public function doInsertUrzedyWydajaceEPrzesylki($aDataItems) {
    
    if (!$aDataItems) {
      return false;
    }

    if ($this->bTestMode === true) {
      $time_start = microtime(true); // pomiar czasu  
    }

    $sSql = "DELETE FROM shipping_points_poczta WHERE id > 0";
    $this->pDbMgr->Query('profit24', $sSql);  
    
    foreach ($aDataItems as $aDataItem) {
      
      if ($this->pDbMgr->Insert('profit24', 'shipping_points_poczta', $aDataItem) === false) {
        return false;
      }
    }
    
    if ($this->bTestMode === true) {
      $time_end = microtime(true);
      $execution_time = ($time_end - $time_start)/60;
      echo '<b>Czas wykonania insertu:</b> '.$execution_time.' Mins'; // pomiar czasu  
    }
  }  
  
  
  /**
   * Metoda pobiera plik csv zip z serwera Poczty Polskiej - http://odbiorwpunkcie.poczta-polska.pl/pliki.php?t=csv
   * 
   * @return bool
   */  
	public function doDownloadUrzedyWydajaceEPrzesylkiCsvZip() {

    $remoteFile = 'http://odbiorwpunkcie.poczta-polska.pl/pliki.php?t=csv';
    $downloadedFile = $this->CSV_PATH.'UrzedyWydajaceEPrzesylki.zip';

    $sStr = file_get_contents($remoteFile);
    if ($sStr !== false && $sStr != '') {
      if (file_put_contents($downloadedFile, $sStr) === false) {
        return false;
      }
    } else {
      return false;
    }

    return true;
  }
  
  
   /**
   * Metoda rozpakowuje plik csv z archiwum zip
   * 
   */  
	public function doExtractUrzedyWydajaceEPrzesylkiCsvZip() {

    $zip = zip_open($this->CSV_PATH.'UrzedyWydajaceEPrzesylki.zip');
    
    if ($zip) {
      while ($zip_entry = zip_read($zip)) {
        $fp = fopen($this->CSV_PATH."UrzedyWydajaceEPrzesylki.csv", "w");
        if (zip_entry_open($zip, $zip_entry, "r")) {
          $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
          fwrite($fp, $buf);
          zip_entry_close($zip_entry);
          fclose($fp);
        }
      }
      zip_close($zip);
    }
  }
 
  
  /**
   * Metoda konwertuje plik csv z ANSI (WINDOWS-1250) na UTF-8
   * 
   */  
	public function doConvertUrzedyWydajaceEPrzesylkiAnsiToUTF8() {

    $data = file_get_contents($this->CSV_PATH.'UrzedyWydajaceEPrzesylki.csv'); 
    $data = iconv("WINDOWS-1250", "UTF-8", $data); 
    file_put_contents($this->CSV_PATH.'UrzedyWydajaceEPrzesylki_utf8.csv', $data);  
  }
  
  
  /**
   * Metoda zwraca tablicę danych o urzędach wydających e-przesyłki - tablica utworzona jest na podstawie pobranego i przekonwertowanego pliku UrzedyWydajaceEPrzesylki_utf8.csv
   * 
   * @return array
   */  
	public function getUrzedyWydajaceEPrzesylkiFromCsv() {
    $aDataItems = array();
   
    $date = new \DateTime();
    $timestamp = $date->format('Y-m-d H:i:s');

    if (!file_exists($this->CSV_PATH.'UrzedyWydajaceEPrzesylki_utf8.csv')) {
      return false;
    }    
    
    $fileCsv = file($this->CSV_PATH.'UrzedyWydajaceEPrzesylki_utf8.csv');

    foreach ($fileCsv as $key => $row) {
      
      if ($key != 0) {
        $rowDetails = explode(';', $row);
        
        $id = $rowDetails[0];
        $x = $rowDetails[1];
        $y = $rowDetails[2];
        $wojewodztwo = $rowDetails[3];        
        $powiat = $rowDetails[4];        
        $gmina = $rowDetails[5];
        $nazwaPlacowki = $rowDetails[6];
        $kodPocztowy = $rowDetails[7];          
        $miejscowosc = $rowDetails[8];
        $ulica = $rowDetails[9];     
        $telefon = $rowDetails[10]; 
        $godzinyPracy = $rowDetails[11];
        $stanPlacowki = $rowDetails[12];

        $aDetails = array(
            'id' => $id,
            'x' => $x,
            'y' => $y,
            'province' => $wojewodztwo,
            'district' => $powiat,
            'community' => $gmina,
            'name' => $nazwaPlacowki,
            'postcode' => $kodPocztowy,
            'city' => $miejscowosc,
            'street' => $ulica,
            'hours' => str_replace('#', '<br />', $godzinyPracy),
            'status' => $stanPlacowki
        );
        
        $aDataItems[] = array(
            'id' => $id,
            '`desc`' => $kodPocztowy.' '.$miejscowosc.' '.$ulica,
            'city' => $miejscowosc,
            'street' => $ulica,
             'postcode' => str_replace('-', '', $kodPocztowy),
            'created' => $timestamp,
            'details' => base64_encode(serialize($aDetails)),
        );
      }
    }
  
    return $aDataItems;
  }
  
    
   /**
   * Metoda zwracająca tablicę z informacjami o konkretnym urzędzie
   * 
   * @param int - $iId - Id urzędu
   * @return array
   */  
	public function getUrzadWydajacyEPrzesylki($iId) {
    
    $sSql = "SELECT * 
            FROM shipping_points_poczta 
            WHERE id = '".$iId."'";
    
    $response = $this->pDbMgr->GetRow('profit24', $sSql);
    $response['details'] = unserialize(base64_decode($response['details']));
    
    return $response;
  }

  
   /**
   * Metoda zwracająca tablicę z informacjami o urzędach
   * 
   * @return array
   */  
	public function getUrzedyWydajaceEPrzesylkiList() {
    
    $sSql = "SELECT `desc` AS label, `id` AS value
             FROM shipping_points_poczta 
             ORDER BY postcode ASC";
    
    $response = $this->pDbMgr->GetAll('profit24', $sSql);
    
    return $response;
  }
}
