<?php

namespace communicator\sources\PocztaPolska;
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 'On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/PocztaPolska/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

$oShipment = new \orders\Shipment('poczta_polska', $pDbMgr);
$oShipment->doDownloadUrzedyWydajaceEPrzesylkiCsvZip('PointsOfReceipt');
$oShipment->doExtractUrzedyWydajaceEPrzesylkiCsvZip('PointsOfReceipt');
$oShipment->doConvertUrzedyWydajaceEPrzesylkiAnsiToUTF8('PointsOfReceipt');
$aDataItems = $oShipment->getUrzedyWydajaceEPrzesylkiFromCsv('PointsOfReceipt');
$insert = $oShipment->doInsertUrzedyWydajaceEPrzesylki('PointsOfReceipt', $aDataItems);
