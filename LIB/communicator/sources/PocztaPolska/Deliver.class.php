<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\PocztaPolska;
class Deliver extends \orders\Shipment\CommonDeliver implements \communicator\sources\iDeliver {
  
  public $aTransportSettings;
  public $bTestMode;
  public $pDbMgr;
  public $iTransportId;
  /**
   *
   * @var Shipment
   */
  private $oPocztaPolska;
  
  /**
   *
   * @var ElektronicznyNadawca $oEN
   */
  public $en;
  
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    parent::__construct($aTransportSettings, $pDbMgr, $bTestMode);
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    $this->iTransportId = $this->aTransportSettings['transport_id'];
    $this->oPocztaPolska = new Shipment($aTransportSettings, $pDbMgr, $bTestMode);
  }
  
  /**
   * 
   * @param array $aTransportNumbers
   * @param array $aOrdersIds
   * @throws Exception
   * @return string
   */
  public function getShipmentList($aTransportNumbers, $aOrdersIds = array()) {
    
    $bufor = $this->createNewSendBuffer();
    $moveShipments = $this->moveShipments($bufor->idBufor, $aOrdersIds);
    $moveShipments = $this->oPocztaPolska->convertToArray($moveShipments);
    
//    foreach ($moveShipments as $moveShipment) {
//      if (!empty($moveShipment->notMovedGuid)) {
//        $this->moveBackShipments($bufor->idBufor, $aOrdersIds);
//        throw new \Exception("Wystąpił błąd podczas przesuwania przesyłek, błąd: ".print_r($moveShipments, true));
//      }
//    }
    
    $oFirmowaPoczta = new \getFirmowaPocztaBook();
    $oFirmowaPoczta->idEnvelope = $this->sendEnvelope($bufor);
    $getOutboxBookResponse = new \getFirmowaPocztaBookResponse();
    $getOutboxBookResponse->error = new \errorType();
    $getOutboxBookResponse = $this->oPocztaPolska->en->getFirmowaPocztaBook($oFirmowaPoczta);
    
    if (!empty($getOutboxBookResponse->error)) {
      throw new \Exception($getOutboxBookResponse->error->errorDesc);
    } else {
      if ($getOutboxBookResponse->pdfContent != '' && is_array($getOutboxBookResponse->pdfContent) === false) {
        $sDeliverFilename = $this->saveDeliverGetFileNamePathTRANSACTION($getOutboxBookResponse->pdfContent, $aOrdersIds);
      } else {
        throw new \Exception(_('Wystąpił błąd podczas pobierania delivera z '.$this->aTransportSettings['name']));
      }
      return $sDeliverFilename;
    }
  }


  /**
   * Nadajemy paczi
   *
   * @param buforType $bufor
   * @return type
   * @throws Exception
   */
  private function sendEnvelope(\buforType $bufor) {
    
		$parameters = new \sendEnvelope();
		$parameters->urzadNadania = $bufor->urzadNadania;
		$parameters->idBufor = $bufor->idBufor;
		$response = $this->oPocztaPolska->en->sendEnvelope($parameters);
		if(!is_null($response->error))
		{
			$sStr = $this->printErrors($response->error);
			throw new \Exception("Błąd wysyłania zbioru ".$sStr);
		}
		else
		{
			return $response->idEnvelope;
		}
  }
  
  
  /**
   * 
   * @return \buforType
   */
  private function createNewSendBuffer() {
    $date = new \DateTime(date("Y-m-d"));
    $sDate = $date->format('Y-m-d');
    $bufor = $this->oPocztaPolska->_createEnvelopeBufor($sDate, $this->aTransportSettings['point_of_giving']);
    return $bufor;
  }
  
  
  /**
   * 
   * @param int $iBufferToId id buffora nadani
   * @param array $aOrdersIds
   */
  private function moveShipments($iBufferToId, $aOrdersIds) {
    $oOrdersDeliverAttributes = new \orders\ordersDeliverAttributes();
    $aBuffersGUIDs = $oOrdersDeliverAttributes->getOrdersAssocDeliverAttribsByCol($aOrdersIds, $this->iTransportId, array('bufer_id', 'guid'));
    
    foreach ($aBuffersGUIDs as $iBufferFromId => $aGUIDs) {
      $moveShipmentsResponse[] = $this->moveMultipleShipments($iBufferToId, $iBufferFromId, $aGUIDs);
    }
    return $moveShipmentsResponse;
  }


  /**
   *
   * @param int $iBufferToId id buffora nadani
   * @param array $aOrdersIds
   */
  private function moveBackShipments($iBufferToId, $aOrdersIds) {
    $oOrdersDeliverAttributes = new \orders\ordersDeliverAttributes();
    $aBuffersGUIDs = $oOrdersDeliverAttributes->getOrdersAssocDeliverAttribsByCol($aOrdersIds, $this->iTransportId, array('bufer_id', 'guid'));
    
    foreach ($aBuffersGUIDs as $iBufferFromId => $aGUIDs) {
      $moveShipmentsResponse[] = $this->moveMultipleShipments($iBufferFromId, $iBufferToId, $aGUIDs);
    }
    return $moveShipmentsResponse;
  }


  /**
   * @param int $iBufferToId
   * @param int $iOrderId
   * @return \moveShipments
   */
  private function moveMultipleShipments($iBufferToId, $iBufferFromId, $aGUIDs) {
    
    $guids = array();
    foreach ($aGUIDs as $aGUID) {
      $guid = new \guidType();
      $guid = $aGUID['guid'];
      $guids[] = $guid;
    }
            
    $moveShipments = new \moveShipments();
    $moveShipments->guid = $guids;
    $moveShipments->idBuforFrom = $iBufferFromId;
    $moveShipments->idBuforTo = $iBufferToId;
    
    $moveShipmentsResponse = new \moveShipmentsResponse();
    $moveShipmentsResponse = $this->oPocztaPolska->en->moveShipments($moveShipments);
    if (!empty($moveShipmentsResponse->error)) {
      $oError = new \errorType();
      $oError = $moveShipmentsResponse->error;
//      throw new \Exception($oError->errorDesc);
    }
    return $moveShipmentsResponse;
  }// end of moveMultipleShipments() method
  
  
  /**
   * @param int $iBufferToId
   * @param int $iOrderId
   * @return \moveShipments
   */
  private function moveSingleShipment($iBufferToId, $iOrderId) {
    $oOrdersDeliverAttributes = new \orders\ordersDeliverAttributes();
    $aOrderDeliverAttribs = $oOrdersDeliverAttributes->getOrderDeliverAttribsByCol($iOrderId, $this->iTransportId, array('bufer_id', 'guid'));
    
    
    $moveShipments = new \moveShipments();
    $moveShipments->guid = $aOrderDeliverAttribs['guid'];
    $moveShipments->idBuforFrom = $aOrderDeliverAttribs['bufer_id'];
    $moveShipments->idBuforTo = $iBufferToId;
    
    $moveShipmentsResponse = new \moveShipmentsResponse();
    $moveShipmentsResponse = $this->oPocztaPolska->en->moveShipments($moveShipments);
    if (!empty($moveShipmentsResponse->error)) {
      $oError = new \errorType();
      $oError = $moveShipmentsResponse->error;
      throw new \Exception($oError->errorDesc);
    }
    return $moveShipmentsResponse;
  }
}
