<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace communicator\sources\PocztaPolska;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Shipment implements \communicator\sources\iShipment {

  public $aTransportSettings;
  public $bTestMode;
  public $pDbMgr;
  public $iTransportId;

  /**
   *
   * @var ElektronicznyNadawca $oEN
   */
  public $en;
  protected $guid;
  protected $sAutoBufferName = "auto-buffor %s";
  CONST KWOTA_UBEZPIECZENIA = 100000;
  
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    $this->iTransportId = $this->aTransportSettings['transport_id'];
    
    $this->init();
  }
  
  /**
   * 
   */
  protected function init() {
    
    $options = array();
    $options["login"] = $this->aTransportSettings['login'];
    $options["password"] = $this->aTransportSettings['password'];
    $options["trace"] = 1;
    $options['connection_timeout'] = 10;
    
    require_once('ElektronicznyNadawca.php');
    $this->en = new \ElektronicznyNadawca($this->aTransportSettings['api_url'], $options);
  }
  
  
  /**
   * 
   * @param array $aOrder
   * @param array $aDeliverAddress
   * @param array $aWebsiteSettings
   * @return string nr listu przewozowego
   * @throws \Exception
   */
  public function addShipment($aOrder, $aDeliverAddress, $aWebsiteSettings) {
    $oOrdersDeliverAttributes = new \orders\ordersDeliverAttributes();
    $iAmountPackages = 1;
    $this->_setAktywnaKarta('profit24.pl');
    $bufor = $this->_getCreateBufforCurDateBufor($this->aTransportSettings['point_of_giving']);

    if ($aOrder['transport_packages'] > 0) {
      $iAmountPackages = $aOrder['transport_packages'];
    }
    $aOrder['weight'] = round(($aOrder['weight']/$iAmountPackages)*1000);
    
    $paczka = $this->_getPaczka($aDeliverAddress, $aOrder['weight'], $aOrder['point_of_receipt'], $aOrder);
    for ($i = 1; $i <= $iAmountPackages; $i++) {
      $przesylki[] = $paczka;
    }
    $oAddShipment = new \addShipment();
    $oAddShipment->idBufor = $bufor->idBufor;
    
    $oAddShipment->przesylki = $przesylki;
    $response = $this->en->addShipment($oAddShipment);
    
    if(!is_null($response->retval->error)) {
      echo "<B>Nie udało się dodać przesyłki </B>" . $response->retval->guid . " z powodu błędów.<BR>";
      $sStr = $this->printErrors($response->retval->error);
      echo $sStr;
      throw new \Exception($sStr);
    }
    
    $oOrdersDeliverAttributes->add($this->iTransportId, $aOrder['id'], $this->guid, $bufor->idBufor);
    if (is_array($response->retval)) {
      return $response->retval[0]->numerNadania;
    } else {
      return $response->retval->numerNadania;
    }
  }


  /**
   * Metoda ustawia aktywna
   *
   * @param type $sName
   * @throws \Exception
   */
  protected function _setAktywnaKarta($sName) {
    $kartaSelected = new \kartaType();
    $parameters = new \getKarty();
    $response = new \getKartyResponse();
    $response = $this->en->getKarty($parameters);
    foreach ($response->karta as $karta) {
      if ($karta->opis == $sName) {
        $kartaSelected = $karta;
      }
    }
	if (empty($kartaSelected)) {
		$kartaSelected = $response->karta[0];
	}
    
    $response->karta = $this->convertToArray($response->karta);
    $this->karty = $response->karta;
    
		$parameters = new \setAktywnaKarta();
		$parameters->idKarta = $kartaSelected->idKarta;
		$response = new \setAktywnaKartaResponse();
		$response = $this->en->setAktywnaKarta($parameters);
    
		if(!is_null($response->error))
		{
			$sStr = $this->printErrors($response->error);
			throw new \Exception("Błąd ustawiania karty.".$sStr);
		}
  }
  
  /**
   * Pobieramy GUID
   * 
   * @return int
   */
  protected function _getGUID() {

   mt_srand((double)microtime() * 10000);
   $charid = strtoupper(md5(uniqid(rand(), true)));
   $retval = substr($charid, 0, 32);
   return $retval;
  }


  /**
   *
   * @param $sDate
   * @param type $iUrzadNadaniaId
   * @return \buforType
   * @throws \Exception
   */
  public function _createEnvelopeBufor($sDate, $iUrzadNadaniaId) {
    
    $bufor = new \createEnvelopeBufor();
    $bufor->bufor = new \buforType();
    $bufor->bufor->active = TRUE;
    $bufor->bufor->dataNadania = $sDate;
    $bufor->bufor->opis = sprintf($this->sAutoBufferName, $bufor->bufor->dataNadania);
    $bufor->bufor->urzadNadania = $iUrzadNadaniaId;
    $response = $this->en->createEnvelopeBufor($bufor);
    
    if(!is_null($response->error))
    {
     $sStr = $this->printErrors($response->error);
     throw new \Exception("Błąd tworzenia bufora. ".$sStr);
    }
    return $response->createdBufor;
  }
  
  /**
   * Pobieramy objekt adresata
   * 
   * @param array $aDeliverAddress
   * @return \adresType
   */
  protected function _getAdresat($aDeliverAddress) {
    
    $a = new \adresType();
    $a->telefon = $aDeliverAddress['phone'];
    $a->kodPocztowy = $aDeliverAddress['postal'];
    $a->kraj = 'POLSKA';
    $a->miejscowosc = $aDeliverAddress['city'];
    $a->nazwa = $aDeliverAddress['name'] .' '. $aDeliverAddress['surname'];
    $a->numerDomu = $aDeliverAddress['number'];
    $a->numerLokalu = $aDeliverAddress['number2'];
    $a->mobile = $aDeliverAddress['phone'];
    $a->nazwa2 = $aDeliverAddress['company'];
    $a->ulica = $aDeliverAddress['street'];
    $a->nip = $aDeliverAddress['nip'];
    $a->email = $aDeliverAddress['email'];
    return $a;
  }
  
  /**
   * Metoda pobiera dzisiejszy buffor
   * 
   * @param string $sDate
   * @return \buforType
   */
  protected function _getDateBufor($sDate) {
    $result = new \getEnvelopeBuforListResponse();
    $result->bufor = new \buforType();
    /** @var \getEnvelopeBuforListResponse $result */
    $result = $this->en->getEnvelopeBuforList(new \getEnvelopeBuforList());
    if ($result->error != '') {
        throw new \Exception($result->error);
    }

    if (!is_array($result->bufor)) {
      
      $result->bufor = array($result->bufor);
    }
    foreach ($result->bufor as $bufor) {
      if ($bufor->dataNadania == $sDate) {
        return $bufor;
      }
    }
    return NULL;
  }
  
  /**
   * 
   * @param int $iUrzadNadaniaId
   * @return \buforType
   */
  protected function _getCreateBufforCurDateBufor($iUrzadNadaniaId) {
    
    $date = new \DateTime(date("Y-m-d"));
    $sDate = $date->format('Y-m-d');

    $buffor = $this->_getDateBufor($sDate);
    if ($buffor === null)  {
      // utwórz dzisiejszy buffor
      $sDateHis = $date->format('Y-m-d H:i:s');
      return $this->_createEnvelopeBufor($sDateHis, $iUrzadNadaniaId);
    } else {
      return $buffor;
    }
  }
  
  /**
   * 
   * @param float $fWeight
   * @param string $sPointOfReceipt
   * @param array $aOrder
   * @return \przesylkaBiznesowaType
   */
  protected function _getPaczka($aDeliverAddress, $fWeight, $sPointOfReceipt, $aOrder) {
    
    $paczka = new \przesylkaBiznesowaType();
    $paczka->gabaryt = \gabarytBiznesowaType::S;
    $paczka->masa = $fWeight;
    $uT = new \ubezpieczenieType();
    $uT->kwota = self::KWOTA_UBEZPIECZENIA;
    $uT->rodzaj = \rodzajUbezpieczeniaType::STANDARD;
    $paczka->ubezpieczenie = $uT;
    $oUWEPT  = new \urzadWydaniaEPrzesylkiType();
    $oUWEPT->id = $sPointOfReceipt;
    $paczka->urzadWydaniaEPrzesylki = $oUWEPT;
    $this->guid = $this->_getGUID();
    $paczka->guid = $this->guid;
    $paczka->adres = $this->_getAdresat($aDeliverAddress);
    
    if ($this->checkIsPostalFee($aOrder)) {
      $paczka->pobranie = $this->getPobranieType($aOrder);
    }
    
    return $paczka;
  }
  
  /**
   * 
   * @param array $aOrder
   * @return \PobranieType
   */
  protected function getPobranieType($aOrder) {
    
    $oPobranie = new \pobranieType();
    $oPobranie->kwotaPobrania = round($this->getPobranie($aOrder) * 100); // podanie w groszach
    $oPobranie->sposobPobrania = \sposobPobraniaType::RACHUNEK_BANKOWY;
    $oPobranie->sprawdzenieZawartosciPrzesylkiPrzezOdbiorce = false;
    $oPobranie->tytulem = $aOrder['order_number'];
    $oPobranie->nrb = '34103000190109850330007969';
    return $oPobranie;
  }
  
  /**
   * 
   * @param array $aOrder
   * @return boolean
   */
  protected function checkIsPostalFee($aOrder) {
    
    if ( ($aOrder['payment_type'] == 'postal_fee' || $aOrder['second_payment_type'] == 'postal_fee') && 
              ($aOrder['to_pay'] - $aOrder['paid_amount']) > 0) {
      return true;
    } else {
      return false;
    }
  }// end of checkIsPostalFee() method

  /**
   * 
   * @return float
   */
  protected function getPobranie($aOrder) {
    
    return $aOrder['to_pay']-$aOrder['paid_amount'];
  }  

  public function doCancelShipment() {
    
  }

  /**
   * Drukujemy
   * 
   * @global \DatabaseManager $pDbMgr
   * @param string $sFilePath
   * @param string $sPrinter
   * @return boolean
   */
  public function doPrintAddressLabel($sFilePath, $sPrinter) {
    global $pDbMgr;
    
    $aValues = array(
        'filetype' => 'PDF',
        'filepath' => $sFilePath,
        'dest_printer' => $sPrinter,
        'created' => 'NOW()'
    );
    if ($pDbMgr->Insert('profit24', 'print_queue', $aValues) === false) {
      return false;
    }
    return true;
  }

  /**
   * 
   * @param string $sFilePath
   * @param int $iOrderId
   * @return string filename
   * @throws \Exception
   */
  public function getAddressLabel($sFilePath, $iOrderId) {
    $aGUIDs = array();
    
    $this->init();
    
    $oOrdersDeliverAttributes = new \orders\ordersDeliverAttributes();
    $aOrderDeliverAttrib = $oOrdersDeliverAttributes->getOrderDeliverAttribsByCol($iOrderId, $this->iTransportId, array('bufer_id', 'guid'));
    
    $iGUID = new \guidType();
    $iGUID = $aOrderDeliverAttrib['guid'];
    $aGUIDs[] = $iGUID;
    
    $parameters = new \getAddresLabelByGuid();
    $parameters->guid = $aGUIDs;
    $parameters->idBufor = $aOrderDeliverAttrib['bufer_id'];
    
    $retval = new \getAddresLabelByGuidResponse();
    $retval = $this->en->getAddresLabelByGuid($parameters);
    
		if(!is_null($retval->error))
		{
			$sStr = $this->printErrors($retval->error);
			throw new \Exception("Pobierania etykiety ".$sStr);
		} else {
      $sFilePathName = $sFilePath;
      $h = fopen($sFilePathName, "w");
      fwrite($h, $retval->content->pdfContent);
      fclose($h);
      return $sFilePathName;
    }
  }

  /**
   * 
   * @global array $aConfig
   * @param int $iId
   * @param string $sTransportNumber
   * @return array
   */
  public function getDeliveryStatus($iId, $sTransportNumber) {
    global $aConfig;
            
    include_once($aConfig['common']['base_path'].'lib/nusoap/nusoap.php');
    $sNamespace = null;
    $oClient = new \nusoap_client($this->aTransportSettings['tracking_url'], "wsdl");
    $oClient->soap_defencoding = "UTF-8";
    $oClient->decode_utf8 = false;
    $sHeader = "<wsse:Security xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>";
    $sHeader.=   "<wsse:UsernameToken xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>";
    $sHeader.=       "<wsse:Username>".$this->aTransportSettings['tracking_login']."</wsse:Username>";
    $sHeader.=       "<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>".$this->aTransportSettings['tracking_password']."</wsse:Password>";
    $sHeader.=   "</wsse:UsernameToken>";
    $sHeader.= "</wsse:Security>";
    $aResult = $oClient->call("sprawdzPrzesylke", array("numer" => $sTransportNumber), $sNamespace,'',$sHeader);
   
    $aStatusesCollection = array();
    if ($aResult['return']['status'] == '0') {

      foreach ($aResult['return']['danePrzesylki']['zdarzenia']['zdarzenie'] as $aOrderStatus) {
        
        $oDeliveryShipment = new \orders\Shipment\DeliveryShipment();
        $oDeliveryShipment->sStatusName = $aOrderStatus['nazwa'];  
        $oDeliveryShipment->sStatusDate = $aOrderStatus['czas'];
        $aStatusesCollection[] = $oDeliveryShipment; 
      }       
    }
    
    return $aStatusesCollection;
  }
  
  
  /**
   * Metoda pobiera ścieżkę do etykiety listu przewozowego
   * 
   * @param string $agConfig
   * @param string $sTransportNumber
   * @return string
   */
  public function getTransportListFilename($agConfig, $sTransportNumber) {
    
    $sFilename = 'NalepkaAdresowa_'.$sTransportNumber.'.pdf';
    return $agConfig['common']['client_base_path'].'/'.$agConfig['common']['cms_dir'].$agConfig['common']['poczta_polska_dir'].$sFilename;
  }// end of getTransportListFilename() method
  
  

	public function convertToArray($data)
	{
		if (!is_array($data)) {
			$data = array($data );
    }
		return $data;
	}
	// -------------------------------------------------------------------------
	public function printErrors($errors)
	{
		$errors = $this->convertToArray($errors);
		foreach($errors as $error)
		{
			$sStr .= "[" . $error->errorNumber . "] " . $error->errorDesc . " " . $error->guid . "<BR>";
		}
    return $sStr;
	}
  
  
  /**
   * Metoda pobiera datę ważności hasła dostępu do WebApi
   * 
   * @return DateTime
   */
  public function getPasswordExpiredDate() {
    
    $oGetPasswordExpiredDate = new \getPasswordExpiredDate();
    $oResponse = $this->en->getPasswordExpiredDate($oGetPasswordExpiredDate);
    
    return $oResponse->dataWygasniecia;    
  }// end of getPasswordExpiredDate() method
  
  

  
  /**
   * Metoda dokonuje zmiany hasła dostępu do WebApi Poczty Polskiej
   * 
   * @param string $sPassword
   * @return bool
   */
  public function changePassword($sPassword) {

    $this->init();
    
    $oChangePassword = new \changePassword();
    $oChangePassword->newPassword = $sPassword;
    
    $oResponse = $this->en->changePassword($oChangePassword);
    if (!empty($oResponse->error)) {
      return array('status' => false, 'message' => $oResponse->error->errorDesc) ;
    }
   
    return array('status' => true, 'message' => '') ;
  }// end of changePassword() method


  /**
   * Metoda dokonuje aktualizacji hasła dostępu do WebApi znajdującego się w danych konfiguracyjnych w bazie danych (kay_value) 
   * 
   * @param string $sPassword
   * @return bool
   */
  public function updatePasswordConfig($sPassword) {

    $oKeyValue = new \table\keyValue('orders_transport_means');

    $sSql = 'SELECT id FROM orders_transport_means WHERE symbol IN ("poczta_polska", "poczta-polska-doreczenie-pod-adres")';
    $aTransportIds = $this->pDbMgr->GetCol('profit24', $sSql);
    foreach ($aTransportIds as $transportId) {
      $oKeyValue->update($transportId, 'password', $sPassword);
    }
    return true;
  }// end of updatePasswordConfig() method
  
  
  /**
   * Metoda wysyła powiadomienie o zmianie hasła do WebApi na zdefiniowane adresy mailowe
   * 
   * @param array $aPasswordChangeErrorRaport
   */
  public function sendPasswordNotice($aPasswordChangeErrorRaport, $sPassword) {

    $sMsgPwd = "Nowe hasło do WebApi Poczty Polskiej: <b>".$sPassword."</b><br /><br />";

    $sMsg .= "Zmiana hasła w WebApi: ";
    if ($aPasswordChangeErrorRaport['api'] == true) {
      $sMsg .= "BŁĄD!<br />";
    } else {
      $sMsg = $sMsgPwd . $sMsg;
      $sMsg .= "OK<br />";
    }
    
    $sMsg .= "Zmiana hasła w bazie danych: ";
    if ($aPasswordChangeErrorRaport['config'] == true) {
      $sMsg .= "BŁĄD!<br />";
    } else {
      $sMsg .= "OK<br />";
    }    

    if ($aPasswordChangeErrorRaport['message'] != '') {
      $sMsg .= "Komunikat błędu z WebAPi: <b>";
      $sMsg .= $aPasswordChangeErrorRaport['message']."</b>";
    }      
    
    $aPasswordRecipients = $this->getPasswordRecipients();
    
    if ($aPasswordChangeErrorRaport['api'] == false && $aPasswordChangeErrorRaport['config'] == false) {
      $sMsgTopicStatus = '[Status: OK]';
    } else {
      $sMsgTopicStatus = '[Status: BŁĄD!]';      
    }
    
    $sMsgTopic = $sMsgTopicStatus.' Raport z automatycznej zmiany hasła do WebApi Poczty Polskiej';
    
    foreach ($aPasswordRecipients as $sPasswordRecipient) {
      \Common::sendMail('OmniaCMS Automat', '', $sPasswordRecipient, $sMsgTopic, $sMsg, true);
    }
  }// end of sendPasswordNotice() method
  
  
  /**
   * Metoda zwraca adresy e-mail, do których wysłana będzie informacja o zmianie hasła do WebApi
   * 
   * @return array
   */
  protected function getPasswordRecipients() {
    
    $oKeyValue = new \table\keyValue('orders_transport_means');
    $sPasswordRecipients = $oKeyValue->getByIdDestKey($this->iTransportId, 'password_recipients');
    $aPasswordRecipients = explode(';', $sPasswordRecipients);
    
    return $aPasswordRecipients;
  }// end of getPasswordRecipients() method
  
  
  /**
   * Metoda tworzy losowe hasło 
   * 
   * @return string
   */
  public function getRandomPassword() {
    
    $aChars = array();
    $aChars[] = array('count' => 7, 'chars' => 'abcdefghijklmnopqrstuvwxyz');
    $aChars[] = array('count' => 7, 'chars' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $aChars[] = array('count' => 5, 'chars' => '0123456789');
    $aChars[] = array('count' => 2, 'chars' => '!@#$+-*&?:');
    
    $aRandomPassword = array();
    foreach ($aChars as $aChar) {
        for ($i = 0; $i < $aChar['count']; $i++) {
            $aRandomPassword[] = $aChar['chars'][rand(0, strlen($aChar['chars']) - 1)];
        }
    }
    shuffle($aRandomPassword);
    
    return implode('', $aRandomPassword);
  }// end of getRandomPassword() method  
}
