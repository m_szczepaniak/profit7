<?php
/**
 * Klasa obsługuje fakture od Panda
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-03-06
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Panda;

use communicator\sources\productToDBF;
use Exception;
class FV_XML {

    /**
     * @var simplexml_load_file $oXML
     */
    private $oXML;

    /**
     * $sXMLFilePath
     *
     * @param string $sXMLFilePath
     * @throws Exception
     */
    function __construct($sXMLFilePath) {
        if (file_exists($sXMLFilePath)) {
            $sXML = str_replace('&', '&amp;', file_get_contents($sXMLFilePath));
            $this->oXML = \simplexml_load_string($sXML, "SimpleXMLElement");

        } else {
            throw new Exception(_('Brak pliku XML do zaimportowania'));
        }
    }

    /**
     * Metoda wybiera numer faktury z dokumentu XML
     *
     * @return string
     */
    public function getFVNumber() {
        /** @var \SimpleXMLElement $aAttribs */
        $aAttribs = $this->oXML->wiersz[0]->attributes();
        return (string)$aAttribs->DOKUMENT;
    }// end of getFVNumber() method


    /**
     * Metoda pobiera produkty w formacie zgodnym z eksportem DBF
     * @return \communicator\sources\productToDBF[]
     * @throws Exception
     */
    public function getProductsToStreamsoft() {


        foreach ($this->oXML->wiersz as $oPozycja) {
            $spNetto = ((string)$oPozycja['WARTOSC_POZ']) / intval((string)$oPozycja['ILOSC']);
            $ean = (string)$oPozycja['EAN'];


            $streamsoftIndeks = $this->findProductEAN($ean);
            if ($streamsoftIndeks != '') {
                $aItem = array(
                    'ean_13' => $streamsoftIndeks,
                    'price_netto' => \Common::formatPrice2($spNetto),
                    'discount' => 0,
                    'vat' => (string)$oPozycja['VAT'],
                    'quantity' => (string)$oPozycja['ILOSC']

                );
                $aItems[] = $aItem;
            } else {
                throw new \Exception("Nie odnaleziono produktu o EAN ".print_r($ean, true));
            }
        }
        return $aItems;
    }// end of getProductsToDBF() method


    /**
     * @param $ean
     * @return mixed
     */
    private function findProductEAN($ean) {
        global $pDbMgr;

        $sSqlSelect = 'SELECT streamsoft_indeks
									 FROM products
									 WHERE 
										isbn_plain = "%1$s" OR 
										isbn_10 = "%1$s" OR 
										isbn_13 = "%1$s" OR 
										ean_13 = "%1$s" OR 
                                        streamsoft_indeks = "%1$s"
                    ';

        $sSql = sprintf($sSqlSelect, $ean);
        return $pDbMgr->GetOne('profit24', $sSql);
    }
}
