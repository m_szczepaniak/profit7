<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-10-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Panda;

use CommonSources;
use Exception;
use orders\OrderMagazineData;
use SimpleXMLElement;
use table\keyValue;

require_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/communicator/sources/CommonSources.class.php');
/**
 * Description of Order
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Order extends CommonSources {

  private $pDbMgr;
  private $bTestMode;
  
  public $oModuleZamowienia;
  
  /**
   *
   * @var keyValue
   */
  private $oKeyValue;
  
  /**
   *
   * @var array
   */
  private $aAccessData;

  /**
   *
   * @var CommS_WCF_TransferService
   */
  private $ws;

  /**
   *
   * @var array
   */
  private $aSellerData;

  /**
   *
   * @var dataProvider
   */
  private $dataProvider;

  /**
   *
   * @var string
   */
  private $sFilePath;
  
  /**
   *
   * @var string
   */
  private $sNewOrderNumber;

  public function __construct($pDbMgr, $bTestMode) {
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    
    $this->oKeyValue = new keyValue('external_providers');
    $this->aAccessData = $this->oKeyValue->getAllByIdDest(Panda::SOURCE_ID);
    $this->dataProvider = new dataProvider($this->pDbMgr);
    $this->aSellerData = $this->dataProvider->getSellerData();
    $this->sFilePath = __DIR__ . '/files/ORDER_'.date("YmdHisu").'XML';
    
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/Module.class.php');
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
    $oNull = new \stdClass();
    $this->oModuleZamowienia = new \Module($oNull, $oNull, TRUE);
    
    parent::__construct();
  }
  
  
  /**
   * 
   * @param array $aBooksList
   * @return array
   */
  public function getBooksList($aBooksList, $vat = null, $notVat = null, $ommitLimit = false) {

    // pobieramy listę
    if (empty($aBooksList)) {
      // pobierzmy listę książek
      $aBooksList = $this->dataProvider->getOrdersToSend($vat, $notVat, $ommitLimit);
    }

    if (!empty($aBooksList)) {
      $aBooksList = $this->_addSourceIndeks($aBooksList);
    }
    return $aBooksList;
  }
  

  /**
   * Metoda dodaje ideks źródła
   * 
   * @param array $aBooksList
   * @return array
   */
  private function _addSourceIndeks($aBooksList) {

    foreach ($aBooksList as $iKey => $aBook) {
      $aProductAttr = $this->dataProvider->_getProductAttr($aBook['id'], array('pts.source_index', 'p.isbn_plain'));
      $aBooksList[$iKey]['source_item_id'] = ($aProductAttr['source_index'] != '' ? $aProductAttr['source_index'] : $aProductAttr['isbn_plain']);
    }
    return $aBooksList;
  }
  
  /**
   * 
   * @param array $aBooksList
   * @return boolean
   */
  public function sendOrder($aBooksList) {
    
    try {
      $o = $this->createExternalOperation($aBooksList);
	  var_dump($o);
      $r = $this->doSendOrder($o);
	  var_dump($r);
	  var_dump(base64_decode($r));
      $iOSHId = $this->ChangeStatusesAddHistory($aBooksList, $this->sFilePath, $this->sNewOrderNumber);
      $resultObject = $this->getResult($r);
      $this->proceedInterpretResult($resultObject, $iOSHId);
    } catch (Exception $ex) {
      $this->aErr[] = $ex;
      $this->sendInfoMail(Panda::SOURCE_SYMBOL.' - '.$this->sNewOrderNumber.' - ' . $ex->getMessage(), implode('<br /><br />', $this->aErr));
      echo Panda::SOURCE_SYMBOL.' - ' . $ex;
      throw new Exception($ex);
    }
  }

  /**
   * Wysyła maila z logiem importu
   * @param $sTopic - temat maila
   * @param $sContent - treść maila
   * @return void
   */
  public function sendInfoMail($sTopic, $sContent){
    global $aConfig;
    $emails = [
        'a.golba@profit24.pl',
        $aConfig['common']['import_send_to']
    ];

    if(!\Common::sendMail('', $aConfig['common']['import_sender_email'], $emails, $sTopic, $sContent))
      dump('error sending mail');
  } // end of sendInfoMail() function

  /**
   * 
   * @param array $aBooksList
   * @return ExternalOperationInvoke
   */
  private function createExternalOperation($aBooksList) {
    $oOrderMagazine = new OrderMagazineData($this->pDbMgr);
    $this->sNewOrderNumber = $oOrderMagazine->getNewListNumber(Panda::NEW_INTERNAL_STATUS);
    $wsdo = new DocumentOrder($aBooksList, $this->aSellerData, $this->aAccessData, $this->sNewOrderNumber);
    
    $OperationInfo = $wsdo->getXMLInfo();
    $sBase64Order = $this->getOrderBase64($wsdo);
    $OperationParams = $wsdo->getXMLParams($sBase64Order);
    
    file_put_contents($this->sFilePath, $sBase64Order, FILE_APPEND | LOCK_EX);
    return new ExternalOperationInvoke($OperationInfo, $OperationParams);
  }
  
  /**
   * 
   * @param array $aBooksList
   * @return string
   */
  private function getOrderBase64($wsdo) {
    
    $aDocumentOrder = (array)$wsdo->getDocumentOrder();
    $xml_file = ArrayToXML::simplexml($aDocumentOrder, 'Document-Order');
    
    return $xml_file;
  }
  
  /**
   * 
   * @param ExternalOperationInvoke $o
   * @return type
   */
  private function doSendOrder(ExternalOperationInvoke $o) {
    
    $result = $this->ws->ExternalOperationInvoke($o);
    return $result->getExternalOperationInvokeResult();
  }
  
  /**
   * 
   * @param string $r
   * @return SimpleXMLElement
   */
  private function getResult($r) {
    
    $daat = base64_decode($r);

    $object = simplexml_load_string($daat);
    $sXML = base64_decode($object->Result);
    file_put_contents($this->sFilePath.'_RESPONSE.xml', $sXML, FILE_APPEND | LOCK_EX);
    if ($sXML == '') {
      throw new Exception(_('Nie otrzymaliśmy odpowiedzi z WebService : '.print_r($daat, true)));
    }
    
    return simplexml_load_string($sXML);
  }
  
  /**
   * 
   * @param SimpleXMLElement $resultObject
   * @param int $iOSHId
   * @return boolean
   * @throws Exception
   */
  private function proceedInterpretResult(SimpleXMLElement $resultObject, $iOSHId) {
    $resultObject = (array)$resultObject;
    
    if ($resultObject['OrderResponse-Header']->ResponseType != '29') {
      throw new Exception(_('Błędny status po złożeniu zamówienia: '.$resultObject['OrderResponse-Header']->ResponseType));
    }
    
    $aLines = (array)$resultObject['OrderResponse-Lines']->Line;
    foreach ($aLines['Line-Item'] as $aOrderedItem) {
        if ($this->proceedOrderedItemsResponse($iOSHId, $aOrderedItem['SupplierItemCode'], $aOrderedItem['QuantityToBeDelivered'], 3, Panda::SOURCE_ID, Panda::NEW_INTERNAL_STATUS) === FALSE) {
          $sErr = 'Wystąpił błąd podczas zmiany statusu zamówienia'
                  .' send_history_id: '.$iOSHId
                  .' bookindeks: '.$aOrderedItem['SupplierItemCode'];
          $this->aErr[] = $sErr;
          throw new Exception($sErr);
        }
    }
    return true;
  }
  
  /**
   * Metoda zmienia statusy zamówionych pozycji oraz dodaje historie wyslanego zamówienia
   * @param array $aBooksList
   * @param string $sFilePath
   * @param int $iSourceOrderId
   * @return int
   */
  private function ChangeStatusesAddHistory($aBooksList, $sFilePath, $iSourceOrderId) {
    $oOrderMagazine = new OrderMagazineData($this->pDbMgr);
    $sOrderNumber = $this->sNewOrderNumber;
    $oOrderMagazine->setOrderedItemsByStatus(Panda::NEW_INTERNAL_STATUS, $this->_getOrdersItemsList($aBooksList));
    //$iOrderToProviderId = $this->oOrdersToProviders->InsertOrderToProvider($sListNumber, Panda::SOURCE_ID, FALSE, $aBooksList, 'auto-ateneum', $iSourceOrderId);
    $aOrderData = array(
        'items' => $aBooksList,
        'panda_order_id' => $sOrderNumber
    );
    $iSendHistoryId = $oOrderMagazine->addSendHistory($aOrderData, $sFilePath, Panda::NEW_INTERNAL_STATUS, Panda::SOURCE_ID, 'panda');
    $oOrderMagazine->addSetHistorySendAttr($iSendHistoryId, 'ident_nr', $iSourceOrderId);
    return $iSendHistoryId;
  }
  

  
  /**
   * Metoda pobiera listę z id orders_items na podstawie tablicy do eksportu
   * 
   * @param array $aParams
   * @return string
   */
  private function _getOrdersItemsList($aParams) {

    $sItems = '';
    if (!empty($aParams)) {
      foreach ($aParams as $aItem) {
        $sItems .= $aItem['items'] . ',';
      }
    }
    return substr($sItems, 0, -1);
  }// end of _getOrdersItemsList() method
}
