<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-22 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources;
interface  iDeliver {

  /**
   * Metoda pobiera książkę nadawczą (dokument wydania z listą wszystkich przesyłek)
   * 
   * @param array $aTransportNumbers
   * @param array $aOrdersIds
   * @return string PDF
   */   
  function getShipmentList($aTransportNumbers, $aOrdersIds = array());
  
  function getDeliverFilename();
  function getOrderDeliverFilePath($iOrderId);
}