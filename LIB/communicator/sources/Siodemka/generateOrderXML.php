<?php
namespace communicator\sources\Siodemka;

use Array2XML;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-11-23 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class generateOrderXML {

  private $bTestMode;

  /**
   *
   * @var string
   */
  private $sOrderNumber;
  
  /**
   *
   * @var string
   */
  private $sFileNameOrder;
  
  private $order;
  
  /**
   *
   * @var \DomDocument
   */
  private $xmlDOM;
  
  CONST FILE_PATH = 'files_XML/';

  /**
   * 
   * @param string $sOrderNumber
   */
  public function __construct($sOrderNumber, $bTestMode) {

    $this->sOrderNumber = $this->getOrderNumber($sOrderNumber);
    $this->sFileNameOrder = $this->setFileNameOrder($this->sOrderNumber);
    $this->bTestMode = $bTestMode;
  }
  
  /**
   * 
   * @param string $sOrderNumber
   * @return string
   */
  private function setFileNameOrder($sOrderNumber) {
    $sOrderNumber = str_replace('/', '-', $sOrderNumber);
    return 'order-PROFIT_WA-'.$sOrderNumber.'.xml';
  }
  
  /**
   * 
   * @return string
   */
  public function getSendOrderNumber() {
    return $this->sOrderNumber;
  }
  
  /**
   * 
   * @param string $sOrderNumber
   * @return string
   */
  private function getOrderNumber($sOrderNumber) {
    $res = preg_replace('/Z(\d+)\/20(\d{2})/', '$1$2', $sOrderNumber);

    $res = $this->generateOrderNumber($res);

    return $res;
  }

  private function generateOrderNumber($string)
  {
    $result = preg_replace("/[^0-9,.]/", "", $string);

    $counted = strlen($result);

    if ($counted == 8) {
      return $result;
    } elseif ($counted > 8) {

      return substr($result, 0, 8);
    } else {
      $iterations = 8 - $counted;

      for($i = 0; $i == $iterations; $i++) {
          $result .= rand(0, 9);
      }

      return $result;
    }
  }

  /**
   * 
   * @return string
   */
  public function getFileNameOrder() {
    return $this->sFileNameOrder;
  }
  
  public function setBooksArray($aBooksList) {
    $order['id'] = $this->sOrderNumber;
    $order['time'] = date('Y-m-d H:i');
    $order['contractor_id'] = 'PROFIT WA';
    foreach ($aBooksList as $iKey => $aItem) {
      $order['items']['item'][$iKey]['productCode'] = $aItem['source_item_id'];
      $order['items']['item'][$iKey]['quantity'] = $aItem['quantity'];
    }
    $order['comment'] = ($this->bTestMode === true? 'TEST' : '');
    $order['delivery'] = 'DPD (wysyłka paczek)';
    $this->order = $order;
  }
  
  /**
   * 
   * @return string XML
   */
  public function getXMLOrder() {
    if (empty($this->order)) {
      return false;
    }
    include_once('Array2XML.class.php');
    $this->xmlDOM = Array2XML::createXML('order', $this->order);
    return $this->xmlDOM;
  }
  
  public function save() {
    $sDirectory = __DIR__.'/'.self::FILE_PATH;
    if (!file_exists($sDirectory)) {
      mkdir($sDirectory);
    }
    $filename = $sDirectory.$this->getFileNameOrder();
    if ($this->xmlDOM->save($filename) === false) {
      return false;
    }

    return $filename;
  }
}

