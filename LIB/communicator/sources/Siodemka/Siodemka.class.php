<?php
/**
 * Klasa obsługi komunikacji ze źródłem Siodemka
 * 
 * @version 0.01
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-02-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\Siodemka;
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/communicator/sources/CommonSources.class.php');

use CommonSources;
use communicator\sources\AutoOrders;
use communicator\sources\productToDBF;
use Curl_HTTP_Client;
use DatabaseManager;
use Exception;
use Module;
use Net_SFTP;
use orders\magazine\providers\ordersToProviders;
use orders\OrderMagazineData;
use stdClass;
use table\keyValue;

class Siodemka extends CommonSources implements AutoOrders {
  
  CONST SOURCE_ID = 26;
  CONST SOURCE_SYMBOL = 'siodemka';
  CONST NEW_INTERNAL_STATUS = 1;
  
  private $sCurrPath;
  
  /**
   * @var Curl_HTTP_Client $oCurl
   */
  private $oCURL;
  
  private $sNow;
  private $bTestMode;
  
  public $aErr = array();
  public $aErrMsg;
  public $oModuleZamowienia;
  /**
   *
   * @var ordersToProviders $oOrdersToProviders
   */
  public $oOrdersToProviders;
  
  /**
   * @var DatabaseManager $pDbMgr
   */
  public $pDbMgr;
  
  /**
   *
   * @var Net_SFTP
   */
 private $sftp;

  
  function __construct($pDbMgr, $bTestMode = TRUE) {
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    $this->oKeyValue = new keyValue('external_providers');
    
    $this->sCurrPath = preg_replace('/(.*)\\' . DIRECTORY_SEPARATOR . '.*/', '$1', __FILE__);
    $this->sNow = date('Ymd_His');
    $oNull = new \stdClass();
    if (!class_exists("Module")) {
      include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/Module.class.php');
      include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
      $this->oModuleZamowienia = new \Module($oNull, $oNull, TRUE);
    }
  }
  
  /**
   * 
   */
  private function initFTP() {
    ini_set('include_path', ini_get('include_path').PATH_SEPARATOR.$_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/phpseclib');
    include('phpseclib/Net/SFTP.php');
    $sftp = new Net_SFTP('213.218.125.80');
    return $sftp;
  }

  /**
   * Metoda grupuje rekordy na fakturze po azymut bookindeksie i zwraca tablice
   *
   * @param productToDBF $aProductsDBFsArray
   * @return array
   */
  private function _getInvoiceGroupByArray($aProductsDBFsArray) {
    $aInvoiceArray = array();

    foreach ($aProductsDBFsArray as $aProductDBF) {
      $sSourceBookindeks = $aProductDBF->indexDostawcy;
      $iCurrentQuantity = intval($aProductDBF->ilosc);
      if (isset($aInvoiceArray[$sSourceBookindeks])) {
        $aInvoiceArray[$sSourceBookindeks] += $iCurrentQuantity;
      } else {
        $aInvoiceArray[$sSourceBookindeks] = $iCurrentQuantity;
      }
    }
    return $aInvoiceArray;
  }// end of _getInvoiceGroupByArray() method

  /**
   * Parsujemy odpowiedź odnośnie zamówionych pozycji
   */
  public function __postRR() {
    $this->pDbMgr->BeginTransaction('profit24');

    try {
      $bLoggedIn = $this->_doLoginFTP();
      if ($bLoggedIn) {
        try {
          $this->readFiles();
        } catch(Exception $ex) {
          // wywalmy koszyk, wywalił błąd
          $this->pDbMgr->RollbackTransaction('profit24');
          throw $ex;
        }
      }
      $this->pDbMgr->CommitTransaction('profit24');
    } catch (Exception $e) {
      $this->pDbMgr->RollbackTransaction('profit24');
      $this->aErr[] = $e;
      $this->sendInfoMail('Siodemka - ' . $e->getMessage(), implode('<br /><br />', $this->aErr));
      echo 'Siodemka - ' . $e;
      throw new Exception($e);
    }
  }

  /**
   *
   * @param string $sDir
   * @return string
   */
  private function readFiles() {

    $sYearMonth = date('Ym');
    $sDir = '/PlikiS7/potwierdzenia/';
    $aFiles = $this->sftp->nlist($sDir);
    if (!empty($aFiles)) {
      foreach ($aFiles as $sName) {
        if ($sName != '.' && $sName != '.' && stristr($sName, '.xml')) {
          if ($this->sftp->get($sDir.$sName, __DIR__ . '/files_XML/' . $sName) === false) {
            throw new Exception(_('Wystąpił błąd podczas pobierania pliku potwierdzenia'));
          }
          $this->parseConfirmFile(__DIR__ . '/files_XML/'.$sName);
          @$this->sftp->mkdir($sDir.'/'.$sYearMonth.'/');
          if ($this->sftp->rename($sDir.$sName, $sDir . '/'.$sYearMonth.'/' . $sName) === false) {
            throw new Exception(_('Wystąpił błąd podczas przenoszenia pliku potwierdzenia do podkatalogu'));
          }
          return true;
        }
      }
    }
    return true;
  }

  /**
   *
   * @param string $sFilenamePath
   * @return boolean
   */
  private function parseConfirmFile($sFilenamePath) {

    if (file_exists($sFilenamePath)) {
      $xml = simplexml_load_file($sFilenamePath);
      $iSendHistoryId = $this->getSourceSendHistoryId($xml->id);
      $aProductsArray = $this->_getRRGroupByArray($xml->items);
      $this->_parseRRItems($iSendHistoryId, $aProductsArray);
      return $aProductsArray;
    } else {
      return false;
    }
  }

  /**
   *
   * @param int $iSendHistoryId
   * @param array $aProductsArray
   * @throws Exception
   */
  private function _parseRRItems($iSendHistoryId, $aProductsArray) {
    foreach ($aProductsArray as $sSourceIndex => $iCurrentQuantity) {
      if ($this->proceedOrderedItemsResponse($iSendHistoryId, $sSourceIndex, $iCurrentQuantity, 3, self::SOURCE_ID, self::NEW_INTERNAL_STATUS) === false) {
        $sErr = 'Wystąpił błąd podczas zmiany statusu zamówienia'
                .' send_history_id: '.$iSendHistoryId
                .' bookindeks: '.$iCurrentQuantity;
        $this->aErr[] = $sErr;
        throw new Exception($sErr);
      }
    }
    return true;
  }

  /**
   *
   * @param object $oItems
   * @return array
   */
  private function _getRRGroupByArray($oItems) {
    $aItemsArray = array();

    foreach ($oItems->item as $oItem) {
      $sSourceBookindeks = (string)$oItem->productCode;
      $iCurrentQuantity = intval((int)$oItem->quantity);
      if (isset($aItemsArray[$sSourceBookindeks])) {
        $aItemsArray[$sSourceBookindeks] += $iCurrentQuantity;
      } else {
        $aItemsArray[$sSourceBookindeks] = $iCurrentQuantity;
      }
    }
    return $aItemsArray;
  }// end of _getInvoiceGroupByArray() method


  /**
   * Metoda wysyłania zamówienia do dostawcy
   *
   * @global array $aConfig
   * @param array $aParams
   * @return return false
   */
  public function __putOrder($aBooksList = NULL) {

    $ommitLimit = false;
    include_once($_SERVER['DOCUMENT_ROOT'].'omniaCMS/modules/m_zamowienia/Module.class.php');
    $oNull = new stdClass();
    $this->oModuleZamowienia = new Module($oNull, $oNull, TRUE);

    $this->pDbMgr->BeginTransaction('profit24');
    try {
      // pobieramy listę
      if (empty($aBooksList) || isset($aBooksList['magazine']) || isset($aBooksList['ommitLimit'])) {
          if (isset($aBooksList['ommitLimit']) && $aBooksList['ommitLimit'] == '1') {
              $ommitLimit = true;
              unset($aBooksList['ommitLimit']);
          }
        // pobierzmy listę książek
        $aBooksList = $this->getSiodemkaOrdersToSend($aBooksList['magazine'], $ommitLimit);
        var_dump($aBooksList);
      }

      if (!empty($aBooksList)) {
        $aBooksList = $this->_addSiodemkaSourceIndeks($aBooksList);
        $oOrderMagazine = new OrderMagazineData($this->pDbMgr);
        $sOrderNumber = $oOrderMagazine->getNewListNumber(self::NEW_INTERNAL_STATUS);
        $genOrderXML = new generateOrderXML($sOrderNumber, $this->bTestMode);
        $genOrderXML->setBooksArray($aBooksList);
        $genOrderXML->getXMLOrder();
        $sLocalFileName = $genOrderXML->save();

        $bLoggedIn = $this->_doLoginFTP();
        if ($bLoggedIn) {
          // dodajemy nowe zamówienie do listy zamówień
          try {
            if ($this->putOrderFTP($sLocalFileName, $genOrderXML->getFileNameOrder()) === false) {
              throw new Exception(_('Nie udało się przesłać pliku FTP'));
            }
            $oOrderMagazine->setOrderedItemsByStatus(self::NEW_INTERNAL_STATUS, $this->_getOrdersItemsList($aBooksList));
            $iSourceOrderId = $genOrderXML->getSendOrderNumber();
            $aOrderData = array (
                        'items' => $aBooksList,
                        'siodemka_order_id' => $sOrderNumber
            );
            $iSendHistoryId = $oOrderMagazine->addSendHistory($aOrderData, $sLocalFileName, self::NEW_INTERNAL_STATUS,  self::SOURCE_ID, 'siodemka');
            $oOrderMagazine->addSetHistorySendAttr($iSendHistoryId, 'ident_nr', $iSourceOrderId);
          } catch(Exception $ex) {
            // wywalmy koszyk, wywalił błąd
            $this->pDbMgr->RollbackTransaction('profit24');
            throw $ex;
          }
        } else {
          $this->pDbMgr->RollbackTransaction('profit24');
          throw new Exception("Logowania/tworzenia katalogu");
        }
      }
      $this->pDbMgr->CommitTransaction('profit24');
      return TRUE;
    } catch (Exception $e) {
      $this->pDbMgr->RollbackTransaction('profit24');
      $this->aErr[] = $e;
      $this->sendInfoMail('Siodemka - ' . $e->getMessage(), implode('<br /><br />', $this->aErr));
      echo 'Siodemka - ' . $e;
      throw new Exception($e);
    }
  }// end of __putOrder() method

  /**
   *
   * @param string $sFileName
   */
  private function putOrderFTP($sFileName, $sRemoteFilename) {

    if ($this->sftp->put($sRemoteFilename, $sFileName, NET_SFTP_LOCAL_FILE) === false) {
      return false;
    }
    return true;
  }
  
  /**
   * Metoda pobiera listę z id orders_items na podstawie tablicy do eksportu
   * 
   * @param array $aParams
   * @return string
   */
  private function _getOrdersItemsList($aParams) {
    
    $sItems = '';
    if (!empty($aParams)) {
      foreach ($aParams as $aItem) {
        $sItems .= $aItem['items'].',';
      }
    }
    return  substr($sItems, 0, -1);
  }// end of _getOrdersItemsList() method
  
  
  /**
   * Metoda dodaje ideks źródła Siodemka
   * 
   * @param array $aBooksList
   * @return array
   */
  private function _addSiodemkaSourceIndeks($aBooksList) {
    
    foreach ($aBooksList as $iKey => $aBook) {
			$aProductAttr = $this->_getProductAttr($aBook['id'], array('pts.source_index', 'p.isbn_plain'));
      $aBooksList[$iKey]['source_item_id'] = ($aProductAttr['source_index'] != '' ? $aProductAttr['source_index'] : $aProductAttr['isbn_plain']);
    }
    return $aBooksList;
  }// end of _addSiodemkaSourceIndeks() method
  
  
  /**
   * Metoda wykonuje pobranie zamówień do wysłania
   * 
   * @return array - lista produktów
   */
  public function getSiodemkaOrdersToSend($magazineLocation, $ommitLimit = false) {
    
    $oOrderMagazine = new OrderMagazineData($this->pDbMgr);
    $aSelectCols = array('CONCAT(O.id, \'-\', OI.product_id)',
        'OI.product_id',
        'GROUP_CONCAT(OI.id SEPARATOR \',\') as items',
        'GROUP_CONCAT(OI.quantity SEPARATOR \',\') as items_quantity',
        'OI.order_id',
        'SUM(OI.quantity) as quantity',
        'OI.shipment_date',
        '(OI.quantity * PS.siodemka_wholesale_price) AS siodemka_wholesale_price'
    );
    return $oOrderMagazine->getOrdersItemsSendToSource(
        self::SOURCE_ID, self::NEW_INTERNAL_STATUS, self::SOURCE_SYMBOL, $aSelectCols,
        $ommitLimit == false ? $this->_getSourceOrdersLimit(): 0.00,
        $ommitLimit == false ? 0 : 9999999,
        $magazineLocation);
  }// end of getSiodemkaOrdersToSend() method
  
  
  /**
   * Metoda pobiera limit zamówień dla Siodemkau
   * 
   * @global object $iProviderId
   * @return string
   */
  private function _getSourceOrdersLimit() {

    $sSql = 'SELECT super_siodemka_limit_price
             FROM orders_magazine_settings
             WHERE id = 1';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of getSiodemkaOrdersLimit() method
  
  
  /**
   * Metoda pobiera atrybuty produktu
   * 
   * @param int $iPId
   * @param array $aCols
   * TABELE: s - sources
   *         pts - products_to_sources
   *         p - products
   * @return array
   */
  private function _getProductAttr($iPId, $aCols) {
    
    $sSql = 'SELECT '.implode(',', $aCols).' 
      FROM sources AS s
      JOIN products_to_sources AS pts 
      ON s.id = pts.source_id
      JOIN products AS p 
      ON pts.product_id = p.id
      WHERE p.id = '.$iPId.' 
        AND s.provider_id = '.self::SOURCE_ID.'
      ORDER BY pts.main_index DESC '; 
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of _getProductAttr() method
  
  
  /**
   * Metoda loguje i pobiera id koszyka
   * 
   * @return int
   * @throws Exception
   */
  private function _doLoginFTP() {
   
    $sLogin = $this->oKeyValue->getByIdDestKey(self::SOURCE_ID,'login');
    $sPassword = $this->oKeyValue->getByIdDestKey(self::SOURCE_ID,'passw');
    $sftp = $this->initFTP();
    if ($sftp->login($sLogin, $sPassword) === false) {
        throw new Exception("Wystąpił błąd podczas próby zalogowania na konto użytkownika");
    }
    $sftp->chdir('PlikiS7');
    $this->sftp = $sftp;
    return true;
  }// end of _doLoginFTP() method
  
  
  /**
   * Metoda loguje się u dostawcy
   * @param object $oCURL
   * @param string $sLogin
   * @param string $sPassword
   * @return string
   */
  public function _doLogin($sLogin, $sPassword, $bLogout=false) {

    return 'Zalogowany jako MARCIN JANOWSKI !!!!';
  }// end of _doLogin() function 
  
  
  /**
   * Metoda obsługuje XML'a z fakturą z Azymutu
   * 
   * @param string $sResponseFilePath
   * @param int $sAzymutTransaction id traksakcji azymut
   * @throws Exception
   */
  public function __parseXMLgetInv($aFvData) {
    
    $iCount = 0;
    $sNrInvoice = $aFvData['add_number'];
    $sExIdentNumber = $aFvData['ex_ident_number'];
    
    if ($sNrInvoice != '' && $sExIdentNumber != '') {
      $iOrderProviderId = $this->oOrdersToProviders->getOrderProviderId($sExIdentNumber);
      // TO JUŻ NIE POWINNO BYĆ POTRZEBNE
      //$iSendHistoryId = $this->_getSendHistoryId($sAzymutTransaction);
      //$oOrderMagazine->addSetHistorySendAttr($iSendHistoryId, 'fv_nr', $sNrInvoice);
      if ($iOrderProviderId > 0) {
        $aInvoiceArray = $this->_getInvoiceGroupByArray($aFvData['items']);
        if (!empty($aInvoiceArray)) {
          foreach ($aInvoiceArray as $sSourceBookindeks => $iCurrentQuantity) {

            if ($this->proceedOrderedItemsResponse($iOrderProviderId, $sSourceBookindeks, $iCurrentQuantity, 6, 'auto-siodemka') === false) {
              $sErr = 'Wystąpił błąd podczas zmiany statusu zamówienia'
                  .' send_history_id: '.$iOrderProviderId
                  .' bookindeks: '.$sSourceBookindeks;
              $this->aErr[] = $sErr;
              throw new Exception($sErr);
            } else {
              $iCount++;
            }
          }
        }

        if ($iCount > 0) {
          // jeśli jakiś element nie wystąpił na FV z tej listy, oznacza to że nie znajduje się na fakturze
          $aOrdersItemsList = $this->oOrdersToProviders->getItemsNotConfirmed($iOrderProviderId);
          if (!empty($aOrdersItemsList) && is_array($aOrdersItemsList)) {
            foreach ($aOrdersItemsList as $aOItem) {
              // jeśli zrealizowano mniej, niż zamówiono, to zmiana statusu: '---'
              if ($this->oOrdersToProviders->changeOrderToProvidersItemsStatus($aOItem['id'], 'B') !== false) {
                // zmiana statusu: '---'
                if ($this->_setProductNewRecount($aOItem, $aOItem['order_id'], $aOItem['order_item_id'], $aOItem['payment_id']) === false) {
                  return false;
                }
              } else {
               return false;
              }
            }
          }
        }
      }
    }
    return TRUE;
  }// end of parseXMLgetInv() method
}// end of Class()
