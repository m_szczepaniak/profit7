<?php
namespace communicator\sources\PocztaPolskaPodAdres;

use communicator\sources\PocztaPolska\Shipment as PocztaShipment;

/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-09
 * Time: 09:16
 */
class Shipment extends PocztaShipment
{
    /**
     *
     * @param float $fWeight
     * @param string $sPointOfReceipt
     * @param array $aOrder
     * @return \przesylkaBiznesowaType
     */
    protected function _getPaczka($aDeliverAddress, $fWeight, $sPointOfReceipt, $aOrder) {

        $paczka = new \przesylkaBiznesowaType();
        $paczka->gabaryt = \gabarytBiznesowaType::S;
        $paczka->masa = $fWeight;
        $uT = new \ubezpieczenieType();
        $uT->kwota = self::KWOTA_UBEZPIECZENIA;
//        $uT->rodzaj = \rodzajUbezpieczeniaType::STANDARD;
        $paczka->ubezpieczenie = $uT;
        $oUWEPT  = new \urzadWydaniaEPrzesylkiType();
        //$oUWEPT->id = $sPointOfReceipt;
        $paczka->urzadWydaniaEPrzesylki = $oUWEPT;
        $this->guid = $this->_getGUID();
        $paczka->guid = $this->guid;
        $paczka->adres = $this->_getAdresat($aDeliverAddress);

        if ($this->checkIsPostalFee($aOrder)) {
            $paczka->pobranie = $this->getPobranieType($aOrder);
        }

        return $paczka;
    }

    /**
     * Pobieramy objekt adresata
     *
     * @param array $aDeliverAddress
     * @return \adresType
     */
    protected function _getAdresat($aDeliverAddress) {

        $a = new \adresType();
        $a->telefon = $aDeliverAddress['phone'];
        $a->kodPocztowy = $aDeliverAddress['postal'];
        $a->kraj = 'POLSKA';
        $a->miejscowosc = $aDeliverAddress['city'];
        $a->nazwa = $aDeliverAddress['name'] .' '. $aDeliverAddress['surname'];
        $a->numerDomu = $aDeliverAddress['number'];
        $a->numerLokalu = $aDeliverAddress['number2'];
//        $a->mobile = $aDeliverAddress['phone'];
        $a->nazwa2 = $aDeliverAddress['company'];
        $a->ulica = $aDeliverAddress['street'];
        $a->nip = $aDeliverAddress['nip'];
        $a->email = $aDeliverAddress['email'];
        return $a;
    }
}