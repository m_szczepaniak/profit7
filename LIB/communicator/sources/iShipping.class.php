<?php
/**
 * Interfejs Spedycja
 * 
 * @author Paweł Bromka
 * @created 2014-11-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources;

interface iShipping {
  
  
  function __construct($bTestMode, $pDbMgr);

  
  /**
   * Metoda pobiera listę punktów odbioru przesyłek
   * 
   * @param string $sCity - Miasto
   * @return array
   */  
  function getDestinationPointsList();
  
  
   /**
   * Metoda generuje unikalny numer przesyłki
   * 
   * @return string
   */  
  function doPackUid();
  
  
   /**
   * Metoda pobiera etykietę adresową do wydruku
   * 
   * @param string $sFilePath - ścieżka pliku
   * @param string $iOrderId - Id paczki
   * @return PDF
   */  
  function getAddressLabel($sFilePath, $iOrderId);
  

   /**
   * Metoda drukuje etykietę adresową
   * 
   * @param string $sFilePath - scieżka do pliku
   * @param string $sPrinter - drukarka
   * @return PDF
   */  
  function doPrintAddressLabel($sFilePath, $sPrinter);
  
  
   /**
   * Metoda tworzy nowe zlecenie na przesyłkę (nowy list przewozowy)
   * 
   * @param array $aOrder
   * @param array $aDeliverAddress
    *@param array $aWebsiteSettings
   */    
  function addShipment($aOrder, $aDeliverAddress, $aWebsiteSettings);
  

   /**
   * Metoda anuluje zlecenie przesyłki
   */    
  function doCancelShipment();
  
  
  /**
   * Metoda pobiera książkę nadawczą (dokument wydania z listą wszystkich przesyłek)
   */   
  function getShipmentList($aOrdersIds);

  
  /**
   * Metoda drukuje książkę nadawczą (dokument wydania z listą wszystkich przesyłek)
   */   
  function doPrintShipmentList();
  
  
   /**
   * Metoda pobiera status doręczenia przesyłki
   * 
   * @param string $sNrNadania - Nr nadawczy przesyłki
   * @return string
   */ 
  function getDeliveryStatus($iId, $sTransportNumber);
  
  /**
   * Pobiera ścieżkę do etykiety listu przewozowego
   * @param array $agConfig
   * @param string $sTransportNumber
   */
  function getTransportListFilename($agConfig, $sTransportNumber);
}