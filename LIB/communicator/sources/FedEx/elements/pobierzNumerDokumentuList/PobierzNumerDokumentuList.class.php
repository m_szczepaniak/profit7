<?php
/**
 * Created by PhpStorm.
 * User: Arek
 * Date: 11.07.2018
 * Time: 09:32
 */
namespace communicator\sources\FedEx\elements\pobierzNumerDokumentuList;

class PobierzNumerDokumentuList
{
    /**
     * The kodDostepu
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var string
     */
    public $kodDostepu;
    /**
     * The numerListu
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var string
     */
    public $numerListu;
    /**
     * Constructor method for pobierzNumerDokumentuList
     * @see parent::__construct()
     * @param string $_kodDostepu
     * @param string $_numerListu
     * @return FEDEXStructPobierzNumerDokumentuList
     */
    public function __construct($_kodDostepu = NULL,$_numerListu = NULL)
    {
        $this->kodDostepu = $_kodDostepu;
        $this->numerListu = $_numerListu;
    }
    /**
     * Get kodDostepu value
     * @return string|null
     */
    public function getKodDostepu()
    {
        return $this->kodDostepu;
    }
    /**
     * Set kodDostepu value
     * @param string $_kodDostepu the kodDostepu
     * @return string
     */
    public function setKodDostepu($_kodDostepu)
    {
        return ($this->kodDostepu = $_kodDostepu);
    }
    /**
     * Get numerListu value
     * @return string|null
     */
    public function getNumerListu()
    {
        return $this->numerListu;
    }
    /**
     * Set numerListu value
     * @param string $_numerListu the numerListu
     * @return string
     */
    public function setNumerListu($_numerListu)
    {
        return ($this->numerListu = $_numerListu);
    }

    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}