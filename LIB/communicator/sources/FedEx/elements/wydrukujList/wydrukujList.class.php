<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-12 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\wydrukujList;
class wydrukujList {

  public $kodDostepu;
  public $numerPrzesylki;
  
  
  function __construct($kodDostepu) {
    $this->kodDostepu = $kodDostepu;
  }
  
  public function setWydrukujList($numerPrzesylki) {
    $this->numerPrzesylki = $numerPrzesylki;
  }
}
