<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\dodajKlienta;
class dodajKlienta {

  public $kodDostepu;
  public $klient;
  
  function __construct($sName, $sEmail) {
    $oKlient = new klient();
    
    $oKlient->emailKontakt = $sEmail;
    $oKlient->nazwa = $sName;
    
    $oKlient->czyFirma = 1;
    $oKlient->czyNadawca = 1;
    $oKlient->kod = '00-199';
    $oKlient->miasto = 'WARSZAWA';
    $oKlient->nrDom = '2';
    $oKlient->nrLokal = 'U5';
    $oKlient->telKontakt = '602582903';
    $oKlient->nip = '5252245459';
    $oKlient->telefonKom = '602582903';
    $oKlient->ulica = 'POKORNA';
    $oKlient->kodKraju = 'PL';
    
    $this->klient = $oKlient;
  }
}
