<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\dodajKlienta;
class klient {

  public $numer;
  public $nrExt;
  public $nazwa;
  public $czyFirma;
  public $nip;
  public $imie;
  public $nazwisko;
  public $miasto;
  public $kod;
  public $kodKraju;
  public $ulica;
  public $nrDom;
  public $nrLokal;
  public $telKontakt;
  public $emailKontakt;
  public $czyNadawca;
  public $telefonKom;
  public $fax;
  
  function __construct() {}
}
