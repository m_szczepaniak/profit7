<?php
/**
 * Zapisz dokument wydania
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-24 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\zapiszDokumentWydania;
class zapiszDokumentWydania {

  public $kodDostepu;
  public $numeryPrzesylki;
  public $separator;
  public $kurierNumer;
  
  function __construct() {}
}
