<?php

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-12 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\wydrukujEtykiete;
class wydrukujEtykiete {

  public $kodDostepu;
  public $numerPrzesylki;
  public $format;
  
  function __construct($kodDostepu) {
    $this->kodDostepu = $kodDostepu;
  }
  
  
  public function setWydrukujEtykiete($numerPrzesylki, $format) {
    $this->numerPrzesylki = $numerPrzesylki;
    $this->format = $format;
  }
}

