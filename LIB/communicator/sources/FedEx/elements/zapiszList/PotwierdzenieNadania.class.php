<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\zapiszList;
class PotwierdzenieNadania {
  
  public $podpisNadawcy;
  public $numerKuriera;
  public $dataNadania;
        
  function __construct() {
    
  }

}
