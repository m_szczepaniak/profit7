<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\zapiszList;
class kontrahentNadawca {
  
  public $numer;
  public $imie;
  public $nazwisko;
  public $nazwa;
  public $telKontakt;
  public $emailKontakt;
  
  function __construct() {
    
  }

}
