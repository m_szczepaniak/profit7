<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\zapiszList;
class Przesylka {

  public $nrPrzesylki;
  public $nrExt;
  public $formaPlatnosci;
  public $rodzajPrzesylki;
  public $placi;
  public $nadawca;
  public $odbiorca;
  public $platnik;
  public $potwierdzenieNadania;
  public $uslugi;
  public $paczki;
  public $uwagi;
  public $mpk;
  
  function __construct() {
    
  }

}