<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\zapiszList;
class Uslugi {
  
  public $pobranie;
  public $ubezpieczenie;
  public $zwrotDokumentow;
  public $zwrotKopiListu;
  public $doreczeniaAdresPrywatny;
  public $doreczeniaSiecHandlowa;
  public $potwDostEmail;
  public $potwNadEmail;
  public $zastrzDorNaGodzine;
  public $zastrzDorDoGodziny;
  public $zastrzDorNaDzien;
  public $ud01;
  public $ud02;
  public $ud03;
  public $ud04;
  public $ud05;
  public $ud06;
  public $ud07;
  public $ud08;
  public $ud09;
  public $ud10;
  public $ud11;
  public $ud12;
  public $ud13;
  public $ud14;
  public $ud15;
  public $ud16;
  public $ud17;
  public $ud18;
  public $ud19;
  public $ud20;
        
  function __construct() {
    
  }

}

