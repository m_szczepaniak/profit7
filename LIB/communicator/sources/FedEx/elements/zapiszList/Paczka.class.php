<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\zapiszList;
class Paczka {

  public $nrpp;
  public $typ;
  public $waga;
  public $gab1;
  public $gab2;
  public $gab3;
  public $ksztalt;
  public $wagaGabaryt;
  public $nrExtPp;
  
  function __construct() {
    
  }

}
