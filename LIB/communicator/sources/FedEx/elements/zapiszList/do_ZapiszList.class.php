<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\zapiszList;
class do_ZapiszList {
  
  public $kodDostepu;
  public $przesylka;

  private $bTestMode;
  private $identyfikator;
  private $FORMA_PLATNOSCI;// = 'P'; // przelew przedpłata
  private $aSellerData;
  private $identyfikatorNadawca;
  
  CONST PLACI = '3'; // 1 - za przesyłkę płaci osoba trzecia
  CONST RODZAJ_PRZESYLKI = 'K'; // Przesyłka krajowa
  CONST OPIS_ZAWARTOSCI = '';
  
  /**
   * Klasa zapisuje nowy list przewozowy w FedEx
   * 
   * @param bool $bTestMode
   * @param int $identyfikator
   * @param string $kodDostepu
   * @param array $identyfikatorNadawca
   * @param array $aSellerData
   */
  function __construct($bTestMode, $identyfikator, $kodDostepu, $identyfikatorNadawca, $aSellerData) {
    $this->bTestMode = $bTestMode;
    $this->identyfikator = $identyfikator;
    $this->kodDostepu = $kodDostepu;
    $this->aSellerData = $aSellerData;
    $this->identyfikatorNadawca = $identyfikatorNadawca;
    
    if ($this->bTestMode == true) {
      $this->FORMA_PLATNOSCI = 'P';
    } else {
      $this->FORMA_PLATNOSCI = 'P';
    }
  }
  
  /**
   * Metoda ustawia zmienne w obrębie metody zapisu listy w FedEx
   * 
   * TODO przewidzieć sytuację wielu zamówień do jednego listu przewozowego
   * 
   * @param array $aOrder
   * @param array $aAdresDostawy
   * @param float $fWeight
   * @return \communicator\sources\FedEx\elements\do_ZapiszList
   */
  public function setDoZapiszList($aOrder, $aAdresDostawy, $fWeight) {
    
    $nadawca = $this->setNadawca($this->identyfikatorNadawca[$aOrder['website_id']]);
    $odbiorca = $this->setOdbiorca($aAdresDostawy, $aOrder['email']);
    $paczki = $this->setPaczki($aOrder['order_number'], $aOrder['transport_packages'], $fWeight);
    $platnik = $this->setPlatnik();
    $potwierdzenieNadania = $this->setPotwierdzenieNadania();
    
    $pobranie = $this->setPobranie($aOrder);
    $uslugi = $this->setUslugi($pobranie, $aOrder);
    $this->przesylka = $this->setPrzesylka($aOrder, $nadawca, $odbiorca, $paczki, $platnik, $potwierdzenieNadania, $uslugi);
    
    return $this;
  }// end of setDoZapiszList() method
  
  
  /**
   * Metoda zapisuje numer listu przewozowego
   * 
   * @todo Tutaj przewidzieć także wiele listów do jednego zamowienia, pewnie zostanie to opracowane poziom wyżej
   * 
   * @global \DatabaseManager $pDbMgr
   * @param string $sFedExTransportNumber
   * @param int $iOrderId
   * @param bool $bPrint
   * @return boolean
   */
  public function zapiszListDB($sFedExTransportNumber, $iOrderId, $bPrint) {
    global $pDbMgr, $aConfig;
    
    $aValues = array(
        'transport_number' => $sFedExTransportNumber,
    );
    if ($pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOrderId) === false) {
      return false;
    }
    
    $oFedex = new \communicator\sources\FedEx\FedEx();
    $sFilename = 'etykiete_'.$sFedExTransportNumber.'.pdf';
    $oReturn = $oFedex->doWydrukujEtykiete($sFedExTransportNumber);
    if (isset($oReturn->etykietaBajty) && !empty($oReturn->etykietaBajty)) {
      $sFilePath = $aConfig['common']['client_base_path'].'/'.$aConfig['common']['cms_dir'].$aConfig['common']['fedex_dir'].$sFilename;
      
      file_put_contents($sFilePath, $oReturn->etykietaBajty);
      if ($bPrint == true) {
        // drukujemy sobie
        if ($this->addToPrintQueue($sFilePath, $_COOKIE['printer_labels']) === false) {
          return false;
        }
      }
    } else {
      return false;
    }

    $sSql = "SELECT id FROM orders_fedex WHERE id = ".$iOrderId;
    $bExists = ($pDbMgr->GetOne('profit24', $sSql) > 0 ? TRUE : FALSE);
    if ($bExists === FALSE) {
      $aValues = array(
          'created' => 'NOW()',
          'id' => $iOrderId
      );
      return  ($pDbMgr->Insert('profit24', 'orders_fedex', $aValues) > 0 ? true : false);
    } else {
      $aValues = array(
          'created' => 'NOW()',
      );
      return  ($pDbMgr->Update('profit24', 'orders_fedex', $aValues, ' id = '.$iOrderId) > 0 ? true : false);
    }
  }// end of zapiszListDB() method
  
  
  /**
   * Dodajemy dokument do kolejki drukowania 
   * 
   * @global \DatabaseManager $pDbMgr
   * @param string $sFilePath
   * @param string $sPrinter
   * @return boolean
   */
  public function addToPrintQueue($sFilePath, $sPrinter) {
    global $pDbMgr;
    
    $aValues = array(
        'filetype' => 'PDF',
        'filepath' => $sFilePath,
        'dest_printer' => $sPrinter,
        'created' => 'NOW()'
    );
    if ($pDbMgr->Insert('profit24', 'print_queue', $aValues) === false) {
      return false;
    }
    return true;
  }// end of addToPrintQueue() method
  
  
  /**
   * Metoda ustawia ubezpieczenie
   *
   */
  private function setUbezpieczenie($aOrder) {
    // skoro jesteśmy tu to znaczy że jest ustawione pobranie
    $ubezpieczenie = new Ubezpieczenie();
    $ubezpieczenie->kwotaUbezpieczenia = $aOrder['to_pay'];
    $ubezpieczenie->opisZawartosci = (isset($aOrder['orders_numbers']) && $aOrder['orders_numbers'] != '' ? $aOrder['orders_numbers'] : $aOrder['order_number']).' '.self::OPIS_ZAWARTOSCI;
    return $ubezpieczenie;
  }// end of setUbezpieczenie() method
  
  
  /**
   * Ustawiane są informacje na temat pobrania
   * 
   * @param array $aOrder
   * @return \communicator\sources\FedEx\elements\Pobranie
   */
  private function setPobranie($aOrder) {
    
    $pobranie = new Pobranie();
    
      
    if ( ($aOrder['payment_type'] == 'postal_fee' || $aOrder['second_payment_type'] == 'postal_fee') && 
              ($aOrder['to_pay'] - $aOrder['paid_amount']) > 0) {
      $pobranie->formaPobrania = 'B';
      // pierwsza metoda płatności lub druga to płatność przy odbiorze i istnieje niedopłata w zamówieniu
      $pobranie->kwotaPobrania = \Common::formatPrice($aOrder['to_pay']-$aOrder['paid_amount']);
      // @TODO dynamicznie z konfiguracji metody transportu
      $pobranie->nrKonta = '34 1030 0019 0109 8503 3000 7969';//$this->aSellerData['bank_account'];
    } else {
      $pobranie->kwotaPobrania = 0;
    }

    return $pobranie;
  }// end of setPobranie() method
  
  
  /**
   * Metoda ustawia usługi
   * 
   * @param Pobranie $pobranie
   * @return Uslugi
   */
  private function setUslugi(Pobranie $pobranie, $aOrder) {
    
    $uslugi = new Uslugi();
    
    if (isset($pobranie) && $pobranie->kwotaPobrania > 0) {
      $uslugi->pobranie = $pobranie;
    }
    $uslugi->ubezpieczenie = $this->setUbezpieczenie($aOrder);
    
    $uslugi->ud01 = '';
    $uslugi->ud02 = '';
    $uslugi->ud03 = '';
    $uslugi->ud04 = '';
    return $uslugi;
  }// end of setUslugi() method
  
  
  /**
   * Metoda ustawia potiwerdzenie nadania
   * 
   * @return \communicator\sources\FedEx\elementsPotwierdzenieNadania
   */
  private function setPotwierdzenieNadania() {
    
    $potwierdzenieNadania = new PotwierdzenieNadania();
    $potwierdzenieNadania->dataNadania = date('Y-m-d H:i');
    $potwierdzenieNadania->numerKuriera = $this->aSellerData['fedex_courier_id'];
    $potwierdzenieNadania->podpisNadawcy = 'KSIEGARNIA INTERNETOWA';
    return $potwierdzenieNadania;
  }// end of setPotwierdzenieNadania() method
  
  
  /**
   * Metoda ustawia płatnika
   * 
   * @return \communicator\sources\FedEx\elementskontrahentPlatnik
   */
  private function setPlatnik() {
    $platnik = new kontrahentPlatnik();
    $platnik->numer = $this->identyfikator;
    return $platnik;
  }// end of setPlatnik() method
  
  
  /**
   * Metoda ustawia paczki
   * 
   * @param int $iOrderId
   * @param int $iFedExPackages
   * @param float $fWeight
   * @return \communicator\sources\FedEx\elementsPaczki
   */
  private function setPaczki($iOrderId, $iFedExPackages, $fWeight) {
    $paczki = new Paczki();

    for ($i = 1; $i <= $iFedExPackages; $i++) {
      $paczka = new Paczka();
      $paczka->typ = 'PC';
      $paczka->waga = \Common::formatPrice($fWeight / $iFedExPackages);
      $paczka->ksztalt = 0;
      $paczka->nrExtPp = $iOrderId.'_'.$i;
      $paczki->paczka[] = $paczka;
    }
    return $paczki;
  }// end of setPaczki() method
  
  
  /**
   * Metoda ustawia odbiorcę
   * 
   * @param array $aAdresDostawy
   * @return kontrahentOdbiorca
   */
  private function setOdbiorca($aAdresDostawy, $email) {
    
    $odbiorca = new kontrahentOdbiorca();
    if ($aAdresDostawy['is_company'] == '1') {
      $odbiorca->czyFirma = 1;
      $odbiorca->nazwa = ($aAdresDostawy['company'] != ''? $aAdresDostawy['company'].' ' : '' ).$aAdresDostawy['name'] . ' ' . $aAdresDostawy['surname'];
    } else {
      $odbiorca->czyFirma = 0;
      $odbiorca->nazwa = ($aAdresDostawy['company'] != ''? $aAdresDostawy['company'].' ' : '' ).$aAdresDostawy['name'] . ' ' . $aAdresDostawy['surname'];
    }
    $odbiorca->imie = $aAdresDostawy['name'];
    $odbiorca->kod = trim($aAdresDostawy['postal']);
    $odbiorca->kodKraju = 'PL';
    $odbiorca->miasto = trim($aAdresDostawy['city']);
    $odbiorca->nazwisko = $aAdresDostawy['surname'];
    $odbiorca->nrDom = $aAdresDostawy['number'];
    $odbiorca->nrLokal = $aAdresDostawy['number2'];
    $odbiorca->telKontakt = $aAdresDostawy['phone'];
    $odbiorca->ulica = $aAdresDostawy['street'];
    $odbiorca->emailKontakt = $email;
    return $odbiorca;
  }// end of setOdbiorca() method
  
  
  /**
   * Metoda ustawia nadawcę
   * 
   * @return kontrahentNadawca
   */
  private function setNadawca($identyfikatorNadawcy) {
    
    $nadawca = new kontrahentNadawca();
    $nadawca->numer = $identyfikatorNadawcy;
    return $nadawca;
  }
  
  
  /**
   * Metoda ustala zmienne przesyłki
   * 
   * @param array $aOrder
   * @param \communicator\sources\FedEx\elementskontrahentNadawca $nadawca
   * @param \communicator\sources\FedEx\elementskontrahentOdbiorca $odbiorca
   * @param \communicator\sources\FedEx\elementsPaczki $paczki
   * @param \communicator\sources\FedEx\elementskontrahentPlatnik $platnik
   * @param \communicator\sources\FedEx\elementsPotwierdzenieNadania $potwierdzenieNadania
   * @param \communicator\sources\FedEx\elementsUslugi $uslugi
   * @return \communicator\sources\FedEx\elementsPrzesylka
   */
  private function setPrzesylka(
          array $aOrder, 
          kontrahentNadawca $nadawca, 
          kontrahentOdbiorca $odbiorca, 
          Paczki $paczki, 
          kontrahentPlatnik $platnik, 
          PotwierdzenieNadania $potwierdzenieNadania, 
          Uslugi $uslugi) {
    
    $przesylka = new Przesylka($this->bTestMode);
    $przesylka->formaPlatnosci = $this->FORMA_PLATNOSCI;
    $przesylka->mpk = substr($aOrder['order_number'], 0, 2);
    $przesylka->nadawca = $nadawca;
    $przesylka->nrExt = $aOrder['order_number'];
    //$przesylka->nrPrzesylki - to jest uzupełniane w response
    $przesylka->odbiorca = $odbiorca;
    $przesylka->paczki = $paczki;
    $przesylka->placi = self::PLACI;
    $przesylka->platnik = $platnik;
    $przesylka->potwierdzenieNadania = $potwierdzenieNadania;
    $przesylka->rodzajPrzesylki = self::RODZAJ_PRZESYLKI;
    $przesylka->uslugi = $uslugi;
    $przesylka->uwagi = ($this->bTestMode ? 'TEST ' : '') . (isset($aOrder['orders_numbers']) && $aOrder['orders_numbers'] != '' ? $aOrder['orders_numbers'] : $aOrder['order_number']) . (isset($aOrder['transport_remarks']) != '' ? "\n".$aOrder['transport_remarks'] : '');
    return $przesylka;
  }
}
