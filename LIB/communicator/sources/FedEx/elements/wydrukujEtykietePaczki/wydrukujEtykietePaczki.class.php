<?php

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-12 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx\elements\wydrukujEtykietePaczki;
class wydrukujEtykietePaczki {

  public $kodDostepu;
  public $numerPaczki;
  public $format;
  
  function __construct($kodDostepu) {
    $this->kodDostepu = $kodDostepu;
  }
  
  
  public function setWydrukujEtykietePaczki($numerPaczki, $format) {
    $this->numerPaczki = $numerPaczki;
    $this->format = $format;
  }
}

