<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-09-11 
 * @copyrights Marcin Chudy - Profit24.pl
 */


namespace communicator\sources\FedEx;
use communicator\sources\FedEx\elements\pobierzNumerDokumentuList\PobierzNumerDokumentuList;
use communicator\sources\FedEx\elements\zapiszList as zapiszList;
use communicator\sources\FedEx\elements\wydrukujList as wydrukujList;
use communicator\sources\FedEx\elements\wydrukujEtykiete as wydrukujEtykiete;
use communicator\sources\FedEx\elements\wydrukujEtykietePaczki as wydrukujEtykietePaczki;
use communicator\sources\FedEx\elements\zapiszDokumentWydania as zapiszDokumentWydania;
use communicator\sources\FedEx\elements\pobierzStatusyPrzesylki as pobierzStatusyPrzesylki;
use communicator\sources\FedEx\elements\dodajKlienta as dodajKlienta;

class FedEx {

  private $kodDostepu;
  private $bTestMode = false;
  private $identyfikator;
  private $aSellerData = array();
  private $identyfikatorNadawca;
  
  function __construct() {
    global $aConfig;
    $this->aSellerData = $this->getSellerData(array(' * '));
    
    if ($this->bTestMode == true) {
//      $this->identyfikator = 502415121;
//      $this->kodDostepu = '223C4F888E4614102878344492D42432';
    } else {
//      $this->identyfikator = $aSellerData['fedex_id'];
//      $this->kodDostepu = $aSellerData['fedex_key'];
//      $this->identyfikator = 5242665;
//      $this->kodDostepu = 'A68A7E19B771B03BC484436301D62590';
    }
    $this->identyfikator = 5242665;
    $this->kodDostepu = 'A68A7E19B771B03BC484436301D62590';
    $this->identyfikatorNadawca[1] = 508777591;// profit24
    $this->identyfikatorNadawca[2] = 509310466;
    $this->identyfikatorNadawca[3] = 508777553;// np
    $this->identyfikatorNadawca[4] = 514160396;// mestro
    $this->identyfikatorNadawca[5] = 520174166;// smarkacz
    $this->identyfikatorNadawca[8] = 526862941; //  naszabiblioteka.pl
  }


  /**
   * Metoda pobiera status przesyłki
   *
   * @param string $sTransportNumber
   */
  public function pobierzStatusyPrzesylki($sTransportNumber) {

    $oPobierzStatusyPrzesylki = new pobierzStatusyPrzesylki\pobierzStatusyPrzesylki();
    $oPobierzStatusyPrzesylki->czyOstatni = '0';
    $oPobierzStatusyPrzesylki->kodDostepu = $this->kodDostepu;
    $oPobierzStatusyPrzesylki->numerPrzesylki = $sTransportNumber;
    $client = new \SoapClient("https://poland.fedex.com/fdsWs/IklServicePort?WSDL",
            array('connection_timeout' => '5', 'trace' => 0, 'login' => $this->identyfikator, 'password' => $this->kodDostepu));
    $TMP['pobierzStatusyPrzesylki'] = (array)$oPobierzStatusyPrzesylki;

    return $client->__soapCall('pobierzStatusyPrzesylki', $TMP);
  }// end of pobierzStatusyPrzesylki() method


  /**
   * Metoda zapisuje dokument WD kurierowi
   *
   * @param array $aTransportNumbers
   * @return object
   */
  public function doZapiszDokumentWydania($aTransportNumbers) {

    $oZapiszDW = new zapiszDokumentWydania\zapiszDokumentWydania();
    $oZapiszDW->kodDostepu = $this->kodDostepu;
    $oZapiszDW->kurierNumer = $this->aSellerData['fedex_courier_id'];
    $oZapiszDW->separator = ',';
    $oZapiszDW->numeryPrzesylki = implode($oZapiszDW->separator, $aTransportNumbers);

    $client = new \SoapClient("https://poland.fedex.com/fdsWs/IklServicePort?WSDL",
            array('connection_timeout' => '5', 'trace' => 0, 'login' => $this->identyfikator, 'password' => $this->kodDostepu));
    $TMP['zapiszDokumentWydania'] = (array)$oZapiszDW;
    return $client->__soapCall('zapiszDokumentWydania', $TMP);
  }// end of doZapiszDokumentWydania() method


    /**
     * @param $aTransportNumbers
     * @return mixed
     */
    public function doSprawdzStatusyPrzesylek($aTransportNumbers) {

        foreach ($aTransportNumbers as $transportNumber) {
            $pobierzNumerDokumentuList = new PobierzNumerDokumentuList($this->kodDostepu, $transportNumber);
            $client = new \SoapClient("https://poland.fedex.com/fdsWs/IklServicePort?WSDL",
                array('connection_timeout' => '5', 'trace' => 0, 'login' => $this->identyfikator, 'password' => $this->kodDostepu));
            $TMP['pobierzNumerDokumentuList'] = (array)$pobierzNumerDokumentuList;
            try {
                $result = $client->__soapCall('pobierzNumerDokumentuList', $TMP);
            } catch (\SoapFault $ex) {
                var_dump($ex);
                if ($ex->getCode() == '0') {
                    echo 'Przesyłka nie nadana jeszcze '.$transportNumber;
                }
            }
        }
    }

    /**
     * Metoda pobiera status przesyłki
     *
     * @param string $sTransportNumber
     * @return array
     */
    public function pobierzOstatniStatusPrzesylki($aTransportNumbers) {

        $errResult = [];
        foreach ($aTransportNumbers as $transportNumber) {
            $oPobierzStatusyPrzesylki = new pobierzStatusyPrzesylki\pobierzStatusyPrzesylki();
            $oPobierzStatusyPrzesylki->czyOstatni = '1';
            $oPobierzStatusyPrzesylki->kodDostepu = $this->kodDostepu;
            $oPobierzStatusyPrzesylki->numerPrzesylki = $transportNumber;
            $client = new \SoapClient("https://poland.fedex.com/fdsWs/IklServicePort?WSDL",
                array('connection_timeout' => '5', 'trace' => 0, 'login' => $this->identyfikator, 'password' => $this->kodDostepu));
            $TMP['pobierzStatusyPrzesylki'] = (array)$oPobierzStatusyPrzesylki;
            $result = $client->__soapCall('pobierzStatusyPrzesylki', $TMP);
            if (!empty($result->statusyPrzesylki) > 0) {
                $errResult[$transportNumber] = $result;
            }
        }
        return $errResult;
    }// end of pobierzStatusyPrzesylki() method


  /**
   * Metoda dodaje klienta
   *
   * @return object
   */
  public function dodajKlienta() {

    $oDodajKlienta = new dodajKlienta\dodajKlienta('Profit24.pl', 'kontakt@profit24.pl');
    $oDodajKlienta->kodDostepu = $this->kodDostepu;

    $client = new \SoapClient("https://poland.fedex.com/fdsWs/IklServicePort?WSDL",
            array('connection_timeout' => '5', 'trace' => 0, 'login' => $this->identyfikator, 'password' => $this->kodDostepu));
    $TMP['dodajKlienta'] = (array)$oDodajKlienta;
    return $client->__soapCall('dodajKlienta', $TMP);
  }


  /**
   * Metoda zapisuje listę i zwraca numer listu przewozowego
   *
   * @param array $aOrder
   * @param array $aAdresDostawy
   * @param float $fWeight
   * @return boolean
   * @throws \Exception
   */
  public function doUtworzList($aOrder, $aAdresDostawy, $fWeight) {

    $doZapiszList = new zapiszList\do_ZapiszList($this->bTestMode, $this->identyfikator, $this->kodDostepu, $this->identyfikatorNadawca, $this->aSellerData);
    $zapiszList = $doZapiszList->setDoZapiszList($aOrder, $aAdresDostawy, $fWeight);

    $client = new \SoapClient("https://poland.fedex.com/fdsWs/IklServicePort?WSDL",
            array('connection_timeout' => '5', 'trace' => 0, 'login' => $this->identyfikator, 'password' => $this->kodDostepu));

    try{

      $TMP['zapiszList'] = (array)$zapiszList;
      $oReturn = $client->__soapCall('zapiszList', $TMP);

    } catch (\Exception $ex)  {
      throw new \Exception($ex->getMessage().print_r($TMP, true));
    }
    if (isset($oReturn) && 
            $oReturn != FALSE && 
            is_object($oReturn) && 
            isset($oReturn->przesylkaZapisana) && 
            isset($oReturn->przesylkaZapisana->nrPrzesylki) &&
            $oReturn->przesylkaZapisana->nrPrzesylki != '') {
      return $oReturn->przesylkaZapisana->nrPrzesylki;
    } else {
      return false;
    }
  }// end of doUtworzList() method
  
  
  /**
   * Metoda zapisuje numer listu przewozowego
   * 
   * @global \DatabaseManager $pDbMgr
   * @param string $sFedExTransportNumber
   * @param int $iOrderId
   * @param bool $bPrint
   * @return boolean
   */
  public function doZapiszList($sFedExTransportNumber, $iOrderId, $bPrint) {
    
    $doZapiszList = new zapiszList\do_ZapiszList($this->bTestMode, $this->identyfikator, $this->identyfikatorNadawca, $this->kodDostepu, $this->aSellerData);
    return $doZapiszList->zapiszListDB($sFedExTransportNumber, $iOrderId, $bPrint);
  }// end of doZapiszList() method
  
  /**
   * Metoda dodaje dokument do kolejki drukowania
   * 
   * @param string $sFilePath
   * @param string $sPrinter
   * @return bool
   */
  public function addToPrintQueue($sFilePath, $sPrinter) {
    $doAddToPrint = new zapiszList\do_ZapiszList($this->bTestMode, $this->identyfikator, $this->identyfikatorNadawca, $this->kodDostepu, $this->aSellerData);
    
    return $doAddToPrint->addToPrintQueue($sFilePath, $sPrinter);
  }// end of addToPrintQueue() method
  
  
  /**
   * Metoda zwraca list przewozowy np. do listów
   * 
   * @param int $numerPrzesylki
   * @return binary
   */
  public function doWydrukujList($numerPrzesylki) {
    
    $client = new \SoapClient("https://poland.fedex.com/fdsWs/IklServicePort?WSDL", 
            array('connection_timeout' => '5', 'trace' => 0, 'login' => $this->identyfikator, 'password' => $this->kodDostepu));
    
    $wydrukujList = new wydrukujList\wydrukujList($this->kodDostepu);
    $wydrukujList->setWydrukujList($numerPrzesylki);
    $TMP['wydrukjList'] = (array)$wydrukujList;
    return $client->__soapCall('wydrukjList', $TMP);
  }// end of doWydrukujList() method
  
  
  /**
   * Metoda drukuje etykietę
   * 
   * @param int $numerPaczki
   * @return binary
   */
  public function doWydrukujEtykiete($numerPaczki) {
    
    $client = new \SoapClient("https://poland.fedex.com/fdsWs/IklServicePort?WSDL", 
            array('connection_timeout' => '5', 'trace' => 0, 'login' => $this->identyfikator, 'password' => $this->kodDostepu));
    
    $wydrukujEtykiete = new wydrukujEtykiete\wydrukujEtykiete($this->kodDostepu);
    $wydrukujEtykiete->setWydrukujEtykiete($numerPaczki, 'PDF');
    $TMP['wydrukujEtykiete'] = (array)$wydrukujEtykiete;
    return $client->__soapCall('wydrukujEtykiete', $TMP);
  }// end of doWydrukujEtykiete() method
  
  
  /**
   * Metoda zwraca konkretną etykietę konkretnej paczki
   * 
   * @param number $numerPaczki
   * @return binary
   */
  public function doWydrukujEtykietePaczki($numerPaczki) {
    
    $client = new \SoapClient("https://poland.fedex.com/fdsWs/IklServicePort?WSDL", 
            array('connection_timeout' => '5', 'trace' => 0, 'login' => $this->identyfikator, 'password' => $this->kodDostepu));
    
    $wydrukujEtykiete = new wydrukujEtykietePaczki\wydrukujEtykietePaczki($this->kodDostepu);
    $wydrukujEtykiete->setWydrukujEtykietePaczki($numerPaczki, 'PDF');
    $TMP['wydrukujEtykietePaczki'] = (array)$wydrukujEtykiete;
    return $client->__soapCall('wydrukujEtykietePaczki', $TMP);
  }// end of doWydrukujEtykietePaczki() method
  
  

  
  /**
   * Metoda pobiera dane sprzedawcy
   * 
   * @global \DatabaseManager $pDbMgr
   * @param array $aCols
   * @return array
   */
  private function getSellerData($aCols = array(' * ')) {
    global $pDbMgr;
    
    $sSql = 'SELECT '.implode(',', $aCols).'
             FROM orders_seller_data 
             LIMIT 1';
    return $pDbMgr->GetRow('profit24', $sSql);
  }// end of getSellerData() method
}
