<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx;
class Shipment extends FedEx implements \communicator\sources\iShipment  {

  private $bTestMode;
  private $pDbMgr;
  private $aTransportSettings;
  
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    parent::__construct();
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }

  /**
   * 
   * @param array $aOrder
   * @param array $aDeliverAddress
   * @param array $aWebsiteSettings
   * @return string 
   */
  public function addShipment($aOrder, $aDeliverAddress, $aWebsiteSettings) {
    try {
      $sTransportNumber = $this->doUtworzList($aOrder, $aDeliverAddress, $aOrder['weight']);

      if (!empty($sTransportNumber)) {
        $this->doZapiszList($sTransportNumber, $aOrder['id'], false);
        return $sTransportNumber;
      } else {
        throw new Exception(_('Brak nr listu przewozowego'));
      }
    } catch (\Exception $ex) {
      throw new \Exception($ex->getMessage());
    }
  }

  /**
   * @TODO zaimplementować, ze względu na usuwanie listu przewozowego 
   *       w przypadku zmiany wartości pobrania
   */
  public function doCancelShipment() {
    
  }

  /**
   * 
   * @global \DatabaseManager $pDbMgr
   * @param string $sFilePath
   * @param string $sPrinter
   * @return boolean
   */
  public function doPrintAddressLabel($sFilePath, $sPrinter) {
    global $pDbMgr;
    
    $aValues = array(
        'filetype' => 'PDF',
        'filepath' => $sFilePath,
        'dest_printer' => $sPrinter,
        'created' => 'NOW()'
    );
    if ($pDbMgr->Insert('profit24', 'print_queue', $aValues) === false) {
      return false;
    }
    return true;
  }

  /**
   * 
   * @param string $sFilePath
   * @param int $iOrderId
   * @return string
   */
  public function getAddressLabel($sFilePath, $iOrderId) {
    return $sFilePath; // ;-) [:->
  }

  
  public function getDeliveryStatus($iId, $sTransportNumber) {

    $oStatuses = $this->pobierzStatusyPrzesylki($sTransportNumber);

    $aStatusesCollection = array();
    if (!empty($oStatuses) && is_object($oStatuses) && isset($oStatuses->statusyPrzesylki)) {

      foreach ($oStatuses->statusyPrzesylki as $oOrderStatus) {
      
        if (isset($oOrderStatus->opis) && isset($oOrderStatus->dataS)) {
          $oDeliveryShipment = new \orders\Shipment\DeliveryShipment();
          $oDeliveryShipment->sStatusName = $oOrderStatus->opis;  
          $oDeliveryShipment->sStatusDate = $oOrderStatus->dataS;
          $aStatusesCollection[] = $oDeliveryShipment;
        }
      }        
    }
    
    return $aStatusesCollection;    
  }   


  public function getTransportListFilename($agConfig, $sTransportNumber) {
    $sFilename = 'etykiete_'.$sTransportNumber.'.pdf';
    return $agConfig['common']['client_base_path'].'/'.$agConfig['common']['cms_dir'].$agConfig['common']['fedex_dir'].$sFilename;
  }
}
