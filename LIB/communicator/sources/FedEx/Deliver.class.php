<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace communicator\sources\FedEx;
use communicator\sources\FedEx\elements\pobierzNumerDokumentuList\PobierzNumerDokumentuList;

class Deliver extends \orders\Shipment\CommonDeliver implements \communicator\sources\iDeliver  {

  public $aTransportSettings;
  public $bTestMode;
  public $pDbMgr;
  private $oFedEx;
  
  /**
   * 
   * @param array $aTransportSettings
   * @param \DatabaseManager $pDbMgr
   * @param bool $bTestMode
   */
  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    parent::__construct($aTransportSettings, $pDbMgr, $bTestMode);
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    $this->oFedEx = new FedEx();
  }

  /**
   * 
   * @param array $aTransportNumbers
   * @param array $aOrdersIds
   * @return bool
   */
  public function getShipmentList($aTransportNumbers, $aOrdersIds = array()) {
    $oResponse = $this->oFedEx->doZapiszDokumentWydania($aTransportNumbers);
    
    if (isset($oResponse->dokumentWydaniaPdf) && !empty($oResponse->dokumentWydaniaPdf)) {
      $sDeliverFilename = $this->saveDeliverGetFileNamePathTRANSACTION($oResponse->dokumentWydaniaPdf, $aOrdersIds);
    } else {
      throw new \Exception(_('Wystąpił błąd podczas pobierania delivera z '.$this->aTransportSettings['name']));
    }
    return $sDeliverFilename;
  }

    /**
     * @param $aTransportNumbers
     * @return mixed
     */
    public function doSprawdzStatusyPrzesylek($aTransportNumbers) {

        return $this->oFedEx->pobierzOstatniStatusPrzesylki($aTransportNumbers);
    }
}
