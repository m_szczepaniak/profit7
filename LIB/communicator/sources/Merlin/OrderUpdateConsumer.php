<?php
namespace communicator\sources\Merlin;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use communicator\sources\Merlin\EmailSender;

class OrderUpdateConsumer {

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    /**
     * @var AMQPChannel
     */
    private $channel;
    private $connection;
    private $queue = 'frontcom-frontend-order-update-profit24';

    private $emailSender;

    private $limit = 0;

    function __construct($pDbMgr, $argv)
    {
        if ( isset($argv[1]) === true ) {
            $this->queue = 'frontcom-frontend-order-update-' . $argv[1];
        }

        $this->pDbMgr = $pDbMgr;

        $this->emailSender = new EmailSender($this->pDbMgr);
    }

    /**
     * Rozpoczęcie konsumowania
     *
     * @throws \ErrorException
     */
    public function startConsuming()
    {
        $error = $this->openRabbitConnection();
        if($error !== false) {
            echo $error;
            return;
        }

        $this->channel->basic_qos(null, 10, null);

        echo " [*] Czekanie na wiadomości. aby wyjść naciśnij CTRL+C\n";

        $this->channel->basic_consume($this->queue, '', false, false, false, false, array($this, 'updateOrder'), null);

        while (count($this->channel->callbacks)) {
            $this->channel->wait(null, false, 10);
        }
    }

    /**
     * Consumer ustawiający status
     *
     * @param AMQPMessage $msg
     * @throws \Exception
     */
    public function updateOrder(AMQPMessage $msg)
    {
        $decodedMsg = json_decode($msg->body, true);
        var_dump($decodedMsg);

        $orderNumber = $decodedMsg['frontendOrderNumber'];
        $payload = $decodedMsg['payload'];

        if ( $payload !== null && empty($payload) === false && $orderNumber !== null && $orderNumber !== "" ) {
            $error = $this->checkKeys($payload);
            if ($error === true) {
                echo "Nie zmapowane wartości!\n";

                $msg->delivery_info['channel']->basic_reject($msg->delivery_info['delivery_tag'], true);
            } else {
                try {
                    $this->pDbMgr->BeginTransaction('profit24');

                    $mappedPayload = $this->mapKeys($payload);

                    $this->pDbMgr->Update('profit24', 'orders', $mappedPayload, 'order_number = "' . $orderNumber . '" LIMIT 1');
                    $this->pDbMgr->CommitTransaction('profit24');
                    $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
                } catch (\Exception $e) {
                    $this->pDbMgr->RollbackTransaction('profit24');
                    $msg->delivery_info['channel']->basic_reject($msg->delivery_info['delivery_tag'], true);
                }
            }
        }

        if ($this->limit > 1000) {
            $msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
        } else {
            $this->limit++;
        }
    }

    /**
     * Utworzenie połączenia z rabbitem
     *
     * @return bool|string
     */
    private function openRabbitConnection()
    {
        global $aConfig;

        try {
            $connection = new AMQPStreamConnection($aConfig['rabbitmq']['host'], $aConfig['rabbitmq']['port'], $aConfig['rabbitmq']['user'], $aConfig['rabbitmq']['pass'], $aConfig['rabbitmq']['vhost']);
            $channel = $connection->channel();
            $this->connection = $connection;
            $this->channel = $channel;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return false;
    }

    /**
     * Sprawdzenie czy wszystkie dane są zmapowane
     *
     * @param $payload
     * @return bool
     */
    public function checkKeys($payload)
    {
        $allowedValues = $this->getValuesMap();
        $keys = array_keys($payload);

        foreach ($keys as $key) {
            if (in_array($key, array_keys($allowedValues), true) === false) {
                return true;
            }
        }
        return false;
    }

    /**
     * Tablica mapująca
     * "nazwa w merlinie" => "nazwa pola w proficie"
     *
     * @return array
     */
    private function getValuesMap() {
        return [
              "tracking_code" => "transport_number"
        ];
    }

    /**
     * Zmienia indeksy z nazw merlina na nazwy
     *
     * @param $payload
     * @return array
     */
    private function mapKeys($payload) {
        $keysMap = $this->getValuesMap();
        $mappedPayload = [];

        foreach ($payload as $key => $value) {
            $mappedPayload[$keysMap[$key]] = $value;
        }

        return $mappedPayload;
    }
}