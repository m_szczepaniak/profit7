<?php
namespace communicator\sources\Merlin;

use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use communicator\sources\Merlin\EmailSender;

class OrderStatusConsumer {

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    /**
     * @var AMQPChannel
     */
    private $channel;
    private $connection;
    private $queue = 'frontend-order-status-profit24';

    private $emailSender;

    private $limit = 0;

    function __construct($pDbMgr, $argv)
    {
        if ( isset($argv[1]) === true ) {
            $this->queue = 'frontend-order-status-' . $argv[1];
        }

        $this->pDbMgr = $pDbMgr;

        $this->emailSender = new EmailSender($this->pDbMgr);
    }

    /**
     * Rozpoczęcie konsumowania
     *
     * @throws \ErrorException
     */
    public function startConsuming()
    {
        $error = $this->openRabbitConnection();
        if($error !== false) {
            echo $error;
            return;
        }

        $this->channel->basic_qos(null, 10, null);

        echo " [*] Czekanie na wiadomości. aby wyjść naciśnij CTRL+C\n";

        $this->channel->basic_consume($this->queue, '', false, false, false, false, array($this, 'setStatus'), null);

        while (count($this->channel->callbacks)) {
            $this->channel->wait(null, false, 10);
        }
    }

    /**
     * Consumer ustawiający status
     *
     * @param AMQPMessage $msg
     * @throws \Exception
     */
    public function setStatus(AMQPMessage $msg)
    {
        $decodedMsg = json_decode($msg->body, true);
        $merlinStatus = (int)$decodedMsg['status'];
        $orderNumber = $decodedMsg['frontendOrderNumber'];

        $profitStatus = $this->getProfitStatus($merlinStatus);
        $profitInternalStatus = $this->getProfitInternal($profitStatus);

        if ( $merlinStatus > 0 && $orderNumber !== null && $orderNumber !== "" && $this->isMerlinStatus($merlinStatus) ) {

            $values = [
                'order_status' => $profitStatus,
                'internal_status' => $profitInternalStatus
            ];

            if ( $profitStatus > 0 ) {
                $values['status_' . $profitStatus . '_update'] = $decodedMsg['setAt'];
                $values['status_' . $profitStatus . '_update_by'] = 'merlin';
            }

            $order = $this->pDbMgr->GetRow('profit24', 'SELECT id, order_status, internal_status FROM orders WHERE order_number = "' . $orderNumber . '" LIMIT 1');

            if ($order['order_status'] !== $profitStatus || $order['internal_status'] !== $profitInternalStatus) {

                $this->pDbMgr->Update('profit24', 'orders', $values, 'order_number = "' . $orderNumber . '" LIMIT 1');

                $this->emailSender->sendEmail($merlinStatus, $order['id']);

            }
        }
        var_dump($decodedMsg);

        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

        if ($this->limit > 1000) {
            $msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
        } else {
            $this->limit++;
        }
    }

    /**
     * Utworzenie połączenia z rabbitem
     *
     * @return bool|string
     */
    private function openRabbitConnection()
    {
        global $aConfig;

        try {
            $connection = new AMQPStreamConnection($aConfig['rabbitmq']['host'], $aConfig['rabbitmq']['port'], $aConfig['rabbitmq']['user'], $aConfig['rabbitmq']['pass'], $aConfig['rabbitmq']['vhost']);
            $channel = $connection->channel();
            $this->connection = $connection;
            $this->channel = $channel;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return false;
    }

    /**
     * Pobranie statusu w proficie na podstawie statusu w merlinie
     *
     * @param $merlinStatus
     * @return string
     */
    private function getProfitStatus($merlinStatus)
    {
        switch ($merlinStatus) {
            case 24:
            case 980:
            case 1900:
            case 1999:
                $status = '5';
                break;
            case 68:
                $status = '1';
                break;
            case 70:
                $status = '2';
                break;
            case 2000:
                $status = '4';
                break;
            case 2:
            default:
                $status = '0';
                break;
        }

        return $status;
    }

    /**
     * Sprawdzanie czy status jest zmapowany
     *
     * @param $merlinStatus
     * @return string
     */
    private function isMerlinStatus($merlinStatus)
    {
        $merlinStatuses = [
            24,
            980,
            1900,
            1999,
            2,
            68,
            70,
            2000
        ];


        return in_array($merlinStatus, $merlinStatuses, true);
    }

    /**
     * Pobranie wewnętrznego statusu Profitu na podstawie statusu
     *
     * @param $profitStatus
     * @return null|string
     * @throws \Exception
     */
    private function getProfitInternal($profitStatus)
    {
        if ($profitStatus > 5 || $profitStatus < 0) {
            throw new \Exception("Zły status zamówienia");
        }

        $profitInternalStatus = null;

        // statusy wewnętrzne
        if ($profitStatus === '5') { // anulowane
            $profitInternalStatus = 'A'; // anulowane
        }
        if ($profitStatus === '4') { // zrealizowane
            $profitInternalStatus = '9'; //wysłane
        }
        if ($profitStatus === '1') { // w realizacji
            $profitInternalStatus = '3'; // do realizacji
        }
        if ($profitStatus === '2') { // skompletowane
            $profitInternalStatus = '4'; // skompletowane
        }
        if ($profitStatus === '3') { // zatwierdzone
            $profitInternalStatus = '5'; // wysyłka
        }
        if ($profitStatus === '0') {
            $profitInternalStatus = '1';
        }
        return $profitInternalStatus;
    }
}