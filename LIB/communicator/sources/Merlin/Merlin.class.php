<?php

namespace communicator\sources\Merlin;

//include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';
require_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/communicator/sources/CommonSources.class.php');

//require_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/merlin/erp-queue-manager/src/ErpQueueManager.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class Merlin extends \CommonSources
{
    public $testMode;
    //rabbitMQ
    public $connection;
    public $channel;
    public $line_end = "\n";

    public $no_status = 0;
    public $status_sent = 1;
    public $status_error = 2;

    public $postURL = 'http://172.16.5.45/api/customer-order/create';

    public $queue_name = 'erp-frontcom-order-create';

    public $consumer_exchange_name = 'frontend-invoice-created';
    public $consumer_queue_name = 'frontend-invoice-created-';

    public $sendMethod = 'RABBIT'; //RABBIT

    public $saveDir = '/var/www/vhosts/profit24.pl/invoices/merlin/';

    public $limit = 1000;


    /*
     * Frontcom json format
     {
         "frontendId": 200,
         "frontendOrderNumber": 5,
         "shipmentMethodId": 104,
         "shipmentPrice": 10.00,
         "shipmentDetails": "KODJAKIS-0001456",
         "itemsPrice": 40.00,
         "paymentMethodId": 100,
         "customerOrderAddress": {
           "firstName": "Jan",
           "lastName": "Nowak",
           "phone": "+48555666777",
           "email": "jnowak@dumm.email",
           "company": "",
           "nip": null,
           "locationCity": "Kraków",
           "locationStreet": "Warszawska",
           "locationBuilding": "5",
           "locationZip": "55-555",
           "locationCountryId": 1
         },
         "customerOrderAddressAdditional": {
           "firstName": "Jan",
           "lastName": "Nowak",
           "phone": "+48555666777",
           "email": "jnowak@dumm.email",
           "company": "",
           "nip": null,
           "locationCity": "Kraków",
           "locationStreet": "Warszawska",
           "locationBuilding": "5",
           "locationZip": "55-555",
           "locationCountryId": 1

        },
         "customer": {
           "firstName": "Jan",
           "middleName": null,
           "lastName": "Nowak",
           "birthday": null,
           "sex": null,
           "email": null,
           "phone": "+48555666777"
         },
         "orderItems": [
           {
             "productId": 4,
             "price": 40.00,
             "priceOnFrontend": 40.00
           }
         ]
        }
     */

    public function __construct($testMode = false)
    {
        global $pDbMgr;
        $this->testMode = $testMode;
        $this->pDbMgr = $pDbMgr;

        $this->PaymentStatus = new PaymentStatus($pDbMgr);
    }

    public function openRabbit()
    {
        global $aConfig;
        $this->connection = new AMQPStreamConnection($aConfig['rabbitmq']['host'], $aConfig['rabbitmq']['port'], $aConfig['rabbitmq']['user'], $aConfig['rabbitmq']['pass']);
        //$this->connection = new AMQPStreamConnection('172.16.5.213','5672', 'test','7SztdEwz8RMTQbvC');
        $this->channel = $this->connection->channel();
    }

    public function closeRabbit()
    {
        $this->channel->close();
        $this->connection->close();
    }

    public function getOrderData($id)
    {
        $sSql = 'SELECT o.id,o.transport_id,o.transport_cost,o.value_brutto,o.invoice_to_pay_days,o.order_number,o.payment_type,o.email,o.phone,o.phone_paczkomaty,o.point_of_receipt,w.merlin_frontend_id,om.send_status FROM orders o 
                 JOIN websites w 
                 ON o.website_id = w.id
                 LEFT JOIN orders_merlin om
                 on o.id = om.order_id
                 WHERE o.id = "'.$id.'"
                 ORDER BY id DESC';

        return \Common::GetAll($sSql);
    }

    public function getOrdersData()
    {
        $sSql = 'SELECT o.id,o.transport_id,o.transport_cost,o.value_brutto,o.invoice_to_pay_days,o.order_number,o.payment_type,o.email,o.phone,o.phone_paczkomaty,o.point_of_receipt,w.merlin_frontend_id,om.send_status FROM orders o 
                 JOIN websites w 
                 ON o.website_id = w.id
                 LEFT JOIN orders_merlin om
                 on o.id = om.order_id
                 WHERE (om.send_status IS NULL OR om.send_status <> 1) AND o.order_status <> \'3\' AND o.order_status <> \'4\' AND o.order_status <> \'5\' AND (seller_streamsoft_id = \'PH02\' OR seller_streamsoft_id = \'PH07\' OR seller_streamsoft_id = \'\')
                 ORDER BY o.id DESC
                 ';//o.id granica umowna

        return \Common::GetAll($sSql);
    }

    public function getOrdersAddresses($id)
    {
        $sSql = 'SELECT * FROM orders_users_addresses WHERE order_id = '.$id;

        return \Common::GetAll($sSql);
    }

    public function getOrderItemsData($id = 0)
    {
        $sSql = 'SELECT oi.*,
                        ( 
                          SELECT merlin_id 
                          FROM products_merlin_mappings pmm 
                          WHERE oi.product_id = pmm.product_id 
                          ORDER BY pmm.main_sku DESC
                          LIMIT 1
                        ) AS merlin_id
                 FROM orders_items oi
                 WHERE oi.order_id = "'.$id.'" 
                      AND oi.deleted = "0"
                      AND (oi.item_type = "I" OR oi.item_type = "P")
                      AND oi.packet = "0"
                 HAVING merlin_id IS NOT NULL
                 ';

        return \Common::GetAll($sSql);
    }

    public function getOrderItemsCount($id = 0)
    {
        $sSql = 'SELECT COUNT(id) FROM orders_items as oi
                 WHERE order_id = "'.$id.'"
                      AND oi.deleted = "0"
                      AND (oi.item_type = "I" OR oi.item_type = "P")
                      AND oi.packet = "0" 
                 ';

        return \Common::GetOne($sSql);
    }

    public function getOrdersMerlin()
    {
        $sSql = 'SELECT * FROM orders_merlin';

        return \Common::GetAll($sSql);
    }

    public function markOrdersMerlin($id = 0,$status = 0,$order)
    {
        if ($id !== 0)
        {
            $sSql2 = 'SELECT order_id FROM orders_merlin WHERE order_id ="'.$id.'" ';
            $query = $this->pDbMgr->GetOne('profit24',$sSql2);
            if ($query === NULL)
            {
                $sSql = 'INSERT INTO orders_merlin (order_id,send_status) VALUES ("' . $id . '","' . $status . '")';
                #echo 'insert '.$id.$this->line_end;;
            }
            else
            {
                $sSql = 'UPDATE orders_merlin 
                         SET send_status = ' . $status . '
                         WHERE order_id = "' . $id.'"';
                #echo 'update '.$id.$this->line_end;
            }

            $this->pDbMgr->Query('profit24',$sSql);
        }
    }

    public function doExport($id = 0)
    {
        //get all orders to export
        if ($id > 0 )
        {
            $orders = $this->getOrderData($id);
        }
        else
        {
            $orders = $this->getOrdersData();
        }

        if ($this->sendMethod === 'RABBIT')
        {
            $this->openRabbit();
        }

        foreach($orders as $order)
        {
            echo "Procesuje ".$order['id'].$this->line_end;
            try {
                $data = $this->prepareData($order);

                /*
                if($_GET['test'] == 1)
                {
                    echo $data;die;
                }
                */

                if ($this->sendMethod === 'RABBIT')
                {
                    $msg = new AMQPMessage($data);
                    //echo $data;
                    $this->channel->basic_publish($msg, $this->queue_name);
                }
                else
                {
                    $this->sendPost($data);
                }

                $status = $this->status_sent;

                //$this->PaymentStatus->update($order['id']);

                echo $order['id'].' wyeksportowany do Merlina'.$this->line_end;

            } catch (\Exception $e) {
                $status = $this->status_error;
                echo $e->getMessage();
            }

            $this->markOrdersMerlin($order['id'], $status, $order);
        }

        if ($this->sendMethod === 'RABBIT')
        {
            $this->closeRabbit();
        }
    }

    public function sendPost($data)
    {
        $url = $this->postURL;

        // use key 'http' even if you send the request to https://...
        $options = array(
            'http' => array(
                'header'  => "Content-type: application/json\r\n" . "apiKey: kLnN9gjyDu3BmR8gfuhLVYMY5YEg63XP\r\n",
                'method'  => 'POST',
                'content' => $data
            )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) { /* Handle error */ }

        var_dump($result);
    }

    public function consumer($queue_name = 'profit')
    {
        $this->consumed = 0;

        $callback = function ($msg) {
            echo ' [x] Received ', $msg->body, "\n";

            $this->processInvoiceFromMerlin($msg->body);

            if ($this->consumed > $this->limit) {
                $msg->delivery_info['channel']->basic_cancel($msg->delivery_info['consumer_tag']);
            } else {
                $this->consumed++;
            }

            $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
        };

        $known_consumers = array('profit24','nieprzeczytane','mestro','smarkacz','naszabiblioteka');

        if (!in_array($queue_name ,$known_consumers))
        {
            throw new \Exception("Nie ma takiego consumera - ".$queue_name.$this->line_end);
        }

        $this->channel->basic_qos(null, 1, null);

        $this->channel->basic_consume($this->consumer_queue_name.$queue_name, '', false, false, false, false, $callback);

        while (count($this->channel->callbacks)) {
            $this->channel->wait(null,false,10);
        }

        $this->closeRabbit();
    }

    public function processInvoiceFromMerlin($data)
    {
        $pure_data = json_decode($data,true);

        //save pdf

        /*

        $def = file_get_contents($pure_data['invoiceFilePath']);

        $file = $this->saveDir.$pure_data['orderFrontendNumber'].'.pdf';

        if (file_put_contents($file, $def) !== false)
        {
            echo 'Invoice dla ordera '.$pure_data['orderFrontendNumber'].' zapisany'.$this->line_end;
        }
        else
        {
            echo 'Nie udało się zapisać pliku dla ordera'.$pure_data['orderFrontendNumber'];
        }

        */

        $sSql = 'SELECT id FROM orders WHERE order_number = "'.$pure_data['orderFrontendNumber'].'" ';

        $orderExists = $this->pDbMgr->GetOne('profit24',$sSql);

        if ($orderExists !== NULL)
        {
            //save record to orders_merlin_invoices

            $sSql = 'INSERT INTO orders_merlin_invoices (order_number,invoice_url,invoice_number) VALUES ("' . $pure_data['orderFrontendNumber'] . '","' . $pure_data['invoiceFilePath'] . '","'.$pure_data['invoiceNumber'].'")';

            $query = $this->pDbMgr->Query("profit24",$sSql);

            if ($query !== false)
            {
                echo 'Faktura dodana do tablicy dla '.$pure_data['orderFrontendNumber'].$this->line_end;
                return true;
            }
            else
            {
                echo 'Nie udalo się dodać rekordu '.$pure_data['orderFrontendNumber'].$this->line_end;
                var_dump($query);
                return false;
            }
        }
        else
        {
            echo "Brak order_number po stronie profitu ".$pure_data['orderFrontendNumber'].$this->line_end;
        }
    }

    public function getMerlinShipment($shipment_id)
    {

        /*
        Merlin
        101 - Kurier 24 (UPS) DPD / UPS ( Losowanie )
        102 - POCZTEX
        103 - INPOST_LIST
        104 - INPOST_PACZKOMAT
        105 - Kiosk RUCHu
        106 - Poczta Polska
        107 - UPS EU
        */

        /*
        Profit
        1 - Fedex - WYPADA
        2 - Odbiór osobisty - NIE MA ODBIORU OSOBISTEGO
        3 - Paczkomaty InPost
        4 - Poczta Polska Odbiór w punkcie - 4
        5 - Paczka w RUCHu
        8 - Poczta Polska Doręczenie pod adres - 8 )
        */

        $mapping = array();

        $mapping[1] = 101; //tymczasowe mapowanie do DPD później status -> 112 UPS
        $mapping[3] = 104; //paczkomaty do dodania w erpie
        $mapping[5] = 105; //ruch
        $mapping[4] = 106; //poczta polska do punktu
        $mapping[8] = 102; //poczta polska pod adres

        if (isset($mapping[$shipment_id]) === false)
        {
            throw new \Exception("Brak mapowania dla danej metody wysyłki - ".$shipment_id.$this->line_end);
        }

        return $mapping[$shipment_id];
    }

    public function getMerlinPayment($payment_type,$invoice_to_pay_days = 0)
    {
        /*
        Merlin
        100 - Za pobraniem
        101 - Przedpłata
        103 - PaYu
        104 - Allegro
        189 - Masterpass
        190 - Blik
        200 - Bon towarowy
        */

        /*
        Profit
        "bank_14days"
        "bank_transfer"
        "card"
        "platnoscipl"
        "payu_bank_transfer"
        "postal_fee"
        */

        $mapping = array();

        $mapping["bank_transfer"] = 101; //przedplata
        $mapping["payu_bank_transfer"] = 103; //przeplata payu
        $mapping["card"] = 103; //karty razem z payu
        $mapping["platnoscipl"] = 103; //payu
        $mapping["postal_fee"] = 100; //za pobraniem

        if ($payment_type == 'bank_14days')
        {
            $mapping["bank_14days"] = 1014;
            /*
            const BANK_TRANSFER_7_DAYS      = 1007;
            const BANK_TRANSFER_10_DAYS     = 1010;
            const BANK_TRANSFER_14_DAYS     = 1014;
            const BANK_TRANSFER_21_DAYS     = 1021;
            const BANK_TRANSFER_30_DAYS     = 1030;
            const BANK_TRANSFER_45_DAYS     = 1045;
            const BANK_TRANSFER_60_DAYS     = 1060;
            */

            $invoice_days = array(
                7  => 1007,
                10 => 1010,
                14 => 1014,
                21 => 1021,
                30 => 1030,
                45 => 1045,
                60 => 1060
            );

            if ($invoice_to_pay_days !== 0 && isset($invoice_days[$invoice_to_pay_days]))
            {
                $mapping["bank_14days"] = $invoice_days[$invoice_to_pay_days];
            }
        }

        if (isset($mapping[$payment_type]) === false)
        {
            throw new \Exception("Brak mapowania dla danej metody płatności - ".$payment_type." - ".$invoice_to_pay_days.$this->line_end);
        }

        return $mapping[$payment_type];
    }

    public function getDataFromRuch($point_of_receipt)
    {
        $sSql = 'SELECT details FROM shipping_points_ruch
                 WHERE id = "'.$point_of_receipt.'"';

        return \Common::GetOne($sSql);
    }

    public function cutCharsToLimit($name,$surname,$limit = 30)
    {
        if (mb_strlen($name) + mb_strlen($surname) > $limit - 1)
        {
            //echo $address["name"].' '.$address["surname"];
            if (mb_strlen($surname) > $limit-3) {
                $cut["lastName"] = mb_substr($surname, 0, 27,'utf-8');
                $cut["firstName"] = mb_substr($name, 0, 1,'utf-8');
            }
            else
            {
                $allowedLength = 28-mb_strlen($surname);
                $cut['lastName'] = $surname;
                if ($allowedLength > 0)
                {
                    $cut["firstName"] = mb_substr($name, 0, $allowedLength,'utf-8');
                }
                else
                {
                    $cut["firstName"] = "";
                }
            }
        }
        else
        {
            $cut['firstName'] = trim($name);
            $cut['lastName'] = trim($surname);
        }

        $cut['length'] = mb_strlen($cut['firstName']) + mb_strlen($cut['lastName']);

        return $cut;
    }

    public function prepareData($orderData)
    {
        $orderItemsData = $this->getOrderItemsData($orderData['id']);

        //sprawdzic czy wszystkie produkty mają merlin_id u nas
        if (count($orderItemsData) != $this->getOrderItemsCount($orderData['id']))
        {
            throw new \Exception("Brak mapowań (merlin_id) dla przynajmniej jednego z produktów zamówienia ".$orderData['id'].$this->line_end);
        }

        $orderAddress = $this->getOrdersAddresses($orderData['id']);

        $data = array();

        $data["frontendId"] = (int)$orderData['merlin_frontend_id'];
        //$data['number'] = '';
        $data["frontendOrderNumber"]  =  $orderData['order_number'];
        $data["shipmentMethodId"]  =  (int)$this->getMerlinShipment($orderData['transport_id']);

        if ($orderData['transport_id'] == 5)
        {
            $base64data = $this->getDataFromRuch($orderData['point_of_receipt']);
            $ruchData = unserialize(base64_decode($base64data));
            if (!empty($ruchData['destination']))
            {
                $orderData['point_of_receipt'] = $ruchData['destination'];
            }
        }

        $data["shipmentPrice"]  =  (float)$orderData['transport_cost'];
        $data["shipmentDetails"]  =  $orderData['point_of_receipt'];
        $data["itemsPrice"]  =  (float)$orderData['value_brutto'];
        $data["paymentMethodId"]  =  (int)$this->getMerlinPayment($orderData['payment_type'],$orderData['invoice_to_pay_days']);
        $data["currencyCode"] = 'PLN';
        $data["isDigital"] = 0;
        //$data["currencyRate"] = '';
        //$data['currencyDate'] = DateTime today

        foreach($orderAddress as $address)
        {
            if ($address['address_type'] == '1')
            {
                $address_field_name = 'customerOrderAddressAdditional';
            }
            else
            {
                $address_field_name = 'customerOrderAddress';
            }

            $addrs = array();
            //walidacja po stronie merlina że te dwa pola razem nie mogą mieć więcej niż 29 znaków (PP)

            $addr = $this->cutCharsToLimit($address['name'],$address['surname'],29);

            if ($addr['length'] == 0)
            {
                $addr2 = $this->cutCharsToLimit($orderAddress[0]["name"],$orderAddress[0]["surname"],29);

                if ($addr2['length'] == 0)
                {
                    $addr3 = $this->cutCharsToLimit($orderAddress[1]["name"],$orderAddress[1]["surname"],29);

                    if ($addr3['length'] == 0)
                    {
                        //throw new \Exception("Brak wystarczającej ilości danych ".$orderData['id'].$this->line_end);
                    }
                    else
                    {
                        $data[$address_field_name]["lastName"] = $addr3['lastName'];
                        $data[$address_field_name]["firstName"] = $addr3['firstName'];
                        $addrs['lastName'] = $addr3['lastName'];
                        $addrs['firstName'] = $addr3['firstName'];
                    }
                }
                else
                {
                    $data[$address_field_name]["lastName"] = $addr2['lastName'];
                    $data[$address_field_name]["firstName"] = $addr2['firstName'];
                    $addrs['lastName'] = $addr2['lastName'];
                    $addrs['firstName'] = $addr2['firstName'];
                }
            }
            else
            {
                $data[$address_field_name]["lastName"] = $addr['lastName'];
                $data[$address_field_name]["firstName"] = $addr['firstName'];
                $addrs['lastName'] = $addr['lastName'];
                $addrs['firstName'] = $addr['firstName'];
            }

            if (empty($data[$address_field_name]["lastName"])) {
                if ($address["is_company"]) {
                    $data[$address_field_name]["lastName"] = mb_substr($address["company"], 0, 22,'utf-8');
                    $data[$address_field_name]["firstName"] = mb_substr($address["company"], 0, 7,'utf-8');
                    $addrs['lastName'] = $data[$address_field_name]["lastName"];
                    $addrs['firstName'] = $data[$address_field_name]["firstName"];
                } else {
                    $data[$address_field_name]["lastName"] = 'XXXXXXX';
                    $addrs['lastName'] = 'XXXXXXX';
                }
            }
            if (empty($data[$address_field_name]["firstName"])) {
                if ($address["is_company"]) {
                    $data[$address_field_name]["firstName"] = 'Firma';
                    $addrs['firstName'] = 'Firma';
                } else {
                    $data[$address_field_name]["firstName"] = 'XXXXXXX';
                    $addrs['firstName'] = 'XXXXXXX';
                }
            }


            if ($orderData['transport_id'] == '3') //paczkomaty
            {
                $data[$address_field_name]["phone"] = "+48 ".$orderData["phone_paczkomaty"];
            }
            else
            {
                if(!empty($address['phone']))
                {
                    $data[$address_field_name]["phone"] = "+48 ".$address["phone"];
                }
                else
                {
                    $data[$address_field_name]["phone"] = "+48 ".$orderData["phone"];
                }

            }

            $data[$address_field_name]["email"] = $orderData["email"];
            $data[$address_field_name]["company"] = $address["is_company"] ? $address["company"] : "";
            $data[$address_field_name]["nip"] = $address["is_company"] ? $address["nip"] : "";
            $data[$address_field_name]["locationCity"] = $address["city"];
            $data[$address_field_name]["locationStreet"] = $address["street"];

            if (empty($address["number2"]))
            {
                $data[$address_field_name]["locationBuilding"] = $address["number"];
            }
            else
            {
                $data[$address_field_name]["locationBuilding"] = $address["number"].'/'.$address["number2"];
            }

            $data[$address_field_name]["locationZip"] = $address["postal"];
            $data[$address_field_name]["locationCountryId"] = 1; //Polska
        }

        $customer = 'customer';

        $maxLength = 28;

        if (empty($address['surname']))
        {
            $data[$customer]["lastName"] = mb_substr($addrs["lastName"],0,$maxLength,'utf-8');
            $maxLength = $maxLength - mb_strlen($addrs['lastName']);
        }
        else
        {
            $data[$customer]["lastName"] = mb_substr($address["surname"],0,$maxLength,'utf-8');
            $maxLength = $maxLength - mb_strlen($address['surname']);
        }

        if (empty($address['name']))
        {
            if ($maxLength > 0)
            {
                $data[$customer]["firstName"] = mb_substr($addrs["firstName"],0,$maxLength,'utf-8');
            }
            else
            {
                $data[$customer]["firstName"] = "";
            }
        }
        else
        {
            if ($maxLength > 0)
            {
                $data[$customer]["firstName"] = mb_substr($address["name"],0,$maxLength,'utf-8');
            }
            else
            {
                $data[$customer]["firstName"] = "";
            }
        }

        //echo $data[$customer]["firstName"]." ".$data[$customer]["lastName"];

        $data[$customer]["middleName"] = null; // nie mamy

        $data[$customer]["birthday"] = null; // nie mamy
        $data[$customer]["sex"] = null; // nie mamy
        $data[$customer]["email"] = $orderData["email"];
        $data[$customer]["phone"] = "+48 ".$address["phone"];//$orderData["phone_paczkomaty"];

        $itemData = array();

        foreach($orderItemsData as $item)
        {
            for($item['quantity']; $item['quantity'] > 0;$item['quantity']--)
            {
                $itemData['productId'] = $item['merlin_id'];//mapped merlin id
                $itemData['price'] = floatval($item['promo_price_brutto'] ? $item['promo_price_brutto'] : $item['price_brutto']);
                $itemData['priceOnFrontend'] = floatval($item['price_brutto']);
                $data["orderItems"][] = $itemData;
            }
        }

        if ($this->sendMethod !== 'POST')
        {
            return json_encode($data,JSON_PRETTY_PRINT);
        }
        else
        {
            return json_encode($data);
        }
    }
}
