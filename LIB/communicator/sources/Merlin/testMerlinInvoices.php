<?php

header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Merlin';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina -
ini_set("memory_limit", '1G');

//ini_set('error_reporting', E_ALL ^ E_STRICT);
//ini_set('display_errors', 'On');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

global $pDbMgr;
$bTestMode = true;

try {
        $oMerlin = new communicator\sources\Merlin\Merlin($bTestMode);
        $oMerlin->openRabbit();
        $oMerlin->consumer($argv[1]); //consumer
        //$oMerlin->proxyInvoice('https://accounting.merlin.pl/api/v1/invoice/fd6b2593a16a650281c3fee95f3d9439');
}
catch (\Exception $ex)
{
    echo $ex->getMessage();
}
