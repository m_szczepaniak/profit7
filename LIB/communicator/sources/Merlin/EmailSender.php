<?php
namespace communicator\sources\Merlin;

use Common;

class EmailSender {

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    function __construct($pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
    }

    public function sendEmail($merlinStatus, $id)
    {
        $website = $this->getWebsiteSymbol($this->getWebsiteId($id));
        $email = $this->findEmailsByMerlinStatus($website, $merlinStatus);

        if ( $email !== null ) {
            $this->sendOrderStatusMail($id, $email);
        }
    }

    /**
     * Wysyła email używając id zamówienia i sybolu emaila
     *
     * @param int $id
     * @param $email
     * @return boolean
     * @global array $aConfig3
     */
    public function sendOrderStatusMail($id, $email) {
        global $aConfig;
        $aMatches = array();

        $sWebsite = $this->getWebsiteSymbol($this->getWebsiteId($id));

        setWebsiteSettings(1, $sWebsite);

        include($_SERVER['DOCUMENT_ROOT'].'omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
        $aModule['lang'] = & $aConfig['lang']['m_zamowienia_mail'];

        $_GET['lang_id'] = 1;
        $aSiteSettings = getSiteSettings($sWebsite);
        $sFrom = $aConfig['default']['website_name'];
        $sFromMail = $aSiteSettings['orders_email'];

        $sSql = "SELECT *,
                    MD5(CONCAT(check_status_key, order_number)) as get_invoice_key,
                    (transport_cost + value_brutto) as total_value_brutto,
                    DATE_FORMAT(order_date, '" . $aConfig['common']['sql_date_hour_format'] . "') as order_date,
                    DATE_FORMAT(status_1_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_1_update,
                    DATE_FORMAT(status_2_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_2_update,
                    DATE_FORMAT(status_3_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_3_update,
                    DATE_FORMAT(status_4_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_4_update,
                    DATE_FORMAT(status_5_update, '" . $aConfig['common']['sql_date_hour_format'] . "') as status_5_update
						 FROM " . $aConfig['tabls']['prefix'] . "orders
						 WHERE id = " . $id;
        $aModule['order'] = & Common::GetRow($sSql);

//        $sMail = $aConfig['lang']['m_zamowienia']['mail_status_' . $aModule['order']['order_status']];
//        if ($aModule['order']['order_status'] == '4') {
//            $sTMeansSymbol = $this->getTransportMeansSymbol($aModule['order']['transport_id']);
//            switch ($sTMeansSymbol) {
//                case 'odbior-osobisty':
//                    $sMail .= '_odb_osob';
//                    break;
//                case 'paczkomaty_24_7':
//                    $sMail .= '_paczkomaty';
//                    break;
//                case 'poczta_polska':
//                    $sMail .= '_poczta_pl';
//                    break;
//                case 'opek-przesylka-kurierska':
//                    $sMail .= '_kurier';
//                    break;
//                case 'poczta-polska-doreczenie-pod-adres':
//                    $sMail .= '_poczta_adres';
//                    break;
//                case 'ruch':
//                    $sMail .= '_ruch';
//                    break;
//                case 'orlen':
//                    $sMail .= '_orlen';
//
//                    $sTransportNumber = $aModule['order']['transport_number'];
//                    preg_match("/^\d{4}(\d{13})/", $sTransportNumber, $aMatches);
//                    $sTransportNumber = $aMatches[1];
//                    $aModule['order']['transport_number'] = $sTransportNumber;
//                    break;
//                case 'tba':
//                    $sMail .= '_tba';
//                    break;
//            }
//            return true;
//        }
        $sFVLink = str_replace('http://', 'https://', $aConfig['common'][$sWebsite]['client_base_url_http_no_slash']).'/sprawdz-status-zamowienia/id'.$id.',invoice-new.html?key='.$aModule['order']['get_invoice_key'];

        if ($this->doesFvExist($id))
        {
            $sFVline = 'Elektroniczną fakturę VAT można pobrać <a href="'.$sFVLink.'"><strong>tutaj.</strong></a> <br />';
        }
        else
        {
            $sFVline = '';
        }
        $aVars = array('{nr_zamowienia}','{data_statusu}','{list_przewozowy}', '{link_do_faktury}','{faktura_vat}');
        $aValues = array($aModule['order']['order_number'], $aModule['order']['status_'.$aModule['order']['order_status'].'_update'], $aModule['order']['transport_number'], $sFVLink,$sFVline);

        $aMail = Common::getWebsiteMail($email, $sWebsite);

        $attachments = [];
        if($aModule['order']['order_status'] == '3' && $sWebsite != 'np'){
            $attachments[] = [realpath($_SERVER['DOCUMENT_ROOT'].'/images/email_attachments/'.$sWebsite.'/Informacja.pdf')];
            $attachments[] = [realpath($_SERVER['DOCUMENT_ROOT'].'/images/email_attachments/'.$sWebsite.'/Zał.1 Odstąpienie od umowy.pdf')];
            $attachments[] = [realpath($_SERVER['DOCUMENT_ROOT'].'/images/email_attachments/'.$sWebsite.'/Zał.2 Reklamacja.pdf')];
        }

        if ($aMail['content'] != '') {
//            Common::sendWebsiteMail($aMail,$sFrom,$sFromMail,'raporty@profit24.pl',  $aMail['content'],$aVars,$aValues,array(), array(), $sWebsite, $attachments);
            return Common::sendWebsiteMail($aMail,$sFrom,$sFromMail, $aModule['order']['email'],  $aMail['content'],$aVars,$aValues,array(), array(), $sWebsite, $attachments);
        }
        return true;
    }

    function doesFVexist($id)
    {
        $sSql = 'SELECT invoice_url FROM orders_merlin_invoices OMI
                 LEFT JOIN orders O 
                 ON O.order_number = OMI.order_number
                 WHERE O.id = "'.$id.'"';

        if ($this->pDbMgr->GetOne('profit24',$sSql) === null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Metoda pobiera symbol serwisu na podstawie id serwisu
     *
     * @param int $websiteId
     * @return mixed|array
     */
    function getWebsiteSymbol($websiteId) {
        $sSql = "SELECT code FROM websites
						 WHERE id=".$websiteId;
        return $this->pDbMgr->GetOne('profit24',$sSql);
    }

    /**
     * Metoda pobiera id serwisu i wprowadza do $this->iWebsiteId
     *
     * @global array $aConfig
     * @param int $iOId
     * @param bool $bRemember
     * @return mixed
     */
    private function getWebsiteId($iOId) {
        global $aConfig;
        $sSql = "SELECT website_id FROM " . $aConfig['tabls']['prefix'] . "orders
                            WHERE id=" . $iOId;
        return Common::GetOne($sSql);
    }

    /**
     * Pobranie wszystkich emaili
     *
     * @return array
     */
    private function getWebsiteEmails($websiteSymbol) {
        return $this->pDbMgr->GetAll($websiteSymbol, "SELECT merlin_statuses, symbol FROM website_emails");
    }

    /**
     * Wyszukanie statusów merlina w emailach
     *
     * @param $websiteSymbol
     * @param $status
     * @return array
     */
    private function findEmailsByMerlinStatus($websiteSymbol, $status) {
        $emails = $this->getWebsiteEmails($websiteSymbol);

        foreach ($emails as $email) {
            $statuses = explode(',',$email['merlin_statuses']);
            foreach ($statuses as $emailStatus) {
                if ($status === (int)$emailStatus) {
                    return $email['symbol'];
                }
            }
        }

        return null;
    }
}