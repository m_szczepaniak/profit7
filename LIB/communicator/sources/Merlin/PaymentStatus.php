<?php
namespace communicator\sources\Merlin;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class PaymentStatus {

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    private $id;
    private $channel;
    private $connection;
    private $exchange = 'erp-frontcom-payment-status';

    private $payments;

    /*
        Merlin
        100 - Za pobraniem
        101 - Przedpłata
        103 - PaYu
        104 - Allegro
        189 - Masterpass
        190 - Blik
        200 - Bon towarowy
    */
    /*
        Profit
        "bank_14days"
        "bank_transfer"
        "card"
        "platnoscipl"
        "postal_fee"
    */
    private $paymentMapping = [
        "bank_transfer" => 101, //przedplata
        "card" => 103, //karty razem z payu
        "platnoscipl" => 103, //payu
        "postal_fee" => 100 //za pobraniem
    ];

    function __construct($pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
    }


    public function update($id = 0)
    {
        $this->id = $id;
        $this->sendBankPayments();
        $this->sendPlatnosciplPayments();
        $this->sendPayments();
    }

    private function sendBankPayments()
    {
        $payments = $this->getBankPayments();

        foreach ($payments as $payment) {
            $merlinPayment = [
                'payment_id' => $payment['id'],
                'bank_statements_id' => $payment['statement_id'] === null ? 'NULL' : $payment['statement_id'],
                'order_id' => $payment['order_id'],
                'sync_status' => 0,
                'error_message' => 'NULL',
            ];

            $payment['merlin_id'] = $this->insertIntoMerlinPayments($merlinPayment, $payment['merlin_id']);
            $payment['paymentMethodId'] = $this->paymentMapping[$payment['type']];

            if (isset($this->payments[$payment['order_number']]) === false) {
                $this->payments[$payment['order_number']] = [];
            }

            $this->payments[$payment['order_number']][(int)$payment['id']] = $payment;
        }

        return false;
    }

    private function sendPlatnosciplPayments()
    {
        $payments = $this->getPlatnosciplPayments();

        foreach ($payments as $payment) {
            $merlinPayment = [
                'platnoscipl_sessions_id' => $payment['id'],
                'order_id' => $payment['order_id'],
                'sync_status' => 0,
                'error_message' => 'NULL',
            ];

            $payment['merlin_id'] = $this->insertIntoMerlinPayments($merlinPayment, $payment['merlin_id']);
            $payment['paymentMethodId'] = 103;

            if (isset($this->payments[$payment['order_number']]) === false) {
                $this->payments[$payment['order_number']] = [];
            }

            $this->payments[$payment['order_number']][(int)$payment['id']] = $payment;
        }

        return false;
    }

    private function insertIntoMerlinPayments($merlinPayment, $merlinId) {
        $id = NULL;
        if ($merlinId === NULL) {
            $id = $this->pDbMgr->Insert('profit24', 'merlin_payments', $merlinPayment);
        } else if ($merlinId !== NULL) {
            $id = $merlinId;
        }
        return $id;
    }

    private function getBankPayments() {
        $orderSQL = '';
        if ((int)$this->id > 0)
        {
//            $orderSQL = ' AND o.id = "'.(int)$this->id.'" ';
        }

        $sSql = 'SELECT opl.*, o.order_number, w.merlin_frontend_id, mp.sync_status, mp.id as merlin_id
                FROM orders_payments_list opl
                LEFT JOIN orders o
                ON opl.order_id = o.id
                LEFT JOIN websites w
                ON o.website_id = w.id 
                LEFT JOIN merlin_payments mp
                ON opl.id = mp.payment_id
                JOIN orders_merlin om
                ON opl.order_id = om.order_id
                WHERE (mp.sync_status IS NULL OR mp.sync_status IN (0,1))
                AND opl.ammount > 0
                AND opl.type IN ("bank_transfer", "card", "platnoscipl", "postal_fee")
                AND om.send_status = 1
                '.$orderSQL.'
                ORDER BY `date` DESC
                LIMIT 100 
              ;';

        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    private function getPlatnosciplPayments() {
        $orderSQL = '';
        if ((int)$this->id > 0)
        {
            //          $orderSQL = ' AND o.id = "'.(int)$this->id.'" ';
        }
        $sSql = 'SELECT ops.*, o.order_number, w.merlin_frontend_id, mp.sync_status, mp.id as merlin_id
                FROM orders_platnoscipl_sessions ops
                INNER JOIN orders o
                ON ops.order_id = o.id
                LEFT JOIN websites w
                ON o.website_id = w.id
                LEFT JOIN merlin_payments mp
                ON ops.id = mp.platnoscipl_sessions_id
                JOIN orders_merlin om
                ON ops.order_id = om.order_id
                WHERE (mp.sync_status IS NULL OR mp.sync_status IN (0,1))
                AND ops.status = 99
                AND om.send_status = 1
                '.$orderSQL.'
                ORDER BY date_created DESC
                LIMIT 100
              ;';

        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    private function openRabbitConnection() {
        global $aConfig;

        try {
            $connection = new AMQPStreamConnection($aConfig['rabbitmq']['host'], $aConfig['rabbitmq']['port'], $aConfig['rabbitmq']['user'], $aConfig['rabbitmq']['pass'], $aConfig['rabbitmq']['vhost']);
            $channel = $connection->channel();

            /*
                name: $exchange
                type: direct
                passive: false
                durable: true // the exchange will survive server restarts
                auto_delete: false //the exchange won't be deleted once the channel is closed.
            */
            $channel->exchange_declare($this->exchange, 'direct', false, true, false);

            $this->connection = $connection;
            $this->channel = $channel;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return false;
    }

    private function sendToRabbit($messageBody) {
        try {
            $message = new AMQPMessage($messageBody, array('content_type' => 'text/plain', 'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT));
            $this->channel->basic_publish($message, $this->exchange);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return false;
    }

    private function closeRabbitConnection() {
        try {
            $this->channel->close();
            $this->connection->close();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return false;
    }

    private function sendPayments()
    {
        foreach ($this->payments as $orderNumber => $payments) {
            $allPayments = $this->getPaymentsByOrderNumber($orderNumber);

            $latestPayment = $this->findLatestPayment($allPayments);

            $latestPayment = $this->sumPayments($latestPayment, $allPayments);

            $paymentToSend = $payments[$latestPayment['id']];
            $paymentToSend['amount'] = $latestPayment['amount'];

            $rabbitMessage = $this->prepareRabbitMessage($paymentToSend);

            $error = $this->openRabbitConnection();
            if ($error !== false) {
                echo $error;
                return true;
            }

            $statusData = [];
            foreach ($payments as $key => $payment) {
                $statusData[$key] = [
                    'sync_status' => '2',
                    'error_message' => ''
                ];
            }

            $error = $this->sendToRabbit($rabbitMessage);
            if ($error !== false) {
                foreach ($payments as $key => $payment) {
                    $statusData[$key]['sync_status'] = '1';
                    $statusData[$key]['error_message'] = $error;
                }
            }

            try {
                $this->pDbMgr->BeginTransaction('profit24');
                foreach ($payments as $key => $payment) {
                    $this->pDbMgr->Update('profit24', 'merlin_payments', $statusData[$key], 'id = ' . $payment['merlin_id']);
                }
                $this->pDbMgr->CommitTransaction('profit24');
            } catch (\Exception $e) {
                $this->pDbMgr->RollbackTransaction('profit24');
            }

            $this->closeRabbitConnection();
        }
    }

    private function getPaymentsByOrderNumber($order_number)
    {
        $sql = 'SELECT ops.id as id, ops.amount, date_created as `date` FROM orders o
                JOIN orders_platnoscipl_sessions ops
                ON o.id = ops.order_id
                WHERE o.order_number = "' . $order_number . '"
                AND ops.status = 99
                ORDER BY date_created DESC';
        $payments = $this->pDbMgr->GetAll('profit24', $sql);

        foreach ($payments as $key => $payment) {
            $payments[$key]['amount'] = $payment['amount']/100;
        }

        $sql = 'SELECT opl.id as id, opl.ammount as amount, `date` FROM orders o
                JOIN orders_payments_list opl
                ON o.id = opl.order_id
                WHERE o.order_number = "' . $order_number . '"
                AND opl.ammount > 0
                AND opl.type IN ("bank_transfer", "card", "platnoscipl", "postal_fee")
                ORDER BY `date` DESC';
        $payments_opl = $this->pDbMgr->GetAll('profit24', $sql);

        $payments = array_merge($payments, $payments_opl);

        return $payments;
    }

    private function findLatestPayment($payments) {
        $latestDate = strtotime($payments[0]['date']);
        $latestId = 0;
        foreach ($payments as $id => $payment) {
            if ( strtotime($payment['date']) > $latestDate ) {
                $latestId = $id;
            }
        }

        return $payments[$latestId];
    }

    private function sumPayments($latestPayment, $allPayments) {
        foreach ($allPayments as $payment) {
            if ($payment['id'] !== $latestPayment['id']) {
                $latestPayment['amount'] += $payment['amount'];
            }
        }
        return $latestPayment;
    }

    public function prepareRabbitMessage($paymentToSend) {
        $rabbitMessage = json_encode([
            'frontendId' => $paymentToSend['merlin_frontend_id'],
            'frontendOrderNumber' => $paymentToSend['order_number'],
            'currentStatusId' => 2,
            'paymentId' => $paymentToSend['merlin_id'],
            'paidAt' => $paymentToSend['date_recv'],
            'paymentMethodId' => $paymentToSend['paymentMethodId'],
            'amount' => $paymentToSend['amount']
        ]);

        return $rabbitMessage;
    }
}
