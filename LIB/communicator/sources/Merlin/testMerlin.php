<?php

//ini_set('display_errors', 'On');
//error_reporting(E_ALL);
/*
 * Testy poczty-polskiej
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-24
 * @copyrights Marcin Chudy - Profit24.pl
 */
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Merlin';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina -
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';
if ($aConfig['common']['status']	!= 'development') {
    $bTestMode = false;
} else {
    $bTestMode = true;
}
$sSql = 'SELECT * FROM orders_seller_data';
$aSellerData = $pDbMgr->GetRow('profit24', $sSql);

global $pDbMgr;
$bTestMode = true;
$oMerlin = new communicator\sources\Merlin\Merlin($bTestMode);
$oPaymentStatus= new communicator\sources\Merlin\PaymentStatus($pDbMgr);

try {

    $aOrder = array(
        'id' => 132691,
        'weight' => 22.1,
        'point_of_receipt' => '255445'
    );
    $aDeliverAddress = array(
        'name' => 'Arkadiusz',
        'surname' => 'Golba',
        'phone' => '690892692',
        'email' => 'arekgolba@gmail.com',

        'street' => 'Pulaskiego',
        'number' => '8',
        'number2' => '18',
        'postal' => '37-500',
        'city' => 'WARSZAWA',

    );

    /*
    if ($_GET['magia'] == 'czesc')
    {
        $payArray = array(
            '1318928',
            '1318927',
            '1318926',
            '1318925',
            '1318924',
            '1318923',
            '1318922',
            '1318921',
            '1318920',
            '1318919',
            '1318918',
            '1318917',
            '1318916',
            '1318915',
            '1318914',
            '1318913',
            '1318912',
            '1318911',
            '1318910',
            '1318909'
        );

        foreach($payArray as $onePayment)
        {
            $oPaymentStatus->update($onePayment);
            echo "Zprocesowane";
        }
    }
    */

    if (isset($_GET['order_id']))
    {
        $oMerlin->doExport((int)$_GET['order_id']);
        $oPaymentStatus->update((int)$_GET['order_id']);
    }
    else
    {
        //header('Content-Type: application/json');
        //echo $oMerlin->prepareData();
        $oMerlin->doExport();
        //$oMerlin->consumer(); //consumer
      $oPaymentStatus->update();
    }
} catch (\Exception $ex) {
    echo $ex->getMessage();
}
