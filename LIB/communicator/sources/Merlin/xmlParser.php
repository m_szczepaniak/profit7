<?php

header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/sources/Merlin';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina -
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

global $pDbMgr;
$bTestMode = true;

$xmlPath = $_SERVER['DOCUMENT_ROOT'].'/LIB/communicator/sources/Internal/streamsoft/import_files/xml/Stany/old/';
$eol = PHP_EOL;
/*

1.Wyciągnąć najnowszy plik dla danego magazynu dla danego folderu XX_[data].xml x
2.Otworzyć xml x
    2a.Dodać do bazy tabela streamsoft_data

*/

//$files = array_diff(scandir($xmlPath), array('..', '.')); //get rid of .. and .

chdir($xmlPath);

foreach (glob('9999_*.xml') as $file) {
    $files[] = $file;
}

$newest_files = array();

foreach($files as $file)
{
    $magazine = mb_substr($file,0,2);
    $date = mb_substr($file,3,14);
    if ( isset($newest_files[$magazine]['date']) )
    {
        if ($date > $newest_files[$magazine]['date'])
        {
            $newest_files[$magazine]['filename'] = $file;
            $newest_files[$magazine]['date'] = $date;
        }
    }
    else
    {
        $newest_files[$magazine]['filename'] = $file;
        $newest_files[$magazine]['date'] = $date;
    }
}


//open xml
$xml = new XMLReader();

foreach($newest_files as $xmlFile) {
    echo 'Procesuje ->'.$xmlFile['filename'].$eol;
    $res = $xml->open($xmlPath . $xmlFile['filename']);
    $xml->setParserProperty(2, true);

    //	Step into the first level of content of the XML
    $xml->read();
    $cnt = 0;
    $brak = 0;
    $missing = '';
    while ($xml->read()) {
        while ($xml->read()) {
            if ($xml->name == 'STAN' && $xml->nodeType == XMLReader::ELEMENT) {
                //	Loop through each table:table node reading the table:name attribute for each worksheet name
                $elements = ['MAGAZYN', 'INDEKS', 'ILOSC', 'CENA_ZAKUPU', 'DOSTAWCA', 'NRDOK','DATADOK','RODZAJ','NRKONTRAH','STAWKA_VAT','CENA_SPRZ_NETTO','CENA_SPRZ_BRUTTO'];

                $node = new SimpleXMLElement($xml->readOuterXML());

                foreach ($elements as $element) {
                    $rowdata[$element] = (string)$node->{$element};
                }

                $rowdata['ILOSC'] = intval($rowdata['ILOSC']);
                $rowdata['MAGAZYN'] = intval($rowdata['MAGAZYN']);

                $product['ean_13'] = $rowdata['INDEKS'];

                $pro = refMatchProductByISBNs($product);

                $aValues = array(
                    'product_id' => $pro['id'],
                    'purchase_price' => \Common::formatPrice2($rowdata['CENA_ZAKUPU']),
                    'quantity' => $rowdata['ILOSC'],
                    'warehouse' => $rowdata['MAGAZYN'],
                    'quantity_taken' => 0,
                    'supplier' => $rowdata['DOSTAWCA'],
                    'supplier_id' => $rowdata['NRKONTRAH'],
                    'document_number' => $rowdata['NRDOK'],
                    'document_date' => $rowdata['DATADOK'],
                    'document_type' => $rowdata['RODZAJ'], //'MM' , 'PZ'
                    'vat' => str_replace("%", "", $rowdata['STAWKA_VAT']),
                    'price_netto' => \Common::formatPrice2($rowdata['CENA_SPRZ_NETTO']),
                    'price_brutto' => \Common::formatPrice2($rowdata['CENA_SPRZ_BRUTTO'])
                );

                if (!empty($pro['id']))
                {
                    if ($pDbMgr->Insert('profit24', 'streamsoft_stock', $aValues) === false) {
                        return false;
                    }
                    else
                    {
                        echo 'Dodałem rekord dla produktu '.$pro['id'].$eol;
                    }
                }
                else
                {
                    $brak++;
                    echo 'Puste product_id dla INDEKSU '.$rowdata['INDEKS'].$eol;
                    $missing .= $rowdata['INDEKS']."\n";
                }
                $cnt++;
            }
        }
    }

    echo 'Przerobionych : '.$cnt.' w tym brak id: '.$brak.$eol;
    file_put_contents($xmlPath.'missing.txt',$missing);
}

function refMatchProductByISBNs($aProduct, $aColsSelect = array('id')){
    global $pDbMgr;
    $aDBProduct = array();

    $sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
                    streamsoft_indeks = "%1$s"
                    ';

    if (empty($aDBProduct) && $aProduct['ean_13'] != '') {
        $sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
        $aDBProduct = $pDbMgr->GetRow('profit24', $sSql);
    }

    if (empty($aDBProduct) && $aProduct['ean_13'] != '') {
        $sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
                    ean_13 = "%1$s"
                    ';
        $sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
        $satesErr = $pDbMgr->GetRow('profit24', $sSql);
        if (!empty($satesErr)) {
            dump($aProduct['ean_13']);
            dump($satesErr);
        }
    }

    if (empty($aDBProduct) && empty($satesErr) && $aProduct['ean_13'] != '') {
        $sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
                    isbn_13 = "%1$s"
                    ';
        $sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
        $satesErr = $pDbMgr->GetRow('profit24', $sSql);
        if (!empty($satesErr)) {
            dump($aProduct['ean_13']);
            dump($satesErr);
        }
    }

    if (empty($aDBProduct) && empty($satesErr) && $aProduct['ean_13'] != '') {
        $sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
                    isbn_10 = "%1$s"
                    ';
        $sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
        $satesErr = $pDbMgr->GetRow('profit24', $sSql);
        if (!empty($satesErr)) {
            dump($aProduct['ean_13']);
            dump($satesErr);
        }
    }

    if (empty($aDBProduct) && empty($satesErr) && $aProduct['ean_13'] != '') {
        $sSqlSelect = 'SELECT '.implode(',', $aColsSelect).' 
									 FROM products
									 WHERE 
                    isbn_plain = "%1$s"
                    ';
        $sSql = sprintf($sSqlSelect, $aProduct['ean_13']);
        $satesErr = $pDbMgr->GetRow('profit24', $sSql);
        if (!empty($satesErr)) {
            dump($aProduct['ean_13']);
            dump($satesErr);
        }
    }

    /*
     * NIE JEST TO POTRZEBNE
    if (isset($aProduct[$sMatchedType]) && $aProduct[$sMatchedType] != '') {
        $aProduct['_matched_type'] = $sMatchedType;
        $aProduct['_matched_product'] = $aDBProduct;
    }
     */
    return $aDBProduct;
}// end of refMatchProductByISBNs() method