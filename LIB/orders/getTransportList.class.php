<?php
/**
 * Klasa pośrednicząca w drukowaniu listu przewozowego
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-10-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders;

use communicator\sources\Internal\Shipment;

ini_set('default_socket_timeout', 15);
class getTransportList {

  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;

  function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }

    /**
     * @param $iOrderId
     * @return bool
     */
  private function validatePaidOrder($iOrderId) {

      $sSql = 'SELECT payment_type, payment_status, paid_amount, to_pay, second_payment_enabled, second_payment_type 
               FROM orders 
               WHERE id = '.$iOrderId;
      $aOrder = $this->pDbMgr->GetRow('profit24', $sSql);
      if (($aOrder['payment_type'] == 'bank_transfer' || $aOrder['payment_type'] == 'dotpay' || $aOrder['payment_type'] == 'platnoscipl' || $aOrder['payment_type'] == 'payu_bank_transfer' || $aOrder['payment_type'] == 'card') &&
          ($aOrder['payment_status'] != 1 || ($aOrder['paid_amount'] < $aOrder['to_pay'])) && !($aOrder['second_payment_enabled'] == '1' && $aOrder['second_payment_type'] == 'postal_fee')) {
          //zezwala na wydruk faktury jesli wybrano 2 sposob platnosci jako pobranie
          return false;
      }
      return true;
  }

  public function getProfitLabel($orderNumber) {
      require_once 'LIB/communicator/sources/Internal/Shipment.class.php';

      $shipment = new \communicator\sources\Internal\Shipment();
      $shipment->getProfitLabel($orderNumber);
  }

    /**
     * Pobieramy nr listu przewozowego, dodajemy etykietę i możemy od razu wydrukować etykietę
     *
     * @param mixed $mTransportIdSymbol
     * @param int $iOrderId
     * @param array $aOrder
     * @param array $aLinkedOrders
     * @param bool $bAddPrintQueue
     * @param bool $ommitInternalLabel
     * @return bool
     * @throws \Exception
     * @global array $aConfig
     * @global \DatabaseManager $pDbMgr
     */
  public function proceedAddressLabel($mTransportIdSymbol, $iOrderId, $aOrder, $aLinkedOrders = array(), $bAddPrintQueue = true, $ommitInternalLabel = false) {
    global $aConfig, $pDbMgr;


    // VALIDUJEMY CZY ZAMÓWIENIE ZOSTAŁO OPŁACONE JEŚLI NIE JEST TO POSTAL_FEE lub FV_14_DNI
    if (false === $this->validatePaidOrder($iOrderId)) {
      throw new \Exception(_('Zamówienie jeszcze nie zostało opłacone, nie mozemy go wysłać'));
    }

    $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
    $oSpeditor = new \orders\Shipment($mTransportIdSymbol, $pDbMgr, $bTestMode);

    try {
        if (isset($aOrder['tmp_transport_number']) && $aOrder['tmp_transport_number'] === "1") {
            // zresetujmy transport_number
            $aOrder['transport_number'] = '';
            $aOrder['tmp_transport_number'] = '0';
            $this->updateOrderTransportNumber($iOrderId, '');
        }
        $aSellerData = $this->getSellerDataByWebsiteId($aOrder['website_id']);
      if ($aOrder['transport_number'] == '') {
        $aDeliverAddress = $this->getDeliverAddress($aOrder, $this->getOrderAddresses($iOrderId, false));
        
        $aOrder['weight'] = floatval(str_replace(',', '.', $this->getOrderWeight($iOrderId,$aOrder['bookstore'])));
        
        if (!empty($aLinkedOrders)) {
          if ($this->margeLinkedOrdersRef($aOrder, $aOrder['weight'], $aLinkedOrders) == false) {
            return false;
          }
        }
        $sTransportNumber = $oSpeditor->addShipment($aOrder, $aDeliverAddress, $aSellerData);
        $sFilePath = $oSpeditor->getTransportListFilename($aConfig, $sTransportNumber);
        if (!file_exists($sFilePath))
        {
            $oSpeditor->getAddressLabel($sFilePath, $iOrderId);
        }
        $this->insertLogoToPdf($sFilePath,$aSellerData,$oSpeditor->iTransportId);

        if ($sTransportNumber != '') {
          if ($this->saveTransportNumberInOrdersByiShipments($oSpeditor, $sTransportNumber, $iOrderId, $aLinkedOrders, $bAddPrintQueue, $aSellerData) === false) {
            throw new \Exception(_("Wystąpił błąd podczas pobierania numeru listu przewozowego"));
          }
          
        } else {
          throw new \Exception(_('Wystąpił nieznany błąd podczas tworzenia przesyłki'));
        }
        
      } else {
        $sTransportNumber = $aOrder['transport_number'];
        // jest list przewozowy 
        $sFilePath = $oSpeditor->getTransportListFilename($aConfig, $sTransportNumber);

        if (!file_exists($sFilePath)) {
          // pobierz list i wyślij na drukarkę
          if ($this->saveTransportNumberInOrdersByiShipments($oSpeditor, $sTransportNumber, $iOrderId, $aLinkedOrders, $bAddPrintQueue, $aSellerData) === false) {
            throw new \Exception(_("Wystąpił błąd podczas pobierania numeru listu przewozowego"));
          }
        } 
        else {
          if ($bAddPrintQueue == true) {
            // tylko wyślij na drukarkę
            if ($oSpeditor->doPrintAddressLabel($sFilePath, $_COOKIE['printer_labels']) === false) {
              throw new \Exception(_("Dodawania pliku do kolejki drukowania"));
            }
          }
        }
      }
    
    } catch (\Exception $ex) {
        if (true === $ommitInternalLabel) {
            throw new \Exception($ex->getMessage());
        } else {
            // Add 0 at the end to make it 13 long
            $orderNumber = $aOrder['order_number'].'0';

            $shipment = new Shipment();
            $shipment->getProfitLabel($orderNumber, $ex->getMessage());
            $shipment->updateOrderTransportNumber($iOrderId, $orderNumber, true);

            if (!empty($aLinkedOrders)) {
                foreach ($aLinkedOrders as $aLOrder) {
                    $this->updateOrderTransportNumber($aLOrder['id'], $orderNumber);
                }
            }
        }
    }
    return true;
  }

    public function insertLogoToPdf($filename,$websiteData,$transportId)
    {
        // Include the main TCPDF library and TCPDI.
        require_once('lib/tcpdf/tcpdf.php');
        require_once('lib/tcpdf/tcpdi.php');

        $symbol = $this->getTransportSymbol($transportId);

        //poczta polska nie lubi loga innych serwisow u siebie
        if ($symbol == 'poczta_polska' || $symbol == 'poczta-polska-doreczenie-pod-adres')
        {
            return false;
        }

        // Create new PDF document.
        $pdf = new \TCPDI(PDF_PAGE_ORIENTATION, PDF_UNIT, ['100','159'], true, 'UTF-8', false);
        $pdf->SetAutoPageBreak(TRUE, 0);
        $page_count = $pdf->setSourceFile($filename);

        for ($i=1 ; $i <= $page_count ; $i++)
        {
            // Add a page from a PDF by file path.
            if ($symbol == 'ruch') // paczka w ruchu
            {
                $pdf->AddPage('L');
                $pdf->StartTransform();
                $pdf->Rotate(90);
                $idx = $pdf->importPage($i,'/MediaBox');

                $pdf->Translate('-57','0');
                $pdf->useTemplate($idx,0,0,137,100,true);
                $pdf->StopTransform();
                $pdf->setPageOrientation('P');
            }
            else if ($symbol == 'poczta_polska' || $symbol == 'poczta-polska-doreczenie-pod-adres') //poczta polska
            {
                $pdf->AddPage('L');
                $pdf->StartTransform();
                $pdf->Rotate(270);
                $idx = $pdf->importPage($i,'/MediaBox');

                $pdf->Translate('-70','-80');
                $pdf->useTemplate($idx,0,0,159,100,true);
                $pdf->StopTransform();
                $pdf->setPageOrientation('P');
            }
            else
            {
                $pdf->AddPage();
                $idx = $pdf->importPage($i,'/MediaBox');
                if ($symbol == 'paczkomaty_24_7') // paczkomaty
                {
                    $pdf->useTemplate($idx,0,0,100,134,true);
                }
                else
                {  //fedex - opek-przesylka-kurierska
                    $pdf->useTemplate($idx,0,0,100,137,true);
                }
            }

            $pdf->SetY(134);
            $logoName = str_replace(".pl","",mb_strtolower($websiteData['paczkomaty_name']));

            //image path depends on from where script is run
            if(mb_strstr(getcwd(),"omniaCMS"))
            {
                $folder = '..';
            }
            else
            {
                $folder = '';
            }

            if (file_exists($_SERVER['DOCUMENT_ROOT'].'/images/loga/'.$logoName.'.jpg'))
            {
                $pdf->writeHTML('<img src="'.$folder.'/images/loga/'.$logoName.'.jpg">', true, false, false, false, '');
            }
        }
        return $pdf->Output($filename, 'F');
    }
  
  
  /**
   * Metoda tworzy adres dostawy dla poczty polskiej
   * 
   * @param array $aOrder
   * @param array $aAddresses
   * @return array
   */
  public function getDeliverAddress($aOrder, $aAddresses) {
    
//    $aInvoiceAddress = $aAddresses[0];
    $aDeliverAddress = $aAddresses[1];
    $aDeliverOrderAddresss = array(
        'name' => $aDeliverAddress['name'],
        'surname' => $aDeliverAddress['surname'],
        'phone' => $aDeliverAddress['phone'],
        'email' => $aOrder['email'],
        'is_company' => $aDeliverAddress['is_company'],
        'company' => $aDeliverAddress['company'],
        'street' => $aDeliverAddress['street'],
        'city' => $aDeliverAddress['city'],
        'postal' => $aDeliverAddress['postal'],
        'number' => $aDeliverAddress['number'],
        'number2' => $aDeliverAddress['number2'],
    );
    return $aDeliverOrderAddresss;
  }// end of getPocztaPolskaDeliverAddress() method
  
  
  /**
   * Metoda wylicza ile paczek FedEx powinno zostać wysłanych, uwzględnia zamówienia wysyłane razem
   * 
   * @param int $iId
   * @param string $sBookstore
   * @param int $iTransportId
   * @param array $aLinkedOrders
   * return int
   */
  public function getSuggestedCountPackages($iId, $sBookstore, $iTransportId, $aLinkedOrders = array()) {
    $fWeight = 0;
    

    $fWeight = $this->getAllOrderWeight($iId, $sBookstore, $aLinkedOrders);
    $fMaxTMWeight = $this->getTransportMaxWeight($iTransportId);
    
    if($fMaxTMWeight > 0 && $fWeight > $fMaxTMWeight){
      $iAmount = ceil($fWeight / $fMaxTMWeight);
    } else {
      $iAmount = 1;
    }
    return $iAmount;
  }// end of getSuggestedCountPackages() method
  
  
	private function getTransportMaxWeight($iOrderId){
    
		$sSql = "SELECT max_weight 
							FROM orders_transport_means
							WHERE id = ".$iOrderId;
		return $this->pDbMgr->GetOne('profit24', $sSql);
	}

    private function getTransportSymbol($iOrderId){

        $sSql = "SELECT symbol
							FROM orders_transport_means
							WHERE id = ".$iOrderId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }
  
  
  /**
   * Metoda pobiera całkowitą wagę pojedynczego zamowienia
   * 
   * @param int $iId
   * @param string $sBookstore
   * @param array $aLinkedOrders
   * @return float
   */
  private  function getAllOrderWeight($iId, $sBookstore, $aLinkedOrders = array()) { 
    
    // sprawdźmy czy są jakieś zamówienia wysyłane razem
    if (!empty($aLinkedOrders)) {
      foreach ($aLinkedOrders as $aLOrder) {
        $fWeight += floatval(str_replace(',', '.', $this->getOrderWeight($aLOrder['id'], $sBookstore)));
      }
    }
    
    $fWeight += floatval(str_replace(',', '.', $this->getOrderWeight($iId, $sBookstore)));
    return $fWeight;
  }// end of getAllOrderWeight() method
  
  
  /**
   * Dodajemy dokument do kolejki drukowania 
   * 
   * @global \DatabaseManager $pDbMgr
   * @param string $sFilePath
   * @param string $sPrinter
   * @return boolean
   */
  public function addToPrintQueue($sFilePath, $sPrinter) {
    global $pDbMgr;
    
    $aValues = array(
        'filetype' => 'PDF',
        'filepath' => $sFilePath,
        'dest_printer' => $sPrinter,
        'created' => 'NOW()'
    );
    if ($pDbMgr->Insert('profit24', 'print_queue', $aValues) === false) {
      return false;
    }
    return true;
  }// end of addToPrintQueue() method
  
  
  /**
   * Metoda pobiera ścieżkę do etykiety listu przewozowego
   * 
   * @global array $aConfig
   * @param string $sTransportNumber
   * @return string
   */
  public function getPaczkomatyTransportListFilename($sTransportNumber) {
    global $aConfig;
    
    $sFilename = 'Paczka_'.$sTransportNumber.'.pdf';
    return $aConfig['common']['client_base_path'].'/'.$aConfig['common']['cms_dir'].$aConfig['common']['paczkomaty_dir'].$sFilename;
  }// end of getPaczkomatyTransportListFilename() method
  
  
  /**
   * Metoda pobiera ścieżkę do etykiety listu przewozowego
   * 
   * @global array $aConfig
   * @param type $sTransportNumber
   * @return type
   */
  public function getFedExTransportListFilename($sTransportNumber) {
    global $aConfig;
    
    $sFilename = 'etykiete_'.$sTransportNumber.'.pdf';
    return $aConfig['common']['client_base_path'].'/'.$aConfig['common']['cms_dir'].$aConfig['common']['fedex_dir'].$sFilename;
  }// end of getFedExTransportListFilename() method
  
  
  /**
   * Metoda zapisuje numer listu przewozowego w zamówieniach, uwzględnia wysyłkę razem,
   * wykorzystywane przez Paczkomaty i FedEx
   * 
   * @param \orders\Shipment $oTransportClass
   * @param string $sTransportNumber
   * @param int $iMainOrderId
   * @param array $aLinkedOrders
   * @return boolean
   */
  private function saveTransportNumberInOrdersByiShipments(&$oTransportClass, $sTransportNumber, $iMainOrderId, $aLinkedOrders, $bPrint , $aSellerData = false) {
    global $aConfig;
    
    // zapisujemy w głównym
    try {
        $sFilePath = $oTransportClass->getTransportListFilename($aConfig, $sTransportNumber);
        $this->updateOrderTransportNumber($iMainOrderId, $sTransportNumber);
      if (!file_exists($sFilePath))
      {
          if ($oTransportClass->getAddressLabel($sFilePath, $iMainOrderId) === false) {
            return false;
          }

          $this->insertLogoToPdf($sFilePath,$aSellerData,$oTransportClass->iTransportId);
      }
      if ($bPrint === true) {
        $oTransportClass->doPrintAddressLabel($sFilePath, $_COOKIE['printer_labels']);
      }
      if (!empty($aLinkedOrders)) {
        foreach ($aLinkedOrders as $aLOrder) {
          $this->updateOrderTransportNumber($aLOrder['id'], $sTransportNumber);
        }
      }
      return true;
    } catch (\Exception $ex) {
      throw new \Exception($ex);
    }
    return false;
  }// end of saveTransportNumberInOrdersByiShipments() method
  
  
  /**
   * Metoda ustawia numer listu przewozowego w zamóweniu
   * 
   * @global \DatabaseManager $pDbMgr
   * @param \communicator\sources\PocztaPolska\type $iOrderId
   * @param \communicator\sources\PocztaPolska\type $sTransportNumber
   */
  public function updateOrderTransportNumber($iOrderId, $sTransportNumber) {
    global $pDbMgr;
    
    $aValues = array(
        'transport_number' => $sTransportNumber,
        'tmp_transport_number' => '0'
    );

    if ($pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOrderId) === false) {
      throw new \Exception(_('Wystąpił błąd podczas zapisywania numeru listu przewozowego'));
    }
    return true;
  }// end of _updateOrderTransportNumber() method
  
  
  /**
   * Metoda zapisuje numer listu przewozowego w zamówieniach, uwzględnia wysyłkę razem,
   * wykorzystywane przez Paczkomaty i FedEx
   * 
   * @param \object $oTransportClass
   * @param string $sTransportNumber
   * @param int $iMainOrderId
   * @param array $aLinkedOrders
   * @return boolean
   */
  private function saveTransportNumberInOrders(&$oTransportClass, $sTransportNumber, $iMainOrderId, $aLinkedOrders, $bPrint) {

    // zapisujemy w głównym
    if ($oTransportClass->doZapiszList($sTransportNumber, $iMainOrderId, $bPrint) === false) {
      return false;
    }
    if (!empty($aLinkedOrders)) {
      foreach ($aLinkedOrders as $aLOrder) {
        if ($oTransportClass->doZapiszList($sTransportNumber, $aLOrder['id'], false) === false) {
          return false;
        }
//        if ($this->setOrderConfirmed($aLOrder, $aLOrder['id']) === false) {
//          return false;
//        }
      }
    }
    return true;
  }// end of saveTransportNumberInOrders() method
  
  
	/**
	 * Metoda pobiera dane sprzedawcy
	 *
	 * @return	array ref	- tablica z danymi
	 */
	private function getPaymentOrderDetailsA($sWebsite = 'profit24') {

		$sSql = "SELECT OSD.*, WS.email
						 FROM orders_seller_data AS OSD, website_settings AS WS";
		return  $this->pDbMgr->GetRow($sWebsite, $sSql);
	} // end of getSellerData() method
  
  
	/**
	 * Pobiera dane adresowe z zamówienai z bazy
	 * 
	 * @param int $iId - id adresu
	 * @return array ref
	 */
	function &getOrderAddresses($iId, $bIgnoreTransportAdress = false){
		global $aConfig;

		$sSql = "SELECT address_type, is_company, company, name, surname, street, number, number2, postal, city, nip, phone, is_postal_mismatch
						 FROM ".$aConfig['tabls']['prefix']."orders_users_addresses 
						 WHERE order_id = ".$iId.
            ($bIgnoreTransportAdress === TRUE ? ' AND address_type="0"' : '').
						 " ORDER BY address_type"; 
		return $this->pDbMgr->GetAssoc('profit24', $sSql);
	} // end of getOrderAddresses() method
  
  
	/**
	 * Oblicza wagę zamówienia
   * 
	 * @param int $iOrderId - id zamowienia
	 * @param string $sBookstore - kod księgarni
	 * @return float - waga
	 */
	private function getOrderWeight($iOrderId,$sBookstore){
		
		// waga opakowania 
		$sSql = "SELECT weight
						 FROM orders_package_weight
						 WHERE bookstore = '".$sBookstore."'";
		$fPackageWeight = (double)$this->pDbMgr->GetOne('profit24', $sSql);
    
		//zliczenie wagi produktów
		$sSql = "SELECT SUM(weight*quantity)
				FROM orders_items
				WHERE order_id = ".$iOrderId."
						AND item_type='I'
						 AND deleted = '0'";
		$fBooksWeight = $this->pDbMgr->GetOne('profit24', $sSql);
		return \Common::formatPrice($fBooksWeight+$fPackageWeight);
	} // end of getOrderWeight() method
  
  
  /**
   * Metoda pobiera powiązane zamówienia z podanym
   * 
   * @global \DatabaseManager $pDbMgr
   * @param int $iOrderItemId
   * @return array
   */
//  public function getOrdersLinked($iOrderItemId) {
//    global $pDbMgr;
//    
//    $sSql = 'SELECT O.*
//             FROM orders_linked_transport AS OLT
//             JOIN orders AS O
//              ON O.id = OLT.linked_order_id
//             WHERE OLT.main_order_id = '.$iOrderItemId;
//    return $pDbMgr->GetAll('profit24', $sSql);
//  }// end of getOrdersLinked() method
  
  
  /**
   * Metoda miesza zamówienia i zwraca dane zamówienia "połączonego"
   * 
   * @TODO to należy dobrze przetestować, czy sumowanie jest właściwe
   * 
   * @param array $aOrder
   * @param float $fOrderWeight
   * @param array $aLinkedOrders
   * @return array
   */
  private function margeLinkedOrdersRef(&$aOrder, &$fOrderWeight, $aLinkedOrders) {
    $bPostalFee = false;
    // mieszamy zamówienia
    foreach ($aLinkedOrders as $aLOrder) {
      // ok
      if ($aLOrder['payment_type'] == 'postal_fee' || $aLOrder['second_payment_type'] == 'postal_fee') {
        $bPostalFee = true;
        // Sumujemy parametry wartosci zamówienia
        $aOrder['to_pay'] += $aLOrder['to_pay'];
        $aOrder['paid_amount'] += $aLOrder['paid_amount'];
        $aOrder['balance'] += $aLOrder['balance'];
      }
      $fOrderWeight += floatval(str_replace(',', '.', $this->getOrderWeight($aLOrder['id'], $aOrder['bookstore'])));
      $aOrder['orders_numbers'] .= ','.$aLOrder['order_number'];
    }
    $aOrder['orders_numbers'] .= ','.$aOrder['order_number'];
    $aOrder['orders_numbers'] = substr($aOrder['orders_numbers'], 1, strlen($aOrder['orders_numbers']));
    // jeśli jakakolwiek metoda płatności podzamówień to płatność przy odbiorze to całe zamówienie ma być płacone przy odbiorze.
    if ($bPostalFee === true) {
      $aOrder['payment_type'] = 'postal_fee';
    }
    return true;
  }// end of margeLinkedOrdersRef() method
  
  
  /**
   * Pobieramy numery listów przewozowych
   * 
   * @param array $aOrder
   * @param array $aLinkedOrders
   * @param bool $bIsAutoGetTransport
   * @throws \Exception
   */
  public function getSimpleTransportAddressLabel($aOrder, $aLinkedOrders, $bIsAutoGetTransport, $ommitInternalLabel = false) {
    
    try {
      switch ($aOrder['transport_id']) {
        case '1':
        case '7':
          // FedEx, TBA
          $iAmountOfPackages = $this->getSuggestedCountPackages($aOrder['id'], 'profit24', $aOrder['transport_id'], $aLinkedOrders);
          if ($iAmountOfPackages == 1 || $aOrder['transport_packages'] > 0) {
            if ($aOrder['transport_packages'] > 0) {
              $iAmountOfPackages = $aOrder['transport_packages'];
            }
            $this->setTransportPackages($aOrder['id'], $iAmountOfPackages, $aLinkedOrders);
            $aOrder['transport_packages'] = $iAmountOfPackages;
            $this->proceedAddressLabel($aOrder['transport_id'], $aOrder['id'], $aOrder, $aLinkedOrders, false, $ommitInternalLabel);
          }
        break;
      
        case '3':
          // paczkomaty
          $iAmountOfPackages = $this->getSuggestedCountPackages($aOrder['id'], 'profit24', $aOrder['transport_id'], $aLinkedOrders);
          if ($iAmountOfPackages >= 2) {
            $iAmountOfPackages = 2;
            // TODO To z DB powinno być pobrane
            $sCharAmount = 'B';
          } else {
            $iAmountOfPackages = 1;
            $sCharAmount = 'A';
          }
          $this->setPaczkomatyPackages($aOrder['id'], $iAmountOfPackages, $sCharAmount, $aOrder['point_of_receipt'], $aLinkedOrders);
          $aOrder['transport_option_id'] = $iAmountOfPackages;
          $aOrder['transport_option_symbol'] = $sCharAmount;
          $this->proceedAddressLabel($aOrder['transport_id'], $aOrder['id'], $aOrder, $aLinkedOrders, false, $ommitInternalLabel);
        break;
        
        case '6':
        case '5':
          // paczka w ruchu, orlen
          $this->proceedAddressLabel($aOrder['transport_id'], $aOrder['id'], $aOrder, $aLinkedOrders, false, $ommitInternalLabel);
        break;
        
        // poczta polska, jesli jest to automatyczne pobranie etykiety to nie pobieramy etykiety z poczty polskiej
        case '4':
        case '8':
          if ($bIsAutoGetTransport === false) {
            $this->proceedAddressLabel($aOrder['transport_id'], $aOrder['id'], $aOrder, $aLinkedOrders, false, $ommitInternalLabel);
          }
        break;
      
        default :
        return true;
      }
      
    } catch (\Exception $ex) {
      throw new \Exception($ex->getMessage());
    }
  }// end of getTransport() method
  
  
	/**
	 * Metoda pobiera symbol serwisu na podstawie id serwisu
	 *
	 * @param type $iWebsiteId
	 * @return type 
	 */
	private function getWebsiteSymbol($iWebsiteId) {
		
		$sSql = "SELECT code
             FROM websites
						 WHERE id=".$iWebsiteId;
		return $this->pDbMgr->GetOne('profit24', $sSql);
	}// end of getWebsiteSymbol() method
  
  
	/**
	 * Metoda pobiera podstawowy email serwisu
	 *
	 * @param string $sWebsite
	 * @return string
	 */
	private function getWebsiteEmail($sWebsite) {
		
		$sSql = "SELECT email 
              FROM website_settings 
              LIMIT 1";
		return $this->pDbMgr->GetOne($sWebsite, $sSql);
	}// end of getWebsiteEmail() method
  
  
  /**
   * Metoda zapisuje ilość paczek FedEx
   * 
   * @param int $iOrderId
   * @param int $iAmount
   * @param array $aLinkedOrders
   * @return boolean
   */
  public function setTransportPackages($iOrderId, $iAmount, $aLinkedOrders) {
    
    $aValuess = array(
        'transport_packages' => $iAmount
    );
    
    if ($this->pDbMgr->Update('profit24', "orders",
                   $aValuess,
                   "id = ".$iOrderId) === false) {
      return false;
    }
    if (!empty($aLinkedOrders)) {
      foreach ($aLinkedOrders as $aLinkedOrder) {
        if ($this->pDbMgr->Update('profit24', "orders",
                        $aValuess,
                        "id = ".$aLinkedOrder['id']) === false) {
           return false;
         }
      }
    }
    return true;
  }// end of setFedExPackages() method
  
  
  /**
   * Metoda ustawia wariant przesyłki paczkomaty
   * 
   * @param int $iOrderId
   * @param int $iTransportOptionId
   * @param string $sTransportOptionSymbol
   * @param string $sPointRecepit
   * @param array $aLinkedOrders
   * @return boolean
   */
  public function setPaczkomatyPackages($iOrderId, $iTransportOptionId, $sTransportOptionSymbol, $sPointRecepit = '', $aLinkedOrders = array()) {
    
    $aValuess = array();
    if ($sPointRecepit != '') {
      $aValuess['point_of_receipt'] = $sPointRecepit;
    }
    $aValuess['transport_option_id'] = $iTransportOptionId;
    $aValuess['transport_option_symbol'] = $sTransportOptionSymbol;
    if ($this->pDbMgr->Update('profit24', 
                   "orders",
                   $aValuess,
                   "id = ".$iOrderId) === false) {
      return false;
    }
    if (!empty($aLinkedOrders)) {
      foreach ($aLinkedOrders as $aLinkedOrder) {
        if ($this->pDbMgr->Update('profit24', 
                   "orders",
                   $aValuess,
                   "id = ".$aLinkedOrder['id']) === false) {
          return false;
        }
      }
    }
    return true;
  }// end of setPaczkomatyPackages() method
  
  
  /**
   * Metoda pobiera powiązane zamówienia z podanym
   * 
   * @global \DatabaseManager $pDbMgr
   * @param int $iOrderItemId
   * @return array
   */
  public function getOrdersLinked($iOrderItemId) {
    global $pDbMgr;
    
    $sSql = 'SELECT O.*
             FROM orders_linked_transport AS OLT
             JOIN orders AS O
              ON O.id = OLT.linked_order_id
             WHERE OLT.main_order_id = '.$iOrderItemId;
    return $pDbMgr->GetAll('profit24', $sSql);
  }// end of getOrdersLinked() method
  
  
  /**
   * Metoda pobiera unikatowe zamówienia, 
   *  jeśli zamówienie ma zostać wysłane z innym to w tablicy występuje tylko zamówienie główne,
   *  pozostałe umieszczane są w tablicy tego rekordu ['linked_orders']
   * 
   * @param array $aAllOrders
   * @return array
   */
  public function uniqueLinkedOrders($aAllOrders) {
    $aOrders = array();
    $aItemLinkedIds = array();
    
    foreach ($aAllOrders as $iKey => $aOrderItem) {
      if (!in_array($aOrderItem['id'], $aItemLinkedIds)) {
        $aOrderLinked = $this->getOrdersLinked($aOrderItem['id']);
        
        foreach ($aOrderLinked as $aOrderL) {
          $aItemLinkedIds[] = $aOrderL['id'];
        }
        $aOrders[$iKey] = $aOrderItem;
        $aOrders[$iKey]['linked_orders'] = $aOrderLinked;
      }
    }
    return $aOrders;
  }// end of uniqueLinkedOrders() method
  
  
  /**
   * Metoda pobiera dane sprzedającego na podstwaie id serwisu
   * 
   * @param int $iWebsiteId
   * @return array
   */
  public function getSellerDataByWebsiteId($iWebsiteId) {
    
    $sWebsiteSymbol = $this->getWebsiteSymbol($iWebsiteId);
    return $this->getPaymentOrderDetailsA($sWebsiteSymbol);
  }// end of getSellerDataByWebsiteId() method
}
