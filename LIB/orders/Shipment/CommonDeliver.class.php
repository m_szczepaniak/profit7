<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-17 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders\Shipment;
use LIB\Assets\deliverPDF;

class CommonDeliver {

  private $bTestMode;
  private $pDbMgr;
  private $aTransportSettings;

  function __construct($aTransportSettings, $pDbMgr, $bTestMode) {
    $this->aTransportSettings = $aTransportSettings;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }

    /**
     * @param $aTransportNumbers
     * @param array $aOrdersIds
     * @return string
     * @throws \Exception
     */
  public function getOwnDeliver($aTransportNumbers, $aOrdersIds = array()) {
      $sSql = 'SELECT name FROM orders_transport_means WHERE id = '.$this->aTransportSettings['transport_id'];
      $transportName = $this->pDbMgr->GetOne('profit24', $sSql);

      $deliverPDF = new deliverPDF();
      $sPDF = $deliverPDF->getPDFDeliver($aTransportNumbers, $transportName);

      if ($sPDF != '' && is_array($sPDF) === false) {
          $sDeliverFilename = $this->saveDeliverGetFileNamePathTRANSACTION($sPDF, $aOrdersIds);
      } else {
          throw new \Exception(_('Wystąpił błąd podczas pobierania delivera z '.$transportName));
      }
      return $sDeliverFilename;
  }

  /**
   * @return string
   */
  private function _getDeliverFileName($iOrderId) {
    
    $sSql = 'SELECT filename 
             FROM orders_shipment_confirm 
             WHERE order_id = '.$iOrderId;
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
  
  
  /**
   * @global array $aConfig
   * @param int $iOrderId
   * @return string
   */
  public function getOrderDeliverFilePath($iOrderId) {
    global $aConfig;
    $sFilename = $this->_getDeliverFileName($iOrderId);
    if ($sFilename != '') {
      return $aConfig['common']['base_path'].'/'.$aConfig['common']['confirm_dir'].$sFilename;
    } else {
      return false;
    }
  }
  

  /**
   * 
   * @global array $aConfig
   * @return string
   */
  public function getDeliverFilename() {
    global $aConfig;
    
    $sDateFilename = date('YmdHis');
    $sFilename = 'Ksiazka_nadawcza_'.$sDateFilename.'.pdf';
    return $aConfig['common']['base_path'].'/'.$aConfig['common']['confirm_dir'].$sFilename;
  }// end of getDeliverFilename() method
  
  
  /**
   * 
   * @param string $sPDF
   * @param array $aOrdersIds
   * @return string filename
   * @throws Exception
   */
  protected function saveDeliverGetFileNamePathTRANSACTION($sPDF, $aOrdersIds) {
    
    $sDeliverFilename = $this->getDeliverFilename();
    if (file_put_contents($sDeliverFilename, $sPDF) === false) {
      throw new \Exception(_('Wystąpił błąd podczas zapisywania delivera z '.$this->aTransportSettings['name']));
    }

    $this->pDbMgr->BeginTransaction('profit24');
    foreach ($aOrdersIds as $iOrderId) {
      if ($this->insertDeliverFile($iOrderId, $sDeliverFilename) === false) {
        $this->pDbMgr->RollbackTransaction('profit24');
        throw new \Exception('Wystąpił błąd podczas dodawania do bazy pliku delivera');
      }
      if ($this->updateConfirmPrintout($iOrderId) === false) {
        $this->pDbMgr->RollbackTransaction('profit24');
        throw new \Exception('Wystąpił błąd podczas zmiany flagi delivera w zamówieniu');
      }
    }
    $this->pDbMgr->CommitTransaction('profit24');
    return $sDeliverFilename;
  }
  
  
  /**
   * Aktualizujemy flage drukowania potwierdzenia nadania
   * 
   * @param int $iOrderId
   * @return bool
   */
  private function updateConfirmPrintout($iOrderId) {
    
    $aVal = array('confirm_printout_speditor' => '1');
    return $this->pDbMgr->Update('profit24', 'orders', $aVal, ' id = '.$iOrderId. ' LIMIT 1');
  }
  
  
  /**
   * 
   * @param int $iOrderId
   * @param string $sFilePathName
   * @return boolean
   */
  private function insertDeliverFile($iOrderId, $sFilePathName) {
    
    $aValue = array(
        'order_id' => $iOrderId,
        'filename' => $this->getFileNameByFilePath($sFilePathName)
    );
    if ($this->pDbMgr->Insert('profit24', 'orders_shipment_confirm', $aValue, '', false) === false) {
      return false;
    }
  }
  
  /**
   * Pobieramy nazwę pliku ze ścieżki
   * 
   * @param string $sFilePathName
   * @return string
   */
  private function getFileNameByFilePath($sFilePathName) {
    
    $aData = explode(DIRECTORY_SEPARATOR, $sFilePathName);
    return array_pop($aData);
  }
}
