<?php

namespace orders\Shipment;


class ShipmentTime
{
    public function getShipmentTime($productId, $shipmentTime) {
        switch ($shipmentTime) {
            case 0:
                $shipmentTimeName = "24h";
                break;
            case 1:
                $shipmentTimeName = "48h";
                break;
            case 2:
                $shipmentTimeName = "3 dni";
                break;
            case 3:
                $shipmentTimeName = "4 dni";
                break;
            case 4:
                $shipmentTimeName = "5 dni";
                break;
            case 5:
                $shipmentTimeName = "6 dni";
                break;
            case 6:
                $shipmentTimeName = "7 dni";
                break;
            case 7:
                $shipmentTimeName = "3-6 dni";
                break;
            case 8:
                $shipmentTimeName = "6-8 dni";
                break;
            case 9:
                $shipmentTimeName = "7-10 dni";
                break;
            case 10:
                $shipmentTimeName = "10-14 dni";
                break;
            default:
                $shipmentTimeName = "48h";
        }

        return $shipmentTimeName;
    }
}