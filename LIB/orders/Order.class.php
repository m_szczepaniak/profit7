<?php
/**
 * Klasa zarządzająca logiką biznesową zamówienia
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-21 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class Order {
  private $oOrderData;
  private $oOrderChange;

  function __construct($iOId) {
    
    include_once('OrderData.class.php');
    include_once('OrderChange.class.php');
    
    $this->oOrderData = new OrderData($iOId);
    $this->oOrderChange = new OrderChange($iOId);
  }
  
  
  
  public function getOrdersItemsSendToSource() {
    $this->oOrderData->getOrdersItemsSendToSource(array('id'));
  }

}

?>
