<?php
/**
 * Klasa obsługi zamówień do dostawcy 
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-02-25 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders\magazine\providers;
class ordersToProviders {
  
  /**
   *
   * @var \DatabaseManager $pDbMgr
   */
  public $pDbMgr;
  
  function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }


  /**
   * Metoda dodaje nowe zamówienie do dostawcy
   * 
   * @param string $sOrderNumber
   * @param int $iProviderId
   * @param bool $bInternal
   * @param array $aBooks
   * @param string $sCreatedBy
   * @param string $sExOrderNumber
   * @throws \Exception
   * @return mixed
   */
  public function InsertOrderToProvider($sOrderNumber, $iProviderId, $bInternal, $aBooks, $sCreatedBy, $sExOrderNumber = 'NULL') {
    
    $iOrderToProviderId = $this->_insertOrderToProvider($sOrderNumber, $iProviderId, $bInternal, $sCreatedBy, $sExOrderNumber);
    if ($iOrderToProviderId <= 0) {
      throw new \Exception(_('Wystąpił błąd podczas dodawania zamówienia do dostawcy'));
    } else {
      // dodano zam i mamy id,
      // dodajemy składowe zamówienia
      if ($this->_insertOrderToProviderItems($iOrderToProviderId, $aBooks) === FALSE) {
        throw new \Exception(_('Wystąpił błąd podczas dodawania składowych zamówienia do dostawcy')); 
      } else {
        return $iOrderToProviderId;
      }
    }
    return FALSE;
  }// end of InsertOrderToProvider() method
  
  
  /**
   * Metoda dodaje do tabeli dokumentów odnośnie zamówień do dostawócw rekord
   * 
   * @param int $iOrderToProvider
   * @param int $iType '0' - wygenerowany dokument zamówienia, '1' - FAKTURA, '2' - Raport Realizacji
   * @param string $sNewOrderNumber
   * @param string $sFilename
   * @param string $sAddNumber
   * @return bool|int
   */
  public function InsertOrderToProviderDocument($iOrderToProvider, $iType, $sNewOrderNumber, $sFilename, $sAddNumber = 'NULL') {
    
    // sciezka relatywna
    $sFilename = str_replace($_SERVER['DOCUMENT_ROOT'], '', $sFilename);
    $aValues = array(
        'orders_to_providers_id' => $iOrderToProvider,
        'type' => $iType,
        'number' => $sNewOrderNumber,
        'add_number' => $sAddNumber,
        'filename' => $sFilename,
        'created' => 'NOW()'
    );
    return $this->pDbMgr->Insert('magazine', 'orders_to_providers_documents', $aValues);
  }// end of InsertOrderToProviderDocument() method
  
  
  /**
   * Metoda wybiera dane składowej zamówienia
   * 
   * @param int $iId
   * @param array $aCols
   * @return array
   */
  public function getOrdersToProvidersToOrder($iId, $aCols) {
    
    $sSql = 'SELECT '.implode(',', $aCols).'
             FROM orders_to_providers_to_order
             WHERE id = '.$iId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of getOrdersToProvidersToOrder() method
  
  
  /**
   * Metoda wybiera dane składowej zamówienia
   * 
   * @param int $iItemOrderId
   * @param array $aCols
   * @return array
   */
  public function getOrdersToProvidersByOrderItemId($iItemOrderId, $aCols) {
    
    $sSql = 'SELECT '.implode(',', $aCols).'
             FROM orders_to_providers_to_order
             WHERE orders_items_id = '.$iItemOrderId.'
               AND status <> "99"';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getOrdersToProvidersByOrderItemId() method
  
  
  /**
   * Metoda sprawdza, czy podzielić zmaówienie do dostawcy
   * 
   * @param int $iItemOrderId
   * @param int $iOrderId
   * @return bool
   */
  public function checkDivideOrderItem($iItemOrderId, $iOrderId) {
    
    $sSql = 'SELECT id, status
             FROM orders_to_providers_to_order
             WHERE orders_items_id = '.$iItemOrderId;
    $aRow = $this->pDbMgr->GetOne('profit24', $sSql);
    if (!empty($aRow)) {
      if (intval($aRow['status']) <= 10) {
        return true;
      } else {
        return  -2;
      }
    } else {
      return FALSE;
    }
  }// end of checkDivideOrderItem() method
  
  
  /**
   * Metoda dodaje zlecenie zamówienia do dostawcy
   * 
   * @param int $iQuantity
   * @param int $iOrderItemId
   * @param int $iOrderId
   * @param char $cStatus - statusy takie jak w orders_items, jeśli podział zamówienia do status = '0'
   * @param int $iProviderId - jeśli podział zamówienia, nie znamy jeszcze źródła wiec tu będzie NULL
   * @param int $iOrderToProviderItemId
   * @return int
   */
  public function InsertOrderToProviderToOrder($iQuantity, $iOrderItemId, $iOrderId, $cStatus, $iProviderId = 'NULL', $iOrderToProviderItemId = 'NULL') {
    
    $aValues = array(
        'quantity' => $iQuantity,
        'orders_items_id' => $iOrderItemId,
        'orders_id' => $iOrderId,
        'status' => $cStatus, // statusy takie jak w orders_items, jeśli podział zamówienia do status = '0'
        'provider_id' => $iProviderId, // jeśli podział zamówienia, nie znamy jeszcze źródła wiec tu będzie NULL
        'orders_to_providers_items_id' => $iOrderToProviderItemId
    );
    //$this->pDbMgr->Insert('magazine', 'orders_to_providers_to_order', $aValues);
    return $this->pDbMgr->Insert('profit24', 'orders_to_providers_to_order', $aValues);
  }// end of InsertOrderToProviderToOrder() method
  
  
  /**
   * Metoda anuluje zamówienie do dostawcy
   * 
   * @param int $iOrderItemId
   * @return bool
   */
  public function CancelOrderToProviderToOrderByOrderItemId($iOrderItemId) {
    
    $aValues = array(
        'status' => '99', // ANULOWANY - 99
    );
    //$this->pDbMgr->Update('magazine', 'orders_to_providers_to_order', $aValues, array('orders_items_id' => $iOrderItemId))
    return $this->pDbMgr->Update('profit24', 'orders_to_providers_to_order', $aValues, array('orders_items_id' => $iOrderItemId));
  }// end of CancelOrderToProviderToOrderByOrderItemId() method
  
  
  /**
   * Metoda anuluje zamówienie do dostawcy
   * 
   * @param int $iId
   * @return bool
   */
  public function changeStatusOrderToProviderToOrder($iId, $cStatus) {
    
    $aValues = array(
        'status' => $cStatus //'99', // ANULOWANY - 99
    );
    //$this->pDbMgr->Update('magazine', 'orders_to_providers_to_order', $aValues, array('id' => $iId));
    return $this->pDbMgr->Update('profit24', 'orders_to_providers_to_order', $aValues, array('id' => $iId));
  }// end of CancelOrderToProviderToOrderByOrderItemId() method
  
  
  /**
   * Metoda zmienia zamówioną ilość 
   * 
   * @param int $iOrderItemId
   * @return bool
   */
  public function changeOrderToProviderQuantity($iId, $iQuantity) {
    
    $aValues = array(
        'quantity' => $iQuantity 
    );
    //$this->pDbMgr->Update('magazine', 'orders_to_providers_to_order', $aValues, array('id' => $iId));
    return $this->pDbMgr->Update('profit24', 'orders_to_providers_to_order', $aValues, array('id' => $iId));
  }// end of changeOrderToProviderQuantity() method
  
  
  /**
   * Metoda zmienia dostawcę do którego należy złożyć zamówienie na pozycje
   * 
   * @param int $iId
   * @param int $iProviderId
   * @return bool
   */
  public function changeOrderToProviderToOrderProvider($iId, $iProviderId) {
    
    $aValues = array(
        'provider_id' => $iProviderId
    );
    $this->pDbMgr->Update('magazine', 'orders_to_providers', $aValues, array('id' => $iId));
  }// end of changeOrderToProviderToOrderProvider() method
  
  
  /**
   * Metoda usuwa zlecenie zamówienia do dostwcy
   * 
   * @param int $iOrderItemId
   * @return bool
   */
  public function DeleteOrderToProviderToOrderByOrderItemId($iOrderItemId) {
    
    $sSql = 'DELETE FROM orders_to_providers_to_order 
             WHERE orders_items_id = ' . $iOrderItemId;
    //$this->pDbMgr->Query('magazine', $sSql);
    return $this->pDbMgr->Query('profit24', $sSql);
  }// end of DeleteOrderToProviderToOrderByOrderItemId() method
  
  
  /**
   * Sprawdźmy czy zlecenie zamówienia do dostawcy, powinno zostać utworzone
   * 
   * @param array $aNewData
   * @param array $aOldData
   * @return bool
   */
  public function checkInsertOrdersToProvidersToOrder($aNewData, $aOldData) {
    
    if ($aOldData['deleted'] == '0' && 
        isset($aNewData['source']) && // ustawione źródło
        $aOldData['status'] <> $aNewData['status'] && // zmieniono status
        $aNewData['status'] == '10' ) { // status zmieniono na do zamówienia
      return true;
    }
    return false;
  }// end of checkInsertOrdersToProvidersToOrder() method
  
  
  /**
   * Sprawdźmy czy zlecenie zamówienia do dostawcy, powinno zostać anulowane
   * 
   * @param array $aNewData
   * @param array $aOldData
   * @return bool
   */
  public function checkCancelOrdersToProvidersToOrder($aNewData, $aOldData) {
    
    if ($aOldData['deleted'] == '0' && 
        isset($aNewData['provider_id']) && // ustawione źródło
        $aOldData['status'] <> $aNewData['status'] && // zmieniono status
        $aNewData['status'] == '0' || $aNewData['status'] == '-1' ) { // status zmieniono na '---' lub zapo
      return true;
    }
    return false;
  }// end of checkCancelOrdersToProvidersToOrder() method
  
  
  /**
   * Metoda sprawdza, czy składowa zamówienia może zostać dodana
   * 
   * @param array $aOldData
   * @param array $aPostMainData
   * @return bool
   */
  public function checkAddToOrdersToProvidersToOrder($aOldData, $aPostMainData) {
    
    if ($aOldData['status'] <> $aPostMainData['status'] &&
            $aPostMainData['status'] == '10' &&
            intval($aPostMainData['quantity']) > 0
            ) {
      return TRUE;
    } else {
      return FALSE;
    }
  }// end of checkAddToOrdersToProviders() method
  
  
  /**
   * Metoda sprawdza, czy może zostać zmieniony status zamówienia
   * 
   * @param char $cNowStatus
   * @param int $iNowQuantity
   * @param int $iOldQuantity
   * @throws \Exception
   * @return bool
   */
  public function checkChangeQuantity($cNowStatus, $iNowQuantity, $iOldQuantity) {
    
    if (intval($cNowStatus) <= 20) {
      return TRUE;
    } elseif (intval($iNowQuantity) <> intval($iOldQuantity)) {
      throw new \Exception(_('Nie można zmieniać ilości składowej zamówienia do dostawcy jeśli jest w statusie późniejszym niż "Rezerwacja"'));
    } else {
      return FALSE;
    }
  }// end of checkChangeQuantity() method
  
  
  /**
   * Metoda sprawdza, czy możemy zmienić dostawcę
   * 
   * @param char $cStatus
   * @param int $iNewProviderId
   * @param int $iOldProviderId
   * @throws \Exception
   * @return bool
   */
  public function checkSetProvider($cStatus, $iNewProviderId, $iOldProviderId) {
    if ($cStatus == '10' &&
        $iNewProviderId <> $iOldProviderId) {
      return true;
    } elseif ($iNewProviderId <> $iOldProviderId) {
      throw new \Exception(_('Nie można zmieniać dostawcy jeśli nie jest w statusie "Do rezerwacji"'));
    } else {
      return FALSE;
    }
  }// end of checkSetProvider
  
  
  /**
   * Metoda sprawdza, czy składowa zamówienia może zostać cofnięta do Statusu - Nowe
   * 
   * @param char $cNewSatus
   * @param char $cOldStatus
   * @return boolean
   */
  public function checkStatusGoBack($cNewSatus, $cOldStatus) {
    
    if ($cNewSatus == '0' && $cOldStatus == '10') {
      return TRUE;
    } else {
      return FALSE;
    }
  }// end of checkGoBack() method
  
  
  /**
   * Metoda sprawdza czy anulować zamówienia do dostawcy
   * 
   * @param char $cNewStatus
   * @param char $cOldStatus
   * @param int $iNewQuantity
   * @param int $iOldQuantity
   * @return boolean
   */
  public function checkCancelOrderToProviderToOrder($cNewStatus, $cOldStatus, $iNewQuantity, $iOldQuantity) {
    
    if ($iNewQuantity == 0 && $iNewQuantity <> $iOldQuantity &&
        $cNewStatus == 0 && $cNewStatus <> $cOldStatus) {
      return TRUE;
    } else {
      return FALSE;
    }
  }// end of checkCancelOrderToProviderToOrder() method
  
  
  /**
   * Sprawdzamy czy zamówienia do dostawcy powinny zostać zmienione w związku ze zmianą pozycji
   *  chodzi np. o przebicie pozycji do innego źródła, lub cofnięcie statusu
   * 
   * @param array $aNewData
   * @param array $aOldData
   * @return bool
   */
  public function checkChangeOrdersToProviders($aNewData, $aOldData) {
    if (((isset($aOldData['provider_id']) && $aOldData['provider_id'] <> $aNewData['provider_id']) || 
          ($aOldData['status'] <> $aNewData['status'] && 
          $aNewData['status'] == '10'  // status nowego to do rezerwacji
          )) // && in_array($aOldData['status'], array('50', '40', '30', '20')) // starsze mogą być późniejsze
            ) {
      return true;
    }
    return false;
  }// end of checkChangeOrdersToProviders() method
  
  
  
/*
    [108555-738653] => Array
        (
            [id] => 738653
            [isbn] => 9788376211770
            [name] => Sprawdzian szóstoklasisty język angielski +CD. Repetytorium z testami
            [publisher] => Macmillan Publishers Ltd
            [publication_year] => 2014
            [location] => 
            [source_item_id] => [np. Azymut bookindeks, pozniej wykorzystane przy potwierdzaniu ze źródła]
            [items] => 342517,342243
            [items_quantity] => Array
                (
                    [0] => 1
                    [1] => 2
                )

            [quantity] => 3
            [azymut_wholesale_price] => 0.00
            [siodemka_wholesale_price] => 54.75
            [ateneum_wholesale_price] => 57.03
            [helion_wholesale_price] => 0.00
            [dictum_wholesale_price] => 0.00
            [olesiejuk_wholesale_price] => 0.00
            [order_id] => 108555
        )

    [108623-585653] => Array
        (
            [id] => 585653
            [isbn] => 9788385430964
            [name] => The grammatical structure of legal English
            [publisher] => Wydawnictwo Translegis
            [publication_year] => 2010
            [location] => 
            [source_item_id] => [np. Azymut bookindeks, pozniej wykorzystane przy potwierdzaniu ze źródła]
            [items] => 342450
            [items_quantity] => Array
                (
                    [0] => 1
                )

            [quantity] => 1
            [azymut_wholesale_price] => 0.00
            [siodemka_wholesale_price] => 0.00
            [ateneum_wholesale_price] => 0.00
            [helion_wholesale_price] => 0.00
            [dictum_wholesale_price] => 0.00
            [olesiejuk_wholesale_price] => 0.00
            [order_id] => 108623
        )
 */
  
  /**
   * Metoda dodaje składowe zamówienia do dostawcy
   * 
   * @param int $iOrderToProviderId
   * @param array $aBooks
   * @return boolean
   */
  private function _insertOrderToProviderItems($iOrderToProviderId, $aBooks) {
    foreach ($aBooks as $aBook) {
      $aOrderItems = explode(',', $aBook['items']);
      foreach ($aOrderItems as $iKey => $iItemOrderId) {
        $aProductData = $this->_getProductAdditionalItemInfo($iItemOrderId);
        
        $aValues = array(
          'orders_to_providers_id' => $iOrderToProviderId,
          'product_id' => $aBook['id'],
          'order_id' => $aBook['order_id'],
          'order_item_id' => $iItemOrderId,
          'status' => '1',
          'to_order_quantity' => $aBook['items_quantity'][$iKey],
          'source_item_id' => isset($aBook['source_item_id']) ? $aBook['source_item_id'] : 'NULL',
          'name' => $aProductData['name'],
          '`desc`' => $aProductData['description'],
          'weight' => $aProductData['weight'],
          'cover' => $aProductData['cover'],
        );
        $iOTPId = $this->pDbMgr->Insert('magazine', 'orders_to_providers_items', $aValues);
        if ($iOTPId === FALSE) {
          return FALSE;
        } else {
          $this->_insertOTPII($iOTPId, $iOrderToProviderId, $aBook);
          $this->_affectOrdersToProvidersToOrder($aValues, $iOTPId);
        }
      }
    }
    return TRUE;
  }// end of _insertOrderToProviderItems() method
  
  /**
   * Metoda dodaje do zlecenia zamówienia do dostawcy dodaje zamówienie
   * 
   * @param array $aValues
   * @param int $iOTPId
   */
  private function _affectOrdersToProvidersToOrder($aValues, $iOTPId) {
    
    $aNewValues = array(
        'orders_to_providers_items_id' => $iOTPId
    );
    /*
    $this->pDbMgr->Update('magazine', 'orders_to_providers_to_order', $aNewValues, ' orders_items_id = '.$aValues['order_item_id'] . ' 
                                                                                    AND quantity = '.$aValues['to_order_quantity'] .' 
                                                                                    AND status <> "99" 
                                                                                    AND orders_to_providers_items_id IS NULL
                                                                                    LIMIT 1');
    */
    $this->pDbMgr->Update('profit24', 'orders_to_providers_to_order', $aNewValues, ' orders_items_id = '.$aValues['order_item_id'] . ' 
                                                                                AND quantity = '.$aValues['to_order_quantity'] .' 
                                                                                AND status <> "99" 
                                                                                AND orders_to_providers_items_id IS NULL
                                                                                LIMIT 1');
  }// end of _affectOrdersToProvidersToOrder() method
  
  
  /**
   * Metoda pobiera dodatkowe dane na temat produktu
   * 
   * @param int $iOrderItemId
   * @return array
   */
  private function _getProductAdditionalItemInfo($iOrderItemId) {
    
    $sSql = 'SELECT DISTINCT OI.id, OI.name, OI.weight, CONCAT(OI.publication_year, " ", OI.publisher, "\n", OI.authors) as description, CONCAT("images/photos/", PI.directory, "/", PI.photo) as cover
             FROM orders_items AS OI
             LEFT JOIN products_images AS PI
              ON PI.product_id = OI.product_id
             WHERE OI.id = '.$iOrderItemId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of _getProductAdditionalItemInfo() method
  
  
  /**
   * Metoda dodaje identyfikatory składowej do zamówienia
   * 
   * @param int $iOTPId
   * @param int $iOrderToProviderId
   * @param array $aBook
   */
  private function _insertOTPII($iOTPId, $iOrderToProviderId, $aBook) {
    
    
    if ($this->_checkExistsOTPII($aBook['id']) == false) {
      $aIdents = array();
      if ($aBook['isbn_plain'] != '') {
        $aIdents[] = $aBook['isbn_plain'];
      }
      if ($aBook['isbn_10'] != '') {
        $aIdents[] = $aBook['isbn_10'];
      }
      if ($aBook['isbn_13'] != '') {
        $aIdents[] = $aBook['isbn_13'];
      }
      if ($aBook['ean_13'] != '') {
        $aIdents[] = $aBook['ean_13'];
      }

      $aIdents = array_unique($aIdents);
      foreach ($aIdents as $sIdent) {
        $aValues = array(
            //'orders_to_providers_id' => $iOrderToProviderId,
            //'orders_to_providers_items_id' => $iOTPId,
            'product_id' => $aBook['id'],
            'ident' => $sIdent
        );
        $this->pDbMgr->Insert('magazine', 'orders_to_providers_items_idents', $aValues);
      }
    }
  }// end of _insertOTPII() method
  
  
  /**
   * Metoda sprawdza, czy istnieje identyfikator składowej
   * 
   * @param int $iProductId
   * @return bool
   */
  private function _checkExistsOTPII($iProductId) {
    $sSql = "SELECT id 
             FROM orders_to_providers_items_idents
             WHERE product_id = ".$iProductId;
    return ($this->pDbMgr->GetOne('magazine', $sSql) > 0 ? TRUE : FALSE);
  }// end of checkExistsOTPII() method

  
  /**
   * Insert do orders_to_providers
   * 
   * @param string $sOrderNumber
   * @param int $iProviderId
   * @param bool $bInternal
   * @param string $sCreatedBy
   * @param string $sExOrderNumber
   * @return bool
   */
  private function _insertOrderToProvider($sOrderNumber, $iProviderId, $bInternal, $sCreatedBy, $sExOrderNumber = 'NULL') {
    
    $aValues = array(
        'number' => $sOrderNumber,
        'ex_ident_number' => $sExOrderNumber == '' ? 'NULL' : $sExOrderNumber,
        'status' => '1',
        'created' => 'NOW()',
        'created_by' => $sCreatedBy
    );
    if ($bInternal === TRUE) {
      $aValues['internal_provider_id'] = $iProviderId;
      $aValues['provider_id'] = $iProviderId;
    } else {
      $aValues['provider_id'] = $iProviderId;
    }
    return $this->pDbMgr->Insert('magazine', 'orders_to_providers', $aValues);
  }// end of _insertOrderToProvider() method
  
  
  /**
   * Metoda przeszukuje zamówione pozycje po indeksie w źródle
   * 
   * @param int $iOrderToProviderId
   * @param source $sSourceItemId
   * @return array
   */
  public function getSourceItemOrderToProvider($iOrderToProviderId, $sSourceItemId) {
    
    $sSql = "
    SELECT OTPI.id, OTPI.order_item_id, OTPI.order_id, OTPI.to_order_quantity, O.payment_id
    FROM orders_to_providers_items AS OTPI
    JOIN orders_to_providers AS OTP
      ON OTP.id = OTPI.orders_to_providers_id AND OTP.id = ".$iOrderToProviderId."
    JOIN orders AS O
      ON O.id = OTPI.order_id
    WHERE OTPI.source_item_id = '".$sSourceItemId."'";
    return $this->pDbMgr->getAll('magazine', $sSql);
  }// end of _getSourceItemOrderToProvider() method
  
  
  /**
   * Metoda pobiera id zamówienia do dostawcy na podstawie zewnętrznego numeru dostawcy
   * 
   * @param string $sExIdentNumber (uniqe)
   * @return id
   */
  public function getOrderProviderId($sExIdentNumber) {
    
    $sSql = "SELECT id 
      FROM orders_to_providers 
      WHERE ex_ident_number = '".$sExIdentNumber."'";
    return $this->pDbMgr->GetOne('magazine', $sSql);
  }// end of getOrderProviderId() method
  
  
  /**
   * Metoda pobiera dane elementu zamówienia do dostawcy
   * 
   * @param int $iOTPId
   * @param array $aSelCols
   * @return array
   */
  public function getOrderToProviderData($iOTPId, $aSelCols) {
    
    $sSql = "SELECT ".implode(',', $aSelCols)." 
             FROM orders_to_providers 
             WHERE id = ".$iOTPId;
    return $this->pDbMgr->GetRow('magazine', $sSql);
  }// end of getOrderToProviderItemData() method
  
  
  /**
   * Metoda pobiera dane elementu zamówienia do dostawcy
   * 
   * @param int $iOTPIId
   * @param array $aSelCols
   * @return array
   */
  public function getOrderToProviderItemData($iOTPIId, $aSelCols) {
    
    $sSql = "SELECT ".implode(',', $aSelCols)." 
             FROM orders_to_providers_items 
             WHERE id = ".$iOTPIId;
    return $this->pDbMgr->GetRow('magazine', $sSql);
  }// end of getOrderToProviderItemData() method
  
  
  /**
   * Metoda zmienia aktualną ilość zamówionych pozycji
   * 
   * @global object $pDbMgr
   * @param int $iOrderItemId
   * @param int $iConfirmedQuantity
   * @param char $cStatus 1 - otwarty normalnie w zamówieniu do dostawcy, 3 - rezerwacja, 6 - FV, 9 - tytuł potwierdzony, B - zabrakło tego tytułu do potwierdzenia, D - zamowienie z tym tytułem zostało anulowane, E - produkt został przebity do innego źródła
   * @return bool
   */
  public function updateCurrentQuantity($iOrderItemId, $iConfirmedQuantity, $cStatus) {
    
    $aValues = array(
        'confirmed_quantity' => $iConfirmedQuantity,
        'status' => $cStatus
    );
    return $this->pDbMgr->Update('magazine', 'orders_to_providers_items', $aValues, ' id = '.$iOrderItemId.' AND status <> "9" ');
  }// end of _updateCurrentQuantity() method
  
  
  /**
   * Metoda zmienia status listy do zamówienia
   * 
   * @param int $iOrderProviderId
   * @param int $iStatus 0 - anulowane/usunięte, 1 - nowe, 3 - rezerwacja, 4 - w trasie, 6 - FV, 8 - przyjechalo na magazyn, 9 - potwierdzone
   * @param string $sModifiedBy
   * @return bool
   */
  public function changeOrdersToProvidersStatus($iOrderProviderId, $iStatus, $sModifiedBy = 'auto') {
    
    $aValues = array(
        'status' => $iStatus,
    );
    $this->pDbMgr->Update('magazine', 'orders_to_providers', $aValues, 'id='.$iOrderProviderId);
    
    $aValues = array(
        'orders_to_providers_id' => $iOrderProviderId,
        'status' => $iStatus,
        'modified' => 'NOW()',
        'modified_by' => $sModifiedBy
    );
    return $this->pDbMgr->Insert('magazine', 'orders_to_providers_status', $aValues);
  }// end of changeOrdersToProvidersStatus() method
  
  
  /**
   * Zmienia id zamówienia
   * 
   * @param int $iNewOrderId
   * @param int $iOldOrderId
   * @return bool
   */
  public function changeOrderToProvidersItemsOrderId($iNewOrderId, $iOldOrderId) {
    
    $aValues = array(
        'order_id' => $iNewOrderId
    );
    return $this->pDbMgr->Update('magazine', 'orders_to_providers_items', $aValues, ' order_id = '.$iOldOrderId);
  }// end of changeOrderToProvidersItemsOrderId() method
  
  /**
   * Zmienia status produktu w źródle
   * 
   * @param int $iOTPIId
   * @param char $cStatus
   * @return bool
   */
  public function changeOrderToProvidersItemsStatus($iOTPIId, $cStatus) {
    
    $aValues = array(
        'status' => $cStatus
    );
    return $this->pDbMgr->Update('magazine', 'orders_to_providers_items', $aValues, 'id = '.$iOTPIId.' AND status <> "9" ');
  }// end of changeOrderToProvidersItemsStatus() method
  
  
  /**
   * Zmienia status produktu w źródle
   * 
   * @param int $iOrderId
   * @param char $cStatus
   * @return bool
   */
  public function changeOrderToProvidersItemsStatusByOrderId($iOrderId, $cStatus) {
    
    $aValues = array(
        'status' => $cStatus
    );
    return $this->pDbMgr->Update('magazine', 'orders_to_providers_items', $aValues, ' order_id = '.$iOrderId.' AND status <> "9" ');
  }// end of changeOrderToProvidersItemsStatus() method
  
  
  /**
   * Zmienia status składowej zamówienia od klienta na liście zamówienia do dostawcy
   * 
   * @param int $iOrderItemId
   * @param char $cStatus
   * @return bool
   */
  public function changeOrderToProvidersItemsStatusByOrderItemId($iOrderItemId, $cStatus) {
    
    $aValues = array(
        'status' => $cStatus
    );
    return $this->pDbMgr->Update('magazine', 'orders_to_providers_items', $aValues, ' order_item_id = '.$iOrderItemId.' AND status <> "9" ');
  }// end of changeOrderToProvidersItemsStatusByOrderItemId() method
  
  
  /**
   * Zmienia status składowej zamówienia od klienta na liście zamówienia do dostawcy
   * 
   * @param int $iOrderItemId
   * @param int $iProviderId
   * @param char $cStatus
   * @return bool
   */
  public function changeOrderToProvidersItemsStatusByOrderItemIdProvider($iItemId, $iProviderId, $cStatus) {
    
    
    $sSql = 'UPDATE `orders_to_providers_items` AS OTPI
            JOIN orders_to_providers AS OTP
            ON OTPI.orders_to_providers_id = OTP.id
            SET OTPI.status = "'.$cStatus.'"
            WHERE (OTP.provider_id = '.$iProviderId.'
                  OR OTP.internal_provider_id = '.$iProviderId.') 
                    AND OTPI.id = '.$iItemId.' 
            AND OTPI.status <> "9";';
    return $this->pDbMgr->Query('magazine', $sSql);
  }// end of changeOrderToProvidersItemsStatusByOrderItemId() method
  
  
  /**
   * Metoda przywraca składowe listy, (zmiana statusu produktu z anulowanego na taki ktory realizuje zamówienie)
   * 
   * @param int $iOId
   * @param int $iOrderItemId
   * @return bool
   * TODO rozbić zapytanie
   */
  public function restoreOrderItems($iOId, $iOrderItemId = 0) {
    // UWAGA NALEŻY ZMIENIĆ zapytanie na analogiczne, ale z rozbiciem na dwa osobne SQL
    
    $sSql = 'SELECT id
             FROM orders_items 
             WHERE order_id = '.$iOId.' AND
                   deleted = "0" AND
                   item_type = "I" AND 
                   status = "2" OR status = "3" ';
    $iItemId = $this->pDbMgr->GetOne('profit24', $sSql);
    
    if ($iItemId > 0) {
      // składowe które, zostały zmienione w zwiazku z anulowaniem czyli jako 'B' i lista nie jest potwierdzona oraz anulowana
      // oraz składowa nie jest usunięta i jej status to zamówiona lub jest nie potwierdzona
      /**
        JOIN orders_items AS OI
          ON OTPI.order_id = OI.order_id
        AND OI.deleted = "0" 
        AND OI.item_type = "I"
        AND (OI.status = "2" OR OI.status = "3")
       */
      $sSql = 'SELECT OTPI.id
               FROM orders_to_providers_items AS OTPI
               JOIN orders_to_providers AS OTP
                ON OTPI.orders_to_providers_id = OTP.id AND OTP.status <> "0" AND OTP.status <> "9"
               WHERE 
                 OTPI.order_id = '.$iOId.' AND '.
                 ($iOrderItemId > 0 ? ' OTPI.order_item_id = '.$iOrderItemId : '' ).
                 ' OTP.status <> "0" AND 
                 OTP.status <> "9" ';
      $aIdsToRestore = $this->pDbMgr->getCol('magazine', $sSql);

      foreach ($aIdsToRestore as $iOTPIId) {
        // przywracamy składową listy w status aktywny, '1'
        if ($this->changeOrderToProvidersItemsStatus($iOTPIId, '1') === FALSE) {
          return FALSE;
        }
      }
    }
    return TRUE;
  }// end of restoreOrderItems
  
  
  /**
   * Metoda pobiera składowe listy, które nie znalazły się na fakturze, ponieważ nie zostały zakupione
   * 
   * @param int $iOrderToProvider
   * @return array(int)
   * XXX rozdzielone zapytania
   */
  public function getItemsNotConfirmed($iOrderToProvider) {
    
    $sSql = "SELECT OTPI.id, OTPI.order_item_id, OTPI.order_id
             FROM orders_to_providers_items AS OTPI
             WHERE OTPI.orders_to_providers_id = ".$iOrderToProvider. 
                 " AND OTPI.status <> '6'";
    $aOTPI = $this->pDbMgr->GetAll('magazine', $sSql);
    
    foreach ($aOTPI as $iKey => $aOTP) {
      $sSql = 'SELECT payment_id 
             FROM orders 
             WHERE id = '.$aOTP['order_id'];
      $iPaymentId = $this->pDbMgr->GetOne('profit24', $sSql);
      $aOTPI[$iKey]['payment_id'] = $iPaymentId;
    }
    
    return $aOTPI;
  }// end of getListItemNotOnInvoice() method
  
  
  /**
   * Metoda potwierdza i zamyka całą listę
   * 
   * @param int $iOTPId
   * @return bool
   */
  public function commitList($iOTPId, $sModifiedBy) {
    return $this->changeOrdersToProvidersStatus($iOTPId, '9', $sModifiedBy);
  }// end of commitList() method
  
  
  /**
   * Metoda pobiera sugerowaną ilość pozycji pozostałych do zamówienia w przypadku, 
   *  podziału zamówienia na składowe, czyli proponuje ile zamówić u danego dostawcy.
   *  Aby to obliczyć należy dla zamówień, które już zostały zamówione (status >= 10 ) pobrać liczbę egz.
   *  która została !!! potwierdzona !!!,
   *  Natomiast jeśli pozycja jest 0 to należy wtedy pobrać ilość, którą jest do zamówienia (status = 0)
   * 
   * @param int $iItemOrderId
   */
  public function getQuantityOrdersToProvidersToOrderToOrder($iItemOrderId) {
    
    $sSql = '
    SELECT IFNULL(SUM(IF(OTPTO.status = "10" OR OTPTO.status = "0", IFNULL(OTPTO.quantity, 0), IFNULL(OTPTO.confirmed_quantity, 0))), 0)
    FROM orders_to_providers_to_order AS OTPTO
    WHERE 
      OTPTO.orders_items_id = '.$iItemOrderId;
    $iSumOTPTO = intval($this->pDbMgr->GetOne('profit24', $sSql));
    
    $sSql = 'SELECT quantity FROM orders_items WHERE id = '.$iItemOrderId;
    $iQuntity = $this->pDbMgr->GetOne('profit24', $sSql);
    return $iQuntity - $iSumOTPTO;
  }// end of getQuantityOrdersToProvidersToOrderToOrder() method
}// end of Class

