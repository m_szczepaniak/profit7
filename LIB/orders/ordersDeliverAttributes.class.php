<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-28 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace orders;

/**
 * Zarządzamy dostawy zamówienia
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ordersDeliverAttributes {
  
  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;


    private static $aTransportMethodsMethodsIdsEqual = [8,4];

  public function __construct() {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
  }
  

  /**
   * @param int $iTransportId
   * @param int $iOrderId
   * @param int $iGUID
   * @param int $iBuferId
   * @return bool
   */
  public function add($iTransportId, $iOrderId, $iGUID = 'NULL', $iBuferId = 'NULL') {
    
    $aValues = array(
        'order_id' => $iOrderId,
        'transport_id' => $iTransportId,
        'guid' => $iGUID,
        'bufer_id' => $iBuferId
    );
    return $this->pDbMgr->Insert('profit24', 'orders_deliver_attributes', $aValues, false);
  }
  
  /**
   * @param int $iOrderId
   * @param int $iTransportId
   * @param int $iBuferId
   */
  public function updateBuffer($iOrderId, $iTransportId, $iBuferId) {
    
    $aValues = array(
        'bufer_id' => $iBuferId
    );

      $iTransportId = $this->mergeTransportIfEqual($iTransportId);

    $sWhere = ' order_id = ' . $iOrderId.
              ' AND transport_id IN (' . $iTransportId .' ) '.
              ' LIMIT 1';
    return $this->pDbMgr->Update('profit24', 'orders_deliver_attributes', $aValues, $sWhere);
  }
  
  /**
   * Pobieramy guid
   * 
   * @param int $iOrderId
   * @param int $iTransportId
   * @return int guid
   */
  public function getGUIDByOrderId($iOrderId, $iTransportId) {
    
    $sCol = 'guid';
    $aData = $this->getOrderDeliverAttribsByCol($iOrderId, $iTransportId, array($sCol));
    return $aData[$sCol];
  }
  
  
  
  /**
   * Metoda pobiera dane attrybuty dostawy
   * 
   * @param int $iOrderId
   * @param int $iTransportId
   * @param array $aCols
   */
  public function getOrderDeliverAttribsByCol($iOrderId, $iTransportId, $aCols) {

      $iTransportId = $this->mergeTransportIfEqual($iTransportId);

    $sSql = 'SELECT '.implode(',', $aCols).' 
             FROM orders_deliver_attributes
             WHERE order_id = '.$iOrderId.' AND transport_id IN ( '.$iTransportId.' )
             ORDER BY id DESC';
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
  
  
  /**
   * Metoda pobiera dane attrybuty dostawy
   * 
   * @param array $aOrderIds
   * @param int $iTransportId
   * @param array $aCols
   */
  public function getOrdersAssocDeliverAttribsByCol($aOrderIds, $iTransportId, $aCols) {

    $iTransportId = $this->mergeTransportIfEqual($iTransportId);

    $sSql = 'SELECT '.implode(',', $aCols).' 
             FROM orders_deliver_attributes
             WHERE order_id IN ('.implode(',', $aOrderIds).') AND transport_id IN ( '.$iTransportId.' )
             ORDER BY id DESC';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);
  }

    /**
     * Metoda łączy metody transportu jeśli jest to metoda równoważna
     *
     * @param $iTransportId
     * @return array
     */
    private function mergeTransportIfEqual($iTransportId) {

        if (in_array($iTransportId,  self::$aTransportMethodsMethodsIdsEqual)) {
            $iTransportId = implode(', ', self::$aTransportMethodsMethodsIdsEqual);
        }
        return $iTransportId;
    }
}
