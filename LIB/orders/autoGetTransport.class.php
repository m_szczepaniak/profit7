<?php
/**
 * Klasa skryptu automatycznego pobierania i zapisywania numer listu przewozowego,
 * jeśli wystąpi błąd podczas pobierania listu oznacza, listę na której znajduje się to zamówinie, jako błędna i musi czekać aż składowe będą prawidłowe
 * 
 * Dodatkowo sprawdzane są za każdym razem wszystkie zamówienia, w listach błędnych, czy można już pobrać listy dla składowych
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-10-27 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders;
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/orders/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(900);
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';
error_reporting(E_ERROR || E_WARNING || E_NOTICE);

class autoGetTransport {

  /**
   *
   * @var bool
   */
  private $bTestMode;
  
  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;

  function __construct($bTestMode) {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    
  }
  
  /**
   * Metoda pobiera z bazy listy które zostały pobrane closed = '0' 
   * wybiera z nich zamówienia, które nie mają wprowadzonego numeru listu transportowego,
   * 
   * Jeśli wystąpił tu problem lista oznaczana jest jako oczekująca
   * 
   * @return array
   */
  public function proceedOrdersNullTransport() {
    // lista ma zostać oznaczona jako błędna closed = '3'
    $oGetTransportList = new \orders\getTransportList($this->pDbMgr);
    
    $aToOrdersGetTransportNumber = $this->getOrdersTransport(array('0', '2'));
    $aOrders = $oGetTransportList->uniqueLinkedOrders($aToOrdersGetTransportNumber);
    foreach ($aOrders as $aOrder) {
      try {
        $this->pDbMgr->BeginTransaction('profit24');
        $oGetTransportList->getSimpleTransportAddressLabel($aOrder, $aOrder['linked_orders'], true);
        $this->pDbMgr->CommitTransaction('profit24');
      } catch (\Exception $ex) {
        $this->pDbMgr->RollbackTransaction('profit24');
        
        $oAlert = new \orders_alert\OrdersAlert();
        $oAlert->createAlert($aOrder['id'], 'auto-trans', 
                _('Wystąpił błąd podczs pobierania nr listu przewozowego - lista błedów').'<br />'.$ex->getMessage());
        $oAlert->removeDuplicates();
        $oAlert->addAlerts();
        
        // oznaczamy listę jako oczekującą '3'
        $this->setStatusList('3', $aOrder['orders_items_lists_id']);
        //throw new \Exception($ex->getMessage(), $aOrder['id']);
      }
    }
  }// end of proceedOrdersNullTransport() method
  
  
  /**
   * Metoda zmienia status oczekujących list
   * 
   * @param array $aUniqueListsIds
   * @return bool
   */
  private function setStatusList($cClosed, $mUniqueListsIds) {
    
    $aValues = array(
        'closed' => $cClosed
    );
    if (is_array($mUniqueListsIds)) {
      foreach ($mUniqueListsIds as $iListId) {
        if ($iListId > 0) {
          if ($this->pDbMgr->Update('profit24', 'orders_items_lists', $aValues, ' id = '.$iListId) === false) {
            return false;
          }
        }
      }
    } elseif (is_numeric($mUniqueListsIds) && $mUniqueListsIds > 0) {
      if ($this->pDbMgr->Update('profit24', 'orders_items_lists', $aValues, ' id = '.$mUniqueListsIds) === false) {
        return false;
      }
    } else {
      return false;
    }
    return true;
  }// end of setStatusList() method
  
  
  /**
   * Metoda pobiera unikatowe id list
   * 
   * @param array $aOrders
   * @return type
   */
  private function getUniqueListsIds($aOrders) {
    
    $aOrdersItemsLists = array();
    foreach ($aOrders as $aOrder) {
      if (!in_array($aOrder['orders_items_lists_id'], $aOrdersItemsLists)) {
        $aOrdersItemsLists[] = $aOrder['orders_items_lists_id'];
      }
    }
    return $aOrdersItemsLists;
  }// end of getUniqueListsIds() method
  
  
  /**
   * Metoda pobiera zamówienia z pusta metodą transportu
   * 
   * @return array
   */
  private function getOrdersTransport($aListStatus) {
    
    $sSql = 'SELECT DISTINCT O.id, O.*, OILI.orders_items_lists_id
             FROM orders_items_lists_items AS OILI
             JOIN orders_items_lists AS OIL 
              ON OIL.id = OILI.orders_items_lists_id AND OIL.closed IN ("'.implode('", "', $aListStatus).'")
             JOIN orders AS O
              ON OILI.orders_id = O.id
             WHERE
                (O.transport_number IS NULL OR O.transport_number = "") 
                AND O.order_status <> "5"
                AND O.order_status <> "4"
                AND O.transport_id <> "2"
                AND OIL.type <> "4"
                ';

    //$sSql = 'SELECT O.id , O.* FROM orders as O WHERE O.id = "1319416"';
     return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getOrdersNullTransport() method
  
  /**
   * Pobieramy listę, która jest oznaczona jako oczekująca, i próbujemy ją odblokować
   * 
   * @throws \Exception
   */
  public function getListsWaiting() {
    // lista ma zostać oznaczona jako oczekująca closed = '3'
    $oGetTransportList = new \orders\getTransportList($this->pDbMgr);
    
    // pobieramy oczekujące
    $aToOrdersGetTransportNumber = $this->getOrdersTransport(array('3'));
    $aOrders = $oGetTransportList->uniqueLinkedOrders($aToOrdersGetTransportNumber);

    foreach ($aOrders as $aOrder) {
      try {
        $this->pDbMgr->BeginTransaction('profit24');
        $oGetTransportList->getSimpleTransportAddressLabel($aOrder, $aOrder['linked_orders'], true);
        $this->pDbMgr->CommitTransaction('profit24');
      } catch (\Exception $ex) {
        $this->pDbMgr->RollbackTransaction('profit24');
        // oznaczamy całą listę jako oczekującą
        $this->setStatusList('3', $aOrder['orders_items_lists_id']);
        $oAlert = new \orders_alert\OrdersAlert();
        $oAlert->createAlert($aOrder['id'], 'auto-trans', 
                _('Wystąpił błąd podczs pobierania nr listu przewozowego - lista błedów').'<br />'.$ex->getMessage());
        $oAlert->removeDuplicates();
        $oAlert->addAlerts();
        throw new \Exception($ex->getMessage(), $aOrder['id']);
      }
    }
    // wczesniej się powiodło, więc oznaczamy listy jako otwarte (ponieważ była wcześniej oczekująca)
    $this->setStatusList('0', $this->getUniqueListsIds($aToOrdersGetTransportNumber));
  }
}// end of Class

try {
  $oAutoGetTransport = new autoGetTransport(false);
  $oAutoGetTransport->getListsWaiting();
  $oAutoGetTransport->proceedOrdersNullTransport();
} catch (\Exception $ex) {
  $oAlert = new \orders_alert\OrdersAlert();
  $oAlert->createAlert('NULL', 'auto-trans', 
          _('Wystąpił błąd podczs pobierania nr listu przewozowego - lista błedów').'<br />'.$ex->getMessage());
  $oAlert->removeDuplicates();
  $oAlert->addAlerts();
  $oAlert->createAlert('NULL', 'auto-trans', 
          _('Wystąpił błąd podczs pobierania nr listu przewozowego - lista błedów').'<br />'.$ex->getMessage());
  $oAlert->sendAlertsMail();
}
echo "\n".'koniec'."\n";
