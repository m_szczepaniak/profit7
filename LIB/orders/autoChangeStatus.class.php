<?php
/**
 * Klasa dokonująca zmian w zamówieniu
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-21 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders;

use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use orders\auto_change_status\autoProviderToOrders;
use orders\auto_change_status\iAutoChangeStatus;

$command = 'ps -Af|grep '.basename(__FILE__);
explode("\n", exec($command, $output));
var_dump($output);
if (count($output) > 4) {
    die;
}


header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/orders';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '2G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';
ini_set('display_errors', 'On');
error_reporting(E_ERROR || E_WARNING || E_NOTICE);
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');

class autoChangeStatus {
  var $aScripts = array(
      '\orders\auto_change_status\autoFraud',
      '\orders\auto_change_status\autoPreviewsAvailableOrders',
      '\orders\auto_change_status\autoPreviewsOrders',
      '\orders\auto_change_status\autoProviderToOrders',
      '\orders\auto_change_status\autoDuplicates'
  );
  var $bTestMode = FALSE;

  function __construct() {
    global $pDbMgr, $aConfig;
    
    // XXX TODO test mode
    $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
    if ($bTestMode === true) {
      $this->aScripts = array(
          '\orders\auto_change_status\autoPreviewsAvailableOrders',
      );
    }
    
    foreach ($this->aScripts as $sScript) {
      $oAutoObj = new $sScript($this->bTestMode, $pDbMgr);
      $this->_proceedAutoChangeStatus($oAutoObj);
    }
  }
  
  
  /**
   * Metoda automatycznie zmienia status zamówień
   * 
   * @param object $oScript
   */
  private function _proceedAutoChangeStatus(iAutoChangeStatus $oAutoObj) {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;

    // pojawiła się zapowiedź w źródle - zmiana statusu wewn. zamówień z 'zapowiedzi' na '---'
    $aOrdersIds = $oAutoObj->getOrdersItemsDataToUpdate();
    if (true == $oAutoObj instanceof autoProviderToOrders){
         return $this->proceedAutoProvidersToOrders($oAutoObj, $aOrdersIds);
    }
    
    $this->pDbMgr->BeginTransaction('profit24');

    $aInformData = $oAutoObj->UpdateOrdersItems($aOrdersIds, true);
    
    if ($aInformData === false) {
      echo 'błąd zmiany statusów zamówień';
      $this->pDbMgr->RollbackTransaction('profit24');
    } else {
      $oAutoObj->informAboutStatusChanged($aInformData);
      $this->pDbMgr->CommitTransaction('profit24');
    }
  }

  /**
   * Obrabia standardowe zamowienia z rezerwacjami
   *
   * @param autoProviderToOrders $oAutoObj
   * @param $aOrdersIds
   * @return bool
   */
  private function proceedAutoProvidersToOrders(autoProviderToOrders $oAutoObj, $aOrdersIds)
  {
      $groupedItems = [];

      foreach($aOrdersIds as $aOrderItem){
        $groupedItems[$aOrderItem['order_id']][] = $aOrderItem;
      }

      if (empty($groupedItems)){
        return false;
      }

      try {
          $oAutoObj->UpdateOrdersItemsAndReservations($groupedItems, true);
      } catch (\Exception $ex){
          echo 'o właśnie tutaj';
          echo $ex->getMessage();
      }


      return true;
  }
}

$oAutoChangeStatus = new autoChangeStatus();

