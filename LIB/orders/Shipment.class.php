<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-17 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders;

use communicator\sources\Internal\Shipment as InternalShipment;

class Shipment 
implements \communicator\sources\iShipment, 
        \communicator\sources\iPointsOfReceipt, 
        \communicator\sources\iDeliver
{

  public $bTestMode;
  public $pDbMgr;
  public $aTransportSettings;
  public $iTransportId;

  /**
   * 
   * @param mixed $mTransportIdSymbol Id metody transportu lub symbol
   * @param \DatabaseManager $pDbMgr
   * @param bool $bTestMode
   */
  function __construct($mTransportIdSymbol, $pDbMgr, $bTestMode = NULL) {
    global $aConfig;
    
    $this->pDbMgr = $pDbMgr;
    if ($bTestMode === NULL) {
        $this->bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
    } else {
      $this->bTestMode = $bTestMode;
    }
    $this->setTransportSettings($mTransportIdSymbol);
  }
  
  /**
   * 
   * @param string $sMethodName
   * @param string $arguments 0 - class name
   *                          ... reszta argumentów
   */  
  function __call($sMethodName, $arguments) {
    $sClassName = $arguments[0];
    $sInterfaceName = 'i'.$sClassName;

    if ($this->_checkInterfaceImplemented($sInterfaceName)) {
    
      $sClassSymbol = $this->aTransportSettings['class_symbol'];
      $sClassNameWithNamespace = '\communicator\sources\\'.$sClassSymbol.'\\'.$sClassName;
      $oClass = new $sClassNameWithNamespace($this->aTransportSettings, $this->pDbMgr, $this->bTestMode);
      $sInterface = '\communicator\sources\\'.$sInterfaceName;
      if ($oClass instanceof $sInterface) {
        unset($arguments[0]);
        ksort($arguments);
        return call_user_func_array(array($oClass, $sMethodName), $arguments);
      } else {
        throw new \Exception(_('Wystąpił błąd podczas ładowania obiektu typu punkt odbioru zadanej metody transportu'));
      }
    } else {
      throw new \Exception(_('Przekazano niezaimplementowany interface '.$sInterfaceName));
    }
  }
  
  /**
   * 
   * @param string $sInterfaceName
   * @return boolean
   */
  private function _checkInterfaceImplemented($sInterfaceName) {
    
    $aImplements = class_implements($this);
    foreach ($aImplements as $sTestInterfaceName) {
      if (stristr($sTestInterfaceName, $sInterfaceName)) {
        return true;
      }
    }
    return false;
  }
  
  
  /**
   * 
   * @param mixed $mTransportIdSymbol
   */
  private function setTransportSettings($mTransportIdSymbol, $sWebsite = 'profit24') {
    if (is_numeric($mTransportIdSymbol)) {
      $iTransportId = $mTransportIdSymbol;
    } else {
      $iTransportId = $this->_getTransportId($mTransportIdSymbol);
    }
    $oKeyValue = new \table\keyValue('orders_transport_means', $sWebsite);
    $this->aTransportSettings = $oKeyValue->getAllByIdDest($iTransportId);
    $this->aTransportSettings['transport_id'] = $iTransportId;
    $this->iTransportId = $iTransportId;
  }
  
  
  /**
   * 
   * @param string $sTransportSymbol
   * @return int
   */
  private function _getTransportId($sTransportSymbol) {
    
    $sSql = 'SELECT id FROM orders_transport_means WHERE symbol = "'.$sTransportSymbol.'" LIMIT 1';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }// end of _getTransportId() method
  
  
  /**
   * 
   * @return \communicator\sources\iPointsOfReceipt
   */
  private function getInstancePointOfReceipt() {
    
    $sClassSymbol = $this->aTransportSettings['class_symbol'];
    $sClassName = '\communicator\sources\\'.$sClassSymbol.'\\PointsOfReceipt';
    $oClass = new $sClassName($this->aTransportSettings, $this->pDbMgr, $this->bTestMode);
    if ($oClass instanceof \communicator\sources\iPointsOfReceipt) {
      return $oClass;
    } else {
      throw new \Exception(_('Wystąpił błąd podczas ładowania obiektu typu punkt odbioru zadanej metody transportu'));
    }
  }
  
  
  /**
   * 
   * @return \communicator\sources\iShipment
   */
  private function getInstanceShipment() {

    $sClassSymbol = $this->aTransportSettings['class_symbol'];
    $sClassName = '\communicator\sources\\'.$sClassSymbol.'\\Shipment';

    if (class_exists($sClassName)) {
      $oClass = new $sClassName($this->aTransportSettings, $this->pDbMgr, $this->bTestMode);
      if ($oClass instanceof \communicator\sources\iShipment) {
          return $oClass;
      } else {
          throw new \Exception(_('Wystąpił błąd podczas ładowania obiektu typu punkt odbioru zadanej metody transportu'));
      }
    }
  }
  
  
  /**
   * 
   * @return \communicator\sources\iDeliver
   */
  private function getInstanceDeliver() {
    
    $sClassSymbol = $this->aTransportSettings['class_symbol'];
    $sClassName = '\communicator\sources\\'.$sClassSymbol.'\\Deliver';
      if (class_exists($sClassName)) {
          $oClass = new $sClassName($this->aTransportSettings, $this->pDbMgr, $this->bTestMode);
          if ($oClass instanceof \communicator\sources\iDeliver) {
              return $oClass;
          } else {
              throw new \Exception(_('Wystąpił błąd podczas ładowania obiektu typu punkt odbioru zadanej metody transportu'));
          }
      }
  }
  
  
  /**
   * 
   * @return void
   */
  public function doCancelShipment() {
    $oShipment = $this->getInstanceShipment();
    return $oShipment->doCancelShipment();
  }

  /**
   * 
   * @param string $sFilePath
   * @param string $sPrinter
   * @return bool
   */
  public function doPrintAddressLabel($sFilePath, $sPrinter) {
    $oShipment = $this->getInstanceShipment();
    return $oShipment->doPrintAddressLabel($sFilePath, $sPrinter);
  }

  /**
   * 
   * @param string $sFilePath
   * @param int $iOrderId
   * @return binary - PDF
   */
  public function getAddressLabel($sFilePath, $iOrderId) {
    $oShipment = $this->getInstanceShipment();
    return $oShipment->getAddressLabel($sFilePath, $iOrderId);
  }

  /**
   * 
   * @param int $iId
   * @param string $sTransportNumber
   * @return mixed
   */
  public function getDeliveryStatus($iId, $sTransportNumber) {
    $oShipment = $this->getInstanceShipment();
    return $oShipment->getDeliveryStatus($iId, $sTransportNumber);
  }


    /**
     *
     * @param array $agConfig
     * @param string $sTransportNumber
     * @param string $sTmpTransportNumber
     * @return string
     * @throws \Exception
     */
  public function getTransportListFilename($agConfig, $sTransportNumber) {

      if (false === $this->checkTransportTMPNumber($sTransportNumber)) {
          $oShipment = $this->getInstanceShipment();
          if (isset($oShipment) && $oShipment instanceof \communicator\sources\iShipment) {
              return $oShipment->getTransportListFilename($agConfig, $sTransportNumber);
          }
      } else {
          $oShipment = new InternalShipment();
          return $oShipment->getTransportListFilename($agConfig, $sTransportNumber);
      }
  }

    /**
     * @param $sTransportNumber
     * @return bool
     */
  private function checkTransportTMPNumber($sTransportNumber) {
        $sSql = 'SELECT id 
               FROM orders 
               WHERE transport_number = "'.$sTransportNumber.'" 
               AND tmp_transport_number = "1" ';
      return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
  }

  /**
   * 
   * @param array $aOrder
   * @param array $aDeliverAddress
   * @param array $aWebsiteSettings - @todo tutaj seller data + website settings, a na pewno email, albo może lepiej samo id serwisu
   * @return string - nr listu przewozowego
   */
  public function addShipment($aOrder, $aDeliverAddress, $aWebsiteSettings) {
    $this->setTransportSettings($this->iTransportId, $this->getWebsiteSymbolByOrderId($aOrder['id']));
    $oShipment = $this->getInstanceShipment();
    return $oShipment->addShipment($aOrder, $aDeliverAddress, $aWebsiteSettings);
  }
  
  
  /**
   * 
   * @param string $sPointOfReceipt
   * @return array
   */
  public function getSinglePointDetails($sPointOfReceipt) {
    $oPointOfReceipt = $this->getInstancePointOfReceipt();
    return $oPointOfReceipt->getSinglePointDetails($sPointOfReceipt);
  }
  
  /**
   * 
   * @param string $sPointOfReceipt
   * @return bool
   */
  public function checkDestinationPointAvailable($sPointOfReceipt) {
    $oPointOfReceipt = $this->getInstancePointOfReceipt();
    return $oPointOfReceipt->checkDestinationPointAvailable($sPointOfReceipt);
  }
  
  /**
   * 
   * @return array
   */
  public function getDestinationPointsList() {
    $oShipmentPoR = $this->getInstancePointOfReceipt();
    return $oShipmentPoR->getDestinationPointsList();
  }

  
  public function getDestinationPointsDropdown() {
    $oShipmentPoR = $this->getInstancePointOfReceipt();
    return $oShipmentPoR->getDestinationPointsDropdown();
  }
  
  /**
   * 
   * @param array $aTransportNumbers
   * @param array $aOrdersIds
   * @return binary PDF
   */
  public function getShipmentList($aTransportNumbers, $aOrdersIds = array()) {
    $this->setTransportSettings($this->iTransportId, $this->getWebsiteSymbolByOrderId($aOrdersIds[0]));
    $oDeliver = $this->getInstanceDeliver();
    return $oDeliver->getShipmentList($aTransportNumbers, $aOrdersIds);
  }

  public function getDeliverFilename() {
    $oShipmentDeliver = $this->getInstanceDeliver();
    return $oShipmentDeliver->getDeliverFilename();
  }

  public function getOrderDeliverFilePath($iOrderId) {
    $oShipmentDeliver = $this->getInstanceDeliver();
      if (isset($oShipmentDeliver) && $oShipmentDeliver instanceof \communicator\sources\iDeliver) {
          return $oShipmentDeliver->getOrderDeliverFilePath($iOrderId);
      }
  }
  
  /**
   * 
   * @return bool
   */
  public function isPersonalReception() {
    return ($this->aTransportSettings['type'] === 'personal_reception' ? true : false);
  }
  
  /**
   * 
   * @return bool
   */
  public function isReceptionAtPoint() {
    return ($this->aTransportSettings['type'] === 'reception_at_point' ? true : false);
  }
  
  /**
   * 
   * @return bool
   */
  public function isDeliveryToAddress() {
    return ($this->aTransportSettings['type'] === 'delivery_to_address' ? true : false);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return string website
   */
  private function getWebsiteSymbolByOrderId($iOrderId) {
    
    $sSql = 'SELECT W.code 
             FROM orders AS O
             JOIN websites AS W
              ON O.website_id = W.id
             WHERE O.id = '.$iOrderId;
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
}
