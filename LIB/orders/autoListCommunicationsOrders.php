<?php
/**
 * Klasa obsługi automatycznych zamówień
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-21 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class autoSendListOrders {
  private $sCurrPath;
  
  function __construct() {
    $this->sCurrPath = preg_replace('/(.*)\\'.DIRECTORY_SEPARATOR.'.*/', '$1', __FILE__);
 
    $this->getSources();
  }

  private function getSources () {
    $sPath = $this->sCurrPath.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'communicator'.DIRECTORY_SEPARATOR.'sources'.DIRECTORY_SEPARATOR;
    
    if ($hDir = opendir($sPath)) {
        while (false !== ($sDir = readdir($hDir))) {
            if ($sDir != "." && $sDir != ".." && is_dir($sPath.$sDir)) {
                echo "$sDir\n";
                include_once($sPath.$sDir);
                switch ($sDir) {
                  case 'Azymut':
                    break;

                  default:
                    break;
                }
            }
        }
        closedir($hDir);
    }
    
  }
}

?>
