<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-12 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders;

/**
 * Logowanie zmian
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class logger {
  
  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;
  
  /**
   * 
   * @param \DatabaseManager $pDbMgr
   */
  public function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return array
   */
  public function getLogsOrders($iOrderId, $aAddSQL = 'ORDER BY id DESC') {
    
    $sSql = 'SELECT DISTINCT OCH.id, OCH.created, CONCAT(U.name, " ", U.surname) AS created_by, OI.name as item_name, OCH.content, OCH.backtrace
             FROM orders_change_history AS OCH
             LEFT JOIN users AS U
              ON U.id = OCH.user_id
             LEFT JOIN orders_items AS OI
              ON OI.id = OCH.order_item_id
             WHERE OCH.order_id = '.$iOrderId.'
             '.$aAddSQL;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param int $iUserId
   * @param array $aChanges
   * @param int $iOrderItemId
   * @return boolean
   */
  public function addLog($iOrderId, $iUserId, $aChanges, $iOrderItemId = 'NULL') {
    
    $aValues = array(
        'order_id' => $iOrderId,
        'order_item_id' => $iOrderItemId,
        'user_id' => $iUserId,
        'created' => 'NOW()',
        'content' => serialize($aChanges),
        'backtrace' => base64_encode(json_encode(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), JSON_UNESCAPED_UNICODE))
    );
    if ($this->pDbMgr->Insert('profit24', 'orders_change_history', $aValues) === false) {
      return false;
    }
    return true;
  }
  
  /**
   * 
   * @param int $iId
   * @return array
   */
  public function getLogDetails($iId) {
    
    $sSql = 'SELECT * 
             FROM orders_change_history
             WHERE id = '.$iId;
    $aOSH = $this->pDbMgr->GetAll('profit24', $sSql);
    $aOSH['changes'] = $this->getLogChangeArray(unserialize($aOSH['content']));
    return $aOSH;
  }
  
  /**
   * 
   * @param array $aLangArray
   * @param string $aChangesItem
   * @return string HTML
   */
  public function formateLogContentByLangArray($aLangArray, $aChangesItem) {
    
    $aNewArray = array();
    foreach ($aChangesItem as $sKey => $sValue) {
      // nic nie zmieniamy
      if (is_array($sValue)) {
        $sTranslatedColName = $this->findLang($aLangArray, $sKey);
        $aNewArray[$sKey] = $sTranslatedColName.':';
        foreach ($sValue as $sNewKey => $sNewValue) {
          $sTranslateName = $this->findLang($aLangArray, $sNewKey);
          $aNewArray[" &nbsp; &nbsp; ".$sTranslateName] = $sNewValue;
        }
      } else {
        $sTranslateName = $this->findLang($aLangArray, $sKey);
        $aNewArray[$sTranslateName] = $sValue;
      }
    }
    $sReturnHTML = $this->getStringToView($aNewArray);
    return $sReturnHTML;
  }
  
  /**
   * 
   * @param arrray $aData
   * @return string
   */
  private function getStringToView($aData) {
    $sHTML = '';
    foreach ($aData as $sKey => $sValue) {
      $sHTML .= '<div style="padding:2px;">'.$sKey .' = <b>'.$sValue.'</b></div> ';
    }
    return $sHTML;
  }
  
  /**
   * 
   * @param array $aLangArray
   * @param string $sLangKey
   * @return string
   */
  private function findLang($aLangArray, $sLangKey) {
    
    foreach ($aLangArray as $sLangType => $aLangs) {
      if (isset($aLangs[$sLangKey]) && !empty($aLangs[$sLangKey])) {
        return $aLangs[$sLangKey];
      }
    }
    return $sLangKey;
  }
  
  
  /**
   * 
   * @param string $sContent
   * @return array
   */
  public function getLogChangeArray($sContent) {
    return unserialize($sContent);
  }
}
