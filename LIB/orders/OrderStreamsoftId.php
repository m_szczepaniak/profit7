<?php

namespace orders;

class OrderStreamsoftId
{
    public function fixOrdersStreamsoftId()
    {
        global $pDbMgr;

        $additionalCondition = '';

        $sql = '
            SELECT OUA.*
            FROM orders AS O
            JOIN orders_users_addresses AS OUA ON OUA.order_id = O.id AND (OUA.streamsoft_id IS NOT NULL AND OUA.streamsoft_id != 0 AND OUA.user_adress_id IS NOT NULL AND OUA.user_adress_id != "")
            WHERE
              (
                O.payment_type = "bank_14days" OR
                (O.second_payment_enabled = "1" AND O.second_payment_type = "bank_14days")
              )
            AND (OUA.address_type = "0" OR OUA.address_type = "2")
            '.$additionalCondition.'
            ORDER BY OUA.id DESC
        ';

        $result = $pDbMgr->GetAll('profit24', $sql);

        $i = 0;
        foreach ($result as $element) {

            $iUOAId = $element['user_adress_id'];
            $userAddress = $pDbMgr->GetRow('profit24', "SELECT * FROM users_accounts_address WHERE id = $iUOAId AND (streamsoft_id is null OR streamsoft_id = '' OR streamsoft_id = 0)");

            if (!is_array($userAddress)) {
                var_dump('error');
                continue;
            }

            $orderUserAddressesValues = $this->prepareValuesToCompare($element);
            $userAddressValues = $this->prepareValuesToCompare($userAddress);

            $md51 = md5(json_encode($orderUserAddressesValues));
            $md52 = md5(json_encode($userAddressValues));

            if ($md51 != $md52) {
                var_dump('error');
                continue;
            }

            $updateRes = $pDbMgr->Update('profit24', 'users_accounts_address', ['streamsoft_id' => $element['streamsoft_id'], 'address_checksum' => $md52], ' id='.$iUOAId);
            var_dump($i += 1);
        }
    }

    /**
     * @param array $values
     * @param array $excludeColumns
     * @return array
     */
    private function prepareValuesToCompare(array $values, array $excludeColumns = [])
    {
        $res = [
            'address_type' => $values['address_type'],
            'is_company' => $values['is_company'],
            'company' => $values['company'],
            'name' => $values['name'],
            'surname' => $values['surname'],
            'street' => $values['street'],
            'number' => $values['number'],
            'number2' => $values['number2'],
            'postal' => $values['postal'],
            'city' => $values['city'],
            'nip' => $values['nip'],
            'phone' => $values['phone'],
            'default_invoice' => $values['default_invoice'],
            'default_transport' => $values['default_transport'],
        ];

        if (!empty($excludeColumns)) {

            foreach($excludeColumns as $columntoExclude) {
                unset($res[$columntoExclude]);
            }
        }

        foreach($res as &$elem){

            if ($elem === null || $elem =='' || $elem == 'NULL' || $elem == 'null') {
                $elem = null;
            }

            if (false !== filter_var($elem, FILTER_VALIDATE_INT)){
                $elem = filter_var($elem, FILTER_VALIDATE_INT);
            }
        }

        return $res;
    }

    /**
     * @param array $newValues
     * @param array $currentValues
     * @param array $excludeColumns
     * @return bool
     */
    public function compareOrderArrays(array $newValues, array $currentValues, array $excludeColumns)
    {
        $comparedNewValues = $this->prepareValuesToCompare($newValues, $excludeColumns);
        $comparedCurrentValues = $this->prepareValuesToCompare($currentValues, $excludeColumns);

        $areTheSame = false;

        if (md5(json_encode($comparedCurrentValues)) == md5(json_encode($comparedNewValues))) {
            $areTheSame = true;
        }

        return $areTheSame;
    }
}