<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-24
 * Time: 10:47
 */

namespace orders;


use orders\MarkUpCookie\AllUsers;
use orders\MarkUpCookie\BuyBox;

class MarkUpCookie
{

    private $classesOfMarkUp;

    /**
     *
     */
    public function __construct() {
        $this->classesOfMarkUp[] = new BuyBox();
        $this->classesOfMarkUp[] = new AllUsers();
    }


    /**
     *
     */
    public function setAdditionalMarkupCookie()
    {
        foreach ($this->classesOfMarkUp as $obj) {
            $obj->setAdditionalMarkupCookie();
        }
    }

    public function isMarkUpActive() {
        if (!empty($this->classesOfMarkUp)) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    private function isMarkUpActiveUsage()
    {
        foreach ($this->classesOfMarkUp as $key => $obj) {
            if ($obj->isMarkUpActive() === true) {
                return true;
            } else {
                unset($this->classesOfMarkUp[$key]);
            }
        }

        return false;
    }

    /**
     * @param $aTarrif
     * @param $iDiscount
     * @return mixed
     */
    public function getMarkup($fPriceBrutto, $aTarrif, $iDiscount, $bCheckCookie = true)
    {
        $this->isMarkUpActiveUsage();
        foreach ($this->classesOfMarkUp as $obj) {

            $aTarrif = $obj->getMarkup($fPriceBrutto, $aTarrif, $iDiscount, $bCheckCookie);
            if ($obj instanceof BuyBox && $obj->bIsUsed === true) {
                return $aTarrif;
            }
        }
        return $aTarrif;
    }

    /**
     * @return bool
     */
    public function isAnyMarkUpActiveForCache() {

        foreach ($this->classesOfMarkUp as $obj) {
            if ($obj->isAnyMarkUpActiveForCache() === true) {
                return true;
            }
        }
        return false;
    }
}