<?php
/**
 * Klasa dokonująca zmian w zamówieniu, wywoływana o 16
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-21 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders;
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/orders';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

class autoChangeStatus {
  var $aScripts = array(
      '\orders\auto_change_status\autoDeadline'
  );
  var $bTestMode = FALSE;

  function __construct() {
    global $pDbMgr;
    
    foreach ($this->aScripts as $sScript) {
      $oAutoObj = new $sScript($this->bTestMode, $pDbMgr);
      $this->_proceedAutoChangeStatus($oAutoObj);
    }
  }
  
  
  /**
   * Metoda automatycznie zmienia status zamówień
   * 
   * @param object $oScript
   */
  private function _proceedAutoChangeStatus(auto_change_status\iAutoChangeStatus $oAutoObj) {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
    
    $this->pDbMgr->BeginTransaction('profit24');
    // pojawiła się zapowiedź w źródle - zmiana statusu wewn. zamówień z 'zapowiedzi' na '---'
    $aOrdersIds = $oAutoObj->getOrdersItemsDataToUpdate();
    $aInformData = $oAutoObj->UpdateOrdersItems($aOrdersIds);
    
    if ($aInformData === false) {
      echo 'błąd zmiany statusów zamówień';
      $this->pDbMgr->RollbackTransaction('profit24');
    } else {
      $oAutoObj->informAboutStatusChanged($aInformData);
      $this->pDbMgr->CommitTransaction('profit24');
    }
  }
}

$oAutoChangeStatus = new autoChangeStatus();

