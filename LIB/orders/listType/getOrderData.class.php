<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType;

use DatabaseManager;
use LIB\EntityManager\Entites\Magazine;

/**
 * Description of getOrderData
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class getOrderData implements OrderDataInterface {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  private $aOrdersRequiredCols = array('O.order_status', 'O.linked_order', 'O.order_number, O.transport_id, O.print_ready_list', ' O.point_of_receipt', 'O.order_on_list_locked');
  private $aProductsRequiredCols = array('P.isbn_plain', 'P.isbn_10', 'P.isbn_13', 'P.ean_13');
  private $aOrdersItemsRequiredCols = array(' CONCAT(OI.order_id, ",", OI.id) AS ident ', 'OI.id', 'OI.get_ready_list', 'OI.quantity', 'OI.product_id', 
      'OI.quantity', 'OI.deleted', 'OI.packet', 'OI.item_type', 'OI.status', 'OI.source', 'OI.name', 'OI.weight',
        '(
          SELECT PEC.page_id 
          FROM products_extra_categories AS PEC 
          JOIN menus_items_books AS PI
           ON PI.page_id = PEC.page_id
          WHERE PEC.product_id = OI.product_id 
          LIMIT 1
      ) AS is_add_books_cat 
      ',
      'OI.vat'
  );
  private $aSelectOrdersItemsCols = array(
      'OI.weight', 'OI.authors', 'OI.publisher', 'OI.publication_year', 'OI.edition',
      '(SELECT CONCAT(directory, "|", photo) FROM products_images AS PI WHERE PI.product_id = OI.product_id LIMIT 1) AS photo'
  );
  /**
   * @var
   */
  private $destinationMagazineDeficiencies;

  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function __construct($pDbMgr, $destinationMagazineDeficiencies = Magazine::TYPE_HIGH_STOCK_SUPPLIES) {
    
    $this->pDbMgr = $pDbMgr;
    $this->destinationMagazineDeficiencies = $destinationMagazineDeficiencies;
  }
  
  /**
   * 
   * @param array $aTransportId
   * @param date $dExpectedShipmentDate
   * @param array $aOrdersIds
   * @return string
   */
  private function getWhereSQL($aTransportId, $dExpectedShipmentDate, $aOrdersIds) {
    $sWhereSQL = '';
    if (!empty($aTransportId) && is_array($aTransportId)) {
      $sWhereSQL .= '
       AND (
          O.transport_id IN ('.implode(', ', $aTransportId).')
        ) ';
    }
    
    if ($dExpectedShipmentDate != '' && $dExpectedShipmentDate != '00-00-0000') {
      $dFormedExpectedDate = preg_replace('/(\d{2})-(\d{2})-(\d{4})/', '$3-$2-$1', $dExpectedShipmentDate);
      $sWhereSQL .= ' AND O.shipment_date_expected <= "'.$dFormedExpectedDate.'" ';
    }
    
    if (!empty($aOrdersIds) && is_array($aOrdersIds)) {
      $sWhereSQL .= '
        AND (
          O.id IN ('.implode(', ', $aOrdersIds).')
        ) ';
    }
    return $sWhereSQL;
  }
  
  /**
   * 
   * @param array $aOrdersItemsRequiredCols
   * @param bool $bGetFromStock
   * @return array
   */
  private function setGetOrderColumnsByStock($aOrdersItemsRequiredCols, $bGetFromTrain) {

    if ($_SERVER['user']['name'] == 'agolba') {
     var_dump($this->destinationMagazineDeficiencies);
    }
    if ($bGetFromTrain === true) {

      if ($this->destinationMagazineDeficiencies == Magazine::TYPE_HIGH_STOCK_SUPPLIES) {
        $sSelectSqlLocation = '(SELECT IF(profit_g_location != "" AND profit_x_location != "", CONCAT(profit_g_location, " ", profit_x_location), IF (profit_g_location != "", profit_g_location, profit_x_location)) FROM products_stock AS PS WHERE PS.id = OI.product_id)';
      } else {
        $sSelectSqlLocation = '(SELECT IF(profit_x_location != "" AND profit_g_location != "", CONCAT(profit_x_location, " ", profit_g_location), IF (profit_x_location != "", profit_x_location, profit_g_location)) FROM products_stock AS PS WHERE PS.id = OI.product_id)';
      }


$sSelectSql = '
(
  IF (
    OI.source = "51" AND OI.status = "3",
    '.$sSelectSqlLocation.'
    ,
    IF (
      OI.source = "51" AND OI.status = "4", 
      IF (
        (
          SELECT OSH.pack_number
          FROM orders_send_history AS OSH
          JOIN orders_send_history_items AS OSHI
            ON OSH.id = OSHI.send_history_id
          WHERE OSHI.item_id = OI.id AND
                OSH.source IN ("33", "34")
          ORDER BY OSH.id DESC
          LIMIT 1
         ) IS NOT NULL
        ,
        (
          SELECT OSH.pack_number
          FROM orders_send_history AS OSH
          JOIN orders_send_history_items AS OSHI
            ON OSH.id = OSHI.send_history_id
          WHERE OSHI.item_id = OI.id AND
                OSH.source IN ("33", "34")
          ORDER BY OSH.id DESC
          LIMIT 1
         ),
        (
          SELECT package_number
          FROM orders_items_lists AS OIL
          JOIN orders_items_lists_items AS OILI
            ON OILI.orders_items_lists_id = OIL.id
          WHERE OILI.orders_items_id = OI.id AND package_number LIKE "20%"
          ORDER BY OIL.id DESC
          LIMIT 1
        )
       )
    ,
      (
        SELECT OSH.pack_number
        FROM orders_send_history AS OSH
        JOIN orders_send_history_items AS OSHI
          ON OSH.id = OSHI.send_history_id
        WHERE OSHI.item_id = OI.id 
        ORDER BY OSH.id DESC
        LIMIT 1
      )
    )
  )
) AS pack_number';
    } else {
      if ($this->destinationMagazineDeficiencies == Magazine::TYPE_HIGH_STOCK_SUPPLIES) {
        $sSelectSql = '(SELECT IF(profit_g_location != "" AND profit_x_location != "", CONCAT(profit_g_location, " ", profit_x_location), IF (profit_g_location != "", profit_g_location, profit_x_location)) FROM products_stock AS PS WHERE PS.id = OI.product_id) AS pack_number';
      } else {
        $sSelectSql = '(SELECT IF(profit_x_location != "" AND profit_g_location != "", CONCAT(profit_x_location, " ", profit_g_location), IF (profit_x_location != "", profit_x_location, profit_g_location)) FROM products_stock AS PS WHERE PS.id = OI.product_id) AS pack_number';
      }
    }
    $aOrdersItemsRequiredCols[] = $sSelectSql;
    return $aOrdersItemsRequiredCols;
  }
  
  /**
   * 
   * @param bool $bGetFromTrain
   * @param date $dExpectedShipmentDate
   * @param array $aTransportId
   * @param array $aOrdersIds
   * @return array
   */
  public function getOrdersItemsListData($bGetFromTrain = false, $dExpectedShipmentDate = '', $aTransportId = array(), $aOrdersIds = array(), $bAddShowColumns = false, $aFilterProducts = []) {
    
    $this->aOrdersItemsRequiredCols = $this->setGetOrderColumnsByStock($this->aOrdersItemsRequiredCols, $bGetFromTrain);
    $sWhereSQL = $this->getWhereSQL($aTransportId, $dExpectedShipmentDate, $aOrdersIds);

      $sFilter = '';
      if (!empty($aFilterProducts)) {
            $sFilter = ' AND OI.product_id IN ('.implode(', ', $aFilterProducts).')';
      }

    $sSql = 'SELECT O.id as oid, O.id AS order_id, 
              O.magazine_get_ready_list_VIP, 
              O.shipment_date_expected, 
              IF(O.payment_type = "postal_fee", "0", "1") as payment_type_order_by_asc,
              O.website_id, '.($bAddShowColumns === true ? implode(', ', $this->aSelectOrdersItemsCols).', ' : '').implode(', ', $this->aOrdersItemsRequiredCols).', '.implode(', ', $this->aOrdersRequiredCols).', '.implode(', ', $this->aProductsRequiredCols).'
             FROM orders_items AS OI
             JOIN orders AS O
              ON OI.order_id = O.id
             LEFT JOIN products AS P
              ON P.id = OI.product_id
             WHERE 
              OI.deleted = "0"
              '.$sFilter.'
              AND
              (
               SELECT id 
               FROM orders_items AS MOI 
               WHERE 
               MOI.order_id = O.id
               AND (MOI.item_type = "I" OR MOI.item_type = "P")
               AND (MOI.weight IS NULL OR MOI.weight = "0.00")
               AND MOI.deleted = "0"
               LIMIT 1
              ) IS NULL
              
              AND (OI.item_type = "I" OR OI.item_type = "P")
              AND OI.packet = "0"
              AND 
              (
                (
                  (
                    O.payment_type = "bank_transfer" OR
                    O.payment_type = "platnoscipl" OR
                    O.payment_type = "card"
                  )
                  AND O.to_pay <= O.paid_amount
                )
                OR
                (
                  O.payment_type = "postal_fee" OR
                  O.payment_type = "bank_14days"
                )
                OR
                (
                  O.second_payment_type = "postal_fee" OR
                  O.second_payment_type = "bank_14days"
                )
              )
              AND 
              (
                O.order_status = "1" OR
                O.order_status = "2" 
              )
              AND 
              (
                O.remarks = ""
                OR O.remarks IS NULL 
                OR O.remarks_read_1 IS NOT NULL
              )
              AND O.print_ready_list = "0"
              AND 
              ( O.send_date = "0000-00-00" OR (O.send_date <> "0000-00-00" AND O.send_date < NOW()) )
              '.$sWhereSQL.'
             ORDER BY O.magazine_get_ready_list_VIP DESC, O.shipment_date_expected ASC, IF(O.payment_type = "postal_fee", "1", "0") DESC, O.id ASC';
    $aReturn = $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);
    return $aReturn;
  }
  
  /**
   * 
   * @param int $iILId
   * @param bool $bGetFromTrain
   * @return array
   */
  public function getOrdersItemsListDataByOILId($iILId, $bGetFromTrain) {
    
    $this->aOrdersItemsRequiredCols = $this->setGetOrderColumnsByStock($this->aOrdersItemsRequiredCols, $bGetFromTrain);
    
    $sSql = 'SELECT O.id as oid, O.id AS order_id, OILI.id AS OILI_ID, '.implode(', ', $this->aSelectOrdersItemsCols).', '.implode(', ', $this->aOrdersItemsRequiredCols).', '.implode(', ', $this->aOrdersRequiredCols).', '.implode(', ', $this->aProductsRequiredCols).'
             FROM orders_items AS OI
             JOIN orders AS O
              ON OI.order_id = O.id
             JOIN orders_items_lists_items AS OILI
              ON OILI.orders_items_id = OI.id AND OILI.orders_items_lists_id = '.$iILId.'
             LEFT JOIN products AS P
              ON P.id = OI.product_id
             WHERE 
              OI.deleted = "0"
              AND (OI.item_type = "I" OR OI.item_type = "P")
              AND OI.packet = "0"
             ORDER BY O.magazine_get_ready_list_VIP DESC, O.shipment_date_expected ASC, IF(O.payment_type = "postal_fee", "1", "0") DESC, O.id ASC';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return array
   */
  public function getOrderListData($iOrderId) {
    
    $sSql = 'SELECT '.implode(', ', $this->aOrdersRequiredCols).'
             FROM orders 
             WHERE id = '.$iOrderId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }

  /**
   * 
   * @param int $iOrderId
   * @return array
   */
  public function getOrderItemsListData($iOrderId) {
    
    $sSql = 'SELECT '.implode(', ', $this->aOrdersItemsRequiredCols).'
             FROM orders_items
             WHERE order_id = '.$iOrderId;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
}
