<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType;

use DatabaseManager;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\SellPredict;

/**
 * Description of getOrderData
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class getOrderDataZD {

    /**
     *
     * @var DatabaseManager
     */
    private $pDbMgr;

    // !!! TUTAJ WOGOLE NIE MOWIMY O ZAMOWIENIACH ... TROCHE ZMIENIA NAM TO PODEJSCIE DO ZBIERANIA JAKO TAKIEGO...



    private $aOrdersRequiredCols = array('P.id');
    private $aProductsRequiredCols = array('P.isbn_plain', 'P.isbn_10', 'P.isbn_13', 'P.ean_13');
    private $aOrdersItemsRequiredCols = array(' CONCAT(MRI.id) AS ident ', 'MRI.product_id',
        '(MRI.quantity - MRI.confirmed_quantity) AS quantity ', 'P.name', 'P.weight',
        '(
          SELECT PEC.page_id 
          FROM products_extra_categories AS PEC 
          JOIN menus_items_books AS PI
           ON PI.page_id = PEC.page_id
          WHERE PEC.product_id = MRI.product_id 
          LIMIT 1
      ) AS is_add_books_cat 
      ',
        'P.vat'
    );
    private $aSelectOrdersItemsCols = array(
        'P.weight', '" " AS authors', '" " AS publisher', ' " " AS publication_year', '" " AS edition',
        '(SELECT CONCAT(directory, "|", photo) FROM products_images AS PI WHERE PI.product_id = MRI.product_id LIMIT 1) AS photo'
    );

    /**
     *
     * @param DatabaseManager $pDbMgr
     */
    public function __construct($pDbMgr) {

        $this->pDbMgr = $pDbMgr;
    }

    /**
     *
     * @param array $aTransportId
     * @param date $dExpectedShipmentDate
     * @param array $aOrdersIds
     * @return string
     */
    private function getWhereSQL($aTransportId, $dExpectedShipmentDate, $aOrdersIds) {
        $sWhereSQL = '';
        if (!empty($aTransportId) && is_array($aTransportId)) {
            $sWhereSQL .= '
       AND (
          O.transport_id IN ('.implode(', ', $aTransportId).')
        ) ';
        }

        if ($dExpectedShipmentDate != '' && $dExpectedShipmentDate != '00-00-0000') {
            $dFormedExpectedDate = preg_replace('/(\d{2})-(\d{2})-(\d{4})/', '$3-$2-$1', $dExpectedShipmentDate);
            $sWhereSQL .= ' AND O.shipment_date_expected <= "'.$dFormedExpectedDate.'" ';
        }

        if (!empty($aOrdersIds) && is_array($aOrdersIds)) {
            $sWhereSQL .= '
        AND (
          O.id IN ('.implode(', ', $aOrdersIds).')
        ) ';
        }
        return $sWhereSQL;
    }

    /**
     *
     * @param array $aOrdersItemsRequiredCols
     * @param bool $bGetFromTrain
     * @return array
     */
    private function setGetOrderColumnsByStock($aOrdersItemsRequiredCols, $bGetFromTrain) {
        $sSelectSql = ' SL.container_id AS pack_number, SL.container_id ';
        $aOrdersItemsRequiredCols[] = $sSelectSql;
        return $aOrdersItemsRequiredCols;
    }

    /**
     *
     * @param bool $bGetFromTrain
     * @param string $magazine
     * @param bool $bAddShowColumns
     * @return array
     */
    public function getOrdersItemsListData($bGetFromTrain = false, $magazine = Magazine::TYPE_HIGH_STOCK_SUPPLIES, $bAddShowColumns = false) {

        $this->aOrdersItemsRequiredCols = $this->setGetOrderColumnsByStock($this->aOrdersItemsRequiredCols, $bGetFromTrain);

        $sSql = 'SELECT MRI.id AS KEY_TMP, MRI.id AS SP_id, PS.profit_g_act_stock, '.($bAddShowColumns === true ? implode(', ', $this->aSelectOrdersItemsCols).', ' : '').implode(', ', $this->aOrdersItemsRequiredCols).', '.implode(', ', $this->aOrdersRequiredCols).', '.implode(', ', $this->aProductsRequiredCols).'
             FROM magazine_return_items AS MRI
             JOIN stock_location_orders_items_lists_items AS SLO
                ON MRI.stock_location_orders_items_lists_items_id = SLO.id AND SLO.stock_location_magazine_type = "'.$magazine.'"
                    AND SLO.status = "1" 
             JOIN stock_location AS SL
                ON SLO.stock_location_id = SL.id
             JOIN products AS P
              ON P.id = MRI.product_id
             JOIN products_stock AS PS
              ON PS.id = MRI.product_id
             ORDER BY MRI.order_by';
        $aReturn = $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);
        return $aReturn;
    }


    /**
     *
     * @param int $iILId
     * @param bool $bGetFromTrain
     * @return array
     */
    public function getOrdersItemsListDataByOILId($magazineReturnId, $bGetFromTrain, $magazine) {

        $this->aOrdersItemsRequiredCols = $this->setGetOrderColumnsByStock($this->aOrdersItemsRequiredCols, $bGetFromTrain);

        $sSql = 'SELECT MRI.id AS KEY_TMP, MRI.id AS SL_id, PS.profit_g_act_stock, SLO.orders_items_lists_items_id AS OILI_ID, 
        '.
            implode(', ', $this->aOrdersItemsRequiredCols).', '.
            implode(', ', $this->aOrdersRequiredCols).', '.
            implode(', ', $this->aSelectOrdersItemsCols).', '.
            implode(', ', $this->aProductsRequiredCols)
            .'
             FROM magazine_return_items AS MRI
             JOIN stock_location_orders_items_lists_items AS SLO
                ON MRI.stock_location_orders_items_lists_items_id = SLO.id AND SLO.stock_location_magazine_type = "'.$magazine.'"
                AND SLO.status = "1"
             JOIN stock_location AS SL
                ON SLO.stock_location_id = SL.id
             JOIN products AS P
              ON P.id = MRI.product_id
             JOIN products_stock AS PS
              ON PS.id = MRI.product_id
             WHERE MRI.magazine_return_id = '.$magazineReturnId.'
             ORDER BY MRI.id';
        $aReturn = $this->pDbMgr->GetAll('profit24', $sSql);
        return $aReturn;
    }

    /**
     *
     * @param int $iOrderId
     * @return array
     */
    public function getOrderListData($iOrderId) {

        $sSql = 'SELECT '.implode(', ', $this->aOrdersRequiredCols).'
             FROM orders 
             WHERE id = '.$iOrderId;
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }

    /**
     *
     * @param int $iOrderId
     * @return array
     */
    public function getOrderItemsListData($iOrderId) {

        $sSql = 'SELECT '.implode(', ', $this->aOrdersItemsRequiredCols).'
             FROM orders_items
             WHERE order_id = '.$iOrderId;
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }
}
