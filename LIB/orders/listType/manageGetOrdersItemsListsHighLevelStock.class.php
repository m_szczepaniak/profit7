<?php

namespace LIB\orders\listType;


/**
 * Description of manageGetOrdersItemsListsHighLevelStock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class manageGetOrdersItemsListsHighLevelStock extends manageGetOrdersItemsList {

  /**
   * manageGetOrdersItemsListsHighLevelStock constructor.
   * @param $pDbMgr
   * @param $destinationMagazineDeficiencies
   */
  public function __construct($pDbMgr, $destinationMagazineDeficiencies)
    {
        parent::__construct($pDbMgr, $destinationMagazineDeficiencies);

        $dataProvider = new getOrderDataHighLevelStock($pDbMgr, $destinationMagazineDeficiencies);
        $this->setDataProvider($dataProvider);
    }

    /**
     *
     * @param array $aOrderItemsList
     * @return array
     */
    protected function getOrdersItems($aOrderItemsList)
    {

        foreach ($aOrderItemsList as $iKey => $aOrderItem) {
            $this->oListTypes->setOrder($aOrderItem[0]);
            $this->oListTypes->setOrderItems($aOrderItem);
            $this->oListTypes->setDb($this->pDbMgr);

            $aOrdersItems[$iKey] = $aOrderItem;
            /*
//            if ($this->oListTypes->checkOrderListType() === true) {
                if ($this->bGetFromTrain === TRUE) {
                    $aTrain = $this->oListTypes->getOrderItemsTran();
                    if (!empty($aTrain)) {
                        $aOrdersItems[$iKey] = $aTrain;
                    }
                } else {
                    $aStock = $this->oListTypes->getOrderItemsStock();
                    if (!empty($aStock)) {
                        $aOrdersItems[$iKey] = $aStock;
                    }
                }
//            }
            */
        }
        return $aOrdersItems;
    }

    /**
     * @param string $dExpectedShipmentDate
     * @param array $aTransportId
     * @param bool $bAddShowColumns
     * @return array|mixed
     */
    public function getOrdersItemsListsType($dExpectedShipmentDate = '', $aTransportId = array(), $bAddShowColumns = false)
    {
        $aOrdersItemsFiltered = array();

        $aOrderItemsList = $this->oGetOrderData->getOrdersItemsListData($this->getGetFromTrain(), $dExpectedShipmentDate, $aTransportId, array(), $bAddShowColumns);
        $aOrdersItems = $this->getOrdersItems($aOrderItemsList);
        if (!empty($aOrdersItems)) {
            $aOrdersItemsFiltered = $this->applyFilters($aOrdersItems);
        }
        $aOrdersItemsFiltered = $this->clearEmptyOrders($aOrdersItemsFiltered);
        $this->dumpAdminsData($aOrdersItemsFiltered);
        return $aOrdersItemsFiltered;
    }
}