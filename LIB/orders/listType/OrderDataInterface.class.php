<?php

namespace LIB\orders\listType;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 14.06.17
 * Time: 10:40
 */
interface OrderDataInterface
{

    public function getOrdersItemsListData($bGetFromTrain = false, $dExpectedShipmentDate = '', $aTransportId = array(), $aOrdersIds = array(), $bAddShowColumns = false);

    public function getOrdersItemsListDataByOILId($iILId, $bGetFromTrain);

    public function getOrderListData($iOrderId);

    public function getOrderItemsListData($iOrderId);

}