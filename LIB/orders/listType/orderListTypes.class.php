<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace LIB\orders\listType;

use DatabaseManager;
use Exception;
use LIB\orders\listType\types\iListTypes;
use LIB\orders\listType\types\typesAbstract;
/**
 * Description of listTypes
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class orderListTypes {

  private $bGetFromTrain;
  private $mListType;
  private $aOrdersItemsTrain;
  private $aOrdersItemsStock;
  private $orderTypes;
  public function __construct($mListType, $bGetFromTrain) {
  
    $this->mListType = $mListType;
    $this->bGetFromTrain = $bGetFromTrain;
    $this->orderTypes = $this->getOrderTypes($this->mListType);
  }
    
  /**
   * 
   * @param array $aOrder
   */
  public function setOrder($aOrder){
    $this->aOrder = $aOrder;
  }

  /**
   * 
   * @param array $aOrderItems
   */
  public function setOrderItems($aOrderItems) {
    $this->aOrderItems = $aOrderItems;
  }
  
  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function setDb(DatabaseManager $pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @return array
   * @throws Exception
   */
  public function getOrderListType() {
    if (empty($this->aOrder) || empty($this->aOrderItems)) {
      throw new Exception(_('Brak danych do sprawdzenia typu listy zamówienia'));
    }
    
    foreach ($this->orderTypes as $orderListType) {
      $orderListType->setOrder($this->aOrder);
      $orderListType->setOrderItems($this->aOrderItems);
      $orderListType->setFirstCheckType($this->bFirstCheckType);
      if ($orderListType->checkType() === true) {
        return $orderListType->getType();
      }
    }
    return false;
  }
  
  
  /**
   * 
   * @return array
   * @throws Exception
   */
  public function checkOrderListType() {
    $this->aOrdersItemsStock = array();
    $this->aOrdersItemsTrain = array();
    
    if (empty($this->aOrder) || empty($this->aOrderItems)) {
      throw new Exception(_('Brak danych do sprawdzenia typu listy zamówienia'));
    }
    foreach ($this->orderTypes as $iKey => $orderListType) {
      $orderListType->setOrder($this->aOrder);
      $orderListType->setOrderItems($this->aOrderItems);
      $orderListType->setDb($this->pDbMgr);
      
      if ($orderListType->checkType() === TRUE) {
        if ($this->bGetFromTrain === TRUE) {
          $aTrain = $orderListType->getOrderItemsTran();
          if (!empty($aTrain)) {
            if (!empty($this->aOrdersItemsTrain)) {
              $this->aOrdersItemsTrain = array_merge($this->aOrdersItemsTrain, $aTrain);
            } else {
              $this->aOrdersItemsTrain = $aTrain;
            }
          }
        } else {
          $aStock = $orderListType->getOrderItemsStock();
          if (!empty($aStock)) {
            if (!empty($this->aOrdersItemsStock)) {
              $this->aOrdersItemsStock = array_merge($this->aOrdersItemsStock, $aStock);
            } else {
              $this->aOrdersItemsStock = $aStock;
            }
          }
        }
        return true;
      }
    }
    
    return false;
  }


  /**
   * @param $rows
   * @param $sMagazineType
   * @return mixed
   */
  private function markAllAsMagazineType($rows, $sMagazineType) {

    foreach ($rows as &$row) {
      $row['magazine_type'] = $sMagazineType;
    }
    return $rows;
  }
  
  /**
   * 
   * @param typesAbstract|iListTypes $orderListType
   * @param mixed $mListType
   * @return boolean
   */
  private function checkListyType(typesAbstract $orderListType, $mListType) {
    $bCheckType = FALSE;
    if (!empty($mListType) && is_array($mListType)) {
      if ($orderListType->checkListsTypes($mListType)) {
        $bCheckType = TRUE;
      }
    } else {
      if ($orderListType->checkListType($mListType)) {
        $bCheckType = TRUE;
      }
    }
    return $bCheckType;
  }
  
  /**
   * 
   * @return array
   */
  public function getOrderItemsTran() {
    return $this->aOrdersItemsTrain;
  }
  
  /**
   * 
   * @return array
   */
  public function getOrderItemsStock() {
    return $this->aOrdersItemsStock;
  }
  
  /**
   * 
   * @param mixed $mListType
   * @return iListTypes[]
   */
  private function getOrderTypes($mListType) {

    $objects = array();
    if ($handle = opendir(__DIR__.'/types')) {
      while (false !== ($entry = readdir($handle))) {
        if (stristr($entry, '.class.php')) {
          $sClassName = str_replace('.class.php', '', $entry);
          $sClass = 'LIB\orders\listType\types\\'.$sClassName;
          $oClass = new $sClass();
          
          if ($oClass instanceof iListTypes && $oClass instanceof typesAbstract) {
            if (!$this->checkListyType($oClass, $mListType)) {
              continue;
            }
            if ($this->bGetFromTrain === TRUE && $oClass->isGetFromTrain() === FALSE) {
              continue;
            }
            $oClass->setParentGetFromTrain($this->bGetFromTrain);
            $objects[] = $oClass;
          }
        }
      }
    }
    return $objects;
  }
  
}
