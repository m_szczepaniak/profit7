<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class quantity11_25 extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;
  const MIN_FILTER_ITEMS = 11;
  const MAX_FILTER_ITEMS = 25;
  const MAX_COUNT = 4;
  
  public function applyFilter() {
    
    $iCurrCount = 0;
    foreach ($this->aOrdersItems as $iKey => $aItem) {
      $iOrdersItemsQuantity = $this->sumQuantity($aItem);
      if ($iOrdersItemsQuantity >= self::MIN_FILTER_ITEMS && $iOrdersItemsQuantity <= self::MAX_FILTER_ITEMS) {
        if ($iCurrCount >= self::MAX_COUNT) {
          unset($this->aOrdersItems[$iKey]);
        } else {
          $iCurrCount++;
        }
      }
    }
  }
}
