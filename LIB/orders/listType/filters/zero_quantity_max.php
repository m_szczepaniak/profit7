<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class zero_quantity_max extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;

    /**
     * @var
     */
  protected $maxQuantity = 31;

    public function setMaxQuantity($maxQuantity) {
        $this->maxQuantity = $maxQuantity;
    }

  public function applyFilter() {

    foreach ($this->aOrdersItems as $iKey => $aItem) {
      if ($this->sumQuantity($aItem) >= $this->maxQuantity) {
        unset($this->aOrdersItems[$iKey]);
      }
    }
  }
}
