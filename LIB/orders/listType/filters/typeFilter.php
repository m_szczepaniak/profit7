<?php
/**
 * Created by PhpStorm.
 * User: Arek
 * Date: 28.08.2017
 * Time: 10:27
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class typeFilter extends filtersAbstract
{
    public $type_filter = self::TYPE_FILTER_ITEMS;


    /**
     *
     * @var array
     */
    public $aOrdersItems;

    private $booksCategoriesIds;

    const TYPE_BOOKS = 'books';
    const TYPE_MIXED = 'mix';

    private $filterCollectingType;


    public function setFilterCollectingType($filterCollectingType)
    {
        if ($filterCollectingType != '') {
            $this->filterCollectingType = $filterCollectingType;
        } else {
            $this->filterCollectingType = self::TYPE_BOOKS;
        }
    }


    /**
     *
     * [order_id] => 942227
     * [website_id] => 5
     * [ident] => 942227,3296592
     * [id] => 3296592
     * [get_ready_list] => 0
     * [quantity] => 1
     * [product_id] => 922957
     * [deleted] => 0
     * [packet] => 0
     * [item_type] => I
     * [status] => 3
     * [source] => 51
     * [name] => Splendor
     * [weight] => 1.11
     * [pack_number] => 1509008050 1706002043
     * [order_status] => 1
     * [linked_order] => 0
     * [order_number] => 310717099717
     * [transport_id] => 1
     * [print_ready_list] => 0
     * [point_of_receipt] =>
     * [order_on_list_locked] => 0
     * [isbn_plain] => 3558380023784
     * [isbn_10] =>
     * [isbn_13] =>
     * [ean_13] => 3558380023784
     */
    public function applyFilter()
    {
        $aVat = [5, 8];

        $this->newBooksArray = [];
        $this->newMixedArray = [];

        $booksPrefix = ['978', '900'];
        //'kalendarze', 'teatr.film.telewizja', 'audiobboki'

        if ($_SERVER) {

        }
        foreach ($this->aOrdersItems as $iKey => $aItems) {
            $bIsBook = true;
            foreach ($aItems as $iKeyItem => $aItem) {
                $prefixISBN = substr($aItem['isbn_plain'], 0, 3);
                if (false == in_array($prefixISBN, $booksPrefix) && false == in_array($aItem['vat'], $aVat) && intval($aItem['is_add_books_cat']) <= 0) {
                    $bIsBook = false;
                    break;
                }
            }

            if ($this->filterCollectingType == self::TYPE_BOOKS) {
                if (true === $bIsBook) {
                    $this->newBooksArray[$iKey] = $aItems;
                }
            }
            elseif ($this->filterCollectingType == self::TYPE_MIXED) {
                if (false === $bIsBook) {
                    $this->newMixedArray[$iKey] = $aItems;
                }
            }
        }

        if ($this->filterCollectingType == self::TYPE_BOOKS) {
            $this->aOrdersItems = $this->newBooksArray;
        }
        elseif ($this->filterCollectingType == self::TYPE_MIXED) {
            $this->aOrdersItems = $this->newMixedArray;
        }
    }
}
