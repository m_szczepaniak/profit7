<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class quantity22_30 extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;
  const MIN_FILTER_ITEMS = 22;
  const MAX_FILTER_ITEMS = 30;
  const MAX_COUNT = 2;
  
  public function applyFilter() {
    
    $iCurrCount = 0;
    foreach ($this->aOrdersItems as $iKey => $aItem) {
      $iOrdersItemsQuantity = $this->sumQuantity($aItem);
      if ($iOrdersItemsQuantity >= self::MIN_FILTER_ITEMS && $iOrdersItemsQuantity <= self::MAX_FILTER_ITEMS) {
        if ($iCurrCount >= self::MAX_COUNT) {
          unset($this->aOrdersItems[$iKey]);
        } else {
          $iCurrCount++;
        }
      }
    }
  }
}
