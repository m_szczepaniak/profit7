<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

use LIB\EntityManager\Entites\SellPredict;
use LIB\EntityManager\Entites\Magazine;
use LIB\orders_semaphore\magazineSemaphore;
use Memcached;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class checkMagazine extends filtersAbstract
{
    /**
     *
     * @var array
     */
    protected $aOrdersItems;
    protected $productsMagazineLocations;
    protected $bUnsetIFExists;
    protected $bUnsetIFNotExists;
    protected $bReserveItem;
    protected $forceReserveItems = false;
    protected $sellPredict;

    private $magazine;
    private $addSellPredict;

    public $type_filter = self::TYPE_FILTER_ITEMS;

    const MINIMUM_QUANTITY_CEILING = 10;


    public function __construct($pDbMgr)
    {
        parent::__construct($pDbMgr);
        $this->sellPredict = new SellPredict($pDbMgr);
        $this->addSellPredict = true;
    }


    /**
     * @param $magazine
     */
    public function setMagazine($magazine)
    {
        $this->magazine = $magazine;
    }

    public function forceReserveItems($forceReserveItems)
    {
        $this->forceReserveItems = $forceReserveItems;
    }

    public function addSellPredict($addSellPredict = true) {
        $this->addSellPredict = $addSellPredict;
    }

    public function unsetIFExists($bUnsetIFExists = false)
    {
        $this->bUnsetIFExists = $bUnsetIFExists;
    }

    public function unsetIFNotExists($bUnsetIFNotExists = true)
    {
        $this->bUnsetIFNotExists = $bUnsetIFNotExists;
    }

    public function reserveItem($bReserveItem)
    {
        $this->bReserveItem = $bReserveItem;
    }

    /**
     * @throws \Exception
     */
    public function applyFilter()
    {
//        if (false === $this->isLocked()) {
        $this->loadStockMagazineLocation();
        foreach ($this->aOrdersItems as $iSellPredictId => $orderItems) {
            $productMagazineStock = [];
            $bValidStockQuantity = true;

            foreach ($orderItems as $iKey => $orderItem) {
                // musi być to składowa odznaczona do Stocku
                if ($orderItem['source'] == '51' || $this->forceReserveItems === true) {

                    $resultReservedMagazineStock = [];
                    $sellPredictId = 0;
                    if (isset($orderItem['SP_id'])) {
                        $sellPredictId = $orderItem['SP_id'];
                    }

                    $resultValidate = $this->validateProductQuantityMagazineStock($orderItem['product_id'], $orderItem['quantity'],
                        $resultReservedMagazineStock, $orderItem['id'], $sellPredictId,
                        $orderItem['vat']
                        );
						if ($this->aOrdersItems[$iSellPredictId][$iKey]['quantity'] != $orderItem['quantity'] && $orderItem['quantity'] > 0) {
							$this->aOrdersItems[$iSellPredictId][$iKey]['quantity'] = $orderItem['quantity'];
						}
                    if (false === $resultValidate) {
                        $bValidStockQuantity = false;
                        // wyłączony BRAK !!!!!
                    } else {
                        // ustawiamy w razie przywracania
                        $productMagazineStock[$orderItem['product_id']] = $resultValidate;
                        if (true === $this->bReserveItem) {
                            $this->aOrdersItems[$iSellPredictId][$iKey]['magazine_stock_localizations_reserved'] = $resultReservedMagazineStock;
                        }
                    }
                }
            }
            // przywracamy zmiany
            if (false === $bValidStockQuantity) {
                foreach ($productMagazineStock as $productId => $rows) {
                    foreach ($rows as $iKey => $row) {
                        $this->productsMagazineLocations[$productId][$iKey] = $row;
                    }
                }
                if (true === $this->bUnsetIFNotExists) {
//                    if ($_SESSION['user']['name'] == 'agolba') {
//                        dump($this->aOrdersItems[$iSellPredictId]);
//                    }
                    unset($this->aOrdersItems[$iSellPredictId]);
                }
            } else {
                if (true === $this->bUnsetIFExists) {
//                    if ($_SESSION['user']['name'] == 'agolba') {
//                        dump($this->aOrdersItems[$iSellPredictId]);
//                    }
                    unset($this->aOrdersItems[$iSellPredictId]);
                }
            }

        }
//        } else {
//            throw new \Exception(_('Lista jest pobierana przez inną osobę, spróbuj ponownie za chwilę.'));
//        }

    }


    /**
     * @param $sellPredict
     * @param $vat
     * @param $diffAvailable
     * @return bool
     */
    private function checkModifiSellPredictQuantity($sellPredict, $vat, $diffAvailable) {

        if ($sellPredict === true && $vat == '5') {

            if ($diffAvailable < self::MINIMUM_QUANTITY_CEILING &&
                $diffAvailable > 0
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $sellPredictId
     * @param $diffAvailable
     * @return bool
     */
    private function modifiSellPredictQuantity($sellPredictId, $diffAvailable) {

        // dodajemy ilosć dopełniającą
        $sellPredictRow = $this->pDbMgr->getTableRow('sell_predict', $sellPredictId, ['quantity']);
        if (false === $this->sellPredict->updateQuantity($sellPredictId, ($sellPredictRow['quantity'] + $diffAvailable))) {
            return false;
        }
        return true;
    }


    /**
     * @param $productId
     * @param $quantity
     * @return bool
     */
    private function validateProductQuantityMagazineStock($productId, &$quantity, &$resultReservedMagazineStock, $iOrderItemId, $sellPredictId = 0, $vat = 0)
    {
        $sellPredict = false;
        if ($sellPredictId > 0 && $this->magazine === Magazine::TYPE_HIGH_STOCK_SUPPLIES && $this->forceReserveItems === true) {
            // wysokie składowanie
            $sellPredict = true;
        }
        $productMagazineStock = [];
        if (isset($this->productsMagazineLocations[$productId])) {
            $productMagazineLocations = &$this->productsMagazineLocations[$productId];

            $curQuantity = $quantity;
            $productMagazineLocationsCOPY = $productMagazineLocations;
            foreach ($productMagazineLocations as $iRow => &$productMagazineLocation) {
                if ($productMagazineLocation['available'] >= $curQuantity) {
                    // minimum 10 egz, tylko produkty na 5% VAT, nie dotykamy zabawek
                    $diffAvailable = ($productMagazineLocation['available'] - $curQuantity);
                    if (true === $this->checkModifiSellPredictQuantity($sellPredict, $vat, $diffAvailable)) {
                        if (true === $this->modifiSellPredictQuantity($sellPredictId, $diffAvailable)) {
                            $curQuantity += $diffAvailable;
							$quantity += $diffAvailable;
                        }
                    }
                    $productMagazineLocation['available'] = $productMagazineLocation['available'] - $curQuantity;
                    $productMagazineLocation['reservation'] = $productMagazineLocation['reservation'] + $curQuantity;
                    $productMagazineLocation['own_reserved'] = $curQuantity;
                    $resultReservedMagazineStock[] = $productMagazineLocation;
                    return $productMagazineLocationsCOPY;
                } else {
                    if ($productMagazineLocation['available'] > 0) {
                        $reserve = $productMagazineLocation['available'];
                        $curQuantity = $curQuantity - $reserve;
                        $productMagazineLocation['available'] = $productMagazineLocation['available'] - $reserve;
                        $productMagazineLocation['reservation'] = $productMagazineLocation['reservation'] + $reserve;
                        $productMagazineLocation['own_reserved'] = $reserve;
                        $resultReservedMagazineStock[] = $productMagazineLocation;
                    }
                }
            }
            // w $curQuantity - powinna być tu brakująca ilość
            if ($this->addSellPredict === true) {
                // @TODO trzeba sprwadzić czy przypadkiem już nie jest to dodane do zbierania

                $productQuantityPredicted = $this->sellPredict->getProductsQuantityReadyToSend($productId, $curQuantity, $iOrderItemId);
                if ($productQuantityPredicted > 0) {
                    $this->sellPredict->insertIfNotExists($productId, $productQuantityPredicted, SellPredict::TYPE_ORDER_PREDICT, 'auto-check-magazine-list-1');
                }
            }
            // DOSZLIŚMY TU PRZYWRACAMY ZMIENIONE STANY...
            $productMagazineLocations = $productMagazineLocationsCOPY;
            $this->productsMagazineLocations[$productId] = $productMagazineLocationsCOPY;
            return false;
        } else {
            if ($this->addSellPredict === true) {
                // @TODO trzeba sprwadzić czy przypadkiem już nie jest to dodane do zbierania

                $productQuantityPredicted = $this->sellPredict->getProductsQuantityReadyToSend($productId, $quantity, $iOrderItemId);
//                if ($_SESSION['user']['name'] == 'agolba') {
//                    dump($productQuantityPredicted);
//                    dump($productId);
//                    dump($quantity);
//                }
                if ($productQuantityPredicted > 0) {
                    $this->sellPredict->insertIfNotExists($productId, $productQuantityPredicted, SellPredict::TYPE_ORDER_PREDICT, 'auto-check-magazine-list-1');
                }
            }
            return false;
        }
    }


    /**
     * @return bool
     */
    private function isLocked()
    {
        global $aConfig;

        if (class_exists('memcached')) {
            $oMemcache = new Memcached();
            $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
            $magazineSemaphore = new magazineSemaphore($oMemcache);
            if (false === $magazineSemaphore->isLocked($this->magazine)) {
                $magazineSemaphore->lock($this->magazine);
                return false;
            } else {
                // druga proba
                $magazineSemaphore->unlock($this->magazine);
                sleep(5);
                if (false === $magazineSemaphore->isLocked($this->magazine)) {
                    $magazineSemaphore->lock($this->magazine);
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    /**
     *
     */
    private function loadStockMagazineLocation()
    {

        $sSql = 'SELECT SL.products_stock_id, SL.* 
                         FROM stock_location AS SL
                         JOIN containers AS C
                          ON C.id = SL.container_id
                         WHERE SL.magazine_type = "' . $this->magazine . '"
                           AND SL.available > 0
                 ORDER BY C.available_for_employee DESC
                         ';
        $this->productsMagazineLocations = $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);
    }
}
