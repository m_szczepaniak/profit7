<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class zero_quantity_min extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;

    /**
     * @var
     */
  protected $minQuantity = 31;

    public function setMinQuantity($minQuantity) {
        $this->minQuantity = $minQuantity;
    }

  public function applyFilter() {

    foreach ($this->aOrdersItems as $iKey => $aItem) {
      if ($this->sumQuantity($aItem) < $this->minQuantity) {
        unset($this->aOrdersItems[$iKey]);
      }
    }
  }
}
