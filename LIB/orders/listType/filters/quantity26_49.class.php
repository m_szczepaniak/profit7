<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class quantity26_49 extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;
  const MIN_FILTER_ITEMS = 26;
  const MAX_FILTER_ITEMS = 49;
  const MAX_COUNT = 1;
  
  public function applyFilter() {
    
    $iCurrCount = 0;
    foreach ($this->aOrdersItems as $iKey => $aItem) {
      $iOrdersItemsQuantity = $this->sumQuantity($aItem);
      if ($iOrdersItemsQuantity >= self::MIN_FILTER_ITEMS && $iOrdersItemsQuantity <= self::MAX_FILTER_ITEMS) {
        if ($iCurrCount >= self::MAX_COUNT) {
          unset($this->aOrdersItems[$iKey]);
        } else {
          $iCurrCount++;
        }
      }
    }
  }
}
