<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ommitDouble extends filtersAbstract
{
    public $type_filter = self::TYPE_FILTER_ITEMS;

    /**
     *
     * @var array
     */
    protected $aOrdersItems;
    const DOUBLE_QUANTITY = 2; // ;)

    public function applyFilter()
    {
        foreach ($this->aOrdersItems as $iKey => $aItem) {
            $quantityAllOrders = $this->sumQuantity($aItem);
            if (intval($quantityAllOrders) == self::DOUBLE_QUANTITY) {
                unset($this->aOrdersItems[$iKey]);
            }
        }
    }
}
