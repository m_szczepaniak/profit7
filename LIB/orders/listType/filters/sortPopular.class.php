<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 15.11.17
 * Time: 09:35
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class sortPopular extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;


  const SORT_ONLY_BY_POPULAR = 1;
  const SORT_BY_OLDER_AND_POPULAR = 0;

  protected $sortMode = 0;

  public function __construct($pDbMgr)
  {
    parent::__construct($pDbMgr);
    $this->sortMode = $this->getConfigurationSortMode();
  }

  /**
   * @return mixed
   */
  private function getConfigurationSortMode() {

    $sSql = "SELECT sort_by_popular
			 FROM orders_magazine_settings";
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }

  public function applyFilter() {


    $productsStats = $this->getStatsProducts();
    $this->getOrdersOrderBy($productsStats);

  }


  /**
   * @param $productsStats
   */
  private function getOrdersOrderBy($productsStats) {
    $aOrderByOrders = [];

    $aOrdersItems = $this->aOrdersItems;
    foreach ($aOrdersItems as $iOrderId => $orderItems) {
      foreach ($orderItems as $orderItem) {
        $magazineGetReadyListVIP = 1;
        if ('1' === $orderItem['magazine_get_ready_list_VIP']) {
          $magazineGetReadyListVIP = 0;
        } else {
          $magazineGetReadyListVIP = 1;
        }
        preg_match("/(\d{4})-(\d{2})-(\d{2})/", $orderItem['shipment_date_expected'], $matches);
        $shipmentDateExpected = $matches[1].$matches[2].$matches[3];

        // testowo nie używamy
        $paymentTypeOrderByAsc = $orderItem['payment_type_order_by_asc'];
        if (isset($productsStats[$orderItem['product_id']])) {
            $productOrderBy = $productsStats[$orderItem['product_id']];
            $diffQuantity = 999999 - $productOrderBy;
        }
        //$paymentTypeOrderByAsc
        if (0 == $this->sortMode) {
          $sOrderBy = sprintf("%01d%08d%01d%06d%08d%08d", $magazineGetReadyListVIP, $shipmentDateExpected, 0, $diffQuantity, $orderItem['product_id'], $iOrderId);
        } else {
          // tylko wg. popularności
          $sOrderBy = sprintf("%01d%08d%01d%06d%08d%08d", 0, 0, 0, $diffQuantity, 0, $iOrderId);
        }
        break;
      }
      $aOrderByOrders[$sOrderBy] = $iOrderId;
    }
    ksort($aOrderByOrders);

    $ordersItems = $this->aOrdersItems;
    $fixedOrdersItems = [];
    foreach ($aOrderByOrders as $orderBy => $orderId) {
      $fixedOrdersItems[$orderId] = $ordersItems[$orderId];
    }

    if ($_SESSION['user']['name'] == 'agolba') {
      dump($fixedOrdersItems);
    }

    $this->aOrdersItems = $fixedOrdersItems;
  }

  /**
   * @return array
   */
  private function getStatsProducts() {
    $productsStats = [];
    $aOrdersItems = $this->aOrdersItems;
    foreach ($aOrdersItems as $iOrderId => $orderItems) {
      foreach ($orderItems as $orderItem) {
        $productsStats[$orderItem['product_id']] += $orderItem['quantity'];
      }
    }
    return $productsStats;
  }
}

