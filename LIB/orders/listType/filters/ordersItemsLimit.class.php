<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ordersItemsLimit extends filtersAbstract
{
    /**
     *
     * @var array
     */
    protected $aOrdersItems;
    private $ordersItemsLimit = 120;

    public function setLimit($ordersItemsLimit)
    {
        $this->ordersItemsLimit = $ordersItemsLimit;
    }

    public function applyFilter()
    {

        $first = true;
        $quantityAllOrders = 0;
        foreach ($this->aOrdersItems as $iKey => $aItem) {
            $quantityAllOrders += $this->sumQuantity($aItem);
            if ($quantityAllOrders >= $this->ordersItemsLimit) {
                if (false === $first) {
                    unset($this->aOrdersItems[$iKey]);
                }
            }
            $first = false;
        }
    }
}
