<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ordersItemsLimit20 extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;
  const MAX_FILTER_ORDERS_ITEMS = 20;
  
  public function applyFilter() {

    $iCurrCount = 0;
    foreach ($this->aOrdersItems as $iKey => $aItem) {
      foreach ($aItem as $iKeyItem => $aItem) {
        $iCurrCount += $aItem['quantity'];
        if ($iCurrCount >= self::MAX_FILTER_ORDERS_ITEMS) {
          unset($this->aOrdersItems[$iKey][$iKeyItem]);
        }
      }
    }
  }
}
