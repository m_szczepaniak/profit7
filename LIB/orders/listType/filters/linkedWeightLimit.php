<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class linkedWeightLimit extends filtersAbstract
{
    public $type_filter = self::TYPE_FILTER_ITEMS;

  /**
   *
   * @var array
   */
  protected $aOrdersItems;

  private $weight;
  private $unsetIfExceed = true;
  private $linkedOrders = [];


  public function setLimit($weight, $unsetIfExceed = true)
  {
    $this->weight = $weight;
    $this->unsetIfExceed = $unsetIfExceed;
  }


  /**
   *
   * @param int $iOrderId
   * @return array
   */
  protected function getOrdersLinked($iOrderId)
  {

    $sSql = 'SELECT linked_order_id FROM orders_linked_transport WHERE main_order_id = ' . $iOrderId;
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }


  /**
   * @param $aListItems
   * @return array
   */
  private function mergeLinkedOrders($aListItems)
  {
    $result = [];
    $usedIds = [];

    // na jedną półkę maja trafić te same zamówienia
    foreach ($aListItems as $iKey => $aOrderItems) {
      if (false === in_array($iKey, $usedIds)) {
        $result[$iKey] = $aOrderItems;
        $linkedRows = $this->getOrdersLinked($iKey);
        $this->linkedOrders[$iKey] = $linkedRows;
        foreach ($linkedRows as $orderId) {
          $usedIds[] = $orderId;

          if (isset($aListItems[$orderId])) {
            $result[$iKey] = array_merge($result[$iKey], $aListItems[$orderId]);
          }
        }
      }
    }
    return $result;
  }


  /**
   *
   */
  public function applyFilter()
  {

    $tmpOrdersItems = $this->aOrdersItems;
    $tmpOrdersItems = $this->mergeLinkedOrders($tmpOrdersItems);
    foreach ($tmpOrdersItems as $iKey => $aItem) {
      $iOrdersItemsWeight = $this->sumWeight($aItem);
      if ($iOrdersItemsWeight > $this->weight) {
        if (true === $this->unsetIfExceed) {
          $this->unsetOwnWithChildren($iKey);
        }
      } else {
        if (false === $this->unsetIfExceed) {
          $this->unsetOwnWithChildren($iKey);
        }
      }
    }
  }

  /**
   * @param $iKey
   */
  private function unsetOwnWithChildren($iKey)
  {
    if (isset($this->linkedOrders[$iKey])) {
      unset($this->aOrdersItems[$iKey]);
      foreach ($this->linkedOrders[$iKey] as $child) {
        unset($this->aOrdersItems[$child]);
      }
    } else {
      throw new _('Brak zamówienia nadrzędnego ' . $iKey . ' w tablicy');
    }
  }
}
