<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 15.11.17
 * Time: 09:35
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class sortPopular extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;

  public function applyFilter() {


    $productsStats = $this->getStatsProducts();
    $this->getOrdersOrderBy($productsStats);

  }


  /**
   * @param $productsStats
   */
  private function getOrdersOrderBy($productsStats) {
    $aOrderByOrders = [];

    $aOrdersItems = $this->aOrdersItems;
    foreach ($aOrdersItems as $iOrderId => $orderItems) {
      foreach ($orderItems as $orderItem) {
        $magazineGetReadyListVIP = 1;
        if ('1' === $orderItem['magazine_get_ready_list_VIP']) {
          $magazineGetReadyListVIP = 0;
        } else {
          $magazineGetReadyListVIP = 1;
        }
        preg_match("/(\d{4})-(\d{2})-(\d{2})/", $orderItem['shipment_date_expected'], $matches);
        $shipmentDateExpected = $matches[1].$matches[2].$matches[3];

        // testowo nie używamy
        $paymentTypeOrderByAsc = $orderItem['payment_type_order_by_asc'];
        if (isset($productsStats[$orderItem['product_id']])) {
            $productOrderBy = $productsStats[$orderItem['product_id']];
            $diffQuantity = 999999 - $productOrderBy;
        }
        //$paymentTypeOrderByAsc
        $sOrderBy = sprintf("%01d%08d%01d%06d%08d%08d", $magazineGetReadyListVIP, $shipmentDateExpected, 0, $diffQuantity, $orderItem['product_id'], $iOrderId);
        break;
      }
      $aOrderByOrders[$sOrderBy] = $iOrderId;
    }
    ksort($aOrderByOrders);

    $ordersItems = $this->aOrdersItems;
    $fixedOrdersItems = [];
    foreach ($aOrderByOrders as $orderBy => $orderId) {
      $fixedOrdersItems[$orderId] = $ordersItems[$orderId];
    }

    if ($_SESSION['user']['name'] == 'agolba') {
      dump($fixedOrdersItems);
    }

    $this->aOrdersItems = $fixedOrdersItems;
  }

  /**
   * @return array
   */
  private function getStatsProducts() {
    $productsStats = [];
    $aOrdersItems = $this->aOrdersItems;
    foreach ($aOrdersItems as $iOrderId => $orderItems) {
      foreach ($orderItems as $orderItem) {
        $productsStats[$orderItem['product_id']] += $orderItem['quantity'];
      }
    }
    return $productsStats;
  }
}

