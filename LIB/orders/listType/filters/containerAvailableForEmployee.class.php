<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2017-06-30
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of containerAvailableForEmployee
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class containerAvailableForEmployee extends filtersAbstract
{
    public $type_filter = self::TYPE_FILTER_ITEMS;


    /**
     *
     * @var array
     */
    protected $aOrdersItems;
    private $availableForEmpoyee = true;

    public function setAvailableForEmpoyee($availableForEmpoyee)
    {
        $this->availableForEmpoyee = $availableForEmpoyee;
    }

    public function applyFilter()
    {
        foreach ($this->aOrdersItems as $iKey => $aItem) {
            foreach ($aItem as $keyRow => $row) {
                $allElementsIsAvailableForEmployee = true;
                if (isset($row['magazine_stock_localizations_reserved']) && !empty($row['magazine_stock_localizations_reserved'])) {
                    foreach ($row['magazine_stock_localizations_reserved'] as $item) {
                        if ($item['container_id'] != '') {
                            $containerData = $this->getContainerAvailableForEmployee($item['container_id']);

                            // czy wysokie składowanie
                            if (isset($containerData['type']) && $containerData['type'] == '3') {
                                // czy dostępne dla usera ręcznie
                                $availableForEmployee = $containerData['available_for_employee'];
                                if ($availableForEmployee === '0') {
                                    $allElementsIsAvailableForEmployee = false;
                                }

                            } else {
                                // tylko od ręki można zbierać jeśli nie ma wysokiej lokalizacji
                                if (false === $this->availableForEmpoyee) {
                                    unset($this->aOrdersItems[$iKey][$keyRow]);
                                }
                            }
                        }
                    }
                }

                if ($allElementsIsAvailableForEmployee === false) {

                    if (true === $this->availableForEmpoyee) {
                        unset($this->aOrdersItems[$iKey][$keyRow]);
                    }
                } else {
                    if (false === $this->availableForEmpoyee) {
                        unset($this->aOrdersItems[$iKey][$keyRow]);
                    }
                }
            }
        }
    }

    /**
     * @param $containerNumber
     * @return mixed
     */
    private function getContainerAvailableForEmployee($containerNumber)
    {
        $sSql = '
        SELECT available_for_employee, type
        FROM containers 
        WHERE id = "'.$containerNumber.'" 
        LIMIT 1 ';

        return $this->pDbMgr->GetRow('profit24', $sSql);
    }
}
