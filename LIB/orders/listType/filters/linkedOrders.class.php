<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

use Exception;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class linkedOrders extends filtersAbstract {

    public $type_filter = self::TYPE_FILTER_ITEMS;

  /**
   *
   * @var array
   */
  protected $aOrdersItems;
  protected $maxLinkedOrders;

  private $currentAddedLinkedOrders;


  /**
   * @param $maxLinkedOrders
   */
  public function setMaxLinkedOrders($maxLinkedOrders) {

    $this->maxLinkedOrders = $maxLinkedOrders;
  }



  public function applyFilter() {
    $aReturn = array();

    if (empty($this->aOrdersItems)) {
      throw new Exception(_('Brak łączonych zamówień'));
    }

      $tmpOrdersItems = $this->aOrdersItems;
      $tmpOrdersItems = $this->mergeLinkedOrders($tmpOrdersItems);
      foreach ($tmpOrdersItems as $iKey => $aItem) {
          $this->currentAddedLinkedOrders++;
          if ($this->currentAddedLinkedOrders > $this->maxLinkedOrders) {
              $this->unsetOwnWithChildren($iKey);
          }
      }
  }
    /**
     * @param $iKey
     */
    private function unsetOwnWithChildren($iKey)
    {
        if (isset($this->linkedOrders[$iKey])) {
            unset($this->aOrdersItems[$iKey]);
            foreach ($this->linkedOrders[$iKey] as $child) {
                unset($this->aOrdersItems[$child]);
            }
        } else {
            throw new _('Brak zamówienia nadrzędnego ' . $iKey . ' w tablicy');
        }
    }

    /**
     * @param $aListItems
     * @return array
     */
    private function mergeLinkedOrders($aListItems)
    {
        $result = [];
        $usedIds = [];

        // na jedną półkę maja trafić te same zamówienia
        foreach ($aListItems as $iKey => $aOrderItems) {
            if (false === in_array($iKey, $usedIds)) {
                $result[$iKey] = $aOrderItems;
                $linkedRows = $this->getOrdersLinked($iKey);

                $this->linkedOrders[$iKey] = $linkedRows;
                foreach ($linkedRows as $orderId) {
                    $usedIds[] = $orderId;

                    if (isset($aListItems[$orderId])) {
                        $result[$iKey] = array_merge($result[$iKey], $aListItems[$orderId]);
                    } else {
                      $this->unsetOwnWithChildren($iKey);
                    }
                }
            }
        }
        return $result;
    }


  /**
   *
   * @param int $iOrderId
   * @return array
   */
  protected function getOrdersLinked($iOrderId) {

    $sSql = 'SELECT linked_order_id FROM orders_linked_transport WHERE main_order_id = '.$iOrderId;
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }
}
