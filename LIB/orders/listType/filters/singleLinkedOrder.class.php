<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

use Exception;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class singleLinkedOrder extends filtersAbstract {
    public $type_filter = self::TYPE_FILTER_QUANTITY;


  /**
   *
   * @var array
   */
  protected $aOrdersItems;
  
  public function applyFilter() {
    $aReturn = array();
    
//    if (empty($this->aOrdersItems)) {
//      throw new Exception(_('Brak łączonych zamówień'));
//    }

    if (!empty($this->aOrdersItems)) {
      foreach ($this->aOrdersItems as $iMainOrderId => $aOrderItems) {
        $aReturn = $this->checkAllLinkedExists($iMainOrderId);
        if ($aReturn === false) {
          unset($this->aOrdersItems[$iMainOrderId]);
        } else {
          break;
        }
      }

      if (!empty($aReturn) && is_array($aReturn)) {
        $aReturn[$iMainOrderId] = $this->aOrdersItems[$iMainOrderId];
        $this->aOrdersItems = $aReturn;
      }
    }
  }
  
  /**
   * 
   * @param int $iMainOrderId
   * @return array|bool
   * @throws Exception
   */
  private function checkAllLinkedExists($iMainOrderId) {
    $aAllowedOrders = $this->getOrdersLinked($iMainOrderId);
    if (empty($aAllowedOrders)) {
      throw new Exception(_('Brak powiązanych zamówień'));
    }
    
    foreach ($aAllowedOrders as $iOrderId) {
      if (!isset($this->aOrdersItems[$iOrderId])) {
        return false;
      } else {
        $aReturn[$iOrderId] = $this->aOrdersItems[$iOrderId];
      }
    }
    return $aReturn;
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return array
   */
  protected function getOrdersLinked($iOrderId) {
    
    $sSql = 'SELECT linked_order_id FROM orders_linked_transport WHERE main_order_id = '.$iOrderId;
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }
}
