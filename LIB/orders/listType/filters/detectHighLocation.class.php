<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;
use LIB\EntityManager\Entites\StockLocation;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class detectHighLocation extends filtersAbstract
{
    public $type_filter = self::TYPE_FILTER_ITEMS;


  /**
   *
   * @var array
   */
  protected $aOrdersItems;
  protected $unsetIfSH;
  protected $unsetIfNotSH;
  protected $minQuantity;

  const MIN_QUANTITY_HIGH_LOCATION = 3;
    private $stockLocation;

    public function __construct($pDbMgr)
    {
        parent::__construct($pDbMgr);
        $this->stockLocation = new StockLocation($pDbMgr);
    }

    public function unsetIFSH($unsetIfSH = false)
  {
    $this->unsetIfSH = $unsetIfSH;
  }

  public function unsetIFNotSH($unsetIfNotSH = true)
  {
    $this->unsetIfNotSH = $unsetIfNotSH;
  }

  public function setMinQuantityStock($minQuantity)
  {
    $this->minQuantity = $minQuantity;
  }


  public function applyFilter()
  {
    $productIds = $this->getProductsIds($this->aOrdersItems);
    if (!empty($productIds)) {
      $cols = ['id', 'profit_g_location', 'profit_x_location', 'profit_g_act_stock', 'profit_g_status'];
      $productsMagazineLocations = $this->stockLocation->getMagazineLocations($productIds, StockLocation::HIGH_MAGAZINE_TYPE);
        $productsMagazineAvailable = $this->sumMagazineLocationAvailable($productsMagazineLocations);
//      $productsStock = $this->loadProductsStock($productIds, $cols);

      foreach ($this->aOrdersItems as $iKey => $aItems) {
        $bIsSH = true;
        foreach ($aItems as $item) {
          if (false === $this->isEnough($productsMagazineAvailable[$item['product_id']], $item['quantity'])) {
            $bIsSH = false;
            break;
          }
        }
        if (false === $bIsSH) {
          if (true === $this->unsetIfNotSH) {
            unset($this->aOrdersItems[$iKey]);
          }
        } else {
          if (true === $this->unsetIfSH) {
            unset($this->aOrdersItems[$iKey]);
          }
        }
      }
    }
  }

  /**
   * @param $productIds
   * @param $cols
   * @return array
   */
  private function loadProductsStock($productIds, $cols)
  {

    $sSql = 'SELECT ' . implode($cols, ', ') . ' FROM products_stock WHERE id IN (' . implode(', ', $productIds) . ')';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, false);
  }

  /**
   * @param $aOrdersItems
   * @return array
   */
  private function getProductsIds($aOrdersItems)
  {
    $productsIds = [];
    foreach ($this->aOrdersItems as $iKey => $aItems) {
      foreach ($aItems as $item) {
        $productsIds[] = $item['product_id'];
      }
    }
    return $productsIds;
  }


  /**
   * @param $item
   * @param $productStock
   * @return bool
   */
  private function checkIsSH($productStock)
  {
    if (!empty($productStock)) {
      if (intval($productStock['profit_g_act_stock']) >= $this->minQuantity) {
        return true;
      }
    }
    return false;
  }

    /**
     * @param $productsMagazineLocations
     * @return array
     */
    private function sumMagazineLocationAvailable($productsMagazineLocations)
    {
        $productsMagazineAvailable = [];
        foreach ($productsMagazineLocations as $productId => $productMagazineLocations) {
            foreach ($productMagazineLocations as $productMagazineLocation) {
                $productsMagazineAvailable[$productId] += $productMagazineLocation['available'];

            }
        }
        return $productsMagazineAvailable;
    }

    /**
     * @param $magazineAvailable
     * @param $requiredQuantity
     * @return bool
     */
    private function isEnough($magazineAvailable, $requiredQuantity)
    {
        if (intval($magazineAvailable) >= intval($requiredQuantity)) {
            return true;
        }
        return false;
    }
}
