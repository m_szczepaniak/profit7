<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class singleOrder extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;
  const MAX_FILTER_ORDERS = 1;
  
  public function applyFilter() {

    $aSingleOrder = [];
    foreach ($this->aOrdersItems as $iKey => $aItem) {
      $aSingleOrder[$iKey] = $aItem;
      break;
    }
    $this->aOrdersItems = $aSingleOrder;
  }
}
