<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class websiteFilter extends filtersAbstract {

    public $type_filter = self::TYPE_FILTER_ITEMS;


    /**
     *
     * @var array
     */
    protected $aOrdersItems;

    public $aWebsites;

    public function setFilteredWebsite($aWebsites = []) {
        $this->aWebsites = $aWebsites;
    }


    public function applyFilter() {

        foreach ($this->aOrdersItems as $iKey => $aItems) {
            foreach ($aItems as $iKeyItem => $aItem) {
				if ($aItem['website_id'] == '0') {
					echo 'brak serwisu';
					die;
				}
                if (false === in_array($aItem['website_id'], $this->aWebsites)) {
                    unset($this->aOrdersItems[$iKey]);
                }
            }
        }
    }
}
