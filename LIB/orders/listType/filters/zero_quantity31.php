<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class zero_quantity31 extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;
  const MIN_FILTER_ITEMS = 31;
  
  public function applyFilter() {

    foreach ($this->aOrdersItems as $iKey => $aItem) {
      if ($this->sumQuantity($aItem) >= self::MIN_FILTER_ITEMS) {
        unset($this->aOrdersItems[$iKey]);
      }
    }
  }
}
