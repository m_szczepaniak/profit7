<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class zero_weight_min extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;

    /**
     * @var
     */
  protected $minWeight = 20.01;

    public function setMinWeight($minWeight) {
        $this->minWeight = $minWeight;
    }

  public function applyFilter() {

    foreach ($this->aOrdersItems as $iKey => $aItem) {
      if ($this->sumWeight($aItem) < $this->minWeight) {
        unset($this->aOrdersItems[$iKey]);
      }
    }
  }
}
