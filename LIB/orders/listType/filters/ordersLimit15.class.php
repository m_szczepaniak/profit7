<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ordersLimit15 extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;



  const MAX_FILTER_ORDERS = 15;
  
  public function applyFilter() {
    
    $iCurrCount = 0;
    foreach ($this->aOrdersItems as $iKey => $aItem) {
      if ($iCurrCount >= self::MAX_FILTER_ORDERS) {
        unset($this->aOrdersItems[$iKey]);
      } else {
        $iCurrCount++;
      }
    }
  }
}
