<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ordersLimit extends filtersAbstract
{
    /**
     *
     * @var array
     */
    protected $aOrdersItems;

    const DEFAULT_MAX_FILTER_ORDERS = 15;
    protected $ordersLimit;

    public function setLimit($ordersLimit)
    {
        $this->ordersLimit = $ordersLimit;
    }

    public function applyFilter()
    {
        if (intval($this->ordersLimit) <= 0 ) {
            $this->ordersLimit = self::DEFAULT_MAX_FILTER_ORDERS;
        }
        $iCurrCount = 0;
        foreach ($this->aOrdersItems as $iKey => $aItem) {
            if ($iCurrCount >= $this->ordersLimit) {
                unset($this->aOrdersItems[$iKey]);
            } else {
                $iCurrCount++;
            }
        }
    }
}
