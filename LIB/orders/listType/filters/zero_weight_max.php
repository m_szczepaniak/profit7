<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class zero_weight_max extends filtersAbstract {
  /**
   *
   * @var array
   */
  protected $aOrdersItems;

    /**
     * @var
     */
  protected $maxWeight = 20;

    public function setMaxWeight($maxWeight) {
        $this->maxWeight = $maxWeight;
    }

  public function applyFilter() {

    foreach ($this->aOrdersItems as $iKey => $aItem) {
      if ($this->sumWeight($aItem) >= $this->maxWeight) {
        unset($this->aOrdersItems[$iKey]);
      }
    }
  }
}
