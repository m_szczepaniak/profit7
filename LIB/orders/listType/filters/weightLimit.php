<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

/**
 * Description of quantity50
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class weightLimit extends filtersAbstract
{
    /**
     *
     * @var array
     */
    protected $aOrdersItems;

    private $iMinWeight;
    private $iMaxWeight;
    private $iLimitQuantity;


    public function setLimit($iMinWeight, $iMaxWeight, $iLimitQuantity)
    {
        $this->iMinWeight = $iMinWeight;
        $this->iMaxWeight = $iMaxWeight;
        $this->iLimitQuantity = $iLimitQuantity;
    }

    public function applyFilter()
    {

        $iCurrCount = 0;
        foreach ($this->aOrdersItems as $iKey => $aItem) {
            $iOrdersItemsWeight = $this->sumWeight($aItem);
            if ($iOrdersItemsWeight >= $this->iMinWeight && $iOrdersItemsWeight <= $this->iMaxWeight) {
                if ($iCurrCount >= $this->iLimitQuantity) {
                    unset($this->aOrdersItems[$iKey]);
                } else {
                    $iCurrCount++;
                }
            }
        }
    }
}
