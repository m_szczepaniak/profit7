<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\filters;

use DatabaseManager;

/**
 * Description of filtersAbstract
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
abstract class filtersAbstract {

    CONST TYPE_FILTER_ITEMS = 1;

    CONST TYPE_FILTER_QUANTITY = 2;


  /**
   *
   * @var array
   */
  protected $aOrdersItems;
  
  /**
   *
   * @var DatabaseManager
   */
  protected $pDbMgr;


    public $type_filter;

  public function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }
  
  public abstract function applyFilter();


    public function getTypeFilter(){
        return $this->type_filter;
    }

  /**
   * 
   * @param array $aOrdersItems
   */
  public function setOrdersItems($aOrdersItems) {
    $this->aOrdersItems = $aOrdersItems;
  }
  
  /**
   * 
   * @return array
   */
  public function getFilteredOrdersItems() {
    return $this->aOrdersItems;
  }
  
  /**
   * 
   * @param array $aOrdersItems
   * @return int
   */
  protected function sumQuantity($aOrdersItems) {
    
    $iQuantity = 0;
    foreach ($aOrdersItems as $aItem) {
      $iQuantity += $aItem['quantity'];
    }
    return $iQuantity;
  }


    /**
     *
     * @param array $aOrdersItems
     * @return int
     */
    protected function sumWeight($aOrdersItems) {

        $iWeight = 0;
        foreach ($aOrdersItems as $aItem) {
            $iWeight += ($aItem['weight'] * $aItem['quantity']);
        }
        return $iWeight;
    }
}
