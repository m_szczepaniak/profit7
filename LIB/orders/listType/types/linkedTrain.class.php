<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\types;

/**
 * Description of linkedTrain
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class linkedTrain extends typesAbstract implements iListTypes{

  private $aOrder;
  private $aOrderItems;
  private $bFirstCheckType;
  private $cType = '6';
  protected $mListType = 'LINKED_TRAIN';
  protected $name = 'Łączone Tramwaj';
  protected $bGetFromTrain = TRUE;
  
  /**
   * 
   * @param type $aOrder
   * @param type $aOrderItems
   * @param type $bFirstCheckType
   */
  public function __contruct() {}
  
  /**
   * 
   * @return type
   */
  public function getType() {
    return $this->cType;
  }

  /**
   * 
   * @param bool $bFirstCheckType
   */
  public function setFirstCheckType($bFirstCheckType) {
    $this->bFirstCheckType = $bFirstCheckType;
  }

  /**
   * 
   * @param array $aOrder
   */
  public function setOrder($aOrder) {
    $this->aOrder = $aOrder;
  }

  /**
   * 
   * @param array $aOrderItems
   */
  public function setOrderItems($aOrderItems) {
    $this->aOrderItems = $aOrderItems;
  }
  
  /**
   * 
   * @return boolean
   */
  public function checkType() {
    
    if ($this->checkOrder() === false) {
      return false;
    }
    
    if ($this->checkOrdersItems() === false) {
      return false;
    }
    return true;
  }
  
  
  /**
   * 
   */
  private function checkOrdersItems() {
    
    $iCountTrainAcceptable = 0;
    foreach ($this->aOrderItems as $aOItem) {
      
      if ($this->checkIsNotDeletedOrPacketOrAttachment($aOItem) === true) {
        $mType = $this->checkOrderItem($aOItem);
        if ($mType === false) {
          return false;
        } else {
          $iCountTrainAcceptable += $aOItem['quantity'];
          if ($iCountTrainAcceptable > 0) {
            if ($this->checkAllLinkedOrders($this->aOrder['order_id']) === false) {
              return false;
            } else {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  /**
   * 
   * @param array $aOItem
   * return bool
   */
  private function checkOrderItem(array $aOItem) {
    if ($aOItem['status'] != '4') {
      return false;
    }
    
    if ($aOItem['source'] != '51') {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @return boolean
   */
  private function checkOrder() {
    if ($this->aOrder['order_status'] == '3' || $this->aOrder['order_status'] == '4' || $this->aOrder['order_status'] == '5') {
      return false;
    }
    
    if ($this->aOrder['linked_order'] == '0') {
      return false;
    }
    
    return true;
  }
  
  /**
   * 
   * @param string $aOItem
   * @return boolean
   */
  private function checkIsNotDeletedOrPacketOrAttachment(array $aOItem) {
    
    if ($aOItem['deleted'] == '0' && $aOItem['get_ready_list'] == '0' && $aOItem['packet'] == '0' && ($aOItem['item_type'] == 'I' || $aOItem['item_type'] == 'P')) {
      return true;
    } 
    return false;
  }

  public function getOrderItemsStock() {
    $this->aReturnOrdersItems = $this->aOrderItems;
    return $this->aOrderItems;
  }

  public function getOrderItemsTran() {
    $this->aReturnOrdersItems = $this->aOrderItems;
    return $this->aOrderItems;
  }
}
