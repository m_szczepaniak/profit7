<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\types;

/**
 * Description of linkedPart
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class linkedPart extends typesAbstract implements iListTypes{

  private $aOrder;
  private $aOrderItems;
  private $bFirstCheckType;
  private $cType = '6';
  protected $mListType = 'LINKED_PART';
  protected $name = 'Łączone częściowe';
  protected $bGetFromTrain = TRUE;
  protected $aReturnOrdersItems = array();
  protected $iMinOrdersItems = 0;
  protected $iMaxOrdersItems = 9999999;
    
  /**
   * 
   * @param type $aOrder
   * @param type $aOrderItems
   * @param type $bFirstCheckType
   */
  public function __contruct() {}
  
  /**
   * 
   * @return type
   */
  public function getType() {
    return $this->cType;
  }

  /**
   * 
   * @param bool $bFirstCheckType
   */
  public function setFirstCheckType($bFirstCheckType) {
    $this->bFirstCheckType = $bFirstCheckType;
  }

  /**
   * 
   * @param array $aOrder
   */
  public function setOrder($aOrder) {
    $this->aOrder = $aOrder;
  }

  /**
   * 
   * @param array $aOrderItems
   */
  public function setOrderItems($aOrderItems) {
    $this->aOrderItems = $aOrderItems;
  }
  
  /**
   * 
   * @return boolean
   */
  public function checkType() {
    
    if ($this->checkOrder() === false) {
      return false;
    }
    
    if ($this->checkOrdersItems() === false) {
      return false;
    }
    return true;
  }
  
  
  /**
   * 
   */
  private function checkOrdersItems() {
    
    $iCountStockAcceptable = 0;
    $iCountTrainAcceptable = 0;
    foreach ($this->aOrderItems as $aOItem) {
      if ($this->checkIsNotDeletedOrPacketOrAttachment($aOItem) === true) {
        $mType = $this->checkOrderItemPrimarySource($aOItem);
        if ($mType === false) {
          return false;
        } 
        else if ($mType === 'S') {
          $iCountStockAcceptable += $aOItem['quantity'];
        } 
        elseif ($mType === 'T') {
          $iCountTrainAcceptable += $aOItem['quantity'];
        }
      }
    }
    
    if ($iCountTrainAcceptable == 0 || $iCountStockAcceptable == 0) {
      return false;
    }

    if ($iCountTrainAcceptable > $this->iMinOrdersItems && $iCountTrainAcceptable < $this->iMaxOrdersItems) {
      if ($this->checkAllLinkedOrders($this->aOrder['order_id']) === false) {
        return false;
      } else {
        return true;
      }
    }
    
    return false;
  }
  
  /**
   * 
   * @param array $aOItem
   * return bool
   */
  private function checkOrderItemPrimarySource(array $aOItem) {
    if ($aOItem['status'] == '0' || $aOItem['status'] == '-1') {
      return false;
    }
    
    if ($aOItem['source'] != '51' && $aOItem['status'] != '4') {
      // jeśli książka jest z zewnątrz i jeszcze nie przyjechała to nie zbieramy jej jeszcze NIGDZIE
      return false; 
    }
    else if ($aOItem['source'] == '51') {
      return 'S';
    } elseif ($aOItem['source'] != '51' && $aOItem['status'] == '4') {
      return 'T';
    } else {
      return false;
    }
  }
  
  
  /**
   * 
   * @param array $aOItem
   * return bool
   */
  private function checkOrderItem(array $aOItem) {
    if ($aOItem['status'] == '0' || $aOItem['status'] == '-1') {
      return false;
    }
    
    if ($aOItem['source'] != '51' && $aOItem['status'] != '4') {
      // jeśli książka jest z zewnątrz i jeszcze nie przyjechała to nie zbieramy jej jeszcze NIGDZIE
      return false; 
    }
    else if ($aOItem['source'] == '51') {
      // 2 to będzie jeśli zaczniemy te produkty zbierać
      if ($aOItem['status'] == '3') {
        return 'S';
      } else if ($aOItem['status'] == '4') {
        // ok mamy już na tramwaju
        return 'T';
      } else {
        return -1;
      }
    } elseif ($aOItem['source'] != '51' && $aOItem['status'] == '4') {
      return 'T'; // to powinno być zdejmowane z tramwaju
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @return boolean
   */
  private function checkOrder() {
    if ($this->aOrder['order_status'] == '3' || $this->aOrder['order_status'] == '4' || $this->aOrder['order_status'] == '5') {
      return false;
    }
    
    if ($this->aOrder['linked_order'] == '0') {
      return false;
    }
    
    return true;
  }
  
  /**
   * 
   * @param string $aOItem
   * @return boolean
   */
  private function checkIsNotDeletedOrPacketOrAttachment(array $aOItem) {
    
    if ($aOItem['deleted'] == '0' && $aOItem['get_ready_list'] == '0' && $aOItem['packet'] == '0' && ($aOItem['item_type'] == 'I' || $aOItem['item_type'] == 'P')) {
      return true;
    } 
    return false;
  }

  /**
   * 
   * @return array
   */
  public function getOrderItemsStock() {
    $aOItems = array();
    foreach ($this->aOrderItems as $aOItem) {
      $mType = $this->checkOrderItem($aOItem);
      if ($mType === 'S') {
        $aOItems[] = $aOItem;
      } elseif ($mType === FALSE) {
        // coś jest nie tak
        return array();
      }
    }
    $this->aReturnOrdersItems = $aOItems;
    return $aOItems;
  }

  /**
   * 
   * @return array
   */
  public function getOrderItemsTran() {
    $aOItems = array();
    foreach ($this->aOrderItems as $iKey => $aOItem) {
      $mType = $this->checkOrderItem($aOItem);
      if ($mType === 'T') {
        $aOItems[] = $aOItem;
      } else {
        // coś jest nie tak
        return array();
      }
    }
    $this->aReturnOrdersItems = $aOItems;
    return $aOItems;
  }
}