<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\types;

use DatabaseManager;

/**
 * Description of typesAbstract
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
abstract class typesAbstract {
  protected $bGetFromTrain;
  protected $mListType;
  protected $iCountOrders;
  protected $aReturnOrdersItems;
  protected $iMaxOrdersItems;
  protected $iMinOrdersItems;
  protected $pDbMgr;
  protected $bParentGetFromTrain;

  public function checkListsTypes($aListTypes) {
    return in_array($this->mListType, $aListTypes);
  }
  
  /**
   * 
   * @param string $sListType
   * @return boolean
   */
  public function checkListType($sListType) {
    if ($this->mListType === $sListType) {
      return true;
    }
    return false;
  }
  
  /**
   * 
   * @return boolean
   */
  public function isGetFromTrain() {
    if ($this->bGetFromTrain === TRUE) {
      return true;
    }
    return false;
  }
  
  /**
   * 
   * @return int
   */
  public function getCountOrders() {
    return $this->iCountOrders;
  }
  
  /**
   * 
   * @return array
   */
  public function getOrdersCounts() {
    $aOrders = array();
    foreach ($this->aReturnOrdersItems as $oItem) {
      $aOrders[$oItem['order_id']] = '1';
    }
    return count($aOrders);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return int
   */
  protected function checkAllLinkedOrders($iOrderId, $bTrainConfirmedRequirded = true) {
    if ($this->bParentGetFromTrain === FALSE) {
      return true;
    }
    
    $aOrdersLinkedIds = $this->getOrdersLinked($iOrderId);
    $oOrderData = new \LIB\orders\listType\getOrderData($this->pDbMgr);
    $aOrdersItemsData = $oOrderData->getOrdersItemsListData(/** bez znaczenia */false, '', array(), $aOrdersLinkedIds);
    return $this->checkAllSubordersConfirmed($aOrdersItemsData, $bTrainConfirmedRequirded);
  }// end of checkAllLinkedOrders() method
  
  /**
   * 
   * @param array $aOrdersItemsData
   * @return array
   */
  private function checkAllSubordersConfirmed($aOrdersItemsData, $bTrainConfirmedRequirded) {
    
    foreach ($aOrdersItemsData as $aOrdersItems) {
      foreach ($aOrdersItems as $aOItem) {
        if ($aOItem['deleted'] == '0' && $aOItem['packet'] == '0' && ($aOItem['item_type'] == 'I' || $aOItem['item_type'] == 'P')) {
          if ($bTrainConfirmedRequirded === true) {
            if ($aOItem['status'] != '4') {
              return false;
            }
          } else {
            if ($this->checkItemTrainConfirmedRequired($aOItem) === false) {
              return false;
            }
          }
        }
      }
    }
    return true;
  }
  
  /**
   * Sprawdzamy czy wszystkie składowe są różne od
   * potwierdzone w przypadku źródeł innych niż 51
   * jest nie potwierdzone 
   * 
   * @param type $aOItem
   * @return boolean
   */
  public function checkItemTrainConfirmedRequired($aOItem) {
    
    if ($aOItem['status'] != '4' && ($aOItem['status'] != '3' || $aOItem['source'] != '51')) {
      return false;
    }
    return true;
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return array
   */
  protected function getOrdersLinked($iOrderId) {
    
    $sSql = 'SELECT linked_order_id FROM orders_linked_transport WHERE main_order_id = '.$iOrderId;
    return $this->pDbMgr->GetCol('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iMaxOrdersItems
   */
  public function setMinOrdersItems($iMaxOrdersItems) {
    $this->iMaxOrdersItems = $iMaxOrdersItems;
  }
  
  /**
   * 
   * @param int $iMinOrdersItems
   */
  public function setMaxOrdersItems($iMinOrdersItems) {
    $this->iMinOrdersItems = $iMinOrdersItems;
  }
  
  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function setDb(DatabaseManager $pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @param bool $bParentGetFromTrain
   */
  public function setParentGetFromTrain($bParentGetFromTrain) {
    $this->bParentGetFromTrain = $bParentGetFromTrain;
  }
}
