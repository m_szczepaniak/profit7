<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace LIB\orders\listType\types;

/**
 * Description of singleStock
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class singleStock extends typesAbstract implements iListTypes{
  
  private $aOrder;
  private $aOrderItems;
  private $bFirstCheckType;
  private $cType = '2';
  protected $mListType = 'SINGLE_SORTER_STOCK';
  protected $name = 'Single Stock';
  protected $bGetFromTrain = FALSE;
  
  /**
   * 
   * @param type $aOrder
   * @param type $aOrderItems
   * @param type $bFirstCheckType
   */
  public function __contruct() {}
  
  /**
   * 
   * @return type
   */
  public function getType() {
    return $this->cType;
  }

  /**
   * 
   * @param bool $bFirstCheckType
   */
  public function setFirstCheckType($bFirstCheckType) {
    $this->bFirstCheckType = $bFirstCheckType;
  }

  /**
   * 
   * @param array $aOrder
   */
  public function setOrder($aOrder) {
    $this->aOrder = $aOrder;
  }

  /**
   * 
   * @param array $aOrderItems
   */
  public function setOrderItems($aOrderItems) {
    $this->aOrderItems = $aOrderItems;
  }

  /**
   * 
   * @return boolean
   */
  public function checkType() {
    
    if ($this->checkOrder() === false) {
      return false;
    }
    
    if ($this->checkOrdersItems() === false) {
      return false;
    }
    return true;
  }
  
  /**
   * 
   */
  private function checkOrdersItems() {
    
    $iCountAcceptable = 0;
    foreach ($this->aOrderItems as $aOItem) {
      if ($this->checkIsNotDeletedOrPacketOrAttachment($aOItem) === true) {
        if ($this->checkOrderItem($aOItem) === true) {
          $iCountAcceptable += $aOItem['quantity'];
          if ($iCountAcceptable > 1) {
            return false;
          }
        } else {
          return false;
        }
      }
    }
    if ($iCountAcceptable === 1) {
      return true;
    } 
    return false;
  }
  
  /**
   * 
   * @param array $aOItem
   * return bool
   */
  private function checkOrderItem(array $aOItem) {
    
    if ($aOItem['source'] == '51' && $aOItem['status'] != '0' && $aOItem['status'] != '-1') {
      return true;
    } else {
      return false;
    }
  }

  /**
   * 
   * @param string $aOItem
   * @return boolean
   */
  private function checkIsNotDeletedOrPacketOrAttachment(array $aOItem) {
    
    if ($aOItem['deleted'] == '0' && $aOItem['get_ready_list'] == '0' && $aOItem['packet'] == '0' && ($aOItem['item_type'] == 'I' || $aOItem['item_type'] == 'P')) {
      return true;
    } 
    return false;
  }
  
  /**
   * 
   * @return boolean
   */
  private function checkOrder() {
    if ($this->aOrder['order_status'] == '3' || $this->aOrder['order_status'] == '2' || $this->aOrder['order_status'] == '4' || $this->aOrder['order_status'] == '5') {
      return false;
    }
    
    if ($this->aOrder['linked_order'] == '1') {
      return false;
    }
    
    return true;
  }

  public function getOrderItemsStock() {
    $this->aReturnOrdersItems = $this->aOrderItems;
    return $this->aOrderItems;
  }

  public function getOrderItemsTran() {
    $this->aReturnOrdersItems = $this->aOrderItems;
    return $this->aOrderItems;
  }
}
