<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-29 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType\types;

/**
 * Description of iListTypes
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
interface iListTypes {
  public function __contruct();
  
  public function setOrder($aOrder);
  public function setOrderItems($aOrderItems);
  public function setFirstCheckType($bFirstCheckType);
  public function checkType();
  public function getType();
  
  public function getOrderItemsTran();
  public function getOrderItemsStock();
}
