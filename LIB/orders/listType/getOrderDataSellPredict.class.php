<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-06-30 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType;

use DatabaseManager;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\SellPredict;

/**
 * Description of getOrderData
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class getOrderDataSellPredict implements OrderDataInterface{

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  // !!! TUTAJ WOGOLE NIE MOWIMY O ZAMOWIENIACH ... TROCHE ZMIENIA NAM TO PODEJSCIE DO ZBIERANIA JAKO TAKIEGO...



  private $aOrdersRequiredCols = array('P.id');
  private $aProductsRequiredCols = array('P.isbn_plain', 'P.isbn_10', 'P.isbn_13', 'P.ean_13');
  private $aOrdersItemsRequiredCols = array(' CONCAT(SP.id) AS ident ', 'SP.product_id',
      'SP.quantity', 'P.name', 'P.weight',
        '(
          SELECT PEC.page_id 
          FROM products_extra_categories AS PEC 
          JOIN menus_items_books AS PI
           ON PI.page_id = PEC.page_id
          WHERE PEC.product_id = SP.product_id 
          LIMIT 1
      ) AS is_add_books_cat 
      ',
      'P.vat'
  );
  private $aSelectOrdersItemsCols = array(
      'P.weight', '" " AS authors', '" " AS publisher', ' " " AS publication_year', '" " AS edition',
      '(SELECT CONCAT(directory, "|", photo) FROM products_images AS PI WHERE PI.product_id = SP.product_id LIMIT 1) AS photo'
  );
  
  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function __construct($pDbMgr) {
    
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @param array $aTransportId
   * @param date $dExpectedShipmentDate
   * @param array $aOrdersIds
   * @return string
   */
  private function getWhereSQL($aTransportId, $dExpectedShipmentDate, $aOrdersIds) {
    $sWhereSQL = '';
    if (!empty($aTransportId) && is_array($aTransportId)) {
      $sWhereSQL .= '
       AND (
          O.transport_id IN ('.implode(', ', $aTransportId).')
        ) ';
    }
    
    if ($dExpectedShipmentDate != '' && $dExpectedShipmentDate != '00-00-0000') {
      $dFormedExpectedDate = preg_replace('/(\d{2})-(\d{2})-(\d{4})/', '$3-$2-$1', $dExpectedShipmentDate);
      $sWhereSQL .= ' AND O.shipment_date_expected <= "'.$dFormedExpectedDate.'" ';
    }
    
    if (!empty($aOrdersIds) && is_array($aOrdersIds)) {
      $sWhereSQL .= '
        AND (
          O.id IN ('.implode(', ', $aOrdersIds).')
        ) ';
    }
    return $sWhereSQL;
  }
  
  /**
   * 
   * @param array $aOrdersItemsRequiredCols
   * @param bool $bGetFromTrain
   * @return array
   */
  private function setGetOrderColumnsByStock($aOrdersItemsRequiredCols, $bGetFromTrain) {
    $sSelectSql = '(SELECT IF(profit_x_location != "" AND profit_g_location != "", CONCAT(profit_x_location, " ", profit_g_location), IF (profit_x_location != "", profit_x_location, profit_g_location)) FROM products_stock AS PS WHERE PS.id = SP.product_id) AS pack_number';
    $aOrdersItemsRequiredCols[] = $sSelectSql;
    return $aOrdersItemsRequiredCols;
  }

  /**
   *
   * @param bool $bGetFromTrain
   * @param date|string $dExpectedShipmentDate
   * @param array $aTransportId
   * @param array $aOrdersIds
   * @return array
   */
  public function getOrdersItemsListData($bGetFromTrain = false, $dExpectedShipmentDate = '', $aTransportId = array(), $aOrdersIds = array(), $bAddShowColumns = false) {
    
    $this->aOrdersItemsRequiredCols = $this->setGetOrderColumnsByStock($this->aOrdersItemsRequiredCols, $bGetFromTrain);
    $sWhereSQL = $this->getWhereSQL($aTransportId, $dExpectedShipmentDate, $aOrdersIds);

    $sSql = 'SELECT SP.id AS KEY_TMP, SP.id AS SP_id, PS.profit_g_act_stock, 
    
    (SELECT container_id FROM stock_location AS SL WHERE SL.products_stock_id = PS.id AND SL.magazine_type = "'.Magazine::TYPE_HIGH_STOCK_SUPPLIES.'" LIMIT 1) AS stock_location_container_id,
    
    '.($bAddShowColumns === true ? implode(', ', $this->aSelectOrdersItemsCols).', ' : '').implode(', ', $this->aOrdersItemsRequiredCols).', '.implode(', ', $this->aOrdersRequiredCols).', '.implode(', ', $this->aProductsRequiredCols).'
             FROM sell_predict AS SP
             JOIN products AS P
              ON P.id = SP.product_id
             JOIN products_stock AS PS
              ON PS.id = SP.product_id
             WHERE SP.status = "'.SellPredict::STATUS_ACTIVE.'"
             ORDER BY IF(TIMESTAMPDIFF(MINUTE, SP.created, NOW()) >= 300, "2", IF(TIMESTAMPDIFF(MINUTE, SP.created, NOW()) >= 120, "1", "0")) DESC, stock_location_container_id ASC ';
    $aReturn = $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);
    return $aReturn;
  }


  /**
   * 
   * @param int $iILId
   * @param bool $bGetFromTrain
   * @return array
   */
  public function getOrdersItemsListDataByOILId($iILId, $bGetFromTrain) {
    
    $this->aOrdersItemsRequiredCols = $this->setGetOrderColumnsByStock($this->aOrdersItemsRequiredCols, $bGetFromTrain);
    
    $sSql = 'SELECT O.id as oid, O.id AS order_id, '.implode(', ', $this->aSelectOrdersItemsCols).', '.implode(', ', $this->aOrdersItemsRequiredCols).', '.implode(', ', $this->aOrdersRequiredCols).', '.implode(', ', $this->aProductsRequiredCols).'
             FROM orders_items AS OI
             JOIN orders AS O
              ON OI.order_id = O.id
             JOIN orders_items_lists_items AS OILI
              ON OILI.orders_items_id = OI.id AND OILI.orders_items_lists_id = '.$iILId.'
             LEFT JOIN products AS P
              ON P.id = OI.product_id
             WHERE 
              OI.deleted = "0"
              AND (OI.item_type = "I" OR OI.item_type = "P")
              AND OI.packet = "0"
             ORDER BY O.magazine_get_ready_list_VIP DESC, O.shipment_date_expected ASC, IF(O.payment_type = "postal_fee", "1", "0") DESC, O.id ASC';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return array
   */
  public function getOrderListData($iOrderId) {
    
    $sSql = 'SELECT '.implode(', ', $this->aOrdersRequiredCols).'
             FROM orders 
             WHERE id = '.$iOrderId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }

  /**
   * 
   * @param int $iOrderId
   * @return array
   */
  public function getOrderItemsListData($iOrderId) {
    
    $sSql = 'SELECT '.implode(', ', $this->aOrdersItemsRequiredCols).'
             FROM orders_items
             WHERE order_id = '.$iOrderId;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
}
