<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-01 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType;

use Assets\htmlHelper;
use Common_get_ready_orders_books;
use LIB\orders\listType\filters\filtersAbstract;
use LIB\orders\listType\filters\ordersLimit15;
use LIB\orders\listType\filters\quantity21;
use LIB\orders\listType\filters\singleOrder;
use LIB\orders\listType\filters\typeFilter;
use Memcached;
use orders_containers\OrdersContainers;
use ordersSemaphore;
use stdClass;

/**
 * Description of manageGetOrdersItemsList
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class manageGetOrdersItemsListMultilocalize extends manageGetOrdersItemsList
{

    protected $bTestMode;
    protected $pDbMgr;
    protected $bGetFromTrain;
    protected $cListType;

    /**
     *
     * @var orderListTypes
     */
    protected $oListTypes;

    /**
     *
     * @var filtersAbstract[]
     */
    protected $aFilters = array();

    /**
     *
     * @var filtersAbstract[]
     */
    protected $aRequiredFilters = array();
    protected $oGetOrderData;

  public function __construct($pDbMgr, $dataProvider)
  {
    parent::__construct($pDbMgr);
    $this->pDbMgr = $pDbMgr;
    if ($_SERVER['REMOTE_ADDR'] == '83.220.102.129' || $_SERVER['REMOTE_ADDR'] == '62.133.145.163') {
      $this->bTestMode = true;
    } else {
      $this->bTestMode = false;
    }
    $this->setDataProvider($dataProvider);
  }


  /**
   *
   * @param date $dExpectedShipmentDate
   * @param array $aTransportId
   * @param bool $bAddShowColumns
   * @return array
   */
  public function getCountOrdersGroupByTransport($dExpectedShipmentDate = '', $aTransportId = array(), $bAddShowColumns = false)
  {

    $aOrderItemsList = $this->oGetOrderData->getOrdersItemsListData($this->getGetFromTrain(), $dExpectedShipmentDate, $aTransportId, array(), $bAddShowColumns);
    if (!empty($aOrderItemsList)) {
      // tutaj filtry powinny być wyłączone, poza tymi wymaganymi
      $aOrderItemsList = $this->applyRequiredFilters($aOrderItemsList);
    }
    $aOrderItemsList = $this->clearEmptyOrders($aOrderItemsList);
    $counts = $this->getCounts($aOrderItemsList);
    return $counts;
  }

  /**
   * @param $rows
   * @return mixed
   */
  private function getCounts($rows) {

    $data = [];
    foreach ($rows as $row) {
      foreach ($row as $item) {
        $data[1] += $item['quantity'];
      }

    }
    return $data;
  }


  /**
   *
   * @param date $dExpectedShipmentDate
   * @param array $aTransportId
   * @return array
   */
  public function getOrdersItemsListsType($dExpectedShipmentDate = '', $aTransportId = array(), $bAddShowColumns = false)
  {
    $aOrdersItemsFiltered = array();

    $aOrdersItems = $this->oGetOrderData->getOrdersItemsListData($this->getGetFromTrain(), $dExpectedShipmentDate, $aTransportId, array(), $bAddShowColumns);
    if (!empty($aOrdersItems)) {
      $aOrdersItemsFiltered = $this->applyFilters($aOrdersItems);
    }
    $aOrdersItemsFiltered = $this->clearEmptyOrders($aOrdersItemsFiltered);
//    $aOrdersItemsFiltered = $this->checkAddTransportOrderItems($aOrdersItemsFiltered, $aOrdersItems, true);

    $this->dumpAdminsData($aOrdersItemsFiltered);
    return $aOrdersItemsFiltered;
  }

    protected function checkAddDefaultFilter($filters) {
        return $filters;
    }
}
