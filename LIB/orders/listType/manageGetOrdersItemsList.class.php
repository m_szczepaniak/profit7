<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-07-01 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\listType;

use Assets\htmlHelper;
use Common_get_ready_orders_books;
use LIB\EntityManager\Entites\Magazine;
use LIB\orders\listType\filters\checkMagazine;
use LIB\orders\listType\filters\filtersAbstract;
use LIB\orders\listType\filters\ordersLimit15;
use LIB\orders\listType\filters\quantity21;
use LIB\orders\listType\filters\singleOrder;
use LIB\orders\listType\filters\typeFilter;
use magazine\Compare\OrdersDistanceCompare;
use Memcached;
use orders_containers\OrdersContainers;
use ordersSemaphore;
use stdClass;

/**
 * Description of manageGetOrdersItemsList
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class manageGetOrdersItemsList
{

    protected $bTestMode;
    protected $pDbMgr;
    protected $bGetFromTrain;
    protected $cListType;

    /**
     *
     * @var orderListTypes
     */
    protected $oListTypes;

    /**
     *
     * @var filtersAbstract[]
     */
    protected $aFilters = array();

    /**
     *
     * @var filtersAbstract[]
     */
    protected $aRequiredFilters = array();
    protected $oGetOrderData;
  private $destinationMagazineDeficiencies;

    private $bDisableCheckMagazine;

  public function __construct($pDbMgr, $destinationMagazineDeficiencies = Magazine::TYPE_HIGH_STOCK_SUPPLIES, $bDisableCheckMagazine = false)
    {
        $this->pDbMgr = $pDbMgr;
        $this->bDisableCheckMagazine = $bDisableCheckMagazine;

        if ($_SERVER['REMOTE_ADDR'] == '83.220.102.129' || $_SERVER['REMOTE_ADDR'] == '62.133.145.163') {
            $this->bTestMode = true;
        } else {
            $this->bTestMode = false;
        }
        $oGetOrderData = new getOrderData($this->pDbMgr, $destinationMagazineDeficiencies);
        $this->setDataProvider($oGetOrderData);
        $this->destinationMagazineDeficiencies = $destinationMagazineDeficiencies;
    }

    /**
     * @param OrderDataInterface $oGetOrderData
     */
    protected function setDataProvider(OrderDataInterface $oGetOrderData)
    {
        $this->oGetOrderData = $oGetOrderData;
    }

    /**
     *
     * @param char $cListType
     */
    public function setListType($cListType)
    {
        $this->cListType = $cListType;
    }

    /**
     *
     * @param bool $bGetFromTrain
     */
    public function setGetFromTrain($bGetFromTrain)
    {
        $this->bGetFromTrain = $bGetFromTrain;
    }

    /**
     *
     * @return bool
     */
    public function getGetFromTrain()
    {
        return $this->bGetFromTrain;
    }

    /**
     *
     */
    public function initOrderListType()
    {
        $this->oListTypes = new orderListTypes($this->cListType, $this->bGetFromTrain);
    }

    /**
     *
     * @param filtersAbstract $aFilters
     */
    public function addFilter(filtersAbstract $aFilters)
    {
        $this->aFilters[] = $aFilters;
    }

    /**
     *
     * @param \LIB\orders\listType\iltersAbstract $aFilters
     */
    public function addRequiredFilter(filtersAbstract $aFilters)
    {
        $this->aRequiredFilters[] = $aFilters;
    }

    /**
     *
     * @param array $aOrdersItems
     * @return array
     */
    protected function applyRequiredFilters(array $aOrdersItems)
    {
        global $aConfig;

        $this->aRequiredFilters = $this->checkAddDefaultFilter($this->aRequiredFilters);
        if (!empty($this->aRequiredFilters)) {
            foreach ($this->aRequiredFilters as $filter) {
                $filter->setOrdersItems($aOrdersItems);
                $filter->applyFilter();
                $aOrdersItems = $filter->getFilteredOrdersItems();
            }
        }
//        if ($_SERVER['REMOTE_ADDR'] === '83.220.102.129' || $aConfig['common']['status'] == 'development' ) {
//            if (count($aOrdersItems) > 1 && count($aOrdersItems) < 1000) {
//                // nie przejedzie więcej
//                $ordersDistanceCompare = new OrdersDistanceCompare($this->pDbMgr);
//                $comparedOrderByItems = $ordersDistanceCompare->compare($aOrdersItems);
//                $aOrdersItems = $ordersDistanceCompare->sortOutOrderItems($comparedOrderByItems, $aOrdersItems);
//            }
//        }
        return $aOrdersItems;
    }

    /**
     *
     * @param array $aOrdersItems
     * @return array
     */
    protected function applyFilters(array $aOrdersItems)
    {
      // XXX multilocalize !!!!1
        $this->aFilters = $this->checkAddDefaultFilter($this->aFilters);
        $a = 1;
        if (!empty($this->aFilters)) {
            foreach ($this->aFilters as $filter) {
                $filter->setOrdersItems($aOrdersItems);
                $filter->applyFilter();
                $aOrdersItems = $filter->getFilteredOrdersItems();
            }
        }
        return $aOrdersItems;
    }


    /**
     * @param filtersAbstract[] $filters
     * @return array
     */
    protected function checkAddDefaultFilter($filters) {
      $bExists = false;

        if (true === $this->bDisableCheckMagazine) {
            return $filters;
        }

        $a = 1;
      if (!empty($filters)) {
        foreach ($filters as $filter) {
          if ($filter instanceof checkMagazine) {
            $bExists = true;
          }
        }
      }
      if (false === $bExists) {
          if ($this->destinationMagazineDeficiencies != Magazine::TYPE_LOW_STOCK_SUPPLIES) {
              $checkMagazine = new checkMagazine($this->pDbMgr);
              $checkMagazine->setMagazine(Magazine::TYPE_LOW_STOCK_SUPPLIES);
              $checkMagazine->unsetIFNotExists(true);
              $checkMagazine->unsetIFExists(false);
              $checkMagazine->reserveItem(true);
          } else {
              $checkMagazine = new checkMagazine($this->pDbMgr);
              $checkMagazine->setMagazine(Magazine::TYPE_HIGH_STOCK_SUPPLIES);
              $checkMagazine->unsetIFNotExists(true);
              $checkMagazine->unsetIFExists(false);
              $checkMagazine->reserveItem(true);
              $checkMagazine->addSellPredict(false);
          }

          $mainFilters = [];
          $secondFilters = [];

          // dodajemy po tych filtrach najważniejszych - ilościowe idą później
          foreach ($filters as $iKey => $filter) {
              if (filtersAbstract::TYPE_FILTER_ITEMS === $filter->getTypeFilter()) {
                  $lastFilterIdKey = $iKey;
              }
          }
          $n[] = $checkMagazine;
          // wrzycamy dokładnie w miejscu filtra
          array_splice( $filters, $lastFilterIdKey + 1, 0, $n );

//        $filters[] = $checkMagazine;
      }
      return $filters;
    }

  /**
   *
   * @param date|string $dExpectedShipmentDate
   * @param array $aTransportId
   * @param bool $bAddShowColumns
   * @return array
   */
    public function getCountOrdersGroupByTransport($dExpectedShipmentDate = '', $aTransportId = array(), $bAddShowColumns = false)
    {

        $aOrderItemsList = $this->oGetOrderData->getOrdersItemsListData($this->getGetFromTrain(), $dExpectedShipmentDate, $aTransportId, array(), $bAddShowColumns);
        $aOrdersItems = $this->getOrdersItems($aOrderItemsList);
        if (!empty($aOrdersItems)) {
            // tutaj filtry powinny być wyłączone, poza tymi wymaganymi
            $aOrdersItemsFiltered = $this->applyRequiredFilters($aOrdersItems);
        }
        $aOrdersItemsFiltered = $this->clearEmptyOrders($aOrdersItemsFiltered);
        $aOrdersItemsFiltered = $this->checkAddTransportOrderItems($aOrdersItemsFiltered, $aOrdersItems, false);
        return $this->getOrdersTransportCounts($aOrdersItemsFiltered);
    }

  /**
   * @param string $dExpectedShipmentDate
   * @param array $aTransportId
   * @param bool $bAddShowColumns
   * @return array
   */
    public function getCountOrdersItemsGroupByTransport($dExpectedShipmentDate = '', $aTransportId = array(), $bAddShowColumns = false)
    {
        $aOrderItemsList = $this->oGetOrderData->getOrdersItemsListData($this->getGetFromTrain(), $dExpectedShipmentDate, $aTransportId, array(), $bAddShowColumns);
        $aOrdersItems = $this->getOrdersItems($aOrderItemsList);
        if (!empty($aOrdersItems)) {
            $aOrdersItemsFiltered = $this->applyRequiredFilters($aOrdersItems);
        }
        $aOrdersItemsFiltered = $this->clearEmptyOrders($aOrdersItemsFiltered);
        $aOrdersItemsFiltered = $this->checkAddTransportOrderItems($aOrdersItemsFiltered, $aOrdersItems, false);

        $this->dumpAdminsData($aOrdersItemsFiltered);
        return $this->getOrdersItemsTransportCounts($aOrdersItemsFiltered);
    }

    /**
     *
     * @param date $dExpectedShipmentDate
     * @param array $aTransportId
     * @return array
     */
    public function getOrdersItemsListsType($dExpectedShipmentDate = '', $aTransportId = array(), $bAddShowColumns = false)
    {
        $aOrdersItemsFiltered = array();

        $aOrderItemsList = $this->oGetOrderData->getOrdersItemsListData($this->getGetFromTrain(), $dExpectedShipmentDate, $aTransportId, array(), $bAddShowColumns);
        $aOrdersItems = $this->getOrdersItems($aOrderItemsList);
        $aOrdersItems = $this->checkUnsetInvalidTransport($aOrdersItems);
        if (!empty($aOrdersItems)) {
            $aOrdersItemsFiltered = $this->applyFilters($aOrdersItems);
        }
        $aOrdersItemsFiltered = $this->clearEmptyOrders($aOrdersItemsFiltered);
        $aOrdersItemsFiltered = $this->checkAddTransportOrderItems($aOrdersItemsFiltered, $aOrdersItems, true);

        $this->dumpAdminsData($aOrdersItemsFiltered);
        return $aOrdersItemsFiltered;
    }

    /**
     *
     */
    protected function dumpAdminsData($aOrdersItemsFiltered)
    {

        if ($_SESSION['user']['type'] == '1' || $_SESSION['user']['type'] == '2' || $this->checkUserHavePrivilagesOrders() === true) {
            if (!empty($aOrdersItemsFiltered)) {
                echo '<span style="font-size: 20px;">
          Zamówienia oczekujące na zebranie: <br />' .
                    htmlHelper::toggleShowHide(
                        htmlHelper::array_to_table(
                            htmlHelper::multiarray_to_array($aOrdersItemsFiltered, ['number', 'order_sequence', 'order_number', 'quantity', 'name', 'isbn_plain', 'id', 'pack_number']), ['class' => 'formTable']
                        )
                    );
                echo '</span>';
            }
        }
    }

    /**
     *
     * @return boolean
     */
    protected function checkUserHavePrivilagesOrders()
    {

        $iOrderModuleId = getModuleId('m_zamowienia_listy');
        if (!hasModulePrivileges($iOrderModuleId)) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     * @param array $aOrdersItemsFiltered
     * @param array $aOrdersItems
     * @param bool $bAddNewOrderToList
     * @return array
     */
    protected function checkAddTransportOrderItems($aOrdersItemsFiltered, $aOrdersItems, $bAddNewOrderToList = true)
    {

        if (true === $bAddNewOrderToList) {

            $iCountBefore = count($aOrdersItemsFiltered);

            if ($this->checkPointsOfReceipt() === true) {
                $aOrdersItemsFiltered = $this->checkUnsetInvalidTransport($aOrdersItemsFiltered);
            }


            $iCountAfter = count($aOrdersItemsFiltered);
            $iDiff = $iCountBefore - $iCountAfter;
            if ($iDiff > 0) {
                if ($this->checkAddNewOrderToList() === true) {
                    $aOrdersItemsFiltered = $this->addNewOrderToList($aOrdersItemsFiltered, $aOrdersItems);
                    $aOrdersItemsFiltered = $this->checkUnsetInvalidTransport($aOrdersItemsFiltered);
                }
            } else {
                return $aOrdersItemsFiltered;
            }
        }

        return $aOrdersItemsFiltered;
    }


    /**
     *
     * @param array $aOrdersItems
     * @return int
     */
    protected function sumQuantity($aOrdersItems)
    {

        $iQuantity = 0;
        foreach ($aOrdersItems as $aItem) {
            $iQuantity += $aItem['quantity'];
        }
        return $iQuantity;
    }

    /**
     *
     * @param array $aOrdersItemsFiltered
     * @param array $aOrdersItems
     * @return array
     */
    protected function addNewOrderToList($aOrdersItemsFiltered, $aOrdersItems)
    {

        if (count($aOrdersItemsFiltered) >= ordersLimit15::MAX_FILTER_ORDERS) {
            return $aOrdersItemsFiltered;
        }

        if (count($aOrdersItems) > count($aOrdersItemsFiltered)) {
            foreach ($aOrdersItems as $iOrderId => $aValue) {
                if (isset($aValue[0]) && true === $this->checkFilterType($aValue)) {
                    if ($this->sumQuantity($aValue) < quantity21::MIN_FILTER_ITEMS) {
                        if (!isset($aOrdersItemsFiltered[$iOrderId])) {
                            $aOrdersItemsFiltered[$iOrderId] = $aValue;
                        }
                    }
                }
                if (count($aOrdersItemsFiltered) >= ordersLimit15::MAX_FILTER_ORDERS) {
                    return $aOrdersItemsFiltered;
                }
            }
        }
        return $aOrdersItemsFiltered;
    }

    /**
     * @param $orderItems
     * @return bool
     */
    private function checkFilterType($orderItems) {
        $aOrdersItems[] = $orderItems;

        foreach ($this->aFilters as $filter) {
            if ($filter instanceof typeFilter) {
                $filter->aOrdersItems = $aOrdersItems;
                $filter->applyFilter();
                if (!empty($filter->aOrdersItems)) {
                    return true;
                }
            }
        }
        return false;
    }

    /*
    protected function checkFilterWebsite($websiteId)
    {
        foreach ($this->aFilters as $filter) {
            if ($filter instanceof websiteFilter) {
                if (in_array($websiteId, $filter->aWebsites)) {
                    return true;
                }
            }
        }
        return false;
    }
    */

    /**
     *
     * @global type $aConfig
     * @return \Memcached
     */
    protected function initMemcached()
    {
        global $aConfig;

        if (class_exists('memcached')) {
            $oMemcache = new Memcached();
            $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
            return $oMemcache;
        }
    }

    /**
     *
     * @param array $aOrdersItemsFiltered
     * @return array
     */
    protected function checkUnsetInvalidTransport($aOrdersItemsFiltered)
    {

        $oParent = new stdClass();
        require_once('modules/m_zamowienia_magazyn_zbieranie_tramwaj/Common_get_ready_orders_books.class.php');
        $oCm = new Common_get_ready_orders_books($oParent);
        $oCm->setDbMgr($this->pDbMgr);

        $oMemcached = $this->initMemcached();
        include_once('../LIB/orders_semaphore/ordersSemaphore.class.php');
        $ordersSemaphore = new ordersSemaphore($oMemcached);


        if (!empty($aOrdersItemsFiltered)) {
            foreach ($aOrdersItemsFiltered as $iKey => $aOrder) {
                $bErrDeliveryAddress = $oCm->validateTransportsMethods($iKey);
                if ($bErrDeliveryAddress !== true) {
                    $this->addErrorOrderToContainer($iKey);
                    unset($aOrdersItemsFiltered[$iKey]);
                }
                if (class_exists('memcached')) {
                    $mIsOrderLocked = $ordersSemaphore->isLocked($iKey, 0);
                    if ($mIsOrderLocked !== false) {
                        unset($aOrdersItemsFiltered[$iKey]);
                    }
                }
            }
        }
        return $aOrdersItemsFiltered;
    }

    /**
     *
     * @return boolean
     */
    protected function checkAddNewOrderToList()
    {
        foreach ($this->aFilters as $filter) {
            if ($filter instanceof singleOrder) {
                return false;
            }
        }

        if (is_array($this->cListType) && (in_array('SORTER_COMMON', $this->cListType) || in_array('SORTER_STOCK', $this->cListType) || in_array('SORTER_TRAIN', $this->cListType))) {
            return true;
        }
        return false;
    }

    /**
     *
     * @return boolean
     */
    protected function checkPointsOfReceipt()
    {

        if ($this->cListType === array('SORTER_COMMON', 'PART', 'LINKED_PART', 'LINKED_STOCK')) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param int $iOrderId
     * @return void
     */
    public function addErrorOrderToContainer($iOrderId)
    {

        $oContainers = new OrdersContainers();
        $oContainers->addOrderToContainers($iOrderId, 'T', true);
    }

    protected function clearEmptyOrders($aOrdersItems)
    {

        if (!empty($aOrdersItems)) {
            foreach ($aOrdersItems as $iKey => $aOrder) {
                if (empty($aOrder)) {
                    unset($aOrdersItems[$iKey]);
                }
            }
        }
        return $aOrdersItems;
    }

    /**
     *
     * @param array $aOrderItemsList
     * @param char $cListType
     * @param bool $bGetFromTrain
     * @return array
     */
    protected function getOrdersItems($aOrderItemsList)
    {

        foreach ($aOrderItemsList as $iKey => $aOrderItem) {
            $this->oListTypes->setOrder($aOrderItem[0]);
            $this->oListTypes->setOrderItems($aOrderItem);
            $this->oListTypes->setDb($this->pDbMgr);

            if ($this->oListTypes->checkOrderListType() === true) {
                if ($this->bGetFromTrain === TRUE) {
                    $aTrain = $this->oListTypes->getOrderItemsTran();
                    if (!empty($aTrain)) {
                        $aTrain = $this->markAllAsMagazineType($aTrain, 'train');
                        $aOrdersItems[$iKey] = $aTrain;
                    }
                } else {
                    $aStock = $this->oListTypes->getOrderItemsStock();
                    if (!empty($aStock)) {
                        $aStock = $this->markAllAsMagazineType($aStock, 'stock');
                        $aOrdersItems[$iKey] = $aStock;
                    }
                }
            }
        }
        return $aOrdersItems;
    }

  /**
   * @param $rows
   * @param $sMagazineType
   * @return mixed
   */
    private function markAllAsMagazineType($rows, $sMagazineType) {

      foreach ($rows as &$row) {
        $row['magazine_type'] = $sMagazineType;
      }
      return $rows;
    }

    /**
     *
     * @return array
     */
    public function getOrdersTransportCounts($aOrdersItems)
    {
        $aOrders = array();
        $aTransports = array();

        if (!empty($aOrdersItems)) {
            foreach ($aOrdersItems as $aOrderItems) {
                foreach ($aOrderItems as $oItem) {
                    $aOrders[$oItem['transport_id']][$oItem['order_id']] = $oItem['order_number'];
                }
            }
            foreach ($aOrders as $iTransportId => $aOItems) {
                $aTransports[$iTransportId] = count($aOItems);
            }
        }
        return $aTransports;
    }

    /**
     *
     * @return array
     */
    public function getOrdersItemsTransportCounts($aOrdersItems)
    {
        $aOrders = array();
        $aTransports = array();

        if (!empty($aOrdersItems)) {
            foreach ($aOrdersItems as $aOrderItems) {
                foreach ($aOrderItems as $oItem) {
                    $aOrders[$oItem['transport_id']][$oItem['order_id']] += $oItem['quantity'];
                }
            }

            foreach ($aOrders as $iTransportId => $aOItems) {
                $aTransports[$iTransportId]['count_orders'] = count($aOItems);
                $aTransports[$iTransportId]['count_orders_items'] = array_sum($aOrders[$iTransportId]);
            }
        }
        return $aTransports;
    }

    /**
     * @return filtersAbstract[]
     */
    public function getFilters()
    {
        return $this->aFilters;
    }

    /**
     * @return filtersAbstract[]
     */
    public function getRequiredFilters()
    {
        return $this->aRequiredFilters;
    }
}
