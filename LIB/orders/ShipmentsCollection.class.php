<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-19 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders;
/**
 * Description of ShipmentsCollections
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class ShipmentsCollection {

  private $bTestMode;
  private $pDbMgr;
  CONST TABLE_NAME = 'orders_transport_means';

  public function __construct($pDbMgr, $bTestMode = NULL) {
    
    
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
  }
  
  public function getShipmentReceptionAtPoint() {
    
    $oTransportTable = new \table\keyValue(self::TABLE_NAME);
    return $oTransportTable->getByKeyValueIdDest("type", "reception_at_point");
  }
  
  public function getDeliveryToAddress() {
    
    $oTransportTable = new \table\keyValue(self::TABLE_NAME);
    return $oTransportTable->getByKeyValueIdDest("type", "delivery_to_address");
  }
  
  public function getPersonalReception() {
    
    $oTransportTable = new \table\keyValue(self::TABLE_NAME);
    return $oTransportTable->getByKeyValueIdDest("type", "personal_reception");
  }
}
