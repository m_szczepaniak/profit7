<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-06-16
 * Time: 13:09
 */

namespace orders\MarkUpCookie;

use Common;
use table\keyValue;


class AllUsers implements MarkUpCookieInterface
{

    private $aOfferComparer = [
        'ceneo.pl',
        'opineo.pl',
        'skapiec.pl',
        'okazje.info.pl',
        'radar.pl',
        'nokaut.pl'
    ];

    /**
     * @param $fPriceBrutto
     * @param $aTarrif
     * @param $iDiscount
     * @param bool|true $bCheckCookie
     * @return mixed
     */
    public function getMarkup($fPriceBrutto, $aTarrif, $iDiscount, $bCheckCookie = true)
    {

        $this->oKeyValue = new keyValue('mark_up_cookie');

        $cookieName = $this->oKeyValue->getByIdDestKey('1', 'all_other_cookie_name');

        if (isset($aTarrif['packet']) && true === $aTarrif['packet']) {
            return $aTarrif;
        }

        if ($bCheckCookie === true) {
            if ($this->checkIsAcceptableCookie($aTarrif['product_id']) === false) {
                return $aTarrif;
            }
        }

        $fAdditionalDiscount = Common::formatPrice2($this->oKeyValue->getByIdDestKey(1, 'all_other_mark_up'));

        if ($iDiscount <= 0) {
            $iDiscount = $aTarrif['discount'];
        }
        $iDiscount = $iDiscount - $fAdditionalDiscount;
        if ($iDiscount <= 0) {
            $iDiscount = 0;
        }

        $aTarrif['discount'] = $iDiscount;
        $aTarrif['price_brutto'] = Common::formatPrice2($fPriceBrutto - Common::formatPrice2($fPriceBrutto * (Common::formatPrice2($iDiscount) / 100)));
        $aTarrif['price_netto'] = Common::formatPrice2($aTarrif['price_brutto'] / (1 + $aTarrif['vat'] / 100));
        $aTarrif['discount_value'] = $fPriceBrutto - $aTarrif['price_brutto'];
        return $aTarrif;
    }

    private function checkIsAcceptableCookie($iProductId)
    {
        $aProducts = [];
        $oKeyValue = new keyValue('mark_up_cookie');
        $cookieName = $oKeyValue->getByIdDestKey('1', 'all_other_cookie_name');
        if (isset($_COOKIE[$cookieName])) {
            $aProducts = $this->getProductsFromCookie($_COOKIE[$cookieName]);
            if (isset($aProducts[$iProductId])) {
                return false;
            }
        }
        return true; //  domyślnie narzucaj rabat
    }

    /**
     * @param $str
     * @return string
     */
    private function str_rot47($str)
    {
        return strtr($str, '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~', 'PQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNO');
    }

    /**
     * @param $str
     * @return string
     */
    private function str_rot47_decode($str)
    {
        return strtr($str, 'PQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNO', '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~');
    }


    /**
     *
     */
    public function setAdditionalMarkupCookie()
    {
        if (isset($_GET['id']) && $_GET['id'] > 0 && $this->checkReferer($_SERVER["HTTP_REFERER"])) {
            $iProductId = intval($_GET['id']);
            $this->_setCookie($iProductId);
        }
    }

    /**
     * @param $iProductId
     */
    private function _setCookie($iProductId)
    {
        $aProducts = [];
        $oKeyValue = new keyValue('mark_up_cookie');
        $cookieName = $oKeyValue->getByIdDestKey('1', 'all_other_cookie_name');
        $lifeTime = $oKeyValue->getByIdDestKey('1', 'all_other_lifetime');

        if (!isset($_COOKIE[$cookieName])) {
            $aProducts[$iProductId] = date("Y-m-d H:i:s");
            $cookieValue = $this->setProductsToCookie($aProducts);
            $expire = time() + 60 * 60 * 24 * $lifeTime;
            AddCookie($cookieName, $cookieValue, $expire);
            $_COOKIE[$cookieName] = $cookieValue;
        } else {
            $aProducts = $this->getProductsFromCookie($_COOKIE[$cookieName]);
            $aProducts[$iProductId] = date("Y-m-d H:i:s");
            $_COOKIE[$cookieName] = $this->setProductsToCookie($aProducts);
            setcookie($cookieName, $_COOKIE[$cookieName]);
        }
    }

    /**
     * @param $aProducts
     * @return string
     */
    private function setProductsToCookie($aProducts)
    {
        return $this->str_rot47(serialize($aProducts));
    }

    /**
     * @param $sCookie
     * @return mixed
     */
    private function getProductsFromCookie($sCookie)
    {
        return unserialize($this->str_rot47_decode($sCookie));
    }

    /**
     * @param $refererURL
     * @return bool
     */
    private function checkReferer($refererURL)
    {
        foreach ($this->aOfferComparer as $site) {
            if (stristr($refererURL, $site)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isMarkUpActive()
    {
        return true;
    }

    /**
     * Sprawdzamy czy mamy jakiś produkt który obniża nam cenę
     * @return bool
     */
    public function isAnyMarkUpActiveForCache() {

        $oKeyValue = new keyValue('mark_up_cookie');
        $cookieName = $oKeyValue->getByIdDestKey('1', 'all_other_cookie_name');
        if (isset($_COOKIE[$cookieName])) {
            return true;
        }
        return false;
    }
}