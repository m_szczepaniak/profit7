<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-06-16
 * Time: 13:25
 */

namespace orders\MarkUpCookie;


interface MarkUpCookieInterface
{
    public function setAdditionalMarkupCookie();
    public function isMarkUpActive();
    public function getMarkup($fPriceBrutto, $aTarrif, $iDiscount, $bCheckCookie = true);
    public function isAnyMarkUpActiveForCache();
}