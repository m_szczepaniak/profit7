<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-06-16
 * Time: 13:09
 */

namespace orders\MarkUpCookie;

use Common;
use table\keyValue;


class AllUsers implements MarkUpCookieInterface
{

    protected $oKeyValueMarkUpCookie;
    protected $cookieName;
    protected $lifetime;
    protected $markUp;

    private $aOfferComparer = [
        'ceneo.pl',
        'opineo.pl',
        'skapiec.pl',
        'okazje.info.pl',
        'radar.pl',
        'nokaut.pl'
    ];
    /**
     * @var
     */
    private $websiteCode;

    public function __construct($websiteCode = 'profit24')
    {
        $this->websiteCode = $websiteCode;
        $this->oKeyValueMarkUpCookie = new keyValue('mark_up_cookie', $websiteCode);
    }
    /**
     * @return string
     */
    private function getMarkUpKeyValue() {

        if (!isset($this->markUp)) {
            $this->markUp = $this->oKeyValueMarkUpCookie->getByIdDestKey(1, 'all_other_mark_up');
        }

        return $this->markUp;
    }

    private function getCookieName() {
        if (!isset($this->cookieName)) {
            $this->cookieName = $this->oKeyValueMarkUpCookie->getByIdDestKey('1', 'all_other_cookie_name');
        }
        return $this->cookieName;
    }


    private function getLifetime() {

        if (!isset($this->lifetime)) {
            $this->lifetime = $this->oKeyValueMarkUpCookie->getByIdDestKey('1', 'all_other_lifetime');
        }
        return $this->lifetime;
    }

    /**
     * @param $fPriceBrutto
     * @param $aTarrif
     * @param $iDiscount
     * @param bool|true $bCheckCookie
     * @return mixed
     */
    public function getMarkup($fPriceBrutto, $aTarrif, $iDiscount, $bCheckCookie = true)
    {
        if (isset($aTarrif['packet']) && true === $aTarrif['packet']) {
            return $aTarrif;
        }

        if ($bCheckCookie === true) {
            if ($this->checkIsAcceptableCookie($aTarrif['product_id']) === false) {
                return $aTarrif;
            }
        }

        $fAdditionalDiscount = Common::formatPrice2($this->getMarkUpKeyValue());

        if ($iDiscount <= 0) {
            $iDiscount = $aTarrif['discount'];
        }
        $iDiscount = $iDiscount - $fAdditionalDiscount;
        if ($iDiscount <= 0) {
            $iDiscount = 0;
        }

        $aTarrif['discount'] = $iDiscount;
        $aTarrif['price_brutto'] = Common::formatPrice2($fPriceBrutto - Common::formatPrice2($fPriceBrutto * (Common::formatPrice2($iDiscount) / 100)));
        $aTarrif['price_netto'] = Common::formatPrice2($aTarrif['price_brutto'] / (1 + $aTarrif['vat'] / 100));
        $aTarrif['discount_value'] = $fPriceBrutto - $aTarrif['price_brutto'];
        return $aTarrif;
    }

    private function checkIsAcceptableCookie($iProductId)
    {
        $aProducts = [];
        $cookieName = $this->getCookieName();
        if (isset($_COOKIE[$cookieName])) {
            $aProducts = $this->getProductsFromCookie($_COOKIE[$cookieName]);
            if (isset($aProducts[$iProductId])) {
                return false;
            }
        }
        return true; //  domyślnie narzucaj rabat
    }

    /**
     * @param $str
     * @return string
     */
    private function str_rot47($str)
    {
        return strtr($str, '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~', 'PQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNO');
    }

    /**
     * @param $str
     * @return string
     */
    protected function str_rot47_decode($str)
    {
        return strtr($str, 'PQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNO', '!"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~');
    }


    /**
     *
     */
    public function setAdditionalMarkupCookie()
    {
        if (isset($_GET['id']) && $_GET['id'] > 0 && $this->checkReferer($_SERVER["HTTP_REFERER"])) {
            $iProductId = intval($_GET['id']);
            $this->_setCookie($iProductId);
        }
    }

    /**
     * @param $iProductId
     */
    private function _setCookie($iProductId)
    {
        $aProducts = [];
        $cookieName = $this->getCookieName();
        $lifeTime = $this->getLifetime();

        if (!isset($_COOKIE[$cookieName])) {
            $aProducts[$iProductId] = date("Y-m-d H:i:s");
            $cookieValue = $this->setProductsToCookie($aProducts);
            $expire = time() + 60 * 60 * 24 * $lifeTime;
            AddCookie($cookieName, $cookieValue, $expire);
            $_COOKIE[$cookieName] = $cookieValue;
        } else {
            $aProducts = $this->getProductsFromCookie($_COOKIE[$cookieName]);
            $aProducts[$iProductId] = date("Y-m-d H:i:s");
            $_COOKIE[$cookieName] = $this->setProductsToCookie($aProducts);
            setcookie($cookieName, $_COOKIE[$cookieName]);
        }
    }

    /**
     * @param $aProducts
     * @return string
     */
    private function setProductsToCookie($aProducts)
    {
        return $this->str_rot47(serialize($aProducts));
    }

    /**
     * @param $sCookie
     * @return mixed
     */
    protected function getProductsFromCookie($sCookie)
    {
        return unserialize($this->str_rot47_decode($sCookie));
    }

    /**
     * @param $refererURL
     * @return bool
     */
    private function checkReferer($refererURL)
    {
        if (isset($_GET['cdn']) && $_GET['cdn'] == '1') {
            return true;
        }
        foreach ($this->aOfferComparer as $site) {
            if (stristr($refererURL, $site)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    public function isMarkUpActive()
    {
        return true;
    }

    /**
     * Sprawdzamy czy mamy jakiś produkt który obniża nam cenę
     * @return bool
     */
    public function isAnyMarkUpActiveForCache() {

        $cookieName = $this->getCookieName();
        if (isset($_COOKIE[$cookieName])) {
            return true;
        }
        return false;
    }
}