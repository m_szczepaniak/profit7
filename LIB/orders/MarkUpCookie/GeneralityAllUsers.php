<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-06-16
 * Time: 13:09
 */

namespace orders\MarkUpCookie;

use Common;
use table\keyValue;


class GeneralityAllUsers extends AllUsers implements GeneralityMarkUpCookieInterface
{

    /**
     * @return float
     */
    public function getGeneralMarkup()
    {
        $this->oKeyValue = new keyValue('mark_up_cookie');
        $fAdditionalDiscount = Common::formatPrice2($this->oKeyValue->getByIdDestKey(1, 'all_other_mark_up'));
        return $fAdditionalDiscount;
    }

    public function getProductsIdsOmmitMarkup() {
        $aProducts = [];
        $oKeyValue = new keyValue('mark_up_cookie');
        $cookieName = $oKeyValue->getByIdDestKey('1', 'all_other_cookie_name');
        $aProducts = $this->getProductsFromCookie($_COOKIE[$cookieName]);
        return $aProducts;
    }


    /**
     * @return bool
     */
    public function isMarkUpActive()
    {
        return true;
    }

}