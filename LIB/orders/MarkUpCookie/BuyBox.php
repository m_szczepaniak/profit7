<?php
namespace orders\MarkUpCookie;
use table\keyValue;
use Common;

/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-06-16
 * Time: 13:00
 */
class BuyBox implements MarkUpCookieInterface
{

    public $bIsUsed = false;
    protected $oKeyValueMarkUpCookie;
    protected $cookieName;
    protected $lifetime;
    protected $markUp;

    public function __construct() {
        $this->oKeyValueMarkUpCookie = new keyValue('mark_up_cookie');
    }

    /**
     * @param $aTarrif
     * @param $iDiscount
     * @return mixed
     */
    public function getMarkup($fPriceBrutto, $aTarrif, $iDiscount, $bCheckCookie = true)
    {
        $this->bIsUsed = false;

        if (isset($aTarrif['packet']) && true === $aTarrif['packet']) {
            return $aTarrif;
        }

        $cookieName = $this->getCookieName();

        if ($bCheckCookie === true) {
            if (!isset($_COOKIE[$cookieName])) {
                return $aTarrif;
            }
        }

        $fAdditionalDiscount = Common::formatPrice2($this->getMarkUpKeyValue());

        if ($iDiscount <= 0) {
            $iDiscount = $aTarrif['discount'];
        }
        $iDiscount = $iDiscount - $fAdditionalDiscount;
        if ($iDiscount <= 0) {
            $iDiscount = 0;
        }
        $this->bIsUsed = true;

        $aTarrif['discount'] = $iDiscount;
        $aTarrif['price_brutto'] = Common::formatPrice2($fPriceBrutto - Common::formatPrice2($fPriceBrutto * (Common::formatPrice2($iDiscount) / 100)));
        $aTarrif['price_netto'] = Common::formatPrice2($aTarrif['price_brutto'] / (1 + $aTarrif['vat'] / 100));
        $aTarrif['discount_value'] = $fPriceBrutto - $aTarrif['price_brutto'];
        return $aTarrif;
    }

    public function setAdditionalMarkupCookie()
    {
        if (isset($_GET['abpid']) && $_GET['abpid'] > 0) {
            $this->_setCookie();
        }
    }

    public function isMarkUpActive() {
        $cookieName = $this->getCookieName();
        return isset($_COOKIE[$cookieName]);
    }

    /**
     * @return string
     */
    private function getMarkUpKeyValue() {

        if (!isset($this->markUp)) {
            $this->markUp = $this->oKeyValueMarkUpCookie->getByIdDestKey(1, 'mark_up');
        }

        return $this->markUp;
    }

    private function getCookieName() {
        if (!isset($this->cookieName)) {
            $this->cookieName = $this->oKeyValueMarkUpCookie->getByIdDestKey('1', 'cookie_name');
        }
        return $this->cookieName;
    }


    private function getLifetime() {

        if (!isset($this->lifetime)) {
            $this->lifetime = $this->oKeyValueMarkUpCookie->getByIdDestKey('1', 'lifetime');
        }
        return $this->lifetime;
    }


    public function getCookieValue() {

        $cookieName = $this->getCookieName();
        $cookieValue = $_COOKIE[$cookieName];
        preg_match('/^(PH\d{2,8})$/', $cookieValue, $matches);
        if (isset($matches['1'])) {
            return $matches['1'];
        }
        return null;
    }


    private function _setCookie()
    {
        $cookieName = $this->getCookieName();
        $lifeTime = $this->getLifetime();

        if (!isset($_COOKIE[$cookieName])) {
            $expire = time() + 60 * 60 * 24 * $lifeTime;
            AddCookie($cookieName, 'nn', $expire);
            $_COOKIE[$cookieName] = 'nn';
        }
    }

    /**
     * @return bool
     */
    public function isAnyMarkUpActiveForCache() {

        $cookieName = $this->getCookieName();
        if (isset($_COOKIE[$cookieName])) {
            return true;
        }
        return false;
    }
}