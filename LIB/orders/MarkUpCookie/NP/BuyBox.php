<?php
namespace orders\MarkUpCookie;
use table\keyValue;
use Common;

/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-06-16
 * Time: 13:00
 */
class BuyBox implements MarkUpCookieInterface
{

    public $bIsUsed = false;

    /**
     * @param $aTarrif
     * @param $iDiscount
     * @return mixed
     */
    public function getMarkup($fPriceBrutto, $aTarrif, $iDiscount, $bCheckCookie = true)
    {
        $this->bIsUsed = false;

        $this->oKeyValue = new keyValue('mark_up_cookie');
        $cookieName = $this->oKeyValue->getByIdDestKey('1', 'cookie_name');

        if (isset($aTarrif['packet']) && true === $aTarrif['packet']) {
            return $aTarrif;
        }

        if ($bCheckCookie === true) {
            if (!isset($_COOKIE[$cookieName])) {
                return $aTarrif;
            }
        }

        $fAdditionalDiscount = Common::formatPrice2($this->oKeyValue->getByIdDestKey(1, 'mark_up'));

        if ($iDiscount <= 0) {
            $iDiscount = $aTarrif['discount'];
        }
        $iDiscount = $iDiscount - $fAdditionalDiscount;
        if ($iDiscount <= 0) {
            $iDiscount = 0;
        }
        $this->bIsUsed = true;

        $aTarrif['discount'] = $iDiscount;
        $aTarrif['price_brutto'] = Common::formatPrice2($fPriceBrutto - Common::formatPrice2($fPriceBrutto * (Common::formatPrice2($iDiscount) / 100)));
        $aTarrif['price_netto'] = Common::formatPrice2($aTarrif['price_brutto'] / (1 + $aTarrif['vat'] / 100));
        $aTarrif['discount_value'] = $fPriceBrutto - $aTarrif['price_brutto'];
        return $aTarrif;
    }

    public function setAdditionalMarkupCookie()
    {
        if (isset($_GET['abpid']) && $_GET['abpid'] > 0) {
            $this->_setCookie();
        }
    }

    public function isMarkUpActive() {
        $oKeyValue = new keyValue('mark_up_cookie');
        $cookieName = $oKeyValue->getByIdDestKey('1', 'cookie_name');
        return isset($_COOKIE[$cookieName]);
    }

    private function _setCookie()
    {
        $oKeyValue = new keyValue('mark_up_cookie');
        $cookieName = $oKeyValue->getByIdDestKey('1', 'cookie_name');
        $lifeTime = $oKeyValue->getByIdDestKey('1', 'lifetime');

        if (!isset($_COOKIE[$cookieName])) {
            $expire = time() + 60 * 60 * 24 * $lifeTime;
            AddCookie($cookieName, 'nn', $expire);
            $_COOKIE[$cookieName] = 'nn';
        }
    }

    /**
     * @return bool
     */
    public function isAnyMarkUpActiveForCache() {

        $oKeyValue = new keyValue('mark_up_cookie');
        $cookieName = $oKeyValue->getByIdDestKey('1', 'cookie_name');
        if (isset($_COOKIE[$cookieName])) {
            return true;
        }
        return false;
    }
}