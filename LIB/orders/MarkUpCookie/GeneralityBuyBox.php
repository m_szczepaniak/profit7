<?php
namespace orders\MarkUpCookie;
use table\keyValue;
use Common;

/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-06-16
 * Time: 13:00
 */
class GeneralityBuyBox implements GeneralityMarkUpCookieInterface
{
    protected $oKeyValue;
    private $bIsUsed = false;


    /**
     * @return mixed
     */
    public function getGeneralMarkup()
    {
        $this->oKeyValue = new keyValue('mark_up_cookie');

        $this->bIsUsed = false;
        $cookieName = $this->oKeyValue->getByIdDestKey('1', 'cookie_name');
        if (!isset($_COOKIE[$cookieName])) {
            return 0;
        }


        $fAdditionalDiscount = Common::formatPrice2($this->oKeyValue->getByIdDestKey(1, 'mark_up'));
        $this->bIsUsed = true;

        return $fAdditionalDiscount;
    }

    /**
     * @return bool
     */
    public function isMarkUpActive() {
        $oKeyValue = new keyValue('mark_up_cookie');
        $cookieName = $oKeyValue->getByIdDestKey('1', 'cookie_name');
        return isset($_COOKIE[$cookieName]);
    }

    /**
     * @return bool
     */
    public function isIsUsed()
    {
        return $this->bIsUsed;
    }

    /**
     * @param bool $bIsUsed
     */
    public function setIsUsed($bIsUsed)
    {
        $this->bIsUsed = $bIsUsed;
    }
}