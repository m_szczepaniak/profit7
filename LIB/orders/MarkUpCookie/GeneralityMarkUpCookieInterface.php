<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-06-16
 * Time: 13:25
 */

namespace orders\MarkUpCookie;


interface GeneralityMarkUpCookieInterface
{
    public function isMarkUpActive();
    public function getGeneralMarkup();
}