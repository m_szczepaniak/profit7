<?php
namespace LIB\orders\counter;

use DatabaseManager;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 07.08.18
 * Time: 10:35
 */
class OrdersCounter {
    const TYPE_SEND_SORTOUT = 'send_sortout';

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    public function __construct(DatabaseManager $pDbMgr)
    {

        $this->pDbMgr = $pDbMgr;
    }

    /**
     * @param $type
     * @return bool
     * @throws \Exception
     */
    public function incCounter($type) {
        $nameCounter = $type;
        if ($nameCounter != '') {
            $columnName = $nameCounter.'_quantity';
            $sSql = '
INSERT INTO orders_counter
(created, '.$columnName.') VALUES (CURDATE(), 1)
ON DUPLICATE KEY UPDATE created = CURDATE(), '.$columnName.' = '.$columnName.' + 1
                     ';
            if (false === $this->pDbMgr->Query('profit24', $sSql)) {
                return false;
            }
            return true;
        }
        throw new \Exception(_('Błąd zwiększania licznika '.$type));
    }

    /**
     * @param $type
     * @param $date
     * @return mixed
     */
    public function getCounter($type, $date){

        $columnName = $type.'_quantity';
        $sSql = 'SELECT '.$columnName.'
                 FROM orders_counter
                 WHERE created = "'.$date.'"';
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }
}