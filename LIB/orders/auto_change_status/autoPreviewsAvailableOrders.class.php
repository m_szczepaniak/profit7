<?php
/**
 * Klasa obsługi automatycznego przechodzenia zmówień pomiędzy zamówieniami
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @property DatabaseManager $pDbMgr
 */
namespace orders\auto_change_status;
class autoPreviewsAvailableOrders implements iAutoChangeStatus {
    private $pDbMgr;
    private $bTestMode;


    public function __construct($bTestMode, $pDbMgr) {
        $this->pDbMgr = $pDbMgr;
        $this->bTestMode = $bTestMode;


        /*

        $this->pDbMgr->BeginTransaction('profit24');
        // pojawiła się zapowiedź w źródle - zmiana statusu wewn. zamówień z 'zapowiedzi' na '---'
        $aOrdersIds = $this->getOrdersItemsDataToUpdate();
        $aInformData = $this->UpdateOrdersItems($aOrdersIds);
        if ($aInformData === false) {
          echo 'błąd zmiany statusów wewnętrznych zamówień z "zapowiedzi" na "---"';
          $this->pDbMgr->RollbackTransaction('profit24');
        } else {
          $this->informAboutStatusChanged($aInformData);
          $this->pDbMgr->CommitTransaction('profit24');
        }
        */
    }

    /**
     * Metoda pobiera składowe zamówień które są zapowiedziami oraz wpadły
     *  jako zapowiedzi, a teraz są już dostęne i nie są zapowiedziami
     *
     * Zamówienia:
     *  nie anulowane, i nie zrealizowane,
     *  oraz jeśli składowa zamówienia jest zapowiedzią
     *  oraz których składowa jeśli istnieje current_shipment_date <= dzisiaj, jeśli nie to shipment_date <= dzisiaj
     *  oraz których status składowej jest wcześniejszy niż "jest potwierdzone"
     *
     * @return bool
     */
    public function getOrdersItemsDataToUpdate() {

        $sSql = "SELECT OI.id
             FROM orders_items AS OI
             JOIN products AS P
              ON P.id = OI.product_id AND prod_status = '1' AND is_previews = '0'
             JOIN orders AS O
              ON OI.order_id = O.id
             WHERE 
              OI.status = '-1' AND 
              OI.preview = '1' AND 
              O.order_status <> '5' AND 
              O.order_status <> '4' AND 
              OI.deleted <> '1'
             ";
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }// end of getOrdersItemsDataToUpdate() method


    /**
     * Metoda dokonuje pobrania składowych zamówień które stały się dostepne
     *  w dowolnym źródle i zmienia jego status z 'zapowiedzi' na '---'
     *
     * @ticket 17.12.2012/M_CHUDY
     */
    public function UpdateOrdersItems($aOrdersIds) {


        if ($this->bTestMode !== TRUE) {
            if (!empty($aOrdersIds)) {
                if ($this->_setAvailablePreviewsOrders($aOrdersIds) === false) {
                    return false;
                }
                // recount
                if ($this->_recountOrdersItems($aOrdersIds) === false) {
                    return false;
                }

                $aInfoData = $this->_getOrderNumberProductNameByOrderItemsIds($aOrdersIds);
                $aTMPItems = array();
                if (!empty($aInfoData)) {
                    foreach ($aInfoData as $iKey => $aItem) {
                        $aTMPItems[$iKey] = array(
                            'new_status' => 0,
                            'old_status' => -1,
                            'order' => $aItem
                        );
                    }
                    return $aTMPItems;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } else {
            dump($aOrdersIds);
        }
    }// end of UpdateOrdersItems() method


    /**
     *
     * @param type $aOrderData
     */
    public function UpdateOrders($aOrderData) {}


    /**
     * Metoda zapisuje do bazy danych informacje na temat zmienionego statusu zamówień
     *
     * @param int $iOldStatus
     * @param int $iNewStatus
     * @param array $aInfoData
     * @return bool
     */
    public function informAboutStatusChanged($aInfoData) {
        global $aConfig;

        include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
        if (isset($aInfoData) && !empty($aInfoData) && is_array($aInfoData)) {
            foreach ($aInfoData as $aOrderItem) {

                $aValues = array(
                    'created' => 'NOW()',
                    'created_by' => 'job_auto_status',
                    'content' => serialize($aOrderItem)
                );
                if ($this->pDbMgr->Insert('profit24', 'events_logs_orders', $aValues) === false) {
                    return false;
                }
            }
        }
        return true;
    }// end of informAboutStatusChanged() method


    /**
     * Metoda przelicza status produktu na nowo
     *
     * @param type $aOrderItems
     * @return boolean
     */
    private function _recountOrdersItems($aOrderItems) {

        $_GET['jobs']['auto_orders'] = true;
        include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/OrderRecount.class.php');
        $oOrderRecount = new \OrderRecount();

        $sSql = "SELECT DISTINCT order_id 
             FROM orders_items 
             WHERE id IN (".implode(',', $aOrderItems).")";
        $aOrdersIds = $this->pDbMgr->GetCol('profit24', $sSql);

        foreach ($aOrdersIds as $iOId) {

            if ($oOrderRecount->recountOrder($iOId) === false) {
                return false;
            }
        }
        return true;
    }// end of _recountOrdersItems() method


    /**
     * Metoda zmienia status składowej zamówienia z 'zapowiedzi' na '---'
     *
     * @param array $aOrdersIds
     * @return boolean
     */
    private function _setAvailablePreviewsOrders($aOrdersIds) {

        if (!empty($aOrdersIds)) {



            $aValues = array(
                'status' => '0'
            );
            return $this->pDbMgr->Update('profit24', 'orders_items', $aValues, " id IN (".implode(',', $aOrdersIds).")");


        } else {
            return false;
        }
    }// end of _setAvailablePreviewsOrders() method

    /**
     * Metoda pobiera numer zamówienia i nazwę produktu na podstawie
     *  przekazanego id-ków składowej zamówienia
     *
     * @param array $aOrderItems
     * @return boolean
     */
    private function _getOrderNumberProductNameByOrderItemsIds($aOrderItems) {

        if (!empty($aOrderItems)) {
            $sSql = "SELECT O.order_number, O.id, P.name, P.isbn_plain
               FROM orders_items AS OI
               JOIN orders AS O
                ON O.id = OI.order_id
               JOIN products AS P
                ON P.id = OI.product_id
               WHERE OI.id IN (".implode(',', $aOrderItems).")";
            return $this->pDbMgr->GetAll('profit24', $sSql);
        } else {
            return false;
        }
    }
}
