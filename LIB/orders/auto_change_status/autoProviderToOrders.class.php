<?php
/**
 * Klasa zmienia automatycznie zamówione produkty do określonych źródeł
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-09-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders\auto_change_status;

use Common;
use DatabaseManager;
use Exception;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use Memcached;
use omniaCMS\lib\Products\ProductsStockReservations;
use OrderRecount;
use orders_alert\OrdersAlert;
use ordersSemaphore;
use ShipmentDateExpected;
class autoProviderToOrders implements iAutoChangeStatus {
  /**
 *
 * @var DatabaseManager 
 */
  private $pDbMgr;
  private $bTestMode;
  private $aInformData;
  public $criticalError = null;

    /**
     * @var ReservationManager
     */
  private $reservationManager;
  /**
   *
   * @var OrderRecount
   */
  private $oOrderRecount;
  /**
   *
   * @var ProductsStockReservations
   */
  private $oReservations;
  private $aEqualExternalProviders;
  private $aExtEqualProviders = array(
      'ateneum', 'azymut', 'dictum', 'helion', 'siodemka', 'olesiejuk', 'platon', 'panda'
  );
  const AUTO_USER_ID = 85;
  
  /**
   *
   * @var ordersSemaphore
   */
  private $ordersSemaphore;
  
  function __construct($bTestMode, $pDbMgr) {
    global $aConfig; // to musi tu zostać
    
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    $this->_UpdateStartupSet();

    include_once('OrderRecount.class.php');
//    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/OrderRecount.class.php');
    $this->oOrderRecount = new OrderRecount();
    $this->oReservations = new ProductsStockReservations($this->pDbMgr);
    
    
    $oMemcached = $this->initMemcached();
    include_once(__DIR__.'/../../orders_semaphore/ordersSemaphore.class.php');
    $this->ordersSemaphore = new ordersSemaphore($oMemcached);
  }
  
  /**
   * 
   * @global type $aConfig
   * @return Memcached
   */
  private function initMemcached() {
    global $aConfig;
    
    if (class_exists('memcached')) {
      $oMemcache = new Memcached();
      $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
      return $oMemcache;
    }
  }

  public function UpdateOrders($aOrderData) {
    
  }

    /**
     * @param ReservationManager $reservationManager
     */
    public function setReservationManager(ReservationManager $reservationManager)
    {
        $this->reservationManager = $reservationManager;
    }

    /**
     * @return ReservationManager
     */
    public function getReservationManager()
    {
        if (null == $this->reservationManager){
            $this->reservationManager = new ReservationManager($this->pDbMgr);
        }

        return $this->reservationManager;
    }

    private function preparOrdersItems($ordersIds)
    {
        $beforeOrdersItems = [];

        if(empty($ordersIds)){
            return $beforeOrdersItems;
        }

        $dataProvider = $this->getReservationManager()->getDataProvider();

        foreach($ordersIds as $orderId) {
            $orderItemsData = $dataProvider->getReservationsByOrderId($orderId);

            $beforeOrdersItems[$orderId] = $orderItemsData;
        }

        return $beforeOrdersItems;
    }

  /**
   * Metoda zmienia składowe zamówienia
   *  Ustalamy ogólne priotytety źródeł
   *  Ogólne założenie:
   *    najpierw jest źródło Nasze G, kolejno inne nasze źródła
   *    następnie jest mapowanie sztywne zdef. dla każdego wydawnictwa
   *    następnie są ustalone w kolejności źródła
   *
   * Nalezy pierwsze wystąpienie źródła równoważnego,
   *        zastąpić wszystkimi równoważnymi,
   *        w kolejności od najniższej ceny zakupu
   *
   *
   * @param array $aOrderItemData
   * @param bool $singleElementTransaction
   * @return bool
   *
  public function UpdateOrdersItems($aOrderItemData) {

    $oAlert = new OrdersAlert();

    $uniqueOrdersIds = array_unique(ArrayHelper::arrayColumn($aOrderItemData, 'order_id'));
    $beforeReservations = $this->preparOrdersItems($uniqueOrdersIds);

    $aInternalProviders = $this->_getInternalProviders();
    $aExternalProviders = $this->_getMappedExternalProviders();
    if (!empty($aOrderItemData)) {

      foreach ($aOrderItemData as $aItemData) {

        $aItemData = $this->mergeActualProductStock($aItemData);
        if ($aItemData['omit_internal_provider'] == '1' || ($aItemData['check_external_border_time'] == '1' && ($aItemData['have_additional_day'] == '1' || $aItemData['have_additional_day'] == '2'))) {
          $bCheckBorderTimeProvider = ($aItemData['check_external_border_time'] == '1' && $aItemData['have_additional_day'] == '1') ? TRUE : FALSE;
          if ($this->proceedExternal($aItemData, $aExternalProviders, $bCheckBorderTimeProvider) === FALSE) {
            $this->proceedInternal($aItemData, $aInternalProviders);
          }
        } else {
          // najpierw wewnętrzne później zewnętrzne
          if ($this->proceedInternal($aItemData, $aInternalProviders) === FALSE) {
            $this->proceedExternal($aItemData, $aExternalProviders);
          }
        }
      }

      $reservationResult = $this->proceedOrdersReservations($uniqueOrdersIds, $beforeReservations, $oAlert, $isAutomat);
    }

    if (false == $reservationResult){
        return false;
    }

    return $this->aInformData;
  }// end of UpdateOrdersItems() method
  */
    
  /**
   * 
   * @param array $groupedItems
   * @return array
   */
  private function removeLockedOrdersItems($groupedItems) {
    $aOrdersIds = array_keys($groupedItems);
    foreach ($aOrdersIds as $iOId) {
      if ($this->ordersSemaphore->isLocked($iOId, self::AUTO_USER_ID) !== false) {
        unset($groupedItems[$iOId]);
      }
    }
    
    return $groupedItems;
  }

  /**
   * Metoda dodana w trakcie prac nad nowym systemem rezerwacji, domyslnie ma zastapic UpdateOrdersItems kiedy bedzie bezuzyteczny
   * @param $aOrderItemData
   * @param bool $isSingle
   * @return bool
   */
  public function UpdateOrdersItemsAndReservations($groupedItems, $isAutomat = false)
  {
    //$aOrderItemData
    $oAlert = new OrdersAlert();

    $groupedItems = $this->removeLockedOrdersItems($groupedItems);

    foreach($groupedItems as $orderId => $aItems){

//      $this->pDbMgr->BeginTransaction('profit24');
      $this->ordersSemaphore->lockOrder($orderId, self::AUTO_USER_ID);

      $reservationManager = new ReservationManager($this->pDbMgr);

      try {
        $dataProvider = $reservationManager->getDataProvider();

        $beforeReservations = $dataProvider->getReservationsByOrderId($orderId);;

        $aInternalProviders = $this->_getInternalProviders();
        $aExternalProviders = $this->_getMappedExternalProviders();

        foreach ($aItems as $aItemData) {


          $aItemData = $this->mergeActualProductStock($aItemData);
          if ($aItemData['omit_internal_provider'] == '1' || ($aItemData['check_external_border_time'] == '1' && ($aItemData['have_additional_day'] == '1' || $aItemData['have_additional_day'] == '2'))) {
            $bCheckBorderTimeProvider = ($aItemData['check_external_border_time'] == '1' && $aItemData['have_additional_day'] == '1') ? TRUE : FALSE;
            if ($this->proceedExternal($aItemData, $aExternalProviders, $bCheckBorderTimeProvider) === FALSE) {
              $this->proceedInternal($aItemData, $aInternalProviders);
            }
          } else {
            // najpierw wewnętrzne później zewnętrzne
            if ($this->proceedInternal($aItemData, $aInternalProviders) === FALSE) {
              $this->proceedExternal($aItemData, $aExternalProviders);
            }
          }


            try {
                $sellPredict = new \LIB\EntityManager\Entites\SellPredict($this->pDbMgr);
                $sellPredict->autoSellPredictOrderItem($aItemData['id']);
            } catch (\Exception $e) {}
        }

        $reservationResult = $reservationManager->createStreamsoftReservation($beforeReservations, $orderId);

        // zamowienie czesciowe, wyswielamy info
        if(false === $reservationResult){
          if (true === $isAutomat){
            Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', 'raporty@profit24.pl', 'BŁĄD rezerwacji ', $reservationManager->getErrorMessage());
            $oAlert->createAlert('NULL', 'automat-rezerwacje', $reservationManager->getErrorMessage());
            $oAlert->addAlerts();
          } else {
            Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', 'raporty@profit24.pl', 'BŁĄD rezerwacji ', $reservationManager->getErrorMessage());
//            throw new Exception($reservationManager->getErrorMessage());
          }
        }

        $this->ordersSemaphore->unlockOrder($orderId, self::AUTO_USER_ID);
//        $this->pDbMgr->CommitTransaction('profit24');
        $this->oOrderRecount->recountOrder($orderId);
      }
      catch (Exception $e) {

//        $this->pDbMgr->RollbackTransaction('profit24');
        $reservationManager->rollbackChanges();
        
        $this->ordersSemaphore->unlockOrder($orderId, self::AUTO_USER_ID);

        if(true === $isAutomat) {
          Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', 'raporty@profit24.pl', 'BŁĄD rezerwacji ', $e->getMessage());
          $oAlert->createAlert('NULL', 'automat-rezerwacje', $e->getMessage());
          $oAlert->addAlerts();
        } else {
          Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', 'raporty@profit24.pl', 'BŁĄD rezerwacji ', $e->getMessage());
          $oAlert->addAlerts();
//          throw new Exception($reservationManager->getErrorMessage());
        }
      }
    }
    
    $this->ordersSemaphore->unlockAllOrders(self::AUTO_USER_ID);

    $oAlert->addAlerts();
    if (false == $this->aInformData) {
//      throw new Exception("Wystapily bledy podczas odznaczania zmaowienia");
    }

    $this->informAboutStatusChanged($this->aInformData);
  }

  
  /**
   * Metoda pobiera i łączy aktualne dane na temat stanu produktu
   * 
   * @param array $aItemData
   * @return array
   */
  private function mergeActualProductStock($aItemData) {
    
    $sSql = 'SELECT *
             FROM products_stock
             WHERE id = '.$aItemData['product_id'];
    $aPS = $this->pDbMgr->GetRow('profit24', $sSql);
    unset($aPS['id']);
    $aItemData = array_merge($aItemData, $aPS);
    return $aItemData;
  }// end of mergeActualProductStock() method
  
  
  /**
   * Metoda przetrwarza zewnętrzne źródła
   * 
   * @param type $aItemData
   * @param type $aExternalProviders
   * @return boolean
   */
  public function proceedExternal($aItemData, $aExternalProviders, $bCheckBorderTimeProvider = FALSE) {
    
    // brak źródła - lecimy ze źródłami zewnętrznymi
    $aProductExternalProviders = $this->_getMappedProvidersByPublishers($aItemData['publisher_id'], $aExternalProviders);
    $aMergedProductExternalProviders = $this->_mergeEqualExternalSources($aProductExternalProviders, $aItemData);
    $iMatchedExternalProviderId = $this->_matchExternalProviders($aItemData, $aMergedProductExternalProviders, $bCheckBorderTimeProvider);
    
    /*XXX do odkomentowania*/
    if ($iMatchedExternalProviderId > 0) {
      $this->setOrderItemSource($aItemData, FALSE, $iMatchedExternalProviderId, $aMergedProductExternalProviders['ID_'.$iMatchedExternalProviderId]['stock_col']);
      return TRUE;
    } else {
      // brak odpowiedniego źródła
      // pozycja do odznaczenia ręcznie
      $this->setLocketAutoOrder($aItemData);
      return FALSE;
    }
  }// end of proceedExternal() method
  
  
  /**
   * Metoda przetwarza wewnętrzne zamówienie
   * 
   * @param array $aItemData
   * @param array $aInternalProviders
   * @return boolean|null
   */
  public function proceedInternal($aItemData, $aInternalProviders) {
    
    $iMatchedInternalProvider = $this->_matchInternalProviders($aItemData, $aInternalProviders);
    if ($iMatchedInternalProvider !== '' && is_numeric($iMatchedInternalProvider)) {

      // ustawiamy źródło
      $this->setOrderItemSource($aItemData, TRUE, $iMatchedInternalProvider, $aInternalProviders[$iMatchedInternalProvider]);
      return TRUE;
    } elseif ($iMatchedInternalProvider === NULL) {
      // możesz sprawdzać następne elementy
      return FALSE;
      
    } elseif ($iMatchedInternalProvider === FALSE) {

      // pozycja do odznaczenia ręcznie - dostepna w sumie w źródłach wewnętrznych
      $this->setLocketAutoOrder($aItemData);
      return TRUE;
    } else {
      echo 'INTERNAL ERROR 1';
      exit(1);
    }
  }// end of proceedInternal() method
  
  
  /**
   * Metoda ustawia źródło dla zamówionego produktu
   * 
   * @param array $aItemData
   * @param bool $bInternal
   * @param int $iMatchedProviderId
   * @param string $sMatchedProviderName
   * @return boolean
   */
  public function setOrderItemSource($aItemData, $bInternal, $iMatchedProviderId, $sMatchedProviderName) {
    if (empty($sMatchedProviderName)) {
      $sMatchedProviderName = $this->pDbMgr->GetOne('profit24', 'SELECT name FROM external_providers WHERE id = '.$iMatchedProviderId);
    }
    $sMatchedProviderName = str_replace(array('_status', '_stock'), '', $sMatchedProviderName);
    
    if ($this->bTestMode === TRUE) {
      $sStr = ' ŹRÓDŁO: '.$sMatchedProviderName;
      if (empty($sMatchedProviderName)) {
        $sStr = ' SZTYWNE ŹRÓDŁO: '.$sMatchedProviderName;
      }
      echo 'ZAMÓWIENIE: '.$aItemData['order_number'].' POZYCJA: '.$aItemData['isbn_plain'].' USTAWIAMY '.$sStr."<hr />";
    } else {
      // ustawiamy status
      
      $aItemTMP = $aItemData;
      $aItemTMP['id'] = $aItemTMP['order_id'];
      
      if ($bInternal === TRUE) {
        // jest niepotwierdzone
        $iNewOrderItemStatus = '3';
      } else {
        // do zamowienia
        $iNewOrderItemStatus = '1';
      }
      
      $aOrderItemValues = array(
          'status' => $iNewOrderItemStatus,
          'source' => $iMatchedProviderId
      );
//      dump($aOrderItemValues);
//      dump(' id = '.$aItemData['id'].' AND order_id = '.$aItemData['order_id'].' LIMIT 1');
//      ini_set('display_erros', 'On');
//      error_reporting(E_ALL);
//      var_dump($this->pDbMgr->Update('profit24', 'orders_items', $aOrderItemValues, ' id = '.$aItemData['id'].' AND order_id = '.$aItemData['order_id'].' LIMIT 1'));
      if ($this->oReservations->createReservation($iMatchedProviderId, $aItemData['product_id'], $aItemData['order_id'], $aItemData['id'], $aItemData['quantity']) === FALSE) {
        return FALSE;
      }
      dump($aOrderItemValues);
      if ($this->pDbMgr->Update('profit24', 'orders_items', $aOrderItemValues, ' id = '.$aItemData['id'].' AND order_id = '.$aItemData['order_id'].' LIMIT 1') === FALSE) {
        return FALSE;
      }
              
      // zmiana statusu
      $this->aInformData[] = array(
          'new_status' => $iNewOrderItemStatus,
          'old_status' => '0',
          'source' => $iMatchedProviderId,
          'source_name' => $sMatchedProviderName,
          'order' => $aItemTMP
      );
      if ($this->oOrderRecount->recountOrder($aItemData['order_id'], false) === FALSE) {
        return FALSE;
      }
    }
  }
  
  
  /**
   * Blokujemy zamówienie przed podnownym obrobieniem
   * 
   * @param array $aItemData
   * @return bool
   */
  public function setLocketAutoOrder($aItemData) {
    
    if ($this->bTestMode === TRUE) {
      echo ('BLOKUJEMY ZAMÓWIENIE PRZED PPO: '.$aItemData['order_number'].' POZYCJA: '.$aItemData['isbn_plain'].'')."<hr />";
    } else {
      $aValues = array(
          'locked_auto_order' => '1'
      );
      return $this->pDbMgr->Update('profit24', 'orders', $aValues, ' id='.$aItemData['order_id']);
    }
  }// end of setLocketAutoOrder() method
  
  
  /**
   * Metoda dopasowuje zewnętrznego dostawcę do zamówionego produktu
   * 
   * @param array $aItemData
   * @param array $aExternalProviders
   * @return boolean
   */
  public function _matchExternalProviders($aItemData, $aExternalProviders, $bCheckBorderTimeProvider = FALSE) {
    
    foreach ($aExternalProviders as $aProviderData) {
      $sProviderId = $aProviderData['id'];
      $sStockColName = $aProviderData['stock_col'];

      // sprawdźmy godzinę graniczną 
      if ($bCheckBorderTimeProvider === TRUE && $aProviderData['border_time'] != '') {
        if ($this->_checkBorderTime($aProviderData['border_time']) === FALSE) {
          // godzina tego źróda została została przekroczona
          // uciekamy z tego źródła
          continue;
        }
      }
      
      if (!empty($sStockColName) && $sStockColName != '') {
        $sSourceSource = $this->getProductsStockSourceSymbolByColName($sStockColName);
        // sprawdzamy cenę zakupu
        if ($aItemData['promo_price_brutto'] < $aItemData[$sSourceSource.'_wholesale_price'] ) {
          // uciekamy od razu, cena zakupu jest zbyt wysoka
          return FALSE;
        }
      
        $mMatched = $this->_doCheckAvailableAndQuantity($aItemData[$sSourceSource.'_status'], $aItemData[$sStockColName], $aItemData['quantity'], $sProviderId);
        if ($mMatched !== FALSE) {
          return $mMatched;// dopasowało dostępność
        }
      } else {
        return intval(str_replace('ID_', '', $sProviderId));
      }
    }
    return FALSE;
  }// end of _matchExternalProviders() method
  
  
  /**
   * helion_stock, abe_status,
   * teraz można by było użyć $aProviderData['symbol'], ale trzeba wszystkie źródła usystematyzować
   * 
   * @param type $sStockColName
   * @return type
   */
  private function getProductsStockSourceSymbolByColName($sStockColName) {
    
    return preg_replace('/(.*)_[^_]*$/', '$1', $sStockColName); 
  }// end of getProductsStockSourceSymbolByColName() method
  
  
  /**
   * Metoda sprawdza czy godzina graniczna została przekroczona
   * 
   * @param time - string $dBorderTime
   * @return boolean
   */
  private function _checkBorderTime($dBorderTime) {
    
    $iBorderTime = floatval(str_replace(':', '', $dBorderTime));
    $iNowTime = floatval(date('His'));
    if ($iBorderTime > $iNowTime) {
      return TRUE;
    }
    return FALSE;
  }// end of _checkBorderTime() method
  
  
  /**
   * Metoda sprawdza czy pozycja w danym źródle, jest dostępna 
   *  oraz czy jej ilość jest wystarczająca, zwraca id dostawcy
   * 
   * @param string $sSourceStatus - dostepność w źródle
   * @param string $sQuantityValue - ilość w źródle opisana stringiem
   * @param int $iOrderedQuantity - zamówiona ilość
   * @param string $sProviderId - id dostawcy
   * @return mixed int|FALSE
   */
  private function _doCheckAvailableAndQuantity($sSourceStatus, $sQuantityValue, $iOrderedQuantity, $sProviderId) {
    if (isset($sSourceStatus) 
         &&
        (intval($sSourceStatus) == '3' || intval($sSourceStatus) == '1')
        ) {

      if ($this->_checkQuanty($sQuantityValue, intval($iOrderedQuantity)) === TRUE) {
        return intval(str_replace('ID_', '', $sProviderId));
      }
    }
    return FALSE;
  }// end of _doCheckAvailableAndQuantity() method
  
  
  /**
   * Metoda sprawdza czy ilość jest wystarczająca w źródle
   * 
   * @param string $sQuantityValue 20-70 | >30 | <10 | 0 | 1-9 | >50 | 20-30 | 0-3
   * @param int $iOrderedQuantity
   * @return bool
   */
  public function _checkQuanty($sQuantityValue, $iOrderedQuantity) {
    // matchowanie ilości
    $aRangeMatches = array();
    $aGreaterThenMatches = array();
    $aLessThenMatches = array();
    $aNumber = array();
    $iLimitOverflow = 5; // zmiana uzgodniona Ł. Głuchowski, K. Małż, M. Chudy - 12.45 2013.10.11
    
    // DETECT
    
    // zakres
    preg_match('/^(\d+)-(\d+)$/', $sQuantityValue, $aRangeMatches);
    if (!empty($aRangeMatches)) {
      
      $iMax = $aRangeMatches[2];
      if ($iOrderedQuantity <= $iMax) {
        return TRUE;
      } else {
        // niewystarczająca ilość
        return FALSE;
      }
    }
    
    // wiecej niż
    preg_match('/^>(\d+)$/', $sQuantityValue, $aGreaterThenMatches);
    if (!empty($aGreaterThenMatches)) {
      
      // jeśli zamówiono wiecej o 1 niż wartość maksymalna, czyli np. zam 71, a jest >70, to już nie obrabiamy
      // ustalono z Marcinem i Łukaszem
      $bGTIsEnough = (($iOrderedQuantity < ($aGreaterThenMatches[1] + $iLimitOverflow)) ? TRUE : FALSE);
      if ($bGTIsEnough === TRUE) {
        return TRUE;
      } else {
        return FALSE;
      }
    }
    
    // mniej niż
    preg_match('/^<(\d+)$/', $sQuantityValue, $aLessThenMatches);
    if (!empty($aLessThenMatches)) {
      
      $bLTIsEnough = (($iOrderedQuantity < $aLessThenMatches[1]) ? TRUE : FALSE);
      if ($bLTIsEnough === TRUE) {
        return TRUE;
      } else {
        return FALSE;
      }
    }
    
    preg_match('/^(\d+)$/', $sQuantityValue, $aNumber);
    if (!empty($aNumber)) {
      
      if ($iOrderedQuantity <= $aNumber['1']) {
        return TRUE;
      } else {
        return FALSE;
      }
    }
    return FALSE;
  }// end of _checkQuanty() method
  
  
  /**
   * Metoda dopasowuje wewnętrznych dostawców do książki
   * 
   * @param array $aItemData
   * @param array $aInternalProviders
   * @return mixed: 
   *  NULL - brak źródła, 
   *  FALSE - nie odznaczamy ŻADNEGO źródła - nasze wystarczają, 
   *  STRING - nalezy wybrać zaznaczone źródło
   */
  public function _matchInternalProviders($aItemData, $aInternalProviders) {
    $iOurQuantity = 0;
    $iMatchedSourceKey = NULL;
    $iMaxProductQuantity = 30;

    foreach ($aInternalProviders as $iSourceKey => $aProvider) {
      if ($this->_checkAvailableInternalProvider($aItemData, $aProvider['column_prefix_products_stock']) === TRUE) {
        $iMatchedSourceKey = $iSourceKey;
        break;
      }
      
      $iOurQuantity += intval($aItemData[$aProvider['column_prefix_products_stock'].'_status']);
    }

    // jeśli ilość większa od 1 sprawdźmy czy wszystkie 
    // nasze źródła wystarczą, żeby dostarczyć książki klientowi
    if (!isset($iMatchedSourceKey) && intval($aItemData['quantity']) > 1 && $iOurQuantity >= intval($aItemData['quantity'])) {
      return FALSE; // należy ręcznie odznaczyć źródło
    }
    
    // @ticket 14.10.2013 M.Chudy - jeśli ilość u nas >= 5 to obrabiane ręcznie
    if (!isset($iMatchedSourceKey) && $iOurQuantity >= 5) {
      return FALSE; // należy ręcznie odznaczyć źródło
    }

    if ($aItemData['quantity'] >= $iMaxProductQuantity) {
        // ustalone z KMalz 2018.09.27
        return FALSE; // należy ręcznie odznaczyć źródło, za dużo zamówiono
    }
    /*
    // sprawdzamy cenę zakupu, jeśli wybrano profit G
    if ($iMatchedSourceKey == 51) {
      // wyjątkowo można na sztywno, wpisać profit_g
      if ($aItemData['promo_price_brutto'] < $aItemData['profit_g_wholesale_price']) {
        // uciekamy od razu, cena zakupu jest zbyt wysoka
        return FALSE;
      }
    } 
     */           
    return $iMatchedSourceKey;
  }// end of _matchInternalProviders() method
  
  
  /**
   * Metoda sprawdza, czy dostawca może dostarczyć produkt
   * 
   * @param type $aItemData
   * @param type $sProviderSymbol
   * @return boolean
   */
  private function _checkAvailableInternalProvider($aItemData, $sProviderSymbol) {
    if (intval($aItemData[$sProviderSymbol.'_status']) >= intval($aItemData['quantity']) && 
        $aItemData['promo_price_brutto'] >= $aItemData[$sProviderSymbol.'_wholesale_price'] ) {
      return TRUE;
    }
    return FALSE;
  }// end of _checkProvider method
  
  
  /**
   * Metoda wyłuskuje ceny analogiczne dla źródeł, i zwraca tablicę źródeł podobnych 
   *  w kolejności od najniższej ceny, jeśli dana cena jest większa niż zero
   * 
   * @param array $aItemData
   * @return array
   */
  private function _extractWHPricesEqualSources($aItemData) {
    $aNewOrderedEqualSources = array();
    $aTMPItems = array();
    
    foreach ($this->aEqualExternalProviders as $sKey => $aProviderData) {
      $fWHPrice = floatval($aItemData[$aProviderData['symbol'].'_wholesale_price']);
      if ($fWHPrice > 0.00) {
        $aTMPItems[$sKey] = $fWHPrice;
      }
    }
    asort($aTMPItems, SORT_NUMERIC);
    
    
    foreach ($aTMPItems as $sIdKey => $sTMPVAL) {
      $aNewOrderedEqualSources[$sIdKey] = $this->aEqualExternalProviders[$sIdKey];
      $aNewOrderedEqualSources[$sIdKey]['id'] = $sIdKey;
    }
    
    return $aNewOrderedEqualSources;
  }// end of _extractWHPricesEqualSources() method
  
  
  /**
   * Metoda łączy podobne źródła
   * 
   * @param array $aExternalProviders
   * @return array
   */
  public function _mergeEqualExternalSources($aExternalProviders, $aItemData) {
    
    $bFirst = TRUE;
    $aNewArray = array();
    // wyłuskujemy z danych, produktu cenę zakupu u wspólnych dostawców
    $aEqualSortedArray = $this->_extractWHPricesEqualSources($aItemData);
    foreach ($aExternalProviders as $aExProvider) {
      if (in_array($aExProvider['id'], array_keys($this->aEqualExternalProviders))) {
        if ($bFirst === TRUE) {
          $bFirst = FALSE;// tylko za pierwszym razem zastępujemy źródło podobne
          // mamy ten element wspólny
          if (!empty($aNewArray)) {
            $aNewArray = array_merge($aNewArray, $aEqualSortedArray);
          } else {
            $aNewArray = $aEqualSortedArray;
          }
        }
      } else {
        if (!empty($aNewArray)) {
          $aNewArray = array_merge($aNewArray, array($aExProvider['id'] => $aExProvider));
        } else {
          $aNewArray = array($aExProvider['id'] => $aExProvider);
        }
      }
    }
    $aNewArray = $this->_sourcesUnique($aNewArray);
    return $aNewArray;
  }// end of _mergeEqualExternalSources() method
  
  
  /**
   * Metoda pobiera powiązanych ręcznie dostawców z bazy danych, nadaje im priorytet, nad posortowanymi domyślnie - także ręcznie i usuwa powtórzenia
   * 
   * @param int $iPublisherId
   * @param array $aExternalProviders
   * @return array
   */
  private function _getMappedProvidersByPublishers($iPublisherId, $aExternalProviders) {
    
    $sSql = "SELECT CONCAT('ID_', EP.id) as id, EP.stock_col, EP.symbol, EP.border_time
             FROM publishers_to_providers AS  PTP
             JOIN external_providers AS EP
              ON EP.id = PTP.provider_id
             WHERE PTP.publisher_id = ".$iPublisherId."
             ORDER BY PTP.order_by ";
    $aPRExternalProviders = $this->pDbMgr->GetAll('profit24', $sSql);
    
    $aExtProv = array_merge($aPRExternalProviders, $aExternalProviders);
    $aTMPExtProv = $this->_sourcesUnique($aExtProv);
    
    return $aTMPExtProv;
  }// end of _getMappedProvidersByPublishers() method
  
  
  /**
   * Metoda nadaje unikatowość tablicy źródeł po wartości pola ['id'] 
   *  w przekazanej tablicy źródeł
   * 
   * @param array $aSources
   * @return array
   */
  private function _sourcesUnique($aSources) {
    $aTMPSourcesIds = array();
    foreach ($aSources as $iKey => $aSource) {
      if (!isset($aTMPSourcesIds[$aSource['id']])) {
        $aTMPSourcesIds[$aSource['id']] = '1';
        // nie istnieje, ustawiamy
      } else {
        // istnieje, wywalamy
        unset($aSources[$iKey]);
      }
    }
    return $aSources;
  }// end of _sourcesUnique() method
  
  
  /**
   * Metoda pobiera wewnętrznych dostawców
   * 
   * @return string
   */
  public function _getInternalProviders() {
    
    $sSql = 'SELECT provider_id, id, symbol, 
                  column_prefix_products_stock, order_by, select_privileged
             FROM sources AS S
             WHERE id IN ("10", "1", "0")
             ORDER BY select_privileged DESC';
    return $this->pDbMgr->GetAssoc('profit24', $sSql);
  }// end of _getInternalProviders() method
  
  
  /**
   * Metoda pobiera dane o mapowaniach zewnętrznych dostawców
   * 
   * @return array
   */
  private function _getMappedExternalProviders($aExtProvidersSymbols = '') {
    
    $sSql = "SELECT CONCAT('ID_', id) as id, stock_col, symbol, border_time
             FROM external_providers 
             WHERE stock_col IS NOT NULL AND stock_col <> ''
             ".(!empty($aExtProvidersSymbols) ? ' AND symbol IN ("'.implode('", "', $aExtProvidersSymbols).'") ' : '' )."
             ORDER BY mapping_order_by";
    if (!empty($aExtProvidersSymbols)) {
      // musi tu byc id jako klucz
      return $this->pDbMgr->GetAssoc('profit24', $sSql);
    } else {
      return $this->pDbMgr->GetAll('profit24', $sSql);
    }
  }// end of _getMappedExternalProviders() method
  

  /**
   * Metoda pobiera składowe zamówień, które powinny 
   *  zostać automatycznie odznaczone do źródeł
   * 
   * Zamówienia:
   *  W statusie nowe - aktywowane,
   *  nie zmienione przez obslugę,
   *  nie mogą występować występować inne otwarte zamówienia, na ten adres email @modified 2014-11-07
   *  
   * Składowa:
   *  nie odznaczona do źródła,
   *  nie usunięta,
   *  jest pozycją "pojedyńczą" i nie jest to pakiet,
   *  lub jest to składowa pakietu
   * 
   * @return array
   */
  public function getOrdersItemsDataToUpdate() {
    global $aConfig;
    
    /*
     * sprawdzanie godziny granicznej odbędzie się w innym miejscu
     IF(
        PP.border_time IS NOT NULL AND CURTIME() < PP.border_time, 
        '1', 
        '0'
       ),  
       IF(
        PP.border_time IS NOT NULL AND ADDDATE('".$dFirstWorkingDay."', INTERVAL 1 DAY) < O.shipment_date, 
        '1', 
        '0'
       )
     */

    // pobierzmy pierwszy dzień roboczy, może to być dziś lub jutro itd.
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/Orders/ShipmentDateExpected.class.php');
    $oShipmentDateExpected = new ShipmentDateExpected($this->pDbMgr, $aConfig);
    $dFirstWorkingDay = $oShipmentDateExpected->getDateAfterWorkingDays(0, $oShipmentDateExpected->getFreeDays());
    /**
     * var have_additional_day
     *  1 - mam +1 jeden dzień roboczy i sprawdź godzinę graniczną
     *  2 - mam więcej niż jeden dzień roboczy lub (@mod 17.03.2014r. dzisiaj jest święto i mamy +1 lub więcej dni roboczych)
     *  0 - przeterminowana wysyłka i trzeba brać od nas
     */
    $sSql = $this->_getMainOrdersToUpdateSQL($dFirstWorkingDay, false);
    $bTestMode = $aConfig['common']['status'] == 'development' ? true : false;
    if ($bTestMode === false) {
      $sSql .= $this->_getCheckDuplicatesSQL();
    }
    dump($sSql);
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getOrdersItemsDataToUpdate() method
  
  
  /**
   * Metoda pobiera glowna czesc SQL, wybierajacego zamowienia do obrobienia
   * 
   * @param date $dFirstWorkingDay
   * @param bool $bUnlockAutoOrder - nie zwracaj uwagi, czy zamówienie zostało zablokowane, przed automatycznym obrobieniem
   * @return string
   */
  private function _getMainOrdersToUpdateSQL($dFirstWorkingDay, $bUnlockAutoOrder = false) {
    
    $sSql = "SELECT PS.*, O.id AS order_id, OI.quantity, OI.promo_price_brutto, O.order_number, O.shipment_date, OI.product_id, P.name, P.isbn_plain, P.publisher_id, PP.check_external_border_time, PP.omit_internal_provider, OI.id AS id,
    IF (
      ((ADDDATE('".$dFirstWorkingDay."', INTERVAL 1 DAY) = O.shipment_date) AND '".$dFirstWorkingDay."' = CURDATE()), 
      '1',
      IF (
        (ADDDATE('".$dFirstWorkingDay."', INTERVAL 1 DAY) < O.shipment_date OR (ADDDATE('".$dFirstWorkingDay."', INTERVAL 1 DAY) <= O.shipment_date AND ".$dFirstWorkingDay." <> CURDATE())), 
        '2',
        '0'
      )
    ) AS have_additional_day
             FROM orders_items AS OI
             JOIN orders AS O
              ON OI.order_id = O.id
             JOIN products AS P
              ON P.id = OI.product_id AND P.prod_status = '1'
             JOIN products_stock AS PS
              ON PS.id = P.id
             LEFT JOIN products_publishers AS PP
              ON PP.id = P.publisher_id
             WHERE 
              O.order_status = '0' AND
              O.internal_status = '1' AND 
              " .( $bUnlockAutoOrder == true ? '' : " O.locked_auto_order = '0' AND "  ) ."
              
              OI.status = '0' AND 
              OI.deleted <> '1' AND 
              (
                (OI.item_type = 'I' AND OI.packet = '0') 
                  OR 
                 OI.item_type = 'P'
               )

             ";
    return $sSql;
  }// end of _getMainOrdersToUpdateSQL() method
  
  
  /**
   * Metoda pobiera SQL, wybierajacy dane pojedynczego zamowienia do obrobienia
   *  Uzywane, przy recznym wywolaniu obrobienia zamowieniaF
   * 
   * @global array $aConfig
   * @param int $iOrderId
   * @return array
   */
  public function getSingleOrderData($iOrderId) {
    global $aConfig;
            
    // pobierzmy pierwszy dzień roboczy, może to być dziś lub jutro itd.
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/Orders/ShipmentDateExpected.class.php');
    $oShipmentDateExpected = new ShipmentDateExpected($this->pDbMgr, $aConfig);
    $dFirstWorkingDay = $oShipmentDateExpected->getDateAfterWorkingDays(0, $oShipmentDateExpected->getFreeDays());
    
    $sSql = $this->_getMainOrdersToUpdateSQL($dFirstWorkingDay, true);
    $sSql .= 'AND O.id = '.$iOrderId;
    
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of getSingleOrderSQL() method
  
  
  /**
   * Metoda generuje SQL, sprawdzajajacego, czy wystepuja duplikaty
   * 
   * @return string
   */
  private function _getCheckDuplicatesSQL() {
    
    $sSql = "               
              AND 
              (
                O.payment_status = '1'
                OR 
                (
                 SELECT O_TEST.id 
                 FROM orders AS O_TEST
                 WHERE 
                   O_TEST.id <> O.id 
                   AND (
                      O_TEST.client_ip = O.client_ip OR 
                      O_TEST.email = O.email
                   )
                   AND (
                     O_TEST.order_status = '0' OR 
                     O_TEST.order_status = '1')
                   LIMIT 1
                 ) IS NULL 
              )
              ";
    return $sSql;
  }// private function _getCheckDuplicatesSQL() method
  

  /**
   * Metoda zapisuje do bazy danych informacje na temat zmienionego statusu zamówień
   * 
   * @param int $iOldStatus
   * @param int $iNewStatus
   * @param array $aInfoData
   * @return bool
   */
  public function informAboutStatusChanged($aInfoData) {
    global $aConfig;
    
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
    if (isset($aInfoData) && !empty($aInfoData) && is_array($aInfoData)) {
      foreach ($aInfoData as $aOrderItem) {

        $aValues = array(
          'created' => 'NOW()',
          'created_by' => 'job_auto_status',
          'content' => serialize($aOrderItem)
        );
        if ($this->pDbMgr->Insert('profit24', 'events_logs_orders', $aValues) === false) {
          return false;
        }
      }
    }
    return true;
  }// end of informAboutStatusChanged() method
  
  
  /**
   * Metoda pobiera zewnętrzne źródła które są równowazne
   * 
   * @param array $aEqualExternalProvidersSymbols
   * @return array
   */
  private function _setEqualExternalProvidersData($aEqualExternalProvidersSymbols) {
    
    $this->aEqualExternalProviders = $this->_getMappedExternalProviders($aEqualExternalProvidersSymbols);
  }// end of _getEqualExternalProvidersData() method

  
  /**
   * Metoda ustawia zmienne potrzebne przy każdym obrabianym zamówieniu
   * 
   * @return void
   */
  private function _UpdateStartupSet() {

    $this->_setEqualExternalProvidersData($this->aExtEqualProviders);
  }

  /**
   * 
   * @param array $aOrderItemData
   */
  public function UpdateOrdersItems($aOrderItemData) {
    
  }
/// end of _UpdateStartupSet() method
}
