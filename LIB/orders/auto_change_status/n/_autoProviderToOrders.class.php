<?php
/**
 * Klasa zmienia automatycznie zamówione produkty do określonych źródeł
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-09-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders\auto_change_status;
class _autoProviderToOrders implements iAutoChangeStatus {
  private $pDbMgr;
  private $bTestMode;
  private $aInformData;
  private $oOrderRecount;
  
  function __construct($bTestMode) {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
    $this->bTestMode = $bTestMode;
    
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/OrderRecount.class.php');
    $this->oOrderRecount = new \OrderRecount();
  }

  public function UpdateOrders($aOrderData) {
    
  }

  /**
   * Metoda zmienia składowe zamówienia
   *  Ustalamy ogólne priotytety źródeł
   *  Ogólne założenie:
   *    najpierw jest źródło Nasze G, kolejno inne nasze źródła
   *    następnie jest mapowanie sztywne zdef. dla każdego wydawnictwa
   *    następnie są ustalone w kolejności źródła
   * 
   * @param array $aOrderItemData
   */
  public function UpdateOrdersItems($aOrderItemData) {
    
    $aInternalProviders = $this->_getInternalProviders();
    $aExternalProviders = $this->_getMappedExternalProviders();
    
    foreach ($aOrderItemData as $aItemData) {
      if ($aItemData['omit_internal_provider'] != '1' && $aItemData['manual_omit_internal_provider'] != '1') {
        // najpierw wewnętrzne później zewnętrzne
        if ($this->proceedInternal($aItemData, $aInternalProviders) === FALSE) {
          $this->proceedExternal($aItemData, $aExternalProviders);
        }
        
      } else {
        // najpierw zewnętrzne później wewnętrzne 
        if ($this->proceedExternal($aItemData, $aExternalProviders) === FALSE) {
          $this->proceedInternal($aItemData, $aInternalProviders);
        }
      }
    }
    return $this->aInformData;
  }// end of UpdateOrdersItems() method
  
  
  /**
   * Metoda przetrwarza zewnętrzne źródła
   * 
   * @param type $aItemData
   * @param type $aExternalProviders
   * @return boolean
   */
  public function proceedExternal($aItemData, $aExternalProviders) {
    
    // brak źródła - lecimy ze źródłami zewnętrznymi
    $aProductExternalProviders = $this->_getMappedProvidersByPublishers($aItemData['publisher_id'], $aExternalProviders);
    $iMatchedExternalProviderId = $this->_matchExternalProviders($aItemData, $aProductExternalProviders);
    if ($iMatchedExternalProviderId > 0) {
      $this->setOrderItemSource($aItemData, FALSE, $iMatchedExternalProviderId, $aProductExternalProviders['ID_'.$iMatchedExternalProviderId]);
      return TRUE;
    } else {
      // brak odpowiedniego źródła
      // pozycja do odznaczenia ręcznie
      $this->setLocketAutoOrder($aItemData);
      return FALSE;
    }
  }// end of proceedExternal() method
  
  
  /**
   * Metoda przetwarza wewnętrzne zamówienie
   * 
   * @param array $aItemData
   * @param array $aInternalProviders
   * @return boolean|null
   */
  public function proceedInternal($aItemData, $aInternalProviders) {
    
    $iMatchedInternalProvider = $this->_matchInternalProviders($aItemData, $aInternalProviders);
    if ($iMatchedInternalProvider !== '' && is_numeric($iMatchedInternalProvider)) {

      // ustawiamy źródło
      $this->setOrderItemSource($aItemData, TRUE, $iMatchedInternalProvider, $aInternalProviders[$iMatchedInternalProvider]);
      return TRUE;
    } elseif ($iMatchedInternalProvider === NULL) {
      // możesz sprawdzać następne elementy
      return FALSE;
      
    } elseif ($iMatchedInternalProvider === FALSE) {

      // pozycja do odznaczenia ręcznie - dostepna w sumie w źródłach wewnętrznych
      $this->setLocketAutoOrder($aItemData);
      return TRUE;
    } else {
      var_dump($iMatchedInternalProvider);
      echo 'INTERNAL ERROR 1';
      exit(1);
    }
  }// end of proceedInternal() method
  
  
  /**
   * Metoda ustawia źródło dla zamówionego produktu
   * 
   * @param array $aItemData
   * @param bool $bInternal
   * @param int $iMatchedProviderId
   * @param string $sMatchedProviderName
   * @return boolean
   */
  public function setOrderItemSource($aItemData, $bInternal, $iMatchedProviderId, $sMatchedProviderName) {
    if (empty($sMatchedProviderName)) {
      $sMatchedProviderName = $this->pDbMgr->GetOne('profit24', 'SELECT name FROM external_providers WHERE id = '.$iMatchedProviderId);
    }
    $sMatchedProviderName = str_replace(array('_status', '_stock'), '', $sMatchedProviderName);
    
    if ($this->bTestMode === TRUE) {
      $sStr = ' ŹRÓDŁO: '.$sMatchedProviderName;
      if (empty($sMatchedProviderName)) {
        $sStr = ' SZTYWNE ŹRÓDŁO: '.$sMatchedProviderName;
      }
      echo 'ZAMÓWIENIE: '.$aItemData['order_number'].' POZYCJA: '.$aItemData['isbn_plain'].' USTAWIAMY '.$sStr."<hr />";
    } else {
      // ustawiamy status
      
      $aItemTMP = $aItemData;
      $aItemTMP['id'] = $aItemTMP['order_id'];
      
      if ($bInternal == TRUE) {
        // jest niepotwierdzone
        $iNewOrderItemStatus = '3';
      } else {
        // do zamowienia
        $iNewOrderItemStatus = '1';
      }
      
      $aOrderItemValues = array(
          'status' => $iNewOrderItemStatus,
          'source' => $iMatchedProviderId
      );
      dump($aOrderItemValues);
      dump(' id = '.$aItemData['id'].' AND order_id = '.$aItemData['order_id'].' LIMIT 1');
//      ini_set('display_erros', 'On');
//      error_reporting(E_ALL);
//      var_dump($this->pDbMgr->Update('profit24', 'orders_items', $aOrderItemValues, ' id = '.$aItemData['id'].' AND order_id = '.$aItemData['order_id'].' LIMIT 1'));
      if ($this->pDbMgr->Update('profit24', 'orders_items', $aOrderItemValues, ' id = '.$aItemData['id'].' AND order_id = '.$aItemData['order_id'].' LIMIT 1') === FALSE) {
        return FALSE;
      }
              
      // zmiana statusu
      $this->aInformData[] = array(
          'new_status' => $iNewOrderItemStatus,
          'old_status' => '0',
          'source' => $iMatchedProviderId,
          'source_name' => $sMatchedProviderName,
          'order' => $aItemTMP
      );
      
      if ($this->oOrderRecount->recountOrder($aItemData['order_id']) === FALSE) {
        return FALSE;
      }
      
    }
  }
  
  
  /**
   * Blokujemy zamówienie przed podnownym obrobieniem
   * 
   * @param array $aItemData
   * @return bool
   */
  public function setLocketAutoOrder($aItemData) {
    
    if ($this->bTestMode === TRUE) {
      echo ('BLOKUJEMY ZAMÓWIENIE PRZED PPO: '.$aItemData['order_number'].' POZYCJA: '.$aItemData['isbn_plain'].'')."<hr />";
    } else {
      $aValues = array(
          'locked_auto_order' => '1'
      );
      return $this->pDbMgr->Update('profit24', 'orders', $aValues, ' id='.$aItemData['order_id']);
    }
  }// end of setLocketAutoOrder() method
  
  
  /**
   * Metoda dopasowuje zewnętrznego dostawcę do zamówionego produktu
   * 
   * @param array $aItemData
   * @param array $aExternalProviders
   * @return boolean
   */
  public function _matchExternalProviders($aItemData, $aExternalProviders) {
    foreach ($aExternalProviders as $sProviderId => $sStockColName) {
      if (!empty($sStockColName) && $sStockColName != '') {
        //helion_stock, abe_status
        $sSourceSource = preg_replace('/(.*)_[^_]*$/', '$1', $sStockColName);

        if (isset($aItemData[$sSourceSource.'_status']) 
             &&
            (intval($aItemData[$sSourceSource.'_status']) == '3' || intval($aItemData[$sSourceSource.'_status']) == '1')
            ) {

          if ($this->_checkQuanty($aItemData[$sStockColName], intval($aItemData['quantity'])) === TRUE) {
            return intval(str_replace('ID_', '', $sProviderId));
          }
        }
      } else {
        return intval(str_replace('ID_', '', $sProviderId));
      }
    }
    return FALSE;
  }// end of _matchExternalProviders() method
  
  
  /**
   * Metoda sprawdza czy ilość jest wystarczająca w źródle
   * 
   * @param string $sQuantityValue 20-70 | >30 | <10 | 0 | 1-9 | >50 | 20-30 | 0-3
   * @param int $iOrderedQuantity
   * @return bool
   */
  public function _checkQuanty($sQuantityValue, $iOrderedQuantity) {
    // matchowanie ilości
    $aRangeMatches = array();
    $aGreaterThenMatches = array();
    $aLessThenMatches = array();
    $aNumber = array();
    $iLimitOverflow = 5; // zmiana uzgodniona Ł. Głuchowski, K. Małż, M. Chudy - 12.45 2013.10.11
    
    // DETECT
    
    // zakres
    preg_match('/^(\d+)-(\d+)$/', $sQuantityValue, $aRangeMatches);
    if (!empty($aRangeMatches)) {
      
      $iMax = $aRangeMatches[2];
      if ($iOrderedQuantity <= $iMax) {
        return TRUE;
      } else {
        // niewystarczająca ilość
        return FALSE;
      }
    }
    
    // wiecej niż
    preg_match('/^>(\d+)$/', $sQuantityValue, $aGreaterThenMatches);
    if (!empty($aGreaterThenMatches)) {
      
      // jeśli zamówiono wiecej o 1 niż wartość maksymalna, czyli np. zam 71, a jest >70, to już nie obrabiamy
      // ustalono z Marcinem i Łukaszem
      $bGTIsEnough = (($iOrderedQuantity < ($aGreaterThenMatches[1] + $iLimitOverflow)) ? TRUE : FALSE);
      if ($bGTIsEnough === TRUE) {
        return TRUE;
      } else {
        return FALSE;
      }
    }
    
    // mniej niż
    preg_match('/^<(\d+)$/', $sQuantityValue, $aLessThenMatches);
    if (!empty($aLessThenMatches)) {
      
      $bLTIsEnough = (($iOrderedQuantity < $aLessThenMatches[1]) ? TRUE : FALSE);
      if ($bLTIsEnough === TRUE) {
        return TRUE;
      } else {
        return FALSE;
      }
    }
    
    preg_match('/^(\d+)$/', $sQuantityValue, $aNumber);
    if (!empty($aNumber)) {
      
      if ($iOrderedQuantity <= $aNumber['1']) {
        return TRUE;
      } else {
        return FALSE;
      }
    }
    return FALSE;
  }// end of _checkQuanty() method
  
  
  /**
   * Metoda dopasowuje wewnętrznych dostawców do książki
   * 
   * @param array $aItemData
   * @param array $aInternalProviders
   * @return mixed: 
   *  NULL - brak źródła, 
   *  FALSE - nie odznaczamy ŻADNEGO źródła - nasze wystarczają, 
   *  STRING - nalezy wybrać zaznaczone źródło
   */
  public function _matchInternalProviders($aItemData, $aInternalProviders) {
    $iOurQuantity = 0;
    $iMatchedSourceKey = NULL;
    foreach ($aInternalProviders as $iSourceKey => $sProviderSymbol) {
      if ($this->_checkAvailableInternalProvider($aItemData, $sProviderSymbol) === TRUE) {
        $iMatchedSourceKey = $iSourceKey;
        break;
      }
      
      $iOurQuantity += intval($aItemData[$sProviderSymbol.'_status']);
    }

    // jeśli ilość większa od 1 sprawdźmy czy wszystkie 
    // nasze źródła wystarczą, żeby dostarczyć książki klientowi
    if (!isset($iMatchedSourceKey) && intval($aItemData['quantity']) > 1 && $iOurQuantity >= intval($aItemData['quantity'])) {
      return FALSE; // należy ręcznie odznaczyć źródło
    }
    
    // @ticket 14.10.2013 M.Chudy - jeśli ilość u nas >= 5 to obrabiane ręcznie
    if (!isset($iMatchedSourceKey) && $iOurQuantity >= 5) {
      return FALSE; // należy ręcznie odznaczyć źródło
    }
            
    return $iMatchedSourceKey;
  }// end of _matchInternalProviders() method
  
  
  /**
   * Metoda sprawdza, czy dostawca może dostarczyć produkt
   * 
   * @param type $aItemData
   * @param type $sProviderSymbol
   * @return boolean
   */
  private function _checkAvailableInternalProvider($aItemData, $sProviderSymbol) {
    if (intval($aItemData[$sProviderSymbol.'_status']) >= intval($aItemData['quantity'])) {
      return TRUE;
    }
    return FALSE;
  }// end of _checkProvider method
  
  
  /**
   * Metoda pobiera powiązanych ręcznie dostawców z bazy danych, nadaje im priorytet, nad posortowanymi domyślnie - także ręcznie i usuwa powtórzenia
   * 
   * @param int $iPublisherId
   * @param array $aExternalProviders
   * @return array
   */
  private function _getMappedProvidersByPublishers($iPublisherId, $aExternalProviders) {
    
    $sSql = "SELECT CONCAT('ID_', EP.id), EP.stock_col
             FROM publishers_to_providers AS  PTP
             JOIN external_providers AS EP
              ON EP.id = PTP.provider_id
             WHERE PTP.publisher_id = ".$iPublisherId."
             ORDER BY PTP.order_by ";
    $aPRExternalProviders = $this->pDbMgr->GetAssoc('profit24', $sSql);
    return array_unique(array_merge($aPRExternalProviders, $aExternalProviders));
  }// end of _getMappedProvidersByPublishers() method
  
  
  /**
   * Metoda pobiera wewnętrznych dostawców
   * 
   * @return string
   */
  public function _getInternalProviders() {
    
    $aProviders = array(
        '51' => 'profit_g',
        '1' => 'profit_e',
        '0' => 'profit_j',
        '5' => 'profit_m',
        '52' => 'profit_x',
    );
    return $aProviders;
  }// end of _getInternalProviders() method
  
  
  /**
   * Metoda pobiera dane o mapowaniach zewnętrznych dostawców
   * 
   * @return array
   */
  private function _getMappedExternalProviders() {
    
    $sSql = "SELECT CONCAT('ID_', id), stock_col
             FROM external_providers 
             WHERE stock_col IS NOT NULL AND stock_col <> ''
             ORDER BY mapping_order_by";
    return $this->pDbMgr->GetAssoc('profit24', $sSql);
  }// end of _getMappedExternalProviders() method
  

  /**
   * Metoda pobiera składowe zamówień, które powinny 
   *  zostać automatycznie odznaczone do źródeł
   * 
   * Zamówienia:
   *  W statusie nowe - aktywowane,
   *  nie zmienione przez obslugę,
   *  
   * Składowa:
   *  nie odznaczona do źródła,
   *  nie usunięta,
   *  jest pozycją "pojedyńczą" i nie jest to pakiet,
   *  lub jest to składowa pakietu
   *  
   * @return array
   */
  public function getOrdersItemsDataToUpdate() {
    global $aConfig;
    
    // pobierzmy pierwszy dzień roboczy, może to być dziś lub jutro itd.
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/Orders/ShipmentDateExpected.class.php');
    $oShipmentDateExpected = new \ShipmentDateExpected($this->pDbMgr, $aConfig);
    $dFirstWorkingDay = $oShipmentDateExpected->getDateAfterWorkingDays(0, $oShipmentDateExpected->getFreeDays());
    
    $sSql = "SELECT PS.*, O.id AS order_id, OI.quantity, O.order_number, O.shipment_date, P.name, P.isbn_plain, P.publisher_id, PP.border_time, PP.omit_internal_provider, OI.id AS id,
    IF (
      (ADDDATE('".$dFirstWorkingDay."', INTERVAL 1 DAY) = O.shipment_date), 
      IF(
        PP.border_time IS NOT NULL AND CURTIME() < PP.border_time, 
        '1', 
        '0'
       ),  
       IF(
        PP.border_time IS NOT NULL AND ADDDATE('".$dFirstWorkingDay."', INTERVAL 1 DAY) < O.shipment_date, 
        '1', 
        '0'
       )
    ) AS manual_omit_internal_provider
             FROM orders_items AS OI
             JOIN orders AS O
              ON OI.order_id = O.id
             JOIN products AS P
              ON P.id = OI.product_id AND P.prod_status = '1'
             JOIN products_stock AS PS
              ON PS.id = P.id
             LEFT JOIN products_publishers AS PP
              ON PP.id = P.publisher_id
             WHERE 
              O.order_status = '0' AND
              O.internal_status = '1' AND 
              O.locked_auto_order = '0' AND
              
              OI.status = '0' AND 
              OI.deleted <> '1' AND 
              (
                (OI.item_type = 'I' AND OI.packet = '0') 
                  OR 
                 OI.item_type = 'P'
               )
             ";
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

  /**
   * Metoda zapisuje do bazy danych informacje na temat zmienionego statusu zamówień
   * 
   * @param int $iOldStatus
   * @param int $iNewStatus
   * @param array $aInfoData
   * @return bool
   */
  public function informAboutStatusChanged($aInfoData) {
    global $aConfig;
    
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
    if (isset($aInfoData) && !empty($aInfoData) && is_array($aInfoData)) {
      foreach ($aInfoData as $aOrderItem) {

        $aValues = array(
          'created' => 'NOW()',
          'created_by' => 'job_auto_status',
          'content' => serialize($aOrderItem)
        );
        if ($this->pDbMgr->Insert('profit24', 'events_logs_orders', $aValues) === false) {
          return false;
        }
      }
    }
    return true;
  }// end of informAboutStatusChanged() method

}