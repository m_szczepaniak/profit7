<?php
/**
 * Metodaje dodaje do tabeli zakładki przeterminowane zamówienia, 
 *  które nie zostały wysłane do określonej godziny
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-07 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders\auto_change_status;
class autoDeadline implements iAutoChangeStatus  {

  private $pDbMgr;
  CONST ORDERS_REASONS_TYPE = 'P'; // przeterminowane
  
  function __construct() {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
  }

  public function UpdateOrders($aOrderData) {
    
  }

  public function UpdateOrdersItems($aOrderItemData) {
    
    $oOrdersContainers = new \orders_containers\OrdersContainers();
    foreach ($aOrderItemData as $aOrderItem) {
      $oOrdersContainers->addOrderToContainers($aOrderItem['id'], static::ORDERS_REASONS_TYPE);
    }
    return $aOrderItem;
  }

  
  /**
   * Metoda pobiera składowe zamówienia do aktualizacji
   * 
   * @return array
   */
  public function getOrdersItemsDataToUpdate() {
    
    // która metoda transportu ?? 
    $sSql = 'SELECT id 
             FROM orders_transport_means 
             WHERE 
              HOUR(NOW()) = border_time_deadline
             ';
    $aTransportIds = $this->pDbMgr->GetCol('profit24', $sSql);
    
    if (!empty($aTransportIds)) {
      // czy występują duplikaty
      $sSql = 'SELECT DISTINCT O.id, O.* 
               FROM orders AS O
               WHERE 
               O.transport_id IN ('.implode($aTransportIds, ',').') AND 
               O.shipment_date_expected <= CURDATE() AND
               O.order_status NOT IN ("3", "4", "5")
        ';
      $sSql .= $this->_getPaidFilter();
      $sSql .= $this->_removeDuplicateSQL();
      $sSql .= $this->_removePreviewsSQL();
      return $this->pDbMgr->GetAll('profit24', $sSql);
    }
    return array();
  }// end of getOrdersItemsDataToUpdate() method
  
  
  /**
   * Metoda dodaje filtr opłacenia zamóienia
   * 
   * @return string
   */
  private function _getPaidFilter() {
    
    $sSql = "
      AND
      (
        (
          (
            O.payment_type = 'bank_transfer' OR
            O.payment_type = 'platnoscipl' OR
            O.payment_type = 'card'
          )
          AND O.to_pay <= O.paid_amount
        )
        OR
        (
          O.payment_type = 'postal_fee' OR
          O.payment_type = 'bank_14days'
        )
        OR
        (
          O.second_payment_type = 'postal_fee' OR
          O.second_payment_type = 'bank_14days'
        )
      )      
      ";
    return $sSql;
  }// end of _getPaidFilter() method
  
  
  /**
   * Metoda usuwa duplikaty
   * 
   * @return string
   */
  private function _removeDuplicateSQL() {
    
    $sSql = '               
      AND (
        SELECT ORC.id 
        FROM orders_reasons_containers AS ORC
        WHERE 
         ORC.order_id = O.id AND
         ORC.orders_reasons_type = "'.static::ORDERS_REASONS_TYPE.'" AND
         ORC.checked = "0"
        LIMIT 1
       ) IS NULL';
    return $sSql;
  }// end of _removeDuplicateSQL() method
  
  
  /**
   * Metoda nie dodaje do przeterminowanych, zamówień zawierających zapowiedzi
   * 
   */
  private function _removePreviewsSQL() {
    
    $sSql = '
      AND (
        SELECT OI.id
        FROM orders_items AS OI
        WHERE 
          OI.id = O.id AND
          OI.status = "-1" AND
          OI.deleted = "0"
        LIMIT 1
      ) IS NULL
      ';
    return $sSql;
  }// end of _removePreviewsSQL() method
  

  /**
   * Metoda zapisuje do bazy danych informacje na temat zmienionego statusu zamówień
   * 
   * @param int $iOldStatus
   * @param int $iNewStatus
   * @param array $aInfoData
   * @return bool
   */
  public function informAboutStatusChanged($aInfoData) {
    
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
    if (isset($aInfoData) && !empty($aInfoData) && is_array($aInfoData)) {
      foreach ($aInfoData as $aOrderItem) {

        $aValues = array(
          'created' => 'NOW()',
          'created_by' => 'job_auto_status',
          'content' => serialize($aOrderItem)
        );
        if ($this->pDbMgr->Insert('profit24', 'events_logs_orders', $aValues) === false) {
          return false;
        }
      }
    }
    return true;
  }// end of informAboutStatusChanged() method
}
