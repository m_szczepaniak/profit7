<?php
/**
 * Metodaje dodaje do tabeli zakładki duplikaty, zamówienia od tego samego adresu email, lub z tego samego adresu email
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-07
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace orders\auto_change_status;
class autoDuplicates implements iAutoChangeStatus
{

    private $pDbMgr;
    CONST ORDERS_REASONS_TYPE = 'D';

    function __construct()
    {
        global $pDbMgr;
        $this->pDbMgr = $pDbMgr;
    }

    /**
     * @param $aOrderData
     */
    public function UpdateOrders($aOrderData)
    {

    }

    /**
     * @param $aOrderItemData
     * @return mixed
     */
    public function UpdateOrdersItems($aOrderItemData)
    {

        $oOrdersContainers = new \orders_containers\OrdersContainers();
        foreach ($aOrderItemData as $aOrderItem) {
            $oOrdersContainers->addOrderToContainers($aOrderItem['id'], static::ORDERS_REASONS_TYPE);
        }
        return $aOrderItem;
    }


    /**
     * @return array
     */
    public function getOrdersItemsDataToUpdate()
    {
        $duplicateOrders = [];
        $sSql = '
             SELECT DISTINCT O.id, O.* 
             FROM orders AS O
             WHERE 
             ( 
               O.order_status = "0" OR 
               O.order_status = "1"
             )
             AND DATE(O.order_date) = CURDATE()
             ';
        $orders = $this->pDbMgr->GetAll('profit24', $sSql);

        $ordersReasonsTypesOrdersIds = $this->getOrdersReasonsType();

        foreach ($orders as $orderCompare) {

            // nie może znajdować się już w zakładce
            if (!in_array($orderCompare['id'], $ordersReasonsTypesOrdersIds)) {
                if (true === $this->searchDuplicate($orders, $orderCompare)) {
                    $duplicateOrders[] = $orderCompare;
                }
            }
        }
        return $duplicateOrders;
    }

    /**
     * @return array
     *
     */
    private function getOrdersReasonsType()
    {
        $sSql = 'SELECT ORC.order_id
                 FROM orders_reasons_containers AS ORC
                 WHERE 
                  ORC.orders_reasons_type = "' . static::ORDERS_REASONS_TYPE . '" AND
                  ORC.created > DATE_SUB(CURDATE(), INTERVAL 3 DAY)
                  ';
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

    /**
     * @param $orders
     * @param $orderCompare
     * @return bool
     */
    private function searchDuplicate($orders, $orderCompare)
    {

        foreach ($orders as $orderSource) {
            if ($orderCompare['id'] === $orderSource['id']) {
                continue;
            }

            if ($orderCompare['client_ip'] === $orderSource['client_ip']) {
                return true;
            }
            if ($orderCompare['email'] === $orderSource['email']) {
                return true;
            }
        }
        return false;
    }


    /**
     * @return array
     */
    public function getOrdersItemsDataToUpdateOld()
    {
        // @TODO tutaj należy usunąć takie, które w składowych mają: preview = "1" AND B.status = "-1"
        // czy występują duplikaty
        $sSql = 'SELECT DISTINCT O.id, O.* 
             FROM orders AS O
             WHERE 
             ( 
               O.order_status = "0" OR 
               O.order_status = "1"
             )
              AND 
              (
               SELECT O_TEST.id 
               FROM orders AS O_TEST
               WHERE 
                 O_TEST.id <> O.id 
                 AND (
                    O_TEST.client_ip = O.client_ip OR 
                    O_TEST.email = O.email
                 )
                 AND (
                   O_TEST.order_status = "0" OR 
                   O_TEST.order_status = "1")
                 AND DATE(O_TEST.order_date) = CURDATE()
                 LIMIT 1
               ) IS NOT NULL
                AND (
                 SELECT ORC.id 
                 FROM orders_reasons_containers AS ORC
                 WHERE 
                  ORC.order_id = O.id AND
                  ORC.orders_reasons_type = "' . static::ORDERS_REASONS_TYPE . '"
                 LIMIT 1
                ) IS NULL
                AND DATE(O.order_date) = CURDATE()
      ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * Metoda zapisuje do bazy danych informacje na temat zmienionego statusu zamówień
     *
     * @param array $aInfoData
     * @return bool
     */
    public function informAboutStatusChanged($aInfoData)
    {
        global $aConfig;

        include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
        if (isset($aInfoData) && !empty($aInfoData) && is_array($aInfoData)) {
            foreach ($aInfoData as $aOrderItem) {

                $aValues = array(
                    'created' => 'NOW()',
                    'created_by' => 'job_auto_status',
                    'content' => serialize($aOrderItem)
                );
                if ($this->pDbMgr->Insert('profit24', 'events_logs_orders', $aValues) === false) {
                    return false;
                }
            }
        }
        return true;
    }// end of informAboutStatusChanged() method

}
