<?php
/**
 * Metodaje dodaje do tabeli zakładki duplikaty, zamówienia od tego samego adresu email, lub z tego samego adresu email
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-07 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders\auto_change_status;
class autoFraud implements iAutoChangeStatus  {

  private $pDbMgr;
  CONST ORDERS_REASONS_TYPE = 'F';
  
  function __construct() {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
  }

  public function UpdateOrders($aOrderData) {
    
  }

  public function UpdateOrdersItems($aOrderItemData) {
    
    $oOrdersContainers = new \orders_containers\OrdersContainers();
    foreach ($aOrderItemData as $aOrderItem) {
      $oOrdersContainers->addOrderToContainers($aOrderItem['id'], static::ORDERS_REASONS_TYPE);
      $this->setLocketAutoOrder($aOrderItem['id']);
    }
    return $aOrderItem;
  }

  
  public function getOrdersItemsDataToUpdate() {
    // czy występują oszuści
    $sSql = 'SELECT DISTINCT O.id, O.* 
             FROM orders AS O
             WHERE 
              O.order_status = "0"
              AND O.internal_status = "1"
              AND O.locked_auto_order = "0"
              AND O.payment_status = "0"
              AND 
              (
               SELECT O_TEST.id 
               FROM orders AS O_TEST
               WHERE 
                 O_TEST.id <> O.id 
                 AND (
                    O_TEST.email = O.email
                 )
                 AND (
                   O_TEST.correction = "1")
                 LIMIT 1
               ) IS NOT NULL
                AND (
                 SELECT ORC.id 
                 FROM orders_reasons_containers AS ORC
                 WHERE 
                  ORC.order_id = O.id AND
                  ORC.orders_reasons_type = "'.static::ORDERS_REASONS_TYPE.'"
                 LIMIT 1
                ) IS NULL
                AND DATE(O.order_date) = CURDATE()
      ';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  

  /**
   * Metoda zapisuje do bazy danych informacje na temat zmienionego statusu zamówień
   * 
   * @param int $iOldStatus
   * @param int $iNewStatus
   * @param array $aInfoData
   * @return bool
   */
  public function informAboutStatusChanged($aInfoData) {
    global $aConfig;
    
    include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
    if (isset($aInfoData) && !empty($aInfoData) && is_array($aInfoData)) {
      foreach ($aInfoData as $aOrderItem) {

        $aValues = array(
          'created' => 'NOW()',
          'created_by' => 'job_auto_status',
          'content' => serialize($aOrderItem)
        );
        if ($this->pDbMgr->Insert('profit24', 'events_logs_orders', $aValues) === false) {
          return false;
        }
      }
    }
    return true;
  }// end of informAboutStatusChanged() method
  
  
  /**
   * Blokujemy zamówienie przed podnownym obrobieniem
   * 
   * @param array $iOId
   * @return bool
   */
  public function setLocketAutoOrder($iOId) {
    
    if ($this->bTestMode === TRUE) {
      echo ('BLOKUJEMY ZAMÓWIENIE PRZED PPO: '.$iOId)."<hr />";
    } else {
      $aValues = array(
          'locked_auto_order' => '1'
      );
      return $this->pDbMgr->Update('profit24', 'orders', $aValues, ' id='.$iOId);
    }
  }// end of setLocketAutoOrder() method
}
