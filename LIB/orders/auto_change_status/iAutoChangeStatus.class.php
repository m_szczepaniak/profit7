<?php
/**
 * Interfejs automatycznego obrabiania zamówień
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-09-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders\auto_change_status;
interface iAutoChangeStatus {
    public function informAboutStatusChanged($aInfoData);
    public function getOrdersItemsDataToUpdate();
    
    public function UpdateOrders($aOrderData);
    public function UpdateOrdersItems($aOrderItemData);
}
?>