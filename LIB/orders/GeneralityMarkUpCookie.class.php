<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-05-24
 * Time: 10:47
 */
namespace orders;


use orders\MarkUpCookie\GeneralityAllUsers;
use orders\MarkUpCookie\GeneralityBuyBox;

class GeneralityMarkUpCookie
{

    private $classesOfMarkUp;

    private $usedMarkUpObj;

    /**
     *
     */
    public function __construct() {
        $this->classesOfMarkUp[] = new GeneralityBuyBox();
        $this->classesOfMarkUp[] = new GeneralityAllUsers();
    }

    public function isMarkUpActive() {
        if (!empty($this->classesOfMarkUp)) {
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    private function setMarkUpActiveUsage()
    {
        foreach ($this->classesOfMarkUp as $key => $obj) {
            if ($obj->isMarkUpActive() === true) {
                return true;
            } else {
                unset($this->classesOfMarkUp[$key]);
            }
        }

        return false;
    }

    /**
     * @return float
     */
    public function getGeneralMarkup()
    {
        $this->setMarkUpActiveUsage();
        foreach ($this->classesOfMarkUp as $obj) {

            $generalDiscount = $obj->getGeneralMarkup();
            $this->usedMarkUpObj = $obj;
            if ($obj instanceof GeneralityBuyBox && $obj->isIsUsed() === true) {
                return $generalDiscount;
            }

        }
        return $generalDiscount;
    }

    /**
     * @return array|null
     */
    public function getProductsIdsOmmitMarkup() {
        if (!isset($this->usedMarkUpObj)) {
            $this->getGeneralMarkup();
        }

        if ($this->usedMarkUpObj instanceof GeneralityAllUsers && $this->usedMarkUpObj->isMarkUpActive()) {
            return $this->usedMarkUpObj->getProductsIdsOmmitMarkup();
        }
    }
}