<?php
/**
 * Klasa wybierająca dane zamówienia
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-03-21 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders;
class OrderMagazineData {
//  protected $iOId;
    public $pDbMgr;

  function __construct($pDbMgr) {
//    $this->iOId = $iOId;
      $this->pDbMgr = $pDbMgr;
  }

  
  /**
   * Metoda pobiera zamówienia do wysłania do źródła
   * 
   * @global object $pDbMgr
   * @param int $iSource
   * @param int $iStatus
   * @param array $aSelectCols
   * @param string $sSource źródło 'azymut', 'siodemka', z tabeli products_stock
   * @param float $fPriceLimit
   * @param int $iCountLimit
   * @return array tablica produktów
   */
  public function getOrdersItemsSendToSource($iSource, $iStatus, $sSource, $aSelectCols, $fPriceLimit = 0.00, $iCountLimit = 0, $iLocationSiodemka = null, $vat = null, $vatNot = null) {

      $whereAdd = '';
    if ($iLocationSiodemka !== null) {
        $whereAdd .= ' AND IF(PS.id IS NOT NULL, (PS.siodemka_location = "'.$iLocationSiodemka.'" OR PS.siodemka_location IS NULL OR PS.siodemka_location = ""), 1=1 ) ';
    }
    if ($vat !== null) {
        $whereAdd .= ' AND OI.vat = "'.$vat.'" ';
    }

    if ($vatNot !== null) {
      $whereAdd .= ' AND OI.vat <> "'.$vatNot.'" ';
    }
            
    $sSql = "SELECT ". implode(',', $aSelectCols) ."
             FROM orders_items AS OI
             JOIN orders AS O
              ON OI.order_id = O.id
             LEFT JOIN products P
               ON P.id = OI.product_id
             LEFT JOIN products_stock PS
               ON PS.id = OI.product_id
             WHERE 1=1 
              " . $whereAdd . "
              ".($iSource != '' ? " AND OI.source = '".$iSource."'" : '')."
              ".($iStatus === '-1' ? " AND (O.internal_status = '2' OR O.internal_status = '3') " : '')."
              AND O.order_status <> '5'
              AND OI.deleted = '0'
              AND OI.packet = '0'
              AND OI.status = '".$iStatus."'".
                ($iStatus == '3'?" AND OI.sent_hasnc = '0'":'').
                ($iStatus === '-1' ? ' ORDER BY OI.shipment_date ASC ' : '').'
              GROUP BY CONCAT(OI.product_id, \'-\', O.id)
              ORDER BY status_1_update ASC ';
    /*
    $sSql = "SELECT ".  implode(',', $aSelectCols) ."
              FROM orders_items OI
              LEFT JOIN products P
               ON P.id = OI.product_id
              LEFT JOIN products_stock PS
               ON PS.id = OI.product_id
              JOIN orders O
               ON O.id = OI.order_id
              WHERE
                   OI.source = '1'
               AND O.order_status <> '5'
               AND OI.deleted = '0'
               AND OI.packet = '0'
               AND OI.status = '3'
               AND OI.sent_hasnc = '0'
              GROUP BY CONCAT(OI.product_id, '-', O.id) 
              ORDER BY O.shipment_date ASC ";
     */
    $aOrdersItems = $this->pDbMgr->GetAll('profit24', $sSql);
    $aAllBooksToConfirm = $this->parseBooksArray($aOrdersItems, $iStatus);

    if ($fPriceLimit > 0.00) {
      $aBooksToConfirm = $this->getLimitedBooksByPrice($aAllBooksToConfirm, $fPriceLimit, $sSource);
    } else {
      $aBooksToConfirm = $this->getLimitedBooks($aAllBooksToConfirm, $iCountLimit);
    }
    return $aBooksToConfirm;
  }// end of getOrdersItemsSendToSource() method
  
  
  /**
   * Metoda parsuje tablicę produktów
   * 
   * @param array $aOrdersItems
   * @param int $iStatus
   * @return array
   */
  protected function parseBooksArray($aOrdersItems, $iStatus, $source = false) {
    $aBooksToConfirm = array();
    
    foreach($aOrdersItems as $sKeyCon =>$aItem){
				if ($iStatus !== '-1') {
					$aBooksToConfirm[$sKeyCon] = $this->_getBookData($aItem['product_id']);
				} else {
					$aBooksToConfirm[$sKeyCon] = $this->_getPreviewBookData($aItem['product_id']);
					$aBooksToConfirm[$sKeyCon]['publication_year'] = $aItem['shipment_date'];
				}
        $aBooksToConfirm[$sKeyCon]['items'] = explode(',', $aItem['items']);
				$aBooksToConfirm[$sKeyCon]['quantity'] = $aItem['quantity'];
        if (isset($aItem['azymut_wholesale_price']) && $aItem['azymut_wholesale_price'] > 0) {
          $aBooksToConfirm[$sKeyCon]['azymut_wholesale_price'] = $aItem['azymut_wholesale_price'];
        }
        if (isset($aItem['siodemka_wholesale_price']) && $aItem['siodemka_wholesale_price'] > 0) {
          $aBooksToConfirm[$sKeyCon]['siodemka_wholesale_price'] = $aItem['siodemka_wholesale_price'];
        }
        if (isset($aItem['ateneum_wholesale_price']) && $aItem['ateneum_wholesale_price'] > 0) {
          $aBooksToConfirm[$sKeyCon]['ateneum_wholesale_price'] = $aItem['ateneum_wholesale_price'];
        }
        if (isset($aItem['platon_wholesale_price']) && $aItem['platon_wholesale_price'] > 0) {
          $aBooksToConfirm[$sKeyCon]['platon_wholesale_price'] = $aItem['platon_wholesale_price'];
        }
        if (isset($aItem['panda_wholesale_price']) && $aItem['panda_wholesale_price'] > 0) {
            $aBooksToConfirm[$sKeyCon]['panda_wholesale_price'] = $aItem['panda_wholesale_price'];
        }
        if (isset($aItem['siodemka_location'])) {
            $aBooksToConfirm[$sKeyCon]['siodemka_location'] = $aItem['siodemka_location'];
        }
        if (isset($aItem['isbn_plain'])) {
            $aBooksToConfirm[$sKeyCon]['isbn_plain'] = $aItem['isbn_plain'];
        }
        if (isset($aItem['ean_13'])) {
            $aBooksToConfirm[$sKeyCon]['ean_13'] = $aItem['ean_13'];
        }
        $aBooksToConfirm[$sKeyCon]['order_id'] = $aItem['order_id'];

        if (true == $source) {

        }
        $aBooksToConfirm[$sKeyCon]['source'] = $aItem['source'];
//        $aBooksToConfirm[$sKeyCon]['source'] = $aItem['source'];
			}
      return $aBooksToConfirm;
  }// end of parseBooksArray() method
  
  
  /**
   * Metoda pobiera dane zapowiedzi
   * 
   * @global array $aConfig
   * @param int $iId
   * @return array
   */
	private function _getPreviewBookData($iId){
		global $aConfig;
    
		$sSql = "SELECT A.id, A.isbn, A.name, B.name AS publisher, A.publication_year, A.location, A.azymut_index
							 FROM ".$aConfig['tabls']['prefix']."products A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers AS B
							 ON B.id = A.publisher_id
							 WHERE A.id = ".$iId;
		return $this->pDbMgr->GetRow('profit24', $sSql);
	}// end of _getPreviewBookData() method
  
  
  /**
   * Metoda pobiera dane produktu
   * 
   * @global array $aConfig
   * @param int $iId
   * @return array
   */
	protected function _getBookData($iId){
		global $aConfig;
    
		$sSql = "SELECT A.id, A.isbn, A.name, B.name AS publisher, A.publication_year, A.location, A.azymut_index, A.price_netto
							 FROM ".$aConfig['tabls']['prefix']."products A
							 LEFT JOIN ".$aConfig['tabls']['prefix']."products_publishers AS B
							 ON B.id = A.publisher_id
							 WHERE A.id = ".$iId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
	}// end of _getBookData() method
  
  
  /**
   * Sumuje ceny książek
   * 
   * @param array $aBooks
   * @throws Exception
   * @return float Suma cen produktów
   */
  protected function _sumBooksPrice($aBooks, $sSource) {
    
    $fSuma = 0.00;
    if (!empty($aBooks) && is_array($aBooks)) {
      foreach ($aBooks as $aBook) {
        if ($aBook[$sSource.'_wholesale_price'] > 0.00) {
          $fSuma += $aBook[$sSource.'_wholesale_price'];
        }
      }
    }
    if ($fSuma > 0.00) {
      return $fSuma;
    }
  }// end of _sumBooksPrice method
  
  
  /**
   * Sumuje ceny książek
   * 
   * @param array $aBooks
   * @throws Exception
   * @return float Suma cen produktów
   */
  protected function _sumBooksPriceNetto($aBooks) {
    
    $fSuma = 0.00;
    if (!empty($aBooks) && is_array($aBooks)) {
      foreach ($aBooks as $aBook) {
        if ($aBook['price_netto'] > 0.00) {
          $fSuma += ($aBook['price_netto']*$aBook['quantity']);
        }
      }
    }
    if ($fSuma > 0.00) {
      return $fSuma;
    }
  }// end of _sumBooksPriceNetto method
  
  
  /**
   * Metoda pobiera książki, uwzględniając limit
   * 
   * @param array $aBooksToConfirm
   * @param int $iLimit
   * @param string $sSource - źródło 'azymut', 'siodemka'
   * @return array
   */
  public function getLimitedBooksByPrice($aBooksToConfirm, $fLimitPrice, $sSource) {
    $aNewBooks = array();
    
    $fSumBooksPrice = $this->_sumBooksPrice($aBooksToConfirm, $sSource);
    if ($fSumBooksPrice >= $fLimitPrice) {
      // jest więcej
      $iPrevOrderId = 0;
      $iNextOrderId = 0;
      $iNowKey = 1;
      $bLastElement = FALSE;

      foreach ($aBooksToConfirm as $sSmb => $aBook) {
        if ($bLastElement === FALSE) {
          $aNewBooks[$sSmb] = $aBook;
          $aNewBooks[$sSmb]['items'] = implode(',', $aBook['items']);
          $aNewBooks = $this->_groupByProductFunctionList($aNewBooks, $sSource);
          $fSumBooksPrice = $this->_sumBooksPrice($aNewBooks, $sSource);
        }
        if ($fSumBooksPrice > $fLimitPrice && $bLastElement === FALSE) {
          // przekroczone, czyli jest to ostatni element zwróconej listy
          $iPrevOrderId = $aBook['order_id'];
          $bLastElement = TRUE;
        } elseif ($bLastElement === TRUE) {
          // kolejny element po ostatnim
          $iNextOrderId = $aBook['order_id'];
          unset($aNewBooks[$sSmb]);
          break;
          // konic elementów
        }
        $iNowKey++;
      }

      if ($iPrevOrderId > 0 && $iNextOrderId > 0 && $iPrevOrderId != $iNextOrderId) {
        // rózne zamówienie, sortujemy alfabetycznie i nic nie obcinamy
      } else {
         $aNewBooks = $this->_groupByProductFunctionList($aNewBooks, $sSource);
         return $aNewBooks;
      }
    } else {
      // poniżej limitu, przerywamy
      return false;
    }
    $aNewBooks = $this->_groupByProductFunctionList($aNewBooks, $sSource);
    return $aNewBooks;
  }// end of getLimitedBooksByPrice() method
  

  /**
   * Metoda sprawdza czy w przekazanej tablicy składowych 
   *  zamówień znajdują się różne zamówienia
   * 
   * @param type $aOrders
   * @return boolean
   */
  protected function _checkDifferentOrders($aNewBooks) {
    $iOrderIdTab = 0;
    foreach ($aNewBooks as $aBook) {
      if ($iOrderIdTab > 0 && $iOrderIdTab != $aBook['order_id']) {
        return true;
      } else {
        $iOrderIdTab = $aBook['order_id'];
      }
    }
    return false;
  } // end of _checkDifferentOrders() method
  
  
  /**
   * Metoda usuwa składowe zamówienia z przekazanego zamówienia
   * 
   * @param array $aNewBooks
   * @param int $iOrderId
   * @return array
   */
  protected function _delOrdersArr($aNewBooks, $iOrderId) {
    foreach ($aNewBooks as $sName => $aElement) {
      if ($aElement['order_id'] == $iOrderId) {
        unset($aNewBooks[$sName]);
      }
    }
    return $aNewBooks;
  }// end of _delOrdersArr() method
  
  
  /**
   * Metoda grupuje te same książki
   * 
   * @param type $aBooks
   * @return type
   */
	protected function _groupByProductFunctionList($aBooks, $sSource) {
    foreach ($aBooks as $sKey => &$aBook) {
      if ($aBook['id'] > 0) {
        // sprawdźmy czy w innych elementach nie mamy tego samego produktu
        $aEqualProducts = $this->_getEqualProducts($aBooks, $sKey, $aBook['id']);
        if (!empty($aEqualProducts)) {
          foreach ($aEqualProducts as $sEqBookKey => $aEqProds) {
            $aBooks[$sKey]['quantity'] += $aEqProds['quantity'];
            if (isset($aEqProds[$sSource.'_wholesale_price']) && $aEqProds[$sSource.'_wholesale_price'] > 0.00) {
              $aBooks[$sKey][$sSource.'_wholesale_price'] += $aEqProds[$sSource.'_wholesale_price'];
            }
            $aBooks[$sKey]['items'] .= ','.$aEqProds['items'];
            unset($aBooks[$sEqBookKey]);
          }
        }
      }
    }
    return $aBooks;
  }// end of _groupByProductFunctionList() method
  
  
  /**
   * Metoda wyszukuje te same produkty
   * 
   * @param array $aBooks
   * @param string $sBookOrderIdent
   * @param int $iBookId
   * @return array
   */
  protected function _getEqualProducts($aBooks, $sBookOrderIdent, $iBookId) {
    $aEqualProducts = array();
    foreach ($aBooks as $sKey => $aBook) {
      if ($sKey <> $sBookOrderIdent && intval($aBook['id']) == intval($iBookId)) {
        $aEqualProducts[$sKey] = $aBook;
      }
    }
    return $aEqualProducts;
  }// end of _getEqualProducts() method 
  
  
  /**
   * Metoda pobiera książki, uwzględniając limit
   * 
   * @param type $aBooksToConfirm
   * @param type $iLimit
   * @return type
   */
  public function getLimitedBooks($aBooksToConfirm, $iLimit, $sSource = 'internal') {
    $aNewBooks = array();
    $iAllBooks = count($aBooksToConfirm)-1;
    if ($iAllBooks >= $iLimit) {
      // jest więcej
      
      $iPrevOrderId = 0;
      $iNextOrderId = 0;
      $iNowKey = 1;
      $bLastElement = FALSE;
      
      foreach ($aBooksToConfirm as $sSmb => $aBook) {
        if ($bLastElement === FALSE) {
          $aNewBooks[$sSmb] = $aBook;
          $aNewBooks[$sSmb]['items'] = implode(',', $aBook['items']);
          $aNewBooks = $this->_groupByProductFunctionList($aNewBooks, $sSource);
        }
        if (count($aNewBooks)-1 == $iLimit) {
          $iPrevOrderId = $aBook['order_id'];
          $bLastElement = TRUE;
        } elseif ($bLastElement === TRUE) {
          $iNextOrderId = $aBook['order_id'];
          unset($aNewBooks[$sSmb]);
          break;
          // konic elementów
        }
        $iNowKey++;
      }
      
      if ($iPrevOrderId > 0 && $iNextOrderId > 0 && $iPrevOrderId != $iNextOrderId) {
        // rózne zamówienie, sortujemy alfabetycznie i nic nie obcinamy
        
      } else {
        $aNewBooks = $this->_groupByProductFunctionList($aNewBooks, $sSource);
        return $aNewBooks;
        /*
        // sprawdźmy czy jest inne zamówienie na liście tych 25 produktów
        if ($this->_checkDifferentOrders($aNewBooks) === TRUE) {
          // usuwamy zamowienia z ostatniego produktu
          $aNewBooks = $this->_delOrdersArr($aNewBooks, $iPrevOrderId);
          // sortujemy
          $aNewBooks = $this->_groupByProductFunctionList($aNewBooks, $sSource);
          return $aNewBooks;
        } else {
          // tylko sortujemy
          $aNewBooks = $this->_groupByProductFunctionList($aNewBooks, $sSource);
          return $aNewBooks;
        }
         */
      }
    } else {
      foreach ($aBooksToConfirm as $sSmb => $aBook) {
        $aNewBooks[$sSmb] = $aBook;
        $aNewBooks[$sSmb]['items'] = implode(',', $aBook['items']);
      }
      $aNewBooks = $this->_groupByProductFunctionList($aNewBooks, $sSource);
      return $aNewBooks;
    }
  }// end of getLimitedBooks() method
  
  
  /**
   * Metoda sortuje alfabetycznie książki
   * 
   * @param array $aArr
   * @return array
   */
  protected function _sortArr($aArr) {
    $tmp = Array(); 
    foreach($aArr as $iKey => &$ma){
      $tmp[$iKey] = &$ma["name"]; 
    }
    array_multisort($tmp, $aArr); 
    return $aArr;
  }// end of _sortArr() method
  
  
  /**
   * Metoda pobiera nowy numer listy produktów do zebrania lub zamówienia
   * 
   * @param type $iStatus
   * @return type
   * @throws Exception
   */
  public function getNewListNumber($iStatus) {
    $iStatus = intval($iStatus);
    if ($iStatus === 3) {
      return $this->_genListNumberInternal();
    } elseif ($iStatus === 1) {
      return $this->_genListNumberExternal();
    } else {
      throw new \Exception('Podano nieprawidłowy status zamówień. Błąd wewnętrzny !');
    }
  }// end of getNewListNumber() method
  
  
	/**
	 * Pobiera i generuje kolejny nr listy dla dostawcy wewnetrznego
   * UWAGA metoda używa lockowania tabel, lockowanie tabel commituje transakcje
   * http://dev.mysql.com/doc/refman/5.0/en/lock-tables-and-transactions.html
   * 
	 * @return string - nr listy
	 */
	protected function _genListNumberInternal() {
		global $aConfig;
		
//		$pDbMgr->Query('profit24', "LOCK TABLES ordered_items_numbering_int WRITE;");
		
		$sSql="SELECT * FROM ".$aConfig['tabls']['prefix']."ordered_items_numbering_int FOR UPDATE";
		$aNumber = $this->pDbMgr->GetRow('profit24', $sSql);
		if(!empty($aNumber)){
			if($aNumber['number_year'] == date('Y')){
				$sSql = "UPDATE ".$aConfig['tabls']['prefix']."ordered_items_numbering_int SET id=id+1";
				if ($this->pDbMgr->Query('profit24', $sSql)===false){
//					 $this->pDbMgr->Query('profit24', "UNLOCK TABLES");
					return false;
				} else{
//					$this->pDbMgr->Query('profit24', "UNLOCK TABLES");
					return sprintf("W%05d/%s",$aNumber['id'],date('Y'));
				}
			} else {
				$aValues = array(
					'id' => 2,
					'number_year' => date('Y')
				);
				if ($this->pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."ordered_items_numbering_int", $aValues,'',false) === false) {
//					$this->pDbMgr->Query('profit24', "UNLOCK TABLES");
		 			return false;
				} else {
//					$this->pDbMgr->Query('profit24', "UNLOCK TABLES");
					return sprintf("W%05d/%s",1,date('Y'));
				}
			}
		} else {
			$aValues = array(
				'id' => 2,
				'number_year' => date('Y')
			);
			if ($this->pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."ordered_items_numbering_int", $aValues,'',false) === false) {
//				  $this->pDbMgr->Query('profit24', "UNLOCK TABLES");
	 				return false;
			} else {
//				$this->pDbMgr->Query('profit24', "UNLOCK TABLES");
				return sprintf("W%05d/%s",1,date('Y'));
			}
		}
//		$this->pDbMgr->Query('profit24', "UNLOCK TABLES");
	} // end of genListNumberInternal() method	
  
  
	/**
	 * Pobiera i generuje kolejny nr listy dla dostawcy zewnetrznego
   * UWAGA metoda używa lockowania tabel, lockowanie tabel commituje transakcje
   * http://dev.mysql.com/doc/refman/5.0/en/lock-tables-and-transactions.html
   * 
	 * @return string - nr listy
	 */
	protected function _genListNumberExternal() {
		global $aConfig;
		
//		$this->pDbMgr->Query('profit24', "LOCK TABLES ordered_items_numbering_ext WRITE;");
		
		$sSql="SELECT * FROM ".$aConfig['tabls']['prefix']."ordered_items_numbering_ext FOR UPDATE";
		$aNumber = $this->pDbMgr->GetRow('profit24', $sSql);
		if(!empty($aNumber)){
			if($aNumber['number_year'] == date('Y')){
				$sSql = "UPDATE ".$aConfig['tabls']['prefix']."ordered_items_numbering_ext SET id=id+1";
				if ($this->pDbMgr->Query('profit24', $sSql)===false){
//					 $this->pDbMgr->Query('profit24', "UNLOCK TABLES");
					return false;
				} else{
//					$this->pDbMgr->Query('profit24', "UNLOCK TABLES");
					return sprintf("Z%05d/%s",$aNumber['id'],date('Y'));
				}
			} else {
				$aValues = array(
					'id' => 2,
					'number_year' => date('Y')
				);
				if ($this->pDbMgr->Update('profit24', $aConfig['tabls']['prefix']."ordered_items_numbering_ext", $aValues,'',false) === false) {
//					$this->pDbMgr->Query('profit24', "UNLOCK TABLES");
		 			return false;
				} else {
//					$this->pDbMgr->Query('profit24', "UNLOCK TABLES");
					return sprintf("Z%05d/%s",1,date('Y'));
				}
			}
		} else {
			$aValues = array(
				'id' => 2,
				'number_year' => date('Y')
			);
			if ($this->pDbMgr->Insert('profit24', $aConfig['tabls']['prefix']."ordered_items_numbering_ext", $aValues,'',false) === false) {
//				  $this->pDbMgr->Query('profit24', "UNLOCK TABLES");
	 				return false;
			} else {
//				$this->pDbMgr->Query('profit24', "UNLOCK TABLES");
				return sprintf('profit24', "Z%05d/%s",1,date('Y'));
			}
		}
//		$this->pDbMgr->Query('profit24', "UNLOCK TABLES");
	} // end of genListNumberExternal() method
  
  
  /**
   * Metoda ustawia składowe zamówienia jako zamówione na zewnątrz
   * 
   * @global array $aConfig
   * @param string $sItems - lista id orders_items 213,213,412
   * @return boolean
   */
	public function setHasNCItems($sItems){
		global $aConfig;
    
		if(!empty($sItems)){
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders_items
								SET sent_hasnc='1'
								WHERE id IN (".$sItems.") AND status='3'";
			if($this->pDbMgr->Query('profit24', $sSql) === false){
				return false;
			}
			return true;
		}
		return false; 
	}// end of setHasNCItems() method
  
  
  /**
   * Metoda zmienia ustawia status pozycjom, które zostaly wysłane w liście
   * 
   * @param int $iStatus - status
   * @param string $sItems - lista id orders_items 213,213,412
   * @return bool
   */
  public function setOrderedItemsByStatus($iStatus, $sItems) {
    if ($iStatus == '1'){
      if ($this->setOrderedItems($sItems) === false) {
        throw new \Exception(_('Wystąpił błąd podczas zmiany statusu pozycjom na zamówione'));
      }
    } elseif($iStatus == '3'){
      if ($this->setHasNCItems($sItems) === false) {
        throw new \Exception(_('Wystąpił błąd podczas zmiany statusu pozycjom na nie potwierdzone'));
      }
    }
    return true;
  }// end of setOrderedItemsByStatus() method
  
  
  /**
   * Metoda dodaje do bazy danych historii informację o wysłanym liście oraz wiązania lista do składowej zamówienia
   * 
   * @TODO poprawić 'number' => $aOrderData['azymut_order_id']
   * @param array $aOrderData - dane produktów w formacie takim jak azymut
   * @param string $sFilePath
   * @param int $iStatus
   * @param int $iSource
   * @return boolean
   * @throws Exception
   */
  public function addSendHistory($aOrderData, $sFilePath, $iStatus, $iSource, $sProvider) {
    
    $aValues = array(
			'content' => nl2br(htmlspecialchars(str_replace('><', ">\r\n<", file_get_contents($sFilePath)))),
			'saved_type' => '4',
			'status' => $iStatus,
			'source' => $iSource,
			'date_send' => 'NOW()',
			'send_by' => 'auto-'.$sProvider,
			'number' => $aOrderData[$sProvider.'_order_id']
		);
		$iExportId=false;
		if ( ($iExportId = $this->pDbMgr->Insert('profit24', "orders_send_history", $aValues, '', true)) === false) {
			throw new \Exception(_('Wystąpił błąd podczas dodawania listy zamówionych pozycji'));
		}
		else {//jezeli zapisal sie nowy eksport to dopisujemy ksiazki do niego
			foreach ($aOrderData['items'] as $aItem) {
        $aOrdersItems = explode(',', $aItem['items']);
        foreach ($aOrdersItems as $iOrderItemId) {
          $iOrderItemId = intval(trim($iOrderItemId));
          if ($iOrderItemId > 0) {
            $aValues = array(
              'send_history_id' => $iExportId,
              'item_id' => $iOrderItemId,
              'item_source_id' => ($iSource == '7' && $aItem['azymut_index'] != '' ? $aItem['azymut_index'] : $aItem['source_item_id'])
            );
            if ( $this->pDbMgr->Insert('profit24', "orders_send_history_items", $aValues, '', false) === false) {
              throw new \Exception(_('Wystąpił błąd podczas dodawania powiązań listy z produktami'));
            }
          }
        }
			}
		}
    return $iExportId;
  }// end of addSendHistory() method
  
  
  /**
   * Metoda dodaje atrybut do historii wysłanych
   * 
   * @throws Exception
   * @global object $this->pDbMgr
   * @param int $iHSId
   * @param string $mCol - kolumna
   * @param mixed $mVal - value
   * @return bool
   */
  public function addSetHistorySendAttr($iHSId, $mCol, $mVal) {
   
    
    $aHSendAttr = $this->_getHistorySendAttr($iHSId);
    $aValues = array('orders_send_history_id' => $iHSId, 
                     $mCol => $mVal);
    if (!empty($aHSendAttr)) {
      // aktualizacja
      if ($this->pDbMgr->Update('profit24', 'orders_send_history_attributes', $aValues, ' orders_send_history_id='.$iHSId) === false) {
        throw new \Exception(_('Wystąpił błąd podczas aktualizacji historii zamówionych o id '.$iHSId.' dane: '.var_export($aValues, true)));
      }
    } else {
      // dodanie
      if ($this->pDbMgr->Insert('profit24', 'orders_send_history_attributes', $aValues, '', false) === false) {
        throw new \Exception(_('Wystąpił błąd podczas dodawania historii zamówionych o id '.$iHSId.' dane: '.var_export($aValues, true)));
      }
    }
    return true;
  }// end of addSetHistorySendAttr() method
  
  
  /**
   * Metoda pobiera atrybuty historii wysłanych zamówień
   * 
   * @param int $iHSId
   * @return array
   */
  private function _getHistorySendAttr($iHSId) {
    
    
    $sSql = "SELECT * FROM orders_send_history_attributes WHERE orders_send_history_id=".$iHSId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }// end of _getHistorySendAttr method
  
  
  /**
   * Metoda zmienia status zamówienia na do zebrania z listy
   * 
   * @global array $aConfig
   * @param string $sItems - lista id orders_items 213,213,412
   * @return boolean
   */
	function setOrderedItems($sItems){
		global $aConfig;
    
		if(!empty($sItems)){
			$sSql = "UPDATE ".$aConfig['tabls']['prefix']."orders_items
								SET status = '2'
								WHERE id IN (".$sItems.") AND status='1'";
			if($this->pDbMgr->Query('profit24', $sSql) === false){
        $this->sendInfoMailError('Błąd - zmiana statusu składowej zamówienia', $sSql);
				return false;
			}
			return true;
		}
		return false; 
	}// end of setOrderedItems() method
  
  /**
	* Wysyła maila z logiem importu
	* @param $sTopic - temat maila
	* @param $sContent - treść maila
	* @return void
	*/
	private function sendInfoMailError($sTopic, $sContent){
		global $aConfig;

        if(!\Common::sendMail('', $aConfig['common']['import_sender_email'], $aConfig['common']['import_send_to'].';a.golba@profit24.pl', $sTopic, $sContent))
            dump('error sending mail');
	} // end of sendInfoMail() function
}// end of OrderMagazineData() class

?>