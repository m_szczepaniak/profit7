<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-18 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\Payment;

/**
 * Description of AutoMatchStatementToOrder
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */

use DatabaseManager;
use Exception;
use stdClass;


/**
 * Description of Payment
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class MatchStatementToOrder {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var stdClass
   */
  private $oStatement;

  /**
   *
   * @var array
   */
  private $aOrder;
  
  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @param stdClass $oStatement
   */
  public function setStatement(stdClass $oStatement) {
    $this->oStatement = $oStatement;
  }
  
  /**
   * 
   * @return array
   */
  public function getMatchedOrder() {
    return $this->aOrder;
  }
  
  /**
   * 
   * @return boolean
   * @throws Exception
   */
  public function matchOrder() {
    
    $this->aOrder = array();
    if ($this->validateStatementIsNullOrEmptyOrderNumber() === true) {
      $aOrder = $this->getOrderDataOrderNumber($this->oStatement->order_nr);
      if ($aOrder) {
        if ($this->validatePayOrder($aOrder) === false) {
          return false;
        } else {
          $this->aOrder = $aOrder;
          return true;
        }
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param array $aOrder
   * @return boolean
   */
  private function validatePayOrder($aOrder) {
    
    if ($aOrder['order_status'] == 5) {
      return false;
    }
    
    if ($aOrder['second_payment_enabled']) {
      if ($this->checkOrderPaymentEqualAccount($aOrder['second_payment_id'], $aOrder['website_id']) === false) {
        return false;
      }
    }
    if ($this->checkOrderPaymentEqualAccount($aOrder['payment_id'], $aOrder['website_id']) === false) {
      return false;
    }
    if ($this->checkOrderToPayEqualPaidAmount($aOrder['to_pay']) === false) {
      return false;
    } else {
      return true;
    }
  }
  
  /**
   * 
   * @param float $fToPayOrder
   * @return boolean
   */
  private function checkOrderToPayEqualPaidAmount($fToPayOrder) {
    
    if (floatval($fToPayOrder) == floatval($this->oStatement->paid_amount)) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param int $iPaymentId
   * @param int $iWebsiteId
   * @return boolean
   */
  private function checkOrderPaymentEqualAccount($iPaymentId, $iWebsiteId) {
    
    $sSql = 'SELECT ident_statments_file
             FROM bank_accounts 
             WHERE orders_payment_types_id = '.$iPaymentId.' AND
                   websites_id = '.$iWebsiteId;
    $iIdentStatmentsFile = $this->pDbMgr->GetOne('profit24', $sSql);
    if (intval($iIdentStatmentsFile) === intval($this->oStatement->ident_statments_file)) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param string $sOrderNumber
   * @return array
   */
  public function getOrderDataOrderNumber($sOrderNumber) {
    
    $aCols = array(
        'id', 'website_id', 'to_pay', 'paid_amount', 'payment_id', 'second_payment_enabled', 'second_payment_id', 'transport_id',
        'order_status'
    );
    return $this->pDbMgr->getTableRow('orders', $sOrderNumber, $aCols, 'profit24', 'order_number');
  }
  
  /**
   * 
   * @return bool
   */
  private function validateStatementIsNullOrEmptyOrderNumber() {
    if (!is_object($this->oStatement)) {
      throw new Exception(_('Nie podano odpowiedniego przelewu do dopasowania.'));
    }
    if ($this->oStatement->order_nr != '') {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param string $sString
   * @return boolean
   */
  public function tryShellOrderNumber($sString) {
    $aMatched = array();
    preg_match("/\s((31|24|06|12|16|74)[0-9]{10})([^\d]|$)/", $sString, $aMatched);
    if (!empty($aMatched) && isset($aMatched[1])) {
      $iInt = preg_replace('/\s/', '', $aMatched[1]);
      if (strlen($iInt) == 12) {
        return $iInt;
      }
    }

      preg_match("/\s((31|24|06|12|16|74)[0-9]{10})(\d{6})([^\d]|$)/", $sString, $aMatched);
      if (!empty($aMatched) && isset($aMatched[1])) {
          $iInt = preg_replace('/\s/', '', $aMatched[1]);
          if (strlen($iInt) == 12) {
              return $iInt;
          }
      }

    return false;
  }
}
