<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\Payment;

use DatabaseManager;
use Exception;

/**
 * Description of transferOverpayment
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class TransferOverpayment extends Payment {


  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   * 
   * @param DatabaseManager $pDbMgr
   * @param int $iOrderFromTransfer
   * @param int $iOrderToTransferId
   */
  public function __construct($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
    parent::__construct($pDbMgr);
  }

  /**
   * 
   * @param int $iOrderFromTransfer
   * @param int $iOrderToTransferId
   * @return boolean
   */
  public function doTransferOverpaymentSetMsg(&$sMsg, $iOrderFromTransfer, $iOrderToTransferId) {
    $aFromOrderData = $this->pDbMgr->getTableRow('orders', $iOrderFromTransfer, 
            array('balance', 'order_number', 'payment', 'payment_id', 'payment_type', 'payment_status', 'payment_date', 
                'second_payment_enabled', 'second_payment', 'second_payment_id', 'second_payment_status', 'second_payment_type', 'second_payment_date'));
    $aToOrderData = $this->pDbMgr->getTableRow('orders', $iOrderToTransferId, 
            array('order_number', 'payment', 'payment_id', 'payment_type', 'payment_status', 'second_payment_enabled', 'payment_date', 
                'second_payment_status', 'second_payment', 'second_payment_id', 'second_payment_type', 'second_payment_date', 'transport_id'));
    
    $fPayBalance = $aFromOrderData['balance'];
    if ($fPayBalance > 0) {
      $fSubPayBalance = -$fPayBalance;
      $iFromStatementId = $this->getLastOrderPayment($iOrderFromTransfer);

      try {
        if ($this->checkChangeDestinationPayment($iFromStatementId, $iOrderToTransferId, $aFromOrderData, $aToOrderData) === false) {
          throw new Exception(_('Błąd zmiany metody transportu.'));
        }
        
        if ($this->doPayBalanceOrder($iOrderFromTransfer, $fSubPayBalance) === false) {
          throw new Exception(_('Błąd opłacania zamówienia '.$aFromOrderData['order_number'] . ' balance: '.$fPayBalance));
        }

        if ($this->addPayment($iOrderFromTransfer, date("Y-m-d H:i:s"), $fSubPayBalance, ' Przeksięgowanie na '.$aToOrderData['order_number'], $aToOrderData['transport_number'], 'PW-OUT', $iFromStatementId) === false) {
          throw new Exception(_('Błąd opłacania zamówienia '.$aToOrderData['order_number'] . ' balance: '.$fPayBalance));
        }

        if ($this->addPayment($iOrderToTransferId, date("Y-m-d H:i:s"), $fPayBalance, ' Przeksięgowanie z '.$aFromOrderData['order_number'], $aFromOrderData['transport_number'], 'PW-IN', $iFromStatementId) === false) {
          throw new Exception(_('Błąd opłacania zamówienia '.$aToOrderData['order_number'] . ' balance: '.$fPayBalance));
        }

        if ($this->doPayBalanceOrder($iOrderToTransferId, $fPayBalance) === false) {
          throw new Exception(_('Błąd opłacania zamówienia '.$aToOrderData['order_number'] . ' balance: '.$fPayBalance));
        }
        
      } catch (Exception $ex) {
        throw $ex;
      }


      
      $sMsg .= _('Przeksięgowano nadpłatę z zamówienia: '.$aFromOrderData['order_number'].' 
        na zamówienie: '.$aToOrderData['order_number'].' w wysokości '.$fPayBalance);
      return true;
    } else {
      throw new Exception(_('Błąd opłacania zamówienia '.$aToOrderData['order_number'] . ' balance: '.$fPayBalance));
    }
  }
  
  /**
   * 
   * @param int $iFromStatementId
   * @param int $iToOrderId
   * @param array $aFromOrderData
   * @param array $aToOrderData
   * @return boolean
   */
  private function checkChangeDestinationPayment($iFromStatementId, $iToOrderId, $aFromOrderData, $aToOrderData) {
    
    if ($this->validateChange($aFromOrderData, $aToOrderData) === false) {
        throw new Exception(_('Błąd przeksięgowania przelewów - niedopasowane metody'));
    } else {
      if ($iFromStatementId > 0) {
        // musi być zgodny rachunek bankowy, jak nie to zmieniamy/dodajemy metodę transportu
        $cBankIdentStatement = $this->getBankIdentStatment($iFromStatementId);
        if ($this->checkBankAccountEqual($iToOrderId, $cBankIdentStatement) === false) {
          if ($this->changeDestinationPaymentByStatement($iToOrderId, $aFromOrderData, $aToOrderData) === false) {
            return false;
          }
        } else {
          if ($this->setOrderAsPayed($iToOrderId, $aToOrderData) === false) {
            return false;
          }
        }
      } else {
        if ($this->changeDestinationPaymentPayU($iToOrderId, $aFromOrderData, $aToOrderData) === false) {
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * 
   * @param int $iToOrderId
   * @param array $aToOrderData
   * @return bool
   */
  private function setOrderAsPayed($iToOrderId, $aToOrderData) {
    
    $bSecond = $this->getPaymentMethodChange($aToOrderData);
    $aValues = $this->setOrderPayedData($bSecond);
    return $this->doUpdateOrderPayment($aValues, $iToOrderId);
  }
  
  /**
   * 
   * @param array $aToOrderData
   * @param array $aOrderData
   * @param bool $bSecond
   * @return array
   */
  private function setOrderPayedData($bSecond) {
    
    $sColPrefix = '';
    if ($bSecond === true) {
      $sColPrefix = 'second_';
    }
    $aPaymentData = [
          $sColPrefix.'payment_status' => '1', // to powinno być 1
          $sColPrefix.'payment_date' => 'NOW()',
      ];
    if ($bSecond) {
      $aPaymentData['second_payment_enabled'] = '1';
    }
    return $aPaymentData;
  }
  
  /**
   * przeksięgowanie nadpłaty P14 dni na TYLKO PP zrzuca błąd
   * przeksięgowanie nadpłaty PP na przelew TYLKO 14 dni zrzuca błąd
   * przeksięgowanie nadpłaty na zam płatne TYLKO przy odbiorze zrzuca bląd
   * 
   * Ogólnie mówiąc nie ma sytuacji kiedy automatycznie zmieniamy pierwszą metodę płatnosci
   * 
   * @param array $aFromOrderData
   * @param array $aToOrderData
   * @return bool
   */
  private function validateChange(array $aFromOrderData, array $aToOrderData) {
    
    $aFromPaymentData = $this->getLastPaymentData($aFromOrderData);
    switch ($aFromPaymentData['payment_type']) {
      default:
        return $this->validatePaymentMethod($aFromPaymentData['payment_type'], $aToOrderData);
      case 'platnoscipl': // te metody można bez problemu matchowac między sobą 
      case 'card':
        return true;
    }
  }// end of validatePaymentMethod() method
  
  /**
   * 
   * @param string $sFromPaymentMethod
   * @param array $aToOrderData
   * @return boolean
   */
  private function validatePaymentMethod($sFromPaymentMethod, $aToOrderData) {
    if ($aToOrderData['payment_type'] == $sFromPaymentMethod ||
         ($aToOrderData['second_payment_enabled'] == '1' && $aToOrderData['second_payment_type'] == $sFromPaymentMethod)) {
       return true;
     } else {
       return false;
     }
  }

  /**
   * 
   * @param int $iToOrderId
   * @param array $aFromOrderData
   * @param array $aToOrderData
   * @return boolean
   */
  private function changeDestinationPaymentByStatement($iToOrderId, $aFromOrderData, $aToOrderData) {
    
    $aFromPaymentData = $this->getLastPaymentData($aFromOrderData);
    if ($aToOrderData['second_payment_enabled'] == '1' && $aToOrderData['second_payment_status'] == '1') {
      return false;// jak już druga jest ustawiona i opłacona to nie ma szans zmienić metody płatności
    } else {
      $bSecond = $this->getPaymentMethodChange($aToOrderData);
      $aFromPaymentData = $this->getLastPaymentData($aFromOrderData);
      if ($this->changePayment($iToOrderId, $aToOrderData, $aFromPaymentData, $bSecond) === false) {
        return false;
      } else {
        return true;
      }
    }
    return false;
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param int $cBankIdentStatment
   * @return bool|int
   */
  private function checkBankAccountEqual($iOrderId, $cBankIdentStatment) {
    
    $sSql = 'SELECT DISTINCT O.id, BA_F.ident_statments_file as first_ident_statments_file, BA_S.ident_statments_file as second_ident_statments_file 
             FROM orders AS O
             JOIN bank_accounts AS BA_F
              ON BA_F.orders_payment_types_id = O.payment_id
             LEFT JOIN bank_accounts AS BA_S
              ON O.second_payment_enabled = "1" AND BA_S.orders_payment_types_id = O.second_payment_id
             WHERE O.id = '.$iOrderId;
    $aOrderPaymentData = $this->pDbMgr->GetRow('profit24', $sSql);
    if ($aOrderPaymentData['first_ident_statments_file'] == $cBankIdentStatment || 
            (intval($aOrderPaymentData['second_ident_statments_file']) > 0 && $aOrderPaymentData['second_ident_statments_file'] == $cBankIdentStatment)) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param int $iFromStatementId
   * @return int
   */
  private function getBankIdentStatment($iFromStatementId) {
    
    $sSql = 'SELECT ident_statments_file
             FROM orders_bank_statements
             WHERE id = '.$iFromStatementId;
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iToOrderId
   * @param array $aFromOrderData
   * @param array $aToOrderData
   * @return boolean
   */
  private function changeDestinationPaymentPayU($iToOrderId, $aFromOrderData, $aToOrderData) {
    
    // PayU
    $aPayAvailablePType = array(
        'card', 
        'platnoscipl'
    );
    // wiemy że przenosimy z payu
    if ($aToOrderData['second_payment_enabled'] == '1' && $aToOrderData['second_payment_status'] == '1' && in_array($aToOrderData['second_payment_type'], $aPayAvailablePType)) {
      // druga metoda płatności, jest zgodna
      return true;
    } elseif ($aToOrderData['payment_status'] == '1' && in_array($aToOrderData['payment_type'], $aPayAvailablePType)) {
      // pierwsza metoda płatności jest zgodna
      return true;
    } else {
      // musimy zmienić/dodać metodę płatności, ale czy możemy...
      if ($aToOrderData['second_payment_enabled'] == '1' && $aToOrderData['second_payment_status'] == '1') {
        return false;
      } else {
        // zmieniamy na PayU
        // ta zmiana powinna odbywać się przez recount, ale wg mnie nie powinna przeliczać zmienia wartości ile jest do zapłaty za zamówienie.
        // blokuj przeliczanie kosztu transportu.
        $bSecond = $this->getPaymentMethodChange($aToOrderData);
        $aFromPaymentData = $this->getLastPaymentData($aFromOrderData);
        if ($this->changePayment($iToOrderId, $aToOrderData, $aFromPaymentData, $bSecond) === false) {
          return false;
        } else {
          return true;
        }
      }
    }
    return false;
  }
  
  /**
   * 
   * @param array $aToOrderData
   * @return boolean
   */
  private function getPaymentMethodChange($aToOrderData) {
    
    if ($aToOrderData['payment_status'] == '0') {
      $bSecond = false;
    } else if ($aToOrderData['second_payment_enabled'] == '0' || $aToOrderData['second_payment_status'] == '0') {
      $bSecond = true;
    } else {
      throw new Exception('Zamówienie ma już opłacone metody płatności');
    }
    return $bSecond;
  }
  
  /**
   * 
   * @param array $aFromOrderData
   * @return array
   */
  private function getLastPaymentData($aFromOrderData) {
    if ($aFromOrderData['second_payment_enabled'] == '1' && $aFromOrderData['second_payment_status'] == '1') {
      return $this->getOrderPaymentData($aFromOrderData, true);
    } else {
      return $this->getOrderPaymentData($aFromOrderData, false);
    }
  }
  
  /**
   * 
   * @param array $aOrderData
   * @param bool $bSecond
   * @return array
   */
  private function getOrderPaymentData($aOrderData, $bSecond) {
    $sColPrefix = '';
    if ($bSecond === true) {
      $sColPrefix = 'second_';
    }
    $aPaymentData = [
          'payment' => $aOrderData[$sColPrefix.'payment'],
          'payment_id' => $aOrderData[$sColPrefix.'payment_id'],
          'payment_type' => $aOrderData[$sColPrefix.'payment_type'],
          'payment_status' => $aOrderData[$sColPrefix.'payment_status'],
          'payment_date' => $aOrderData[$sColPrefix.'payment_date'],
      ];
    return $aPaymentData;
  }
  
  /**
   * 
   * @param array $aToOrderData
   * @param array $aOrderData
   * @param bool $bSecond
   * @return array
   */
  private function setOrderPaymentData(array $aToOrderData, array $aOrderData, $bSecond) {
    
    $sColPrefix = '';
    if ($bSecond === true) {
      $sColPrefix = 'second_';
    }
    $aPaymentData = [
          $sColPrefix.'payment' => $aOrderData['payment'],
          $sColPrefix.'payment_id' => $this->getPaymentIdMatchedTransport($aToOrderData['transport_id'], $aOrderData['payment_type']),
          $sColPrefix.'payment_type' => $aOrderData['payment_type'],
          $sColPrefix.'payment_status' => '1', // to powinno być 1
          $sColPrefix.'payment_date' => 'NOW()',
      ];
    if ($bSecond) {
      $aPaymentData['second_payment_enabled'] = '1';
    }
    return $aPaymentData;
  }
  
  /**
   * 
   * @param int $iTransportId
   * @param string $sTransportPType
   * @return int
   */
  private function getPaymentIdMatchedTransport($iTransportId, $sTransportPType) {
    
    $sSql = 'SELECT id 
      FROM `orders_payment_types` 
      WHERE transport_mean = '.$iTransportId.' 
        AND ptype = "'.$sTransportPType.'"';
    $iOPLId = $this->pDbMgr->GetOne('profit24', $sSql);
    if ($iOPLId > 0) {
      return $iOPLId;
    } else {
      throw new Exception(_('Wystąpił błąd podczas przeksięgowania zamówienia'));
    }
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param array $aToOrderData
   * @param array $aChangePaymentData
   * @param bool $bSecondPayment
   */
  private function changePayment($iOrderId, array $aToOrderData, array $aChangePaymentData, $bSecondPayment) {
    
    $aValues = $this->setOrderPaymentData($aToOrderData, $aChangePaymentData, $bSecondPayment);
    return $this->doUpdateOrderPayment($aValues, $iOrderId);
  }
  
  /**
   * 
   * @param array $aValues
   * @param int $iOrderId
   */
  private function doUpdateOrderPayment($aValues, $iOrderId) {
    
    return $this->pDbMgr->Update('profit24', 'orders', $aValues, ' id = '.$iOrderId.' LIMIT 1');
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return int
   */
  private function getLastOrderPayment($iOrderId) {
    
    $sSql = 'SELECT statement_id 
             FROM orders_payments_list
             WHERE order_id = '.$iOrderId.' AND 
                   statement_id IS NOT NULL AND
                   ammount > 0
             ORDER BY id DESC
             LIMIT 1';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }  
}