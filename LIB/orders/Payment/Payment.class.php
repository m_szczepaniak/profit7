<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-16 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\Payment;

use DatabaseManager;

/**
 * Description of Payment
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Payment {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var array
   */
  private $aPTypes = [
      'internal' => ['PW-IN', 'PW-OUT'],
      'external' => ['tba', 'bank_14days', 'opek', 'bank_transfer', 'card', 'platnoscipl']
  ];

  public function __construct($pDbMgr) {
    
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * Metoda dodaje płatność do listy płatności zamówienia
   * 
   * @global array $aConfig
   * @param int $iOrderId
   * @param string $sTransferDate
   * @param string $sTransferPayedValue
   * @param string $sTransferTitle
   * @param string $sTransportNumber
   * @param string $sType 
   * @param int $iStatementId
   */
  public function addPayment($iOrderId, $sTransferDate, $sTransferPayedValue, $sTransferTitle, $sTransportNumber, $sType, $iStatementId = 0, $bankAccount = '') {
    global $aConfig;
    
    $aValues = array(
      'order_id' => $iOrderId,
      'statement_id' => ($iStatementId > 0 ? $iStatementId : 'NULL'),
      'date' => $sTransferDate,
      'ammount' => floatval(str_replace(',', '.', $sTransferPayedValue)),
      'type' => $sType,
      'iban' => $bankAccount,
      'reference' => $sTransportNumber, // nr listu przewozowego
      'title' => $sTransferTitle,
      'matched_by' => $_SESSION['user']['name']
    );
      
    if($this->pDbMgr->Insert('profit24', "orders_payments_list", $aValues,'', false) === false){
        return false;	
    }
    return true;
  }// end of _addPayment() method

  
  /**
   * 
   * @param int $iOrderId
   * @param float $fAddBalance
   */
  public function doPayBalanceOrder($iOrderId, $fAddBalance) {
    
    $sAddSQL = '';
    if ($fAddBalance >= 0) {
      $sAddSQL = ' + '.$fAddBalance;
    } else {
      $sAddSQL = $fAddBalance;
    }
    
    $sSql = "UPDATE orders
         SET paid_amount = (paid_amount ".$sAddSQL.")
         WHERE id = ".$iOrderId;
    if($this->pDbMgr->Query('profit24', $sSql) === false) {
      return false;
    }
    return true;
  }
  
  /**
   * 
   * @param int $iPaymentId
   * @param int $iStatementId
   * @param int $iOrderId
   * @return bool
   */
  public function deletePayment($iPaymentId, $iStatementId, $iOrderId) {
    
    if ($this->checkPaymentTypeCanDelte($iStatementId)) {
      $aPayment = $this->pDbMgr->getTableRow('orders_payments_list', $iPaymentId, array('*'));
      if (!empty($aPayment)) {
        if ($this->checkStatmentRestore($aPayment)) {
          if ($this->backUnmatchOrdersPayment($iStatementId) === false) {
            return false;
          } else {
            if ($this->returnStatement($iStatementId) === false) {
              return false;
            } else {
              return true;
            }
          }
        } else {
          if ($this->backInternalTransfer($iStatementId, $iOrderId, $aPayment) === false) {
            return false;
          } else {
            return true;
          }
        }
      }
    } else {
      throw new \Exception(_('Nie można usunąć tego przelewu, ponieważ jest to przelew nie związany z konkretnym przelewem bankowym.'));
    }
  }
  
  /**
   * 
   * @param int $iStatementId
   * @return bool
   */
  private function returnStatement($iStatementId) {
    
    $aValues = array(
        'finished' => '0',
        'deleted' => '0',
    );
    return $this->pDbMgr->Update('profit24', 'orders_bank_statements', $aValues, ' id = '.$iStatementId.' LIMIT 1');
  }
  
  /**
   * 
   * @param int $iStatementId
   * @param array $aPayment
   * @return bool
   */
  private function backUnmatchOrdersPayment($iStatementId) {
    $aOrders = $this->getOrdersPayedStatement($iStatementId);
    return $this->unmatchOrdersPayment($aOrders, $iStatementId);
  }
  
  /**
   * 
   * @param array $aOrders
   * @param int $iStatementId
   * @return bool
   */
  private function unmatchOrdersPayment($aOrders, $iStatementId) {
    foreach ($aOrders as $aOrder) {
      if ($aOrder['ammount'] > 0) {
        if ($this->addPaymentAndBalanceOrder($aOrder['order_id'], -$aOrder['ammount'], $iStatementId, 'PW-OUT', ' Rozparowanie płatności przelewm o id '.$iStatementId) === false) {
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * 
   * @param int $iStatementId
   */
  private function getOrdersPayedStatement($iStatementId) {
    
    $sSql = 'SELECT *
             FROM orders_payments_list 
             WHERE statement_id = '.$iStatementId;
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
  
  /**
   * 
   * @param int $iStatementId
   * @param int $iOrderId
   * @param array $aPayment
   */
  private function backInternalTransfer($iStatementId, $iOrderId, $aPayment) {
    $iSecondOrderId = $this->shellOrderInternalTransfer($aPayment['title']);
    // na ten drugi trzeba przeksięgować ponownie ma przyjść na taką kwotę
    $aFirstOrder = $this->pDbMgr->getTableRow('orders', $iOrderId, array('order_number'));
    $aSecondOrder = $this->pDbMgr->getTableRow('orders', $iSecondOrderId, array('order_number'));
    if ($aPayment['ammount'] > 0) {
      // czyli przeksięgowujemy na minus
      $this->transferPayment($iStatementId, $iOrderId, $iSecondOrderId, $aPayment['ammount'], $aFirstOrder['order_number'], $aSecondOrder['order_number']);
    } else {
      $this->transferPayment($iStatementId, $iSecondOrderId, $iOrderId, $aPayment['ammount'], $aSecondOrder['order_number'], $aFirstOrder['order_number']);
    }
  }
  
  /**
   * 
   * @param int $iStatementId
   * @param int $iFromOrderId
   * @param int $iToOrderId
   * @param flaot $fBalance
   * @param string $sFromOrderNumber
   * @param string $sToOrderNumber
   */
  private function transferPayment($iStatementId, $iFromOrderId, $iToOrderId, $fBalance, $sFromOrderNumber, $sToOrderNumber) {
    if ($this->addPaymentAndBalanceOrder($iFromOrderId, -$fBalance, $iStatementId, 'PW-OUT', 'Przywrócenie przelewu na '.$sToOrderNumber) === false) {
      return false;
    }
    
    if ($this->addPaymentAndBalanceOrder($iToOrderId, $fBalance, $iStatementId, 'PW-IN', 'Przywrócenie przelewu z '.$sFromOrderNumber) === false) {
      return false;
    }
    
    return true;
  }
  
  /**
   * 
   * @param int $iOrderId
   * @param float $fPaidAmount
   * @param int $iStatementId
   * @param string $sType
   * @param string $sTitle
   * @return boolean
   */
  private function addPaymentAndBalanceOrder($iOrderId, $fPaidAmount, $iStatementId, $sType, $sTitle) {
    
    if ($this->addPayment($iOrderId, 'NOW()', $fPaidAmount, $sTitle, '', $sType, $iStatementId) === false) {
      return false;
    } else {
      if ($this->doPayBalanceOrder($iOrderId, $fPaidAmount) === false) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * 
   * @param string $sTitle
   * @return string
   * @throws \Exception
   */
  private function shellOrderInternalTransfer($sTitle) {
    
    $aMatched = array();
    preg_match('/(\d{12})$/', $sTitle, $aMatched);
    if ($aMatched[1] != '') {
      $sOrderNumber = $aMatched[1];
    } else {
      throw new \Exception(_('Wystąpił błąd podczas pobierania sąsiedniego zamóienia'));
    }
    $sSql = 'SELECT id FROM orders WHERE order_number = "'.$sOrderNumber.'"';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
  
  
  
  /**
   * 
   * @param integer $iStatementId
   * @return boolean
   */
  private function checkPaymentTypeCanDelte($iStatementId) {
    
    if ($iStatementId > 0) {
      return true;
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param array $aPayment
   * @return boolean
   */
  private function checkStatmentRestore($aPayment) {
    if (in_array($aPayment['type'], $this->aPTypes['internal'])) {
      // zwmówienie wewnętrzne
      return false; // wewnerzne przeksiegowanie
    } else {
      // przelew zewnętrzny
      return true;
    }
  }
  
  /**
   * 
   * @param int $iOrderId
   * @return boolean
   */
  public function markAsExported($iOrderId) {
    $aValues = array(
        'erp_export' => "NOW()"
    );
    if ($this->pDbMgr->Update('profit24', 'orders_payments_list', $aValues, ' id = '.$iOrderId) === false) {
      return false;
    } else {
      return true;
    }
  }
  
  /**
   * 
   * @param date $fData
   * @return array
   */
  public function getPaymentsToERP($fData) {
    
    // type opek
    // type tba
    // type payu
    // pobieramy najpierw standardowe płatności
    $iFedExId = 1825;
    $iTBAId = 9006;
    $iRUCHId = 6690;
    $iPPId = 9267;
    $sSql = '
      SELECT 
        BA.streamsoft_id, 
        DATE(OPL.date) as c_date, 
        IF(OPL.ammount > 0.00, "BP_KROZ", "BW_KROZ2") as `type`, 
        IF(OPL.type = "bank_14days", OUA.streamsoft_id, 
          IF(OPL.type = "opek", "'.$iFedExId.'", 
            IF(OPL.type = "tba", "'.$iTBAId.'", 
              IF(OPL.type = "ruch", "'.$iRUCHId.'", 
                IF(OPL.type = "poczta_polska" OR OPL.type = "poczta-polska-doreczenie-pod-adres", "'.$iPPId.'", "0")
              )
            )
          )
        ) AS kontrahent,
        REPLACE(IF(OPL.ammount > 0, OPL.ammount, 0), ".", ",") as wplata, 
        REPLACE(IF(OPL.ammount <= 0, OPL.ammount, 0)*(-1), ".", ",") as wyplata,
        O.order_number,
        OPL.id
      FROM orders_payments_list AS OPL
      JOIN orders_bank_statements AS OBS
       ON OPL.statement_id = OBS.id
      JOIN bank_accounts AS BA
       ON BA.ident_statments_file = OBS.ident_statments_file
      JOIN orders AS O
       ON O.id = OPL.order_id
      LEFT JOIN orders_users_addresses AS OUA
        ON OUA.order_id = O.id AND OUA.streamsoft_id IS NOT NULL
      WHERE 
       OPL.statement_id > 0 AND 
       OPL.erp_export IS NULL AND 
       IF(OPL.type = "bank_14days", (OUA.streamsoft_id > 0 AND OUA.streamsoft_id IS NOT NULL), 1=1)
       '
       .($fData != '' ? ' AND DATE(OPL.date) = "'.$fData.'"' : '').
       '
      GROUP BY OPL.id
              ';
      /*
       AND OPL.id NOT IN (
        SELECT 
           OPL.id
         FROM orders_payments_list AS OPL
         JOIN orders_bank_statements AS OBS
          ON OPL.statement_id = OBS.id
         JOIN bank_accounts AS BA
          ON BA.ident_statments_file = OBS.ident_statments_file
         JOIN orders AS O
          ON O.id = OPL.order_id
         WHERE 
          OPL.statement_id > 0 AND 
          OPL.erp_export IS NULL AND
          DATE(O.order_date) > "2015-04-30"
      )
      */
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }
}
