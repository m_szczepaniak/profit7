<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-12-17 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\orders\Payment;

use DatabaseManager;

/**
 * Description of lockStatements
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class lockStatements {

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;

  const TABLE_NAME = 'orders_bank_statements_lock';
  
  public function __construct(DatabaseManager $pDbMgr) {
    
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @return boolean
   */
  public function lock() {
    $sSql = 'SELECT locked FROM orders_bank_statements_lock FOR UPDATE';
    $iLocked = $this->pDbMgr->GetOne('profit24', $sSql);
    
    if (intval($iLocked) === 1) {
      $this->pDbMgr->Update('profit24', 'orders_bank_statements_lock', ['locked' => 1]);
      return false;
    } else {
      if ($this->pDbMgr->Update('profit24', 'orders_bank_statements_lock', ['locked' => 1]) === false) {
        return false;
      }
      return true;
    }
  }
  
  /**
   * 
   * @return boolean
   */
  public function unlock() {
    
    if ($this->pDbMgr->Update('profit24', 'orders_bank_statements_lock', ['locked' => 0], ' id > 0') === false) {
      return false;
    }
    return true;
  }
}
