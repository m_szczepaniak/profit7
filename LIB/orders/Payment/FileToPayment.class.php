<?php
namespace LIB\orders\Payment;

use Common;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-12-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class FileToPayment {

  private $pDbMgr;
  private $sFile;

  function __construct($pDbMgr, $sFile) {
    
    $this->sFile = $sFile;
    $this->pDbMgr = $pDbMgr;
  }
  
  public function checkFileItems() {
    $xml = simplexml_load_file($this->sFile);
    $fAmountSum = 0;
    foreach($xml->Transaction as $transaction) {
      $fAmount = Common::formatPrice2(str_replace('.', '', $transaction->amount));
      if ($fAmount > 0.00) {
        $fAmountSum += $fAmount;
      }
//      $iStatementId = $this->getPaymentId($transaction);
//      if ($iStatementId > 0) {
//        echo 'OK ';
//      } else {
//        print_r($transaction);
//        echo 'ERR ';
//      }
//      $aOrder = $this->getStatementOrderBalance($iStatementId);
//      if ($aOrder['balance'] == 0.00) {
//        echo 'OK ';
//      } else {
//        echo 'ERR ';
//        print_r($transaction);
//      }
    }
    dump($fAmountSum);
  }
  
  private function getPaymentId($t) {
    global $pDB;
    $sSql = 'SELECT id 
             FROM orders_bank_statements
             WHERE TRIM(title) = '.$pDB->quoteSmart((string)$t->description);
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }

  /**
   * 
   * @param int $iStatementId
   * @return float
   */
  private function getStatementOrderBalance($iStatementId) {
    
    $sSql = 'SELECT O.balance, O.*
             FROM orders_payments_list AS OPL
             JOIN orders AS O
              ON O.id = OPL.order_id
             WHERE OPL.statement_id = '.$iStatementId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
}
