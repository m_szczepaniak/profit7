<?php

namespace Items;

use LIB\Helpers\ArrayHelper;

class Confirmer
{
  
  public function changeConfirmedQuantityItems($iOSHId) {
    
    $aInConfirmedItems = $this->getOrderedItems($iOSHId, array('item_id', 'in_confirmed_quantity'));
      include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/lib/OrderRecount.class.php');
    $oOrderRecount = new \OrderRecount();

    foreach($aInConfirmedItems as $item) {
      $changedItem = $this->changeConfirmedQuantityItem($item);
      if ($oOrderRecount->recountOrder($changedItem['order_id'], false) === false) {
          return false;
      }

      $aInConfirmedItems = $this->getOrderedItems($iOSHId, array('item_id', 'in_confirmed_quantity'));

      if(false === $this->changeElementsStatusToLines($iOSHId, $aInConfirmedItems)){
        return false;
      }
    }

    return true;
  }
  
  
  /**
   * Przetwarza listę pozycji, potwierdza, w przypadku mniejszej ilości dodaje duplikat na brak
   * @param type $iIID item id
   */
  public function changeConfirmedQuantityItem($aInConfirmedItem) {

    $aOrderItem = $this->getOrderItemData($aInConfirmedItem['item_id']);
    //Jeżeli ilość potwierdzonych < zamówionych dodajemy duplikat zamówionej pozycji i zmieniamy ilość
    if ($aOrderItem['quantity'] > $aInConfirmedItem['in_confirmed_quantity'] && $aInConfirmedItem['in_confirmed_quantity'] > 0) {
      if ($aOrderItem['deleted'] != '1' && $aOrderItem['order_status'] != '5') {
        include_once ($_SERVER['DOCUMENT_ROOT'].'/LIB/communicator/sources/CommonSources.class.php');;
        $oCommonSources = new \CommonSources();
        $iToConfirm = $aInConfirmedItem['in_confirmed_quantity'];
        $iMissing = $aOrderItem['quantity'] - $aInConfirmedItem['in_confirmed_quantity'];
        $oCommonSources->addNewDuplicateItem($aOrderItem['id'], $iMissing);
        $oCommonSources->changeOrderQuantity($aOrderItem['id'], $iToConfirm);
        return $this->getOrderItemData($aInConfirmedItem['item_id']);
      }
    }
    return $aOrderItem;
  }
  
  
  /**
   * @param $aInConfirmedItems
   */
  private function changeElementsStatusToLines($iOSHId, $aInConfirmedItems)
  {
      global $pDbMgr;

      $elementsToChangeInternalStatus = [];

      foreach ($aInConfirmedItems as $element) {
          if (false === $this->checkIsNewestOrderToSource($iOSHId, $element['item_id'])) {
              if ($element['in_confirmed_quantity'] == 0) {
                  $elementsToChangeInternalStatus[] = $element['item_id'];
                  if ($pDbMgr->Update('profit24', 'orders_items', ['status' => 0], ' id = ' . $element['item_id']) === false) {
                      return false;
                  }
              }


              if (false === empty($elementsToChangeInternalStatus)) {
                  if (false === $this->changeInternalStatus($pDbMgr, $elementsToChangeInternalStatus)) {
                      return false;
                  }
              }
          }

      }
      return true;
  }


    /**
     * @param $iOSHId
     * @param $iOId
     * @return mixed
     */
    private function checkIsNewestOrderToSource($iOSHId, $iOId) {
        global $pDbMgr;

        $sSql = 'SELECT id FROM orders_send_history_items WHERE send_history_id > '.$iOSHId.' AND send_history_id <> '.$iOSHId.' AND item_id = '.$iOId;
        return ($pDbMgr->GetOne('profit24', $sSql) > 0 ? TRUE : FALSE);
    }


  /**
   * 
   * @global \DatabaseManager $pDbMgr
   * @param int $iOrderId
   * @return bool
   */
  private function changeOrderToNew($iOrderId) {
    global $pDbMgr;
    
    return $pDbMgr->Update('profit24', 'orders', ['internal_status' => '1'], ' id = '.$iOrderId);
  }
  
  private function changeInternalStatus(\DatabaseManager $pDbMgr, $elementsToChangeInternalStatus)
  {
    $implodedItemsIds = implode(',', $elementsToChangeInternalStatus);
    $ordersIds = $pDbMgr->GetCol('profit24', "SELECT distinct order_id FROM orders_items where id in($implodedItemsIds)");

    foreach($ordersIds as $orderId){
      if ($this->changeOrderToNew($orderId) === false) {
        return false;
      }
    }

    return true;
  }

    public function divideOrderItem($iOrderId, array $orderItem, $iConfirmed, $iMissing, $oldItemNewStatus)
    {
        include_once ($_SERVER['DOCUMENT_ROOT'].'/LIB/communicator/sources/CommonSources.class.php');;
        $oCommonSources = new \CommonSources();

        // jak sie nic nie zarezerwowalo to nie duplikujemy tylko przenosimy wpis do kresek
        if ($iConfirmed < 1) {
            if ($this->changeOrderToNew($iOrderId) === false) {
              throw new Exception("Błąd zmiany statusu zamówienia, wycofanie do kresek ".$iOrderId);
            }
            return $oCommonSources->changeOrderItemStatus($orderItem['id'], 0);
        }

        if ($iMissing > 0){
            $oCommonSources->addNewDuplicateItem($orderItem['id'], $iMissing);
            $oCommonSources->changeOrderQuantity($orderItem['id'], $iConfirmed, $oldItemNewStatus);
        }
    }

  /**
   * Metoda pobiera składową zamóienia
   *
   * @global \DatabaseManager $pDbMgr
   * @param int $iOrderItemId
   * @return array
   */
  private function getOrderItemData($iOrderItemId) {
    global $pDbMgr;

    $sSql = "SELECT A.id, A.quantity, A.order_id, A.product_id, A.weight, B.order_number, A.name, A.parent_id, A.deleted, B.order_status
            FROM orders_items A
            JOIN orders B
            ON B.id = A.order_id
            WHERE A.id = " . $iOrderItemId;
    return $pDbMgr->GetRow('profit24', $sSql);
  }// end of getOrderItemData() method

  /**
   * Metoda pobiera dane na temat listy zamówionych pozyji
   *
   * @global \DatabaseManager $pDbMgr
   * @param int $iOILId
   *
   * @param array $aCols
   * @return array
   */
  public function getOrderedItems($iOILId, $aCols) {
    global $pDbMgr;

    $sSql = 'SELECT OI.source ,' . implode(',', $aCols) . '
             FROM orders_send_history_items as OSHI
             JOIN orders_items AS OI ON OSHI.item_id = OI.id
             WHERE send_history_id = ' . $iOILId;

      $res = $pDbMgr->GetAll('profit24', $sSql);
    return ArrayHelper::toKeyValues('item_id', $res);
  }// end of getOrderedItems() method
}
