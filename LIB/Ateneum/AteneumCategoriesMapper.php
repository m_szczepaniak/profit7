<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Ateneum;

class AteneumCategoriesMapper
{
    /**
     * @var string
     */
    private $csvFilePath = 'katalog_new.csv';

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct()
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;
    }

    public function synchronizeCategories()
    {
        $arr = $this->csvs(file_get_contents($this->csvFilePath));

        $new = array();

        $tree = $this->createTree($new, array($arr[0]));

        $d = '';
    }

    function createTree(&$list, $parent){
        $tree = array();
        foreach ($parent as $k=>$l){
            if(isset($list[$l['id']])){
                $l['children'] = $this->createTree($list, $list[$l['id']]);
            }
            $tree[] = $l;
        }
        return $tree;
    }

    private function csvs($string, $skip_rows = 0, $separatorChar = ',', $enclosureChar = '"', $newlineChar = "\n")
    {
        // @author: Klemen Nagode
        // @source: http://stackoverflow.com/questions/1269562/how-to-create-an-array-from-a-csv-file-using-php-and-the-fgetcsv-function
        $array = array();
        $size = strlen($string);
        $columnIndex = 0;
        $rowIndex = 0;
        $fieldValue="";
        $isEnclosured = false;
        for($i=0; $i<$size;$i++) {

            $char = $string{$i};
            $addChar = "";

            if($isEnclosured) {
                if($char==$enclosureChar) {

                    if($i+1<$size && $string{$i+1}==$enclosureChar){
                        // escaped char
                        $addChar=$char;
                        $i++; // dont check next char
                    }else{
                        $isEnclosured = false;
                    }
                }else {
                    $addChar=$char;
                }
            }else {
                if($char==$enclosureChar) {
                    $isEnclosured = true;
                }else {

                    if($char==$separatorChar) {

                        $array[$rowIndex][$columnIndex] = $fieldValue;
                        $fieldValue="";

                        $columnIndex++;
                    }elseif($char==$newlineChar) {
                        echo $char;
                        $array[$rowIndex][$columnIndex] = $fieldValue;
                        $fieldValue="";
                        $columnIndex=0;
                        $rowIndex++;
                    }else {
                        $addChar=$char;
                    }
                }
            }
            if($addChar!=""){
                $fieldValue.=$addChar;

            }
        }

        if($fieldValue) { // save last field
            $array[$rowIndex][$columnIndex] = $fieldValue;
        }


        /**
         * Skip rows.
         * Returning empty array if being told to skip all rows in the array.
         */
        if ($skip_rows > 0) {
            if (count($array) == $skip_rows)
                $array = array();
            elseif (count($array) > $skip_rows)
                $array = array_slice($array, $skip_rows);

        }

        $filtered = [];

        foreach($array as $elements) {

            $filteredArray = array_filter($elements);
            $filtered[] = $filteredArray;
        }

        return $filtered;
    }
}