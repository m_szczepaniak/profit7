<?php
namespace table;
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */

/**
 * Klasa odpowiedzialna za GRID na tabeli key_value
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class keyValue {

  private $sWebsite;

  /**
   *
   * @var string
   */
  private $sTable;
  
  /**
   *
   * @var \DatabaseManager
   */
  private $pDbMgr;
  
  /**
   *
   * @var database
   */
  private $pDB;
  
  CONST TABLE_NAME_KEY_VALUE = 'key_value';
  CONST POST_KEY_VALUE_FIELD = 'key_value';
  
  /**
   * 
   * @global \DatabaseManager $pDbMgr
   * @global \Query $pDB
   * @param string $sTable
   * @param string $sWebsite
   */
  public function __construct($sTable, $sWebsite = 'profit24') {
    global $pDbMgr, $pDB;
    $this->sTable = $sTable;
    $this->pDbMgr = $pDbMgr;
    $this->pDB = $pDB;
    
    $this->sWebsite = $sWebsite;
  }
  
  public function setpDB($pDB) {
    $this->pDB = $pDB;
  }
  
  public function setpDbMgr($pDbMgr) {
    $this->pDbMgr = $pDbMgr;
  }

  /**
   * Pobieramy z posta dane na temat danych z tablicy klucz wartość
   * 
   * @param int $iIdDest
   * @param array $aPost
   * @return boolean
   */
  public function insertUpdateByPost($iIdDest, $aPost) {
    
    if (isset($aPost[self::POST_KEY_VALUE_FIELD]) && is_array($aPost[self::POST_KEY_VALUE_FIELD])) {
      $aKeysValues = $aPost[self::POST_KEY_VALUE_FIELD];
      foreach ($aKeysValues as $sKey => $sValue) {
        if ($this->insertUpdate($iIdDest, $sKey, $sValue) === false){
          return false;
        }
      }
    }
    return true;
  }
  
  
  /**
   * Pobieramy z posta dane na temat danych z tablicy klucz wartość
   * 
   * @param int $iIdDest
   * @param array $aPost
   * @return boolean
   */
  public function insertByPost($iIdDest, $aPost) {
    
    if (isset($aPost[self::POST_KEY_VALUE_FIELD]) && is_array($aPost[self::POST_KEY_VALUE_FIELD])) {
      $aKeysValues = $aPost[self::POST_KEY_VALUE_FIELD];
      foreach ($aKeysValues as $sKey => $sValue) {
        if ($this->insert($iIdDest, $sKey, $sValue) === false){
          return false;
        }
      }
    }
    return true;
  }
  
  /**
   * 
   * @param int $iIdDest
   * @param string $sKey
   * @param string $sValue
   * @return int|false
   */
  public function insert($iIdDest, $sKey, $sValue) {
    $aValues = array(
        'table_name' => $this->sTable,
        'id_dest' => $iIdDest,
        '`key`' => $sKey,
        '`value`' => $sValue
    );
    return $this->pDbMgr->Insert($this->sWebsite, self::TABLE_NAME_KEY_VALUE, $aValues);
  }
  
  /**
   * 
   * @param int $iIdDest
   * @param string $sKey
   * @param string $sValue
   * @return bool
   */
  public function update($iIdDest, $sKey, $sValue) {
    
    $aValues = array(
        '`value`' => $sValue
    );
    
    $sWhere = ' id_dest = ' . $iIdDest.
              ' AND `key` = ' . $this->pDB->quoteSmart($sKey).
              ' AND table_name = '. $this->pDB->quoteSmart($this->sTable).
              ' LIMIT 1';
    return $this->pDbMgr->Update($this->sWebsite, self::TABLE_NAME_KEY_VALUE, $aValues, $sWhere);
  }
  
  /**
   * Metoda aktualizuje dany rekord jeśli istnieje, jesli nie to go dodaje
   * 
   * @param int $iIdDest
   * @param string $sKey
   * @param string $sValue
   * @return bool
   */
  public function insertUpdate($iIdDest, $sKey, $sValue) {
    
    $sValueAct = $this->getByIdDestKey($iIdDest, $sKey);
    if (is_null($sValueAct)) {
      return $this->insert($iIdDest, $sKey, $sValue);
    } elseif ($sValue !== $sValueAct) {
      return $this->update($iIdDest, $sKey, $sValue);
    }
    return true;
  }
 
  /**
   * 
   * @param int $iId
   * @return bool
   */
  public function deleteById($iId) {
    
    $sSql = 'DELETE FROM '.self::TABLE_NAME_KEY_VALUE.
            ' WHERE id = '.$iId.
            ' AND table_name = '. $this->pDB->quoteSmart($this->sTable).
            ' LIMIT 1';
    return $this->pDbMgr->Query($this->sWebsite, $sSql);
  }
  
  /**
   * 
   * @param int $iIdDest
   * @param string $sKey
   * @return bool
   */
  public function deleteByIdDestKey($iIdDest, $sKey) {
    
    $sSql = 'DELETE FROM '.self::TABLE_NAME_KEY_VALUE.
            ' WHERE id_dest = ' . $iIdDest.
            ' AND `key` = ' . $this->pDB->quoteSmart($sKey).
            ' AND table_name = '. $this->pDB->quoteSmart($this->sTable).
            ' LIMIT 1';
    return $this->pDbMgr->Query($this->sWebsite, $sSql);
  }

  /**
   * 
   * @param int $iIdDest
   * @param string $sKey
   * @return string
   */
  public function getByIdDestKey($iIdDest, $sKey) {
    
    $sSql = 'SELECT `value` FROM ' . self::TABLE_NAME_KEY_VALUE.
            ' WHERE id_dest = ' . $iIdDest.
            ' AND `key` = ' . $this->pDB->quoteSmart($sKey).
            ' AND table_name = ' . $this->pDB->quoteSmart($this->sTable).
            ' LIMIT 1';
    return $this->pDbMgr->GetOne($this->sWebsite, $sSql);
  }
  
  /**
   * 
   * @param int $iIdDest
   * @return value
   */
  public function getAllByIdDest($iIdDest) {
    
    $sSql = 'SELECT `key`, `value` FROM ' . self::TABLE_NAME_KEY_VALUE.
            ' WHERE id_dest = ' . $iIdDest.
            ' AND table_name = ' . $this->pDB->quoteSmart($this->sTable);
    return $this->pDbMgr->GetAssoc($this->sWebsite, $sSql);
  }
  
  
  /**
   * 
   * @param string $sKey
   * @param string $sValue
   * @return array[int]
   */
  public function getByKeyValueIdDest($sKey, $sValue) {
    
    $sSql = 'SELECT id_dest FROM ' . self::TABLE_NAME_KEY_VALUE.
            ' WHERE `value` = ' . $this->pDB->quoteSmart($sValue).
            ' AND `key` = ' . $this->pDB->quoteSmart($sKey).
            ' AND table_name = ' . $this->pDB->quoteSmart($this->sTable);
    return $this->pDbMgr->GetCol($this->sWebsite, $sSql);
  }
  
  /**
   * Metoda miesza dwie table, nawet jeśli jedna jest pusta lub nullem
   * 
   * @param array $aFirst
   * @param array $aSecound
   * @return array
   */
  public function mergeArraysEvenNull($aFirst, $aSecound) {
    if (empty($aFirst) || !is_array($aFirst)) {
      $aFirst = array();
    }
    if (empty($aSecound) || !is_array($aFirst)) {
      $aSecound = array();
    }
    return array_merge($aFirst, $aSecound);
  }// end of mergeArraysEvenNull() method
}
