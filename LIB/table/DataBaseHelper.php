<?php

namespace table;

class DataBaseHelper
{
    public static function arrayToMultiInsert($table, $data, array $excludeColumns = [])
    {
        $valArr = [];

        foreach ($data as $k => &$v) {

            foreach ($excludeColumns as $excludeColumn) {
                unset($v[$excludeColumn]);
            }

            $valArr[] = '('.join(',', $v).')';
        }

        $fields = join(',', array_keys(end($data)));

        $values = implode(",", $valArr).';';

        $insert = "INSERT INTO `%s` (%s) VALUES %s";
        $sql = sprintf($insert, $table, $fields, $values);

        return $sql;
    }
}