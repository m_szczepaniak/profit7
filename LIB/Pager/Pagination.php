<?php

namespace Pager;

class Pagination
{
    /**
     * @var float
     */
    private $allPages;

    /**
     * @var array
     */
    private $rows;

    /**
     * @var int
     */
    private $rowsAmount;

    /**
     * @var int
     */
    private $nextOffset;

    /**
     * Pagination constructor.
     * @param array $rows
     * @param $perPage
     * @param $currentPage
     */
    public function __construct(array $rows, $perPage, $currentPage)
    {
        $this->rowsAmount = count($rows);
        $this->allPages = ceil($this->rowsAmount / $perPage);
        $this->rows = $rows;
        $this->offset = ($currentPage - 1) * $perPage + 1;
        $this->nextOffset = $this->offset + $perPage;
    }

    /**
     * @return array
     */
    public function getCurrentRows()
    {
        $rows = [];

        $i = 1;
        foreach($this->rows as $row) {

            if ($i >= $this->offset && $i < $this->nextOffset) {

                $rows[] = $row;
            }

            if ($i > $this->nextOffset) {

                break;
            }

            $i++;
        }

        return $rows;
    }

    /**
     * @return int
     */
    public function getRowsAmount()
    {
        return $this->rowsAmount;
    }
}
