<?php

namespace View;

use \Smarty;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 23.02.18
 * Time: 15:03
 */

class touchKeyboard {

    /**
     * @var \Smarty
     */
    private $pSmarty;

    public function __construct(Smarty $pSmarty)
    {
        $this->pSmarty = $pSmarty;
    }

    /**
     * @return mixed|string|void
     */
    public function getKeyboardNumeric() {

        $aChars = $this->charsArrayLine();
        $this->pSmarty->assign_by_ref('aChars', $aChars);
        $sHtml = $this->pSmarty->fetch('touchKeyboardUniwersal.tpl');
        $this->pSmarty->clear_assign('aChars', $aChars);
        return $sHtml;
    }


    /**
     * Metoda pobiera tablicę zanków
     *
     * @return array
     */
    private function charsArrayLine() {

        $aChars =
            array(
//          'chars' => array(
//          array('Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'),
//          array('A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'),
//          array('', 'Z', 'X', 'C', 'V', 'B', 'N', 'M', '&#8629;'),
//          ),
            'int' => array(
                array(7, 8, 9),
                array(4, 5, 6),
                array(1, 2, 3),
                array(0, 'X', '&#8629;')
            )
        );
        return $aChars;
    }// end of charsArrayLine() method
}