<?php

namespace Ceneo;

use Ceneo\Api\ApiManager;
use Ceneo\Exception\ApiException;
use Ceneo\Exception\LowDiscountException;
use Ceneo\PriceModifier\DecreasePrice;
use Ceneo\PriceModifier\IncreasePrice;
use Common;
use DatabaseManager;

class PriceUpdater
{
    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**Nceneo
     * @var array
     */
    private $aConfig;

    /**
     * @var Products
     */
    private $products;

    /**
     * @var ProductsBuffer
     */
    private $productsBuffer;

    /**
     * @var ApiManager
     */
    private $apiManager;

    /**
     * @var array
     */
    private $ceneoWebsitesUnactiveAndActie;


    public function __construct()
    {
        global $aConfig, $pDbMgr;

        $this->products = new Products();
        $this->ceneoDataProvider = new CeneoDataProvider();
        $this->productsBuffer = new ProductsBuffer();
        $this->aConfig = $aConfig;
        $this->pDbMgr = $pDbMgr;
        $this->apiManager = new ApiManager();
        $this->ceneoWebsitesUnactiveAndActie = $this->ceneoDataProvider->getActiveAndUnactiveCeneoWebsites();
    }

    public function updatePrices($websiteId)
    {
        $tarrifs = $this->ceneoDataProvider->getTarrifsToUpdateByWebsiteId($websiteId);

        if (empty($tarrifs)) {
            return false;
        }

        $websites = $this->ceneoDataProvider->getCeneoWebsites();
        $websiteDbCode = $websites[$websiteId]['code'];

        $xml = new Xml($websiteDbCode);
        $xmlProductIndents = $xml->prepareProductsXmlRows($tarrifs);
        $preparedProducts = $this->prepareRawProductIndents($xmlProductIndents);
        $rawData = $preparedProducts['raw'];
        $xmlData = $preparedProducts['xml'];

        $fulPackageData = $this->createFullPackage($xmlData, $websiteId);
        $fulPackages = $fulPackageData['xmlPackages'];
        $addedIds = $fulPackageData['addedIds'];

        $sentTarrifs = [];

        foreach($fulPackages as $singlePackage) {
            // TODO zrobic try cache z wyjatkeim
            try {
                $parsedXml = $xml->getFullXml($singlePackage);

                $res = $this->apiManager->sendXmlData($parsedXml);
                $sentPacketTarrifs = $this->saveTarrifs($singlePackage, $rawData, $tarrifs, $websiteId);

                foreach($sentPacketTarrifs as $sTarrif) {

                    $sentTarrifs[] = $sTarrif;
                }
            } catch(ApiException $e) {

                HelperFunctions::sendApiErrorMail('Ceneo - Problem z poalczeniem API - update', $e->getMessage());
                continue;
            }
        }

        HelperFunctions::sendUpdateRaportMail($sentTarrifs);
        $d = '';
    }

    private function saveTarrifs(array $singlePackage, array $rawData, array $originalTarrifs, $websiteId)
    {
        $newTarrifs = [];

        foreach($singlePackage as $package) {

            $product = $originalTarrifs[$package['product_id']];
            $product['id'] = $rawData[$package['product_id']]['id'];
            $newTarrifs[$package['product_id']] = $product;
        }

        $this->ceneoDataProvider->saveTarrifs($newTarrifs, $websiteId, true);
        $this->ceneoDataProvider->removeTarrifsFromTarrifsBuffor($newTarrifs, $websiteId);
        return $newTarrifs;
    }

    /**
     * Tworzy paczke pakeitow do wyslania
     *
     * @param array $xmlProductIndents
     * @return array
     */
    private function createFullPackage(array $xmlProductIndents, $websiteId)
    {
        $xmlPackages = [];
        $fullAddedProducts = [];
        $packagesLimit = CeneoDataProvider::getPackagesLimit($websiteId);

        $i = 0;
        while(true) {

            // zabezpieczenie jakby cos poszlo nie tak
            if ($i == 41 || true == empty($xmlProductIndents) || count($xmlPackages) == $packagesLimit) {
                break;
            }

            $singlePackage = $this->createSinglePackage($xmlProductIndents);

            $xmlPackages[] = $singlePackage['packet'];

            foreach($singlePackage['addedIds'] as $id) {

                $fullAddedProducts[] = $id;
            }

            $i++;
        }

        return [
            'xmlPackages' => $xmlPackages,
            'addedIds' => $fullAddedProducts
        ];
    }

    /**
     * Tworzy pojedynczy pakiet - max 100 elementow
     *
     * @param array $xmlProductIndents
     * @return array
     */
    private function createSinglePackage(array &$xmlProductIndents)
    {
        $packet = [];
        $packetLimit = CeneoDataProvider::SINGLE_PACKAGE_LIMIT;
        $addedIds = [];

        foreach($xmlProductIndents as $productId => $xmlIndents) {

            $countPacket = count($packet);
            $countProductPacket = count($xmlIndents);

            if ($countPacket == $packetLimit) {
                break;
            }

            // jezeli mozna do pakeitu dodac
            if (($countPacket + $countProductPacket) <= $packetLimit) {

                // dodajemy pojedyncze wpisy xml do pakietu
                foreach($xmlIndents as $singleIndent) {
                    $packet[] = [
                        'xmlData' => $singleIndent,
                        'product_id' => $productId
                    ];
                }

                $addedIds[] = $productId;
            }
        }

        foreach($addedIds as $addedId) {

            unset($xmlProductIndents[$addedId]);
        }

        return [
            'packet' => $packet,
            'addedIds' => $addedIds
        ];
    }

    private function prepareRawProductIndents(array $xmlProductIndents)
    {
        $newPRoductsIndents = [];
        $raw = [];

        foreach($xmlProductIndents as $productId => $productIndent) {

            foreach($productIndent['xml'] as $xmled) {

                $newPRoductsIndents[$productId][] = $xmled;
            }

            foreach($productIndent['raw'] as $rawed) {

                $raw[$productId] = $rawed;
            }
        }

        return [
            'xml' => $newPRoductsIndents,
            'raw' => $raw
        ];
    }
}
