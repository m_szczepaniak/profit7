<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Ceneo\XmlGenerator;


use Common;

class MestroXmlGenerator
{
    /**
     * @var string
     */
    private $websiteConnCode;

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    private $host = 'https://mestro.pl';

    public function __construct($websiteConnCode)
    {
        global $pDbMgr;
        $this->websiteConnCode = $websiteConnCode;
        $this->pDbMgr = $pDbMgr;
    }

    public function getShipmentTimes(){

        $sSql = "SELECT shipment_time as id, text FROM products_comparison_sites";
        return $this->pDbMgr->GetAssoc($this->websiteConnCode, $sSql, false, array(), DB_FETCHMODE_ASSOC, false);
    }

    public function getProductCat($iId) {
        global $aConfig;
        $sSql = "SELECT A.id, A.name
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					 AND A.parent_id IS NOT NULL
					ORDER BY A.priority
					LIMIT 1";

        $res = $this->pDbMgr->GetRow($this->websiteConnCode, $sSql);;

        if (!empty($res)) {
            return $res;
        }

        $sSql2 = "SELECT A.id, A.name
						FROM ".$aConfig['tabls']['prefix']."menus_items A
						JOIN ".$aConfig['tabls']['prefix']."products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					ORDER BY A.priority
					LIMIT 1";

        return $this->pDbMgr->GetRow($this->websiteConnCode, $sSql2);
    }

    /**
     * Funkcja pobiera czas realizacji i konwertuje na wartości CENEO
     *
     * @param type $iShipment
     * @return int
     */
    public function getShipmentTimeCeneo($iShipment, $productStatus){
        /*
         *
         * wartość z Ksiegarni :
            ['shipment_0'] = "Od ręki";
            ['shipment_1'] = "1 - 2 dni";
            ['shipment_2'] = "7 - 9 dni";
            ['shipment_3'] = "tylko na zamówienie";
         *
         *
         * wartości z Ceneo:
          1 – dostępny, sklep posiada produkt,
          3 – sklep będzie posiadał produkt do 3 dni,
          7 – sklep będzie posiadał produkt w ciągu tygodnia,
          14 – produkt dostępny nie wcześniej niż za tydzień,
          99 lub brak wartości – link prowadzący do informacji w sklepie. Opcjonalnie
         */

        if ($productStatus == '3') {
            return 110;
        }
        switch(intval($iShipment)){
            case 0:
            case 1:
                return 1;
                break;
            case 2:
            case 3:
                return 3;
                break;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                return 7;
                break;
            default:
                return 99;
                break;
        }
    }

    /**
     * Czyści błędne kody ascii z opisu
     * @param string $sStr - string wejściowy
     * @return string - przeczyszczony
     */
    public function clearAsciiCrap($sStr) {
        return (string)str_replace(array("\x01","\x02","\x03","\x04","\x05","\x06","\x07","\x08","\x09","\x0A","\x0B","\x0C","\x0D","\x0E","\x0F","\x10","\x11","\x12","\x13","\x14","\x15","\x16","\x17","\x18","\x19","\x1A","\x1B","\x1C","\x1D","\x1E","\x1F"), '', $sStr);
    }

    public function getProductAuthors($iId) {
        global $aConfig;
        $sAuthors='';
        // wybiera grupy kategorii dla tego produktu
        $sSql = "SELECT CONCAT(B.surname,' ',B.name) AS name
  				 FROM ".$aConfig['tabls']['prefix']."products_to_authors A
  				 JOIN ".$aConfig['tabls']['prefix']."products_authors B
  				 ON A.author_id=B.id
  				 WHERE A.product_id =".$iId."
  				ORDER BY A.order_by";
        $aItems =& $this->pDbMgr->GetCol($this->websiteConnCode, $sSql);
        foreach ($aItems as $sItem) {
            $sAuthors .= $sItem.', ';
        }
        if(!empty($sAuthors)) {
            $sAuthors = substr($sAuthors,0,-2);
        }
        return $sAuthors;
    }

    function getProductImg($iId) {
        global $aConfig;

        $sSql = "SELECT directory, photo, mime
					 FROM ".$aConfig['tabls']['prefix']."products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
        $aImg =& $this->pDbMgr->GetRow($this->websiteConnCode, $sSql);

        if (!empty($aImg)) {
            $sDir = $aConfig['common']['photo_dir'];
            if (!empty($aImg['directory'])) {
                $sDir .= '/'.$aImg['directory'];
            }

            if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/__b_'.$aImg['photo'])) {
                return $sDir.'/__b_'.$aImg['photo'];
            }
            else {
                if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])) {
                    return $sDir.'/'.$aImg['photo'];
                }
            }
        }
        return '';
    } // end of getItemImage() function

    function getCategoryParent($iId) {
        global $aConfig;

        $sSql="SELECT B.name
				FROM ".$aConfig['tabls']['prefix']."menus_items A
				JOIN ".$aConfig['tabls']['prefix']."menus_items B
				ON B.id=A.parent_id
				WHERE A.id = ".$iId;
        $aItem = $this->pDbMgr->GetRow($this->websiteConnCode, $sSql);
        return (!empty($aItem['name'])?$aItem['name'].'/':'');
    }

    public function generateElement(&$aItem, $sIsbnEan, $aCat, $aShipmentTimes, $bFirst, $editable = false) {
        global $aConfig;
        $sXML = '';
        $sElement = '';
        $sCatFullPath = htmlspecialchars($this->getCategoryParent($aCat['id']).str_replace('/ ','',$aCat['name']), ENT_QUOTES,'UTF-8',false);

        $fTarrifPrice = $aItem['price_brutto'];

        $sAuthors = $this->getProductAuthors($aItem['id']);

        $sImgSrc = $this->getProductImg($aItem['id']);
        if ($sImgSrc!='') {
            $sImg = $this->host.'/'.$sImgSrc;
        }

        $sPlainName = trimString(htmlspecialchars($aItem['plain_name'],ENT_QUOTES,'UTF-8',false).$aShipmentTimes[$aItem['shipment_time']], 150);

        // TODO SPRAWDZIĆ set='0'
        $sXML .= '<o id="'.$aItem['id'] . ($bFirst === false ? '_'.$sIsbnEan : '' ).'" '.
            'url="'.$this->host.createProductLink($aItem['id'], $aItem['plain_name'].'-'.$sIsbnEan).'?cdn=1'.'" '.
            'price="'.$fTarrifPrice.'" '.
            'avail="'.$this->getShipmentTimeCeneo($aItem['shipment_time'], $aItem['prod_status']).'" '.
            'set="'.$aItem['packet'].'" '.
            ($aItem['weight']!=''? ' weight="'.$aItem['weight'].'"' : '') .'>'."\n";

        $sXML .= 	'	<cat>'.
            '<![CDATA['.htmlspecialchars($sCatFullPath,ENT_QUOTES,'UTF-8',false).']]>'.
            '</cat>'."\n";

        $sXML .= 	'	<name>'.
            '<![CDATA['.$sPlainName.']]>'.
            '</name>'."\n";

        if (!empty($sImg)) {
            $sXML .=	'	<imgs>'.
                '<main url="'.$sImg.'"/>'.
                '</imgs>'."\n";
        }

        $sXML	.=	'	<desc>'.
            '<![CDATA['.htmlspecialchars(stripslashes(strip_tags($this->clearAsciiCrap(($aItem['description_on_create'])))),ENT_QUOTES,'UTF-8',false).']]>'.
            '</desc>'."\n";

        $sXML .=	'	<attrs>'."\n".
            (!empty($sAuthors)?
                '<a name="Autor">'."\n". //<!--name: tekst (długość 512) wymagany-->
                '<![CDATA['.trimString(htmlspecialchars($sAuthors,ENT_QUOTES,'UTF-8',false), 512).']]>'."\n".
                '</a>'."\n"
                :'').
            '<a name="' . (strlen($sIsbnEan) == 13 && !preg_match('/^978.+/', $sIsbnEan) ? 'EAN' : 'ISBN') . '">'."\n".
            '<![CDATA['.$sIsbnEan.']]>'."\n".
            '</a>'."\n".
            (!empty($aItem['pages'])?
                '<a name="Ilosc_stron">'."\n".
                '<![CDATA['.$aItem['pages'].']]>'."\n".
                '</a>'."\n"
                :'').
            (!empty($aItem['publisher_name'])?
                '<a name="Wydawnictwo">'."\n".
                '<![CDATA['.htmlspecialchars($aItem['publisher_name'],ENT_QUOTES,'UTF-8',false).']]>'."\n".
                '</a>'."\n"
                :'').
            (!empty($aItem['binding_name'])?
                '<a name="Oprawa">'."\n".
                '<![CDATA['.$aItem['binding_name'].']]>'."\n".
                '</a>'."\n"
                : '').
            (!empty($aItem['dimension_name'])?
                '<a name="Format">'."\n".
                '<![CDATA['.$aItem['dimension_name'].']]>'."\n".
                '</a>'."\n"
                :'').
            '	</attrs>'."\n";
        $sXML .= '</o>'."\n";
        return $sXML;
    }

    /**
     * Metoda pobiera unikatowe indentyfikatory
     *
     * @param array $aItemData
     * @return array
     */
    public function getUniqueIdents($aItemData) {

        if ($aItemData['isbn_plain'] != '') {
            $aIdents[] = $aItemData['isbn_plain'];
        }
        if ($aItemData['isbn_10'] != '') {
            $aIdents[] = $aItemData['isbn_10'];
        }
        if ($aItemData['isbn_13'] != '') {
            $aIdents[] = $aItemData['isbn_13'];
        }
        if ($aItemData['ean_13'] != '') {
            $aIdents[] = $aItemData['ean_13'];
        }
        $aIdents = array_unique($aIdents);
        return $aIdents;
    }
}