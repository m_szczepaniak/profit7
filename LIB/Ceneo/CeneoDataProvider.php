<?php

namespace Ceneo;

use Ceneo\Api\MestroApi;
use Ceneo\Api\NieprzeczytaneApi;
use Ceneo\XmlGenerator\MestroXmlGenerator;
use Ceneo\XmlGenerator\NieprzeczytaneXmlGenerator;
use DatabaseManager;
use LIB\Helpers\ArrayHelper;

class CeneoDataProvider
{
    const PRODUCT_COMPARE_LIMIT = 40000;

    const FULL_PACKAGE_LIMIT = 4000;

    const SINGLE_PACKAGE_LIMIT = 100;
    const PACKAGES_LIMIT = 10;

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * @var array
     */
    private $aConfig;

    public function __construct()
    {
        global $aConfig, $pDbMgr;

        $this->aConfig = $aConfig;
        $this->pDbMgr = $pDbMgr;
    }

    /**
     * @param $website
     *
     * @return int
     */
    public static function getPackagesLimit($websiteId) {

        if ($websiteId == 3) {
            return 40;
        }
        return 10;
    }

    public static function setCeneoConfigByWebsiteId($websiteId)
    {
        if (!is_numeric($websiteId)) {
            throw new \Exception('Invalid websiteId');
        }

        global $aConfig;

        $data = [
            'api_class' => null,
            'generator_class' => null,
            'name' => null
        ];

        if ($websiteId == 3) {
            $data['api_class'] = '\Ceneo\Api\NieprzeczytaneApi';
            $data['generator_class'] = '\Ceneo\XmlGenerator\NieprzeczytaneXmlGenerator';
            $data['name'] = 'Nieprzeczytane';
        }

        if ($websiteId == 4) {
            $data['api_class'] = '\Ceneo\Api\MestroApi';
            $data['generator_class'] = '\Ceneo\XmlGenerator\MestroXmlGenerator';
            $data['name'] = 'Mestro';
        }

        if (null == $data['api_class'] || null == $data['generator_class']) {
            throw new \Exception("Config is not set properly");
        }

        $aConfig['ceneo_config'] = $data;
    }

    /**
     * Pobieramy sklepy ktore walcza na ceneo
     * @return array
     * @throws \Exception
     */
    public function getCeneoWebsites()
    {
        $result = $this->pDbMgr->GetAll('profit24', "SELECT * FROM websites WHERE ceneo is not null and ceneo != '' AND ceneo_active = 1");

        if (false === $result) {

            throw new \Exception('Błąd w pobieraniu listy zamawiarek ceneo');
        }

        if (!empty($result)) {
            $result = ArrayHelper::toKeyValues('id', $result);
        }

        return $result;
    }

    /**
     * pobieramy sklepy ktore maja wpisana nazwe na ceneo - w celu wykluczenia z wynikow
     *
     * @return array
     * @throws \Exception
     */
    public function getActiveAndUnactiveCeneoWebsites()
    {
        $result = $this->pDbMgr->GetAssoc('profit24', "SELECT id, ceneo FROM websites WHERE ceneo is not null and ceneo != ''");

        if (false === $result) {

            throw new \Exception('Błąd w pobieraniu listy zamawiarek ceneo');
        }

        return $result;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getWebsiteCeneoCategories()
    {
        $result = $this->pDbMgr->GetAll('profit24', "SELECT * FROM website_ceneo_categories");

        if (false === $result) {

            throw new \Exception('Błąd w pobieraniu ceneo website categories');
        }

        if (!empty($result)) {
            $result = ArrayHelper::toKeyValues('category_id', $result);
        }

        return $result;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getWebsiteCeneoSettings()
    {
        $result = $this->pDbMgr->GetAll('profit24', "SELECT WCS.*, W.code FROM website_ceneo_settings as WCS JOIN websites as W ON WCS.website_id = W.id");

        if (false === $result) {

            throw new \Exception('Błąd w pobieraniu listy zamawiarek ceneo');
        }

        if (!empty($result)) {
            $result = ArrayHelper::toKeyValues('website_id', $result);
        }

        return $result;
    }

    public function saveTarrifsToBuffor(array $tarrifs)
    {

        $sql = "INSERT INTO tarrifs_buffor (product_id, shop_product_id, website_id, discount, discount_value, price_netto, price_brutto, minimum, main_shop_id, vip) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                ON DUPLICATE KEY
                UPDATE product_id = %s, shop_product_id = %s, website_id = %s, discount = %s, discount_value = %s, price_netto = %s, price_brutto = %s, minimum = %s, main_shop_id = %s, vip = %s
                ";

        foreach($tarrifs as $tarif) {

            foreach($tarif as $tarrifWebsite) {
                $productId = $tarrifWebsite['product_id'];
                $shopProductId = $tarrifWebsite['shop_product_id'];
                $websiteId = $tarrifWebsite['website_id'];
                $discount = $tarrifWebsite['discount'];
                $discountValue = $tarrifWebsite['discount_value'];
                $priceNetto = $tarrifWebsite['price_netto'];
                $priceBrutto = $tarrifWebsite['price_brutto'];
                $minimum = $tarrifWebsite['minimum'];
                $mainShopId = $tarrifWebsite['main_shop_id'];
                $vip = $tarrifWebsite['vip'];

                $sql2 = sprintf($sql,
                    $productId, $shopProductId, $websiteId, $discount, $discountValue, $priceNetto, $priceBrutto, $minimum, $mainShopId, $vip,
                    $productId, $shopProductId, $websiteId, $discount, $discountValue, $priceNetto, $priceBrutto, $minimum, $mainShopId, $vip
                );

                $this->pDbMgr->Query('profit24', $sql2);
            }
        }
    }

    /**
     * Pobiera cenniki od najstarszych dla danej ksiegarni
     *
     * @param $websiteId
     * @return array
     * @throws \Exception
     */
    public function getTarrifsToUpdateByWebsiteId($websiteId)
    {
        $sql = "SELECT * FROM tarrifs_buffor WHERE website_id = %s ORDER BY vip DESC, main_shop_id = %s DESC, updatet_at LIMIT %s";

        $result = $this->pDbMgr->GetAll('profit24', sprintf($sql, $websiteId, $websiteId, self::FULL_PACKAGE_LIMIT));

        if (false === $result) {

            throw new \Exception('Błąd w pobieraniu cennikow do wyslania natychmiast');
        }

        if (!empty($result)) {
            $result = ArrayHelper::toKeyValues('product_id', $result);
        }

        return $result;
    }

    /**
     * id = id produktu w danym sklepie
     *
     * @param array $tarrifs
     * @param $websiteId
     * @param bool $startDateNow
     * @throws \Exception
     */
    public function saveTarrifs(array $tarrifs, $websiteId, $startDateNow = false)
    {
        $website = $this->getCeneoWebsites();
        $website = $website[$websiteId];

        $productIds = ArrayHelper::arrayColumn($tarrifs, 'id');
        $imploded = implode(',', $productIds);
        $existsTarrifsBuffor = $this->pDbMgr->GetAssoc($website['code'], sprintf("SELECT product_id, price_brutto FROM products_tarrifs WHERE product_id IN(%s) AND `type` = 1", $imploded));

        $dateStart = strtotime ( '-5 hours' , strtotime ( date('Y-m-d H:i:s') ) ) ;

        if (true == $startDateNow) {

            $dateStart = strtotime ( '+15 minutes' , strtotime ( date('Y-m-d H:i:s') ) ) ;
        }

        if ('generator' == $startDateNow) {

            $dateStart = strtotime ( '-2 hours' , strtotime ( date('Y-m-d H:i:s') ) ) ;
        }

        foreach($tarrifs as $product) {

            $values = [
                'product_id' => $product['id'],
                'discount' => $product['discount'],
                'discount_value' => $product['discount_value'],
                'price_netto' => $product['price_netto'],
                'price_brutto' => $product['price_brutto'],
                'start_date' => $dateStart,
                'type' => 1
            ];

            if (true === array_key_exists($product['id'], $existsTarrifsBuffor)) {
                $id = $product['id'];

                $res = $this->pDbMgr->Update($website['code'], 'products_tarrifs', $values, 'product_id = ' . $id.' AND type = 1 LIMIT 1');
                $r = '';

            } else {
                $res = $this->pDbMgr->Insert($website['code'], 'products_tarrifs', $values);
                $c = '';
            }
        }
    }

    /**
     * product_id => id z profitu
     *
     * @param array $tarrifs
     * @param $websiteId
     * @throws \Exception
     */
    public function removeTarrifsFromTarrifsBuffor(array $tarrifs, $websiteId)
    {
        $sql = "DELETE FROM tarrifs_buffor WHERE product_id = %s AND website_id = %s";

        foreach($tarrifs as $tarrif) {

            $res = $this->pDbMgr->Query('profit24', sprintf($sql, $tarrif['product_id'], $websiteId));
            $casdas = '';
        }
    }

    public function moveBufforToTarrifs($websiteId, $startDateNow = false)
    {
        $sql = "SELECT *, shop_product_id as id FROM tarrifs_buffor WHERE website_id = %s";

        $products = $this->pDbMgr->GetAll('profit24', sprintf($sql, $websiteId));

        $this->saveTarrifs($products, $websiteId, $startDateNow);
        $this->removeTarrifsFromTarrifsBuffor($products, $websiteId);
        $this->removeInvalidTarrifs($websiteId);
    }

    private function removeInvalidTarrifs($websiteId)
    {

        $website = $this->getCeneoWebsites();
        $website = $website[$websiteId];

        $dbCode = $website['code'];

        $column = 'profit24_id';

        if ($dbCode == 'profit24') {
            $column = 'id';
        }

        $this->deleteInvalidProductsFrequequently($websiteId);

        // normalne produkty
        $sql2 = "
        DELETE PT
        FROM products_tarrifs AS PT
        JOIN products AS P ON P.id = PT.product_id
        JOIN main_profit24.products_publishers AS PP ON PP.id = P.publisher_id
        WHERE PT.type = 1
        AND (P.published != '1' OR P.packet != '0' OR P.prod_status NOT IN ('1', '3'))
        AND P.%s NOT IN (SELECT CTI.product_id FROM main_profit24.ceneo_tabs_items AS CTI)
        AND PP.include_publishers_products != 1
        ";

        $res = $this->pDbMgr->Query($dbCode, sprintf($sql2, $column));
        var_dump($res);

        // ceneo in, out, vip
        $sql3 = "
        DELETE PT
        FROM products_tarrifs AS PT
        JOIN products AS P ON P.id = PT.product_id
        WHERE (P.packet != '0' OR P.published != '1' OR P.prod_status NOT IN ('1', '3'))
        AND PT.type = 1
        AND P.%s IN (SELECT CTI.product_id FROM main_profit24.ceneo_tabs_items AS CTI)
        ";

        // produkty publishera z warunkami ceneo_tabs
        $res = $this->pDbMgr->Query($dbCode, sprintf($sql3, $column));
        var_dump($res);

        $sql4 = "
        DELETE PT
        FROM products_tarrifs AS PT
        JOIN products AS P ON P.id = PT.product_id
        JOIN main_profit24.products_publishers AS PP ON PP.id = P.publisher_id
        WHERE (P.packet != '0' OR P.published != '1' OR P.prod_status NOT IN ('1', '3'))
        AND PT.type = 1
        AND PP.include_publishers_products = 1
        ";

        $res = $this->pDbMgr->Query($dbCode, $sql4);
        var_dump($res);
    }

    public function deleteInvalidProductsFrequequently($websiteId)
    {
        $website = $this->getCeneoWebsites();
        $website = $website[$websiteId];

        $dbCode = $website['code'];
        $dbId = $website['id'];

        $column = 'profit24_id';

        if ($dbCode == 'profit24') {
            $column = 'id';
        }

        $sql = "
        DELETE PT
        FROM products_tarrifs AS PT
        JOIN products AS P ON P.id = PT.product_id
        WHERE PT.`type` =1
        AND PT.price_brutto < P.wholesale_price
        ";

//        $res = $this->pDbMgr->Query($dbCode, $sql);
//        var_dump($res);

        $overheadSql = "SELECT minimal_overhead FROM website_ceneo_settings WHERE website_id = $dbId LIMIT 1;";

        $minimalverhead = $this->pDbMgr->GetOne('profit24', $overheadSql);

        if (!$minimalverhead) {
            return false;
        }

//        $sql2 = "
//        SELECT PT.id as tarrif_id, P.id, P.wholesale_price, PT.price_brutto, (ROUND( P.wholesale_price + ((P.wholesale_price * %s) / 100), 2 )) AS minimum_value,
//        (select WCC.website_id from main_profit24.products AS MP JOIN main_profit24.website_ceneo_categories AS WCC ON WCC.category_id = MP.main_category_id where MP.id = P.%s) as test
//        FROM products_tarrifs AS PT
//        JOIN products AS P ON PT.product_id = P.id
//        WHERE PT.type = 1
//        AND P.wholesale_price != P.price_brutto
//        HAVING PT.price_brutto < minimum_value AND test = %s
//        ";

        $sql2 = "
        SELECT PT.id as tarrif_id, P.id, P.wholesale_price, PT.price_brutto,

        (ROUND( P.wholesale_price + (P.wholesale_price * (

          IF((SELECT minimal_overhead FROM main_profit24.products_general_discount_common WHERE source = P.source AND website_id = %d) > 0, (SELECT minimal_overhead FROM main_profit24.products_general_discount_common WHERE source = P.source AND website_id = %d), %s)

        ) / 100), 2 )) AS minimum_value,

        (select WCC.website_id from main_profit24.products AS MP JOIN main_profit24.website_ceneo_categories AS WCC ON WCC.category_id = MP.main_category_id where MP.id = P.%s) as test
        FROM products_tarrifs AS PT
        JOIN products AS P ON PT.product_id = P.id
        WHERE PT.type = 1
        AND P.wholesale_price != P.price_brutto
        HAVING PT.price_brutto < minimum_value AND test = %s
        ";

        $result = $this->pDbMgr->GetAll($dbCode, sprintf($sql2, $websiteId, $websiteId, $minimalverhead, $column, $dbId));

	    var_dump($result);

        if ($result === false) {

            var_dump('nie udalo sie usunac cennikow');
        }

        var_dump(count($result));

        if (!empty($result) && false !== $result) {

            foreach($result as $rees) {

                $this->pDbMgr->Query($dbCode, sprintf("DELETE FROM products_tarrifs WHERE id = %s AND `type` = 1 LIMIT 1", $rees['tarrif_id']));
            }
        }
    }

    public function groupDeleteFrequently()
    {
        $websites = $this->getCeneoWebsites();

        foreach($websites as $website) {

            $this->deleteInvalidProductsFrequequently($website['id']);
        }
    }

    /**
     * Zwraca produkty nalezące do promocji|
     * @return array
     * @throws \Exception
     */
    public function getPreparedPromotionProducts()
    {
        $websitePromotionsProducts = [];
        $ceneoWebsites = $this->getCeneoWebsites();

        $promotionsSql = "SELECT id FROM products_promotions WHERE ignore_ceneo_tarrifs  = 1 AND start_date <= NOW() AND end_date >= NOW()";
        $promotionProductsSql = "SELECT P.%s, P.%s as pid, PT.discount, PT.price_brutto FROM products_tarrifs AS PT JOIN products AS P ON P.id = PT.product_id WHERE PT.promotion_id IN(%s)";

        foreach($ceneoWebsites as $websiteId => $website) {

            $promotions = $this->pDbMgr->GetCol($website['code'], $promotionsSql);

            if (empty($promotions)) {
                $websitePromotionsProducts[$websiteId] = [];
                continue;
            }

            $promotions = implode(',', $promotions);

            $field = 'id';

            if (1 != $websiteId) {
                $field = 'profit24_id';
            }

            $promotionsProducts = $this->pDbMgr->GetAll($website['code'], sprintf($promotionProductsSql, $field, $field, $promotions));
            $promotionsProducts = ArrayHelper::groupByArrayValue($promotionsProducts, 'pid');

            $finalPromotions = [];

            foreach($promotionsProducts as $productId => $promotionTarrifs) {

                usort($promotionTarrifs, function($a, $b) {

                    return $a['discount'] <= $b['discount'];
                });

                $lowestVal = reset($promotionTarrifs);

                $finalPromotions[$productId] = $lowestVal['price_brutto'];
            }

            $websitePromotionsProducts[$websiteId] = $finalPromotions;
        }

        return $websitePromotionsProducts;
    }

    public function removeIgnoredPromotionsProducts()
    {
        $ceneoWebsites = $this->getCeneoWebsites();

        $promotionProducts = $this->getPreparedPromotionProducts();

        foreach($promotionProducts as $websiteId => $websitePromotion) {

            $code = $ceneoWebsites[$websiteId]['code'];

            foreach($websitePromotion as $promotionProduct => $promotionLowestPrice) {

                $pid = $promotionProduct;

                if ($websiteId != 1) {

                    $pidSql = "SELECT id FROM products WHERE profit24_id = %s LIMIT 1";
                    $pid = $this->pDbMgr->GetOne($code, sprintf($pidSql, $promotionProduct));
                }

                if (!empty($pid)) {

                    $res = $this->pDbMgr->Query($code, sprintf("DELETE FROM products_tarrifs WHERE product_id = %s AND `type` = 1 LIMIT 1", $pid));
                }
            }
        }
    }
}
