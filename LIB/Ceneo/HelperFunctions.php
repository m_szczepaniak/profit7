<?php

namespace Ceneo;

class HelperFunctions
{
    public static function writeLine($text)
    {
        print_r($text."\r\n");
    }

    public static function sendMail($theme, $content)
    {
        global $aConfig;

        $now = date('Y-m-d');
        $path = $aConfig['common']['ceneo_xml_path'].$now.'.log';

        if (file_exists($path)) {
            $fh = fopen($path, 'a');
        } else {
            $fh = fopen($path, 'w');
        }

        $stringData = date('Y-m-d H:i:s'). ': ' . $theme.' --- '.$content.PHP_EOL;
        fwrite($fh, $stringData);
        fclose($fh);
    }

    public static function sendApiErrorMail($theme, $content)
    {
        $receivers = [
            'raporty@profit24.pl'
        ];

        foreach($receivers as $mailReciver) {
            \Common::sendMail('raporty@profit24.pl', 'raporty@profit24.pl', $mailReciver, $theme, $content, true);
        }
    }


    public static function sendRaportMail(array $tarrifs, array $websites)
    {
        $firstValue = reset($tarrifs);

        $content = '

        Produkty do bufora <br>
        <table>
        <th>Id profit</th>
        <th>ISBN</th>
        <th>Nazwa glownego sklepu</th>
        <th>Glowne minimum</th>
        <th>Typ zmiany</th>
        <th>profit24.pl</th>
        <th>nieprzeczytane.pl</th>
        <th>mestro.pl</th>
        ';

        $rows = '';

        foreach($tarrifs as $product) {

            $firstValue2 = reset($product);

            $productRow = '<tr>';
            $productRow .= '<td>'.$firstValue2['product_id'].'</td>';
            $productRow .= '<td>'.$firstValue2['isbn_plain'].'</td>';
            $productRow .= '<td>'.$firstValue2['main_shop_id'].'</td>';
            $productRow .= '<td>'.$firstValue2['minimum'].'</td>';
            $productRow .= '<td>'.$firstValue2['change_type'].'</td>';

            $profitPrice = !isset($product[1]) || empty($product[1]['price_brutto']) ? 'Brak' :$product[1]['price_brutto'];
            $npPrice = !isset($product[3]) || empty($product[3]['price_brutto']) ? 'Brak' :$product[3]['price_brutto'];
            $mestroPrice = !isset($product[4]) || empty($product[4]['price_brutto']) ? 'Brak' :$product[4]['price_brutto'];

            $productRow .= '<td>'.$profitPrice.'</td>';
            $productRow .= '<td>'.$npPrice.'</td>';
            $productRow .= '<td>'.$mestroPrice.'</td>';

            $productRow .= '</tr>';

            $rows .= $productRow;
        }

        $content .= $rows;

        $content .= '</table>';

        $receivers = [
            'raporty@profit24.pl'
        ];

        foreach($receivers as $mailReciver) {
            \Common::sendMail('raporty@profit24.pl', 'raporty@profit24.pl', $mailReciver, 'Ceneo raport - bufor', $content, true);
        }
    }

    public static function sendUpdateRaportMail(array $tarrifs)
    {
        global $aConfig;

        $name = $aConfig['ceneo_config']['name'];

        $receivers = [
            'raporty@profit24.pl'
        ];

        $content = '

        Produkty NP <br>
        <table>
        <th>Id profit</th>
        <th>Id sklepu</th>
        <th>ISBN</th>
        <th>Glowne minimum</th>
        <th>Nowa cena</th>
        ';

        foreach($tarrifs as $product) {

            $productRow = '<tr>';
            $productRow .= '<td>'.$product['product_id'].'</td>';
            $productRow .= '<td>'.$product['id'].'</td>';
            $productRow .= '<td>'.$product['isbn_plain'].'</td>';
            $productRow .= '<td>'.$product['minimum'].'</td>';
            $productRow .= '<td>'.$product['price_brutto'].'</td>';
            $productRow .= '</tr>';

            $content .= $productRow;
        }

        $content .= '</table>';

        foreach($receivers as $mailReciver) {
            \Common::sendMail('raporty@profit24.pl', 'raporty@profit24.pl', $mailReciver, "Ceneo raport - bufor - $name", $content, true);
        }
    }
}
