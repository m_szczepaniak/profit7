<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Ceneo;

class CategoriesMapper
{
    /**
     * @var string
     */
    private $xmlCategoriesUrl = 'https://api.ceneo.pl/Kategorie/dane.xml';

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct()
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;
    }

    public function synchronizeCategories()
    {
        $categories = simplexml_load_file($this->xmlCategoriesUrl);
        $categories = json_decode(json_encode($categories), 1);

        $array = [];

        $this->prepareCategories($categories['Category'], $array);

        foreach($array as $Id => $ceneoCategory) {

            $this->pDbMgr->Insert('profit24', 'ceneo_categories', [
                'id' => $Id,
                'name' => $ceneoCategory,
            ]);
        }
    }

    /**
     * @return array
     */
    public function findAllCeneoCategories()
    {
        return $this->pDbMgr->GetAssoc('profit24', "SELECT * FROM ceneo_categories ORDER by name");
    }

    /**
     * @param array $categories
     * @param $array
     * @param null $parent
     * @return int
     */
    private function prepareCategories(array $categories, &$array, $parent = null)
    {

        if (isset($categories['Id'])) {

            $categories = [$categories];
        }

        foreach($categories as $key => $category) {

            $del = $parent == null ? '' : '/';

            if ($category['Id'] == null || $category['Name'] == null || !isset($category['Name'])) {

                $ddddd = '';
            }

            $array[$category['Id']] = $parent.$del.$category['Name'];

            if (isset($category['Subcategories']) && !empty($category['Subcategories'])) {

                if ($parent == null) {
                    $parent = $category['Name'];
                } else {
                    $parent .= '/'.$category['Name'];
                }

                $res = $this->prepareCategories($category['Subcategories']['Category'], $array, $parent);

                if ($res == 1) {

                    $explodedParent = explode('/', $parent);
                    array_pop($explodedParent);

                    $parent = implode('/', $explodedParent);
                }
            }

            $arrayKeys = array_keys($categories);
            $lastArrayKey = array_pop($arrayKeys);

            if ($lastArrayKey == $key) {

                return 1;
            }
        }
    }
}