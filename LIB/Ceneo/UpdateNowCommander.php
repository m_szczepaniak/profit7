<?php

use Ceneo\CeneoDataProvider;
use Ceneo\HelperFunctions;
use Ceneo\Limit;
use Ceneo\PriceComparer;
use Ceneo\PriceUpdater;
use Ceneo\ProductsBuffer;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */

//error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED);

//ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');
ini_set('max_execution_time', 1500);
ini_set('memory_limit','2048M');

$aConfig['config']['project_dir'] = 'LIB/Ceneo/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr, $aConfig;

/**
 * START
 */

var_dump("WebsiteId: ".$argv[1]);

HelperFunctions::writeLine('Sprawdzam limity ...');

CeneoDataProvider::setCeneoConfigByWebsiteId($argv[1]);

$limit = new Limit();

if (false === $limit->canRunUpdateScript($argv[1])) {

    exit('Nie mozna bylo uruchomic skryptu');
}

$priceComparer = new PriceUpdater();
$priceComparer->updatePrices($argv[1]);
