<?php

namespace Ceneo;

use Ceneo\XmlGenerator\MestroXmlGenerator;
use Ceneo\XmlGenerator\NieprzeczytaneXmlGenerator;
use DOMDocument;
use LIB\Helpers\ArrayHelper;
use LIB\Helpers\FileHelper;

/**
 * Nakladka na syf w generateCeneoXml_v2.php
 *
 * Class Xml
 * @package PriceUpdater\Ceneo
 */
class Xml
{
    /**
     * @var string
     */
    private $websiteConnCode;

    public function __construct($websiteConnCode)
    {
        $this->websiteConnCode = $websiteConnCode;
    }

    /**
     * @param array $productsToSend
     *
     * @return array
     */
    public function prepareProductsXmlRows(array $productsToSend)
    {
        global $aConfig;

        $productsToSend = $this->getPreparedProductForCeneoUpdate($aConfig, $productsToSend);
        $nieprzeczytaneXmlGenerator = new $aConfig['ceneo_config']['generator_class']($this->websiteConnCode);

        $aShipmentTimes = $nieprzeczytaneXmlGenerator->getShipmentTimes();

        $elements = [];

        $i = 0;
        /** DODAWANIE PRODUKTOW */
        foreach($productsToSend as $product) {
            $aCat = $nieprzeczytaneXmlGenerator->getProductCat($product['id']);
            if(!empty($aCat['name'])){
                $aUniqeIdents = $nieprzeczytaneXmlGenerator->getUniqueIdents($product);

                $bFirst = true;
                foreach ($aUniqeIdents as $sUniqueIdent) {
                    $elements[$product['profit24_id']]['xml'][] = $nieprzeczytaneXmlGenerator->generateElement($product, $sUniqueIdent, $aCat, $aShipmentTimes, $bFirst);
                    $elements[$product['profit24_id']]['raw'][$product['profit24_id']] = $product;
                    $bFirst = false;
                }
            }
            $i++;
        }

        return $elements;
    }

    /**
     * @param $aConfig
     * @param array $productToUpdate
     *
     * @return array
     */
    private function getPreparedProductForCeneoUpdate($aConfig, array $productToUpdate)
    {
        global $pDbMgr, $aConfig;

        $productIds = implode(',', ArrayHelper::arrayColumn($productToUpdate, 'product_id'));

        $sSql = "SELECT A.id, A.profit24_id, A.plain_name, A.isbn_plain, A.isbn_10, A.isbn_13, A.ean_13, A.pages, A.description, A.description_on_create,
									A.shipment_time, B.name AS publisher_name,
									C.binding AS binding_name, D.dimension AS dimension_name,
									A.packet, A.weight, A.prod_status
					 FROM products A
					 LEFT JOIN products_publishers B
					 	ON B.id=A.publisher_id
					 LEFT JOIN products_bindings C
					 	ON C.id=A.binding
					 LEFT JOIN products_dimensions D
					 	ON C.id=A.dimension
					 LEFT JOIN products_images I
							 	 ON I.product_id = A.id
					 WHERE A.profit24_id IN ($productIds)
					 			GROUP BY A.id
                                ORDER BY FIELD(A.profit24_id, $productIds)
					 			";

        $result = (array) $pDbMgr->GetAll($this->websiteConnCode, $sSql);

        $result = $this->prepareProductsForXml($result, $productToUpdate);

        return $result;
    }

    /**
     * @param array $productsToPrepare
     * @param $productToUpdate
     * @return array
     */
    private function prepareProductsForXml(array $productsToPrepare, $productToUpdate)
    {
        $newProducts = [];

        foreach ($productsToPrepare as $product) {

            if (false === isset($productToUpdate[$product['profit24_id']])) {
                continue;
            }

            $product['price_brutto'] = $productToUpdate[$product['profit24_id']]['price_brutto'];

            $newProducts[$product['profit24_id']] = $product;
        }

        return $newProducts;
    }

    public function getFullXml($singlePackage)
    {
        $sXml = '<?xml version="1.0" encoding="utf-8"?>
        <offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">
        <group name="books">
        ';

        foreach($singlePackage as $package) {

            $sXml .= $package['xmlData'];
        }

        $sXml .= '</group>'.'</offers>';

        return $sXml;
    }
}