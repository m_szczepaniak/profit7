<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Ceneo\Api;


class NieprzeczytaneApi extends AbstractApi
{
    protected $host = "https://developers.ceneo.pl/api/v2/function/";

    protected $key = "eab8423f-71b6-452a-a816-09674f42186d";

    protected $shopId = 3;
}