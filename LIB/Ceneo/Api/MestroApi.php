<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Ceneo\Api;


class MestroApi extends AbstractApi
{
    protected $host = "https://developers.ceneo.pl/api/v2/function/";

    protected $key = "96751ff2-5a41-4067-bf57-d790926cc519";

    protected $shopId = 4;
}