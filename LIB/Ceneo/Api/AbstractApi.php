<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Ceneo\Api;

use Ceneo\Exception\ApiException;
use Ceneo\HelperFunctions;
use Exception;

abstract class AbstractApi
{
    protected $host;

    protected $key;

    protected $shopId;

    /**
     * Wybiera z produktow idki ksiegarni podpietej pod api
     *
     * @param array $products
     * @return array
     */
    public function getProductIdsByShopApiId(array $products)
    {
        $productIds = [
            'to_ceneo' => [],
            'shop_profit_id' => []
        ];

        foreach($products as $product) {


            $pWid = $product['website_data'][$this->shopId]['id'];

            if (null == $pWid) {
                continue;
            }

            $productIds['to_ceneo'][] = $pWid;
            $productIds['shop_profit_id'][$pWid] = [
                'pid' => $product['website_data'][1]['id'],
                'minimum_price' => $product['minimum_price'],
                'minimum_price_type' => $product['minimum_price_type'],
            ];
        }

        return $productIds;
    }

    /**
     * Frontcontroller do wywolania
     *
     * @param $method
     * @param array $arguments
     * @param $otherConnection
     * @return mixed
     * @throws ApiException
     */
    public function call($method, array $arguments = array(), $otherConnection = false)
    {
        $response = $this->_callServer($method, $arguments, $otherConnection);

        return $response;
    }

    /**
     * Wysyla posta do api
     *
     * @param $method
     * @param array $parameters
     * @param bool $otherConnection
     * @return mixed
     * @throws ApiException
     */
    private function _callServer($method, $parameters = array(), $otherConnection = false)
    {
        //TODO dorobic parametry

        $fields = [
            'apiKey' => $this->key,
            'resultFormatter' => 'json',
        ];

        $merged = array_merge($fields, $parameters);

        $url = $this->host.$method.'/Call';

        $post_field_string = http_build_query($merged, '', '&');

        $tempU = $url.'?'.$post_field_string;
        $start_time = microtime(TRUE);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field_string);
         curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_POST, true);

        $response = curl_exec($ch);
        curl_close ($ch);

        $tempResp = json_encode($response);
        $response = json_decode($response, true)['Response'];

        $end_time = microtime(TRUE);

        $finishTime = $end_time - $start_time;

        if (false == $response || null == $response) {

            \Common::sendMail('l.jaskiewicz@profit24.pl', 'l.jaskiewicz@profit24.pl', 'l.jaskiewicz@profit24.pl', 'CENEO _TEST POROWNYWANIA - BLAD - PROFIT', $tempU.' - '.$finishTime. ' - '.$tempResp, true);
        }


        $add = '';

        if ('webapi_data_critical.shop_getProductTop10OffersBy_IDs' == $method) {

            $add .= ' '.implode(',', $parameters);
        }

        if (null === $response) {
            throw new ApiException('Zadanie trwalo zbyt dlugo'.$add);
        }

        if (false === $response) {
            throw new ApiException('Nieoczekiwany blad poalczenia API'.$add);
        }

        if ($response['Status'] != 'OK') {
            throw new ApiException($response['Exceptions']['Exception'][0]. '==='. $response['Exceptions']['Exception'][1]. '==='.$add);
        }

        return $response['Result'];
    }

    public function getTop10OffersByIdsLimit()
    {
        $this->call('webapi_meta.GetExecutionLimits');
    }

    public function getTop10OffersByIds(array $products)
    {
        /**
         * TODO test
         */

        $imploded = trim(implode(',', $products));

        $result = $this->call('webapi_data_critical.shop_getProductTop10OffersBy_IDs', [
            'shop_product_ids_comma_separated' => $imploded
        ], true);

        if (false === isset($result['product_offers_by_ids']) || false === isset($result['product_offers_by_ids']['product_offers_by_id'])) {
            throw new ApiException('Lista ofert jest pusta dla produktow '.$imploded);
        }

        return $result['product_offers_by_ids']['product_offers_by_id'];
    }

    public function sendXmlData($xml)
    {
        $result = $this->call('offer_processor.process_offers', [
            'xml' => $xml
        ]);
        var_dump($xml);
        var_dump($result);

        if (false === isset($result['execution_result']['success']) || $result['execution_result']['success'] != 'true') {
            throw new ApiException('Błąd podczas wysyłania xml ceneo - success != true');
        }

        return $result;
    }

    public function getLimits()
    {
        $result = $this->call('webapi_meta.GetExecutionLimits');

        if (false === isset($result['limits']['limit'])) {
            HelperFunctions::sendApiErrorMail('Ceneo - Problem z poalczeniem API - update', 'Błąd podczas pobierania limitow');
            throw new ApiException('Błąd podczas pobierania limitow');
        }

        return $result['limits']['limit'];
    }

    /**
     * @return mixed
     */
    public function getShopId()
    {
        return $this->shopId;
    }
}