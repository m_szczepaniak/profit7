<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Ceneo\Api;

class ApiManager
{
    /**
     * @var AbstractApi[]
     */
    private $ceneoApiConnectors = [];

    public function __construct()
    {
        global $aConfig;

        $connector = new $aConfig['ceneo_config']['api_class']();

        $this->ceneoApiConnectors[] = $connector;
    }

    /**
     * Zwraca top 10 ofert dla 20 podanych produktow
     *
     * @param array $chunkedProducts
     * @return array
     * @throws \Ceneo\Exception\ApiException
     */
    public function getTop10Offers(array $chunkedProducts)
    {
        global $pDbMgr;

        $api = $this->ceneoApiConnectors[0];
        $productIds = $api->getProductIdsByShopApiId($chunkedProducts);

        $profitIds = $productIds['shop_profit_id'];
        $productIds = $productIds['to_ceneo'];

        $result = $api->getTop10OffersByIds($productIds);

        $shopId = $api->getShopId();

        $newResult = [];

        $now = new \DateTime();
        $nowFormatted = $now->format('Y-m-d H:i:s');

        foreach($result as $key => $offer) {

            $ceneoId = $offer['CeneoProdID'];
            $profit24id = $profitIds[$offer['CustProdID']]['pid'];

            $values = [
                'product_id' => $offer['CustProdID'],
                'profit24_id' => $profit24id,
                'response_data' => json_encode($offer['offers']),
                'date' => $nowFormatted,
                'shop_id' => $shopId,
                'minimum_price' => $profitIds[$offer['CustProdID']]['minimum_price'],
                'minimum_price_type' => $profitIds[$offer['CustProdID']]['minimum_price_type'],
                'ceneo_id' => $ceneoId
            ];

            $pDbMgr->Insert('ceneo_reports', 'ceneo_history', $values);

            $cSql = "
            INSERT INTO ceneo_product_ids (profit24_id , ceneo_id) VALUES ($profit24id, '$ceneoId')
            ON DUPLICATE KEY UPDATE profit24_id = $profit24id, ceneo_id = '$ceneoId';
            ";
            $pDbMgr->Query("ceneo_reports", $cSql);

            $newResult[$shopId][$offer['CustProdID']] = $offer;
        }

        return $newResult;
    }

    /**
     * Wysyla paczke 100 produktow do ceneo
     *
     * @param $xml
     * @return mixed
     */
    public function sendXmlData($xml)
    {
        $api = $this->ceneoApiConnectors[0];

        $result = $api->sendXmlData($xml);

        return $result;
    }

    public function getLimits()
    {
        $api = $this->ceneoApiConnectors[0];

        return $api->getLimits();
    }
}