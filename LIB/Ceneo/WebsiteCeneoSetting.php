<?php

namespace Ceneo;

use DatabaseManager;

class WebsiteCeneoSettings
{
    const PRODUCT_COMPARE_LIMIT = 4000;

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * @var array
     */
    private $aConfig;

    public function __construct()
    {
        global $aConfig, $pDbMgr;

        $this->aConfig = $aConfig;
        $this->pDbMgr = $pDbMgr;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getCeneoWebsites()
    {
        $result = $this->pDbMgr->GetAll('profit24', "SELECT * FROM websites WHERE ceneo is not null and ceneo != ''");

        if (false === $result) {

            throw new \Exception('Błąd w pobieraniu listy zamawiarek ceneo');
        }

        return $result;
    }
}
