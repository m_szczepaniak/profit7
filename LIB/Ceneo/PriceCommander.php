<?php

use Ceneo\CeneoDataProvider;
use Ceneo\HelperFunctions;
use Ceneo\Limit;
use Ceneo\PriceComparer;
use Ceneo\ProductsBuffer;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */

//error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED);

//ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');
ini_set('max_execution_time', 1500);
ini_set('memory_limit','2048M');

$aConfig['config']['project_dir'] = 'LIB/Ceneo/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$websiteId = 4;

/**
 * START
 */
var_dump("WebsiteId: $websiteId");

HelperFunctions::writeLine('Sprawdzam limity ...');
//TODO sprawdzic limity

CeneoDataProvider::setCeneoConfigByWebsiteId($websiteId);

$limit = new Limit();

if (false === $limit->canRunCompareScript()) {

    exit('Nie mozna bylo uruchomic skryptu');
}

$priceComparer = new PriceComparer();
$priceComparer->comparePrices();
