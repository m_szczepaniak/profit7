<?php

namespace Ceneo;

use DatabaseManager;
use Exception;
use Listener\CyclicBufforListener;

class ProductsBuffer
{
    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * @var array
     */
    private $aConfig;

    public function __construct()
    {
        global $aConfig, $pDbMgr;

        $this->aConfig = $aConfig;
        $this->pDbMgr = $pDbMgr;
    }

    /**
     * Pobiera produkty do przygotowania i porownania
     *
     * @return array
     * @throws Exception
     */
    public function getProductsToCompare($type, $previous = [])
    {
        $bufforListener = new CyclicBufforListener();
        $bufforListener->onBufforRun();
        $avaibleOnStock = '';

        $full = 40000;
        $byHandLimit = 28000;
        $limit = $byHandLimit;
        $shipmentTime = 0;

        if ($type == 2) {

            $limit = 12000;
            $previousCounted = count($previous);

            if ($previousCounted < $byHandLimit) {

                $limit = $full - $previousCounted;
            }

            $shipmentTime = 1;
        }

        $testIds = [];


        if ($shipmentTime == '0') {
            $availableOnStock = ' JOIN products_stock AS PS ON PS.id = P.id AND PS.profit_j_status = "1" ';
        }

        if (!empty($testIds)) {

            $imploded = implode(',', $testIds);

            $stm = "
            SELECT S.symbol as source, P.id, P.vat, P.shipment_time, P.isbn_plain, PP.discount_limit, P.wholesale_price, PP.ignore_discount,
                   P.price_brutto AS catalogue_price_brutto, P.main_category_id, CTI.product_id AS vip, S.id as source_id

            FROM cyclic_buffor as CB
            JOIN products as P ON CB.product_id = P.id
            JOIN sources as S ON P.source = S.id
            ' . $availableOnStock . '
            LEFT JOIN products_publishers PP on P.publisher_id = PP.id
            LEFT JOIN ceneo_tabs_items AS CTI ON P.id = CTI.product_id
            WHERE CB.status = '1'
            AND P.product_type = 'K'
            AND P.shipment_time = '$shipmentTime'
            AND CB.product_id IN(".$imploded.")
            GROUP BY P.id
            ORDER BY CB.priority DESC
            LIMIT %d
        ";
        } else {
            $stm = "
            SELECT S.symbol as source, P.id, P.vat, P.shipment_time, P.isbn_plain, PP.discount_limit, P.wholesale_price, PP.ignore_discount,
                   P.price_brutto AS catalogue_price_brutto, P.main_category_id, CTI.product_id AS vip, S.id as source_id

            FROM cyclic_buffor as CB
            JOIN products as P ON CB.product_id = P.id
            JOIN sources as S ON P.source = S.id
            ' . $availableOnStock . '
            LEFT JOIN products_publishers PP on P.publisher_id = PP.id
            LEFT JOIN ceneo_tabs_items AS CTI ON P.id = CTI.product_id
            WHERE CB.status = '1'
            AND P.product_type = 'K'
            AND P.shipment_time = '$shipmentTime'
            GROUP BY P.id
            ORDER BY CB.priority DESC
            LIMIT %d
        ";
        }

        $sql = sprintf($stm, $limit);
        $result = $this->pDbMgr->GetAll('profit24', $sql);

        if (false === $result) {
            throw new Exception('Błąd podczas pobierania plikow z bufora produktow');
        }

        return $result;
    }

    public function rebuildBuffer()
    {
        $this->deleteComparedProducts();

        $products = $this->getProductsToBuffer();

        $cyclicBufferListener = new CyclicBufforListener();
        $products = $cyclicBufferListener->onBufforCreate($products);

        if (empty($products)) {
            throw new Exception('Brak produktow do utworzenia bufora');
        }

        $this->saveToProductsBuffer($products);

        HelperFunctions::sendApiErrorMail('CENEO BUFOR ZOSTAL ODBUDOWANY PROFIT', 'bufor odbudowany');

        return $products;
    }

    /**
     * Zapisuje do bufora podane produkty ze statusem 1
     *
     * @param array $products
     * @throws Exception
     */
    public function saveToProductsBuffer(array $products)
    {
        $sql = "INSERT INTO cyclic_buffor (product_id, priority, status) VALUES (%s, '%s' , '1')
                ON DUPLICATE KEY
                UPDATE product_id = %s, priority = CASE WHEN priority = '1' THEN '1' ELSE '%s' END, status = '1'";

        foreach($products as $product) {

            $productId = $product['product_id'];
            $priority = (int)$product['priority'];

            if (false === $this->pDbMgr->Query('profit24', sprintf($sql, $productId, $priority, $productId, $priority))) {

                throw new \Exception('Nie udalo sie zaktualizowac produktu do bufora');
            }
        }
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getProductsToBuffer()
    {
        $sql = "
            SELECT P.id AS product_id, IF(PS.profit_j_status = '1' AND P.shipment_time = '0', '0', '1') AS priority
            FROM website_ceneo_categories as WCC
            JOIN products AS P on P.main_category_id = WCC.category_id
            JOIN products_stock AS PS ON PS.id = P.id
            AND P.published = '1'
            AND P.packet = '0'
            AND (P.prod_status = '1' OR P.prod_status = '3')
            AND (P.shipment_time='0' || P.shipment_time='1')
            AND P.product_type = 'K'
            JOIN products_publishers AS PP ON P.publisher_id = PP.id AND PP.exclude_publishers_products = 0
            GROUP BY P.id
            ORDER BY P.shipment_time DESC
            LIMIT %d
        ";

        //TODO limit calodzienny, zeby nie okazalo sie, ze idzie za duzo produktow przez caly dzien
        $limit = 6000000;

        $standardBufferProducts = $this->pDbMgr->GetAll('profit24', sprintf($sql, $limit));

        if (false === $standardBufferProducts) {

            throw new Exception('Błąd podczas pobierania produktow bazowych do bufora (rebuild)');
        }

        return $standardBufferProducts;
    }

    /**
     * Sprawdza czy bufor jest calkowicie pusty. Puszczamy tylko raz by stworzyc bufor, w inych przypadkach uzywamy isReadyForRebuild
     *
     * @return bool
     */
    public function isEmpty()
    {
        $productCount = (int) $this->pDbMgr->GetOne('profit24', "SELECT COUNT(*) FROM cyclic_buffor");

        return $productCount < 1 ? true : false;
    }

    /**
     * Sprawdzamy czy liczba produktow do porownania jest mniejsza od liczby naszego godzinnego limitu
     *
     * @return bool
     */
    public function isReadyForRebuild()
    {
        /**
         * Zliczamy produkty ktore nie zostaly porownane
         */
        $productCount = (int) $this->pDbMgr->GetOne('profit24', "
                    SELECT COUNT(*)
                    FROM cyclic_buffor AS CB
                    JOIN products AS P ON P.id = CB.product_id AND P.product_type = 'K'
                    WHERE CB.`status` = '1' AND CB.`priority` = '0'");

        if ($productCount < 1) {


            return true;
        }

        return false;
    }

    /**
     * Usuwamy produkty ktore zostaly porowane, zostawiamy priority 1
     * @throws Exception
     */
    private function deleteComparedProducts()
    {
        if (false === $this->pDbMgr->Query('profit24', "DELETE FROM cyclic_buffor WHERE `status` = '0'")) {

            throw new Exception('Nie mozna bylo wyrzucic produktow porownanych');
        }
    }

    public function deactiveComparedProducts(array $products)
    {
        $sql = "UPDATE cyclic_buffor SET `status` = '0' WHERE product_id = %d";

        foreach($products as $product) {

            $this->pDbMgr->Query('profit24', sprintf($sql, $product['id']));
        }
    }

    public function removeNonExistProducts()
    {
        $sql = "
        DELETE CB from cyclic_buffor AS CB
        LEFT JOIN products AS MP ON CB.product_id = MP.id
        LEFT JOIN sources as S ON MP.source = S.id
        WHERE MP.id IS NULL
        OR S.id IS NULL
        OR MP.shipment_time not in('1', '0')
        ";

        $this->pDbMgr->Query('profit24', $sql);
    }
}
