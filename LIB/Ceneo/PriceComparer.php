<?php

namespace Ceneo;

use Ceneo\Api\ApiManager;
use Ceneo\Exception\ApiException;
use Ceneo\Exception\LowDiscountException;
use Ceneo\PriceModifier\DecreasePrice;
use Ceneo\PriceModifier\IncreasePrice;
use Ceneo\PriceModifier\PromotionPrice;
use Common;
use DatabaseManager;
use LIB\Helpers\ArrayHelper;

class PriceComparer
{
    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * @var array
     */
    private $aConfig;

    /**
     * @var Products
     */
    private $products;

    /**
     * @var ProductsBuffer
     */
    private $productsBuffer;

    /**
     * @var ApiManager
     */
    private $apiManager;

    /**
     * @var array
     */
    private $ceneoWebsitesUnactiveAndActie;

    public function __construct()
    {
        global $aConfig, $pDbMgr;

        $this->products = new Products();
        $this->ceneoDataProvider = new CeneoDataProvider();
        $this->productsBuffer = new ProductsBuffer();
        $this->aConfig = $aConfig;
        $this->pDbMgr = $pDbMgr;
        $this->apiManager = new ApiManager();
        $this->ceneoWebsitesUnactiveAndActie = $this->ceneoDataProvider->getActiveAndUnactiveCeneoWebsites();
    }

    public function comparePrices()
    {
        $products = $this->products->getPreparedProductsToCompare();
//        die;
//        foreach($products as $product) {
//
//            if ($product['id'] == 880800) {
//
//                $products = [$product];
//                break;
//            }
//        }

//        var_dump(json_encode($products));die;
//        $dupa = '[{"source":"azymut","id":"592267","vat":"5","shipment_time":"1","isbn_plain":"9788376090887","discount_limit":"0.00","wholesale_price":"176.41","ignore_discount":"1","catalogue_price_brutto":"198.00","main_category_id":"273","has_changed":false,"website_data":{"1":{"id":"592267","price_brutto":"185.98","original_price_brutto":"185.98","code":"profit24","ceneo":"profit24.pl"},"3":{"id":"511964","price_brutto":"183.47","original_price_brutto":null,"code":"np","ceneo":"nieprzeczytane.pl"},"4":{"id":"90589","price_brutto":"198.00","original_price_brutto":null,"code":"mestro","ceneo":"mestro.pl"}},"main_shop_config":{"category_id":"273","website_id":"1","np_percentage_modifier":"13.00","profit24_percentage_modifier":"0.00","mestro_percentage_modifier":"23.00","minimal_overhead":"5.00","price_modifier":"0.02","catalogue_price_substractor":"5.00","minimal_opinions":"1","ignored_shops":"","code":"profit24","ceneo":"profit24.pl"},"minimum_price":185.2305,"minimum_price_type":"STANDARD"}]';
//        $products = json_decode($dupa, true);

        /** dzielimy produkty na wywolania api, po 20 */
        $dividedProducts = array_chunk($products, 105);

        $comparedProducts = [];

        foreach($dividedProducts as $chunkedProducts) {

            try {

                $ceneoOffers = $this->apiManager->getTop10Offers($chunkedProducts);

                /** mergujemy oferty ceneo z produktami */
                $chunkedProducts = $this->mergeOffersWithProducts($ceneoOffers, $chunkedProducts);
                /** produkty z ofertami odfiltrowanymi i nieodfiltrowanymi */
                $chunkedProducts = $this->filterAndSortrOfferByShops($chunkedProducts);
                /** porownujemy paczke 20 produktow i zwracamy tylko te ktore sie zmienily */
                $chunkedProducts = $this->compareChunkedProducts($chunkedProducts);

                //mergujemy
                foreach($chunkedProducts as $comparedProduct) {

                    $comparedProducts[] = $comparedProduct;
                }

            } catch (ApiException $e) {

                HelperFunctions::sendApiErrorMail('Ceneo - Problem z poalczeniem API - update', $e->getMessage());
                continue;
            }
        }

        // produkty zmergowane, porownane i tylk ozmienione
        $tarrifs = $this->prepareTarrifs($comparedProducts);
        HelperFunctions::sendRaportMail($tarrifs, $this->ceneoWebsitesUnactiveAndActie);
        $this->ceneoDataProvider->saveTarrifsToBuffor($tarrifs);

        //TODO dezaktywowac w cyclic buforze ZAPYTAC ARKA KTORE , cenniki ?, porownane ?, wszystkie ?
        $this->productsBuffer->deactiveComparedProducts($products);

        /**
         * Jezeli liczba produktow do porownania jest mniejsza niz limit to czas na odbudowe bufora NA KONCU POROWNYWANIA)
         */
        if (true === $this->productsBuffer->isReadyForRebuild()) {

            $this->productsBuffer->rebuildBuffer();
        }
    }

    /**
     * Ustala kwote brutto dla wszystkich sklepow
     *
     * @param $mainPriceBrutto
     * @param $mainShopId
     * @param array $product
     * @return array
     */
    private function preparePricesForAllShops($mainPriceBrutto, $mainShopId, array $product)
    {
        $prices = [];

        foreach($product['website_data'] as $shopId => $website)
        {
            if ($shopId == $mainShopId) {
                $prices[$shopId] = $mainPriceBrutto;

                continue;
            }

            $shopPercentageModifierType = $website['code'].'_percentage_modifier';
            $percentage = $product['main_shop_config'][$shopPercentageModifierType];
            $finalPriceForShop = $mainPriceBrutto + (($mainPriceBrutto * $percentage) / 100);

            $prices[$shopId] = $finalPriceForShop;
        }

        return $prices;
    }

    /**
     * Tworzy cenniki do zapisu
     *
     * @param array $comapredProducts
     * @return array
     */
    private function prepareTarrifs(array $comapredProducts)
    {
        $tarrifs = [];

        foreach($comapredProducts as $product) {

            if ($product['wholesale_price'] < 0.1) {
                HelperFunctions::sendMail('Ceneo - Za niska cena zakupu', $product['id']);
                continue;
            }
            $mainShopId = $product['main_shop_config']['website_id'];
            $priceBrutto = $product['website_data'][$mainShopId]['price_brutto'];
            $originalPriceBrutto = $product['website_data'][$mainShopId]['original_price_brutto'];

            $allShopsPriceBrutto = $this->preparePricesForAllShops($priceBrutto, $mainShopId, $product);

            $productTarrifs = [];

            foreach ($allShopsPriceBrutto as $websiteID => $shopPriceBrutto) {

                try {

                    /** jezeli produkt jest w promocji to nie tworzymy mu cennika */
                    if (true === isset($product['website_data'][$websiteID]['promotion_value'])) {

                        continue;
                    }

                    $sTarrif = $this->prepareSingleTarrif($mainShopId, $websiteID, $product, $shopPriceBrutto, $priceBrutto);
                    $productTarrifs[$websiteID] = $sTarrif;

                } catch (LowDiscountException $e) {
                    HelperFunctions::sendMail('Ceneo - Problem pdoczas tworzenia cennika', $e->getMessage());
                    continue;
                }
            }

            if (!empty($productTarrifs)) {

                $tarrifs[$product['id']] = $productTarrifs;
            }
        }

        return $tarrifs;
    }

    private function prepareSingleTarrif($mainShopId, $websiteID, array $product, $priceBrutto, $ceneoPrice)
    {
        // obliczanie rabatu
        $discount = Common::formatPrice2((($product['catalogue_price_brutto'] - $priceBrutto) / $product['catalogue_price_brutto']) * 100);
        $originalPriceBrutto = $product['website_data'][$websiteID]['original_price_brutto'];


        if ($discount < 0){
            $discount = 0;
        }

        $fDiscountValue = Common::formatPrice2($product['catalogue_price_brutto'] * ($discount / 100));
        $fNewBrutto = $product['catalogue_price_brutto'] - $fDiscountValue;
        $fNewNetto = Common::formatPrice2($fNewBrutto / (1 + $product['vat']/100));

        $fNewBrutto = Common::formatPrice2($fNewBrutto);

        if ($fNewBrutto < $product['minimum_price']) {
            throw new LowDiscountException('Ceneo - cena nizsza niz wholesale :'. $product['id'].' cena: '.$fNewBrutto.' minimum: '.$product['minimum_price'].' cena ceneo: '.$ceneoPrice);
        }

        if ($originalPriceBrutto == $fNewBrutto) {

            throw new LowDiscountException('Ceneo - brak zmiany ceny - cena rowna starej : SKLEP '.$websiteID.' '.$product['id'] .'cena stara: '.$originalPriceBrutto.' cena nowa: '.$fNewBrutto.' cena ceneo: '.$ceneoPrice);
        }

        $newProduct = [
            'website_id' => $websiteID,
            'product_id' => $product['id'],
            'shop_product_id' => $product['website_data'][$websiteID]['id'],
            'discount' => $discount,
            "discount_value" => $fDiscountValue,
            'price_brutto' => $fNewBrutto,
            'price_netto' => $fNewNetto,
            "change_type" => $product['change_type'],
            "profit_id" => $product['id'],
            "isbn_plain" => $product['isbn_plain'],
            "minimum" => Common::formatPrice2($product['minimum_price']),
            'main_shop_id' => $mainShopId,
            'vip' => $product['vip'] == null ? 0 : $product['vip']
        ];

        return $newProduct;
    }

    /**
     * Porownujemy paczki prouktow po 20 i zwracamy
     *
     * @param array $compareChunkedProducts
     * @return array
     */
    private function compareChunkedProducts(array $compareChunkedProducts)
    {
        $comparedProducts = [];

        foreach($compareChunkedProducts as $product) {

            $promotion = new PromotionPrice($this->ceneoWebsitesUnactiveAndActie);
            $product = $promotion->applyPriceFilter($product);

            if ($product['has_changed'] == true) {

                $comparedProducts[] = $product;
                continue;
            }

            //TODO - probojemy zwiekszyc cene
            $increase = new IncreasePrice($this->ceneoWebsitesUnactiveAndActie);
            $product = $increase->applyPriceFilter($product);
            $decrease = new DecreasePrice($this->ceneoWebsitesUnactiveAndActie);
            $product = $decrease->applyPriceFilter($product);

            if ($product['has_changed'] == true) {

                $comparedProducts[] = $product;
            } else {
                //TODO jakis log ?
            }
        }

        return $comparedProducts;
    }

    /**
     * Laczymy produkty z ofertami - rowniez z pustymi w celu sprawdzenia czy jestesmy sami
     *
     * @param array $ceneoOffers
     * @param array $chunkedProducts
     * @return array
     */
    private function mergeOffersWithProducts(array $ceneoOffers, array $chunkedProducts)
    {
        $ceneoProductsWithOffers = [];
        $apiShopId = array_keys($ceneoOffers)[0];

        foreach($chunkedProducts as $product) {

            $apiProductId = $product['website_data'][$apiShopId]['id'];
            $productOffers = $ceneoOffers[$apiShopId][$apiProductId]['offers']['offer'];
            $productOffers = empty($productOffers) ? [] : $productOffers;

            $product['offers'] = $productOffers;

            $ceneoProductsWithOffers[] = $product;
        }

        return $ceneoProductsWithOffers;
    }

    /**
     * Ogolne przygotowanie ofert do porownywania, usuwanie niepotrzebnych i sortowanie
     *
     * @param array $chunkedProducts
     * @return array
     */
    private function filterAndSortrOfferByShops(array $chunkedProducts)
    {
        $finalProducts = [];

        /** filtrujemy i sortuejmy oferty */
        foreach($chunkedProducts as $product) {

            $unfiltered = $product['offers'];

            /** sortujemy po cenie */
            usort($unfiltered, function($a, $b) {
                return (float)$a['Price'] < (float)$b['Price'] ? -1 : 1;
            });

            $product['unfiltered_offers'] = $unfiltered;

            // standardowe oferty
            $productOffers = $this->prepareOffers($product['offers'], $product);

            $product['offers'] = $productOffers;

            $finalProducts[] = $product;
        }

        // produkty posortowane i poprzypisywane oferty
        return $finalProducts;
    }

    private function prepareOffers(array $offers, $product)
    {
        $productOffers = [];

        /** dorzucamy nasze ksiegarnie do ignorowanych ofert */
        $exclude = explode(',', $product['main_shop_config']['ignored_shops']);

        $ourWebsitesActiveAndUnactive = $this->ceneoWebsitesUnactiveAndActie;
        // usuwamy z wykluczenia ksiegarnie glowna
        unset($ourWebsitesActiveAndUnactive[$product['main_shop_config']['website_id']]);
        $merged = array_unique(array_merge($exclude, $ourWebsitesActiveAndUnactive));

        /** Wywalamy nasze ksiegarnie ale zostawiamy glowna */

        foreach($offers as $offer) {

            /** jezeli jedna z ignorowanych to out */
            if (in_array($offer['CustName'], $merged)) {
                continue;
            }

            /** jezeli mniej opini niz to out */
            if ($offer['ReviewsNumber'] < $product['main_shop_config']['minimal_opinions']) {
                continue;
            }

            $productOffers[] = $offer;
        }

        /** sortujemy po cenie */
        usort($productOffers, function($a, $b) {
            return (float)$a['Price'] < (float)$b['Price'] ? -1 : 1;
        });

        return $productOffers;
    }
}
