<?php

namespace Ceneo;

use Buffer\TopProductsBuffer;
use Ceneo\Exception\MinimumPriceLteZeroException;
use Ceneo\Exception\NoMainCategoryException;
use Ceneo\Exception\NoWebsiteSettingsException;
use DatabaseManager;
use Exception;
use Common;
use LIB\Helpers\ArrayHelper;

class Products
{
    const STANDARD_MINIMUM_PRICE_TYPE = 'STANDARD';
    const PUBLISHER_MINIMUM_PRICE_TYPE = 'PUBLISHER';
    const ZERO_MINIMUM_PRICE_TYPE = 'ZERO';
    const SOURCE_MINIMUM_PRICE_TYPE = 'SOURCE';
    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * @var array
     */
    private $aConfig;

    /**
     * @var ProductsBuffer
     */
    private $productsBuffer;

    /**
     * @var array
     */
    private $websiteSettings;

    /**
     * @var array
     */
    private $websiteCeneoCategories;

    /**
     * @var array
     */
    private $websiteCeneoSettings;

    /**
     * @var TopProductsBuffer
     */
    private $topProductsBuffer;

    /**
     * @var array
     */
    private $sources = [
        'abe' => 15
    ];

    /**
     * @var array
     */
    private $sourcePriceData = [];

    public function __construct()
    {
        global $aConfig, $pDbMgr;

        $this->aConfig = $aConfig;
        $this->pDbMgr = $pDbMgr;
        $this->ceneoDataProvider = new CeneoDataProvider();
        $this->productsBuffer = new ProductsBuffer();
        $this->websiteSettings = $this->ceneoDataProvider->getCeneoWebsites();
        $this->websiteCeneoCategories = $this->ceneoDataProvider->getWebsiteCeneoCategories();
        $this->websiteCeneoSettings = $this->ceneoDataProvider->getWebsiteCeneoSettings();
        $this->topProductsBuffer = new TopProductsBuffer();

        $data = $pDbMgr->GetAll('profit24', "SELECT minimal_overhead, catalogue_price_substractor, website_id, source FROM products_general_discount_common");

        foreach($data as $element) {
            $this->sourcePriceData[$element['website_id']][$element['source']] = $element;
        }
    }

    /**
     * @param $products
     * @return array
     */
    private function mergeWithTopProducts($products)
    {
        $topProductsIds = $this->topProductsBuffer->getTopProductsIds();

        if (empty($topProductsIds)) {
            return $products;
        }

        $implodedTopProductsIds = implode(',', $topProductsIds);

        $sql = "            SELECT S.symbol as source, P.id, P.vat, P.shipment_time, P.isbn_plain, PP.discount_limit, P.wholesale_price, PP.ignore_discount,
                   P.price_brutto AS catalogue_price_brutto, P.main_category_id, CTI.product_id AS vip, S.id as source_id

            FROM products as P
            JOIN sources as S ON P.source = S.id
            LEFT JOIN products_publishers PP on P.publisher_id = PP.id
            LEFT JOIN ceneo_tabs_items AS CTI ON P.id = CTI.product_id
            WHERE 1=1
            AND P.product_type = 'K'
            AND (P.shipment_time = '0' || P.shipment_time = '1')
            AND P.id IN(".$implodedTopProductsIds.")
            GROUP BY P.id
            ";

        $result = $this->pDbMgr->GetAll('profit24', $sql);
        $products = ArrayHelper::toKeyValues('id', $products);

        foreach($result as $topProduct) {
            if (isset($products[$topProduct['id']])) {
                unset($products[$topProduct['id']]);
            }
        }

        foreach($products as $prd) {
            $result[] = $prd;
        }

        return $result;
    }

    /**
     * Zwraca produkty przygotowane do porownywania
     *
     * @return array
     * @throws Exception
     */
    public function getPreparedProductsToCompare()
    {
        $this->productsBuffer->removeNonExistProducts();

        if (true === $this->productsBuffer->isEmpty()) {

            $this->productsBuffer->rebuildBuffer();
        }

        $products = $this->productsBuffer->getProductsToCompare(1);
        $products2 = $this->productsBuffer->getProductsToCompare(2, $products);

        $products = array_merge($products, $products2);
        $products = $this->mergeWithTopProducts($products);

        if (empty($products)) {

            throw new Exception('Niema produktow do porownania');
        }

        $preparedProducts = [];

        // Pobieramy i przygotowujemy promocje w celu sprawdzenia czy dla produktu ma byc tworzony cennik
        $promotions = $this->ceneoDataProvider->getPreparedPromotionProducts();

        $invalidProducts = [];

        /**
         * Przygotowujemy produkty do porownania
         */
        foreach($products as $product) {

            try {
                $product = $this->prepareSingleProduct($product, $promotions);
                $preparedProducts[] = $product;

            } catch (NoMainCategoryException $e) {
                HelperFunctions::sendMail('Ceneo - produkt nie ma przypisanej kategori glownego poziomu '. $product['id'], $e->getMessage());
                $invalidProducts[] = $product;
                continue;
            } catch (NoWebsiteSettingsException $e) {
                HelperFunctions::sendMail('Ceneo - ksiegarnia niema ustawionych wszystkich opcji '. $product['id'], $e->getMessage());
                $invalidProducts[] = $product;
                continue;
            } catch (MinimumPriceLteZeroException $e) {
                HelperFunctions::sendMail('Ceneo - minimum_price mniejsze lub rowne 0 '. $product['id'], $e->getMessage());
                $invalidProducts[] = $product;
                continue;
            }

            //TODO sprawdzic wszystkie rzeczy, price brutto itp itd
        }

        if (!empty($invalidProducts)) {

            $this->productsBuffer->deactiveComparedProducts($invalidProducts);
        }

        return $preparedProducts;
    }

    /**
     * Ustawia wszystkie wartosci dla pojedynczego produktu do porownania
     *
     * @param array $product
     * @param array $promotions
     * @return array|mixed
     * @throws MinimumPriceLteZeroException
     * @throws NoMainCategoryException
     * @throws NoWebsiteSettingsException
     */
    private function prepareSingleProduct(array $product, array $promotions)
    {
        /** Ustawiamy defaultowe wartosci */
        $product['has_changed'] = false;

        $product = $this->prepareWebsitesData($product);
        $product = $this->prepareBasicData($product);
        $product = $this->prepareSourcePriceData($product);
        $product = $this->prepareMinimumPrice($product);

        $productIgnoredPromotions = [];

        foreach($promotions as $websiteId => $promotion) {

            if (isset($promotion[$product['id']])) {

                $product['website_data'][$websiteId]['promotion_value'] = $promotion[$product['id']];
            }
        }

        if (isset($product['vip']) && $product['vip'] !== null) {

            $product['vip'] = 2;

            return $product;
        }

        if ($product['shipment_time'] == 0) {

            $product['vip'] = 1;
        }

        return $product;
    }

    private function prepareSourcePriceData(array $product)
    {
        $mainShopId = $product['main_shop_config']['website_id'];
        $sourceId = $product['source_id'];

        if (null == $mainShopId || null == $sourceId) {
            throw new NoWebsiteSettingsException('Produkt niema id glownego sklepu lub id zrodla');
        }

        $sourceData = $this->sourcePriceData[$mainShopId][$sourceId];

        if ($sourceData['minimal_overhead'] > 0) {
            $product['main_shop_config']['minimal_overhead'] = $sourceData['minimal_overhead'];
        }

        if ($sourceData['catalogue_price_substractor'] > 0) {
            $product['main_shop_config']['catalogue_price_substractor'] = $sourceData['catalogue_price_substractor'];
        }

        return $product;
    }

    /**
     * Ustala cene minimalna na podstawie kilku kryteriow
     *
     * @param array $product
     * @return array
     * @throws MinimumPriceLteZeroException
     */
    private function prepareMinimumPrice(array $product)
    {
        $purchasePrice     = $product['wholesale_price'];
        $publisherDiscount = $product['discount_limit'];
        $ignoreDiscount    = (bool)$product['ignore_discount'];
        $catPriceBrutto    = $product['catalogue_price_brutto'];
        $minimumPrice = 0 ;
        $minimumPriceType = null;

        if (false === $ignoreDiscount && $publisherDiscount > 0) {

            $maxWholesalePriceDiscount = (($catPriceBrutto - $purchasePrice) * 100) / $catPriceBrutto;
            $acceptableMinimumPrice = $purchasePrice + (($purchasePrice * $product['main_shop_config']['minimal_overhead']) / 100);

            if ($maxWholesalePriceDiscount < $publisherDiscount) {

                $minimumPrice = $acceptableMinimumPrice;
            } else {

                $minimumPrice = $catPriceBrutto - (($catPriceBrutto * $publisherDiscount) / 100);
            }

            if ($minimumPrice < $acceptableMinimumPrice) {

                $minimumPrice = $acceptableMinimumPrice;
            }

            /** druga opcja, jak odznaczony jest ifnore discount i limit wiekszy od 0 */
            $minimumPriceType = self::PUBLISHER_MINIMUM_PRICE_TYPE;

        } else {
            /** oblicza cene minimalna za jaka kwote moze byc sprzedany produkt - PODSTAWOWY WARIANT */
            $minimumPrice = $purchasePrice + (($purchasePrice * $product['main_shop_config']['minimal_overhead']) / 100);
            $minimumPriceType = self::STANDARD_MINIMUM_PRICE_TYPE;
        }

        /**
         * Jezeli minimum price mniejsze od 0 albo rowne to znaczy ze jest jakas lipa i ustawiamy minimum jako cene katalogowa
         */
        if ($minimumPrice <= 0) {
            $minimumPrice = $product['catalogue_price_brutto'];

            $minimumPriceType = $minimumPriceType .' + '. self::ZERO_MINIMUM_PRICE_TYPE;

            if (true === in_array($product['source'], array_keys($this->sources))) {
                $minimumPrice = $product['catalogue_price_brutto'] - (($product['catalogue_price_brutto'] * $this->sources[$product['source']]) / 100);
                $minimumPriceType = $minimumPriceType .' + '. self::SOURCE_MINIMUM_PRICE_TYPE;
            }
        }

        /**
         * Jezeli pomimo powyzszych ustawien dalej jest lipa to wyjatek i dziekuje
         */
        if ($minimumPrice <= 0) {

            throw new MinimumPriceLteZeroException('Minimum price jest mniejsze lub rowne 0 dla '.$product['id']);
        }

        if ($minimumPrice < $purchasePrice) {

            throw new MinimumPriceLteZeroException('Minimum price jest mniejsza od ceny zakupu '.$product['id']);
        }

        if ($minimumPrice >= $catPriceBrutto && $catPriceBrutto > 0 && $catPriceBrutto != null) {

            $minimumPrice = $catPriceBrutto;
        }

        $product['minimum_price'] = Common::formatPrice2($minimumPrice);
        $product['minimum_price_type'] = $minimumPriceType;

        return $product;
    }

    /**
     * Pobiera dane z innych ksiegarn, takie jak id, cenniki itp
     *
     * @param array $product
     * @return array
     */
    private function prepareWebsitesData(array $product)
    {
        $product['website_data'] = [];

        $sql = "SELECT P.id, (SELECT PT.price_brutto FROM products_tarrifs AS PT WHERE PT.product_id = P.id AND PT.start_date <= UNIX_TIMESTAMP() AND PT.end_date >= UNIX_TIMESTAMP() ORDER BY PT.type DESC, PT.discount DESC LIMIT 1) AS price_brutto
                FROM products as P WHERE P.profit24_id = %s";

        foreach($this->websiteSettings as $website) {

            /** jezeli profit, to bierzemy od siebie dane */
            if ('profit24' == $website['code']) {
                $pBrutto  = $this->pDbMgr->GetOne('profit24', sprintf("SELECT PT.price_brutto FROM products_tarrifs AS PT WHERE PT.product_id = %s AND PT.start_date <= UNIX_TIMESTAMP() AND PT.end_date >= UNIX_TIMESTAMP() ORDER BY PT.type DESC, PT.discount DESC LIMIT 1", $product['id']));
                $websiteData = [
                    'id' => $product['id'],
                    'price_brutto' => $pBrutto,
                    'original_price_brutto' => $pBrutto,
                    'code' => $website['code'],
                    'ceneo' => $website['ceneo'],
                ];
            } else {
                $websiteData = $this->pDbMgr->GetRow($website['code'], sprintf($sql, $product['id']));
                $websiteData['original_price_brutto'] = $websiteData['price_brutto'];
                $websiteData['code'] = $website['code'];
                $websiteData['ceneo'] = $website['ceneo'];
            }

            $product['website_data'][$website['id']] = $websiteData;
        }

        return $product;
    }

    /**
     * Ustala glowny sklep na podstawie kategori i ustawienia glownej strony
     *
     * @param $product
     * @return mixed
     * @throws NoMainCategoryException
     * @throws NoWebsiteSettingsException
     */
    private function prepareBasicData($product)
    {
        /**
         * Ustala glowny sklep na podstawie kategori
         */

        $priorityMainCategory = $this->pDbMgr->GetRow('profit24', "SELECT * FROM ceneo_product_priority WHERE website_id is not null AND product_id = ".$product['id']);

        if (!empty($priorityMainCategory)) {

            $productMainCategory = $priorityMainCategory;
            unset($productMainCategory['product_id']);
            unset($productMainCategory['id']);
        } else {

            $productMainCategory = $this->websiteCeneoCategories[$product['main_category_id']];
        }

        if (null === $productMainCategory) {
            throw new NoMainCategoryException('Kategoria glowna dla produktu '.$product['id'].' nie jest ustalona, kategoria: '.$product['main_category_id'] );
        }

        $productWebsiteCeneoSettings = $this->websiteCeneoSettings[$productMainCategory['website_id']];

        if (null === $productWebsiteCeneoSettings) {
            throw new NoWebsiteSettingsException('Brak ustawien dla tego sklepu, kategoria: '.$productMainCategory['category_id'].' website_id: '.$productMainCategory['website_id']);
        }

        $mergedConfig = array_merge($productMainCategory, $productWebsiteCeneoSettings);
        $mergedConfig['ceneo'] = $this->websiteSettings[$mergedConfig['website_id']]['ceneo'];

        $product['main_shop_config'] = $mergedConfig;

        return $product;
    }
}
