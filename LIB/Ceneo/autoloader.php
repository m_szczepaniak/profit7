<?php
/**
 * Autoloader lib
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-02-05 
 * @copyrights Marcin Chudy - Profit24.pl
 */

$extensions = array('.php','.class.php','.interface.php');
$ext = implode(',',$extensions);
spl_autoload_register(null, false);
spl_autoload_extensions($ext);
 
function classLoaderCMSLIB($classname){
    $extensions = array('.php','.class.php','.interface.php');
    if (class_exists($classname, false)){ return; }
    $class = explode('\\', strval($classname));
    $deeps = count($class);
    
    $file = preg_replace('/(.*)\\'.DIRECTORY_SEPARATOR.'.*/', '$1', __FILE__);
    for ($i=0;$i<$deeps;$i++){
        $file .= '/'.$class[$i];
    }
    foreach ($extensions as $ext){
        $fileClass = $file.$ext;
        if (file_exists($fileClass) && is_readable($fileClass) && !class_exists($classname, false)){
            require_once($fileClass);
            return true;
        }
    }
    return false;
}
spl_autoload_register('classLoaderCMSLIB');