<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Ceneo;


use Ceneo\Api\ApiManager;
use DateTime;

class Limit
{

    const GET_PRODUCTS_LIMIT_NAME = "webapi_data_critical.shop_getProductTop10OffersBy_IDs";

    const GET_UPDATE_LIMIT_NAME = "offer_processor.process_offers";

    /**
     * @var ApiManager
     */
    private $apiManager;

    public function __construct()
    {
        global $aConfig, $pDbMgr;

        $this->apiManager = new ApiManager();
    }

    /**
     * @return bool
     */
    public function canRunCompareScript()
    {
        // jezeli skrypt odpali sie w niedozwolonyc hgodzinach to wychodzimy odrazu
        if (true === $this->isTimeToBreak()) {
            return false;
        }


        for($i = 0; $i < 5; $i++) {

            $ceneoResponse = $this->canRunCeneoMethod(400, self::GET_PRODUCTS_LIMIT_NAME);

            if (true === $ceneoResponse) {

                HelperFunctions::writeLine('Limity OK');
                return true;
            }

            HelperFunctions::writeLine('Czekam ... 44');
            sleep(150);
        }

        // jezeli skrypt odpali sie w niedozwolonyc hgodzinach to wychodzimy odrazu
        if (true === $this->isTimeToBreak()) {
            return false;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function canRunUpdateScript($websiteId)
    {
        // jezeli skrypt odpali sie w niedozwolonyc hgodzinach to wychodzimy odrazu
        if (true === $this->isTimeToBreak()) {
            return false;
        }


        for($i = 0; $i < 5; $i++) {

            $ceneoResponse = $this->canRunCeneoMethod(CeneoDataProvider::getPackagesLimit($websiteId), self::GET_UPDATE_LIMIT_NAME, true);

            if (true === $ceneoResponse) {

                HelperFunctions::writeLine('Limity OK');
                return true;
            }

            HelperFunctions::writeLine('Czekam ... 55');
            sleep(150);
        }

        // jezeli skrypt odpali sie w niedozwolonyc hgodzinach to wychodzimy odrazu
        if (true === $this->isTimeToBreak()) {
            return false;
        }

        return false;
    }

    /**
     * @param array $limitsList
     * @param $limitName
     * @return null
     */
    private function getLimit(array $limitsList, $limitName)
    {
        foreach($limitsList as $limit) {
            if($limit['function'] == $limitName) {
                return $limit;
            }
        }

        return null;
    }

    /**
     * @param int $acceptableExecutions
     * @param $limitName
     * @return bool|int
     */
    private function canRunCeneoMethod($acceptableExecutions, $limitName, $checkLimit = false)
    {
        $executionLimits = $this->apiManager->getLimits();
        $limit = $this->getLimit($executionLimits, $limitName);

        if (null == $limit) {
            return false;
        }

        // oznacza, ze stary limit wygasl i mozemy aktualizowac
        if (null === $limit['period_executions_left']) {
            return true;
        }

        // jezeli pozostala ilosc wykonan zapytania jest za mala to czekamy
        if ($limit['period_executions_left'] < $acceptableExecutions) {
            return false;
        }

        if (true == $checkLimit) {

            if ($acceptableExecutions != $limit['execution_per_period']) {
                HelperFunctions::sendApiErrorMail('ceneo - zmienil sie limit PROFIT', 'Zmienil sie limit dla update bylo: '.$acceptableExecutions.' jest: '.$limit['execution_per_period']);

                return false;
            }
        }

        return true;
    }


    private function isTimeToBreak()
    {
        $from = new DateTime('21:59');
        $fromFormatted = $from->format('Y-m-d H:i:s');

        $to = new DateTime('23:59');
        $toFormatted = $to->format('Y-m-d H:i:s');

        $now = new DateTime('NOW');
        $nowFormatted = $now->format('Y-m-d H:i:s');

        if ($nowFormatted >= $fromFormatted && $nowFormatted <= $toFormatted)
        {
            return true;
        }

        $from = new DateTime('12:00');
        $fromFormatted = $from->format('Y-m-d H:i:s');

        $to = new DateTime('12:59');
        $toFormatted = $to->format('Y-m-d H:i:s');

        $now = new DateTime('NOW');
        $nowFormatted = $now->format('Y-m-d H:i:s');

        if ($nowFormatted >= $fromFormatted && $nowFormatted <= $toFormatted)
        {
            return true;
        }

        $from = new DateTime('00:00');
        $fromFormatted = $from->format('Y-m-d H:i:s');

        $to = new DateTime('06:59');
        $toFormatted = $to->format('Y-m-d H:i:s');

        $now = new DateTime('NOW');
        $nowFormatted = $now->format('Y-m-d H:i:s');

        if ($nowFormatted >= $fromFormatted && $nowFormatted <= $toFormatted)
        {
            return true;
        }

        return false;
    }
}
