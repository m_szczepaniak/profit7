<?php

namespace Ceneo\PriceModifier;

use Ceneo\HelperFunctions;

class IncreasePrice extends AbstractProductModifier
{

    public function applyPriceFilter(array $product)
    {
        $offers = $product['offers'];
        $priceModifyValue = $product['main_shop_config']['price_modifier'];
        $mainWebsiteData = $product['website_data'][$product['main_shop_config']['website_id']];
        $priceSubstractor = $product['main_shop_config']['catalogue_price_substractor'];
        $priceBrutto = $mainWebsiteData['price_brutto'];
        $unfilteredOffers = $product['unfiltered_offers'];
        $mainShopId = $product['main_shop_config']['website_id'];


        // Sprawdzamy czy jestesmy sami lub czy w ofercie sa tylko nasze ksiegarnie
        if (true === empty($unfilteredOffers) || $this->offersContainsOnlyOurShops($unfilteredOffers)) {

            $pricePercentageValue = $this->round($product['catalogue_price_brutto'] * ($priceSubstractor / 100));

            $priceBrutto = $this->round(($product['catalogue_price_brutto'] - $pricePercentageValue)) ;
            $product['change_type'] = self::INCREASE_ALONE;
            $product['has_changed'] = true;

            if ($priceBrutto < $product['minimum_price']) {
                $priceBrutto = $product['minimum_price'];
            }

            $product['website_data'][$mainShopId]['price_brutto'] = $priceBrutto;
        }
        // Przypadek kiedy sa inne ksiegarnie - tylk oz odfiltorwanych
        else {

            // probojemy powiekszyc jezeli ksiegarnia jest pierwsza oferta (nasze wyrzucone)
            if (true === $this->mainShopOfferIsLowest($product)) {

                $otherLatestMinimalOffer = $offers[1];
                // probojemy podwozszyc tylk owtedy keidy jest konkurencyjna oferta
                if (null !== $otherLatestMinimalOffer) {

                    $newPriceBrutto = $this->round($otherLatestMinimalOffer['Price'] - $priceModifyValue);

                    if ($this->round($priceBrutto) == $newPriceBrutto) {
                        return $product;
                    }

                    $product['website_data'][$mainShopId]['price_brutto'] = $newPriceBrutto;

                    if ($product['website_data'][$mainShopId]['price_brutto'] < $product['minimum_price']) {
                        return $product;
                    }

                    $product['change_type'] = self::INCREASE;
                    $product['has_changed'] = true;
                }
            }
        }

        /**
         * Akcje po porownaniu
         */
        // Jeżeli cena która mamy podniesc jest wyzsza od ceny katalogowej to nic nie robimy
        if ($product['website_data'][$mainShopId]['price_brutto'] > $product['catalogue_price_brutto']) {

            $pricePercentageValue = $this->round($product['catalogue_price_brutto'] * ($priceSubstractor / 100));

            $changtp = 'INCREASE_ALONE_FIX';

            $priceBrutto = $this->round(($product['catalogue_price_brutto'] - $pricePercentageValue));

            if ($priceBrutto < $product['minimum_price']) {

                $priceBrutto = $product['catalogue_price_brutto'];
                $changtp = 'CATAL_MIN_PERCENT_LOWER_THAN_MIN';
            }

            $product['change_type'] = $changtp;
            $product['has_changed'] = true;

            if ($priceBrutto < $product['minimum_price']) {
                $priceBrutto = $product['minimum_price'];
            }

            $product['website_data'][$mainShopId]['price_brutto'] = $priceBrutto;
        }

        return $product;
    }
}
