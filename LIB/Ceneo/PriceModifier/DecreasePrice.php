<?php

namespace Ceneo\PriceModifier;

class DecreasePrice extends AbstractProductModifier
{
    private $minimumPriceDiff = 0.01;

    /**
     * @param array $product
     * @return mixed
     */
    public function applyPriceFilter(array $product)
    {
        if ($product['has_changed'] == true) {

            return $product;
        }

        $offers = $product['offers'];
        $priceModifyValue = $product['main_shop_config']['price_modifier'];
        $mainWebsiteData = $product['website_data'][$product['main_shop_config']['website_id']];
        $mainShopId = $product['main_shop_config']['website_id'];

        /** Maksymalna cena do ktorej mozemy obnizyc (minimalna cena + narzut) bo musi nam sie oplacic */
        $acceptablePrice = $product['minimum_price'];

        foreach($offers as $offer) {

            if (false === $this->canDecreasePrice($offer, $acceptablePrice)) {
                continue;
            }

            if (true === $this->isCurrentBookStore($offer['CustName'], $product)) {
                continue;
            }

            $product['website_data'][$mainShopId]['price_brutto'] = $this->preparePriceBrutto($offer, $acceptablePrice, $priceModifyValue);

            $product['has_changed']  = true;
            $product['change_type']  = self::DECREASE;

            return $product;
        }

        return $product;
    }

    private function preparePriceBrutto($offer, $minimalPrice, $priceModifyValue)
    {
        if ($offer['Price'] == $minimalPrice) {
            return $minimalPrice;
        }

        $minimalPlusModifierFromDb = $this->round($offer['Price'] - $priceModifyValue);
        $minimalPlusModifierDefault = $this->round($offer['Price'] - $this->minimumPriceDiff);

        if ($minimalPlusModifierFromDb > $minimalPrice) {

            return $minimalPlusModifierFromDb;
        }

        if ($minimalPlusModifierDefault > $minimalPrice) {

            return $minimalPlusModifierDefault;
        }

        return $minimalPrice;
    }

    /**
     * @param $offer
     * @param $acceptablePrice
     *
     * @return bool
     */
    private function canDecreasePrice($offer, $acceptablePrice)
    {
        if ($offer['Price'] >= $acceptablePrice) {
            return true;
        }

        return false;
    }
}