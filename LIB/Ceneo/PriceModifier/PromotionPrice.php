<?php

namespace Ceneo\PriceModifier;

use Ceneo\HelperFunctions;

class PromotionPrice extends AbstractProductModifier
{
    const PROMOTION = 'promotion_change_type';

    public function applyPriceFilter(array $product)
    {
        $offers = $product['offers'];
        $priceModifyValue = $product['main_shop_config']['price_modifier'];
        $mainWebsiteData = $product['website_data'][$product['main_shop_config']['website_id']];
        $priceSubstractor = $product['main_shop_config']['catalogue_price_substractor'];
        $priceBrutto = $mainWebsiteData['price_brutto'];
        $unfilteredOffers = $product['unfiltered_offers'];
        $mainShopId = $product['main_shop_config']['website_id'];

        if (isset($mainWebsiteData['promotion_value'])) {

            $product['website_data'][$mainShopId]['price_brutto'] = $mainWebsiteData['promotion_value'];
            $product['change_type'] = self::PROMOTION;
            $product['has_changed'] = true;
        }

        return $product;
    }
}
