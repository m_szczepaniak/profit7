<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Ceneo\PriceModifier;

use Common;

abstract class AbstractProductModifier
{
    const DECREASE = 'decrease';

    const INCREASE = 'increase';

    const INCREASE_ALONE = 'increase_alone';

    const MINIMAL = 'minimal';

    const NEW_PRODUCT = 'new_product';

    const CATALOGUE_PRICE_IS_LOWER = 'catalogue_price_is_lower';

    const PRECISSION = 2;

    /**
     * @var array
     */
    private $ceneoWebsitesUnactiveAndActie;

    public function __construct(array $ceneoWebsitesUnactiveAndActie)
    {
        $this->ceneoWebsitesUnactiveAndActie = $ceneoWebsitesUnactiveAndActie;
    }

    /**
     * @return array
     */
    public function getCeneoWebsitesUnactiveAndActie()
    {
        return $this->ceneoWebsitesUnactiveAndActie;
    }

    /**
     * Szukamy czy nasza oferta jest najnizsza (szukamy w ofertach z usunietymi naszymi ksiegarniami - nie usunelismy ksiegarni glownej)
     *
     * @param array $product
     * @return bool
     */
    protected function mainShopOfferIsLowest(array $product)
    {
        if ($product['offers'][0]['CustName'] == $product['main_shop_config']['ceneo']) {

            return true;
        }

        return false;
    }

    protected function isCurrentBookStore($shopName, array $product)
    {
        return $shopName == $product['main_shop_config']['ceneo'];
    }

    protected function offersContainsOnlyOurShops(array $unfilteredOffers)
    {
        foreach($unfilteredOffers as $offer) {

            $offerName = $offer['CustName'];

            if (!in_array($offerName, $this->ceneoWebsitesUnactiveAndActie)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $val
     * @return string
     */
    protected function round($val)
    {
        return Common::formatPrice2($val);
    }
}