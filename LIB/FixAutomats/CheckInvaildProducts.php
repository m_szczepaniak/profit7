<?php

use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$firstG = "
SELECT PS.id FROM products_stock AS PS
LEFT JOIN products_stock_supplies AS PSS ON PS.id = PSS.product_id AND PSS.source = 10
WHERE PSS.id IS NULL
AND PS.profit_g_status != 0
";

//$firstJ = "
//SELECT PS.id FROM products_stock AS PS
//LEFT JOIN products_stock_supplies AS PSS ON PS.id = PSS.product_id AND PSS.source = 0
//WHERE PSS.id IS NULL
//AND PS.profit_j_status != 0
//";
//
//$firstE = "
//SELECT PS.id FROM products_stock AS PS
//LEFT JOIN products_stock_supplies AS PSS ON PS.id = PSS.product_id AND PSS.source = 1
//WHERE PSS.id IS NULL
//AND PS.profit_e_status != 0
//";

$firstG = $pDbMgr->GetCol('profit24', $firstG);

if (!empty($firstG)) {

    $firstGImploded = implode(',', $firstG);
    $sqlGnew = "UPDATE products_stock SET profit_g_status = 0, profit_g_act_stock = 0, profit_g_reservations = 0 WHERE id IN($firstGImploded)";
    $resasdasdasd = $pDbMgr->Query('profit24', $sqlGnew);
    var_dump(1);
    var_dump($resasdasdasd);
}

$sqlJ = "SELECT PS.id FROM products_stock as PS WHERE PS.profit_j_reservations != (select sum(reservation) from products_stock_supplies where product_id = PS.id and source = 0)";
$sqlE = "SELECT PS.id FROM products_stock as PS WHERE PS.profit_e_reservations != (select sum(reservation) from products_stock_supplies where product_id = PS.id and source = 1)";
$sqlG = "SELECT PS.id FROM products_stock as PS WHERE PS.profit_g_reservations != (select sum(reservation) from products_stock_supplies where product_id = PS.id and source = 10)";

// ----------
$productsStockService = new \LIB\Supplies\ProductStockService($pDbMgr);

$jRes = $pDbMgr->GetCol('profit24', $sqlJ);
$eRes = $pDbMgr->GetCol('profit24', $sqlE);
$gRes = $pDbMgr->GetCol('profit24', $sqlG);

$merged = array_unique(array_merge($jRes, $eRes, $gRes));

if (!empty($merged)) {
    //todo odkomentowac - test
    $productsStockService->updateStockByProductsIds($merged, false, true);
}

$jRes = $pDbMgr->GetCol('profit24', $sqlJ);
$eRes = $pDbMgr->GetCol('profit24', $sqlE);
$gRes = $pDbMgr->GetCol('profit24', $sqlG);

$fixer = new \Reservations\FixNonExistResInSupplies($pDbMgr);

//TODO odkomentowac - test
//$fixer->fixProducts($jRes, 0);
//$fixer->fixProducts($eRes, 1);
//$fixer->fixProducts($gRes, 10);

// ----------

$sqlFirst = "
SELECT PSS.product_id, (reservation - reservation_to_reduce) as reservation, (

SELECT SUM( reservated_quantity ) 
FROM `products_stock_supplies_to_reservations` AS PSSTR
WHERE PSSTR.products_stock_supplies_id = PSS.id
GROUP BY products_stock_supplies_id
) as sum,
reservation_erp
FROM `products_stock_supplies` AS PSS
WHERE (reservation - reservation_to_reduce) <> ( 
SELECT SUM( reservated_quantity ) 
FROM `products_stock_supplies_to_reservations` AS PSSTR
WHERE PSSTR.products_stock_supplies_id = PSS.id
GROUP BY products_stock_supplies_id ) 
ORDER BY PSS.id DESC
";

$sqlSecond = "
SELECT PSR.order_id, quantity,
(
   SELECT SUM( reservated_quantity )
   FROM `products_stock_supplies_to_reservations` AS PSSTR
   WHERE PSSTR.products_stock_reservations_id = PSR.id
   GROUP BY products_stock_reservations_id
) AS sum
FROM `products_stock_reservations` AS PSR
WHERE quantity <> 
(
   SELECT SUM( reservated_quantity )
   FROM `products_stock_supplies_to_reservations` AS PSSTR
   WHERE PSSTR.products_stock_reservations_id = PSR.id
   GROUP BY products_stock_reservations_id
)
AND PSR.move_to_erp = '0'
ORDER BY PSR.id DESC
";

$jRes = $pDbMgr->GetCol('profit24', $sqlJ);
$eRes = $pDbMgr->GetCol('profit24', $sqlE);
$gRes = $pDbMgr->GetCol('profit24', $sqlG);

$firstRes = compress($pDbMgr->GetAll('profit24', $sqlFirst));
$secondRes = compress($pDbMgr->GetAll('profit24', $sqlSecond));

$mailRecipments = ['l.jaskiewicz@profit24.pl', 'raporty@profit24.pl', 'a.golba@profit24.pl'];

if (false == empty($jRes) || false == empty($eRes) || false == empty($gRes) || false == empty($firstRes) || false == empty($secondRes)){

    $content = [
        'BLEDNE PRODUKTY J: ', implode(', ', $jRes),
        'BLEDNE PRODUKTY E: ', implode(', ', $eRes),
        'BLEDNE PRODUKTY G: ', implode(', ', $gRes),
        'BLEDNE SUPPLIES: ', $firstRes,
        'BLEDNE REZERWACJE: ', $secondRes,
    ];

    $content = implode(PHP_EOL, $content);

    foreach($mailRecipments as $recipment){
        Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', $recipment, 'Bledne produkty rezerwacji ', $content);
    }
}

function compress($array)
{
    if (empty($array)){
        return null;
    }

    $newArray = [];

    foreach($array as $v){
        $newArray[] = implode(' , ', $v);
    }

    return implode(PHP_EOL, $newArray);
}
