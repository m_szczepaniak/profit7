<?php

use PriceUpdater\ProductProvider\FinalPriceProductProvider;
use PriceUpdater\PriceUpdater;
use PriceUpdater\Ceneo\Api;
use PriceUpdater\PriceModifier\PriceModifierStrategy;
use PriceUpdater\Ceneo\CeneoRepository;
use PriceUpdater\Tarrifs\TarrifsUpdater;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

function csvs($string, $skip_rows = 0, $separatorChar = ';', $enclosureChar = '"', $newlineChar = "\n")
{
    // @author: Klemen Nagode
    // @source: http://stackoverflow.com/questions/1269562/how-to-create-an-array-from-a-csv-file-using-php-and-the-fgetcsv-function
    $array = array();
    $size = strlen($string);
    $columnIndex = 0;
    $rowIndex = 0;
    $fieldValue="";
    $isEnclosured = false;
    for($i=0; $i<$size;$i++) {

        $char = $string{$i};
        $addChar = "";

        if($isEnclosured) {
            if($char==$enclosureChar) {

                if($i+1<$size && $string{$i+1}==$enclosureChar){
                    // escaped char
                    $addChar=$char;
                    $i++; // dont check next char
                }else{
                    $isEnclosured = false;
                }
            }else {
                $addChar=$char;
            }
        }else {
            if($char==$enclosureChar) {
                $isEnclosured = true;
            }else {

                if($char==$separatorChar) {

                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";

                    $columnIndex++;
                }elseif($char==$newlineChar) {
                    echo $char;
                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";
                    $columnIndex=0;
                    $rowIndex++;
                }else {
                    $addChar=$char;
                }
            }
        }
        if($addChar!=""){
            $fieldValue.=$addChar;

        }
    }

    if($fieldValue) { // save last field
        $array[$rowIndex][$columnIndex] = $fieldValue;
    }


    /**
     * Skip rows.
     * Returning empty array if being told to skip all rows in the array.
     */
    if ($skip_rows > 0) {
        if (count($array) == $skip_rows)
            $array = array();
        elseif (count($array) > $skip_rows)
            $array = array_slice($array, $skip_rows);

    }

    return $array;
}

function getKey($elements, $validElement){
    foreach($elements as $key => $element) {
        if($validElement != $element){
            return $key;
        }
    }

    return null;
}

$arrayToProceed = csvs(file_get_contents('csv.csv'));

$idsToRemove = [];

foreach($arrayToProceed as $element){
    $element = array_values($element);
    $element[2] = str_replace(' ', '', $element[2]);
    $element[4] = str_replace(' ', '', $element[4]);

    $vaildContainer = preg_replace("/[^0-9]/","",$element[6]);

    $containers = explode(',', $element[2]);
    $ids = explode(',', $element[4]);

    $invalidKey = getKey($containers, $vaildContainer);

    // nie znaleziono klucza
    if(null === $invalidKey){
        exit('nie znaleziono klucza');
    }

    $invalidId = $ids[$invalidKey];

    $idsToRemove[] = $invalidId;
}

//TODO TO ODKOMENTOWAC
//$deleteSql = "DELETE FROM orders_items_lists_items WHERE id = %d";
//
//$deleted = 0;
//$deletedIds = [];
//foreach($idsToRemove as $toDelete){
//    $stm = $pDbMgr->Query('profit24', sprintf($deleteSql, $toDelete));
//
//    if (false !== $stm){
//        $deleted++;
//        $deletedIds[] = $toDelete;
//    }
//}

var_dump($deleted);
var_dump($idsToRemove);

