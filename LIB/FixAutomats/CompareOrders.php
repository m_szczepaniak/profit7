<?php

use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$firstArray = [
    '061110022715',
    '061125011215',
    '061126019715',
    '061126033115',
    '061126040815',
    '061126041015',
    '161126017315',
    '241126050615',
    '061127000915',
    '061127001815',
    '061127003115',
    '061127003615',
    '061127023115',
    '061127023715',
    '061127024915',
    '061127038315',
    '061127039715',
    '061128028415',
    '061128034515',
    '161128002515',
    '061129002815',
    '061129003615',
    '061129018615',
    '061129039215',
    '061129039815',
    '061129056815',
    '061129063915',
    '161129052515',
    '061130006315',
];

$secondArray = [
    '061110022715',
    '061124025215',
    '061125011215',
    '061125053215',
    '161126017315',
    '061126019715',
    '161126020615',
    '061126033115',
    '061126040815',
    '061126041015',
    '241126050615',
    '061126051815',
    '061127000915',
    '061127001815',
    '061127003115',
    '061127003615',
    '061127023115',
    '061127023715',
    '061127024915',
    '061127029915',
    '061127038315',
    '061127039715',
    '161128002515',
    '061128028415',
    '061128034515',
    '061129002815',
    '061129003615',
    '061129018615',
    '061129039215',
    '061129039815',
    '161129052515',
    '061129056815',
    '061129063915',
    '061130006315',
];

$diff = array_diff($firstArray, $secondArray);
var_dump($diff);
$asdasd = '';