<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

require_once('../../omniaCMS/lib/OrderRecount.class.php');


$sql = "
SELECT OI.id, O.id as order_id
FROM `orders_send_history` AS OSH
JOIN orders_send_history_items AS OSHI ON OSHI.send_history_id = OSH.id
JOIN orders_items AS OI ON OI.id = OSHI.item_id
JOIN orders AS O ON OI.order_id = O.id
WHERE OSH.number IN ('Z10172/2016', 'Z10187/2016', 'Z10238/2016', 'Z10247/2016', 'Z10244/2016')
AND OSHI.in_confirmed_quantity = OI.quantity
AND OI.deleted = '0'
AND OI.status != 4
AND O.order_status != '5'
";

$data = $pDbMgr->GetAll('profit24', $sql);

if (empty($data)) {
    exit('lipa');
}

$oOrderRecount = new OrderRecount();

foreach($data as $element) {

    $iid = $element['id'];

    $pDbMgr->Query('profit24', "UPDATE orders_items set status = 4 WHERE id = $iid LIMIT 1");

    $oOrderRecount->recountOrder($element['order_id'], false);
}