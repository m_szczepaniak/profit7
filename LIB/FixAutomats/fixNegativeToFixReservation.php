<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;
use Reservations\OrderRepairer2;

ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$file = file_get_contents('reservations/res.txt', 'r');

$ordersToRepair = $pDbMgr->GetCol('profit24', '
    SELECT O.order_number
    FROM orders AS O
    INNER JOIN orders_items AS OI
        ON O.id = OI.order_id
    WHERE
     O.order_status IN ("0", "1") AND
     OI.deleted = "0" AND
     OI.source = "51" AND
    OI.product_id IN (
        SELECT product_id FROM products_stock_supplies WHERE quantity < reservation
    )
    GROUP BY OI.product_id
');
$ordersToRepair = array_unique($ordersToRepair);

if (empty($ordersToRepair) || count($ordersToRepair) < 1) {
    var_dump('Brak zamowien do naprawy');die;
}

$orderClass = new Order($pDbMgr);
foreach($ordersToRepair as $order) {
    var_dump($order);
    try {
        $orderRepair = new OrderRepairer2($order, $orderClass);
        $orderRepair->fixOrder();

        $fixer = new \LIB\Supplies\SuppliesToReservationFixer($pDbMgr);
        $fixer->fix();

    } catch(\Exception $e) {
        var_dump($e->getMessage());
        continue;
    }
}
