<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$table = $pDbMgr->GetCol('profit24', "SELECT * FROM `containers` WHERE id REGEXP '15.{5,5}0(4|5)0$'");

$ready = [];

foreach($table as $element) {

    $element = (string) $element;
    $element = substr($element, 0, -1);

    for($i = 1; $i < 5; $i++) {

        $ready[] = $element.$i;
    }
}

$finalString = '';

foreach($ready as $readyElement) {

    $prepared = "INSERT INTO containers (id, locked, `type`) VALUES (".$readyElement.", 0, 2);\n";
    $finalString .= $prepared;
}

file_put_contents(__DIR__.'/prepared.txt', $finalString);

$d = '';