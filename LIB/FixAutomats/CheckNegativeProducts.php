<?php

use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$sql1 = "select product_id from products_stock_supplies where CAST(quantity AS SIGNED INTEGER) - CAST(reservation AS SIGNED INTEGER) < 0";
$firstRes = $pDbMgr->GetCol('profit24', $sql1);

$sql2 = "SELECT id FROM `products_stock`
WHERE (profit_g_reservations < 0 OR profit_j_reservations < 0 OR profit_e_reservations < 0 OR profit_g_status < 0 OR profit_j_status < 0 OR profit_e_status < 0 OR profit_g_act_stock < 0 OR profit_j_act_stock < 0 OR profit_e_act_stock < 0 )";

$secondRes = $pDbMgr->GetCol('profit24', $sql1);

$merged = array_unique(array_merge($firstRes, $secondRes));

if (count($merged) < 6) {
    exit;
}

$mailRecipments = ['l.jaskiewicz@profit24.pl', 'raporty@profit24.pl'];

foreach($mailRecipments as $recipment){
    Common::sendMail('a.golba@profit24.pl', 'a.golba@profit24.pl', $recipment, 'MINUSOWE PRODUKTY REZERWACJI ', json_encode($merged));
}

var_dump('poszlo');