<?php
use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/FixAutomats';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$sSql = "
SELECT PSS.product_id, (reservation - reservation_to_reduce) as reservation_cms, (

SELECT SUM( reservated_quantity ) 
FROM `products_stock_supplies_to_reservations` AS PSSTR
WHERE PSSTR.products_stock_supplies_id = PSS.id
GROUP BY products_stock_supplies_id
) as sum,
reservation_erp
FROM `products_stock_supplies` AS PSS
WHERE (reservation - reservation_to_reduce) <> ( 
  SELECT SUM( reservated_quantity ) 
  FROM `products_stock_supplies_to_reservations` AS PSSTR
  WHERE PSSTR.products_stock_supplies_id = PSS.id
  GROUP BY products_stock_supplies_id 
) 
ORDER BY PSS.id DESC
";


$aData = $pDbMgr->GetAll('profit24', $sSql);
dump($aData);
foreach ($aData as $item) {
  $bEqual = false;
  if ($item['reservation_cms'] == $item['reservation_erp']) {
    $bEqual = true;
    var_dump($item);
    GetOSSTR($item['product_id']);
  }
  // pobierz wszystkie products_stock_supplies_to_reservations na ten produkt i sprawdź, czy pasuje do rezerwacji
  
}