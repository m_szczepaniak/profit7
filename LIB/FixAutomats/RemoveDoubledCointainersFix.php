<?php

use PriceUpdater\ProductProvider\FinalPriceProductProvider;
use PriceUpdater\PriceUpdater;
use PriceUpdater\Ceneo\Api;
use PriceUpdater\PriceModifier\PriceModifierStrategy;
use PriceUpdater\Ceneo\CeneoRepository;
use PriceUpdater\Tarrifs\TarrifsUpdater;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$sql = '
SELECT COUNT(orders_items_id), GROUP_CONCAT(OILI_TEST.id SEPARATOR ",") as grouped_ids, OI.name, O.order_number, GROUP_CONCAT(OIL_TEST.closed SEPARATOR ",") as grouped_status
FROM orders_items_lists_items AS OILI_TEST
JOIN orders_items_lists AS OIL_TEST
ON OILI_TEST.orders_items_lists_id = OIL_TEST.id
JOIN orders_items AS OI
ON OI.id = OILI_TEST.orders_items_id
JOIN orders AS O
ON O.id =
OILI_TEST.orders_id
  WHERE (DATE(OIL_TEST.created) = "2015-08-07" OR DATE(OIL_TEST.created) = "2015-08-06")  AND OIL_TEST.type = "0"
  AND OIL_TEST.get_from_train = "1"
  GROUP BY orders_items_id
  HAVING COUNT(orders_items_id) > 1
  ORDER BY OIL_TEST.closed DESC
';

$result = $pDbMgr->GetAll('profit24', $sql);

if(true === empty($result)) {
    exit;
}
$idsToRemove = [];

foreach($result as $element){
    $element['grouped_ids'] = explode(',', $element['grouped_ids']);
    $element['grouped_status'] = explode(',', $element['grouped_status']);

    if(false === in_array(1, $element['grouped_status'])){
        continue;
    }

    foreach($element['grouped_status'] as $key => $status) {
        if ($status == 0) {
            $idsToRemove[$element['grouped_ids'][$key]] = $element['grouped_ids'][$key];
        }
    }
}

$deleteSql = "DELETE FROM orders_items_lists_items WHERE id = %d";

$deleted = 0;
$deletedIds = [];
foreach($idsToRemove as $toDelete){
    $stm = $pDbMgr->Query('profit24', sprintf($deleteSql, $toDelete));

    if (false !== $stm){
        $deleted++;
        $deletedIds[] = $toDelete;
    }
}

var_dump($deleted);
var_dump($deletedIds);

