<?php

use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$url = "http://developers.ceneo.pl/api/v2/function/webapi_data_critical.shop_getProductTop10OffersBy_IDs/Call";

$post_field_string = "apiKey=96751ff2-5a41-4067-bf57-d790926cc519&resultFormatter=json&shop_product_ids_comma_separated=321220%2C318779%2C317514%2C310918%2C302061%2C274962%2C338209%2C321711%2C321655%2C56453%2C178416%2C230021%2C260742%2C88020%2C110632%2C208841%2C5952%2C153243%2C101264%2C306893%2C275416%2C282891%2C152294%2C256064%2C302283%2C286702%2C344039%2C255298%2C311448%2C338939%2C219725%2C343243%2C223854%2C318207%2C188358%2C254461%2C238871%2C116326%2C253361%2C196533%2C121941%2C237719%2C81885%2C115652%2C7847%2C80064%2C309268%2C292672%2C201096%2C278207%2C156580%2C258845%2C330735%2C292295%2C245557%2C159199%2C277249%2C289072%2C226876%2C231441%2C192709%2C244292%2C340739%2C222670%2C267260%2C320430%2C17621%2C222445%2C266503%2C62374%2C299370%2C283877%2C341561%2C105497%2C322756%2C215925%2C298493%2C13323%2C315898%2C295375%2C103910%2C322134%2C93889%2C58037%2C235409%2C315428%2C111503%2C294926%2C281103%2C164978%2C56930%2C230621%2C110563%2C163653%2C5904%2C101175%2C306857%2C275381%2C99618%2C305958%2C312245%2C20319%2C303747%2C286657%2C344348";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40);
curl_setopt($ch, CURLOPT_TIMEOUT, 40);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_field_string);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_POST, true);

$response = curl_exec($ch);

var_dump(json_decode($response, true)['Response']['Status']);
curl_close ($ch);