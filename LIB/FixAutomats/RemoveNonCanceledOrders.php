<?php

use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$orderIds = [
    "061020035115",
    "061019081915",
    "060609019215",
    "060821046615",
    "061018019215",
    "060805008015",
    "061012040015",
    "061014075415",
    "061020004615",
    "240818051315",
    "061019045415",
    "240911054315",
    "240813011115",
    "061019033415",
    "060914079915",
    "241019034715",
    "061020052615",
    "241015012815",
    "061020070515",
    "061019018915",
    "241015030515",
    "061018062515",
    "240611024215",
    "241007047915",
    "061014029915",
    "241014016215",
    "061012058015",
    "241021026315",
    "241017036115",
    "060917021915",
    "060902021015",
    "061015002015",
    "061018063515",
    "060828039215",
    "241014029415",
    "240928021215",
    "061022043615",
    "241022033715",
];

$orderIds = implode(',', $orderIds);

$sql = "SELECT * FROM orders WHERE order_number IN($orderIds) AND order_status = '4'";
$orders = $pDbMgr->GetAll('profit24', $sql);

foreach($orders as $order){
    $rm = new \LIB\communicator\sources\Internal\streamsoft\ReservationManager($pDbMgr);
//    var_dump($order);
    $rm->removeOrder($order);
}