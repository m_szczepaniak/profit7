<?php

use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$prefix = "_e_";

$sql = "
SELECT OI.id, P.weight, OI.product_id
FROM products AS P
JOIN products_stock AS PS ON P.id = PS.id
JOIN orders_items AS OI ON P.id = OI.product_id
JOIN orders AS O ON O.id = OI.order_id
AND O.order_status NOT 
IN (
'4',  '5'
)
WHERE (
OI.weight =0
OR OI.weight IS NULL
)
AND P.weight > 0
ORDER BY O.order_date DESC 
";
$rows = $pDbMgr->GetAll('profit24', $sql);
dump($rows);
foreach($rows as $row) {
    $sSql = 'UPDATE orders_items
             SET weight = "'.$row['weight'].'"
             WHERE id = '.$row['id'].' 
             AND product_id = '.$row['product_id'].'
             LIMIT 1';
    Common::Query($sSql);
}