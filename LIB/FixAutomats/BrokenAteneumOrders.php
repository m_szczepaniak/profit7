<?php

use LIB\Helpers\ArrayHelper;
use PriceUpdater\ProductProvider\FinalPriceProductProvider;
use PriceUpdater\PriceUpdater;
use PriceUpdater\Ceneo\Api;
use PriceUpdater\PriceModifier\PriceModifierStrategy;
use PriceUpdater\Ceneo\CeneoRepository;
use PriceUpdater\Tarrifs\TarrifsUpdater;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$brokenOrders = [];
$brokenSerialized = [];

$sql = "SELECT O.id, O.order_number, O.value_brutto, OD.order_session
        FROM `orders` AS O
        JOIN `orders_debug` AS OD ON O.ID = OD.oid
        WHERE O.order_date > '2015-08-05'
        AND O.order_on_list_locked != '1'
        ";

$sql2 = "SELECT id, product_id, quantity
          FROM orders_items
          WHERE order_id = %d
          AND added_by_employee = 0
          AND deleted != '1'
          ";

$result = $pDbMgr->GetAll('profit24', $sql);

if (true === empty($result)) {
    exit('Nie znaleziono produktow');
}

foreach($result as $element) {

    $unsrlzd = unserialize($element['order_session']);

    if (false == $unsrlzd) {

        $unsrlzd = mb_unserialize($element['order_session']);

        if (false == $unsrlzd) {
            $brokenSerialized[] = $element['id'];
        }
    }

    $element['order_session'] = $unsrlzd;

//    if ((string)$element['order_session']['total_price_brutto'] == (string)$element['value_brutto']) {
//        continue;
//    }

    $isOrderInvalid = checkInvaildOrdersItems($pDbMgr, $sql2, $element);

    if (false === $isOrderInvalid) {
        continue;
    }

    $newElement = [
        'id' => $element['id'],
        'original_price_brutto' => $element['order_session']['total_price_brutto'],
        'new_price_brutto' => $element['value_brutto'],
        'order_number' => $element['order_number']
    ];

    $brokenOrders[] = $newElement;
}

var_dump($brokenSerialized);
var_dump(count($brokenOrders));
var_dump($brokenOrders);

// FUNKCJE
function checkInvaildOrdersItems($pDbMgr, $sql2, $element)
{
    $originalProducts = $element['order_session']['products'];

    $ordersItems = $pDbMgr->GetAll('profit24', sprintf($sql2, $element['id']));

    foreach($originalProducts as $productId => $originalProduct) {
        $sameProducts = getElementsWhere($ordersItems, ['product_id' => $productId]);
        if (count($sameProducts) > 1) {
            return true;
        }
    }

    return false;
}

function getElementsWhere($array, $where)
{
    $newElements = [];

    foreach($array as $element) {

        $isValid = true;

        foreach($where as $key => $clause) {
            if($element[$key] != $clause){
                $isValid = false;
                break;
            }
        }

        if ($isValid == true){
            $newElements[] = $element;
        }
    }

    return $newElements;
}

function mb_unserialize($string) {
    $string = preg_replace_callback(
        '!s:(\d+):"(.*?)";!s',
        function ($matches) {
            if ( isset( $matches[2] ) )
                return 's:'.strlen($matches[2]).':"'.$matches[2].'";';
        },
        $string
    );
    return unserialize($string);
}