<?php


/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$sql = "
DELETE PT
FROM products_tarrifs AS PT
JOIN products AS P ON P.id = PT.product_id
WHERE PT.`type` =1
AND PT.price_brutto < P.wholesale_price
";

$result = $pDbMgr->GetAll('np', $sql);

if (!empty($result)) {
    foreach($result as $tarrif){
        $id = $tarrif['id'];
        $sql = "DELETE FROM products_tarrifs WHERE id = $id LIMIT 1";
        $pDbMgr->Query('np', $sql);
    }
}
