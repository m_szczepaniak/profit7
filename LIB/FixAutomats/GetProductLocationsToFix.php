<?php

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

function findProductLocation($productId) {
    global $pDbMgr;


    $sSql = 'SELECT SL.container_id
             FROM stock_location AS SL
             WHERE SL.products_stock_id = '.$productId.' AND  SL.magazine_type = "SH" ';
    $containersStockLocations = $pDbMgr->GetCol('profit24', $sSql);

    $sSql = 'SELECT I.container_id
             FROM inventory_items AS II
             JOIN inventory AS I
              ON II.inventory_id = I.id
             JOIN containers AS C
              ON I.container_id = C.id AND C.magazine_type = "SH"
             WHERE II.product_id = '.$productId.'
             ';
    $containersInventory = $pDbMgr->GetCol('profit24', $sSql);

    if (!empty($containersStockLocations) && $containersInventory) {
        return array_unique(array_merge($containersInventory, $containersInventory));
    } else {
        if (!empty($containersStockLocations)) {
            return array_unique($containersStockLocations);
        }
        elseif (!empty($containersInventory)) {
            return array_unique($containersInventory);
        } else {
            return false;
        }
    }

}

function checkEqualStock($productId) {
    global $pDbMgr;

    $sSql = 'SELECT SUM(quantity)
             FROM stock_location
             WHERE products_stock_id = '.$productId;
    $iLocation = $pDbMgr->GetOne('profit24', $sSql);

    $sSql = 'SELECT profit_g_act_stock
             FROM products_stock
             WHERE id = '.$productId;
    $iStock = $pDbMgr->GetOne('profit24', $sSql);
    if (intval($iLocation) == intval($iStock)) {
        return true;
    }
    return false;
}


$sSql = '
SELECT P.name, P.ean_13, SL.container_id, SL.quantity, SL.reservation, OILI.product_id, OILI.id, SLO.id AS SLO_ID
FROM orders_items_lists_items AS OILI
JOIN orders_items_lists AS OIL
 ON OILI.orders_items_lists_id = OIL.id AND OIL.type = "8"
JOIN products AS P
 ON P.id = OILI.product_id
LEFT JOIN stock_location_orders_items_lists_items AS SLO
  ON OILI.id = SLO.orders_items_lists_items_id AND SLO.stock_location_magazine_type = "SH"
LEFT JOIN stock_location AS SL
  ON SLO.stock_location_id = SL.id AND SLO.stock_location_magazine_type = SL.magazine_type
WHERE 
DATE(OIL.created) < "2018-02-01"
GROUP BY OILI.product_id
';
$ordersItemsListsItems = $pDbMgr->GetAll('profit24', $sSql);
echo implode("\t", array_keys($ordersItemsListsItems[0]))."\n";
foreach ($ordersItemsListsItems as $ordersItemsListsItem) {
//
//    if ($ordersItemsListsItem['SLO_ID'] > 0) {
//        $checkIsDouble = checkIsDouble($ordersItemsListsItem['id']);
//        if (true == $checkIsDouble) {
//
//        }
//    }


    $isEqual = checkEqualStock($ordersItemsListsItem['product_id']);
    // można by tutaj sprawdzić czy stany się zgadzają

    if (false === $isEqual) {
        if (!empty($ordersItemsListsItem['container_id'])) {
            echo $ordersItemsListsItem['name'] . "\t" . $ordersItemsListsItem['ean_13'] . "\t" . $ordersItemsListsItem['container_id'] . "\t" . $ordersItemsListsItem['quantity'] . "\t" . $ordersItemsListsItem['reservation'] . "\n";
        } else {
            $containers = findProductLocation($ordersItemsListsItem['product_id']);
            if ($containers === false) {
                echo 'BŁĄD BRAK LOKALIZACJI PRODUKTU O EAN'.$ordersItemsListsItem['ean_13'].' , tytuł '.$ordersItemsListsItem['name'].' PRZYJĘTE PZ w tym tyg. przez'."\n";
                $sSql = 'SELECT OIL.created, OILI.quantity, CONCAT(U.name, " ",U.surname) as created_by
                         FROM orders_items_lists_items AS OILI
                         JOIN orders_items_lists  AS OIL
                          ON OILI.orders_items_lists_id = OIL.id AND OIL.type = "7"
                         JOIN users AS U
                          ON OIL.user_id = U.id
                         WHERE product_id = "'.$ordersItemsListsItem['product_id'].'"
                         ';
                $data = $pDbMgr->GetAll('profit24', $sSql);
                foreach ($data as $item) {
                    echo $item['created_by'].' o godz: '.$item['created'].' w ilości: '.$item['quantity']."\n";
                }
            } else {
                echo $ordersItemsListsItem['name'] . "\t" . $ordersItemsListsItem['ean_13'] . "\t" .
                    implode(", ", $containers) . "\n";
            }
        }
    }
}
