<?php

use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use Reservations\ReservationRenewer;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'Off');

$aConfig['config']['project_dir'] = 'LIB/FixAutomats';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/import/CommonSynchro.class.php');

function csvs($string, $skip_rows = 0, $separatorChar = ',', $enclosureChar = '"', $newlineChar = "\n")
{
    // @author: Klemen Nagode
    // @source: http://stackoverflow.com/questions/1269562/how-to-create-an-array-from-a-csv-file-using-php-and-the-fgetcsv-function
    $array = array();
    $size = strlen($string);
    $columnIndex = 0;
    $rowIndex = 0;
    $fieldValue="";
    $isEnclosured = false;
    for($i=0; $i<$size;$i++) {

        $char = $string{$i};
        $addChar = "";

        if($isEnclosured) {
            if($char==$enclosureChar) {

                if($i+1<$size && $string{$i+1}==$enclosureChar){
                    // escaped char
                    $addChar=$char;
                    $i++; // dont check next char
                }else{
                    $isEnclosured = false;
                }
            }else {
                $addChar=$char;
            }
        }else {
            if($char==$enclosureChar) {
                $isEnclosured = true;
            }else {

                if($char==$separatorChar) {

                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";

                    $columnIndex++;
                }elseif($char==$newlineChar) {
                    echo $char;
                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";
                    $columnIndex=0;
                    $rowIndex++;
                }else {
                    $addChar=$char;
                }
            }
        }
        if($addChar!=""){
            $fieldValue.=$addChar;

        }
    }

    if($fieldValue) { // save last field
        $array[$rowIndex][$columnIndex] = $fieldValue;
    }


    /**
     * Skip rows.
     * Returning empty array if being told to skip all rows in the array.
     */
    if ($skip_rows > 0) {
        if (count($array) == $skip_rows)
            $array = array();
        elseif (count($array) > $skip_rows)
            $array = array_slice($array, $skip_rows);

    }

    return $array;
}




$file = 'csv/LEGO_KODY_EAN_ZBIORCZE.csv';
$arrayToProceed = csvs(file_get_contents($file));

$commonSynchro = new \CommonSynchro();

foreach ($arrayToProceed as $rows) {
    $ean = trim($rows[0]);
    $standardCode = '0'.trim($rows[1]);
    $standardQuantity = intval($rows[2]);

    // szukamy EANU
    $test['ean_13'] = $standardCode;
    $matchedProduct = $commonSynchro->refMatchProductByISBNs($test, ['id', 'isbn_plain', 'isbn_10', 'isbn_13', 'ean_13', 'streamsoft_indeks']);
    if (isset($matchedProduct['id']) && $matchedProduct['id'] > 0) {
        if (
            $standardCode == $matchedProduct['isbn_plain'] ||
            $standardCode == $matchedProduct['isbn_10'] ||
            $standardCode == $matchedProduct['isbn_13'] ||
            $standardCode == $matchedProduct['ean_13'] ||
            $standardCode == $matchedProduct['streamsoft_indeks']
        ) {
            echo 'DUPLIKAT KODU STANDARD: '.$standardCode;
            die;
        }
    }



    $product['ean_13'] = $ean;
    $matchedProduct = $commonSynchro->refMatchProductByISBNs($product);

    if ($matchedProduct['id'] > 0) {
        var_dump($matchedProduct);
        var_dump($ean);
        var_dump($standardCode);
        var_dump($standardQuantity);
        $values = [
            'standard_code' => $standardCode,
            'standard_quantity' => $standardQuantity
        ];
        Common::Update('products', $values, ' id = '.$matchedProduct['id'].' LIMIT 1');
    } else {
        echo "brak";
    }
}