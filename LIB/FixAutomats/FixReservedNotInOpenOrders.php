<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;


$sSql = 'SELECT PSS.id, PSS.product_id, PS.profit_g_act_stock, P.EAN_13, PSS.reservation_erp
FROM `products_stock_supplies` AS PSS 
JOIN products_stock AS PS 
  ON PSS.product_id = PS.id
LEFT JOIN products AS P
    ON P.id = PSS.product_id
WHERE
(PSS.reservation-PSS.reservation_to_reduce) > 0 AND
PSS.source = 10
AND
(
    SELECT OI.id
    FROM orders_items AS OI
    JOIN orders AS O
        ON O.id = OI.order_id
    WHERE
    OI.product_id = PSS.product_id
    AND OI.deleted = "0"
    AND OI.source = 51
    AND OI.status <> "0"
    AND O.order_status <> "5"
    AND O.erp_export IS NULL
    AND DATE(O.order_date) > "2017-01-01"
    LIMIT 1
)
IS NULL

';
$data = $pDbMgr->GetAll('profit24', $sSql);
dump($data);
foreach ($data as $row) {
    $aValues = [
        'profit_g_reservations' => 0,
        'profit_g_status' => $row['profit_g_act_stock']
    ];
    if ($pDbMgr->Update('profit24', 'products_stock',  $aValues, ' id = '.$row['product_id'].' LIMIT 1') === false) {
        throw new Exception('Błąd aktualizacji product_stock');
    }


    $aVal2 = [
        'reservation' => 0,
    ];

    if ($pDbMgr->Update('profit24',  'products_stock_supplies',  $aVal2,' id = '.$row['id'].' LIMIT 1') === false) {
        throw new Exception('Błąd aktualizacji products_stock_supplies');
    }

    $sSql = 'SELECT PSR.id
              FROM products_stock_supplies_to_reservations AS PSSTR
              JOIN products_stock_reservations AS PSR
                ON PSSTR.products_stock_reservations_id = PSR.id
              WHERE PSSTR.products_stock_supplies_id = '.$row['id'].'
             ';
    $items = $pDbMgr->GetCol('profit24', $sSql);

    if (!empty($items)) {
        $sSql = 'DELETE FROM products_stock_reservations WHERE id IN ("' . implode(',', $items) . '") ';
        $pDbMgr->Query('profit24', $sSql);
    }
}

echo 'done';