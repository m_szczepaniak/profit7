<?php

use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use Reservations\ReservationRenewer;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$ceneoHistory = $pDbMgr->GetAll('ceneo_reports', "SELECT * FROM ceneo_history group by profit24_id");

foreach($ceneoHistory as $element) {

    $p24id = $element['profit24_id'];
    $ceneo = $pDbMgr->GetRow('ceneo_reports', "SELECT * FROM ceneo_history WHERE profit24_id = $p24id ORDER BY date desc limit 1");
    $ceneoId = $ceneo['ceneo_id'];

    $cSql = "
            INSERT INTO ceneo_product_ids (profit24_id , ceneo_id) VALUES ($p24id, '$ceneoId')
            ON DUPLICATE KEY UPDATE profit24_id = $p24id, ceneo_id = '$ceneoId';
            ";
    $pDbMgr->Query("ceneo_reports", $cSql);

    var_dump($ceneoId);
}
