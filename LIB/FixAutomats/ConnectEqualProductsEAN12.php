<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 17.05.17
 * Time: 08:45
 */
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/FixAutomats';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;


$sql = "
SELECT 
    P.isbn_plain AS P_isbn_plain, 
    P2.isbn_plain AS P2_isbn_plain, 
    P.id AS first_id, 
    P2.id AS second_id 
FROM `products` AS P
JOIN products AS P2
  ON P.id <> P2.id AND CONCAT('0', P.isbn_plain) = P2.isbn_plain
WHERE LENGTH(P.isbn_plain) = 12
";
$products = $pDbMgr->GetAll('profit24', $sql);
dump($products);

include_once($_SERVER['DOCUMENT_ROOT'].'./omniaCMS/modules/m_oferta_produktowa/Module.class.php');
$oParent = new \stdClass();
$module = new Module($pSmarty, $oParent, TRUE);
$aWebsites = [
    'np',
    'it',
    'mestro',
    'smarkacz',
    'nb'
];

$pDbMgr->BeginTransaction('profit24');
foreach ($aWebsites as $codes) {
    $pDbMgr->BeginTransaction($codes);
}
$bIsErr = false;

foreach ($products as $product) {
    if ($module->connectTwoProducts($aWebsites, $product['second_id'], $product['first_id']) === false) {
        $bIsErr = true;
        break;
    }
}

if ($bIsErr === false) {
    $pDbMgr->CommitTransaction('profit24');
    foreach ($aWebsites as $codes) {
        $pDbMgr->CommitTransaction($codes);
    }
    echo 'COMMIT';
} else {
    $pDbMgr->RollbackTransaction('profit24');
    foreach ($aWebsites as $codes) {
        $pDbMgr->RollbackTransaction($codes);
    }
    echo sprintf(_('Wystąpił błąd podczas łączenia produktów z powodu %s'), $module->reasonMsg);
}

