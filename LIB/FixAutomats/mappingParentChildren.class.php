<?php

namespace LIB\FixAutomats;
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 29.08.17
 * Time: 14:26
 */
class mappingParentChildren
{
    private $pDbMgr;

    public function __construct($pDbMgr)
    {

        $this->pDbMgr = $pDbMgr;
    }

    /**
     *
     */
    public function getChildsCategories(&$refChilds, $iCategory)
    {
        $results = array_flip($this->getChilds($iCategory));

        if (!empty($results)) {
            $refChilds[$iCategory] = $results;
            foreach ($results as $iChild => $childs) {
                $this->getChildsCategories($refChilds[$iCategory], $iChild);
            }
            $allMyChildIds = [];
            $this->getAllMyChildIds($allMyChildIds, $refChilds[$iCategory]);
            $this->addMyChildsMappings($iCategory, $allMyChildIds);
            $refChilds[$iCategory]['child_ids'] = $allMyChildIds;

        }
    }

    public function getAllMyChildIds(&$allMyChildIds, $rows) {
        if (!empty($rows) && is_array($rows)) {
            foreach ($rows as $row => $value) {
                if ($row != 'child_ids') {
                    $allMyChildIds[] = $row;
                    if (!empty($value) && is_array($value)) {
                        $this->getAllMyChildIds($allMyChildIds, $value);
                    }
                }
            }
        }
    }

    private function getChilds($iCategory) {

        $sSql = ' SELECT id 
                  FROM menus_items 
                  WHERE parent_id = '.$iCategory;
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

    public function addMyChildsMappings($iCategory, $allMyChildIds)
    {
        $sSql = 'SELECT source_id, source_category_id
                 FROM menus_items_mappings
                 WHERE page_id IN ('.implode(', ', array_values($allMyChildIds)).')';
        $aMappings = $this->pDbMgr->GetAll('profit24', $sSql);
        foreach ($aMappings as $mapping) {
            $aValues = [
                'page_id' => $iCategory,
                'source_id' => $mapping['source_id'],
                'source_category_id' => $mapping['source_category_id'],
            ];
            var_export($aValues);
            $this->pDbMgr->Insert('profit24', 'menus_items_mappings', $aValues);
        }


    }
}