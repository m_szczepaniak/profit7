<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'] . '/LIB/autoloader.php');

global $pDbMgr;

/*
15
AL 7 REG 27 7 półek 2 strefy
AL 8 REG 26
AL 9 REG 32

1101001011
1507027074
1507027075
*/
$numbers = [];





$alejki = ['1'];
$maxReg = 17;
$maxPol = 5;
$stref = ['1', '2', '3'];

$prefix = '17';
//for ($iAl = 1; $iAl <= $maxAl; $iAl++) {
foreach ($alejki as $alejka) {
    $numberAl = sprintf("%1$02d", $alejka);

    for ($iReg = 1; $iReg <= $maxReg; $iReg++) {
        $numberReg = sprintf("%1$03d", $iReg);

        for ($iPol = 1; $iPol <= $maxPol; $iPol++) {
            $numberPol = sprintf("%1$02d", $iPol);
                foreach ($stref as $oneStref) {
                    $number = $prefix . $numberAl . $numberReg . $numberPol . $oneStref;
                    $numbers[] = $number;
                    echo $number . '<br />';
                }
        }
    }
}
die;

$prefix = '1508';
$maxReg = 26;
for ($iReg = 1; $iReg <= $maxReg; $iReg++) {
    $numberReg = sprintf("%1$03d", $iReg);

    for ($iPol = 1; $iPol <= $maxPol; $iPol++) {
        $numberPol = sprintf("%1$02d", $iPol);
        if ($numberPol == 4 || $numberPol == 5) {
            foreach ($stref as $oneStref) {
                $number = $prefix . $numberReg . $numberPol . $oneStref;
                $numbers[] = $number;
                echo $number . '<br />';
            }
        } else {
            $oneStref = '0';
            $number = $prefix . $numberReg . $numberPol . $oneStref;
            $numbers[] = $number;
            echo $number . '<br />';
        }
    }
}


$prefix = '1701';
$maxReg = 17;
for ($iReg = 1; $iReg <= $maxReg; $iReg++) {
    $numberReg = sprintf("%1$03d", $iReg);

    for ($iPol = 1; $iPol <= $maxPol; $iPol++) {
        $numberPol = sprintf("%1$02d", $iPol);
        if ($numberPol == 4 || $numberPol == 5) {
            foreach ($stref as $oneStref) {
                $number = $prefix . $numberReg . $numberPol . $oneStref;
                $numbers[] = $number;
                echo $number . '<br />';
            }
        } else {
            $oneStref = '0';
            $number = $prefix . $numberReg . $numberPol . $oneStref;
            $numbers[] = $number;
            echo $number . '<br />';
        }
    }
}


var_dump($numbers);

/*
$numbers = [];
for ($i = 1; $i < 91; $i++) {
    $iteration = sprintf("%1$02d", $i);
    $number_iterator = $prefix . $iteration . $postfix;
    $numbers[] = $number_iterator;
    $values = [
        'id' => $number_iterator,
        'locked' => '0',
        'type' => '0',
        'item_id' => 'NULL'
    ];
    if (false === $pDbMgr->Insert('profit24', 'containers', $values)) {
        dump($values);
        echo 'BABOŁ ';
    }
    echo $number_iterator . "\n";
}
*/