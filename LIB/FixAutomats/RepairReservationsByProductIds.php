<?php

use Common;
use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\communicator\sources\Internal\streamsoft\Reservation;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;
use Reservations\ReservationRenewer;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$productIds = [
    981543,
    975108,
    909660,
    909690,
    904697,
    904698,
    569100,
    873507,
    791009,
    791010,
    693884,
    627486,
    558275,
    558274,
    612353,
    635550,
    989959,
    889347,
    738723,
    991102,
    994765,
    996557,
    681813,
    602510,
    929216,
    997290,
    996678,
    982244,
    993359,
    862302,
    990771,
    998876,
    952247,
    994215,
    994903,
];

$datetime = new DateTime('2013-01-29');
$datetime->modify('-20 days');

$finalDate = $datetime->format('Y-m-d');

$reservationRenewer = new ReservationRenewer($pDbMgr , new ReservationManager($pDbMgr), new DataProvider($pDbMgr));

foreach($productIds as $product) {

    var_dump("product: $product");

    $sql = "
        SELECT O.id from orders AS O
        JOIN orders_items AS OI ON OI.order_id = O.id AND OI.product_id = ".$product." AND OI.source = '51'
        WHERE O.order_status = '1'
        AND
        (
            SELECT count(OI2.id)
            FROM orders_items AS OI2
            WHERE OI2.source = '51' AND
            OI2.status = '4' AND
            OI2.order_id = O.id
        ) = 0
        ORDER BY O.to_pay ASC
        LIMIT 1
    ";

    $orderId = $pDbMgr->GetOne('profit24', $sql);

    if (null != $orderId && !empty($orderId)) {

        // odpalamy
        $reservationRenewer->repairSingleOrderReservations($orderId);
    }
    var_dump($orderId);
}