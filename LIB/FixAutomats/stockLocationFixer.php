<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 10.10.18
 * Time: 12:46
 */
namespace LIB\FixAutomats;

use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\StockLocation;
use LIB\EntityManager\Entites\StockLocationOrdersItemsListsItems;
use magazine\HighLevelStock;

class stockLocationFixer {


    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct(\DatabaseManager $pDbMgr) {

        $this->pDbMgr = $pDbMgr;
    }

    /**
     * @param $productId
     */
    public function fixProductStockLocation($productId){

        $stockLocationFixer = [];
        $this->pDbMgr->BeginTransaction('profit24');
        // sprawdzamy ile mamy na stocku
        $productStockActQuantity = $this->getProductStockActQuantity($productId);
        $stockLocationActQuantity = 0;

        // przyjmujemy że uszkodzone mogą być tylko dostawy na których są jakieś ilości
        $stockLocations = $this->getStockLocations($productId);
        foreach ($stockLocations as $stockLocation) {
            $stockLocationActQuantity += $stockLocation['available'];
        }

        if ($productStockActQuantity > $stockLocationActQuantity) {
            $missingQuantity = $productStockActQuantity - $stockLocationActQuantity;
            foreach ($stockLocations as $stockLocation) {
                // sprawdź mi ile mogę tutaj zrzucić
                $stockLocationOrderItems = $this->getCanRepairLocation($stockLocation);
                $quantityToFix = 0;
                foreach ($stockLocationOrderItems as $stockLocationOrderItem) {
                    $quantityToFix += $stockLocationOrderItem['reserved_quantity'];
                }
                if ($quantityToFix >= $missingQuantity) {
                    $stockLocationToFix[$stockLocation['id']] = $stockLocationOrderItems;
                }

            }
        }

        $stockLocationObj = new StockLocation($this->pDbMgr);
        if (!empty($stockLocationToFix)) {
            foreach ($stockLocationToFix as $sl) {
                // pozostałe parametry nie mają znaczenia
                $stockLocationObj->unreserveStockLocationsOrdersItems($sl, 0);
            }
        }


        $this->pDbMgr->CommitTransaction('profit24');
        return $stockLocationToFix;
    }

    /**
     * @param $stockLocation
     * @return array
     */
    private function getCanRepairLocation($stockLocation) {

        $sSql = 'SELECT SLOILI.*
                 FROM stock_location_orders_items_lists_items AS SLOILI
                 JOIN orders_items_lists_items AS OILI
                  ON SLOILI.orders_items_lists_items_id = OILI.id
                 JOIN orders_items AS OI
                  ON OILI.orders_items_id = OI.id AND OI.status = "4"
                 JOIN orders AS O
                  ON OI.order_id = O.id 
                  AND O.order_status = "4" 
                  AND O.erp_export IS NOT NULL 
                  AND O.erp_send_ouz IS NOT NULL
                 WHERE SLOILI.stock_location_id = '.$stockLocation['id'].'
                    AND SLOILI.type = "'.StockLocationOrdersItemsListsItems::TYPE_MM.'"
                    AND SLOILI.reserved_quantity > 0 
                    AND SLOILI.reservation_subtracted = "0"
                 ORDER BY SLOILI.id DESC
                 FOR UPDATE
                     ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * @param $productId
     * @return array
     */
    private function getStockLocations($productId) {
        $sSql = 'SELECT * 
                 FROM stock_location
                 WHERE products_stock_id = '.$productId.'
                 FOR UPDATE ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @param $stockLocationId
     * @return array
     */
    private function getStockLocationData($stockLocationId) {
        $sSql = 'SELECT * 
                 FROM stock_location 
                 WHERE id = '.$stockLocationId.'
                 FOR UPDATE';
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }

    /**
     * @param $productId
     * @return mixed
     */
    private function getProductStockActQuantity($productId)
    {
        $sSql = 'SELECT profit_g_act_stock
                 FROM products_stock
                 WHERE id = '.$productId.'
                 FOR UPDATE ';
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }
}