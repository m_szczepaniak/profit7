<?php

use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/FixAutomats';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$sql = "
UPDATE `products` AS P
LEFT JOIN products_news AS PN
ON PN.product_id = P.id
SET P.is_news = '0'
WHERE P.is_news = '1' AND P.is_previews = '1'
";
$pDbMgr->Query('profit24', $sql);


$sql = 'UPDATE
products, products_shadow
SET
products_shadow.is_news = products.is_news
WHERE
products_shadow.id=products.id
AND products_shadow.is_news <> products.is_news
';

$pDbMgr->Query('profit24', $sql);