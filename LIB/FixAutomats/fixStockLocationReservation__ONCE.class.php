<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-07
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
use LIB\EntityManager\Entites\StockLocation;

ini_set('display_errors', 'On');

$aConfig['config']['project_dir'] = 'LIB/FixAutomats';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
require_once($_SERVER['DOCUMENT_ROOT'] . 'LIB/autoloader.php');

global $pDbMgr;

$stockLocation = new StockLocation($pDbMgr);


$sSql = '
SELECT PS.id
FROM products_stock AS PS
WHERE PS.stock_act_stock <> IFNULL((SELECT SUM(quantity) FROM stock_location AS SL WHERE SL.products_stock_id = PS.id), 0);
';
// usunięte/zmienione źródło
$data = $pDbMgr->GetCol('profit24', $sSql);
foreach ($data as $productId) {
    echo $productId.':';
    $stockLocation->recountProductStock(null, $productId);
}
