<?php

use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use Reservations\ReservationRenewer;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

//$ordersIds = [
//    673839
//];

//$ordersIds = [382860, 382836];
//
//$sql = 'SELECT DISTINCT O.id
//FROM products_stock_reservations AS PSR
//JOIN orders AS O
//ON O.id = PSR.order_id AND O.order_status = "1"
//WHERE PSR.source_id = 10 AND streamsoft_lp IS NULL';

//$ordersIds = array_unique($ordersIds);

$sql = "
    select DISTINCT OI.order_id from orders_items AS OI
    JOIN orders AS O ON O.id = OI.order_id AND (O.order_status = '1' OR O.order_status = '0' OR O.order_status = '-1')
    WHERE OI.status = '3' AND OI.source = '51'
    AND ( select count(id) from products_stock_reservations where orders_items_id = OI.id ) > 1
            AND
            (
                SELECT count(OI2.id)
                FROM orders_items AS OI2
                WHERE OI2.source = '51' AND
                OI2.status = '4' AND
                OI2.order_id = O.id
            ) = 0
    LIMIT 50
    ";

//$ordersIds = $pDbMgr->GetCol('profit24', $sql);

$ordersIds = [
    1132782
];




//1134379,
//1134383,
//1134444,
//1134451,
//1134458,
//1134477,
//1134488,
//1134491,
//1134521,
//1134538,
//1134578,
//1134587,
//1134624,
//1134635,
//1134640,
//1134644,
//1134649,
//1134681,
//1134714,
//1134723,
//1134726,
//1134728,
//1134731,
//1134753,
//1134757,
//1134758,
//1134759,
//1134763,
//1134779,
//1134790,
//1134799,
//1134809,
//1134815,
//1134818,
//1134935,
//1135026,
//1135029,
//1135057,
//1135061,
//1135249,
//1135287,
//1135288,

foreach($ordersIds as $orderId) {

    $reservationRenewer = new ReservationRenewer($pDbMgr, new ReservationManager($pDbMgr), new DataProvider($pDbMgr));

    $result = $reservationRenewer->repairSingleOrderReservations($orderId);

    if (false == $result){
        $sMsg = "Wystąpił błąd podczas odtwarzania rzerwacji";
    } else if(true == is_string($result)){
        $sMsg = $result;
    } else {
        $sMsg = "Rezerwacje zostały odbudowane";
    }

    var_dump($sMsg. ' orderId: '.$orderId);
}
