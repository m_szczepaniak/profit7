<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;
use Reservations\OrderRepairer2;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

//$file = file_get_contents('reservations/res.txt', 'r');

$date = strtotime("-1 day", time());
$formated = date('Y-m-d 00:00:00', $date);


$quantity = 260;
//$quantity = 1;

$ordersToRepair = $pDbMgr->GetAssoc('profit24', "SELECT id, order_number FROM fix_reservation_buffer ORDER BY `type` LIMIT $quantity");
$ordersToRepair = array_unique($ordersToRepair);

if (empty($ordersToRepair) || count($ordersToRepair) < 1) {
    var_dump('Brak zamowien do naprawy');die;
}

var_dump($res1);
var_dump($res2);

$orderClass = new Order($pDbMgr);

foreach($ordersToRepair as $iid => $order) {

    var_dump($order);

    try {
        $orderRepair = new OrderRepairer2($order, $orderClass);
        $orderRepair->fixOrder();

        $pDbMgr->Query('profit24', "DELETE FROM fix_reservation_buffer WHERE id = $iid LIMIT 1");

    } catch(\Exception $e) {
        var_dump($e->getMessage());
        continue;
    }
}
