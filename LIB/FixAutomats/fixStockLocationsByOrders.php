<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;


function getQuantityPZ($stockLocationId) {
    global $pDbMgr;

    $sSql = 'SELECT SUM(reserved_quantity)
             FROM stock_location_orders_items_lists_items 
             WHERE stock_location_id = '.$stockLocationId.' AND type = "PZ" ';
    return $pDbMgr->GetOne('profit24', $sSql);
}

$a = \LIB\EntityManager\Entites\OrdersItemsLists::HIGH_STOCK_LEVEL_DOCUMENT_TYPE;
$sSql = '
SELECT SLO.stock_location_id, SL.container_id, SL.products_stock_id, SUM(OILI.quantity) as reserved_quantity, SL.quantity, SL.available, SL.reservation 
         FROM stock_location_orders_items_lists_items AS SLO
         LEFT JOIN stock_location AS SL
          ON SLO.stock_location_id = SL.id AND SLO.stock_location_magazine_type = SL.magazine_type
         JOIN orders_items_lists_items AS OILI
          ON SLO.orders_items_lists_items_id = OILI.id
         JOIN orders_items_lists AS OIL
          ON OILI.orders_items_lists_id = OIL.id AND OIL.type <> "8"
         JOIN orders_items AS OI 
          ON OILI.orders_items_id = OI.id AND OI.deleted = "0" AND OI.source = "51"
         JOIN orders AS O
          ON OI.order_id = O.id AND O.erp_send_ouz IS NOT NULL AND O.erp_export >= "2018-01-29"
         WHERE SLO.reservation_subtracted = "0"

        ';
$data = $pDbMgr->GetAll('profit24', $sSql);
$counter = [];
foreach ($data as $item) {

}
