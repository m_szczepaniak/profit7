<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

include_once('simple_html_dom.php');

$allData = [];



for($i = 42; $i < 43; $i++) {

    var_dump($i);
    $html = file_get_html('http://www.lex.pl/baza-teleadresowa/-/adreson/sady/Polska/'.$i);

//    foreach($html->find('.lex-list-buttons li span.address') as $e) {
    foreach($html->find('.lex-list-buttons li') as $e) {

        $first = trim($e->find('h1 a')[0]->innertext);

        $second = trim(preg_replace('/<span[^>]*>.*?<\/span>/i', '', $e->find('span.address')[0]->innertext));

        $elem = $first.': '.$second;
        $elem = iconv('UTF-8', 'CP1250', $elem);
        $allData[] = $elem;
    }

}

$allData = array_unique($allData);

$string = '';

foreach($allData as $singleElement) {

    $string .= $singleElement."\r\n";
}

$ddd = '';