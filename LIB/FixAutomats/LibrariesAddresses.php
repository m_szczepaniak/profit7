<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');
ini_set('max_execution_time', 15000);


$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

include_once('simple_html_dom.php');

$allData = [];

function array2csv2(array &$array, $filename)
{
    $headers = [
        'adres',
        'mail',
    ];

    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen($filename, 'w');
    fputcsv($df, $headers);
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}

for($i = 1; $i < 256; $i++) {

    var_dump($i);
    $html = file_get_html('http://www.sbp.pl/o_bibliotekach/baza_bibliotek?security=false&biblioteka_nazwa=&biblioteka_wojewodztwo=&biblioteka_typ=1&biblioteka_miejscowosc=&biblioteka_system=0&biblioteka_strona_www=-1&szukaj=1&_page='.$i);

//    foreach($html->find('.lex-list-buttons li span.address') as $e) {
    foreach($html->find('.content .content-border') as $e) {

        $link = '';

        preg_match("/(?<=href=(\"|'))[^\"']+(?=(\"|'))/", $e->innertext, $link);

        $link = "http://www.sbp.pl".$link[0];

        $linkHTML = file_get_html($link);
        $mail = $linkHTML->find('#content')[0]->find('a[href^="mailto:"]')[0]->innertext;

        $elementHtml = preg_replace('~<a(.*?)</a>~Usi', "", $e->innertext);
        $elementHtml = strip_tags($elementHtml);
        $elementHtml = trim(iconv('UTF-8', 'CP1250', $elementHtml));
        $elementHtml = preg_replace('!\s+!', ' ', $elementHtml);
//        $elementHtml .= ' - '.$mail;

        $parsedSite = [
            'address' => str_replace(['"', ',', ';'], '', $elementHtml),
            'mail' => str_replace(['"', ',', ';'], '', $mail),
        ];

        $allData[] = [
            'address' => $elementHtml,
            'mail' => $mail
        ];

        $cnt = '';

//        if ($i == 1) {
//            $cnt .= "\r\n".$cnt;
//        }

//        $cnt .= utf8_encode(implode(',', $parsedSite))."\r\n";
//        $cnt .= iconv('ISO-8859-2', 'UTF-8', implode(',', $parsedSite))."\r\n";
        $cnt .= implode(';', $parsedSite)."\r\n";

        file_put_contents('publiczne.csv', $cnt, FILE_APPEND);
    }
    var_dump($i);
}

$allData = array_unique($allData);

$string = '';

foreach($allData as $singleElement) {

//    $string .= $singleElement."\r\n";
//    array2csv2($singleElement, 'szkoly.csv');
}

$ddd = '';