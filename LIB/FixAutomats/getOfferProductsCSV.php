<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

/**
 * @var DatabaseManager
 */
global $pDbMgr;

function getProductType($productType) {
    switch ($productType) {
        case 'K':
            return "Książka";
            break;
        case 'Z':
            return "Zabawka";
            break;
        case 'G':
            return "Gra";
            break;
        case 'L':
            return "LEGO";
            break;
        case 'P':
            return "Puzzle";
            break;
        case '3':
            return "Artykuły szkolne i biurowe";
            break;
        case '4':
            return "Dom i ogród";
            break;
        case '5':
            return "Dziecko";
            break;
        case '6':
            return "Ebook";
            break;
        case '7':
            return "Elektronika";
            break;
        case '8':
            return "Film";
            break;
        case '9':
            return "Gadżety i galanteria";
            break;
        case 'J':
            return "Gra komputerowa";
            break;
        case 'M':
            return "Muzyka";
            break;
        case 'S':
            return "Sport i turystyka";
        case 'H':
            return "Zdrowie i uroda";
            break;
        case 'A':
            return "Audiobook";
            break;
        default:
            return "Książka";
            break;
    }
}

$sSql = ' SELECT P.id, streamsoft_indeks, P.name, isbn, isbn_plain, isbn_13, isbn_10, ean_13, P.vat, 
          P.created_by, MI.name AS category_name, product_type, P.created, publication_year, 
          pages, GROUP_CONCAT(CONCAT(PA.name, " ", PA.surname) SEPARATOR \';\') as author, PP.name as publisher, CONCAT(PI.directory, "/", PI.photo) as image, PD.dimension, PS.profit_g_act_stock, P.description
          FROM products P 
          LEFT JOIN menus_items MI ON P.main_category_id = MI.id 
          LEFT JOIN products_to_authors PTA on P.id = PTA.product_id 
          LEFT JOIN products_authors PA on PTA.author_id = PA.id
          LEFT JOIN products_publishers PP on P.publisher_id = PP.id
          LEFT JOIN products_images PI on P.id = PI.product_id
          LEFT JOIN products_dimensions PD on P.dimension = PD.id
          LEFT JOIN products_stock PS on P.id = PS.id
          GROUP BY P.id';

$products = $pDbMgr->PlainQuery('profit24', $sSql);

if ($products !== null && $products !== false) {
    $profitProducts = fopen($_SERVER['DOCUMENT_ROOT'].'/profit_products.csv', 'w');
    fputcsv($profitProducts, [
        'id',
        'streamsoft_indeks',
        'name',
        'isbn',
        'isbn_plain',
        'isbn_13',
        'isbn_10',
        'ean_13',
        'vat',
        'created_by',
        'category_name',
        'product_type',
        'created',
        'publication_year',
        'pages',
        'author',
        'publisher',
        'image',
        'dimension',
        'amount',
        'description'
    ]);
    while ($product =& $products->fetchRow(DB_FETCHMODE_ASSOC)) {
        $product['product_type'] = getProductType($product['product_type']);
        $product['image'] = 'https://www.profit24.pl/'.$aConfig['common']['photo_dir'].'/'.$product['image'];
        $tmpStr = str_replace("\n", '', $product['description']);
        $tmpStr = str_replace("\r", '', $tmpStr);
        $tmpStr = str_replace("^M", '', $tmpStr);
        $product['description'] = htmlspecialchars($tmpStr);
        fputcsv($profitProducts, $product);
    }
    fclose($profitProducts);
}
