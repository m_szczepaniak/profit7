<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;
use orders\Shipment;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$sql = "
select PT.id
from products_tarrifs AS PT
join products AS P ON P.id = PT.product_id
where PT.price_brutto = P.price_brutto
AND PT.type = 1
";

$cod = 'mestro';

$orders = $pDbMgr->GetAll($cod, $sql);
var_dump(count($orders));

foreach($orders as $order) {

    $id = $order['id'];
    $pDbMgr->Query($cod, "DELETE from products_tarrifs where id = $id limit 1");
}