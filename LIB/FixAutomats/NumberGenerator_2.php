<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;
//$number = '7000001000';
$number = '7000200000';
$prefix = '70002';
$postfix = '000';

$numbers = [];
for ($i = 1; $i < 91; $i++) {
    $iteration = sprintf("%1$02d", $i);
    $number_iterator = $prefix.$iteration.$postfix;
    $numbers[] = $number_iterator;
    $values = [
        'id' => $number_iterator,
        'locked' => '0',
        'type' => '0',
        'item_id' => 'NULL'
    ];
    if (false === $pDbMgr->Insert('profit24', 'containers', $values)) {
        dump($values);
        echo 'BABOŁ ';
    }
    echo $number_iterator."\n";
}