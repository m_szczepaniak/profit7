<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 21.02.18
 * Time: 13:19
 */

ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$sSql = 'SELECT DISTINCT products_stock_id FROM stock_location';
$stockLocationProductsStockIds = $pDbMgr->GetCol('profit24', $sSql);

$sSql = 'SELECT DISTINCT id 
          FROM products_stock 
          WHERE 
          profit_g_act_stock > 0 OR profit_g_reservations > 0 OR profit_g_status > 0 ';
$productsStockIds = $pDbMgr->GetCol('profit24', $sSql);
$items = array_unique(array_merge($stockLocationProductsStockIds, $productsStockIds));



$stockLocation = new \LIB\EntityManager\Entites\StockLocation($pDbMgr);
foreach ($items as $productId) {
    dump($productId);
    $stockLocation->recountProductStock(null, $productId);
}
