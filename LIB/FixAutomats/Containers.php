<?php

use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

function csvs($string, $skip_rows = 0, $separatorChar = ',', $enclosureChar = '"', $newlineChar = "\n")
{
    // @author: Klemen Nagode
    // @source: http://stackoverflow.com/questions/1269562/how-to-create-an-array-from-a-csv-file-using-php-and-the-fgetcsv-function
    $array = array();
    $size = strlen($string);
    $columnIndex = 0;
    $rowIndex = 0;
    $fieldValue="";
    $isEnclosured = false;
    for($i=0; $i<$size;$i++) {

        $char = $string{$i};
        $addChar = "";

        if($isEnclosured) {
            if($char==$enclosureChar) {

                if($i+1<$size && $string{$i+1}==$enclosureChar){
                    // escaped char
                    $addChar=$char;
                    $i++; // dont check next char
                }else{
                    $isEnclosured = false;
                }
            }else {
                $addChar=$char;
            }
        }else {
            if($char==$enclosureChar) {
                $isEnclosured = true;
            }else {

                if($char==$separatorChar) {

                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";

                    $columnIndex++;
                }elseif($char==$newlineChar) {
                    echo $char;
                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";
                    $columnIndex=0;
                    $rowIndex++;
                }else {
                    $addChar=$char;
                }
            }
        }
        if($addChar!=""){
            $fieldValue.=$addChar;

        }
    }

    if($fieldValue) { // save last field
        $array[$rowIndex][$columnIndex] = $fieldValue;
    }


    /**
     * Skip rows.
     * Returning empty array if being told to skip all rows in the array.
     */
    if ($skip_rows > 0) {
        if (count($array) == $skip_rows)
            $array = array();
        elseif (count($array) > $skip_rows)
            $array = array_slice($array, $skip_rows);

    }

    return $array;
}

//$arrayToProceed = csvs(file_get_contents('csv.csv'));

$files = glob("csv/*.csv");
$i = 0;
foreach($files as $file) {
    $arrayToProceed = csvs(file_get_contents($file));

    if(empty($arrayToProceed)) {
        continue;
    }

    foreach($arrayToProceed as $element) {
        $id = $element[0];
        $type = getTypes($id);

        $el = $pDbMgr->GetOne('profit24', "SELECT id FROM containers WHERE id = $id");

        if (!empty($el)) {
            continue;
        }
        $i++;
        $values = [
            'id' => $id,
            'type' => $type,
            'locked' => 0
        ];

        $pDbMgr->Insert('profit24', 'containers', $values);
    }
}


var_dump($i);
function getTypes($element)
{
    $type = substr($element, 0, 2);

    switch ($type) {
        case 15:
            return 2;
            break;
        case 20:
            return 1;
            break;
        case 11:
            return 3;
            break;
    }
}