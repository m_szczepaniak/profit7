<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;


function getQuantityPZ($stockLocationId) {
    global $pDbMgr;

    $sSql = 'SELECT SUM(reserved_quantity)
             FROM stock_location_orders_items_lists_items 
             WHERE stock_location_id = '.$stockLocationId.' AND type = "PZ" ';
    return $pDbMgr->GetOne('profit24', $sSql);
}

function getQuantityMMminus($stockLocationId) {
    global $pDbMgr;

    $sSql = 'SELECT SUM(SLO.reserved_quantity)
             FROM stock_location_orders_items_lists_items AS SLO
             JOIN orders_items_lists_items AS OILI
              ON SLO.orders_items_lists_items_id = OILI.id
             JOIN orders_items_lists AS OIL
              ON OILI.orders_items_lists_id = OIL.id AND OIL.type = "8"
             WHERE SLO.stock_location_id = '.$stockLocationId.' AND SLO.type = "MM" ';
    return $pDbMgr->GetOne('profit24', $sSql);
}


$a = \LIB\EntityManager\Entites\OrdersItemsLists::HIGH_STOCK_LEVEL_DOCUMENT_TYPE;
//SLO.reserved_quantity
$sSql = 'SELECT SL.magazine_type, SLO.stock_location_id, SL.container_id, SL.products_stock_id, SUM(OILI.quantity) as reserved_quantity, SL.quantity, SL.available, SL.reservation, O.order_number, OI.isbn
         FROM stock_location_orders_items_lists_items AS SLO
         LEFT JOIN stock_location AS SL
          ON SLO.stock_location_id = SL.id AND SLO.stock_location_magazine_type = SL.magazine_type
         JOIN orders_items_lists_items AS OILI
          ON SLO.orders_items_lists_items_id = OILI.id AND confirmed = "1"
         JOIN orders_items_lists AS OIL
          ON OILI.orders_items_lists_id = OIL.id AND OIL.type <> "8"
         JOIN orders_items AS OI 
          ON OILI.orders_items_id = OI.id AND OI.deleted = "0" AND OI.source = "51"
         JOIN orders AS O
          ON OI.order_id = O.id AND O.erp_send_ouz IS NOT NULL AND DATE(O.erp_export) >= "2018-01-29"
         GROUP BY SLO.stock_location_id, OILI.product_id
        ';
//WHERE SL.products_stock_id = 982657
$data = $pDbMgr->GetAll('profit24', $sSql);
$counter = [];
foreach ($data as $item) {
//    $sSql = 'SELECT id FROM sell_predict WHERE product_id = '.$item['products_stock_id'];
//    $iID = $pDbMgr->GetOne('profit24', $sSql);
//    if ($iID > 0) {
//        continue;
//    }

    $sSql = 'SELECT II.quantity FROM inventory_items AS II
             JOIN inventory AS I
              ON II.inventory_id = I.id AND I.container_id = "'.$item['container_id'].'"
             WHERE 
              product_id = '.$item['products_stock_id'].' LIMIT 1';
    $quantityOnInventory = $pDbMgr->GetOne('profit24', $sSql);
    if (intval($quantityOnInventory) == 0) {
        continue;
    }
    $quantityAllRecounted = $quantityOnInventory - $item['reserved_quantity'];

    // trzeba uwzględnić PZ

    $quantityPZ = getQuantityPZ($item['stock_location_id']);
    $quantityAllRecounted += $quantityPZ;

    $quantityMMminus = getQuantityMMminus($item['stock_location_id']);
    $item['mm_minus'] = $quantityMMminus;
    if ($item['magazine_type'] == 'SL') {
        $quantityAllRecounted += $quantityMMminus;
    } else {
        $quantityAllRecounted -= $quantityMMminus;
    }



    if ($item['stock_location_id'] == '63407') {
        echo 'bbb:';
        print_r($quantityOnInventory);
        echo ':';
        print_r($quantityPZ);
        echo ':';
        print_r($quantityMMminus);
        echo ':';
        print_r($quantityAllRecounted);
        echo ':';
        dump($item);
    }

    if (intval($quantityAllRecounted) > ($item['quantity'])) {

//        // porównujemy stock
//        $sSql = 'SELECT profit_g_act_stock
//                 FROM products_stock
//                 WHERE id = '.$item['products_stock_id'];
//        $erpQuantity = $pDbMgr->GetOne('profit24', $sSql);
//        $sSql = '
//        SELECT sum(quantity)
//        FROM stock_location
//        WHERE products_stock_id = '.$item['products_stock_id'];
//        $stockLocationSumQuantity = $pDbMgr->GetOne('profit24', $sSql);
//
//        if ($erpQuantity <> $stockLocationSumQuantity) {
            $item['quantity_from_inventory'] = $quantityAllRecounted;
            $item['diff'] = $quantityAllRecounted - $item['quantity'];
            $counter[$item['products_stock_id']][] = $item;
//        }

    }
}
foreach ($counter as $item) {
    if (count($item) == 1) {
        $itemToUpdate=$item[0];
        echo "\r\n".'do naprostowania:';
        print_r($item[0]);
        /*
         * NAPRAWIACZ:
        $sSql = '
        UPDATE stock_location
        SET 
          available = available + '.$itemToUpdate['diff'].',
          quantity = quantity + '.$itemToUpdate['diff'].'
        WHERE id = '.$itemToUpdate['stock_location_id'].'
        LIMIT 1';
        print_r($sSql);
        $pDbMgr->Query('profit24', $sSql);
        */

    } else {
//        echo "\r\n".'za dużo niejasności:';
//        print_r($item);
    }

}
print_r($counter);

/**
 *
DUPLIKATY NA LIŚCIE:
 *
SELECT *, count(id)
FROM `stock_location_orders_items_lists_items`
WHERE type = 'MM'
GROUP BY orders_items_lists_items_id
HAVING count(id) > 1
ORDER BY `stock_location_orders_items_lists_items`.`type` DESC

 */