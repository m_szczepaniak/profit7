<?php

use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$prefix = "_e_";

$sql = "
SELECT *
FROM `products_stock`
WHERE profit".$prefix."act_stock != ( profit".$prefix."status + profit".$prefix."reservations )
";
//var_dump($sql);
$res = $pDbMgr->GetAll('profit24', $sql);

$sql2 = "UPDATE products_stock set profit".$prefix."act_stock = %d WHERE id = %d";

foreach($res as $stock) {
    $newActStock = $stock['profit'.$prefix.'reservations'] + $stock['profit'.$prefix.'status'];
    $sql3 = sprintf($sql2, $newActStock, $stock['id']);

//    var_dump($sql3);die;
    $pDbMgr->Query('profit24', $sql3);
}