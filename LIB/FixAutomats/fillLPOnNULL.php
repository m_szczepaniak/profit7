<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 09.12.16
 * Time: 11:42
 */

ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');


$sSql = 'SELECT O.id
          FROM products_stock_reservations AS PSR
          JOIN orders AS O 
            ON O.id = PSR.order_id AND O.order_status = "1"
          WHERE PSR.source_id = 10 AND streamsoft_lp IS NULL
          
         ';