<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$dir = 'pliki';
//
//$file = file_get_contents('pliki/res.txt', 'r');
$arrayToProceed = csvs(file_get_contents('csv/transakcje.csv'));

global $pDbMgr;

$statuses = [
    4 => 'Zrealizowane',
    5 => 'Anulowane',
    1 => 'W realizacji',
    2 => 'Skompletowane',
    3 => 'Zatwierdzone',
];

$data = [];
//var_dump($arrayToProceed);die;
foreach($arrayToProceed as $element) {
    
    $element = str_replace(["\r", "\n"], '', $element[0]);

    if (strlen($element) == 11) {

        $element = '0'.$element;
    }
//var_dump($element);die;
    $elementStatus = $pDbMgr->GetOne('profit24', "SELECT order_status FROM orders WHERE order_number = '$element'");
    $mail = $pDbMgr->GetOne('profit24', "SELECT email FROM orders WHERE order_number = '$element'");
	$order_date = $pDbMgr->GetOne('profit24', "SELECT order_date FROM orders WHERE order_number = '$element'");

    $elementStatus = $statuses[$elementStatus];

    $data[] = [
        'oid' => $element,
        'status' => $elementStatus,
        'email' => $mail,
		'order_date' => $order_date
    ];
}

usort($data, function ($item1, $item2) {
    if ($item1['status'] == $item2['status']) return 0;
    return $item1['status'] < $item2['status'] ? -1 : 1;
});

array2csv2($data, 'csv/transakcje_2.csv');




function array2csv2(array &$array, $filename)
{
    $headers = [
        'id',
        'status',
        'mail',
		'data zamowienia'
    ];

    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen($filename, 'w');
    fputcsv($df, $headers);
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}



function findProductByIdentssss($sProdIdent) {

    global $pDbMgr;

    $sSql = 'SELECT PS.profit_g_location, PS.profit_x_location, INV.container_id as rem_loc
             FROM products AS P
             JOIN products_stock AS PS
              ON P.id = PS.id
             LEFT JOIN inventory_items AS II ON P.id = II.product_id
             LEFT JOIN inventory AS INV ON II.inventory_id = INV.id
             WHERE
                (
                  P.isbn_plain = "%1$s" OR
                  P.isbn_10 = "%1$s" OR
                  P.isbn_13 = "%1$s" OR
                  P.ean_13 = "%1$s"
                )'; // OR streamsoft_indeks = "%1$s"
    $sSql = sprintf($sSql, $sProdIdent);
    return $pDbMgr->GetRow('profit24', $sSql);
}


function csvs($string, $skip_rows = 0, $separatorChar = ',', $enclosureChar = '"', $newlineChar = "\n")
{
    // @author: Klemen Nagode
    // @source: http://stackoverflow.com/questions/1269562/how-to-create-an-array-from-a-csv-file-using-php-and-the-fgetcsv-function
    $array = array();
    $size = strlen($string);
    $columnIndex = 0;
    $rowIndex = 0;
    $fieldValue="";
    $isEnclosured = false;
    for($i=0; $i<$size;$i++) {

        $char = $string{$i};
        $addChar = "";

        if($isEnclosured) {
            if($char==$enclosureChar) {

                if($i+1<$size && $string{$i+1}==$enclosureChar){
                    // escaped char
                    $addChar=$char;
                    $i++; // dont check next char
                }else{
                    $isEnclosured = false;
                }
            }else {
                $addChar=$char;
            }
        }else {
            if($char==$enclosureChar) {
                $isEnclosured = true;
            }else {

                if($char==$separatorChar) {

                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";

                    $columnIndex++;
                }elseif($char==$newlineChar) {
                    echo $char;
                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";
                    $columnIndex=0;
                    $rowIndex++;
                }else {
                    $addChar=$char;
                }
            }
        }
        if($addChar!=""){
            $fieldValue.=$addChar;

        }
    }

    if($fieldValue) { // save last field
        $array[$rowIndex][$columnIndex] = $fieldValue;
    }


    /**
     * Skip rows.
     * Returning empty array if being told to skip all rows in the array.
     */
    if ($skip_rows > 0) {
        if (count($array) == $skip_rows)
            $array = array();
        elseif (count($array) > $skip_rows)
            $array = array_slice($array, $skip_rows);

    }

    return $array;
}
