<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$dir = 'pliki';
//
//$file = file_get_contents('pliki/res.txt', 'r');

$files = glob("pliki/*.csv");


foreach($files as $f) {

    $arrayToProceed = csvs(file_get_contents($f));
    unset($arrayToProceed[0]);

    $newItems = [];

    foreach($arrayToProceed as $item) {

        $isbn = $item[0];

        $loc = findProductByIdentssss($isbn);

        $data['index'] = $isbn;
        $data['nazwa'] = $item[1];
        $data['ilosc'] = $item[2];
        $data['g_loc'] = $loc['profit_g_location'];
        $data['x_loc'] = $loc['profit_x_location'];
        $data['rem_loc'] = $loc['rem_loc'];

        $newItems[] = $data;
    }

    array2csv2($newItems, 'pliki/new/'.str_replace('pliki/', '',$f));
}




function array2csv2(array &$array, $filename)
{
    $headers = [
        'indeks',
        'nazwa',
        'ilosc',
        'lokalizacja 1',
        'lokalizacja 2',
        'lokalizacja rem.',
    ];

    if (count($array) == 0) {
        return null;
    }
    ob_start();
    $df = fopen($filename, 'w');
    fputcsv($df, $headers);
    foreach ($array as $row) {
        fputcsv($df, $row);
    }
    fclose($df);
    return ob_get_clean();
}



function findProductByIdentssss($sProdIdent) {

    global $pDbMgr;

    $sSql = 'SELECT PS.profit_g_location, PS.profit_x_location, INV.container_id as rem_loc
             FROM products AS P
             JOIN products_stock AS PS
              ON P.id = PS.id
             LEFT JOIN inventory_items AS II ON P.id = II.product_id
             LEFT JOIN inventory AS INV ON II.inventory_id = INV.id
             WHERE
                (
                  P.isbn_plain = "%1$s" OR
                  P.isbn_10 = "%1$s" OR
                  P.isbn_13 = "%1$s" OR
                  P.ean_13 = "%1$s"
                )'; // OR streamsoft_indeks = "%1$s"
    $sSql = sprintf($sSql, $sProdIdent);
    return $pDbMgr->GetRow('profit24', $sSql);
}


function csvs($string, $skip_rows = 0, $separatorChar = ',', $enclosureChar = '"', $newlineChar = "\n")
{
    // @author: Klemen Nagode
    // @source: http://stackoverflow.com/questions/1269562/how-to-create-an-array-from-a-csv-file-using-php-and-the-fgetcsv-function
    $array = array();
    $size = strlen($string);
    $columnIndex = 0;
    $rowIndex = 0;
    $fieldValue="";
    $isEnclosured = false;
    for($i=0; $i<$size;$i++) {

        $char = $string{$i};
        $addChar = "";

        if($isEnclosured) {
            if($char==$enclosureChar) {

                if($i+1<$size && $string{$i+1}==$enclosureChar){
                    // escaped char
                    $addChar=$char;
                    $i++; // dont check next char
                }else{
                    $isEnclosured = false;
                }
            }else {
                $addChar=$char;
            }
        }else {
            if($char==$enclosureChar) {
                $isEnclosured = true;
            }else {

                if($char==$separatorChar) {

                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";

                    $columnIndex++;
                }elseif($char==$newlineChar) {
                    echo $char;
                    $array[$rowIndex][$columnIndex] = $fieldValue;
                    $fieldValue="";
                    $columnIndex=0;
                    $rowIndex++;
                }else {
                    $addChar=$char;
                }
            }
        }
        if($addChar!=""){
            $fieldValue.=$addChar;

        }
    }

    if($fieldValue) { // save last field
        $array[$rowIndex][$columnIndex] = $fieldValue;
    }


    /**
     * Skip rows.
     * Returning empty array if being told to skip all rows in the array.
     */
    if ($skip_rows > 0) {
        if (count($array) == $skip_rows)
            $array = array();
        elseif (count($array) > $skip_rows)
            $array = array_slice($array, $skip_rows);

    }

    return $array;
}
