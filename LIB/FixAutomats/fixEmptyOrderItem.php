<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 17.05.17
 * Time: 08:45
 */
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/FixAutomats';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;


$sql = "
    SELECT OI.id
    FROM `orders_items` AS OI
    JOIN orders AS O 
     ON O.id = OI.order_id
    WHERE
    O.order_status NOT IN ('3', '4', '5') AND
    OI.`product_id` IS NULL AND 
    OI.`name` = '' AND 
    OI.`price_netto` = 0.00 AND 
    OI.`vat` = 0 AND 
    OI.`item_type` = 'I' AND 
    OI.`parent_id` IS NULL
";
$ordersItemsIds = $pDbMgr->GetCol('profit24', $sql);

if (!empty($ordersItemsIds)) {
    foreach ($ordersItemsIds as $orderItemId) {
        $sql = 'DELETE FROM orders_items WHERE id = ' . $orderItemId.' LIMIT 1';
        $pDbMgr->Query('profit24', $sql);
    }
}

