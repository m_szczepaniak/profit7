<?php

use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\communicator\sources\Internal\streamsoft\Reservation;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacja;
use LIB\communicator\sources\Internal\streamsoft\WS\wsRezerwacjeResult;
use Reservations\ReservationRenewer;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;


function updateReservationLp($newPos, $reservation, DataProvider $dataProvider)
{
    $test = $reservation->getValue();
    $isInstanceof = $reservation instanceof wsRezerwacjeResult;
    if (false == $isInstanceof || empty($test)){
        return;
    }

    foreach($reservation->getValue() as $reservationItem){

        if (null == $reservationItem){
            continue;
        }

        foreach($newPos as &$element) {

            if ($element['proceed'] === true) {

                continue;
            }

            if ($reservationItem->getIndeks() == $element['streamsoft_indeks']) {

                if(false === $dataProvider->updateRservationLp($element['id'], $reservationItem->getLp())){
//                    throw new \Exception('blad podczas aktualziacji kolumny LP');
                }

                $element['proceed'] = true;
                break;
            }
        }
    }

//    var_dump('chuj2');die;
}


$orderIds = $pDbMgr->GetCol('profit24', 'SELECT DISTINCT O.id
          FROM products_stock_reservations AS PSR
          JOIN orders AS O
            ON O.id = PSR.order_id
          WHERE PSR.source_id = 10 AND streamsoft_lp IS NULL
          LIMIT 200
          ');

//var_dump($orderIds);die;

//$orderIds = [711660];
//$orderIds = [336];

$dataProvider = new DataProvider($pDbMgr);
$reservationD = new Reservation($pDbMgr);

foreach($orderIds as $orderId) {

    $order = $dataProvider->getOrder($orderId, ['O.id', 'O.website_id', 'P.id as product_id', 'O.order_number', 'O.order_number_iteration'], false, true);

    $oitems = $dataProvider->getReservationsByOrderId($order['id'], 10);

    $reservation = $reservationD->showReservation($order['streamsoft_order_number']);

    updateReservationLp($oitems, $reservation, $dataProvider);
}





















