<?php

use Common;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

/**
 * @var DatabaseManager
 */
global $pDbMgr;

$sSql = 'SELECT id, streamsoft_indeks, name, isbn, isbn_plain, isbn_13, isbn_10, ean_13 FROM products WHERE prod_status = "1" AND published = "1" AND shipment_time = "0"';

$products = $pDbMgr->PlainQuery('profit24', $sSql);

if ($products !== null && $products !== false) {
    $profitProducts = fopen($_SERVER['DOCUMENT_ROOT'].'/profit_products.csv', 'w');
    while ($product =& $products->fetchRow(DB_FETCHMODE_ASSOC)) {
        fputcsv($profitProducts, $product);
    }
    fclose($profitProducts);
}
