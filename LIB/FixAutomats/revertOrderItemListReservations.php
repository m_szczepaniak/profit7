<?php

use Common;
use LIB\communicator\sources\Internal\streamsoft\Reservation;
use Reservations\OrderRepairer;
use LIB\communicator\sources\Internal\streamsoft\Order;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');
global $pDbMgr;

$sSql = 'SELECT SLO.*
         FROM orders_items_lists_items AS OILI
         JOIN stock_location_orders_items_lists_items AS SLO
          ON OILI.id = SLO.orders_items_lists_items_id
         WHERE orders_items_lists_id = 92500';
$rows = $pDbMgr->GetAll('profit24', $sSql);

foreach ($rows as $row) {
    $quantity = $row['reserved_quantity'];
    $sSql = 'SELECT * FROM stock_location WHERE id = '.$row['stock_location_id'];
    $stockLocation = $pDbMgr->GetRow('profit24', $sSql);

    $values = [
        'reservation' => $stockLocation['reservation'] - $quantity,
        'available' => $stockLocation['available'] + $quantity,
    ];

//    var_dump($pDbMgr->Update('profit24', 'stock_location', $values, ' id = '.$row['stock_location_id'].' LIMIT 1'));
    dump($row);
    dump($stockLocation);
    dump($values);
}

