<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 29.08.17
 * Time: 14:24
 */

use LIB\FixAutomats\mappingParentChildren;

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/FixAutomats/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
$aConfig['use_db_manager'] = true;
include_once($_SERVER['DOCUMENT_ROOT'] . '/omniaCMS/config/ini.inc.php');
set_time_limit(10800); // maksymalny czas wykonywania 3 godziny -
ini_set("memory_limit", '512M');
global $pDbMgr;

$items = [
    '2646',
    '2657',
    '2656',
];

$mappingParentChildren = new mappingParentChildren($pDbMgr);
$childs = [];
foreach ($items as $row) {
    $childs[$row] = [];
    $mappingParentChildren->getChildsCategories($childs[$row], $row);

    $allMyChildIds = [];
    $mappingParentChildren->getAllMyChildIds($allMyChildIds, $childs[$row]);
    $mappingParentChildren->addMyChildsMappings($row, $allMyChildIds);
    $refChilds[$row]['child_ids'] = $allMyChildIds;
}

var_export($childs);
