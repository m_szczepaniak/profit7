<?php

namespace Listener;

use Ceneo\CeneoDataProvider;
use Common;

class TabListener
{
    private $productIds;
    private $type;
    private $pDbMgr;

    public function __construct($productIds, $type)
    {
        global $pDbMgr;

        $this->productIds = $productIds;
        $this->type = $type;
        $this->pDbMgr = $pDbMgr;
        $ceneoDataProvider = new CeneoDataProvider();
        $this->websitesData = $ceneoDataProvider->getCeneoWebsites();
    }

    public function onAddOut()
    {
        $sql = "DELETE FROM cyclic_buffor WHERE product_id = %d LIMIT 1";
        $sql1 = "DELETE FROM tarrifs_buffor WHERE product_id = %d";
        $sql2 = "DELETE FROM products_tarrifs WHERE product_id = %d AND type = 1 LIMIT 1";

        if (empty($this->productIds)){
            return false;
        }

        Common::sendMail('l.jaskiewicz@profit24.pl', 'l.jaskiewicz@profit24.pl', 'l.jaskiewicz@profit24.pl', 'ODPALONO USUWANIE W ZAKLADKACH CENEO NP', json_encode($this->productIds));

        // usuwamy z tarrifs buffor i cyclic buffor tylko z profitu bo tylk otam jest
        foreach($this->productIds as $profitId) {

            // cyclic buffor
            $res = $this->pDbMgr->Query('profit24', sprintf($sql, $profitId));

            // tarfis buffor
            $res2 = $this->pDbMgr->Query('profit24', sprintf($sql1, $profitId));
        }

        foreach($this->websitesData as $website) {
            foreach($this->productIds as $productId){


                if ($website['code'] != 'profit24') {
                    $productId = $this->pDbMgr->GetOne($website['code'], "SELECT id from products where profit24_id = $productId LIMIT 1");
                    if (empty($productId)) {
                        continue;
                    }
                }

                $this->pDbMgr->Query($website['code'], sprintf($sql2, $productId));
            }
        }

        return true;
    }
}