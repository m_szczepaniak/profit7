<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Listener;

use Elasticsearch\Profit\ElasticIndex;

class ElasticListener
{
    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct()
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;
    }

    public function onProductAuthorAdd($productId)
    {
        $elasticIndex = new ElasticIndex();
        $elasticIndex->createIndex([$productId], 1);
    }

    public function onAuthorAdd($authorId)
    {
        // TODO dorzucic aktualizacje indeksu autorow
    }

    public function onAuthorDelete($authorId)
    {
        $this->updateAllAuthorProductsInElastic($authorId);
    }

    public function onAuthorUpdate($authorId)
    {
        $this->updateAllAuthorProductsInElastic($authorId);
    }

    public function onProductLink($productIds)
    {
        $elasticIndex = new ElasticIndex();
        $elasticIndex->createIndex($productIds);
    }

    public function onProductUpdate($productId)
    {
        $elasticIndex = new ElasticIndex();
        $elasticIndex->createIndex([$productId]);
    }

    public function onPublisherUpdate($publisherId)
    {
        $this->updatePublisherProducts($publisherId);
    }

    public function onPublisherDelete($publisherId)
    {
        $this->updatePublisherProducts($publisherId);
    }



    private function updateAllAuthorProductsInElastic($authorId)
    {
        $productIds = $this->pDbMgr->GetCol('profit24', "select product_id FROM products_to_authors where author_id = $authorId");

        if (!empty($productIds)) {

            $elasticIndex = new ElasticIndex();
            $elasticIndex->createIndex($productIds);
        }
    }

    private function updatePublisherProducts($publisherId)
    {
        $productIds = $this->pDbMgr->GetCol('profit24', "select id from products where publisher_id = $publisherId");

        if (!empty($productIds)) {

            $elasticIndex = new ElasticIndex();
            $elasticIndex->createIndex($productIds);
        }
    }
}