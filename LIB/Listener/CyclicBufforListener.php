<?php

namespace Listener;

use Ceneo\ProductsBuffer;
use EntityManager\EntityManager;
use LIB\Helpers\ArrayHelper;
use PriceUpdater\Buffor\CyclicBuffor;
use Repositories\CeneoTabsRepository;

class CyclicBufforListener
{
    public function __construct()
    {
        global $aConfig;

        $this->config = $aConfig;
    }

    public function onBufforRun()
    {
        $em = new EntityManager();
        $repository = $em->getRepository('Repositories\CeneoTabsRepository', 'profit24');

        $vipProducts = $repository->findAllVipProducts(CeneoTabsRepository::CENEO_VIP_TYPE);

        if (empty($vipProducts)){
            return false;
        }

        $productsBuffer = new ProductsBuffer();
        $productsBuffer->saveToProductsBuffer($vipProducts);

        return true;
    }

    public function onBufforCreate($bufforProducts)
    {
        $bufforProducts = ArrayHelper::toKeyValues('product_id', $bufforProducts);

        $em = new EntityManager();
        $repository = $em->getRepository('Repositories\CeneoTabsRepository', 'profit24');

        $publishersProduct = $this->findPublishersProducts($this->config['website_id']);

        if (false === empty($publishersProduct)){
            $bufforProducts = $this->mergeProducts($bufforProducts, $publishersProduct);
        }

        $tabProducts = $repository->findAllByCeneoCategories($this->config['website_id'], CeneoTabsRepository::CENEO_IN_TYPE);

        if (false === empty($tabProducts)){
            $bufforProducts = $this->mergeProducts($bufforProducts, $tabProducts);
        }

        $productsToDelete = $repository->findProductsToDelete(CeneoTabsRepository::CENEO_OUT_TYPE);

        if (false === empty($productsToDelete)){
            $bufforProducts = $this->removeProducts($bufforProducts, $productsToDelete);
            $tabListener = new TabListener($productsToDelete, CeneoTabsRepository::CENEO_OUT_TYPE);
            $tabListener->onAddOut();
        }

        return array_values($bufforProducts);
    }

    private function findPublishersProducts($shopId)
    {
        global $pDbMgr;

        $sql = "
          SELECT P.id as product_id, 0 as priority FROM products_publishers AS PP
          JOIN products AS P ON P.publisher_id = PP.id
          JOIN website_ceneo_categories AS WCC ON P.main_category_id = WCC.category_id
          WHERE PP.include_publishers_products = 1
          AND P.packet = '0'
          AND P.published= '1'
          AND (P.prod_status = '1' OR P.prod_status = '3')
          AND (P.shipment_time = '1' OR P.shipment_time = '0')
        ";

        return $pDbMgr->GetAll('profit24', $sql);
    }

    private function mergeProducts($bufforProducts, $tabProducts)
    {
        foreach($tabProducts as $tabProduct){
            $bufforProducts[$tabProduct['product_id']] = $tabProduct;
        }

        return $bufforProducts;
    }

    private function removeProducts($bufforProducts, $productsToDelete)
    {
        foreach($productsToDelete as $product){
            unset($bufforProducts[$product]);
        }

        return $bufforProducts;
    }
}