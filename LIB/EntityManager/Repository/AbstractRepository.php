<?php

namespace EntityManager\Repository;

use DatabaseManager;
use EntityManager\Helper\DbHelper;

class AbstractRepository
{
    /** @var DatabaseManager  */
    protected $db = null;

    protected $conn = null;

    protected $tableName = null;

    public function findAllBy(array $args)
    {
        $cond = DbHelper::prepareConditionsColumns($args);
        $sql = "
        SELECT * FROM $this->tableName $cond
        ";

        return $this->getAll($sql);
    }

    public function findOneBy(array $args)
    {

    }

    public function insert($aValuesList, $sWhere='', $bReturnId=true)
    {
        return $this->db->Insert($this->conn, $this->tableName, $aValuesList, $sWhere, $bReturnId);
    }

    public function query($sSQL, $mParameters = array())
    {
        return $this->db->Query($this->conn, $sSQL, $mParameters);
    }

    public function getCol($sSQL, $mColumn=0, $mParameters=array())
    {
        return $this->db->GetCol($this->conn, $sSQL, $mColumn, $mParameters);
    }

    public function getAll($sql)
    {
        return $this->db->GetAll($this->conn, $sql);
    }

    public function __call($method, $args)
    {
        $args = $args[0];
        $methodData = explode(' ', $this->fromCamelCase($method));

        $methodName = 'findAllBy';

        if (true == in_array('One', $methodData)){
            $methodName = 'findOneBy';
        }

        unset($methodData[0]);
        unset($methodData[1]);
        unset($methodData[2]);

        if (true === empty($methodData)){
            throw new \Exception("Invalid args for method $method");
        }

        $newArgs = $this->prepareArgs($methodData, $args);

        return $this->$methodName($newArgs);
    }

    private function prepareArgs(array $methodData, $args)
    {
        $methodData = array_values($methodData);
        $nArgs = [];

        foreach($methodData as $key => $method) {
            $method = str_replace(' ', '_', $this->fromCamelCase($method));
            $method = strtolower($method);
            if (null === $args) {
                $data = null;
            } else {
                $data = $args[$key];
            }
            $nArgs[$method] = $data;
        }

        return $nArgs;
    }

    /**
     * @return null
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * @param null $db
     */
    public function setDb($db)
    {
        $this->db = $db;
    }

    /**
     * @return null
     */
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * @param null $conn
     */
    public function setConn($conn)
    {
        $this->conn = $conn;
    }

    private function fromCamelCase($camelCaseString) {
    $re = '/(?<=[a-z])(?=[A-Z])/x';
    $a = preg_split($re, $camelCaseString);
    return join($a, " " );
}
}