<?php

namespace EntityManager;

use DatabaseManager;
use EntityManager\Repository\AbstractRepository;

class EntityManager
{
    /** @var DatabaseManager */
    private $db;

    public function __construct()
    {
        global $pDbMgr;

        $this->db = $pDbMgr;
    }

    public function getRepository($name, $conn = null)
    {
        global $aConfig;

        if (false === class_exists($name)){
            throw new \Exception("class $name does not exists");
        }
//        var_dump($aConfig['website_id']);
        if (null === $conn){
            $conn = 'profit24';
        }

        /** @var AbstractRepository $repository */
        $repository = new $name();
        $repository->setConn($conn);
        $repository->setDb($this->db);

        return $repository;
    }
}