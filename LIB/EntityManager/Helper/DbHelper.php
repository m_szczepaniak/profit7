<?php

namespace EntityManager\Helper;

class DbHelper
{
    public static function prepareConditionsColumns(array $columns)
    {
        $string = '';

        $i = 0;
        foreach($columns as $column => $value){
            $cond = " WHERE ";
            if ($i > 0) {
                $cond = " AND ";
            }
            $string .= $cond.$column.' = '.$value;
            $i++;
        }

        return $string;
    }
}