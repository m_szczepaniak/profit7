<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 20.11.17
 * Time: 09:31
 */

namespace LIB\EntityManager\Entites;


use DatabaseManager;

class OrdersMailsSended
{
  const TYPE_NOT_SEND_ON_TIME = 1;
  const TYPE_PLEASE_REVIEW = 2;
  /**
   * @var DatabaseManager
   */
  private $pDbMgr;


  public function __construct(DatabaseManager $pDbMgr)
  {
    $this->pDbMgr = $pDbMgr;
  }

  /**
   * @param $id
   * @param $type
   * @return bool
   */
  public function add($id, $type)
  {

    $values = [
        'order_id' => $id,
        'mail_type' => $type,
        'created' => 'NOW()'
    ];
    if (false === $this->pDbMgr->Insert('profit24', 'orders_mails_sended', $values)) {
      return false;
    }
    return true;
  }

  /**
   * @param $id
   * @param $type
   * @return mixed
   */
  public function checkExists($id, $type)
  {
    $sSql = 'SELECT id 
             FROM orders_mails_sended
             WHERE 
                order_id = '.$id.' AND
                mail_type = "'.$type.'"
             ';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
}