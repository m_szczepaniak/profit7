<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 31.10.17
 * Time: 13:03
 */
namespace LIB\EntityManager\Entites;

class Containers
{

  const TYPE_CUVETTE = 0;
  const TYPE_TRAIN = 1;
  const TYPE_STOCK_A3 = 2;
  const TYPE_STOCK_B6 = 3;


  /**
   * @param $containerType
   * @return bool|mixed
   */
  public static function getStockLocationType($containerType)
  {

    $mapping = [
        self::TYPE_STOCK_A3 => Magazine::TYPE_LOW_STOCK_SUPPLIES,
        self::TYPE_STOCK_B6 => Magazine::TYPE_HIGH_STOCK_SUPPLIES
    ];
    if (isset($mapping[$containerType])) {
      return $mapping[$containerType];
    }
    return false;
  }

  /**
   * @param $containerNumber
   * @return array|null
   */
  public function findContainer($containerNumber)
  {
    global $pDbMgr;

    if (false === $container = $pDbMgr->GetRow('profit24', "SELECT * FROM containers WHERE id = $containerNumber")) {

      return null;
    }

    return $container;
  }
}
