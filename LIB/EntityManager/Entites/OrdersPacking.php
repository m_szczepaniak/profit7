<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 05.02.18
 * Time: 13:41
 */
namespace LIB\EntityManager\Entites;

use DatabaseManager;

class OrdersPacking {

    private $pDbMgr;

    public function __construct(DatabaseManager $pDbMgr)
    {

        $this->pDbMgr = $pDbMgr;
    }

    public function add($orderId, $transportNumber) {
        $values = [
            'order_id' => $orderId,
            'transport_number' => $transportNumber,
            'created' => 'NOW()',
            'created_by' => $_SESSION['user']['name']
        ];
        return $this->pDbMgr->Insert('profit24', 'orders_packing', $values);
    }

    /**
     * @param $orderId
     * @return bool
     */
    public function exist($orderId)
    {

        $sSql = 'SELECT id FROM orders_packing WHERE order_id = ' . $orderId;
        $result = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($result > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $orderId
     * @return array
     */
    public function getOrderPacking($orderId) {

        $sSql = 'SELECT * FROM orders_packing WHERE order_id = ' . $orderId;
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }


}