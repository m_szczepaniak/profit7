<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 07.11.17
 * Time: 09:27
 */

namespace LIB\EntityManager\Entites;


use DatabaseManager;

class ProductsStock
{

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   * ProductsStock constructor.
   * @param DatabaseManager $pDbMgr
   */
  public function __construct(DatabaseManager $pDbMgr)
  {

    $this->pDbMgr = $pDbMgr;
  }

  /**
   * @TODO przetestować !!
   *
   * @param $id
   * @param $sourceColumn
   * @param $quantity
   * @param $reservation
   * @return mixed
   */
  public function updateSourceQuantityReservation($id, $sourceColumn, $quantity, $reservation)
  {

    $sSql = 'UPDATE products_stock
             SET 
             '.$sourceColumn.'_act_stock = '.$quantity.',
             '.$sourceColumn.'_reservations = '.$reservation.',
             '.$sourceColumn.'_status = '.($quantity - $reservation).'
             
             WHERE id = '.$id;

    return $this->pDbMgr->Query('profit24', $sSql);
  }


  /**
   * @param $productId
   * @param $magazineLocationType
   * @return mixed
   */
  public function clearStockLocationByType($productId, $magazineLocationType) {

    $sColName = $this->getStockLocationColByType($magazineLocationType);

    $value = [
        $sColName => ''
    ];
    return $this->pDbMgr->Update('profit24', 'products_stock', $value, ' id='.$productId.' LIMIT 1');
  }

  /**
   * @param $magazineLocationType
   * @return string
   * @throws \Exception
   */
  public function getStockLocationColByType($magazineLocationType) {

    if ($magazineLocationType == Magazine::TYPE_HIGH_STOCK_SUPPLIES) {
      return 'profit_g_location';
    }
    elseif ($magazineLocationType == Magazine::TYPE_LOW_STOCK_SUPPLIES) {
      return 'profit_x_location';
    } else {
      throw new \Exception(_('Brak przekazanego typu lokalizacji'));
    }
  }
}