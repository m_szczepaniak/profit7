<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 08.08.18
 * Time: 15:11
 */

namespace LIB\EntityManager\Entites;

class OrdersPaymentReturns
{

    const TYPE_RETURN = 'Z'; // zwrot zwykły
    const TYPE_RENOUNCEMENT = 'D'; // Odstąpienie - dodaje się na dole po zrealizowaniu zamówienia
}