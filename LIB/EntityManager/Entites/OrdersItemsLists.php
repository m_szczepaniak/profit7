<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 03.11.17
 * Time: 10:52
 */


namespace LIB\EntityManager\Entites;

use DatabaseManager;

class OrdersItemsLists
{
  const INDEPENDENT_SUPPLY_DOCUMENT_TYPE = 7;

  const HIGH_STOCK_LEVEL_DOCUMENT_TYPE = 8;

  const RETURN_TYPE = 9;

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;

  public function __construct(DatabaseManager $pDbMgr)
  {
    $this->pDbMgr = $pDbMgr;
    $this->stockLocationOrdersItemsListsItems = new StockLocationOrdersItemsListsItems($pDbMgr);
  }

  /**
   * @param $productId
   * @param $quantity
   * @return array|bool
   */
  public function findOrdersItemsListsDocuments($productId, $quantity)
  {
    $supplies = [];

    $sSql = 'SELECT OIL.id AS OIL_ID, OILI.id AS OILI_ID, OILI.product_id, (OILI.quantity - OILI.confirmed_quantity) AS def_quantity,
              OILI.ean_13
             FROM orders_items_lists AS OIL
             JOIN orders_items_lists_items AS OILI
              ON OILI.orders_items_lists_id = OIL.id 
              AND OILI.confirmed = "0"
              AND OILI.product_id = "' . $productId . '"
             WHERE OIL.type = "'.self::INDEPENDENT_SUPPLY_DOCUMENT_TYPE.'" AND OIL.closed = "0"
              ';
    $aOrdersItemsLists = $this->pDbMgr->GetAll('profit24', $sSql);
    $iCurQuantity = $quantity;
    foreach ($aOrdersItemsLists as $aOrderListRow) {
      // liczymy ile podebrać żeby zebrać pełną ilość...
      $aOrderListRow['def_quantity'];
      if ($aOrderListRow['def_quantity'] >= $iCurQuantity) {
        $aOrderListRow['used_quantity'] = $iCurQuantity;
        $supplies[] = $aOrderListRow;
        $iCurQuantity -= $aOrderListRow['def_quantity'];
        break;
      } else {
        $aOrderListRow['used_quantity'] = $aOrderListRow['def_quantity'];
        $supplies[] = $aOrderListRow;
        $iCurQuantity -= $aOrderListRow['def_quantity'];
      }
    }

    if ($iCurQuantity > 0) {
      // za mało egz. w dostawach
      return false;
    } else {
      return $supplies;
    }
  }

    /**
     * @param $productId
     * @return array
     */
  public function findOpenHighStockLevelProductList($productId)
  {
    $sSql = sprintf("
            SELECT OILI.* FROM orders_items_lists AS OIL
            JOIN orders_items_lists_items AS OILI 
              ON OILI.orders_items_lists_id = OIL.id 
                AND OILI.product_id = %d 
                AND OILI.confirmed = '0'
            JOIN sell_predict AS SL
              ON SL.orders_items_lists_items_id = OILI.id AND SL.status = '".SellPredict::STATUS_COMPLETATION."'
            WHERE OIL.type = '%s'
            AND OIL.closed = '0'
            AND OILI.quantity > OILI.confirmed_quantity

    ", $productId, self::HIGH_STOCK_LEVEL_DOCUMENT_TYPE);

    return $this->pDbMgr->GetAll('profit24', $sSql);
  }


    /**
     * @param $cListType
     * @param $listItems
     * @param $returnId
     * @param string $sPackageNumber
     * @param bool $bGetFromTrain
     * @param null $mTransportId
     * @throws \Exception
     */
  public function addListItemsWithStockLocationReservation($cListType, $listItems, $returnId, $sPackageNumber = '', $bGetFromTrain = false, $mTransportId = null){
        global $aConfig;

      $aValuesOIL = array(
          'user_id' => $_SESSION['user']['id'],
          'document_number' => ($this->pDbMgr->GetOne('profit24', 'SELECT MAX( id ) FROM orders_items_lists') + 1),
          'package_number' => ($sPackageNumber == '' ? 'TMP' : $sPackageNumber),
          'closed' => '0',
          'type' => $cListType,
          'get_from_train' => $bGetFromTrain,
          'created' => 'NOW()',
      );
      if (is_array($mTransportId)) {
          $aValuesOIL['transport_id'] = 'NULL';
      } elseif ($mTransportId <= 0) {
          $aValuesOIL['transport_id'] = 'NULL';
      } else {
          $aValuesOIL['transport_id'] = $mTransportId;
      }
      $iILId = $this->pDbMgr->Insert('profit24', 'orders_items_lists', $aValuesOIL);
      foreach ($listItems as $items) {
          $item = $items[0];
          $iOILIId = $this->addItemsStockLocationReservation($iILId, $item['quantity'], $item['product_id'], $item['ean_13']);
          $iSLOId = $this->stockLocationOrdersItemsListsItems->reserveOnMagazineReturn($iOILIId, $item, StockLocationOrdersItemsListsItems::TYPE_ZD, $returnId);

          if (intval($iSLOId) <= 0 || intval($iOILIId) <= 0) {
              throw new \Exception(_('Wystąpił błąd podczas rezerwowania towaru na lokalizacjach $iSLOId:'.$iSLOId.' $iOILIId:'.$iOILIId. ' '.print_r($item, true)));
          }

      }
  }

    /**
     * @param $iILId
     * @param $quantity
     * @param $productId
     * @param $ean13
     * @return bool
     * @throws \Exception
     */
    public function addItemsStockLocationReservation($iILId, $quantity, $productId, $ean13) {

        $aValuesOILI = array(
            'orders_items_lists_id' => $iILId,
            'orders_items_id' => 'NULL',
            'orders_id' => 'NULL',
            'products_id' => $productId,
            'product_id' => $productId,
            'quantity' => $quantity,
            'isbn_plain' => $ean13,
            'isbn_10' => $ean13,
            'isbn_13' => $ean13,
            'ean_13' => $ean13,
            'confirmed' => '0'
        );
        $iOIlId = $this->pDbMgr->Insert('profit24', 'orders_items_lists_items', $aValuesOILI);
        if ($iOIlId === FALSE) {
            throw new \Exception('Wystąpił błąd podczas dodawania orders_items_lists_items : '.print_r($aValuesOILI, true));
        }
        return $iOIlId;
    }

}