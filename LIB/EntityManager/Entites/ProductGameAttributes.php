<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-04-29
 * Time: 10:24
 */

namespace LIB\EntityManager\Entites;


class ProductGameAttributes
{
    const SEX_FEMALE = 'F';
    const SEX_MALE = 'M';
    const SEX_MIXED = '0';

    public static function getSexOptions()
    {
        return [
            _('Kobieta') => self::SEX_FEMALE,
            _('Mężczyzna') => self::SEX_MALE,
            _('Dowolna') => self::SEX_MIXED
        ];
    }
}