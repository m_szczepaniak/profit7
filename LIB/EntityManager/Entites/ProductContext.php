<?php

namespace LIB\EntityManager\Entites;


class ProductContext
{
    private $aFields = ['name', 'name2', 'description', 'short_description'];

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct(\DatabaseManager $pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
    }

    function getBookstores()
    {

        $sSql = "SELECT w.code, w.name
						 FROM websites AS w
						 WHERE bookstore = 1";
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    // Pobierz wszystkie dane
    function getData($iProductId)
    {
        $aSites = $this->getBookstores();
        $aData = [];

        foreach ($aSites as $aSite) {
            $sSql = 'SELECT p.name, p.name2, p.description, p.short_description, ' .
                ($aSite['code'] != 'profit24' ? 'p.profit24_id' : 'p.id') .
                ' FROM products as p
                 WHERE ' . ($aSite['code'] != 'profit24' ? 'p.profit24_id' : 'p.id') . ' = ' . $iProductId . ' 
                 LIMIT 1';

            $aProductData = $this->pDbMgr->GetRow($aSite['code'], $sSql);
            if ($aProductData !== null)
                $aData[$aSite['code']] = $aProductData;
        }

        return $aData;
    }

    // Uaktualnij wszystkie dane
    function updateShopsProductWithTransaction($aData, $productId)
    {
        $sites = $this->getBookstores();
        $bError = false;

        foreach ($sites as $aSite) {
            if ('profit24' != $aSite['code']) {
                $currErr = false;
                $this->pDbMgr->BeginTransaction($aSite['code']);
                $sDesc = eregi_replace('<a.*</a>', '', $aData[$aSite['code'] . '_' . 'description']);
                $sDesc = eregi_replace('<script.*</script>', '', $sDesc);

                $aPreparedData = [
                    'name' => $aData[$aSite['code'] . '_' . 'name'],
                    'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($aData[$aSite['code'] . '_' . 'name'], ' '))),
                    'description' => $sDesc,
                    'short_description' => strip_tags(br2nl(trimString($aData[$aSite['code'] . '_' . 'short_description'], 200), ' '))
                ];

                $aPreparedDataShadow = [
                    'title' => $aData[$aSite['code'] . '_' . 'name'],
                ];

                if ($aData[$aSite['code'] . '_' . 'name2'] != '') {
                    $aPreparedData['name2'] = $aData[$aSite['code'] . '_' . 'name2'];
                    $aPreparedDataShadow['name2'] = $aData[$aSite['code'] . '_' . 'name2'];
                }

                if ($this->isSiteEmpty($aSite, $aData) === false) {
                    if ($aSite['code'] != 'profit24' && $this->checkIfProductExists($aSite['code'], $productId) === false) {
                        if (false === $this->insertProduct($aSite, $aData, $productId)) {
                            $bError = true;
                            $currErr = true;
                        }
                    } else {
                        if (false === $this->pDbMgr->Update($aSite['code'], 'products', $aPreparedData, ($aSite['code'] != 'profit24' ? 'profit24_id' : 'id') . ' = ' . $productId . ' LIMIT 1')) {
                            $bError = true;
                            $currErr = true;
                        }

                        if ($aSite['code'] != 'profit24') {
                            $sSql = 'SELECT id
                         FROM products p
                         WHERE p.profit24_id = ' . $productId . ' LIMIT 1';

                            $sProductId = $this->pDbMgr->GetOne($aSite['code'], $sSql);
                        } else {
                            $sProductId = $productId;
                        }

                        if (false === $this->pDbMgr->Update($aSite['code'], 'products_shadow', $aPreparedDataShadow, 'id = ' . $sProductId . ' LIMIT 1')) {
                            $bError = true;
                            $currErr = true;
                        }
                    }
                }
                if (false === $currErr) {
                    $this->pDbMgr->CommitTransaction($aSite['code']);
                } else {
                    $this->pDbMgr->RollbackTransaction($aSite['code']);
                }
            }
        }
        return ($bError === true ? false : true);
    }

    /**
     * @param $aData
     * @param $iProfitId
     * @return bool
     */
    function insertProductToShopsWithTransactions($aData, $iProfitId)
    {
        $sites = $this->getBookstores();
        $bError = false;

        foreach ($sites as $aSite) {
            if ($aSite['code'] !== 'profit24') {
                $this->pDbMgr->BeginTransaction($aSite['code']);
                if (false === $this->insertProduct($aSite, $aData, $iProfitId)) {
                    $bError = true;
                    $this->pDbMgr->RollbackTransaction($aSite['code']);
                } else {
                    $this->pDbMgr->CommitTransaction($aSite['code']);
                }

            } else if ($aSite['code'] !== 'profit24') {
                $bError = true;
            }
        }
        return ($bError === true ? false : true);
    }

    function getPageId($sCode, $iProfitId)
    {
        $aProfitCategories = $this->getProfitCategories($iProfitId);

        $sSql = 'SELECT page_id FROM menus_items_mappings 
                         WHERE source_id = "1" 
                           AND source_category_id IN (' . implode(', ', $aProfitCategories) . ') 
                         LIMIT 1';
        $categoryId = $this->pDbMgr->GetOne($sCode, $sSql);
        return $categoryId;
    }

    function getProfitCategories($iProfitId)
    {
        $sSql = 'SELECT page_id FROM products_extra_categories WHERE product_id = ' . $iProfitId;
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

    function checkIfProductExists($sSite, $iProfitId)
    {
        $sSql = 'SELECT id FROM products WHERE profit24_id = ' . $iProfitId . ' LIMIT 1';
        return $this->pDbMgr->getOne($sSite, $sSql) !== null;
    }

    function insertProduct($aSite, $aData, $iProfitId)
    {
        $bError = false;

        $iPageId = $this->getPageId($aSite['code'], $iProfitId);

        if ($iPageId > 0) {
            $sDesc = eregi_replace('<a.*</a>', '', $aData[$aSite['code'] . '_' . 'description']);
            $sDesc = eregi_replace('<script.*</script>', '', $sDesc);

            $aPreparedData = [
                'name' => $aData[$aSite['code'] . '_' . 'name'],
                'plain_name' => preg_replace('/ +/', ' ', strip_tags(br2nl($aData[$aSite['code'] . '_' . 'name'], ' '))),
                'description' => $sDesc,
                'short_description' => strip_tags(br2nl(trimString($aData[$aSite['code'] . '_' . 'short_description'], 200), ' ')),
                'isbn' => $_POST['isbn'],
                'isbn_plain' => $_POST['isbn_plain'] != '' ? isbn2plain($_POST['isbn_plain']) : $_POST['isbn'],
                'isbn_10' => $_POST['isbn_10'] != '' ? $_POST['isbn_10'] : 'NULL',
                'isbn_13' => $_POST['isbn_13'] != '' ? $_POST['isbn_13'] : 'NULL',
                'ean_13' => $_POST['ean_13'] != '' ? $_POST['ean_13'] : 'NULL',
                'page_id' => (double)$iPageId,
                'profit24_id' => $iProfitId,
                'published' => $_POST['published'],
                'prod_status' => 0
            ];

            $aPreparedDataShadow = [
                'title' => $aData[$aSite['code'] . '_' . 'name'],
                'isbn' => $_POST['isbn'],
                'isbn_plain' => $_POST['isbn_plain'] != '' ? isbn2plain($_POST['isbn_plain']) : $_POST['isbn'],
                'isbn_10' => $_POST['isbn_10'] != '' ? $_POST['isbn_10'] : 'NULL',
                'isbn_13' => $_POST['isbn_13'] != '' ? $_POST['isbn_13'] : 'NULL',
                'ean_13' => $_POST['ean_13'] != '' ? $_POST['ean_13'] : 'NULL',
                'prod_status' => 0
            ];

            if ($aData[$aSite['code'] . '_' . 'name2'] != '') {
                $aPreparedData['name2'] = $aData[$aSite['code'] . '_' . 'name2'];
                $aPreparedDataShadow['name2'] = $aData[$aSite['code'] . '_' . 'name2'];
            }

            if ($aPreparedData['description'] != '' && $aPreparedData['name'] != '') {

                $iId = $this->pDbMgr->Insert($aSite['code'], 'products', $aPreparedData);

                $aPreparedDataShadow['id'] = $iId;

                $iShadowId = $this->pDbMgr->Insert($aSite['code'], 'products_shadow', $aPreparedDataShadow);

                if ($iId === false || $iShadowId === false) {
                    $bError = true;
                }
            }
        }

        return ($bError === true ? false : true);
    }

    function isSiteEmpty($aSite, $aData)
    {
        $bEmpty = true;
        foreach ($this->aFields as $aField)
            if ($aData[$aSite['code'] . '_' . $aField] !== '' && $aData[$aSite['code'] . '_' . $aField] !== null)
                $bEmpty = false;
        return $bEmpty;
    }
}