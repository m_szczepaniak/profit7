<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 31.10.17
 * Time: 12:24
 */

namespace LIB\EntityManager\Entites;

use DatabaseManager;
use Exception;
use LIB\EntityManager\Entites\OrdersItemsLists;

class StockLocation
{

    const HIGH_MAGAZINE_TYPE = 'SH';
    const LOW_MAGAZINE_TYPE = 'SL';

    /**
     * @var ProductsStock
     */
    protected $productStock;

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    const PRODUCT_STOCK_PREFIX = 'stock';

    /**
     * StockLocation constructor.
     * @param DatabaseManager $pDbMgr
     */
    public function __construct(DatabaseManager $pDbMgr)
    {
        $this->productStock = new ProductsStock($pDbMgr);
        $this->pDbMgr = $pDbMgr;
    }

    /**
     * @param $productId
     * @param $location
     * @param $magazineType
     * @param int $quantity
     * @param int $reservation
     * @param int $available
     * @return int|mixed
     * @throws Exception
     */
    public function findAddSumLocation($productId, $location, $magazineType, $quantity = 0, $reservation = 0, $available = 0)
    {
        global $pDbMgr;

        $sSql = 'SELECT id FROM stock_location 
             WHERE products_stock_id = ' . $productId . ' 
                AND container_id = "' . $location . '"
                AND magazine_type = "' . $magazineType . '"';
        $id = $pDbMgr->GetOne('profit24', $sSql);
        if ($id > 0) {
            if ($quantity > 0 || $reservation > 0 || $available > 0) {
                if (false === $this->sumLocationQuantity($id, $quantity, $reservation, $available)) {
                    throw new Exception(_('Wystąpił błąd podczas sumowania produktów na lokalizacji'));
                }
            }
            return $id;
        } else {
            // trzeba dodać
            if (false === $magazineType || null === $magazineType) {
                throw new Exception(_('Wystąpił błąd podczas definiowania typu lokalizacji'));
            }
            $id = $this->add($magazineType, $productId, $location, $quantity, $reservation, $available);
            if ($id > 0) {
                return $id;
            } else {
                throw new Exception(_('Wystąpił błąd podczas dodawania produktu na lokalizacji - ' . $magazineType . ', ' . $productId . ', ' . $location));
            }
        }
    }


    /**
     * @param $sourceContainerId
     * @param $destinationContainerId
     * @return bool
     * @throws Exception
     */
    public function changeContainerProduct($sourceContainerId, $destinationContainerId, $productId) {
        $sSql = 'SELECT magazine_type FROM containers WHERE id = "'.$destinationContainerId.'"';
        $destinationMagazineType = $this->pDbMgr->GetOne('profit24', $sSql);


        $sSql = 'SELECT * FROM stock_location
                 WHERE container_id = "'.$sourceContainerId.'" AND products_stock_id = '.$productId;
        $stockLocationProducts = $this->pDbMgr->GetAll('profit24', $sSql);
        foreach ($stockLocationProducts as $locationProduct) {
            if ($locationProduct['quantity'] > 0) {
                if (false === $this->zeroLocationQuantity($locationProduct['id'])) {
                    throw new Exception('Wystąpił błąd podczas zerowania stanu id ' . $locationProduct['id']);
                }
                $this->findAddSumLocation($locationProduct['products_stock_id'], $destinationContainerId, $destinationMagazineType,
                    $locationProduct['quantity'], $locationProduct['reservation'], $locationProduct['available']
                );
            }
        }
        return true;
    }

    /**
     * @param $sourceContainerId
     * @param $destinationContainerId
     * @param $productId
     * @param $quantity
     * @return bool
     * @throws Exception
     */
    public function moveContainerProduct($sourceContainerId, $destinationContainerId, $productId, $quantity) {
        $sSql = 'SELECT magazine_type FROM containers WHERE id = "'.$destinationContainerId.'"';
        $destinationMagazineType = $this->pDbMgr->GetOne('profit24', $sSql);


        $sSql = 'SELECT * FROM stock_location
                 WHERE container_id = "'.$sourceContainerId.'" AND products_stock_id = '.$productId;
        $stockLocationProducts = $this->pDbMgr->GetAll('profit24', $sSql);
        foreach ($stockLocationProducts as $locationProduct) {
            if ($locationProduct['quantity'] > 0) {
                if ($quantity >= 0) {
                    if (false === $this->subtractLocationQuantity($locationProduct['id'], $quantity)) {
                        throw new Exception('Wystąpił błąd podczas odejmowania stanu id ' . $locationProduct['id']);
                    }
                    $this->findAddSumLocation($locationProduct['products_stock_id'], $destinationContainerId, $destinationMagazineType,
                        $quantity, 0, $quantity
                    );
                }
            }
        }
        return true;
    }


    /**
     * @param $sourceContainerId
     * @param $destinationContainerId
     * @return bool
     * @throws Exception
     */
    public function changeContainerWithAllProducts($sourceContainerId, $destinationContainerId) {
        $sSql = 'SELECT magazine_type FROM containers WHERE id = "'.$destinationContainerId.'"';
        $destinationMagazineType = $this->pDbMgr->GetOne('profit24', $sSql);


        $sSql = 'SELECT * FROM stock_location
                 WHERE container_id = "'.$sourceContainerId.'"';
        $stockLocationProducts = $this->pDbMgr->GetAll('profit24', $sSql);
        foreach ($stockLocationProducts as $locationProduct) {
            if ($locationProduct['quantity'] > 0) {
                if (false === $this->zeroLocationQuantity($locationProduct['id'])) {
                    throw new Exception('Wystąpił błąd podczas zerowania stanu id ' . $locationProduct['id']);
                }
                $this->findAddSumLocation($locationProduct['products_stock_id'], $destinationContainerId, $destinationMagazineType,
                    $locationProduct['quantity'], $locationProduct['reservation'], $locationProduct['available']
                );
            }
        }
        return true;
    }

    /**
     * @param $magazineType
     * @param $productId
     * @param $location
     * @param int $quantity
     * @param int $reservation
     * @param int $available
     * @return int
     */
    public function add($magazineType, $productId, $location, $quantity = 0, $reservation = 0, $available = 0)
    {
        global $pDbMgr;

        $values = [
            'magazine_type' => $magazineType,
            'products_stock_id' => $productId,
            'container_id' => $location,
            'quantity' => $quantity,
            'reservation' => $reservation,
            'available' => $available
        ];
        $mReturn = $pDbMgr->Insert('profit24', 'stock_location', $values);
        if (false === $mReturn) {
            return false;
        }

        if (false === $this->recountProductStock(null, $productId)) {
            return false;
        }
        return $mReturn;
    }

    /**
     * @param $productId
     * @return array
     */
    public function getFindLocations($productId)
    {
        global $pDbMgr;

        $sSql = '
        SELECT PS.*, M.type AS magazine_type
        FROM stock_location AS PS
        JOIN magazine AS M
          ON  M.type = PS.magazine_type
        WHERE PS.products_stock_id = ' . $productId . '
        ';
        return $pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * @param $productIds
     * @param $magazineType
     * @return array
     */
    public function getMagazineLocations($productIds, $magazineType)
    {
        global $pDbMgr;

        $sSql = '
        SELECT PS.products_stock_id AS TMP_ID, PS.*
        FROM stock_location AS PS
        WHERE PS.products_stock_id IN (' . implode(', ', $productIds) . ')
        AND PS.magazine_type = "' . $magazineType . '"
        ';
        return $pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);
    }

    /**
     * @param $stockLocationId
     * @param $quantity
     * @return int
     */
    public function incQuantity($stockLocationId, $productId, $quantity)
    {
        global $pDbMgr;

        $quantity = intval($quantity);
        $sSql = 'UPDATE stock_location SET
             quantity = quantity + ' . $quantity . ',
             available = available + ' . $quantity . '
             WHERE id = ' . $stockLocationId . '
             LIMIT 1
            ';
        $stockLocation = $pDbMgr->Query('profit24', $sSql);
        if (false === $stockLocation) {
            return false;
        }

        if (false === $this->recountProductStock(null, $productId)) {
            return false;
        }

        return true;
    }


    /**
     * @param $productId
     * @param $quantity
     * @param $location
     * @return bool
     * @throws Exception
     */
    public function incAddStockLocationQuantity($productId, $quantity, $location)
    {
        // szukamy lokalizacji
        $sSql = '
        SELECT PS.*
        FROM stock_location AS PS
        WHERE PS.products_stock_id = ' . $productId . ' 
          AND PS.container_id = "' . $location . '"
        LIMIT 1';
        $stockLocation = $this->pDbMgr->GetRow('profit24', $sSql);
        if (!empty($stockLocation)) {
            if (false === $this->incQuantity($stockLocation['id'], $productId, $quantity)) {
                throw new Exception('Wystąpił błąd podczas aktualizacji stock_location');
            } else {
                return true;
            }
        } else {
            // trzeba dodać
            $container = $this->pDbMgr->getTableRow('containers', $location, ['magazine_type']);
            if (!empty($container)) {
                if (false === $this->add($container['magazine_type'], $productId, $location, $quantity, 0, $quantity)) {
                    throw new Exception(_('Wystąpił błąd podczas dodawania lokalizacji'));
                } else {
                    return true;
                }
            } else {
                throw new Exception(_('Nie odnaleziono lokalizacji ' . $location . ' w bazie'));
            }

        }
        return false;
    }


    /**
     * @param $stockLocationId
     * @param $quantity
     * @return int
     */
    public function decQuantity($stockLocationId, $productId, $quantity)
    {
        global $pDbMgr;

        $quantity = intval($quantity);
        $sSql = 'UPDATE stock_location SET
             quantity = quantity - ' . $quantity . ',
             available = available - ' . $quantity . '
             WHERE id = ' . $stockLocationId . '
             LIMIT 1
            ';
        $stockLocation = $pDbMgr->Query('profit24', $sSql);
        if (false === $stockLocation) {
            return false;
        }

        if (false === $this->recountProductStock(null, $productId)) {
            return false;
        }
        return true;
    }

    /**
     * @param $stockLocationId
     * @param $quantity
     * @param $reserved
     * @param $available
     * @return mixed
     */
    public function changeStockLocationQuantity($stockLocationId, $quantity, $reserved, $available)
    {

        $values = [
            'available' => $available,
            'reservation' => $reserved,
            'quantity' => $quantity
        ];

        $result = $this->pDbMgr->Update('profit24', 'stock_location', $values, ' id=' . $stockLocationId);

        if (false === $this->recountProductStock($stockLocationId)) {
            $result = false;
        }
        return $result;
    }

    /**
     * @param $productId
     * @param $magazine
     * @return mixed
     */
    public function getProductMagazineAvailableQuantity($productId, $magazine)
    {

        $sSql = '
            SELECT SUM(available) 
            FROM stock_location AS SL
            WHERE SL.products_stock_id = ' . $productId . '
              AND SL.magazine_type = "' . $magazine . '"
    ';
        return intval($this->pDbMgr->GetOne('profit24', $sSql));
    }


    /**
     * @param $stockLocationId
     * @return int
     */
    public function zeroLocationQuantity($stockLocationId)
    {
        $sSql = '
            UPDATE stock_location
            SET 
                quantity = 0,
                reservation = 0,
                available = 0 
            WHERE id = ' . $stockLocationId . '
            LIMIT 1;
            ';

        $result = $this->pDbMgr->Query('profit24', $sSql);

        if (false === $this->recountProductStock($stockLocationId)) {
            $result = false;
        }
        return $result;
    }

    /**
     * @param $stockLocationId
     * @param $quantity
     * @param $reservation
     * @param $available
     * @return int
     */
    private function subtractLocationQuantity($stockLocationId, $quantity)
    {
        $sSql = '
            UPDATE stock_location
            SET 
                quantity = quantity - ' . $quantity . ',
                available = quantity - reservation 
            WHERE id = ' . $stockLocationId . '
            LIMIT 1;
            ';

        $result = $this->pDbMgr->Query('profit24', $sSql);

        if (false === $this->recountProductStock($stockLocationId)) {
            $result = false;
        }
        return $result;
    }

    /**
     * @param $stockLocationId
     * @param $quantity
     * @param $reservation
     * @param $available
     * @return int
     */
    private function sumLocationQuantity($stockLocationId, $quantity, $reservation, $available)
    {
        $sSql = '
            UPDATE stock_location
            SET 
                quantity = quantity + ' . $quantity . ',
                reservation = reservation + ' . $reservation . ',
                available = quantity - reservation 
            WHERE id = ' . $stockLocationId . '
            LIMIT 1;
            ';

        $result = $this->pDbMgr->Query('profit24', $sSql);

        if (false === $this->recountProductStock($stockLocationId)) {
            $result = false;
        }
        return $result;
    }

    /**
     * @param $productId
     * @param $containerId
     * @return mixed
     */
    public function getStockLocation($productId, $containerId)
    {
        $sSql = 'SELECT * 
                 FROM stock_location 
                 WHERE container_id = "' . $containerId . '" 
                   AND products_stock_id = "' . $productId . '" ';
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }


    /**
     * @param $orderId
     * @return bool
     */
    public function unreserveStockLocationOrder($orderId)
    {

        $hsdT = OrdersItemsLists::HIGH_STOCK_LEVEL_DOCUMENT_TYPE;

        $sSql = 'SELECT OILI.id
                 FROM orders_items_lists_items AS OILI
                 JOIN orders_items_lists AS OIL
                  ON OILI.orders_items_lists_id = OIL.id AND OIL.type <> "' . $hsdT . '"
                 WHERE OILI.orders_id = "' . $orderId . '" 
                 ORDER BY OILI.id DESC
                 ';
        $orderItemsListsItemsIds = $this->pDbMgr->GetCol('profit24', $sSql);
        foreach ($orderItemsListsItemsIds as $orderItemsListsItemsId) {
            if (!empty($orderItemsListsItemsId)) {
                $sSql = 'SELECT * 
                     FROM stock_location_orders_items_lists_items
                 WHERE orders_items_lists_items_id = ' . $orderItemsListsItemsId . ' 
                 AND reservation_subtracted = "0" ';
                $stockLocationOrdersItems = $this->pDbMgr->GetAll('profit24', $sSql);
                $this->unreserveStockLocationsOrdersItems($stockLocationOrdersItems, $orderId);
            }
        }
        return true;
    }


    /**
     * @param $stockLocationOrdersItems
     */
    public function unreserveStockLocationsOrdersItems($stockLocationOrdersItems, $orderId) {

        if (!empty($stockLocationOrdersItems)) {
            foreach ($stockLocationOrdersItems as $stockLocationOrdersItem) {
                $quantityToSubstractLocation = $stockLocationOrdersItem['reserved_quantity'];
                if ($quantityToSubstractLocation > 0) {
                    $stockLocationId = $stockLocationOrdersItem['stock_location_id'];
                    $magazineType = $stockLocationOrdersItem['stock_location_magazine_type'];

                    $sSql = 'SELECT * 
                             FROM stock_location
                             WHERE id = ' . $stockLocationId;
                    $stockLocation = $this->pDbMgr->GetRow('profit24', $sSql);
                    if (!empty($stockLocation)) {
                        if ($stockLocation['reservation'] >= $quantityToSubstractLocation) {


                            // cofamy ilości
                            $sSql = 'UPDATE stock_location
                                     SET reservation = reservation - ' . $quantityToSubstractLocation . ',
                                         available = available + ' . $quantityToSubstractLocation . '
                                     WHERE id = ' . $stockLocationId . ' AND magazine_type = "' . $magazineType . '"';
                            if (false === $this->pDbMgr->Query('profit24', $sSql)) {
                                echo 'ERR 88 Błąd pomniejszania ilosci  ' . $sSql;
                            }

                            if (false === $this->recountProductStock(null, $stockLocation['products_stock_id'])) {
                                echo 'ERR 8aB8 Błąd pomniejszania ilosci  ' . $stockLocation['products_stock_id'];
                            }

                            $value = ['reservation_subtracted' => '1'];
                            $this->pDbMgr->Update('profit24', 'stock_location_orders_items_lists_items', $value, ' id = ' . $stockLocationOrdersItem['id']);
                            $sContent = 'oid:' . $orderId;
                            $sContent .= print_r($stockLocationOrdersItem, true);
                            $sContent .= print_r($stockLocation, true);
//                                    file_put_contents('stock_location_orders_items_lists_items_' . date('Ymd') . '.log', $sContent, FILE_APPEND | LOCK_EX);
                        } else {
                            echo "\r\n" . 'ERR 88 za mało zarezerwowane ' . $stockLocation['reservation'] . ' za dużo do zrzucenia ' . $quantityToSubstractLocation . ' !! zamid = ' . $orderId . ' order_item_id = ' . $orderItemId;
                            print_r($stockLocationOrdersItem);
                            print_r($stockLocation);
                        }
                    } else {
                        echo "\r\n" . 'ERR 88 pusta dostawa !! zamid = ' . $orderId;
                        print_r($stockLocationOrdersItem);
                    }
                } else {
                    echo "\r\n" . 'ERR 88 zerowa ilość rezerwacji zamid = ' . $orderId;
                    print_r($stockLocationOrdersItem);
                }
            }
        }
    }


    /**
     * @param $productId
     * @param $stockLocationId
     * @param $quantityToSubstractLocation
     * @param $stockLocationOILId
     */
    function decQuantityLocation($productId, $stockLocationId, $quantityToSubstractLocation, $stockLocationOILId) {
        // cofamy ilości
        $sSql = 'UPDATE stock_location
                                     SET reservation = reservation - ' . $quantityToSubstractLocation . ',
                                         available = available + ' . $quantityToSubstractLocation . '
                                     WHERE id = ' . $stockLocationId;
        if (false === $this->pDbMgr->Query('profit24', $sSql)) {
            echo 'ERR 88 Błąd pomniejszania ilosci  ' . $sSql;
        }

        if (false === $this->recountProductStock(null, $productId)) {
            echo 'ERR 8aB8 Błąd pomniejszania ilosci  ' . $productId;
        }

        $value = ['reservation_subtracted' => '1'];
        $this->pDbMgr->Update('profit24', 'stock_location_orders_items_lists_items', $value, ' id = ' . $stockLocationOILId);
    }



    /**
     * @param $orderId
     * @param $orderItemId
     * @return bool
     */
    public function unreserveStockLocation($orderId, $orderItemId)
    {

        $hsdT = OrdersItemsLists::HIGH_STOCK_LEVEL_DOCUMENT_TYPE;

        $sSql = 'SELECT OILI.id
                 FROM orders_items_lists_items AS OILI
                 JOIN orders_items_lists AS OIL
                  ON OILI.orders_items_lists_id = OIL.id AND OIL.type <> "' . $hsdT . '"
                 WHERE OILI.orders_id = "' . $orderId . '" 
                   AND OILI.orders_items_id = "' . $orderItemId . '"
                 ORDER BY OILI.id DESC
                 ';
        $orderItemsListsItemsId = $this->pDbMgr->GetOne('profit24', $sSql);

        if (!empty($orderItemsListsItemsId)) {
            $sSql = 'SELECT * 
                     FROM stock_location_orders_items_lists_items
                 WHERE orders_items_lists_items_id = ' . $orderItemsListsItemsId . ' 
                 AND reservation_subtracted = "0" ';
            $stockLocationOrdersItems = $this->pDbMgr->GetAll('profit24', $sSql);
            if (!empty($stockLocationOrdersItems)) {
                foreach ($stockLocationOrdersItems as $stockLocationOrdersItem) {
                    $quantityToSubstractLocation = $stockLocationOrdersItem['reserved_quantity'];
                    if ($quantityToSubstractLocation > 0) {
                        $stockLocationId = $stockLocationOrdersItem['stock_location_id'];
                        $magazineType = $stockLocationOrdersItem['stock_location_magazine_type'];

                        $sSql = 'SELECT * 
                             FROM stock_location
                             WHERE id = ' . $stockLocationId;
                        $stockLocation = $this->pDbMgr->GetRow('profit24', $sSql);
                        if (!empty($stockLocation)) {
                            if ($stockLocation['reservation'] >= $quantityToSubstractLocation) {


                                // pomniejszamy ilośći
                                $sSql = 'UPDATE stock_location
                                     SET reservation = reservation - ' . $quantityToSubstractLocation . ',
                                         quantity = quantity - ' . $quantityToSubstractLocation . '
                                     WHERE id = ' . $stockLocationId . ' AND magazine_type = "' . $magazineType . '"';
                                if (false === $this->pDbMgr->Query('profit24', $sSql)) {
                                    echo 'ERR 88 Błąd pomniejszania ilosci  ' . $sSql;
                                }

                                if (false === $this->recountProductStock(null, $stockLocation['products_stock_id'])) {
                                    echo 'ERR 8aB8 Błąd pomniejszania ilosci  ' . $stockLocation['products_stock_id'];
                                }

                                $value = ['reservation_subtracted' => '1'];
                                $this->pDbMgr->Update('profit24', 'stock_location_orders_items_lists_items', $value, ' id = ' . $stockLocationOrdersItem['id']);
                                $sContent = 'oid:' . $orderId . ',oiid:' . $orderItemId;
                                $sContent .= print_r($stockLocationOrdersItem, true);
                                $sContent .= print_r($stockLocation, true);
//                                file_put_contents('stock_location_orders_items_lists_items_' . date('Ymd') . '.log', $sContent, FILE_APPEND | LOCK_EX);
                            } else {
                                echo "\r\n" . 'ERR 88 za mało zarezerwowane ' . $stockLocation['reservation'] . ' za dużo do zrzucenia ' . $quantityToSubstractLocation . ' !! zamid = ' . $orderId . ' order_item_id = ' . $orderItemId;
                                print_r($stockLocationOrdersItem);
                                print_r($stockLocation);
                            }
                        } else {
                            echo "\r\n" . 'ERR 88 pusta dostawa !! zamid = ' . $orderId . ' order_item_id = ' . $orderItemId;
                            print_r($stockLocationOrdersItem);
                        }
                    } else {
                        echo "\r\n" . 'ERR 88 zerowa ilość rezerwacji zamid = ' . $orderId . ' order_item_id = ' . $orderItemId;
                        print_r($stockLocationOrdersItem);
                    }
                }
            }
        }
        return true;
    }

    /**
     * @param $iSLOId
     * @param $stockLocationId
     * @param $quantity
     */
    public function unreserveByStockLocationItems($iSLOId, $stockLocationId, $quantity)
    {

        // pomniejszamy ilośći
        $sSql = 'UPDATE stock_location
                                     SET reservation = reservation - ' . $quantity . ',
                                         quantity = quantity - ' . $quantity . '
                                     WHERE id = ' . $stockLocationId;
        if (false === $this->pDbMgr->Query('profit24', $sSql)) {
            echo 'ERR 88 Błąd pomniejszania ilosci  ' . $sSql;
        }

        if (false === $this->recountProductStock($stockLocationId)) {
            echo 'ERR 8aB8 Błąd pomniejszania ilosci  ' . $stockLocationId;
        }

        $value = [
            'reservation_subtracted' => '1',
            'status' => '0'
        ];
        $this->pDbMgr->Update('profit24', 'stock_location_orders_items_lists_items', $value, ' id = ' . $iSLOId);
    }


    /**
     * @param $orderItemListItems
     * @return mixed
     */
    public function getStockLocationReservation($orderItemListItems)
    {

        foreach ($orderItemListItems AS $iKey => $orderItemListItem) {
            $orderItemReservation = $this->getOrderItemListReservation($orderItemListItem['OILI_ID']);
            if (!empty($orderItemReservation)) {
                $orderItemListItems[$iKey]['magazine_stock_localizations_reserved'] = $orderItemReservation;
            }
        }
        return $orderItemListItems;
    }

    /**
     * @param $iOILId
     * @return array
     */
    public function getOrderItemListReservation($iOILId)
    {

        $sSql = 'SELECT SL.id, SL.magazine_type, SL.container_id, SL.quantity, SL.reservation, SL.available, SLO.reserved_quantity AS own_reserved 
                 FROM stock_location_orders_items_lists_items AS SLO
                 JOIN stock_location AS SL
                  ON SLO.stock_location_id = SL.id AND SLO.stock_location_magazine_type = SL.magazine_type
                 WHERE SLO.orders_items_lists_items_id = ' . $iOILId;
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @param $sellPredictId
     * @return bool
     * @throws Exception
     */
    public function unreserveBySellPredict($sellPredictId)
    {

        $sSql = '
        SELECT (SLO.reserved_quantity - SLO.confirmed_quantity) diff_quantiy, SL.*
        FROM sell_predict AS SP
        JOIN products AS P
          ON P.id = SP.product_id
        JOIN orders_items_lists_items AS OILI
          ON SP.orders_items_lists_items_id = OILI.id
        JOIN stock_location_orders_items_lists_items AS SLO
          ON OILI.id = SLO.orders_items_lists_items_id
        JOIN stock_location AS SL
          ON SLO.stock_location_id = SL.id AND SLO.stock_location_magazine_type = SL.magazine_type
        WHERE SP.id = ' . $sellPredictId . '
        AND SLO.stock_location_magazine_type = "' . Magazine::TYPE_HIGH_STOCK_SUPPLIES . '"
        ';
        $diffQuantity = $this->pDbMgr->GetAll('profit24', $sSql);
        foreach ($diffQuantity as $row) {
            $reservation = $row['reservation'] - $row['diff_quantiy'];
            $values = [
                'available' => $row['available'] + $row['diff_quantiy'],
                'reservation' => $reservation < 0 ? 0 : $reservation,
            ];

            if (false === $this->pDbMgr->Update('profit24', 'stock_location', $values, ' id=' . $row['id'])) {
                throw new Exception('Wystąpil blad podczas aktualizacji');
            }
            if (false === $this->recountProductStock(null, $row['products_stock_id'])) {
                throw new Exception('Wystąpil blad podczas aktualizacji 8555');
            }

        }
        return true;
    }


    /**
     * @param $locationId
     * @param $quantityToReserve
     * @return mixed
     */
    public function reserve($locationId, $quantityToReserve)
    {

        $sSql = '
        UPDATE stock_location
        SET 
            reservation = reservation + ' . intval($quantityToReserve) . ',
            available = available - ' . intval($quantityToReserve) . '
        WHERE id = ' . $locationId . '
        LIMIT 1 ';
        $result = $this->pDbMgr->Query('profit24', $sSql);
        if (false === $this->recountProductStock($locationId)) {
            $result = false;
        }
        return $result;
    }

    /**
     * @param $stockLocationId
     * @param $quantity
     * @return bool
     */
    public function changeQuantity($stockLocationId, $quantity){

        if ($stockLocationId > 0) {
            $stockLocationRow = $this->pDbMgr->getTableRow('stock_location', $stockLocationId, ['*']);


            if (!empty($stockLocationRow)) {
                if ($stockLocationRow['quantity'] != $quantity) {
                    if ($quantity == 0) {
                        $this->zeroLocationQuantity($stockLocationId);
                    } else {
                        $productId = $stockLocationRow['products_stock_id'];
                        if ($stockLocationRow['quantity'] < $quantity) {
                            $quantityDiff = $quantity - $stockLocationRow['quantity'];
                            if (false === $this->incQuantity($stockLocationId, $productId, $quantityDiff)) {
                                return false;
                            }
                        } elseif ($stockLocationRow['quantity'] > $quantity) {
                            $quantityDiff = $stockLocationRow['quantity'] - $quantity;
                            if (false === $this->decQuantity($stockLocationId, $productId, $quantityDiff)) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }


    /**
     * @param $stockLocation
     * @param $quantity
     * @return bool
     * @throws Exception
     */
    public function changeQuantityByStockLocationQuantity($stockLocation, $quantity) {

        if ($stockLocation['reservation'] > $quantity) {
            throw new \Exception('Podana ilość jest mniejsza niż ilość rezerwacji');
        }

        $aValues = [
            'quantity' => intval($quantity),
            'available' => intval($quantity - $stockLocation['reservation'])
        ];
        return (false === $this->pDbMgr->Update('profit24', 'stock_location', $aValues, ' id = '.$stockLocation['id']) ? false : true);
    }


    /**
     * @param $stockLocationId
     * @param null $productId
     * @return int|mixed
     */
    public function recountProductStock($stockLocationId = null, $productId = null)
    {

        if (null === $productId && $stockLocationId > 0) {
            // pobierzmy ilość na lokalizacjach
            $sSql = 'SELECT products_stock_id FROM stock_location WHERE id = ' . $stockLocationId;
            $productId = $this->pDbMgr->GetOne('profit24', $sSql);
        }

        if ($productId > 0) {
            $sSql = 'SELECT SUM(quantity) FROM stock_location WHERE products_stock_id = ' . $productId;
            $quantityProduct = intval($this->pDbMgr->GetOne('profit24', $sSql));

            if ($quantityProduct > 0) {

                $sSql = 'UPDATE products_stock
                         SET stock_act_stock = ' . $quantityProduct . ' ,
                             stock_reservations = profit_g_reservations,
                             stock_status = IF('.$quantityProduct.' - profit_g_reservations >= 0, '.$quantityProduct.' - profit_g_reservations, 0) 
                         WHERE id = ' . $productId;
                return $this->pDbMgr->Query('profit24', $sSql);
            } else {
                return $this->productStock->updateSourceQuantityReservation($productId, self::PRODUCT_STOCK_PREFIX, 0, 0);
            }
        }
        return false;
    }
}