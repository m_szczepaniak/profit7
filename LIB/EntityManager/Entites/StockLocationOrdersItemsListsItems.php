<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 31.10.17
 * Time: 10:02
 */

namespace LIB\EntityManager\Entites;

use Exception;
use magazine\Containers as magazineContainers;
use LIB\EntityManager\Entites\Containers as ContainersEntity;

class StockLocationOrdersItemsListsItems
{
  const TYPE_PZ = 'PZ';
  const TYPE_MM = 'MM';// przesunięcie między magazynami - czyli zbieranie z magazu
  const TYPE_MM_PLUS = 'MM+';// przesunięcie między magazynami - czyli zbieranie z magazu
  const TYPE_FV = 'FV';// pod sprzedaż
  const TYPE_ZD = 'ZD';

  public function addByOrdersItemsLists($ordersItemsListsItems, $productId, $location, $documentType) {
    global $pDbMgr;

    foreach ($ordersItemsListsItems as $itemsListsItem) {

      $type = [3, 2];
      $containers = new magazineContainers($pDbMgr, $type);
      $containerType = $containers->checkContainer($location);
      if (-2 === $containerType) {
        throw new Exception(_('Brak wprowadzonej lokalizacji'));
      }

      $magazineType = ContainersEntity::getStockLocationType($containerType);
      $stockLocation = new StockLocation($pDbMgr);
      $stockLocationId = $stockLocation->findAddSumLocation($productId, $location, $magazineType);

      $values = [
          'stock_location_id' => $stockLocationId,
          'stock_location_magazine_type' => $magazineType,
          'orders_items_lists_items_id' => $itemsListsItem['OILI_ID'] ,
          'reserved_quantity' =>  $itemsListsItem['used_quantity'],
          'type' => $documentType,
          'created' => 'NOW()',
          'created_by' => $_SESSION['user']['name']
      ];
      if (false === $pDbMgr->Insert('profit24', 'stock_location_orders_items_lists_items', $values)) {
        throw new Exception('Błąd dodawania produktu na lokalizacji PZ - '.print_r($ordersItemsListsItems, true));
      }
      // zwiększamy ilość na lokalizacji
      if (false === $stockLocation->incQuantity($stockLocationId, $productId, $itemsListsItem['used_quantity'])) {
        throw new Exception('Błąd zwiększania ilości na stock_location na  - '.$stockLocationId.' : '.$itemsListsItem['used_quantity']);
      }

      // zwiększamy ilość potwierdzoną na składowej
      // sprawdzamy czy już wszystko zostało potwierdzone na składowej
      if (false === $this->confirmQuantityOILI($itemsListsItem['OILI_ID'], $itemsListsItem['used_quantity'])) {
        throw new Exception('Wystąpił błąd podczas potwierdzania listy OILI '.$itemsListsItem['OILI_ID']);
      }

      // jeśli składowa jest zamykana to sprawdzamy czy cały dokument też już można zamknąć
        // @TODO wygląda na niewydajne
      if (false === $this->confirmCloseOILI($itemsListsItem['OIL_ID'])) {
        throw new Exception('Wystąpił błąd podczas potwierdzania listy OIL '.$itemsListsItem['OIL_ID']);
      }
    }
    return true;
  }

  /**
   * @param $iOILIId
   * @param $quantity
   */
  public function confirmQuantityOILI($iOILIId, $quantity) {
    global $pDbMgr;

    $sSql = 'UPDATE orders_items_lists_items
             SET 
              confirmed_quantity = confirmed_quantity + '.$quantity.',
              confirmed = IF(confirmed_quantity >= quantity, "1", "0")
             WHERE id = '.$iOILIId;
    return $pDbMgr->Query('profit24', $sSql);
  }

  /**
   * @param $iOILId
   * @return mixed
   */
  public function confirmCloseOILI($iOILId) {
    global $pDbMgr;

    $sSql = '
    UPDATE orders_items_lists AS OIL
    SET
      closed = IF((SELECT id FROM orders_items_lists_items AS OILI WHERE OILI.orders_items_lists_id = '.$iOILId.' AND confirmed = "0" LIMIT 1) > 0, "0", "1")
    WHERE id = '.$iOILId.'
    LIMIT 1
    ';
    return $pDbMgr->Query('profit24', $sSql);
  }


    /**
     * @param $iOILIId
     * @param $aItem
     * @param $reservationType
     * @param $returnId
     * @return bool
     * @throws Exception
     */
    public function reserveOnMagazineReturn($iOILIId, $aItem, $reservationType, $returnId) {
        global $pDbMgr, $aConfig;

        $stockLocation = new StockLocation($pDbMgr);
        // tutaj sobie rezerwujemy na lokalizacji wysokiego/niskiego
        if (!empty($aItem['magazine_stock_localizations_reserved'])) {
            foreach ($aItem['magazine_stock_localizations_reserved'] as $key => $row) {
                $values = [
                    'stock_location_id' => $row['id'],
                    'stock_location_magazine_type' => $row['magazine_type'],
                    'orders_items_lists_items_id' => $iOILIId,
                    'reserved_quantity' =>  $row['own_reserved'],
                    'type' => $reservationType,
                    'created' => 'NOW()',
                    'created_by' => $_SESSION['user']['name']
                ];
                $iSLOId = $pDbMgr->Insert('profit24', 'stock_location_orders_items_lists_items', $values);
                if (false === $iSLOId) {
                    return false;
                }

                $value = [
                    'magazine_return_id' => $returnId,
                    'quantity' => $row['own_reserved'],
                    'stock_location_orders_items_lists_items_id' => $iSLOId,
                    'orders_items_lists_items_id' => $iOILIId,
                    'product_id' => $aItem['product_id']
                ];
                $iMRIId = $pDbMgr->Insert('profit24', 'magazine_return_items', $value);
                if (intval($iMRIId) <= 0) {
                    return false;
                }

                // rezerwujemy na magazynie
                if (false === $stockLocation->reserve($row['id'], $row['own_reserved'])) {
                    return false;
                }

            }
        }
        return true;
    }


    /**
     * @param $iId
     * @param $status
     * @return mixed
     */
    public function changeStatus($iId, $status) {
        global $pDbMgr;

        $value = [
            'status' => $status
        ];
        return $pDbMgr->Update('profit24', 'stock_location_orders_items_lists_items', $value, 'id = '.$iId);
    }
}