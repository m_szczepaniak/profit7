<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 31.08.18
 * Time: 15:06
 */

namespace LIB\EntityManager\Entites;

use DatabaseManager;

class MagazineReturn{
    private $pDbMgr;

    public function __construct(DatabaseManager $pDbMgr)
    {

        $this->pDbMgr = $pDbMgr;
    }

    /**
     * @param $returnId
     * @return array
     */
    public function getListReturnItems($returnId) {

        $sSql = 'SELECT MRI.product_id, P.name, P.ean_13, MRI.quantity, MRI.confirmed_quantity, SL.container_id
                 FROM magazine_return_items AS MRI
                 LEFT JOIN stock_location_orders_items_lists_items AS SLO
                  ON MRI.stock_location_orders_items_lists_items_id = SLO.id
                 LEFT JOIN stock_location AS SL
                  ON SLO.stock_location_id = SL.id AND SLO.stock_location_magazine_type = SL.magazine_type
                 LEFT JOIN products AS P
                  ON P.id = MRI.product_id
                 WHERE magazine_return_id = '.$returnId;
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


}