<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 17.10.17
 * Time: 13:54
 */

namespace LIB\EntityManager\Entites;

/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-04-29
 * Time: 09:59
 */
class Magazine
{
    const TYPE_HIGH_STOCK_SUPPLIES = 'SH'; // B
    const TYPE_LOW_STOCK_SUPPLIES = 'SL'; // A
    const TYPE_DEFECT_DELIVERY = 'DD'; // defekty z dostawy
    const TYPE_DEFECT_REVEALED = 'DR'; // defekty ujawnione

    public static $MAGAZINE_NAME =
        [
            self::TYPE_LOW_STOCK_SUPPLIES => 'A',
            self::TYPE_HIGH_STOCK_SUPPLIES => 'B',
            self::TYPE_DEFECT_DELIVERY => 'DD',
            self::TYPE_DEFECT_REVEALED => 'DR',
        ];

  /**
   * @param $magazineType
   * @return mixed
   * @throws \Exception
   */
    public static function getMagazineNameByType($magazineType) {

      if (isset(self::$MAGAZINE_NAME[$magazineType])) {
          return self::$MAGAZINE_NAME[$magazineType];
      }
      throw new \Exception(_('Brak przekazanego typu: '.$magazineType));
    }


    /**
     * @param $productId
     * @return array
     */
    public function getProductLocations($productId) {
        global $pDbMgr;

        $sSql = '
        SELECT PS.*, M.name AS magazine_type
        FROM stock_location AS PS
        JOIN magazine AS M
          ON  M.type = PS.magazine_type
        WHERE PS.products_stock_id = '.$productId.'
        ';
        return $pDbMgr->GetAll('profit24', $sSql);
    }

}