<?php

namespace LIB\EntityManager\Entites;


class UsersGroups
{
    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct(\DatabaseManager $pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
    }


    function getGroups() {
        $sSql = 'SELECT id, name FROM users_groups LIMIT 100';

        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    function getUserGroups($iUserId) {
        $sSql = 'SELECT group_id FROM users_to_groups WHERE user_id = ' . $iUserId;

        return $this->pDbMgr->GetCol('profit24', $sSql);
    }

    function userInGroup($iUserId, $iGroupId) {
        $aUserGroups = $this->getUserGroups($iUserId);

        $bInGroup = false;
        foreach ( $aUserGroups as $iUserGroup ) {
            if ( $iUserGroup === $iGroupId ) {
                $bInGroup = true;
            }
        }

        return $bInGroup;
    }

    function getUsersInGroup($iGroupId) {
        $sSql = 'SELECT B.login, B.name, B.surname, A.user_id, A.group_id 
                FROM users_to_groups as A 
                JOIN users as B ON B.id = A.user_id 
                WHERE group_id = ' . $iGroupId;

        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    function addUserToGroup($iUserId, $iGroupId) {
        $aValues = [
            'user_id' => $iUserId,
            'group_id' => $iGroupId
        ];

        return $this->pDbMgr->Insert('profit24', 'users_to_groups', $aValues);
    }

    function removeUserFromGroup($iUserId, $iGroupId) {
        $sSql = 'DELETE FROM users_to_groups 
                WHERE user_id = ' . $iUserId . '
                AND group_id = ' . $iGroupId;

        $this->pDbMgr->Query('profit24', $sSql);
    }

    function removeGroup($iGroupId) {
        $sSql = 'DELETE FROM users_groups
            WHERE id = ' . $iGroupId;

        return $this->pDbMgr->Query('profit24', $sSql);
    }

    function getAlertForGroup($aUserGroups, $iUserId) {
        $sSql = "SELECT A.id, A.message, A.created, A.created_by FROM orders_to_review AS A
                             WHERE 
                             A.group_id IN (".implode(',', $aUserGroups).") AND
                             A.user_id IS NULL AND
                             A.disabled = '0' AND
                             A.alert = '1' AND
                             A.alert_datetime <= NOW() AND
                             NOT EXISTS (SELECT 1 FROM users_groups_alert_read AS B WHERE A.id = B.alert_id AND B.user_id = " . $iUserId . " AND B.read = 1)
                             LIMIT 1
                             ";

        return $this->pDbMgr->GetRow("profit24", $sSql);
    }
}