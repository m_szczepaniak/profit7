<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 31.10.17
 * Time: 12:24
 */

namespace LIB\EntityManager\Entites;

use DatabaseManager;
use Exception;
use LIB\EntityManager\Entites\OrdersItemsLists;

class StockLocationMissing
{

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * StockLocation constructor.
     * @param DatabaseManager $pDbMgr
     */
    public function __construct(DatabaseManager $pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
    }

    private function findEqual($orderItemId) {
        $sSql = 'SELECT id FROM stock_location_missing WHERE order_item_id = '.$orderItemId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    public function insert($productId, $orderItemId, $requiredQuantity, $availableQuantity) {
        $id = $this->findEqual($orderItemId);
        if ($id <= 0) {
            $values = [
                'product_id' => $productId,
                'order_item_id' => $orderItemId,
                'required_quantity' => $requiredQuantity,
                'available_quantity' => $availableQuantity,
                'created' => 'NOW()'
            ];
            $this->pDbMgr->Insert('profit24', 'stock_location_missing', $values);
        }
        return true;
    }
}