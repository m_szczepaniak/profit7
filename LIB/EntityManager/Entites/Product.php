<?php
namespace LIB\EntityManager\Entites;

/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-04-29
 * Time: 09:59
 */
class Product
{
    const PRODUCT_TYPE_BOOK = 'K';
    const PRODUCT_TYPE_TOY ='Z';
    const PRODUCT_TYPE_GAME = 'G';
    const PRODUCT_TYPE_LEGO = 'L';
    const PRODUCT_TYPE_PUZZLE = 'P';
    const TYPE_SCHOOL_AND_OFFICE_SUPPLIES = '3';
    const TYPE_HOUSE_AND_GARDEN = '4';
    const TYPE_CHILD = '5';
    const TYPE_E_BOOK = '6';
    const TYPE_ELECTRONICS = '7';
    const TYPE_FILM = '8';
    const TYPE_GADGETS_AND_ACCESSORIES = '9';
    const TYPE_COMPUTER_GAME = 'J';
    const TYPE_MUSIC = 'M';
    const TYPE_SPORT_AND_TOURISM = 'S';
    const TYPE_HEALTH_AND_BEAUTY = 'H';
    const TYPE_AUDIO = 'A';
    const PRODUCT_DEFAULT_TYPE =  self::PRODUCT_TYPE_BOOK;

    // UWAGA 14 dni produkt ma być dostępny
    // zlecił kmalz
    const PREVIEW_DAYS_AVAILABLE = 14;

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    /**
     * @return array
     */
    public static function getProductTypes() {
        return [
            _('Książka') => self::PRODUCT_TYPE_BOOK,
            _('Zabawka') => self::PRODUCT_TYPE_TOY,
            _('Gra') => self::PRODUCT_TYPE_GAME,
            _('Puzzle') => self::PRODUCT_TYPE_PUZZLE,
            _('LEGO') => self::PRODUCT_TYPE_LEGO,
            _('Artykuły szkolne i biurowe') => self::TYPE_SCHOOL_AND_OFFICE_SUPPLIES,
            _('Dom i ogród') => self::TYPE_HOUSE_AND_GARDEN,
            _('Dziecko') => self::TYPE_CHILD,
            _('Ebook') => self::TYPE_E_BOOK,
            _('Elektronika') => self::TYPE_ELECTRONICS,
            _('Film') => self::TYPE_FILM,
            _('Gadżety i galanteria') => self::TYPE_GADGETS_AND_ACCESSORIES,
            _('Gra komputerowa') => self::TYPE_COMPUTER_GAME,
            _('Muzyka') => self::TYPE_MUSIC,
            _('Sport i turystyka') => self::TYPE_SPORT_AND_TOURISM,
            _('Zdrowie i uroda') => self::TYPE_HEALTH_AND_BEAUTY,
            _('Audiobook') => self::TYPE_AUDIO,
        ];
    }


    public function __construct(\DatabaseManager $pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
    }


    /**
     *
     * @param string $sProdIdent
     * @return array
     */
    public function findProductByIdent($sProdIdent)
    {

        $sSql = 'SELECT PS.id, CONCAT(PS.profit_g_location, " ", PS.profit_x_location) AS location, PS.profit_g_act_stock, P.name
             FROM products AS P
             JOIN products_stock AS PS
              ON P.id = PS.id
             WHERE 
                (
                  P.isbn_plain = "%1$s" OR 
                  P.isbn_10 = "%1$s" OR 
                  P.isbn_13 = "%1$s" OR 
                  P.ean_13 = "%1$s"
                )'; // OR streamsoft_indeks = "%1$s"
        $sSql = sprintf($sSql, $sProdIdent);
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }


    /**
     * @param $iId
     * @return null|string
     */
    public function getProductImagePath($iId)
    {
        global $aConfig;

        $sSql = "SELECT directory, photo, mime
					 FROM products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
        $aImg =& $this->pDbMgr->GetRow('profit24', $sSql);

        if (!empty($aImg)) {
            $sDir = $aConfig['common']['photo_dir'];
            if (!empty($aImg['directory'])) {
                $sDir .= '/'.$aImg['directory'];
            }

            if ($aSize = @getimagesize($aConfig['common']['client_base_path'].$sDir.'/'.$aImg['photo'])) {

                if (!empty($aSize)) {
                    return $sDir.'/'.$aImg['photo'];
                }
            }
        }
        return null;
    }
}