<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 09.11.17
 * Time: 11:59
 */
namespace LIB\EntityManager\Entites;

use DatabaseManager;
use LIB\orders\listType\filters\onlyDouble;
use LIB\orders\listType\filters\typeFilter;
use LIB\orders\listType\getOrderData;
class SellPredict
{
    const TYPE_ORDER_PREDICT = 1;// sprzedaż i przewidywanie sprzedaży
    const TYPE_MANUAL = 2;// ręcznie dodane do listy
    const TYPE_SELL_PREDICT = 3;// przewidywanie sprzedaży

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_COMPLETATION = 2;
    const STATUS_CLOSED = 3;

    private static $orderByType = [
        self::TYPE_ORDER_PREDICT => '1',
        self::TYPE_MANUAL => '2',
        self::TYPE_SELL_PREDICT => '3',
    ];

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;
    private $stockLocationMissing;
    protected $productAvailableStockLocationCache;

    public function __construct(DatabaseManager $pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
        $this->magazineLocation = new StockLocation($pDbMgr);
        $this->stockLocationMissing = new StockLocationMissing($pDbMgr);
    }

    /**
     * @return array
     */
    public static function getOrderByType($type)
    {
        return self::$orderByType[$type];
    }


    /**
     * @param $id
     * @param $quantity
     * @return mixed
     */
    public function updateQuantity($id, $quantity)
    {
        $value = [
            'quantity' => $quantity
        ];
        return $this->pDbMgr->Update('profit24', 'sell_predict', $value, ' id = ' . $id);
    }

    /**
     * @param $productId
     * @param $quantity
     * @param $type
     * @param $createdBy
     * @return int
     */
    public function insert($productId, $quantity, $type, $createdBy)
    {
        $value = [
            'product_id' => $productId,
            'quantity' => $quantity,
            'type' => $type,
            'status' => '1',// aktywne
            'order_by' => self::getOrderByType($type),
            'created' => 'NOW()',
            'created_by' => $createdBy
        ];
        return $this->pDbMgr->Insert('profit24', 'sell_predict', $value);
    }

    /**
     * @param $productId
     * @param $quantity
     * @param $type
     * @param $createdBy
     * @return bool
     */
    public function insertIfNotExists($productId, $quantity, $type, $createdBy)
    {
        $sSql = 'SELECT id FROM sell_predict 
             WHERE 
             product_id = "' . $productId . '"
             AND status <> "' . self::STATUS_INACTIVE . '"
             AND status <> "'.self::STATUS_CLOSED.'"';

        $id = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($id > 0) {
            return true;
        }

        return $this->insert($productId, $quantity, $type, $createdBy);
    }

    /**
     * @param $id
     * @param $status
     * @return mixed
     */
    public function setStatus($id, $status)
    {
        $values = [
            'status' => $status
        ];
        return $this->pDbMgr->Update('profit24', 'sell_predict', $values, ' id = ' . $id);
    }


    public function setOrderItemListItem($id, $iOILIId)
    {
        $values = [
            'orders_items_lists_items_id' => $iOILIId
        ];
        return $this->pDbMgr->Update('profit24', 'sell_predict', $values, ' id = ' . $id);
    }

    /**
     * @param $orderId
     * @return array
     */
    private function getAllOrderItems($orderId) {

        $sSql = 'SELECT OI.quantity, OI.vat, OI.status, OI.source, OI.product_id
                     FROM orders_items AS OI
                     WHERE OI.order_id = ' . $orderId . '
                       AND OI.deleted = "0"';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * @param $productId
     * @return int
     */
    public function getProductsQuantityReadyToSend($productId, $orderRequiredQuantity, $iOrderItemId = 0)
    {

        $minBooksSupply = 10;
        $productAvailableQuantity = $this->getProductMagazineAvailableQuantityCache($productId, StockLocation::HIGH_MAGAZINE_TYPE);
        if ($orderRequiredQuantity > $productAvailableQuantity) {
            $this->stockLocationMissing->insert($productId, $iOrderItemId, $orderRequiredQuantity, $productAvailableQuantity);
        }
        $toOrderQuantity = 0;

        $aFilterProducts[] = $productId;
        // tutaj nie ma to już większego znaczenia
        // TODO wykluczyć już pobrane na listę

        $sSql = 'SELECT OI.quantity, OI.vat, OI.order_id
                 FROM orders_items AS OI
                 JOIN orders AS O
                  ON OI.order_id = O.id AND O.order_status IN ("1", "2")
                 WHERE OI.product_id = '.$productId.' 
                 AND OI.deleted = "0"
                 AND OI.status <> "4"
                 AND OI.get_ready_list = "0"
                 ';
        $ordersItems = $this->pDbMgr->GetAll('profit24', $sSql);
        if (!empty($ordersItems)) {
            foreach ($ordersItems as $orderItem) {

                $allOrdersItems = $this->getAllOrderItems($orderItem['order_id']);
                $bIsAllToys = true;
                $bIsAllOnMagazineHighLevelStock = true;
                $itemsQuantity = 0;
                foreach ($allOrdersItems as $ordersItemRow) {
                    if ($ordersItemRow['vat'] == 5) {
                        // jeśli jest na 5% to możemy doładowywać
                        $bIsAllToys = false;
                    }
                    $itemsQuantity += $ordersItemRow['quantity'];
                }
                // optymalizacja tutaj musi iść drugi raz pętla bo jest SQL, wiec sprawdzamy kluczowe warunki i
                // dopiero wtedy odpalamy SQL
                if ($bIsAllToys == true && ($itemsQuantity == 1 || $itemsQuantity == 2)) {
                    foreach ($allOrdersItems as $ordersItemRow) {
                        if ($ordersItemRow['status'] == '3' && $ordersItemRow['source'] == '51') {
                            // oba produkty są na wysokim to pomijamy !!!!
                            $quantityOnHighLevelMagazine = $this->getProductMagazineAvailableQuantityCache($ordersItemRow['product_id'], StockLocation::HIGH_MAGAZINE_TYPE);
                            if ($quantityOnHighLevelMagazine < $ordersItemRow['quantity']) {
                                $bIsAllOnMagazineHighLevelStock = false;
                            }
                        } else {
                            $bIsAllOnMagazineHighLevelStock = false;
                        }
                    }
                }


                if ($bIsAllOnMagazineHighLevelStock == true && $bIsAllToys == true && ($itemsQuantity == 1 || $itemsQuantity == 2)) {
                    // wszystkie produkty to zabawki i ilość szt. w zamówieniu to single lub double
                    // i ilosć na wysokim składowaniu jest wystarczajaca
                    // NIE doładowywujemy !!!!!!

                } else {
                    $toOrderQuantity += $orderItem['quantity'];
                }
            }

            // zamawiamy podówjną ilość tego co zostało zamówione
            $toOrderQuantity = $toOrderQuantity * 2;
            if (isset($ordersItems[0]) && $ordersItems[0]['vat'] == '5') {
                // minimum 10
                if ($toOrderQuantity < $minBooksSupply) {
                    $toOrderQuantity = $minBooksSupply;
                }
            }
            // jeśli na półce jest o 10 więcej
            if ($toOrderQuantity > $productAvailableQuantity) {
                // jeśli chcemy zamówić więcej niż jest dostepna aktualnie ilość na magazynie
                return $productAvailableQuantity;

            }
            return $toOrderQuantity;
        } else {
            return 0;
        }
    }


    /**
     * @param $productId
     * @param $magazineType
     * @return mixed
     */
    private function getProductMagazineAvailableQuantityCache($productId, $magazineType) {

        if (!isset($this->productAvailableStockLocationCache[$magazineType][$productId])) {
            $result = $this->magazineLocation->getProductMagazineAvailableQuantity($productId, $magazineType);
            $this->productAvailableStockLocationCache[$magazineType][$productId] = $result;
            return $result;
        } else {
            return $this->productAvailableStockLocationCache[$magazineType][$productId];
        }

    }

    /**
     *
     */
    public function doAutoSellPredict()
    {
        $toFilterByListType = [];
        // tutaj nie ma to już większego znaczenia
        $orderData = new getOrderData($this->pDbMgr, StockLocation::HIGH_MAGAZINE_TYPE);
        $orderReadyToSend = $orderData->getOrdersItemsListData(false, '', [], [], false);

        // analizujemy tutaj tylko produkty które są na wysokim składowaniu, a których nie ma na niskim składowaniu w wystarczającej ilości
        // wykluczyć stąd należy produkty z zamówień  single i double zabawkowe które są dostępne na wysokim składowaniu


        $arrayProductsOrders = $this->getProductsIdsArray($orderReadyToSend);
        $productsIsNotOnLowAndExistsOnHighLevel = $this->getProductsIsNotOnLowAndExistsOnHighLevel($arrayProductsOrders);
        foreach ($arrayProductsOrders as $productId => $data) {
            $sLowKey = $productId . '_' . Magazine::TYPE_LOW_STOCK_SUPPLIES;
            $sHighKey = $productId . '_' . Magazine::TYPE_HIGH_STOCK_SUPPLIES;

            if (isset($productsIsNotOnLowAndExistsOnHighLevel[$sLowKey])) {
                $defQuantity = $data['quantity'] - $productsIsNotOnLowAndExistsOnHighLevel[$sLowKey]['available'];
                if ($defQuantity <= 0) {
                    // olewamy mamy wystarczajaco dużo
                } else {
                    if (isset($productsIsNotOnLowAndExistsOnHighLevel[$sHighKey])) {
                        if ($productsIsNotOnLowAndExistsOnHighLevel[$sHighKey]['available'] >= $defQuantity) {
                            // ok powinno być git
                            // teraz sprawdzamy typy zamówień
                            // jeśli przejdzie filtry z B to nie dodajemy do zamówienia
                            foreach ($data['orders'] as $orderId) {
                                $toFilterByListType[$orderId] = $orderReadyToSend[$orderId];
                            }


                        }
                    }
                }
            }
        }

        $resultArray = $toFilterByListType;

        // przez róznice będziemy tu eliminować
        $oFilterDuplicates = new onlyDouble($this->pDbMgr);
        $oFilterDuplicates->setOrdersItems($toFilterByListType);
        $oFilterDuplicates->applyFilter();
        $toFilterByListType = $oFilterDuplicates->getFilteredOrdersItems();

        $oFilter = new typeFilter($this->pDbMgr);
        $oFilter->setOrdersItems($toFilterByListType);
        $oFilter->setFilterCollectingType('mix');
        $oFilter->applyFilter();
        $toFilterByListType = $oFilter->getFilteredOrdersItems();
        foreach ($toFilterByListType as $iOrderId => $value) {
            unset($resultArray[$iOrderId]);
        }

        foreach ($resultArray as $orderId => $items) {
            if (count($items) == 1) {
                foreach ($items as $item) {
                    if ($item['quantity'] == 1) {
                        // usuwamy single
                        unset($resultArray[$orderId]);
                    }
                }
            }
        }


        dump($resultArray);
        $resultArray;// - tu są dane do zamówienia
        foreach ($resultArray as $items) {
            foreach ($items as $item) {
                $quantityPredicted = $this->getProductsQuantityReadyToSend($item['product_id'], $item['quantity']);
//                $this->insertIfNotExists($productId, $quantityPredicted, SellPredict::TYPE_ORDER_PREDICT, 'auto-check-magazine-list-1');
            }

        }
        $d = 1;
    }


    /**
     * @param $arrayProductsOrders
     * @return array
     */
    private function getProductsIsNotOnLowAndExistsOnHighLevel($arrayProductsOrders)
    {

        $sSql = ' SELECT CONCAT(products_stock_id, "_", magazine_type), SUM(available) as available, products_stock_id
          FROM stock_location
          WHERE products_stock_id IN (' . implode(', ', array_keys($arrayProductsOrders)) . ')
          GROUP BY products_stock_id, magazine_type
        ';
        return $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, false);
    }

    /**
     * @param $orderReadyToSend
     */
    private function getProductsIdsArray($orderReadyToSend)
    {

        $arrayProductsOrders = [];
        foreach ($orderReadyToSend as $orderId => $ordersItems) {
            foreach ($ordersItems as $orderItem) {
                if ($orderItem['product_id'] > 0 &&
                    $orderItem['status'] == '3' &&
                    $orderItem['source'] == '51' &&
                    $orderItem['deleted'] == '0'
                ) {

                    $arrayProductsOrders[$orderItem['product_id']]['orders'][] = $orderItem['order_id'];
                    $arrayProductsOrders[$orderItem['product_id']]['quantity'] += $orderItem['quantity'];
                }
            }
        }
        return $arrayProductsOrders;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getActiveProductOnPredict($id)
    {
        $sSql = 'SELECT id FROM sell_predict WHERE product_id = '.$id.' AND status <> "'.SellPredict::STATUS_INACTIVE.'" AND status <> "'.SellPredict::STATUS_CLOSED.'" ';
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }


    /**
     * @param $iOrderItemId
     * @return bool
     */
    public function autoSellPredictOrderItem($iOrderItemId) {

        $sSql = 'SELECT id, OI.product_id, OI.quantity
                 FROM orders_items AS OI
                 WHERE OI.id = '.$iOrderItemId.' AND 
                 OI.deleted = "0" AND
                 OI.status = "3" AND
                 OI.source = "51"
                 ';
        $orderItemData = $this->pDbMgr->GetRow('profit24', $sSql);
        $iValidatedOrderItemId = $orderItemData['id'];
        if ($iValidatedOrderItemId > 0) {
            // dobra mamy to, sprawdzamy stan na niskim i wysokim
            $sSql = 'SELECT magazine_type, SUM(available)
                     FROM stock_location
                     WHERE products_stock_id = '.$orderItemData['product_id'].'
                     GROUP BY magazine_type';
            $availableMagazine = $this->pDbMgr->GetAssoc('profit24', $sSql);
            if (isset($availableMagazine[StockLocation::LOW_MAGAZINE_TYPE]) && $availableMagazine[StockLocation::LOW_MAGAZINE_TYPE] >= $orderItemData['quantity']) {
                // mamy wystarczająco na niskim
                return true;
            }

            if (isset($availableMagazine[StockLocation::HIGH_MAGAZINE_TYPE]) && $availableMagazine[StockLocation::HIGH_MAGAZINE_TYPE] > 0) {
                // mozemy coś zamówić z wysokiego składowania
                $deficiencyQuantity = $orderItemData['quantity'] - $availableMagazine[StockLocation::LOW_MAGAZINE_TYPE];
                if ($deficiencyQuantity > 0) {
                    $productQuantityPredicted = $this->getProductsQuantityReadyToSend($orderItemData['product_id'], $deficiencyQuantity, $iOrderItemId);
                    if ($productQuantityPredicted > 0) {
                        $this->insertIfNotExists($orderItemData['product_id'], $productQuantityPredicted, SellPredict::TYPE_ORDER_PREDICT, 'auto-check-magazine-list-1');
                    }
                }
            }
        }
        return true;
    }
}