<?php
/**
 * Klasa zarządzania zamówieniami w zakładkach
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-07 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders_containers;
class OrdersContainers {

  private $pDbMgr;
  
  function __construct() {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * Metoda usuwa alert z kontenerow
   * 
   * @param type $iOrderId
   * @param type $cReasonType
   * @return type
   */
  public function checkContainer($iOrderId, $cReasonType) {
    
    $aValues = array(
        'checked' => '1',
        'checked_by' => $_SESSION['user']['name']
    );
    return $this->pDbMgr->Update('profit24', 'orders_reasons_containers', $aValues, ' id = '.$iOrderId. ' AND orders_reasons_type = "'.$cReasonType.'" ');
  }// end of checkContainer() method
  

  /**
   * Metoda dodaje nową informację do typów zakładek
   * 
   * @param int $iOrderId
   * @param char $cReasonType
   * @param bool $bCheckExists
   * @return int
   */
  public function addOrderToContainers($iOrderId, $cReasonType, $bCheckExists = false) {
    $bExists = false;
    
    if ($bCheckExists == true) {
      $bExists = $this->checkExists($iOrderId, $cReasonType);
    }
    if ($bExists === false) {
      $aValues = array(
          'orders_reasons_type' => $cReasonType,
          'order_id' => $iOrderId,
          'checked' => '0',
          'created' => 'NOW()'
      );
      return $this->pDbMgr->Insert('profit24', 'orders_reasons_containers', $aValues);
    }
    return true;
  }
  
  
  /**
   * Metoda sprawdza, czy zamówienie istnieje już w zakładce
   * Uwaga metoda nie sprawdza, czy dane zamówienie zostało odznaczone
   * 
   * @param int $iOrderId
   * @param char $cReasonType
   * @return bool
   */
  private function checkExists($iOrderId, $cReasonType) {
    
    $sSql = 'SELECT id 
             FROM orders_reasons_containers
             WHERE order_id = '.$iOrderId.' 
               AND orders_reasons_type = "'.$cReasonType.'"';
    return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? TRUE : FALSE);
  }// end of checkExists() method
  
  /**
   * Metoda tworzy widok zamówień w zakładkach
   * 
   * @param char $cReasonType
   * @return HTML
   */
  public function getOrdersContainersView($cReasonType, $aCols, $aRecordsHeader, $sParentName) {
    global $pDbMgr;
		// dolaczenie klasy View
		include_once('View/View.class.php');
		$aTransportMeansList = [];
    
		// zapamietanie opcji
		rememberViewState($this->sModule.'_'.$cReasonType);
		$aHeader = array(
			'header'	=> _('Zamówienia'),
			'refresh'	=> true,
			'search'	=> true,
			'checkboxes'	=> false
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);
    
    // domyslnieie aktywne TAK - 
    if (!isset($_POST['f_checked'])) {
      $_POST['f_checked'] = '0';
    }
    
    $sSql = $this->_getCountRecordsSQL($cReasonType);
    $iRowCount = $this->pDbMgr->GetOne('profit24', $sSql);
    
    $pView = new \View('send_history_'.$cReasonType, $aHeader, $aAttribs);
    $pView->AddRecordsHeader($aRecordsHeader);
    $aChecked = array(
        array('label' => _('NIE'), 'value' => '0'),
        array('label' => _('TAK'), 'value' => '1'),
    );
    $pView->AddFilter('f_checked', _('Wyłączone'), addDefaultValue($aChecked, _('Wszystkie')), $_POST['f_checked']);

    if ($cReasonType == 'T') {
      $aTransportMeansList = $this->_getTransportMeansList();
      $pView->AddFilter('f_transport', _('Transport'), addDefaultValue($aTransportMeansList, _('Wszystkie')), $_POST['f_transport']);    
    }
    
		if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
      
      $sSql = $this->_getRecordsSQL($aCols, $cReasonType, $iCurrentPage);
      
      foreach ($aTransportMeansList as $aTransportMean) {
          $aTransportNames[$aTransportMean['value']] = $aTransportMean['label'];
      }
      
      $oShipment = new \orders\Shipment('tba', $this->pDbMgr, $bTestMode);
      $aRecords = $this->pDbMgr->GetAll('profit24', $sSql);
      foreach ($aRecords as $iKey => $aRecord) {
        $aRecords[$iKey]['action']['links']['details'] = phpSelf(array('module' => 'm_zamowienia', 'do' => 'details', 'id' => $aRecord['order_id'], 'ppid' => '0', 'save_referer_url' => urlencode('Duplikaty')), array('action'));
        if ($aRecord['checked'] == '1') {
          $aRecords[$iKey]['disabled'][] = 'activate';
        }
        if (isset($aRecord['checked'])) {
          $aRecords[$iKey]['checked'] = $aRecord['checked'] == '1' ? _('TAK') : _('NIE');
        }
 
        if (isset($aRecord['transport_id'])) {
          $aRecords[$iKey]['transport_id'] = $aTransportNames[$aRecord['transport_id']];
        }        
        
        $iTransportId = $aRecord['transport_id'];
        $sPostal = substr($aRecord['postal_city'], 0, 6);
        $sCity = substr($aRecord['postal_city'], 7);
        $bValidatePostcode = $oShipment->validatePostcode('PointsOfReceipt', $sPostal);  
        
        // wyróżniamy kolorem, co jest uznane za błąd
        // błąd, jeśli istnieje kod pocztowy i nie pokrywa się z miastem
        if (isset($aRecord['postal_city']) && (!$this->checkCityPostal($sCity, $sPostal))) {
          $aRecords[$iKey]['postal_city'] = '<div style="color:#960000; font-weight:bold;">'.$aRecords[$iKey]['postal_city'].'</div>';
        
        // dla TBA (id transportu 7) - błąd, jeśli kod pocztowy jest poza obszarem doręczeń  TBA
        } else if ($iTransportId == '7' && isset($aRecord['postal_city']) && (!$bValidatePostcode)) {
          $aRecords[$iKey]['postal_city'] = '<div style="color:#960000; font-weight:bold;">'.$aRecords[$iKey]['postal_city'].'</div>';
        
        // w pozostałych przypadkach przyjmujemy, że błędny jest punkt odbioru  
        } else {
          if (isset($aRecord['point_of_receipt'])) {
            $aRecords[$iKey]['point_of_receipt'] = '<div style="color:#960000; font-weight:bold;">'.$aRecords[$iKey]['point_of_receipt'].'</div>';        
          }
        }
        
        if ($aRecord['order_status'] != '0') {
          $aRecords[$iKey]['disabled'][] = 'auto-source';
        }
        unset($aRecords[$iKey]['order_status']);
      }
			$aColSettings = array(
				'id' => array (
					'show'	=> false
				),
				'order_number' => array ('link'	=> phpSelf(array('module' => 'm_zamowienia', 'do' => 'details', 'id' => '{order_id}', 'ppid' => '0', 'save_referer_url' => urlencode($sParentName)), array('action'))),
          
				'action' => array (
					'actions'	=> array ('auto-source', 'activate', 'details'),
					'params' => array (
                        'auto-source' => array('id' => '{order_id}'),
                        'activate'	=> array('id' => '{id}'),
//												'details'	=> array('id' => '{order_id}')
											),
					'show' => false	
				) 
 			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
    }
    
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array()
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		
		return $pView->Show();
  }// end of getOrdersContainersView() method
  
  
  /**
   * Metoda pobiera ilosc rekordow
   * 
   * @return type
   */
  private function _getCountRecordsSQL($cReasonType) {
    
    $aCols = array('DISTINCT count(O.id)');
    $sSql = $this->_getOrdersContainersDataSQL($aCols, $cReasonType);
    $sSql .= $this->_getFilterSQL();
    return $sSql;
  }// end of _getCountRecordsSQL() method
  
  /**
   * 
   * @param char $cReasonType
   * @param int $iCurrentPage
   * @return string
   */
  private function _getRecordsSQL($aCols, $cReasonType, $iCurrentPage) {
    global $aConfig;
    
    $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
    $iStartFrom = ($iCurrentPage - 1) * $iPerPage;

    $sSql = $this->_getOrdersContainersDataSQL($aCols, $cReasonType);

    $sSql .= $this->_getFilterSQL();

    if (isset($_GET['sort'])) {
        $sSql .= ' ORDER BY '. $_GET['sort'].' '.$_GET['order'] ;
    } else {
      if ($cReasonType == 'D') {
        // w kolejności emaili
        $sSql .= ' ORDER BY O.email, O.client_ip, O.id ASC ';
      } else {
        // od najstarszej daty wysyłki
        $sSql .= ' ORDER BY O.shipment_date_expected ASC, O.id ASC ';
      }
    }
    $sSql .= " LIMIT ".$iStartFrom.", ".$iPerPage;
    return $sSql;
  }// end of getRecordsSQL() method
  
  
  /**
   * Filtr SQL
   * 
   * @return string
   */
  private function _getFilterSQL() {
    
    $sSql = '';
    $sSql .= ( isset($_POST['search']) && !empty($_POST['search']) ? 
            " AND (
              email LIKE '%".$_POST['search']."%' OR 
              client_ip LIKE '%".$_POST['search']."%' OR 
              order_number LIKE '%".$_POST['search']."%'
              ) " 
            : '');
    $sSql .= ( isset($_POST['f_checked']) && $_POST['f_checked'] != '' ? ' AND checked = "'.$_POST['f_checked'].'"' : '');
    $sSql .= ( isset($_POST['f_transport']) && $_POST['f_transport'] != '' ? ' AND O.transport_id = "'.$_POST['f_transport'].'"' : '');
    return $sSql;
  }// end of _getFilterSQL() method
  
  
  /**
   * Metoda pobiera główną część zapytania pobierającego informacje 
   *  na temat zamówien w zakładce
   * 
   * @param array $aCols
   * @param char $cReasonType
   * @return string
   */
  private function _getOrdersContainersDataSQL($aCols, $cReasonType) {
    
    $sSql = 'SELECT '.implode(', ', $aCols).'
      FROM orders_reasons_containers AS ORC
      JOIN orders AS O 
      ON ORC.order_id = O.id
      WHERE 
      ORC.orders_reasons_type = "'.$cReasonType.'"
      ';
    return $sSql;
  }// end of _getOrdersContainersDataSQL() method

  
  /**
   * Metoda usuwa stare informacje o zamówiniach w zakładkach
   * 
   */
  public function autoClearOldContainers() {
    
    $sSql = 'DELETE FROM orders_reasons_containers 
             WHERE 
              created < DATE_SUB(CURDATE(), INTERVAL 1 MONTH ) AND 
              checked = "1" ';
    $this->pDbMgr->Query('profit24', $sSql);
  }// end of autoClearOldContainers() method
  

  /**
   * Metoda zwraca listę metod transportu dla wygenerowania listy wyboru filtra
   * 
   * @return array
   */
  private function _getTransportMeansList() {
    
    $sSql = 'SELECT id AS value, name AS label 
      FROM orders_transport_means';
    
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of _getTransportMeansList() method
  
  
	// metoda weryfikuje, czy przekazany kod pocztowy jest poprawnym kodem dla przekazanej miejscowości
	function checkCityPostal($sCity,$sPostal){
		global $pDbMgr, $aConfig;
    
    include_once($aConfig['common']['client_base_path'].'LIB/autoloader.php');
        
		$sSql = "SELECT count(1)
						FROM ".$aConfig['tabls']['prefix']."users_accounts_postals
						WHERE city = ".\Common::Quote(stripslashes($sCity)).
						 " AND postal = ".\Common::Quote(stripslashes($sPostal));
		return (intval(\Common::GetOne($sSql)) > 0);
	}
  
}// end of Class
