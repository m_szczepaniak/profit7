<?php
/**
 * Klasa zarządzania zamówieniami w zakładkach
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-11-07 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders_containers;
class OrdersPausedCollection extends OrdersContainers {

  private $pDbMgr;
  
  function __construct() {
    global $pDbMgr;
    $this->pDbMgr = $pDbMgr;
  }
  


  /**
   * Metoda tworzy widok zamówień w zakładkach
   * 
   * @param char $cReasonType
   * @return HTML
   */
  public function getOrdersContainersView($aCols, $aRecordsHeader, $sParentName) {
    global $pDbMgr;
		// dolaczenie klasy View
		include_once('View/View.class.php');
		$aTransportMeansList = [];
    
		// zapamietanie opcji
		rememberViewState($this->sModule.'_paused_collection');
		$aHeader = array(
			'header'	=> _('Zamówienia'),
			'refresh'	=> true,
			'search'	=> true,
			'checkboxes'	=> false,
		);
		$aAttribs = array(
			'width'				=> '100%',
			'border'			=> 0,
			'cellspacing'	=> 0,
			'cellpadding'	=> 0,
			'class'				=> 'viewHeaderTable'
		);

    $sSql = $this->_getCountRecordsSQL();
    $iRowCount = $this->pDbMgr->GetOne('profit24', $sSql);
    
    $pView = new \View('send_history_paused_collection', $aHeader, $aAttribs);
    $pView->AddRecordsHeader($aRecordsHeader);

      $aTransportMeansList = $this->_getTransportMeansList();
      $pView->AddFilter('f_transport', _('Transport'), addDefaultValue($aTransportMeansList, _('Wszystkie')), $_POST['f_transport']);

      if ($iRowCount > 0) {
			// dodanie Pagera do widoku
			$iCurrentPage = $pView->AddPager($iRowCount);
      
      $sSql = $this->_getRecordsSQL($aCols, $iCurrentPage);
      
      foreach ($aTransportMeansList as $aTransportMean) {
          $aTransportNames[$aTransportMean['value']] = $aTransportMean['label'];
      }
      
      $oShipment = new \orders\Shipment('tba', $this->pDbMgr);
      $aRecords = $this->pDbMgr->GetAll('profit24', $sSql);
      foreach ($aRecords as $iKey => $aRecord) {
        $aRecords[$iKey]['action']['links']['details'] = phpSelf(array('module' => 'm_zamowienia', 'do' => 'details', 'id' => $aRecord['order_id'], 'ppid' => '0', 'save_referer_url' => urlencode('Duplikaty')), array('action'));

        if (isset($aRecord['transport_id'])) {
          $aRecords[$iKey]['transport_id'] = $aTransportNames[$aRecord['transport_id']];
        }

        unset($aRecords[$iKey]['order_status']);
      }
			$aColSettings = array(
				'id' => array (
					'show'	=> false
				),
				'order_number' => array ('link'	=> phpSelf(array('module' => 'm_zamowienia', 'do' => 'details', 'id' => '{order_id}', 'ppid' => '0', 'save_referer_url' => urlencode($sParentName)), array('action'))),
          
				'action' => array (
					'params' => array (
											),
					'show' => false
				)
 			);
			// dodanie rekordow do widoku
			$pView->AddRecords($aRecords, $aColSettings);
    }
    
		// dodanie stopki do widoku
		$aRecordsFooter = array(
			array()
		);
		$pView->AddRecordsFooter($aRecordsFooter);
		
		return $pView->Show();
  }// end of getOrdersContainersView() method
  
  
  /**
   * Metoda pobiera ilosc rekordow
   * 
   * @return type
   */
  private function _getCountRecordsSQL() {
    
    $aCols = array('count(O.id)');
    $sSql = $this->_getOrdersContainersDataSQL($aCols);
    $sSql .= $this->_getFilterSQL();
    return $sSql;
  }// end of _getCountRecordsSQL() method
  
  /**
   * 
   * @param char $cReasonType
   * @param int $iCurrentPage
   * @return string
   */
  private function _getRecordsSQL($aCols, $iCurrentPage) {
    global $aConfig;
    
    $iPerPage = isset($_POST['per_page']) && !empty($_POST['per_page']) ? $_POST['per_page'] : $aConfig['default']['per_page'];
    $iStartFrom = ($iCurrentPage - 1) * $iPerPage;

    $sSql = $this->_getOrdersContainersDataSQL($aCols);
    $sSql .= ' GROUP BY O.id ';

    $sSql .= $this->_getFilterSQL();


    if (isset($_GET['sort'])) {
        $sSql .= ' ORDER BY '. $_GET['sort'].' '.$_GET['order'] ;
    } else {
    }
    $sSql .= " LIMIT ".$iStartFrom.", ".$iPerPage;
    return $sSql;
  }// end of getRecordsSQL() method
  
  
  /**
   * Filtr SQL
   * 
   * @return string
   */
  private function _getFilterSQL() {
    
    $sSql = '';
    $sSql .= ( isset($_POST['search']) && !empty($_POST['search']) ?
            " AND (
              O.email LIKE '%".$_POST['search']."%' OR 
              O.order_number LIKE '%".$_POST['search']."%'
              ) "
            : '');
    $sSql .= ( isset($_POST['f_transport']) && $_POST['f_transport'] != '' ? ' AND O.transport_id = "'.$_POST['f_transport'].'"' : '');
    return $sSql;
  }// end of _getFilterSQL() method
  
  
  /**
   * Metoda pobiera główną część zapytania pobierającego informacje 
   *  na temat zamówien w zakładce
   * 
   * @param array $aCols
   * @param char $cReasonType
   * @return string
   */
  private function _getOrdersContainersDataSQL($aCols) {
    
    $sSql = 'SELECT '.implode(', ', $aCols).'
      FROM collecting_high_level AS CHL
      JOIN orders_items AS OI 
        ON CHL.product_id = OI.product_id 
        AND OI.deleted = "0"
        AND OI.source = "51"
        AND OI.status = "3"
      JOIN orders AS O 
        ON OI.order_id = O.id 
            AND O.order_status <> "3" 
            AND O.order_status <> "4" 
            AND O.order_status <> "5"
      WHERE 
        CHL.completed = "0"
      ';
    return $sSql;
  }// end of _getOrdersContainersDataSQL() method


  /**
   * Metoda zwraca listę metod transportu dla wygenerowania listy wyboru filtra
   * 
   * @return array
   */
  private function _getTransportMeansList() {
    
    $sSql = 'SELECT id AS value, name AS label 
      FROM orders_transport_means';
    
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }// end of _getTransportMeansList() method

}// end of Class
