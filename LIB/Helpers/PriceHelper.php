<?php
namespace LIB\Helpers;

/**
 * Class PriceHelper
 * @package Profit\UtilsBundle\Helpers
 */
class PriceHelper
{
    /**
     * @param $number
     * @param int $decimals
     * @param string $decPoint
     * @param string $thousandsSep
     * @return float
     */
    public static function getPrice($number, $decimals = 2, $decPoint = ',', $thousandsSep = '')
    {
        $price = number_format((double)str_replace(',', '.', $number), $decimals, $decPoint, $thousandsSep);

        return (double)str_replace(',', '.', $price);
    }

    /**
     * @param $number
     * @param int $decimals
     * @param string $decPoint
     * @param string $thousandsSep
     * @return float
     */
    public static function getPercent($number, $decimals = 4, $decPoint = ',', $thousandsSep = '')
    {
        $price = number_format((double)str_replace(',', '.', $number), $decimals, $decPoint, $thousandsSep);

        return (double)str_replace(',', '.', $price);
    }

    /**
     * @param $number
     * @param int $decimals
     * @param string $decPoint
     * @param string $thousandsSep
     * @return string
     */
    public static function priceFilter($number, $decimals = 2, $decPoint = ',', $thousandsSep = '')
    {
        $number = (double)str_replace(',', '.', $number);
        $price = number_format($number, $decimals, $decPoint, $thousandsSep);

        return $price;
    }

    /**
     * @XXX NIE UŻYWAĆ W KODZIE, U NAS MA SIĘ WSZYSYKO LICZYĆ OD BRUTTO !!!!
     * NIE WYWALAC, w specyficznych przypadkach jest wykorzystywane
     *
     * @param $priceNetto
     * @param string $vat
     * @return float
     */
    public static function priceBruttoByNetto($priceNetto, $vat)
    {
        return PriceHelper::getPrice(PriceHelper::getPrice($priceNetto) * (1 + PriceHelper::getPrice($vat) / 100));
    }


    /**
     * @param $priceBrutto
     * @param string $vat
     * @return float
     */
    public static function priceNettoByBrutto($priceBrutto, $vat)
    {
        return PriceHelper::getPrice(PriceHelper::getPrice($priceBrutto) / (1 + PriceHelper::getPrice($vat) / 100));
    }

    /**
     * Konwertuje wartośc całkowitą na postać zapisaną słownie
     * @param int $iVal - wartośc całkowita
     * @return string - wartosc słownie
     */
    public static function intValue2words($iVal)
    {
        $sStr = '';
        $bMinus = false;
        $iRzad = 0;
        $j = 0;
        $aJednosci = array('', ' jeden', ' dwa', ' trzy', ' cztery', ' pięć', ' sześć', ' siedem', ' osiem', ' dziewięć');
        $aNascie = array('dziesięć', ' jedenaście', ' dwanaście', ' trzynaście', ' czternaście', ' piętnaście', ' szesnaście', ' siedemnaście', ' osiemnaście', ' dziewiętnaście');
        $aDziesiatki = array('', ' dziesięć', ' dwadzieścia', ' trzydzieści', ' czterdzieści', ' pięćdziesiąt', ' sześćdziesiąt', ' siedemdziesiąt', ' osiemdziesiąt', ' dziewięćdziesiąt');
        $aSetki = array('', ' sto', ' dwieście', ' trzysta', ' czterysta', ' pięćset', ' sześćset', ' siedemset', ' osiemset', ' dziewięćset');
        $aRzedy = array('', ' tys.', ' mln.', ' mld.', ' bln.', ' bld.');

        if ($iVal < 0) {
            $bMinus = true;
            $iVal = -$iVal;
        }

        if ($iVal == 0) $sStr = 'zero';

        while ($iVal > 0) {
            //	dump($iVal .' - '.$sStr);
            $iRest = (int)($iVal % 10);
            $iVal /= 10;
            $iVal = (int)$iVal;
            if (($j == 0) && ($iVal % 100 != 0 || $iVal < 100)) {
                $sStr = $aRzedy[$iRzad] . $sStr;
            }
            if (($j == 0) && ($iVal % 10 != 1)) {
                $sStr = $aJednosci[$iRest] . $sStr;
            }
            if (($j == 0) && ($iVal % 10 == 1)) {
                $sStr = $aNascie[$iRest] . $sStr;
                $iVal /= 10;
                $iVal = (int)$iVal;
                $j += 2;
                continue;
            }
            if ($j == 1) {
                $sStr = $aDziesiatki[$iRest] . $sStr;
            }
            if ($j == 2) {
                $sStr = $aSetki[$iRest] . $sStr;
                $j = -1;
                $iRzad++;
            }
            $j++;
        }

        if ($bMinus) {
            $sStr = "minus" . $sStr;
        }
        return $sStr;
    }

    /**
     * Funkcja konwertuje cene na postać słowną
     * @param float $fVal - wartośc liczbowa
     * @return string - zapis słowny
     */
    public static function price2Words($fVal)
    {
        $iPrice = (int)$fVal;
        $iFrac = (int)round((100 * ($fVal - $iPrice)));
        $sStr = self::intValue2words($iPrice) . ' ' . 'zł';
        $sStr .= ' ' . trim(self::intValue2words($iFrac)) . ' ' . 'gr.';
        return $sStr;
    }
}