<?php

namespace LIB\Helpers;

class FileHelper
{
    public static function createPath($path)
    {
        if(@mkdir($path) or file_exists($path)) return true;
        return (self::createPath(dirname($path)) and mkdir($path, 0777));
    }
}