<?php

namespace LIB\Helpers;

class SearchHelper
{
    public static $searchFields = [
        'q',
        'top_cat',
        'cat',
        'autor',
        'year',
        'publisher',
        'series',
        'tag',
        'isbn',
        'price_from',
        'price_to',
        'sex',
        'aval'
    ];

    public static function getPassedSearchParametersAmount()
    {
        $count = 0;

        foreach(self::$searchFields as $parameter) {

            $param = trim($_GET[$parameter]);

            if (null !== $param && "" != $param) {

                $count++;
            }
        }

        return $count;
    }
}
