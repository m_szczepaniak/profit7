<?php

namespace LIB\Helpers;


class DbHelper
{
    /**
     * Robi where i and z tablicy kolumna => wartosc
     *
     * @param array $where
     * @return string
     */
    public static function createWhereCondition(array $where = [])
    {
        $whereCondidion = '';
        if (false === empty($where)){
            $defaultCnd = "WHERE";
            $i = 0;
            foreach($where as $condidtion){
                if ($i > 0){
                    $defaultCnd = 'AND';
                }

                $whereCondidion .= $defaultCnd.' '.$condidtion['column'].' = '.$condidtion['value'].' ';
                $i++;
            }
        }

        return $whereCondidion;
    }

} 