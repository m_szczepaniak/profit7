<?php

namespace LIB\Helpers;

class ArrayHelper
{
    /**
     * Zwraca tablice key => value
     *
     * @param $array
     * @param $key
     * @param $value
     *
     * @return array
     */
    public static function toKeyValue($array, $key, $value)
    {
        $result = [];

        foreach($array as $row) {
            $result[$row[$key]] = $row[$value];
        }

        return $result;
    }

    /**
     * @param $key
     * @param $array
     *
     * @return array
     */
    public static function toKeyValues($key, $array)
    {
        $result = [];

        foreach($array as $row) {
            $result[$row[$key]] = $row;
        }

        return $result;
    }

    /**
     * Z asocjacyjnej tablicy wejściowej tworzy nową tablicę z wartościami kolumny
     * 
     * @see http://php.net/manual/en/function.array-column.php
     * @param array $array
     * @param string $key
     * @return array
     */
    public static function arrayColumn($array, $key)
    {
        $newArray = [];

        foreach($array as $element) {
            $newArray[] = $element[$key];
        }

        return $newArray;
    }

    /**
     * Merguje dwie tablice i sumuje wartość wybranej kolumny
     * 
     * @param array $array1
     * @param array $array2
     * @param string $key
     * @return array
     */
    public static function sumColumnArrays($array1, $array2, $key)
    {
      $return = [];
      foreach ($array1 as $key1 => $value1) {
        $return[$key1] = $value1;
        if (isset($array2[$key1])) {
          $return[$key1][$key] += $array2[$key1][$key];
        }
      }
      
      foreach ($array2 as $key2 => $value2) {
        if (!isset($array1[$key2])) {
          $return[$key2] = $value2;
        }
      }
      return $return;
    }

    public static function getImplodedArrayColumn($array, $key, $separator = ',')
    {
        return implode($separator, self::toKeyValue($array, $key, $key));
    }

    public static function sumArrayColumn($array, $key)
    {
        $sum = 0;

        foreach($array as $value) {
            $sum += (int)$value[$key];
        }

        return $sum;
    }

    public static function sortByColumn(&$array, $key, $direction = "asc")
    {
        usort($array, function($a, $b) use ($key, $direction) {
            if($direction == 'asc') {
                $return = $a[$key] + $b[$key];
            } else {
                $return = $b[$key] - $a[$key];
            }

            return $return;
        });
    }

    public static function groupByArrayValue(array $array, $keyElement)
    {
        $newArray = [];

        foreach($array as $key => $value){

            $newArray[$value[$keyElement]][$key] = $value;
        }

        return $newArray;
    }
}
