<?php

namespace LIB\Helpers;

use Exception;

class MailHelper
{
    public static function getMailAttachments($symbol, $website)
    {
        global $pDbMgr;

        $attachments = $pDbMgr->GetCol('profit24', "SELECT path FROM email_attachment WHERE template = '$symbol' AND website_id = $website");

        if (empty($attachments)) {

            return [];
        }

        $newAttachments = [];

        foreach($attachments as $attachment) {

            $finalPath = realpath($_SERVER['DOCUMENT_ROOT'].$attachment);

            if (empty($finalPath)) {

                continue;
            }

            $newAttachments[] = [$finalPath];
        }

        return $newAttachments;
    }
}
