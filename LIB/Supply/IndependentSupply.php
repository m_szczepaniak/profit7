<?php
/**
 * Created by PhpStorm.
 * User: lukasz
 * Date: 30.10.17
 * Time: 11:27
 */

namespace Supply;

use LIB\EntityManager\Entites\OrdersItemsLists;

class IndependentSupply
{
    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct()
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;
    }


    /**
     * @param array $aProductsScanned
     * @return bool
     * @throws \Exception
     */
    public function createIndependentSupplyList(array $aProductsScanned, $iOSHId = 0)
    {
        $aValues = [
            'package_number' => 'TMP',
            'document_number' => $this->generateDocumentNumber(),
            'closed' => '0',
            'type' => OrdersItemsLists::INDEPENDENT_SUPPLY_DOCUMENT_TYPE,
            'get_from_train' => '0',
            'is_g' => '0',
            'created' => 'NOW()',
            'user_id' => $_SESSION['user']['id'],
            'orders_send_history_id' => $iOSHId
        ];

        $oilRes = $this->pDbMgr->Insert('profit24', 'orders_items_lists', $aValues);

        if (false === $oilRes) {
            return false;
        }

        foreach($aProductsScanned as $productId => $scannedProduct) {

            $itemValues = [
                'orders_items_lists_id' => $oilRes,
                'product_id' => $productId,
                'products_id' => $productId,
                'quantity' => $scannedProduct['quantity'],
                'shelf_number' => '',
                'confirmed_quantity' => 0,
                'isbn_plain' => $scannedProduct['ean_13'],
                'ean_13' => $scannedProduct['ean_13'],
            ];

            $oiliRes = $this->pDbMgr->Insert('profit24', 'orders_items_lists_items', $itemValues);

            if (false === $oiliRes){
                dump($itemValues);
                return false;
            }
        }

        return true;
    }


  /**
   * @return int
   * @throws \Exception
   */
  private function generateDocumentNumber()
  {
    $sql = sprintf("SELECT document_number FROM orders_items_lists WHERE `type` = '%s' ORDER BY document_number DESC", OrdersItemsLists::INDEPENDENT_SUPPLY_DOCUMENT_TYPE);

    $currentNumber = $this->pDbMgr->GetOne('profit24', $sql);

    if (false === $currentNumber) {
      throw new \Exception('Wystapil blad podczas generowania numeru dokumentu');
    }

    $number = 1;

    if (null !== $currentNumber && '' != $currentNumber) {
      $number = $currentNumber + 1;
    }

    return $number;
  }
}