<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Elasticsearch\Profit;

use DatabaseManager;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ElasticAbstract
{
    protected $client = null;

    /**
     * @var string
     */
    protected $indexName = 'product_backup';

    /**
     * @var string
     */
    protected $typeName = 'product';

    /**
     * @var int
     */
    protected $pDbMgr;
    protected $config;

    public function __construct()
    {
        global $pDbMgr, $aConfig;

        $this->pDbMgr = $pDbMgr;
        $this->config = $aConfig;
    }

    public function getClient()
    {
        if (null === $this->client) {
            $this->setClient();
        }

        return $this->client;
    }

    public function setClient()
    {
        $configData = [
            'localhost:9460',       // Just IP/ SSL to IP + Port
//            'profit24.pl:9460',       // Just IP/ SSL to IP + Port
//            'profit_elast:sj58nsdkw@127.0.0.1:9200',       // Just IP/ SSL to IP + Port
//            'profit_elast:sj58nsdkw@profit24.pl:9200',       // Just IP/ SSL to IP + Port
        ];

        $this->client = ClientBuilder::create()->setHosts($configData)->build();
    }
}