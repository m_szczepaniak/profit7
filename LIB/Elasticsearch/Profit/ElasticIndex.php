<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Elasticsearch\Profit;

use Common;
use DatabaseManager;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

/**
 * @property mixed countProductsInOrders
 */
class ElasticIndex extends ElasticAbstract
{
    /**
     * @var \DatabaseManager
     */
    protected $pDbMgr;
    protected $countProductsInOrders;
    protected $moduleCommon;
    private $websites;

    public function __construct()
    {
        global $aConfig;
        //46680, 9359, 22897, 34570, 35307, 53645, 5184, 16362, 53578, 6501
        $this->aConfig = $aConfig;

        include_once('DatabaseManager.class.php');
        $this->pDbMgr = new DatabaseManager($aConfig);
        $this->websites = $this->pDbMgr->GetAssoc('profit24', "SELECT id, code from websites where id in(1, 3, 4, 5, 8) AND code != 'scale'");

        include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_oferta_produktowa/Module_Common.class.php');
        $this->moduleCommon = new \Module_Common();

    }

    /**
     * @return mixed
     */
    private function getCountProductsInOrders() {
        global $aConfig;

        if (!isset($this->countProductsInOrders)) {

            $sSql = '
                 SELECT OI.product_id, count(O.id)
                 FROM orders AS O
                 JOIN orders_items AS OI 
                    ON O.id = OI.order_id
                 WHERE O.order_status = "4" AND O.order_date > DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)
                 AND OI.product_id IS NOT NULL 
                 GROUP BY OI.product_id
                 ';
            $md5SQL = md5($sSql);
            // spróbujmy ciągnąć z memcached
            if (class_exists('memcached')) {
                $oMemcache = new \Memcached();
                $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
                $result = unserialize($oMemcache->get('sql_'.$md5SQL));
                if (!isset($result) || empty($result)) {
                    // trzeba pobrać z MYSQL
                        $sSql = '
                     SELECT OI.product_id, count(O.id)
                     FROM orders AS O
                     JOIN orders_items AS OI 
                        ON O.id = OI.order_id
                     WHERE O.order_status = "4" AND O.order_date > DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)
                     AND OI.product_id IS NOT NULL 
                     GROUP BY OI.product_id
                     ';
                    $this->countProductsInOrders = Common::GetAssoc($sSql, false, array(), DB_FETCHMODE_ASSOC, false);
                    if (!empty($this->countProductsInOrders)) {
                        // save memcached
                        $oMemcache->set('sql_' . $md5SQL, serialize($this->countProductsInOrders), 21600);
                    }
                } else {
                    $this->countProductsInOrders = $result;
                }

            }
        }
        return $this->countProductsInOrders;
    }

    public function updateIndex()
    {
        $productSql = $this->prepareProductSql2();
        $ress = $this->pDbMgr->PlainQuery('profit24', $productSql);

        $indexes = $this->prepareIndexes($ress);

        $i = 0;
        foreach($indexes as $index) {
            $response = $this->getClient()->index($index);
            $this->pDbMgr->Query('profit24', sprintf("UPDATE product_changes_data set elastic_has_changed = 1 WHERE product_id = %d", $index['id']));
            $i++;
        }
    }

    public function createIndex(array $products = null, $limit = 1000, $fullCreate = false)
    {
        $productSql = $this->prepareProductSql($products, $limit, $fullCreate);
        $ress = $this->pDbMgr->PlainQuery('profit24', $productSql);

        if ($products !== null && $products !== false) {
            $indexes = $this->prepareIndexes($ress);

            $i = 0;
            foreach ($indexes as $index) {
                $response = $this->getClient()->index($index);
                $iid = $index['id'];
                $this->pDbMgr->Query('profit24', "INSERT INTO products_index_create (product_id) VALUES ($iid)");
                $i++;
            }
        }
    }

    public function prepareProductSql2()
    {
        $productSql = "
          SELECT DISTINCT P.id, PGA.lego_code, P.publisher_id, P.name2, P.plain_name AS name, P.publication_year, PP.name as publisher, P.isbn, P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13, P.prod_status, P.published, P.created, P.is_news, P.is_bestseller, P.is_previews, P.product_type, PGA.sex, PGA.age, P.shipment_time, MPS.id as smarkacz,
          PS.order_by_status_old_style as order_by_status_old_style2,
          PS.order_by,
          P.wholesale_price, P.price_brutto as catalog_price_brutto, P.vat,
          (
			 	 SELECT GROUP_CONCAT(PTG.tag)
	          FROM products_to_tags AS PTTG
	          JOIN products_tags AS PTG ON PTTG.tag_id = PTG.id
	          WHERE PTTG.product_id = P.id
			 ) as tags,
          (
			 	 SELECT GROUP_CONCAT(PTS.series_id)
	          FROM products_to_series AS PTS
	          WHERE PTS.product_id = P.id
			 ) as series,
			 (
			 	 SELECT GROUP_CONCAT(CONCAT (PA.name, ' ',PA.surname))
	          FROM products_to_authors AS PTA
	          JOIN products_authors AS PA ON PTA.author_id = PA.id
	          WHERE PTA.product_id = P.id
			 ) as authors,

             (
             SELECT GROUP_CONCAT(CONCAT (PA.surname, ' ',PA.name))
	          FROM products_to_authors AS PTA
	          JOIN products_authors AS PA ON PTA.author_id = PA.id
	          WHERE PTA.product_id = P.id
			 ) as authors2
          FROM product_changes_data AS PCD
          JOIN products AS P ON PCD.product_id = P.id
          LEFT JOIN products_shadow AS PS ON PS.id = P.id
          LEFT JOIN products_publishers AS PP ON P.publisher_id = PP.id
          LEFT JOIN products_game_attributes AS PGA ON P.id = PGA.product_id
          LEFT JOIN main_smarkacz.products AS MPS ON P.id = MPS.profit24_id
          WHERE 1 = 1
          AND PCD.elastic_has_changed = 0
          ORDER BY P.id
          LIMIT 20000
          ";

        return $productSql;
    }

    public function prepareProductSql(array $products = null, $limit, $fullCreate = false)
    {
        $productSql = "
          SELECT DISTINCT P.id, PGA.lego_code, P.publisher_id, P.name2, P.plain_name AS name, P.publication_year, PP.name as publisher, P.isbn, P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13, P.prod_status, P.published, P.created, P.is_news, P.is_bestseller, P.is_previews, P.product_type, PGA.sex, PGA.age, P.shipment_time, MPS.id as smarkacz,
          PS.order_by_status_old_style as order_by_status_old_style2,
          PS.order_by,
          P.wholesale_price, P.price_brutto as catalog_price_brutto, P.vat,
          (
			 	 SELECT GROUP_CONCAT(PTG.tag)
	          FROM products_to_tags AS PTTG
	          JOIN products_tags AS PTG ON PTTG.tag_id = PTG.id
	          WHERE PTTG.product_id = P.id
			 ) as tags,
          (
			 	 SELECT GROUP_CONCAT(PTS.series_id)
	          FROM products_to_series AS PTS
	          WHERE PTS.product_id = P.id
			 ) as series,
			 (
			 	 SELECT GROUP_CONCAT(CONCAT (PA.name, ' ',PA.surname))
	          FROM products_to_authors AS PTA
	          JOIN products_authors AS PA ON PTA.author_id = PA.id
	          WHERE PTA.product_id = P.id
			 ) as authors,

             (
             SELECT GROUP_CONCAT(CONCAT (PA.surname, ' ',PA.name))
	          FROM products_to_authors AS PTA
	          JOIN products_authors AS PA ON PTA.author_id = PA.id
	          WHERE PTA.product_id = P.id
			 ) as authors2
          FROM products AS P
          LEFT JOIN products_shadow AS PS ON PS.id = P.id
          LEFT JOIN products_publishers AS PP ON P.publisher_id = PP.id
          LEFT JOIN products_game_attributes AS PGA ON P.id = PGA.product_id
          LEFT JOIN products_index_create AS PIC ON P.id = PIC.product_id
          LEFT JOIN main_smarkacz.products AS MPS ON P.id = MPS.profit24_id
          WHERE 1 = 1
          ";

        //TODO tylko rpzy odtwarznaiu indeksu
        if (true == $fullCreate) {
            $productSql .= 'AND PIC.product_id IS NULL';
        }

//        $counted = count($products);
//        if ($counted > 1) {
//            $productSql .= " AND P.published = '1'";
//        }

        if (null !== $products) {
            $imploded = implode(',', $products);
            $productSql .= sprintf(" AND P.id IN(%s)", $imploded);
        }

        $productSql .= "
          GROUP BY P.id
          ORDER BY P.id
        ";

        if ($limit != null & $limit > 0) {

            $productSql .= "
                  LIMIT $limit
            ";
        }

        //var_dump($productSql);die;
        return $productSql;
    }

    /**
     * @param array $index
     * @return array
     */
    public function getIndex(array $index)
    {
        return $this->getClient()->get($index);
    }

    /**
     * @param $products
     * @return array
     */
    private function prepareIndexes($products)
    {
        global $pDbMgr;
        $indexes = [];
        $productsWebsites = $pDbMgr->GetCol("profit24", "SELECT product_id FROM products_websites");

        if ($products !== null && $products !== false) {
            while ($product =& $products->fetchRow(DB_FETCHMODE_ASSOC)) {

                $index = [
                    'id' => $product['id'],
                    'index' => $this->indexName,
                    'type' => $this->typeName,
                    'body' => $this->prepareProductData($product, $productsWebsites)
                ];

                $indexes[] = $index;
            }
        }

        return $indexes;
    }

    private function prepareProductData(array $product, array $productsWebsites)
    {
        if ($product['smarkacz'] == null) {
            $product['is_smarkacz_product'] = 0;
        } else {
            $product['is_smarkacz_product'] = 1;
        }

        unset($product['smarkacz']);

        $tags = $product['tags'];
        $series = $product['series'];
        $authors = $product['authors'];
        $authors2 = $product['authors2'];
        $product['catalog_price_brutto'] = (float)$product['catalog_price_brutto'];
        $product['wholesale_price'] = (float)$product['wholesale_price'];

        $wdata = $this->prepareWeight($product);;

        $product['base_weight'] = $wdata['base_weight'];
        $product['prod_status_weight'] = $wdata['prod_status_weight'];
        $product['shipment_time_weight'] = $wdata['shipment_time_weight'];
        $product['news_weight'] = $wdata['news_weight'];
        $product['previews_weight'] = $wdata['previews_weight'];
        $product['year_weight'] = (int)$wdata['year_weight'];

        $age = $product['age'];
        $sex = $product['sex'];
        unset($product['age']);
        unset($product['sex']);

        if ($age !== null) {
            $age = str_replace(' ', '-', trim($age));
            $exploded = explode(' ', $age);
            $product['from_age'] = (int)$exploded[0];
            $product['to_age'] = (int)$exploded[1];

            if ($product['to_age'] == 0) {
                $product['to_age'] = 99999;
            }
        }

        if ($sex !== null) {
            $product['sex'] = $sex;
        }

        $product['id'] = (int) $product['id'];
        $product['tags'] = [];
        $product['created'] = (new \DateTime($product['created']))->format('Y-m-d');
        $product['series'] = [];
        $product['authors'] = [];
        $product['is_news'] = (int) $product['is_news'];
        $product['is_bestseller'] = (int) $product['is_bestseller'];
        $product['is_previews'] = (int) $product['is_previews'];
        $product['product_type'] = (string) $product['product_type'];

        $product['created_integer'] = strtotime($product['created']);
        $product['order_by_status_old_style2'] = (int) $product['order_by_status_old_style2'];
        $product['order_by'] = (int) $product['order_by'];

        if ($product['created'] <= 0) {
            $product['created'] = 0;
            $product['created_integer'] = 0;
        }

        $product['publisher_not_analyzed'] = (string) $product['publisher'];
        $product['publisher_id'] = (int) $product['publisher_id'];

        if (!empty($product['lego_code'])) {

            $product['name'] = trim($product['name']).' '.$product['lego_code'];
            $product['name2'] = trim($product['name2']).' '.$product['lego_code'];
        }

        $product = $this->prepareWebsitesData($product);

        if (!empty($tags)) {
            $product['tags'] = explode(',', $tags);
        }

        if (!empty($series)) {
            $product['series'] = explode(',', $series);
        }

        if (!empty($authors)) {
            $auth = explode(',', $authors);
            $auth2 = explode(',', $authors2);
            $c = [];
            foreach($auth as $a){
                $c[] = trim(str_replace('-', ' ', $a));
            }

            foreach($auth2 as $a2){
                $c[] = trim(str_replace('-', ' ', $a2));
            }

            $product['authors'] = $c;
        }

        if (in_array($product['id'], $productsWebsites)) {

            $product['published'] = 1;
        }

        if ($product['order_by_status_old_style2'] == '') {
            // wyliczamy order_by
            $product['order_by_status_old_style2'] = $this->moduleCommon->getOrderByStatusOldStyle($product);
        }

        return $product;
    }

    private function prepareWebsitesData(array $product)
    {

        $sql = "
            SELECT price_netto, price_brutto, discount
            FROM products_tarrifs
            WHERE product_id = %d AND start_date <= UNIX_TIMESTAMP() AND end_date >= UNIX_TIMESTAMP()
            ORDER BY vip DESC, type DESC, discount DESC LIMIT 1";

        $sqlCats = "SELECT page_id, page_id FROM products_extra_categories WHERE product_id = %s";

        foreach($this->websites as $website) {
            $websiteProdId = $website == 'profit24' ? $product['id'] : $this->pDbMgr->GetOne($website, sprintf("SELECT id from products where profit24_id = %d", $product['id']));

            if (empty($websiteProdId)) {
                continue;
            }

            $data = $this->pDbMgr->GetRow($website, sprintf($sql, $websiteProdId));
            $pbrutto = $data['price_brutto'];
            $pbruttodec = $data['price_brutto'];
            $pbrutto = (int)str_replace([',', '.'], '', $pbrutto);

            $pnetto = $data['price_netto'];
            $pnettoodec = $data['price_netto'];
            $pnetto = (int)str_replace([',', '.'], '', $pnetto);

            $discount = (float)$data['discount'];

            $catsTemp = [];

            $cats = $this->pDbMgr->GetAssoc($website, sprintf($sqlCats, $websiteProdId));

            foreach($cats as $cat) {
                $catsTemp[] = $cat;
            }

            $product[$website.'_price_brutto'] = $pbrutto;
            $product[$website.'_price_brutto_dec'] = (float)$pbruttodec;
            $product[$website.'_price_netto'] = $pnetto;
            $product[$website.'_price_netto_dec'] = (float)$pnettoodec;
            $product[$website.'_discount'] = (float)$discount;
            $product[$website.'_cats'] = $catsTemp;
        }

        return $product;
    }

    private function prepareWeight(array $aItem)
    {
        $weight = [
            'prod_status_weight' => 0,
            'shipment_time_weight' => 0,
            'news_weight' => 0,
            'previews_weight' => 0,
            'year_weight' => 0,
            'base_weight' => 0
        ];

        if ($aItem['prod_status'] == '1' || $aItem['prod_status'] == '3') {

            $weight['prod_status_weight'] = 1;

            if ($aItem['shipment_time'] == '0') {
                $weight['shipment_time_weight'] = 1;
                $iGroup = 2;
            } elseif ($aItem['shipment_time'] == '1') {
                $weight['shipment_time_weight'] = 0.75;
                $iGroup = 2;
            } elseif ($aItem['shipment_time'] == '2') {
                $weight['shipment_time_weight'] = 0.50;
                $iGroup = 1;
            } elseif ($aItem['shipment_time'] == '3') {
                $weight['shipment_time_weight'] = 0.25;
                $iGroup = 1;
            } else {
                $weight['shipment_time_weight'] = 0.25;
                $iGroup = 1;
            }
        } else {
            $iGroup = 0;
        }
        $bIsPreviewsORNews = 0;
        if (intval($aItem['is_news']) > 0 || intval($aItem['is_previews']) > 0) {
            $bIsPreviewsORNews = 1;
        }


        $countProductsInOrders = $this->getCountProductsInOrders();
        $var = sprintf("%01u%03u%01u%04u%08u",$iGroup, $countProductsInOrders[$aItem['id']], intval($bIsPreviewsORNews),intval($aItem['publication_year']),$aItem['id']);
        $var = $var / 10000000000000000;


        if (intval($aItem['is_news']) > 0) {
            $weight['news_weight'] = 1;
        }

        if (intval($aItem['is_previews']) > 0) {
            $weight['previews_weight'] = 1;
        }

        $weight['year_weight'] = $aItem['publication_year'];
        $weight['base_weight'] = $var;

        return $weight;
    }
}