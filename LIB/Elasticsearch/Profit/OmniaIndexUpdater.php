<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Elasticsearch\Profit;

use DatabaseManager;
use Exception;

class OmniaIndexUpdater
{

    public static function getReturnColumns($tableName = null)
    {
        if ('products' == $tableName) {
            return 'id as product_id';
        } elseif ('products_stock') {
            return 'published, prod_status';
        }

        return 'product_id';
    }

    public static function acceptEmptyValue()
    {
        return true;
    }

    public function getData()
    {

    }

    public function customValue($table, $colsValues = null, $where = null)
    {
        if ($table !== 'products_stock') {
            return false;
        }

        $asda = '';
    }

    public function updateStockIndex($data)
    {
        $asdas = '';
    }

    public function updateProductIndex($data)
    {
        $this->updateIndex($data['product_id']);
    }

    public function updateTarrifsIndex($data)
    {
        $this->updateIndex($data['product_id']);
    }

    private function updateIndex($productId)
    {
        if (null == $productId || empty($productId)) {
            var_dump('ProductId hasnt been typed');
            return;
        }

        $elasticIndex = new ElasticIndex();
        $elasticIndex->createIndex([$productId], 1);
    }
}