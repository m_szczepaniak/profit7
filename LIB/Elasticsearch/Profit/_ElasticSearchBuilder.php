<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Elasticsearch\Profit;

use DatabaseManager;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ElasticSearchBuilder extends ElasticAbstract
{
    const PHRASE_SEARCH_TYPE = 'phrase';

    const NGRAMS_SEARCH_TYPE = 'ngrams';

    private $website;
    protected $pDbMgr;

    public function __construct()
    {
        global $pDbMgr, $aConfig;

        $this->pDbMgr = $pDbMgr;
        $this->website = $aConfig['common']['bookstore_code'];
    }

    private function prepareSort(array $params)
    {
        if (!isset($_GET['sort']) || empty($_GET['sort'])) {
            return $params;
        }

        $sort = [];

        switch ($_GET['sort']) {
            case 'cena':
                $sort = [
                    $this->getPriceBruttoColumn() => [
                        'order' => 'asc'
                    ]
                ];
                break;
            case 'cena_desc':
                $sort = [
                    $this->getPriceBruttoColumn() => [
                        'order' => 'desc'
                    ]
                ];
                break;
            case 'data':
//                $params['body']['sort'][] = '_score';
//                $sort = [
//                    'created_integer' => [
//                        'order' => 'desc'
//                    ],
//                    'publication_year' => [
//                        'order' => 'desc'
//                    ]
//                ];

                //'script' => "(_score / 1000000.0) + (doc['weight'].value * 100)",

                $sort = [
                    '_script' => [
                        'type' => 'number',
//                        'script' => "( Math.round(_score * 100.0)/100 ) + doc['weight'].value ",
//                        'script' => "( (doc['weight'].value / 100.0) + Math.round(_score * 100))",

//                        'script' => "_score + doc['weight'].value",
                        'script' => "(_score / 1000000.0) + (doc['weight'].value * 100)",

                        'params' => [
                            'factor' => 1
                        ],
                        'order' => 'desc',
                        'nested_filter' => [
                            'script' => "doc['publication_year'].value",
                            'order' => 'asc',
                        ]
                    ]
                ];


                break;
            case 'nazwa':
                $sort = [
                    'sort' => [
                        'order' => 'asc'
                    ]
                ];
                break;
        }

        $params['body']['sort'][] = $sort;

        return $params;
    }

    private function detectQueryType($keyword, $searchType)
    {
        return [
            'bool' => [
                'should' => [
                    [
                        'query_string' => [
                            "default_field" => "ngrams",
                            "default_operator" => "AND",
                            "query" => $keyword
                        ]
                    ]
                ]
            ]
        ];

        if (self::PHRASE_SEARCH_TYPE == $searchType) {

            // podmienia query jezeli zapytanie leci z autocompletera
            return [
                'bool' => [
                    'should' => [
                        [
                            'match_phrase' => [
                                'name' => [
                                    'query' => $keyword,
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        if (self::NGRAMS_SEARCH_TYPE == $searchType) {

            return [
                'bool' => [
                    'should' => [
                        [
                            'query_string' => [
                                "default_field" => "ngrams",
                                "default_operator" => "AND",
                                "query" => $keyword
                            ]
//                            'query_string' => [
//                                'default_field' => 'ngrams',
//                                'query' => $keyword,
//                            ]
                        ]
                    ]
                ]
            ];
        }

        // podmienia query jezeli zapytanie leci z autocompletera
        $qDefault = [
            'bool' => [
                'should' => [
                    [
                        //'match' => [
//                                $searchField => [
//                                    'query' => $keyword,
//                                    "boost" => 55
//                                ],
                        'query_string' => [
                            'default_field' => 'ngrams',
                            'query' => $keyword,
                        ]
                        //]
                    ]
                ]
            ]
        ];

        //TODO do dwoch ponizszych dodac sprawdzanie czy jest odpalona zaawansowana wyszukiwarka, jezeli tak to nie szukamy w glownym query - ale chyba juz jest - do zweryfikowania
//        if (null === $publisher) {
//            $qDefault['bool']['should'][] = [
//                'match' => [
//                    'publisher' => [
//                        'query' => $keyword,
//                        "boost" => 25
//                    ],
//                ]
//            ];
//        }

        // dodajemy szukanie po autorze
//        if (null === $author) {
//            $qDefault['bool']['should'][] = [
//                'match' => [
//                    'authors' => [
//                        'query' => $keyword
//                    ],
//                ]
//            ];
//        }
    }

    public function search($perPage = 20, $isAComplete = false, $returnProductIds = true, $searchType = null)
    {
        array_walk_recursive($_GET, function(&$item, $key){

            $item = str_replace('+', ' ', $item);
        });


        if (!isset($_GET['sort']) || empty($_GET['sort'])) {

            $_GET['sort'] = 'data';
        }

        $cats = null;

        if (isset($_GET['top_cat']) && !empty($_GET['top_cat'])) {
            $cats = [$_GET['top_cat']];
        }

        if (isset($_GET['cat']) && !empty($_GET['cat'])) {
            $cats = [$_GET['cat']];
        }

        $keyword = isset($_GET['q']) ? $_GET['q'] : $_GET['term'];
        $unavailable = isset($_GET['aval']) && ('on' == $_GET['aval'] || 1 == $_GET['aval']) ? true : false;
        $publicationYear = isset($_GET['year']) ? $_GET['year'] : null;
        $publisher = isset($_GET['publisher']) && !empty($_GET['publisher']) ? html_entity_decode($_GET['publisher']) : null;
        $priceFrom = isset($_GET['price_from']) && !empty($_GET['price_from']) ? $this->priceToInt($_GET['price_from']) : null;
        $priceTo = isset($_GET['price_to']) && !empty($_GET['price_to']) ? $this->priceToInt($_GET['price_to']) : null;
        $series = isset($_GET['series']) && !empty($_GET['series']) ? $_GET['series'] : null;
        $tag = isset($_GET['tag']) && !empty($_GET['tag']) ? $_GET['tag'] : null;
        $author = isset($_GET['autor']) && !empty($_GET['autor']) ? $_GET['autor'] : null;
        $isbn = isset($_GET['isbn']) && !empty($_GET['isbn']) ? $_GET['isbn'] : null;
        $fromAge = isset($_GET['from_age']) && !empty($_GET['from_age']) ? $_GET['from_age'] : null;
        $toAge = isset($_GET['to_age']) && !empty($_GET['to_age']) ? $_GET['to_age'] : null;
        $sex = isset($_GET['sex']) && !empty($_GET['sex']) ? $_GET['sex'] : null;

        $searchField = 'ngrams';

        $qDefault = $this->detectQueryType($keyword, $searchType);

        $tempNumberic = str_replace('-', '', $keyword);

        // jezeli keyqord jest numerczny to szukamy po filtrze isbna i wyalczamy query
        if (is_numeric($tempNumberic)) {
            $qDefault = null;
            $isbn = $tempNumberic;
        }

        // jezeli pusto to znaczy ze lecimy pewnie samymi filtrai
        if (null === $keyword || empty($keyword)) {
            $qDefault = null;
        }

        // podstawa query ktory zostaje rozbudowywany
        $query = [
            'filter' => [
                'bool' => [
                    'must' => [
                        [
                            'term' => [
                                'published' => 1
                            ]
                        ]
                    ]
                ]
            ]
        ];

        // sprawdzamy czy lecimy samymi filtram
        if (null !== $qDefault) {
            $query['query'] = $qDefault;
        }

        // gotowe query
        $query = $this->createQuery($query, $keyword, $unavailable, $publicationYear, $publisher, $priceFrom, $priceTo, $cats, $series, $author, $tag, $isbn, $fromAge, $toAge, $sex);

        // paginacja
        $from = 0;
        if (isset($_GET['p'])) {
            if ($_GET['p'] < 1) {
                $_GET['p'] = 1;
            }

            $from = ($_GET['p'] * $perPage) - $perPage;
        }

        $params = [
            'index' => $this->indexName,
            'from' => $from,
            'size' => $perPage,
            'type' => $this->typeName,
            'body' => [
                'query' => [
                    'filtered' => $query
                ]
            ]
        ];

        // sortujemy
        $params = $this->prepareSort($params);

        if (true === $isAComplete) {
            $params['body'] += [
                'highlight' => [
                    'fields' => [
                        $searchField => ['content' => ['force_source' => true]]
                    ]
                ]
            ];
        }

        // pobieramy same id
        if (true === $returnProductIds) {
            $params['body']['fields'] = ['id'];
        }

        $debug = json_encode($params['body']);

        $res = $this->getClient()->search($params);

        if (true === $returnProductIds) {
            return $this->returnProductIds($res);
        }

        return $res;
    }

    private function returnProductIds($res)
    {
        $array = [
            'total' => $res['hits']['total'],
            'ids' => []
        ];

        if (!empty($res['hits']['hits'])) {
            foreach($res['hits']['hits'] as $hit) {
                $array['ids'][] = $hit['_id'];
            }
        }

        return $array;
    }

    private function priceToInt($price)
    {
        return (int)(str_replace([',', '.'], '', $price));
    }

    private function createQuery(
        $query,
        $keyword,
        $unavailable = false,
        $publicationYear = null,
        $publisher = null,
        $priceFrom = null,
        $priceTo = null,
        array $cats = null,
        $series = null,
        $author = null,
        $tag = null,
        $isbn = null,
        $fromAge = null,
        $toAge = null,
        $sex = null
    )
    {
        global $aConfig;

        if (false === $unavailable) {
            $query = $this->prepareUnavailable($query);
        }

        if (!empty($publicationYear)) {
            $query = $this->preparePublicationYear($query, $publicationYear);
        }

        if (!empty($publisher)) {
            $query = $this->preparePublisher($query, $publisher);
        }

        if (null !== $priceFrom || null !== $priceTo) {
            $query = $this->preparePrices($query, $priceFrom, $priceTo);
        }

        if (true === is_array($cats)) {
            $query = $this->hasValue($query, $this->getCatsColumn(), $cats);
        }

        if (null !== $series) {
            $query = $this->prepareSeries($query, $series);
        }

        if (null !== $author) {
            $query = $this->prepareAuthor($query, $author);
        }

        if (null !== $tag) {
            $query = $this->prepareTags($query, $tag);
        }

        if (null !== $isbn) {
            $query = $this->prepareIsbn($query, $isbn);
        }

        if (null !== $sex) {
            $query = $this->prepareSex($query, $sex);
        }

        if ($aConfig['common']['bookstore_code'] == 'smarkacz') {

            if (null !== $fromAge || null !== $toAge) {
                $query = $this->prepareAges($query, $fromAge, $toAge);
            }

            $query['filter']['bool']['must'][] = [
                'term' => [
                    'is_smarkacz_product' => 1
                ]
            ];
        } else {
            $query['filter']['bool']['must'][] = [
                'term' => [
                    'product_type' => 'K'
                ]
            ];
        }

        return $query;
    }

    private function prepareSex($query, $sex)
    {
        $query['filter']['bool']['must'][] = [
            'term' => [
                'sex' => $sex
            ]
        ];

        return $query;
    }

    private function prepareSeries($query, $series)
    {
        $sql = "SELECT id FROM products_series where `name` = '%s'";
        $sId = $this->pDbMgr->GetRow($this->website, sprintf($sql, $series));

        if (empty($sId)) {
            return $query;
        }

        $query['filter']['bool']['must'][] = [
            'query' => [
                'match_phrase' => [
                    'series' => $sId['id']
                ]
            ]
        ];

        return $query;
    }

    private function prepareUnavailable($query)
    {
        $query['filter']['bool']['must'][] = [
            'terms' => [
                'prod_status' => [1, 3]
            ]
        ];

        return $query;
    }

    private function preparePublicationYear($query, $publicationYear)
    {
        $query['filter']['bool']['must'][] = [
            'term' => [
                'publication_year' => $publicationYear
            ]
        ];

        return $query;
    }

    private function preparePublisher($query, $publisher)
    {
        $query['filter']['bool']['must'][] = [
            'query' => [
                'match_phrase' => [
                    'publisher' => $publisher
                ]
            ]
        ];

        return $query;
    }

    private function prepareAges($query, $fromAge = null, $toAge = null)
    {
        if (isset($_GET['age_type']) && $_GET['age_type'] != null && $_GET['age_type'] == 'Y') {
            $fromAge = $fromAge * 12;
            $toAge = $toAge * 12;
        }

        if (null !== $fromAge) {
            $query['filter']['bool']['must'][] = [
                'range' => [
                    'from_age' => [
                        'gte' => $fromAge,
                    ]
                ]
            ];
        }

        if (null !== $toAge) {
            $query['filter']['bool']['must'][] = [
                'range' => [
                    'to_age' => [
                        'lte' => $toAge
                    ]
                ]
            ];
        }

        return $query;
    }

    private function preparePrices($query, $priceFrom = null, $priceTo = null)
    {
        if (null !== $priceFrom) {
            $query['filter']['bool']['must'][] = [
                'range' => [
                    $this->getPriceBruttoColumn() => [
                        'gte' => $priceFrom
                    ]
                ]
            ];
        }

        if (null !== $priceTo) {
            $query['filter']['bool']['must'][] = [
                'range' => [
                    $this->getPriceBruttoColumn() => [
                        'lte' => $priceTo
                    ]
                ]
            ];
        }

        return $query;
    }

    private function getPriceBruttoColumn()
    {
        return $this->website.'_price_brutto';
    }

    private function getCatsColumn()
    {
        return $this->website.'_cats';
    }

    private function hasValue($query, $string, array $data)
    {
        if (empty($data)) {
            return $query;
        }

        $query['filter']['bool']['must'][] = [
            'terms' => [
                $string => $data
            ]
        ];

        return $query;
    }

    private function prepareAuthor($query, $author)
    {
        $query['filter']['bool']['must'][] = [
            'query' => [
                'match_phrase' => [
                    'authors' => $author
                ]
            ]
        ];

        return $query;
    }

    private function prepareTags($query, $tag)
    {
        $query['filter']['bool']['must'][] = [
            'query' => [
                'match' => [
                    'tags' => $tag
                ]
            ]
        ];

        return $query;
    }

    private function prepareIsbn($query, $isbn)
    {
        $isbnFields = [
            'isbn', 'isbn_plain', 'isbn_10', 'isbn_13', 'ean_13'
        ];

        $terms = [];

        foreach($isbnFields as $field) {

            $terms[] = [
                'term' => [
                    $field => $isbn
                ]
            ];
        }

        $query['filter']['bool']['must'][] = [
            'or' => [
                'filters' => $terms
            ]
        ];

        return $query;
    }
}