<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Elasticsearch\Profit;

use DatabaseManager;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ElasticSearchStrategy extends ElasticAbstract
{
    public function regularSearch($itemsPerPage, $isAcomplete, $returnIds)
    {
        $keyword = (isset($_GET['q']) && !empty($_GET['q'])) ? $_GET['q'] : $_GET['term'];

        $elastic = new ElasticSearchBuilder($itemsPerPage);

        if (strpos($keyword, '*')) {

            // szukamy pierwsze po frazie
            $phraseResultsEdgeGram = $elastic->search($itemsPerPage, $isAcomplete, $returnIds, ElasticSearchBuilder::EDGE_GRAMS_SEARCH_TYPE, 0);

            if (!empty($phraseResultsEdgeGram['ids']) || !empty($phraseResultsEdgeGram['hits']['hits'])) {

                return $phraseResultsEdgeGram;
            }

            return null;
        }

        // szukamy pierwsze po frazie i po autorze
        $phraseResults = $elastic->search($itemsPerPage, $isAcomplete, $returnIds, ElasticSearchBuilder::PHRASE_SEARCH_TYPE, 1);

        if (!empty($phraseResults['ids']) || !empty($phraseResults['hits']['hits'])) {

            return $phraseResults;
        }

        return null;
    }

    public function autocompleteSearch($itemsPerPage, $isAcomplete, $returnIds)
    {
        $elastic = new ElasticSearchBuilder($itemsPerPage);

        // szukamy pierwsze po frazie
        $phraseResults = $elastic->search($itemsPerPage, $isAcomplete, $returnIds, ElasticSearchBuilder::AUTOCOMPLETE_SEARCH, 0);

        if (!empty($phraseResults['ids']) || !empty($phraseResults['hits']['hits'])) {

            $temp = [];
            $finalResults = [];

            foreach($phraseResults['hits']['hits'] as $element) {

                $temp[$element['_source']['name']] = $element['_source']['id'];
            }

            foreach($phraseResults['hits']['hits'] as $element) {

                if (in_array($element['_source']['id'], $temp)) {

                    $finalResults[] = [
                        'name' => $element['_source']['name'],
                        'highlight' => $element['highlight']['autocomplete'][0],
                    ];
                }
            }

            return $finalResults;
        }

        return null;
    }
}