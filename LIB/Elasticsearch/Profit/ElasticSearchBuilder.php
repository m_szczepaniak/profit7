<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Elasticsearch\Profit;

use DatabaseManager;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class ElasticSearchBuilder extends ElasticAbstract
{
    const PHRASE_SEARCH_TYPE = 'phrase';

    const NGRAMS_SEARCH_TYPE = 'ngrams';

    const AUTOCOMPLETE_SEARCH = 'autocomplete';

    const EDGE_GRAMS_SEARCH_TYPE = 'edgegram';

    const TEXT_AUTHOR_SEARCH_TYPE = 'author';

    private $website;
    protected $pDbMgr;

    public function __construct()
    {
        global $pDbMgr, $aConfig;

        $this->pDbMgr = $pDbMgr;
        $this->website = $aConfig['common']['bookstore_code'];
    }

    private function prepareSort(array $params, $searchType)
    {
        if (!isset($_GET['sort']) || empty($_GET['sort'])) {
            return $params;
        }

        $keyword = (isset($_GET['q']) && !empty($_GET['q'])) ? $_GET['q'] : $_GET['term'];

        $sort = [];

        switch ($_GET['sort']) {
            case 'cena':
                $sort = [
                    $this->getPriceBruttoColumn() => [
                        'order' => 'asc'
                    ]
                ];
                break;
            case 'cena_desc':
                $sort = [
                    $this->getPriceBruttoColumn() => [
                        'order' => 'desc'
                    ]
                ];
                break;
            case 'najlepiej_dopasowane':

                $matchedSort = "(_score / 1000.0) + doc['base_weight'].value * 100.0";

                if ($searchType == self::AUTOCOMPLETE_SEARCH) {

                    $matchedSort = '_score';
                }

                if ($searchType == self::EDGE_GRAMS_SEARCH_TYPE || $searchType == ElasticSearchBuilder::PHRASE_SEARCH_TYPE) {

                    $matchedSort = "(_score) + doc['base_weight'].value * 10.0000";
                }

                if (empty($keyword)) {

                    $sort = [
                        '_script' => [
                            'type' => 'number',
                            'script' => $matchedSort,
                            'params' => [
                                'factor' => 1
                            ],
                            'order' => 'desc',
                        ]
                    ];
                }

                break;
            case 'nazwa':
                $sort = [
                    'sort' => [
                        'order' => 'asc'
                    ]
                ];
                break;
            case 'data':
                $sort = [
                    'order_by' => [
                        'order' => 'desc'
                    ]
                ];
                break;
            case 'order_by_status_old_style':
                $sort = [
                    'order_by_status_old_style2' => [
                        'order' => 'desc'
                    ]
                ];
                break;
        }

        if (!empty($sort)) {
            $params['body']['sort'][] = $sort;
        }

        return $params;
    }

    /**
     * @param $keyword
     * @param $searchType
     * @param int $fuzziness - parametr dopuszczajacy maksymalny rozmiar pomylki, poki co korzystamy tylko z 0 i 1
     * @return array
     */
    private function detectQueryType($keyword, $searchType, $fuzziness = 0)
    {
        //szukanie po gwiazdce

        if ($searchType == self::EDGE_GRAMS_SEARCH_TYPE) {

            return [
                'bool' => [
                    'should' => [
                        [
                            'function_score' => [
                                "query" => [
                                    "match" => [
                                        "autocomplete" => [
                                            'query' => $keyword,
                                            'operator' => 'AND'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];

        }

        //szukanie po autocomplete

        if ($searchType == self::AUTOCOMPLETE_SEARCH) {

            return [
                'bool' => [
                    'should' => [
                        [
                            'function_score' => [
                                "query" => [
                                    "match" => [
                                        "autocomplete" => [
                                            'query' => $keyword,
                                            'operator' => 'AND'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
        }

        if ($searchType == self::PHRASE_SEARCH_TYPE) {

            return [
                'function_score' => [
                    "query" => [
                        "multi_match" => [
                            //TODO JAKBY CO TO ZMIENIC AUTHORS NA 20
                            "fields" => ['ngrams_3^2', 'name^8', 'name2', 'authors^3', 'tags^12'],
                            //TODO crossfields nie dziala z fuzziness - potrzebujemy do wyszukiwania po tytule i podtytule
                            "type" => "cross_fields",
                            "query" => $keyword,
                            "fuzziness" => 1,
                            "operator" => "AND",
                            "minimum_should_match" => "95%",
                        ],
//                        "dis_max" => [
//                            "queries" => $defaultQueries
//                        ]
                    ],
                    // filtry - wazne
                    'functions' => [
                        [
                            // podbija te ktore najlepiej pasuaj do frazy
                            'filter' => [
                                "query" => [
                                    "match" => [
                                        'name' => $keyword
                                    ]
                                ]
                            ],
                            'weight' => 20
                        ],
                        [
                            "script_score" => [
                                "script" => "((int)doc['base_weight'].value)"
                            ]
                        ],
//                        [
//                            "script_score" => [
//                                "script" => "((int)doc['publication_year'].value)"
//                            ]
//                        ],
                        [
                            // podbija nowosci
                            'filter' => [
                                'term' => [
                                    'is_news' => 1
                                ]
                            ],
                            'weight' => 1
                        ],
                        [
                            // podbija nowosci
                            'filter' => [
                                'term' => [
                                    'is_bestseller' => 1
                                ]
                            ],
                            'weight' => 1
                        ],
                        [
                            "filter" => [
                                "query" => [
                                    "match" => [
                                        "authors" => $keyword
                                    ]
                                ]
                            ],
                            "weight" => 20
                        ]
                    ],
//                    'boost' => 4500,
//                    'max_boost' => 999999999999999,
                    'score_mode' => 'multiply',
                    'boost_mode' => 'sum',
                ]
            ];
        }
    }

    public function search($perPage = 20, $isAComplete = false, $returnProductIds = true, $searchType = null, $fuzziness)
    {
        $_GET['q'] = urldecode(html_entity_decode(strip_tags($_GET['q'])));
        $_GET['term'] = urldecode(html_entity_decode(strip_tags($_GET['term'])));


        array_walk_recursive($_GET, function (&$item, $key) {

            $item = str_replace('+', ' ', $item);

            if (strtolower($item) == '2 3d') {
                $item = '2+3d';
            }
        });


        if (!isset($_GET['sort']) || empty($_GET['sort'])) {

            $_GET['sort'] = 'najlepiej_dopasowane';
        }

        $cats = null;

        if (isset($_GET['top_cat']) && !empty($_GET['top_cat'])) {
            $cats = [$_GET['top_cat']];
        }

        if (isset($_GET['cat']) && !empty($_GET['cat'])) {
            $cats = [$_GET['cat']];
        }

        $keyword = (isset($_GET['q']) && !empty($_GET['q'])) ? $_GET['q'] : $_GET['term'];

        $keyword = str_replace('*', '', $keyword);

        $unavailable = isset($_GET['aval']) && ('on' == $_GET['aval'] || 1 == $_GET['aval']) ? true : false;
        $publicationYear = isset($_GET['year']) ? $_GET['year'] : null;
        $publisher = isset($_GET['publisher']) && !empty($_GET['publisher']) ? html_entity_decode($_GET['publisher']) : null;
        $priceFrom = isset($_GET['price_from']) && !empty($_GET['price_from']) ? $this->priceToInt($_GET['price_from']) : null;
        $priceTo = isset($_GET['price_to']) && !empty($_GET['price_to']) ? $this->priceToInt($_GET['price_to']) : null;
        $series = isset($_GET['series']) && !empty($_GET['series']) ? $_GET['series'] : null;
        $tag = isset($_GET['tag']) && !empty($_GET['tag']) ? $_GET['tag'] : null;
        $author = isset($_GET['autor']) && !empty($_GET['autor']) ? $_GET['autor'] : null;
        $isbn = isset($_GET['isbn']) && !empty($_GET['isbn']) ? $_GET['isbn'] : null;
        $fromAge = isset($_GET['from_age']) && !empty($_GET['from_age']) ? $_GET['from_age'] : null;
        $toAge = isset($_GET['to_age']) && !empty($_GET['to_age']) ? $_GET['to_age'] : null;
        $sex = isset($_GET['sex']) && !empty($_GET['sex']) ? $_GET['sex'] : null;

        $searchField = 'ngrams';

        $qDefault = $this->detectQueryType($keyword, $searchType, $fuzziness);

        $tempNumberic = str_replace('-', '', $keyword);

        // jezeli keyqord jest numerczny to szukamy po filtrze isbna i wyalczamy query
        if (is_numeric($tempNumberic) && strlen($tempNumberic) > 8) {
            $qDefault = null;
            $isbn = $tempNumberic;
        }

        // jezeli pusto to znaczy ze lecimy pewnie samymi filtrai
        if (null === $keyword || empty($keyword)) {
            $qDefault = null;
        }

        // podstawa query ktory zostaje rozbudowywany
        $query = [
            'filter' => [
                'bool' => [
                    'must' => [
                        [
                            'term' => [
                                'published' => 1
                            ]
                        ]
                    ]
                ]
            ]
        ];

        // sprawdzamy czy lecimy samymi filtram
        if (null !== $qDefault) {
            $query['query'] = $qDefault;
        }

        // gotowe query
        $query = $this->createQuery($query, $keyword, $unavailable, $publicationYear, $publisher, $priceFrom, $priceTo, $cats, $series, $author, $tag, $isbn, $fromAge, $toAge, $sex);

        // paginacja
        $from = 0;
        if (isset($_GET['p'])) {
            if ($_GET['p'] < 1) {
                $_GET['p'] = 1;
            }

            $from = ($_GET['p'] * $perPage) - $perPage;
        }

        $params = [
            'index' => $this->indexName,
            'from' => $from,
            'size' => $perPage,
            'type' => $this->typeName,
            'body' => [
                'query' => [
                    'filtered' => $query
                ]
            ]
        ];

        // sortujemy
        $params = $this->prepareSort($params, $searchType);

        if (true === $isAComplete) {
            $params['body'] += [
                'highlight' => [
                    'fields' => [
                        'autocomplete' => ['content' => ['force_source' => true]]
                    ]
                ]
            ];
        }

        // pobieramy same id
        if (true === $returnProductIds) {
            $params['body']['fields'] = ['id'];
        }

        $debug = json_encode($params['body']);
//        if ($_SERVER['REMOTE_ADDR'] == '83.220.102.129') {
//            $debug = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
//                return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UTF-16BE');
//            }, $debug);
//            var_dump($debug);
//        }

        $res = $this->getClient()->search($params);

        if (true === $returnProductIds) {
            return $this->returnProductIds($res);
        }

        return $res;
    }

    private function returnProductIds($res)
    {
        $array = [
            'total' => $res['hits']['total'],
            'ids' => []
        ];

        if (!empty($res['hits']['hits'])) {
            foreach ($res['hits']['hits'] as $hit) {
                $array['ids'][] = $hit['_id'];
            }
        }

        return $array;
    }

    private function priceToInt($price)
    {
        return (int)(str_replace([',', '.'], '', $price));
    }

    private function createQuery(
        $query,
        $keyword,
        $unavailable = false,
        $publicationYear = null,
        $publisher = null,
        $priceFrom = null,
        $priceTo = null,
        array $cats = null,
        $series = null,
        $author = null,
        $tag = null,
        $isbn = null,
        $fromAge = null,
        $toAge = null,
        $sex = null
    )
    {
        global $aConfig;

        if (false === $unavailable) {
            $query = $this->prepareUnavailable($query);
        }

        if (!empty($publicationYear)) {
            $query = $this->preparePublicationYear($query, $publicationYear);
        }

        if (!empty($publisher)) {
            $query = $this->preparePublisher($query, $publisher);
        }

        if (null !== $priceFrom || null !== $priceTo) {
            $query = $this->preparePrices($query, $priceFrom, $priceTo);
        }

        if (true === is_array($cats)) {
            $query = $this->hasValue($query, $this->getCatsColumn(), $cats);
        }

        if (null !== $series) {
            $query = $this->prepareSeries($query, $series, $publisher);
        }

        if (null !== $author) {
            $query = $this->prepareAuthor($query, $author);
        }

        if (null !== $tag) {
            $query = $this->prepareTags($query, $tag);
        }

        if (null !== $isbn) {
            $query = $this->prepareIsbn($query, $isbn);
        }

        if (null !== $sex) {
            $query = $this->prepareSex($query, $sex);
        }

        if ($aConfig['common']['bookstore_code'] == 'smarkacz') {

            if (null !== $fromAge || null !== $toAge) {
                $query = $this->prepareAges($query, $fromAge, $toAge);
            }

            $query['filter']['bool']['must'][] = [
                'term' => [
                    'is_smarkacz_product' => 1
                ]
            ];
        } else {
            $query['filter']['bool']['must'][] = [
                'terms' => [
                    'product_type' => $aConfig['common']['allowed_product_type']
                ]
            ];
        }

        return $query;
    }

    private function prepareSex($query, $sex)
    {
        $query['filter']['bool']['must'][] = [
            'term' => [
                'sex' => $sex
            ]
        ];

        return $query;
    }

    private function prepareSeries($query, $series, $publisher)
    {
        $sqlPublisher = "SELECT id from products_publishers where name = '$publisher'";
        $publisherId = $this->pDbMgr->GetOne($this->website, $sqlPublisher);

        $sql = "SELECT id FROM products_series where `name` = '%s' AND publisher_id = %d";
        $sId = $this->pDbMgr->GetRow($this->website, sprintf($sql, $series, $publisherId));

        if (empty($sId)) {
            return $query;
        }

        $query['filter']['bool']['must'][] = [
            'query' => [
                'match_phrase' => [
                    'series' => $sId['id']
                ]
            ]
        ];

        return $query;
    }

    private function prepareUnavailable($query)
    {
        $query['filter']['bool']['must'][] = [
            'terms' => [
                'prod_status' => [1, 3]
            ]
        ];

        return $query;
    }

    private function preparePublicationYear($query, $publicationYear)
    {
        $query['filter']['bool']['must'][] = [
            'term' => [
                'publication_year' => $publicationYear
            ]
        ];

        return $query;
    }

    private function preparePublisher($query, $publisher)
    {
        $sqlPublisher = "SELECT id from products_publishers where name = '$publisher'";
        $publisherId = $this->pDbMgr->GetOne($this->website, $sqlPublisher);

//        // stara wersja, byc moze potrzebna
//        $query['filter']['bool']['must'][] = [
//            'query' => [
//                'match_phrase' => [
//                    'publisher' => $publisher
//                ]
//            ]
//        ];

        if (empty($publisherId)) {

            $publisherId = 99999;
        }

        $query['filter']['bool']['must'][] = [
            'term' => [
                'publisher_id' => $publisherId
            ]
        ];

        return $query;
    }

    private function prepareAges($query, $fromAge = null, $toAge = null)
    {
        if (isset($_GET['age_type']) && $_GET['age_type'] != null && $_GET['age_type'] == 'Y') {
            $fromAge = $fromAge * 12;
            $toAge = $toAge * 12;
        }

        if (null !== $fromAge) {
            $query['filter']['bool']['must'][] = [
                'range' => [
                    'from_age' => [
                        'gte' => $fromAge,
                    ]
                ]
            ];
        }

        if (null !== $toAge) {
            $query['filter']['bool']['must'][] = [
                'range' => [
                    'to_age' => [
                        'lte' => $toAge
                    ]
                ]
            ];
        }

        return $query;
    }

    private function preparePrices($query, $priceFrom = null, $priceTo = null)
    {
        if (null !== $priceFrom) {
            $query['filter']['bool']['must'][] = [
                'range' => [
                    $this->getPriceBruttoColumn() => [
                        'gte' => $priceFrom
                    ]
                ]
            ];
        }

        if (null !== $priceTo) {
            $query['filter']['bool']['must'][] = [
                'range' => [
                    $this->getPriceBruttoColumn() => [
                        'lte' => $priceTo
                    ]
                ]
            ];
        }

        return $query;
    }

    private function getPriceBruttoColumn()
    {
        return $this->website . '_price_brutto';
    }

    private function getCatsColumn()
    {
        return $this->website . '_cats';
    }

    private function hasValue($query, $string, array $data)
    {
        if (empty($data)) {
            return $query;
        }

        $query['filter']['bool']['must'][] = [
            'terms' => [
                $string => $data
            ]
        ];

        return $query;
    }

    private function prepareAuthor($query, $author)
    {
        $query['filter']['bool']['must'][] = [
            'query' => [
                'match_phrase' => [
                    'authors' => str_replace('-', ' ', $author)
                ]
            ]
        ];

        return $query;
    }

    private function prepareTags($query, $tag)
    {
        $query['filter']['bool']['must'][] = [
            'query' => [
                'match' => [
                    'tags' => [
                        'query' => $tag,
                        'operator' => 'AND'
                    ]
                ]
            ]
        ];
//        $query['filter']['bool']['must'][] = [
//            'term' => [
//                'tags' => $tag
//            ]
//        ];


        return $query;
    }

    private function prepareIsbn($query, $isbn)
    {
        $isbnFields = [
            'isbn', 'isbn_plain', 'isbn_10', 'isbn_13', 'ean_13'
        ];

        $terms = [];

        foreach ($isbnFields as $field) {

            $terms[] = [
                'term' => [
                    $field => $isbn
                ]
            ];
        }

        $query['filter']['bool']['must'][] = [
            'or' => [
                'filters' => $terms
            ]
        ];

        return $query;
    }
}