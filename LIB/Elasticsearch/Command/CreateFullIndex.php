<?php

use Elasticsearch\Profit\ElasticIndex;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'Off');
ini_set('safe_mode', 'Off');
ini_set('error_reporting', 30711);
ini_set('max_execution_time', 432000);
ini_set('memory_limit', '512M');

//error_reporting(E_ALL ^ E_NOTICE ^ E_USER_NOTICE);

$aConfig['config']['project_dir'] = 'LIB/Elasticsearch/Command';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

purgeBuffor();
$indexService = new ElasticIndex();

$count = countAvailableProducts();
var_dump("Pierwsze przeliczenie: ".$count);

//if ($count < 1) {
//    purgeBuffor();
//}

//$indexService->createIndex([673274]);
//$indexService->createIndex([673274,872926,721724,905270,898994,880876,834897,797483,629390,618355,612324,908371,927417,518009,926382]);
//$indexService->createIndex(null, 10000);

$i = 0;

while(true) {

    $i++;
    $counted = countAvailableProducts();

    var_dump("Przeliczenie $i: ".$counted);

    $temp = $indexService->createIndex(null, 25000, true);
    unset($temp);

    if ($i > 23 || $counted < 1) {

        break;
    }
}

function purgeBuffor()
{
    global $pDbMgr;

    $pDbMgr->Query('profit24', "DELETE FROM products_index_create");
}

function countAvailableProducts()
{
    global $pDbMgr;

    $productSql = "
          SELECT DISTINCT COUNT(P.id)
          FROM products AS P
          LEFT JOIN products_index_create AS PIC ON P.id = PIC.product_id
          WHERE 1 = 1
          AND PIC.product_id IS NULL
          ";

    $count = $pDbMgr->GetOne('profit24', $productSql);

    return $count;
}