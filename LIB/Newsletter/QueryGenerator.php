<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Newsletter;


class QueryGenerator
{
    /**
     * @var array
     */
    private $queryData;

    public function __construct(array $queryData)
    {
        $defaultValues = [
            'promotions_agreement' => '1',
            'deleted' => '0',
            'fields' => [],
        ];

        $merged = array_merge($defaultValues, $queryData);

        array_unshift($merged['fields'], 'UI.email');

        $this->queryData = $merged;
    }

    public function generateQuery()
    {
        $baseQuery = sprintf("SELECT DISTINCT %s FROM users_accounts AS UI", implode(',', $this->queryData['fields']));

        switch ($this->queryData['strategy']) {
            case 'category':
                $baseQuery = $this->categoryStrategy($baseQuery);
                break;
            case 'product':
                $baseQuery = $this->productStrategy($baseQuery);
                break;
            default:
                $baseQuery = $this->defaultStrategy($baseQuery);
                break;
        }

        $baseQuery = $this->addUserAccountsTableConditions($baseQuery);

        return $baseQuery;
    }

    private function defaultStrategy($baseQuery)
    {
        $orderStatues = $this->getOrdersStatusesConditions();

        if (!empty($this->queryData['year']) || !empty($this->queryData['publisher']) || !empty($this->queryData['created_from']) || !empty($this->queryData['created_to']) || null !== $orderStatues)
        {
            $orderStatues = $this->getOrdersStatusesConditions();
            $baseQuery .= sprintf("\r\nJOIN orders AS O ON UI.id = O.user_id %s", $orderStatues);
            $baseQuery = $this->modifyOrder($baseQuery);
            $baseQuery .= sprintf("\r\nJOIN orders_items AS OI ON O.id = OI.order_id");
            $baseQuery = $this->modifyOrderItems($baseQuery);
        }

        return $baseQuery;
    }

    private function productStrategy($baseQuery)
    {
        $orderStatues = $this->getOrdersStatusesConditions();
        $baseQuery .= sprintf("\r\nJOIN orders AS O ON UI.id = O.user_id %s", $orderStatues);
        $baseQuery = $this->modifyOrder($baseQuery);
        $baseQuery .= sprintf("\r\nJOIN orders_items AS OI ON O.id = OI.order_id AND OI.product_id IN(%s)", $this->queryData['products']);
        $baseQuery = $this->modifyOrderItems($baseQuery);

        return $baseQuery;
    }

    private function categoryStrategy($baseQuery)
    {
        $orderStatues = $this->getOrdersStatusesConditions();
        $pageIds = implode(',', $this->queryData['extra_categories']);

        $baseQuery .= sprintf("\r\nJOIN orders AS O ON UI.id = O.user_id %s", $orderStatues);
        $baseQuery = $this->modifyOrder($baseQuery);
        $baseQuery .= sprintf("\r\nJOIN orders_items AS OI ON O.id = OI.order_id");
        $baseQuery = $this->modifyOrderItems($baseQuery);
        $baseQuery .= sprintf("\r\nJOIN products_extra_categories AS PEC ON OI.product_id = PEC.product_id AND PEC.page_id IN (%s)", $pageIds);

        return $baseQuery;
    }

    private function modifyOrder($baseQuery)
    {
        if (!empty($this->queryData['created_from'])) {
            $from = sprintf("\r\nAND O.order_date >= '%s'", (new \DateTime($this->queryData['created_from']))->format('Y-m-d'));
            $baseQuery .= $from;
        }

        if (!empty($this->queryData['created_to'])) {
            $to = sprintf("\r\nAND O.order_date <= '%s'", (new \DateTime($this->queryData['created_to']))->format('Y-m-d'));
            $baseQuery .= $to;
        }

        return $baseQuery;
    }

    private function modifyOrderItems($baseQuery)
    {
        if (!empty($this->queryData['year'])) {
            $baseQuery .= sprintf(" AND OI.publication_year = %s", $this->queryData['year']);
        }

        if (!empty($this->queryData['publisher'])) {
            $baseQuery .= sprintf(" AND OI.publisher = '%s'", $this->queryData['publisher']);
        }

        return $baseQuery;
    }

    private function getOrdersStatusesConditions()
    {
        $statuses = null;

        if (isset($this->queryData['order_statuses'])) {

            $implodedStatuses = '';

            $i = 0;
            foreach($this->queryData['order_statuses'] as $orderStatus) {

                $i++;

                if ($i == 1) {
                    $implodedStatuses .= sprintf("O.order_status = '%s'", $orderStatus);
                    continue;
                }

                $implodedStatuses .= sprintf(" OR O.order_status = '%s'", $orderStatus);
            }

            $statuses = sprintf(" AND (%s)", $implodedStatuses);
        }

        return $statuses;
    }

    /**
     * @param $baseQuery
     * @return string
     */
    private function addUserAccountsTableConditions($baseQuery)
    {
        $websiteId = $this->queryData['website_id'];
        $baseQuery .= "\r\nWHERE UI.website_id = $websiteId";

        if (isset($this->queryData['date_from']) && !empty($this->queryData['date_from'])) {
            $from = sprintf("\r\nAND UI.created >= '%s'", (new \DateTime($this->queryData['date_from']))->format('Y-m-d'));
            $baseQuery .= $from;
        }

        if (isset($this->queryData['date_to']) && !empty($this->queryData['date_to'])) {
            $to = sprintf("\r\nAND UI.created <= '%s'", (new \DateTime($this->queryData['date_to']))->format('Y-m-d'));
            $baseQuery .= $to;
        }

        $baseQuery .= sprintf("\r\nAND UI.promotions_agreement = '%s'", $this->queryData['promotions_agreement']);
        $baseQuery .= sprintf("\r\nAND UI.deleted = '%s'", $this->queryData['deleted']);

        return $baseQuery;
    }
}