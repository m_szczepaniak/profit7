<?php
/**
 * Created by PhpStorm.
 * User: lukasz
 * Date: 28.04.17
 * Time: 11:38
 */

namespace Service;

class ProductsTariffsService
{
    const VIP_STATUS = 1;

    const NON_VIP_STATUS = 0;

    /**
     * @param $promotionId
     * @param $status
     * @return int
     * @throws \Exception
     */
    public function changeTariffsVipStatus($promotionId, $status)
    {
        global $pDbMgr, $aConfig;

        $bookstoreCode = $aConfig['common']['bookstore_code'];

        if (null == $bookstoreCode) {

            throw new \Exception("bookstorecode doesnt exist");
        }

        $sql = "
        UPDATE products_tarrifs SET vip = $status WHERE promotion_id = $promotionId
        ";

        $res = $pDbMgr->Query($bookstoreCode, $sql);

        return $res;
    }
}