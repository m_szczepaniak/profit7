<?php

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */

use LIB\Fv\FvGenerator;

ini_set('max_execution_time', 1500);

$aConfig['config']['project_dir'] = 'LIB/Fv/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

/**
 * START
 */

$fromDate = (new DateTime('first day of this month'))->format('Y-m-d 00:00:00');
$toDate = (new DateTime('last day of this month'))->format('Y-m-d 23:59:00');

$fromDate = new DateTime($fromDate);
$toDate = new DateTime($toDate);

$fvGenerator = new FvGenerator();
$fvGenerator->createBackupFromAllServices($fromDate, $toDate);