<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\Fv;

use DateTime;
use Exception;
use FtpClient\FtpClient;
use Invoice;
use LIB\Helpers\FileHelper;

include_once('lib/Invoice.class.php');

class FvGenerator
{
    private $websites;

    private $mailTemplatePath;

    private $backupDirectory;

    private $pDbMgr;

    private $host = '192.168.1.142';

    private $login = 'developer';

    private $password = 'developer$21';

    public function __construct()
    {
        global $pDbMgr, $aConfig;

        $this->pDbMgr = $pDbMgr;
        $this->websites = $this->pDbMgr->GetAll('profit24', "SELECT id, code FROM websites WHERE code != 'scale' AND code != 'it'");
        $this->mailTemplatePath = $aConfig['common']['client_base_path'] . 'omniaCMS/smarty/templates/';
        $this->backupDirectory = realpath($_SERVER['DOCUMENT_ROOT'].'../fv_backup');

        if (false === $this->backupDirectory) {

            throw new Exception('Directory fv_backup does not exist or has invalid privilages');
        }
    }

    public function createBackupFromAllServices(DateTime $fromDate, DateTime $toDate)
    {
        foreach($this->websites AS $website) {

            $path = $this->backupDirectory.'/'.$website['id'].'/'.date("Y").'/'.date("m");

            try {

                $this->createBackupForWebsite($website['id'], $path, $fromDate, $toDate);

                $this->removeTempFiles($path);
            } catch(Exception $e) {

                $this->removeTempFiles($path);
            }
        }
    }

    private function removeTempFiles($path)
    {
        $files = glob($path . '/*', GLOB_MARK);

        foreach($files as $file)
        {
            if ((strpos($file, '.pdf') && strpos($file, 'fv_backup')) || (strpos($file, '.zip') && strpos($file, 'fv_backup'))) {

                unlink($file);
            }
        }
    }

    private function createBackupForWebsite($websiteId, $path, DateTime $fromDate, DateTime $toDate)
    {

        $fromDateFormatted = $fromDate->format('Y-m-d H:i:s');
        $toDateFormatted = $toDate->format('Y-m-d H:i:s');

        $zipFilename = date("Y").'_'.date("m").'.zip';

        $zipPath = $this->backupDirectory.'/'.$websiteId.'/'.date("Y").'/'.date("m").'/'.$zipFilename;

        $ordersWithFv = $this->pDbMgr->PlainQuery('profit24', "
          SELECT *
          FROM orders
          WHERE invoice_id IS NOT NULL
          AND invoice_id != ''
          AND order_date >= '$fromDateFormatted'
          AND order_date <= '$toDateFormatted'
          AND website_id = $websiteId
        ");

        while ($order =& $ordersWithFv->fetchRow(DB_FETCHMODE_ASSOC)) {

            if (null == $order) {
                continue;
            }

            $this->createAndSaveDoc($path, $order);
        }

        exec("zip -r -j $zipPath $path");

        $ftp = new FtpClient();
        $ftp->connect($this->host);
        $ftp->login($this->login, $this->password);
        $ftp->pasv(true);

        $isDir = $ftp->isDir('fv_backup/'.$websiteId);

        if (false == $isDir) {

            // TODO jak false to znaczy ze nie moze utworzyc, wywalic wyjatek
            $mkDirRes = $ftp->mkdir('fv_backup/'.$websiteId, true);
        }

        error_reporting(E_ALL);
        ini_set('display_errors', 1);

        // TODO sprawdzic jak cos nie Wyjdzie
        $uploadRes = $ftp->put('fv_backup/'.$websiteId.'/'.$zipFilename, $zipPath, FTP_ASCII);
    }

    private function createAndSaveDoc($path, array $order)
    {
        FileHelper::mkDirRecursive($path);

        $oInvoice = new Invoice();
        $oInvoice->sTemplatePath = $this->mailTemplatePath;
        $invoice = $oInvoice->getInvoicePDF($order['id'], $order, 3);

        $path .= '/'.$order['invoice_id'].'.pdf';

        FileHelper::saveFile($path, $invoice);

        unset($oInvoice);
        unset($invoice);
    }
}
