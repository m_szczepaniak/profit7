<?php

namespace Product;

use DatabaseManager;

class ProductUpdater
{
    /**
     * @var DatabaseManager $db
     */
    private $db;

    public function __construt()
    {
        global $pDbMgr;

        $this->db = $pDbMgr;
    }

    /**
     * @param $productId
     *
     * @return bool|mixed
     */
    public function updateProductMainCategory($productId)
    {
        $sql = "
        SELECT PEC.page_id
        FROM products AS P
        JOIN products_extra_categories as PEC ON P.id = PEC.product_id
        JOIN menus_items AS MI ON MI.id = PEC.page_id AND MI.published = '1' AND MI.priority > 0
        WHERE P.id = 1
        ORDER BY MI.priority DESC
        LIMIT 1;
        ";

        $result = $this->db->GetCol('profit24', $sql);

        if(true === empty($result)) {
            return false;
        }

        return $this->db->Update('profit24', 'products', ['main_category_id' => $result], 'id = '.$productId);
    }
}