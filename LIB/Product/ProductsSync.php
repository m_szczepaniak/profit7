<?php

namespace Product;

use Common;

class ProductsSync {
    /**
     * @var $merlinDatabaseUrl
     */
    private $merlinDatabaseUrl = "https://mas.merlin.pl/feed/get/profitm/availability";

    /**
     * @var \DatabaseManager $pDbMgr
     */
    private $pDbMgr;

    /**
     * @var $merlinProducts
     */
    private $merlinProducts;

    /**
     * @var $profitProducts
     */
    private $profitProducts;

    /**
     * @var $mappedSourceByMerlinCode
     */
    private $mappedSourceByMerlinCode;

    /**
     * @var $productsStockPrefixCols
     */
    private $productsStockPrefixCols;

    const MAIN_SOURCE_STOCK = 'profit_j';

    const MAIN_MERLIN_NOT_MAPPED = 'merlin';

    private $debug = false;

    private $debugGetTestFile = false;

    function __construct($pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
        $this->debug = true;
        $this->debugGetTestFile = false;
        $_GET['benchmark'] = '1';
    }

    /**
     * Synchronizowanie produktów
     */
    function syncProducts() {
        echo "Pobieram feeda\n";
        $this->getMerlinProductsUpdates();
        echo "Gotowe\n";

        if ($this->merlinProducts === null || empty($this->merlinProducts) === true) {
            echo "Błąd pobierania feeda\n";
            return;
        }

        $offset = 0;
        $count = 1000;

        echo "Pobieram produkty oraz porównuję\n";
        $this->getProfitProductsStock($offset, $count);
        while(count($this->profitProducts) > 0) {
            echo "\r\033[K" . "Sprawdzono: " . $offset . " produktów";
            $this->findChanges();
            $offset += $count;
            $this->getProfitProductsStock($offset, $count);
        }
        echo "\r\033[KGotowe\n";
    }

    /**
     * Pobranie produktów z api merlina
     */
    protected function getMerlinProductsUpdates() {
        global $aConfig;

        if (true !== $this->debugGetTestFile) {

            $username = $aConfig['merlin']['user'];
            $password = $aConfig['merlin']['pass'];

            $ch = curl_init();

            curl_setopt_array($ch, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $this->merlinDatabaseUrl,
                CURLOPT_USERPWD => "$username:$password",
                CURLOPT_HTTPAUTH => CURLAUTH_BASIC
            ]);

            $rawData = curl_exec($ch);

            $this->saveFile($rawData);
        } else {
            $rawData = file_get_contents(__DIR__.'/logs/merlinStock-2019-02-25-07-33-40.json');
        }

        $merlinProducts = json_decode($rawData, true);
        foreach ($merlinProducts as $product) {
            $this->merlinProducts[$product['sku']] = $product;
        }

        curl_close($ch);
    }

    /**
     * Pobranie danych z products_stock
     */
    private function getProfitProductsStock($offset, $count) {
        $this->clearProfitProducts();

        $tmpProfitProducts = $this->pDbMgr->PlainQuery('profit24',
            'SELECT GROUP_CONCAT(PMM.merlin_sku SEPARATOR ",") AS merlin_sku, PS.* 
             FROM products_stock AS PS 
             JOIN products_merlin_mappings AS PMM 
              ON PMM.product_id = PS.id 
             GROUP BY PS.id
             LIMIT ' . $offset . ', ' . $count);


        if ($tmpProfitProducts !== null && $tmpProfitProducts !== false) {
            while ($product =& $tmpProfitProducts->fetchRow(DB_FETCHMODE_ASSOC)) {
                $this->profitProducts[$product['merlin_sku']] = $product;
            }
        }
    }

    /**
     * Metoda pobiera mapowanie
     *
     * @link https://topmall.atlassian.net/projects/PM/issues/PM-36
     * @param $code
     * @return string
     */
    private function getMappedSourceByMerlinCode($code) {

        if ($code == '' || $code == 'null' || $code == null) {
            // stock
            return self::MAIN_SOURCE_STOCK;
        }
         // Azymut	AZZ01
        $mappedSourceByMerlinCode = $this->setMappedSourcesByMerlinCode();

        if (isset($mappedSourceByMerlinCode[$code])) {
            return $mappedSourceByMerlinCode[$code];
        } else {
            return self::MAIN_MERLIN_NOT_MAPPED;
        }
    }

    /**
     * @return array
     */
    private function setMappedSourcesByMerlinCode() {

        if (!isset($this->mappedSourceByMerlinCode)) {
            $sSql = 'SELECT merlin_source_code, column_prefix_products_stock 
                     FROM sources
                     WHERE merlin_source_code <> "" AND merlin_source_code IS NOT NULL ';
            $this->mappedSourceByMerlinCode = $this->pDbMgr->GetAssoc('profit24', $sSql);
        }
        return $this->mappedSourceByMerlinCode;
    }

    /**
     * @return array
     */
    private function setProductsStockPrefixCols() {

        if (!isset($this->productsStockPrefixCols)) {
            $sSql = 'SELECT column_prefix_products_stock 
                     FROM sources
                     WHERE column_prefix_products_stock <> "" ';
            $this->productsStockPrefixCols = $this->pDbMgr->GetCol('profit24', $sSql);
        }
        return $this->productsStockPrefixCols;
    }


    /**
     * @param $aMerlinSkuGrouped
     * @return string sku
     */
    private function getMainProductMerlin($aMerlinSkuGrouped) {

        $minPrice = null;
        $mainSKU = null;
        $bestStatus = null;

        foreach ($aMerlinSkuGrouped as $sku) {
            if (isset($this->merlinProducts[$sku])) {
                $currentMerlinProduct = $this->merlinProducts[$sku];

                $statusProduct = $this->getProductStatus($currentMerlinProduct);
                $priceNettoBuy = Common::formatPrice2($currentMerlinProduct['price_netto_buy']);
                if ($statusProduct == '1') {
                    // available

                    $bestStatus = $statusProduct;
                    if (null === $minPrice) {
                        $minPrice = $priceNettoBuy;
                        $mainSKU = $sku;
                    } elseif ($priceNettoBuy < $minPrice) {
                        $minPrice = $priceNettoBuy;
                        $mainSKU = $sku;
                    }
                } elseif ($statusProduct == '3' && $bestStatus != '1') {
                    // preview and product dont have any available line
                    $bestStatus = $statusProduct;
                    $mainSKU = $sku;
                }
            }
        }
        if ($mainSKU === null) {
            return $aMerlinSkuGrouped[0];
        }

        return $mainSKU;
    }

    /**
     * @param $mainSKU
     * @param $productId
     * @return bool
     */
    private function updateMainSKU($mainSKU, $productId) {

        // zerujemy wszystkie
        $this->pDbMgr->BeginTransaction('profit24');
        $sSql = 'UPDATE products_merlin_mappings
                 SET main_sku = "0"
                 WHERE product_id = '.$productId;
        if (false === $this->pDbMgr->Query('profit24', $sSql)) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }

        $sSql = 'UPDATE products_merlin_mappings
                 SET main_sku = "1"
                 WHERE product_id = '.$productId.' 
                   AND merlin_id = '.$mainSKU.'
                 ';
        if (false === $this->pDbMgr->Query('profit24', $sSql)) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }

        $this->pDbMgr->CommitTransaction('profit24');
        return true;
    }


    /**
     * Wyszukanie zmian w stanach produktów
     */
    private function findChanges() {
        $maximal_markup = $this->getMaximalMarkup();
        $i = 0;
        foreach ($this->profitProducts as $merlinSkuGrouped => $profitProduct) {
            // najlepszy rekord
            $values = [];
            $aMerlinSkuGrouped = explode(',', $merlinSkuGrouped);
            if (!empty($aMerlinSkuGrouped) && is_array($aMerlinSkuGrouped) && count($aMerlinSkuGrouped) > 1) {
                $merlinSku = $this->getMainProductMerlin($aMerlinSkuGrouped);
                $this->updateMainSKU($merlinSku, $profitProduct['id']);
            } else {
                $merlinSku = $merlinSkuGrouped;
            }
            if ( isset( $this->merlinProducts[$merlinSku] ) ) {
                $merlinProduct = $this->merlinProducts[$merlinSku];

                $colProductsStockPrefix = $this->getMappedSourceByMerlinCode($merlinProduct['contract_code']);
                if (true === $this->debug) {
                    var_dump($profitProduct['id']);
                    var_dump($merlinSku);
                    var_dump($merlinProduct['contract_code']);
                    var_dump($colProductsStockPrefix);
                }
                $productStock = $this->getStockPriceNettoBrutto($profitProduct['id']);
                $vatDefault = $productStock['profit_g_vat'];
                if (Common::formatPrice2($vatDefault) <= 0) {
                    $vatDefault = Common::formatPrice2($merlinProduct['rate']);
                }

                $values[$colProductsStockPrefix.'_status'] = $this->getProductStatus($merlinProduct);
                $values[$colProductsStockPrefix.'_shipment_time'] = $this->getProfitShipmentTime($merlinProduct['eta']);
                $values[$colProductsStockPrefix.'_shipment_date'] = $profitProduct[$colProductsStockPrefix.'_shipment_date'] === null ? $merlinProduct['premiere_at'] : $profitProduct[$colProductsStockPrefix.'_shipment_date'];
                $values[$colProductsStockPrefix.'_wholesale_price'] = Common::formatPrice2($merlinProduct['price_netto_buy'] * (1 + $vatDefault/100));//Common::formatPrice2($merlinProduct['price_netto_buy']);
                $values[$colProductsStockPrefix.'_vat'] = $vatDefault;
                if ($productStock['profit_g_price_brutto'] > 0 && $productStock['profit_g_price_netto'] > 0) {
                    $values[$colProductsStockPrefix.'_price_netto'] = $productStock['profit_g_price_netto'];
                    $values[$colProductsStockPrefix.'_price_brutto'] = $productStock['profit_g_price_brutto'];
                } else {
                    // ma byc 17
                    $minimalSRPBrutto = Common::formatPrice2($values[$colProductsStockPrefix.'_wholesale_price']*(1 + $maximal_markup/100));

                    if (Common::formatPrice2($merlinProduct['srp']) > 0 && $minimalSRPBrutto < Common::formatPrice2($merlinProduct['srp'])) {
                        $values[$colProductsStockPrefix.'_price_netto'] = $vatDefault > 0 ? Common::formatPrice2($merlinProduct['srp'] / (1 + ($vatDefault / 100))) : $merlinProduct['srp'];
                        $values[$colProductsStockPrefix.'_price_brutto'] = Common::formatPrice2($merlinProduct['srp']);
                    } else {
                        $addDefaultMod = 40;
                        $newSRPNetto = Common::formatPrice2($merlinProduct['price_netto_buy'] * (1 + $addDefaultMod/100));
                        $values[$colProductsStockPrefix.'_price_netto'] = $newSRPNetto;
                        $values[$colProductsStockPrefix.'_price_brutto'] = Common::formatPrice2($newSRPNetto * (1 + $vatDefault/100));
                    }

                }

                $diff = array_diff_assoc($values, $profitProduct);

                if ( count( $diff ) > 0 ) {
                    $diff[$colProductsStockPrefix.'_last_import'] = "NOW()";
                    // mamy zmiany zerujemy wszystkie poza wybranym
                    $valuesUnsetStatus = $this->getAllSourcesColumnsFor('status', '0', $colProductsStockPrefix);
                    if (!empty($diff) && !empty($valuesUnsetStatus)) {
                        $diff = array_merge($diff, $valuesUnsetStatus);
                    }

                    if (true === $this->debug) {
                        var_export($diff);
                    }
                    if (false === $this->pDbMgr->Update('profit24', 'products_stock', $diff, ' id = ' . $profitProduct['id'] )) {
                        throw new \Exception('Błąd aktualizacji '.print_r($diff, true).' id = '.$profitProduct['id']);
                    }
                }
            } else {

                if (true === $this->checkAvailableInSource($profitProduct)) {
                    // zerujemy wszystkie kolumny
                    $values = $this->getAllSourcesColumnsFor('status', '0');
                    if (true === $this->debug) {
                        var_export($values);
                    }
                    if (false === $this->pDbMgr->Update('profit24', 'products_stock', $values, ' id = ' . $profitProduct['id'] )) {
                        throw new \Exception('Błąd aktualizacji '.print_r($values, true).' id = '.$profitProduct['id']);
                    }
                }
            }
            echo "\r\033[K" . "Iteruję: " . $i . " rekordów w products_stock";
            $i++;
        }
    }


    /**
     * @param $profitProduct
     * @return bool
     */
    private function checkAvailableInSource($profitProduct) {

        $allSourcesPrefix = $this->setProductsStockPrefixCols();
        foreach ($allSourcesPrefix as $sourcePrefix) {
            // dowolny ma status, trzeba wyzerować
            if ($profitProduct[$sourcePrefix.'_status'] !== '0') {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $columnPostfixName
     * @param $value
     * @param mixed null|string $exceptColumnPrefixName
     * @return mixed
     */
    private function getAllSourcesColumnsFor($columnPostfixName, $value, $exceptColumnPrefixName = null) {

        $allSourcesPrefix = $this->setProductsStockPrefixCols();
        foreach ($allSourcesPrefix as $columnPrefixName) {
                $values[$columnPrefixName . '_' . $columnPostfixName] = $value;
        }
        if (isset($exceptColumnPrefixName) && isset($values[$exceptColumnPrefixName . '_' . $columnPostfixName])) {
            unset($values[$exceptColumnPrefixName . '_' . $columnPostfixName]);
        }
        return $values;
    }

    /**
     * @param string $merlin
     * @return mixed
     */
    private function getMaximalMarkup($merlin = '14')
    {
        $sSql = 'SELECT MAX(mark_up) FROM products_general_discount 
                 WHERE source = "'.$merlin.'" 
                 ORDER BY mark_up DESC 
                 LIMIT 1';

        return $this->pDbMgr->GetOne('profit24',$sSql);
    }

    /**
     * @param $productId
     * @return array
     */
    private function getStockPriceNettoBrutto($productId) {

        $sSql = 'SELECT price_brutto as profit_g_price_brutto,price_netto as profit_g_price_netto,vat as profit_g_vat
                 FROM streamsoft_stock
                 WHERE product_id = '.$productId;

        return $this->pDbMgr->GetRow('profit24', $sSql);
    }

    /**
     * Czyści tablice produktów
     */
    private function clearProfitProducts() {
        unset($this->profitProducts);
        $this->profitProducts = [];
    }

    /**
     * Pobiera status produktu merlina
     *
     * @param $merlinProduct
     * @return int
     */
    private function getProductStatus($merlinProduct) {
        $status = 0;

        $string = $merlinProduct['premiere_at'];
        $date = date('Y-m-d',time());

        $time1 = strtotime($string);
        $time2 = strtotime($date);

        if(intval($merlinProduct['qty']) > 0) {
            $status = '1';
        } else if ($time1 > $time2) {
            $status = '3';
        }
        return $status;
    }

    /**
     * Pobranie shipment time profitu na podstawie shipment time merlina
     *
     * @param $merlinShipmentTime
     * @return int
     */
    private function getProfitShipmentTime($merlinShipmentTime) {
        $tmpShipmentTime = intval($merlinShipmentTime);
        $tmpShipmentTime--;
        if ($tmpShipmentTime < 0) {
            $tmpShipmentTime = 0;
        } else if ($tmpShipmentTime > 10) {
            $tmpShipmentTime = 10;
        }
        return $tmpShipmentTime;
    }

    /**
     * @param $rawData
     */
    private function saveFile($rawData)
    {
        $file = fopen($_SERVER['DOCUMENT_ROOT']."/LIB/Product/logs/merlinStock-" . date('Y-m-d-H-i-s') . ".json", 'w');
        fputs($file, $rawData);
        fclose($file);
    }
}