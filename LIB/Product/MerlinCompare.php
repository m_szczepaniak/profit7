<?php

namespace Product;

class MerlinCompare extends MerlinCompareBase {

    private $connectedToMultipleProducts = [];

    function __construct($pDbMgr)
    {

        $this->pDbMgr = $pDbMgr;
    }

    /**
     * Porównaj ofertę produktową
     */
    public function compareProducts() {

        $this->profitProducts[0] = [
            'id',
            'streamsoft_indeks',
            'name',
            'isbn',
            'isbn_plain',
            'isbn_13',
            'isbn_10',
            'ean_13',
            'created_by',
            'category_name',
            'product_type',
            'created',
            'publication_year',
            'pages',
            'author',
            'publisher',
            'image',
            'dimension',
            'amount',
            'description'
        ];

        $this->importData();
        echo "Zaimportowano dane\n";

        echo "Przygotowuję dane\n";
        $this->prepareData();

        echo "Wyszukuje braków w proficie\n";
        $missingProfit = $this->findMissingInProfit();

        echo "Wyszukuje braków w merlinie\n";
        $missingMerlin = $this->findMissingInMerlin();

        echo "Zapisuje dane do pliku csv\n";
        $this->saveDataToCSV($missingProfit, $missingMerlin);

    }

    public function findProfitMissing() {
        echo "Pobieram bazę produktów merlina\n";
        $startTime = microtime(true);
//        $this->downloadMerlinProducts();
        echo "Zakończono w: ". (microtime(true) - $startTime) ." sekundy\n";

        echo "Importuje merlina\n";
        $this->merlinProducts = $this->importCSV("merlin_products.csv", ",");

        $profitSKUs = $this->getProfitSku();

        echo "Zaimportowano dane\n";

        echo "Przygotowuję dane\n";
        $this->prepareData();

        echo "Wyszukuje braków w proficie\n";
        $missingProfit = $this->findMissingInProfitWithoutMapping($profitSKUs);

        echo "Zapisuje dane do pliku csv\n";
        $this->saveCSV('profit_missing.csv', $this->merlinProducts);
    }

    /**
     * Importuje produkty merlina i profitu
     */
    private function importData() {
        echo "Pobieram bazę produktów merlina\n";
        $startTime = microtime(true);
        $this->downloadMerlinProducts();
        echo "Zakończono w: ". (microtime(true) - $startTime) ." sekundy\n";

        echo "Importuje merlina\n";
        $this->merlinProducts = $this->importCSV("merlin_products.csv", ",");

        echo "Importuje profit\n";
        $this->profitProducts = $this->profitProducts + $this->importCSV("profit_products.csv", ",");
    }

    /**
     * Pobiera produkty z feeda merlina
     */
    private function downloadMerlinProducts() {
        $this->getMerlinProducts(true);

        $f = fopen("merlin_products.csv",'w');
        foreach ($this->merlinProducts as $merlinProduct) {
            fputcsv($f, $merlinProduct);
        }
        fclose($f);
    }

    /**
     * Wyciągnięcie SKU z bazy
     *
     * @return array|bool
     */
    private function getProfitSku() {
        $sql = "SELECT merlin_sku FROM products_merlin_mappings";
        $profitSKUs = [];
        $SKUs = $this->pDbMgr->PlainQuery('profit24', $sql);

        while ($sku =& $SKUs->fetchRow(DB_FETCHMODE_ASSOC)) {
            $profitSKUs[$sku['merlin_sku']] = $sku['merlin_sku'];
        }
        return $profitSKUs;
    }

    /**
     * Wyszukaj brakujących produktów w proficie
     *
     * @return array
     */
    private function findMissingInProfit() {
        $missing = [];
        $all = count($this->mEan);
        $processed = 1;
        $missingCount = 1;
        foreach ($this->mEan as $ean => $id) {
            $this->showProgress($processed++, $all, $missingCount);

            $missingProduct = null;
            $pid = 0;

            if ($this->findEanInProfit((string)$ean) === null) {
                $missingProduct = $id;

                if ( strlen($ean) == 12 ) {
                    $eanZero = '0' . $ean;

                    if ( isset($this->mEan[$eanZero]) === false && ($tmpPid = $this->findEanInProfit((string)$eanZero)) !== null) {
                        $missingProduct = null;
                        $pid = $tmpPid;
                    }
                }
            } else {
                $pid = $this->findEanInProfit((string)$ean);
            }

            if ($missingProduct !== null) {
                $missing[] = $missingProduct;
                $missingCount++;
            } else {
                $similarProduct = $this->createSimilarProduct($id, $pid);

                if (isset($this->similarProducts[$pid])) {
                    $this->connectedToMultipleProducts[] = $this->similarProducts[$pid];
                    $this->connectedToMultipleProducts[] = $similarProduct;
                    unset($this->similarProducts[$pid]);
                } else {
                    $this->similarProducts[$pid] = $similarProduct;
                }
            }
        }

        $this->clearLine();

        return $missing;
    }

    /**
     * Wyszukanie produktów merlina które nie mają u nas sku
     *
     * @param $profitSKUs
     */
    private function findMissingInProfitWithoutMapping($profitSKUs) {
        foreach ($this->merlinProducts as $key => $merlinProduct) {
            if (isset($profitSKUs[(int)$merlinProduct[0]])) {
                unset($this->merlinProducts[$key]);
            }
        }
    }

    /**
     * Wyszukaj brakujących produktów w merlinie
     *
     * @return array
     */
    private function findMissingInMerlin() {
        $missing = [];
        $all = count($this->profitProducts);
        $processed = 1;
        $missingCount = 1;

        foreach ($this->profitProducts as $id => $product) {
            $this->showProgress($processed++, $all, $missingCount);

            if ( $this->isEanInMerlin($product, $this->mEan) === false &&
                $this->isEanInMerlin($product, $this->mEanZero) === false )
            {
                $missing[] = $id;
                $missingCount++;
            }
        }

        $this->clearLine();

        return $missing;
    }

    /**
     * Zapisuje dane do pliku CSV
     *
     * @param $merlinMissing
     * @param $profitMissing
     */
    private function saveDataToCSV($profitMissing, $merlinMissing) {
        $this->saveCSV('profit_missing.csv', $this->merlinProducts, $profitMissing);

        $this->saveCSV('merlin_missing.csv', $this->profitProducts, $merlinMissing);

        $this->saveCSV('connected_products.csv', $this->similarProducts);

        $this->saveCSV('connected_to_multiple_products.csv', $this->connectedToMultipleProducts);
    }
}