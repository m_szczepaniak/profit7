<?php

namespace Product;

abstract class MerlinCompareBase
{
    /**
     * @var \DatabaseManager
     */
    protected $pDbMgr;

    /**
     * @var string Merlin Database url
     */
    private $merlinDatabaseUrl = "https://mas.merlin.pl/feed/get/profitm/products?param[limit]=%d&param[offset]=%d";

    /**
     * @var array Array z produktami merlina
     */
    protected $merlinProducts = [];

    /**
     * @var array Array z produktami profitu
     */
    protected $profitProducts = [];

    /**
     * @var array Array z EAN merlina jako indeks
     */
    protected $mEan;

    /**
     * @var array Array z EAN merlina jako indeks z dodatkowym zerem na początku
     */
    protected $mEanZero;

    /**
     * @var array Array z ISBN profitu jako indeks
     */
    protected $pISBN;

    /**
     * @var array Array z ISBN10 profitu jako indeks
     */
    protected $pISBN10;

    /**
     * @var array Array z ISBN13 profitu jako indeks
     */
    protected $pISBN13;

    /**
     * @var array Array z EAN13 profitu jako indeks
     */
    protected $pEAN13;

    /**
     * @var array Array z połączonymi produktami
     */
    protected $similarProducts = [];

    /**
     * Przygotuj dane do wyszukiwania
     */
    protected function prepareData() {
        foreach ($this->merlinProducts as $id => $product) {
            if(count($product) === 8) {
                $this->mEan[(string)$product[5]] = $id;
            }
        }

        foreach ($this->merlinProducts as $id => $product) {
            if (strlen($product[5]) == 12 && isset($this->mEan['0' . $product[5]]) === false ) {
                $this->mEanZero['0' . $product[5]] = $id;
            }
        }

        foreach ($this->profitProducts as $id => $product) {
            if ( empty($product[3]) !== true ) {
                $this->pISBN[(string)$product[3]] = $id;
            }
            if ( empty($product[6]) !== true ) {
                $this->pISBN10[(string)$product[6]] = $id;
            }
            if ( empty($product[5]) !== true ) {
                $this->pISBN13[(string)$product[5]] = $id;
            }
            if ( empty($product[7]) !== true ) {
                $this->pEAN13[(string)$product[7]] = $id;
            }
        }
    }

    /**
     * Przygotuj dane do wyszukiwania
     */
    protected function prepareDataAssoc() {
        foreach ($this->merlinProducts as $id => $product) {
            $this->mEan[$product['ean']] = $id;
        }

        foreach ($this->profitProducts as $id => $product) {
            $this->pISBN[$product['isbn']] = $id;
            $this->pISBN10[$product['isbn_10']] = $id;
            $this->pISBN13[$product['isbn_13']] = $id;
            $this->pEAN13[$product['ean_13']] = $id;
        }
    }

    /**
     * Wyświetl postęp
     *
     * @param $progress
     * @param $total
     * @param null $missing
     */
    protected function showProgress($progress, $total, $missing = null) {
        echo "\r\033[K" . ($total - $progress) . '/' . $total . ($missing !== null ? " Brakuje: " . $missing : "");
    }

    /**
     * Wyczyść linię
     */
    protected function clearLine() {
        echo "\r\033[K";
    }

    /**
     * Stwórz array podobnego produktu
     *
     * @param $id
     * @param $pid
     * @return array
     */
    protected function createSimilarProduct($id, $pid) {
        return [
            $this->merlinProducts[$id][0],
            $this->merlinProducts[$id][1],
            $this->merlinProducts[$id][5],
            $this->profitProducts[$pid][0],
            $this->profitProducts[$pid][1],
            $this->profitProducts[$pid][2],
            $this->profitProducts[$pid][3],
            $this->profitProducts[$pid][4],
            $this->profitProducts[$pid][5],
            $this->profitProducts[$pid][6],
            $this->profitProducts[$pid][7],
            $this->profitProducts[$pid][8],
            $this->profitProducts[$pid][9],
            $this->profitProducts[$pid][10],
            $this->profitProducts[$pid][11],
            $this->profitProducts[$pid][12],
            $this->profitProducts[$pid][13],
            $this->profitProducts[$pid][14],
            $this->profitProducts[$pid][15],
            $this->profitProducts[$pid][16],
            $this->profitProducts[$pid][17],
            $this->profitProducts[$pid][18],
        ];
    }

    /**
     * Znajdź EAN w danych profitu
     *
     * @param $ean string EAN
     * @return string|null
     */
    protected function findEanInProfit($ean) {
        $pid = null;

        if (isset($this->pISBN[$ean]) === true) {
            $pid = $this->pISBN[$ean];
        } else if (isset($this->pISBN10[$ean]) === true) {
            $pid = $this->pISBN10[$ean];
        } else if (isset($this->pISBN13[$ean]) === true) {
            $pid = $this->pISBN13[$ean];
        } else if (isset($this->pEAN13[$ean]) === true) {
            $pid = $this->pEAN13[$ean];
        }

        return $pid;
    }

    /**
     * Znajdź EAN w danych merlina
     *
     * @param $profitProduct
     * @param $merlinProducts
     * @return bool
     */
    protected function isEanInMerlin($profitProduct, $merlinProducts) {
        return ((empty($profitProduct[3]) === false && isset($merlinProducts[(string)$profitProduct[3]]) === true) ||
            (empty($profitProduct[5]) === false && isset($merlinProducts[(string)$profitProduct[5]]) === true) ||
            (empty($profitProduct[6]) === false && isset($merlinProducts[(string)$profitProduct[6]]) === true) ||
            (empty($profitProduct[7]) === false && isset($merlinProducts[(string)$profitProduct[7]]) === true) );
    }

    /**
     * Importuje zamówienia z CSV
     *
     * @param $filename
     * @param $delimiter
     * @return array
     */
    protected function importCSV($filename, $delimiter) {
        $resultData = [];

        if (($handle = fopen($filename, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, $delimiter)) !== FALSE) {
                foreach ($data as $key => $value) {
                    $tmpStr = $value;
                    $tmpStr = str_replace("\n", '', $tmpStr);
                    $tmpStr = str_replace("\r", '', $tmpStr);
                    $tmpStr = str_replace("^M", '', $tmpStr);
                    $data[$key] = $tmpStr;
                }
                $resultData[] = $data;
            }
            fclose($handle);
        }
        return $resultData;
    }

    /**
     * Pobranie produktów z api merlina
     * @param bool $onlySku
     */
    protected function getMerlinProducts($onlySku = false) {
        global $aConfig;

        $merlinProducts = [];

        $maxPerRequest = 100000;
        $offset = 0;

        $username = $aConfig['merlin']['user'];
        $password = $aConfig['merlin']['pass'];

        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => sprintf($this->merlinDatabaseUrl, $maxPerRequest, $offset),
            CURLOPT_USERPWD => "$username:$password",
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC
        ]);

        while(count($tmpMerlinProducts = json_decode(curl_exec($ch), true)) > 0) {

            foreach ($tmpMerlinProducts as $key => $tmpMerlinProduct) {
                $tmpStr = $tmpMerlinProduct;
                $tmpStr = str_replace("\n", '', $tmpStr);
                $tmpStr = str_replace("\r", '', $tmpStr);
                $tmpStr = str_replace("^M", '', $tmpStr);
                if ($onlySku === true) {
                    $tmpMerlinProducts[$key] = [
                        'id' => $tmpStr['id'],
                        'sku' => $tmpStr['sku'],
                        'ean' => $tmpStr['ean'],
                    ];
                } else {
                    $tmpMerlinProducts[$key] = $tmpStr;
                }
            }

            $merlinProducts = array_merge($merlinProducts, $tmpMerlinProducts);

            echo "\r\033[K" . "Pobrano: " . count($merlinProducts) . " produktów";

            $offset += count($tmpMerlinProducts);
            curl_setopt($ch, CURLOPT_URL, sprintf($this->merlinDatabaseUrl, $maxPerRequest, $offset));
        }

        curl_close($ch);

        $this->merlinProducts = $merlinProducts;

        echo "\r\033[K";
    }

    /**
     * Zapis arraya do CSV
     *
     * @param $filename
     * @param $data
     * @param array $indexes
     */
    protected function saveCSV($filename, array $data, array $indexes = null) {
        $file = fopen($filename, 'w');

        if ($indexes === null) {
            foreach ($data as $element) {
                fputcsv($file, $element);
            }
        } else {
            foreach ($indexes as $key) {
                fputcsv($file, $data[$key]);
            }
        }
        fclose($file);
    }
}