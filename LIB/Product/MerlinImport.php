<?php

namespace Product;


class MerlinImport extends MerlinCompareBase
{
    function __construct($pDbMgr)
    {

        $this->pDbMgr = $pDbMgr;
    }

    /**
     * Dodanie id produktów merlina do naszej bazy produktów
     */
    public function importMerlinProducts() {
        echo "Pobieram produkty z merlina\n";
        $this->getMerlinProducts(true);

        echo "Pobieram produkty z profitu\n";
        $this->getProfitProducts();

        echo "Parsowanie danych\n";
        $this->prepareDataAssoc();

        echo "Wyszukiwanie połączeń\n";
        $this->findSimilarInProfit();

        echo "Zapisuje do bazy\n";
        $this->saveToDatabase();

        echo "Gotowe\n";
    }

    /**
     * Pobranie naszej oferty produktowej
     */
    private function getProfitProducts() {
        $sSql = 'SELECT p.id, p.streamsoft_indeks, p.name, p.isbn, p.isbn_13, p.isbn_10, p.ean_13, pmm.merlin_id, pmm.merlin_sku
                FROM products as p
                LEFT JOIN products_merlin_mappings as pmm
                ON p.id = pmm.product_id
                WHERE pmm.merlin_sku IS NULL AND pmm.merlin_id IS NULL';

        $products = $this->pDbMgr->PlainQuery('profit24', $sSql);

        if ($products !== null && $products !== false) {
            while ($product =& $products->fetchRow(DB_FETCHMODE_ASSOC)) {
                $this->profitProducts[] = $product;
            }
        }
    }

    /**
     * Wyszukaj brakujących produktów w proficie
     */
    private function findSimilarInProfit() {
        $all = count($this->mEan);
        $processed = 1;
        foreach ($this->mEan as $ean => $id) {
            echo "\r\033[K" . ($all - $processed) . '/' . $all . " Połączono: " . count($this->similarProducts);

            if (($pid = $this->findEanInProfit($ean)) !== null) {
                $this->similarProducts[] = [
                    'merlin_id' => $this->merlinProducts[$id]['id'],
                    'merlin_sku' => $this->merlinProducts[$id]['sku'],
                    'profit_id' => $pid
                ];
            }

            if ( strlen($ean) == 12 ) {
                $eanZero = '0' . $ean;
                if (($pid = $this->findEanInProfit($eanZero)) !== null) {
                    $this->similarProducts[] = [
                        'merlin_id' => $this->merlinProducts[$id]['id'],
                        'merlin_sku' => $this->merlinProducts[$id]['sku'],
                        'profit_id' => $pid
                    ];
                }
            }

            $processed++;
        }

        echo "\r\033[K";
    }

    /**
     * Zapis danych do bazy
     */
    private function saveToDatabase() {
        $all = count( $this->similarProducts );
        $processed = 1;
        foreach ( $this->similarProducts as $similarProduct ) {
            echo "\r\033[K" . ( $all - $processed ) . '/' . $all;

            $aValues = [
                'product_id' => $this->profitProducts[$similarProduct['profit_id']]['id'],
                'merlin_id' => $similarProduct['merlin_id'],
                'merlin_sku' => $similarProduct['merlin_sku'],
                'updated_at' => 'NOW()'
            ];

            $this->pDbMgr->Insert('profit24', 'products_merlin_mappings', $aValues);

            $processed++;
        }
        echo "\r\033[K";
    }
}