<?php
namespace LIB\Assets;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 18.04.18
 * Time: 11:57
 */

class sitemapGenerateListing {

    protected $bookstore;

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct(\DatabaseManager $pDbMgr)
    {
        global $aConfig;
        $this->pDbMgr = $pDbMgr;
        $this->bookstore = $aConfig['common']['bookstore_code'];
    }


    public function getItems() {

        $result = [];
        $publishers = $this->getPublishers();
        foreach ($publishers as $key => $publisher) {
            $publishers[$key]['link'] = \getPublisherLink($publisher['name']);
            $result[] = $publishers[$key];
        }

        $authors = $this->getAuthors();
        foreach ($authors as $key => $author) {
            $authors[$key]['link'] = \getAuthorLink($author['name']);
            $result[] = $authors[$key];
        }

        $series = $this->getSeries();
        foreach ($series as $key => $sery) {
            $series[$key]['link'] = \getSeriesLink($sery['publisher_name'], $sery['name']);
            $result[] = $series[$key];
        }

        $categories = $this->getCategories();
        foreach ($categories as $key => $category) {
            $categories[$key]['link'] = '/'.$category['symbol'];
            $result[] = $categories[$key];
        }

        $tags = $this->getTags();
        foreach ($tags as $key => $tag) {
            $tags[$key]['link'] = \getTagLink($tag['tag']);
            $result[] = $tags[$key];
        }
        return $result;
    }

    private function getPublishers() {
        $sSql = '
        SELECT DISTINCT PP.id, PP.name 
        FROM `products_publishers` AS PP 
        JOIN products AS P 
            ON P.publisher_id = PP.id AND P.prod_status = "1" AND P.published = "1"
        ';

        return $this->pDbMgr->GetAll($this->bookstore, $sSql);
    }

    private function getAuthors() {
        $sSql = '
        SELECT DISTINCT PA.id, CONCAT(PA.name,\' \',PA.surname ) AS name
        FROM `products_authors` AS PA
        JOIN products_to_authors AS PTA
          ON PA.id = PTA.author_id
        JOIN products AS P
          ON PTA.product_id = P.id AND P.prod_status = "1" AND P.published = "1"
        ';

        return $this->pDbMgr->GetAll($this->bookstore, $sSql);
    }

    private function getCategories() {
        $sSql = '
        SELECT DISTINCT MI.id, MI.symbol 
        FROM `menus_items` AS MI
        JOIN products_extra_categories AS PEC
          ON MI.id = PEC.page_id
        JOIN products AS P 
            ON P.id = PEC.product_id AND P.prod_status = "1" AND P.published = "1"
        ';

        return $this->pDbMgr->GetAll($this->bookstore, $sSql);
    }

    private function getSeries() {
        $sSql = '
        SELECT DISTINCT PS.id, PS.name, PP.name as publisher_name
        FROM `products_series` AS PS
        JOIN products_publishers AS PP
          ON PS.publisher_id = PP.id
        JOIN products_to_series AS PTS
          ON PS.id = PTS.series_id
        JOIN products AS P 
            ON P.id = PTS.product_id AND P.prod_status = "1" AND P.published = "1"
        ';

        return $this->pDbMgr->GetAll($this->bookstore, $sSql);
    }

    private function getTags() {
        $sSql = '
        SELECT DISTINCT PT.id, PT.tag 
        FROM `products_tags` AS PT
        JOIN products_to_tags AS PTT
          ON PT.id = PTT.tag_id
        JOIN products AS P 
            ON P.id = PTT.product_id AND P.prod_status = "1" AND P.published = "1"
        ';

        return $this->pDbMgr->GetAll($this->bookstore, $sSql);
    }
}