<?php
/**
 * Created by PhpStorm.
 * User: Arek
 * Date: 11.07.2018
 * Time: 15:42
 */

namespace LIB\Assets;


class deliverPDF {

    /**
     *
     * @param array $aTransportNumbers
     * @return string
     */
    private function getHTMLDeliver($aTransportNumbers, $transportName) {
        $sDateFull = date('d.m.Y H:i:s');
        $sHTML ='
      <table style="width:100%; height:100%;">
        <tr>
          <td>Potwierdzenie nadania paczek: '.$transportName.'<br />
          Profit M Spółka z ograniczoną odpowiedzialnością<br />
          Pl. Defilad 1, Metro Centrum, lok.2002D<br />
          00-110 Warszawa 1 <br />
          525-22-45-459 <br />
          </td>
          <td>Warszawa<br />Dnia: '.$sDateFull.'</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td style="font-size: 60px;">Ilość paczek: '.count($aTransportNumbers).'</td>
          <td></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Podpis kuriera</td>
          <td>Podpis nadawcy</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>..............................</td>
          <td>'.$_SESSION['user']['author'].'</TD>
        </TR>
        <TR>
          <TD></TD>
          <TD>..............................</TD>
        </TR>
      </TABLE>
      <BR /><BR /><BR /><BR />
      
';
        $sHTML .= implode(',  ', $aTransportNumbers);
        return $sHTML;
    }

    /**
     *
     * @param array $aTransportNumbers
     * @return string
     */
    public function getPDFDeliver($aTransportNumbers, $transportName) {
        $sDateFull = date('YmdHis');
        $sHTMLDeliver = $this->getHTMLDeliver($aTransportNumbers, $transportName);

        require_once('tcpdf/config/lang/pl.php');
        require_once('tcpdf/tcpdf.php');

        // create new PDF document
        $pdf = new \TCPDF (PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('');
        $pdf->SetTitle(_('Deliver '.$sDateFull));
        $pdf->SetSubject(_('Deliver '.$sDateFull));
        $pdf->SetKeywords('');
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array('freesans', '', PDF_FONT_SIZE_DATA));


        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
        //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 10);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l);

        // set font
        $pdf->SetFont('freesans', '', 8);

        $pdf->AddPage();
        $pdf->writeHTML($sHTMLDeliver, true, false, false, false, '');

        // nazwa pliku - uwzgledniajaca daty
        $sFileName = 'deliver_ruch_'.$sDateFull.'.pdf';
        return $pdf->Output($sFileName, 'S');
    }

}