<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 17.04.18
 * Time: 08:56
 */
namespace LIB\Assets;


class additionalHead {
    CONST ADDITIONAL_HEAD_KEY = 'additional_heads_code';

    /**
     * additionalHead constructor.
     */
    public function __construct()
    {
        global $aConfig;

        $this->aConfig = $aConfig;
    }

    /**
     * @see <link rel="canonical" href="tutaj-należy-wpisać-adres-właściwej-strony" />
     *      <link rel="next" href="http://www.example.com/artykul-czesc2.html">
     *
     * @param $typeLink
     * @param $value
     * @return string
     */
    public function addHeadLinkRel($typeLink, $value) {
        $code = '<link rel="'.$typeLink.'" href="'.$value.'" />'."\n";

        $this->addAdditionalHeadsCode($typeLink, $code);
    }

    /**
     * @param $type
     * @param $sourceCode
     * @throws \Exception
     */
    public function addAdditionalHeadsCode($type, $sourceCode) {
        global $aConfig;

        if (!isset($aConfig[self::ADDITIONAL_HEAD_KEY][$type])) {
            $aConfig[self::ADDITIONAL_HEAD_KEY][$type] = $sourceCode;
        }

    }

    /**
     * @return string
     */
    public function getAdditionalHeadsCode() {
        global $aConfig;

        $headCode = '';
        if (isset($aConfig[self::ADDITIONAL_HEAD_KEY]) && !empty($aConfig[self::ADDITIONAL_HEAD_KEY])) {
            foreach ($aConfig[self::ADDITIONAL_HEAD_KEY] as $headTypeCode) {
                $headCode .= $headTypeCode;
            }
        }
        return $headCode;
    }
}