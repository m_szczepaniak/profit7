<?php

namespace Assets;

use DateTime;

/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-12-03 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class htmlCalendar {

  function __construct() {
    
  }
  
  /**
   * 
   */
  public function generateCalendar(\DateTime $oNow, \DateTime $oShipmentDate, \DateTime $oTransportDate, \DateTime $oTransportDateMax, $sAdditionalMessage) {
    
    $sHTML = '';
    
    $today = $oNow->format("d"); // Current day
    $month = $oNow->format("m"); // Current month
    $year = $oNow->format("Y"); // Current year
    $days = cal_days_in_month(CAL_GREGORIAN,$month,$year); // Days in current month
    
//    $shipment_date = $oShipmentDate->format("d");
//    $shipment_date_month = ($oShipmentDate->format("m") - $oNow->format("m"));
    
    
    $send_date = $oTransportDate->format("d");
    $send_date_month = ($oTransportDate->format("m") - $oNow->format("m"));

    $send_date_max = $oTransportDateMax->format("d");
    $send_date_month_max = ($oTransportDateMax->format("m") - $oNow->format("m"));

    $iNextMonth = $oNow->modify('next month')->format('m');
//    $shipment_date_month_check = ($oShipmentDate->format("m") - $iNextMonth);
    $send_date_month_check = ($oTransportDate->format("m") - $iNextMonth);
    $send_date_month_max_check = ($oTransportDateMax->format("m") - $iNextMonth);

    $lastmonth = date("t", mktime(0,0,0,$month-1,1,$year)); // Days in previous month

    $start = date("N", mktime(0,0,0,$month,1,$year)); // Starting day of current month
    $finish = date("N", mktime(0,0,0,$month,$days,$year)); // Finishing day of  current month
    $laststart = $start - 1; // Days of previous month in calander

    $counter = 1;
    $nextMonthCounter = 1;
    
    if($start > 5){	$rows = 7; }else {$rows = 6; }
    

    for($i = 1; $i <= $rows; $i++){
      $sHTML .= '<tr class="week">';
      for($x = 1; $x <= 7; $x++){
        $aCurrentClass = [];

        if(($counter - $start) < 0){
          $date = (($lastmonth - $laststart) + $counter);
          $aCurrentClass = ['blur'];
        } else if(($counter - $start) >= $days){
          // przyszły miesiąc
          $date = ($nextMonthCounter);
          

          $aCurrentClass = ['blur'];
//          if ($shipment_date == $nextMonthCounter && ($shipment_date_month_check == 0)) {
//            $aCurrentClass = ['shipment'];
//          }
          if ($send_date == $nextMonthCounter && ($send_date_month_check == 0)) {
            $aCurrentClass[] = 'send';
          }
          if ($send_date_max == $nextMonthCounter && ($send_date_month_max_check == 0)) {
              $aCurrentClass[] = 'send';
          }
          $nextMonthCounter++;
        } else {
          // aktualny miesiąc
          $date = ($counter - $start + 1);
          if($today == $counter - $start + 1){
            $aCurrentClass = ['today'];
          }
//          if ($shipment_date >= $date && $shipment_date == $date && ($shipment_date_month == 0)) {
//            $aCurrentClass[] = 'shipment';
//          }
          
          if ($send_date >= $date && $send_date == $date && ($send_date_month == 0)) {
            $aCurrentClass[] = 'send';
          }

          if ($send_date_max >= $date && $send_date_max == $date && ($send_date_month_max == 0)) {
              $aCurrentClass[] = 'send';
          }
        }


        $sHTML .= '<td class="'.implode(' ', $aCurrentClass).'"><span class="date">'. $date . '</span></td>';

        $counter++;
        $class = '';
      }
      $sHTML .= '</tr>';
    }
    return $this->getHTMLContainerCalendar($sHTML, $oNow, $oShipmentDate, $oTransportDate, $sAdditionalMessage);
  }
  
  
  private function getHTMLContainerCalendar($sHTML, \DateTime $oNow, \DateTime $oShipmentDate, \DateTime $oTransportDate, $sAdditionalMessage) {
    
    $sReturnHTML = '
        <div class="shipment_info_calendar_content">
            <div class="info">Informacja o wysyłce:</div>
            <div class="clear"></div>
            <div>

              <table class="calendar">
                  <tr class="header">
                      <td>Pon</td>
                      <td>Wt</td>
                      <td>Sr</td>
                      <td>Czw</td>
                      <td>Pt</td>
                      <td>Sob</td>
                      <td>Nd</td>
                  </tr>
                  '.$sHTML.'
              </table>
              <div>
              '.$sAdditionalMessage.'

              </div>
              
            </div>
        </div>
        <div class="clear"></div>
      ';
    return $sReturnHTML;
  }

}

