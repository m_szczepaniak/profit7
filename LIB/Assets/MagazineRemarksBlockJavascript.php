<?php

namespace Assets;

class MagazineRemarksBlockJavascript
{
    public function getJavascript()
    {
        return '<script type="text/javascript">

        $(window).bind(\'beforeunload\', function(){
            if ($("#transport_number") != undefined && $("#transport_number").val() == "") {
                return \'Czy chcesz opuścić stronę ?\';                
            }
        });
    
        $(function() {
            $("#transport_number").focus();
            $(document).keypress(function(event){

                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == 13){
                    var remarksHiddenValue = $("#remarks_has_been_read").val();

                    if(remarksHiddenValue == 0){

                        $("#accept_text").show();
                        $("#transport_number").focus();
                        return false;
                    }
                    else {
                        $("#transport_number").focus();
                    }
                }

            });

            $("#transport_number").keypress(function (e) {
                if(e.which == 13) {
                    var inputValue = $(this).val();
                    if (inputValue == "ok"){
                        $(this).val("");
                        $("#remarks_has_been_read").val(1);
                        $("#accept_text").hide();
                        $("#transport_number").focus();
                        return false;
                    }
                    else {
                        if ($("#remarks_has_been_read").val() == 0)
                        {
                            $(this).val("");
                        }
                    }
                }
            });
        });
    </script>';
    }

    public function getHiddenInput()
    {
        return '<input style="overflow:hidden;opacity:0 !important; height:0 !important; width:0 !important" id="remarks_scanner" /><div style="display:none;color:red;font-weight: bold" id="accept_text">'._('Przeczytaj i potwierdź uwagi magazynowe !!!/<br>ВНИМАТЕЛЬНО ПРОЧИТАЙ УСЛОВИЯ И ПОЗОВИ ЛИДЕРА!').'</div>';
    }
} 