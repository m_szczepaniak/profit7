<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-11-17 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Assets;

/**
 * Description of htmlHelper
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class htmlHelper {
  
  public static function array_to_table($data,$args=false) {
      if (!is_array($args)) { $args = array(); }
      foreach (array('class','column_widths','custom_headers','format_functions','nowrap_head','nowrap_body','capitalize_headers') as $key) {
        if (array_key_exists($key,$args)) { $$key = $args[$key]; } else { $$key = false; }
      }
      if ($class) { $class = ' class="'.$class.'"'; } else { $class = ''; }
      if (!is_array($column_widths)) { $column_widths = array(); }

        //get rid of headers row, if it exists (headers should exist as keys)
        if (array_key_exists('headers',$data)) { unset($data['headers']); }

      $t = '<table'.$class.'>';
      $i = 0;
      foreach ($data as $row) {
        $i++;
        //display headers
        if ($i == 1) { 
          foreach ($row as $key => $value) {
            if (array_key_exists($key,$column_widths)) { $style = ' style="width:'.$column_widths[$key].'px;"'; } else { $style = ''; }
            $t .= '<col'.$style.' />';
          }
          $t .= '<thead><tr>';
          foreach ($row as $key => $value) {
            if (is_array($custom_headers) && array_key_exists($key,$custom_headers) && ($custom_headers[$key])) { $header = $custom_headers[$key]; }
            elseif ($capitalize_headers) { $header = ucwords($key); }
            else { $header = $key; }
            if ($nowrap_head) { $nowrap = ' nowrap'; } else { $nowrap = ''; }
            $t .= '<th'.$nowrap.'>'.$header.'</th>';
          }
          $t .= '</tr></thead>';
        }

        //display values
        if ($i == 1) { $t .= '<tbody>'; }
        $t .= '<tr>';
        foreach ($row as $key => $value) {
          if (is_array($format_functions) && array_key_exists($key,$format_functions) && ($format_functions[$key])) {
            $function = $format_functions[$key];
            if (!function_exists($function)) { custom_die('Data format function does not exist: '.htmlspecialchars($function)); }
            $value = $function($value);
          }
          if ($nowrap_body) { $nowrap = ' nowrap'; } else { $nowrap = ''; }
          $t .= '<td'.$nowrap.'>'.$value.'</td>';
        }
        $t .= '</tr>';
      }
      $t .= '</tbody>';
      $t .= '</table>';
      return $t;
    }
    
    /**
     * 
     * @param array $data
     * @return array
     */
    public static function multiarray_to_array($data, $aColumnAccept) {
      $newArray = [];
      $i = 1;
      $iOrderSequence = 0;
      foreach ($data as &$items) {
        $iOrderSequence ++;
        foreach ($items as &$item) {
          $item['number'] = $i++;
          $item['order_sequence'] = $iOrderSequence;
          $newItem = [];
          foreach ($aColumnAccept as $columnName) {
            if (!empty($item) && isset($item[$columnName])) {
              $newItem[$columnName] = $item[$columnName];
            }
          }
          
          
          $newArray[] = $newItem;
        }
      }
      return $newArray;
    }
    
    /**
     * 
     * @param string $sHTML
     * @return string HTML
     */
    public static function toggleShowHide($sHTML) {
      $sJS = '<script type="text/javascript">
        function toggleHTML() {
          $("#toggled_element").toggle("fast");
        }
       </script>';
      
      $sNewHTML = '<a href="#" onclick="toggleHTML()">Pokaż/Ukryj oczekujące</a>
       <div id="toggled_element" style="display: none;">
        '.$sHTML.'
       </div>
        ';
      return $sNewHTML.$sJS;
    }
}
