<?php
/**
 * Klasa do zakładania i zwalniania blokady na zamówienia
 *  wykorzystuje memcached oraz sesje
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class ordersSemaphore {
  
  /**
   * @var Memcache oMemcached
   */
  public  $oMemcached;
  
  const  PREFIX_CMS_SEMAPHORE = 'PREFIX_CMS_SEMAPHORE';
  const  EXPIRATION_CMS_SEMAPHORE = 120;
  
  /**
   * Konstruktor
   * 
   * @param type $oMemcached
   */
  function __construct(&$oMemcached) {
    $this->oMemcached = $oMemcached;
  }// end of __construct() method
  
  
  /**
   * Metoda na podstawie id zamówienia, zwraca klucz dla memcached danego zamowienia
   * 
   * @param type $iOId
   * @return type
   */
  private function _getKeyName($iOId) {
    return  self::PREFIX_CMS_SEMAPHORE.'_'.$iOId.'_';
  }// end of _getKeyName09 method
  
  
  /**
   * Metoda blokuje zamówienie i dodaje informacje o tym fakcie do sesji
   * 
   * @param type $iOId
   * @param type $iUId
   * @return boolean
   */
  public function lockOrder($iOId, $iUId) {
    
    $_SESSION['user']['orders_locked'][$this->_getKeyName($iOId)] = '1';
    $this->oMemcached->add($this->_getKeyName($iOId), $iUId, self::EXPIRATION_CMS_SEMAPHORE);
    return true;
  }// end of lockOrder() method
  
  
  /**
   * Metoda odblokowuje wszystkie zamówienia przekazanego użytkownika
   * 
   * @param int $iUId
   * @return boolean
   */
  public function unlockAllOrders($iUId) {
    
    if (isset($_SESSION['user']['orders_locked']) && !empty($_SESSION['user']['orders_locked'])) {
      foreach ($_SESSION['user']['orders_locked'] as $sKeyName => $sValue) {
        unset($_SESSION['user']['orders_locked'][$sKeyName]);
        if ($this->oMemcached->get($sKeyName) == $iUId) {
          $this->oMemcached->delete($sKeyName);
        }
      }
      return true;
    }
  }// end of unlockAllOrders() method
  
  
  /**
   * Metoda odblokowuje pojedyncze zamowienie
   * 
   * @param int $iOId
   * @param int $iUId
   * @return boolean
   */
  public function unlockOrder($iOId, $iUId) {
    
    unset($_SESSION['user']['orders_locked'][$this->_getKeyName($iOId)]);
    if ($this->oMemcached->get($this->_getKeyName($iOId)) == $iUId) {
      $this->oMemcached->delete($this->_getKeyName($iOId));
    }
    return true;
  }// end of unlockOrder
  
  
  /**
   * Metoda sprawdza czy zamówienie jest zablokowane
   * 
   * @param int $iOId
   * @param int $iUId
   * @return mixded id uzytkownika który dodał blokade | FALSE - brak blokady
   */
  public function isLocked($iOId, $iUId) {
    
    // jeśli zalokowany uzytkownik jest inny to jest zalokowane
    $mRet = $this->oMemcached->get($this->_getKeyName($iOId));
    return ($mRet != $iUId && $mRet !== false ? $mRet : FALSE);
  }// end of isLocked() method
}// end of class()
?>