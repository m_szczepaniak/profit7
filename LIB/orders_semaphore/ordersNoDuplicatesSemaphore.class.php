<?php
namespace LIB\orders_semaphore;

class ordersNoDuplicatesSemaphore {

    /**
     * @var Memcache oMemcached
     */
    public  $oMemcached;

    const  PREFIX_CMS_SEMAPHORE = 'PREFIX_NODUPLICATES_CMS_SEMAPHORE';
    const  EXPIRATION_CMS_SEMAPHORE = 62;

    private $key;

    /**
     * Konstruktor
     *
     * @param type $oMemcached
     */
    function __construct(&$oMemcached,$session,$ip) {
        $this->oMemcached = $oMemcached;
        $this->session = $session;
        $this->ip = $ip;
    }// end of __construct() method


    /**
     * Metoda na podstawie ip i sesji , zwraca klucz dla memcached danego zamowienia
     *
     * @param type $iOId
     * @return type
     */
    private function _getKeyName() {

        if (!isset($this->key) && $this->key !== '') {
            $data = [];
            $data['wu_cart']['products'] = $this->session['wu_cart']['products'];
            $data['w_user'] = $this->session['w_user']['id'];

            $this->key = md5($this->ip . serialize($data));
        }
        return  self::PREFIX_CMS_SEMAPHORE.'_'.$this->key.'_';
    }// end of _getKeyName09 method


    /**
     * Metoda blokuje zamówienie i dodaje informacje o tym fakcie do sesji
     *
     * @param type $iOId
     * @param type $iUId
     * @return boolean
     */
    public function lock() {
        $this->oMemcached->add($this->_getKeyName(), 1, self::EXPIRATION_CMS_SEMAPHORE);
        return true;
    }// end of lockOrder() method



    /**
     * Metoda odblokowuje pojedyncze zamowienie
     *
     * @param int $iOId
     * @param int $iUId
     * @return boolean
     */
    public function unlock() {
        $this->oMemcached->delete($this->_getKeyName());
        return true;
    }// end of unlockOrder


    /**
     * Metoda sprawdza czy zamówienie jest zablokowane
     *
     * @param int $iOId
     * @param int $iUId
     * @return mixded id uzytkownika który dodał blokade | FALSE - brak blokady
     */
    public function isLocked() {
        //echo $this->_getKeyName();
        $mRet = $this->oMemcached->get($this->_getKeyName());
        return ($mRet !== false ? true : FALSE);
    }// end of isLocked() method
}// end of class()
?>