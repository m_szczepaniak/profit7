<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 27.04.17
 * Time: 12:43
 */

namespace magazine;


use DatabaseManager;

class BACK_HighLevelStock
{
    private $pDbMgr;

    /**
     * @param DatabaseManager $pDbMgr
     */
    function __construct(DatabaseManager $pDbMgr = null)
    {
        if (null === $pDbMgr) {
            global $pDbMgr;
        }
        $this->pDbMgr = $pDbMgr;
    }

    /**
     * @param $productId
     * @param $orderItemListId
     * @param $deficiencyQuantity
     * @return bool
     * @throws \Exception
     */
    public function addHighLevelDeficiency($productId, $orderItemListId, $deficiencyQuantity)
    {
        if (!isset($this->pDbMgr)) {
            global $pDbMgr;
            $this->pDbMgr = $pDbMgr;
        }
        $collectingHighLevel = $this->getIsActiveBookDeficiency($productId);
        if ($collectingHighLevel > 0) {
            if (false === $this->checkIsInsertedDeficiency($collectingHighLevel, $orderItemListId)) {
                if (false === $this->updateHighLevelActive($collectingHighLevel, $productId, $orderItemListId, $deficiencyQuantity)) {
                    throw new \Exception('Wystąpił błąd podczas aktualizacji ilości na wysokim składowaniu');
                }
            } else {
//                $msg = 'Uwaga nie można było dodać produktu do listy wysokiego składowania ponieważ produkt już tam istnieje: ' . $productId . ' : ' . $collectingHighLevel . ' : ' . $orderItemListId . ' : ' . $deficiencyQuantity;
//                throw new \Exception($msg);
            }
        } else {
            if (false === $this->insertHighLevelActive($productId, $orderItemListId, $deficiencyQuantity)) {
                throw new \Exception('Wystąpił błąd podczas dodawania ilości na wysokim składowaniu');
            }
        }
        return true;
    }

    /**
     * @param $collectingHighLevel
     * @param $orderItemListId
     * @return bool
     */
    private function checkIsInsertedDeficiency($collectingHighLevel, $orderItemListId)
    {
        $sSql = 'SELECT id 
                 FROM collecting_high_level_lists
                 WHERE collecting_high_level_id = ' . $collectingHighLevel . '
                 AND orders_items_lists_id = ' . $orderItemListId;
        if ($this->pDbMgr->GetOne('profit24', $sSql) > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $productId
     * @return mixed
     */
    private function getIsActiveBookDeficiency($productId)
    {
        $sSql = 'SELECT id
                 FROM collecting_high_level
                 WHERE product_id = ' . $productId . '
                 AND active = "1" ';
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }


    /**
     * @param $collectingHighLevel
     * @param $productId
     * @param $orderItemListId
     * @param $deficiencyQuantity
     * @return bool
     */
    private function updateHighLevelActive($collectingHighLevel, $productId, $orderItemListId, $deficiencyQuantity)
    {

        $this->pDbMgr->BeginTransaction('profit24');
        $sSql = 'UPDATE collecting_high_level 
                 SET quantity = quantity + ' . $deficiencyQuantity . ' 
                 WHERE id = ' . $collectingHighLevel . ' AND active = "1" 
                 LIMIT 1';
        if (false === $this->pDbMgr->Query('profit24', $sSql)) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }

        if (false === $this->addCollectingHighLevelLists($collectingHighLevel, $orderItemListId, $deficiencyQuantity)) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }
        $this->pDbMgr->CommitTransaction('profit24');
        return true;
    }


    /**
     * @param $collectingHighLevel
     * @param $orderItemListId
     * @param $deficiencyQuantity
     * @return int
     */
    private function addCollectingHighLevelLists($collectingHighLevel, $orderItemListId, $deficiencyQuantity)
    {

        $values = [
            'collecting_high_level_id' => $collectingHighLevel,
            'orders_items_lists_id' => $orderItemListId,
            'quantity' => $deficiencyQuantity
        ];
        return $this->pDbMgr->Insert('profit24', 'collecting_high_level_lists', $values);
    }


    /**
     * @param $productId
     * @param $orderItemListId
     * @param $deficiencyQuantity
     * @return bool
     */
    private function insertHighLevelActive($productId, $orderItemListId, $deficiencyQuantity)
    {
        $this->pDbMgr->BeginTransaction('profit24');
        $values = [
            'product_id' => $productId,
            'active' => '1',
            'quantity' => $deficiencyQuantity,
            'created' => 'NOW()',
            'created_by' => $_SESSION['user']['name'],
        ];
        $collectingHighLevel = $this->pDbMgr->Insert('profit24', 'collecting_high_level', $values);
        if (intval($collectingHighLevel) <= 0) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }

        if (false === $this->addCollectingHighLevelLists($collectingHighLevel, $orderItemListId, $deficiencyQuantity)) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }
        $this->pDbMgr->CommitTransaction('profit24');
        return true;
    }

    /**
     * @param $productId
     * @return bool
     */
    public function checkProductCanBeCollecting($productId ) {

        $sSql = 'SELECT id 
                 FROM collecting_high_level 
                 WHERE product_id = '.$productId .' 
                 AND completed = "0"
        ';
        if ($this->pDbMgr->GetOne('profit24', $sSql) > 0) {
            return false;
        }
        return true;
    }
}