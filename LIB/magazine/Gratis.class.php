<?php
/**
 * Created by PhpStorm.
 * User: agolba
 * Date: 2016-06-02
 * Time: 12:47
 */

namespace magazine;


class Gratis
{
    private $pDbMgr;

    /**
     * @var bool
     */
    private $bIsSecondGratis;

    /**
     * @param \DatabaseManager $pDbMgr
     * @param bool $bIsSecondGratis
     */
    function __construct(\DatabaseManager $pDbMgr, $bIsSecondGratis = false)
    {
        $this->pDbMgr = $pDbMgr;
        $this->bIsSecondGratis = $bIsSecondGratis;
    }


    public function getGratis($iOrderId)
    {
        $aOrderData = $this->pDbMgr->getTableRow('orders', $iOrderId, [' * ']);

        $aGratis = $this->getAllAvailableGratis($aOrderData['to_pay']);
        foreach ($aGratis as $iKey => $aSingleGratis) {
            if ($aSingleGratis['unique_on_email'] == '1') {
                if ($this->checkEmailUsed($aSingleGratis['id'], $aOrderData['email'], $iOrderId) === true) {
                    unset($aGratis[$iKey]);
                }
            }

            if ($this->checkPublisher($aSingleGratis['id'], $iOrderId) === false) {
                unset($aGratis[$iKey]);
            }

            if ($this->checkCategories($aSingleGratis['id'], $iOrderId) === false) {
                unset($aGratis[$iKey]);
            }

            if ($this->checkWebsites($aSingleGratis['id'], $aOrderData['website_id']) === false) {
                unset($aGratis[$iKey]);
            }
        }


        if (!empty($aGratis)) {
            $aGratis = $this->sortArrayByCountUsages($aGratis, $aOrderData['email']);
            return array_pop($aGratis);
        }
        return false;
    }


    public function addUseGratisOrder($iOrderId, $iGratisId)
    {
        $sSql = 'SELECT id
                 FROM magazine_gratis_usages
                 WHERE order_id = ' . $iOrderId . '
                 AND magazine_gratis_id = "' . $iGratisId.'"';
        $iExists = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($iExists > 0) {
            return true;
        } else {
            $aValues = [
                'order_id' => $iOrderId,
                'magazine_gratis_id' => $iGratisId,
                'created' => 'NOW()',
                'created_by' => $_SESSION['user']['name']
            ];
            if ($this->pDbMgr->Insert('profit24', 'magazine_gratis_usages', $aValues) === false) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $iOrderId
     * @return array
     */
    public function getOrderAddedGratis($iOrderId) {

        $sSql = 'SELECT MG.name, MGU.*
FROM magazine_gratis_usages AS MGU
JOIN magazine_gratis AS MG
  ON MG.id = MGU.magazine_gratis_id
WHERE MGU.order_id = '.$iOrderId;
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * @param $aGratis
     * @param $sEmail
     * @return mixed
     */
    private function sortArrayByCountUsages($aGratis, $sEmail)
    {
        $aNewArrayGratis = [];
        foreach ($aGratis as $iKey => $aSingleGratis) {
            $iCountUsages = $this->getCountOrderGratisUsages($aSingleGratis['id'], $sEmail);
            $aNewArrayGratis[$iCountUsages . '_' . $iKey] = $aSingleGratis;
        }
        krsort($aNewArrayGratis);
        return $aNewArrayGratis;
    }

    /**
     * @param $iGratisId
     * @param $sEmail
     * @return mixed
     */
    private function getCountOrderGratisUsages($iGratisId, $sEmail)
    {
        global $pDB;

        $sSql = 'SELECT count(1)
                 FROM magazine_gratis_usages AS MGU
                 JOIN orders AS O
                 ON
                  MGU.order_id = O.id AND
                  O.email = ' . $pDB->quoteSmart($sEmail) . '
                 WHERE MGU.magazine_gratis_id = ' . $iGratisId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }


    /**
     * @param $iMGId
     * @param $iOrderWebsiteId
     * @return bool
     */
    private function checkWebsites($iMGId, $iOrderWebsiteId)
    {

        $sSql = 'SELECT website_id
                 FROM magazine_gratis_website
                 WHERE magazine_gratis_id = ' . $iMGId;
        $aWebsites = $this->pDbMgr->GetCol('profit24', $sSql);

        if (!empty($aWebsites)) {
            foreach ($aWebsites as $iWebsiteId) {
                if (intval($iWebsiteId) === intval($iOrderWebsiteId)) {
                    return true;
                }
            }
            // nie znaleziono wydawnictwa produktu w zamówieniu z tego wydawnictwa
            return false;
        }

        return true;
    }


    /**
     * @param $iPageId
     * @param $iOrderId
     * @return mixed
     */
    private function checkOrderWebsite($iPageId, $iOrderId)
    {

        $sSql = 'SELECT OI.id
                     FROM orders_items AS OI
                     JOIN products AS P
                      ON P.id = OI.product_id
                      AND P.main_category_id = ' . $iPageId . '
                     WHERE OI.order_id = ' . $iOrderId;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }


    /**
     * @param $iMGId
     * @param $iOrderId
     * @return bool
     */
    private function checkCategories($iMGId, $iOrderId)
    {

        $sSql = 'SELECT page_id
                 FROM magazine_gratis_categories
                 WHERE magazine_gratis_id = ' . $iMGId;
        $aCategories = $this->pDbMgr->GetCol('profit24', $sSql);

        if (!empty($aCategories)) {
            foreach ($aCategories as $iPageId) {
                if ($this->checkOrderProductsCategories($iPageId, $iOrderId) === true) {
                    return true;
                }
            }
            // nie znaleziono wydawnictwa produktu w zamówieniu z tego wydawnictwa
            return false;
        }

        return true;
    }


    /**
     * @param $iPageId
     * @param $iOrderId
     * @return mixed
     */
    private function checkOrderProductsCategories($iPageId, $iOrderId)
    {

        $sSql = 'SELECT OI.id
                     FROM orders_items AS OI
                     JOIN products AS P
                      ON P.id = OI.product_id
                      AND P.main_category_id = ' . $iPageId . '
                     WHERE OI.order_id = ' . $iOrderId;
        return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
    }


    /**
     * @param $iMGId
     * @param $iOrderId
     * @return bool
     */
    private function checkPublisher($iMGId, $iOrderId)
    {

        $sSql = 'SELECT products_publishers_id
                 FROM magazine_gratis_publisher
                 WHERE magazine_gratis_id = ' . $iMGId;
        $aPublishers = $this->pDbMgr->GetCol('profit24', $sSql);

        if (!empty($aPublishers)) {
            foreach ($aPublishers as $iPublisherId) {
                if ($this->checkOrderProductsPublisher($iPublisherId, $iOrderId) === true) {
                    return true;
                }
            }
            // nie znaleziono wydawnictwa produktu w zamówieniu z tego wydawnictwa
            return false;
        }

        return true;
    }

    /**
     * @param $iPublisherId
     * @param $iOrderId
     * @return mixed
     */
    private function checkOrderProductsPublisher($iPublisherId, $iOrderId)
    {

        $sSql = 'SELECT OI.id
                 FROM orders_items AS OI
                 JOIN products AS P
                  ON P.id = OI.product_id
                  AND P.publisher_id = ' . $iPublisherId . '
                 WHERE OI.order_id = ' . $iOrderId;
        return $this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false;
    }


    /**
     * @param $iMGId
     * @param $sEmail
     * @param $iOrderId
     * @return bool
     */
    private function checkEmailUsed($iMGId, $sEmail, $iOrderId)
    {
        global $pDB;

        $sSql = 'SELECT MGU.id
                 FROM magazine_gratis_usages AS MGU
                 JOIN orders AS O
                  ON O.id = MGU.order_id AND O.email = ' . $pDB->quoteSmart($sEmail) . ' AND O.id <> '.$iOrderId.'
                 WHERE MGU.magazine_gratis_id = ' . $iMGId . ' ';
        return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
    }


    /**
     * @param $fOrderValueBrutto
     * @return array
     */
    private function getAllAvailableGratis($fOrderValueBrutto)
    {

        $sSql = 'SELECT * FROM magazine_gratis WHERE minimum_price <= "' . $fOrderValueBrutto . '" AND '.(true == $this->bIsSecondGratis ? ' multiple_addition = "1" ' : ' multiple_addition = "0" ') ;
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * @param $gratisId
     * @return array
     */
    public function getGratisById($gratisId) {

        $sSql = 'SELECT * 
                 FROM magazine_gratis 
                 WHERE id = '.$gratisId;
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }
}