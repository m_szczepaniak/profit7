<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 09.11.17
 * Time: 11:40
 */

namespace magazine;


use DatabaseManager;
use LIB\EntityManager\Entites\Magazine;
use LIB\EntityManager\Entites\SellPredict;
use LIB\EntityManager\Entites\StockLocation;

class PredictCollecting
{

  /**
   * @var DatabaseManager
   */
  private $pDbMgr;

  public function __construct(DatabaseManager $pDbMgr)
  {

    $this->pDbMgr = $pDbMgr;
  }


  /**
   * @param $iLimitRows
   * @param $iDaysPredict
   * @return array
   */
  public function getProductsPredict($iLimitRows, $iDaysPredict, $productId = 0)
  {

    $sSql = '
      SELECT 
          OI.product_id,
          IF(OI.quantity > 50, 3, OI.quantity) AS sum_quantity
      FROM orders_items AS OI
      JOIN orders AS O
        ON OI.order_id = O.id AND O.order_status <> "5"
          AND O.order_date >= DATE_SUB(NOW(), INTERVAL "' . $iDaysPredict . '" DAY)
      JOIN products AS P
        ON P.id = OI.product_id
      JOIN products_publishers AS PP
        ON P.publisher_id = PP.id
      JOIN products_stock AS PS
        ON PS.id = P.id
      JOIN stock_location AS SL
        ON SL.products_stock_id = P.id AND SL.magazine_type = "' . Magazine::TYPE_HIGH_STOCK_SUPPLIES . '"
      WHERE OI.deleted = "0"
            ' . ($productId > 0 ? ' AND OI.product_id = ' . $productId . ' ' : '') . '
            
                    ';
    $data = $this->pDbMgr->GetAll('profit24', $sSql);

    $newData = $this->sumArrayColsGroup($data, 'product_id', ['sum_quantity'], 'orders_count', 'orders_count');
    unset($data);
    $newData = $this->checkProductsShouldBeOrdered($newData);

    $newData = array_slice($newData, 0, $iLimitRows);

    return $newData;
  }

  /**
   * @param $newData
   * @return mixed
   */
  private function checkProductsShouldBeOrdered($newData) {

    $stockLocation = new StockLocation($this->pDbMgr);
    foreach ($newData as &$item) {
      $availableQuantity = $stockLocation->getProductMagazineAvailableQuantity($item['product_id'], Magazine::TYPE_LOW_STOCK_SUPPLIES);
      if ($availableQuantity >= $item['sum_quantity']) {
        // dobra mamy wystarczającą ilość...
        // ciekawe jak tutaj będziemy robić z zapasem
      } else {
        // trzeba domówić
        $requireQuantity = $item['sum_quantity'] - $availableQuantity;
        $item['require_quantity'] = $requireQuantity;
      }
    }
    return $newData;
  }


  /**
   * @param $aData
   * @param $sGroupByCol
   * @param $aSumCols
   * @param string $sortAfterByColumn
   * @return array
   */
  private function sumArrayColsGroup($aData, $sGroupByCol, $aSumCols, $sortAfterByColumn = '', $countGroupedBy = '')
  {

    $newArray = [];
    $sortByColumnArray = [];
    foreach ($aData as $iKey => $aItem) {
      $colGroupByValue = $aItem[$sGroupByCol];
      if (!isset($newArray[$colGroupByValue])) {
        // define
        $newArray[$colGroupByValue] = $aItem;
      } else {
        // only sum Values
        foreach ($aSumCols as $sSumColumn) {
          $newArray[$colGroupByValue][$sSumColumn] += $aItem[$sSumColumn];
        }
      }
      if ($countGroupedBy != '') {
        // zliczenie ile jest zgrupowanych
        $newArray[$colGroupByValue][$countGroupedBy]++;
      }
      if ($sortAfterByColumn != '') {
        // tworzymy nową tabelkę pomocną później przy sortowaniu
        $sortByColumnArray[$colGroupByValue] = $newArray[$colGroupByValue][$sortAfterByColumn];
      }
    }
    if (!empty($sortByColumnArray)) {
      // sortujemy
      array_multisort($sortByColumnArray, SORT_DESC, $newArray);
    }
    return $newArray;
  }


  /**
   * @param $data
   */
  public function addPredictedItems($data)
  {
    $sellPredict = new SellPredict($this->pDbMgr);
    foreach ($data as $item) {
      $row = $this->checkProductPredictedExist($item['product_id']);
      if (!empty($row) ) {
        $sellPredict->updateQuantity($row['id'], $item['require_quantity']);
      } else {
        $sellPredict->insert($item['product_id'], $item['require_quantity'], SellPredict::TYPE_SELL_PREDICT, 'auto_TYPE_SELL_PREDICT');
      }
    }
  }


  /**
   * @param $productId
   * @return array
   */
  private function checkProductPredictedExist($productId) {

    $sSql = '
        SELECT id, quantity 
        FROM sell_predict 
        WHERE product_id = "'.$productId.'"
            AND type = "'.SellPredict::TYPE_SELL_PREDICT.'"
    ';
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }
}