<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 09.11.17
 * Time: 11:42
 */

use magazine\PredictCollecting;

header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/orders';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina -
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';
//error_reporting(E_ERROR || E_WARNING || E_NOTICE);
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia/lang/admin_pl.php');
global $pDbMgr;

$predictCollecting = new PredictCollecting($pDbMgr);
$iDaysPredict = 360;
$iProductLimit = 10;
$data = $predictCollecting->getProductsPredict($iProductLimit, $iDaysPredict);
$predictCollecting->addPredictedItems($data);
dump($data);