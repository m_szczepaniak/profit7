<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 27.04.17
 * Time: 12:43
 */

namespace magazine;


use DatabaseManager;

class HighLevelStock
{
    private $pDbMgr;
    private $destinationMagazineDeficiencies;

  /**
   * @param DatabaseManager $pDbMgr
   * @param null $destinationMagazineDeficiencies
   * @throws \Exception
   */
    function __construct(DatabaseManager $pDbMgr = null, $destinationMagazineDeficiencies = null)
    {
        if (null === $pDbMgr) {
            global $pDbMgr;
        }
        $this->pDbMgr = $pDbMgr;
        if (null === $destinationMagazineDeficiencies) {
          throw new \Exception(_('Nie wybrano magazynu docelowego'));
        }
        $this->destinationMagazineDeficiencies = $destinationMagazineDeficiencies;
    }

    /**
     * @param $productId
     * @param $orderItemListId
     * @param $deficiencyQuantity
     * @param $concatOrderOrderItem
     * @return bool
     * @throws \Exception
     */
    public function addHighLevelDeficiency($productId, $orderItemListId, $deficiencyQuantity, $concatOrderOrderItem)
    {
        if (!isset($this->pDbMgr)) {
            global $pDbMgr;
            $this->pDbMgr = $pDbMgr;
        }
        $collectingHighLevel = $this->getIsActiveBookDeficiency($productId);
        if ($collectingHighLevel > 0) {
            if (false === $this->checkIsInsertedDeficiency($collectingHighLevel, $orderItemListId)) {
                if (false === $this->updateHighLevelActive($collectingHighLevel, $productId, $orderItemListId, $deficiencyQuantity, $concatOrderOrderItem)) {
                    throw new \Exception('Wystąpił błąd podczas aktualizacji ilości na wysokim składowaniu');
                }
            } else {
                // @TODO tutaj wypadało by dodać relację
                if (false === $this->addOrdersItems($collectingHighLevel, $concatOrderOrderItem, $deficiencyQuantity)) {
                    $this->pDbMgr->RollbackTransaction('profit24');
                    return false;
                }


//                $msg = 'Uwaga nie można było dodać produktu do listy wysokiego składowania ponieważ produkt już tam istnieje: ' . $productId . ' : ' . $collectingHighLevel . ' : ' . $orderItemListId . ' : ' . $deficiencyQuantity;
//                throw new \Exception($msg);
            }
        } else {
            if (false === $this->insertHighLevelActive($productId, $orderItemListId, $deficiencyQuantity, $concatOrderOrderItem)) {
                throw new \Exception('Wystąpił błąd podczas dodawania ilości na wysokim składowaniu');
            }
        }
        return true;
    }

    /**
     * @param $collectingHighLevel
     * @param $orderItemListId
     * @return bool
     */
    private function checkIsInsertedDeficiency($collectingHighLevel, $orderItemListId)
    {
        $sSql = 'SELECT id 
                 FROM collecting_high_level_lists
                 WHERE collecting_high_level_id = ' . $collectingHighLevel . '
                 AND orders_items_lists_id = ' . $orderItemListId;
        if ($this->pDbMgr->GetOne('profit24', $sSql) > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $productId
     * @return mixed
     */
    private function getIsActiveBookDeficiency($productId)
    {
        $sSql = 'SELECT id
                 FROM collecting_high_level
                 WHERE product_id = ' . $productId . '
                 AND active = "1" ';
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }


    /**
     * @param $collectingHighLevel
     * @param $productId
     * @param $orderItemListId
     * @param $deficiencyQuantity
     * @return bool
     */
    private function updateHighLevelActive($collectingHighLevel, $productId, $orderItemListId, $deficiencyQuantity, $concatOrderOrderItem)
    {

        $this->pDbMgr->BeginTransaction('profit24');
        $sSql = 'UPDATE collecting_high_level 
                 SET quantity = quantity + ' . $deficiencyQuantity . ' 
                 WHERE id = ' . $collectingHighLevel . ' AND active = "1" 
                 LIMIT 1';
        if (false === $this->pDbMgr->Query('profit24', $sSql)) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }

        if (false === $this->addCollectingHighLevelLists($collectingHighLevel, $orderItemListId, $deficiencyQuantity)) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }

        if (false === $this->addOrdersItems($collectingHighLevel, $concatOrderOrderItem, $deficiencyQuantity)) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }

        $this->pDbMgr->CommitTransaction('profit24');
        return true;
    }


    /**
     * @param $collectingHighLevel
     * @param $orderItemListId
     * @param $deficiencyQuantity
     * @return int
     */
    private function addCollectingHighLevelLists($collectingHighLevel, $orderItemListId, $deficiencyQuantity)
    {

        $values = [
            'collecting_high_level_id' => $collectingHighLevel,
            'orders_items_lists_id' => $orderItemListId,
            'quantity' => $deficiencyQuantity
        ];
        return $this->pDbMgr->Insert('profit24', 'collecting_high_level_lists', $values);
    }


    /**
     * @param $productId
     * @param $orderItemListId
     * @param $deficiencyQuantity
     * @param $concatOrderOrderItem
     * @return bool
     */
    private function insertHighLevelActive($productId, $orderItemListId, $deficiencyQuantity, $concatOrderOrderItem)
    {
      if (!isset($this->destinationMagazineDeficiencies)) {
        throw new \Exception(_('Brak wybranego magazynu do wskazania braków na składowaniu'));
      }
        $this->pDbMgr->BeginTransaction('profit24');
        $values = [
            'product_id' => $productId,
            'active' => '1',
            'destination_magazine' => $this->destinationMagazineDeficiencies,
            'quantity' => $deficiencyQuantity,
            'created' => 'NOW()',
            'created_by' => $_SESSION['user']['name'],
        ];
        $collectingHighLevel = $this->pDbMgr->Insert('profit24', 'collecting_high_level', $values);
        if (intval($collectingHighLevel) <= 0) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }

        if (false === $this->addOrdersItems($collectingHighLevel, $concatOrderOrderItem, $deficiencyQuantity)) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }

        if (false === $this->addCollectingHighLevelLists($collectingHighLevel, $orderItemListId, $deficiencyQuantity)) {
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }
        $this->pDbMgr->CommitTransaction('profit24');
        return true;
    }

  /**
   * @param $productId
   * @return bool
   * @throws \Exception
   */
    public function checkProductCanBeCollecting($productId)
    {
        $sSql = 'SELECT destination_magazine
                 FROM collecting_high_level 
                 WHERE product_id = ' . $productId . ' 
                 AND completed = "0"
        ';
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * @param $iId
     * @param $concatOrderOrderItem
     * @param $quantity
     */
    private function addOrdersItems($iId, $concatOrderOrderItem, $quantity)
    {

        //        CONCAT(orders_id, ",", orders_items_id) = "'.$sIdent.'"
        $data = explode(',', $concatOrderOrderItem);


        if (false === $this->checkOrderItemExists($iId, $data[0], $data[1])) {
            $values = [
                'collecting_high_level_id' => $iId,
                'order_id' => $data[0],
                'orders_items_id' => $data[1],
                'quantity' => $quantity,
                'created' => 'NOW()'
            ];
            $this->pDbMgr->Insert('profit24', 'collecting_high_level_orders_items', $values);
        }
    }


    /**
     * @param $iId
     * @param $orderId
     * @param $orderItemId
     * @return bool
     */
    private function checkOrderItemExists($iId, $orderId, $orderItemId)
    {
        $sSql = 'SELECT id 
                 FROM collecting_high_level_orders_items
                 WHERE 
                  collecting_high_level_id = ' . $iId . '
                  AND order_id = ' . $orderId . '
                  AND orders_items_id = ' . $orderItemId . '
                 LIMIT 1
                ';
        return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
    }

    /**
     * @param $iCollectingHighLevelId
     * @return mixed
     */
    public function markCollecting($iCollectingHighLevelId)
    {
        $values = [
            'collecting' => '1',
            'collecting_datetime' => 'NOW()'
        ];

        return $this->pDbMgr->Update('profit24', 'collecting_high_level', $values, ' id = ' . $iCollectingHighLevelId);
    }

    /**
     * @param $iCollectingHighLevelId
     * @return mixed
     */
    public function unmarkCollecting($iCollectingHighLevelId)
    {
        $values = [
            'collecting' => '0',
        ];

        return $this->pDbMgr->Update('profit24', 'collecting_high_level', $values, ' id = ' . $iCollectingHighLevelId);
    }

    /**
     * @param $iCollectingHighLevelId
     * @return mixed
     */
    public function deactivateHighLevelStock($iCollectingHighLevelId)
    {
        $values = [
            'active' => '0'
        ];

        return $this->pDbMgr->Update('profit24', 'collecting_high_level', $values, ' id = ' . $iCollectingHighLevelId);
    }

    /**
     * @param $product_id
     * @param $diffQuantity
     * @return mixed
     */
    public function getProductQuantityToLowStock($product_id, $diffQuantity)
    {
        $iDaysSupplyStock = 3;

        $stockQuantity = $this->getStockQuantity($product_id);
        $OrdersForProductData = $this->getOrdersForProduct($product_id, $iDaysSupplyStock);
        if ($OrdersForProductData['O_COUNT'] == 0) {
            $OrdersForProductData['O_COUNT'] = 1;
        }

        $avgQuantity = $OrdersForProductData['O_COUNT'] / $iDaysSupplyStock;
        if ($OrdersForProductData['OI_QUANTITY'] <= 0) {
            $OrdersForProductData['OI_QUANTITY'] = 1;
        }

        $modifier = $avgQuantity / $OrdersForProductData['OI_QUANTITY'];
        $proposedQuantity = ceil(($modifier * $OrdersForProductData['OI_QUANTITY']) * $iDaysSupplyStock);


        if ($proposedQuantity < $diffQuantity) {
            // weźmy przynajmniej tyle ile trzeba do zamówienia
            if ($stockQuantity < $diffQuantity) {
                // na stocku jest mniej niż potrzebujemy
                return $stockQuantity;
            } else {
                return $diffQuantity;
            }
        } else {
            // ok weźmy więcej, tyle ile sugerują ostatnie zamówienia
            if ($stockQuantity < $diffQuantity) {
                // na stocku jest mniej niż potrzebujemy
                return $stockQuantity;
            } else {
                return $diffQuantity;
            }
        }
    }

    /**
     * @param $product_id
     * @param $iDaysSupplyStock
     * @return array
     */
    private function getOrdersForProduct($product_id, $iDaysSupplyStock)
    {

        $sSql = '
                 SELECT count(O.id) AS O_COUNT, SUM(OI.quantity) AS OI_QUANTITY
                 FROM orders AS O
                 JOIN orders_items AS OI
                  ON OI.order_id = O.id AND OI.product_id = ' . $product_id . ' AND OI.deleted = "0"
                 WHERE O.status_1_update > DATE_SUB(CURDATE(), INTERVAL ' . $iDaysSupplyStock . ' DAY) AND O.order_status <> "5"
                  ';
        return $this->pDbMgr->GetRow('profit24', $sSql);
    }

    /**
     * @param $product_id
     * @return mixed
     */
    private function getStockQuantity($product_id)
    {

        $sSql = 'SELECT PS.profit_g_act_stock
                 FROM products_stock AS PS
                 WHERE PS.id = ' . $product_id;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

  /**
   * @param $productId
   * @return mixed
   * @throws \Exception
   */
    public function getProductCollectedId($productId) {
        if (!isset($this->destinationMagazineDeficiencies)) {
          throw new \Exception(_('Brak wybranego magazynu do wskazania braków na składowaniu'));
        }
        $sSql = '
                SELECT id 
                FROM collecting_high_level
                WHERE product_id = '.$productId.'
                AND active = "0"
                AND completed = "0"
                AND destination_magazine = "'.$this->destinationMagazineDeficiencies.'"
                ';
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * @param $collectedProductId
     * @return mixed
     */
    public function markProductCompleted($collectedProductId)
    {
        $value = [
            'completed' => '1'
        ];
        return $this->pDbMgr->Update('profit24', 'collecting_high_level', $value, ' id = '.$collectedProductId);
    }


  /**
   * @param $orderId
   */
  public function cancelOrderReservations($orderId)
  {
    $sSql = 'SELECT id, collecting_high_level_id 
                 FROM collecting_high_level_orders_items
                 WHERE 
                    order_id = ' . $orderId . '
                 LIMIT 1
                ';
    $cancelLists = $this->pDbMgr->GetAll('profit24', $sSql);
    foreach ($cancelLists as $row) {
      $itemsActiveCollecting = $this->checkListRowOtherRows($row['collecting_high_level_id']);
      if ($itemsActiveCollecting == 1) {
        // deaktywujemy
        $this->markProductCompleted($row['collecting_high_level_id']);
        $this->deactivateHighLevelStock($row['collecting_high_level_id']);
      } else {
        // usuwamy tylko nasze powiązanie ze zbieraniem
        $this->removeCollectingHighLevelOrdersItems($row['id']);
      }
    }
  }


  /**
   * @param $orderId
   */
  public function cancelOrderItemReservations($orderItemId)
  {
    $sSql = 'SELECT id, collecting_high_level_id 
                 FROM collecting_high_level_orders_items
                 WHERE 
                    orders_items_id = ' . $orderItemId . '
                 LIMIT 1
                ';
    $cancelLists = $this->pDbMgr->GetAll('profit24', $sSql);
    foreach ($cancelLists as $row) {
      $itemsActiveCollecting = $this->checkListRowOtherRows($row['collecting_high_level_id']);
      if ($itemsActiveCollecting == 1) {
        // deaktywujemy
        $this->markProductCompleted($row['collecting_high_level_id']);
        $this->deactivateHighLevelStock($row['collecting_high_level_id']);
      } else {
        // usuwamy tylko nasze powiązanie ze zbieraniem
        $this->removeCollectingHighLevelOrdersItems($row['id']);
      }
    }
  }

  /**
   * @param $item
   * @return int
   */
  private function removeCollectingHighLevelOrdersItems($item) {

    $sSql = 'DELETE FROM collecting_high_level_orders_items WHERE id = '.$item;
    return $this->pDbMgr->Query('profit24', $sSql);
  }

  /**
   * @param $item
   * @return mixed
   */
  private function checkListRowOtherRows($item) {

    $sSql = 'SELECT count(1)
                 FROM collecting_high_level_orders_items
                 WHERE 
                    collecting_high_level_id = ' . $item . '
                 LIMIT 1';
    return $this->pDbMgr->GetOne('profit24', $sSql);
  }
}