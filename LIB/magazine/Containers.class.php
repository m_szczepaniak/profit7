<?php
/**
 * Klasa odpowiedzialna za obsługę kontenerów aplikacji
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-10-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace magazine;


use LIB\EntityManager\Entites\Containers as ContainersEntity;

class Containers {
  private $pDbMgr;
  
  /**
   *
   * @var char Typ 
   *  0 - zwykłe kuwety
   *  1 - Tramwaj
   *  2 - Magazyn
   */
  private $mType;

  public static $TYPE_MAGAZINE_LOCATION = [ContainersEntity::TYPE_STOCK_A3, ContainersEntity::TYPE_STOCK_B6];

  function __construct($pDbMgr, $mType = 0) {
    $this->pDbMgr = $pDbMgr;
    $this->mType = $mType;
  }


  /**
   * Metoda blokuje kontener
   * 
   * @global type $pDbMgr
   * @param type $sPackNumber
   * @return mixed - zwraca bool|int
   *  bool false - jesli nie powiodła się aktualizacja blokowania
   *  int:
   *   1 - ten kontener jest odblokowany
   *  -1 - kontener zablokowany
   *  -2 - brak takiego kontenera
   */
  public function lockContainer($sPackNumber) {
    
    $iReturn = $this->checkContainer($sPackNumber);
    if ($iReturn === 1) {
      $aValues = array(
          'locked' => '1'
      );
      return ($this->pDbMgr->Update('profit24', 'containers', $aValues, ' id = '.$sPackNumber) === FALSE ? FALSE : TRUE);
    } else {
      return $iReturn;
    }
  }// end of lockContainer() method
  
  
  /**
   * Metoda blokuje kontener
   * 
   * @param string $sPackNumber
   * @return bool
   */
  public function unlockContainer($sPackNumber) { 
    
    $aValues = array(
        'locked' => '0'
    );
    return ($this->pDbMgr->Update('profit24', 'containers', $aValues, ' id = '.$sPackNumber) === FALSE ? FALSE : TRUE);
  }// end of unlockContainer() method


    /**
     * Metoda blokuje kontener
     *
     * @param string $sPackNumber
     * @return bool
     */
    public function clearOrderContainer($sPackNumber) {

        $aValues = array(
            'item_id' => 'NULL'
        );
        return ($this->pDbMgr->Update('profit24', 'containers', $aValues, ' id = '.$sPackNumber) === FALSE ? FALSE : TRUE);
    }// end of unlockContainer() method
  
  
  /**
   * Metoda sprawdza status kontenera
   * 
   * @param string $sPackNumber
   * @return int - zwraca
   *  1 - ten kontener jest odblokowany
   *  -1 - kontener zablokowany
   *  -2 - brak takiego kontenera
   */
  public function checkContainer($sPackNumber) {
    $bIsMultipleTypes = is_array($this->mType) ? true : false;
    
    $sSql = 'SELECT C.*
             FROM containers AS C
             WHERE C.id = '.$sPackNumber;
    if (true === $bIsMultipleTypes) {
       $sSql .= ' AND type IN ("'.implode('", "', $this->mType).'") ';
    } else {
       $sSql .= ' AND type = "'.$this->mType.'" ';
    }
    $aContaner = $this->pDbMgr->GetRow('profit24', $sSql);
    if (!empty($aContaner) && isset($aContaner['locked']) && $aContaner['locked'] == '0') {
      if (true === $bIsMultipleTypes) {
        return $aContaner['type'];
      }
      return 1;
    } elseif (empty($aContaner)) {
      return -2; // brak takiego kontenera
    } else {
      return -1; // kontener zablokowany
    }
  }// end of checkContainer() method
  
  /**
   * 
   * @param type $iProductId
   * @param type $sLocation
   * @return boolean|-2
   */
  public function updateProductLocation($iProductId, $sLocation) {
    
    $cContainerType = $this->checkContainer($sLocation);
    if ($cContainerType === -2) {
      return $cContainerType;
    }
    
    $column = $this->getProductsStockColByContainerType($cContainerType);

    $aValues = array(
        $column => $sLocation
    );
    if ($this->pDbMgr->Update('profit24', 'products_stock', $aValues, ' id = '.$iProductId.' LIMIT 1') === false) {
      return false;
    }
    return true;
  }

  /**
   * @param $productsIds
   * @param $sourceLocationType
   * @return
   */
  public function getProductsTypeLocations($productsIds, $sourceLocationType) {

    $colName = $this->getProductsStockColByContainerType($sourceLocationType);
    $sSql = 'SELECT PS.'.$colName.' as location, PS.id, P.name, P.ean_13
             FROM products_stock AS PS
             JOIN products AS P
              ON P.id = PS.id
             WHERE PS.id IN ('.$productsIds.')';
    return $this->pDbMgr->GetAll('profit24', $sSql);
  }

  
  /**
   * 
   * @param int $type
   * @return string
   */
  private function getProductsStockColByContainerType($type) {
    
    switch ($type) {
      case '2':
        return 'profit_g_location';
      case '3':
        return 'profit_x_location';
    }
  }
}// end of Class
