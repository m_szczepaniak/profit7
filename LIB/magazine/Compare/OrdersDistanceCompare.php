<?php
namespace magazine\Compare;

use DatabaseManager;

/**
 * Created by PhpStorm.
 * User: Arek
 * Date: 19.10.2018
 * Time: 09:41
 */
class OrdersDistanceCompare {

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    /**
     * @var int[]
     */
    private $lanes;

    private $ordersLocations = [];

    private $simpleOrdersLocations = [];

    public function __construct(DatabaseManager $pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
    }


    /**
     * @param $resultComparedOrdersItems
     * @param $ordersItems
     * @return mixed
     */
    public function sortOutOrderItems($resultComparedOrdersItems, $ordersItems) {
        if (empty($resultComparedOrdersItems)) {
            // coś poszło nie tak nie ma wszystkich zamówień przesortowanych
            return $ordersItems;
        }
        if (count($ordersItems) <> count($resultComparedOrdersItems)) {
            // coś poszło nie tak nie ma wszystkich zamówień przesortowanych
            return $ordersItems;
        }

        $sortedComparedOrdersItems = [];
        foreach ($resultComparedOrdersItems as $orderId => $orderBy) {
            $sortedComparedOrdersItems[$orderId] = $ordersItems[$orderId];
        }
        return $sortedComparedOrdersItems;
    }


    /**
     * @param $ordersItems
     * @return array
     */
    public function compare($ordersItems) {
        $comparedOrderItems = [];

        foreach ($ordersItems as $iFirstKey => $firstOrderItems) {
            foreach ($ordersItems as $iSecondKey => $secondOrderItems) {
                if ($iFirstKey <> $iSecondKey) {
                    if (
                        false == isset($comparedOrderItems[$iFirstKey.'_'.$iSecondKey]) &&
                        false == isset($comparedOrderItems[$iSecondKey.'_'.$iFirstKey])
                    ) {
                        $comparedOrderItems[$iFirstKey.'_'.$iSecondKey] = $this->compareOrders($firstOrderItems, $secondOrderItems);
                    }
                }
            }
        }

        asort($comparedOrderItems);

        $mostPopularLane = $this->getMostPopularLane();
        $ordersLocationsSorterByPopularLane = $this->sortOrdersLocations($mostPopularLane);

        $resultOrderByOrders = $this->megaComparator($comparedOrderItems, $ordersLocationsSorterByPopularLane, $ordersItems);


        return $resultOrderByOrders;
    }


    /**
     * @param $comparedOrderItems
     * @param $ordersLocationsSorterByPopularLane
     * @param $ordersItems
     * @return array
     */
    private function megaComparator($comparedOrderItems, $ordersLocationsSorterByPopularLane, $ordersItems) {

        $resultOrderByOrders = [];
        foreach ($ordersLocationsSorterByPopularLane as $orderId => $popularLaneRow) {
            // musimy pobrać datę
            $modifiedOrderBy = $this->getOrderByOrderItemModifier($ordersItems[$orderId]);


            if (!in_array($orderId, $resultOrderByOrders)) {
                $popular = sprintf("%1$011d", $popularLaneRow);
                $resultOrderByOrders[$orderId] = $modifiedOrderBy.$popular;
            }
            foreach ($comparedOrderItems as $ordersNumbers => $diffLocations) {
                $orderNumbersArray = explode('_', $ordersNumbers);
                if ($orderNumbersArray[0] == $orderId) {
                    if (!in_array($orderNumbersArray[1], $resultOrderByOrders)) {
                        $popular = sprintf("%1$011d", $diffLocations);
                        $resultOrderByOrders[$orderNumbersArray[1]] = $modifiedOrderBy.$popular;
                        unset($comparedOrderItems[$ordersNumbers]);
                    }
                }
                if ($orderNumbersArray[1] == $orderId) {
                    if (!in_array($orderNumbersArray[0], $resultOrderByOrders)) {
                        $popular = sprintf("%1$011d", $diffLocations);
                        $resultOrderByOrders[$orderNumbersArray[0]] = $modifiedOrderBy.$popular;
                        unset($comparedOrderItems[$ordersNumbers]);
                    }
                }
            }
        }

        asort($resultOrderByOrders);
        return $resultOrderByOrders;
    }

    /**
     * @return int
     */
    private function getMostPopularLane() {

        if (empty($this->lanes)) {
            return 00000;
        }
        asort($this->lanes);
        end($this->lanes);
        $key = key($this->lanes);

        return $key;
    }

    /**
     * @param $firstOrderItems
     * @param $secondOrderItems
     * @return array
     */
    public function compareOrders($firstOrderItems, $secondOrderItems) {

        $diffInt = 0;
        foreach ($firstOrderItems as $firstItemId => $firstOrderItem) {
            foreach ($secondOrderItems as $secondItemId => $secondOrderItem) {
                $diffInt += $this->compareDistanceOrdersItems($firstOrderItem, $secondOrderItem, $firstItemId, $secondItemId);
            }
        }
        return $diffInt;
    }


    /**
     * @param $firstOrderItem
     * @param $secondOrderItem
     * @return number
     */
    private function compareDistanceOrdersItems($firstOrderItem, $secondOrderItem, $firstItemId, $secondItemId) {

        $firstLocations = $this->getLocations($firstOrderItem);
        $secondLocations = $this->getLocations($secondOrderItem);
        $this->addCalculationLocations($firstLocations, $firstOrderItem['order_id'], $firstItemId);
        $this->addCalculationLocations($secondLocations, $secondOrderItem['order_id'], $secondItemId);

        $diff = 0;
        foreach ($firstLocations as $first) {
            foreach ($secondLocations as $second) {
                $firstPackNumber = intval($first);
                $secondPackNumber = intval($second);
                $diff += abs($firstPackNumber - $secondPackNumber);
            }
        }

        return $diff;
    }


    /**
     * @param $firstLocations
     * @param $orderId
     * @param $itemId
     */
    private function addCalculationLocations($firstLocations, $orderId, $itemId) {
        if (!isset($this->ordersLocations[$orderId][$itemId])) {
            foreach ($firstLocations as $location) {
                $this->ordersLocations[$orderId][$itemId][] = $location;
                $this->simpleOrdersLocations[$orderId][] = $location;
                if ('' != $location) {
                    $lane = $this->getLineFromLocation($location);
                    if ($lane) {
                        $this->lanes[$lane]++;
                    }
                }
            }
            if (!empty($this->ordersLocations[$orderId][$itemId])) {
                asort($this->simpleOrdersLocations[$orderId]);
                asort($this->ordersLocations[$orderId][$itemId]);
            }
        }
    }

    /**
     * @param $location
     * @return int
     */
    private function getLineFromLocation($location) {
        preg_match("/^(\d{2}\d{3})/", $location, $matches);
        if (isset($matches[1])) {
            return $matches[1];
        }
        return 0;
    }

    /**
     * @param $orderItem
     * @return array
     */
    private function getLocations($orderItem) {
        $locations = [];

        if (!empty($orderItem['magazine_stock_localizations_reserved']) && is_array($orderItem['magazine_stock_localizations_reserved'])) {
            foreach ($orderItem['magazine_stock_localizations_reserved'] as $item) {
                $locations[] = $item['container_id'];
            }
        } else {
            $locations = explode(" ", $orderItem['pack_number']);
        }

        return $locations;
    }

    /**
     * @param $mostPopularLane
     * @return array
     */
    private function sortOrdersLocations($mostPopularLane)
    {
        $diffOrderLane = [];

        $virtualMostPopularLocationMin = $mostPopularLane . '00000';
        $virtualMostPopularLocationMax = $mostPopularLane . '99999';
        foreach ($this->simpleOrdersLocations as $orderId => $orderLanes) {
            $diffLane = 0;
            foreach ($orderLanes as $orderLane) {
                if ($orderLane > $virtualMostPopularLocationMax) {
                    $diffLane += abs($virtualMostPopularLocationMax - $orderLane);
                } else {
                    $diffLane += abs($virtualMostPopularLocationMin - $orderLane);
                }

            }
            $diffOrderLane[$orderId] = $diffLane;
        }

        asort($diffOrderLane);
        return $diffOrderLane;
    }

    /**
     * @param $orderId
     * @return string
     */
    private function getOrderByOrderItemModifier($orderId)
    {
        $modifiedOrderBy = '1'.date('Ymd');
        if (isset($ordersItems[$orderId][0])) {
            $matches = [];
            preg_match('/(\d{4})-(\d{2})-(\d{2})/', $ordersItems[$orderId][0]['shipment_date_expected'], $matches);
            // vip jest odwracany ponieważ sortowanie od najmniejszej ilości, najwcześniejsza data
            $modifiedOrderBy = ($ordersItems[$orderId][0]['magazine_get_ready_list_VIP'] == '1' ? '0' : '1').$matches[1].$matches[2].$matches[3];
        }
        return $modifiedOrderBy;
    }
}