<?php
namespace LIB\orders_semaphore;

/**
 * Klasa do zakładania i zwalniania blokady na zamówienia
 *  wykorzystuje memcached oraz sesje
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-10 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class compareMemcached {
  
  /**
   * @var Memcache oMemcached
   */
  public  $oMemcached;
  
  const  PREFIX_CMS_SEMAPHORE = 'COMPARE_ORDERS_COMPLETATION';
  const  EXPIRATION_CMS_SEMAPHORE = 259200;

    /**
     * compareMemcached constructor.
     * @param $oMemcached
     */
  function __construct(&$oMemcached) {
    $this->oMemcached = $oMemcached;
  }// end of __construct() method


    /**
     * @param $magazine
     * @return string
     */
  private function _getKeyName($magazine) {
    return  self::PREFIX_CMS_SEMAPHORE.'_'.$magazine.'_';
  }// end of _getKeyName09 method


    /**
     * @param $key
     * @param $value
     * @return bool
     */
  public function set($key, $value) {

    $this->oMemcached->add($this->_getKeyName($key), $value, self::EXPIRATION_CMS_SEMAPHORE);
    return true;
  }// end of lockOrder() method


    /**
     * @param $magazine
     * @return bool
     */
  public function delete($magazine) {

    $this->oMemcached->delete($this->_getKeyName($magazine));
    return true;
  }// end of unlockOrder

    /**
     * @param $key
     * @return bool
     */
  public function get($key) {
    
    // jeśli zalokowany uzytkownik jest inny to jest zalokowane
    $mRet = $this->oMemcached->get($this->_getKeyName($key));
    return ($mRet !== false ? true : FALSE);
  }// end of get() method
}// end of class()
?>