<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 08.10.18
 * Time: 10:19
 */
namespace magazine\FastTrack;

use DatabaseManager;

class dataAnalyzer
{

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * dataAnalyzer constructor.
     * @param DatabaseManager $pDbMgr
     */
    public function __construct(DatabaseManager $pDbMgr)
    {

        $this->pDbMgr = $pDbMgr;
    }


    /**
     * @param $iOSHId
     * @return array
     */
    public function analyzeOrders($iOSHId)
    {
        // w zamówieniu ma być wyłącznie 1 szt. do zbierania
        // zamówienie ma umożliwiać wysyłkę - czyli ma być np. opłacone
        // nie będzie na odroczony termin wysyłki
        $ordersSingleListItems = [];

        $ordersIds = $this->getOrdersList($iOSHId);

        if (!empty($ordersIds)) {
            $ordersSingleListItems = $this->getOrdersSingleListItems($ordersIds);
        }


        // dobra mamy listę
        return $ordersSingleListItems;
    }


    /**
     * @param $ordersIds
     * @return array
     */
    private function getOrdersSingleListItems($ordersIds) {
//     warunek który usunąłem żeby można pomimo przerwania pracy móc obsługiwać drugi raz raz zeskanowane single   AND O.print_ready_list = "0"
        $sSql = '
        SELECT O.order_number, OI.id, SUM(OI.quantity) AS items_quantity
        
        FROM orders_items AS OI
        JOIN orders AS O 
          ON OI.order_id = O.id
        LEFT JOIN orders_linked_transport AS OLT
          ON O.id = OLT.linked_order_id
        WHERE
            OI.deleted = "0"
            AND OLT.id IS NULL
            AND O.id IN (' . implode(', ', $ordersIds) . ')
            
            
            AND ( O.send_date = "0000-00-00" OR (O.send_date <> "0000-00-00" AND O.send_date < NOW()) )
             
            
            AND (OI.item_type = "I" OR OI.item_type = "P")
            AND OI.packet = "0"

              AND 
              (
                O.order_status = "1" OR
                O.order_status = "2" 
              )
            
            AND 
              (
                (
                  (
                    O.payment_type = "bank_transfer" OR
                    O.payment_type = "platnoscipl" OR
                    O.payment_type = "card"
                  )
                  AND O.to_pay <= O.paid_amount
                )
                OR
                (
                  O.payment_type = "postal_fee" OR
                  O.payment_type = "bank_14days"
                )
                OR
                (
                  O.second_payment_type = "postal_fee" OR
                  O.second_payment_type = "bank_14days"
                )
              )
            
        GROUP BY OI.order_id
        HAVING items_quantity = 1
        ';

        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @param $iOSHId
     * @return array
     */
    private function getOrdersList($iOSHId) {

        $sSql = 'SELECT O.id
                 FROM orders AS O
                 JOIN orders_items AS OI
                  ON O.id = OI.order_id
                 JOIN orders_send_history_items AS OSHI
                  ON OSHI.item_id = OI.id
                 WHERE OSHI.send_history_id = ' . $iOSHId . '
                 ';
        return $this->pDbMgr->GetCol('profit24', $sSql);
    }
}