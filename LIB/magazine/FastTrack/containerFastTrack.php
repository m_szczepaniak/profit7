<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 08.10.18
 * Time: 13:07
 */

namespace magazine\FastTrack;

use Common_get_ready_orders_books;
use DatabaseManager;
use \Exception;
use magazine\Containers;
use Memcached;
use ordersSemaphore;
use stdClass;

class containerFastTrack {

    private $userId;

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    private $fastTrack;


    public function __construct(DatabaseManager $pDbMgr, $userId)
    {
        $this->pDbMgr = $pDbMgr;
        $this->userId = $userId;
    }


    /**
     * @return array|bool
     */
    public function useFastTrack(){

        $sSql = 'SELECT * FROM fast_track WHERE user_id = '.$this->userId.' AND closed = "0" LIMIT 1';
        $result = $this->pDbMgr->GetRow('profit24', $sSql);
        if (!empty($result) && $result['container_id'] != '') {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @param $fastTrackId
     * @param $userId
     * @return array|bool
     */
    public function useFastTrackById($fastTrackId, $userId){

        $sSql = 'SELECT * FROM fast_track 
                 WHERE user_id = '.$userId.'
                  AND id = '.$fastTrackId.'
                 AND closed = "0" 
                 LIMIT 1';
        $result = $this->pDbMgr->GetRow('profit24', $sSql);
        if (!empty($result) && $result['container_id'] != '') {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * @param $fastTrackId
     * @return array
     */
    public function getFastTruckItems($fastTrackId) {
        $sSql = '
        SELECT FTI.container_id, OSH.number, OI.name, OI.isbn
        FROM fast_track_items AS FTI
        JOIN orders_send_history_items AS OSHI
          ON FTI.orders_send_history_items_id = OSHI.id
        JOIN orders_send_history AS OSH
          ON OSH.id = OSHI.send_history_id
        JOIN orders_items AS OI
          ON OSHI.item_id = OI.id
        WHERE FTI.fast_track_id = '.$fastTrackId.'
        ';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }


    /**
     * @param $fastTrackId
     * @return mixed
     */
    public function getProductQuantityFastTrack($fastTrackId) {
        $sSql = 'SELECT SUM(OI.quantity)
                 FROM fast_track_items AS FTI
                 JOIN orders_send_history_items AS OSHI
                  ON OSHI.id = FTI.orders_send_history_items_id
                 JOIN orders_items AS OI
                  ON OI.id = OSHI.item_id 
                 WHERE FTI.fast_track_id = '. $fastTrackId ;
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }

    /**
     * @param $containerId
     * @return bool
     * @throws Exception
     */
    public function openUserFastTrack($containerId) {

        $sSql = 'SELECT id 
                 FROM fast_track 
                 WHERE user_id = '.$this->userId.' 
                   AND closed = "0" ';
        $fastTrackId = $this->pDbMgr->GetOne('profit24', $sSql);
        if ($fastTrackId > 0) {
            return false;
        } else {
            $values = [
                'container_id' => $containerId,
                'user_id' => $this->userId,
                'open_datetime' => 'NOW()',
                'closed' => '0'
            ];
            if (false === $this->pDbMgr->Insert('profit24', 'fast_track', $values)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $fastTrackOrdersItemsScanned
     * @return bool
     * @throws Exception
     */
    public function addOrderSendHistoryItemToFastTrack($fastTrackOrdersItemsScanned) {
        $fastTrackData = $this->useFastTrack();
        if (false === $fastTrackData){
            throw new Exception('Użytkownik nie ma ustawionego FastTracka');
        }

        foreach ($fastTrackOrdersItemsScanned as $fastTrackOrdersItemsCellId) {

            preg_match('/^cell_(\d+)_weight$/', $fastTrackOrdersItemsCellId, $matches);
            if (isset($matches[1]) && intval($matches[1]) > 0) {
                $orderSendHistoryItemId = intval($matches[1]);

                $this->addFastTrackItem($fastTrackData['id'], $orderSendHistoryItemId, $fastTrackData['container_id']);
            }
        }
        return true;
    }


    /**
     * @param $fastTrackId
     * @param $orderSendHistoryItemId
     * @param $containerId
     * @return bool
     * @throws Exception
     */
    private function addFastTrackItem($fastTrackId, $orderSendHistoryItemId, $containerId) {

        $sSql = 'SELECT id FROM fast_track_items
                         WHERE fast_track_id = "'.$fastTrackId.'" AND 
                               orders_send_history_items_id = "'.$orderSendHistoryItemId.'" AND 
                               container_id = "'.$containerId.'"
                         LIMIT 1';
        $existItemInFastTrack = $this->pDbMgr->GetOne('profit24', $sSql);
        if (empty($existItemInFastTrack)) {
            $values = [
                'fast_track_id' => $fastTrackId,
                'orders_send_history_items_id' => $orderSendHistoryItemId,
                'container_id' => $containerId
            ];
            if (false === $this->pDbMgr->Insert('profit24', 'fast_track_items', $values)) {
                throw new Exception('Wystąpił błąd podczas dodawania składowej na FastTracka');
            }
        }
        return true;
    }


    /**
     * @param $oshiItemId
     * @param $fastTrackId
     * @return array|bool
     */
    public function unlockOrderToFastTrack($oshiItemId, $fastTrackId)
    {
        $this->pDbMgr->BeginTransaction('profit24');

        $sSql = 'SELECT OI.id, OI.order_id 
             FROM orders_send_history_items AS OSHI
             JOIN orders_items AS OI
              ON OSHI.item_id = OI.id
             WHERE OSHI.id = '.$oshiItemId.'
             FOR UPDATE';
        $ordersItems = $this->pDbMgr->GetAll('profit24', $sSql);

        foreach ($ordersItems as $item ) {
            // wkładamy już na fast tracka składową
            $fastTrackData = $this->useFastTrack();
            if (!empty($fastTrackData)) {
                if (false === $this->checkOrderAndOrderItemUnlockFastTruck($item['id'], $item['order_id'])) {
                    $this->pDbMgr->RollbackTransaction('profit24');
                    return false;
                }

                try {
                    if (false === $this->removeFastTrackItem($fastTrackId, $oshiItemId, $fastTrackData['container_id'])) {
                        $this->pDbMgr->RollbackTransaction('profit24');
                        return false;
                    }
                } catch (Exception $ex) {
                    $this->pDbMgr->RollbackTransaction('profit24');
                    return false;
                }

                $aOrderItemValues = array('get_ready_list' => '0', 'status' => '2');
                if ($this->pDbMgr->Update('profit24', 'orders_items', $aOrderItemValues, 'id = ' . $item['id'] . ' LIMIT 1') === FALSE) {
                    $this->pDbMgr->RollbackTransaction('profit24');
                    return false;
                }

                $aOrderItemValues = array('in_confirmed_quantity' => '0', 'ordered_quantity' => '0');
                if ($this->pDbMgr->Update('profit24', 'orders_send_history_items', $aOrderItemValues, 'id = ' . $oshiItemId . ' LIMIT 1') === FALSE) {
                    $this->pDbMgr->RollbackTransaction('profit24');
                    return false;
                }

                // zmieniamy składową w zamówieniu
                $aOrderValues = array(
                    'print_ready_list' => '0',
                    'order_on_list_locked' => '0',
                    'order_status' => '1'
                );
                if ($this->pDbMgr->Update('profit24', 'orders', $aOrderValues, 'id = ' . $item['order_id'] . ' LIMIT 1') === FALSE) {
                    $this->pDbMgr->RollbackTransaction('profit24');
                    return false;
                }
            } else {
                return false;
            }
        }

        $this->pDbMgr->CommitTransaction('profit24');
        return $ordersItems;
    }

    /**
     * @param $itemId
     * @param $orderId
     * @return bool
     */
    private function checkOrderAndOrderItemUnlockFastTruck($itemId, $orderId) {

        $sSql = 'SELECT O.id
                 FROM orders AS O
                 JOIN orders_items AS OI
                  ON O.id = OI.order_id
                 WHERE O.order_status = "2" AND OI.status = "4" AND order_on_list_locked = "1"
                 AND OI.id = '.$itemId.' AND O.id = '.$orderId;
        return ($this->pDbMgr->GetOne('profit24', $sSql) > 0 ? true : false);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function removeFastTrackItem($fastTrackId, $orderSendHistoryItemId, $containerId) {

        $sSql = 'SELECT id FROM fast_track_items
                         WHERE fast_track_id = "'.$fastTrackId.'" AND 
                               orders_send_history_items_id = "'.$orderSendHistoryItemId.'" AND 
                               container_id = "'.$containerId.'"
                         LIMIT 1';
        $existItemInFastTrack = $this->pDbMgr->GetOne('profit24', $sSql);
        $sSql = 'DELETE FROM fast_track_items WHERE id = '.$existItemInFastTrack;
        if (false === $this->pDbMgr->Query('profit24', $sSql)) {
            return false;
        }

        return true;
    }

    /**
     * @param $oshiItemId
     * @return bool
     */
    public function lockOrderToFastTrack($oshiItemId, $fastTrackId) {

        $this->pDbMgr->BeginTransaction('profit24');

        $sSql = 'SELECT OI.id, OI.order_id 
             FROM orders_send_history_items AS OSHI
             JOIN orders_items AS OI
              ON OSHI.item_id = OI.id
             WHERE OSHI.id = '.$oshiItemId.'
             FOR UPDATE';
        $ordersItems = $this->pDbMgr->GetAll('profit24', $sSql);

        foreach ($ordersItems as $item ) {
            // wkładamy już na fast tracka składową
            $fastTrackData = $this->useFastTrack();
            try {
                if (false === $this->addFastTrackItem($fastTrackId, $oshiItemId, $fastTrackData['container_id'])) {
                    $this->pDbMgr->RollbackTransaction('profit24');
                    return false;
                }
            } catch (Exception $ex) {
                $this->pDbMgr->RollbackTransaction('profit24');
                return false;
            }

            $aOrderItemValues = array('get_ready_list' => '1', 'status' => '4');
            if ($this->pDbMgr->Update('profit24', 'orders_items', $aOrderItemValues, 'id = '. $item['id'].' LIMIT 1') === FALSE) {
                $this->pDbMgr->RollbackTransaction('profit24');
                return false;
            }


            $aOrderItemValues = array('in_confirmed_quantity' => '1', 'ordered_quantity' => '1');
            if ($this->pDbMgr->Update('profit24', 'orders_send_history_items', $aOrderItemValues, 'id = '. $oshiItemId.' LIMIT 1') === FALSE) {
                $this->pDbMgr->RollbackTransaction('profit24');
                return false;
            }


            // zmieniamy składową w zamówieniu
            $aOrderValues = array(
                'print_ready_list' => '1',
                'order_on_list_locked' => '1',
                'order_status' => '2'
            );
            if ($this->pDbMgr->Update('profit24', 'orders', $aOrderValues, 'id = '. $item['order_id'].' LIMIT 1') === FALSE) {
                $this->pDbMgr->RollbackTransaction('profit24');
                return false;
            }
        }

        $this->pDbMgr->CommitTransaction('profit24');
        return $ordersItems;
    }

    /**
     * @param $fastTrackId
     * @return bool
     */
    public function closeFastTrack($fastTrackId) {
        $value = [
            'closed' => '1',
            'close_datetime' => 'NOW()',
        ];
        if (false === $this->pDbMgr->Update('profit24', 'fast_track', $value, ' id = '.$fastTrackId)) {
            return false;
        }
        return true;
    }

    /**
     * @param $id
     * @return array
     */
    public function getFastTrackItemsForUpdate($id)
    {
        $sSql = 'SELECT OI.id, OI.order_id, OI.product_id, OI.quantity, 
                        OI.item_type, OI.source,
                          P.isbn_plain, P.isbn_10, P.isbn_13, P.ean_13, 
                          IF(OI.source = "51", "1", "0") AS is_g
                 FROM orders_items AS OI
                 LEFT JOIN products AS P
                    ON OI.product_id = P.id
                 JOIN orders_send_history_items AS OSHI 
                  ON OSHI.item_id = OI.id
                 JOIN fast_track_items AS FTI
                  ON OSHI.id = FTI.orders_send_history_items_id
                 WHERE FTI.fast_track_id = '.$id.'
                   AND OI.deleted = "0"
                 FOR UPDATE';
        return $this->pDbMgr->GetAll('profit24', $sSql);
    }

    /**
     * @param $fastTrackId
     * @param $listId
     * @return bool
     */
    public function setOrderItemListId($fastTrackId, $listId)
    {
        $value = [
            'orders_items_lists_id' => $listId
        ];
        if (false === $this->pDbMgr->Update('profit24', 'fast_track', $value, ' id = '.$fastTrackId)) {
            return false;
        }
        return true;
    }


    /**
     * @param $fastTrackData
     * @throws Exception
     */
    public function closeConfirmFastTruck($fastTrackData) {
        global $aConfig;

        if (!empty($fastTrackData)) {
            $quantityFastTrack = intval($this->getProductQuantityFastTrack($fastTrackData['id']));
            if ($quantityFastTrack > 0) {

                    // potwierdzamy fast tracka zamykamy
                    // tworzymy zamkniętą listę zbierania singli i przypisujemy ją do właściciela fast tracka

                    $this->pDbMgr->BeginTransaction('profit24');
                    $ordersItemsFastTrack = $this->getFastTrackItemsForUpdate($fastTrackData['id']);
                    if (!empty($ordersItemsFastTrack)) {
                        if (false === $this->closeFastTrack($fastTrackData['id'])) {
                            $this->pDbMgr->RollbackTransaction('profit24');
                            $msg = _('Wystąpił błąd podczas zamykania fast tracka '.$fastTrackData['container_id'].' użytkownika '.$_SESSION['user']['name']);
                            throw new Exception($msg);
                        } else {
                            $oMemcache = new Memcached();
                            $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);

                            include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_zamowienia_magazyn_zbieranie_tramwaj/Common_get_ready_orders_books.class.php');
                            include_once($_SERVER['DOCUMENT_ROOT'].'/LIB/orders_semaphore/ordersSemaphore.class.php');

                            $stClass = new stdClass();
                            $stClass->pDbMgr = $this->pDbMgr;
                            $stClass->oOrdersSemaphore = new ordersSemaphore($oMemcache);
                            $commonGetReadyOrdersBooks = new Common_get_ready_orders_books($stClass);

                            $listId = $commonGetReadyOrdersBooks->doCreateListWithItems($ordersItemsFastTrack, $fastTrackData['container_id'], '1', '1', '', 0);
                            $this->setOrderItemListId($fastTrackData['id'], $listId);

                            if ($listId > 0) {
                                $this->pDbMgr->CommitTransaction('profit24');
                                return true;
                            } else {
                                $this->pDbMgr->RollbackTransaction('profit24');
                                $msg = _('Wystąpił błąd podczas zamykania fast tracka '.$fastTrackData['container_id'].' użytkownika '.$_SESSION['user']['name']);
                                throw new Exception($msg);
                            }
                        }
                    } else {
                        $this->pDbMgr->RollbackTransaction('profit24');
                        $msg = _('Wystąpił błąd podczas zamykania fast tracka '.$fastTrackData['container_id'].' użytkownika '.$_SESSION['user']['name']);
                        throw new Exception($msg);
                    }
            } else {
                if (false === $this->closeFastTrack($fastTrackData['id'])) {
                    $this->pDbMgr->RollbackTransaction('profit24');
                    $msg = _('Wystąpił błąd podczas zamykania fast tracka '.$fastTrackData['container_id'].' użytkownika '.$_SESSION['user']['name']);
                    throw new Exception($msg);
                } else {
                    $oMemcache = new Memcached();
                    $oMemcache->addServer($aConfig['memcached_storage']['host'], $aConfig['memcached_storage']['port'], 1);
                    $oOrdersSemaphore = new Containers($this->pDbMgr);
                    $oOrdersSemaphore->unlockContainer($fastTrackData['container_id']);

                    $this->pDbMgr->CommitTransaction('profit24');
                    return true;
                }
            }
        } else {
            $msg = _('Brak otwartych fast tracków użytkownika '.$_SESSION['user']['name']);
            throw new Exception($msg);
        }
    }
}