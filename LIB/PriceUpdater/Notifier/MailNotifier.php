<?php

namespace PriceUpdater\Notifier;

class MailNotifier
{
    private static $instance;

    private $bufforProducts = [];

    private $apiResponse;

    private $unUpdatedProducts = [];

    private $updatedProducts = [];

    private function __construct() {}

    public function setApiResponse($response)
    {
        $this->apiResponse = json_encode($response);
    }

    private function __clone() {}

    public static function getInstance()
    {
        if(self::$instance === null) {
            self::$instance = new MailNotifier();
        }

        return self::$instance;
    }

    public function sendReport()
    {
        global $aConfig;
        $shopName = $aConfig['common']['bookstore_code'];

        include_once($aConfig['common']['client_base_path'].$aConfig['common']['cms_dir'].'lib/Common.class.php');
        $content = $this->getContent();

        foreach($aConfig['ceneo']['mail_recivers'] as $mailReciver) {
            \Common::sendMail('', $aConfig['common']['import_sender_email'], $mailReciver, "Raport Ceneo dla sklepu: $shopName", $content, true);
        }
    }

    /**
     * @param array $products
     */
    public function addUnUpdatedProducts(array $products)
    {
        $this->unUpdatedProducts = array_merge($this->unUpdatedProducts, $products);
    }

    /**
     * @param $products
     */
    public function addUpdatedProducts($products)
    {
        $this->updatedProducts = array_merge($this->updatedProducts, $products);
    }

    /**
     * @param $products
     */
    public function addBufforProducts($products)
    {
        $this->bufforProducts = array_merge($this->bufforProducts, $products);
    }

    /**
     * @return array
     */
    public function getUnupdatedProducts()
    {
        return $this->unUpdatedProducts;
    }

    private function getContent()
    {
        $updatedProducts = $this->generateProductRowsHtml($this->updatedProducts);
        $unUpdatedProducts = $this->generateProductRowsHtml($this->unUpdatedProducts);
        $bufforProducts = $this->generateProductRowsHtml($this->bufforProducts, true);

        $content = '

        Odpowiedz Api: '.$this->apiResponse.'

        <br>
        <br>

        Produkty zaktualizowane <br>
        <table>
        <th>Id produktu</th>
        <th>ISBN</th>
        <th>Zaktualizowana cena na ceneo</th>
        <th>Data startu cennika w ceneo i w cenniku</th>
        <th>Procent rabatu</th>
        <th>Wartosc rabatu</th>
        <th>Minimum</th>
        <th>Typ zmiany</th>
            %s
        </table>

        <br>
        <br>
        <br>
        Produkty niezaktualizowane <br>
        <table>
        <th>Id produktu</th>
        <th>ISBN</th>
        <th>Zaktualizowana cena na ceneo</th>
        <th>Data startu cennika w ceneo i w cenniku</th>
        <th>Procent rabatu</th>
        <th>Wartosc rabatu</th>
        <th>Minimum</th>
        <th>Typ zmiany</th>
            %s
        </table>

        <br>
        <br>
        <br>
        Produkty ktore poszly do bufora cennikow <br>
        <table>
        <th>Id produktu</th>
        <th>ISBN</th>
        <th>Zaktualizowana cena na ceneo</th>
        <th>Data startu cennika w ceneo i w cenniku</th>
        <th>Procent rabatu</th>
        <th>Wartosc rabatu</th>
        <th>Minimum</th>
        <th>Typ zmiany</th>
            %s
        </table>
        ';

        return sprintf($content, $updatedProducts, $unUpdatedProducts, $bufforProducts);
    }

    private function generateProductRowsHtml($products, $disableDate = false)
    {
        $rows = '';

        foreach($products as $product) {

            $startDate = false == $disableDate ? date('Y-m-d H:i:s', $product['start_date']) : '';

            $productRow = '<tr>';
            $productRow .= '<td>'.$product['product_id'].'</td>';
            $productRow .= '<td>'.$product['isbn_plain'].'</td>';
            $productRow .= '<td>'.$product['price_brutto'].'</td>';
            $productRow .= '<td>'.$startDate.'</td>';
            $productRow .= '<td>'.$product['discount'].'</td>';
            $productRow .= '<td>'.$product['discount_value'].'</td>';
            $productRow .= '<td>'.$product['minimum'].'</td>';
            $productRow .= '<td>'.$product['change_type'].'</td>';
            $productRow .= '</tr>';

            $rows .= $productRow;
        }

        return $rows;
    }
}