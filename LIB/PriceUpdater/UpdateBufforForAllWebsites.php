<?php

use PriceUpdater\Buffor\CyclicBuffor;
use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');
ini_set('max_execution_time', 1000);

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;
//
//$websites = $pDbMgr->GetAll('profit24', "SELECT * FROM websites WHERE ceneo != ''");
//Common::sendMail('l.jaskiewicz@profit24.pl', 'l.jaskiewicz@profit24.pl', 'l.jaskiewicz@profit24.pl', 'buffor zowtal odnowiont', 'buffor zowtal odnowiont');
//
//$cyclicBuffor = new CyclicBuffor();
//foreach($websites as $website) {
//  try {
//    $cyclicBuffor->createBuffor($website);
//  } catch(\Exception $e) {
//    continue;
//  }
//}
