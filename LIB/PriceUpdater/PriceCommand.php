<?php

use Listener\CyclicBufforListener;
use PriceUpdater\Ceneo\LimitsRepository;
use PriceUpdater\ProductProvider\FinalPriceProductProvider;
use PriceUpdater\PriceUpdater;
use PriceUpdater\Ceneo\Api;
use PriceUpdater\PriceModifier\PriceModifierStrategy;
use PriceUpdater\Ceneo\CeneoRepository;
use PriceUpdater\Tarrifs\TarrifsUpdater;
use PriceUpdater\Buffor\CyclicBuffor;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');
ini_set('max_execution_time', 1000);

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$cyclicBuffor = new CyclicBuffor();

if ($cyclicBuffor->getStandardCountedProducts() < 15) {
    $cyclicBuffor->createBuffor();
}

$bufforListener = new CyclicBufforListener();
$bufforListener->onBufforRun();

$limits = new LimitsRepository();
if(false === $limits->canRunProductScript($cyclicBuffor)) {
  echo 'Nie mozna bylo wykonac skryptu';
  exit;
}

$limits->lockUpdate('compare');

$api = new Api();
$ceneoRepository = new CeneoRepository($api);

$sql = sprintf("SELECT id, ceneo FROM websites WHERE ceneo != '%s' AND ceneo != ''", $aConfig['price_updater']['our_book_stores']['current_book_store']);
$aConfig['price_updater']['our_book_stores']['other_book_stores'] = $pDbMgr->GetAssoc('profit24', $sql);

$priceModifyData = $pDbMgr->GetAssoc($aConfig['common']['bookstore_code'], "SELECT * FROM products_comparison_sites");

$productProvider       = new FinalPriceProductProvider($pDbMgr, (float)$priceModifyData['n']);
$priceModifierStrategy = new PriceModifierStrategy($aConfig['price_updater']['our_book_stores']);
$tarrifsUpdater = new TarrifsUpdater();

$priceUpdater = new PriceUpdater($pDbMgr, $productProvider, $priceModifierStrategy, $ceneoRepository, $tarrifsUpdater, $priceModifyData);

$priceUpdater->compareProducts(0, 1000);
$priceUpdater->onlyToBuffor = true;
$priceUpdater->updateProducts();

$limits->unLockUpdate('compare');

if ($cyclicBuffor->getStandardCountedProducts() < 15) {
    $cyclicBuffor->createBuffor();
}
