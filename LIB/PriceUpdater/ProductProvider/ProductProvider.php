<?php

namespace PriceUpdater\ProductProvider;

interface ProductProvider
{

    /**
     * @param $updateNowLimit
     * @param $bufforLimit
     *
     * @return mixed
     */
    public function getPreparedProducts($updateNowLimit, $bufforLimit);
}