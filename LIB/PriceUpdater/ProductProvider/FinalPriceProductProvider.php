<?php

namespace PriceUpdater\ProductProvider;

use DatabaseManager;
use LIB\Helpers\ArrayHelper;
use PriceUpdater\Buffor\CyclicBuffor;

class FinalPriceProductProvider implements ProductProvider
{
    /**
     * @var DatabaseManager
     */
    private $db;

    /**
     * @var null
     */
    private $products = null;

    /**
     * float @var
     */
    private $maxDiscount;

    /**
     * @var array
     */
    private $config;

    /**
     * @var string
     */
    private $mainBookstore = 'np';

    /**
     * @var array
     */
    private $sources = [
        'abe' => 15
    ];

    public function __construct(DatabaseManager $db, $maxDiscount)
    {
        global $aConfig;

        $this->config = $aConfig;
        $this->db = $db;
        $this->maxDiscount = $maxDiscount;
    }

    /**
     * @param $updateNowLimit
     * @param $bufforLimit
     *
     * @return array
     */
    public function getPreparedProducts($updateNowLimit, $bufforLimit)
    {
        if (null === $this->products) {
            $this->setPreparedProducts($updateNowLimit, $bufforLimit);
        }

        return $this->products;
    }

    /**
     * @param $updateNowLimit
     * @param $bufforLimit
     */
    private function setPreparedProducts($updateNowLimit, $bufforLimit)
    {
        $productsToBufor = $this->getProductsData($bufforLimit);

        $products = $productsToBufor;

        if (true === empty($products)) {
            $this->products = [];
        }

        $this->products = $this->prepareProducts($products);
    }

    private function setMainBookstoreId($products)
    {
        $productsIds = implode(',', ArrayHelper::toKeyValue($products, 'id', 'id'));

        $sql = sprintf("SELECT id, profit24_id FROM products WHERE profit24_id IN(%s)", $productsIds);

        $npIds = ArrayHelper::toKeyValue($this->db->GetAll($this->mainBookstore, $sql), 'profit24_id', 'id');

        return $npIds;
    }

    /**
     * @param $products
     *
     * @return array
     */
    private function prepareProducts($products)
    {
        $npIds = $this->setMainBookstoreId($products);

        foreach($products as $key => $product) {

            if (false === isset($npIds[$product['id']])) {
                CyclicBuffor::deleteSingleFromBuffor($product['id']);
                unset($products[$key]);
                continue;
            }

            $products[$key]['id'] = $npIds[$product['id']];
            $product['id'] = $npIds[$product['id']];

            $purchasePrice     = (float) $product['wholesale_price'];
            $publisherDiscount = (float) $product['discount_limit'];
            $ignoreDiscount    = (bool) $product['ignore_discount'];
            $priceBrutto       = (float) $product['price_brutto'];
			$catPriceBrutto    = (float) $product['catalogue_price_brutto'];

            $products[$key]['has_changed'] = false;
	
            // oblicza cene minimalna za jaka kwote moze byc sprzedany produkt
            $minimumPrice = $purchasePrice + (($purchasePrice * $this->maxDiscount) / 100);
			
            $minimumPublisherPrice = 0;

            if (false === $ignoreDiscount && $publisherDiscount > 0) {
                $minimumPublisherPrice = $catPriceBrutto - (($catPriceBrutto * $publisherDiscount) / 100);
            }

            $products[$key]['minimum_price'] = $this->prepareMinimumPrice($product, $minimumPublisherPrice, $minimumPrice);

            unset($products[$key]['source']);
			
            $products[$key]['minimum_price'] = round($products[$key]['minimum_price'], 2);
        }

        return $products;
    }

    /**
     * @param $product
     * @param $minimumPublisherPrice
     * @param $minimumPrice
     *
     * @return float
     */
    private function prepareMinimumPrice($product, $minimumPublisherPrice, $minimumPrice)
    {
        $minPrice = null;

        if ($minimumPublisherPrice > 0) {
            $minPrice = $minimumPublisherPrice;
        } else {
            $minPrice = $minimumPrice;
        }

        if ($minPrice == 0) {
            $minPrice = $product['catalogue_price_brutto'];

            if (true === in_array($product['source'], array_keys($this->sources))) {
                $minPrice = $product['catalogue_price_brutto'] - (($product['catalogue_price_brutto'] * $this->sources[$product['source']]) / 100);
            }
        }

        return (float)$minPrice;
    }

    /**
     * @param $limit
     *
     * @param int $priority
     *
     * @return array
     */
    private function getProductsData($limit)
    {
        $column = $this->config['common']['bookstore_code'] != 'profit24' ? "P.profit24_id" : "P.id";

        $stm = "
            SELECT S.symbol as source, P.id, P.vat, P.shipment_time, P.isbn_plain, PP.discount_limit, P.wholesale_price, PP.ignore_discount,
                   P.price_brutto AS catalogue_price_brutto, %s AS profit24_id,
                    (SELECT PT.price_brutto FROM products_tarrifs AS PT WHERE PT.product_id = P.id ORDER BY PT.type DESC, PT.price_brutto LIMIT 1) AS price_brutto
            FROM cyclic_buffor as CB
            JOIN products as P ON CB.product_id = %s
            JOIN sources as S ON P.source = S.id
            JOIN products_publishers PP on P.publisher_id = PP.id
            WHERE CB.status = '1'
            GROUP BY P.id
            ORDER BY CB.priority DESC
            LIMIT %d
        ";

        $sql = sprintf($stm, $column, $column, $limit);
		$result = $this->db->GetAll($this->config['common']['bookstore_code'], $sql);
		//var_dump('test');

		return $result;
        //return $this->db->GetAll($this->config['common']['bookstore_code'], $sql);

    }
}