<?php

namespace PriceUpdater\Buffor;

use DatabaseManager;
use Listener\CyclicBufforListener;
use table\DataBaseHelper;
use LIB\Helpers\ArrayHelper;

class CyclicBuffor
{
    const BUFFOR_TABLE = 'cyclic_buffor';

    /**
     * @var array
     */
    private $currentWebsite;
    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    private $limit = 35000;

    public function __construct()
    {
        global $pDbMgr, $aConfig;

        $currentWebsite = $pDbMgr->GetRow('profit24', sprintf("SELECT * FROM websites WHERE code = '%s' LIMIT 1", $aConfig['common']['bookstore_code']));

        $this->currentWebsite = $currentWebsite;
        $this->pDbMgr = $pDbMgr;
    }

    public function createBuffor($website = null)
    {
        if (null !== $website) {
            $this->currentWebsite = $website;
        }

        $this->clearCyclicBuffor();

        $productsToBuffor = $this->getProductsToBuffor();

        if (true === empty($productsToBuffor)) {
            throw new \Exception(sprintf('Brak produktow w wybranych kategoriach dla sklepu: %s', $this->currentWebsite['code']));
        }

        $this->insertData($this->getProductsToBuffor());
    }

    /**
     * @return array
     */
    private function getProductsToBuffor()
    {
        $sql = "
            SELECT P.id AS product_id, P.shipment_time AS priority
            FROM website_ceneo_categories as WCC
            JOIN products AS P on P.main_category_id = WCC.category_id
            WHERE WCC.website_id = %d
            AND P.published = '1'
            AND P.packet = '0'
            AND P.prod_status = '1'
            AND P.shipment_time='0'
            GROUP BY P.id
            ORDER BY P.shipment_time DESC
            LIMIT %d
        ";

        $standardBufforProducts = $this->pDbMgr->GetAll('profit24', sprintf($sql, $this->currentWebsite['id'], $this->limit));

        $cyclicBufforListener = new CyclicBufforListener();
        $standardBufforProducts = $cyclicBufforListener->onBufforCreate($standardBufforProducts);

        return $standardBufforProducts;
    }

    private function getBufforData()
    {
        $sql = "
            SELECT product_id, priority
            FROM cyclic_buffor
            WHERE priority = '1' AND `status` = 1
        ";

        return $this->pDbMgr->GetAssoc($this->currentWebsite['code'], $sql);
    }

    /**
     * @param $products
     */
    private function insertData($products)
    {
        $priorityProducts = $this->getBufforData();

        foreach($products as $product) {

            if (array_key_exists($product['product_id'], $priorityProducts)) {
                continue;
            }

            $values = [
                'product_id' => $product['product_id'],
            ];

            $this->pDbMgr->Insert($this->currentWebsite['code'], self::BUFFOR_TABLE, $values);
        }
    }

    public function getCountedProducts()
    {
        $sql = "SELECT count(*) FROM %s";

        return $this->pDbMgr->GetOne($this->currentWebsite['code'], sprintf($sql, self::BUFFOR_TABLE));
    }

    public function getStandardCountedProducts()
    {
        $sql = "SELECT count(*) FROM %s WHERE `status` = '1' AND `priority` != '1'";

        return $this->pDbMgr->GetOne($this->currentWebsite['code'], sprintf($sql, self::BUFFOR_TABLE));
    }

    public function clearCyclicBuffor()
    {
        global $aConfig;

        $sql = "DELETE FROM %s WHERE `priority` = '0' OR (`priority` = '1' AND `status` = '0')";

        return (bool) $this->pDbMgr->Query($aConfig['common']['bookstore_code'], sprintf($sql, self::BUFFOR_TABLE));
    }


    public static function changeStatusToUnactive($originalProducts)
    {
        global $pDbMgr, $aConfig;

        $sql = "UPDATE %s SET status = '0' WHERE product_id = %d";

        foreach($originalProducts as $product) {

            $pDbMgr->Query($aConfig['common']['bookstore_code'], sprintf($sql, self::BUFFOR_TABLE, $product['profit24_id']));
        }
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public static function deleteSingleFromBuffor($id)
    {
        global $pDbMgr, $aConfig;
        $sql = "DELETE FROM %s WHERE product_id = %d";

        return (bool) $pDbMgr->Query($aConfig['common']['bookstore_code'], sprintf($sql, self::BUFFOR_TABLE, $id));
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public static function insertSingleToBuffor($id, $priority)
    {
        global $pDbMgr, $aConfig;

        $values = [
            'product_id' => $id,
            'priority' => $priority
        ];

        return (bool) $pDbMgr->Insert($aConfig['common']['bookstore_code'], self::BUFFOR_TABLE, $values);
    }
}
