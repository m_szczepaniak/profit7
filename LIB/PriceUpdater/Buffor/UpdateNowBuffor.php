<?php

namespace PriceUpdater\Buffor;

use DatabaseManager;
use table\DataBaseHelper;

class UpdateNowBuffor
{
    /**
     * @var DatabaseManager
     */
    private $db;

    /**
     * @var array
     */
    private $config;

    public function __construct()
    {
        global $pDbMgr, $aConfig;

        $this->db = $pDbMgr;
        $this->config = $aConfig;
    }

    /**
     * @param array $preparedProducts
     *
     * @return bool
     */
    public function saveProducts(array $preparedProducts)
    {
        if (true === empty($preparedProducts)) {
            return false;
        }

        $existsProducts = $this->getExistsProducts();

        foreach($preparedProducts as $product) {

            $values = [
                'product_id' => $product['product_id'],
                'discount' => $product['discount'],
                'discount_value' => $product['discount_value'],
                'price_netto' => $product['price_netto'],
                'price_brutto' => $product['price_brutto'],
                'minimum' => $product['minimum'],
                'change_type' => $product['change_type'],
            ];

            if (true === array_key_exists($product['product_id'], $existsProducts)) {
                $id = $product['product_id'];

                $this->db->Update($this->config['common']['bookstore_code'], "update_now_buffor", $values, 'product_id = '.$id);

            } else {
                $this->db->Insert($this->config['common']['bookstore_code'], "update_now_buffor", $values);
            }
        }

        return true;
    }

    private function getExistsProducts()
    {
        $sql = "
            SELECT product_id, id
            FROM update_now_buffor
        ";

        return $this->db->GetAssoc($this->config['common']['bookstore_code'], $sql);
    }

  public function getBufforProducts($limit = 0)
  {
    $sql = "SELECT TB.id, TB.product_id, TB.discount, TB.discount_value, TB.price_netto, TB.price_brutto, P.isbn_plain, TB.minimum, TB.change_type
                FROM update_now_buffor AS TB
                LEFT JOIN products as P ON TB.product_id = P.id
                LIMIT %d
                ";

    $results = $this->prepareBuforProductBeforeUpdate($this->db->GetAll($this->config['common']['bookstore_code'], sprintf($sql, $limit)));

    return $results;
  }

  /**
   * @param $bufforProducts
   *
   * @return array
   */
  private function prepareBuforProductBeforeUpdate($bufforProducts)
  {
    $preparedProducts = [];

    foreach($bufforProducts as $product) {
      $product['start_date'] = strtotime('+60 minutes', strtotime(date('Y-m-d H:i')));
      $product['type'] = 1;

      $preparedProducts[] = $product;
    }

    return $preparedProducts;
  }

  /**
   * @param $preparedProducts
   *
   * @return bool
   */
  public function deleteProductsFromBuffor($preparedProducts)
  {
    $sql = "DELETE FROM update_now_buffor WHERE id = %d";

    foreach($preparedProducts as $product) {
        $this->db->Query($this->config['common']['bookstore_code'], sprintf($sql, $product['id']));
    }
  }

  public function getCountedProducts()
  {
    $sql = "SELECT count(*) FROM update_now_buffor";

    return $this->db->GetOne($this->config['common']['bookstore_code'], $sql);
  }
}