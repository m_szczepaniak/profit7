<?php

namespace PriceUpdater\Tarrifs;

use DatabaseManager;
use table\DataBaseHelper;

class TarrifsUpdater
{
    const UPDATED_TYPE = 1;

    const TARRIFS_BUFFOR_TABLE = 'tarrifs_buffor';

    /**
     * @var DatabaseManager
     */
    private $db;

    /**
     * @var array
     */
    private $config;

    public function __construct()
    {
        global $pDbMgr, $aConfig;

        $this->db = $pDbMgr;
        $this->config = $aConfig;
    }

    /**
     * @param array $preparedProducts
     *
     * @return bool
     */
    public function updateProductTarrifs(array $preparedProducts)
    {
        if (true === empty($preparedProducts)) {
            return false;
        }

        $existsTarrifs = $this->getExistsTarrifs();

        foreach($preparedProducts as $product) {

            $values = [
                'product_id' => $product['product_id'],
                'discount' => $product['discount'],
                'discount_value' => $product['discount_value'],
                'price_netto' => $product['price_netto'],
                'price_brutto' => $product['price_brutto'],
                'start_date' => $product['start_date'],
                'type' => $product['type'],
            ];

            if (true === isset($existsTarrifs[$product['product_id']])) {
                $id = $existsTarrifs[$product['product_id']];

                $this->db->Update($this->config['common']['bookstore_code'], "products_tarrifs", $values, 'id = '.$id);

            } else {
                $this->db->Insert($this->config['common']['bookstore_code'], "products_tarrifs", $values);
            }
        }

        return true;
    }

    public function deleteProductsFromTarrifsBuffor($preparedProducts)
    {
        if (true === empty($preparedProducts)) {
            return false;
        }

        foreach($preparedProducts as $product) {
                $this->db->Query($this->config['common']['bookstore_code'], "DELETE FROM tarrifs_buffor WHERE product_id = ".$product['product_id']);
        }
    }

    public function moveBufforProductsToTarrifs()
    {
        $bufforProducts = $this->getTarrifsBuforProducts();

        return $this->updateProductTarrifs($bufforProducts);
    }

    public function getTarrifsBuforProducts()
    {
        $sql = "SELECT TB.product_id, TB.discount, TB.discount_value, TB.price_netto, TB.price_brutto, P.isbn_plain
                FROM tarrifs_buffor AS TB
                LEFT JOIN products as P ON TB.product_id = P.id
                ";

        $results = $this->prepareBuforProductBeforeUpdate($this->db->GetAll($this->config['common']['bookstore_code'], $sql));

        return $results;
    }

    /**
     * @param $bufforProducts
     *
     * @return array
     */
    private function prepareBuforProductBeforeUpdate($bufforProducts)
    {
        $preparedProducts = [];

        foreach($bufforProducts as $product) {
            $product['start_date'] = strtotime(date('Y-m-d h:i'));
            $product['type'] = 1;

            $preparedProducts[] = $product;
        }

        return $preparedProducts;
    }

    public function clearTarrifsBuforData()
    {
        $this->db->GetAll($this->config['common']['bookstore_code'], "DELETE FROM tarrifs_buffor");
    }

    /**
     * @param array $preparedProducts
     *
     * @return bool
     */
    public function saveProductsToTarrifsBuffor(array $preparedProducts)
    {
        if (true === empty($preparedProducts)) {
            return false;
        }

        $existsTarrifsBuffor = $this->getExistsTarrifsBuffor();

        foreach($preparedProducts as $product) {

            $values = [
                'product_id' => $product['product_id'],
                'discount' => $product['discount'],
                'discount_value' => $product['discount_value'],
                'price_netto' => $product['price_netto'],
                'price_brutto' => $product['price_brutto'],
            ];

            if (true === array_key_exists($product['product_id'], $existsTarrifsBuffor)) {
                $id = $product['product_id'];

                $this->db->Update($this->config['common']['bookstore_code'], self::TARRIFS_BUFFOR_TABLE, $values, 'product_id = ' . $id);

            } else {
                $this->db->Insert($this->config['common']['bookstore_code'], self::TARRIFS_BUFFOR_TABLE, $values);
            }
        }
    }

    private function getExistsTarrifsBuffor()
    {
        $sql = "
            SELECT product_id, id
            FROM tarrifs_buffor

        ";

        return $this->db->GetAssoc($this->config['common']['bookstore_code'], $sql);
    }

    private function getExistsTarrifs()
    {
        $sql = "
            SELECT product_id, id
            FROM products_tarrifs
            WHERE TYPE = ".self::UPDATED_TYPE."
        ";

        return $this->db->GetAssoc($this->config['common']['bookstore_code'], $sql);
    }
}