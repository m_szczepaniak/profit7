<?php

namespace PriceUpdater\Ceneo;

use DateTime;
use PriceUpdater\Buffor\CyclicBuffor;
use PriceUpdater\Buffor\UpdateNowBuffor;
use Common;

class LimitsRepository
{
    const GET_PRODUCTS_LIMIT_NAME = "webapi_data_critical.shop_getProductTop10OffersBy_IDs";

    const GET_UPDATE_LIMIT_NAME = "offer_processor.process_offers";

    const MINUTES_TO_ADD = 2;

    const TIME_TO_SLEEP_WHEN_LOCKED = 130;

    private $api;

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct()
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;

        $this->api = new Api();
		
		        $executionLimits = $this->api->call('webapi_meta.GetExecutionLimits');
	
        $limit = $this->getLimit($executionLimits['limits']['limit'], self::GET_PRODUCTS_LIMIT_NAME);
        $limit2 = $this->getLimit($executionLimits['limits']['limit'], self::GET_UPDATE_LIMIT_NAME);
		var_dump($limit);
		var_dump($limit2);
		//die;
    }
	
	private function fixBrokenSemaphore()
    {
        $sql = "UPDATE ceneo_locks set `value` = 0";

        return $this->pDbMgr->Query('profit24', $sql);
    }

    private function isTimeToBreak()
    {
        $from = new DateTime('00:00');
        $fromFormatted = $from->format('Y-m-d H:i:s');

        $to = new DateTime('07:00');
        $toFormatted = $to->format('Y-m-d H:i:s');

        $now = new DateTime('NOW');
        $nowFormatted = $now->format('Y-m-d H:i:s');

        if ($nowFormatted >= $fromFormatted && $nowFormatted <= $toFormatted)
        {
			$this->fixBrokenSemaphore();
			
            return true;
        }
		
		$from = new DateTime('12:00');
        $fromFormatted = $from->format('Y-m-d H:i:s');

        $to = new DateTime('12:59');
        $toFormatted = $to->format('Y-m-d H:i:s');

        $now = new DateTime('NOW');
        $nowFormatted = $now->format('Y-m-d H:i:s');

        if ($nowFormatted >= $fromFormatted && $nowFormatted <= $toFormatted)
        {
            return true;
        }

        return false;
    }

  /**
   * @param CyclicBuffor $cyclicBuffor
   * @return bool
   */
    public function canRunProductScript(CyclicBuffor $cyclicBuffor)
    {
        if (true === $this->isTimeToBreak()) {
            return false;
        }

        if ($cyclicBuffor->getCountedProducts() == 0) {
            return false;
        }

        if (true === $this->isLocked('compare')) {
            var_dump('Czekam ... 11');
            sleep(self::TIME_TO_SLEEP_WHEN_LOCKED);
            return $this->canRunProductScript($cyclicBuffor);
        }

        $productLimit = $this->checkLimitsForGetProducts(100, self::GET_PRODUCTS_LIMIT_NAME);

        if(true === is_int($productLimit)) {
            var_dump('Czekam ... 22');
            sleep($productLimit);
            return $this->canRunProductScript($cyclicBuffor);
        }

        return true;
    }

    /**
     * @param $lockerName
     *
     * @return bool
     */
    public function isLocked($lockerName)
    {
        $sql = "SELECT `value` FROM ceneo_locks WHERE `name` = '$lockerName'";

        return (bool) $this->pDbMgr->GetOne('profit24', $sql);
    }

    public function canRunUpdateScript(UpdateNowBuffor $updateNowBuffor)
    {
        if (true === $this->isTimeToBreak()) {
            return false;
        }

        if($updateNowBuffor->getCountedProducts() == 0) {
          var_dump('Czekam ... 1 ');
          sleep(self::TIME_TO_SLEEP_WHEN_LOCKED);
          return $this->canRunUpdateScript($updateNowBuffor);
        }

        if (true === $this->isLocked('update')) {
            var_dump('Czekam ... 2');
            sleep(self::TIME_TO_SLEEP_WHEN_LOCKED);
            return $this->canRunUpdateScript($updateNowBuffor);
        }

        $productLimit = $this->checkLimitsForGetProducts(10, self::GET_UPDATE_LIMIT_NAME);

        if(true === is_int($productLimit)) {
          var_dump('Czekam ... 3');
          sleep($productLimit);
          return $this->canRunUpdateScript($updateNowBuffor);
        }

        return true;
    }

  /**
   * @param int $acceptableExecutions
   * @param $limitName
   * @return bool|int
   */
    public function checkLimitsForGetProducts($acceptableExecutions = 100, $limitName)
    {
        $executionLimits = $this->api->call('webapi_meta.GetExecutionLimits');
	
        $limit = $this->getLimit($executionLimits['limits']['limit'], $limitName);

        // oznacza, ze stary limit wygasl i mozemy aktualizowac
        if (null === $limit['period_executions_left']) {
            return false;
        }

        // jezeli pozostala ilosc wykonan zapytania jest za mala to czekamy
        if ($limit['period_executions_left'] < $acceptableExecutions) {
            return (int)$this->getSecondsToWait($limit['period_expires']);
        }

        return false;
    }

  /**
   * @param $periodExpires
   * @return int
   */
    private function getSecondsToWait($periodExpires)
    {
        $now = new DateTime(date('Y-m-d H:i:00'));
        $periodExpires = new DateTime($periodExpires);

        $dateDiff = $now->diff($periodExpires)->format('%i') + self::MINUTES_TO_ADD;

        return $dateDiff * 60;
    }

  /**
   * @param array $limitsList
   * @param $limitName
   * @return null
   */
    private function getLimit(array $limitsList, $limitName)
    {
        foreach($limitsList as $limit) {
            if($limit['function'] == $limitName) {
              return $limit;
            }
        }

        return null;
    }

    /**
     * @param $lockName
     *
     * @return mixed
     */
    public function lockUpdate($lockName)
    {
        return $this->pDbMgr->Update('profit24', 'ceneo_locks', ['value' => '1'], "`name` = '$lockName'");
    }

    /**
     * @param $lockName
     *
     * @return mixed
     */
    public function unLockUpdate($lockName)
    {
        return $this->pDbMgr->Update('profit24', 'ceneo_locks', ['value' => '0'], "`name` = '$lockName'");
    }
}