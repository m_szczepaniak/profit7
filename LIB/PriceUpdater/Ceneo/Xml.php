<?php

namespace PriceUpdater\Ceneo;

use DOMDocument;
use LIB\Helpers\ArrayHelper;
use LIB\Helpers\FileHelper;

/**
 * Nakladka na syf w generateCeneoXml_v2.php
 *
 * Class Xml
 * @package PriceUpdater\Ceneo
 */
class Xml
{
    /**
     * @param array $productsToSend
     *
     * @return string
     */
    public function generateCeneoXml(array $productsToSend)
    {
        global $aConfig;

        include_once($aConfig['common']['base_path'].'import/ceneo_functions.php');

        $productsToSend = $this->getPreparedProductForCeneoUpdate($aConfig, $productsToSend);

        /** NAGLOWEK XMLA */
        $sXml = '<?xml version="1.0" encoding="utf-8"?>
        <offers xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1">
        <group name="books">
        ';

        $aShipmentTimes = getShipmentTimes();

        $i = 0;
        /** DODAWANIE PRODUKTOW */
        foreach($productsToSend as $product) {
            $aCat = getProductCat($product['id']);
            if(!empty($aCat['name'])){
                $aUniqeIdents = getUniqueIdents($product);
                $sUniqueIdent = $aUniqeIdents[0];

//                $btrue = true;
//                foreach ($aUniqeIdents as $sUniqueIdent) {
                    $sXml .= generateElement($product, $sUniqueIdent, $aCat, $aShipmentTimes, true, true);
//                    $btrue = false;
//                }
            }
            $i++;
        }

        $sXml .= '</group>'.'</offers>';

        return $sXml;
    }

    /**
     * @param $aConfig
     * @param array $productToUpdate
     *
     * @return array
     */
    private function getPreparedProductForCeneoUpdate($aConfig, array $productToUpdate)
    {
        global $pDbMgr, $aConfig;

        $productIds = implode(',', ArrayHelper::arrayColumn($productToUpdate, 'product_id'));

        $sSql = "SELECT A.id, A.plain_name, A.isbn_plain, A.isbn_10, A.isbn_13, A.ean_13, A.pages, A.description,
									A.shipment_time, B.name AS publisher_name,
									C.binding AS binding_name, D.dimension AS dimension_name,
									A.packet, A.weight
					 FROM " . $aConfig['tabls']['prefix'] . "products A
					 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_publishers B
					 	ON B.id=A.publisher_id
					 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_bindings C
					 	ON C.id=A.binding
					 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_dimensions D
					 	ON C.id=A.dimension
					 LEFT JOIN " . $aConfig['tabls']['prefix'] . "products_images I
							 	 ON I.product_id = A.id
					 WHERE A.id IN ($productIds)
					 			GROUP BY A.id";

        $result = (array) $pDbMgr->GetAll($aConfig['common']['bookstore_code'], $sSql);

        $this->prepareProductsForXml($result, $productToUpdate);

        return $result;
    }

    /**
     * @param array $productsToPrepare
     * @param $productToUpdate
     */
    private function prepareProductsForXml(array &$productsToPrepare, $productToUpdate)
    {
        $productPrices = ArrayHelper::toKeyValue($productToUpdate, 'product_id', 'price_brutto');

        foreach ($productsToPrepare as &$product) {

            if (false === isset($productPrices[$product['id']])) {
                continue;
            }

            $product['price_brutto'] = $productPrices[$product['id']];
        }
    }

    /**
     * @param $xml
     */
    public function saveXmlToDisk($xml, $status = 'success')
    {
        global $aConfig;

        $now = date('Y-m-d h-i');
        $name = substr(md5(mt_rand(100,999)), 0 , 10).'_'.date('Y-m-d h-i-s').'.xml';

        $path = $aConfig['common']['ceneo_xml_path'].$now.'/'.$status;

        $file = dirname($path).'/'.$status.'/'.$name;

        FileHelper::createPath($path);

        file_put_contents($file, $xml);
    }
}