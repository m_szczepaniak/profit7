<?php

namespace PriceUpdater\Ceneo;

class CeneoHelper
{
    private $ourStores;
    private $config;

    /**
     * @param $ourStores
     */
    public function __construct($ourStores)
    {
        global $aConfig;

        $this->ourStores = $ourStores;
        $this->config = $aConfig;
    }

    /**
     * @param $offers
     * @return bool
     */
    public function ourAreNear($offers)
    {
        $offers = array_values($offers);

        foreach($offers as $key => $offer){

            $currentOurKey = array_search($offer['CustName'], $this->ourStores);
            $currentOurShopName = $this->ourStores[$currentOurKey];

            if (null !== $currentOurShopName){
                $next = $offers[($key + 1)];

                if (null == $next){
                    return false;
                }

                $nextOurKey = array_search($next['CustName'], $this->ourStores);
                $nextOurShopName = $this->ourStores[$nextOurKey];
                if (null !== $nextOurShopName && $nextOurShopName != $currentOurShopName){
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $products
     */
    public function sendNearProductsMail($products)
    {
        $content = $this->getContent($products);

        foreach($this->config['ceneo']['near_products_mail_recivers'] as $mailReciver) {
            \Common::sendMail('', $this->config['common']['import_sender_email'], $mailReciver, "Nieprawidlowe produkty:", $content, true);
        }
    }

    /**
     * @param $products
     * @return string
     */
    private function getContent($products)
    {
        $html = '';

        foreach($products as $product){
            $html .= $product['isbn_plain'].'<br>';
        }

        return $html;
    }
} 