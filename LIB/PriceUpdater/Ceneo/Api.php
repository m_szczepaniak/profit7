<?php

namespace PriceUpdater\Ceneo;

class Api
{

  private $_api_domain = "https://partnerzyapi.ceneo.pl/";

  private $_api_key = null;
  private $_authorized = false;

  private $_token = null;
  private $_token_type = null;
  private $_token_expire = null;
  private $_time_to_live = 5;
  private $_resultFormat = 'json';

  public function __construct()
  {
    global $aConfig;

    $this->_api_key = $aConfig['ceneo']['api_key'];
    $this->_api_domain = $aConfig['ceneo']['api_domain'];
    $this->_time_to_live = $aConfig['ceneo']['api_time_to_live_minutes'];
  }

  public function call($resource_name, array $arguments = array(), $shouldAuthorize = false, $try = 3)
  {
    if (!$this->_isAuthorized() && true === $shouldAuthorize) {
      $this->_authorize();
    }

    $headers = array("Authorization: Bearer {$this->_token}");
    if (!isset($arguments['resultFormatter'])) {
      $arguments['resultFormatter'] = $this->_resultFormat;
    }

    $response = $this->_callServer($resource_name, $arguments, $headers);

      if (!isset($response['Status'])) {
          return null;
      }

      return $response['Result'];
  }


  private function _isAuthorized()
  {
    return ($this->_authorized && time() < $this->_token_expire);
  }


  private function _authorize()
  {
    $parameters = [
      'time_to_live_minutes'    => $this->_time_to_live,
      'apiKey'          => $this->_api_key,
      'resultFormatter' => $this->_resultFormat
    ];
    $response = $this->_callServer('auth.CreateToken', $parameters, []);
    $this->_token = $response['Result']['token'];
    $this->_token_expire = time() + ($this->_time_to_live * 60);
    $this->_authorized = ($response['Status'] == 'OK');
  }

  private function _callServer($method, $parameters = array())
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);

    // pomiĹ ssl weryfikacjÄ:
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    $url = $this->_api_domain.$method;

    if (false === empty($parameters)) {

      foreach($parameters as $key => $parameter) {
        if (is_array($parameter)) {
          $parameters[$key] = implode(',', $parameter);
        }
      }

      $url.="/Call?".http_build_query($parameters);
    }

    $url .= '&apiKey='.$this->_api_key;

    // ustawiamy adres pod jaki wchodzimy
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
      curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    // wywoĹujemy ĹźÄdanie:
    $response = $this->decodeResponse(curl_exec($ch));

    curl_close($ch);

      if (null === $response) {
          throw new \Exception('Zadanie trwalo zbyt dlugo');
      }

      if ($response['Status'] == 'FAILED') {
          throw new \Exception($response['Exceptions']['Exception'][0]);
      }

    return $response;
  }

  private function decodeResponse($response)
  {
    switch ($this->_resultFormat) {
      case 'json':
        return json_decode($response, true)['Response'];
        break;
      default:
        return $response;
    }
  }

  private function _httpParseHeaders($raw_headers)
  {
    if (function_exists('http_parse_headers')) {
      return http_parse_headers($raw_headers);
    }
    $headers = array();

    foreach (explode("\n", $raw_headers) as $i => $h) {
      $h = explode(':', $h, 2);

      if (isset($h[1])) {
        if (!isset($headers[$h[0]])) {
          $headers[$h[0]] = trim($h[1]);
        } else if (is_array($headers[$h[0]])) {
          $tmp = array_merge($headers[$h[0]], array(trim($h[1])));
          $headers[$h[0]] = $tmp;
        } else {
          $tmp = array_merge(array($headers[$h[0]]), array(trim($h[1])));
          $headers[$h[0]] = $tmp;
        }
      }
    }

    return $headers;
  }
}

//$api_key = "PUT_API_KEY_HERE";
//$api = new CeneoApi($api_key);
//$r = $api->call('PartnerService.svc/Categories');
//print_r($r);
//
//$r = $api->call('PartnerService.svc/Categories(40)');
//print_r($r);
//
//$r = $api->call('http://partnerzyapi.ceneo.pl/PartnerService.svc/Categories(40)/Products');
//print_r($r);




