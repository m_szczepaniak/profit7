<?php

namespace PriceUpdater\Ceneo;

use PriceUpdater\Ceneo\Api as CeneoApi;
use DatabaseManager;
use LIB\Helpers\ArrayHelper;
use PriceUpdater\Notifier\MailNotifier;

class CeneoRepository
{
    /**
     * @var CeneoApi
     */
    private $api;

    /**
     * @var array $config
     */
    private $config;

    /**
     * @var DatabaseManager $db
     */
    private $db;

    public function __construct(CeneoApi $api)
    {
        global $config, $pDbMgr;

        $this->config = $config;
        $this->db = $pDbMgr;
        $this->api = $api;
    }

    /**
     * @param $products
     *
     * @return array
     */
    public function getProductsByIds($products)
    {
        $dividedProducts = array_chunk($products, 20);

        $ceneoProducts = [];

        foreach($dividedProducts as $divided) {
            try {
                $currentCeneoProducts = $this->api->call('webapi_data_critical.shop_getProductTop10OffersBy_IDs', [
                    'shop_product_ids_comma_separated' => ArrayHelper::arrayColumn($divided, 'id')
                ]);
            } catch (\Exception $e) {
                continue;
            }

            if (false === isset($currentCeneoProducts['product_offers_by_ids']['product_offers_by_id'])) {
                continue;
            }

            $result = array_merge($currentCeneoProducts['product_offers_by_ids']['product_offers_by_id'], $ceneoProducts);
            $ceneoProducts = $result;
        }

        $result = [];

        foreach($ceneoProducts as $ceneoProduct) {
            if (false === isset($ceneoProduct['offers'])) {
                $ceneoProduct['offers']['offer'] = [];
            }

            $result[$ceneoProduct['CustProdID']] = $ceneoProduct;
        }

        return $result;
    }

    private function sendPostXml($xml)
    {

        //set POST variables
                $url = 'http://developers.ceneo.pl/api/v2/function/offer_processor.process_offers/Call';
                $fields = array(
                    'apiKey' => urlencode('eab8423f-71b6-452a-a816-09674f42186d'),
                    'xml' => urlencode($xml),
                    'resultFormatter' => urlencode('json'),
                );
        $fields_string = '';
        //url-ify the data for the POST
                foreach($fields as $key=>$value) {
                    $fields_string .= $key.'='.$value.'&';
                }
                rtrim($fields_string, '&');

        //open connection
                $ch = curl_init();

        //set the url, number of POST vars, POST data
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch,CURLOPT_POST, count($fields));
                curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,TRUE);

        //execute post
                $result = json_decode(curl_exec($ch), true);

        //close connection
                curl_close($ch);

        MailNotifier::getInstance()->setApiResponse($result);

        if ($result['Response']['Status'] == 'FAILED') {
            throw new \Exception($result['Response']['Exceptions']['Exception'][0]);
        }
    }

    /**
     * @param $preparedProducts
     *
     * @return string
     */
    public function sendProductsToCeneo($preparedProducts)
    {
        $dividedProducts = array_chunk($preparedProducts, 100);
        $mailer = MailNotifier::getInstance();
        $ceneoXml = new Xml();

        foreach ($dividedProducts as $dividedArray) {
            $xml = $ceneoXml->generateCeneoXml($dividedArray);

            //TODO odkomentowanie spowoduje wyslanie danych do ceneo
            try {

                $this->sendPostXml($xml);

                $ceneoXml->saveXmlToDisk($xml);

            } catch (\Exception $e) {
                $mailer->addUnUpdatedProducts($dividedArray);

                $this->removeUnUpdatedProducts($preparedProducts, $dividedArray);

                $ceneoXml->saveXmlToDisk($xml, 'error');
            }
        }

        $mailer->addUpdatedProducts($preparedProducts);

        return $preparedProducts;
    }

    /**
     * @param $preparedProducts
     * @param $productsToRemove
     */
    private function removeUnUpdatedProducts(&$preparedProducts, $productsToRemove)
    {
        $removedKeyValue = ArrayHelper::arrayColumn($productsToRemove, 'product_id');

        foreach($preparedProducts as $key => &$productToRemove) {
            if(in_array($productToRemove['product_id'], $removedKeyValue)) {
                unset($preparedProducts[$key]);
            }
        }
    }
}