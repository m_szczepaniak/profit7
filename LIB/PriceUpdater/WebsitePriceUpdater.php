<?php

use LIB\Helpers\ArrayHelper;
use PriceUpdater\Buffor\CyclicBuffor;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');
ini_set('max_execution_time', 1000);

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $aConfig, $pDbMgr;

$sql = "
SELECT WCC2.category_id, DISTINCT count(P.id) as product_amount
		FROM website_ceneo_categories AS WCC2
        JOIN products_extra_categories as PEC2
        ON PEC2.page_id = WCC2.category_id
        JOIN products as P
        ON P.id = PEC2.product_id AND P.published = '1' AND P.packet = '0'
        WHERE
			(P.prod_status = '1' OR P.prod_status = '3')
			AND (P.shipment_time = '0' OR P.shipment_time='1')
			AND WCC2.category_id IN
		(
           SELECT DISTINCT PEC.page_id
           FROM products_extra_categories AS PEC
           JOIN menus_items AS MI
			ON MI.id = PEC.page_id AND MI.priority > 0

			JOIN products as P1
				ON P1.id = PEC.product_id AND P1.published = '1' AND P1.packet = '0'
			WHERE
			(P1.prod_status = '1' OR P1.prod_status = '3')
			AND (P1.shipment_time = '0' OR P1.shipment_time='1')

		   GROUP BY PEC.product_id
		   ORDER BY MI.priority ASC
        )
        GROUP BY WCC2.category_id
        LIMIT 1
";

$result = $pDbMgr->GetAssoc('profit24', $sql);

if (true === empty($result)) {
    exit;
}

$websiteCeneoCategories = ArrayHelper::toKeyValues('category_id', Common::GetAll("SELECT category_id, website_id, product_amount FROM ".$aConfig['tabls']['prefix']."website_ceneo_categories"));;

$table = $aConfig['tabls']['prefix']."website_ceneo_categories";

foreach($result as $category => $amount) {

    $websiteCategoryData = [
        'category_id' => $category,
        'product_amount' => $amount
    ];

    if (true === isset($websiteCeneoCategories[$category])) {
        $pDbMgr->Update('profit24', $table, $websiteCategoryData, 'category_id = '.$category);

    } else {
        $pDbMgr->Insert('profit24', $table, $websiteCategoryData);
    }
}
