<?php

namespace PriceUpdater;

use Common;
use LIB\Helpers\ArrayHelper;
use PriceUpdater\Buffor\CyclicBuffor;
use PriceUpdater\Buffor\UpdateNowBuffor;
use PriceUpdater\Ceneo\CeneoHelper;
use PriceUpdater\Ceneo\CeneoRepository;
use PriceUpdater\PriceModifier\PriceModifierConcrete\AbstractProductModifier;
use PriceUpdater\PriceModifier\PriceModifierConcrete\MinimalPrice;
use PriceUpdater\PriceModifier\PriceModifierConcrete\NewProduct;
use PriceUpdater\PriceModifier\PriceModifierConcrete\PriceModifierConcreteInterface;
use PriceUpdater\PriceModifier\PriceModifierStrategy;
use PriceUpdater\ProductProvider\ProductProvider as ProductProviderInerface;
use DatabaseManager;
use PriceUpdater\Tarrifs\TarrifsUpdater;
use PriceUpdater\Notifier\MailNotifier;

class PriceUpdater
{

    const MAX_UPDATE_AT_NOW = 0;

    /**
     * @var ProductProviderInerface
     */
    private $productProvider;

    /**
     * @var PriceModifierStrategy
     */
    private $priceModifierStrategy;
    /**
     * @var CeneoRepository
     */
    private $ceneoRepository;

    /**
     * @var array
     */
    private $comparedProducts = [];

    /**
     * @var array
     */
    private $priceModifyData = [];

    /**
     * @var int
     */
    private $minutesToStart = 60;

    /**
     * @var DatabaseManager
     */
    private $db;

    /**
     * @var array
     */
    private $config;

    /**
     * @var TarrifsUpdater
     */
    private $tarrifsUpdater;

    /**
     * @var bool
     */
    public $onlyToBuffor = false;

    /**
     * @var array
     */
    private $originalProducts;

    /**
     * @var array
     */
    private $ourStores;

  /**
     * @param DatabaseManager $db
     * @param ProductProviderInerface $productProvider
     * @param PriceModifierStrategy $priceModifierStrategy
     * @param CeneoRepository $ceneoRepository
     * @param $priceModifyData
     */
    public function __construct(
        DatabaseManager $db,
        ProductProviderInerface $productProvider,
        PriceModifierStrategy $priceModifierStrategy,
        CeneoRepository $ceneoRepository,
        TarrifsUpdater $tarrifsUpdater,
        $priceModifyData
    )
    {
        global $aConfig;

        $this->config = $aConfig;
        $this->db = $db;
        $this->productProvider = $productProvider;
        $this->priceModifierStrategy = $priceModifierStrategy;
        $this->ceneoRepository = $ceneoRepository;
        $this->priceModifyData = $priceModifyData;
        $this->tarrifsUpdater = $tarrifsUpdater;

        $ourStores = $this->config['price_updater']['our_book_stores']['other_book_stores'];
        $ourStores[] = $this->config['price_updater']['our_book_stores']['current_book_store'];
        $this->ourStores = $ourStores;
        $this->ignorableShops = $this->findIgnorableShops();
    }

    private function findIgnorableShops()
    {
        $ignorableShops = $this->db->GetRow('profit24', "SELECT `text` FROM products_comparison_sites WHERE shipment_time = 'k' limit 1");

        if (empty($ignorableShops) || empty($ignorableShops['text'])) {
            return [];
        }

        return explode(',', $ignorableShops['text']);
    }

    /**
     * @param $updateNowLimit
     * @param $bufforLimit
     */
    public function compareProducts($updateNowLimit, $bufforLimit)
    {
        $products = $this->productProvider->getPreparedProducts($updateNowLimit, $bufforLimit);
			
        $ceneoHelper = new CeneoHelper($this->ourStores);

        $this->originalProducts = $products;

        $ceneoProducts = $this->ceneoRepository->getProductsByIds($products);
        $nearProducts = [];

        foreach($products as $key => $product) {
		
            if (null === $product['price_brutto']) {
                continue;
            }

            // jezeli niema ofert to moze byc blad i nic nie robimy
            if (false === isset($ceneoProducts[$product['id']]['offers']['offer'])) {
                continue;
            }

            $offersToCompare = $ceneoProducts[$product['id']]['offers']['offer'];

            $unfilteredOffers = $ceneoProducts[$product['id']]['offers']['offer'];
            $this->sortProducts($unfilteredOffers);


            // wyrzuca ksiegarnie z mala iloscia opini itp
            $offersToCompare = $this->prepareBeforeCompare($offersToCompare);

            if(true === empty($offersToCompare)) {
                continue;
            }

            $this->sortProducts($offersToCompare);

            // sprawdzamy czy produkt jest obok naszej ksiegarni
            if (true == $ceneoHelper->ourAreNear($offersToCompare)) {
                $nearProducts[] = $product;
            }

            $minimalFilter = new MinimalPrice();
            $product = $this->applyMinimalFilter($minimalFilter, $product, $offersToCompare);

            // musimy ustalic na naszej ofercie z ceneo nowa cene i przesortowac liste
            if ($product['change_type'] == AbstractProductModifier::MINIMAL) {

              $ourOfferKey = $minimalFilter->getCurrentShopOffer(true);
              $offersToCompare[$ourOfferKey]['Price'] = $product['price_brutto'];

              $this->sortProducts($offersToCompare);
            }

            /** @var PriceModifierConcreteInterface $priceModifier */
            $priceModifier = $this->priceModifierStrategy
                ->setWebsiteConfig($this->priceModifyData)
                ->setOffersToCompare($offersToCompare)
                ->setUnfilteredOffers($unfilteredOffers)
                ->setProduct($product)
                ->getStrategy();

            $product = $priceModifier->applyPriceFilter($this->priceModifyData['m']);
            $this->comparedProducts[$key] = $product;
        }
		
        if (false == empty($nearProducts)){
            $ceneoHelper->sendNearProductsMail($nearProducts);
        }
    }

  /**
   * @param $product
   * @param $offers
   * @return array
   */
    private function applyMinimalFilter(MinimalPrice $minimalFilter, $product, $offers)
    {
        $minimalFilter->setProduct($product);
        $minimalFilter->setOffersToCompare($offers);
        $minimalFilter->setConfig($this->priceModifierStrategy->getConfig());

        if ($minimalFilter->shouldApply()) {
            $product = $minimalFilter->applyPriceFilter($this->priceModifyData['m']);
        }

        return $product;
    }

  /**
   * @param array $offersToCompare
   * @return array
   */
    private function prepareBeforeCompare(array $offersToCompare)
    {
        $minOpinions = $this->priceModifyData['o'];

        $result = [];

        foreach($offersToCompare as $offer) {

            if(in_array($offer['CustName'], $this->ignorableShops)) {
                continue;
            }

            if($offer['ReviewsNumber'] < $minOpinions) {
                continue;
            }

            $result[] = $offer;
        }

        return $result;
    }

    public function updateProducts()
    {
        $comparedProducts = $this->getComparedProducts();

        $productsToUpdate = $this->prepareProductsToUpdate($comparedProducts);

        // zaips produktow do bufora
        $this->tarrifsUpdater->saveProductsToTarrifsBuffor($productsToUpdate['to_buffor']);

        MailNotifier::getInstance()->addBufforProducts($productsToUpdate['to_buffor']);

        //CyclicBuffor::deleteProductsFromBuffor($this->originalProducts);

        CyclicBuffor::changeStatusToUnactive($this->originalProducts);

        MailNotifier::getInstance()->sendReport();
    }

    /**
     * @param array $preparedProducts
     *
     * @return array
     */
    public function prepareProductsToUpdate($preparedProducts)
    {
        $products = [
            'update_now' => [],
            'to_buffor' => [],
        ];

        $oldProducts = ArrayHelper::toKeyValue($this->originalProducts, 'id', 'price_brutto');

        $i = 0;
        foreach($preparedProducts as $product) {
            if (false === $product['has_changed']) {
                continue;
            }

            if ($product['wholesale_price'] < 0.1) {
                continue;
            }

            // obliczanie rabatu
            $discount = Common::formatPrice2((($product['catalogue_price_brutto'] - $product['price_brutto']) / $product['catalogue_price_brutto']) * 100);

            if ($discount < 0){
                continue;
            }

            $fDiscountValue = Common::formatPrice2($product['catalogue_price_brutto'] * ($discount / 100));
            $fNewBrutto = Common::formatPrice2($product['catalogue_price_brutto'] - $fDiscountValue);
            $fNewNetto = Common::formatPrice2($fNewBrutto / (1 + $product['vat']/100));

            if ($fNewBrutto < $product['minimum_price']) {
                continue;
            }

            if ($oldProducts[$product['id']] == $fNewBrutto) {
                continue;
            }

            $newProduct = [
                'product_id' => $product['profit24_id'],
                'discount' => $discount,
                "discount_value" => $fDiscountValue,
                'price_brutto' => $fNewBrutto,
                'price_netto' => $fNewNetto,
                'start_date' => strtotime("+$this->minutesToStart min"),
                "type" => 1,
                "change_type" => $product['change_type'],
                "profit_id" => $product['profit24_id'],
                "isbn_plain" => $product['isbn_plain'],
                "minimum" => $product['minimum_price'],
            ];

            // sprawdza gdzie ma wrzucac produkty
            if ($i < self::MAX_UPDATE_AT_NOW && false === $this->onlyToBuffor && $product['change_type'] != AbstractProductModifier::NEW_PRODUCT) {
                $key = 'update_now';
                $i++;
            } else {
                $key = 'to_buffor';
            }

            if (null === $newProduct['price_brutto']) {
                $newProduct['price_brutto'] = 0;
            }

            $products[$key][] = $newProduct;
        }

        return $products;
    }

    /**
     * @return array
     */
    public function getComparedProducts()
    {
        return $this->comparedProducts;
    }

    /**
     * @param $offersToCompare
     */
    private function sortProducts(&$offersToCompare)
    {
        usort($offersToCompare, function($a, $b) {
            if ((float)$a['Price'] === (float)$b['Price']) {
                $res = 0;
                if (true === in_array($a['CustName'], $this->ourStores)) {
                  $res = 1;
                }
                return $res;
            }
            return (float)$a['Price'] < (float)$b['Price'] ? -1 : 1;
        });
    }
}
