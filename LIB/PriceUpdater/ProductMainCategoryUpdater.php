<?php

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');
ini_set('max_execution_time', 10000000);

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $aConfig, $pDbMgr;

// Aktualizacja glownych kategorii produktow

//$productSql = "
//SELECT DISTINCT P.id, (
//	SELECT MI2.id
//        FROM products_extra_categories as PEC2
//        JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
//        JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
//        WHERE PEC2.product_id = P.id
//        AND IF(P.product_type != 'K', MENUS.game = '1', (MENUS.game = '1' OR MENUS.game = '0'))
//        AND MI2.published = '1'
//        AND MI2.mtype = '0'
//	    AND MI2.priority > 0
//	    AND MI2.parent_id IS NOT NULL
//        ORDER BY MI2.priority ASC LIMIT 1) as page_id
//FROM products AS P
//WHERE P.main_category_id is null
//GROUP BY P.id

$productSql = "
SELECT DISTINCT P.id, (
	SELECT MI2.id
        FROM products_extra_categories as PEC2
        JOIN menus_items AS MI2 ON MI2.id = PEC2.page_id
        JOIN menus AS MENUS ON MI2.menu_id = MENUS.id
        WHERE PEC2.product_id = P.id
        AND IF(P.product_type != 'K', MENUS.product_type = P.product_type, (MENUS.product_type = 'K'))
        AND MI2.published = '1'
        AND MI2.mtype = '0'
	    AND MI2.priority > 0
	    AND MI2.parent_id IS NOT NULL
        ORDER BY MI2.priority ASC LIMIT 1) as page_id
FROM products AS P
GROUP BY P.id
";

$oToClear = $pDbMgr->PlainQuery('profit24', $productSql);

while ($aItem =& $oToClear->fetchRow(DB_FETCHMODE_ASSOC)) {

    $toUpdate = [
        'main_category_id' => $aItem['page_id']
    ];

    $pDbMgr->Update('profit24', "products", $toUpdate, 'id = '.$aItem['id']);
}

