<?php

namespace PriceUpdater\PriceModifier\PriceModifierConcrete;

use Common;

abstract class AbstractProductModifier
{
    const DECREASE = 'decrease';

    const INCREASE = 'increase';

    const MINIMAL = 'minimal';

    const NEW_PRODUCT = 'new_product';

    const CATALOGUE_PRICE_IS_LOWER = 'catalogue_price_is_lower';

    const PRECISSION = 2;

    /**
     * @var bool $ourIsLowestOffer
     */
    private $ourIsLowestOffer = false;

    /**
     * @var array $offersToCompare
     */
    protected $offersToCompare;

    /**
     * @var array $product
     */
    protected $product;

    /**
     * @var array $config
     */
    protected $config;

    /**
     * @var array $websiteConfig
     */
    protected $websiteConfig;

    /**
     * @var array $unfilteredOffers
     */
    protected $unfilteredOffers;

    /**
     * @param array $offersToCompare
     *
     * @return $this
     */
    public function setOffersToCompare(array $offersToCompare)
    {
        $this->offersToCompare = $offersToCompare;

        return $this;
    }

    /**
     * @param array $product
     *
     * @return $this
     */
    public function setProduct(array $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @param $bookStoreName
     *
     * @return bool
     */
    public function isCurrentBookStore($bookStoreName)
    {
        return $bookStoreName == $this->getConfig()['current_book_store'];
    }

    /**
     * @param $bookStoreName
     *
     * @return bool
     */
    public function isOurBookStore($bookStoreName)
    {
        if (in_array($bookStoreName, $this->getConfig()['other_book_stores'])) {
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getOurIsLowestOffer()
    {
        return $this->ourIsLowestOffer;
    }

    /**
     * @param $ourIsLowestOffer
     *
     * @return $this
     */
    public function setOurIsLowestOffer($ourIsLowestOffer)
    {
        $this->ourIsLowestOffer = $ourIsLowestOffer;

        return $this;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return bool
     */
    public function ourPriceIsToHigh()
    {
        $ourOffer = $this->getCurrentShopOffer();

        if (null === $ourOffer) {
            return false;
        }

        if ($ourOffer['Price'] < $this->product['minimum_price']) {
            return true;
        }

        return false;
    }

    /**
     * @return null|array
     */
    public function getCurrentShopOffer($returnKey = false)
    {
        foreach($this->offersToCompare as $key => $offer) {
            if ($offer['CustName'] == $this->config['current_book_store']) {
                if (true === $returnKey) {
                    return $key;
                }
                return $offer;
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function getWebsiteConfig()
    {
      return $this->websiteConfig;
    }

    /**
     * @param $websiteConfig
     * @return $this
     */
    public function setWebsiteConfig($websiteConfig)
    {
      $this->websiteConfig = $websiteConfig;

      return $this;
    }

    /**
     * @return array
     */
    public function getUnfilteredOffers()
    {
      return $this->unfilteredOffers;
    }

  /**
   * @param $unfilteredOffers
   *
   * @return $this
   */
    public function setUnfilteredOffers($unfilteredOffers)
    {
      $this->unfilteredOffers = $unfilteredOffers;

      return $this;
    }

  /**
     * @param $val
     *
     * @return float
     */
    protected function round($val)
    {
        return Common::formatPrice2($val);
    }
}