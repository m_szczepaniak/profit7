<?php

namespace PriceUpdater\PriceModifier\PriceModifierConcrete;

class StandardPrice extends AbstractProductModifier implements PriceModifierConcreteInterface
{

    /**
     * @return bool
     */
    public function shouldApply()
    {
        return $this->product['has_changed'] === false;
    }

    /**
     * @param $priceModifyValue
     *
     * @return array
     */
    public function applyPriceFilter($priceModifyValue)
    {
        return $this->product;
    }
}