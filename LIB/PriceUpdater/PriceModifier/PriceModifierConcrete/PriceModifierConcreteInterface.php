<?php

namespace PriceUpdater\PriceModifier\PriceModifierConcrete;

interface PriceModifierConcreteInterface
{
    /**
     * @return bool
     */
    public function shouldApply();

    /**
     * @param $priceModifyValue
     *
     * @return mixed
     */
    public function applyPriceFilter($priceModifyValue);

    /**
     * @param array $offersToCompare
     *
     * @return PriceModifierConcreteInterface
     */
    public function setOffersToCompare(array $offersToCompare);

    /**
     * @param array $product
     *
     * @return mixed
     */
    public function setProduct(array $product);

    /**
     * @param $getConfig
     */
    public function setConfig($getConfig);

    /**
     * @return array
     */
    public function getConfig();

    /**
     * @return array
     */
    public function getWebsiteConfig();

    /**
     * @param $websiteConfig
     * @return $this
     */
    public function setWebsiteConfig($websiteConfig);

    /**
     * @return array
     */
    public function getUnfilteredOffers();


    /**
     * @param $unfilteredOffers
     *
     * @return $this
     */
    public function setUnfilteredOffers($unfilteredOffers);

}