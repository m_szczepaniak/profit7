<?php

namespace PriceUpdater\PriceModifier\PriceModifierConcrete;

use Common;

class NewProduct
{
  /**
   * @param $product
   * @param $percentagePriceModifier
   * @return mixed
   */
    public function applyPriceFilter($product, $percentagePriceModifier)
    {
        $pricePercentageValue = Common::formatPrice2(($product['catalogue_price_brutto'] * ($percentagePriceModifier / 100)));

        $product['price_brutto'] = Common::formatPrice2($product['catalogue_price_brutto'] - $pricePercentageValue) ;

        $product['has_changed'] = true;
        $product['change_type'] = AbstractProductModifier::NEW_PRODUCT;

        return $product;
    }
}