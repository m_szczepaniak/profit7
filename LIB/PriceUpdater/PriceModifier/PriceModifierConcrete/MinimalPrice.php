<?php

namespace PriceUpdater\PriceModifier\PriceModifierConcrete;

class MinimalPrice extends AbstractProductModifier implements PriceModifierConcreteInterface
{
    /**
     * @return bool
     */
    public function shouldApply()
    {
        if (false === $this->ourPriceIsToHigh()) {
            return false;
        }

        return true;
    }

    /**
     * @param $priceModifyValue
     *
     * @return array
     */
    public function applyPriceFilter($priceModifyValue)
    {
        $newPriceBrutto = $this->round($this->product['minimum_price']);

        $this->product['price_brutto'] = $newPriceBrutto;
        $this->product['change_type'] = self::MINIMAL;

        $this->product['has_changed'] = true;

        return $this->product;
    }
}