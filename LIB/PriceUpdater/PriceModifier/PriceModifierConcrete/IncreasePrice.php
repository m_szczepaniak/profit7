<?php

namespace PriceUpdater\PriceModifier\PriceModifierConcrete;

class IncreasePrice extends AbstractProductModifier implements PriceModifierConcreteInterface
{

    /**
     * @return bool
     */
    public function shouldApply()
    {
        // powiekszamy cene tylko w przypadku kiedy nasza jest najnizsza
        if (true === $this->isCurrentBookStore($this->offersToCompare[0]['CustName'])) {

            // sprawdza czy druga oferta ktora bedziemy porownywali jest jedna z naszych ksiegarn
//            if (true === $this->isOurBookStore($this->offersToCompare[1]['CustName'])) {
//                return false;
//            }

            $otherLatestMinimalOffer = $this->offersToCompare[1];

            if (null !== $otherLatestMinimalOffer) {
                // jezeli cena bedzie rowna cenie konkurencji to nic nie robimy ze zwiekszaniem
                if ($otherLatestMinimalOffer['Price'] == $this->product['price_brutto']) {
                  return false;
                }
            }

            $this->setOurIsLowestOffer(true);

            return true;
        }

        return false;
    }

    /**
     * @param $priceModifyValue
     *
     * @return array
     */
    public function applyPriceFilter($priceModifyValue)
    {
        $otherLatestMinimalOffer = $this->offersToCompare[1];

        // warunek jest spelniony gdy produkt jest oferowany przez inne ksiegarnie
        if (null !== $otherLatestMinimalOffer) {

            $newPriceBrutto = $this->round($otherLatestMinimalOffer['Price'] - $priceModifyValue);

            if ($this->round($this->product['price_brutto']) == $newPriceBrutto) {
                return $this->product;
            }

            $this->product['price_brutto'] = $newPriceBrutto;

            if ($this->product['price_brutto'] < $this->product['minimum_price']) {
                return $this->product;
            }

            $this->product['change_type'] = self::INCREASE;
            $this->product['has_changed'] = true;
        }
        // jezeli jestesmy sami
        // TODO Co robimy kiedy niema innych ksiegarn oprocz przefiltrowanych z powodu opinii
        else{

          if ($this->getUnfilteredOffers()[0]['CustName'] == $this->config['current_book_store']) {
              $pricePercentageValue = $this->round($this->product['catalogue_price_brutto'] * ($this->getWebsiteConfig()['p'] / 100));

              $this->product['price_brutto'] = $this->round(($this->product['catalogue_price_brutto'] - $pricePercentageValue)) ;
              $this->product['change_type'] = self::INCREASE;
              $this->product['has_changed'] = true;
          }
        }

      // TODO dodac do logow
        // Jeżeli cena która mamy podniesc jest wyzsza od ceny katalogowej to nic nie robimy
        if ($this->product['price_brutto'] > $this->round($this->product['catalogue_price_brutto'])) {
            $this->product['change_type'] = self::CATALOGUE_PRICE_IS_LOWER;
            $this->product['has_changed'] = false;
        }

        return $this->product;
    }
}