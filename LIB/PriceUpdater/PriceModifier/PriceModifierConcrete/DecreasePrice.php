<?php

namespace PriceUpdater\PriceModifier\PriceModifierConcrete;

class DecreasePrice extends AbstractProductModifier implements PriceModifierConcreteInterface
{

    private $minimumPriceDiff = 0.01;

    /**
     * @return bool
     */
    public function shouldApply()
    {
        return true;
    }

    private function preparePriceBrutto($offer, $minimalPrice, $priceModifyValue)
    {
        if ($offer['Price'] == $minimalPrice) {
          return $minimalPrice;
        }

        $minimalPlusModifier = $minimalPrice + $priceModifyValue;

        if ($offer['Price'] < $minimalPlusModifier || $offer['Price'] == $minimalPlusModifier) {
          return $offer['Price'] - $this->minimumPriceDiff;
        }

        return $offer['Price'] - $this->minimumPriceDiff;
    }

    /**
     * @return mixed
     */
    public function applyPriceFilter($priceModifyValue)
    {
        /** Maksymalna cena do ktorej mozemy obnizyc (minimalna cena + narzut) bo musi nam sie oplacic */
        $acceptablePrice = $this->product['minimum_price'];

        foreach($this->offersToCompare as $offer) {

            if (false === $this->canDecreasePrice($offer, $acceptablePrice)) {
                continue;
            }

            if (true === $this->isOurBookStore($offer['CustName']) || true === $this->isCurrentBookStore($offer['CustName'])) {
                continue;
            }

            $this->product['price_brutto'] = $this->round($this->preparePriceBrutto($offer, $acceptablePrice, $priceModifyValue));

            $this->product['has_changed']  = true;
            $this->product['change_type']  = self::DECREASE;

            return $this->product;
        }

        return $this->product;
    }

    /**
     * @param $offer
     * @param $acceptablePrice
     *
     * @return bool
     */
    private function canDecreasePrice($offer, $acceptablePrice)
    {
        if ($offer['Price'] >= $acceptablePrice) {
            return true;
        }

        return false;
    }
}