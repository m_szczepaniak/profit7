<?php

namespace PriceUpdater\PriceModifier;

use PriceUpdater\PriceModifier\PriceModifierConcrete;
use PriceUpdater\PriceModifier\PriceModifierConcrete\AbstractProductModifier;
use PriceUpdater\PriceModifier\PriceModifierConcrete\PriceModifierConcreteInterface;

class PriceModifierStrategy extends AbstractProductModifier
{
    private $availableStrategies = [
        '\PriceUpdater\PriceModifier\PriceModifierConcrete\IncreasePrice',
        '\PriceUpdater\PriceModifier\PriceModifierConcrete\DecreasePrice',
        '\PriceUpdater\PriceModifier\PriceModifierConcrete\StandardPrice',
    ];

    public function __construct($bookStoreConfig)
    {
        $this->setConfig($bookStoreConfig);
    }

    /**
     * @return PriceModifierConcreteInterface
     */
    public function getStrategy()
    {
        $concretModifier = null;

        foreach($this->availableStrategies as $priceStrategy) {

            /** @var PriceModifierConcreteInterface $concrete */
            $concrete = new $priceStrategy();
            $concrete->setProduct($this->product);
            $concrete->setOffersToCompare($this->offersToCompare);
            $concrete->setConfig($this->getConfig());
            $concrete->setUnfilteredOffers($this->getUnfilteredOffers());
            $concrete->setWebsiteConfig($this->getWebsiteConfig());

            if (true === $concrete->shouldApply()) {
                return $concrete;
            }
        }
    }
}