<?php

use Common;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');
ini_set('max_execution_time', 1000);

$aConfig['config']['project_dir'] = 'LIB/PriceUpdater';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $aConfig, $pDbMgr;

$bookstorecode = $aConfig['common']['bookstore_code'];

$sql = "
SELECT PT.*, P.price_brutto AS catalogue_price_brutto, P.vat
FROM products_tarrifs AS PT
JOIN products AS P ON PT.product_id = P.id
WHERE PT.type = 1
";

$result = $pDbMgr->GetAll($bookstorecode, $sql);

$res = [];
$string = '';
$i = 0;
foreach($result as $element) {
    $before = $element;
    $fDiscount = $element['discount'];

    $fDiscountValue = Common::formatPrice2($element['catalogue_price_brutto'] * ($fDiscount / 100));
    $fNewBrutto = $element['catalogue_price_brutto'] - $fDiscountValue;
    $fNewNetto = Common::formatPrice2($fNewBrutto / (1 + $element['vat']/100));


    $values = [];

    $values['discount'] = $fDiscount;
    $values['discount_value'] = $fDiscountValue;
    $values['price_brutto'] = $fNewBrutto;
    $values['price_netto'] = $fNewNetto;
//    $values['before'] = $before[$element['id']];

    $id = $element['id'];

    $beforeFloat = (string)$before['price_brutto'];
    $afterFloat = (string)$values['price_brutto'];

    if ($beforeFloat != $afterFloat) {
        $i++;
        $string .= $i.' - '.$id.'  '.$beforeFloat.' '.$afterFloat.PHP_EOL;
    }

    $pDbMgr->Update($bookstorecode, 'products_tarrifs', $values, 'id = '.$id. ' LIMIT 1');

    $res[] = $values;
}

file_put_contents('tarrifs_update_log.txt', $string);


