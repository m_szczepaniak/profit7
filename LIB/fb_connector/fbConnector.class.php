<?php
/**
 * Klasa do obsługi integracji z facebookiem, na ten moment:
 *  rejestracja, logowanie, wylogowanie, odłączenie konta
 *  Wymaga:
 *    konfiguracji z $aConfig,
 *    objektu bazy danych w global $pDbMgr,
 *    biblioteki facebooka w wersji 3.2.2
 *    modules/m_konta/client/UserLogin.class.php
 *    modules/m_konta/client/User.class.php
 *    z /inc/functions.ini.php encodePasswd i rc4
 *  UWAGA: 
 *    klasa facebooka inicjalizuje sesje, więc powinna być używana 
 *    po wystartowaniu sesji w systemie
 *    
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-05-09 
 * @copyrights Marcin Chudy - Profit24.pl
 */
final class fbConnector {
  /**
   * Przechowuje instancję klasy Singleton
   *
   * @var object
   * @access private
   */
  private static $oInstance = false;
    
  private $_oFb;
  
  const WEBSITE = 'profit24';
  const WEBSITE_ID = 1;
  const LOGIN_URL = 'moje-konto/fb-logowanie.html';
  
  private $sLoginURL;
  
  /**
   * Zwraca instancję obiektu Singleton
   *
   * @return Singleton
   * @access public
   * @static
   */
  public static function getInstance()
  {
    if( self::$oInstance == false ) {
        self::$oInstance = new fbConnector();
    }
    return self::$oInstance;
  }// end of getInstance() method
  
  /**
   * Prywatny konstruktor klasy
   * 
   * @global array $aConfig
   */
  private function __construct() {
    global $aConfig;
    
    $this->sLoginURL = $aConfig['common']['client_base_url_https'].'moje-konto/fb-logowanie';
    
    require_once 'php-sdk/src/facebook.php';
    
    $config = array(
        'appId' => $aConfig['facebook']['appId'],
        'secret' => $aConfig['facebook']['secret']
    );
    $this->_oFb = new Facebook($config);
  }// end of __construct()
  

  /**
   * Metoda loguje uzytkownika
   * 
   * @param type $iUserId
   * @return boolean
   */
  public function doTryLogin() {
    
    $iFBUserId = $this->getFBUId();
    if ($iFBUserId > 0) {
      $iUserId = $this->getUserIdByFBUId($iFBUserId);
      if ($iUserId > 0) {
        include_once('modules/m_konta/client/UserLogin.class.php');
        $oUserLogin = new UserLogin();
        if ($oUserLogin->doLoginById($iUserId, 1) <= 0) {
          return false;
        } else {
          return true;
        }
      } else {
        return false;
      }
    }
    return false;
  }// end of doTryLogin() method
  
  
  /**
   * Metoda pobiera dane uzytkownika
   * 
   * @global array $aConfig
   * @return boolean|array
   */
  public function getUserData() {
    global $aConfig;
    
    try {
      $userData = $this->_oFb->api('/me','GET');
      if (!empty($userData) 
          && is_array($userData) 
          && preg_match($aConfig['class']['form']['email']['pcre'], $userData['email'])
          && $userData['verified'] == '1') {
        return $userData;
      } else {
        return false;
      }
    } catch(FacebookApiException $e) {
      // If the user is logged out, you can have a 
      // user ID even though the access token is invalid.
      // In this case, we'll get an exception, so we'll
      // just ask the user to login again here.
      return false;
    }   
  }// end of getUserData() method
  
  
  /**
   * Metoda sprawdza, czy uzytkownik jest zalogowany na facebook
   * 
   * @return integer
   */
  public function getFBUId() {
    return $this->_oFb->getUser();
  }// end of getFBUId() method
  
  
  /**
   * Metoda pobiera adres logowania
   * 
   * @param string $sRedirectURI adres przekierowania po zalogowaniu
   * @return string adres logowania na fb
   */
  public function getLoginURL() {
    global $aConfig;
    
    $aLoginParamas = array(
        'scope' => 'email',
        'redirect_uri' => $aConfig['common']['client_base_url_https'].self::LOGIN_URL
    );
    return $this->_oFb->getLoginUrl($aLoginParamas);
  }// end of getLoginURL() method
  
  
  /**
   * Metoda NISZCZY SESJĘ
   * 
   * @param type $sRedirectURI
   */
  public function doLogout() {
    $this->_oFb->destroySession();
  }// end of doLogout() method
  
  
  /**
   * Metoda sprawdza czy istnieje konto uzytkownika o podanym adresie email
   * 
   * @global object $pDbMgr
   * @global array $aConfig
   * @param string $sEmail
   * @return int
   */
  private function _getUserIdByEmail($sEmail) {
    global $pDbMgr;
    
    $sSql = "SELECT id 
             FROM users_accounts 
             WHERE email='".$sEmail."'
                  AND website_id=".self::WEBSITE_ID;
    return $pDbMgr->GetOne(self::WEBSITE, $sSql);
  }// end of _getUserIdByEmail() method
  
  
  /**
   * Metoda łączy konta
   * 
   * @global array $aConfig
   * @global object $pDbMgr
   * @param int $iFbUId
   * @param array $aUData
   * @return boolean
   */
  public function connectAccounts($iFbUId, $aUData, $bLoggedIn = false) {
    global $pDbMgr;
    
    if ($iFbUId > 0 && !empty($aUData)) {
      $iUId = $this->_getUserIdByEmail($aUData['email']);
      if ($iUId > 0) {
        // istnieje juz uzytkownik, dopisujemy id facebooka
        $aValues = array(
            'fb_uid' => $iFbUId,
            'deleted' => '0',
            'active' => '1',
            'privacy_agreement' => '1',
            'regulations_agreement' => '1',
            'registred' => '1'
        );
        if ($pDbMgr->Update(self::WEBSITE, 'users_accounts', $aValues, ' id = '.$iUId) !== false) {
          if ($bLoggedIn === false) {
            if ($this->_doLoginUser($iUId) === true) {
              return 1; // konta zostały połączone
            } else {
              return -4;
            }
          } else {
            return 1; // konta zostały połączone
          }
        } else {
          return -1;
        }
      } else {
        // tworzymy nowe konto, allegro nie ma akceptacji regulaminu to my też
        $aValues = array(
            'fb_uid' => $iFbUId,
            'website_id' => self::WEBSITE_ID,
            'passwd' => '',//$this->_getRandomPasswd()
            'email' => $aUData['email'],
            'regulations_agreement' => '1',
            'privacy_agreement' => '1',
            'active' => '1',
            'discount' => '0.00',
            'created' => 'NOW()',
            'registred' => '1'
        );
        $iUId = $pDbMgr->Insert(self::WEBSITE, 'users_accounts', $aValues);
        
        // przepisanie startych zamówień
        include_once('modules/m_konta/client/User.class.php');
        $aTMP = array();
        $oUser = new User('', '', $aTMP, '');
        if ($oUser->addOldOrdersToAccount($aUData['email'], $iUId) === false) {
          return -4;
        }
        
        if ($iUId > 0) {
          if ($bLoggedIn === false) {
            if ($this->_doLoginUser($iUId) === true) {
              return 2;// zarejestrowany i zalogowany
            } else {
              return -4;
            }
          } else {
            return 3; // zarejestrowamy
          }
        } else {
          return -2;
        }
      }
    } else {
      return -3;
    }
  }// end of connectAccounts() method
  
  
  /**
   * Metoda rozłącza konta
   * 
   * @global object $pDbMgr
   * @param type $iUId
   * @return type
   */
  public function disconnectAccounts($iUId) {
    global $pDbMgr;
    
    $aValues = array(
        'fb_uid' => 'NULL'
    );
    return ($pDbMgr->Update(self::WEBSITE, 'users_accounts', $aValues, ' id = '.$iUId) !== false);
  }// end of disconnectAccounts() method
  
  
  /**
   * Meotda loguje użytkownika
   * 
   * @param int $iUId
   * @return boolean
   */
  private function _doLoginUser($iUId) {
    
    include_once('modules/m_konta/client/UserLogin.class.php');
    $oUserLogin = new UserLogin();
    if ($oUserLogin->doLoginById($iUId, 1) > 0) {
      return true;
    } else {
      return -4;
    }
  }// end of _doLoginUser() method
  
  
  /**
   * Metoda pobiera losowe hasło
   * 
   * @return type
   *
  public function _getRandomPasswd() {
    $iLength = 8;
    
    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
    $pass = array();
    $alphaLength = strlen($alphabet) - 1;
    for ($i = 0; $i < $iLength; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return encodePasswd(implode($pass));
  }// end of _getRandomPasswd() method
  */
  
  /**
   * Metoda sprawdza czy użytkownik o podanym id facebook istnieje
   * 
   * @global object $pDbMgr
   * @param int $iFUId
   * @return int id user_id
   */
  public function getUserIdByFBUId($iFUId) {
    global $pDbMgr, $aConfig;
    
    $sSql = "SELECT id 
             FROM users_accounts 
             WHERE fb_uid=".$iFUId."
                  AND website_id=".$aConfig['profit24_website_id'];
    return $pDbMgr->GetOne(self::WEBSITE, $sSql);
  }// end of getUserIdByFBUId() method
}// end of class
?>