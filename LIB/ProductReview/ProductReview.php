<?php
namespace ProductReview;

use DatabaseManager;

/**
 * Created by PhpStorm.
 * User: arek
 * Date: 29.11.17
 * Time: 15:33
 */
class ProductReview
{


    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * ProductReview constructor.
     * @param DatabaseManager $pDbMgr
     */
    public function __construct(DatabaseManager $pDbMgr)
    {

        $this->pDbMgr = $pDbMgr;
    }

    /**
     * @param $productsReviewsUsersId
     * @return bool
     * @throws \Exception
     */
    public function recountProductsReviewsUser($productsReviewsUsersId)
    {
        $sSql = 'SELECT count(1) AS reviews_count, (SUM(vote_like) - SUM(vote_unlike)) AS vote_sum 
                 FROM products_reviews
                 WHERE products_reviews_users_id = ' . $productsReviewsUsersId.' AND active = "1"  ';
        $productsReviewsUsers = $this->pDbMgr->GetRow('profit24', $sSql);
        $values = [
            'reviews_count' => $productsReviewsUsers['reviews_count'],
            'vote_sum' => $productsReviewsUsers['vote_sum'],
        ];
        if (false === $this->pDbMgr->Update('profit24', 'products_reviews_users', $values, ' id = ' . $productsReviewsUsersId)) {
            throw new \Exception('Wystąpił błąd podczas przeliczenia products_reviews_users');
        } else {
            return true;
        }
    }


}