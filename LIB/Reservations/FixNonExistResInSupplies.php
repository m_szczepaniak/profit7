<?php

namespace Reservations;

use LIB\communicator\sources\Internal\streamsoft\DataProvider;

class FixNonExistResInSupplies
{
    private $dataProvider;
    private $pDbMgr;

    public function __construct(\DatabaseManager $pDbMgr)
    {
        $this->dataProvider = new DataProvider($pDbMgr);
        $this->pDbMgr = $pDbMgr;
    }

    public function fixProducts($products, $source)
    {
        if (empty($products)) {
            return false;
        }

        foreach($products as $productId) {

            $resses = $this->getResByProdId($productId, $source);

            // jezeli istnieja rezerwacje to nic nie robimy
            if (false == empty($resses)) {
                continue;
            }

            $this->resetSuppliesReservations($productId, $source);
        }
    }

    private function getResByProdId($productId, $source)
    {
        $sql = "SELECT * from products_stock_reservations WHERE products_stock_id = $productId AND source_id = $source";

        return $this->pDbMgr->GetAll('profit24', $sql);
    }

    private function resetSuppliesReservations($productId, $source)
    {
        $sql = "SELECT * FROM products_stock_supplies WHERE product_id = $productId AND source = $source";
        $sql2 = "UPDATE products_stock_supplies SET reservation = 0 WHERE id = %d";

        $res = $this->pDbMgr->GetAll('profit24', $sql);

        if (!empty($res)) {
            foreach($res as $supply) {
                $this->pDbMgr->Query('profit24', sprintf($sql2, $supply['id']));
            }
        }
    }
}