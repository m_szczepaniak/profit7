<?php

namespace Reservations;

use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;
use LIB\Helpers\ArrayHelper;
use LIB\Supplies\ProductStockService;
use omniaCMS\lib\Orders\orderItemReservation;
use omniaCMS\lib\Products\ProductsStockReservations;

class ReservationRenewer
{
    const LOG_FILE = '';

    /**
     * @var ReservationManager
     */
    private $reservationManager;

    /**
     * @var DataProvider
     */
    private $dataProvider;

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct(\DatabaseManager $pDbMgr, ReservationManager $reservationManager, DataProvider $dataProvider)
    {
        $this->reservationManager = $reservationManager;
        $this->dataProvider = $dataProvider;
        $this->pDbMgr = $pDbMgr;
    }

    public function renewReservations()
    {
//        $ordersIds = [
//            [
//                'id' => 238,
//                'order_status' => 3
//            ]
//        ];
        $ordersIds = $this->dataProvider->getAllOrdersIdsToRecreateReservations();

        if (empty($ordersIds)) {
            return;
        }

        $i = 0;
        foreach($ordersIds as $order)
        {
            $this->pDbMgr->BeginTransaction('profit24');
            $orderId = $order['id'];
            try {

                $createresRes = $this->reservationManager->createLocalReservationsByOrderId($orderId);
                if (false == $createresRes){
                    continue;
                }

                $this->reservationManager->createStreamsoftReservation([], $orderId, true);
                
                $anyReservations = $this->dataProvider->getReservationsByOrderId($orderId, 10);

                if (true == empty($anyReservations) && $this->reservationManager->isPartial() == false){
                    $this->reservationManager->removeOrder(null);
                }

                if(null !== $this->reservationManager->getErrorMessage()) {
                    $this->putNotification($this->reservationManager->getErrorMessage(), $orderId, 'CZESCIOWE');
                }

                $this->pDbMgr->CommitTransaction('profit24');
                echo "Przetworzono zamowienie: ".$orderId.PHP_EOL;

            } catch (\Exception $e) {
                $this->pDbMgr->RollbackTransaction('profit24');
                $this->reservationManager->rollbackChanges();
                $this->putNotification($e->getMessage(), $orderId);
                echo "Blad w zamowieniu: ".$orderId.PHP_EOL;
            }
            $i++;
            echo "obrobiono $i zamowien";
        }
    }

    private function putNotification($errorMessage, $orderId, $type = 'BLAD')
    {
        $now = new \DateTime();
        $message = $now->format('Y-m-d H:i:s').' - '.$orderId.' - '.$type.' - '.$errorMessage.PHP_EOL;

        file_put_contents('renew_log.txt', $message, FILE_APPEND);
    }

    public function repairSingleOrderReservations($orderId)
    {
        $order = $this->dataProvider->getOrder($orderId, ['O.id', 'O.website_id', 'P.id as product_id', 'O.order_number', 'O.order_number_iteration'], false, true);

        $oldReservations = $this->dataProvider->getOldReservationsToRepair($order['id']);

        if (empty($oldReservations)){
            return false;
        }

        $oldReservationsIds = ArrayHelper::arrayColumn($oldReservations, 'id');
        $this->pDbMgr->BeginTransaction('profit24');
        try {
            $this->dataProvider->deleteReservationByOrderIdAndItems($order['id'], $oldReservationsIds);
            $this->reservationManager->removeOrder($order);
            $this->removeOldResQuantityFromSupplies($oldReservations);
            $createresRes = $this->createRepairedLocalReservations($order['id']);

            $reservationResult = $this->reservationManager->createStreamsoftReservation([], $order['id'], false, true);
        } catch(\Exception $e) {
            $this->pDbMgr->RollbackTransaction('profit24');
            $this->reservationManager->rollbackChanges();
            return $e->getMessage();
        }
//
        if (false == $createresRes || false == $reservationResult){
            $this->pDbMgr->RollbackTransaction('profit24');
            return false;
        }

        $this->pDbMgr->CommitTransaction('profit24');


        return true;
    }

    private function removeOldResQuantityFromSupplies($oldReservations)
    {
        foreach($oldReservations as $reservation){
            $supplies = $this->dataProvider->getSuppliesByProductIdAndSource($reservation['products_stock_id'], $reservation['source_id']);
            $supplies = $this->resetSupplies($supplies);
            $this->reUpdateSupplices($supplies, $reservation['products_stock_id'], $reservation['source_id']);
        }
    }

    private function reUpdateSupplices($productStockSupplies, $productId, $source)
    {
        $afterReservationQuantity = $this->dataProvider->getReservationsSumBySourceAndProductId($productId, $source);
        $afterReservationQuantity = (int)$afterReservationQuantity['sum'];

        $newStockSupplies = [];

        $i = 0;
        foreach($productStockSupplies as $productStockSupply) {

            // wychodzimy jak nie trzeba jzu nic zmieniac
            if ($afterReservationQuantity == 0 && $i > 0){
                $newStockSupplies[] = $productStockSupply;
                continue;
            }

            $availableToReserve = ($productStockSupply['available_quantity'] - $afterReservationQuantity < 0) ? $productStockSupply['available_quantity'] : $afterReservationQuantity;

            $productStockSupply['reservation'] += $availableToReserve;
            $productStockSupply['available_quantity'] -= $availableToReserve;
            $productStockSupply['reservated'] = $availableToReserve;
            $productStockSupply['has_changed'] = true;

            $afterReservationQuantity -= $availableToReserve;

            $newStockSupplies[] = $productStockSupply;

            $i++;

            if ($afterReservationQuantity == 0) {
                $allHasBeenUpdated = true;
            }
        }

        if (empty($newStockSupplies)){
            return false;
        }

        foreach($newStockSupplies as $newStockSupply){
            $this->dataProvider->updateProductStockSupply($newStockSupply, ['reservation']);
        }

        $productStockSupplies = $this->dataProvider->getSuppliesByProductIdAndSource($productId, $source);
        $productStockService = new ProductStockService($this->pDbMgr);
        $productStockService->updateProductStock($productStockSupplies);
    }

    public function resetSupplies($supplies)
    {
        $newSupplies = [];

        foreach($supplies as $supply){
            $supply['reservation'] = 0;
            $supply['available_quantity'] = $supply['quantity'];
            $newSupplies[] = $supply;
        }

        return $newSupplies;
    }

    private function createRepairedLocalReservations($orderId)
    {
        $changedOrderItems = $this->dataProvider->getOrdersItemsForReparation($orderId);

        // jezeli niema rezerwacji do stworzenia do nic nie robimy
        if(true === empty($changedOrderItems)){
            return false;
        }

        $productsStokReservations = new ProductsStockReservations($this->pDbMgr);

        foreach($changedOrderItems as $item) {

            // ustawiamy status rezerwazcjom na 0, nie mozemy podac pustej tablicy, skrypt sprawdza status starej rezerwacji
            $nonChangedOrderItem = $item;
            $nonChangedOrderItem['status'] = 0;

            $orderItemReservation = new orderItemReservation($productsStokReservations);
            $orderItemReservation->updateValues($item, $nonChangedOrderItem);
        }

        return true;
    }
}