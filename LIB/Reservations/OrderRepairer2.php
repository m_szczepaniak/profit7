<?php

namespace Reservations;

use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\communicator\sources\Internal\streamsoft\Order;
use LIB\communicator\sources\Internal\streamsoft\ReservationManager;

class OrderRepairer2
{
    /** @var \DatabaseManager  */
    private $pDbMgr;

    /**
     * @var Order
     */
    private $orderClass;

    /**
     * @var void
     */
    private $order;

    public function __construct($orderNumber, Order $orderClass)
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;
        $this->order = $this->prepareOrder($orderNumber);
        $this->orderClass = $orderClass;
    }


    public function fixOrder()
    {
        $order = $this->getOrder();
        $ordersToRemove = [];

        if (isset($order['latest_iteration'])) {

            $ordersToRemove[] = $order['order_number'];

            for($i = 1; $i <= $order['latest_iteration']; $i++) {

                $ordersToRemove[] = $order['order_number'].$i;
            }
        } else {
            $ordersToRemove[] = $order['order_number'];
        }

//        foreach($ordersToRemove as $ordNbr) {
//
//            $rem = $this->orderClass->cancelOrder($ordNbr);
//
//            if ($rem->getErrorCode() == 0) {
//                var_dump('Poszlo: '. $ordNbr);
//            } else {
//                var_dump($rem->getErrorMessage().': '.$ordNbr);
//            }
//        }

        $reservationRenewer = new ReservationRenewer2($this->pDbMgr, new ReservationManager($this->pDbMgr), new DataProvider($this->pDbMgr));

        $result = $reservationRenewer->repairSingleOrderReservations($order['id']);

//        $res = $this->pDbMgr->Query('profit24', "UPDATE orders SET is_streamsoft_canceled = 1 WHERE order_number = ".$this->order['realOrderNumber'].' LIMIT 1');
        $res = $this->pDbMgr->Query('profit24', sprintf("UPDATE orders SET is_streamsoft_canceled = 1 WHERE order_number = '%s' LIMIT 1", $this->order['realOrderNumber']));
        $dupa = '';
    }

    private function getOrder()
    {
        $order = $this->getOrderData($this->order['realOrderNumber'], ['O.id', 'O.website_id', 'O.order_number', 'O.order_number_iteration'], false, true);

        if (!isset($order['id'])) {
            throw new \Exception('brak zmaowienia: '.$this->order['realOrderNumber']);
        }

        return $order;
    }

    private function prepareOrder($orderNumber)
    {
        $array = [
            'originalNumber' => $orderNumber,
            'realOrderNumber' => null,
            'hasIteration' => false,
            'iteration' => 0
        ];

        if (strlen($orderNumber) > 12) {
            $array['hasIteration'] = true;
            $array['realOrderNumber'] = substr($orderNumber, 0, 12);
            $array['iteration'] = substr($orderNumber, 12);
        }
        else if(strlen($orderNumber) < 12) {
            throw new \Exception('Cos za krotki numer zamowienia');
        }
        else {
            $array['realOrderNumber'] = $orderNumber;
        }

        return $array;
    }

    public function getOrderData($iOid, $columns, $onlyOrderData = false, $streamsoftOrder = false) {

        $implodedColumns = implode(',', $columns);

//        $sSql =  'SELECT '.$implodedColumns.'
//             FROM orders AS O
//             WHERE ( (O.order_status = '."'4'".' AND O.erp_export IS NOT NULL) OR O.order_status = '."'5'".')
//             AND O.order_number = '."'".$iOid."'";

        $sSql =  'SELECT '.$implodedColumns.'
             FROM orders AS O
             WHERE O.order_number = '."'".$iOid."'";

        if (true == $onlyOrderData) {
            $sSql =  'SELECT '.$implodedColumns.'
             FROM orders AS O
             WHERE O.id = '.$iOid;
        }

        $result = $this->pDbMgr->GetRow('profit24', $sSql);

        if ($streamsoftOrder == true) {
            $orderNumber = $result['order_number'];
            if (null != $result['order_number_iteration']){
                $orderNumber = $orderNumber.''.$result['order_number_iteration'];
                $result['latest_iteration'] = $result['order_number_iteration'];
            }
            $result['streamsoft_order_number'] = $orderNumber;
        }

        return $result;
    }
}