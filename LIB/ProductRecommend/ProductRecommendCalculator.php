<?php

namespace ProductRecommend;

use DatabaseManager;

class ProductRecommendCalculator
{
    /**
     * @var DatabaseManager
     */
    private $db;

    /**
     * @var array
     */
    private $config;

    /**
     * @var array
     */
    private $recommendedOrdersPriorities;

    const ORDERS_DAYS = 60;

    const MIN_PRIORITY = 5;

    const DUPLICATE_MULTIPLY = 1;

    public function __construct()
    {
        global $pDbMgr, $aConfig;

        $this->db = $pDbMgr;
        $this->config = $aConfig;
        $this->recommendedOrdersPriorities = [];
    }

    public function calculateProductsRecommends() {

        echo "Pobieram zamówienia \r\n";
        $orders = $this->getOrdersProducts();

        echo "Obliczam priorytety dla ".count($orders)." zamówień \r\n";
        $recommendedProductsPriorities = $this->calculatePriorities($orders);

        echo "Pomnożenie priorytetów powtórzonych zamówień przez priorytety produktów, liczba dospasowań ".count($recommendedProductsPriorities)." \r\n";
        $recommendedProductsPriorities = $this->multiplePriorities($recommendedProductsPriorities);

        echo "Czyszczę produkty \r\n";
        $this->cleanProducts();

        echo "Dodawanie rekomendacji ".count($recommendedProductsPriorities)." \r\n";
        $this->insertRecommends($recommendedProductsPriorities);
    }

    /**
     * Obliczenie priorytetów
     * @param $orders array Zamówieina
     * @return array Połączone produkty razem z priorytetem
     */
    protected function calculatePriorities($orders) {
        $aOrders = $orders;
        $recommendedProductsPriorities = [];

        foreach ($aOrders as $orderId => $aProducts) {
            if (count($aProducts) < 2) {
                unset($aOrders[$orderId]);
            } else {
                $this->addOrderPriority($aProducts);    // Dodanie priorytetow powtórzonych zamówień do tablicy z priorytetami
                $priority = $this->getPriority($aProducts);
                foreach ($aProducts as $productId) {
                    foreach ($aProducts as $connectedId) {
                        if ($productId != $connectedId) {
                            if ($priority > 0 && $productId != "" && $connectedId != "") {
                                if (!isset($recommendedProductsPriorities[$productId]) || !isset($recommendedProductsPriorities[$productId][$connectedId])) {
                                    $recommendedProductsPriorities[$productId][$connectedId] = $priority;
                                } else {
                                    $recommendedProductsPriorities[$productId][$connectedId] += $priority;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $recommendedProductsPriorities;
    }

    /**
     * Obliczenie priorytetu dla zamówienia
     *
     * @param $order array Tablica z produktami zamówienia
     * @return int Priorytet
     */
    protected function getPriority($order) {
        $count = count($order);
        if ($count == 0) $priority = 0;
        $priority = round(10/$count);
        return $priority;
    }

    /**
     * Dodanie zamówienia do tablicy z pryiorytetami powtórzonych zamówień
     *
     * @param $aOrder array Tablica z produktami zamówienia
     */
    protected function addOrderPriority($aOrder) {
        $priorities = $this->recommendedOrdersPriorities;
        $order = $aOrder;

        sort($order);

        $index = implode("-", $order);

        // Dodaj parę lub priorytet
        if (isset($priorities[$index]))
            $priorities[$index] += self::DUPLICATE_MULTIPLY;
        else
            $priorities[$index] = self::DUPLICATE_MULTIPLY;
        $this->recommendedOrdersPriorities = $priorities;
    }

    /**
     * Pomnożenie priorytetów powtórzonych zamówień przez priorytety produktów
     *
     * @param $recommendedProductsPriorities array Tablica z połączonymi produktami i priorytetami
     * @return array Tablica z połączonymi produktami i priorytetami pomnożona przez ilość powtórzonych zamówień
     */
    function multiplePriorities($recommendedProductsPriorities) {
        $multipliedPriorities = $recommendedProductsPriorities;
        $ordersPriorities = $this->recommendedOrdersPriorities;

        foreach ($ordersPriorities as $orderKey => $orderPriority) {
            if ($orderPriority > 1) {
                $orderProducts = explode("-", $orderKey);

                foreach ($orderProducts as $productId) {
                    foreach ($orderProducts as $connectedId) {
                        if ($productId != $connectedId && isset($multipliedPriorities[$productId]) && isset($multipliedPriorities[$productId][$connectedId])) {
                            $multipliedPriorities[$productId][$connectedId] *= $orderPriority;
                        }
                    }
                }
            }
        }

        return $multipliedPriorities;
    }

    /**
     *  Wyczyszczenie tablicy polecanych
     */
    protected function cleanProducts() {
        $this->db->Query("profit24", "DELETE FROM recommend_products");
    }

    /**
     * Pobranie produktów zamówień
     *
     * @return array Zamówienia wraz z produktami
     */
    protected function getOrdersProducts() {
        $sSql="SELECT o.id, oi.product_id
             FROM orders o
             LEFT JOIN orders_items oi ON oi.order_id = o.id
             WHERE DATE(o.order_date) > DATE_SUB(CURDATE(), INTERVAL ".self::ORDERS_DAYS." DAY)";

        return $this->db->GetAssoc("profit24", $sSql, false, [], DB_FETCHMODE_ASSOC, true );
    }

    /**
     * @param $recommenedProductsPriorites array Tablica z połączonymi produktami i ich priorytetami
     */
    protected function insertRecommends($recommenedProductsPriorites) {
        $recommended = $recommenedProductsPriorites;

        foreach ($recommended as $productId => $products) {
            foreach ($products as $connectedId => $priority) {
                if ($priority > self::MIN_PRIORITY) {
                    $this->db->Insert("profit24", "recommend_products", ["product_id" => $productId, "recommended_product_id" => $connectedId, "priority" => $priority]);
                }
            }
        }
    }
}