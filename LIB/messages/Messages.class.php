<?php

/**
 * Klasa obsługi komunikatów
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-03 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace messages;
class Messages {

  function __construct() {}
  
  
  /**
   * Funkcja pobiera tresc komunikatu strony 
   * 
   * @param	string $sSymbol		-	symbol komunikatu
   * @return 	string				-	tresc komunikatu
   */	
  public function getWebsiteMessage($sSymbol){
    global $aConfig, $pDbMgr;

    $sSql = "SELECT content FROM ".$aConfig['tabls']['prefix']."website_messages WHERE symbol = '".$sSymbol."'";
    return $pDbMgr->GetOne('profit24', $sSql);
  }// end of getWebsiteMessage() method

  
  /**
   * Metoda w przekazanym komunikacie zamienia symbole na wartości
   * 
   * @param string $sMsg komunikat
   * @param array $aSymbols tabela symboli
   * @param array $aValues tabela wartości
   * @return string
   */
  public function printWebsiteMessage($sMsg,$aSymbols=array(),$aValues=array()){
    
    $aToFind=array();
    foreach($aSymbols as $iKey => $sSmb)
      $aToFind[$iKey]='{'.$sSmb.'}';
    return str_replace($aToFind,$aValues,$sMsg);
  }// end of printWebsiteMessage(0 method
}
?>