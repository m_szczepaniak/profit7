<?php
/**
 * Created by PhpStorm.
 * User: arek
 * Date: 18.12.17
 * Time: 09:56
 */
namespace LIB\Supplies;

use DatabaseManager;
use LIB\communicator\sources\Internal\streamsoft\DataProvider;

class SuppliesToReservationFixer
{


    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    public function __construct(DatabaseManager $pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
        $this->dataProvider = new DataProvider($this->pDbMgr);
    }


    public function fix()
    {
        $aBuggedRecords = $this->getBuggedRows();
        foreach ($aBuggedRecords as $iKey => $row) {
            $productSourceSupplies = $this->dataProvider->getSuppliesByProductIdAndSource($row['product_id'], $row['source_id']);
            $sumReservations = $this->sumReservations($productSourceSupplies);
            // teraz musimy pobrać aktualne rezerwacje do zamówień

            $productStockReservations = $this->dataProvider->getReservationsToSupplies($row['product_id']);
            $sumReservationsPSSTR = $this->sumReservationsPSSTR($productStockReservations);
            if ($sumReservations <> $sumReservationsPSSTR) {
//                echo $row['product_id'] . ' - ' . $sumReservations . ' : ' . $sumReservationsPSSTR;
//                dump($productSourceSupplies);
//                dump($PSSTR);
            } else {
                echo 'identyczne';

                // dobra to tutaj lecimy z przepisywaniem
                $this->createRelationRows($productStockReservations, $productSourceSupplies);
            }


            // pobieramy wszystkie supplies
        }
    }


    private function createRelationRows($productStockReservations, $productSourceSupplies)
    {
        $suppliesReservation = [];
        foreach ($productStockReservations as $productStockReservation) {
            $productStockReservation['quantity'];
            $suppliesReservation[$productStockReservation['id']] = $this->GetReservationSuppliesByRef($productSourceSupplies, $productStockReservation['quantity']);
        }

        $this->recreateRelationTable($suppliesReservation);


        dump($suppliesReservation);
        dump($productStockReservations);
        dump($productSourceSupplies);
    }

    /**
     * @param $suppliesReservation
     */
    public function recreateRelationTable($suppliesReservation) {

        foreach ($suppliesReservation as $productStockReservationId => $supplies) {
            $this->dataProvider->deleteProductStockSuppliesToResByResId($productStockReservationId);
            foreach ($supplies as $supply) {
                $this->dataProvider->insertProdutStockSupplyReservation($productStockReservationId, $supply['supply'], $supply['reservation']);
            }
        }
    }

    /**
     * @param $productSourceSupplies
     * @param $quantity
     * @return array
     */
    private function GetReservationSuppliesByRef(&$productSourceSupplies, $quantity) {
        $suppliesToReservation = [];
        $currentReservationQuantity = $quantity;
        foreach ($productSourceSupplies as &$productSourceSupply) {
            if ($productSourceSupply['reservation'] > 0) {
                if ($currentReservationQuantity > $productSourceSupply['reservation']) {
                    $currentReservationQuantity = $currentReservationQuantity - $productSourceSupply['reservation'];

                    $suppliesToReservation[] =
                        [
                            'reservation' => $productSourceSupply['reservation'],
                            'supply' => $productSourceSupply['id']
                        ];

                    $productSourceSupply['reservation'] = 0;
                } else {
                    $productSourceSupply['reservation'] = $productSourceSupply['reservation'] - $currentReservationQuantity;

                    $suppliesToReservation[] =
                        [
                            'reservation' => $currentReservationQuantity,
                            'supply' => $productSourceSupply['id']
                        ];

                    $currentReservationQuantity = 0;
                    break;
                }
            }
        }
        return $suppliesToReservation;
    }

    private function sumReservationsPSSTR($PSSTR)
    {

        $sum = 0;
        foreach ($PSSTR as $row) {
            $sum += $row['quantity'];
        }
        return $sum;
    }

    private function sumReservations($productSourceSupplies)
    {

        $sum = 0;
        foreach ($productSourceSupplies as $row) {
            $sum += ($row['reservation'] - $row['reservation_to_reduce']);
        }
        return $sum;
    }

    private function getBuggedRows()
    {

        // to fix
        // SELECT * FROM products_stock_supplies_to_reservations
        // WHERE products_stock_supplies_id IN (356693,407733)

        $sSql = '
        SELECT PSS.id, PSS.product_id, 
        (PSS.reservation - PSS.reservation_to_reduce) AS reservation,
        (
            SELECT SUM( PSSTR.reservated_quantity ) 
            FROM products_stock_supplies_to_reservations AS PSSTR
            WHERE PSSTR.products_stock_supplies_id = PSS.id
            GROUP BY products_stock_supplies_id
        ) AS sum,
        PSS.source AS source_id
        
        FROM products_stock_supplies AS PSS
        WHERE (PSS.reservation - PSS.reservation_to_reduce) <> ( 
            SELECT SUM( PSSTR.reservated_quantity ) 
            FROM products_stock_supplies_to_reservations AS PSSTR
            WHERE PSSTR.products_stock_supplies_id = PSS.id
            GROUP BY products_stock_supplies_id 
        ) 
        ORDER BY PSS.id DESC
        LIMIT 20
';

        return $this->pDbMgr->GetAll('profit24', $sSql);
    }
}