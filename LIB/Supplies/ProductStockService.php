<?php

namespace LIB\Supplies;

use DatabaseManager;
use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\Helpers\ArrayHelper;

class ProductStockService
{
    const STOCK_SOURCE_ID = 10;

    const VALID_STOCK_SOURCE_ID = 51;

    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * @var DataProvider
     */
    private $dataProvider;

    /**
     * @var array
     */
    private $mappedSources;

    /**
     * @param DatabaseManager $pDbMgr
     */
    public function __construct(\DatabaseManager $pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
        $this->dataProvider = new DataProvider($this->pDbMgr);
        $this->mappedSources = $this->dataProvider->getMappedSources();
        $this->mappedSources[10] = $this->mappedSources[51];
    }

    /**
     * @param array $productStockSupplies
     * @param bool $remapKeys
     */
    public function updateProductStock(array $productStockSupplies, $remapKeys = false)
    {
        if (true === $remapKeys){
            $productStockSupplies = $this->remapKeys($productStockSupplies);
        }

        $productStockSupplies = $this->sortArray($productStockSupplies);
        //ArrayHelper::sortByColumn($productStockSupplies, 'wholesale_price', 'asc');
        $lowestWholesalePrice = $this->findLowestWholesalePrice($productStockSupplies);
        $quantitySum = ArrayHelper::sumArrayColumn($productStockSupplies, 'quantity');
        $reservationSum = ArrayHelper::sumArrayColumn($productStockSupplies, 'reservation');

        $singleRow = end($productStockSupplies);

        $source = $singleRow['source'];
        if (self::STOCK_SOURCE_ID == $source){
            $source = self::VALID_STOCK_SOURCE_ID;
        }


        $prefix = $this->mappedSources[$source];

        $productStock = $this->dataProvider->getProductStockById($singleRow['product_id']);
        $productStock = $this->prepareProductStock($prefix, $quantitySum, $reservationSum, $lowestWholesalePrice, $remapKeys, $productStock);


        $this->dataProvider->updateProductStock($productStock, $singleRow['product_id']);

    }

    private function sortArray($array)
    {
        $newArray = [];
        foreach ($array as $key => $row)
        {
            $newArray[$key] = $row['wholesale_price'];
        }
        array_multisort($newArray, SORT_ASC, $array);

        return $array;
    }

    /**
     * @param $prefix
     * @param $quantitySum
     * @param $reservationSum
     * @param $lowestWholesalePrice
     *
     * @param $remapKeys
     * @return mixed
     * @internal param $productStock
     */
    private function prepareProductStock($prefix, $quantitySum, $reservationSum, $lowestWholesalePrice, $remapKeys, $oldProductStock)
    {
        $mappedColumns = [
            'quantity' => $prefix.'_act_stock', // suma wszystkich quantity z PSS
            'status' => $prefix.'_status', // suma wszystkich reservations z PSS
            'price_brutto' => $prefix.'_price_brutto',
            'price_netto' => $prefix.'_price_netto',
            'vat' => $prefix.'_vat',
            'wholesale_price' => $prefix.'_wholesale_price',
            'last_import' => $prefix.'_last_import',
            'rez' => $prefix.'_reservations',
        ];

        $newProductStock = [];

        if (null === $lowestWholesalePrice){
            if (true == $remapKeys) {
                $newProductStock[$mappedColumns['status']] = 0;
                $newProductStock[$mappedColumns['quantity']] = $quantitySum;
            }
        } else {
            if (true == $remapKeys) {
                $newProductStock[$mappedColumns['quantity']] = $quantitySum;
            }

            if (null == $newProductStock[$mappedColumns['quantity']] && null != $oldProductStock[$prefix.'_act_stock']) {

                $newProductStock[$mappedColumns['quantity']] = $oldProductStock[$prefix.'_act_stock'];
            }

            $newProductStock[$mappedColumns['status']] = $quantitySum - $reservationSum;
            $newProductStock[$mappedColumns['price_brutto']] = $lowestWholesalePrice['price_brutto'];
            $newProductStock[$mappedColumns['price_netto']] = $lowestWholesalePrice['price_netto'];
            $newProductStock[$mappedColumns['vat']] = $lowestWholesalePrice['vat'];
            $newProductStock[$mappedColumns['wholesale_price']] = $lowestWholesalePrice['wholesale_price'];
        }

        $newProductStock[$mappedColumns['rez']] = $newProductStock[$mappedColumns['quantity']] - $newProductStock[$mappedColumns['status']];
        $newProductStock[$mappedColumns['last_import']] = (new \DateTime())->format('Y-m-d H:i:s');

        return $newProductStock;
    }

    /**
     * @param $productStockSupplies
     * @return null
     */
    private function findLowestWholesalePrice($productStockSupplies)
    {
        foreach($productStockSupplies as $stockSupply) {

            $data = $this->dataProvider->getSupplyByProductIdSourceAndWhPrice($stockSupply['product_id'], $stockSupply['wholesale_price'], $stockSupply['source']);

            $available = $data['quantity'] - $data['reservation'];

            if($available > 0){
                return $stockSupply;
            }
        }

        return null;
    }

    /**
     * @param $productStockSupplies
     *
     * @return array
     */
    private function remapKeys($productStockSupplies)
    {
        $newArray = [];

        foreach($productStockSupplies as $prodctStockSupply){
            $newSupply = [
                'id' => $prodctStockSupply['_id'],
                'quantity' => $prodctStockSupply['_stock'],
                'available_quantity' => $prodctStockSupply['_stock'],
                'source' => $prodctStockSupply['_source_id'],
                'wholesale_price' => $prodctStockSupply['_wholesale_price'],
                'price_brutto' => $prodctStockSupply['price_brutto'],
                'price_netto' => $prodctStockSupply['price_netto'],
                'product_id' => $prodctStockSupply['_product_id'],
                'vat' => $prodctStockSupply['vat'],
            ];

            if (isset($prodctStockSupply['_POREZ'])) {

                $newSupply['_POREZ'] = $prodctStockSupply['_POREZ'];
            }

            $newArray[] = $newSupply;
        }

        return $newArray;
    }

    public function updateStockByProductsIds($productsIds, $debug = false, $bDontBreakException = false)
    {
        foreach($productsIds as $productId){

            $productStock = [
                'profit_g_reservations' => 0,
                'profit_e_reservations' => 0,
                'profit_j_reservations' => 0
            ];

            $productReservations = $this->pDbMgr->GetAll('profit24', "SELECT * from products_stock_supplies WHERE product_id = $productId ");

            foreach($productReservations as $res){

                $source = $res['source'];

                if (10 == $source) {
                    $source = 51;
                }

                $prefix = $this->mappedSources[$source];
                $productStock[$prefix.'_reservations'] += $res['reservation'];
            }

            $currentProductStock = $this->dataProvider->getProductStockById($productId);

            foreach($productStock as $key => $reservation){
                $middlePrefix = explode('_', $key);
                $column = 'profit_'.$middlePrefix[1].'_status';
                $actStockColumn = 'profit_'.$middlePrefix[1].'_act_stock';
                $productStock[$column] = $currentProductStock[$actStockColumn] - $reservation;
				if ($productStock[$column] < 0) {
                    if (false === $bDontBreakException) {
                        //throw new \Exception('Błąd wystąpiła ujemna rezerwacja na stanach magazynowych id = ' . $productId . ' - ' . $column . ' ' . $productStock[$column]);
                    } else {
                        echo 'INFO - wystąpiła ujemna rezerwacja na stanach magazynowych id = ' . $productId . ' - ' . $column . ' ' . $productStock[$column];
                    }

                }
            }
			
            if (false === empty($productStock)){

                if (false === $this->dataProvider->updateProductStock($productStock, $productId))
                {
                    throw new \Exception(_('Blad pcozas aktualizacji stocku'));
                }
            }
        }









        //TODO zakomentowane do testow
        return true;

        foreach($productsIds as $productId){

            $productStock = [
                'profit_g_reservations' => 0,
                'profit_e_reservations' => 0,
                'profit_j_reservations' => 0
            ];

            $productReservations = $this->pDbMgr->GetAll('profit24', "SELECT * from products_stock_reservations WHERE products_stock_id = $productId ");

            foreach($productReservations as $res){
                $prefix = $this->mappedSources[$res['source_id']];
                $productStock[$prefix.'_reservations'] += $res['quantity'];
            }

            $currentProductStock = $this->dataProvider->getProductStockById($productId);

            foreach($productStock as $key => $reservation){
                $middlePrefix = explode('_', $key);
                $column = 'profit_'.$middlePrefix[1].'_status';
                $actStockColumn = 'profit_'.$middlePrefix[1].'_act_stock';
                $productStock[$column] = $currentProductStock[$actStockColumn] - $reservation;
            }

            if (false === empty($productStock)){

                if (false === $this->dataProvider->updateProductStock($productStock, $productId))
                {
                    throw new \Exception(_('Blad pcozas aktualizacji stocku'));
                }
            }
        }
    }
} 
