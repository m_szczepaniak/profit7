<?php

namespace LIB\Supplies;

use DatabaseManager;
use LIB\communicator\sources\Internal\streamsoft\DataProvider;
use LIB\Helpers\ArrayHelper;

class StockSuppliceService
{
    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    /**
     * @var DataProvider
     */
    private $dataProvider;

    /**
     * @var ProductStockService
     */
    private $productStockService;

    /**
     * @param $pDbMgr
     */
    public function __construct($pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
        $this->dataProvider = new DataProvider($this->pDbMgr);
        $this->productStockService = new ProductStockService($this->pDbMgr);
    }

    /**
     * @param $reservationId
     * @param $beforeSource
     * @param $afterSource
     * @param $productId
     * @param $beforeReservationQuantity
     * @param $afterReservationQuantity
     *
     * @throws \Exception
     */
    private function updateStockReservationItem($reservationId, $beforeSource, $afterSource, $productId, $beforeReservationQuantity, $afterReservationQuantity, $suppliesToReduce)
    {
		if (empty($reservationId) || $reservationId == 0 || $reservationId < 10){
			throw new \Exception("Taka rezerwacja nie istnieje");
		}
		
        // redukuje stara ilosc rezerwacji na produkt w zrodle
        if(null !== $beforeSource) {

            $supplyKeys = array_keys($suppliesToReduce);

            if (empty($supplyKeys)) {
                throw new \Exception('Blad w relacji psstr reservation '.$reservationId);
            }

            $beforeProductStockSupplies = $this->dataProvider->getSuppliesByProductIdAndSource($productId, $beforeSource, $supplyKeys);

            if (true === empty($beforeProductStockSupplies) && $beforeReservationQuantity > 0){

                $sql = "SELECT order_id FROM products_stock_reservations WHERE id = $reservationId";
                $orderNumber = $this->pDbMgr->GetRow('profit24', $sql);

                throw new \Exception(_("Brak dostaw mozliwych do cofniecia".' '.$orderNumber['order_id']));
            }

            $this->reduceBeforeReservations($reservationId, $beforeProductStockSupplies, $beforeReservationQuantity, $suppliesToReduce);

            $productStockSupplies = $this->dataProvider->getSuppliesByProductIdAndSource($productId, $beforeSource);
            
            $this->productStockService->updateProductStock($productStockSupplies);
        }

        // aktualizuje nowa ilosc rezerwacji na produkt w zrodle
        if(null !== $afterSource) {
            $afterProductStockSupplies = $this->dataProvider->getSuppliesByProductIdAndSource($productId, $afterSource);

            if (true === empty($afterProductStockSupplies) && $afterReservationQuantity > 0){

                $sql = "SELECT order_id FROM products_stock_reservations WHERE id = $reservationId";
                $orderNumber = $this->pDbMgr->GetRow('profit24', $sql);

                throw new \Exception(_("Brak dostaw mozliwych do zarezerwowania".' '.$orderNumber['order_id']));
            }

            $this->updateSuppliesQuantity($reservationId, $afterProductStockSupplies, $afterReservationQuantity);

            $productStockSupplies = $this->dataProvider->getSuppliesByProductIdAndSource($productId, $afterSource);

            $this->productStockService->updateProductStock($productStockSupplies);
        }
    }

    private function updateStockValues($reservationId, $productStockSupplies, $updateSupplies = true)
    {
        //$productStockReservation = $this->dataProvider->getProductStockReservationById($reservationId);

        //if(empty($productStockReservation)){
            if(false === $this->dataProvider->deleteProductStockSuppliesToResByResId($reservationId)){
                throw new \Exception(_('Blad w akutalizacji stock_supplies_to_reservations'));
            }
            //return false;
        //}

        foreach($productStockSupplies as $productStockSupply) {

            // jak sie nic nie zmienio to nic nie robimy
            if(null == $productStockSupply['has_changed']){
                continue;
            }

            $this->dataProvider->updateProductStockSupply($productStockSupply, ['reservation']);

            if (true == $updateSupplies && $productStockSupply['reservated'] > 0){
                $this->createStockSupplyReservationIfNotExist($reservationId, $productStockSupply['id'], $productStockSupply['reservated']);
            }
        }

        return true;
    }

    /**
     * @param $reservationId
     * @param $productStockSupplies
     * @param $afterReservationQuantity
     *
     * @return array
     *
     * @throws \Exception
     */
    private function updateSuppliesQuantity($reservationId, $productStockSupplies, $afterReservationQuantity)
    {
        $newStockSupplies = [];
        $allHasBeenUpdated = false;

        $debugQuantity = $afterReservationQuantity;

        $i = 0;

        foreach($productStockSupplies as $productStockSupply) {

            // wychodzimy jak nie trzeba jzu nic zmieniac
            if ($afterReservationQuantity == 0 && $i > 0){
                break;
            }

            $availableToReserve = (($productStockSupply['available_quantity'] - $afterReservationQuantity) < 0) ? $productStockSupply['available_quantity'] : $afterReservationQuantity;

            $productStockSupply['reservation'] += $availableToReserve;
            $productStockSupply['available_quantity'] -= $availableToReserve;
            $productStockSupply['reservated'] = $availableToReserve;
            $productStockSupply['has_changed'] = true;

            $afterReservationQuantity -= $availableToReserve;

            $newStockSupplies[] = $productStockSupply;

            $i++;

            if ($afterReservationQuantity == 0) {
                $allHasBeenUpdated = true;
            }
        }

        if (false == $allHasBeenUpdated) {
            var_dump($productStockSupplies);
			$sql = "SELECT order_id FROM products_stock_reservations WHERE id = $reservationId";
			$orderNumber = $this->pDbMgr->GetRow('profit24', $sql);
            throw new \Exception('Nie mozna bylo zarezerwowac wszystkich produktow z rezerwacji '.$orderNumber['order_id']);
        }

        $aLogger = [
            'args' => func_get_args(),
            'to_update' => $newStockSupplies,
            'beforeProductStockSupplies' => $productStockSupplies,
            'beforeReservationQuantity' => $debugQuantity
        ];
        $this->dataProvider->insertProductsStockReservationDebug($reservationId, $aLogger);


        $this->updateStockValues($reservationId, $newStockSupplies);

        return $newStockSupplies;
    }

    /**
     * @param $reservationId
     * @param $productStockSupplies
     * @param $beforeReservationQuantity
     *
     * @throws \Exception
     *
     * @return array
     */
    private function reduceBeforeReservations($reservationId, $productStockSupplies, $beforeReservationQuantity, $suppliesToReduce)
    {
        $newStockSupplies = [];

        foreach($productStockSupplies as $productStockSupply) {

            $toReduce = $suppliesToReduce[$productStockSupply['id']];

            $productStockSupply['reservation'] -= $toReduce;
            $productStockSupply['available_quantity'] += $toReduce;
//            $productStockSupply['reservated'] = $toReduce;

            if ($productStockSupply['reservation'] < 0) {
                throw new \Exception('Zbyt duza ilosc do usuwania');
            }

            $productStockSupply['has_changed'] = true;

            $newStockSupplies[] = $productStockSupply;
        }

        $this->updateStockValues($reservationId, $newStockSupplies, false);

        return $newStockSupplies;
    }

    /**
     * @param $oldOrderReservations
     * @param $newOrderReservations
     */
    public function updateStockReservationsByReservationsItems($oldOrderReservations, $newOrderReservations)
    {
        $oldOrderReservations = ArrayHelper::toKeyValues('id', $oldOrderReservations);

        $reservationsQuantity = $this->prepareReservationsQuantity($oldOrderReservations, $newOrderReservations);

        foreach($reservationsQuantity as $reservation) {

            // jezeli nic sie nie zmienilo to lecimy dalej
            if ($reservation['before_reservation_quantity'] == $reservation['new_reservation_quantity']) {
                continue;
            }

            $this->updateStockReservationItem(
                $reservation['id'],
                $reservation['before_source_id'],
                $reservation['new_source_id'],
                $reservation['product_id'],
                $reservation['before_reservation_quantity'],
                $reservation['new_reservation_quantity'],
                $reservation['supplies']
            );
        }
    }

    /**
     * @param $oldOrderReservations
     * @param $newOrderReservations
     *
     * @return array
     */
    private function prepareReservationsQuantity($oldOrderReservations, $newOrderReservations)
    {
        $newValues = [];

        $elementsToIterate = array_unique(array_merge(array_keys($oldOrderReservations), array_keys($newOrderReservations)));

        foreach($elementsToIterate as $element) {

            $explodedSuppies = null;
            $suppliesToPrepare = $oldOrderReservations[$element]['supplies'];
            if (null != $suppliesToPrepare) {
                $explodedSuppies = $this->explodeSupplies($suppliesToPrepare);
            }

            $value = [
                'id' => $element,
                'before_source_id' => $oldOrderReservations[$element]['source_id'],
                'new_source_id' => $newOrderReservations[$element]['source_id'],
                'product_id' => null != $oldOrderReservations[$element]['product_id'] ? $oldOrderReservations[$element]['product_id'] : $newOrderReservations[$element]['product_id'],
                'before_reservation_quantity' => (int) $oldOrderReservations[$element]['quantity'],
                'new_reservation_quantity' => (int) $newOrderReservations[$element]['quantity'],
                'supplies' => $explodedSuppies
            ];

            $newValues[$element] = $value;
        }

        return $newValues;
    }

    private function explodeSupplies($elementSupplies)
    {
        $newSupplies = [];
        $elementSupplies = explode(',', $elementSupplies);

        foreach($elementSupplies as $element) {
            $exploded = explode(':', $element);
            $newSupplies[$exploded[0]] = $exploded[1];
        }

        return $newSupplies;
    }

    /**
     * @param $reservationId
     * @param $supplyId
     *
     * @return bool
     * @throws \Exception
     */
    private function createStockSupplyReservationIfNotExist($reservationId, $supplyId, $reservated)
    {

//        $productStockSupplyReservation = $this->dataProvider->getProductStockDataReservationByResId($reservationId, $supplyId);
//
//        if (null !== $productStockSupplyReservation) {
//            return false;
//        }

        if(false === $this->dataProvider->insertProdutStockSupplyReservation($reservationId, $supplyId, $reservated)){
            throw new \Exception(_('Blad w akutalizacji stock_supplies_to_reservations'));
        }

        return true;
    }

    /**
     * @param $orderId
     * @param null $itemsToReduce
     * @return bool
     * @throws \Exception
     */
    public function updateReservationsToReduceByOrderId($orderId, $itemsToReduce = null)
    {
        $groupedSupplies= [];

        if (null !== $itemsToReduce){
            $stockSupplies = $itemsToReduce;
        }
        else {
            $stockSupplies = $this->dataProvider->getProductStockDataResByResId($orderId);
        }

        foreach($stockSupplies as $stockSupply){
            $groupedSupplies[$stockSupply['products_stock_supplies_id']] += $stockSupply['reservated_quantity'];
        }

        $supplies = $this->dataProvider->getSuppliesByIdIn(array_keys($groupedSupplies));

        foreach($supplies as $supply){
            $toUpdate = $supply['reservation_to_reduce'] + $groupedSupplies[$supply['id']];

            $debug = [
                'before' => $stockSupplies,
                'update' => $toUpdate
            ];
            $this->dataProvider->insertProductsStockSuppliesDebug($supply['id'], $debug);
            if (false === $this->dataProvider->updateProductStockResSupplyToReduce($supply['id'], $toUpdate)) {
                throw new \Exception('Blad podczas aktualizacji reservation_to_reduce');
            }
        }

        foreach($stockSupplies as $stockSupply){

            if (false === $this->dataProvider->resetReservationToReduce($stockSupply['id'])) {
                throw new \Exception('Blad podczas aktualizacji products_stock_supplies_to_reservations');
            }
        }

        return true;
    }
}
