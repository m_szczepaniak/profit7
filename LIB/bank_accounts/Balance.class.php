<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-03-19 
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\bank_accounts;

use DatabaseManager;

/**
 * Description of Balance
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class Balance {

  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   * 
   * @param DatabaseManager $pDbMgr
   */
  public function __construct($pDbMgr) {
    
    $this->pDbMgr = $pDbMgr;
  }
  
  /**
   * 
   * @param string $sIdentStatementsFile
   * @param float $fAmount
   * @return type
   */
  public function newStatement($sIdentStatementsFile, $fAmount) {
    
    $sSql = 'UPDATE bank_accounts
             SET balance = balance + '.$fAmount.'
             WHERE ident_statments_file = "'.$sIdentStatementsFile.'"
             ORDER BY id ASC';
    return $this->pDbMgr->Query('profit24', $sSql);
  }
  
  /**
   * 
   * @return array
   */
  public function getBankAccountsBalances() {
    
    $sSql = 'SELECT ident_statments_file, number, balance 
             FROM bank_accounts
             GROUP BY number';
    return $this->pDbMgr->GetAssoc('profit24', $sSql);
  }
  
  
  /**
   * 
   * @param int $iPrevDays
   * @return array
   */
  public function getStatements($iPrevDays) {
    
    $sSql = '
      SELECT ident_statments_file, SUM(paid_amount) as `paid_amount`, DATE(created) as `date`
      FROM orders_bank_statements 
      WHERE DATE(created) >= DATE_SUB(CURDATE(), INTERVAL '.$iPrevDays.' DAY) AND deleted = "0"
      GROUP BY ident_statments_file, DATE(created)
      ORDER BY created DESC
             ';
    return $this->pDbMgr->GetAssoc('profit24', $sSql, true, array(), DB_FETCHMODE_ASSOC, true);
  }
  
  /**
   * 
   * @param array $aBalances
   * @param array $aStatements
   * @return array
   */
  public function linkBankBalanceAndStatement($aBalances, $aStatements) {
    foreach ($aBalances as $sIdentStatement => $aBalance) {
      if (isset($aStatements[$sIdentStatement])) { 
        $aBalances[$sIdentStatement]['days_transfers'] = $aStatements[$sIdentStatement];
      }
    }
    return $aBalances;
  }
}
