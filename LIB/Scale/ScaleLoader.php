<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace Scale;

use Common;

class ScaleLoader
{
    /**
     * @var DatabaseManager
     */
    private $pDbMgr;

    private $connection = 'profit24';

    private $tableName = 'scale';

    public function __construct()
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;
    }

    public function loadScales()
    {
        return $this->pDbMgr->GetAll($this->connection, "SELECT id, name FROM $this->tableName");
    }

    public function scaleWeight($scaleId)
    {
        return Common::formatPrice($this->pDbMgr->GetOne($this->connection, "SELECT current_weight FROM $this->tableName WHERE id = $scaleId") / 1000);
    }

    public function updateScaleValue($scaleId, $scaleValue)
    {
        $this->pDbMgr->Query($this->connection, "UPDATE `scale` SET current_weight = $scaleValue WHERE id = $scaleId");
    }
}