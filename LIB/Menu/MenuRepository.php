<?php

namespace Menu;

use Common;

class MenuRepository
{
    /**
     * @var array $config
     */
    private $config;

    /**
     * @var \DatabaseManager $pDbMgr
     */
    private $pDbMgr;

    /**
     * @var
     */
    private $aPrivileges;

    public function __construct()
    {
        global $aConfig, $pDbMgr;

        $this->config = $aConfig;
        $this->pDbMgr = $pDbMgr;
        $this->aPrivileges =& $_SESSION['user']['privileges'];
    }

    /**
     * @return array
     */
    public function getAvailableModules()
    {
        $sModules = '';

        // pobranie listy dostepnych dla uzytkownika modulow
        if ($_SESSION['user']['type'] != 0) {
            // uzytkownik typu superadmin lub admin
            $sSql = "SELECT id, symbol, ".$_SESSION['user']['type']." AS profile
							 FROM ".$this->config['tabls']['prefix']."modules
							 ORDER BY order_by";
            $aModules = $this->pDbMgr->GetAll('profit24', $sSql);
        }
        elseif (isset($this->aPrivileges['modules']) &&
            !empty($this->aPrivileges['modules'])) {
            // zwykly uzytkownik
            // okreslenie ID modulow do ktorych ma dostep uzytkownik
            foreach ($this->aPrivileges['modules'] as $iMId => $iPriv) {
                $sModules .= $iMId.',';
            }
            $sModules = substr($sModules, 0, -1);
            $sSql = "SELECT id, symbol
							 FROM ".$this->config['tabls']['prefix']."modules
							 WHERE id IN (".$sModules.")
							 ORDER BY order_by";
            $aModules = $this->pDbMgr->GetAll('profit24', $sSql);
        }

        return $aModules;
    }

    /**
     * @return array
     */
    public function getModulesIds()
    {
        $sSql = "SELECT symbol, id
							 FROM ".$this->config['tabls']['prefix']."modules
							 ORDER BY order_by";

        return $this->pDbMgr->GetAssoc('profit24', $sSql);
    }
}