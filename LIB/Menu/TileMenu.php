<?php

namespace Menu;

class TileMenu
{
    //const SUB_MENU_URL = 'admin.php?frame=main&module_id=24&module=m_menu&do=sub-menu&menu-symbol=';
    const SUB_MENU_URL = 'admin.php?frame=main';

    /**
     * @var string $modulesPath
     */
    private $modulesPath;

    /**
     * @var MenuRepository $menuRepository
     */
    private $menuRepository;

    /**
     * @var array $configs
     */
    private $config;

    public function __construct()
    {
        global $aConfig;

        $this->modulesPath = $_SERVER['DOCUMENT_ROOT'].$aConfig['common']['cms_dir'].'modules';
        $this->menuRepository = new MenuRepository();
        $this->config = $aConfig;
    }

    public function renderMenu($menuSymbol = null)
    {
        $menu = $this->prepareMenu($menuSymbol);

        return $menu;
    }

    private function prepareMenu($menuSymbol = null)
    {
        $xml = false;
        if (null === $menuSymbol){
            $modules = $this->menuRepository->getAvailableModules();
        } else {
            $modules = $this->getXMLFile($menuSymbol);
            $xml = true;
        }

        $modules = $this->prepareMenuItems($modules, $xml);

        return $modules;
    }

    private function prepareMenuItems($modules, $xml = false)
    {
        $aModuleItem = [];

        if (false == $xml){
            foreach($modules as $menuItem) {

                if (false === file_exists($this->modulesPath.'/'.$menuItem['symbol'].'/menu.xml')) {
                    continue;
                }

                $menuItem['url'] = self::SUB_MENU_URL.'&module_id='.$menuItem['id'].'&menu-symbol='.$menuItem['symbol'];
                $name = (isset($this->config['lang']['c_modules'][$menuItem['symbol']]) ? $this->config['lang']['c_modules'][$menuItem['symbol']] : $menuItem['symbol']);
                $name = implode('<br>', explode(' ', $name));
                $menuItem['name'] = $name;
                $aModuleItem[] = $menuItem;
            }

            return $aModuleItem;
        }

        $privs = $_SESSION['user']['privileges'];

        foreach ($modules->item as $item) {
            $attr = $item->attributes();

          if ($_SESSION['user']['type'] == 0) {
              if (false === in_array($_GET['module_id'], array_keys($privs['modules']))) {
                continue;
              }
            }
            $nameString = (string) $attr->name;
            $name = (isset($this->config['lang'][$_GET['menu-symbol']][$nameString]) ? $this->config['lang'][$_GET['menu-symbol']][$nameString] : $nameString);
            $name = implode('<br>', explode(' ', $name));
            $aModuleItem[] = [
                'name' => $name,
                'privs' => (string) $attr->privs,
                'target' =>  (string) $attr->target,
                'url' =>  (string) sprintf($attr->url, $_GET['module_id'])
            ];
        }

        return $aModuleItem;
    }

    private function getXMLFile($menuSymbol) {

        $menuFile = $this->modulesPath.'/'.$menuSymbol.'/menu.xml';

        $xmlElement = simplexml_load_file($menuFile);
        $elementAttr = $xmlElement->attributes();
        $sModule = (string)$elementAttr->module;
        $items = $xmlElement->children();

        return $items;
    }
}