<?php

namespace Exception;

use Exception;

class StreamsoftErrorException extends Exception
{
    public function __construct($title, $message, $code = 0, Exception $previous = null) {

        $this->title = $title;

        parent::__construct($message, $code, $previous);
    }
}