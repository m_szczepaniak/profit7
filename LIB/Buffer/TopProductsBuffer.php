<?php

namespace Buffer;

class TopProductsBuffer
{
    const COMPARISON_SITES_DB_NAME = 'profit24';

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;

    public function __construct()
    {
        global $pDbMgr;

        $this->pDbMgr = $pDbMgr;
    }

    /**
     * @return array
     */
    public function getTopProductsIds()
    {
        return $this->pDbMgr->GetCol(self::COMPARISON_SITES_DB_NAME, "SELECT product_id FROM top_products_buffer ORDER BY priority");
    }

    public function generateBuffer()
    {
        $topProducts = $this->findTopProducts();

        $this->saveDataToBuffer($topProducts);
    }

    /**
     * @param array $topProducts
     */
    private function saveDataToBuffer(array $topProducts)
    {
        $this->pDbMgr->Query(self::COMPARISON_SITES_DB_NAME, "DELETE FROM top_products_buffer");

        $i = 1;
        foreach($topProducts as $product) {

            $values = [
                'product_id' => $product['product_id'],
                'priority' => $i,
            ];

            $this->pDbMgr->Insert(self::COMPARISON_SITES_DB_NAME, "top_products_buffer", $values);
            $i++;
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function findTopProducts()
    {
        $comparisonData = $this->pDbMgr->GetAssoc(self::COMPARISON_SITES_DB_NAME, "SELECT * FROM products_comparison_sites WHERE shipment_time = 'd' || shipment_time = 't'");
        $days = (int)$comparisonData['d'];
        $topProductsAmount = (int)$comparisonData['t'];

        if ($days == 0 || $topProductsAmount == 0) {
            throw new \Exception('Nie skonfigurowano danych w konfiguracji porownywarek');
        }

        $now = new \DateTime();
        $now->modify("- $days days");

        $sStartDate = $now->format('Y-m-d 00:00:00');
        $limit = $topProductsAmount;

        $sSql = '
        SELECT "0" as lp, IF(P.ean_13 <> "", P.ean_13, OI.isbn), OI.name, PP.name as publisher, P.wholesale_price, OI.publication_year, "0" AS orders_count, OI.quantity as sum_quantity,
          PS.profit_g_status, PS.profit_g_reservations,
          PS.profit_e_status,
          PS.profit_j_status,
          IF(PS.azymut_status = "0", "0", PS.azymut_stock) AS azymut_stock,
            PS.azymut_wholesale_price,
          IF(PS.ateneum_status = "0", "0", PS.ateneum_stock) AS ateneum_stock,
            PS.ateneum_wholesale_price,
          IF(PS.siodemka_status = "0", "0", PS.siodemka_stock) AS siodemka_stock,
            PS.siodemka_wholesale_price,
          IF(PS.dictum_status = "0", "0", PS.dictum_stock) AS dictum_stock,
            PS.dictum_wholesale_price,
          IF(PS.platon_status = "0", "0", PS.platon_stock) AS platon_stock,
            PS.platon_wholesale_price,
            P.id as product_id
      FROM orders_items AS OI
      JOIN orders AS O
        ON OI.order_id = O.id AND O.order_status <> "5"
              							 AND O.order_date >= "'.$sStartDate.'"
      JOIN products AS P
        ON P.id = OI.product_id
      JOIN products_publishers AS PP
        ON P.publisher_id = PP.id
      JOIN products_stock AS PS
        ON PS.id = P.id

      WHERE 1 = 1 AND
            OI.deleted = "0"';
        $data = $this->pDbMgr->GetAll('profit24', $sSql);
        $newData = $this->sumArrayColsGroup($data, 'product_id', ['sum_quantity'], 'orders_count', 'orders_count');
        unset($data);

        $newData = array_slice($newData, 0, $limit);

        return $newData;
    }

    /**
     * @param $aData
     * @param $sGroupByCol
     * @param $aSumCols
     * @param string $sortAfterByColumn
     * @return array
     */
    private function sumArrayColsGroup($aData, $sGroupByCol, $aSumCols, $sortAfterByColumn = '', $countGroupedBy = '') {

        $newArray = [];
        $sortByColumnArray = [];
        foreach ($aData as $iKey => $aItem) {
            $colGroupByValue = $aItem[$sGroupByCol];
            if (!isset($newArray[$colGroupByValue])) {
                // define
                $newArray[$colGroupByValue] = $aItem;
            }
            else {
                // only sum Values
                foreach ($aSumCols as $sSumColumn) {
                    $newArray[$colGroupByValue][$sSumColumn] += $aItem[$sSumColumn];
                }
            }
            if ($countGroupedBy != '') {
                // zliczenie ile jest zgrupowanych
                $newArray[$colGroupByValue][$countGroupedBy]++;
            }
            if ($sortAfterByColumn != '') {
                // tworzymy now� tabelk� pomocn� p�niej przy sortowaniu
                $sortByColumnArray[$colGroupByValue] = $newArray[$colGroupByValue][$sortAfterByColumn];
            }
        }
        if (!empty($sortByColumnArray)) {
            // sortujemy
            array_multisort($sortByColumnArray, SORT_DESC, $newArray);
        }
        return $newArray;
    }
}