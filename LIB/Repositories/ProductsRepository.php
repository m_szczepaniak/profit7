<?php

namespace Repositories;

use EntityManager\Repository\AbstractRepository;

class ProductsRepository extends AbstractRepository
{
    protected $tableName = 'products';
}