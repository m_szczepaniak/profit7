<?php

namespace Repositories;

use Common;
use EntityManager\Repository\AbstractRepository;
use LIB\Helpers\StringHelper;

class CeneoTabsRepository extends AbstractRepository
{
    const CENEO_IN_TYPE = '0';
    const CENEO_OUT_TYPE = '1';
    const CENEO_VIP_TYPE = '2';

    protected $tableName = 'ceneo_tabs_items';

    public function init($type)
    {
        if ($type == 4) {

            $this->tableName = 'ceneo_product_priority';
        }
    }

    public function deleteById($id)
    {
        $sql = "DELETE FROM $this->tableName WHERE id = $id limit 1";

        return $this->query($sql);
    }

    public function getAllByType($type, $priceFrom, $priceTo, $search, $perPage, $iCurrentPage = null, $rsql = false)
    {
        global $aConfig;
        $iNow = time();

        $iPerPage = isset($perPage) && !empty($perPage) ? $perPage : $aConfig['default']['per_page'];
        $iStartFrom = ($iCurrentPage - 1) * $iPerPage;
        if (null == $iCurrentPage){
            $iStartFrom = 0;
        }

        $sSearch = '';
        if (isset($search) && !empty($search)) {
            $search = trim($search);
            if (StringHelper::SimpleCheckISBN($search)) {
                $sSearch = ' AND (
                          P.id = ' . (int) $search . ' OR
                          P.isbn_plain LIKE \'' . isbn2plain($search) . '\' OR
                          P.isbn_10 LIKE \'' . isbn2plain($search) . '\' OR
                          P.isbn_13 LIKE \'' . isbn2plain($search) . '\' OR
                          P.ean_13 LIKE \'' . isbn2plain($search) . '\'
                          )';
            } else {
                $sSearchStr = StringHelper::parseSearchString($search);
                $sSearch = ' AND (
                          P.name LIKE \'' . $sSearchStr . '\'
                          )';
            }
        }

        if ($type == 3){

            $sql = "
            select distinct P.id, P.isbn, P.id as product_id, P.name from products AS P
            ";

        } else {
            $sql = "
            SELECT distinct CTI.id, P.isbn, CTI.product_id, P.name FROM $this->tableName AS CTI
            JOIN products AS P ON CTI.product_id = P.id";
        }

        $sql .= (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ?
            " LEFT JOIN products_publishers F
							 	 ON P.publisher_id = F.id" : "");
        $sql .= (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ?
            " LEFT JOIN products_to_series G
							 	 ON G.product_id = P.id" : '');

        $sql .= ($priceFrom > 0 || $priceTo > 0 ? ' LEFT JOIN products_tarrifs T ON T.product_id=P.id' : '');

        if ($type != 3){
            $sql .=    " WHERE CTI.type = '$type'";
        }

        if ($type == 3 ){
            $sql .= " where P.published = '1'
            AND P.packet = '0'
            AND (P.prod_status = '1' OR P.prod_status = '3')
            AND (P.shipment_time = '1' OR P.shipment_time = '0')
            and (select count(*) from products_extra_categories as PEC2 join menus_items as MI ON MI.id = PEC2.page_id AND MI.priority != '0' WHERE PEC2.product_id = P.id) < 1";
        }

        $sql .= ($priceFrom > 0 ? ' AND T.price_brutto>=' . Common::FormatPrice2($priceFrom) . ' AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' AND T.price_brutto=(SELECT price_brutto FROM products_tarrifs TT WHERE TT.product_id=P.id AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' ORDER BY price_brutto ASC LIMIT 1)' : '');
        $sql .= ($priceTo > 0 ? ' AND T.price_brutto<=' . Common::FormatPrice2($priceTo) . ' AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' AND T.price_brutto=(SELECT price_brutto FROM products_tarrifs TT WHERE TT.product_id=P.id AND T.start_date<=' . $iNow . ' AND  T.end_date>=' . $iNow . ' ORDER BY price_brutto ASC LIMIT 1)' : '');
        $sql .= $sSearch;
        $sql .= (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' ? ' AND F.name = \'' . $_POST['f_publisher'] . '\'' : '');
        $sql .= (isset($_POST['f_publisher']) && $_POST['f_publisher'] != '' && isset($_POST['f_series']) && $_POST['f_series'] != '' ? ' AND G.series_id = ' . $_POST['f_series'] . '' : '');


        if ($rsql != true){
            $sql .= " LIMIT " . $iStartFrom . ", " . $iPerPage;
        }

        if (true == $rsql){
            return count($this->getAll($sql));
        }

        return $this->getAll($sql);
    }

    public function getAllProductsIds()
    {
        $sql = "
            SELECT CTI.product_id FROM $this->tableName AS CTI
        ";

        return $this->getCol($sql);
    }

    public function addProductsToTabs($products, $type)
    {
        $existProducts = array_values($this->getAllProductsIds());

        foreach($products as $product){
            if (in_array($product, $existProducts)){
                continue;
            }

            $values = [
                'product_id' => $product,
                'type' => $type
            ];

            $this->insert($values);
        }
    }

    public function findProductsByTabId($ids) {

        $ids = implode(',', $ids);

        $sql = "SELECT product_id FROM $this->tableName WHERE id IN($ids)";

        return $this->getCol($sql);
    }

    public function findAllVipProducts($type)
    {
        $sql = "
          SELECT P.id as product_id, 1 as priority FROM $this->tableName AS CT
          JOIN products AS P ON P.id = CT.product_id
          JOIN website_ceneo_categories as WCC ON P.main_category_id = WCC.category_id
          WHERE CT.`type` = '$type'
          AND P.packet = '0'
          AND P.published= '1'
          AND (P.prod_status = '1' OR P.prod_status = '3')
          AND (P.shipment_time = '1' OR P.shipment_time = '0')
        ";

        return $this->getAll($sql);
    }

    public function findAllByCeneoCategories($shopId, $type)
    {
        $sql = "
          SELECT P.id as product_id, 0 as priority FROM $this->tableName AS CT
          JOIN products AS P ON P.id = CT.product_id
          JOIN website_ceneo_categories AS WCC ON P.main_category_id = WCC.category_id
          WHERE CT.`type` = '$type'
          AND P.packet = '0'
          AND P.published= '1'
          AND (P.prod_status = '1' OR P.prod_status = '3')
          AND (P.shipment_time = '1' OR P.shipment_time = '0')
        ";

        return $this->getAll($sql);
    }

    public function findProductsToDelete($type)
    {
        $sql = "
          SELECT CT.product_id FROM $this->tableName AS CT
          WHERE CT.`type` = '$type'
        ";

        return $this->getCol($sql);
    }
}