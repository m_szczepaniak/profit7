<?php

namespace Migrations;

class MigrationsController {
    private $pDbMgr;
    private $migrationsPath;

    function __construct($pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
        $this->migrationsPath = $_SERVER['DOCUMENT_ROOT'] . 'BAZA/migrations/';
    }

    /**
     * Migruje bazę danych
     * @throws \Exception
     */
    public function migrate() {
        $latestMigration = $this->getLatestMigration();
        echo "Ostatnia migracja " . $latestMigration . "\n";

        $migrationFiles = $this->getMigrationFiles();

        $newMigrationFiles = $this->getNewMigrations($migrationFiles, $latestMigration);
        $newMigrationFilesCount = count($newMigrationFiles);
        if ( $newMigrationFilesCount > 0 ) {
            echo $newMigrationFilesCount . " migracje do zmigrowania\n";
        } else if ( $newMigrationFilesCount === 0 ) {
            echo "Brak migracji do zmigrowania\n";
            return false;
        }

        $this->loadAndExecuteMigrations($newMigrationFiles);

        $this->saveLatestMigration(end($newMigrationFiles));
    }

    /**
     * Generuj migracje
     *
     * @throws \Exception
     */
    public function generateMigration() {
        $date = date("Ymd");

        $latestMigration = $date."00";

        $migrationFiles = $this->getMigrationFiles();

        $newMigrationFiles = $this->getNewMigrations($migrationFiles, $latestMigration);
        $newMigrationFilesCount = count($newMigrationFiles);
        if ( $newMigrationFilesCount > 0 ) {
            $tmpMigrationNumber = intval(substr(end($newMigrationFiles), 8, 2));
            $tmpMigrationNumber++;
            if ( $tmpMigrationNumber >= 10 ) {
                $migrationNumber = (string)$tmpMigrationNumber;
            } else {
                $migrationNumber = "0" . (string)$tmpMigrationNumber;
            }
        } else {
            $migrationNumber = "01";
        }

        $newMigrationVersion = $date . $migrationNumber;
        $this->generateMigrationFile($newMigrationVersion);
        echo "Wygenerowano plik BAZA/migrations/" . $newMigrationVersion . ".php\n";
    }

    /**
     * Pobiera listę plików w katalogu z migracjami
     *
     * @return array
     * @throws \Exception
     */
    private function getMigrationFiles()
    {
        $files = scandir($this->migrationsPath);

        if ($files == false) {
            throw new \Exception('Katalog migracji nie istnieje! Utwórz folder /BAZA/migrations');
        }

        // array_diff removes .. and . from files list
        return array_diff($files, ['..', '.']);
    }

    /**
     * Wybiera z listy plików tylko te które są nowsze od najnowszej migracji w bazie
     *
     * @param array $migrationFiles
     * @param $latestMigration
     * @return array
     */
    public function getNewMigrations(array $migrationFiles, $latestMigration) {
        $newMigrationFiles = [];
        foreach ($migrationFiles as $file) {
            $migrationVersion = $this->getMigrationVersionFromFilename($file);
            if ( $migrationVersion > $latestMigration ) {
                $newMigrationFiles[] = $file;
            }
        }
        return $newMigrationFiles;
    }

    /**
     * Pobiera część nazwy pliku z wersją migracji
     *
     * @param $filename
     * @return int
     */
    private function getMigrationVersionFromFilename($filename) {
        $versionString = explode('.', $filename)[0];
        return intval($versionString);
    }

    /**
     * Pobiera ostatnią migrację
     */
    private function getLatestMigration() {
        return $this->pDbMgr->GetOne('profit24', 'SELECT version FROM migration_versions ORDER BY version DESC');
    }

    /**
     * Includuj migracje i wykonaj je
     *
     * @param $newMigrationFiles
     * @throws \Exception
     */
    private function loadAndExecuteMigrations($newMigrationFiles)
    {
        foreach ($newMigrationFiles as $file) {
            $version = $this->getMigrationVersionFromFilename($file);
            include_once $this->migrationsPath . $file;
            $clasName = 'Version' . $version;
            $migration = new $clasName($this->pDbMgr);
            if ( $migration instanceof Migration ) {
                $start = microtime(true);

                $error = $migration->migrate();
                if ( $error === true ) {
                    throw new \Exception("Błąd przy migrowaniu " . $version );
                }

                $timeElapsedSecs = microtime(true) - $start;
                echo "Migracja " . $version . " została zmigrowana w " . $timeElapsedSecs . " sekund.\n";
            } else {
                throw new \Exception("Klasa " . $clasName . " nie ma interfejsu Migration!");
            }
        }
    }

    /**
     * Zapisz wersje ostatniej migracji do bazy danych
     *
     * @param $end
     */
    private function saveLatestMigration($migrationFile)
    {
        $version = $this->getMigrationVersionFromFilename($migrationFile);

        $sql = 'INSERT INTO migration_versions VALUES (' . $version . ')';

        $this->pDbMgr->PlainQuery('profit24', $sql);
    }

    /**
     * Generuje plik Migracji
     *
     * @param $newMigrationVersion
     */
    private function generateMigrationFile($newMigrationVersion)
    {
        $migrationTemplate = file_get_contents($_SERVER['DOCUMENT_ROOT'] . 'LIB/Migrations/migration.php.template');
        $migrationTemplate = str_replace('{migrationVersion}', $newMigrationVersion, $migrationTemplate);
        file_put_contents($this->migrationsPath . $newMigrationVersion . '.php', $migrationTemplate);
    }

}