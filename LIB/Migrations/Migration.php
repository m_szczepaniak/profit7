<?php

namespace Migrations;

abstract class Migration {
    protected $pDbMgr;

    function __construct($pDbMgr)
    {
        $this->pDbMgr = $pDbMgr;
    }

    abstract public function migrate();
}