<?php

namespace BuyBox;

class ScriptGenerator
{
    /**
     * @param $orderNumber
     * @param $aData
     * @param bool $withMainScript
     * @return string
     */
    public static function generateOrderScript($orderNumber, $aData, $withMainScript = true)
    {
        global $aConfig;
        $commisionId = $aConfig['common']['buybox_commision_id'];

        $data = [
            'orderId' => $orderNumber,
            'products' => []
        ];

        foreach($aData['products'] as $product) {

            $price = $product['promo_price'];
            $id = $product['id'];
            $quantity = $product['quantity'];

            if (empty($price)) {

                $price = $product['price_brutto'];
            }

            $data['products'][] = [
                'productId' => $id,
                'quantity' => $quantity,
                'commissionId' => $commisionId,
                'gross' => $price,
            ];
        }

        $encoded = json_encode($data);

        $string = '';

        if (true == $withMainScript) {

            $string .= '
            	<script type="text/javascript">
                    "use strict";(function(scriptUrl){if(!window.bb){window.bb=function(){
                        for(var _len=arguments.length,params=Array(_len),_key=0;_key<_len;_key++){
                            params[_key]=arguments[_key]}return window.bb.q.push(params)};window.bb.q=[]
                    ;var script=document.createElement("script")
                            ;var firstScript=document.getElementsByTagName("script")[0];script.async=true
                    ;script.src=scriptUrl;firstScript.parentNode.insertBefore(script,firstScript)}
                    })("https://shop.buybox.click/bb-shop-133.js");
                </script>
            ';
        }

        $string .= '
        <script type="text/javascript">
            bb("order", '.$encoded.');
        </script>
        ';

        return $string;
    }

    /**
     * @param $productId
     * @return string
     */
    public static function generateProductScript($productId)
    {
        $string = '
        <script type="text/javascript">
        bb("productView", { productId: "'.$productId.'" })
        </script>
        ';

        return $string;
    }
}