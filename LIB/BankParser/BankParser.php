<?php
/*
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@mail.com>.
 * @created: 2015-11-24
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace BankParser;

use Array2XML;

include_once __DIR__.'/../../omniaCMS/lib/Array2XML.class.php';

class BankParser
{
//    private $pattern = '/(\d{2}\/\d{2}) ?(PRZELEW ZEWN..TRZNY - WP..ATA)?(.+) (\d{26})(.*) ([\d+,.]+)/';
    private $pattern = '/(\d{2}\/\d{2}) ?(PRZELEW ZEWN..TRZNY - WP..ATA|PRZELEW PRZYCHODZ..CY|PRZELEW WYCHODZ..CY -INTERNET|PRZELEW WEWN..TRZNY W CITIBANK ONLINE|PRZELEW OTRZYMANY)?(.+) ([- ]+[\d+,.]+)/';

    /**
     * @param $filePath
     */
    public function parseBankFile($filePath, $accNumber = null)
    {
//        var_dump(__DIR__.'../../lib/Array2XML.class.php');
        $loadedFileData = $this->loadFile($filePath);

        $accountNumber = substr(str_replace([' ', '.txt'], '', $filePath), -10);

        if (null !== $accNumber) {
            $accountNumber = $accNumber;
        }

        $parsedElements = $this->parseElementsToArray($loadedFileData, $accountNumber);

        return $this->saveToXml($parsedElements, $accountNumber);
    }

    /**
     * @param $filePath
     * @return array|string
     */
    private function loadFile($filePath)
    {

        $data = file_get_contents($filePath);

        $converted = iconv("ISO-8859-2","UTF-8", $data);
        $data = array_filter(explode("\r\n", $converted));

        return $data;
    }

    /**
     * @param array $data
     * @param $accountNumber
     *
     * @return array
     */
    private function parseElementsToArray(array $data, $accountNumber)
    {
        $transactions = [];

        foreach($data as $element) {
            $transactions['Transaction'][] = $this->parseSingleElement($element, $accountNumber);
        }

        return $transactions;
    }

    /**
     * @param $element
     * @param $accountNumber
     * @return array
     */
    private function parseSingleElement($element, $accountNumber)
    {
        preg_match($this->pattern, $element, $result);

        if (
            false == isset($result[1]) ||
            false == isset($result[2]) ||
            false == isset($result[3]) ||
            false == isset($result[4]) ||
            empty($result[1]) ||
            empty($result[2]) ||
            empty($result[3]) ||
            empty($result[4])
        ) {
            if (!strpos($element,'PRZELEW POMIĘDZY WŁASNYMI RACHUNKAMI')) {
                throw new \Exception($element);
            }
        }

        $oldDate = $result[1];
        $date = $result[1].'/2015';
        $data = [
            'date' => $date,
            'description' => $result[3],
            'amount' => str_replace(' ', '', $result[4]),
            'account_number' => $accountNumber,
            'account_name' => null,
            'transaction_balance' => null,
            'transaction_type' => null,
        ];

        $ddd = substr_count($element, $oldDate);

        if ($ddd > 1) {
            throw new \Exception('Plik jest nieprawidlowy, prawdopodobnie niektore rekordy nie zostaly rozdzielone nowa linia');
        }

        return $data;
    }

    private function saveToXml($data, $filePath)
    {
        $xml = Array2XML::createXML('Transactions', $data);
        $path = __DIR__.'/bank_files/xml/'.$filePath.'.xml';
        $xml->save($path);

        return $path;
    }
}

