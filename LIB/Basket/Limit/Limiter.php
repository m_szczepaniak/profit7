<?php

namespace Basket\Limit;

use Common;
use Exception;

/**
 * Class Limiter
 * @package Basket\Limit
 */
class Limiter
{
    /**
     * @var ProductLimit[]
     */
    private $productsLimits = [];

    /**
     * Limiter constructor.
     */
    public function __construct() {}

    /**
     * @param array $products
     */
    private function setProductsLimits(array $products)
    {
        foreach ($products as $product) {
            if (isset($this->productsLimits[$product['id']])) {

                continue;
            }

            // TODO TUTAJ WBIJAMY SIĘ z limitem ilości na wydawcy.
            $promotionLimit = (int)Common::GetOne('
            SELECT PP.product_limit
            FROM products_tarrifs AS PT
            JOIN products_promotions AS PP
                ON PT.promotion_id = PP.id
                AND PP.product_limit > 0
                AND PP.start_date <= NOW()
                AND PP.end_date >= NOW()
            WHERE PT.product_id = '.$product['id'].'
            AND PT.promotion_id IS NOT NULL
            ORDER BY PP.product_limit DESC
            LIMIT 1
            '
            );

            $this->productsLimits[$product['id']] = new ProductLimit($product['id'], $promotionLimit);
        }
    }

    /**
     * @param array $products
     * @throws Exception
     */
    private function prepareProductsLimits(array $products)
    {
        if (empty($products)) {

            throw new Exception("Products ids haven't been passed");
        }

        $this->setProductsLimits($products);
    }

    /**
     * @param array $products
     * @return array
     */
    public function determineMaximumAmount(array $products)
    {
        $availableProducts = [];

        $this->prepareProductsLimits($products);

        foreach ($products as $product) {

            $array = [
                'requested' => $product['quantity']
            ];

            if (null == $this->productsLimits[$product['id']]) {

                $available = $product['quantity'];
            } else {

                /** @var ProductLimit $productLimit */
                $productLimit = $this->productsLimits[$product['id']];
                $available = $productLimit->getAvailableQuantity($product['quantity']);
            }

            $array['available'] = $available;
            $array['fully_available'] = $product['quantity'] == $available ? true : false;

            $availableProducts[] = $array;
        }

        return $availableProducts;
    }

    /**
     * @param $data
     * @return bool|string
     */
    public function checkLimits($data)
    {
        $invalidProducts = [];

        $i = 0;
        foreach($_SESSION['wu_cart']['products'] as $productId => $product) {

            $limitData = $data[$i];

            if ($limitData['fully_available'] == false) {

                $_SESSION['wu_cart']['products'][$productId]['quantity'] = $limitData['available'];
                $invalidProducts[] = [
                    'id' => $productId,
                    'name' => $product['name'],
                    'new_quantity' => $limitData['available']
                ];
            }

            $i++;
        }

        if (!empty($invalidProducts)) {

            $msg = "Przekroczono limit dla następujących produktów i zmieniono ich ilość:<br/>";

            foreach($invalidProducts as $invalidProduct) {

                $msg .= '- '.$invalidProduct['name'].': ilość: '.$invalidProduct['new_quantity']."<br>";
            }

            $msg .= "<br><br><h5>Aby uczciwie traktować wszystkich naszych Klientów ograniczyliśmy
możliwość zakupu tego produktu. Jeżeli ograniczenie zostanie przekroczone
w innym zamówieniu może zostać ono anulowane.</h5>";

            return $msg;
        }
        return true;
    }
}