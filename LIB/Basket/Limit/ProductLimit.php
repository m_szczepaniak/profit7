<?php

namespace Basket\Limit;

/**
 * Class Limiter
 * @internal
 * @package Basket\Limit
 */
class ProductLimit
{
    /**
     * @var int
     */
    private $productId;

    /**
     * @var int
     */
    private $promotionLimit;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $checkLimitActive = true;

    /**
     * ProductLimit constructor.
     * @param $productId
     * @param $promotionLimit
     */
    public function __construct($productId, $promotionLimit)
    {
        $this->productId = $productId;
        $this->promotionLimit = $promotionLimit;

        $this->limit = $promotionLimit;

        if ($promotionLimit < 1) {

            $this->checkLimitActive = false;
        }
    }

    /**
     * @param $quantity
     * @return int
     */
    public function getAvailableQuantity($quantity)
    {
        if ($this->checkLimitActive == false) {

            return $quantity;
        }

        $aval = $this->limit - $quantity;

        if ($aval <= 0) {

            $available = $this->limit;
        } else {

            $available = $quantity;
        }

        $this->limit -= $available;

        return $available;
    }
}