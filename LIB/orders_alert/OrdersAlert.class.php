<?php
/**
 * Klasa do obsługi automatycznych zdarzeń zamówień
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-03 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders_alert;

class OrdersAlert {
  private $pObj = NULL;
  private $aAlerts = array();

  function __construct() {

  }
  
  
  /**
   * Metoda uruchamia parsowanie klas pomocniczych w celu pobrania alertów
   * 
   */
  public function getAutoAlertsClassElements() {
    
    $this->pObj = new \StdClass;
    
    if ($handle = opendir($_SERVER['DOCUMENT_ROOT'].'/LIB/orders_alert/alerts/')) {
      while (false !== ($sFileName = readdir($handle))) {
        if ($sFileName != "." && $sFileName != ".." && stristr($sFileName, '.class.php')) {
          $sClassName = '\orders_alert\alerts\\'.str_replace('.class.php', '', $sFileName);
          
          //include_once($_SERVER['DOCUMENT_ROOT'].'/LIB/orders_alert/alerts/'.$sFileName);
          $this->pObj->$sClassName = new $sClassName();
        }
      }
      closedir($handle);
    }
  }
  
  /**
   * Metoda tworzy nowy alert ręcznie
   * 
   * @param int $iOrderId
   * @param string $sCreatedBy
   * @param string $sMessage
   * @param DateTime $dAlertDateTime
   * @param int $iOrderItemId
   * @param int $iUserId
   * @return boolean
   */
  public function createAlert($iOrderId, $sCreatedBy, $sMessage, $dAlertDateTime = 'NULL', $iOrderItemId = 'NULL', $iUserId = 'NULL') {
    
      $aAlert = array();
      $aAlert['order_id'] = $iOrderId;
      $aAlert['order_item_id'] = $iOrderItemId;
      $aAlert['user_id'] = $iUserId;
      $aAlert['alert'] = '1';
      $aAlert['alert_datetime'] = $dAlertDateTime;
      
      $aAlert['created'] = 'NOW()';
      $aAlert['created_by'] = $sCreatedBy;
      $aAlert['message'] = $sMessage;
      $this->aAlerts['manual'][] = $aAlert;
      return true;
  }// end of createAlert() method
  
  
  /**
   * Metoda dodaje wszystkie alerty
   * 
   * @global array $aConfig
   */
  public function addAlerts() {
    global $aConfig;
    
    if (!empty($this->aAlerts)) {
      foreach ($this->aAlerts as $sAlertType => $aAlerts) {
        foreach ($aAlerts as $iKAlert => $aAlert) {
          if ($this->_insertAlert($aAlert) === false) {
            $this->_sendMail("Wystąpił błąd podczas dodawania alertu", "Wystąpił błąd podczas dodawania alertu ".print_r($aAlert, true));
          }
        }
      }
    }
  }// end of addAlerts() method
  
  
  /**
   * Metoda ma wysyłać maila 
   * @todo trzeba dodać tutaj wysyłanie maila
   * 
   */
  public function sendAlertsMail() {
    global $aConfig;
    
    //'Wystąpił błąd podczs pobierania nr listu przewozowego - lista błedów'
    if (!empty($this->aAlerts)) {
      return \Common::sendMail('', $aConfig['common']['import_sender_email'], $aConfig['common']['import_send_to'], 'Errror', var_export($this->aAlerts, true));
    } else {
      return true;
    }
  }// end of sendAlertsMail() method
  
  
  /**
   * Metoda usuwa duplikaty alertów
   * 
   * @return void
   */
  public function removeDuplicates() {
    $aUniqeKeys = array(
        'order_id',
        'order_item_id',
        'user_id',
//        'message',
//        'comments',
        'alert_datetime'
    );
    
    $aNewAlertCompare = array();
    foreach ($this->aAlerts as $sAlertName => $aAlerts) {
      foreach ($aAlerts as $sAlertsKey => $aAlert) {
        foreach ($aAlert as $sAKey => $sAValue) {
          if (in_array($sAKey, $aUniqeKeys)) {
            $aNewAlertCompare[$sAlertName][$sAlertsKey][$sAKey] = $sAValue;
          }
        }
        
        // tutaj wypadalo by czy jest zamówienie w bazie z identycznymi wartościami
        if ($this->_checkAlertEqual($aNewAlertCompare[$sAlertName][$sAlertsKey]) === TRUE) {
          unset($this->aAlerts[$sAlertName][$sAlertsKey]);
        }
      }
    }
  }// end of removeDuplicates() method



    /**
     * Metoda usuwa duplikaty alertów
     *
     * @return void
     */
    public function removeDuplicatesMessage() {
        $aUniqeKeys = array(
//            'order_id',
//            'order_item_id',
//            'user_id',
        'message',
//        'comments',
        );

        $aNewAlertCompare = array();
        foreach ($this->aAlerts as $sAlertName => $aAlerts) {
            foreach ($aAlerts as $sAlertsKey => $aAlert) {
                foreach ($aAlert as $sAKey => $sAValue) {
                    if (in_array($sAKey, $aUniqeKeys)) {
                        $aNewAlertCompare[$sAlertName][$sAlertsKey][$sAKey] = $sAValue;
                    }
                }

                // tutaj wypadalo by czy jest zamówienie w bazie z identycznymi wartościami
                if ($this->_checkAlertEqual($aNewAlertCompare[$sAlertName][$sAlertsKey]) === TRUE) {
                    unset($this->aAlerts[$sAlertName][$sAlertsKey]);
                }
            }
        }
    }// end of removeDuplicates() method
  
  
  /**
   * Metoda na obiekcie Alerts wywołuje metodę getAlertElements, 
   *  która to z kolei pobiera alerty z klas potomnych
   * 
   * @return void
   */
  public function getAutoAlerts() {
    
    foreach ($this->pObj as $sAlert => $oAlert) {
      $this->aAlerts[$sAlert] = $oAlert->getAlertElements();
    }
  }// end of getAutoAlerts() method
  
  
  /**
   * Metoda sprawcza czy istnieje już identyczny komunikat
   * 
   * @global object $pDbMgr
   * @param array $aAlert
   * @return bool
   */
  private function _checkAlertEqual($aAlert) {
    global $pDbMgr;
    
    $sSql = "SELECT id FROM orders_to_review WHERE 1=1 AND ";
    foreach ($aAlert as $sCol => $sVal) {
      $sSql .= $sCol;
      if ($sVal != 'NULL' && $sVal != null) {
        $sSql .= ' = "'.$sVal.'" ';
      } else {
        $sSql .= ' IS NULL ';
      }
      $sSql .= 'AND ';
    }
    $sSql = substr($sSql, 0, -4);
    return (intval($pDbMgr->GetOne('profit24', $sSql)) > 0 ? TRUE : FALSE);
  }// end of _checkAlertEqual() method
  
  
  /**
   * Metoda dodaje alert do bazy danych
   * 
   * @global \DatabaseManager $pDbMgr
   * @global \DB $pDB
   * @param array $aAlert
   * @return mixed
   */
  private function _insertAlert($aAlert) {
    global $pDbMgr, $pDB;
    return $pDbMgr->Insert('profit24', 'orders_to_review', $aAlert);
  }// end of _insertAlert() method
  
  
  /**
   * Metoda wysyła mail do uzytkownikow
   * 
   * @param type $sTopic
   * @param type $sContent
   * @return bool
   */
  private function _sendMail($sTopic, $sContent) {
    global $aConfig;
    
    return \Common::sendMail('', $aConfig['common']['import_sender_email'], $aConfig['common']['import_send_to'], $sTopic, $sContent);
  }
}// end of class