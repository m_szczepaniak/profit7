<?php
/**
 * Klasa alertów zapowiedzi
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-04-03 
 * @copyrights Marcin Chudy - Profit24.pl
 */
namespace orders_alert\alerts;
class Previews {

  function __construct() {}
  
  /**
   * Metoda pobiera przeterminowane zamówienia
   * 
   * Zamówienia:
   *  nie anulowane, i nie zrealizowane,
   *  oraz jeśli składowa zamówienia jest zapowiedzią
   *  oraz których składowa jeśli istnieje current_shipment_date <= dzisiaj, jeśli nie to shipment_date <= dzisiaj
   *  oraz których status składoej jest wcześniejszy niż "jest potwierdzone"
   * 
   * @return array zamówienia
   */
  private function _getOrdersPreviewsOutdated() {
    global $aConfig, $pDbMgr;
    
    $sSql = "SELECT O.order_number, OI.*
             FROM ".$aConfig['tabls']['prefix']."orders_items AS OI
             JOIN ".$aConfig['tabls']['prefix']."orders AS O
              ON OI.order_id = O.id
             WHERE 
              O.order_status <> '4' AND 
              O.order_status <> '5' AND
              OI.status <> '4' AND
              OI.preview = '1' AND
              OI.deleted <> '1' AND
              IF(current_shipment_date IS NOT NULL, OI.current_shipment_date <= CURDATE(), OI.shipment_date <= CURDATE())
             ";
    return $pDbMgr->GetAll('profit24', $sSql);
  }// end of _getOrdersPreviewsOutdated() method
  
  
  /**
   * Metoda tworzy alerty na podstawie przekazanej tablicy składowych zamówień
   * 
   * @param array $aOrdersItems
   * @return array
   */
  private function _createAlert($aOrdersItems) {
    $aAlerts = array();
    $oMessage = new \messages\Messages();
    $sAlertMSG = $oMessage->getWebsiteMessage("alert_brak_zapowiedzi");
    
    foreach ($aOrdersItems as $iKey => $aOItem) {
      $aAlerts[$iKey] = array();
      $aAlerts[$iKey]['order_id'] = $aOItem['order_id'];
      $aAlerts[$iKey]['order_item_id'] = $aOItem['id'];
      $aAlerts[$iKey]['user_id'] = 'NULL';
      $aAlerts[$iKey]['alert'] = '1';
      if ($aOItem['current_shipment_date'] != '') {
        $aAlerts[$iKey]['alert_datetime'] = $aOItem['current_shipment_date'];
      } elseif($aOItem['shipment_date']) {
        $aAlerts[$iKey]['alert_datetime'] = $aOItem['shipment_date'];
      } else {
        $aAlerts[$iKey]['alert_datetime'] = 'NOW()';
      }
      
      $aAlerts[$iKey]['created'] = 'NOW()';
      $aAlerts[$iKey]['created_by'] = 'auto-zapowiedzi';
      $aAlerts[$iKey]['message'] = $oMessage->printWebsiteMessage($sAlertMSG, array_keys($aOItem), array_values($aOItem));
    }
    return $aAlerts;
  }// end of _createAlert() method
  
  
  /**
   * Metoda tworzy alerty
   * 
   * @return array
   */
  public function getAlertElements() {
    
    // pobieramy zamówienia do sprawdzenia
    $aOrdersItems = $this->_getOrdersPreviewsOutdated();
    
    // zrobmy z tych zamowień alerty
    return $this->_createAlert($aOrdersItems);
  }// end of getAlertElements() method

} // end of class
?>