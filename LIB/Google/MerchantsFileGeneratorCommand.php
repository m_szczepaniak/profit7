<?php

use Ceneo\HelperFunctions;
use Ceneo\Limit;
use Ceneo\PriceComparer;
use Ceneo\ProductsBuffer;
use LIB\Google\FileGenerator;

/**
 * Klasa testowa dla komunikacji zamówień z Ateneum
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-04-21
 * @copyrights Marcin Chudy - Profit24.pl
 */

error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED);

ini_set('display_errors', 'On');
ini_set('safe_mode', 'Off');
ini_set('max_execution_time', 1500);
ini_set('memory_limit', '4096M');

$aConfig['config']['project_dir'] = 'LIB/Ceneo/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
require_once($_SERVER['DOCUMENT_ROOT'].'/import/import_func.inc.php');


include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php');

global $pDbMgr;

$argv['1'] = 'smarkacz';

/**
 * START
 */

$availableShops = [
    'np', 'profit24', 'smarkacz'
];

if (!isset($argv['1']) || !in_array($argv['1'], $availableShops)) {

    exit("Podany sklep nie jest obslugiwany lub nie zostal podany");
}

$code = $argv[1];

var_dump($code);

if ($code == 'np') {
    $fileGenerator = new FileGenerator('np', 'https://www.nieprzeczytane.pl');
}
else if($code == 'profit24') {
    $fileGenerator = new FileGenerator('profit24', 'https://www.profit24.pl');
}
else if($code == 'smarkacz') {
    $fileGenerator = new FileGenerator('smarkacz', 'https://www.smarkacz.pl');
}

$fileGenerator->generateXmlFile();
