<?php
/**
 * @author Lukasz Jaskiewicz <lucas.jaskiewicz@gmail.com>.
 * @created: 2015-12-15
 * @copyrights Marcin Chudy - Profit24.pl
 */

namespace LIB\Google;

use DateTime;
use Google\Shop\MerchantAbstract;
use Google\Shop\MerchantInterface;
use LIB\Helpers\ArrayHelper;
use orders\MarkUpCookie\AllUsers;
use table\keyValue;

include_once ('../../import/import_func.inc.php');

class FileGenerator
{
    /**
     * @var string
     */
    private $websiteCode;

    /**
     * @var \DatabaseManager
     */
    private $pDbMgr;
    private $websiteLink;

    private $oKeyValue;
    private $oCommonOfertaProduktowa;

    /** @var MerchantInterface|MerchantAbstract */
    private $merchantShop;
    /**
     * @var string
     */
    private $env;


    public function __construct($websiteCode, $websiteLink, $env = 'dev')
    {
        global $pDbMgr;

        $this->env = $env;
        $this->websiteCode = $websiteCode;
        $this->pDbMgr = $pDbMgr;
        $this->websiteLink = $websiteLink;
        $this->oKeyValue = new keyValue('mark_up_cookie', $this->websiteCode);
        include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/modules/m_oferta_produktowa/client/Common.class.php');
        $this->oCommonOfertaProduktowa = new \Common_m_oferta_produktowa();
        $this->oAllUsersMarkUp = new AllUsers($websiteCode);

        $merhantName = "\\Google\\Shop\\".ucfirst($websiteCode).'Merchant';
        $this->merchantShop = new $merhantName($this->websiteCode, $this->env);
    }

    public function generateXmlFile()
    {
        $fileName = $this->websiteCode.'_merchant';

        $products = $this->findProductsToGenerate();

        if (empty($products)) {
            return false;
        }

        $generatedXml = $this->prepareXmlFile($fileName, $this->websiteLink, 'Oferta nieprzeczytane.pl', $products);

        $this->saveXml($fileName, $generatedXml);
    }

    /**
     * @param $shopCode
     * @return string
     */
    private function getOverwrittenProductsIds($shopCode)
    {
        global $pDbMgr;

        $array = [
            'in' => [],
            'notIn' => [],
        ];

        $sql = "SELECT product_id, website_id FROM ceneo_product_priority WHERE website_id IS NOT NULL";

        $result = $pDbMgr->GetAll('profit24', $sql);

        if (empty($result)) {

            return $array;
        }

        foreach($result as $product) {

            if ($product['website_id'] == $shopCode) {

                $array['in'][] = $product['product_id'];

                continue;
            }

            $array['notIn'][] = $product['product_id'];
        }

        return $array;
    }

    private function addInProducts(array $oProducts, array $inProducts)
    {
        $sSql = $this->merchantShop->getGenerateSql(422, ['K'], $inProducts, true);

        $oProducts2 = $this->pDbMgr->GetAll($this->websiteCode, $sSql);

        if (empty($oProducts2)) {

            return $oProducts;
        }

        $oProducts2 = ArrayHelper::toKeyValues('p24id', $oProducts2);

        foreach($oProducts2 as $p24Id => $product) {

            $oProducts[$p24Id] = $product;
        }

        return $oProducts;
    }

    /**
     * @return array
     */
    private function getSmarkaczAdultProducts()
    {
        if ($this->websiteCode != 'smarkacz') {

            return [];
        }

        global $pDbMgr;

        $sql = "
            SELECT id, profit24_id, name, description, isbn FROM `products`
            WHERE
            (
                `name` LIKE '%erotyczn%'
                OR `name` LIKE '%erotic%'
                OR `name` LIKE '%gra tylko dla dorosłych%'
                OR `name` LIKE '%gra tylko dla doroslych%'
                OR `name` LIKE '%seks%'
                OR `name` LIKE '%seksie%'
                OR `name` LIKE '%sex%'
                OR `description` LIKE '%erotyczn%'
                OR `description` LIKE '%erotic%'
                OR `description` LIKE '%gra tylko dla dorosłych%'
                OR `description` LIKE '%gra tylko dla doroslych%'
                OR `description` LIKE '%seks %'
                OR `description` LIKE '%seksie%'
                OR `description` LIKE '% sex %'
            )
            AND `name` NOT LIKE '%sexting%'
            AND `description` NOT LIKE '%sexting%'
        ";

        $res = $pDbMgr->GetAll($this->websiteCode, $sql);

        if (empty($res)) {

            return [];
        }

        var_dump(count($res));

        return ArrayHelper::toKeyValues('profit24_id', $res);
    }

    private function findProductsToGenerate()
    {
        global $aConfig;
        $shopCode = MerchantAbstract::getCodeBySymbol($this->websiteCode);
        $sSql = $this->merchantShop->getGenerateSql(422, ['K']);

        $oProducts = $this->pDbMgr->GetAll($this->websiteCode, $sSql);
        $oProducts = ArrayHelper::toKeyValues('p24id', $oProducts);

        $productIds = $this->getOverwrittenProductsIds($shopCode);

        if (!empty($productIds['in'])) {

            $oProducts = $this->addInProducts($oProducts, $productIds['in']);
        }

        $smarkaczAdultProducts = $this->getSmarkaczAdultProducts();

        $newProducts = [];
        foreach($oProducts as $p24Id => $product) {

            if (in_array($p24Id, $productIds['notIn'])) {

                continue;
            }

            $preparedProduct = $this->prepareSingleProduct($product, $smarkaczAdultProducts);

            if (null === $preparedProduct) {

                continue;
            }

            $newProducts[$product['id']][] = $preparedProduct;
        }

        return $newProducts;
    }

    private function prepareGtin(array $product)
    {
        if (!empty($product['ean_13'])) {

            return str_replace('-', '', $product['ean_13']);
        }

        if (!empty($product['isbn_13'])) {

            return str_replace('-', '', $product['isbn_13']);
        }

        if (!empty($product['isbn_10'])) {

            return str_replace('-', '', $product['isbn_10']);
        }

        return null;
    }

    private function prepareSingleProduct(array $product, array $smarkaczAdultProducts)
    {
        global $aConfig;

        $imgLink = $this->getProductImg($product['id']);

        if ($aConfig['common']['status'] != 'development') {
            if (empty($imgLink)) {
                return null;
            }
        }


        $aTarrif = $this->oCommonOfertaProduktowa->getTarrif($product['id'], $this->websiteCode);
        $aTarrif = $this->oAllUsersMarkUp->getMarkup($product['price_brutto'], $aTarrif, $aTarrif['discount'], false);


        $newProduct = [];
        $newProduct['p24id'] = $product['p24id'];
        $newProduct['is_bestseller'] = $product['is_bestseller'];
        $newProduct['prod_status'] = $product['prod_status'];
        $newProduct['is_custom_news'] = $product['is_custom_news'];
        $sPlainName = mb_substr(htmlspecialchars($product['plain_name'],ENT_QUOTES,'UTF-8',false), 0, 150, 'UTF-8');
        $newProduct['title'] = $sPlainName;
        $newProduct['description'] = mb_substr(htmlspecialchars(stripslashes(strip_tags(clearAsciiCrap(($product['description'])))),ENT_QUOTES,'UTF-8',false), 0, 5000, 'UTF-8');
        $newProduct['unique_id'] = $this->websiteCode.$product['id'];
        $newProduct['link'] = $this->createProductLink($product['id'], $product['plain_name']);
        $newProduct['image_link'] = $this->websiteLink.'/'.$imgLink;
        $newProduct['price'] = $aTarrif['price_brutto'];
        $newProduct['condition'] = 'new';
        $newProduct['category'] = $this->websiteCode == 'smarkacz' ? 1239 : 784;
        $newProduct['gtin'] = $this->prepareGtin($product);
        $newProduct['custom_cat'] = $this->getProductCat($product['id']);
 ;
        if ($product['shipment_date'] != null && $product['is_previews'] == 1 && $product['is_in_preview'] != null && $product['shipment_date'] != '0000-00-00') {

            $tempDate = new \DateTime($product['shipment_date']);
            $c = $tempDate->format(DateTime::ISO8601);
            $newProduct['availability'] = 'preorder';
            $newProduct['availability_date'] = $c;
        } else {

            $newProduct['availability'] = 'in stock';
            $newProduct['availability_date'] = null;
        }

        if (!empty($product['publisher_name']) && $product['publisher_name'] != '') {

            $newProduct['brand'] = $product['publisher_name'];
        }

        if ($this->websiteCode == 'smarkacz') {

            $newProduct['adult'] = 'no';

            if (isset($smarkaczAdultProducts[$product['p24id']])) {

                $newProduct['adult'] = 'yes';
            }
        }

        return $newProduct;
    }

    /**
     * @param $p24id
     * @return mixed
     */
    private function getMainCategoryName($p24id) {
        $sSql = 'SELECT MI.name AS main_category_name
                 FROM products AS P
                 JOIN menus_items AS MI
                  ON MI.id = P.main_category_id
                 WHERE P.id = '.$p24id.'
                 LIMIT 1
                 ';
        return $this->pDbMgr->GetOne('profit24', $sSql);
    }


    /**
     * @param array $products
     * @return string
     */
    private function prepareXmlItems(array $products)
    {
        $items = '';

        foreach($products as $productCollection) {

            foreach ($productCollection as $product) {

                $product['main_category_name'] = $this->getMainCategoryName($product['p24id']);

                $xItem = '<item>'."\n";
                $xItem .= '<title>' . $this->cutStringByWordsLength(htmlspecialchars($this->clean_string($product['title']), ENT_XML1, 'UTF-8'), 20) . '</title>'."\n";
                $xItem .= '<description>' . htmlspecialchars($this->clean_string($product['description']), ENT_XML1, 'UTF-8') . '</description>'."\n";
//                $xItem .= '<description>' . trim(htmlspecialchars($product['description'], ENT_XML1, 'UTF-8')) . '</description>'."\n";
                $xItem .= '<link>' . htmlspecialchars($product['link'], ENT_XML1, 'UTF-8') . '</link>'."\n";
                $xItem .= '<g:id>' . htmlspecialchars($product['unique_id'], ENT_XML1, 'UTF-8') . '</g:id>'."\n";
                $xItem .= '<g:image_link>' . htmlspecialchars($product['image_link'], ENT_XML1, 'UTF-8') . '</g:image_link>'."\n";
                $xItem .= '<g:condition>' . htmlspecialchars($product['condition'], ENT_XML1, 'UTF-8') . '</g:condition>'."\n";
                $xItem .= '<g:price>' . htmlspecialchars($product['price'], ENT_XML1, 'UTF-8') . ' PLN</g:price>'."\n";
                $xItem .= '<g:gtin>' . htmlspecialchars($product['gtin'], ENT_XML1, 'UTF-8') . '</g:gtin>'."\n";
                $xItem .= '<g:google_product_category>' . htmlspecialchars($product['category'], ENT_XML1, 'UTF-8') . '</g:google_product_category>'."\n";
                $xItem .= '<g:availability>' . htmlspecialchars($product['availability'], ENT_XML1, 'UTF-8') . '</g:availability>'."\n";
                $xItem .= '<g:product_type>' . htmlspecialchars($product['custom_cat'], ENT_XML1, 'UTF-8') . '</g:product_type>'."\n";
                $xItem .= '<g:custom_label_0>' . htmlspecialchars($product['main_category_name'], ENT_XML1, 'UTF-8') . '</g:custom_label_0>'."\n";
                $xItem .= '<g:custom_label_1>' . ($product['is_bestseller'] == '1' ? 'Bestseller_tak' : 'Bestseller_nie') . '</g:custom_label_1>'."\n";
                $xItem .= '<g:custom_label_2>' . ($product['is_custom_news'] == '1' ? 'Nowość' : ( $product['prod_status'] == '3' ? 'Zapowiedź' : 'Standard' )) . '</g:custom_label_2>'."\n";

                if (isset($product['adult'])) {

                    $xItem .= '<g:adult>' . $product['adult'] . '</g:adult>'."\n";
                }

                if (isset($product['brand'])) {

                    $xItem .= '<g:brand>' . htmlspecialchars($product['brand'], ENT_XML1, 'UTF-8') . '</g:brand>'."\n";
                }

                if ($product['availability_date'] != null) {

                    $xItem .= '<g:availability_date>' . htmlspecialchars($product['availability_date'], ENT_XML1, 'UTF-8') . '</g:availability_date>'."\n";
                }

                $xItem .= '</item>'."\n";

                $items .= $xItem;
            }
        }

        return $items;
    }

    private function prepareXmlFile($fileName, $source, $description, array $products)
    {
        $xmlBase = '<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">
<channel>
<title>'.$fileName.'</title>
<link>'.$source.'</link>
<description>'.$description.'</description>
%s
</channel>
</rss>';

        $preparedFiles = $this->prepareXmlItems($products);

        $xml = sprintf($xmlBase, $preparedFiles);

        return $xml;
    }

    function clean_string($string) {
        $s = trim(strip_tags($string));
        $s = iconv("UTF-8", "UTF-8//IGNORE", $s); // drop all non utf-8 characters

        // this is some bad utf-8 byte sequence that makes mysql complain - control and formatting i think
        $s = preg_replace('/(?>[\x00-\x1F]|\xC2[\x80-\x9F]|\xE2[\x80-\x8F]{2}|\xE2\x80[\xA4-\xA8]|\xE2\x81[\x9F-\xAF])/', ' ', $s);

        $s = preg_replace('/\s+/', ' ', $s); // reduce all multiple whitespace to a single space

        return $s;
    }

    private function saveXml($fileName, $data)
    {
        $path = $_SERVER['DOCUMENT_ROOT'].$fileName.'.xml';
        dump($path);
        file_put_contents($path, $data);
    }

    private function createProductLink($iId, $sName='') {
        global $aConfig;

        $sPageLink = $this->websiteLink.'/';
        if($sName != ''){
            if(mb_strlen($sName, 'UTF-8') > 210) {
                $sPageLink .= mb_substr(link_encode($sName),0,210, 'UTF-8').',';
            } else {
                $sPageLink .= link_encode($sName).',';
            }
        }
        $sPageLink .= 'product'.$iId;
        $sPageLink .= '.html';
        return $sPageLink;
    }

    function getProductImg($iId) {
        global $aConfig;

        $sSql = "SELECT directory, photo, mime
					 FROM products_images
				 	 WHERE
				 	 product_id = ".$iId."
				 	 ORDER BY order_by LIMIT 0, 1";
        $aImg = $this->pDbMgr->GetRow($this->websiteCode, $sSql);

        if (!empty($aImg)) {
            $sDir = $aConfig['common']['photo_dir'];
            if (!empty($aImg['directory'])) {
                $sDir .= '/'.$aImg['directory'];
            }

            $externalLink = $this->websiteLink.'/'.$sDir.'/__b_'.$aImg['photo'];

            if (@getimagesize($externalLink)) {

                return $sDir.'/__b_'.$aImg['photo'];

            } else {

                return $sDir.'/'.$aImg['photo'];
            }
        }
        return null;
    }

    function getProductCat($iId) {
        global $aConfig;
        $sSql = "SELECT A.name
						FROM menus_items A
						JOIN products_extra_categories B
						ON A.id=B.page_id
					 WHERE B.product_id = ".$iId."
					ORDER BY A.priority
					LIMIT 1";
        return $this->pDbMgr->GetOne($this->websiteCode, $sSql);
    }

    /**
     * @param $string
     * @param int $maxWords
     * @return string
     */
    public function cutStringByWordsLength($string, $maxWords = 30)
    {
        return mb_substr($string,0, 149, 'UTF-8');

        $phrase_array = explode(' ', $string);

        if (count($phrase_array) > $maxWords && $maxWords > 0) {

            $string = implode(' ', array_slice($phrase_array, 0, $maxWords));
        }

        return $string;
    }
}
