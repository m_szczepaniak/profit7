<?php
/**
 * Created by PhpStorm.
 * User: lukasz
 * Date: 30.01.17
 * Time: 13:20
 */

namespace Google\Shop;

use Exception;

abstract class MerchantAbstract implements MerchantInterface
{
    /**
     * @var
     */
    protected $websiteCode;
    /**
     * @var
     */
    private $env;

    /**
     * @param $websiteCode
     */
    public function __construct($websiteCode, $env)
    {
        $this->websiteCode = $websiteCode;
        $this->env = $env;
    }

    /**
     * @param int $unsortedCategory
     * @param array $allowedProductTypes
     * @param array $productsIds
     * @param bool $ignoreJoin
     * @return string
     * @throws Exception
     */
    public function getGenerateSql($unsortedCategory = 422, array $allowedProductTypes = ['K', 'A'], array $productsIds = null, $ignoreJoin = false)
    {
        global $pDbMgr;

        $sSql = "SELECT *  FROM `products_tags` WHERE `tag` = 'czasopisma'";
        $iTagId = $pDbMgr->GetOne($this->websiteCode, $sSql);
        $extraCondition = '';

        $operator = "IN";
        if ($this->websiteCode == 'smarkacz') {

            $operator = "NOT IN";
            $extraCondition = "AND A.shipment_time = '0'";
        }

        //$aConfig['import']['unsorted_category']
        //$aConfig['common']['allowed_product_type']
        $shopCode = $this->getCodeBySymbol($this->websiteCode);

        $column = ' A.id';

        if ($this->websiteCode != 'profit24') {

            $column = ' A.profit24_id';
        }

            $sSql = "SELECT $column as p24id, A.id, A.plain_name, A.isbn_plain, A.isbn_10, A.isbn_13, A.ean_13, A.pages, A.price_brutto, A.description, PPO.id as is_in_preview, A.is_previews, A.shipment_date,
                                    A.shipment_time, B.name AS publisher_name,
                                    C.binding AS binding_name, D.dimension AS dimension_name,
                                    A.packet, A.weight
                        FROM products A ";

        if ($this->websiteCode != 'smarkacz' && $ignoreJoin == false) {

            if ($this->websiteCode == 'np') {

                $sSql .= "
                JOIN main_profit24.website_ceneo_categories AS WCC ON WCC.category_id = (SELECT main_category_id from main_profit24.products where id = A.profit24_id) AND (WCC.website_id = 4 OR WCC.website_id = 3)
                ";
            }
            elseif ($this->websiteCode != 'profit24') {

                $sSql .= "
                JOIN main_profit24.website_ceneo_categories AS WCC ON WCC.category_id = (SELECT main_category_id from main_profit24.products where id = A.profit24_id) AND WCC.website_id = $shopCode
                ";
            } else {

                //TODO TUTAJ
                $sSql .= "
                JOIN main_profit24.website_ceneo_categories AS WCC ON WCC.category_id = A.main_category_id AND WCC.website_id = $shopCode
                ";
            }
        }

        $sSql .= "
        LEFT JOIN products_publishers B ON B.id = A.publisher_id
                        LEFT JOIN products_bindings C ON C.id=A.binding
                        LEFT JOIN products_dimensions D ON C.id=A.dimension
                        LEFT JOIN products_images I ON I.product_id = A.id
                        LEFT JOIN products_previews AS PPO ON A.id = PPO.product_id
                        LEFT JOIN products_to_tags PT ON PT.product_id = A.id AND PT.tag_id = ".$iTagId."
                        WHERE A.published = '1'
                        ";
        if (!empty($productsIds)) {
            $imploded = implode(',', $productsIds);
            $sSql .=" AND $column IN($imploded) ";
        }
        $sSql .=        "
                        AND PT.tag_id IS NULL
                        AND A.packet = '0'
                        AND (A.prod_status = '1' OR A.prod_status = '3')

                        AND A.page_id <> ".$unsortedCategory."
                        AND A.product_type $operator (\"".implode('", "', $allowedProductTypes)."\")
                        $extraCondition
                        GROUP BY A.id
                        ";

        if ($this->env = 'dev') {
//            $sSql .= "
//                LIMIT 10";
        }

        //AND (A.shipment_time = '0' OR A.shipment_time='1')

        return $sSql;
    }

    public static function getCodeBySymbol($key)
    {
        $codes = [
            'np' => 3,
            'profit24' => 1,
            'mestro' => 4,
            'smarkacz' => 5
        ];

        if (false == isset($codes[$key])) {

            throw new Exception("Website with code $key does not exist");
        }

        return $codes[$key];
    }
}