<?php
/**
 * Created by PhpStorm.
 * User: lukasz
 * Date: 30.01.17
 * Time: 13:20
 */

namespace Google\Shop;

interface MerchantInterface
{
    public function getGenerateSql($unsortedCategory, array $allowedProductTypes);
}