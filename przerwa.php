<?php 
	if(!file_exists('przerwa/przerwa_konfiguracja.php')){
		header("location:http://".$_SERVER['SERVER_NAME']."/");
		exit(0);
	}
?><html>
	<head>
		<title>profit24</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<style type="text/css">
			body {font-family: Tahoma; background-color: #fff;}
			#kontener {width: 670px; margin: 0 auto; height: 400px; text-align: center; }
			#logo {background: url(/images/gfx/logo.gif) no-repeat;	width: 304px; height: 116px; margin: 0 auto 20px auto;}
			h1 {font-size: 22px; color: #ad0200; font-weight: normal;}
			h2 {font-size: 22px;}
			html {height: 100%;} 
			table {width: 100%; height: 100%; margin: 0;}
			#stopka {padding: 30px 0 0 0; font-size: 11px; color: #313030; }
			#realizacja {font-size: 11px; color: #313030; padding: 15px 0 0 0;}
			#realizacja a {color: #313030; text-decoration: none;}
			#realizacja a span {color: #e99a02;}
			#realizacja a:hover {text-decoration: underline;}
		</style>
	</head>
	<body>
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td valign="middle" style="text-align: center;">
					<div id="kontener">
						<div id="logo"></div>
						<h1 style="font-weight: normal"><?php 
							include_once('przerwa/przerwa_konfiguracja.php');
							echo $przerwa_wiadomosc; 
						?></h1>
                        <div id="stopka"><strong>Profit M Spółka z ograniczoną odpowiedzialnością</strong> Al. Jerozolimskie 134, 02-305 Warszawa<br /> NIP 525-22-45-459, KRS: 0000756053 Sąd Rejonowy dla miasta stołecznego Warszawy
						w Warszawie, XII Wydział Gospodarczy KRS</div>
						<div id="realizacja">realizacja i CMS: <strong><a href="http://www.omnia.pl">omnia<span>.</span>pl</a></strong></div>
					</div>
				</td>
			</tr>
		</table>
	</body>
</html>