<?php
// bootstrap.php
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
require_once "vendor/autoload.php";

$paths = array(__DIR__ . '/src/');

// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);


// the connection configuration
$conn = array(
    'driver'   => 'pdo_mysql',
    'user'     => 'root',
    'password' => 'golar#67',
    'dbname'   => 'main_profit24_old',
    'charset' => 'UTF8'
);

$config = Setup::createConfiguration($isDevMode);
$driver = new AnnotationDriver(new AnnotationReader(), $paths);

// registering noop annotation autoloader - allow all annotations by default
AnnotationRegistry::registerLoader('class_exists');
$config->setMetadataDriverImpl($driver);

$entityManager = EntityManager::create($conn, $config);
$conn = $entityManager->getConnection();
$conn->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');


//$user = $em->find('Users', 5);

//$person = new \Entities\Websites();
//$person->setCode('123');
//$entityManager->persist($person);
//$entityManager->flush();
//die;