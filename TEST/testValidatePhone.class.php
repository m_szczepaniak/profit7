<?php
/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-02 
 * @copyrights Marcin Chudy - Profit24.pl
 */

/**
 * Description of testValidatePhone
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
class testValidatePhone {
  private $aPhoneToValidate = array(
      '800891691',
      '800700123',
      '690892692',
      '690892692',
      '692728',
  );
  
  public function __construct() {
    include_once('../omniaCMS/lib/Form/ValidateFields/ValidatePhone.class.php');
    $oValudatePhone = new ValidatePhone();
    
    foreach ($this->aPhoneToValidate as $sPhone) {
      var_dump($oValudatePhone->validate($sPhone));
    }
  }
}
$oTestValidatePhone = new testValidatePhone();