<?php

use PriceUpdater\PriceModifier\PriceModifierStrategy;
use PriceUpdater\PriceUpdater;
use PriceUpdater\Tarrifs\TarrifsUpdater;

class PriceUpdaterTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var PriceUpdater null
     */
    private $priceUpdater = null;

    protected function setUp()
    {
        global $aConfig;

        include_once('DatabaseManager.class.php');
        $this->pDbMgr = new DatabaseManager($aConfig);
        $this->aConfig = $aConfig;
        $this->aConfig['price_updater']['our_book_stores']['current_book_store'] = "profit24.pl";
        $this->aConfig['price_updater']['our_book_stores']['other_book_stores'][] = "nieprzeczytane.pl";
        $this->comparerSettings = $priceModifyData = $this->pDbMgr->GetAssoc('profit24', "SELECT * FROM products_comparison_sites");
    }

    public function testPriceIncrease()
    {
        $productsToTest = [
            0 => [
                'id' => "8",
                'discount_limit' => "10.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => "7.98",
                'old_price_brutto' => "7.98",
                'has_changed' => false,
                "minimum_price" => 6,
            ],
            1 => [
                'id' => "13",
                'discount_limit' => "1.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => "4",
                'old_price_brutto' => "4",
                'has_changed' => false,
                "minimum_price" => 6,
            ],
        ];

        $ceneoProducts = [
            8 => [
                "offers" => [
                    "offer" => [
                        0 => [
                            "CustName" => "profit24.pl",
                            "Price" => "6.000",
                        ],
                        1 => [
                            "CustName" => "blabla.pl",
                            "Price" => "7.330",
                        ],
                        2 => [
                            "CustName" => "drdrdr.pl",
                            "Price" => "7.340",
                        ]
                    ]
                ]
            ],
            13 => [
                "offers" => [
                    "offer" => [
                        ]
                    ]
            ],
        ];

        $comparedProducts = $this->compareProducts($productsToTest, $ceneoProducts)->getComparedProducts();
        $comparedProducts = $this->priceUpdater->prepareProductsToUpdate($comparedProducts, false);

        $expected = [
            0 => [
                'id' => "8",
                'discount_limit' => "10.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => 7.23,
                'old_price_brutto' => "7.98",
                'has_changed' => true,
                "minimum_price" => 6,
                'change_type' => "increase",
                "discount" => 17.01,
                "discount_value" => 1.23,
            ],
            1 => [
                'id' => "13",
                'discount_limit' => "1.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => "4",
                'old_price_brutto' => "4",
                'has_changed' => true,
                "minimum_price" => 6,
                'change_type' => "increase",
                "discount" => 0,
                "discount_value" => 0,
            ],
        ];

        $this->assertTrue(($expected == $comparedProducts));
    }

    public function testDecreasePrice()
    {
        $productsToTest = [
            0 => [
                'id' => "8",
                'discount_limit' => "10.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => "7.98",
                'old_price_brutto' => "7.98",
                'has_changed' => false,
                "minimum_price" => 6.5,
            ],
            1 => [
                'id' => "13",
                'discount_limit' => "10.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => "7.98",
                'old_price_brutto' => "7.98",
                'has_changed' => false,
                "minimum_price" => 3,
            ],
            2 => [
                'id' => "15",
                'discount_limit' => "10.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => "7.98",
                'old_price_brutto' => "7.98",
                'has_changed' => false,
                "minimum_price" => 3,
            ],
            3 => [
                'id' => "16",
                'discount_limit' => "10.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => "7.98",
                'old_price_brutto' => "7.98",
                'has_changed' => false,
                "minimum_price" => 5,
            ],
        ];

        $ceneoProducts = [
            8 => [
                "offers" => [
                    "offer" => [
                        0 => [
                            "CustName" => "eee.pl",
                            "Price" => "6.000",
                        ],
                        1 => [
                            "CustName" => "blabla.pl",
                            "Price" => "7.300",
                        ],
                        2 => [
                            "CustName" => "drdrdr.pl",
                            "Price" => "7.340",
                        ]
                    ]
                ]
            ],
            13 => [
                "offers" => [
                    "offer" => [
                        0 => [
                            "CustName" => "eee.pl",
                            "Price" => "6.000",
                        ],
                        1 => [
                            "CustName" => "profit24.pl",
                            "Price" => "7.300",
                        ],
                        2 => [
                            "CustName" => "drdrdr.pl",
                            "Price" => "7.340",
                        ]
                    ]
                ]
            ],
            15 => [
                "offers" => [
                    "offer" => [
                        0 => [
                            "CustName" => "eee.pl",
                            "Price" => "2.000",
                        ],
                        1 => [
                            "CustName" => "profit24.pl",
                            "Price" => "7.300",
                        ],
                        2 => [
                            "CustName" => "drdrdr.pl",
                            "Price" => "7.340",
                        ]
                    ]
                ]
            ],
            16 => [
                "offers" => [
                    "offer" => [
                        0 => [
                            "CustName" => "eee.pl",
                            "Price" => "2.000",
                        ],
                        1 => [
                            "CustName" => "olesiejuk.pl",
                            "Price" => "3.300",
                        ],
                        2 => [
                            "CustName" => "nieprzeczytane.pl",
                            "Price" => "6.000",
                        ]
                    ]
                ]
            ],
        ];

        $comparedProducts = $this->compareProducts($productsToTest, $ceneoProducts)->getComparedProducts();

        $expected = [
            0 => [
                'id' => "8",
                'discount_limit' => "10.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => 7.2,
                'old_price_brutto' => "7.98",
                'has_changed' => true,
                "minimum_price" => 6.5,
                "discount" => 9.77,
                "discount_value" => 0.78,
            ],
            1 => [
                'id' => "13",
                'discount_limit' => "10.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'old_price_brutto' => "7.98",
                'price_brutto' => 5.9,
                'has_changed' => true,
                "minimum_price" => 3,
                "discount" => 26.07,
                "discount_value" => 2.08,
            ],
            2 => [
                'id' => "15",
                'discount_limit' => "10.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => "7.98",
                'old_price_brutto' => "7.98",
                'has_changed' => false,
                "minimum_price" => 3,
            ],
            3 => [
                'id' => "16",
                'discount_limit' => "10.00",
                'wholesale_price' => "7.32",
                'ignore_discount' => "0",
                'price_brutto' => "7.98",
                'old_price_brutto' => "7.98",
                'has_changed' => false,
                "minimum_price" => 5,
            ],
        ];

        $this->assertTrue(($expected == $comparedProducts));
    }

    private function compareProducts($products, $ceneoProducts)
    {
        $productProviderMock = $this->getMockBuilder('\PriceUpdater\ProductProvider\FinalPriceProductProvider')->disableOriginalConstructor()->getMock();
        $productProviderMock->expects($this->any())->method('getPreparedProducts')->will($this->returnValue($products));

        $ceneoRepositoryMock = $this->getMockBuilder('\PriceUpdater\Ceneo\CeneoRepository')->disableOriginalConstructor()->getMock();
        $ceneoRepositoryMock->expects($this->any())->method('getProductsByIds')->will($this->returnValue($ceneoProducts));

        $priceUpdater = $this->getPriceUpdater($productProviderMock, $ceneoRepositoryMock);
        $priceUpdater->compareProducts(10, 2);

        return $priceUpdater;
    }

    private function getPriceUpdater($productProviderMock, $ceneoRepositoryMock)
    {
        if (null !== $this->priceUpdater) {
            return $this->priceUpdater;
        }

        $priceModifyData = [
            'm' => "0.10",
            'n' => '0.10',
        ];

        $priceModifierStrategy = new PriceModifierStrategy($this->aConfig['price_updater']['our_book_stores']);
        $tarrifsUpdater = new TarrifsUpdater();

        $priceUpdater = new PriceUpdater($this->pDbMgr, $productProviderMock, $priceModifierStrategy, $ceneoRepositoryMock, $tarrifsUpdater, (float)$priceModifyData['m']);
        $priceUpdater->onlyToBuffor = true;


        $this->priceUpdater = $priceUpdater;
        return $priceUpdater;
    }
}
