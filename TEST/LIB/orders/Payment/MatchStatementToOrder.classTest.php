<?php

namespace LIB\orders\Payment;

use DatabaseManager;
use PHPUnit_Framework_TestCase;
use stdClass;

/**
 * Generated by PHPUnit_SkeletonGenerator on 2015-03-18 at 14:01:30.
 */
class MatchStatementToOrderTest extends PHPUnit_Framework_TestCase {

  /**
   * @var MatchStatementToOrder
   */
  protected $object;
  
  /**
   *
   * @var DatabaseManager
   */
  private $pDbMgr;

  /**
   * Sets up the fixture, for example, opens a network connection.
   * This method is called before a test is executed.
   */
  protected function setUp() {
    global $aConfig;

    include_once('DatabaseManager.class.php');
    $this->pDbMgr = new DatabaseManager($aConfig);
    $this->object = new MatchStatementToOrder($this->pDbMgr);
  }

  /**
   * Tears down the fixture, for example, closes a network connection.
   * This method is called after a test is executed.
   */
  protected function tearDown() {
    
  }

  
  /**
   * 
   * @param int $iStatementId
   * @return array
   */
  private function getStatementData($iStatementId) {
    
    $sSql = 'SELECT * 
             FROM orders_bank_statements
             WHERE id = '.$iStatementId;
    return $this->pDbMgr->GetRow('profit24', $sSql);
  }

  /**
   */
  public function testMatchOrder() {
    $iStatementId = 59898;
    $sOrderNumber = '240312031915';
    $oStatement = (object)$this->getStatementData($iStatementId);
    $this->object->setStatement($oStatement);
    $this->object->matchOrder();
    $aOrderResult = $this->object->getMatchedOrder();
    $aOrderTest = $this->object->getOrderDataOrderNumber($sOrderNumber);
    $this->assertEquals($aOrderTest, $aOrderResult);
  }
  
  
  /**
   */
  public function testNotEqualMatchOrder() {
    $iStatementId = 59898;
    $sOrderNumber = '2403120319142';
    $oStatement = (object)$this->getStatementData($iStatementId);
    $this->object->setStatement($oStatement);
    $this->object->matchOrder();
    $aOrderResult = $this->object->getMatchedOrder();
    $aOrderTest = $this->object->getOrderDataOrderNumber($sOrderNumber);
    $this->assertNotEquals($aOrderTest, $aOrderResult);
  }
  
  /**
   */
  public function testNotEqual2MatchOrder() {
    $iStatementId = 59897;
    $oStatement = (object)$this->getStatementData($iStatementId);
    $this->object->setStatement($oStatement);
    $bMatch = $this->object->matchOrder();
    $this->assertEquals($bMatch, FALSE);
  }
  
  /**
   */
  public function testSecondErrMatchOrder() {
    $iStatementId = 59895;
    $oStatement = (object)$this->getStatementData($iStatementId);
    $this->object->setStatement($oStatement);
    $bMatch = $this->object->matchOrder();
    $this->assertEquals($bMatch, FALSE);
  }
  
  /**
   * 
   */
  public function testTryShellOrderNumber() {
    $aStatements = array(
        'PANI BOŻENA B NOWIŃSKA 5310901522000060000104046543060218023015 ODSTĄPIENIE' => false,
        'PANI BOŻENA B NOWIŃSKA 5310901522000060000104046543 060218023015 ODSTĄPIENIE' => '060218023015',
        'WOJCIECH SŁAWOMIR MICHALSKI 13147000022253156194510001 060316035215 NADPŁATA' => '060316035215',
        'WOJCIECH SŁAWOMIR MICHALSKI 13147000022253156194510001 06 0316035215 NADPŁATA' => false,
        'WOJCIECH SŁAWOMIR MICHALSKI 13147000022253156194510001 0603 16 035215 NADPŁATA' => '060316035215',
        'WOJCIECH SŁAWOMIR MICHALSKI 13147000022253156194510001 0603 16 035 215 NADPŁATA' => false,
        'WOJCIECH SŁAWOMIR MICHALSKI 13147000022253156194510001 060 316035215 NADPŁATA' => '060316035215',
        'WOJCIECH SŁAWOMIR MICHALSKI 13147000022253156194510001 060 3160352155 NADPŁATA' => false,
    ); 
    foreach ($aStatements as $sDecription => $sShellResult) {
      $sResult = $this->object->tryShellOrderNumber($sDecription);
      $this->assertEquals($sShellResult, $sResult);
    }
  }
}
