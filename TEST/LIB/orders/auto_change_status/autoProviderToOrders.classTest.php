<?php

namespace orders\auto_change_status;
use DatabaseManager;
use PHPUnit_Framework_TestCase;


require_once "LIB/orders/auto_change_status/autoProviderToOrders.class.php";
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-09-17 
 * @copyrights Marcin Chudy - Profit24.pl
 */
class autoProviderToOrdersTest extends PHPUnit_Framework_TestCase {

  /**
   * @var ProductsStockReservations
   */
  protected $object;

  /**
   * Sets up the fixture, for example, opens a network connection.
   * This method is called before a test is executed.
   */
  protected function setUp() {
    global $aConfig;

    include_once('DatabaseManager.class.php');
    $this->pDbMgr = new DatabaseManager($aConfig);
    $this->object = new autoProviderToOrders(TRUE, $this->pDbMgr);
    
  }
  
  public function testCheckQuanty() {
    
    $this->assertEquals(
            $this->object->_checkQuanty('20-70', 10), 
            TRUE
            );
    $this->assertEquals(
            $this->object->_checkQuanty('20-70', 70),
            TRUE
            );
    $this->assertEquals(
            $this->object->_checkQuanty('20-70', 5),
            TRUE
            );
    
    $this->assertEquals(
            $this->object->_checkQuanty('>30', 32),
            TRUE
            );
    $this->assertEquals(
            $this->object->_checkQuanty('>30', 31),
            TRUE
            );
    $this->assertEquals(
            $this->object->_checkQuanty('>30', 36),
            FALSE
            );
    $this->assertEquals(
            $this->object->_checkQuanty('>30', 30),
            TRUE
            );
    
    $this->assertEquals(
            $this->object->_checkQuanty('<10', 10),
            FALSE
            );
    $this->assertEquals(
            $this->object->_checkQuanty('<10', 11),
            FALSE
            );
    $this->assertEquals(
            $this->object->_checkQuanty('<10', 9),
            TRUE
            );
    
    $this->assertEquals(
            $this->object->_checkQuanty('0', 0),
            TRUE);
    $this->assertEquals(
            $this->object->_checkQuanty('0', 1),
            FALSE);
    
    $this->assertEquals(
            $this->object->_checkQuanty('5', 2),
            TRUE);
    $this->assertEquals(
            $this->object->_checkQuanty('2', 5),
            FALSE);
    
    $this->assertEquals(
            $this->object->_checkQuanty('1-9', 9),
            TRUE
            );
    $this->assertEquals(
            $this->object->_checkQuanty('1-9', 10),
            FALSE
            );
    $this->assertEquals(
            $this->object->_checkQuanty('1-9', 1),
            TRUE
            );
    $this->assertEquals(
            $this->object->_checkQuanty('1-9', 0),
            TRUE);
    
  }
  
  
  public function testCheckMatchProvider() {

    
    $this->object = new autoProviderToOrders(TRUE, $this->pDbMgr);
    
    $aItemData = array (
      'id' => '488528',
      'quantity' => '2',
      'price_brutto' => '23.00',
      'publisher_id' => '7839',
      'profit_e_status' => '0',
      'profit_j_status' => '0',
      'profit_m_status' => '0',
      'profit_g_status' => '1',
      'profit_g_wholesale_price' => '22.00',
      'profit_x_status' => '0',
      'profit_e_price_brutto' => '34.90',
      'profit_e_price_netto' => '33.24',
      'profit_e_vat' => '5',
      'profit_e_shipment_time' => '0',
      'profit_e_last_import' => '2013-09-11 15:01:22',
      'profit_e_location' => '70',
      'profit_j_price_brutto' => '34.90',
      'profit_j_price_netto' => '33.24',
      'profit_j_vat' => '5',
      'profit_j_shipment_time' => '0',
      'profit_j_last_import' => '2013-05-23 16:05:34',
      'profit_j_location' => '70',
      'profit_m_price_brutto' => '34.90',
      'profit_m_price_netto' => '33.24',
      'profit_m_vat' => '5',
      'profit_m_shipment_time' => '0',
      'profit_m_last_import' => '2013-07-12 20:09:25',
      'profit_m_location' => '70',
      'profit_g_price_brutto' => '34.90',
      'profit_g_price_netto' => '33.24',
      'profit_g_vat' => '5',
      'profit_g_shipment_time' => '0',
      'profit_g_last_import' => '2013-09-11 15:03:12',
      'profit_g_location' => '70',
      'profit_x_price_brutto' => '0.00',
      'profit_x_price_netto' => '0.00',
      'profit_x_vat' => '0',
      'profit_x_shipment_time' => '',
      'profit_x_last_import' => NULL,
      'profit_x_location' => '',
    );
    
    $this->assertEquals(
            $this->object->_matchInternalProviders($aItemData, $this->object->_getInternalProviders()),
            NULL);
    
    $aItemData['profit_g_status'] = 2;
    $this->assertEquals(
            $this->object->_matchInternalProviders($aItemData, $this->object->_getInternalProviders()),
            FALSE);
    
    $aItemData['profit_g_status'] = 10;
    $this->assertEquals(
            $this->object->_matchInternalProviders($aItemData, $this->object->_getInternalProviders()),
            FALSE);
    
    $aItemData['profit_g_status'] = 1;
    $aItemData['profit_j_status'] = 1;
    $this->assertEquals(
            $this->object->_matchInternalProviders($aItemData, $this->object->_getInternalProviders()),
            FALSE);
    
    // kolejnosc
    $aItemData['profit_g_status'] = 2;
    $aItemData['profit_e_status'] = 2;
    $this->assertEquals(
            $this->object->_matchInternalProviders($aItemData, $this->object->_getInternalProviders()),
            1);
    
    $aItemData = array (
      'id' => '283107',
      'price_brutto' => '21.78',
      'azymut_status' => '1',
      'abe_status' => '0',
      'helion_status' => '0',
      'profit_e_status' => '0',
      'profit_j_status' => '0',
      'profit_m_status' => '0',
      'profit_g_status' => '0',
      'profit_x_status' => '0',
      'azymut_price_brutto' => '33.00',
      'azymut_price_netto' => '31.43',
      'azymut_vat' => '5',
      'azymut_shipment_time' => '1',
      'azymut_last_import' => '2013-10-03 23:30:12',
      'azymut_wholesale_price' => '21.78',
      'azymut_stock' => '5-10',
      'abe_price_brutto' => '0.00',
      'abe_price_netto' => '0.00',
      'abe_vat' => '0',
      'abe_shipment_time' => '',
      'abe_last_import' => NULL,
      'helion_price_brutto' => '0.00',
      'helion_price_netto' => '0.00',
      'helion_vat' => '0',
      'helion_shipment_time' => '',
      'helion_last_import' => NULL,
      'helion_stock' => NULL,
      'profit_e_price_brutto' => '0.00',
      'profit_e_price_netto' => '0.00',
      'profit_e_vat' => '0',
      'profit_e_shipment_time' => '',
      'profit_e_last_import' => NULL,
      'profit_e_location' => '',
      'profit_j_price_brutto' => '0.00',
      'profit_j_price_netto' => '0.00',
      'profit_j_vat' => '0',
      'profit_j_shipment_time' => '',
      'profit_j_last_import' => NULL,
      'profit_j_location' => '',
      'profit_m_price_brutto' => '0.00',
      'profit_m_price_netto' => '0.00',
      'profit_m_vat' => '0',
      'profit_m_shipment_time' => '',
      'profit_m_last_import' => NULL,
      'profit_m_location' => '',
      'profit_g_price_brutto' => '0.00',
      'profit_g_price_netto' => '0.00',
      'profit_g_vat' => '0',
      'profit_g_shipment_time' => '',
      'profit_g_last_import' => NULL,
      'profit_g_location' => '',
      'profit_x_price_brutto' => '0.00',
      'profit_x_price_netto' => '0.00',
      'profit_x_vat' => '0',
      'profit_x_shipment_time' => '',
      'profit_x_last_import' => NULL,
      'profit_x_location' => '',
      'dictum_status' => '0',
      'dictum_price_brutto' => '0.00',
      'dictum_price_netto' => '0.00',
      'dictum_vat' => '0',
      'dictum_shipment_time' => '',
      'dictum_last_import' => NULL,
      'dictum_wholesale_price' => '0.00',
      'dictum_stock' => NULL,
      'siodemka_status' => '1',
      'siodemka_price_brutto' => '0.00',
      'siodemka_price_netto' => '0.00',
      'siodemka_vat' => '0',
      'siodemka_shipment_time' => '',
      'siodemka_last_import' => NULL,
      'siodemka_wholesale_price' => '0.00',
      'siodemka_stock' => '>10',
      'olesiejuk_status' => '1',
      'olesiejuk_price_brutto' => '33.00',
      'olesiejuk_price_netto' => '31.43',
      'olesiejuk_vat' => '5',
      'olesiejuk_shipment_time' => '1',
      'olesiejuk_last_import' => '2013-10-03 03:15:41',
      'olesiejuk_wholesale_price' => '24.75',
      'olesiejuk_stock' => '0',
      'profit_e_shipment_date' => NULL,
      'profit_j_shipment_date' => NULL,
      'profit_m_shipment_date' => NULL,
      'profit_g_shipment_date' => NULL,
      'profit_x_shipment_date' => NULL,
      'azymut_shipment_date' => NULL,
      'abe_shipment_date' => NULL,
      'helion_shipment_date' => NULL,
      'dictum_shipment_date' => NULL,
      'siodemka_shipment_date' => NULL,
      'olesiejuk_shipment_date' => NULL,
      'order_id' => '86871',
      'quantity' => '9', // XXXX TUUU
      'order_number' => '77886/2013/10/PR24',
      'name' => 'Prawdziwa księga pustki',
      'isbn_plain' => '8389933209',
      'publisher_id' => '6598',
      'check_external_border_time' => '1',
      'omit_internal_provider' => '0',
      'have_additional_day' => '1',
    );
    $aExternalProviders = array (
        'ID_7' => 
        array (
          'stock_col' => 'azymut_stock',
          'symbol' => 'azymut',
          'border_time' => '18:50:00',
          'id' => 'ID_7',
        ),
        'ID_26' => 
        array (
          'stock_col' => 'siodemka_stock',
          'symbol' => 'siodemka',
          'border_time' => '17:50:00',
          'id' => 'ID_26',
        ),
        'ID_27' => 
        array (
          'id' => 'ID_27',
          'stock_col' => 'olesiejuk_stock',
          'symbol' => 'olesiejuk',
          'border_time' => '07:30:00',
        ),
        'ID_8' => 
        array (
          'id' => 'ID_8',
          'stock_col' => 'abe_status',
          'symbol' => 'abe',
          'border_time' => NULL,
        )
      );
    $this->assertEquals(
            $this->object->_matchExternalProviders($aItemData, $aExternalProviders, TRUE),
            FALSE
            );
    
    $aItemData['quantity'] = '14';
    $this->assertEquals(
            $this->object->_matchExternalProviders($aItemData, $aExternalProviders, TRUE),
            FALSE
            );
    
    // godzina graniczna dla azymutu, wybiera siodemke
    $aItemData['quantity'] = '1';
    $aExternalProviders['ID_7']['border_time'] = '07:50:00';
    $this->assertEquals(
            $this->object->_matchExternalProviders($aItemData, $aExternalProviders, TRUE),
            FALSE
            );
    
    $aItemData = array (
        'id' => '88387', 
        'azymut_status' => '1', 'abe_status' => '0', 'helion_status' => '0', 'profit_e_status' => '0', 'profit_j_status' => '0', 'profit_m_status' => '0', 'profit_g_status' => '10', 'profit_x_status' => '0', 'azymut_price_brutto' => '24.90', 'azymut_price_netto' => '23.71', 'azymut_vat' => '5', 'azymut_shipment_time' => '1', 'azymut_last_import' => '2013-10-10 06:30:07', 'azymut_wholesale_price' => '16.43', 'azymut_stock' => '>50', 'abe_price_brutto' => '0.00', 'abe_price_netto' => '0.00', 'abe_vat' => '0', 'abe_shipment_time' => '', 'abe_last_import' => NULL, 'helion_price_brutto' => '0.00', 'helion_price_netto' => '0.00', 'helion_vat' => '0', 'helion_shipment_time' => '', 'helion_last_import' => NULL, 'helion_stock' => NULL, 'profit_e_price_brutto' => '0.00', 'profit_e_price_netto' => '0.00', 'profit_e_vat' => '0', 'profit_e_shipment_time' => '', 'profit_e_last_import' => NULL, 'profit_e_location' => '', 'profit_j_price_brutto' => '0.00', 'profit_j_price_netto' => '0.00', 'profit_j_vat' => '0', 'profit_j_shipment_time' => '', 'profit_j_last_import' => NULL, 'profit_j_location' => '', 'profit_m_price_brutto' => '0.00', 'profit_m_price_netto' => '0.00', 'profit_m_vat' => '0', 'profit_m_shipment_time' => '', 'profit_m_last_import' => NULL, 'profit_m_location' => '', 'profit_g_price_brutto' => '24.90', 'profit_g_price_netto' => '23.71', 'profit_g_vat' => '5', 'profit_g_shipment_time' => '0', 'profit_g_last_import' => '2013-10-10 09:03:53', 'profit_g_location' => '', 'profit_x_price_brutto' => '0.00', 'profit_x_price_netto' => '0.00', 'profit_x_vat' => '0', 'profit_x_shipment_time' => '', 'profit_x_last_import' => NULL, 'profit_x_location' => '', 'dictum_status' => '0', 'dictum_price_brutto' => '0.00', 'dictum_price_netto' => '0.00', 'dictum_vat' => '0', 'dictum_shipment_time' => '', 'dictum_last_import' => NULL, 'dictum_wholesale_price' => '0.00', 'dictum_stock' => NULL, 'siodemka_status' => '0', 'siodemka_price_brutto' => '0.00', 'siodemka_price_netto' => '0.00', 'siodemka_vat' => '0', 'siodemka_shipment_time' => '', 'siodemka_last_import' => NULL, 'siodemka_wholesale_price' => '0.00', 'siodemka_stock' => NULL, 'olesiejuk_status' => '0', 'olesiejuk_price_brutto' => '0.00', 'olesiejuk_price_netto' => '0.00', 'olesiejuk_vat' => '0', 'olesiejuk_shipment_time' => '', 'olesiejuk_last_import' => NULL, 'olesiejuk_wholesale_price' => '0.00', 'olesiejuk_stock' => NULL, 'profit_e_shipment_date' => NULL, 'profit_j_shipment_date' => NULL, 'profit_m_shipment_date' => NULL, 'profit_g_shipment_date' => NULL, 'profit_x_shipment_date' => NULL, 'azymut_shipment_date' => NULL, 'abe_shipment_date' => NULL, 'helion_shipment_date' => NULL, 'dictum_shipment_date' => NULL, 'siodemka_shipment_date' => NULL, 'olesiejuk_shipment_date' => NULL, 'order_id' => '88387', 
        'quantity' => '30', 
        'order_number' => '79402/2013/10/PR24', 'name' => 'Spawanie w osĹonie gazĂłw metodami MAG i MIG', 'isbn_plain' => '8371416156', 'publisher_id' => '5936', 'omit_internal_provider' => '0');
    $this->assertSame(
            $this->object->_matchInternalProviders($aItemData, $this->object->_getInternalProviders()),
            FALSE
            );
    
    $aItemData['quantity'] = '10';
    $this->assertSame(
            $this->object->_matchInternalProviders($aItemData, $this->object->_getInternalProviders()),
            51
            );
    
    $aItemData['profit_e_status'] = '2';
    $aItemData['profit_j_status'] = '1';
    $aItemData['profit_m_status'] = '2';
    $aItemData['profit_g_status'] = '0';
    $aItemData['profit_x_status'] = '0';
    $this->assertSame(
            $this->object->_matchInternalProviders($aItemData, $this->object->_getInternalProviders()),
            NULL
            );
    
    $aItemData['profit_e_status'] = '0';
    $aItemData['profit_j_status'] = '0';
    $aItemData['profit_m_status'] = '0';
    $aItemData['profit_g_status'] = '0';
    $aItemData['profit_x_status'] = '0';
    $this->assertSame(
            $this->object->_matchInternalProviders($aItemData, $this->object->_getInternalProviders()),
            NULL
            );
  }
  
  
  public function testCheckMergeProviders() {
    
    $aItemData = array (
      'id' => '295986',
      'azymut_status' => '1',
      'abe_status' => '0',
      'helion_status' => '1',
      'profit_e_status' => '0',
      'profit_j_status' => '0',
      'profit_m_status' => '0',
      'profit_g_status' => '0',
      'profit_x_status' => '0',
      'azymut_price_brutto' => '49.98',
      'azymut_price_netto' => '47.60',
      'azymut_vat' => '5',
      'azymut_shipment_time' => '1',
      'azymut_last_import' => '2013-10-28 23:30:07',
      'azymut_wholesale_price' => '32.99',
      'azymut_stock' => '>50',
      'abe_price_brutto' => '0.00',
      'abe_price_netto' => '0.00',
      'abe_vat' => '0',
      'abe_shipment_time' => '',
      'abe_last_import' => NULL,
      'helion_price_brutto' => '41.98',
      'helion_price_netto' => '41.98',
      'helion_vat' => '5',
      'helion_shipment_time' => '',
      'helion_last_import' => NULL,
      'helion_stock' => '>70',
      'helion_wholesale_price' => '31.99',
      'profit_e_price_brutto' => '0.00',
      'profit_e_price_netto' => '0.00',
      'profit_e_vat' => '0',
      'profit_e_shipment_time' => '',
      'profit_e_last_import' => NULL,
      'profit_e_location' => '',
      'profit_j_price_brutto' => '0.00',
      'profit_j_price_netto' => '0.00',
      'profit_j_vat' => '0',
      'profit_j_shipment_time' => '',
      'profit_j_last_import' => NULL,
      'profit_j_location' => '',
      'profit_m_price_brutto' => '0.00',
      'profit_m_price_netto' => '0.00',
      'profit_m_vat' => '0',
      'profit_m_shipment_time' => '',
      'profit_m_last_import' => NULL,
      'profit_m_location' => '',
      'profit_g_price_brutto' => '0.00',
      'profit_g_price_netto' => '0.00',
      'profit_g_vat' => '0',
      'profit_g_shipment_time' => '',
      'profit_g_last_import' => NULL,
      'profit_g_location' => '',
      'profit_x_price_brutto' => '0.00',
      'profit_x_price_netto' => '0.00',
      'profit_x_vat' => '0',
      'profit_x_shipment_time' => '',
      'profit_x_last_import' => NULL,
      'profit_x_location' => '',
      'dictum_status' => '0',
      'dictum_price_brutto' => '0.00',
      'dictum_price_netto' => '0.00',
      'dictum_vat' => '0',
      'dictum_shipment_time' => '',
      'dictum_last_import' => NULL,
      'dictum_wholesale_price' => '0.00',
      'dictum_stock' => NULL,
      'siodemka_status' => '0',
      'siodemka_price_brutto' => '0.00',
      'siodemka_price_netto' => '0.00',
      'siodemka_vat' => '0',
      'siodemka_shipment_time' => '',
      'siodemka_last_import' => NULL,
      'siodemka_wholesale_price' => '0.00',
      'siodemka_stock' => NULL,
      'olesiejuk_status' => '1',
      'olesiejuk_price_brutto' => '49.98',
      'olesiejuk_price_netto' => '47.60',
      'olesiejuk_vat' => '5',
      'olesiejuk_shipment_time' => '1',
      'olesiejuk_last_import' => '2013-10-28 03:08:21',
      'olesiejuk_wholesale_price' => '37.49',
      'olesiejuk_stock' => '>10',
      'profit_e_shipment_date' => NULL,
      'profit_j_shipment_date' => NULL,
      'profit_m_shipment_date' => NULL,
      'profit_g_shipment_date' => NULL,
      'profit_x_shipment_date' => NULL,
      'azymut_shipment_date' => NULL,
      'abe_shipment_date' => NULL,
      'helion_shipment_date' => NULL,
      'dictum_shipment_date' => NULL,
      'siodemka_shipment_date' => NULL,
      'olesiejuk_shipment_date' => NULL,
      'order_id' => '91923',
      'quantity' => '1',
      'order_number' => '82936/2013/10/PR24',
      'shipment_date' => '2013-10-30',
      'name' => 'Między endecją a sanacją',
      'isbn_plain' => '9788362610495',
      'publisher_id' => '6292',
      'check_external_border_time' => '0',
      'omit_internal_provider' => '0',
      'have_additional_day' => '0',
    );
    $this->object = new autoProviderToOrders(TRUE, $this->pDbMgr);
    
    // helion tańszy
    $aExternalProviders = array (
      'ID_7' => 
      array (
        'stock_col' => 'azymut_stock',
        'symbol' => 'azymut',
        'border_time' => '18:50:00',
        'id' => 'ID_7',
      ),
      'ID_27' => 
      array (
        'id' => 'ID_27',
        'stock_col' => 'olesiejuk_stock',
        'symbol' => 'olesiejuk',
        'border_time' => '07:30:00',
      ),
      'ID_8' => 
      array (
        'id' => 'ID_8',
        'stock_col' => 'abe_status',
        'symbol' => 'abe',
        'border_time' => NULL,
      ),
    );
    $aReturn = Array (
        'ID_6' => Array (
            'stock_col' => 'helion_stock',
            'symbol' => 'helion',
            'border_time' => '12:00:00',
            'id' => 'ID_6'
        ),
        'ID_7' => Array (
            'stock_col' => 'azymut_stock',
            'symbol' => 'azymut',
            'border_time' => '17:00:00',
            'id' => 'ID_7'
        ),
        'ID_27' => Array (
            'stock_col' => 'olesiejuk_stock',
            'symbol' => 'olesiejuk',
            'border_time' => '18:30:00',
            'id' => 'ID_27',
        ),
        'ID_8' => Array (
            'id' => 'ID_8',
            'stock_col' => 'abe_status',
            'symbol' => 'abe',
            'border_time' => null
        )
    );
    $this->assertSame(
            $this->object->_mergeEqualExternalSources($aExternalProviders, $aItemData),
            $aReturn
            );
    
    // azymut najtańszy
    $aReturn = Array (
        'ID_7' => Array (
            'stock_col' => 'azymut_stock',
            'symbol' => 'azymut',
            'border_time' => '17:00:00',
            'id' => 'ID_7'
        ),
        'ID_6' => Array (
            'stock_col' => 'helion_stock',
            'symbol' => 'helion',
            'border_time' => '12:00:00',
            'id' => 'ID_6'
        ),
        'ID_27' => Array (
            'stock_col' => 'olesiejuk_stock',
            'symbol' => 'olesiejuk',
            'border_time' => '18:30:00',
            'id' => 'ID_27'
        ),
        'ID_8' => Array (
            'id' => 'ID_8',
            'stock_col' => 'abe_status',
            'symbol' => 'abe',
            'border_time' => null
        )
    );
    $aItemData['azymut_wholesale_price'] = '30.99';
    $this->assertSame(
            $this->object->_mergeEqualExternalSources($aExternalProviders, $aItemData),
            $aReturn
            );
    
    
    // inna kolejność, azymut najtańszy
    $aExternalProviders = array (
      'ID_8' => 
      array (
        'id' => 'ID_8',
        'stock_col' => 'abe_status',
        'symbol' => 'abe',
        'border_time' => NULL,
      ),
      'ID_7' => 
      array (
        'stock_col' => 'azymut_stock',
        'symbol' => 'azymut',
        'border_time' => '18:50:00',
        'id' => 'ID_7',
      ),
      'ID_27' => 
      array (
        'id' => 'ID_27',
        'stock_col' => 'olesiejuk_stock',
        'symbol' => 'olesiejuk',
        'border_time' => '07:30:00',
      ),
    );
    $aReturn = Array (
        'ID_8' => Array (
            'id' => 'ID_8',
            'stock_col' => 'abe_status',
            'symbol' => 'abe',
            'border_time' => null
        ),
        'ID_7' => Array (
            'stock_col' => 'azymut_stock',
            'symbol' => 'azymut',
            'border_time' => '17:00:00',
            'id' => 'ID_7'
        ),
        'ID_6' => Array (
            'stock_col' => 'helion_stock',
            'symbol' => 'helion',
            'border_time' => '12:00:00',
            'id' => 'ID_6'
        ),
        'ID_27' => Array (
            'stock_col' => 'olesiejuk_stock',
            'symbol' => 'olesiejuk',
            'border_time' => '18:30:00',
            'id' => 'ID_27',
        )
    );
    $aItemData['azymut_wholesale_price'] = '30.99';
    $this->assertSame(
            $this->object->_mergeEqualExternalSources($aExternalProviders, $aItemData),
            $aReturn
            );
    
  }
}
