<?php

/*
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-14 
 * @copyrights Marcin Chudy - Profit24.pl
 */

/**
 * Description of Shipment_TEST
 *
 * @author Arkadiusz Golba <arekgl0@op.pl>
 */
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/PaczkaWRuchu';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

$oShipment = new \orders\Shipment(3, $pDbMgr, $bTestMode);
$aParams = array(
    'email' => 'test'
);
var_dump($oShipment->getDestinationPointsDropdown('PointsOfReceipt', $aParams));
