<?php

use LIB\Helpers\ArrayHelper;

class ArrayHelperTest extends PHPUnit_Framework_TestCase
{
    public function testSortByColumn()
    {
        $array = [
            0 => [
                'id' => 1,
                'price' => 121.01,
            ],
            1 => [
                'id' => 2,
                'price' => 12,
            ],
            2 => [
                'id' => 3,
                'price' => 59,
            ],
            3 => [
                'id' => 4,
                'price' => 74,
            ],
        ];

        $expected = [
            [
                'id' => 2,
                'price' => 12,
            ],
            [
                'id' => 3,
                'price' => 59,
            ],
            [
                'id' => 4,
                'price' => 74,
            ],
            [
                'id' => 1,
                'price' => 121.01,
            ],
        ];

        ArrayHelper::sortByColumn($array, 'price');

        $this->assertSame($expected, $array);
    }
} 