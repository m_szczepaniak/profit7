<?php

namespace communicator\sources;

use DatabaseManager;
use PHPUnit_Framework_TestCase;

/**
 * @backupGlobals enabled
 * @backupStaticAttributes enabled
 */
class matchFVtoPZTest extends PHPUnit_Framework_TestCase {

  /**
   * @var matchFVtoPZ
   */
  protected $object;

  /**
   * Sets up the fixture, for example, opens a network connection.
   * This method is called before a test is executed.
   */
  protected function setUp() {
    global $aConfig, $pDbMgr;
    $pDbMgr = $this->pDbMgr;
    
    include_once('DatabaseManager.class.php');
    $this->pDbMgr = new DatabaseManager($aConfig);
    $this->object = new matchFVtoPZ($this->pDbMgr);
  }

  /**
   * Tears down the fixture, for example, closes a network connection.
   * This method is called after a test is executed.
   */
  protected function tearDown() {}

  /**
   * @backupGlobals enabled
   */
  public function testProceedMatch() {
    global $aConfig, $pDbMgr;
    include_once('DatabaseManager.class.php');
    $pDbMgr = new DatabaseManager($aConfig);
    
    $this->assertTrue($this->object->proceedMatch('Ateneum', 31));
  }

}
