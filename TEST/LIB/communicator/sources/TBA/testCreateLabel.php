<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-18 
 * @copyrights Marcin Chudy - Profit24.pl
 */
header('Content-Type: text/html; charset=UTF-8');
//$aConfig['config']['project_dir'] = 'LIB/communicator/PaczkaWRuchu';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

/**
 * Pobiera dane zamówienia
 * @param int $iOrderId - id zamowienia
 * @return array
 */
function getOrderData($iOrderId){
  global $aConfig;
  $sSql = "SELECT *
          FROM ".$aConfig['tabls']['prefix']."orders
          WHERE id = ".$iOrderId;
  return Common::GetRow($sSql);
} // end of getOrderData() method

$bTestMode = TRUE;
$oGetTransportList = new \orders\getTransportList($pDbMgr);


$iOrderId = 133047;
$aOrder = getOrderData($iOrderId);
$aDeliverAddress = $oGetTransportList->getDeliverAddress($aOrder, $oGetTransportList->getOrderAddresses($iOrderId, false));
$aSellerData = $oGetTransportList->getSellerDataByWebsiteId($aOrder['website_id']);
$aOrder['weight'] = 1.22;
// TEST
$oTBA = new communicator\sources\TBA\TBA(true, $pDbMgr);
$oTBA->addShipment($aOrder, $aDeliverAddress, $aSellerData);

