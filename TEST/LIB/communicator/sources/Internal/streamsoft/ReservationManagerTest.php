<?php

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use PHPUnit_Framework_TestCase;

class ReservationManagerTest extends PHPUnit_Framework_TestCase
{
    private $pDbMgr;

    protected function setUp() {
        global $aConfig;

        include_once('DatabaseManager.class.php');
        $this->pDbMgr = new DatabaseManager($aConfig);
    }

    public function testCreateReservation()
    {
        $reservationManager = new ReservationManager($this->pDbMgr);
        $reservationManager->createLocalReservationsByOrderId(296016);
    }
}