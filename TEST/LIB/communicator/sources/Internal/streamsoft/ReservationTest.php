<?php

namespace LIB\communicator\sources\Internal\streamsoft;

use DatabaseManager;
use PHPUnit_Framework_TestCase;

class ReservationTest extends PHPUnit_Framework_TestCase
{
    private $pDbMgr;

    protected function setUp() {
        global $aConfig;

        include_once('DatabaseManager.class.php');
        $this->pDbMgr = new DatabaseManager($aConfig);
    }

    public function testCreateReservation()
    {
        $order = $this->getOrder();
        $reservation = new Reservation($this->pDbMgr);

        $result = $reservation->createReservation($order);

        $this->assertEquals(0, $result->getErrorCode());
    }

    private function getOrder()
    {
        $positions = [
            [
                'id' => 829145,
                'in_confirmed_quantity' => 1,
                'streamsoft_indeks' => 9788325555108,
                'price_netto' => null,
                'item_source_id' => null,
            ],
            [
                'id' => 829146,
                'in_confirmed_quantity' => 1,
                'streamsoft_indeks' => 9788325558246,
                'price_netto' => null,
                'item_source_id' => null,
            ],
            [
                'id' => 829147,
                'in_confirmed_quantity' => 1,
                'streamsoft_indeks' => 9788326481901,
                'price_netto' => null,
                'item_source_id' => null,
            ],
        ];

        $externalCode = rand(10,100000000);

        $order = new Order($this->pDbMgr);

        return $order->createOrder($externalCode, $positions);
    }
}