function showRuchGeoPicker(customOptions) {
	var options = {
		height : '776',
		width : '680',
		allowMaximize : false,
		allowClose : false,
		modal : true,
		contentStyle : ' z-index: 1000; position: fixed !important; top: 20px !important;',
		closeButtonImg : '../img/close.png',
		styleClassPrefix : 'paczkawruchu-',
		modalBgStyle :  'background-color: #000000;z-index:1000;'
	};

	if (customOptions !== undefined) {
		for ( var key in customOptions) {
			options[key] = customOptions[key];
		}
	}
	new GlassLayerPopup(options)
			.showUrl('https://www.paczkawruchu.pl/RuchGeoPicker/html/paczka_w_ruchu.html');
}

function closeRuchGeoPicker() {
	GlassLayerPopupsManager.closeAllPopups();
}

function ruchDeliveryChosen(event) {
	closeRuchGeoPicker();

	if (event.data != "undefined") {
		var psd = JSON.parse(event.data);
		getPsdFromRuchGeoPicker(psd);
	}
}

function getPsdFromRuchGeoPicker(psd){
	$('form input[name="punktodbioru"]').val(psd.psd);
	$('form input[name="ulica"]').val(psd.street_name);
	$('form input[name="miasto"]').val(psd.city);
	$('form input[name="lokalizacja"]').val(psd.location);
};

window.addEventListener("message", ruchDeliveryChosen, false);