<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2014-12-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */
header('Content-Type: text/html; charset=UTF-8');

$aConfig['config']['project_dir'] = 'LIB/communicator/PaczkaWRuchu';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));

include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once($_SERVER['DOCUMENT_ROOT'].'/lang/pl.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');
set_time_limit(10800);
header('Content-Type: text/html; charset=UTF-8');
set_time_limit(3600); // maksymalny czas wykonywania 1 godzina - 
ini_set("memory_limit", '1G');

include_once $_SERVER['DOCUMENT_ROOT'].'/LIB/autoloader.php';

	/**
	 * Pobiera dane zamówienia
	 * @param int $iOrderId - id zamowienia
	 * @return array
	 */
	function getOrderData($iOrderId){
		global $aConfig;
		$sSql = "SELECT *
						FROM ".$aConfig['tabls']['prefix']."orders
						WHERE id = ".$iOrderId;
		return Common::GetRow($sSql);
	} // end of getOrderData() method


$bTestMode = TRUE;
$oPaczkaWRuchu = new \communicator\sources\PaczkaWRuchu\PaczkaWRuchu($bTestMode, $pDbMgr);
$oGetTransportList = new \orders\getTransportList($pDbMgr);

$iOrderId = 132881;
$aOrder = getOrderData($iOrderId);
$aDeliverAddress = $oGetTransportList->getDeliverAddress($aOrder, $oGetTransportList->getOrderAddresses($iOrderId, false));
$aSellerData = $oGetTransportList->getSellerDataByWebsiteId($aOrder['website_id']);

$oPaczkaWRuchu->addShipment($aOrder, $aDeliverAddress, $aSellerData);