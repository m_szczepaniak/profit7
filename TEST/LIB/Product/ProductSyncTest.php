<?php

use PriceUpdater\PriceModifier\PriceModifierStrategy;
use PriceUpdater\PriceUpdater;
use PriceUpdater\Tarrifs\TarrifsUpdater;
use Product\ProductsSync;

class ProductSyncTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var PriceUpdater null
     */
    private $priceUpdater = null;

    protected function setUp()
    {
        global $aConfig;

        include_once('DatabaseManager.class.php');
        $this->pDbMgr = new DatabaseManager($aConfig);
        $this->aConfig = $aConfig;

    }


    /**
     * @return mixed
     */
    public function testGetMainProductMerlin() {
        $productsSync = new ProductsSync($this->pDbMgr);

        $reflection = new \ReflectionClass(get_class($productsSync));
        $method = $reflection->getMethod('getMainProductMerlin');
        $method->setAccessible(true);
        $merlinProductsProperty = $reflection->getProperty('merlinProducts');
        $merlinProductsProperty->setAccessible(true);

        $merlinProducts =
            [
                279826 =>
                    array (
                        'sku' => '279826',
                        'ean' => '5901844916920',
                        'price_netto_buy' => '22.00000000',
                        'contract_code' => 'SON01',
                        'eta' => '2',
                        'srp' => NULL,
                        'rate' => '23',
                        'qty' => '11',
                        'premiere_at' => '2001-08-27',
                    ),
                279827 =>
                    array (
                        'sku' => '279827',
                        'ean' => '9788391337752',
                        'price_netto_buy' => '10.71000000',
                        'contract_code' => 'AZZ01',
                        'eta' => '1',
                        'srp' => '15.0045',
                        'rate' => '5',
                        'qty' => '1',
                        'premiere_at' => NULL,
                    ),
            ];

        $merlinProductsProperty->setValue($productsSync, $merlinProducts);

        $aMerlinSkuGrouped = [279826, 279827];
        $sku = $method->invokeArgs($productsSync, ['0' => $aMerlinSkuGrouped]);
        $this->assertEquals('279827', $sku);


        $merlinProducts =
            [
                279826 =>
                    array (
                        'sku' => '279826',
                        'ean' => '5901844916920',
                        'price_netto_buy' => '22.00000000',
                        'contract_code' => 'SON01',
                        'eta' => '2',
                        'srp' => NULL,
                        'rate' => '23',
                        'qty' => '11',
                        'premiere_at' => '2001-08-27',
                    ),
                279827 =>
                    array (
                        'sku' => '279827',
                        'ean' => '9788391337752',
                        'price_netto_buy' => '10.71000000',
                        'contract_code' => 'AZZ01',
                        'eta' => '1',
                        'srp' => '15.0045',
                        'rate' => '5',
                        'qty' => '0',
                        'premiere_at' => NULL,
                    ),
            ];

        $merlinProductsProperty->setValue($productsSync, $merlinProducts);

        $aMerlinSkuGrouped = [279826, 279827];
        $sku = $method->invokeArgs($productsSync, ['0' => $aMerlinSkuGrouped]);
        $this->assertEquals('279826', $sku);


        $merlinProducts =
            [
                279826 =>
                    array (
                        'sku' => '279826',
                        'ean' => '5901844916920',
                        'price_netto_buy' => '22.00000000',
                        'contract_code' => 'SON01',
                        'eta' => '2',
                        'srp' => NULL,
                        'rate' => '23',
                        'qty' => '11',
                        'premiere_at' => '2001-08-27',
                    ),
                279827 =>
                    array (
                        'sku' => '279827',
                        'ean' => '9788391337752',
                        'price_netto_buy' => '10.71000000',
                        'contract_code' => 'AZZ01',
                        'eta' => '1',
                        'srp' => '15.0045',
                        'rate' => '5',
                        'qty' => '1',
                        'premiere_at' => NULL,
                    ),
            ];

        $merlinProductsProperty->setValue($productsSync, $merlinProducts);

        $aMerlinSkuGrouped = [279827, 279826];
        $sku = $method->invokeArgs($productsSync, ['0' => $aMerlinSkuGrouped]);
        $this->assertEquals('279827', $sku);


        $merlinProducts =
            [
                279826 =>
                    array (
                        'sku' => '279826',
                        'ean' => '5901844916920',
                        'price_netto_buy' => '22.00000000',
                        'contract_code' => 'SON01',
                        'eta' => '2',
                        'srp' => NULL,
                        'rate' => '23',
                        'qty' => '11',
                        'premiere_at' => '2001-08-27',
                    ),
                279827 =>
                    array (
                        'sku' => '279827',
                        'ean' => '9788391337752',
                        'price_netto_buy' => '10.71000000',
                        'contract_code' => 'AZZ01',
                        'eta' => '1',
                        'srp' => '15.0045',
                        'rate' => '5',
                        'qty' => '0',
                        'premiere_at' => NULL,
                    ),
            ];

        $merlinProductsProperty->setValue($productsSync, $merlinProducts);

        $aMerlinSkuGrouped = [279827, 279826];
        $sku = $method->invokeArgs($productsSync, ['0' => $aMerlinSkuGrouped]);
        $this->assertEquals('279826', $sku);


        // preview + available 1
        $merlinProducts =
            [
                279826 =>
                    array (
                        'sku' => '279826',
                        'ean' => '5901844916920',
                        'price_netto_buy' => '22.00000000',
                        'contract_code' => 'SON01',
                        'eta' => '2',
                        'srp' => NULL,
                        'rate' => '23',
                        'qty' => '0',
                        'premiere_at' => '2050-01-01',
                    ),
                279827 =>
                    array (
                        'sku' => '279827',
                        'ean' => '9788391337752',
                        'price_netto_buy' => '10.71000000',
                        'contract_code' => 'AZZ01',
                        'eta' => '1',
                        'srp' => '15.0045',
                        'rate' => '5',
                        'qty' => '0',
                        'premiere_at' => NULL,
                    ),
            ];

        $merlinProductsProperty->setValue($productsSync, $merlinProducts);

        $aMerlinSkuGrouped = [279827, 279826];
        $sku = $method->invokeArgs($productsSync, ['0' => $aMerlinSkuGrouped]);
        $this->assertEquals('279826', $sku);



        // preview + available 1
        $merlinProducts =
            [
                279826 =>
                    array (
                        'sku' => '279826',
                        'ean' => '5901844916920',
                        'price_netto_buy' => '22.00000000',
                        'contract_code' => 'SON01',
                        'eta' => '2',
                        'srp' => NULL,
                        'rate' => '23',
                        'qty' => '0',
                        'premiere_at' => '2050-01-01',
                    ),
                279827 =>
                    array (
                        'sku' => '279827',
                        'ean' => '9788391337752',
                        'price_netto_buy' => '10.71000000',
                        'contract_code' => 'AZZ01',
                        'eta' => '1',
                        'srp' => '15.0045',
                        'rate' => '5',
                        'qty' => '0',
                        'premiere_at' => NULL,
                    ),
            ];

        $merlinProductsProperty->setValue($productsSync, $merlinProducts);

        $aMerlinSkuGrouped = [279826, 279827];
        $sku = $method->invokeArgs($productsSync, ['0' => $aMerlinSkuGrouped]);
        $this->assertEquals('279826', $sku);




        // preview + available 1
        $merlinProducts =
            [
                279826 =>
                    array (
                        'sku' => '279826',
                        'ean' => '5901844916920',
                        'price_netto_buy' => '22.00000000',
                        'contract_code' => 'SON01',
                        'eta' => '2',
                        'srp' => NULL,
                        'rate' => '23',
                        'qty' => '0',
                        'premiere_at' => '2050-01-01',
                    ),
                279827 =>
                    array (
                        'sku' => '279827',
                        'ean' => '9788391337752',
                        'price_netto_buy' => '10.71000000',
                        'contract_code' => 'AZZ01',
                        'eta' => '1',
                        'srp' => '15.0045',
                        'rate' => '5',
                        'qty' => '1',
                        'premiere_at' => NULL,
                    ),
            ];

        $merlinProductsProperty->setValue($productsSync, $merlinProducts);

        $aMerlinSkuGrouped = [279826, 279827];
        $sku = $method->invokeArgs($productsSync, ['0' => $aMerlinSkuGrouped]);
        $this->assertEquals('279827', $sku);



        // preview + available 1
        $merlinProducts =
            [
                279826 =>
                    array (
                        'sku' => '279826',
                        'ean' => '5901844916920',
                        'price_netto_buy' => '22.00000000',
                        'contract_code' => 'SON01',
                        'eta' => '2',
                        'srp' => NULL,
                        'rate' => '23',
                        'qty' => '0',
                        'premiere_at' => NULL,
                    ),
                279827 =>
                    array (
                        'sku' => '279827',
                        'ean' => '9788391337752',
                        'price_netto_buy' => '10.71000000',
                        'contract_code' => 'AZZ01',
                        'eta' => '1',
                        'srp' => '15.0045',
                        'rate' => '5',
                        'qty' => '0',
                        'premiere_at' => NULL,
                    ),
            ];

        $merlinProductsProperty->setValue($productsSync, $merlinProducts);

        $aMerlinSkuGrouped = [279827, 279826];
        $sku = $method->invokeArgs($productsSync, ['0' => $aMerlinSkuGrouped]);
        $this->assertEquals('279827', $sku);
    }



}
