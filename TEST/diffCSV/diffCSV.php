<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2015-01-15 
 * @copyrights Marcin Chudy - Profit24.pl
 */
/**
 * 
 * @param array $aBase
 * @param string $sISBN
 * @param float $fPrice - sep ","
 * @return boolean
 */
function searchInBase($aBase, $sISBN, $fPrice) {
  foreach ($aBase as $aItem) {
    if ($aItem['isbn'] == $sISBN && $aItem['price'] == $fPrice) {
      return true;
    }
  }
  return false;
}

$sBaseFile = "przychody ateneum.csv";
$sToSearchFile = "magazyny ateneum.csv";
$aToSearch = array();
$aBase = array();

$iCount = 0;
if (($handle = fopen($sBaseFile, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
      $aBase[$iCount]['isbn'] = $data[0];
      $aBase[$iCount]['price'] = $data[1];
      $iCount++;
    }
    fclose($handle);
}

if (($handle = fopen($sToSearchFile, "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 0, ";")) !== FALSE) {
      $aToSearch[$iCount]['isbn'] = $data[0];
      $aToSearch[$iCount]['price'] = $data[3];
      $aToSearch[$iCount]['name'] = $data[1];
      $aToSearch[$iCount]['quantity'] = $data[2];
      $iCount++;
    }
    fclose($handle);
}

$fp = fopen('search_Ateneum.csv', 'w');
// wyszykiwanie
foreach ($aToSearch as $aToSearchItem) {
  if ($aToSearchItem['isbn'] != '' && searchInBase($aBase, $aToSearchItem['isbn'], $aToSearchItem['price']) === TRUE) {
    $aToSearchItem['ZWROT'] = '1';
    fputcsv($fp, $aToSearchItem, ";");
  } else {
    $aToSearchItem['ZWROT'] = '0';
    fputcsv($fp, $aToSearchItem, ";");
  }
}
fclose($fp);