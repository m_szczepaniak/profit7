<?php
$host = '192.168.1.208:3050//var/lib/firebird/data/PROFIT.gdb';
$username = 'sysdba';
$password = 'ytrewq54321';
$dbh = ibase_connect($host, $username, $password, 'ISO8859_2');


$stmt = "SELECT FIRST 10
  m.oznnrwydr                                            AS MAGAZYN,
  k.indeks                                               AS INDEKS,
  CASE WHEN (n.id_defdok = 10001)
    THEN 'MM'
  ELSE 'FA' END                                          AS RODZAJ,
  sum(CASE WHEN (dd.ID_SPISDOK != 10)
    THEN (CASE WHEN (d.blokada = 0)
      THEN d.STANAKTDOST END)
      ELSE d.STANAKTDOST END)                            AS ILOSC,
  d.cenazakdost                                          AS CENA_ZAKUPU,
  sv.ident                                               AS STAWKA_VAT,
  c.cenan                                                AS CENA_SPRZ_NETTO,
  c.cenab                                                AS CENA_SPRZ_BRUTTO,
  sum(d.STANZAREZERWOWANY)                               AS REZ,
  sum(CASE WHEN (dd.ID_SPISDOK != 10)
    THEN (CASE WHEN (d.blokada = 0)
      THEN d.STANAKTDOST END)
      ELSE d.STANAKTDOST END) - sum(d.STANZAREZERWOWANY) AS POREZ
FROM dostawa d
  JOIN magazyn m ON m.id_magazyn = d.id_magazyn
  JOIN poz p ON p.id_poz = d.id_pozzrodla
  JOIN kartoteka k ON k.id_kartoteka = p.id_kartoteka
  JOIN cennik c ON c.id_kartoteka = p.id_kartoteka AND c.id_defceny = 10001
  JOIN nagl n ON n.id_nagl = p.id_nagl
  JOIN stawkavat sv ON sv.id_stawkavat = p.id_stawkavat
  JOIN poz p2 ON p2.id_poz = d.id_poz
  JOIN nagl n2 ON n2.id_nagl = p2.id_nagl
  JOIN defdok dd ON dd.id_defdok = n2.id_defdok

WHERE
d.id_magazyn = 10009 AND d.stanaktdost != 0 AND indeks = '9788361987338'
GROUP BY MAGAZYN, INDEKS, RODZAJ, ILOSC, CENA_ZAKUPU, STAWKA_VAT, CENA_SPRZ_NETTO, CENA_SPRZ_BRUTTO, REZ, POREZ
HAVING sum(CASE WHEN (dd.ID_SPISDOK != 10)
  THEN (CASE WHEN (d.blokada = 0)
    THEN d.STANAKTDOST END)
           ELSE d.STANAKTDOST END) <> 0
ORDER BY k.indeks, d.cenazakdost DESC
";
$sth = ibase_query($dbh, $stmt);
while ($row = ibase_fetch_object($sth)) {
    echo $row->NAZWA_KOLUMNY, "\n";
}
ibase_free_result($sth);
ibase_close($dbh);