<?php
/**
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-07-08 
 * @copyrights Marcin Chudy - Profit24.pl
 */
$ps = PATH_SEPARATOR;
$Ds = DIRECTORY_SEPARATOR;
$sFullPaths = realpath(dirname(__FILE__));
$aPaths = explode($Ds, $sFullPaths);
array_pop($aPaths);
$sBasePath = implode($Ds, $aPaths);

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors','On');

set_include_path(get_include_path() . PATH_SEPARATOR . $sBasePath);
//require_once "PHPUnit/Autoload.php";
require_once "autoloader.php";
require_once "LIB/autoloader.php";
//require_once "inc/functions.inc.php";

ini_set("include_path", ini_get("include_path").$ps.$sBasePath.$ps.$sBasePath.$Ds.'omniaCMS'.$Ds.$ps.$sBasePath.$Ds.'omniaCMS'.$Ds.'lib'.$ps.$sBasePath.$Ds.'omniaCMS'.$Ds.'lib'.$Ds.'PEAR');
include('config/common.inc.php');
$aConfig['common']['client_base_path'] = $sBasePath.'/';
$aConfig['common']['base_path'] = $sBasePath.'/';