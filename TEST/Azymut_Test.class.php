<?php
/**
 * Klasa testowa dla komunikacji zamówień z Azymut
 * 
 * @author Arkadiusz Golba <arekgl0@op.pl>
 * @created 2013-09-20 
 * @copyrights Marcin Chudy - Profit24.pl
 */
error_reporting(E_ERROR | E_WARNING | E_PARSE); 
$aConfig['config']['project_dir'] = 'TEST/';
$_SERVER['DOCUMENT_ROOT'] = ($_SERVER['DOCUMENT_ROOT'] != '' ? $_SERVER['DOCUMENT_ROOT'] : substr(dirname(__FILE__), 0, -(strlen($aConfig['config']['project_dir']))));
include_once($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/common.inc.php');
$aConfig['common']['use_session'] = false;
include_once ($_SERVER['DOCUMENT_ROOT'].'/omniaCMS/config/ini.inc.php');

require_once($_SERVER['DOCUMENT_ROOT'].'LIB/autoloader.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/communicator/sources/CommonSources.class.php');
require_once($_SERVER['DOCUMENT_ROOT'].'LIB/communicator/sources/Azymut/Azymut.class.php');

class Azymut_Test {
  function __construct() {}
  
  public function test_parseXMLgetInv() {


    $sTransactionId = '2013-09-23_14-49-88_0_03_03831';
    $oAutoProviderToOrders = new Azymut(TRUE);
    
    echo "\nTEST 1:";
    if ($oAutoProviderToOrders->parseXMLgetInv($_SERVER['DOCUMENT_ROOT'].'/TEST/xml-test/getInv.XML', $sTransactionId) === TRUE) {
      echo 'OK';
    } else {
      echo 'ERR';
    }
    
  }
  
  
  public function test_parseXMLpostRR() {


    $oAutoProviderToOrders = new Azymut(TRUE);
    
    echo "\nTEST 2:";
    if ($oAutoProviderToOrders->parseXMLpostRR($_SERVER['DOCUMENT_ROOT'].'/TEST/xml-test/postRR.XML') === TRUE){
      echo 'OK';
    } else {
      echo 'ERR';
    }   
  }
  
  
  public function test___putOrder() {
    
    $oAutoProviderToOrders = new Azymut(TRUE);
    echo "\nTEST wysyłki XML do Azymut:";
    if ($oAutoProviderToOrders->__putOrder() === TRUE) {
      echo 'OK';
    } else {
      echo 'ERR';
    }
  }
}

$oAzymut_Test = new Azymut_Test();
//$oAzymut_Test->test___putOrder();
//$oAzymut_Test->test_parseXMLpostRR();
$pDbMgr->BeginTransaction('profit24');
//$oAzymut_Test->test_parseXMLgetInv();
$pDbMgr->CommitTransaction('profit24');