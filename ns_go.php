<?php
include_once('omniaCMS/config/common.inc.php');
// nie wlaczaj sesji
$aConfig['common']['use_session'] = false;
include_once ('omniaCMS/config/ini.inc.php');

/**
 * Funkcja pobiera link i Id $iId dla newslettera o Id $iNId
 * 
 * @param	integer	$iNId	- Id newslettera
 * @param	integer	$iId	- Id linku
 * @return	string
 */
function getLink($iNId, $iId) {
	global $aConfig;
	
	$sSql = "SELECT link
					 FROM ".$aConfig['tabls']['prefix']."newsletters_links
					 WHERE newsletter_id = ".$iNId." AND
					 			 id = ".$iId;
	return Common::GetOne($sSql);
} // end of getLink() method

/**
 * Funkcja datę wysłania dla newslettera o Id $iNId
 * 
 * @param	integer	$iNId	- Id newslettera
 * @return	string
 */
function getNewsletterDate($iNId) {
	global $aConfig;
	
	/*$sSql = "SELECT DATE_FORMAT(IF(A.auto_id IS NULL, A.sent, B.send_date), '%d-%m-%Y') AS newsletter_date
					 FROM ".$aConfig['tabls']['prefix']."newsletters A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."newsletters_auto B
					 ON B.id=A.auto_id
					 WHERE A.id = ".$iNId;*/
	$sSql = "SELECT IF(A.auto_id IS NULL,'',DATE_FORMAT(B.send_date, '%d-%m-%Y')) AS newsletter_date
					 FROM ".$aConfig['tabls']['prefix']."newsletters A
					 LEFT JOIN ".$aConfig['tabls']['prefix']."newsletters_auto B
					 ON B.id=A.auto_id
					 WHERE A.id = ".$iNId;
	return Common::GetOne($sSql);
} // end of getNewsletterDate() method

/**
 * Funkcja odpowiada za zapis klikniecia w link newslettera, proobe
 * pobrania danych uzykownika - jego Id i telefon
 * 
 * @param	integer	$iNId	- Id newslettera
 * @param	integer	$iId	- Id Id linku
 * @param	string	$sEmail	- email klikajacego
 * @return	void
 */
function addClick($iNId, $iId, $sEmail) {
	global $aConfig;
	
	$aValues = array(
		'link_id' => $iId,
		'click_date'	=> 'NOW()',
		'source_email' => $sEmail
	);
	
	// prooba odnalezienia danych klikajacego
	$sSql = "SELECT id
					 FROM ".$aConfig['tabls']['prefix']."users_accounts
					 WHERE email = '".$sEmail."'";
	$aUser =& Common::GetRow($sSql);
	if (!empty($aUser)) {
		$aValues['source_konto_id'] = $aUser['id'];
	}
	
	// zapis do bazy
	Common::Insert($aConfig['tabls']['prefix']."newsletters_clicked_links", $aValues, '', false);

	// zwiekszenie liczby klikniec linku
	$sSql = "UPDATE ".$aConfig['tabls']['prefix']."newsletters_links SET clicked = clicked + 1
					 WHERE newsletter_id = ".$iNId." AND
					 			 id = ".$iId;
	Common::Query($sSql);
} // end of addClick() function

/**
 * Funkcja przekierowuje przegladarke uzytkownika na adres
 * zdefiniowany dla linku
 * 
 * @param	string	$sUrl	- adres na ktory nastapi przekierowanie
 * @return	void
 */
function redirect($sUrl='/') {
	$sUrl = str_replace("&amp;", "&", $sUrl);
	header("Location: ".$sUrl);
	exit(0);
} // end of redirect() function

/**
 * Funkcja do linku pod ktory nastapi przekierowanie dodaje parametr
 * informujacy o zrodle klikniecia 'newsletter:$iNId'
 * 
 * @param	string	$sLink	- link
 * @param	integer	$iNId	- Id newslettera
 * @return	string
 */
function addSourceInfo($sLink, $iNId, $sDate='') {
	global $aConfig;
	if (strpos($sLink, 'profit24.pl') != '') {
		$sSource = base64_encode('newsletter:'.$iNId);
		if (strpos($sLink, '?') !== false) {
			// QUERY_STRING juz jest
			$sLink .= '&src=';
		}
		else {
			// brak QUERY_STRING
			$sLink .= '?src=';
		}
		$sLink .= $sSource;
		if($aConfig['common']['urchin_tracker'] && !empty($sDate)){
			$sLink .= '&utm_source=newsletter&utm_medium=email&utm_campaign='.$sDate;
		}
	}
	return $sLink;
} // end of addSourceInfo() function

/**
 * Funkcja sprawdza czy link rozpoczyna sie od http://, jezeli nie dodaje
 * 
 * @param	string	$sLink	- link
 * @return	string
 */
function checkHttp($sLink) {
	if (!preg_match('/^http:\/\//', $sLink)) {
		$sLink = 'http://'.$sLink;
	}
	return $sLink;
} // end of checkHttp() function

if (is_numeric($_GET['nid']) && is_numeric($_GET['id']) && isset($_GET['email'])) {
	// odkodowanie adresu email
	$sEmail = base64_decode($_GET['email']);
	$iNId = (double) $_GET['nid'];
	$iId = (double) $_GET['id'];
	//echo $sEmail.' , '.$iNId.' , '.$iId ."\n";die();
	if (preg_match($aConfig['class']['form']['email']['pcre'], $sEmail)) {
		// adres email jest prawidlowy
		// pobranie adresu linku
		
		$sLink = getLink($iNId, $iId);
		
		if ($sLink != '') {
			$sDate = getNewsletterDate($iNId);
			// dodanie informacji o kliknieciu
			addClick($iNId, $iId, $sEmail);
			// przekierowanie do linku
			redirect(checkHttp(addSourceInfo($sLink, $iNId, $sDate)));
		}
	}
	// jezeli jestesmy tutaj - albo adres email nieprawidlowy albo cos namieszane
	// z Id newslettera i Id linku
	redirect();
}
?>