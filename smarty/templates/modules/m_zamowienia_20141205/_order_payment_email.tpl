<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://w3.org/TR/html4/transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="font-family: Tahoma, Arial, Verdana, Helvetica; padding: 0; margin: 0; font-size: 11px; background: url({$aModule.order.path}/images/gfx/mail_top_tlo.gif) repeat-x;">
	<div style="width: 100%">
		<div style="margin: 0 auto; width: 650px;">	
			<div style="height: 68px; ">
				<img src="logo_mail.png" width="200" height="76" />
			</div>
			<div style="height: 23px; text-align: right; flaot: right; color: #000; padding: 25px 0 0 0; font-weight: bold; font-size: 14px;">{$aModule.lang.order_number} {$aModule.order.order_number}</div>
			{$aModule.lang.selected_transport} <strong>{$aModule.order.transport}</strong><br/>
			{$aModule.lang.selected_payment} <strong>{$aModule.order.payment}</strong><br/>
			{if $aModule.order.payment_type == 'bank_transfer' || $aModule.order.payment_type == 'bank_14days'}
			<div style="font-size: 12px; font-weight: bold; margin: 21px 0 17px 0; padding: 0 0 2px 10px;">{$aModule.lang.seller_data_header}</div>
			<div style="margin: 0 0 40px 31px; line-height: 150%;">
				{$aModule.seller_data}
			</div>
		{/if}
		</div>
	</div>	
<img src="{$aConfig.common.client_base_path}/images/gfx/mail_top_tlo.gif" width=0 height=0 alt ="" />	
<img src="{$aConfig.common.client_base_path}images/gfx/logo_mail.png" width=0 height=0 alt ="" />
<img src="{$sBaseHref}/images/gfx/menu_podzialka.gif" width=0 height=0 alt ="" />	
</body>
</html>
